/*!
\file
implementation for Transport class
*/
#include "wpch.hpp"
#include "transport.hpp"
#include "ai.hpp"
#include "proxyWeapon.hpp"
#include "person.hpp"
#include "global.hpp"
#include "world.hpp"
#include "landscape.hpp"
//#include "loadStream.hpp"
#include <El/Common/randomGen.hpp>
#include "keyInput.hpp"
#include "diagModes.hpp"

#include "titEffects.hpp"
#include "camEffects.hpp"
//#include "strIncl.hpp"
#include "stringtableExt.hpp"
#include "sentences.hpp"
#include "camera.hpp"

#include "network.hpp"
#include "chat.hpp"
#include "uiActions.hpp"
#include "frameInv.hpp"

#include "move_actions.hpp"
#include "manActs.hpp"
#include "SpecLods.hpp"

#include <El/Common/enumNames.hpp>

// TODO: remove AIUnit::Sending

//static const float CmdPlanDist = 100.0f;
//static const float CmdMinDist = 40.0f;
static const float CmdPlanDist = 30.0f;
static const float CmdMinDist = 10.0f;

const float DriverReactionTime=0.3;

// common implementation of transport vehicles

DEFINE_CASTING(Transport)

/*
static float TransportGetFieldCost(int x, int z, void *param)
{
	if (!param) return SET_UNACCESSIBLE;
	Transport *vehicle = (Transport *)param;
	//return subgrp->GetFieldCost(x, z);

	if( !InRange(x,z) ) return SET_UNACCESSIBLE;
	GeographyInfo geogr = GLOB_LAND->GetGeography(x,z);

	float cost = vehicle->GetCost(geogr)*vehicle->GetFieldCost(geogr);	
	cost *= LandGrid; // cost = time for distance == LandGrid
	return cost;
}
*/

#pragma warning(disable:4355)

// config identifiers
#define TitleTxt 0 // down fades text
#define TitleTxtDown 1 // down fades text
#define TitleRsc 2 // resource text
#define TitleObj 3

/*!
\patch 1.47 Date 3/7/2002 by Jirka
- Fixed: Taking weapon from cargo space could crash dedicated server.
*/

void CutScene( const char *name )
{
	if (!GWorld->CameraOn()) return;

	static const char *lastName=NULL;
	static Link<TitleEffect> lastEffect;
	static Link<AbstractWave> lastSound;

	TitleEffect *effect=GWorld->GetTitleEffect();
	if( effect && effect==lastEffect )
	{
		if( !name )
		{
			effect->Terminate();
			return;
		}
		else if( name==lastName )
		{
			effect->Prolong(1);
			// TODO: prolong sound effect?
		}
	}
	if( effect || !name ) return;

	if( GWorld->GetCameraEffect() ) return;
	
	const ParamEntry &entry=Pars>>"CfgCutScenes">>name;
	// set-up title
	int titName=entry>>"titleType";
	RString titText=entry>>"title";

	TitleEffect *titEff=NULL;
	switch( titName )
	{
		case TitleTxt: titEff=CreateTitleEffect(TitPlain,titText);break;
		case TitleTxtDown: titEff=CreateTitleEffect(TitPlainDown,titText);break;
		//case TitleRsc: titEff=CreateTitleEffectRsc(TitPlainDown,titText);break;
	}
	if( titEff )
	{
		GWorld->SetTitleEffect(titEff);
		lastEffect=titEff;
		lastName=name;
	}

	// set-up sound effect
	SoundPars pars;
	GetValue(pars, entry>>"sound");
	if( pars.name.GetLength()>0 )
	{
		AbstractWave *wave=GSoundScene->OpenAndPlayOnce
		(
			pars.name,
			GWorld->CameraOn()->Position(),GWorld->CameraOn()->ObjectSpeed(),
			pars.vol,pars.freq
		);
		if( wave )
		{
			GSoundScene->AddSound(wave);
			lastSound=wave;
		}
	}
}


static bool IsSupply( EntityAI *vehicle )
{
	const VehicleType *type=vehicle->GetType();
	if( type->GetMaxFuelCargo()>0 ) return true;
	if( type->GetMaxRepairCargo() ) return true;
	if( type->GetMaxAmmoCargo() ) return true;
	if( type->GetMaxWeaponsCargo()>0 ) return true;
	if( type->GetMaxMagazinesCargo()>0 ) return true;
	if( type->IsAttendant() ) return true;
	for (int i=0; i<type->_weaponCargo.Size(); i++)
	{
		if (type->_weaponCargo[i].count > 0) return true;
	}
	for (int i=0; i<type->_magazineCargo.Size(); i++)
	{
		if (type->_magazineCargo[i].count > 0) return true;
	}
	return type->_forceSupply;
}

ResourceSupply::ResourceSupply( EntityAI *vehicle )
:_parent(vehicle)
{
	const VehicleType *type=vehicle->GetType();
	_fuelCargo = type->GetMaxFuelCargo();
	_repairCargo = type->GetMaxRepairCargo();
	_ammoCargo = type->GetMaxAmmoCargo();
	//_infantryAmmoCargo = type->GetMaxInfantryAmmoCargo();
	for (int i=0; i<type->_weaponCargo.Size(); i++)
	{
		const WeaponCargoItem &item = type->_weaponCargo[i];
		AddWeaponCargo(item.weapon, item.count);
	}
	for (int i=0; i<type->_magazineCargo.Size(); i++)
	{
		const MagazineCargoItem &item = type->_magazineCargo[i];
		AddMagazineCargo(item.magazine, item.count);
	}
	// init vars
	// _lastSupportTime=Glob.time-60;
	_action = ATNone;
	_actionParam = 0;
	_actionParam2 = 0;
}

// used for serialization only
ResourceSupply::ResourceSupply()
{
	_fuelCargo = 0;
	_repairCargo = 0;
	_ammoCargo = 0;
	_action = ATNone;
	_actionParam = 0;
	_actionParam2 = 0;
}

bool CheckSupply(EntityAI *vehicle, EntityAI *parent, SupportCheckF check, float limit, bool now)
{
	if (parent == vehicle) return false;

	if (now)
	{
		// check distance
		Vector3 supplyPos = parent->PositionModelToWorld
		(
			parent->GetType()->GetSupplyPoint()
		);
		float supplyRadius = parent->GetType()->GetSupplyRadius();

		float dist2 = vehicle->Position().Distance2(supplyPos);
		if (dist2 > Square(40)) return false;
		float maxDistance = vehicle->CollisionSize() * 1.2 + 1 + supplyRadius;
		if (dist2 > Square(maxDistance)) return false;
	//	if (vehicle->Speed().Distance2(parent->Speed()) > Square(0.5)) return false;
	}

	if (check)
		return (vehicle->*check)() > limit;
	else
		return true;
}

bool ResourceSupply::Check(EntityAI *vehicle, SupportCheckF check, float limit, bool now) const
{
	if (!vehicle) return false;
	if (!vehicle->GetShape()) return false;

	return CheckSupply(vehicle, _parent, check, limit, now);
}

/*
EntityAI *ResourceSupply::Scan( SupportCheckF check, float limit ) const
{
	EntityAI *par=_parent;
	EntityAI *needsMost=NULL;
	float needMax=0;
	float myRadius=par->CollisionSize()*1.2;
	Vector3 supplyPos=par->PositionModelToWorld(par->GetType()->GetSupplyPoint());
	float supplyRadius=par->GetType()->GetSupplyRadius();

	for( int i=0; i<GWorld->NVehicles(); i++ )
	{
		Vehicle *v=GWorld->GetVehicle(i);
		Vector3Val pos=v->Position();
		float dist2=pos.Distance2(supplyPos);
		if( dist2>Square(40) ) continue;
		EntityAI *vehicle=dyn_cast<EntityAI>(v);
		if( !vehicle ) continue;
		if( !vehicle->GetShape() ) continue;
		float maxDistance=vehicle->CollisionSize()*1.2+1+supplyRadius;
		if( dist2>Square(maxDistance) ) continue;
		if( vehicle->Speed().Distance2(par->Speed())>Square(0.5) ) continue;
		// TODO: balance between need/distance
		if( par==vehicle ) continue;
		float need=(vehicle->*check)();
		if( need<limit ) continue;
		if( needMax<need ) needMax=need,needsMost=vehicle;
	}
	return needsMost;
}
*/

static const EnumName UIActionNames[]=
{
	EnumName(ATNone, "NONE"),
	EnumName(ATGetInCommander, "GETIN COMMANDER"),
	EnumName(ATGetInDriver, "GETIN DRIVER"),
	EnumName(ATGetInGunner, "GETIN GUNNER"),
	EnumName(ATGetInCargo, "GETIN CARGO"),
	EnumName(ATHeal, "HEAL"),
	EnumName(ATRepair, "REPAIR"),
	EnumName(ATRefuel, "REFUEL"),
	EnumName(ATRearm, "REARM"),
	EnumName(ATGetOut, "GETOUT"),
	EnumName(ATLightOn, "LIGHT ON"),
	EnumName(ATLightOff, "LIGHT OFF"),
	EnumName(ATEngineOn, "ENGINE ON"),
	EnumName(ATEngineOff, "ENGINE OFF"),
	EnumName(ATSwitchWeapon, "SWITCH WEAPON"),
	EnumName(ATUseWeapon, "USE WEAPON"),
	EnumName(ATLoadMagazine, "LOAD MAGAZINE"),
	EnumName(ATTakeWeapon, "TAKE WEAPON"),
	EnumName(ATTakeMagazine, "TAKE MAGAZINE"),
	EnumName(ATTakeFlag, "TAKE FLAG"),
	EnumName(ATReturnFlag, "RETURN FLAG"),
	EnumName(ATTurnIn, "TURNIN"),
	EnumName(ATTurnOut, "TURNOUT"),
	EnumName(ATWeaponInHand, "WEAPONINHAND"),
	EnumName(ATWeaponOnBack, "WEAPONONBACK"),
	EnumName(ATSitDown, "SITDOWN"),
	EnumName(ATLand, "LAND"),
	EnumName(ATCancelLand, "CANCEL LAND"),
	EnumName(ATEject, "EJECT"),
	EnumName(ATMoveToDriver, "MOVETODRIVER"),
	EnumName(ATMoveToGunner, "MOVETOGUNNER"),
	EnumName(ATMoveToCommander, "MOVETOCOMMANDER"),
	EnumName(ATMoveToCargo, "MOVETOCARGO"),
	EnumName(ATHideBody,"HIDEBODY"),
	EnumName(ATTouchOff,"TOUCHOFF"),
	EnumName(ATSetTimer,"SETTIMER"),
	EnumName(ATDeactivate,"DEACTIVATE"),
	EnumName(ATManualFire,"MANUALFIRE"),
	EnumName(ATNVGoggles,"NVGOGGLES"),
	EnumName(ATAutoHover,"AUTOHOVER"),
	EnumName(ATStrokeFist,"STROKEFIST"),
	EnumName(ATStrokeGun,"STROKEGUN"),

	EnumName(ATLadderUp,"LADDERUP"),
	EnumName(ATLadderDown,"LADDERDOWN"),
	EnumName(ATLadderOnDown,"LADDERONDOWN"),
	EnumName(ATLadderOnUp,"LADDERONUP"),
	EnumName(ATLadderOff,"LADDEROFF"),
	EnumName(ATFireInflame, "FIRE INFLAME"),
	EnumName(ATFirePutDown, "FIRE PUT DOWN"),

	EnumName(ATLandGear,"LAND GEAR"),
	EnumName(ATFlapsDown,"FLAPS DOWN"),
	EnumName(ATFlapsUp,"FLAPS UP"),

	EnumName(ATSalute,"SALUTE"),

	EnumName(ATScudLaunch, "SCUD LAUNCH"),
	EnumName(ATScudStart, "SCUD START"),

	EnumName(ATUser, "USER"),

	EnumName(ATDropWeapon, "DROP WEAPON"),
	EnumName(ATDropMagazine, "DROP MAGAZINE"),

	EnumName(ATUserType, "USER TYPE"),

	EnumName(ATHandGunOn, "HANDGUN ON"),
	EnumName(ATHandGunOff, "HANDGUN OFF"),
	
	EnumName(ATTakeMine, "TAKE MINE"),
	EnumName(ATDeactivateMine, "DEACTIVATE MINE"),

	EnumName(ATUseMagazine, "USE MAGAZINE"),
	
	EnumName()
};
template<>
const EnumName *GetEnumNames(UIActionType dummy)
{
	return UIActionNames;
}

LSError ResourceSupply::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("fuelCargo", _fuelCargo, 1, 0))
	CHECK(ar.Serialize("repairCargo", _repairCargo, 1, 0))
	CHECK(ar.Serialize("ammoCargo", _ammoCargo, 1, 0))

	// no need to serialize parent - sets in VehicleSupply::Serialize
	if (!IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
	{
		CHECK(ar.SerializeRef("Supplying", _supplying, 1))
		CHECK(ar.SerializeRef("Alloc", _alloc, 1))
		CHECK(ar.SerializeEnum("action", _action, 1, (UIActionType)ATNone))
		CHECK(ar.Serialize("actionParam", _actionParam, 1, 0))
		CHECK(ar.Serialize("actionParam2", _actionParam2, 1, 0))
		CHECK(ar.Serialize("actionParam3", _actionParam3, 1, ""))
	}

	CHECK(ar.Serialize("WeaponCargo", _weaponCargo, 1));
	CHECK(ar.Serialize("MagazineCargo", _magazineCargo, 1));

//	CHECK(ar.Serialize("lastSupportTime", _lastSupportTime, 1))

	return LSOK;
}

/*!
\patch 1.96d Date 9/16/2005 by Ondra
- Fixed: Possible excessive network bandwidth usage due to using uninitialized memory.
*/

IndicesResourceSupply::IndicesResourceSupply()
{
	fuelCargo = -1;
	repairCargo = -1;
	ammoCargo = -1;
	//infantryAmmoCargo = -1;
	supplying = -1;
	alloc = -1;
	action = -1;
	actionParam = -1;
	actionParam2 = -1;
	actionParam3 = -1;
}

void IndicesResourceSupply::Scan(NetworkMessageFormatBase *format)
{
	SCAN(fuelCargo)
	SCAN(repairCargo)
	SCAN(ammoCargo)
	//SCAN(infantryAmmoCargo)
	SCAN(supplying)
	SCAN(alloc)
	SCAN(action)
	SCAN(actionParam)
	SCAN(actionParam2)
	SCAN(actionParam3)
}

void ResourceSupply::CreateFormat(NetworkMessageFormat &format)
{
	format.Add("fuelCargo", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Transported fuel"), ET_ABS_DIF, 0.01 * ERR_COEF_VALUE_MINOR);
	format.Add("repairCargo", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Transported repair material"), ET_ABS_DIF, 0.01 * ERR_COEF_VALUE_MINOR);
	format.Add("ammoCargo", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Transported ammunition (for vehicles)"), ET_ABS_DIF, 0.01 * ERR_COEF_VALUE_MINOR);
	//format.Add("infantryAmmoCargo", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Obsolete"), ET_ABS_DIF, 0.01 * ERR_COEF_VALUE_MINOR);
	format.Add("supplying", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Currently supplying unit"), ET_NOT_EQUAL, ERR_COEF_MODE);
	format.Add("alloc", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Unit allocated for supplying"), ET_NOT_EQUAL, ERR_COEF_MODE);
	format.Add("action", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, ATNone), DOC_MSG("Currently processing action"), ET_NOT_EQUAL, ERR_COEF_MODE);
	format.Add("actionParam", NDTInteger, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Action parameter"), ET_NOT_EQUAL, ERR_COEF_MODE);
	format.Add("actionParam2", NDTInteger, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Action parameter"), ET_NOT_EQUAL, ERR_COEF_MODE);
	format.Add("actionParam3", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Action parameter"), ET_NOT_EQUAL, ERR_COEF_MODE);
/*
	format.Add("weaponCargo", NDTStringArray, NCTNone, DEFVALUESTRINGARRAY, ET_NOT_EQUAL, ERR_COEF_MODE);
	format.Add("magazineCargo", NDTObjectArray, NCTNone, DEFVALUE_MSG(NMTMagazine));
*/

//	format.Add("lastSupportTime", NDTTime, NCTNone, DEFVALUE(Time, Time(0)));
}

TMError ResourceSupply::TransferMsg(NetworkMessageContext &ctx, const IndicesResourceSupply *indices)
{
	ITRANSF(fuelCargo)
	ITRANSF(repairCargo)
	ITRANSF(ammoCargo)
	//ITRANSF(infantryAmmoCargo)
	ITRANSF_REF(supplying)
	ITRANSF_REF(alloc)
//	ITRANSF(lastSupportTime)
	ITRANSF_ENUM(action)
	ITRANSF(actionParam)
	ITRANSF(actionParam2)
	ITRANSF(actionParam3)
/*
	// magazines
	TMCHECK(ctx.IdxTransferObjArray(indices->magazineCargo, _magazineCargo));
	// weapons
	if (ctx.IsSending())
	{
		// weapons
		AutoArray<RString> weaponCargo;
		weaponCargo.Resize(_weaponCargo.Size());
		for (int i=0; i<_weaponCargo.Size(); i++) weaponCargo[i] = _weaponCargo[i]->GetName();
		TMCHECK(ctx.IdxTransfer(indices->weaponCargo, weaponCargo))
	}
	else
	{
		// weapons
		AutoArray<RString> weaponCargo;
		TMCHECK(ctx.IdxTransfer(indices->weaponCargo, weaponCargo));
		_weaponCargo.Resize(weaponCargo.Size());
		for (int i=0; i<weaponCargo.Size(); i++)
		{
			_weaponCargo[i] = WeaponTypes.New(weaponCargo[i]);
		}
	}
*/
	return TMOK;
}

float ResourceSupply::CalculateError(NetworkMessageContext &ctx, const IndicesResourceSupply *indices)
{
	float error = 0;
	switch (ctx.GetClass())
	{
	case NMCUpdateGeneric:
		ICALCERR_NEQREF(EntityAI, alloc, ERR_COEF_MODE)
		ICALCERR_NEQREF(EntityAI, supplying, ERR_COEF_MODE)
		ICALCERR_NEQ(int, action, ERR_COEF_MODE)
		ICALCERR_NEQ(int, actionParam, ERR_COEF_MODE)
		ICALCERR_NEQ(int, actionParam2, ERR_COEF_MODE)
		ICALCERR_NEQSTR(actionParam3, ERR_COEF_MODE)
		ICALCERR_ABSDIF(float, fuelCargo, 0.01 * ERR_COEF_VALUE_MINOR)
		ICALCERR_ABSDIF(float, repairCargo, 0.01 * ERR_COEF_VALUE_MINOR)
		ICALCERR_ABSDIF(float, ammoCargo, 0.01 * ERR_COEF_VALUE_MINOR)
		//ICALCERR_ABSDIF(float, infantryAmmoCargo, 0.01 * ERR_COEF_VALUE_MINOR)
	/*
		// weapons
		AutoArray<RString> weaponCargo;
		if (ctx.IdxTransfer(indices->weaponCargo, weaponCargo) == TMOK)
		{
			bool changed = weaponCargo.Size() != _weaponCargo.Size();
			if (!changed) for (int i=0; i<weaponCargo.Size(); i++)
				if (weaponCargo[i] != _weaponCargo[i]->GetName())
				{
					changed = true; break;
				}
			if (changed) error += ERR_COEF_MODE;
		}
		// magazines
		RefArray<Magazine> magazineCargo;
		if (ctx.IdxTransferObjArray(indices->magazineCargo, magazineCargo) == TMOK)
		{
			bool changed = magazineCargo.Size() != _magazineCargo.Size();
			if (!changed) for (int i=0; i<_magazineCargo.Size(); i++)
				if (magazineCargo[i]->_creator != _magazineCargo[i]->_creator || magazineCargo[i]->_id != _magazineCargo[i]->_id)
				{
					changed = true; break;
				}
			if (changed) error += ERR_COEF_MODE;
		}
	*/
		break;
	default:
		break;
	}
	return error;
}

bool ResourceSupply::Supply(EntityAI *vehicle, UIActionType action, int param, int param2, RString param3)
{
	if (vehicle)
	{
		if (_alloc && _alloc != vehicle) return false;
		// do not allocate - you would unallocate immediatelly
		/*
		{
			_alloc = vehicle;
			LogF
			(
				"%s: Late alloc to %s",
				(const char *)_parent->GetDebugName(),
				(const char *)vehicle->GetDebugName()
			);
		}
		else if (_alloc != vehicle) return false;
		*/
	}
	_supplying = vehicle;
	_action = action;
	_actionParam = param;
	_actionParam2 = param2;
	_actionParam3 = param3;
	return true;
}

struct MagazineInWeapon
{
	Ref<MuzzleType> muzzle;
	Ref<Magazine> magazine;
};
TypeIsMovableZeroed(MagazineInWeapon)

/*!
\patch 1.82 Date 8/28/2002 by Jirka
- Fixed: Unit in cargo space of vehicle can take / drop items even when vehicle moves
*/

void ResourceSupply::Simulate( float deltaT, SimulationImportance prec )
{
	EntityAI *veh = _parent;
	VehicleSupply *parent = dyn_cast<VehicleSupply>(veh);
	DoAssert(parent);

	if (_alloc)
	{
		// we are allocated to some unit
		float limit2 = _alloc->QIsManual() ? Square(30) : Square(100);
		if
		(
			_alloc->Position().Distance2(veh->Position()) > limit2
			// TODO: check if units needs us
		)
		{
			// allocated vehicle is very far - unallocate it
			_alloc = NULL;
			_supplying = NULL;
		}
	}
	if (_supplying)
	{
		if (_supplying->Speed().Distance2(veh->Speed())>Square(2))
		{
			// FIX: Check if unit is not in cargo
			AIUnit *unit = _supplying->CommanderUnit();
			if (unit && unit->GetPerson() == _supplying && unit->GetVehicleIn() == _parent)
			{
				// unit in cargo space of vehicle can take / drop items even when vehicle moves
			}
			else
			{
				_supplying = NULL;
			}
		}
	}
	//Assert(_alloc || !_supplying);
	
	// support unit - perform support if neccessary
	//if (_supplying && _supplying->IsLocal())
	if (_supplying)
	{
		const float thold = 0;
		const float fuelThold = 0.01;
		EntityAI *target = _supplying;
		switch (_action)
		{
		case ATHeal:
			if (veh->GetType()->IsAttendant() && target->CommanderUnit() && target->NeedsAmbulance() > 0)
			{
				// TODO: scan also driver, gunner and cargo soldiers
				EntityAI *person = target->CommanderUnit()->GetPerson();
				person->Repair(1);
				_supplying = NULL; // done
				_alloc = NULL;
				return;
			}
			break;
		case ATRepair:
			if (GetRepairCargo() > 0 && target->NeedsRepair() > 0)
			{
				float dammage = target->GetTotalDammage();
				float total = target->GetType()->GetCost();
				float howMuch = 0.3 * total * dammage;
				if (howMuch <= total * thold) _supplying = NULL; // done
				saturateMin(howMuch, _repairCargo);
				const float maxFlow = total * 0.1;
				saturateMin(howMuch, maxFlow * deltaT);			
				target->Repair(howMuch / total);
				_repairCargo -= howMuch;
				if (target == GWorld->CameraOn()) CutScene("Repair");
				return;
			}
			break;
		case ATRefuel:
			if (GetFuelCargo() > 0 && target->NeedsRefuel() > 0)
			{
				float total = target->GetType()->GetFuelCapacity();
				float howMuch = total - target->GetFuel();
				if (howMuch <= total * fuelThold) _supplying = NULL; // done
				saturateMin(howMuch, _fuelCargo);
				const float maxFlow = total * 0.1;
				saturateMin(howMuch, maxFlow * deltaT);
				target->Refuel(howMuch);
				_fuelCargo -= howMuch;
				if (target == GWorld->CameraOn()) CutScene("Refuel");
				return;
			}
			break;
		case ATRearm:
			{
				Person *man = dyn_cast<Person>(target);
				if (man)
				{
					// men rearm
					AIUnit *unit = man->Brain();
					if (!unit) return;
					// which magazines we needed
					const MuzzleType *muzzle1 = NULL;
					const MuzzleType *muzzle2 = NULL;
					int slots1 = 0, slots2 = 0, slots3 = 0;
					unit->CheckAmmo(muzzle1, muzzle2, slots1, slots2, slots3);
					for (int i=0; i<GetMagazineCargoSize();)
					{
						Ref<const Magazine> magazine = GetMagazineCargo(i);
						if (!magazine || magazine->_ammo == 0)
						{
							i++;
							continue;
						}
						// check if magazine can be used
						const MagazineType *type = magazine->_type;
						int slots = GetItemSlotsCount(type->_magazineType);

						bool add = false;
						if (muzzle1 && muzzle1->CanUse(type))
						{
							if (slots <= slots1)
							{
								slots1 -= slots;
								add = true;
							}
						}
						else if (muzzle2 && muzzle2->CanUse(type))
						{
							if (slots <= slots2)
							{
								slots2 -= slots;
								add = true;
							}
						}
						else if (man->IsMagazineUsable(type))
						{
							if (slots <= slots3)
							{
								slots3 -= slots;
								add = true;
							}
						}
						if (add)
						{
							// add magazine
							AUTO_STATIC_ARRAY(Ref<const Magazine>, conflict, 16);
							if (man->CheckMagazine(magazine, conflict))
							{
								for (int j=0; j<conflict.Size(); j++)
								{
									const Magazine *m = conflict[j];
									man->RemoveMagazine(m);
									AddMagazineCargo(const_cast<Magazine *>(m));
									GetNetworkManager().AddMagazineCargo(parent, m);
								}
								RemoveMagazineCargo(const_cast<Magazine *>(magazine.GetRef()));
								GetNetworkManager().RemoveMagazineCargo(parent, magazine->_creator, magazine->_id);
								DoVerify(man->AddMagazine(const_cast<Magazine *>(magazine.GetRef())) >= 0);
							}
              else i++;
						}
						else i++;
					}
					_supplying = NULL;	// done
					if (target == GWorld->CameraOn()) CutScene("TakeMagazine");
					return;
				}
				else
				{
					// vehicle rearm
					if (GetAmmoCargo() > 0 && target->NeedsRearm() > 0)
					{
						float total = target->GetMaxAmmoCost();
						float howMuch = total - target->GetAmmoCost();
						if (howMuch <= total * thold) _supplying = NULL; // done
						saturateMin(howMuch, _ammoCargo);
						const float maxFlow1 = total * 0.05;
						const float maxFlow2 = 3000;
						saturateMin(howMuch, maxFlow1 * deltaT);
						saturateMin(howMuch, maxFlow2 * deltaT);
						target->Rearm(howMuch);
						_ammoCargo -= howMuch;
						if (target == GWorld->CameraOn()) CutScene("Rearm");
					}
					return;
				}
			}
			break;
		case ATTakeWeapon:
			{
				// find weapon		
				Ref<WeaponType> weapon = WeaponTypes.New(_actionParam3);
				if (!weapon) break;
				AUTO_STATIC_ARRAY(Ref<const WeaponType>, conflict, 16);
				if (FindWeapon(weapon) && target->CheckWeapon(weapon, conflict))
				{
					// 1. find magazines in weapons
					AUTO_STATIC_ARRAY(MagazineInWeapon, miwConflict, 16);
					for (int i=0; i<conflict.Size(); i++)
					{
						const WeaponType *weapon = conflict[i];
						for (int j=0; j<weapon->_muzzles.Size(); j++)
						{
							MuzzleType *muzzle = weapon->_muzzles[j];
							for (int k=0; k<target->NMagazineSlots(); k++)
							{
								const MagazineSlot &slot = target->GetMagazineSlot(k);
								if (slot._muzzle == muzzle)
								{
									if (slot._magazine && slot._magazine->_ammo > 0)
									{
										int index = miwConflict.Add();
										miwConflict[index].muzzle = slot._muzzle;
										miwConflict[index].magazine = slot._magazine;
									}
									break;
								}
							}
						}
					}
					// 2. remove weapons
					for (int i=0; i<conflict.Size(); i++)
					{
						const WeaponType *wt = conflict[i];
						target->RemoveWeapon(wt, false);
						AddWeaponCargo(const_cast<WeaponType *>(wt), 1);
						GetNetworkManager().AddWeaponCargo(parent, wt->GetName());
					}
					// 3. add weapon
					RemoveWeaponCargo(weapon);
					GetNetworkManager().RemoveWeaponCargo(parent, weapon->GetName());
					DoVerify(target->AddWeapon(weapon, false, false, false) >= 0);
					if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == target)
						GWorld->UI()->ResetVehicle(target);
					// 4. remove magazines in weapons
					for (int i=0; i<miwConflict.Size(); i++)
					{
						Ref<Magazine> magazine = miwConflict[i].magazine;
						if (!magazine) continue;
						target->RemoveMagazine(magazine);
						AddMagazineCargo(magazine);
						GetNetworkManager().AddMagazineCargo(parent, magazine);
					}
					// 5. remove unusable magazines
					for (int i=0; i<target->NMagazines();)
					{
						Ref<Magazine> magazine = target->GetMagazine(i);
						if (!magazine || target->IsMagazineUsable(magazine->_type))
						{
							i++;
							continue;
						}
						target->RemoveMagazine(magazine);
						if (magazine->_ammo > 0)
						{
							AddMagazineCargo(magazine);
							GetNetworkManager().AddMagazineCargo(parent, magazine);
						}
					}
					// 6. add usable magazines
					for (int i=0; i<weapon->_muzzles.Size(); i++)
					{
						MuzzleType *muzzle = weapon->_muzzles[i];
						for (int j=0; j<muzzle->_magazines.Size(); j++)
						{
							MagazineType *type = muzzle->_magazines[j];
							for (int k=0; k<_magazineCargo.Size();)
							{
								Ref<Magazine> magazine = _magazineCargo[k];
								if (!magazine || magazine->_type != type)
								{
									k++;
									continue;
								}
								// add magazine
								AUTO_STATIC_ARRAY(Ref<const Magazine>, conflict, 16);
								if (!target->CheckMagazine(magazine, conflict) || conflict.Size() > 0)
									goto EnoughMagazines;
								RemoveMagazineCargo(magazine);
								GetNetworkManager().RemoveMagazineCargo(parent, magazine->_creator, magazine->_id);
								DoVerify(target->AddMagazine(magazine) >= 0);
							}
						}
					}
EnoughMagazines:
					// 7. reload weapon
					for (int i=0; i<weapon->_muzzles.Size(); i++)
					{
						MuzzleType *muzzle = weapon->_muzzles[i];
						int best = target->FindMagazineByType(muzzle);
						if (best >= 0) target->AttachMagazine(muzzle, target->GetMagazine(best));
					}
				}
				_supplying = NULL;	// done

				target->OnWeaponChanged();
				if (target == GWorld->CameraOn()) CutScene("TakeWeapon");
				else
				{
					AIUnit *unit = target->CommanderUnit();
					if (unit && unit->GetVehicleIn() == GWorld->CameraOn()) CutScene("TakeWeapon");
				}

				void UpdateWeaponsInBriefing();
				UpdateWeaponsInBriefing();
				return;
			}
			break;
		case ATTakeMagazine:
			{
				Ref<const Magazine> magazine = FindMagazine(_actionParam3);
				if (!magazine) break;
				AUTO_STATIC_ARRAY(Ref<const Magazine>, conflict, 16);
				if (target->CheckMagazine(magazine, conflict))
				{
					for (int i=0; i<conflict.Size(); i++)
					{
						const Magazine *m = conflict[i];
						target->RemoveMagazine(m);
						if (m->_ammo > 0)
						{
							AddMagazineCargo(const_cast<Magazine *>(m));
							GetNetworkManager().AddMagazineCargo(parent, m);
						}
					}
					RemoveMagazineCargo(const_cast<Magazine *>(magazine.GetRef()));
					GetNetworkManager().RemoveMagazineCargo(parent, magazine->_creator, magazine->_id);
					Verify(target->AddMagazine(const_cast<Magazine *>(magazine.GetRef()),false,true) >= 0);
				}
				_supplying = NULL;	// done
				if (target == GWorld->CameraOn()) CutScene("TakeMagazine");
				else
				{
					AIUnit *unit = target->CommanderUnit();
					if (unit && unit->GetVehicleIn() == GWorld->CameraOn()) CutScene("TakeMagazine");
				}

				void UpdateWeaponsInBriefing();
				UpdateWeaponsInBriefing();

				return;
			}
			break;
		case ATDropWeapon:
			{
				// find weapon		
				Ref<WeaponType> weapon = WeaponTypes.New(_actionParam3);
				if (!weapon || !weapon->_canDrop) break;
	
				if (target->FindWeapon(weapon))
				{
					// remove weapon
					target->RemoveWeapon(weapon);
					if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == target)
						GWorld->UI()->ResetVehicle(target);

					AddWeaponCargo(weapon, 1);
					GetNetworkManager().AddWeaponCargo(parent, weapon->GetName());
					
					// remove unusable magazines
					for (int i=0; i<target->NMagazines();)
					{
						Ref<Magazine> magazine = target->GetMagazine(i);
						if (!magazine || target->IsMagazineUsable(magazine->_type))
						{
							i++;
							continue;
						}
						target->RemoveMagazine(magazine);
						if (magazine->_ammo > 0)
						{
							AddMagazineCargo(magazine);
							GetNetworkManager().AddMagazineCargo(parent, magazine);
						}
					}
				}
				_supplying = NULL;	// done
				if (target == GWorld->CameraOn()) CutScene("DropWeapon");
				else
				{
					AIUnit *unit = target->CommanderUnit();
					if (unit && unit->GetVehicleIn() == GWorld->CameraOn()) CutScene("DropWeapon");
				}
				return;
			}
			break;
		case ATDropMagazine:
			{
				Ref<MagazineType> type = MagazineTypes.New(_actionParam3);
				if (!type) break;

				Ref<const Magazine> magazine;
				int minCount = INT_MAX;
				// find in nonused magazines
				for (int i=0; i<target->NMagazines(); i++)
				{
					const Magazine *m = target->GetMagazine(i);
					if (!m) continue;
					if (m->_type != type) continue;
					if (target->IsMagazineUsed(m)) continue;
					if (m->_ammo < minCount)
					{
						magazine = m;
						minCount = m->_ammo; 
					}
				}
				// find in all magazines
				if (!magazine) for (int i=0; i<target->NMagazines(); i++)
				{
					const Magazine *m = target->GetMagazine(i);
					if (!m) continue;
					if (m->_type != type) continue;
					if (m->_ammo < minCount)
					{
						magazine = m;
						minCount = m->_ammo; 
					}
				}

				if (magazine)
				{
					target->RemoveMagazine(magazine);
					if (minCount > 0)
					{
						AddMagazineCargo(const_cast<Magazine *>(magazine.GetRef()));
						GetNetworkManager().AddMagazineCargo(parent, magazine);
					}
				}
						
				_supplying = NULL;	// done
				if (target == GWorld->CameraOn()) CutScene("DropMagazine");
				else
				{
					AIUnit *unit = target->CommanderUnit();
					if (unit && unit->GetVehicleIn() == GWorld->CameraOn()) CutScene("DropMagazine");
				}
				return;
			}
			break;
		default:
			Fail("Unsupported action");
			break;
		}
		_supplying = NULL;
	}
	CutScene(NULL);
}

/*!
\patch 1.21 Date 8/24/2001 by Jirka
- Fixed: Allow Repair, Refuel, Rearm, Heal, TakeMagazine and TakeWeapon actions in MP only to vehicle owner (driver)
\patch 1.75 Date 2/5/2002 by Jirka
- Added: Player can drop actual weapon and magazine
\patch 1.75 Date 2/13/2002 by Jirka
- Added: Player can take weapons and magazines from vehicle if sitting in cargo 
*/

void ResourceSupply::GetActions(UIActions &actions, AIUnit *unit, bool now)
{
	if (!unit) return;
	
	EntityAI *veh = unit->GetVehicle();
	if (now && !veh->IsLocal()) return;

	if (_alloc && _alloc != veh) return;	// wait

#if _ENABLE_DATADISC
	bool inCargo = unit->GetVehicleIn() == _parent && unit->IsInCargo();
#else
	bool inCargo = false;
#endif
	bool check = Check(veh, NULL, 0, now); // check distance

	if (check)
	{
		if (GetRepairCargo() > 0)
		{
			if (Check(veh, &EntityAI::NeedsRepair, 0, now))
			{
				actions.Add(ATRepair, _parent, 0.5, 0, true);
			}
		}
		if (GetFuelCargo() > 0)
		{
			if (Check(veh, &EntityAI::NeedsRefuel, 0, now))
			{
				actions.Add(ATRefuel, _parent, 0.5, 0, true);
			}
		}
		if (GetAmmoCargo() > 0)
		{
			if (Check(veh, &EntityAI::NeedsRearm, 0, now))
			{
				actions.Add(ATRearm, _parent, 0.5, 0, true);
			}
		}
		if (_parent->GetType()->IsAttendant())
		{
			if (Check(veh, &EntityAI::NeedsAmbulance, 0, now))
			{
				actions.Add(ATHeal, _parent, 0.5, 0, true);
			}
		}
	}

	if (!check && !inCargo) return;

	bool soldier = unit->IsFreeSoldier();
	if (inCargo || (soldier && !unit->GetPerson()->IsActionInProgress(MFReload)))
	{
		veh = unit->GetPerson();

		AUTO_STATIC_ARRAY(bool, checked, 32);
		// weapons
		int n = _weaponCargo.Size();
		checked.Resize(n); for (int i=0; i<n; i++) checked[i] = false;
		for (int i=0; i<n; i++)
		{
			if (checked[i]) continue;
			checked[i] = true;
			const WeaponType *weapon = _weaponCargo[i];
			Assert(weapon->_scope == 2);
			AUTO_STATIC_ARRAY(Ref<const WeaponType>, conflict, 16);
			if (!veh->CheckWeapon(weapon, conflict)) continue;
			actions.Add(ATTakeWeapon, _parent, 0.52, 0, true, false, 0, weapon->GetName());
			for (int j=i+1; j<n; j++)
				if (_weaponCargo[j] == weapon) checked[j] = true;
		}
		// magazines
		n = _magazineCargo.Size();
		checked.Resize(n); for (int i=0; i<n; i++) checked[i] = false;
		for (int i=0; i<n; i++)
		{
			if (checked[i]) continue;
			checked[i] = true;
			const Magazine *magazine = _magazineCargo[i];
			Assert(magazine->_type->_scope == 2);
			if (magazine->_ammo == 0) continue;
			if (!veh->IsMagazineUsable(magazine->_type)) continue;
			for (int j=i+1; j<n; j++)
			{
				const Magazine *magazineJ = _magazineCargo[j];
				if (magazineJ->_type == magazine->_type)
				{
					checked[j] = true;
					if (magazineJ->_ammo > magazine->_ammo) magazine = magazineJ;
				}
			}
			AUTO_STATIC_ARRAY(Ref<const Magazine>, conflict, 16);
			if (!veh->CheckMagazine(magazine, conflict)) continue;
//			actions.Add(ATTakeMagazine, _parent, 0.53, magazine->_creator, true, false, magazine->_id);
			actions.Add(ATTakeMagazine, _parent, 0.53, 0, true, false, 0, magazine->_type->GetName());
		}
#if _ENABLE_DATADISC
		// if (!inCargo && !(_parent->CommanderUnit() && _parent->CommanderUnit()->GetPerson() == _parent))
		{
			int index = veh->SelectedWeapon();
			if (index >= 0 && !_parent->GetType()->_showWeaponCargo)
			{
				const MagazineSlot &slot = veh->GetMagazineSlot(index);
				if (GetFreeWeaponCargo() > 0 && slot._weapon && slot._weapon->_canDrop)
					actions.Add(ATDropWeapon, _parent, -0.01, 0, true, false, 0, slot._weapon->GetName());
				if (GetFreeMagazineCargo() > 0 && slot._magazine)
				{
					const MagazineType *type = slot._magazine->_type;
					for (int i=0; i<veh->NMagazines(); i++)
					{
						const Magazine *magazine = veh->GetMagazine(i);
						if (!magazine) continue;
						if (magazine->_ammo == 0) continue;
						if (magazine->_type != type) continue;
						// if (veh->IsMagazineUsed(magazine)) continue;
						actions.Add(ATDropMagazine, _parent, -0.02, 0, true, false, 0, type->GetName());
						break;
					}
				}
			}
		}
#endif
	}
}

bool ResourceSupply::FindWeapon(const WeaponType *weapon) const
{
	for (int i=0; i<_weaponCargo.Size(); i++)
		if (_weaponCargo[i] == weapon) return true;
	return false;
}

bool ResourceSupply::FindMagazine(const Magazine *magazine) const
{
	for (int i=0; i<_magazineCargo.Size(); i++)
		if (_magazineCargo[i] == magazine) return true;
	return false;
}

const Magazine *ResourceSupply::FindMagazine(RString name) const
{
	Ref<MagazineType> type = MagazineTypes.New(name);
	const Magazine *magazine = NULL;
	int ammo = 0;
	for (int i=0; i<_magazineCargo.Size(); i++)
	{
		Magazine *m = _magazineCargo[i];
		if (!m) continue;
		if (m->_type != type) continue;
		if (m->_ammo > ammo)
		{
			magazine = m;
			ammo = m->_ammo;
		}
	}
	return magazine;
}

const Magazine *ResourceSupply::FindMagazine(int creator, int id) const
{
	for (int i=0; i<_magazineCargo.Size(); i++)
	{
		const Magazine *magazine = _magazineCargo[i];
		if (!magazine) continue;
		if (magazine->_creator == creator && magazine->_id == id) return magazine;
	}
	return NULL;
}

extern SRef<EntityAI> GDummyVehicle;

static bool WeaponCanBeDropped(Vector3Par p)
{
	//LogF("WeaponCanBeDropped test at %.1f,%.1f",p.X(),p.Z());
	// check if we are inside some object
	int lx = (int)(p.X()*InvLandGrid), lz = (int)(p.Z()*InvLandGrid);
	for (int lxx=lx-1; lxx<=lx+1; lxx++) for (int lzz=lz-1; lzz<=lz+1; lzz++)
	{
		if (!InRange(lxx,lzz)) continue;
		const ObjectList &list = GLandscape->GetObjects(lzz,lxx);
		for (int o=0; o<list.Size(); o++)
		{
			const Object *oo = list[o];
			if (oo->GetType()!=Primary && oo->GetType()!=TypeVehicle) continue;
			if (!oo->GetShape()) continue;
			// check if we are inside
			float colSize = oo->CollisionSize();
			saturateMax(colSize,1);
			float distXZ2 = p.DistanceXZ2(oo->Position());
			if (distXZ2>=Square(colSize)) continue;
			return false;
		}
	}
	//LogF("  WeaponCanBeDropped passed");
	return true;
}


const int WeaponCountInOperGrid = 3;

static bool OperGridWeaponCanBeDropped(Vector3Par pos, void *context)
{
	//LogF("OperGridWeaponCanBeDropped test at %.1f,%.1f",pos.X(),pos.Z());
	// one square is big enough to contain several weapons (3x3 m)
	const int weaponMax = (WeaponCountInOperGrid-1)/2;
	const float weaponDropGrid = OperItemGrid*(1.0f/WeaponCountInOperGrid);
	for (int x=-weaponMax; x<=weaponMax; x++)
	for (int z=-weaponMax; z<=weaponMax; z++)
	{
		Vector3 p = pos+Vector3(x*weaponDropGrid,0,z*weaponDropGrid);
		if (WeaponCanBeDropped(p))
		{
			//LogF("  OperGridWeaponCanBeDropped passed");
			return true;
		}
	}
	return false;
}

static bool FindDropPos(Matrix4 &transform)
{
	Vector3 pos = transform.Position(), normal = VUp;

	//LogF("FindFreePosition drop pos %.1f,%.1f",pos.X(),pos.Z());
	if
	(
		!GDummyVehicle || !AIUnit::FindFreePosition
		(
			pos, normal, true, GDummyVehicle, OperGridWeaponCanBeDropped
		)
	)
	{
		// no free position - we will rather destroy the container
		// that place it somewhere where it cannot be reached
		return false;
	}
	//LogF("Find drop pos %.1f,%.1f",pos.X(),pos.Z());
	// we have to find some position where is no weapon lying yet
	// search max. 50 m from this position

	if (WeaponCanBeDropped(pos))
	{
		transform.SetPosition(pos);
		Matrix3 dir(MRotationY,H_PI*2*GRandGen.RandomValue());
		transform.SetUpAndDirection(normal,dir.Direction());
		return true;
	}
	// one square is big enough to contain several weapons (3x3 m)
	const int weaponMax = (WeaponCountInOperGrid-1)/2;
	const float weaponDropGrid = OperItemGrid*(1.0f/WeaponCountInOperGrid);
	for (int x=-weaponMax; x<=weaponMax; x++)
	for (int z=-weaponMax; z<=weaponMax; z++)
	{
		Vector3 p = pos+Vector3(x*weaponDropGrid,0,z*weaponDropGrid);
		if (WeaponCanBeDropped(p))
		{
			transform.SetPosition(p);
			Matrix3 dir(MRotationY,H_PI*2*GRandGen.RandomValue());
			transform.SetUpAndDirection(normal,dir.Direction());
			return true;
		}
		
	}
	return false;
}

static bool DropWeapon(WeaponType *weapon, const Matrix4 &trans)
{
	// create container
	Ref<EntityAI> veh = NewVehicle("WeaponHolder");
	int slots = weapon->_weaponType;
	if ((slots & MaskSlotSecondary) != 0 && (slots & MaskSlotPrimary) == 0)
		veh = NewVehicle("SecondaryWeaponHolder");
	else
		veh = NewVehicle("WeaponHolder");
	Ref<VehicleSupply> container = dyn_cast<VehicleSupply,EntityAI>(veh);
	if (!container) return false;

	Matrix4 transform = trans;
	if (!FindDropPos(transform)) return false;
	container->PlaceOnSurface(transform);
	container->SetTransform(transform);
	container->Init(transform);

	// add weapon to container
	int free = container->GetFreeWeaponCargo();
	if (free>0)
	{
		GWorld->AddBuilding(container);

		if (GWorld->GetMode() == GModeNetware)
			GetNetworkManager().CreateVehicle(container, VLTBuilding, "", -1);

		container->AddWeaponCargo(weapon, 1);
		if (GWorld->GetMode() == GModeNetware)
			GetNetworkManager().AddWeaponCargo(container, weapon->GetName());

		return true;
	}
	else
	{
		ErrF("WeaponHolder has no space for weapon");
		return false;
	}
}

static bool DropMagazine(Magazine *magazine, Matrix4 &transform)
{
	// create container
	Ref<EntityAI> veh = NewVehicle("WeaponHolder");
	Ref<VehicleSupply> container = dyn_cast<VehicleSupply,EntityAI>(veh);
	if (!container) return false;
	
	if (!FindDropPos(transform)) return false;
	container->PlaceOnSurface(transform);
	container->SetTransform(transform);
	container->Init(transform);

	// add weapon to container
	int free = container->GetFreeMagazineCargo();
	if (free>0)
	{
		GWorld->AddBuilding(container);
		if (GWorld->GetMode() == GModeNetware)
			GetNetworkManager().CreateVehicle(container, VLTBuilding, "", -1);

		container->AddMagazineCargo(magazine);
		if (GWorld->GetMode() == GModeNetware)
			GetNetworkManager().AddMagazineCargo(container, magazine);

		return true;
	}
	else
	{
		ErrF("WeaponHolder has no space for magazine");
		return false;
	}
}

int ResourceSupply::AddWeaponCargo(WeaponType *weapon, int count, bool deleteWhenFull)
{
	int free = GetFreeWeaponCargo();
	if (count > free && !deleteWhenFull)
	{
		Vector3 pos = _parent->PositionModelToWorld
		(
			_parent->GetType()->GetSupplyPoint()
		);
		Matrix3 dir = _parent->Orientation();
		Matrix4 transform;
		transform.SetPosition(pos);
		transform.SetOrientation(dir);
		while (count > free && count>0)
		{
			DropWeapon(weapon, transform);
			count--;
		}
	}

	int index = -1;
	for (int j=0; j<count; j++)
	{
		if (_parent->GetType()->_showWeaponCargo) weapon->ShapeAddRef();
		index = _weaponCargo.Add(weapon);
	}
	return index;
}

bool ResourceSupply::RemoveWeaponCargo(WeaponType *weapon)
{
	for (int i=0; i<_weaponCargo.Size(); i++)
	{
		if (_weaponCargo[i] == weapon)
		{
			if (_parent->GetType()->_showWeaponCargo) weapon->ShapeRelease();
			_weaponCargo.Delete(i);
			if (_parent->GetType()->_forceSupply && _weaponCargo.Size() == 0 && _magazineCargo.Size() == 0)
				_parent->SetDelete();
			return true;
		}
	}
	return false;
}

void ResourceSupply::ClearWeaponCargo()
{
	if (_parent->GetType()->_showWeaponCargo)
	{
		for (int i=0; i<_weaponCargo.Size(); i++)
			if (_weaponCargo[i]) _weaponCargo[i]->ShapeRelease();
	}
	_weaponCargo.Clear();
	if (_parent->GetType()->_forceSupply && _magazineCargo.Size() == 0)
		_parent->SetDelete();
}

int ResourceSupply::AddMagazineCargo(Magazine *magazine, bool deleteWhenFull)
{
	int free = GetFreeMagazineCargo();
	if (free < 1 && !deleteWhenFull)
	{
		Vector3 pos = _parent->PositionModelToWorld
		(
			_parent->GetType()->GetSupplyPoint()
		);
		Matrix3 dir = _parent->Orientation();
		Matrix4 transform;
		transform.SetPosition(pos);
		transform.SetOrientation(dir);
		DropMagazine(magazine, transform);
		return -1;
	}

	if (_parent->GetType()->_showWeaponCargo) magazine->_type->MagazineShapeAddRef();
	return _magazineCargo.Add(magazine);
}

int ResourceSupply::AddMagazineCargo(MagazineType *type, int count, bool deleteWhenFull)
{
	int free = GetFreeMagazineCargo();
	if (count > free && !deleteWhenFull)
	{
		Vector3 pos = _parent->PositionModelToWorld
		(
			_parent->GetType()->GetSupplyPoint()
		);
		Matrix3 dir = _parent->Orientation();
		Matrix4 transform;
		transform.SetPosition(pos);
		transform.SetOrientation(dir);
		while (count > free && count>0)
		{
			Ref<Magazine> magazine = new Magazine(type);
			magazine->_ammo = type->_maxAmmo;
			magazine->_reload = 0;
			magazine->_reloadMagazine = 0;
			DropMagazine(magazine, transform);
			count--;
		}
	}

	int index = -1;
	for (int j=0; j<count; j++)
	{
		Ref<Magazine> magazine = new Magazine(type);
		magazine->_ammo = type->_maxAmmo;
		magazine->_reload = 0;
		magazine->_reloadMagazine = 0;
		if (_parent->GetType()->_showWeaponCargo) magazine->_type->MagazineShapeAddRef();
		index = _magazineCargo.Add(magazine);
	}
	return index;
}

bool ResourceSupply::RemoveMagazineCargo(Magazine *magazine)
{
	for (int i=0; i<_magazineCargo.Size(); i++)
	{
		if (_magazineCargo[i] == magazine)
		{
			if (_parent->GetType()->_showWeaponCargo) magazine->_type->MagazineShapeRelease();
			_magazineCargo.Delete(i);
			if (_parent->GetType()->_forceSupply && _weaponCargo.Size() == 0 && _magazineCargo.Size() == 0)
				_parent->SetDelete();
			return true;
		}
	}
	return false;
}

void ResourceSupply::ClearMagazineCargo()
{
	if (_parent->GetType()->_showWeaponCargo)
	{
		for (int i=0; i<_magazineCargo.Size(); i++)
			if (_magazineCargo[i]) _magazineCargo[i]->_type->MagazineShapeRelease();
	}

	_magazineCargo.Clear();
	if (_parent->GetType()->_forceSupply && _weaponCargo.Size() == 0)
		_parent->SetDelete();
}

DEFINE_CASTING(VehicleSupply)

VehicleSupply::VehicleSupply(EntityAIType *name, bool fullCreate)
:base(name,fullCreate)
{
	if( IsSupply(this) )
	{
		_supply=new ResourceSupply(this);
	}
}	

void VehicleSupply::SupplyStarted( AIUnit *unit )
{
	LogF
	(
		"%s SupplyStarted for %s",
		(const char *)GetDebugName(),(const char *)unit->GetDebugName()
	);

	Assert( _supplyUnits.Find(unit)<0 );
	_supplyUnits.AddUnique(unit);
}

void VehicleSupply::SupplyFinished( AIUnit *unit )
{
	// done/canceled
	LogF
	(
		"%s SupplyFinished for %s",
		(const char *)GetDebugName(),(const char *)unit->GetDebugName()
	);
	if (GetAllocSupply()==unit->GetVehicle())
	{
		LogF("  patch unalloc");
		SetAllocSupply(NULL);
	}
	
	for (int i=0; i<_supplyUnits.Size(); i++)
	{
		if( unit == _supplyUnits[i] )
		{
			_supplyUnits.Delete(i);
			break;
		}
	}

	if (_supplyUnits.Size()==0)
	{
		if (GetAllocSupply())
		{
			LogF("  %s: hard patch unalloc",(const char *)GetDebugName());
		}
	}
	UpdateStop();
}

void VehicleSupply::WaitForSupply(AIUnit *unit)
{
	// add to queue only when commander is alive
	AIUnit *commander = CommanderUnit();
	if ( !commander || commander->GetLifeState()!=AIUnit::LSAlive) return;
	SupplyStarted(unit);	
}

bool VehicleSupply::CanCancelStop() const
{
/*
	if (_supplyUnits.Count()<=0)
	{
		_supplyUnits.Clear();
		return true;
	}
*/
	return true;
}

bool VehicleSupply::FindWeapon(const WeaponType *weapon) const
{
	if (_supply && _supply->FindWeapon(weapon)) return true;
	return base::FindWeapon(weapon);
}

bool VehicleSupply::FindMagazine(const Magazine *magazine) const
{
	if (_supply && _supply->FindMagazine(magazine)) return true;
	return base::FindMagazine(magazine);
}

const Magazine *VehicleSupply::FindMagazine(RString name) const
{
	if (_supply)
	{
		const Magazine *magazine = _supply->FindMagazine(name);
		if (magazine) return magazine;
	}
	return base::FindMagazine(name);
}

const Magazine *VehicleSupply::FindMagazine(int creator, int id) const
{
	if (_supply)
	{
		const Magazine *magazine = _supply->FindMagazine(creator, id);
		if (magazine) return magazine;
	}
	return base::FindMagazine(creator, id);
}

LSError VehicleSupply::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	CHECK(ar.Serialize("Supply", _supply, 1))
	if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassSecond)
	{
		if (_supply)
			_supply->SetParent(this);
	}
	CHECK(ar.SerializeRefs("SupplyUnits", _supplyUnits, 1))
	return LSOK;
}

NetworkMessageType VehicleSupply::GetNMType(NetworkMessageClass cls) const
{
	switch (cls)
	{
	case NMCUpdateGeneric:
		return NMTUpdateVehicleSupply;
	default:
		return base::GetNMType(cls);
	}
}

IndicesUpdateVehicleSupply::IndicesUpdateVehicleSupply()
{
}

void IndicesUpdateVehicleSupply::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);

	supply.Scan(format);
}

NetworkMessageIndices *GetIndicesUpdateVehicleSupply() {return new IndicesUpdateVehicleSupply();}

NetworkMessageFormat &VehicleSupply::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	switch (cls)
	{
	case NMCUpdateGeneric:
		base::CreateFormat(cls, format);
		ResourceSupply::CreateFormat(format);
		break;
	default:
		base::CreateFormat(cls, format);
		break;
	}
	return format;
}

TMError VehicleSupply::TransferMsg(NetworkMessageContext &ctx)
{
	switch (ctx.GetClass())
	{
	case NMCUpdateGeneric:
		TMCHECK(base::TransferMsg(ctx))
		if (_supply)
		{
			Assert(dynamic_cast<const IndicesUpdateVehicleSupply *>(ctx.GetIndices()))
			const IndicesUpdateVehicleSupply *indices = static_cast<const IndicesUpdateVehicleSupply *>(ctx.GetIndices());

			TMCHECK(_supply->TransferMsg(ctx, &indices->supply))
		}
		break;
	default:
		return base::TransferMsg(ctx);
	}
	return TMOK;
}

float VehicleSupply::CalculateError(NetworkMessageContext &ctx)
{
	float error = 0;
	switch (ctx.GetClass())
	{
	case NMCUpdateGeneric:
		error += base::CalculateError(ctx);
		if (_supply)
		{
			Assert(dynamic_cast<const IndicesUpdateVehicleSupply *>(ctx.GetIndices()))
			const IndicesUpdateVehicleSupply *indices = static_cast<const IndicesUpdateVehicleSupply *>(ctx.GetIndices());
			error += _supply->CalculateError(ctx, &indices->supply);
		}
		break;
	default:
		error += base::CalculateError(ctx);
		break;
	}

	return error;
}

void VehicleSupply::ResetStatus()
{
	if( IsSupply(this) )
	{
		_supply=new ResourceSupply(this);
	}

	base::ResetStatus();
}


void VehicleSupply::SetAllocSupply( EntityAI *vehicle )
{
	if( _supply )
	{
		if (_supply->GetAlloc()!=vehicle)
		{
			LogF
			(
				"%s Allocated for %s",
				(const char *)GetDebugName(),
				vehicle ? (const char *)vehicle->GetDebugName() : "NULL"
			);
			_supply->SetAlloc(vehicle);
		}
	}
}

EntityAI *VehicleSupply::GetAllocSupply() const
{
	return _supply ? _supply->GetAlloc() : NULL;
}

bool VehicleSupply::Supply(EntityAI *vehicle, UIActionType action, int param, int param2, RString param3)
{
	return _supply? _supply->Supply(vehicle, action, param, param2, param3) : false;
}

EntityAI *VehicleSupply::GetSupplying() const
{
	return _supply ? _supply->GetSupplying() : NULL;
}

float VehicleSupply::GetExplosives() const
{
	float expl=GetType()->_secondaryExplosion;
	if( expl>=0 ) return expl;
	// how much explosives is in
	// how much explosives is in
	// convert fuel and ammo to common value
	const float AmmoCostToExplosion=0.05;
	//const float IAmmoCostToExplosion=0.01;
	const float FuelCostToExplosion=0.10;
	// estimate explosiveness from cost
	float explosion = base::GetExplosives(); // ammunition
	// calculate total fuel load
	float fuelExplosion = (GetFuel()+GetFuelCargo())*FuelCostToExplosion;
	// calculate total ammo load
	float cargoExplosion = GetAmmoCargo()*AmmoCostToExplosion;
	//LogF("Ammo explosion %.1f",explosion);
	//LogF("Fuel explosion %.1f",fuelExplosion);
	//LogF("Ammo cargo explosion %.1f",cargoExplosion);
	explosion += fuelExplosion;
	explosion += cargoExplosion;
	// multiply result by config factor
	return explosion*fabs(expl);
}

void VehicleSupply::Simulate( float deltaT, SimulationImportance prec )
{
	//if (_supply && !IsDammageDestroyed() && IsLocal())
	if (_supply && !IsDammageDestroyed())
	{
		_supply->Simulate(deltaT,SimulateVisibleFar);
	}
	base::Simulate(deltaT,prec);
}

RString VehicleSupply::GetActionName(const UIAction &action)
{
	return base::GetActionName(action);
}

void VehicleSupply::PerformAction(const UIAction &action, AIUnit *unit)
{
	EntityAI *veh = unit->GetVehicle();
	switch(action.type)
	{
		case ATTakeWeapon:
		case ATTakeMagazine:
		case ATDropWeapon:
		case ATDropMagazine:
			veh = unit->GetPerson();
			// continue
		case ATHeal:
		case ATRepair:
		case ATRefuel:
		case ATRearm:
			if (!unit->GetVehicle()->IsActionInProgress(MFReload))
			{
				Supply
				(
					veh, action.type, action.param, action.param2, action.param3
				);
			}
			break;
		default:
			base::PerformAction(action,unit);
			break;
	}
}

void VehicleSupply::GetActions(UIActions &actions, AIUnit *unit, bool now)
{
	base::GetActions(actions, unit, now);
	if (unit && _supply && !IsDammageDestroyed())
	{
		AIGroup *grp = unit->GetGroup();
		AICenter *center = grp ? grp->GetCenter() : NULL;
		if (center && center->IsEnemy(GetTargetSide())) return;
		_supply->GetActions(actions, unit, now);
	}
}

TransportType::TransportType( const ParamEntry *param )
:base(param)
{
}

//! convert action name from name to numeric id

static ManVehAction GetActionVehName(const RStringB &name)
{
	int id = GActionVehNames.GetValue(name);
	if (id>=0) return ManVehAction(id);
	RptF("%s not found in GActionVehNames",(const char *)name);
	return ManVehActNone;
}

void TransportType::Load(const ParamEntry &par)
{
	base::Load(par);

	#define GET_PAR(x) _##x=par>>#x

	GET_PAR(getInRadius);

	GET_PAR(hasDriver);
	GET_PAR(hasGunner);
	GET_PAR(hasCommander);
	GET_PAR(driverIsCommander);

	_crew = par >> "crew";
	_getInAction = ManAction(int(par >> "getInAction"));
	_getOutAction = ManAction(int(par >> "getOutAction"));
	_driverAction = GetActionVehName(par >> "driverAction");
	_gunnerAction = GetActionVehName(par >> "gunnerAction");
	_commanderAction = GetActionVehName(par >> "commanderAction");
	_driverInAction = GetActionVehName(par >> "driverInAction");
	_gunnerInAction = GetActionVehName(par >> "gunnerInAction");
	_commanderInAction = GetActionVehName(par >> "commanderInAction");

	_maxManCargo=par>>"transportSoldier";
	//_maxVehicleCargo=par>>"transportVehiclesCount";
	//_maxVehicleCargoMass=par>>"transportVehiclesMass";
	_typicalCargo.Resize(0);
	const ParamEntry &typCargo=par>>"typicalCargo";
	for (int i=0; i<typCargo.GetSize(); i++)
	{
		RString name=typCargo[i];
		const VehicleNonAIType *vType=VehicleTypes.New(name);
		const VehicleType *type=dynamic_cast<const VehicleType *>(vType);
		if( !type ) continue;
		_typicalCargo.Add(const_cast<VehicleType *>(type));
	}
	_typicalCargo.Compact();

	const ParamEntry &cargo = par >> "cargoAction";
	_cargoAction.Realloc(cargo.GetSize());
	_cargoAction.Resize(cargo.GetSize());
	for (int i=0; i<cargo.GetSize(); i++)
	{
		_cargoAction[i] = GetActionVehName(cargo[i]);
	}
	const ParamEntry &cargoCoDriver = par >> "cargoIsCoDriver";
	_cargoCoDriver.Realloc(cargoCoDriver.GetSize());
	_cargoCoDriver.Resize(cargoCoDriver.GetSize());
	for (int i=0; i<cargoCoDriver.GetSize(); i++)
	{
		_cargoCoDriver[i] = cargoCoDriver[i];
	}

	GET_PAR(hideProxyInCombat);

	GET_PAR(forceHideGunner);
	GET_PAR(forceHideDriver);
	GET_PAR(forceHideCommander);

	GET_PAR(outGunnerMayFire);
	GET_PAR(viewGunnerInExternal);
	GET_PAR(unloadInCombat);

	GET_PAR(insideSoundCoef);

	_driverOpticsColor = GetPackedColor(par >> "driverOpticsColor");
	_gunnerOpticsColor = GetPackedColor(par >> "gunnerOpticsColor");
	_commanderOpticsColor = GetPackedColor(par >> "commanderOpticsColor");

	_viewCommander.Load(par>>"ViewCommander");
	_viewGunner.Load(par>>"ViewGunner");
	_viewCargo.Load(par>>"ViewCargo");
	_viewOptics.Load(par>>"ViewOptics");

	GET_PAR(gunnerUsesPilotView);
	GET_PAR(commanderUsesPilotView);

	GET_PAR(castDriverShadow);
	GET_PAR(castGunnerShadow);
	GET_PAR(castCommanderShadow);
	GET_PAR(castCargoShadow);

	GET_PAR(ejectDeadDriver);
	GET_PAR(ejectDeadGunner);
	GET_PAR(ejectDeadCommander);
	GET_PAR(ejectDeadCargo);

	GET_PAR(hideWeaponsDriver);
	GET_PAR(hideWeaponsGunner);
	GET_PAR(hideWeaponsCommander);
	GET_PAR(hideWeaponsCargo);
	
	#undef GET_PAR
}

void TransportType::AddProxy
(
	ManProxy &mProxy, const ProxyObject &proxy, ManAnimationType anim
)
{
	//LogF("%s: add proxy %s",(const char *)GetName(),(const char *)anim);
	Matrix4 &trans=mProxy.transform;
	trans = proxy.obj->Transform();
	LODShapeWithShadow *pshape = proxy.obj->GetShape();
	// reset position BC correction
	trans.SetPosition(trans.FastTransform(-pshape->BoundingCenter()));
	// rotate around y-axis (shape is reversed)
	trans.SetOrientation(trans.Orientation() * Matrix3(MScale,-1,1,-1));
	mProxy.selection = proxy.selection;
}

Threat TransportType::GetStrategicThreat( float distance2, float visibility, float cosAngle ) const
{
	Threat threat = base::GetStrategicThreat(distance2,visibility,cosAngle);
	// cargo not known, assume typical cargo
	for( int i=0; i<_typicalCargo.Size(); i++ )
	{
		const VehicleType *type=_typicalCargo[i];
		Assert( this!=type );
		threat+=type->GetStrategicThreat(distance2,visibility,-1);
	}
	return threat;
}

Threat TransportType::GetDammagePerMinute
(
	float distance2, float visibility, EntityAI *vehicle
) const
{
	Threat threat = base::GetDammagePerMinute(distance2,visibility,vehicle);

	// cargo not known, assume typical cargo
	if( vehicle && _maxManCargo>0 )
	{
		// if we know cargo, calculate exact
		Assert( dyn_cast<const Transport>(vehicle) );
		Assert(vehicle->GetType()==this);
		const Transport *transp=static_cast<const Transport *>(vehicle);
		const ManCargo &cargo=transp->GetManCargo();
		for( int i=0; i<cargo.Size(); i++ )
		{
			Person *man=cargo[i];
			if (man) threat+=man->GetType()->GetDammagePerMinute(distance2,visibility,man);
		}
	}
	else
	{
		// cargo not known, assume typical cargo
		for( int i=0; i<_typicalCargo.Size(); i++ )
		{
			const VehicleType *type=_typicalCargo[i];
			Assert( type!=this );
			threat+=type->GetDammagePerMinute(distance2,visibility);
		}
	}

	return threat;
}

//#include "moveIncl.hpp"
#include <Es/Common/filenames.hpp>

static bool NameInList( const ParamEntry &cfg, const char *name )
{
	for (int i=0; i<cfg.GetSize(); i++)
	{
		RStringB val = cfg[i];
		if (!strcmpi(val,name)) return true;
	}
	return false;
}

void TransportType::InitShape()
{
	base::InitShape();

	for( int level=0; level<_shape->NLevels(); level++ )
	{
		LevelProxies &proxies=_proxies[level];
		Shape *shape=_shape->LevelOpaque(level);
		// clear proxy
		proxies._driverProxy = ManProxy();
		proxies._gunnerProxy = ManProxy();
		proxies._commanderProxy = ManProxy();
		proxies._cargoProxy.Resize(0);
		
		// convert shape proxies to my proxies
		for( int i=0; i<shape->NProxies(); i++ )
		{
			const ProxyObject &proxy=shape->Proxy(i);
			// first check for ProxyCrew
			ProxyCrew *proxyCrew = dyn_cast<ProxyCrew,Object>(proxy.obj);
			if (proxyCrew)
			{
				// check crew type
				CrewPosition pos = proxyCrew->Type()->GetCrewPosition();
				if (pos==CPDriver)
				{
					AddProxy(proxies._driverProxy,proxy,"Driver");
				}
				else if (pos==CPGunner)
				{
					AddProxy(proxies._gunnerProxy,proxy,"Gunner");
				}
				else if (pos==CPCommander)
				{
					AddProxy(proxies._commanderProxy,proxy,"Commander");
				}
				else if (pos==CPCargo)
				{
					int id = proxy.id;
					if (id < 1)
					{
						Fail("Bad cargo proxy index");
						continue;
					}
					if (proxies._cargoProxy.Size() < id) proxies._cargoProxy.Resize(id);
					ManProxy &mProxy=proxies._cargoProxy[id - 1];
					AddProxy(mProxy,proxy,"Cargo");
				}
				continue;
			}

			// try to get vehicle class name
			char shortname[256];
			Vehicle *proxyVeh = dyn_cast<Vehicle,Object>(proxy.obj);
			if (proxyVeh)
			{
				//LogF("%s: veh proxy %s",(const char *)GetName(),shortname);
				strcpy(shortname,proxyVeh->GetName());
			}
			else
			{
				//LogF("%s: obj proxy %s",(const char *)GetName(),shortname);
				if (proxy.obj->GetShape())
				{
					GetFilename(shortname,proxy.obj->GetShape()->Name());
				}
				else
				{
					*shortname=0;
				}
			}
			const ParamEntry &cfg = Pars>>"CfgCrew";
			const ParamEntry &driverList = cfg>>"drivers";
			const ParamEntry &gunnerList = cfg>>"gunners";
			const ParamEntry &commanderList = cfg>>"commanders";
			const ParamEntry &cargoList = cfg>>"cargo";

			if (NameInList(driverList,shortname))
			{
				AddProxy(proxies._driverProxy,proxy,"Driver");
				RptF("%s: old crew proxy %s",(const char *)GetName(),shortname);
			}
			else if (NameInList(gunnerList,shortname))
			{
				AddProxy(proxies._gunnerProxy,proxy,"Gunner");
				RptF("%s: old crew proxy %s",(const char *)GetName(),shortname);
			}
			else if (NameInList(commanderList,shortname))
			{
				AddProxy(proxies._commanderProxy,proxy,"Commander");
				RptF("%s: old crew proxy %s",(const char *)GetName(),shortname);
			}
			else if (NameInList(cargoList,shortname))
			{
				int id = proxy.id;
				if (id < 1)
				{
					Fail("Bad cargo proxy index");
					continue;
				}
				if (proxies._cargoProxy.Size() < id) proxies._cargoProxy.Resize(id);
				ManProxy &mProxy=proxies._cargoProxy[id - 1];
				AddProxy(mProxy,proxy,"Cargo");
				RptF("%s: old crew proxy %s",(const char *)GetName(),shortname);
			}
		}
	}

	Shape *memory = _shape->MemoryLevel();
	if (memory)
	{
		/*
		_driverOpticsPos = memory->PointIndex("driverview");
		if (_driverOpticsPos < 0) _driverOpticsPos = memory->PointIndex("pilot");
		_gunnerOpticsPos = memory->PointIndex("gunnerview");
		_commanderOpticsPos = memory->PointIndex("commanderview");
		*/
		_driverOpticsPos = memory->FindNamedSel("driverview");
		if (_driverOpticsPos < 0) _driverOpticsPos = memory->FindNamedSel("pilot");
		_gunnerOpticsPos = memory->FindNamedSel("gunnerview");
		_commanderOpticsPos = memory->FindNamedSel("commanderview");
	}
	else
	{
		_driverOpticsPos = -1;
		_gunnerOpticsPos = -1;
		_commanderOpticsPos = -1;
	}

	const ParamEntry &par = *_par;

	RStringB oModelName = par >> "driverOpticsModel";
	_driverOpticsModel = oModelName.GetLength() > 0 ? Shapes.New(::GetShapeName(oModelName), true, false) : NULL;
	if (_driverOpticsModel && _driverOpticsModel->NLevels() > 0)
	{
		_driverOpticsModel->LevelOpaque(0)->MakeCockpit();
		_driverOpticsModel->OrSpecial(BestMipmap | NoDropdown);
	}
	oModelName = par >> "gunnerOpticsModel";
	_gunnerOpticsModel = oModelName.GetLength() > 0 ? Shapes.New(::GetShapeName(oModelName), true, false) : NULL;
	if (_gunnerOpticsModel && _gunnerOpticsModel->NLevels() > 0)
	{
		_gunnerOpticsModel->LevelOpaque(0)->MakeCockpit();
		_gunnerOpticsModel->OrSpecial(BestMipmap | NoDropdown);
	}
	oModelName = par >> "commanderOpticsModel";
	_commanderOpticsModel = oModelName.GetLength() > 0 ? Shapes.New(::GetShapeName(oModelName), true, false) : NULL;
	if (_commanderOpticsModel && _commanderOpticsModel->NLevels() > 0)
	{
		_commanderOpticsModel->LevelOpaque(0)->MakeCockpit();
		_commanderOpticsModel->OrSpecial(BestMipmap | NoDropdown);
	}

	_driverGetInPos.Resize(0);
	_commanderGetInPos.Resize(0);
	_gunnerGetInPos.Resize(0);
	_cargoGetInPos.Resize(0);
	if (memory)
	{
		int index = memory->FindNamedSel("pos driver");
		if (index >= 0)
		{
			const Selection &sel = memory->NamedSel(index);
			for (int i=0; i<sel.Size(); i++)
			{
				if (sel[i] >= 0) _driverGetInPos.Add(memory->Pos(sel[i]));
			}
		}
		index = memory->FindNamedSel("pos commander");
		if (index >= 0)
		{
			const Selection &sel = memory->NamedSel(index);
			for (int i=0; i<sel.Size(); i++)
			{
				if (sel[i] >= 0) _commanderGetInPos.Add(memory->Pos(sel[i]));
			}
		}
		index = memory->FindNamedSel("pos gunner");
		if (index >= 0)
		{
			const Selection &sel = memory->NamedSel(index);
			for (int i=0; i<sel.Size(); i++)
			{
				if (sel[i] >= 0) _gunnerGetInPos.Add(memory->Pos(sel[i]));
			}
		}
		index = memory->FindNamedSel("pos cargo");
		if (index >= 0)
		{
			const Selection &sel = memory->NamedSel(index);
			for (int i=0; i<sel.Size(); i++)
			{
				if (sel[i] >= 0) _cargoGetInPos.Add(memory->Pos(sel[i]));
			}
		}
		index = memory->FindNamedSel("pos codriver");
		if (index >= 0)
		{
			const Selection &sel = memory->NamedSel(index);
			for (int i=0; i<sel.Size(); i++)
			{
				if (sel[i] >= 0) _coDriverGetInPos.Add(memory->Pos(sel[i]));
			}
		}
	}
	if (_driverGetInPos.Size() == 0)
	{
		// some reasonable default
		float sizeCoef=_shape->GeometrySphere()*(1.0/4);
		_driverGetInPos.Add(Vector3(+3*sizeCoef,+1,-3*sizeCoef));
	}
	if (_commanderGetInPos.Size() == 0)
	{
		// if there is no commander point, use driver point instead
		_commanderGetInPos = _driverGetInPos;
	}
	if (_gunnerGetInPos.Size() == 0)
	{
		// if there is no gunner point, use driver point instead
		_gunnerGetInPos = _driverGetInPos;
	}
	if (_cargoGetInPos.Size() == 0)
	{
		// if there is no cargo point, use driver point instead
		_cargoGetInPos = _driverGetInPos;
	}
	if (_coDriverGetInPos.Size() == 0)
	{
		// if there is no co-driver point, use cargo point
		_coDriverGetInPos = _cargoGetInPos;
	}
	_driverGetInPos.Compact();
	_commanderGetInPos.Compact();
	_gunnerGetInPos.Compact();
	_cargoGetInPos.Compact();
	_coDriverGetInPos.Compact();

	if (_driverGetInPos.Size() == 0)
	{
		LogF
		(
			"Type %s missing driver position",(const char *)GetName()
		);
	}
}

void TransportType::DeinitShape()
{
	_driverOpticsModel.Free();
	_gunnerOpticsModel.Free();
	_commanderOpticsModel.Free();
	base::DeinitShape();
}

#pragma warning( disable: 4355 )

static const EnumName LandingModeNames[]=
{
	EnumName(Transport::LMNone, "NONE"),
	EnumName(Transport::LMLand, "LAND"),
	EnumName(Transport::LMGetIn, "GET IN"),
	EnumName(Transport::LMGetOut, "GET OUT"),
	EnumName()
};

template<>
const EnumName *GetEnumNames(Transport::LandingMode dummy)
{
	return LandingModeNames;
}

Transport::Transport(VehicleType *name, Person *driver, bool fullCreate)
:base(name,fullCreate),

//_planner(TransportGetFieldCost,this),

//_crewDammage(0), // TODO: remove - not used
_getinTime(0),
_getoutTime(Glob.time-60),
_engineOff(true),

_getOutAfterDammage(TIME_MAX), // never get out
_explosionTime(TIME_MAX), // never explode

_turretFrontUntil(TIME_MIN),

_driverPos(VZero),
_dirWanted(VZero),
_moveMode(VMMFormation),
_turnMode(VTMNone),
_fireEnabled(false),
_mouseDirWanted(VForward),
//_useStrategicPlan(false),
//_strategicPlanValid(false),
//_lastStrategicIndex(-1),
//_completedSent(false),
_lock(LSDefault),
_landing(LMNone),

_radio(CCVehicle, this, RNIntercomm),

_showDmg(false),
_showDmgValid(Glob.time-60),

_randomizer(GRandGen.RandomValue()), // do not use same sound frequency

// all hatches are by default closed
_commanderHidden(Type()->_hideProxyInCombat),
_driverHidden(Type()->_hideProxyInCombat),
_gunnerHidden(Type()->_hideProxyInCombat),
_commanderHiddenWanted(Type()->_hideProxyInCombat),
_driverHiddenWanted(Type()->_hideProxyInCombat),
_gunnerHiddenWanted(Type()->_hideProxyInCombat),

/*
_commanderHidden(Type()->_forceHideCommander),
_driverHidden(Type()->_forceHideDriver),
_gunnerHidden(Type()->_forceHideGunner),
_commanderHiddenWanted(Type()->_forceHideCommander),
_driverHiddenWanted(Type()->_forceHideDriver),
_gunnerHiddenWanted(Type()->_forceHideGunner),
*/

_manualFire(false),

_doCrash(CrashNone)
{
	_destrType=GetType()->GetDestructType();
	if( (DestructType)_destrType==DestructDefault ) _destrType=DestructEngine;

	// note: passing driver is obsolete
	DoAssert (driver==NULL);

	DriverConstruct(driver);
	_manCargo.Resize(Type()->_maxManCargo);
	_fuel=Type()->GetFuelCapacity();
}

void Transport::DriverConstruct( Person *driver )
{
	_driver=driver;
	// change brain vehicle
	if( driver )
	{
		AIUnit *brain=_driver->Brain();
		if( brain ) brain->SetVehicleIn(this);
	}
}

/*!
	\patch 1.23 Date 9/12/2001 by Ondra
	- Fixed: Empty tractor and Rapid cars were flying 1 m above ground.
	\patch_internal 1.23 Date 9/12/2001 by Ondra
	- Fixed: positioning on objects containing proxies might be incorrect.
*/
void Transport::PlaceOnSurface(Matrix4 &trans)
{
	if (!GetShape()) return;

	// place in steady position
	Vector3 pos=trans.Position();
	Matrix3 orient=trans.Orientation();
	
	float dx,dz;
	pos[1] = GLandscape->RoadSurfaceYAboveWater(pos,&dx,&dz);

	//if( type->IsKindOf(GWorld->Preloaded(VTypeStatic)) )
	if (!Type()->HasDriver())
	{
		// static vehicle - place as in buldozer
		pos += orient * GetShape()->BoundingCenter();

	}
	else
	{
		// move and change orienation
		Vector3 up(-dx,1,-dz);
		Matrix3 orient;
		orient.SetUpAndDirection(up,trans.Direction());
		trans.SetOrientation(orient);

		Shape *geom = _shape->LandContactLevel();
		if (!geom) geom = _shape->GeometryLevel();
		if (!geom) geom = _shape->Level(0);

		// dynamic vehicle
		if (geom)
		{
			Vector3 minC(0, geom->Min().Y(), 0); 
			pos -= orient*minC;
		}
	}
	trans.SetPosition(pos);
}

void Transport::EngineOn()
{
	bool isStatic = GetType()->GetFuelCapacity()<=0;
	if (isStatic) return;
	if (!_engineOff) return;
	_engineOff=false;
	OnEvent(EEEngine,!_engineOff);
}

void Transport::EngineOff()
{
	if (_engineOff) return;
	if (!EngineCanBeOff()) return;
	_engineOff=true;
	OnEvent(EEEngine,!_engineOff);
}


AIUnit *Transport::CommanderBrain() const
{
	return _commander ? _commander->Brain() : NULL;
}

Vector3 Transport::GetEnginePos() const
{
	// sound source position in model coord
	// engine is usually back
	return Vector3(0,0,-2);
}

Vector3 Transport::GetEnvironPos() const
{
	// sound source position in model coord
	// aerodynamic sound is ussually in front of us
	return Vector3(0,0,2);
}

AIUnit *Transport::DriverBrain() const
{
	return _driver ? _driver->Brain() : NULL;
}

AIUnit *Transport::GunnerBrain() const
{
	return _gunner ? _gunner->Brain() : NULL;
}

AIUnit *Transport::ObserverUnit() const
{
	return _commander ? _commander->Brain() : NULL;
}

AIUnit *Transport::CommanderUnit() const
{
	if (_effCommander)
	{
		// check if effective commander is in the vehicle
		AIUnit *unit = _effCommander->Brain();
		if (unit && unit->GetVehicleIn()==this) return unit;
	}
	if (CommanderBrain())
		return CommanderBrain();
	else if (Type()->DriverIsCommander())
	{
		if (DriverBrain())
			return DriverBrain();
		else
			return GunnerBrain();
	}
	else
	{
		if (GunnerBrain())
			return GunnerBrain();
		else
			return DriverBrain();
	}
}

AIUnit *Transport::PilotUnit() const
{
	return DriverBrain();
}

AIUnit *Transport::GunnerUnit() const
{
	if (Type()->_hasGunner)
	{
		return GunnerBrain();
	}
	else
	{
		return DriverBrain();
	}
}

AIUnit *Transport::EffectiveGunnerUnit() const
{
	if (_manualFire)
		return CommanderUnit();
	else
		return GunnerUnit();
}

TargetSide Transport::GetTargetSide() const
{
	if (!IsDammageDestroyed())
	{
		if (Commander() && !Commander()->IsDammageDestroyed()) goto NotEmpty;
		if (Driver() && !Driver()->IsDammageDestroyed()) goto NotEmpty;
		if (Gunner() && !Gunner()->IsDammageDestroyed()) goto NotEmpty;
		for (int i=0; i<GetManCargo().Size(); i++)
		{
			if (GetManCargo()[i] && !GetManCargo()[i]) goto NotEmpty;
		}
		return TCivilian;
	}
NotEmpty:
	return base::GetTargetSide();
}

float Transport::VisibleLights() const
{
	// TODO: consider light dammage
	return _pilotLight;
}

bool Transport::QIsManual() const
{
	if( !GLOB_WORLD->PlayerManual() ) return false;
	if( !GLOB_WORLD->PlayerOn() ) return false;
	if (GLOB_WORLD->PlayerOn() == Commander()) return true;
	if (GLOB_WORLD->PlayerOn() == Driver()) return true;
	if (GLOB_WORLD->PlayerOn() == Gunner()) return true;
	return false;
}

bool Transport::QIsManual(const AIUnit *unit) const
{
	if( !GLOB_WORLD->PlayerManual() ) return false;
	if( !GLOB_WORLD->PlayerOn() ) return false;
	return unit==GLOB_WORLD->PlayerOn()->Brain();
}

static void RemoveMan( Person *man, EntityAI *killer )
{
	//if( man==GLOB_WORLD->PlayerOn() ) GWorld->PlayerKilled(killer);
	//GLOB_WORLD->RemoveOutVehicle(man);
	GLOB_WORLD->RemoveSensor(man);
}

/*!
\patch 1.93 Date 9/2/2003 by Jirka
- Fixed: Units inside vehicles sometimes did not respawn.
*/

/*
static void RemoveManUnit( AIUnit *unit )
{
	if (unit)
	{
		unit->SendAnswer(AI::UnitDestroyed);
	}
}
*/

static bool CrewDammage
(
	Transport *transport, Person *man,
	EntityAI *killer, float overkill, RString ammo
)
{
	if( !man ) return false;
	//float dammage=overkill-GRandGen.RandomValue()*1.0;
	// TODO: check actual crew positions
	float dammage=overkill*(GRandGen.RandomValue()*0.5+0.5);
	/*
	if( man->GetTotalDammage()+dammage>=1.0 )
	{
		AIUnit *unit=man->Unit();
		RemoveMan(man);
		RemoveManUnit(unit);
		return true;
	}
	else
	*/
	{
		if( dammage>=0 )
		{
//			avoid check if owner is null
//			man->LocalDammage(NULL,killer,VZero,dammage,1.0);
			if (man->IsLocal())
			{
				// process dammage
				man->DoDammage(killer, VZero, dammage, 1.0, ammo);
				if (man->GetNetworkId().creator == 1)
				{
					// broadcast dammage of static object over network
					GetNetworkManager().AskForDammage(man, killer, VZero, dammage, 1.0, ammo);
				}
			}
			else
			{
				// ask owner for dammage
				GetNetworkManager().AskForDammage(man, killer, VZero, dammage, 1.0, ammo);
			}

			if (man->IsDammageDestroyed())
			{
				RemoveMan(man,killer);
				return true;
			}
		}
	}
	return false;
}

void Transport::DammageCrew( EntityAI *killer, float howMuch, RString ammo )
{
	if (!IsLocal())
	{
		ErrF("Canot dammage remote transport %s",(const char *)GetDebugName());
		//return;
	}
	for( int i=_manCargo.Size(); --i>=0; )
	{
		Person *man=_manCargo[i];
		if (!man) continue;
		if( CrewDammage(this,man,killer,howMuch,ammo) )
		{
			if (man)
			{
				man->KilledBy(killer);
/*
				AIUnit *unit=man->Brain();
				RemoveManUnit(unit);
*/
			}
		}
	}
	// crew can also survive
	if( CrewDammage(this,_driver,killer,howMuch,ammo) )
	{
		if (_driver)
		{
			_driver->KilledBy(killer);
/*
			AIUnit *unit=_driver->Brain();
			RemoveManUnit(unit);
*/
		}
	}
	if( CrewDammage(this,_gunner,killer,howMuch,ammo) )
	{
		if (_gunner)
		{
			_gunner->KilledBy(killer);
/*
			AIUnit *unit=_gunner->Brain();
			RemoveManUnit(unit);
*/
		}
	}
	if( CrewDammage(this,_commander,killer,howMuch,ammo) )
	{
		if (_commander)
		{
			_commander->KilledBy(killer);
/*
			AIUnit *unit=_commander->Brain();
			RemoveManUnit(unit);
*/
		}
	}

	if (!IsPossibleToGetIn() || !IsAbleToMove())
	{
		if (_getOutAfterDammage>Glob.time+15)
		{ // not set yet
			_getOutAfterDammage=Glob.time+GRandGen.Gauss(1.5,3,7);
		}
	}
}

void Transport::HitBy( EntityAI *killer, float howMuch, RString ammo)
{
	if (!IsLocal()) return;
	base::HitBy(killer,howMuch, ammo);
	DammageCrew(killer,howMuch, ammo);
}

void Transport::Destroy
(
	EntityAI *killer, float overkill, float minExp, float maxExp
)
{
	base::Destroy(killer,overkill,minExp,maxExp);
	// kill or eject all brains
	DammageCrew(killer,overkill,"");
	GLOB_WORLD->RemoveSensor(this); // vehicle destroyed
	UpdateStop();
}

/*!
\patch 1.22 Date 8/30/2001 by Ondra
- Fixed: AI units now eject from dammaged helicopter or plane.
\patch 1.34 Date 12/6/2001 by Ondra
- Fixed: MP: When plane or heli was dammaged, player autoejected.
*/

bool Transport::EjectCrew(Person *man)
{
	AIUnit *unit = man->Brain();
	if (!unit) return false;
	AIGroup *group=unit->GetGroup();
	if (!group) return false;

	Eject(unit);

	if( group ) group->UnassignVehicle(this);
	return true;
}

bool Transport::EjectIfAlive(Person *man)
{
	if (!man || man->IsDammageDestroyed()) return false;
	if (man->IsNetworkPlayer()) return false;
	AIUnit *unit = man->Brain();
	if (!unit || unit->GetLifeState()!=AIUnit::LSAlive) return false;

	AIGroup *group=unit->GetGroup();

	Eject(unit);

	if( group ) group->UnassignVehicle(this);

	return true;
}

bool Transport::EjectIfDead(Person *man)
{
	if (!man || !man->IsDammageDestroyed()) return false;
	AIUnit *unit = man->Brain();
	if (!unit) return false;
	Assert(IsLocal());
	unit->DoGetOut(this,false);
	return true;
}

void Transport::EjectAllNotFixed()
{
	if (Driver() && Type()->_ejectDeadDriver)
	{
		EjectCrew(Driver());
	}
	if (Commander() && Type()->_ejectDeadCommander)
	{
		EjectCrew(Commander());
	}
	if (Gunner() && Type()->_ejectDeadGunner)
	{
		EjectCrew(Gunner());
	}
	if (Type()->_ejectDeadCargo)
	{
		for (int i=0; i<GetManCargo().Size(); i++)
		{
			Person *crew = GetManCargo()[i];
			if (crew) EjectCrew(crew);
		}
	}
}

void Transport::SetDammage(float dammage)
{
	bool doRepair = dammage < GetTotalDammage();
	base::SetDammage(dammage);
	if (doRepair)
	{
		_getOutAfterDammage = TIME_MAX;
		_explosionTime = TIME_MAX;
	}
}

void Transport::ReactToDammage()
{
	if (IsDammageDestroyed()) _isDead=true;
	if( _isDead || _isUpsideDown )
	{
		// tank  up - it is finished
		EngineOff();
	}

	if( Glob.time>_getOutAfterDammage )
	{
		// all must get out - vehicle unusable
		// unassign all assigned soldiers 
		// man->AssignAsCargo(NULL);
		// TODO: there may be some assigned soldier outside the vehicle

		for( int i=_manCargo.Size(); --i>=0; )
		{
			EjectIfAlive(_manCargo[i]);
		}
		EjectIfAlive(_driver);
		EjectIfAlive(_gunner);
		EjectIfAlive(_commander);
	}

	if (IsLocal())
	{
		if (Type()->_ejectDeadDriver) EjectIfDead(_driver);
		if (Type()->_ejectDeadGunner) EjectIfDead(_gunner);
		if (Type()->_ejectDeadDriver) EjectIfDead(_commander);
		if (Type()->_ejectDeadCargo)
		{
			for( int i=_manCargo.Size(); --i>=0; )
			{
				EjectIfDead(_manCargo[i]);
			}
		}
	}

	if (Glob.time>_explosionTime)
	{
		if (GetRawTotalDammage()<1 )
		{
			EntityAI *killer = this;
			if (_lastDammage && _lastDammageTime > Glob.time - 60)
				killer = _lastDammage;

			// kill crew (with slight chance of escape)
			DammageCrew(killer,10,"");
			// explode
			//LogF("%.3f Start smoke",Glob.time.toFloat());
			Destroy(killer,1.0,0.5,0.75);
			SetTotalDammage(1);
		}
	}

	/*
	if( !IsWorking() || !IsAbleToMove() )
	{
		if (_getOutAfterDammage>Glob.time+15)
		{ // not set yet
			_getOutAfterDammage=Glob.time+GRandGen.Gauss(1.5,3,7);
		}
	}
	*/
}

float Transport::GetExplosives() const
{
	// if there is an explicit value, use it
	// if it is negative, it is coeficient
	// how much explosives is in
	return base::GetExplosives(); // ammunition
}

/*!
\patch 1.25 Date 09/29/2001 by Ondra
- Fixed: MP: When bad client-side prediction led to crash,
vehicle was dammaged and could be even destroyed.
*/

void Transport::CrashDammage( float ammount, const Vector3 &pos )
{
	// only local vehicles may crash
	if (IsLocal())
	{
		ammount*=GetType()->GetInvArmor();
		LocalDammage(NULL,this,pos,ammount,GetRadius());
	}
}

void Transport::RemoveAssignement(AIUnit *unit)
{
	if (_driverAssigned == unit)
		_driverAssigned = NULL;
	if (_gunnerAssigned == unit)
		_gunnerAssigned = NULL;
	if (_commanderAssigned == unit)
		_commanderAssigned = NULL;
	int i, n = _cargoAssigned.Size();
	for (i=0; i<n; i++)
	{
		if (_cargoAssigned[i] == unit) _cargoAssigned[i] = NULL;
	}
	_cargoAssigned.Compact();
}

void Transport::KeyboardAny(AIUnit *unit, float deltaT)
{
}

ManVehAction Transport::DriverAction() const
{
	if (_driverHidden>0.5) return  Type()->_driverInAction;
	return Type()->_driverAction;
}
ManVehAction Transport::CommanderAction() const
{
	if (_commanderHidden>0.5) return  Type()->_commanderInAction;
	return Type()->_commanderAction;
}
ManVehAction Transport::GunnerAction() const
{
	if (_gunnerHidden>0.5) return  Type()->_gunnerInAction;
	return Type()->_gunnerAction;
}
ManVehAction Transport::CargoAction(int pos) const
{
	int maxPos = Type()->_cargoAction.Size()-1;
	if (maxPos<0) return ManVehActNone;
	saturateMin(pos,maxPos);
	return Type()->_cargoAction[pos];
}

float Transport::DriverAnimSpeed() const
{
	return 1;
}
float Transport::CommanderAnimSpeed() const
{
	return 1;
}
float Transport::GunnerAnimSpeed() const
{
	return 1;
}
float Transport::CargoAnimSpeed(int position) const
{
	return 1;
}

bool Transport::IsPersonHidden(Person *person) const
{
	if (person == Commander())
		return IsCommanderHidden();
	else if (person == Driver())
		return IsDriverHidden();
	else if (person == Gunner())
		return IsGunnerHidden();

	return false;
}

void Transport::HidePerson(Person *person, float hide)
{
	if (person == Commander())
		HideCommander(hide);
	else if (person == Driver())
		HideDriver(hide);
	else if (person == Gunner())
		HideGunner(hide);
}

void Transport::AimDriver(Vector3Par direction)
{
	if (QIsManual())
	{
		if (GInput.lookAroundEnabled)
		{
			_mouseDirWanted = Direction();
			//_mouseTurnWanted = 0;
		}
		else
		{
			_mouseDirWanted = direction;
			//Vector3 relDir(VMultiply,DirWorldToModel(),direction);
			//_mouseTurnWanted = atan2(relDir.X(),relDir.Z()) * 0.7;
		}
	}
}

void Transport::SelectWeaponCommander(AIUnit *unit, int weapon)
{
	// used when weapon is selected from UI
	if (unit == GunnerUnit() || IsManualFire())
	{
		base::SelectWeaponCommander(unit,weapon);
	}
	else
	{
		if (IsLocal())
		{
			SendLoad(weapon);
		}
		else
		{
			RadioMessageVLoad msg(this, weapon);
			GetNetworkManager().SendRadioMessage(&msg);
		}
	}
}

#if _ENABLE_AI
/*!
\patch 1.08 Date 7/21/2001 by Ondra.
- Fixed: Sometimes tank was aiming at target in 'prepare' mode only,
because the target was already inactive. Tank commander now cancels target
in such situation.
*/

void Transport::AICommander(AIUnit *unit, float deltaT )
{
	Assert(unit);
	Assert(unit->GetSubgroup());

	if (unit != PilotUnit())
	{
		if (unit->IsSubgroupLeader())
		{
			if (unit->GetPlanningMode() == AIUnit::LeaderPlanned)
			{
				// new valid path
				Vector3Val wantedPosition = unit->GetWantedPosition();
				float prec = GetPrecision();
				if (_moveMode != VMMMove || wantedPosition.Distance2(_driverPos) > 0)
				{
					if (wantedPosition.Distance2(_driverPos) > Square(prec))
					{
						if (_radio.IsEmpty())	// radio protocol is empty
						{
							SendMove(wantedPosition);
						}
					}
					else 
					{
						_moveMode = VMMMove;
						_driverPos = wantedPosition;
					}
				}
			}
		}
		else
		{
			if (_moveMode != VMMFormation && _radio.IsEmpty())
			{
				SendJoin();
			}
		}
	}

	if (unit == GunnerUnit())
	{
		SelectFireWeapon();
		if (_fire._fireMode >= 0) SelectWeapon(_fire._fireMode);
	}
	else
	{
		// old state
		int oldWeapon = _commanderFire._fireMode;
		// commander must remember his decision
		SelectFireWeapon(_commanderFire);

		if (_commanderFire._firePrepareOnly)
		{
			// we do not want to fire
			Target *tgt = _commanderFire._fireTarget;
			if (tgt)
			{
				// FIX
				// we have some target - check if we should cancel it
				// we cannot do it if our leader is player and it is out assigned target
				AIGroup *grp = unit->GetGroup();
				if (grp && !grp->IsAnyPlayerGroup() || tgt!=unit->GetTargetAssigned())
				{
					AICenter *center = grp->GetCenter();
					// check if target is unsuitable
					if (tgt->State(unit)<TargetEnemyEmpty || center->IsFriendly(tgt->side))
					{
						#if 0
						LogF
						(
							"*** commander canceling target %s",
							(const char *)tgt->idExact->GetDebugName()
						);
						#endif
						_commanderFire._fireTarget = NULL;
						_commanderFire._fireMode = -1;
						// unassign target if necessary
						if (tgt==unit->GetTargetAssigned())
						{
							unit->AssignTarget(NULL);
						}

					}
				}
				// FIX END
			}
		}

		// send load only when weapon changed
		if (_commanderFire._fireMode >= 0 && _commanderFire._fireMode != oldWeapon)
		{
			if (IsLocal())
				SendLoad(_commanderFire._fireMode);
			else
			{
				RadioMessageVLoad msg(this, _commanderFire._fireMode);
				GetNetworkManager().SendRadioMessage(&msg);
			}
//			SelectWeapon(_commanderFire._fireMode);
		}

		// TODO: better check the radio channel
		if (_radio.IsEmpty())	// radio protocol is empty
		{
			if (_commanderFire._fireTarget != _fire._fireTarget)
			{
				if(_commanderFire._fireTarget)
				{
					SendTarget(_commanderFire._fireTarget);
				}
				else if (_radio.IsSilent())
				{
					// when channel is silent, we may transmit "no target"
					// (very low priority message)
					SendTarget(NULL);
				}
			}
			else if (!_commanderFire._firePrepareOnly)
			{
				if
				(
					_fire._firePrepareOnly &&
					_commanderFire._fireTarget
				)
				{
					// FIX: automatic weapon - assume gunner is able to fire/cease fire well
					//const WeaponModeType *mode = GetWeaponMode(_currentWeapon);

					if
					(
						//{ FIX - crash on dedicated server
						_currentWeapon >= 0 && _currentWeapon < NMagazineSlots() &&
						//}
						GetWeaponLoaded(_currentWeapon) &&
						GetAimed(_currentWeapon,_commanderFire._fireTarget)>=0.75 &&
						GetWeaponReady(_currentWeapon,_commanderFire._fireTarget)
					)
					// FIX END
					{
						if (!GetAIFireEnabled(_commanderFire._fireTarget)) ReportFireReady();
						else
						{
							SendSimpleCommand(SCFire);
						}
					}
				}
			}
			else
			{

				if (!_fire._firePrepareOnly)
				{
					SendSimpleCommand(SCCeaseFire);
				}
			}
		}
	}

	// turn in / turn out
	if (Type()->_hideProxyInCombat)
	{
		if (unit->GetCombatMode() >= CMCombat || Type()->_forceHideCommander)
		{
			// some danger - head in
			Person *person = unit->GetPerson();
			HidePerson(person, 1);
		}
		else if (unit->GetCombatMode() <= CMSafe)
		{
			// safe - we may turn heads out
			Person *person = unit->GetPerson();
			HidePerson(person, 0);
		}
	}
}
#endif //_ENABLE_AI

static void ReportIfDead(AIUnit *&unit, AIUnit *reportedBy)
{
	if (unit && unit->GetLifeState()!=AIUnit::LSAlive)
	{
		if (reportedBy)
		{
			AIGroup *grp = unit->GetGroup();
			if (grp) grp->SetReportBeforeTime(unit,Glob.time+10);
		}
		unit = NULL;
	}
}

/*!
\patch 1.33 Date 11/29/2001 by Ondra.
- Fixed: MP: Airplanes autofiring from MG or unguided missiles.
*/

bool Transport::SimulateUnits( float deltaT )
{
	bool manual = GLOB_WORLD->PlayerManual() && GLOB_WORLD->PlayerOn();
	AIUnit *player = manual ? GLOB_WORLD->PlayerOn()->Brain() : NULL;

#if _ENABLE_CHEATS
	extern bool disableUnitAI;
	if( disableUnitAI ) return true;
#endif

	bool isCommanderHidden = false;

	AIUnit *commanderUnit = CommanderUnit();
	AIUnit *gunnerUnit = GunnerUnit();
	AIUnit *driverUnit = PilotUnit();

	/**/
	AIUnit *reportedBy = NULL;
	// find any unit that is alive and is able to report the vehicle
	if (driverUnit && driverUnit->GetLifeState()==AIUnit::LSAlive)
	{
		reportedBy = driverUnit;
	}
	if (gunnerUnit && gunnerUnit->GetLifeState()==AIUnit::LSAlive)
	{
		reportedBy = gunnerUnit;
	}
	if (commanderUnit && commanderUnit->GetLifeState()==AIUnit::LSAlive)
	{
		reportedBy = commanderUnit;
	}
	// TODO: search in cargo
	for (int i=0; i<_manCargo.Size(); i++)
	{
		Person *person = _manCargo[i];
		if (!person) continue;
		AIUnit *unit = person->Brain();
		if (!unit || unit->GetLifeState()!=AIUnit::LSAlive) continue;
		reportedBy = unit;
	}

	// this unit should report any dead units
	ReportIfDead(gunnerUnit,reportedBy);
	ReportIfDead(driverUnit,reportedBy);
	ReportIfDead(commanderUnit,reportedBy);

	/*
	// commander automaticaly reports dead crew members
	if (gunnerUnit && gunnerUnit->GetLifeState()!=AIUnit::LSAlive)
	{
		gunnerUnit = NULL;
	}
	if (driverUnit && driverUnit->GetLifeState()!=AIUnit::LSAlive)
	{
		driverUnit = NULL;
	}
	if (commanderUnit && commanderUnit->GetLifeState()!=AIUnit::LSAlive)
	{
		if (gunnerUnit || driverUnit)
		{
			AIGroup *grp = commanderUnit->GetGroup();
			if (grp) grp->SetReportBeforeTime(commanderUnit,Glob.time+10);
		}
		commanderUnit = NULL;
	}
	*/

	if (commanderUnit && commanderUnit->GetLifeState()==AIUnit::LSAlive)
	{
		if (manual && commanderUnit == player)
		{
			if (commanderUnit->IsGroupLeader())
			{
				Person *person = commanderUnit->GetPerson();
				const TransportType *type = Type();
				bool hidden = false;
				if (person == Commander())
					hidden = type->_hideProxyInCombat && IsCommanderHidden();
				else if (person == Driver())
					hidden = type->_hideProxyInCombat && IsDriverHidden();
				else if (person == Gunner())
					hidden = type->_hideProxyInCombat && IsGunnerHidden();

				commanderUnit->GetGroup()->SetCombatModeMinor(hidden ? CMCombat : CMSafe);
			}
		}
		else if (commanderUnit->GetPerson()->IsRemotePlayer())
		{
			// remote player - avoid fake commander commands
		}
#if _ENABLE_AI
		else
			AICommander(commanderUnit, deltaT);
#endif
		
		isCommanderHidden = IsPersonHidden(commanderUnit->GetPerson());
	}

	if (gunnerUnit)
	{
		if (manual && gunnerUnit == player)
		{
			// KeyboardGunner is realized in InGameUI
		}
		else if (gunnerUnit->GetPerson()->IsRemotePlayer())
		{
			// remote player - avoid fake commander fires
		}
#if _ENABLE_AI
		else if (gunnerUnit->IsLocal())
		{
			if
			(
				!_fire._fireTarget || _fire.GetTargetFinished(CommanderUnit())
			)
			{
				_fire._fireMode = -1;
				_fire._fireTarget = NULL;
			}
			if (_currentWeapon >= 0)
			{
				const WeaponModeType *mode = GetWeaponMode(_currentWeapon);
				if (!_fire._firePrepareOnly && mode && mode->_autoFire)
				{
					// machine gun automat

					// if we do not know target precision exactly enough
					if (_fire._fireTarget)
					{
						// target may be very old
						if
						(
							!_fire._fireTarget->IsKnownBy(gunnerUnit) ||
							_fire.GetTargetFinished(gunnerUnit) ||
							_fire._fireTarget->lastSeen < Glob.time-10 ||
							_fire._fireTarget->idExact &&
							!_fire._fireTarget->idExact->LockPossible(mode->_ammo)
						)
						{
							_fire.SetTarget(gunnerUnit,NULL);
						}
					}

					float timeToLive = gunnerUnit->GetTimeToLive();
					AIGroup *grp = gunnerUnit->GetGroup();
					AICenter *center = grp->GetCenter();
					FireResult result;
					if
					(
						!gunnerUnit->IsHoldingFire() &&
						(
							!_fire._fireTarget ||
							!WhatFireResult(result, *_fire._fireTarget, _currentWeapon, timeToLive)
						)
					)
					{
						// preferred target not visible - fire on anything else
						// select only few nearest enemy targets
						// select enemy target with best fire result
//						_firePrepareOnly = true;
						const TargetList &list = grp->GetTargetList();
						int maxEnemies = 4;
						for (int i=0; i<list.Size(); i++)
						{
							FireResult tResult;
							Target *tgtI = list[i];
							if (!tgtI->IsKnownBy(gunnerUnit)) continue;
							if (tgtI->State(gunnerUnit)<TargetEnemyCombat) continue;
							if (!center->IsEnemy(tgtI->side)) continue;
							// TODO: time argument? - max of reload time?
							if (WhatFireResult(tResult, *tgtI, _currentWeapon, timeToLive))
							{
								if (tResult.Surplus() > result.Surplus())
								{
									result = tResult;
									_fire.SetTarget(gunnerUnit,tgtI);
//									_firePrepareOnly = false;
								}
							}
							if (--maxEnemies <= 0) break;
						}
					}
				}
			}
			AIGunner(gunnerUnit, deltaT);
			if (gunnerUnit && gunnerUnit != commanderUnit)
			{
				HidePerson
				(
					gunnerUnit->GetPerson(),
					isCommanderHidden || Type()->_forceHideGunner ? 1 : 0
				);
			}
		}
#endif //_ENABLE_AI
	}
	if (driverUnit)
	{
		if (manual && driverUnit == player)
		{
			if( !GWorld->GetPlayerSuspended() )
			{
				CheckAway();
				KeyboardPilot(driverUnit, deltaT);
			}
			else
			{
				SuspendedPilot(driverUnit, deltaT);
			}
		}
		// else if (driverUnit->GetPerson()->IsRemotePlayer())
		else if (!driverUnit->GetPerson()->IsLocal())
		{
			FakePilot(deltaT);
			return true;
		}
#if _ENABLE_AI
		else
		{
			AIPilot(driverUnit, deltaT);
			if (driverUnit && driverUnit != CommanderUnit())
			{
				bool hideDriver = isCommanderHidden || GetFireTarget()!=NULL;
				HidePerson
				(
					driverUnit->GetPerson(),
					hideDriver || Type()->_forceHideDriver
				);
			}
		}
#endif //_ENABLE_AI
		return true;
	}

	return false; // no pilot performed
}

void Transport::InitUnits()
{
	// called atfer vehicle is placed in landscape
	// unit are setup and initialized
	AIUnit *unit = CommanderUnit();
	if (unit && Type()->_hideProxyInCombat)
	{
		// initialize hatches - open when in aware or safe mode
		CombatMode cm = unit->GetCombatMode();
		bool hide = cm>=CMCombat;
		if (Type()->_hasDriver && Driver())
		{
			_driverHidden = _driverHiddenWanted = hide || Type()->_forceHideDriver;
			_driver->SwitchVehicleAction(DriverAction());
		}
		if (Type()->_hasGunner && Gunner())
		{
			_gunnerHidden = _gunnerHiddenWanted = hide || Type()->_forceHideGunner;
			_gunner->SwitchVehicleAction(GunnerAction());
		}
		if (Type()->_hasCommander && Commander())
		{
			_commanderHidden = _commanderHiddenWanted = hide || Type()->_forceHideCommander;
			_commander->SwitchVehicleAction(CommanderAction());
		}
	}
	base::InitUnits();
}

bool Transport::ValidateCrew(Person *crew, bool complex) const
{
	if (!crew) return true;
	// check if crew is out
	bool ok = true;
	if (crew->IsInLandscape())
	{
		RptF
		(
			"Error: Crew %s of %s in landscape",
			(const char *)crew->GetDebugName(),
			(const char *)GetDebugName()
		);
		ok = false;
	}
	return ok;
}

bool Transport::Validate(bool complex) const
{
	// check if all crew members are out
	bool ok = true;
	if (!ValidateCrew(_driver,complex))
	{
		RptF("Driver invalid");
		ok = false;
	}
	if (!ValidateCrew(_commander,complex))
	{
		RptF("Commander invalid");
		ok = false;
	}
	if (!ValidateCrew(_gunner,complex))
	{
		RptF("Gunner invalid");
		ok = false;
	}
	for (int i=0; i<_manCargo.Size(); i++)
	{
		if (!ValidateCrew(_manCargo[i],complex))
		{
			RptF("ManCargo %d invalid",i);
			ok = false;
		}
		
	}
	return ok;
}

void Transport::Init( Matrix4Par pos )
{
	if (!EngineCanBeOff())
	{
		_engineOff = false;
	}
	base::Init(pos);
}

void Transport::TrackTargets
(
	TargetList &res, bool initialize, float trackTargetsPeriod
)
{
	const VehicleType *type = GetType();
	AIUnit *unit=CommanderBrain();
	if (unit) base::TrackTargets
	(
		res,unit,type->_commanderCanSee,initialize,1e10,
		trackTargetsPeriod
	);
	unit=GunnerBrain();
	if (unit) base::TrackTargets
	(
		res,unit,type->_gunnerCanSee,initialize,1e10,
		trackTargetsPeriod
	);
	// all units may have driver
	unit=DriverBrain();
	if (unit) base::TrackTargets
	(
		res,unit,type->_driverCanSee,initialize,1e10,
		trackTargetsPeriod
	);

	// ignore fast vehicles - shots and smokes
	_trackTargetsTime=Glob.time;
}

Vector3 Transport::GetCameraDirection( CameraType camType ) const
{
	Matrix4 transf;
	// used for external camera
	if (GetProxyCamera(transf, camType))
	{
		return DirectionModelToWorld(transf.Direction());
	}
	return Direction();
}

Matrix4 Transport::InsideCamera( CameraType camType ) const
{
	Matrix4 transf;
	if (camType == CamGunner && GetOpticsCamera(transf, camType)) return transf;		
	if (GetProxyCamera(transf, camType))
	{
		/*
		// sometimes view is forced to follow weapon
		if (_head.IsValid())
		{
			// apply head movement
			Matrix3 headTrans;
			Vector3 up = _head.Position()-_head.Neck();
			headTrans.SetUpAndDirection(up,VForward);

			// change camera orientation only
			transf.SetUpAndDirection
			(
				headTrans * transf.DirectionUp(),transf.Direction()
			);
		}
		*/
		return transf;
	}
	// no proxy: return vehicle position
	return MIdentity;
}

int Transport::InsideLOD( CameraType camType ) const
{
	bool hidden = false;
	AIUnit *unit = GWorld->FocusOn();
	Person *player = unit ? unit->GetPerson() : NULL;
	if (player)
	{
		if (player == Commander()) hidden = IsCommanderHidden();
		else if (player == Driver()) hidden = IsDriverHidden();
		else if (player == Gunner()) hidden = IsGunnerHidden();
		else
		{

			int index = GetShape()->FindSpecLevel(VIEW_CARGO);
			if (index>=0) return index;
		}
		if (!Type()->_hideProxyInCombat)
		{
			// always use internal views if there is no in/out position
			hidden = true;
		}
		if (hidden)
		{
			if (player == Gunner())
			{
				int index = GetShape()->FindSpecLevel(VIEW_GUNNER);
				if (index < 0)
				{
					if (camType == CamGunner)
						index = 0;
					else
					{
						// check if we should use gunner or cargo view
						if (Type()->_gunnerUsesPilotView) index = GetShape()->FindSpecLevel(VIEW_PILOT);
						else index = GetShape()->FindSpecLevel(VIEW_CARGO);
					}
				}
				//if (index < 0) index = GetShape()->FindSpecLevel(VIEW_COMMANDER);
				return index;
			}
			else if (player == Commander())
			{
				if (camType == CamGunner) return 0;
				int index = -1;
				if (Type()->_commanderUsesPilotView) index = GetShape()->FindSpecLevel(VIEW_PILOT);
				else index = GetShape()->FindSpecLevel(VIEW_CARGO);
				if (index < 0) index = GetShape()->FindSpecLevel(VIEW_GUNNER);
				return index;
			}
			else
			{
				if (camType == CamGunner) return 0;
				int index = GetShape()->FindSpecLevel(VIEW_PILOT);
				if (index < 0) index = GetShape()->FindSpecLevel(VIEW_CARGO);
				return index;
			}
		}
	}

	return 0;
}

static CursorMode CursorMouseModeDetect()
{
	// mouse absolute while mouse is active, relative later
	if (GInput.keyboardCursorLastActive>GInput.mouseCursorLastActive)
	{
		// keyboard cursor control pending - return relative mouse
		return CMouseRel;
	}
	if (GInput.mouseCursorLastActive<Glob.uiTime-1)
	{
		return CMouseRel;
	}
	return CMouseAbs;
}

CursorMode Transport::GetCursorRelMode(CameraType camType) const
{
	// different rules based on focus role
	// driver rules - check lookAround

	AIUnit *unit = GWorld->FocusOn();
	Person *player = unit ? unit->GetPerson() : NULL;
	if (player)
	{
		if (player==Driver())
		{
			// relative cursor only in virtual cockpit/view
			// and only when mouse is not active
			if (camType==CamInternal || camType==CamExternal)
			{
				if (IsVirtual(camType))
				{
					if (GInput.JoystickActive())
					{
						return CMouseRel;
					}
					if
					(
						!GInput.MouseCursorActive() ||
						!GInput.MouseTurnActive() && !GInput.lookAroundEnabled
					)
					{
						return CKeyboard;
					}
				}
				if (GInput.lookAroundEnabled)
				{
					return CursorMouseModeDetect();
				}
				// when driving, absolute mouse is required
				return CMouseAbs;
			}
			else if (camType==CamGunner) return CMouseAbs;
			else
			{
				return base::GetCursorRelMode(camType);
			}
		}
	}
	// rules for non-driver (cargo, commander etc..)
	if (camType==CamInternal || camType==CamExternal)
	{
		return CursorMouseModeDetect();
	}
	else if (camType==CamGunner) return CMouseAbs;
	else
	{
		return base::GetCursorRelMode(camType);
	}
}

bool Transport::IsVirtual( CameraType camType ) const
{
	if( camType==CamGunner ) return false;
	return true;
}

bool Transport::IsVirtualX( CameraType camType ) const
{
	AIUnit *unit = GWorld->FocusOn();
	Person *player = unit ? unit->GetPerson() : NULL;
	if (player)
	{
		if (player==Driver())
		{
			if (GInput.lookAroundEnabled) return true;
			return camType!=CamInternal && camType!=CamExternal;
		}
	}
	return true;
}

bool Transport::IsGunner( CameraType camType ) const
{
	// check unit position
	AIUnit *unit = GWorld->FocusOn();
	if (unit && unit==PilotUnit())
	{
		return base::IsGunner(camType);
	}
	const TransportType *type = Type();
	if (type->GetOutGunnerMayFire() || !type->_hideProxyInCombat)
	{
		if (camType==CamInternal) return true;
	}
	return camType==CamGunner || camType==CamExternal;
}

void Transport::LimitVirtual
(
	CameraType camType, float &heading, float &dive, float &fov
) const
{	
	if (camType==CamInternal || camType==CamExternal)
	{
		// internal camera - default processing
		AIUnit *unit = GWorld->FocusOn();
		Person *person = unit ? unit->GetPerson() : NULL;
		if (person)
		{
			if (person==_commander)
			{
				Type()->_viewCommander.LimitVirtual(camType,heading,dive,fov);
			}
			else if (person==_gunner)
			{
				Type()->_viewGunner.LimitVirtual(camType,heading,dive,fov);

				// sometimes view is forced to follow weapon
				if (_gunnerHidden<0.5 && Type()->GetOutGunnerMayFire())
				{
					heading = 0;
					dive = 0;
				}
			}
			else if (person==_driver)
			{
				Type()->_viewPilot.LimitVirtual(camType,heading,dive,fov);
			}
			else
			{
				Type()->_viewCargo.LimitVirtual(camType,heading,dive,fov);
			}
			return;
		}
	}
	else if (camType == CamGunner)
	{
		Type()->_viewOptics.LimitVirtual(camType,heading,dive,fov);
	}

	base::LimitVirtual(camType,heading,dive,fov);
}

void Transport::LimitCursor
(
	CameraType camType, Vector3 &dir
) const
{
}

/*!
\patch_internal 1.27 Date 10/10/2001 by Jirka
- Changed: optics fov, heading and dive limits and initials moved from program into config
\patch_internal 1.28 Date 10/26/2001 by Jirka
- Fixed: initial parameters for optics was replaced by base::InitVirtual
*/
void Transport::InitVirtual
(
	CameraType camType, float &heading, float &dive, float &fov
) const
{
	AIUnit *unit = GWorld->FocusOn();
	// driver or not internal - default processing
	Person *person = unit ? unit->GetPerson() : NULL;
	if (camType == CamGunner)
	{
		Type()->_viewOptics.InitVirtual(camType,heading,dive,fov);
		return;
	}
	else if (person)
	{
		if (person==_commander)
		{
			Type()->_viewCommander.InitVirtual(camType,heading,dive,fov);
		}
		else if (person==_gunner)
		{
			Type()->_viewGunner.InitVirtual(camType,heading,dive,fov);
		}
		else if (person==_driver)
		{
			Type()->_viewPilot.InitVirtual(camType,heading,dive,fov);
		}
		else
		{
			Type()->_viewCargo.InitVirtual(camType,heading,dive,fov);
		}
		return;
	}
	// driver or not internal - default processing
	base::InitVirtual(camType,heading,dive,fov);
}

bool Transport::ConsumeFuel(float ammount)
{
	bool wasFuel = _fuel>0;
	_fuel -= ammount;
	bool isFuel = _fuel>0;
	if (isFuel!=wasFuel) OnEvent(EEFuel,isFuel);
	saturate(_fuel,0,GetType()->GetFuelCapacity());
	return isFuel;
}

void Transport::Refuel(float ammount)
{
	ConsumeFuel(-ammount);
}


void Transport::Simulate( float deltaT, SimulationImportance prec )
{
	// turn in / turn out
	const float turnInOutSpeed = 1.0;
	if (_commanderHiddenWanted > _commanderHidden)
	{
		_commanderHidden += turnInOutSpeed * deltaT;
		saturateMin(_commanderHidden, _commanderHiddenWanted);
		if (_commander) _commander->SwitchVehicleAction(CommanderAction());
	}
	else if (_commanderHiddenWanted < _commanderHidden)
	{
		_commanderHidden -= turnInOutSpeed * deltaT;
		saturateMax(_commanderHidden, _commanderHiddenWanted);
		if (_commander) _commander->SwitchVehicleAction(CommanderAction());
	}
	if (_driverHiddenWanted > _driverHidden)
	{
		_driverHidden += turnInOutSpeed * deltaT;
		saturateMin(_driverHidden, _driverHiddenWanted);
		if (_driver) _driver->SwitchVehicleAction(DriverAction());
	}
	else if (_driverHiddenWanted < _driverHidden)
	{
		_driverHidden -= turnInOutSpeed * deltaT;
		saturateMax(_driverHidden, _driverHiddenWanted);
		if (_driver) _driver->SwitchVehicleAction(DriverAction());
	}
	if (_gunnerHiddenWanted > _gunnerHidden)
	{
		_gunnerHidden += turnInOutSpeed * deltaT;
		saturateMin(_gunnerHidden, _gunnerHiddenWanted);
		if (_gunner) _gunner->SwitchVehicleAction(GunnerAction());
	}
	else if (_gunnerHiddenWanted < _gunnerHidden)
	{
		_gunnerHidden -= turnInOutSpeed * deltaT;
		saturateMax(_gunnerHidden, _gunnerHiddenWanted);
		if (_gunner) _gunner->SwitchVehicleAction(GunnerAction());
	}
	
	// advance animation of proxy objects
	if (_driver)
	{
		_driver->BasicSimulation(deltaT, prec, DriverAnimSpeed());
	}
	if (_gunner)
	{
		_gunner->BasicSimulation(deltaT, prec, GunnerAnimSpeed());
	}
	if (_commander)
	{
		_commander->BasicSimulation(deltaT, prec, CommanderAnimSpeed());
	}
	for (int i=0; i<_manCargo.Size(); i++)
	{
		ManCargoItem &item = _manCargo.Set(i);
		Person *man = item.man;
		if (man)
		{
			man->BasicSimulation(deltaT, prec, CargoAnimSpeed(i));
		}
	}

	_radio.Simulate(deltaT);
	base::Simulate(deltaT,prec);
	ReactToDammage();

	// markers on / off
	if (!CommanderUnit() || CommanderUnit()->IsInCargo())
	{
		// empty vehicle
		_pilotLight = false;
	}
	else if (CommanderUnit()->HasAI())
	{
		// vehicle with AI driver
		_pilotLight = false;
		float thold = _randomizer * 0.6 + 0.2;
		if (GScene->MainLight()->NightEffect() > thold)
		{
			if (!IsCautiousOrDanger())
			{
				//LogF("%s: %.0f on",(const char *)GetDebugName(),Glob.time-Time(0));
				_pilotLight = true;
			}
			else
			{
				//LogF("%s: %.0f off",(const char *)GetDebugName(),Glob.time-Time(0));
			}
		}
	}

	if (_markers.Size() == 0 && _markersBlink.Size() == 0)
	{
		if (_pilotLight)
		{
			LODShapeWithShadow *shape = GLOB_SCENE->Preloaded(Marker);
			for (int i=0; i<GetType()->_lights.Size(); i++)
			{
				const LightInfo &info = GetType()->_lights[i];
				LightPointOnVehicle *light = new LightPointOnVehicle
				(
					shape, info.color, info.ambient,
					this, info.position
				);
				light->SetBrightness(info.brightness);

				if (info.type == LightTypeMarker)
					_markers.Add(light);
				else if (info.type == LightTypeMarkerBlink)
					_markersBlink.Add(light);

				GLOB_SCENE->AddLight(light);
			}
			_markersOn = Glob.time - GRandGen.RandomValue();
		}
	}
	else
	{
		if (!_pilotLight)
		{
			_markers.Resize(0);
			_markersBlink.Resize(0);
		}
	}

	if (_pilotLight)
	{
		bool on = (toInt(2.0 * (Glob.time - _markersOn)) % 2) != 0; 
		for (int i=0; i<_markersBlink.Size(); i++)
		{
			_markersBlink[i]->Switch(on);
		}
	}

	if (Glob.time > _showDmgValid)
		_showDmg = false;
}

/*!
\patch 1.52 Date 4/19/2002 by Ondra
- Fixed: Dammage alarm was still functional in some fully destroyed vehicles.
*/

void Transport::ShowDammage(int part)
{
	if (IsDammageDestroyed()) return;
	if (!EngineIsOn()) return;
	_showDmg = true;
	_showDmgValid = Glob.time + 5.0f;
	base::ShowDammage(part);
}

bool Transport::IsAnimated( int level ) const
{
	return true;
}

void Transport::Animate( int level )
{
	if (GScene->MainLight()->NightEffect() > 0.01 && EngineIsOn())
	{
		GetType()->_dashboard.Unhide(_shape, level);
	}
	else
	{
		GetType()->_dashboard.Hide(_shape, level);
	}

	//float dt = _showDmgValid - Glob.time;
	bool on = (toInt(2.0 * (_showDmgValid - Glob.time)) % 2) != 0; 
	if (_showDmg && on)
		GetType()->_showDmg.Unhide(_shape, level);
	else
		GetType()->_showDmg.Hide(_shape, level);

	base::Animate(level);
}

void Transport::AnimateManProxyMatrix
(
	int level, const ManProxy &proxy, Matrix4 &proxyTransform
) const
{
	AnimateMatrix(proxyTransform,level,proxy.selection);
	// default: do not know how to animate man proxy
}

void Transport::Deanimate( int level )
{
	GetType()->_dashboard.Unhide(_shape, level);
	base::Deanimate(level);
}

LODShapeWithShadow *Transport::GetOpticsModel(Person *person)
{
	if (!person) return NULL;
	// if person can be hidden and is not, it means he cannot use optics

	if (person == _driver)
	{
		if (Type()->_hideProxyInCombat && _driverHidden<0.5) return NULL;
		return Type()->_driverOpticsModel;
	}
	else if (person == _gunner)
	{
		if (Type()->_hideProxyInCombat && _gunnerHidden<0.5) return NULL;
		return Type()->_gunnerOpticsModel;
	}
	else if (person == _commander)
	{
		if (Type()->_hideProxyInCombat && _commanderHidden<0.5) return NULL;
		return Type()->_commanderOpticsModel;
	}
	return NULL;
}

bool Transport::GetForceOptics(Person *person) const
{
	return false;
}

PackedColor Transport::GetOpticsColor(Person *person)
{
	if (!person) return PackedBlack;

	if (person == _driver) return Type()->_driverOpticsColor;
	else if (person == _gunner) return Type()->_gunnerOpticsColor;
	else if (person == _commander) return Type()->_commanderOpticsColor;
	return PackedBlack;
}

Texture *Transport::GetFlagTexture()
{
	if (_commander && _commander->GetFlagTexture()) return _commander->GetFlagTexture();
	if (_driver && _driver->GetFlagTexture()) return _driver->GetFlagTexture();
	if (_gunner && _gunner->GetFlagTexture()) return _gunner->GetFlagTexture();
	for( int i=0; i<_manCargo.Size(); i++ )
	{
		Person *cargo = _manCargo[i];
		if (cargo && cargo->GetFlagTexture()) return cargo->GetFlagTexture();
	}
	return NULL;
}

ManProxy::ManProxy()
{
	selection = -1;
}

int Transport::PassNum( int lod )
{
	if (GWorld->GetCameraType() == CamGunner && GWorld->CameraOn() == this)
	{
		AIUnit *unit = GWorld->FocusOn();
		Person *person = unit ? unit->GetPerson() : NULL;
		if (person)
		{
			LODShapeWithShadow *oShape = GetOpticsModel(person);
			if (oShape) return 3;
		}
	}
	return base::PassNum(lod);
}

void Transport::Draw( int level, ClipFlags clipFlags, const FrameBase &pos )
{
	if (level == LOD_INVISIBLE) return; // invisible LOD
#if _ENABLE_CHEATS
	if (CHECK_DIAG(DETransparent))
	{
		return;
	}
#endif

	// check if drawing some internal LOD
	// if yes, we need to clear z-buffer first

	bool zSpace = 
	(
		_shape->IsSpecLevel(level,VIEW_CARGO) ||
		_shape->IsSpecLevel(level,VIEW_GUNNER) ||
		_shape->IsSpecLevel(level,VIEW_COMMANDER) ||
		_shape->IsSpecLevel(level,VIEW_PILOT)
	);
	float oldCNear = 0;
	float oldCFar = 0;
	if (zSpace)
	{
		// before drawing inside view clear z-buffer
		//GlobalShowMessage(100,"clrZ");
		// set different clipping planes
		Camera *cam = GScene->GetCamera();
		oldCNear = cam->ClipNear();
		oldCFar = cam->ClipFar();
		cam->SetClipRange(0.01,50);
		GEngine->Clear(true,false);
	}

	// apply base
	base::Draw(level,clipFlags,pos);

	if (GWorld->GetCameraType() == CamGunner && GWorld->CameraOn() == this)
	{
		AIUnit *unit = GWorld->FocusOn();
		Person *person = unit ? unit->GetPerson() : NULL;
		if (person)
		{
			LODShapeWithShadow *oShape = GetOpticsModel(person);
			if (oShape) Draw2D(oShape, 0, GetOpticsColor(person));
		}
	}
	if (zSpace)
	{
		Camera *cam = GScene->GetCamera();
		cam->SetClipRange(oldCNear,oldCFar);
	}
}

void Transport::DrawDiags()
{
	#if _ENABLE_CHEATS
	LODShapeWithShadow *forceArrow=GScene->ForceArrow();

	if (CHECK_DIAG(DEPath))
	{
		LODShapeWithShadow *shape = GScene->Preloaded(SphereModel);
		PackedColor colorGetIn = PackedColor(Color(0,1,0,1));
		PackedColor colorGetOut = PackedColor(Color(1,0,0,1));
		if (CommanderBrain())
		{
			Vector3Val pos = GetCommanderGetOutPos(Commander());
			Ref<Object> obj = new ObjectColored(shape, -1);
			obj->SetPosition(pos);
			obj->SetConstantColor(colorGetOut);
			GScene->ObjectForDrawing(obj);
		}
		else if (GetCommanderAssigned())
		{
			Vector3Val pos = GetCommanderGetInPos(GetCommanderAssigned()->GetPerson(), GetCommanderAssigned()->GetPerson()->WorldPosition());
			Ref<Object> obj = new ObjectColored(shape, -1);
			obj->SetPosition(pos);
			obj->SetConstantColor(colorGetIn);
			GScene->ObjectForDrawing(obj);
		}
		if (DriverBrain())
		{
			Vector3Val pos = GetDriverGetOutPos(Driver());
			Ref<Object> obj = new ObjectColored(shape, -1);
			obj->SetPosition(pos);
			obj->SetConstantColor(colorGetOut);
			GScene->ObjectForDrawing(obj);
		}
		else if (GetDriverAssigned())
		{
			Vector3Val pos = GetDriverGetInPos(GetDriverAssigned()->GetPerson(), GetDriverAssigned()->GetPerson()->WorldPosition());
			Ref<Object> obj = new ObjectColored(shape, -1);
			obj->SetPosition(pos);
			obj->SetConstantColor(colorGetIn);
			GScene->ObjectForDrawing(obj);
		}
		if (GunnerBrain())
		{
			Vector3Val pos = GetGunnerGetOutPos(Gunner());
			Ref<Object> obj = new ObjectColored(shape, -1);
			obj->SetPosition(pos);
			obj->SetConstantColor(colorGetOut);
			GScene->ObjectForDrawing(obj);
		}
		else if (GetGunnerAssigned())
		{
			Vector3Val pos = GetGunnerGetInPos(GetGunnerAssigned()->GetPerson(), GetGunnerAssigned()->GetPerson()->WorldPosition());
			Ref<Object> obj = new ObjectColored(shape, -1);
			obj->SetPosition(pos);
			obj->SetConstantColor(colorGetIn);
			GScene->ObjectForDrawing(obj);
		}
		for (int i=0; i<_cargoAssigned.Size(); i++)
		{
			AIUnit *unit = _cargoAssigned[i];
			if (!unit) continue;
			if (unit->GetVehicleIn() == this)
			{
				Vector3Val pos = GetCargoGetOutPos(unit->GetPerson());
				Ref<Object> obj = new ObjectColored(shape, -1);
				obj->SetPosition(pos);
				obj->SetConstantColor(colorGetOut);
				GScene->ObjectForDrawing(obj);
			}
			else if (!unit->GetVehicleIn())
			{
				Vector3Val pos = GetCargoGetInPos(unit->GetPerson(), unit->GetPerson()->WorldPosition());
				Ref<Object> obj = new ObjectColored(shape, -1);
				obj->SetPosition(pos);
				obj->SetConstantColor(colorGetIn);
				GScene->ObjectForDrawing(obj);
			}
		}

		{
			{
				Matrix3 mat(MRotationY,-_azimutWanted);
				Ref<Object> arrow=new ObjectColored(forceArrow,-1);

				float size=0.15;
				arrow->SetPosition(Position()+VUp*1.0);
				arrow->SetOrient(mat.Direction(),VUp);
				arrow->SetPosition
				(
					arrow->PositionModelToWorld(forceArrow->BoundingCenter()*size)
				);
				arrow->SetScale(size);
				arrow->SetConstantColor(PackedColor(Color(1,1,0,0.5)));

				GScene->ObjectForDrawing(arrow);
			}
			{
				float dirWanted = atan2(_dirWanted.X(), _dirWanted.Z());
				Matrix3 mat(MRotationY,-dirWanted);
				Ref<Object> arrow=new ObjectColored(forceArrow,-1);

				float size=0.1;
				arrow->SetPosition(Position()+VUp*1.5);
				arrow->SetOrient(mat.Direction(),VUp);
				arrow->SetPosition
				(
					arrow->PositionModelToWorld(forceArrow->BoundingCenter()*size)
				);
				arrow->SetScale(size);
				arrow->SetConstantColor(PackedColor(Color(1,1,0,0.5)));

				GScene->ObjectForDrawing(arrow);
			}
		}
	}
	if (CHECK_DIAG(DEPath) && QIsManual())
	{
		{
			Ref<Object> arrow=new ObjectColored(forceArrow,-1);

			float size=1.0f;
			arrow->SetPosition(Position()+Direction()*10+VUp);
			arrow->SetOrient(_mouseDirWanted,VUp);
			arrow->SetPosition
			(
				arrow->PositionModelToWorld(forceArrow->BoundingCenter()*size)
			);
			arrow->SetScale(size);
			arrow->SetConstantColor(PackedColor(Color(0.7,1,0,0.5)));

			GScene->ObjectForDrawing(arrow);
		}
	}
	#endif
	base::DrawDiags();
}

void Transport::DrawCameraCockpit()
{
	AIUnit *unit = GWorld->FocusOn();
	if (!unit) return;
	Person *man = unit->GetPerson();
	if (!man) return;

	float alpha = 0;
	if (man == _commander)
		alpha = floatMin(_commanderHidden, 1.0 - _commanderHidden);
	else if (man == _driver)
		alpha = floatMin(_driverHidden, 1.0 - _driverHidden);
	else if (man == _gunner)
		alpha = floatMin(_gunnerHidden, 1.0 - _gunnerHidden);

	if (alpha <= 0) return;
	
	alpha *= 2; saturateMin(alpha, 1);
	PackedColor color = PackedColor(Color(0, 0, 0, alpha));
	const float w = GEngine->Width();
	const float h = GEngine->Height();
	MipInfo mip = GEngine->TextBank()->UseMipmap(NULL, 0, 0);
	GEngine->Draw2D(mip, color, Rect2DAbs(0, 0, w, h));
}

void Transport::DrawCrewMember
(
	int level,
	ClipFlags clipFlags,
	const Matrix4 &transform, const Matrix4 &invTransform,
	float dist2, float z2, const LightList &lights,
	const ManProxy &proxy, Person *man,
	bool hideWeapons
)
{
	//similiar to Object::DrawProxies

	// animate and draw driver
	LODShapeWithShadow *manShape=man->GetShape();

	Matrix4 proxyTransform=proxy.transform;

	// animate proxy matrix
	AnimateManProxyMatrix(level,proxy,proxyTransform);

	proxyTransform.SetPosition(proxyTransform.FastTransform(manShape->BoundingCenter()));
	Matrix4Val pTransform=transform * proxyTransform;

	int pLevel;
	bool insidePlayer = false;
	CameraType camType = GWorld->GetCameraType();
	if (camType != CamExternal && camType != CamGroup)
	{
		if (!GWorld->GetCameraEffect())
		{
			AIUnit *unit = GWorld->FocusOn();
			Person *player = unit ? unit->GetPerson() : NULL;
			insidePlayer = man == player;
		}
	}

	// LOD detection
	if (insidePlayer)
	{
		#if 0
		pLevel = man->InsideLOD(CamInternal);
		#else
		pLevel = 0; // select best external LOD
		#endif
	}
	else
	{
		pLevel = GScene->LevelFromDistance2
		(
			manShape,dist2,pTransform.Scale(),
			pTransform.Direction(),GScene->GetCamera()->Direction()
		);
	}
	if( pLevel!=LOD_INVISIBLE )
	{
		Matrix4Val invPTransform=pTransform.InverseScaled();

		FrameWithInverse pFrame(pTransform,invPTransform);
		man->ShowWeapons(!hideWeapons, false);
		if (insidePlayer) man->ShowHead(pLevel,false);
		man->ShowFlag(false);
		man->Draw(pLevel,clipFlags,pFrame);
		man->ShowFlag(true);
		if (insidePlayer) man->ShowHead(pLevel,true);
		man->ShowWeapons(true, true);
	}
}

int Transport::GetCrewMemberComplexity
(
	int level, const FrameBase &pos, float dist2,
	const ManProxy &proxy, Person *man
) const
{
	//similiar to Object::DrawProxies

	// animate and draw driver
	LODShapeWithShadow *manShape=man->GetShape();

	Matrix4 proxyTransform=proxy.transform;

	// animate proxy matrix
	AnimateManProxyMatrix(level,proxy,proxyTransform);

	proxyTransform.SetPosition(proxyTransform.FastTransform(manShape->BoundingCenter()));
	Matrix4Val pTransform = pos.Transform() * proxyTransform;

	int pLevel;
	bool insidePlayer = false;
	CameraType camType = GWorld->GetCameraType();
	if (camType != CamExternal && camType != CamGroup)
	{
		AIUnit *unit = GWorld->FocusOn();
		Person *player = unit ? unit->GetPerson() : NULL;
		insidePlayer = man == player;
	}

	// LOD detection
	if (insidePlayer)
	{
		#if 0
		pLevel = man->InsideLOD(CamInternal);
		#else
		pLevel = 0; // select best external LOD
		#endif
	}
	else
	{
		pLevel = GScene->LevelFromDistance2
		(
			manShape,dist2,pTransform.Scale(),
			pTransform.Direction(),GScene->GetCamera()->Direction()
		);
	}
	if( pLevel!=LOD_INVISIBLE )
	{
		return man->GetComplexity(pLevel,pos);
	}
	return 0;
}

bool Transport::CastProxyShadow(int level, int i) const
{
	const TransportType *type=Type();
	const LevelProxies &proxies=type->_proxies[level];
	// find corresponding proxy
	int cargoCount = proxies._cargoProxy.Size();
	const ManProxy *proxy = NULL;
	bool castShadow = false;
	Person *man = NULL;
	if (i<cargoCount)
	{
		proxy = &proxies._cargoProxy[i];
		if (i<_manCargo.Size())
		{
			man = _manCargo[i];
			castShadow = type->_castCargoShadow;
		}
	}
	else
	{
		i -= cargoCount;
		if (proxies._driverProxy.Present())
		{
			if (i==0 && !IsDriverHidden())
			{
				proxy = &proxies._driverProxy, man = _driver;
				castShadow = type->_castDriverShadow;
			}
			i--;
		}
		if (proxies._gunnerProxy.Present())
		{
			if (i==0 && !IsGunnerHidden())
			{
				proxy = &proxies._gunnerProxy, man = _gunner;
				castShadow = type->_castGunnerShadow;
			}
			i--;
		}
		if (proxies._commanderProxy.Present())
		{
			if (i==0 && !IsCommanderHidden())
			{
				proxy = &proxies._commanderProxy, man = _commander;
				castShadow = type->_castCommanderShadow;
			}
			i--;
		}
	}
	return proxy && man && castShadow;
}

int Transport::GetProxyCount(int level) const
{
	const TransportType *type=Type();
	const LevelProxies &proxies=type->_proxies[level];
	int count = proxies._cargoProxy.Size();
	if (proxies._driverProxy.Present()) count++;
	if (proxies._gunnerProxy.Present()) count++;
	if (proxies._commanderProxy.Present()) count++;
	return count;
}

Object *Transport::GetProxy
(
	LODShapeWithShadow *&shape,
	int level,
	Matrix4 &transform, Matrix4 &invTransform,
	const FrameBase &parentPos, int i
) const
{
	const TransportType *type=Type();
	// may return NULL
	const LevelProxies &proxies=type->_proxies[level];
	// find corresponding proxy
	int cargoCount = proxies._cargoProxy.Size();
	const ManProxy *proxy = NULL;
	Person *man = NULL;
	if (i<cargoCount)
	{
		proxy = &proxies._cargoProxy[i];
		if (i<_manCargo.Size())
		{
			man = _manCargo[i];
		}
	}
	else
	{
		i -= cargoCount;
		if (proxies._driverProxy.Present())
		{
			if (i==0 && !IsDriverHidden()) proxy = &proxies._driverProxy, man = _driver;
			i--;
		}
		if (proxies._gunnerProxy.Present())
		{
			if (i==0 && !IsGunnerHidden()) proxy = &proxies._gunnerProxy, man = _gunner;
			i--;
		}
		if (proxies._commanderProxy.Present())
		{
			if (i==0 && !IsCommanderHidden()) proxy = &proxies._commanderProxy, man = _commander;
			i--;
		}
	}
	if (!proxy || !man) return NULL;
	
	LODShapeWithShadow *manShape=man->GetShape();

	Matrix4 proxyTransform=proxy->transform;

	AnimateManProxyMatrix(level,*proxy,proxyTransform);

	proxyTransform.SetPosition(proxyTransform.FastTransform(manShape->BoundingCenter()));
	transform = parentPos.Transform()*proxyTransform;
	invTransform = transform.InverseScaled();

	shape = manShape;

	return man;
}

Matrix4 Transport::ProxyWorldTransform(const Object *obj) const
{
	// find proxy
	int level = 0;
	int n = GetProxyCount(level);
	for (int i=0; i<n; i++)
	{
		// check if it is the wanted proxy
		// problem: which level?
		Matrix4 trans = Transform();
		Matrix4 invTrans = GetInvTransform();
		LODShapeWithShadow *shape = NULL;
		Object *proxy = GetProxy(shape,level,trans,invTrans,*this,i);
		if (proxy==obj)
		{
			return trans;
		}
	}
	return Transform();
}
Matrix4 Transport::ProxyInvWorldTransform(const Object *obj) const
{
	// find proxy
	// TODO: seach not only level 0
	// man should be placed somewhere not to be dependant on level
	int level=0;
	int n = GetProxyCount(level);
	for (int i=0; i<n; i++)
	{
		// check if it is the wanted proxy
		// problem: which level?
		Matrix4 trans = Transform();
		Matrix4 invTrans = GetInvTransform();
		LODShapeWithShadow *shape = NULL;
		Object *proxy = GetProxy(shape,level,trans,invTrans,*this,i);
		if (proxy==obj)
		{
			return invTrans;
		}
	}
	return GetInvTransform();
}

/*!
\patch_internal 1.05 Date 7/18/2001 by Jirka
- Changed: enable drawing gunner in external camera even if turned in
\patch 1.85 Date 9/10/2002 by Jirka
- Added: CfgVehicles properties hideWeaponsDriver, hideWeaponsGunner, hideWeaponsCommander, hideWeaponsCargo
*/

void Transport::DrawProxies
(
	int level, ClipFlags clipFlags,
	const Matrix4 &transform, const Matrix4 &invTransform,
	float dist2, float z2, const LightList &lights
)
{

	bool external = !_shape->IsSpecLevel(level,VIEW_CARGO) && !_shape->IsSpecLevel(level,VIEW_PILOT) && !_shape->IsSpecLevel(level,VIEW_GUNNER);

	const TransportType *type=Type();
	const LevelProxies &proxies=type->_proxies[level];
	if (_driver && proxies._driverProxy.Present() && !(external && IsDriverHidden()))
	{
		DrawCrewMember
		(
			level,clipFlags,transform,invTransform,dist2,z2,lights,
			proxies._driverProxy,_driver,Type()->_hideWeaponsDriver
		);
	}
	// CHANGED
	if (_gunner && proxies._gunnerProxy.Present() && !(external && IsGunnerHidden() && !type->GetViewGunnerInExternal()))
	{
		DrawCrewMember
		(
			level,clipFlags,transform,invTransform,dist2,z2,lights,
			proxies._gunnerProxy,_gunner,Type()->_hideWeaponsGunner
		);
	}
	if (_commander && proxies._commanderProxy.Present()  && !(external && IsCommanderHidden()))
	{
		DrawCrewMember
		(
			level,clipFlags,transform,invTransform,dist2,z2,lights,
			proxies._commanderProxy,_commander,Type()->_hideWeaponsCommander
		);
	}
	for( int i=0; i<_manCargo.Size(); i++ )
	{
		Person *cargo=_manCargo[i];
		if (!cargo) continue;
		// if cargo position is not defined, do not draw
		if (i>=proxies._cargoProxy.Size()) break;
		if (!proxies._cargoProxy[i].Present()) break;
		DrawCrewMember
		(
			level,clipFlags,transform,invTransform,dist2,z2,lights,
			proxies._cargoProxy[i],cargo,Type()->_hideWeaponsCargo
		);
	}

	// draw missiles
	// check number of missiles
	int nMissiles = CountMissiles();


	Shape *sShape = _shape->LevelOpaque(level);
	for (int i=0; i<sShape->NProxies(); i++)
	{
		const ProxyObject &proxy = sShape->Proxy(i);
		Object *obj = proxy.obj;
		const EntityType *type = obj->GetVehicleType();
		if (!type) continue;
		if (!strcmp(type->_simName, "maverickweapon"))
		{
			if (proxy.id>nMissiles) continue;
			// smart clipping par of obj->Draw
			Matrix4Val pTransform = transform * obj->Transform();
			Matrix4Val invPTransform = proxy.invTransform * invTransform;

			// LOD detection
			// check weapon proxy
			LODShapeWithShadow *pshape = GetMissileShape();
			if (!pshape) continue;
			int level = GScene->LevelFromDistance2
			(
				pshape, dist2, pTransform.Scale(),
				pTransform.Direction(), GScene->GetCamera()->Direction()
			);
			if (level == LOD_INVISIBLE) continue;

			FrameWithInverse pFrame(pTransform, invPTransform);

			// construct FrameWithInverse from transform and invTransform

			LODShapeWithShadow *oldShape = obj->GetShape();
			obj->SetShape(pshape);
			obj->Draw(level, ClipAll, pFrame);
			obj->SetShape(oldShape);
		}
	}

	base::DrawProxies
	(
		level, clipFlags, transform, invTransform,
		dist2, z2, lights
	);

}

int Transport::GetProxyComplexity
(
	int level, const FrameBase &pos, float dist2
) const
{
	int nFaces = 0;

	bool external = !_shape->IsSpecLevel(level,VIEW_CARGO) && !_shape->IsSpecLevel(level,VIEW_PILOT);

	const TransportType *type=Type();
	const LevelProxies &proxies=type->_proxies[level];
	if (_driver && proxies._driverProxy.Present() && !(external && IsDriverHidden()))
	{
		nFaces += GetCrewMemberComplexity
		(
			level,pos,dist2,
			proxies._driverProxy,_driver
		);
	}
	if (_gunner && proxies._gunnerProxy.Present() && !(external && IsGunnerHidden()))
	{
		nFaces += GetCrewMemberComplexity
		(
			level,pos,dist2,
			proxies._gunnerProxy,_gunner
		);
	}
	if (_commander && proxies._commanderProxy.Present()  && !(external && IsCommanderHidden()))
	{
		nFaces += GetCrewMemberComplexity
		(
			level,pos,dist2,
			proxies._commanderProxy,_commander
		);
	}
	for( int i=0; i<_manCargo.Size(); i++ )
	{
		Person *cargo=_manCargo[i];
		if (!cargo) continue;
		// if cargo position is not defined, do not draw
		if (i>=proxies._cargoProxy.Size()) break;
		if (!proxies._cargoProxy[i].Present()) break;
		nFaces += GetCrewMemberComplexity
		(
			level,pos,dist2,
			proxies._cargoProxy[i],cargo
		);
	}

	int nMissiles = CountMissiles();

	Shape *sShape = _shape->LevelOpaque(level);
	for (int i=0; i<sShape->NProxies(); i++)
	{
		const ProxyObject &proxy = sShape->Proxy(i);
		Object *obj = proxy.obj;
		if (!obj) continue;
		const EntityType *type = obj->GetVehicleType();
		if (!type) continue;
		RString simulation = type->_simName;
		if (strcmp(simulation, "maverickweapon") == 0)
		{
			if (proxy.id>nMissiles) continue;
			// smart clipping par of obj->Draw
			// LOD detection
			Matrix4Val pTransform=pos.Transform()*proxy.obj->Transform();

			LODShapeWithShadow *pshape = obj->GetShapeOnPos(pTransform.Position());
			if (!pshape) continue;
			int level = GScene->LevelFromDistance2
			(
				pshape, dist2, pTransform.Scale(),
				pTransform.Direction(), GScene->GetCamera()->Direction()
			);
			if (level == LOD_INVISIBLE) continue;

			// construct FrameWithInverse from transform and invTransform
			Matrix4Val invPTransform=proxy.invTransform*pos.GetInvTransform();
			FrameWithInverse pFrame(pTransform,invPTransform);

			nFaces += obj->GetComplexity(level,pFrame);
		}
	}

	return nFaces;
}

Vector3 Transport::FindMissilePos(int index, bool &found) const
{
	found = false;
	Shape *sShape = _shape->LevelOpaque(0);
	for (int i=0; i<sShape->NProxies(); i++)
	{
		const ProxyObject &proxy = sShape->Proxy(i);
		Object *obj = proxy.obj;
		const EntityType *type = obj->GetVehicleType();
		if (!type) continue;
		if (strcmp(type->_simName, "maverickweapon")) continue;
		if (proxy.id!=index) continue;
		found = true;
		return proxy.obj->Position();
	}

	return VZero;
}

bool Transport::GetOpticsCamera(Matrix4 &transf, CameraType camType) const
{
	AIUnit *unit = GWorld->FocusOn();
	if (!unit) return false;

	Person *man = unit->GetPerson();
	if (!man) return false;

	const TransportType *type = Type();
	
	int index = -1;
	if (man == _driver)
	{
		index = type->_driverOpticsPos;
	}
	else if (man == _gunner)
	{
		index = type->_gunnerOpticsPos;
	}
	else if (man == _commander)
	{
		index = type->_commanderOpticsPos;
	}
/*
	else for (int i=0; i<_manCargo.Size(); i++)
	{
		if (i >= proxies._cargoProxy.Size()) break;
		if (!proxies._cargoProxy[i].Present()) continue;
		if (man == _manCargo[i])
		{
			proxy = &proxies._cargoProxy[i];
			break;
		}
	}
*/
	if (index < 0) return false;

	// animate
	Shape *memory = _shape->MemoryLevel();
	transf.SetPosition(memory->Pos(memory->NamedSel(index)[0]));
	transf.SetOrientation(M3Identity);
	AnimateMatrix(transf, _shape->FindMemoryLevel(), index);
	return true;
}

bool Transport::GetProxyCamera(Matrix4 &transf, CameraType camType) const
{
	AIUnit *unit = GWorld->FocusOn();
	if (!unit) return false;

	Person *man = unit->GetPerson();
	if (!man) return false;

	const TransportType *type = Type();
	int level = InsideLOD(camType);
	if (level == LOD_INVISIBLE) return false;
	const LevelProxies &proxies = type->_proxies[level];

	const ManProxy *proxy = NULL;
	if (man == _driver)
	{
		proxy = &proxies._driverProxy;
	}
	else if (man == _gunner)
	{
		proxy = &proxies._gunnerProxy;
	}
	else if (man == _commander)
	{
		proxy = &proxies._commanderProxy;
	}
	else for (int i=0; i<_manCargo.Size(); i++)
	{
		if (i >= proxies._cargoProxy.Size()) break;
		if (!proxies._cargoProxy[i].Present()) continue;
		if (man == _manCargo[i])
		{
			proxy = &proxies._cargoProxy[i];
			break;
		}
	}
	if (!proxy || !proxy->Present()) return false;

	// animate
	LODShapeWithShadow *manShape = man->GetShape();
	transf = proxy->transform;
	// TODO: 
	Vector3 pos = man->GetPilotPosition(camType);
	transf.SetPosition(transf.FastTransform(manShape->BoundingCenter()));
	transf.SetPosition(transf.FastTransform(pos));
	AnimateMatrix(transf,level,proxy->selection);
	return true;
}

float Transport::NeedsLoadFuel() const
{
	if (IsDammageDestroyed()) return 0;
	float max=GetType()->GetMaxFuelCargo();
	if( max<=0 ) return 0;
	return 1-GetFuelCargo()/max;	
}

float Transport::NeedsLoadAmmo() const
{
	if (IsDammageDestroyed()) return 0;
	float max=GetType()->GetMaxAmmoCargo();
	if( max<=0 ) return 0;
	return 1-GetAmmoCargo()/max;
}

float Transport::NeedsLoadRepair() const
{
	if (IsDammageDestroyed()) return 0;
	float max=GetType()->GetMaxRepairCargo();
	if( max<=0 ) return 0;
	return 1-GetRepairCargo()/max;	
}


bool Transport::IsPossibleToGetIn() const
{
	if( _isDead ) return false;
	if( _isUpsideDown ) return false;
	if (IsDammageDestroyed()) return false;
	//if( GetTotalDammage()>=MaxDammageWorking ) return false;
	return true;
}

bool Transport::IsWorking() const
{
	if( _isDead ) return false;
	if( _isUpsideDown ) return false;
	if (IsDammageDestroyed()) return false;
	//if( GetTotalDammage()>=MaxDammageWorking ) return false;
	return true;
}

bool Transport::IsAbleToMove() const
{
	// check if there is dead driver
	Person *driver = Driver();
	if (driver)
	{
		AIUnit *unit = driver->Brain();
		if (!unit && !_driverAssigned) return false;
	}
	// if vehicle has no fuel and should have some, it cannot move
	//if (GetFuel()<=0 && Type()->GetFuelCapacity()>0) return false;
	return IsWorking();
}
bool Transport::IsAbleToFire() const
{
	if (GetType()->NMagazines() == 0) return false;
	return IsWorking();
}

float Transport::CalculateTotalCost() const
{
	float cost = GetType()->GetCost();
	if (_driver) cost += _driver->GetType()->GetCost();
	if (_commander) cost += _commander->GetType()->GetCost();
	if (_gunner) cost += _gunner->GetType()->GetCost();
	for (int i=0; i<_manCargo.Size(); i++)
	{
		if (_manCargo[i]) cost += _manCargo[i]->GetType()->GetCost();
	}
	return cost;
}

bool Transport::QCanIBeIn( Person *who ) const
{
	if( !IsWorking() ) return false;
/*
	if( !who || who->QIsManual() )
	{
		// let player decide if its worth getting in
		return true;
	}
	else
	{
		// check IsAbleToFire?
		return IsAbleToMove();
	}
*/
	return true;
}

RString Transport::GetActionName(const UIAction &action)
{
	switch (action.type)
	{
		case ATMoveToDriver:
			if (GetType()->IsKindOf(GWorld->Preloaded(VTypeAir)))
				return LocalizeString(IDS_ACTION_TO_PILOT);
			else
				return LocalizeString(IDS_ACTION_TO_DRIVER);
		case ATMoveToGunner:
			return LocalizeString(IDS_ACTION_TO_GUNNER);
		case ATMoveToCommander:
			return LocalizeString(IDS_ACTION_TO_COMMANDER);
		case ATMoveToCargo:
			return LocalizeString(IDS_ACTION_TO_CARGO);
		case ATManualFire:
			if (_manualFire)
				return LocalizeString(IDS_ACTION_MANUAL_FIRE_CANCEL);
			else
				return LocalizeString(IDS_ACTION_MANUAL_FIRE);
	}
	return base::GetActionName(action);
}

typedef bool (AIUnit::*AssignF)(Transport *veh);


static void SwapPositions
(
	Transport *vehicle,
	Ref<Person> &p1, ManVehAction a1, bool cargo1, AssignF assign1,
	Ref<Person> &p2, ManVehAction a2, bool cargo2, AssignF assign2
)
{
	// switch animation state
	swap(p1,p2);
	if (p1) p1->SwitchVehicleAction(a1);
	if (p2) p2->SwitchVehicleAction(a2);
	AIUnit *unit1 = p1 ? p1->Brain() : NULL;
	AIUnit *unit2 = p2 ? p2->Brain() : NULL;
	if (unit1)
	{
		if (cargo1)
		{
			unit1->SetState(AIUnit::InCargo);
		}
		else if (unit1->GetState()==AIUnit::InCargo)
		{
			unit1->SetState(AIUnit::Wait);
			//unit1->ClearOperativePlan();
		}
		(unit1->*assign1)(vehicle);
	}
	if (unit2)
	{
		if (cargo2)
		{
			unit2->SetState(AIUnit::InCargo);
		}
		else if (unit2->GetState()==AIUnit::InCargo)
		{
			unit2->SetState(AIUnit::Wait);
			//unit2->ClearOperativePlan();
		}
		(unit2->*assign2)(vehicle);
	}
}

void Transport::ChangePosition(UIActionType type, Person *soldier)
{
	// check current unit position
	ManVehAction unitAct = ManVehActNone;
	bool unitCargo = false;
	AssignF unitAssign = &AIUnit::AssignAsCargo;

	Ref<Person> *unitPos = NULL;
	if (soldier==_driver)
	{
		unitPos = &_driver;
		unitAct = DriverAction();
		unitAssign = &AIUnit::AssignAsDriver;
		if (_driver->Brain() == CommanderUnit())
			_manualFire = false;
	}
	else if (soldier==_gunner)
	{
		unitPos = &_gunner;
		unitAct = GunnerAction();
		unitAssign = &AIUnit::AssignAsGunner;
	}
	else if (soldier==_commander)
	{
		unitPos = &_commander;
		unitAct = CommanderAction();
		unitAssign = &AIUnit::AssignAsCommander;
		if (_commander->Brain() == CommanderUnit())
			_manualFire = false;
	}
	else
	{
		// find if cargo
		for (int i=0; i<_manCargo.Size(); i++)
		{
			if (soldier==_manCargo[i])
			{
				unitPos = &_manCargo.Set(i).man, unitAct = CargoAction(i);
				unitCargo = true;
			}
		}
	}
	if (!unitPos) return;
	switch (type)
	{
		case ATMoveToDriver:
			SwapPositions
			(
				this,_driver,DriverAction(),false,&AIUnit::AssignAsDriver,
				*unitPos,unitAct,unitCargo,unitAssign
			);
			break;
		case ATMoveToGunner:
			SwapPositions
			(
				this,_gunner,GunnerAction(),false,&AIUnit::AssignAsGunner,
				*unitPos,unitAct,unitCargo,unitAssign
			);
			break;
		case ATMoveToCommander:
			SwapPositions
			(
				this,_commander,CommanderAction(),false,&AIUnit::AssignAsCommander,
				*unitPos,unitAct,unitCargo,unitAssign
			);
			break;
		case ATMoveToCargo:
			if (_manCargo.Size()>0)
			{
				// TODO: find suitable cargo position
				SwapPositions
				(	
					this,_manCargo.Set(0).man,CargoAction(0),true,&AIUnit::AssignAsCargo,
					*unitPos,unitAct,unitCargo,unitAssign
				);
			}
			break;
	}
}

void Transport::PerformAction(const UIAction &action, AIUnit *unit)
{
	// check if we are able to handle the action
	switch (action.type)
	{
	case ATMoveToDriver: case ATMoveToGunner:
	case ATMoveToCommander: case ATMoveToCargo:
		if (IsLocal())
			ChangePosition(action.type, unit->GetPerson());
		else
			GetNetworkManager().AskForChangePosition(unit->GetPerson(), this, action.type);
		return;
	case ATManualFire:
//		if (GWorld->GetMode() == GModeNetware) _manualFire = false;
		if (GunnerUnit() && GunnerUnit()->IsAnyPlayer()) _manualFire = false;
		else _manualFire = !_manualFire;
		return;
	case ATGetOut:
		unit->ProcessGetOut(false);
		return;
	case ATEngineOn:
		EngineOnAction();
		return;
	case ATEngineOff:
		EngineOffAction();
		return;
	case ATEject:
		Eject(unit);
		return;
	case ATLand:
		Land();
		return;
	case ATCancelLand:
		CancelLand();
		return;
	case ATTurnIn:
		HidePerson(unit->GetPerson(), 1.0);
		return;
	case ATTurnOut:
		HidePerson(unit->GetPerson(), 0.0);
		return;
	}
	// fall-back to parent
	base::PerformAction(action,unit);
}

/*!
\patch 1.27 Date 10/17/2001 by Jirka
- Fixed: Manual fire in MP enabled if gunner is not player
*/
void Transport::GetActions(UIActions &actions, AIUnit *unit, bool now)
{
	base::GetActions(actions, unit, now);
	if (!unit) return;

	if (unit->IsFreeSoldier())
	{
		if (unit->IsPlayer() && _lock == LSLocked) return;
		bool leader = _lock == LSUnlocked || unit->IsGroupLeader() || GWorld->GetMode() == GModeNetware;
		if (!leader && unit->VehicleAssigned() != this) return;
		AIGroup *grp = unit->GetGroup();
		Assert(grp);
		AICenter *center = grp->GetCenter();
		Assert(center);
		Person *person = unit->GetPerson();

		{
			Person *flagOwner = NULL;
			if (Commander() && Commander()->IsDammageDestroyed() && Commander()->GetFlagTexture())
			{
				flagOwner = Commander();
				goto FlagFound;
			}
			if (Driver() && Driver()->IsDammageDestroyed() && Driver()->GetFlagTexture())
			{
				flagOwner = Driver();
				goto FlagFound;
			}
			if (Gunner() && Gunner()->IsDammageDestroyed() && Gunner()->GetFlagTexture())
			{
				flagOwner = Gunner();
				goto FlagFound;
			}
			for (int i=0; i<GetManCargo().Size(); i++)
			{
				Person *man = GetManCargo()[i];
				if (man && man->IsDammageDestroyed() && man->GetFlagTexture())
				{
					flagOwner = man;
					goto FlagFound;
				}
			}
FlagFound:
			if (flagOwner)
			{
				EntityAI *flagCarrier = flagOwner->GetFlagCarrier();
				
				TargetSide side = flagCarrier->GetFlagSide();
				AIGroup *grp = unit->GetGroup();
				AICenter *center = grp ? grp->GetCenter() : NULL;
				if (center)
				{
					if (center->IsEnemy(side))
					{
						actions.Add(ATTakeFlag, flagOwner, 0.99, 0, true, true);
					}
					else if (center->IsFriendly(side))
					{
						actions.Add(ATReturnFlag, flagOwner, 0.99, 0, true, true);
					}
				}
			}
		}

		// avoid get in enemy vehicle
		if (Commander() && center->IsEnemy(Commander()->GetTargetSide())) return;
		if (Driver() && center->IsEnemy(Driver()->GetTargetSide())) return;
		if (Gunner() && center->IsEnemy(Gunner()->GetTargetSide())) return;
		for (int i=0; i<GetManCargo().Size(); i++)
		{
			Person *man = GetManCargo()[i];
			if (!man) continue;
			if (center->IsEnemy(man->GetTargetSide())) return;
		}

		float maxDist2 = 0;
		float invMaxDist2 = 0;
		if (now)
		{
			// check if stopped
			if (Speed().SquareSize() > Square(1.5)) return;
/*			
			// check if near
			float dist2 = Position().Distance2(person->Position());
			if (dist2 > Square(10)) return;
*/
			// check if in front
			Vector3Val relPos = person->PositionWorldToModel(Position());
			if (relPos.Z() <= 0) return;

			maxDist2 = Square(GetGetInRadius());
			invMaxDist2 = 1.0 / maxDist2;
		}

		if (leader)
		{
			if (QCanIGetIn(person))
			{
				if (now)
				{
					// check if near
					Vector3 pos = GetDriverGetInPos(person, person->Position());
					float dist2 = pos.Distance2(person->Position());
					if (dist2 <= maxDist2)
					{
						float priority = 0.900 - 0.3 * dist2 * invMaxDist2;
						actions.Add(ATGetInDriver, this, priority, 0, true);
					}
				}
				else
					actions.Add(ATGetInDriver, this, 0.9, 0, true);
			}
			if (QCanIGetInCommander(person))
			{
				if (now)
				{
					// check if near
					Vector3 pos = GetCommanderGetInPos(person, person->Position());
					float dist2 = pos.Distance2(person->Position());
					if (dist2 <= maxDist2)
					{
						float priority = 0.899 - 0.3 * dist2 * invMaxDist2;
						actions.Add(ATGetInCommander, this, priority, 0, true);
					}
				}
				else
					actions.Add(ATGetInCommander, this, 0.8, 0, true);
			}
			if (QCanIGetInGunner(person))
			{
				if (now)
				{
					// check if near
					Vector3 pos = GetGunnerGetInPos(person, person->Position());
					float dist2 = pos.Distance2(person->Position());
					if (dist2 <= maxDist2)
					{
						float priority = 0.898 - 0.3 * dist2 * invMaxDist2;
						actions.Add(ATGetInGunner, this, priority, 0, true);
					}
				}
				else
					actions.Add(ATGetInGunner, this, 0.7, 0, true);
			}
			if (QCanIGetInCargo(person))
			{
				if (now)
				{
					// check if near
					Vector3 pos = GetCargoGetInPos(person, person->Position());
					float dist2 = pos.Distance2(person->Position());
					if (dist2 <= maxDist2)
					{
						float priority = 0.897 - 0.3 * dist2 * invMaxDist2;
						actions.Add(ATGetInCargo, this, priority, 0, true);
					}
					else
					{
						// check also co-driver position
						Vector3 pos = GetCoDriverGetInPos(person, person->Position());
						float dist2 = pos.Distance2(person->Position());
						if (dist2 <= maxDist2)
						{
							float priority = 0.897 - 0.3 * dist2 * invMaxDist2;
							actions.Add(ATGetInCargo, this, priority, 0, true);
						}
					}
				}
				else
					actions.Add(ATGetInCargo, this, 0.6, 0, true);
			}
		}
		else
		{
			if (now)
			{
				// check if near
				Vector3 pos = GetUnitGetInPos(unit);
				float dist2 = pos.Distance2(person->Position());
				if (dist2 > maxDist2) return;
			}
			if (GetDriverAssigned() == unit)
			{
				if (QCanIGetIn(person))
					actions.Add(ATGetInDriver, this, 0.9, 0, true);
			}
			else if (GetCommanderAssigned() == unit)
			{
				if (QCanIGetInCommander(person))
					actions.Add(ATGetInCommander, this, 0.8, 0, true);
			}
			else if (GetGunnerAssigned() == unit)
			{
				if (QCanIGetInGunner(person))
					actions.Add(ATGetInGunner, this, 0.7, 0, true);
			}
			else // assigned in cargo
			{
				if (QCanIGetInCargo(person))
					actions.Add(ATGetInCargo, this, 0.6, 0, true);
			}		
		}
	}
	else if (unit->GetVehicleIn() == this)
	{
		if (!unit->IsPlayer() || GetLock() != LSLocked /*|| unit->VehicleAssigned() != this*/)
		{
			if (IsStopped())
			{
				actions.Add(ATGetOut, this, 0.9, 0, true);

				// check if I can change position
				if
				(
					_effCommander && unit==_effCommander->Brain() &&
					(IsPersonHidden(unit->GetPerson()) || unit->IsInCargo())
				)
				{
					bool leader = unit->IsGroupLeader() || GWorld->GetMode() == GModeNetware;
					if (unit!=PilotUnit() && Type()->_hasDriver && (leader || _driverAssigned == unit))
					{
						actions.Add(ATMoveToDriver, this, 0.04);
					}
					if (unit!=ObserverUnit() && Type()->_hasCommander && (leader || _commanderAssigned == unit))
					{
						actions.Add(ATMoveToCommander, this, 0.03);
					}
					if (unit!=GunnerUnit() && Type()->_hasGunner && (leader || _gunnerAssigned == unit))
					{
						actions.Add(ATMoveToGunner, this, 0.02);
					}
					bool enabled = leader;
					if (!enabled)
					{
						for (int i=0; i<_cargoAssigned.Size(); i++)
							if (_cargoAssigned[i] == unit)
							{
								enabled = true; break;
							}
					}
					if (_manCargo.Size()>0 && !unit->IsInCargo() && enabled)
					{
						actions.Add(ATMoveToCargo, this, 0.01);
					}
				}
			}
			else
			{
				actions.Add(ATEject, this, 0.05);
			}
		}
		if (PilotUnit() == unit && GetFuel()>0)
		{
			if (EngineIsOn())
			{
				actions.Add(ATEngineOff, this, 0.1);
			}
			else
			{
				actions.Add(ATEngineOn, this, 0.1);
			}
		}
		if (unit->IsPlayer())
		{
			if
			(
				Type()->HasGunner() && CommanderUnit() == unit &&
				(unit->GetPerson() == _commander || unit->GetPerson() == _driver) &&
//				GWorld->GetMode() != GModeNetware
				(!GunnerUnit() || !GunnerUnit()->IsAnyPlayer())
			)
			{
				actions.Add(ATManualFire, this, 0.59);
			}

			if (Type()->_hideProxyInCombat)
			{
				Person *person = unit->GetPerson();
				bool turn = false;
				bool out = false;
				if (person == Commander())
				{
					turn = !Type()->_forceHideCommander;
					out = IsCommanderHidden();
				}
				else if (person == Driver())
				{
					turn = !Type()->_forceHideDriver;
					out = IsDriverHidden();
				}
				else if (person == Gunner())
				{
					turn = !Type()->_forceHideGunner;
					out = IsGunnerHidden();
				}
				if (turn)
				{
					if (out)
						actions.Add(ATTurnOut, this, 0.6);
					else
						actions.Add(ATTurnIn, this, 0.95);
				}
			}
		}

	}
}

bool Transport::QCanIGetIn( Person *who ) const
{
	if( !QCanIBeIn(who) ) return false;
	if (!Type()->HasDriver()) return false;
	return _driver==NULL || _driver->IsDammageDestroyed();
}

bool Transport::QCanIGetInGunner( Person *who ) const
{
	if( !QCanIBeIn(who) ) return false;
	if (!Type()->HasGunner()) return false;
	return _gunner==NULL || _gunner->IsDammageDestroyed();
}

bool Transport::QCanIGetInCommander( Person *who ) const
{
	if( !QCanIBeIn(who) ) return false;
	if (!Type()->HasCommander()) return false;
	return _commander==NULL || _commander->IsDammageDestroyed();
}

bool Transport::QCanIGetInCargo( Person *who ) const
{
	if( !QCanIBeIn(who) ) return false;
	if (GetFreeManCargo() > 0) return true;

	// try to find some dead body
	for (int i=0; i<_manCargo.Size(); i++)
	{
		if (_manCargo[i] && _manCargo[i]->IsDammageDestroyed()) return true;
	}
	return false;
}

bool Transport::QCanIGetInAny( Person *who ) const
{
	return QCanIGetIn(who) ||
		QCanIGetInGunner(who) ||
		QCanIGetInCommander(who) ||
		QCanIGetInCargo(who);
}

void Transport::GetInStarted( AIUnit *unit )
{
	// TODO: BUG Assert failed
	// Assert( _getinUnits.Find(unit)<0 );
	if( _getinUnits.Find(unit)<0 ) _getinUnits.Add(unit);
}
void Transport::GetOutStarted( AIUnit *unit )
{
	Assert( _getoutUnits.Find(unit)<0 );
	if( _getoutUnits.Find(unit)<0 ) _getoutUnits.Add(unit);
}

void Transport::GetInFinished( AIUnit *unit )
{
	// done/canceled
	for (int i=0; i<_getinUnits.Size(); i++)
	{
		if( unit == _getinUnits[i] )
		{
			_getinUnits.Delete(i);
			return;
		}
	}
	// TODO: Manual GetIn
	//Fail("Getin finished and never started.");
}
void Transport::GetOutFinished( AIUnit *unit )
{
	// done/canceled
	for (int i=0; i<_getoutUnits.Size(); i++)
	{
		if( unit == _getoutUnits[i] )
		{
			_getoutUnits.Delete(i);
			return;
		}
	}
	// TODO: Manual GetOut
	//Fail("Getout finished and never started.");
}

void Transport::WaitForGetIn( AIUnit *unit)
{
	if (unit->GetVehicle() == this) return;
	
	AIUnit *brain = PilotUnit();
	if (brain)
	{
		if (brain->GetState() == AIUnit::Stopped)
		{
			// Calculate cost from starting field
			int x = toIntFloor(unit->Position().X() * InvLandGrid);
			int z = toIntFloor(unit->Position().Z() * InvLandGrid);
			GeographyInfo geogr = GLOB_LAND->GetGeography(x, z);
			float dist = (Position() - unit->Position()).SizeXZ();
			Time time = Glob.time + 2.0 * unit->GetVehicle()->GetCost(geogr) * dist + 30.0;
			if (time > GetGetInTimeout()) SetGetInTimeout(time);
		}
		else
		{
			SetGetInTimeout(TIME_MAX);
			if (brain->GetState() != AIUnit::Stopping)
				Verify(brain->SetState(AIUnit::Stopping));
		}
	}

	GetInStarted(unit);
	
#if 0
LogF
(
	"%.2f: Vehicle %s, wait for get in %s until %.2f",
	Glob.time.toFloat(),
	(const char *)veh->GetDebugName(),
	(const char *)unit->GetDebugName(),
	time.toFloat()
);
#endif
}

void Transport::WaitForGetOut(AIUnit *unit)
{
	AIUnit *brain = PilotUnit();

	GetOutStarted(unit);	
	if (brain && brain->GetState() != AIUnit::Stopping && brain->GetState() != AIUnit::Stopped)
	{
		Verify(brain->SetState(AIUnit::Stopping));
	}
}

void Transport::LandStarted(LandingMode landing)
{
	_landing = landing;
	AIUnit *brain = PilotUnit();
	if (!brain) return;
	if (brain->GetState() != AIUnit::Stopping && brain->GetState() != AIUnit::Stopped)
	{
		Verify(brain->SetState(AIUnit::Stopping));
	}
}

bool Transport::IsStopped() const
{
	AIUnit *unitInto = PilotUnit();
	if (!unitInto ) return true;
	if( !unitInto->IsPlayer() )
	{
		// Stopped may timeout - test for Stopping
		if( unitInto->GetState()== AIUnit::Stopping) return false;
		if( unitInto->GetState()== AIUnit::Stopped) return true;
	}
	return Speed().SquareSize()<Square(0.5); 
}

bool Transport::CanCancelStop() const
{
	if (_landing != LMNone) return false;
	if (!base::CanCancelStop()) return false;
	if
	(
		WhoIsGettingIn().Count() == 0 &&
		WhoIsGettingOut().Count() == 0
	)
	{
		_getinUnits.Clear();
		_getoutUnits.Clear();
		return true;
	}
	return false;
}

void VehicleSupply::UpdateStop()
{
	// if vehicle can be moving, stop waiting
	if( CanCancelStop() )
	{
		AIUnit *brain=PilotUnit();
		if( brain )
		{
			if( brain && (brain->GetState()==AIUnit::Stopping || brain->GetState()==AIUnit::Stopped) )
			{
				brain->SetState(AIUnit::Wait);	// valid pass Stopped -> Wait
			}
		}
	}
}

Vector3 Transport::GetStopPosition() const
{
	// predict stop position based on current speed
	float estT=2;
	return Position()+Speed()*estT+0.5*estT*estT*Acceleration();
}

/*!
\patch_internal 1.27 Date 10/17/2001 by Ondra
- Changed: ResetMovement moved after SetVehicleIn.
Required because new testing of IsInLandscape in ResetMovement.
\patch 1.27 Date 10/17/2001 by Ondra
- Fixed: speed of vehicle not kept after jumping out (ejecting) from vehicle.
\patch 1.82 Date 8/9/2002 by Ondra
- Fixed: MP: Problems with get-in very quickly followed by get-out.
*/

void Transport::GetOutAny
(
	const Matrix4 &outPos, Person *soldier, bool sound, const char *name
)
{
	Assert( soldier );
	//soldier->ForgetMove();

	DoAssert (GWorld->ValidateOutVehicle(soldier));
	DoAssert (ValidateCrew(soldier));

	if (soldier->IsInLandscape())
	{
		RptF
		(
			"GetOutAny soldier %s already in landscape",
			(const char *)soldier->GetDebugName()
		);
	}
	#if 0
	LogF
	(
		"%s moved from %s into landscape",
		(const char *)soldier->GetDebugName(),
		(const char *)GetDebugName()
	);
	#endif
	if (soldier->IsMoveOutInProgress())
	{
		RptF("%s: Getting out while IsMoveOutInProgress", (const char *)soldier->GetDebugName());
		soldier->CancelMoveOutInProgress();
		soldier->Move(outPos);
		soldier->Init(outPos);
	}
	else
	{
		soldier->SetTransform(outPos);
		soldier->Init(outPos);
		soldier->ResetMoveOut(); // mark it is in landscape
		GLOB_WORLD->AddVehicle(soldier);
		GLOB_WORLD->RemoveOutVehicle(soldier);
	}

	// lock position of empty vehicles
	LockPosition();

	if( soldier->Brain() )
	{
		soldier->Brain()->SetVehicleIn(NULL);
	}
	soldier->ResetMovement(2.5, Type()->_getOutAction);
	soldier->SetSpeed(_speed);

	if (!_doorSound && sound) 
	{
		const SoundPars &pars=GetType()->GetGetOutSound();
		_doorSound=GSoundScene->OpenAndPlayOnce
		(
			pars.name,Position(),Speed(),pars.vol
		);
		if( _doorSound )
		{
			GSoundScene->SimulateSpeedOfSound(_doorSound);
			GSoundScene->AddSound(_doorSound);
		}
	}
	if (sound) OnEvent(EEGetOut,name,soldier);
}


void Transport::GetInAny(Person *soldier, bool sound, const char *name)
{
	// check if soldier is in some vehicle
	// if yes, get him out before gettin in "this"
	AIUnit *unit=soldier->Brain();
	if (unit) 
	{
		Transport *in = unit->GetVehicleIn();
		Assert (in!=this);
		if (in)
		{
			unit->DoGetOut(in,false);
			#if 0
			LogF
			(
				"Unit %s forced out from %s",
				(const char *)unit->GetDebugName(),
				(const char *)GetDebugName()
			);
			#endif
		}
	}
	
	soldier->UnlockPosition();
	if (soldier->IsInLandscape())
	{
		soldier->SetMoveOut(this); // remove reference from the world
	}
	else
	{
		Assert(!soldier->IsMoveOutInProgress());
		Assert(soldier->GetHierachyParent()==this);
	}
	#if 0
	LogF
	(
		"%s will move into %s",
		(const char *)soldier->GetDebugName(),
		(const char *)GetDebugName()
	);
	#endif
	soldier->SetPilotLight(false);
	soldier->SwitchLight(false);
	if (unit)
	{
		unit->SetVehicleIn(this);
	}
	
	if (!_doorSound && sound) 
	{
		const SoundPars &pars=GetType()->GetGetInSound();
		_doorSound=GSoundScene->OpenAndPlayOnce
		(
			pars.name,Position(),Speed(),pars.vol
		);
		if( _doorSound )
		{
			GSoundScene->SimulateSpeedOfSound(_doorSound);
			GSoundScene->AddSound(_doorSound);
		}
	}
	if (sound) OnEvent(EEGetIn,name,soldier);
}


void Transport::GetInDriver(Person *driver, bool sound)
{
	if (_driver)
	{
		DoAssert(_driver->IsDammageDestroyed());

		Vector3 pos = Type()->GetDriverGetInPos(0);
		Matrix4 transform;
		PositionModelToWorld(pos, pos);
		transform.SetPosition(pos);
		transform.SetUpAndDirection(VUp, pos - Position());

		GetOutDriver(transform, false);		
	}

	_driver=driver;
	if (!_commander) SetTargetSide(_driver->GetTargetSide());
	if( driver==GLOB_WORLD->CameraOn() )
	{
		GLOB_WORLD->SwitchCameraTo(this,GLOB_WORLD->GetCameraType());
	}
	/*
	EngineOn();
	*/
	GetInAny(driver,sound,"driver");
	_driverHiddenWanted = _driverHidden = Type()->_hideProxyInCombat;
	driver->SwitchVehicleAction(DriverAction());

	if (!_commander && (Type()->_driverIsCommander || !_gunner) )
	{
		// driver will sometimes become effective commander
		_effCommander = driver;
		SetTargetSide(driver->GetTargetSide());
	}
}

int Transport::GetManCargoSize() const
{
	int n = 0;
	for (int i=0; i<_manCargo.Size(); i++)
	{
		if (_manCargo[i]) n++;
	}
	return n;
}

int Transport::GetFreeManCargo() const
{
	return Type()->_maxManCargo - GetManCargoSize();
}

void Transport::GetOutDriver(const Matrix4 &outPos, bool sound)
{
	if( !_driver ) return;
	GetOutAny(outPos,_driver,sound,"driver");
	EngineOff();
	GWorld->VehicleSwitched(this,_driver);
	if (_driver->Brain() == CommanderUnit())
		_manualFire = false;
	_driver=NULL;
}

void Transport::GetInCargo(Person *soldier, bool sound, int index)
{
	// if position is not specified, search for first free one
	if (index<0)
	{
		for (int i=0; i<_manCargo.Size(); i++)
		{
			if (_manCargo[i] == NULL || _manCargo[i]->IsDammageDestroyed())
			{
				index = i;
				break;
			}
		}
		if (index < 0)
		{
			Fail("No free position in cargo");
			return;
		}
	}

	if (_manCargo[index])
	{
		bool coDriver = false;
		if (index>=Type()->_cargoCoDriver.Size())
		{
			coDriver = Type()->_cargoCoDriver[Type()->_cargoCoDriver.Size()-1];
		}
		else
		{
			coDriver = Type()->_cargoCoDriver[index];
		}
		// check if cargo or co-driver
		Vector3 pos = coDriver ? Type()->GetCoDriverGetInPos(0) : Type()->GetCargoGetInPos(0);
		Matrix4 transform;
		PositionModelToWorld(pos, pos);
		transform.SetPosition(pos);
		transform.SetUpAndDirection(VUp, pos - Position());

		GetOutCargo(_manCargo[index], transform, false);		
	}

	_manCargo.Set(index).man = soldier;
	GetInAny(soldier,sound,"cargo");
	soldier->SwitchVehicleAction(CargoAction(index));
	GWorld->VehicleSwitched(soldier,this);
	if (!_driver && !_gunner && !_commander)
	{
		// we become effective commnader
		_effCommander = soldier;
		SetTargetSide(soldier->GetTargetSide());
	}
}

void Transport::GetOutCargo(Person *cargo, const Matrix4 &outPos, bool sound)
{
	GetOutAny(outPos,cargo,sound,"cargo");
	for (int i=0; i<_manCargo.Size(); i++)
	{
		if (_manCargo[i] == cargo)
		{
			_manCargo.Set(i).man = NULL;
			return;
		}
	}
	Fail("Unit is not in cargo");
}

void Transport::GetInGunner(Person *soldier, bool sound)
{
	if (_gunner)
	{
		DoAssert(_gunner->IsDammageDestroyed());

		Vector3 pos = Type()->GetGunnerGetInPos(0);
		Matrix4 transform;
		PositionModelToWorld(pos, pos);
		transform.SetPosition(pos);
		transform.SetUpAndDirection(VUp, pos - Position());

		GetOutGunner(transform, false);		
	}

	_gunner=soldier;
	_gunnerHiddenWanted = _gunnerHidden = Type()->_hideProxyInCombat;
	GetInAny(soldier,sound,"gunner");
	soldier->SwitchVehicleAction(GunnerAction());
	GWorld->VehicleSwitched(soldier,this);

	if (!_commander && (!Type()->_driverIsCommander || !_driver))
	{
		// gunner will sometimes become effective commander
		_effCommander = soldier;
		SetTargetSide(soldier->GetTargetSide());
	}

	// disable manual fire in MP when player gunner gets in
	if (GunnerUnit() && GunnerUnit()->IsAnyPlayer()) _manualFire = false;
}

void Transport::GetOutGunner(const Matrix4 &outPos, bool sound)
{
	if( !_gunner ) return;
	GetOutAny(outPos,_gunner,sound,"gunner");
	_gunner=NULL;
}

/*!
\patch 1.28 Date 10/30/2001 by Ondra
- Fixed: MP character animation in cargo (looking through roof).
*/

void Transport::GetInCommander(Person *soldier, bool sound)
{
	if (_commander)
	{
		DoAssert(_commander->IsDammageDestroyed());

		Vector3 pos = Type()->GetCommanderGetInPos(0);
		Matrix4 transform;
		PositionModelToWorld(pos, pos);
		transform.SetPosition(pos);
		transform.SetUpAndDirection(VUp, pos - Position());

		GetOutCommander(transform, false);		
	}

	_commander=soldier;
	SetTargetSide(_commander->GetTargetSide());
	_commanderHiddenWanted = _commanderHidden = Type()->_hideProxyInCombat;
	GetInAny(soldier,sound,"commander");
	soldier->SwitchVehicleAction(CommanderAction());
	GWorld->VehicleSwitched(soldier,this);

	// commander will always become effective commander
	_effCommander = soldier;
	SetTargetSide(soldier->GetTargetSide());
}

void Transport::GetOutCommander(const Matrix4 &outPos, bool sound)
{
	if( !_commander ) return;
	GetOutAny(outPos,_commander,sound,"commander");
	if (_commander->Brain() == CommanderUnit())
		_manualFire = false;
	_commander=NULL;
}

void Transport::Eject(AIUnit *unit)
{
	// TODO: create parachute if necessary
	unit->ProcessGetOut(false);
}

void Transport::Land()
{
}

void Transport::CancelLand()
{
}

Vector3 Transport::GetUnitGetInPos(AIUnit *unit)
{
	Assert(unit);
	Person *person = unit->GetPerson();
	Vector3Val unitPos = person->Position();
	if (GetDriverAssigned() == unit)
		return GetDriverGetInPos(person, unitPos);
	else if (GetCommanderAssigned() == unit)
		return GetCommanderGetInPos(person, unitPos);
	else if (GetGunnerAssigned() == unit)
		return GetGunnerGetInPos(person, unitPos);
	else
		return GetCargoGetInPos(person, unitPos);
}

Vector3 Transport::GetUnitGetOutPos(AIUnit *unit)
{
	Assert(unit);
	Person *person = unit->GetPerson();
	if (Driver() == person)
		return GetDriverGetOutPos(person);
	else if (Commander() == person)
		return GetCommanderGetOutPos(person);
	else if (Gunner() == person)
		return GetGunnerGetOutPos(person);
	else
		return GetCargoGetOutPos(person);
}

Vector3 Transport::GetDriverGetInPos(Person *person, Vector3Par pos) const
{
	Vector3Val unitPos = pos;
	Vector3 bestPos = VZero;
	float minDist2 = FLT_MAX;
	int n = Type()->NDriverGetInPos();
	for (int i=0; i<n; i++)
	{
		Vector3 pos = Type()->GetDriverGetInPos(i);
		PositionModelToWorld(pos, pos);
		float dist2 = pos.Distance2(unitPos);
		if (dist2 < minDist2)
		{
			minDist2 = dist2;
			bestPos = pos;
		}
	}
	return bestPos;
}

Vector3 Transport::GetCommanderGetInPos(Person *person, Vector3Par pos) const
{
	Vector3Val unitPos = pos;
	Vector3 bestPos = VZero;
	float minDist2 = FLT_MAX;
	int n = Type()->NCommanderGetInPos();
	for (int i=0; i<n; i++)
	{
		Vector3 pos = Type()->GetCommanderGetInPos(i);
		PositionModelToWorld(pos, pos);
		float dist2 = pos.Distance2(unitPos);
		if (dist2 < minDist2)
		{
			minDist2 = dist2;
			bestPos = pos;
		}
	}
	return bestPos;
}

Vector3 Transport::GetGunnerGetInPos(Person *person, Vector3Par pos) const
{
	Vector3Val unitPos = pos;
	Vector3 bestPos = VZero;
	float minDist2 = FLT_MAX;
	int n = Type()->NGunnerGetInPos();
	for (int i=0; i<n; i++)
	{
		Vector3 pos = Type()->GetGunnerGetInPos(i);
		PositionModelToWorld(pos, pos);
		float dist2 = pos.Distance2(unitPos);
		if (dist2 < minDist2)
		{
			minDist2 = dist2;
			bestPos = pos;
		}
	}
	return bestPos;
}

Vector3 Transport::GetCargoGetInPos(Person *person, Vector3Par pos) const
{
	Vector3Val unitPos = pos; // person->Position();
	Vector3 bestPos = VZero;
	float minDist2 = FLT_MAX;
	int n = Type()->NCargoGetInPos();
	for (int i=0; i<n; i++)
	{
		Vector3 pos = Type()->GetCargoGetInPos(i);
		PositionModelToWorld(pos, pos);
		float dist2 = pos.Distance2(unitPos);
		if (dist2 < minDist2)
		{
			minDist2 = dist2;
			bestPos = pos;
		}
	}
	return bestPos;
}

Vector3 Transport::GetCoDriverGetInPos(Person *person, Vector3Par pos) const
{
	Vector3Val unitPos = pos; // person->Position();
	Vector3 bestPos = VZero;
	float minDist2 = FLT_MAX;
	int n = Type()->NCoDriverGetInPos();
	for (int i=0; i<n; i++)
	{
		Vector3 pos = Type()->GetCoDriverGetInPos(i);
		PositionModelToWorld(pos, pos);
		float dist2 = pos.Distance2(unitPos);
		if (dist2 < minDist2)
		{
			minDist2 = dist2;
			bestPos = pos;
		}
	}
	return bestPos;
}

Vector3 Transport::GetDriverGetOutPos(Person *person) const
{
	Vector3Val pos = person->WorldPosition();
	return GetDriverGetInPos(person, pos);
}
Vector3 Transport::GetCommanderGetOutPos(Person *person) const
{
	Vector3Val pos = person->WorldPosition();
	return GetCommanderGetInPos(person, pos);
}
Vector3 Transport::GetGunnerGetOutPos(Person *person) const
{
	Vector3Val pos = person->WorldPosition();
	return GetGunnerGetInPos(person, pos);
}

Vector3 Transport::GetCargoGetOutPos(Person *person) const
{
	Vector3Val pos = person->WorldPosition();

	// check which cargo slot are we in
	int index = -1;
	for (int i=0; i<_manCargo.Size(); i++)
	{
		if (person == _manCargo[i])
		{
			index = i;
			break;
		}
	}
	if (index < 0)
	{
		// no slot: fall back
		return GetCargoGetInPos(person, pos);
	}
	// check which array should be used
	// check if we should use co-driver or cargo position
	bool coDriver = false;
	if (index >= Type()->_cargoCoDriver.Size())
	{
		coDriver = Type()->_cargoCoDriver[Type()->_cargoCoDriver.Size() - 1];
	}
	else
	{
		coDriver = Type()->_cargoCoDriver[index];
	}
	if (coDriver)
	{
		return GetCoDriverGetInPos(person, pos);
	}
	else
	{
		return GetCargoGetInPos(person, pos);
	}

}

void Transport::PerformFF( FFEffects &effects )
{
	base::PerformFF(effects);

	float vol,freq;
	vol=GetEngineVol(freq);

	effects.engineMag = vol*0.09f;
	effects.engineFreq = freq*50;
}

/*!
\patch 1.24 Date 09/26/2001 by Ondra
- Fixed: Speed of sound delay was missing on vehicle crash sound.
*/
void Transport::Sound( bool inside, float deltaT )
{
	float vol,freq;
	vol=GetEngineVol(freq);
	if( freq>0.01 && vol>0.01 )
	{
		const SoundPars &sound=GetType()->GetMainSound();
		if( !_engineSound && sound.name.GetLength()>0)
		{
			_engineSound=GSoundScene->OpenAndPlay
			(
				sound.name,PositionModelToWorld(GetEnginePos()),Speed()
			);
		}
		if( _engineSound )
		{
			freq*=sound.freq;
			vol*=sound.vol;
			if (inside) vol *= Type()->_insideSoundCoef;
			_engineSound->SetVolume(vol,freq); // volume, frequency
			_engineSound->SetPosition(PositionModelToWorld(GetEnvironPos()),Speed());
			_engineSound->Set3D(!inside);
		}
	}
	else
	{
		_engineSound.Free();
	}

	vol=GetEnvironVol(freq);
	if( freq>0.01 && vol>0.01 )
	{
		const SoundPars &sound=GetType()->GetEnvSound();
		if( !_envSound && sound.name.GetLength()>0 )
		{
			_envSound=GSoundScene->OpenAndPlay
			(
				sound.name,Position(),Speed()
			);
		}
		if( _envSound )
		{
			freq*=sound.freq;
			vol*=sound.vol;
			if (inside) vol *= Type()->_insideSoundCoef;
			_envSound->SetVolume(vol,freq); // volume, frequency
			_envSound->SetPosition(Position(),Speed());
			_envSound->Set3D(!inside);
		}
	}
	else
	{
		_envSound.Free();
	}

	if (_showDmg)
	{
		const SoundPars &sound=GetType()->GetDmgSound();
		if (!_dmgSound && sound.name.GetLength() > 0)
		{
			_dmgSound = GSoundScene->OpenAndPlay
			(
				sound.name, PositionModelToWorld(GetType()->_showDmgPoint), Speed()
			);
		}
		if (_dmgSound)
		{
			freq=sound.freq;
			vol=sound.vol;
			_dmgSound->Set3D(!inside);
			_dmgSound->SetVolume(vol,freq); // volume, frequency
			_dmgSound->SetPosition(PositionModelToWorld(GetType()->_showDmgPoint),Speed());
		}
	}
	else
	{
		_dmgSound.Free();
	}

	if( _doorSound )
	{
		const SoundPars &pars=GetType()->GetGetOutSound();
		float vol=pars.vol;
		_doorSound->SetVolume(vol); // volume, frequency
		_doorSound->Set3D(!inside);
		_doorSound->SetPosition(Position(),Speed());
	}

	if( _doCrash!=CrashNone && Glob.time>_timeCrash+1.0 )
	{
		_timeCrash=Glob.time;
		const SoundPars *pars=NULL;
		switch( _doCrash )
		{
			case CrashObject:
				pars=&GetType()->GetCrashSound();
			break;
			case CrashLand:
				pars=&GetType()->GetLandCrashSound();
			break;
			case CrashWater:
				pars=&GetType()->GetWaterCrashSound();
			break;
		}
		if( pars )
		{
			float volume=pars->vol*_crashVolume;
			float freq=pars->freq;
			AbstractWave *sound=GSoundScene->OpenAndPlayOnce
			(
				pars->name,Position(),Speed(),volume,freq
			);
			if( sound )
			{
				GSoundScene->SimulateSpeedOfSound(sound);
				GSoundScene->AddSound(sound);
			}
		}
	}
	_doCrash=CrashNone;
	base::Sound(inside,deltaT);
}

void Transport::UnloadSound()
{
	_engineSound.Free();
	_envSound.Free();
	_dmgSound.Free();
	base::UnloadSound();
}

bool Transport::IsFireEnabled()
{
	// who is firing
	if (Type()->HasGunner())
	{
		if (!EffectiveGunnerUnit()) return false;
	}
	else
	{
		if (!PilotUnit()) return false;
	}

/*		
	// who is commanding
	if (GetType()->HasCommander())
	{
		if (!Commander()) return false;
		if (Commander() == GWorld->PlayerOn()) return _fireEnabled;
	}
	// else commanding == firing
*/
	return true;
}

#define DIAG_SPEED 0

#include "dikCodes.h"

bool Transport::StopAtStrategicPos() const
{
	switch (_moveMode)
	{
		case VMMBackward:
		case VMMSlowForward:
		case VMMForward:
		case VMMFastForward:
			return false;
		default:
			return true;
	}
}

float Transport::CalculateDriverPos(float &headChange, float minDist)
{
	const float reactionTime = DriverReactionTime;
	float speedCoef = 1.0f;
	switch (_moveMode)
	{
		case VMMFormation:
			Fail("No command");
			return speedCoef;
		case VMMMove:
			break;
		case VMMStop:
			if (_turnMode == VTMAbs)
			{
				Vector3 dir = DirectionWorldToModel(_dirWanted);
				headChange = atan2(dir.X(), dir.Z());
				if (fabs(headChange) < 0.01 * H_PI) _turnMode = VTMNone;
			}
			return speedCoef;
		case VMMBackward:
		{
			speedCoef = -1.0;
			if (_turnMode != VTMNone)
			{
				_turnMode = VTMNone;
			}
			else
			{
				float dist2 = (_driverPos - Position()).SquareSizeXZ();
				if (dist2 >= Square(minDist)) break;
			}
			Vector3 from=Position() + Speed()*reactionTime;
			_driverPos = from - CmdPlanDist * _dirWanted;
			break;
		}
		case VMMSlowForward:
			speedCoef = 0.5;
			goto Forward;
		case VMMForward:
			speedCoef = 1.0;
			goto Forward;
		case VMMFastForward:
			speedCoef = 2.0;
			goto Forward;
		Forward:
		{
			if (_turnMode != VTMNone)
			{
				_turnMode = VTMNone;
			}
			else
			{
				float dist2 = (_driverPos - Position()).SquareSizeXZ();
				if (dist2 >= Square(minDist)) break;
			}
			Vector3 from = Position() + Speed()*reactionTime;
			_driverPos = from + CmdPlanDist * _dirWanted;
			break;
		}
	}
	return speedCoef;
}

#if _ENABLE_AI

void Transport::CommandPilot(float &speedWanted, float &headChange, float &turnPredict)
{
	// driver has any command
	speedWanted = 0;
	headChange = 0;
	turnPredict=0;

	if (_turnMode == VTMLeft)
	{
		headChange = -0.25 * H_PI;
		return;
	}
	else if (_turnMode == VTMRight)
	{
		headChange = +0.25 * H_PI;
		return;
	}

	float speedCoef = CalculateDriverPos(headChange,CmdMinDist);

	if (_moveMode==VMMFormation || _moveMode==VMMStop)
	{
		return;
	}

	AIUnit *unit = PilotUnit();
	Assert(unit);

	if (unit->GetPlanningMode() != AIUnit::VehiclePlanned || unit->GetWantedPosition().Distance2(_driverPos) > Square(1))
		unit->SetWantedPosition(_driverPos, AIUnit::VehiclePlanned, false);

	LeaderPathPilot(unit, speedWanted, headChange, turnPredict, speedCoef);
/*
	Path &path = unit->GetPath();
	if (path.Size() >= 2)
	{
		bool done = PathPilot(speedWanted, headChange, turnPredict, speedCoef);

		float cost = path.CostAtPos(Position());
		Vector3Val pos = path.PosAtCost(cost, Position());

		// measure distance from corresponding surface point?
		float distPath2 = (Position() - pos).SquareSizeXZ();
		float distEnd2 = Position().Distance2(path.End());

		float precision = GetPrecision();

		if (distEnd2 < Square(precision) || done)
		{
			unit->SendAnswer(AI::StepCompleted);
		}
		else if (distPath2 > Square(precision * 0.8))
		{
			if (distPath2 > Square(precision * 2) || path.GetSearchTime() < Glob.time - 2)
			{
				// path is not recent
				unit->SendAnswer(AI::StepTimeOut);
			}
		}
		else if (path.GetMaxIndex() < path.Size())
		{
			int lastValidIndex = path.GetMaxIndex() - 1;
			Assert(lastValidIndex >= 1);
			// check if we are in valid region
			if (cost > path[lastValidIndex]._cost)
			{
				// we need to replan
				unit->SendAnswer(AI::StepTimeOut);
			}
		}
	}
*/
}
/*!
\patch 1.82 Date 8/14/2002 by Ondra
- Fixed: MP: When AI soldier was gunner in vehicle where driver was player who was not group leader,
turret movement was not visible on driver's computer.
*/

void Transport::AIGunner(AIUnit *unit, float deltaT )
{
	if( _isDead || _isUpsideDown ) return;
	Assert(unit);

	if( !GetFireTarget() )
	{
		// aim weapon based on formation / watch
		AIUnit *commander = CommanderUnit();
		if (!commander) commander = unit;

		if (Glob.time<_turretFrontUntil)
		{
			AimWeapon(_currentWeapon,Direction());
			return;
		}
		_turretFrontUntil = TIME_MIN;

		Vector3Val watchDir = commander->GetWatchDirection();
		float watchDirSize2 = watchDir.SquareSize();

		if (watchDirSize2>0.1f) AimWeapon(_currentWeapon,watchDir);
		return;
	}

	// note: vehicle may be remote
	AimWeapon(_currentWeapon,GetFireTarget());
	if (!IsLocal())
	{
		AskForAimWeapon(_currentWeapon,GetWeaponDirectionWanted(_currentWeapon));
	}
	
	if( _currentWeapon<0 ) return;
	if( _fire._firePrepareOnly ) return;
	
	// check if weapon is aimed
	if
	(
		GetWeaponLoaded(_currentWeapon) && GetAimed(_currentWeapon,GetFireTarget())>=0.75
		&& GetWeaponReady(_currentWeapon,GetFireTarget()) && !IsManualFire()
	)
	{
		if (!GetAIFireEnabled(GetFireTarget())) ReportFireReady();
		else
		{
			FireWeapon(_currentWeapon,GetFireTarget()->idExact);
		}
//		_firePrepareOnly = true;
	}
}

void Transport::FormationPilot( float &speedWanted, float &headChange, float &turnPredict )
{
	if (_userStopped || !PilotUnit())
	{
		speedWanted=0;
		headChange=0;
		turnPredict=0;
		return;
	}

	if (_moveMode == VMMFormation)
		base::FormationPilot(speedWanted, headChange,turnPredict);
	else
		CommandPilot(speedWanted, headChange,turnPredict);
}

void Transport::LeaderPilot( float &speedWanted, float &headChange, float &turnPredict )
{
	if (_moveMode == VMMFormation)
		base::LeaderPilot(speedWanted,headChange,turnPredict);
	else
		CommandPilot(speedWanted, headChange,turnPredict);
}

#endif // _ENABLE_AI


// crew channel xmit done
void Transport::ReceivedMove( Vector3Par tgt )
{
	_moveMode = VMMMove;
//	UpdatePath(tgt);
	_driverPos = tgt;
}

void Transport::ReceivedJoin()
{
	_moveMode = VMMFormation;
//	_useStrategicPlan = false;

	if (CommanderUnit())
	{
		AISubgroup *subgrp = CommanderUnit()->GetSubgroup();
		if (subgrp)
		{
			AIGroup *grp = subgrp->GetGroup();
			Assert(grp);
			if (subgrp != grp->MainSubgroup())
			{
				subgrp->JoinToSubgroup(grp->MainSubgroup());
			}
		}
	}

	// delete current plan
//	if (PilotUnit()) PilotUnit()->SetState(AIUnit::Wait);
	if (PilotUnit())
		PilotUnit()->SetWantedPosition(_driverPos, AIUnit::DoNotPlan, true);
}

void Transport::ReceivedCeaseFire()
{
	_fire._firePrepareOnly = true;
}

void Transport::ReceivedFire(Target *tgt)
{
	AIUnit *unit = GunnerUnit();
	_fire.SetTarget(unit,tgt);
	_fire._fireMode = _currentWeapon;
	_fire._firePrepareOnly = false;
	//LogF("Received fire, weapon %d",_currentWeapon);
}

void Transport::ReceivedTarget(Target *tgt)
{
	AIUnit *unit = GunnerUnit();
	_fire.SetTarget(unit,tgt);
	_fire._fireMode = _currentWeapon;
	_fire._firePrepareOnly = true;
	//LogF("Received target, weapon %d",_currentWeapon);
}

void Transport::ReceivedSimpleCommand( SimpleCommand cmd )
{
	const float reactionTime=DriverReactionTime;
	switch (cmd)
	{
		case SCForward:
			if (!_driver) return;
			if (_moveMode == VMMFormation || _moveMode == VMMMove)
			{
				_dirWanted = Direction();
				_azimutWanted = atan2(_dirWanted.X(), _dirWanted.Z());
			}
			_moveMode = VMMForward;
//			_useStrategicPlan = false;
			_driverPos = Position()+Speed()*reactionTime;
//			_completedSent = false;
			break;
		case SCStop:
			if (!_driver) return;
			if (_moveMode == VMMFormation || _moveMode == VMMMove)
			{
				_dirWanted = Direction();
				_azimutWanted = atan2(_dirWanted.X(), _dirWanted.Z());
			}
			_moveMode = VMMStop;
//			_useStrategicPlan = false;
			_driverPos = Position()+Speed()*reactionTime;
//			_completedSent = false;
			break;
		case SCBackward:
			if (!_driver) return;
			if (_moveMode == VMMFormation || _moveMode == VMMMove)
			{
				_dirWanted = Direction();
				_azimutWanted = atan2(_dirWanted.X(), _dirWanted.Z());
			}
			_moveMode = VMMBackward;
//			_useStrategicPlan = false;
			_driverPos = Position()+Speed()*reactionTime;
//			_completedSent = false;
			break;
		case SCFaster:
			if (!_driver) return;
			if (_moveMode == VMMFormation || _moveMode == VMMMove)
			{
				_dirWanted = Direction();
				_azimutWanted = atan2(_dirWanted.X(), _dirWanted.Z());
			}
			_moveMode = VMMFastForward;
//			_useStrategicPlan = false;
			_driverPos = Position()+Speed()*reactionTime;
//			_completedSent = false;
			break;
		case SCSlower:
			if (!_driver) return;
			if (_moveMode == VMMFormation || _moveMode == VMMMove)
			{
				_dirWanted = Direction();
				_azimutWanted = atan2(_dirWanted.X(), _dirWanted.Z());
			}
			_moveMode = VMMSlowForward;
//			_useStrategicPlan = false;
			_driverPos = Position()+Speed()*reactionTime;
//			_completedSent = false;
			break;
		case SCLeft:
		case SCRight:
			if (!_driver) return;
			if (_moveMode == VMMFormation || _moveMode == VMMMove)
			{
				_dirWanted = Direction();
				_azimutWanted = atan2(_dirWanted.X(), _dirWanted.Z());
				if
				(
					_moveMode == VMMFormation &&
					CommanderUnit() && CommanderUnit()->IsGroupLeader()
				)
				{
					_moveMode = VMMStop;
				}
				else
				{
					_moveMode = VMMForward;
				}
			}
			if( cmd==SCRight )
				_turnMode = VTMRight;
			else
				_turnMode = VTMLeft;
			break;
		case SCFire:
			//if (!_gunner || GunnerBrain()->IsAnyPlayer()) return;
			if (GunnerBrain())
			{
				if (_currentWeapon >= 0)
				{
					if (GunnerBrain()->IsAnyPlayer())
					{
						// note: we must remmember we already ordered player to fire
						_fire._firePrepareOnly = false;
					}
					else
					{
						const WeaponModeType *mode = GetWeaponMode(_currentWeapon);
						if (mode && mode->_autoFire)
							_fire._firePrepareOnly = false;
						else if (GetWeaponLoaded(_currentWeapon))
							FireWeapon
							(
								_currentWeapon,
								_fire._fireTarget ? _fire._fireTarget->idExact : NULL
							);
						else
							SendFireFailed();
					}
				}
			}
			break;
		case SCCeaseFire:
			if (!_gunner) return;
			_fire._firePrepareOnly = true;
			break;
		default:
			RptF("Simple command %d", cmd);
			break;
	}
}

void Transport::ReceivedLoad(int weapon)
{
//LogF("Receiving load %d->%d", _currentWeapon, weapon);

	if (!GunnerUnit()->IsAnyPlayer())
	{
		//LogF("Received load %d->%d", _currentWeapon, weapon);
		SelectWeapon(weapon);
		_fire._fireMode = weapon;
		_fire._firePrepareOnly = true;
	}
}

void Transport::ReceivedAzimut(float azimut)
{
	_azimutWanted = azimut;
	_dirWanted = Vector3(sin(_azimutWanted), 0, cos(_azimutWanted));
	if (_moveMode == VMMFormation || _moveMode == VMMMove)
	{
		_moveMode = VMMForward;
	}
	_turnMode = VTMAbs;
//	_useStrategicPlan = false;
//	_completedSent = false;
}

/*!
\patch 1.52 Date 4/19/2002 by Ondra
- Fixed: Improved tank driver reactions for turning commands.
*/

void Transport::ReceivedStopTurning(float azimut)
{
	_azimutWanted = azimut;
	_dirWanted = Vector3(sin(_azimutWanted), 0, cos(_azimutWanted));
	_turnMode = VTMAbs;
	AIUnit *unit = PilotUnit();
	if (unit)
	{
		// check if moving
		switch (_moveMode)
		{
			case VMMBackward:
			case VMMSlowForward:
			case VMMForward:
			case VMMFastForward:
			{
				// replan path immediatelly - to get short reaction time
				float headChange;
				CalculateDriverPos(headChange,0);

				unit->SetWantedPosition(_driverPos, AIUnit::VehiclePlanned, true);
			}
			break;
		}
	}
}

RadioMessage *CreateVehicleMessage(int type)
{
	switch (type)
	{
	case RMTVehicleMove:
		return new RadioMessageVMove();
	case RMTVehicleFire:
		return new RadioMessageVFire();
	case RMTVehicleFormation:
		return new RadioMessageVFormation();
	case RMTVehicleCeaseFire:
		return new RadioMessageVCeaseFire();
	case RMTVehicleSimpleCommand:
		return new RadioMessageVSimpleCommand();
	case RMTVehicleTarget:
		return new RadioMessageVTarget();
	case RMTVehicleLoad:
		return new RadioMessageVLoad();
	case RMTVehicleAzimut:
		return new RadioMessageVAzimut();
	case RMTVehicleStopTurning:
		return new RadioMessageVStopTurning();
	case RMTVehicleFireFailed:
		return new RadioMessageVFireFailed();
	}
	Fail("Message Type");
	return NULL;
}

RadioMessageVFireFailed::RadioMessageVFireFailed(Transport *vehicle)
{
	_vehicle=vehicle;
}

LSError RadioMessageVFireFailed::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.SerializeRef("Vehicle", _vehicle, 1))
	return LSOK;
}

void RadioMessageVFireFailed::Transmitted()
{
}

AIUnit *RadioMessageVFireFailed::GetSender() const
{
	return _vehicle ? _vehicle->GunnerUnit() : NULL;
}

NetworkMessageType RadioMessageVFireFailed::GetNMType(NetworkMessageClass cls) const
{
	return NMTMsgVFireFailed;
}

IndicesVMessage::IndicesVMessage()
{
	vehicle = -1;
}

void IndicesVMessage::Scan(NetworkMessageFormatBase *format)
{
	SCAN(vehicle)
}

class IndicesMsgVFireFailed : public IndicesVMessage
{
	typedef IndicesVMessage base;

public:
	IndicesMsgVFireFailed();
	NetworkMessageIndices *Clone() const {return new IndicesMsgVFireFailed;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesMsgVFireFailed::IndicesMsgVFireFailed()
{
	vehicle = -1;
}

void IndicesMsgVFireFailed::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);
}

NetworkMessageIndices *GetIndicesMsgVFireFailed() {return new IndicesMsgVFireFailed();}

NetworkMessageFormat &RadioMessageVFireFailed::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("In vehicle"));
	return format;
}

TMError RadioMessageVFireFailed::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesMsgVFireFailed *>(ctx.GetIndices()))
	const IndicesMsgVFireFailed *indices = static_cast<const IndicesMsgVFireFailed *>(ctx.GetIndices());

	ITRANSF_REF(vehicle)
	return TMOK;
}

void RadioMessageVFireFailed::Send()
{
	if (_vehicle) _vehicle->SendFireFailed();
}

const char *RadioMessageVFireFailed::PrepareSentence(SentenceParams &params)
{
	if (!_vehicle) return NULL;
	AIUnit *receiver = _vehicle->CommanderUnit();
	if (!receiver) return NULL;

	const char *sentence = "VehicleFireFailed";
	return sentence;
}

RadioMessageWithTarget::RadioMessageWithTarget( Transport *vehicle, Target *tgt )
{
	_vehicle=vehicle;
	_target=tgt;
}

AIUnit *RadioMessageWithTarget::GetSender() const
{
	return _vehicle ? _vehicle->CommanderUnit() : NULL;
}

class IndicesMessageWithTarget : public IndicesVMessage
{
	typedef IndicesVMessage base;
public:
	int target;

	IndicesMessageWithTarget();
	NetworkMessageIndices *Clone() const {return new IndicesMessageWithTarget;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesMessageWithTarget::IndicesMessageWithTarget()
{
	target = -1;
}

void IndicesMessageWithTarget::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);
	SCAN(target)
}

NetworkMessageFormat &RadioMessageWithTarget::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("In vehicle"));
	format.Add("target", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Target"));
	return format;
}

TMError RadioMessageWithTarget::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesMessageWithTarget *>(ctx.GetIndices()))
	const IndicesMessageWithTarget *indices = static_cast<const IndicesMessageWithTarget *>(ctx.GetIndices());

	ITRANSF_REF(vehicle)

	if (ctx.IsSending())
	{
		EntityAI *target = NULL;
		if (_target) target = _target->idExact;
		TMCHECK(ctx.IdxTransferRef(indices->target, target))
	}
	else
	{
		_target = NULL;
		EntityAI *target = NULL;
		TMCHECK(ctx.IdxTransferRef(indices->target, target))
		if (target)
		{
			AIUnit *unit = _vehicle->CommanderUnit();
			AIGroup *grp = unit ? unit->GetGroup() : NULL;
			if (grp)
			{
				_target = grp->FindTarget(target);
			}
		}
	}

	return TMOK;
}

static bool CheckUnitAlive(AIUnit *unit)
{
	// check if unit is alive
	// it should be responding to some message
	// if it is not, it must be dead
	if (!unit) return false;
	if (unit->GetLifeState()==AIUnit::LSAlive) return true;
	AIGroup *grp = unit->GetGroup();
	// TODO: set also who should report unit is dead (vehicle commander)
	// or do hack that in-vehicle units are always reported by the vehicle commander
	if (grp) grp->SetReportBeforeTime(unit,Glob.time+10);
	return false;
}

RadioMessageVTarget::RadioMessageVTarget( Transport *vehicle, Target *tgt )
:base(vehicle,tgt)
{
}

void RadioMessageVTarget::Transmitted()
{
	if (!_vehicle) return;
	AIUnit *receiver = _vehicle->GunnerUnit();
	if (!CheckUnitAlive(receiver)) return;

	_vehicle->ReceivedTarget(_target);
}

LSError RadioMessageWithTarget::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.SerializeRef("Vehicle", _vehicle, 1))
	CHECK(ar.SerializeRef("Target", _target, 1))
	return LSOK;
}

NetworkMessageType RadioMessageVTarget::GetNMType(NetworkMessageClass cls) const
{
	return NMTMsgVTarget;
}

NetworkMessageIndices *GetIndicesMsgVTarget() {return new IndicesMessageWithTarget();}

void RadioMessageVTarget::Send()
{
	if (_vehicle) _vehicle->SendTarget(_target);
}

/*!
\patch 1.31 Date 11/20/2001 by Ondra.
- New: heading is transmitted when assigning targets to gunner.
This makes finding targets in MP much easier.
*/

const char *RadioMessageVTarget::PrepareSentence(SentenceParams &params)
{
	if (!_vehicle) return NULL;
	AIUnit *receiver = _vehicle->GunnerUnit();
	if (!receiver) return NULL;

	if (!_target) return "VehicleNoTarget";
	
	//TargetSide side = _target->side;
	const VehicleType *type = _target->type;
	params.AddWord(type->GetNameSound(), type->GetDisplayName());
	params.AddRelativePosition(_target->AimingPosition()-_vehicle->Position());
	return "VehicleTarget";
}

RadioMessageVFire::RadioMessageVFire( Transport *vehicle, Target *tgt )
:base(vehicle,tgt)
{
}

void RadioMessageVFire::Transmitted()
{
	if (!_vehicle) return;
	AIUnit *receiver = _vehicle->GunnerUnit();
	if (!CheckUnitAlive(receiver)) return;

	_vehicle->ReceivedFire(_target);
}

NetworkMessageType RadioMessageVFire::GetNMType(NetworkMessageClass cls) const
{
	return NMTMsgVFire;
}

NetworkMessageIndices *GetIndicesMsgVFire() {return new IndicesMessageWithTarget();}

void RadioMessageVFire::Send()
{
	if (_vehicle) _vehicle->SendFire(_target);
}

const char *RadioMessageVFire::PrepareSentence(SentenceParams &params)
{
	if (!_vehicle) return NULL;
	AIUnit *receiver = _vehicle->GunnerUnit();
	if (!receiver) return NULL;

	const char *sentence = "VehicleFire";
	if (_target)
	{
		//TargetSide side = _target->side;
		const VehicleType *type = _target->type;
		params.AddWord(type->GetNameSound(), type->GetDisplayName());
	}
	else
	{
		params.AddWord("unknown", LocalizeString(IDS_WORD_UNKNOWN));
	}
	return sentence;
}

RadioMessageVMove::RadioMessageVMove( Transport *vehicle, Vector3Par pos )
{
	_vehicle=vehicle;
	_destination=pos;
}

AIUnit *RadioMessageVMove::GetSender() const
{
	return _vehicle ? _vehicle->CommanderUnit() : NULL;
}

void RadioMessageVMove::Transmitted()
{
	if (!_vehicle) return;
	AIUnit *receiver = _vehicle->PilotUnit();
	if (!CheckUnitAlive(receiver)) return;

	_vehicle->ReceivedMove(_destination);
}

LSError RadioMessageVMove::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.SerializeRef("Vehicle", _vehicle, 1))
	CHECK(ar.Serialize("destination", _destination, 1))
	return LSOK;
}

NetworkMessageType RadioMessageVMove::GetNMType(NetworkMessageClass cls) const
{
	return NMTMsgVMove;
}

class IndicesMsgVMove : public IndicesVMessage
{
	typedef IndicesVMessage base;

public:
	int destination;

	IndicesMsgVMove();
	NetworkMessageIndices *Clone() const {return new IndicesMsgVMove;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesMsgVMove::IndicesMsgVMove()
{
	destination = -1;
}

void IndicesMsgVMove::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);
	SCAN(destination)
}

NetworkMessageIndices *GetIndicesMsgVMove() {return new IndicesMsgVMove();}

NetworkMessageFormat &RadioMessageVMove::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("In vehicle"));
	format.Add("destination", NDTVector, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Destination position"));
	return format;
}

TMError RadioMessageVMove::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesMsgVMove *>(ctx.GetIndices()))
	const IndicesMsgVMove *indices = static_cast<const IndicesMsgVMove *>(ctx.GetIndices());

	ITRANSF_REF(vehicle)
	ITRANSF(destination)
	return TMOK;
}

void RadioMessageVMove::Send()
{
	if (_vehicle) _vehicle->SendMove(_destination);
}

const char *RadioMessageVMove::PrepareSentence(SentenceParams &params)
{
	if (!_vehicle) return NULL;
	AIUnit *receiver = _vehicle->PilotUnit();
	if (!receiver) return NULL;

	const char *sentence = "VehicleMove";

	Vector3 dir = _destination - _vehicle->Position();
	int azimut = toInt(0.1 * atan2(dir.X(), dir.Z()) * (180 / H_PI));
	if (azimut < 0) azimut += 36;
	int dist = toInt(0.1 * dir.SizeXZ());
	params.Add("%02d", azimut);
	params.Add(dist);
	return sentence;
}

RadioMessageVFormation::RadioMessageVFormation( Transport *vehicle )
{
	_vehicle=vehicle;
}

AIUnit *RadioMessageVFormation::GetSender() const
{
	return _vehicle ? _vehicle->CommanderUnit() : NULL;
}

void RadioMessageVFormation::Transmitted()
{
	if (!_vehicle) return;
	AIUnit *receiver = _vehicle->PilotUnit();
	if (!CheckUnitAlive(receiver)) return;

	_vehicle->ReceivedJoin();
}

LSError RadioMessageVFormation::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.SerializeRef("Vehicle", _vehicle, 1))
	return LSOK;
}

NetworkMessageType RadioMessageVFormation::GetNMType(NetworkMessageClass cls) const
{
	return NMTMsgVFormation;
}

class IndicesMsgVFormation : public IndicesVMessage
{
	typedef IndicesVMessage base;
public:
	IndicesMsgVFormation();
	NetworkMessageIndices *Clone() const {return new IndicesMsgVFormation;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesMsgVFormation::IndicesMsgVFormation()
{
}

void IndicesMsgVFormation::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);
}

NetworkMessageIndices *GetIndicesMsgVFormation() {return new IndicesMsgVFormation();}

NetworkMessageFormat &RadioMessageVFormation::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("In vehicle"));
	return format;
}

TMError RadioMessageVFormation::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesMsgVFormation *>(ctx.GetIndices()))
	const IndicesMsgVFormation *indices = static_cast<const IndicesMsgVFormation *>(ctx.GetIndices());

	ITRANSF_REF(vehicle)
	return TMOK;
}

void RadioMessageVFormation::Send()
{
	if (_vehicle) _vehicle->SendJoin();
}

const char *RadioMessageVFormation::PrepareSentence(SentenceParams &params)
{
	if (!_vehicle) return NULL;
	AIUnit *receiver = _vehicle->PilotUnit();
	if (!receiver) return NULL;

	const char *sentence = "VehicleJoin";
	return sentence;
}

RadioMessageVCeaseFire::RadioMessageVCeaseFire( Transport *vehicle )
{
	_vehicle=vehicle;
}

AIUnit *RadioMessageVCeaseFire::GetSender() const
{
	return _vehicle ? _vehicle->CommanderUnit() : NULL;
}

void RadioMessageVCeaseFire::Transmitted()
{
	if (!_vehicle) return;
	AIUnit *receiver = _vehicle->GunnerUnit();
	if (!CheckUnitAlive(receiver)) return;

	_vehicle->ReceivedCeaseFire();
}

LSError RadioMessageVCeaseFire::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.SerializeRef("Vehicle", _vehicle, 1))
	return LSOK;
}

const char *RadioMessageVCeaseFire::PrepareSentence(SentenceParams &params)
{
	if (!_vehicle) return NULL;
	AIUnit *receiver = _vehicle->GunnerUnit();
	if (!receiver) return NULL;

	const char *sentence = "VehicleCeaseFire";
	return sentence;
}

RadioMessageVSimpleCommand::RadioMessageVSimpleCommand( Transport *vehicle, SimpleCommand cmd )
{
	_vehicle=vehicle;
	_cmd=cmd;
}

AIUnit *RadioMessageVSimpleCommand::GetSender() const
{
	return _vehicle ? _vehicle->CommanderUnit() : NULL;
}

void RadioMessageVSimpleCommand::Transmitted()
{
	if (!_vehicle) return;
	AIUnit *receiver = NULL;
	if (_cmd == SCFire || _cmd == SCCeaseFire || _cmd == SCKeyFire)
		receiver = _vehicle->GunnerUnit();
	else
		receiver = _vehicle->PilotUnit();
	if (!CheckUnitAlive(receiver)) return;

	_vehicle->ReceivedSimpleCommand(_cmd);
}

static const EnumName SimpleCommandNames[]=
{
	EnumName(SCForward, "FORWARD"),
	EnumName(SCStop, "STOP"),
	EnumName(SCBackward, "BACK"),
	EnumName(SCFaster, "FAST"),
	EnumName(SCSlower, "SLOW"),
	EnumName(SCLeft, "LEFT"),
	EnumName(SCRight, "RIGHT"),
	EnumName(SCFire, "FIRE"),
	EnumName(SCCeaseFire, "CEASE FIRE"),
	EnumName(SCKeyUp, "KEY UP"),
	EnumName(SCKeyDown, "KEY DOWN"),
	EnumName(SCKeySlow, "KEY SLOW"),
	EnumName(SCKeyFast, "KEY FAST"),
	EnumName(SCKeyFire, "KEY FIRE"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(SimpleCommand dummy)
{
	return SimpleCommandNames;
}

LSError RadioMessageVSimpleCommand::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.SerializeRef("Vehicle", _vehicle, 1))
	CHECK(ar.SerializeEnum("cmd", _cmd, 1))
	return LSOK;
}

NetworkMessageType RadioMessageVSimpleCommand::GetNMType(NetworkMessageClass cls) const
{
	return NMTMsgVSimpleCommand;
}

class IndicesMsgVSimpleCommand : public IndicesVMessage
{
	typedef IndicesVMessage base;

public:
	int cmd;

	IndicesMsgVSimpleCommand();
	NetworkMessageIndices *Clone() const {return new IndicesMsgVSimpleCommand;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesMsgVSimpleCommand::IndicesMsgVSimpleCommand()
{
	cmd = -1;
}

void IndicesMsgVSimpleCommand::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);
	SCAN(cmd)
}

NetworkMessageIndices *GetIndicesMsgVSimpleCommand() {return new IndicesMsgVSimpleCommand();}

NetworkMessageFormat &RadioMessageVSimpleCommand::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("In vehicle"));
	format.Add("cmd", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Command"));
	return format;
}

TMError RadioMessageVSimpleCommand::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesMsgVSimpleCommand *>(ctx.GetIndices()))
	const IndicesMsgVSimpleCommand *indices = static_cast<const IndicesMsgVSimpleCommand *>(ctx.GetIndices());

	ITRANSF_REF(vehicle)
	ITRANSF_ENUM(cmd)
	return TMOK;
}

void RadioMessageVSimpleCommand::Send()
{
	if (_vehicle) _vehicle->SendSimpleCommand(_cmd);
}

const char *RadioMessageVSimpleCommand::PrepareSentence(SentenceParams &params)
{
	// translate keys
	if (!_vehicle) return NULL;
	AIUnit *receiver = NULL;
	if (_cmd == SCFire || _cmd == SCCeaseFire || _cmd == SCKeyFire)
		receiver = _vehicle->GunnerUnit();
	else
		receiver = _vehicle->PilotUnit();
	if (!receiver) return NULL;

	VehicleMoveMode mode = _vehicle->GetMoveMode();
	if (mode == VMMFormation || mode == VMMMove)
	{
		float speed = _vehicle->ModelSpeed().Z();
		if (speed < -0.1) mode = VMMBackward;
		else if (speed > 0.1) mode = VMMForward;
		else mode = VMMStop;
	}

	switch (_cmd)
	{
		case SCKeyUp:
			{
				if (mode == VMMBackward)
					_cmd = SCStop;
				else
					_cmd = SCForward;
			}
			break;
		case SCKeyDown:
			{
				if (mode == VMMSlowForward || mode == VMMForward || mode == VMMFastForward)
					_cmd = SCStop;
				else
					_cmd = SCBackward;
			}
			break;
		case SCKeySlow:
			{
				if (mode == VMMBackward)
					_cmd = SCStop;
				else
					_cmd = SCSlower;
			}
			break;
		case SCKeyFast:
			{
				if (mode == VMMBackward)
					_cmd = SCStop;
				else
					_cmd = SCFaster;
			}
			break;
		case SCKeyFire:
			if (_vehicle->IsFirePrepare())
				_cmd = SCFire;
			else
				_cmd = SCCeaseFire;
			break;
	}
	
	switch (_cmd)
	{
		case SCForward:
			return "VehicleForward";
		case SCStop:
			return "VehicleStop";
		case SCBackward:
			return "VehicleBackward";
		case SCFaster:
			return "VehicleFaster";
		case SCSlower:
			return "VehicleSlower";
		case SCLeft:
			return "VehicleLeft";
		case SCRight:
			return "VehicleRight";
		case SCFire:
			return "VehicleDirectFire";
		case SCCeaseFire:
			return "VehicleCeaseFire";
	}
	Fail("Simple command");
	return NULL;
}

RadioMessageVLoad::RadioMessageVLoad(Transport *vehicle, int weapon)
{
	_vehicle = vehicle;
	_weapon = weapon;
}

AIUnit *RadioMessageVLoad::GetSender() const
{
	return _vehicle ? _vehicle->CommanderUnit() : NULL;
}

LSError RadioMessageVLoad::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.SerializeRef("Vehicle", _vehicle, 1))
	CHECK(ar.Serialize("weapon", _weapon, 1))
	return LSOK;
}

NetworkMessageType RadioMessageVLoad::GetNMType(NetworkMessageClass cls) const
{
	return NMTMsgVLoad;
}

class IndicesMsgVLoad : public IndicesVMessage
{
	typedef IndicesVMessage base;

public:
	int weapon;

	IndicesMsgVLoad();
	NetworkMessageIndices *Clone() const {return new IndicesMsgVLoad;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesMsgVLoad::IndicesMsgVLoad()
{
	weapon = -1;
}

void IndicesMsgVLoad::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);
	SCAN(weapon)
}

NetworkMessageIndices *GetIndicesMsgVLoad() {return new IndicesMsgVLoad();}

NetworkMessageFormat &RadioMessageVLoad::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("In vehicle"));
	format.Add("weapon", NDTInteger, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Weapon index"));
	return format;
}

TMError RadioMessageVLoad::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesMsgVLoad *>(ctx.GetIndices()))
	const IndicesMsgVLoad *indices = static_cast<const IndicesMsgVLoad *>(ctx.GetIndices());

	ITRANSF_REF(vehicle)
	ITRANSF(weapon)
	return TMOK;
}

void RadioMessageVLoad::Send()
{
	if (_vehicle) _vehicle->SendLoad(_weapon);
}

void RadioMessageVLoad::Transmitted()
{
	if (!_vehicle) return;
	AIUnit *receiver = _vehicle->GunnerUnit();
	if (!CheckUnitAlive(receiver)) return;

	_vehicle->ReceivedLoad(_weapon);
}

const char *RadioMessageVLoad::PrepareSentence(SentenceParams &params)
{
	if (!_vehicle) return NULL;
	AIUnit *receiver = _vehicle->GunnerUnit();
	if (!receiver) return NULL;

	if (_weapon < 0 || _weapon >= _vehicle->NMagazineSlots()) return NULL;

	const char *sentence = "VehicleLoad";
	const Magazine *magazine = _vehicle->GetMagazineSlot(_weapon)._magazine;
	if (!magazine) return NULL;
	const MagazineType *type = magazine->_type;
	params.AddWord(type->GetNameSound(), type->GetDisplayName());
	return sentence;
}

RadioMessageVAzimut::RadioMessageVAzimut(Transport *vehicle, float azimut)
{
	_vehicle = vehicle;
	_azimut = azimut;
}

AIUnit *RadioMessageVAzimut::GetSender() const
{
	return _vehicle ? _vehicle->CommanderUnit() : NULL;
}

LSError RadioMessageVAzimut::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.SerializeRef("Vehicle", _vehicle, 1))
	CHECK(ar.Serialize("azimut", _azimut, 1))
	return LSOK;
}

NetworkMessageType RadioMessageVAzimut::GetNMType(NetworkMessageClass cls) const
{
	return NMTMsgVAzimut;
}

class IndicesMsgVAzimut : public IndicesVMessage
{
	typedef IndicesVMessage base;

public:
	int azimut;

	IndicesMsgVAzimut();
	NetworkMessageIndices *Clone() const {return new IndicesMsgVAzimut;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesMsgVAzimut::IndicesMsgVAzimut()
{
	azimut = -1;
}

void IndicesMsgVAzimut::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);
	SCAN(azimut)
}

NetworkMessageIndices *GetIndicesMsgVAzimut() {return new IndicesMsgVAzimut();}

NetworkMessageFormat &RadioMessageVAzimut::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("In vehicle"));
	format.Add("azimut", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Orientation azimuth"));
	return format;
}

TMError RadioMessageVAzimut::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesMsgVAzimut *>(ctx.GetIndices()))
	const IndicesMsgVAzimut *indices = static_cast<const IndicesMsgVAzimut *>(ctx.GetIndices());

	ITRANSF_REF(vehicle)
	ITRANSF(azimut);
	return TMOK;
}

void RadioMessageVAzimut::Send()
{
	if (_vehicle) _vehicle->SendAzimut(_azimut);
}

void RadioMessageVAzimut::Transmitted()
{
	if (!_vehicle) return;
	AIUnit *receiver = _vehicle->PilotUnit();
	if (!CheckUnitAlive(receiver)) return;

	_vehicle->ReceivedAzimut(_azimut);
}

const char *RadioMessageVAzimut::PrepareSentence(SentenceParams &params)
{
	if (!_vehicle) return NULL;
	AIUnit *receiver = _vehicle->PilotUnit();
	if (!receiver) return NULL;

	const char *sentence = "VehicleAzimut";
	int azimut = toInt(_azimut * (4.0 / H_PI));
	switch (azimut)
	{
		case 0:
			params.AddWord("north", LocalizeString(IDS_Q_NORTH));
			break;
		case 1:
			params.AddWord("northEast", LocalizeString(IDS_Q_NORTH_EAST));
			break;
		case 2:
			params.AddWord("east", LocalizeString(IDS_Q_EAST));
			break;
		case 3:
			params.AddWord("southEast", LocalizeString(IDS_Q_SOUTH_EAST));
			break;
		case 4:
			params.AddWord("south", LocalizeString(IDS_Q_SOUTH));
			break;
		case 5:
			params.AddWord("southWest", LocalizeString(IDS_Q_SOUTH_WEST));
			break;
		case 6:
			params.AddWord("west", LocalizeString(IDS_Q_WEST));
			break;
		case 7:
			params.AddWord("northWest", LocalizeString(IDS_Q_NORTH_WEST));
			break;
	}

	return sentence;
}

// class RadioMessageVStopTurning
RadioMessageVStopTurning::RadioMessageVStopTurning(Transport *vehicle, float azimut)
{
	_vehicle = vehicle;
	_azimut = azimut;
}

LSError RadioMessageVStopTurning::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.SerializeRef("Vehicle", _vehicle, 1))
	CHECK(ar.Serialize("azimut", _azimut, 1))
	return LSOK;
}

void RadioMessageVStopTurning::Transmitted()
{
	if (!_vehicle) return;
	AIUnit *receiver = _vehicle->PilotUnit();
	if (!CheckUnitAlive(receiver)) return;

	_vehicle->ReceivedStopTurning(_azimut);
}

AIUnit *RadioMessageVStopTurning::GetSender() const
{
	return _vehicle ? _vehicle->CommanderUnit() : NULL;
}

NetworkMessageType RadioMessageVStopTurning::GetNMType(NetworkMessageClass cls) const
{
	return NMTMsgVStopTurning;
}

class IndicesMsgVStopTurning : public IndicesVMessage
{
	typedef IndicesVMessage base;

public:
	int azimut;

	IndicesMsgVStopTurning();
	NetworkMessageIndices *Clone() const {return new IndicesMsgVStopTurning;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesMsgVStopTurning::IndicesMsgVStopTurning()
{
	azimut = -1;
}

void IndicesMsgVStopTurning::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);
	SCAN(azimut)
}

NetworkMessageIndices *GetIndicesMsgVStopTurning() {return new IndicesMsgVStopTurning();}

NetworkMessageFormat &RadioMessageVStopTurning::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("In vehicle"));
	format.Add("azimut", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Final azimuth"));
	return format;
}

TMError RadioMessageVStopTurning::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesMsgVStopTurning *>(ctx.GetIndices()))
	const IndicesMsgVStopTurning *indices = static_cast<const IndicesMsgVStopTurning *>(ctx.GetIndices());

	ITRANSF_REF(vehicle)
	ITRANSF(azimut);
	return TMOK;
}

void RadioMessageVStopTurning::Send()
{
	if (_vehicle) _vehicle->SendStopTurning(_azimut);
}

const char *RadioMessageVStopTurning::PrepareSentence(SentenceParams &params)
{
	if (!_vehicle) return NULL;
	AIUnit *receiver = _vehicle->PilotUnit();
	if (!receiver) return NULL;

	switch (_vehicle->GetMoveMode())
	{
		default:
		case VMMStop:
			return "VehicleStop";
		case VMMBackward:
			return "VehicleBackward";
		case VMMSlowForward:
			return "VehicleSlower";
		case VMMForward:
			return "VehicleForward";
		case VMMFastForward:
			return "VehicleFaster";
	}
}

// xmit into crew channel
void Transport::SendMove( Vector3Par pos )
{
	AIUnit *commander=CommanderUnit();
	if( !commander ) return;

	AIUnit *receiver = PilotUnit();
	if (!receiver || receiver == commander) return;

	// ignore very short moves
	if (pos.Distance2(Position()) < Square(2))
	{
		commander->SetState(AIUnit::Completed);
		return;
	}

	_radio.Transmit
	(
		new RadioMessageVMove(this,pos),
		commander->GetGroup()->GetCenter()->GetLanguage()
	);
}

void Transport::SendTarget(Target *tgt)
{
	AIUnit *commander = CommanderUnit();
	if( !commander ) return;

	AIUnit *receiver = GunnerUnit();
	if (!receiver || receiver == commander) return;
	
	// check radio channel
	int index = INT_MAX;
	while (true)
	{
		RadioMessage *msg = _radio.GetPrevMessage(index);
		if (!msg) break;
		switch (msg->GetType())
		{
		case RMTVehicleTarget:
			{
				RadioMessageVTarget *msgT = static_cast<RadioMessageVTarget *>(msg);
				msgT->SetTarget(tgt);
				return;
			}
			break;
		case RMTVehicleSimpleCommand:
			{
				RadioMessageVSimpleCommand *msgT = static_cast<RadioMessageVSimpleCommand *>(msg);
				if (msgT->GetCommand() == SCFire) goto SendTargetTransmit; 
			}
			break;
		}
	}

	{
		RadioMessage *msg = _radio.GetActualMessage();
		if (msg) switch (msg->GetType())
		{
		case RMTVehicleTarget:
			{
				RadioMessageVTarget *msgT = static_cast<RadioMessageVTarget *>(msg);
				if (msgT->GetTarget() == tgt) return;
				else goto SendTargetTransmit;
			}
			break;
		case RMTVehicleSimpleCommand:
			{
				RadioMessageVSimpleCommand *msgT = static_cast<RadioMessageVSimpleCommand *>(msg);
				if (msgT->GetCommand() == SCFire) goto SendTargetTransmit; 
			}
			break;
		}
	}

	if (tgt == _fire._fireTarget) return;

SendTargetTransmit:

	_radio.Transmit
	(
		new RadioMessageVTarget(this,tgt),
		commander->GetGroup()->GetCenter()->GetLanguage()
	);
	_radio.Simulate(0);
}

/*!
\patch 1.51 Date 4/19/2002 by Ondra
- Fixed: Vehicle crew "Fire" command is now higher priority
than reporting enemies messages.
*/


void Transport::SendFire(Target *tgt)
{
	AIUnit *commander=CommanderUnit();
	if( !commander ) return;

	AIUnit *receiver = GunnerUnit();
	if (!receiver || receiver == commander) return;

	_radio.Transmit
	(
		new RadioMessageVFire(this,tgt),
		commander->GetGroup()->GetCenter()->GetLanguage()
	);
	_radio.Simulate(0);
}

void Transport::SendJoin()
{
	AIUnit *commander=CommanderUnit();
	if( !commander ) return;

	AIUnit *receiver = PilotUnit();
	if (!receiver || receiver == commander) return;

	_radio.Transmit
	(
		new RadioMessageVFormation(this),
		commander->GetGroup()->GetCenter()->GetLanguage()
	);
}

static bool IsSimpleMove( SimpleCommand type )
{
	switch (type)
	{
		case SCForward:
		case SCStop:
		case SCBackward:
		case SCFaster:
		case SCSlower:
//	SCLeft,
//	SCRight,
		case SCKeyUp:
		case SCKeyDown:
		case SCKeySlow:
		case SCKeyFast:
			return true;
	}
	return false;
}
void Transport::SendSimpleCommand( SimpleCommand cmd )
{
	AIUnit *commander=CommanderUnit();
	if( !commander ) return;

	AIUnit *receiver = NULL;
	if (cmd == SCFire || cmd == SCCeaseFire || cmd == SCKeyFire)
		receiver = GunnerUnit();
	else
		receiver = PilotUnit();
	if (!receiver || receiver == commander) return;
	
	if (IsSimpleMove(cmd))
	{
		// remove all other simple moves from queue
		int index = INT_MAX;
		while (true)
		{
			RadioMessage *msg = _radio.GetPrevMessage(index);
			if (!msg) break;
			// check message type
			if (msg->GetType()==RMTVehicleSimpleCommand)
			{
				// check if it is left or right
				RadioMessageVSimpleCommand *cmd = static_cast<RadioMessageVSimpleCommand *>
				(
					msg
				);
				if (IsSimpleMove(cmd->GetCommand()))
				{
					_radio.Cancel(msg);
				}
			}
		}

	}
	_radio.Transmit
	(
		new RadioMessageVSimpleCommand(this,cmd),
		commander->GetGroup()->GetCenter()->GetLanguage()
	);
}

/*!
\patch 1.08 Date 7/21/2001 by Ondra.
- Fixed:
When tank commander was changing weapons often,
weapon change command was sometimes lost.
*/

void Transport::SendLoad(int weapon)
{
	AIUnit *commander = CommanderUnit();
	if (!commander) return;

	AIUnit *receiver = GunnerUnit();
	if (!receiver || receiver == commander) return;

	//LogF("Request to send load %d->%d", _currentWeapon, weapon);

	// check radio channel
	int index = INT_MAX;
	while (true)
	{
		RadioMessage *msg = _radio.GetPrevMessage(index);
		if (!msg) break;
		switch (msg->GetType())
		{
		case RMTVehicleLoad:
			{
				RadioMessageVLoad *msgT = static_cast<RadioMessageVLoad *>(msg);
				msgT->SetWeapon(weapon);
				return;
			}
			break;
		case RMTVehicleSimpleCommand:
			{
				RadioMessageVSimpleCommand *msgT = static_cast<RadioMessageVSimpleCommand *>(msg);
				if (msgT->GetCommand() == SCFire) goto SendLoadTransmit; 
			}
			break;
		}
	}

	// FIX
	{

		int actWeapon =  _currentWeapon;
		RadioMessage *msg = _radio.GetActualMessage();
		if (msg) switch (msg->GetType())
		{
		case RMTVehicleLoad:
			{
				RadioMessageVLoad *msgT = static_cast<RadioMessageVLoad *>(msg);
				actWeapon = msgT->GetWeapon();
			}
			break;
		case RMTVehicleSimpleCommand:
			{
				RadioMessageVSimpleCommand *msgT = static_cast<RadioMessageVSimpleCommand *>(msg);
				if (msgT->GetCommand() == SCFire) goto SendLoadTransmit; 
			}
			break;
		}
		if (actWeapon == weapon) return;
	}



SendLoadTransmit:
	//LogF("Sending load %d->%d", _currentWeapon, weapon);
	_radio.Transmit
	(
		new RadioMessageVLoad(this, weapon),
		commander->GetGroup()->GetCenter()->GetLanguage()
	);
}

void Transport::SendAzimut(float azimut)
{
	AIUnit *commander = CommanderUnit();
	if (!commander) return;

	AIUnit *receiver = PilotUnit();
	if (!receiver || receiver == commander) return;


	_radio.Transmit
	(
		new RadioMessageVAzimut(this, azimut),
		commander->GetGroup()->GetCenter()->GetLanguage()
	);
}

void Transport::SendStopTurning(float azimut)
{
	AIUnit *commander = CommanderUnit();
	if (!commander) return;

	AIUnit *receiver = PilotUnit();
	if (!receiver || receiver == commander) return;

	// remove all previous turn / stop turning from the queue
	// leave only corresonding turn
	// remove all stop turning
	int index = INT_MAX;
	bool oneTurnSkipped = false;
	while (true)
	{
		RadioMessage *msg = _radio.GetPrevMessage(index);
		if (!msg) break;
		// check message type
		if (msg->GetType()==RMTVehicleStopTurning)
		{
			_radio.Cancel(msg);
		}
		else if (msg->GetType()==RMTVehicleSimpleCommand)
		{
			// check if it is left or right
			RadioMessageVSimpleCommand *cmd = static_cast<RadioMessageVSimpleCommand *>
			(
				msg
			);
			if (cmd->GetCommand()==SCLeft || cmd->GetCommand()==SCRight)
			{
				if (!oneTurnSkipped) oneTurnSkipped=true;
				else _radio.Cancel(msg);
			}
		}
	}
	// scan channer


	_radio.Transmit
	(
		new RadioMessageVStopTurning(this, azimut),
		commander->GetGroup()->GetCenter()->GetLanguage()
	);
}

void Transport::SendFireFailed()
{
	AIUnit *from = GunnerUnit();
	if (!from) return;

	AIUnit *receiver = CommanderUnit();
	if (!receiver || receiver == from) return;

	_radio.Transmit
	(
		new RadioMessageVFireFailed(this),
		from->GetGroup()->GetCenter()->GetLanguage()
	);
}

void Transport::ResetStatus()
{
	base::ResetStatus();
}

void Transport::UpdateStopTimeout()
{
	float maxTime = 0;

	for (int i=0; i<_getinUnits.Size(); i++)
	{
		AIUnit *unit = _getinUnits[i];
		if (!unit) continue;

		// Calculate cost from starting field
		int x = toIntFloor(unit->Position().X() * InvLandGrid);
		int z = toIntFloor(unit->Position().Z() * InvLandGrid);
		GeographyInfo geogr = GLOB_LAND->GetGeography(x, z);
		float dist = Position().Distance(unit->Position());
		float time = 2.0 * unit->GetVehicle()->GetCost(geogr) * dist + 30.0;
		saturateMax(maxTime, time);
	}

	_getinTime = Glob.time + maxTime;
}

static const EnumName MoveModeNames[]=
{
	EnumName(VMMFormation,"FORMATION"),
	EnumName(VMMMove,"MOVE"),
	EnumName(VMMBackward,"BACK"),
	EnumName(VMMStop,"STOP"),
	EnumName(VMMSlowForward,"SLOW"),
	EnumName(VMMForward,"FORWARD"),
	EnumName(VMMFastForward,"FAST"),
	EnumName(),
};

template<>
const EnumName *GetEnumNames( VehicleMoveMode dummy ) {return MoveModeNames;}

static const EnumName TurnModeNames[]=
{
	EnumName(VTMNone,"NONE"),
	EnumName(VTMLeft,"LEFT"),
	EnumName(VTMRight,"RIGHT"),
	EnumName(VTMAbs,"ABS"),
	EnumName()
};

template<>
const EnumName *GetEnumNames( VehicleTurnMode dummy ) {return TurnModeNames;}

/*!
\patch 1.95 Date 10/25/2003 by Ondra
- Fixed: after load, engine was off for cars, tanks and planes 
*/

LSError Transport::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))

	if (!IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
	{
		CHECK(ar.SerializeRef("Driver", _driver, 1))
		CHECK(ar.SerializeRef("Gunner", _gunner, 1))
		CHECK(ar.SerializeRef("Commander", _commander, 1))
		CHECK(ar.SerializeRef("EffCommander", _effCommander, 1))
		// TODO: improve
		if (ar.IsSaving())
		{
			RefArray<Person> manCargo;
			int n = _manCargo.Size();
			manCargo.Resize(n);
			for (int i=0; i<n; i++)
			{
				manCargo[i] = _manCargo[i];
			}
			CHECK(ar.SerializeRefs("ManCargo", manCargo, 1))
		}
		else if (ar.GetPass() == ParamArchive::PassSecond)
		{
			RefArray<Person> manCargo;
			CHECK(ar.SerializeRefs("ManCargo", manCargo, 1))
			int n = Type()->_maxManCargo;
			_manCargo.Resize(n);
			saturateMin(n, manCargo.Size());
			for (int i=0; i<n; i++)
			{
				_manCargo.Set(i).man = manCargo[i];
			}

			// switch light off for soldiers in vehicle
			if (_driver)
			{
				_driver->SetPilotLight(false);
				_driver->SwitchLight(false);
			}
			if (_gunner)
			{
				_gunner->SetPilotLight(false);
				_gunner->SwitchLight(false);
			}
			if (_commander)
			{
				_commander->SetPilotLight(false);
				_commander->SwitchLight(false);
			}
			for (int i=0; i<n; i++)
			{
				Person *man = _manCargo[i];
				if (man)
				{
					man->SetPilotLight(false);
					man->SwitchLight(false);
				}
			}
		}
		
		// assined to ...
		CHECK(ar.SerializeRef("GroupAssigned", _groupAssigned, 1))
		CHECK(ar.SerializeRef("DriverAssigned", _driverAssigned, 1))
		CHECK(ar.SerializeRef("GunnerAssigned", _gunnerAssigned, 1))
		CHECK(ar.SerializeRef("CommanderAssigned", _commanderAssigned, 1))
		CHECK(ar.SerializeRefs("CargoAssigned", _cargoAssigned, 1))

		CHECK(ar.SerializeRefs("GetoutUnits", _getoutUnits, 1))
		CHECK(ar.SerializeRefs("GetinUnits", _getinUnits, 1))
		CHECK(ar.Serialize("getinTime", _getinTime, 1))
		CHECK(ar.Serialize("getoutTime", _getoutTime, 1))

		CHECK(ar.Serialize("driverPos", _driverPos, 1, VZero))
		CHECK(ar.Serialize("dirWanted", _dirWanted, 1, VZero))

		CHECK(ar.SerializeEnum("moveMode", _moveMode, 1, VMMFormation))
		CHECK(ar.SerializeEnum("turnMode", _turnMode, 1, VTMNone))

		CHECK(ar.Serialize("comFireMode", _commanderFire._fireMode, 1, -1))
		CHECK(ar.Serialize("comFirePrepareOnly", _commanderFire._firePrepareOnly, 1, true))
		CHECK(ar.SerializeRef("comFireTarget", _commanderFire._fireTarget, 1))
		
	//	CHECK(ar.Serialize("lastStrategicIndex", _lastStrategicIndex, 1, -1))
	//	CHECK(ar.Serialize("useStrategicPlan", _useStrategicPlan, 1, 0))
	//	CHECK(ar.Serialize("strategicPlanValid", _strategicPlanValid, 1, 0))
		// TODO: check and remove  _fireEnabled

	//	CHECK(ar.Serialize("Planner", _planner, 1))
		CHECK(ar.Serialize("Radio", _radio, 1))

		if (ar.IsSaving() || ar.GetArVersion() >= 10)
			CHECK(ar.SerializeEnum("lock", _lock, 1, (LockState)LSDefault))	
		else if (ar.GetPass() == ParamArchive::PassFirst)
		{
			bool locked;
			CHECK(ar.Serialize("locked", locked, 1, false))	
			_lock = locked ? LSDefault : LSLocked;
		}

		CHECK(ar.Serialize("manualFire", _manualFire, 1, false))
		CHECK(ar.Serialize("turretFrontUntil", _turretFrontUntil, 1, TIME_MIN))
	}
	CHECK(ar.Serialize("fuel", _fuel, 1, Type()->GetFuelCapacity()))
	CHECK(ar.Serialize("engineOff", _engineOff, 1, false))
	return LSOK;
}

NetworkMessageType Transport::GetNMType(NetworkMessageClass cls) const
{
	switch (cls)
	{
	case NMCUpdateGeneric:
		return NMTUpdateTransport;
	default:
		return base::GetNMType(cls);
	}
}

IndicesUpdateTransport::IndicesUpdateTransport()
{
	commander = -1;
	driver = -1;
	gunner = -1;
	effCommander = -1;
	manCargo = -1;
	comFireTarget = -1;
	lock = -1;
	driverHiddenWanted = -1;
	fuel = -1;
	engineOff = -1;
}

void IndicesUpdateTransport::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);

	SCAN(commander)
	SCAN(driver)
	SCAN(gunner)
	SCAN(effCommander)
	SCAN(manCargo)
	SCAN(comFireTarget)
	SCAN(lock)
	SCAN(driverHiddenWanted)
	SCAN(fuel);
	SCAN(engineOff);
}

NetworkMessageIndices *GetIndicesUpdateTransport() {return new IndicesUpdateTransport();}

NetworkMessageFormat &Transport::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	switch (cls)
	{
	case NMCUpdateGeneric:
		base::CreateFormat(cls, format);
		format.Add("commander", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Commander (observer) person"), ET_NOT_EQUAL, ERR_COEF_STRUCTURE);
		format.Add("driver", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Driver person"), ET_NOT_EQUAL, ERR_COEF_STRUCTURE);
		format.Add("gunner", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Gunner person"), ET_NOT_EQUAL, ERR_COEF_STRUCTURE);
		format.Add("effCommander", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Currently commanding person"), ET_NOT_EQUAL, ERR_COEF_STRUCTURE);
		format.Add("manCargo", NDTRefArray, NCTNone, DEFVALUEREFARRAY, DOC_MSG("Men in cargo space"), ET_NOT_EQUAL_COUNT, 0.5 * ERR_COEF_STRUCTURE);
		format.Add("comFireTarget", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Fire target, selected by commander"), ET_NOT_EQUAL, ERR_COEF_MODE);
		format.Add("lock", NDTInteger, NCTSmallSigned, DEFVALUE(int, LSDefault), DOC_MSG("Lock state"), ET_NOT_EQUAL, ERR_COEF_MODE);
		format.Add("driverHiddenWanted", NDTFloat, NCTFloat0To1, DEFVALUE(float, 0), DOC_MSG("Wanted position of (tank) driver - inside / outside"), ET_NOT_EQUAL, ERR_COEF_VALUE_MAJOR);
		format.Add("fuel", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Current amount of fuel"), ET_ABS_DIF, ERR_COEF_VALUE_MINOR);
		format.Add("engineOff", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Engine is off / on"), ET_ABS_DIF, ERR_COEF_MODE);
		break;
	default:
		base::CreateFormat(cls, format);
		break;
	}
	return format;
}

CameraType ValidateCamera(CameraType cam);

bool IsInVehicle(Person *soldier, Transport *veh)
{
	if (veh->Commander() == soldier) return true;
	if (veh->Driver() == soldier) return true;
	if (veh->Gunner() == soldier) return true;
	for (int i=0; i<veh->GetManCargo().Size(); i++)
	{
		if (veh->GetManCargo()[i] == soldier) return true;
	}
	return false;
}

/*!
\patch 1.28 Date 10/30/2001 by Ondra
- Fixed: MP: bug in cargo position transfer. Caused problems when
unit from first cargo seat disembarked while someone was still
sitting in second. It could also lead to player animation corruption
or character duplication when player made double attempt to enter vehicle,
thinking first ateempt failed when result was lagged.
\patch_internal 1.28 Date 10/30/2001 by Ondra
- Fixed: GetInCargo can now be position specific
\patch 1.31 Date 11/07/2001 by Jirka
- Fixed: Problem with dead bodies inside vehicle in multiplayer
\patch 1.47 Date 3/7/2002 by Ondra
- Fixed: Different number of cargo slot could crash server or client in MP.
*/

TMError Transport::TransferMsg(NetworkMessageContext &ctx)
{
	switch (ctx.GetClass())
	{
	case NMCUpdateGeneric:
		TMCHECK(base::TransferMsg(ctx))
		{
			Assert(dynamic_cast<const IndicesUpdateTransport *>(ctx.GetIndices()))
			const IndicesUpdateTransport *indices = static_cast<const IndicesUpdateTransport *>(ctx.GetIndices());

			if (ctx.IsSending())
			{
				ITRANSF_REF(commander)
				ITRANSF_REF(driver)
				ITRANSF_REF(gunner)
				ITRANSF_REF(effCommander)
				RefArray<Person> manCargo;
				int n = _manCargo.Size();
				manCargo.Resize(n);
				for (int i=0; i<n; i++)
				{
					manCargo[i] = _manCargo[i];
				}
				TMCHECK(ctx.IdxTransferRefs(indices->manCargo, manCargo))
				EntityAI *target = NULL;
				if (_commanderFire._fireTarget) target = _commanderFire._fireTarget->idExact;
				TMCHECK(ctx.IdxTransferRef(indices->comFireTarget, target));
			}
			else
			{
				Person *soldier;
				TMCHECK(ctx.IdxTransferRef(indices->commander, soldier))
				if (soldier != _commander)
				{
					if (soldier && IsInVehicle(soldier, this))
					{
						ChangePosition(ATMoveToCommander, soldier);
					}
					else
					{
						if (_commander)
						{
							// get out
							//GetOutCommander(*_commander);
							AIUnit *unit = _commander->Brain();
							if (unit)
							{
								bool isFocused = (GWorld->FocusOn() == unit);
								unit->DoGetOut(this, false);
								if (isFocused)
									GWorld->SwitchCameraTo(unit->GetVehicle(), ValidateCamera(GWorld->GetCameraType()));
							}
							else
							{
								// FIX - enable get out dead body (without brain)
								// calculate get out position
								Vector3 pos = GetCommanderGetOutPos(_commander);
								Matrix4 transform;
								transform.SetPosition(pos);
								transform.SetUpAndDirection(VUp, pos - Position());
								// get out
								GetOutCommander(transform);
							}
						}
						if (soldier)
						{
							// get in
							GetInCommander(soldier);
							if (GWorld->GetRealPlayer() == soldier)
								GWorld->SwitchCameraTo(this, ValidateCamera(GWorld->GetCameraType()));
							AIUnit *unit = soldier->Brain();
							if (unit)
							{
								unit->AssignAsCommander(this);
								unit->OrderGetIn(true);
							}
						}
					}
				}
				TMCHECK(ctx.IdxTransferRef(indices->driver, soldier))
				if (soldier != _driver)
				{
					if (soldier && IsInVehicle(soldier, this))
					{
						ChangePosition(ATMoveToDriver, soldier);
					}
					else
					{
						if (_driver)
						{
							// get out
							//GetOutDriver(*_driver);
							AIUnit *unit = _driver->Brain();
							if (unit)
							{
								bool isFocused = (GWorld->FocusOn() == unit);
								unit->DoGetOut(this, false);
								if (isFocused)
									GWorld->SwitchCameraTo(unit->GetVehicle(), ValidateCamera(GWorld->GetCameraType()));
							}
							else
							{
								// FIX - enable get out dead body (without brain)
								// calculate get out position
								Vector3 pos = GetDriverGetOutPos(_driver);
								Matrix4 transform;
								transform.SetPosition(pos);
								transform.SetUpAndDirection(VUp, pos - Position());
								// get out
								GetOutDriver(transform);
							}
						}
						if (soldier)
						{
							// get in
							GetInDriver(soldier);
							if (GWorld->GetRealPlayer() == soldier)
								GWorld->SwitchCameraTo(this, ValidateCamera(GWorld->GetCameraType()));
							AIUnit *unit = soldier->Brain();
							if (unit)
							{
								unit->AssignAsDriver(this);
								unit->OrderGetIn(true);
							}
						}
					}
				}
				TMCHECK(ctx.IdxTransferRef(indices->gunner, soldier))
				if (soldier != _gunner)
				{
					if (soldier && IsInVehicle(soldier, this))
					{
						ChangePosition(ATMoveToGunner, soldier);
					}
					else
					{
						if (_gunner)
						{
							// get out
							//GetOutGunner(*_gunner);
							AIUnit *unit = _gunner->Brain();
							if (unit)
							{
								bool isFocused = (GWorld->FocusOn() == unit);
								unit->DoGetOut(this, false);
								if (isFocused)
									GWorld->SwitchCameraTo(unit->GetVehicle(), ValidateCamera(GWorld->GetCameraType()));
							}
							else
							{
								// FIX - enable get out dead body (without brain)
								// calculate get out position
								Vector3 pos = GetGunnerGetOutPos(_gunner);
								Matrix4 transform;
								transform.SetPosition(pos);
								transform.SetUpAndDirection(VUp, pos - Position());
								// get out
								GetOutGunner(transform);
							}
						}
						if (soldier)
						{
							// get in
							GetInGunner(soldier);
							if (GWorld->GetRealPlayer() == soldier)
								GWorld->SwitchCameraTo(this, ValidateCamera(GWorld->GetCameraType()));
							AIUnit *unit = soldier->Brain();
							if (unit)
							{
								unit->AssignAsGunner(this);
								unit->OrderGetIn(true);
							}
						}
					}
				}
				RefArray<Person> manCargo;
				TMCHECK(ctx.IdxTransferRefs(indices->manCargo, manCargo))
				//DoAssert(manCargo.Size() == _manCargo.Size());
				for (int i=0; i<_manCargo.Size(); i++)
				{
					soldier = i<manCargo.Size() ? manCargo[i] : NULL;
					if (soldier != _manCargo[i])
					{
						if (_manCargo[i])
						{
							// get out
							//GetOutCargo(_manCargo[i], *_manCargo[i]);
							AIUnit *unit = _manCargo[i]->Brain();
							if (unit)
							{
								bool isFocused = (GWorld->FocusOn() == unit);
								unit->DoGetOut(this, false);
								if (isFocused)
									GWorld->SwitchCameraTo(unit->GetVehicle(), ValidateCamera(GWorld->GetCameraType()));
							}
							else
							{
								// FIX - enable get out dead body (without brain)
								// calculate get out position
								Vector3 pos = GetCargoGetOutPos(_manCargo[i]);
								Matrix4 transform;
								transform.SetPosition(pos);
								transform.SetUpAndDirection(VUp, pos - Position());
								// get out
								GetOutCargo(_manCargo[i], transform);
							}
						}
						if (soldier)
						{
							// get in
							GetInCargo(soldier,true,i);
							if (GWorld->GetRealPlayer() == soldier)
								GWorld->SwitchCameraTo(this, ValidateCamera(GWorld->GetCameraType()));
							AIUnit *unit = soldier->Brain();
							if (unit)
							{
								unit->AssignAsCargo(this);
								unit->OrderGetIn(true);
							}
						}
					}
				}
				
				EntityAI *target = NULL;
				_commanderFire._fireTarget = NULL;
				TMCHECK(ctx.IdxTransferRef(indices->comFireTarget, target));
				if (target)
				{
					AIUnit *unit = CommanderUnit();
					AIGroup *grp = unit ? unit->GetGroup() : NULL;
					if (grp)
					{
						_commanderFire._fireTarget = grp->FindTarget(target);
					}
				}
				
			}
			ITRANSF_ENUM(lock)
			ITRANSF(driverHiddenWanted)
			if (ctx.IsSending())
			{
				ITRANSF(engineOff)
				ITRANSF(fuel)
			}
			else
			{
				bool engineOff;
				float fuel;
				TMCHECK(ctx.IdxTransfer(indices->engineOff, engineOff))
				TMCHECK(ctx.IdxTransfer(indices->fuel, fuel))
				if (engineOff!=_engineOff)
				{
					_engineOff = engineOff;
					OnEvent(EEEngine,_engineOff);
				}
				if ((_fuel>0)!=(fuel>0))
				{
					OnEvent(EEFuel,fuel>0);
				}
				_fuel = fuel;

			}
		}
		break;
	default:
		return base::TransferMsg(ctx);
	}
	return TMOK;
}

float Transport::CalculateError(NetworkMessageContext &ctx)
{
	float error = 0;
	switch (ctx.GetClass())
	{
	case NMCUpdateGeneric:
		{
			error += base::CalculateError(ctx);

			Assert(dynamic_cast<const IndicesUpdateTransport *>(ctx.GetIndices()))
			const IndicesUpdateTransport *indices = static_cast<const IndicesUpdateTransport *>(ctx.GetIndices());

			ICALCERR_NEQREF(Person, driver, ERR_COEF_STRUCTURE)
			ICALCERR_NEQREF(Person, commander, ERR_COEF_STRUCTURE)
			ICALCERR_NEQREF(Person, gunner, ERR_COEF_STRUCTURE)

			RefArray<Person> manCargo;
			if (ctx.IdxTransferRefs(indices->manCargo, manCargo) == TMOK)
			{
				const float coef = 0.5 * ERR_COEF_STRUCTURE;
				for (int i=0; i<_manCargo.Size(); i++)
				{
					Person *soldier = i<manCargo.Size() ? manCargo[i] : NULL;
					if (soldier != _manCargo[i]) error += coef;
				}
			}
			EntityAI *target = NULL;
			if (_commanderFire._fireTarget) target = _commanderFire._fireTarget->idExact;
			ICALCERRE_NEQREF(Object, comFireTarget, target, ERR_COEF_MODE)

			ICALCERR_NEQ(int, lock, ERR_COEF_STRUCTURE)
			ICALCERR_ABSDIF(float, driverHiddenWanted, ERR_COEF_VALUE_MAJOR)
			ICALCERR_ABSDIF(float, fuel, ERR_COEF_VALUE_MINOR)
			ICALCERR_NEQ(bool, engineOff, ERR_COEF_MODE)
		}
		break;
	default:
		error += base::CalculateError(ctx);
		break;
	}
	return error;
}

/*!
\patch 1.37 Date 12/18/2001 by Jirka
- Fixed: MP parachute - get out units on clients when parachute destroyed
*/
void Transport::DestroyObject()
{
	if (_commander)
	{
		AIUnit *unit = _commander->Brain();
		if (unit)
		{
			// get out
			bool isFocused = (GWorld->FocusOn() == unit);
			unit->DoGetOut(this, false);
			if (isFocused)
				GWorld->SwitchCameraTo(unit->GetVehicle(), ValidateCamera(GWorld->GetCameraType()));
		}
		else
		{
			// calculate get out position
			Vector3 pos = GetCommanderGetOutPos(_commander);
			Matrix4 transform;
			transform.SetPosition(pos);
			transform.SetUpAndDirection(VUp, pos - Position());
			// get out
			GetOutCommander(transform);
		}
	}
	if (_driver)
	{
		AIUnit *unit = _driver->Brain();
		if (unit)
		{
			// get out
			bool isFocused = (GWorld->FocusOn() == unit);
			unit->DoGetOut(this, false);
			if (isFocused)
				GWorld->SwitchCameraTo(unit->GetVehicle(), ValidateCamera(GWorld->GetCameraType()));
		}
		else
		{
			// calculate get out position
			Vector3 pos = GetDriverGetOutPos(_driver);
			Matrix4 transform;
			transform.SetPosition(pos);
			transform.SetUpAndDirection(VUp, pos - Position());
			// get out
			GetOutDriver(transform);
		}
	}
	if (_gunner)
	{
		AIUnit *unit = _gunner->Brain();
		if (unit)
		{
			// get out
			bool isFocused = (GWorld->FocusOn() == unit);
			unit->DoGetOut(this, false);
			if (isFocused)
				GWorld->SwitchCameraTo(unit->GetVehicle(), ValidateCamera(GWorld->GetCameraType()));
		}
		else
		{
			// calculate get out position
			Vector3 pos = GetGunnerGetOutPos(_gunner);
			Matrix4 transform;
			transform.SetPosition(pos);
			transform.SetUpAndDirection(VUp, pos - Position());
			// get out
			GetOutGunner(transform);
		}
	}
	for (int i=0; i<_manCargo.Size(); i++)
	{
		if (_manCargo[i])
		{
			AIUnit *unit = _manCargo[i]->Brain();
			if (unit)
			{
				// get out
				bool isFocused = (GWorld->FocusOn() == unit);
				unit->DoGetOut(this, false);
				if (isFocused)
					GWorld->SwitchCameraTo(unit->GetVehicle(), ValidateCamera(GWorld->GetCameraType()));
			}
			else
			{
				// calculate get out position
				Vector3 pos = GetCargoGetOutPos(_manCargo[i]);
				Matrix4 transform;
				transform.SetPosition(pos);
				transform.SetUpAndDirection(VUp, pos - Position());
				// get out
				GetOutCargo(_manCargo[i], transform);
			}
		}
	}

	base::DestroyObject();
}
