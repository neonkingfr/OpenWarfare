// implementation of OperMap 

#include "wpch.hpp"

#include "ai.hpp"
#include "aiDefs.hpp"
#include "operMap.hpp"
#include "landscape.hpp"
#include "debugWin.hpp"
#include "house.hpp"

#include "perfProf.hpp"
#include "diagModes.hpp"

#define USE_NEW_ASTAR	1

#if USE_NEW_ASTAR
#include "aStar.hpp"

struct ASOField
{
	union
	{
		int key;
		struct
		{
			short int x;
			short int z;
		} coord;
	} u;
	BYTE dir;
	bool house;
	ASOField(int x, int z, int d, bool h) {u.coord.x = x; u.coord.z = z; dir = d; house = h;}
	bool operator == (const ASOField &with) const {return with.u.key == u.key;}
	int GetKey() const {return u.key;}
};

typedef AStarNode<ASOField> ASONode;
TypeIsSimple(ASONode *)
DEFINE_FAST_ALLOCATOR(ASONode)

class ASOCostFunction
{
protected:
	OperMap *_map;
	bool _locks;
	bool _soldier;
	OLink<EntityAI> _vehicle;

public:
	ASOCostFunction(OperMap *map, bool locks, EntityAI *vehicle, bool soldier)
	{
		_map = map;
		_locks = locks;
		_vehicle = vehicle;
		_soldier = soldier;
	}
	float operator () (const ASOField &field1, const ASOField &field2) const;

protected:
	float GetFieldCost(int x, int z) const;
};

float ASOCostFunction::operator () (const ASOField &field1, const ASOField &field2) const
{
	int xf = field1.u.coord.x;
	int zf = field1.u.coord.z;
	int xt = field2.u.coord.x;
	int zt = field2.u.coord.z;

	if (field2.house)
	{
		// special case - path inside house
		Object *house = _map->FindDoor(xf, zf, xt, zt);
		Assert(house);
		Assert(house->GetIPaths());

		return house->GetIPaths()->GetBType()->GetCoefInsideHeur() *
			OperItemGrid *
			sqrt(Square(xt - xf) + Square(zt - zf)) *
			_vehicle->GetType()->GetMinCost();
	}
	if (field1.house)
	{
		// special case - exitting doors
		return OperItemGrid * sqrt(Square(xt - xf) + Square(zt - zf)) * _vehicle->GetType()->GetMinCost();
	}

	const float H_SQRT5 = 2.2360679775;
	const float H_SQRT5_4 = 0.25 * H_SQRT5;
	const float H_SQRT2_2 = 0.5 * H_SQRT2;

	int dx = xt - xf;
	int dz = zt - zf;

	float result = GetFieldCost(xf, zf) + GetFieldCost(xt, zt);
	switch (dx)
	{
	case -2:
		switch (dz)
		{
		case -1:
			result += GetFieldCost(xf - 1, zf - 1);
			result += GetFieldCost(xf - 1, zf);
			result *= H_SQRT5_4;
			break;
		case 0:
			result *= 0.5;
			result += GetFieldCost(xf - 1, zf);
			break;
		case 1:
			result += GetFieldCost(xf - 1, zf);
			result += GetFieldCost(xf - 1, zf + 1);
			result *= H_SQRT5_4;
			break;
		case -2:
		case 2:
			Fail("Unused");
			break;
		}
		break;
	case -1:
		switch (dz)
		{
		case -2:
			result += GetFieldCost(xf - 1, zf - 1);
			result += GetFieldCost(xf, zf - 1);
			result *= H_SQRT5_4;
			break;
		case -1:
			result *= H_SQRT2_2;
			break;
		case 0:
			result *= 0.5;
			break;
		case 1:
			result *= H_SQRT2_2;
			break;
		case 2:
			result += GetFieldCost(xf - 1, zf + 1);
			result += GetFieldCost(xf, zf + 1);
			result *= H_SQRT5_4;
			break;
		}
		break;
	case 0:
		switch (dz)
		{
		case -2:
			result *= 0.5;
			result += GetFieldCost(xf, zf - 1);
			break;
		case -1:
			result *= 0.5;
			break;
		case 1:
			result *= 0.5;
			break;
		case 2:
			result *= 0.5;
			result += GetFieldCost(xf, zf + 1);
			break;
		case 0:
			Fail("Unused");
			break;
		}
		break;
	case 1:
		switch (dz)
		{
		case -2:
			result += GetFieldCost(xf, zf - 1);
			result += GetFieldCost(xf + 1, zf - 1);
			result *= H_SQRT5_4;
			break;
		case -1:
			result *= H_SQRT2_2;
			break;
		case 0:
			result *= 0.5;
			break;
		case 1:
			result *= H_SQRT2_2;
			break;
		case 2:
			result += GetFieldCost(xf, zf + 1);
			result += GetFieldCost(xf + 1, zf + 1);
			result *= H_SQRT5_4;
			break;
		}
		break;
	case 2:
		switch (dz)
		{
		case -1:
			result += GetFieldCost(xf + 1, zf - 1);
			result += GetFieldCost(xf + 1, zf);
			result *= H_SQRT5_4;
			break;
		case 0:
			result *= 0.5;
			result += GetFieldCost(xf + 1, zf);
			break;
		case 1:
			result += GetFieldCost(xf + 1, zf);
			result += GetFieldCost(xf + 1, zf + 1);
			result *= H_SQRT5_4;
			break;
		case -2:
		case 2:
			Fail("Unused");
			break;
		}
		break;
	}

	int dirD = field2.dir - field1.dir;
	if (dirD >= 8)
		dirD -= 16;
	else if (dirD < -8)
		dirD += 16;
	if (dirD != 0)
		result += _vehicle->GetCostTurn(dirD);

	if (_soldier && result >= GET_UNACCESSIBLE)
	{
		// search for doors
		OperDoor *door = _map->FindDoor(xt, zt);
		if (door)
		{
			// special case - entering doors
			result = OperItemGrid * sqrt(Square(xt - xf) + Square(zt - zf)) * _vehicle->GetType()->GetMinCost();
		}
	}

	return result;
}

float ASOCostFunction::GetFieldCost(int x, int z) const
{
	Assert(_map);
	return _map->GetFieldCost(x, z, _locks, _vehicle, _soldier);
}

class ASOHeuristicFunction
{
protected:
	float _coef;
	OLink<EntityAI> _vehicle;

public:
	ASOHeuristicFunction(float coef, EntityAI *vehicle) {_coef = coef; _vehicle = vehicle;}
	float operator () (const ASOField &field1, const ASOField &field2) const
	{
		if (field1 == field2) return 0;

		int xs = field1.u.coord.x;
		int zs = field1.u.coord.z;
		int xe = field2.u.coord.x;
		int ze = field2.u.coord.z;
		int dx = abs(xs - xe);
		int dz = abs(zs - ze);
/*
		float minD = (dx + dz - fabs(dx - dz)) * 0.5;
		float maxD = (dx + dz + fabs(dx - dz)) * 0.5;
		return _coef * ((maxD - minD) + H_SQRT2 * minD);
*/
		// optimization
		float dif = fabs(dx - dz);
		float result = _coef * (dif + (0.5f * H_SQRT2) * (dx + dz - dif));

		// direction penalty
		if (field1.dir != 0xee)
		{
			int dirD = AI::CalcDirection(Vector3(xe - xs, 0, ze - zs)) - field1.dir;
			if (dirD >= 8)
				dirD -= 16;
			else if (dirD < -8)
				dirD += 16;
			if (dirD != 0)
				result += _vehicle->GetCostTurn(1) * abs(dirD);
		}

		return result;
	}
};

class ASOIterator
{
protected:
	int _x, _z;
	int _index;
	int _doorFrom;
	int _doorTo;
	OperMap *_map;

public:
	ASOIterator(const ASOField &field, void *context);
	operator bool () const {return _index < 8;}
	void operator ++ ();
	operator ASOField ();
};

ASOIterator::ASOIterator(const ASOField &field, void *context)
{
	_x = field.u.coord.x; _z = field.u.coord.z;
	_map = reinterpret_cast<OperMap *>(context);
	_doorFrom = 0;
	_doorTo = -1;
	if (_map)
	{
		_index = -1;
		++(*this);
	}
	else _index = 0;
}

void ASOIterator::operator ++ ()
{
	if (_index >= 0) _index++;
	else
	{
		Assert(_map);
		while (_doorFrom < _map->_doors.Size())
		{
			OperDoor &doorFrom = _map->_doors[_doorFrom];
			if (!doorFrom.house || doorFrom.x != _x || doorFrom.z != _z)
			{
				_doorFrom++;
				continue;
			}
			while (++_doorTo < _map->_doors.Size())
			{
				if (_doorTo == _doorFrom) continue;
				OperDoor &doorTo = _map->_doors[_doorTo];
				if (doorTo.house == doorFrom.house) return;
			}
			_doorFrom++;
			_doorTo = -1;
		};
		_index = 0;
	}
}

ASOIterator::operator ASOField ()
{
	if (_index >= 0)
		return ASOField
		(
			_x + direction_delta[2 * _index][0],
			_z + direction_delta[2 * _index][1],
			direction_delta[2 * _index][2],
			false
		);
	else
	{
		Assert(_map);
		OperDoor &doorTo = _map->_doors[_doorTo];
		return ASOField(doorTo.x, doorTo.z, 0xee, true);
	}
}

struct ASOOpenListTraits
{
	static bool IsLess(const ASONode *a, const ASONode *b) {return a->_f < b->_f;}
	static bool IsLessOrEqual(const ASONode *a, const ASONode *b){return a->_f <= b->_f;}
};
class ASOOpenList : public HeapArray<ASONode *, MemAllocD, ASOOpenListTraits>
{
typedef HeapArray<ASONode *, MemAllocD, ASOOpenListTraits> base;
public:
	void UpdateUp(ASONode *node) {base::HeapUpdateUp(node);}
	void Add(ASONode *node) {base::HeapInsert(node);}
	bool RemoveFirst(ASONode *&node) {return base::HeapRemoveFirst(node);}
	const ASONode *GetFirst() const {return (*this)[0];}
};

class ASONodeRef : public SRef<ASONode>
{
	typedef SRef<ASONode> base;

public:
	ASONodeRef() {}
	ASONodeRef(ASONode *node) : base(node) {}
	int GetKey() const {return (*this)->_field.u.key;}
};
TypeIsMovableZeroed(ASONodeRef)

struct ASOClosedListTraits
{
	//! key type
	typedef int KeyType;
	//! calculate hash value
	static unsigned int CalculateHashValue(KeyType key)
	{
		return (unsigned int)key;
	}

	//! compare keys, return negative when k1<k2, positive when k1>k2, zero when equal
	static int CmpKey(KeyType k1, KeyType k2)
	{
		return k1 - k2;
	}
  static KeyType GetKey(const ASONodeRef &item) {return item.GetKey();}
};
class ASOClosedList : public MapStringToClass<ASONodeRef, AutoArray<ASONodeRef>, ASOClosedListTraits>
{
typedef MapStringToClass<ASONodeRef, AutoArray<ASONodeRef>, ASOClosedListTraits> base;

public:
	int Add(ASONode *node) {return base::Add(node);}
};

typedef AStar
<
	ASOField,
	ASOCostFunction, ASOHeuristicFunction,
	ASOIterator,
	ASOClosedList, ASOOpenList
> AStarOperative;
#endif

#if 0
inline float OperHeuristic( float dx, float dz )
{
	dx=fabs(dx);
	dz=fabs(dz);
	float minD=(dx+dz-fabs(dx-dz))*0.5;
	float maxD=(dx+dz+fabs(dx-dz))*0.5;
	return (maxD - minD) + H_SQRT2 * minD;
}
#else
// optimized
inline float OperHeuristic( float dx, float dz )
{
	dx=fabs(dx);
	dz=fabs(dz);
	float sum=dx+dz;
	float adif=fabs(dx-dz);
	float minD2=(sum-adif);
	float maxD2=(sum+adif);
	return (maxD2 - minD2)*0.5 + ( H_SQRT2 *0.5 )* minD2;
}
#endif


int AI::CalcDirection(Vector3 direction)
{
	// TODO: avoid atan2
	// 16 directions (+-8)

	float angle = atan2(direction.X(), direction.Z()) + H_PI;
	// angle is from 0 to 2*pi

	angle *= 8 / H_PI;
	//int dir = toInt(angle) & (Directions - 1);
	int dir = toInt(angle);
	if( dir>16 ) dir-=16;
	if( dir<0 ) dir+=16;
	return dir;
}

#if _ENABLE_CHEATS

#include "debugWinImpl.hpp"

// class DebugWindowOperMap
class DebugWindowOperMap : public DebugWindow
{
protected:
	OperMap *_map;
	OLink<AIUnit> _unit;
	int _xs, _zs, _xe, _ze;
	int _searchID;
	bool _unitAssigned;
	float _heurCost;

public:
	DebugWindowOperMap();
	void Attach(OperMap *map, AIUnit *unit, int xs, int zs, int xe, int ze, float heurCost);
	void Detach();
	void AssignUnit(AIUnit *unit) {_unit = unit; _unitAssigned = true;}
	void UnassignUnit() {_unitAssigned = false;}
	void SetSearchID(int id) {_searchID = id;}
	bool IsAttached() const {return _map != NULL;}

	void OnPaint( const OnPaintContext &dc);
};

DebugWindowOperMap::DebugWindowOperMap()
: DebugWindow("Debug - Operative Map")
{
	_map = NULL;
	_unit = NULL;
	_unitAssigned = false;
}

#include "global.hpp"

void DebugWindowOperMap::Attach(OperMap *map, AIUnit *unit, int xs, int zs, int xe, int ze, float heurCost)
{
	if (_unitAssigned)
	{
		if (_unit != unit) return;
	}
	else
		_unit = unit;
	_map = map;
	_xs = xs;
	_zs = zs;
	_xe = xe;
	_ze = ze;
	_heurCost = heurCost;
	_searchID = -1;
	if (unit)
	{
		EntityAI *veh = unit->GetVehicle();
		RString title =
		(
			RString("Operative map for ") +
			veh->GetType()->GetDisplayName() + RString(" ") +
			unit->GetDebugName()
		);
		char buf[256];
		sprintf(buf,",t: %.2f, iter %d", Glob.time-Time(0), unit->GetIter());
		title = title + RString(buf);
		float invMinCost = veh->GetType()->GetMaxSpeedMs();
		sprintf(buf,",h: %.2f", heurCost * invMinCost);
		title = title + RString(buf);
		HWND hwnd=GetWindowHandle(this);
		SetWindowText(hwnd, title);
	}
}

void DebugWindowOperMap::Detach()
{
	if (_map)
	{
		char text[1024];
		HWND hwnd=GetWindowHandle(this);
		GetWindowText(hwnd,text,sizeof(text));
		strcat(text,"[Finished]");
		SetWindowText(hwnd,text);
		_map = NULL;
	}
}

void DebugWindowOperMap::OnPaint(const OnPaintContext &pc)
{
	HDC dc=pc.dc;
	if (!_map) return;
	if (!_unit) return;

	if (_map->_fields.Size() == 0) return;

	int xMin = INT_MAX;
	int xMax = INT_MIN;
	int zMin = INT_MAX;
	int zMax = INT_MIN;

	for (int i=0; i<_map->_fields.Size(); i++)
	{
		OperField *field = _map->_fields[i];
		saturateMin(xMin, field->_x);
		saturateMax(xMax, field->_x);
		saturateMin(zMin, field->_z);
		saturateMax(zMax, field->_z);
	}
	
	const int itemSize = 8;
	const int fieldSize = OperItemRange * itemSize;

	HWND hwnd=GetWindowHandle(this);

	RECT clientRect;
	GetClientRect(hwnd, &clientRect);

	int w = fieldSize * (xMax - xMin + 1);
	int h = fieldSize * (zMax - zMin + 1);

	if (clientRect.bottom > h)
	{
		RECT rect;
		rect.left = 0;
		rect.right = clientRect.right;
		rect.top = h;
		rect.bottom = clientRect.bottom;
		HBRUSH hbrush = (HBRUSH)GetStockObject(WHITE_BRUSH);
		FillRect(dc, &rect, hbrush);
		clientRect.bottom = h;
	}
	if (clientRect.right > w)
	{
		RECT rect;
		rect.left = w;
		rect.right = clientRect.right;
		rect.top = 0;
		rect.bottom = clientRect.bottom;
		HBRUSH hbrush = (HBRUSH)GetStockObject(WHITE_BRUSH);
		FillRect(dc, &rect, hbrush);
		clientRect.right = w;
	}

	EntityAI *veh = _unit->GetVehicle();
	bool soldier = _unit->IsFreeSoldier();
//	AIGroup *grp = _unit->GetGroup();

	float unaccLimit = veh->GetType()->GetKind() == VArmor ? GET_UNACCESSIBLE : 4.0;
//	float invMinCost = veh->GetType()->GetMaxSpeedMs();
	float invHeurCost = 1.0 / _heurCost;

	// field costs and locks
	HPEN lockPen = CreatePen(PS_SOLID, 0, RGB(255, 0, 255));
	HGDIOBJ oldPen = SelectObject(dc, lockPen);
	for (int i=0; i<_map->_fields.Size(); i++)
	{
		OperField *field = _map->_fields[i];
		LockField *locks = GLandscape->LockingCache()->FindLockField
		(
			field->_x, field->_z
		);
		float baseCost = field->_baseCost;

		RECT rect;
		float left = fieldSize * (field->_x - xMin); 
		rect.bottom = fieldSize * (zMax + 1 - field->_z);
		for (int z=0; z<OperItemRange; z++)
		{
			rect.top = rect.bottom - itemSize;
			rect.left = left;
			for (int x=0; x<OperItemRange; x++)
			{
				OperItem &item = field->_items[z][x];
				rect.right = rect.left + itemSize;
				OperItemType type = soldier ?
					item._typeSoldier :
					item._type;
				float cost = veh->GetTypeCost(type);
				COLORREF color;
				if (cost >= unaccLimit)
					color = RGB(255, 0, 0);
				else
				{
					cost *= baseCost;// * invMinCost;
					if (cost <= _heurCost)
					{
						int green = toIntFloor(cost * invHeurCost * 256.0);
						saturate(green, 0, 255);
						color = RGB(0, (BYTE)green, 0);
					}
					else
					{
						int red = toIntFloor((cost - _heurCost) * invHeurCost * 64.0);
						saturate(red, 0, 127);
						color = RGB(255, (BYTE)(255 - red), 0);
					}
				}
				HBRUSH hbrush = CreateSolidBrush(color);
				FillRect(dc, &rect, hbrush);
				DeleteObject(hbrush);
				
				if (locks && locks->IsLocked(x, z, soldier))
				{
					MoveToEx(dc, rect.left, rect.top, NULL);
					LineTo(dc, rect.right, rect.bottom);
					MoveToEx(dc, rect.left, rect.bottom, NULL);
					LineTo(dc, rect.right + 1, rect.top);
				}

				rect.left = rect.right;
			}
			rect.bottom = rect.top;
		}
	}
	SelectObject(dc, oldPen);
	DeleteObject(lockPen);

	// doors
	HGDIOBJ oldBrush = SelectObject(dc, GetStockObject(NULL_BRUSH));
	oldPen = SelectObject(dc, GetStockObject(WHITE_PEN));
	for (int i=0; i<_map->_doors.Size(); i++)
	{
		OperDoor &door = _map->_doors[i];
		int left = (door.x - OperItemRange * xMin) * itemSize;
		int bottom = (OperItemRange * (zMax + 1) - door.z) * itemSize;
		int right = left + itemSize;
		int top = bottom - itemSize;
		Ellipse(dc, left, top, right, bottom);
	}
	SelectObject(dc, oldPen);
	SelectObject(dc, oldBrush);

	// path
	HPEN openPen = CreatePen(PS_SOLID, 0, RGB(191, 191, 255));
	HPEN closedPen = CreatePen(PS_SOLID, 0, RGB(0, 0, 0));
	oldPen = SelectObject(dc, GetStockObject(NULL_PEN));
	for (int i=0; i<_map->_fields.Size(); i++)
	{
		OperField *field = _map->_fields[i];
		for (int z=0; z<OperItemRange; z++)
			for (int x=0; x<OperItemRange; x++)
			{
				OperItem &item = field->_items[z][x];
				if (item._searchID != _searchID) continue;
				if (!item._parent) continue;
				int x1 = (item._x - OperItemRange * xMin) * itemSize + itemSize / 2;
				int z1 = (OperItemRange * (zMax + 1) - item._z) * itemSize - itemSize / 2;
				int x2 = (item._parent->_x - OperItemRange * xMin) * itemSize + itemSize / 2;
				int z2 = (OperItemRange * (zMax + 1) - item._parent->_z) * itemSize - itemSize / 2;
				SelectObject(dc, item._open ? openPen : closedPen);
				MoveToEx(dc, x1, z1, NULL);
				LineTo(dc, x2, z2);
			}
	}
	SelectObject(dc, oldPen);
	DeleteObject(closedPen);
	DeleteObject(openPen);
		
	// start & end point
	oldPen = SelectObject(dc, GetStockObject(WHITE_PEN));
	HBRUSH sbrush = CreateSolidBrush(RGB(255, 0, 255));
	HBRUSH ebrush = CreateSolidBrush(RGB(0, 255, 255));
	oldBrush = SelectObject(dc, sbrush);
	{
		int left = (_xs - OperItemRange * xMin) * itemSize;
		int bottom = (OperItemRange * (zMax + 1) - _zs) * itemSize;
		int right = left + itemSize;
		int top = bottom - itemSize;
		Ellipse(dc, left + 1, top + 1, right - 1, bottom - 1);
	}
	SelectObject(dc, ebrush);
	{
		int left = (_xe - OperItemRange * xMin) * itemSize;
		int bottom = (OperItemRange * (zMax + 1) - _ze) * itemSize;
		int right = left + itemSize;
		int top = bottom - itemSize;
		Ellipse(dc, left + 1, top + 1, right - 1, bottom - 1);
	}
	SelectObject(dc, oldBrush);
	DeleteObject(ebrush);
	DeleteObject(sbrush);
	SelectObject(dc, oldPen);
}

static Link<DebugWindowOperMap> GOperMapDebug;
static bool DebugAutoHide;

void DebugOperMap()
{
	if (!GOperMapDebug)
	{
		GOperMapDebug = new DebugWindowOperMap();
		DebugAutoHide = false;
	}
	GOperMapDebug->UnassignUnit();
}

void DebugOperMap(AIUnit *unit)
{
	if (!GOperMapDebug)
	{
		GOperMapDebug = new DebugWindowOperMap();
		DebugAutoHide = false;
	}
	GOperMapDebug->AssignUnit(unit);
}

static bool showTrouble=false;
void DebugOperMapTrouble()
{
	showTrouble = !showTrouble;
	GlobalShowMessage(500,"Show path troubles %s",showTrouble ? "On" : "Off");
}

#endif

// class OperMap
OperMap::OperMap()
{
	InitStaticStorage();
	_alternateGoal = false;
}

OperMap::~OperMap()
{
}

void OperMap::InitStaticStorage()
{
	static StaticStorage< Ref<OperField> > fieldsStorage;
	static StaticStorage<OperInfo> pathStorage;
	static StaticStorage<OperDoor> doorStorage;
	_fields.SetStorage(fieldsStorage.Init(256));
	_doors.SetStorage(doorStorage.Init(256));
	_path.SetStorage(pathStorage.Init(64));
}

OperItem* OperMap::Item(int x, int z)
{
	if (x < 0 || x >= OperItemRange * LandRange || z < 0 || z >= OperItemRange * LandRange)
		return NULL;

	int x0 = x / OperItemRange;
	int z0 = z / OperItemRange;
	for (int i=0; i<_fields.Size(); i++)
		if (_fields[i]->_x == x0 && _fields[i]->_z == z0)
		{
			x0 = x - x0 * OperItemRange;
			z0 = z - z0 * OperItemRange;
			return &_fields[i]->_items[z0][x0];
		}
	return NULL;
}

#define FC(x,z) GetFieldCost(x, z, locks, veh, soldier)

float OperMap::GetCost(int xf, int zf, int dir, bool locks, EntityAI *veh, bool soldier)
{
	const float H_SQRT5 = 2.2360679775;
	const float H_SQRT5_4 = 0.25 * H_SQRT5;
	const float H_SQRT2_2 = 0.5 * H_SQRT2;
	switch (dir)
	{
		case 0:
			return 0.5 * (FC(xf, zf) + FC(xf, zf - 1));
		case 1:
			return H_SQRT5_4 * (FC(xf, zf) + FC(xf, zf - 1) + FC(xf - 1, zf - 1) + FC(xf - 1, zf - 2));
		case 2:
			return H_SQRT2_2 * (FC(xf, zf) + FC(xf - 1, zf - 1));
		case 3:
			return H_SQRT5_4 * (FC(xf, zf) + FC(xf - 1, zf) + FC(xf - 1, zf - 1) + FC(xf - 2, zf - 1));
		case 4:
			return 0.5 * (FC(xf, zf) + FC(xf - 1, zf));
		case 5:
			return H_SQRT5_4 * (FC(xf, zf) + FC(xf - 1, zf) + FC(xf - 1, zf + 1) + FC(xf - 2, zf + 1));
		case 6:
			return H_SQRT2_2 * (FC(xf, zf) + FC(xf - 1, zf + 1));
		case 7:
			return H_SQRT5_4 * (FC(xf, zf) + FC(xf, zf + 1) + FC(xf - 1, zf + 1) + FC(xf - 1, zf + 2));
		case 8:
			return 0.5 * (FC(xf, zf) + FC(xf, zf + 1));
		case 9:
			return H_SQRT5_4 * (FC(xf, zf) + FC(xf, zf + 1) + FC(xf + 1, zf + 1) + FC(xf + 1, zf + 2));
		case 10:
			return H_SQRT2_2 * (FC(xf, zf) + FC(xf + 1, zf + 1));
		case 11:
			return H_SQRT5_4 * (FC(xf, zf) + FC(xf + 1, zf) + FC(xf + 1, zf + 1) + FC(xf + 2, zf + 1));
		case 12:
			return 0.5 * (FC(xf, zf) + FC(xf + 1, zf));
		case 13:
			return H_SQRT5_4 * (FC(xf, zf) + FC(xf + 1, zf) + FC(xf + 1, zf - 1) + FC(xf + 2, zf - 1));
		case 14:
			return H_SQRT2_2 * (FC(xf, zf) + FC(xf + 1, zf - 1));
		case 15:
			return H_SQRT5_4 * (FC(xf, zf) + FC(xf, zf - 1) + FC(xf + 1, zf - 1) + FC(xf + 1, zf - 2));
		case 16:
			return 0.5 * (FC(xf, zf) + FC(xf, zf - 2)) + FC(xf, zf - 1);
		case 17:
			return 0.5 * (FC(xf, zf) + FC(xf - 2, zf)) + FC(xf - 1, zf);
		case 18:
			return 0.5 * (FC(xf, zf) + FC(xf, zf + 2)) + FC(xf, zf + 1);
		case 19:
			return 0.5 * (FC(xf, zf) + FC(xf + 2, zf)) + FC(xf + 1, zf);
	}
	Fail("Unaccessible for 20 directions.");
	return 0;
}

float OperMap::GetFieldCost(int x, int z, bool locks, EntityAI *veh, bool soldier)
{
	if (x < 0 || x >= OperItemRange * LandRange || z < 0 || z >= OperItemRange * LandRange)
		return SET_UNACCESSIBLE;

	if (locks && GLOB_LAND->LockingCache()->IsLocked(x, z, soldier))
		return SET_UNACCESSIBLE;

	int x0 = x / OperItemRange;
	int z0 = z / OperItemRange;
	for (int i=0; i<_fields.Size(); i++)
		if (_fields[i]->_x == x0 && _fields[i]->_z == z0)
		{
			x0 = x - x0 * OperItemRange;
			z0 = z - z0 * OperItemRange;
			OperItemType type = soldier ?
				_fields[i]->_items[z0][x0]._typeSoldier :
				_fields[i]->_items[z0][x0]._type;
			return _fields[i]->_baseCost * veh->GetTypeCost(type);
		}
	// field not found
#if FIELD_ON_DEMAND
	int mask = MASK_AVOID_OBJECTS | MASK_PREFER_ROADS | MASK_USE_BUFFER;
	OperField *field = CreateField(x0, z0, mask, veh);
	Assert(field);
	x0 = x - x0 * OperItemRange;
	z0 = z - z0 * OperItemRange;
	OperItemType type = soldier ?
		field->_items[z0][x0]._typeSoldier :
		field->_items[z0][x0]._type;
	return field->_baseCost * veh->GetTypeCost(type);
#else
	return SET_UNACCESSIBLE;
#endif
}

void OperMap::ClearMap()
{
	_fields.Clear();
	_doors.Clear();
}

OperField *OperMap::CreateField(int x, int z, int mask, EntityAI *veh)
{
	// x,z are in LandGrid units
	if (!InRange(z, x))
		return NULL;

	OperField *fld = NULL;
	// lookup map
	int n = _fields.Size();
	if (n>50*50)
	{
		LogF("Very large OperMap");
	}
	for (int i=0; i<n; i++)
		if (_fields[i]->IsField(x, z))
		{
			fld = _fields[i];
			break; // already in map
		}

	
	if (fld == NULL)
	{
		if (mask & MASK_USE_BUFFER)
		{
			#if _ENABLE_AI
			fld = GLOB_LAND->OperationalCache()->GetOperField(x, z, mask);
			#else
			Fail("OperField Cache used.");
			#endif
		}
		else
			fld = new OperField(x, z, mask);
		_fields.Add(fld);
		for (int i=0; i<fld->_doors.Size(); i++)
		{
			_doors.Add(fld->_doors[i]);
		}
	}
	
	Assert( InRange(x,z) );
	GeographyInfo geogr = GLOB_LAND->GetGeography(x, z);
	fld->_baseCost = OperItemGrid * veh->GetCost(geogr);
	fld->_heurCost = fld->_baseCost * veh->GetFieldCost(geogr);

	return fld;
}

#include "scene.hpp"

bool OperMap::FindNearestEmpty
(
	int& x, int& z,  float xf, float zf,
	int xbase, int zbase, int xsize, int zsize,
	bool locks, EntityAI *veh, bool soldier,
	FindNearestEmptyCallback *isFree, void *context
)
{
	// x, z, xbase, zbase are in OperItemGrid units
	// xf,zf have the same meaning as x,z, but are with sub-sqaure precision
	int xrest = x - xbase;
	int zrest = z - zbase;
	int xt, zt;
	int t;
	bool inner;
	int nMax = xrest;
	if (zrest > nMax) nMax = zrest;
	t = xsize - 1 - xrest;
	if (t > nMax) nMax = t;
	t = zsize - 1 - zrest;
	if (t > nMax) nMax = t;

	AssertDebug( nMax<160 );
	if( nMax>=160 ) return false;

	const float operItemGrid = OperItemGrid;

	// for soldier check also position inside building
	float nearestDist2=1e10;
	#if 1
	if (soldier)
	{
		float posX = xf*operItemGrid;
		float posZ = zf*operItemGrid;
		Vector3 pos = GLandscape->PointOnSurface(posX,0,posZ);
		// convert to LandGrid units
		int xLand = x/OperItemRange;
		int zLand = z/OperItemRange;
		for (int xx=xLand-1; xx<=xLand+1; xx++)
		for (int zz=zLand-1; zz<=zLand+1; zz++)
		{
			if (!InRange(xx, zz)) continue;
			const ObjectList &list = GLandscape->GetObjects(zz,xx);
			for (int o=0; o<list.Size(); o++)
			{
				const Object *obj = list[o];
				const IPaths *paths = obj->GetIPaths();
				if (!paths) continue;
				Vector3 retPos;
				if (paths->FindNearestPoint(pos,retPos) < 0) continue;
				float dist2 = retPos.Distance2(pos);
				if (nearestDist2>dist2)
				{
					nearestDist2 = dist2;
					x = toIntFloor(retPos.X()*InvOperItemGrid);
					z = toIntFloor(retPos.Z()*InvOperItemGrid);
				}
			}
		}
		// limit nMax with position already found in house
		if (nearestDist2<1e9)
		{
			float maxDist = sqrt(nearestDist2)*H_SQRT2;
			int maxDistGrid = toIntCeil(maxDist*InvOperItemGrid);
			saturateMin(nMax,maxDistGrid);

#if _ENABLE_CHEATS
			if (CHECK_DIAG(DECostMap))
			{
				float xPos = operItemGrid*x;
				float zPos = operItemGrid*z;
				float yPos = GLandscape->SurfaceYAboveWater(xPos,zPos);
				Vector3 dPos(xPos,yPos,zPos);

				Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),-1);
				obj->SetPosition(dPos);
				obj->SetScale(0.5);
				obj->SetConstantColor(PackedColor(Color(1,1,1)));
				GLandscape->ShowObject(obj);
			}
#endif
		}
	}
	#endif

	for (int n=1; n<nMax; n++)
	{

		#define IS_FREE(xg, zg) \
			isFree \
			( \
				Vector3 \
				( \
					(xg)*operItemGrid+0.5*operItemGrid, \
					0, \
					(zg)*operItemGrid+0.5*operItemGrid \
				),context \
			)

		#define CHECK_EMPTY \
			float dist2 = Square(xbase+xt-xf)+Square(zbase+zt-zf); \
			if (dist2<nearestDist2) \
			{ \
				nearestDist2 = dist2; \
				x = xbase + xt; \
				z = zbase + zt; \
				saturateMin(nMax,i+1); \
			}
			// if something found, limit number of iterations

		for (int i=0; i<=n; i++)
		{
			inner = 0 < i && i < n;
			xt = xrest - n;
			if (xt >= 0)
			{
				zt = zrest + i;
				if
				(
					zt < zsize && FC(xbase + xt, zbase + zt) < GET_UNACCESSIBLE &&
					IS_FREE(xbase + xt, zbase + zt) ||
					inner && (zt = zrest - i) >= 0 &&
					FC(xbase + xt, zbase + zt) < GET_UNACCESSIBLE &&
					IS_FREE(xbase + xt, zbase + zt)
				)
				{
					CHECK_EMPTY
				}
			}
			zt = zrest - n;
			if (zt >= 0)
			{
				xt = xrest + i;
				if
				(
					xt < xsize && FC(xbase + xt, zbase + zt) < GET_UNACCESSIBLE &&
					IS_FREE(xbase + xt, zbase + zt) ||
					inner && (xt = xrest - i) >= 0 &&
					FC(xbase + xt, zbase + zt) < GET_UNACCESSIBLE &&
					IS_FREE(xbase + xt, zbase + zt)
				)
				{
					CHECK_EMPTY
				}
			}
			xt = xrest + n;
			if (xt < xsize)
			{
				zt = zrest - i;
				if
				(
					zt >= 0 && FC(xbase + xt, zbase + zt) < GET_UNACCESSIBLE &&
					IS_FREE(xbase + xt, zbase + zt) ||
					inner && (zt = zrest + i) < zsize &&
					FC(xbase + xt, zbase + zt) < GET_UNACCESSIBLE &&
					IS_FREE(xbase + xt, zbase + zt)
				)
				{
					CHECK_EMPTY
				}
			}
			zt = zrest + n;
			if (zt < zsize)
			{
				xt = xrest - i;
				if
				(
					xt >=0  && FC(xbase + xt, zbase + zt) < GET_UNACCESSIBLE &&
					IS_FREE(xbase + xt, zbase + zt) ||
					inner && (xt = xrest + i) < xsize &&
					FC(xbase + xt, zbase + zt) < GET_UNACCESSIBLE &&
					IS_FREE(xbase + xt, zbase + zt)
				) 
				{
					CHECK_EMPTY
				}
			}
		}		
	}
	return nearestDist2<1e9;
}

void OperMap::LogMap(int xMin, int xMax, int zMin, int zMax, 
	int xs, int xe, int zs, int ze, bool locks)
{
#if _ENABLE_CHEATS
	int xsize = (xMax - xMin + 1) * OperItemRange;
	int zsize = (zMax - zMin + 1) * OperItemRange;
	int i, j;

	LogF("Fields: %d..%d, %d..%d", xMax, xMin, zMin, zMax);

	Temp< Temp<char> > map(zsize);
	for (i=0; i<zsize; i++)
		map[i].Realloc(xsize*2 + 1),memset(map[i],' ',xsize*2);

	for (i=zMin*OperItemRange; i<(zMax+1)*OperItemRange; i++)
	{
		for (j=xMin*OperItemRange; j<(xMax+1)*OperItemRange; j++)
		{
			if (!InRange(
				toIntFloor(i / OperItemRange),
				toIntFloor(j / OperItemRange))) continue;

			OperItem* item = NULL;
			OperField* fld = NULL;
			int x0 = j / OperItemRange;
			int z0 = i / OperItemRange;
			for (int k=0; k<_fields.Size(); k++)
				if (_fields[k]->_x == x0 && _fields[k]->_z == z0)
				{
					fld = _fields[k];
					x0 = j - x0 * OperItemRange;
					z0 = i - z0 * OperItemRange;
					item = &fld->_items[z0][x0];
				}

			Assert(fld && item);

			char ch = '?';
			if (locks && GLOB_LAND->LockingCache()->IsLocked(j, i, false)) ch = 'L';
			else if (!item)
				ch = ' ';
			else
			{
				switch (item->_typeSoldier)
				{
				case OITNormal:
					ch = '.';
					break;
				case OITAvoidBush:
					ch = 'b';
					break;
				case OITAvoidTree:
					ch = 't';
					break;
				case OITAvoid:
					ch = 'x';
					break;
				case OITWater:
					ch = 'W';
					break;
				case OITSpaceRoad:
					ch = 'R';
					break;
				case OITRoad:
					ch = 'r';
					break;
				case OITSpaceBush:
					ch = 'B';
					break;
				case OITSpaceTree:
					ch = 'T';
					break;
				case OITSpace:
					ch = 'X';
					break;
				case OITRoadForced:
					ch = 'F';
					break;
				}
			}
			map[i - zMin * OperItemRange][(xsize - 1 - (j - xMin * OperItemRange))*2] = ch;
		}
		map[i - zMin * OperItemRange][xsize*2] = 0;
	}
	for (i=0; i<_path.Size(); i++)
	{
		int iz = _path[i]._z - zMin * OperItemRange;
		if (iz < 0 || iz >= zsize)
			continue;
		int ix = xsize - 1 - (_path[i]._x - xMin * OperItemRange);
		if (ix < 0 || ix >= xsize)
			continue;
		map[iz][2 * ix + 1] = '+';
	}
	map[zs - zMin * OperItemRange][(xsize - 1 - (xs - xMin * OperItemRange))*2 + 1] = 'S';
	map[ze - zMin * OperItemRange][(xsize - 1 - (xe - xMin * OperItemRange))*2 + 1] = 'E';
	LogF("Begin loop ... %d, begin at %d, %d", zsize, xsize - 1 - (xs - xMin * OperItemRange), zs - zMin * OperItemRange);
	for (i=0; i<zsize; i++)
		LogF("%02d:%s", i, map[i]);
	LogF("End loop ...");
	Sleep(50);
#endif
}

bool OperMap::IsIntersection(int xs, int zs, int xe, int ze,
				float &cost, float &costPerItem, EntityAI *veh, bool soldier)
{
	// returned values:
	// cost is total cost of direct path
	// costPerItem is cost per single item (rectangle of size OperItemGrid)
	if (xs==xe && zs==ze)
	{
		cost = 0;
		costPerItem = 1;
		return false;
	}
	float dx = xe - xs;
	float dz = ze - zs;
	int incz = zs < ze ? 1 : -1;
	int incx = xs < xe ? 1 : -1;
	cost = 0;

//LogF("Check intersection %d, %d -> %d, %d", xs, zs, xe, ze);	
	if (fabs(dx) < fabs(dz))
	{
		float invabsdz = 1.0 / fabs(dz);
		dx *= invabsdz;
		float invdx = dx!=0 ? 1.0 / dx : 1e10;
		float coef = sqrt(1 + Square(dx));
		float x = xs + 0.5;
		for (int z=zs; z!=ze+incz; z+=incz)
		{
			x += 0.5 * dx;
			if (toIntFloor(x) != xs)
			{
				Assert(toIntFloor(x) == xs + incx);
				float cost1 = GetFieldCost(xs, z, true, veh, soldier);
				float cost2 = GetFieldCost(xs + incx, z, true, veh, soldier);
				float a;
				if (dx > 0)
					a = (x - (xs + 1)) * invdx;
				else
					a = (x - xs) * invdx; 
				if (a < 0.99f && cost1 >= GET_UNACCESSIBLE)
					return true;
				if (a > 0.01f && cost2 >= GET_UNACCESSIBLE)
					return true;
				if (z!=zs) cost += a * cost2 + (1.0 - a) * cost1;
				xs += incx;
			}
			else
			{
				float cost1 = GetFieldCost(xs, z, true, veh, soldier);
				if (cost1 >= GET_UNACCESSIBLE)
					return true;
				if (z!=zs) cost += cost1;
			}
			x += 0.5 * dx;
		}
		costPerItem = cost * invabsdz;
		cost *= coef;
	}
	else
	{
		float invabsdx = 1.0 / fabs(dx);
		dz *= invabsdx;
		float invdz = dz!=0 ? 1.0 / dz : 1e10;
		float coef = sqrt(1 + Square(dz));
		float z = zs + 0.5;
		for (int x=xs; x!=xe+incx; x+=incx)
		{
			z += 0.5 * dz;
			if (toIntFloor(z) != zs)
			{
//				Assert(toIntFloor(z) == zs + incz);
				float cost1 = GetFieldCost(x, zs, true, veh, soldier);
				float cost2 = GetFieldCost(x, zs + incz, true, veh, soldier);
				float a;
				if (dz > 0)
					a = (z - (zs + 1)) * invdz;
				else
					a = (z - zs) * invdz; 
				if (a < 0.99f && cost1 >= GET_UNACCESSIBLE)
					return true;
				if (a > 0.01f && cost2 >= GET_UNACCESSIBLE)
					return true;
				if (x!=xs) cost += a * cost2 + (1.0 - a) * cost1;
				zs += incz;
			}
			else
			{
				float cost1 = GetFieldCost(x, zs, true, veh, soldier);
				if (cost1 >= GET_UNACCESSIBLE)
					return true;
				if (x!=xs) cost += cost1;
			}
			z += 0.5 * dz;
		}
		costPerItem = cost * invabsdx;
		cost *= coef;
	}
	return false;
}

void OperMap::CreateMap(EntityAI *veh, int xMin, int zMin, int xMax, int zMax)
{
	int mask = MASK_AVOID_OBJECTS | MASK_PREFER_ROADS | MASK_USE_BUFFER;

// Create map
	int i, j;
	for (i=xMin; i<=xMax; i++)
		for (j=zMin; j<=zMax; j++)
		{
			if (!InRange(i, j)) continue;
			CreateField(i, j, mask, veh);
		}
}

bool OperMap::IsSimplePath(AIUnit* unit, int xs, int zs, int xe, int ze)
{
	float simpleCost, simpleHeurCost;
	return !IsIntersection(xs, zs, xe, ze, simpleCost, simpleHeurCost, unit->GetVehicle(), unit->IsSoldier());
}

#define MAX_ITER									1000

bool operator <(OperItem &a, OperItem &b)
{
	return a._cost + a._heur < b._cost + b._heur;
}

bool operator <=(OperItem &a, OperItem &b)
{
	return a._cost + a._heur <= b._cost + b._heur;
}

TypeIsSimple(OperItem *);
typedef HeapArray<OperItem *,MemAllocSS> OperOpenList;

#define PERF_STATS 0

#if PERF_STATS
	#include "perfLog.hpp"
#endif

#if _DEBUG
	const int UpdateSleep=1000;
	const int FinishSleep=1000;
#else
	const int UpdateSleep=100; // time to sleep after single interation
	const int FinishSleep=1000; // time to sleep after finishing search
#endif

#if _ENABLE_AI

bool OperMap::FindPath
(
	AIUnit* unit, int dir,
	int xs, int zs, int xe, int ze,
	bool locks
)
{
	PROFILE_SCOPE(fpAll);

#if LOG_MAPS>=2 || LOG_PROBL
	int xMin = LandRangeMask;
	int xMax = 0;
	int zMin = LandRangeMask;
	int zMax = 0;
	for (int i=0; i<_fields.Size(); i++)
	{
		OperField *fld = _fields[i];
		if (fld->_x < xMin) xMin = fld->_x;
		if (fld->_x > xMax) xMax = fld->_x;
		if (fld->_z < zMin) zMin = fld->_z;
		if (fld->_z > zMax) zMax = fld->_z;
	}
#endif

// LogF("Searching from: %d, %d to %d, %d", xs, zs, xe, ze);

	_alternateGoal = false;

	// scan for min,max,avg cost and do some arithmetics with results
	float maxCost = 0, minCost = GET_UNACCESSIBLE, avgCost = 0;
	int cntCost = 0;
	
	int n = _fields.Size();
	for (int i=0; i<n; i++)
	{
		OperField *fld = _fields[i];
		float cost = fld->HeurCost();
		if (cost < GET_UNACCESSIBLE)
		{
			saturateMax(maxCost, cost);
			saturateMin(minCost, cost);
			avgCost += cost;
			cntCost++;
		}
	}
	avgCost /= cntCost;

	float heurCost;
	// TODO: tune coefs
	switch (unit->GetIter())
	{
		case 0:
			heurCost = avgCost * 0.8 + minCost * 0.2;
			break;
		case 1:
			heurCost = maxCost * 0.4 + avgCost * 0.6;
			break;
		case 2:
		default:
			heurCost = maxCost;
			break;
	}

	EntityAI *veh = unit->GetVehicle();
	bool isSoldier = unit->IsSoldier();

	//DoAssert (GetFieldCost(xs, zs, true, veh, isSoldier)<GET_UNACCESSIBLE);
	//DoAssert (GetFieldCost(xe, ze, true, veh, isSoldier)<GET_UNACCESSIBLE);


#if _ENABLE_CHEATS
	if (GOperMapDebug)
	{
		GOperMapDebug->Attach(this, unit, xs, zs, xe, ze, heurCost);
		if (GOperMapDebug->IsAttached())
		{
			GOperMapDebug->Update();
			Sleep(UpdateSleep);
		}
	}
#endif

//	Check if simplified methods can be used
	float simpleCost, simpleHeurCost;
	if (!IsIntersection(xs, zs, xe, ze, simpleCost, simpleHeurCost, veh, isSoldier))
	{
#if LOG_MAPS
			LogF
			(
				"Unit %s: Direct path %s, heur %.3f, found %.3f",
				(const char *)unit->GetDebugName(),
				simpleHeurCost <= 1.01f * heurCost ? "found" : "not found",
				heurCost,simpleHeurCost
			);
#endif
		// check if we match heuristic estimation
		if (simpleHeurCost <= 1.01f * heurCost)
		{
SimplePath:
			_path.Resize(2);
			_path[0]._x = xs;
			_path[0]._z = zs;
			_path[0].house = NULL;
			_path[0].from = -1;
			_path[0].to = -1;
			_path[0]._cost = 0;
			_path[1]._x = xe;
			_path[1]._z = ze;
			_path[1].house = NULL;
			_path[1].from = -1;
			_path[1].to = -1;
			_path[1]._cost = simpleCost;
#if _ENABLE_CHEATS
			if (GOperMapDebug && GOperMapDebug->IsAttached())
			{
				GOperMapDebug->Update();
				Sleep(FinishSleep);
				GOperMapDebug->Detach();
				if (DebugAutoHide) WaitForClose(GOperMapDebug);
				//if (DebugAutoHide) GOperMapDebug->Close();
			}
#endif
			return true;
		}
		else
		{
			// TODO: verify difference between using and not using following statement
			//heurCost = simpleHeurCost; // use simple heur cost 
		}
	}

#if USE_NEW_ASTAR
	OperItem *test = Item(xs, zs);
	if (!test)
	{
		// cannot plan operative path outside map
		ErrF("Out of map");
		
		// error recovery - try to use simple path
		goto SimplePath;
	}

	bool house = isSoldier && FindDoor(xs, zs);

	ASOField start(xs, zs, dir, house);
	ASOField end(xe, ze, 0xff, false);
	ASOCostFunction costFunction(this, locks, veh, isSoldier);
	ASOHeuristicFunction heuristicFunction(heurCost, veh);
	SRef<AStarOperative> algorithm = new AStarOperative
	(
		start, end,
		costFunction, heuristicFunction,
		GET_UNACCESSIBLE
	);

	algorithm->Process(MAX_ITER, isSoldier ? this : NULL);

	const ASONode *last = NULL;
	if (algorithm->IsDone() && algorithm->IsFound())
	{
		// path found
		last = algorithm->GetLastNode();
	}
	else
	{
		// path not found - try to select alternate goal
		last = algorithm->GetBestNode();
		if (!last) return false;
		if (last->_field == start) return false; // singular path
		if (last->_f - last->_g >= heuristicFunction(start, end)) return false;
		_alternateGoal = true;
	}

	// export path
	int depth = 0;
	for (const ASONode *cur=last; cur!=NULL; cur=cur->_parent) depth++;
	_path.Resize(depth);
	int i = depth - 1;
	for (const ASONode *cur=last; cur != NULL; cur=cur->_parent)
	{
		OperInfo &info = _path[i--];
		info._cost = cur->_g;
		info._x = cur->_field.u.coord.x;
		info._z = cur->_field.u.coord.z;

		if (isSoldier && cur->_field.house)
		{
			if (cur->_parent)
			{
				Assert(FindDoor
				(
					info,
					cur->_parent->_field.u.coord.x, cur->_parent->_field.u.coord.z,
					cur->_field.u.coord.x, cur->_field.u.coord.z
				));
			}
			else
			{
				OperDoor *door = FindDoor(cur->_field.u.coord.x, cur->_field.u.coord.z);
				Assert(door);
				if (door)
				{
					info.house = door->house;
					info.from = door->exit;
					info.to = door->exit;
				}
			}
		}
	}
	return true;

#else
	int searchID = GLOB_LAND->LockingCache()->SearchID();
	int xc, zc;
	int xx, zz;
	int iter = 0;

	float cost, heur;
	
	static StaticStorage<OperItem *> openListStorage;
	OperOpenList openList;
	// use static storage to contain typical searches
	openList.SetStorage(openListStorage.Init(1024));

	OperItem *best, *cur;

	best = Item(xs, zs);
	if (!best)
	{
		// cannot plan operative path outside map
		ErrF("Out of map");
		
		// error recovery - try to use simple path
		goto SimplePath;
	}
	best->_x = xs;
	best->_z = zs;
	best->_dir = dir;
	best->_cost = 0;
	int dirD;
	if (xe == xs && ze == zs)
		best->_heur = 0;
	else
	{
		best->_heur = OperHeuristic(xe-xs, ze-zs) * heurCost;
		dirD = AI::CalcDirection(Vector3(xe-xs, 0, ze-zs)) - dir;
		if (dirD >= 8)
			dirD -= 16;
		else if (dirD < -8)
			dirD += 16;
		if (dirD != 0)
			best->_heur += veh->GetCostTurn(1)*abs(dirD);
	}
	best->_parent = NULL;
//	best->_depth = 0;
	best->_open = true;
	best->_searchID = searchID;

	bool soldier = unit->IsFreeSoldier();	// use doors only for soldiers
	if (soldier)
	{
		for (int d=0; d<_doors.Size(); d++)
		{
			OperDoor &doorIn = _doors[d];
			if (!doorIn.house) continue;
			if (doorIn.x != xs) continue;
			if (doorIn.z != zs) continue;
			best->_dir = 0xee; // start in doors
			break;
		}
	}

	openList.Add(best);	// openList is empty, HeapInsert == Add

#if _ENABLE_CHEATS
	if (GOperMapDebug && GOperMapDebug->IsAttached())
	{
		GOperMapDebug->SetSearchID(searchID);
	}
#endif
	
	while (1)	// search cycle
	{
#if _ENABLE_CHEATS
		if (GOperMapDebug && GOperMapDebug->IsAttached())
		{
			// do not update on every iteration
			if( (iter&31)==0 )
			{
				GOperMapDebug->Update();
				Sleep(UpdateSleep);
			}
		}
#endif
		
		bool ok = openList.HeapRemoveFirst(best);
		if (!ok || ++iter > MAX_ITER)
		{
//LogF("%s: %d iters", (const char *)unit->GetDebugName(), iter);
			// try to found alternate path (to the point nearest to goal)
			// found alternate goal at open list
			best = NULL;
			float minDist2 = Square(xe - xs) + Square(ze - zs);
			for (int i=0; i<openList.Size(); i++)
			{
				cur = openList[i];
				float dist2 = Square(xe - cur->_x) + Square(ze - cur->_z);
				if (dist2 < minDist2)
				{
					minDist2 = dist2;
					best = cur;
				}
			}
			if (best)
			{
#if PERF_STATS
				ADD_COUNTER(aiFNC,1);
				ADD_COUNTER(aiFSI,iter);
#endif
#if LOG_MAPS
				LogF("Unit %s: Alternate path found in %d iters.", (const char *)unit->GetDebugName(), iter);
#endif
				float dist2 = Square(xs-best->_x)+Square(zs-best->_z);

				if (Square(veh->GetPrecision())>dist2*Square(OperItemGrid))
				{
					// path to short - do not consider it as solution
					goto PathNotFound;

				}
				_alternateGoal = true;
				goto PathFound;
			}
			else
			{
				PathNotFound:
				#if PERF_STATS
					ADD_COUNTER(aiFNC,1);
					ADD_COUNTER(aiFSI,iter);
				#endif
				LogF("Unit %s: Path not found in %d iters.", (const char *)unit->GetDebugName(), iter);
#if LOG_PROBL
				LogMap(xMin, xMax, zMin, zMax, xs, xe, zs, ze, locks);
#endif
#if _ENABLE_CHEATS
				if (GOperMapDebug && GOperMapDebug->IsAttached())
				{
					GOperMapDebug->Update();
					Sleep(FinishSleep);
					GOperMapDebug->Detach();
					if (DebugAutoHide) WaitForClose(GOperMapDebug);
					//if (DebugAutoHide) GOperMapDebug->Close();
				}
#endif
				return false; // no solution
			}
		}


		#if _ENABLE_CHEATS
		if(iter==700)
		{
			#define DIAG_LARGE_SEARCH 0
			#if DIAG_LARGE_SEARCH
			char buf[512];
			sprintf
			(
				buf,"Unit %s: from %d,%d to %d,%d, Time %.2f",
				(const char *)unit->GetDebugName(),xs,zs,xe,ze,Glob.time-Time(0)
			);
			//RptF("%s",buf);
			static char lastBuf[sizeof(buf)];
			if( !strcmp(lastBuf,buf) )
			{
				Fail("Same path search twice");
			}
			else
			{
				strcpy(lastBuf,buf);
			}
			#endif

			if (showTrouble)
			{
				// diagnose pathfinding slowdown
				bool doAttach = !GOperMapDebug;
				if (doAttach)
				{
					GOperMapDebug = new DebugWindowOperMap();
					DebugAutoHide = true;
				}
				if( GOperMapDebug )
				{
					if( doAttach )
					{
						GOperMapDebug->Attach(this, unit, xs, zs, xe, ze, heurCost);
						GOperMapDebug->SetSearchID(searchID);
					}
					GOperMapDebug->Update();
					Sleep(UpdateSleep);
				}
			}
		}
		#endif

		best->_open = false;
		xc = best->_x;
		zc = best->_z;
//LogF("Best: %d, %d", xc, zc);
		if (xc == xe && zc == ze)
		{
//LogF("%s: %d iters", (const char *)unit->GetDebugName(), iter);
#if PERF_STATS
			ADD_COUNTER(aiFPC,1);
			ADD_COUNTER(aiFSI,iter);
#endif
#if LOG_MAPS
			LogF("Unit %s: Path found in %d iters.", (const char *)unit->GetDebugName(), iter);
#endif
//			DoAssert(best->_depth >= 1);

PathFound:
			cur = best;
			int i = -1;
			while (cur)
			{
				i++;
				cur = cur->_parent;
			}
			//			i = best->_depth;
			_path.Resize(i + 1);
			cur = best;
			while (cur)
			{
				OperInfo &info = _path[i--];
				info._cost = cur->_cost;
				info._x = cur->_x;
				info._z = cur->_z;
				if (soldier && cur->_dir == 0xee)
				{
					if (cur->_parent)
					{
						//int exit1 = -1;
						//int exit2 = -1;
						//Object *house = NULL;

						int x = cur->_parent->_x;
						int z = cur->_parent->_z;
						for (int d=0; d<_doors.Size(); d++)
						{
							OperDoor &door = _doors[d];
							if (door.x == x && door.z == z && door.house)
							{
								info.house = door.house;
								info.from = door.exit;
								{
									int x = cur->_x;
									int z = cur->_z;
									for (int d=0; d<_doors.Size(); d++)
									{
										OperDoor &door = _doors[d];
										if (door.x == x && door.z == z && door.house == info.house)
										{
											info.to = door.exit;
											goto TeleportFound;
										}
									}
								}
							}
						}

TeleportFound:;
						// TODO: make assert working
						// no variable tested here is ever set
						//Assert(house && exit1 >= 0 && exit2 >= 0);
					}
					else
					{
						int x = cur->_x;
						int z = cur->_z;
						for (int d=0; d<_doors.Size(); d++)
						{
							OperDoor &door = _doors[d];
							if (door.x == x && door.z == z && door.house)
							{
								info.house = door.house;
								info.from = door.exit;
								info.to = door.exit;
								break;
							}
						}
					}
				}

				cur = cur->_parent;
			}
#if _ENABLE_CHEATS
			if (GOperMapDebug && GOperMapDebug->IsAttached())
			{
				GOperMapDebug->Update();
				Sleep(FinishSleep);
				GOperMapDebug->Detach();
				if( DebugAutoHide )
				{
					
					GOperMapDebug->Close();
				}
			}
#endif
#if LOG_MAPS
	#if LOG_MAPS>=2
			LogMap(xMin, xMax, zMin, zMax, xs, xe, zs, ze, locks);
	#endif
#endif
			return _path.Size() >= 2; // path found - is nontrivial?
		}
		
		//for (i=0; i<Directions; i++)	// generate successors
		for (int i=0; i<16; i+=2)	// generate successors - only 8 directions
		{
			xx = xc + direction_delta[i][0];
			zz = zc + direction_delta[i][1];
			int iDir = direction_delta[i][2];

			// cost value of the field
			if (best->_dir == 0xee)
			{
				// from doors (doors may be on unaccessible field)
				cost =
					OperItemGrid *
					sqrt(Square(xx - xc) + Square(zz - zc)) *
					veh->GetType()->GetMinCost();
			}
			else
			{
				cost = GetCost(xc, zc, i, locks, veh, isSoldier);
				// affect by change of direction
				dirD = iDir - best->_dir;
				if (dirD >= 8)
					dirD -= 16;
				else if (dirD < -8)
					dirD += 16;
				if (dirD != 0)
					cost += veh->GetCostTurn(dirD);
			}
			// add to aggregate cost
			Assert( best->_cost>=0 );
			if (soldier && best->_cost + cost >= GET_UNACCESSIBLE)
			{
				// search for doors
				bool door = false;
				for (int d=0; d<_doors.Size(); d++)	// generate successors
				{
					OperDoor &doorIn = _doors[d];
					if (!doorIn.house) continue;
					if (doorIn.x != xx) continue;
					if (doorIn.z != zz) continue;
					door = true;
					break;
				}
				// to doors (doors may be on unaccessible field)
				if (door)
				{
					cost =
						OperItemGrid *
						sqrt(Square(xx - xc) + Square(zz - zc)) *
						veh->GetType()->GetMinCost();
				}
				else
					continue;
			}
			cost += best->_cost;

			if (xx == xe && zz == ze)
				heur = 0;
			else
			{
				heur = OperHeuristic(xx-xe, zz-ze) * heurCost;
				dirD = AI::CalcDirection(Vector3(xe-xx, 0, ze-zz)) - i;
				if (dirD >= 8)
					dirD -= 16;
				else if (dirD < -8)
					dirD += 16;
				if (dirD != 0)
					heur += veh->GetCostTurn(1)*abs(dirD);
			}
//LogF("Open: %d, %d, dir %d, cost %.1f, heur %.1f", xx, zz, i, cost, heur);
			cur = Item(xx, zz);
			if (!cur) continue;
			if (cur->_searchID == searchID)
			{
				if (cur->_open)
				{
					if (cost + heur < cur->_cost + cur->_heur)
					{
						cur->_dir = i;
						cur->_cost = cost;
						cur->_heur = heur;
						cur->_parent = best;
//						cur->_depth = best->_depth + 1;
						openList.HeapUpdateUp(cur);
					}
					else
					{
						continue;
					}
				}
				else
				{
					continue;
				}
			}
			else
			{
				cur->_x = xx;
				cur->_z = zz;
				cur->_dir = i;
				cur->_cost = cost;
				cur->_heur = heur;
				cur->_parent = best;
//				cur->_depth = best->_depth + 1;
				cur->_open = true;
				cur->_searchID = searchID;
				openList.HeapInsert(cur);
			}
		}				// end of generate successors
		if (!soldier) continue;
		for (int i=0; i<_doors.Size(); i++)	// generate successors
		{
			OperDoor &doorIn = _doors[i];
//LogF("Doors: %d at %d, %d", doorIn.house.GetLink(), doorIn.x, doorIn.z);
			if (!doorIn.house) continue;
			if (doorIn.x != xc) continue;
			if (doorIn.z != zc) continue;
			for (int j=0; j<_doors.Size(); j++)
			{
				if (j == i) continue;
				OperDoor &doorOut = _doors[j];
				if (doorOut.house != doorIn.house) continue;

				xx = doorOut.x;
				zz = doorOut.z;
				Assert(doorIn.house->GetIPaths());
				cost =
					doorIn.house->GetIPaths()->GetBType()->GetCoefInsideHeur() *
					OperItemGrid *
					sqrt(Square(xx - xc) + Square(zz - zc)) *
					veh->GetType()->GetMinCost();
				// add to aggregate cost
				Assert( best->_cost>=0 );
				cost += best->_cost;
				if (cost >= GET_UNACCESSIBLE)
					continue;

				if (xx == xe && zz == ze)
					heur = 0;
				else
				{
					heur = OperHeuristic(xx - xe, zz - ze) * heurCost;
					if (best->_dir != 0xee)
					{
						dirD = AI::CalcDirection(Vector3(xe-xx, 0, ze-zz)) - best->_dir;
						if (dirD >= 8)
							dirD -= 16;
						else if (dirD < -8)
							dirD += 16;
						if (dirD != 0)
							heur += veh->GetCostTurn(1)*abs(dirD);
					}
				}
//LogF("Open (doors used): %d, %d, dir %d, cost %.1f, heur %.1f", xx, zz, i, cost, heur);
				cur = Item(xx, zz);
				if (!cur) continue;
				if (cur->_searchID == searchID)
				{
					if (cur->_open)
					{
						if (cost + heur < cur->_cost + cur->_heur)
						{
							cur->_dir = 0xee;
							cur->_cost = cost;
							cur->_heur = heur;
							cur->_parent = best;
//							cur->_depth = best->_depth + 1;
							openList.HeapUpdateUp(cur);
						}
						else
						{
							continue;
						}
					}
					else
					{
						continue;
					}
				}
				else
				{
					cur->_x = xx;
					cur->_z = zz;
					cur->_dir = 0xee;
					cur->_cost = cost;
					cur->_heur = heur;
					cur->_parent = best;
//					cur->_depth = best->_depth + 1;
					cur->_open = true;
					cur->_searchID = searchID;
					openList.HeapInsert(cur);
				}
			}
		}				// end of generate successors
	}					// end of search cycle
#endif
}

OperDoor *OperMap::FindDoor(int x, int z)
{
	for (int d=0; d<_doors.Size(); d++)
	{
		OperDoor &door = _doors[d];
		if (door.x == x && door.z == z && door.house) return &door;
	}
	return NULL;
}

OperDoor *OperMap::FindDoor(int x, int z, Object *house)
{
	for (int d=0; d<_doors.Size(); d++)
	{
		OperDoor &door = _doors[d];
		if (door.x == x && door.z == z && door.house == house) return &door;
	}
	return NULL;
}

bool OperMap::FindDoor(OperInfo &info, int xs, int zs, int xe, int ze)
{
	for (int d=0; d<_doors.Size(); d++)
	{
		OperDoor &door1 = _doors[d];
		if (door1.x != xs || door1.z != zs || !door1.house) continue;
		OperDoor *door2 = FindDoor(xe, ze, door1.house);
		if (!door2) continue;

		// teleport found
		info.house = door1.house;
		info.from = door1.exit;
		info.to = door2->exit;
		return true;
	}
	return false;
}

Object *OperMap::FindDoor(int xs, int zs, int xe, int ze)
{
	for (int d=0; d<_doors.Size(); d++)
	{
		OperDoor &door1 = _doors[d];
		if (door1.x != xs || door1.z != zs || !door1.house) continue;
		OperDoor *door2 = FindDoor(xe, ze, door1.house);
		if (!door2) continue;

		// teleport found
		return door1.house;
	}
	return NULL;
}

#endif //_ENABLE_AI

#define DIAG 0
#define DIAG_PATH		0

#if _ENABLE_AI

int OperMap::ResultPath(AIUnit* unit)
{
#if DIAG
LogF("Path for %s, length = %d", (const char *)unit->GetDebugName(), _path.Size());
for (int i=0; i<_path.Size(); i++)
{
	OperInfo &info = _path[i];
	LogF(" %d: house %x %d .. %d", i, (Object *)info.house, info.from, info.to);
}
#endif
#if DIAG_PATH
// Original path
LogF("Raw path for %s, length = %d", (const char *)unit->GetDebugName(), _path.Size());
for (int i=0; i<_path.Size(); i++)
{
	OperInfo &info = _path[i];
	if (info.house)
		LogF(" %d: %d, %d, cost %.0f (house %x %d->%d)", i, info._x, info._z, info._cost, (Object *)info.house, info.from, info.to);
	else
		LogF(" %d: %d, %d, cost %.0f", i, info._x, info._z, info._cost);
}
#endif

	Path &path = unit->GetPath();

	path.Clear();
	int n = _path.Size();
	Assert(n >= 1);
	if (n < 1) return 0;

// avoid more steps inside one building
	for (int i=1; i<n; i++)
	{
		OperInfo &last = _path[i - 1];
		OperInfo &cur = _path[i];
		if (last.house && cur.house && last.house == cur.house)
		{
			// join
			cur.from = last.from;
			i--; n--;
			_path.Delete(i);	// delete last
		}
	}
#if DIAG
LogF("After optimization: length = %d", _path.Size());
for (int i=0; i<_path.Size(); i++)
{
	OperInfo &info = _path[i];
	LogF(" %d: house %x %d .. %d", i, (Object *)info.house, info.from, info.to);
}
#endif

	EntityAI *veh = unit->GetVehicle();
	float combatHeight = veh->GetCombatHeight();

	int lastIndex = 0;
	float lastCost = 0;
	Vector3 lastPos;
	lastPos.Init();
	lastPos[0] = _path[0]._x * OperItemGrid + 0.5 * OperItemGrid;
	lastPos[2] = _path[0]._z * OperItemGrid + 0.5 * OperItemGrid;
	lastPos[1] = GLandscape->RoadSurfaceYAboveWater(lastPos[0], lastPos[2]) + combatHeight;
	
	{
		// add start point
		int index = path.Add();
		OperInfoResult &info = path[index];
		info._pos = lastPos;
		info._cost = lastCost; 
		if (n < 2)
		{
			RptF
			(
				"Error %s: Invalid path - lenght 1",
				(const char *)unit->GetDebugName()
			);
			return 1;
		}
	}

	int maxIndex = -1;
	float pathDist = 0;

	while (lastIndex < n - 1)
	{
		int i = lastIndex + 1;

		//const OperInfo &prv = _path[i-1];
		//const OperInfo &cur = _path[i];
		//const OperInfo &nxt = _path[i+1];

		int dx = _path[i]._x-_path[i-1]._x;
		int dz = _path[i]._z-_path[i-1]._z;
		while
		(
			i < n - 1 &&
			_path[i].house == NULL &&				// way through building
			_path[i+1].house == NULL &&
			// check direction change
			_path[i+1]._x-_path[i]._x == dx && _path[i+1]._z-_path[i]._z == dz
		)
		{
			i++;
			// check direction to last point
		}
#if DIAG
LogF("    Index down to %d", i);
#endif
		lastIndex = i;
		float distance = 0;
		int index = i;
		if (_path[i].house != NULL)
		{
			HOUSE_PATH_ARRAY(hPath,64);
			bool found = _path[i].house->GetIPaths()->SearchPath
			(
				_path[i].from,
				_path[i].to,
				hPath
			);
			(void)found;
			Assert(found);
			for (int p=0; p<hPath.Size(); p++)
			{
				Vector3 actPos = hPath[p].pos;
				actPos[1] += combatHeight;
				float d = actPos.Distance(lastPos);
				if (d < 0.1) continue;
				lastCost +=
					_path[i].house->GetIPaths()->GetBType()->GetCoefInside() *
					d *
					veh->GetType()->GetMinCost();
				index = path.Add();
#if DIAG
LogF("  Added point %d (house)", index);
#endif
				OperInfoResult &info = path[index];
				info._pos = actPos;
				info._cost = lastCost;
				info._house = _path[i].house;
				info._index = hPath[p].index;
				distance += d;
				lastPos = actPos;
			}
			float diffCost = lastCost - _path[i]._cost;
			for (int j=i; j<n; j++) _path[j]._cost += diffCost;
#if DIAG
LogF(" %d: Inserted house path length %d", i, path.Size());
#endif
		}
		else
		{
			index = path.Add();
#if DIAG
LogF("  Added point %d (no house)", index);
#endif
			OperInfoResult &info = path[index];
			Vector3 actPos;
			actPos.Init();
			actPos[0] = _path[i]._x * OperItemGrid + 0.5 * OperItemGrid;
			actPos[2] = _path[i]._z * OperItemGrid + 0.5 * OperItemGrid;
			actPos[1] = GLandscape->RoadSurfaceYAboveWater(actPos[0], actPos[2]) + combatHeight;
			info._pos = actPos;
			info._cost = lastCost = _path[i]._cost;
			distance = actPos.Distance(lastPos);
			lastPos = actPos;
		}
		pathDist += distance;
		if (maxIndex < 0 && pathDist > DIST_MAX_OPER)
		{
			maxIndex = index + 1;
#if DIAG
LogF("  Max index is %d", maxIndex);
#endif
		}
	}

#if DIAG
LogF("");
#endif
//		float minCostPerM = 1/unit->GetVehicle()->GetType()->GetMaxSpeedMs();

	// optimizing pass - remove unnecessary turns
	/*
	if (path.Size()>2)
	{
		Vector3 lastPos = path[0]._pos;
		float lastCost = path[0]._cost;
		for (int i=1; i<path.Size(); i++)
		{
			Vector3 thisPos = path[i]._pos;
			float thisCost = path[i]._cost;
		}
	}
	*/

	// final pass - limit cost to reasonable value
	if (path.Size()>1)
	{

		float minCostPerM = veh->GetType()->GetMinCost();
		float maxCostPerM = veh->GetType()->GetMinCost() * 20;
		saturateMin(maxCostPerM, 3.6);
		// note: saturateMax was above - but that would select the hight cost of the two
		// we need to move it at higher of speeds maxspeed/20 or  1/3.6 m/s

		Vector3 lastPos = path[0]._pos;
		float lastSrcCost = path[0]._cost;
		float lastDstCost = 0;
		for (int i=1; i<path.Size(); i++)
		{
			Vector3Val pos = path[i]._pos;
			float srcCost = path[i]._cost;
			float dist = pos.Distance(lastPos);

			float diffCost = srcCost-lastSrcCost;

			float minCost = minCostPerM*dist;
			float maxCost = maxCostPerM*dist;

			if (diffCost<minCost) diffCost = minCost;
			else if (diffCost>maxCost)
			{
				if (diffCost>1e5)
				{
					LogF("%s: Cost too high",(const char *)unit->GetDebugName());
				}
				diffCost = maxCost;
			}
			
			lastDstCost = lastDstCost + diffCost;
			path[i]._cost = lastDstCost;
			// 
			lastPos = pos;
			lastSrcCost = srcCost;
			//LogF("  update from %.2f to %.2f",lastSrcCost,lastDstCost);
		}
	}
	if (maxIndex < 0) maxIndex = path.Size();

#if DIAG_PATH
// Result path
LogF("Result path for %s, length = %d", (const char *)unit->GetDebugName(), maxIndex);
for (int i=0; i<path.Size(); i++)
{
	LogF
	(
		" %d: %.0f, %.0f, cost %.0f",
		i,
		path[i]._pos.X() / OperItemGrid,
		path[i]._pos.Z() / OperItemGrid,
		path[i]._cost
	);
}
#endif

	return maxIndex;
}


///////////////////////////////////////////////////////////////////////////////
// class OperMap


void OperField::Rasterize
(
	Object *obj, const Vector3 *minMax,
	RastMode mode, OperItemType type, float xResize, float zResize, bool soldier
)
{
	//OperItem *item;
	Point3 rect[4];
	rect[0] = minMax[0];
	rect[0][0] -= xResize;
	rect[0][2] -= zResize;
	rect[2] = minMax[1];
	rect[2][0] += xResize;
	rect[2][2] += zResize;
	rect[1].Init();
	rect[1][0] = rect[0][0];
	rect[1][1] = rect[0][1];
	rect[1][2] = rect[2][2];
	rect[3].Init();
	rect[3][0] = rect[2][0];
	rect[3][1] = rect[0][1];
	rect[3][2] = rect[0][2];
	for (int j=0; j<4; j++)
	{
		rect[j] = obj->PositionModelToWorld(rect[j]);
		rect[j][0]*=InvOperItemGrid;
		rect[j][2]*=InvOperItemGrid;
	}
	float xMin = rect[0][0];
	float xMax = rect[0][0];
	int jzMin = 0;
	float zMin = rect[0][2];
	float zMax = rect[0][2];
	for ( int j=1; j<4; j++)
	{
		if (rect[j][0] < xMin)
			xMin = rect[j][0];
		else if (rect[j][0] > xMax)
			xMax = rect[j][0];
		if (rect[j][2] < zMin)
		{
			zMin = rect[j][2];
			jzMin = j;
		}
		else if (rect[j][2] > zMax)
			zMax = rect[j][2];
	}
	if (xMin >= (_x + 1) * OperItemRange)
		return;
	if (xMax < _x * OperItemRange)
		return;
	if (zMin >= (_z + 1) * OperItemRange)
		return;
	if (zMax < _z * OperItemRange)
		return;

	int baseX=_x * OperItemRange;
	int baseZ=_z * OperItemRange;

	int xxMinLast=INT_MIN,xxMaxLast=INT_MAX;

	MapCoord zzMin = toIntFloor(zMin);
	MapCoord zzMax = toIntFloor(zMax);
	MapCoord zzL, zzR, xx, xxMin, xxMax;
	float xL, xR, dxL, dxR;
	xL = xR = rect[jzMin][0];
	int jLCur, jRCur, jLNext, jRNext;
	jLCur = jRCur = jzMin;
	jLNext = (jLCur - 1) & 3;
	jRNext = (jRCur + 1) & 3;
	bool bEnd = false;

	float dz = zzMin + 1 - zMin;

	float denomL = (rect[jLNext][2] - rect[jLCur][2]);
	float denomR = (rect[jRNext][2] - rect[jRCur][2]);
	float invDL = denomL!=0 ? 1/denomL : 1e10;
	float invDR = denomR!=0 ? 1/denomR : 1e10;

	dxL = (rect[jLNext][0] - rect[jLCur][0]) * invDL * dz;
	dxR = (rect[jRNext][0] - rect[jRCur][0]) * invDR * dz;

	for (int j=zzMin; j<=zzMax; j++)
	{
		zzL = toIntFloor(rect[jLNext][2]);
		if (zzL == j)
			xL = rect[jLNext][0];
		else
			xL += dxL;
		zzR = toIntFloor(rect[jRNext][2]);
		if (zzR == j)
			xR = rect[jRNext][0];
		else
			xR += dxR;
		if (xL < xR)
		{
			xxMin = toIntFloor(xL);
			xxMax = toIntFloor(xR);
		}
		else
		{
			xxMax = toIntFloor(xL);
			xxMin = toIntFloor(xR);
		}
		
		while (!bEnd && zzL == j)
		{
			xx = toIntFloor(rect[jLNext][0]);
			if (xx < xxMin)
				xxMin = xx;
			else if (xx > xxMax)
				xxMax = xx;
			jLCur--;
			jLCur &= 3;
			xL = rect[jLCur][0];
			jLNext--;
			jLNext &= 3;
			bEnd = jLNext == jRCur;
			zzL = toIntFloor(rect[jLNext][2]);
		}
		if (!bEnd)
		{
			//dxL = (rect[jLNext][0] - rect[jLCur][0])
			//	/ (rect[jLNext][2] - rect[jLCur][2]);

			float denomL = (rect[jLNext][2] - rect[jLCur][2]);
			float invDL = denomL!=0 ? 1/denomL : 1e10;
			dxL = (rect[jLNext][0] - rect[jLCur][0]) * invDL;
		}

		while (!bEnd && zzR == j)
		{
			xx = toIntFloor(rect[jRNext][0]);
			if (xx < xxMin)
				xxMin = xx;
			else if (xx > xxMax)
				xxMax = xx;
			jRCur++;
			jRCur &= 3;
			xR = rect[jRCur][0];
			jRNext++;
			jRNext &= 3;
			bEnd = jRNext == jLCur;
			zzR = toIntFloor(rect[jRNext][2]);
		}
		if (!bEnd)
		{
			//dxR = (rect[jRNext][0] - rect[jRCur][0])
			//	/ (rect[jRNext][2] - rect[jRCur][2]);

			float denomR = (rect[jRNext][2] - rect[jRCur][2]);
			float invDR = denomR!=0 ? 1/denomR : 1e10;
			dxR = (rect[jRNext][0] - rect[jRCur][0]) * invDR;
		}
		
		int z0 = j - baseZ;
		if (z0 < 0 || z0 >= OperItemRange)
			continue;

		/**/
		// force rasterized section to be continuous
		if( xxMin>xxMaxLast ) xxMin=xxMaxLast;
		if( xxMax<xxMinLast ) xxMax=xxMinLast;
		/**/

		xxMinLast=xxMin,xxMaxLast=xxMax;

		int x0 = xxMin - baseX;
		int x1 = xxMax - baseX;
		saturateMax( x0, 0 );
		saturateMin( x1, OperItemRange-1 );

		OperItem *itemsZ0=_items[z0];

//Log("Row %d (%d), columns %d - %d (%d - %d)", j, z0, xxMin, xxMax, x0, x1);
		Assert(mode==RMMax);

		if (soldier)
			for (int k=x0; k<=x1; k++)
			{
				if (type > itemsZ0[k]._typeSoldier) itemsZ0[k]._typeSoldier = type;
			}
		else
			for (int k=x0; k<=x1; k++)
			{
				if (type > itemsZ0[k]._type) itemsZ0[k]._type = type;
			}
	}
}

// reserve at balk
#define OBJ_SPACE						1.0
#define OBJ_AVOID						8.0
//#define ROAD_SPACE					3.0
#define ROAD_SPACE					1.0
#define OBJ_HEIGHT	        2.5

#define OBJ_SPACE_SOLDIER		0.2
#define OBJ_AVOID_SOLDIER		2.0
//#define ROAD_SPACE_SOLDIER	1.0
#define ROAD_SPACE_SOLDIER	0.0
#define OBJ_HEIGHT_SOLDIER	0.5	// !!! by Viktor Bocan, 2.3.2001

/*!
\patch 1.53 Date 4/26/2002 by Ondra
- New: Bridge supported as part of road network.
*/

void OperField::CreateField(int mask)
{
	//ADD_COUNTER(operF,1);
	OperItem *item;
	int ix, iz, i;
	const float maxWater = -0.5;
	for (iz=0; iz<OperItemRange; iz++) for (ix=0; ix<OperItemRange; ix++)
	{
		item = &_items[iz][ix];
		item->_searchID = 0;
		item->_type = OITNormal;
		item->_typeSoldier = OITNormal;
		// deep water should be marked as OITSpace for soldiers
		// get coordinates in terrain
		if (mask&MASK_USE_BUFFER)
		{
			// sample center of the grid squares
			float xt = _x * LandGrid + ix*OperItemGrid + OperItemGrid*0.5f;
			float zt = _z * LandGrid + iz*OperItemGrid + OperItemGrid*0.5f;
			// note: all samples are in the same grid
			// SurfaceY may be optimized for contant grid
			float yt = GLandscape->SurfaceY(xt,zt);
			if (yt<maxWater)
			{
				item->_typeSoldier = OITWater;
				item->_type = OITWater;
			}

		}

	}

	Object *obj;
	// avoid
	for (ix=_x-1; ix<=_x+1;ix++)
		for (iz=_z-1; iz<=_z+1;iz++) 
			if (InRange(ix, iz))
			{
				const ObjectList &list = GLOB_LAND->GetObjects(iz, ix);
				for (i=0;i<list.Size();i++)
				{
					obj = list[i];
					if (obj == NULL) continue;
					LODShape *shape=obj->GetShape();
					if (!shape) continue;
					if (obj->GetMass() < 5.0) continue;
					if (!obj->HasGeometry()) continue;
					if (obj->Static())
					{
						if ((mask & MASK_AVOID_OBJECTS)== 0) continue;
					}
					else if (obj->GetType() == TypeVehicle )
					{
						if( (mask & MASK_AVOID_VEHICLES) == 0) continue;
					}
					//if (obj->GetType() == Network) continue;

					OperItemType type = OITAvoid;
					if (obj->IsPassable() ) type = OITAvoidBush;
					else if (obj->GetDestructType() == DestructTree) type = OITAvoidTree;
					// render all convex components
					const ConvexComponents &ccs=shape->GetGeomComponents();
					for( int c=0; c<ccs.Size(); c++ )
					{
						const ConvexComponent *cc=ccs.Get(c);
						Vector3Val min=obj->PositionModelToWorld(cc->Min());
						float minSurfY=GLOB_LAND->RoadSurfaceY(min.X(),min.Z());
						float height=min.Y()-minSurfY;
						// rasterize for both soldiers and vehicles
						if (height <= OBJ_HEIGHT_SOLDIER)
						{
							Rasterize(obj, cc->MinMax(), RMMax, type, OBJ_AVOID_SOLDIER, OBJ_AVOID_SOLDIER, true);
						}
						if (height <= OBJ_HEIGHT)
						{
							Rasterize(obj, cc->MinMax(), RMMax, type, OBJ_AVOID, OBJ_AVOID, false);
						}
					}
					// insert doors
					const IPaths *house = obj->GetIPaths();
					if (house)
					{
						int n = house->NExits();
						for (int i=0; i<n; i++)
						{
							int exit = house->GetExit(i);
							Vector3Val pos = house->GetPosition(exit);
							int x = toIntFloor(pos.X() * InvOperItemGrid);
							int z = toIntFloor(pos.Z() * InvOperItemGrid);
							int xx = x / OperItemRange;
							int zz = z / OperItemRange;
							if (xx == _x && zz == _z)
							{
								int index = _doors.Add();
								OperDoor &door = _doors[index];
								door.house = const_cast<Object *>(house->GetObject());
								door.exit = exit;
								door.x = x;
								door.z = z;
							}
						}
					}
				}
			}
	// space
	for (ix=_x-1; ix<=_x+1;ix++)
		for (iz=_z-1; iz<=_z+1;iz++) 
			if (InRange(ix, iz))
			{
				const ObjectList &list = GLOB_LAND->GetObjects(iz, ix);
				for (i=0;i<list.Size();i++)
				{
					obj = list[i];
					if (obj == NULL) continue;
					LODShape *shape=obj->GetShape();
					if (!shape) continue;
					if (obj->Static())
					{
						if ((mask & MASK_AVOID_OBJECTS)== 0) continue;
					}
					else if (obj->GetType() == TypeVehicle )
					{
						if( (mask & MASK_AVOID_VEHICLES) == 0) continue;
					}
					OperItemType type = OITSpace;
					OperItemType typeHigh = OITSpace;
					float bottomOfHigh = 1e10; // where typeHigh should start
					float forceXBorder = 0;
					if (obj->GetType() == Network)
					{
						// note: roads do not have any components

						if (!obj->HasGeometry() || shape->FindGeometryLevel()<0)
						{
							Rasterize(obj, shape->MinMax(), RMMax, OITRoad, OBJ_SPACE_SOLDIER, OBJ_SPACE_SOLDIER, true);
							Rasterize(obj, shape->MinMax(), RMMax, OITRoad, OBJ_SPACE, OBJ_SPACE, false);
							continue;
						}
						// if there is geometry, there may be some components as well
						// if it has components, it is probably a bridge
						float width = shape->Max().X()-shape->Min().X();
						// leave only minimal area around roadway center
						float xBorder = (width - 1.0)*0.5f;
						float zBorder = OBJ_SPACE+1.5;
						saturateMin(xBorder,6);

						Rasterize(obj, shape->MinMax(), RMMax, OITRoadForced, -xBorder, zBorder, true);
						Rasterize(obj, shape->MinMax(), RMMax, OITRoadForced, -xBorder, zBorder, false);
						forceXBorder = 5.0;

						type = OITSpaceRoad;
						Shape *road = shape->RoadwayLevel();
						if (road) bottomOfHigh = road->Max().Y();
					}
					else
					{
						if (!obj->HasGeometry()) continue;
						if (obj->GetMass() < 5.0) continue;

						if (obj->IsPassable() ) type = OITSpaceBush;
						else if (obj->GetDestructType() == DestructTree) type = OITSpaceTree;
					}

					bool hasPaths = obj->GetIPaths()!=NULL;
					//bool hasPaths = false;

					// render all convex components
					const ConvexComponents &ccs=shape->GetGeomComponents();
					for( int c=0; c<ccs.Size(); c++ )
					{
						const ConvexComponent *cc=ccs.Get(c);
						Vector3Val min=obj->PositionModelToWorld(cc->Min());
						Vector3Val ave=obj->PositionModelToWorld(0.5 * (cc->Min() + cc->Max()));
						float aveSurfY=GLOB_LAND->RoadSurfaceY(ave.X(),ave.Z());
						float height=min.Y()-aveSurfY;
						// rasterize for both soldiers and vehicles
						//Vector3 minMax[2]= cc->MinMax();
						OperItemType cType = type;
						float fXBorder = forceXBorder;
						float maxBorder = 100;
						/**/
						if (cc->Max().Y()>bottomOfHigh+OBJ_HEIGHT)
						{
							cType = typeHigh;
							fXBorder = 0;
							//LogF("%s: high component %d",shape->Name(),c);
							maxBorder = 0.2;
						}
						/**/
						if (hasPaths || height <= OBJ_HEIGHT_SOLDIER)
						{
							Rasterize
							(
								obj, cc->MinMax(), RMMax, cType,
								floatMin(maxBorder,floatMax(forceXBorder,OBJ_SPACE_SOLDIER)),
								floatMin(maxBorder,OBJ_SPACE_SOLDIER), true
							);
						}
						if (type == OITSpaceRoad || height <= OBJ_HEIGHT)
						{
							Rasterize
							(
								obj, cc->MinMax(), RMMax, cType,
								floatMin(maxBorder,floatMax(forceXBorder,OBJ_SPACE)),
								floatMin(maxBorder, OBJ_SPACE), false
							);
						}
					}
				}
			}
}

// OperCache

// keep cache size in this range

#endif // _ENABLE_AI
