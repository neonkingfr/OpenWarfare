#ifdef _MSC_VER
#pragma once
#endif

#ifndef _ROXXE_HPP
#define _ROXXE_HPP

#define ProtocolName "Roxx_Log.txt"

void PerformRandomRoxxeTest_000(char CDDrive);
void PerformRandomRoxxeTest_001(char CDDrive);
void PerformRandomRoxxeTest_002(char CDDrive);
void PerformRandomRoxxeTest_003(char CDDrive);

// extern char CDDrive;

#endif