// Operation FlashPoint - vehicle class
// (C) 2001, Bohemia Interactive Studio

#include "wpch.hpp"
#include "head.hpp"
#include "global.hpp"
#include <El/Common/randomGen.hpp>
#include "paramFileExt.hpp"
#include <El/QStream/QBStream.hpp>
#include "soundsys.hpp"
#include "vehicle.hpp"

// class ManLipInfo

bool ManLipInfo::AttachWave(AbstractWave *wave, float freq)
{
	if (!wave) return false;
	char buffer[256];
	strcpy(buffer, wave->Name());
	char *ext = strrchr(buffer, '.');
	if (!ext) return false;
	ext++;
	strcpy(ext, "lip");
	if (!QIFStreamB::FileExist(buffer)) return false;

	// parse file
	QIFStreamB f;
	f.AutoOpen(buffer);
	while ( !f.eof() )
	{
        // read line
        f.readLine(buffer,sizeof(buffer));
		// process line
		float time;
		int phase;
		if (sscanf(buffer, "%f, %d", &time, &phase) == 2)
		{
			int index = _items.Add();
			_items[index].time = time;
			_items[index].phase = phase;
		}
		else
		{
			float frame;
			if (sscanf(buffer, "frame = %f", &frame) == 1)
			{
				_frame = frame;
				_invFrame = 1.0 / frame;
			}
		}
	}

	_current = 0;
	_freq = freq;
	_start = Glob.time;
	return true;
}

bool ManLipInfo::GetPhase(int &phase)
{
	float offset = _freq * (Glob.time - _start);
	while
	(
		_current < _items.Size() &&
		_items[_current].time < offset
	) _current++;
	if (_current > _items.Size()) return false;
	if (_current == 0)
		phase = -1;
	else
		phase = _items[_current - 1].phase;
	return true;
}

float ManLipInfo::GetPhase()
{
	float offset = _freq * (Glob.time - _start);

	float floor = fastFloor(offset * _invFrame) * _frame;

	while
	(
		_current < _items.Size() &&
		_items[_current].time < floor
	) _current++;
	if (_current > _items.Size()) return -1;

	float oldPhase = 0;
	if (_current > 0) oldPhase = _items[_current - 1].phase;

	int c = _current;
	while
	(
		c < _items.Size() &&
		_items[c].time < floor + _frame
	) c++;

	float newPhase = 0;
	if (c > 0 && c < _items.Size()) newPhase = _items[c - 1].phase;
	
	float dif = offset - floor;
	return (1.0 / 7) * _invFrame * (dif * newPhase + (_frame - dif) * oldPhase);
}

// class HeadType

HeadType::HeadType()
{
}

void HeadType::Load(const ParamEntry &cfg)
{
	RStringB microName = cfg >> "microMimics";
	const ParamEntry &micro = Pars >> "CfgMimics" >> microName;
	_lBrowRandom.Load(micro>>"lBrow");
	_mBrowRandom.Load(micro>>"mBrow");
	_rBrowRandom.Load(micro>>"rBrow");
	_lMouthRandom.Load(micro>>"lMouth");
	_mMouthRandom.Load(micro>>"mMouth");
	_rMouthRandom.Load(micro>>"rMouth");
}

void HeadType::InitShape(const ParamEntry &cfg, LODShape *shape)
{
	_personality.Init(shape,"osobnost",NULL);

	// get some texture from personality
	// note: there should be only one personality texture
	_textureOrig = _personality.GetTexture(shape);
	
	
	_glasses.Init(shape,"brejle",NULL);

	_lBrow.Init(shape,"loboci",NULL);
	_mBrow.Init(shape,"soboci",NULL);
	_rBrow.Init(shape,"poboci",NULL);
	_lMouth.Init(shape,"lkoutek",NULL);
	_mMouth.Init(shape,"skoutek",NULL);
	_rMouth.Init(shape,"pkoutek",NULL);

	_eyelid.Init(shape,"vicka",NULL);
	_lip.Init(shape,"spodni ret",NULL);
}

// class Head

Head::Head(const HeadType &type, LODShape *lShape)
{
	_winkPhase = 2;
	_forceWinkPhase = -1;
	_nextWink = Glob.time;	// recalculate now

	_mimicMode = 0;

	_mimicPhase = 0;
	_nextMimicTime = 1e10;

	_lBrow = VZero;
	_mBrow = VZero;
	_rBrow = VZero;
	_lMouth = VZero;
	_mMouth = VZero;
	_rMouth = VZero;
	_lBrowOld = VZero;
	_mBrowOld = VZero;
	_rBrowOld = VZero;
	_lMouthOld = VZero;
	_mMouthOld = VZero;
	_rMouthOld = VZero;

	_rBrowRandom.SetWanted(VZero,0);
	_mBrowRandom.SetWanted(VZero,0);
	_lBrowRandom.SetWanted(VZero,0);

	_rMouthRandom.SetWanted(VZero,0);
	_mMouthRandom.SetWanted(VZero,0);
	_lMouthRandom.SetWanted(VZero,0);

	SetFace(type, true, lShape, "Default");
	SetGlasses(type, lShape, "None");
	SetMimic("Default");

	_randomLip = false;
}

void Head::AttachWave(AbstractWave *wave, float freq)
{
	_lipInfo = new ManLipInfo();
	if (!_lipInfo->AttachWave(wave, freq)) _lipInfo = NULL;
}

void Head::NextRandomLip()
{
	_wantedRandomLip = GRandGen.RandomValue();
	float time = 0.05 + 0.1 * GRandGen.RandomValue();
	_nextChangeRandomLip = Glob.time + time;
	_speedRandomLip = 1.0 / time;
}

void Head::SetRandomLip(bool set)
{
	if (set && !_randomLip)
	{
		_actualRandomLip = 0;
		NextRandomLip();
	}
	_randomLip = set;
}

static void OffsetAnimation
(
	const Animation &anim, Shape *shape, int level, Vector3Par offset
)
{
	int selection = anim.GetSelection(level);
	if (selection < 0) return;
	const NamedSelection &sel = shape->NamedSel(selection);
	for (int i=0; i<sel.Size(); i++)
	{
		int posI = sel[i];
		shape->SetPos(posI) += offset;
	}
	shape->InvalidateNormals();
}

#define OFFSET_ANIM(x) \
	OffsetAnimation \
	( \
		type.x, shape, level, \
		trans * (coefOld * x##Old + coefNew * x + coefRand * (Vector3) x##Random) \
	);

void Head::Animate
(
	const HeadType &type, LODShape *lShape, int level, bool isDead, Matrix3Par trans, bool hiddenHead
)
{
	// TODO: BUG
	const_cast<Matrix3 &>(trans).Orthogonalize();

	Shape *shape = lShape->Level(level);

	// face
	if (_texture)
	{
		type._personality.SetTexture(lShape, level, _texture);
	}

	// glasses
	if (_glasses && !hiddenHead)
	{
		type._glasses.Unhide(lShape, level);
		type._glasses.SetTexture(lShape, level, _glasses);
	}
	else
		type._glasses.Hide(lShape, level);

	Vector3 up = trans * VUp;

	if (!isDead)
	{
		// wink 
		float winkPhase = 0;
		if (_forceWinkPhase >= 0)
		{
			winkPhase = 2 * _forceWinkPhase;
		}
		else
		{
			winkPhase = 2.0 * _winkPhase;
		}
		if (winkPhase > 1) winkPhase = 2.0 - winkPhase;
		OffsetAnimation(type._eyelid, shape, level, winkPhase * -0.015 * up);

		// lip
		if (_lipInfo)
		{
			float lipPhase = _lipInfo->GetPhase();
			if (lipPhase > -0.1)
			{
				OffsetAnimation(type._lip, shape, level, lipPhase * -0.02 * up);
				OffsetAnimation(type._lMouth, shape, level, lipPhase * -0.01 * up);
				OffsetAnimation(type._rMouth, shape, level, lipPhase * -0.01 * up);
			}
			else
			{
				_lipInfo = NULL;
			}
		}
		else if (_randomLip)
		{
			OffsetAnimation(type._lip, shape, level, _actualRandomLip * -0.02 * up);
			OffsetAnimation(type._lMouth, shape, level, _actualRandomLip * -0.01 * up);
			OffsetAnimation(type._rMouth, shape, level, _actualRandomLip * -0.01 * up);
		}
	}

	// mimic
	float coefNew = _mimicPhase;
	float coefOld = 1.0f - _mimicPhase;
	const float coefRand = +1.0f;

	OFFSET_ANIM(_rBrow);
	OFFSET_ANIM(_mBrow);
	OFFSET_ANIM(_lBrow);
	OFFSET_ANIM(_rMouth);
	OFFSET_ANIM(_mMouth);
	OFFSET_ANIM(_lMouth);
}

void Head::Deanimate(const HeadType &type, LODShape *lShape, int level, bool isDead, Matrix3Par trans, bool hiddenHead)
{
	// TODO: BUG
	const_cast<Matrix3 &>(trans).Orthogonalize();

	Shape *shape = lShape->Level(level);

	Vector3 up = trans * VUp;

	// mimic
	float coefNew = -_mimicPhase;
	float coefOld = -(1.0f - _mimicPhase);
	const float coefRand = -1;
	OFFSET_ANIM(_rBrow);
	OFFSET_ANIM(_mBrow);
	OFFSET_ANIM(_lBrow);
	OFFSET_ANIM(_rMouth);
	OFFSET_ANIM(_mMouth);
	OFFSET_ANIM(_lMouth);

	// wink
	if (!isDead)
	{
		float winkPhase = 0;
		if (_forceWinkPhase >= 0)
		{
			winkPhase = 2 * _forceWinkPhase;
		}
		else
		{
			winkPhase = 2.0 * _winkPhase;
		}
		if (winkPhase > 1) winkPhase = 2.0 - winkPhase;
		OffsetAnimation(type._eyelid, shape, level, winkPhase * 0.015 * up);
	}

	// lip
	if (!isDead && _lipInfo)
	{
		float lipPhase = _lipInfo->GetPhase();
		if (lipPhase > -0.1)
		{
			OffsetAnimation(type._lip, shape, level, lipPhase * 0.02 * up);
			OffsetAnimation(type._lMouth, shape, level, lipPhase * 0.01 * up);
			OffsetAnimation(type._rMouth, shape, level, lipPhase * 0.01 * up);
		}
	}
}

#undef OFFSET_ANIM

void RandomVector3Type::Load(const ParamEntry &cfg)
{
	rng = Vector3(cfg[0],cfg[1],cfg[2]);
	minT = cfg[3];
	maxT = cfg[4];
}


RandomVector3::RandomVector3()
{
	_spd = VZero;
	_cur = VZero;
	_timeToWanted = 0;
}

void RandomVector3::SetWanted(Vector3Par wanted, float time)
{
	/*
	LogF
	(
		"SetW %.4f,%.4f,%.4f,  %.4f",
		wanted[0],wanted[1],wanted[2],time
	);
	*/
	if (time<=0)
	{
		_spd = VZero;
		_cur = wanted;
		_timeToWanted = 0;
	}
	else
	{
		_spd = (wanted-_cur)/time;
		_timeToWanted = time;
	}
}

bool RandomVector3::Simulate(float deltaT)
{
	if (_timeToWanted<=0) return true;
	float dt = deltaT;
	saturateMin(dt,_timeToWanted);
	_timeToWanted -= deltaT;
	_cur += _spd*dt;
	/*
	LogF
	(
		"cur %.4f,%.4f,%.4f,  spd %.4f,%.4f,%.4f,  %.4f",
		_cur[0],_cur[1],_cur[2],
		_spd[0],_spd[1],_spd[2],dt
	);
	*/

	return false;
}

void RandomVector3::SetRandomTgt(Vector3Par rng, float minT, float maxT)
{
	float x = GRandGen.RandomValue()*rng.X()*2-rng.X();
	float y = GRandGen.RandomValue()*rng.Y()*2-rng.Y();
	float z = GRandGen.RandomValue()*rng.Z()*2-rng.Z();
	float t = GRandGen.RandomValue()*(maxT-minT)+minT;
	SetWanted(Vector3(x,y,z),t);
}

bool RandomVector3::SimulateAndSetRandomTgt
(
	float deltaT,
	Vector3Par rng, float minT, float maxT
)
{
	if (Simulate(deltaT))
	{
		SetRandomTgt(rng,minT,maxT);
		return true;
	}
	return false;
}

bool RandomVector3::SimulateAndSetRandomTgt
(
	float deltaT, const RandomVector3Type &type
)
{
	if (Simulate(deltaT))
	{
		SetRandomTgt(type.rng,type.minT,type.maxT);
		return true;
	}
	return false;
}



const float MimicPeriod = 5;

void Head::Simulate(const HeadType &type, float deltaT, SimulationImportance prec, bool dead)
{
	const float winkSpeed = 8.0;
	if (Glob.time >= _nextWink)
	{
		_winkPhase += winkSpeed * deltaT;
		if (_winkPhase >= 1)
		{
			_winkPhase = 0;
			_nextWink = Glob.time + 1.0f + 5.0f * GRandGen.RandomValue();
		}
	}

	const float mimicSpeed = 2.0;
	_mimicPhase += mimicSpeed * deltaT;
	saturateMin(_mimicPhase, 1.0f);

	if (_mimicPhase>=1)
	{
		// change mimic when in some mimic mode
		if (_forceMimic.GetLength()>0)
		{
		}
		else if (_mimicMode)
		{
			_nextMimicTime -= deltaT;
			if (_nextMimicTime<0)
			{
				// select random mimic
				float rand = GRandGen.RandomValue();
				RStringB name = (*_mimicMode)[_mimicMode->GetSize()-1];
				for (int i=0; i<_mimicMode->GetSize()-1; i+=2)
				{
					float prop = (*_mimicMode)[i+1];
					rand -= prop;
					if (rand<=0)
					{
						name = (*_mimicMode)[i];
						break;
					}
				}
				//LogF("Set mimic %s",(const char *)name);
				SetMimic(name);
				_nextMimicTime = MimicPeriod;
			}
		}

	}

	float diff = _wantedRandomLip - _actualRandomLip;
	saturate(diff, -_speedRandomLip * deltaT, _speedRandomLip * deltaT);
	_actualRandomLip += diff;
	if (Glob.time >= _nextChangeRandomLip) NextRandomLip();

	if (prec<=SimulateVisibleNear)
	{
		if (!dead)
		{
			_lBrowRandom.SimulateAndSetRandomTgt(deltaT,type._lBrowRandom);
			_mBrowRandom.SimulateAndSetRandomTgt(deltaT,type._mBrowRandom);
			_rBrowRandom.SimulateAndSetRandomTgt(deltaT,type._rBrowRandom);
			_lMouthRandom.SimulateAndSetRandomTgt(deltaT,type._lMouthRandom);
			_mMouthRandom.SimulateAndSetRandomTgt(deltaT,type._mMouthRandom);
			_rMouthRandom.SimulateAndSetRandomTgt(deltaT,type._rMouthRandom);
		}
		else
		{
			_lBrowRandom.SetWanted(VZero,0);
			_mBrowRandom.SetWanted(VZero,0);
			_rBrowRandom.SetWanted(VZero,0);
			_lMouthRandom.SetWanted(VZero,0);
			_mMouthRandom.SetWanted(VZero,0);
			_rMouthRandom.SetWanted(VZero,0);
		}
	}

	//LogF("_lBrowRandom %.4f,%.4f,%.4f,",_lBrowRandom.X(),_lBrowRandom.Y(),_lBrowRandom.Z());
	//LogF("_mBrowRandom %.4f,%.4f,%.4f,",_mBrowRandom.X(),_mBrowRandom.Y(),_mBrowRandom.Z());
	//LogF("_rBrowRandom %.4f,%.4f,%.4f,",_rBrowRandom.X(),_rBrowRandom.Y(),_rBrowRandom.Z());
}

/*!
\patch 1.43 Date 1/30/2002 by Ondra
- Fixed: Random crash when custom face texture file was corrupted.
*/

void Head::SetFace(const HeadType &type, bool women, LODShape *lShape, RString name, RString player)
{
	const ParamEntry *cls = (Pars >> "CfgFaces").FindEntry(name);
	if (!cls)
	{
		cls = (Pars >> "CfgFaces").FindEntry("Default");
//		WarningMessage("Unknown face: %s", (const char *)name);
		if (!cls) return;
	}
	
	bool faceWoman = false;
	if (cls->FindEntry("woman")) faceWoman = *cls >> "woman";
	// do not use women face to man head and vice versa
	if (faceWoman != women) return;

	RString textName = GetPictureName(*cls >> "texture");
	Ref<Texture> text = GlobLoadTexture(textName);
	if (text)
		_texture = text;
	else
		ErrF("Cannot find texture: %s", (const char *)textName);

	if (stricmp(name, "custom") == 0)
	{
		RString GetUserDirectory();
		RString dir;
		if (player.GetLength() > 0)
			dir = RString("tmp\\players\\") + player + RString("\\");
		else
			dir = GetUserDirectory();
		RString name = dir + RString("face.paa");
		if (QIFStream::FileExists(name))
		{
			Ref<Texture> text = GlobLoadTexture(name);
			if (text)
				_texture = text;
		}
		else
		{
			name = dir + RString("face.jpg");
			if (QIFStream::FileExists(name))
			{
				Ref<Texture> text = GlobLoadTexture(name);
				if (text)
					_texture = text;
			}
		}
	}

	// check if there is some wounded texture associated
	if (_texture)
	{
		const ParamEntry &cfg = Pars>>"CfgFaceWounds">>"wounds";
		for (int i=0; i<cfg.GetSize()-1; i+=2)
		{
			RStringB origName = cfg[i];
			RStringB woundName = cfg[i+1];
			// check if origName is name of currently set texture

			RStringB origTexName = GetDefaultName(origName,"data\\",".pac");
			if (strcmp(origTexName,_texture->GetName())) continue;
			RStringB woundTexName = GetDefaultName(woundName,"data\\",".pac");
			_textureWounded = GlobLoadTexture(woundTexName);
		}

		// register textures in shape
		// _textureFace is used on selection
		lShape->RegisterTexture(_texture,type._personality);
	}
	if (_textureWounded)
	{
		lShape->RegisterTexture(_textureWounded,type._personality);
	}
}

void Head::SetGlasses(const HeadType &type, LODShape *lShape, RString name)
{
	const ParamEntry *cls = (Pars >> "CfgGlasses").FindEntry(name);
	if (!cls)
	{
		_glasses = NULL;
		return;
	}

	RString textName = GetPictureName(*cls >> "texture");
	if (textName.GetLength() == 0)
	{
		_glasses = NULL;
		return;
	}

	Ref<Texture> text = GlobLoadTexture(textName);
	if (text)
		_glasses = text;
	else
		WarningMessage("Cannot find texture: %s", (const char *)textName);

	// register textures in shape
	if (_glasses) lShape->RegisterTexture(_glasses, type._glasses);
}

void Head::SetMimicMode(RStringB modeName)
{
	if (modeName.GetLength()>0)
	{
		const ParamEntry &par = Pars >> "CfgMimics" >> modeName;
		if (_mimicMode!=&par)
		{
			_mimicMode = &par;
			_nextMimicTime = 0;
		}
	}
	else
	{
		_mimicMode = NULL;
	}
}

RStringB Head::GetMimicMode() const
{
	if (!_mimicMode) return RStringBEmpty;
	return _mimicMode->GetName();
}

void Head::SetForceMimic(RStringB name)
{
	_forceMimic = name;
	SetMimic(name);
}

/*!
\patch_internal 1.03 Date 07/13/2001 by Jirka
	- warning message removed
	- avoid WarningMessage in released campaign
*/
void Head::SetMimic(RStringB name)
{
	const ParamEntry *cls = (Pars >> "CfgMimics" >> "States").FindEntry(name);
	if (!cls)
	{
		// WarningMessage("Unknown mimic: %s", (const char *)name);
		ErrF("Unknown mimic: %s", (const char *)name);
		return;
	}

	// set current mimic as old
	float coefNew = _mimicPhase;
	float coefOld = 1.0f - _mimicPhase;
	_lBrowOld = coefOld * _lBrowOld + coefNew * _lBrow;
	_mBrowOld = coefOld * _mBrowOld + coefNew * _mBrow;
	_rBrowOld = coefOld * _rBrowOld + coefNew * _rBrow;
	_lMouthOld = coefOld * _lMouthOld + coefNew * _lMouth;
	_mMouthOld = coefOld * _mMouthOld + coefNew * _mMouth;
	_rMouthOld = coefOld * _rMouthOld + coefNew * _rMouth;
	_mimicPhase = 0;

	// load new mimic
	const float coef = 0.01;
	_lBrow[0] = (*cls >> "lBrow")[0]; _lBrow[0] *= coef;
	_lBrow[1] = (*cls >> "lBrow")[1]; _lBrow[1] *= coef;
	_mBrow[0] = (*cls >> "mBrow")[0]; _mBrow[0] *= coef;
	_mBrow[1] = (*cls >> "mBrow")[1]; _mBrow[1] *= coef;
	_rBrow[0] = (*cls >> "rBrow")[0]; _rBrow[0] *= coef;
	_rBrow[1] = (*cls >> "rBrow")[1]; _rBrow[1] *= coef;
	_lMouth[0] = (*cls >> "lMouth")[0]; _lMouth[0] *= coef;
	_lMouth[1] = (*cls >> "lMouth")[1]; _lMouth[1] *= coef;
	_mMouth[0] = (*cls >> "mMouth")[0]; _mMouth[0] *= coef;
	_mMouth[1] = (*cls >> "mMouth")[1]; _mMouth[1] *= coef;
	_rMouth[0] = (*cls >> "rMouth")[0]; _rMouth[0] *= coef;
	_rMouth[1] = (*cls >> "rMouth")[1]; _rMouth[1] *= coef;
}
