// global redefiniton of new, delete operators
#include "wpch.hpp"
#include "winpch.hpp"
#include "global.hpp"
#include <new.h>

void __cdecl ErrorMessage( const char *format, ... );

// manage memory pool
// use standard malloc/free
#if _MSC_VER
#define MEM_CONV __cdecl
#else
#define MEM_CONV
#endif

static int MEM_CONV OutOfMemory( size_t size )
{
	ErrorMessage("Out of memory");
	// return 1 to force malloc to retry
	return 0;
}
void InitMemory( int size )
{
	(void)size; // system
	_set_new_handler(OutOfMemory);
	_set_new_mode(1);
}
