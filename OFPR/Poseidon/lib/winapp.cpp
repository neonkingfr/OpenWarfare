// Poseidon -- windows application initialization and termination
// (C) 1997, SUMA
// Win95 version
#include "wpch.hpp"
#include "winpch.hpp"

#include "resource.h"
#include <Es/Common/win.h>
#include "debugTrap.hpp"

#define MULTI_MON_WIN 0

#if MULTI_MON_WIN
#define COMPILE_MULTIMON_STUBS 1
#include <multimon.h>
#endif

/**************************************************************************
	Global Variables
**************************************************************************/

// version dependent constants

//#define WIN_STYLE ( WS_CAPTION|WS_MINIMIZEBOX|WS_SYSMENU )
#define WIN_STYLE ( WS_CAPTION|WS_MINIMIZEBOX|WS_SYSMENU|WS_SIZEBOX )
#define WIN_EX_STYLE WS_EX_APPWINDOW

#if 1
	#define FULL_STYLE ( WS_OVERLAPPED|WS_CAPTION|WS_SYSMENU|WS_BORDER|WS_DLGFRAME|WS_CLIPSIBLINGS )
	#define FULL_EX_STYLE ( WS_EX_APPWINDOW|WS_EX_WINDOWEDGE )
#else
	#define FULL_STYLE ( 0 )
	#define FULL_EX_STYLE ( 0 )
#endif


LONG CALLBACK AppWndProc(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);
LONG CALLBACK DebugWndProc(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);

HINSTANCE GHInstance;

#if MULTI_MON_WIN
struct MonEnumContext
{
  HMONITOR monitor;
	RECT rect;
};

static BOOL CALLBACK MonEnumProc
(
  HMONITOR hMonitor,  // handle to display monitor
  HDC hdcMonitor,     // handle to monitor-appropriate device context
  LPRECT lprcMonitor, // pointer to monitor intersection rectangle
  LPARAM dwData       // data passed from EnumDisplayMonitors
)
{
	MonEnumContext *context=(MonEnumContext *)dwData;
	// check if we want this monitor
	// we want to use first non-primary display
	if (lprcMonitor->top!=0 && lprcMonitor->left!=0)
	{
		if (context->monitor==NULL)
		{
			LogF
			(
				"Mon %x: %d,%d..%d,%d",hMonitor,
				lprcMonitor->top,lprcMonitor->left,
				lprcMonitor->bottom,lprcMonitor->right
			);
			context->rect = *lprcMonitor;
			context->monitor = hMonitor;
		}
	}
	return TRUE;
}
#endif

HWND AppInit
(
	HINSTANCE hInst,HINSTANCE hPrev,int sw, bool fullscreen,
	int x, int y, int width, int height
)
{
	#ifndef _XBOX
	// try to create window on secondary display
	RECT rect;
	rect.top = y;
	rect.left = x;
	#if MULTI_MON_WIN
	MonEnumContext context;
	context.monitor = NULL;
	EnumDisplayMonitors(NULL,NULL,MonEnumProc,(LPARAM)&context);
	if (context.monitor)
	{
		rect.top  = context.rect.top;
		rect.left = context.rect.left;
	}
	#endif

	GHInstance = hInst;
	if (!hPrev)
	{
		//***  Register a class for the main application window
		WNDCLASS cls;
		cls.hCursor        = LoadCursor(0,IDC_ARROW);
		cls.hIcon          = LoadIcon(hInst, "APPICON");
		cls.lpszMenuName   = NULL;
		cls.lpszClassName  = AppName;
		cls.hbrBackground  = (HBRUSH)GetStockObject(BLACK_BRUSH);
		cls.hInstance      = hInst;
		cls.style          = CS_VREDRAW | CS_HREDRAW;
		cls.lpfnWndProc    = (WNDPROC)AppWndProc;
		cls.cbClsExtra     = 0;
		cls.cbWndExtra     = 0;
		
		if( !RegisterClass(&cls) ) return NULL;

		WNDCLASS clsDebug;
		clsDebug.hCursor        = LoadCursor(0,IDC_ARROW);
		clsDebug.hIcon          = LoadIcon(hInst, "APPICON");
		clsDebug.lpszMenuName   = NULL;
		clsDebug.lpszClassName  = "DEBUG WINDOW";
		clsDebug.hbrBackground  = (HBRUSH)GetStockObject(BLACK_BRUSH);
		clsDebug.hInstance      = hInst;
		clsDebug.style          = CS_VREDRAW | CS_HREDRAW;
		clsDebug.lpfnWndProc    = (WNDPROC)DebugWndProc;
		clsDebug.cbClsExtra     = 0;
		clsDebug.cbWndExtra     = 0;
		if( !RegisterClass(&clsDebug) ) return NULL;
	}

	HWND hwndApp;
	if( !fullscreen )
	{
		//RECT rect;
		int scrW=width;
		int scrH=height;
		
		//rect.left=0;
		//rect.top=0;
		rect.right = scrW-1+rect.left;
		rect.bottom = scrH-1+rect.top;
		AdjustWindowRectEx(&rect,WIN_STYLE,FALSE,WIN_EX_STYLE);
		scrW=rect.right-rect.left+1;
		scrH=rect.bottom-rect.top+1;

		hwndApp = CreateWindowEx
		(  
			WIN_EX_STYLE,
			AppName,           // Class name
			AppName,           // Caption
			WIN_STYLE,
			rect.left, rect.top,   // Position
			scrW,scrH,             // Size
			NULL,                   // Parent window (no parent)
			NULL,                   // use class menu
			hInst,               // handle to window instance
			NULL                    // no params to pass on
		);
	}
	else
	{
		int scrW=GetSystemMetrics(SM_CXSCREEN);
		int scrH=GetSystemMetrics(SM_CYSCREEN);
		
		//RECT rect;
		//rect.left=0;
		//rect.top=0;
		rect.right=scrW-1 + rect.left;
		rect.bottom=scrH-1 + rect.top;
		BOOL retVal=AdjustWindowRectEx(&rect,FULL_STYLE,FALSE,FULL_EX_STYLE);
		
		hwndApp = CreateWindowEx
		(  
			FULL_EX_STYLE,
			AppName,           // Class name
			AppName,           // Caption
			FULL_STYLE,
			rect.left, rect.top,   // Position
			rect.right-rect.left+1,rect.bottom-rect.top+1,             // Size
			NULL,                   // Parent window (no parent)
			NULL,                   // use class menu
			hInst,               // handle to window instance
			NULL                    // no params to pass on
		);
		RECT client;
		retVal=GetClientRect(hwndApp,&client);

		if( client.bottom<scrH-1 )
		{	
			int enlarge=scrH-1-client.bottom;
			rect.bottom+=enlarge;
			client.bottom+=enlarge;
			SetWindowPos
			(
				hwndApp,HWND_TOP,
				rect.left,rect.top,
				rect.right-rect.left+1,rect.bottom-rect.top+1,
				0
			);
			//SetWindowSize(rect
		}
	}
	
	//if (!GDebugger.IsDebugger())
	{
		//SetForegroundWindow(hwndApp);
		ShowWindow(hwndApp,sw);
		UpdateWindow(hwndApp);
		SetFocus(hwndApp);
	}
	
	return hwndApp;
	#else
	return NULL;
	#endif
}

// MSI has its own window class - force our own attributes

extern bool LandEditor;

void InitWindow( HINSTANCE hInst, HWND hwnd )
{
	#ifndef _XBOX
	// set our text
	if( LandEditor )
	{
		SetWindowText(hwnd,"Buldozer");
	}
	else
	{
		SetWindowText(hwnd,AppName);
	}
	// set our own icon
	HICON icon=LoadIcon(hInst,"APPICON");
	SetClassLong(hwnd,GCL_HICON,(LONG)icon);
	#endif
}

