#ifdef _MSC_VER
#pragma once
#endif

#ifndef _MAP_TYPES_HPP
#define _MAP_TYPES_HPP

DEFINE_ENUM_BEG(MapType)
	MapTree,MapSmallTree,MapBush,
	MapBuilding,MapHouse,
	MapForestBorder,MapForestTriangle,MapForestSquare,
	MapChurch,MapChapel,MapCross,
	MapRock,
	MapBunker, MapFortress,
	MapFountain,
	MapViewTower, MapLighthouse, MapQuay,
	MapFuelstation, MapHospital,
	MapFence, MapWall,
	MapHide,
	MapBusStop,
	NMapTypes
DEFINE_ENUM_END(MapType)

#endif
