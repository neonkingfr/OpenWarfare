#ifdef _MSC_VER
#pragma once
#endif

#ifndef _SOUNDSCENE_HPP
#define _SOUNDSCENE_HPP

#include "types.hpp"
#include <El/Math/math3d.hpp>
#include "soundsys.hpp"
#include "handledList.hpp"

//typedef Ref<AbstractWave> SoundHandle;

class Speaker;
class RadioChannel;
class RadioMessage;

enum {ChannelEast,ChannelWest,ChannelGuerrila,NChannels};

struct SoundPars;

#include "time.hpp"

class SoundScene
{ // remmember global sounds
	AutoArray< SRef<Speaker> > _speakers;
	SRef<Speaker> _speakerPlayer;

	RefArray<AbstractWave> _globalSounds;

	LinkArray<AbstractWave> _all2DSounds;
	LinkArray<AbstractWave> _all3DSounds;

	Ref<AbstractWave> _musicTrack;

	float _musicVolume; // volume controls for _musicTrack
	float _musicTargetVolume;
	Time _musicTargetTime;

	float _soundVolume; // sound controls for all sounds with accomodation
	float _soundTargetVolume;
	Time _soundTargetTime;

	float _musicInternalVolume; // given from config / wave
	float _musicInternalFrequency; // given from config / wave

	enum {MaxEnvSounds=8};
	Ref<AbstractWave> _envSounds[MaxEnvSounds]; // four corners of environment, two stages
	bool _envSoundRenewed[MaxEnvSounds];

	float _earAccomodation;
	
	public:
	SoundScene();
	~SoundScene();
	
	AbstractWave *Open( const char *filename, bool is3D=true, bool accom=true );
	AbstractWave *OpenAndPlay
	(
		const char *filename, Vector3Par pos, Vector3Par speed,
		float volume=1.0, float frequency=1.0
	);
	AbstractWave *OpenAndPlayOnce
	(
		const char *filename, Vector3Par pos, Vector3Par speed,
		float volume=1.0, float frequency=1.0
	);
	AbstractWave *OpenAndPlay2D
	(
		const char *filename,
		float volume=1.0, float frequency=1.0,
		bool accomodate=true
	);
	AbstractWave *OpenAndPlayOnce2D
	(
		const char *filename,
		float volume=1.0, float frequency=1.0,
		bool accomodate=true
	);
	void SimulateSpeedOfSound(AbstractWave *sound);

	void AddSound( AbstractWave *wave ); // global sounds (Ref is in SoundScene)
	void DeleteSound( AbstractWave *wave );

	void AdvanceAll( float deltaT, bool paused ); // maintain all sounds 
	void Reset();

	// environmental sounds
	void SetEnvSound( const char *name, float volume );
	void AdvanceEnvSounds();
	void StartMusicTrack(const SoundPars &pars, float from=0);
	void StopMusicTrack();
	//AbstractWave *GetMusicTrack();

	float GetMusicVolume() const;
	void SetMusicVolume(float vol, float time);

	float GetSoundVolume() const;
	void SetSoundVolume(float vol, float time);

	AbstractWave *Say( int speakerID, RString id );
	AbstractWave *SayPause( float time );
	//void Transmit( int channel, int speaker, RadioMessage *msg );
};

extern SoundScene *GSoundScene;

#endif
