// implementation of network message transfer

#include "wpch.hpp"
#include "networkImpl.hpp"
#include "debugTrap.hpp"
#include <El/Common/perfLog.hpp>
#include "perfProf.hpp"
#include "global.hpp"
#include "idString.hpp"

#include <Es/Algorithms/qsort.hpp>

#include "crc.hpp"
#include "ai.hpp"

/*!
\file
Interface file for low-level multiplayer functions
*/


///////////////////////////////////////////////////////////////////////////////
// common string table

// note: following strings are obtained from MP report file
// (see NetStrStats)

// note: due to compression used most used string should be listed first

static RStringB NetStrings[]=
{
	// weapons and magazines
	"M16",
	"AK74",
	"HandGrenade",
	"M60",
	"M4",
	"Put",
	"Throw",
	"NVGoggles",
	"CarHorn",
	"TruckHorn",

	"AK47",
	"KozliceBall",
	"KozliceShell",
	"M21",
	"GrenadeLauncher",
	"AK47CZ",
	"Kozlice",
	"AK47GrenadeLauncher",

	"SmokeShell",
	"Binocular",
	"Bullet7_6W",
	"Bullet12_7W",
	"Bullet7_6E",
	"Bullet12_7E",
	"Bullet7_6G",
	"Bullet12_7G",

	"BulletBurstW",
  "BulletSingleW",
	"BulletFullAutoW",
	"BulletBurstE",
  "BulletSingleE",
	"BulletFullAutoE",
	"BulletBurstG",
  "BulletSingleG",
	"BulletFullAutoG",

	"ZsuCannon",
	"MachineGun7_6",
	"Wire",
	"PK",
	"AT3Launcher",
	"Heat73",
	"Gun73",
	"AK74SU",
	"Shell73",
	"Heat120",
	"Gun120",
	"Shell120",
	"RPGLauncher",
	"LAWLauncher",
	"StrokeFist",

	"HellfireLauncherCobra",
	"MachineGun30",
	"ZuniLauncher38",

	// vehicle class names
	"SoldierEB",
	"SoldierECrew",
	"SoldierWB",
	"SoldierWCrew",
	"FenceWood",
	"Danger",
	"ObjectDestructed",

	// TODO: motion states

	// format (type) messages
	"objectId",
	"objectPosition",
	"objectCreator",
	"vehicle",
	"dammage",
	"canSmoke",
	"isDestroyed",
	"destroyed",
	"targetSide",
	"name",
	"type",
	"magazines",
	"initialUpdate",
	"weapons",
	"currentWeapon",
	"magazineSlots",
	"action",
	"pilotLight",
	"fireTarget",
	"hit",
	"isDead",
	"position",
	"fuelCargo",
	"ammoCargo",
	"infantryAmmoCargo",
	"repairCargo",
	"alloc",
	"actionParam",
	"actionParam2",
	"id",
	"actionParam3",
	"supplying",
};

/*!
\patch 1.25 Date 9/28/2001 by Ondra
- Optimized: MP network bandwidth optimizations.
\patch_internal 1.25 Date 9/28/2001 by Ondra
- New: Strings replaced by id if known.
*/

static IdStringTable NetIdStrings(NetStrings,sizeof(NetStrings)/sizeof(*NetStrings));

// note: due to compression used most used string should be listed first

static RStringB NetMoves[]=
{
	// basic movement
	"stand",
	"combatsprintrf",
	"combatsprintf",
	"combatsprintlf",
	"combat",
	"combatrunr",
	"combatrunl",
	"combatrunb",
	"combatrunf",
	"combatrunrb",
	"crouchrunf",
	"combatrelaxed",
	"combatturnlrelaxed",
	"combatturnrrelaxed",
	"combatrelaxedstill",
	"lyingfastcrawlf",
	"lyingcrawlf",
	"crouch",
	"lying",
	"crouchrunr",
	"combatrunlb",
	"crouchrunl",
	"crouchrunrf",
	"civilrunf",
	"crouchrunlf",
	"combatwalkf",
	"combatrunrf",
	"lyingcrawlrf",
	"crouchrunlb",
	"civil",
	"combatrunlf",
	"crouchrunb",
	"crouchrunrb",
	"lyingcrawllf",
	"civillying",
	"lyingcrawlb",
	"lyingcrawllb",
	"crouchsprintf",
	"combatdeadver2",
	"civillyingcrawlf",
	"lyingcrawlrb",
	"combatrundead",
	"civillyingfastcrawlf",
	"combatturnr",
	"lyingturnl",
	"combatturnl",
	"civilwalkf",
	"lyingdead",
	"combatstillplayer",
	"lyingturnr",
	"combatwalkr",
	"civilstillv1",
	"civilrunrf",
	"combatdeadver3",
	"putdown",
	"binoclying",
	"binoc",
};

static IdStringTable NetIdMoves(NetMoves,sizeof(NetMoves)/sizeof(*NetMoves));

///////////////////////////////////////////////////////////////////////////////
// Message format

template Ref<NetworkObject>;

NetworkMessageFormatBase::NetworkMessageFormatBase()
{
}

void NetworkMessageFormatBase::Init(NetworkMessageIndices *indices)
{
	DoAssert(indices);
	DoAssert(_map.NItems() == 0);

	// create _map
	int n = _items.Size();
	for (int i=0; i<n; i++)
	{
		const char *name = _items[i].name;

		// avoid duplicity in names
		#if _ENABLE_REPORT
		const NameToIndex &check = _map[name];
		DoAssert(_map.IsNull(check));
		#endif

		_map.Add(NameToIndex(name, i));
	}

	// scan indices
	_indices = indices;
	_indices->Scan(this);
}

void NetworkMessageFormatBase::Clear()
{
	_items.Clear();
	_map.Clear();
}

int NetworkMessageFormatBase::FindIndex(const char *name) const
{
	const NameToIndex &item = _map[name];
	if (_map.NotNull(item))
		return item.index;
	else
	{
LogF("Warning: item %s not found", name);
		return -1;
	}
}

//! network message indices for NetworkMessageFormatItem class
class IndicesMsgFormatItem : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int name;
	int type;
	int compression;
	int defValue;
	//@}

	//! Constructor
	IndicesMsgFormatItem();
	NetworkMessageIndices *Clone() const {return new IndicesMsgFormatItem;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesMsgFormatItem::IndicesMsgFormatItem()
{
	name = -1;
	type = -1;
	compression = -1;
	defValue = -1;
}

void IndicesMsgFormatItem::Scan(NetworkMessageFormatBase *format)
{
	SCAN(name)
	SCAN(type)
	SCAN(compression)
	SCAN(defValue)
}

//! Create network message indices for NetworkMessageFormatItem class
NetworkMessageIndices *GetIndicesMsgFormatItem() {return new IndicesMsgFormatItem();}

NetworkMessageFormat &NetworkMessageFormatItem::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("name", NDTString, NCTDefault, ND_NULL, DOC_MSG("Item name"));
	format.Add("type", NDTInteger, NCTSmallUnsigned, ND_NULL, DOC_MSG("Item type"));
	format.Add("compression", NDTInteger, NCTSmallUnsigned, ND_NULL, DOC_MSG("Item compression"));
	format.Add("defValue", NDTData, NCTNone, ND_NULL, DOC_MSG("Default value of item"));
	return format;
}

TMError NetworkMessageFormatItem::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesMsgFormatItem *>(ctx.GetIndices()))
	const IndicesMsgFormatItem *indices = static_cast<const IndicesMsgFormatItem *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransfer(indices->name, name))
	TMCHECK(ctx.IdxTransfer(indices->type, (int &)type))
	TMCHECK(ctx.IdxTransfer(indices->compression, (int &)compression))
	TMCHECK(ctx.IdxTransfer(indices->defValue, type, compression, defValue))
	return TMOK;
}

//! network message indices for NetworkMessageFormatBase class
class IndicesMsgFormat : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int index;
	int items;
	//@}

	//! Constructor
	IndicesMsgFormat();
	NetworkMessageIndices *Clone() const {return new IndicesMsgFormat;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesMsgFormat::IndicesMsgFormat()
{
	index = -1;
	items = -1;
}

void IndicesMsgFormat::Scan(NetworkMessageFormatBase *format)
{
	SCAN(index)
	SCAN(items)
}

//! Create network message indices for NetworkMessageFormatBase class
NetworkMessageIndices *GetIndicesMsgFormat() {return new IndicesMsgFormat();}

//! Return index of field "index" in IndicesMsgFormat
int IndicesMsgFormatGetIndex(const NetworkMessageIndices *ind)
{
	Assert(dynamic_cast<const IndicesMsgFormat *>(ind))
	const IndicesMsgFormat *indices = static_cast<const IndicesMsgFormat *>(ind);

	return indices->index;
}

NetworkMessageFormat &NetworkMessageFormatBase::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("index", NDTInteger, NCTSmallUnsigned, ND_NULL, DOC_MSG("Index of message type"));
	format.Add("items", NDTObjectArray, NCTNone, DEFVALUE_MSG(NMTMsgFormatItem), DOC_MSG("List of items"));
	return format;
}

TMError NetworkMessageFormatBase::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesMsgFormat *>(ctx.GetIndices()))
	const IndicesMsgFormat *indices = static_cast<const IndicesMsgFormat *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferArray(indices->items, _items))
	return TMOK;
}

int NetworkMessageFormat::Add
(
	const char *name,
	NetworkDataType type,
	NetworkCompressionType compression,
	const RefNetworkData &defValue,
	const char *description,
	NetworkMessageErrorType errType,
	float errCoef
)
{
	// add to items
	int index = _items.Add();
	NetworkMessageFormatItem &item = _items[index];
	item.name = name;
	item.type = type;
	item.compression = compression;
	item.defValue = defValue;

	// add to errors
	DoVerify(_errors.Add() == index);
	NetworkMessageErrorInfo &info = _errors[index];
	info.type = errType;
	info.coef = errCoef;

	// add to descriptions
#if DOCUMENT_MSG_FORMATS
	DoVerify(_descriptions.Add() == index);
	_descriptions[index] = description;
#endif

	return index;
}

int NetworkMessageFormat::Add
(
	const char *name,
	NetworkDataType type,
	NetworkCompressionType compression,
	int defValue,
	const char *description,
	NetworkMessageErrorType errType,
	float errCoef
)
{
	Assert(defValue==NULL);
	// add to items
	int index = _items.Add();
	NetworkMessageFormatItem &item = _items[index];
	item.name = name;
	item.type = type;
	item.compression = compression;
	item.defValue = RefNetworkDataNull();

	// add to errors
	DoVerify(_errors.Add() == index);
	NetworkMessageErrorInfo &info = _errors[index];
	info.type = errType;
	info.coef = errCoef;

	// add to descriptions
#if DOCUMENT_MSG_FORMATS
	DoVerify(_descriptions.Add() == index);
	_descriptions[index] = description;
#endif

	return index;
}

NetworkMessageFormat *NetworkMessageFormat::Init(NetworkMessageIndices *indices)
{
	base::Init(indices);

	return this;
}

void NetworkMessageFormat::Clear()
{
	base::Clear();
	_errors.Clear();
}

// val1.GetVal is called to check if we know type
// if not, dynamic_cast will assert

#if _RELEASE
	#define CHECK_TYPE(a) 
#else
	#define CHECK_TYPE(a) (a->GetVal())
#endif

// common case - used for scalar types
template <class Type>
float RefNetworkDataTyped<Type>::CalculateError(NetworkMessageErrorType type, const RefNetworkDataTyped<Type> &with, NetworkMessageFormatItem &item, float dt) const
{
	const Type &value = GetVal();
	const Type &withValue = with.GetVal();

	switch (type)
	{
		case ET_NOT_EQUAL:
			return value != withValue ? 1 : 0;
		case ET_ABS_DIF:
			return fabs(value - withValue);
		case ET_SQUARE_DIF:
			return Square(value - withValue);
		case ET_DER_DIF:
			return dt * fabs(value - withValue);
		default:
			Fail("Unexpected error type");
			return 0;
	}
}


template <>
float RefNetworkDataTyped<RString>::CalculateError(NetworkMessageErrorType type, const RefNetworkDataTyped<RString> &with, NetworkMessageFormatItem &item, float dt) const
{
	switch (type)
	{
		case ET_NOT_EQUAL:
			return stricmp(GetVal(), with.GetVal()) != 0 ? 1 : 0;
		default:
			Fail("Unexpected error type");
			return 0;
	}
}

template <>
float RefNetworkDataTyped<Vector3>::CalculateError(NetworkMessageErrorType type, const RefNetworkDataTyped<Vector3> &with, NetworkMessageFormatItem &item, float dt) const
{
	Vector3Val value = GetVal();
	Vector3Val withValue = with.GetVal();
	switch (type)
	{
		case ET_NOT_EQUAL:
			return value != withValue ? 1 : 0;
		case ET_SQUARE_DIF:
			return value.Distance2(withValue);
		case ET_ABS_DIF:
			return value.Distance(withValue);
		case ET_DER_DIF:
			return dt * value.Distance(withValue);
		default:
			Fail("Unexpected error type");
			return 0;
	}
}

template <>
float RefNetworkDataTyped<Matrix3>::CalculateError(NetworkMessageErrorType type, const RefNetworkDataTyped<Matrix3> &with, NetworkMessageFormatItem &item, float dt) const
{
	Matrix3Val value = GetVal();
	Matrix3Val withValue = with.GetVal();
	switch (type)
	{
		case ET_SQUARE_DIF:
			{
				float err;
				err = value.DirectionAside().Distance2(withValue.DirectionAside());
				err += value.DirectionUp().Distance2(withValue.DirectionUp());
				err += value.Direction().Distance2(withValue.Direction());
				return err;
			}
		case ET_ABS_DIF:
			{
				float err;
				err = value.DirectionAside().Distance(withValue.DirectionAside());
				err += value.DirectionUp().Distance(withValue.DirectionUp());
				err += value.Direction().Distance(withValue.Direction());
				return err;
			}
		case ET_DER_DIF:
			{
				float err;
				err = value.DirectionAside().Distance(withValue.DirectionAside());
				err += value.DirectionUp().Distance(withValue.DirectionUp());
				err += value.Direction().Distance(withValue.Direction());
				return dt * err;
			}
		default:
			Fail("Unexpected error type");
			return 0;
	}
}

// specialized constructor
template <>
NetworkDataTyped< AutoArray<char> >::NetworkDataTyped(void *val, int size)
:value((char *)val,size)
{
}

const int MaxAbsInt16 = 0x4000;
// we want to have 0.5 represented exactly
// we also want to keep some reserve range in case small overflow will occur

inline int EncodeFloat16(float x)
{
	Assert(x>=-1);
	Assert(x<=+1);
	int r = toInt(MaxAbsInt16*x);
	saturate(r,-MaxAbsInt16,+MaxAbsInt16);
	return r;
}

__forceinline float DecodeFloat16(short x)
{
	const float coef = 1.0/MaxAbsInt16;
	return x*coef;
}

/*!
\patch 1.27 Date 10/12/2001 by Ondra
- Optimized: MP: more compression on position updates.
*/

void EncodedMatrix3::Encode(const Matrix3 &m)
{
	// encoding is quite simple
	_01c = EncodeFloat16(m(0,1));
	_11c = EncodeFloat16(m(1,1));
	// note: abs. value can be deduced from previous value, sign not
	_21sign = m(2,1)>=0 ? 1 : -1;
	_02c = EncodeFloat16(m(0,2));
	_12c = EncodeFloat16(m(1,2));
	_22sign = m(2,2)>=0 ? 1 : -1;
	// verify matrix is orthogonal
	#if _DEBUG
		float m21_sq = 1-m(0,1)*m(0,1)-m(1,1)*m(1,1);
		float m21 = (m21_sq>=0 ? sqrt(m21_sq) : 0)*_21sign;
		DoAssert (fabs(m21-m(2,1))<1e-3);
		float m22_sq = 1-m(0,2)*m(0,2)-m(1,2)*m(1,2);
		float m22 = (m22_sq >=0 ? sqrt(m22_sq) : 0)*_22sign;
		DoAssert (fabs(m22-m(2,2))<1e-3);
		Vector3 mAside = m.DirectionUp().CrossProduct(m.Direction());
		DoAssert (mAside.Distance2(m.DirectionAside())<1e-6);
	#endif
}

/*!
\patch 1.93 Date 9/11/2003 by Ondra
- Fixed: MP: Small numeric error in vehicle orientation network transfer could make
gunner aiming very difficult (cursor could move with no reason).
*/

void EncodedMatrix3::Decode(Matrix3 &m) const
{
	// decoding is a little bit more tricky
	// decode col. 1 (up)
	Vector3 up;
	up.Init();
	up[0] = DecodeFloat16(_01c);
	up[1] = DecodeFloat16(_11c);
	float m21_sq = 1-up[0]*up[0]-up[1]*up[1];
	up[2] = (m21_sq>=0 ? sqrt(m21_sq) : 0)*_21sign;

	// decode col. 0 (direction)
	Vector3 dir;
	dir.Init();
	dir[0] = DecodeFloat16(_02c);
	dir[1] = DecodeFloat16(_12c);
	float m22_sq = 1-dir[0]*dir[0]-dir[1]*dir[1];
	dir[2] = (m22_sq >=0 ? sqrt(m22_sq) : 0)*_22sign;

	m.SetDirectionAndUp(dir,up);
}

void EncodedMatrix3::Decode(Matrix4 &m) const
{
	// decoding is a little bit more tricky
	// decode col. 1 (up)
	Vector3 up;
	up.Init();
	up[0] = DecodeFloat16(_01c);
	up[1] = DecodeFloat16(_11c);
	float m21_sq = 1-up[0]*up[0]-up[1]*up[1];
	up[2] = (m21_sq>=0 ? sqrt(m21_sq) : 0)*_21sign;

	// decode col. 0 (direction)
	Vector3 dir;
	dir.Init();
	dir[0] = DecodeFloat16(_02c);
	dir[1] = DecodeFloat16(_12c);
	float m22_sq = 1-dir[0]*dir[0]-dir[1]*dir[1];
	dir[2] = (m22_sq >=0 ? sqrt(m22_sq) : 0)*_22sign;
	
	m.SetDirectionAndUp(dir,up);
}

Vector3 EncodedMatrix3::DirectionUp() const
{
	float m01 = DecodeFloat16(_01c);
	float m11 = DecodeFloat16(_11c);

	float m21_sq = 1-m01*m01-m11*m11;
	float m21 = (m21_sq>=0 ? sqrt(m21_sq) : 0)*_21sign;
	return Vector3(m01,m11,m21);
}

Vector3 EncodedMatrix3::Direction() const
{
	float m02 = DecodeFloat16(_02c);
	float m12 = DecodeFloat16(_12c);
	float m22_sq = 1-m02*m02-m12*m12;
	float m22 = (m22_sq >=0 ? sqrt(m22_sq) : 0)*_22sign;
	return Vector3(m02,m12,m22);
}
/*!
\patch_internal 1.15 Date 8/10/2001 by Ondra
- Improved: Specialized combined packets using raw message.
*/

template <>
float RefNetworkDataTyped< AutoArray<char> >::CalculateError(NetworkMessageErrorType type, const RefNetworkDataTyped< AutoArray<char> > &with, NetworkMessageFormatItem &item, float dt) const
{
	const AutoArray<char> &value = GetVal();
	const AutoArray<char> &withValue = with.GetVal();

	switch (type)
	{
		case ET_UPD_ENTITY_POS:
		{
			// check data size
			if (value.Size()!=sizeof(NetworkUpdEntityPos)) return 0;
			if (withValue.Size()!=sizeof(NetworkUpdEntityPos)) return 0;
			const NetworkUpdEntityPos &d1 = *(NetworkUpdEntityPos *)value.Data();
			const NetworkUpdEntityPos &d2 = *(NetworkUpdEntityPos *)withValue.Data();
			float err;
			err = d1.position.Distance(d2.position);

			err += d1.orientation.DirectionUp().Distance(d2.orientation.DirectionUp());
			err += d1.orientation.Direction().Distance(d2.orientation.Direction());

			err += dt * d1.speed.Distance(d2.speed);
			err += dt * d1.angMomentum.Distance(d2.angMomentum);

			return err;
		}
		case ET_UPD_MAN_POS:
		{
			if (value.Size()!=sizeof(NetworkUpdManPos)) return 0;
			if (withValue.Size()!=sizeof(NetworkUpdManPos)) return 0;
			const NetworkUpdManPos &d1 = *(NetworkUpdManPos *)value.Data();
			const NetworkUpdManPos &d2 = *(NetworkUpdManPos *)withValue.Data();
			float err;
			err  = fabs(CompareRot8b(d1.gunXRotWantedC,d2.gunXRotWantedC)) * ERR_COEF_VALUE_MINOR;
			err += fabs(CompareRot8b(d1.gunYRotWantedC,d2.gunYRotWantedC)) * ERR_COEF_VALUE_MINOR;
			err += fabs(CompareRot8b(d1.headXRotWantedC,d2.headXRotWantedC)) * ERR_COEF_VALUE_MINOR;
			err += fabs(CompareRot8b(d1.headYRotWantedC,d2.headYRotWantedC)) * ERR_COEF_VALUE_MINOR;
			return err;
		}
		default:
			Fail("Unexpected error type");
			return 0;
	}
}

template <>
float RefNetworkDataTyped< AutoArray<int> >::CalculateError(NetworkMessageErrorType type, const RefNetworkDataTyped< AutoArray<int> > &with, NetworkMessageFormatItem &item, float dt) const
{
	const AutoArray<int> &value = GetVal();
	const AutoArray<int> &withValue = with.GetVal();

	switch (type)
	{
		case ET_NOT_EQUAL:
			{
				if (value.Size() != withValue.Size()) return 1;
				for (int i=0; i<value.Size(); i++)
				{
					if (value[i] != withValue[i]) return 1;
				}
				return 0;
			}
		default:
			Fail("Unexpected error type");
			return 0;
	}
}

template <>
float RefNetworkDataTyped< AutoArray<float> >::CalculateError(NetworkMessageErrorType type, const RefNetworkDataTyped< AutoArray<float> > &with, NetworkMessageFormatItem &item, float dt) const
{
	const AutoArray<float> &value = GetVal();
	const AutoArray<float> &withValue = with.GetVal();

	switch (type)
	{
		case ET_NOT_EQUAL:
			{
				if (value.Size() != withValue.Size()) return 1;
				for (int i=0; i<value.Size(); i++)
				{
					if (value[i] != withValue[i]) return 1;
				}
				return 0;
			}
		case ET_ABS_DIF:
			{
				int minSize = min(value.Size(), withValue.Size());
				float error = value.Size() - minSize + withValue.Size() - minSize;
				for (int i=0; i<minSize; i++)
					error += fabs(value[i] - withValue[i]);
				return error;
			}
		default:
			Fail("Unexpected error type");
			return 0;
	}
}

template <>
float RefNetworkDataTyped< AutoArray<RString> >::CalculateError(NetworkMessageErrorType type, const RefNetworkDataTyped< AutoArray<RString> > &with, NetworkMessageFormatItem &item, float dt) const
{
	const AutoArray<RString> &value = GetVal();
	const AutoArray<RString> &withValue = with.GetVal();

	switch (type)
	{
		case ET_NOT_EQUAL:
			{
				if (value.Size() != withValue.Size()) return 1;
				for (int i=0; i<value.Size(); i++)
				{
					if (value[i] != withValue[i]) return 1;
				}
				return 0;
			}
		default:
			Fail("Unexpected error type");
			return 0;
	}
}

template <>
float RefNetworkDataTyped<RadioSentence>::CalculateError(NetworkMessageErrorType type, const RefNetworkDataTyped<RadioSentence> &with, NetworkMessageFormatItem &item, float dt) const
{
	Fail("Unexpected error type");
	return 0;
}

float RefNetworkDataTyped<NetworkMessage>::CalculateError(NetworkMessageErrorType type, const RefNetworkDataTyped<NetworkMessage> &with, NetworkMessageFormatItem &item, float dt) const
{
	// message format
	CHECK_ASSIGN(formatType,item.defValue,const RefNetworkDataTyped<int>);
	NetworkMessageFormat *format = GMsgFormats[formatType.GetVal()];

	NetworkMessage &msg1 = GetVal();
	NetworkMessage &msg2 = with.GetVal();

	float errValue = 0;
	for (int i=0; i<format->NItems(); i++)
	{
		const NetworkMessageErrorInfo &info = format->GetErrorInfo(i);
		if (info.type == ET_NONE) continue;

		const RefNetworkData &value1 = msg1.values[i];
		const RefNetworkData &value2 = msg2.values[i];
		errValue += info.coef * value1.CalculateError(info.type, value2, format->GetItem(i), dt);
	}
	
	return errValue;
}

NetworkMessageFormat &MagazineNetworkInfo::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("type", NDTString, NCTDefault, DEFVALUE(RString, ""), DOC_MSG("Magazine type"));
	format.Add("ammo", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Ammo count"));
	format.Add("burstLeft", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("How many shots are there in the burst (auto fired)"));
	format.Add("reload", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Time rest to reload shot"));
	format.Add("reloadMagazine", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Time rest to reload magazine"));
	format.Add("creator", NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("Network ID of magazine"));
	format.Add("id", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Network ID of magazine"));
	return format;
}

TMError MagazineNetworkInfo::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesMagazine *>(ctx.GetIndices()))
	const IndicesMagazine *indices = static_cast<const IndicesMagazine *>(ctx.GetIndices());

	ITRANSF(type)
	ITRANSF(ammo)
	ITRANSF(burstLeft)
	ITRANSF(reload)
	ITRANSF(reloadMagazine)
	ITRANSF(creator)
	ITRANSF(id)
	return TMOK;
}

TMError MagazineNetworkInfo::TransferMsgSimple(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesMagazine *>(ctx.GetIndices()))
	const IndicesMagazine *indices = static_cast<const IndicesMagazine *>(ctx.GetIndices());

	ITRANSF(ammo)
	ITRANSF(creator)
	ITRANSF(id)

	return TMOK;
}

/*!
\patch 1.23 Date 09/13/2001 by Jirka
- Improved: Multiplayer server performance (CPU load) improved
\patch_internal 1.23 Date 09/13/2001 by Jirka
- Changed: error calculation for array of magazines
*/
template <>
float RefNetworkDataTyped< AutoArray<NetworkMessage> >::CalculateError(NetworkMessageErrorType type, const RefNetworkDataTyped< AutoArray<NetworkMessage> > &with, NetworkMessageFormatItem &item, float dt) const
{
	// non-const value required here, because NetworkMessageContext requires
	// NetworkMessage *, but messages are actually not changed.
	AutoArray<NetworkMessage> &value = GetVal();
	AutoArray<NetworkMessage> &withValue = with.GetVal();

	switch (type)
	{
		case ET_MAGAZINES:
			{
				// magazines
				// RefArray<Magazine> magazines1, magazines2;
				// AutoArray<MagazineNetworkInfo> magazines1, magazines2;
				AUTO_STATIC_ARRAY(MagazineNetworkInfo, magazines1, 32);
				AUTO_STATIC_ARRAY(MagazineNetworkInfo, magazines2, 32);

				// messages format
				NetworkComponent *component = GNetworkManager.GetServer();
				const RefNetworkData &defVal = item.defValue;
				CHECK_ASSIGN(defValTyped, defVal, const RefNetworkDataTyped<int>)
				NetworkMessageFormatBase *formatItem = component->GetFormat((NetworkMessageType)defValTyped.GetVal());

				// transfer 1
				int n = value.Size();
				magazines1.Resize(n);
				for (int i=0; i<n; i++)
				{
					NetworkMessageContext ctx(&value[i], formatItem, component, 0, false);
					// magazines1[i] = Magazine::CreateObject(ctx);
					// TMCHECK(magazines1[i]->TransferMsg(ctx))
					if (magazines1[i].TransferMsgSimple(ctx) != TMOK) return 0;
				}
				// transfer 2
				n = withValue.Size();
				magazines2.Resize(n);
				for (int i=0; i<n; i++)
				{
					NetworkMessageContext ctx(&withValue[i], formatItem, component, 0, false);
					// magazines2[i] = Magazine::CreateObject(ctx);
					// TMCHECK(magazines2[i]->TransferMsg(ctx))
					if (magazines2[i].TransferMsgSimple(ctx) != TMOK) return 0;
				}

				bool changed = magazines1.Size() != magazines2.Size();
				int ammoDiff = 0;
				if (!changed) for (int i=0; i<magazines1.Size(); i++)
					if (magazines1[i]._creator != magazines2[i]._creator || magazines1[i]._id != magazines2[i]._id)
					{
						changed = true; break;
					}
					else ammoDiff += abs(magazines1[i]._ammo - magazines2[i]._ammo);
				if (changed) return ERR_COEF_MODE;
				else return ammoDiff * ERR_COEF_VALUE_MINOR;
			}
		default:
			Fail("Unexpected error type");
			return 0;
	}
}

template <>
float RefNetworkDataTyped<NetworkId>::CalculateError(NetworkMessageErrorType type, const RefNetworkDataTyped<NetworkId> &with, NetworkMessageFormatItem &item, float dt) const
{
	NetworkId value = GetVal();
	NetworkId withValue = with.GetVal();

	switch (type)
	{
		case ET_NOT_EQUAL:
			return value != withValue ? 1 : 0;
		default:
			Fail("Unexpected error type");
			return 0;
	}
}

template <>
float RefNetworkDataTyped< AutoArray<NetworkId> >::CalculateError(NetworkMessageErrorType type, const RefNetworkDataTyped< AutoArray<NetworkId> > &with, NetworkMessageFormatItem &item, float dt) const
{
	const AutoArray<NetworkId> &value = GetVal();
	const AutoArray<NetworkId> &withValue = with.GetVal();

	switch (type)
	{
		case ET_NOT_EQUAL_COUNT:
			{
				Assert(value.Size() == withValue.Size()); // TODO: enable
				int count = min(value.Size(), withValue.Size());
				int dif = 0;
				for (int i=0; i<count; i++)
				{
					if (value[i] != withValue[i]) dif++;
				}
				return dif;
			}
		case ET_NOT_CONTAIN_COUNT:
			{
				int dif = 0;
				for (int i=0; i<withValue.Size(); i++)
				{
					NetworkId ref2 = withValue[i];
					if (ref2.IsNull()) continue;
					bool found = false;
					for (int j=0; j<value.Size(); j++)
						if (value[j] == ref2)
						{
							found = true;
							break;
						}
					if (!found) dif++;
				}
				return dif;
			}
		default:
			Fail("Unexpected error type");
			return 0;
	}
}

float RefNetworkData::CalculateError(NetworkMessageErrorType type, const RefNetworkData &value2, NetworkMessageFormatItem &item, float dt) const
{
	switch (item.type)
	{
		#define ERR_CASE(caseType) \
		{ \
			const RefNetworkDataTyped< caseType > *val1 = static_cast<const RefNetworkDataTyped< caseType > *>(this); \
			const RefNetworkDataTyped< caseType > *val2 = static_cast<const RefNetworkDataTyped< caseType > *>(&value2); \
			CHECK_TYPE(val1); \
			CHECK_TYPE(val2); \
			return val1->CalculateError(type,*val2,item,dt); \
		}
		case NDTBool: ERR_CASE(bool)
		case NDTInteger: ERR_CASE(int)
		case NDTFloat: ERR_CASE(float)
		case NDTString: ERR_CASE(RString)
		case NDTRawData: ERR_CASE(AutoArray<char>)
		case NDTTime: ERR_CASE(Time)
		case NDTVector: ERR_CASE(Vector3)
		case NDTMatrix: ERR_CASE(Matrix3)
		case NDTIntArray: ERR_CASE(AutoArray<int>)
		case NDTFloatArray: ERR_CASE(AutoArray<float>)
		case NDTStringArray: ERR_CASE(AutoArray<RString>)
		case NDTSentence: ERR_CASE(RadioSentence)
		case NDTObject: ERR_CASE(NetworkMessage)
		case NDTObjectArray: ERR_CASE(AutoArray<NetworkMessage>)
		case NDTRef: ERR_CASE(NetworkId)
		case NDTRefArray: ERR_CASE(AutoArray<NetworkId>)
		//case NDTData: ERR_CASE(xxx)
		#undef ERR_CASE
		default:
			Fail("Error calculation not implemented");
			return 0;
	}
}

NetworkDataWithFormat::NetworkDataWithFormat
(
	NetworkDataType type, 
	NetworkCompressionType compression,
	const RefNetworkData &data
)
{
	format.name = "data";
	format.type = type;
	format.compression = compression;
	format.defValue = data;
}

float RefNetworkDataWithFormat::CalculateError
(
	NetworkMessageErrorType type, const RefNetworkDataWithFormat &with,
	NetworkMessageFormatItem &item, float dt
) const
{
	Fail("Not implemented");
	return 0;
}

int RefNetworkDataWithFormat::CalculateSize(NetworkCompressionType compression, const NetworkMessageFormatItem &item) const
{
	Fail("Not implemented");
	return 0;
}


// size calculation - simple types
//@{
//! Calculate size of serialized and compressed value
/*!
\param value value itself
\param compression compression algorithm
*/
template <class Type>
int CalculateValueSize(const Type &value, NetworkCompressionType compression)
{
	switch (compression)
	{
		case NCTNone: case NCTDefault:
			return sizeof(value);
		default:
			ErrF("Unsupported compression method %d", compression);
			return 0;
	}
}


template <>
int CalculateValueSize(const int &value, NetworkCompressionType compression)
{
	switch (compression)
	{
		case NCTNone:
			return sizeof(int);
		case NCTSmallUnsigned:
			{
				int size = 0;
				unsigned int val = value;
				for (;;)
				{
					val >>= 7;
					size++;
					if (!val) return size;
				}
			}
		case NCTSmallSigned:
			{
				int size = 0;
				unsigned int val;
				if (value >= 0)
					val = value << 1;
				else
					val = (-value << 1) | 1;
				for (;;)
				{
					val >>= 7;
					size++;
					if (!val) return size;
				}
			}
		default:
			RptF("Unsupported compression method %d", compression);
			return 0;
	}
}

template <>
int CalculateValueSize(const Vector3 &value, NetworkCompressionType compression)
{
	switch (compression)
	{
		case NCTDefault:
		case NCTNone:
			return 3*sizeof(float);
		default:
			RptF("Unsupported compression method %d", compression);
			return 0;
	}
}

template <>
int CalculateValueSize(const Matrix3 &value, NetworkCompressionType compression)
{
	switch (compression)
	{
		case NCTNone:
		case NCTDefault:
			return 9*sizeof(float);
		case NCTMatrixOrientation:
			return sizeof(EncodedMatrix3);
		default:
			RptF("Unsupported compression method %d", compression);
			return 0;
	}
}

template <>
int CalculateValueSize(const RString &value, NetworkCompressionType compression)
{
	switch (compression)
	{
		case NCTNone:
			return value.GetLength() + 1;
			break;
		case NCTStringGeneric: // NCTDefault
		{
			int id = NetIdStrings.GetId(value);
			// 0 will be transmitted to indicate no ID
			// therefore we add 1 to offset zero to 1
			// -1 is not used, so that unsigned compression can be used
			if (id>=0) return CalculateValueSize(id+1,NCTSmallUnsigned);
			return value.GetLength() + 2;
		}
		case NCTStringMove:
		{
			int id = NetIdMoves.GetId(value);
			// 0 will be transmitted to indicate no ID
			// therefore we add 1 to offset zero to 1
			// -1 is not used, so that unsigned compression can be used
			if (id>=0) return CalculateValueSize(id+1,NCTSmallUnsigned);
			return value.GetLength() + 2;
		}
		default:
			RptF("Unsupported compression method %d", compression);
			return 0;
	}
}

template <>
int CalculateValueSize(const NetworkId &value, NetworkCompressionType compression)
{
	switch (compression)
	{
		case NCTNone: case NCTDefault:
			return 2 * sizeof(int);
		default:
			RptF("Unsupported compression method %d", compression);
			return 0;
	}
}

#define FLOAT_COMPRESSION 1


template <>
int CalculateValueSize(const float &value, NetworkCompressionType compression)
{
	switch (compression)
	{
		float min,max,invRange;
		#define RANGE(a,b) min = (a), max = (b), invRange = 1.0f/((b)-(a))
		case NCTFloatMostly0To1:
			RANGE(0,1);
			goto CompressionUnlimited;
		case NCTFloatAngle:
			RANGE(-H_PI,+H_PI);
			goto CompressionUnlimited;
		case NCTFloat0To2:
		case NCTFloat0To1:
		case NCTFloatM1ToP1:
			#if FLOAT_COMPRESSION
			return 1;
			#endif

		CompressionUnlimited:
			#if FLOAT_COMPRESSION
			{
				float compressed = (value-min)*invRange;
				int iValue = toInt(compressed*254+127);
				return CalculateValueSize(iValue,NCTSmallSigned);
			}
			#endif			
		case NCTNone: case NCTDefault:
			return sizeof(value);
		default:
			ErrF("Unsupported compression method %d", compression);
			return 0;
		#undef RANGE
	}
}

//@}

// size calculation - compound types
template <class Type>
int RefNetworkDataTyped<Type>::CalculateSize(NetworkCompressionType compression, const NetworkMessageFormatItem &item) const
{
	const Type &value = GetVal();
	return CalculateValueSize(value, compression);
}


template <>
int RefNetworkDataTyped<Time>::CalculateSize(NetworkCompressionType compression, const NetworkMessageFormatItem &item) const
{
	switch (compression)
	{
		case NCTNone: case NCTDefault:
			return sizeof(int);
		default:
			ErrF("Unsupported compression method %d", compression);
			return 0;
	}
}

template <>
int RefNetworkDataTyped< AutoArray<char> >::CalculateSize(NetworkCompressionType compression, const NetworkMessageFormatItem &item) const
{
	const AutoArray<char> &value = GetVal();
	int size = 0;
	int m = value.Size();
	size += CalculateValueSize(m, NCTSmallUnsigned);
	for (int j=0; j<m; j++)
		size += CalculateValueSize(value[j], compression);
	return size;
}

template <>
int RefNetworkDataTyped< AutoArray<int> >::CalculateSize(NetworkCompressionType compression, const NetworkMessageFormatItem &item) const
{
	const AutoArray<int> &value = GetVal();
	int size = 0;
	int m = value.Size();
	size += CalculateValueSize(m, NCTSmallUnsigned);
	for (int j=0; j<m; j++)
		size += CalculateValueSize(value[j], compression);
	return size;
}

template <>
int RefNetworkDataTyped< AutoArray<float> >::CalculateSize(NetworkCompressionType compression, const NetworkMessageFormatItem &item) const
{
	const AutoArray<float> &value = GetVal();
	int size = 0;
	int m = value.Size();
	size += CalculateValueSize(m, NCTSmallUnsigned);
	for (int j=0; j<m; j++)
		size += CalculateValueSize(value[j], compression);
	return size;
}

template <>
int RefNetworkDataTyped< AutoArray<RString> >::CalculateSize(NetworkCompressionType compression, const NetworkMessageFormatItem &item) const
{
	const AutoArray<RString> &value = GetVal();
	int size = 0;
	int m = value.Size();
	size += CalculateValueSize(m, NCTSmallUnsigned);
	for (int j=0; j<m; j++)
		size += CalculateValueSize(value[j], compression);
	return size;
}

template <>
int RefNetworkDataTyped< AutoArray<NetworkId> >::CalculateSize(NetworkCompressionType compression, const NetworkMessageFormatItem &item) const
{
	const AutoArray<NetworkId> &value = GetVal();
	int size = 0;
	int m = value.Size();
	size += CalculateValueSize(m, NCTSmallUnsigned);
	for (int j=0; j<m; j++)
		size += CalculateValueSize(value[j], compression);
	return size;
}

template <>
int RefNetworkDataTyped<RadioSentence>::CalculateSize(NetworkCompressionType compression, const NetworkMessageFormatItem &item) const
{
	const RadioSentence &value = GetVal();
	int size = 0;
	int m = value.Size();
	size += CalculateValueSize(m, NCTSmallUnsigned);
	for (int j=0; j<m; j++)
	{
		size += CalculateValueSize(value[j].id, compression);
		size += CalculateValueSize(value[j].pauseAfter, compression);
	}
	return size;
}

int RefNetworkData::CalculateSize
(
	NetworkCompressionType compression, const NetworkMessageFormatItem &item
) const
{
	switch (item.type)
	{
		#define ERR_CASE(caseType) \
		{ \
			const RefNetworkDataTyped< caseType > *val1 = static_cast<const RefNetworkDataTyped< caseType > *>(this); \
			CHECK_TYPE(val1); \
			return val1->CalculateSize(compression,item); \
		}
		case NDTBool: ERR_CASE(bool)
		case NDTInteger: ERR_CASE(int)
		case NDTFloat: ERR_CASE(float)
		case NDTString: ERR_CASE(RString)
		case NDTRawData: ERR_CASE(AutoArray<char>)
		case NDTTime: ERR_CASE(Time)
		case NDTVector: ERR_CASE(Vector3)
		case NDTMatrix: ERR_CASE(Matrix3)
		case NDTIntArray: ERR_CASE(AutoArray<int>)
		case NDTFloatArray: ERR_CASE(AutoArray<float>)
		case NDTStringArray: ERR_CASE(AutoArray<RString>)
		case NDTSentence: ERR_CASE(RadioSentence)
		case NDTObject: ERR_CASE(NetworkMessage)
		case NDTObjectArray: ERR_CASE(AutoArray<NetworkMessage>)
		case NDTRef: ERR_CASE(NetworkId)
		case NDTRefArray: ERR_CASE(AutoArray<NetworkId>)
		//case NDTData: ERR_CASE(xxx)
		#undef ERR_CASE
		default:
			Fail("Size calculation not implemented");
			return 0;
	}
}

///////////////////////////////////////////////////////////////////////////////
// Message context

void NetworkMessageContext::PrepareMessage()
{
	Assert(_format)
	int n = _format->NItems();
	_msg->values.Resize(n);
	for (int i=0; i<n; i++)
	{
		const NetworkMessageFormatItem &item = _format->GetItem(i);
		if (item.type == 	NDTObject) _msg->values[i] = RefNetworkDataTyped<NetworkMessage>();
		else if (item.type == NDTObjectArray) _msg->values[i] = RefNetworkDataTyped< AutoArray<NetworkMessage> >();
		else _msg->values[i] = item.defValue;
	}
}


void NetworkMessageContext::LogMessage(int level, RString indent) const
{
	DiagLogF
	(
		"%sid (time = %.3f)",
		(const char *)indent, _msg->time.toFloat()
	);
	if (level < 3) return;
	int n = _format->NItems();
	for (int i=0; i<n; i++)
	{
		const NetworkMessageFormatItem &item = _format->GetItem(i);
		const RefNetworkData &val = _msg->values[i];
		switch (item.type)
		{
		case NDTBool:
			{
				CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped<bool>)
				DiagLogF
				(
					"%s%s = %s", (const char *)indent,
					(const char *)item.name, valTyped.GetVal() ? "true" : "false"
				);
			}
			break;
		case NDTInteger:
			{
				CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped<int>)
				DiagLogF
				(
					"%s%s = %d", (const char *)indent,
					(const char *)item.name, valTyped.GetVal()
				);
			}
			break;
		case NDTFloat:
			{
				CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped<float>)
				DiagLogF
				(
					"%s%s = %f", (const char *)indent,
					(const char *)item.name, valTyped.GetVal()
				);
			}
			break;
		case NDTString:
			{
				CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped<RString>)
				DiagLogF
				(
					"%s%s = %s", (const char *)indent,
					(const char *)item.name, (const char *)valTyped.GetVal()
				);
			}
			break;
		case NDTRawData:
			{
				CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped< AutoArray<char> >)
				DiagLogF
				(
					"%s%s = %x, size %d", (const char *)indent,
					(const char *)item.name, valTyped.GetVal().Data(), valTyped.GetVal().Size()
				);
			}
			break;
		case NDTTime:
			{
				CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped<Time>)
				DiagLogF
				(
					"%s%s = %.3f", (const char *)indent,
					(const char *)item.name, valTyped.GetVal().toFloat()
				);
			}
			break;
		case NDTVector:
			{
				CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped<Vector3>)
				DiagLogF
				(
					"%s%s = {%f, %f, %f}", (const char *)indent,
					(const char *)item.name, valTyped.GetVal().X(),
					valTyped.GetVal().Y(), valTyped.GetVal().Z()
				);
			}
			break;
		case NDTMatrix:
			{
				CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped<Matrix3>)
				DiagLogF
				(
					"%s%s = ", (const char *)indent,
					(const char *)item.name
				);
				DiagLogF
				(
					"%s\t%f, %f, %f", (const char *)indent,
					valTyped.GetVal()(0, 0), valTyped.GetVal()(0, 1),
					valTyped.GetVal()(0, 2)
				);
				DiagLogF
				(
					"%s\t%f, %f, %f", (const char *)indent,
					valTyped.GetVal()(1, 0), valTyped.GetVal()(1, 1),
					valTyped.GetVal()(1, 2)
				);
				DiagLogF
				(
					"%s\t%f, %f, %f", (const char *)indent,
					valTyped.GetVal()(2, 0), valTyped.GetVal()(2, 1),
					valTyped.GetVal()(2, 2)
				);
			}
			break;
		case NDTIntArray:
			{
				CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped< AutoArray<int> >)
				DiagLogF
				(
					"%s%s:", (const char *)indent,
					(const char *)item.name
				);
				const AutoArray<int> &array = valTyped.GetVal();
				int m = array.Size();
				for (int j=0; j<m; j++)
				{
					DiagLogF
					(
						"%s\tItem %d = %d", (const char *)indent,
						j, array[j]
					);
				}
			}
			break;
		case NDTFloatArray:
			{
				CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped< AutoArray<float> >)
				DiagLogF
				(
					"%s%s:", (const char *)indent,
					(const char *)item.name
				);
				const AutoArray<float> &array = valTyped.GetVal();
				int m = array.Size();
				for (int j=0; j<m; j++)
				{
					DiagLogF
					(
						"%s\tItem %d = %f", (const char *)indent,
						j, array[j]
					);
				}
			}
			break;
		case NDTStringArray:
			{
				CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped< AutoArray<RString> >)
				DiagLogF
				(
					"%s%s:", (const char *)indent,
					(const char *)item.name
				);
				const AutoArray<RString> &array = valTyped.GetVal();
				int m = array.Size();
				for (int j=0; j<m; j++)
				{
					DiagLogF
					(
						"%s\tItem %d = %s", (const char *)indent,
						j, (const char *)array[j]
					);
				}
			}
			break;
		case NDTSentence:
			{
				CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped<RadioSentence>)
				DiagLogF
				(
					"%s%s:", (const char *)indent,
					(const char *)item.name
				);
				const RadioSentence &array = valTyped.GetVal();
				int m = array.Size();
				for (int j=0; j<m; j++)
				{
					DiagLogF
					(
						"%s\tItem %d = %s, %.3f", (const char *)indent,
						j, (const char *)array[j].id, array[j].pauseAfter
					);
				}
			}
			break;
		case NDTObject:
			{
				// access to message
				CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped<NetworkMessage>)
				DiagLogF
				(
					"%s%s:", (const char *)indent,
					(const char *)item.name
				);

				NetworkMessage &msg = const_cast<NetworkMessage &>(valTyped.GetVal());

				// message format
				const RefNetworkData &defVal = item.defValue;
				CHECK_ASSIGN(defValTyped,defVal,const RefNetworkDataTyped<int>);
				NetworkMessageFormatBase *msgFormat = _component->GetFormat((NetworkMessageType)defValTyped.GetVal());

				NetworkMessageContext ctx(&msg, msgFormat, *const_cast<NetworkMessageContext *>(this));
				ctx.LogMessage(level, indent + RString("\t"));
			}
			break;
		case NDTObjectArray:
			{
				// access to message array
				CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped< AutoArray<NetworkMessage> >)
				DiagLogF
				(
					"%s%s:", (const char *)indent,
					(const char *)item.name
				);

				const AutoArray<NetworkMessage> &array = valTyped.GetVal();

				// messages format
				const RefNetworkData &defVal = item.defValue;
				CHECK_ASSIGN(defValTyped,defVal,const RefNetworkDataTyped<int>);
				NetworkMessageFormatBase *msgFormat = _component->GetFormat((NetworkMessageType)defValTyped.GetVal());

				int m = array.Size();
				for (int j=0; j<m; j++)
				{
					DiagLogF
					(
						"%s\tItem %d:", (const char *)indent, j
					);
					NetworkMessage &msg = const_cast<NetworkMessage &>(array[j]);
					NetworkMessageContext ctx(&msg, msgFormat, *const_cast<NetworkMessageContext *>(this));
					ctx.LogMessage(level, indent + RString("\t\t"));
				}
			}
			break;
		case NDTRef:
			{
				CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped<NetworkId>)
				DiagLogF
				(
					"%s%s = %d:%d", (const char *)indent,
					(const char *)item.name,
					valTyped.GetVal().creator, valTyped.GetVal().id
				);
			}
			break;
		case NDTRefArray:
			{
				CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped< AutoArray<NetworkId> >)
				DiagLogF
				(
					"%s%s:", (const char *)indent,
					(const char *)item.name
				);
				const AutoArray<NetworkId> &array = valTyped.GetVal();
				int m = array.Size();
				for (int j=0; j<m; j++)
				{
					DiagLogF
					(
						"%s\tItem %d = %d:%d", (const char *)indent,
						j, array[j].creator, array[j].id
					);
				}
			}
			break;
		case NDTData:
			{
				DiagLogF
				(
					"%s%s = <generic data>", (const char *)indent,
					(const char *)item.name
				);
			}
			break;
		default:
			RptF("Bad data type %d", item.type);
			break;
		}
	}
}


TMError NetworkMessageContext::IdxTransfer(int index, bool &value)
{
	TRANSFER_FORMAT
	Assert
	(
		item.type == NDTBool
	);
	TRANSFER(bool)
}

TMError NetworkMessageContext::IdxTransfer(int index, int &value)
{
	TRANSFER_FORMAT
	Assert
	(
		item.type == NDTInteger
	);
	TRANSFER(int)
}

TMError NetworkMessageContext::IdxTransfer(int index, float &value)
{
	TRANSFER_FORMAT
	Assert
	(
		item.type == NDTFloat
	);
	TRANSFER(float)
}

TMError NetworkMessageContext::IdxTransfer(int index, RString &value)
{
	TRANSFER_FORMAT
	Assert
	(
		item.type == NDTString
	);
	TRANSFER(RString)
}

/*!
transfer raw data. Optimized memory allocation,
as there is no need create temporary AutoArray<char> buffer.
*/

TMError NetworkMessageContext::IdxGetRaw(int index, void *&value, int &size)
{
	Assert(!_sending);
	TRANSFER_FORMAT
	const RefNetworkData &val = _msg->values[index];
	CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped< AutoArray<char> >)
	value = valTyped.GetVal().Data();
	size = valTyped.GetVal().Size();
	return TMOK;
}

TMError NetworkMessageContext::IdxSendRaw(int index, void *value, int size)
{
	Assert(_sending);
	TRANSFER_FORMAT
	Assert
	(
		item.type == NDTRawData
	);
	_msg->values[index] = RefNetworkDataTyped< AutoArray<char> >(value,size);
	return TMOK;
}


TMError NetworkMessageContext::IdxTransfer(int index, Time &value)
{
	TRANSFER_FORMAT
	Assert
	(
		item.type == NDTTime
	);
	TRANSFER(Time)
}

TMError NetworkMessageContext::IdxTransfer(int index, Vector3 &value)
{
	TRANSFER_FORMAT
	Assert
	(
		item.type == NDTVector
	);
	TRANSFER(Vector3)
}

TMError NetworkMessageContext::IdxTransfer(int index, Matrix3 &value)
{
	TRANSFER_FORMAT
	Assert
	(
		item.type == NDTMatrix
	);
	TRANSFER(Matrix3)
}

/*!
See also NetworkMessageContext::IdxTransferRaw
for version with direct data access.
*/

/*
TMError NetworkMessageContext::IdxTransfer(int index, AutoArray<char> &value)
{
	TRANSFER_FORMAT
	Assert
	(
		item.type == NDTRawData
	);

	TRANSFER(AutoArray<char>)
}

TMError NetworkMessageContext::IdxTransfer(int index, AutoArray<int> &value)
{
	TRANSFER_FORMAT
	Assert
	(
		item.type == NDTIntArray
	);
	TRANSFER(AutoArray<int>)
}

TMError NetworkMessageContext::IdxTransfer(int index, AutoArray<float> &value)
{
	TRANSFER_FORMAT
	Assert
	(
		item.type == NDTFloatArray
	);
	TRANSFER(AutoArray<float>)
}

TMError NetworkMessageContext::IdxTransfer(int index, AutoArray<RString> &value)
{
	TRANSFER_FORMAT
	Assert
	(
		item.type == NDTStringArray
	);
	TRANSFER(AutoArray<RString>)
}
*/

TMError NetworkMessageContext::IdxTransfer(int index, RadioSentence &value)
{
	TRANSFER_FORMAT
	Assert
	(
		item.type == NDTSentence
	);
	TRANSFER(RadioSentence)
}

TMError NetworkMessageContext::IdxGetId(int index, NetworkId &id)
{
	if (_sending) return TMGeneric;
	TRANSFER_FORMAT
	Assert(item.type == NDTRef);
	const RefNetworkData &val = _msg->values[index];
	CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped<NetworkId>)
	id = valTyped.GetVal();
	return TMOK;
}

TMError NetworkMessageContext::IdxGetIds(int index, AutoArray<NetworkId> &array)
{
	if (_sending) return TMGeneric;
	TRANSFER_FORMAT
	Assert(item.type == NDTRefArray);
	const RefNetworkData &val = _msg->values[index];
	CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped< AutoArray<NetworkId> >)
	array = valTyped.GetVal();
	return TMOK;
}

TMError NetworkMessageContext::IdxTransfer
(
	int index,
	NetworkDataType &type, NetworkCompressionType &compression,
	RefNetworkData &defVal
)
{
	TRANSFER_FORMAT
	Assert(item.type == NDTData);

	if (_sending)
	{
		_msg->values[index] = RefNetworkDataWithFormat(type, compression, defVal);
	}
	else
	{
		const RefNetworkData &val = _msg->values[index];
		CHECK_ASSIGN(valTyped, val, const RefNetworkDataWithFormat)
		type = valTyped->format.type;
		compression = valTyped->format.compression;
		defVal = valTyped->format.defValue;
	}
	
	return TMOK;
}

///////////////////////////////////////////////////////////////////////////////
// Raw message

void NetworkMessageRaw::Put(bool value, NetworkCompressionType compression)
{
	switch (compression)
	{
		case NCTNone: case NCTDefault:
			Write(&value, sizeof(bool));
			break;
		default:
			ErrF("Unsupported compression method %d", compression);
			break;
	}
}

void NetworkMessageRaw::Put(char value, NetworkCompressionType compression)
{
	switch (compression)
	{
		case NCTNone: case NCTDefault:
			Write(&value, sizeof(char));
			break;
		default:
			ErrF("Unsupported compression method %d", compression);
			break;
	}
}

void NetworkMessageRaw::Put(int value, NetworkCompressionType compression)
{
	switch (compression)
	{
		case NCTNone:
			Write(&value, sizeof(int));
			break;
		case NCTSmallUnsigned:
			{
				unsigned int val = value;
				for (;;)
				{
					unsigned char c = val & 0x7f;
					val >>= 7;
					if (val)
					{
						c |= 0x80;
						Write(&c, sizeof(unsigned char));
					}
					else
					{
						// no more bits left
						Write(&c, sizeof(unsigned char));
						break;
					}
				}
			}
			break;
		case NCTSmallSigned:
			{
				unsigned int val;
				if (value >= 0)
					val = value << 1;
				else
					val = (-value << 1) | 1;
				for (;;)
				{
					unsigned char c = val & 0x7f;
					val >>= 7;
					if (val)
					{
						c |= 0x80;
						Write(&c, sizeof(unsigned char));
					}
					else
					{
						// no more bits left
						Write(&c, sizeof(unsigned char));
						break;
					}
				}
			}
			break;
		default:
			ErrF("Unsupported compression method %d", compression);
			break;
	}
}

void NetworkMessageRaw::Put(float value, NetworkCompressionType compression)
{
	switch (compression)
	{
		float min,max,invRange;
		#define RANGE(a,b) min = (a), max = (b), invRange = 1.0f/((b)-(a))
		case NCTFloat0To1:
			RANGE(0,1);
			goto Compression;
		case NCTFloat0To2:
			RANGE(0,2);
			goto Compression;
		case NCTFloatM1ToP1:
			RANGE(-1,+1);
			goto Compression;
		case NCTFloatAngle:
			RANGE(-H_PI,+H_PI);
			goto CompressionUnlimited;
		case NCTFloatMostly0To1:
			RANGE(0,1);
			goto CompressionUnlimited;
		#undef RANGE
		Compression:
			#if FLOAT_COMPRESSION
			//DoAssert(value>=min);
			//DoAssert(value<=max);
			{
				// map min to 0, max to 254
				// note: 254 is used instead of 255, so that 0 with min = max
				// can be represented exactly
				float compressed = (value-min)*invRange;
				int iValue = toInt(compressed*254);
				saturate(iValue,0,254);
				unsigned char cValue = iValue;
				Write(&cValue,sizeof(cValue));
			}
			break;
			#endif
		CompressionUnlimited:
			#if FLOAT_COMPRESSION
			{
				// map min to -128, max to 127
				float compressed = (value-min)*invRange;
				int iValue = toInt(compressed*254-127);
				Put(iValue,NCTSmallSigned);
			}
			break;
			#endif
		case NCTNone: case NCTDefault:
			Write(&value, sizeof(value));
			break;
		default:
			ErrF("Unsupported compression method %d", compression);
			break;
	}
}

#if _ENABLE_CHEATS
	#include "statistics.hpp"

	StatisticsByName NetStrStats;
	StatisticsByName NetMoveStats;

	static class NetStrStatsReportClass
	{
		public:
		~NetStrStatsReportClass()
		{
			NetStrStats.Report();
			NetMoveStats.Report();
		}
	} NetStrStatsReport;
#endif

void NetworkMessageRaw::Put(RString value, NetworkCompressionType compression)
{
	switch (compression)
	{
		case NCTStringGeneric: //NCTDefault:
		{
			int id = NetIdStrings.GetId(value);
			if (id>=0)
			{
				Put(id+1,NCTSmallUnsigned);
			}
			else
			{
				Assert( strcmpi(value,"HandGrenade") );
				Put(0,NCTSmallUnsigned);
				Write((const char *)value, value.GetLength() + 1);
				#if _ENABLE_CHEATS
					NetStrStats.Count(value);
				#endif
			}
			break;
		}
		case NCTStringMove:
		{
			int id = NetIdMoves.GetId(value);
			if (id>=0)
			{
				Put(id+1,NCTSmallUnsigned);
			}
			else
			{
				Put(0,NCTSmallUnsigned);
				Write((const char *)value, value.GetLength() + 1);
				#if _ENABLE_CHEATS
					NetMoveStats.Count(value);
				#endif
			}
			break;
		}
		case NCTNone:
			Write((const char *)value, value.GetLength() + 1);
			break;
		default:
			ErrF("Unsupported compression method %d", compression);
			break;
	}
}

void NetworkMessageRaw::Put(Time value, NetworkCompressionType compression)
{
	switch (compression)
	{
		case NCTNone: case NCTDefault:
			Put(value.toInt(), NCTNone);
			break;
		default:
			ErrF("Unsupported compression method %d", compression);
			break;
	}
}

void NetworkMessageRaw::Put(Vector3Par value, NetworkCompressionType compression)
{
	switch (compression)
	{
		case NCTNone: case NCTDefault:
			for (int i=0; i<3; i++)
				Put(value[i], NCTNone);
			break;
		default:
			ErrF("Unsupported compression method %d", compression);
			break;
	}
}

void NetworkMessageRaw::Put(Matrix3Par value, NetworkCompressionType compression)
{
	switch (compression)
	{
		case NCTNone: case NCTDefault:
			for (int i=0; i<3; i++)
				for (int j=0; j<3; j++)
					Put(value(i, j), NCTNone);
			break;
		case NCTMatrixOrientation:
		{
			EncodedMatrix3 encoded;
			encoded.Encode(value);
			Write(&encoded,sizeof(encoded));
			break;
		}
		default:
			ErrF("Unsupported compression method %d", compression);
			break;
	}
}

void NetworkMessageRaw::Put(NetworkId &value, NetworkCompressionType compression)
{
	switch (compression)
	{
		case NCTNone: case NCTDefault:
			Put(value.creator, NCTNone);
			Put(value.id, NCTNone);
			break;
		default:
			ErrF("Unsupported compression method %d", compression);
			break;
	}
}

bool NetworkMessageRaw::Get(bool &value, NetworkCompressionType compression)
{
	switch (compression)
	{
		case NCTNone: case NCTDefault:
			return Read(&value, sizeof(bool));
		default:
			ErrF("Unsupported compression method %d", compression);
			return false;
	}
}

bool NetworkMessageRaw::Get(char &value, NetworkCompressionType compression)
{
	switch (compression)
	{
		case NCTNone: case NCTDefault:
			return Read(&value, sizeof(value));
		default:
			ErrF("Unsupported compression method %d", compression);
			return false;
	}
}

bool NetworkMessageRaw::Get(int &value, NetworkCompressionType compression)
{
	switch (compression)
	{
		case NCTNone:
			return Read(&value, sizeof(value));
		case NCTSmallUnsigned:
			{
				unsigned int val = 0;
				int offset = 0;
				while (true)
				{
					unsigned char c;
					if (!Read(&c, sizeof(unsigned char))) return false;
					// transfer 7 bits ber byte
					val |= (c & 0x7f) << offset;
					// check terminator
					if ((c & 0x80) == 0) break;
					offset += 7;
				}
				value = val;
			}
			return true;
		case NCTSmallSigned:
			{
				unsigned int val = 0;
				int offset = 0;
				while (true)
				{
					unsigned char c;
					if (!Read(&c, sizeof(unsigned char))) return false;
					// transfer 7 bits ber byte
					val |= (c & 0x7f) << offset;
					// check terminator
					if ((c & 0x80) == 0) break;
					offset += 7;
				}
				if (val & 1)
					value = -(int)(val >> 1);
				else
					value = val >> 1;
			}
			return true;
		default:
			ErrF("Unsupported compression method %d", compression);
			return false;
	}
}

bool NetworkMessageRaw::Get(float &value, NetworkCompressionType compression)
{
	switch (compression)
	{
		float min,coef;
		#define RANGE(a,b) min = (a), coef = float((b)-(a))/254
		case NCTFloat0To1:
			RANGE(0,1);
			goto Compression;
		case NCTFloat0To2:
			RANGE(0,2);
			goto Compression;
		case NCTFloatM1ToP1:
			RANGE(-1,+1);
			goto Compression;
		case NCTFloatAngle:
			RANGE(-H_PI,+H_PI);
			goto CompressionUnlimited;
		case NCTFloatMostly0To1:
			RANGE(0,1);
			goto CompressionUnlimited;
		#undef RANGE
		Compression:
			#if FLOAT_COMPRESSION
			{
				unsigned char cValue;
				if (!Read(&cValue,sizeof(cValue))) return false;
				value = cValue*coef+min;
			}
			return true;
			#endif
		CompressionUnlimited:
			#if FLOAT_COMPRESSION
			{
				int iValue;
				if (!Get(iValue,NCTSmallSigned)) return false;
				value = (iValue+127)*coef+min;
			}
			return true;
			#endif
		case NCTNone: case NCTDefault:
			return Read(&value, sizeof(float));
		default:
			ErrF("Unsupported compression method %d", compression);
			return false;
	}
}

bool NetworkMessageRaw::GetNoCompression(RString &value)
{
	int lastPos = _pos;
	if (_externalBuffer)
	{
		while (lastPos < _externalBufferSize && _externalBuffer[lastPos]) lastPos++;
		if (lastPos >= _externalBufferSize) return false;
		value = _externalBuffer + _pos;
	}
	else
	{
		while (lastPos < _buffer.Size() && _buffer[lastPos]) lastPos++;
		if (lastPos >= _buffer.Size()) return false;
		value = _buffer.Data() + _pos;
	}
	_pos = lastPos + 1;
	return true;
}

bool NetworkMessageRaw::Get(RString &value, NetworkCompressionType compression)
{
	switch (compression)
	{
		case NCTNone:
			return GetNoCompression(value);
		case NCTStringGeneric: //NCTDefault:
		{
			int id;
			bool ok = Get(id,NCTSmallUnsigned);
			if (!ok) return false;
			if (id>0)
			{
				value = NetIdStrings.GetString(id-1);
				return true;
			}
			else
			{
				ok = GetNoCompression(value);
				return ok;
			}
		}
		case NCTStringMove:
		{
			int id;
			bool ok = Get(id,NCTSmallUnsigned);
			if (!ok) return false;
			if (id>0)
			{
				value = NetIdMoves.GetString(id-1);
				return true;
			}
			else
			{
				ok = GetNoCompression(value);
				return ok;
			}
		}
		default:
			ErrF("Unsupported compression method %d", compression);
			return false;
	}
}

bool NetworkMessageRaw::Get(Time &value, NetworkCompressionType compression)
{
	switch (compression)
	{
		case NCTNone: case NCTDefault:
			{
				int intVal;
				if (!Get(intVal, NCTNone)) return false;
				value = Time(intVal);
				return true;
			}
		default:
			ErrF("Unsupported compression method %d", compression);
			return false;
	}
}

bool NetworkMessageRaw::Get(Vector3 &value, NetworkCompressionType compression)
{
	switch (compression)
	{
		case NCTNone: case NCTDefault:
			{
				for (int i=0; i<3; i++)
				{
					if (!Get(value[i], NCTNone)) return false;
				}
				return true;
			}
		default:
			ErrF("Unsupported compression method %d", compression);
			return false;
	}
}

bool NetworkMessageRaw::Get(Matrix3 &value, NetworkCompressionType compression)
{
	switch (compression)
	{
		case NCTNone: case NCTDefault:
			{
				for (int i=0; i<3; i++)
					for (int j=0; j<3; j++)
					{
						if (!Get(value(i, j), NCTNone)) return false;
					}
				return true;
			}
		case NCTMatrixOrientation:
		{
			EncodedMatrix3 encoded;
			Read(&encoded,sizeof(encoded));
			encoded.Decode(value);
			return true;
		}
		default:
			ErrF("Unsupported compression method %d", compression);
			return false;
	}
}

bool NetworkMessageRaw::Get(NetworkId &value, NetworkCompressionType compression)
{
	switch (compression)
	{
		case NCTNone: case NCTDefault:
			if (!Get(value.creator, NCTNone)) return false;
			if (!Get(value.id, NCTNone)) return false;
			return true;
		default:
			ErrF("Unsupported compression method %d", compression);
			return false;
	}
}

///////////////////////////////////////////////////////////////////////////////
// Network Components

// low level transfer functions

bool NetworkClient::DXSendMsg(int to, NetworkMessageRaw &rawMsg, DWORD &msgID, NetMsgFlags flags)
{
	Assert(_client);
	Assert(to == TO_SERVER);

	PROFILE_SCOPE(dPlay);
	return _client->SendMsg((BYTE *)rawMsg.GetData(), rawMsg.GetSize(), msgID, flags);
}

bool NetworkServer::DXSendMsg(int to, NetworkMessageRaw &rawMsg, DWORD &msgID, NetMsgFlags dwFlags)
{
	Assert(_server);

	//{ DEDICATED SERVER SUPPORT
	#if _ENABLE_DEDICATED_SERVER
	if (_dedicated) _monitorOut += rawMsg.GetSize();
	#endif
	//}

	PROFILE_SCOPE(dPlay);
	return _server->SendMsg(to, (BYTE *)rawMsg.GetData(), rawMsg.GetSize(), msgID, dwFlags);
}

#if _ENABLE_CHEATS
extern bool outputLogs;
#endif

int NetworkComponent::CalculateMessageSize(NetworkMessage *msg, NetworkMessageType type)
{
	DoAssert(type != NMTMessages);

	NetworkMessageFormatBase *format = GetFormat(type);

	int size = 0;
	size += CalculateValueSize((int)type, NCTSmallUnsigned);
	size += CalculateValueSize(msg->time, NCTNone);
	size += CalculateMsgSize(msg, format);
	return size;
}

DWORD NetworkComponent::SendMsg
(
	int to, NetworkMessage *msg,
	NetworkMessageType type, NetMsgFlags dwFlags
)
{
	#if _ENABLE_MP
	if (to == TO_SERVER)
	{
		NetworkServer *server = _parent->GetServer();
		if (server)
		{
			msg->size = 0;
//			server->OnMessage(GetPlayer(), msg, type);
			server->AddLocalMessage(GetPlayer(), type, msg);
			return 0;
		}
		else if (dwFlags & NMFGuaranteed)
		{
			msg->size = CalculateMessageSize(msg, type);
#if _ENABLE_CHEATS
	StatMsgSent(type, msg->size);
#endif
			if (dwFlags&NMFHighPriority)
			{
				DWORD err = SendMsgRemote(to,msg,type,dwFlags|NMFStatsAlreadyDone);
				if (err==0xffffffff) return err;
				return 0;
			}
			else
			{
				EnqueueMsg(to, msg, type);
				return 0;
			}
		}
		else
		{
			msg->size = CalculateMessageSize(msg, type);
#if _ENABLE_CHEATS
	StatMsgSent(type, msg->size);
#endif
			EnqueueMsgNonGuaranteed(to, msg, type);
			return 1;
//			return SendMsgRemote(to, msg, type, dwFlags);
		}
	}
	else
	{
/*
		NetworkClient *client = _parent->GetClient();
		if (client && client->GetPlayer() == to)
*/
		if (IsLocalClient(to))
		{
			msg->size = 0;
//			client->OnMessage(GetPlayer(), msg, type);
			NetworkClient *client = _parent->GetClient();
			client->AddLocalMessage(GetPlayer(), type, msg);
			return 0;
		}
		else if (dwFlags & NMFGuaranteed)
		{
			msg->size = CalculateMessageSize(msg, type);
#if _ENABLE_CHEATS
	StatMsgSent(type, msg->size);
#endif
			if (dwFlags&NMFHighPriority)
			{
				DWORD err = SendMsgRemote(to,msg,type,dwFlags|NMFStatsAlreadyDone);
				if (err==0xffffffff) return err;
				return 0;
			}
			else
			{
				EnqueueMsg(to, msg, type);
				return 0;
			}
		}
		else
		{
			msg->size = CalculateMessageSize(msg, type);
#if _ENABLE_CHEATS
	StatMsgSent(type, msg->size);
#endif
			EnqueueMsgNonGuaranteed(to, msg, type);
			return 1;
//			return SendMsgRemote(to, msg, type, dwFlags);
		}
	}
	#else
		return E_FAIL;
	#endif
}

/*!
\patch_internal 1.26 Date 10/02/2001 by Ondra
- Fixed: guranteed messages that were not agregated were calculated twice.
*/

DWORD NetworkComponent::SendMsgRemote
(
	int to,
	NetworkMessage *msg,
	NetworkMessageType type,
	NetMsgFlags dwFlags
)
{
	NetworkMessageFormatBase *format = GetFormat(type);
	if (!format) return 0xFFFFFFFF;

#if _ENABLE_CHEATS
	if (outputLogs)
	{
		if (dwFlags&NMFHighPriority)
		{
			DiagLogF("%s: send HP message %s to %d", GetDebugName(), NetworkMessageTypeNames[type], to);
			NetworkMessageContext ctx(msg, format, this, to, MSG_RECEIVE);
			ctx.LogMessage(3, "\t");
		}
	}
#endif

#if _ENABLE_CHEATS
	int level = GetDiagLevel(type,!_parent->GetClient() || to!=_parent->GetClient()->GetPlayer());
	if (level >= 2)
	{
		DiagLogF("%s: send message %s to %d, flags %d", GetDebugName(), NetworkMessageTypeNames[type], to, dwFlags);
		NetworkMessageContext ctx(msg, format, this, to, MSG_RECEIVE);
		ctx.LogMessage(level, "\t");
	}

	if (outputLogs)
		LogF("%s: send message %s to %d, flags %d", GetDebugName(), NetworkMessageTypeNames[type], to, dwFlags);
#else
	int level = 0;
#endif

	NetworkMessageRaw rawMsg;

	// Message header
	rawMsg.Put(type, NCTSmallUnsigned);
	rawMsg.Put(msg->time, NCTNone);					// TODO: compression
	
	// Message body
	EncodeMsg(rawMsg, msg, format);

	msg->size = rawMsg.GetSize();

#if _ENABLE_CHEATS
	// note: all SendMsgRemote message are already counted in stats
	// we can probably remove this flag
	DoAssert ((dwFlags&NMFStatsAlreadyDone)!=0);

	if ((dwFlags&NMFStatsAlreadyDone)==0)
	{
		StatMsgSent(type, msg->size);
	}
#endif

	return SendMsgRaw(to, rawMsg, dwFlags, level);
}

DWORD NetworkComponent::SendMsgQueue
(
	int to, NetworkMessageQueue &queue,
	int begin, int end, NetMsgFlags dwFlags
)
{
	int n = end - begin;

	DoAssert(n > 0);
	if (n == 1)
	{
		return SendMsgRemote
		(
			to, queue[begin].msg, queue[begin].type, dwFlags|NMFStatsAlreadyDone
		);
	}

	NetworkMessageType type = NMTMessages;
	Time time = Glob.time;

#if _ENABLE_CHEATS
	bool remote = !_parent->GetClient() || to!=_parent->GetClient()->GetPlayer();
	int level = GetDiagLevel(type, remote);
	if (level >= 2)
	{
		DiagLogF("%s: send %d messages to %d", GetDebugName(), n, to);
		for (int i=begin; i<end; i++)
		{
			NetworkMessageType type = queue[i].type;
			int level = GetDiagLevel(type, remote);
			if (level < 2) continue;
			DiagLogF("\tMessage %s", NetworkMessageTypeNames[type]);
			NetworkMessageContext ctx(queue[i].msg, GetFormat(type), this, to, MSG_RECEIVE);
			ctx.LogMessage(level, "\t\t");
		}
	}
	else
	{
		for (int i=begin; i<end; i++)
		{
			NetworkMessageType type = queue[i].type;
			int level = GetDiagLevel(queue[i].type,remote);
			if (level < 2) continue;
			DiagLogF("%s: send message %s to %d, flags %d", GetDebugName(), NetworkMessageTypeNames[type], to, dwFlags);
			NetworkMessageContext ctx(queue[i].msg, GetFormat(type), this, to, MSG_RECEIVE);
			ctx.LogMessage(level, "\t");
		}
	}

	if (outputLogs)
		LogF("%s: send messages to %d", GetDebugName(), to);
#else
	int level = 0;
#endif

	NetworkMessageRaw rawMsg;

	// Message header
	rawMsg.Put(type, NCTSmallUnsigned);
	rawMsg.Put(time, NCTNone);					// TODO: compression

	rawMsg.Put(n, NCTSmallUnsigned);
	for (int i=begin; i<end; i++)
	{
		rawMsg.Put(queue[i].type, NCTSmallUnsigned);
		EncodeMsg(rawMsg, queue[i].msg, GetFormat(queue[i].type));
	}

	return SendMsgRaw(to, rawMsg, dwFlags, level);
}

DWORD NetworkComponent::SendMsgRaw(int to, NetworkMessageRaw &rawMsg, NetMsgFlags dwFlags, int diagLevel)
{
#if _ENABLE_DEDICATED_SERVER || _ENABLE_CHEATS
	StatRawMsgSent(to, rawMsg.GetSize());
#endif

	static CRCCalculator calculator;
	int crc = calculator.CRC(rawMsg.GetData(), rawMsg.GetSize());
	rawMsg.Put(crc, NCTNone);

	GDebugger.PauseCheckingAlive();

	DWORD msgID = 0xFFFFFFFF;
	bool result = DXSendMsg(to, rawMsg, msgID, dwFlags);

	int size = rawMsg.GetSize();
	ADD_COUNTER(netSN, 1);
	ADD_COUNTER(netSS, size);

	GDebugger.ResumeCheckingAlive();

#if 0
	LogF("Message %s, size = %d", NetworkMessageTypeNames[type], size);
#endif

	if (diagLevel >= 2)
	{
		DiagLogF("  result = %x, message ID = %x, size = %d", result, msgID, size);
	}
	if (!result)
	{
		const PlayerIdentity *ident = FindIdentity(to);
		ErrF
		(
			"Message not sent - error %x, message ID = %x, to %d (%s)",
			result, msgID, to, ident ? (const char *)ident->name : "<no>"
		);
		return 0xFFFFFFFF;
	}
	return msgID;
}

void NetworkComponent::EncodeMsgItem
(
	NetworkMessageRaw &dst, NetworkMessage *msg, 
	const RefNetworkData &val,
	const NetworkMessageFormatItem &formatItem
)
{
	switch (formatItem.type)
	{
	case NDTBool:
		{
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped<bool>)
			dst.Put(valTyped.GetVal(), formatItem.compression);
		}
		break;
	case NDTInteger:
		{
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped<int>)
			dst.Put(valTyped.GetVal(), formatItem.compression);
		}
		break;
	case NDTFloat:
		{
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped<float>)
			dst.Put(valTyped.GetVal(), formatItem.compression);
		}
		break;
	case NDTString:
		{
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped<RString>)
			dst.Put(valTyped.GetVal(), formatItem.compression);
		}
		break;
	case NDTRawData:
		{
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped< AutoArray<char> >)
			AutoArray<char> &array = valTyped.GetVal();
			int m = array.Size();
			dst.Put(m, NCTSmallUnsigned);
			for (int j=0; j<m; j++)
			{
				dst.Put(array[j], formatItem.compression);
			}
		}
		break;
	case NDTTime:
		{
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped<Time>)
			dst.Put(valTyped.GetVal(), formatItem.compression);
		}
		break;
	case NDTVector:
		{
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped<Vector3>)
			dst.Put(valTyped.GetVal(), formatItem.compression);
		}
		break;
	case NDTMatrix:
		{
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped<Matrix3>)
			dst.Put(valTyped.GetVal(), formatItem.compression);
		}
		break;
	case NDTIntArray:
		{
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped< AutoArray<int> >)
			AutoArray<int> &array = valTyped.GetVal();
			int m = array.Size();
			dst.Put(m, NCTSmallUnsigned);
			for (int j=0; j<m; j++)
			{
				dst.Put(array[j], formatItem.compression);
			}
		}
		break;
	case NDTFloatArray:
		{
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped< AutoArray<float> >)
			AutoArray<float> &array = valTyped.GetVal();
			int m = array.Size();
			dst.Put(m, NCTSmallUnsigned);
			for (int j=0; j<m; j++)
			{
				dst.Put(array[j], formatItem.compression);
			}
		}
		break;
	case NDTStringArray:
		{
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped< AutoArray<RString> >)
			AutoArray<RString> &array = valTyped.GetVal();
			int m = array.Size();
			dst.Put(m, NCTSmallUnsigned);
			for (int j=0; j<m; j++)
			{
				dst.Put(array[j], formatItem.compression);
			}
		}
		break;
	case NDTSentence:
		{
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped<RadioSentence>)
			RadioSentence &array = valTyped.GetVal();
			int m = array.Size();
			dst.Put(m, NCTSmallUnsigned);
			for (int j=0; j<m; j++)
			{
				dst.Put(array[j].id, formatItem.compression);
				dst.Put(array[j].pauseAfter, formatItem.compression);
			}
		}
		break;
	case NDTObject:
		{
			// access to message
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped<NetworkMessage>)
			NetworkMessage &submsg = valTyped.GetVal();

			// message format
			const RefNetworkData &defVal = formatItem.defValue;
			CHECK_ASSIGN(defValTyped, defVal, const RefNetworkDataTyped<int>)
			NetworkMessageFormatBase *msgFormat = GetFormat((NetworkMessageType)defValTyped.GetVal());

			// propagate message header into submessage
			submsg.time = msg->time;
			EncodeMsg(dst, &submsg, msgFormat);
		}
		break;
	case NDTObjectArray:
		{
			// access to message array
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped< AutoArray<NetworkMessage> >)
			AutoArray<NetworkMessage> &array = valTyped.GetVal();

			// messages format
			const RefNetworkData &defVal = formatItem.defValue;
			CHECK_ASSIGN(defValTyped, defVal, const RefNetworkDataTyped<int>)
			NetworkMessageFormatBase *itemFormat = GetFormat((NetworkMessageType)defValTyped.GetVal());

			int m = array.Size();
			dst.Put(m, NCTSmallUnsigned);
			for (int j=0; j<m; j++)
			{
				// propagate message header into submessages
				array[j].time = msg->time;
				EncodeMsg(dst, &array[j], itemFormat);
			}
		}
		break;
	case NDTRef:
		{
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped<NetworkId>)
			dst.Put(valTyped.GetVal(), formatItem.compression);
		}
		break;
	case NDTRefArray:
		{
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped< AutoArray<NetworkId> >)
			AutoArray<NetworkId> &array = valTyped.GetVal();
			int m = array.Size();
			dst.Put(m, NCTSmallUnsigned);
			for (int j=0; j<m; j++)
			{
				dst.Put(array[j], formatItem.compression);
			}
		}
		break;
	case NDTData:
		{
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataWithFormat)
			NetworkDataType type = valTyped->format.type;
			dst.Put(type, NCTSmallUnsigned);
			dst.Put(valTyped->format.compression, NCTSmallUnsigned);
			if (type == NDTObject || type == NDTObjectArray)
			{
				NetworkMessageFormatItem format;
				format.type = NDTInteger;
				format.compression = NCTSmallUnsigned;
				// name and defValue are not used
				EncodeMsgItem(dst, msg, valTyped->format.defValue, format);
			}
			else
				EncodeMsgItem(dst, msg, valTyped->format.defValue, valTyped->format);
		}
		break;
	default:
		RptF("Bad data type %d", formatItem.type);
		break;
	}
}

void NetworkComponent::EncodeMsg(NetworkMessageRaw &dst, NetworkMessage *msg, NetworkMessageFormatBase *format)
{
	Assert(format);
	for (int i=0; i<format->NItems(); i++)
		EncodeMsgItem(dst, msg, msg->values[i], format->GetItem(i));
}

RefNetworkData NetworkComponent::DecodeMsgItem
(
	NetworkMessageRaw &src, NetworkMessage *msg,
	NetworkMessageFormatItem &formatItem
)
{
	switch (formatItem.type)
	{
	case NDTBool:
		{
			bool value;
			src.Get(value, formatItem.compression);
			return RefNetworkDataTyped<bool>(value);
		}
	case NDTInteger:
		{
			int value;
			src.Get(value, formatItem.compression);
			return RefNetworkDataTyped<int>(value);
		}
	case NDTFloat:
		{
			float value;
			src.Get(value, formatItem.compression);
			return RefNetworkDataTyped<float>(value);
		}
	case NDTString:
		{
			RString value;
			src.Get(value, formatItem.compression);
			return RefNetworkDataTyped<RString>(value);
		}
	case NDTRawData:
		{
			RefNetworkDataTyped< AutoArray<char> > value;
			//	RefNetworkDataTyped< AutoArray<char> >();
			AutoArray<char> &array = value.GetVal();
			int m;
			src.Get(m, NCTSmallUnsigned);
			array.Resize(m);
			for (int j=0; j<m; j++)
			{
				src.Get(array[j], formatItem.compression);
			}
			return value;
		}
	case NDTTime:
		{
			Time value;
			src.Get(value, formatItem.compression);
			return RefNetworkDataTyped<Time>(value);
		}
	case NDTVector:
		{
			// TODO: avoid copy
			Vector3 value;
			src.Get(value, formatItem.compression);
			return RefNetworkDataTyped<Vector3>(value);
		}
	case NDTMatrix:
		{
			// TODO: avoid copy
			Matrix3 value;
			src.Get(value, formatItem.compression);
			return RefNetworkDataTyped<Matrix3>(value);
		}
	case NDTIntArray:
		{
			RefNetworkDataTyped< AutoArray<int> > value;
			//	RefNetworkDataTyped< AutoArray<int> >();
			AutoArray<int> &array = value.GetVal();
			int m;
			src.Get(m, NCTSmallUnsigned);
			array.Resize(m);
			for (int j=0; j<m; j++)
			{
				src.Get(array[j], formatItem.compression);
			}
			return value;
		}
	case NDTFloatArray:
		{
			RefNetworkDataTyped< AutoArray<float> > value; 
			//	RefNetworkDataTyped< AutoArray<float> >();
			AutoArray<float> &array = value.GetVal();
			int m;
			src.Get(m, NCTSmallUnsigned);
			array.Resize(m);
			for (int j=0; j<m; j++)
			{
				src.Get(array[j], formatItem.compression);
			}
			return value;
		}
	case NDTStringArray:
		{
			RefNetworkDataTyped< AutoArray<RString> > value ;
			//	RefNetworkDataTyped< AutoArray<RString> >();
			AutoArray<RString> &array = value.GetVal();
			int m;
			src.Get(m, NCTSmallUnsigned);
			array.Resize(m);
			for (int j=0; j<m; j++)
			{
				src.Get(array[j], formatItem.compression);
			}
			return value;
		}
	case NDTSentence:
		{
			RefNetworkDataTyped<RadioSentence> value;
			//	RefNetworkDataTyped<RadioSentence>();
			RadioSentence &array = value.GetVal();
			int m;
			src.Get(m, NCTSmallUnsigned);
			array.Resize(m);
			for (int j=0; j<m; j++)
			{
				src.Get(array[j].id, formatItem.compression);
				src.Get(array[j].pauseAfter, formatItem.compression);
			}
			return value;
		}
	case NDTObject:
		{
			// access to message
			RefNetworkDataTyped<NetworkMessage> value; 
				//RefNetworkDataTyped<NetworkMessage>();
			NetworkMessage &submsg = value.GetVal();

			// message format
			const RefNetworkData &defVal = formatItem.defValue;
			CHECK_ASSIGN(defValTyped, defVal, const RefNetworkDataTyped<int>)
			NetworkMessageFormatBase *msgFormat = GetFormat((NetworkMessageType)defValTyped.GetVal());

			// propagate message header into submessage
			submsg.time = msg->time;
			DecodeMsg(&submsg, src, msgFormat);

			return value;
		}
	case NDTObjectArray:
		{
			// access to message array
			RefNetworkDataTyped< AutoArray<NetworkMessage> > value;
			///	RefNetworkDataTyped< AutoArray<NetworkMessage> >();
			AutoArray<NetworkMessage> &array = value.GetVal();

			// messages format
			const RefNetworkData &defVal = formatItem.defValue;
			CHECK_ASSIGN(defValTyped, defVal, const RefNetworkDataTyped<int>)
			NetworkMessageFormatBase *itemFormat = GetFormat((NetworkMessageType)defValTyped.GetVal());

			int m;
			src.Get(m, NCTSmallUnsigned);
			array.Resize(m);
			for (int j=0; j<m; j++)
			{
				// propagate message header into submessages
				array[j].time = msg->time;
				DecodeMsg(&array[j], src, itemFormat);
			}

			return value;
		}
	case NDTRef:
		{
			NetworkId value;
			src.Get(value, formatItem.compression);
			return RefNetworkDataTyped<NetworkId>(value);
		}
	case NDTRefArray:
		{
			RefNetworkDataTyped< AutoArray<NetworkId> > value;
			//	RefNetworkDataTyped< AutoArray<NetworkId> >();
			AutoArray<NetworkId> &array = value.GetVal();
			int m;
			src.Get(m, NCTSmallUnsigned);
			array.Resize(m);
			for (int j=0; j<m; j++)
			{
				src.Get(array[j], formatItem.compression);
			}
			return value;
		}
	case NDTData:
		{
			NetworkDataType type;
			NetworkCompressionType compression;
			src.Get((int &)type, NCTSmallUnsigned);
			src.Get((int &)compression, NCTSmallUnsigned);
			RefNetworkDataWithFormat value(type, compression, ND_NULL);
			if (type == NDTObject || type == NDTObjectArray)
			{
				NetworkMessageFormatItem format;
				format.type = NDTInteger;
				format.compression = NCTSmallUnsigned;
				// name and defValue are not used
				value->format.defValue = DecodeMsgItem(src, msg, format);
			}
			else
				value->format.defValue = DecodeMsgItem(src, msg, value->format);
			return value;
		}
	default:
		RptF("Bad data type %d", formatItem.type);
		return RefNetworkDataNull();
	}
}

void NetworkComponent::DecodeMsg(NetworkMessage *msg, NetworkMessageRaw &src, NetworkMessageFormatBase *format)
{
	Assert(format);
	int n = format->NItems();
	msg->values.Resize(n);
	for (int i=0; i<n; i++)
		msg->values[i] = DecodeMsgItem(src, msg, format->GetItem(i));
}

int NetworkComponent::CalculateMsgItemSize(NetworkMessage *msg, const RefNetworkData &val, const NetworkMessageFormatItem &formatItem)
{
	switch (formatItem.type)
	{
	case NDTInteger:
		{
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped<int>)
			int value = valTyped.GetVal();
			return CalculateValueSize(value, formatItem.compression);
		}
	case NDTBool:
	case NDTFloat:
	case NDTString:
	case NDTTime:
	case NDTVector:
	case NDTMatrix:
	case NDTRawData:
	case NDTIntArray:
	case NDTFloatArray:
	case NDTStringArray:
	case NDTSentence:
	case NDTRef:
	case NDTRefArray:
		return val.CalculateSize(formatItem.compression,formatItem);
	case NDTObject:
		{
			// access to message
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped<NetworkMessage>)
			NetworkMessage &submsg = valTyped.GetVal();

			// message format
			const RefNetworkData &defVal = formatItem.defValue;
			CHECK_ASSIGN(defValTyped, defVal, const RefNetworkDataTyped<int>)
			NetworkMessageFormatBase *msgFormat = GetFormat((NetworkMessageType)defValTyped.GetVal());

			return CalculateMsgSize(&submsg, msgFormat);
		}
	case NDTObjectArray:
		{
			// access to message array
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped< AutoArray<NetworkMessage> >)
			AutoArray<NetworkMessage> &array = valTyped.GetVal();

			// messages format
			const RefNetworkData &defVal = formatItem.defValue;
			CHECK_ASSIGN(defValTyped, defVal, const RefNetworkDataTyped<int>)
			NetworkMessageFormatBase *itemFormat = GetFormat((NetworkMessageType)defValTyped.GetVal());

			int size = 0;
			int m = array.Size();
			size += CalculateValueSize(m, NCTSmallUnsigned);
			for (int j=0; j<m; j++)
			{
				size += CalculateMsgSize(&array[j], itemFormat);
			}
			return size;
		}
	case NDTData:
		{
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataWithFormat)
			NetworkDataType type = valTyped->format.type;
			int size = 0;
			size += CalculateValueSize((int)type, NCTSmallUnsigned);
			size += CalculateValueSize((int)valTyped->format.compression, NCTSmallUnsigned);
			if (type == NDTObject || type == NDTObjectArray)
			{
				NetworkMessageFormatItem format;
				format.type = NDTInteger;
				format.compression = NCTSmallUnsigned;
				// name and defValue are not used
				size += CalculateMsgItemSize(msg, valTyped->format.defValue, format);
			}
			else
				size += CalculateMsgItemSize(msg, valTyped->format.defValue, valTyped->format);
			return size;
		}
	default:
		RptF("Bad data type %d", formatItem.type);
		return 0;
	}
}

int NetworkComponent::CalculateMsgSize(NetworkMessage *msg, NetworkMessageFormatBase *format)
{
	Assert(format);
	int size = 0;
	int n = format->NItems();
	for (int i=0; i<n; i++)
		size += CalculateMsgItemSize(msg, msg->values[i], format->GetItem(i));
	return size;
}

/*!
\patch_internal 1.15 Date 8/10/2001 by Ondra
- Improved: faster memory allocation during message transfer.
*/


#define NDT_DEFINE_ALLOC(type,name,description) DEFINE_FAST_ALLOCATOR(NetworkDataTyped< type >)

NETWORK_DATA_TYPES(NDT_DEFINE_ALLOC)

DEFINE_FAST_ALLOCATOR(NetworkDataTyped<AutoArray<RStringI> >)
DEFINE_FAST_ALLOCATOR(NetworkDataTyped<StaticArrayAuto<float> >)
