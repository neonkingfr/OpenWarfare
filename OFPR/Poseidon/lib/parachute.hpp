#ifdef _MSC_VER
#pragma once
#endif

#ifndef _PARACHUTE_HPP
#define _PARACHUTE_HPP

#include "transport.hpp"

#include "rtAnimation.hpp"

class ParachuteType: public TransportType
{
	typedef TransportType base;
	friend class Parachute;
	friend class ParachuteAuto;

	protected:
//	Vector3 _lightPos,_lightDir;
	
	Vector3 _pilotPos; // neutral neck and head positions

/*
	Vector3 _gunPos,_gunDir; // to do: gun position and direction
	Vector3 _gunAxis;
	float _neutralGunXRot;


	float _minGunTurn,_maxGunTurn; // gun movement
	float _minGunElev,_maxGunElev;
*/

	Ref<WeightInfo> _weights; // rt animation weighting info	
	Ref<Skeleton> _skeleton;

	Ref<AnimationRT> _open;
	//Ref<AnimationRT> _fly;
	Ref<AnimationRT> _drop;
	
	public:
	ParachuteType( const ParamEntry *param );
	virtual void Load(const ParamEntry &par);
	void InitShape();
	void DeinitShape();
};

class Parachute: public Transport
{
	typedef Transport base;

	protected:

	// pilot can directly coordinate these parameters
	float _backRotor,_backRotorWanted;

/*
	float _gunYRot,_gunYRotWanted;
	float _gunXRot,_gunXRotWanted;
	float _gunXSpeed,_gunYSpeed;
*/

	Vector3 _turbulence;
	Time _lastTurbulenceTime;

	float _openState;
	// 0..1 = closed (init)<1
	// 1..2  = opening
	// 2..3  = dropping

	public:
	Parachute( VehicleType *name, Person *pilot );
	~Parachute();
	
	const ParachuteType *Type() const
	{
		return static_cast<const ParachuteType *>(GetType());
	}

	bool IsAnimated( int level ) const; // appearence changed with Animate
	bool IsAnimatedShadow( int level ) const; // shadow changed with Animate
	void Animate( int level );
	void Deanimate( int level );

	void Draw( int level, ClipFlags clipFlags, const FrameBase &pos );

	//bool SelectTexture( int level, float rotorSpeed );
	//bool AnimateTexture( int level, bool shadow );
	//bool DeanimateTexture( int level, bool shadow );

	void GetActions(UIActions &actions, AIUnit *unit, bool now);
	bool IsAway(float factor);

	void Simulate( float deltaT, SimulationImportance prec );
	void Sound( bool inside, float deltaT );
	void UnloadSound();

	float GetEngineVol( float &freq ) const;
	float GetEnvironVol( float &freq ) const;

	float Rigid() const {return 0.1;} // how much energy is transfered in collision

	bool IsVirtual( CameraType camType ) const {return true;}
	//bool IsContinuous( CameraType camType ) const {return true;}
	bool HasFlares( CameraType camType ) const;

	Matrix4 InsideCamera( CameraType camType ) const;
	int InsideLOD( CameraType camType ) const;
	void InitVirtual( CameraType camType, float &heading, float &dive, float &fov ) const;
	void LimitVirtual( CameraType camType, float &heading, float &dive, float &fov ) const;
	float TrackingSpeed() const {return 100;}

	float GetCombatHeight() const {return 30;}
	float GetMinCombatHeight() const {return 30;}
	float GetMaxCombatHeight() const {return 100;}

	SimulationImportance WorstImportance() const {return SimulateVisibleFar;}
	SimulationImportance BestImportance() const {return SimulateCamera;}

	bool IsAbleToMove() const;
	bool IsPossibleToGetIn() const;
	bool Airborne() const;

	LSError Serialize(ParamArchive &ar);
};

class ParachuteAuto: public Parachute
{
	typedef Parachute base;

	protected:
	float _pilotHeading;
	float _dirCompensate;  // how much we compensate for estimated change
	// esp. when landing

	// helpers for keyboard pilot
	bool _pilotHelper; // keyboard helper activated
	bool _targetOutOfAim;

	// non-helper interface (joystrick)
	Vector3 _lastAngVelocity; // helper for prediction

	public:
	ParachuteAuto( VehicleType *name, Person *pilot );

	void Simulate( float deltaT, SimulationImportance prec );
	void AvoidGround( float minHeight );

	void Eject(AIUnit *unit);

	float MakeAirborne();

	bool FireWeapon( int weapon, TargetType *target );
	void FireWeaponEffects(int weapon, const Magazine *magazine,EntityAI *target);

	float FireValidTime() const {return 45;}

	void SuspendedPilot(AIUnit *unit, float deltaT );
	void KeyboardPilot(AIUnit *unit, float deltaT );
	void JoystickPilot( float deltaT );
	void FakePilot( float deltaT );

	// AI interface
	Matrix4 GunTransform() const;

	bool AimWeapon(int weapon, Vector3Par direction );
	bool AimWeapon(int weapon, Target *target );
	Vector3 GetWeaponDirection( int weapon ) const;
	float GetAimed( int weapon, Target *target ) const;

	float GetFieldCost( const GeographyInfo &info ) const;
	float GetCost( const GeographyInfo &info ) const;
	float GetCostTurn( int difDir ) const;

	float FireInRange( int weapon, float &timeToAim, const Target &target ) const;
	float FireAngleInRange( int weapon, Vector3Par rel ) const;

	void MoveWeapons( float deltaT );

#if _ENABLE_AI
	void AIGunner(AIUnit *unit, float deltaT );
	void AIPilot(AIUnit *unit, float deltaT );
#endif

	void DrawDiags();
	RString DiagText() const;

	LSError Serialize(ParamArchive &ar);

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx);
};

#endif

