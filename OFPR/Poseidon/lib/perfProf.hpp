#ifdef _MSC_VER
#pragma once
#endif

#ifndef _PERF_PROF_HPP
#define _PERF_PROF_HPP

#ifdef _WIN32
#if _ENABLE_PERFLOG
// exact time measurement (for profiling purposes)

#include <El/Common/perfLog.hpp>

// note: VC6 supports rdtsc directly

#define CPUID	__asm __emit 0fh __asm __emit 0a2h
#define RDTSC	__asm __emit 0fh __asm __emit 031h

#pragma warning(disable:4035)
inline __int64 ReadTsc()
{
	RDTSC
	// return value in edx:eax
}

#pragma warning(default:4035)

#define START_PROFILE(name) \
	__int64 Profile__##name=ReadTsc(); \
	DEF_COUNTER_P(name,10000)
#define END_PROFILE(name) \
	__int64 Diff_##name=ReadTsc()-Profile__##name; \
	int DiffI_##name=int(Diff_##name); \
	ADD_COUNTER_P(name,DiffI_##name)

class ScopeProfiler
{
	__int64 _start;
	PerfCounter &_counter;
	public:
	ScopeProfiler(PerfCounter &counter)
	:_counter(counter),_start(ReadTsc())
	{
	}
	int TimeSpent() const // how much time was spent from the profiler start
	{
		int add = int(ReadTsc()-_start);
		return add;
	}
	~ScopeProfiler()
	{
		int add = int(ReadTsc()-_start);
		_counter+=add;
		#if 0
			LogF("%s: profile %d",_counter.GetName(),add);
		#endif
	}
};

#define PROFILE_SCOPE(name) \
	DEF_COUNTER_P(name,10000); \
	ScopeProfiler scopeProf__(Counter_##name)

#define TIME_IN_SCOPE() scopeProf__.TimeSpent()

#else

#define START_PROFILE(name)
#define END_PROFILE(name)
#define PROFILE_SCOPE(name)
#define TIME_IN_SCOPE() 1

#endif

#else

#define START_PROFILE(name)
#define END_PROFILE(name)
#define PROFILE_SCOPE(name)
#define TIME_IN_SCOPE() 1

#endif

#endif
