#ifdef _MSC_VER
#pragma once
#endif

#ifndef _FSM_HPP
#define _FSM_HPP

// universal FSM core
#include <Es/Containers/staticArray.hpp>
//#include "loadSave.hpp"
#include "time.hpp"

class FSM: public SerializeClass
{
	public:
	typedef int State;
	enum {FinalState=-1};

	typedef void Context;

	typedef void StateFunction( Context *context );
	typedef StateFunction * pStateFunction;

	class StateInfo
	{
		protected:
		friend class FSM;
		friend class StateArray;
		const char *_name;
		StateFunction *_init;
		StateFunction *_check;
		
		StateInfo(){}
		StateInfo( const char *name, StateFunction *init, StateFunction *check )
		:_name(name),_init(init),_check(check)
		{}
		void Init( Context *context ) const {_init(context);}
		void Check( Context *context ) const {_check(context);}
		const char *GetName() const {return _name;}
		ClassIsSimpleZeroed(StateInfo);
	};

	enum {MaxVar=8};
	
	private:
	State _curState;
	//Ref<StateArray> _state;
	const StateInfo *_state;
	const pStateFunction *_special;	// special functions
																		// 0 - entry
																		// 1 - exit
	Time _timeOut;
	int _iVar[MaxVar];
	Time _tVar[MaxVar];

	int _nStates;
	int _nSpecials;

	const StateInfo &CurState() const
	{
		Assert( _curState>=0 && _curState<_nStates );
		return _state[_curState];
	}

	public:
	void SetState( State state, Context *context );
	void SetState( State state ) {_curState = state;}
	State GetState() const {return _curState;}
	const char *GetStateName() const {return CurState().GetName();}

	// variables
	int &Var( int i ) {return _iVar[i];}
	int Var( int i ) const {return _iVar[i];}

	Time &VarTime( int i ) {return _tVar[i];}
	Time VarTime( int i ) const {return _tVar[i];}

	void SetTimeOut( float sec );
	float GetTimeOut() const;
	bool TimedOut() const;

	FSM();	
	FSM
	(
		const StateInfo *states, int n,
		const pStateFunction *functions = NULL, int nFunc = 0
	); // typical contructor - static state space
	virtual ~FSM();

	void Init
	(
		const StateInfo *states, int n,
		const pStateFunction *functions, int nFunc
	);
	bool Update( Context *context ); // true -> final state reached
	void Refresh( Context *context ); // use only when FSM state is supposed to be lost
	void Enter( Context *context );
	void Exit( Context *context );

	LSError Serialize(ParamArchive &ar);
};

// type-safe implementation of FSM
template <class ContextType>
class FSMTyped: public FSM
{
	public:
	typedef void StateFunction( ContextType *context );
	typedef StateFunction * pStateFunction;
	class StateInfo: public FSM::StateInfo
	{
		public:
		StateInfo( const char *name, StateFunction *init, StateFunction *check )
		:FSM::StateInfo(name,(FSM::StateFunction *)init,(FSM::StateFunction *)check)
		{}
	};

	FSMTyped():FSM(){}
	FSMTyped
	(
		const StateInfo *states, int n,
		const pStateFunction *functions = NULL, int nFunc = 0
	);
	~FSMTyped(){}

	void SetState( State state, ContextType *context ) {FSM::SetState(state,context);}
	bool Update( ContextType *context ) {return FSM::Update(context);}
};

template <class ContextType>
FSMTyped<ContextType>::FSMTyped
(
	const StateInfo *states, int n,
	const pStateFunction *functions, int nFunc
)
:FSM(states,n,(const FSM::pStateFunction *)functions,nFunc)
{
}

#endif
