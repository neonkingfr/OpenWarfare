/*!
\file
Implementation for EntityAI class - general functions
*/

#include "wpch.hpp"

#include "dikCodes.h"
//#include <malloc.h>
//#include "vehicle.hpp"
//#include "vehicleAI.hpp"
#include "transport.hpp"
#include "ai.hpp"
#include "aiRadio.hpp"
#include "world.hpp"
#include "landscape.hpp"
#ifdef _WIN32
  #include "Joystick.hpp"
#endif
//#include "engine.hpp"
#include "shots.hpp"
#include "keyInput.hpp"
#include <El/Common/randomGen.hpp>
#include <Es/Strings/bstring.hpp>
#include "camera.hpp"
//#include "loadStream.hpp"
#include "visibility.hpp"
#include "roads.hpp"
#include "thing.hpp"
#include "pathSteer.hpp"
#include "operMap.hpp"
#include "txtPreload.hpp"
#include "frameInv.hpp"
#include "diagModes.hpp"

#include "SpecLods.hpp"
#include "engine.hpp"
#include "integrity.hpp"

#include "tlVertex.hpp"

#include <El/Common/perfLog.hpp>

#include "paramArchive.hpp"

#include "network.hpp"
#include "uiActions.hpp"

#include <Es/Algorithms/qsort.hpp>

#include "move_actions.hpp"

#include "detector.hpp"	// flag

#include <El/Common/enumNames.hpp>

#include "gameStateExt.hpp"

#include "mbcs.hpp"

static const Color MarkerRedColor(1.0, 0.1, 0.1);
static const Color MarkerRedAmbient(0.1, 0.01, 0.01);
static const float MarkerRedBrightness = 0.002;

static const Color MarkerGreenColor(0.1, 1.0, 0.1);
static const Color MarkerGreenAmbient(0.01, 0.1, 0.01);
static const float MarkerGreenBrightness = 0.002;

static const Color MarkerBlueColor(0.1, 0.1, 1.0);
static const Color MarkerBlueAmbient(0.01, 0.01, 0.1);
static const float MarkerBlueBrightness = 0.002;

static const Color MarkerWhiteColor(1.0, 1.0, 1.0);
static const Color MarkerWhiteAmbient(0.1, 0.1, 0.1);
static const float MarkerWhiteBrightness = 0.001;

static BankArray<RecoilFunction> RecoilFunctions;

const float LostUnitMin = 150.0f;
const float LostUnitMax = 500.0f;

#pragma warning(disable:4355)

void ReflectorInfo::Load(EntityAIType &type, const ParamEntry &cls, float armor)
{
	LODShape *shape = type.GetShape();
	color = GetColor(cls >> "color");
	colorAmbient = GetColor(cls >> "ambient");
	positionIndex = -1;
	directionIndex = -1;
	position = VZero;
	direction = VForward;
	Shape *memory = shape->MemoryLevel();
	if (memory)
	{
		RString name = cls >> "position";
		positionIndex = memory->PointIndex(name);
		if (positionIndex >= 0) position = memory->Pos(positionIndex);
		name = cls >> "direction";
		directionIndex = memory->PointIndex(name);
		if (directionIndex >= 0 && positionIndex >= 0)
		{
			direction = memory->Pos(directionIndex) - position;
			direction.Normalize();
		}
	}
	size = cls >> "size";
	brightness = cls >> "brightness";
	RString selName = cls >> "selection";
	selection.Init(shape, selName, NULL);
	RString hitName = cls >> "hitpoint";
	hitPoint = HitPoint(shape->HitpointsLevel(), hitName, NULL, armor, -1);
	hitPoint.SetIndex(type.GetHitPoints().Add(&hitPoint));
};

void PlateInfo::Init(Shape *shape, Offset face)
{
	_face = face;
	const Poly &f = shape->Face(face);
	
	_center = VZero;
	for (int v=0; v<f.N(); v++ )
	{
		int vertex = f.GetVertex(v);
		_center += shape->Pos(vertex);
	}
	float invFN = 1.0f/f.N();
	_center *=invFN;
	Vector3 normal = f.GetNormal(*shape); 
	_normal = normal.Normalized();
	// TODO: calculate actual size (based on max. distance of points)
	_size = sqrt(normal.Size())*0.4;
}

void PlateInfos::Init(LODShape *shape, RString name, FontID font, PackedColor color)
{
	for (int level=0; level<shape->NLevels(); level++)
	{
		Shape *sShape = shape->Level(level);
		int sel = sShape->FindNamedSel(name);
		if (sel>=0)
		{
			const FaceSelection &faces = sShape->NamedSel(sel).FaceOffsets(sShape);
			int n = faces.Size();
			_plates[level].Resize(n);
			for (int f=0; f<n; f++)
			{
				Offset off = faces[f];
				_plates[level][f].Init(sShape, off);
			}
		}
	}
	_font = GEngine->LoadFont(font);
	_color = color;
/*
	Ref<Font> font=GEngine->LoadFont("fonts\\tahomaB48");
	static PackedColor color(Color(0,0,0,0.75));
*/
}

void PlateInfos::Draw(int level, ClipFlags clipFlags, const FrameBase &pos, const char *text) const
{
	// find position to draw at (based on _plate selection)
	for (int plate=0; plate<_plates[level].Size(); plate++)
	{
		const PlateInfo &pi = _plates[level][plate];

		Vector3 center = pi._center;
		Vector3 normal = pi._normal;

		pos.PositionModelToWorld(center,center);
		pos.DirectionModelToWorld(normal,normal);

		Vector3 up = pos.DirectionUp();
		Vector3 aside = up.CrossProduct(normal).Normalized();
		up = normal.CrossProduct(aside).Normalized();

		/*
		GEngine->DrawLine3D(center,center+aside,PackedColor(Color(1,0,0)),0);
		GEngine->DrawLine3D(center,center+up,PackedColor(Color(0,1,0)),0);
		GEngine->DrawLine3D(center,center-normal,PackedColor(Color(0,0,1)),0);
		*/

		up *= pi._size;
		aside *= pi._size*0.8;
		center -= normal*0.01;

		GEngine->DrawText3D
		(
			center+up*0.5-aside*3,up,aside,clipFlags,_font,_color,0,text
		);
	}
}

EntityAIType::EntityAIType( const ParamEntry *param )
:EntityType(param)
{
	_scopeLevel=0;
}

EntityAIType::~EntityAIType()
{
	// make sure all vehicles of this type are already released
	Assert( _refVehicles==0 );
}

void ViewPars::Load( const ParamEntry &cfg )
{
	#define GET_PAR(x) _##x=cfg>>#x
	GET_PAR(initAngleY);GET_PAR(minAngleY);GET_PAR(maxAngleY);
	GET_PAR(initAngleX);GET_PAR(minAngleX);GET_PAR(maxAngleX);
	GET_PAR(initFov);GET_PAR(minFov);GET_PAR(maxFov);
	#undef GET_PAR
	saturateMax(_maxFov,_initFov); // force: fov is always in valid range
	saturateMin(_minFov,_initFov);
	
}

void ViewPars::InitVirtual
(
	CameraType camType, float &heading, float &dive, float &fov
) const
{
	fov = _initFov;
	dive = _initAngleX*(H_PI/180);
	heading = _initAngleY*(H_PI/180);
}

void ViewPars::LimitVirtual
(
	CameraType camType, float &heading, float &dive, float &fov
) const
{
	float minHeading = _minAngleY*(H_PI/180);
	float maxHeading = _maxAngleY*(H_PI/180);
	if( maxHeading-minHeading<H_PI*15/8 )
	{
		float initHeading = _initAngleY*(H_PI/180);
		minHeading = AngleDifference(minHeading,initHeading);
		maxHeading = AngleDifference(maxHeading,initHeading);
		heading = AngleDifference(heading,initHeading);

		saturate(heading,minHeading,maxHeading);

		heading += initHeading;
	}
	float minDive = _minAngleX*(H_PI/180);
	float maxDive = _maxAngleX*(H_PI/180);
	saturate(dive,minDive,maxDive);
	saturate(fov,_minFov,_maxFov);
}

void EntityAIType::Load(const ParamEntry &par)
{
	base::Load(par);

	#define GET_PAR(x) _##x=par>>#x

	GET_PAR(accuracy);
	GET_PAR(camouflage);
	GET_PAR(audible);
	GET_PAR(camouflage);
	GET_PAR(displayName);

	GET_PAR(weaponSlots);

	GET_PAR(spotableNightLightsOff);
	GET_PAR(spotableNightLightsOn);
		
	GET_PAR(visibleNightLightsOff);
	GET_PAR(visibleNightLightsOn);

	_nameSound=par>>"nameSound";
	_typicalSide=(TargetSide)(par>>"side").GetInt();
	_destrType=(DestructType)(par>>"destrType").GetInt();

	//Assert( _parClass );
	//RString model=par>>"model";
	RString picture=par>>"picture";
	// some parent vehicle classes have no model or picture defined
	//if( !picture || !picture[0] ) picture=model; 
	if( picture.GetLength()>0 )
	{
		_picture=GlobLoadTexture(GetPictureName(picture));
	}
	RString icon=par>>"icon";
	if( icon.GetLength()>0 )
	{
		_icon=GlobLoadTexture(GetPictureName(icon));
	}

	GET_PAR(cost);
	GET_PAR(fuelCapacity);
	GET_PAR(armor);

	_extCameraPosition.Init();
	_extCameraPosition[0]=(par>>"extCameraPosition")[0];
	_extCameraPosition[1]=(par>>"extCameraPosition")[1];
	_extCameraPosition[2]=(par>>"extCameraPosition")[2];

	GET_PAR(maxSpeed);
	GET_PAR(secondaryExplosion);
	GET_PAR(sensitivity);
	GET_PAR(sensitivityEar);

	GET_PAR(brakeDistance);
	GET_PAR(precision);
	GET_PAR(formationX);
	GET_PAR(formationZ);
	GET_PAR(formationTime);
	GET_PAR(steerAheadSimul);
	GET_PAR(steerAheadPlan);

	// get camera limits / initial values
	_viewPilot.Load(par>>"ViewPilot");

	// get control / simulation properties
	GET_PAR(predictTurnSimul);
	GET_PAR(predictTurnPlan);
	GET_PAR(minFireTime);

	if (_armor>1e-10)
	{
		_invArmor = 1/_armor;
		_logArmor = log(_armor);
	}
	else
	{
		_invArmor = 1e10;
		_logArmor = 25;
	}

	_invFormationTime = 1/_formationTime;
	_structuralDammageCoef=1/(float)(par>>"armorStructural");

	// cargo definition in config
	_maxFuelCargo=par>>"transportFuel";
	_maxRepairCargo=par>>"transportRepair";
	_maxAmmoCargo=par>>"transportAmmo";
	_maxWeaponsCargo=par>>"transportMaxWeapons";
	_maxMagazinesCargo=par>>"transportMaxMagazines";
	
	const ParamEntry &weaponCargo = par >> "TransportWeapons";
	int n = weaponCargo.GetEntryCount();
	_weaponCargo.Resize(n);
	for (int i=0; i<n; i++)
	{
		const ParamEntry &par = weaponCargo.GetEntry(i);
		RStringB weapon = par >> "weapon";
		_weaponCargo[i].weapon = WeaponTypes.New(weapon);
		_weaponCargo[i].count = par >> "count";
	}
	const ParamEntry &magazineCargo = par >> "TransportMagazines";
	n = magazineCargo.GetEntryCount();
	_magazineCargo.Resize(n);
	for (int i=0; i<n; i++)
	{
		const ParamEntry &par = magazineCargo.GetEntry(i);
		RStringB magazine = par >> "magazine";
		_magazineCargo[i].magazine = MagazineTypes.New(magazine);
		_magazineCargo[i].count = par >> "count";
	}
	
	_minCost=_maxSpeed>0 ? 3.6/_maxSpeed : 1e10;

	GET_PAR(irTarget);
	if (!par.FindEntry("irScanRange"))
	{
		GET_PAR(irScanToEyeFactor);
		GET_PAR(irScanRangeMin);
		GET_PAR(irScanRangeMax);
	}
	else
	{
		LogF("Old irScanRange used in %s",(const char *)GetName());
		// compatibility settings
		float irRange = par>>"irScanRange";
		_irScanRangeMin = irRange;
		_irScanRangeMax = irRange;
		_irScanToEyeFactor = 1;
	}

	GET_PAR(irScanGround);
	GET_PAR(nightVision);

	GET_PAR(laserScanner);
	GET_PAR(laserTarget);

	GET_PAR(commanderCanSee);
	GET_PAR(driverCanSee);
	GET_PAR(gunnerCanSee);


	GET_PAR(attendant);
	GET_PAR(preferRoads);

	_unitInfoType=(UnitInfoType)(par>>"unitInfoType").GetInt();
	GET_PAR(hideUnitInfo);
	
	GetValue(_mainSound, par>>"soundEngine");
	GetValue(_envSound, par>>"soundEnviron");
	GetValue(_dmgSound, par>>"soundDammage");
	GetValue(_crashSound, par>>"soundCrash");
	GetValue(_landCrashSound, par>>"soundLandCrash");
	GetValue(_waterCrashSound, par>>"soundWaterCrash");
	GetValue(_getInSound, par>>"soundGetIn");
	GetValue(_getOutSound, par>>"soundGetOut");
	GetValue(_servoSound, par>>"soundServo");

	const ParamEntry &extSounds = par >> "SoundEnvironExt";
	n = extSounds.GetEntryCount();
	_extEnvSounds.Realloc(n);
	_extEnvSounds.Resize(n);
	for (int i=0; i<n; i++)
	{
		const ParamEntry &par = extSounds.GetEntry(i);
		// par should be array of arrays
		_extEnvSounds[i].name = par.GetName();
		_extEnvSounds[i].pars.Realloc(par.GetSize());
		_extEnvSounds[i].pars.Resize(0);
		for (int j=0; j<par.GetSize(); j++)
		{
			int index = _extEnvSounds[i].pars.Add();
			GetValue(_extEnvSounds[i].pars[index], par[j]);
		}
		_extEnvSounds[i].pars.Compact();
	}
	_extEnvSounds.Compact();

	// load weapon info
	const ParamEntry &weapons = par >> "weapons";
	_weapons.Resize(0);
	_weapons.Realloc(weapons.GetSize());
	for (int i=0; i<weapons.GetSize(); i++)
	{
		AddWeapon(weapons[i].GetValue());
	}
	_weapons.Compact();
	_magazines.Resize(0);
	const ParamEntry &magazines = par >> "magazines";
	for (int i=0; i<magazines.GetSize(); i++)
	{
		AddMagazine(magazines[i].GetValue());
	}
	_magazines.Compact();

	// threat
	const ParamEntry &threat=par>>"threat";
	_threat[VSoft]=threat[0];
	_threat[VArmor]=threat[1];
	_threat[VAir]=threat[2];

	// cargo

	_kind=(VehicleKind)(par>>"type").GetInt();

	const ParamEntry *entry = par.FindEntry("forceSupply");
	if (entry) _forceSupply = *entry;
	else _forceSupply = false;

	entry = par.FindEntry("showWeaponCargo");
	if (entry) _showWeaponCargo = *entry;
	else _showWeaponCargo = false;

	_showDmgPoint = VZero;
	_shapeReversed=par>>"reversed";
	EntityType::_shapeReversed=_shapeReversed;
}

const SoundPars &EntityAIType::GetEnvSoundExt(RStringB name) const
{
	for (int i=0; i<_extEnvSounds.Size(); i++)
	{
		const ExtSoundInfo &info = _extEnvSounds[i];
		if (stricmp(info.name, name) == 0)
		{
			return info.pars[0];
		}
	}
	return _envSound;
}

const SoundPars &EntityAIType::GetEnvSoundExtRandom(RStringB name) const
{
	for (int i=0; i<_extEnvSounds.Size(); i++)
	{
		const ExtSoundInfo &info = _extEnvSounds[i];
		if (stricmp(info.name, name) == 0)
		{
			// select random
			int n = info.pars.Size();
			if (n<=1) return info.pars[0];
			float val = GRandGen.RandomValue();
			int i = toIntFloor(val*n);
			saturate(i,0,n-1);
			return info.pars[i];
		}
	}
	return _envSound;
}

bool EntityAI::CanLock(TargetType *type, int weapon) const
{
	// check current ammo

	if (weapon < 0) weapon = _currentWeapon;
	if (weapon < 0) return false;

	const WeaponModeType *mode = GetWeaponMode(weapon);
	if (!mode)
	{
		// FIX
		// if there is no mode, use some other way to check if it is LAW
		// check weapon typical magazine
		const MagazineSlot &slot = GetMagazineSlot(weapon);
		const MuzzleType *muzzle = slot._muzzle;
		if (!muzzle) return false;
		if (muzzle->_magazines.Size()<=0) return false;
		MagazineType *magazine = muzzle->_magazines[0];
		if (!magazine)
		{
			magazine = muzzle->_typicalMagazine;
		}
		if (!magazine) return false;
		// check first magazine mode
		if (magazine->_modes.Size()<=0) return false;
		mode = magazine->_modes[0];
	}
	if (!mode) return false;
	if (!mode->_ammo) return false;
	return type->LockPossible(mode->_ammo);
}

/*!
\patch 1.33 Date 11/28/2001 by Ondra.
- Fixed: Laser guided bomb lock was possible on IR targets.
*/

bool EntityAI::LockPossible( const AmmoType *ammo ) const
{
	const EntityAIType *type=GetType();
	// laser target can locked with laser capable weapon only
	if (type->GetLaserTarget())
	{
		return ammo->laserLock;
	}
	if (ammo->laserLock && !ammo->irLock)
	{
		if (!type->GetLaserTarget()) return false;
	}
	// disable locking of airborne targets
	if( Airborne() )
	{
		if( !ammo->airLock ) return false;
	}
	// some weapons can lock only ir targets
	if (!type->GetIRTarget() && ammo->irLock ) return false;
	return true;
}

void EntityAIType::AddHitPoints(const ParamEntry &par)
{
	// _shape is already valid
	// we want to load hitpoints
	// each hitpoint is defined by:
	// 
	for (int i=0; i<par.GetSize(); i++)
	{
	}

}

/*!
\patch 1.78 Date 7/16/2002 by Ondra
- Fixed: setObjectTexture often did not work when repeating mission.
*/

void EntityAIType::InitShape()
{
	EntityType::InitShape();
	// after shape is loaded
	_unitNumber.Init(_shape,"cislo",NULL);
	_groupSign.Init(_shape,"grupa",NULL);
	_sectorSign.Init(_shape,"sektor",NULL);
	_sideSign.Init(_shape,"side",NULL);
	_clan.Init(_shape,"clan",NULL);
	_dashboard.Init(_shape,"podsvit pristroju",NULL);
	_showDmg.Init(_shape,"poskozeni",NULL);

	_backLights.Init(_shape,"zadni svetlo",NULL);

	_squadTitles.Init(_shape, "clan_sign", GetFontID("tahomaB48"), PackedColor(Color(0,0,0,0.75)));

	int pilot = _shape->FindSpecLevel(VIEW_PILOT);
	if (pilot >= 0)
	{
		Shape *pilotLevel = _shape->LevelOpaque(pilot);
		Assert(pilotLevel);
		_showDmgPoint = pilotLevel->NamedPosition("poskozeni");
	}
	

	_lights.Clear();
	if( _shape->MemoryPointExists("doplnovani") )
	{
		Vector3Val d1=_shape->MemoryPoint("doplnovani");
		Vector3Val d2=_shape->MemoryPoint("doplnovani2");
		_supplyPoint=d1;
		_supplyRadius=d2.Distance(d1);

		// check if point is outside of the model
		// if not, issue warning
		if (_shape->IsInside(_supplyPoint))
		{
			LogF("%s: supply point inside the model.",(const char *)_shape->Name());
			// we should find some solution
			// quick patch is moving it to bounding sphere
			Vector3 supplyDir = _supplyPoint-_shape->GeometryCenter();
			float minDist = _shape->GeometrySphere();

			if (supplyDir.SquareSize()<Square(minDist))
			{
				supplyDir.Normalize();
				supplyDir *= minDist;
				_supplyPoint = _shape->GeometryCenter()+supplyDir;
				LogF("  supply point patched.");
			}
		}

	}
	else
	{
		_supplyPoint=VZero;
		_supplyRadius=(_shape->GeometrySphere()+1)*1.1;
	}
	Shape *memory = _shape->MemoryLevel();
	if (memory)
	{
		int index = memory->FindNamedSel("cerveny pozicni");
		if (index >= 0)
		{
			const NamedSelection &sel = memory->NamedSel(index);
			for (int i=0; i<sel.Size(); i++)
			{
				int pt = sel[i];
				const V3 &point = memory->Pos(pt);
				LightInfo &light = _lights.Set(_lights.Add());
				light.type = LightTypeMarker;
				light.position = point;
				light.direction = VUp;
				light.color = MarkerRedColor;
				light.ambient = MarkerRedAmbient;
				light.brightness = MarkerRedBrightness;
			}
		}

		index = memory->FindNamedSel("zeleny pozicni");
		if (index >= 0)
		{
			const NamedSelection &sel = memory->NamedSel(index);
			for (int i=0; i<sel.Size(); i++)
			{
				int pt = sel[i];
				const V3 &point = memory->Pos(pt);
				LightInfo &light = _lights.Set(_lights.Add());
				light.type = LightTypeMarker;
				light.position = point;
				light.direction = VUp;
				light.color = MarkerGreenColor;
				light.ambient = MarkerGreenAmbient;
				light.brightness = MarkerGreenBrightness;
			}
		}

		index = memory->FindNamedSel("bily pozicni blik");
		if (index >= 0)
		{
			const NamedSelection &sel = memory->NamedSel(index);
			for (int i=0; i<sel.Size(); i++)
			{
				int pt = sel[i];
				const V3 &point = memory->Pos(pt);
				LightInfo &light = _lights.Set(_lights.Add());
				light.type = LightTypeMarkerBlink;
				light.position = point;
				light.direction = VUp;
				light.color = MarkerWhiteColor;
				light.ambient = MarkerWhiteAmbient;
				light.brightness = MarkerWhiteBrightness;
			}
		}

		index = memory->FindNamedSel("cerveny pozicni blik");
		if (index >= 0)
		{
			const NamedSelection &sel = memory->NamedSel(index);
			for (int i=0; i<sel.Size(); i++)
			{
				int pt = sel[i];
				const V3 &point = memory->Pos(pt);
				LightInfo &light = _lights.Set(_lights.Add());
				light.type = LightTypeMarkerBlink;
				light.position = point;
				light.direction = VUp;
				light.color = MarkerRedColor;
				light.ambient = MarkerRedAmbient;
				light.brightness = MarkerRedBrightness;
			}
		}

		index = memory->FindNamedSel("zeleny pozicni blik");
		if (index >= 0)
		{
			const NamedSelection &sel = memory->NamedSel(index);
			for (int i=0; i<sel.Size(); i++)
			{
				int pt = sel[i];
				const V3 &point = memory->Pos(pt);
				LightInfo &light = _lights.Set(_lights.Add());
				light.type = LightTypeMarkerBlink;
				light.position = point;
				light.direction = VUp;
				light.color = MarkerGreenColor;
				light.ambient = MarkerGreenAmbient;
				light.brightness = MarkerGreenBrightness;
			}
		}

		index = memory->FindNamedSel("modry pozicni blik");
		if (index >= 0)
		{
			const NamedSelection &sel = memory->NamedSel(index);
			for (int i=0; i<sel.Size(); i++)
			{
				int pt = sel[i];
				const V3 &point = memory->Pos(pt);
				LightInfo &light = _lights.Set(_lights.Add());
				light.type = LightTypeMarkerBlink;
				light.position = point;
				light.direction = VUp;
				light.color = MarkerBlueColor;
				light.ambient = MarkerBlueAmbient;
				light.brightness = MarkerBlueBrightness;
			}
		}
	}
	_hitPoints.Clear();
	// must be first of InitShape chain

	// reflectors
	const ParamEntry &par = *_par;
	float armor = GetArmor() * (float)(par >> "armorLights");
	const ParamEntry &reflectors = par >> "Reflectors";
	int n = reflectors.GetEntryCount();
	_reflectors.Resize(n);
	for (int i=0; i<n; i++)
	{
		_reflectors[i].Load(*this, reflectors.GetEntry(i), armor);
	}

	// hidden selections
	const ParamEntry &hiddenSelections = par >> "hiddenSelections";
	_hiddenSelections.Clear();
	_hiddenSelections.Realloc(hiddenSelections.GetSize());
	for (int i=0; i<hiddenSelections.GetSize(); i++)
	{
		RString name = hiddenSelections[i];
		int index = _hiddenSelections.Add();
		_hiddenSelections[index].Init(_shape, name, NULL);
	}

	// reload animations
	const ParamEntry *array = par.FindEntry("ReloadAnimations");
	if (array)
	{
		int n = array->GetEntryCount();
		_reloadAnimations.Realloc(n);
		_reloadAnimations.Resize(n);
		for (int i=0; i<n; i++)
		{
			const ParamEntry &entry = array->GetEntry(i);
			RStringB weapon = entry >> "weapon";
			_reloadAnimations[i].weapon = WeaponTypes.New(weapon);
			_reloadAnimations[i].animation = AnimationType::CreateObject(entry, _shape);
			_reloadAnimations[i].multiplier = entry >> "multiplier";
		}
	}
	array = par.FindEntry("EventHandlers");
	if (array)
	{
		for (int i=0; i<NEntityEvent; i++)
		{
			EntityEvent e = (EntityEvent)i;
			RString name = FindEnumName(e);
			const ParamEntry *entry = array->FindEntry(name);
			if (entry)
			{
				RStringB expression = *entry;
				_eventHandlers[e] = expression;
			}
		}
	}

	// user type actions
	array = par.FindEntry("UserActions");
	if (array)
	{
		int n = array->GetEntryCount();
		_userTypeActions.Realloc(n);
		_userTypeActions.Resize(n);
		for (int i=0; i<n; i++)
		{
			const ParamEntry &entry = array->GetEntry(i);
			_userTypeActions[i].displayName = entry >> "displayName";
			RString pos = entry >> "position";
			_userTypeActions[i].modelPosition = _shape->MemoryPoint(pos);
			_userTypeActions[i].radius = entry >> "radius";
			_userTypeActions[i].condition = entry >> "condition";
			_userTypeActions[i].statement = entry >> "statement";
		}
	}

	/*
	// force loading all default weapons
	for (int i=0; i<NWeaponSystems(); i++)
	{
		const WeaponType *type = GetWeaponSystem(i);
		type->ShapeAddRef();
	}
	*/
}

void EntityAIType::DeinitShape()
{
	_hiddenSelections.Clear();
	_reloadAnimations.Clear();

	/*
	// unload all default weapons
	for (int i=0; i<NWeaponSystems(); i++)
	{
		const WeaponType *type = GetWeaponSystem(i);
		type->ShapeRelease();
	}
	*/
	EntityType::DeinitShape();
}

/*!
\patch 1.50 Date 4/15/2002 by Ondra
- Fixed: When setViewDistance is used to increase visible range,
tanks did not adjust their IR detector range accordingly.
*/

float EntityAIType::GetIRScanRange() const
{
	//float eyeRange = TACTICAL_VISIBILITY;
	//float eyeRange = Glob.config.tacticalZ;
	float eyeRange = Glob.config.tacticalZ;
	float irRange = eyeRange*_irScanToEyeFactor;
	saturate(irRange,_irScanRangeMin,_irScanRangeMax);
	return irRange;
}

LSError Serialize(ParamArchive &ar, RString name, const EntityType * &value, int minVersion)
{
	if (ar.GetArVersion() < minVersion) return LSOK;
	if (ar.IsSaving())
	{
		RString str = value ? value->GetName() : "";
		CHECK(ar.Serialize(name, str, minVersion));
	}
	else
	{
		if (ar.GetPass() != ParamArchive::PassFirst) return LSOK;
		RString str;
		CHECK(ar.Serialize(name, str, minVersion));
		if (str.GetLength() > 0)
			value = VehicleTypes.New(str);
		else
			value = NULL;
	}
	return LSOK;
}

LSError Serialize(ParamArchive &ar, RString name, const EntityType * &value, int minVersion, const EntityType *defValue)
{
	if (ar.GetArVersion() < minVersion)
	{
		if (!ar.IsSaving()) value = defValue;
		return LSOK;
	}
	if (ar.IsSaving())
	{
		RString str = value ? value->GetName() : "";
		CHECK(ar.Serialize(name, str, minVersion));
	}
	else
	{
		if (ar.GetPass() != ParamArchive::PassFirst) return LSOK;
		RString str;
		CHECK(ar.Serialize(name, str, minVersion));
		if (str.GetLength() > 0)
			value = VehicleTypes.New(str);
		else
			value = NULL;
	}
	return LSOK;
}

LSError Serialize(ParamArchive &ar, RString name, const EntityAIType * &value, int minVersion)
{
	const EntityType *tValue = value;
	LSError ok = Serialize(ar,name,tValue,minVersion);
	value = static_cast<const EntityAIType *>(tValue);
	return ok;
}

LSError Serialize(ParamArchive &ar, RString name, const EntityAIType * &value, int minVersion, const EntityAIType *defValue)
{
	const EntityType *tValue = value;
	LSError ok = Serialize(ar,name,tValue,minVersion,defValue);
	value = static_cast<const EntityAIType *>(tValue);
	return ok;
}

VehicleTypeBank VehicleTypes;


DEFINE_CASTING(EntityAI)

/*!
\param type Entity type
\param fullCreate	When fullCreate is false, entity will be serialized
	 or transfered over network and many thing need not be initalized
	 (e.g. weapon list).
*/

EntityAI::EntityAI(EntityAIType *type, bool fullCreate)
:Entity(type->_shape,type,-1),

_lockedBeg(VZero),
_lockedSoldier(false),
_stratGoToPos(VZero),

_avoidSpeed(1e5),
_avoidSpeedTime(TIME_MIN),

_avoidAside(0),_avoidAsideWanted(0), // obstacle avoidance offset
_lastSimplePath(TIME_MIN),

_isDead(false),_isStopped(false),_isUpsideDown(false),
_userStopped(false), _pilotLight(false),

_showFlag(true),
_laserTargetOn(false),

_inFormation(true),

_shootVisible(0),_shootAudible(0),_shootTimeRest(1e10),
#if _ENABLE_CHEATS
_allowDammage(true),
#endif

_locked(false),_tempLocked(false),
_locker(new AILocker()),

// when we scanned for visible targets
_trackTargetsTime(TIME_MIN),
_trackNearTargetsTime(TIME_MIN),
_newTargetsTime(TIME_MIN),
_nearestEnemy(1e10), // nearest known enemy
_sensorColID(-1),

_lastDammageTime(Glob.time - 120),
_lastShotTime(Glob.time-120),
_limitSpeed(GetType()->GetMaxSpeedMs()*2),

_lastWeaponNotReady(Glob.time),_lastWeaponReady(Glob.time-10),

_currentWeapon(-1),
_forceFireWeapon(-1),
_rearmCredit(0), // used during rearm

// attack position search
_attackEngageTime(Glob.time-60),
_attackRefreshTime(Glob.time-60),

_attackAggresivePos(VZero),
_attackEconomicalPos(VZero),

// goto automat
//_goToState(GoToReported),
_fireState(FireDone),
_fireStateDelay(Glob.time-60),

_aimObserverAsked(VZero),

_nextUserActionId(0)

// fireat/goto automat
//_moveMode(gotoWait),
{
	Assert(type == GetType())

	_hit.Realloc(type->_hitPoints.Size());
	_hit.Resize(type->_hitPoints.Size());
	for( int i=0; i<type->_hitPoints.Size(); i++ ) _hit[i]=0;

	#if 0
	{
		Shape *hitShape=_shape->HitpointsLevel();
		if( hitShape )
		{
			LogF("Hits %s",(const char *)_shape->Name());
			for( int i=0; i<type->_hitPoints.Size(); i++ )
			{
				const HitPoint &hit=*type->_hitPoints[i];
				int index=hit.GetSelection();
				if( index>=0 )
				{
					const NamedSelection &sel=hitShape->NamedSel(index);
					LogF("  %s",(const char *)sel.Name());
				}
			}
		}
	}
	#endif

	Assert( type );
	Assert( !type->IsAbstract() );
	Assert( _shape==type->_shape );

	if (fullCreate)
	{
		// get default weapon information
		int n = type->NMagazines();
		_magazines.Resize(n);
		for (int i=0; i<n; i++)
		{
			const MagazineType *mType = type->GetMagazine(i);
			_magazines[i] = new Magazine(mType);
			_magazines[i]->_ammo = mType->_maxAmmo;
			_magazines[i]->_reload = 0;
			if (mType->_modes.Size() > 0)
			{
				float	reload = 0.8 + 0.2 * GRandGen.RandomValue();
				_magazines[i]->_reload = reload * mType->_modes[0]->_reloadTime;
			}
			_magazines[i]->_reloadMagazine = 0;
		}

		for (int i=0; i<type->NWeaponSystems(); i++)
			AddWeapon(const_cast<WeaponType *>(type->GetWeaponSystem(i)));
	
		AutoReloadAll();
	}

	_targetSide=type->_typicalSide;
	_lastMovement=Glob.time;

	// create reflectors
	int n = type->_reflectors.Size();
	_reflectors.Resize(n);
	LODShapeWithShadow *shape = GLOB_SCENE->Preloaded(CobraLight);
	for (int i=0; i<n; i++)
	{
		const ReflectorInfo &info = type->_reflectors[i];
		LightReflectorOnVehicle *light = new LightReflectorOnVehicle
		(
			shape, info.color, info.colorAmbient,
			this, info.position, info.direction,
			info.size
		);
		light->SetBrightness(info.brightness);
		GScene->AddLight(light);
		_reflectors[i] = light;
	}
	
	_hiddenSelectionsTextures.Realloc(type->_hiddenSelections.Size());
	_hiddenSelectionsTextures.Resize(type->_hiddenSelections.Size());
}

EntityAI::~EntityAI()
{
	// if there are any weapons left, release their shapes
	for (int i=0; i<_weapons.Size(); i++)
	{
		WeaponType *weapon = _weapons[i];
		weapon->ShapeRelease();
	}
	_weapons.Clear();
	
	PerformUnlock();
}

void EntityAI::SetObjectTexture(int index, Texture *texture)
{
	if (index >= 0 && index < _hiddenSelectionsTextures.Size())
		_hiddenSelectionsTextures[index] = texture;
}

bool EntityAI::FindWeapon(const WeaponType *weapon) const
{
	for (int i=0; i<_weapons.Size(); i++)
		if (_weapons[i] == weapon) return true;
	return false;
}

bool EntityAI::EmptySlot(const MagazineSlot &slot) const
{
	const Magazine *magazine = slot._magazine;
	if (!magazine) return true;
	if (magazine->_ammo > 0) return false;
  const MagazineType *type = magazine->_type;
  if (!type || slot._mode < 0 || slot._mode >= type->_modes.Size()) return true;
  if (!magazine->_type->_modes[slot._mode]->_ammo) return false;
	for (int i=0; i<NMagazines(); i++)
	{
		const Magazine *reserve = GetMagazine(i);
		if (!reserve || reserve == magazine) continue;
		if (reserve->_type != magazine->_type) continue;
		if (reserve->_ammo > 0) return false;
	}
	return true;
}


int EntityAI::FirstWeapon() const
{
	int maxPrimary = MaxPrimaryLevel();
	// check max. primary level available
	// try to find primary weapon with max level
	for (int i=0; i<NMagazineSlots(); i++)
	{
		const MagazineSlot &slot = GetMagazineSlot(i);
		if (slot._muzzle->_primary<maxPrimary) continue;
		if (!slot._muzzle->_showEmpty && EmptySlot(slot)) continue;
		if (IsHandGunSelected())
		{
			if (slot._weapon->_weaponType & MaskSlotHandGun) return i;
		}
		else
		{
			if (slot._weapon->_weaponType & MaskSlotPrimary) return i;
		}
	}
	// find any weapon with max level
	for (int i=0; i<NMagazineSlots(); i++)
	{
		const MagazineSlot &slot = GetMagazineSlot(i);
		if (slot._muzzle->_primary<maxPrimary) continue;
		if (!slot._muzzle->_showEmpty && EmptySlot(slot)) continue;
		if (IsHandGunSelected())
		{
			// ignore primary weapon
			if (slot._weapon->_weaponType & MaskSlotPrimary) continue;
		}
		else
		{
			// ignore handgun
			if (slot._weapon->_weaponType & MaskSlotHandGun) continue;
		}
		return i;
	}
	return -1;
}



int EntityAI::MaxPrimaryLevel() const
{
	int maxPrimary = 0;
	for (int i=0; i<NMagazineSlots(); i++)
	{
		const MagazineSlot &slot = GetMagazineSlot(i);
		if (!slot._muzzle->_showEmpty && EmptySlot(slot)) continue;
		if (!slot._magazine) continue;
		if (slot._magazine->_ammo<=0) continue;
		saturateMax(maxPrimary,slot._muzzle->_primary);
	}
	if (maxPrimary>0) return maxPrimary;
	// check max. primary level of all weapons
	for (int i=0; i<NMagazineSlots(); i++)
	{
		const MagazineSlot &slot = GetMagazineSlot(i);
		if (!slot._muzzle->_showEmpty && EmptySlot(slot)) continue;
		saturateMax(maxPrimary,slot._muzzle->_primary);
	}
	return maxPrimary;
}

int EntityAI::NextWeapon(int weapon) const
{
	// check max. primary level
	int maxPrimary = MaxPrimaryLevel();

	if (weapon >= 0)
	{
		const MagazineSlot &slot = GetMagazineSlot(weapon);
		if (slot._muzzle->_primary<maxPrimary) return FirstWeapon();
	}
	
	int n = NMagazineSlots();

	for (int i=0; i<n; i++)
	{
		weapon++;
		if (weapon > n - 1) weapon = 0;
		const MagazineSlot &slot = GetMagazineSlot(weapon);
		if (slot._muzzle->_primary<maxPrimary) continue;
		if (!slot._muzzle->_showEmpty && EmptySlot(slot)) continue;
		if (IsHandGunSelected())
		{
			// ignore primary weapon
			if (slot._weapon->_weaponType & MaskSlotPrimary) continue;
		}
		else
		{
			// ignore handgun
			if (slot._weapon->_weaponType & MaskSlotHandGun) continue;
		}
		return weapon;
	}
	return -1;
}

int EntityAI::PrevWeapon(int weapon) const
{
	int maxPrimary = MaxPrimaryLevel();
	if (weapon >= 0)
	{
		const MagazineSlot &slot = GetMagazineSlot(weapon);
		if (slot._muzzle->_primary<maxPrimary) return FirstWeapon();
	}
	
	int n = NMagazineSlots();

	for (int i=0; i<n; i++)
	{
		weapon--;
		if (weapon < 0) weapon = n - 1;
		const MagazineSlot &slot = GetMagazineSlot(weapon);
		if (slot._muzzle->_primary<maxPrimary) continue;
		if (!slot._muzzle->_showEmpty && EmptySlot(slot)) continue;
		if (IsHandGunSelected())
		{
			// ignore primary weapon
			if (slot._weapon->_weaponType & MaskSlotPrimary) continue;
		}
		else
		{
			// ignore handgun
			if (slot._weapon->_weaponType & MaskSlotHandGun) continue;
		}
		return weapon;
	}
	return -1;
}

int EntityAI::FindWeaponType(int maskInclude, int maskExclude) const
{
	for ( int i=0; i<NWeaponSystems(); i++ )
	{
		const WeaponType *weapon = GetWeaponSystem(i);
		if 
		(
			(weapon->_weaponType & maskInclude) != 0 &&
			(weapon->_weaponType & maskExclude) == 0
		) return i;
	}
	return -1;
}

/*!
\param magazine checked magazine
\return true if magazine found, false otherwise
*/
bool EntityAI::FindMagazine(const Magazine *magazine) const
{
	for (int i=0; i<_magazines.Size(); i++)
		if (_magazines[i] == magazine) return true;
	return false;
}

/*!
\param weapon checked type of weapon
\return true, if weapon can be added - in this case conflict contains weapons must be removed
*/
bool EntityAI::CheckWeapon
(
	const WeaponType *weapon,
	AutoArray<Ref<const WeaponType>, MemAllocSA> &conflict
) const
{
	if (FindWeapon(weapon)) return false;

	int slots = weapon->_weaponType;
	if (slots == 0) return true;

	int maxSlots = GetType()->_weaponSlots;

	DoAssert((slots & MaskSlotItem) == 0);
	const int primaryMax = maxSlots & MaskSlotPrimary;
	const int secondaryMax = maxSlots & MaskSlotSecondary;
	const int binocularMax = maxSlots & MaskSlotBinocular;
	const int handgunMax = maxSlots & MaskSlotHandGun;
	int primary = slots & MaskSlotPrimary;
	int secondary = slots & MaskSlotSecondary;
	int binocular = slots & MaskSlotBinocular;
	int handgun = slots & MaskSlotHandGun;
	if (primary > primaryMax) return false;
	if (secondary > secondaryMax) return false;
	if (binocular > binocularMax) return false;
	if (handgun > handgunMax) return false;

	// check weapons
	for (int i=0; i<_weapons.Size(); i++)
	{
		// check duplicity
		const WeaponType *weaponI = _weapons[i];
		// check slots
		int slotsI = weaponI->_weaponType;
		int primaryI = slotsI & MaskSlotPrimary;
		int secondaryI = slotsI & MaskSlotSecondary;
		int binocularI = slotsI & MaskSlotBinocular;
		int handgunI = slotsI & MaskSlotHandGun;

		if (primary + primaryI > primaryMax) conflict.Add(weaponI);
		else primary += primaryI;
		if (secondary + secondaryI > secondaryMax) conflict.Add(weaponI);
		else secondary += secondaryI;
		if (binocular + binocularI > binocularMax) conflict.Add(weaponI);
		else binocular += binocularI;
		if (handgun + handgunI > handgunMax) conflict.Add(weaponI);
		else handgun += handgunI;
	}
	return true;
}

int CmpMagazines(const Ref<const Magazine> *pm1, const Ref<const Magazine> *pm2, const EntityAI *veh)
{
	const Magazine *m1 = *pm1;
	const Magazine *m2 = *pm2;

	// first remove empty magazines
	bool empty1 = m1->_ammo == 0;
	bool empty2 = m2->_ammo == 0;
	if (empty1 && !empty2) return -1;
	if (empty2 && !empty1) return 1;

	// then unused magazines
	bool used1 = false;
	bool used2 = false;
	for (int i=0; i<veh->NWeaponSystems(); i++)
	{
		const WeaponType *weapon = veh->GetWeaponSystem(i);
		for (int j=0; j<weapon->_muzzles.Size(); j++)
		{
			const MuzzleType *muzzle = weapon->_muzzles[j];
			for (int k=0; k<muzzle->_magazines.Size(); k++)
			{
				const MagazineType *type = muzzle->_magazines[k];
				if (type == m1->_type) used1 = true;
				if (type == m2->_type) used2 = true;
			}
		}
	}
	if (used1 && !used2) return 1;
	if (used2 && !used1) return -1;

	// then less full
	float full1 = m1->_type->_maxAmmo > 0 ? m1->_ammo / m1->_type->_maxAmmo : 1;
	float full2 = m2->_type->_maxAmmo > 0 ? m2->_ammo / m2->_type->_maxAmmo : 1;
	return sign(full1 - full2);
}

/*!
\param magazine checked magazine
\return true, if magazine can be added - in this case conflict contains magazines must be removed
*/
bool EntityAI::CheckMagazine
(
	const Magazine *magazine,
	AutoArray<Ref<const Magazine>, MemAllocSA> &conflict
) const
{
	int slots = magazine->_type->_magazineType;
	if (slots == 0) return true;

	int maxSlots = GetType()->_weaponSlots;

	DoAssert((slots & (MaskSlotItem | MaskSlotHandGunItem)) == slots);
	const int itemMax = maxSlots & MaskSlotItem;
	const int itemMaxHandGun = maxSlots & MaskSlotHandGunItem;
	int item = slots & MaskSlotItem;
	int itemHandGun = slots & MaskSlotHandGunItem;
	int reserve = itemMax - item;
	int reserveHandGun = itemMaxHandGun - itemHandGun;
	if (reserve <= 0 && reserveHandGun <= 0) return false;

	// check magazines
	for (int i=0; i<_magazines.Size(); i++)
	{
		const Magazine *magazineI = _magazines[i];
		// check slots
		int itemI = magazineI->_type->_magazineType & MaskSlotItem;
		int itemIHandGun = magazineI->_type->_magazineType & MaskSlotHandGunItem;
		if (itemI > 0 || itemIHandGun > 0)
		{
			reserve -= itemI;
			reserveHandGun -= itemIHandGun;
			// check if not used
			if (IsMagazineUsed(magazineI)) continue;
			// do not change magazines of the same type (except empty)
			if
			(
				magazineI->_type != magazine->_type ||
				magazineI->_ammo == 0
			) conflict.Add(magazineI);	// candidate for remove
		}
	}

	if (reserve >= 0 && reserveHandGun >= 0)
	{
		conflict.Clear();
		return true;
	}
	
	QSort(conflict.Data(), conflict.Size(), this, CmpMagazines);
	for (int i=0; i<conflict.Size();)
	{
		const Magazine *magazineI = conflict[i];
		int itemI = magazineI->_type->_magazineType & MaskSlotItem;
		int itemIHandGun = magazineI->_type->_magazineType & MaskSlotHandGunItem;
		reserve += itemI;
		reserveHandGun += itemIHandGun;
		if (itemI != 0)
		{
			DoAssert((itemI & MaskSlotItem) == itemI);
			if (reserve >= itemI) conflict.Delete(i);
			else i++;
		}
		else if (itemIHandGun != 0)
		{
			DoAssert((itemIHandGun & MaskSlotHandGunItem) == itemIHandGun);
			if (reserveHandGun >= itemIHandGun) conflict.Delete(i);
			else i++;
		}
		else
		{
			Fail("Unexpected magazine type");
			conflict.Delete(i);
		}
	}
	if (reserve >= 0 && reserveHandGun >= 0) return true;
	conflict.Clear();
	return false;
}

/*!
\param magazine checked magazine taype
\return true, if magazine fits (can be loaded) in some weapon
*/
bool EntityAI::IsMagazineUsable(const MagazineType *magazine) const
{
	for (int i=0; i<_weapons.Size(); i++)
	{
		const WeaponType *weapon = _weapons[i];
		for (int j=0; j<weapon->_muzzles.Size(); j++)
		{
			const MuzzleType *muzzle = weapon->_muzzles[j];
			for (int k=0; k<muzzle->_magazines.Size(); k++)
				if (muzzle->_magazines[k] == magazine) return true;
		}
	}
	return false;
}

bool CheckAccess(const ParamEntry &entry);
bool CheckAccessCreate(const ParamEntry &entry);


/*!
\param weapon weapon type
\param force add weapon even if no free slots (bypass check)
\param reload try to reload weapon immediatelly
\return index of new weapon in array
*/
int EntityAI::AddWeapon(WeaponType *weapon, bool force, bool reload, bool checkSelected)
{
	if (!CheckAccessCreate(*weapon->_parClass))
	{
		return -1;
	}
	// add weapon
	if (!force)
	{
		AUTO_STATIC_ARRAY(Ref<const WeaponType>, conflict, 16);
		if (!CheckWeapon(weapon, conflict)) return -1;
		if (conflict.Size() > 0) return -1;
	}
	weapon->ShapeAddRef();
	int index = _weapons.Add(weapon);

	// add magazine slots
	for (int j=0; j<weapon->_muzzles.Size(); j++)
	{
		MuzzleType *muzzle = weapon->_muzzles[j];

		Magazine *magazine = NULL;
		if (reload)
		{
			int index = FindMagazineByType(muzzle);
			if (index >= 0) magazine = GetMagazine(index);
		}
		for (int mode=0; mode<muzzle->_nModes; mode++)
		{
			int slot = _magazineSlots.Add();
			_magazineSlots[slot]._weapon = weapon;
			_magazineSlots[slot]._muzzle = muzzle;
			_magazineSlots[slot]._mode = mode;
			_magazineSlots[slot]._magazine = magazine;
		}
	}
	if (checkSelected) OnWeaponAdded();
	return index;
}

/*!
\patch_internal 1.01 Date 6/26/2001 by Ondra. Added to optimize NVG search.
*/
void EntityAI::OnWeaponAdded()
{
}

/*!
\patch_internal 1.01 Date 6/26/2001 by Ondra. Added to optimize NVG search.
*/
void EntityAI::OnWeaponRemoved()
{
}

/*!
\patch_internal 1.63 Date 6/3/2002 by Jirka
- Fixed: Better handgun manipulation
*/
void EntityAI::OnWeaponChanged()
{
	if (_weapons.Size() > 0)
		SelectWeapon(FirstWeapon(), true);
	else
		SelectWeapon(-1, true);
	if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == this)
		GWorld->UI()->ResetVehicle(this);
}

/*!
\patch_internal 1.27 Date 10/18/2001 by Ondra.
- New: event callback when danger is detected.
*/
void EntityAI::OnDanger()
{
}

/*!
\param name weapon type name
\param force add weapon even if no free slots (bypass check)
\param reload try to reload weapon immediatelly
\return index of new weapon in array
*/
int EntityAI::AddWeapon(RStringB name, bool force, bool reload, bool checkSelected)
{
	Ref<WeaponType> weapon = WeaponTypes.New(name);
	return AddWeapon(weapon, force, reload, checkSelected);
}

/*!
\param name weapon type name
*/
void EntityAI::RemoveWeapon(RStringB name, bool checkSelected)
{
	// find and remove weapon
	Ref<const WeaponType> weapon;
	for (int i=0; i<_weapons.Size(); i++)
	{
		if (stricmp(_weapons[i]->GetName(), name) == 0)
		{
			weapon = _weapons[i];
			// deinit muzzles
			weapon->ShapeRelease();
			// remove weapon
			_weapons.Delete(i);
			break;
		}
	}
	if (!weapon) return;
	// remove attached magazine slots
	for (int i=0; i<_magazineSlots.Size();)
	{
		if (_magazineSlots[i]._weapon == weapon)
			_magazineSlots.Delete(i);
		else
			i++;
	}
	if (checkSelected)
	{
		OnWeaponRemoved();
		// TODO: move to OnWeaponRemoved
		if (_weapons.Size() > 0)
			SelectWeapon(FirstWeapon(), true);
		else
			SelectWeapon(-1, true);
		if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == this)
			GWorld->UI()->ResetVehicle(this);
	}
}

/*!
\param weapon weapon type
*/
void EntityAI::RemoveWeapon(const WeaponType *weapon, bool checkSelected)
{
	// find and remove weapon
	bool found = false;
	for (int i=0; i<_weapons.Size(); i++)
	{
		if (_weapons[i] == weapon)
		{
			found = true;
			// deinit muzzle
			weapon->ShapeRelease();

			_weapons.Delete(i);
			break;
		}
	}
	if (!found) return;
	// remove attached magazine slots
	for (int i=0; i<_magazineSlots.Size();)
	{
		if (_magazineSlots[i]._weapon == weapon)
			_magazineSlots.Delete(i);
		else
			i++;
	}
	if (checkSelected)
	{
		OnWeaponRemoved();
		if (_weapons.Size() > 0)
			SelectWeapon(FirstWeapon(), true);
		else
			SelectWeapon(-1, true);
		if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == this)
			GWorld->UI()->ResetVehicle(this);
	}
}

void EntityAI::RemoveAllWeapons()
{
	for (int i=0; i<_weapons.Size(); i++)
	{
		_weapons[i]->ShapeRelease();
	}
	_weapons.Clear();
	_magazineSlots.Clear();
	OnWeaponRemoved();
	SelectWeapon(-1, true);
	if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == this)
		GWorld->UI()->ResetVehicle(this);
}

void EntityAI::MinimalWeapons()
{
	RemoveAllMagazines();
}

/*!
\param name magazine type name
\param force add magazine even if no free slots (bypass check)
\param autoreload reload magazine into weapon immediatelly
\return index of new weapon in array
*/
int EntityAI::AddMagazine(Magazine *magazine, bool force, bool autoload)
{
	if (!CheckAccessCreate(*magazine->_type->_parClass))
	{
		return -1;
	}
	// add magazine type
	if (!force)
	{
		AUTO_STATIC_ARRAY(Ref<const Magazine>, conflict, 16);
		if (!CheckMagazine(magazine, conflict)) return -1;
		if (conflict.Size() > 0) return -1;
	}
	int index = _magazines.Add(magazine);

	// TODO: bug repaired - remove the patch
	#if _DEBUG
	CheckMagazines();
	#endif

	if (autoload)
	{
		for (int weapon=0; weapon<NMagazineSlots(); weapon++)
		{
			const MagazineSlot &slot = GetMagazineSlot(weapon);
			const MuzzleType *muzzle = slot._muzzle;
			if (!muzzle) continue;
			// if slot is not empty, there is no need to reload
			if (slot._magazine && slot._magazine->_ammo>0) continue;

			for (int k=0; k<muzzle->_magazines.Size(); k++)
				if (muzzle->_magazines[k] == magazine->_type)
				{
					AutoReload(weapon);
					return index;
				}

		}		
	}

	return index;
}

/*!
\param name magazine type name
\param force add magazine even if no free slots (bypass check)
\return index of new weapon in array
*/
int EntityAI::AddMagazine(RStringB name, bool force)
{
	Ref<MagazineType> magazineType = MagazineTypes.New(name);
	Ref<Magazine> magazine = new Magazine(magazineType);
	magazine->_ammo = magazineType->_maxAmmo;
	int index = AddMagazine(magazine, force);
	if (index < 0) return index;

	magazine->_reload = 0;
	if (magazine->_type->_modes.Size() > 0)
	{
		float	reload = 0.8 + 0.2 * GRandGen.RandomValue();
		magazine->_reload = reload * magazine->_type->_modes[0]->_reloadTime;
	}
	magazine->_reloadMagazine = 0;
	return index;
}

/*!
\param name magazine type name
*/
void EntityAI::RemoveMagazine(RStringB name)
{
	for (int i=0; i<_magazines.Size(); i++)
	{
		Ref<Magazine> magazine = _magazines[i];
		if (stricmp(magazine->_type->GetName(), name) == 0)
		{
			_magazines.Delete(i);
			for (int j=0; j<_magazineSlots.Size();)
			{
				if (_magazineSlots[j]._magazine == magazine)
					_magazineSlots[j]._magazine = NULL;
				else
					j++;
			}
			return;
		}
	}
}

/*!
\param magazine magazine to remove
*/
void EntityAI::RemoveMagazine(const Magazine *magazine)
{
	for (int i=0; i<_magazines.Size(); i++)
	{
		if (_magazines[i] == magazine)
		{
			_magazines.Delete(i);
			for (int j=0; j<_magazineSlots.Size();)
			{
				if (_magazineSlots[j]._magazine == magazine)
					_magazineSlots[j]._magazine = NULL;
				else
					j++;
			}
			return;
		}
	}
}

/*!
\param name name of magazine type to remove
*/
void EntityAI::RemoveMagazines(RStringB name)
{
	for (int i=0; i<_magazines.Size();)
	{
		Ref<Magazine> magazine = _magazines[i];
		if (stricmp(magazine->_type->GetName(), name) == 0)
		{
			_magazines.Delete(i);
			for (int j=0; j<_magazineSlots.Size(); j++)
			{
				if (_magazineSlots[j]._magazine == magazine)
					_magazineSlots[j]._magazine = NULL;
			}
		}
		else
			i++;
	}
}

void EntityAI::RemoveAllMagazines()
{
	_magazines.Clear();
	for (int i=0; i<_magazineSlots.Size(); i++)
		_magazineSlots[i]._magazine = NULL;
}

/*!
\param muzzle muzzle where magazine will be attached
\param magazine attached magazine
*/
void EntityAI::AttachMagazine(const MuzzleType *muzzle, Magazine *magazine)
{
  if (!muzzle->CanUse(magazine->_type))
  {
    RptF
    (
      "Cannot use magazine %s in muzzle %s",
      (const char *)magazine->_type->GetName(),
      (const char *)muzzle->GetName()
    );
    return;
  }
	for (int i=0; i<_magazineSlots.Size(); i++)
	{
		if (_magazineSlots[i]._muzzle == muzzle)
			_magazineSlots[i]._magazine = magazine;
	}
}

bool EntityAI::IsActionInProgress(MoveFinishF action) const
{ 
	return false;
}

void EntityAI::SelectWeaponCommander(AIUnit *unit, int weapon)
{
	if (weapon < 0 || weapon >= NMagazineSlots()) weapon = -1;
	if (IsLocal())
		SelectWeapon(weapon);
	else if (unit == GunnerUnit() && unit->GetPerson()->IsLocal())
	{
		SelectWeapon(weapon);
		GetNetworkManager().UpdateWeapons(this);
	}
	else
	{
		GetNetworkManager().AskForSelectWeapon(this, weapon);
	}
}

void EntityAI::SelectWeapon( int weapon, bool changed )
{
	if (!changed && _currentWeapon == weapon) return;
	if (_currentWeapon>=0 && _currentWeapon<NMagazineSlots())
	{
		// safefy patch:
		// if new weapon is selected, cancel rest of the burst
		const MagazineSlot &slot = GetMagazineSlot(_currentWeapon);
		Magazine *magazine = slot._magazine;

		if (magazine)
		{
			magazine->_burstLeft = 0;
		}
	}

	_currentWeapon = weapon;

	// check if handgun or primary weapon was selected
	if (_currentWeapon>=0 && _currentWeapon<NMagazineSlots())
	{
		const MagazineSlot &slot = GetMagazineSlot(_currentWeapon);
		WeaponType *weaponType = slot._weapon;
		if (weaponType)
		{
			int mask = weaponType->_weaponType;
			if (mask & MaskSlotPrimary) SelectHandGun(false);
			else if (mask & MaskSlotHandGun) SelectHandGun(true);
		}
	}
}

/*!
During some operations weapon manipulation is not possible
like during burst or during reload animation
*/

bool EntityAI::EnableWeaponManipulation() const
{
	/**/
	// during some operations weapon manipulation is not possible
	// like during burst or during reload animation
	// check if we are in the middle of the burst
	if (_currentWeapon>=0 && _currentWeapon<NMagazineSlots())
	{
		// safefy patch:
		// if new weapon is selected, cancel rest of the burst
		const MagazineSlot &slot = GetMagazineSlot(_currentWeapon);
		Magazine *magazine = slot._magazine;

		if (magazine)
		{
			if (magazine->_burstLeft>0) return false;
		}
	}
	/**/

	return true;
}

bool EntityAI::EnableViewThroughOptics() const
{
	// check if unit is able to use optics
	return true;
}



float EntityAI::GetHit( const HitPoint &hitpoint ) const
{
	int index=hitpoint.GetIndex();
	if( index>=0 )
	{
		// discreete hit simulation
		if (_hit[index]>=0.9 ) return 1;
	}
	return 0;
}

float EntityAI::GetHitCont( const HitPoint &hitpoint ) const
{
	int index=hitpoint.GetIndex();
	if( index>=0 )
	{
		// continuous hit simulation
		return _hit[index];
	}
	return 0;
}

/*!\param kind
* - 0 hull
* - 1 engine
* - 2 left track
* - 3 right track
* - 4 turret
* - 5 gun
*/

float EntityAI::GetHitForDisplay(int kind) const
{
	return 0;
}


bool EntityAI::CanFire() const
{
	if( _isDead ) return false;
	if( _isUpsideDown ) return false;
	return true;
}

bool EntityAI::IsAbleToMove() const
{
	return CanFire();
}

bool EntityAI::IsAbleToFire() const
{
	// do we have some ammo
	if (NMagazineSlots() == 0) return false;
	return CanFire();
}

bool EntityAI::IsCautious() const
{
	AIUnit *unit = PilotUnit();
	if (!unit) return false;
	CombatMode mode = unit->GetCombatMode();
	return mode == CMStealth || mode == CMCombat || mode == CMAware;
}

bool EntityAI::IsCautiousOrDanger() const
{
	AIUnit *unit = PilotUnit();
	if (!unit) return false;
	if (unit->IsDanger()) return true;
	return IsCautious();
}

float EntityAI::CalculateTotalCost() const
{
	return GetType()->GetCost();
}

float EntityAI::CalculateExposure(int x, int z) const
{
	AIUnit *unit = PilotUnit();
	if (!unit) return 0;
	AIGroup *grp = unit->GetGroup();
	if (!grp) return 0;
	AICenter *center = grp->GetCenter();
	if (!center) return 0;

	float expField = unit->IsHoldingFire() ?
		center->GetExposurePessimistic(x, z) :
		center->GetExposureOptimistic(x, z);
	expField *= 1.0f / 60.0f;

	float cost = CalculateTotalCost();
	float dammage = expField * GetType()->GetInvArmor();

	bool ableToDefend = GetAmmoHit() > 50 && !unit->IsHoldingFire();
	if (!ableToDefend) dammage *= 4;

	return dammage * cost;
}

void EntityAI::ForgetAimTarget()
{
	_fire._fireTarget=NULL;
}

bool EntityAI::StopAtStrategicPos() const
{
	return true;
}

void EntityAI::GoToStrategic( Vector3Par pos )
{
	// if destionation has changed, we cannot be in position
	if( _stratGoToPos.Distance2(pos)>0.5 ) _inFormation=false;
	_stratGoToPos=pos;
}

#define DIAG_SPEED 0
#define DIAG_COL 0

const float MaxBackAngle=H_PI*0.25;

#if _ENABLE_AI

/*! Common functionality for FormationPilot and LeaderPilot*/

/*!
\patch 1.63 Date 6/1/2002 by Ondra
- Fixed: AIPilot headChange was wrong if vehicle was banked (like motorcycle).
*/

bool EntityAI::PathPilot( float &speedWanted, float &headChange, float &turnPredict, float speedCoef)
{
	// check path position
	AIUnit *unit=PilotUnit();
	const Path &path=unit->GetPath();
	Assert( path.Size()>=2 );

	Vector3 steerPos=SteerPoint(GetSteerAheadSimul(),GetSteerAheadPlan());
	Vector3Val steerPredict=SteerPoint(GetPredictTurnSimul(),GetPredictTurnPlan());

	float hcOffset=0;

	float spdFactor=ModelSpeed()[2]*(1.0/15);
	saturate(spdFactor,0,1);

	Vector3 steerWant;
	if( spdFactor>0 )
	{
		steerWant=PositionWorldToModel(steerPos);

		if( steerWant.Z()>0 )
		{
			hcOffset=steerWant.X()*0.02;
			saturate(hcOffset,-0.25,+0.25);
		}
	}

	steerPos += DirectionAside()*_avoidAside;

	Matrix4 vertical, invVertical;
	vertical.SetUpAndDirection(VUp,Direction());
	vertical.SetPosition(Position());
	invVertical = vertical.InverseRotation();

	steerWant = invVertical.FastTransform(steerPos);

	Vector3Val steerPredictRel = invVertical.FastTransform(steerPredict);

	if (speedCoef >= 0)
	{
		headChange = atan2(steerWant.X(), steerWant.Z()) + hcOffset * spdFactor;
		turnPredict=atan2(steerPredictRel.X(),steerPredictRel.Z());
	}
	else
	{
		headChange = atan2(-steerWant.X(), -steerWant.Z()) - hcOffset * spdFactor;
		turnPredict=atan2(-steerPredictRel.X(),-steerPredictRel.Z());
	}

	float precision=GetPrecision();

	EngineOn();
	//_moveMode=gotoNormal;
	float cost=path.CostAtPos(Position());
	Vector3Val pos=path.PosAtCost(cost,Position());

	float distPath2=Position().Distance2(pos);
	float distEnd2=Position().Distance2(path.End());

	speedWanted=path.SpeedAtCost(cost) * speedCoef;

	AIUnit *leader=unit->GetSubgroup()->Leader();


	float tholdDist2 = Square(precision);
	if (unit->GetSubgroup()->GetMode()==AISubgroup::DirectGo)
	{
		tholdDist2 = Square(precision*0.9);
	}
	else if (_inFormation)
	{
		tholdDist2 = Square(precision*3);
	}

	if
	(
		leader->GetVehicle()->Speed().SquareSize()<Square(3) &&
		distEnd2<tholdDist2
	)
	{
		_inFormation=true;
		speedWanted=0;
		headChange=0;
		turnPredict=0;
	}
	else
	{
		_inFormation=false;
		if( distPath2>tholdDist2 )
		{
			saturateMax(speedWanted,GetType()->GetMaxSpeedMs()*0.25);
		}
	}

	if( Glob.time<_avoidSpeedTime )
	{
		saturate(speedWanted,-_avoidSpeed,+_avoidSpeed);
	}
	return _inFormation;
}

void EntityAI::LeaderPathPilot(AIUnit *unit, float &speedWanted, float &headChange, float &turnPredict, float speedCoef)
{
	// check path position
	const Path &path=unit->GetPath();
	if( path.Size()>=2 )
	{
		bool done=PathPilot(speedWanted,headChange,turnPredict, speedCoef);

		//_moveMode=gotoNormal;
		float cost=path.CostAtPos(Position());
		Vector3Val pos=path.PosAtCost(cost,Position());

		// measure distance from corresponding surface point?
		float distPath2=(Position()-pos).SquareSizeXZ();
		float distEnd2=Position().Distance2(path.End());

		float precision=GetPrecision();

		#if DIAG_SPEED
		if( this==GWorld->CameraOn() )
		{
			LogF("SpeedAtCost %.1f",speedWanted*3.6);
		}
		#endif


		// do not report in DirectGo mode
		if( unit->GetSubgroup()->GetMode()!=AISubgroup::DirectGo )
		{
			if (distEnd2 < Square(precision) || done)
			{
				unit->SendAnswer(AI::StepCompleted);
			}
			else if( distPath2>Square(precision*0.8) )
			{
				if( distPath2>Square(precision*2) || path.GetSearchTime()<Glob.time-2 )
				{
					// path is not recent
					unit->SendAnswer(AI::StepTimeOut);
				}
			}
			else if( path.GetMaxIndex()<path.Size() )
			{
				int lastValidIndex=path.GetMaxIndex()-1;
				Assert( lastValidIndex>=1 );
				// check if we are in valid region
				if( cost>path[lastValidIndex]._cost )
				{
					// we need to replan
					unit->SendAnswer(AI::StepTimeOut);
				}
			}
		}
	}
	else
	{
		speedWanted=0;
		turnPredict=0;
		headChange=0;
	}

	if (fabs(speedWanted)<0.1f)
	{
		AIUnit *commander = CommanderUnit();
		Vector3Val watchDir = commander->GetWatchDirection();
		float watchDirSize2 = watchDir.SquareSize();
		if( _fire._fireTarget && !_fire._firePrepareOnly )
		{
			Vector3Val tgtDir=PositionWorldToModel(_fire._fireTarget->AimingPosition());
			headChange=atan2(tgtDir.X(),tgtDir.Z());
		}
		else if (watchDirSize2>0.1f && commander->GetWatchMode()!=AIUnit::WMNo)
		{
			// if watch is commanded watch target only if you fire
			Vector3Val tgtDir = DirectionWorldToModel(watchDir);
			headChange=atan2(tgtDir.X(),tgtDir.Z());
		}
		else if( _fire._fireTarget )
		{
			Vector3Val tgtDir=PositionWorldToModel(_fire._fireTarget->AimingPosition());
			headChange=atan2(tgtDir.X(),tgtDir.Z());
		}
		else if (watchDirSize2>0.1f)
		{
			// formation watch - if no interesting target
			Vector3Val tgtDir = DirectionWorldToModel(watchDir);
			headChange=atan2(tgtDir.X(),tgtDir.Z());
		}
	}

	if( GetStopped() && fabs(headChange)<0.05f ) EngineOff();

	AISubgroup *subgrp = unit->GetSubgroup();
	if
	(
		_stratGoToPos.SquareSize()>0.1 &&
		subgrp->GetMode()!=AISubgroup::DirectGo
		&& StopAtStrategicPos()
	)
	{
		// in some modes we should not stop
		// we will replan route before we reach the position

		// strategic target known
		float finalDist2=_stratGoToPos.Distance2(Position());
		float bDist=GetType()->GetBrakeDistance();
		float maxSpeed=GetType()->GetMaxSpeedMs();
		if( finalDist2<Square(bDist*4) )
		{
			float limSpd=maxSpeed*0.5;
			if( finalDist2<Square(bDist*2) )
			{
				limSpd=maxSpeed*0.3;
				if( finalDist2<Square(bDist) )
				{
					limSpd=maxSpeed*0.1;
				}
				saturateMax(limSpd,1);
			}
			saturate(speedWanted,-limSpd,+limSpd);
			/*
			GlobalShowMessage
			(
				100,"Dist %.1f, spd %.1f, lim %.1f",
				sqrt(finalDist2),speedWanted*3.6,limSpd*3.6
			);
			*/
		}
	}

	// DirectGo is isued only on short distance
	// we should not move too fast
	if (subgrp && subgrp->GetMode()==AISubgroup::DirectGo)
	{
		float limSpd = 4; // 4m/s - i.e. 14 km/h
		saturate(speedWanted,-limSpd,+limSpd);
	}

	// check exposure
	AIUnit *commander = CommanderUnit();
	if( !commander )
	{
		LogF("Pilot, no commander");
		return;
	}

	AIGroup *group = subgrp->GetGroup();

	// do not limit speed if we are in danger and fleeing
	if( !commander->IsFreeSoldier() || !group->GetFlee() )
	{
		saturateMin(speedWanted,_limitSpeed); // move max. by given speed

		float maxSpeed=GetType()->GetMaxSpeedMs();
		// wait for convoy successor
		AIUnit *unitFollowed = subgrp->GetFormationNext(commander);
		if (unitFollowed)
		{
			EntityAI *followed = unitFollowed->GetVehicle();
			// each of vehicles has half influence to formation distance
			float factorZ = ( GetFormationZ() +followed->GetFormationZ() )*0.5;

			float dist = followed->Position().Distance(Position());
			// wanted distance must be bigger than 1.0
			// to force followed to catch me up
			float wantedDistance = factorZ * 3;
			// if we are at wantedDistance from him, we want to go at his speed
			float isFar=(dist-wantedDistance)*(0.1/factorZ);
			saturate(isFar,-1,1);
			// if we are further, we want to go slower (positive isFar)
			// if we are nearer, we want to go faster (negative isFar)
			float waitSpeed=-GetType()->GetMaxSpeedMs()*isFar;
			Vector3 followedRelSpeed=DirectionWorldToModel(followed->Speed());
			// his speed relative to me (positiove - he's going away)
			float followedSpeedZ=followedRelSpeed.Z();
			// wanted speed is given by his speed (followedSpeedZ)
			// and by waitSpeed (my wanted speed relative to him)
			saturateMin(speedWanted, floatMax(followedSpeedZ + waitSpeed, maxSpeed*0.1));
		}
	}
}

void EntityAI::LeaderPilot( float &speedWanted, float &headChange, float &turnPredict )
{ // subgroup leader -
	// if we are near the target we have to operate more precisely
	if (_userStopped)
	{
		speedWanted=0;
		headChange=0;
		turnPredict=0;
		return;
	}
		
	speedWanted=0; // go faster
	AIUnit *unit=PilotUnit();
	
	if (unit->GetAIDisabled()&AIUnit::DAMove) return;

	#if DIAG_SPEED
	if( this==GWorld->CameraOn() )
	{
		LogF("Basic speed %.1f",speedWanted*3.6);
	}
	#endif

	// check if hiding
	// (similiar to keeping formation)

	Vector3 tgtPos = HideFrom();
	if( _hideBehind )
	{
		Vector3Val hideBC=_hideBehind->GetShape()->BoundingCenter();
		Vector3Val hideBehindPos=_hideBehind->PositionModelToWorld(-hideBC);

		Vector3 offset=tgtPos-hideBehindPos;

		float behind=_hideBehind->CollisionSize()+CollisionSize();

		Vector3Val aPos=Position();
		float aboveSurface=aPos.Y()-GLandscape->SurfaceYAboveWater(aPos.X(),aPos.Z());

		Vector3 pos = hideBehindPos - offset.Normalized()*behind;

#if _ENABLE_CHEATS
		if (CHECK_DIAG(DECombat))
		{
			Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),-1);
			obj->SetPosition(pos);
			obj->SetScale(0.5);
			obj->SetConstantColor(PackedColor(Color(0,0,0)));
			GLandscape->ShowObject(obj);			
		}
#endif

		pos[1]=GLandscape->SurfaceYAboveWater(pos.X(),pos.Z()) + aboveSurface;

		Vector3Val watchDir=_fire._fireTarget ? _fire._fireTarget->AimingPosition() : tgtPos;
		Vector3 norm = (watchDir-Position());
		Vector3 dir=norm.Normalized();

		float precision=floatMin(_hideBehind->CollisionSize()*0.25,Object::CollisionSize());
		saturate(precision,2,50);
		PositionPilot(speedWanted,headChange,turnPredict,pos,dir,precision);
		return;
	}

	LeaderPathPilot(unit, speedWanted, headChange, turnPredict);
}

#endif //_ENABLE_AI

float EntityAI::PilotSpeed() const
{
	float speedWanted=0; // go faster	
	// check path position
	const Path &path=PilotUnit()->GetPath();
	if( path.Size()>=2 )
	{
		float cost=path.CostAtPos(Position());
		speedWanted=path.SpeedAtCost(cost);
	}
	return speedWanted;
}

#if _ENABLE_AI

void EntityAI::PositionPilot
(
	float &speedWanted, float &headChange, float &turnPredict,
	Vector3Par pos, Vector3Par dir, float precision
)
{
	AIUnit *unit=PilotUnit();

	// control vehicle to get to given position
	// default: no speed limit
	_limitSpeed=GetType()->GetMaxSpeedMs()*1.5;
	float dist2=pos.Distance2(Position());

	saturateMin(precision,floatMax(4,GetPrecision()));
	float limit=precision;
	if( !_inFormation ) limit*=0.5;
	else limit*=2;

	if( dist2<Square(limit) )
	{
		// in position - turn to direction
		_inFormation=true;
		speedWanted=0;
		//if( _fireTarget!=NULL && !_firePrepareOnly && _fireMode>=0 )
		if( _fire._fireTarget )
		{
			Vector3Val tgtDir=PositionWorldToModel(_fire._fireTarget->AimingPosition());
			headChange=atan2(tgtDir.X(),tgtDir.Z());
		}
		else
		{
			Vector3Val relMoveDir=DirectionWorldToModel(dir);
			headChange=atan2(relMoveDir.X(),relMoveDir.Z());
			if( fabs(headChange)<0.05 && GetStopped() )
			{
				EngineOff();
			}
		}
	}
	else
	{
		// out of position
		_inFormation=false;
		// check if trivial solution is good enough

		// check if we are already there
		// find nearest empry position
		Vector3 freePos = pos;
		unit->FindNearestEmpty(freePos);

		dist2=freePos.Distance2(Position());

		if( dist2<Square(limit) )
		{
			// we cannot be in formation - in-formation position is not free
			_inFormation = true;
		}

		bool simple=false;
		if( Glob.time>_lastSimplePath+0.5 )
		{
			simple=unit->IsSimplePath(Position(),freePos);
			if( simple ) _lastSimplePath=Glob.time;
		}
		else simple=true;
		if( simple )
		{
			//Path &path=unit->GetPath();
			// trivial solution works
			//_goToState=GoToNormal;
			Vector3Val moveDir=PositionWorldToModel(freePos);
			headChange=atan2(moveDir.X(),moveDir.Z());
			speedWanted=sqrt(dist2)*GetInvFormationTime();
			saturateMax(speedWanted,1.5);
			if
			(
				moveDir.SquareSize()<Square(GetType()->GetMaxSpeedMs()*3)
				&& fabs(AngleDifference(headChange,H_PI))<MaxBackAngle
			)
			{
				// go back
				headChange=AngleDifference(headChange,H_PI);
				speedWanted=-speedWanted;
			}
			unit->SetWantedPosition(freePos, AIUnit::DoNotPlan, true);
		}
		else
		{
			// non-trivial solution needed
			Path &path=unit->GetPath();
			bool update=false;

			bool pathInvalid =
			(
				path.Size()<2 ||
				path.End().DistanceXZ2(freePos)>Square(limit)
			);

			if( path.Size()<2 )
			{ // no known solution
				// avoid searching too often
				if( Glob.time-path.GetSearchTime()>2 ) update=true;
			}
			else if( pathInvalid && Glob.time-path.GetSearchTime()>5 )
			{ // solution is invalid and too old
				update=true;
			}
			else if( Glob.time-path.GetSearchTime()>15 )
			{ // solution is too old
				update=true;
			}
/*
			if( update )
			{
				// TODO: try to move in the same direction for a while
				unit->CreatePath(Position(),freePos);
			}
*/
			unit->SetWantedPosition(freePos, AIUnit::FormationPlanned, update);
//			unit->SetWantedPosition(freePos, AIUnit::FormationPlanned, false);

			if( path.Size()>=2 )
			{
				PathPilot(speedWanted,headChange,turnPredict);
				// note: pathpilot does AvoidCollision
			}
			else speedWanted=0,headChange=0;
			// plan is used by default handler (steer pos)
		}
	}
	turnPredict=headChange;
}

#endif //_ENABLE_AI

bool EntityAI::IsOnRoad() const
{
	AIUnit *unit=CommanderUnit();
	if( !unit ) return false;
	if( QIsManual() )
	{
		return GRoadNet->IsOnRoad(Position(),CollisionSize()) != NULL;
	}
	else
	{
		const Path &path=unit->GetPath();
		return ( path.GetOnRoad() && path.Size()>=2 );
	}
}

bool EntityAI::IsOnRoadMoving( float minSpeed ) const
{
	if( Speed().SquareSize()<Square(minSpeed) ) return false;
	return IsOnRoad();
}

void EntityAI::CheckAway()
{
	AIUnit *unit = CommanderUnit();
	if( !unit ) return;
	bool away = IsAway();
	if (away)
	{
		unit->SetAway();
	}
}

bool EntityAI::IsAway(float factor)
{
	AIUnit *unit = CommanderUnit();
	if( !unit ) return false;
	AISubgroup * subgrp = unit->GetSubgroup();

	float lostUnit = 12.0 * floatMax(GetFormationX(),GetFormationZ());
	saturate(lostUnit, LostUnitMin, LostUnitMax);
	lostUnit *= factor;

	if (!unit->IsSubgroupLeader())
	{
		// check if we keep in formation
		if( !IsCautious() )
		{
			EntityAI *follow=subgrp->GetFormationPrevious(unit)->GetVehicle();
			// each of vehicles has half influence to formation distance
			float factorZ = ( GetFormationZ() +follow->GetFormationZ() )*0.5;
			Vector3 relPos(0, 0, -0.4 * factorZ);

			if ((follow->Position()-Position()).SquareSizeXZ() > Square(lostUnit))
			{
				return true;
			}
		}
		else
		{
			Vector3 formPos = unit->GetFormationAbsolute();
			if (formPos.Distance2(Position())>Square(lostUnit))
			{
				return true;
			}
		}
	}
	return false;
}

void EntityAI::SwitchToFormation()
{
	_hideBehind = NULL;
	_inFormation = false;
}
void EntityAI::SwitchToLeader()
{
	_hideBehind = NULL;
	_inFormation = false;
}

#if _ENABLE_AI

void EntityAI::FormationPilot( float &speedWanted, float &headChange, float &turnPredict )
{
	if (_userStopped)
	{
		speedWanted=0;
		headChange=0;
		turnPredict=0;
		return;
	}

	// predict leader position
	AIUnit *unit=PilotUnit();
	if( !unit ) return;
	AIUnit *commander=CommanderUnit();
	if( !commander ) return;
	if (unit->GetAIDisabled()&AIUnit::DAMove) return;

	AISubgroup *subgrp=commander->GetSubgroup();
	Assert(subgrp);
	if (!subgrp) return;
	
	AIGroup *grp=commander->GetGroup();
	Assert(grp);
	if (!grp) return;
	
	AIUnit *leader=subgrp->Leader();
	Assert(leader);
	if (!leader) return;

	EntityAI *lVehicle=leader->GetVehicle();

	// check if leader is on road
	bool onRoad=false;
	if( !IsCautious() )
	{
		onRoad=lVehicle->IsOnRoad();

		// use follow mode - ignore formation

		// special case - leader on road
		//EntityAI *follow=lVehicle;
		// follow previous vehicle in subgroup
		EntityAI *follow=subgrp->GetFormationPrevious(commander)->GetVehicle();
		// each of vehicles has half influence to formation distance
		float factorZ = ( GetFormationZ() +follow->GetFormationZ() )*0.5;
		Vector3 relPos(0, 0, -0.4 * factorZ);

		Vector3 followPos = follow->PositionModelToWorld(relPos);
		bool forceReplan = false;

		if( !onRoad )
		{
			followPos+=follow->Speed()*GetFormationTime()*0.6;
		}
		else
		{
			followPos += follow->Speed()*GetFormationTime()*0.3;
		}

		Path &path=unit->GetPath();
		if( onRoad && path.GetOnRoad() )
		{
			// road path refresh should be quite cheap
			if
			(
				path.Size()>=2 && path.CostAtPos(Position()+Speed())>=path.EndCost() ||
				Glob.time-path.GetSearchTime()>0.5
			)
			{
				// or we are on the end of the path
				// try cheap search
				followPos = GRoadNet->GetNearestRoadPoint(followPos);
				forceReplan = true;
			}
		}

		float precision=GetPrecision();
		if
		( 
			path.Size()>=2 && path.End().Distance2(Position())<Square(precision) &&
			Glob.time-path.GetSearchTime()>0.5
		)
		{
			// end of path reached - replan
			unit->FindNearestEmpty(followPos);
			forceReplan = true;
		}
		if( Glob.time-path.GetSearchTime()>2 )
		{
			// path is old - consider replan
			if( path.Size()<2 || path.End().Distance2(followPos)>Square(precision) )
			{
				unit->FindNearestEmpty(followPos);
				// no path or path invalid
				forceReplan = true;
			}
		}
		// ask unit to create path
		unit->SetWantedPosition(followPos, AIUnit::FormationPlanned, forceReplan);
//		unit->SetWantedPosition(followPos, AIUnit::FormationPlanned, false);
		
		if( path.Size()>=2 )
		{
			PathPilot(speedWanted,headChange,turnPredict);
			// note: path pilot does AvoidCollision

			// dynamic stability
			float limitSpeed;
			float maxSpeed=GetType()->GetMaxSpeedMs();
			{
				// do not try to overtake the vehicle you should follow
				float dist=follow->Position().Distance(Position());
				// keep safe distance based on his speed
				float wantedDistance=factorZ*0.6 + + fabs(follow->ModelSpeed().Z())*0.5;
				// if we are at wantedDistance from him, we want to go at his speed
				float isFar=(dist-wantedDistance)*(0.4/factorZ);
				//isFar*=fabs(isFar);
				saturate(isFar,-0.3,1);
				// if we are further, we want to go faster (positive isFar)
				// if we are nearer, we want to go slower (negative isFar)
				float catchUpSpeed=maxSpeed*isFar;
				Vector3 followRelSpeed=DirectionWorldToModel(follow->Speed());
				// his speed relative to me (positiove - he's going away)
				float followSpeedZ=followRelSpeed.Z();
				// wanted speed is given by his speed (followSpeedZ)
				// and by catchUpSpeed (my wanted speed relative to him)
				limitSpeed=floatMax(followSpeedZ+catchUpSpeed,maxSpeed*0.1);
				// speedWanted is currently max speed given by path planning
			}

			AIUnit *unitFollowed = subgrp->GetFormationNext(commander);
			if (unitFollowed)
			{
				EntityAI *followed = unitFollowed->GetVehicle();
				// each of vehicles has half influence to formation distance
				float factorZ = ( GetFormationZ() +followed->GetFormationZ() )*0.5;

				float dist = followed->Position().Distance(Position());
				// wanted distance must be bigger than 1.0
				// to force followed to catch me up
				float wantedDistance = factorZ * 3;
				// if we are at wantedDistance from him, we want to go at his speed
				float isFar=(dist-wantedDistance)*(0.1/factorZ);
				saturate(isFar,-1,1);
				// if we are further, we want to go slower (positive isFar)
				// if we are nearer, we want to go faster (negative isFar)
				float waitSpeed=-GetType()->GetMaxSpeedMs()*isFar;
				Vector3 followedRelSpeed=DirectionWorldToModel(followed->Speed());
				// his speed relative to me (positiove - he's going away)
				float followedSpeedZ=followedRelSpeed.Z();
				// wanted speed is given by his speed (followedSpeedZ)
				// and by waitSpeed (my wanted speed relative to him)
				saturateMin(limitSpeed, floatMax(followedSpeedZ + waitSpeed, maxSpeed*0.1));
			}

			saturate(speedWanted,-limitSpeed,+limitSpeed);
		}
		else
		{
			speedWanted=0;
			headChange=0;
			turnPredict=0;
		}
		return;
	}

	float estT=GetFormationTime();

	// 
	bool enableHide = commander->IsFreeSoldier() && commander->GetCombatMode()>=CMCombat;
	if (enableHide)
	{
		if (leader->IsPlayer())
		{
			enableHide = !commander->IsKeepingFormation();
		}
	}

	// predict leader position
	Vector3 predSpeed = lVehicle->Speed();
	float maxSpeed = lVehicle->GetType()->GetMaxSpeedMs();
	float spSize2 = predSpeed.SquareSize();
	if (spSize2<Square(maxSpeed*0.5) && spSize2>Square(maxSpeed*0.1))
	{
		predSpeed.Normalize();
		predSpeed *= maxSpeed*0.5;
	}
	Vector3Val estPos=lVehicle->Position()+estT*predSpeed;
	// predict leader orientation
	Matrix4 estTransform;
	estTransform.SetPosition(estPos);
	// get formation orientation
	Vector3Val formDir=leader->GetSubgroup()->GetFormationDirection();
	estTransform.SetDirectionAndUp(formDir,VUp);

	Vector3Val formPos=commander->GetFormationRelative()-leader->GetFormationRelative();
	//Vector3Val moveDir=estTransform.Direction();
	Vector3Val moveDir = commander->GetWatchDirection();

	// check if we are already in cover
	if (enableHide)
	{
		Vector3 movePos=lVehicle->PositionModelToWorld(formPos);
		// check position is combat height
		float height = GetCombatHeight();

		movePos += formDir*GetType()->GetMaxSpeedMs()*10;

		movePos[1] = GLandscape->SurfaceYAboveWater(movePos[0],movePos[2])+height;
	
		unit->FindNearestEmpty(movePos);

		float radius=GetType()->GetMaxSpeedMs()*20;
		float maxDist = radius*0.25;

		// hiding enabled
		if
		(
			_hideBehind && _hideBehind->Position().DistanceXZ2(movePos)>Square(maxDist*1.5f)
		)
		{
			// hideBehind too far
			_hideBehind = NULL;
		}
		if (_inFormation || !_hideBehind)
		{
			// find some cover near formation position
			if( _hideRefreshTime<Glob.time-5 )
			{
				// hide in front of formation
				FindHideBehind(movePos,maxDist);
			}
		}
		if (_hideBehind)
		{
			Vector3Val hideBC=_hideBehind->GetShape()->BoundingCenter();
			Vector3Val hideBehindPos=_hideBehind->PositionModelToWorld(-hideBC);

			Vector3 tgtPos = HideFrom();
			Vector3 offset=tgtPos-hideBehindPos;

			float behind=_hideBehind->CollisionSize()+CollisionSize();

			Vector3Val aPos=Position();
			float aboveSurface=aPos.Y()-GLandscape->SurfaceYAboveWater(aPos.X(),aPos.Z());

			Vector3 pos= hideBehindPos - offset.Normalized()*behind;

			pos[1]=GLandscape->SurfaceYAboveWater(pos.X(),pos.Z()) + aboveSurface;

			Vector3Val watchDir=_fire._fireTarget ? _fire._fireTarget->AimingPosition() : tgtPos;
			Vector3 norm = (watchDir-Position());
			Vector3 dir=norm.Normalized();

#if _ENABLE_CHEATS
			if (CHECK_DIAG(DECombat))
			{
				Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),-1);
				obj->SetPosition(pos);
				obj->SetScale(0.5);
				obj->SetConstantColor(PackedColor(Color(0.3,0,0)));
				GLandscape->ShowObject(obj);			
			}
#endif

			float precision=floatMin(_hideBehind->CollisionSize()*0.25,Object::CollisionSize());
			saturate(precision,2,50);
			PositionPilot(speedWanted,headChange,turnPredict,pos,dir,precision);
			return;
		}
	}
	else
	{
		_hideBehind = NULL;
	}

	Vector3 movePos=estTransform.FastTransform(formPos);
	// check position is combat height
	float height = GetCombatHeight();
	movePos[1] = GLandscape->SurfaceYAboveWater(movePos[0],movePos[2])+height;
	
#if _ENABLE_CHEATS
		if (CHECK_DIAG(DECombat))
		{
			Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),-1);
			obj->SetPosition(movePos);
			obj->SetScale(0.6);
			obj->SetConstantColor(PackedColor(Color(1,0,1)));
			GLandscape->ShowObject(obj);
		}
#endif

	// calculate "ideal" speed
	// based on current position and current formation position
	Vector3Val formPosAct = commander->GetFormationAbsolute();
	Vector3Val formPosRel = PositionWorldToModel(formPosAct);
	Vector3Val leadSpdRel = DirectionWorldToModel(lVehicle->Speed());

	// ideal speed is such speed that we will reach position in estT time
	float speedZ = floatMax(formPosRel.Z()/estT+leadSpdRel.Z(),1);

	PositionPilot(speedWanted,headChange,turnPredict,movePos,moveDir,1e10);
	saturateMin(speedWanted,speedZ);
}

#endif

void EntityAI::PerformFF( FFEffects &effects )
{
	//effects=_ff;
	//_ff.gunCount=0; // reset guns
	// calculate acceleration effects
	//Vector3Val accel=DirectionWorldToModel(Acceleration());
	//Vector3Val accelR=accel*0.1;
	//effects.forceX=-accelR[0]*0.5;
	//effects.forceY=-accelR[1]-accelR[2];
	effects.engineMag=0;
	effects.engineFreq=1;
}

void EntityAI::ResetFF()
{
	//_ff.gunCount=0; // reset guns
}

void EntityAI::LimitSpeed( float speed )
{
	_limitSpeed=speed;
}

AIGroup* EntityAI::GetGroup() const
{
	AIUnit *unit=CommanderUnit();
	if (!unit) unit=PilotUnit();
	if (!unit) unit=GunnerUnit();
	if (!unit) return NULL;
	return unit->GetGroup();
}

bool EntityAI::DisableWeapons() const
{
	return false;
}

RString EntityAI::GetActionName(const UIAction &action)
{
	if (action.type == ATUser)
	{
		const UserActionDescription *desc = FindUserAction(action.param);
		if (desc) return desc->text;
	}
	else if (action.type == ATUserType)
	{
		const EntityAIType *type = GetType();
		return type->_userTypeActions[action.param].displayName;
	}
	return RString("Unknown action")+FindEnumName(action.type);
}

bool EntityAI::CheckActionProcessing(UIActionType action, AIUnit *unit) const
{
	// return action is not processing
	// default is process all action immediatelly
	return false;
}

void EntityAI::StartActionProcessing(const UIAction &action, AIUnit *unit)
{
	// default: process action directly
	action.Process(unit);
}

/*!
\patch 1.61 Date 5/30/2002 by Jirka
- Fixed: Enable put action for all owned explosives
*/

void EntityAI::PerformAction(const UIAction &action, AIUnit *unit)
{
	switch (action.type)
	{
		case ATLightOn:
			SetPilotLight(true);
			return;
		case ATLightOff:
			SetPilotLight(false);
			return;
		case ATSwitchWeapon:
			SelectWeaponCommander(unit,action.param);
			return;
		case ATUseWeapon:
			FireWeapon(action.param, NULL);
			return;
		case ATUseMagazine:
			{
				// find magazine
				int m = -1;
				for (int i=0; i<_magazines.Size(); i++)
				{
					Magazine *magazine = _magazines[i];
					if (magazine && magazine->_creator == action.param && magazine->_id == action.param2)
					{
						m = i;
						break;
					}
				}
				if (m < 0) return;
				Magazine *magazine = _magazines[m];

				// find slot
				int s = -1;
				for (int i=0; i<_magazineSlots.Size();)
				{
					MagazineSlot &slot = _magazineSlots[i];
					if (!slot._muzzle)
					{
						i++;
						continue;
					}
					if (slot._muzzle->CanUse(magazine->_type))
					{
						s = i;
						break;
					}
					i += slot._muzzle->_nModes;
				}
				if (s < 0) return;

				// reload and fire
				ReloadMagazineTimed(s, m, true);
				magazine->_reload = 0;
				FireWeapon(s, NULL);
			}
			return;
		case ATUser:
			if (unit)
			{
				const UserActionDescription *desc = FindUserAction(action.param);
				if (desc && desc->script.GetLength() > 0)
				{
					GameArrayType arguments;
					arguments.Add(GameValueExt(this));
					arguments.Add(GameValueExt(unit->GetPerson()));
					arguments.Add(GameValue((float)desc->id));
					Script *script = new Script(desc->script, GameValue(arguments));
					GWorld->AddScript(script);
				}
			}
			return;
		case ATUserType:
			if (unit)
			{
				const UserTypeAction &act = GetType()->_userTypeActions[action.param];
				GameState *gstate = GWorld->GetGameState();
				gstate->VarSet("this", GameValueExt(this), true);
				gstate->Execute(act.statement);
			}
			return;
	}
	// note: action is performed by target of the action
	// unit is who activated the action
	ErrF
	(
		"%s (%s): Unknown action %s, target %s",
		(const char *)GetDebugName(),
		(const char *)GetType()->GetDisplayName(),
		(const char *)FindEnumName(action.type),
		(const char *)action.target->GetDebugName()
	);
}


bool EntityAI::CheckMagazines()
{
	bool doubleId = false;
	// check all magazines for unique id
	for (int i=0; i<_magazines.Size(); i++)
	{
		const Magazine *magI = _magazines[i];
		if (!magI) continue;
		for (int j=0; j<i; j++)
		{
			const Magazine *magJ = _magazines[j];
			if (!magJ) continue;
			if (magJ->_id!=magI->_id) continue;
			// take action - double ID
			// remove magazine I

			ErrF
			(
				"Double magazine %d:%s (%x) %d:%s (%x) in %s",
				magI->_id,(const char *)magI->_type->GetName(),magI,
				magJ->_id,(const char *)magJ->_type->GetName(),magJ,
				(const char *)GetDebugName()
			);

			_magazines.Delete(i);
			i--; // to compensate for i++ in for loop commande
			doubleId = true;
				
			break;
		}

	}
	return doubleId;
}

LODShapeWithShadow *EntityAI::GetMissileShape() const
{
	for (int i=0; i<NMagazines(); i++)
	{
		const Magazine *mag = GetMagazine(i);
		if (mag->_type->_modes.Size()<=0) continue;
		const WeaponModeType *mode = mag->_type->_modes[0];
		const AmmoType *ammo = mode->_ammo;
		if (!ammo) continue;
		if (ammo->_simulation!=AmmoShotMissile) continue;
		if (ammo->maxControlRange<10) continue;
		if (ammo->_proxyShape) return ammo->_proxyShape;
	}
	return NULL;
}

int EntityAI::CountMissiles() const
{
	int total = 0;
	for (int i=0; i<NMagazines(); i++)
	{
		const Magazine *mag = GetMagazine(i);
		if (mag->_type->_modes.Size()<=0) continue;
		const WeaponModeType *mode = mag->_type->_modes[0];
		const AmmoType *ammo = mode->_ammo;
		if (!ammo) continue;
		if (ammo->_simulation!=AmmoShotMissile) continue;
		if (ammo->maxControlRange<10) continue;
		total += mag->_ammo;
	}
	return total;		
}

/*!
\param type given magazine type
\param ammo minimal amount of ammo magazine must contain
\return index of magazine in array or -1 if not found
*/
int EntityAI::FindBestMagazine(const MagazineType *type, int ammo) const
{
	int best = -1;
	for (int j=0; j<NMagazines(); j++)
	{
		const Magazine *reserve = GetMagazine(j);
		if (!reserve) continue;
		if (reserve->_type != type) continue;
		if (reserve->_ammo > ammo)
		{
			ammo = reserve->_ammo;
			best = j;
		}
	}
	return best;
}

static void GetReloadActions
(
	EntityAI *veh, UIActions &actions,
	int iSlot, const MuzzleType *currentMuzzle
)
{
	const MagazineSlot &slot = veh->GetMagazineSlot(iSlot);
	const Magazine *magazine = slot._magazine;
  const MagazineType *mType = magazine ? magazine->_type : NULL;
	bool empty = false;
  if (mType && slot._mode >= 0 && slot._mode < mType->_modes.Size())
	{
		const WeaponModeType *mode = mType->_modes[slot._mode];
		if (mode->_ammo && mType->_maxAmmo > 0) empty = magazine->_ammo == 0;
		if
		(
			!empty && currentMuzzle->_autoReload &&
			(mType->_maxAmmo == 1 || (slot._weapon->_weaponType & MaskHardMounted) != 0)
		)
		{
			// do not offer reload HandGrenade etc.
		}
		else
		{
			// magazine of the same type
			int best = veh->FindBestMagazine(mType, 0);
			if (best >= 0)
			{
				float prior = empty ? 1.5 : 0.35;
				const Magazine *magazine = veh->GetMagazine(best);
				RString muzzleID = slot._weapon->GetName() + RString("|") + slot._muzzle->GetName();
				actions.Add
				(
					ATLoadMagazine, veh, prior,
					magazine->_creator, empty, true,
					magazine->_id, muzzleID
				);
			}
		}
	}
	else empty = true;
	// magazines of other type
	for (int i=0; i<currentMuzzle->_magazines.Size(); i++)
	{
		const MagazineType *change = currentMuzzle->_magazines[i];
		if (change == mType) continue;
		int best = veh->FindBestMagazine(change, 0);
		if (best >= 0)
		{
			const Magazine *magazine = veh->GetMagazine(best);
			RString muzzleID = slot._weapon->GetName() + RString("|") + slot._muzzle->GetName();
			actions.Add
			(
				ATLoadMagazine, veh, 0.34,
				magazine->_creator, empty, true,
				magazine->_id, muzzleID
			);
		}
	}
}

TypeIsSimple(MagazineType *)

/*!
\param actions List to which result should be appended.
\param unit Unit that tries to perform action on this entity
\param now If true, only actions that can be performed
from current unit position are listed.
\patch 1.11 Date 08/03/2001 by Jirka
- Improved: user actions enabled also for AI units
\patch 1.34 Date 12/6/2001 by Jirka
- Fixed: commander can switch ligths only if not in cargo
\patch 1.89 Date 10/25/2002 by Ondra
- Fixed: Custom action inside vehicle was enable only until vehicle moved.
*/

void EntityAI::GetActions(UIActions &actions, AIUnit *unit, bool now)
{
	if (!unit) return;
	// user actions
	// FIX
	if (_userActions.Size() > 0)
	{
		bool ok = true;
		if (now && unit->GetVehicle() != this)
		{
			// check if in front
			Vector3Val relPos = unit->GetPerson()->PositionWorldToModel(Position());
			ok = relPos.Z() > 0 && relPos.SquareSize() < Square(10);
		}
		if (ok)
		{
			for (int i=0; i<_userActions.Size(); i++)
			{
				int id = _userActions[i].id;
				actions.Add(ATUser, this, 1.5 - 0.001 * id, id, true);
			}
		}
	}

	// user type actions
#if _ENABLE_DATADISC
	const EntityAIType *type = GetType();
	if (type->_userTypeActions.Size() > 0)
	{
		Vector3Val relPos = PositionWorldToModel(unit->GetPerson()->WorldPosition());
		GameState *gstate = GWorld->GetGameState();
		gstate->VarSet("this", GameValueExt(this), true);

		for (int i=0; i<type->_userTypeActions.Size(); i++)
		{
			const UserTypeAction &act = type->_userTypeActions[i];
			if (relPos.Distance2(act.modelPosition) > Square(act.radius)) continue;
			if (!gstate->EvaluateBool(act.condition)) continue;

			actions.Add(ATUserType, this, 1.4 - 0.001 * i, i, true);
		}
	}
#endif

	if (unit->GetVehicle() == this && CommanderUnit() == unit && !unit->IsInCargo())
	{
		// lights on / off
		if (unit->IsPlayer() && _reflectors.Size()>0)
		{
			if (IsPilotLight())
			{
				actions.Add(ATLightOff, this, 0.3);
			}
			else
			{
				if (GScene->MainLight()->NightEffect() > 0)
					actions.Add(ATLightOn, this, 0.3);
			}
		}
		if (EnableWeaponManipulation())
		{
			// secondary weapons
			int primary = -1;
			bool isPrimaryWeapon = false;
			int primaryMask = IsHandGunSelected() ? MaskSlotHandGun : MaskSlotPrimary;
			for (int i=0; i<NMagazineSlots(); i++)
			{
				if (i == _currentWeapon) continue;
				const MagazineSlot &slot = GetMagazineSlot(i);
				const MuzzleType *muzzle = slot._muzzle;
				if (muzzle->_primary)
				{
					if (primary < 0)
					{
						primary = i;
						isPrimaryWeapon = (slot._weapon->_weaponType & primaryMask) != 0;
					}
					else if (!isPrimaryWeapon && (slot._weapon->_weaponType & primaryMask) != 0)
					{
						primary = i;
						isPrimaryWeapon = true;
					}
				}
				else
				{
					if (!slot._muzzle->_showEmpty && EmptySlot(slot)) continue;
					const WeaponModeType *mode = GetWeaponMode(i);				
					if (mode && mode->_useAction)
					{
						// actions.Add(ATUseWeapon, this, 0.5 - 0.01 * i, i);
					}
					else
						actions.Add(ATSwitchWeapon, this, 0.5 - 0.01 * i, i);
				}
			}
			// primary weapon
			if (_currentWeapon >= 0)
			{
				const MagazineSlot &slot = GetMagazineSlot(_currentWeapon);
				const MuzzleType *muzzle = slot._muzzle;
				if (!muzzle->_primary && primary >= 0)
				{
					// search for primary weapon
					actions.Add(ATSwitchWeapon, this, 0.51, primary);
				}
			}
			// use magazines
			AUTO_STATIC_ARRAY(MagazineType *,usedTypes,32);
			for (int i=0; i<_magazines.Size(); i++)
			{
				Magazine *magazine = _magazines[i];
				if (magazine && magazine->_ammo > 0 && magazine->_type->_useAction)
				{
					// check if this magazine type wasn't processed already
					bool found = false;
					for (int j=0; j<usedTypes.Size(); j++)
						if (magazine->_type == usedTypes[j])
						{
							found = true; break;
						}

					if (!found)
					{
						actions.Add(ATUseMagazine, this, 0.519 - 0.0001 * i, magazine->_creator, false, true, magazine->_id);
						usedTypes.Add(magazine->_type);
					}
				}
			}
			// magazines
			const MuzzleType *currentMuzzle = NULL;
			if (_currentWeapon >= 0)
			{
				currentMuzzle = GetMagazineSlot(_currentWeapon)._muzzle;
				GetReloadActions(this, actions, _currentWeapon, currentMuzzle);
			}
			// background reloading
			int iSlot = 0;
			for (int w=0; w<_weapons.Size(); w++)
			{
				const WeaponType *weapon = _weapons[w];
				for (int m=0; m<weapon->_muzzles.Size(); m++)
				{
					const MuzzleType *muzzle = weapon->_muzzles[m];
					if (muzzle != currentMuzzle && muzzle->_backgroundReload)
					{
						GetReloadActions(this, actions, iSlot, muzzle);
					}
					iSlot += muzzle->_nModes;
				}
			}
		}
	}
}

/*!
	\param text displayed text of action
	\param script this script will be performed when action is activated
	\return id of action
*/
int EntityAI::AddUserAction(RString text, RString script)
{
	int index = _userActions.Add();
	UserActionDescription &action = _userActions[index];
	action.id = _nextUserActionId++;
	action.text = text;
	action.script = script;
	return action.id;
}

/*!
	\param id of action
*/
void EntityAI::RemoveUserAction(int id)
{
	for (int i=0; i<_userActions.Size(); i++)
	{
		if (_userActions[i].id == id)
		{
			_userActions.Delete(i);
			return;
		}
	}
}

/*!
	\param id of action
*/
const UserActionDescription *EntityAI::FindUserAction(int id) const
{
	for (int i=0; i<_userActions.Size(); i++)
	{
		if (_userActions[i].id == id) return &_userActions[i];
	}
	return NULL;
}

DEFINE_ENUM(EntityEvent,EE,ENTITY_EVENT_ENUM)

int EntityAI::AddEventHandler(EntityEvent event, RString expression)
{
	return _eventHandlers[event].Add(expression);
}
void EntityAI::RemoveEventHandler(EntityEvent event, int handle)
{
	_eventHandlers[event].Delete(handle);
}
void EntityAI::ClearEventHandlers(EntityEvent event)
{
	_eventHandlers[event].Clear();
}

const AutoArray<RString> &EntityAI::GetEventHandlers(EntityEvent event) const
{
	return _eventHandlers[event];
}

class GameArrayTypeRef
{
	const GameArrayType &_ref;
	public:
	GameArrayTypeRef(const GameArrayType &ref):_ref(ref){}
	operator const GameArrayType &() const {return _ref;}
	operator GameValue() const {return GameValue(_ref);}
};

bool EntityAI::IsEventHandler(EntityEvent event) const
{
	const AutoArray<RString> &handlers = GetEventHandlers(event);
	if (handlers.Size()>0) return true;
	RString handler = GetType()->_eventHandlers[event];
	if (handler.GetLength()>0) return true;
	return false;
}

/*!
\patch 1.82 Date 8/20/2002 by Ondra
- New: Addons and scripting: Customizable event handlers for events:
	Killed, Hit, Dammaged, GetIn, GetOut, Init,
	Engine, Gear, Fuel, Fired, IncomingMissile.
\patch 1.93 Date 9/10/2003 by Ondra
- Fixed: Possible crash during removeAllEventHandlers. 
\patch 1.97 Date 4/1/2004 by Jirka
- Added: Event handler AnimChanged
*/


void EntityAI::OnEvent(EntityEvent event, const GameValue &pars)
{
	const AutoArray<RString> &handlers = GetEventHandlers(event);
	for (int i=0; i<handlers.Size(); i++)
	{
		GameVarSpace local;
		GameState *gstate = GWorld->GetGameState();
		gstate->BeginContext(&local);
		gstate->VarSetLocal("_this",pars,true);
    RString handler = handlers[i];
		gstate->Execute(handler);
		gstate->EndContext();
	}
	{
		// execute type based event handler
		RString handler = GetType()->_eventHandlers[event];
		GameVarSpace local;
		GameState *gstate = GWorld->GetGameState();
		gstate->BeginContext(&local);
		gstate->VarSetLocal("_this",pars,true);
		gstate->Execute(handler);
		gstate->EndContext();
	}
}

void EntityAI::OnEvent(EntityEvent event, bool par1)
{
	if (!IsEventHandler(event)) return;
	GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
	GameArrayType &arguments = value;
	arguments.Add(GameValueExt(this));
	arguments.Add(GameValue(par1));
	OnEvent(event,value);
}

void EntityAI::OnEvent(EntityEvent event, RString par1)
{
	if (!IsEventHandler(event)) return;
	GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
	GameArrayType &arguments = value;
	arguments.Add(GameValueExt(this));
	arguments.Add(GameValue(par1));
	OnEvent(event,value);
}

void EntityAI::OnEvent(EntityEvent event, RString par1, EntityAI *par2)
{
	if (!IsEventHandler(event)) return;
	GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
	GameArrayType &arguments = value;
	arguments.Add(GameValueExt(this));
	arguments.Add(GameValue(par1));
	arguments.Add(GameValueExt(par2));
	OnEvent(event,value);
}

void EntityAI::OnEvent(EntityEvent event, RString par1, float par2)
{
	if (!IsEventHandler(event)) return;
	GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
	GameArrayType &arguments = value;
	arguments.Add(GameValueExt(this));
	arguments.Add(GameValue(par1));
	arguments.Add(GameValue(par2));
	OnEvent(event,value);
}

void EntityAI::OnEvent(EntityEvent event)
{
	if (!IsEventHandler(event)) return;
	GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
	GameArrayType &arguments = value;
	arguments.Add(GameValueExt(this));
	OnEvent(event,value);
}


void EntityAI::OnEvent(EntityEvent event, EntityAI *par1, float par2)
{
	if (!IsEventHandler(event)) return;
	GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
	GameArrayType &arguments = value;
	arguments.Add(GameValueExt(this));
	arguments.Add(GameValueExt(par1));
	arguments.Add(GameValue(par2));
	OnEvent(event,value);
}


void EntityAI::OnEvent(EntityEvent event, EntityAI *par1)
{
	if (!IsEventHandler(event)) return;
	GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
	GameArrayType &arguments = value;
	arguments.Add(GameValueExt(this));
	arguments.Add(GameValueExt(par1));
	OnEvent(event,value);
}

const float ManLockRadius=1.0;

void EntityAI::PerformLock()
{
	if( _locked && !_tempLocked )
	{
		float manRadius = _lockedSoldier ? ManLockRadius : floatMax(4,_lockedRadius*0.6);
		_locker->LockPosition(_lockedBeg, _lockedRadius, _lockedSoldier, GetShape()->GeometrySphere());
		_locker->LockPositionMan(_lockedBeg, manRadius);
		_tempLocked=true;
	}
}

void EntityAI::PerformUnlock()
{
	if( _locked && _tempLocked )
	{
		float manRadius = _lockedSoldier ? ManLockRadius : floatMax(4,_lockedRadius*0.6);
		_locker->UnlockPositionMan(_lockedBeg, manRadius);
		_locker->UnlockPosition(_lockedBeg, _lockedRadius, _lockedSoldier);
		_tempLocked=false;
	}
}


void EntityAI::LockPosition()
{
	if( _static ) return; // no lock for buildings ...
	// helper functions for lock/unlock
	// lock current position and area around it
	float radius=CollisionSize()*1.5+2.5;
	saturateMax(radius,4);
	_locked=true;
	// make some shift aside - we prefer having right side free
	Vector3Val lockPos=AimingPosition();
	if( _tempLocked )
	{
		// check if already locked - no change
		float dist2=(lockPos-_lockedBeg).SquareSizeXZ();
		if( dist2<Square(0.25f) ) return;
		PerformUnlock();
	}
	// this also helps getting in (drivers gets in from the left)
	_lockedBeg=lockPos;
	_lockedRadius=radius;
	_lockedSoldier = CommanderUnit() ? CommanderUnit()->GetPerson() == this : false;
	PerformLock();
	//_locker->LockPosition(_lockedBeg, _lockedRadius, _lockedSoldier, CollisionSize());
	//if( _lockedSoldier ) _locker->LockPositionMan(_lockedBeg, ManLockRadius);
	//_tempLocked=true;
}

void EntityAI::UnlockPosition()
{
	// lock some area around vehicle
	PerformUnlock();
	_locked=false;
}

/* \note
This function is currently never used
*/

bool EntityAI::HasPriorityOver( EntityAI *who ) const
{
	if( !who->CommanderUnit() ) return false; // he cannot dodge
	if( !CommanderUnit() ) return true; // I cannot dodge
	// first rule: if I am at his back and he is in front of me, he has priority
	bool iAmInFrontOfHim=who->PositionWorldToModel(Position()).Z()>0;
	bool heIsInFrontOfMe=PositionWorldToModel(who->Position()).Z()>0;
	// he has priority
	if( iAmInFrontOfHim && !heIsInFrontOfMe ) return true;
	if( !iAmInFrontOfHim && heIsInFrontOfMe ) return false;
	// uncertain: check priority from the right
	return false;
}

void EntityAI::AvoidCollision
(
	float deltaT, float &speedWanted, float &headChange
)
{
	AIUnit *unit = PilotUnit();
	if( !unit ) return;

	const float neutralAside = 0;

	AISubgroup *mySubgrp = unit->GetSubgroup();
	_avoidAsideWanted=neutralAside;

	if( mySubgrp && mySubgrp->GetMode()==AISubgroup::DirectGo ) return;
	// if we are stopped, do not try to avoid
	// TODO: some vehicles (esp. men) should go out of way of tanks
	const float maxSpeedStopped=0.05;
	if( fabs(speedWanted)<=maxSpeedStopped ) return;
	// avoid collisions
	float mySize=CollisionSize(); // assume vehicle is not round
	float mySpeedSize=fabs(ModelSpeed()[2]);
	float maxSpeed=GetType()->GetMaxSpeedMs();
	float myBrakeDist=Square(mySpeedSize)*0.2;
	float myBrakeTime=mySpeedSize*0.05;
	float maxDist=floatMax(GetPrecision()*4,5)+myBrakeDist*1.3;
	// check if we are on collision course
	VehicleCollisionBuffer ret;
	float gapFactor=Interpolativ(mySpeedSize,maxSpeed*0.5,maxSpeed,0.5,1);
	float gap=mySize*gapFactor;
	float maxTime=1.5+myBrakeTime*1.3;

	//LogF("%s: Predict gap %.1f, maxDist %.1f",(const char *)GetDebugName(),gap,maxDist);

	GLOB_LAND->PredictCollision(ret,this,maxTime,gap,maxDist);
	if( ret.Size()<=0 ) return;

	bool iAmMan=unit->IsSoldier();
	bool iAmHeavy = GetMass()>5000;

	AIGroup *myGroup = unit->GetGroup();
	AICenter *myCenter = myGroup ? myGroup->GetCenter() : NULL;

	// precalculate
	float invMaxSpeed=1/maxSpeed;

	saturate(speedWanted,-maxSpeed,maxSpeed);
	Vector3 mySpeed=Direction()*speedWanted;

	float maxAvoid=GetPrecision()*0.5;
	saturate(maxAvoid,1,4); // 4m is enough to avoid on any road with any vehicle
	float wantAvoid=0;

	for( int i=0; i<ret.Size(); i++ )
	{
		const VehicleCollision &info=ret[i];
		const EntityAI *who=info.who;

		// something is near
		// some vehicle
		// determine who should slow down
		// if who is in front of us, slow down to his speed
		// if we are heave and he is enemy soldier, ignore him
		if (iAmHeavy)
		{
			AIUnit *whoUnit = who->CommanderUnit();
			if (whoUnit)
			{
				AIGroup *whoGroup = whoUnit->GetGroup();
				if (whoGroup)
				{
					AICenter *whoCenter = whoGroup->GetCenter();
					if (myCenter->IsEnemy(whoCenter->GetSide()))
					{
						continue;
					}
				}
			}

		}

#if _ENABLE_CHEATS
		if( CHECK_DIAG(DECombat) )
		{
			Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),-1);
			obj->SetPosition(info.pos);
			Color color(1,1,0,0.3);
			//color=Color(1,-1,0)*danger+Color(0,1,0);
			obj->SetScale(info.distance<gap ? 1 : 0.3);
			obj->SetConstantColor(PackedColor(color));
			GLandscape->ShowObject(obj);
		}
#endif

		if( info.distance<gap )
		{
			// check relative speed and position

			#if DIAG_COL
			if( this==GWorld->CameraOn() )
			{
				LogF("%s vs %s",(const char *)GetDebugName(),(const char *)who->GetDebugName());
			}
			#endif

			Vector3Val v=mySpeed-who->Speed();
			Vector3Val r=who->Position()-Position();

			// projection of v to r is how much we are getting nearer
			Vector3 nearerV=v.Project(r);
			// nearer is oriented relative speed size
			float nearer=nearerV*r.Normalized();

			// actual object distance
			float distance=info.distance;
			float isNear;
			if( distance<gap )
			{
				isNear=Interpolativ(distance,0,gap,-maxSpeed*0.5,-maxSpeed*0.1);
			}
			else
			{
				isNear=Interpolativ(distance,gap,gap+myBrakeDist,-maxSpeed*0.1,maxSpeed);
			}

			float dirFactor=nearer*invMaxSpeed;

			#if DIAG_COL
			if( this==GWorld->CameraOn() )
			{
				LogF("  nearer %.3f, dirFactor %.3f",nearer,dirFactor);
			}
			#endif
			if( dirFactor<=-0.1 )
			{
				#if DIAG_COL
				if( this==GWorld->CameraOn() )
				{
					LogF("  going away");
				}
				#endif
				continue; // safe: going away
			}

			if( Airborne() )
			{
				// check if obstacle is dangerous to airborne vehicle
				float maxY=who->Position().Y()+who->GetShape()->Max().Y();
				if( maxY<Position().Y()-10 ) continue;
			}

			if( who->Static() )
			{
				float limSpeed=isNear;
				// static object - should be considered in path planner
				saturateMax(limSpeed,maxSpeed*0.5);
				saturate(speedWanted,-limSpeed,+limSpeed);

				#if DIAG_COL
				if( this==GWorld->CameraOn() )
				{
					LogF
					(
						"  Obj %.1f, idist %.1f, dist %.1f, gap %.1f, brakeDist %.1f",
						speedWanted*3.6,info.distance,distance,gap,myBrakeDist
					);
				}
				#endif

			}
			else
			{
				Vector3Val hisSpeedR=DirectionWorldToModel(who->Speed());
				float hisSpeedRZ=hisSpeedR.Z();

				// check collision on predicted position

				float t=info.time;
				Vector3 myPos=Position()+t*Speed();
				Vector3 whoPos=who->Position()+t*who->Speed();

				FrameBase myTrans,whoTrans;
				myTrans.SetTransform(Transform());
				myTrans.SetPosition(myPos);

				whoTrans.SetTransform(who->Transform());
				whoTrans.SetPosition(whoPos);
				// enlarge slightly - be carefull
				myTrans.SetScale(1.1);
				whoTrans.SetScale(1.1);

				bool dangerous=true;
				AIUnit *whoUnit = who->CommanderUnit();
				if( whoUnit && !whoUnit->IsFreeSoldier() )
				{
					// do not check collision with men
					// assume collision will happen
					CollisionBuffer objCol;
					Intersect(objCol,const_cast<EntityAI *>(who),myTrans,whoTrans);
					dangerous=( objCol.Size()>0 );
				}

				bool sameSubgroup=false;
				if( whoUnit )
				{
					AISubgroup *whoSubgroup=whoUnit->GetSubgroup();
					sameSubgroup=( whoSubgroup==mySubgrp );
				}


#if _ENABLE_CHEATS
				if( CHECK_DIAG(DECombat) )
				{
					Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),-1);
					float scale=(maxTime-info.time)/maxTime*2;
					//float scale=
					saturate(scale,0.2,2);
					Color color;
					obj->SetPosition(info.pos);
					float danger=-hisSpeedRZ*(1.0/10);
					saturate(danger,0,1);
					color=Color(1,0,dangerous)*danger+Color(0,1,dangerous)*(1-danger);
					//color=Color(1,-1,0)*danger+Color(0,1,0);
					obj->SetScale(scale);
					obj->SetConstantColor(PackedColor(color));
					GLandscape->ShowObject(obj);

					GScene->DrawCollisionStar(myPos,0.2);
					GScene->DrawCollisionStar(whoPos,0.2);
				}
#endif	

				// check direction of target
				bool overtaken = false;
				bool overtaking = false;
				float whoSpeedD=who->ObjectSpeed()*Direction();
				float mySpeedD=Speed()*Direction();

				if( !sameSubgroup )
				{
					// no overtaking/avoiding in same group
					float avoidThis = (who->CollisionSize()+CollisionSize())*0.6;
					Vector3 whoRelPos=PositionWorldToModel(who->Position());
					if( fabs(whoRelPos.X())>20 && fabs(whoRelPos.X())>whoRelPos.Z()*0.5 )
					{
						// aside of our direction - do not avoid
						avoidThis = 0;
					}
					else if( Direction()*who->Direction()<-0.2 )
					{
						// opposite direction - both should avoid
					}
					else if( whoRelPos.Z()<0 && whoRelPos.X()<0 )
					{
						// he's overtaking us
						overtaken = true;
					}
					else if( whoRelPos.Z()>0 && whoRelPos.X()>0 )
					{
						// we're overtaking him
						overtaking = true;
					}
					else if( mySpeedD>5 )
					{
						// I am moving forward
						overtaking = whoSpeedD<mySpeedD;
						overtaken = !overtaking;
					}
					
					if (!dangerous)
					{
						avoidThis *= 0.5;
						saturate(avoidThis,-maxAvoid*0.5,+maxAvoid*0.5);
						#if DIAG_COL
						if( this==GWorld->CameraOn() )
						{
							LogF("  not dangerous");
						}
						#endif
					}

					if (overtaking)
					{
						// overtake - same direction
						if( wantAvoid<=0 ) wantAvoid -= avoidThis;
						#if DIAG_COL
						if( this==GWorld->CameraOn() )
						{
							LogF("  overtaking %.2f",wantAvoid);
						}
						#endif
					}
					else
					{
						// avoid - different direction
						if( wantAvoid>=0 ) wantAvoid += avoidThis;
						#if DIAG_COL
						if( this==GWorld->CameraOn() )
						{
							LogF("  overtaken %.2f",wantAvoid);
						}
						#endif
					}
				} //if( mySubgrp!=whoSubgrp )

				// check if he is behind us

				Vector3 whoRelPos=PositionWorldToModel(who->Position());
				if( whoRelPos.Z()<0 )
				{
					// he's behind us
					// do not brake - it would be only worse
					if( speedWanted>0 )
					{
						#if DIAG_COL
						if( this==GWorld->CameraOn() )
						{
							LogF("  behind us");
						}
						#endif
						continue;
					}
				}
				else if( hisSpeedRZ>maxSpeed*0.1 )
				{
					// he is is front of us - going forward
					// we can limit speed to his speed
					float curDist=who->Position().Distance(Position());
					float maxNear=mySize*2+myBrakeDist*0.6;
					float curNear=curDist/maxNear-1;
					if( !dangerous )
					{
						// he is not dangerous - move
						saturateMax(curNear,0.1*maxSpeed);
					}
					saturateMin(isNear,curNear);

					//LogF("%s: in front of us %.2f",(const char *)GetDebugName(),curNear);
				}

				if( who->Speed().SquareSize()<Square(2) )
				{
					// he is stopped - we have to move
					if( !sameSubgroup )
					{
						saturateMax(isNear,0.1*maxSpeed);
					}
				}

				{
					float limSpeed=isNear+hisSpeedRZ;
					// check relative position of collision
					//Vector3Val relPos=PositionWorldToModel(pos);
					float carelessSpeed=hisSpeedRZ+maxSpeed*0.3;
					//saturateMax(limSpeed,GetType()->GetMaxSpeedMs()*0.05);
					//saturateMax(limSpeed,0);
					limSpeed=Interpolativ(dirFactor,0,0.1,carelessSpeed,limSpeed);
					// moving vehicle very near - slow down
					if( !whoUnit )
					{
						// vehicle empty - should be considered in path planner
						limSpeed=carelessSpeed;
					}
					else
					{
						float limSpeedMin = 0;
						if( iAmMan && whoUnit->IsSoldier() )
						{
							// man vs. man: never stop
							limSpeedMin = 1;
						}
						else if( who->IsOnRoadMoving(3) && IsOnRoadMoving(3) )
						{
							// if both are moving and on road, there is no need to stop
							if (overtaking)
							{
								limSpeedMin = hisSpeedRZ+maxSpeed*0.2;
							}
							else
							{
								limSpeedMin = dangerous ? 6 : 12;
							}
							// if we are heading same direction
							// we will overtake and should not brake under his speed + some reserve
						}
						else
						{
							// stop if neccessary
							limSpeedMin = dangerous ? 0 : 6;
						}
						saturateMax(limSpeed,limSpeedMin);
					}

					// keep braking for some time
					if( Glob.time>_avoidSpeedTime || _avoidSpeed>limSpeed )
					{
						_avoidSpeedTime=Glob.time+1;
						_avoidSpeed=limSpeed;
						#if DIAG_COL
						if( this==GWorld->CameraOn() )
						{
							LogF("  limit speed %.1f",limSpeed);
						}
						#endif
					}
 					saturate(speedWanted,-limSpeed,+limSpeed);
				}
				
				
				#if DIAG_COL
				if( this==GWorld->CameraOn() )
				{
					LogF
					(
						"  Col %.1f dist %.1f, gap %.1f, brakeDist %.1f, dir %.2f",
						speedWanted*3.6,distance,gap,myBrakeDist,dirFactor
					);
				}
				#endif

			}
		} // if( info.distance<gap )
		else
		{
			#if DIAG_COL
			if( this==GWorld->CameraOn() )
			{
				LogF
				(
					"%s vs %s ignored (%.2f>%.2f)",
					(const char *)GetDebugName(),(const char *)who->GetDebugName(),
					info.distance,gap
				);
			}
			#endif
		}
	} // for(i)

	saturate(wantAvoid,-maxAvoid,+maxAvoid);
	_avoidAsideWanted=wantAvoid+neutralAside;

	if( fabs(speedWanted)<maxSpeedStopped || _objectContact )
	{
		CreateFreshPlan();
	}
}

void EntityAI::CreateFreshPlan()
{
	// do not report in DirectGo mode
	AIUnit *unit=PilotUnit();
	if( !unit ) return;
	if( unit->GetSubgroup()->GetMode()==AISubgroup::DirectGo ) return;
	Path &path=unit->GetPath();
	if( path.Size()>=2 )
	{
		// check if we have recent path
		if( path.GetSearchTime()<Glob.time-5 )
		{
			// replan
			if( unit->IsSubgroupLeader() )
			{
				unit->SendAnswer(AI::StepTimeOut);
			}
			else
			{
				unit->ForceReplan();
			}
		}
	}
}


Vector3 EntityAI::SteerPoint( float spdTime, float costTime )
{
	// calculate point on trajectory in time (relative in sec)
	// estimate position after time
	AIUnit *unit=PilotUnit();
	if( !unit ) return Position();
	const Path &path=unit->GetPath();
	// no path - no steering
	if( path.Size()<2 ) return Position()+Direction();
	Vector3Val sPos=Position()+Speed()*spdTime+0.5*spdTime*spdTime*_acceleration;

	float cost=path.CostAtPos(sPos)+costTime;

	Vector3Val pos=path.PosAtCost(cost,Position());

	return pos;
}

float EntityAI::GetPathCost( const GeographyInfo &info, float dist ) const
{
	float cost=GetFieldCost(info)*GetCost(info);
	if( cost>GET_UNACCESSIBLE ) cost=GetType()->GetMinCost()*2;
	return cost*dist;
}

void EntityAI::FillPathCost( Path &path ) const
{
	if( path.Size()<=0 ) return;

	Vector3 lastPos = path[0]._pos;
	path[0]._cost=0;

	float sumCost=0;
	for (int i=1; i<path.Size(); i++)
	{
		Vector3 pos = path[i]._pos;
		float dist=pos.Distance(lastPos);

		// update cost info if necessary
		int cx=toIntFloor(lastPos.X()*InvLandGrid);
		int cz=toIntFloor(lastPos.Z()*InvLandGrid);
		GeographyInfo geogr=GLOB_LAND->GetGeography(cx,cz);

		float cost=GetPathCost(geogr,dist);

		sumCost += cost;

		path[i]._cost = sumCost;

		lastPos=pos;
	}
	
}


float EntityAI::VisibleMovement() const
{
	float camouflage=GetType()->_camouflage;
	float vis=camouflage;

	saturateMax(vis,_shootVisible); // firing target is better visible
	float rSpeed=fabs(ModelSpeed().Z())*3;
	if( rSpeed>vis*GetRadius() )
	{ // moving target is better visible
		float relSpeed=rSpeed/GetRadius();
		saturateMin(relSpeed,8);
		saturateMax(vis,relSpeed*camouflage);
	}
	return vis;
}

float EntityAI::VisibleLights() const
{
	return 0;
}

/*!
\patch 1.28 Date 10/19/2001 by Ondra
- Fixed: Firing weapons was heard not far enough by AI.
*/
float EntityAI::Audible() const
{
	float aud =EngineIsOn() ? 3 : 0.1;
	float rSpeed=fabs(ModelSpeed().Z());
	if( rSpeed>GetRadius()*0.5 )
	{ // moving target is better visible
		float relSpeed=rSpeed*0.3/GetRadius();
		saturateMin(relSpeed,2);
		aud += relSpeed;
	}
	aud *= GetType()->GetAudible();
	saturateMax(aud,_shootAudible); // firing target is better audible
	return aud;

}

float EntityAI::GetHidden() const
{
	return 1;
}

float EntityAI::VisibleFire() const {return _shootVisible;}
float EntityAI::AudibleFire() const {return _shootAudible;}

TargetType *EntityAI::FiredAt() const {return _shootTarget;}

float EntityAI::GetExplosives() const
{
	return GetAmmoHit()*0.015f; // assume in-place explosion is much less effective
}

inline float CalcHitDammage( float distance2, float valRange2 )
{
	if( distance2<=valRange2 ) return 1;
	else return valRange2*valRange2/(distance2*distance2);
	//else return valRange2*valRange2*valRange2/(distance2*distance2*distance2);
}

RString EntityAI::HitpointName(int i) const
{
	const HitPointList &hitpoints=GetType()->GetHitPoints();
	if (i>=hitpoints.Size()) return RString();
	if (i<0) return RString();
	int sel = hitpoints[i]->GetSelection();
	LODShape *lShape = GetShape();
	if (!lShape) return RString();

	// FIX: hitpoint with no selection - avoid crash
	if (sel < 0)
	{
		ErrF("%s: Hitpoint %d is in no selection", (const char *)lShape->GetName(), i);
		return RString();
	}
	
	Shape *hits = lShape->HitpointsLevel();
	if (!hits) return RString();
	const NamedSelection &nsel = hits->NamedSel(sel);
	return nsel.GetName();
}

float EntityAI::DirectLocalHit(int component, float val)
{
#if _ENABLE_CHEATS
	if (!_allowDammage) return 0;
#endif
	if (component<0) return 1;
	// scan for corresponding hitpoint
	const HitPointList &hitpoints=GetType()->GetHitPoints();
	for( int i=0; i<hitpoints.Size(); i++ )
	{
		const HitPoint &hit=*hitpoints[i];
		if (!hit.IsConnectedCC(component)) continue;

		float hitVal=val*hit.GetInvArmor(); // TODO: avoid division
		//LogF("Hit %g, val %g, armor %g",hitVal,val,hit.GetArmor());

		float oldHit=_hit[i];
		float newHit=oldHit + hitVal;
		saturateMin(newHit,1);
		_hit[i]=newHit;

		#if 0
			int index=hit.GetSelection();
			Shape *hitShape=_shape->HitpointsLevel();
			if (index>=0 && hitShape)
			{
				const NamedSelection &sel=hitShape->NamedSel(index);
				LogF("Direct local hit on %s: %.2f",sel.Name(),newHit);
			}
			else
			{
				LogF("Direct local hit on hitpoint %d: %.2f",i,newHit);
			}
		#endif

		return hit.GetPassThrough();
		
	}
	return 1;
}

void EntityAI::ChangeHit( int i, float newHit)
{
	float oldHit=_hit[i];
	saturateMin(newHit,1);
	_hit[i]=newHit;
	if
	(
		(oldHit < 0.5 && newHit >= 0.5) ||
		(oldHit < 0.7 && newHit >= 0.7) ||
		(oldHit < 0.9 && newHit >= 0.9)
	)
	{
		ShowDammage(i);
	}
}

float EntityAI::LocalHit( Vector3Par pos, float val, float valRange )
{
#if _ENABLE_CHEATS
	if (!_allowDammage) return 0;
#endif
	// scan all hitpoints and dammage them
	Shape *hitShape=_shape->HitpointsLevel();
	if( !hitShape ) return 1;

	/*
	LogF
	(
		"%s hit (%.1f,%.1f,%.1f) (val %.2f,%.2f)",
		(const char *)GetDebugName(),
		pos[0],pos[1],pos[2],val,valRange
	);
	*/

	// TODO: rename to FindHitpointsLevel
	Animate(_shape->FindHitpoints());

	if (valRange<0)
	{
		// note: this change of local dammage model was done
		// to improve hit locality for soldiers
		// time showed in is suitable for all vehicles
		valRange *= 0.25; // smaller area around direct hit
		//val *= 2; // but stronger effect
	}

	float valRange2=Square(valRange);
	const HitPointList &hitpoints=GetType()->GetHitPoints();
	for( int i=0; i<hitpoints.Size(); i++ )
	{
		const HitPoint &hit=*hitpoints[i];
		int index=hit.GetSelection();
		if( index>=0 )
		{
			const NamedSelection &sel=hitShape->NamedSel(index);
			for( int j=0; j<sel.Size(); j++ )
			{
				int pIndex=sel[j];
				Vector3Val hitPt=hitShape->Pos(pIndex);
				float distance2=hitPt.Distance2(pos);
				float dammage=val*CalcHitDammage(distance2,valRange2);
				if( dammage>1e-4 )
				{
					float hitVal=dammage/hit.GetArmor(); // TODO: avoid division
					ChangeHit(i,_hit[i] + hitVal);
				}
			}
		}
	}
	Deanimate(_shape->FindHitpoints());
	return GetType()->GetStructuralDammageCoef();
}

static bool FindCeaseFireInRadio(RadioChannel &radio, AIUnit *to)
{
	// check radio channel
	int index = INT_MAX;
	while (true)
	{
		RadioMessage *msg = radio.FindPrevMessage(RMTCeaseFire, index);
		if (!msg) break;
		Assert( dynamic_cast<RadioMessageCeaseFire *>(msg) );
		RadioMessageCeaseFire *msgTyped = static_cast<RadioMessageCeaseFire *>(msg);
		Assert(msgTyped);
		if (msgTyped->GetTo() == to) return true;
	}

	{
		RadioMessage *msg = radio.GetActualMessage();
		if (msg && msg->GetType() == RMTCeaseFire)
		{
			Assert( dynamic_cast<RadioMessageCeaseFire *>(msg) );
			RadioMessageCeaseFire *msgTyped = static_cast<RadioMessageCeaseFire *>(msg);
			Assert(msgTyped);
			if (msgTyped->GetTo() == to) return true;
		}
	}

	return false;
}

void EntityAI::ShowDammage(int part)
{
	if (part<0 || part>=_hit.Size()) return;
	RString name = HitpointName(part);
	OnEvent(EEDammaged,name,_hit[part]);
}

void EntityAI::HitBy( EntityAI *killer, float howMuch, RString ammo)
{
#if _ENABLE_VBS
	if (IsLocal() && howMuch >= 0.05 && killer && !_isDead)
	{
		GStats.OnVehicleDamaged(this, killer, howMuch, ammo);
		GetNetworkManager().OnVehicleDamaged(this, killer, howMuch, ammo);
	}
#endif
	if (IsLocal() && howMuch >= 0.05 && !_isDead)
	{
		OnEvent(EEHit,killer,howMuch);
	}
	base::HitBy(killer,howMuch,ammo);
	AIGroup *g=GetGroup();
	if( g && killer )
	{
		AIUnit *killerUnit = killer->CommanderUnit();
		if (!killerUnit) return;
		AIGroup *killerGroup = killerUnit->GetGroup();
		// if we are dammaged by other unit of the same group,  do not react
		if (!killerGroup) return;
		AICenter *killerCenter = killerGroup->GetCenter();
		if (!killerCenter) return;
		if (killerGroup == g && !(CommanderUnit() && CommanderUnit()->GetPerson()->GetExperience() < ExperienceRenegadeLimit))
		{
			AIUnit *sender = CommanderUnit();
			if
			(
				howMuch >= 0.05 &&
				killer != this && killerUnit->GetVehicleIn() != this &&
				(!sender || sender->GetVehicleIn() != killer)
			)
			{
				Log
				(
					"Friendly fire (in group): %s by %s (%s)",
					(const char *)GetDebugName(),
					(const char *)killer->GetDebugName(),
					(const char *)killer->GetType()->GetName()
				);
				RadioChannel &radio = g->GetRadio();
				if (!FindCeaseFireInRadio(radio, killerUnit))
				{
					if (!sender || sender->GetPerson()->IsDammageDestroyed())
						sender = g->Leader();
					if (sender && !sender->GetPerson()->IsDammageDestroyed() && !sender->IsAnyPlayer())
					{
						radio.Transmit
						(
							new RadioMessageCeaseFire(sender, killerUnit, true),
							killerCenter->GetLanguage()
						);
					}
				}
			}
			return;
		}
		// reveal killer (at random position around us)
		Vector3 pos=Position()+Vector3
		(
			GRandGen.RandomValue()*200-100,
			GRandGen.RandomValue()*20-10,
			GRandGen.RandomValue()*200-100
		);
		float accuracy = 0.1;
		float sideAcc = 1.5;
		float delay = 0.8f*GetInvAbility();
		float delayOtherUnits = 15;
		// reveal side of killer
		// reporting units knows almost immediatelly about the target
		// other units will notice it after a short delay
		g->AddTarget
		(
			killer,accuracy,sideAcc,delayOtherUnits,&pos,
			CommanderUnit(),delay
		);

		// do not disclose when killer is friendly
		AICenter *gCenter = g->GetCenter();
		if (!gCenter) return;
		if (gCenter->IsEnemy(killerCenter->GetSide()))
		{
			AIUnit *vehBrain=CommanderUnit();
			if( vehBrain )
			{
				vehBrain->Disclose();
				if (PilotUnit() && PilotUnit() != vehBrain)
					PilotUnit()->Disclose();
				if (GunnerUnit() && GunnerUnit() != vehBrain)
					GunnerUnit()->Disclose();
			}
		}
		else if (gCenter == killerCenter && !(CommanderUnit() && CommanderUnit()->GetPerson()->GetExperience() < ExperienceRenegadeLimit))
		{
			AIUnit *sender = CommanderUnit();
			if
			(
				howMuch >= 0.05 &&
				killer != this && killerUnit->GetVehicleIn() != this &&
				(!sender || sender->GetVehicleIn() != killer)
			)
			{
				Log
				(
					"Friendly fire: %s by %s (%s), dist %.1f",
					(const char *)GetDebugName(),
					(const char *)killer->GetDebugName(),
					(const char *)killer->GetType()->GetName(),
					killer->Position().Distance(Position())
				);
				RadioChannel &radio = gCenter->GetRadio();
				if (!FindCeaseFireInRadio(radio, killerUnit))
				{
					if (!sender || sender->GetPerson()->IsDammageDestroyed()) sender = g->Leader();
					if (sender && !sender->GetPerson()->IsDammageDestroyed() && !sender->IsAnyPlayer())
					{
						radio.Transmit
						(
							new RadioMessageCeaseFire(sender, killerUnit, false),
							killerCenter->GetLanguage()
						);
					}
				}
			}
		}
	}

}

void EntityAI::DoDammage
(
	EntityAI *owner, Vector3Par pos,
	float val, float valRange, RString ammo
)
{
	if (owner)
	{
		_lastDammage = owner;
		_lastDammageTime = Glob.time;
	}

	base::DoDammage(owner, pos, val, valRange, ammo);
}

bool EntityAI::IsDammageDestroyed() const
{
	if (_isDead) return true;
	return base::IsDammageDestroyed();
}

void EntityAI::Repair( float ammount )
{
	//if (ammount>0)
	{
		// repair all hitpoints
		for( int i=0; i<_hit.Size(); i++ ) _hit[i]=0;
	}
	base::Repair(ammount);
}

void EntityAI::SetDammage(float dammage)
{
	for (int i=0; i<_hit.Size(); i++)
		_hit[i] = dammage;
	bool doDammage = GetTotalDammage() > dammage;
	base::SetDammage(dammage);
	if (doDammage) ReactToDammage();
	_isDead = false;
	_isDead = IsDammageDestroyed();
}

void EntityAI::ReactToDammage()
{
}

void EntityAI::Destroy
(
	EntityAI *killer, float overkill, float minExp, float maxExp
)
{
	if (IsLocal())
	{
		OnEvent(EEKilled,killer);
		GStats.OnVehicleDestroyed(this, killer);
		GetNetworkManager().OnVehicleDestroyed(this, killer);
	}
	base::Destroy(killer,overkill,minExp,maxExp);
	if( killer )
	{
		// use Entity member to get original target side
		// all dead bodies are considered civilian
		TargetSide origSide = Entity::GetTargetSide();

		// increase killer's experience
		AIUnit *kBrain=killer->CommanderUnit();
		if( kBrain )
		{
			kBrain->IncreaseExperience(*GetType(),origSide);
			// send radio message
			AIGroup *killerGroup = kBrain->GetGroup();
			if (killerGroup && killerGroup->GetCenter()->IsEnemy(origSide))
			{
				// find corresponding target
				Target *tar = killerGroup->FindTargetAll(this);
				if (tar)
				{
					// mark killer
					// when destroyed will be set, it will be marked for reporting
					tar->idKiller = killer;
				}
			}
		}
		if (killer->GunnerUnit() && killer->GunnerUnit() != kBrain)
			killer->GunnerUnit()->IncreaseExperience(*GetType(),origSide);
		if (killer->PilotUnit() && killer->PilotUnit() != kBrain)
			killer->PilotUnit()->IncreaseExperience(*GetType(),origSide);
	}
}

/*!
\param creator player id of client where magazine was created
\param id unique id of magazine on given client
\return pointer to magazine or NULL when not found
*/
const Magazine *EntityAI::FindMagazine(int creator, int id) const
{
	for (int i=0; i<NMagazines(); i++)
	{
		const Magazine *magazine = GetMagazine(i);
		if (!magazine) continue;
		if (magazine->_creator == creator && magazine->_id == id) return magazine;
	}
	return NULL;
}

const Magazine *EntityAI::FindMagazine(RString name) const
{
	Ref<MagazineType> type = MagazineTypes.New(name);
	const Magazine *magazine = NULL;
	int ammo = 0;
	for (int i=0; i<NMagazines(); i++)
	{
		const Magazine *m = GetMagazine(i);
		if (!m) continue;
		if (m->_type != type) continue;
		if (m->_ammo > ammo && !IsMagazineUsed(m))
		{
			magazine = m;
			ammo = m->_ammo;
		}
	}
	return magazine;
}

/*!
\param magazine checked magazine
\return true if magazine is used (loaded) in some weapon
*/
bool EntityAI::IsMagazineUsed(const Magazine *magazine) const
{
	if (magazine->_ammo <= 0) return false;

	for (int i=0; i<NMagazineSlots(); i++)
	{
		if (GetMagazineSlot(i)._magazine == magazine) return true;
	}
	return false;
}

/*!
\param s index of slot reload to
\param m index of reloading magazine
\param afterAnimation true if reload immediatelly (no delay)
*/
bool EntityAI::ReloadMagazineTimed(int s, int m, bool afterAnimation)
{
	if (m >= NMagazines() || s >= NMagazineSlots()) return false;

	Magazine *magazine = GetMagazine(m);
	if (!magazine) return false;

	#if 0
	LogF
	(
		"%s: ReloadMagazineTimed slot %d, magazine %s",
		(const char *)GetDebugName(),s,
		(const char *)magazine->_type->GetName()
	);
	#endif

	MagazineSlot &slot = _magazineSlots[s];
  Magazine *oldMagazine = slot._magazine;
  if (oldMagazine == magazine)
  {
    // FIX: do not reload magazine with itself
    return false;
  }

  const MuzzleType *muzzle = slot._muzzle;
  if (!muzzle->CanUse(magazine->_type))
  {
    RptF
      (
      "Cannot use magazine %s in muzzle %s",
      (const char *)magazine->_type->GetName(),
      (const char *)muzzle->GetName()
      );
    return false;
  }

  if (magazine->_type->_modes.Size() != muzzle->_nModes)
	{
		ErrF
		(
			"Error: Reload magazine %s into %s %s",
			(const char *)magazine->_type->GetName(),
			(const char *)slot._weapon->GetName(),
			(const char *)muzzle->GetName()
		);
		return false;
	}

  if (slot._mode < 0 || slot._mode >= magazine->_type->_modes.Size()) return false;

	// destroy empty magazine
	if (oldMagazine && oldMagazine->_ammo == 0)
	{
		RemoveMagazine(oldMagazine);
	}

	// prepare magazine
	magazine->_reloadMagazine = afterAnimation ? 0 :
		muzzle->_magazineReloadTime *
		GetInvAbility() *
		GRandGen.PlusMinus(1, 0.2);
	const WeaponModeType *mode = magazine->_type->_modes[slot._mode];

	// vary reload time depending on skill in range 0..2

	float reloadAbility = (GetInvAbility()-1)*0.25+1;
	// compress reload ability to range 1..2

	magazine->_reload = mode->_reloadTime * reloadAbility * GRandGen.PlusMinus(1, 0.1);

	// change in all slots with curent muzzle
	for (int j=0; j<_magazineSlots.Size(); j++)
	{
		MagazineSlot &slot = _magazineSlots[j];
		if (slot._muzzle == muzzle) slot._magazine = magazine;
	}
	return true;
}

/*!
\param muzzle reloaded muzzle
\param oldMagazineType prefered magazine type
\return index of magazine if found, otherwise -1
*/
int EntityAI::FindMagazineByType(const MuzzleType *muzzle, const MagazineType *oldMagazineType)
{
	// search for reserve magazine
	// first search the same type as old magazine
	if (oldMagazineType)
	{
		int ammoMax = 0;
		int jBest = -1;
		for (int j=0; j<NMagazines(); j++)
		{
			Magazine *magazine = GetMagazine(j);
			if
			(
				magazine && magazine->_type == oldMagazineType &&
				magazine->_ammo > ammoMax
			)
			{
				ammoMax = magazine->_ammo;
				jBest = j;
			}
		}
		if (jBest >= 0) return jBest;
	}
	// then other magazines fits into muzzle
	for (int i=0; i<muzzle->_magazines.Size(); i++)
	{
		const MagazineType *type = muzzle->_magazines[i];
		if (type == oldMagazineType) continue;
		int ammoMax = 0;
		int jBest = -1;
		for (int j=0; j<NMagazines(); j++)
		{
			Magazine *magazine = GetMagazine(j);
			if
			(
				magazine && magazine->_type == type &&
				magazine->_ammo > ammoMax
			)
			{
				ammoMax = magazine->_ammo;
				jBest = j;
			}
		}
		if (jBest >= 0) return jBest;
	}
	return -1;
}

// TODO: make member of EntityAI

static Vector3 GetWeaponSoundPos(EntityAI *veh, int weapon)
{
	return veh->PositionModelToWorld(veh->GetWeaponPoint(weapon));
}

/*!
\param slotIndex magazine slot to reload
*/
bool EntityAI::ReloadMagazine(int slotIndex)
{
	const MagazineSlot &slot = GetMagazineSlot(slotIndex);
	const MuzzleType *muzzle = slot._muzzle;
	Magazine *oldMagazine = slot._magazine;

	const MagazineType *oldMagazineType =
		oldMagazine ? oldMagazine->_type : NULL;

	int iMagazine = FindMagazineByType(muzzle, oldMagazineType);

	if (iMagazine<0) return false;

	return ReloadMagazine(slotIndex,iMagazine);
}

/*!
\param slotIndex magazine slot to reload
\param iMagazine index of reloading magazine
*/
bool EntityAI::ReloadMagazine(int slotIndex, int iMagazine)
{
	return ReloadMagazineTimed(slotIndex, iMagazine, false);
}

/*!
\param weapon magazine slot index
\param muzzle reloading muzzle
*/
void EntityAI::PlayReloadMagazineSound(int weapon, const MuzzleType *muzzle)
{
	// make sound when reloaded
	const SoundPars &pars = muzzle->_reloadMagazineSound;
	if (pars.name.GetLength() > 0)
	{
		float rndFreq = GRandGen.RandomValue() * 0.1 + 0.95;
		AbstractWave *wave = GSoundScene->OpenAndPlayOnce
		(
			pars.name, GetWeaponSoundPos(this,weapon), Speed(),
			pars.vol, pars.freq * rndFreq
		);
		if (wave)
		{
			GSoundScene->SimulateSpeedOfSound(wave);
			GSoundScene->AddSound(wave);
			_reloadMagazineSound = wave;
		}
	}
}

/*!
\param weapon magazine slot index
\patch 1.24 Date 09/26/2001 by Ondra
- Fixed: Speed of sound delay was missing on weapon firing sound.
*/
void EntityAI::PlayEmptyMagazineSound(int weapon)
{
	const MuzzleType *muzzle = _magazineSlots[weapon]._muzzle;
	const SoundPars &sound = muzzle->_sound;

	if (sound.name.GetLength() > 0)
	{
		float rndFreq = 1;
		float volume = sound.vol;
		float freq = sound.freq * rndFreq;
		_weaponFired.Access(weapon); // playing sound of the weapon
		if (_weaponFired[weapon])
		{
			// tell sound it should stop
			_weaponFired[weapon]->LastLoop();
			//GetNetworkManager().SoundState(_weaponFired[weapon],SSLastLoop);
			_weaponFired[weapon] = NULL;
		}
		_weaponFiredTime.Access(weapon);
		_weaponFiredTime[weapon] = Glob.time;
		AbstractWave *wave = GSoundScene->OpenAndPlayOnce
		(
			sound.name, GetWeaponSoundPos(this,weapon), Speed(), volume, freq
		);
		if (wave)
		{
			GSoundScene->SimulateSpeedOfSound(wave);
			GSoundScene->AddSound(wave);
			//GetNetworkManager().PlaySound(sound.name, Position(), Speed(), volume, freq, wave);
			_weaponFired[weapon] = wave;
		}
	}
}

void EntityAI::PlaceOnSurface(Matrix4 &trans)
{
	base::PlaceOnSurface(trans);
}

bool EntityAI::AutoReload(int weapon)
{
	// this should be called whenever when:
	// slot is empty
	// there is some magazine that fits into the slot
	// such cases are:
	//   FireWeapon
	//   AddMagazine ????
	//   AddWeapon ????

	const MagazineSlot &slot = GetMagazineSlot(weapon);
	const MuzzleType *muzzle = slot._muzzle;
	// TODO: flag reload on background
	//Magazine *magazine = slot._magazine;
	if
	(
		muzzle->_autoReload ||
		!GWorld->PlayerOn() ||
		!GWorld->PlayerOn()->Brain() ||
		GWorld->PlayerOn()->Brain() != CommanderUnit()
	)
	{
		return ReloadMagazine(weapon);
	}
	return false;
}

int EntityAI::AutoReloadAll()
{
	int ret = -1;
	for (int s=0; s<NMagazineSlots();)
	{
		const MagazineSlot &slot = GetMagazineSlot(s);
		if (!slot._magazine && slot._muzzle)
		{
			if (AutoReload(s) && ret<0) ret = s;
		}
		if (slot._muzzle) s += slot._muzzle->_nModes;
		else s++;
	}
	return ret;
}

void EntityAI::Draw( int level, ClipFlags clipFlags, const FrameBase &pos )
{
	base::Draw(level,clipFlags,pos);

	RString text;
	if (CommanderUnit()) text = CommanderUnit()->GetPerson()->GetInfo()._squadTitle;

	if (text.GetLength() > 0)
		GetType()->_squadTitles.Draw(level,clipFlags,pos,text);
}

bool EntityAI::AimWeaponForceFire(int weapon)
{
	Vector3 dir = Direction();
	dir[1] = 3;
	dir.Normalize();
	return AimWeapon(_currentWeapon, dir);
}

void EntityAI::SimulateWeaponActivity( float deltaT, SimulationImportance prec )
{
	// reload all muzzles
	const Magazine *lastMag = NULL;
	for (int i=0; i<NMagazineSlots(); i++)
	{
		const MagazineSlot &slot = GetMagazineSlot(i);
		const MuzzleType *muzzle = slot._muzzle;
		// TODO: flag reload on background
		Magazine *magazine = slot._magazine;
		if (magazine && magazine->_ammo > 0)
		{
			// reload each magazine once
			if (lastMag==magazine)
			{
				// same magazine in two magazine slots
				continue;
			}
			#if 1
			bool found = false;
			for (int j=0; j<i; j++)
				if (GetMagazineSlot(j)._magazine == magazine)
				{
					found = true;
					Fail("Bad double magazine detection");
					break;
				}
			if (found) continue;
			#endif
			lastMag = magazine;
			if (magazine->_reloadMagazine > 0)
			{
				magazine->_reloadMagazine -= deltaT;
				if
				(
					magazine->_reloadMagazine <= muzzle->_reloadMagazineSoundDuration &&
					!_reloadMagazineSound
				)
				{
					// make sound when reloaded
					PlayReloadMagazineSound(i,muzzle);
					saturateMax(magazine->_reloadMagazine, 0);
				}
			}
			else if (magazine->_reload > 0)
			{
				magazine->_reload -= deltaT;
				if
				(
					magazine->_reload <= muzzle->_reloadSoundDuration &&
					!_reloadSound
				)
				{
					// make sound when reloaded
					const SoundPars &pars = muzzle->_reloadSound;
					if (pars.name.GetLength() > 0)
					{
						float rndFreq = GRandGen.RandomValue() * 0.1 + 0.95;
						AbstractWave *wave = GSoundScene->OpenAndPlayOnce
						(
							pars.name, Position(), Speed(),
							pars.vol, pars.freq * rndFreq
						);
						if (wave)
						{
							GSoundScene->SimulateSpeedOfSound(wave);
							GSoundScene->AddSound(wave);
							_reloadSound = wave;
						}
					}
					saturateMax(magazine->_reload, 0);
				}
			}
		}
	}

	// simulate shooting visibility
	_shootTimeRest-=deltaT;
	if( _shootTimeRest<0 )
	{
		// return to default
		_shootTimeRest=1e10;
		_shootVisible=0;
		_shootAudible=0;
	}

	if (_forceFireWeapon >= 0 && !_isDead)
	{
		if (_forceFireWeapon == SelectedWeapon())
		{
			bool aimed = AimWeaponForceFire(_currentWeapon);
			if
			(
				GetWeaponLoaded(_currentWeapon) &&
				( GetWeaponDirection(_currentWeapon).Y() >= 0.7 || aimed )
			)
			{
				const WeaponModeType *mode = GetWeaponMode(_currentWeapon);				
				if (mode && mode->_useAction)
				{
					UIAction action;
					action.type = ATUseWeapon;
					action.target = this;
					action.param = _currentWeapon;
					action.param2 = 0;
					action.priority = 0;
					action.showWindow = false;
					action.hideOnUse = false;
					StartActionProcessing(action, GunnerUnit());
					_forceFireWeapon = -1;
				}
				else
				{
					if (FireWeapon(_currentWeapon, NULL)) _forceFireWeapon = -1;
				}
			}
		}
	}

	AIUnit *unit = GWorld->FocusOn();
	if (unit && unit == CommanderUnit() && !unit->IsInCargo() && !GWorld->GetCameraEffect())
	{
		if (GInput.GetActionToDo(UAHeadlights))
		{
			_pilotLight=!_pilotLight;
		}
	}

	// reflectors
	for (int i=0; i<_reflectors.Size(); i++)
	{
		Light *light = _reflectors[i];
		const ReflectorInfo &info = GetType()->_reflectors[i];
		bool on = _pilotLight && GetHit(info.hitPoint) < 0.9;
		light->Switch(on);
	}

	// check for dead condition
	/*
	if( _isDead || _isUpsideDown )
	{
		// tank bottom up - it is finished
		_moveMode=gotoDone;
	}
	*/

	// TODO: some good handling of not loaded weapons (ARM switch)
	/*
	AIUnit *unit = GunnerUnit();
	if (unit && !unit->IsPlayer())
	{
		AIGroup *grp = unit->GetGroup();
		if (grp->GetCombatMode() == CMSafe)
		{
			if (!_fireTarget && _currentWeapon >= 0)
				_currentWeapon = -1;
		}
		else
		{
			if (_currentWeapon < 0 && NWeapons() > 0)
				_currentWeapon = 0;
		}
	}
	*/

	// advance time for all looping weapon sounds
	for (int i=0; i<_weaponFired.Size(); i++)
	{
		AbstractWave *wave = _weaponFired[i];
		if (wave)
		{
			float time = Glob.time-_weaponFiredTime[i];
			wave->SetStopValue(time);
			// send termination request when wave is no longer needed
			// this is difficult, beacause only Link is stored here
			/*
			if (wave->IsTerminated())
			{
				GetNetworkManager().SoundState(wave,SSStop);
			}
			*/
		}
	}
}

/*
void EntityAI::GetNetState( UNIT_STATE &state )
{
	state.totalDammage = GetTotalDammage();
}

void EntityAI::SetNetState( const UNIT_STATE &state )
{
	SetTotalDammage(state.totalDammage);
}
*/

void EntityAI::IsMoved()
{
	// move condition detected
	_lastMovement=Glob.time;
	CancelStop();
}
void EntityAI::StopDetected()
{
	// stop condition detected
	if( Glob.time>_lastMovement+TimeToStop() )
	{
		Stop();
		_speed=VZero;
		_angMomentum=VZero;
	}
}

void EntityAI::SwitchLight(bool on)
{
	for (int i=0; i<_reflectors.Size(); i++)
	{
		Light *light = _reflectors[i];
		light->Switch(on);
	}
}

void EntityAI::Sound( bool inside, float deltaT )
{
	for (int i=0; i<_weaponFired.Size(); i++)
	{
		AbstractWave *wave = _weaponFired[i];
		if (!wave) continue;
		wave->SetPosition(GetWeaponSoundPos(this,i),Speed());
	}
}

void EntityAI::UnloadSound()
{
	// reset all sounds
	_weaponFired.Clear();
	_weaponFiredTime.Clear();
}

void EntityAI::StartRecoilFF()
{
#ifdef _WIN32
	if (_recoil)
	{
		RecoilFFRamp ramp;
		_recoil->GetFFRamp(_recoilFFIndex,ramp);
			
		float ffCoef = 10;
		float begMag = ramp._begAmplitude * _recoilFactor*ffCoef;
		float endMag = ramp._endAmplitude * _recoilFactor*ffCoef;
		JoystickDev->PlayRamp(begMag,endMag,ramp._duration);
	}
	else
	{
		JoystickDev->PlayRamp(0,0,0);
	}
#endif
}

void EntityAI::OnRecoilAbort()
{
}

float EntityAI::GetRecoilFactor() const
{
	// TODO: get from config
	return 0.5f;
}

/*!
React to impulse by force feedback / recoil effect
*/

void EntityAI::OnAddImpulse(Vector3Par force, Vector3Par torque)
{
	// construct artificial recoil effect based on impulse
	// offset is based on force?
	// angle on torque?
	float factor = fabs(force.Z()*GetInvMass())*0.4f;

	static RStringB impulse = "impulse";
	Ref<RecoilFunction> recoil = RecoilFunctions.New(impulse);
	//LogF("Impulse %.2f",factor);
	saturate(factor,0,1);
	StartRecoil(recoil,factor*GetRecoilFactor());
}


void EntityAI::StartRecoil( RecoilFunction *recoil, float recoilFactor )
{
	if (recoil->GetTerminated(0))
	{
		// no need to start recoil that has already been terminated
		return;
	}
	if (_recoil)
	{
		if (recoilFactor<_recoilFactor)
		{
			// current recoil is stronger - prefer it
			return;
		}
		OnRecoilAbort();
	}
	_recoil = recoil;
	_recoilTime = 0;
	_recoilFactor = recoilFactor;
	_recoilFFIndex = 0;
	if (this==GWorld->CameraOn())
	{
		StartRecoilFF();
	}
}

void EntityAI::Simulate( float deltaT, SimulationImportance prec )
{
	if( _shape && !_static ) // do not lock buildings
	{
		if
		(
			HasGeometry() && ( _isStopped || _speed.SquareSize()<Square(1.5) ) &&
			(
				!Airborne() || // lock for vehicle that are not airborne
				// lock also for low static airborne vehicle
				Position().Y()<GLandscape->SurfaceYAboveWater(Position().X(),Position().Z())+15
			)
		)
		{
			LockPosition();
		}
		else
		{
			UnlockPosition();
		}
	}
	float delta;
	delta=_avoidAsideWanted-_avoidAside;
	if( fabs(_avoidAsideWanted)>fabs(_avoidAside) )
	{
		//saturate(delta,-2.5*deltaT,+2.5*deltaT);
		saturate(delta,-20*deltaT,+20*deltaT);
	}
	else
	{
		saturate(delta,-0.2*deltaT,+0.2*deltaT);
	}
	_avoidAside+=delta;

	SimulateWeaponActivity(deltaT,prec);

	// simulate flag
	Texture *texture = GetFlagTexture();
	if (texture)
	{
		Shape *sShape = _shape->LevelOpaque(0);
		if (sShape) for (int i=0; i<sShape->NProxies(); i++)
		{
			const ProxyObject &proxy = sShape->Proxy(i);
			Entity *veh = dyn_cast<Entity>(proxy.obj.GetRef());
			if (veh)
			{
				LODShapeWithShadow *lodShape = veh->GetShape();
				if (strnicmp(lodShape->Name(), "data3d\\flag_", 12) == 0)
				{
					Matrix4 proxyTransform = AnimateProxyMatrix(0,proxy);

					//Matrix4 proxyTransform = veh->Transform();
					//proxyTransform.SetPosition(proxyTransform.FastTransform(lodShape->BoundingCenter()));
					Matrix4Val pTransform = Transform() * proxyTransform;
					
					Vector3 speed(VZero);
					if (_flag)
						speed = (1.0 / deltaT) * (pTransform.Position() - _flag->Position());
					else
					{
						_flag = dyn_cast<Flag>(NewNonAIVehicle(veh->GetNonAIType()->GetName(), lodShape->Name()));
						_flag->Init(pTransform);
					}
					// proxy may be animation
					_flag->SetTransform(pTransform);
					_flag->SetSpeed(speed);
					_flag->FlagSimulate(pTransform,deltaT, prec);
				}
			}
		}
	}
	else
	{
		if (_flag) _flag = NULL;
	}

	// simulate burst fire
	if (IsLocal())
	{
		int weapon = _currentWeapon;
		if (weapon>=0 && weapon<NMagazineSlots())
		{
			const Magazine *magazine = GetMagazineSlot(weapon)._magazine;
			if (magazine && magazine->_burstLeft > 0)
			{
				// fire on same target as before
				FireWeapon(weapon,_shootTarget);
			}
		}
	}

	if (_recoil)
	{
		_recoilTime += deltaT;

		if (this==GWorld->CameraOn())
		{
			// force feedback simulation is necessary
			RecoilFFRamp ramp;
			_recoil->GetFFRamp(_recoilFFIndex,ramp);
			// check if we need to advance recoil index
			if (_recoilTime>=ramp._startTime+ramp._duration)
			{
				_recoilFFIndex++;
				StartRecoilFF();
			}
		}
		if (_recoil->GetTerminated(_recoilTime))
		{
			_recoil.Free();
			if (this==GWorld->CameraOn())
			{
				StartRecoilFF();
			}
		}
	}
	if (IsLocal())
	{
		if (!_laserTargetOn)
		{
			StopLaser();
		}
		else
		{
			TrackLaser(_currentWeapon);
		}
	}

}

void EntityAI::AddDefaultWeapons()
{
}

void EntityAI::Init( Matrix4Par pos )
{
	// called before vehicle is placed
	if (_flag)
	{
		Matrix4 trans = pos * _flag->Transform();
		_flag->Init(trans);
	}
	OnEvent(EEInit);
}

void EntityAI::InitUnits()
{
	// called atfer vehicle is placed in landscape
	// unit are setup and initialized

}


/*\deprecated Currently not used*/

Texture *EntityAI::GetSideSign() const
{
	if( _targetSide==TEast ) return GLOB_SCENE->Preloaded(SignSideE);
	else if( _targetSide==TWest ) return GLOB_SCENE->Preloaded(SignSideW);
	else if( _targetSide==TGuerrila ) return GLOB_SCENE->Preloaded(SignSideG);
	return NULL; // neutral, guerilla .. etc
}

// all vehicles are supposed to be animated unless explicitly overridden
bool EntityAI::IsAnimated( int level ) const {return true;}
bool EntityAI::IsAnimatedShadow( int level ) const {return true;}

/*!
\patch 1.75 Date 3/7/2002 by Jirka
- Added: Support for reload animations for vehicles
\patch 1.78 Date 7/16/2002 by Ondra
- Fixed: AH1 cannon was aiming, but not moving (wrong animation).
*/

void EntityAI::Animate( int level )
{
	Shape *shape=_shape->Level(level);
	if( !shape ) return;
	base::Animate(level);
	const EntityAIType *type=GetType();
	Texture *tex;
/*
	tex = GetUnitSign();
	type->_unitNumber.SetTexture(_shape,level,tex);
	if (tex) type->_unitNumber.Unhide(_shape,level);
	else  type->_unitNumber.Hide(_shape,level);
	tex = GetGroupSign();
	type->_groupSign.SetTexture(_shape,level,tex);
	if (tex) type->_groupSign.Unhide(_shape,level);
	else  type->_groupSign.Hide(_shape,level);
	tex = GetSectorSign();
	type->_sectorSign.SetTexture(_shape,level,tex);
	if (tex) type->_sectorSign.Unhide(_shape,level);
	else  type->_sectorSign.Hide(_shape,level);
*/

	if (CommanderUnit()) tex = CommanderUnit()->GetPerson()->GetInfo()._squadPicture;
	else tex = NULL;
	if (tex != _squadTexture)
	{
		// ?? unregister _squadTexture ??
		if (tex) _shape->RegisterTexture(tex, type->_clan);
		_squadTexture = tex;
	}
	type->_clan.SetTexture(_shape,level,tex);
	if (tex) type->_clan.Unhide(_shape,level);
	else  type->_clan.Hide(_shape,level);

	if( _pilotLight )
	{
		type->_backLights.Unhide(_shape,level);
	}
	else
	{
		type->_backLights.Hide(_shape,level);
	}

	// reflectors
	for (int i=0; i<_reflectors.Size(); i++)
	{
		//Light *light = _reflectors[i];
		const ReflectorInfo &info = type->_reflectors[i];
		bool on = _pilotLight && GetHit(info.hitPoint) < 0.9;
		if (on)
			info.selection.Unhide(_shape, level);
		else
			info.selection.Hide(_shape, level);
	}

	// hidden selections
	for (int i=0; i<type->_hiddenSelections.Size(); i++)
	{
		if (_hiddenSelectionsTextures[i])
		{
			type->_hiddenSelections[i].Unhide(_shape, level);
			type->_hiddenSelections[i].SetTexture(_shape, level, _hiddenSelectionsTextures[i]);
		}
		else
			type->_hiddenSelections[i].Hide(_shape, level);
	}

	// reload animations
	if (_currentWeapon >= 0)
	{
		const MagazineSlot &s = GetMagazineSlot(_currentWeapon);
		const WeaponType *weapon = s._weapon;
		const Magazine *magazine = s._magazine;
		const MagazineType *magazineType = magazine ? magazine->_type : NULL;
		if (weapon && magazineType && magazineType->_modes.Size() > 0)
		{
			AnimationType *animation = NULL;
			float multiplier = 0;
			for (int i=0; i<type->_reloadAnimations.Size(); i++)
			{
				if (type->_reloadAnimations[i].weapon == weapon)
				{
					animation = type->_reloadAnimations[i].animation;
					multiplier = type->_reloadAnimations[i].multiplier;
					break;
				}
			}
			if (animation && animation->GetSelection(level)>=0)
			{
				float reload = magazine->_reload / magazineType->_modes[0]->_reloadTime;
				saturate(reload, 0, 1);
				float coef = (magazine->_ammo + reload) / magazineType->_maxAmmo;
				Matrix4 baseAnim = MIdentity;
				AnimateMatrix(baseAnim,level,animation->GetSelection(level));
				animation->Animate(_shape, level, fastFmod(multiplier * coef, 1), baseAnim);
			}
		}
	}
}

void EntityAI::Deanimate( int level )
{
	Shape *shape=_shape->Level(level);
	if( !shape ) return;
	const EntityAIType *type=GetType();
	type->_unitNumber.SetTexture(_shape,level,NULL);
	type->_groupSign.SetTexture(_shape,level,NULL);
	type->_sectorSign.SetTexture(_shape,level,NULL);
	//type->_sideSign.SetTexture(_shape,level,NULL);
	type->_clan.SetTexture(_shape,level,NULL);

	// hidden selections
	for (int i=0; i<type->_hiddenSelections.Size(); i++)
	{
		type->_hiddenSelections[i].Unhide(_shape, level);
	}

	base::Deanimate(level);
}

/*!
\patch 1.31 Date 11/23/2001 by Ondra
- Fixed: Cars (jeep and BRDM) assumed their weapon is always aimed.
*/

float EntityAI::GetAimed( int weapon, Target *target ) const
{


	if( weapon<0 ) return 0;
	if (!target) return 0;
	if (!target->idExact) return 0;
	if (target->lastSeen<Glob.time-5) return 0;

	float visible=_visTracker.Value(this,_currentWeapon,target->idExact,0.9);
	const Magazine *magazine = GetMagazineSlot(weapon)._magazine;
	const MagazineType *aInfo = magazine ? magazine->_type : NULL;
	const WeaponModeType *mode = GetWeaponMode(weapon);

	const AmmoType *ammo = mode ? mode->_ammo : NULL;
	if (!ammo) return 1;
	if (target->posError.SquareSize() > Square(ammo->indirectHitRange*2)) return 0;
	
	// decrease landscape occlussion significance
	visible=1-(1-visible)*0.3f;

	Vector3 ap=target->AimingPosition();
	if (ammo && ammo->_simulation != AmmoShotMissile)
	{
		// predict shot result
		float dist=ap.Distance(Position());
		float time=dist*aInfo->_invInitSpeed;
		Vector3 estPos=ap+target->speed*time;
		//Vector3 wSpeed=GetWeaponDirection(weapon)*aInfo->_initSpeed;
		Vector3 wDir=GetWeaponDirection(weapon);
		Vector3 wPos=PositionModelToWorld(GetWeaponPoint(weapon));
		float eDist=wPos.Distance(estPos);
		Vector3 hit=wPos+wDir*eDist;
		hit[1]-=G_CONST*time*time*0.5;
		Vector3 hError=hit-estPos;
		hError[1]*=2;
		float error=hError.Size()*0.5;

		//float ability=GetInvAbility();
		float tgtSize=target->idExact->GetShape()->GeometrySphere();
		//float maxError=(tgtSize+info._ammo->indirectHitRange)*(ability*0.5);
		//maxError+=dist*info._dispersion*GetInvAbility();
		//float maxError=(tgtSize+info._ammo->indirectHitRange)*0.5;
		float maxError=tgtSize*0.7+mode->_ammo->indirectHitRange*0.3;
		maxError+=dist*mode->_dispersion;

		if (mode->_ammo->_simulation != AmmoShotBullet)
		{
			maxError *=2;
		}

#if _ENABLE_CHEATS
		if ((Object *)this==GWorld->CameraOn() && CHECK_DIAG(DECombat))
		{
			GlobalShowMessage
			(
				2000,"Error %.1f, tgtSize %.1f, maxError %.1f",
				error,tgtSize,maxError
			);
		}
#endif
		return ( error<maxError )*visible;
		//return( xError<tgtSize && yError<tgtSize*0.25 )*visible;
	}
	else
	{
		Vector3 relPos=ap-PositionModelToWorld(GetWeaponPoint(weapon));
		// check if target is in front of us
		if( relPos.SquareSize()<=Square(30) ) return 0; // missile fire impossible
		// check if target position is withing missile lock cone
		Vector3 wDir=GetWeaponDirection(weapon);
		float wepCos=relPos.CosAngle(wDir);
		float wepAimed=1-(1-wepCos)*30;
		saturateMax(wepAimed,0);
		return visible*wepAimed;
	}

	//return 1.0; // default: assume always aimed
}

typedef StaticArrayAuto< OLink<EntityAI> > EntityAIList;

/*!
\patch 1.31 Date 11/23/2001 by Ondra
- New: AI: safety check on near friendly troops to avoid friendly fire
when firing from riffles and machineguns.
*/

static void CheckNearFriendlies
(
	EntityAIList &res, const EntityAI *from, const EntityAI *target, Vector3Par dir,
	float maxDistance, float maxAside
)
{
	// check all near friendly troops
	// do not check myself and target
	// collect information about near troops
	int xMin,xMax,zMin,zMax;
	Vector3Val fromPos = from->Position();
	ObjRadiusRectangle
	(
		xMin,xMax,zMin,zMax,fromPos,fromPos,maxDistance
	);
	AIGroup *grp = from->GetGroup();
	if (!grp) return;
	AICenter *center = grp->GetCenter();
	if (!center) return;

	for (int z=zMin; z<=zMax; z++) for (int x=xMin; x<=xMax; x++)
	{
		const ObjectList &list = GLandscape->GetObjects(z,x);
		for (int oi=0; oi<list.Size(); oi++)
		{
			Object *obj = list[oi];
			Vector3Val tgtPos = obj->Position();
			if (tgtPos.Distance2(fromPos)>Square(maxDistance))
			{
				#if 0 //_ENABLE_CHEATS
				EntityAI *tgt = dyn_cast<EntityAI>(obj);
				if (!tgt) continue;
				LogF
				(
					"%s: Friendly %s too far (%.1f)",
					(const char *)from->GetDebugName(),
					(const char *)tgt->GetDebugName(),
					tgtPos.Distance(fromPos)
				);
				#endif
				continue;
			}
			EntityAI *tgt = dyn_cast<EntityAI>(obj);
			if (!tgt) continue;
			if (tgt==target || tgt==from) continue;
			TargetSide side = tgt->GetTargetSide();
			if (!center->IsFriendly(side)) continue;
			if (tgt->IsDammageDestroyed()) continue;

			// calculate asside distance
			// this is distance of tgt from line fromPos,dir

			Vector3 tgtAimPos = tgt->AimingPosition();
			// find nearest point on line fromPos,dir to tgtAimPos
			float t = dir*(tgtAimPos-fromPos);
			// t is front-back distance from fromPos to tgtAimPos
			Vector3 nearest = fromPos+dir*t;
			if (t<-maxAside*0.5f)
			{
				/*
				LogF
				(
					"%s: Friendly %s out of aside range (back %.1f)",
					(const char *)from->GetDebugName(),
					(const char *)tgt->GetDebugName(),
					-t
				);
				*/
				continue;
			}
			if (nearest.Distance2(tgtAimPos)>Square(maxAside))
			{
				/*
				LogF
				(
					"%s: Friendly %s out of aside range (aside %.1f)",
					(const char *)from->GetDebugName(),
					(const char *)tgt->GetDebugName(),
					nearest.Distance(tgtPos)
				);
				*/
				continue;
			}
			/*
			LogF
			(
				"%s: Near friendly - %s",
				(const char *)from->GetDebugName(),
				(const char *)tgt->GetDebugName()
			);
			*/
			res.Add(tgt);
		}
	}
}

bool EntityAI::CheckFriendlyFire( int weapon, Target *target ) const
{
	const WeaponModeType *mode = GetWeaponMode(weapon);
	// if we have no ammo, we can safely fire, we can do no harm
	if (!mode) return true;
	const AmmoType *ammo = mode->_ammo;
	if (!ammo) return true;


	switch(ammo->_simulation)
	{
		case AmmoShotBullet: //case AmmoShotShell:
		{
			AUTO_STATIC_ARRAY(OLink<EntityAI>,nearFriendlies,32);
			// check near part of bullet trajectory
			// 
			Vector3Par dir = GetWeaponDirection(weapon);
			float distToTarget = target->AimingPosition().Distance(Position());
			// if target is very near, be less carefull about friendly fire
			float radius = 2.0f;
			float maxDist = 50;
			if (distToTarget<maxDist)
			{
				maxDist = distToTarget;
				radius = 1.0f;
				if (distToTarget<25) radius = 0.5f;
			}
			CheckNearFriendlies(nearFriendlies,this,target->idExact,dir,maxDist,radius);
			if (nearFriendlies.Size()>0)
			{
				// some friendly unit blocks fire - we have to wait
				return false;
			}
		}
	}
	
	// default: unknown weapon - assume it is safe to fire
	return true;
}


float EntityAI::GetFormationTime() const {return GetType()->GetFormationTime();}
float EntityAI::GetInvFormationTime() const {return GetType()->GetInvFormationTime();}


/*!
Armored vehicles do not care much for collisions, but
for soft vehicles, air vehicles or men collision can do much dammage
or it can be even critical. 
*/

float EntityAI::AfraidOfCollision( VehicleKind with ) const
{
	switch( GetType()->GetKind() )
	{
		case VArmor:
			return 1;
		case VSoft:
			if( with==VSoft ) return 1;
			return 2;
		default:
			return 4; // be very carefull
	}
}

/*!
	For most entities this is very low (0-5 m),
	as land vehicles or men move on the ground.
	This is significant for aerial vehicles.
*/

float EntityAI::GetCombatHeight() const
{
	return -_shape->Min()[1];
}
float EntityAI::GetMinCombatHeight() const
{
	return -_shape->Min()[1];
}

float EntityAI::GetMaxCombatHeight() const
{
	return -_shape->Min()[1];
}

void EntityAI::AimWeaponManDir( int weapon, Vector3Par direction )
{
	AimWeapon(weapon,direction);
}

bool EntityAI::AimWeapon( int weapon, Target *target )
{
	Vector3 dir;
	bool ret = CalculateAimWeapon(weapon,dir,target);
	if (!ret) return false;
	return AimWeapon(weapon,dir);
}

bool EntityAI::AimObserver(Target *target )
{
	Vector3 dir;
	bool ret = CalculateAimObserver(dir,target);
	if (!ret) return false;
	return AimObserver(dir);
}

/*!
This functions is used for manual units when UI is reporting cursor position
when using mouse controls.
*/

void EntityAI::AimDriver(Vector3Par direction)
{
}

/*!
This function is used for manual units
to adjust weapon depending on current field of view.
Example of such weapon is M21 sniper rifle.
*/

void EntityAI::AdjustWeapon
(
	int weapon, CameraType camType, float fov, Vector3 &camDir
)
{
	// default is no adjustment
}

//! if necessary, ask server to aim weapon
void EntityAI::AskForAimWeapon(int weapon, Vector3Val dir)
{
	if (weapon<0) return;
	if (_aimWeaponAsked.Size()<=weapon)
	{
		int oldSize = _aimWeaponAsked.Size();
		_aimWeaponAsked.Access(weapon);
		for (int i=oldSize; i<_aimWeaponAsked.Size(); i++)
		{
			_aimWeaponAsked[i] = VZero;
		}
		
	}
	Vector3 &lastRequest = _aimWeaponAsked[weapon];
	if (dir.Distance2(lastRequest)>Square(0.01))
	{
		lastRequest = dir;
		GetNetworkManager().AskForAimWeapon(this, weapon, dir);
	}
}

//! if necessary, ask server to aim observer
void EntityAI::AskForAimObserver(Vector3Val dir)
{
	if (dir.Distance2(_aimObserverAsked)>Square(0.01))
	{
		_aimObserverAsked = dir;
		GetNetworkManager().AskForAimObserver(this, dir);
	}
}

Vector3 EntityAI::GetEyeDirectionWanted() const
{
	return GetEyeDirection();
}

Vector3 EntityAI::GetWeaponDirectionWanted( int weapon ) const
{
	return GetWeaponDirection(weapon);
}

/*!
\return
This direction returned is typical the head (eye) direction
or direction of observer turret when appropriate.
*/


Vector3 EntityAI::GetEyeDirection() const
{
	if (NWeaponSystems()<=0) return Direction();
	return GetWeaponDirection(0);
}

Vector3 EntityAI::GetWeaponDirection( int weapon ) const
{
	// in world coordinates
	return Direction();
}

Vector3 EntityAI::GetWeaponCenter( int weapon ) const
{
	// in model coordinates
	return VZero;
}


Vector3 EntityAI::GetWeaponPoint( int weapon ) const
{
	return GetWeaponCenter(weapon);
}

bool EntityAI::GetWeaponCartridgePos
(
	int weapon, Matrix4 &pos, Vector3 &vel
) const
{

	/**/
	if (weapon >= 0 && weapon < NMagazineSlots())
	{
		const MuzzleType *muzzle = GetMagazineSlot(weapon)._muzzle;

		if
		(
			muzzle->_cartridgeOutPosIndex<0 ||
			muzzle->_cartridgeOutEndIndex<0
		)
		{
			return false;
		}
		int memLevel = _shape->FindMemoryLevel();

		Vector3 cPos = AnimatePoint(memLevel,muzzle->_cartridgeOutPosIndex);
		Vector3 cEnd = AnimatePoint(memLevel,muzzle->_cartridgeOutEndIndex);

		Vector3Val cDir = GetWeaponDirection(weapon);

		PositionModelToWorld(cPos,cPos);
		PositionModelToWorld(cEnd,cEnd);

		vel = (cEnd - cPos)*3;

		// create matrix for cartridge position
		pos.SetDirectionAndUp(cDir,VUp);
		pos.SetPosition(cPos);

		return true;
	}
	// convert neutral gun position

	pos = MIdentity;
	vel = VZero;

	return false;
}

bool EntityAI::GetWeaponLoaded( int weapon ) const
{
	const Magazine *magazine = GetMagazineSlot(weapon)._magazine;
	if (!magazine) return true; // no ammo
	const WeaponModeType *mode = GetWeaponMode(weapon);
	if (!mode) return true; // bad weapon
	if (!mode->_ammo) return true; // no ammo
	if (mode->_ammo->_simulation == AmmoNone) return true; // no ammo
	if( magazine->_reload>0 ) return false; // not loaded
	if( magazine->_reloadMagazine>0 ) return false; // not loaded
	if (IsActionInProgress(MFReload)) return false; // not loaded

	if( magazine->_ammo<=0 ) return false; // no ammo
	return true;
}

bool EntityAI::GetWeaponReady( int weapon, Target *target ) const
{
	const WeaponModeType *mode = GetWeaponMode(weapon);
	if (!mode) return false;
	if (!target) return true;
	float timeFromLastShot = Glob.time-_lastShotTime;
	float rateOfFire = mode->_aiRateOfFire;
	if (timeFromLastShot>rateOfFire) return true;
	float dist2 = target->AimingPosition().Distance2(Position());
	float maxDist2 = Square(mode->_aiRateOfFireDistance);
	if (dist2<maxDist2)
	{
		return Square(timeFromLastShot)*maxDist2>Square(rateOfFire)*dist2;
	}
	else
	{
		return timeFromLastShot>rateOfFire;
	}
}


bool EntityAI::FireMissile
(
	int weapon,
	Vector3Par offset, Vector3Par direction, Vector3Par initSpeed,
	TargetType *target
)
{
	if( _isDead || _isUpsideDown ) return false;
	const WeaponModeType *mode = GetWeaponMode(weapon);
	const AmmoType *type = mode ? mode->_ammo : NULL;
	//if( !type || !aInfo ) return false;

	_fire._nextWeaponSwitch=Glob.time+GetInvAbility()*0.5;

	if (!EffectiveGunnerUnit()->GetPerson()->IsLocal())
	{
		// remote vehicles do not shot
		return true;
	}

	if (target && target->IsEventHandler(EEIncomingMissile))
	{
		target->OnEvent(EEIncomingMissile,type->GetName(),this);
		GetNetworkManager().OnIncomingMissile(target, type->GetName(), this);
	}

	Entity *shot=NewShot(this,type,target);
	Vector3 wDirection(NoInit);
	DirectionModelToWorld(wDirection,direction.Normalized());
	shot->SetOrient(wDirection,VUp);
	shot->SetSpeed(_speed+DirectionModelToWorld(initSpeed));
	shot->SetPosition(PositionModelToWorld(offset));
	GLOB_WORLD->AddFastVehicle(shot);

	if (GWorld->GetMode() == GModeNetware)
	{
		// shot created
		GetNetworkManager().CreateVehicle(shot, VLTFast, "", -1);
	}	
	#if _ENABLE_CHEATS
		if (this==GWorld->CameraOn() && CHECK_DIAG(DECombat))
		{
			GWorld->SwitchCameraTo(shot,CamInternal);
		}
	#endif

	AIUnit *unit = CommanderUnit();
	Target *assigned = unit ? unit->GetTargetAssigned() : NULL;
	if( assigned && target==assigned->idExact )
	{
		_lastShotAtAssignedTarget = Glob.time;
	}
	_lastShot = shot;

	return true;
}

bool EntityAI::FireShell
(
	int weapon,
	Vector3Par offset, Vector3Par direction,
	TargetType *target
)
{
	// fire
	if( _isDead || _isUpsideDown ) return false;

	_fire._nextWeaponSwitch=Glob.time+GetInvAbility()*0.5;

	const Magazine *magazine = GetMagazineSlot(weapon)._magazine;
	if (!magazine) return false;
	const MagazineType *aInfo = magazine->_type;
	const WeaponModeType *mode = GetWeaponMode(weapon);
	Assert(mode);
	const AmmoType *type = mode->_ammo;
	if (!type) return false;

	if (!EffectiveGunnerUnit()->GetPerson()->IsLocal())
	{
		// remote vehicles do not shot
		return true;
	}

	Entity *shot=NewShot(this,type,NULL);
	Vector3 wDirection(NoInit);
	DirectionModelToWorld(wDirection,direction);
	shot->SetOrient(wDirection,VUp);
	// do random dispersion of projectiles
	const int gausPassNum=4;
	float gaussX=0,gaussY=0;
	for( int c=gausPassNum; --c>=0;  )
	{
		gaussX+=GRandGen.RandomValue();
		gaussY+=GRandGen.RandomValue();
	}
	gaussX*=2.0/gausPassNum;
	gaussY*=2.0/gausPassNum;

	float randomX=(gaussX-1)*mode->_dispersion;
	float randomY=(gaussY-1)*mode->_dispersion;

	if( QIsManual() )
	{
		IF_FADE()
		{
			randomX *= 20;
			randomY *= 20;
		}
	}

	Vector3 dir(randomX,randomY,1);
	shot->DirectionModelToWorld(dir,dir);
	shot->SetOrient(dir,shot->DirectionUp());
	// final speed and position
	shot->SetSpeed(_speed+shot->Direction()*aInfo->_initSpeed);
	shot->SetPosition(PositionModelToWorld(offset));
	GLOB_WORLD->AddFastVehicle(shot);

	if (GWorld->GetMode() == GModeNetware)
	{
		// shot created
		GetNetworkManager().CreateVehicle(shot, VLTFast, "", -1);
	}	

	AIUnit *unit = CommanderUnit();
	Target *assigned = unit ? unit->GetTargetAssigned() : NULL;
	if( assigned && target==assigned->idExact )
	{
		_lastShotAtAssignedTarget = Glob.time;
	}
	_lastShot = shot;

	//ADD_COUNTER(wFire,1);
	return true;
}

bool EntityAI::FireMGun
(
	int weapon,
	Vector3Par offset, Vector3Par direction,
	TargetType *target
)
{
	if( _isDead || _isUpsideDown ) return false;

	// fire
	const MagazineSlot &slot = GetMagazineSlot(weapon);
	const Magazine *magazine = slot._magazine;
	if (!magazine) return false;
	const MagazineType *aInfo = magazine->_type;
  if (!aInfo || slot._mode < 0 || slot._mode >= aInfo->_modes.Size()) return false;
	const WeaponModeType *mode = aInfo->_modes[slot._mode];
	const AmmoType *type = mode->_ammo;
	if (!type) return false;
	const MuzzleType &info = *slot._muzzle;

	if (!EffectiveGunnerUnit()->GetPerson()->IsLocal())
	{
		// remote vehicles do not shot
		return true;
	}

	Entity *shell=NewShot(this,type,NULL);
	Vector3 wDirection(NoInit);
	DirectionModelToWorld(wDirection,direction);
	shell->SetOrient(wDirection,VUp);
	// do random dispersion of projectiles
	const int gausPassNum=4;
	float gaussX=0,gaussY=0;
	for( int c=gausPassNum; --c>=0; )
	{
		gaussX+=GRandGen.RandomValue();
		gaussY+=GRandGen.RandomValue();
	}
	gaussX*=2.0/gausPassNum;
	gaussY*=2.0/gausPassNum;
	float randomX=(gaussX-1)*(mode->_dispersion);
	float randomY=(gaussY-1)*(mode->_dispersion);
	if( !QIsManual() )
	{
		randomX*=info._aiDispersionCoefX;
		randomY*=info._aiDispersionCoefY;
	}
	else
	{
		IF_FADE()
		{
			randomX *= 20;
			randomY *= 20;
		}
	}

	_fire._nextWeaponSwitch=Glob.time+GetInvAbility()*0.5;

	Vector3 dir(randomX,randomY,1);
	shell->DirectionModelToWorld(dir,dir);
	shell->SetOrient(dir,shell->DirectionUp());
	// final speed and position
	shell->SetSpeed(_speed+dir*aInfo->_initSpeed);
	shell->SetPosition(PositionModelToWorld(offset));
	GLOB_WORLD->AddFastVehicle(shell);

	if (GWorld->GetMode() == GModeNetware)
	{
		// shot created
		GetNetworkManager().CreateVehicle(shell, VLTFast, "", -1);
	}	

	AIUnit *unit = CommanderUnit();
	Target *assigned = unit ? unit->GetTargetAssigned() : NULL;
	if( assigned && target==assigned->idExact )
	{
		_lastShotAtAssignedTarget = Glob.time;
	}
	_lastShot = shell;

	//ADD_COUNTER(wFire,1);
	return true;
}

bool EntityAI::CalculateLaser(Vector3 &pos, Vector3 &dir, int weapon) const
{
	dir = GetWeaponDirection(weapon);
	dir.Normalize();
	Vector3 weaponPos = PositionModelToWorld(GetWeaponPoint(weapon));

	//+dir*50;


	// calculate intersection with land
	// between Position() and position
	float maxDist = Glob.config.horizontZ;

	float t = GLandscape->IntersectWithGroundOrSea(&pos,weaponPos,dir,0,maxDist*1.1);
	bool found = false;
	if (t<=maxDist)
	{
		// some intersection with land found
		found = true;
		pos[1] = GLandscape->SurfaceY(pos[0],pos[2]);
		pos[1] += 0.05f;
	}
	else
	{
		t = maxDist;
		pos = weaponPos + maxDist*dir;
	}

	// try to find some intersection with object

	CollisionBuffer collision;
	GLandscape->ObjectCollision
	(
		collision,_laserTarget,const_cast<EntityAI *>(this),
		weaponPos,pos,0.2f
	);
	if (collision.Size()>0)
	{
		// check first non-glass collision
		float minT = FLT_MAX;
		int minI = -1;

		Texture *glass = GPreloadedTextures.New(TextureBlack);
		for( int i=0; i<collision.Size(); i++ )
		{
			// info.pos is relative to object
			CollisionInfo &info=collision[i];
			// we can go through some textures

			if (info.texture == glass) continue;
			if( info.object )
			{
				if( minT>info.under ) minT=info.under,minI=i;
			}
		}
		if( minI>=0 )
		{
			CollisionInfo &info=collision[minI];
			pos=info.object->PositionModelToTop(info.pos);
			// make point a little bit off-surface to make sure it is visible
			pos -= dir*0.07f;
			found = true;
		}
	}

	return found;
}

/*!
\patch 1.28 Date 10/25/2001 by Ondra.
- New: Laser target designators.
\patch 1.58 Date 5/17/2002 by Ondra
- Fixed: AI targeting laser designated targets improved.
*/

void EntityAI::StopLaser()
{
	if (_laserTarget)
	{
		_laserTarget->SetDelete();
	}
}

void EntityAI::TrackLaser(int weapon)
{
	// create a new laser target
	Vector3 pos,dir;
	if (CalculateLaser(pos,dir,weapon))
	{
		if (!_laserTarget)
		{
			TargetSide side = GetGroup()->GetCenter()->GetSide();
			const char *laserName = "LaserTargetC";
			if (side==TEast) laserName = "LaserTargetE";
			else if (side==TWest) laserName = "LaserTargetW";
			EntityAI *ltgt = NewVehicle(laserName,"");

			if (ltgt)
			{
				// laser target is oriented to face target
				// its forward direction goes away from laser source
				if (dir.Distance2(VUp)>Square(0.2))
				{
					ltgt->SetDirectionAndUp(dir,VUp);
				}
				else
				{
					ltgt->SetDirectionAndAside(dir,VAside);
				}
				ltgt->SetPosition(pos);
				ltgt->Init(*ltgt);

				GWorld->AddVehicle(ltgt);
				GetNetworkManager().CreateVehicle(ltgt, VLTVehicle, "", -1);

				_laserTarget = ltgt;
			}
		}
		else
		{
			_laserTarget->Move(pos);
		}
	}
	// when we do not call move, target is not activated
	// it stops shining
	// and after some time is autodestroys
}

void EntityAI::FireLaser(int weapon,TargetType *target)
{
	_laserTargetOn = !_laserTargetOn;
}

RString EntityAI::GetCurrentMove() const
{
	// normal vehicle cannot play any motion capture moves
	return RString();
}

void EntityAI::PlayMove( RStringB move, ActionContextBase *context)
{
	// normal vehicle cannot play any motion capture moves
}

void EntityAI::SwitchMove(RStringB move, ActionContextBase *context)
{
	// normal vehicle cannot play any motion capture moves
}

const float DeadAbility=0.2; // private

float EntityAI::GetAbility() const
{
	// returns from 1 (able) to 0.5 (unable)
	AIUnit *brain=CommanderUnit();
	if( !brain ) return DeadAbility;
	return brain->GetAbility();
}

float EntityAI::GetInvAbility() const
{
	// returns from 1 (able) to 2 (unable)
	AIUnit *brain=CommanderUnit();
	if( !brain ) return 1/DeadAbility;
	return brain->GetInvAbility();
}

float EntityAI::GetTypeCost(OperItemType type) const
{
	static const float costs[] =
	{
		1.0, // OITNormal,
		3.0, 3.0, 3.0, //OITAvoidBush, OITAvoidTree, OITAvoid
		SET_UNACCESSIBLE,	 // OITWater,
		SET_UNACCESSIBLE, // OITSpaceRoad
		0.5, //  OITRoad
		SET_UNACCESSIBLE, // OITSpaceBush
		SET_UNACCESSIBLE, // OITSpaceTree
		SET_UNACCESSIBLE, // OITSpace
		0.5 // OITRoadForced
	};
	Assert(sizeof(costs)/sizeof(*costs)==NOperItemType);
	return costs[type];
}


/*!
\patch 1.12 Date 08/07/2001 by Ondra
- New: Force feedback effect on fire / hit.
\patch 1.30 Date 11/02/2001 by Ondra
- Fixed: Speed of sound simulation caused distant fullauto sounds muted (since 1.27)
*/

void EntityAI::FireWeaponEffects
(
	int weapon, const Magazine *magazine, EntityAI *target
)
{
	if (weapon<0 || weapon>=NMagazineSlots()) return;
	const MagazineSlot &slot = GetMagazineSlot(weapon);
	if (slot._magazine!=magazine) return;

	const MuzzleType *muzzle = slot._muzzle;
	const SoundPars *sound = &muzzle->_sound;
  const MagazineType *mType = magazine ? magazine->_type : NULL;
  const WeaponModeType *mode = NULL;
	bool continuous = muzzle ? muzzle->_soundContinuous : false;
	bool noSound = false;
	/**/
  if (mType && slot._mode >= 0 && slot._mode < mType->_modes.Size())
	{
    mode = mType->_modes[slot._mode];
		sound = &mode->_sound;
		continuous = mode->_soundContinuous;
		// sound only on first shot in burst
		if (magazine->_burstLeft>0) noSound = true;
	}
	/**/

	// play sound
	if (sound->name.GetLength() > 0 && !noSound)
	{
		//float rndFreq = GRandGen.RandomValue() * 0.1 + 0.95;
		float rndFreq = 1;
		float volume = sound->vol;
		float freq = sound->freq * rndFreq;
		_weaponFired.Access(weapon); // playing sound of the weapon
		_weaponFiredTime.Access(weapon); // playing sound of the weapon
		if (!_weaponFired[weapon])
		{
			AbstractWave *wave = GSoundScene->OpenAndPlayOnce
			(
				sound->name, GetWeaponSoundPos(this,weapon), Speed(), volume, freq
			);
			if (wave)
			{
				// TODO: proper solution of SOS problems with continuos sounds
				if (!continuous)
				{
					GSoundScene->SimulateSpeedOfSound(wave);
				}
				GSoundScene->AddSound(wave);
				_weaponFired[weapon] = wave;
				if (continuous)
				{
					wave->PlayUntilStopValue(wave->GetLength());
				}
			}
			_weaponFiredTime[weapon] = Glob.time;
		}
		else
		{
			// sound may be single loop or repeated
			// if the soond is repeating, it should be marked
			// if not, it should be restarted
			AbstractWave *wave = _weaponFired[weapon];
			if (!continuous)
			{
				wave->Restart();
				//GetNetworkManager().SoundState(wave,SSRestart);
			}
			else
			{
				_weaponFiredTime[weapon] = Glob.time;
				wave->PlayUntilStopValue(wave->GetLength());
				//GetNetworkManager().SoundState(wave,SSRepeat);
			}
		}
	}

	const AmmoType *type = mode ? mode->_ammo : NULL;
	if (type)
	{
		_shootVisible = type->visibleFire;
		_shootAudible = type->audibleFire;
		_shootTimeRest = type->visibleFireTime;
		_shootTarget = target;
		// perform cartridge effects
		#if 1
		if (type->_cartridgeType && EnableVisualEffects(SimulateVisibleNear))
		{
			Vector3 viewerPos=GScene->GetCamera()->Position();

			SimulationImportance importance = CalculateImportance(&viewerPos,1);
			if (importance<=SimulateVisibleNear)
			{
				// check current muzzle catridge position / velocity
				// create a transformation from weapon direction
				Matrix4 pos;
				Vector3 vel;
				if (GetWeaponCartridgePos(weapon,pos,vel))
				{

					pos = Transform() * pos;
					vel = Transform().Rotate(vel);

					Entity *fx = CreateThing(type->_cartridgeType,pos,vel);
					if (fx)
					{
						// any additional processing
					}
				}
			}

		}
		#endif
	}

	RStringB recoilName = mode->_recoilName;
	Ref<RecoilFunction> recoil = RecoilFunctions.New(recoilName);
	StartRecoil(recoil,GetRecoilFactor());

	if (IsEventHandler(EEFired))
	{
		GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
		GameArrayType &arguments = value;
		const MagazineSlot &slot = GetMagazineSlot(weapon);
		RString weaponName = slot._weapon->GetName();
		RString muzzleName = slot._muzzle->GetName();
		RString modeName = mode ? mode->GetName() : "";
		RString ammoName = type ? type->GetName() : "";
		arguments.Add(GameValueExt(this));
		arguments.Add(weaponName);
		arguments.Add(muzzleName);
		arguments.Add(modeName);
		arguments.Add(ammoName);
		OnEvent(EEFired,value);
	}
}

bool EntityAI::FireWeapon( int weapon, TargetType *target )
{
	if (weapon >= NMagazineSlots() || weapon < 0) return false; // nothing to fire
	
	_lastShotTime = Glob.time;

	const MagazineSlot &slot = GetMagazineSlot(weapon);
	Magazine *magazine = slot._magazine;

	FireWeaponEffects(weapon,magazine,target);
	GetNetworkManager().FireWeapon(this, weapon, magazine, target);

	if (!magazine) return false;

	const WeaponModeType *mode = NULL;
  const MagazineType *type = magazine ? magazine->_type : NULL;
	if (magazine)
	{
    if (type && slot._mode >= 0 && slot._mode < type->_modes.Size())
      mode = type->_modes[slot._mode];
	}

	// only last bullet in burst should be reloaded based on ability

	if (magazine->_burstLeft<=0)
	{
		magazine->_burstLeft = mode->_burst;
		saturateMin(magazine->_burstLeft, magazine->_ammo/mode->_mult);
	}

	if (magazine->_burstLeft==1 || mode->_burst==0)
	{
		AIUnit *unit = GunnerUnit();
		if (!unit) unit = CommanderUnit();
		float invAbility = unit ? unit->GetInvAbility() : 1/DeadAbility;
		magazine->_reload = mode->_reloadTime * invAbility * GRandGen.PlusMinus(1, 0.1);
	}
	else
	{
		magazine->_reload = mode->_reloadTime;
	}

	magazine->_burstLeft--; // count shots in burst

	int burst = mode->_mult;
	saturateMin(burst, magazine->_ammo);
	magazine->_ammo -= burst;
	// TODO: ?? auto reload magazine here?
	if (!IsLocal())
	{
		GetNetworkManager().AskForAmmo(this, weapon, burst);
	}

	// remove magazine
	if (magazine->_ammo <= 0)
	{
		if (weapon != _currentWeapon)
		{
			RemoveMagazine(magazine);
		}
		// try to start auto-reloading
		AutoReload(weapon);
	}
	return true;
}

RefArray<Object> MapDiags;

#if _RELEASE
	#define DIAG_VEHICLE 0
	#define DIAG_ROAD 0
#else
	#define DIAG_VEHICLE 0
	#define DIAG_ROAD 0
#endif

static const EnumName SimulationImportanceNames[]=
{
	EnumName(SimulateCamera,"Camera"),
	EnumName(SimulateVisibleNear,"Near"),
	EnumName(SimulateVisibleFar,"Far"),
	EnumName(SimulateInvisibleNear,"InvisNear"),
	EnumName(SimulateInvisibleFar,"InvisFar"),
	EnumName(SimulateDefault,"Default"),
	EnumName()
};

template<>
const EnumName *GetEnumNames( SimulationImportance dummy ) {return SimulationImportanceNames;}

RString EntityAI::DiagText() const
{
	AIUnit *unit=PilotUnit();
	BString<4096> buf;
	strcpy(buf,"");
	if( unit )
	{
		float pSpeed=0;
		const Path &path=unit->GetPath();
		if( path.Size()>=2 )
		{
			float cost=path.CostAtPos(Position());
			pSpeed=path.SpeedAtCost(cost);
		}
		sprintf
		(
			buf,
			"Spd %.1f (path %d: v:%.1f, vMax:%.1f: t:%.2f%s)",
			ModelSpeed().Z()*3.6,path.Size(),pSpeed*3.6,_limitSpeed*3.6,
			Glob.time-path.GetSearchTime(),
			path.GetOnRoad() ? " Road" : ""
		);
	}
	if( GetStopped() ) strcat(buf," STOP");
	strcat(buf," ");
	strcat(buf,FindEnumName(_prec));

	if (!IsLocal())
	{
		if (Glob.time-_lastUpdateTime>0.100)
		{
			BString<256> add;
			sprintf(add," Last update %.3f",Glob.time-_lastUpdateTime);
			strcat(buf,add);
		}
		if (CheckPredictionFrozen())
		{
			strcat(buf," Frozen");
		}
	}

	Shape *hitShape=_shape->HitpointsLevel();
	if( hitShape )
	{
		//bool first=true;
		// report dammages
#if _ENABLE_CHEATS
		if (CHECK_DIAG(DEDammage))
		{
			char hits[512];
			hits[0]=0;
			if (GetRawTotalDammage()>0)
			{
				sprintf(hits+strlen(hits)," Tot:%.2f",GetRawTotalDammage());
			}
			const HitPointList &hitpoints=GetType()->GetHitPoints();
			for( int i=0; i<hitpoints.Size(); i++ )
			{
				const HitPoint &hit=*hitpoints[i];
				int index=hit.GetSelection();
				if( index<0 ) continue;
				const NamedSelection &sel=hitShape->NamedSel(index);
				float hitVal=_hit[i];
				if( hitVal>0.3 )
				{
					sprintf(hits+strlen(hits)," %s:%.2f",sel.Name(),hitVal);
				}
			}
			strcat(buf,hits);
		}
#endif
	}

	return (const char *)buf;
}

/*!
Check what axis is contoled by what peripheral.
Principal axes are:
	mouse cursor
	thrust

\patch_internal 1.11 Date 7/27/2001 by Ondra.
Different control device detection method
(decentralized, vehicle simulation is responsible for decision).
*/



inline PackedColor CostToColor(float cost)
{
	static const float int1 = 1.0;
	static const float invint1 = (1.0 / int1);
	static const float int2 = 4.0;
	static const float invint2 = (1.0 / int2);
	static const float int12 = int1 + int2;

	static const float alpha = 0.5;
	static const Color colorR(1, 0.5f, 0, alpha);
	static const Color colorG(0, 1, 0, alpha);
	static const Color colorY(1, 1, 0, alpha);

	saturateMin(cost, int12);
	if (cost <= int1)
	{
		return PackedColor
		(
			colorY * (cost * invint1)  + colorG * (1 - cost * invint1)
		);
	}
	else
	{
		cost -= int1;
		return PackedColor
		(
			colorR * (cost * invint2) + colorY * (1 - cost * invint2)
		);
	}
}

#include "objLine.hpp"

void EntityAI::DrawDiags()
{
#if _ENABLE_CHEATS
	AIUnit *unit=CommanderUnit();
	if( !unit )
	{
		base::DrawDiags();
		return;
	}
	// draw object target
	LODShapeWithShadow *forceArrow=GScene->ForceArrow();

	#if 1
	
	if
	(
		CHECK_DIAG(DEPath) &&
		!GetType()->IsKindOf(GLOB_WORLD->Preloaded(VTypeStatic)) &&
		!_isDead &&
		!(GWorld->PlayerManual() && GWorld->PlayerOn() && GWorld->PlayerOn()->Brain() == PilotUnit())
	)
	{


		{
			Point3 steerPos=SteerPoint(GetSteerAheadSimul(),GetSteerAheadPlan());
			//AvoidCollision(steerPos);

			steerPos += DirectionAside()*_avoidAside;

			Ref<Object> arrow=new ObjectColored(forceArrow,-1);
			Point3 pos=steerPos;

			float size=0.6;
			arrow->SetPosition(pos);
			arrow->SetOrient(VUp,VForward);
			arrow->SetPosition
			(
				arrow->PositionModelToWorld(forceArrow->BoundingCenter()*size)
			);
			arrow->SetScale(size);
			arrow->SetConstantColor(PackedColor(Color(0.7,0.5,0)));
			//#define DRAW_OBJ(obj) (obj)->Draw(0,ClipAll,*(obj))
			#define DRAW_OBJ(obj) GScene->ObjectForDrawing(obj); 
			DRAW_OBJ(arrow);
		}

		{
			Point3 steerPos=SteerPoint(GetPredictTurnSimul(),GetPredictTurnPlan());

			Ref<Object> arrow=new ObjectColored(forceArrow,-1);
			Point3 pos=steerPos;

			float size=0.4;
			arrow->SetPosition(pos);
			arrow->SetOrient(VUp,VForward);
			arrow->SetPosition
			(
				arrow->PositionModelToWorld(forceArrow->BoundingCenter()*size)
			);
			arrow->SetScale(size);
			arrow->SetConstantColor(PackedColor(Color(0.9,0.7,0.3)));
			DRAW_OBJ(arrow);

		}


		if (_hideBehind)
		{
			Ref<Object> arrow=new ObjectColored(forceArrow,-1);
			Point3 pos=_hideBehind->Position();

			float size=1.5;
			arrow->SetPosition(pos);
			arrow->SetOrient(VUp,VForward);
			arrow->SetPosition
			(
				arrow->PositionModelToWorld(forceArrow->BoundingCenter()*size)
			);
			arrow->SetScale(size);
			arrow->SetConstantColor(PackedColor(Color(0,0,0,0.5)));
			DRAW_OBJ(arrow);

		}

		if( unit->IsSubgroupLeader() )
		{
			{ // draw strategic GoTo destination
				Ref<Object> arrow=new ObjectColored(forceArrow,-1);

				float size=0.5;
				if( GLOB_WORLD->CameraOn()==this ) size=2;
				arrow->SetPosition(_stratGoToPos);
				arrow->SetOrient(VUp,VForward);
				arrow->SetPosition
				(
					arrow->PositionModelToWorld(forceArrow->BoundingCenter()*size)
				);
				arrow->SetScale(size);
				arrow->SetConstantColor(PackedColor(Color(1,1,1)));

				DRAW_OBJ(arrow);
			}
			{ // draw command destination
				AISubgroup *subgrp=unit->GetSubgroup();
				if( subgrp && subgrp->GetCommand() )
				{
					Vector3Val pos=subgrp->GetCommand()->_destination;
					Ref<Object> arrow=new ObjectColored(forceArrow,-1);

					float size=0.5;
					if( GLOB_WORLD->CameraOn()==this ) size=2;
					arrow->SetPosition(pos);
					arrow->SetOrient(VUp,VForward);
					arrow->SetPosition
					(
						arrow->PositionModelToWorld(forceArrow->BoundingCenter()*size)
					);
					arrow->SetScale(size);
					arrow->SetConstantColor(PackedColor(Color(0.5,1,1)));

					DRAW_OBJ(arrow);
				}
			}
		}
		else
		{
			AIGroup *grp = unit->GetGroup();
			AIUnit *leader = grp ? grp->Leader() : NULL;
			if( leader )
			{
				EntityAI *lVehicle=leader->GetVehicle();
				// predict leader position
				const float estT=GetFormationTime();

				float maxSpeed=lVehicle->GetType()->GetMaxSpeedMs();
				Vector3 predSpeed = lVehicle->Speed();
				float spSize2 = predSpeed.SquareSize();
				if (spSize2<Square(maxSpeed*0.5) && spSize2>Square(maxSpeed*0.1))
				{
					predSpeed.Normalize();
					predSpeed *= maxSpeed*0.5;
				}
				Vector3Val estPos=lVehicle->Position()+estT*predSpeed;

				// predict leader orientation
				Matrix4 estTransform;
				estTransform.SetPosition(estPos);
				// get formation orientation
				Vector3Val formDir=leader->GetSubgroup()->GetFormationDirection();
				estTransform.SetDirectionAndUp(formDir,VUp);
				Vector3Val formPos=unit->GetFormationRelative()-leader->GetFormationRelative();
				Vector3Val movePos=estTransform.FastTransform(formPos);

				{ // draw predicted formation position
					Vector3Val pos=movePos;
					Ref<Object> arrow=new ObjectColored(forceArrow,-1);

					float size=0.5;
					if( GLOB_WORLD->CameraOn()==this ) size=2;
					arrow->SetPosition(pos);
					arrow->SetOrient(VUp,VForward);
					arrow->SetPosition
					(
						arrow->PositionModelToWorld(forceArrow->BoundingCenter()*size)
					);
					arrow->SetScale(size);
					arrow->SetConstantColor(PackedColor(Color(0.5,1,1)));

					DRAW_OBJ(arrow);
				}

				{ // draw predicted formation position
					Ref<Object> arrow=new ObjectColored(forceArrow,-1);

					arrow->SetPosition(unit->GetFormationAbsolute());
					arrow->SetOrient(VUp,VForward);
					float size = 0.5;
					arrow->SetPosition
					(
						arrow->PositionModelToWorld(forceArrow->BoundingCenter()*size)
					);
					arrow->SetScale(size);
					arrow->SetConstantColor(PackedColor(Color(1,1,1)));

					DRAW_OBJ(arrow);
				}
			}
		}

	}
	#endif

	#if 1
		// draw current weapon direction
	if (CHECK_DIAG(DECombat))
	{
		{
			Vector3Val dir=GetWeaponDirection(0);
			if( dir.SquareSize()>0.1 )
			{
				Ref<Object> arrow=new ObjectColored(forceArrow,-1);

				float size=0.1;
				arrow->SetPosition(Position());
				arrow->SetOrient(dir,VUp);
				arrow->SetPosition
				(
					arrow->PositionModelToWorld(forceArrow->BoundingCenter()*size)
				);
				arrow->SetScale(size);
				arrow->SetConstantColor(PackedColor(Color(1,0,0,0.5)));

				DRAW_OBJ(arrow);
			}
		}
		{
			Vector3Val dir=GetWeaponDirectionWanted(0);
			if( dir.SquareSize()>0.1 )
			{
				Ref<Object> arrow=new ObjectColored(forceArrow,-1);

				float size=0.2;
				arrow->SetPosition(Position());
				arrow->SetOrient(dir,VUp);
				arrow->SetPosition
				(
					arrow->PositionModelToWorld(forceArrow->BoundingCenter()*size)
				);
				arrow->SetScale(size);
				arrow->SetConstantColor(PackedColor(Color(1,0.1,0,0.5)));

				DRAW_OBJ(arrow);
			}
		}
		{
			Vector3Val dir=GetEyeDirection();
			if( dir.SquareSize()>0.1 )
			{
				Ref<Object> arrow=new ObjectColored(forceArrow,-1);

				float size=0.4;
				arrow->SetPosition(Position());
				arrow->SetOrient(dir,VUp);
				arrow->SetPosition
				(
					arrow->PositionModelToWorld(forceArrow->BoundingCenter()*size)
				);
				arrow->SetScale(size);
				arrow->SetConstantColor(PackedColor(Color(1,1,0,0.5)));

				DRAW_OBJ(arrow);
			}
		}
	}
	#endif
	#if 1
		if( !QIsManual() && CHECK_DIAG(DECombat))
		{
			float attackSize=_attackTarget.IdExact() ? 1 : 0.3;
			{
				Ref<Object> arrow=new ObjectColored(forceArrow,-1);
				float size=attackSize;
				Vector3 aPos=_attackAggresivePos;
				arrow->SetPosition(aPos);
				arrow->SetOrient(VUp,VForward);
				arrow->SetPosition
				(
					arrow->PositionModelToWorld(forceArrow->BoundingCenter()*size)
				);
				arrow->SetScale(size);
				arrow->SetConstantColor(PackedColor(Color(1,0.5,0)));
				DRAW_OBJ(arrow);
			}
			{
				Ref<Object> arrow=new ObjectColored(forceArrow,-1);
				float size=attackSize;
				Vector3 aPos=_attackEconomicalPos;
				arrow->SetPosition(aPos);
				arrow->SetOrient(Vector3(0,-1,0),Vector3(0,0,-1));
				arrow->SetPosition
				(
					arrow->PositionModelToWorld(-forceArrow->BoundingCenter()*size)
				);
				arrow->SetScale(size);
				arrow->SetConstantColor(PackedColor(Color(1,0,0)));
				DRAW_OBJ(arrow);
			}
		}
	#endif

	if (CHECK_DIAG(DEPath))
	{
		Ref<Object> arrow=new ObjectColored(forceArrow,-1);
		float size=0.2;
		Vector3 aPos=PositionModelToWorld(GetType()->_supplyPoint);
		arrow->SetPosition(aPos);
		arrow->SetOrient(Vector3(0,-1,0),Vector3(0,0,-1));
		arrow->SetPosition
		(
			arrow->PositionModelToWorld(-forceArrow->BoundingCenter()*size)
		);
		arrow->SetScale(size);
		arrow->SetConstantColor(PackedColor(Color(0,1,0)));
		DRAW_OBJ(arrow);
	}

	// draw path
	if (PilotUnit())
	{
		if(CHECK_DIAG(DEPath))
		{
			const Path &path=PilotUnit()->GetPath();
			float size=0.05;
			PackedColor color(Color(1,0,1,1));
			if( this==GLOB_WORLD->CameraOn() ) size*=2;
			if( unit->IsSubgroupLeader() ) color=PackedColor(Color(1,1,0,1));
			for( int i=1; i<path.Size(); i++ )
			{
				Ref<LODShapeWithShadow> shape=ObjectLine::CreateShape();
				Ref<Object> lineObj=new ObjectLineDiag(shape);
				lineObj->SetConstantColor(color);

				const OperInfoResult &cur=path[i-1];
				const OperInfoResult &nxt=path[i];
				Vector3Val cPos = cur._pos;
				Vector3Val nPos = nxt._pos;
				lineObj->SetPosition(cPos);
				ObjectLine::SetPos(shape,VZero,nPos-cPos);

				GScene->ObjectForDrawing(lineObj);
			}
			for( int i=0; i<path.Size(); i++ )
			{
				const OperInfoResult &info=path[i];

				Ref<Object> arrow=new ObjectColored(forceArrow,-1);

				Vector3Val pos = info._pos;
				arrow->SetPosition(pos);
				bool revert=( i>=unit->MaxOperIndex() );
				if( revert )
				{
					arrow->SetOrient(Vector3(0,-1,0),Vector3(0,0,-1));
					arrow->SetPosition
					(
						arrow->PositionModelToWorld(-forceArrow->BoundingCenter()*size)
					);
				}
				else
				{
					arrow->SetOrient(Vector3(0,+1,0),Vector3(0,0,+1));
					arrow->SetPosition
					(
						arrow->PositionModelToWorld(forceArrow->BoundingCenter()*size)
					);
				}
				arrow->SetScale(size);
				arrow->SetConstantColor(color);

				DRAW_OBJ(arrow);

			}
		}

		if (this == GLOB_WORLD->CameraOn())
		{
			LODShapeWithShadow *rectShape = GScene->Preloaded(RectangleModel);
			int index=0;
	
			#if 1

			// draw fields costs
			if (CHECK_DIAG(DECostMap))
			{
				bool soldier = unit->IsFreeSoldier();
				int x, xLand = toIntFloor(Position().X() * InvLandGrid);
				int z, zLand = toIntFloor(Position().Z() * InvLandGrid);
				for (z=zLand-1; z<=zLand+1; z++)
					for (x=xLand-1; x<=xLand+1; x++)
					{
						Ref<OperField> fld = GLOB_LAND->OperationalCache()->GetOperField
						(
							x, z, MASK_AVOID_OBJECTS|MASK_PREFER_ROADS
						);
						if (!fld) continue;
						int xx, zz;
						for (zz=0; zz<OperItemRange; zz++)
							for (xx=0; xx<OperItemRange; xx++)
							{
								PackedColor color;
								OperItemType type = 
								(
									soldier ?
									fld->_items[zz][xx]._typeSoldier :
									fld->_items[zz][xx]._type
								);
								#if 0
								float unaccLimit = soldier ? 4.8 : GET_UNACCESSIBLE;
								float cost = GetTypeCost(type);
								if (cost >= unaccLimit)
									color = PackedColor(Color(1,0,0,1));
								else
									color = CostToColor(cost);
								#else
								static const PackedColor typeColor[NOperItemType]=
								{
									PackedColor(Color(1,  1,  1,  0)), // OITNormal,
									PackedColor(Color(0,  0.5,0.5,1)), // OITAvoidBush,
									PackedColor(Color(0,  0.5,0,  1)), // OITAvoidTree,
									PackedColor(Color(0.5,0.5,0.5,1)), // OITAvoid,
									PackedColor(Color(0,  0,  1  ,1)), // OITWater,
									PackedColor(Color(1,  0,  1  ,1)), // OITSpaceRoad,
									PackedColor(Color(1,  1,  1  ,1)), // OITRoad,
									PackedColor(Color(0,  1,  1  ,1)), // OITSpaceBush,
									PackedColor(Color(0,  1,  0  ,1)), // OITSpaceTree,
									PackedColor(Color(0,  0,  0  ,1)), // OITSpace
									PackedColor(Color(1,  1,  0  ,1)), // OITRoadForced
								};
								color = typeColor[type];

								#endif

								Vector3 pos;
								pos.Init();
								pos[0] = x * LandGrid + xx * OperItemGrid + 0.5 * OperItemGrid;
								pos[2] = z * LandGrid + zz * OperItemGrid + 0.5 * OperItemGrid;
								pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0],pos[2]) + 0.2;

								if ((pos - Position()).SquareSizeXZ() <= Square(50))
								{
									Ref<Object> rect;
									if( index<MapDiags.Size() ) rect=MapDiags[index];
									else
									{
										rect = new ObjectColored(rectShape, -1);
										MapDiags.Access(index);
										MapDiags[index]=rect;
									}
									index++;
									rect->SetPosition(pos);
									rect->SetConstantColor(color);
									GScene->ObjectForDrawing(rect);
								}
							}
					}
				}
			#endif

			// done: draw field costs
			#if DIAG_ROAD
			// draw road connections
			{
				int x, xLand = toIntFloor(Position().X() * InvLandGrid);
				int z, zLand = toIntFloor(Position().Z() * InvLandGrid);
				for (z=zLand-1; z<=zLand+1; z++) for (x=xLand-1; x<=xLand+1; x++)
				if( InRange(x,z) )
				{
					RoadList &list=GRoadNet->GetRoadList(x,z);
					for( int r=0; r<list.Size(); r++ )
					{
						RoadLink *road=list[r];
						// draw beg-end connection
						if( road->NConnections()>=2 )
						{
							Vector3Val beg=road->PosConnections()[0];
							Vector3Val end=road->PosConnections()[1];
							bool begCon=road->Connections()[0]!=0;
							bool endCon=road->Connections()[1]!=0;
							// draw arrow from beg to end

							if( !begCon )
							{
								Ref<Object> arrow=new ObjectColored(forceArrow,-1);

								arrow->SetPosition(beg+Vector3(0,1.5,0));
								arrow->SetOrient(VUp,VAside);
								float size=0.05;
								PackedColor color(Color(1,0,1,0.5));
								arrow->SetPosition
								(
									arrow->PositionModelToWorld(-forceArrow->BoundingCenter()*size)
								);
								arrow->SetScale(size);
								arrow->SetConstantColor(color);
								DRAW_OBJ(arrow);
							}
							if( !endCon )
							{
								Ref<Object> arrow=new ObjectColored(forceArrow,-1);
								arrow->SetPosition(end+Vector3(0,1.5,0));
								arrow->SetOrient(VUp,VAside);
								float size=0.05;
								PackedColor color(Color(1,0,1,0.5));
								arrow->SetPosition
								(
									arrow->PositionModelToWorld(-forceArrow->BoundingCenter()*size)
								);
								arrow->SetScale(size);
								arrow->SetConstantColor(color);
								DRAW_OBJ(arrow);
							}

							Ref<Object> arrow=new ObjectColored(forceArrow,-1);

							arrow->SetPosition(beg+VUp);
							arrow->SetOrient(beg-end,VUp);
							float size=end.Distance(beg)*0.1;
							if( size>0.001 )
							{
								PackedColor color;
								if (road->IsLocked())
									color = PackedColor(Color(1,0.2,0,0.5));
								else
									color = PackedColor(Color(1,0,1,0.5));
								arrow->SetPosition
								(
									arrow->PositionModelToWorld(-forceArrow->BoundingCenter()*size)
								);
								arrow->SetScale(size);
								arrow->SetConstantColor(color);

								DRAW_OBJ(arrow);
							}
							else
							{
								Fail("Beg..end same");
							}
						}
					}
				}
			}
			#endif

			#if 1
			if (CHECK_DIAG(DELockMap))
			{
				int operX=toInt(Position().X()*InvOperItemGrid);
				int operZ=toInt(Position().Z()*InvOperItemGrid);
				ILockCache *locks=GLandscape->LockingCache();
				const int range=50;
				for( int x=operX-range; x<=operX+range; x++ )
				for( int z=operZ-range; z<=operZ+range; z++ )
				{
					bool lockV=locks->IsLocked(x,z,false);
					bool lockS=locks->IsLocked(x,z,true);

					if( !lockV && !lockS ) continue;

					PackedColor color(Color(lockV,0,lockS));

					Vector3 pos;
					pos.Init();
					pos[0] = x * OperItemGrid + 0.5 * OperItemGrid;
					pos[2] = z * OperItemGrid + 0.5 * OperItemGrid;
					pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0],pos[2])+0.2;

					if ((pos - Position()).SquareSizeXZ() <= Square(50))
					{
						Ref<Object> rect;
						if( index<MapDiags.Size() ) rect=MapDiags[index];
						else
						{
							rect = new ObjectColored(rectShape, -1);
							MapDiags.Access(index);
							MapDiags[index]=rect;
						}
						index++;
						rect->SetPosition(pos);
						rect->SetConstantColor(color);
						GScene->ObjectForDrawing(rect);
					}
				}
			}
			#endif
		}
	}

	#if DIAG_VEHICLE
	base::DrawDiags();
	#endif
#endif
}

Matrix4 EntityAI::AnimateProxyMatrix( int level, const ProxyObject &proxy ) const
{
	Object *obj = proxy.obj;
	LODShapeWithShadow *lodShape = obj->GetShape();

	Matrix4 proxyTransform = obj->Transform();
	proxyTransform.SetPosition(proxyTransform.FastTransform(lodShape->BoundingCenter()));

	AnimateMatrix(proxyTransform,level,proxy.selection);

	return proxyTransform;
}

int EntityAI::GetProxyComplexity
(
	int level, const FrameBase &pos, float dist2
) const
{
	// TODO: calculate flag
	return 0;
}


void EntityAI::DrawProxies
(
	int level, ClipFlags clipFlags,
	const Matrix4 &transform, const Matrix4 &invTransform,
	float dist2, float z2, const LightList &lights
)
{
	// draw flag
	Shape *sShape = _shape->LevelOpaque(level);
	for (int i=0; i<sShape->NProxies(); i++)
	{
		const ProxyObject &proxy = sShape->Proxy(i);
		Object *obj = proxy.obj;
		const EntityType *type = obj->GetVehicleType();
		if (!type) continue;
		RString simulation = type->_simName;
		if (strcmp(simulation, "alwaysshow") == 0)
		{
			// smart clipping par of obj->Draw
			Matrix4Val pTransform = transform * obj->Transform();
			Matrix4Val invPTransform = proxy.invTransform * invTransform;

			// LOD detection
			LODShapeWithShadow *pshape = obj->GetShapeOnPos(pTransform.Position());
			if (!pshape) continue;
			int level = GScene->LevelFromDistance2
			(
				pshape, dist2, pTransform.Scale(),
				pTransform.Direction(), GScene->GetCamera()->Direction()
			);
			if (level == LOD_INVISIBLE) continue;

			FrameWithInverse pFrame(pTransform, invPTransform);

			// construct FrameWithInverse from transform and invTransform

			obj->Draw(level, ClipAll, pFrame);
		}
		else if (_flag && _showFlag)
		{
/*
			LODShapeWithShadow *lodShape = obj->GetShape();
			if (strnicmp(lodShape->Name(), "data3d\\flag_", 12) == 0)
*/
			if (stricmp(simulation, "flag") == 0)
			{
				LODShapeWithShadow *lodShape = obj->GetShape();

				Matrix4 proxyTransform = AnimateProxyMatrix(level,proxy);

				// TODO: smart clipping
				Matrix4Val pTransform = transform * proxyTransform;
				Matrix4Val invPTransform = pTransform.InverseScaled();

				int pLevel = GScene->LevelFromDistance2
				(
					lodShape, dist2, pTransform.Scale(),
					pTransform.Direction(), GScene->GetCamera()->Direction()
				);
				if (pLevel == LOD_INVISIBLE) continue;

				_flag->SetFlagTexture(GetFlagTexture());

				FrameWithInverse pFrame(pTransform,invPTransform);

				_flag->FlagAnimate(pFrame,pLevel);
				Shape *shape = lodShape->LevelOpaque(pLevel);
				shape->PrepareTextures(z2, shape->Special());
				shape->Draw
				(
					this, lights,
					ClipAll, shape->Special(),
					pTransform, invPTransform
				);
				_flag->FlagDeanimate(pFrame,pLevel);
			}
		}
	}
}

Texture *EntityAI::GetCursorTexture(Person *person)
{
	// check actual weapon
	int weapon = SelectedWeapon();
	if (weapon<0 || weapon>NMagazineSlots())
	{
		// return no weapon cursor
		return NULL;
	}
	const MagazineSlot &slot = GetMagazineSlot(weapon);
	const MuzzleType *muzzle = slot._muzzle;
	return muzzle->_cursorTexture;
}

Texture *EntityAI::GetCursorAimTexture(Person *person)
{
	// check actual weapon
	int weapon = SelectedWeapon();
	if (weapon<0 || weapon>NMagazineSlots())
	{
		// return no weapon cursor
		return NULL;
	}
	const MagazineSlot &slot = GetMagazineSlot(weapon);
	const MuzzleType *muzzle = slot._muzzle;
	return muzzle->_cursorAimTexture;
}


/*!
\note Function is currently not used
*/

PackedColor EntityAI::GetCursorColor(Person *person)
{
	return PackedWhite;
}

/*!
\note
Object::Draw2D is used to draw this model
*/

LODShapeWithShadow *EntityAI::GetOpticsModel(Person *person)
{
	return NULL;
}

/*!
Example of situation when true is returned is when binocular is used.
Any other weapon that can be used only through optics could do the same.
*/

bool EntityAI::GetForceOptics(Person *person) const
{
	return false;
}

PackedColor EntityAI::GetOpticsColor(Person *person)
{
	return PackedBlack;
}

Texture *EntityAI::GetFlagTexture()
{
	return NULL;
}

Texture *EntityAI::GetFlagTextureInternal()
{
	return NULL;
}

void EntityAI::SetFlagTexture(RString name)
{
}

Person *EntityAI::GetFlagOwner()
{
	return NULL;
}

void EntityAI::SetFlagOwner(Person *veh)
{
}

TargetSide EntityAI::GetFlagSide() const
{
	return TSideUnknown;
}

void EntityAI::SetFlagSide(TargetSide side)
{
}

void EntityAI::GetMaterial(TLMaterial &mat, int index) const
{
	// IAnimator interface implementation
	// check special materials

	if (index>0 && index<200)
	{
		// expected materials range in 50..69 (range 20

		// offset +20 -> shining
		// offset +40 -> in shadow
		bool shining = false;
		bool inShadow = false;
		if (index>=90)
		{
			inShadow = true;
			index -= 40;
		}
		if (index>=70)
		{
			shining = true;
			index -= 20;
		}

		const EntityAIType *type = GetType();
		// search hits to find this material
		const HitPointList &hits = type->GetHitPoints();
		for (int i=0; i<hits.Size(); i++)
		{
			if (hits[i]->GetMaterial()==index)
			{
				// dammaged material
				float dammage = GetHitCont(*hits[i]);
				float brightFactor = 1-dammage*0.85;
				Color color(brightFactor,brightFactor,brightFactor);

#if _ENABLE_CHEATS
				if (CHECK_DIAG(DEDammage))
				{
					color = Color(dammage,1-dammage,0);
				}
#endif

				color = color*GEngine->GetAccomodateEye();
				if (shining)
				{
					CreateMaterialShining(mat,color);
				}
				else if (inShadow)
				{
					CreateMaterialConstant(mat,color,0);
				}
				else
				{
					CreateMaterialNormal(mat,color);
				}
				return;
			}
		}
	}
	base::GetMaterial(mat,index);
}

Vector3 EntityAI::ExternalCameraPosition( CameraType camType ) const
{
	return GetType()->_extCameraPosition;
}

void EntityAI::LimitVirtual
(
	CameraType camType, float &heading, float &dive, float &fov
) const
{
	//if( camType==CamInternal ) fov=GetType()->_fov;
	if (camType==CamInternal)
	{
		GetType()->_viewPilot.LimitVirtual(camType,heading,dive,fov);
	}
	else
	{
		base::LimitVirtual(camType,heading,dive,fov);
	}
}

void EntityAI::InitVirtual
(
	CameraType camType, float &heading, float &dive, float &fov
) const
{
	if( camType==CamInternal )
	{
		GetType()->_viewPilot.InitVirtual(camType,heading,dive,fov);
	}
	else
	{
		base::InitVirtual(camType,heading,dive,fov);
	}
}

float EntityAI::GetAmmoCost() const
{
	// sum-up ammo state
	float curAmmo =	0;
	int i, n = NMagazines();
	for (i=0; i<n; i++)
	{
		const Magazine *magazine = GetMagazine(i);
		if (!magazine) continue;
		if (magazine->_ammo == 0) continue;
		const MagazineType *type = magazine->_type;
		if (!type) continue;
		if (type->_modes.Size() == 0) continue;
		const AmmoType *ammo = type->_modes[0]->_ammo;
		if (!ammo) continue;
		curAmmo += magazine->_ammo * ammo->cost;
	}
	return curAmmo;
}
float EntityAI::GetAmmoHit() const
{
	// sum-up ammo state
	float curAmmo =	0;
	int i, n = NMagazines();
	for (i=0; i<n; i++)
	{
		const Magazine *magazine = GetMagazine(i);
		if (!magazine) continue;
		if (magazine->_ammo == 0) continue;
		const MagazineType *type = magazine->_type;
		if (!type) continue;
		if (type->_modes.Size() == 0) continue;
		const AmmoType *ammo = type->_modes[0]->_ammo;
		if (!ammo) continue;
		curAmmo += magazine->_ammo * ammo->hit;
	}
	return curAmmo;
}
float EntityAI::GetMaxAmmoCost() const
{
	// sum-up ammo state
	float curAmmo =	0;
	int i, n = NMagazines();
	for (i=0; i<n; i++)
	{
		const Magazine *magazine = GetMagazine(i);
		if (!magazine) continue;
		const MagazineType *type = magazine->_type;
		if (!type) continue;
		if (type->_maxAmmo == 0) continue;
		if (type->_modes.Size() == 0) continue;
		const AmmoType *ammo = type->_modes[0]->_ammo;
		if (!ammo) continue;
		curAmmo += type->_maxAmmo * ammo->cost;
	}
	return curAmmo;
}
float EntityAI::GetMaxAmmoHit() const
{
	// sum-up ammo state
	float curAmmo =	0;
	int i, n = NMagazines();
	for (i=0; i<n; i++)
	{
		const Magazine *magazine = GetMagazine(i);
		if (!magazine) continue;
		const MagazineType *type = magazine->_type;
		if (!type) continue;
		if (type->_maxAmmo == 0) continue;
		if (type->_modes.Size() == 0) continue;
		const AmmoType *ammo = type->_modes[0]->_ammo;
		if (!ammo) continue;
		curAmmo += type->_maxAmmo * ammo->hit;
	}
	return curAmmo;
}

float EntityAI::Rearm( float resources )
{
	resources += _rearmCredit;
	// sum-up ammo state
	int i, n = NMagazines();
	for (i=0; i<n; i++)
	{
		Magazine *magazine = GetMagazine(i);
		if (!magazine) continue;
		const MagazineType *type = magazine->_type;
		if (!type) continue;
		if (type->_maxAmmo == 0) continue;
		if (type->_modes.Size() == 0) continue;
		const AmmoType *ammo = type->_modes[0]->_ammo;
		if (!ammo) continue;
		float cost = ammo->cost;
		int need = type->_maxAmmo - magazine->_ammo;
		if (need * cost > resources)
			need = toIntFloor(resources / cost);
		if (need == 0) continue;
		
		magazine->_ammo += need;
		resources -= need * cost;
		// ?? TODO: ?? change _reload
	}
	_rearmCredit = resources; // accumulate what's rest
	return resources;
}

float EntityAI::NeedsRearm() const
{
	if (IsDammageDestroyed()) return 0;

	// sum-up ammo state
	float minWeapon = 1.0;
	int i, n = NMagazines();
	for (i=0; i<n; i++)
	{
		const Magazine *magazine = GetMagazine(i);
		if (!magazine) continue;
		const MagazineType *type = magazine->_type;
		if (!type) continue;
		if (type->_maxAmmo == 0) continue;
		float ratio = (float)magazine->_ammo / type->_maxAmmo;
		saturateMin(minWeapon, ratio * 1.5);
	}
	saturate(minWeapon, 0, 1);

	float max = GetMaxAmmoHit();
	if (max <= 0) return 0;
	float act = GetAmmoHit();
	return 1.0 - floatMin(act / max, minWeapon);
}

float EntityAI::NeedsAmbulance() const
{
	if( !CommanderUnit() ) return 0;
	return CommanderUnit()->GetPerson()->NeedsAmbulance();
}

float EntityAI::NeedsRepair() const
{
	if (IsDammageDestroyed()) return 1;
	return GetTotalDammage();
}

float EntityAI::NeedsRefuel() const
{
	if (IsDammageDestroyed()) return 0;
	float max=GetType()->GetFuelCapacity();
	if( max<=0 ) return 0;
	float act=GetFuel();
	return 1-act/max;
}

float EntityAI::NeedsInfantryRearm() const {return 0;}

float EntityAI::NeedsLoadFuel() const {return 0;}
float EntityAI::NeedsLoadAmmo() const {return 0;}
float EntityAI::NeedsLoadRepair() const {return 0;}

bool EntityAI::IsMoveTarget() const
{
	// no vehicles with AI may be move targets
	return false;
	//return CommanderUnit()==NULL && PilotUnit()==NULL;
}

// accuracy simulation

const EntityAIType *EntityAI::GetTypeAtLeast( float accuracy ) const
{
	// start with certain information
	const EntityType *type=GetType();
	while( type->_parentType.NotNull() && type->_parentType->_accuracy>=accuracy  )
	{
		type=type->_parentType;
	}
	const EntityAIType *vType=dynamic_cast<const EntityAIType *>(type);
	if( !vType )
	{
		Fail("Non-ai type");
	}
	return vType;
}

const EntityAIType *EntityAI::GetType( float accuracy ) const
{
	// start with certain information
	const EntityType *type=GetType();
	while( type->_parentType.NotNull() && type->_accuracy>=accuracy  )
	{
		type=type->_parentType;
	}
	Assert (dynamic_cast<const EntityAIType *>(type));
	const EntityAIType *vType=static_cast<const EntityAIType *>(type);
	return vType;
}

TargetSide EntityAI::GetTargetSide() const
{
	if (_isDead) return TCivilian;

	AIUnit *unit=CommanderUnit();
	if (unit)
	{
		if (unit->GetPerson()->GetExperience() < ExperienceRenegadeLimit)
		{
			// he is crazy - shooting at friendlies - enemy to all sides
			return TEnemy;
		}
	}

	return base::GetTargetSide();
}

/*
TargetSide EntityAI::GetTargetSide() const
{
	if ( !CommanderUnit() && GetTotalDammage()<1 && !_fake ) return TCivilian; // empty - report neutral
	return base::GetTargetSide();
}
*/

TargetSide EntityAI::GetTargetSide( float accuracy ) const
{
	AIUnit *unit=CommanderUnit();
	if (accuracy<1.5)
	{
		const EntityAIType *type = GetType(accuracy);
		TargetSide side = type->_typicalSide;
		if (side!=TSideUnknown)
		{
			if (unit && unit->GetCaptive()) return TCivilian;
		}
		return side;
	}
	if (unit && unit->GetCaptive()) return TCivilian;
	TargetSide side = GetTargetSide();
	if (unit)
	{
		if (unit->GetPerson()->GetExperience() < ExperienceRenegadeLimit)
		{
			// he is crazy - shooting at friendlies - enemy to all sides
			return TEnemy;
		}
	}
	return side;
}

RString EntityAI::GetDebugName() const
{
	if( CommanderUnit() ) return CommanderUnit()->GetDebugName();
	char ptr[256];
	sprintf(ptr,"%x# ",this);
	return RString(ptr)+base::GetDebugName()+(IsLocal()?RString():RString(" REMOTE"));
}

void EntityAI::ResetStatus()
{
	_isDead = false;
	_aimObserverAsked = VZero;
	_aimWeaponAsked.Clear();
	_sensorColID=SensorColID(-1);
	for( int i=0; i<_hit.Size(); i++ ) _hit[i]=0;
	_visTracker.Clear();
	for (int i=0; i<NEntityEvent; i++)
	{
		_eventHandlers[i].Clear();
	}
	base::ResetStatus();
}


static const EnumName FireStateNames[]=
{
	EnumName(EntityAI::FireInit,"INIT"),
	EnumName(EntityAI::FireAim,"AIM"),
	EnumName(EntityAI::FireAimed,"AIMED"),
	EnumName(EntityAI::FireDone,"DONE"),
	EnumName()
};

template<>
const EnumName *GetEnumNames( EntityAI::FireState dummy ) {return FireStateNames;}

static const EnumName TargetStateNames[]=
{
	EnumName(TargetDestroyed,"DESTROYED"),
	EnumName(TargetAlive,"ALIVE"),
	EnumName(TargetEnemyEmpty,"ENEMYEMPTY"),
	EnumName(TargetEnemy,"ENEMY"),
	EnumName(TargetEnemyCombat,"COMBAT"),
	EnumName()
};

template<>
const EnumName *GetEnumNames(TargetState dummy)
{
	return TargetStateNames;
}

LSError UserActionDescription::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("id", id, 1))
	CHECK(ar.Serialize("text", text, 1, ""))
	CHECK(ar.Serialize("script", script, 1, ""))

	return LSOK;
}

/*!
\patch_internal 1.23 Date 09/11/2001 by Ondra
- New: Some more flags serialized.
\patch 1.93 Date 8/24/2003 by Ondra
- Fixed: Event handlers were not saved properly.
\patch_internal 1.94 Date 10/10/2003 by Jirka
- Fixed: Avoid putting magazines into incompatible weapons
*/

LSError EntityAI::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	if (ar.IsSaving())
	{
		RString type = GetType()->GetName();
		CHECK(ar.Serialize("type", type, 1))
	}
	if (!IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
	{
		bool moveOut = (MoveOutState)_moveOutState>MOIn;
		DoAssert((MoveOutState)_moveOutState!=MOMovingOut);
		CHECK(ar.Serialize("moveOut", moveOut, 1, false))
		if (ar.IsLoading())
		{
			_moveOutState = moveOut ? MOMovedOut : MOIn;
		}

		CHECK(ar.Serialize("stratGoToPos", _stratGoToPos, 1, VZero))
	}
	CHECK(ar.SerializeArray("hit", _hit, 1))
	if (!IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
	{
		CHECK(ar.Serialize("limitSpeed", _limitSpeed, 1, 0))
		CHECK(ar.Serialize("fireMode", _fire._fireMode, 1, -1))
		CHECK(ar.Serialize("firePrepareOnly", _fire._firePrepareOnly, 1, true))
		CHECK(ar.SerializeRef("fireTarget", _fire._fireTarget, 1))
		CHECK(ar.SerializeEnum("fireState", _fireState, 1, FireDone))
		CHECK(ar.Serialize("fireStateDelay", _fireStateDelay, 1))
	}
	SerializeBitBool(ar, "isDead", _isDead, 1, false)
	if (!IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
	{
		CHECK(ar.Serialize("sensorColID", _sensorColID, 1, -1))

		CHECK(ar.Serialize("forceFireWeapon", _forceFireWeapon, 1, -1))

		CHECK(ar.SerializeRef("attackTarget", _attackTarget, 1))
		CHECK(ar.Serialize("attackAggresivePos", _attackAggresivePos, 1, VZero))
		CHECK(ar.Serialize("attackEconomicalPos", _attackEconomicalPos, 1, VZero))
		CHECK(ar.Serialize("lastShotAtAssignedTarget", _lastShotAtAssignedTarget, 1, Time(0)))
		CHECK(ar.Serialize("lastShotTime", _lastShotTime, 1, Time(0)))
		CHECK(ar.SerializeRef("lastShot", _lastShot, 1))
		//mutable Time _attackEngageTime; // how is this information old
		//mutable Time _attackRefreshTime; // last refresh of _attackXXXResult
		//mutable FireResult _attackAggresiveResult;
		//mutable FireResult _attackEconomicalResult;

		CHECK(ar.SerializeRef("hideTarget", _hideTarget, 1))
		CHECK(ar.SerializeRef("hideBehind", _hideBehind, 1))
	#if _ENABLE_CHEATS
		CHECK(ar.Serialize("allowDammage", _allowDammage, 1, true))
	#endif
		CHECK(ar.Serialize("hideRefreshTime", _hideRefreshTime, 1, Glob.time))

		CHECK(ar.Serialize("newTargetsTime", _newTargetsTime, 1, Glob.time))
		CHECK(ar.Serialize("trackTargetsTime", _trackTargetsTime, 1, Glob.time))
		CHECK(ar.Serialize("trackNearTargetsTime", _trackNearTargetsTime, 1, Glob.time))

		SerializeBitBool(ar,"landContact",_landContact,1,false); // TODO: default is true
		SerializeBitBool(ar,"objectContact",_objectContact,1,false);
		SerializeBitBool(ar,"objectContact",_waterContact,1,false);
		SerializeBitBool(ar,"inFormation",_inFormation,1,true);

		CHECK(ar.Serialize("isStopped", _isStopped, 1, false))
		CHECK(ar.Serialize("upsideDown", _isUpsideDown, 1, false))

		CHECK(ar.Serialize("nextUserActionId", _nextUserActionId, 1, 0))
		CHECK(ar.Serialize("UserActions", _userActions, 1))
		for (int i=0; i<NEntityEvent; i++)
		{
      const char *eventName = FindEnumName((EntityEvent)i);
      BString<80> eHandlerName;
      sprintf(eHandlerName,"EventHandlers%s",eventName);
			CHECK(ar.SerializeArray(eHandlerName.cstr(), _eventHandlers[i], 1))
		}
	}

	// serialize weapons
	if (ar.GetArVersion() >= 7)
	{
		if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
		{
			RemoveAllWeapons();
			RemoveAllMagazines();
		}
		CHECK(ar.Serialize("Weapons", _weapons, 1));
		if (!ar.IsSaving() && ar.GetPass() == ParamArchive::PassSecond)
		{
			// remove NULLs
			for (int i=0; i<_weapons.Size();)
				if (_weapons[i]) i++;
				else _weapons.Delete(i);
		}
		CHECK(ar.Serialize("Magazines", _magazines, 1));
		if (ar.IsSaving())
		{
			AutoArray<int> slots;
			int n = _magazineSlots.Size();
			slots.Resize(n);
			for (int i=0; i<n; i++)
			{
				slots[i] = -1;
				Magazine *magazine = _magazineSlots[i]._magazine;
				if (magazine)
				{
					for (int j=0; j<_magazines.Size(); j++)
					{
						if (_magazines[j] == magazine)
						{
							slots[i] = j;
							break;
						}
					}
				}
			}
			CHECK(ar.SerializeArray("magazineSlots", slots, 1));
		}
		else if (ar.GetPass() == ParamArchive::PassSecond)
		{
			int n = 0;
			for (int i=0; i<_weapons.Size(); i++)
			{
				WeaponType *weapon = _weapons[i];
				for (int j=0; j<weapon->_muzzles.Size(); j++)
				{
					MuzzleType *muzzle = weapon->_muzzles[j];
					n += muzzle->_nModes;
				}
			}
			_magazineSlots.Resize(n);
			int slot = 0;
			for (int i=0; i<_weapons.Size(); i++)
			{
				WeaponType *weapon = _weapons[i];

				// load shape
				weapon->ShapeAddRef();

				for (int j=0; j<weapon->_muzzles.Size(); j++)
				{
					MuzzleType *muzzle = weapon->_muzzles[j];
					Magazine *magazine = NULL;
					for (int mode=0; mode<muzzle->_nModes; mode++)
					{
						_magazineSlots[slot]._weapon = weapon;
						_magazineSlots[slot]._muzzle = muzzle;
						_magazineSlots[slot]._mode = mode;
						_magazineSlots[slot]._magazine = magazine;
						slot++;
					}
				}
			}
			AutoArray<int> slots;
			ar.FirstPass();
			CHECK(ar.SerializeArray("magazineSlots", slots, 1));
			Assert(slots.Size() == n);
			for (int i=0; i<n; i++)
			{
        MagazineSlot &slot = _magazineSlots[i];
        int index = slots[i];
        if (index < 0 || index >= _magazines.Size()) continue;
        Magazine *magazine = _magazines[index];
        if (!magazine || !magazine->_type) continue;
        if (!slot._muzzle->CanUse(magazine->_type))
        {
          RptF
          (
            "Cannot use magazine %s in muzzle %s",
            (const char *)magazine->_type->GetName(),
            (const char *)slot._muzzle->GetName()
          );
          continue;
        }
        slot._magazine = magazine;
			}
			ar.SecondPass();

			if (n > 0 && _currentWeapon < 0) SelectWeapon(0, true);
			AutoReloadAll();
		}
	}

	// must be after RemoveAllWeapons
	if (!IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
	{
		int currentWeapon = _currentWeapon;
		CHECK(ar.Serialize("currentWeapon", currentWeapon, 1, -1))
		if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
		{
			SelectWeapon(currentWeapon, true);
		}
	}

	// TODO: check and remove _condition
	return LSOK;

/* TODO: need serialization ?
	
	Vector3 _lockedBeg; // where is the lock?

	// locking
	bool _locked :1,_tempLocked:1; // is locked?
	bool _isUpsideDown:1;
	bool _lockedSoldier; // type of lock
	Time _lastMovement; // when last movement prevented stopping
	float _shootVisible; // how much are we visible (1.0 default)
	float _shootTimeRest; // how long will current visibility last (before returning to 1.0)
	OLink<Object> _shootTarget; // what we fired at
	float _lockedRadius; // how large is the lock?
	//Ref<AILocker> _locker; // who locked - no real AI ...
	Ptr<AILocker> _locker; // who locked - no real AI ...
	SensorColID _sensorColID;
	AutoArray<MuzzleState> _muzzleState;
	OLink<FlashGunFire> _gunFlash;
	mutable VisibilityTrackerCache _visTracker;
	float _rearmCredit; // used during rearm
	// GoTo/FireAt
	float _avoidAside,_avoidAsideWanted; // obstacle avoidance offset
	Time _lastSimplePath; // formation pilot helper
	LinkTarget _fireTarget;

	// current attack test status
	// set during engage
	mutable LinkTarget _attackTarget; // which target
	mutable Time _attackEngageTime; // how is this information old
	mutable Time _attackRefreshTime; // last refresh of _attackXXXResult
	mutable Vector3 _attackAggresivePos;
	mutable Vector3 _attackEconomicalPos;
	mutable FireResult _attackAggresiveResult;
	mutable FireResult _attackEconomicalResult;
	FFEffects _ff; // result of ff simulation - only in SimCamera mode

	// support for getting in/out
	float _nearestEnemy; // nearest known enemy
*/
}

NetworkMessageType EntityAI::GetNMType(NetworkMessageClass cls) const
{
	switch (cls)
	{
	case NMCUpdateDammage:
		return NMTUpdateDammageVehicleAI;
	case NMCUpdateGeneric:
		return NMTUpdateVehicleAI;
	default:
		return base::GetNMType(cls);
	}
}

class IndicesUpdateEntityAIWeapons
{
public:
	int initialUpdate;
	int currentWeapon;
	int weapons;
	int magazines;
	int magazineSlots;

	IndicesUpdateEntityAIWeapons();
	void Scan(NetworkMessageFormatBase *format);
};

IndicesUpdateEntityAIWeapons::IndicesUpdateEntityAIWeapons()
{
	initialUpdate = -1;
	currentWeapon = -1;
	weapons = -1;
	magazines = -1;
	magazineSlots = -1;
}

void IndicesUpdateEntityAIWeapons::Scan(NetworkMessageFormatBase *format)
{
	SCAN(initialUpdate)
	SCAN(currentWeapon)
	SCAN(weapons)
	SCAN(magazines)
	SCAN(magazineSlots)
}

IndicesUpdateEntityAIWeapons *GetIndicesUpdateEntityAIWeapons() {return new IndicesUpdateEntityAIWeapons();}
void DeleteIndicesUpdateEntityAIWeapons(IndicesUpdateEntityAIWeapons *weapons) {delete weapons;}

void ScanIndicesUpdateEntityAIWeapons(IndicesUpdateEntityAIWeapons *weapons, NetworkMessageFormatBase *format)
{
	weapons->Scan(format);
}

IndicesUpdateVehicleAI::IndicesUpdateVehicleAI()
{
	pilotLight = -1;
	fireTarget = -1;
	weapons = GetIndicesUpdateEntityAIWeapons();
}

IndicesUpdateVehicleAI::~IndicesUpdateVehicleAI()
{
	DeleteIndicesUpdateEntityAIWeapons(weapons);
}

void IndicesUpdateVehicleAI::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);

	SCAN(fireTarget)
	SCAN(pilotLight)
	weapons->Scan(format);
}

IndicesUpdateDammageVehicleAI::IndicesUpdateDammageVehicleAI()
{
	isDead = -1;
	hit = -1;
}

IndicesUpdateDammageVehicleAI::~IndicesUpdateDammageVehicleAI()
{
}

void IndicesUpdateDammageVehicleAI::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);

	SCAN(isDead)
	SCAN(hit)
}

NetworkMessageIndices *GetIndicesUpdateVehicleAI() {return new IndicesUpdateVehicleAI();}

NetworkMessageIndices *GetIndicesUpdateDammageVehicleAI() {return new IndicesUpdateDammageVehicleAI();}

/*!
\patch 1.24 Date 09/26/2001 by Ondra
- Fixed: vehicle light state was not transferred across network.
*/
NetworkMessageFormat &EntityAI::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	switch (cls)
	{
	case NMCUpdateDammage:
		base::CreateFormat(cls, format);
		format.Add("isDead", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Unit is destroyed (unusable)"), ET_NOT_EQUAL, ERR_COEF_STRUCTURE);
		format.Add("hit", NDTFloatArray, NCTFloat0To1, DEFVALUEFLOATARRAY, DOC_MSG("Damage of parts of entity"), ET_ABS_DIF, ERR_COEF_STRUCTURE);
		break;
	case NMCUpdateGeneric:
		base::CreateFormat(cls, format);
		format.Add("pilotLight", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Lights are on / off"), ET_NOT_EQUAL, ERR_COEF_VALUE_MAJOR);
		format.Add("fireTarget", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Selected fire target"), ET_NOT_EQUAL, ERR_COEF_MODE);
		CreateFormatWeapons(format);
		break;
	default:
		base::CreateFormat(cls, format);
		break;
	}
	return format;
}

void EntityAI::CreateFormatWeapons(NetworkMessageFormat &format)
{
	format.Add("initialUpdate", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Initial update"));
	format.Add("currentWeapon", NDTInteger, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Index of currently selected weapon"), ET_NOT_EQUAL, ERR_COEF_MODE);
	format.Add("weapons", NDTStringArray, NCTDefault, DEFVALUESTRINGARRAY, DOC_MSG("List of weapons"), ET_NOT_EQUAL, ERR_COEF_MODE);
	format.Add("magazines", NDTObjectArray, NCTNone, DEFVALUE_MSG(NMTMagazine), DOC_MSG("List of magazines"), ET_MAGAZINES, 1);
	format.Add("magazineSlots", NDTIntArray, NCTSmallSigned, DEFVALUEINTARRAY, DOC_MSG("List of currently charged weapons/magazines/modes"), ET_NOT_EQUAL, ERR_COEF_MODE);
}

TMError EntityAI::TransferMsg(NetworkMessageContext &ctx)
{
	switch (ctx.GetClass())
	{
	case NMCUpdateDammage:
		TMCHECK(base::TransferMsg(ctx))
		{
			Assert(dynamic_cast<const IndicesUpdateDammageVehicleAI *>(ctx.GetIndices()))
			const IndicesUpdateDammageVehicleAI *indices = static_cast<const IndicesUpdateDammageVehicleAI *>(ctx.GetIndices());

			ITRANSF(isDead)
			if (ctx.IsSending())
			{
				ITRANSF(hit)
			}
			else
			{
				AUTO_STATIC_ARRAY(float,hit,32);
				TMCHECK(ctx.IdxTransfer(indices->hit, hit))
				for (int i=0; i<_hit.Size(); i++)
				{
					if (i<hit.Size()) ChangeHit(i,hit[i]);
				}
			}
		}
		break;
	case NMCUpdateGeneric:
		TMCHECK(base::TransferMsg(ctx))
		{
			Assert(dynamic_cast<const IndicesUpdateVehicleAI *>(ctx.GetIndices()))
			const IndicesUpdateVehicleAI *indices = static_cast<const IndicesUpdateVehicleAI *>(ctx.GetIndices());

			ITRANSF(pilotLight);
			if (ctx.IsSending())
			{
				EntityAI *target = NULL;
				if (_fire._fireTarget) target = _fire._fireTarget->idExact;
				TMCHECK(ctx.IdxTransferRef(indices->fireTarget, target));
			}
			else
			{
				_fire._fireTarget = NULL;
				EntityAI *target = NULL;
				TMCHECK(ctx.IdxTransferRef(indices->fireTarget, target));
				if (target)
				{
					AIUnit *unit = CommanderUnit();
					AIGroup *grp = unit ? unit->GetGroup() : NULL;
					if (grp)
					{
						_fire._fireTarget = grp->FindTarget(target);
					}
				}
			}
			TMCHECK(TransferMsgWeapons(ctx, indices->weapons))
		}
		break;
	default:
		return base::TransferMsg(ctx);
	}
	return TMOK;
}

/*
struct MagazineReloadInfo
{
	int creator;
	int id;
	float reload;
};
TypeIsSimple(MagazineReloadInfo)
*/

/*!
\patch 1.12 Date 08/06/2001 by Jirka
- Fixed: Initial state of ammunition was bad on gunner's computer
\patch 1.12 Date 08/08/2001 by Jirka
- Fixed: Ammo reload by gunner in MP (sabot / heat switching)
*/
TMError EntityAI::TransferMsgWeapons(NetworkMessageContext &ctx, IndicesUpdateEntityAIWeapons *indices)
{
	ITRANSF(currentWeapon)
	// FIX
	bool initialUpdate;
	if (ctx.IsSending()) initialUpdate = ctx.GetInitialUpdate();
	TMCHECK(ctx.IdxTransfer(indices->initialUpdate, initialUpdate))
	// magazines
	if (ctx.IsSending() || initialUpdate || !(GunnerUnit() && GunnerUnit()->GetPerson()->IsLocal()))
	{
		TMCHECK(ctx.IdxTransferObjArray(indices->magazines, _magazines))
	}
	else
	{
/*
		AUTO_STATIC_ARRAY(MagazineReloadInfo, reloads, 32);
		int n = _magazines.Size();
		reloads.Resize(n);
		for (int i=0; i<n; i++)
		{
			reloads[i].creator = _magazines[i]->_creator;
			reloads[i].id = _magazines[i]->_id;
			reloads[i].reload = _magazines[i]->_reload;
		}
		TMCHECK(ctx.IdxTransferObjArray(indices->magazines, _magazines));
		int m = _magazines.Size();
		for (int i=0; i<n; i++)
		{
			int creator = reloads[i].creator;
			int id = reloads[i].id;
			if (_magazines[i]->_id == id && _magazines[i]->_creator == creator)
			{
				_magazines[i]->_reload = reloads[i].reload;
			}
			else for (int j=0; j<m; j++)
			{
				if (j != i && _magazines[j]->_id == id && _magazines[j]->_creator == creator)
				{
					_magazines[j]->_reload = reloads[i].reload;
					break;
				}
			}
		}
*/
		AUTO_STATIC_ARRAY(MagazineNetworkInfo, magazines, 32);
		TMCHECK(ctx.IdxTransferArray(indices->magazines, magazines))
		int n = magazines.Size();
		RefArray<Magazine> oldMagazines = _magazines;
		int oldN = oldMagazines.Size();
		_magazines.Resize(n);
		for (int i=0; i<n; i++)
		{
			int creator = magazines[i]._creator;
			int id = magazines[i]._id;
			int found = -1;
			if (i < oldN && oldMagazines[i]->_creator == creator && oldMagazines[i]->_id == id)
			{
				found = i;
			}
			else for (int j=0; j<oldN; j++)
			{
				if (j != i && oldMagazines[j]->_creator == creator && oldMagazines[j]->_id == id)
				{
					found = j; break;
				}
			}
			if (found >= 0)
			{
				_magazines[i] = oldMagazines[found];
			}
			else
			{
				_magazines[i] = new Magazine(MagazineTypes.New(magazines[i]._type));
				_magazines[i]->_creator = creator;
				_magazines[i]->_id = id;
				_magazines[i]->_reload = magazines[i]._reload;
				_magazines[i]->_reloadMagazine = magazines[i]._reloadMagazine;
			}
			_magazines[i]->_ammo = magazines[i]._ammo;
			_magazines[i]->_burstLeft = magazines[i]._burstLeft;
		}
	}
	if (ctx.IsSending())
	{
		// weapons
		AutoArray<RString> weapons;
		weapons.Resize(_weapons.Size());
		for (int i=0; i<_weapons.Size(); i++) weapons[i] = _weapons[i]->GetName();
		TMCHECK(ctx.IdxTransfer(indices->weapons, weapons))
		// magazine slots
		AutoArray<int> slots;
		int n = _magazineSlots.Size();
		slots.Resize(n);
		for (int i=0; i<n; i++)
		{
			slots[i] = -1;
			Magazine *magazine = _magazineSlots[i]._magazine;
			if (magazine)
			{
				for (int j=0; j<_magazines.Size(); j++)
				{
					if (_magazines[j] == magazine)
					{
						slots[i] = j;
						break;
					}
				}
			}
		}
		TMCHECK(ctx.IdxTransfer(indices->magazineSlots, slots));
	}
	else
	{
		// weapons
		AutoArray<RString> weapons;
		TMCHECK(ctx.IdxTransfer(indices->weapons, weapons));
		bool changed = weapons.Size() != _weapons.Size();
		if (!changed) for (int i=0; i<_weapons.Size(); i++)
			if (weapons[i] != _weapons[i]->GetName())
			{
				changed = true;
				break;
			}
		if (changed)
		{
			RemoveAllWeapons();
			for (int i=0; i<weapons.Size(); i++)
			{
				AddWeapon(weapons[i], true);
			}
		}
		// magazine slots
		// FIX
		if (initialUpdate || !(GunnerUnit() && GunnerUnit()->GetPerson()->IsLocal()))
		{
			AutoArray<int> slots;
			TMCHECK(ctx.IdxTransfer(indices->magazineSlots, slots));
			int n = _magazineSlots.Size();
			// FIX
			int m = slots.Size();
			DoAssert(m == n);
      for (int i=0; i<n; i++)
      {
        MagazineSlot &slot = _magazineSlots[i];
        slot._magazine = NULL;
        if (i >= m) continue;
        int index = slots[i];
        if (index < 0 || index >= _magazines.Size()) continue;
        Magazine *magazine = _magazines[index];
        if (!magazine || !magazine->_type) continue;
        if (!slot._muzzle->CanUse(magazine->_type))
        {
          RptF
          (
            "Cannot use magazine %s in muzzle %s",
            (const char *)magazine->_type->GetName(),
            (const char *)slot._muzzle->GetName()
          );
          continue;
        }
        slot._magazine = magazine;
      }
		}

		if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == this && _currentWeapon < 0)
			GWorld->UI()->ResetVehicle(this);
	}
	return TMOK;
}

float EntityAI::CalculateError(NetworkMessageContext &ctx)
{
	float error = 0;
	switch (ctx.GetClass())
	{
	case NMCUpdateDammage:
		{
			error += base::CalculateError(ctx);
			{
				Assert(dynamic_cast<const IndicesUpdateDammageVehicleAI *>(ctx.GetIndices()))
				const IndicesUpdateDammageVehicleAI *indices = static_cast<const IndicesUpdateDammageVehicleAI *>(ctx.GetIndices());
				ICALCERR_NEQ(bool, isDead, ERR_COEF_STRUCTURE)
				// hitpoints
				AutoArray<float> hit;
				float hitError = 0;
				if (ctx.IdxTransfer(indices->hit, hit) == TMOK)
				{
					int minSize = _hit.Size();
					saturateMin(minSize, hit.Size());
					hitError += _hit.Size() - minSize + hit.Size() - minSize;
					for (int i=0; i<minSize; i++)
						hitError += fabs(_hit[i] - hit[i]);
				}
				error += hitError * ERR_COEF_STRUCTURE;
			}
		}
		break;
	case NMCUpdateGeneric:
		{
			error += base::CalculateError(ctx);
			{
				Assert(dynamic_cast<const IndicesUpdateVehicleAI *>(ctx.GetIndices()))
				const IndicesUpdateVehicleAI *indices = static_cast<const IndicesUpdateVehicleAI *>(ctx.GetIndices());

				ICALCERR_NEQ(bool, pilotLight, ERR_COEF_VALUE_MAJOR)

				EntityAI *target = NULL;
				if (_fire._fireTarget) target = _fire._fireTarget->idExact;
				ICALCERRE_NEQREF(Object, fireTarget, target, ERR_COEF_MODE)

				int currentWeapon;
				if (ctx.IdxTransfer(indices->weapons->currentWeapon, currentWeapon) == TMOK)
						if (currentWeapon != _currentWeapon) error += ERR_COEF_MODE;
				// weapons
				AutoArray<RString> weapons;
				if (ctx.IdxTransfer(indices->weapons->weapons, weapons) == TMOK)
				{
					bool changed = weapons.Size() != _weapons.Size();
					if (!changed) for (int i=0; i<_weapons.Size(); i++)
						if (weapons[i] != _weapons[i]->GetName())
						{
							changed = true; break;
						}
					if (changed) error += ERR_COEF_MODE;
				}
				// magazines
				/*
				RefArray<Magazine> magazines;
				if (ctx.IdxTransferObjArray(indices->weapons->magazines, magazines) == TMOK)
				{
					bool changed = magazines.Size() != _magazines.Size();
					int ammoDiff = 0;
					if (!changed) for (int i=0; i<_magazines.Size(); i++)
						if (magazines[i]->_creator != _magazines[i]->_creator || magazines[i]->_id != _magazines[i]->_id)
						{
							changed = true; break;
						}
						else ammoDiff += abs(magazines[i]->_ammo - _magazines[i]->_ammo);
					if (changed) error += ERR_COEF_MODE;
					else error += ammoDiff * ERR_COEF_VALUE_MINOR;
				}
				*/
				AUTO_STATIC_ARRAY(MagazineNetworkInfo, magazines, 32);
				if (ctx.IdxTransferArraySimple(indices->weapons->magazines, magazines) == TMOK)
				{
					bool changed = magazines.Size() != _magazines.Size();
					int ammoDiff = 0;
					if (!changed) for (int i=0; i<_magazines.Size(); i++)
						if (magazines[i]._creator != _magazines[i]->_creator || magazines[i]._id != _magazines[i]->_id)
						{
							changed = true; break;
						}
						else ammoDiff += abs(magazines[i]._ammo - _magazines[i]->_ammo);
					if (changed) error += ERR_COEF_MODE;
					else error += ammoDiff * ERR_COEF_VALUE_MINOR;
				}

				// magazine slots
				AutoArray<int> slots;
				if (ctx.IdxTransfer(indices->weapons->magazineSlots, slots) == TMOK)
				{
					bool changed = slots.Size() != _magazineSlots.Size();
					if (!changed) for (int i=0; i<_magazineSlots.Size(); i++)
						if (slots[i] >= _magazines.Size())
						{
							changed = true; break;
						}
						else if (slots[i] >= 0)
						{
							if (_magazineSlots[i]._magazine != _magazines[slots[i]])
							{
								changed = true; break;
							}
						}
						else
						{
							if (_magazineSlots[i]._magazine != NULL)
							{
								changed = true; break;
							}
						}
					if (changed) error += ERR_COEF_MODE;
				}
			}
		}
		break;
	default:
		error += base::CalculateError(ctx);
		break;
	}
	return error;
}

HitPoint::HitPoint()
{
	_selection=-1; // selection index (in Hitpoints LOD)
	_armor=10; // hit point armor
	_index=-1;
	_indexCC.Clear();
	_material = -1;
	_passThrough = 1;
	_invArmor = 1/_armor;
}

HitPoint::HitPoint
(
	Shape *shape, const char *name, const char *altName, float armor, int material
)
{
	_selection=shape ? shape->FindNamedSel(name,altName) : -1; 
	_armor=armor;
	_index=-1;
	_indexCC.Clear();
	_material = material;
	_passThrough = 1;
	//LogF("old hitpoint %s: armor %g",(const char *)name,_armor);
	if( _selection>=0 )
	{
		const NamedSelection &sel=shape->NamedSel(_selection);
		_armor*=sel.Size();
	}

	_invArmor = 1/_armor;
}

HitPoint::HitPoint(Shape *shape, const ParamEntry &par, float armor)
{
	RStringB selName = par>>"name";
	_selection=shape ? shape->FindNamedSel(selName) : -1; 
	_armor= par>>"armor";
	_armor *= armor;
	_material = par>>"material";
	_index=-1;
	_indexCC.Clear();
	_passThrough = par>>"passThrough";

	//LogF("new hitpoint %s: armor %g",(const char *)selName,_armor);
	if( _selection>=0 )
	{
		const NamedSelection &sel=shape->NamedSel(_selection);
		_armor*=sel.Size();
	}

	_invArmor = 1/_armor;
}
