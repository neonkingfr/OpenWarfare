// Implementation of display's instances

#include "wpch.hpp"
#include "uiMap.hpp"

#include "landscape.hpp"

#include "resincl.hpp"
extern class ParamFile Res;

#include "keyInput.hpp"

#include "camera.hpp"

#include <El/QStream/QBStream.hpp>

#include <El/Common/randomGen.hpp>
#include "strFormat.hpp"

#include "scripts.hpp"

#include "dikCodes.h"
#include <Es/Common/win.h>
#include "vkCodes.h"

#include <ctype.h>
#ifdef _WIN32
  #include <io.h>
  #include <direct.h>
#endif
#include <sys/stat.h>

#include "network.hpp"

#include "chat.hpp"

#include "dynSound.hpp"
#include "displayUI.hpp"

#include "cdapfncond.h"

#include "mbcs.hpp"

#include "saveVersion.hpp"

#include <Es/Strings/bstring.hpp>

#include "stringtableExt.hpp"

#include <Es/Algorithms/qsort.hpp>

#include <Es/Common/filenames.hpp>

/*
#if _CZECH
#include "roxxe.hpp"
#pragma comment(lib,"clientlib")
#endif
*/

/*!
\file
Implementation file for particular displays.
\patch_internal 1.01 Date 6/29/2001 by Ondra.
Win32 system calls replaced by corresponding C counterparts to avoid DLL linkage.
DLL linkage make function protection impossible.
*/

void ShowCinemaBorder(bool show);

int GetNetworkPort();
RString GetNetworkPassword();

extern bool AutoTest;

RString GetIdentityText(const PlayerIdentity &identity);

//! Delete directory and whole his content
/*!
	\patch 1.01 Date 06/12/2001 by Jirka
	- Fixed: user cannot be deleted sometimes
	\patch_internal 1.01 Date 06/12/2001 by Jirka
	- fixed - do not delete only current and parent directory
	- no directory starting with "." was not deleted
  \patch 1.92 Date 7/14/2003 by Jirka
  - Fixed: Avoid delete of partition (rmdir *.*) when DirectPlay server creation failed
*/

void DeleteDirectoryStructure(const char *name, bool deleteDir =true)
{
  if (!name || *name == 0) return;

#ifdef _WIN32
	char buffer[256];
	sprintf(buffer, "%s\\*.*", name);

	_finddata_t info;
	long h = _findfirst(buffer, &info);
	if (h != -1)
	{
		do
		{
			if ((info.attrib & _A_SUBDIR) != 0)
			{
				// FIX - do not delete only current and parent directory
				if (strcmp(info.name, ".") != 0 && strcmp(info.name, "..") != 0)
				{
					sprintf(buffer, "%s\\%s", name, info.name);
					DeleteDirectoryStructure(buffer);
				}
			}
			else
			{
				sprintf(buffer, "%s\\%s", name, info.name);
				chmod(buffer,_S_IREAD | _S_IWRITE);
				unlink(buffer);
			}
		}
		while (_findnext(h, &info)==0);
		_findclose(h);
	}
	if (deleteDir)
	{
		chmod(name,_S_IREAD | _S_IWRITE);
		rmdir(name);
	}
#else
    LocalPath(dname,name);
    DIR *dir = opendir(dname);
    if ( !dir ) return;
    struct dirent *entry;
    int len = strlen(dname);
    dname[len++] = '/';
    while ( (entry = readdir(dir)) ) {      // process one directory item..
        strcpy(dname+len,entry->d_name);
	struct stat st;
	if ( !stat(dname,&st) )             // valid item
	    if ( S_ISDIR(st.st_mode) ) {    // sub-directory
	        if ( entry->d_name[0] != '.' ||
		     entry->d_name[1] &&
		     (entry->d_name[1] != '.' || entry->d_name[2]) )
		    DeleteDirectoryStructure(dname);
                }
            else {                          // file
                chmod(dname,S_IREAD|S_IWRITE);
		unlink(dname);
	        }
        }
    closedir(dir);
    if ( deleteDir ) {                      // remove the directory itself
        dname[len-1] = (char)0;
        chmod(dname,S_IREAD|S_IWRITE);
        rmdir(dname);
        }
#endif
}

void CopyDirectoryStructure(const char *dst, const char *src)
{
#ifdef _WIN32
	char buffer[256];
	sprintf(buffer, "%s\\*.*", src);

	_finddata_t info;
	long h = _findfirst(buffer, &info);
	if (h != -1)
	{
		do
		{
			if ((info.attrib & _A_SUBDIR) != 0)
			{
				// FIX - do not delete only current and parent directory
				if (strcmp(info.name, ".") != 0 && strcmp(info.name, "..") != 0)
				{
					char srcNew[256];
					char dstNew[256];
					sprintf(srcNew, "%s\\%s", src, info.name);
					sprintf(dstNew, "%s\\%s", dst, info.name);
					mkdir(dstNew);
					CopyDirectoryStructure(dstNew, srcNew);
				}
			}
			else
			{
				char srcNew[256];
				char dstNew[256];
				sprintf(srcNew, "%s\\%s", src, info.name);
				sprintf(dstNew, "%s\\%s", dst, info.name);
				::CopyFile(srcNew, dstNew, FALSE);
			}
		}
		while (_findnext(h, &info)==0);
		_findclose(h);
	}
#else
    LocalPath(srcname,src);
    LocalPath(dstname,dst);                 // dstname must exist
    DIR *dir = opendir(srcname);
    if ( !dir ) return;
    struct dirent *entry;
    int srclen = strlen(srcname);
    srcname[srclen++] = '/';
    int dstlen = strlen(dstname);
    dstname[dstlen++] = '/';
    while ( (entry = readdir(dir)) ) {      // process one src-directory item..
        strcpy(srcname+srclen,entry->d_name);
	strcpy(dstname+dstlen,entry->d_name);
	struct stat st;
	if ( !stat(srcname,&st) )           // valid item
	    if ( S_ISDIR(st.st_mode) ) {    // sub-directory
	        if ( entry->d_name[0] != '.' ||
		     entry->d_name[1] &&
		     (entry->d_name[1] != '.' || entry->d_name[2]) ) {
		    mkdir(dstname,NEW_DIRECTORY_MODE);
		    CopyDirectoryStructure(dstname,srcname);
		    }
                }
            else                            // file
                fileCopy(srcname,dstname);
        }
    closedir(dir);
#endif
}

void CreatePath(RString path);

void RunInitScript()
{
	RString initScript = GetMissionDirectory() + RString("init.sqs");
	if (QIFStreamB::FileExist(initScript))
	{
		Script *script = new Script("init.sqs", GameValue(), INT_MAX);
		GWorld->AddScript(script);
		GWorld->SimulateScripts();
	}
}

// Configure controls display
static int ReservedKeys[] =
{
	// pause
	DIK_ESCAPE,
	// menu
	DIK_1,DIK_2,DIK_3,DIK_4,DIK_5,DIK_6,DIK_7,DIK_8,DIK_9,DIK_0,DIK_BACK,
	// units
	DIK_F1,DIK_F2,DIK_F3,DIK_F4,DIK_F5,DIK_F6,
	DIK_F7,DIK_F8,DIK_F9,DIK_F10,DIK_F11,DIK_F12
};

bool IsReservedKey(int dikCode)
{
	int n = sizeof(ReservedKeys) / sizeof(int);

	for (int i=0; i<n; i++)
		if (dikCode == ReservedKeys[i]) return true;

	return false;
};


RString GetKeyName(int dikCode)
{
	#if 0
		// ask DirectX about key name
		extern RString GetKeyNameDI(int dik);
		RString name = GetKeyNameDI(dikCode);
		if (name.GetLength()>0)
		{
			return name;
		}
	#endif
	switch (dikCode)
	{
	case DIK_ESCAPE: return LocalizeString(IDS_DIK_ESCAPE);
	case DIK_1: return LocalizeString(IDS_DIK_1);
	case DIK_2: return LocalizeString(IDS_DIK_2);
	case DIK_3: return LocalizeString(IDS_DIK_3);
	case DIK_4: return LocalizeString(IDS_DIK_4);
	case DIK_5: return LocalizeString(IDS_DIK_5);
	case DIK_6: return LocalizeString(IDS_DIK_6);
	case DIK_7: return LocalizeString(IDS_DIK_7);
	case DIK_8: return LocalizeString(IDS_DIK_8);
	case DIK_9: return LocalizeString(IDS_DIK_9);
	case DIK_0: return LocalizeString(IDS_DIK_0);
	case DIK_MINUS: return LocalizeString(IDS_DIK_MINUS);
	case DIK_EQUALS: return LocalizeString(IDS_DIK_EQUALS);
	case DIK_BACK: return LocalizeString(IDS_DIK_BACK);
	case DIK_TAB: return LocalizeString(IDS_DIK_TAB);
	case DIK_Q: return LocalizeString(IDS_DIK_Q);
	case DIK_W: return LocalizeString(IDS_DIK_W);
	case DIK_E: return LocalizeString(IDS_DIK_E);
	case DIK_R: return LocalizeString(IDS_DIK_R);
	case DIK_T: return LocalizeString(IDS_DIK_T);
	case DIK_Y: return LocalizeString(IDS_DIK_Y);
	case DIK_U: return LocalizeString(IDS_DIK_U);
	case DIK_I: return LocalizeString(IDS_DIK_I);
	case DIK_O: return LocalizeString(IDS_DIK_O);
	case DIK_P: return LocalizeString(IDS_DIK_P);
	case DIK_LBRACKET: return LocalizeString(IDS_DIK_LBRACKET);
	case DIK_RBRACKET: return LocalizeString(IDS_DIK_RBRACKET);
	case DIK_RETURN: return LocalizeString(IDS_DIK_RETURN);
	case DIK_LCONTROL: return LocalizeString(IDS_DIK_LCONTROL);
	case DIK_A: return LocalizeString(IDS_DIK_A);
	case DIK_S: return LocalizeString(IDS_DIK_S);
	case DIK_D: return LocalizeString(IDS_DIK_D);
	case DIK_F: return LocalizeString(IDS_DIK_F);
	case DIK_G: return LocalizeString(IDS_DIK_G);
	case DIK_H: return LocalizeString(IDS_DIK_H);
	case DIK_J: return LocalizeString(IDS_DIK_J);
	case DIK_K: return LocalizeString(IDS_DIK_K);
	case DIK_L: return LocalizeString(IDS_DIK_L);
	case DIK_SEMICOLON: return LocalizeString(IDS_DIK_SEMICOLON);
	case DIK_APOSTROPHE: return LocalizeString(IDS_DIK_APOSTROPHE);
	case DIK_GRAVE: return LocalizeString(IDS_DIK_GRAVE);
	case DIK_LSHIFT: return LocalizeString(IDS_DIK_LSHIFT);
	case DIK_BACKSLASH: return LocalizeString(IDS_DIK_BACKSLASH);
	case DIK_Z: return LocalizeString(IDS_DIK_Z);
	case DIK_X: return LocalizeString(IDS_DIK_X);
	case DIK_C: return LocalizeString(IDS_DIK_C);
	case DIK_V: return LocalizeString(IDS_DIK_V);
	case DIK_B: return LocalizeString(IDS_DIK_B);
	case DIK_N: return LocalizeString(IDS_DIK_N);
	case DIK_M: return LocalizeString(IDS_DIK_M);
	case DIK_COMMA: return LocalizeString(IDS_DIK_COMMA);
	case DIK_PERIOD: return LocalizeString(IDS_DIK_PERIOD);
	case DIK_SLASH: return LocalizeString(IDS_DIK_SLASH);
	case DIK_RSHIFT: return LocalizeString(IDS_DIK_RSHIFT);
	case DIK_MULTIPLY: return LocalizeString(IDS_DIK_MULTIPLY);
	case DIK_LMENU: return LocalizeString(IDS_DIK_LMENU);
	case DIK_SPACE: return LocalizeString(IDS_DIK_SPACE);
	case DIK_CAPITAL: return LocalizeString(IDS_DIK_CAPITAL);
	case DIK_F1: return LocalizeString(IDS_DIK_F1);
	case DIK_F2: return LocalizeString(IDS_DIK_F2);
	case DIK_F3: return LocalizeString(IDS_DIK_F3);
	case DIK_F4: return LocalizeString(IDS_DIK_F4);
	case DIK_F5: return LocalizeString(IDS_DIK_F5);
	case DIK_F6: return LocalizeString(IDS_DIK_F6);
	case DIK_F7: return LocalizeString(IDS_DIK_F7);
	case DIK_F8: return LocalizeString(IDS_DIK_F8);
	case DIK_F9: return LocalizeString(IDS_DIK_F9);
	case DIK_F10: return LocalizeString(IDS_DIK_F10);
	case DIK_NUMLOCK: return LocalizeString(IDS_DIK_NUMLOCK);
	case DIK_SCROLL: return LocalizeString(IDS_DIK_SCROLL);
	case DIK_NUMPAD7: return LocalizeString(IDS_DIK_NUMPAD7);
	case DIK_NUMPAD8: return LocalizeString(IDS_DIK_NUMPAD8);
	case DIK_NUMPAD9: return LocalizeString(IDS_DIK_NUMPAD9);
	case DIK_SUBTRACT: return LocalizeString(IDS_DIK_SUBTRACT);
	case DIK_NUMPAD4: return LocalizeString(IDS_DIK_NUMPAD4);
	case DIK_NUMPAD5: return LocalizeString(IDS_DIK_NUMPAD5);
	case DIK_NUMPAD6: return LocalizeString(IDS_DIK_NUMPAD6);
	case DIK_ADD: return LocalizeString(IDS_DIK_ADD);
	case DIK_NUMPAD1: return LocalizeString(IDS_DIK_NUMPAD1);
	case DIK_NUMPAD2: return LocalizeString(IDS_DIK_NUMPAD2);
	case DIK_NUMPAD3: return LocalizeString(IDS_DIK_NUMPAD3);
	case DIK_NUMPAD0: return LocalizeString(IDS_DIK_NUMPAD0);
	case DIK_DECIMAL: return LocalizeString(IDS_DIK_DECIMAL);
	case DIK_OEM_102: return LocalizeString(IDS_DIK_OEM_102);
	case DIK_F11: return LocalizeString(IDS_DIK_F11);
	case DIK_F12: return LocalizeString(IDS_DIK_F12);
	case DIK_F13: return LocalizeString(IDS_DIK_F13);
	case DIK_F14: return LocalizeString(IDS_DIK_F14);
	case DIK_F15: return LocalizeString(IDS_DIK_F15);
	case DIK_KANA: return LocalizeString(IDS_DIK_KANA);
	case DIK_ABNT_C1: return LocalizeString(IDS_DIK_ABNT_C1);
	case DIK_CONVERT: return LocalizeString(IDS_DIK_CONVERT);
	case DIK_NOCONVERT: return LocalizeString(IDS_DIK_NOCONVERT);
	case DIK_YEN: return LocalizeString(IDS_DIK_YEN);
	case DIK_ABNT_C2: return LocalizeString(IDS_DIK_ABNT_C2);
	case DIK_NUMPADEQUALS: return LocalizeString(IDS_DIK_NUMPADEQUALS);
	case DIK_PREVTRACK: return LocalizeString(IDS_DIK_PREVTRACK);
	case DIK_AT: return LocalizeString(IDS_DIK_AT);
	case DIK_COLON: return LocalizeString(IDS_DIK_COLON);
	case DIK_UNDERLINE: return LocalizeString(IDS_DIK_UNDERLINE);
	case DIK_KANJI: return LocalizeString(IDS_DIK_KANJI);
	case DIK_STOP: return LocalizeString(IDS_DIK_STOP);
	case DIK_AX: return LocalizeString(IDS_DIK_AX);
	case DIK_UNLABELED: return LocalizeString(IDS_DIK_UNLABELED);
	case DIK_NEXTTRACK: return LocalizeString(IDS_DIK_NEXTTRACK);
	case DIK_NUMPADENTER: return LocalizeString(IDS_DIK_NUMPADENTER);
	case DIK_RCONTROL: return LocalizeString(IDS_DIK_RCONTROL);
	case DIK_MUTE: return LocalizeString(IDS_DIK_MUTE);
	case DIK_CALCULATOR: return LocalizeString(IDS_DIK_CALCULATOR);
	case DIK_PLAYPAUSE: return LocalizeString(IDS_DIK_PLAYPAUSE);
	case DIK_MEDIASTOP: return LocalizeString(IDS_DIK_MEDIASTOP);
	case DIK_VOLUMEDOWN: return LocalizeString(IDS_DIK_VOLUMEDOWN);
	case DIK_VOLUMEUP: return LocalizeString(IDS_DIK_VOLUMEUP);
	case DIK_WEBHOME: return LocalizeString(IDS_DIK_WEBHOME);
	case DIK_NUMPADCOMMA: return LocalizeString(IDS_DIK_NUMPADCOMMA);
	case DIK_DIVIDE: return LocalizeString(IDS_DIK_DIVIDE);
	case DIK_SYSRQ: return LocalizeString(IDS_DIK_SYSRQ);
	case DIK_RMENU: return LocalizeString(IDS_DIK_RMENU);
	case DIK_PAUSE: return LocalizeString(IDS_DIK_PAUSE);
	case DIK_HOME: return LocalizeString(IDS_DIK_HOME);
	case DIK_UP: return LocalizeString(IDS_DIK_UP);
	case DIK_PRIOR: return LocalizeString(IDS_DIK_PRIOR);
	case DIK_LEFT: return LocalizeString(IDS_DIK_LEFT);
	case DIK_RIGHT: return LocalizeString(IDS_DIK_RIGHT);
	case DIK_END: return LocalizeString(IDS_DIK_END);
	case DIK_DOWN: return LocalizeString(IDS_DIK_DOWN);
	case DIK_NEXT: return LocalizeString(IDS_DIK_NEXT);
	case DIK_INSERT: return LocalizeString(IDS_DIK_INSERT);
	case DIK_DELETE: return LocalizeString(IDS_DIK_DELETE);
	case DIK_LWIN: return LocalizeString(IDS_DIK_LWIN);
	case DIK_RWIN: return LocalizeString(IDS_DIK_RWIN);
	case DIK_APPS: return LocalizeString(IDS_DIK_APPS);
	case DIK_POWER: return LocalizeString(IDS_DIK_POWER);
	case DIK_SLEEP: return LocalizeString(IDS_DIK_SLEEP);
	case DIK_WAKE: return LocalizeString(IDS_DIK_WAKE);
	case DIK_WEBSEARCH: return LocalizeString(IDS_DIK_WEBSEARCH);
	case DIK_WEBFAVORITES: return LocalizeString(IDS_DIK_WEBFAVORITES);
	case DIK_WEBREFRESH: return LocalizeString(IDS_DIK_WEBREFRESH);
	case DIK_WEBSTOP: return LocalizeString(IDS_DIK_WEBSTOP);
	case DIK_WEBFORWARD: return LocalizeString(IDS_DIK_WEBFORWARD);
	case DIK_WEBBACK: return LocalizeString(IDS_DIK_WEBBACK);
	case DIK_MYCOMPUTER: return LocalizeString(IDS_DIK_MYCOMPUTER);
	case DIK_MAIL: return LocalizeString(IDS_DIK_MAIL);
	case DIK_MEDIASELECT: return LocalizeString(IDS_DIK_MEDIASELECT);
	case INPUT_DEVICE_MOUSE: return LocalizeString(IDS_INPUT_DEVICE_MOUSE_0);
	case INPUT_DEVICE_MOUSE + 1: return LocalizeString(IDS_INPUT_DEVICE_MOUSE_1);
	case INPUT_DEVICE_MOUSE + 2: return LocalizeString(IDS_INPUT_DEVICE_MOUSE_2);
	case INPUT_DEVICE_MOUSE + 3: return LocalizeString(IDS_INPUT_DEVICE_MOUSE_3);
	case INPUT_DEVICE_MOUSE + 4: return LocalizeString(IDS_INPUT_DEVICE_MOUSE_4);
	case INPUT_DEVICE_MOUSE + 5: return LocalizeString(IDS_INPUT_DEVICE_MOUSE_5);
	case INPUT_DEVICE_MOUSE + 6: return LocalizeString(IDS_INPUT_DEVICE_MOUSE_6);
	case INPUT_DEVICE_MOUSE + 7: return LocalizeString(IDS_INPUT_DEVICE_MOUSE_7);

	#ifdef _XBOX

	case INPUT_DEVICE_STICK:      return "XBox A";
	case INPUT_DEVICE_STICK + 1:  return "XBox B";
	case INPUT_DEVICE_STICK + 2:  return "XBox X";
	case INPUT_DEVICE_STICK + 3:  return "XBox Y";
	case INPUT_DEVICE_STICK + 4:  return "XBox Up";
	case INPUT_DEVICE_STICK + 5:  return "XBox Down";
	case INPUT_DEVICE_STICK + 6:  return "XBox Left";
	case INPUT_DEVICE_STICK + 7:  return "XBox Right";
	case INPUT_DEVICE_STICK + 8:  return "XBox Start";
	case INPUT_DEVICE_STICK + 9:  return "XBox Back";
	case INPUT_DEVICE_STICK + 10: return "XBox Black";
	case INPUT_DEVICE_STICK + 11: return "XBox White";
	case INPUT_DEVICE_STICK + 12: return "XBox Left Trigger";
	case INPUT_DEVICE_STICK + 13: return "XBox Right Trigger";
	case INPUT_DEVICE_STICK + 14: return "XBox Left Thumb";
	case INPUT_DEVICE_STICK + 15: return "XBox Right Thumb";

	case INPUT_DEVICE_STICK + 16: return "XBox Left Thumb X Right";
	case INPUT_DEVICE_STICK + 17: return "XBox Left Thumb Y Up";
	case INPUT_DEVICE_STICK + 18: return "XBox Right Thumb X Right";
	case INPUT_DEVICE_STICK + 19: return "XBox Right Thumb Y Up";

	case INPUT_DEVICE_STICK + 20: return "XBox Left Thumb X Left";
	case INPUT_DEVICE_STICK + 21: return "XBox Left Thumb Y Down";
	case INPUT_DEVICE_STICK + 22: return "XBox Right Thumb X Left";
	case INPUT_DEVICE_STICK + 23: return "XBox Right Thumb Y Down";

	#else

	case INPUT_DEVICE_STICK: return LocalizeString(IDS_INPUT_DEVICE_STICK_0);
	case INPUT_DEVICE_STICK + 1: return LocalizeString(IDS_INPUT_DEVICE_STICK_1);
	case INPUT_DEVICE_STICK + 2: return LocalizeString(IDS_INPUT_DEVICE_STICK_2);
	case INPUT_DEVICE_STICK + 3: return LocalizeString(IDS_INPUT_DEVICE_STICK_3);
	case INPUT_DEVICE_STICK + 4: return LocalizeString(IDS_INPUT_DEVICE_STICK_4);
	case INPUT_DEVICE_STICK + 5: return LocalizeString(IDS_INPUT_DEVICE_STICK_5);
	case INPUT_DEVICE_STICK + 6: return LocalizeString(IDS_INPUT_DEVICE_STICK_6);
	case INPUT_DEVICE_STICK + 7: return LocalizeString(IDS_INPUT_DEVICE_STICK_7);

	#endif

	case INPUT_DEVICE_STICK_AXIS + 0: return LocalizeString(IDS_INPUT_DEVICE_STICK_AXIS_X);
	case INPUT_DEVICE_STICK_AXIS + 1: return LocalizeString(IDS_INPUT_DEVICE_STICK_AXIS_Y);
	case INPUT_DEVICE_STICK_AXIS + 2: return LocalizeString(IDS_INPUT_DEVICE_STICK_AXIS_Z);
	case INPUT_DEVICE_STICK_AXIS + 3: return LocalizeString(IDS_INPUT_DEVICE_STICK_ROT_X);
	case INPUT_DEVICE_STICK_AXIS + 4: return LocalizeString(IDS_INPUT_DEVICE_STICK_ROT_Y);
	case INPUT_DEVICE_STICK_AXIS + 5: return LocalizeString(IDS_INPUT_DEVICE_STICK_ROT_Z);
	case INPUT_DEVICE_STICK_AXIS + 6: return LocalizeString(IDS_INPUT_DEVICE_STICK_SLIDER_1);
	case INPUT_DEVICE_STICK_AXIS + 7: return LocalizeString(IDS_INPUT_DEVICE_STICK_SLIDER_2);

	case INPUT_DEVICE_STICK_POV + 0: return LocalizeString(IDS_INPUT_DEVICE_POV_N);
	case INPUT_DEVICE_STICK_POV + 1: return LocalizeString(IDS_INPUT_DEVICE_POV_NE);
	case INPUT_DEVICE_STICK_POV + 2: return LocalizeString(IDS_INPUT_DEVICE_POV_E);
	case INPUT_DEVICE_STICK_POV + 3: return LocalizeString(IDS_INPUT_DEVICE_POV_SE);
	case INPUT_DEVICE_STICK_POV + 4: return LocalizeString(IDS_INPUT_DEVICE_POV_S);
	case INPUT_DEVICE_STICK_POV + 5: return LocalizeString(IDS_INPUT_DEVICE_POV_SW);
	case INPUT_DEVICE_STICK_POV + 6: return LocalizeString(IDS_INPUT_DEVICE_POV_W);
	case INPUT_DEVICE_STICK_POV + 7: return LocalizeString(IDS_INPUT_DEVICE_POV_NW);
	default: return "";
	}
}

CKeys::CKeys(ControlsContainer *parent, int idc, const ParamEntry &cls)
: C3DListBox(parent, idc, cls)
{
	_sb3DWidth = 0.05;
	_mode = -1;
	_ignoredKey = -1;
}

void CKeys::SetKeys(int action, const AutoArray<int> &keys)
{
	while (_keys[action].Size() > 0) RemoveKey(action);
	for (int i=0; i<keys.Size(); i++) AddKey(action, keys[i]);
}

void CKeys::CheckCollisions(int key)
{
	int count = 0;
	int iCol = -1, jCol = -1;
	for (int i=0; i<UAN; i++)
		for (int j=0; j<_keys[i].Size(); j++)
			if (_keys[i][j] == key)
			{
				count++;
				if (count > 1) return; // collision still persist
				iCol = i; jCol = j;
			}
	if (count > 0) _collisions[iCol][jCol] = false;
}

void CKeys::RemoveKey(int action)
{
	int index = _keys[action].Size() - 1;
	if (index < 0) return;

	int key = _keys[action][index];
	bool collision = _collisions[action][index];
	
	// delete
	_keys[action].Resize(index);
	_collisions[action].Resize(index);

	// check collisions
	if (collision) CheckCollisions(key);
}

void CKeys::AddKey(int action, int key)
{
	for (int j=0; j<_keys[action].Size(); j++)
	{
		if (_keys[action][j] == key)
		{
			// remove key instead
			bool collision = _collisions[action][j];

			// delete
			_keys[action].Delete(j);
			_collisions[action].Delete(j);

			// check collisions
			if (collision) CheckCollisions(key);

			return;
		}
	}

	// check collisions
	bool collision = false;
	for (int i=0; i<UAN; i++)
		for (int j=0; j<_keys[i].Size(); j++)
			if (_keys[i][j] == key)
			{
				collision = true;
				_collisions[i][j] = true;
			}

	_keys[action].Add(key);
	_collisions[action].Add(collision);
}

inline PackedColor ModAlpha( PackedColor color, float alpha )
{
	int a=toInt(alpha*color.A8());
	saturate(a,0,255);
	return PackedColorRGB(color,a);
}

void CKeys::DrawItem
(
	Vector3Par position, Vector3Par down, int i, float alpha
)
{
	float y1c = 0;
	float y2c = 1;
	if (i < _topString) y1c = _topString - i;
	if (i > _topString + _rows - 1) y2c = _topString + _rows - i;
	
	Vector3 normal = _down.CrossProduct(_right).Normalized(); 

	Vector3 rightSB = _right;
	if (GetSize() > _rows)
		rightSB = (1.0 - _sb3DWidth) * _right;
	float rightSBSize = rightSB.Size();
	Vector3 border = 0.02 * _right;

	bool selected = i == GetCurSel() && IsEnabled();
	PackedColor color = ModAlpha(GetFtColor(i), alpha);
	PackedColor selColor = color;
	if (selected && _showSelected)
	{
		PackedColor selBgColor = ModAlpha(_selBgColor, alpha);
		switch (_mode)
		{
		case -1:
			GEngine->Draw3D
			(
				position, down, 0.4 * rightSB, ClipAll, selBgColor, DisableSun, NULL,
				0, y1c, 1, y2c
			);
			break;
		case 0:
			GEngine->Draw3D
			(
				position + 0.4 * rightSB, down, 0.6 * rightSB, ClipAll, selBgColor, DisableSun, NULL,
				0, y1c, 1, y2c
			);
			break;
		}
		selColor = ModAlpha(GetSelColor(i), alpha);
	}

	Vector3 curPos = position - 0.002 * normal;

	Texture *texture = GetTexture(i);
	if (texture)
	{
		Vector3 right = (float)texture->AWidth() / (float)texture->AHeight() * down.Size() * _right.Normalized();
		float rightSize = right.Size();
		float x2c = 1;
		if (rightSize > rightSBSize) x2c = rightSBSize / rightSize;
		GEngine->Draw3D
		(
			curPos, down, right, ClipAll, color, // PackedColor(Color(1, 1, 1, alpha)),
			DisableSun, texture,
			0, y1c, x2c, y2c
		);
		curPos += right;
		rightSBSize -= rightSize;
		if (rightSBSize <= 0) return;
	}

	float top = 0.5 * (1.0 - _size);
	Vector3 pos = curPos + top * down + border;
	Vector3 up = -_size * down;
	Vector3 right = 0.75 * up.Size() * _right.Normalized();
	float invRightSize = 1.0 / right.Size();
	float x2ct = rightSBSize * invRightSize;
	float y1ct = 0;
	float y2ct = 1;
	if (y1c > top)
		y1ct = (y1c - top) / _size;
	if (y2c < top + _size)
		y2ct = (y2c - top) / _size;

	RString text = GetText(i);
	float x2c = 0.4 * x2ct - 2.0 * invRightSize * border.Size();
	GEngine->DrawText3D
	(
		pos, up, right, ClipAll, _font, _mode == -1 ? selColor: color, DisableSun, text,
		0, y1ct, x2c, y2ct
	);
	pos += 0.4 * rightSB;

	x2c = 0.6 * x2ct - 2.0 * invRightSize * border.Size();
	PackedColor noCollision = _mode == 0 ? selColor : color;
	PackedColor collision = PackedColor(Color(1, 0, 0, alpha));
	for (int j=0; j<_keys[i].Size(); j++)
	{
		// prefix
		if (j > 0)
		{
			const char *prefix = ", ";
			GEngine->DrawText3D
			(
				pos, up, right, ClipAll, _font, noCollision, DisableSun, prefix,
				0, y1ct, x2c, y2ct
			);
			Vector3 width = GEngine->GetText3DWidth(right, _font, prefix);
			x2c -= width.Size() * invRightSize;
			if (x2c <= 0) break;
			pos += width;
		}

		// value
		text = RString("\"") + GetKeyName(_keys[i][j]) + RString("\"");
		PackedColor col = _collisions[i][j] ? collision : noCollision;
		GEngine->DrawText3D
		(
			pos, up, right, ClipAll, _font, col, DisableSun, text,
			0, y1ct, x2c, y2ct
		);
		Vector3 width = GEngine->GetText3DWidth(right, _font, text);
		x2c -= width.Size() * invRightSize;
		if (x2c <= 0) break;
		pos += width;
	}
}

bool CKeys::OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	if (_mode == -1)
	{
		if (nChar == VK_RIGHT)
		{
			_mode = 0;
			_ignoredKey = DIK_RIGHT;
			return true;
		}
		else
			return C3DListBox::OnKeyDown(nChar, nRepCnt, nFlags);
	}
	else
	{
		if (nChar == VK_ESCAPE) _mode = -1;
		return true;
	}
}

void CKeys::CheckIgnoredKey()
{
	if (_ignoredKey != -1)
	{
		GInput.keysToDo[_ignoredKey] = false;
		_ignoredKey = -1;
	}
}

void CKeys::OnLButtonDown(float x, float y)
{
	IsInside(x, y);
	if (_scrollbar.IsEnabled())
	{
		if (_u > 1.0 - _sb3DWidth)
		{
			_scrollbar.SetPos(_topString);
			_scrollbar.OnLButtonDown(_v);
			_topString = _scrollbar.GetPos();
			return;
		}
		else
		{
			_u *= 1.0 / (1.0 - _sb3DWidth);
		}
	}

	float index = _v * _rows;
	if (index >= 0 && index < _rows)
	{
		_parent->OnLBDrag(IDC(), toIntFloor(_topString + index));
		_dragging = true;
	}
	if (_u < 0.4) _mode = -1;
	else _mode = 0;
}

DisplayConfigure::DisplayConfigure(ControlsContainer *parent, bool enableSimulation)
	: Display(parent)
{
	_enableSimulation = enableSimulation;
	_keys = NULL;
	Load("RscDisplayConfigure");
	Assert(_keys);

	_oldRevMouse = GInput.revMouse;
	_oldJoystickEnabled = GInput.joystickEnabled;
	_oldMouseButtonsReversed = GInput.mouseButtonsReversed;
	_oldMouseSensitivityX = GInput.mouseSensitivityX;
	_oldMouseSensitivityY = GInput.mouseSensitivityY;

	C3DActiveText *ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_CONFIG_YREVERSED));
	if (GInput.revMouse)
		ctrl->SetText(LocalizeString(IDS_CONFIG_YREVERSED));
	else
		ctrl->SetText(LocalizeString(IDS_CONFIG_YNORMAL));
	ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_CONFIG_JOYSTICK));
	if (GInput.joystickEnabled)
		ctrl->SetText(LocalizeString(IDS_CONFIG_JOYSTICK_ENABLED));
	else
		ctrl->SetText(LocalizeString(IDS_CONFIG_JOYSTICK_DISABLED));
	ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_CONFIG_BUTTONS));
	if (GInput.mouseButtonsReversed)
		ctrl->SetText(LocalizeString(IDS_RIGHT_BUTTON));
	else
		ctrl->SetText(LocalizeString(IDS_LEFT_BUTTON));
}

void DisplayConfigure::Destroy()
{
	Display::Destroy();
	
	if (_exit != IDC_OK)
	{
		GInput.revMouse = _oldRevMouse;
		GInput.joystickEnabled = _oldJoystickEnabled;
		GInput.mouseSensitivityX = _oldMouseSensitivityX;
		GInput.mouseSensitivityY = _oldMouseSensitivityY;
		GInput.mouseButtonsReversed = _oldMouseButtonsReversed;
		return;
	}

	for (int i=0; i<UAN; i++)
	{
		GInput.userKeys[i] = _keys->GetKeys(i);
		GInput.userKeys[i].Compact();
	}
	GInput.SaveKeys();
}

void DisplayConfigure::OnSimulate(EntityAI *vehicle)
{
	Display::OnSimulate(vehicle);

	if (!_keys->IsFocused()) return;

	if (_keys->GetMode() < 0) return;

	int index = _keys->GetCurSel();
	if (index < 0) return;

	_keys->CheckIgnoredKey();

	int count = 0;
	int dik = -1;
	//const AutoArray<int> keyList = _keys->GetKeys(index);

	if (!GInput.userActionDesc[index].axis)
	{
		for (int i=0; i<256; i++)
		{
			if (GInput.keysToDo[i])
			{
				dik = i;
				count++;
			}
		}
		for (int i=0; i<N_MOUSE_BUTTONS; i++)
		{
			if (i == 0) continue; // TODO: Primary mouse button
			if (GInput.mouseButtonsToDo[i])
			{
				dik = INPUT_DEVICE_MOUSE + i;
				count++;
			}
		}
		for (int i=0; i<N_JOYSTICK_BUTTONS; i++)
		{
			if (GInput.stickButtonsToDo[i])
			{
				dik = INPUT_DEVICE_STICK + i;
				count++;
			}
		}
		for (int i=0; i<N_JOYSTICK_POV; i++)
		{
			if (GInput.stickPovToDo[i])
			{
				dik = INPUT_DEVICE_STICK_POV + i;
				count++;
			}
		}

	}
	else
	{
		for (int i=0; i<N_JOYSTICK_AXES; i++)
		{
			if (GInput.jAxisBigLastActive[i]>Glob.uiTime-0.2)
			{
				GInput.jAxisBigLastActive[i] = UITIME_MIN;
				dik = INPUT_DEVICE_STICK_AXIS + i;
				count++;
			}
		}
	}
	if (count != 1) return;

	if (dik == DIK_BACK) _keys->RemoveKey(index);
	else if (!IsReservedKey(dik)) _keys->AddKey(index, dik);
}

Control *DisplayConfigure::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
	case IDC_CONFIG_KEYS:
		_keys = new CKeys(this, idc, cls);
		for (int i=0; i<UAN; i++)
		{
			_keys->AddString(LocalizeString(GInput.userActionDesc[i].desc));
			_keys->SetKeys(i, GInput.userKeys[i]);
		}
		_keys->SetCurSel(0);
		return _keys;
	case IDC_CONFIG_XAXIS:
		{
			C3DSlider *ctrl = new C3DSlider(this, idc, cls);
			ctrl->SetRange(0.5, 2.0);
			ctrl->SetSpeed(0.2, 0.02);
			ctrl->SetThumbPos(GInput.mouseSensitivityX);
			return ctrl;
		}
	case IDC_CONFIG_YAXIS:
		{
			C3DSlider *ctrl = new C3DSlider(this, idc, cls);
			ctrl->SetRange(0.5, 2.0);
			ctrl->SetSpeed(0.2, 0.02);
			ctrl->SetThumbPos(GInput.mouseSensitivityY);
			return ctrl;
		}
	default:
		return Display::OnCreateCtrl(type, idc, cls);
	}
}

void DisplayConfigure::OnButtonClicked(int idc)
{
	switch (idc)
	{
	case IDC_CONFIG_DEFAULT:
		for (int i=0; i<UAN; i++)
		{
			_keys->SetKeys(i, Input::userActionDesc[i].keys);
		}
		{
			GInput.revMouse = false;
			C3DActiveText *ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_CONFIG_YREVERSED));
			ctrl->SetText(LocalizeString(IDS_CONFIG_YNORMAL));
		}
		{
			GInput.joystickEnabled = true;
			C3DActiveText *ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_CONFIG_JOYSTICK));
			ctrl->SetText(LocalizeString(IDS_CONFIG_JOYSTICK_ENABLED));
		}
		{
			GInput.mouseButtonsReversed = false;
			C3DActiveText *ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_CONFIG_BUTTONS));
			ctrl->SetText(LocalizeString(IDS_LEFT_BUTTON));
		}
		{
			GInput.mouseSensitivityX = 1.0;
			C3DSlider *ctrl = static_cast<C3DSlider *>(GetCtrl(IDC_CONFIG_XAXIS));
			ctrl->SetThumbPos(1.0);
		}
		{
			GInput.mouseSensitivityY = 1.0;
			C3DSlider *ctrl = static_cast<C3DSlider *>(GetCtrl(IDC_CONFIG_YAXIS));
			ctrl->SetThumbPos(1.0);
		}
		break;
	case IDC_CONFIG_YREVERSED:
		{
			GInput.revMouse = !GInput.revMouse;
			C3DActiveText *ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_CONFIG_YREVERSED));
			if (GInput.revMouse)
				ctrl->SetText(LocalizeString(IDS_CONFIG_YREVERSED));
			else
				ctrl->SetText(LocalizeString(IDS_CONFIG_YNORMAL));
		}
		break;
	case IDC_CONFIG_JOYSTICK:
		{
			GInput.joystickEnabled = !GInput.joystickEnabled;
			C3DActiveText *ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_CONFIG_JOYSTICK));
			if (GInput.joystickEnabled)
				ctrl->SetText(LocalizeString(IDS_CONFIG_JOYSTICK_ENABLED));
			else
				ctrl->SetText(LocalizeString(IDS_CONFIG_JOYSTICK_DISABLED));
		}
		break;
	case IDC_CONFIG_BUTTONS:
		{
			GInput.mouseButtonsReversed = !GInput.mouseButtonsReversed;
			C3DActiveText *ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_CONFIG_BUTTONS));
			if (GInput.mouseButtonsReversed)
				ctrl->SetText(LocalizeString(IDS_RIGHT_BUTTON));
			else
				ctrl->SetText(LocalizeString(IDS_LEFT_BUTTON));
		}
		break;
	default:
		Display::OnButtonClicked(idc);
		break;
	}
}

void DisplayConfigure::OnSliderPosChanged(int idc, float pos)
{
	switch (idc)
	{
	case IDC_CONFIG_XAXIS:
		GInput.mouseSensitivityX = pos;
		break;
	case IDC_CONFIG_YAXIS:
		GInput.mouseSensitivityY = pos;
		break;
	default:
		Display::OnSliderPosChanged(idc, pos);
		break;
	}
}

// Select island game display
Control *DisplaySelectIsland::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
		case IDC_SELECT_ISLAND:
			{
				C3DListBox *lbox = new C3DListBox(this, idc, cls);
				int sel = 0;
//				int m = (Pars>>"CfgWorlds">>"worlds").GetSize();
				int m = (Pars >> "CfgWorldList").GetEntryCount();
				for (int j=0; j<m; j++)
				{
					const ParamEntry &entry = (Pars >> "CfgWorldList").GetEntry(j);
					if (!entry.IsClass()) continue;
					RString name = entry.GetName();
//					RString name = (Pars>>"CfgWorlds">>"worlds")[j];

					// ADDED - check if wrp file exists
					RString fullname = GetWorldName(name);
					if (!QIFStreamB::FileExist(fullname)) continue;
			
					int index = lbox->AddString
					(
						Pars >> "CfgWorlds" >> name >> "description"
					);
					lbox->SetData(index, name);
					if (stricmp(name, Glob.header.worldname) == 0)
						sel = index;
					RString textureName = Pars >> "CfgWorlds" >> name >> "icon";
					RString fullName = FindPicture(textureName);
					fullName.Lower();
					Ref<Texture> texture = GlobLoadTexture
					(
						fullName
					);
					lbox->SetTexture(index, texture);
				}
				lbox->SetCurSel(sel);
				return lbox;
			}
	}
	return Display::OnCreateCtrl(type, idc, cls);
}

void DisplaySelectIsland::OnLBDblClick(int idc, int curSel)
{
	if (idc == IDC_SELECT_ISLAND)
	{
		OnButtonClicked(IDC_OK);
	}
	else
		Display::OnLBDblClick(idc, curSel);
}

void DisplaySelectIsland::OnButtonClicked(int idc)
{
	switch (idc)
	{
//	case IDC_OK:
	case IDC_CANCEL:
		{
			_exitWhenClose = idc;
			ControlObjectContainerAnim *ctrl =
				dynamic_cast<ControlObjectContainerAnim *>(GetCtrl(IDC_SELECT_ISLAND_NOTEBOOK));
			if (ctrl) ctrl->Close();
		}
		break;
#if _ENABLE_EDITOR
	case IDC_SELECT_ISLAND_WIZARD:
		{
			C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_SELECT_ISLAND));
			if (!lbox) break;	

			int index = lbox->GetCurSel();
			if (index < 0) break;
			RString world = lbox->GetData(index);

			CreateChild(new DisplayWizardTemplate(this, world, false));
		}
		break;
#endif //_ENABLE_EDITOR
	default:
		Display::OnButtonClicked(idc);
		break;
	}
}

void DisplaySelectIsland::OnChildDestroyed(int idd, int exit)
{
	if (idd == IDD_WIZARD_TEMPLATE && exit == IDC_OK)
	{
		Display::OnChildDestroyed(idd, exit);
		Exit(IDC_CUST_PLAY);
	}
	else
		Display::OnChildDestroyed(idd, exit);
}

void DisplaySelectIsland::OnCtrlClosed(int idc)
{
	if (idc == IDC_SELECT_ISLAND_NOTEBOOK)
		Exit(_exitWhenClose);
	else
		Display::OnCtrlClosed(idc);
}

// Custom arcade game display
Control *DisplayCustomArcade::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	return Display::OnCreateCtrl(type, idc, cls);
}

void DisplayCustomArcade::OnButtonClicked(int idc)
{
	switch (idc)
	{
		case IDC_CUST_PLAY:
		case IDC_CUST_EDIT:
			Exit(idc);
			break;
		case IDC_CUST_DELETE:
			{
				CTree *tree = dynamic_cast<CTree *>(GetCtrl(IDC_CUST_GAME));
				CTreeItem *item = tree->GetSelected();
				if (item && item->level == 3)
				{
					CreateMsgBox
					(
						MB_BUTTON_OK | MB_BUTTON_CANCEL,
						LocalizeString(IDS_SURE),
						IDD_MSG_DELETEGAME
					);
				}
			}
			break;
		default:
			Display::OnButtonClicked(idc);
			break;
	}
}

void DisplayCustomArcade::OnChildDestroyed(int idd, int exit)
{
	switch (idd)
	{
		case IDD_MSG_DELETEGAME:
			Display::OnChildDestroyed(idd, exit);
			if (exit == IDC_OK)
			{
				CTree *tree = dynamic_cast<CTree *>(GetCtrl(IDC_CUST_GAME));
				CTreeItem *item = tree->GetSelected();
				if (!item) break;
				if (item->level != 3) break;
				RString mission = item->data;
				item = item->parent;
				RString world = item->data;
				item = item->parent;
				RString campaign = item->data;
				char buffer[256];
/*
				if (campaign.GetLength() > 0)
				{
					sprintf
					(
						buffer, "Campaigns\\%s\\Missions\\%s.%s\\mission.sqm",
						(const char *)campaign,
						(const char *)mission,
						(const char *)world
					);
				}
				else
				{
					sprintf
					(
						buffer, "Missions\\%s.%s\\mission.sqm",
						(const char *)mission,
						(const char *)world
					);
				}
				unlink(buffer);
*/
				if (campaign.GetLength() > 0)
				{
					sprintf
					(
						buffer, "Campaigns\\%s\\Missions\\%s.%s",
						(const char *)campaign,
						(const char *)mission,
						(const char *)world
					);
				}
				else
				{
					sprintf
					(
						buffer, "Missions\\%s.%s",
						(const char *)mission,
						(const char *)world
					);
				}
				DeleteDirectoryStructure(buffer);
				InsertGames();
			}
			break;
		default:
			Display::OnChildDestroyed(idd, exit);
			break;
	}
}

bool DisplayCustomArcade::CanDestroy()
{
	if (!Display::CanDestroy()) return false;

	if (_exit == IDC_CUST_PLAY)
	{
		CTree *tree = dynamic_cast<CTree *>(GetCtrl(IDC_CUST_GAME));
		CTreeItem *item = tree->GetSelected();
		if (!item) return false;
		return item->level >= 3;
	}
	else if (_exit == IDC_CUST_EDIT)
	{
		CTree *tree = dynamic_cast<CTree *>(GetCtrl(IDC_CUST_GAME));
		CTreeItem *item = tree->GetSelected();
		if (!item) return false;
		return item->level >= 2;
	}
	else
		return true;
}

void DisplayCustomArcade::ShowButtons()
{
	CTree *tree = dynamic_cast<CTree *>(GetCtrl(IDC_CUST_GAME));
	IControl *play = GetCtrl(IDC_CUST_PLAY);
	CButton *edit = dynamic_cast<CButton *>(GetCtrl(IDC_CUST_EDIT));
	IControl *del = GetCtrl(IDC_CUST_DELETE);

	CTreeItem *item = tree->GetSelected();
	if (!item)
	{
		if (play) play->ShowCtrl(false);
		if (edit) edit->ShowCtrl(false);
		if (del) del->ShowCtrl(false);
		return;
	}
	if (play) play->ShowCtrl(item->level >= 3);
	if (del) del->ShowCtrl(item->level >= 3);
	if (edit)
	{
		edit->ShowCtrl(item->level >= 2);
		if (item->level >= 3)
			edit->SetText(LocalizeString(IDS_CUST_EDIT));
		else
			edit->SetText(LocalizeString(IDS_CUST_NEW));
	}
}

void DisplayCustomArcade::OnTreeSelChanged(int idc)
{
	if (idc == IDC_CUST_GAME)
	{
		ShowButtons();
	}
	Display::OnTreeSelChanged(idc);
}

void DisplayCustomArcade::InsertGames()
{
	CTree *tree = dynamic_cast<CTree *>(GetCtrl(IDC_CUST_GAME));
	if (!tree) return;

	tree->RemoveAll();

	CTreeItem *root = tree->GetRoot();
	CTreeItem *selected = NULL;
	root->text = "Root";
	{
		CTreeItem *itemCampaign = root->AddChild();
		itemCampaign->text = "Single Mission";
		itemCampaign->data = "";
		// int m = (Pars>>"CfgWorlds">>"worlds").GetSize();
		int m = (Pars >> "CfgWorldList").GetEntryCount();
		for (int j=0; j<m; j++)
		{
			const ParamEntry &entry = (Pars >> "CfgWorldList").GetEntry(j);
			if (!entry.IsClass()) continue;
			RString name = entry.GetName();

			// RString name = (Pars>>"CfgWorlds">>"worlds")[j];

			// ADDED - check if wrp file exists
			RString fullname = GetWorldName(name);
			if (!QIFStreamB::FileExist(fullname)) continue;

			CTreeItem *itemWorld = itemCampaign->AddChild();

			itemWorld->text = Pars>>"CfgWorlds">>name>>"description";
			itemWorld->data = name;
			bool wexp = stricmp(Glob.header.worldname, name) == 0;
			if (wexp && Glob.header.filename[0] == 0)
				selected = itemWorld;
#ifdef _WIN32
			_finddata_t info;
			char buffer[256];
			sprintf(buffer, "Missions\\*.%s", (const char *)name);
			long h = _findfirst(buffer, &info);
			if (h != -1)
			{
				do
				{
					if 
					(
						(info.attrib & _A_SUBDIR) != 0 &&
						info.name[0] != '.'
					)
					{
						char name[256];
						strcpy(name,info.name);
						char *ext=strrchr(name,'.');
						if( ext ) *ext=0;
						CTreeItem *itemMission = itemWorld->AddChild();
						itemMission->text = name;
						itemMission->data = name;
						if (wexp && stricmp(Glob.header.filename, name) == 0)
							selected = itemMission;
					}
				}
				while (_findnext(h, &info)==0);
				_findclose(h);
			}
#else
			char buffer[2048] = "missions";
                        DIR *dir = opendir(buffer);
                        if ( dir ) {
                            struct dirent *entry;
			    char extension[256];
			    sprintf(extension,".%s",(const char *)name);
			    unixPath(extension);
                            int len = strlen(buffer);
                            buffer[len++] = '/';
                            while ( (entry = readdir(dir)) ) {      // process one directory item..
                                strcpy(buffer+len,entry->d_name);
	                        struct stat st;
	                        if ( entry->d_name[0] != '.' &&
				     isSuffix(entry->d_name,extension) && // valid suffix
				     !stat(buffer,&st) &&                 // valid directory item
				     S_ISDIR(st.st_mode) ) {              // sub-directory
				    char name[256];
                                    strcpy(name,entry->d_name);
				    char *ext = strrchr(name,'.');
				    if ( ext ) *ext = (char)0;
				    CTreeItem *itemMission = itemWorld->AddChild();
				    itemMission->text = name;
				    itemMission->data = name;
				    if ( wexp && stricmp(Glob.header.filename, name) == 0 )
				        selected = itemMission;
				    }
				}
                            closedir(dir);
                            }
#endif
			itemWorld->SortChildren();
		}
	}

#ifdef _WIN32
	_finddata_t info;
	long h = _findfirst("Campaigns\\*.*", &info);
	if (h != -1)
	{
		do
		{
			if 
			(
				(info.attrib & _A_SUBDIR) != 0 &&
				info.name[0] != '.'
			)
			{
				RString campaign = info.name;
				CTreeItem *itemCampaign = root->AddChild();
				ParamFile cfg;
				cfg.Parse(GetCampaignDirectory(campaign) + RString("description.ext"));
				itemCampaign->text = cfg >> "Campaign" >> "name";
				itemCampaign->data = campaign;
				// int m = (Pars>>"CfgWorlds">>"worlds").GetSize();
				int m = (Pars >> "CfgWorldList").GetEntryCount();
				for (int j=0; j<m; j++)
				{
					const ParamEntry &entry = (Pars >> "CfgWorldList").GetEntry(j);
					if (!entry.IsClass()) continue;
					RString name = entry.GetName();

					// RString name = (Pars>>"CfgWorlds">>"worlds")[j];

					// ADDED - check if wrp file exists
					RString fullname = GetWorldName(name);
					if (!QIFStreamB::FileExist(fullname)) continue;

					CTreeItem *itemWorld = itemCampaign->AddChild();
					
					itemWorld->text = Pars>>"CfgWorlds">>name>>"description";
					itemWorld->data = name;
					bool wexp = stricmp(Glob.header.worldname, name) == 0;
					_finddata_t info;
					char buffer[256];
					sprintf
					(
						buffer, "Campaigns\\%s\\Missions\\*.%s",
						(const char *)campaign, (const char *)name
					);
					long h = _findfirst(buffer, &info);
					if (h != -1)
					{
						do
						{
							if 
							(
								(info.attrib & _A_SUBDIR) != 0 &&
								info.name[0] != '.'
							)
							{
								char name[256];
								strcpy(name,info.name);
								char *ext=strrchr(name,'.');
								if( ext ) *ext=0;
								CTreeItem *itemMission = itemWorld->AddChild();
								itemMission->text = name;
								itemMission->data = name;
								if (wexp && stricmp(Glob.header.filename, name) == 0)
									selected = itemMission;
							}
						}
						while (_findnext(h, &info)==0);
						_findclose(h);
					}
					itemWorld->SortChildren();
				}
			}
		}
		while (_findnext(h, &info)==0);
		_findclose(h);
	}
#else
	char buffer[2048] = "campaigns";
        DIR *dir = opendir(buffer);
        if ( dir ) {
            struct dirent *entry;
            int len = strlen(buffer);
            buffer[len++] = '/';
            while ( (entry = readdir(dir)) ) {      // process one directory item..
                strcpy(buffer+len,entry->d_name);
                struct stat st;
                if ( entry->d_name[0] != '.' &&
		     !stat(buffer,&st) &&           // valid directory item
		     S_ISDIR(st.st_mode) ) {        // sub-directory
                    RString campaign = entry->d_name;
		    CTreeItem *itemCampaign = root->AddChild();
		    ParamFile cfg;
		    cfg.Parse(GetCampaignDirectory(campaign) + RString("description.ext"));
		    itemCampaign->text = cfg >> "Campaign" >> "name";
		    itemCampaign->data = campaign;
		    int m = (Pars >> "CfgWorldList").GetEntryCount();
		    for (int j=0; j<m; j++) {
			const ParamEntry &entry = (Pars >> "CfgWorldList").GetEntry(j);
			if (!entry.IsClass()) continue;
			RString name = entry.GetName();
			// ADDED - check if wrp file exists
			RString fullname = GetWorldName(name);
			if (!QIFStreamB::FileExist(fullname)) continue;
			CTreeItem *itemWorld = itemCampaign->AddChild();
			itemWorld->text = Pars>>"CfgWorlds">>name>>"description";
			itemWorld->data = name;
			bool wexp = stricmp(Glob.header.worldname, name) == 0;
			sprintf(buffer+len,"%s/missions",(const char *)campaign);
			char extension[256];
			sprintf(extension,".%s",(const char *)name);
			DIR *sub = opendir(buffer);
			if ( sub ) {
			    struct dirent *subentry;
			    int sublen = strlen(buffer);
			    buffer[sublen++]  = '/';
			    while ( (subentry = readdir(sub)) ) {
			        strcpy(buffer+sublen,subentry->d_name);
				if ( subentry->d_name[0] != '.' &&
				     !stat(buffer,&st) &&
				     S_ISDIR(st.st_mode) ) {
				    char name[256];
				    strcpy(name,subentry->d_name);
				    char *ext=strrchr(name,'.');
				    if ( ext ) *ext = (char)0;
				    CTreeItem *itemMission = itemWorld->AddChild();
				    itemMission->text = name;
				    itemMission->data = name;
				    if ( wexp && stricmp(Glob.header.filename, name) == 0 )
				        selected = itemMission;
				    }
				}
			    closedir(sub);
			    }
			itemWorld->SortChildren();
		        }
		    }
		}
            }
#endif
	if (selected)
	{
		tree->SetSelected(selected);
		while (selected)
		{
			tree->Expand(selected, true);
			selected = selected->parent;
		}
	}
}

// Multiplayer display

#if !_DISABLE_GUI
// Macrovision CD Protection
void __cdecl CDPCreateServer();
CDAPFN_DECLARE_GLOBAL(CDPCreateServer, CDAPFN_OVERHEAD_L2, CDAPFN_CONSTRAINT_NONE);

/*!
\patch 1.34 Date 12/7/2001 by Jirka
- Fixed: When server creation failed, application was in corrupted state
*/
void __cdecl CDPCreateServer()
{
	Display *options = dynamic_cast<Display *>(GWorld->Options());
	if (!options) return;

	int port = GetNetworkPort();
	RString password = GetNetworkPassword();
	GetNetworkManager().Init("", port, false);
	GetNetworkManager().CreateSession(port, password);
	if (GetNetworkManager().IsServer()) options->CreateChild(new DisplayServer(options));

	CDAPFN_ENDMARK(CDPCreateServer);
/*
#if _CZECH
	PerformRandomRoxxeTest_001(CDDrive);
#endif
*/
}

// Macrovision CD Protection
void __cdecl CDPCreateClient(RString ip, int port, RString password);
CDAPFN_DECLARE_GLOBAL(CDPCreateClient, CDAPFN_OVERHEAD_L2, CDAPFN_CONSTRAINT_NONE);

void __cdecl CDPCreateClient(RString ip, int port, RString password)
{
	Display *options = dynamic_cast<Display *>(GWorld->Options());
	if (!options) return;

	GetNetworkManager().Init(ip, port);
	// CHANGED
	if (!GetNetworkManager().WaitForSession())
	{
		GetNetworkManager().Done();
		return;
	}
	RString guid = GetNetworkManager().IPToGUID(ip, port);
	ConnectResult result = GetNetworkManager().JoinSession(guid, password);
	switch (result)
	{
	case CROK:
		options->CreateChild(new DisplayClient(options));
		break;
	case CRPassword:
		options->CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_MP_PASSWORD));
		break;
	case CRVersion:
		options->CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_MP_VERSION));
		break;
	case CRError:
		options->CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_MP_CONNECT_ERROR));
		break;
	case CRSessionFull:
		options->CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_MP_SESSION_FULL));
		break;
	}

	CDAPFN_ENDMARK(CDPCreateClient);

/*
#if _CZECH
	PerformRandomRoxxeTest_001(CDDrive);
#endif
*/
}
#endif

//! Session filter display
class DisplaySessionFilter : public Display
{
	typedef Display base;

protected:
	SessionFilter _filter;

public:
	//! constructor
	/*!
		\param parent parent display
		\param filter current session filter
	*/
	DisplaySessionFilter(ControlsContainer *parent, SessionFilter &filter)
		: Display(parent)
	{
		_enableSimulation = false;
		_filter = filter;
		Load("RscDisplayFilter");
		UpdateValues();
		UpdateButtons();
	}
	void OnButtonClicked(int idc);

	bool CanDestroy();
	void Destroy();

	//! return session filter
	const SessionFilter &GetFilter() const {return _filter;}

protected:
	//! update switches (show/hide)
	void UpdateButtons();

	//! update edit boxes
	void UpdateValues();
};

void DisplaySessionFilter::OnButtonClicked(int idc)
{
	switch (idc)
	{
	case IDC_FILTER_FULL:
		_filter.fullServers = !_filter.fullServers;
		UpdateButtons();
		break;
	case IDC_FILTER_PASSWORDED:
		_filter.passwordedServers = !_filter.passwordedServers;
		UpdateButtons();
		break;
	case IDC_FILTER_DEFAULT:
		_filter = SessionFilter();
		UpdateValues();
		UpdateButtons();
		break;
	default:
		base::OnButtonClicked(idc);
		break;
	}
}

bool DisplaySessionFilter::CanDestroy()
{
	if (!base::CanDestroy()) return false;

	return true;
}

void DisplaySessionFilter::Destroy()
{
	C3DEdit *ctrl = dynamic_cast<C3DEdit *>(GetCtrl(IDC_FILTER_SERVER));
	if (ctrl) _filter.serverName = ctrl->GetText();

	ctrl = dynamic_cast<C3DEdit *>(GetCtrl(IDC_FILTER_MISSION));
	if (ctrl) _filter.missionName = ctrl->GetText();

	ctrl = dynamic_cast<C3DEdit *>(GetCtrl(IDC_FILTER_MAXPING));
	if (ctrl) _filter.maxPing = atoi(ctrl->GetText());

	ctrl = dynamic_cast<C3DEdit *>(GetCtrl(IDC_FILTER_MINPLAYERS));
	if (ctrl) _filter.minPlayers = atoi(ctrl->GetText());

	ctrl = dynamic_cast<C3DEdit *>(GetCtrl(IDC_FILTER_MAXPLAYERS));
	if (ctrl) _filter.maxPlayers = atoi(ctrl->GetText());

	base::Destroy();
}

void DisplaySessionFilter::UpdateValues()
{
	C3DEdit *ctrl = dynamic_cast<C3DEdit *>(GetCtrl(IDC_FILTER_SERVER));
	if (ctrl) ctrl->SetText(_filter.serverName);

	ctrl = dynamic_cast<C3DEdit *>(GetCtrl(IDC_FILTER_MISSION));
	if (ctrl) ctrl->SetText(_filter.missionName);

	ctrl = dynamic_cast<C3DEdit *>(GetCtrl(IDC_FILTER_MAXPING));
	if (ctrl) ctrl->SetText(Format("%d", _filter.maxPing));

	ctrl = dynamic_cast<C3DEdit *>(GetCtrl(IDC_FILTER_MINPLAYERS));
	if (ctrl) ctrl->SetText(Format("%d", _filter.minPlayers));

	ctrl = dynamic_cast<C3DEdit *>(GetCtrl(IDC_FILTER_MAXPLAYERS));
	if (ctrl) ctrl->SetText(Format("%d", _filter.maxPlayers));
}

void DisplaySessionFilter::UpdateButtons()
{
	C3DActiveText *button = dynamic_cast<C3DActiveText *>(GetCtrl(IDC_FILTER_FULL));
	if (button)
	{
		button->SetText
		(
			Format
			(
				LocalizeString(IDS_FILTER_FULL),
				(const char *)(_filter.fullServers ? LocalizeString(IDS_DISP_SHOW) : LocalizeString(IDS_DISP_HIDE))
			)
		);
	}

	button = dynamic_cast<C3DActiveText *>(GetCtrl(IDC_FILTER_PASSWORDED));
	if (button)
	{
		button->SetText
		(
			Format
			(
				LocalizeString(IDS_FILTER_PASSWORDED),
				(const char *)(_filter.passwordedServers ? LocalizeString(IDS_DISP_SHOW) : LocalizeString(IDS_DISP_HIDE))
			)
		);
	}
}

CSessions::CSessions(ControlsContainer *parent, int idc, const ParamEntry &cls)
: C3DListBox(parent, idc, cls)
{
	_password = GlobLoadTexture("data\\zamecek.paa");
	_version = GlobLoadTexture("data\\mission_uncomplete.paa");
	_none = GlobLoadTexture("data\\clear_empty.paa");

	_sb3DWidth = 0.05;
}

inline void DrawColumnLine(Vector3 pos, Vector3 down, float top, float bottom, PackedColor color)
{
	GEngine->DrawLine3D(pos + top * down, pos + bottom * down, color, DisableSun);
}

#define COLUMN_LINE DrawColumnLine(pos - top * down, down, y1c, y2c, color);

/*!
\patch 1.93 Date 9/9/2003 by Jirka
- Fixed: Wrong time left info position in MP session list 
\patch 1.93 Date 9/9/2003 by Jirka
- Improved: Nonconnectable sessions are on the end of MP session list
*/

void CSessions::DrawItem
(
	Vector3Par position, Vector3Par down, int i, float alpha
)
{
	DWORD t = _sessions[i].lastTime;
	float age = t == 0xffffffff ? 0 : 0.001 * (GetTickCount() - t);
	if (age > 10) alpha *= (15.0 - age) * (1.0 / 5.0);

	float y1c = 0;
	float y2c = 1;
	if (i < _topString) y1c = _topString - i;
	if (i > _topString + _rows - 1) y2c = _topString + _rows - i;
	
	Vector3 normal = _down.CrossProduct(_right).Normalized(); 
	Vector3 dir = _right.Normalized();

	float rightSBSize = (1.0 - _sb3DWidth) * _right.Size();
	float border = 0.01 * rightSBSize;
	Vector3 curPos = position - 0.002 * normal;
	
	// background
	bool selected = i == GetCurSel() && IsEnabled();
	PackedColor color;
	if (selected && _showSelected)
	{
		PackedColor selBgColor = ModAlpha(_selBgColor, alpha);
		Vector3 rightSB = _right;
		if (GetSize() > _rows) rightSB = (1.0 - _sb3DWidth) * _right;
		GEngine->Draw3D
		(
			position, down, rightSB, ClipAll, selBgColor, DisableSun, NULL,
			0, y1c, 1, y2c
		);

		color = ModAlpha(_selColor, alpha);
	}
	else
		color = ModAlpha(_ftColor, alpha);

	// first line
	float top = 0.05;
	float size = 0.4;
	float y1ct = 0;
	float y2ct = 1;
	if (y1c > top)
		y1ct = (y1c - top) / size;
	if (y2c < top + size)
		y2ct = (y2c - top) / size;
	Vector3 pos = curPos + top * down;
	Vector3 up = -size * down;
	Vector3 right = 0.75 * up.Size() * dir;
	float invRightSize = 1.0 / right.Size();

	// icon
	Texture *texture = NULL;
	PackedColor col = color;
	if (_sessions[i].badActualVersion || _sessions[i].badRequiredVersion || _sessions[i].badMod)
	{
		texture = _version;
		col = PackedColor(Color(1, 0, 0, alpha));
	}
	else if (_sessions[i].password)
	{
		texture = _password;
	}
	else texture = _none;
	float iconSize = 0;
	if (texture)
	{
		Vector3 right = (float)texture->AWidth() / (float)texture->AHeight() * up.Size() * dir;
		float rightSize = right.Size();
		float x2c = 1;
		if (rightSize > rightSBSize) x2c = rightSBSize / rightSize;
		GEngine->Draw3D
		(
			pos + border * dir, -up, right, ClipAll, col,
			DisableSun, texture,
			0, y1ct, x2c, y2ct
		);
		iconSize = rightSize + border;
	}

	// session name
	RString text = _sessions[i].name;
	float column = 0.38 * rightSBSize;
	float x2c = (column - iconSize - 2.0 * border) * invRightSize;
	GEngine->DrawText3D
	(
		pos + (iconSize + border) * dir, up, right, ClipAll, _font, color, DisableSun, text,
		0, y1ct, x2c, y2ct
	);
	pos += column * dir;
	COLUMN_LINE

	// mission
	column = 0.3 * rightSBSize;
	x2c = (column - 2.0 * border) * invRightSize;
	if (_sessions[i].mission.GetLength() > 0)
	{
		text = _sessions[i].mission;
		col = color;
		GEngine->DrawText3D
		(
			pos + border * dir, up, right, ClipAll, _font, col, DisableSun, text,
			0, y1ct, x2c, y2ct
		);
	}
	pos += column * dir;
	COLUMN_LINE

	// state
	column = 0.14 * rightSBSize;
	x2c = (column - 2.0 * border) * invRightSize;
	RString state;
	col = color;
	switch (_sessions[i].gameState)
	{
		case NGSCreating:
		case NGSCreate:
		case NGSLogin:
			state = LocalizeString(IDS_SESSION_CREATE);
			break;
		case NGSEdit:
			state = LocalizeString(IDS_SESSION_EDIT);
			break;
		case NGSPrepareSide:
			state = LocalizeString(IDS_SESSION_WAIT);
			break;
		case NGSPrepareRole:
		case NGSPrepareOK:
		case NGSTransferMission:
		case NGSLoadIsland:
			state = LocalizeString(IDS_SESSION_SETUP);
			col = PackedColor(Color(1, 0, 0, alpha));
			break;
		case NGSDebriefing:
		case NGSDebriefingOK:
			state = LocalizeString(IDS_SESSION_DEBRIEFING);
			col = PackedColor(Color(1, 0, 0, alpha));
			break;
		case NGSBriefing:
			state = LocalizeString(IDS_SESSION_BRIEFING);
			col = PackedColor(Color(1, 0, 0, alpha));
			break;
		case NGSPlay:
			state = LocalizeString(IDS_SESSION_PLAY);
			col = PackedColor(Color(1, 0, 0, alpha));
			break;
	}
	GEngine->DrawText3D
	(
		pos + border * dir, up, right, ClipAll, _font, col, DisableSun, state,
		0, y1ct, x2c, y2ct
	);
	pos += column * dir;
	COLUMN_LINE

	// players
    if ( _sessions[i].maxPlayers )
	    text = Format("%d/%d", _sessions[i].numPlayers, _sessions[i].maxPlayers - 2 );
    else
	    text = Format("%d", _sessions[i].numPlayers );
	column = 0.10 * rightSBSize;
	x2c = (column - 2.0 * border) * invRightSize;
	GEngine->DrawText3D
	(
		pos + border * dir, up, right, ClipAll, _font, color, DisableSun, text,
		0, y1ct, x2c, y2ct
	);
	pos += column * dir;
	COLUMN_LINE

	// ping
	text = Format("%d", _sessions[i].ping);
	column = 0.08 * rightSBSize;
	x2c = (column - 2.0 * border) * invRightSize;
	GEngine->DrawText3D
	(
		pos + border * dir, up, right, ClipAll, _font, color, DisableSun, text,
		0, y1ct, x2c, y2ct
	);
/*
	pos += column * dir;
	COLUMN_LINE
*/

	// second line
	top = 0.5;
	size = 0.4;
	y1ct = 0;
	y2ct = 1;
	if (y1c > top)
		y1ct = (y1c - top) / size;
	if (y2c < top + size)
		y2ct = (y2c - top) / size;
	pos = curPos + top * down;
	up = -size * down;
	right = 0.75 * up.Size() * dir;
	invRightSize = 1.0 / right.Size();
	
	// versions
	column = 0.38 * rightSBSize;
	x2c = (column - 2.0 * border) * invRightSize;
	char buffer[256];
	float ver = 0.01f * _sessions[i].actualVersion;
	saturateMax(ver, 1.0f);
	sprintf(buffer, LocalizeString(IDS_SESSION_VERSION), ver);
	col = _sessions[i].badActualVersion ? PackedColor(Color(1, 0, 0, alpha)) : color;
	GEngine->DrawText3D
	(
		pos + border * dir, up, right, ClipAll, _font, col, DisableSun, buffer,
		0, y1ct, x2c, y2ct
	);
	Vector3 width = GEngine->GetText3DWidth(right, _font, buffer);
	//pos += width;
	x2c -= width.Size() * invRightSize;

	ver = 0.01f * _sessions[i].requiredVersion;
	saturateMax(ver, 1.0f);
	sprintf(buffer, LocalizeString(IDS_SESSION_REQUIRED), ver);
	col = _sessions[i].badRequiredVersion ? PackedColor(Color(1, 0, 0, alpha)) : color;
	GEngine->DrawText3D
	(
		pos + width + border * dir, up, right, ClipAll, _font, col, DisableSun, buffer,
		0, y1ct, x2c, y2ct
	);
	pos += column * dir;

  // mods
  column = 0.3 * rightSBSize;
  x2c = (column - 2.0 * border) * invRightSize;
  col = _sessions[i].badMod ? PackedColor(Color(1, 0, 0, alpha)) : color;
  text = _sessions[i].mod;
  GEngine->DrawText3D
  (
    pos + border * dir, up, right, ClipAll, _font, col, DisableSun, text,
    0, y1ct, x2c, y2ct
  );
  pos += column * dir;

  // time left
  column = 0.14 * rightSBSize;
  x2c = (column - 2.0 * border) * invRightSize;
	if (_sessions[i].mission.GetLength() > 0 && _sessions[i].timeleft > 0)
	{
		text = Format(LocalizeString(IDS_TIME_LEFT),_sessions[i].timeleft);
		col = color;
		GEngine->DrawText3D
		(
			pos + border * dir, up, right, ClipAll, _font, col, DisableSun, text,
			0, y1ct, x2c, y2ct
		);
	}
}

struct CmpSessionsContext
{
	SortColumn column;
	bool ascending;
};

int CmpSessions(const SessionInfo *info1, const SessionInfo *info2, CmpSessionsContext ctx)
{
  bool bad1 = info1->badActualVersion || info1->badRequiredVersion || info1->badMod;
  bool bad2 = info2->badActualVersion || info2->badRequiredVersion || info2->badMod;
  int diff = bad1 - bad2;
  if (diff != 0) return diff;

  int value = 0;
	switch (ctx.column)
	{
		case SCServer:
			value = stricmp(info1->name, info2->name);
			break;
		case SCMission:
			value = stricmp(info1->mission, info2->mission);
			break;
		case SCState:
			value = info1->gameState - info2->gameState;
			break;
		case SCPlayers:
			value = info1->numPlayers - info2->numPlayers;
			break;
		case SCPing:
			value = info1->ping - info2->ping;
			break;
	}
	if (ctx.ascending) return value;
	else return -value;
}

void CSessions::Sort(SortColumn column, bool ascending)
{
	// remember actually selected row
	RString selGUID;
	int sel = GetCurSel();
	if (sel >= 0 && sel < GetSize()) selGUID = GetData(sel);

	// sort list
	CmpSessionsContext ctx;
	ctx.column = column;
	ctx.ascending = ascending;
	QSort(_sessions.Data(), _sessions.Size(), ctx, CmpSessions);

	// select original selected row
	sel = 0;
	for (int i=0; i<GetSize(); i++)
		if (GetData(i) == selGUID)
		{
			sel = i;
			break;
		}
	SetCurSel(sel);
}

bool IsUseSockets();
void SetUseSockets(bool set);

#if _ENABLE_GAMESPY
void ListCallBack(GServerList serverlist, int msg, void *instance, void *param1, void *param2)
{
	if (msg == LIST_PROGRESS)
	{
		((DisplayMultiplayer *)instance)->SetProgress(0.01 * (int)param2);
	}
}
#endif

DisplayMultiplayer::DisplayMultiplayer(ControlsContainer *parent)
	: Display(parent)
{
	_enableSimulation = false;
	_sessions = NULL;
	_portRemote = _portLocal = GetNetworkPort();

	_source = BSLAN;

	Load("RscDisplayMultiplayer");
	LoadParams();

	_refresh = true;
	_refreshing = false;

	bool datadisc = false;
#if _ENABLE_DATADISC
	datadisc = true;
#endif
#if _ENABLE_GAMESPY
#if _ENABLE_MP
	_serverList = ServerListNew
	(
		GetGameName(datadisc), GetGameName(true), GetSecretKey(true),
		10, (void*)&ListCallBack, GCALLBACK_FUNCTION, this
	);
#else
	_serverList = NULL;
#endif
#endif

	_sort = SCPing;
	_ascending = true;
	_sortUp = "\\misc\\sipkau.paa";
	_sortDown = "\\misc\\sipkad.paa";

	BrowsingSource source = _source;
	if (source == BSRemote) source = BSLAN;
	
	UpdateAddress("", GetNetworkPort());
	UpdatePassword("");
	UpdateSessions();
	UpdateSockets(IsUseSockets());

	SetSource(source);
	SetProgress(0);

	UpdateIcons();
	UpdateFilter();
}

DisplayMultiplayer::~DisplayMultiplayer()
{
#if _ENABLE_MP && _ENABLE_GAMESPY
	ServerListFree(_serverList);
#endif
}

Control *DisplayMultiplayer::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
	case IDC_MULTI_SESSIONS:
		_sessions = new CSessions(this, idc, cls);
		return _sessions;
	default:
		return Display::OnCreateCtrl(type, idc, cls);
	}
}

void DisplayMultiplayer::OnSimulate(EntityAI *vehicle)
{
#if _ENABLE_GAMESPY
	if (_source == BSInternet)
		UpdateServerList();
	else
#endif
		UpdateSessions();
	Display::OnSimulate(vehicle);
}

void __cdecl CDPCreateDisplayIPAddress(ControlsContainer *parent);
CDAPFN_DECLARE_GLOBAL(CDPCreateDisplayIPAddress, CDAPFN_OVERHEAD_L2, CDAPFN_CONSTRAINT_NONE);

void __cdecl CDPCreateDisplayIPAddress(ControlsContainer *parent)
{
	parent->CreateChild(new DisplayIPAddress(parent));

	CDAPFN_ENDMARK(CDPCreateDisplayIPAddress);

/*
#if _CZECH
	PerformRandomRoxxeTest_002(CDDrive);
#endif
*/
}

/*!
\patch 1.79 Date 7/24/2002 by Jirka
- Fixed: Multiplayer session list: bad title appears when open the screen and Internet sessions was selected last
*/
void DisplayMultiplayer::SetSource(BrowsingSource source)
{
	_source = source;
	SaveParams();

#if _ENABLE_GAMESPY
	bool internet = source == BSInternet;
#else
	bool internet = false;
#endif
	if (GetCtrl(IDC_MULTI_GAMESPY)) GetCtrl(IDC_MULTI_GAMESPY)->ShowCtrl(internet);
	if (GetCtrl(IDC_MULTI_PROGRESS)) GetCtrl(IDC_MULTI_PROGRESS)->ShowCtrl(internet);
	if (GetCtrl(IDC_MULTI_REFRESH)) GetCtrl(IDC_MULTI_REFRESH)->ShowCtrl(internet);
	if (GetCtrl(IDC_MULTI_FILTER)) GetCtrl(IDC_MULTI_FILTER)->ShowCtrl(internet);
	if (GetCtrl(IDC_MULTI_PORT)) GetCtrl(IDC_MULTI_PORT)->ShowCtrl(!internet);

	C3DActiveText *button = dynamic_cast<C3DActiveText *>(GetCtrl(IDC_MULTI_INTERNET));
	if (button)
	{
		if (internet) button->SetText
		(
			Format(LocalizeString(IDS_SESSIONS_SOURCE), (const char *)LocalizeString(IDS_SESSIONS_INTERNET))
		);
		else if (source == BSLAN) button->SetText
		(
			Format(LocalizeString(IDS_SESSIONS_SOURCE), (const char *)LocalizeString(IDS_MULTI_LAN))
		);
		else button->SetText
		(
			Format(LocalizeString(IDS_SESSIONS_SOURCE), (const char *)_ipAddress)
		);
	}

	if (internet)
	{
		CStatic *title = dynamic_cast<CStatic *>(GetCtrl(IDC_MULTI_TITLE));
		if (title)
		{
			title->SetText(LocalizeString(IDS_MULTI_TITLE_INTERNET));
		}
		_refresh = true;
	}
}

void DisplayMultiplayer::UpdateIcons()
{
	C3DStatic *icon = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MULTI_SERVER_ICON));
	if (icon)
	{
		if (_sort == SCServer)
		{
			icon->ShowCtrl(true);
			if (_ascending) icon->SetText(_sortUp);
			else icon->SetText(_sortDown);
		}
		else icon->ShowCtrl(false);
	}
	
	icon = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MULTI_MISSION_ICON));
	if (icon)
	{
		if (_sort == SCMission)
		{
			icon->ShowCtrl(true);
			if (_ascending) icon->SetText(_sortUp);
			else icon->SetText(_sortDown);
		}
		else icon->ShowCtrl(false);
	}
	
	icon = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MULTI_STATE_ICON));
	if (icon)
	{
		if (_sort == SCState)
		{
			icon->ShowCtrl(true);
			if (_ascending) icon->SetText(_sortUp);
			else icon->SetText(_sortDown);
		}
		else icon->ShowCtrl(false);
	}
	
	icon = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MULTI_PLAYERS_ICON));
	if (icon)
	{
		if (_sort == SCPlayers)
		{
			icon->ShowCtrl(true);
			if (_ascending) icon->SetText(_sortUp);
			else icon->SetText(_sortDown);
		}
		else icon->ShowCtrl(false);
	}
	
	icon = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MULTI_PING_ICON));
	if (icon)
	{
		if (_sort == SCPing)
		{
			icon->ShowCtrl(true);
			if (_ascending) icon->SetText(_sortUp);
			else icon->SetText(_sortDown);
		}
		else icon->ShowCtrl(false);
	}
}

void DisplayMultiplayer::UpdateFilter()
{
	C3DStatic *text = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MULTI_SERVER_FILTER));
	if (text)
		if (_filter.serverName.GetLength() > 0)
			text->SetText(Format("(%s)", (const char *)_filter.serverName));
		else
			text->SetText(RString());

	text = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MULTI_MISSION_FILTER));
	if (text)
		if (_filter.missionName.GetLength() > 0)
			text->SetText(Format("(%s)", (const char *)_filter.missionName));
		else
			text->SetText(RString());

	text = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MULTI_PLAYERS_FILTER));
	if (text)
		if (_filter.minPlayers > 0 || _filter.maxPlayers > 0)
			text->SetText(Format("%d..%d", _filter.minPlayers, _filter.maxPlayers));
		else
			text->SetText(RString());

	text = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MULTI_PING_FILTER));
	if (text)
		if (_filter.maxPing > 0)
			text->SetText(Format("<%d", _filter.maxPing));
		else
			text->SetText(RString());
}

void DisplayMultiplayer::SetProgress(float progress)
{
	CProgressBar *ctrl = dynamic_cast<CProgressBar *>(GetCtrl(IDC_MULTI_PROGRESS));
	if (ctrl) ctrl->SetPos(progress);
}


/*!
\patch 1.50 Date 4/9/2002 by Jirka
- Added: Can choose DirectPlay / Sockets MP implementation at run time. 
\patch 1.50 Date 4/12/2002 by Jirka
- Improved: Multiplayer screen design
*/

void DisplayMultiplayer::OnButtonClicked(int idc)
{
	switch (idc)
	{
		case IDC_MULTI_REMOTE:
			// Macrovision CD Protection
			CDPCreateDisplayIPAddress(this);
			break;
		case IDC_MULTI_INTERNET:
#if _ENABLE_GAMESPY
			if (_source == BSInternet)
			{
				UpdateAddress("", _portLocal);
			}
			else
			{
				SetSource(BSInternet);
			}
#else
			UpdateAddress("", _portLocal);
#endif
			break;
		case IDC_MULTI_PASSWORD:
			CreateChild(new DisplayPassword(this, _password));
			break;
		case IDC_MULTI_PORT:
			CreateChild(new DisplayPort(this, GetPort()));
			break;
		case IDC_MULTI_FILTER:
			CreateChild(new DisplaySessionFilter(this, _filter));
			break;
		case IDC_MULTI_NEW:
			// create session
			GetNetworkManager().CreateSession(GetPort(), _password);
			if (GetNetworkManager().IsServer()) CreateChild(new DisplayServer(this));
			break;
		case IDC_MULTI_JOIN:
			{
				C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_MULTI_SESSIONS));
				if (lbox)
				{
					int sel = lbox->GetCurSel();
					if (sel >= 0)
					{
						// join session
						ConnectResult result = GetNetworkManager().JoinSession(lbox->GetData(sel), _password);
						switch (result)
						{
						case CROK:
							CreateChild(new DisplayClient(this));
							break;
						case CRPassword:
							CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_MP_PASSWORD));
							break;
						case CRVersion:
							CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_MP_VERSION));
							break;
						case CRError:
							CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_MP_CONNECT_ERROR));
							break;
						case CRSessionFull:
							CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_MP_SESSION_FULL));
							break;
						}
					}
				}
			}
			break;
		case IDC_CANCEL:
			{
				_exitWhenClose = idc;
				ControlObjectContainerAnim *ctrl =
					dynamic_cast<ControlObjectContainerAnim *>(GetCtrl(IDC_MULTI_NOTEBOOK));
				if (ctrl) ctrl->Close();
			}
			break;
		case IDC_MULTI_DPLAY:
			{
				UpdateSockets(!IsUseSockets());
				SaveParams();
			}
			break;
		case IDC_MULTI_SERVER_COLUMN:
			if (_sort == SCServer)
				_ascending = !_ascending;
			else
			{
				_sort = SCServer;
				_ascending = true;
			}
			_sessions->Sort(_sort, _ascending);
			UpdateIcons();
			break;
		case IDC_MULTI_MISSION_COLUMN:
			if (_sort == SCMission)
				_ascending = !_ascending;
			else
			{
				_sort = SCMission;
				_ascending = true;
			}
			_sessions->Sort(_sort, _ascending);
			UpdateIcons();
			break;
		case IDC_MULTI_STATE_COLUMN:
			if (_sort == SCState)
				_ascending = !_ascending;
			else
			{
				_sort = SCState;
				_ascending = true;
			}
			_sessions->Sort(_sort, _ascending);
			UpdateIcons();
			break;
		case IDC_MULTI_PLAYERS_COLUMN:
			if (_sort == SCPlayers)
				_ascending = !_ascending;
			else
			{
				_sort = SCPlayers;
				_ascending = true;
			}
			_sessions->Sort(_sort, _ascending);
			UpdateIcons();
			break;
		case IDC_MULTI_PING_COLUMN:
			if (_sort == SCPing)
				_ascending = !_ascending;
			else
			{
				_sort = SCPing;
				_ascending = true;
			}
			_sessions->Sort(_sort, _ascending);
			UpdateIcons();
			break;
		case IDC_MULTI_REFRESH:
			_refresh = true;
			break;
		default:
			Display::OnButtonClicked(idc);
			break;
	}
}

void DisplayMultiplayer::OnLBDblClick(int idc, int curSel)
{
	if (idc == IDC_MULTI_SESSIONS && curSel >= 0)
		OnButtonClicked(IDC_MULTI_JOIN);
	else
		Display::OnLBDblClick(idc, curSel);
}

void DisplayMultiplayer::OnCtrlClosed(int idc)
{
	if (idc == IDC_MULTI_NOTEBOOK)
		Exit(_exitWhenClose);
	else
		Display::OnCtrlClosed(idc);
}

RString GetUserParams();

void DisplayMultiplayer::OnChildDestroyed(int idd, int exit)
{
	switch (idd)
	{
		case IDD_PASSWORD:
			if (exit == IDC_OK)
			{
				C3DEdit *edit = dynamic_cast<C3DEdit *>(_child->GetCtrl(IDC_PASSWORD));
				if (edit)
				{
					UpdatePassword(edit->GetText());
				}
			}
			Display::OnChildDestroyed(idd, exit);
			break;
		case IDD_IP_ADDRESS:
			if (exit == IDC_OK)
			{
				C3DEdit *editIP = dynamic_cast<C3DEdit *>(_child->GetCtrl(IDC_IP_ADDRESS));
				C3DEdit *editPort = dynamic_cast<C3DEdit *>(_child->GetCtrl(IDC_IP_PORT));
				if (editIP && editPort)
				{
					RString ip = editIP->GetText();
	
					if (ip.GetLength() > 0)
					{
						int port = atoi(editPort->GetText());
						if (port <= 0) port = GetNetworkPort();
						UpdateAddress(ip, port);

						RString filename = GetUserParams();
						if (QIFStream::FileExists(filename))
						{
							ParamFile cfg;
							cfg.Parse(filename);
							cfg.Add("remoteIPAddress", ip);
							cfg.Add("remotePort", port);
							cfg.Save(filename);
						}
					}
					else
					{
						UpdateAddress("", _portLocal);
					}
				}
			}
			Display::OnChildDestroyed(idd, exit);
			break;
		case IDD_PORT:
			if (exit == IDC_OK)
			{
				C3DEdit *edit = dynamic_cast<C3DEdit *>(_child->GetCtrl(IDC_PORT_PORT));
				if (edit)
				{
					int port = atoi(edit->GetText());
					if (port <= 0) port = GetNetworkPort();
					UpdateAddress(_ipAddress, port);
				}
			}
			Display::OnChildDestroyed(idd, exit);
			break;
		case IDD_FILTER:
			if (exit == IDC_OK)
			{
				DisplaySessionFilter *display = dynamic_cast<DisplaySessionFilter *>(_child.GetRef());
				if (display)
				{
					_filter = display->GetFilter();
					SaveParams();
					UpdateFilter();
					_refresh = true;
				}
			}
			Display::OnChildDestroyed(idd, exit);
			break;
		case IDD_SERVER:
		case IDD_CLIENT:
			GetNetworkManager().Close();
			Display::OnChildDestroyed(idd, exit);
			break;
		default:
			Display::OnChildDestroyed(idd, exit);
			break;
	}
}

void DisplayMultiplayer::UpdateAddress(RString ipAddress, int port)
{
	_ipAddress = ipAddress;
	SetPort(port);
	bool lan = _ipAddress.GetLength() == 0;
	SetSource(lan ? BSLAN : BSRemote);

	CStatic *title = dynamic_cast<CStatic *>(GetCtrl(IDC_MULTI_TITLE));
	if (title)
	{
		char buffer[256];
		if (lan)
		{
			sprintf(buffer, LocalizeString(IDS_MULTI_TITLE_LAN), port);
		}
		else
		{
			sprintf(buffer, LocalizeString(IDS_MULTI_TITLE_REMOTE), (const char *)_ipAddress, port);
		}
		title->SetText(buffer);
	}
	
	if (!GetNetworkManager().Init(ipAddress, port))
	{
		Fail("Network");
	}
}

void DisplayMultiplayer::UpdatePassword(RString password)
{
	_password = password;

	C3DActiveText *button = dynamic_cast<C3DActiveText *>(GetCtrl(IDC_MULTI_PASSWORD));
	if (button)
	{
		if (password.GetLength() > 0)
		{
			BString<512> buffer;
			sprintf(buffer, LocalizeString(IDS_PASSWORD), (const char *)password);
			button->SetText((const char *)buffer);
		}
		else
		{
			BString<512> buffer;
			sprintf(buffer, LocalizeString(IDS_PASSWORD), (const char *)LocalizeString(IDS_NO_PASSWORD));
			button->SetText((const char *)buffer);
		}
	}
}

void DisplayMultiplayer::UpdateSessions()
{
	if (!_sessions) return;

	RString selGUID;
	int sel = _sessions->GetCurSel();
	if (sel >= 0 && sel < _sessions->GetSize()) selGUID = _sessions->GetData(sel);

	_sessions->_sessions.Resize(0);
	GetNetworkManager().GetSessions(_sessions->_sessions);

	sel = 0;
	for (int i=0; i<_sessions->GetSize(); i++)
		if (_sessions->GetData(i) == selGUID)
		{
			sel = i;
			break;
		}
	_sessions->SetCurSel(sel);
}

static RString And(" and ");
inline void AddCondition(RString &filter, RString condition)
{
	if (filter.GetLength() > 0) filter = filter + And;
	filter = filter + condition;
}

RString CreateServerFilter(const SessionFilter &filter)
{
	RString value;
	if (filter.serverName.GetLength() > 0)
		AddCondition(value, Format("hostname like '%%%s%%'", (const char *)filter.serverName));
	if (filter.missionName.GetLength() > 0)
		AddCondition(value, Format("gametype like '%%%s%%'", (const char *)filter.missionName));

	if (filter.minPlayers > 0)
		AddCondition(value, Format("numplayers >= %d", filter.minPlayers));
	if (filter.maxPlayers > 0)
		AddCondition(value, Format("numplayers <= %d", filter.maxPlayers));

	if (!filter.fullServers)
		AddCondition(value, "numplayers < maxplayers");

	return value;
}

/*!
\patch 1.59 Date 5/24/2002 by Jirka
- Added: Ingame server browser 
\patch 1.89 Date 10/23/2002 by Jirka
- Fixed: Joining to DirectPlay sessions through ingame browser often failed
*/


#if _ENABLE_GAMESPY
void DisplayMultiplayer::UpdateServerList()
{
	if (!_sessions || !_serverList) return;

	if (_refresh)
	{
		SetProgress(0);
		GServerListState state = ServerListState(_serverList);
		if (state != sl_idle)
			ServerListHalt(_serverList);
		else
		{
			// start new query
			RString filter = CreateServerFilter(_filter);	// server side filtering
			ServerListClear(_serverList);
			ServerListUpdate2(_serverList, true, (char *)(const char *)filter, qt_info_rules);

			_refresh = false;
			_refreshing = true;
		}
	}
	
	if (_refreshing)
	{
		GServerListState state = ServerListState(_serverList);
		if (state != sl_idle)
			ServerListThink(_serverList);
		else
		{
			// get current selection
			RString selGUID;
			int sel = _sessions->GetCurSel();
			if (sel >= 0 && sel < _sessions->GetSize()) selGUID = _sessions->GetData(sel);

			// update _sessions
			int n = ServerListCount(_serverList);
			_sessions->_sessions.Resize(0);
			for (int i=0; i<n; i++)
			{
				GServer server = ServerListGetServer(_serverList, i);

				// client side filtering
				RString implementation = ServerGetStringValue(server, "impl", "");
				RString wantedImplementation = IsUseSockets() ? "sockets" : "dplay";
				if (stricmp(implementation, wantedImplementation) != 0) continue;

				bool password = ServerGetIntValue(server, "password", 0) != 0;
				if (!_filter.passwordedServers && password) continue;

				int ping = ServerGetPing(server);
				if (_filter.maxPing > 0 && ping > _filter.maxPing) continue;

				// ok - add new session
				int index = _sessions->_sessions.Add();
				SessionInfo &session = _sessions->_sessions[index];

				const char *ip = ServerGetAddress(server);
				int port = ServerGetIntValue(server, "hostport", 0);
				
				session.guid = GetNetworkManager().IPToGUID(ip, port);

				session.name = ServerGetStringValue(server, "hostname", "");
				session.mission = ServerGetStringValue(server, "gametype", "");
				session.lastTime = 0xFFFFFFFF;

				session.actualVersion = ServerGetIntValue(server, "actver", 0);
				session.requiredVersion = ServerGetIntValue(server, "reqver", 0);
				session.password = password;
				session.gameState = ServerGetIntValue(server, "gstate", NGSCreate);
				
				void CheckMPVersion(SessionInfo &info);
				CheckMPVersion(session);

				session.ping = ping;
				session.numPlayers = ServerGetIntValue(server, "numplayers", 0);
				session.maxPlayers = ServerGetIntValue(server, "maxplayers", 0);
				
				session.timeleft = ServerGetIntValue(server, "timeleft", 15);

        session.mod = ServerGetStringValue(server, "mod", "");
        session.equalModRequired = ServerGetIntValue(server, "equalModRequired", 0) != 0;
			}

			// set current selection
			sel = 0;
			for (int i=0; i<_sessions->GetSize(); i++)
				if (_sessions->GetData(i) == selGUID)
				{
					sel = i;
					break;
				}
			_sessions->SetCurSel(sel);

			// sort server list
			_sessions->Sort(_sort, _ascending);

			_refreshing = false;
		}
	}
}
#endif

int DisplayMultiplayer::GetPort()
{
	if (_ipAddress.GetLength() == 0) return _portLocal;
	else return _portRemote;
}

void DisplayMultiplayer::SetPort(int port)
{
	if (_ipAddress.GetLength() == 0) _portLocal = port;
	else _portRemote = port;

	C3DActiveText *button = dynamic_cast<C3DActiveText *>(GetCtrl(IDC_MULTI_PORT));
	if (button)
	{
		BString<512> buffer;
		sprintf(buffer, LocalizeString(IDS_DISP_PORT_BUTTON), port);
		button->SetText((const char *)buffer);
	}
}

void DisplayMultiplayer::UpdateSockets(bool sockets)
{
	C3DActiveText *text = dynamic_cast<C3DActiveText *>(GetCtrl(IDC_MULTI_DPLAY));
	if (text)
	{
		if (sockets)
			text->SetText(LocalizeString(IDS_DISP_MULTI_SOCKETS));
		else
			text->SetText(LocalizeString(IDS_DISP_MULTI_DPLAY));
	}

	if (sockets != IsUseSockets())
	{
		GetNetworkManager().Done();
		SetUseSockets(sockets);
		GetNetworkManager().Init(_ipAddress, GetPort());
	}

	_sessions->_sessions.Clear();
	_refresh = true;
}

LSError SessionFilter::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("serverName", serverName, 1))
	CHECK(ar.Serialize("missionName", missionName, 1))
	CHECK(ar.Serialize("maxPing", maxPing, 1))
	CHECK(ar.Serialize("minPlayers", minPlayers, 1))
	CHECK(ar.Serialize("maxPlayers", maxPlayers, 1))
	CHECK(ar.Serialize("fullServers", fullServers, 1))
	CHECK(ar.Serialize("passwordedServers", passwordedServers, 1))
	return LSOK;
}

DEFINE_ENUM(BrowsingSource, BS, BROWSING_SOURCE_ENUM)

void DisplayMultiplayer::LoadParams()
{
	ParamArchiveLoad ar(GetUserParams());
	bool sockets = true;
	if (ar.Serialize("useSockets", sockets, 1, true) == LSOK)
	{
		SetUseSockets(sockets);
	}
	else
	{
		WarningMessage("Cannot load user paremeters.");
	}
	
	ar.SerializeEnum("browsingSource", _source, 1, BSLAN);
	ParamArchive arSubcls;
	if (ar.IsSubclass("Filter"))
	{
		ar.Serialize("Filter", _filter, 1);
	}
}

void DisplayMultiplayer::SaveParams()
{
	ParamArchiveSave ar(UserInfoVersion);
	ar.Parse(GetUserParams());
	bool sockets = IsUseSockets();
	if (ar.Serialize("useSockets", sockets, 1) != LSOK)
	{
		// TODO: save failed
	}
	ar.SerializeEnum("browsingSource", _source, 1);
	ar.Serialize("Filter", _filter, 1);

	if (ar.Save(GetUserParams()) != LSOK)
	{
		// TODO: save failed
	}
}

Control *DisplayPassword::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
		case IDC_PASSWORD:
		{
			C3DEdit *edit = new C3DEdit(this, idc, cls);
			edit->SetText(_password);
			return edit;
		}
		default:
			return Display::OnCreateCtrl(type, idc, cls);
	}
}

Control *DisplayPort::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
		case IDC_PORT_PORT:
		{
			C3DEdit *edit = new C3DEdit(this, idc, cls);
			char buffer[256];
			#ifdef _WIN32
			itoa(_port, buffer, 10);
			#else
			sprintf(buffer,"%d",_port);
			#endif
			edit->SetText(buffer);
			return edit;
		}
		default:
			return Display::OnCreateCtrl(type, idc, cls);
	}
}

Control *DisplayIPAddress::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
		case IDC_IP_ADDRESS:
		{
			C3DEdit *edit = new C3DEdit(this, idc, cls);
			RString address;
			RString filename = GetUserParams();
			if (QIFStream::FileExists(filename))
			{
				ParamFile cfg;
				cfg.Parse(filename);
				const ParamEntry *entry = cfg.FindEntry("remoteIPAddress");
				if (entry) address = *entry;
			}			
			edit->SetText(address);
			return edit;
		}
		case IDC_IP_PORT:
		{
			C3DEdit *edit = new C3DEdit(this, idc, cls);
			int port = GetNetworkPort();
			RString filename = GetUserParams();
			if (QIFStream::FileExists(filename))
			{
				ParamFile cfg;
				cfg.Parse(filename);
				const ParamEntry *entry = cfg.FindEntry("remotePort");
				if (entry) port = *entry;
			}			
			char buffer[256];
			#ifdef _WIN32
			itoa(port, buffer, 10);
			#else
			sprintf(buffer,"%d",port);
			#endif
			edit->SetText(buffer);
			return edit;
		}
		default:
			return Display::OnCreateCtrl(type, idc, cls);
	}
}

bool DisplayIPAddress::CanDestroy()
{
	if (!Display::CanDestroy()) return false;

	if (_exit == IDC_OK)
	{
		C3DEdit *edit = dynamic_cast<C3DEdit *>(GetCtrl(IDC_IP_ADDRESS));
		Assert(edit);
		RString address = edit->GetText();
		// TODO: check IP address
		int n = address.GetLength() - 1;
		for (; n>=0; n--)
		{
			if (!isspace(address[n])) break;
		}
		if (n < 0 || address[n] != '.') return true;
		CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_MP_BAD_ADDRESS));
		return false;
	}

	return true;
}

class CMPMissionsList : public C3DListBox
{
	typedef C3DListBox base;
public:
	CMPMissionsList(ControlsContainer *parent, int idc, const ParamEntry &cls);
	virtual void DrawTooltip(float x, float y);
};

CMPMissionsList::CMPMissionsList(ControlsContainer *parent, int idc, const ParamEntry &cls)
	: C3DListBox(parent, idc, cls)
{
}

void CMPMissionsList::DrawTooltip(float x, float y)
{
	_tooltip = RString();

	if (_scrollbar.IsLocked()) return;
	if (!IsReadOnly() && IsInside(x, y))
	{
		if (_scrollbar.IsEnabled() && _u > (1.0 - _sb3DWidth)) return;

		float index = _v * _rows;
		if (index >= 0 && index < _rows)
		{
			int sel = toIntFloor(_topString + index);
			if (sel >= 0 && sel < GetSize()) _tooltip = GetText(sel);
			base::DrawTooltip(x, y);
		}
	}
}

// Server display
Control *DisplayServer::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
		case IDC_SERVER_ISLAND:
			{
				C3DListBox *lbox = new C3DListBox(this, idc, cls);
				int sel = 0;
				// int m = (Pars>>"CfgWorlds">>"worlds").GetSize();
				int m = (Pars >> "CfgWorldList").GetEntryCount();
				for (int j=0; j<m; j++)
				{
					const ParamEntry &entry = (Pars >> "CfgWorldList").GetEntry(j);
					if (!entry.IsClass()) continue;
					RString name = entry.GetName();

#if _FORCE_DEMO_ISLAND
					RString demo = Pars >> "CfgWorlds" >> "demoWorld";
					if (stricmp(name, demo) != 0) continue;
#endif

					// RString name = (Pars>>"CfgWorlds">>"worlds")[j];

					// ADDED - check if wrp file exists
					RString fullname = GetWorldName(name);
					if (!QIFStreamB::FileExist(fullname)) continue;
			
					int index = lbox->AddString
					(
						Pars >> "CfgWorlds" >> name >> "description"
					);
					lbox->SetData(index, name);
					if (stricmp(name, Glob.header.worldname) == 0)
						sel = index;
					RString textureName = Pars >> "CfgWorlds" >> name >> "icon";
					RString fullName = FindPicture(textureName);
					if (fullName.GetLength() > 0)
					fullName.Lower();
					Ref<Texture> texture = GlobLoadTexture
					(
						fullName
					);
					lbox->SetTexture(index, texture);
				}
				lbox->SetCurSel(sel);
				return lbox;
			}
		case IDC_SERVER_MISSION:
			return new CMPMissionsList(this, idc, cls);
	}
	return Display::OnCreateCtrl(type, idc, cls);
}

void DisplayServer::OnChangeDifficulty()
{
	CActiveText *ctrl = dynamic_cast<CActiveText *>(GetCtrl(IDC_SERVER_DIFF));
	if (!ctrl) return;
	RString text; 
	if (_cadetMode) text = LocalizeString(IDS_DIFF_CADET);
	else text = LocalizeString(IDS_DIFF_VETERAN);
	ctrl->SetText(text);
}

RString CreateMPMissionBank(RString filename, RString island);
void RemoveBank(const char *prefix);

bool DisplayServer::SetMission(bool editor)
{
	C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_SERVER_ISLAND));
	if (!lbox) return false;	

	CurrentCampaign = "";
	CurrentBattle = "";
	CurrentMission = "";
	Glob.config.easyMode = false;

	int index = lbox->GetCurSel();
	if (index < 0) return false;
	RString world = lbox->GetData(index);
	lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_SERVER_MISSION));
	if (!lbox) return false;	
	index = lbox->GetCurSel();
	if (index < 0)
	{
		if (editor)
		{
OnEditor:
			RString userDir = GetUserDirectory(); 
			SetBaseDirectory(userDir);
			::CreateDirectory(GetBaseDirectory() + RString("MPMissions"),NULL);
			::SetMission(world, "", MPMissionsDir);
			return true;
		}
		else
			return false;
	}
	RString mission = lbox->GetData(index);
	int value = lbox->GetValue(index);

	switch (value)
	{
	case 0:
		if (editor) goto OnEditor;
		else
		{
			SetBaseDirectory("");
			GetNetworkManager().CreateMission(mission, world);
			::SetMission(world, "__cur_mp", MPMissionsDir);
			Glob.header.filenameReal = mission;
		}
		break;
	case 1:
		if (editor) goto OnEditor;
		else
		{
			SetBaseDirectory("");
			GetNetworkManager().CreateMission("", "");
			::SetMission(world, mission, MPMissionsDir);
		}
		break;
	case 2:
		{
			RString userDir = GetUserDirectory(); 
			SetBaseDirectory(userDir);
			::CreateDirectory(GetBaseDirectory() + RString("MPMissions"),NULL);
			if (!editor) GetNetworkManager().CreateMission("", "");
			::SetMission(world, mission, MPMissionsDir);
		}
		break;
	default:
		if (editor) goto OnEditor;
		break;
	}
	return true;
}

#if _ENABLE_EDITOR

void __cdecl CDPCreateEditor(ControlsContainer *parent, bool multiplayer = false);
CDAPFN_DECLARE_GLOBAL(CDPCreateEditor, CDAPFN_OVERHEAD_L2, CDAPFN_CONSTRAINT_NONE);

void __cdecl CDPCreateEditor(ControlsContainer *parent, bool multiplayer)
{
	parent->CreateChild(new DisplayArcadeMap(parent, multiplayer));

	CDAPFN_ENDMARK(CDPCreateEditor);

/*
#if _CZECH
	PerformRandomRoxxeTest_002(CDDrive);
#endif
*/
}

#endif

void DisplayServer::OnButtonClicked(int idc)
{
	switch (idc)
	{
#if _ENABLE_EDITOR
		case IDC_SERVER_EDITOR:
			{
				if (!SetMission(true)) break;

				GetNetworkManager().SetGameState(NGSEdit);

				GLOB_WORLD->SwitchLandscape(GetWorldName(Glob.header.worldname));
				// Macrovision CD Protection
				CDPCreateEditor(this, true);
				//CreateChild(new DisplayArcadeMap(this, true));
			}
			break;
#endif // #if _ENABLE_EDITOR
		case IDC_OK:
			{
#if _ENABLE_EDITOR
				C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_SERVER_MISSION));
				if (lbox)
				{
					int index = lbox->GetCurSel();
					if (index >= 0)
					{
						int value = lbox->GetValue(index);
						if (value == -2)
						{
							if (!SetMission(true)) break;
			
							GetNetworkManager().SetGameState(NGSEdit);
							
							GLOB_WORLD->SwitchLandscape(GetWorldName(Glob.header.worldname));

							// Macrovision CD Protection
							CDPCreateEditor(this, true);
							break;
						}
						else if (value == -1)
						{
							C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_SERVER_ISLAND));
							if (!lbox) break;	

							int index = lbox->GetCurSel();
							if (index < 0) break;
							RString world = lbox->GetData(index);

							GetNetworkManager().SetGameState(NGSEdit);

							CreateChild(new DisplayWizardTemplate(this, world, true));
							break;
						}
					}
				}
#endif

				if (!SetMission(false)) break;

				// load template
				CurrentTemplate.Clear();
				if (GWorld->UI()) GWorld->UI()->Init();
				if (!ParseMission(true)) break;

				Glob.config.easyMode = _cadetMode;
				GetNetworkManager().InitMission(_cadetMode);
				GetNetworkManager().SetGameState(NGSPrepareSide);
				CreateChild(new DisplayMultiplayerSetup(this));
			}
			break;
		case IDC_SERVER_DIFF:
			_cadetMode = !_cadetMode;
			SaveParams();
			OnChangeDifficulty();
			break;
		default:
			Display::OnButtonClicked(idc);
			break;
	}
}

void DisplayServer::OnLBSelChanged(int idc, int curSel)
{
	if (idc == IDC_SERVER_ISLAND)
		UpdateMissions();
	else if (idc == IDC_SERVER_MISSION)
		OnMissionChanged(curSel);
	Display::OnComboSelChanged(idc, curSel);
}

void DisplayServer::OnLBDblClick(int idc, int curSel)
{
	if (idc == IDC_SERVER_MISSION && curSel >= 0)
		OnButtonClicked(IDC_OK);
	else
		Display::OnLBDblClick(idc, curSel);
}

void DisplayServer::OnChildDestroyed(int idd, int exit)
{
	switch (idd)
	{
		case IDD_ARCADE_MAP:
			Display::OnChildDestroyed(idd, exit);
			GetNetworkManager().SetGameState(NGSCreate);

			UpdateMissions(Glob.header.filename);
			if (exit == IDC_OK)
			{
				OnButtonClicked(IDC_OK);
			}
			break;
//		case IDD_SERVER_SETUP:
		case IDD_SERVER_SIDE:
			Display::OnChildDestroyed(idd, exit);
			GetNetworkManager().SetGameState(NGSCreate);
			break;
		case IDD_WIZARD_TEMPLATE:
			Display::OnChildDestroyed(idd, exit);
			GetNetworkManager().SetGameState(NGSCreate);
			UpdateMissions(Glob.header.filename);
			if (exit == IDC_OK)
			{
				// load template
				CurrentTemplate.Clear();
				if (GWorld->UI()) GWorld->UI()->Init();
				if (!ParseMission(true)) break;

				Glob.config.easyMode = _cadetMode;
				GetNetworkManager().InitMission(_cadetMode);
				GetNetworkManager().SetGameState(NGSPrepareSide);
				CreateChild(new DisplayMultiplayerSetup(this));
			}
			break;
		default:
			Display::OnChildDestroyed(idd, exit);
			break;
	}
}

/*!
\patch 1.34 Date 12/05/2001 by Jirka
- Added: difficulty flag in multiplayer is saved in user profile (default Cadet) 
*/
void DisplayServer::LoadParams()
{
	ParamArchiveLoad ar(GetUserParams());
	if (ar.Serialize("cadetModeMP", _cadetMode, 1, true) != LSOK)
	{
		WarningMessage("Cannot load user paremeters.");
	}
}

void DisplayServer::SaveParams()
{
	ParamArchiveSave ar(UserInfoVersion);
	ar.Parse(GetUserParams());
	if (ar.Serialize("cadetModeMP", _cadetMode, 1) != LSOK)
	{
		// TODO: save failed
	}
	if (ar.Save(GetUserParams()) != LSOK)
	{
		// TODO: save failed
	}
}

/*!
\patch 1.13 Date 08/08/2001 by Jirka
- Improved: Server display design
*/
void DisplayServer::UpdateMissions(RString filename)
{
	C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_SERVER_ISLAND));
	if (!lbox) return;	
	int index = lbox->GetCurSel();
	RString island = index < 0 ? "" : lbox->GetData(index);

	lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_SERVER_MISSION));
	if (!lbox) return;	

	RString mission = filename;
	int value = 2;
	if (mission.GetLength() == 0)
	{
		index = lbox->GetCurSel();
		if (index >= 0)
		{
			mission = lbox->GetData(index);
			value = lbox->GetValue(index);
		}
	}

	lbox->ClearStrings();
	if (island.GetLength() == 0) return;

#if _ENABLE_EDITOR
	index = lbox->AddString(LocalizeString(IDS_MPW_NEW_EDIT));
	lbox->SetValue(index, -2);
	lbox->SetFtColor(index, PackedColor(Color(0, 1, 0, 0.25)));
	lbox->SetSelColor(index, PackedColor(Color(0, 1, 0, 0.5)));

	index = lbox->AddString(LocalizeString(IDS_MPW_NEW_WIZ));
	lbox->SetValue(index, -1);
	lbox->SetFtColor(index, PackedColor(Color(0, 1, 0, 0.25)));
	lbox->SetSelColor(index, PackedColor(Color(0, 1, 0, 0.5)));
#endif

#ifdef _WIN32
	char buffer[256];
	sprintf(buffer, "MPMissions\\*.%s.pbo", (const char *)island);

	_finddata_t info;
	long h = _findfirst(buffer, &info);
	if (h != -1)
	{
		do
		{
			if ((info.attrib & _A_SUBDIR) == 0)
			{
				char name[256];
				strcpy(name, info.name);
				char *ext = strrchr(name, '.');	// extension .pbo
				*ext = 0;
				ext = strrchr(name, '.');	// world name
				*ext = 0;
				int index = lbox->AddString(name);
				lbox->SetData(index, name);
				lbox->SetValue(index, 0);	// public bank
			}
		}
		while (0==_findnext(h, &info));
		_findclose(h);
	}

	sprintf(buffer, "MPMissions\\*.%s", (const char *)island);

	h = _findfirst(buffer, &info);
	if (h != -1)
	{
		do
		{
			if 
			(
				(info.attrib & _A_SUBDIR) != 0 &&
				info.name[0] != '.'
			)
			{
				char name[256];
				strcpy(name, info.name);
				char *ext = strrchr(name, '.');
				if (ext) *ext = 0;
				int index = lbox->AddString(name);
				lbox->SetData(index, name);
				lbox->SetValue(index, 1);	// public directory
			}
		}
		while (0==_findnext(h, &info));
		_findclose(h);
	}

	sprintf
	(
		buffer, "%sMPMissions\\*.%s",
		(const char *)GetUserDirectory(),
		(const char *)island
	);

	h = _findfirst(buffer, &info);
	if (h != -1)
	{
		do
		{
			if 
			(
				(info.attrib & _A_SUBDIR) != 0 &&
				info.name[0] != '.'
			)
			{
				char name[256];
				strcpy(name, info.name);
				char *ext = strrchr(name, '.');
				if (ext) *ext = 0;
				int index = lbox->AddString(name);
				lbox->SetData(index, name);
				lbox->SetValue(index, 2);	// private directory
				lbox->SetFtColor(index, PackedColor(Color(1, 1, 0, 0.5)));
				lbox->SetSelColor(index, PackedColor(Color(1, 1, 0, 1)));
			}
		}
		while (0==_findnext(h, &info));
		_findclose(h);
	}
#else                                       // !defined _WIN32
	char buffer[2048] = "mpmissions";
	int len = strlen(buffer);
	char extension[256];
	sprintf(extension, ".%s.pbo",(const char *)island);

        DIR *dir = opendir(buffer);
	if ( dir ) {
	    struct dirent *entry;
	    buffer[len++] = '/';
	    while ( (entry = readdir(dir)) ) { // process one entry
	        strcpy(buffer+len,entry->d_name);
		struct stat st;
		if ( !stat(buffer,&st) &&
		     S_ISREG(st.st_mode) &&
		     isSuffix(entry->d_name,extension) ) {
		    char name[256];
		    int namelen = strlen(entry->d_name) - strlen(extension);
		    memcpy(name,entry->d_name,namelen);
		    name[namelen] = (char)0;
		    int index = lbox->AddString(name);
		    lbox->SetData(index, name);
		    lbox->SetValue(index, 0);	// public bank
		    }
		}
	    closedir(dir);
	    }

        buffer[--len] = (char)0;
	sprintf(extension,".%s",(const char *)island);

        dir = opendir(buffer);
	if ( dir ) {
	    struct dirent *entry;
	    buffer[len++] = '/';
	    while ( (entry = readdir(dir)) ) { // process one entry
	        strcpy(buffer+len,entry->d_name);
		struct stat st;
		if ( entry->d_name[0] != '.' &&
		     !stat(buffer,&st) &&
		     S_ISDIR(st.st_mode) &&
		     isSuffix(entry->d_name,extension) ) {
		    char name[256];
		    int namelen = strlen(entry->d_name) - strlen(extension);
		    memcpy(name,entry->d_name,namelen);
		    name[namelen] = (char)0;
		    int index = lbox->AddString(name);
		    lbox->SetData(index, name);
		    lbox->SetValue(index, 1);	// public directory
		    }
		}
	    closedir(dir);
	    }

	sprintf(buffer,"%smpmissions",(const char *)GetUserDirectory());
	len = strlen(buffer);
	sprintf(extension,".%s",(const char *)island);

        dir = opendir(buffer);
	if ( dir ) {
	    struct dirent *entry;
	    buffer[len++] = '/';
	    while ( (entry = readdir(dir)) ) { // process one entry
	        strcpy(buffer+len,entry->d_name);
		struct stat st;
		if ( entry->d_name[0] != '.' &&
		     !stat(buffer,&st) &&
		     S_ISDIR(st.st_mode) &&
		     isSuffix(entry->d_name,extension) ) {
		    char name[256];
		    int namelen = strlen(entry->d_name) - strlen(extension);
		    memcpy(name,entry->d_name,namelen);
		    name[namelen] = (char)0;
		    int index = lbox->AddString(name);
		    lbox->SetData(index, name);
		    lbox->SetValue(index, 2);	// private directory
		    lbox->SetFtColor(index, PackedColor(Color(1, 1, 0, 0.5)));
		    lbox->SetSelColor(index, PackedColor(Color(1, 1, 0, 1)));
		    }
		}
	    closedir(dir);
	    }
#endif

	lbox->SortItemsByValue();

	int sel = 0;
	for (int i=0; i<lbox->GetSize(); i++)
	{
		if
		(
			stricmp(lbox->GetData(i), mission) == 0 && lbox->GetValue(i) == value
		) sel = i;
	}
	lbox->SetCurSel(sel);

	OnMissionChanged(lbox->GetCurSel());
}

void DisplayServer::OnMissionChanged(int sel)
{
	C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_SERVER_MISSION));
	if (!lbox) return;	

/*
	RString tooltip;
	if (sel >= 0) tooltip = lbox->GetText(sel);
	lbox->SetTooltip(tooltip);
*/

#if _ENABLE_EDITOR
	if (sel >= 0)
	{
		int value = lbox->GetValue(sel);

		IControl *ctrl = GetCtrl(IDC_SERVER_EDITOR);
		if (ctrl) ctrl->ShowCtrl(value == 2);
	}
#endif
}

// Gamemaster select mission display
Control *DisplayRemoteMissions::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
		case IDC_SERVER_ISLAND:
			{
				C3DListBox *lbox = new C3DListBox(this, idc, cls);
				int sel = 0;
				// int m = (Pars>>"CfgWorlds">>"worlds").GetSize();
				int m = (Pars >> "CfgWorldList").GetEntryCount();
				for (int j=0; j<m; j++)
				{
					const ParamEntry &entry = (Pars >> "CfgWorldList").GetEntry(j);
					if (!entry.IsClass()) continue;
					RString name = entry.GetName();

					// RString name = (Pars>>"CfgWorlds">>"worlds")[j];

					// ADDED - check if wrp file exists
					RString fullname = GetWorldName(name);
					if (!QIFStreamB::FileExist(fullname)) continue;
			
					int index = lbox->AddString
					(
						Pars >> "CfgWorlds" >> name >> "description"
					);
					lbox->SetData(index, name);
					if (stricmp(name, Glob.header.worldname) == 0)
						sel = index;
					RString textureName = Pars >> "CfgWorlds" >> name >> "icon";
					RString fullName = FindPicture(textureName);
					if (fullName.GetLength() > 0)
					fullName.Lower();
					Ref<Texture> texture = GlobLoadTexture
					(
						fullName
					);
					lbox->SetTexture(index, texture);
				}
				lbox->SetCurSel(sel);
				return lbox;
			}
		case IDC_SERVER_MISSION:
			return new CMPMissionsList(this, idc, cls);
	}
	return Display::OnCreateCtrl(type, idc, cls);
}

void DisplayRemoteMissions::OnChangeDifficulty()
{
	CActiveText *ctrl = dynamic_cast<CActiveText *>(GetCtrl(IDC_SERVER_DIFF));
	if (!ctrl) return;
	RString text; 
	if (_cadetMode) text = LocalizeString(IDS_DIFF_CADET);
	else text = LocalizeString(IDS_DIFF_VETERAN);
	ctrl->SetText(text);
}

void DisplayRemoteMissions::OnButtonClicked(int idc)
{
	switch (idc)
	{
		case IDC_SERVER_DIFF:
			_cadetMode = !_cadetMode;
			OnChangeDifficulty();
			break;
		case IDC_AUTOCANCEL:
			Exit(idc);
			break;
		default:
			Display::OnButtonClicked(idc);
			break;
	}
}

void DisplayRemoteMissions::OnLBSelChanged(int idc, int curSel)
{
	if (idc == IDC_SERVER_ISLAND)
		UpdateMissions();
	else if (idc == IDC_SERVER_MISSION)
		OnMissionChanged(curSel);
	Display::OnComboSelChanged(idc, curSel);
}

void DisplayRemoteMissions::OnLBDblClick(int idc, int curSel)
{
	if (idc == IDC_SERVER_MISSION && curSel >= 0)
		OnButtonClicked(IDC_OK);
	else
		Display::OnLBDblClick(idc, curSel);
}

/*!
\patch 1.30 Date 11/01/2001 by Jirka
- Added: Multiplayer - state of mission voting is displayed
*/
void DisplayRemoteMissions::OnSimulate(EntityAI *vehicle)
{
	// update player list
	CListBox *lbox = dynamic_cast<CListBox *>(GetCtrl(IDC_SERVER_PLAYERS));
	if (lbox)
	{
		lbox->SetReadOnly();
		lbox->ShowSelected(false);
		lbox->ClearStrings();

		const AutoArray<PlayerIdentity> &identities = *GetNetworkManager().GetIdentities();
		for (int i=0; i<identities.Size(); i++)
		{
			const PlayerIdentity &identity = identities[i];
			int index = lbox->AddString(GetIdentityText(identity));
			switch (identity.state)
			{
			case NGSNone:
			case NGSCreating:
			case NGSCreate:
			case NGSLogin:
			case NGSEdit:
				lbox->SetFtColor(index, PackedColor(Color(1, 0, 0, 1)));
				lbox->SetSelColor(index, PackedColor(Color(1, 0, 0, 1)));
				lbox->SetValue(index, 0);
				break;
			case NGSMissionVoted:
			case NGSPrepareSide:
			case NGSPrepareRole:
			case NGSPrepareOK:
			case NGSDebriefing:
			case NGSDebriefingOK:
			case NGSTransferMission:
			case NGSLoadIsland:
			case NGSBriefing:
			case NGSPlay:
				lbox->SetFtColor(index, PackedColor(Color(0, 1, 0, 1)));
				lbox->SetSelColor(index, PackedColor(Color(0, 1, 0, 1)));
				lbox->SetValue(index, 2);
				break;
			default:
				break;
			}
		}
		lbox->SortItemsByValue();
	}

	if (!GetNetworkManager().CanSelectMission() && !GetNetworkManager().CanVoteMission())
	{
		OnButtonClicked(IDC_AUTOCANCEL);
		return;
	}

	if (GetNetworkManager().GetGameState() == NGSNone)
	{
		OnButtonClicked(IDC_CANCEL);
		return;
	}

	if (GetNetworkManager().GetServerState() < NGSCreate) 
	{
		OnButtonClicked(IDC_CANCEL);
		return;
	}
	else if (GetNetworkManager().GetServerState() > NGSCreate)
	{
		OnButtonClicked(IDC_AUTOCANCEL);
		return;
	}


	Display::OnSimulate(vehicle);
}

void DisplayRemoteMissions::UpdateMissions(RString filename)
{
	C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_SERVER_ISLAND));
	if (!lbox) return;	
	int index = lbox->GetCurSel();
	RString island = index < 0 ? "" : lbox->GetData(index);

	lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_SERVER_MISSION));
	if (!lbox) return;	

	RString mission = filename;
	if (mission.GetLength() == 0)
	{
		index = lbox->GetCurSel();
		if (index >= 0)
		{
			mission = lbox->GetText(index);
		}
	}

	lbox->ClearStrings();
	if (island.GetLength() == 0) return;

	const AutoArray<RString> &names = GetNetworkManager().GetServerMissions();
	for (int i=0; i<names.Size(); i++)
	{
		RString name = names[i];
		const char *beg = name;
		const char *end = strrchr(beg, '.');
		if (!end || stricmp(end + 1, island) != 0) continue;
		int index = lbox->AddString(name.Substring(0, end - beg));
		lbox->SetData(index, name);
	}

	lbox->SortItems();

	int sel = 0;
	for (int i=0; i<lbox->GetSize(); i++)
	{
		if (stricmp(lbox->GetText(i), mission) == 0) sel = i;
	}
	lbox->SetCurSel(sel);

	OnMissionChanged(lbox->GetCurSel());
}

void DisplayRemoteMissions::OnMissionChanged(int sel)
{
/*
	C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_SERVER_MISSION));
	if (!lbox) return;	

	RString tooltip;
	if (sel >= 0) tooltip = lbox->GetText(sel);
	lbox->SetTooltip(tooltip);
*/
}

// Multiplayer wizard displays
/*!
\patch 1.70 Date 6/5/2002 by Jirka
- Added: Support for templates in banks
*/

#if _ENABLE_EDITOR
DisplayWizardTemplate::DisplayWizardTemplate(ControlsContainer *parent, RString world, bool multiplayer)
	: Display(parent)
{
	_enableSimulation = false;
	_world = world;
	_multiplayer = multiplayer;
	Load("RscDisplayWizardTemplate");

	OnTemplateChanged();
}

Control *DisplayWizardTemplate::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
#ifdef _WIN32
		case IDC_WIZT_TEMPLATES:
			{
				C3DListBox *lbox = new C3DListBox(this, idc, cls);

				char buffer[256];
				_finddata_t info;

				// directories
				if (_multiplayer)
					sprintf(buffer, "Templates\\*.%s", (const char *)_world);
				else
					sprintf(buffer, "SPTemplates\\*.%s", (const char *)_world);
				long h = _findfirst(buffer, &info);
				if (h != -1)
				{
					do
					{
						if 
						(
							(info.attrib & _A_SUBDIR) != 0 &&
							info.name[0] != '.'
						)
						{
							char name[256];
							strcpy(name, info.name);
							char *ext = strrchr(name, '.');
							if (ext) *ext = 0;
							int index = lbox->AddString(name);
							lbox->SetData(index, name);
							lbox->SetValue(index, 0); // directory
						}
					}
					while (_findnext(h, &info) == 0);
					_findclose(h);
				}

				// banks
				if (_multiplayer)
					sprintf(buffer, "Templates\\*.%s.pbo", (const char *)_world);
				else
					sprintf(buffer, "SPTemplates\\*.%s.pbo", (const char *)_world);
				h = _findfirst(buffer, &info);
				if (h != -1)
				{
					do
					{
						if ((info.attrib & _A_SUBDIR) == 0)
						{
							char name[256];
							strcpy(name, info.name);
							char *ext = strrchr(name, '.');
							if (ext) *ext = 0;	// pbo
							ext = strrchr(name, '.');
							if (ext) *ext = 0;	// world
							int index = lbox->AddString(name);
							lbox->SetData(index, name);
							lbox->SetValue(index, 1); // bank
						}
					}
					while (_findnext(h, &info) == 0);
					_findclose(h);
				}

				lbox->SetCurSel(0);
				return lbox;
			}
#endif
		default:
			return Display::OnCreateCtrl(type, idc, cls);
	}
}

void DisplayWizardTemplate::OnLBSelChanged(int idc, int curSel)
{
	if (idc == IDC_WIZT_TEMPLATES)
	{
		OnTemplateChanged();
	}
	else
		Display::OnLBSelChanged(idc, curSel);
}

void DisplayWizardTemplate::OnLBDblClick(int idc, int curSel)
{
	if (idc == IDC_WIZT_TEMPLATES)
	{
		OnButtonClicked(IDC_OK);
	}
	else
		Display::OnLBDblClick(idc, curSel);
}

void DisplayWizardTemplate::OnButtonClicked(int idc)
{
	switch (idc)
	{
		case IDC_OK:
			{
				C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_WIZT_TEMPLATES));
				if (!lbox) break;

				int index = lbox->GetCurSel();
				if (index < 0) break;
				RString t = lbox->GetData(index);
				bool bank = lbox->GetValue(index) == 1;

				C3DEdit *edit = dynamic_cast<C3DEdit *>(GetCtrl(IDC_WIZT_NAME));
				if (!edit) break;

				RString name = edit->GetText();
				if (name.GetLength() == 0)
				{
					CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_MPW_NONAME));
					return;
				}
				
				// _world, t, bank, name are valid now
				CreateChild(new DisplayWizardMap(this, _world, t, bank, name, _multiplayer));
			}
			break;
		default:
			Display::OnButtonClicked(idc);
			break;
	}
}

void DisplayWizardTemplate::OnChildDestroyed(int idd, int exit)
{
	Display::OnChildDestroyed(idd, exit);
	if (idd == IDD_WIZARD_MAP && exit == IDC_OK) Exit(IDC_OK);
}

RString CreateSingleMissionBank(RString filename);

void DisplayWizardTemplate::OnTemplateChanged()
{
	C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_WIZT_TEMPLATES));
	if (!lbox) return;

	int index = lbox->GetCurSel();
	if (index < 0) return;
	RString t = lbox->GetData(index);
	bool bank = lbox->GetValue(index) == 1;

	C3DHTML *html = dynamic_cast<C3DHTML *>(GetCtrl(IDC_WIZT_OVERVIEW));
	if (!html) return;
	html->Init();

	RString dir;
	if (bank)
	{
		RString filename;
		if (_multiplayer)
			filename = RString("Templates\\") + t + RString(".") + _world;
		else
			filename = RString("SPTemplates\\") + t + RString(".") + _world;
		dir = CreateSingleMissionBank(filename);
	}
	else
	{
		if (_multiplayer)
			dir = RString("Templates\\") + t + RString(".") + _world + RString("\\");
		else
			dir = RString("SPTemplates\\") + t + RString(".") + _world + RString("\\");
	}

	RString GetOverviewFile(RString dir);
	RString filename = GetOverviewFile(dir);
	if (filename.GetLength() > 0)
		html->Load(filename, false);
}


CStaticMapWizard::CStaticMapWizard
(
	ControlsContainer *parent, int idc, const ParamEntry &cls,
	float scaleMin, float scaleMax, float scaleDefault
)
: CStaticMap(parent, idc, cls, scaleMin, scaleMax, scaleDefault)
{
}

static const char *WizVarPrefix = "WIZVAR_";
static bool HasWizVarPrefix(const char *name)
{
	return strnicmp(name, WizVarPrefix, strlen(WizVarPrefix)) == 0;
}

SignInfo CStaticMapWizard::FindSign(float x, float y)
{
	struct SignInfo info;
	info._type = signNone;
	info._unit = NULL;
	info._id = NULL;

	Point3 pt = ScreenToWorld(DrawCoord(x, y));
	const float sizeLand = LandGrid * LandRange;
	float dist, minDist = 0.02 * sizeLand * _scaleX;
	minDist *= minDist;

	// markers
	int n = _template->markers.Size();
	for (int i=0; i<n; i++)
	{
		ArcadeMarkerInfo &mInfo = _template->markers[i];
		if (!HasWizVarPrefix(mInfo.name)) continue;
		dist = (pt - mInfo.position).SquareSizeXZ();
		if (dist < minDist)
		{
			minDist = dist;
			info._type = signArcadeMarker;
			info._index = i;
		}
	}
	
	return info;
}

void CStaticMapWizard::OnLButtonDown(float x, float y)
{
	SignInfo info = FindSign(x, y);
	_infoClickCandidate = info;
	_lastPos = ScreenToWorld(DrawCoord(x, y));
}

void CStaticMapWizard::OnLButtonClick(float x, float y)
{
	switch (_infoClickCandidate._type)
	{
	case signArcadeMarker:
		_infoClick = _infoClickCandidate;
		_dragging = GInput.mouseL;
		_selecting = false;
		{
			ArcadeMarkerInfo &mInfo = _template->markers[_infoClickCandidate._index];

			if (!_dragging || !mInfo.selected)
			{
				// change selection
				if (!GInput.keys[DIK_LCONTROL] && !GInput.keys[DIK_RCONTROL])
				{
					_template->ClearSelection();
				}
				mInfo.selected = !mInfo.selected;
			}
		}
		break;		
	case signNone:
		if (GInput.mouseL)
		{
			_dragging = false;
			_selecting = true;
			_special = _lastPos;
		}
		break;
	}
}

void CStaticMapWizard::OnLButtonUp(float x, float y)
{
	if (_dragging)
	{
		_infoClick._type = signNone;
		_infoClickCandidate._type = signNone;
		_infoMove._type = signNone;
		_dragging = false;
	}
	else if (_selecting)
	{
		_selecting = false;
		if (!GInput.keys[DIK_LCONTROL] && !GInput.keys[DIK_RCONTROL])
			_template->ClearSelection();

		float xMin = floatMin(_special.X(), _lastPos.X());
		float xMax = floatMax(_special.X(), _lastPos.X());
		float zMin = floatMin(_special.Z(), _lastPos.Z());
		float zMax = floatMax(_special.Z(), _lastPos.Z());

		for (int i=0; i<_template->markers.Size(); i++)
		{
			ArcadeMarkerInfo &info = _template->markers[i];
			if (!HasWizVarPrefix(info.name)) continue;
			if
			(
				info.position.X() >= xMin && info.position.X() < xMax &&
				info.position.Z() >= zMin && info.position.Z() < zMax
			) info.selected = !info.selected;
		}
	}
}

void CStaticMapWizard::OnMouseHold(float x, float y, bool active)
{
	if (_dragging)
	{
		Vector3 curPos = ScreenToWorld(DrawCoord(x, y));

		if (GInput.keys[DIK_LSHIFT] > 0 || GInput.keys[DIK_RSHIFT] > 0)
		{
			// rotate
			Vector3 sum = VZero;
			int count = 0;
			_template->CalculateCenter(sum, count, true);
			if (count > 0)
			{
				Vector3 center = (1.0f / count) * sum;
				Vector3 oldDir = _lastPos - center;
				Vector3 dir = curPos - center;
				float angle = AngleDifference(atan2(dir.X(), dir.Z()), atan2(oldDir.X(), oldDir.Z()));
				_template->Rotate(center, angle, true);
			}
		}
		else
		{
			Vector3 offset = curPos - _lastPos;
			// move all selected items by offset
			for (int i=0; i<_template->markers.Size(); i++)
			{
				ArcadeMarkerInfo &mInfo = _template->markers[i];
				if (!HasWizVarPrefix(mInfo.name)) continue;
				if (mInfo.selected)
				{
					Vector3 pos = mInfo.position + offset;
					pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);
					_template->MarkerChangePosition(i, pos);
				}
			}
		}

		_lastPos = curPos;
	}
	else if (_selecting)
	{
		_lastPos = ScreenToWorld(DrawCoord(x, y));
	}
}

void CStaticMapWizard::DrawExt(float alpha)
{
	CStaticMap::DrawExt(alpha);

	DrawMarkers();

	DrawLabel(_infoMove, _colorInfoMove);

	if (_selecting)
	{
		PackedColor color = PackedColor(Color(0,1,0,1));
		DrawCoord pt1 = WorldToScreen(_special);
		DrawCoord pt2 = WorldToScreen(_lastPos);
		GEngine->DrawLine
		(
			Line2DPixel(pt1.x * _wScreen, pt1.y * _hScreen,
			pt2.x * _wScreen, pt1.y * _hScreen),
			color, color, _clipRect
		);
		GEngine->DrawLine
		(
			Line2DPixel(pt2.x * _wScreen, pt1.y * _hScreen,
			pt2.x * _wScreen, pt2.y * _hScreen),
			color, color, _clipRect
		);
		GEngine->DrawLine
		(
			Line2DPixel(pt2.x * _wScreen, pt2.y * _hScreen,
			pt1.x * _wScreen, pt2.y * _hScreen),
			color, color, _clipRect
		);
		GEngine->DrawLine
		(
			Line2DPixel(pt1.x * _wScreen, pt2.y * _hScreen,
			pt1.x * _wScreen, pt1.y * _hScreen),
			color, color, _clipRect
		);
	}
}

void CStaticMapWizard::DrawMarkers()
{
	int n = _template->markers.Size();
	for (int i=0; i<n; i++)
	{
		ArcadeMarkerInfo &mInfo = _template->markers[i];
		if (!HasWizVarPrefix(mInfo.name)) continue;

		PackedColor color = mInfo.color;
		if (!mInfo.selected)
		{
			color.SetA8(color.A8() / 2);
		}

		switch (mInfo.markerType)
		{
		case MTIcon:
			{
				float size = mInfo.size;
				if (size == 0) size = 32;
				if (!mInfo.icon) break;
				DrawSign
				(
					mInfo.icon, color,
					mInfo.position,
					size * mInfo.a, size * mInfo.b,
					mInfo.angle * (H_PI / 180.0),
					Localize(mInfo.text)
				);
			}
			break;
		case MTRectangle:
			FillRectangle
			(
				mInfo.position, mInfo.a, mInfo.b, mInfo.angle,
				color, mInfo.fill
			);
			break;
		case MTEllipse:
			FillEllipse
			(
				mInfo.position, mInfo.a, mInfo.b, mInfo.angle,
				color, mInfo.fill
			);
			break;
		}
	}
}

void CStaticMapWizard::DrawLabel(struct SignInfo &info, PackedColor color)
{
	RString text;
	Point3 pos;
	switch (info._type)
	{
	case signNone:
		break;
	case signArcadeMarker:
		{
			ArcadeMarkerInfo &mInfo = _template->markers[info._index];
			text = Localize(mInfo.text);
			if (text.GetLength() == 0) text = LocalizeString(IDS_CFG_MARKERS_MARKER);
			pos = mInfo.position;
			break;
		}
		break;
	}

	if (text.GetLength()>0)
	{
		DrawCoord posMap = WorldToScreen(pos);

		// place label
		float w = GLOB_ENGINE->GetTextWidth(_sizeLabel, _fontLabel, text);
		float h = _sizeLabel;
		posMap.x -= 0.5 * w;
		posMap.y -= 0.01 + h;

		MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
		GLOB_ENGINE->Draw2D
		(
			mip, _colorLabelBackground,
			Rect2DPixel(posMap.x * _wScreen, posMap.y * _hScreen,
			w * _wScreen, h * _hScreen),
			_clipRect
		);

		GLOB_ENGINE->DrawText
		(
			posMap, _sizeLabel,
			Rect2DFloat(_x, _y, _w, _h),
			_fontLabel, color, text
		);
	}
}

void CStaticMapWizard::Center()
{
	Point3 pt = _defaultCenter;
	int n = _template->markers.Size();
	for (int i=0; i<n; i++)
	{
		ArcadeMarkerInfo &mInfo = _template->markers[i];
		if (HasWizVarPrefix(mInfo.name))
		{
			pt = mInfo.position;
			break;
		}
	}

	CStaticMap::Center(pt);
}


DisplayWizardMap::DisplayWizardMap(ControlsContainer *parent, RString world, RString t, bool bank, RString name, bool multiplayer)
: Display(parent)
{
	_enableSimulation = false;
	_world = world;
	_t = t;
	_bank = bank;
	_name = name;
	_multiplayer = multiplayer;
	_map = NULL;

	_cursor = CursorArrow;

	CurrentCampaign = "";
	CurrentBattle = "";
	CurrentMission = "";

	SetBaseDirectory("");
	RString dir;
	RString mission;
	if (bank)
	{
		RString filename;
		if (_multiplayer)
			filename = RString("Templates\\") + t + RString(".") + world;
		else
			filename = RString("SPTemplates\\") + t + RString(".") + world;
		CreateSingleMissionBank(filename);
		dir = "missions\\";
		mission = "__cur_sp";
	}
	else
	{
		if (_multiplayer)
			dir = "Templates\\";
		else
			dir = "SPTemplates\\";
		mission = t;
	}

	::SetMission(world, mission, dir);

	GLOB_WORLD->SwitchLandscape(GetWorldName(world));

	RString filename = dir + mission + RString(".") + world + RString("\\mission.sqm");
	ParamArchiveLoad ar(filename);
	ATSParams params;
	ar.SetParams(&params);
	if (ar.Serialize("Mission", _mission, 1) != TMOK)
	{
		CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_LOAD_TEMPL_FAIL));
	}

	Load("RscDisplayWizardMap");

	// setting default values
	if (_map)
	{
		_map->SetScale(-1);
		_map->Center();
		_map->Reset();
	}
}

Control *DisplayWizardMap::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
		case IDC_MAP:
			_map = new CStaticMapWizard(this, idc, cls, 0.001, 0.5, 0.16);
			_map->SetTemplate(&_mission);
			return _map;
		default:
			return Display::OnCreateCtrl(type, idc, cls);
	}
}

struct SaveContext
{
	QFBank *bank;
	RString folder;
};

static void SaveFile(const FileInfoO &fi, const FileBankType *files, void *context)
{
	SaveContext *sc = (SaveContext *)context;
	// save given file
	QIFStreamB file;
	file.open(*sc->bank,fi.name);
	// note: file may be compressed - auto handled by bank

	RString outFilename=sc->folder+RString("\\")+RString(fi.name);
	// note file name may contain subfolder
	char folderName[1024];
	strcpy(folderName,outFilename);
	*GetFilenameExt(folderName) = 0;
	if (strlen(folderName)>0)
	{
		if (folderName[strlen(folderName)-1]=='\\') folderName[strlen(folderName)-1]=0;
		// FIX: create whole path (not only topmost directory)
		CreatePath(folderName);
		::CreateDirectory(folderName,NULL);
	}
	// 
	FILE *tgt = fopen(outFilename,"wb");
	if (!tgt) return;

	setvbuf(tgt,NULL,_IOFBF,256*1024);
	fwrite(file.act(),file.rest(),1,tgt);

	fclose(tgt);
}

static void ExtractBank(RString dst, RString src)
{
	QFBank bank;
	bank.open(src);

	// enumerate files in bank
	SaveContext saveContext;
	saveContext.bank = &bank;
	saveContext.folder = dst;
	bank.ForEach(SaveFile, &saveContext);
}

bool DisplayWizardMap::CreateMission()
{
	RString src;
	RString dst;
	if (_multiplayer)
	{
		src = RString("Templates\\") + _t + RString(".") + _world;
		dst = GetUserDirectory() + RString("MPMissions\\") + _name + RString(".") + _world;
	}
	else
	{
		src = RString("SPTemplates\\") + _t + RString(".") + _world;
		dst = GetUserDirectory() + RString("Missions\\") + _name + RString(".") + _world;
	}
	DeleteDirectoryStructure(dst, false);
	CreatePath(dst + RString("\\"));
	if (_bank)
		ExtractBank(dst, src);
	else
		CopyDirectoryStructure(dst, src);

	ArcadeTemplate mission;
	ArcadeTemplate intro;
	ArcadeTemplate outroWin;
	ArcadeTemplate outroLoose;
	RString filename = dst + RString("\\mission.sqm");
	if (!QIFStream::FileExists(filename))
	{
		CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_LOAD_TEMPL_FAIL));
		return false;
	}

	for (int i=0; i<_mission.markers.Size(); i++)
	{
		ArcadeMarkerInfo &marker = _mission.markers[i];
		if (HasWizVarPrefix(marker.name))
		{
			static RString suffixX("_X");
			static RString suffixY("_Y");
			static RString suffixZ("_Z");

			GGameState.VarSet(marker.name + suffixX, GameValue(marker.position[0]));
			GGameState.VarSet(marker.name + suffixY, GameValue(marker.position[1]));
			GGameState.VarSet(marker.name + suffixZ, GameValue(marker.position[2]));
		}
	}

	// load mission
	{
		ParamArchiveLoad ar(filename);
		ATSParams params;
		ar.SetParams(&params);
		if (ar.Serialize("Mission", mission, 1) != TMOK)
		{
			CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_LOAD_TEMPL_FAIL));
			return false;
		}
		if (ar.Serialize("Intro", intro, 1) != TMOK)
		{
			CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_LOAD_TEMPL_FAIL));
			return false;
		}
		if (ar.Serialize("OutroWin", outroWin, 1) != TMOK)
		{
			CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_LOAD_TEMPL_FAIL));
			return false;
		}
		if (ar.Serialize("OutroLoose", outroLoose, 1) != TMOK)
		{
			CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_LOAD_TEMPL_FAIL));
			return false;
		}
	}

	// GGameState.Reset();
	
	// remove markers
	for (int i=0; i<mission.markers.Size();)
	{
		ArcadeMarkerInfo &mDst = mission.markers[i];
		if (HasWizVarPrefix(mDst.name))
			mission.markers.Delete(i);
		else
			i++;
	}

	// save mission
	{
	#ifdef _WIN32
		chmod(filename,_S_IREAD | _S_IWRITE);
	#else
	        LocalPath(fn,filename);
		chmod(fn,S_IREAD | S_IWRITE);
	#endif
		ParamArchiveSave ar(MissionsVersion);
		ATSParams params;
		ar.SetParams(&params);
		if (ar.Serialize("Mission", mission, 1) != TMOK)
		{
			CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_SAVE_TEMPL_FAIL));
			return false;
		}
		if (ar.Serialize("Intro", intro, 1) != TMOK)
		{
			CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_SAVE_TEMPL_FAIL));
			return false;
		}
		if (ar.Serialize("OutroWin", outroWin, 1) != TMOK)
		{
			CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_SAVE_TEMPL_FAIL));
			return false;
		}
		if (ar.Serialize("OutroLoose", outroLoose, 1) != TMOK)
		{
			CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_SAVE_TEMPL_FAIL));
			return false;
		}
		LSError err = ar.Save(filename);
		if (err != LSOK)
		{
			if (err == LSAccessDenied)
				CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_TEMPL_ACCESSDENIED));
			else
				CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_SAVE_TEMPL_FAIL));
			return false;
		}
	}

	return true;
}

void DisplayWizardMap::OnButtonClicked(int idc)
{
	switch (idc)
	{
		case IDC_OK:
			if (CreateMission())
			{
				// set mission
				RString userDir = GetUserDirectory(); 
				SetBaseDirectory(userDir);
				if (_multiplayer)
				{
					GetNetworkManager().CreateMission("", "");
					::SetMission(_world, _name, MPMissionsDir);
				}
				else
				{
					::SetMission(_world, _name, MissionsDir);
				}

				Exit(IDC_OK);
			}
			else
			{
				Exit(IDC_CANCEL);
			}
			break;
		case IDC_WIZM_EDIT:
			if (CreateMission())
			{
				// set mission
				RString userDir = GetUserDirectory(); 
				SetBaseDirectory(userDir);
				if (_multiplayer)
				{
					::SetMission(_world, _name, MPMissionsDir);
				}
				else
				{
					::SetMission(_world, _name, MissionsDir);
				}

				CDPCreateEditor(this, _multiplayer);
			}
			break;
		default:
			Display::OnButtonClicked(idc);
			break;
	}
}

void DisplayWizardMap::OnSimulate(EntityAI *vehicle)
{
	Display::OnSimulate(vehicle);
	if (_map && _map->IsVisible())
	{
		_map->ProcessCheats();

		float mouseX = 0.5 + GInput.cursorX * 0.5;
		float mouseY = 0.5 + GInput.cursorY * 0.5;

		// automatic map movement on edges
		float dif = 0.02 - mouseX;
		if (dif > 0) _map->ScrollX(0.003 * exp(dif * 100.0f));
		else
		{
			dif = mouseX - 0.98;
			if (dif > 0) _map->ScrollX(-0.003 * exp(dif * 100.0f));
		}

		dif = 0.02 - mouseY;
		if (dif > 0) _map->ScrollY(0.003 * exp(dif * 100.0f));
		else
		{
			dif = mouseY - 0.98;
			if (dif > 0) _map->ScrollY(-0.003 * exp(dif * 100.0f));
		}

		IControl *ctrl = GetCtrl(mouseX, mouseY);
		if (ctrl && ctrl == _map)
		{
			if (_map->_dragging || _map->_selecting)
			{
				if (_cursor != CursorMove)
				{
					_cursor = CursorMove;
					SetCursor("Move");
				}
			}
			else if (_map->_moving)
			{
				if (_cursor != CursorScroll)
				{
					_cursor = CursorScroll;
					SetCursor("Scroll");
				}
			}
			else
			{
				if (_cursor != CursorTrack)
				{
					_cursor = CursorTrack;
					SetCursor("Track");
				}
			}
		}
		else
		{
			if (_cursor != CursorArrow)
			{
				_cursor = CursorArrow;
				SetCursor("Arrow");
			}
		}
	}
}

void DisplayWizardMap::OnChildDestroyed(int idd, int exit)
{
	switch (idd)
	{
		case IDD_ARCADE_MAP:
			Display::OnChildDestroyed(idd, exit);
//			if (exit == IDC_OK)
			{
				RString userDir = GetUserDirectory(); 
				SetBaseDirectory(userDir);
				if (_multiplayer)
				{
					GetNetworkManager().CreateMission("", "");
					::SetMission(_world, _name, MPMissionsDir);
				}
				else
				{
					::SetMission(_world, _name, MissionsDir);
				}

				OnButtonClicked(IDC_OK);
			}
			break;
		default:
			Display::OnChildDestroyed(idd, exit);
			break;
	}
}
#endif // _ENABLE_EDITOR

// Client display
void DisplayClient::OnButtonClicked(int idc)
{
	switch (idc)
	{
		case IDC_OK:
			{
				// init game info
				SetBaseDirectory("");
				CurrentCampaign = "";
				CurrentBattle = "";
				CurrentMission = "";
				Glob.config.easyMode = false;

				CurrentTemplate.Clear();
				if (GWorld->UI()) GWorld->UI()->Init();

				CreateChild(new DisplayMultiplayerSetup(this));
			}
			break;
		case IDC_AUTOCANCEL:
			Exit(idc);
			break;
		default:
			Display::OnButtonClicked(idc);
			break;
	}
}

void DisplayClient::OnChildDestroyed(int idd, int exit)
{
	switch (idd)
	{
		case IDD_MP_SETUP:
			Display::OnChildDestroyed(idd, exit);
			if (exit == IDC_CANCEL) Exit(IDC_CANCEL);  
			break;
//		case IDD_CLIENT_SETUP:
		case IDD_CLIENT_SIDE:
			Display::OnChildDestroyed(idd, exit);
			if (exit == IDC_CANCEL) Exit(IDC_CANCEL);  
			break;
		case IDD_SERVER:
			// remote missions display
			if (exit == IDC_OK)
			{
				RString mission;
				C3DListBox *lbox = dynamic_cast<C3DListBox *>(_child->GetCtrl(IDC_SERVER_MISSION));
				if (lbox)
				{
					int sel = lbox->GetCurSel();
					if (sel >= 0) mission = lbox->GetData(sel);
				}

				bool cadetMode = false;
				DisplayRemoteMissions *disp = dynamic_cast<DisplayRemoteMissions *>((ControlsContainer *)_child);
				if (disp) cadetMode = disp->_cadetMode;

				Display::OnChildDestroyed(idd, exit);
				
				if (GetNetworkManager().CanSelectMission())
				{
					GetNetworkManager().SelectMission(mission, cadetMode);
					GetNetworkManager().ClientReady(NGSMissionVoted);
				}
				else if (GetNetworkManager().CanVoteMission())
				{
					GetNetworkManager().VoteMission(mission, cadetMode);
					GetNetworkManager().ClientReady(NGSMissionVoted);
				}
			}
			else
			{
				Display::OnChildDestroyed(idd, exit);
				if (exit == IDC_CANCEL) Exit(IDC_CANCEL);	// do not end session when autocancel
			}
			break;
		default:
			Display::OnChildDestroyed(idd, exit);
			break;
	}
}

/*!
\patch 1.55 Date 5/7/2002 by Jirka
- Fixed: MP - when mission was voted repeatedly, all players was green (even when didn't vote yet)
*/
void DisplayClient::OnSimulate(EntityAI *vehicle)
{
	NetworkGameState gameState = GetNetworkManager().GetServerState();

	switch (gameState)
	{
	case NGSNone:
		OnButtonClicked(IDC_AUTOCANCEL);
		return;
	case NGSCreating:
	case NGSCreate:
	case NGSLogin:
		// continue with waiting
		if (GetNetworkManager().CanSelectMission())
		{
			CreateChild(new DisplayRemoteMissions(this));
			GetNetworkManager().ClientReady(NGSLogin);
		}
		else if (GetNetworkManager().CanVoteMission())
		{
			CreateChild(new DisplayRemoteMissions(this));
			GetNetworkManager().ClientReady(NGSLogin);
		}
		{
			CStatic *text = dynamic_cast<CStatic *>(GetCtrl(IDC_CLIENT_TEXT));
			if (text) text->SetText(LocalizeString(IDS_DISP_CLIENT_TEXT));
		}
		break;
	case NGSEdit:
		// continue with waiting
		{
			CStatic *text = dynamic_cast<CStatic *>(GetCtrl(IDC_CLIENT_TEXT));
			if (text) text->SetText(LocalizeString(IDS_DISP_CLIENT_TEXT_EDIT));
		}
		break;
	case NGSPrepareSide:
	case NGSPrepareRole:
	case NGSPrepareOK:
	case NGSDebriefing:
	case NGSTransferMission:
	case NGSLoadIsland:
	case NGSBriefing:
	case NGSPlay:
		OnButtonClicked(IDC_OK);
		break;
	}

	// update player list
	CListBox *lbox = dynamic_cast<CListBox *>(GetCtrl(IDC_CLIENT_PLAYERS));
	if (lbox)
	{
		lbox->SetReadOnly();
		lbox->ShowSelected(false);
		lbox->ClearStrings();

		const AutoArray<PlayerIdentity> &identities = *GetNetworkManager().GetIdentities();
		for (int i=0; i<identities.Size(); i++)
		{
			const PlayerIdentity &identity = identities[i];
			int index = lbox->AddString(GetIdentityText(identity));
			switch (identity.state)
			{
			case NGSNone:
			case NGSCreating:
			case NGSCreate:
			case NGSLogin:
			case NGSEdit:
				lbox->SetFtColor(index, PackedColor(Color(1, 0, 0, 1)));
				lbox->SetSelColor(index, PackedColor(Color(1, 0, 0, 1)));
				lbox->SetValue(index, 0);
				break;
			case NGSMissionVoted:
			case NGSPrepareSide:
			case NGSPrepareRole:
			case NGSPrepareOK:
			case NGSDebriefing:
			case NGSDebriefingOK:
			case NGSTransferMission:
			case NGSLoadIsland:
			case NGSBriefing:
			case NGSPlay:
				lbox->SetFtColor(index, PackedColor(Color(0, 1, 0, 1)));
				lbox->SetSelColor(index, PackedColor(Color(0, 1, 0, 1)));
				lbox->SetValue(index, 2);
				break;
			default:
				break;
			}
		}
		lbox->SortItemsByValue();
	}

	Display::OnSimulate(vehicle);
}

static bool SideExist(TargetSide side)
{
	for (int i=0; i<GetNetworkManager().NPlayerRoles(); i++)
	{
		if (GetNetworkManager().GetPlayerRole(i)->side == side) return true;
	}
	return false;
}

static NetworkGameState GetPlayerState(int player, TargetSide *side = NULL, bool *locked = NULL)
{
	if (side) *side = TSideUnknown;
	if (locked) *locked = false;

	for (int i=0; i<GetNetworkManager().NPlayerRoles(); i++)
	{
		const PlayerRole *role = GetNetworkManager().GetPlayerRole(i);
		if (role->player == player)
		{
			if (side) *side = role->side;
			if (locked) *locked = role->roleLocked;
			
			const PlayerIdentity *identity = GetNetworkManager().FindIdentity(player);
			if (identity && identity->state >= NGSPrepareOK) return NGSPrepareOK;
			else return NGSPrepareRole;
		}
	}
	return NGSPrepareSide;
}

static RString GetGroupDescription(int group)
{
	// group
	int colors = (Pars >> "CfgWorlds" >> "GroupColors").GetEntryCount();
	int name = group / colors;
	int color = group % colors;
	const ParamEntry &clsName = (Pars >> "CfgWorlds" >> "GroupNames").GetEntry(name);
	const ParamEntry &clsColor = (Pars >> "CfgWorlds" >> "GroupColors").GetEntry(color);
	RString descName = clsName >> "name";
	RString descColor = clsColor >> "name";

	BString<512> buffer;
	sprintf(buffer, LocalizeString(IDS_MPSETUP_GRP_DESC), (const char *)descName, (const char *)descColor);
	return (const char *)buffer;
}

static RString GetRoleDescription(const PlayerRole *role)
{
	// vehicle
	const ParamEntry &clsVehicle = Pars >> "CfgVehicles" >> role->vehicle;
	RString vehicle = clsVehicle >> "displayName";
	RString position;
	switch (role->position)
	{
	case PRPCommander:
		position = RString(" ") + LocalizeString(IDS_POSITION_COMMANDER); break;
	case PRPDriver:
		{
			const ParamEntry &clsAir = Pars >> "CfgVehicles" >> "Air";
			Assert(clsVehicle.IsClass());
			Assert(clsAir.IsClass());
			if
			(
				static_cast<const ParamClass &>(clsVehicle).IsDerivedFrom
				(static_cast<const ParamClass &>(clsAir))
			)
				position = RString(" ") + LocalizeString(IDS_POSITION_PILOT);
			else	
				position = RString(" ") + LocalizeString(IDS_POSITION_DRIVER);
			break;
		}
	case PRPGunner:
		position = RString(" ") + LocalizeString(IDS_POSITION_GUNNER); break;
	}
	
	// group leader
	RString leader;
	if (role->leader) leader = LocalizeString(IDS_POSITION_LEADER);

	BString<512> buffer;
	sprintf
	(
		buffer, "%d: %s%s%s",
		role->unit + 1,
		(const char *)vehicle,
		(const char *)position,
		(const char *)leader
	);
	return (const char *)buffer;
}

class CMPSideButton : public C3DActiveText
{
protected:
	bool _toggled;
	PackedColor _colorShade;
	PackedColor _colorDisabled;
	float _textureWidth;
	float _textureHeight;
	float _textHeight;
	TargetSide _side;
	Ref<Texture> _sideDisabled;

public:
	CMPSideButton(ControlsContainer *parent, int idc, const ParamEntry &cls, TargetSide side);

	bool IsToggled() const {return _toggled;}
	void Toggle(bool set) {_toggled = set;}

	virtual void OnDraw(float alpha);
};

CMPSideButton::CMPSideButton(ControlsContainer *parent, int idc, const ParamEntry &cls, TargetSide side)
	: C3DActiveText(parent, idc, cls)
{
	_side = side;
	_toggled = false;

	_colorShade = GetPackedColor(cls >> "colorShade");
	_colorDisabled = GetPackedColor(cls >> "colorDisabled");
	
	RString path = FindPicture(cls >> "picture");
	path.Lower();
	_texture = GlobLoadTexture(path);
	if (_texture) _texture->SetMaxSize(1024); // no limits

	_textureWidth = cls >> "pictureWidth";
	_textureHeight = cls >> "pictureHeight";
	_textHeight = cls >> "textHeight";

	_sideDisabled = GlobLoadTexture("misc\\side_krizek.paa");
}

void CMPSideButton::OnDraw(float alpha)
{
	PackedColor baseColor;
	if (!_enabled) baseColor = _colorDisabled;
	else if (_active) baseColor = _colorActive;
	else baseColor = _color;
/*	
	else if (_toggled) baseColor = _color;
	else baseColor = _colorDisabled;
*/

	PackedColor color = ModAlpha(baseColor, alpha);
	PackedColor colorShade = ModAlpha(_colorShade, alpha);
	
	// exception - right line
	PackedColor baseColorRight(0, _colorShade.A8(), 0, 255);
	PackedColor colorRight = ModAlpha(baseColorRight, alpha);

	Vector3 normal = _down.CrossProduct(_right).Normalized(); 
	Vector3 position = _position - 0.002 * normal;

	// shade
	if (_toggled) GEngine->Draw3D
	(
		_position, _down, _right, ClipAll, colorShade, DisableSun, NULL
	);

	// frame
	if (_toggled)
	{
		GEngine->DrawLine3D(position, position + _right, color, DisableSun);
		GEngine->DrawLine3D(position + _right, position + _right + _down, colorRight, DisableSun);
		GEngine->DrawLine3D(position + _right + _down, position + _down, color, DisableSun);
		GEngine->DrawLine3D(position + _down, position, color, DisableSun);
	}

	// picture
	Vector3 posPicture = position + 0.5 * (1.0 - _textureWidth) * _right;
	Vector3 downPicture = _textureHeight * _down;
	Vector3 rightPicture = _textureWidth * _right;
	if (_texture)
		GEngine->Draw3D(posPicture, downPicture, rightPicture, ClipAll, color, DisableSun, _texture);

	Vector3 up = -_textHeight * _down;
	position = _position - 0.003 * normal + (1.0 - _textHeight) * _down;

	Vector3 right = 0.75 * up.Size() * _right.Normalized();

	Vector3 offset = VZero;
	Vector3 width = GEngine->GetText3DWidth
	(
		right, _font, _text
	);
	switch (_style & ST_HPOS)
	{
		case ST_RIGHT:
			offset = _right - width;
			break;
		case ST_CENTER:
			offset = 0.5 * (_right - width);
			break;
		default:
			Assert((_style & ST_HPOS) == ST_LEFT)
			break;
	}
	position += offset;

	float invRSize = 1.0 / right.Size();
	float x1c = 0, x2c = _right.Size() * invRSize;
	bool clip = width.SquareSize() > _right.SquareSize();
	if (clip)
	{
		float offsetSize = offset.Size() * invRSize;
		x1c += offsetSize;
		x2c += offsetSize;
	}

	/*			
	bool focused = IsFocused();
	bool selected = IsDefault() && !_parent->GetFocused()->CanBeDefault();
	if (focused || selected)
	{
		PackedColor col;
		if (focused) col = color;
		else col = ModAlpha(color, 0.5);

		if (clip)
			GEngine->DrawLine3D
			(
				position - offset + _down, position - offset + _down + _right,
				col, DisableSun
			);
		else
			GEngine->DrawLine3D
			(
				position + _down, position + _down + width,
				col, DisableSun
			);
	}
	*/

	PackedColor colorText(Color(1, 1, 0, 1));
	colorText.SetA8(color.A8());

	// text
	GEngine->DrawText3D
	(
		position, up, right, ClipAll, _font, colorText, DisableSun, _text,
		x1c, 0, x2c, 1 
	);

	// number of assigned / all roles
	int all = 0, assigned = 0;
	for (int i=0; i<GetNetworkManager().NPlayerRoles(); i++)
	{
		const PlayerRole *role = GetNetworkManager().GetPlayerRole(i);
		if (role && role->side == _side)
		{
			if (role->player != NO_PLAYER)
			{
				all++;
				if (role->player != AI_PLAYER) assigned++;
			}
		}
	}
	if (all > 0)
	{
		RString text = Format("%d/%d", assigned, all);
		width = GEngine->GetText3DWidth(right, _font, text);
		position = _position - 0.003 * normal + 0.5 * (_right - width);
		GEngine->DrawText3D
		(
			position, up, right,
			ClipAll, _font, colorText, DisableSun, text
		);
	}

	if (_texture && !_enabled)
	{
		// PackedColor white = ModAlpha(PackedWhite, alpha);
		GEngine->Draw3D(posPicture, downPicture, rightPicture, ClipAll, color, DisableSun, _sideDisabled);
	}
}

class CMPRoles : public C3DListBox
{
	typedef C3DListBox base;
protected:
	PackedColor _bgColor;

	Ref<Texture> _enableAI;
	Ref<Texture> _disableAI;
	
public:
	CMPRoles(ControlsContainer *parent, int idc, const ParamEntry &cls);

	virtual void OnLButtonUp(float x, float y);

	virtual void DrawItem(Vector3Par position, Vector3Par down, int i, float alpha);
	virtual void DrawTooltip(float x, float y);
};

CMPRoles::CMPRoles(ControlsContainer *parent, int idc, const ParamEntry &cls)
	: C3DListBox(parent, idc, cls)
{
	_sb3DWidth = 0.05;
	_bgColor = GetPackedColor(cls >> "colorBackground");

	_enableAI = GlobLoadTexture("misc\\compx.paa");
	_disableAI = GlobLoadTexture("misc\\compai.paa");
}

static const float col1coef = 0.87;

void CMPRoles::OnLButtonUp(float x, float y)
{
	if (_scrollbar.IsLocked())
	{
		_scrollbar.OnLButtonUp();
	}
	else if (!IsReadOnly() && IsInside(x, y))
	{
		if (_scrollbar.IsEnabled() && _u > (1.0 - _sb3DWidth))
		{
		}
		else
		{
			float index = _v * _rows;
			if (index >= 0 && index < _rows)
			{
				int sel = toIntFloor(_topString + index);

				float column1 = (1.0 - _sb3DWidth) * col1coef;
				if (_u <= column1)
				{
					// column1
					SetCurSel(sel);
				}
				else
				{
					// column2
					// enable / disable AI
#if _ENABLE_AI
					if (!GetNetworkManager().GetMissionHeader()->disabledAI)
					{
						int value = GetValue(sel);
						const PlayerRole *role = GetNetworkManager().GetPlayerRole(value);
						if (role)
						{
							if (role->player == AI_PLAYER)
							{
								GetNetworkManager().AssignPlayer(value, NO_PLAYER);
							}
							else if (role->player == NO_PLAYER)
							{
								GetNetworkManager().AssignPlayer(value, AI_PLAYER);
							}
						}
					}
#endif
				}
			}
		}
	}

	if (_dragging)
	{
		if (_parent) _parent->OnLBDrop(x, y);
		_dragging = false;
	}
}

void CMPRoles::DrawTooltip(float x, float y)
{
	if (_scrollbar.IsLocked()) return;
	if (!IsReadOnly() && IsInside(x, y))
	{
		if (_scrollbar.IsEnabled() && _u > (1.0 - _sb3DWidth)) return;

		float index = _v * _rows;
		if (index >= 0 && index < _rows)
		{
#if _ENABLE_AI
			if (GetNetworkManager().GetMissionHeader()->disabledAI) return;
			int sel = toIntFloor(_topString + index);
			if (sel < 0 || sel >= GetSize()) return;

			float column1 = (1.0 - _sb3DWidth) * col1coef;
			if (_u <= column1) return;

			// enable / disable AI
			int value = GetValue(sel);
			const PlayerRole *role = GetNetworkManager().GetPlayerRole(value);
			if (role)
			{
				if (role->player == AI_PLAYER)
				{
					_tooltip = LocalizeString(IDS_TOOLTIP_DISABLE_AI);
					base::DrawTooltip(x, y);
				}
				else if (role->player == NO_PLAYER)
				{
					_tooltip = LocalizeString(IDS_TOOLTIP_ENABLE_AI);
					base::DrawTooltip(x, y);
				}
			}
#endif
		}
	}
}

void CMPRoles::DrawItem(Vector3Par position, Vector3Par down, int i, float alpha)
{
	float y1c = 0;
	float y2c = 1;
	if (i < _topString) y1c = _topString - i;
	if (i > _topString + _rows - 1) y2c = _topString + _rows - i;
	
	Vector3 normal = _down.CrossProduct(_right).Normalized(); 

/*
	Vector3 rightSB = _right;
	if (GetSize() > _rows)
		rightSB = (1.0 - _sb3DWidth) * _right;
	float rightSBSize = rightSB.Size();
	Vector3 border = 0.02 * _right;
*/
	Vector3 column1 = (1.0 - _sb3DWidth) * _right;
	Vector3 column2 = VZero;
	Vector3 border = 0.02 * column1;

	int value = GetValue(i);
	const PlayerRole *role = value >= 0 ? GetNetworkManager().GetPlayerRole(value) : NULL;

	Vector3 pos = position;

	// draw selection
	PackedColor baseColor = GetFtColor(i);
	if (value >= 0)
	{
		column2 = (1.0 - col1coef) * column1;
		column1 *= col1coef;

		const float indent = 0.05;
		pos += indent * column1;
		column1 *= (1.0 - indent);

		Vector3 curPos = pos - 0.002 * normal;
		PackedColor baseBgColor = _bgColor;
		bool selected = i == GetCurSel() && IsEnabled();
		if (selected && _showSelected)
		{
			baseBgColor = _selBgColor;
			baseColor = GetSelColor(i);
		}
		PackedColor bgColor = ModAlpha(baseBgColor, alpha);
		const float coef = 0.9;
		const float invCoef = 1.0 / coef;
		float topClip = y1c * invCoef; saturate(topClip, 0, 1);
		float bottomClip = y2c * invCoef; saturate(bottomClip, 0, 1);
		GEngine->Draw3D
		(
			curPos, coef * down, column1, ClipAll, bgColor, DisableSun, NULL,
			0, topClip, 1, bottomClip
		);
	}
	PackedColor color = ModAlpha(baseColor, alpha);
	float column1Size = column1.Size();
	// float column2Size = column2.Size();

	// draw picture
	Vector3 curPos = pos - 0.003 * normal;
	Texture *texture = GetTexture(i);
	if (texture)
	{
		Vector3 right = (float)texture->AWidth() / (float)texture->AHeight() * down.Size() * _right.Normalized();
		float rightSize = right.Size();
		float x2c = 1;
		if (rightSize > column1Size) x2c = column1Size / rightSize;
		GEngine->Draw3D
		(
			curPos, down, right, ClipAll, color,
			DisableSun, texture,
			0, y1c, x2c, y2c
		);
		curPos += right;
		column1Size -= rightSize;
		if (column1Size <= 0) return;
	}

	// draw text
	RString text = GetText(i);

	if (value < 0)
	{
		const float size = 0.5;
		float top = 0.4;
		float y1ct = 0;
		float y2ct = 1;
		if (y1c > top)
			y1ct = (y1c - top) / size;
		if (y2c < top + size)
			y2ct = (y2c - top) / size;

		Vector3 posText = curPos + top * down + border;
		Vector3 up = -size * down;
		Vector3 right = 0.75 * up.Size() * _right.Normalized();
		float x2c = (column1Size - 2.0 * border.Size()) / right.Size();
		GEngine->DrawText3D
		(
			posText, up, right, ClipAll, _font, color, DisableSun, text,
			0, y1ct, x2c, y2ct
		);
	}
	else
	{
		// first row
		const float size = 0.4;
		float top = 0;
		float y1ct = 0;
		float y2ct = 1;
		if (y1c > top)
			y1ct = (y1c - top) / size;
		if (y2c < top + size)
			y2ct = (y2c - top) / size;

		Vector3 posText = curPos + top * down + border;
		Vector3 up = -size * down;
		Vector3 right = 0.75 * up.Size() * _right.Normalized();
		float x2c = (column1Size - 2.0 * border.Size()) / right.Size();
		GEngine->DrawText3D
		(
			posText, up, right, ClipAll, _font, color, DisableSun, text,
			0, y1ct, x2c, y2ct
		);

		// second row
		if (role)
		{
			// player name
			float textAlpha = (color.A8() / 255.0) * alpha;
			PackedColor color = PackedColor(Color(0, 1, 0, textAlpha));
			RString player = LocalizeString(IDS_PLAYER_AI);
			int dpid = role->player;
			if (dpid == AI_PLAYER)
			{
			}
			else if (dpid == NO_PLAYER)
			{
				player = LocalizeString(IDS_PLAYER_NONE);
				color = PackedColor(Color(1, 1, 1, textAlpha));
			}
			else
			{
				const PlayerIdentity *identity = GetNetworkManager().FindIdentity(dpid);
				// if (identity) player = GetIdentityText(*identity);
				if (identity)
				{
					player = identity->GetName();
					color = PackedColor(Color(1, 1, 0, textAlpha));
				}
			}

			const float size = 0.5;
			float top = 0.4;
			float y1ct = 0;
			float y2ct = 1;
			if (y1c > top)
				y1ct = (y1c - top) / size;
			if (y2c < top + size)
				y2ct = (y2c - top) / size;

			Vector3 posText = curPos + top * down + border;
			Vector3 up = -size * down;
			Vector3 right = 0.75 * up.Size() * _right.Normalized();
			float x2c = (column1Size - 2.0 * border.Size()) / right.Size();
			GEngine->DrawText3D
			(
				posText, up, right, ClipAll, _font, color, DisableSun, player,
				0, y1ct, x2c, y2ct
			);
		}

		// second column
#if _ENABLE_AI
		if
		(
			!GetNetworkManager().GetMissionHeader()->disabledAI &&
			role && (role->player == AI_PLAYER || role->player == NO_PLAYER)
		)
		{
			Texture *texture;
			if (role->player == AI_PLAYER)
				texture = _disableAI;
			else
				texture = _enableAI;

			if (texture)
			{
				const float coef = 0.9;
				const float invCoef = 1.0 / coef;
				float topClip = y1c * invCoef; saturate(topClip, 0, 1);
				float bottomClip = y2c * invCoef; saturate(bottomClip, 0, 1);
				GEngine->Draw3D
				(
					curPos + column1, coef * down, column2, ClipAll, color, DisableSun, texture,
					0, topClip, 1, bottomClip
				);
			}
		}
#endif
	}
}

DisplayMultiplayerSetup::DisplayMultiplayerSetup(ControlsContainer *parent)
: Display(parent)
{
	_enableSimulation = false;
	_player = NO_PLAYER;

	_transferMission = true;
	_loadIsland = true;
	_play = false;

	_sessionLocked = false;

	_allDisabled = false;
	
	_side = TSideUnknown;

	_dragging = false;
	_dragFont = GEngine->LoadFont(GetFontID("courierNewB64"));
	_dragSize = 0.024;
	_dragColor = PackedColor(Color(1, 1, 1, 0.75));

	const ParamEntry &fontPars = Pars>>"CfgInGameUI">>"ProgressFont";
	_messageFont = GEngine->LoadFont(GetFontID(fontPars>>"font"));
	_messageSize = 0.75 * _messageFont->Height();

	_none = GlobLoadTexture("data\\clear_empty.paa");
	_westUnlocked = GlobLoadTexture("misc\\usflag_normal.paa");
	_westLocked = GlobLoadTexture("misc\\usflag_locked.paa");
	_eastUnlocked = GlobLoadTexture("misc\\rusflag_normal.paa");
	_eastLocked = GlobLoadTexture("misc\\rusflag_locked.paa");
	_guerUnlocked = GlobLoadTexture("misc\\fiaflag_normal.paa");
	_guerLocked = GlobLoadTexture("misc\\fiaflag_locked.paa");
	_civlUnlocked = GlobLoadTexture("misc\\civflag_normal.paa");
	_civlLocked = GlobLoadTexture("misc\\civflag_locked.paa");

	Load("RscDisplayMultiplayerSetup");

	Preinit();
	_init = false;
}

Control *DisplayMultiplayerSetup::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
	case IDC_MPSETUP_WEST:
		return new CMPSideButton(this, idc, cls, TWest);
	case IDC_MPSETUP_EAST:
		return new CMPSideButton(this, idc, cls, TEast);
	case IDC_MPSETUP_GUERRILA:
		return new CMPSideButton(this, idc, cls, TGuerrila);
	case IDC_MPSETUP_CIVILIAN:
		return new CMPSideButton(this, idc, cls, TCivilian);
	case IDC_MPSETUP_ROLES:
		return new CMPRoles(this, idc, cls);
	default:
		return Display::OnCreateCtrl(type, idc, cls);
	}
}

void DisplayMultiplayerSetup::OnButtonClicked(int idc)
{
	switch (idc)
	{
	case IDC_MPSETUP_WEST:
		_side = TWest;
		break;
	case IDC_MPSETUP_EAST:
		_side = TEast;
		break;
	case IDC_MPSETUP_GUERRILA:
		_side = TGuerrila;
		break;
	case IDC_MPSETUP_CIVILIAN:
		_side = TCivilian;
		break;
	case IDC_OK:
		if (GetNetworkManager().IsServer())
		{
			// test if all users are assigned
			const AutoArray<PlayerIdentity> *identities = GetNetworkManager().GetIdentities();
			if (!identities) break;

			int n = 0;
			bool foundPlayer = false;
			int player = GetNetworkManager().GetPlayer();
			for (int i=0; i<GetNetworkManager().NPlayerRoles(); i++)
			{
				int dpid = GetNetworkManager().GetPlayerRole(i)->player;
				if (GetNetworkManager().FindIdentity(dpid)) n++;
				if (dpid == player) foundPlayer = true;
			}
			
			if (!foundPlayer)
			{
				CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_MP_ASSIGN_PLAYERS));
				break;
			}

			if (n < GetNetworkManager().NPlayerRoles() && n < identities->Size())
			{
				CreateMsgBox
				(
					MB_BUTTON_OK | MB_BUTTON_CANCEL,
					LocalizeString(IDS_MSG_LAUNCH_GAME),
					IDD_MSG_LAUNCHGAME
				);
				break;
			}

			// create game
			GetNetworkManager().SetGameState(NGSTransferMission);
		}
		else
		{
			if (GetNetworkManager().IsGameMaster())
			{
				const AutoArray<PlayerIdentity> *identities = GetNetworkManager().GetIdentities();
				if (!identities) break;
				int n = 0;
				for (int i=0; i<GetNetworkManager().NPlayerRoles(); i++)
				{
					int dpid = GetNetworkManager().GetPlayerRole(i)->player;
					if (GetNetworkManager().FindIdentity(dpid)) n++;
				}
				if (n < GetNetworkManager().NPlayerRoles() && n < identities->Size())
				{
					CreateMsgBox
					(
						MB_BUTTON_OK | MB_BUTTON_CANCEL,
						LocalizeString(IDS_MSG_LAUNCH_GAME),
						IDD_MSG_LAUNCHGAME
					);
					break;
				}
			}
			GetNetworkManager().ClientReady(NGSPrepareOK);
		}
		break;
	case IDC_CANCEL:
		if (GetNetworkManager().IsServer())
		{
			// unassign all roles
			for (int i=0; i<GetNetworkManager().NPlayerRoles(); i++)
			{
				const PlayerRole *role = GetNetworkManager().GetPlayerRole(i);
				if (role->player != AI_PLAYER) 
					GetNetworkManager().AssignPlayer(i, AI_PLAYER);
			}
			GetNetworkManager().SetGameState(NGSCreate);
		}
		Exit(IDC_CANCEL);
		break;
	case IDC_AUTOCANCEL:
		Exit(idc);
		break;
	case IDC_MPSETUP_KICK:
		if (GetNetworkManager().IsServer())
		{
			if (_player != GetNetworkManager().GetPlayer())
				GetNetworkManager().KickOff(_player,KORKick);
		}
		else if (GetNetworkManager().IsGameMaster())
		{
			if (_player != GetNetworkManager().GetPlayer())
				GetNetworkManager().SendKick(_player);
		}
		break;
	case IDC_MPSETUP_ENABLE_ALL:
		{
#if _ENABLE_AI
			if (!GetNetworkManager().IsServer() && !GetNetworkManager().IsGameMaster()) break;
			if (GetNetworkManager().GetMissionHeader()->disabledAI) break;

			bool allDisabled = true;
			for (int i=0; i<GetNetworkManager().NPlayerRoles(); i++)
			{
				const PlayerRole *role = GetNetworkManager().GetPlayerRole(i);
				if (role->player == AI_PLAYER)
				{
					allDisabled = false;
					break;
				}
			}
			for (int i=0; i<GetNetworkManager().NPlayerRoles(); i++)
			{
				const PlayerRole *role = GetNetworkManager().GetPlayerRole(i);
				if (allDisabled)
				{
					if (role->player == NO_PLAYER)
						GetNetworkManager().AssignPlayer(i, AI_PLAYER);
				}
				else
				{
					if (role->player == AI_PLAYER)
						GetNetworkManager().AssignPlayer(i, NO_PLAYER);
				}
			}
#endif
		}
		break;
	case IDC_MPSETUP_LOCK:
		if (GetNetworkManager().IsServer())
		{
			_sessionLocked = !_sessionLocked;
			GetNetworkManager().LockSession(_sessionLocked);
		}
		else if (GetNetworkManager().IsGameMaster() && !GetNetworkManager().IsAdmin())
		{
			_sessionLocked = !_sessionLocked;
			GetNetworkManager().SendLockSession(_sessionLocked);
		}
		break;
	default:
		Display::OnButtonClicked(idc);
		break;
	}
}

void DisplayMultiplayerSetup::OnLBSelChanged(int idc, int curSel)
{
	switch (idc)
	{
	case IDC_MPSETUP_ROLES:
		if (curSel >= 0 && _player != NO_PLAYER)
		{
			C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_MPSETUP_ROLES));
			if (lbox)
			{
				int val = lbox->GetValue(curSel);
				if (val >= 0)
				{
					const PlayerRole *role = GetNetworkManager().GetPlayerRole(val);
					if (role)
					{
						if (role->player == _player)
						{
							if (_player == GetNetworkManager().GetPlayer())
								GetNetworkManager().ClientReady(NGSPrepareSide);

							if (_allDisabled)
								GetNetworkManager().AssignPlayer(val, NO_PLAYER);
							else
								GetNetworkManager().AssignPlayer(val, AI_PLAYER);
						}
						else
							GetNetworkManager().AssignPlayer(val, _player);
					}
				}
			}
		}
		break;
	case IDC_MPSETUP_POOL:
		if (curSel >=0)
		{
			C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_MPSETUP_POOL));
			if (lbox) _player = lbox->GetValue(curSel);
		}
		break;
	case IDC_MPSETUP_PARAM1:
	case IDC_MPSETUP_PARAM2:
		{
			float param1 = 0, param2 = 0;
			const MissionHeader *header = GetNetworkManager().GetMissionHeader();

			C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_MPSETUP_PARAM1));
			if (lbox)
			{
				int sel = lbox->GetCurSel();
				if (sel >= 0 && header && header->valuesParam1.Size() == lbox->GetSize())
					param1 = header->valuesParam1[sel];
			}

			lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_MPSETUP_PARAM2));
			if (lbox)
			{
				int sel = lbox->GetCurSel();
				if (sel >= 0 && header && header->valuesParam2.Size() == lbox->GetSize())
					param2 = header->valuesParam2[sel];
			}

			GetNetworkManager().SetParams(param1, param2);
		}
		break;
	default:
		Display::OnLBSelChanged(idc, curSel);
		break;
	}
}

void DisplayMultiplayerSetup::OnChildDestroyed(int idd, int exit)
{
	if (GetNetworkManager().IsServer())
	{
		switch (idd)
		{
		case IDD_MSG_LAUNCHGAME:
			Display::OnChildDestroyed(idd, exit);
			if (exit == IDC_OK)
			{
				// check if player assigned
				int player = GetNetworkManager().GetPlayer();
				bool found = false;
				if (player != 0)
					for (int i=0; i<GetNetworkManager().NPlayerRoles(); i++)
					{
						const PlayerRole *role = GetNetworkManager().GetPlayerRole(i);
						if (role && role->player == player)
						{
							found = true;
							break;
						}
					}
				if (!found)
				{
					CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_MP_ASSIGN_PLAYERS));
					break;
				}

				// create game
				GetNetworkManager().SetGameState(NGSTransferMission);
				// wait until NGSTransferMission on client
			}
			break;
		case IDD_SERVER_GET_READY:
			{
				Display::OnChildDestroyed(idd, exit);
				if (exit == IDC_OK)
				{
					GetNetworkManager().SetGameState(NGSPlay);
					_play = true;
				}
				else
				{
					GetNetworkManager().SetGameState(NGSPrepareSide);
					GetNetworkManager().ClientReady(NGSPrepareSide);
					_transferMission = true;
					_loadIsland = true;
					_play = false;
					_message = "";
				}
			}
			break;
		case IDD_MISSION:
			{
				Display::OnChildDestroyed(idd, exit);
				GetNetworkManager().DestroyAllObjects();
				GetNetworkManager().SetGameState(NGSDebriefing);
				GStats.OnMPMissionEnd();
				CreateChild(new DisplayDebriefing(this, false));
			}
			break;
		case IDD_DEBRIEFING:
			{
				GetNetworkManager().SetGameState(NGSPrepareSide);
				GetNetworkManager().ClientReady(NGSPrepareSide);
				_transferMission = true;
				_loadIsland = true;
				_play = false;
				Display::OnChildDestroyed(idd, exit);
			}
			break;
		default:
			Display::OnChildDestroyed(idd, exit);
			break;
		}
	}
	else
	{
		switch (idd)
		{
		case IDD_MSG_LAUNCHGAME:
			Display::OnChildDestroyed(idd, exit);
			if (exit == IDC_OK && GetNetworkManager().IsGameMaster())
			{
				GetNetworkManager().ClientReady(NGSPrepareOK);
				GetCtrl(IDC_OK)->ShowCtrl(false);
			}
			break;
		case IDD_CLIENT_GET_READY:
			Display::OnChildDestroyed(idd, exit);
			if (exit == IDC_OK)
			{
				GetNetworkManager().ClientReady(NGSPlay);
				CreateChild(new DisplayMission(this));
			}
			else if (exit == IDC_CANCEL)
			{
				Exit(IDC_CANCEL);
			}
			else
			{
				GetNetworkManager().DestroyAllObjects();
				_message = "";
				CreateChild(new DisplayClientWait(this));
			}
			break;
		case IDD_MISSION:
			{
				Display::OnChildDestroyed(idd, exit);
				GetNetworkManager().DestroyAllObjects();
				GetNetworkManager().ClientReady(NGSDebriefing);
				CreateChild(new DisplayClientDebriefing(this, false));
			}
			break;
		case IDD_CLIENT_WAIT:
			Display::OnChildDestroyed(idd, exit);
			if (exit == IDC_CANCEL) Exit(IDC_CANCEL);  
			break;
		case IDD_DEBRIEFING:
			Display::OnChildDestroyed(idd, exit);
			if (GetNetworkManager().IsGameMaster())
			{
				// GetNetworkManager().ClientReady(NGSDebriefingOK);
				_transferMission = true;
				_loadIsland = true;
				_play = false;
			}
			else if (exit == IDC_CANCEL)
			{
				Exit(IDC_CANCEL);
			}
			break;
		default:
			Display::OnChildDestroyed(idd, exit);
			break;
		}
	}
}

void DisplayMultiplayerSetup::OnSimulate(EntityAI *vehicle)
{
	NetworkGameState state = GetNetworkManager().GetServerState();

	if (GetNetworkManager().IsServer())
	{
		switch (state)
		{
		case NGSTransferMission:
			if (_transferMission)
			{
				_transferMission = false;
				GetNetworkManager().SendMissionFile();
				GetNetworkManager().SetGameState(NGSLoadIsland);
				// wait until NGSTransferMission on client
			}
			break;
		case NGSLoadIsland:
			if (_loadIsland)
			{
				_loadIsland = false;
				GLOB_WORLD->SwitchLandscape(GetWorldName(Glob.header.worldname));

				// set parameters - after SwitchLandscape
				float param1 = 0, param2 = 0;
				GetNetworkManager().GetParams(param1, param2);
				GameState *gstate = GWorld->GetGameState();
				gstate->VarSet("param1", GameValue(param1), false);
				gstate->VarSet("param2", GameValue(param2), false);
				GetNetworkManager().PublicVariable("param1");
				GetNetworkManager().PublicVariable("param2");
				
				GStats.ClearMission();
				GWorld->ActivateAddons(CurrentTemplate.addOns);
				GLOB_WORLD->InitGeneral(CurrentTemplate.intel);
				if (!GLOB_WORLD->InitVehicles(GModeNetware, CurrentTemplate))
				{
					// TODO: initvehicles returned error, we cannot proceed to the next state
				}
				
				_message = LocalizeString(IDS_NETWORK_SEND);

				GetNetworkManager().CreateAllObjects();
				// wait until NGSBriefing on client
			}
			break;
		case NGSBriefing:
			if (!_play)
			{
				Assert(GWorld->CameraOn());
				GetNetworkManager().ClientReady(NGSPlay);
				// always show briefing - used for synchronization
				RString weapons = GetSaveDirectory() + RString("weapons.cfg");
				unlink(weapons);
				RunInitScript();
				CreateChild(new DisplayServerGetReady(this));
				return;
			}
		case NGSPlay:
			if (_play)
			{
				_play = false;
				_message = "";
				CreateChild(new DisplayMission(this));
				return;
			}
		}
	}
	else
	{
		switch (state)
		{
		case NGSNone:
		case NGSCreating:
		case NGSCreate:
		case NGSLogin:
		case NGSEdit:
			OnButtonClicked(IDC_AUTOCANCEL);
			break;
		case NGSPrepareSide:
		case NGSPrepareRole:
		case NGSPrepareOK:
			// continue with waiting
			break;
		case NGSTransferMission:
			{
				int curBytes, totBytes;
				GetNetworkManager().GetTransferStats(curBytes, totBytes);
				int curKB = toInt(curBytes / 1024);
				int totKB = toInt(totBytes / 1024);
				BString<256> buffer;
				sprintf(buffer, LocalizeString(IDS_MP_TRANSFER_FILE), curKB, totKB);
				_message = (const char *)buffer;
			}
			// continue with waiting
			break;
		case NGSLoadIsland:
			_message = LocalizeString(IDS_NETWORK_RECEIVE);
			// continue with waiting
			break;
		case NGSBriefing:
			// always show briefing - used for synchronization
			_message = "";
			if (GetNetworkManager().GetMyPlayerRole())
			{
				// create markers
				markersMap.Clear();
				int n = CurrentTemplate.markers.Size();
				for (int i=0; i<n; i++) markersMap.Add(CurrentTemplate.markers[i]);
				
				RString weapons = GetSaveDirectory() + RString("weapons.cfg");
				unlink(weapons);

				RunInitScript();
				CreateChild(new DisplayClientGetReady(this));
				GetNetworkManager().ClientReady(NGSBriefing);
				break;
			}
			// else continue
		case NGSPlay:
			CreateChild(new DisplayClientWait(this));
			break;
		case NGSDebriefing:
			break;
		}
	}

	if (state >= NGSPrepareSide && GetNetworkManager().FindIdentity(GetNetworkManager().GetPlayer()))
	{
		if (!_init)
		{
			Init();
			_init = true;
		}
		Update();
	}

	if (_message.GetLength() == 0)
		Display::OnSimulate(vehicle);
}

void DisplayMultiplayerSetup::Preinit()
{
	// hide params
	if (GetCtrl(IDC_MPSETUP_PARAM1_TITLE)) GetCtrl(IDC_MPSETUP_PARAM1_TITLE)->ShowCtrl(false);
	if (GetCtrl(IDC_MPSETUP_PARAM1)) GetCtrl(IDC_MPSETUP_PARAM1)->ShowCtrl(false);
	if (GetCtrl(IDC_MPSETUP_PARAM2_TITLE)) GetCtrl(IDC_MPSETUP_PARAM2_TITLE)->ShowCtrl(false);
	if (GetCtrl(IDC_MPSETUP_PARAM2)) GetCtrl(IDC_MPSETUP_PARAM2)->ShowCtrl(false);

	// hide roles
	if (GetCtrl(IDC_MPSETUP_ROLES_TITLE)) GetCtrl(IDC_MPSETUP_ROLES_TITLE)->ShowCtrl(false);
	if (GetCtrl(IDC_MPSETUP_ROLES)) GetCtrl(IDC_MPSETUP_ROLES)->ShowCtrl(false);

	// empty mission info
	C3DStatic *text = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MPSETUP_ISLAND));
	if (text) text->SetText("");
	text = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MPSETUP_NAME));
	if (text) text->SetText("");
	text = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MPSETUP_DESC));
	if (text) text->SetText("");

	// side buttons
	C3DActiveText *button = dynamic_cast<C3DActiveText *>(GetCtrl(IDC_MPSETUP_WEST));
	if (button) button->EnableCtrl(false);
	button = dynamic_cast<C3DActiveText *>(GetCtrl(IDC_MPSETUP_EAST));
	if (button) button->EnableCtrl(false);
	button = dynamic_cast<C3DActiveText *>(GetCtrl(IDC_MPSETUP_GUERRILA));
	if (button) button->EnableCtrl(false);
	button = dynamic_cast<C3DActiveText *>(GetCtrl(IDC_MPSETUP_CIVILIAN));
	if (button) button->EnableCtrl(false);

	// message
	text = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MPSETUP_MESSAGE));
	if (text) text->SetText(LocalizeString(IDS_MSG_WAIT_CONNECTING));

	// hide kick button
	if (GetCtrl(IDC_MPSETUP_KICK)) GetCtrl(IDC_MPSETUP_KICK)->ShowCtrl(false);

	// hide enable / disable all
	if (GetCtrl(IDC_MPSETUP_ENABLE_ALL)) GetCtrl(IDC_MPSETUP_ENABLE_ALL)->ShowCtrl(false);

	// hide lock session
	if (GetCtrl(IDC_MPSETUP_LOCK)) GetCtrl(IDC_MPSETUP_LOCK)->ShowCtrl(false);

	// cancel button
	if (!GetNetworkManager().IsServer())
	{
		CActiveText *button = dynamic_cast<CActiveText *>(GetCtrl(IDC_CANCEL));
		if (button) button->SetText(LocalizeString(IDS_DISP_DISCONNECT));
	}
}

void DisplayMultiplayerSetup::Init()
{
	const MissionHeader *header = GetNetworkManager().GetMissionHeader();

	// param1
	int n;
	if (header && (n = header->valuesParam1.Size()) > 0)
	{
		C3DStatic *text = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MPSETUP_PARAM1_TITLE));
		if (text)
		{
			text->ShowCtrl(true);
			text->SetText(header->titleParam1);
		}
		C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_MPSETUP_PARAM1));
		if (lbox)
		{
			lbox->ShowCtrl(true);

			Assert(header->textsParam1.Size() == n);
			int sel = 0;
			for (int i=0; i<n; i++)
			{
				lbox->AddString(header->textsParam1[i]);
				if (header->valuesParam1[i] == header->defValueParam1) sel = i;
			}
			lbox->SetCurSel(sel, false);
		}
	}
	else
	{
		GetCtrl(IDC_MPSETUP_PARAM1_TITLE)->ShowCtrl(false);
		GetCtrl(IDC_MPSETUP_PARAM1)->ShowCtrl(false);
	}
	
	// param2
	if (header && (n = header->valuesParam2.Size()) > 0)
	{
		C3DStatic *text = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MPSETUP_PARAM2_TITLE));
		if (text)
		{
			text->ShowCtrl(true);
			text->SetText(header->titleParam2);
		}
		C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_MPSETUP_PARAM2));
		if (lbox)
		{
			lbox->ShowCtrl(true);

			Assert(header->textsParam2.Size() == n);
			int sel = 0;
			for (int i=0; i<n; i++)
			{
				lbox->AddString(header->textsParam2[i]);
				if (header->valuesParam2[i] == header->defValueParam2) sel = i;
			}
			lbox->SetCurSel(sel, false);
		}
	}
	else
	{
		GetCtrl(IDC_MPSETUP_PARAM2_TITLE)->ShowCtrl(false);
		GetCtrl(IDC_MPSETUP_PARAM2)->ShowCtrl(false);
	}

	C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_MPSETUP_POOL));
	if (lbox)
	{
		lbox->SetColorPicture(true);
	}
}

/*!
\patch 1.61 Date 5/29/2002 by Jirka
- Fixed: Multiplayer setup display - when roles was scrolled down and switched on side with fewer roles, listbox was corrupted
\patch 1.79 Date 7/29/2002 by Jirka
- Fixed: Multiplayer setup display - sometimes bad message appears for server (or admin)
\patch 1.82 Date 8/19/2002 by Fixed
- Fixed: MP: Player could press OK before selecting role.
*/

void DisplayMultiplayerSetup::Update()
{
	bool server = GetNetworkManager().IsServer() || GetNetworkManager().IsGameMaster();

	// mission info
	const MissionHeader *header = GetNetworkManager().GetMissionHeader();
	C3DStatic *text = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MPSETUP_ISLAND));
	if (text)
	{
		RString island = "";
		if (header && header->island.GetLength() > 0) island = Pars >> "CfgWorlds" >> header->island  >> "description";
		text->SetText(island);
	}
	text = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MPSETUP_NAME));
	if (text)
		text->SetText(header ? header->name : "");
	text = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MPSETUP_DESC));
	if (text)
		text->SetText(header ? header->description : "");

	// players
	C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_MPSETUP_POOL));
	if (lbox)
	{
		lbox->ClearStrings();

		const AutoArray<PlayerIdentity> &identities = *GetNetworkManager().GetIdentities();
		if (&identities) for (int i=0; i<identities.Size(); i++)
		{
			const PlayerIdentity &identity = identities[i];
			int index = lbox->AddString(GetIdentityText(identity));
			lbox->SetValue(index, identity.dpnid);
			TargetSide side;
			bool locked;
			NetworkGameState state = GetPlayerState(identity.dpnid, &side, &locked);
			if (state >= NGSPrepareOK)
			{
				lbox->SetFtColor(index, PackedColor(Color(0, 1, 0, 1)));
				lbox->SetSelColor(index, PackedColor(Color(0, 1, 0, 1)));
			}
			else if (state >= NGSPrepareRole)
			{
				lbox->SetFtColor(index, PackedColor(Color(1, 1, 0, 1)));
				lbox->SetSelColor(index, PackedColor(Color(1, 1, 0, 1)));
			}
			else
			{
				lbox->SetFtColor(index, PackedColor(Color(1, 0, 0, 1)));
				lbox->SetSelColor(index, PackedColor(Color(1, 0, 0, 1)));
			}

			Texture *texture = _none;
			if (state >= NGSPrepareRole) switch (side)
			{
			case TWest:
				if (locked) texture = _westLocked;
				else texture = _westUnlocked;
				break;
			case TEast:
				if (locked) texture = _eastLocked;
				else texture = _eastUnlocked;
				break;
			case TGuerrila:
				if (locked) texture = _guerLocked;
				else texture = _guerUnlocked;
				break;
			case TCivilian:
				if (locked) texture = _civlLocked;
				else texture = _civlUnlocked;
				break;
			}
			lbox->SetTexture(index, texture);
		}
		lbox->SortItemsByValue();
		
		if (server)
		{
			lbox->SetReadOnly(false);
			if (_player == NO_PLAYER) _player = GetNetworkManager().GetPlayer();
			int sel = 0;
			for (int i=0; i<lbox->GetSize(); i++)
			{
				if (lbox->GetValue(i) == _player)
				{
					sel = i;
					break;
				}
			}
			lbox->SetCurSel(sel);
		}
		else
		{
			lbox->SetReadOnly(true);
			_player = GetNetworkManager().GetPlayer();
			for (int i=0; i<lbox->GetSize(); i++)
			{
				if (lbox->GetValue(i) == _player)
				{
					lbox->SetCurSel(i);
					break;
				}
			}
		}
	}


	// sides
	if (_side == TSideUnknown && GetNetworkManager().NPlayerRoles() > 0)
		_side = GetNetworkManager().GetPlayerRole(0)->side;

	C3DActiveText *button = dynamic_cast<C3DActiveText *>(GetCtrl(IDC_MPSETUP_WEST));
	if (button) button->EnableCtrl(SideExist(TWest));
	button = dynamic_cast<C3DActiveText *>(GetCtrl(IDC_MPSETUP_EAST));
	if (button) button->EnableCtrl(SideExist(TEast));
	button = dynamic_cast<C3DActiveText *>(GetCtrl(IDC_MPSETUP_GUERRILA));
	if (button) button->EnableCtrl(SideExist(TGuerrila));
	button = dynamic_cast<C3DActiveText *>(GetCtrl(IDC_MPSETUP_CIVILIAN));
	if (button) button->EnableCtrl(SideExist(TCivilian));

	CMPSideButton *btnWest = dynamic_cast<CMPSideButton *>(GetCtrl(IDC_MPSETUP_WEST));
	CMPSideButton *btnEast = dynamic_cast<CMPSideButton *>(GetCtrl(IDC_MPSETUP_EAST));
	CMPSideButton *btnGuer = dynamic_cast<CMPSideButton *>(GetCtrl(IDC_MPSETUP_GUERRILA));
	CMPSideButton *btnCivl = dynamic_cast<CMPSideButton *>(GetCtrl(IDC_MPSETUP_CIVILIAN));
	if (btnWest) btnWest->Toggle(false);
	if (btnEast) btnEast->Toggle(false);
	if (btnGuer) btnGuer->Toggle(false);
	if (btnCivl) btnCivl->Toggle(false);
	switch (_side)
	{
	case TWest:
		if (btnWest) btnWest->Toggle(true);
		break;
	case TEast:
		if (btnEast) btnEast->Toggle(true);
		break;
	case TGuerrila:
		if (btnGuer) btnGuer->Toggle(true);
		break;
	case TCivilian:
		if (btnCivl) btnCivl->Toggle(true);
		break;
	}

	NetworkGameState state = GetPlayerState(_player);

	// roles
	lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_MPSETUP_ROLES));
	if (lbox)
	{
		lbox->ClearStrings();
		if (_side != TSideUnknown)
		{
			lbox->ShowCtrl(true);

			int sel = -1;
			int group = -1;
			for (int i=0; i<GetNetworkManager().NPlayerRoles(); i++)
			{
				const PlayerRole *role = GetNetworkManager().GetPlayerRole(i);
				if (role->side != _side) continue;

				if (role->group != group)
				{
					group = role->group;
					RString desc = GetGroupDescription(group);
					int index = lbox->AddString(desc);
					lbox->SetValue(index, -1);
				}

				RString desc = GetRoleDescription(role);
				int index = lbox->AddString(desc);
				lbox->SetValue(index, i);
				if (role->player == _player) sel = index;
			}
			if (sel >= 0)
				lbox->SetCurSel(sel, false);
			else
				lbox->Check();
			lbox->ShowSelected(sel >= 0);

			text = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MPSETUP_ROLES_TITLE));
			if (text)
			{
				text->ShowCtrl(true);
				RString message;
				switch (_side)
				{
				case TWest:
					message = LocalizeString(IDS_DISP_MPSETUP_ROLES_WEST);
					break;
				case TEast:
					message = LocalizeString(IDS_DISP_MPSETUP_ROLES_EAST);
					break;
				case TGuerrila:
					message = LocalizeString(IDS_DISP_MPSETUP_ROLES_GUERRILA);
					break;
				case TCivilian:
					message = LocalizeString(IDS_DISP_MPSETUP_ROLES_CIVILIAN);
					break;
				default:
					message = LocalizeString(IDS_DISP_MPROLE_ROLES);
					break;
				}
				text->SetText(message);
			}
		}
		else
		{
			GetCtrl(IDC_MPSETUP_ROLES_TITLE)->ShowCtrl(false);
			lbox->ShowCtrl(false);
		}
	}

	// parameters
	float param1 = 0, param2 = 0;
	GetNetworkManager().GetParams(param1, param2);

	lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_MPSETUP_PARAM1));
	if (lbox && header && header->valuesParam1.Size() == lbox->GetSize())
	{
		lbox->SetReadOnly(!server);
		int sel = 0;
		for (int i=0; i<lbox->GetSize(); i++)
		{
			if (param1 == header->valuesParam1[i])
			{
				sel = i;
				break;
			}
		}
		lbox->SetCurSel(sel, false);
	}

	lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_MPSETUP_PARAM2));
	if (lbox && header && header->valuesParam2.Size() == lbox->GetSize())
	{
		lbox->SetReadOnly(!server);
		int sel = 0;
		for (int i=0; i<lbox->GetSize(); i++)
		{
			if (param2 == header->valuesParam2[i])
			{
				sel = i;
				break;
			}
		}
		lbox->SetCurSel(sel, false);
	}

	// message
	text = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MPSETUP_MESSAGE));
	if (text)
	{
		RString message;
		if (_player == NO_PLAYER)
			message = LocalizeString(IDS_MSG_WAIT_CONNECTING);
		else switch (state)
		{
			case NGSPrepareSide:
				message = LocalizeString(IDS_MSG_CHOOSE_ROLE);
				break;
			case NGSPrepareRole:
				if (server)
					message = LocalizeString(IDS_MSG_OK_LAUNCH_GAME);
				else
					message = LocalizeString(IDS_MSG_OK_READY);
				break;
			case NGSPrepareOK:
				if (server)
					message = LocalizeString(IDS_MSG_OK_LAUNCH_GAME);
				else
					message = LocalizeString(IDS_MSG_WAIT_FOR_OTHERS);
				break;
		}
		text->SetText(message);
	}

	// kick button
	if (GetCtrl(IDC_MPSETUP_KICK)) GetCtrl(IDC_MPSETUP_KICK)->ShowCtrl(server && _player != GetNetworkManager().GetPlayer());

	// enable / disable all
	button = dynamic_cast<C3DActiveText *>(GetCtrl(IDC_MPSETUP_ENABLE_ALL));
	if (button)
	{
#if _ENABLE_AI
		if (server && !header->disabledAI)
		{
			bool allDisabled = true;
			bool allPlayers = true;
			for (int i=0; i<GetNetworkManager().NPlayerRoles(); i++)
			{
				const PlayerRole *role = GetNetworkManager().GetPlayerRole(i);
				if (role->player == AI_PLAYER)
				{
					allDisabled = false;
					allPlayers = false;
					break;
				}
				else if (role->player == NO_PLAYER)
					allPlayers = false;
			}
			if (allPlayers)
				button->ShowCtrl(false);
			else
			{
				button->ShowCtrl(true);
				if (allDisabled)
				{
					button->SetText("\\misc\\comp_allx.paa");
					button->SetTooltip(LocalizeString(IDS_TOOLTIP_ENABLE_ALL_AI));
				}
				else
				{
					button->SetText("\\misc\\comp_allai.paa");
					button->SetTooltip(LocalizeString(IDS_TOOLTIP_DISABLE_ALL_AI));
				}
			}
			_allDisabled = allDisabled && !allPlayers;
		}
		else
#endif
			button->ShowCtrl(false);
	}

	// lock session
	button = dynamic_cast<C3DActiveText *>(GetCtrl(IDC_MPSETUP_LOCK));
	if (button)
	{
		// TODO: stringtable
		if (GetNetworkManager().IsServer() || GetNetworkManager().IsGameMaster() && !GetNetworkManager().IsAdmin())
		{
			button->ShowCtrl(true);
			if (_sessionLocked)
			{
				button->SetText("\\misc\\lock_ed.paa");
				button->SetTooltip(LocalizeString(IDS_UNLOCK_HOST));
			}
			else
			{
				button->SetText("\\misc\\lock_open.paa");
				button->SetTooltip(LocalizeString(IDS_LOCK_HOST));
			}
		}
		else button->ShowCtrl(false);
	}

	CActiveText *okButton = dynamic_cast<CActiveText *>(GetCtrl(IDC_OK));
	if (okButton)
	{
		bool enabled = true;
		bool foundPlayer = false;
		int player = GetNetworkManager().GetPlayer();
		int n = 0;
		for (int i=0; i<GetNetworkManager().NPlayerRoles(); i++)
		{
			int dpid = GetNetworkManager().GetPlayerRole(i)->player;
			if (dpid == player) foundPlayer = true;
			if (GetNetworkManager().FindIdentity(dpid)) n++;
		}
		if (!GetNetworkManager().IsGameMaster())
		{
			// starting mission has no sense if my role is not assigned yet
			if (!foundPlayer) enabled = false;
		}
		else
		{
			// starting mission has no sense if no role is not assigned yet
			// my role need not be assigned - I am admin
			if (n==0) enabled = false;
		}
		okButton->EnableCtrl(enabled);
	}

}

/*!
\patch 1.90 Date 10/30/2002 by Ondra
- Improved: Adjustable 2D viewport - improved support for Surround Gaming.
*/

void DisplayMultiplayerSetup::OnDraw(EntityAI *vehicle, float alpha)
{
	if (_message.GetLength() > 0)
	{
		//const int w = GLOB_ENGINE->Width2D();
		//const int h = GLOB_ENGINE->Height2D();
	
		const int w3d = GLOB_ENGINE->Width();
		const int h3d = GLOB_ENGINE->Height();

		MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
		GLOB_ENGINE->Draw2D(mip, PackedBlack, Rect2DAbs(0, 0, w3d, h3d));

		float textW = GEngine->GetTextWidth
		(
			_messageSize, _messageFont, _message
		);
		GEngine->DrawText
		(
			Point2DFloat(0.5 - 0.5 * textW, 0.5),
			_messageSize, _messageFont,
			PackedWhite, _message
		);
	}
	else
	{
		Display::OnDraw(vehicle, alpha);

		if (_dragging)
		{
			float x = GInput.cursorX * 0.5 + 0.5;
			float y = GInput.cursorY * 0.5 + 0.5;
			y -= 0.5 * _dragSize;
			GEngine->DrawText(Point2DFloat(x, y), _dragSize, _dragFont, _dragColor, _dragName);
		}
	}
}

bool DisplayMultiplayerSetup::CanDrag(int player)
{
	if (GetNetworkManager().IsServer() || GetNetworkManager().IsGameMaster()) return true;
	return player == GetNetworkManager().GetPlayer();
}

void DisplayMultiplayerSetup::OnLBDrag(int idc, int curSel)
{
	switch (idc)
	{
	case IDC_MPSETUP_POOL:
		{
			C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(idc));
			Assert(lbox);
			int player = lbox->GetValue(curSel);
			if (!CanDrag(player)) break;
			_player = player;
			_dragging = true;
			SetCursor(NULL);
			_dragPlayer = player;
			const PlayerIdentity *identity = GetNetworkManager().FindIdentity(player);
			if (identity) _dragName = identity->GetName();
			else _dragName = lbox->GetText(curSel);
		}
		break;
	default:
		Display::OnLBDrag(idc, curSel);
		break;
	}
}

void DisplayMultiplayerSetup::OnLBDragging(float x, float y)
{
	if (_dragging)
	{
		IControl *ctrl = GetCtrl(x, y);
		if (ctrl && ctrl->IDC() == IDC_MPSETUP_ROLES)
		{
/*
			C3DListBox *lbox = dynamic_cast<C3DListBox *>(ctrl);
			Assert(lbox);
			lbox->SetCurSel(x, y);
*/
		}
	}
	else
		Display::OnLBDragging(x, y);
}

void DisplayMultiplayerSetup::OnLBDrop(float x, float y)
{
	if (_dragging)
	{
		IControl *ctrl = GetCtrl(x, y);
		if (ctrl && ctrl->IDC() == IDC_MPSETUP_ROLES)
		{
			C3DListBox *lbox = dynamic_cast<C3DListBox *>(ctrl);
			Assert(lbox);
			lbox->SetCurSel(x, y);
			int sel = lbox->GetCurSel();
			int role = lbox->GetValue(sel);
			GetNetworkManager().AssignPlayer(role, _dragPlayer);
		}

		_dragging = false;
		SetCursor("Arrow");
	}
	else
		Display::OnLBDrop(x, y);
}


// Client and server briefing, debriefing 

DisplayClientDebriefing::DisplayClientDebriefing(ControlsContainer *parent, bool animation)
: base(parent, animation)
{
}

void DisplayClientDebriefing::OnSimulate(EntityAI *vehicle)
{
	NetworkGameState gameState = GetNetworkManager().GetGameState();
	switch (gameState)
	{
	case NGSNone:
	case NGSCreating:
	case NGSCreate:
	case NGSLogin:
	case NGSEdit:
	case NGSPrepareSide:
	case NGSPrepareRole:
	case NGSPrepareOK:
	case NGSTransferMission:
	case NGSLoadIsland:
	case NGSBriefing:
		OnButtonClicked(IDC_AUTOCANCEL);
		break;
	}
/*
	CActiveText *button = dynamic_cast<CActiveText *>(GetCtrl(IDC_CANCEL));
	if (button)
	{
		if (GetNetworkManager().IsGameMaster())
			button->SetText(LocalizeString(IDS_DISP_CANCEL));
		else
			button->SetText(LocalizeString(IDS_DISP_DISCONNECT));
	}
*/		
	base::OnSimulate(vehicle);
}

DisplayServerGetReady::DisplayServerGetReady(ControlsContainer *parent)
	: base(parent, "RscDisplayServerGetReady")
{
}

/*!
\patch 1.04 Date 07/16/2001 by Jirka
- Fixed: show warning message box if all players are not ready in server briefing
*/
void DisplayServerGetReady::OnButtonClicked(int idc)
{
	switch (idc)
	{
	case IDC_OK:
		{
			CListBox *lbox = dynamic_cast<CListBox *>(GetCtrl(IDC_SERVER_READY_PLAYERS));
			Assert(lbox);
			for (int i=0; i<lbox->GetSize(); i++)
			{
				if (lbox->GetValue(i) == 0)
				{
					CreateMsgBox
					(
						MB_BUTTON_OK | MB_BUTTON_CANCEL,
						LocalizeString(IDS_MSG_LAUNCH_GAME),
						IDD_MSG_LAUNCHGAME
					);
					return;
				}
			}
			Exit(IDC_OK);
		}
		break;
	default:
		base::OnButtonClicked(idc);
		break;
	}
}

void DisplayServerGetReady::Destroy()
{
	base::Destroy();
}

/*!
\patch 1.40 Date 12/18/2001 by Jirka
- Fixed: Host cannot insert markers at briefing
*/

void DisplayServerGetReady::OnChildDestroyed(int idd, int exit)
{
	switch (idd)
	{
		case IDD_MSG_LAUNCHGAME:
			Display::OnChildDestroyed(idd, exit);
			if (exit == IDC_OK) Exit(IDC_OK);
			break;
		default:
			DisplayMap::OnChildDestroyed(idd, exit);
			break;
	}
}

/*!
\patch 1.04 Date 07/16/2001 by Jirka
- Fixed: show only assigned players (all connected players was shown)
*/
void DisplayServerGetReady::OnSimulate(EntityAI *vehicle)
{
	// update player list
	CListBox *lbox = dynamic_cast<CListBox *>(GetCtrl(IDC_SERVER_READY_PLAYERS));
	if (lbox)
	{
/*
		AUTO_STATIC_ARRAY(NetPlayerInfo, players, 32)
		GetNetworkManager().GetPlayers(players);
*/

		lbox->SetReadOnly();
		lbox->ShowSelected(false);
		lbox->ClearStrings();
		// FIXED
/*
		for (int i=0; i<players.Size(); i++)
		{
			NetPlayerInfo &info = players[i];
			int index = lbox->AddString(info.name);
			switch (GetNetworkManager().GetPlayerState(info.dpid))
*/
		for (int i=0; i<GetNetworkManager().NPlayerRoles(); i++)
		{
			int dpnid = GetNetworkManager().GetPlayerRole(i)->player;
			if (dpnid == NO_PLAYER || dpnid == AI_PLAYER) continue;
			const PlayerIdentity *identity = GetNetworkManager().FindIdentity(dpnid);
			if (!identity) continue;
			int index = lbox->AddString(GetIdentityText(*identity));
//			switch (GetNetworkManager().GetPlayerState(dpnid))
			switch (identity->state)
			{
			case NGSNone:
			case NGSCreating:
			case NGSCreate:
			case NGSLogin:
			case NGSEdit:
			case NGSMissionVoted:
			case NGSPrepareSide:
			case NGSPrepareRole:
			case NGSPrepareOK:
			case NGSDebriefing:
			case NGSDebriefingOK:
			case NGSTransferMission:
			case NGSLoadIsland:
				lbox->SetFtColor(index, PackedColor(Color(1, 0, 0, 1)));
				lbox->SetSelColor(index, PackedColor(Color(1, 0, 0, 1)));
				lbox->SetValue(index, 0);
				break;
			case NGSBriefing:
				lbox->SetFtColor(index, PackedColor(Color(1, 1, 0, 1)));
				lbox->SetSelColor(index, PackedColor(Color(1, 1, 0, 1)));
				lbox->SetValue(index, 1);
				break;
			case NGSPlay:
				lbox->SetFtColor(index, PackedColor(Color(0, 1, 0, 1)));
				lbox->SetSelColor(index, PackedColor(Color(0, 1, 0, 1)));
				lbox->SetValue(index, 2);
				break;
			default:
				break;
			}
		}
		lbox->SortItemsByValue();
	}

	base::OnSimulate(vehicle);
}

DisplayClientGetReady::DisplayClientGetReady(ControlsContainer *parent)
	: base(parent, "RscDisplayClientGetReady")
{
}

void DisplayClientGetReady::Destroy()
{
	base::Destroy();
}

void DisplayClientGetReady::OnButtonClicked(int idc)
{
	switch (idc)
	{
		case IDC_AUTOCANCEL:
			Exit(idc);
			break;
		case IDC_OK:
			GetNetworkManager().ClientReady(NGSPlay);
			GetCtrl(IDC_OK)->ShowCtrl(false);
			break;
		default:
			base::OnButtonClicked(idc);
			break;
	}
}


void DisplayClientGetReady::OnSimulate(EntityAI *vehicle)
{
	// update player list
	CListBox *lbox = dynamic_cast<CListBox *>(GetCtrl(IDC_CLIENT_READY_PLAYERS));
	if (lbox)
	{
		lbox->SetReadOnly();
		lbox->ShowSelected(false);
		lbox->ClearStrings();

		for (int i=0; i<GetNetworkManager().NPlayerRoles(); i++)
		{
			int dpnid = GetNetworkManager().GetPlayerRole(i)->player;
			if (dpnid == NO_PLAYER || dpnid == AI_PLAYER) continue;
			const PlayerIdentity *identity = GetNetworkManager().FindIdentity(dpnid);
			if (!identity) continue;
			int index = lbox->AddString(GetIdentityText(*identity));
			switch (identity->state)
			{
			case NGSNone:
			case NGSCreating:
			case NGSCreate:
			case NGSLogin:
			case NGSEdit:
			case NGSMissionVoted:
			case NGSPrepareSide:
			case NGSPrepareRole:
			case NGSPrepareOK:
			case NGSDebriefing:
			case NGSDebriefingOK:
			case NGSTransferMission:
			case NGSLoadIsland:
				lbox->SetFtColor(index, PackedColor(Color(1, 0, 0, 1)));
				lbox->SetSelColor(index, PackedColor(Color(1, 0, 0, 1)));
				lbox->SetValue(index, 0);
				break;
			case NGSBriefing:
				lbox->SetFtColor(index, PackedColor(Color(1, 1, 0, 1)));
				lbox->SetSelColor(index, PackedColor(Color(1, 1, 0, 1)));
				lbox->SetValue(index, 1);
				break;
			case NGSPlay:
				lbox->SetFtColor(index, PackedColor(Color(0, 1, 0, 1)));
				lbox->SetSelColor(index, PackedColor(Color(0, 1, 0, 1)));
				lbox->SetValue(index, 2);
				break;
			default:
				break;
			}
		}
		lbox->SortItemsByValue();
	}

	NetworkGameState gameState = GetNetworkManager().GetServerState();
	switch (gameState)
	{
	case NGSNone:
	case NGSCreating:
	case NGSCreate:
	case NGSLogin:
	case NGSEdit:
	case NGSPrepareSide:
	case NGSPrepareRole:
	case NGSPrepareOK:
	case NGSDebriefing:
	case NGSTransferMission:
	case NGSLoadIsland:
		OnButtonClicked(IDC_AUTOCANCEL);
		break;
	case NGSBriefing:
		// continue with waiting
		break;
	case NGSPlay:
		Exit(IDC_OK);
		break;
	}
	base::OnSimulate(vehicle);
}

// Multiplayer players display
/*!
\patch 1.27 Date 10/17/2001 by Jirka
- Fixed: "Players" dialog closed when multiplayer game session end
*/
void DisplayMPPlayers::OnSimulate(EntityAI *vehicle)
{
	if (GInput.GetActionToDo(UANetworkPlayers, true, false))
	{
		OnButtonClicked(IDC_CANCEL);
	}

	if (GetNetworkManager().GetGameState() < NGSPlay)
	{
		OnButtonClicked(IDC_CANCEL);
	}

	UpdatePlayers();
	Display::OnSimulate(vehicle);
}

void DisplayMPPlayers::OnLBSelChanged(int idc, int curSel)
{
	if (idc == IDC_MP_PLAYERS)
		UpdatePlayerInfo();
	else
		Display::OnLBSelChanged(idc, curSel);
}

/*!
\patch 1.25 Date 10/01/2001 by Jirka
- Changed: Players in Multiplayer Players Dialog are colored by the same way as in statistics.
\patch 1.34 Date 12/7/2001 by Jirka
- Added: Time left in MP mission displayed if EstimatedEndTime variable defined
*/

void DisplayMPPlayers::UpdatePlayers()
{
	const MissionHeader *header = GetNetworkManager().GetMissionHeader();
	
	RString mission, island;
	int time = 0;
	if (header)
	{
		mission = header->name;
		island = header->island;
		if (GetNetworkManager().GetServerState() == NGSPlay)
			time = GlobalTickCount() - header->start;
	}

	C3DStatic *ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MP_PL_MISSION));
	if (ctrl) ctrl->SetText(mission);

	ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MP_PL_ISLAND));
	if (ctrl) ctrl->SetText(island);

	int t = time / 1000;
	int m = t / 60;
	int s = t - m * 60;

	ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MP_PL_TIME));
	if (ctrl)
	{
		char buffer[256];
		sprintf(buffer, "%d:%02d", m, s);
		ctrl->SetText(buffer);
	}

	ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MP_PL_REST));
	if (ctrl)
	{
		float et = GetNetworkManager().GetEstimatedEndTime().toFloat();
		if (et > 0)
		{
			et -= t;
			int em = toInt(et / 60.0);
			if (time != 0) saturateMax(em, 1);

			char buffer[256];
			sprintf(buffer, LocalizeString(IDS_TIME_LEFT), em);
			ctrl->SetText(buffer);
			ctrl->ShowCtrl(true);
		}
		else
			ctrl->ShowCtrl(false);
	}

	C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_MP_PLAYERS));
	if (!lbox) return;

/*
	PackedColor inactive = ModAlpha(lbox->GetFtColor(), 0.5);
	PackedColor inactiveSel = ModAlpha(lbox->GetSelColor(), 0.5);
*/

	int sel = lbox->GetCurSel();
	int cur = sel >= 0 ? lbox->GetValue(sel) : 0;

	sel = 0;
	lbox->ClearStrings();
	const AutoArray<PlayerIdentity> *pid = GetNetworkManager().GetIdentities();
	if (pid)
	{
		for (int i=0; i<pid->Size(); i++)
		{
			const PlayerIdentity &identity = pid->Get(i);
			char buffer[256];
			sprintf(buffer, "%d: %s", identity.playerid, (const char *)identity.GetName());
			int index = lbox->AddString(buffer);
			lbox->SetValue(index, identity.dpnid);
			PackedColor colorSel = _color;
			if (identity.state == NGSPlay)
			{
				for (int j=0; j<GetNetworkManager().NPlayerRoles(); j++)
				{
					const PlayerRole *item = GetNetworkManager().GetPlayerRole(j);
					if (item->player == identity.dpnid)
					{
						switch (item->side)
						{
						case TEast:
							colorSel = _colorEast;
							break;
						case TWest:
							colorSel = _colorWest;
							break;
						case TGuerrila:
							colorSel = _colorRes;
							break;
						case TCivilian:
							colorSel = _colorCiv;
							break;
						}
						break;
					}
				}
			}
			PackedColor color = ModAlpha(colorSel, 0.5);
			lbox->SetFtColor(index, color);
			lbox->SetSelColor(index, colorSel);
			/*
			if (identity.state != NGSPlay)
			{
				lbox->SetFtColor(index, inactive);
				lbox->SetSelColor(index, inactiveSel);
			}
			*/
			if (identity.dpnid == cur) sel = index;
		}
	}
	lbox->SetCurSel(sel);
}


static RString FormatNumberMaxDigits(int number, int maxDigits)
{
	int maxNumber = 1;
	while (--maxDigits>=0) maxNumber *= 10;
	maxNumber--;
	if (number>maxNumber) return FormatNumber(maxNumber);
	return FormatNumber(number);
}

inline RString FormatNumberBandwidth(int number)
{
	return FormatNumberMaxDigits(number, 6);
}

inline RString FormatNumberPing(int number)
{
	return FormatNumberMaxDigits(number, 4);
}

/*!
\patch 1.05 Date 7/17/2001 by Jirka
- Added: server can kick off players from "Players" dialog
*/
void DisplayMPPlayers::UpdatePlayerInfo()
{
	const PlayerIdentity *player = NULL;
	const SquadIdentity *squad = NULL;

	C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_MP_PLAYERS));
	if (lbox)
	{
		int sel = lbox->GetCurSel();
		if (sel >= 0)
		{
			player = GetNetworkManager().FindIdentity(lbox->GetValue(sel));
			if (player) squad = player->squad;
		}
	}

	C3DStatic *ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MP_PL));
	if (ctrl) ctrl->SetText(player ? player->name : "");
	ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MP_PL_NAME));
	if (ctrl) ctrl->SetText(player ? player->fullname : "");
	ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MP_PL_MAIL));
	if (ctrl) ctrl->SetText(player ? player->email : "");
	ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MP_PL_ICQ));
	if (ctrl) ctrl->SetText(player ? player->icq : "");
	ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MP_PL_REMARK));
	if (ctrl) ctrl->SetText(player ? player->remark : "");

	ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MP_PL_MINPING));
	if (ctrl) ctrl->SetText(player ? FormatNumberPing(player->_minPing) : "");
	ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MP_PL_AVGPING));
	if (ctrl) ctrl->SetText(player ? FormatNumberPing(player->_avgPing) : "");
	ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MP_PL_MAXPING));
	if (ctrl) ctrl->SetText(player ? FormatNumberPing(player->_maxPing) : "");
	ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MP_PL_MINBAND));
	if (ctrl) ctrl->SetText(player ? FormatNumberBandwidth(player->_minBandwidth) : "");
	ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MP_PL_AVGBAND));
	if (ctrl) ctrl->SetText(player ? FormatNumberBandwidth(player->_avgBandwidth) : "");
	ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MP_PL_MAXBAND));
	if (ctrl) ctrl->SetText(player ? FormatNumberBandwidth(player->_maxBandwidth) : "");
	ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MP_PL_DESYNC));
	if (ctrl) ctrl->SetText(player ? FormatNumberBandwidth(player->_desync) : "");

	ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MP_SQ));
	if (ctrl) ctrl->SetText(squad ? squad->nick : "");
	ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MP_SQ_NAME));
	if (ctrl) ctrl->SetText(squad ? squad->name : "");
	ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MP_SQ_ID));
	if (ctrl) ctrl->SetText(squad ? squad->id : "");
	ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MP_SQ_MAIL));
	if (ctrl) ctrl->SetText(squad ? squad->email : "");
	ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MP_SQ_WEB));
	if (ctrl) ctrl->SetText(squad ? squad->web : "");
	RString picture;
	if (squad && squad->picture.GetLength() > 0)
	{
		picture = RString("tmp\\squads\\") + squad->nick + RString("\\") + squad->picture;
		if (!QIFStream::FileExists(picture)) picture = "";
		else picture = RString("\\") + picture;
	}
	ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MP_SQ_PICTURE));
	if (ctrl) ctrl->SetText(picture);
	ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MP_SQ_TITLE));
	if (ctrl) ctrl->SetText(squad ? squad->title : "");
	
	// ADDED
	if (player)
	{
		bool show = GetNetworkManager().IsServer() && player->dpnid != GetNetworkManager().GetPlayer();
		IControl *ctrl = GetCtrl(IDC_MP_KICKOFF);
		if (ctrl) ctrl->ShowCtrl(show || GetNetworkManager().IsGameMaster() && player->dpnid != GetNetworkManager().GetPlayer());
		ctrl = GetCtrl(IDC_MP_BAN);
		if (ctrl) ctrl->ShowCtrl(show);
	}
}

// ADDED
void DisplayMPPlayers::OnButtonClicked(int idc)
{
	switch (idc)
	{
		case IDC_MP_KICKOFF:
			if (GetNetworkManager().IsServer())
			{
				C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_MP_PLAYERS));
				if (!lbox) break;
				int sel = lbox->GetCurSel();
				if (sel < 0) break;
				int dpnid = lbox->GetValue(sel);
				if (dpnid != GetNetworkManager().GetPlayer())
					GetNetworkManager().KickOff(dpnid,KORKick);
			}
			else if (GetNetworkManager().IsGameMaster())
			{
				C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_MP_PLAYERS));
				if (!lbox) break;
				int sel = lbox->GetCurSel();
				if (sel < 0) break;
				int dpnid = lbox->GetValue(sel);
				if (dpnid != GetNetworkManager().GetPlayer())
					GetNetworkManager().SendKick(dpnid);
			}
			break;
		case IDC_MP_BAN:
			if (GetNetworkManager().IsServer())
			{
				C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_MP_PLAYERS));
				if (!lbox) break;
				int sel = lbox->GetCurSel();
				if (sel < 0) break;
				int dpnid = lbox->GetValue(sel);
				if (dpnid != GetNetworkManager().GetPlayer())
					GetNetworkManager().Ban(dpnid);
			}
			break;
		default:
			Display::OnButtonClicked(idc);
			break;
	}
}

// Client wait display
/*!
\patch 1.42 Date 1/10/2002 by Jirka
- Fixed: KickOff for Game In Progress screen
*/

void DisplayClientWait::OnButtonClicked(int idc)
{
	switch (idc)
	{
		case IDC_AUTOCANCEL:
			Exit(idc);
			break;
		default:
			DisplayMPPlayers::OnButtonClicked(idc);
			break;
	}
}

void DisplayClientWait::OnSimulate(EntityAI *vehicle)
{
	NetworkGameState gameState = GetNetworkManager().GetServerState();
	RString state;
	switch (gameState)
	{
	case NGSPrepareRole:
	case NGSPrepareOK:
		state = LocalizeString(IDS_SESSION_SETUP);
		{
			TargetSide side = TSideUnknown;
			int player = GetNetworkManager().GetPlayer();
			for (int i=0; i<GetNetworkManager().NPlayerRoles(); i++)
			{
				const PlayerRole *role = GetNetworkManager().GetPlayerRole(i);
				if (role->player == player)
				{
					side = role->side;
					break;
				}
			}
			if (side == TSideUnknown) break; // continue with waiting
		}
	case NGSNone:
	case NGSCreating:
	case NGSCreate:
	case NGSLogin:
	case NGSEdit:
	case NGSPrepareSide:
		OnButtonClicked(IDC_AUTOCANCEL);
		return;
	case NGSDebriefing:
	case NGSDebriefingOK:
		state = LocalizeString(IDS_SESSION_DEBRIEFING);
		// continue with waiting
		break;
	case NGSTransferMission:
	case NGSLoadIsland:
		state = LocalizeString(IDS_SESSION_SETUP);
		// continue with waiting
		break;
	case NGSBriefing:
		state = LocalizeString(IDS_SESSION_BRIEFING);
		// continue with waiting
		break;
	case NGSPlay:
		state = LocalizeString(IDS_SESSION_PLAY);
		// continue with waiting
		break;
	}
	if (state.GetLength() > 0)
	{
		char buffer[256];
		sprintf(buffer, LocalizeString(IDS_CLIENT_WAIT_TITLE), (const char *)state);
		CStatic *text = dynamic_cast<CStatic *>(GetCtrl(IDC_CLIENT_WAIT_TITLE));
		if (text) text->SetText(buffer);
	}

	UpdatePlayers();
	Display::OnSimulate(vehicle);
}

///////////////////////////////////////////////////////////////////////////////
// Login display

Control *DisplayLogin::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
#ifdef _WIN32
	if (idc == IDC_LOGIN_USER)
	{
//		CCombo *combo = new CCombo(this, idc, cls);
		C3DListBox *ctrl = new C3DListBox(this, idc, cls);

		_finddata_t info;
		long h = _findfirst("Users\\*.*", &info);
		if (h != -1)
		{
			do
			{
				if
				(
					(info.attrib & _A_SUBDIR) != 0 &&
					info.name[0] != '.'
				)
				{
					ctrl->AddString(info.name);
				}
			}
			while (0==_findnext(h, &info));
			_findclose(h);
		}
		ctrl->SortItems();
		ctrl->SetCurSel(0);
		for (int i=0; i<ctrl->GetSize(); i++)
		{
			if (Glob.header.playerName == ctrl->GetText(i))
				ctrl->SetCurSel(i);
		}
		return ctrl;
	}
#endif
	return Display::OnCreateCtrl(type, idc, cls);
}

void DisplayLogin::OnButtonClicked(int idc)
{
	switch (idc)
	{
	case IDC_LOGIN_NEW:
	case IDC_LOGIN_EDIT:
		Exit(idc);
		break;
	case IDC_OK:
	case IDC_CANCEL:
		{
			_exit = idc;
			bool canDestroy = CanDestroy();
			_exit = -1;
			if (!canDestroy) break;
			_exitWhenClose = idc;
			ControlObjectContainerAnim *ctrl =
				dynamic_cast<ControlObjectContainerAnim *>(GetCtrl(IDC_LOGIN_NOTEBOOK));
			if (ctrl) ctrl->Close();
		}
		break;
	case IDC_LOGIN_DELETE:
		CreateMsgBox
		(
			MB_BUTTON_OK | MB_BUTTON_CANCEL,
			LocalizeString(IDS_SURE),
			IDD_MSG_DELETEPLAYER
		);
		break;
	default:
		Display::OnButtonClicked(idc);
		break;
	}
}

void DisplayLogin::OnCtrlClosed(int idc)
{
	if (idc == IDC_LOGIN_NOTEBOOK)
		Exit(_exitWhenClose);
	else
		Display::OnCtrlClosed(idc);
}

void DisplayLogin::OnChildDestroyed(int idd, int exit)
{
	switch (idd)
	{
		case IDD_MSG_DELETEPLAYER:
			Display::OnChildDestroyed(idd, exit);
			if (exit == IDC_OK)
			{
				C3DListBox *ctrl = dynamic_cast<C3DListBox *>(GetCtrl(IDC_LOGIN_USER));
				int index = ctrl->GetCurSel();
				if (index >= 0)
				{
					const char *name = ctrl->GetText(index);
					char buffer[256];
					sprintf(buffer, "Users\\%s", name);
					DeleteDirectoryStructure(buffer);
					ctrl->DeleteString(index);
					ctrl->SetCurSel(0);
				}
			}
			break;
		default:
			Display::OnChildDestroyed(idd, exit);
			break;
	}
}

bool DisplayLogin::CanDestroy()
{
	if (!Display::CanDestroy()) return false;

	if (_exit == IDC_OK)
	{
		C3DListBox *ctrl = dynamic_cast<C3DListBox *>(GetCtrl(IDC_LOGIN_USER));
		if (ctrl->GetSize() == 0 || ctrl->GetCurSel() < 0)
		{
			CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_PLAYERNAME_EMPTY));
			return false;
		}
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////
// New user display

CHead::CHead(ControlsContainer *parent, int idc, const ParamEntry &cls)
: ControlObject(parent, idc, cls)
{
	_lastSimulation = Glob.uiTime;
	_woman = false;

	_manShape = _shape;
	_manHeadType = new HeadType();
	_manHeadType->Load(Pars>>"CfgMimics">>"HeadPreview");
	_manHeadType->InitShape(cls, _shape);
	_manHead = new Head(*_manHeadType, _shape);

	RString FindShape(RString name);
	_womanShape = Shapes.New(FindShape(cls >> "modelWoman"), false, false);
	_womanHeadType = new HeadType();
	_womanHeadType->Load(Pars>>"CfgMimics">>"HeadPreview");
	_womanHeadType->InitShape(cls, _womanShape);
	_womanHead = new Head(*_womanHeadType, _womanShape);
}

void CHead::OnDraw(float alpha)
{
	LODShapeWithShadow *oldShape = GetShape();
	SetShape(GetHeadShape());	
	ControlObject::OnDraw(alpha);
	SetShape(oldShape);	
}

void CHead::Animate(int level)
{
	GetHead()->Animate(*GetHeadType(), GetHeadShape(), level, false, M3Identity, false);
}

void CHead::Deanimate(int level)
{
	GetHead()->Deanimate(*GetHeadType(), GetHeadShape(), level, false, M3Identity, false);
}

void CHead::Simulate()
{
	UITime time = Glob.uiTime;
	const float t0 = 4.0;
	float oldT = fastFmod(_lastSimulation.toFloat(), t0);
	float newT = fastFmod(time.toFloat(), t0);
	float angle = (H_PI * 2.0 / t0) * newT;
	Matrix3 orient(MRotationY, angle);
	float scale = Scale();
	SetOrientation(orient);
	SetScale(scale);

	const float tChange = 0.5 * t0;
	if (oldT < tChange && newT >= tChange)
	{
		int size = (Pars >> "CfgMimics" >> "States").GetEntryCount();
		int i = toIntFloor(size * GRandGen.RandomValue());
		GetHead()->SetMimic((Pars >> "CfgMimics" >> "States").GetEntry(i).GetName());
	}

	GetHead()->Simulate(*GetHeadType(), time - _lastSimulation, SimulateVisibleNear, false);
	_lastSimulation = time;
}

void CHead::SetFace(RString name)
{
	const ParamEntry *cls = (Pars >> "CfgFaces").FindEntry(name);
	if (!cls)
	{
		cls = (Pars >> "CfgFaces").FindEntry("Default");
//		WarningMessage("Unknown face: %s", (const char *)name);
		if (!cls) return;
	}
	
	_woman = false;
	if (cls->FindEntry("woman")) _woman = *cls >> "woman";

	GetHead()->SetFace(*GetHeadType(), _woman, GetHeadShape(), name);
}

void CHead::SetGlasses(RString name)
{
	GetHead()->SetGlasses(*GetHeadType(), GetHeadShape(), name);
}

void CHead::AttachWave(AbstractWave *wave, float freq)
{
	GetHead()->AttachWave(wave, freq);
}

DisplayNewUser::DisplayNewUser(ControlsContainer *parent, RString name, bool edit)
: Display(parent)
{
	_edit = edit;
	if (edit)
	{
		_name = name;
		_face = Glob.header.playerFace;
		_glasses = Glob.header.playerGlasses;
		_speaker = Glob.header.playerSpeaker;
		_pitch = Glob.header.playerPitch;

		RString filename = RString("Users\\") + name + RString("\\UserInfo.cfg");
		if (QIFStream::FileExists(filename))
		{
			ParamFile cfg;
			cfg.Parse(filename);
			const ParamEntry *identity = cfg.FindEntry("Identity");
			if (identity)
			{
				_face = (*identity) >> "face";
				if (identity->FindEntry("glasses"))
					_glasses = (*identity) >> "glasses";
				_speaker = (*identity) >> "speaker";
				_pitch = (*identity) >> "pitch";
				if (identity->FindEntry("squad"))
					_squad = (*identity) >> "squad";
			}
		}
	}
	else
	{
		_name = "PLAYER 1";
		_face = "Default";
		_glasses = "None";
		_speaker = (Pars >> "CfgVoice" >> "voices")[0];
		_pitch = 1.0;
	}

	_doPreview = false;
	
	Load("RscDisplayNewUser");
	_head->SetFace(_face);
	_head->SetGlasses(_glasses);

#if !_VERIFY_KEY
	GetCtrl(IDC_NEW_USER_SQUAD)->ShowCtrl(false);
	GetCtrl(IDC_NEW_USER_SQUAD_TEXT)->ShowCtrl(false);
#endif
}

Control *DisplayNewUser::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
	case IDC_NEW_USER_TITLE:
		{
			CStatic *text = new CStatic(this, idc, cls);
			if (_edit)
				text->SetText(LocalizeString(IDS_NEWUSER_TITLE2));
			else
				text->SetText(LocalizeString(IDS_NEWUSER_TITLE1));
			return text;
		}
	case IDC_NEW_USER_ID:
		{
			C3DStatic *text = new C3DStatic(this, idc, cls);
			char buffer[256];
			RString GetPublicKey();
			sprintf(buffer, LocalizeString(IDS_PLAYER_ID), (const char *)GetPublicKey());
			text->SetText(buffer);
			return text;
		}
	case IDC_NEW_USER_NAME:
		{
			C3DEdit *edit = new C3DEdit(this, idc, cls);
			if (GetLangID() == Korean)
				edit->SetMaxChars(14);
			else
				edit->SetMaxChars(24);
			edit->SetText(_name);
//			edit->EnableCtrl(!_edit);
			return edit;
		}
	case IDC_NEW_USER_FACE:
		{
			C3DListBox *ctrl = new C3DListBox(this, idc, cls);
			int sel = 0;
			const ParamEntry &list = Pars >> "CfgFaces";
			for (int i=0; i<list.GetEntryCount(); i++)
			{
				const ParamEntry &entry = list.GetEntry(i);
				if (entry.FindEntry("disabled")) continue;
/*
				bool woman = false;
				if (entry.FindEntry("woman")) woman = entry >> "woman";
				if (woman) continue;
*/
				RString name = entry.GetName();
				int index = ctrl->AddString(entry >> "name");
				ctrl->SetData(index, name);
				if (name == _face) sel = index;
			}
			ctrl->SetCurSel(sel);
			return ctrl;
		}
	case IDC_NEW_USER_GLASSES:
		{
			C3DListBox *ctrl = new C3DListBox(this, idc, cls);
			int sel = 0;
			const ParamEntry &list = Pars >> "CfgGlasses";
			for (int i=0; i<list.GetEntryCount(); i++)
			{
				const ParamEntry &entry = list.GetEntry(i);
				RString name = entry.GetName();
				int index = ctrl->AddString(entry >> "name");
				ctrl->SetData(index, name);
				if (name == _glasses) sel = index;
			}
			ctrl->SetCurSel(sel);
			return ctrl;
		}
	case IDC_NEW_USER_SPEAKER:
		{
			C3DListBox *ctrl = new C3DListBox(this, idc, cls);
			int sel = 0;
			const ParamEntry &list = Pars >> "CfgVoice";
			const ParamEntry &array = list >> "voices";
			for (int i=0; i<array.GetSize(); i++)
			{
				RString name = array[i];
				const ParamEntry &entry = list >> name;
				int index = ctrl->AddString(entry >> "name");
				ctrl->SetData(index, name);
				if (name == _speaker) sel = index;
			}
			ctrl->SetCurSel(sel);
			return ctrl;
		}
	case IDC_NEW_USER_PITCH:
		{
			C3DSlider *ctrl = new C3DSlider(this, idc, cls);
			ctrl->SetRange(0.8, 1.2);
			ctrl->SetThumbPos(_pitch);
			ctrl->SetSpeed(0.02, 0.1);
			return ctrl;
		}
	case IDC_NEW_USER_SQUAD:
		{
			C3DEdit *edit = new C3DEdit(this, idc, cls);
			edit->SetText(_squad);
			return edit;
		}
	default:
		return Display::OnCreateCtrl(type, idc, cls);
	}
}

ControlObject *DisplayNewUser::OnCreateObject(int type, int idc, const ParamEntry &cls)
{
	if (idc == IDC_NEW_USER_HEAD)
	{
		_head = new CHead(this, idc, cls);
		return _head;
	}
	else
		return Display::OnCreateObject(type, idc, cls);
}

void DisplayNewUser::OnButtonClicked(int idc)
{
	switch (idc)
	{
	case IDC_OK:
	case IDC_CANCEL:
		{
			_exit = idc;
			bool canDestroy = CanDestroy();
			_exit = -1;
			if (!canDestroy) break;
			_exitWhenClose = idc;
			_head->ShowCtrl(false);
			ControlObjectContainerAnim *ctrl =
				dynamic_cast<ControlObjectContainerAnim *>(GetCtrl(IDC_NEW_USER_NOTEBOOK));
			if (ctrl) ctrl->Close();
		}
		break;
	default:
		Display::OnButtonClicked(idc);
		break;
	}
}

void DisplayNewUser::OnSliderPosChanged(int idc, float pos)
{
	if (idc == IDC_NEW_USER_PITCH)
	{
		C3DListBox *ctrl = dynamic_cast<C3DListBox *>(GetCtrl(IDC_NEW_USER_SPEAKER));
		if (ctrl)
		{
			int sel = ctrl->GetCurSel();
			if (sel >= 0)
			{
				_previewSpeaker = ctrl->GetData(sel);
				_previewPitch = pos;
				_doPreview = true;
				_previewTime = GlobalTickCount() + 200;
			}
		}
	}
	else
		Display::OnSliderPosChanged(idc, pos);
}

void DisplayNewUser::OnLBSelChanged(int idc, int curSel)
{
	if (idc == IDC_NEW_USER_SPEAKER)
	{
		if (curSel >= 0)
		{
			C3DSlider *slider = dynamic_cast<C3DSlider *>(GetCtrl(IDC_NEW_USER_PITCH));
			C3DListBox *ctrl = dynamic_cast<C3DListBox *>(GetCtrl(IDC_NEW_USER_SPEAKER));
			if (slider && ctrl)
			{
				_previewSpeaker = ctrl->GetData(curSel);
				_previewPitch = slider->GetThumbPos();
				_doPreview = true;
				_previewTime = GlobalTickCount() + 200;
			}
		}
	}
	else if (idc == IDC_NEW_USER_FACE)
	{
		C3DListBox *ctrl = dynamic_cast<C3DListBox *>(GetCtrl(IDC_NEW_USER_FACE));
		if (ctrl && _head && curSel >= 0)
		{
			_head->SetFace(ctrl->GetData(curSel));
		}
	}
	else if (idc == IDC_NEW_USER_GLASSES)
	{
		C3DListBox *ctrl = dynamic_cast<C3DListBox *>(GetCtrl(IDC_NEW_USER_GLASSES));
		if (ctrl && _head && curSel >= 0)
		{
			_head->SetGlasses(ctrl->GetData(curSel));
		}
	}
	else
		Display::OnSliderPosChanged(idc, curSel);
}

void DisplayNewUser::OnObjectMoved(int idc, Vector3Par offset)
{
	if (idc == IDC_NEW_USER_NOTEBOOK)
	{
		Assert(dynamic_cast<Control3D *>(GetCtrl(IDC_NEW_USER_HEAD_AREA)));
		Control3D *area = static_cast<Control3D *>(GetCtrl(IDC_NEW_USER_HEAD_AREA));
		Vector3 pos = area->GetCenter();
		float coef = _head->Position().Z() / pos.Z();
		_head->SetPosition(_head->Position() + coef * offset);
	}
	Display::OnObjectMoved(idc, offset);
}

void DisplayNewUser::OnCtrlClosed(int idc)
{
	if (idc == IDC_NEW_USER_NOTEBOOK)
		Exit(_exitWhenClose);
	else
		Display::OnCtrlClosed(idc);
}

void DisplayNewUser::OnSimulate(EntityAI *vehicle)
{
	if (_head) _head->Simulate();

	Display::OnSimulate(vehicle);

	if (_previewSpeech && _previewSpeech->IsTerminated()) _previewSpeech = NULL;
	if (!_previewSpeech && _doPreview && GlobalTickCount() >= _previewTime)
	{
		Preview(_previewSpeaker, _previewPitch);
		_doPreview = false;
	}
}

void DisplayNewUser::Preview(RString speaker, float pitch)
{
	int index = Glob.config.singleVoice ? 1 : 0; 
	RString dir = (Pars >> "CfgVoice" >> speaker >> "directories")[index];
	RString word = Pars >> "CfgVoice" >> "preview";
	RString name = RString("voice\\") + dir + word + RString(".wss");

	_previewSpeech = GSoundScene->OpenAndPlayOnce2D(name, 1, pitch, false);
	if (_previewSpeech)
	{
		_previewSpeech->SetKind(WaveSpeech);
		_head->AttachWave(_previewSpeech, pitch);
	}
}

bool DisplayNewUser::CanDestroy()
{
	if (!Display::CanDestroy()) return false;

#ifdef _WIN32
	if (_exit == IDC_OK)
	{
		C3DEdit *edit = dynamic_cast<C3DEdit *>(GetCtrl(IDC_NEW_USER_NAME));
		const char *name = edit->GetText();
		if (!name || !*name)
		{
			CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_PLAYERNAME_EMPTY));
			return false;
		}
		if (strcspn(name, "\\/:*?\"<>|") < strlen(name))
		{
			CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_PLAYERNAME_INVALID));
			return false;
		}

		if (!_edit || stricmp(name, _name) != 0)
		{
			_finddata_t info;
			RString fullname = RString("Users\\") + RString(name);
			long check = _findfirst(fullname, &info);
			if (check != -1)
			{
				// player already exist
				CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_PLAYER_EXIST));
				_findclose(check);
				return false;
			}
		}
	}
#endif

	return true;
}

///////////////////////////////////////////////////////////////////////////////
// Mission, intro, outro displays

bool BreakIntro()
{
	if (GInput.keysToDo[DIK_ESCAPE])
	{
		GInput.keysToDo[DIK_ESCAPE] = false;
		return true;
	}
	if (GInput.keysToDo[DIK_SPACE])
	{
		GInput.keysToDo[DIK_SPACE] = false;
		return true;
	}
/*
	if (GInput.mouseLToDo)
	{
		GInput.mouseLToDo = false;
		return true;
	}
	if (GInput.mouseRToDo)
	{
		GInput.mouseRToDo = false;
		return true;
	}
*/
	return false;
}

void ProcessMouse(DWORD timeDelta = 1);

//! HUD hint display
/*!
	This variant stops simulation and waits for user input.
*/
class DisplayHintC : public Display
{
protected:
	//@{
	//! fast access to control
	CStatic *_background;
	CStatic *_hint;
	CActiveText *_button;
	//@}

public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayHintC(ControlsContainer *parent, RString hint);
	void OnSimulate(EntityAI *vehicle);
	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	//! returns displayed hint
	RString GetHint() {return _hint->GetText();}
};

DisplayHintC::DisplayHintC(ControlsContainer *parent, RString hint)
: Display(parent)
{
	_enableSimulation = false;
	Load(Res >> "RscDisplayHintC");
	
	_hint->SetText(hint);
	float h = _hint->GetTextHeight();
	float offset = _button->Y() - _background->Y() - _background->H();
	float dh = _background->H() - _hint->H();
	_hint->SetPos
	(
		_hint->X(), _hint->Y(), _hint->W(), h
	);
	_background->SetPos
	(
		_background->X(), _background->Y(), _background->W(), h + dh
	);
	_button->SetPos
	(
		_button->X(), _background->Y() + _background->H() + offset, _button->W(), _button->H()
	);
}

Control *DisplayHintC::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
	case IDC_HINTC_BG:
		_background = new CStatic(this, idc, cls);
		return _background;
	case IDC_HINTC_HINT:
		_hint = new CStatic(this, idc, cls);
		_hint->EnableCtrl(false);
		return _hint;
	case IDC_CANCEL:
		_button = new CActiveText(this, idc, cls);
		return _button;
	default:
		return Display::OnCreateCtrl(type, idc, cls);
	}
}

void DisplayHintC::OnSimulate(EntityAI *vehicle)
{
	if (GInput.keysToDo[DIK_SPACE])
	{
		GInput.keysToDo[DIK_SPACE];
		Exit(IDC_CANCEL);
	}

	Display::OnSimulate(vehicle);
}

DisplayMission::DisplayMission(ControlsContainer *parent, bool editor, bool erase, bool load)
	: Display(parent)
{
	_editor = editor;
	Load("RscDisplayMission");
	_compass->ShowCtrl(false);
	_watch->ShowCtrl(false);

	SetCursor(NULL);

	// update mouse state to avoid cursor movement after EnableSimulation
	ProcessMouse();
	GInput.cursorX = 0;
	GInput.cursorY = 0;
	GInput.cursorMovedX = 0;
	GInput.cursorMovedY = 0;
	GInput.cursorMovedZ = 0;

	InitUI();
	GStats.OnMissionStart();

	if (erase)
	{
		RString dir = GetSaveDirectory();

		RString name = dir + RString("autosave.fps");
		unlink(name);

		name = dir + RString("save.fps");
		unlink(name);
	}
	
	if (IsCampaign() && !load)
	{
		RString displayName = Localize(CurrentTemplate.intel.briefingName);
		if (displayName.GetLength() == 0)
			displayName = RString(Glob.header.filename) + RString(".") + RString(Glob.header.worldname);
		void AddMission(RString campaign, RString battle, RString mission, RString displayName);
		AddMission(CurrentCampaign, CurrentBattle, CurrentMission, displayName);
	}

	ContinueSaved = editor || GWorld->GetMode() == GModeNetware;

	GWorld->ForceEnd(false);

	ShowCinemaBorder(true);

	// Czech CD Protection added
/*
#if _CZECH
	PerformRandomRoxxeTest_000(CDDrive);
#endif
*/
}

ControlObject *DisplayMission::OnCreateObject(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
	case IDC_MAP_COMPASS:
		_compass = new Compass(this, idc, cls);
		return _compass;
	case IDC_MAP_WATCH:
		_watch = new Watch(this, idc, cls);
		return _watch;
	default:
		return Display::OnCreateObject(type, idc, cls);
	}
}

void DisplayMission::ShowHint(RString hint)
{
	CreateChild(new DisplayHintC(this, hint));
}

void DisplayMission::InitUI()
{
	// main map
	GWorld->CreateMainMap();
	DisplayMap *map = dynamic_cast<DisplayMap *>(GWorld->Map());
	if (map)
	{
		const ParamEntry *cls = ExtParsMission.FindEntry("showMap");
		map->ShowMap(!cls ? true : (*cls));
		cls = ExtParsMission.FindEntry("showWatch");
		map->ShowWatch(!cls ? true : (*cls));
		cls = ExtParsMission.FindEntry("showCompass");
		map->ShowCompass(!cls ? true : (*cls));
		cls = ExtParsMission.FindEntry("showNotepad");
		map->ShowNotepad(!cls ? true : (*cls));
		cls = ExtParsMission.FindEntry("showGPS");
		map->ShowGPS(!cls ? false : (*cls));
	}

	// in game UI
	AbstractUI *ui = GWorld->UI();
	if (ui)
	{
		const ParamEntry *cls = ExtParsMission.FindEntry("showHUD");
		bool show = !cls ? true : (*cls);
		ui->ShowAll(show);
	}
	
	// chat
	GChatList.Clear();
	GChatList.SetRect(0.05, 0.68, 0.7, 0.02);
	GChatList.SetRows(6);
}

static void SaveContinue()
{
	if (!ContinueSaved && GWorld->GetRealPlayer() && !GWorld->GetRealPlayer()->IsDammageDestroyed())
	{
		char buffer[256];
		sprintf(buffer, "%scontinue.fps", (const char *)GetSaveDirectory());
		GWorld->SaveBin(buffer, IDS_AUTOSAVE_GAME);
	}
	ContinueSaved = true;
}

/*!
\patch 1.78 Date 7/16/2002 by Jirka
- Fixed: User dialog doesn't disappear when mission end
*/

DisplayMission::~DisplayMission()
{
	GChatList.Clear();
	GChatList.SetRect(0.05, 0.02, 0.9, 0.02);
	GChatList.SetRows(4);
	// SaveContinue();
	if (GWorld)
	{
		GWorld->UnloadSounds();
		// FIX
		GWorld->DestroyUserDialog();
	}
}

/*!
\patch 1.75 Date 2/11/2002 by Jirka
- Added: exit.sqs script launched if mission ends 
*/

void DisplayMission::OnSimulate(EntityAI *vehicle)
{
#if _ENABLE_CHEATS
	if (GInput.GetCheat1ToDo(DIK_C))
	{
		CreateChild(new DisplayDebug(this,GWorld->GetGameState()));
		return;
	}
	/*
	if (GInput.GetCheat1ToDo(DIK_V))
	{
		CreateChild(new DisplayDebug(this,GWorld->GetGameState(),"RscDisplayDiag"));
		return;
	}
	*/
#endif

	_compass->ShowCtrl(GWorld->HasCompass());
	_watch->ShowCtrl(GWorld->HasWatch());
	
 	if (GWorld->GetMode() == GModeNetware)
	{
		NetworkGameState gameState = GetNetworkManager().GetGameState();
		if (gameState < NGSPlay)
		{
			// end mission
//			GStats.Update();
			GWorld->DestroyMap(IDC_OK);
//			GWorld->EnableSimulation(false);
			Exit(IDC_CANCEL);
			return;
		}

		if (GInput.GetActionToDo(UANetworkPlayers))
		{
			CreateChild(new DisplayMPPlayers(this));
		}
	}
	else
	{
		if (GWorld->GetEndMode() == EMKilled && GWorld->CameraOn())
		{
			if (GWorld->IsEndDialogEnabled())
			{
				if (AutoTest)
				{
					GWorld->DestroyMap(IDC_OK);
					Exit(IDC_MAIN_QUIT);
				}
				else CreateChild(new DisplayMissionEnd(this));
			}
			return;
		}
	}

	if
	(
		GWorld->GetEndMode() != EMContinue && GWorld->GetEndMode() != EMKilled &&
		(
			(!GWorld->GetCameraEffect() && !GWorld->GetTitleEffect()) || GWorld->IsEndForced()
		) 
	)
	{
#if _ENABLE_DATADISC
		RString exitScript = GetMissionDirectory() + RString("exit.sqs");
		if (QIFStreamB::FileExist(exitScript))
		{
			float end = GWorld->GetEndMode() - EMLoser;
			Script *script = new Script("exit.sqs", GameValue(end), INT_MAX);
			GWorld->AddScript(script);
			GWorld->SimulateScripts();
		}
#endif

		// end mission
//		GStats.Update();
		GWorld->DestroyMap(IDC_OK);
		if (AutoTest)
			Exit(IDC_MAIN_QUIT);
		else
			Exit(IDC_CANCEL);
	}
	else if (GInput.GetKeyToDo(DIK_ESCAPE))
	{
		CreateChild(new DisplayInterrupt(this));
	}
}

bool DisplayMission::RetryMission()
{
	if (GStats._mission._lives == 0) return false;

	// retry mission
	RString name = GetSaveDirectory() + RString("autosave.fps");
	if (QIFStream::FileExists(name))
	{
		GWorld->LoadBin(name, IDS_LOAD_GAME);
		if (GStats._mission._lives > 0)
		{
			GStats._mission._lives--;
			// TODO:?? update save ??
		}
	}
	else
	{
		GWorld->SwitchLandscape(GetWorldName(Glob.header.worldname));
		GStats.ClearMission();
		// if (IsCampaign())
		{
			// load variables
			GameState *gstate = GWorld->GetGameState();
			AutoArray<GameVariable> &variables = GStats._campaign._variables;
			for (int i=0; i<variables.Size(); i++)
			{
				GameVariable &var = variables[i];
				gstate->VarSet(var._name, var._value, var._readOnly);
			}
		}
		GWorld->ActivateAddons(CurrentTemplate.addOns);
		GWorld->InitGeneral(CurrentTemplate.intel);
		GWorld->InitVehicles(GModeArcade, CurrentTemplate);
		int lives = GStats._mission._lives;
		if (GStats._mission._lives > 0)
			GStats._mission._lives = lives - 1;

		RString weapons = GetSaveDirectory() + RString("weapons.cfg");
		if (QIFStream::FileExists(weapons))
		{
			WeaponsInfo info;
			info.Load(weapons);
			info.Apply();
		}
	}

	GWorld->DestroyMap(IDC_OK);
	InitUI();
	GStats.OnMissionStart();

	// FIX
	GWorld->DestroyUserDialog();

	return true;
}

/*!
	\patch 1.01 Date 06/11/2001 by Jirka
	- Fixed: "End" button in "Player killed" screen was handled bad
	\patch_internal 1.01 Date 06/11/2001 by Jirka
	- when button 'End' was pressed dialog exits with IDC_CANCEL
	- the right exit code is IDC_MAIN_QUIT
*/

void DisplayMission::OnChildDestroyed(int idd, int exit)
{
	switch (idd)
	{
	case IDD_MISSION_END:
		Display::OnChildDestroyed(idd, exit);
		if (exit == IDC_ME_RETRY)
		{
			if (!RetryMission())
			{
				// end mission
//				GStats.Update();
				GWorld->DestroyMap(IDC_OK);
				Exit(IDC_CANCEL);
			}
		}
		else if (exit == IDC_ME_LOAD)
		{
			RString name = GetSaveDirectory() + RString("save.fps");
			GWorld->LoadBin(name, IDS_LOAD_GAME);
			GStats.OnMissionStart();
		}
		else if (exit == IDC_CANCEL)
		{
			// end mission
			GWorld->DestroyMap(IDC_OK);
			// fixed: was Exit(IDC_CANCEL)
			Exit(IDC_MAIN_QUIT);
		}
		break;
	case IDD_INTERRUPT:
		Display::OnChildDestroyed(idd, exit);
		if (exit == IDC_INT_ABORT)
		{
			// abort mission
			SaveContinue();
			GWorld->DestroyMap(IDC_OK);
			Exit(IDC_MAIN_QUIT);
		}
		else if (exit == IDC_INT_RETRY)
		{
			if (!RetryMission())
			{
				// abort mission
				SaveContinue();
				GWorld->DestroyMap(IDC_OK);
				Exit(IDC_MAIN_QUIT);
			}
		}
		else if (exit == IDC_INT_SAVE)
		{
			RString name = GetSaveDirectory() + RString("save.fps");
			GWorld->SaveBin(name, IDS_SAVE_GAME);
		}
		else if (exit == IDC_INT_LOAD)
		{
			// FIX
			GWorld->DestroyUserDialog();

			RString name = GetSaveDirectory() + RString("save.fps");
			GWorld->LoadBin(name, IDS_LOAD_GAME);
			GStats.OnMissionStart();
		}
		break;
	case IDD_HINTC:
		{
			DisplayHintC *child = static_cast<DisplayHintC *>(_child.GetRef());
			GWorld->UI()->ShowHint(child->GetHint());
		}
		Display::OnChildDestroyed(idd, exit);
		break;		
	default:
		Display::OnChildDestroyed(idd, exit);
		break;		
	}
}

DisplayCutscene::DisplayCutscene(ControlsContainer *parent)
	: Display(parent)
{
	SetCursor(NULL);
//	GWorld->EnableSimulation(true);
	ShowCinemaBorder(true);
}

DisplayCutscene::~DisplayCutscene()
{
	GWorld->UnloadSounds();
}

void DisplayCutscene::OnSimulate(EntityAI *vehicle)
{
#if _ENABLE_CHEATS
	if (GInput.GetCheat1ToDo(DIK_C))
	{
		CreateChild(new DisplayDebug(this,GWorld->GetGameState()));
		return;
	}
	/*
	if (GInput.GetCheat1ToDo(DIK_V))
	{
		CreateChild(new DisplayDebug(this,GWorld->GetGameState(),"RscDisplayDiag"));
		return;
	}
	*/
#endif

	if (BreakIntro() || GWorld->GetEndMode() != EMContinue/* || !GLOB_WORLD->GetCameraEffect() */)
	{
//		GWorld->EnableSimulation(false);
		Exit(IDC_CANCEL);
	}
}

DisplayIntro::DisplayIntro(ControlsContainer *parent, DisplayIntro::NoInit)
	: DisplayCutscene(parent)
{
	Load("RscDisplayIntro");

	Init();
}

DisplayIntro::DisplayIntro(ControlsContainer *parent)
	: DisplayCutscene(parent)
{
	Load("RscDisplayIntro");

	ParseIntro();

	if (CurrentTemplate.groups.Size() == 0)
	{
//		GWorld->EnableSimulation(false);
		Exit(IDC_CANCEL);
		return;
	}

	GLOB_WORLD->SwitchLandscape(GetWorldName(Glob.header.worldname));
	GWorld->ActivateAddons(CurrentTemplate.addOns);
	GLOB_WORLD->InitGeneral(CurrentTemplate.intel);
	if (!GLOB_WORLD->InitVehicles(GModeIntro, CurrentTemplate))
	{
		Exit(IDC_CANCEL);
		return;
	}

	Init();
}

DisplayIntro::DisplayIntro(ControlsContainer *parent, RString cutscene)
	: DisplayCutscene(parent)
{
	Load("RscDisplayIntro");

	if (cutscene.GetLength() == 0)
	{
//		GWorld->EnableSimulation(false);
		Exit(IDC_CANCEL);
		return;
	}

	if (!ProcessTemplateName(cutscene))
	{
		Exit(IDC_CANCEL);
		return;
	}
	ParseIntro();

	if (CurrentTemplate.groups.Size() == 0)
	{
//		GWorld->EnableSimulation(false);
		Exit(IDC_CANCEL);
		return;
	}

	GLOB_WORLD->SwitchLandscape(GetWorldName(Glob.header.worldname));
	GWorld->ActivateAddons(CurrentTemplate.addOns);
	GLOB_WORLD->InitGeneral(CurrentTemplate.intel);
	if (!GLOB_WORLD->InitVehicles(GModeIntro, CurrentTemplate))
	{
		Exit(IDC_CANCEL);
		return;
	}

	Init();
}

/*!
\patch 1.75 Date 2/4/2002 by Jirka
- Added: script initIntro.sqs is launched on the beginning of intro
\patch 1.82 Date 8/8/2002 by Jirka
- Fixed: initIntro.sqs didn't respond to saved variables
*/

void DisplayIntro::Init()
{
	// main map
	GWorld->CreateMainMap();
	DisplayMap *map = dynamic_cast<DisplayMap *>(GWorld->Map());
	if (map)
	{
		map->ShowMap(true);
		map->ShowWatch(false);
		map->ShowCompass(false);
		map->ShowNotepad(false);
		map->ShowGPS(false);

		map->SetCursor(NULL);
	}

	//if (IsCampaign())
	{
		// load variables
		GameState *gstate = GWorld->GetGameState();
		AutoArray<GameVariable> &variables = GStats._campaign._variables;
		for (int i=0; i<variables.Size(); i++)
		{
			GameVariable &var = variables[i];
			gstate->VarSet(var._name, var._value, var._readOnly);
		}
	}

#if _ENABLE_DATADISC
	RString initScript = GetMissionDirectory() + RString("initintro.sqs");
	if (QIFStreamB::FileExist(initScript))
	{
		Script *script = new Script("initintro.sqs", GameValue(), INT_MAX);
		GWorld->AddScript(script);
		GWorld->SimulateScripts();
	}
#endif
}

DisplayOutro::DisplayOutro(ControlsContainer *parent, EndMode mode)
	: DisplayCutscene(parent)
{
	Load("RscDisplayOutro");

	if (mode == EMContinue || mode == EMKilled) _mode = EMLoser;
	else _mode = mode;

	switch (_mode)
	{
		case EMLoser:
			ParseCutscene("OutroLoose", false);
			break;
		case EMEnd1:
		case EMEnd2:
		case EMEnd3:
		case EMEnd4:
		case EMEnd5:
		case EMEnd6:
			ParseCutscene("OutroWin", false);
			break;
	}

	if (CurrentTemplate.groups.Size() == 0)
	{
//		GWorld->EnableSimulation(false);
		Exit(IDC_CANCEL);
		return;
	}

	GLOB_WORLD->SwitchLandscape(GetWorldName(Glob.header.worldname));
	GWorld->ActivateAddons(CurrentTemplate.addOns);
	GLOB_WORLD->InitGeneral(CurrentTemplate.intel);
	if (!GLOB_WORLD->InitVehicles(GModeIntro, CurrentTemplate))
	{
		Exit(IDC_CANCEL);
		return;
	}
}

DisplayAward::DisplayAward(ControlsContainer *parent, RString cutscene)
	: DisplayCutscene(parent)
{
	Load("RscDisplayAward");

	if (cutscene.GetLength() == 0)
	{
		Exit(IDC_CANCEL);
		return;
	}

	if (!ProcessTemplateName(cutscene))
	{
		Exit(IDC_CANCEL);
		return;
	}
	ParseIntro();

	if (CurrentTemplate.groups.Size() == 0)
	{
		Exit(IDC_CANCEL);
		return;
	}

	GLOB_WORLD->SwitchLandscape(GetWorldName(Glob.header.worldname));
	GWorld->ActivateAddons(CurrentTemplate.addOns);
	GLOB_WORLD->InitGeneral(CurrentTemplate.intel);
	if (!GLOB_WORLD->InitVehicles(GModeIntro, CurrentTemplate))
	{
		Exit(IDC_CANCEL);
		return;
	}
}

DisplayCampaignIntro::DisplayCampaignIntro(ControlsContainer *parent, RString cutscene)
	: DisplayCutscene(parent)
{
	Load("RscDisplayCampaign");

	if (cutscene.GetLength() == 0)
	{
//		GWorld->EnableSimulation(false);
		Exit(IDC_CANCEL);
		return;
	}

	if (!ProcessTemplateName(cutscene))
	{
		Exit(IDC_CANCEL);
		return;
	}
	ParseIntro();

	if (CurrentTemplate.groups.Size() == 0)
	{
//		GWorld->EnableSimulation(false);
		Exit(IDC_CANCEL);
		return;
	}

	GLOB_WORLD->SwitchLandscape(GetWorldName(Glob.header.worldname));
	GWorld->ActivateAddons(CurrentTemplate.addOns);
	GLOB_WORLD->InitGeneral(CurrentTemplate.intel);
	if (!GLOB_WORLD->InitVehicles(GModeIntro, CurrentTemplate))
	{
		Exit(IDC_CANCEL);
		return;
	}

#if _ENABLE_DATADISC
	RString initScript = GetMissionDirectory() + RString("initintro.sqs");
	if (QIFStreamB::FileExist(initScript))
	{
		Script *script = new Script("initintro.sqs", GameValue(), INT_MAX);
		GWorld->AddScript(script);
		GWorld->SimulateScripts();
	}
#endif
}

inline bool OnKilled()
{
/*
	int lives = GStats._mission._lives;
	Assert(lives != 0);
	if (lives > 0)
	{
		lives--;
		if (lives > 0)
		{
			GStats.ClearMission();
			GStats._mission._lives = lives;
			return false;
		}
		else
		{
			GStats._mission._lives = lives;
			return true;
		}
	}
	else if (lives < 0)
	{
		GStats.ClearMission();
	}
	return false;
*/
	Assert(GStats._mission._lives != 0);
	if (GStats._mission._lives > 0)
	{
		GStats._mission._lives--;
		return GStats._mission._lives == 0;
	}
	else
		return false;
}

DisplayMissionEnd::DisplayMissionEnd(ControlsContainer *parent/*, bool editor*/)
	: Display(parent)
{
	int quotations = (IDS_QUOTE_LAST - IDS_QUOTE_1) / 2 + 1;
	_quotation = toIntFloor(quotations * GRandGen.RandomValue());
	Load("RscDisplayMissionEnd");

	GetCtrl(IDC_ME_LOAD)->ShowCtrl(false);
	GetCtrl(IDC_ME_RETRY)->ShowCtrl(false);
	if (GWorld->GetMode() != GModeNetware)
	{
		RString name = GetSaveDirectory() + RString("save.fps");
		if (QIFStream::FileExists(name))
			GetCtrl(IDC_ME_LOAD)->ShowCtrl(true);
		GetCtrl(IDC_ME_RETRY)->ShowCtrl(true);
	}
	GInput.ChangeGameFocus(+1);
}

DisplayMissionEnd::~DisplayMissionEnd()
{
	GInput.ChangeGameFocus(-1);
}

Control *DisplayMissionEnd::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
		case IDC_ME_SUBTITLE:
			{
				CStatic *text = new CStatic(this, idc, cls);
				int lives = GStats._mission._lives;
				Assert(lives != 0);
				if (lives <= 0)
				{
					text->SetText("");
				}
				else if (lives <= 1)
				{
					text->SetText(LocalizeString(IDS_LIVES_1));
				}
				else if (lives <= 3)
				{
					char buffer[256];
					sprintf
					(
						buffer,
						LocalizeString(IDS_LIVES_3),
						lives
					);
					text->SetText(buffer);
				}
				else
				{
					char buffer[256];
					sprintf
					(
						buffer,
						LocalizeString(IDS_LIVES_MORE),
						lives
					);
					text->SetText(buffer);
				}
				return text;
			}
		case IDC_ME_QUOTATION:
			{
				CStatic *text = new CStatic(this, idc, cls);
				text->SetText(RString("\"") + LocalizeString(IDS_QUOTE_1 + 2 * _quotation) + RString("\""));
				text->EnableCtrl(false);
				return text;
			}
		case IDC_ME_AUTHOR:
			{
				CStatic *text = new CStatic(this, idc, cls);
				text->SetText(LocalizeString(IDS_AUTHOR_1 + 2 * _quotation));
				return text;
			}
	}
	return Display::OnCreateCtrl(type, idc, cls);
}

void DisplayMissionEnd::OnButtonClicked(int idc)
{
	Exit(idc);
}

#if _ENABLE_CHEATS
DisplayDebug::DisplayDebug(ControlsContainer *parent, GameState *gs, const char *cls)
: Display(parent)
{
	_current = -1;
	_gs = gs;

	Load(cls);

	_enableSimulation = false;
	_enableDisplay = true;

	GInput.ChangeGameFocus(+1);
}

DisplayDebug::~DisplayDebug()
{
	GInput.ChangeGameFocus(-1);
}


const int DebugConsoleExpRows = 4;
struct DebugConsoleInfo
{
	AutoArray<RString> history;
	RString exp[4];
};

AutoArray<RString> DebugConsoleLog;
DebugConsoleInfo GDebugConsoleInfo;

void DisplayDebug::UpdateLog(CListBox *lbox)
{
	lbox->ClearStrings();
	int n = DebugConsoleLog.Size();
	for (int i=0; i<n; i++)
	{
		lbox->AddString(DebugConsoleLog[i]);
	}
	lbox->SetCurSel(n - 1);
}

Control *DisplayDebug::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
	case IDC_DEBUG_LOG:
		{
			CListBox *lbox = new CListBox(this, idc, cls);
			UpdateLog(lbox);
			return lbox;
		}
	case IDC_DEBUG_EXP:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			_current = GDebugConsoleInfo.history.Size() - 1;
			edit->SetText(_current >= 0 ? GDebugConsoleInfo.history[_current] : "");
			return edit;
		}
	case IDC_DEBUG_EXP1:
	case IDC_DEBUG_EXP2:
	case IDC_DEBUG_EXP3:
	case IDC_DEBUG_EXP4:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			edit->SetText(GDebugConsoleInfo.exp[idc - IDC_DEBUG_EXP1]);
			return edit;
		}
	case IDC_DEBUG_RES1:
	case IDC_DEBUG_RES2:
	case IDC_DEBUG_RES3:
	case IDC_DEBUG_RES4:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			edit->EnableCtrl(false);
			return edit;
		}
	default:
		return Display::OnCreateCtrl(type, idc, cls);
	}
}

void DisplayDebug::OnButtonClicked(int idc)
{
	switch (idc)
	{
	case IDC_DEBUG_APPLY:
	case IDC_OK:
		// execute expression
		{
			CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_DEBUG_EXP));
			if (edit)
			{
				RString exp = edit->GetText();
				GameState *gstate = GWorld->GetGameState();
				if (!gstate->CheckExecute(edit->GetText()))
				{
					FocusCtrl(IDC_DEBUG_EXP);
					CreateMsgBox(MB_BUTTON_OK, gstate->GetLastErrorText());
					edit->SetCaretPos(gstate->GetLastErrorPos());
					return;
				}
				gstate->Execute(exp);

				CListBox *lbox = dynamic_cast<CListBox *>(GetCtrl(IDC_DEBUG_LOG));
				if (lbox) UpdateLog(lbox);
			}
		}
		// continue
	case IDC_CANCEL:
		{
			CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_DEBUG_EXP));
			Assert(edit);
			RString text = edit->GetText();
			int last = GDebugConsoleInfo.history.Size() - 1;
			if (last < 0 || GDebugConsoleInfo.history[last] != text) GDebugConsoleInfo.history.Add(text);
		}
		for (int i=0; i<DebugConsoleExpRows; i++)
		{
			CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_DEBUG_EXP1 + i));
			Assert(edit);
			GDebugConsoleInfo.exp[i] = edit->GetText();
		}
		break;
	}
	
	Display::OnButtonClicked(idc);
}

bool DisplayDebug::OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	if (this->GetFocused()->IDC() == IDC_DEBUG_EXP)
	{
		switch (nChar)
		{
		case VK_UP:
			if (_current > 0)
			{
				_current--;
				CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_DEBUG_EXP));
				Assert(edit);
				edit->SetText(GDebugConsoleInfo.history[_current]);
			}
			return true;
		case VK_DOWN:
			if (_current < GDebugConsoleInfo.history.Size() - 1)
			{
				_current++;
				CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_DEBUG_EXP));
				Assert(edit);
				edit->SetText(GDebugConsoleInfo.history[_current]);
			}
			return true;
		}
	}

	return Display::OnKeyDown(nChar, nRepCnt, nFlags);
}

void DisplayDebug::OnDraw(EntityAI *vehicle, float alpha)
{
	for (int i=0; i<DebugConsoleExpRows; i++)
	{
		char var[16];
		sprintf(var, "debug%d", i + 1);
		GameState *gstate = GWorld->GetGameState();
		gstate->VarDelete(var);
	}
	for (int i=0; i<DebugConsoleExpRows; i++)
	{
		char var[16];
		sprintf(var, "debug%d", i + 1);
		GameState *gstate = GWorld->GetGameState();
		CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_DEBUG_EXP1 + i));
		Assert(edit);
		RString exp = edit->GetText();
		RString result;
		if (exp.GetLength() > 0)
		{
			if (!gstate->CheckEvaluate(exp))
			{
				result = "Error";
			}
			else
			{
				GameValue val = gstate->Evaluate(exp);
				gstate->VarSet(var, val);
				result = val.GetText();
			}
		}
		edit = dynamic_cast<CEdit *>(GetCtrl(IDC_DEBUG_RES1 + i));
		Assert(edit);
		edit->SetText(result);
	}

	Display::OnDraw(vehicle, alpha);
}

AbstractOptionsUI *CreateDebugConsole()
{
	return new DisplayDebug(NULL, GWorld->GetGameState());
}

void DisplayDebug::DestroyHUD(int exit)
{
	GLOB_WORLD->DestroyOptions(exit);
}

#endif

///////////////////////////////////////////////////////////////////////////////
// display with dirty glasses

DisplayNotebook::DisplayNotebook(ControlsContainer *parent)
: Display(parent)
{
/*
	_glassA = GlobLoadTexture("data\\notasskloa.paa");
	_glassB = GlobLoadTexture("data\\notassklob.paa");
	_glassC = GlobLoadTexture("data\\notasskloc.paa");
	_glassD = GlobLoadTexture("data\\notassklod.paa");
	_glassE = GlobLoadTexture("data\\notasskloe.paa");
*/
	_lastWeather = WSUndefined;
}

extern void PositionToAA11(Vector3Val pos, char *buffer);

Control *DisplayNotebook::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
	case IDC_TIME:
		return new CStaticTime(this, idc, cls, false);
	default:
		return Display::OnCreateCtrl(type, idc, cls);
	}
}

void DisplayNotebook::OnDraw(EntityAI *vehicle, float alpha)
{
	// set values of info items
	WeatherState cur = GetWeather();
	if (cur != _lastWeather)
	{
		Assert(dynamic_cast<CStatic *>(GetCtrl(IDC_WEATHER)));
		CStatic *picture = static_cast<CStatic *>(GetCtrl(IDC_WEATHER));
		switch (cur)
		{
		case WSClear:
			picture->SetText("jasnosl.paa");
			break;
		case WSCloudly:
			picture->SetText("polojasno.paa");
			break;
		case WSOvercast:
			picture->SetText("zatazenosl.paa");
			break;
		case WSRainy:
			picture->SetText("destivo.paa");
			break;
		case WSStormy:
			picture->SetText("bourka.paa");
			break;
		}
		_lastWeather = cur;
	}

	char buffer[256];
	Assert(dynamic_cast<CStaticTime *>(GetCtrl(IDC_TIME)));
	CStaticTime *time = static_cast<CStaticTime *>(GetCtrl(IDC_TIME));
	Clock clock = GetTime();
	time->SetTime(clock);

	Assert(dynamic_cast<CStatic *>(GetCtrl(IDC_DATE)));
	CStatic *text = static_cast<CStatic *>(GetCtrl(IDC_DATE));
	clock.FormatDate(LocalizeString(IDS_DATE_FORMAT), buffer);	
	text->SetText(buffer);

	Assert(dynamic_cast<CStatic *>(GetCtrl(IDC_POSITION)));
	text = static_cast<CStatic *>(GetCtrl(IDC_POSITION));
	Point3 pos;
	if (GetPosition(pos))
		PositionToAA11(pos, buffer);
	else
		buffer[0] = 0;
	text->SetText(buffer);

	// draw controls
	Display::OnDraw(vehicle,alpha);

/*
	// draw glasses
	PackedColor color(Color(1,1,1,1));
	const int w = GLOB_ENGINE->Width();
	const int h = GLOB_ENGINE->Height();

	MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(_glassA, 0, 0);
	GLOB_ENGINE->Draw2D(mip, color, 0, 0, 0.4 * w, 0.5 * h);
	mip = GLOB_ENGINE->TextBank()->UseMipmap(_glassB, 0, 0);
	GLOB_ENGINE->Draw2D(mip, color, 0.4 * w, 0, 0.4 * w, 0.5 * h);
	mip = GLOB_ENGINE->TextBank()->UseMipmap(_glassC, 0, 0);
	GLOB_ENGINE->Draw2D(mip, color, 0, 0.5 * h, 0.4 * w, 0.5 * h);
	mip = GLOB_ENGINE->TextBank()->UseMipmap(_glassD, 0, 0);
	GLOB_ENGINE->Draw2D(mip, color, 0.4 * w, 0.5 * h, 0.4 * w, 0.5 * h);
	mip = GLOB_ENGINE->TextBank()->UseMipmap(_glassE, 0, 0);
	GLOB_ENGINE->Draw2D(mip, color, 0.8 * w, 0, 0.2 * w, 0.5 * h);
*/
}

#if _ENABLE_EDITOR

DisplayMapEditor::DisplayMapEditor(ControlsContainer *parent)
: DisplayNotebook(parent)
{
	_cursor = CursorArrow;
}

void DisplayMapEditor::OnSimulate(EntityAI *vehicle)
{
	CStaticMap *map = GetMap();
	if (map && GetCtrl(IDC_MAP)->IsVisible())
	{
		float mouseX = 0.5 + GInput.cursorX * 0.5;
		float mouseY = 0.5 + GInput.cursorY * 0.5;
		IControl *ctrl = GetCtrl(mouseX, mouseY);
		if (ctrl && ctrl->IDC() == IDC_MAP)
		{
			if (map->_dragging || map->_selecting)
			{
				if (_cursor != CursorMove)
				{
					_cursor = CursorMove;
					SetCursor("Move");
				}
			}
			else if (map->_moving)
			{
				if (_cursor != CursorScroll)
				{
					_cursor = CursorScroll;
					SetCursor("Scroll");
				}
			}
			else
			{
				if (_cursor != CursorTrack)
				{
					_cursor = CursorTrack;
					SetCursor("Track");
				}
			}
		}
		else
		{
			if (_cursor != CursorArrow)
			{
				_cursor = CursorArrow;
				SetCursor("Arrow");
			}
		}
	}

	DisplayNotebook::OnSimulate(vehicle);
}

bool DisplayMapEditor::OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	CStaticMap *map = GetMap();
	if (map && GetCtrl(IDC_MAP)->IsVisible())
	{
		if (map->OnKeyDown(nChar, nRepCnt, nFlags)) return true;
	}
	return DisplayNotebook::OnKeyDown(nChar, nRepCnt, nFlags);
}

bool DisplayMapEditor::OnKeyUp(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	CStaticMap *map = GetMap();
	if (map && GetCtrl(IDC_MAP)->IsVisible())
	{
		if (map->OnKeyUp(nChar, nRepCnt, nFlags)) return true;
	}
	return DisplayNotebook::OnKeyUp(nChar, nRepCnt, nFlags);
}

void DisplayMapEditor::OnDraw(EntityAI *vehicle, float alpha)
{
	DisplayNotebook::OnDraw(vehicle,alpha);

/*
	CStaticMap *map = GetMap();
	if (map && GetCtrl(IDC_MAP)->IsVisible() && !_child && !_msgBox)
	{
		float mouseX = 0.5 + GInput.cursorX * 0.5;
		float mouseY = 0.5 + GInput.cursorY * 0.5;
		if (map->IsInside(mouseX, mouseY))
		{
			int wScreen = GLOB_ENGINE->Width();
			int hScreen = GLOB_ENGINE->Height();
			const float mouseH = 16.0 / 600;
			const float mouseW = 16.0 / 800;
		
			PackedColor color = GetCursorColor();
			color.SetA8(color.A8() / 2);
			float pt = mouseX - mouseW;
			if (pt > map->X())
				GLOB_ENGINE->DrawLine
				(
					wScreen * map->X(), toInt(hScreen * mouseY),
					wScreen * pt, toInt(hScreen * mouseY),
					color, color 
				);
			pt = mouseX + mouseW;
			if (pt < map->X() + map->W())
				GLOB_ENGINE->DrawLine
				(
					wScreen * pt, toInt(hScreen * mouseY),
					wScreen * (map->X() + map->W()), toInt(hScreen * mouseY),
					color, color 
				);
			pt = mouseY - mouseH;
			if (pt > map->Y())
				GLOB_ENGINE->DrawLine
				(
					toInt(wScreen * mouseX), hScreen * map->Y(),
					toInt(wScreen * mouseX), hScreen * pt,
					color, color 
				);
			pt = mouseY + mouseH;
			if (pt < map->Y() + map->H())
				GLOB_ENGINE->DrawLine
				(
					toInt(wScreen * mouseX), hScreen * pt,
					toInt(wScreen * mouseX), hScreen * (map->Y() + map->H()),
					color, color 
				);
		}
	}
*/
}

#endif // #if _ENABLE_EDITOR

///////////////////////////////////////////////////////////////////////////////
// creation of main display

AbstractOptionsUI *CreateMainOptionsUI()
{
	return new DisplayMain(NULL);
}

AbstractOptionsUI *CreateEndOptionsUI(int mode)
{
	Fail("Obsolete");
	return NULL;
}

void DisplayMain::DestroyHUD(int exit)
{
	GLOB_WORLD->DestroyOptions(exit);
}

#if _ENABLE_EDITOR

void OpenEditor()
{
	Display *options = dynamic_cast<Display *>(GWorld->Options());
	if (!options) return;
	GWorld->SwitchLandscape(GetWorldName(Glob.header.worldname));
	// Macrovision CD Protection
	CDPCreateEditor(options);
	//options->CreateChild(new DisplayArcadeMap(options));
}

#endif // #if _ENABLE_EDITOR

void StartIntro()
{
	Display *options = dynamic_cast<Display *>(GWorld->Options());
	if (!options) return;
	options->CreateChild(new DisplayIntro(options));
}

void StartMission()
{
	Display *options = dynamic_cast<Display *>(GWorld->Options());
	if (!options) return;
	options->CreateChild(new DisplayMission(options));
}

bool StartAutoTest()
{
	CurrentTemplate.Clear();
	if (GWorld->UI()) GWorld->UI()->Init();

	if (!ParseMission(false) || CurrentTemplate.groups.Size() == 0)
		return false;

	GWorld->SwitchLandscape(GetWorldName(Glob.header.worldname));
	GWorld->ActivateAddons(CurrentTemplate.addOns);
	GWorld->InitGeneral(CurrentTemplate.intel);
	if (!GWorld->InitVehicles(GModeArcade, CurrentTemplate))
		return false;

	GStats.ClearAll();

	// remove temporary files
	RString dir = GetSaveDirectory();
	RString weapons = dir + RString("weapons.cfg");
	::DeleteFile(weapons);
	RString save = dir + RString("continue.fps");
	::DeleteFile(save);
	save = dir + RString("autosave.fps");
	::DeleteFile(save);
	save = dir + RString("save.fps");
	::DeleteFile(save);

	Display *options = dynamic_cast<Display *>(GWorld->Options());
	if (!options) return false;
	options->CreateChild(new DisplayMission(options));
	return true;
}
