#ifdef _MSC_VER
#pragma once
#endif

#ifndef _TXTGLIDE_HPP
#define _TXTGLIDE_HPP

#include "extGlide.h"

#include "types.hpp"
#include <El/Math/math3d.hpp>

#include <Es/Containers/cachelist.hpp>
#include "pactext.hpp"
#include "textbank.hpp"

enum {MaxTMUs=8};

#define GLIDE_HEAP_SIZE ( Glob.config.heapSize*(1024*1024) )

// manage offscreen resources
typedef FxU32 GRAMOffset;

typedef Heap<GRAMOffset,int>::HeapItem GRAMItem;

#include <Es/Memory/normalNew.hpp>

class GRAMHeap: public Heap<GRAMOffset,int>
{
	public:
	//GRAMHeap(){}
	~GRAMHeap();
	
	void Init( GRAMOffset begin, GRAMOffset end );
};

class HMipCacheGlide: public CLDLink
{
	public:
	// only textures are stored in VRAM memory
	TextureGlide *texture;
	int level;

	USE_FAST_ALLOCATOR;
};
class HMipCacheRoot: public CLList<HMipCacheGlide,CLDLink>
{
};

#include <Es/Memory/debugNew.hpp>

/*! Glide specific info.
\patch_internal 1.01 Date 6/22/2001 by Ondra. added separate Glide specific info.
*/

class MipmapLevelGlideInfo
{
	// one mipmap level actually contains all smaller mipmaps
	// this is done in single allocation unit
	// only largest mip-map is available via PacLevelMem interface
	public:	
	GrTexInfo _info;
	int _size;

	MipmapLevelGlideInfo():_size(0) {}


	int TotalSize() const {return _size;}
	bool Permanent() const;
};

#include <Es/Memory/normalNew.hpp>

class TextureGlide: public Texture
{
	friend class TextBankGlide;
	friend class EngineGlide;
	
	private:
	// note: some textures have no palette
	SRef<ITextureSource> _src;

	Ref<TextureGlide> _interpolate;
	float _iFactor; // current interpolation factor

	int _maxSize; // landscape textures have different limit than object...

	// store all neccessary LOD information for multibase addressing
	GrAspectRatio_t _aRatio;

	bool _isDetail:1;
	bool _useDetail:1;

	signed char _nMipmaps;
	signed char _tmu; // which TMU are we downloaded to?
	signed char _gramLevel; // which level is in the GRAM
	signed char _usedCount; // system mem is currently used - do not release
	signed char _gramUsedCount; // GRAM mem is currently used - do not release
	signed char _memLevel; // which level is in the heap

	PacLevelMem _mipmaps[MAX_MIPMAPS];
	//! Glide specific info.
	/*!
	\patch_internal 1.01 Date 6/22/2001 by Ondra. Change: Glide specific info in separate structure.
	This is to help universal data source.
	*/
	MipmapLevelGlideInfo _mipmapsInfo[MAX_MIPMAPS];

	// store information about what is allocated and where
	GRAMItem *_isGRAM; // location in texture VRAM
	HMipCacheGlide *_cacheGRAM; // is allocated in GRAM cache?
	const HMipCacheRoot *_cacheGRAMRoot; // which heap are we in?

	MemoryItem *_memory; // location in texture heap 
	HMipCacheGlide *_cache; // is allocated in heap?

	//GrTexInfo _texInfo;

	public:
	TextureGlide();
	~TextureGlide();
	
	int LoadPalette( QIStream &in );
	//int SkipPalette( QIStream &in ) {return _pal.Skip(in);}; // load palette
	
	// handle single mip-map levels
	int LoadPart(int level, bool noFree=false );

	void ForceTmu( int tmu );

	void ReleaseMemory();
	void ReleaseGRAM();
	
	void InitLoaded();

	void CacheUse( HMipCacheRoot *list, int level );
	void GRAMUse( HMipCacheRoot *list, int level );
	
	void SetCacheGRAMRoot( const HMipCacheRoot *cacheGRAMRoot )
	{
		_cacheGRAMRoot=cacheGRAMRoot;
	}
	
	int AMaxSize() const {return _maxSize;}
	void SetMaxSize( int size );

	bool IsAlpha() const {return _src && _src->IsAlpha();}
	void SetMultitexturing( int type );

	void SetupInfo();

	int SelectTMU() const;
			
	void SetMipmapRange( int min, int max );
	void CompensateGamma( int minLevel );
	int Load( int minLevel, int maxLevel ); // load PAC file - one mipmap level
	
	//int ALoad( int minLevel, int maxLevel );

	int Preload( int minLevel ); // load PAC file - coarse mipmap levels
	int PreloadGRAM( int level ); // load PAC file into GRAM

	int Init( RStringB name ); // load PAC file - all mipmap levels

	const PacLevelMem *Mipmap( int level ) const {return &_mipmaps[level];}
	
	int NMipmaps() const {return _nMipmaps;}
	bool VerifyChecksum( const MipInfo &mip ) const;

	void ASetNMipmaps( int n );
	int ANMipmaps() const {return _nMipmaps;}
	int AWidth( int level=0 ) const {return _mipmaps[level]._w;}
	int AHeight( int level=0 ) const {return _mipmaps[level]._h;}
	bool IsTransparent() const {return _src && _src->IsTransparent();}

	AbstractMipmapLevel &AMipmap( int level ) {return _mipmaps[level];}
	const AbstractMipmapLevel &AMipmap( int level ) const {return _mipmaps[level];}

	Color GetPixel( int level, float u, float v ) const;
	Color GetColor() {return _src ? _src->GetAverageColor() : HBlack;}

	int Width( int level ) const {return _mipmaps[level]._w;}
	int Height( int level ) const {return _mipmaps[level]._h;}

	// Glide requires u,v conversion

	float UToPhysical( float u ) const;
	float VToPhysical( float v ) const;

	float UToLogical( float u ) const;
	float VToLogical( float v ) const;

	NoCopy(TextureGlide)
	USE_FAST_ALLOCATOR
};

#include <Es/Types/removeLinks.hpp>
#include <Es/Memory/debugNew.hpp>

class TextBankGlide: public AbstractTextBank
{
	friend class TextureGlide;
		
	private:
	LLinkArray<TextureGlide> _texture;
	
	SystemHeap _heap;
	
	class TMUCache
	{
		friend class TextBankGlide;
		friend class TextureGlide;
		GRAMHeap _vram;
		// last-recently-used list of mip-maps and palettes alocated in GRAM cache
		HMipCacheRoot _thisFrameGRAM;
		HMipCacheRoot _lastFrameGRAM;
		HMipCacheRoot _previousGRAM;

		public:
		TMUCache(){}
		private:
		TMUCache( const TMUCache &src );
		void operator = ( const TMUCache &src );
	};

	TMUCache _tmu[MaxTMUs];
	mutable int _selectTMU;


	Ref<TextureGlide> _detail; // detail texture - tmu 1
	Ref<TextureGlide> _specular; // specular texture - tmu 1

	void InitDetailTextures();

	int _fromHeapToGRAM; // statistics - heap efficiency
	int _fromDiskToHeap;

	int _usedGRAM; // GRAM used in this frame

	EngineGlide *_engine;
	
	// last-recently-used list of allocated mip-map levels
	HMipCacheRoot _lastUsed;
	HMipCacheRoot _permanentUsed;
	//HMipCacheRoot _permanentGRAM;
	
	protected:


	int Find( RStringB name1, TextureGlide *interpolated=NULL );
	int FindFree();
	TextureGlide *Copy( int from );

	public:
	TextBankGlide( EngineGlide *engine );
	~TextBankGlide();

	void ReloadAll(); // texture cache was destroyed - reload
	
	MemoryHeap *Heap(){return &_heap;}
	GRAMHeap *GRAM( int tmu ){return &_tmu[tmu]._vram;}

	// overloading AbstractTextBank
	virtual int NTextures() const {return _texture.Size();}
	virtual Texture *GetTexture( int i ) const {return _texture[i];}

	void Compact();
	void Preload(); // prepare bank (preallocate heaps...)
	void PreloadTextures(); // preload coarse mip-maps
	void FlushTextures();
	void FlushBank(QFBank *bank);
	
	Ref<Texture> Load( RStringB name );
	Ref<Texture> LoadInterpolated( RStringB n1, RStringB n2, float factor );

	void StartFrame();
	void FinishFrame();
	virtual bool NeedUVConversion() const {return true;}

	void StopAll(); // stop all background activity (before shutdown)

	TextureGlide *DetailTexture() const {return _detail;}
	TextureGlide *SpecularTexture() const {return _specular;}

	MipInfo UseMipmap
	(
		Texture *texture, int level, int LevelTop
	);
	//void ReleaseMipmap();

	void InitGRAM();
	void RecreateGRAM(); // GRAM size has been changed (e.g. resolution change)

	protected:
	void HeapMoved( int offset );
	void ReserveMemory( int size, HMipCacheRoot *root );
	bool ReserveGRAM( int tmu, int size, HMipCacheRoot *root );

	int SelectTMU() const;
	public:
	int NTMUs() const;
	//void SetNTMUs( int nTMUs ) {_nTMUs=nTMUs;}

	void ReserveMemory( int size ); // reserve memory in heap
	bool ReserveGRAM( int tmu, int size, bool needed ); // reserve memory in GRAM cache

	bool VerifyChecksums();
	int UsedGRAM( HMipCacheRoot *root ); // GRAM used to draw this and last frame
	int LastFrameGRAM(); // GRAM used to draw this and last frame
	
	float HeapEfficiency() const {return 1.0f-(float)_fromDiskToHeap/_fromHeapToGRAM;}
	float GRAMEfficiency();

	NoCopy(TextBankGlide)
};

#endif

