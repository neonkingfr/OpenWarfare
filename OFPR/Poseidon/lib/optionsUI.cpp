// Implementation of display's instances

#include "wpch.hpp"
#include "uiMap.hpp"

#include "landscape.hpp"

#include "resincl.hpp"
extern class ParamFile Res;

#include "keyInput.hpp"

#include "camera.hpp"
#include "progress.hpp"

#include "paramArchive.hpp"
#include "saveVersion.hpp"

#include <El/QStream/QBStream.hpp>

#include <El/Common/randomGen.hpp>

#include "scripts.hpp"

#include "dikCodes.h"
#include "vkCodes.h"
#include <Es/Common/win.h>
#ifdef _WIN32
  #include <io.h>
  #include <direct.h>
#endif

#include "network.hpp"
#include "chat.hpp"

#include "dynSound.hpp"
#include "displayUI.hpp"

#include "cdapfncond.h"

/*
#if _CZECH
#include "roxxe.hpp"
#endif
*/

#include "stringtableExt.hpp"
#include "mbcs.hpp"

#include "gameStateExt.hpp"

#include "strFormat.hpp"

/*!
\file
Interface and implementation file for particular displays.
*/

extern bool AutoTest;

//! creates formatted string for given amount of currency
/*!
	\param buffer output buffer
	\param res amount of currency
*/
void FormatCurrency(char *buffer, float res)
{
	if (res <= 0)
		strcpy(buffer, LocalizeString(IDS_CURRENCY_NONE));
	else
	{
		if (res >= 9.9e9)
			strcpy(buffer, LocalizeString(IDS_CURRENCY_LOT));
		else
		{
			char temp[128];
			sprintf(buffer, "%.0f", res);
			int digits = strlen(buffer);
			int i = 0;
			int j = 0;
			temp[i++] = buffer[j++];
			digits--;
			while (digits > 0)
			{
				if ((digits % 3) == 0)
					temp[i++] = LocalizeString(IDS_CURRENCY_BLANK)[0];
				temp[i++] = buffer[j++];
				digits--;
			}
			temp[i++] = buffer[j++];
			digits--;
			temp[i++] = 0;
			sprintf(buffer, (const char *)LocalizeString(IDS_CURRENCY_FORMAT), temp);
		}
	}
}

//! returns path of user directory for current user
RString GetUserDirectory()
{
	#ifdef _XBOX
	RString dir = "u:\\Users";
	#else
	RString dir = "Users";
	#endif
	::CreateDirectory(dir,NULL);
	dir = dir + RString("\\");
	dir = dir + RString(Glob.header.playerName);
	::CreateDirectory(dir,NULL);
	dir = dir + RString("\\");
	return dir;
}

//! returns user profile (parameters) file for current user
RString GetUserParams()
{
	return GetUserDirectory() + RString("UserInfo.cfg");
}

//! returns campaign directory for given campaign
RString GetCampaignDirectory(RString campaign)
{
	Assert(campaign.GetLength() > 0);
	return RString("campaigns\\") + campaign + RString("\\");
}

//! returns mission directory for given mission in given campaign
RString GetCampaignMissionDirectory(RString campaign, RString mission)
{
	Assert(campaign.GetLength() > 0);
	Assert(mission.GetLength() > 0);
	return RString("campaigns\\") + campaign + RString("\\missions\\") + mission + RString("\\");
}

//! base directory
/*!
	Possible values:	
	- base application directory for single missions or multiplayer missions
	- campaign directory for missions in campaign
	- user directory for user created missions
*/
static RString BaseDirectory;
//! base subdirectory
/*!
	Possible values:
	- Missions for singleplayer missions
	- MPMissions for multiplayer missions
	- Anims for animations
*/
static RString BaseSubdirectory;

//! returns base directory
RString GetBaseDirectory()
{
	return BaseDirectory;
}

//! returns base subdirectory
RString GetBaseSubdirectory()
{
	return BaseSubdirectory;
}

//! returns current directory for missions
RString GetMissionsDirectory()
{
	return BaseDirectory + BaseSubdirectory;
}

//! returns directory of current mission
RString GetMissionDirectory()
{
	return GetMissionsDirectory() +
		RString(Glob.header.filename) + RString(".") +
		RString(Glob.header.worldname) + RString("\\");
}

//! returns path of current mission's briefing
RString GetBriefingFile()
{
	RString prefix = GetMissionDirectory() + RString("briefing.");
	RString name = prefix + GLanguage + RString(".html");
	if (QIFStreamB::FileExist(name)) return name;
	name = prefix + RString("html");
	if (QIFStreamB::FileExist(name)) return name;
	return RString();
}

//! create path to given directory
void CreatePath(RString path)
{
	// string will be changed temporary
	char *end = (char *)path.Data();
	while (end = strchr(end, '\\'))
	{
		*end = 0;
		::CreateDirectory(path,NULL);
		*end = '\\';
		end++;
	}
}

//! returns save directory for current user and current mission
RString GetSaveDirectory()
{
/*
	RString dir = GetUserDirectory() + RString("Saved");
	mkdir(dir, NULL);
	return dir;
*/
	RString dir = GetUserDirectory() + RString("Saved\\") + GetBaseDirectory();
	if (!IsCampaign())
		dir = dir + GetBaseSubdirectory() +
		RString(Glob.header.filenameReal) + RString(".") +
		RString(Glob.header.worldname) + RString("\\");
	CreatePath(dir);
	return dir;
}

//! returns temporary save directory for current user
RString GetTmpSaveDirectory()
{
	RString dir = GetUserDirectory() + RString("Saved\\Tmp\\");
	CreatePath(dir);
	return dir;
}

//! returns save directory for current user and given campaign
RString GetCampaignSaveDirectory(RString campaign)
{
	if (campaign.GetLength() == 0) return GetTmpSaveDirectory();
	RString dir = GetUserDirectory() + RString("Saved\\campaigns\\") + campaign + RString("\\");
	return dir;
}

//! returns save directory for current user and given single mission
RString GetMissionSaveDirectory(RString mission)
{
	RString dir = GetUserDirectory() + RString("Saved\\missions\\") + mission + RString("\\");
	return dir;
}

//! find description of environmental sound in configuration files
/*!
	Description is subclass of class CfgEnvSounds.
	Order of searching:
	- description.ext of current mission
	- description.ext of current campaign
	- global config
	\param name name of subclass
	\param day output - sound parameters for environmental sound during day
	\param night output - sound parameters for environmental sound during night
*/
void FindEnvSound(RString name, SoundPars &day, SoundPars &night)
{
	// ignore @ - not used now
	if (name[0] == '@') name = (const char *)name + 1;

	// find in mission
	const ParamEntry *cls = ExtParsMission.FindEntry("CfgEnvSounds");
	const ParamEntry *entry = cls ? cls->FindEntry(name) : NULL;
	if (entry)
	{
		GetValue(day, (*entry) >> "sound");
		if (day.name.GetLength() > 0)
			day.name = GetMissionDirectory() + day.name;
		GetValue(night, (*entry) >> "soundNight");
		if (night.name.GetLength() > 0)
			night.name = GetMissionDirectory() + night.name;
		return;
	}

	// find in campaign
	cls = ExtParsCampaign.FindEntry("CfgEnvSounds");
	entry = cls ? cls->FindEntry(name) : NULL;
	if (entry)
	{
		GetValue(day, (*entry) >> "sound");
		if (day.name.GetLength() > 0)
			day.name = BaseDirectory + RString("dtaExt\\") + day.name;
		GetValue(night, (*entry) >> "soundNight");
		if (night.name.GetLength() > 0)
			night.name = BaseDirectory + RString("dtaExt\\") + night.name;
		return;
	}

	// find in config
	cls = Pars.FindEntry("CfgEnvSounds");
	entry = cls ? cls->FindEntry(name) : NULL;
	if (entry)
	{
		GetValue(day, (*entry) >> "sound");
		GetValue(night, (*entry) >> "soundNight");
		return;
	}

	WarningMessage("Environmental sound %s not found", (const char *)name);
}

//! find description of SFX sound scheme in configuration files
/*!
	Description is subclass of class CfgSFX.
	Order of searching:
	- description.ext of current mission
	- description.ext of current campaign
	- global config
	\param name name of subclass
	\param emptySound empty sound
	\param sounds sound scheme
*/
void FindSFX(RString name, SoundEntry &emptySound, AutoArray<SoundEntry> &sounds)
{
	// ignore @ - not used now
	if (name[0] == '@') name = (const char *)name + 1;

	// find in mission
	const ParamEntry *cls = ExtParsMission.FindEntry("CfgSFX");
	const ParamEntry *entry = cls ? cls->FindEntry(name) : NULL;
	if (entry)
	{
		// load empty sound
		emptySound = DynSound::LoadEntry((*entry) >> "empty");
		if (emptySound.name.GetLength() > 0)
			emptySound.name = GetMissionDirectory() + emptySound.name;
		// load sound scheme
		const ParamEntry &list = (*entry) >> "sounds";
		int n = list.GetSize();
		sounds.Resize(n);
		for (int i=0; i<n; i++)
		{
			RString name = list[i];
			SoundEntry &sound = sounds[i];
			sound	= DynSound::LoadEntry((*entry) >> name);
			if (sound.name.GetLength() > 0)
				sound.name = GetMissionDirectory() + sound.name;
		}
		// return sound location
		return; // ExtParsMission.GetName();
	}

	// find in campaign
	cls = ExtParsCampaign.FindEntry("CfgSFX");
	entry = cls ? cls->FindEntry(name) : NULL;
	if (entry)
	{
		// load empty sound
		emptySound = DynSound::LoadEntry((*entry) >> "empty");
		if (emptySound.name.GetLength() > 0)
			emptySound.name = BaseDirectory + RString("dtaExt\\") + emptySound.name;
		// load sound scheme
		const ParamEntry &list = (*entry) >> "sounds";
		int n = list.GetSize();
		sounds.Resize(n);
		for (int i=0; i<n; i++)
		{
			RString name = list[i];
			SoundEntry &sound = sounds[i];
			sound	= DynSound::LoadEntry((*entry) >> name);
			if (sound.name.GetLength() > 0)
				sound.name = BaseDirectory + RString("dtaExt\\") + sound.name;
		}
		// return sound location
		return; // ExtParsCampaign.GetName();
	}

	// find in config
	cls = Pars.FindEntry("CfgSFX");
	entry = cls ? cls->FindEntry(name) : NULL;
	if (entry)
	{
		// load empty sound
		emptySound = DynSound::LoadEntry((*entry) >> "empty");
		// load sound scheme
		const ParamEntry &list = (*entry) >> "sounds";
		int n = list.GetSize();
		sounds.Resize(n);
		for (int i=0; i<n; i++)
		{
			RString name = list[i];
			SoundEntry &sound = sounds[i];
			sound	= DynSound::LoadEntry((*entry) >> name);
		}
		return; // Pars.GetName();
	}

	WarningMessage("SFX %s not found", (const char *)name);
	return; // "";
}

//! find description of sound in configuration files
/*!
	Description is subclass of class CfgSounds.
	Order of searching:
	- description.ext of current mission
	- description.ext of current campaign
	- global config
	\param name name of subclass
	\param pars sound parameters
*/
const ParamEntry *FindSound(RString name, SoundPars &pars)
{
	// ignore @ - not used now
	if (name[0] == '@') name = (const char *)name + 1;

	// find in mission
	const ParamEntry *cls = ExtParsMission.FindEntry("CfgSounds");
	const ParamEntry *entry = cls ? cls->FindEntry(name) : NULL;
	if (entry)
	{
		GetValue(pars, (*entry) >> "sound");
		if (pars.name.GetLength() > 0)
			pars.name = GetMissionDirectory() +	pars.name;
		return entry;
	}
	
	// find in campaign
	cls = ExtParsCampaign.FindEntry("CfgSounds");
	entry = cls ? cls->FindEntry(name) : NULL;
	if (entry)
	{
		GetValue(pars, (*entry) >> "sound");
		if (pars.name.GetLength() > 0)
			pars.name = BaseDirectory + RString("dtaExt\\") + pars.name;
		return entry;
	}
	
	// find in config
	cls = Pars.FindEntry("CfgSounds");
	entry = cls ? cls->FindEntry(name) : NULL;
	if (entry)
	{
		GetValue(pars, (*entry) >> "sound");
		return entry;
	}

	WarningMessage("Sound %s not found", (const char *)name);
	return NULL;
}

//! find description of music track in configuration files
/*!
	Description is subclass of class CfgMusic.
	Order of searching:
	- description.ext of current mission
	- description.ext of current campaign
	- global config
	\param name name of subclass
	\param pars sound parameters
*/
const ParamEntry *FindMusic(RString name, SoundPars &pars)
{
	// find in mission
	const ParamEntry *cls = ExtParsMission.FindEntry("CfgMusic");
	const ParamEntry *entry = cls ? cls->FindEntry(name) : NULL;
	if (entry)
	{
		GetValue(pars, (*entry) >> "sound");
		if (pars.name.GetLength() > 0)
			pars.name = GetMissionDirectory() +	pars.name;
		return entry;
	}
	
	// find in campaign
	cls = ExtParsCampaign.FindEntry("CfgMusic");
	entry = cls ? cls->FindEntry(name) : NULL;
	if (entry)
	{
		GetValue(pars, (*entry) >> "sound");
		if (pars.name.GetLength() > 0)
			pars.name = BaseDirectory + RString("dtaExt\\") + pars.name;
		return entry;
	}
	
	// find in config
	cls = Pars.FindEntry("CfgMusic");
	entry = cls ? cls->FindEntry(name) : NULL;
	if (entry)
	{
		GetValue(pars, (*entry) >> "sound");
		return entry;
	}

	WarningMessage("Music %s not found", (const char *)name);
	return NULL;
}

//! find description of radio message in configuration files
/*!
	Description is subclass of class CfgRadio.
	Order of searching:
	- description.ext of current mission
	- description.ext of current campaign
	- global config
	\param name name of subclass
	\param pars sound parameters
	\return description subclass
*/
const ParamEntry *FindRadio(RString name, SoundPars &pars)
{
	// find in mission
	const ParamEntry *cls = ExtParsMission.FindEntry("CfgRadio");
	const ParamEntry *entry = cls ? cls->FindEntry(name) : NULL;
	if (entry)
	{
		GetValue(pars, (*entry) >> "sound");
		if (pars.name.GetLength() > 0)
			pars.name = GetMissionDirectory() +	pars.name;
		return entry;
	}
	
	// find in campaign
	cls = ExtParsCampaign.FindEntry("CfgRadio");
	entry = cls ? cls->FindEntry(name) : NULL;
	if (entry)
	{
		GetValue(pars, (*entry) >> "sound");
		if (pars.name.GetLength() > 0)
			pars.name = BaseDirectory + RString("dtaExt\\") + pars.name;
		return entry;
	}
	
	// find in config
	cls = Pars.FindEntry("CfgRadio");
	entry = cls ? cls->FindEntry(name) : NULL;
	if (entry)
	{
		GetValue(pars, (*entry) >> "sound");
		return entry;
	}

	WarningMessage("Radio message %s not found", (const char *)name);
	return NULL;
}

//! find description of camera effect in configuration files
/*!
	Description is subclass of class CfgCameraEffects.
	Order of searching:
	- description.ext of current mission
	- description.ext of current campaign
	- global config
	\param name name of subclass
	\return description subclass
*/
const ParamEntry *FindCameraEffect(RString name)
{
	// ignore @ - not used now
	if (name[0] == '@') name = (const char *)name + 1;

	// find in mission
	const ParamEntry *cls = ExtParsMission.FindEntry("CfgCameraEffects");
	const ParamEntry *array = cls ? cls->FindEntry("Array") : NULL;
	const ParamEntry *entry = array ? array->FindEntry(name) : NULL;
	if (entry) return entry;
	
	// find in campaign
	cls = ExtParsCampaign.FindEntry("CfgCameraEffects");
	array = cls ? cls->FindEntry("Array") : NULL;
	entry = array ? array->FindEntry(name) : NULL;
	if (entry) return entry;

	// find in config
	cls = Pars.FindEntry("CfgCameraEffects");
	array = cls ? cls->FindEntry("Array") : NULL;
	entry = array ? array->FindEntry(name) : NULL;
	if (entry) return entry;

	WarningMessage("Camera effect %s not found", (const char *)name);
	return NULL;
}

//! find script file
/*!
	Order of searching:
	- mission directory of current mission
	- "scripts" subdirectory in campaign directory of current campaign
	- "scripts" subdirectory in base aplication directory
	\param name simple script name
	\return full script name (path)
*/
RString FindScript(RString name)
{
	// find in mission
	RString fullname = GetMissionDirectory() + name;
	if (QIFStreamB::FileExist(fullname)) return fullname;

	// find in campaign
	fullname = BaseDirectory + RString("scripts\\") + name;
	if (QIFStreamB::FileExist(fullname)) return fullname;

	// find in root
	fullname = RString("scripts\\") + name;
	if (QIFStreamB::FileExist(fullname)) return fullname;

	WarningMessage("Script %s not found", (const char *)name);
	return "";
}

//! find model (d3d) file
/*!
	Order of searching:
	- mission directory of current mission
	- "dtaExt" subdirectory in campaign directory of current campaign
	- "dtaExt" subdirectory in base aplication directory
	\param name simple model file name
	\return full model file name (path)
*/
RString FindShape(RString name)
{
	// ignore @ - not used now
	if (name[0] == '@') name = (const char *)name + 1;

	// find in mission
	RString fullname = GetMissionDirectory() + name;
	if (QIFStreamB::FileExist(fullname)) return fullname;
	
	// find in campaign
	fullname = BaseDirectory + RString("dtaExt\\") + name;
	if (QIFStreamB::FileExist(fullname)) return fullname;
	
	// find in root
	fullname = RString("dtaExt\\") + name;
	if (QIFStreamB::FileExist(fullname)) return fullname;
	
	// find in bank
	fullname = GetShapeName(name);
	if (QIFStreamB::FileExist(fullname)) return fullname;

	WarningMessage("Shape %s not found", (const char *)name);
	return "";
}

//! find image (texture) file
/*!
	Order of searching:
	- mission directory of current mission
	- "dtaExt" subdirectory in campaign directory of current campaign
	- "dtaExt" subdirectory in base aplication directory
	\param name simple image file name
	\return full image file name (path)
*/
RString FindPicture(RString name)
{
	// name "" used for NULL texture
	if (name.GetLength() == 0) return name;

	// ignore @ - not used now
	if (name[0] == '@') name = (const char *)name + 1;

	name.Lower();

	// find in mission
	RString fullname = GetMissionDirectory() + name;
	if (QIFStreamB::FileExist(fullname)) return fullname;
	
	// find in campaign
	fullname = BaseDirectory + RString("dtaExt\\") + name;
	if (QIFStreamB::FileExist(fullname)) return fullname;
	
	// find in root
	fullname = RString("dtaExt\\") + name;
	if (QIFStreamB::FileExist(fullname)) return fullname;
	
	// find in bank
	fullname = GetPictureName(name);
	if (QIFStreamB::FileExist(fullname)) return fullname;

	WarningMessage("Picture %s not found", (const char *)name);
	return "";
}

//! find description of resource based title effect in configuration files
/*!
	Description is subclass of class RscTitles.
	Order of searching:
	- description.ext of current mission
	- description.ext of current campaign
	- global resource
	\param name name of subclass
	\return description subclass
	\patch 1.21 Date 08/22/2001 by Jirka
	- Added: "RscTitles" section in description.ext
*/
const ParamEntry *FindRscTitle(RString name)
{
	// find in mission
	const ParamEntry *cls = ExtParsMission.FindEntry("RscTitles");
	const ParamEntry *entry = cls ? cls->FindEntry(name) : NULL;
	if (entry) return entry;
	
	// find in campaign
	cls = ExtParsCampaign.FindEntry("RscTitles");
	entry = cls ? cls->FindEntry(name) : NULL;
	if (entry) return entry;

	// find in resource
	cls = Res.FindEntry("RscTitles");
	entry = cls ? cls->FindEntry(name) : NULL;
	if (entry) return entry;

	WarningMessage("Resource title %s not found", (const char *)name);
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////
// campaign description.ext

//! sets base directory
/*!
	Also reads new campaign stringtable and campaign description.ext file.
\patch 1.91 Date 12/2/2002 by Jirka
- Fixed: When MP mission was played after resistance campaign, weapon pool was available in MP.
*/

void SetBaseDirectory(RString dir)
{
	BaseDirectory = dir;
	ExtParsCampaign.Clear();
	if (BaseDirectory.GetLength()>0)
	{
		// campaign stringtable
		RString filename = BaseDirectory + RString("stringtable.csv"); 
		LoadStringtable("Campaign", filename, 1, true);
		// campaign description
		filename = BaseDirectory + RString("description.ext"); 
		if (QIFStreamB::FileExist(filename))
			ExtParsCampaign.Parse(filename);
	}
}

//! sets base subdirectory
void SetBaseSubdirectory(RString dir)
{
	BaseSubdirectory = dir;
}

//! sets current campaign
void SetCampaign(RString name)
{
	CurrentCampaign = name;
	if (name.GetLength() == 0)
		SetBaseDirectory("");
	else
		SetBaseDirectory(GetCampaignDirectory(name));
}

////////////////////////////////////////////////////////////////////////////////
// mission description.ext

RString MPMissionsDir("mpmissions\\");
RString AnimsDir("anims\\");
RString MissionsDir("missions\\");

//! sets current mission
/*!
	Also reads new mission stringtable and mission description.ext file.
*/
void SetMission(RString world, RString mission, RString subdir)
{
	strcpy(Glob.header.worldname, world);
	strcpy(Glob.header.filename, mission);
	Glob.header.filenameReal = mission;

	BaseSubdirectory = subdir;
	ExtParsMission.Clear();
	// if (mission.GetLength() == 0) return; // do not create directory ".Eden" etc.
	// directory may not exist (multiplayer client)
	// mkdir(GetMissionDirectory(), NULL);
	// mission stringtable
	RString filename = GetMissionDirectory() + RString("stringtable.csv");
	LoadStringtable("Mission", filename, 2, true);
/*
	MissionStringTable.Clear();
	if (QIFStreamB::FileExist(filename))
		MissionStringTable.Load(filename);
*/
	// mission description
	filename = GetMissionDirectory() + RString("description.ext");
	if (QIFStreamB::FileExist(filename))
		ExtParsMission.Parse(filename);
}

//! sets current mission (used for singleplayer missions only)
void SetMission(RString world, RString mission)
{
	SetMission(world, mission, MissionsDir);
}

////////////////////////////////////////////////////////////////////////////////
// single mission bank

//! reads packed mission file (pbo) and create bank
RString CreateSingleMissionBank(RString filename)
{
	// suppose filename is without extension (.pbo)

	// remove bank
	const char *prefix = "missions\\__cur_sp.";
	int prefixLen = strlen(prefix);
	for (int i=0; i<GFileBanks.Size();)
	{
		QFBank &bank = GFileBanks[i];
		if (strnicmp(bank.GetPrefix(), prefix, prefixLen) == 0)
			GFileBanks.Delete(i);
		else
			i++;
	}

	// extract island name
	const char *ext = strrchr(filename, '.');
	if (!ext) return "";
	RString island = ext + 1;

	// create bank
	int index = GFileBanks.Add();
	QFBank &bank = GFileBanks[index];
	bank.open(filename);
	RString str = 
		RString(prefix) + island + RString("\\");
	str.Lower();
	bank.SetPrefix(str);
	return str;
}

///////////////////////////////////////////////////////////////////////////////
// Interface

struct CountedString : SerializeClass
{
	//! name
	RString name;
	//! count	
	int count;

	LSError Serialize(ParamArchive &ar);
};
TypeIsMovable(CountedString)

LSError CountedString::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("name", name, 1))
	CHECK(ar.Serialize("count", count, 1))
	return LSOK;
}

//! mission entry in campaign progress (sqc) file
struct MissionHistory
{
	//! internal name of mission (name of class in decription.ext)
	RString missionName;
	//! readable name of mission
	RString displayName;
	//! if mission was completed
	bool completed;
	//! summary statistics
	AIStatsCampaign stats;
	//! weapons pool
	AutoArray<CountedString> weapons;
	//! weapons pool
	AutoArray<CountedString> magazines;
	//! dead units
	AutoArray<RString> dead;

	//! constructor
	MissionHistory() {missionName = ""; displayName = ""; completed = false;}
	//! serialization
	LSError Serialize(ParamArchive &ar);
	//! add weapons to pool
	void AddWeapons(RString name, int count);
	//! add magazines to pool
	void AddMagazines(RString name, int count);
};
TypeIsGeneric(MissionHistory);

LSError MissionHistory::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("missionName", missionName, 1))
	CHECK(ar.Serialize("displayName", displayName, 1))
	CHECK(ar.Serialize("completed", completed, 1))
	if (ar.GetArVersion() >= 2)
	{
		ParamArchive arSubcls;
		if (!ar.OpenSubclass("Stats", arSubcls)) return LSStructure;
		stats.CampaignSerialize(arSubcls);
	}
	else if (ar.IsLoading()) stats.Clear();
	CHECK(ar.Serialize("Weapons", weapons, 1))
	CHECK(ar.Serialize("Magazines", magazines, 1))
	CHECK(ar.SerializeArray("Dead", dead, 1))
	return LSOK;
}

//! battle entry in campaign progress (sqc) file
struct BattleHistory
{
	//! internal name of battle (name of class in decription.ext)
	RString battleName;
	//! played misssions
	AutoArray<MissionHistory> missions;

	//! constructor
	BattleHistory() {battleName = "";}
	//! serialization
	LSError Serialize(ParamArchive &ar);
};
TypeIsMovable(BattleHistory);

LSError BattleHistory::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("battleName", battleName, 1))
	CHECK(ar.Serialize("Missions", missions, 1))
	return LSOK;
}

//! campaign progress description (actual content of sqc file)
struct CampaignHistory : public SerializeClass
{
	//! internal name of campaign (name of campaign directory)
	RString campaignName;
	//! played battles
	AutoArray<BattleHistory> battles;

	//! constructor
	CampaignHistory() {campaignName = "";}
	//! initialization
	void Clear(RString campaign) {campaignName = campaign; battles.Clear();}
	//! Return current mission entry
	MissionHistory *CurrentMission();
	//! adds mission entry
	/*!
		\param displayName readable name of mission
	*/
	void AddMission(RString campaign, RString battle, RString mission, RString displayName);
//	void SetDisplayName(RString campaign, RString battle, RString mission, RString displayName);
	//! declare given mission as completed
	void MissionCompleted(RString campaign, RString battle, RString mission);
	//! Creates weapon pool
	void LoadWeaponPool(WeaponsInfo &pool);
	//! Stores weapon pool
	void SaveWeaponPool(WeaponsInfo &pool);

	//! serialization
	LSError Serialize(ParamArchive &ar);
};
TypeIsMovable(CampaignHistory);

void CampaignHistory::AddMission(RString campaign, RString battle, RString mission, RString displayName)
{
	if (!campaign || campaign.GetLength() <= 0) return;
	if (stricmp(campaign, campaignName) != 0) return;
	int i = battles.Size() - 1;
	if (i < 0 || stricmp(battles[i].battleName, battle) != 0)
	{
		i = battles.Add();
		battles[i].battleName = battle;
	}
	BattleHistory &bh = battles[i];

	// check if mission already added
	i = bh.missions.Size() - 1;
	if (i >= 0 && mission == bh.missions[i].missionName) return;

	i = bh.missions.Add();
	bh.missions[i].missionName = mission;
	bh.missions[i].displayName = displayName;
	bh.missions[i].stats = GStats._campaign;

//LogF("Campaign: Mission %s:%s:%s added", (const char *)campaign, (const char *)battle, (const char *)mission);
}

/*
void CampaignHistory::SetDisplayName(RString campaign, RString battle, RString mission, RString displayName)
{
	if (!campaign || campaign.GetLength() <= 0) return;
	if (stricmp(campaign, campaignName) != 0) return;
	int i = battles.Size() - 1;
	if (i < 0) return;
	BattleHistory &bh = battles[i];
	if (stricmp(bh.battleName, battle) != 0) return;
	i = bh.missions.Size() - 1;
	if (i < 0) return;
	MissionHistory &mh = bh.missions[i];
	if (stricmp(mh.missionName, mission) != 0) return;
	mh.displayName = displayName;
}
*/

MissionHistory *CampaignHistory::CurrentMission()
{
	int i = battles.Size() - 1;
	if (i < 0) return NULL;
	BattleHistory &bh = battles[i];
	i = bh.missions.Size() - 1;
	if (i < 0)
	{
		Fail("Empty battle");
		return NULL;
	}
	return &bh.missions[i];
}

void CampaignHistory::MissionCompleted(RString campaign, RString battle, RString mission)
{
	if (!campaign || campaign.GetLength() <= 0) return;
	if (stricmp(campaign, campaignName) != 0) return;
	int i = battles.Size() - 1;
	if (i < 0) return;
	BattleHistory &bh = battles[i];
	if (stricmp(bh.battleName, battle) != 0) return;
	i = bh.missions.Size() - 1;
	if (i < 0) return;
	MissionHistory &mh = bh.missions[i];
	if (stricmp(mh.missionName, mission) != 0) return;
	mh.completed = true;
//LogF("Campaign: Mission %s:%s:%s completed", (const char *)campaign, (const char *)battle, (const char *)mission);
}

LSError CampaignHistory::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("campaignName", campaignName, 1))
	CHECK(ar.Serialize("Battles", battles, 1))
	return LSOK;
}

void CampaignHistory::LoadWeaponPool(WeaponsInfo &pool)
{
	MissionHistory *mh = CurrentMission();
RptF("Campaign: Loading weapon pool of mission %s", mh ? (const char *)mh->missionName : "<none>");
	if (!mh) return;

	for (int i=0; i<mh->weapons.Size(); i++)
	{
		Ref<WeaponType> weapon = WeaponTypes.New(mh->weapons[i].name);
		if (!weapon) continue;
		int count = mh->weapons[i].count;
		for (int j=0; j<count; j++) pool._weaponsPool.Add(weapon);
	}
	for (int i=0; i<mh->magazines.Size(); i++)
	{
		Ref<MagazineType> type = MagazineTypes.New(mh->magazines[i].name);
		if (!type) continue;
		int count = mh->magazines[i].count;
		for (int j=0; j<count; j++)
		{
			Ref<Magazine> magazine = new Magazine(type);
			magazine->_ammo = type->_maxAmmo;
			magazine->_reloadMagazine = 0;
			magazine->_reload = 0;
			if (type->_modes.Size() > 0)
			{
				float	reload = 0.8 + 0.2 * GRandGen.RandomValue();
				magazine->_reload = reload * type->_modes[0]->_reloadTime;
			}
			pool._magazinesPool.Add(magazine);
		}
	}

}

void MissionHistory::AddWeapons(RString name, int count)
{
	for (int j=0; j<weapons.Size(); j++)
	{
		if (stricmp(weapons[j].name, name) == 0)
		{
			weapons[j].count += count;
			if (weapons[j].count <= 0) weapons.Delete(j);
			return;
		}
	}
	if (count <= 0) return;
	int index = weapons.Add();
	weapons[index].name = name;
	weapons[index].count = count;
}

void MissionHistory::AddMagazines(RString name, int count)
{
	for (int j=0; j<magazines.Size(); j++)
	{
		if (stricmp(magazines[j].name, name) == 0)
		{
			magazines[j].count += count;
			if (magazines[j].count <= 0) magazines.Delete(j);
			return;
		}
	}
	if (count <= 0) return;
	int index = magazines.Add();
	magazines[index].name = name;
	magazines[index].count = count;
}

void CampaignHistory::SaveWeaponPool(WeaponsInfo &pool)
{
	MissionHistory *mh = CurrentMission();
//RptF("Campaign: Saving weapon pool of mission %s", mh ? (const char *)mh->missionName : "<none>");
	if (!mh) return;

	mh->weapons.Clear();
	for (int i=0; i<pool._weaponsPool.Size(); i++)
	{
		RString name = pool._weaponsPool[i]->GetName();
		mh->AddWeapons(name, 1);
	}

	mh->magazines.Clear();
	for (int i=0; i<pool._magazinesPool.Size(); i++)
	{
		RString name = pool._magazinesPool[i]->_type->GetName();
		mh->AddMagazines(name, 1);
	}
}

//! current campaign progress description
CampaignHistory GCampaignHistory;

void CampaignSaveWeaponPool(WeaponsInfo &pool)
{
	GCampaignHistory.SaveWeaponPool(pool);
}

void CampaignLoadWeaponPool(WeaponsInfo &pool)
{
	GCampaignHistory.LoadWeaponPool(pool);
}

/*!
\patch 1.57 Date 5/15/2002 by Jirka
- Improved: Identitied killed in player group doesn't appear in next missions in campaign
*/

bool IsIdentityDead(RString identity)
{
	MissionHistory *mh = GCampaignHistory.CurrentMission();
	if (!mh) return false;

	for (int i=0; i<mh->dead.Size(); i++)
	{
		if (mh->dead[i] == identity) return true;
	}
	return false;
}

void EmptyDeadIdentities()
{
	MissionHistory *mh = GCampaignHistory.CurrentMission();
	if (mh) mh->dead.Clear();
}

void AddDeadIdentity(RString identity)
{
	MissionHistory *mh = GCampaignHistory.CurrentMission();
	if (mh) mh->dead.Add(identity);
}

Object *GetObject(GameValuePar oper);

/*!
\patch 1.81 Date 8/8/2002 by Jirka
- Fixed: Bug in functions addWeaponPool, addMagazinePool
*/

GameValue PoolAddWeapon(const GameState *state, GameValuePar oper1)
{
	const GameArrayType &array = oper1;
	if
	(
		array.Size() != 2 ||
		array[0].GetType() != GameString ||
		array[1].GetType() != GameScalar
	) return GameValue();

	RString name = array[0];
	int count = toInt((float)array[1]);

	int i = GCampaignHistory.battles.Size() - 1;
	if (i < 0) return GameValue();
	BattleHistory &bh = GCampaignHistory.battles[i];
	i = bh.missions.Size() - 1;
	if (i < 0) return GameValue();
	MissionHistory &mh = bh.missions[i];
	mh.AddWeapons(name, count);

	return GameValue();
}

GameValue PoolAddMagazine(const GameState *state, GameValuePar oper1)
{
	const GameArrayType &array = oper1;
	if
	(
		array.Size() != 2 ||
		array[0].GetType() != GameString ||
		array[1].GetType() != GameScalar
	) return GameValue();

	RString name = array[0];
	int count = toInt((float)array[1]);

	int i = GCampaignHistory.battles.Size() - 1;
	if (i < 0) return GameValue();
	BattleHistory &bh = GCampaignHistory.battles[i];
	i = bh.missions.Size() - 1;
	if (i < 0) return GameValue();
	MissionHistory &mh = bh.missions[i];
	mh.AddMagazines(name, count);

	return GameValue();
}

GameValue PoolGetWeapons(const GameState *state, GameValuePar oper1)
{
	Object *obj = GetObject(oper1);
	if (!obj) return GameValue();

	VehicleSupply *veh = dyn_cast<VehicleSupply>(obj);
	if (!veh) return GameValue();

	int i = GCampaignHistory.battles.Size() - 1;
	if (i < 0) return GameValue();
	BattleHistory &bh = GCampaignHistory.battles[i];
	i = bh.missions.Size() - 1;
	if (i < 0) return GameValue();
	MissionHistory &mh = bh.missions[i];

	// veh->ClearWeaponCargo();
	// veh->ClearMagazineCargo();

	for (int i=0; i<mh.weapons.Size(); i++)
	{
		RString name = mh.weapons[i].name;
		Ref<WeaponType> weapon = WeaponTypes.New(name);
		if (!weapon) continue;
		veh->AddWeaponCargo(weapon, mh.weapons[i].count);
	}

	for (int i=0; i<mh.magazines.Size(); i++)
	{
		RString name = mh.magazines[i].name;
		Ref<MagazineType> magazine = MagazineTypes.New(name);
		if (!magazine) continue;
		veh->AddMagazineCargo(magazine, mh.magazines[i].count);
	}

	mh.weapons.Clear();
	mh.magazines.Clear();

	return GameValue();
}

GameValue PoolSetWeapons(const GameState *state, GameValuePar oper1)
{
	Object *obj = GetObject(oper1);
	if (!obj) return GameValue();

	VehicleSupply *veh = dyn_cast<VehicleSupply>(obj);
	if (!veh) return GameValue();

	int i = GCampaignHistory.battles.Size() - 1;
	if (i < 0) return GameValue();
	BattleHistory &bh = GCampaignHistory.battles[i];
	i = bh.missions.Size() - 1;
	if (i < 0) return GameValue();
	MissionHistory &mh = bh.missions[i];

	// mh.weapons.Clear();
	// mh.magazines.Clear();

	for (int i=0; i<veh->GetWeaponCargoSize(); i++)
	{
		const WeaponType *weapon = veh->GetWeaponCargo(i);
		if (!weapon) continue;
		RString name = weapon->GetName();
		mh.AddWeapons(name, 1);
	}

	for (int i=0; i<veh->GetMagazineCargoSize(); i++)
	{
		const Magazine *magazine = veh->GetMagazineCargo(i);
		if (!magazine) continue;
		RString name = magazine->_type->GetName();
		mh.AddMagazines(name, 1);
	}

	veh->ClearWeaponCargo();
	veh->ClearMagazineCargo();

	return GameValue();
}

GameValue PoolClearWeapons(const GameState *state)
{
	int i = GCampaignHistory.battles.Size() - 1;
	if (i < 0) return GameValue();
	BattleHistory &bh = GCampaignHistory.battles[i];
	i = bh.missions.Size() - 1;
	if (i < 0) return GameValue();
	MissionHistory &mh = bh.missions[i];

	mh.weapons.Clear();

	return GameValue();
}

GameValue PoolClearMagazines(const GameState *state)
{
	int i = GCampaignHistory.battles.Size() - 1;
	if (i < 0) return GameValue();
	BattleHistory &bh = GCampaignHistory.battles[i];
	i = bh.missions.Size() - 1;
	if (i < 0) return GameValue();
	MissionHistory &mh = bh.missions[i];

	mh.magazines.Clear();

	return GameValue();
}

GameValue PoolQueryWeapons(const GameState *state, GameValuePar oper1)
{
	RString name = oper1;

	int i = GCampaignHistory.battles.Size() - 1;
	if (i < 0) return 0.0f;
	BattleHistory &bh = GCampaignHistory.battles[i];
	i = bh.missions.Size() - 1;
	if (i < 0) return 0.0f;
	MissionHistory &mh = bh.missions[i];

	for (int j=0; j<mh.weapons.Size(); j++)
	{
		if (stricmp(mh.weapons[j].name, name) == 0)
		{
			return (float)mh.weapons[j].count;
		}
	}
	return 0.0f;
}

GameValue PoolQueryMagazines(const GameState *state, GameValuePar oper1)
{
	RString name = oper1;

	int i = GCampaignHistory.battles.Size() - 1;
	if (i < 0) return 0.0f;
	BattleHistory &bh = GCampaignHistory.battles[i];
	i = bh.missions.Size() - 1;
	if (i < 0) return 0.0f;
	MissionHistory &mh = bh.missions[i];

	for (int j=0; j<mh.magazines.Size(); j++)
	{
		if (stricmp(mh.magazines[j].name, name) == 0)
		{
			return (float)mh.magazines[j].count;
		}
	}
	return 0.0f;
}

//! saves current campaign progress description into file
LSError SaveMission(const char *filename)
{
//MissionHistory *mh = GCampaignHistory.CurrentMission();
//RptF("Campaign: Saving mission %s to %s", mh ? (const char *)mh->missionName : "<none>", filename);

	ParamArchiveSave ar(CampaignVersion);
	CHECK(ar.Serialize("Campaign", GCampaignHistory, 1))
#if _ENABLE_CHEATS
	CHECK(ar.Save(filename))
#else
	if (!ar.SaveBin(filename)) return LSUnknownError;
#endif
	return LSOK;
}

//! load current campaign progress description from file
LSError LoadMission(const char *filename)
{
	ParamArchiveLoad ar;
	if (!ar.LoadBin(filename) && ar.Load(filename) != LSOK) return LSUnknownError;
	CHECK(ar.Serialize("Campaign", GCampaignHistory, 1))

//MissionHistory *mh = GCampaignHistory.CurrentMission();
//RptF("Campaign: Loading mission %s from %s", mh ? (const char *)mh->missionName : "<none>", filename);
		
	return LSOK;
}

/*
void SetMissionDisplayName(RString campaign, RString battle, RString mission, RString displayName)
{
	GCampaignHistory.SetDisplayName(campaign, battle, mission, displayName);
	RString filename = GetTmpSaveDirectory() + campaign + RString(".sqc");
	GCampaignHistory.campaignName = campaign;
	SaveMission(filename);
}
*/

//! adds mission entry into current campaign progress description
void AddMission(RString campaign, RString battle, RString mission, RString displayName)
{
	GStats.ClearMission();
/*
	GCampaignHistory.AddMission(campaign, battle, mission, displayName);
	RString filename = GetTmpSaveDirectory() + campaign + RString(".sqc");
	GCampaignHistory.campaignName = campaign;
	SaveMission(filename);
*/
	RString filename = GetTmpSaveDirectory() + campaign + RString(".sqc");

	// Transfer weapons pool to next mission
	WeaponsInfo pool;
	CampaignLoadWeaponPool(pool);
	LoadMission(filename);
	GCampaignHistory.AddMission(campaign, battle, mission, displayName);
	GCampaignHistory.campaignName = campaign;
	CampaignSaveWeaponPool(pool);
	SaveMission(filename);
}

//! NOT USED CURRENTLY
struct ContinueInfo
{
	Rank rank;
	float time;
	RString island;
	RString mission;

	LSError Serialize(ParamArchive &ar);
};

//bool IsHeader();
//void SaveHeader();

//! Display for selection of single mission
class DisplaySingleMission : public Display
{
protected:
	//! wanted exit code (exit is performed after close animation is finished)
	int _exitWhenClose;

	RString _directory;

public:
	//! selected difficulty
	bool _cadetMode;

public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplaySingleMission(ControlsContainer *parent);
	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	void OnLBSelChanged(int idc, int curSel);
	void OnLBDblClick(int idc, int curSel);
	void OnButtonClicked(int idc);
	void OnCtrlClosed(int idc);
	RString GetDirectory() {return _directory;}

protected:
	//! perform updates (show / hide buttons etc.) when mission selection is changed
	void OnChangeMission();
	//! perform updates (show / hide buttons etc.) when difficulty is changed
	void OnChangeDifficulty();
	//! load options for display from user profile
	void LoadParams();
	//! save options for display to user profile
	void SaveParams();
	//! reloads directory content
	void LoadDirectory();
};

// Revert display
//! Display for warning if revert in campaign (or restart of campaign) selected
class DisplayRevert : public Display
{
protected:
	//! wanted exit code (exit is performed after close animation is finished)
	int _exitWhenClose;

public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayRevert(ControlsContainer *parent);

	void OnButtonClicked(int idc);
	void OnSimulate(EntityAI *vehicle);
};

//! "Campaign book" display
class DisplayCampaignLoad : public Display
{
protected:
	//! wanted exit code (exit is performed after close animation is finished)
	int _exitWhenClose;
	//! parsed description.ext of selected campaign
	ParamFile _cfg;

public:
	//! campaign progress descriptions for all found campaigns
	AutoArray<CampaignHistory> _campaigns;
	//! currently selected campaign
	int _currentCampaign;
	//! selected difficulty
	bool _cadetMode;
	//! selected difficulty
	bool _showStatistics;

public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayCampaignLoad(ControlsContainer *parent);

	void OnButtonClicked(int idc);
	void OnLBSelChanged(int idc, int curSel);
	void OnLBDblClick(int idc, int curSel);
	void OnCtrlClosed(int idc);
	void OnSimulate(EntityAI *vehicle);
	void OnChildDestroyed(int idd, int exit);

protected:
	//! preloads informations abaut all found campaigns
	void LoadCampaigns();

	//! perform display updates when campaign selection is changed
	void OnChangeCampaign();
//	void OnChangeBattle();
	//! perform display updates when campaign selection is changed
	void OnChangeMission();
	//! perform display updates when difficulty is changed
	void OnChangeDifficulty();
	//! load options for display from user profile
	void LoadParams();
	//! save options for display to user profile
	void SaveParams();

	//! handle campaign cheat (public all missions in campaign into campaign progress description)
	void OnCampaignCheat();

	//! show / hide statistics controls
	void ShowStatistics(bool show);

	//! update overall campaign statistics
	void UpdateStatistics(AIStatsCampaign *begin, AIStatsCampaign *end);
};

// Game display
//! NOT USED CURRENTLY
class DisplayGame : public Display
{
public:
	DisplayGame(ControlsContainer *parent)
		: Display(parent)
	{
		Load("RscDisplayGame");
	}

	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	bool CanDestroy();
};

//! NOT USED CURRENTLY
class DisplaySave : public Display
{
public:
	DisplaySave(ControlsContainer *parent)
		: Display(parent)
	{
		Load("RscDisplaySave");
	}

	bool CanDestroy();
};

//! Options display
class DisplayOptions : public Display
{
protected:
	//! wanted exit code (exit is performed after close animation is finished)
	int _exitWhenClose;

public:
/*
	float _oldBright;
	float _oldGamma;
	float _oldFrameRate;
	float _oldQuality;

	float _oldMusic;
	float _oldEffects;
	float _oldVoices;

	bool _oldTitles;
	bool _oldRadio;
*/

	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayOptions(ControlsContainer *parent, bool enableSimulation, bool credits);
//	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	void OnButtonClicked(int idc);
	void OnChildDestroyed(int idd, int exit);
//	void OnSliderPosChanged(int idc, float pos);
	void OnCtrlClosed(int idc);
};

//! NOT USED CURRENTLY
class DisplayCredits : public Display
{
public:
	DisplayCredits(ControlsContainer *parent)
		: Display(parent)
	{
		Load("RscDisplayCredits");
	}
};

//! Video options display
class DisplayOptionsVideo : public Display
{
public:
	//! original value of Brightness
	float _oldBright;
	//! original value of Gamma correction
	float _oldGamma;
	//! original value of Frame rate
	float _oldFrameRate;
	//! original value of Visual quality
	float _oldQuality;
	//! original value of Visibility
	float _oldVisibility;
	//! original value of Terrain interpolation
	float _oldTerrain;

	//@{
	//! original value of Resolution
	int _oldResX,_oldResY,_oldBpp;
	bool _oldWindowed;
	//@}
	//! original value of Refresh rate
	int _oldRefreshRate;

	//! original value of Object shadows (enabled / disabled)
	bool _oldObjShadows;
	//! original value of Vehicle shadows (enabled / disabled)
	bool _oldVehShadows;
	//! original value of Cloudlets (enabled / disabled)
	bool _oldSmokes;
	//! original value of Blood (enabled / disabled)
	bool _oldBlood;
	//! original value of Occlusions (enabled / disabled)
	bool _oldOcclusions;
	//! original value of Hardware T & L (enabled / disabled)
	bool _oldHWTL;
	//! original value of Multitexturing (enabled / disabled)
	bool _oldMultitexturing;
	//! original value of W-buffer (enabled / disabled)
	bool _oldWBuffer;

	//! available resolutions
	FindArray<ResolutionInfo> _resolutions;
	//! available refresh rates for selected resolution
	FindArray<int> _refreshRates;

	//! constructor
	/*!
		\param parent parent display
		\param enableSimulation enable game simulation
		- true if called form main menu
		- false in case of in game invocation
	*/
	DisplayOptionsVideo(ControlsContainer *parent, bool enableSimulation);
	Control *OnCreateCtrl(int type, int		 idc, const ParamEntry &cls);
	void OnButtonClicked(int idc);
	void OnSliderPosChanged(int idc, float pos);
	void OnLBSelChanged(int idc, int curSel);

protected:
	//! init list of available resolutions
	int InitResolutions();
	//! update list of available refresh rates for given resolution
	void UpdateRefreshRates(int res);
	void UpdateTerrainGrids();
};

//! Audio options display
class DisplayOptionsAudio : public Display
{
public:
	//! original value of music volume
	float _oldMusic;
	//! original value of effects volume
	float _oldEffects;
	//! original value of voices volume
	float _oldVoices;
	// _oldSamplingRate;
	//! original value of Hardware acceleration (enabled / disabled)
	bool _oldHWAcc;
	//! original value of EAX (enabled / disabled)
	bool _oldEAX;
	//! original value of limited number of voices (enabled / disabled)
	bool _oldSingleVoice;

	//! constructor
	/*!
		\param parent parent display
		\param enableSimulation enable game simulation
		- true if called form main menu
		- false in case of in game invocation
	*/
	DisplayOptionsAudio(ControlsContainer *parent, bool enableSimulation);
	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	void OnButtonClicked(int idc);
	void OnSliderPosChanged(int idc, float pos);
};

//! Difficulties selection control
class CDifficulties : public C3DListBox
{
public:
	//! current settings for Cadet mode
	bool cadetDifficulty[DTN];
	//! current settings for Veteran mode
	bool veteranDifficulty[DTN];

protected:
	//! selected column
	int _column;

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	CDifficulties(ControlsContainer *parent, int idc, const ParamEntry &cls);

	bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	void OnLButtonDown(float x, float y);

	void DrawItem
	(
		Vector3Par position, Vector3Par down, int i, float alpha
	);

protected:
	//! toggle current value (enabled / disabled)
	/*!
		\param column cadet / veteran settings
		\param index index of settings
	*/
	void Toggle(int column, int index)
	{
		if (column == 1)
		{
			cadetDifficulty[index] = !cadetDifficulty[index];
		}
		else if (column == 2 && Glob.config.diffDesc[index].enabledInVeteran)
		{
			veteranDifficulty[index] = !veteranDifficulty[index];
		}
	}
};

//! Difficulty display
class DisplayDifficulty : public Display
{
public:
	//! Difficulties selection control
	CDifficulties *_diff;

	//! original value of Show subtitles (enabled / disabled)
	bool _oldTitles;
	//! original value of Show Radio subtitles (enabled / disabled)
	bool _oldRadio;

	//! constructor
	/*!
		\param parent parent display
		\param enableSimulation enable game simulation
		- true if called form main menu
		- false in case of in game invocation
	*/
	DisplayDifficulty(ControlsContainer *parent, bool enableSimulation);
	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	void OnButtonClicked(int idc);
};

///////////////////////////////////////////////////////////////////////////////
// Implementation

//! starts next mission in campaign
void StartMission(Display *disp, bool newBattle)
{
	const ParamEntry &battleCls = ExtParsCampaign>>"Campaign">>CurrentBattle;
	CurrentTemplate.Clear();

	disp->ForceExit(-1);
	if (newBattle)
	{
		RString intro = battleCls >> "cutscene";
		disp->CreateChild(new DisplayCampaignIntro(disp, intro));
	}
	else
	{
		const ParamEntry &missionCls = battleCls>>CurrentMission;

/*
		// enable / disable HUD, radio, map etc.
		if (GWorld->UI())
		{
			int showHUD = missionCls >> "showHUD";
			bool bShowHUD = showHUD != 0;
			GWorld->UI()->ShowAll(bShowHUD);
			GWorld->UI()->ShowMessage(UIMsgCenter, bShowHUD);
			GWorld->UI()->ShowMessage(UIMsgGroup, bShowHUD);
			GWorld->UI()->ShowMessage(UIMsgVehicle, bShowHUD);
			GWorld->UI()->ShowMessage(UIMsgHelp, bShowHUD);
		}
*/

		RString epizode = missionCls>>"template";
		disp->CreateChild(new DisplayIntro(disp, epizode));
	}
}

// Macrovision CD Protection
void __cdecl CDPPlayAward(Display *disp, RString name, const ParamEntry &cls);
CDAPFN_DECLARE_GLOBAL(CDPPlayAward, CDAPFN_OVERHEAD_L3, CDAPFN_CONSTRAINT_NONE);

void __cdecl CDPPlayAward(Display *disp, RString name, const ParamEntry &cls)
{
	RString dir = GetTmpSaveDirectory();
	char buffer[256];
	sprintf(buffer, "%s%s.sqc", (const char *)dir, (const char *)CurrentCampaign);
	GCampaignHistory.campaignName = CurrentCampaign;
	SaveMission(buffer);
	// play it
	RString cutscene = cls >> Glob.header.worldname;
	disp->CreateChild(new DisplayAward(disp, cutscene));

	CDAPFN_ENDMARK(CDPPlayAward);

/*
#if _CZECH
	PerformRandomRoxxeTest_003(CDDrive);
#endif
*/
}

//! checks if award / penalty cutscene will be performed
/*!
\patch 1.03 Date 7/12/2001 by Jirka
	- Fixed: show last award/penalty cutscenes in more can be played
	- play the last one, mark all as played
*/
static bool CheckAward(Display *disp)
{
	// check if awards disabled
	const ParamEntry &campaignCls = ExtParsCampaign >> "Campaign";
	const ParamEntry &battleCls = campaignCls >> CurrentBattle;
	const ParamEntry &missionCls = battleCls >> CurrentMission;
	if (missionCls.FindEntry("noAward") && (bool)(missionCls >> "noAward"))
		return false;

	float oldScore = GStats._campaign._lastScore;
	float newScore = GStats._campaign._score;
	if (newScore < oldScore)
	{
		// penalties
		const ParamEntry *penalties = ExtParsCampaign.FindEntry("Penalties");
		if (penalties)
		{
			// fix - play cutscene with minimal score, mark all
			int best = INT_MAX;
			int bestIndex = -1;

			for (int i=0; i<penalties->GetEntryCount(); i++)
			{
				const ParamEntry &cls = penalties->GetEntry(i);
				float limit = cls >> "limit";
				if (newScore <= limit)
				{
					RString name = cls.GetName();
					// check if cutscene was played before
					bool found = false;
					for (int j=0; j<GStats._campaign._penalties.Size(); j++)
					{
						if (GStats._campaign._penalties[j] == name)
						{
							found = true;
							break;
						}
					}
					if (!found)
					{
						// update campaign history
						GStats._campaign._penalties.Add(name);
						// FIX
						/*
						CDPPlayAward(disp, name, cls);
						return true;
						*/
						if (limit < best)
						{
							bestIndex = i;
							best = (int)limit;
						}
					}
				}
			}
			// FIX
			if (bestIndex >= 0)
			{
				const ParamEntry &cls = penalties->GetEntry(bestIndex);
				RString name = cls.GetName();
				CDPPlayAward(disp, name, cls);
				return true;
			}
		}
	}
	else if (newScore > oldScore)
	{
		// awards
		const ParamEntry *awards = ExtParsCampaign.FindEntry("Awards");
		if (awards)
		{
			// fix - play cutscene with maximal score, mark all
			int best = INT_MAX;
			int bestIndex = -1;

			for (int i=0; i<awards->GetEntryCount(); i++)
			{
				const ParamEntry &cls = awards->GetEntry(i);
				float limit = cls >> "limit";
				if (newScore >= limit)
				{
					RString name = cls.GetName();
					// check if cutscene was played before
					bool found = false;
					for (int j=0; j<GStats._campaign._awards.Size(); j++)
					{
						if (GStats._campaign._awards[j] == name)
						{
							found = true;
							break;
						}
					}
					if (!found)
					{
						// update campaign history
						GStats._campaign._awards.Add(name);
						// FIX
						/*
						CDPPlayAward(disp, name, cls);
						return true;
						*/
						if (limit > best)
						{
							bestIndex = i;
							best = (int)limit;
						}
					}
				}
			}
			// FIX
			if (bestIndex >= 0)
			{
				const ParamEntry &cls = awards->GetEntry(bestIndex);
				RString name = cls.GetName();
				CDPPlayAward(disp, name, cls);
				return true;
			}
		}
	}
	return false;
}

//! find next mission in campaign and start it
static bool NextMission(Display *disp, EndMode mode)
{
	SetBaseDirectory(GetCampaignDirectory(CurrentCampaign));
	GCampaignHistory.MissionCompleted(CurrentCampaign, CurrentBattle, CurrentMission);
	RString filename = GetTmpSaveDirectory() + CurrentCampaign + RString(".sqc");
	SaveMission(filename);

	const ParamEntry &campaignCls = ExtParsCampaign>>"Campaign";
	const ParamEntry &battleCls = campaignCls>>CurrentBattle;
	const ParamEntry &missionCls = battleCls>>CurrentMission;

	if (missionCls.FindEntry("noAward") && (bool)(missionCls >> "noAward"))
	{
		// award cutscene disabled
	}
	else
	{
		if (ExtParsCampaign.FindEntry("exitScore"))
		{
			float exitScore = ExtParsCampaign >> "exitScore";
			if (GStats._campaign._score <= exitScore)
			{
				// total end of campaign because of terrible score
				SetCampaign("");
				CurrentBattle = "";
				CurrentMission = "";
				return false;
			}
		}
	}
	
	switch (mode)
	{
	case EMLoser:
		CurrentMission = missionCls >> "lost";
		break;
	case EMEnd1:
		CurrentMission = missionCls >> "end1";
		break;
	case EMEnd2:
		CurrentMission = missionCls >> "end2";
		break;
	case EMEnd3:
		CurrentMission = missionCls >> "end3";
		break;
	case EMEnd4:
		CurrentMission = missionCls >> "end4";
		break;
	case EMEnd5:
		CurrentMission = missionCls >> "end5";
		break;
	case EMEnd6:
		CurrentMission = missionCls >> "end6";
		break;
	}

	bool showCampaignDisplay = false;
	if (CurrentMission.GetLength() == 0)
	{
		showCampaignDisplay = true;
		switch (mode)
		{
		case EMLoser:
			CurrentBattle = battleCls >> "lost";
			break;
		case EMEnd1:
			CurrentBattle = battleCls >> "end1";
			break;
		case EMEnd2:
			CurrentBattle = battleCls >> "end2";
			break;
		case EMEnd3:
			CurrentBattle = battleCls >> "end3";
			break;
		case EMEnd4:
			CurrentBattle = battleCls >> "end4";
			break;
		case EMEnd5:
			CurrentBattle = battleCls >> "end5";
			break;
		case EMEnd6:
			CurrentBattle = battleCls >> "end6";
			break;
		}
		if (CurrentBattle.GetLength() == 0)
		{
			SetCampaign("");
			CurrentBattle = "";
			CurrentMission = "";
			return false;
		}
		CurrentMission = campaignCls>>CurrentBattle>>"firstMission";
	}

	RString epizode;
	char buffer[256];
	{
		const ParamEntry &battleCls = campaignCls>>CurrentBattle;
		const ParamEntry &missionCls = battleCls>>CurrentMission;

		epizode = missionCls>>"template";
		if (!ProcessTemplateName(epizode))
		{
			RptF("Invalid mission name %s", (const char *)epizode);
			ErrorMessage("Error in campaign structure");
			return false;
		}

		sprintf
		(
			buffer, "%smission.sqm",
			(const char *)GetMissionDirectory()
		);
		if (!QIFStreamB::FileExist(buffer))
		{
			RptF("Cannot find mission %s", buffer);
			ErrorMessage("Error in campaign structure");
			return false;
		}
	}

	// new campaign index
/*
	GStats.ClearMission();
	GCampaignHistory.AddMission(CurrentCampaign, CurrentBattle, CurrentMission);
	RString dir = GetTmpSaveDirectory();
	sprintf(buffer, "%s%s.sqc", (const char *)dir, (const char *)CurrentCampaign);
	GCampaignHistory.campaignName = CurrentCampaign;
	SaveMission(buffer);
*/
	StartMission(disp, showCampaignDisplay);
	return true;
}

// Interrupt display
// TODO: completly rebuil

DisplayInterrupt::DisplayInterrupt(ControlsContainer *parent)
	: Display(parent)
{
	_enableSimulation = false;
	_enableUI = false;
	Load("RscDisplayInterrupt");

	if (GWorld->GetMode() == GModeNetware)
	{
		GetCtrl(IDC_INT_SAVE)->ShowCtrl(false);
		GetCtrl(IDC_INT_LOAD)->ShowCtrl(false);
		GetCtrl(IDC_INT_RETRY)->ShowCtrl(false);
//		GetCtrl(IDC_INT_RESTART)->ShowCtrl(false);
	}
	else
	{
		RString name = GetSaveDirectory() + RString("save.fps");
		if (QIFStream::FileExists(name))
			GetCtrl(IDC_INT_SAVE)->ShowCtrl(false);
		else
			GetCtrl(IDC_INT_LOAD)->ShowCtrl(false);
	}
	GInput.ChangeGameFocus(+1);
}

DisplayInterrupt::~DisplayInterrupt()
{
	GInput.ChangeGameFocus(-1);
}

Control *DisplayInterrupt::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
	case IDC_INT_TITLE:
		{
			CStatic *text = new CStatic(this, idc, cls);
			if (GWorld->GetMode() == GModeNetware)
				text->SetText(LocalizeString(IDS_DISP_INT_TITLE_MP));
			else
				text->SetText(LocalizeString(IDS_DISP_INT_TITLE));
			return text;
		}
	default:
		return Display::OnCreateCtrl(type, idc, cls);
	}
}

void DisplayInterrupt::OnButtonClicked(int idc)
{
	switch (idc)
	{
		case IDC_INT_OPTIONS:
			CreateChild(new DisplayOptions(this, false, false));
			break;
		default:
			Exit(idc);
			break;
	}
}

void DisplayInterrupt::OnChildDestroyed(int idd, int exit)
{
	Display::OnChildDestroyed(idd, exit);
}

// Main display
/*

LSError ContinueInfo::Serialize(ParamArchive &ar)
{
	CHECK(ar.SerializeEnum("rank", rank, 1))
	CHECK(ar.Serialize("time", time, 1))
	CHECK(ar.Serialize("island", island, 1))
	CHECK(ar.Serialize("mission", mission, 1))
	return LSOK;
}

bool IsHeader()
{
	char buffer[256];
	sprintf(buffer, "%scontinue.sqh", (const char *)GetSaveDirectory());
	return QIFStream::FileExists(buffer);
}

void DisplayMain::LoadHeader()
{
	// TODO: player killed

	char buffer[256];
	sprintf(buffer, "%scontinue.sqh", (const char *)GetSaveDirectory());

	if (QIFStream::FileExists(buffer))
	{
		ParamArchiveLoad ar(buffer);
		ContinueInfo header;
		if (header.Serialize(ar) != LSOK)
		{
			WarningMessage("Cannot load header %s", buffer);
			goto LoadHeaderFailed;
		}

		C3DStatic *text = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MAIN_RANK));
		if (text)
		{
			sprintf
			(
				buffer,
				LocalizeString(IDS_MAIN_RANK),
				(const char *)LocalizeString(IDS_PRIVATE + header.rank)
			);
			text->SetText(buffer);
		}
		text = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MAIN_ISLAND));
		if (text) text->SetText(Pars>>"CfgWorlds">>header.island>>"description");
		text = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MAIN_MISSION));
		if (text)
		{
			sprintf
			(
				buffer,
				LocalizeString(IDS_MAIN_MISSION),
				(const char *)header.mission
			);
			text->SetText(buffer);
		}
	}
	else
	{
LoadHeaderFailed:
		C3DStatic *text = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MAIN_RANK));
		if (text) text->SetText("");
		text = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MAIN_ISLAND));
		if (text) text->SetText("");
		text = dynamic_cast<C3DStatic *>(GetCtrl(IDC_MAIN_MISSION));
		if (text) text->SetText("");
	}
}

void SaveHeader()
{
	// TODO: player killed

	char buffer[256];
	sprintf(buffer, "%scontinue.sqh", (const char *)GetSaveDirectory());

	ContinueInfo header;
	Person *veh = GWorld->GetRealPlayer();
	AIUnit *unit = veh ? veh->Brain() : NULL;
	if (unit)
		header.rank = unit->GetPerson()->GetRank();
	else
		header.rank = GStats._campaign._playerInfo.rank;
	header.time = Glob.clock.GetTimeInYear();
	header.island = Glob.header.worldname;
	if (CurrentTemplate.intel.briefingName.GetLength() > 0)
		header.mission = CurrentTemplate.intel.briefingName;
	else
		header.mission = Glob.header.filename;
	
	ParamArchiveSave ar(1);
	header.Serialize(ar);
	ar.Save(buffer);
}
*/

bool ContinueSaved = true;

//! starts random cutscene (behind main menu)
void StartRandomCutscene(RString world)
{
	if (world.GetLength() == 0)
#if _FORCE_DEMO_ISLAND
		world = Pars>>"CfgWorlds">>"demoWorld";
#else
		world = Pars>>"CfgWorlds">>"initWorld";
#endif

	const ParamEntry &cls = Pars >> "CfgWorlds" >> world >> "cutscenes";
	int n = cls.GetSize();
	int i = toIntFloor(n * GRandGen.RandomValue());
	
	RString name = cls[i];

	SetMission(world, name, AnimsDir);
//	SetCampaign("");
	SetBaseDirectory("");

	ParseIntro();

	if (CurrentTemplate.groups.Size() > 0)
	{
		GLOB_WORLD->SwitchLandscape(GetWorldName(world));
		GWorld->ActivateAddons(CurrentTemplate.addOns);
		GLOB_WORLD->InitGeneral(CurrentTemplate.intel);
		GLOB_WORLD->InitVehicles(GModeIntro, CurrentTemplate);
//		GWorld->EnableSimulation(true);
	}
}

//! retrieves version of application
/*!
	\patch_internal 1.01 Date 06/21/2001 by Jirka - encapsulates retrieving of version
	- application version is now used not only for display in main manu, but also for addons checks
	\patch_internal 1.03 Date 07/12/2001 by Ondra
	- version retrieving method changed.
*/

#include "versionNo.h"

RString GetAppVersion()
{
	return APP_VERSION_TEXT;
}

DisplayMain::DisplayMain(ControlsContainer *parent)
	: Display(parent)
{
	_version = GetAppVersion();

#if _VBS1
	Load("RscDisplayMainVBS1");
#else
	Load("RscDisplayMain");
#endif
//	LoadHeader();

	GetCtrl(IDC_MAIN_CONTINUE)->EnableCtrl(false);
#if !_ENABLE_CAMPAIGN
	GetCtrl(IDC_MAIN_GAME)->EnableCtrl(false);
#endif
#if !_ENABLE_MP
	GetCtrl(IDC_MAIN_MULTIPLAYER)->EnableCtrl(false);
#endif
#if !_ENABLE_EDITOR
	GetCtrl(IDC_MAIN_EDITOR)->EnableCtrl(false);
#endif
#if !_ENABLE_ALL_MISSIONS
	GetCtrl(IDC_MAIN_CUSTOM)->ShowCtrl(false);
#endif
#if !_ENABLE_AI || !_ENABLE_SINGLE_MISSION
	GetCtrl(IDC_MAIN_SINGLE)->EnableCtrl(false);
	FocusCtrl(IDC_MAIN_MULTIPLAYER);
#endif
}

Control *DisplayMain::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
		case IDC_MAIN_VERSION:
			{
				CStatic *text = new CStatic(this, idc, cls);
				char buffer[256];
				sprintf(buffer, LocalizeString(IDS_VERSION_INFO), (const char *)_version);
				text->SetText(buffer);
				return text;
			}
		case IDC_MAIN_DATE:
			{
				CStatic *text = new CStatic(this, idc, cls);
				#if defined _WIN32 && !defined _XBOX
				HANDLE hf = ::CreateFile
				(
					"bin\\generic.bin", GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL
				);
				FILETIME info, infoLocal;
				::GetFileTime(hf, NULL, NULL, &info);
				::CloseHandle(hf);
				::FileTimeToLocalFileTime(&info, &infoLocal);
				WORD date, time;
				::FileTimeToDosDateTime(&infoLocal, &date, &time);
				int day = date & 0x1f;
				int month = (date >> 5) & 0x0f;
				int year = (date >> 9) + 1980;
				
				char buffer[256];
				sprintf(buffer, "%d/%d/%d", month, day, year);
				text->SetText(buffer);
				#endif

				return text;
			}
		default:
			return Display::OnCreateCtrl(type, idc, cls);
	}
}

//! starts campaign from the beginning
void StartCampaign(RString campaign, Display *disp)
{
	if (!disp)
		disp = dynamic_cast<Display *>(GWorld->Options());
	if (!disp) return;
	SetCampaign(campaign);
	CurrentBattle = ExtParsCampaign>>"Campaign">>"firstBattle";
	if (CurrentBattle.GetLength() == 0) return;
	CurrentMission = ExtParsCampaign>>"Campaign">>CurrentBattle>>"firstMission";
	if (CurrentMission.GetLength() == 0) return;

	Glob.header.playerSide = TWest;

	GStats.ClearAll();
	GCampaignHistory.Clear(CurrentCampaign);
	GCampaignHistory.campaignName = CurrentCampaign;

	// GCampaignHistory.AddMission(CurrentCampaign, CurrentBattle, CurrentMission);
	RString dir = GetTmpSaveDirectory();
	char buffer[256];
	sprintf(buffer, "%s%s.sqc", (const char *)dir, (const char *)CurrentCampaign);
	SaveMission(buffer);

	StartMission(disp, true);
}

//! reads user profile when current player changed
static void UpdateUserProfile()
{
	int GetLangID();

	bool NoBlood();
	if (NoBlood())
	{
		Glob.config.blood = false;
	}
	else
	{
		Glob.config.blood = GetLangID() != Korean;

		RString filename = GetUserParams();
		ParamFile cfg;
		cfg.Parse(filename);
		const ParamEntry *entry = cfg.FindEntry("blood");
		if (entry) Glob.config.blood = *entry;
	}

	GInput.LoadKeys();
	Glob.config.LoadDifficulties();

	GEngine->LoadConfig();
	GScene->LoadConfig();
	if ( GSoundsys ) GSoundsys->LoadConfig();
}

#if _ENABLE_EDITOR
void __cdecl CDPCreateEditor(ControlsContainer *parent, bool multiplayer = false);
#endif

// Macrovision CD Protection
void __cdecl CDPCreateOutro(Display *disp, EndMode end);
CDAPFN_DECLARE_GLOBAL(CDPCreateOutro, CDAPFN_OVERHEAD_L4, CDAPFN_CONSTRAINT_NONE);

void __cdecl CDPCreateOutro(Display *disp, EndMode end)
{
	disp->CreateChild(new DisplayOutro(disp, end));

	CDAPFN_ENDMARK(CDPCreateOutro);

/*
#if _CZECH
	PerformRandomRoxxeTest_003(CDDrive);
#endif
*/
}

/*!
\patch 1.20 Date 08/15/2001 by Jirka
- Fixed: Bad state of main menu (no background cutscene).
- Happens after disconnect from multiplayer game started with command "-connect" or -host.
\patch 1.82 Date 8/23/2002 by Ondra
- Fixed: Weapon pool lost after using Revert or Continue
to start missions First Strike or Fireworks.
*/

void DisplayMain::OnChildDestroyed(int idd, int exit)
{
#ifdef _WIN32
	switch (idd)
	{
		case IDD_CAMPAIGN_LOAD:	// Campaign
			if (exit == IDC_OK)
			{
				DisplayCampaignLoad *disp = dynamic_cast<DisplayCampaignLoad *>((ControlsContainer *)_child);
				Assert(dynamic_cast<C3DListBox *>(disp->GetCtrl(IDC_CAMPAIGN_HISTORY)));

				Glob.config.easyMode = disp->_cadetMode;

				C3DListBox *lbox = static_cast<C3DListBox *>(disp->GetCtrl(IDC_CAMPAIGN_HISTORY));
				CampaignHistory &campaign = disp->_campaigns[disp->_currentCampaign];
				int sel = lbox->GetCurSel();
				DoAssert(sel >= 0);
				int value = lbox->GetValue(sel);
				if (value == -2)
				{
					// remove continue.fps
					RString dir = GetCampaignSaveDirectory(campaign.campaignName);
					RString save = dir + RString("continue.fps");
					::DeleteFile(save);
					save = dir + RString("autosave.fps");
					::DeleteFile(save);
					save = dir + RString("save.fps");
					::DeleteFile(save);

					RString name = campaign.campaignName;
					Display::OnChildDestroyed(idd, exit);
					StartCampaign(name, this);
				}
				else if (value == -1)
				{
					GCampaignHistory = campaign;
					RString save = GetCampaignSaveDirectory(campaign.campaignName) + RString("continue.fps");
					GWorld->LoadBin(save, IDS_LOAD_GAME);
					// remove continue.fps
					::DeleteFile(save);
					Display::OnChildDestroyed(idd, exit);
					CreateChild(new DisplayMission(this, false, false, true));
				}
				else
				{
					// remove continue.fps
					RString dir = GetCampaignSaveDirectory(campaign.campaignName);
					RString save = dir + RString("continue.fps");
					::DeleteFile(save);
					save = dir + RString("autosave.fps");
					::DeleteFile(save);
					save = dir + RString("save.fps");
					::DeleteFile(save);

					int iBattle = value >> 16;
					int iMission = value & 0xffff;
					BattleHistory &battle = campaign.battles[iBattle];
					MissionHistory &mission = battle.missions[iMission];

					SetCampaign(campaign.campaignName);
					CurrentBattle = battle.battleName;
					CurrentMission = mission.missionName;
					GStats._campaign = mission.stats;
					
					if (iMission==0)
					{
						campaign.battles.Resize(iBattle);
						DoAssert(iBattle<=0 || campaign.battles[iBattle-1].missions.Size()>0);
					}
					else
					{
						campaign.battles.Resize(iBattle + 1);
						battle.missions.Resize(iMission);
					}
					GCampaignHistory = campaign;

					Display::OnChildDestroyed(idd, exit);
					
					{
						RString dir = GetTmpSaveDirectory();
						char buffer[256];
						sprintf(buffer, "%s%s.sqc", (const char *)dir, (const char *)CurrentCampaign);
						GCampaignHistory.campaignName = CurrentCampaign;
						SaveMission(buffer);
						StartMission(this, iMission == 0);
					}
				}
			}
			else if (exit == IDC_CAMPAIGN_REPLAY)
			{
				DisplayCampaignLoad *disp = dynamic_cast<DisplayCampaignLoad *>((ControlsContainer *)_child);
				Assert(dynamic_cast<C3DListBox *>(disp->GetCtrl(IDC_CAMPAIGN_HISTORY)));
				C3DListBox *lbox = static_cast<C3DListBox *>(disp->GetCtrl(IDC_CAMPAIGN_HISTORY));
				CampaignHistory &campaign = disp->_campaigns[disp->_currentCampaign];
				int sel = lbox->GetCurSel();
				Assert(sel >= 0);
				int value = lbox->GetValue(sel);
				Assert(value != -1);
				int iBattle = value >> 16;
				int iMission = value & 0xffff;
				BattleHistory &battle = campaign.battles[iBattle];
				MissionHistory &mission = battle.missions[iMission];

				Glob.config.easyMode = disp->_cadetMode;

				CurrentCampaign = "";
				CurrentBattle = "";
				CurrentMission = "";
				SetBaseDirectory(GetCampaignDirectory(campaign.campaignName));

				RString src = GetCampaignSaveDirectory(campaign.campaignName) + RString("objects.sav");
				RString dst = GetCampaignSaveDirectory("") + RString("objects.sav");
				::CopyFile(src, dst, FALSE);

				RString epizode = ExtParsCampaign >> "Campaign" >> battle.battleName >> mission.missionName >> "template";

				GStats.ClearAll();
				GStats._campaign = mission.stats;

				//campaign.battles.Resize(iBattle + 1);
				//battle.missions.Resize(iMission);

				if (iMission==0)
				{
					campaign.battles.Resize(iBattle);
					DoAssert(iBattle<=0 || campaign.battles[iBattle-1].missions.Size()>0);
				}
				else
				{
					campaign.battles.Resize(iBattle + 1);
					battle.missions.Resize(iMission);
				}

				GCampaignHistory = campaign;

				Display::OnChildDestroyed(idd, exit);
				CreateChild(new DisplayIntro(this, epizode));
			}
			else
			{
				Display::OnChildDestroyed(idd, exit);
			}
			break;
		case IDD_CAMPAIGN:
			{
				Display::OnChildDestroyed(idd, exit);
				CurrentMission = ExtParsCampaign>>"Campaign">>CurrentBattle>>"firstMission";
				SetMission(Glob.header.worldname, CurrentMission);
				StartMission(this, false);
			}
			break;
		case IDD_GAME:	// Load
			if (exit == IDC_OK)
			{
				CListBox *selector = dynamic_cast<CListBox *>(_child->GetCtrl(IDC_GAME_SELECT));
				int selGame = selector->GetCurSel();
				// Load game
				{
					// Return to previous game is not availiable now
					GLOB_WORLD->Clear();

					// load
					RString dir = GetTmpSaveDirectory();
					char buffer[256];
					sprintf
					(
						buffer, "%s%s.fps",
						(const char *)dir,
						(const char *)selector->GetText(selGame)
					);
					GLOB_WORLD->LoadBin(buffer, IDS_LOAD_GAME);
				}
				Display::OnChildDestroyed(idd, exit);
				Exit(IDC_MAIN_GAME);
			}
			else
				Display::OnChildDestroyed(idd, exit);
			break;
#if _ENABLE_EDITOR
		case IDD_CUSTOM_ARCADE:	// Custom game
			if (exit == IDC_CUST_PLAY)
			{
				// Return to previous game is not availiable now
				CurrentTemplate.Clear();
				if (GWorld->UI()) GWorld->UI()->Init();

				CTree *tree = dynamic_cast<CTree *>(_child->GetCtrl(IDC_CUST_GAME));
				CTreeItem *item = tree->GetSelected();
				Assert(item);
				Assert(item->level == 3);

				RString mission = item->data;
				item = item->parent;
				RString world = item->data;
				item = item->parent;

				CurrentCampaign = "";
				CurrentBattle = "";
				CurrentMission = "";
				Glob.config.easyMode = false;
				if (item->data.GetLength() == 0)
					SetBaseDirectory("");
				else
				{
					SetBaseDirectory(GetCampaignDirectory(item->data));

					RString src = GetCampaignSaveDirectory(item->data) + RString("objects.sav");
					RString dst = GetCampaignSaveDirectory("") + RString("objects.sav");
					::CopyFile(src, dst, FALSE);
				}
				SetMission(world, mission);

				GStats.ClearAll();
				Display::OnChildDestroyed(idd, exit);
				CreateChild(new DisplayIntro(this));
			}
			else if (exit == IDC_CUST_EDIT)
			{
				// Return to previous game is not availiable now
				CurrentTemplate.Clear();
				if (GWorld->UI()) GWorld->UI()->Init();

				CTree *tree = dynamic_cast<CTree *>(_child->GetCtrl(IDC_CUST_GAME));
				CTreeItem *item = tree->GetSelected();
				Assert(item);
				Assert(item->level >= 2);

				RString mission;
				if (item->level == 3)
				{
					mission = item->data;
					item = item->parent;
				}
				else
				{
					mission = "";
				}
				RString world = item->data;
				item = item->parent;

				CurrentCampaign = "";
				CurrentBattle = "";
				CurrentMission = "";
				Glob.config.easyMode = false;
				if (item->data.GetLength() == 0)
					SetBaseDirectory("");
				else
				{
					SetBaseDirectory(GetCampaignDirectory(item->data));

					RString src = GetCampaignSaveDirectory(item->data) + RString("objects.sav");
					RString dst = GetCampaignSaveDirectory("") + RString("objects.sav");
					::CopyFile(src, dst, FALSE);
				}
				SetMission(world, mission);

				Display::OnChildDestroyed(idd, exit);
				GLOB_WORLD->SwitchLandscape(GetWorldName(Glob.header.worldname));
				// Macrovision CD Protection
				CDPCreateEditor(this);
				// CreateChild(new DisplayArcadeMap(this));
			}
			else
				Display::OnChildDestroyed(idd, exit);
			break;
#endif // #if _ENABLE_EDITOR
		case IDD_SINGLE_MISSION:	// Single mission
			if (exit == IDC_OK)
			{
				// Return to previous game is not availiable now
				CurrentTemplate.Clear();
				if (GWorld->UI()) GWorld->UI()->Init();

				C3DListBox *lbox = dynamic_cast<C3DListBox *>(_child->GetCtrl(IDC_SINGLE_MISSION));
				int sel = lbox->GetCurSel();
				if (sel < 0)
				{
					Display::OnChildDestroyed(idd, exit);
					break;
				}
			
				DisplaySingleMission *disp = dynamic_cast<DisplaySingleMission *>((ControlsContainer *)_child);
				Glob.config.easyMode = disp->_cadetMode;

				CurrentCampaign = "";
				CurrentBattle = "";
				CurrentMission = "";
				SetCampaign("");

				RString dir = RString("missions\\") + disp->GetDirectory();
				if (lbox->GetValue(sel) == 1)
				{
					RString filename = lbox->GetData(sel);
					RString directory = dir + filename;
					char buffer[1024];
					strcpy(buffer, CreateSingleMissionBank(directory));
					if (*buffer == 0)
					{
						Display::OnChildDestroyed(idd, exit);
						break;
					}
					char *str = strrchr(buffer, '\\');
					Assert(str);
					*str = 0;
					char *world = strrchr(buffer, '.');
					Assert(world);
					*world = 0;
					world++;
					const char *mission = strrchr(buffer, '\\');
					Assert(mission);
					mission++;
					SetMission(world, mission);
					strcpy(buffer, filename);
					world = strrchr(buffer, '.');
					Assert(world);
					*world = 0;
					Glob.header.filenameReal = disp->GetDirectory() + RString(buffer);
				}
				else
				{
					RString mission = lbox->GetData(sel);
					if (!ProcessTemplateName(mission, dir))
					{
						Display::OnChildDestroyed(idd, exit);
						break;
					}
				}

				GStats.ClearAll();
				Display::OnChildDestroyed(idd, exit);
				CreateChild(new DisplayIntro(this));
			}
			else if (exit == IDC_SINGLE_LOAD)
			{
				C3DListBox *lbox = dynamic_cast<C3DListBox *>(_child->GetCtrl(IDC_SINGLE_MISSION));
				int sel = lbox->GetCurSel();
				Assert(sel >= 0);

				DisplaySingleMission *disp = dynamic_cast<DisplaySingleMission *>((ControlsContainer *)_child);
				
				RString mission = lbox->GetData(sel);
				RString filename = GetMissionSaveDirectory(disp->GetDirectory() + mission) + RString("continue.fps");
				GWorld->LoadBin(filename, IDS_LOAD_GAME);

				Display::OnChildDestroyed(idd, exit);
				CreateChild(new DisplayMission(this, false, false));
			}
			else
				Display::OnChildDestroyed(idd, exit);
			break;
#if _ENABLE_EDITOR
		case IDD_SELECT_ISLAND:
			if (exit == IDC_OK)
			{
				// Return to previous game is not availiable now
//				GNetworkManager->DoneNetworkManager();
				CurrentTemplate.Clear();
				if (GWorld->UI()) GWorld->UI()->Init();

//				CListBox *lbox = dynamic_cast<CListBox *>(_child->GetCtrl(IDC_SELECT_ISLAND));
				C3DListBox *lbox = dynamic_cast<C3DListBox *>(_child->GetCtrl(IDC_SELECT_ISLAND));
				RString world = lbox->GetData(lbox->GetCurSel());

				RString userDir = GetUserDirectory(); 
				CurrentCampaign = "";
				CurrentBattle = "";
				CurrentMission = "";
				SetBaseDirectory(userDir);
				::CreateDirectory(BaseDirectory + RString("missions"),NULL);
				SetMission(world, "");

				Display::OnChildDestroyed(idd, exit);
				GLOB_WORLD->SwitchLandscape(GetWorldName(Glob.header.worldname));
				// Macrovision CD Protection
				CDPCreateEditor(this);
				// CreateChild(new DisplayArcadeMap(this));
			}
			else if (exit == IDC_CUST_PLAY)
			{
				CurrentTemplate.Clear();
				if (GWorld->UI()) GWorld->UI()->Init();

				CurrentCampaign = "";
				CurrentBattle = "";
				CurrentMission = "";
				Glob.config.easyMode = false;

				GStats.ClearAll();
				Display::OnChildDestroyed(idd, exit);
				CreateChild(new DisplayIntro(this));
			}
			else
				Display::OnChildDestroyed(idd, exit);
			break;
#endif // #if _ENABLE_EDITOR
		case IDD_INTRO:
			{
				Display::OnChildDestroyed(idd, exit);
				GWorld->DestroyMap(IDC_OK);

				CurrentTemplate.Clear();
				if (GWorld->UI()) GWorld->UI()->Init();
				if
				(
					!ParseMission(false) ||
					CurrentTemplate.groups.Size() == 0
				)
				{
					// add intro into campaign tree
					if (IsCampaign())
					{
						RString displayName = Localize(CurrentTemplate.intel.briefingName);
						if (displayName.GetLength() == 0)
							displayName = RString(Glob.header.filename) + RString(".") + RString(Glob.header.worldname);
						AddMission(CurrentCampaign, CurrentBattle, CurrentMission, displayName);
					}
					
					_end = EMLoser;
					goto StartOutro;
				}

				GLOB_WORLD->SwitchLandscape(GetWorldName(Glob.header.worldname));
				GStats.ClearMission();
				if (IsCampaign())
				{
					GStats._mission._lives = ExtParsCampaign>>"Campaign">>CurrentBattle>>CurrentMission>>"lives";
				}
				// if (IsCampaign())
				{
					// load variables
					GameState *gstate = GWorld->GetGameState();
					AutoArray<GameVariable> &variables = GStats._campaign._variables;
					for (int i=0; i<variables.Size(); i++)
					{
						GameVariable &var = variables[i];
						gstate->VarSet(var._name, var._value, var._readOnly);
					}
				}
				GWorld->ActivateAddons(CurrentTemplate.addOns);
				GLOB_WORLD->InitGeneral(CurrentTemplate.intel);
				GLOB_WORLD->InitVehicles(GModeArcade,CurrentTemplate);

				RString weapons = GetSaveDirectory() + RString("weapons.cfg");
				::DeleteFile(weapons);

				// remove continue.fps
				RString dir = GetSaveDirectory();
				RString save = dir + RString("continue.fps");
				::DeleteFile(save);
				save = dir + RString("autosave.fps");
				::DeleteFile(save);
				save = dir + RString("save.fps");
				::DeleteFile(save);

				RString briefing = GetBriefingFile();
				if (briefing.GetLength() > 0)
				{
					CreateChild(new DisplayGetReady(this));
				}
				else
				{
					goto StartMission;
				}
			}
			break;
		case IDD_INTEL_GETREADY:
			{
				Display::OnChildDestroyed(idd, exit);
				if (exit == IDC_CANCEL)
				{
					if (IsCampaign())
					{
						RString displayName = Localize(CurrentTemplate.intel.briefingName);
						if (displayName.GetLength() == 0)
							displayName = RString(Glob.header.filename) + RString(".") + RString(Glob.header.worldname);
						// void AddMission(RString campaign, RString battle, RString mission, RString displayName);
						AddMission(CurrentCampaign, CurrentBattle, CurrentMission, displayName);
					}

					// exit to main menu
					StartRandomCutscene(Glob.header.worldname);
					break;
				}
StartMission:				
				CreateChild(new DisplayMission(this));
			}
			break;
		case IDD_MISSION:
		{
			Display::OnChildDestroyed(idd, exit);
			if (exit == IDC_MAIN_QUIT)
			{
/*
				// save state for continue
				if (!ContinueSaved && GWorld->GetRealPlayer() && !GWorld->GetRealPlayer()->IsDammageDestroyed())
				{
					char buffer[256];
					sprintf(buffer, "%scontinue.fps", (const char *)GetSaveDirectory());
					GWorld->SaveBin(buffer);
				}
				ContinueSaved = true;
*/
				// escape directly into menu
				if (AutoTest) Exit(IDC_MAIN_QUIT);
				else StartRandomCutscene(Glob.header.worldname);
			}
			else
			{
				_end = GWorld->GetEndMode();
				ParamEntry *entry = ExtParsMission.FindEntry("debriefing");
				if (entry && !(bool)(*entry))
				{
					GStats.Update();
					goto StartOutro;
				}
				else
					CreateChild(new DisplayDebriefing(this, true));
			}
		}
		break;
		case IDD_DEBRIEFING:
			{
				Display::OnChildDestroyed(idd, exit);
				if (exit == IDC_OK)
				{
					// restart mission
					RString weapons = GetSaveDirectory() + RString("weapons.cfg");
					::DeleteFile(weapons);

					// remove continue.fps
					RString dir = GetSaveDirectory();
					RString save = dir + RString("continue.fps");
					::DeleteFile(save);
					save = dir + RString("autosave.fps");
					::DeleteFile(save);
					save = dir + RString("save.fps");
					::DeleteFile(save);


					RString briefing = GetBriefingFile();
					if (briefing.GetLength() > 0)
					{
						CreateChild(new DisplayGetReady(this));
						break;
					}
					else
					{
						goto StartMission;
					}
				}
StartOutro:
				CDPCreateOutro(this, _end);
			}
			break;
		case IDD_OUTRO:
			{
				Display::OnChildDestroyed(idd, exit);
				if (IsCampaign())
				{
					if (!CheckAward(this)) goto StartNextMission;
				}
				else
				{
					StartRandomCutscene(Glob.header.worldname);
				}
			}
			break;
		case IDD_AWARD:
			{
				Display::OnChildDestroyed(idd, exit);
				Assert(IsCampaign());
StartNextMission:
				if (NextMission(this, _end))
				{
					// continue with campaign
				}
				else
				{
					// TODO: total end of campaign
					StartRandomCutscene(Glob.header.worldname);
				}
			}
			break;
		case IDD_ARCADE_MAP:
			Display::OnChildDestroyed(idd, exit);
			StartRandomCutscene(Glob.header.worldname);
			break;
		case IDD_OPTIONS:
			Display::OnChildDestroyed(idd, exit);
			break;
		case IDD_SAVE:
			{
				CEdit *edit = dynamic_cast<CEdit *>(_child->GetCtrl(IDC_SIDE_NAME));
				const char *name = edit->GetText();
				Assert(name && strlen(name) > 0);
				RString dir = GetTmpSaveDirectory();
				char buffer[256];
				sprintf(buffer, "%s%s.fps", (const char *)dir, name);
				GLOB_WORLD->SaveBin(buffer, IDS_SAVE_GAME);
				Display::OnChildDestroyed(idd, exit);
			}
			break;
		case IDD_MULTIPLAYER:
			Assert(exit == IDC_CANCEL);
			GetNetworkManager().Done();
			Display::OnChildDestroyed(idd, exit);
			StartRandomCutscene(Glob.header.worldname);
			break;
		case IDD_CLIENT:
		case IDD_SERVER:
			// FIX
			GetNetworkManager().Done();
			Display::OnChildDestroyed(idd, exit);
			StartRandomCutscene(Glob.header.worldname);
			break;
		case IDD_LOGIN:
			if (exit == IDC_OK)
			{
				C3DListBox *ctrl = dynamic_cast<C3DListBox *>(_child->GetCtrl(IDC_LOGIN_USER));
				int index = ctrl->GetCurSel();
				Assert(index >= 0);
				Glob.header.playerName = ctrl->GetText(index);
//				LoadHeader();
				#ifndef _XBOX
				const char *name = Glob.header.playerName;
				HKEY key;
				if
				(
					::RegCreateKey(HKEY_CURRENT_USER, ConfigApp, &key) == ERROR_SUCCESS
				)
				{
					::RegSetValueEx(key, "Player Name", NULL, REG_SZ, (BYTE *)name, strlen(name) + 1);
					::RegCloseKey(key);
				}
				#endif
				Display::OnChildDestroyed(idd, exit);

				// load info from identity
				RString filename = GetUserParams();
				if (QIFStream::FileExists(filename))
				{
					ParamFile cfg;
					cfg.Parse(filename);
					const ParamEntry *identity = cfg.FindEntry("Identity");
					if (identity)
					{
						Glob.header.playerFace = (*identity) >> "face";
						if (identity->FindEntry("glasses"))
							Glob.header.playerGlasses = (*identity) >> "glasses";
						Glob.header.playerSpeaker = (*identity) >> "speaker";
						Glob.header.playerPitch = (*identity) >> "pitch";
					}
					UpdateUserProfile();
				}
			}
			else if (exit == IDC_LOGIN_NEW || exit == IDC_LOGIN_EDIT)
			{
				C3DListBox *ctrl = dynamic_cast<C3DListBox *>(_child->GetCtrl(IDC_LOGIN_USER));
				int index = ctrl->GetCurSel();
				if (index >= 0) Glob.header.playerName = ctrl->GetText(index);
				RString player = Glob.header.playerName;
				Display::OnChildDestroyed(idd, exit);
				CreateChild(new DisplayNewUser(this, player, exit == IDC_LOGIN_EDIT));
			}
			else
			{
				Display::OnChildDestroyed(idd, exit);
			}
			break;
		case IDD_NEW_USER:
			if (exit == IDC_OK)
			{
				C3DEdit *edit = dynamic_cast<C3DEdit *>(_child->GetCtrl(IDC_NEW_USER_NAME));
				Glob.header.playerName = edit->GetText();
				const char *name = Glob.header.playerName;

				DisplayNewUser *disp = static_cast<DisplayNewUser *>((ControlsContainer *)_child);
				if (disp->_edit && stricmp(name, disp->_name) != 0)
				{
					// rename directory
					::MoveFile
					(
						RString("Users\\") + disp->_name,
						RString("Users\\") + Glob.header.playerName
					);
				}
				
//				LoadHeader();
				#ifndef _XBOX
				HKEY key;
				if
				(
					::RegCreateKey(HKEY_CURRENT_USER, ConfigApp, &key) == ERROR_SUCCESS
				)
				{
					::RegSetValueEx(key, "Player Name", NULL, REG_SZ, (BYTE *)name, strlen(name) + 1);
					::RegCloseKey(key);
				}
				#endif
				GetTmpSaveDirectory();	// only create directory

				C3DListBox *ctrl = dynamic_cast<C3DListBox *>(_child->GetCtrl(IDC_NEW_USER_FACE));
				if (ctrl)
				{
					int sel = ctrl->GetCurSel();
					if (sel >= 0) Glob.header.playerFace = ctrl->GetData(sel);
				}
				ctrl = dynamic_cast<C3DListBox *>(_child->GetCtrl(IDC_NEW_USER_GLASSES));
				if (ctrl)
				{
					int sel = ctrl->GetCurSel();
					if (sel >= 0) Glob.header.playerGlasses = ctrl->GetData(sel);
				}
				ctrl = dynamic_cast<C3DListBox *>(_child->GetCtrl(IDC_NEW_USER_SPEAKER));
				if (ctrl)
				{
					int sel = ctrl->GetCurSel();
					if (sel >= 0) Glob.header.playerSpeaker = ctrl->GetData(sel);
				}
				C3DSlider *slider = dynamic_cast<C3DSlider *>(_child->GetCtrl(IDC_NEW_USER_PITCH));
				if (slider) Glob.header.playerPitch = slider->GetThumbPos();

				RString squad;
				edit = dynamic_cast<C3DEdit *>(_child->GetCtrl(IDC_NEW_USER_SQUAD));
				if (edit) squad = edit->GetText();

				// save info to identity
				RString filename = GetUserParams();
				ParamFile cfg;
				cfg.Parse(filename);
				ParamEntry *identity = cfg.AddClass("Identity");
				identity->Add("face", Glob.header.playerFace);
				identity->Add("glasses", Glob.header.playerGlasses);
				identity->Add("speaker", Glob.header.playerSpeaker);
				identity->Add("pitch", Glob.header.playerPitch);
				identity->Add("squad", squad);
				cfg.Save(filename);
				UpdateUserProfile();
			}
			Display::OnChildDestroyed(idd, exit);
			break;
		default:
			Display::OnChildDestroyed(idd, exit);
			break;
	}
#endif            // _WIN32
}

#include "cdapfncond.h"

// Macrovision CD Protection - disabled
void __cdecl CDPCreateDisplaySingleMission(ControlsContainer *parent);
// CDAPFN_DECLARE_GLOBAL(CDPCreateDisplaySingleMission, CDAPFN_OVERHEAD_L2, CDAPFN_CONSTRAINT_NONE);

void __cdecl CDPCreateDisplaySingleMission(ControlsContainer *parent)
{
	parent->CreateChild(new DisplaySingleMission(parent));

//	CDAPFN_ENDMARK(CDPCreateDisplaySingleMission);
}

// Macrovision CD Protection - disabled
void __cdecl CDPCreateDisplayCampaign(ControlsContainer *parent);
// CDAPFN_DECLARE_GLOBAL(CDPCreateDisplayCampaign, CDAPFN_OVERHEAD_L2, CDAPFN_CONSTRAINT_NONE);

void __cdecl CDPCreateDisplayCampaign(ControlsContainer *parent)
{
	parent->CreateChild(new DisplayCampaignLoad(parent));

//	CDAPFN_ENDMARK(CDPCreateDisplayCampaign);
}

void DisplayMain::OnButtonClicked(int idc)
{
	switch (idc)
	{
		case IDC_MAIN_SINGLE:
			// Macrovision CD Protection
			CDPCreateDisplaySingleMission(this);
			//CreateChild(new DisplaySingleMission(this));
			break;
		case IDC_MAIN_GAME:
			// Macrovision CD Protection
			CDPCreateDisplayCampaign(this);
			//CreateChild(new DisplayCampaignLoad(this));
			break;
		case IDC_MAIN_OPTIONS:
			CreateChild(new DisplayOptions(this, true, true));
			break;
		case IDC_MAIN_CUSTOM:
			CreateChild(new DisplayCustomArcade(this));
			break;
		case IDC_MAIN_EDITOR:
			CreateChild(new DisplaySelectIsland(this));
			break;
		case IDC_MAIN_MULTIPLAYER:
			CreateChild(new DisplayMultiplayer(this));
			break;
		case IDC_MAIN_QUIT:
			Exit(IDC_MAIN_QUIT);
			break;
		case IDC_MAIN_PLAYER:
			CreateChild(new DisplayLogin(this));
			break;
		case IDC_MAIN_CREDITS:
			CreateChild(new DisplayCredits(this));
			break;
		case IDC_MAIN_LOAD:
			CreateChild(new DisplayGame(this));
			break;
		case IDC_MAIN_SAVE:
			CreateChild(new DisplaySave(this));
			break;
		default:
			// no escape - only QUIT
			break;
	}
}

void DisplayMain::OnDraw(EntityAI *vehicle, float alpha)
{
	char buffer[256];
	sprintf(buffer, LocalizeString(IDS_MAIN_PLAYER), (const char *)Glob.header.playerName);
	CActiveText *text = dynamic_cast<CActiveText *>(GetCtrl(IDC_MAIN_PLAYER));
	Assert(text);
	text->SetText(buffer);
	Display::OnDraw(vehicle,alpha);
}

// Single mission display
DisplaySingleMission::DisplaySingleMission(ControlsContainer *parent)
	: Display(parent)
{
	Load("RscDisplaySingleMission");
	LoadDirectory();
	OnChangeMission();
	LoadParams();
	OnChangeDifficulty();
	_exitWhenClose = -1;
}

void DisplaySingleMission::LoadParams()
{
	ParamArchiveLoad ar(GetUserParams());
	if (ar.Serialize("cadetMode", _cadetMode, 1, true) != LSOK)
	{
		WarningMessage("Cannot load user paremeters.");
	}
}

void DisplaySingleMission::SaveParams()
{
	ParamArchiveSave ar(UserInfoVersion);
	ar.Parse(GetUserParams());
	if (ar.Serialize("cadetMode", _cadetMode, 1) != LSOK)
	{
		// TODO: save failed
	}
	if (ar.Save(GetUserParams()) != LSOK)
	{
		// TODO: save failed
	}
}

#ifdef _XBOX
	const char *FullXBoxName(const char *name, char *temp);

	long X_findfirst( const char *name, _finddata_t *info)
	{
		char temp[1024];
		name = FullXBoxName(name,temp);
		return _findfirst(name, info);
	}
#else
	#define X_findfirst _findfirst
#endif

Control *DisplaySingleMission::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
/*
	switch (idc)
	{
	case IDC_SINGLE_MISSION:
		{
			const char *searchStr = "briefingName";
			int searchLen = strlen(searchStr);

			C3DListBox *lbox = new C3DListBox(this, idc, cls);
			_finddata_t info;

			#ifdef _XBOX
				//long h = X_findfirst("Missions\\*.pbo", &info);
				long h = X_findfirst("Missions\\*", &info);
			#else
				long h = _findfirst("Missions\\*.pbo", &info);
			#endif
			if (h != -1)
			{
				do
				{
					if 
					(
						(info.attrib & _A_SUBDIR) == 0
					)
					{
						// remove extension (.pbo)
						RString name = info.name;
						int n = name.GetLength() - 4;
						Assert(stricmp(name + n, ".pbo") == 0);
						RString nameNoExt = name.Substring(0, n);

						// create bank (temporary)
						QFBank bank;
						bank.open(RString("Missions\\") + nameNoExt);

						ParamFile f;
						if (f.ParseBin(bank, "mission.sqm"))
						{
							const ParamEntry &entry = f >> "Mission" >> "Intel";
							if (entry.FindEntry("briefingName")) name = entry >> "briefingName";
						}
						else
						{
							// fast search for mission name without parsing
							// suppose "briefingName" is found in first 4KB of mission file
							QIFStreamB file;
							file.open(bank, "mission.sqm");

							const char *searchIn = file.act();
							const char *maxEnd = searchIn+file.rest();
							int searchInLen = file.rest();
							saturateMin(searchInLen,4*1024); // seach max. one page
							searchInLen -= searchLen;

							for (int s=0; s<searchInLen; s++)
							{
								if (!strnicmp(searchIn+s,searchStr,searchLen))
								{
									// candidate for a match
									// scan for '=' sign
									const char *end = searchIn+s+searchLen;
									while (*end==' ' && end<maxEnd) end++;
									if (*end=='=')
									{
										end++;
										while (*end==' ' && end<maxEnd) end++;
										// should be followed with '"'
										if (*end=='"')
										{
											end++;
											// scan name
											char buf[1024];
											int maxC = 0;;
											while (*end!='"' && end<maxEnd && maxC<sizeof(buf)-1)
											{
												buf[maxC++]=*end++;
											}
											buf[maxC]=0;
											if (*buf) name = buf;
										}
									}
									break;
								}
							}
						}
						
						
						int index = lbox->AddString(Localize(name));
						lbox->SetData(index, nameNoExt);
						lbox->SetValue(index, 1); // bank
					}
				}
				while (0==_findnext(h, &info));
				_findclose(h);
			}
			h = X_findfirst("Missions\\*.*", &info);
			if (h != -1)
			{
				const char *searchStr = "briefingName";
				int searchLen = strlen(searchStr);
				do
				{
					if 
					(
						(info.attrib & _A_SUBDIR) != 0 &&
						info.name[0] != '.'
					)
					{
						RString name = info.name;
						RString filename = RString("missions\\") + name + RString("\\mission.sqm");

						ParamFile f;
						if (f.ParseBin(filename))
						{
							const ParamEntry &entry = f >> "Mission" >> "Intel";
							if (entry.FindEntry("briefingName")) name = entry >> "briefingName";
						}
						else
						{
							// fast search for mission name without parsing
							// suppose "briefingName" is found in first 4KB of mission file
							QIFStream file;
							file.open(filename);

							const char *searchIn = file.act();
							const char *maxEnd = searchIn+file.rest();
							int searchInLen = file.rest();
							saturateMin(searchInLen,4*1024); // seach max. one page
							searchInLen -= searchLen;

							for (int s=0; s<searchInLen; s++)
							{
								if (!strnicmp(searchIn+s,searchStr,searchLen))
								{
									// candidate for a match
									// scan for '=' sign
									const char *end = searchIn+s+searchLen;
									while (*end==' ' && end<maxEnd) end++;
									if (*end=='=')
									{
										end++;
										while (*end==' ' && end<maxEnd) end++;
										// should be followed with '"'
										if (*end=='"')
										{
											end++;
											// scan name
											char buf[1024];
											int maxC = 0;;
											while (*end!='"' && end<maxEnd && maxC<sizeof(buf)-1)
											{
												buf[maxC++]=*end++;
											}
											buf[maxC]=0;
											if (*buf) name = buf;
										}
									}
									break;
								}
							}
						}
						
						int index = lbox->AddString(Localize(name));
						lbox->SetData(index, info.name);
						lbox->SetValue(index, 0); // directory
					}
				}
				while (0==_findnext(h, &info));
				_findclose(h);
			}
			lbox->SortItems();
			lbox->SetCurSel(0);
			return lbox;
		}
	default:
		return Display::OnCreateCtrl(type, idc, cls);
	}
*/
	return Display::OnCreateCtrl(type, idc, cls);
}

void DisplaySingleMission::OnLBSelChanged(int idc, int curSel)
{
	if (idc == IDC_SINGLE_MISSION)
		OnChangeMission();

	Display::OnLBSelChanged(idc, curSel);
}

void DisplaySingleMission::OnLBDblClick(int idc, int curSel)
{
	if (idc == IDC_SINGLE_MISSION)
		OnButtonClicked(IDC_OK);
	else	
		Display::OnLBDblClick(idc, curSel);
}

/*!
\patch 1.75 Date 2/6/2002 by Jirka
- Added: directory structure at single missions 
*/

void DisplaySingleMission::LoadDirectory()
{
	ProgressStart( LocalizeString(IDS_LOAD_WORLD) );
#ifdef _WIN32
	C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_SINGLE_MISSION));
	if (!lbox) return;

	lbox->ClearStrings();

#if _ENABLE_DATADISC
	if (_directory.GetLength() > 0)
	{
		// Parent directory
		int index = lbox->AddString("..");
		lbox->SetValue(index, -2); // subdirectory
	}
#endif

	RString dir = RString("Missions\\") + _directory;

	const char *searchStr = "briefingName";
	int searchLen = strlen(searchStr);

	_finddata_t info;

	// first search for .pbo files
	#ifdef _XBOX
		//long h = X_findfirst("Missions\\*.pbo", &info);
		long h = X_findfirst(dir + RString("*"), &info);
	#else
		long h = _findfirst(dir + RString("*.pbo"), &info);
	#endif
	if (h != -1)
	{
		do
		{
			if 
			(
				(info.attrib & _A_SUBDIR) == 0
			)
			{
				// remove extension (.pbo)
				RString name = info.name;
				int n = name.GetLength() - 4;
				Assert(stricmp(name + n, ".pbo") == 0);
				RString nameNoExt = name.Substring(0, n);

				// create bank (temporary)
				QFBank bank;
				bank.open(dir + nameNoExt);

				ProgressRefresh();

				ParamFile f;
				if (f.ParseBin(bank, "mission.sqm"))
				{
					const ParamEntry &entry = f >> "Mission" >> "Intel";
					if (entry.FindEntry("briefingName")) name = entry >> "briefingName";
				}
				else
				{
					// fast search for mission name without parsing
					// suppose "briefingName" is found in first 4KB of mission file
					QIFStreamB file;
					file.open(bank, "mission.sqm");

					const char *searchIn = file.act();
					const char *maxEnd = searchIn+file.rest();
					int searchInLen = file.rest();
					saturateMin(searchInLen,4*1024); // seach max. one page
					searchInLen -= searchLen;

					for (int s=0; s<searchInLen; s++)
					{
						if (!strnicmp(searchIn+s,searchStr,searchLen))
						{
							// candidate for a match
							// scan for '=' sign
							const char *end = searchIn+s+searchLen;
							while (*end==' ' && end<maxEnd) end++;
							if (*end=='=')
							{
								end++;
								while (*end==' ' && end<maxEnd) end++;
								// should be followed with '"'
								if (*end=='"')
								{
									end++;
									// scan name
									char buf[1024];
									int maxC = 0;;
									while (*end!='"' && end<maxEnd && maxC<sizeof(buf)-1)
									{
										buf[maxC++]=*end++;
									}
									buf[maxC]=0;
									if (*buf) name = buf;
								}
							}
							break;
						}
					}
				}
				
				
				int index = lbox->AddString(Localize(name));
				lbox->SetData(index, nameNoExt);
				lbox->SetValue(index, 1); // bank
			}
		}
		while (0==_findnext(h, &info));
		_findclose(h);
	}
	// search for missions is subdirectories and for folders of missions
	h = X_findfirst(dir + RString("*.*"), &info);
	if (h != -1)
	{
		const char *searchStr = "briefingName";
		int searchLen = strlen(searchStr);
		do
		{
			if 
			(
				(info.attrib & _A_SUBDIR) != 0 &&
				info.name[0] != '.'
			)
			{
				RString name = info.name;

				if (strchr(name, '.'))
				{
					// Mission directory
					RString filename = dir + name + RString("\\mission.sqm");

					ProgressRefresh();

					ParamFile f;
					if (f.ParseBin(filename))
					{
						const ParamEntry &entry = f >> "Mission" >> "Intel";
						if (entry.FindEntry("briefingName")) name = entry >> "briefingName";
					}
					else
					{
						// fast search for mission name without parsing
						// suppose "briefingName" is found in first 4KB of mission file
						QIFStream file;
						file.open(filename);

						const char *searchIn = file.act();
						const char *maxEnd = searchIn+file.rest();
						int searchInLen = file.rest();
						saturateMin(searchInLen,4*1024); // seach max. one page
						searchInLen -= searchLen;

						for (int s=0; s<searchInLen; s++)
						{
							if (!strnicmp(searchIn+s,searchStr,searchLen))
							{
								// candidate for a match
								// scan for '=' sign
								const char *end = searchIn+s+searchLen;
								while (*end==' ' && end<maxEnd) end++;
								if (*end=='=')
								{
									end++;
									while (*end==' ' && end<maxEnd) end++;
									// should be followed with '"'
									if (*end=='"')
									{
										end++;
										// scan name
										char buf[1024];
										int maxC = 0;;
										while (*end!='"' && end<maxEnd && maxC<sizeof(buf)-1)
										{
											buf[maxC++]=*end++;
										}
										buf[maxC]=0;
										if (*buf) name = buf;
									}
								}
								break;
							}
						}
					}
					
					int index = lbox->AddString(Localize(name));
					lbox->SetData(index, info.name);
					lbox->SetValue(index, 0); // directory
				}
#if _ENABLE_DATADISC
				else
				{
					// Subdirectory
					int index = lbox->AddString(name + RString("..."));
					lbox->SetData(index, name);
					lbox->SetValue(index, -1); // subdirectory
				}
#endif
			}
		}
		while (0==_findnext(h, &info));
		_findclose(h);
	}
	lbox->SortItemsByValue();
	lbox->SetCurSel(0);
#endif                // _WIN32
	ProgressFinish();
}

//! find overview file for current language
RString GetOverviewFile(RString dir)
{
	RString prefix = dir + RString("overview.");
	RString name = prefix + GLanguage + RString(".html");
	if (QIFStreamB::FileExist(name)) return name;
	name = prefix + RString("html");
	if (QIFStreamB::FileExist(name)) return name;
	return RString();
}

//! recovery when continue.fps file not found
static void CheckContinueSave(RString dir)
{
#ifdef _WIN32
	RString resume = dir + RString("continue.fps");
	if (QIFStream::FileExists(resume)) return;

	RString save = dir + RString("save.fps");
	RString autosave = dir + RString("autosave.fps");
	if (QIFStream::FileExists(save))
	{
		if (QIFStream::FileExists(autosave))
		{
			// select later
			FILETIME infoSave, infoAutosave;
			HANDLE handle = ::CreateFile
			(
				save, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL
			);
			::GetFileTime(handle, NULL, NULL, &infoSave);
			::CloseHandle(handle);
			handle = ::CreateFile
			(
				autosave, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL
			);
			::GetFileTime(handle, NULL, NULL, &infoAutosave);
			::CloseHandle(handle);
			if
			(
				infoSave.dwHighDateTime > infoAutosave.dwHighDateTime ||
				infoSave.dwHighDateTime == infoAutosave.dwHighDateTime &&
				infoSave.dwLowDateTime >= infoAutosave.dwLowDateTime
			)
			{
				::CopyFile(save, resume, FALSE);
			}
			else
			{
				::CopyFile(autosave, resume, FALSE);
			}
		}
		else
		{
			::CopyFile(save, resume, FALSE);
		}
	}
	else
	{
		if (QIFStream::FileExists(autosave))
		{
			::CopyFile(autosave, resume, FALSE);
		}
	}
#endif
}

void DisplaySingleMission::OnChangeMission()
{
#ifdef _WIN32
	C3DHTML *html = dynamic_cast<C3DHTML *>(GetCtrl(IDC_SINGLE_OVERVIEW));
	if (!html) return;
	html->Init();
	C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_SINGLE_MISSION));
	if (!lbox) return;
	int sel = lbox->GetCurSel();
	if (sel < 0) return;

	RString mission = lbox->GetData(sel);
	RString directory = RString("missions\\") + _directory + mission;

	if (lbox->GetValue(sel) < 0)
	{
		if (lbox->GetValue(sel) == -1)
		{
			RString filename = GetOverviewFile(directory + RString("\\"));
			if (filename.GetLength() > 0)
				html->Load(filename, false);
		}

		CActiveText *ctrl = static_cast<CActiveText *>(GetCtrl(IDC_OK));
		if (ctrl) ctrl->SetText(LocalizeString(IDS_SINGLE_OPEN));
		ctrl = static_cast<CActiveText *>(GetCtrl(IDC_SINGLE_LOAD));
		if (ctrl) ctrl->ShowCtrl(false);
		return;
	}
	
	RString filename;
	if (lbox->GetValue(sel) == 1)
	{
		RString bank = CreateSingleMissionBank(directory);
		if (bank.GetLength() == 0) return;
		filename = GetOverviewFile(bank);
	}
	else
		filename = GetOverviewFile(directory + RString("\\"));
	if (filename.GetLength() > 0)
		html->Load(filename, false);

	// update buttons
	RString dir = GetMissionSaveDirectory(_directory + mission);
	CheckContinueSave(dir);
	RString save = dir + RString("continue.fps");
	if (QIFStream::FileExists(save))
	{
		CActiveText *ctrl = static_cast<CActiveText *>(GetCtrl(IDC_OK));
		if (ctrl) ctrl->SetText(LocalizeString(IDS_SINGLE_RESTART));
		ctrl = static_cast<CActiveText *>(GetCtrl(IDC_SINGLE_LOAD));
		if (ctrl)
		{
			HANDLE handle = ::CreateFile
			(
				save, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL
			);
			FILETIME info, infoLocal;
			::GetFileTime(handle, NULL, NULL, &info);
			::CloseHandle(handle);
			::FileTimeToLocalFileTime(&info, &infoLocal);
			SYSTEMTIME stime;
			::FileTimeToSystemTime(&infoLocal, &stime);
			int day = stime.wDay;
			int month = stime.wMonth;
			int year = stime.wYear;
			int minute = stime.wMinute;
			int hour = stime.wHour;
			char buffer[256];
			sprintf(buffer, LocalizeString(IDS_SINGLE_RESUME), month, day, year, hour, minute);
			ctrl->SetText(buffer);
			ctrl->ShowCtrl(true);
		}
	}
	else
	{
		CActiveText *ctrl = static_cast<CActiveText *>(GetCtrl(IDC_OK));
		if (ctrl) ctrl->SetText(LocalizeString(IDS_SINGLE_PLAY));
		ctrl = static_cast<CActiveText *>(GetCtrl(IDC_SINGLE_LOAD));
		if (ctrl) ctrl->ShowCtrl(false);
	}
#endif
}

void DisplaySingleMission::OnChangeDifficulty()
{
#ifdef _WIN32
	CActiveText *ctrl = dynamic_cast<CActiveText *>(GetCtrl(IDC_SINGLE_DIFF));
	if (!ctrl) return;
	RString text; 
	if (_cadetMode) text = LocalizeString(IDS_DIFF_CADET);
	else text = LocalizeString(IDS_DIFF_VETERAN);
	ctrl->SetText(text);
#endif
}

void DisplaySingleMission::OnButtonClicked(int idc)
{
#ifdef _WIN32
	switch (idc)
	{
	case IDC_OK:
		{
			C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_SINGLE_MISSION));
			if (!lbox) return;
			int sel = lbox->GetCurSel();
			if (sel < 0) return;
			int value = lbox->GetValue(sel);

			if (value == -1)
			{
				_directory = _directory + lbox->GetData(sel) + RString("\\");
				LoadDirectory();
				return;
			}
			else if (value == -2)
			{
				int last = _directory.GetLength() - 1;
				if (last < 0) return;
				Assert(_directory[last] == '\\');
				RString name = _directory.Substring(0, last);
				const char *ext = strrchr(name, '\\');
				if (ext) _directory = name.Substring(0, ext - (const char *)name + 1);
				else _directory = "";
				LoadDirectory();
				return;
			}
			else Exit(idc);
		}
		break;
	case IDC_CANCEL:
		{
			_exitWhenClose = idc;
			ControlObjectContainerAnim *ctrl =
				dynamic_cast<ControlObjectContainerAnim *>(GetCtrl(IDC_SINGLE_MISSION_PAD));
			if (ctrl) ctrl->Close();
		}
		break;
	case IDC_SINGLE_LOAD:
		Exit(idc);
		break;
	case IDC_SINGLE_DIFF:
		_cadetMode = !_cadetMode;
		SaveParams();
		OnChangeDifficulty();
		break;
	default:
		Display::OnButtonClicked(idc);
		break;
	}
#endif
}

void DisplaySingleMission::OnCtrlClosed(int idc)
{
	if (idc == IDC_SINGLE_MISSION_PAD)
		Exit(_exitWhenClose);
	else
		Display::OnCtrlClosed(idc);
}

// Load campaign display
DisplayCampaignLoad::DisplayCampaignLoad(ControlsContainer *parent)
	: Display(parent)
{
	LoadCampaigns();
	Load("RscDisplayCampaignLoad");

	_currentCampaign = 0;
	LoadParams();

	_showStatistics = false;

	OnChangeCampaign();
	OnChangeDifficulty();

	IControl *ctrl = GetCtrl(IDC_CAMPAIGN_DESCRIPTION);
	if (ctrl) ctrl->ShowCtrl(false);
	ctrl = GetCtrl(IDC_CAMPAIGN_MISSION);
	if (ctrl) ctrl->ShowCtrl(false);
}

/*!
\patch 1.43 Date 1/29/2002 by Jirka
- Improved: Last played campaign is selected in Campaign book 
*/

void DisplayCampaignLoad::LoadParams()
{
	ParamArchiveLoad ar(GetUserParams());
	if (ar.Serialize("cadetMode", _cadetMode, 1, true) != LSOK)
	{
		WarningMessage("Cannot load user paremeters.");
	}
	RString campaign;
	if (ar.Serialize("currentCampaign", campaign, 1, "") != LSOK)
	{
		WarningMessage("Cannot load user paremeters.");
	}
	if (campaign.GetLength() > 0)
	{
		for (int i=0; i<_campaigns.Size(); i++)
		{
			if (stricmp(_campaigns[i].campaignName, campaign) == 0)
			{
				_currentCampaign = i;
				break;
			}
		}
	}
}

void DisplayCampaignLoad::SaveParams()
{
	ParamArchiveSave ar(UserInfoVersion);
	ar.Parse(GetUserParams());
	if (ar.Serialize("cadetMode", _cadetMode, 1) != LSOK)
	{
		// TODO: save failed
	}
	RString campaign;
	if (_currentCampaign >= 0 && _currentCampaign < _campaigns.Size())
		campaign = _campaigns[_currentCampaign].campaignName;
	if (ar.Serialize("currentCampaign", campaign, 1) != LSOK)
	{
		// TODO: save failed
	}
	if (ar.Save(GetUserParams()) != LSOK)
	{
		// TODO: save failed
	}
}

void DisplayCampaignLoad::OnChangeDifficulty()
{
	CActiveText *ctrl = dynamic_cast<CActiveText *>(GetCtrl(IDC_CAMPAIGN_DIFF));
	if (!ctrl) return;
	RString text; 
	if (_cadetMode) text = LocalizeString(IDS_DIFF_CADET);
	else text = LocalizeString(IDS_DIFF_VETERAN);
	ctrl->SetText(text);
}

void DisplayCampaignLoad::OnLBSelChanged(int idc, int curSel)
{
	if (idc == IDC_CAMPAIGN_HISTORY)
	{
		OnChangeMission();
	}
	else
		Display::OnLBSelChanged(idc, curSel);
}

void DisplayCampaignLoad::OnLBDblClick(int idc, int curSel)
{
	if (idc == IDC_CAMPAIGN_HISTORY)
	{
		C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_CAMPAIGN_HISTORY));
		if (!lbox) return;
		int sel = lbox->GetCurSel();
		DoAssert(sel >= 0);
		int value = lbox->GetValue(sel);
		if (value < 0)
			OnButtonClicked(IDC_OK);
		else
			OnButtonClicked(IDC_CAMPAIGN_REPLAY);
	}
}

void DisplayCampaignLoad::OnButtonClicked(int idc)
{
	switch (idc)
	{
		case IDC_CANCEL:
			{
				_exitWhenClose = idc;
				ControlObjectContainerAnim *ctrl =
					dynamic_cast<ControlObjectContainerAnim *>(GetCtrl(IDC_CAMPAIGN_BOOK));
				if (ctrl) ctrl->Close();
			}
			break;
		case IDC_OK:
			{
				C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_CAMPAIGN_HISTORY));
				if (!lbox) break;
				int sel = lbox->GetCurSel();
				DoAssert(sel >= 0);
				int value = lbox->GetValue(sel);
				if (value == -1 || sel == lbox->GetSize() - 1)
					Exit(IDC_OK);
				else
					CreateChild(new DisplayRevert(this));
			}
			break;
		case IDC_CAMPAIGN_REPLAY:
			{
				C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_CAMPAIGN_HISTORY));
				if (!lbox) break;
				int sel = lbox->GetCurSel();
				DoAssert(sel >= 0);
				int value = lbox->GetValue(sel);
				if (value >= 0)
				{
					CampaignHistory &campaign = _campaigns[_currentCampaign];
					BattleHistory &battle = campaign.battles[value >> 16];
					MissionHistory &mission = battle.missions[value & 0xffff];
					if (mission.completed) Exit(idc);
				}
			}
			break;
		case IDC_CAMPAIGN_PREV:
			if (_currentCampaign > 0)
			{
				_currentCampaign--;
				SaveParams();
				OnChangeCampaign();
			}
			break;
		case IDC_CAMPAIGN_NEXT:
			if (_currentCampaign < _campaigns.Size() - 1)
			{
				_currentCampaign++;
				SaveParams();
				OnChangeCampaign();
			}
			break;
		case IDC_CAMPAIGN_DIFF:
			_cadetMode = !_cadetMode;
			SaveParams();
			OnChangeDifficulty();
			break;
		default:
			Display::OnButtonClicked(idc);
			break;
	}
}

void DisplayCampaignLoad::OnChildDestroyed(int idd, int exit)
{
	Display::OnChildDestroyed(idd, exit);
	
	if (idd == IDD_REVERT && exit == IDC_OK) Exit(IDC_OK);
}

void DisplayCampaignLoad::OnCtrlClosed(int idc)
{
	if (idc == IDC_CAMPAIGN_BOOK)
		Exit(_exitWhenClose);
	else
		Display::OnCtrlClosed(idc);
}

void DisplayCampaignLoad::OnChangeCampaign()
{
#ifdef _WIN32
	if (_currentCampaign < 0 || _currentCampaign >= _campaigns.Size())
	{
		if (GetCtrl(IDC_CAMPAIGN_PREV)) GetCtrl(IDC_CAMPAIGN_PREV)->ShowCtrl(false);
		if (GetCtrl(IDC_CAMPAIGN_NEXT)) GetCtrl(IDC_CAMPAIGN_NEXT)->ShowCtrl(false);
		return;
	}

	CampaignHistory &campaign = _campaigns[_currentCampaign];
	_cfg.Clear();
	_cfg.Parse(GetCampaignDirectory(campaign.campaignName) + RString("description.ext"));

	C3DStatic *text = dynamic_cast<C3DStatic *>(GetCtrl(IDC_CAMPAIGN_CAMPAIGN));
	if (text)
	{
		text->SetText(_cfg >> "Campaign" >> "name");
	}

	C3DActiveText *active = dynamic_cast<C3DActiveText *>(GetCtrl(IDC_CAMPAIGN_PREV));
	if (active)
	{
		active->ShowCtrl(_currentCampaign > 0);
	}

	active = dynamic_cast<C3DActiveText *>(GetCtrl(IDC_CAMPAIGN_NEXT));
	if (active)
	{
		active->ShowCtrl(_currentCampaign < _campaigns.Size() - 1);
	}

	C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_CAMPAIGN_HISTORY));
	if (lbox)
	{
		lbox->ClearStrings();
		int index = lbox->AddString(LocalizeString(IDS_CAMPAIGN_BEGIN));
		lbox->SetValue(index, -2);
		for (int i=0; i<campaign.battles.Size(); i++)
		{
			BattleHistory &battle = campaign.battles[i];
			for (int j=0; j<battle.missions.Size(); j++)
			{
				MissionHistory &mission = battle.missions[j];
				if (mission.displayName.GetLength() == 0) continue;
				int index = lbox->AddString(mission.displayName);
//				lbox->SetData(index, mission.missionName);
				lbox->SetValue(index, (i << 16) + j);
			}
/*
			RString text = _cfg >> "Campaign" >> battle.battleName >> "name";
			int index = lbox->AddString(text);
			lbox->SetData(index, battle.battleName);
			lbox->SetValue(index, i);
*/
		}
		RString dir = GetCampaignSaveDirectory(campaign.campaignName);
		CheckContinueSave(dir);
		RString save = dir + RString("continue.fps");
		if (QIFStream::FileExists(save))
		{
			HANDLE handle = ::CreateFile
			(
				save, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL
			);
			FILETIME info, infoLocal;
			::GetFileTime(handle, NULL, NULL, &info);
			::CloseHandle(handle);
			::FileTimeToLocalFileTime(&info, &infoLocal);
			SYSTEMTIME stime;
			::FileTimeToSystemTime(&infoLocal, &stime);
			int day = stime.wDay;
			int month = stime.wMonth;
			int year = stime.wYear;
			int minute = stime.wMinute;
			int hour = stime.wHour;
			char buffer[256];
			sprintf(buffer, LocalizeString(IDS_CAMPAIGN_LB_CONTINUE), month, day, year, hour, minute);
			int index = lbox->AddString(buffer);
			lbox->SetValue(index, -1);
		}
		lbox->SetCurSel(lbox->GetSize() - 1);
	}
//	OnChangeBattle();
#endif
}

/*
void DisplayCampaignLoad::OnChangeBattle()
{
	C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_CAMPAIGN_HISTORY));
	if (!lbox) return;

	CampaignHistory &campaign = _campaigns[_currentCampaign];

	int sel = lbox->GetCurSel();
	if (sel < 0)
	{
		const ParamEntry *entry = (_cfg >> "Campaign").FindEntry("description");
		if (entry)
		{
			RString description = *entry;
			C3DHTML *html = dynamic_cast<C3DHTML *>(GetCtrl(IDC_CAMPAIGN_DESCRIPTION));
			if (html)
				html->Load
				(
					GetCampaignDirectory(campaign.campaignName) +
					description
				);
		}
	}
	else
	{
		RString battle = lbox->GetData(sel);
		const ParamEntry *entry = (_cfg >> "Campaign" >> battle).FindEntry("description");
		if (entry)
		{
			RString description = *entry;
			C3DHTML *html = dynamic_cast<C3DHTML *>(GetCtrl(IDC_CAMPAIGN_DESCRIPTION));
			if (html)
				html->Load
				(
					GetCampaignDirectory(campaign.campaignName) +
					description
				);
		}
	}
}
*/

void DisplayCampaignLoad::OnChangeMission()
{
#ifdef _WIN32
	C3DListBox *lbox = dynamic_cast<C3DListBox *>(GetCtrl(IDC_CAMPAIGN_HISTORY));
	if (!lbox) return;

	CampaignHistory &campaign = _campaigns[_currentCampaign];

	int sel = lbox->GetCurSel();
	DoAssert(sel >= 0);
	int value = lbox->GetValue(sel);
	if (value < 0)
	{
		C3DHTML *html = dynamic_cast<C3DHTML *>(GetCtrl(IDC_CAMPAIGN_DESCRIPTION));
		if (html)
			html->Load
			(
				GetCampaignDirectory(campaign.campaignName) +
				RString("overview.html")
			);

		C3DStatic *text = dynamic_cast<C3DStatic *>(GetCtrl(IDC_CAMPAIGN_MISSION));
		if (text)
			text->SetText("");

		CActiveText *ctrl = dynamic_cast<CActiveText *>(GetCtrl(IDC_CAMPAIGN_REPLAY));
		if (ctrl) ctrl->ShowCtrl(false);

		ctrl = dynamic_cast<CActiveText *>(GetCtrl(IDC_OK));
		if (ctrl)
		{
			if (value == -2)
				ctrl->SetText(LocalizeString(IDS_CAMPAIGN_BEGIN));
			else
				ctrl->SetText(LocalizeString(IDS_CAMPAIGN_RESUME));
		}
		
		UpdateStatistics(NULL, NULL);
	}
	else
	{
		int battleIndex = value >> 16;
		int missionIndex = value & 0xffff;
		BattleHistory &battle = campaign.battles[battleIndex];
		MissionHistory &mission = battle.missions[missionIndex];
		RString templ = _cfg >> "Campaign" >> battle.battleName >> mission.missionName >> "template";		

		C3DHTML *html = dynamic_cast<C3DHTML *>(GetCtrl(IDC_CAMPAIGN_DESCRIPTION));
		if (html)
			html->Load
			(
				GetCampaignMissionDirectory(campaign.campaignName, templ) +
				RString("overview.html")
			);

		C3DStatic *text = dynamic_cast<C3DStatic *>(GetCtrl(IDC_CAMPAIGN_MISSION));
		if (text)
			text->SetText(mission.displayName);

		CActiveText *ctrl = dynamic_cast<CActiveText *>(GetCtrl(IDC_CAMPAIGN_REPLAY));
		if (ctrl) ctrl->ShowCtrl(mission.completed);

		ctrl = dynamic_cast<CActiveText *>(GetCtrl(IDC_OK));
		if (ctrl)
		{
			if (sel == lbox->GetSize() - 1)
				ctrl->SetText(LocalizeString(IDS_CAMPAIGN_RESUME));
			else
				ctrl->SetText(LocalizeString(IDS_CAMPAIGN_RESTART));
		}

		AIStatsCampaign *newStats = NULL;
		missionIndex++;
		if (missionIndex < battle.missions.Size())
		{
			MissionHistory &newMission = battle.missions[missionIndex];
			newStats = &newMission.stats;
		}
		else
		{
			do
			{
				battleIndex++;
			} while (battleIndex < campaign.battles.Size() && campaign.battles[battleIndex].missions.Size() == 0);
			if (battleIndex < campaign.battles.Size())
			{
				BattleHistory &newBattle = campaign.battles[battleIndex];
				MissionHistory &newMission = newBattle.missions[0];
				newStats = &newMission.stats;
			}
		}
		UpdateStatistics(&mission.stats, newStats);
	}
#endif
}

void DisplayCampaignLoad::ShowStatistics(bool show)
{
#ifdef _WIN32
	GetCtrl(IDC_CAMPAIGN_DATE)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_SCORE)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_DURATION)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_CASUALTIES)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_KILLS_TITLE)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_ENEMY_ROW)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_FRIENDLY_ROW)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_CIVILIAN_ROW)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_INFANTRY_COLUMN)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_SOFT_COLUMN)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_ARMORED_COLUMN)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_AIRCRAFT_COLUMN)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_TOTAL_COLUMN)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_EINF)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_ESOFT)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_EARM)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_EAIR)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_ETOT)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_FINF)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_FSOFT)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_FARM)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_FAIR)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_FTOT)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_CINF)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_CSOFT)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_CARM)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_CAIR)->ShowCtrl(show);
	GetCtrl(IDC_CAMPAIGN_CTOT)->ShowCtrl(show);
#endif
}

void DisplayCampaignLoad::UpdateStatistics(AIStatsCampaign *begin, AIStatsCampaign *end)
{
#ifdef _WIN32
	if (_showStatistics && begin != NULL && end != NULL)
	{
		ShowStatistics(true);

		C3DStatic *ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_CAMPAIGN_DATE));
		if (ctrl)
		{
			if (end->_date == 0) ctrl->ShowCtrl(false);
			else
			{
				tm *t = localtime(&end->_date);
				char buffer[256];
				strftime(buffer, 256, LocalizeString(IDS_CAMPAIGN_DATE_FORMAT), t);
				RString value = Format(LocalizeString(IDS_CAMPAIGN_DATE), buffer);
				ctrl->SetText(value);
			}
		}
		ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_CAMPAIGN_SCORE));
		if (ctrl)
		{
			RString value = Format(LocalizeString(IDS_CAMPAIGN_SCORE), end->_score - begin->_score, end->_score);
			ctrl->SetText(value);
		}
		ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_CAMPAIGN_DURATION));
		if (ctrl)
		{
			float t = end->_inCombat * (1.0f / 3600.0f);
			int hours = toIntFloor(t); t -= hours; t *= 60;
			int minutes = toInt(t);
			RString total;
			if (hours > 0) total = Format(LocalizeString(IDS_DURATION_LONG), hours, minutes);
			else total = Format(LocalizeString(IDS_DURATION_SHORT), minutes);

			t = (end->_inCombat - begin->_inCombat) * (1.0f / 3600.0f);
			hours = toIntFloor(t); t -= hours; t *= 60;
			minutes = toInt(t);
			RString mission;
			if (hours > 0) mission = Format(LocalizeString(IDS_DURATION_LONG), hours, minutes);
			else mission = Format(LocalizeString(IDS_DURATION_SHORT), minutes);

			RString value = Format(LocalizeString(IDS_CAMPAIGN_DURATION), (const char *)mission, (const char *)total);
			ctrl->SetText(value);
		}
		ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_CAMPAIGN_CASUALTIES));
		if (ctrl)
		{
			RString value = Format(LocalizeString(IDS_CAMPAIGN_CASUALTIES), end->_casualties - begin->_casualties, end->_casualties);
			ctrl->SetText(value);
		}

		ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_CAMPAIGN_EINF));
		if (ctrl)
		{
			int total = end->_kills[SKEnemyInfantry];
			int mission = total - begin->_kills[SKEnemyInfantry];
			if (mission != 0 || total != 0)
			{
				RString value = Format("%d(%d)", mission, total);
				ctrl->SetText(value);
			}
			else ctrl->SetText(RString());
		}
		ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_CAMPAIGN_ESOFT));
		if (ctrl)
		{
			int total = end->_kills[SKEnemySoft];
			int mission = total - begin->_kills[SKEnemySoft];
			if (mission != 0 || total != 0)
			{
				RString value = Format("%d(%d)", mission, total);
				ctrl->SetText(value);
			}
			else ctrl->SetText(RString());
		}
		ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_CAMPAIGN_EARM));
		if (ctrl)
		{
			int total = end->_kills[SKEnemyArmor];
			int mission = total - begin->_kills[SKEnemyArmor];
			if (mission != 0 || total != 0)
			{
				RString value = Format("%d(%d)", mission, total);
				ctrl->SetText(value);
			}
			else ctrl->SetText(RString());
		}
		ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_CAMPAIGN_EAIR));
		if (ctrl)
		{
			int total = end->_kills[SKEnemyAir];
			int mission = total - begin->_kills[SKEnemyAir];
			if (mission != 0 || total != 0)
			{
				RString value = Format("%d(%d)", mission, total);
				ctrl->SetText(value);
			}
			else ctrl->SetText(RString());
		}
		ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_CAMPAIGN_ETOT));
		if (ctrl)
		{
			int total = end->_kills[SKEnemyInfantry] + end->_kills[SKEnemySoft] + end->_kills[SKEnemyArmor] + end->_kills[SKEnemyAir];
			int mission = total - begin->_kills[SKEnemyInfantry] - begin->_kills[SKEnemySoft] - begin->_kills[SKEnemyArmor] - begin->_kills[SKEnemyAir];
			if (mission != 0 || total != 0)
			{
				RString value = Format("%d(%d)", mission, total);
				ctrl->SetText(value);
			}
			else ctrl->SetText(RString());
		}
		ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_CAMPAIGN_FINF));
		if (ctrl)
		{
			int total = end->_kills[SKFriendlyInfantry];
			int mission = total - begin->_kills[SKFriendlyInfantry];
			if (mission != 0 || total != 0)
			{
				RString value = Format("%d(%d)", mission, total);
				ctrl->SetText(value);
			}
			else ctrl->SetText(RString());
		}
		ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_CAMPAIGN_FSOFT));
		if (ctrl)
		{
			int total = end->_kills[SKFriendlySoft];
			int mission = total - begin->_kills[SKFriendlySoft];
			if (mission != 0 || total != 0)
			{
				RString value = Format("%d(%d)", mission, total);
				ctrl->SetText(value);
			}
			else ctrl->SetText(RString());
		}
		ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_CAMPAIGN_FARM));
		if (ctrl)
		{
			int total = end->_kills[SKFriendlyArmor];
			int mission = total - begin->_kills[SKFriendlyArmor];
			if (mission != 0 || total != 0)
			{
				RString value = Format("%d(%d)", mission, total);
				ctrl->SetText(value);
			}
			else ctrl->SetText(RString());
		}
		ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_CAMPAIGN_FAIR));
		if (ctrl)
		{
			int total = end->_kills[SKFriendlyAir];
			int mission = total - begin->_kills[SKFriendlyAir];
			if (mission != 0 || total != 0)
			{
				RString value = Format("%d(%d)", mission, total);
				ctrl->SetText(value);
			}
			else ctrl->SetText(RString());
		}
		ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_CAMPAIGN_FTOT));
		if (ctrl)
		{
			int total = end->_kills[SKFriendlyInfantry] + end->_kills[SKFriendlySoft] + end->_kills[SKFriendlyArmor] + end->_kills[SKFriendlyAir];
			int mission = total - begin->_kills[SKFriendlyInfantry] - begin->_kills[SKFriendlySoft] - begin->_kills[SKFriendlyArmor] - begin->_kills[SKFriendlyAir];
			if (mission != 0 || total != 0)
			{
				RString value = Format("%d(%d)", mission, total);
				ctrl->SetText(value);
			}
			else ctrl->SetText(RString());
		}
		ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_CAMPAIGN_CINF));
		if (ctrl)
		{
			int total = end->_kills[SKCivilianInfantry];
			int mission = total - begin->_kills[SKCivilianInfantry];
			if (mission != 0 || total != 0)
			{
				RString value = Format("%d(%d)", mission, total);
				ctrl->SetText(value);
			}
			else ctrl->SetText(RString());
		}
		ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_CAMPAIGN_CSOFT));
		if (ctrl)
		{
			int total = end->_kills[SKCivilianSoft];
			int mission = total - begin->_kills[SKCivilianSoft];
			if (mission != 0 || total != 0)
			{
				RString value = Format("%d(%d)", mission, total);
				ctrl->SetText(value);
			}
			else ctrl->SetText(RString());
		}
		ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_CAMPAIGN_CARM));
		if (ctrl)
		{
			int total = end->_kills[SKCivilianArmor];
			int mission = total - begin->_kills[SKCivilianArmor];
			if (mission != 0 || total != 0)
			{
				RString value = Format("%d(%d)", mission, total);
				ctrl->SetText(value);
			}
			else ctrl->SetText(RString());
		}
		ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_CAMPAIGN_CAIR));
		if (ctrl)
		{
			int total = end->_kills[SKCivilianAir];
			int mission = total - begin->_kills[SKCivilianAir];
			if (mission != 0 || total != 0)
			{
				RString value = Format("%d(%d)", mission, total);
				ctrl->SetText(value);
			}
			else ctrl->SetText(RString());
		}
		ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_CAMPAIGN_CTOT));
		if (ctrl)
		{
			int total = end->_kills[SKCivilianInfantry] + end->_kills[SKCivilianSoft] + end->_kills[SKCivilianArmor] + end->_kills[SKCivilianAir];
			int mission = total - begin->_kills[SKCivilianInfantry] - begin->_kills[SKCivilianSoft] - begin->_kills[SKCivilianArmor] - begin->_kills[SKCivilianAir];
			if (mission != 0 || total != 0)
			{
				RString value = Format("%d(%d)", mission, total);
				ctrl->SetText(value);
			}
			else ctrl->SetText(RString());
		}
	}
	else
	{
		ShowStatistics(false);
	}
#endif
}

void DisplayCampaignLoad::LoadCampaigns()
{
#ifdef _WIN32
	RString dir = GetTmpSaveDirectory();

	_campaigns.Clear();

	_finddata_t info;
	long h = _findfirst("Campaigns\\*.*", &info);
	if (h != -1)
	{
		do
		{
			if 
			(
				(info.attrib & _A_SUBDIR) != 0 &&
				info.name[0] != '.'
			)
			{
				RString name = info.name;
				int i = _campaigns.Add();
				CampaignHistory &campaign = _campaigns[i];
				campaign.Clear(name);

				char buffer[256];
				sprintf(buffer, "%s%s.sqc", (const char *)dir, (const char *)name);
				ParamArchiveLoad ar;
				if
				(
					ar.LoadBin(buffer) || ar.Load(buffer) == LSOK
				)
				{
					if (ar.Serialize("Campaign", campaign, 1) != LSOK)
					{
						campaign.Clear(name);
						WarningMessage("Cannot load campaign description %s", buffer);
					}
				}
			}
		}
		while (0==_findnext(h, &info));
		_findclose(h);
	}
/*	
	h = _findfirst("Campaigns\\*.pb?", &info);
	if (h != -1)
	{
		do
		{
			char name[256];
			strcpy(name, info.name);
			char *ext = strrchr(name, '.');
			Assert(ext);
			*ext = 0;

			int i = _campaigns.Add();
			CampaignHistory &campaign = _campaigns[i];
			campaign.Clear(name);

			char buffer[256];
			sprintf(buffer, "%s%s.sqc", (const char *)dir, name);
			ParamArchiveLoad ar;
			if
			(
				ar.LoadBin(buffer) || ar.Load(buffer) == LSOK
			)
			{
				if (ar.Serialize("Campaign", campaign, 1) != LSOK)
				{
					campaign.Clear(name);
					WarningMessage("Cannot load campaign description %s from bank", buffer);
				}
			}
		}
		while (0==_findnext(h, &info));
		_findclose(h);
	}
*/
	static const char campaigns[] = "campaigns\\";
	for (int i=0; i<GFileBanks.Size(); i++)
	{
		const QFBank &bank = GFileBanks[i];
		const char *prefix = bank.GetPrefix();
		if (strnicmp(prefix, campaigns, strlen(campaigns)) == 0)
		{
			char name[256];
			strcpy(name, prefix + strlen(campaigns));
			int n = strlen(name) - 1;
			if (name[n] == '\\' || name[n] == '/') name[n] = 0;

			int i = _campaigns.Add();
			CampaignHistory &campaign = _campaigns[i];
			campaign.Clear(name);

			char buffer[256];
			sprintf(buffer, "%s%s.sqc", (const char *)dir, name);
			ParamArchiveLoad ar;
			if
			(
				ar.LoadBin(buffer) || ar.Load(buffer) == LSOK
			)
			{
				if (ar.Serialize("Campaign", campaign, 1) != LSOK)
				{
					campaign.Clear(name);
					WarningMessage("Cannot load campaign description %s from bank", buffer);
				}
			}
		}
	}
#endif
}

void DisplayCampaignLoad::OnSimulate(EntityAI *vehicle)
{
	if (GInput.CheatActivated() == CheatUnlockCampaign)
	{
		OnCampaignCheat();
		GInput.CheatServed();
	}
	bool show = true;
	ControlObjectContainerAnim *book = dynamic_cast<ControlObjectContainerAnim *>(GetCtrl(IDC_CAMPAIGN_BOOK));
	if (book) show = book->GetPhase() > 0.75;
	IControl *ctrl = GetCtrl(IDC_CAMPAIGN_DESCRIPTION);
	if (ctrl) ctrl->ShowCtrl(show);
	ctrl = GetCtrl(IDC_CAMPAIGN_MISSION);
	if (ctrl) ctrl->ShowCtrl(show);

	if (show && !_showStatistics)
	{
		_showStatistics = true;
		OnChangeMission();
	}
	else if (!show && _showStatistics)
	{
		_showStatistics = false;
		ShowStatistics(false);
	}

	Display::OnSimulate(vehicle);
}

void DisplayCampaignLoad::OnCampaignCheat()
{
	CampaignHistory &campaign = _campaigns[_currentCampaign];
	RString name = campaign.campaignName;
	campaign.Clear(name);

	RString filename = GetCampaignDirectory(name) + RString("description.ext"); 
	if (!QIFStreamB::FileExist(filename)) return;
	ParamFile description;
	description.Parse(filename);

	const ParamEntry &cfgCampaign = description >> "Campaign";
	for (int i=0; i<cfgCampaign.GetEntryCount(); i++)
	{
		const ParamEntry &cfgBattle = cfgCampaign.GetEntry(i);
		if (!cfgBattle.IsClass()) continue;
		int index = campaign.battles.Add();
		BattleHistory &battle = campaign.battles[index];
		battle.battleName = cfgBattle.GetName();
		for (int j=0; j<cfgBattle.GetEntryCount(); j++)
		{
			const ParamEntry &cfgMission = cfgBattle.GetEntry(j);
			if (!cfgMission.IsClass()) continue;
			int index = battle.missions.Add();
			MissionHistory &mission = battle.missions[index];
			mission.missionName = cfgMission.GetName();
			mission.displayName = cfgMission >> "template";
			mission.completed = true;
		}
	}
	OnChangeCampaign();
}

DisplayRevert::DisplayRevert(ControlsContainer *parent)
: Display(parent)
{
	Load("RscDisplayRevert");
	_exitWhenClose = -1;
	
	ControlObjectContainerAnim *book = dynamic_cast<ControlObjectContainerAnim *>(GetCtrl(IDC_REVERT_BOOK));
	if (book) book->Open();

	IControl *ctrl = GetCtrl(IDC_REVERT_TITLE);
	if (ctrl) ctrl->ShowCtrl(false);
	ctrl = GetCtrl(IDC_REVERT_QUESTION);
	if (ctrl)
	{
		ctrl->ShowCtrl(false);
		C3DStatic *text = dynamic_cast<C3DStatic *>(ctrl);
		if (text) text->FormatText();
	}
}

void DisplayRevert::OnButtonClicked(int idc)
{
	switch (idc)
	{
		case IDC_CANCEL:
			{
				_exitWhenClose = idc;
				ControlObjectContainerAnim *book =
					dynamic_cast<ControlObjectContainerAnim *>(GetCtrl(IDC_REVERT_BOOK));
				if (book)	book->Close();
			}
			break;
		default:
			Display::OnButtonClicked(idc);
			break;
	}
}

void DisplayRevert::OnSimulate(EntityAI *vehicle)
{
	ControlObjectContainerAnim *book =
		dynamic_cast<ControlObjectContainerAnim *>(GetCtrl(IDC_REVERT_BOOK));
	bool show = true;
	if (book)
	{
		if (_exitWhenClose >= 0 && book->GetPhase() <= 0.6)
		{
			Exit(_exitWhenClose);
			return;
		}
		show = book->GetPhase() > 0.75;
	}
	IControl *ctrl = GetCtrl(IDC_REVERT_TITLE);
	if (ctrl) ctrl->ShowCtrl(show);
	ctrl = GetCtrl(IDC_REVERT_QUESTION);
	if (ctrl) ctrl->ShowCtrl(show);

	Display::OnSimulate(vehicle);
}

// Game display
Control *DisplayGame::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
#ifdef _WIN32
	if (idc == IDC_GAME_SELECT)
	{
		Assert(type == CT_LISTBOX);
		CListBox *ctrl = new CListBox(this, idc, cls);
		{
			RString dir = GetTmpSaveDirectory();
			char buffer[256];
			sprintf(buffer, "%s*.fps", (const char *)dir);
			_finddata_t info;
			long h = _findfirst(buffer, &info);
			if (h != -1)
			{
				do
				{
					char name[256];
					strcpy(name,info.name);
					char *ext=strrchr(name,'.');
					if( ext ) *ext=0;
					ctrl->AddString(name);
				}
				while (0==_findnext(h, &info));
				_findclose(h);
			}
		}
		ctrl->SortItems();
		ctrl->SetCurSel(0);
		return ctrl;
	}
#endif
	return Display::OnCreateCtrl(type, idc, cls);
}

bool DisplayGame::CanDestroy()
{
	if (!Display::CanDestroy()) return false;
#ifdef _WIN32
	if (_exit == IDC_OK)
	{
		CListBox *ctrl = dynamic_cast<CListBox *>(GetCtrl(IDC_GAME_SELECT));
		Assert(ctrl);
		if (ctrl->GetCurSel() < 0)
		{
			CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_SELECT_GAME));
			return false;
		}
	}
#endif
	return true;
}

// Save display
bool DisplaySave::CanDestroy()
{
	if (!Display::CanDestroy()) return false;
#ifdef _WIN32
	if (_exit != IDC_CANCEL)
	{
		CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_SIDE_NAME));
		Assert(edit);
		if (!edit->GetText() || !edit->GetText()[0])
		{
			CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_NAME_EMPTY));
			return false;
		}
	}
#endif
	return true;
}

// Options display
DisplayOptions::DisplayOptions(ControlsContainer *parent, bool enableSimulation, bool credits)
	: Display(parent)
{
	_enableSimulation = enableSimulation;
	Load("RscDisplayOptions");
#if _ENABLE_AI && !_FORCE_DEMO_ISLAND
	if (!credits)
#endif
		GetCtrl(IDC_OPTIONS_CREDITS)->ShowCtrl(false);
}

void DisplayOptions::OnButtonClicked(int idc)
{
#ifdef _WIN32
	switch (idc)
	{
	case IDC_CANCEL:
		{
			_exitWhenClose = idc;
			ControlObjectContainerAnim *ctrl =
				dynamic_cast<ControlObjectContainerAnim *>(GetCtrl(IDC_OPTIONS_NOTEBOOK));
			if (ctrl) ctrl->Close();
		}
		break;
	case IDC_OPTIONS_VIDEO:
		CreateChild(new DisplayOptionsVideo(this, _enableSimulation));
		break;
	case IDC_OPTIONS_AUDIO:
		CreateChild(new DisplayOptionsAudio(this, _enableSimulation));
		break;
	case IDC_OPTIONS_CONFIGURE:
		CreateChild(new DisplayConfigure(this, _enableSimulation));
		break;
	case IDC_OPTIONS_DIFFICULTY:
		CreateChild(new DisplayDifficulty(this, _enableSimulation));
		break;
#if _ENABLE_AI
	case IDC_OPTIONS_CREDITS:
		{
			RString cutscene = Pars >> "CfgCredits" >> "cutscene";
			char buffer[256];
			strcpy(buffer, cutscene);

			// parse mission name

			// mission
			char *world = strchr(buffer, '.');
			Assert(world);
			*world = 0;
			world++;

			SetMission(world, buffer, AnimsDir);
			SetBaseDirectory("");

			ParseIntro();

			if (CurrentTemplate.groups.Size() > 0)
			{
				GLOB_WORLD->SwitchLandscape(GetWorldName(world));
				GWorld->ActivateAddons(CurrentTemplate.addOns);
				GLOB_WORLD->InitGeneral(CurrentTemplate.intel);
				GLOB_WORLD->InitVehicles(GModeIntro, CurrentTemplate);
				CreateChild(new DisplayIntro(this));
			}
		}
		break;
#endif
	default:
		Display::OnButtonClicked(idc);
	}
#endif
}

void DisplayOptions::OnChildDestroyed(int idd, int exit)
{
#ifdef _WIN32
	Display::OnChildDestroyed(idd, exit);

	switch (idd)
	{
	case IDD_INTRO:
		GWorld->DestroyMap(IDC_OK);
		void ShowCinemaBorder(bool show);
		ShowCinemaBorder(true);
		StartRandomCutscene(Glob.header.worldname);
		break;
	}
#endif
}

void DisplayOptions::OnCtrlClosed(int idc)
{
	if (idc == IDC_OPTIONS_NOTEBOOK)
		Exit(_exitWhenClose);
	else
		Display::OnCtrlClosed(idc);
}

extern bool EnableHWTL;

extern void ReleaseVBuffers();
extern void RestoreVBuffers();

void SetHWTL(bool val)
{
	if (EnableHWTL==val) return;
	EnableHWTL = val;
	if (!EnableHWTL)
	{
		ReleaseVBuffers();
	}
	else
	{
		RestoreVBuffers();
	}
}

// Video options display
/*!
\patch 1.27 Date 10/11/2001 by Jirka
- Added: non-blood option (Video options display)
\patch 1.30 Date 11/02/2001 by Jirka
- Added: multitexturing option (Video options display)
\patch 1.30 Date 11/02/2001 by Jirka
- Added: w-buffer option (Video options display)
*/
DisplayOptionsVideo::DisplayOptionsVideo(ControlsContainer *parent, bool enableSimulation)
	: Display(parent)
{
	_enableSimulation = enableSimulation;
	Load("RscDisplayOptionsVideo");

	_oldBright = GEngine->GetBrightness();
	_oldGamma = GEngine->GetGamma();
	_oldFrameRate = GScene->GetFrameRateSettings();
	_oldQuality = GScene->GetQualitySettings();

	// TODO: save actual resolution and refresh rate

	_oldResX = GEngine->Width();
	_oldResY = GEngine->Height();
	_oldBpp = GEngine->PixelSize();
	_oldWindowed = GEngine->IsWindowed();
	_oldRefreshRate = GEngine->RefreshRate();

	_oldObjShadows = GScene->GetObjectShadows();
	_oldVehShadows = GScene->GetVehicleShadows();
	_oldSmokes = GScene->GetCloudlets();
	//_oldOcclusions;

	_oldBlood = Glob.config.blood;

	_oldHWTL = EnableHWTL;
	_oldMultitexturing = GEngine->IsMultitexturing();

	_oldWBuffer = GEngine->IsWBuffer();

	_oldVisibility = GScene->GetPreferredViewDistance();

	_oldTerrain = GScene->GetPreferredTerrainGrid();

#ifdef _WIN32
	{
		C3DSlider *ctrl = dynamic_cast<C3DSlider *>(GetCtrl(IDC_OPTIONS_BRIGHT_SLIDER));
		if (ctrl) OnSliderPosChanged(ctrl->IDC(), ctrl->GetThumbPos());
		ctrl = dynamic_cast<C3DSlider *>(GetCtrl(IDC_OPTIONS_GAMMA_SLIDER));
		if (ctrl) OnSliderPosChanged(ctrl->IDC(), ctrl->GetThumbPos());
		ctrl = dynamic_cast<C3DSlider *>(GetCtrl(IDC_OPTIONS_RATE_SLIDER));
		if (ctrl) OnSliderPosChanged(ctrl->IDC(), ctrl->GetThumbPos());
		ctrl = dynamic_cast<C3DSlider *>(GetCtrl(IDC_OPTIONS_QUALITY_SLIDER));
		if (ctrl) OnSliderPosChanged(ctrl->IDC(), ctrl->GetThumbPos());
		ctrl = dynamic_cast<C3DSlider *>(GetCtrl(IDC_OPTIONS_VISIBILITY_SLIDER));
		if (ctrl) OnSliderPosChanged(ctrl->IDC(), ctrl->GetThumbPos());
	}
	{
		C3DActiveText *ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_OPTIONS_OBJSHADOWS));
		if (_oldObjShadows)
			ctrl->SetText(LocalizeString(IDS_ENABLED));
		else
			ctrl->SetText(LocalizeString(IDS_DISABLED));

		ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_OPTIONS_VEHSHADOWS));
		if (_oldVehShadows)
			ctrl->SetText(LocalizeString(IDS_ENABLED));
		else
			ctrl->SetText(LocalizeString(IDS_DISABLED));

		ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_OPTIONS_CLOUDLETS));
		if (_oldSmokes)
			ctrl->SetText(LocalizeString(IDS_ENABLED));
		else
			ctrl->SetText(LocalizeString(IDS_DISABLED));

		ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_OPTIONS_BLOOD));
		if (_oldBlood)
			ctrl->SetText(LocalizeString(IDS_ENABLED));
		else
			ctrl->SetText(LocalizeString(IDS_DISABLED));
		
		bool NoBlood();
		if (NoBlood())
		{
			ctrl->ShowCtrl(false);

			C3DStatic *text = static_cast<C3DStatic *>(GetCtrl(IDC_OPTIONS_BLOOD_TEXT));
			if (text) text->ShowCtrl(false);
		}

		ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_OPTIONS_HWTL));
		if (_oldHWTL)
			ctrl->SetText(LocalizeString(IDS_ENABLED));
		else
			ctrl->SetText(LocalizeString(IDS_DISABLED));

		ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_OPTIONS_MULTITEXTURING));
		if (_oldMultitexturing)
			ctrl->SetText(LocalizeString(IDS_ENABLED));
		else
			ctrl->SetText(LocalizeString(IDS_DISABLED));

		ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_OPTIONS_WBUFFER));
		if (_oldWBuffer)
			ctrl->SetText(LocalizeString(IDS_ENABLED));
		else
			ctrl->SetText(LocalizeString(IDS_DISABLED));
		ctrl->EnableCtrl(GEngine->CanWBuffer());
	}
#endif

	int res = InitResolutions();
	if (res >= 0) UpdateRefreshRates(res);
}

struct TerrainInterpolationInfo
{
	float value;
	int &ids;

	TerrainInterpolationInfo(float v, int &i) : ids(i) {value = v;}
};
static TerrainInterpolationInfo terrainInfos[] =
{
	TerrainInterpolationInfo(3.125, IDS_TERRAIN_3_125),
	TerrainInterpolationInfo(6.25, IDS_TERRAIN_6_25),
	TerrainInterpolationInfo(12.5, IDS_TERRAIN_12_5),
	TerrainInterpolationInfo(25, IDS_TERRAIN_25),
	TerrainInterpolationInfo(50, IDS_TERRAIN_50),
};

Control *DisplayOptionsVideo::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
#ifdef _WIN32
	switch (idc)
	{
		case IDC_OPTIONS_GAMMA_SLIDER:
		{
			Assert(type == CT_3DSLIDER);
			C3DSlider *ctrl = new C3DSlider(this, idc, cls);
			ctrl->SetRange(0.5, 2.3);
			ctrl->SetSpeed(0.05, 0.25);
			ctrl->SetThumbPos(GEngine->GetGamma());
//			ctrl->OnPosChanged(ctrl->GetPos());	
			return ctrl;
		}
		case IDC_OPTIONS_BRIGHT_SLIDER:
		{
			Assert(type == CT_3DSLIDER);
			C3DSlider *ctrl = new C3DSlider(this, idc, cls);
			ctrl->SetRange(0.4, 1.8);
			ctrl->SetSpeed(0.05, 0.25);
			ctrl->SetThumbPos(GEngine->GetBrightness());
//			ctrl->OnPosChanged(ctrl->GetPos());	
			return ctrl;
		}
		case IDC_OPTIONS_RATE_SLIDER:
		{
			Assert(type == CT_3DSLIDER);
			C3DSlider *ctrl = new C3DSlider(this, idc, cls);
//			ctrl->OnPosChanged(ctrl->GetPos());	
			ctrl->SetRange(10, 30);
			//ctrl->SetSpeed(0.05, 0.25);
			ctrl->SetThumbPos(GScene->GetFrameRateSettings());
			return ctrl;
		}
		case IDC_OPTIONS_QUALITY_SLIDER:
		{
			Assert(type == CT_3DSLIDER);
			C3DSlider *ctrl = new C3DSlider(this, idc, cls);
			//ctrl->SetRange(-log(1.0), -log(0.005));
			ctrl->SetRange(-log(0.05), -log(0.005));
			float value = GScene->GetQualitySettings();
			ctrl->SetThumbPos(-log(value));
			return ctrl;
		}
		case IDC_OPTIONS_VISIBILITY_SLIDER:
		{
			Assert(type == CT_3DSLIDER);
			C3DSlider *ctrl = new C3DSlider(this, idc, cls);
			ctrl->SetRange(log(500), log(5000));
			float range = ctrl->GetRange();
			ctrl->SetSpeed(0.01 * range, 0.1 * range);
			float value = GScene->GetPreferredViewDistance();
			ctrl->SetThumbPos(log(value));
			return ctrl;
		}
		case IDC_OPTIONS_TERRAIN:
		{
			Assert(type == CT_3DLISTBOX);
			C3DListBox *lbox = new C3DListBox(this, idc, cls);

			lbox->ClearStrings();
			int sel = 0, selI = 0;
			float value = GScene->GetPreferredTerrainGrid();
			float minValue = GScene->GetMinimalTerrainGrid();
			float selDiff = 1e10;
			for (int i=0; i<sizeof(terrainInfos)/sizeof(TerrainInterpolationInfo); i++)
			{
				if (terrainInfos[i].value<minValue) continue;
				int index = lbox->AddString(LocalizeString(terrainInfos[i].ids));
				lbox->SetValue(index,i);
				float diff = fabs(terrainInfos[i].value-value);
				if (diff<selDiff) selDiff = diff, sel = selI;
				selI++;
			}
			lbox->SetCurSel(sel);
			return lbox;
		}
		default:
			return Display::OnCreateCtrl(type, idc, cls);
	}
#else
    return Display::OnCreateCtrl(type, idc, cls);
#endif
}

void DisplayOptionsVideo::OnSliderPosChanged(int idc, float pos)
{
#ifdef _WIN32
	C3DStatic *ctrl;
	char buffer[128];
	switch (idc)
	{
		case IDC_OPTIONS_GAMMA_SLIDER:
			GEngine->SetGamma(pos);
			ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_OPTIONS_GAMMA_VALUE));
			sprintf(buffer, "%.1f", pos);
			if (ctrl) ctrl->SetText(buffer);
			break;
		case IDC_OPTIONS_BRIGHT_SLIDER:
			GEngine->SetBrightness(pos);
			ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_OPTIONS_BRIGHT_VALUE));
			sprintf(buffer, "%.1f", pos);
			if (ctrl) ctrl->SetText(buffer);
			break;
		case IDC_OPTIONS_RATE_SLIDER:
			GScene->SetFrameRateSettings(pos);
			ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_OPTIONS_RATE_VALUE));
			//sprintf(buffer, "%.1f", pos);
			if (ctrl) ctrl->SetText(GScene->GetFrameRateText());
			break;
		case IDC_OPTIONS_QUALITY_SLIDER:
			GScene->SetQualitySettings(exp(-pos));
			ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_OPTIONS_QUALITY_VALUE));
			//sprintf(buffer, "%.1f", pos);
			if (ctrl) ctrl->SetText(GScene->GetQualityText());
			break;
		case IDC_OPTIONS_VISIBILITY_SLIDER:
			GScene->SetPreferredViewDistance(exp(pos));
			ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_OPTIONS_VISIBILITY_VALUE));
			char buffer[256];
			sprintf(buffer, "%.0f", GScene->GetPreferredViewDistance());
			if (ctrl) ctrl->SetText(buffer);
			UpdateTerrainGrids();
			break;
	}
#endif
}

void DisplayOptionsVideo::OnButtonClicked(int idc)
{
#ifdef _WIN32
	switch (idc)
	{
	case IDC_OK:
		{
			ParamFile userCfg;
			userCfg.Parse(GetUserParams());
			userCfg.Add("blood", Glob.config.blood);
			userCfg.Save(GetUserParams());
		}
		Exit(idc);
		break;
	case IDC_CANCEL:
		GEngine->SwitchWindowed(_oldWindowed);
		if (!_oldWindowed) GEngine->SwitchRes(_oldResX, _oldResY, _oldBpp);
		GEngine->SwitchRefreshRate(_oldRefreshRate);
		GEngine->SetBrightness(_oldBright);
		GEngine->SetGamma(_oldGamma);
		GScene->SetFrameRateSettings(_oldFrameRate);
		GScene->SetQualitySettings(_oldQuality);
		SetHWTL(_oldHWTL);
		GScene->SetObjectShadows(_oldObjShadows);
		GScene->SetVehicleShadows(_oldVehShadows);
		GScene->SetCloudlets(_oldSmokes);
		Glob.config.blood = _oldBlood;
		GEngine->SetMultitexturing(_oldMultitexturing);
		if (GEngine->CanWBuffer()) GEngine->SetWBuffer(_oldWBuffer);
		GScene->SetPreferredViewDistance(_oldVisibility);
		GScene->SetPreferredTerrainGrid(_oldTerrain);
		Exit(idc);
		break;
	case IDC_OPTIONS_OBJSHADOWS:
		{
			C3DActiveText *ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_OPTIONS_OBJSHADOWS));
			bool set = !GScene->GetObjectShadows();
			GScene->SetObjectShadows(set);
			ctrl->SetText(LocalizeString(set ? IDS_ENABLED : IDS_DISABLED));
		}
		break;
	case IDC_OPTIONS_VEHSHADOWS:
		{
			C3DActiveText *ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_OPTIONS_VEHSHADOWS));
			bool set = !GScene->GetVehicleShadows();
			GScene->SetVehicleShadows(set);
			ctrl->SetText(LocalizeString(set ? IDS_ENABLED : IDS_DISABLED));
		}
		break;
	case IDC_OPTIONS_CLOUDLETS:
		{
			C3DActiveText *ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_OPTIONS_CLOUDLETS));
			bool set = !GScene->GetCloudlets();
			GScene->SetCloudlets(set);
			ctrl->SetText(LocalizeString(set ? IDS_ENABLED : IDS_DISABLED));
		}
		break;
	case IDC_OPTIONS_BLOOD:
		bool NoBlood();
		if (!NoBlood())
		{
			C3DActiveText *ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_OPTIONS_BLOOD));
			Glob.config.blood = !Glob.config.blood;
			ctrl->SetText(LocalizeString(Glob.config.blood ? IDS_ENABLED : IDS_DISABLED));
		}
		break;
	case IDC_OPTIONS_HWTL:
		{
			// only indication: impossible to toggle
			//C3DActiveText *ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_OPTIONS_HWTL));
			//SetHWTL(!EnableHWTL);

			//ctrl->SetText(LocalizeString(EnableHWTL ? IDS_ENABLED : IDS_DISABLED));

		}
		break;
	case IDC_OPTIONS_MULTITEXTURING:
		{
			C3DActiveText *ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_OPTIONS_MULTITEXTURING));
			bool set = !GEngine->IsMultitexturing();
			GEngine->SetMultitexturing(set);
			ctrl->SetText(LocalizeString(set ? IDS_ENABLED : IDS_DISABLED));
		}
		break;
	case IDC_OPTIONS_WBUFFER:
		if (GEngine->CanWBuffer())
		{
			C3DActiveText *ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_OPTIONS_WBUFFER));
			bool set = !GEngine->IsWBuffer();
			GEngine->SetWBuffer(set);
			ctrl->SetText(LocalizeString(set ? IDS_ENABLED : IDS_DISABLED));
		}
		break;
	default:
		Display::OnButtonClicked(idc);
	}
#endif
}

void DisplayOptionsVideo::OnLBSelChanged(int idc, int curSel)
{
#ifdef _WIN32
	if (curSel < 0) return;

	switch (idc)
	{
	case IDC_OPTIONS_RESOLUTION:
		{
			ResolutionInfo &info = _resolutions[curSel];
			if (info.w == 0 && info.h == 0 && info.bpp == 0)
			{
				GEngine->SwitchWindowed(true);
			}
			else
			{
				GEngine->SwitchWindowed(false);
				GEngine->SwitchRes(info.w, info.h, info.bpp);
			}
			UpdateRefreshRates(curSel);
		}
		break;
	case IDC_OPTIONS_REFRESH:
		{
			C3DListBox *lbox = static_cast<C3DListBox *>(GetCtrl(IDC_OPTIONS_REFRESH));
			if (lbox)
			{
				GEngine->SwitchRefreshRate(lbox->GetValue(curSel));
			}
		}
		break;
	case IDC_OPTIONS_TERRAIN:
		{
			C3DListBox *lbox = static_cast<C3DListBox *>(GetCtrl(IDC_OPTIONS_TERRAIN));
			if (lbox)
			{
				int value = lbox->GetValue(curSel);
				GScene->SetPreferredTerrainGrid(terrainInfos[value].value);
			}
		}
		break;
	}
#endif
}

int DisplayOptionsVideo::InitResolutions()
{
#ifdef _WIN32
	C3DListBox *lbox = static_cast<C3DListBox *>(GetCtrl(IDC_OPTIONS_RESOLUTION));
	if (!lbox) return -1;

	GEngine->ListResolutions(_resolutions);
	int sel = 0;
	for (int i=0; i<_resolutions.Size(); i++)
	{
		ResolutionInfo &info = _resolutions[i];
		char buffer[256];
		sprintf(buffer, LocalizeString(IDS_RESOLUTION_FORMAT), info.w, info.h, info.bpp);
		lbox->AddString(buffer);
		if (info.w == _oldResX && info.h == _oldResY && info.bpp == _oldBpp) sel = i;
	}
/*
	if (GEngine->CanBeWindowed())
	{
		int i = _resolutions.Add();
		ResolutionInfo &info = _resolutions[i];
		info.h = info.w = info.bpp = 0;
		lbox->AddString(LocalizeString(IDS_WINDOWED));
		if (_oldWindowed) sel = i;
	}
*/
	if (_resolutions.Size() == 0) return -1;
	lbox->SetCurSel(sel);
	return sel;
#else
    return -1;
#endif
}

void DisplayOptionsVideo::UpdateRefreshRates(int res)
{
#ifdef _WIN32
	C3DListBox *lbox = static_cast<C3DListBox *>(GetCtrl(IDC_OPTIONS_REFRESH));
	if (!lbox) return;

	int rate;
	int sel = lbox->GetCurSel();
	if (sel >= 0)
		rate = lbox->GetValue(sel);
	else
		rate = _oldRefreshRate;

	lbox->ClearStrings();
	GEngine->ListRefreshRates(_refreshRates);
	if (_refreshRates.Size() == 0) return;
	sel = -1;
	for (int i=0; i<_refreshRates.Size(); i++)
	{
		int value = _refreshRates[i];
		char buffer[256];
		if (value==0)
		{
			sprintf(buffer, LocalizeString(IDS_DISP_DEFAULT), value);
		}
		else
		{
			sprintf(buffer, LocalizeString(IDS_REFRESH_RATE_FORMAT), value);
		}
		int index = lbox->AddString(buffer);
		lbox->SetValue(index, value);
		if (value == rate) sel = i;
	}
	if (sel >= 0)
	{
		lbox->SetCurSel(sel);
	}
	else
	{
		lbox->SetCurSel(0);
		GEngine->SwitchRefreshRate(lbox->GetValue(0));
	}
#endif
}

void DisplayOptionsVideo::UpdateTerrainGrids()
{
#ifdef _WIN32
	C3DListBox *lbox = static_cast<C3DListBox *>(GetCtrl(IDC_OPTIONS_TERRAIN));
	if (!lbox) return;

	lbox->ClearStrings();
	int sel = 0;
	float value = GScene->GetPreferredTerrainGrid();
	float minValue = GScene->GetMinimalTerrainGrid();
	float selDiff = 1e10;
	int selI = 0;
	for (int i=0; i<sizeof(terrainInfos)/sizeof(TerrainInterpolationInfo); i++)
	{
		if (terrainInfos[i].value<minValue) continue;
		int index = lbox->AddString(LocalizeString(terrainInfos[i].ids));
		lbox->SetValue(index,i);
		float diff = fabs(terrainInfos[i].value-value);
		if (diff<selDiff) selDiff = diff, sel = selI;
		selI++;
	}
	LogF("Sel %d of %d",sel,selI);
	lbox->SetCurSel(sel);
#endif
}

// Audio options display
DisplayOptionsAudio::DisplayOptionsAudio(ControlsContainer *parent, bool enableSimulation)
	: Display(parent)
{
	_enableSimulation = enableSimulation;
	Load("RscDisplayOptionsAudio");


	if ( GSoundsys ) {
	    _oldMusic = GSoundsys->GetCDVolume();
	    _oldEffects = GSoundsys->GetWaveVolume();
	    _oldVoices = GSoundsys->GetSpeechVolume();
	    // _oldSamplingRate;
	    _oldHWAcc = GSoundsys->GetHWAccel();
	    _oldEAX = GSoundsys->GetEAX();
	    }
	_oldSingleVoice = Glob.config.singleVoice;

#ifdef _WIN32
	{
		C3DSlider *ctrl = dynamic_cast<C3DSlider *>(GetCtrl(IDC_OPTIONS_MUSIC_SLIDER));
		if (ctrl) OnSliderPosChanged(ctrl->IDC(), ctrl->GetThumbPos());
		ctrl = dynamic_cast<C3DSlider *>(GetCtrl(IDC_OPTIONS_EFFECTS_SLIDER));
		if (ctrl) OnSliderPosChanged(ctrl->IDC(), ctrl->GetThumbPos());
		ctrl = dynamic_cast<C3DSlider *>(GetCtrl(IDC_OPTIONS_VOICES_SLIDER));
		if (ctrl) OnSliderPosChanged(ctrl->IDC(), ctrl->GetThumbPos());
	}
	{
		C3DActiveText *ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_OPTIONS_HWACC));
		if (_oldHWAcc)
			ctrl->SetText(LocalizeString(IDS_ENABLED));
		else
			ctrl->SetText(LocalizeString(IDS_DISABLED));

		ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_OPTIONS_EAX));
		if (_oldEAX)
			ctrl->SetText(LocalizeString(IDS_ENABLED));
		else
			ctrl->SetText(LocalizeString(IDS_DISABLED));

		ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_OPTIONS_SINGLE_VOICE));
		if (_oldSingleVoice)
			ctrl->SetText(LocalizeString(IDS_SINGLE_VOICE));
		else
			ctrl->SetText(LocalizeString(IDS_ALL_VOICES));
#if _FORCE_SINGLE_VOICE
		ctrl->EnableCtrl(false);
#endif
	}
#endif
}

Control *DisplayOptionsAudio::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
#ifdef _WIN32
	switch (idc)
	{
		case IDC_OPTIONS_MUSIC_SLIDER:
		{
			Assert(type == CT_3DSLIDER);
			C3DSlider *ctrl = new C3DSlider(this, idc, cls);
			ctrl->SetThumbPos(GSoundsys->GetCDVolume());
//			ctrl->OnPosChanged(ctrl->GetPos());	
			return ctrl;
		}
		case IDC_OPTIONS_EFFECTS_SLIDER:
		{
			Assert(type == CT_3DSLIDER);
			C3DSlider *ctrl = new C3DSlider(this, idc, cls);
			ctrl->SetThumbPos(GSoundsys->GetWaveVolume());
//			ctrl->OnPosChanged(ctrl->GetPos());	
			return ctrl;
		}
		case IDC_OPTIONS_VOICES_SLIDER:
		{
			Assert(type == CT_3DSLIDER);
			C3DSlider *ctrl = new C3DSlider(this, idc, cls);
			ctrl->SetThumbPos(GSoundsys->GetSpeechVolume());
//			ctrl->OnPosChanged(ctrl->GetPos());	
			return ctrl;
		}
		default:
			return Display::OnCreateCtrl(type, idc, cls);
	}
#else
    return Display::OnCreateCtrl(type, idc, cls);
#endif
}

void DisplayOptionsAudio::OnSliderPosChanged(int idc, float pos)
{
#ifdef _WIN32
	C3DStatic *ctrl;
	char buffer[128];
	switch (idc)
	{
		case IDC_OPTIONS_MUSIC_SLIDER:
			GSoundsys->SetCDVolume(pos);
			GSoundsys->StartPreview();
			ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_OPTIONS_MUSIC_VALUE));
			sprintf(buffer, "%.1f", pos);
			if (ctrl) ctrl->SetText(buffer);
			break;
		case IDC_OPTIONS_EFFECTS_SLIDER:
			GSoundsys->SetWaveVolume(pos);
			GSoundsys->StartPreview();
			ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_OPTIONS_EFFECTS_VALUE));
			sprintf(buffer, "%.1f", pos);
			if (ctrl) ctrl->SetText(buffer);
			break;
		case IDC_OPTIONS_VOICES_SLIDER:
			GSoundsys->SetSpeechVolume(pos);
			GSoundsys->StartPreview();
			ctrl = dynamic_cast<C3DStatic *>(GetCtrl(IDC_OPTIONS_VOICES_VALUE));
			sprintf(buffer, "%.1f", pos);
			if (ctrl) ctrl->SetText(buffer);
			break;
	}
#endif
}

void DisplayOptionsAudio::OnButtonClicked(int idc)
{
#ifdef _WIN32
	switch (idc)
	{
	case IDC_OK:
		{
			GSoundsys->TerminatePreview();
			Exit(idc);
		}
		break;
	case IDC_CANCEL:
		GSoundsys->SetCDVolume(_oldMusic);
		GSoundsys->SetWaveVolume(_oldEffects);
		GSoundsys->SetSpeechVolume(_oldVoices);
		GSoundsys->EnableEAX(_oldEAX);
		GSoundsys->EnableHWAccel(_oldHWAcc);
#if _FORCE_SINGLE_VOICE
		Glob.config.singleVoice = true;
#else
		Glob.config.singleVoice = _oldSingleVoice;
#endif
		GSoundsys->TerminatePreview();
		Exit(idc);
		break;
	case IDC_OPTIONS_HWACC:
		{
			C3DActiveText *ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_OPTIONS_HWACC));
			bool val = GSoundsys->GetHWAccel();
			val = GSoundsys->EnableHWAccel(!val);
			ctrl->SetText(LocalizeString(val ? IDS_ENABLED : IDS_DISABLED));

		}
		break;
	case IDC_OPTIONS_EAX:
		{
			C3DActiveText *ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_OPTIONS_EAX));
			bool val = GSoundsys->GetEAX();
			val = GSoundsys->EnableEAX(!val);
			ctrl->SetText(LocalizeString(val ? IDS_ENABLED : IDS_DISABLED));

		}
		break;
	case IDC_OPTIONS_SINGLE_VOICE:
		{
#if !_FORCE_SINGLE_VOICE
			Glob.config.singleVoice = !Glob.config.singleVoice;
			C3DActiveText *ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_OPTIONS_SINGLE_VOICE));
			if (Glob.config.singleVoice)
				ctrl->SetText(LocalizeString(IDS_SINGLE_VOICE));
			else
				ctrl->SetText(LocalizeString(IDS_ALL_VOICES));
#endif
		}
		break;
	default:
		Display::OnButtonClicked(idc);
	}
#endif
}

// Difficulty display
DisplayDifficulty::DisplayDifficulty(ControlsContainer *parent, bool enableSimulation)
	: Display(parent)
{
	_enableSimulation = enableSimulation;
	_diff = NULL;
	Load("RscDisplayDifficulty");

	_oldTitles = Glob.config.showTitles;
	_oldRadio = GChatList.Enabled();

#ifdef _WIN32
	{
		C3DActiveText *ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_OPTIONS_SUBTITLES));
		if (Glob.config.showTitles)
			ctrl->SetText(LocalizeString(IDS_OPT_SUBTITLES_ENABLED));
		else
			ctrl->SetText(LocalizeString(IDS_OPT_SUBTITLES_DISABLED));

		ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_OPTIONS_RADIO));
		if (GChatList.Enabled())
			ctrl->SetText(LocalizeString(IDS_OPT_RADIO_ENABLED));
		else
			ctrl->SetText(LocalizeString(IDS_OPT_RADIO_DISABLED));
	}
#endif
}

Control *DisplayDifficulty::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
#ifdef _WIN32
	switch (idc)
	{
		case IDC_DIFFICULTIES_DIFFICULTIES:
			_diff = new CDifficulties(this, idc, cls);
			for (int i=0; i<DTN; i++)
			{
				_diff->AddString(LocalizeString(Config::diffDesc[i].desc));
				_diff->cadetDifficulty[i] = Glob.config.cadetDifficulty[i];
				_diff->veteranDifficulty[i] = Glob.config.veteranDifficulty[i];
			}
			_diff->SetCurSel(0);
			return _diff;
		default:
			return Display::OnCreateCtrl(type, idc, cls);
	}
#else
    return Display::OnCreateCtrl(type, idc, cls);
#endif
}


void DisplayDifficulty::OnButtonClicked(int idc)
{
#ifdef _WIN32
	switch (idc)
	{
	case IDC_DIFFICULTIES_DEFAULT:
		for (int i=0; i<DTN; i++)
		{
			_diff->cadetDifficulty[i] = Config::diffDesc[i].defaultCadet;
			_diff->veteranDifficulty[i] = Config::diffDesc[i].defaultVeteran;
		}
		break;
	case IDC_OK:
		{
			for (int i=0; i<DTN; i++)
			{
				Glob.config.cadetDifficulty[i] = _diff->cadetDifficulty[i];
				Glob.config.veteranDifficulty[i] = _diff->veteranDifficulty[i];
			}
			Glob.config.SaveDifficulties();
		}
		Exit(idc);
		break;
	case IDC_CANCEL:
		Glob.config.showTitles = _oldTitles;
		GChatList.Enable(_oldRadio);
		Exit(idc);
		break;
	case IDC_OPTIONS_SUBTITLES:
		{
			Glob.config.showTitles = !Glob.config.showTitles;
			C3DActiveText *ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_OPTIONS_SUBTITLES));
			if (Glob.config.showTitles)
				ctrl->SetText(LocalizeString(IDS_OPT_SUBTITLES_ENABLED));
			else
				ctrl->SetText(LocalizeString(IDS_OPT_SUBTITLES_DISABLED));
		}
		break;
	case IDC_OPTIONS_RADIO:
		{
			GChatList.Enable(!GChatList.Enabled());
			C3DActiveText *ctrl = static_cast<C3DActiveText *>(GetCtrl(IDC_OPTIONS_RADIO));
			if (GChatList.Enabled())
				ctrl->SetText(LocalizeString(IDS_OPT_RADIO_ENABLED));
			else
				ctrl->SetText(LocalizeString(IDS_OPT_RADIO_DISABLED));
		}
		break;
	default:
		Display::OnButtonClicked(idc);
	}
#endif
}

CDifficulties::CDifficulties(ControlsContainer *parent, int idc, const ParamEntry &cls)
: C3DListBox(parent, idc, cls)
{
	_sb3DWidth = 0.05;
	_column = 0;
}

bool CDifficulties::OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
#ifdef _WIN32
	switch (nChar)
	{
	case VK_LEFT:
		_column--;
		if (_column < 0) _column = 2;
		return true;
	case VK_RIGHT:
		_column++;
		if (_column > 2) _column = 0;
		return true;
	case VK_SPACE:
		Toggle(_column, GetCurSel());
		return true;
	default:
		return C3DListBox::OnKeyDown(nChar, nRepCnt, nFlags);
	}
#else
    return false;
#endif
}

void CDifficulties::OnLButtonDown(float x, float y)
{
#ifdef _WIN32
	IsInside(x, y);
    if (_scrollbar.IsEnabled())
	{
		if (_u > 1.0 - _sb3DWidth)
		{
			_scrollbar.SetPos(_topString);
			_scrollbar.OnLButtonDown(_v);
			_topString = _scrollbar.GetPos();
			return;
		}
		else
		{
			_u *= 1.0 / (1.0 - _sb3DWidth);
		}
	}

	float index = _v * _rows;
	if (index >= 0 && index < _rows)
	{
		_parent->OnLBDrag(IDC(), toIntFloor(_topString + index));
		_dragging = true;
	}
	if (_u < 0.6) _column = 0;
	else if (_u < 0.8)
	{
		_column = 1;
		Toggle(1, index);
	}
	else
	{
		_column = 2;
		Toggle(2, index);
	}
#endif
}

inline PackedColor ModAlpha( PackedColor color, float alpha )
{
	int a=toInt(alpha*color.A8());
	saturate(a,0,255);
	return PackedColorRGB(color,a);
}

void CDifficulties::DrawItem
(
	Vector3Par position, Vector3Par down, int i, float alpha
)
{
#ifdef _WIN32
	float y1c = 0;
	float y2c = 1;
	if (i < _topString) y1c = _topString - i;
	if (i > _topString + _rows - 1) y2c = _topString + _rows - i;
	
	Vector3 normal = _down.CrossProduct(_right).Normalized(); 

	Vector3 rightSB = _right;
	if (GetSize() > _rows)
		rightSB = (1.0 - _sb3DWidth) * _right;
	float rightSBSize = rightSB.Size();
	Vector3 border = 0.02 * _right;

	bool selected = i == GetCurSel() && IsEnabled();
	PackedColor color = ModAlpha(GetFtColor(i), alpha);
	PackedColor selColor = color;
	if (selected && _showSelected)
	{
		PackedColor selBgColor = ModAlpha(_selBgColor, alpha);
		switch (_column)
		{
		case 0:
			GEngine->Draw3D
			(
				position, down, 0.6 * rightSB, ClipAll, selBgColor, DisableSun, NULL,
				0, y1c, 1, y2c
			);
			break;
		case 1:
			GEngine->Draw3D
			(
				position + 0.6 * rightSB, down, 0.2 * rightSB, ClipAll, selBgColor, DisableSun, NULL,
				0, y1c, 1, y2c
			);
			break;
		case 2:
			GEngine->Draw3D
			(
				position + 0.8 * rightSB, down, 0.2 * rightSB, ClipAll, selBgColor, DisableSun, NULL,
				0, y1c, 1, y2c
			);
			break;
		}
		selColor = ModAlpha(GetSelColor(i), alpha);
	}

	Vector3 curPos = position - 0.002 * normal;

	Texture *texture = GetTexture(i);
	if (texture)
	{
		Vector3 right = (float)texture->AWidth() / (float)texture->AHeight() * down.Size() * _right.Normalized();
		float rightSize = right.Size();
		float x2c = 1;
		if (rightSize > rightSBSize) x2c = rightSBSize / rightSize;
		GEngine->Draw3D
		(
			curPos, down, right, ClipAll, color, // PackedColor(Color(1, 1, 1, alpha)),
			DisableSun, texture,
			0, y1c, x2c, y2c
		);
		curPos += right;
		rightSBSize -= rightSize;
		if (rightSBSize <= 0) return;
	}

	float top = 0.5 * (1.0 - _size);
	Vector3 pos = curPos + top * down + border;
	Vector3 up = -_size * down;
	Vector3 right = 0.75 * up.Size() * _right.Normalized();
	float invRightSize = 1.0 / right.Size();
	float x2ct = rightSBSize * invRightSize;
	float y1ct = 0;
	float y2ct = 1;
	if (y1c > top)
		y1ct = (y1c - top) / _size;
	if (y2c < top + _size)
		y2ct = (y2c - top) / _size;

	RString text = GetText(i);
	float x2c = 0.6 * x2ct - 2.0 * invRightSize * border.Size();
	PackedColor col = _column == 0 ? selColor: color;
	GEngine->DrawText3D
	(
		pos, up, right, ClipAll, _font, col, DisableSun, text,
		0, y1ct, x2c, y2ct
	);
	pos += 0.6 * rightSB;

	x2c = 0.2 * x2ct - 2.0 * invRightSize * border.Size();
	text = cadetDifficulty[i] ? LocalizeString(IDS_ENABLED) : LocalizeString(IDS_DISABLED);
	col = _column == 1 ? selColor: color;
	GEngine->DrawText3D
	(
		pos, up, right, ClipAll, _font, col, DisableSun, text,
		0, y1ct, x2c, y2ct
	);
	pos += 0.2 * rightSB;

	text = veteranDifficulty[i] ? LocalizeString(IDS_ENABLED) : LocalizeString(IDS_DISABLED);
	if (Glob.config.diffDesc[i].enabledInVeteran)
		col = _column == 2 ? selColor: color;
	else
		col = PackedColor(Color(1, 0, 0, alpha));
	GEngine->DrawText3D
	(
		pos, up, right, ClipAll, _font, col, DisableSun, text,
		0, y1ct, x2c, y2ct
	);
#endif
}
