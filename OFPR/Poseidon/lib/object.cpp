/*!
\file
Implementation for Object class
Implements general and  drawing functions
*/
// (C) 1997, SUMA
#include "wpch.hpp"


#include "object.hpp"
#include "tlVertex.hpp"
#include "scene.hpp"
#include "engine.hpp"
#include "camera.hpp"
#include "landscape.hpp"
#include "lights.hpp"
#include <El/Common/randomGen.hpp>
#include "global.hpp"
#include <El/Common/perfLog.hpp>
#include <El/ParamFile/paramFile.hpp>
#include "paramArchive.hpp"
#include "frameInv.hpp"
#include <El/Common/enumNames.hpp>
#include "diagModes.hpp"
#include <Es/Common/filenames.hpp>

#include "SpecLods.hpp"
#include "network.hpp"
#include "ai.hpp"

#include "stringtableExt.hpp"

#ifdef PREPROCESS_DOCUMENTATION
#include "math3d.hpp"
#endif

#if _RELEASE || !_ENABLE_CHEATS
	#define STARS 0
	#define SHOT_STARS 0
#else
	#define STARS 0
	#define SHOT_STARS 1
#endif

#if _ENABLE_PERFLOG
extern bool LogStatesOnce;
#endif

#define PERF_ISECT 0

static const EnumName SideNames[]=
{
	EnumName(TWest,"WEST"),
	EnumName(TEast,"EAST"),
	EnumName(TGuerrila,"GUER"),
	EnumName(TCivilian,"CIV"),
	EnumName(TSideUnknown,"UNKNOWN"),
	EnumName(TEnemy,"ENEMY"),
	EnumName(TFriendly,"FRIENDLY"),
	EnumName(TLogic,"LOGIC"),
	EnumName(TEmpty,"EMPTY"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(TargetSide dummy)
{
	return SideNames;
}

#include "resincl.hpp"
#include "mapTypes.hpp"

RString Object::GetDisplayName() const
{
	static RString empty;
	if( !_shape ) return empty;
	MapType type=_shape->GetMapType();
	switch( type )
	{
		case MapTree: case MapSmallTree:
			return LocalizeString(IDS_DN_TREE);
		case MapBush:
			return LocalizeString(IDS_DN_BUSH);
		case MapBuilding:
			return LocalizeString(IDS_DN_BUILDING);
		case MapHouse:
			return LocalizeString(IDS_DN_HOUSE);
		case MapForestBorder: case MapForestTriangle: case MapForestSquare:
			return LocalizeString(IDS_DN_FOREST);
		case MapChurch:
			return LocalizeString(IDS_DN_CHURCH);
		case MapChapel:
			return LocalizeString(IDS_DN_CHAPEL);
		case MapCross:
			return LocalizeString(IDS_DN_CROSS);
		case MapRock:
			return LocalizeString(IDS_DN_ROCK);
		case MapBusStop:
			return LocalizeString(IDS_DN_BUS_STOP);
		// ignored maptypes
		//MapFountain,
		//MapFence, MapWall,
		// vehicle should handle some types
		case MapBunker: case MapFortress:
		case MapViewTower: case MapLighthouse: case MapQuay:
		case MapFuelstation: case MapHospital:
			LogF("%s: This should be handled by VehicleType",(const char *)GetDebugName());
		default:
			return empty;
	}
}

RString Object::GetNameSound() const
{
	static RString empty;
	if( !_shape ) return empty;

	MapType type=_shape->GetMapType();
	switch( type )
	{
		case MapTree: case MapSmallTree:
			return "tree";
		case MapBush:
			return "bush";
		case MapBuilding:
			return "building";
		case MapHouse:
			return "house";
		case MapForestBorder: case MapForestTriangle: case MapForestSquare:
			return "forest";
		case MapChurch:
			return "church";
		case MapChapel:
			return "chapel";
		case MapCross:
			return "cross";
		case MapRock:
			return "rock";
		case MapBusStop:
			return "building";
		// ignored maptypes
		//MapFountain,
		//MapFence, MapWall,
		// vehicle should handle some types
		case MapBunker: case MapFortress:
		case MapViewTower: case MapLighthouse: case MapQuay:
		case MapFuelstation: case MapHospital:
			LogF("%s: This should be handled by VehicleType",(const char *)GetDebugName());
		default:
			return empty;
	}
}

bool Object::IsMoveTarget() const
{
	if( !_shape ) return false;
	if( GetDisplayName().GetLength() == 0) return false;
	if( GetNameSound().GetLength() == 0) return false;
	return true;
}

RString Object::GetDebugName() const
{
	char nameS[128];
	if (GetShape())
	{
		GetFilename(nameS,GetShape()->Name());
	}
	else
	{
		strcpy(nameS,"<no shape>");
	}
	char buf[256];
	if( ID()<0 )
	{
		sprintf(buf,"NOID %s",nameS);
	}
	else
	{
		sprintf(buf,"%d: %s",ID(),nameS);
	}
	return buf;
}

bool Object::IsAnimated( int level ) const
{
	Shape *shape=_shape->Level(level);
	if( !shape ) return false;
	if( shape->NFaces()<=0 ) return false;
	if( GetTotalDammage()>0 ) return true;
	if( _isDestroyed && _destroyPhase>0 )
	{
		if (GetDestructType()!=DestructTree) return true;
	}
	if( (shape->GetOrHints()&(ClipLandKeep|ClipLandOn)) && GLOB_LAND ) return true;
	return false;
}

bool Object::IsAnimatedShadow( int level ) const
{
	if( _isDestroyed && _destroyPhase>0 )
	{
		if (GetDestructType()!=DestructTree) return true;
	}
	return false;
}

/*!
*\param level lod level
*\param index vertex index
*\return Animated point world coordinates.
*/

Vector3 Object::AnimatePoint( int level, int index ) const
{
	Shape *shape = _shape->LevelOpaque(level);

	// return animated point world coordinates

	if( (shape->GetOrHints()&(ClipLandKeep|ClipLandOn)) && GLOB_LAND )
	{
		shape->SaveOriginalPos();

		Matrix4Val toWorld=Transform();
		ClipFlags clip=shape->OrigClip(index);
		Vector3Val pos=shape->OrigPos(index);
		Vector3 tPos(VFastTransform,toWorld,pos);
		if( clip&ClipLandKeep )
		{
			// shape y is relative to surface
			// calculate world coordinates
			float yPos=pos[1]+_shape->BoundingCenter().Y();
			tPos[1]=GLOB_LAND->SurfaceY(tPos[0],tPos[2])+yPos;
		}
		else if( clip&ClipLandOn )
		{
			// clamp y to surface
			tPos[1]=GLOB_LAND->SurfaceY(tPos[0],tPos[2]);
		}
		return tPos;
	}
	else
	{
		Vector3Val modelPos = shape->Pos(index);
		return PositionModelToWorld(modelPos);
	}
}

/*!
*\param mat matrix to be animated
*\param level lod level
*\param selection index of named selection this matrix is related to
*/

void Object::AnimateMatrix( Matrix4 &mat, int level, int selection ) const
{
}

// access to world space transformation
Matrix4 Object::WorldTransform() const
{
	// normal objects are not in any hierearchy
	// thay have world-space transform same as in-hierarchy
	return Transform();
}

Vector3 Object::WorldSpeed() const
{
	return ObjectSpeed();
}


Matrix4 Object::ProxyWorldTransform(const Object *obj) const
{
	return Transform()*obj->Transform();
}

Matrix4 Object::ProxyInvWorldTransform(const Object *obj) const
{
	return obj->CalcInvTransform()*CalcInvTransform();
}


Matrix4 Object::WorldInvTransform() const
{
	return CalcInvTransform();
}

float Object::CloudletClippingCoef() const
{
	return 1;
}

/*!
\patch 1.53 Date 4/27/2002 by Ondra
- Fixed: Geometry used for walking is now also deformed
during object destruction deformation.
*/

void Object::Animate( int level )
{
	AssertDebug( _animatedCount++<=2 );
	Shape *shape=_shape->Level(level);
	if( !shape ) return;
	if( _isDestroyed && _destroyPhase>0 && GetDestructType()!=DestructTree)
	{
		shape->SaveOriginalPos();
		// animate destruction
		LODShape *destShape=NULL;
		switch( (DestructType)_destrType )
		{
			case DestructTent:
				destShape=_shape->MakeTreeDestroyed();
		  break;
			default:
				destShape=_shape->MakeDestroyed();
			break;
		}
		Shape *destLevel=destShape->Level(level);
		Assert( destLevel );
		Assert( destLevel->NPos()==shape->NPos() );
		float ratio=GetDestroyed();
		if( ratio>0.99 ) 
		{
			// TODO: copy whole array at once
			for( int i=0; i<shape->NPos(); i++ )
			{
				shape->SetPos(i)=destLevel->Pos(i);
			}
		}
		else
		{
			for( int i=0; i<shape->NPos(); i++ )
			{
				V3 &pos=shape->SetPos(i);
				const V3 &dPos=destLevel->Pos(i);
				pos=dPos*ratio+pos*(1-ratio);
			}
		}
		shape->InvalidateBuffer();
		shape->InvalidateNormals();

		// set minmax box and sphere
		Vector3 min = shape->MinOrig();
		Vector3 max = shape->MaxOrig();
		Vector3 bCenter = shape->BSphereCenterOrig();
		float bRadius = shape->BSphereRadiusOrig();
		// some space on borders required
		const float sFactor = 1.1;
		min = bCenter+(min-bCenter)*sFactor;
		max = bCenter+(max-bCenter)*sFactor;
		bRadius *= sFactor;
		shape->SetMinMax(min,max,bCenter,bRadius);

	}
	// check if object needs surface animation
	if ((shape->GetAndHints()&ClipLandMask)==ClipLandOn)
	{
		// no animation required: will be done during SurfaceSplit
	}
	else if( (shape->GetOrHints()&(ClipLandKeep|ClipLandOn)) && GLOB_LAND )
	{
		// save original position
		shape->SaveOriginalPos();
		Matrix4Val toWorld=Transform();
		Matrix4Val fromWorld=GetInvTransform();
		// change object shape to reflect surface

		float yOffset = 0;
		Vector3 bCenter=VZero;
		if (shape->GetOrHints()&ClipLandKeep)
		{
			// world space bounding center position
			bCenter.SetFastTransform(toWorld,-_shape->BoundingCenter());

			float bcSurfaceY = GLandscape->SurfaceY(bCenter[0],bCenter[2]);
			yOffset = bCenter.Y()-bcSurfaceY; //+_shape->BoundingCenter().Y();
			//LogF("%s: yOffset %g",(const char *)GetDebugName(),yOffset);
		}
		for( int i=0; i<shape->NPos(); i++ )
		{
			ClipFlags clip=shape->OrigClip(i);
			if( clip&ClipLandKeep )
			{
				//Vector3Val pos=shape->OrigPos(i);
				Vector3Val pos=shape->Pos(i);
				// shape y is relative to surface
				// calculate world coordinates
				//float yPos = pos[1]+yOffset;

				Vector3 tPos(VFastTransform,toWorld,pos);
				// calculate transformed pos above world space bCenter
				float yAbove = tPos.Y()-bCenter.Y()+yOffset;

				V3 &dPos=shape->SetPos(i);

				//tPos[1]=GLandscape->SurfaceY(tPos[0],tPos[2])+yPos;
				tPos[1]=GLandscape->SurfaceY(tPos[0],tPos[2])+yAbove;

				dPos.SetFastTransform(fromWorld,tPos);
				clip &= ~ClipLandKeep;
				shape->SetClip(i,clip);
			}
			else if( clip&ClipLandOn )
			{
				Vector3Val pos=shape->OrigPos(i);
				// shape y is relative to surface
				// calculate world coordinates
				Vector3 tPos(VFastTransform,toWorld,pos);
				tPos[1]=GLOB_LAND->SurfaceY(tPos[0],tPos[2]);
				V3 &dPos=shape->SetPos(i);
				dPos.SetFastTransform(fromWorld,tPos);
				//clip&=~ClipLandOn;
				shape->SetClip(i,clip);
			}
		}
		shape->InvalidateNormals();
		shape->InvalidateBuffer();
	}
}

void Object::AnimatedMinMax( int level, Vector3 *minMax )
{
	// default implementation - slow, but robust
	Animate(level);

	Shape *shape = GetShape()->Level(level);
	if( _isDestroyed && _destroyPhase>0 && GetDestructType()!=DestructTree)
	{
		// set minmax box and sphere
		Vector3Val min = shape->MinOrig();
		Vector3Val max = shape->MaxOrig();
		// some space on borders required
		Vector3Val cnt = (min+max)*0.5f;
		const float sFactor = 1.1;
		minMax[0] = cnt+(min-cnt)*sFactor;
		minMax[1] = cnt+(max-cnt)*sFactor;
	}
	else
	{
		shape->MinMaxDynamic(minMax);
	}
	Deanimate(level);
}

void Object::AnimatedBSphere( int level, Vector3 &bCenter, float &bRadius, bool isAnimated )
{
	// isAnimated should be set
	// when function is called inside Animate/Deanimate block
	// default implementation - slow, but robust
	if (!isAnimated) Animate(level);
	Shape *shape = GetShape()->Level(level);
	shape->BSphereDynamic(bCenter,bRadius);
	if (!isAnimated) Deanimate(level);
}

void Object::Deanimate( int level )
{
	AssertDebug( --_animatedCount>=0 );
	Shape *shape=_shape->Level(level);
	if( !shape ) return;
	if( _isDestroyed && _destroyPhase>0 && GetDestructType()!=DestructTree)
	{
		shape->InvalidateBuffer();
		// normals may be changed after Animate and may be different from original state
		shape->InvalidateNormals();
		shape->RestoreOriginalPos();
		shape->RestoreMinMax();
	}
	if ((shape->GetAndHints()&ClipLandMask)==ClipLandOn)
	{
		// no animation required: will be done during SurfaceSplit
	}
	else if ((shape->GetOrHints()&(ClipLandKeep|ClipLandOn)) && GLOB_LAND)
	{
		// restore saved position
		shape->RestoreOriginalPos();
		shape->InvalidateNormals();
		shape->InvalidateBuffer();
		shape->RestoreMinMax();
	}

	shape->RestoreMinMax();
}

bool Object::Invisible() const
{
	return false;
}
bool Object::OcclusionFire() const
{
	return true;
}
bool Object::OcclusionView() const
{
	// note: if object is animated as destructed, view geometry becomes inaccurate
	return true; //_destroyPhase==0;
}

/*!
	Default implementation uses LODShape::ViewDensity()
*/
float Object::ViewDensity() const
{
	LODShape *shape = GetShape();
	return shape ? shape->ViewDensity() : -100;	
}

/*!
*	\note
	This function is currently called only for vehicles
	created in function CreateVehicle in aiCenter.cpp
*/
void Object::Init( Matrix4Par pos )
{
}


/*!\note If the animation is peformed, components are invalidated.*/

void Object::AnimateComponentLevel(int level)
{
	bool change=IsAnimated(level);
	Animate(level);
	if( change ) _shape->InvalidateConvexComponents(level);
}

void Object::DeanimateComponentLevel(int level)
{
	Deanimate(level);
}

void Object::AnimateGeometry()
{
	int level=_shape->FindGeometryLevel();
	if( level>=0 )
	{
		bool change=IsAnimated(level);
		Animate(level);
		if( change ) _shape->InvalidateGeomComponents();
	}
}

void Object::DeanimateGeometry()
{
	int level=_shape->FindGeometryLevel();
	if( level>=0 )
	{
		Deanimate(level);
	}
}

void Object::AnimateViewGeometry()
{
	int level=_shape->FindViewGeometryLevel();
	if( level>=0 )
	{
		bool change=IsAnimated(level);
		Animate(level);
		if( change ) _shape->InvalidateViewComponents();
	}
}

void Object::DeanimateViewGeometry()
{
	int level=_shape->FindViewGeometryLevel();
	if( level>=0 )
	{
		Deanimate(level);
	}
}

void Object::AnimateLandContact()
{
	int level=_shape->FindLandContactLevel();
	if( level>=0 ) Animate(level);
}

void Object::DeanimateLandContact()
{
	int level=_shape->FindLandContactLevel();
	if( level>=0 ) Deanimate(level);
}

void Object::Move(Matrix4Par transform)
{
/*
void CheckPosition(Object *obj, const Vector3 &newPos);
CheckPosition(this, transform.Position());
*/
	GLOB_LAND->MoveObject(this,transform);
}

//!Change object position
/*!
* \overload void Object::Move(Matrix4Par transform)
*/

void Object::Move(Vector3Par position)
{
	GLOB_LAND->MoveObject(this,position);
}

void Object::MoveNetAware(Matrix4Par transform )
{
	if (IsLocal())
	{
		Move(transform);
	}
	else
	{
		GetNetworkManager().AskForMove(this,transform);
	}
}

/*!
* \overload void Object::MoveNetAware(Matrix4Par transform)
*/

void Object::MoveNetAware(Vector3Par pos)
{
	if (IsLocal())
	{
		Move(pos);
	}
	else
	{
		GetNetworkManager().AskForMove(this,pos);
	}
}

/*!\todo implement corresponding GameStateExt function*/
void Object::SetPlateNumber( RString plate )
{
}


bool NoFogNeeded( float dist2, const Shape *shape, float bRadius )
{
	//return false;
	// determine pesimistic fog treshold
	float minFog=GLOB_SCENE->GetFogMinRange();
	ClipFlags andFog=shape->GetAndHints()&ClipFogMask;
	ClipFlags orFog=shape->GetOrHints()&ClipFogMask;
	if( andFog==ClipFogSky ) minFog=MinSkyFog;
	if( orFog==ClipFogShadow ) minFog=GLOB_SCENE->GetShadowFogMinRange();
	// check object coordinates
	float minDist = floatMax(minFog-bRadius,0);
	return( dist2<Square(minDist) );
}

float ConstFogUsed( float dist2, const Shape *shape, float bRadius )
{
	// check if homogenous fog can be used
	// return -1 if not
	ClipFlags andFog=shape->GetAndHints()&ClipFogMask;
	ClipFlags orFog=shape->GetOrHints()&ClipFogMask;

	float maxFog=GLOB_SCENE->GetFogMaxRange();
	if( orFog==ClipFogSky ) maxFog=MaxSkyFog;
	if( andFog==ClipFogShadow ) maxFog=GLOB_SCENE->GetShadowFogMaxRange();
	// check object coordinates
	if( dist2>Square(maxFog+bRadius) || dist2>Square(maxFog+maxFog) )
	{
		#if _ENABLE_PERFLOG
		if (LogStatesOnce)
		{
			LogF
			(
				"Const fog 1 - dist = %.2f, maxFog = %.2f, bRadius = %.2f",
				sqrt(dist2),maxFog,bRadius
			);
		}
		#endif
		return 1;
	}
	if( andFog!=orFog ) return -1;
	float dist=dist2*InvSqrt(dist2);
	//float minDistance2=Square(floatMax(dist-bRadius,0));
	float minDistance2=Square(dist-bRadius);
	float maxDistance2=Square(dist+bRadius);
	int minValue=0,maxValue=0;
	if( andFog==ClipFogNormal )
	{
		minValue=GScene->Fog8(minDistance2);
		maxValue=GScene->Fog8(maxDistance2);
	}
	else if( andFog==ClipFogSky )
	{
		minValue=GScene->SkyFog8(minDistance2);
		maxValue=GScene->SkyFog8(maxDistance2);
	}
	else if( andFog==ClipFogShadow )
	{
		minValue=GScene->ShadowFog8(minDistance2);
		maxValue=GScene->ShadowFog8(maxDistance2);
	}
	// TODO: statitics
	// note: maxValue should be always >= minValue
	int diff=maxValue-minValue;
	//if( diff>8) return -1;
	if( diff>+8) return -1;
	float ret = (minValue+maxValue)*(0.5/255);
	#if _ENABLE_PERFLOG
	if (LogStatesOnce)
	{
		LogF
		(
			"Const fog %.4f - dist = %.2f, maxFog = %.2f, "
			"bRadius = %.2f, minDist = %.2f, maxDist = %.2f",
			ret,
			sqrt(dist2),maxFog,bRadius,sqrt(minDistance2),sqrt(maxDistance2)
		);
	}
	#endif
	return ret;
}

int Object::PassNum( int lod )
{
	if( !_shape ) return 0;
	Shape *shape=_shape->Level(lod);
	if( !shape ) return 0;
	int spec=shape->Special()|GetObjSpecial();
	// non-alpha objects may always be drawn before water
	//if( !(spec&IsAlpha) ) return 0;
	// cockpits must be drawn after water and everything
	//float resolution = _resolution[lod];
	// TODO: better check, whether level is cockpit
	if( spec&NoDropdown ) return 3; // cockpits

	// surface objects (roads and craters) must be drawn after grass
	if( spec&(OnSurface|IsOnSurface) )
	{
		return GEngine->CanGrass() ? 2 : 0;
	}
	#if !ALPHA_SPLIT
		const int alphaFlags=IsAlpha|IsLight;
		if( (spec&alphaFlags)==0 ) return 1; // no alpha - normal object
		// alpha objects - after water
		// detect object is alpha blended
		int alphaVal=shape->GetColor().A8();
		if( spec&IsColored )
		{
			alphaVal=(alphaVal*GetConstantColor().A8())>>8;
		}
		if( alphaVal<0xd0 )
		{
			/*
			LogF
			(
				"%x,s: alpha %d, cc %d",
				_shape.GetRef(),(const char *)_shape->Name(),alphaVal,GetConstantColor().A8()
			);
			*/
			return 2;
		}
		//if( spec&(IsAlpha|IsTransparent) ) return 2;
	#endif
	return 1;
}

bool Object::CastShadow() const
{
	return IS_SHADOW_OBJECT;
}

bool Object::CastProxyShadow(int level, int index) const
{
	return false;
}

	// virtual access to all proxy objects
int Object::GetProxyCount(int level) const
{
	Shape *sShape=_shape->LevelOpaque(level);
	return sShape->NProxies();
}

Object *Object::GetProxy
(
	LODShapeWithShadow *&shape,
	int level,
	Matrix4 &transform, Matrix4 &invTransform,
	const FrameBase &parent, int i
) const
{
	Shape *sShape=_shape->LevelOpaque(level);

	const ProxyObject &proxy=sShape->Proxy(i);
	
	// TODO: smart clipping
	transform = transform*proxy.obj->Transform();
	invTransform = proxy.invTransform*invTransform;

	// construct FrameWithInverse from transform and invTransform
	shape = proxy.obj->GetShapeOnPos(transform.Position());
	return proxy.obj;
}

void Object::DrawProxies
(
	int level, ClipFlags clipFlags,
	const Matrix4 &transform, const Matrix4 &invTransform,
	float dist2, float z2, const LightList &lights
)
{
	Shape *sShape=_shape->LevelOpaque(level);

	// draw all proxy objects
	for( int i=0; i<sShape->NProxies(); i++ )
	{
		const ProxyObject &proxy=sShape->Proxy(i);
		
		// smart clipping par of obj->Draw
		Matrix4Val pTransform=transform*proxy.obj->Transform();
		Matrix4Val invPTransform=proxy.invTransform*invTransform;

		// LOD detection
		LODShapeWithShadow *pshape = proxy.obj->GetShapeOnPos(pTransform.Position());
		if (!pshape) continue;
		int level=GScene->LevelFromDistance2
		(
			pshape,dist2,pTransform.Scale(),
			pTransform.Direction(),GScene->GetCamera()->Direction()
		);
		if( level==LOD_INVISIBLE ) continue;

		FrameWithInverse pFrame(pTransform,invPTransform);

		// construct FrameWithInverse from transform and invTransform

		proxy.obj->Draw(level,ClipAll,pFrame);
	}
}

/*!
*	\param level LOD level index to draw.
*	\param clipFlags Which clipping planes have to be tested.
*	\param pos Position where the object should be draw.
* \note Parameter pos is used mainly for proper proxy object positioning.

* This is the main function used for object rendering.
*	Default implementation peforms Object::Animate before
*	and Object::Deanimate after rendering,
*	checks for fog optimization (early constant fog evaluation),
*	selects lights that may influence this object,
*	draws all proxies via Object::DrawProxies
*	and draws given LOD level via Shape::Draw.
*/

void Object::Draw( int forceLOD, ClipFlags clipFlags, const FrameBase &pos )
{
	#if _ENABLE_PERFLOG
	if (LogStatesOnce)
	{
		LogF
		(
			"Draw object %s:%d, pass %d",
			_shape->Name(),forceLOD,forceLOD>=0 ? PassNum(forceLOD) : -1
		);
	}
	#endif

	if( forceLOD==LOD_INVISIBLE ) return; // invisible LOD

	Shape *sShape=_shape->LevelOpaque(forceLOD);
	if( sShape->_face.Size()<=0 ) return;

	#if _ENABLE_PERFLOG
	if (LogStatesOnce)
	{
		LogF
		(
			"Draw object %s:%d, pass %d",_shape->Name(),forceLOD,PassNum(forceLOD)
		);
	}
	#endif

	ADD_COUNTER(obj,1);

	// test if reference points to valid object
	// get object position in clipping coordinates
	Animate(forceLOD);

	if (clipFlags)
	{
		// try to clip bounding sphere
		Vector3 bCenter;
		float bRadius;
		AnimatedBSphere(forceLOD,bCenter,bRadius,true);

		// note: this we may be inside of proxy object
		Vector3 bCenterW = pos.PositionModelToWorld(bCenter);
		float bRadiusW = bRadius*Scale();
		const ClipFlags clipKeep = ClipUser0;
		clipFlags &= GScene->GetCamera()->MayBeClipped(bCenterW,bRadiusW,1)|clipKeep;
	}

	Assert( forceLOD>=0 );

	LightList work(true);
	// many object are not fogged at all
	// disable fog calculation for them
	int special=sShape->Special()|GetObjSpecial();
	float dist2=GScene->GetCamera()->Position().Distance2(pos.Position());

	#if _ENABLE_PERFLOG
	if (LogStatesOnce)
	{
		LogF
		(
			"Pos %.1f,%.1f,%.1f, cam  %.1f,%.1f,%.1f, scale %.2f, bSphere %.6f",
			pos.Position().X(),
			pos.Position().Y(),
			pos.Position().Z(),
			GScene->GetCamera()->Position().X(),
			GScene->GetCamera()->Position().Y(),
			GScene->GetCamera()->Position().Z(),
			pos.Scale(),_shape->BoundingSphere()
		);
	}
	#endif

	float constFog=-1;
	bool skip = false;
	if( (special&FogDisabled)==0 )
	{
		float radius = pos.Scale()*_shape->BoundingSphere();
		if( NoFogNeeded(dist2,sShape,radius) ) special|=FogDisabled,constFog=0;
		else
		{
			constFog=ConstFogUsed(dist2,sShape,radius);
			if (constFog>=0.99 && (special&IsAlphaFog))
			{
				// object is fully transparent - do not draw it
				// this commonly happens to clouds, almost never to anything else
				skip = true;
			}
		}
	}
	else constFog=0;

	if (!skip)
	{
		if( special&IsColored ) GScene->SetConstantColor(GetConstantColor());	

		GScene->SetConstantFog(constFog);

		float z2=special&NoDropdown ? 0 : dist2;

		const LightList &lights=GScene->SelectLights(pos,this,forceLOD,work);
		Matrix4Val invTransform=pos.GetInvTransform();

		DrawProxies
		(
			forceLOD,clipFlags,pos.Transform(),invTransform,dist2,z2,lights
		);

		sShape->PrepareTextures(z2,special);
		// perform actual drawing


		// if neccessary, split it
		if ((special&OnSurface) && (sShape->GetAndHints()&ClipLandMask)==ClipLandOn)
		{
			// if all point are LandOn, we can split cache
			Ref<Shape> split = GScene->GetShadowCache().Shadow
			(
				this,VUp,forceLOD,*this,true
			);
			int specAdj = (special&~OnSurface)|IsOnSurface;
			split->Draw
			(
				this,
				lights,
				clipFlags,specAdj,
				pos.Transform(),invTransform
			);
		}
		else
		{
			sShape->Draw
			(
				this,
				lights,
				clipFlags,special,
				pos.Transform(),invTransform
			);
		}


		GScene->SetConstantFog(-1);
	}
	#if _ENABLE_PERFLOG
	else if (LogStatesOnce)
	{
		LogF("Shape skipped (fog), special %x, fog %.3f",special,constFog);
	}
	#endif


	Deanimate(forceLOD);

}

int Object::GetProxyComplexity
(
	int level, const FrameBase &pos, float dist2
) const
{
	int nFaces = 0;

	Shape *sShape=_shape->LevelOpaque(level);

	// calculate default proxies (included in shape)
	for( int i=0; i<sShape->NProxies(); i++ )
	{
		const ProxyObject &proxy=sShape->Proxy(i);
		
		// smart clipping par of obj->Draw
		Matrix4Val pTransform=pos.Transform()*proxy.obj->Transform();

		// LOD detection
		LODShapeWithShadow *pshape = proxy.obj->GetShapeOnPos(pTransform.Position());
		if (!pshape) continue;
		int level=GScene->LevelFromDistance2
		(
			pshape,dist2,pTransform.Scale(),
			pTransform.Direction(),GScene->GetCamera()->Direction()
		);
		if( level==LOD_INVISIBLE ) continue;

		Matrix4Val invPTransform=proxy.invTransform*pos.GetInvTransform();
		FrameWithInverse pFrame(pTransform,invPTransform);

		// construct FrameWithInverse from transform and invTransform

		nFaces += proxy.obj->GetComplexity(level,pFrame);
	}

	return nFaces;
}

int Object::GetComplexity(int level, const FrameBase &pos) const
{
	// preview draw with given level
	// return number of faces
	if( level==LOD_INVISIBLE ) return 0; // invisible LOD
	LODShape *lshape = GetShapeOnPos(pos.Position());
	if (!lshape) return 0;
	Shape *shape = lshape->Level(level);
	if (!shape) return 0;
	int nFaces = shape->NFaces();
	if (shape->NProxies()>0)
	{
		float dist2=GScene->GetCamera()->Position().Distance2(pos.Position());
		nFaces += GetProxyComplexity(level,pos,dist2);
	}
	return nFaces;

}

void Object::DrawDecal( int forceLOD, ClipFlags clipFlags, const FrameBase &pos )
{
	if( forceLOD==LOD_INVISIBLE ) return; // invisible LOD

	#if ALPHA_SPLIT
		bool alpha=true;
		Shape *shape=_shape->LevelAlpha(forceLOD);
		if( !shape ) shape=_shape->LevelOpaque(forceLOD),alpha=false;
	#else
		//bool alpha=false;
		Shape *shape=_shape->LevelOpaque(forceLOD);
	#endif
	Assert( shape->_face.Size()==1 );

	ADD_COUNTER(obj,1);
	
	// many object are not fogged at all
	// disable fog calculation for them
	int special=shape->Special()|GetObjSpecial();

	const Camera &camera=*GScene->GetCamera();

	// following line is faster version
	Vector3 posp=GScene->ScaledInvTransform()*pos.Position();

	// camera plane clip test
	// perform more distant clipping than normal
	float nearest=camera.Near()*10;
	if( posp.Z()<nearest ) return;

	float size=Scale();

	Animate(forceLOD);

	// scale and 1/scale is kept in Frame

	// apply perspective on position and size
	Matrix4Val project=camera.Projection();

	float invW=1/posp[2];
	posp[0]=project(0,2)+project(0,0)*posp[0]*invW;
	posp[1]=project(1,2)+project(1,1)*posp[1]*invW;
	//pos[2]=(project.Position()[2]-size*0.1)*invW;
	float nearPos=floatMax(posp[2]-size,nearest);
	float invNearPos = 1/nearPos;
	posp[2]=project(2,2)+project.Position()[2]*invNearPos;
	float rhw = invNearPos;

	// perspective screen size
	float sizeX=+project(0,0)*size*invW*camera.InvLeft()*0.5;
	float sizeY=-project(1,1)*size*invW*camera.InvTop()*0.5;

	if( sizeX*sizeY>=0.5 )
	{
		// never draw too small objects
		// TODO: mip-map management
		Texture *texture=shape->FaceIndexed(0).GetTexture();
		// calculate single-point single-normal lighting
		// world space normal is opposite to camera direction
		ClipFlags hints=shape->GetOrHints();
		Color colorI;
		int mat = (hints&ClipUserMask)/ClipUserStep;
		if (mat!=MSShining)
		{
			TLMaterial mat;
			mat.ambient = HWhite;
			mat.diffuse = HWhite*0.25;
			mat.emmisive = HBlack;
			mat.forcedDiffuse = HWhite*0.25;
			mat.specFlags = 0;
			LightSun *sun=GScene->MainLight();
			sun->SetMaterial(mat);
			colorI=sun->FullResult(0.5);
			float addLightsFactor=sun->NightEffect();
			if( addLightsFactor>0.01 )
			{
				const LightList &lights=GScene->ActiveLights();
				if( lights.Size()>0 )
				{
					Matrix4 worldToModel(MTranslation,-pos.Position());
					for( int index=0; index<lights.Size(); index++ )
					{
						// TODO: PrepareDecal function
						lights[index]->Prepare(worldToModel);
						lights[index]->SetMaterial(mat);
					}

					// TODO: consider: special lighting modes
					for( int index=0; index<lights.Size(); index++ )
					{
						colorI+=lights[index]->Apply(VZero,VForward);
					}
				}
			}
			ColorVal accom=GEngine->GetAccomodateEye();
			colorI=colorI*accom;
			colorI.SetA(1);
		}
		else
		{
			colorI=GEngine->GetAccomodateEye();
		}
		if( special&IsColored ) colorI=colorI*(ColorVal)GetConstantColor();
		// check if there are not some unsupported lighting flags
		//float fog=GScene->Fog(pos.SquareSize());
		float dist2=Position().Distance2(camera.Position());
		float fog=GScene->Fog8(dist2)*(1.0/255);
		if( special&IsAlphaFog ) colorI.SetA((1-fog)*colorI.A());
		else colorI.SetA(1-(1-fog)*colorI.A());
		PackedColor color(colorI);
		// setup drawing
		MipInfo mip=GEngine->TextBank()->UseMipmap(texture,0,0);
		Assert( mip.IsOK() );
		if( !mip.IsOK() ) return;
		//GEngine->PrepareTriangle(mip,special);
		GEngine->DrawDecal(posp,rhw,sizeX,sizeY,color,mip,special);
		// drawn - clean up
		//GEngine->TextBank()->ReleaseMipmap();
	}
	Deanimate(forceLOD);
}

/*!
\patch 1.63 Date 6/1/2002 by Ondra
- Fixed: Shining faces in optics and ironsight view did not shine.
*/

void Object::Draw2D( LODShape *lShape, int lod, PackedColor cColor )
{
	LightSun *sun=GScene->MainLight();
	TLMaterial mat;
	mat.ambient = HWhite;
	mat.diffuse = HWhite;
	mat.emmisive = HBlack;
	mat.forcedDiffuse = HBlack;
	mat.specFlags = 0;

	sun->SetMaterial(mat);

	Color colorI=sun->FullResult(0.5);
	ColorVal accom=GEngine->GetAccomodateEye();
	colorI=colorI*accom;
	colorI.SetA(1);
	colorI=colorI*(ColorVal)cColor;
	PackedColor color(colorI);

	// TODO: 2D via non-perspective projection 
	float xMin=+1e10,xMax=-1e10;
	float yMin=xMin,yMax=xMax;

	Shape *shape=lShape->Level(lod);
	if( !shape ) return;

	if( shape->NFaces()<=0 ) return;
	// many object are not fogged at all
	//int special=shape->Special()|GetObjSpecial();

	ADD_COUNTER(obj,1);

	// ignore position
	//Animate(lod,alpha!=0);

	// scan all faces for max x,y coordinated
	for( int v=0; v<shape->NPos(); v++ )
	{
		Vector3Val vv=shape->Pos(v);
		saturateMin(xMin,vv.X()),saturateMax(xMax,vv.X());
		saturateMin(yMin,vv.Y()),saturateMax(yMax,vv.Y());
	}

	// leave some out reserve
	float xCoef=Inv(xMax-xMin);
	float yCoef=(3.0/4)*Inv(yMax-yMin);
	float coef=floatMax(xCoef,yCoef)*1.02;
	float xAvg=(xMax+xMin)*0.5;
	float yAvg=(yMax+yMin)*0.5;

	float w=GEngine->Width();
	float h=GEngine->Height();

	// draw all faces
	for( Offset o=shape->BeginFaces(); o<shape->EndFaces(); shape->NextFace(o) )
	{
		const Poly &face=shape->Face(o);
		if (face.Special() & (IsHidden|IsHiddenProxy)) continue;
		if( face.N()!=4 )
		{
			Fail("Non square in 2D object");
			continue; // draw only rectangular faces
		}
		// find TL and BR corner
		// TL is with min x, min y
		// minimize sum x+y
		float minSum=1e10;
		int minI=0;
		for( int i=0; i<face.N(); i++ )
		{
			int vi=face.GetVertex(i);
			Vector3Val vv=shape->Pos(vi);
			float sum=vv.X()+vv.Y();
			if( minSum>sum ) minSum=sum,minI=i;
		}
		// TL is minI, BL is (minI+2)%4
		int iTL=face.GetVertex(minI);
		int iTR=face.GetVertex((minI+3)&3);
		int iBR=face.GetVertex((minI+2)&3);
		int iBL=face.GetVertex((minI+1)&3);
		// find BL 
		Texture *texture=face.GetTexture();
		//Texture *texture=GScene->Preloaded(TextureWhite);
		// setup drawing
		Draw2DPars pars;
		const int clampMask=NoClamp|ClampU|ClampV;
		pars.mip=GEngine->TextBank()->UseMipmap(texture,0,0);
		pars.spec=NoZBuf|IsAlpha|IsAlphaFog|(clampMask&face.Special());
		ClipFlags clip =
		(
			shape->Clip(iTL)&shape->Clip(iTR)&
			shape->Clip(iBL)&shape->Clip(iBR)
		);
		if ((clip&ClipUserMask)==MSShining*ClipUserStep)
		{
			pars.SetColor(PackedWhite);
		}
		else
		{
			pars.SetColor(color);
		}
		// TODO: avoid conversion to logical
		// use physical is Draw2D instead
		if (texture)
		{
			pars.uBL=texture->UToLogical(shape->U(iTL));
			pars.vBL=texture->VToLogical(shape->V(iTL));
			pars.uBR=texture->UToLogical(shape->U(iTR));
			pars.vBR=texture->VToLogical(shape->V(iTR));
			pars.uTL=texture->UToLogical(shape->U(iBL));
			pars.vTL=texture->VToLogical(shape->V(iBL));
			pars.uTR=texture->UToLogical(shape->U(iBR));
			pars.vTR=texture->VToLogical(shape->V(iBR));
		}
		else
		{
			pars.uBL=0;
			pars.vBL=0;
			pars.uBR=0;
			pars.vBR=0;
			pars.uTL=0;
			pars.vTL=0;
			pars.uTR=0;
			pars.vTR=0;
		}
		Rect2DAbs rect;
		// float 
		Vector3Val tlPos=shape->Pos(iTL);
		Vector3Val brPos=shape->Pos(iBR);
		// TODO: remove tr,bl
		//Vector3Val trPos=shape->Pos(iTR);
		//Vector3Val blPos=shape->Pos(iBL);
		rect.x=(tlPos.X()-xAvg)*coef*w;
		rect.y=(yAvg-brPos.Y())*coef*(4.0/3)*h;
		rect.w=(brPos.X()-xAvg)*coef*w-rect.x;
		rect.h=(yAvg-tlPos.Y())*coef*(4.0/3)*h-rect.y;
		rect.x+=w*0.5;
		rect.y+=h*0.5;
		GEngine->Draw2D(pars,rect);
		// drawn - clean up
		//GEngine->TextBank()->ReleaseMipmap();
	}
}

void Object::Draw2D( int lod )
{
	LightSun *sun=GScene->MainLight();
	Color colorI=sun->FullResult(0.5);
	ColorVal accom=GEngine->GetAccomodateEye();
	colorI=colorI*accom;
	colorI.SetA(1);
	int sSpecial=GetSpecial();
	PackedColor cColor = sSpecial&IsColored ? GetConstantColor() : PackedWhite;
	Draw2D(_shape,lod,cColor);
}

/*
bool Object::DrawReflection
(
	int level, ClipFlags clipFlags, const FrameBase &frame,
	const WaterLevel &wLevel, int flags
)
{
	// used to draw sky reflection
	// test if reference points to valid object
	if( !_shape ) return false;
	// get object position in clipping coordinates
	Point3 reflPosition=frame.Position();
	float waterY=wLevel._min.Y();
	float waterMinZ=wLevel._viewMin.Z();
	if( reflPosition[1]+GetRadius()<=waterY ) return false; // can never be reflected
	float aboveWater=reflPosition[1]-waterY;
	reflPosition[1]=waterY-aboveWater; // reflected on level water
	//Point3 pos(VFastTransform,GScene->ScaledInvTransform(),reflPosition);
	//if( (flags&REFL_NO_ZTEST)==0 && waterY>5 )
	if( (flags&REFL_NO_ZTEST)==0 )
	{
		if( frame.Position().Z()<waterMinZ ) return false; // this one cannot be reflected
	}

	// check if reflected object will be in the visible part of scene
	// calculate reflection position on water surface
	Vector3Val camPos=GScene->GetCamera()->Position();
	Point3 reflUpPos=reflPosition+Vector3(0,GetRadius(),0);
	reflUpPos+=GScene->GetCamera()->Direction()*GetRadius();
	float t=(camPos.Y()-reflUpPos.Y())/(waterY-reflUpPos.Y());
	Point3 waterPos=(camPos-reflUpPos)*t+reflUpPos;
	// note: water position can be used for many interesting things
	// TODO: make some waves in reflections based on waterPos
	if( (flags&REFL_NO_ZTEST)==0 && waterY>5 )
	{
		Point3 viewWaterPos(VFastTransform,GScene->ScaledInvTransform(),waterPos);
		//viewWaterPos[2]+=GetRadius();
		// check if viewWaterPos is in view frustum
		if( viewWaterPos.Z()>=GScene->GetCamera()->ClipFar() ) return false;
		// check if waterPos is in water clipping rectangle
		if( viewWaterPos.Z()>=wLevel._viewMax.Z() ) return false;
		//float reserve=
		//if( wLevel._min.X()>waterPos.X() || wLevel._max.X()
	}


	// determine which LOD will be used
	// calculate size of detail that would result in one pixel
	Shape *sShape=_shape->LevelOpaque(level);
	#if ALPHA_SPLIT
	if( !sShape || sShape->NFaces()<=0 ) sShape=_shape->LevelAlpha(level);
	#endif
	if( !sShape || sShape->NFaces()<=0 ) return false;

	ADD_COUNTER(obj,1);

	//pos[1]=water-(pos[1]-water); // reflected on level zero
	// TODO: general reflections on any level
	// detection of reflection chance (portal clipping?)
	if( clipFlags )
	{
		// try to clip bounding sphere
		// if whole bounding sphere is out, object is already skipped
		// landscape bound objects may be enlarged
		float radius=GetRadius();
		if( sShape->GetOrHints()&(ClipLandOn|ClipLandKeep) ) radius*=1.5;
		if( GScene->GetCamera()->IsClipped(reflPosition,radius,0)&clipFlags ) return false;
		clipFlags&=GScene->GetCamera()->MayBeClipped(reflPosition,radius,0);
	}
	
	// calculate point transformation
	// temporarily override object Transform
	// calculate reflected transformation
	Matrix4 reflTransform=frame.Transform();
	//reflTransform(0,1)=-reflTransform(0,1);
	reflTransform(1,0)=-reflTransform(1,0);
	reflTransform(1,1)=-reflTransform(1,1);
	reflTransform(1,2)=-reflTransform(1,2);
	//reflTransform(2,1)=-reflTransform(2,1);
	reflTransform(1,3)=waterY+waterY-reflTransform(1,3);
	
	Matrix4Val pointView=GScene->ScaledInvTransform()*reflTransform;
	
	// calculate normal transformation
	Animate(level);

	LightList work(true);
	TLVertexTable tlTable(this,*sShape,pointView);

	// many reflections do not need any shadow
	int special=sShape->Special()|GetObjSpecial();
	float rDist2=GScene->GetCamera()->Position().Distance2(reflPosition);
	if( NoFogNeeded(rDist2,sShape,GetRadius()) ) special|=FogDisabled;
	// TODO: const fog & dammage
	if( special&IsColored ) GScene->SetConstantColor(GetConstantColor());	
	sShape->PrepareTextures(rDist2,special);
	sShape->_face.Draw
	(
		this,
		tlTable,
		GScene->SelectLights(this,level,work),
		*sShape,clipFlags,special,
		frame.GetInvTransform()
	);


	Deanimate(level);

	return true;
}
*/

void Object::DrawPoints( int level, ClipFlags clipFlags, const FrameBase &frame )
{
	if( level==LOD_INVISIBLE ) return; // invisible LOD
	// test if reference points to valid object
	// get object position in clipping coordinates
	//Point3 pos(VFastTransform,GScene->ScaledInvTransform(),frame.Position());
	if( clipFlags )
	{
		// try to clip bounding sphere
		// if whole bounding sphere is out, object is already skipped
		clipFlags&=GScene->GetCamera()->MayBeClipped(frame.Position(),GetRadius(),1);
	}
	
	// determine which LOD will be used
	Assert( level>=0 );
	Shape *sShape=_shape->LevelOpaque(level);
	if( sShape->NPos()<=0 ) return;
	
	Animate(level);

	Matrix4Val pointView=GScene->ScaledInvTransform()*frame.Transform();

	// draw all points
	TLVertexTable tlTable(this,*sShape,pointView);
	ClipFlags clipAnd=clipFlags;
	clipFlags&=tlTable.CheckClipping(*GScene->GetCamera(),clipFlags,clipAnd);
	if( !clipAnd )
	{
		#if _ENABLE_PERFLOG
		if (LogStatesOnce)
		{
			LogF
			(
				"Draw object points %s:%d, pass %d",_shape->Name(),level,PassNum(level)
			);
		}
		#endif
		LightList noLights;
		GEngine->FlushQueues();
		int spec = sShape->Special();
		GEngine->PrepareMesh(spec);
		int bias = (spec&ZBiasMask)/ZBiasStep;
		// max. bias value is 3
		GEngine->SetBias(bias*5);

		if (spec&IsColored)
		{
			GScene->SetConstantColor(GetConstantColor());
		}

		tlTable.DoLighting(this,MIdentity,noLights,*sShape,spec);
		tlTable.DoPerspective(*GScene->GetCamera(),clipFlags);
		GEngine->BeginMesh(tlTable,sShape->Special());
		MipInfo mip=GEngine->TextBank()->UseMipmap(NULL,0,0);
		if( !mip.IsOK() ) return;
		GEngine->PrepareTriangle(mip,spec);

		GEngine->DrawPoints(0,tlTable.NVertex());
		GEngine->EndMesh(tlTable);
		GEngine->FlushQueues();
	}

	Deanimate(level);
}

void Object::DrawLines( int level, ClipFlags clipFlags, const FrameBase &frame )
{
	if( level==LOD_INVISIBLE ) return; // invisible LOD
	// test if reference points to valid object
	// get object position in clipping coordinates
	//Point3 pos(VFastTransform,GScene->ScaledInvTransform(),frame.Position());
	if( clipFlags )
	{
		// try to clip bounding sphere
		// if whole bounding sphere is out, object is already skipped
		clipFlags&=GScene->GetCamera()->MayBeClipped(frame.Position(),GetRadius(),1);
	}
	
	// determine which LOD will be used
	Assert( level>=0 );
	//bool alpha=false;
	Shape *sShape=_shape->LevelOpaque(level);
	if( sShape->NPos()<=0 ) return;
	
	//if( sShape->NPos()!=2 ) return; // TODO: general line drawing

	Animate(level);

	Matrix4Val pointView=GScene->ScaledInvTransform()*frame.Transform();

	//LightSun *sun=GScene->MainLight();
	// perform lighting
	// first of all apply directional light to all normals
	// this can be done at TLVertexTable level
	//
	//Matrix4Val invScale=GScene->GetCamera()->InvScaleMatrix();

	// draw all points
	TLVertexTable tlTable(this,*sShape,pointView);
	ClipFlags clipAnd=clipFlags;
	clipFlags&=tlTable.CheckClipping(*GScene->GetCamera(),clipFlags,clipAnd);
	if( !clipAnd )
	{
		GScene->SetConstantColor(GetConstantColor());

		LightList noLights;
		tlTable.DoLighting(this,MIdentity,noLights,*sShape,sShape->Special());

		// clip
		FaceArray clippedFaces(0,false);
		clippedFaces.Clip(sShape->_face,tlTable,*GScene->GetCamera(),clipFlags,false);

		if( clippedFaces.Begin()<clippedFaces.End() )
		{
			GEngine->PrepareMesh(sShape->Special());
			GEngine->SetBias(0);
			tlTable.DoPerspective(*GScene->GetCamera(),clipFlags);

			// draw first edge of all faces
			GEngine->BeginMesh(tlTable,sShape->Special());
			for( Offset o=clippedFaces.Begin(); o<clippedFaces.End(); clippedFaces.Next(o) )
			{
				const Poly &face=clippedFaces[o];
				Assert( face.N()>=2 );
				GEngine->DrawLine(face.GetVertex(0),face.GetVertex(1));
			}

			GEngine->EndMesh(tlTable);
		}
	}

	Deanimate(level);
}

ObjectShapeType::ObjectShapeType(LODShapeWithShadow *shape)
:_shape(shape),
//_destrType(DestructBuilding),
_loadRef(1)
{
	
}

ObjectShapeType::ObjectShapeType(RStringB shapeName)
:_shapeName(shapeName),
//_destrType(DestructBuilding),
_loadRef(0)
{
	
}

void ObjectShapeType::AddLoadRef()
{
	if (_loadRef++==0)
	{
		// load shape
		_shape = Shapes.New(_shapeName,false,true);
	}
}

void ObjectShapeType::ReleaseLoadRef()
{
	if (--_loadRef==0)
	{
		_shape.Free();
	}
}


DEFINE_CASTING(Object)

Object::Object( LODShapeWithShadow *shape, int id )
:_shape(shape),_static(true),_id(id),
_destrType(DestructBuilding),
#if !_RELEASE
	_animatedCount(0),
#endif

//_hasProxies(false),
_inList(false),

_destroyPhase(0),
_canSmoke(true),_isDestroyed(false)
{
	#if 0 // !_RELEASE
		const char *name="";
		if( _shape && _shape->Name() ) name=_shape->Name();
		Log("New Object id=%d %s",id,name);
	#endif
	// _type default to Primary type
	if( _shape && *_shape->_name ) _type=Primary;
	else _type=Temporary;

	// no destruction for forests
	if( (ObjectType)_type!=Primary ) _destrType=DestructNo;
	else if( _shape )
	{
		const char *dammageName=_shape->GetPropertyDammage();
		if( !strcmpi(dammageName,"No") ) _destrType=DestructNo;
		else if( !strcmpi(dammageName,"engine") ) _destrType=DestructEngine;
		else if( !strcmpi(dammageName,"building") ) _destrType=DestructBuilding;
		else if( !strcmpi(dammageName,"tree") ) _destrType=DestructTree;
		else if( !strcmpi(dammageName,"tent") ) _destrType=DestructTent;
		else
		{
			// use autodetection
			if( GetMass()<10000 ) _destrType=DestructTree;
		}
	}
	// check if there are some proxies
	/*
	if( _shape )
	{
		// scan all LODs
		for( int l=0; l<_shape->NLevels(); l++ )
		{
			Shape *shape=_shape->LevelOpaque(l);
			if( shape->_proxy.Size()>0 ) _hasProxies=true;
		}
	}
	*/
}

Object::~Object()
{
	/*
	// check if we have some shadow cached
	if (_shadow)
	{
		// remove all shadows from shadow cache
		GScene->GetShadowCache().
	}
	*/
}

Object *Object::LoadRef(ParamArchive &ar)
{
	int id;
	if (ar.Serialize("id", id, 1) != LSOK) return NULL;
	return GLandscape->FindObject(id);
}

LSError Object::SaveRef(ParamArchive &ar)
{
	CHECK(ar.Serialize("id", _id, 1))
	return LSOK;
}

bool Object::IgnoreObstacle(Object *obstacle, ObjIntersect type) const
{
	return false;
}

float Object::CollisionSize() const
{
	// TODO: consider convex components in collisions
	// collision size determined from geometry
	// considered horizontal min-max and bounding sphere radius
	Shape *shape=_shape->GeometryLevel();
	if( !shape ) return 0; // no collision possible
	Vector3Val min=shape->Min();
	Vector3Val max=shape->Max();
	return (max-min).SizeXZ()*0.5;
	//float radius=_shape->GeometrySphere();
	//if( Square(radius*2)<xzSize2 ) return radius;
	//return sqrt(xzSize2)*0.5;
}

PackedColor Object::GetConstantColor() const
{
	static const PackedColor halfOpaqueWhite(Color(1,1,1,0.5));
#if _ENABLE_CHEATS
	if (CHECK_DIAG(DETransparent)) return halfOpaqueWhite;
#endif
	return PackedWhite;
}

// IAnimator interface implementation
void Object::GetMaterial(TLMaterial &mat, int index) const
{
	ColorVal accom=GEngine->GetAccomodateEye();
	// distrubute by predefined materials
	CreateMaterial(mat,accom,index);
}

void Object::DoTransform
(
	TLVertexTable &dst,
	const Shape &src, const Matrix4 &posView,
	int from, int to
) const
{
	dst.DoTransformPoints(src,posView,from,to);
}

void Object::DoLight
(
	TLVertexTable &dst,
	const Shape &src, const Matrix4 &worldToModel, const LightList &lights,
	int spec, int material, int from, int to
) const
{
	TLMaterial mat;
	GetMaterial(mat,material);
	mat.specFlags = spec;

	#if USE_QUADS
	if (dst.UsingQuads())
	{
		dst.DoMaterialLightingQ(mat,worldToModel,lights,src,from,to);
	}
	else
	#endif
	{
		dst.DoMaterialLightingP(mat,worldToModel,lights,src,from,to);
	}
	/*
	dst.DoMaterialLightingP
	(
		mat,worldToModel,lights,spec,from,to
	);
	*/
}

bool Object::GetAnimated(const Shape &src) const
{
	if( _isDestroyed && _destroyPhase>0 && GetDestructType()!=DestructTree)
	{
		return true;
	}
	return false;
}

#if SUPPORT_RANDOM_SHAPES
LODShapeWithShadow *Object::GetShapeOnPos(Vector3Val pos) const
{
	return GetShape();
}
#endif

float Object::VisibleSize() const
{
	return GetShape()->GeometrySphere()*Scale();
}

float Object::VisibleSizeRequired() const
{
	return VisibleSize();
}


Vector3 Object::VisiblePosition() const {return PositionModelToWorld(GetShape()->GeometryCenter());}

/*!
* \return Result is world space position to aim to.
*/
Vector3 Object::AimingPosition() const
{
	LODShape *lShape = GetShape();
	if (!lShape)
	{
		ErrF("No shape object %s tested for aiming position", (const char *)GetDebugName());
		return Position();
	}
	return PositionModelToWorld(GetShape()->AimingCenter());
}
/*!
* \return Result is world space position to aim to.
*/
Vector3 Object::CameraPosition() const
{
	return AimingPosition();
}

void Object::AttachWave(AbstractWave *wave, float freq)
{
	// lip-sync or wave source attach
}

float Object::GetSpeaking() const
{
	return 0;
}

bool Object::IsPassable() const
{
	// soldier and other vehicles may pass through
	return GetMass()<10;
}

FrameBase::FrameBase()
:_scale(1)
{
}

void FrameBase::SetPosition( Vector3Par pos )
{
	Frame::SetPosition(pos);
}

void FrameBase::SetTransform( const Matrix4 &transform )
{
	Frame::SetTransform(transform);
	_scale=transform.Scale();
}
void FrameBase::SetOrient( const Matrix3 &dir )
{
	Frame::SetOrientation(dir);
	_scale=dir.Scale();
}

void FrameBase::SetOrient( Vector3Par dir, Vector3Par up )
{
	Frame::SetDirectionAndUp(dir,up);
	_scale=1;
}
void FrameBase::SetOrientScaleOnly( float scale )
{
	SetOrientation(Matrix3(MScale,scale));
	_scale=scale;
}

Matrix4 FrameBase::GetInvTransform() const
{
	return Frame::InverseScaled();
	//return _transform.InverseGeneral();
}

inline Vector3 MinMaxCorner
(
	const Vector3 *minMax, int x, int y, int z
)
{
	return Vector3(minMax[x][0],minMax[y][1],minMax[z][2]);
}

// check if two shapes are in collision state

#include "objLine.hpp"

//#include "vehicleAI.hpp"
//#include "ai.hpp"

Ref<Object> DrawDiagLine(Vector3Par from, Vector3Par to, PackedColor color)
{
	Ref<LODShapeWithShadow> shape=ObjectLine::CreateShape();
	Ref<Object> lineObj=new ObjectLineDiag(shape);
	lineObj->SetConstantColor(color);

	lineObj->SetPosition(from);
	ObjectLine::SetPos(shape,VZero,to-from);
	return lineObj;
}

static void DiagBBox
(
	const Vector3 *minMax, const Frame &pos, PackedColor color
)
{
	LODShapeWithShadow *shape = GScene->Preloaded(SphereModel);

	for( int lr=0; lr<2; lr++ )
	for( int ud=0; ud<2; ud++ )
	for( int fb=0; fb<2; fb++ )
	{
		Vector3 wCorner=MinMaxCorner(minMax,lr,ud,fb);
		Ref<Object> obj=new ObjectColored(shape,-1);
		obj->SetPosition(pos.PositionModelToWorld(wCorner));
		obj->SetScale( 0.1 );
		obj->SetConstantColor(color);
		GLandscape->ShowObject(obj);
	}

	// draw 12 lines
	static const int lines[12][2][3]=
	{
		{{0,0,0},{0,0,1}}, // diff in Z
		{{0,1,0},{0,1,1}},
		{{1,0,0},{1,0,1}},
		{{1,1,0},{1,1,1}},

		{{0,0,0},{0,1,0}}, // diff in Y
		{{0,0,1},{0,1,1}},
		{{1,0,0},{1,1,0}},
		{{1,0,1},{1,1,1}},

		{{0,0,0},{1,0,0}}, // diff in X
		{{0,0,1},{1,0,1}},
		{{0,1,0},{1,1,0}},
		{{0,1,1},{1,1,1}},
	};
	for (int l=0; l<12; l++)
	{
		// get from
		Vector3 from = MinMaxCorner(minMax,lines[l][0][0],lines[l][0][1],lines[l][0][2]);
		Vector3 to   = MinMaxCorner(minMax,lines[l][1][0],lines[l][1][1],lines[l][1][2]);
		Ref<Object> obj = DrawDiagLine
		(
			pos.PositionModelToWorld(from),pos.PositionModelToWorld(to),color
		);
		GLandscape->ShowObject(obj);
	}

}


static bool IntersectBBox
(
	Vector3 *isect,
	const Vector3 tMinMax[2], const Vector3 wMinMax[2],
	const Matrix4 &withToThis
)
{
	#if 1
	// calculate bbox of transformed bbox
	Vector3 wtMin(+1e10,+1e10,+1e10),wtMax(-1e10,-1e10,-1e10); // in this coordinates

	Vector3 wCorners[2][2][2];
	for( int lr=0; lr<2; lr++ )
	for( int ud=0; ud<2; ud++ )
	for( int fb=0; fb<2; fb++ )
	{
		Vector3 wCorner=MinMaxCorner(wMinMax,lr,ud,fb);
		Vector3 wtCorner;
		wtCorner.SetFastTransform(withToThis,wCorner);
		wCorners[lr][ud][fb]=wtCorner;
		CheckMinMax(wtMin,wtMax,wtCorner);
	}


	// clip wtMinMax against tMinMax

	if (wtMin[0]>=tMinMax[1][0]) return false;
	if (wtMin[1]>=tMinMax[1][1]) return false;
	if (wtMin[2]>=tMinMax[1][2]) return false;
	if (wtMax[0]<=tMinMax[0][0]) return false;
	if (wtMax[1]<=tMinMax[0][1]) return false;
	if (wtMax[2]<=tMinMax[0][2]) return false;

	if (isect)
	{
		// the caller requires intersection result
		isect[0] = VectorMax(wtMin,tMinMax[0]);
		isect[1] = VectorMin(wtMax,tMinMax[1]);
	}

	#else

	// use planes with normals (1,0,0), (0,1,0) and (0,0,1)
	isect[0] = tMinMax[0];
	isect[1] = tMinMax[1];

	Vector3 wCorners[2][2][2];
	for( int lr=0; lr<2; lr++ )
	for( int ud=0; ud<2; ud++ )
	for( int fb=0; fb<2; fb++ )
	{
		Vector3 wCorner=MinMaxCorner(wMinMax,lr,ud,fb);
		Vector3 wtCorner;
		wtCorner.SetFastTransform(withToThis,wCorner);
		wCorners[lr][ud][fb]=wtCorner;
	}

	for( int c=0; c<3; c++ )
	{
		// boundbox of intersections between two planes and all edges
		// search for maximal point above minimum
		Vector3 cMin(+1e10,+1e10,+1e10);
		Vector3 cMax(-1e10,-1e10,-1e10);
		for( int diff=0; diff<3; diff++ )
		for( int same1=0; same1<2; same1++ )
		for( int same2=0; same2<2; same2++ )
		{
			int x1,y1,z1;
			int x2,y2,z2;
			if( diff==0 )      x1=0,y1=same1,z1=same2,x2=1,y2=same1,z2=same2;
			else if( diff==1 ) x1=same1,y1=0,z1=same2,x2=same1,y2=1,z2=same2;
			else x1=same1,y1=same2,z1=0,x2=same1,y2=same2,z2=1; // diff==2
			// calculate intersection between a,b and all six planes of cThis

			Vector3Val rA=wCorners[x1][y1][z1];
			Vector3Val rB=wCorners[x2][y2][z2];

			// t=(nA-nP)/(nA-nB)
			float aC=rA[c],bC=rB[c];
			// min plane - inX<0 => x is out
			float inMinA=aC-tMinMax[0][c];
			float inMinB=bC-tMinMax[0][c];
			bool isInMinA=( inMinA>0 );
			bool isInMinB=( inMinB>0 );
			// max plane - inX>0 => x is out
			float inMaxA=aC-tMinMax[1][c];
			float inMaxB=bC-tMinMax[1][c];
			bool isInMaxA=( inMaxA<0 );
			bool isInMaxB=( inMaxB<0 );
			if( isInMinA!=isInMinB )
			{
				float t=inMinA/(aC-bC);
				Assert( t>=-1e5 && t<=1+1e5 );
				Point3 iSect=(rB-rA)*t+rA;
				CheckMinMax(cMin,cMax,iSect);
			}
			if( isInMaxA!=isInMaxB )
			{
				float t=inMaxA/(aC-bC);
				Assert( t>=-1e5 && t<=1+1e5 );
				Point3 iSect=(rB-rA)*t+rA;
				CheckMinMax(cMin,cMax,iSect);
			}
			if( isInMinA && isInMaxA ) CheckMinMax(cMin,cMax,rA);
			if( isInMinB && isInMaxB ) CheckMinMax(cMin,cMax,rB);
		}
		// limit min-max box of this to the newly acquired region
		SaturateMax(isect[0],cMin);
		SaturateMin(isect[1],cMax);

		if( isect[0][0]>=isect[1][0] ) return false;
		if( isect[0][1]>=isect[1][1] ) return false;
		if( isect[0][2]>=isect[1][2] ) return false;
	}
	#endif
	return true;
}

// need only OcclusionPoly
// TODO: create LogicalPoly class

#include "occlusion.hpp"


static bool CalculateIntersectionsExact
(
	CollisionBuffer &result, const Object *thisObj,
	const ConvexComponent &cThis, const ConvexComponent &cWith,
	Matrix4Val thisToWith, Matrix4Val withToThis,
	Matrix4Val thisToWorld,
	int hierLevel
)
{
	Vector3 direction;
	float under; //, underVolume;
	// calculate sum of center of volume*volume
	//Vector3 cov = VZero;
	//float volume = 0;
	// all calulation done in this space
	// clip all faces of cThis with cWith
	const Shape *sThis = cThis.GetShape();
	sThis->BuildFaceIndexToOffset();

	#if 1
	AUTO_STATIC_ARRAY(Vector3,thisVertexResult,256);
	#endif

	// calculate direction
	Vector3 thisNormal = VZero;
	bool someThisLeft = false;
	OcclusionPoly poly;
	OcclusionPoly resClip;
	for (int i=0; i<cThis.NPlanes(); i++)
	{
		const Poly &face = cThis.GetFace(i);
		// contruct analytical poly
		poly.Clear();
		// face of this in with space
		for (int f=0; f<face.N(); f++)
		{
			Vector3Val pos = sThis->Pos(face.GetVertex(f));
			// verify vertex is in plane
			poly.Add(thisToWith.FastTransform(pos));
		}
		// clip it with all planes of with
		for (int w=0; w<cWith.NPlanes(); w++)
		{
			const Plane &wPlane = cWith.GetPlane(w);
			resClip.Clear();
			if (poly.Clip(resClip,wPlane.Normal(),wPlane.D()))
			{
				// TODO: more efficent handling with temp1, temp2 and pointer
				poly = resClip;
				if (poly.N()<3) break;
			}
		}
		// check if something is rest after clipping
		if (poly.N()>=3)
		{
			// transform poly to this space
			poly.Transform(withToThis);
			// calculare oriented area
			poly.SumCrossProducts(thisNormal);
			// calculate volume and center of volume
			//poly.SumXYVolume(volume,cov);
			someThisLeft = true;

			for (int v=0; v<poly.N(); v++)
			{	
				// add only unique vertices
				Vector3Val pos = poly.Get(v);
				bool alreadyIn = false;
				for (int i=0; i<thisVertexResult.Size(); i++)
				{
					if (thisVertexResult[i].Distance2(pos)<Square(1e-4))
					{
						alreadyIn = true;
						break;
					}
				}
				if (!alreadyIn) thisVertexResult.Add(poly.Get(v));
			}
		}
	}

	if (!someThisLeft)
	{
		// nothing left from this
		// no intersection
		return false;
	}

	direction = thisNormal.Normalized();
	// we may calculate max. d of this
	float maxDThis = -1e10;
	float minDWith = +1e10;
	for (int i=0; i<thisVertexResult.Size(); i++)
	{
		float d = direction*thisVertexResult[i];
		saturateMax(maxDThis,d);
	}

	bool someWithLeft = false;
	// clip all faces of cWith with cThis
	// scan all points of cWith

	#if 0
	LogF
	(
		"ThisPlane %.2f,%.2f,%.2f:%.2f",
		direction[0],direction[1],direction[2],maxDThis
	);
	#endif

	const Shape *sWith = cWith.GetShape();
	sWith->BuildFaceIndexToOffset();

	#if 1
	//OcclusionPoly poly;
	for (int i=0; i<cWith.NPlanes(); i++)
	{
		const Poly &face = cWith.GetFace(i);
		// contsruct analytical poly
		poly.Clear();
		// face of this in with space
		for (int f=0; f<face.N(); f++)
		{
			Vector3Val wPos = sWith->Pos(face.GetVertex(f));
			Vector3Val pos = withToThis.FastTransform(wPos);

			float under = maxDThis - pos*direction;

			if (under>0)
			{
				//LogF("  %d: W under %.2f (pos %.3f,%.3f,%.3f,)",i,under,pos[0],pos[1],pos[2]);

				// add only unique vertices
				bool alreadyIn = false;
				for (int i=0; i<result.Size(); i++)
				{
					if (result[i].pos.Distance2(pos)<Square(1e-4)) alreadyIn = true;
				}

				if (!alreadyIn)
				{
					CollisionInfo &ret=result.Append();
					// calculate average of bounding boxes in this space
					ret.texture = NULL;
					ret.pos = pos;
					ret.dirOut = direction;
					ret.underVolume = 0; // TODO: remove underVolume field
					ret.under = under;
					ret.hierLevel = hierLevel;
					ret.object=const_cast<Object *>(thisObj);
					ret.component = -1; // no check 
					#if STARS
						GLOB_SCENE->DrawCollisionStar(thisToWorld.FastTransform(ret.pos));
					#endif
				}
			}

		}
	}
	#else
	Vector3 withCenter = VZero, withNormal = VZero;
	OcclusionPoly poly;
	OcclusionPoly resClip;
	for (int i=0; i<cWith.NPlanes(); i++)
	{
		const Poly &face = cWith.GetFace(i);
		// contsruct analytical poly
		poly.Clear();
		// face of this in with space
		for (int f=0; f<face.N(); f++)
		{
			Vector3Val pos = sWith->Pos(face.GetVertex(f));
			poly.Add(withToThis.FastTransform(pos));
		}
		// clip it with all planes of with
		for (int w=0; w<cThis.NPlanes(); w++)
		{
			const Plane &wPlane = cThis.GetPlane(w);
			resClip.Clear();
			if (poly.Clip(resClip,wPlane.Normal(),wPlane.D()))
			{
				// TODO: more efficent handling with temp1, temp2 and pointer
				poly = resClip;
				if (poly.N()<3) break;
			}
		}
		// check if something is rest after clipping
		if (poly.N()>=3)
		{
			// calculate volume and center of volume
			//poly.SumXYVolume(volume,cov);
			someWithLeft = true;


			for (int v=0; v<poly.N(); v++)
			{
				Vector3Val pos = poly.Get(v);
				/*
				LogF("w %d: %.2f,%.2f,%.2f",v,pos[0],pos[1],pos[2]);
				float d = direction*pos;
				saturateMin(minDWith,d);
				*/

				float under = maxDThis - pos*direction;

				if (under>0.001)
				{
					//LogF("  %d: w under %.2f (pos %.3f,%.3f,%.3f,)",i,under,pos[0],pos[1],pos[2]);
					// add only unique vertices
					bool alreadyIn = false;
					for (int i=0; i<result.Size(); i++)
					{
						if (result[i].pos.Distance2(pos)<Square(1e-4)) alreadyIn = true;
					}

					if (!alreadyIn)
					{
						CollisionInfo &ret=result.Append();
						// calculate average of bounding boxes in this space
						ret.texture = NULL;
						ret.pos = pos;
						ret.dirOut = direction;
						ret.hierLevel = hierLevel;
						ret.underVolume = 0; // TODO: remove underVolume field
						ret.under = under;
						ret.component = -1;
						ret.object=const_cast<Object *>(thisObj);
						#if STARS
							GLOB_SCENE->DrawCollisionStar(thisToWorld.FastTransform(ret.pos));
						#endif
					}
				}
			}
		}
	}
	#endif
	//LogF("d %.3f..%.3f",maxDThis,minDWith);

	if (!someWithLeft)
	{
		// this is fully contained in with
		// special case
		// actual volume calculation would be exact here
		// we provide "large" values instead
		direction = VUp;
		//underVolume = 1;
		under = 0.5;
		return true; // special handling required for this situation
	}
	else
	{

		//underVolume = volume;
		under = maxDThis-minDWith;
	}



	return true;
}

#if _ENABLE_CHEATS
#define DIAG_INTERSECT 1
#endif


void Object::Intersect
(
	CollisionBuffer &result, Object *with,
	const FrameBase &thisPos, const FrameBase &withPos,
	int hierLevel
) const
{
	// typical usage:
	// this is object in landscape
	// with is vehicle we test on collisions
	// with inverse transformation is typically cheap
	LODShape *withShape=with->GetShape();
	LODShape *thisShape=this->GetShape();
	if( !withShape || !thisShape ) return;
	// check all possible collisions between components of this and with
	// create transformation between with and this
	// first of all check object bounding spheres
	float dist2=thisPos.Position().Distance2(withPos.Position());
	float sumRadius2=Square(this->GetRadius()+with->GetRadius());
	if( dist2>sumRadius2 ) return; // no collision possible
	#if 1 //#if !_RELEASE
		if( thisShape->_geomComponents->Size()<=0 ) return;
		if( withShape->_geomComponents->Size()<=0 ) return;
	#endif
	Shape *withGeom=withShape->GeometryLevel();
	Shape *thisGeom=thisShape->GeometryLevel();

	if( !withGeom || !thisGeom ) return;
	// make sure both objects have well defined geometry level
	// note: we will deanimate it again
	const_cast<Object *>(this)->AnimateGeometry();
	const_cast<Object *>(with)->AnimateGeometry();

	// minMax may be invalid now
	thisGeom->RecalculateNormalsAsNeeded();
	withGeom->RecalculateNormalsAsNeeded();


	#if PERF_ISECT
	ADD_COUNTER(isecO,1);
	#endif

	Matrix4Val withToThis=thisPos.GetInvTransform()*withPos;

	// draw min-max box positions
	#if DIAG_INTERSECT
	if (CHECK_DIAG(DECollision))
	{
		// draw bbox of this and with
		DiagBBox(thisGeom->MinMax(),thisPos,PackedColor(Color(0,0,1,0.5)));
		DiagBBox(withGeom->MinMax(),withPos,PackedColor(Color(0,1,0,0.5)));
	}
	#endif
	Vector3 isect[2];
	if( IntersectBBox(isect,thisGeom->MinMax(),withGeom->MinMax(),withToThis) )
	{
		#if DIAG_INTERSECT
		if (CHECK_DIAG(DECollision))
		{
			DiagBBox(isect,thisPos,PackedColor(Color(0,0,0,0.5)));
		}
		#endif
		Matrix4Val thisToWith=withPos.GetInvTransform()*thisPos;

		thisShape->RecalculateGeomComponentsAsNeeded();
		withShape->RecalculateGeomComponentsAsNeeded();

		for( int iThis=0; iThis<thisShape->_geomComponents->Size(); iThis++ )
		{
			const ConvexComponent &cThis=*(*thisShape->_geomComponents)[iThis];
			// check cThis with "with" bsphere
			#if PERF_ISECT
			ADD_COUNTER(isecT,1);
			#endif

			Vector3 thisCenter=thisPos.FastTransform(cThis.GetCenter());
			float dist2=thisCenter.Distance2(withPos.Position());
			float sumRadius2=Square(cThis.GetRadius()+with->GetRadius());
			if( dist2>sumRadius2 ) continue;

			for( int iWith=0; iWith<withShape->_geomComponents->Size(); iWith++ )
			{
				const ConvexComponent &cWith=*(*withShape->_geomComponents)[iWith];
				// check this and with bounding sphere

				#if PERF_ISECT
				ADD_COUNTER(isecS,1);
				#endif

				float sphereDist2=(thisToWith*cThis.GetCenter()).Distance2(cWith.GetCenter());
				if( sphereDist2>Square(cThis.GetRadius()+cWith.GetRadius()) ) continue;
				// calculate in this space
				// edges have always one component different
				Vector3 tMinMax[2];

				#if PERF_ISECT
				ADD_COUNTER(isecB,1);
				#endif

				if( !IntersectBBox(tMinMax,cThis.MinMax(),cWith.MinMax(),withToThis) ) continue;

				#if DIAG_INTERSECT
				if (CHECK_DIAG(DECollision))
				{
					DiagBBox(tMinMax,thisPos,PackedColor(Color(0.5,0.5,0.5,1)));
				}
				#endif

				#if PERF_ISECT
				ADD_COUNTER(isecP,1);
				#endif

				CalculateIntersectionsExact
				(
					result,this,
					cThis,cWith,thisToWith,withToThis,
					*this,hierLevel
				);

			} // for( iWith )
		} // for( iThis )
	}

	// scan all proxies in geometry level
	int geomLevel = thisShape->FindGeometryLevel();
	int nProxies = GetProxyCount(geomLevel);
	if (nProxies>0)
	{
		Matrix4 trans0 = Transform();
		Matrix4 invTrans0 = GetInvTransform();
		for (int p=0; p<nProxies; p++)
		{
			Matrix4 trans = trans0;
			Matrix4 invTrans = invTrans0;
			// note: shape is ignored
			LODShapeWithShadow *shape = NULL;
			Object *obj = GetProxy
			(
				shape,geomLevel,trans,invTrans,*this,p
			);
			if (!obj) continue;
			// pass frame
			FrameWithInverse objFrame(trans,invTrans);
			// add intersection with given proxy
			obj->Intersect(result,with,objFrame,withPos,hierLevel+1);
		}
	}

	const_cast<Object *>(this)->DeanimateGeometry();
	const_cast<Object *>(with)->DeanimateGeometry();
}

void Object::Intersect
(
	CollisionBuffer &result, Object *with
) const
{
	Intersect(result,with,*this,*with);
}

inline Vector3 NearestPoint( Vector3Par beg, Vector3Par end, Vector3Par pos )
{
	// point on line beg .. end nearest to pos
	Vector3Val eb = end - beg;
	Vector3Val pb = pos - beg;
	float t = (eb*pb) / eb.SquareSizeInline();
	saturate(t,0,1);
	return beg+eb*t;
}

void Object::Intersect
(
	CollisionBuffer &result,
	Vector3Par beg, Vector3Par end, float radius,
	ObjIntersect type
) const
{
	Intersect(*this,result,beg,end,radius,type);
}

#define LOG_ISECT 0

void Object::Intersect
(
	const FrameBase &pos,
	CollisionBuffer &result,
	Vector3Par beg, Vector3Par end, float radius,
	ObjIntersect type, int hierLevel
) const
{
	LODShape *thisShape=this->GetShape();
	if( !thisShape ) return;
	// make sure there is well defined geometry LOD

	ConvexComponents *cc=NULL;
	int geomLevel;
	if( type==ObjIntersectFire || type==ObjIntersectIFire )
	{
		cc=thisShape->_fireComponents, geomLevel = thisShape->_geometryFire;
	}
	else if( type==ObjIntersectView )
	{
		cc=thisShape->_viewComponents, geomLevel = thisShape->_geometryView;
	}
	else
	{
		cc=thisShape->_geomComponents, geomLevel = thisShape->_geometry;
	}
	if (geomLevel<0) return;

	if( cc->Size()<=0 ) return;

	if (_isDestroyed)
	{
		if ((DestructType)_destrType==DestructTree) return;
		if ((DestructType)_destrType==DestructTent) return;
	}

	// note: we will deanimate it again
	if( geomLevel<0 ) return;

	Shape *geomShape=thisShape->LevelOpaque(geomLevel);

	#if 0 //_ENABLE_CHEATS
	bool verbose = false;
	if (CHECK_DIAG(DECollision))
	{
		if (!strcmpi(GetShape()->GetName(),"data3d\\lampadrevo.p3d"))
		{
			verbose = true;
		}
	}
	#endif

	// check if distance of line beg..end from boundingsphere is low enough
	// distance of line and point:
	// find nearest point on line
	Vector3 rp = NearestPoint(beg,end,pos.Position());

	float scale = Scale();

	float rDistFromLine2 = rp.Distance2(pos.Position());
	float factor = IsAnimated(geomLevel) ? 2 : 1;
	float rDistFromLine2Max = Square(factor*scale*thisShape->BoundingSphere()); 
	// quick rejection of very far objects
	if( rDistFromLine2>rDistFromLine2Max )
	{
		#if 0 //_ENABLE_CHEATS
		if (verbose)
		{
			LogF
			(
				" obj missed, dist %.3f>%.3f, bRadius %.3f",
				sqrt(rDistFromLine2),sqrt(rDistFromLine2Max),
				thisShape->BoundingSphere()
			);
		}
		#endif
		// collision not possible
		ADD_COUNTER(ssecN,1);
		return;
	}


	const_cast<Object *>(this)->AnimateComponentLevel(geomLevel);

	// update normals, min-max and bSphere
	geomShape->RecalculateNormalsAsNeeded();

	Vector3 bCenter = pos.PositionModelToWorld(geomShape->BSphereCenter());
	Vector3 p = NearestPoint(beg,end,bCenter);
	float bRadius = geomShape->BSphereRadius();
	float distFromLine2 = p.Distance2(bCenter);
	float distFromLine2Max = Square(bRadius*scale);
	if( distFromLine2>distFromLine2Max )
	{
		#if 0 //_ENABLE_CHEATS
		if (verbose)
		{
			LogF
			(
				" line missed, dist %.3f>%.3f, bRadius %.3f",
				sqrt(distFromLine2),sqrt(distFromLine2Max),
				bRadius
			);
		}
		#endif
		const_cast<Object *>(this)->DeanimateComponentLevel(geomLevel);
		ADD_COUNTER(ssecN,1);
		return;
	}

	

	// TODO: perform some statistics: which object are recalculated most often (and why)

	cc->RecalculateAsNeeded(geomShape); //thisShape->RecalculateConvexComponentsAsNeeded();
	// all calculation will be performed in this space
	// convert beg and end

	ADD_COUNTER(ssecO,1);

	Matrix4Val invTransform=pos.GetInvTransform();
	Vector3Val begThis=invTransform.FastTransform(beg);
	Vector3Val endThis=invTransform.FastTransform(end);

	#if LOG_ISECT
		//if (type==ObjIntersectFire)
		{
			LogF("begThis %.1f,%.1f,%.1f,",begThis.X(),begThis.Y(),begThis.Z());
			LogF("endThis %.1f,%.1f,%.1f,",endThis.X(),endThis.Y(),endThis.Z());
		}
	#endif

	for( int iThis=0; iThis<cc->Size(); iThis++ )
	{
		ADD_COUNTER(ssecC,1);

		const ConvexComponent &cThis=*cc->Get(iThis);
		// check intersection will all convex components
		// check line beg..end with all cThis faces
		Vector3 b=begThis,e=endThis;
		float bt=0,et=1;
		// clip the b..e line with all planes of cThis
		int i;
		Assert( cThis.NPlanes()>=4 );
		Vector3 dirOut;

		#if LOG_ISECT
		LogF
		(
			"%s (%s): component %d",
			(const char *)GetDebugName(),(const char *)GetShape()->Name(),iThis
		);
		#endif

		for( i=0; i<cThis.NPlanes(); i++ )
		{
			const Plane &plane=cThis.GetPlane(i);
			float distE=plane.Distance(e);
			float distB=plane.Distance(b);
			// dist<0 means point is in outer space
			if( distB<0 && distE<0 )
			{
				#if LOG_ISECT
					LogF("  NotInside");
				#endif
				goto NotInside;
			}
			// dist>=0 means point is in inner space
			if( distB>=0 && distE>=0 ) continue; // no clip
			Vector3Val normal=plane.Normal();
			Vector3 bme=b-e;
			float denom=bme*normal;
			if( fabs(denom)<1e-6 )
			{
				#if _ENABLE_REPORT
				if( bme.Normalized()*normal<1e-6 )
				{
					RptF
					(
						"Object::Intersect bme %.2f,%.2f,%.2f normal %.2f,%.2f,%.2f",
						bme.X(),bme.Y(),bme.Z(),
						normal.X(),normal.Y(),normal.Z()
					);
				}
				#endif
				#if LOG_ISECT
					LogF("  Parallel");
				#endif
				continue; // parallel - no clip
			}
			float t=(plane.Normal()*b+plane.D())/denom;

			// there must be some intersection
			Assert( t>=-1e-3 );
			Assert( t<=1+1e-3 );
			//Vector3 pt=(b-e)*t+e;
			Vector3 pt=(e-b)*t+b;
			Assert( plane.Distance(pt)<1e-3 );
			// note: t is between bt and et
			float tt = bt+t*(et-bt);

			if( distB<0 )
			{
				// b is outside 
				//Assert(t<=et);
				b=pt,bt=tt;
				#if LOG_ISECT
					LogF("  setb %.3f",tt);
				#endif
			}
			else
			{
			  // e is outside
				Assert(distE<0);
				//Assert(bt<=t);
				e=pt,et=tt;
				#if LOG_ISECT
					LogF("  sete %.3f",tt);
				#endif
			}
			dirOut=normal;

			#if 0 //SHOT_STARS
				if( CHECK_DIAG(DECollision) )
				{
					GScene->DrawCollisionStar
					(
						PositionModelToWorld(pt),0.05,PackedColor(Color(0.25,0,0.25))
					);
				}
			#endif
		}
		{
			// b,e interval is intersection
			/**/
			if (bt>et)
			{
				// there must be some degenerate planes?
				// probably non-convex component?
				#if LOG_ISECT
					LogF("  t %.3f..%.3f no hit",bt,et);
				#endif
				//continue;
			}
			/**/

			// return collision information
			CollisionInfo &ret=result.Append();
			// check nearest point of intersections
			Vector3 tPoint=b;
			float t=bt;
			#if 1
			float maxDist=_shape->GeometrySphere()*2;
			if( tPoint.SquareSize()>Square(maxDist) )
			{
#if LOG_ISECT
					LogF("Impossible hit %.3f,%.3f,%.3f",tPoint[0],tPoint[1],tPoint[2]);
#endif
				tPoint=tPoint.Normalized()*maxDist;
			}
			#endif
			#if 0 //_ENABLE_CHEATS //SHOT_STARS
#if LOG_ISECT
					LogF("  tPoint       %.3f,%.3f,%.3f",tPoint[0],tPoint[1],tPoint[2]);
#endif
				if( CHECK_DIAG(DECollision) )
				{
					GScene->DrawCollisionStar
					(
						PositionModelToWorld(tPoint),0.05,PackedColor(Color(0.5,0,0.5))
					);
					GScene->DrawCollisionStar
					(
						PositionModelToWorld(b),0.025,PackedColor(Color(0.25,0,0.25))
					);
					GScene->DrawCollisionStar
					(
						PositionModelToWorld(e),0.025,PackedColor(Color(0.25,0,0.25))
					);
				}
			#endif
			ret.pos=tPoint;

			// get texture from any face of the component

			ret.texture=cThis.GetTexture();
			ret.dirOut=e-b; // note: dir out is nonsense
			ret.under=t;
			ret.hierLevel = hierLevel;
			ret.object=const_cast<Object *>(this);
			ret.component = iThis;
			#if 0
				LogF("  t %.3f..%.3f hit %s",bt,et,(const char *)GetDebugName());
				// calculate distance from beg
				LogF("  dist %.3f, %s",tPoint.Distance(begThis)/endThis.Distance(begThis),GetShape()->Name());
			#endif
			#if 0 //_ENABLE_CHEATS //SHOT_STARS
				if( CHECK_DIAG(DECollision) )
				{
					GScene->DrawCollisionStar
					(
						PositionModelToWorld(ret.pos),0.05,PackedColor(Color(1,0,1))
					);
				}
			#endif
		}

		
		NotInside:;
	}

	// scan all proxies in geometry level
	int nProxies = GetProxyCount(geomLevel);
	if (nProxies>0)
	{
		Matrix4 trans0 = Transform();
		Matrix4 invTrans0 = GetInvTransform();
		for (int p=0; p<nProxies; p++)
		{
			// note: shape is ignored
			LODShapeWithShadow *shape = NULL;
			Matrix4 trans = trans0;
			Matrix4 invTrans = invTrans0;
			Object *obj = GetProxy
			(
				shape,geomLevel,trans,invTrans,*this,p
			);
			if (!obj) continue;
			// pass frame
			FrameWithInverse objFrame(trans,invTrans);
			// add intersection with given proxy
			obj->Intersect(objFrame,result,beg,end,radius,type,hierLevel+1);
		}
	}

	const_cast<Object *>(this)->Deanimate(geomLevel);
}

bool Object::VerifyStructure() const
{
	return true;
}

bool Object::IsInside(Vector3Par pos, ObjIntersect type) const
{
	LODShape *thisShape=this->GetShape();
	if( !thisShape ) return false;
	// make sure there is well defined geometry LOD

	ConvexComponents *cc=NULL;
	int geomLevel;
	if( type==ObjIntersectFire || type==ObjIntersectIFire )
	{
		cc=thisShape->_fireComponents, geomLevel = thisShape->_geometryFire;
	}
	else if( type==ObjIntersectView )
	{
		cc=thisShape->_viewComponents, geomLevel = thisShape->_geometryView;
	}
	else
	{
		cc=thisShape->_geomComponents, geomLevel = thisShape->_geometry;
	}
	if (geomLevel<0) return false;
	Shape *geomShape=thisShape->LevelOpaque(geomLevel);

	if( cc->Size()<=0 ) return false;

	if (_isDestroyed)
	{
		if ((DestructType)_destrType==DestructTree) return false;
		if ((DestructType)_destrType==DestructTent) return false;
	}

	// note: we will deanimate it again
	const_cast<Object *>(this)->AnimateComponentLevel(geomLevel);
	geomShape->RecalculateNormalsAsNeeded();
	thisShape->RecalculateConvexComponentsAsNeeded(geomLevel);
	// all calculation will be performed in this space
	// convert beg and end

	Matrix4Val invTransform=GetInvTransform();
	Vector3Val point=invTransform.FastTransform(pos);
	bool ret=false;
	for( int iThis=0; iThis<cc->Size(); iThis++ )
	{
		const ConvexComponent &cThis=*(*cc)[iThis];
		// check intersection will all convex components
		if( cThis.IsInside(point) )
		{
			ret=true;
			break;
		}
	}
	const_cast<Object *>(this)->DeanimateComponentLevel(geomLevel);
	return ret;
}

void Object::DrawDiags()
{
	#if 0
	// draw bounding sphere of geometry level
	Shape *geom = _shape ? _shape->GeometryLevel() : NULL;
	if (geom)
	{
		Vector3 bCenter = PositionModelToWorld(geom->BSphereCenter());
		float bRadius = geom->BSphereRadius();


		LODShapeWithShadow *shape = GScene->Preloaded(SphereModel);
		Ref<Object> obj=new ObjectColored(shape,-1);
		obj->SetPosition(bCenter);
		obj->SetScale( bRadius/shape->BoundingSphere() );
		obj->SetConstantColor(PackedColor(Color(0,0,0,0.1)));
		obj->Draw(0,ClipAll,*obj);
	}
	#endif

}

void Object::InitSkew( Landscape *land )
{
}

DEFINE_FAST_ALLOCATOR(ObjectPlain)

ObjectPlain::ObjectPlain
(
	LODShapeWithShadow *shape, int id
)
:base(shape,id)
{
}

DEFINE_FAST_ALLOCATOR(ObjectColored)

ObjectColored::ObjectColored
(
	LODShapeWithShadow *shape, int id
)
:base(shape,id)
{
	_constantColor = PackedWhite;
	_special = 0;
}

PackedColor ObjectColored::GetConstantColor() const
{
#if _ENABLE_CHEATS
	if (CHECK_DIAG(DETransparent))
	{
		return PackedColorRGB(_constantColor,_constantColor.A8()/2);
	}
#endif
	return _constantColor;
}

void ObjectColored::GetMaterial(TLMaterial &mat, int index) const
{
	Color accom=GEngine->GetAccomodateEye();
	Color ccolor = GetConstantColor();
	ccolor = ccolor * accom;

	// distrubute by predefined materials
	CreateMaterial(mat,ccolor,index);
}
