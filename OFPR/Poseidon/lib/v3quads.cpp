#include "wpch.hpp"

#include "v3quads.hpp"
#include "Shape.hpp"

// SoAoS: Structure of arrays of structures

V3Array::V3Array()
{
	_size=0;
}

V3Array::~V3Array()
{
}

const V3QElement V3QZero(0,0,0);
const V3QElement V3QAside(1,0,0);
const V3QElement V3QUp(0,1,0);
const V3QElement V3QForward(0,0,1);

void V3Array::SetStorage( MemAllocS *storage )
{
	void *data = storage->GetMemory();
	if (((int)data&15)!=0)
	{
		LogF("Static storage not aligned - %x",data);
		Fail("  misaligned data");
	}
	_data.SetStorage(storage);
}

inline bool Aligned( const void *x, int a )
{
	return (int(x)&(a-1))==0;
}

#define AssertAligned(x,a) \
	if (!Aligned(x,a))\
	{\
		LogF("Alignment error: %s %x",#x,x); \
		Fail("Misaligned data"); \
	}

#if USE_QUADS
void VertexTable::ConvertToQArray()
{
	extern bool EnablePIII;
	if (!EnablePIII) return;
	// conver
	int n = _pos.Size();
	_posQ.Realloc(n);
	_normQ.Realloc(n);
	_posQ.Resize(n);
	_normQ.Resize(n);

	AssertAligned(_posQ.QuadData(),16);
	AssertAligned(_normQ.QuadData(),16);

	for (int i=0; i<n; i++)
	{
		_posQ.Set(i) = _pos[i];
		_normQ.Set(i) = _norm[i];
	}
}

void VertexTable::RemoveNormalArrays()
{
	_pos.Clear();
	_norm.Clear();
	_orig.Clear();
	_origNorm.Clear();
}

#endif
