/*!
\file
Scene implementation
*/
#include "wpch.hpp"
#include "global.hpp"
#include "keyInput.hpp"
#include "dikCodes.h"
#include <El/Common/randomGen.hpp>

#include "scene.hpp"
#include "engine.hpp"
#include "textbank.hpp"
#include "lights.hpp"
#include "landscape.hpp"
#include "txtPreload.hpp"
#include "object.hpp"
#include "camera.hpp"
#include "poly.hpp"
#include "animation.hpp"
#include <El/ParamFile/paramFile.hpp>
#include "vehicleAI.hpp"
#include "clipVert.hpp"
#include "shots.hpp"
#include "objLine.hpp"
#include "objectClasses.hpp"
#include "frameInv.hpp"
#include "ai.hpp" // remove dependancy
#include "world.hpp"
#include <Es/Algorithms/qsort.hpp>
//#include "strIncl.hpp"
#include "stringtableExt.hpp"
#include <time.h>
#include "diagModes.hpp"

#include <El/Common/perfLog.hpp>
#include "perfProf.hpp"

Scene *GScene;

#define PASS_VEC(x) x.X(),x.Y(),x.Z()

typedef float FogF( float dist, float start, float end );

#define FogLinear ( (FogF *)NULL )

float FogQuadratic( float dist, float start, float end )
{
	return Square(dist-start)/Square(end-start);
}

#define LN_20 2.9957322736f

extern bool EnableHWTL;

float FogExponential( float dist, float start, float end )
{
	//float clear=exp(-LN_20*dist/end);
	//float invClearStart=exp(LN_20*start/end);
	// scale so that at start fog is 0
	//return 1-clear*invClearStart;
	//float clear=exp(-LN_20*dist/end);
	//float invClearStart=exp(LN_20*start/end);
	// scale so that at start fog is 0
	return 1-exp(LN_20*(start-dist)/end);
}

#define FOG_FUNCTION FogExponential

static void AdaptVolumeLight( LODShape *lodShape )
{
	lodShape->OrSpecial
	(
		NoShadow|IsAlpha|IsLight|NoZWrite|ClampU|ClampV|IsAlphaFog|IsColored
	);
}

template Ref<RoadType>; // bug is MSVC 5.0 - force instantiation

#if 0 //_ENABLE_CHEATS
#define DENSITY_LOD 1
#endif

#if DENSITY_LOD
#define _drawDensity _lodInvWidth
#endif


Scene::Scene()
:_skyColor(HWhite),
_mainLight(NULL),
_camera(new Camera),
_tacticalVisibility(TACTICAL_VISIBILITY),
_rainRange(900),
_constantFog(0),
_constantColor(HWhite),
_objectShadows(false),
_vehicleShadows(true),
_cloudlets(true),
_preferredTerrainGrid(EnableHWTL ? 12.5 : 25),
_preferredViewDistance(900)
{
	static StaticStorage<ActiveLightPointer> aLightsS;
	_aLights.SetStorage(aLightsS.Init(64));

	LoadConfig();

	_lodInvWidth = (_minLodInvWidth+_maxLodInvWidth)*0.5;
	
	#if _PIII
		Assert( ((int)this&0xf)==0 );
	#endif
	ResetFog();
}

static void UpdateFogRange( float &minRange, float &maxRange, float tacVis )
{
	//float origMax=maxRange;
	//float tacCoef=tacVis*(1.0f/TACTICAL_VISIBILITY);
	//float coefMax=origMax*tacCoef;
	saturateMin(maxRange,tacVis);
	saturateMin(minRange,maxRange*0.4f);
}

void Scene::ResetFog()
{
	float minRange,maxRange;
	// shadow fog
	minRange=MAX_SHADOWFOG*0.3f,maxRange=MAX_SHADOWFOG;
	UpdateFogRange(minRange,maxRange,_rainRange);
	_shadowFog.Set(minRange,maxRange,FOG_FUNCTION);
	_shadowFogMinRange=minRange,_shadowFogMaxRange=maxRange;
	// display fog
	// set back clipping to fog range
	Glob.config.horizontZ=floatMin(_rainRange+20,Glob.config.tacticalZ);
	minRange=MAX_FOG*0.3f,maxRange=MAX_FOG;
	UpdateFogRange(minRange,maxRange,_rainRange);
	_fog.Set(minRange,maxRange,FOG_FUNCTION);
	_fogMinRange=minRange,_fogMaxRange=maxRange;
	// sky fog is constant
	_skyFog.Set(MinSkyFog,MaxSkyFog,FOG_FUNCTION);
	_tacticalFog.Set(minRange,_tacticalVisibility,FOG_FUNCTION);
	#if 0
		GLOB_ENGINE->ShowMessage
		(
			1000,"TVis %.1f, fvis %.1f",_tacticalVisibility,_fogMaxRange
		);
	#endif
}

void Scene::SetTacticalVisibility( float tv, float rainRange )
{
	_tacticalVisibility=tv;
	if( fabs(rainRange-_rainRange)<3 ) return;
	_rainRange=rainRange;
	//LogF("Tac range %.3f",rainRange);
	ResetFog();
}


void Scene::Init( Engine *engine, Landscape *landscape )
{
	// forget all cached entries
	// init ...
	//_engine=engine;

	if( _landscape!=landscape ) _landscape=landscape;
	AbstractTextBank *bank=GEngine->TextBank();
	if( _landscape )
	{
		// some shapes have been modified and must be reloaded

		_landscape->SetOvercast(0.0);

		_skyTexture=_landscape->SkyTexture();

		if (!_preloaded[CraterShell])
		{
			_preloaded[CraterShell]=Shapes.New("data3d\\krater.p3d",false,false);
			_preloaded[CraterBullet]=Shapes.New("data3d\\krater_po_kulce.p3d",false,false);
#if _ENABLE_DATADISC
			_preloaded[SlopBlood]=Shapes.New("misc\\krvava_skvrna.p3d",false,false);
#endif
			_preloaded[CloudletBasic]=Shapes.New("data3d\\cl_basic.p3d",false,false);
			_preloaded[CloudletFire]=Shapes.New("data3d\\cl_fire.p3d",false,false);
			_preloaded[CloudletFireD]=Shapes.New("data3d\\cl_fireD.p3d",false,false);
			_preloaded[CloudletWater]=Shapes.New("data3d\\cl_water.p3d",false,false);
			_preloaded[CloudletMissile]=Shapes.New("data3d\\missileSmoke.p3d",false,false);

			_preloaded[CinemaBorder]=Shapes.New("data3d\\kino.p3d",false,false);

			for( int c=0; c<4; c++ )
			{
				char name[256];
				sprintf(name,"data3d\\mrak%d.p3d",c+1);
				PreloadedShape cs=(PreloadedShape)(Cloud1+c);
				_preloaded[cs]=Shapes.New(name,false,false);
				_preloaded[cs]->OrSpecial(NoZBuf|IsAlpha|IsAlphaFog);
				Shape *shape=_preloaded[cs]->LevelOpaque(0);
				for( int v=0; v<shape->NPos(); v++ )
				{
					shape->SetClip(v,ClipFogSky|ClipLightCloud|(ClipAll&~ClipBack));
				}
				shape->CalculateHints();
				_preloaded[cs]->CalculateHints();
				_preloaded[cs]->AllowAnimation();
			}

			const int cloudletSpec=ClampU|ClampV|NoZWrite|IsAlpha|IsAlphaFog|NoShadow|IsColored;
			_preloaded[CloudletBasic]->OrSpecial(cloudletSpec);
			_preloaded[CloudletFire]->OrSpecial(cloudletSpec);
			_preloaded[CloudletFireD]->OrSpecial(cloudletSpec);
			_preloaded[CloudletWater]->OrSpecial(cloudletSpec);
			_preloaded[CloudletMissile]->OrSpecial(cloudletSpec);
			_preloaded[CraterShell]->OrSpecial(ClampU|ClampV|NoZWrite|IsAlpha|IsAlphaFog|OnSurface|NoShadow|IsColored);
			_preloaded[CraterBullet]->OrSpecial(ClampU|ClampV|NoZWrite|IsAlpha|IsAlphaFog|OnSurface|NoShadow|IsColored);
#if _ENABLE_DATADISC
			_preloaded[SlopBlood]->OrSpecial(ClampU|ClampV|NoZWrite|IsAlpha|IsAlphaFog|OnSurface|NoShadow|IsColored);
#endif
			_preloaded[CobraLight]=Shapes.New("data3d\\cobrasvetlo.p3d",false,false);
			_preloaded[SphereLight]=Shapes.New("data3d\\koulesvetlo.p3d",false,false);
			_preloaded[HalfLight]=Shapes.New("data3d\\halflight.p3d",false,false);
			_preloaded[Marker]=Shapes.New("data3d\\obrysove svetlo.p3d",false,false);
			
			_preloaded[FootStepL]=Shapes.New("data3d\\stopa_l.p3d",false,false);
			_preloaded[FootStepR]=Shapes.New("data3d\\stopa_p.p3d",false,false);

			_preloaded[PaperCarModel]=Shapes.New("data3d\\papauto.p3d",true,true);
			_preloaded[ForceArrowModel]=Shapes.New("data3d\\force.p3d",false,false);
			_preloaded[SphereModel]=Shapes.New("data3d\\koule.p3d",false,false);
			_preloaded[RectangleModel]=Shapes.New("data3d\\rect.p3d",false,false);

			_preloaded[ForceArrowModel]->OrSpecial(IsColored|IsAlpha|IsAlphaFog);
			_preloaded[SphereModel]->OrSpecial(IsColored|IsAlpha|IsAlphaFog);
			_preloaded[RectangleModel]->OrSpecial(IsColored|IsAlpha|IsAlphaFog);

			_preloaded[SkySphere] = Shapes.New("data3d\\obloha.p3d",false,false);
			_preloaded[HorizontObject] = Shapes.New("data3d\\horizont.p3d",false,false);

			const ParamEntry &veh = Pars>>"CfgVehicles";

			#define FX_LOAD(name) \
				_preloaded[name] = Shapes.New \
				( \
					GetShapeName(veh>>#name>>"model"),veh>>#name>>"reversed",true \
				)

			FX_LOAD(FxExploGround1);
			FX_LOAD(FxExploGround2);

			FX_LOAD(FxExploArmor1);
			FX_LOAD(FxExploArmor2);
			FX_LOAD(FxExploArmor3);
			FX_LOAD(FxExploArmor4);

			FX_LOAD(FxCartridge);

			
			const int FootStepFlags=IsAlpha|NoShadow|NoZWrite|IsAlphaFog|OnSurface|IsColored;
			_preloaded[FootStepL]->OrSpecial(FootStepFlags);
			_preloaded[FootStepR]->OrSpecial(FootStepFlags);

			_preloaded[BulletLine] = ObjectLine::CreateShape();

			AdaptVolumeLight(_preloaded[CobraLight]);
			AdaptVolumeLight(_preloaded[SphereLight]);
			AdaptVolumeLight(_preloaded[HalfLight]);
			AdaptVolumeLight(_preloaded[Marker]);

			LODShapeWithShadow *collisionShape=Shapes.New("data3d\\colision.p3d",false,false);
			collisionShape->OrSpecial(IsAlpha|IsAlphaFog|IsColored);
			_collisionStar=new ObjectColored(collisionShape,-1);
		}

	}
	else
	{
		//_forceScale=NULL;
		_skyTexture=bank->Load("data\\zatazeno.pac");
	}

	CalculateSkyColor(_skyTexture);
}

void Scene::CalculateSkyColor( Texture *texture )
{
	if( !texture ) texture=_skyTexture;
	else _skyTexture=texture;
	if( _skyTexture )
	{
		// guarantee that the mipmap is loaded
		MipInfo mip=GEngine->TextBank()->UseMipmap(_skyTexture,0,0);
		Assert( mip.IsOK() );
		if( mip.IsOK() )
		{
			_skyColor=_skyTexture->GetPixel(0,1,1);
		}
		//Log("SkyColor %f,%f,%f",_skyColor.R(),_skyColor.G(),_skyColor.B());
		//GEngine->TextBank()->ReleaseMipmap();
	}
}

void Scene::CleanUp()
{
	for( int i=0; i<_drawObjects.Size(); i++ )
	{
		_drawObjects[i]->object->SetInList(NULL);
	}
	_drawObjects.Resize(0);
	_drawMergers.Resize(0);
}

Scene::~Scene()
{
	SaveConfig();
	ResetLights();
	if( _mainLight ) delete _mainLight,_mainLight=NULL;
	if( _camera ) delete _camera,_camera=NULL;
	_skyTexture=NULL;
	_landscape.Free();
	GLandscape=NULL;
}

void Scene::SetCamera( const Camera &camera )
{
	if( _camera ) *_camera=camera;
	else _camera=new Camera(camera);
}

FogFunction::FogFunction()
{
	_start2=0;
	_end2=1;
	_divisor=(LEN_FOG_TABLE-1)/(_end2-_start2);
}

void FogFunction::Set
(
	float start, float end,
	float (*function)( float distRel, float start, float end )
)
{
	_start2=start*start;
	_end2=end*end;
	_divisor=(LEN_FOG_TABLE-1)/(_end2-_start2);
	float invEmS=1.0f/(end-start);
	for( int i=0; i<LEN_FOG_TABLE; i++ )
	{
		// index into the table is i, corresponding relative distance is
		float indexRel=i*(1.0f/(LEN_FOG_TABLE-1));
		// indexRel corresponds to (dist*dist-_start*_start)/(_end*_end-_start*_start)
		// we would like to calculate (dist-_start)/(_end-_start)
		float dist=sqrt(indexRel*(_end2-_start2)+_start2);
		float fogLF=(dist-start)*invEmS;
		float fogF=function ? function(dist,start,end) : fogLF;
		//_fog[i]=fogF;
		int fog=toInt(fogF*256);
		if( fog<0 ) fog=0;else if( fog>255 ) fog=255;
		_fog[i]=fog;
	}
	_fog[LEN_FOG_TABLE-1]=0xff; // full fog
	//_fog[LEN_FOG_TABLE-1]=1.0; // full fog
}

int FogFunction::operator () ( float distSquare ) const
{
	// calculate index between 0 and 1
	float indexFlt=(distSquare-_start2)*_divisor;
	int index=toIntFloor(indexFlt);
	if( index<=0 ) return _fog[0];
	if( index<LEN_FOG_TABLE ) return _fog[index];
	return _fog[LEN_FOG_TABLE-1];
}

void Scene::AddLight( Light *light )
{
	int i;
	// if possible insert the light into some empty slot
	for( i=0; i<_lights.Size(); i++ ) if( !_lights[i] )
	{
		_lights[i]=light;
		return;
	}
	_lights.Add(light);
}

void Scene::ResetLights()
{
	_lights.Clear();
}

static int CmpALight
(
	const ActiveLightPointer *l0, const ActiveLightPointer *l1,
	LightContext *context
)
{
	const Light *light0=*l0;
	const Light *light1=*l1;
	Assert( light0 );
	Assert( light1 );
	Assert( light0!=light1 );
	return light1->Compare(*light0,*context);
}

static int CmpALight( const ActiveLightPointer *l0, const ActiveLightPointer *l1 )
{
	const Light *light0=*l0;
	const Light *light1=*l1;
	Assert( light0 );
	Assert( light1 );
	Assert( light0!=light1 );
	return light1->Compare(*light0);
}

/*!
\patch_internal 1.01 Date 6/18/2001 by Ondra
	This function has been introduces to optimize lights used on landscape.
\patch 1.16 Date 6/18/2001 by Ondra
	Fixed: Night landscape lighting
	(terrain was black sometimes even directly undeer lamppost).
*/

const LightList &Scene::SelectLights
(
	Vector3Par pos, float radius, LightList &work
)
{
	// select only the nearest lights to make lighting faster
	int i;
	for( i=0; i<_aLights.Size(); i++ )
	{
		// select only light which can add something to object lighting
		Light *light=_aLights[i];
		//float dist = light->SquareDistance(pos);
		float dist2 = light->SquareDistance(pos);
		if (Square(radius)>dist2*Square(0.1))
		{
			float dist = dist2*InvSqrt(dist2)-radius;
			saturateMax(dist,0);
			if (light->Brightness()>0.02*Square(dist))
			{
				work.Add(light);
			}
		}
		else
		{
			if (light->Brightness()>0.02*dist2)
			{
				work.Add(light);
			}
		}
		/**/
		#if 0 //!_RELEASE // light calculation statistics
			static int sumW,sumA;
			static int counter;
			sumW+=work.Size(),sumA+=_aLights.Size();
			if( ++counter==100 )
			{
				counter=0;
				Log("Lights: %d of %d",sumW,sumA);
			}
		#endif
	}
	// try to optimize lights
	if( work.Size()>1)
	{
		LightContext context;
		context.position=pos;
		QSort(work.Data(),work.Size(),&context,CmpALight);
		const int maxLightsPerObject=7;
		if (work.Size()>maxLightsPerObject)
		{
			work.Resize(maxLightsPerObject);
		}
		#if 0 //_ENABLE_REPORT
			if (radius>100)
			{
				LogF("Light report");
				for (int i=0; i<work.Size(); i++)
				{
					Light * l = work[i];
					LogF
					(
						"  %d: dist %.2f, bright %.2f, type %s",
						i,
						sqrt(l->SquareDistance(pos)),
						l->Brightness(),
						typeid(*l).name()
					);
				}
			}
			else
			{
			}
		#endif
	}
	return work;
}

#define DIAG_LIGHT_SELECT

/*!
\patch 1.01 Date 6/18/2001 by Ondra
- Fixed: light selection skipped.
- This caused problems when lighting simple objects with HW T&L,
because lights were not presorted, but some had to be dropped.
\patch 1.64 Date 6/3/2002 by Ondra
- Fixed: Lighting of interior objects (like furniture) was broken,
they were often black.
*/

const LightList &Scene::SelectLights
(
	Matrix4Par objPos, const Object *object, int level, LightList &work
)
{
	work.Clear();
	// no lights used - full day light
	int spec = object->GetSpecial();
	if (spec & DisableSun) return _aLights;

	if( (spec&DisableSun)==0 && MainLight()->NightEffect()<0.01 ) return work;
	// may return work or something else
	LODShape *lShape=object->GetShape();
	Shape *oShape=lShape->LevelOpaque(level);

	// no light selection for sky, clouds ... etc.
	if (spec&IsShadow) return work;
	ClipFlags globalLight = oShape->GetAndHints()&ClipLightMask;
	if( globalLight!=(oShape->GetOrHints()&ClipLightMask) ) globalLight=0;
	switch (globalLight)
	{
		case ClipLightCloud:
		case ClipLightSky:
		case ClipLightStars:
			return work;
	}

	//int maxNPos=oShape->NPos();
	// FIX light selection skipped
	/*
	//if( lShape->BoundingSphere()>=150.0 || maxNPos<=16 )
	if( lShape->BoundingSphere()>=150.0)
	{
		// the object is very large - we are not able to select lights
		return _aLights;
	}
	*/
	// select only the nearest lights to make lighting faster
	return SelectLights(objPos.Position(),lShape->BoundingSphere(),work);
}

static StaticStorage<ActiveLightPointer> LightStorage;

LightList::LightList( bool staticStorage )
{
	if( staticStorage ) SetStorage(LightStorage.Init(64));
}

LightList::LightList( const LightList &src )
{
	SetStorage(LightStorage.Init(64));
	Realloc(src.Size());
	Resize(src.Size());
	// note: we assume LightList does not need any destruction
	CTraits::CopyData(Data(),src.Data(),src.Size());
}

void Scene::SetActiveLights( const LightList &lights )
{
	_aLights.Resize(0);	
	// copy all lights from the list
	for( int i=0; i<lights.Size(); i++ )
	{
		Light *light=lights[i];
		_aLights.Add(light);
	}
}



void Scene::SelectActiveLights( Object *dimmed )
{
	_aLights.Resize(0);
	
	// enumerate all light objects for drawing
	Vector3Val camPos = GetCamera()->Position();
	for( int i=0; i<_lights.Size(); i++ )
	{
		Light *light=_lights[i];
		if( light && light->IsOn() )
		{
			// check if light can be ignored (very far)
			if (light->Position().Distance2(camPos)>Square(Glob.config.horizontZ+500))
			{
				continue;
			}

			bool invisible=false;
			Object *attach=light->AttachedOn();
			if( attach && attach==dimmed ) invisible=true;
			light->ToDraw(ClipAll,invisible);
			_aLights.Add(light);
		}
	}

	// we need to know the strongest light source	
	QSort(_aLights.Data(),_aLights.Size(),CmpALight);
	if( _aLights.Size()>Glob.config.maxLights )
	{
		// use only strongest lights
		_aLights.Resize(Glob.config.maxLights);
	}
}

void Scene::MainLightChanged()
{
	if( GetLandscape() )
	{
		Color sunColor=_mainLight->GetColorFull()*(GetLandscape()->SkyThrough()*0.8f+0.2f);
		sunColor.SaturateMinMax();
		_mainLight->SetDiffuse(sunColor);
	}
	// recalculate background color
	// calculate fog color from sky texture
	// we should apply some lighting (to have dark fog in the night)
	Color lighting=_mainLight->SkyColor();
	lighting=lighting*GEngine->GetAccomodateEye();
	lighting.SaturateMinMax();
	Color color=_skyColor*lighting;
	color.SaturateMinMax();
	color.SetA(1);
	GEngine->SetFogColor(color);
}

const Matrix4 &Scene::ScaledInvTransform() const {return _camera->_scaledInvTransform;}
const Matrix3 &Scene::CamNormalTrans() const {return _camera->_camNormalTrans;}
const Matrix4 &Scene::CamInvTrans() const {return _camera->_camInvTrans;}

float Scene::GetMinimalTerrainGrid() const
{
	// depending on computer speed and visibility, some terrain grid may be unavailable
	float minimalTerrainGrid = 3.125;

	// on PIII/733  with HWT&L we will enable max. 2500m/6.25m
	// when compared to 900m/50m, this is about 500 slower

	//float GetPreferredTerrainGrid() const {return _preferredTerrainGrid;}
	//float GetPreferredViewDistance() const {return _preferredViewDistance;}

	float maxFactor = Glob.config.benchmark*0.5f;
	if (!EnableHWTL) maxFactor *= 0.33f;

	for(;;)
	{
		if (minimalTerrainGrid>26) break;
		float factor = Square(_preferredViewDistance*50/(900*minimalTerrainGrid));
		if (factor<maxFactor)
		{
			LogF("Slowdown factor %.1f",factor);			
			break;
		}
		minimalTerrainGrid *= 2;
	}

	return minimalTerrainGrid;
}

void Scene::SetPreferredTerrainGrid(float x)
{
	/*
	float minGrid = GetMinimalTerrainGrid();
	if (x<minGrid)
	{
		LogF("Grid %.1f would be too slow, clamping to %.1f",x,minGrid);
		x = minGrid;
	}
	*/
	saturate(x,0.5,100);
	_preferredTerrainGrid=x;
	if (GWorld && GWorld->GetMode()!=GModeNetware)
	{
		GWorld->AdjustSubdivision(GWorld->GetMode());
	}
}
void Scene::SetPreferredViewDistance(float x)
{
	saturate(x,500,5000);
	_preferredViewDistance=x;

	float minGrid = GetMinimalTerrainGrid();
	float grid = GetPreferredTerrainGrid();
	if (grid<minGrid)
	{
		LogF("Grid %.1f would be too slow, clamping to %.1f",grid,minGrid);
		_preferredTerrainGrid=grid;
	}

	if (GWorld && GWorld->GetMode()!=GModeNetware)
	{
		GWorld->AdjustSubdivision(GWorld->GetMode());
	}
}

void Scene::SetObjectShadows(bool set)
{
	_objectShadows = set;
}

void Scene::SetVehicleShadows(bool set)
{
	_vehicleShadows = set;
}

void Scene::SetCloudlets(bool set)
{
	_cloudlets = set;
}

void Scene::SetQualitySettings(float val)
{
	_qualitySettings=val;

	// allow immediate change
	_lastScaleBetterTime = UITIME_MIN;
	_lastScaleWorseTime = UITIME_MIN;

	_minLodInvWidth = _qualitySettings;
	_maxLodInvWidth = _qualitySettings*16;

  #if !DENSITY_LOD
	saturate(_lodInvWidth,_minLodInvWidth,_maxLodInvWidth);
  #endif
}

void Scene::SetFrameRateSettings(float val)
{
	_frameRateSettings=val;

	float minFPS = _frameRateSettings*(10.0/15);
	float maxFPS = _frameRateSettings*(20.0/15);
	_maxTargetFrameDuration = 1000/minFPS;
	_minTargetFrameDuration = 1000/maxFPS;

	// allow immediate change
	_lastScaleBetterTime = UITIME_MIN;
	_lastScaleWorseTime = UITIME_MIN;
}


RString Scene::GetQualityText() const
{
	char buffer[256];
	float minQ = -10*log10(_minLodInvWidth);
	float maxQ = -10*log10(_maxLodInvWidth);

	//sprintf(buffer, "%.1f..%.1f", minFPS,maxFPS);
	sprintf(buffer, "%.0f..%.0f", maxQ,minQ);

	return buffer;
}
RString Scene::GetFrameRateText() const
{
	char buffer[256];
	float minFPS = 1000/_maxTargetFrameDuration;
	float maxFPS = 1000/_minTargetFrameDuration;

	//sprintf(buffer, "%.1f..%.1f", minFPS,maxFPS);
	sprintf(buffer, "%.0f..%.0f", minFPS,maxFPS);

	return buffer;
}

RString GetUserParams();

/*!
\patch 1.82 Date 8/14/2002 by Ondra
- New: New userInfo.cfg entries fovLeft and fovTop to improved wide-screen support.
(Default values are fovLeft = 1, fovTop = 0.75. For three monitor view use fovLeft = 3).
*/
void Scene::LoadConfig()
{
	RString name = GetUserParams();

	ParamFile cfg;
	cfg.Parse(name);
	if (cfg.FindEntry("frameRate"))
	{
		float value = cfg>>"frameRate";
		SetFrameRateSettings(value);
	}
	else
	{
		SetFrameRateSettings(15);
	}
	if (cfg.FindEntry("visualQuality"))
	{
		float value = cfg>>"visualQuality";
		SetQualitySettings(value);
	}
	else
	{
		const float qFactor = Glob.config.lodCoef*2/GEngine->Width();

		SetQualitySettings(qFactor);
	}

	if (cfg.FindEntry("objectShadows"))
	{
		bool value = cfg >> "objectShadows";
		SetObjectShadows(value);
	}
	if (cfg.FindEntry("vehicleShadows"))
	{
		bool value = cfg >> "vehicleShadows";
		SetVehicleShadows(value);
	}
	if (cfg.FindEntry("cloudlets"))
	{
		bool value = cfg >> "cloudlets";
		SetCloudlets(value);
	}
	if (cfg.FindEntry("viewDistance"))
	{
		float value = cfg >> "viewDistance";
		SetPreferredViewDistance(value);
	}
	if (cfg.FindEntry("terrainGrid"))
	{
		float value = cfg >> "terrainGrid";
		SetPreferredTerrainGrid(value);
	}

}



void Scene::SaveConfig() const
{
	if (!IsOutOfMemory())
	{
		RString name = GetUserParams();

		ParamFile cfg;
		cfg.Parse(name);
		cfg.Add("frameRate",GetFrameRateSettings());
		cfg.Add("visualQuality",GetQualitySettings());
		cfg.Add("objectShadows",GetObjectShadows());
		cfg.Add("vehicleShadows",GetVehicleShadows());
		cfg.Add("cloudlets",GetCloudlets());
		cfg.Add("viewDistance",GetPreferredViewDistance());
		cfg.Add("terrainGrid",GetPreferredTerrainGrid());

		cfg.Save(name);
	}
}

/*
static void RecalcFrameDuration()
{
	float minFPS = TargetFrameRate*(10.0/15);
	float maxFPS = TargetFrameRate*(20.0/15);
	MaxTargetFrameDuration = 1000/minFPS;
	MinTargetFrameDuration = 1000/maxFPS;
}
*/

extern bool ObjViewer;

bool EnableObjOcc=true;

#include "occlusion.hpp"

SRef<Occlusion> Occlusions=new Occlusion(256,256);

void Scene::BeginObjects()
{
	// mark all objects and not in list:
	for( int i=0; i<_drawObjects.Size(); i++ )
	{
		_drawObjects[i]->notUsed=true;
	}

	//_drawObjects.Resize(0);

	_shadowCache.CleanUp(); // remove old shadows
	Occlusions->Clear();

	//DWORD frameDuration=GEngine->GetLastFrameDuration();

#if _ENABLE_CHEATS
	#if !DENSITY_LOD
	const int FRStep = 1;
	if( GInput.GetCheat1ToDo(DIK_LBRACKET) )
	{
		float val = GetFrameRateSettings() - FRStep;
		saturateMax(val,5);
		SetFrameRateSettings(val);
		GEngine->ShowMessage
		(
			500,"FPS@%s",(const char *)GetFrameRateText()
		);
	}
	if( GInput.GetCheat1ToDo(DIK_RBRACKET) )
	{
		float val = GetFrameRateSettings() + FRStep;
		saturateMin(val,100);
		SetFrameRateSettings(val);
		GEngine->ShowMessage
		(
			500,"FPS@%s",(const char *)GetFrameRateText()
		);
	}

	if( GInput.GetCheat2ToDo(DIK_LBRACKET) )
	{
		float val = GetQualitySettings() / 1.2f;
		saturate(val,0.001,1000);
		SetQualitySettings(val);
		GEngine->ShowMessage
		(
			500,"LOD@%s",(const char *)GetQualityText()
		);
	}
	if( GInput.GetCheat2ToDo(DIK_RBRACKET) )
	{
		float val = GetQualitySettings() * 1.2f;
		saturate(val,0.001,1000);
		SetQualitySettings(val);
		GEngine->ShowMessage
		(
			500,"LOD@%s",(const char *)GetQualityText()
		);
	}
  #endif
#endif
	/**/

	if( ObjViewer )
	{
		_lodInvWidth=Glob.config.lodCoef*2/(GEngine->Width());
	}

	if( _camera )
	{
		_camera->Adjust(GEngine);
		//_camera->SetUserClipPars(VUp,0.2f-seaLevel);
	}
	else
	{
		Fail("No camera.");
	}

	// precalculate shadow properties - constant precalculation
	float addLightsFactor=GScene->MainLight()->NightEffect();
	float skyCoef=floatMin(GScene->GetLandscape()->SkyThrough(),0.6);
	float shadowFactor=skyCoef*0.3+0.1;
	int shadowFactorI=toIntFloor(shadowFactor*(1-addLightsFactor)*255);
	GEngine->SetShadowFactor(shadowFactorI);
}

DEFINE_FAST_ALLOCATOR(SortObject)

#if 0
static float MaxDistance2( Object *obj )
{
	//return Square(Glob.config.objectsZ);
	return Square(Glob.config.horizontZ);
}
#else
//#define MaxDistance2(obj) ( Square(Glob.config.horizontZ) )
#define MaxDistance2(obj) ( Square(Glob.config.objectsZ) )
#endif

/*!
\patch 1.35 Date 12/10/2001 by Ondra
- Fixed: Disappearing shadows when sun is low.
*/

void Scene::ObjectForDrawing( Object *obj, int forceLOD, ClipFlags clip )
{
	LODShapeWithShadow *shape=obj->GetShapeOnPos(obj->Position());

	// some objects may be trivially clipped
	// get rid of them ...
	bool invisible=false;
	Vector3Val pos=obj->Position();
	if( !shape ) return; // nothing to draw
	float radius=obj->GetRadius();
	// get object position in clipping coordinates
	
	if (obj!=GWorld->CameraOn())
	{
		if( _camera->IsClipped(pos,radius,1) )
		{
			// check if shadow is enabled
			if (shape->Special()&NoShadow)
			{
				return;
			}
			// some invisible objects have visible shadows
			// simple test - shadow possible
			if( _camera->IsClipped(pos,radius+50,1) )
			{
				return;
			}

			Vector3 objTop=obj->Position()+Vector3(0,radius,0);
			Vector3 shadowPos=objTop;
			
			// estimate shadow position
			if( !ShadowPos(objTop,shadowPos,_mainLight) ) return;
			if (_camera->IsClipped(shadowPos,0.5f,1))
			{
				// shadow of object top is clipped
				// check whole shadow shape
				// estimate shadow radius and shadow center
				float distTopBot = shadowPos.Distance(pos)*0.5f;
				Vector3 shadowCenter = (shadowPos+pos)*0.5f;
				float shadowRadius = floatMax(distTopBot,radius);
				if( _camera->IsClipped(shadowCenter,shadowRadius,1) )
				{
					return;
				}
			}
			invisible=true;
		}
	}
	
	// calculate distance
	float dist2=_camera->Position().Distance2Inline(pos);

	#if DENSITY_LOD
  // if object is too far, we ignore its shadow
	if( dist2>Square(Glob.config.objectsZ) ) return;

  // if object is smaller than 1 pixel, do not draw it
  // it would cause aliasing artifacts  
  float areaK = _camera->InvLeft()*_camera->InvTop()*GEngine->Width()*GEngine->Height();
	if (Square(radius*2)*areaK<dist2)
  {
    return;
  }
  #endif

	if
	(
		(shape->GetAndHints()&ClipFogMask)==ClipFogShadow &&
		(shape->GetOrHints()&ClipFogMask)==ClipFogShadow
	)
	{
		//float maxDist=Glob.config.shadowsZ;
		if( dist2>=Square(_shadowFogMaxRange+radius) ) return;
	}

	SortObject *sObj=obj->GetInList();
	if( !sObj )
	{
		int index=_drawObjects.Add(new SortObject);
		sObj=_drawObjects[index];

		sObj->object=obj;
		sObj->shape=shape;
		sObj->radius=radius;
		obj->SetInList(sObj); // this object is in list
	}


	if( invisible ) sObj->forceDrawLOD=LOD_INVISIBLE;
	else sObj->forceDrawLOD=forceLOD;

	// if object is near we use nearest distance instead of center distance
	// this avoid degenerate LODs when beign near
	// if radius is 0.25 of distance, it is considered significant
	if (Square(radius)>dist2*Square(0.25))
	{
		float dist = dist2*InvSqrt(dist2);
		float distNear = dist - radius;
		saturateMax(distNear,0);
		dist2 = Square(distNear);
	}

  #if DENSITY_LOD
	sObj->shadowLOD = -1; // override with autodetection
	sObj->drawLOD = -1; // override with autodetection
  #else
	sObj->shadowLOD = LOD_INVISIBLE; // override with autodetection
	sObj->drawLOD = LOD_INVISIBLE; // override with autodetection
  #endif
	sObj->distance2 = dist2;
	sObj->passNum = -1; // noninit

	sObj->orClip=clip;
	sObj->notUsed=false;
}

#define DRAW_OBJS 1

void Scene::CloudletForDrawing( Object *obj )
{
	if (!GetCloudlets()) return;
	#if DRAW_OBJS
	// some objects may be trivially clipped
	// get rid of them ...
	Vector3Val pos=obj->Position();
	float radius=obj->GetRadius();

	//float size=obj->Scale();
	// perform clip test?
	if( _camera->IsClipped(pos,radius,1) ) return;

	Vector3 cPos=GScene->ScaledInvTransform()*pos;

	// camera plane clip test
	// perform more distant clipping than normal
	// in case of real 3d object peform normal clipping
	float nearest=_camera->Near()*obj->CloudletClippingCoef();
	if( cPos.Z()<nearest ) return;

	// if we want to have alpha objects sorted correctly, we need to consider
	// minimum distance

	float dist2=_camera->Position().Distance2Inline(pos);

	// float estimate area
	float size2=Square(radius*GEngine->Width()*_camera->InvLeft());
	if( size2<dist2 ) return;
	
	SortObject *sObj=obj->GetInList();
	if( !sObj )
	{
		int index=_drawObjects.Add(new SortObject);
		sObj=_drawObjects[index];

		sObj->object=obj;
		sObj->radius=radius;
		sObj->shape = obj->GetShape();
		obj->SetInList(sObj); // this object is in list
		 // new object - never occluded?
	}
	sObj->object = obj;
	sObj->drawLOD = 0;
	sObj->forceDrawLOD = 0;
	sObj->shadowLOD = LOD_INVISIBLE;
	sObj->distance2 = dist2;
	sObj->radius = radius;
	sObj->passNum=2; // all cloudlets drawn in alpha pass
	//sObj->drawPosition=*obj;
	sObj->notUsed=false;
	#endif
}

void Scene::ObjectForDrawing( Object *obj )
{
	// this function now obsolete
	// it is used for drawdiags and volume lights
	ObjectForDrawing(obj,-1,ClipAll);
}

typedef Ref<SortObject> SortObjectItem;

static int CmpRevDistObj( const SortObjectItem *p1, const SortObjectItem *p0 )
{
	// the first object is the nearest one
	const SortObject *o0=*p0;
	const SortObject *o1=*p1;
	Coord dif=o0->distance2-o1->distance2;
	if( dif<0 ) return -1;
	if( dif>0 ) return +1;
	return 0;
}

static int CmpShapeObj( const SortObjectItem *p1, const SortObjectItem *p2 )
{
	const SortObject *o1=*p1;
	const SortObject *o2=*p2;

	// first sort by pass
	int sDif = o1->passNum - o2->passNum;
	if (sDif) return sDif;

	LODShape *s1=o1->object->GetShape();
	LODShape *s2=o2->object->GetShape();
	sDif=(int)s2-(int)s1;
	// no invisible LODs here
	if( sDif )
	{
		Shape *ss1 = s1->Level(0);
		Shape *ss2 = s2->Level(0);
		int complex1 = ss1->NFaces();
		int complex2 = ss2->NFaces();
		int cDiff = complex1 - complex2;
		// sort by shape complexity
		// first draw simple shapes
		if (cDiff) return cDiff;
		return sDif;
	}
	// sort by LOD - fine LODs (small LOD numbers) last
	sDif = o2->drawLOD-o1->drawLOD;
	if( sDif ) return sDif;
	// first draw 
	// same shape sort by distance, back first (helps to alpha transparency)
	Coord fDif=o2->distance2-o1->distance2;
	if( fDif<0 ) return -1;
	if( fDif>0 ) return +1;
	return 0;
}

void Scene::EndObjects()
{
	// sort by screen size

}

/*!
\patch 1.20 Date 8/16/2001 by Ondra
- Fixed: Lens-flares are no longer colored in night vision.
*/
void Scene::DrawFlare( ColorVal color, Vector3Par lightPos, bool secondary )
{
	Color lightColor=color * GEngine->GetAccomodateEye();
	float oldA = lightColor.A();
	// we can draw flares now
	// flare positions between posLight (0) and screen center (1)
	static const float flarePos[FlareLast+1-Flare0]=
	{
		0.0f,-0.2f,-0.1f,+0.25f,+0.275f,+0.3f,+0.4f,+0.5f,
		+0.65f,+0.7f,+0.725f,+0.75f,+0.875f,+0.885f,+1.0f,+1.1f
	};
	// note - z is ignored when doing 2D draw
	// but is significant for decal draw
	float w=GLOB_ENGINE->Width();
	float h=GLOB_ENGINE->Height();
	// calculate screen position of light
	Vector3Val pos=ScaledInvTransform()*lightPos;
	// apply perspective
	Matrix4Val project=GetCamera()->Projection();
	const float thold=0.6f;
	if( pos[2]<thold ) return; // no flares
	float vis=( pos[2]-thold )*(1.0f/(1-thold));
	saturateMin(vis,1);
	float a=0.9f*vis*vis;
	if( a>0.01 )
	{
		float invW=1/pos[2];
		Vector3 lightPos
		(
			project(0,2)+project(0,0)*pos[0]*invW,
			project(1,2)+project(1,1)*pos[1]*invW,
			1
			//project.Position()[2]*invW
		);

		//float size=0.1*vis;
		float size=0.1f;
		float sizeX=+project(0,0)*size*GetCamera()->InvLeft();
		float sizeY=-project(1,1)*size*GetCamera()->InvTop();
		
		Vector3 center(0.5f*w,0.5f*h,1);
		const int special=NoZBuf|IsLight|ClampU|ClampV|IsAlphaFog;
		// Flare0 is handled specially
		{
			lightColor.SetA(oldA*a*2);
			PackedColor color=PackedColor(lightColor);
			Texture *texture=Preloaded(PreloadedTexture(Flare0));
			if (texture)
			{
				MipInfo mip=GLOB_ENGINE->TextBank()->UseMipmap(texture,0,0);
				if( mip.IsOK() )
				{
					float sizeCoef=texture->AWidth()*(1.0f/64);
					//GEngine->PrepareTriangle(mip,special);
					GEngine->DrawDecal(lightPos,1,sizeX*sizeCoef,sizeY*sizeCoef,color,mip,special);
					//GLOB_ENGINE->TextBank()->ReleaseMipmap();
				}
			}
		}
		if( !secondary ) return;
		lightColor.SetA(a*oldA);
		PackedColor color=PackedColor(lightColor);
		for( int s=Flare0+1; s<=FlareLast; s++ )
		{
			float coef=flarePos[s-Flare0];
			Texture *texture=Preloaded(PreloadedTexture(s));
			if (texture)
			{
				MipInfo mip=GLOB_ENGINE->TextBank()->UseMipmap(texture,0,0);
				if( mip.IsOK() )
				{
					Vector3 pos=lightPos+coef*(center-lightPos);
					float sizeCoef=texture->AWidth()*(1.0f/64);
					//GEngine->PrepareTriangle(mip,special);
					GEngine->DrawDecal(pos,1,sizeX*sizeCoef,sizeY*sizeCoef,color,mip,special);
					//GLOB_ENGINE->TextBank()->ReleaseMipmap();
				}
			}
		}
	}
}

void Scene::DrawFlares()
{
	if( !GetLandscape() ) return;
	float vis=GetLandscape()->SkyThrough();
	float night=MainLight()->NightEffect();

	Vector3Val cPos=GetCamera()->Position();

	if( vis>0.05 && night<0.95 )
	{
		// draw sun flare
		CameraType camType=GWorld->GetCameraType();

		bool secondary=( camType!=CamInternal );
		// check HasFlares of camera source
		Object *camObj = GWorld->CameraOn();
		if (camObj)
		{
			secondary = camObj->HasFlares(camType);
		}
		Vector3 lightDir=MainLight()->SunDirection();
		Color lightColor=MainLight()->SunColor()*(1-night)*vis;

		// fictive sun position
		Vector3 sunPos = cPos - lightDir*Glob.config.horizontZ;
		float sunRadius = 0.02*Glob.config.horizontZ;

		// TODO: check intersection with ground
		float visLand = 1; //GLandscape->Visible(cPos,sunPos,0.5,GWorld->CameraOn(),NULL);
		if (EnableHWTL)
		{	
			float t = GLandscape->IntersectWithGroundOrSea
			(
				NULL,cPos,-lightDir,0,Glob.config.horizontZ*1.1
			);
			visLand = t>=Glob.config.horizontZ;
		}
		if (visLand>0)
		{
			float a = visLand*0.25;
			//saturateMin(a,0.5);
			// check against occlusion buffer
			float occ = Occlusions->TestSphereWSpace(sunPos,sunRadius);
			a *= occ;
			if (a>=0.01)
			{
				// check line against view geometries
				CollisionBuffer col;
				Object *camOn = GWorld->CameraOn();
				if (camOn && !GWorld->GetCameraEffect())
				{
					GLandscape->ObjectCollision(col,camOn,NULL,cPos,sunPos,0,ObjIntersectView);
					// check if any of the objects is not considered
					for (int i=0; i<col.Size(); i++)
					{
						Object *obj = col[i].object;
						if (obj && obj->GetShape() && !obj->GetShape()->CanOcclude())
						{
							// object is not included in occlusion buffer, check it now
							a = 0;
						}
					}
				}
				if (a>=0.01)
				{
					saturateMin(a,1);
					lightColor.SetA(a);
					DrawFlare(lightColor,GetCamera()->Position()-lightDir,secondary);
				}
			}
		}
	}
	if( night>=0.2 )
	{
		// draw flares from active lights
		Vector3Val camPos = GetCamera()->Position();
		Vector3Val camDir = GetCamera()->Direction();
		for( int i=0; i<_aLights.Size(); i++ )
		{
			Light *light=_aLights[i];
			Vector3 dir=camPos-light->Position();
			if( dir*GetCamera()->Direction()>0 ) continue; // this one has no flare
			float intensity = light->FlareIntensity(camPos,camDir);
			if (intensity<0.01) continue;
			// light

			// TODO: check intersection with ground
			// check if light position is visible from camera position
			//float visLand = GLandscape->Visible(cPos,light->Position(),0.5,GWorld->CameraOn(),NULL);
			float visLand = 1;
			if (EnableHWTL)
			{	
				float t = GLandscape->IntersectWithGroundOrSea(NULL,cPos,dir,0,1.1);
				visLand = t>1;
			}

			if (visLand>0)
			{
				Color lightColor=light->GetObjectColor();
				float a = visLand*intensity*2;
				float occ = Occlusions->TestSphereWSpace(light->Position(),0.4);
				a *= occ;
				saturateMin(a,0.5);
				lightColor.SetA(a);
				DrawFlare(lightColor,light->Position(),false);
			}
		}
	}
}

void Scene::DrawRainLevel
(
	float alpha, float yDensity, float xOffset, float yOffset, float z
)
{
	// TODO: preload rain texture
	//LODShapeWithShadow *rain=GetLandscape()->NewShape("data3d\\dest.p3d",false,false,true);
	Texture *texture=Preloaded(TextureRain);
	//rain->LevelOpaque(0)->FaceIndexed(0).GetTexture();
	Color color(HWhite);
	color.SetA(alpha);
	//Vector3Val dir=GetCamera()->Direction();
	//float hSpeed=dir.X();
	Draw2DPars pars;
	pars.mip=GLOB_ENGINE->TextBank()->UseMipmap(texture,0,0);
	pars.SetU(xOffset-z*0.5f,xOffset+z*0.5f);
	pars.SetV(yOffset-z*0.5f*yDensity,yOffset+z*0.5f*yDensity);
	pars.SetColor(PackedColor(color));
	pars.spec=NoZWrite|IsAlpha|NoClamp|IsAlphaFog;
	Rect2DAbs rect(0,0,GLOB_ENGINE->Width(),GLOB_ENGINE->Height());
	GLOB_ENGINE->Draw2D(pars,rect);
	//GLOB_ENGINE->TextBank()->ReleaseMipmap();
}

void Scene::DrawRain()
{
	// start with single level rain
	if( !GetLandscape() ) return;
	float density=GetLandscape()->GetRainDensity();
	if( density>=0.1 )
	{
		Vector3Val dir=GetCamera()->Direction();
		float speed=fabs(GetCamera()->Speed()*dir)*0.05f;
		saturate(speed,0,2);
		static float rainOffset;
		static Time rainT;
		float deltaRT=Glob.time-rainT;
		rainT+=deltaRT;
		//rainOffset+=deltaRT/(1+speed);
		rainOffset+=deltaRT;
		float yOffset=-fastFmod(rainOffset,1);
		float xOffset=atan2(dir.X(),dir.Z())*0.3f;
		float yDensity=0.3f+fabs(dir.Y())+speed;
		saturate(yDensity,0.1f,1);
		// draw all levels
		density*=0.2f;
		//DrawRainLevel(density,yDensity,xOffset,yOffset,20);
		DrawRainLevel(density,yDensity,xOffset,yOffset,8);
		//DrawRainLevel(density,yDensity,xOffset,yOffset,5);
		DrawRainLevel(density,yDensity,xOffset,yOffset,2);
		//DrawRainLevel(density,yDensity,xOffset,yOffset,1);
	}
}


void Scene::ObjectsDrawn()
{

	// release all references
	DrawRain();

	DrawObjectsAndShadowsPass3();

	#if 0
		CleanUp();
		// easy solution if list management fails
	#endif
	// clear working list
	_drawMergers.Resize(0);
	// keep drawObjects for next frame

	DrawFlares();
	_aLights.Resize(0);
	// TODO: realloc lights of storage is too big
}


int Scene::LevelFromDistance2
(
	LODShape *shape,
	float distance2, float oScale,
	Vector3Par direction, Vector3Par viewDirection
)
{
	// if pixel size is lower than 2, object can be considered invisible
	// size in pixels is
	float scale=GetCamera()->Left();
	float diameter=shape->BoundingSphere()*2*oScale;

	if( distance2>Square(Glob.config.objectsZ) ) return LOD_INVISIBLE;

	float pixelLimit=0.125;
	float detail2=distance2*Square(_lodInvWidth);

	if (Square(diameter)<Square(pixelLimit*scale)*detail2)
	{
		return LOD_INVISIBLE;
	}

	if( shape->NLevels()<2 ) return 0;

	//float limit=Glob.config.objectLODLimit;
	//saturateMax(detail2,Square(diameter*limit));

	float resol2=detail2*Square(scale);
	// disable decal LODs
	int level=shape->FindSqrtLevel(resol2,true);

	return level;
}



int Scene::LevelShadowFromDistance2
(
	LODShape *shape,
	float distance2, float oScale,
	Vector3Par direction, Vector3Par viewDirection
)
{
	distance2*=8;

	float scale=GetCamera()->Left();
	float diameter=shape->BoundingSphere()*2*oScale;

	float pixelLimit=0.25;
	float detail2 = distance2*Square(_lodInvWidth);

	if (Square(diameter)<Square(pixelLimit*scale)*detail2)
	{
		return LOD_INVISIBLE;
	}
	
	if( shape->NLevels()<2 ) return 0;
	float limit=Glob.config.shadowLODLimit;
	saturateMax(detail2,Square(diameter*limit));
	float resol2=detail2*Square(scale);
	// disable decal LODs
	int level=shape->FindSqrtLevel(resol2,true);

	return level;
}


#define DO_STAT 0

#if DO_STAT

	#include "statistics.hpp"
	
	NameStatistics Alpha;
	NameStatistics Opaque;
	NameStatistics Shadow;
#endif

#if _ENABLE_CHEATS

// advances diagnostics - via scripting
#include <El/evaluator/express.hpp>

#define NOTHING GameValue()

#define DIAG_DRAW_MODE_ENUM(type,prefix,XX) \
	XX(type, prefix, Normal) \
	XX(type, prefix, Roadway) \
	XX(type, prefix, Geometry) \
	XX(type, prefix, ViewGeometry) \
	XX(type, prefix, FireGeometry) \
	XX(type, prefix, Paths)

DECLARE_DEFINE_ENUM(DiagDrawMode,DDM,DIAG_DRAW_MODE_ENUM)

DEFINE_ENUM(DiagEnable,DE,DIAG_ENABLE_ENUM)

DiagDrawMode DiagDrawModeState = DDMNormal;
int DiagMode;

static GameValue SetDiagDrawMode(const GameState *state, GameValuePar oper)
{
	const char *modeStr = (RString)oper;
	DiagDrawMode mode = GetEnumValue<DiagDrawMode>(modeStr);
	if ((int)mode==-1) return NOTHING;
	DiagDrawModeState = mode;
	return NOTHING;
}

static GameValue SetDiagEnable(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	const char *modeStr = (RString)oper1;
	bool onOff = oper2;
	int modeMask = ~0;
	if (strcmpi(modeStr,"all"))
	{
		DiagEnable mode = GetEnumValue<DiagEnable>(modeStr);
		if ((int)mode==-1) return NOTHING;
		modeMask = 1<<mode;
	}

	if (onOff) DiagMode |= modeMask;
	else DiagMode &= ~modeMask;
	return NOTHING;
}

static GameValue SetDiagToggle(const GameState *state, GameValuePar oper1)
{
	const char *modeStr = (RString)oper1;
	DiagEnable mode = GetEnumValue<DiagEnable>(modeStr);
	if ((int)mode==-1) return NOTHING;
	int modeMask = 1<<mode;

	DiagMode ^= modeMask;
	return NOTHING;
}

#include <El/Modules/modules.hpp>

/*
static const GameNular ObjNular[]={};
*/
static const GameFunction ObjUnary[]=
{
	GameFunction(GameNothing,"diag_drawmode",SetDiagDrawMode,GameString),
	GameFunction(GameNothing,"diag_toggle",SetDiagToggle,GameString),
};
static const GameOperator ObjBinary[]=
{
	GameOperator(GameNothing,"diag_enable",function,SetDiagEnable,GameString,GameBool),
};


INIT_MODULE(GameStateObj, 3)
{
	GGameState.NewOperators(ObjBinary,sizeof(ObjBinary)/sizeof(*ObjBinary));
	GGameState.NewFunctions(ObjUnary,sizeof(ObjUnary)/sizeof(*ObjUnary));
	//GGameState.NewFunctions(ObjNular,sizeof(ObjNular)/sizeof(*ObjNular));
};

#endif

/*!
\patch 1.27 Date 10/17/2001 by Ondra
- Fixed: Random crash during scene LOD management.
*/

int Scene::AdjustComplexity( SortObjectList &objs )
{
	ADD_COUNTER(adjCo,1);
	int totalComplexity = 0;
	for( int i=0; i<objs.Size(); i++ )
	{
		SortObject *oi=objs[i];
		Object *obj=oi->object;
		if (!obj)
		{
			Fail("No obj in SortObject info");
			continue;
		}
		LODShape *shape=oi->shape;
		if (oi->forceDrawLOD>=0)
		{
			oi->drawLOD=oi->forceDrawLOD;
		}
		else
		{
			int drawLevel=LevelFromDistance2
			(
				shape,oi->distance2,
				obj->Scale(),obj->Direction(),_camera->Direction()
			);
			if( drawLevel!=LOD_INVISIBLE )
			{
#if _ENABLE_CHEATS
				if (DiagDrawModeState!=DDMNormal)
				{
					int geom = -1;
					switch (DiagDrawModeState)
					{
						case DDMGeometry:
							geom=shape->FindGeometryLevel();
							break;
						case DDMViewGeometry:
							geom=shape->FindViewGeometryLevel();
							break;
						case DDMFireGeometry:
							geom=shape->FindFireGeometryLevel();
							break;
						case DDMRoadway:
							geom=shape->FindRoadwayLevel();
							break;
						case DDMPaths:
							geom=shape->FindPaths();
							break;
					}
					if( geom>=0 ) drawLevel=geom;
				}
#endif
			}
			oi->drawLOD = drawLevel;
		}
		// check number of faces in given level
		if (oi->drawLOD!=LOD_INVISIBLE)
		{
			oi->passNum=obj->PassNum(oi->drawLOD);
#if _ENABLE_CHEATS
			if (CHECK_DIAG(DETransparent))
			{
				// all geometries are drawn transparent
				if (oi->drawLOD==shape->FindGeometryLevel())
				{
					if (oi->passNum<2) oi->passNum = 2;
				}
			}
#endif
			totalComplexity += obj->GetComplexity(oi->drawLOD,*obj);
		}

	}
	return totalComplexity;
}


static int ShadowFactor(Scene *scene)
{
	float addLightsFactor=scene->MainLight()->NightEffect();
	float skyCoef=floatMin(scene->GetLandscape()->SkyThrough(),0.6f);
	float shadowFactor=skyCoef+0.1f;
	return toIntFloor(shadowFactor*(1-addLightsFactor)*255);
}

int Scene::AdjustShadowComplexity( SortObjectList &objs )
{
	#if 1
	int totalComplexity = 0;

	int shadowFactorI=ShadowFactor(this);

	for( int i=0; i<objs.Size(); i++ )
	{
		SortObject *oi=objs[i];
		Object *obj=oi->object;
		LODShape *shape = oi->shape;
		if (!shape) continue;
		if
		(
			!(shape->Special()&NoShadow) && oi->distance2<Square(_shadowFogMaxRange)
			&& shadowFactorI>=12 && obj->CastShadow()
		)
		{
			int level=LevelShadowFromDistance2
			(
				shape,oi->distance2,
				obj->Scale(),obj->Direction(),_mainLight->ShadowDirection()
			);
			if( level!=LOD_INVISIBLE )
			{
				level=shape->FindNearestWithoutProperty(level,"lodnoshadow");
				if( level<0 ) level=LOD_INVISIBLE;
			}
			oi->shadowLOD=level;
		}
		else
		{
			oi->shadowLOD=LOD_INVISIBLE;
		}
		if (oi->shadowLOD!=LOD_INVISIBLE)
		{
			// check number of faces in given level
			Shape *level = shape->Level(oi->shadowLOD);
			totalComplexity += level->NFaces();
		}
	}
	return totalComplexity;
	#else

	float addLightsFactor=MainLight()->NightEffect();
	float skyCoef=floatMin(GetLandscape()->SkyThrough(),0.6f);
	float shadowFactor=skyCoef+0.1f;
	int shadowFactorI=toIntFloor(shadowFactor*(1-addLightsFactor)*255);

	for( int i=0; i<objs.Size(); i++ )
	{
		SortObject *oi=objs[i];
		Object *obj=oi->object;
		LODShape *shape = oi->shape;
		if
		(
			!(shape->Special()&NoShadow) && oi->distance2<Square(_shadowFogMaxRange)
			&& shadowFactorI>=12
		)
		{
			int level=0;
			if( level!=LOD_INVISIBLE )
			{
				level=shape->FindNearestWithoutProperty(level,"lodnoshadow");
				if( level<0 ) level=LOD_INVISIBLE;
			}
			oi->shadowLOD=level;
		}
		else
		{
			oi->shadowLOD=LOD_INVISIBLE;
		}
	}
	return 1000;
	#endif
}

// TODO: make global
inline void CheckMinMaxIter( Vector3 &min, Vector3 &max, Vector3Par val )
{
	#if __ICL
	if( min[0]>val[0] ) min[0]=val[0];
	if( max[0]<val[0] ) max[0]=val[0];
	if( min[1]>val[1] ) min[1]=val[1];
	if( max[1]<val[1] ) max[1]=val[1];
	if( min[2]>val[2] ) min[2]=val[2];
	if( max[2]<val[2] ) max[2]=val[2];
	#else
	if( min[0]>val[0] ) min[0]=val[0];
	else if( max[0]<val[0] ) max[0]=val[0];
	if( min[1]>val[1] ) min[1]=val[1];
	else if( max[1]<val[1] ) max[1]=val[1];
	if( min[2]>val[2] ) min[2]=val[2];
	else if( max[2]<val[2] ) max[2]=val[2];
	#endif
}

static bool FarEnoughForOcclusion( const SortObject *oi )
{
	// do not occlude things that are very near
	// the test would be very slow and is very like to fail
	const float occNearest = 20;
	if (oi->distance2>Square(occNearest+50)) return true;
	else
	{
		float distNear = oi->distance2*InvSqrt(oi->distance2) - oi->radius;
		if (distNear>occNearest) return true;
	}
	return false;
}

#if !DENSITY_LOD

void Scene::AdjustComplexity()
{
	PROFILE_SCOPE(sceAC);

	float oldLodInvWidth = _lodInvWidth;

	//float minLodInvWidth = Glob.config.lodCoef*2/(GEngine->Width()*Square(maxScaleDownCoef));
	//float maxLodInvWidth = Glob.config.lodCoef*2/(GEngine->Width()*Square(minScaleDownCoef));
	// we are adjusting lodInvWidth
	// we aim to be on line given by points
	// (MinTargetFrameDuration,minLodInvWidth)
	// (MaxTargetFrameDuration,maxLodInvWidth)
	// l is _lodInvWidth	
	// t is estimated time
	// calculate line equation: lineL*l+lineT*t+lineC = 0
	// calculate line normal
	float lMin = _minLodInvWidth;
	float lMax = _maxLodInvWidth;


	/*
	if (EnableHWTL)
	{
		//lMin = 0.05;
		//lMax = 0.05;
		lMin = 0.01;
		lMax = 0.01;
	}
	*/
	float tMin = _minTargetFrameDuration;
	float tMax = _maxTargetFrameDuration;
	
	float lineL = tMin - tMax;
	float lineT = lMax - lMin;
	// normalize lineL +  lineT
	float invLineTLSize = InvSqrt(lineL*lineL+lineT*lineT);
	lineL *= invLineTLSize;
	lineT *= invLineTLSize;

	float lineC = -lineL*lMin-lineT*tMin;
	#define DIAG_LOD 0
	#if DIAG_LOD
	LogF("t range %.1f..%.1f",tMin,tMax);
	LogF("l range %.3f..%.3f",lMin,lMax);
	#endif

	int targetGeom = Glob.config.maxObjects*75;
	int complexity = 0;
	int treshold = 8;
	for( int iters=5; --iters>=0;)
	{
		// calculate complexity
		complexity = AdjustComplexity(_drawObjects);
		complexity += AdjustShadowComplexity(_drawObjects);
		// include landscape complexity
		complexity += toLargeInt( Square(Glob.config.horizontZ*(1.0f/50)) );

		float frameDuration=GEngine->GetAvgFrameDuration(4);

		// estimate duration of this frame
		float avgComplexity = 0;
		for (int i=0; i<NStoreComplexities; i++)
		{
			avgComplexity += _lastComplexity[i];
		}
		avgComplexity *= 1.0f/NStoreComplexities;
		int estDuration = 200; // no estimate yet
		if (avgComplexity>0)
		{
			estDuration = toLargeInt( frameDuration * complexity/avgComplexity );
		}
		// estimate some minimal complexity this computer is able to render
		//float maxDuration = toLargeInt( complexity/float(targetGeom)*150 );
		//float triCoef = 150; // good estimation for GeForce
		float triCoef = 200; // good estimation for Voodoo2
		int maxDuration = toLargeInt( complexity/float(targetGeom)*triCoef );

		saturateMin(estDuration,maxDuration);
		
		// react to quick change in complexity
		// estimate frame rate based on previous complexity and time
		
		// calculate l and t
		// l is _lodInvWidth, t is estimated time
		// line normal is (lineL,lineT)
		float l = _lodInvWidth;
		float t = estDuration;
		float dist = lineL*l+lineT*t+lineC;
		// check distance from line
		// note: we might be in area out of tmin,tmax
		// find nearest point on line

		// old special case:
		/*
		int tDiff=estDuration-TargetFrameDuration;
		LogF("old %d new %.2f",tDiff,dist);
		*/
		#if DIAG_LOD
		LogF("  t %.1f, l %.3f, dist %.2f",t,l,dist);
		#endif
		float tDiff = dist*200;

		if (tDiff>treshold)
		{
			if (_lodInvWidth>=_maxLodInvWidth ) break; // fast path - min reached
			treshold = 4; // allow smoother change
			// scale - we are too slow
			float change=tDiff*-0.005f; // change is negative
			saturate(change,-0.2f,+0.2f);
			_lodInvWidth /= Square(1+change); // TODO: avoid division
			//LogF("  ADJUST DOWN %.3f",_lodInvWidth);
			//LogF("  change %.2f, complext %d",change,complexity);
			if (_lodInvWidth>_maxLodInvWidth )
			{
				// extreme reached - limit
				//LogF("  SAT   %.3f",_lodInvWidth);
				_lodInvWidth = _maxLodInvWidth;
				complexity = AdjustComplexity(_drawObjects);
				complexity += AdjustShadowComplexity(_drawObjects);
    		complexity += toLargeInt( Square(Glob.config.horizontZ*(1.0f/50)) );
				break;
			}
		}
		else if (tDiff<-treshold)
		{
			if (_lodInvWidth<=_minLodInvWidth) break; // fast path - max reached
			treshold = 4; // allow smoother change
			// scale - we are too fast
			float change=tDiff*-0.005f; // change is possitive
			saturate(change,-0.2f,+0.2f);

			_lodInvWidth /= Square(1+change); // TODO: avoid division

			//LogF("  ADJUST UP   %.3f",_lodInvWidth);
			//LogF("  change %.2f, complext %d",change,complexity);
			if (_lodInvWidth<_minLodInvWidth)
			{
				// extreme reached - limit
				_lodInvWidth= _minLodInvWidth;
				//LogF("  SAT   %.3f",_lodInvWidth);
				complexity = AdjustComplexity(_drawObjects);
				complexity += AdjustShadowComplexity(_drawObjects);
    		complexity += toLargeInt( Square(Glob.config.horizontZ*(1.0f/50)) );
				break;
			}
		}
		else
		{
			// wanted state reached - terminate loop
			break;
		}
	}


	#if 0
	GlobalShowMessage
	(
		100,"Last better %g, worse %g",
		Glob.uiTime-lastScaleBetterTime,
		Glob.uiTime-lastScaleWorseTime
	);
	#endif
	if (oldLodInvWidth>_lodInvWidth)
	{
		// going better - check for oscilation
		if (_lastScaleWorseTime>Glob.uiTime-5)
		{
			// avoid change in this direction
			// restore state
			_lodInvWidth =oldLodInvWidth;
			//LogF("  OSC   %.3f",_lodInvWidth);
			complexity = AdjustComplexity(_drawObjects);
			complexity += AdjustShadowComplexity(_drawObjects);
		}
		_lastScaleBetterTime = Glob.uiTime;
	}
	if (oldLodInvWidth<_lodInvWidth)
	{
		// going worse - check for oscilation
		//
		if (_lastScaleBetterTime>Glob.uiTime-0.5)
		{
			// avoid change in this direction
			// restore state
			_lodInvWidth =oldLodInvWidth;
			//LogF("  OSC   %.3f",_lodInvWidth);
			complexity = AdjustComplexity(_drawObjects);
			complexity += AdjustShadowComplexity(_drawObjects);
		}
		_lastScaleWorseTime = Glob.uiTime;
	}

	ADD_COUNTER(compX,complexity);

	//_lodInvWidth=Glob.config.lodCoef*2/(GEngine->Width()*Square(_scaleDownCoef));

	// store complexity to complexity history
	for (int i=1; i<NStoreComplexities; i++)
	{
		_lastComplexity[i-1] = _lastComplexity[i];
	}
	_lastComplexity[NStoreComplexities-1] = complexity;

  #if _ENABLE_REPORT
		float avgComplexity = 0;
		for (int i=0; i<NStoreComplexities; i++)
		{
			avgComplexity += _lastComplexity[i];
		}
		avgComplexity *= 1.0f/NStoreComplexities;
    #if 0
    GlobalShowMessage(500,"Complexity %8d, ~ %8.0f",complexity,avgComplexity);
    #endif
  #endif
}
float Scene::GetSmokeGeneralization() const
{
  return _lodInvWidth*0.1f;
}

#else

static inline float CoveredArea(Object *obj, float dist2, float oScale)
{
  LODShape *shape = obj->GetShape();
  if (!shape) return 0;
  float radius = shape->BoundingSphere()*oScale;

  Camera *cam = GScene->GetCamera();
  // return area in pixels
  float areaK = cam->InvLeft()*cam->InvTop()*GEngine->Width()*GEngine->Height();

  const float maxArea = 0.5f;
  // if (Square(radius)/dist2>maxArea)
  if (Square(radius)>dist2*maxArea)
  {
    return maxArea*areaK;
  }
  return Square(radius)*areaK/dist2;
}

static int CmpCoveredAreaObj( const SortObjectItem *p1, const SortObjectItem *p0 )
{
	// the first object is the nearest one
	const SortObject *o0 = *p0;
	const SortObject *o1 = *p1;
	float dif= (
    CoveredArea(o0->object,o0->distance2,o0->object->Scale())-
    CoveredArea(o1->object,o1->distance2,o1->object->Scale())
  );
	if( dif<0 ) return -1;
	if( dif>0 ) return +1;
  // make sure ordering is stable
  dif = o0->distance2-o1->distance2;
	if( dif<0 ) return +1;
	if( dif>0 ) return -1;
	return CmpShapeObj(p1,p0);
}

static inline int Complexity(LODShape *lShape, int level)
{
  return lShape->Level(level)->NFaces();
}

static int FindLevelWithComplexity(LODShape *lShape, float complexity, float maxDif)
{
  Assert(complexity>=0);
  int bestI = -1;
  float bestDif = maxDif;
  for (int i=0; i<lShape->NLevels(); i++)
  {
    float resol = lShape->Resolution(i);
    if (resol>900) break;
    int lComplex = Complexity(lShape,i);
    float dif = fabs(complexity-lComplex);
    if (bestDif>dif)
    {
      bestDif = dif;
      bestI = i;
    }
  }
  return bestI;
}

static int FindShadowLevelWithComplexity(LODShape *lShape, float complexity, float maxDif)
{
  int i = FindLevelWithComplexity(lShape,complexity,maxDif);
  if (i<0) return i;
	return lShape->FindNearestWithoutProperty(i,"lodnoshadow");
}

void Scene::AdjustComplexity()
{
  #if _ENABLE_CHEATS
  static int displayComplexityLimit = 100000;
  #endif

	PROFILE_SCOPE(sceAC);
  // set _lodInvWidth in case any needs it
  // development only: it should not be used when Density Lod system is active
	_lodInvWidth = (_minLodInvWidth+_maxLodInvWidth)*0.5f;

	int shadowFactorI=ShadowFactor(this);


  #if _ENABLE_CHEATS
  static float maxDensity = 0.3; // max. 5 polygons per pixel wanted
  static float shadowAreaCoef = 1.0f/32;
  //static float invShadowAreaCoef = 1/shadowAreaCoef;
	if( GInput.GetCheat2ToDo(DIK_LBRACKET) )
	{
		maxDensity /= 1.2f;
	}
	if( GInput.GetCheat2ToDo(DIK_RBRACKET) )
	{
		maxDensity *= 1.2f;
	}

	if( GInput.GetCheat1ToDo(DIK_LBRACKET) )
	{
		shadowAreaCoef /=1.5f;
    //invShadowAreaCoef = 1/shadowAreaCoef;
	}
	if( GInput.GetCheat1ToDo(DIK_RBRACKET) )
	{
		shadowAreaCoef *=1.5f;
    //invShadowAreaCoef = 1/shadowAreaCoef;
	}
  #else
  const float maxDensity = 0.3; // max. 5 polygons per pixel wanted
  const float shadowAreaCoef = 1.0f/32;
  //const float invShadowAreaCoef = 1/shadowAreaCoef;
  #endif

  // calculate total covered area
  float totalArea = 0;
  // sum complexity of objects that are excluded from lod management
  int complexityUsed = 0;
	for( int i=0; i<_drawObjects.Size(); i++ )
	{
		SortObject *oi=_drawObjects[i];
		Object *obj = oi->object;
		if (oi->forceDrawLOD>=0)
		{
      // count complexity as used
      if (oi->forceDrawLOD!=LOD_INVISIBLE)
      {
        complexityUsed += obj->GetComplexity(oi->forceDrawLOD,*obj);
      }
		}
		else
		{
			float area = CoveredArea(obj,oi->distance2,obj->Scale());
      totalArea += area;
		}

    LODShape *shape = obj->GetShape();
		if
		(
			shape && !(shape->Special()&NoShadow) && oi->distance2<Square(_shadowFogMaxRange)
			&& shadowFactorI>=12 && obj->CastShadow()
		)
    {
			float area = CoveredArea(obj,oi->distance2,obj->Scale());
      totalArea += area*shadowAreaCoef;
    }
    else
    {
      oi->shadowLOD = LOD_INVISIBLE;
    }
	}


  // if total area is zero, there are no controlled visible objects and density does not matter
  // we have total area, we know how much complexity we want - wa may calculate density now
  const int wantedComplexity = 30000;
  complexityUsed += toLargeInt( Square(Glob.config.horizontZ*(1.0f/50)) );

	int complexity = complexityUsed;

  float density = (wantedComplexity-complexity)/floatMax(totalArea,1e-20);
  saturate(density,0,maxDensity);

#if _ENABLE_REPORT
  float density0 = density;
  float totalArea0 = totalArea;
#endif
  // note: if too many polygons were already used, density may be negative

  float minArea = totalArea*1e-4f;
  // first pass: select object which use lod 0
  // such objects often do not use available complexity
	for( int i=0; i<_drawObjects.Size(); i++ )
	{
		SortObject *oi=_drawObjects[i];
		Object *obj = oi->object;
		if (oi->forceDrawLOD<0)
		{
      LODShape *lShape = obj->GetShape();
			float area = CoveredArea(obj,oi->distance2,obj->Scale());
      float oComplexity = area*density;
      int level = FindLevelWithComplexity(lShape,oComplexity,oComplexity*1.5f+200);
      if (level==0)
      {
        int levelComplexity = Complexity(lShape,level);
        #if _ENABLE_CHEATS
        if (abs(oComplexity-levelComplexity)>displayComplexityLimit)
        {
          LogF(
            "%s - complexity %d, wanted %.0f",
            (const char *)lShape->GetName(),levelComplexity,oComplexity
          );
        }
        #endif
        complexity += levelComplexity;
			  oi->passNum = obj->PassNum(level);
        oi->drawLOD = level;

        totalArea -= area;
      }
		}
    else
    {
      if (oi->forceDrawLOD!=LOD_INVISIBLE)
      {
			  oi->passNum = obj->PassNum(oi->forceDrawLOD);
      }
      oi->drawLOD = oi->forceDrawLOD;
    }
    // similiar for shadows
		if (oi->shadowLOD<0)
		{
      LODShape *lShape = obj->GetShape();
			float area = CoveredArea(obj,oi->distance2,obj->Scale());
      float oComplexity = area*density*shadowAreaCoef;
      int level = FindShadowLevelWithComplexity(lShape,oComplexity,oComplexity*1.5f+200);
      if (level==0) // lShape->_minShadow should be used here instead
      {
        int levelComplexity = Complexity(lShape,level);
        complexity += levelComplexity;
        oi->shadowLOD = level;

        totalArea -= area*shadowAreaCoef;
      }
		}

	}

  // when we rendered almost everything, there is no need to update density any more
  if (totalArea>minArea)
  {
    density = (wantedComplexity-complexity)/floatMax(totalArea,1e-20);
    saturate(density,0,maxDensity);
  }

  // selects lods for all other objects
	for( int i=0; i<_drawObjects.Size(); i++ )
	{
		SortObject *oi=_drawObjects[i];
		Object *obj = oi->object;
		if (oi->drawLOD<0)
		{
      LODShape *lShape = obj->GetShape();
			float area = CoveredArea(obj,oi->distance2,obj->Scale());
      float oComplexity = area*density;
      int level = FindLevelWithComplexity(lShape,oComplexity,oComplexity*1.5f+200);
      if (level<0)
      {
        oi->drawLOD = LOD_INVISIBLE;
      }
      else
      {
        int levelComplexity = Complexity(lShape,level);
        #if _ENABLE_CHEATS
        if (abs(oComplexity-levelComplexity)>displayComplexityLimit)
        {
          LogF(
            "%s - complexity %d, wanted %.0f",
            (const char *)lShape->GetName(),levelComplexity,oComplexity
          );
        }
        #endif
        complexity += levelComplexity;
			  oi->passNum = obj->PassNum(level);
        oi->drawLOD = level;
      }
		}
		if (oi->shadowLOD<0)
		{
      LODShape *lShape = obj->GetShape();
			float area = CoveredArea(obj,oi->distance2,obj->Scale());
      float oComplexity = area*density*shadowAreaCoef;
      int level = FindShadowLevelWithComplexity(lShape,oComplexity,oComplexity*1.5f+200);
      if (level<0)
      {
        oi->shadowLOD = LOD_INVISIBLE;
      }
      else
      {
        int levelComplexity = Complexity(lShape,level);
        #if _ENABLE_CHEATS
        if (abs(oComplexity-levelComplexity)>displayComplexityLimit)
        {
          LogF(
            "%s - complexity %d, wanted %.0f",
            (const char *)lShape->GetName(),levelComplexity,oComplexity
          );
        }
        #endif
        complexity += levelComplexity;
        oi->shadowLOD = level;
      }
		}
	}

	ADD_COUNTER(compX,complexity);

	// store complexity to complexity history
	for (int i=1; i<NStoreComplexities; i++)
	{
		_lastComplexity[i-1] = _lastComplexity[i];
	}
	_lastComplexity[NStoreComplexities-1] = complexity;

  #if _ENABLE_REPORT
		float avgComplexity = 0;
		for (int i=0; i<NStoreComplexities; i++)
		{
			avgComplexity += _lastComplexity[i];
		}
		avgComplexity *= 1.0f/NStoreComplexities;
    #if 1
    GlobalShowMessage(
      500,
      "Complex %8d, ~ %8.0f, density %0.5f (%0.5f) < %0.5f, totArea %0.f (%0.f), check %0.1f, cUsed %d, shadC %.2f",
      complexity,avgComplexity,density,density0,maxDensity,
      totalArea,totalArea0,density0*totalArea0,complexityUsed,1/shadowAreaCoef
    );
    #endif
  #endif
}
float Scene::GetSmokeGeneralization() const
{
	return (_minLodInvWidth+_maxLodInvWidth)*0.5f*0.1f;
}
#endif


void Scene::DrawObjectsAndShadowsPass1()
{
	PROFILE_SCOPE(oPas1);
	// select first objects - those with highest visual priority

	int s=0,t=0;
	//LogF("Before not used %d",_drawObjects.Size());
	for( ; s<_drawObjects.Size(); s++ )
	{
		// TODO: optimize
		SortObject *sObj=_drawObjects[s];
		if( !sObj->notUsed )
		{
			if( s!=t ) _drawObjects[t]=sObj;
			t++;
		}
		else sObj->object->SetInList(NULL); // removed from the list
	}
	_drawObjects.Resize(t);

	//LogF("After not used %d",_drawObjects.Size());

	#if DO_STAT
		Alpha.Clear();
		Opaque.Clear();
		Shadow.Clear();
	#endif

	// remove all objects that should not be used

	AdjustComplexity();

	// copy objects to working list (mergers)
	// do not copy objects that are not drawn
	{
		//Ref<SortObject> *src=_drawObjects.Data();
		//int i=_drawObjects.Size();
		// skip what is not moved
		//LogF("Before compact: %d",_drawObjects.Size());
		// make smaller only when really necessary
		int objNeed = _drawObjects.Size();
		int objHave = _drawMergers.MaxSize();
		if (objNeed>=objHave)
		{
			_drawMergers.Reserve(objNeed,objNeed);
		}
		else if (objNeed*2<objHave && objHave>1024)
		{
			// no need to keep it big now - make it smaller
			_drawMergers.Realloc(objNeed);
		}
		_drawMergers.Resize(0);
		for( int s=0; s<_drawObjects.Size(); s++ )
		{
			SortObject *sObj=_drawObjects[s];
			if( sObj->drawLOD==LOD_INVISIBLE && sObj->shadowLOD==LOD_INVISIBLE )
			{
				continue;
			}
			if (!sObj->object || !sObj->shape) continue;
			_drawMergers.Add(sObj);
		}
	}
	// if we sort by shape, we group object with similiar textures
	// sort by distance
	
#if _ENABLE_CHEATS
	if( GInput.GetCheat2ToDo(DIK_H) )
	{
		EnableObjOcc=!EnableObjOcc;
		GlobalShowMessage(500,"Object occlusions %s",EnableObjOcc ? "On":"Off");
	}
#endif

	if( EnableObjOcc )
	{
		// sort only what needs to checked/drawn for occlusion
		// this will remove especially cloudlets from occlusion testing
		// it also helps to maintain _drawMergers sorted
		// because sort is performed in different array
		// therefore _drawMergers sort may be performed incrementally
		AUTO_STATIC_ARRAY(Ref<SortObject>,occSort,2048);
		for (int i=0; i<_drawMergers.Size(); i++)
		{
			SortObject *oi=_drawMergers[i];
			if( oi->drawLOD==LOD_INVISIBLE ) continue;
			Object *obj=oi->object;
			if (!obj) continue;
			LODShape *shape=oi->shape;
			if (!shape) continue;

			if
			(
				shape->CanBeOccluded() ||
				shape->CanOcclude() && obj->OcclusionView()
			)
			{
				occSort.Add(_drawMergers[i]);
			}
		}
		//QSort(_drawMergers.Data(),_drawMergers.Size(),CmpRevDistObj);
		//GlobalShowMessage(100,"Occ %d, draw %d",occSort.Size(),_drawMergers.Size());
		QSort(occSort.Data(),occSort.Size(),CmpRevDistObj);
		// before drawing anything draw cockpit occlusion
		// check if we are in internal view
		#if 1
		if (GWorld->GetCameraType()==CamInternal && !GWorld->GetCameraEffect())
		{
			Object *obj = GWorld->CameraOn();
			if (obj)
			{
				int view = obj->InsideViewGeomLOD(CamInternal);
				if (view!=LOD_INVISIBLE && view>=0)
				{
					// draw that particular view geometry
					obj->AnimateComponentLevel(view);
					// if object can occlude something, render its components

					LODShape *lShape = obj->GetShape();

					Shape *shape = lShape->Level(view);
					const ConvexComponents *cc = lShape->GetConvexComponents(view);
					if (cc)
					{
						Occlusions->RenderShape(*obj,shape,*cc,ClipAll);
					}
					else
					{
						LogF("Inconsistent view geom in %s",(const char *)obj->GetDebugName());
					}
					obj->DeanimateComponentLevel(view);
				}
			}
		}
		#endif


		// draw occlusions front to back

		PROFILE_SCOPE(o1Occ);

		//for( int i=_drawMergers.Size(); --i>=0; )
		//{
		//	SortObject *oi=_drawMergers[i];
		for( int i=occSort.Size(); --i>=0; )
		{
			SortObject *oi = occSort[i];
			Object *obj = oi->object;
			LODShape *shape = oi->shape;
			if (oi->drawLOD==LOD_INVISIBLE) continue;
			// select view geometry
			// no occlusions for or by camera vehicle
			if (obj==GWorld->CameraOn()) continue;
			// if object is small and simple if should not be tested nor rendered
			bool occluded=false;
			// check object distance
			// if the objects is very near, do not check occlusion

			if( shape->CanBeOccluded() )
			{
				// do not occlude things that are very near
				// the test would be very slow and is very like to fail
				if (FarEnoughForOcclusion(oi))
				{
					Vector3 minMax[2];
					//obj->Animate(oi->drawLOD);
					obj->AnimatedMinMax(oi->drawLOD,minMax);
					//obj->Deanimate(oi->drawLOD);
					// check if object is occluded by objects
					if( !Occlusions->TestBBox(*oi->object,minMax,oi->orClip) )
					{
						occluded=true;
						ADD_COUNTER(oOcc,1);
					}
				}
			}
			if( !occluded )
			{
				// TODO: remove OcclusionView, implement using CanOcclude()
				if( shape->CanOcclude() && obj->OcclusionView() )
				{
					Shape *view=shape->ViewGeometryLevel();
					if( !view ) continue;
					// test if object is near enough to make a some occlusion
					float areaNom = Square(oi->radius*_camera->InvLeft());
					float areaDenom = oi->distance2;
					//if (areaNom/areaDenom<0.001)
					if (areaNom>0.001*areaDenom)
					{
						obj->AnimateViewGeometry();
						// if object can occlude something, render its components
						Occlusions->RenderShape
						(
							*oi->object,view,shape->GetViewComponents(),oi->orClip
						);
						obj->DeanimateViewGeometry();
					}
				}
			}
			else
			{
				// occluded - do not draw, do not render into occlusion buffer
				oi->drawLOD=LOD_INVISIBLE;
			}
		}
	}

	QSort(_drawMergers.Data(),_drawMergers.Size(),CmpShapeObj);
	// first of all draw non-alpha objects

	#if DRAW_OBJS
	{
		PROFILE_SCOPE(o1Drw);
		for( int i=0; i<_drawMergers.Size(); i++ )
		{
			SortObject *oi=_drawMergers[i];
			LODShape *shape=oi->object->GetShape();
			if( oi->drawLOD==LOD_INVISIBLE ) continue;
			Assert( oi->drawLOD>=0 );
			Shape *sShape=shape->LevelOpaque(oi->drawLOD);
			if( !sShape ) continue;
			if( oi->passNum<=1 )
			{
				oi->object->Draw(oi->drawLOD,oi->orClip,*oi->object);
				#if DO_STAT
					Opaque.Count(shape->Name());
				#endif
			}
		}
	}
	#endif
}

void Scene::DrawObjectsAndShadowsPass2()
{
	PROFILE_SCOPE(oPas2);
	// must be sorted by distance
	// then draw shadows
	// note: some polygons can not be ordered
	int nDraw=_drawMergers.Size();
	#if DRAW_OBJS
	if( GetLandscape() ) for( int i=0; i<nDraw; i++ )
	{
		SortObject *oi=_drawMergers[i];
		if( oi->shadowLOD==LOD_INVISIBLE ) continue;
		//DrawExShadow(oi->object,oi->shadowLOD,*oi->object);
		DrawExShadow(oi);
	}
	#endif
	// draw alpha parts of roads
	GEngine->FlushQueues();
	GEngine->EnableReorderQueues(false);
	QSort(_drawMergers.Data(),_drawMergers.Size(),CmpRevDistObj);
	// last draw alpha objects (not roads - they are already drawn)
	for( int i=0; i<nDraw; i++ )
	{
		SortObject *oi=_drawMergers[i];
		LODShape *shape=oi->object->GetShape();
		if( oi->drawLOD==LOD_INVISIBLE ) continue;
		Assert( oi->drawLOD>=0 );
		Shape *sShape=shape->LevelOpaque(oi->drawLOD);
		if( !sShape ) continue;
		if( oi->passNum==2 )
		{
			oi->object->Draw(oi->drawLOD,oi->orClip,*oi->object);
			#if DO_STAT
				Alpha.Count(shape->Name());
			#endif
		}
	}
	GEngine->EnableReorderQueues(true);
}

#include <Es/Common/filenames.hpp>

void Scene::DrawObjectsAndShadowsPass3()
{
	#if DRAW_OBJS
	int i,nDraw=_drawMergers.Size();
	// pass 3 - draw cockpits after all external alpha effects
	for( i=0; i<nDraw; i++ )
	{
		SortObject *oi=_drawMergers[i];
		LODShape *shape=oi->object->GetShape();
		if( oi->drawLOD!=LOD_INVISIBLE )
		{
			Assert( oi->drawLOD>=0 );
			Shape *sShape=shape->LevelOpaque(oi->drawLOD);
			if( !sShape ) continue; // TODO: more consistent BUG solution
			if( oi->passNum==3 )
			{
#if 0 //_ENABLE_CHEATS
			
				if (CHECK_DIAG(DEXxx) && GWorld->GetCameraType()==CamInternal)
				{
					int view = oi->object->InsideViewGeomLOD(GWorld->GetCameraType());
					if (view!=LOD_INVISIBLE && view>=0)
					{
						oi->object->Draw(view,ClipAll,*oi->object);
					}
				}
				else
#endif
				{
					oi->object->Draw(oi->drawLOD,oi->orClip,*oi->object);
				}
			}
		}
	}
	#endif

	#if DO_STAT
		LogF("Alpha objects");
		Alpha.Report();
		LogF("Opaque objects");
		Opaque.Report();
		LogF("Shadow objects");
		Shadow.Report();
	#endif


	#if 0
	// diagnostic pass
	static PackedColor textColor(Color(1,0.9,0.8,0.5));
	for( i=0; i<nDraw; i++ )
	{
		SortObject *oi=_drawMergers[i];
		LODShape *shape=oi->object->GetShape();
		if( oi->drawLOD==LOD_INVISIBLE ) continue;
		// if best lod is selected, do not draw
		if( oi->drawLOD==0 ) continue;
		// draw text info about LOD level
		// project on screen
		Vector3 camPos = GScene->ScaledInvTransform()* (*oi->object).Position();
		if( camPos.Z()<0.5 ) continue;
		float invCZ = 1/camPos.Z();
		Vector3 camScreen(camPos.X()*invCZ,camPos.Y()*invCZ,invCZ);

		//LogF("c %.3f,%.3f",camScreen.X(),camScreen.Y());
		//Ref<Font>

		Ref<Font> font = GEngine->LoadFont("fonts\\tahomaB24");

		int level = oi->drawLOD;
		char name[256];
		GetFilename(name,shape->Name());
		GEngine->DrawTextF
		(
			(camScreen.X()+1)*0.5,(-camScreen.Y()+1)*0.5,0.015,font,textColor,
			"%s: %.1f",
			name,shape->Resolution(level)
		);
	}
	#endif

}

void Scene::DrawReflections( const WaterLevel &water )
{
}

void Scene::DrawCollisionStar( Vector3Par pos, float size, PackedColor color )
{
	if( _collisionStar.NotNull() && size>=0.01 )
	{
		Ref<Object> star=new ObjectColored(_collisionStar->GetShape(),-1);
		star->SetTransform(M4Identity);
		star->SetPosition(pos);
		star->SetScale(size*4);
		star->SetConstantColor(color);
		GetLandscape()->ShowObject(star);
	}
}

void Scene::DrawDiagModel
(
	Vector3Par pos, LODShapeWithShadow *shape,
	float size, PackedColor color
)
{
	Ref<Object> star=new ObjectColored(shape,-1);
	star->SetTransform(M4Identity);
	star->SetPosition(pos);
	star->SetScale(size/shape->BoundingSphere());
	star->SetConstantColor(color);
	GetLandscape()->ShowObject(star);
}


void Scene::DrawVolumeLight
(
	LODShapeWithShadow *shape, PackedColor color,
	const Frame &pos, float size
)
{
	Ref<Object> object=new ObjectColored(shape,-1);
	// draw light shape
	object->SetTransform(pos.Transform());
	object->SetPosition(object->PositionModelToWorld(shape->BoundingCenter()));
	//ObjectForDrawing(object,-1,(Glob.config.reflections&REFL_OBJECT)!=0);

	object->SetConstantColor(color);
	object->SetScale(size);
	ObjectForDrawing(object);
}

/*!
\patch 1.24 Date 8/16/2001 by Ondra
- Fixed: Random crash during shadow rendering.
*/

void Scene::DrawExShadow( SortObject *oi )
{
	// calculate position of the shadow of the object top
	Object *obj = oi->object;
	if (!obj) return;
	int level = oi->shadowLOD;
	LODShapeWithShadow *shape=oi->shape;
	Vector3Val objPos=obj->Position();
	Point3 shadowPos=objPos;
	const FrameBase &pos=*obj;
	
	if( !ShadowPos(objPos,shadowPos,_mainLight) ) return;
	
	//shape->CreateShadow();
	// convert shape into shadow
	//if( shape->Shadow() )
	Ref<Shape> sShape = obj->PrepareShadow(level,shadowPos,pos);
	// check bbox clipping and occlusion

	// per object clipping possible - bounding sphere was recalculated
	if( !sShape ) return; // no shadow
	if( sShape->NPos()<=0 ) return;

	float bRadius = sShape->BSphereRadius()*pos.Scale();
	Vector3Val bCenterM = sShape->BSphereCenter();
	Vector3Val bCenter=pos.PositionModelToWorld(bCenterM);
	// try to clip bounding sphere
	// if whole bounding sphere is out, object is already skipped
	if( GScene->GetCamera()->IsClipped(bCenter,bRadius,1) )
	{
		return;
	}
	ClipFlags clipFlags=GScene->GetCamera()->MayBeClipped(bCenter,bRadius,1);
	/**/
	if( EnableObjOcc )
	{
		bool occluded=false;
		if( shape->CanBeOccluded() )
		{
			if (FarEnoughForOcclusion(oi))
			{
				// check occlusion - bbox recalculation
				Vector3 minMax[2];
				// TODO: optimize
				Vector3Val pos0=sShape->Pos(0);
				minMax[0]=pos0;
				minMax[1]=pos0;
				for( int i=0; i<sShape->NPos(); i++ )
				{
					Vector3Val posI=sShape->Pos(i);
					CheckMinMaxIter(minMax[0],minMax[1],posI);
				}

				// check if object is occluded by objects
				if( !Occlusions->TestBBox(pos,minMax,clipFlags) )
				{
					occluded=true;
				}
			}
		}
		if( occluded )
		{
			return;
		}
	}

	// first draw shadows of some proxies?
	/**/
	for (int i=0; i<obj->GetProxyCount(level); i++)
	{
		if (!obj->CastProxyShadow(level,i)) continue;
		Matrix4 trans = obj->Transform(),invTrans = obj->GetInvTransform();

		LODShapeWithShadow *pshape = NULL;
		Object *proxy = obj->GetProxy(pshape,level,trans,invTrans,*obj,i);
		if (!proxy) continue;
		// note: it is not sure we need to draw shadows of all proxies
		// proxy shadow must be always calculated dynamically
		// we do not want to draw proxies of complex objects like vehicle crews

		// = proxy->GetShapeOnPos(trans.Position());
		if (!pshape) continue;
		float dist2 = trans.Position().Distance2(GetCamera()->Position());
		int plevel=LevelShadowFromDistance2
		(
			pshape,dist2,trans.Scale(),
			trans.Direction(),GetCamera()->Direction()
		);
		if( plevel!=LOD_INVISIBLE )
		{
			plevel=pshape->FindNearestWithoutProperty(plevel,"lodnoshadow");
			if( plevel<0 ) plevel=LOD_INVISIBLE;
		}
		if( plevel==LOD_INVISIBLE ) continue;

		proxy->Animate(plevel);

		FrameWithInverse pframe(trans,invTrans);
		if (proxy->RecalcShadow(plevel,pframe,true,pshape))
		{
			LODShape *shadowShape = pshape->Shadow();
			Ref<Shape> ret = shadowShape->LevelOpaque(plevel);
			// we can draw shape ret now as shadow
			proxy->DrawShadow(ret,shadowPos,ClipAll,pframe);
		}

		proxy->Deanimate(plevel);

	}
	/**/

	
	//obj->DrawShadow(shadow,shadowPos,ClipAll,pos);
	obj->DrawShadow(sShape,shadowPos,clipFlags,pos);

	#if DO_STAT
		Shadow.Count(oi->shape->Name());
	#endif
}

// different global variables

void ManCleanUp();

void ClearShapes()
{
	VehicleTypes.Clear();
	//WeaponInfos.Clear();
	RoadTypes.Clear();
	// check if all shapes have been released
	#if !_RELEASE
	for( int i=0; i<Shapes.Size(); i++ )
	{
		LODShapeWithShadow *shape=Shapes[i];
		if( shape )
		{
			Log
			(
				"Shape %s not released (%d refs).",
				shape->Name(),
				shape->RefCounter()
			);
		}
	}
	#endif
	Shapes.Clear();

	MagazineTypes.Clear();
	WeaponTypes.Clear();
}


bool Scene::ShadowPos
(
	Vector3Par pos, Vector3 &aprox, LightSun *light
)
{ // 
	Vector3Val dir=light->ShadowDirection();
	if( dir.Y()>0 )
	{
		return false; // shadow casted up - no real shadow
	}
	const float maxShadow = 300;
	float t = GetLandscape()->IntersectWithGround(&aprox,pos,dir,0,maxShadow*1.1);
	if (t>maxShadow)
	{
		// no intersection
		return false;
	}
	if (t<=0.01f)
	{
		// up -> on surface required
		aprox[1] = GLandscape->SurfaceY(aprox[0],aprox[2]);
	}
	return true;
}

