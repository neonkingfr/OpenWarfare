#ifndef __ENGGLIDE_HPP
#define __ENGGLIDE_HPP

#include <Es/common/win.h>

#include <Es/Containers/staticArray.hpp>
#include "winpch.hpp"
#include "types.hpp"
#include "engine.hpp"
#include "txtGlide.hpp"

/* define old GrVertex - porting from Glide2X */
struct GlideTmuVertex
{
	float sow,tow; // nothing else used for texture mapping
};

/*
  float sx,sy,sz,rhw; // D3DFVF_XYZRHW 
  DWORD color; // D3DFVF_DIFFUSE
  DWORD specular; // D3DFVF_SPECULAR
  float tu0,tv0; // D3DFVF_TEXCOORDSIZE2(0) 
  float tu1,tv1; // D3DFVF_TEXCOORDSIZE2(1) 
*/

#include "tlVertex.hpp"

// GlideVertex and TLVertex have exactly same format

#include <Es/Containers/array.hpp>

#define OPT_STATS 0

#if OPT_STATS
	void StateChangeRequest();
	void StateChangeDone();
#else
	#define StateChangeRequest()
	#define StateChangeDone()
#endif

enum AlphaCombineMode;
enum AlphaBlendMode;
enum ColorCombineMode;

class EngineGlide: public Engine
{
	typedef Engine base;

	protected:

	TextBankGlide *_textBank;
	HWND _hwndApp;
	int _w,_h;
	int _refresh;

	static bool _initDone;
	GrContext_t _context;
	bool _tripleBuffering;
	bool _fogCoordExtension;
	bool _hasPassthrough;

	private:
	Color _textColor;

	int _nTMUs;
	int _tmuActive;

	// actual texture parameters
	InitPtr<const MipmapLevelGlideInfo> _lastMipmap; // per triangle mipmapping
	InitPtr<const TextureGlide> _lastTexture; // per pixel mipmapping
	int _lastLevel; // best mip-map level loaded
	
	// other actual parameters
	int _lastSpec;
	bool _lastClampU[MaxTMUs],_lastClampV[MaxTMUs];
	bool _lastBilin[MaxTMUs];
	int _currentBias;

	float _gamma;

	//StaticArray<GlideVertex> _meshCache; // see BeginMesh,EndMesh
	TLVertexTable *_mesh; // mesh data used during rendering

	// draw multiple triangles with single Glide call
	enum VFormatSet {SingleTex,DetailTex,SpecularTex};
	VFormatSet _formatSet;

	void SetMultiTexturing( VFormatSet format );

	private:
	// smart state change
	int _alphaTestValue;
	int _alphaCombineMode;
	int _alphaBlendMode;
	int _colorCombineMode;
	int _fogMode;
	int _depthFunction;
	bool _depthMask;
	bool _aaEnabled;

	void SetAlphaTestValue( int value )
	{
		StateChangeRequest();
		if( _alphaTestValue!=value ) DoSetAlphaTestValue(value);
	}
	void SetAlphaCombine( AlphaCombineMode mode )
	{
		StateChangeRequest();
		if( _alphaCombineMode!=mode ) DoSetAlphaCombine(mode);
	}
	void SetAlphaBlend( AlphaBlendMode mode )
	{
		StateChangeRequest();
		if( _alphaBlendMode!=mode ) DoSetAlphaBlend(mode);
	}
	void SetColorCombine( ColorCombineMode mode )
	{
		StateChangeRequest();
		if( _colorCombineMode!=mode ) DoSetColorCombine(mode);
	}
	void SetFogMode( int mode )
	{
		StateChangeRequest();
		if( _fogMode!=mode ) DoSetFogMode(mode);
	}
	void SetDepthFunction( int value )
	{
		StateChangeRequest();
		if( _depthFunction!=value ) DoSetDepthFunction(value);
	}
	void SetDepthMask( bool value )
	{
		StateChangeRequest();
		if( _depthMask!=value ) DoSetDepthMask(value);
	}
	void EnableAA( bool value )
	{
		StateChangeRequest();
		if( _aaEnabled!=value ) DoEnableAA(value);
	}

	// used to compensate for far objects
	bool CanZBias() const {return true;}
	bool ZBiasExclusion() const {return true;}

	virtual bool IsWBuffer() const {return true;}
	virtual bool CanWBuffer() const {return false;}

	void DoSetAlphaTestValue( int value );
	void DoSetAlphaCombine( AlphaCombineMode mode );
	void DoSetAlphaBlend( AlphaBlendMode mode );
	void DoSetColorCombine( ColorCombineMode mode );
	void DoSetFogMode( int mode );
	void DoSetDepthFunction( int value );
	void DoSetDepthMask( bool value );
	void DoEnableAA( bool value );
		
	int FogEnable() const;
	int FogDisable() const;

	private:
	void InitGlide(bool fullscreen);
	void DeinitGlide();
	
	public:
	// constructor - init 
	EngineGlide
	(
		HINSTANCE hInst, HINSTANCE hPrev, int sw,
		int width, int height, bool fullscreen=true
	);
	
	// destructor
	virtual ~EngineGlide();

	int NTMUs() const {return _nTMUs;}
	void ActivateDetailTexturing();
	void ActivateSpecularTexturing();
	void ActivateTMU( int aTmu );
	void DownloadMipmap( TextureGlide *texture, int level );
	void PrepareState( int specFlags );

	void PrepareClamping
	(
		TextureGlide *texture, int level, bool clampU, bool clampV
	);

	int HowLongIdle();
	void DrawDecal
	(
		Vector3Par pos, float rhw, float sizeX, float sizeY, PackedColor col,
		const MipInfo &mip, int specFlags
	);
	void PrepareTriangle
	(
		const MipInfo &mip, int specFlags
	);
	void TextureDestroyed( Texture *tex ); // may be current texture

	void Draw2D
	(
		const Draw2DPars &pars, const Rect2DAbs &rect, const Rect2DAbs &clip=Rect2DClipAbs
	);
	void DrawPoly
	(
		const MipInfo &mip, const Vertex2DPixel *vertices, int n,
		const Rect2DPixel &clipRect=Rect2DClipPixel, int specFlags=DefSpecFlags2D
	);
	void DrawPoly
	(
		const MipInfo &mip, const Vertex2DAbs *vertices, int n,
		const Rect2DAbs &clipRect=Rect2DClipAbs, int specFlags=DefSpecFlags2D
	);
	void DrawLine
	(
		const Line2DAbs &line,
		PackedColor c0, PackedColor c1,
		const Rect2DAbs &clip=Rect2DClipAbs
	);
	
	void DrawPolygon( const VertexIndex *i, int n );
	void DrawSection
	(
		const FaceArray &face, Offset beg, Offset end
	);
	
	void DrawPoints( int beg, int end );
	void DrawLine( int beg, int end );
	
	void PrepareMesh( int spec );
	void BeginMesh( TLVertexTable &mesh, int spec ); // convert all mesh vertices
	void EndMesh( TLVertexTable &mesh ); // forget mesh

	AbstractTextBank *TextBank(){return _textBank;}
	void Clear( bool clearZ=true, bool clear=true, PackedColor color=PackedColor(0) );
	void InitDraw( bool clear=false, PackedColor color=PackedColor(0) ); // Begin scene
	void FinishDraw();

	void Recreate(); // Begin scene

	void Pause();
	void Restore();
	void StopAll();

	void FogColorChanged( ColorVal fogColor );

	void SetGamma( float g );
	float GetGamma() const {return _gamma;}

	void SaveConfig();
	void LoadConfig();

	void Activate();
	void Deactivate();
	void Resize( int x, int y, int w, int h );

	//bool CaptureMessage( HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam );

	// charW<0 means proportional size
	protected:
	void TextState(); // prepare rendering state

	public:
	
	bool SwitchRes(int w, int h, int bpp);
	bool SwitchRefreshRate(int refresh);
	bool SwitchWindowed(bool windowed) {return false;}

	void ListResolutions(FindArray<ResolutionInfo> &ret);
	void ListRefreshRates(FindArray<int> &ret);

	float ZShadowEpsilon() const {return 0.01;} // bias used for shadows
	float ZRoadEpsilon() const {return 0.005;} // bias used for roads
	float ObjMipmapCoef() const {return 1.5;}
	float LandMipmapCoef() const {return 1.0;}
	bool ShadowsFirst() const {return false;}
	bool SortByShape() const {return true;}
	
	void SetBias( int value );
	int GetBias() {return _currentBias;}
	virtual void GetZCoefs(float &zAdd, float &zMult);

	RString GetDebugName() const;
	int Width() const {return _w;}
	int Height() const {return _h;}
	int PixelSize() const {return 16;} // 16 or 32 bit mode?
	int RefreshRate() const;
	bool CanBeWindowed() const {return false;}
	bool IsWindowed() const {return false;}

	// be a little bit conservative, so that rounding errors do not crash you
	#if 1
	int MinGuardX() const {return 0;}
	int MinGuardY() const {return 0;}
	int MaxGuardX() const {return _w;}
	int MaxGuardY() const {return _h;}
	#else
	int MinGuardX() const {return -64;}
	int MinGuardY() const {return -64;}
	int MaxGuardX() const {int x=_w+64;saturateMin(x,2047);return x;}
	int MaxGuardY() const {int y=_h+64;saturateMin(y,2047);return y;}
	#endif

	#if 0
	int MinSatX() const {return 0;}
	int MinSatY() const {return 0;}
	int MaxSatX() const {return _w;}
	int MaxSatY() const {return _h;}
	#else
	int MinSatX() const {return -512;}
	int MinSatY() const {return -512;}
	int MaxSatX() const {int x=_w+512;saturateMin(x,2047);return x;}
	int MaxSatY() const {int y=_h+512;saturateMin(y,2047);return y;}
	#endif
	/*
	
	int MinSatX() const {return -2047*0.95;}
	int MinSatY() const {return -2047*0.95;}
	int MaxSatX() const {return +2047*0.95;}
	int MaxSatY() const {return +2047*0.95;}
	*/

	// functions usefull for TextBankGlide
	void CopyMipmap( TextureGlide *texture, int tgtLevel, int srcLevel );
	
	GRAMOffset TexMinAddress( int tmu ) const;
	GRAMOffset TexMaxAddress( int tmu ) const;

	static bool Detect();
	
	// functions for frame rate control
	int FrameTime() const;
	
	int AFrameTime() const {return FrameTime();}
};

#endif

