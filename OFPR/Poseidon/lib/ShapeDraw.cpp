


// Poseidon - shape management
// (C) 1998, SUMA

#include "wpch.hpp"
#include "Shape.hpp"
#include "engine.hpp"
#include "global.hpp"
#include "scene.hpp"
#include <El/QStream/QBStream.hpp>
#include <El/ParamFile/paramFile.hpp>
#include "textbank.hpp"
#include "keyInput.hpp"
#include "dikCodes.h"
#include "animation.hpp"
#include "tlVertex.hpp"

#include <El/Common/perfLog.hpp>

#include "edges.hpp"
#include "SpecLods.hpp"

#include <Es/Algorithms/qsort.hpp>
#include <Es/Common/filenames.hpp>

//#include <malloc.h>

#include "data3d.h"

#include "mapTypes.hpp"

#ifdef _MSC_VER
	#pragma warning( disable: 4355 )
#endif

inline bool IsSpec( float resolution, float spec )
{
	if( fabs(resolution-spec)<spec*1e-3 ) return true;
	return false;
}

extern bool EnableHWTL;
extern bool EnablePIII;









#if _ENABLE_PERFLOG
	extern bool LogStatesOnce;
#endif

extern bool DisableTextures;

bool EnableHWTLState = true;

/*!
\patch 1.33 Date 11/27/2001 by Ondra
 - Fixed: Dark notepad after load or Alt-Tab.
\patch 1.46 Date 3/1/2002 by Ondra
- Fixed: Bug in HW T&L code: Material change was sometimes not recognized
when polygons shared texture. This caused some trees were rendered darker.
*/

void Shape::Draw
(
	class IAnimator *matSource,
	const LightList &lights,
	ClipFlags clip, int spec,
	const Matrix4 &transform, const Matrix4 &invTransform
)
{
	#ifndef ACCESS_ONLY
	// if engine has T&L interface, use it
	// cannot use T&L on some surface types (OnSurface?)

	bool tlAble = (spec&OnSurface)==0 && (clip&ClipUser0)==0;

	Engine *engine=GEngine;

#if _ENABLE_CHEATS
	// toggle T&L on/off
	if (engine->GetTL())
	{
		if (GInput.GetCheat2ToDo(DIK_Q))
		{
			EnableHWTLState = !EnableHWTLState;
			GlobalShowMessage(500,"DX T&L %s",EnableHWTLState ? "On":"Off");
		}
	}
#endif

	if (engine->GetTL() && EnableHWTLState && tlAble && _buffer )
	{
		#if 0 //_ENABLE_PERFLOG
		if (LogStatesOnce)
		{
			LogF("Draw shape - HW T&L");
		}
		#endif
		// use engine T&L
		// TODO: use matSource to retrieve materials during rendering
		// use split shape instead of shape

		if( BeginFaces()<EndFaces() && NSections()>0)
		{
			GEngine->PrepareMeshTL(lights,transform,spec);
			if (spec&(OnSurface|IsOnSurface))
			{
				engine->SetBias(0x10);
			}
			else
			{
				int bias = (spec&ZBiasMask)/ZBiasStep;
				// max. bias value is 3
				engine->SetBias(bias*5);
			}
			// prepare sections (if neccessary)
			// prepare lights and materials
			//GEngine->EnableSunLight(true);
					
			ADD_COUNTER(poly,NFaces());
			ADD_COUNTER(TLd3d,NFaces());

			// check if shape is dynamic or not
			bool dynamic = matSource->GetAnimated(*this);
			engine->BeginMeshTL(*this,spec,dynamic);
			// check first face properties
			int secBeg = -1;
			int secEnd = -1;
			Texture *secTexture = (Texture *)-1;
			int secSpecial = -1;
			int secMaterial = -1;
			TexMaterial *secSurfMat = NULL;

			for (int i=0; i<NSections(); i++)
			{
				const ShapeSection &sec = GetSection(i);
				if (sec.properties.Special()&(IsHidden|IsHiddenProxy)) continue;

				if (secBeg<0)
				{
					secBeg = i;
					secEnd = i+1;
					secTexture = sec.properties.GetTexture();
					secSpecial = sec.properties.Special();
					secMaterial = sec.material;
					secSurfMat = sec.surfMat;
				}
				else if
				(
					sec.properties.GetTexture()==secTexture &&
					sec.properties.Special()==secSpecial &&
					sec.material==secMaterial &&
					sec.surfMat==secSurfMat &&
					i==secEnd
				)
				{
					// extend section
					secEnd=i+1;
				}
				else
				{
					// flush section
					TLMaterial mat;
					#if _ENABLE_PERFLOG
					if (LogStatesOnce)
					{
						LogF("Material %d",GetSection(secBeg).material);
					}
					#endif
					matSource->GetMaterial(mat,GetSection(secBeg).material);
					//GEngine->SetMaterial(mat,lights,spec);
					GetSection(secBeg).PrepareTL(mat,lights,spec);
					GEngine->DrawSectionTL(*this,secBeg,secEnd);
					// open another section
					secBeg = i;
					secEnd = i+1;
					secTexture = sec.properties.GetTexture();
					secSpecial = sec.properties.Special();
					secMaterial = sec.material;
					secSurfMat = sec.surfMat;
				}


			}
			if (secEnd>secBeg)
			{
				int matIndex = GetSection(secBeg).material;
				TLMaterial mat;
				#if _ENABLE_PERFLOG
				if (LogStatesOnce)
				{
					LogF("Material %d",matIndex);
				}
				#endif
				matSource->GetMaterial(mat,matIndex);
				// flush section
				GetSection(secBeg).PrepareTL(mat,lights,spec);

				GEngine->DrawSectionTL(*this,secBeg,secEnd);
			}


			engine->EndMeshTL(*this);
		}
	}
	else
	{
		#if 0 //_ENABLE_PERFLOG
		if (LogStatesOnce)
		{
			LogF("Draw shape - SW T&L");
		}
		#endif
		DoAssert
		(
			_face._sections.Size()==0 ||
			_face._sections[_face._sections.Size()-1].end==EndFaces()
		);

		// custom T&L
		_face.Draw(matSource,lights,*this,clip,spec,transform,invTransform);
	}
	#endif
}


LODShapeWithShadow *ShapeBank::New
(
	const char *name, bool reversed, bool shadow
)
{
	char lowName[128];
	strcpy(lowName,name);
	strlwr(lowName);

	int remNeeded=0;
	int remAvoid=0;
	if( reversed ) remNeeded|=REM_REVERSED;
	else remAvoid|=REM_REVERSED;
	if( !shadow ) remNeeded|=REM_NOSHADOW;
	else remAvoid|=REM_NOSHADOW;
	Compact();

	RStringB lowNameB = lowName;
	for( int i=0; i<Size(); i++ )
	{
		LODShapeWithShadow *shape=Get(i);
		Assert( shape );
		//if( !shape ) continue;
		if ( shape->GetName()!=lowNameB ) continue;
		if ( (shape->Remarks()&remNeeded)!=remNeeded ) continue;
		if ( (shape->Remarks()&remAvoid)!=0 ) continue;
		return shape;
	}
	LODShapeWithShadow *shape=new LODShapeWithShadow(lowName,reversed);
	shape->SetRemarks(shape->Remarks()|remNeeded);
	if( !shadow )
	{
		shape->OrSpecial(NoShadow);
	}
	Add(shape);
	return shape;
}


void Shape::ConvertToVBuffer(VBType type)
{
	if (NVertex()<=0) return;
	if (!EnableHWTL) return;
	DoAssert(!_buffer);
	if (!_buffer)
	{
		_buffer = GEngine->CreateVertexBuffer(*this,type);
	}
}

void Shape::ReleaseVBuffer()
{
	_buffer.Free();
}

void ShapeBank::ReleaseAllVBuffers()
{
	for( int i=0; i<Size(); i++ )
	{
		LODShapeWithShadow *shape=Get(i);
		if (!shape) continue;
		for (int l=0; l<shape->NLevels(); l++)
		{
			Shape *level = shape->Level(l);
			level->ReleaseVBuffer();
		}
	}
}

void LODShape::OptimizeRendering()
{
	LODShape *shape = this;
	bool reload = false;
	for (int l=0; l<shape->NLevels(); l++)
	{
		Shape *level = shape->Level(l);
		bool optimizeHW = false;
		bool optimizeSSE = false;
		if (EnableHWTL)
		{
			optimizeHW = level->NVertex()>0 && level->NFaces()>0;
			//optimize = level->NVertex()>=16 && level->NFaces()>=8;
			//optimize = true; //level->NVertex()>=16 && level->NFaces()>=8;
		}
		if (EnablePIII)
		{
			optimizeSSE = !shape->GetAllowAnimation() && level->NVertex()>=16;
		}
		if (optimizeHW || optimizeSSE)
		{
			ClipFlags globalLight=shape->GetAndHints()&ClipLightMask;
			if( globalLight!=(shape->GetOrHints()&ClipLightMask) ) globalLight=0;
			switch (globalLight)
			{
				case ClipLightCloud: case ClipLightSky:
				case ClipLightStars: case ClipLightLine:
					optimizeHW = optimizeSSE = false;
			}
			if (level->Special()&(IsLight|OnSurface))
			{
				optimizeHW = optimizeSSE = false;
			}
		}
		// no optimize on geometry levels
		if (shape->Resolution(l)>900)
		{
			// ignore some special levels
			if
			(
				l==shape->FindFireGeometryLevel() ||
				l==shape->FindGeometryLevel() ||
				l==shape->FindViewGeometryLevel() ||
				l==shape->FindViewPilotGeometryLevel() ||
				l==shape->FindViewCargoGeometryLevel() ||
				l==shape->FindViewCommanderGeometryLevel() ||
				l==shape->FindViewGunnerGeometryLevel() ||
				l==shape->FindMemoryLevel() ||
				l==shape->FindPaths() ||
				l==shape->FindLandContactLevel() ||
				l==shape->FindRoadwayLevel()
			)
			{
				continue;
			}
		}
		#if USE_QUADS
		if (optimizeSSE)
		{
			if (level->PosQuad().Size()<=0)
			{
				level->ConvertToQArray();
			}
		}
		#endif
		if (optimizeHW)
		{
			const RStringB tentString("tent");
			VBType type = shape->GetAllowAnimation() ? VBDynamic : VBStatic;
			if (shape->GetPropertyDammage()==tentString) type = VBDynamic;
			level->ConvertToVBuffer(type);
		}
		else
		{
			// check if there are some normal arrays
			if (level->NVertex()==0)
			{
				// back conversion not possible
				#if USE_QUADS
				if (level->GetVertexBuffer() || level->PosQuad().Size()>0)
				#else
				if (level->GetVertexBuffer())
				#endif
				{
					// shape is not empty
					// we need to reload shape
					reload = true;
				}
			}
		}
	} // for (l)
	if (reload)
	{
		// TODO: reload
		Fail("Reload not possible");
		RptF("Reloading %s",shape->Name());
		/*
		char name[256];
		strcpy(name,shape->Name());
		bool reversed = (shape->Remarks()&REM_REVERSED)!=0;
		shape->Load(name,reversed)
		*/

	}
}

void ShapeBank::OptimizeAll()
{
	// first of all: flush all vertex buffers
	// recreate vertex buffers
	if (EnableHWTL)
	{
		ReleaseAllVBuffers();
		LogF("ShapeBank::OptimizeAll");
	}
	for( int i=0; i<Size(); i++ )
	{
		LODShapeWithShadow *shape=Get(i);
		if (!shape) continue;
		shape->OptimizeRendering();
	} // for (i)
	
}


ShapeBank Shapes;

// TODO: move to some header
void PrepareTexture( Texture *texture, float z2, int special, float areaOTex );


void Shape::PrepareTextures( float z2, int special ) const
{
	#ifndef ACCESS_ONLY
	// hint mipmap for all textures used on this shape
	for (int t=0; t<_textures.Size(); t++)
	{
		Texture *txt = _textures[t];
		if (txt)
		{
			float areaOTex = _areaOTex[t];
			PrepareTexture(txt,z2,special,areaOTex);
		}
	}
	#endif
}

