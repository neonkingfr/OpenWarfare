/*!
	\file
	Interface of Entity
*/

#ifdef _MSC_VER
#pragma once
#endif

#ifndef _VEHICLE_HPP
#define _VEHICLE_HPP

#include "object.hpp"
#include "soundsys.hpp"
#include "soundScene.hpp"
#include "handledList.hpp"

#include "aiTypes.hpp"

#include "animation.hpp"
#include "head.hpp"

// Simul is basic simulation class - no rendering

//! importance determines simulation precision
DEFINE_ENUM_BEG(SimulationImportance)
	SimulateCamera, //! most precise - camera is closely following it
	SimulateVisibleNear,SimulateVisibleFar, //! it is visible
	SimulateInvisibleNear,SimulateInvisibleFar, //! it is inivisible
	SimulateDefault //! no specific value
DEFINE_ENUM_END(SimulationImportance)

// most vehicles are visible objects


// autopilot state is used for autopilots in diffrent vehicles
enum AutopilotState
{
	AutopilotFar,AutopilotBrake,
	AutopilotNear,AutopilotAlign,AutopilotReached
};

struct FireResult
{
	float dammage; // 0..1
	float gain;
	float cost;
	float loan;
	int weapon;

	float Surplus() const {return gain-cost-loan;} // loan considered
	float CleanSurplus() const {return gain-cost;} // no loan considered

	FireResult() {Reset();}
	void Reset()
	{
		dammage=gain=cost=loan=0;
		weapon=0;
	}
};

TypeIsSimpleZeroed(FireResult);

enum Condition {ConditionGreen,ConditionYellow,ConditionRed};

struct FFEffects
{
	//float forceX,forceY;
	float engineFreq,engineMag;
	//float gunFreq,gunMag;
	//int gunCount;
	float stiffnessX,stiffnessY; // autocentering forces

	FFEffects();
};

struct AnimateTextureInfo
{
	AnimationAnimatedTexture animation;
	float animSpeed;
};
TypeIsMovable(AnimateTextureInfo)

class VehiclesDistributed;

//! Information shared between Entity object of the same type
class EntityType: public RefCount
{
	friend class Entity;
	protected:
	mutable int _refVehicles; // count vehicles of this type
	mutable bool _refVehiclesLocked;
	mutable Ref<LODShapeWithShadow> _shape;
	RString _shapeName;

	public: // tired of writing access functions
	InitPtr<ParamEntry> _par;
	InitPtr<EntityType> _parentType; // for uncertain determination
	float _accuracy;
	
	bool _shapeReversed;
	bool _shapeAutoCentered;
	bool _shapeAnimated;
	bool _useRoadwayForVehicles;

	RString _className;
	RString _simName;

	// some types are created without vehicle
	// and cannot be used as full type info
	int _scopeLevel;
	// corresponding config file entry
	//Ref<const ParamClass> _parClass;

	AutoArray<AnimateTextureInfo> _animateTextures;
	RefArray<AnimationType> _animations;

	public:
	EntityType( const ParamEntry *param );
	~EntityType();

	void Init(const ParamEntry *param) {Load(*param);}
	virtual void Load(const ParamEntry &par);

	LODShapeWithShadow *GetShape() const {return _shape;}
	RString GetShapeName() const {return _shapeName;}

	float GetAccuracy() const {return _accuracy;}

	void VehicleAddRef() const; // vehicle created
	void VehicleRelease() const; // vehicle destroyed

	void VehicleLock() const; // disable releasing shape
	void VehicleUnlock() const; // enable releasing shape

	const ParamEntry &GetParamEntry() const;
	virtual void InitShape(); // after shape is loaded
	virtual void DeinitShape(); // before shape is unloaded

	void AttachShape( LODShapeWithShadow *shape ); // force some shape
	void ReloadShape( QIStream &f );

	RString GetName() const {return _className;}

	bool IsKindOf(const EntityType *predecessor) const;
	bool IsAbstract() const {return _scopeLevel<=0;}
	bool IsShaped() const {return _scopeLevel>=2;}

	//! some roadways (houses, guard towers) should be used for soldiers only
	bool GetUseRoadwayForVehicles() const {return _useRoadwayForVehicles;}
};

typedef EntityType VehicleNonAIType;

class ObjectTyped: public Object
{
	typedef Object base;

	Ref<EntityType> _type;

	public:
	ObjectTyped(LODShapeWithShadow *shape, const EntityType *type, int id);
	~ObjectTyped();

//	const EntityType *GetEntityType() const {return _type;}
	const EntityType *GetVehicleType() const {return _type;}
};

#include <Es/Containers/bankInitArray.hpp>

class VehicleTypeBank: public BankInitArray<EntityType>
{
	typedef BankArray<EntityType> base;

	protected:
	int Load( const char *name );
	
	public:
	void Preload(); // preload types, no shapes

	void LockAllTypes(); // disable automatic deleting types
	void UnlockAllTypes(); // enable automatic deleting types

	VehicleTypeBank();
	~VehicleTypeBank();

	EntityType *New( const char *name );
	EntityType *FindShape( const char *shape );	
	EntityType *FindShapeAndSimulation( const char *shape, const char *sim);	
};

extern VehicleTypeBank VehicleTypes;

DECL_ENUM(GroundType)

struct ContactPoint
{
	// generic contact point (common for landscape / objects)
	Vector3 pos; // world space position
	Vector3 dirOut; // and direction
	float under; // how much under surface
	Texture *texture; // what kind of surface
	GroundType type; // type of collision (water, solid...)
	Object *obj; // contact object (for transfering momentum)
};

TypeIsMovableZeroed(ContactPoint)

//! Point in which friction should be applied
/*!
Used in friction calculation, introduced in Thing class,
in future it should be used more.
See also functions Entity::ApplyForcesAndFriction and Entity::ConvertContactsToFrictions.
*/

struct FrictionPoint
{	
	Vector3 pos; //!< world space coordinates
	Vector3 outDir; //!< world space out-of-solid direction
	//! friction coeficient, determines how much of total friction
	//! is distributed to this point
	float frictionCoef;
	Object *obj; // which object causes the friction
};

TypeIsSimpleZeroed(FrictionPoint);

typedef StaticArrayAuto<ContactPoint> ContactArray;
typedef StaticArrayAuto<FrictionPoint> FrictionArray;

class IndicesCreateVehicle : public IndicesNetworkObject
{
	// derived directly form NetworkObject, not Object !!!
	typedef IndicesNetworkObject base;

public:
	int list;
	int type;
	int shape;
	int position;
	int name;
	int idVehicle;
	int hierParent;

	IndicesCreateVehicle();
	NetworkMessageIndices *Clone() const {return new IndicesCreateVehicle;}
	void Scan(NetworkMessageFormatBase *format);
};
class IndicesUpdateVehicle : public IndicesUpdateObject
{
	typedef IndicesUpdateObject base;

public:
	int targetSide;
	int animations;

	IndicesUpdateVehicle();
	NetworkMessageIndices *Clone() const {return new IndicesUpdateVehicle;}
	void Scan(NetworkMessageFormatBase *format);
};
class IndicesUpdatePositionVehicle : public IndicesNetworkObject
{
	typedef IndicesNetworkObject base;

public:
	/*
	int orientation;
	int position;
	int speed;
	int angMomentum;
	*/
	int entityUpdPos;
	int prec;

	IndicesUpdatePositionVehicle();
	NetworkMessageIndices *Clone() const {return new IndicesUpdatePositionVehicle;}
	void Scan(NetworkMessageFormatBase *format);
};

//! current in-landscape state of the vehicle
enum MoveOutState
{
	MOIn, //! is present in the lanscape
	MOMovingOut, //! should be moved out from the landscape, but is still in the normal list
	MOMovedOut //! is moved out from the landscape and is in the outVehicles list
};

//! any object that needs simulation
/*!
	Principal type for any simulated or active objects.
*/
class Entity: public Object
{
	typedef Object base;

	protected:

	float _simulationPrecision;
	float _simulationSkipped; // how much has been skipped
	//! last used precision - important for MP prediction
	SimulationImportance _prec;

	mutable bool _invDirty;
	mutable Matrix4 _invTransform;

	PackedColor _constantColor;

	InitPtr<VehiclesDistributed> _list; // which list contains this vehicle?

	Vector3 _speed;
	Vector3 _modelSpeed; // speed in model coordinates (updated in Move())
	Vector3 _angVelocity; // angular velocity (omega)
	Vector3 _angMomentum; // angular momentum (L)
	Matrix3 _invAngInertia; // inverse world-space inertia tensor (inv I)
	Vector3 _acceleration;

	Vector3 _impulseForce; // propagated collision result (1 sec long)
	Vector3 _impulseTorque;

	// diagnostics - draw all forces
	void AddForce
	(
		Vector3Par pos, Vector3Par force, Color color=HWhite
	);

	TargetSide _targetSide; // easy identification

	const Ref<EntityType> _type;

	// body constants
	// mass and inverse body space inertia tensor are parts of shape
	// body quantities
	// position and orientation are parts of Object

	// auxiliary quantities
	bool _objectContact,_landContact,_waterContact;
	bool _delete,_convertToObject;	
	bool _local;	// local / remote in network game

	SizedEnum<MoveOutState,char> _moveOutState; // moved someplace else
	OLink<Object> _hierParent; // parent in hierarchy

	Time _disableDammageUntil; // no dammage when emerged from far sim

	NetworkId _networkId;

	AutoArray<float> _animateTexturesTimes;	//!< Times for texture animations
	AutoArray<AnimationInstance> _animations;

	public:
	Entity
	(
		LODShapeWithShadow *shape, const EntityType *type, int id
	);
	~Entity();

	virtual void SetVarName(RString name) {}

	const char *GetName() const {return _type->GetName();}
	const EntityType *GetNonAIType() const {return _type;}

	const EntityType *GetVehicleType() const {return _type;}

	Vector3Val Speed() const {return _speed;} // camera needs to know object speed
	void SetSpeed( Vector3Par speed )
	{
		_speed=speed;
		DirectionWorldToModel(_modelSpeed,speed);
	} // camera needs to know object speed
	// ModelSpeed is updated in Move()
	Vector3Val ModelSpeed() const {return _modelSpeed;} // camera needs to know object speed

	Vector3Val ObjectSpeed() const {return _speed;} // virtual member of Object
	Object *GetHierachyParent() const {return _hierParent;} // parent in hierarchy

	virtual void PerformFF( FFEffects &effects );
	virtual void ResetFF();

	void SetConstantColor( PackedColor color ) {_constantColor=color;}
	PackedColor GetConstantColor() const;
	
	// IAnimator interface implementation
	void GetMaterial(TLMaterial &mat, int index) const;
	
	// more functions
	void SetList( VehiclesDistributed *list ){_list=list;}
	VehiclesDistributed *GetList() const {return _list;}

	Vector3Val AngVelocity() const {return _angVelocity;} // angular velocity (omega)
	void OrientationSurface();

	Vector3Val Acceleration() const {return _acceleration;}
	Vector3Val AngMomentum() const {return _angMomentum;}
	void SetAngMomentum( Vector3Par val ) {_angMomentum=val;}

	NetworkId GetNetworkId() const;
	void SetNetworkId(NetworkId &id) {_networkId = id;}
	bool IsLocal() const {return _local;}
	void SetLocal(bool local = true) {_local = local;}

	float GetAnimationPhase(RString animation) const;
	void SetAnimationPhase(RString animation, float phase);

	public:
	virtual void PlaceOnSurface(Matrix4 &trans); // place in steady position
	virtual SimulationImportance WorstImportance() const {return SimulateInvisibleFar;}
	virtual SimulationImportance BestImportance() const {return SimulateCamera;}

	void SetLastImportance(SimulationImportance prec){_prec=prec;}
	SimulationImportance GetLastImportance() const {return _prec;}

	SimulationImportance CalculateImportance(const Vector3 *viewerPos, int nViewers) const;
	bool EnableVisualEffects(SimulationImportance prec) const;

	void DisableDammageUntil( Time time ){_disableDammageUntil=time;}
	//void SwitchImportance( SimulationImportance importance ); // place in steady position

	void ApplySpeed( Matrix4 &result, float deltaT );
	void ApplyForces
	(
		float deltaT,
		Vector3Par force, Vector3Par torque,
		Vector3Par friction, Vector3Par torqueFriction,
		float staticFric=0
	);

	void ApplyForcesAndFriction
	(
		float deltaT,
		Vector3Par force, Vector3Par torque,
		const FrictionPoint *fric, int nFric
	);

	//! event handler - called in AddImpulse()
	virtual void OnAddImpulse(Vector3Par force, Vector3Par torque);

	void AddImpulse(Vector3Par force, Vector3Par torque);

	void AddImpulseNetAware(Vector3Par force, Vector3Par torque);

	virtual float Rigid() const {return 0.9f;} // how much energy is transfered in collision

	//virtual Vector3 InsideCameraDistance( CameraType camType ) const {return Vector3(0,0,0);}
	//virtual Vector3 InsideCameraDirection( CameraType camType ) const {return Vector3(0,0,1);}
	
	void SetTargetSide( TargetSide side ) {_targetSide=side;}
	//void SetTargetType( TargetType type ) {_targetType=type;}
	//TargetType GetTargetType() const {return _targetType;}
	TargetSide GetTargetSide() const {return _targetSide;}

	//bool IsNew() const {return _isNew;}
	bool ToDelete() const {return _delete;}
	bool ToMoveOut() const {return (MoveOutState)_moveOutState>=MOMovingOut;}
	bool IsMoveOutInProgress() const {return (MoveOutState)_moveOutState==MOMovingOut;}
	void CancelMoveOutInProgress();
	bool ToConvertToObject() const {return _convertToObject;}

	//void SetNew() {_isNew=true;}
	void SetDelete() {_delete=true;}
	void SetMoveOut(Object *parent);
	void SetMoveOutDone(Object *parent);
	void SetMoveOutFlag();
	void SetConvertToObject() {_convertToObject=true;}
	
	// access to world space transformation
	virtual Matrix4 WorldTransform() const;
	virtual Matrix4 WorldInvTransform() const;
	virtual Vector3 WorldSpeed() const;
	virtual bool IsInLandscape() const;

	virtual Matrix4 ProxyWorldTransform(const Object *obj) const;
	virtual Matrix4 ProxyInvWorldTransform(const Object *obj) const;


	//void ResetNew() {_isNew=false;}
	void ResetDelete() {_delete=false;}
	void ResetMoveOut();
	void ResetConvertToObject() {_convertToObject=false;}

	virtual void ResetStatus();

	// generic simulation helpers
	void ScanContactPoints
	(
		ContactArray &contacts, const Frame &moveTrans,
		SimulationImportance prec, float above, bool ignoreObjects=false
	);
	void ConvertContactsToFrictions
	(
		const ContactArray &contacts, FrictionArray &frictions,
		const Frame &moveTrans,
		Vector3 &offset, Vector3 &force, Vector3 &torque,
		float crash, float maxColSpeed2
	);

	virtual void StartFrame() {} // start frame - used for motion blur
	virtual void Simulate( float deltaT, SimulationImportance prec );

	virtual bool CastShadow() const;

	virtual void SimulateOptimized( float deltaT, SimulationImportance prec );
	void SimulateRest( float deltaT, SimulationImportance prec );

	__forceinline float SimulationPrecision() const {return _simulationPrecision;}
	void SetSimulationPrecision( float val ) {_simulationPrecision=val;}
	
	virtual void Sound( bool inside, float deltaT ){}
	virtual void UnloadSound(){}

	bool IsAnimated( int level ) const;
	bool IsAnimatedShadow( int level ) const;

	void Animate( int level );
	void Deanimate( int level );

	virtual bool MustBeSaved() const {return true;}

	virtual LSError Serialize(ParamArchive &ar);
	static Entity *CreateObject(ParamArchive &ar);

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	static Entity *CreateObject(NetworkMessageContext &ctx);
	void DestroyObject();
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx);

	// easier InvTransform calculation
	void CalculateInv() const;
	void InvDirty() const {_invDirty=true;}
	const Matrix4 &InvTransform() const
	{
		if( _invDirty ) _invDirty=false,CalculateInv();
		return _invTransform;
	}
	Matrix4 GetInvTransform() const // overload Object
	{
		if( _invDirty ) _invDirty=false,CalculateInv();
		return _invTransform;
	}

	// overload Frame set member
	void SetPosition( Vector3Par pos );
	void SetTransform( const Matrix4 &transform );

	void SetOrient( const Matrix3 &dir );
	void SetOrient( Vector3Par dir, Vector3Par up );
	void SetOrientScaleOnly( float scale );

	const Matrix4 &WorldToModel() const {return InvTransform();}
	const Matrix3 &DirWorldToModel() const {return InvTransform().Orientation();}
	
	Vector3 PositionWorldToModel( Vector3Par v ) const {return Vector3(VFastTransform,InvTransform(),v);}
	Vector3 DirectionWorldToModel( Vector3Par v ) const {return Vector3(VRotate,InvTransform(),v);}

	void PositionWorldToModel( Vector3 &res, Vector3Par v ) const {res.SetFastTransform(InvTransform(),v);}
	void DirectionWorldToModel( Vector3 &res, Vector3Par v ) const {res.SetRotate(InvTransform(),v);}

	USE_CASTING(base)
};

typedef Entity Vehicle;

typedef OLink<Vehicle> LinkVehicle;
typedef OLink<Entity> LinkEntity;

class AttachedOnVehicle: virtual public Frame
{
	protected:
	// lights can be relative to vehicle
	OLink<Object> _vehicle;
	Vector3 _pos,_dir; // relative to vehicle

	public:
	AttachedOnVehicle(){}
	AttachedOnVehicle
	(
		Object *object, Vector3Par pos, Vector3Par dir
	);
	~AttachedOnVehicle();

	Object *AttachedOn(){return _vehicle;}
	void SetAttachedPos( Vector3Par pos, Vector3Par dir ){_pos=pos,_dir=dir;}
	virtual void UpdatePosition();

	LSError Serialize(ParamArchive &ar);
};

// general simulation functions

#define G_CONST 9.8066f


void Friction( float &speed, float friction, float accel, float deltaT );
void Friction
(
	Vector3 &speed, Vector3Par friction, Vector3Par accel, float deltaT
);


#endif

