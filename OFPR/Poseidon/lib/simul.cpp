/*!
	\file
	Implementation of Entity
*/

#include "wpch.hpp"
#include "vehicle.hpp"
#include "pilotHead.hpp"
#include "world.hpp"
#include "global.hpp"
#include "landscape.hpp"
#include "scene.hpp"
#include "camera.hpp"
#include "animation.hpp"
#include "ai.hpp"
//#include "loadStream.hpp"
#include "network.hpp"
#include <El/Common/randomGen.hpp>
#include <El/Common/perfLog.hpp>
#include "gameStateExt.hpp"
#include "keyInput.hpp"
#include "SpecLods.hpp"
#include "network.hpp"
#include "tlVertex.hpp"
#include "diagModes.hpp"

#include "engine.hpp"

#if _DEBUG
	#define ARROWS 1
#endif

/*!
\patch_internal 1.45 Date 2/15/2002 by Jirka
- Added: support for animated textures added to config
*/

void Entity::Simulate( float deltaT, SimulationImportance prec )
{
	const EntityType *type = GetNonAIType();

	// animate textures
	for (int i=0; i<_animateTexturesTimes.Size(); i++)
	{
		float &phase = _animateTexturesTimes[i];
		phase += deltaT * type->_animateTextures[i].animSpeed;
		while (phase >= 1.0f) phase -= 1.0f;
	}
}

void Entity::SimulateOptimized( float deltaT, SimulationImportance prec )
{
	float step = SimulationPrecision();
	_simulationSkipped+=deltaT;
	if( _simulationSkipped>=step )
	{
		Simulate(step,prec);
		_simulationSkipped-=step;
		ADD_COUNTER(simO,1);
	}
}
void Entity::SimulateRest( float deltaT, SimulationImportance prec )
{
	_simulationSkipped+=deltaT;
	if( _simulationSkipped>1e-4 )
	{
		Simulate(_simulationSkipped,prec);
		_simulationSkipped=0;
		ADD_COUNTER(simO,1);
	}
}

static Vector3 AddFriction( Vector3Val accel, Vector3Val friction )
{
	Vector3 res=accel+friction;
	if( res[0]*accel[0]<0 ) res[0]=0;
	if( res[1]*accel[1]<0 ) res[1]=0;
	if( res[2]*accel[2]<0 ) res[2]=0;
	return res;
}

FFEffects::FFEffects()
{
	engineFreq=engineMag=0;
	stiffnessX = 0.3f,stiffnessY = 0.3f; // autocentering forces
}

void Entity::PerformFF( FFEffects &effects )
{
	effects=FFEffects();
}

void Entity::ResetFF()
{
}

EntityType::EntityType( const ParamEntry *param )
{
	_par = const_cast<ParamEntry *>(param);
	_refVehicles = 0;
	_refVehiclesLocked = false;
	// 
	_shapeReversed = true;
	_shapeAutoCentered = true;
	_useRoadwayForVehicles = false;
}

void EntityType::Load(const ParamEntry &par)
{
	_accuracy=1.0;

	_className=par.GetName();
	_simName=par>>"simulation";

	RString model=par>>"model";
	_shapeName=::GetShapeName(model);

	_shapeReversed=false;
	_shapeAutoCentered = true;
	_shapeAnimated = true;
	_useRoadwayForVehicles = false;

	if( par.FindEntry("reversed") )
	{
		_shapeReversed=par>>"reversed";
	}
	if( par.FindEntry("autocenter") )
	{
		_shapeAutoCentered=par>>"autocenter";
	}
	if( par.FindEntry("animated") )
	{
		_shapeAnimated=par>>"animated";
	}
	if( par.FindEntry("useRoadwayForVehicles") )
	{
		_useRoadwayForVehicles = par>>"useRoadwayForVehicles";
	}


	// check if there is some parent
	const ParamClass *cls=dynamic_cast<const ParamClass *>(&par);
	if( cls )
	{
		const char *baseName=cls->GetBaseName();
		if( baseName )
		{
			// load parent type
			_parentType=VehicleTypes.New(baseName);
		}
	}
}

EntityType::~EntityType()
{
	VehicleUnlock();
	Assert (_refVehicles==0);
}

bool EntityType::IsKindOf(const EntityType *predecessor) const
{
	const EntityType *cur = this;
	while (cur)
	{
		if (cur == predecessor) return true;
		//if( !strcmpi(cur->GetName(),predecessor->GetName()) ) return true;
		cur = cur->_parentType;
	}
	return false;
}

const ParamEntry &EntityType::GetParamEntry() const
{
	return *_par;
}

void EntityType::InitShape()
{
	// animate textures
	const ParamEntry *array = _par->FindEntry("animateTextures");
	if (array)
	{
		int n = array->GetSize() / 2, index = 0;
		_animateTextures.Realloc(n);
		_animateTextures.Resize(n);
		for (int i=0; i<n; i++)
		{
			RString name = (*array)[index++];
			_animateTextures[i].animation.Init(_shape, name, NULL);
			float animTime = (*array)[index++];
			_animateTextures[i].animSpeed = animTime > 0 ? 1.0f / animTime : 0;
		}
	}

	array = _par->FindEntry("Animations");
	if (array)
	{
		int n = array->GetEntryCount();
		_animations.Realloc(n);
		_animations.Resize(n);
		for (int i=0; i<n; i++)
		{
			_animations[i] = AnimationType::CreateObject(array->GetEntry(i), _shape);
		}
	}
}

void EntityType::DeinitShape()
{
}

void EntityType::ReloadShape( QIStream &f )
{
	if( _refVehicles>0 )
	{
		DeinitShape();
		_shape->Reload(f,_shapeReversed);
		InitShape();
	}
}

void EntityType::AttachShape( LODShapeWithShadow *shape )
{
	if( _refVehicles!=0 )
	{
		LogF("Shape %s changed when used.",(const char *)shape->Name());
	}
	_shape=shape;
	InitShape();
	if( _refVehicles<=0 ) _refVehicles=1;
}

void EntityType::VehicleAddRef() const
{
	// vehicle created
	if( _refVehicles++==0 )
	{
		if( _shapeName[0] )
		{
			bool shadow = true;
			// check if shadow is enabled for this particular shape
			// generally there shadow setting for some "vehicles"
			// should be taken from objects
			if (!_shapeReversed) shadow = true;
			_shape=Shapes.New
			(
				_shapeName,_shapeReversed,shadow
			);
			if (!_shapeAutoCentered)
			{
				_shape->SetAutoCenter(false);
				_shape->CalculateBoundingSphere();
			}
			const_cast<EntityType *>(this)->InitShape();
			//LogF("Type %s shape loaded",(const char *)GetName());
			if (_shapeAnimated)
			{
				_shape->AllowAnimation();
			}
		}
		else
		{
			const_cast<EntityType *>(this)->InitShape();
		}
	}
}

void EntityType::VehicleRelease() const
{
	// vehicle destroyed
	if( --_refVehicles==0 )
	{
		if (_refVehiclesLocked)
		{
			Fail("Locked vehicle released");
		}
		// 2 Refs - one in last vehicle, one in bank
		const_cast<EntityType *>(this)->DeinitShape();
		if (_shape)
		{
			//LogF("Type %s shape released",(const char *)GetName());
			_shape.Free(); // shape no longer needed
		}
	}
}

void EntityType::VehicleLock() const
{
	if (_refVehiclesLocked) return;
	if (_refVehicles<=0) return;
	// shape loaded - lock it
	_refVehiclesLocked = true;
	VehicleAddRef();
}
void EntityType::VehicleUnlock() const
{
	if (!_refVehiclesLocked) return;
	_refVehiclesLocked = false;
	VehicleRelease();
	// enable releasing shape
}

DEFINE_CASTING(Entity)

Entity::Entity
(
	LODShapeWithShadow *shape, const EntityType *type, int id
)
:Object(shape ? shape : type->GetShape(),id),
_type(const_cast<EntityType *>(type)),
//_integrationDone(false),
_delete(false),
_convertToObject(false),_moveOutState(MOIn),
_local(true),
_prec(SimulateDefault),

_objectContact(false),_landContact(true),_waterContact(false),

_constantColor(PackedWhite),

_simulationPrecision(1.0/15),
_simulationSkipped(0),

_invAngInertia(M3Identity),
_speed(VZero),_modelSpeed(VZero),_acceleration(VZero),
_angVelocity(VZero),_angMomentum(VZero),

_invTransform(MIdentity),_invDirty(false),
_impulseForce(VZero),_impulseTorque(VZero),

_targetSide(TCivilian)
{
	if( !type )
	{
		if( _shape )
		{
			LogF("No type, shape %s",(const char *)_shape->Name());
		}
		Fail("No type");
	}
	_type->VehicleAddRef();
	_disableDammageUntil=Glob.time+2; // disable dammage
	// note: orientation is MIdentity - we are in contructor
	Matrix3Val orientation=Orientation();
	if( _shape )
	{
		_invAngInertia=orientation*_shape->InvInertia()*orientation.InverseScaled();
		//if( _shape->Mass()<=0 ) _invMass=1;
		//else _invMass=1.0f/_shape->Mass();
	}
	else
	{
		_invAngInertia=M3Identity;
		//_invMass=1;
	}
	_static=false;
	_destrType=DestructNo;
	Object::_type=TypeVehicle; // change object type
	//DammagePerVertex();

	_animateTexturesTimes.Realloc(type->_animateTextures.Size());
	_animateTexturesTimes.Resize(type->_animateTextures.Size());
	for (int i=0; i<type->_animateTextures.Size(); i++) _animateTexturesTimes[i] = 0;

	_animations.Realloc(type->_animations.Size());
	_animations.Resize(type->_animations.Size());
	for (int i=0; i<type->_animations.Size(); i++) _animations[i].SetType(type->_animations[i]);
}

Entity::~Entity()
{
	_type->VehicleRelease();
	// sound will self-destruct - Ref<> is used
	//UnloadSound();
}

void Entity::SetPosition( Vector3Par pos )
{
	base::SetPosition(pos);
	InvDirty();
}
void Entity::SetTransform( const Matrix4 &transform )
{
	base::SetTransform(transform);
	InvDirty();
}
void Entity::SetOrient( const Matrix3 &dir )
{
	base::SetOrient(dir);
	InvDirty();
}

void Entity::SetOrient( Vector3Par dir, Vector3Par up )
{
	base::SetOrient(dir,up);
	InvDirty();
}
void Entity::SetOrientScaleOnly( float scale )
{
	base::SetOrientScaleOnly(scale);
	InvDirty();
}

void Entity::CalculateInv() const
{
	_invTransform=CalcInvTransform();
}

PackedColor Entity::GetConstantColor() const
{
#if _ENABLE_CHEATS
	if (CHECK_DIAG(DETransparent))
	{
		return PackedColorRGB(_constantColor,_constantColor.A8()/2);
	}
#endif
	return _constantColor;
}

void Entity::GetMaterial(TLMaterial &mat, int index) const
{
	Color accom=GEngine->GetAccomodateEye();
	Color ccolor = GetConstantColor();
	ccolor = ccolor * accom;

	// distrubute by predefined materials
	CreateMaterial(mat,ccolor,index);
}

void Entity::AddForce
(
	Vector3Par pos, Vector3Par force, Color color
)
{
	LODShapeWithShadow *forceArrow=GScene->ForceArrow();
	float size=force.Size()*0.05;
	if( size>1 ) size=1;

	Ref<Object> arrow=new ObjectColored(forceArrow,-1);
	Vector3 aside=force.CrossProduct(VAside);
	Vector3 zAside=force.CrossProduct(VForward);
	if( zAside.SquareSize()>aside.SquareSize() ) aside=zAside;
	arrow->SetPosition(pos);
	arrow->SetOrient(force,aside);
	arrow->SetPosition
	(
		arrow->PositionModelToWorld(forceArrow->BoundingCenter()*size)
	);
	arrow->SetScale(size);
	arrow->SetConstantColor(PackedColor(Color(color)));
	//GScene->ObjectForDrawing(arrow);

	GLandscape->ShowObject(arrow);
}

void Entity::OrientationSurface()
{
	// aling direction and bank with surface
	// calculate surface normal
	Vector3Val pos=Position();
	float dX,dZ;
	float sY=GLOB_LAND->SurfaceY(pos.X(),pos.Z(),&dX,&dZ);
	(void)sY;
	// approximate calculation:
	//Vector3 normal=Vector3(-dX,1,-dZ).Normalized();
	// exact calculation is:
	Vector3Val normal=Vector3(0,dZ,1).CrossProduct(Vector3(1,dX,0)).Normalized();
	Matrix4 transform=Transform();
	transform.SetUpAndDirection(normal,Direction());
	SetTransform(transform);
}

void Entity::ApplySpeed( Matrix4 &result, float deltaT )
{
	// change position according to speed
	Vector3 position=Position();
	position+=_speed*deltaT;
	// change orientation accorging to angular velocity
	// note: we have to rotate around the center of mass
	// this means:
	// translate so that center of mass is in (0,0,0) (using actual orientation)
	// rotate
	// translate back (using new orientation)
	Vector3Val com = GetCenterOfMass();
	Matrix3 orientation=Orientation();
	Vector3Val translateCOM=orientation*com;
	orientation+=_angVelocity.Tilda()*orientation*deltaT;
	//orientation+=_angVelocity.Tilda()*deltaT;
	orientation.Orthogonalize();
	Vector3 backCOM=orientation*com;
	position+=translateCOM-backCOM;

	result.SetPosition(position);
	result.SetOrientation(orientation);
}

void Entity::OnAddImpulse(Vector3Par force, Vector3Par torque)
{
}

void Entity::AddImpulse( Vector3Par force, Vector3Par torque )
{
	OnAddImpulse(force,torque);
	#if 0
	if (!force.IsFinite() || !torque.IsFinite())
	{
		LogF("%s: AddImpulse",(const char *)GetDebugName());
		LogF("  force %g,%g,%g",force[0],force[1],force[2]);
		LogF("  torque %g,%g,%g",torque[0],torque[1],torque[2]);
		return;
	}
	#endif
	_impulseForce += force;
	_impulseTorque += torque;
}

#if _ENABLE_CHEATS
	#define ENABLE_STATS 0
#endif

#if ENABLE_STATS
	#include "statistics.hpp"
	static StatisticsByName impulseStats;
#endif


/*!
\patch 1.50 Date 3/15/2002 by Ondra
- Fixed: MP: Collisions of predicted remote vehicle caused excessive data transfers.
\patch_internal 1.50 Date 3/15/2002 by Ondra
- Fixed: MP: Even very small force impulses were transferred accross network.
*/
void Entity::AddImpulseNetAware( Vector3Par force, Vector3Par torque )
{
#if ENABLE_STATS
	static Time nextReportTime = Glob.time+60;
	if (Glob.time>nextReportTime)
	{
		impulseStats.Report(true);
		nextReportTime = Glob.time+60;
	}
#endif

	// if impulse is very small, do not transmit it
	if (torque.SquareSize()<0.1f && force.SquareSize()<0.1f)
	{
#if ENABLE_STATS
		impulseStats.Count("impulse<0.1");
#endif
		return;
	}
#if ENABLE_STATS
	else if (torque.SquareSize()+force.SquareSize()<1.0f)
	{
		impulseStats.Count("impulse<1.0");
	}
	else if (torque.SquareSize()+force.SquareSize()<10.0f)
	{
		impulseStats.Count("impulse<10.0");
	}
	else if (torque.SquareSize()+force.SquareSize()<100.0f)
	{
		impulseStats.Count("impulse<100.0");
	}
	else
	{
		impulseStats.Count("big impulse");
	}
#endif

	if (IsLocal())
	{
		AddImpulse(force,torque);
	}
	else
	{
		GetNetworkManager().AskForAddImpulse(this,force,torque);
	}
}

void Entity::ApplyForces
(
	float deltaT,
	Vector3Par force, Vector3Par torque,
	Vector3Par friction, Vector3Par torqueFriction,
	float staticFric
)
{
	// integrate quantities

	Vector3 accel=DirectionWorldToModel(force*GetInvMass());
	Vector3 fricAccel=DirectionWorldToModel(friction*GetInvMass());
	Vector3 oldSpeed=_speed;
	Vector3 speed(VMultiply,DirWorldToModel(),_speed);
	Friction(speed,fricAccel,accel,deltaT);
	DirectionModelToWorld(_speed,speed);
	if( deltaT>0 ) _acceleration=(_speed-oldSpeed)*(1/deltaT);
	else _acceleration=VZero;
	Friction(_angMomentum,torqueFriction,torque,deltaT);
	//_angMomentum+=torque*deltaT;

	float rigid=Rigid();
	_speed+=_impulseForce*(GetInvMass()*rigid);
	_angMomentum+=_impulseTorque*rigid;

	// apply statical friction
	// max. speed that can be held by statical friction is
	// staticFric/mass*deltaT
	float staticCanHold = staticFric*GetInvMass()*deltaT;
	if (_speed.SquareSize()<Square(staticCanHold))
	{
		_speed = VZero;
	}
	_impulseTorque=VZero;
	_impulseForce=VZero;

	// calculate auxiliary quantities
	Matrix3Val orientation0=Transform().Orientation();
	Matrix3Val invOrientation0=InvTransform().Orientation();
	if( _shape )
	{
		_invAngInertia=orientation0*InvInertia()*invOrientation0;
	}
	else
	{
		_invAngInertia=M3Zero;
	}

	_angVelocity=_invAngInertia*_angMomentum;
}

inline float fFric(float x)
{
	x *= 2;
	saturate(x,-1,+1);
	return x;
}

inline Vector3 VFric(Vector3Val x,float factor)
{
	float size2 = x.SquareSize()*Square(factor);
	if (size2>1)
	{
		factor *=InvSqrt(size2); 
	}
	return x*factor;
}

inline void SaturateAbs(float &v, float m)
{
	if (fabs(v)>fabs(m)) v = fSign(v)*fabs(m);
}

void SaturateAbs(Vector3 &v, const Vector3 &m)
{
	SaturateAbs(v[0],m[0]);
	SaturateAbs(v[1],m[1]);
	SaturateAbs(v[2],m[2]);
}

#if _DEBUG
#define LOG_FRIC 0
#endif

void Entity::ApplyForcesAndFriction
(
	float deltaT,
	Vector3Par force, Vector3Par torque,
	const FrictionPoint *fric, int nFric
)
{
	// TODO: add parameter determining fricting strength
	float frictionCoef = 10;
	float angularFrictionCoef = 1.0f;

	// integrate quantities

	Vector3 wAccel = force*GetInvMass();
	//Vector3 accel=DirectionWorldToModel(force*GetInvMass());
	Vector3 oldSpeed=_speed;
	Vector3 speed(VMultiply,DirWorldToModel(),_speed);

	//Friction(speed,VZero,accel,deltaT);
	_angMomentum += torque*deltaT;

	_speed += wAccel*deltaT;

	//DirectionModelToWorld(_speed,speed);
	//_angMomentum+=torque*deltaT;

	float rigid=Rigid();

	_speed += _impulseForce*(GetInvMass()*rigid);
	_angMomentum += _impulseTorque*rigid;

	_impulseTorque=VZero;
	_impulseForce=VZero;

	// calculate auxiliary quantities
	Matrix3Val orientation0=Transform().Orientation();
	Matrix3Val invOrientation0=InvTransform().Orientation();
	if( _shape )
	{
		_invAngInertia=orientation0*InvInertia()*invOrientation0;
	}
	else
	{
		_invAngInertia=M3Zero;
	}

	_angVelocity=_invAngInertia*_angMomentum;

	#if 0 //ARROWS
	{
		Vector3 wCenter(VFastTransform,ModelToWorld(),GetCenterOfMass());

		Vector3 pCenter(1,1,1);
		Vector3 radVel = _angVelocity.CrossProduct(pCenter);
		Vector3 actVel = _speed + radVel;
		AddForce
		(
			wCenter+pCenter,radVel*2,Color(0,0.5,0,0.5)
		);
		AddForce
		(
			wCenter+pCenter,actVel*2,Color(0,1,0,0.5)
		);
		/*
		AddForce
		(
			wCenter+pCenter,-pVelChange*(mass/deltaT),
			full ? Color(1,0,0,0.5) : Color(1,0.5,0,0.5)
		);
		*/
	}
	#endif

	#if 1
	if (nFric>0)
	{
		AUTO_STATIC_ARRAY(Vector3,pVelChange,128);
		pVelChange.Resize(nFric);
		/*
		LogF("* deltaT %.3f, NF %d",deltaT,nFric);
		LogF("Old     V %.2f,%.2f,%.2f size %.2f",_speed[0],_speed[1],_speed[2],_speed.Size());
		LogF("    ang M %.2f,%.2f,%.2f size %.2f",_angMomentum[0],_angMomentum[1],_angMomentum[2],_angMomentum.Size());
		LogF("    ang V %.2f,%.2f,%.2f size %.2f",_angVelocity[0],_angVelocity[1],_angVelocity[2],_angVelocity.Size());
		*/

		Vector3 velChange(VZero);
		Vector3 angMomChange(VZero);
		//Vector3Val com = GetCenterOfMass();
		//Matrix3 orientation=Orientation();
		//Vector3Val translateCOM=orientation*com;
		float mass = GetMass();

		//LogF("_speed %.3f,%.3f,%.3f",_speed[0],_speed[1],_speed[2]);
		//LogF("_angMomentum %.3f,%.3f,%.3f",_angMomentum[0],_angMomentum[1],_angMomentum[2]);

		Vector3 wCenter(VFastTransform,ModelToWorld(),GetCenterOfMass());
		// if there is more of them, each one 
		/**/
		float sumFricCoef = 0;
		for (int f=0; f<nFric; f++)
		{
			const FrictionPoint &fp = fric[f];
			sumFricCoef += fp.frictionCoef;
		}
		// sum of all frictionFactors is in range 0 .. 2
		// typically sumFricCoef is around 1
		// we may need it in range 0 .. 1
		// note: it may be higher when in water
		float invSumFricCoef = sumFricCoef>1 ? 1.0f/sumFricCoef : 1;
		/**/

		//float speedSize = _speed.Size()*1.2f;
		float angMomFriction = 0 ;
		for (int f=0; f<nFric; f++)
		{
			const FrictionPoint &fp = fric[f];
			Vector3 pCenter = fp.pos-wCenter;
			// calculate actual velocity of the point
			// relPos is in model coordinates
			Vector3 radVel = _angVelocity.CrossProduct(pCenter);
			Vector3 actVel = _speed + radVel;
			//Vector3 actVel = _speed;
			// apply friction opposite to actVel
			#if ARROWS
				AddForce
				(
					wCenter+pCenter,actVel*2,Color(0,0,1,0.25)
				);
			#endif

			Vector3 actVelVert = fp.outDir*(actVel*fp.outDir);
			//Vector3 actVelVert = VZero;
			Vector3 actVelHoriz = actVel - actVelVert;

			float speedFrac = 10*deltaT;
			// friction may absorb max. all speed, not more
			saturateMin(speedFrac,1);

			float frictionCoefNorm = invSumFricCoef*fp.frictionCoef;

			pVelChange[f] = actVelVert*(speedFrac*frictionCoefNorm);

			Vector3 vDelta = sign(actVelHoriz)*frictionCoefNorm*frictionCoef*deltaT;

			SaturateAbs(vDelta,actVelHoriz*floatMin(frictionCoefNorm,1));

			pVelChange[f] += vDelta;

			angMomFriction += frictionCoefNorm*angularFrictionCoef*deltaT;

			#if ARROWS
				AddForce
				(
					wCenter+pCenter,-pVelChange[f]*(1.0/deltaT),
					Color(1,0,0,0.5)
				);
			#endif
		}

		// note: we could check energy conservation here
		// we have speed changes (acceleration*time) in all points
		// special case: if angular velocity is very low,
		// total speed change should not be bigger than total speed

		for (int f=0; f<nFric; f++)
		{
			const FrictionPoint &fp = fric[f];
			Vector3 pCenter = fp.pos-wCenter;

			velChange -= pVelChange[f];
			// calculate partical change
			Vector3 pAngMomChange = pCenter.CrossProduct(pVelChange[f]*mass);
			angMomChange -= pAngMomChange;


			//LogF("  actVel %.3f,%.3f,%.3f",actVel[0],actVel[1],actVel[2]);
			//LogF("  pVelChange %.3f,%.3f,%.3f",pVelChange[0],pVelChange[1],pVelChange[2]);
			//LogF("  pAngMomChange %.3f,%.3f,%.3f",pAngMomChange[0],pAngMomChange[1],pAngMomChange[2]);


		}

		#if LOG_FRIC
		Log
		(
			"_speed %.3f,%.3f,%.3f velChange %.3f,%.3f,%.3f",
			_speed[0],_speed[1],_speed[2],
			velChange[0],velChange[1],velChange[2]
		);
		#endif

		_speed += velChange;
		if (_speed.SquareSize()>1e6)
		{
			Log
			(
				"Rotating fast? %.2f,%.2f,%.2f",
				_angVelocity[0],_angVelocity[1],_angVelocity[2]
			);

		}
		_angMomentum += angMomChange;
		#if LOG_FRIC
		Log
		(
			"_angMomentum %.3f,%.3f,%.3f angMomChange %.3f,%.3f,%.3f, fric %.2f",
			_angMomentum[0],_angMomentum[1],_angMomentum[2],
			angMomChange[0],angMomChange[1],angMomChange[2],
			angMomFriction
		);
		#endif

		_angMomentum *= 1-floatMin(angMomFriction,1);
		#if LOG_FRIC
		GlobalShowMessage
		(
			100,"AM %5.1f, M %5.1f, S %5.1f",
			_angMomentum.Size(),_speed.Size()*GetMass(),
			_angMomentum.Size()+_speed.Size()*GetMass()
		);
		#endif
	}
	#endif

	// change momentum and angular momentum
	// momentum changed: recalculate auxiliary quantities
	if( _shape )
	{
		_invAngInertia=orientation0*InvInertia()*invOrientation0;
	}
	else
	{
		_invAngInertia=M3Zero;
	}
	_angVelocity=_invAngInertia*_angMomentum;

	if( deltaT>0 ) _acceleration=(_speed-oldSpeed)*(1/deltaT);
	else _acceleration=VZero;
}


PilotHeadPars::PilotHeadPars()
{
	friction=10;
	movement=160;
	maxAmp=0.05;
	maxSpeed=3;
	radius=0.2;
}

PilotHeadPars::PilotHeadPars( const ParamEntry &entry )
{
	friction=entry>>"friction";
	movement=entry>>"movement";
	maxAmp=entry>>"maxAmp";
	maxSpeed=entry>>"maxSpeed";
	radius=entry>>"radius";
}

PilotHead::PilotHead()
{
	_valid = false;
}

void PilotHead::Init( Vector3Par neck, Vector3Par head, const Entity *vehicle )
{
	_neck=neck,_head=head;
	Reset(vehicle);
	_valid = true;
}

void PilotHead::SetPars( const PilotHeadPars &pars )
{
	_pars=pars;
}

void PilotHead::SetPars( const char *name )
{
	_pars=PilotHeadPars(Pars>>"CfgHeads">>name);

}

void PilotHead::Move( float deltaT, const Frame &newFrame, const Frame &oldFrame )
{
	// we need to calculate world space force applied to the head
	// the only actual force is neck force
	float invDeltaT=1/deltaT;
	Vector3Val oldNeckPos=oldFrame.PositionModelToWorld(_neck);
	Vector3Val newNeckPos=newFrame.PositionModelToWorld(_neck);
	Vector3Val neckSpeed=(newNeckPos-oldNeckPos)*invDeltaT;

	// simulate where would head be if it was moving only by inertia
	Vector3Val posWEnd=oldFrame.PositionModelToWorld(_pos)+_speed*deltaT;
	// simulate where neck wants head to be
	Vector3Val headWEnd=newFrame.PositionModelToWorld(_head);
	
	// assume neck "centers" head with centering force
	Matrix4Val invTransform=newFrame.CalcInvTransform();
	Vector3 oSpeed=invTransform.Rotate(_speed-neckSpeed);
	_pos+=oSpeed*deltaT; // integrate object space head speed
	// keep pos on sphere centered around the neck
	// limit head movement
	saturate(_pos[0],_head[0]-_pars.maxAmp,_head[0]+_pars.maxAmp);
	saturate(_pos[1],_head[1]-_pars.maxAmp,_head[1]+_pars.maxAmp);
	saturate(_pos[2],_head[2]-_pars.maxAmp,_head[2]+_pars.maxAmp);
	Vector3 norm = (_pos-_neck);
	_pos=_neck+norm.Normalized()*_pars.radius;
	// optimized for helicopter
	Vector3 center=(headWEnd-posWEnd)*_pars.movement;
	Vector3 friction=oSpeed*_pars.friction;
	//Vector3 friction=oSpeed*5;
	center=invTransform.Rotate(center);
	//friction[1]*=2; // y-friction is extra strong
	//center[1]*=10; // y-centering is extra strong
	Friction(oSpeed,friction,center,deltaT);
	// limit head speed
	saturate(oSpeed[0],-_pars.maxSpeed,_pars.maxSpeed);
	saturate(oSpeed[1],-_pars.maxSpeed,_pars.maxSpeed);
	saturate(oSpeed[2],-_pars.maxSpeed,_pars.maxSpeed);
	_speed=newFrame.DirectionModelToWorld(oSpeed)+neckSpeed;
}

// general simulation functions

void Friction( float &speed, float friction, float accel, float deltaT )
{
	// part of accel is consumed to overcome friction
	speed+=accel*deltaT;
	if( speed*friction>0 )
	{ // friction must be against the speed
		float oSpeed=speed;
		speed-=friction*deltaT;
		// friction can never change sign of the speed
		if( oSpeed*speed<=0 ) speed=0;
	}
}

void Friction
(
	Vector3 &speed, Vector3Par friction, Vector3Par accel, float deltaT
)
{
	Friction(speed[0],friction[0],accel[0],deltaT);
	Friction(speed[1],friction[1],accel[1],deltaT);
	Friction(speed[2],friction[2],accel[2],deltaT);
}

AttachedOnVehicle::AttachedOnVehicle
(
	Object *vehicle, Vector3Par pos, Vector3Par dir
)
:_vehicle(vehicle),_pos(pos),_dir(dir)
{
	GLOB_WORLD->AddAttachment(this);
}

AttachedOnVehicle::~AttachedOnVehicle()
{
	GLOB_WORLD->RemoveAttachment(this);
}

void AttachedOnVehicle::UpdatePosition()
{
	if( _vehicle!=NULL )
	{
		float scale=Scale();
		Matrix4 toWorld = _vehicle->WorldTransform();
		SetPosition(toWorld.FastTransform(_pos));
		//_speed=_vehicle->Speed();
		SetOrient
		(
			toWorld.Rotate(_dir),toWorld.DirectionUp()
		);
		SetScale(scale);
	}
}

LSError AttachedOnVehicle::Serialize(ParamArchive &ar)
{
	CHECK(ar.SerializeRef("Entity", _vehicle, 1))
	CHECK(ar.Serialize("pos", _pos, 1))
	CHECK(ar.Serialize("dir", _dir, 1))
	if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassSecond) UpdatePosition();
	return LSOK;
}

float Object::CamEffectFOV() const
{
	return 0.7f;
}

bool Object::IsContinuous( CameraType camType ) const
{
	return camType==CamGunner || camType==CamGroup;
}

bool Object::IsVirtual( CameraType camType ) const
{
	return camType!=CamGunner;
}

Vector3 Object::GetCameraDirection( CameraType camType ) const
{
	return Direction();
}

bool Object::IsVirtualX( CameraType camType ) const
{
	// used only is IsVirtual is true
	// if mouse rotates, this should return false
	return true;
}

void Object::DetectControlMode() const
{
	static const UserAction moveActions[]=
	{
		UAMoveForward,UAMoveBack,
		UAMoveFastForward,UAMoveSlowForward,
		UAMoveUp,UAMoveDown
	};
	static const UserAction turnActions[]=
	{
		UAMoveLeft,UAMoveRight,
		UATurnLeft,UATurnRight
	};
	static const UserAction cursorActions[]=
	{
		UALookLeftDown,UALookDown,UALookRightDown,
		UALookLeft,UALookCenter,UALookRight,
		UALookLeftUp,UALookUp,UALookRightUp
	};
	static const UserAction thrustActions[]=
	{
		UAMoveUp,UAMoveDown,
	};

	const int nMoveActions = sizeof(moveActions)/sizeof(*moveActions);
	const int nTurnActions = sizeof(turnActions)/sizeof(*turnActions);
	const int nCursorActions = sizeof(cursorActions)/sizeof(*cursorActions);
	const int nThrustActions = sizeof(thrustActions)/sizeof(*thrustActions);
	DetectControlModeActions
	(
		moveActions,nMoveActions,
		turnActions,nTurnActions,
		cursorActions,nCursorActions,
		thrustActions,nThrustActions
	);

}


static bool CheckAxisActive(UserAction action)
{
	const KeyList &keys = GInput.userKeys[action];
	int axis = -1;
	for (int ii=0; ii<keys.Size(); ii++)
	{
		int key = keys[ii];
		if ((key&INPUT_DEVICE_MASK)==INPUT_DEVICE_STICK_AXIS)
		{
			axis = key-INPUT_DEVICE_STICK_AXIS;
			if (axis>=0 && axis<N_JOYSTICK_AXES)
			{
				if (GInput.jAxisLastActive[axis]>=Glob.uiTime-0.1)
				{
					return true;
				}
			}
		}
	}
	return false;
}

void Object::DetectControlModeActions
(
	const UserAction *moveActions, int nMoveActions,
	const UserAction *turnActions, int nTurnActions,
	const UserAction *cursorActions, int nCursorActions,
	const UserAction *thrustActions, int nThrustActions
) const
{
	bool keyMoveActive = false;
	bool keyTurnActive = false;
	bool keyCursorActive = false;
	bool keyThrustActive = false;
	for (int i=0; i<nMoveActions; i++)
	{
		if (GInput.GetAction(moveActions[i])) {keyMoveActive = true;break;}
	}
	for (int i=0; i<nTurnActions; i++)
	{
		if (GInput.GetAction(turnActions[i])) {keyTurnActive = true;break;}
	}
	for (int k=0; k<nCursorActions; k++)
	{
		if( GInput.GetAction(cursorActions[k]) ) {keyCursorActive=true;break;}
	}
	for (int k=0; k<nThrustActions; k++)
	{
		if( GInput.GetAction(thrustActions[k]) ) {keyThrustActive=true;break;}
	}

	if (keyMoveActive)
	{
		GInput.keyboardMoveLastActive = Glob.uiTime;
	}

	if (keyTurnActive)
	{
		GInput.keyboardTurnLastActive=Glob.uiTime;
	}
	
	// check if keyboard is used
	if (keyCursorActive)
	{
		GInput.keyboardCursorLastActive = Glob.uiTime;
		// disable mouse turning
		GInput.keyboardTurnLastActive = Glob.uiTime;
	}

	if (keyThrustActive)
	{
		GInput.keyboardThrustLastActive=Glob.uiTime;
	}

	{

		// check joystick configuration and scan appropriate axes (controls)
		static const UserAction moveAxis[] =
		{
			UAAxisTurn,UAAxisDive,UAAxisRudder
		};
		for( int i=0; i<sizeof(moveAxis)/sizeof(*moveAxis); i++ )
		{
			// seek stick axis
			if (CheckAxisActive(moveAxis[i]))
			{
				GInput.joystickMoveLastActive=Glob.uiTime;
				break;
			}

		}
		
		if (CheckAxisActive(UAAxisThrust))
		{
			GInput.joystickThrustLastActive = Glob.uiTime;
		}
	}
	/*
	GlobalShowMessage
	(
		100,"Thurst last: joy %.2f, key %.2f",
		Glob.uiTime-GInput.joystickThrustLastActive,
		Glob.uiTime-GInput.keyboardThrustLastActive
	);
	*/

}

Vector3 Object::ExternalCameraPosition( CameraType camType ) const
{
	return Vector3(0,2,-20);
}

bool Object::HasFlares( CameraType camType ) const
{
	return camType!=CamInternal;
}

bool Object::IsExternal( CameraType camType ) const
{
	return camType==CamGroup;
}

int Object::InsideViewGeomLOD( CameraType camType ) const
{
	if (!_shape) return LOD_INVISIBLE;
	// check inside LOD type
	int inside = InsideLOD(camType);
	int view = LOD_INVISIBLE;
	if (inside!=LOD_INVISIBLE && inside>=0)
	{
		if (_shape->IsSpecLevel(inside,VIEW_PILOT))
		{
			view = _shape->FindViewPilotGeometryLevel();
		}
		else if (_shape->IsSpecLevel(inside,VIEW_GUNNER))
		{
			view = _shape->FindViewGunnerGeometryLevel();
		}
		else if (_shape->IsSpecLevel(inside,VIEW_COMMANDER))
		{
			view = _shape->FindViewCommanderGeometryLevel();
		}
		else if (_shape->IsSpecLevel(inside,VIEW_CARGO))
		{
			view = _shape->FindViewCargoGeometryLevel();
		}
	}
	return view;
}

bool Object::IsGunner( CameraType camType ) const
{
	return camType==CamGunner || camType==CamInternal || camType==CamExternal;
	//return camType==CamGunner;
}
bool Object::IsTurret( CameraType camType ) const
{
	return false;
}
bool Object::ShowAim( int weapon, CameraType camType ) const
{
	return true;
}
bool Object::ShowCursor( int weapon, CameraType camType ) const
{
	return true;
}

void Object::LimitCursorHard
(
	CameraType camType, Vector3 &dir
) const
{
}

void Object::LimitCursor
(
	CameraType camType, Vector3 &dir
) const
{
}

void Object::OverrideCursor
(
	CameraType camType, Vector3 &dir
) const
{
}

void Object::SimulateHUD(CameraType camType,float deltaT)
{
	// default - no simulation
}

CursorMode Object:: GetCursorRelMode(CameraType camType) const
{
	// relative cursor only in virtual cockpit/view
	// and only when mouse is not active
	if
	( 
		IsVirtual(camType)
		&& !GInput.lookAroundEnabled
		&& !GInput.MouseTurnActive()
	)
	{
		return CKeyboard;
	}
	return CMouseAbs;
}

void Object::LimitVirtual
(
	CameraType camType, float &heading, float &dive, float &fov
) const
{
	switch( camType )
	{
		case CamInternal: case CamExternal:
			saturate(fov,0.42,0.85);
		break;
		default:
			saturate(fov,0.01,1.5);
		break;
	}
	heading=AngleDifference(heading,0);
	saturate(dive,-H_PI*0.3,+H_PI*0.3);
}

void Object::InitVirtual
(
	CameraType camType, float &heading, float &dive, float &fov
) const
{
	switch( camType )
	{
		case CamExternal: case CamGroup:
			heading=0;
			dive=0.1;
			fov=0.7;
		break;
		default:
			heading=dive=0;
			fov=0.7;
		break;
	}
	if( fabs(heading)>2*H_PI ) heading=fastFmod(heading,2*H_PI);
	if( fabs(dive)>2*H_PI ) dive=fastFmod(dive,2*H_PI);
}


bool EnableVisualEffects(Vector3Par effPos, SimulationImportance prec)
{
	// on decicated server there are no visual effects or sound effects
	#if _ENABLE_DEDICATED_SERVER
		bool IsDedicatedServer();
		if (IsDedicatedServer()) return false;
	#endif
	// check distance from camera
	// to see if visual effect may be visible
	Vector3 pos = GScene->GetCamera()->Position();
	float dist2 = pos.Distance2(effPos);
	return dist2<Square(Glob.config.objectsZ);
}

/*!
\patch_internal 1.82 Date 8/14/2002 by Ondra
- Optimized: Calculation of visual effects is independent on simulation precision.
*/

bool Entity::EnableVisualEffects(SimulationImportance prec) const
{
	return ::EnableVisualEffects(Position(),prec);
}

/*!
\patch 1.82 Date 8/14/2002 by Ondra
- Fixed: MP: Fixed inconsistencies in determination of simulation precision,
which caused remote vehicle shaking.
*/

SimulationImportance Entity::CalculateImportance
(
	const Vector3 *viewerPos, int nViewers
) const
{
	if (!IsLocal() && _prec!=SimulateDefault) return _prec;
	SimulationImportance maxPrec = WorstImportance();
	SimulationImportance minPrec = BestImportance();
	if (maxPrec<=minPrec) return maxPrec;

	SimulationImportance ret = SimulateVisibleNear;
	float dist2 = 1e20;
	for (int i=0; i<nViewers; i++)
	{
		float d2=viewerPos[i].Distance2(Position());
		saturateMin(dist2,d2);
	}
	float radius = GetRadius();
	if( radius>=100 )
	{
		float dist=floatMax(dist2*InvSqrt(dist2)-radius,0);
		dist2=Square(dist);
		//LogF("Distance of big object is %.1f (r=%.1f)",dist,radius);
	}
	if( dist2>Square(Glob.config.objectsZ+500) ) ret = SimulateInvisibleFar;
	else if( dist2>Square(Glob.config.objectsZ) ) ret = SimulateInvisibleNear;
	else if( dist2>Square(100) ) ret =SimulateVisibleFar;

	if (ret>maxPrec) ret=maxPrec;
	if (ret<minPrec) ret=minPrec;
	if (IsLocal())
	{
		// TODO: make _prec mutable
		const_cast<Entity *>(this)->_prec = ret;
	}
	return ret;
}

/*!
	\patch_internal 1.23 Date 9/12/2001 by Ondra
	- Fixed: positioning on objects containing proxies might be incorrect.
*/
void Entity::PlaceOnSurface(Matrix4 &trans)
{
	if (!GetShape())
		return;

	// find steady position on surface
	// use ground collistion test to validate position
	float dx,dz;
	Vector3 pos;
	pos= trans.Position();
	pos[1] = GLandscape->RoadSurfaceYAboveWater(pos, &dx, &dz);

	// use min coordinate to avoid submerging landscape
	LODShape *shape = GetShape();
	// check which convention should we use
	// moving vehicles should be adjusted for min. Y

	// calculate new directions

	Matrix4 newTransform;
	if ((shape->GetOrHints()&ClipLandKeep|ClipLandOn))
	{
		// ClipLandKeep objects up must point up in world space
		newTransform.SetUpAndDirection(VUp,trans.Direction());
	}
	else
	{
		// move and change orienation
		Vector3 up(-dx,1,-dz);
		Matrix4 newTransform;
		newTransform.SetUpAndDirection(up,trans.Direction());
	}
	if (!Static())
	{
		Shape *geom = shape->LandContactLevel();
		if (!geom) geom = shape->GeometryLevel();
		if (!geom) geom = shape->Level(0);

		if (!geom) pos[1] -= shape->Min().Y();
		else pos[1] -= geom->Min().Y();
	}
	else
	{
		pos += newTransform.Orientation() * GetShape()->BoundingCenter();
	}
	newTransform.SetPosition(pos);
	trans = newTransform;
}

DEF_RSB(type)
DEF_RSB(shape)
DEF_RSB(id)
DEF_RSB(Transform)

#define TRANS(x) \
	DEF_RSB(x); \
	CHECK(ar.Serialize(RSB(x), _##x, 1))

#define TRANS_DEF(x,defVal) \
	DEF_RSB(x); \
	CHECK(ar.Serialize(RSB(x), _##x, 1, defVal))

bool Entity::CastShadow() const
{
	return IS_SHADOW_VEHICLE;
}

float Entity::GetAnimationPhase(RString animation) const
{
	for (int i=0; i<_animations.Size(); i++)
	{
		if (stricmp(_animations[i].GetName(), animation) == 0)
		{
			return _animations[i].GetPhase();
		}
	}
	return 0.0f;
}

void Entity::SetAnimationPhase(RString animation, float phase)
{
	for (int i=0; i<_animations.Size(); i++)
	{
		if (stricmp(_animations[i].GetName(), animation) == 0)
		{
			_animations[i].SetPhaseWanted(phase);
			return;
		}
	}
}

bool Entity::IsAnimated( int level ) const
{
	if (base::IsAnimated(level)) return true;
	return _animations.Size() > 0;
}

bool Entity::IsAnimatedShadow( int level ) const
{
	if (base::IsAnimatedShadow(level)) return true;
	return _animations.Size() > 0;
}

void Entity::Animate( int level )
{
	const EntityType *type = GetNonAIType();

	// animate textures
	for (int i=0; i<type->_animateTextures.Size(); i++)
	{
		type->_animateTextures[i].animation.AnimateTexture(_shape, level, _animateTexturesTimes[i]);
	}

	// animate animations
	for (int i=0; i<_animations.Size(); i++)
	{
		const AnimationInstance &anim = _animations[i];
		if (anim.GetSelection(level)>=0)
		{
			Matrix4 baseAnim = MIdentity;
			AnimateMatrix(baseAnim,level,_animations[i].GetSelection(level));
			_animations[i].Animate(_shape, level,baseAnim);
		}
	}

	base::Animate(level);
}

void Entity::Deanimate( int level )
{
	const EntityType *type = GetNonAIType();

	// deanimate animations
	for (int i=0; i<_animations.Size(); i++)
	{
		_animations[i].Deanimate(_shape, level);
	}

	// deanimate textures
	for (int i=0; i<type->_animateTextures.Size(); i++)
	{
		type->_animateTextures[i].animation.AnimateTexture(_shape, level, 0);
	}

	base::Deanimate(level);
}

/*!
	\patch 1.01 Date 06/12/2001 by Jirka
	- Fixed: if save called when training target was destroying, save was corrupted
	\patch_internal 1.01 Date 06/12/2001 by Jirka
	- transform matrix must be load before Object::Serialize is called
	- Object::Serialize is using transform matrix sometimes (SetDestroyPhase is called)
*/
LSError Entity::Serialize(ParamArchive &ar)
{
	// FIX - read position before Object::Serialize
	if (!IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
	{
		if (Object::GetType() != Primary)
		{
			if (ar.IsSaving())
			{
				Matrix4 &trans = const_cast<Matrix4 &>(Transform());
				CHECK(ar.Serialize(RSB(Transform), trans, 1))
			}
			else if (ar.GetPass() == ParamArchive::PassFirst)
			{
				Matrix4 trans;
				CHECK(ar.Serialize(RSB(Transform), trans, 1))
				SetTransform(trans);
			}
		}
	}

	base::Serialize(ar);
	if (ar.IsSaving())
	{
		// save type
		//bool ai = dyn_cast<VehicleWithAI>(this) != NULL;
		//CHECK(ar.Serialize("ai", ai, 1, true))
		//if (!ai)
		{
			//const type_info &typei = typeid(*this);
			//const char *name = typei.name();
			//static const char cPrefix[]="class ";
			//const int cPrefixLen = sizeof(cPrefix) - 1;
			//Assert(strncmp(name, cPrefix, cPrefixLen) == 0);
			//RString simulation = name + cPrefixLen;
			RString type = GetName() ? GetName() : "";
			RString shape = _shape ? _shape->Name() : "";
			//CHECK(ar.Serialize("simulation", simulation, 1))
			CHECK(ar.Serialize(RSB(type), type, 1))
			CHECK(ar.Serialize(RSB(shape), shape, 1))
		}
	}

	if (!IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
	{
		if (Object::GetType() != Primary)
		{
			if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
			{
				CHECK(ar.Serialize(RSB(id), _id, 1))
				GLandscape->AddToIDCache(this);
			}

			TRANS_DEF(speed,VZero)
			TRANS_DEF(angMomentum,VZero)
			//CHECK(ar.Serialize("moveHeight", _moveHeight, 1))
			//CHECK(ar.Serialize("disableDammageUntil", _disableDammageUntil, 1))
			if (ar.IsLoading())
			{
				// recalculate or initalize auxiliary properties
				_invDirty = true;
				DirectionWorldToModel(_modelSpeed,_speed);

				Matrix3Val orientation0=Transform().Orientation();
				Matrix3Val invOrientation0=InvTransform().Orientation();
				if( _shape )
				{
					_invAngInertia=orientation0*InvInertia()*invOrientation0;
				}
				else
				{
					_invAngInertia=M3Zero;
				}
				_angVelocity=_invAngInertia*_angMomentum;
				_acceleration = VZero;
				_impulseForce = VZero;
				_impulseTorque = VZero;
				//_moveTrans.SetTransform(Transform());
			}

			CHECK(ar.SerializeRef("hierParent",_hierParent, 1));
		}
		CHECK(ar.SerializeEnum("targetSide", _targetSide, 1))
	}

	return LSOK;

	// sets in VehiclesDistributed::Serialize: _list

	// calculated: _modelSpeed
	// calculated: _angVelocity
	// calculated: _invAngInertia
	// calculated: _acceleration
	// calculated: _impulseForce
	// calculated: _impulseTorque

	// temporary: _moveTrans
	// temporary: _integrationDone;

	// sets in NewVehicle: _name

	// temporary: _delete
	// temporary: _convertToObject
	// temporary: _moveOut

	// network: _netID;
}

Entity *Entity::CreateObject(ParamArchive &ar)
{
	RString simulation, type, shape;
	//if (ar.Serialize("simulation", simulation, 1) != LSOK) return NULL;
	if (ar.Serialize("type", type, 1) != LSOK) return NULL;
	if (ar.Serialize("shape", shape, 1) != LSOK) return NULL;
	Entity *veh = NewNonAIVehicle(type, shape,false);
	if (veh && veh->Object::GetType() == Primary)
	{
		veh->SetType(TypeVehicle);
	}
	return veh;
}

NetworkId Entity::GetNetworkId() const
{
	if ((ObjectType)Object::_type == Primary) return Object::GetNetworkId();
	else return _networkId;
}

NetworkMessageType Entity::GetNMType(NetworkMessageClass cls) const
{
	switch (cls)
	{
	case NMCCreate:
		return NMTCreateVehicle;
	case NMCUpdateGeneric:
		return NMTUpdateVehicle;
	case NMCUpdatePosition:
		return NMTUpdatePositionVehicle;
	default:
		return base::GetNMType(cls);
	}
}

IndicesCreateVehicle::IndicesCreateVehicle()
{
	list = -1;
	type = -1;
	shape = -1;
	position = -1;
	name = -1;
	idVehicle = -1;
	hierParent = -1;
}

void IndicesCreateVehicle::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);

	SCAN(list)
	SCAN(type)
	SCAN(shape)
	SCAN(position)
	SCAN(name)
	SCAN(idVehicle)
	SCAN(hierParent)
}

NetworkMessageIndices *GetIndicesCreateVehicle() {return new IndicesCreateVehicle();}

IndicesUpdateVehicle::IndicesUpdateVehicle()
{
	targetSide = -1;
	animations = -1;
}

void IndicesUpdateVehicle::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);

	SCAN(targetSide)
	SCAN(animations)
}

NetworkMessageIndices *GetIndicesUpdateVehicle() {return new IndicesUpdateVehicle();}

IndicesUpdatePositionVehicle::IndicesUpdatePositionVehicle()
{
/*
	orientation = -1;
	position = -1;
	speed = -1;
	angMomentum = -1;
*/
	entityUpdPos = -1;
	prec = -1;
}

void IndicesUpdatePositionVehicle::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);

/*
	SCAN(orientation)
	SCAN(position)
	SCAN(speed)
	SCAN(angMomentum)
*/
	SCAN(entityUpdPos)
	SCAN(prec)
}

NetworkMessageIndices *GetIndicesUpdatePositionVehicle() {return new IndicesUpdatePositionVehicle();}

NetworkMessageFormat &Entity::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	Vector3 temp = VZero;
	switch (cls)
	{
	case NMCCreate:
		NetworkObject::CreateFormat(cls, format);
		format.Add("list", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Container, which manage this entity"));
		format.Add("type", NDTString, NCTDefault, DEFVALUE(RString, ""), DOC_MSG("Entity type"));
		format.Add("shape", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Entity shape (model)"));
		format.Add("position", NDTVector, NCTNone, DEFVALUE(Vector3, temp), DOC_MSG("Current position"));
		format.Add("name", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Name of variable"));
		format.Add("idVehicle", NDTInteger, NCTSmallSigned, DEFVALUE(int, -1), DOC_MSG("ID in map of vehicles (used in waypoints and triggers)"));
		format.Add("hierParent", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Parent in hierarchy"), ET_NOT_EQUAL, ERR_COEF_STRUCTURE);
		break;
	case NMCUpdateGeneric:
		base::CreateFormat(cls, format);
		format.Add("targetSide", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, TWest), DOC_MSG("Side"));
		format.Add("animations", NDTFloatArray, NCTNone, DEFVALUEFLOATARRAY, DOC_MSG("Current state of users defined animations"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);
		break;
	case NMCUpdatePosition:
		base::CreateFormat(cls, format);
/*
		format.Add("orientation", NDTMatrix, NCTNone, DEFVALUE(Matrix3, M3Identity), ET_SQUARE_DIF, 1);
		format.Add("position", NDTVector, NCTNone, DEFVALUE(Vector3, temp), ET_SQUARE_DIF, 1);
		format.Add("speed", NDTVector, NCTNone, DEFVALUE(Vector3, temp));
		format.Add("angMomentum", NDTVector, NCTNone, DEFVALUE(Vector3, temp));
*/
		format.Add("entityUpdPos", NDTRawData, NCTNone, DEFVALUERAWDATA, DOC_MSG("Encoded position"), ET_UPD_ENTITY_POS, 1);
		format.Add("prec", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, SimulateDefault), DOC_MSG("Simulation precision"), ET_NOT_EQUAL, ERR_COEF_VALUE_MAJOR);
		break;
	default:
		base::CreateFormat(cls, format);
		break;
	}
	return format;
}

void Entity::CancelMoveOutInProgress()
{
	DoAssert((MoveOutState)_moveOutState==MOMovingOut);
	_moveOutState=MOIn;
}

void Entity::SetMoveOut(Object *parent)
{
	Assert(_moveOutState==MOIn);
	_moveOutState=MOMovingOut,_hierParent=parent;
}
void Entity::SetMoveOutDone(Object *parent)
{
	Assert(_moveOutState==MOIn);
	_moveOutState=MOMovedOut,_hierParent=parent;
}

void Entity::SetMoveOutFlag()
{
	//Assert(_moveOutState==MOIn);
	// _hierParent will be transfered separatelly - do not set it to NULL
	_moveOutState=MOMovedOut;
}

/*!
\patch 1.92 Date 8/1/2003 by Jirka
- Fixed: Event handler "init" is now launched on all machines in MP game
*/

Entity *Entity::CreateObject(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesCreateVehicle *>(ctx.GetIndices()))
	const IndicesCreateVehicle *indices = static_cast<const IndicesCreateVehicle *>(ctx.GetIndices());

	VehicleListType list;
	if (ctx.IdxTransfer(indices->list, (int &)list) != TMOK) return NULL;
	RString type;
	if (ctx.IdxTransfer(indices->type, type) != TMOK) return NULL;
	RString shape;
	if (ctx.IdxTransfer(indices->shape, shape) != TMOK) return NULL;
	Vector3 position;
	if (ctx.IdxTransfer(indices->position, position) != TMOK) return NULL;

	Entity *veh = NewNonAIVehicle(type, shape, false);
	if (!veh) return NULL;
  Matrix4 trans;
  trans.SetIdentity();
  trans.SetPosition(position);
  veh->Init(trans);
	veh->SetPosition(position);
	if (veh && veh->Object::GetType() == Primary)
	{
		veh->SetType(TypeVehicle);
	}
	switch (list)
	{
	case VLTVehicle:
		GWorld->AddVehicle(veh);
		break;
	case VLTAnimal:
		GWorld->AddAnimal(veh);
		break;
	case VLTBuilding:
		GWorld->AddBuilding(veh);
		break;
	case VLTCloudlet:
		GWorld->AddCloudlet(veh);
		break;
	case VLTFast:
		GWorld->AddFastVehicle(veh);
		break;
	case VLTOut:
		GWorld->AddOutVehicle(veh);
		veh->SetMoveOutFlag();
		break;
	default:
		Fail("Bad list type");
		return NULL;
	}

	int idVeh;
	if (ctx.IdxTransfer(indices->idVehicle, idVeh) != TMOK) return NULL;
	if (idVeh >= 0)
	{
		if (idVeh >= vehiclesMap.Size()) vehiclesMap.Resize(idVeh + 1);
		VehicleWithAI *vai = dyn_cast<VehicleWithAI>(veh);
		if (vai)
		{
			vehiclesMap[idVeh] = vai;
		}
		else
		{
			ErrF("NonAI vehicle where AI expected - %s",(const char *)veh->GetDebugName());
		}
	}

	RString name;
	if (ctx.IdxTransfer(indices->name, name) != TMOK) return NULL;
	if (name.GetLength() > 0)
	{
		veh->SetVarName(name);
		GWorld->GetGameState()->VarSet(name, GameValueExt(veh), true);
	}

	NetworkId objectId;
	if (ctx.IdxTransfer(indices->objectCreator, objectId.creator) != TMOK) return NULL;
	if (ctx.IdxTransfer(indices->objectId, objectId.id) != TMOK) return NULL;
	veh->SetNetworkId(objectId);
	veh->SetLocal(false);

	#if _ENABLE_REPORT
		if (dyn_cast<Transport>(veh))
		{
			bool IsUpdateTransport(NetworkMessageType type);
			DoAssert(IsUpdateTransport(veh->GetNMType(NMCUpdateGeneric)));
		}
	#endif

	return veh;
}

void Entity::DestroyObject()
{
	SetDelete();
}



TMError Entity::TransferMsg(NetworkMessageContext &ctx)
{
	switch (ctx.GetClass())
	{
	case NMCCreate:
		TMCHECK(NetworkObject::TransferMsg(ctx))
		if (ctx.IsSending())
		{
			Assert(dynamic_cast<const IndicesCreateVehicle *>(ctx.GetIndices()))
			const IndicesCreateVehicle *indices = static_cast<const IndicesCreateVehicle *>(ctx.GetIndices());

			RString type = GetName() ? GetName() : "";
			TMCHECK(ctx.IdxTransfer(indices->type, type))
			RString shape = _shape ? _shape->Name() : "";
			TMCHECK(ctx.IdxTransfer(indices->shape, shape))
			Vector3 &position = const_cast<Vector3 &>(Position());
			TMCHECK(ctx.IdxTransfer(indices->position, position))
			TMCHECK(ctx.IdxTransferRef(indices->hierParent, _hierParent))
		}
		break;
	case NMCUpdateGeneric:
		TMCHECK(base::TransferMsg(ctx))
		{
			Assert(dynamic_cast<const IndicesUpdateVehicle *>(ctx.GetIndices()))
			const IndicesUpdateVehicle *indices = static_cast<const IndicesUpdateVehicle *>(ctx.GetIndices());
			ITRANSF_ENUM(targetSide)
			if (ctx.IsSending())
			{
				AUTO_STATIC_ARRAY(float,animations,32);
				animations.Resize(_animations.Size());
				for (int i=0; i<_animations.Size(); i++) animations[i] = _animations[i].GetPhaseWanted();
				TMCHECK(ctx.IdxTransfer(indices->animations, animations))
			}
			else
			{
				AUTO_STATIC_ARRAY(float,animations,32);
				TMCHECK(ctx.IdxTransfer(indices->animations, animations))
				for (int i=0; i<_animations.Size(); i++)
				{
					if (i<animations.Size()) _animations[i].SetPhaseWanted(animations[i]);
				}
			}
		}
		break;
	case NMCUpdatePosition:
		TMCHECK(base::TransferMsg(ctx))
		{
			Assert(dynamic_cast<const IndicesUpdatePositionVehicle *>(ctx.GetIndices()))
			const IndicesUpdatePositionVehicle *indices = static_cast<const IndicesUpdatePositionVehicle *>(ctx.GetIndices());
			if (ctx.IsSending())
			{
				NetworkUpdEntityPos pos;
				//EncodedMatrix3 posO;
				//posO.Encode(Orientation());
				pos.orientation.Encode(Orientation());
				pos.position = Position();
				pos.speed = _speed;
				pos.angMomentum = _angMomentum;
				TMCHECK(ctx.IdxSendRaw(indices->entityUpdPos, &pos, sizeof(pos)));
			}
			else
			{
				void *data;
				int size;
				TMCHECK(ctx.IdxGetRaw(indices->entityUpdPos, data, size));
				if (size == sizeof(NetworkUpdEntityPos))
				{
					if (IsInLandscape())
					{
						// only entities that are really present in the lanscape
						// may receive updates
						const NetworkUpdEntityPos &pos = *(NetworkUpdEntityPos *)data;
						Matrix4 trans;
						pos.orientation.Decode(trans);
						trans.SetPosition(pos.position);
						Move(trans);
						_speed = pos.speed;
						_angMomentum = pos.angMomentum;
					}
					/*else
					{
						LogF
						(
							"Received update about entity %s - not in landscape",
							(const char *)GetDebugName()
						);
					}*/
				}
				else
				{
					Fail("Bad size of NetworkUpdEntityPos field");
				}
			}
			ITRANSF_ENUM(prec)
		}
		break;
	default:
		TMCHECK(base::TransferMsg(ctx))
		break;
	}
	return TMOK;
}

float Entity::CalculateError(NetworkMessageContext &ctx)
{
	float error = 0;
	switch (ctx.GetClass())
	{
	case NMCUpdateGeneric:
		error += base::CalculateError(ctx);
		{
			Assert(dynamic_cast<const IndicesUpdateVehicle *>(ctx.GetIndices()))
			const IndicesUpdateVehicle *indices = static_cast<const IndicesUpdateVehicle *>(ctx.GetIndices());

			AUTO_STATIC_ARRAY(float,animations,32);
			if (ctx.IdxTransfer(indices->animations, animations) == TMOK)
			{
				int size = animations.Size();
				saturateMin(size, _animations.Size());
				float animError = 0;
				for (int i=0; i<size; i++)
					animError += fabs(animations[i] - _animations[i].GetPhaseWanted());
				error += animError * ERR_COEF_VALUE_MAJOR;
			}
		}
		break;
	case NMCUpdatePosition:
		error += base::CalculateError(ctx);
		{
			Assert(dynamic_cast<const IndicesUpdatePositionVehicle *>(ctx.GetIndices()))
			const IndicesUpdatePositionVehicle *indices = static_cast<const IndicesUpdatePositionVehicle *>(ctx.GetIndices());
			{
				AutoArray<char> temp;
				if
				(
					ctx.IdxTransfer(indices->entityUpdPos, temp) == TMOK &&
					temp.Size()==sizeof(NetworkUpdEntityPos)
				)
				{
					NetworkUpdEntityPos &d = *(NetworkUpdEntityPos *)temp.Data();
					error += d.position.Distance(Position());
					error += d.orientation.DirectionUp().Distance(Orientation().DirectionUp());
					error += d.orientation.Direction().Distance(Orientation().Direction());

					float dt = Glob.time - ctx.GetMsgTime();
					error += dt * d.speed.Distance(Speed());
					error += dt * d.angMomentum.Distance(AngMomentum());
				}
			}
			ICALCERR_NEQ(int, prec, ERR_COEF_VALUE_MAJOR)

			// TODO: implementation
		}
		break;
	default:
		break;
	}
	return error;
}

void Entity::ScanContactPoints
(
	ContactArray &contacts, const Frame &moveTrans,
	SimulationImportance prec, float above,
	bool ignoreObjects
)
{
	// check collision on new position

	_objectContact=false;

	if (!ignoreObjects)
	{
		#define MAX_IN 0.2
		#define MAX_IN_FORCE 0.1
		#define MAX_IN_FRICTION 0.2


		CollisionBuffer collision;
		GLandscape->ObjectCollision(collision,this,moveTrans);

		for( int i=0; i<collision.Size(); i++ )
		{
			// info.pos is relative to object
			const CollisionInfo &info=collision[i];
			Object *obj=info.object;
			if( !obj ) continue;

			ContactPoint &contact = contacts.Append();
			contact.under = info.under;
			contact.pos = info.object->PositionModelToWorld(info.pos);
			contact.dirOut = info.object->DirectionModelToWorld(info.dirOut);
			contact.type = GroundSolid;
			contact.texture = info.texture;
			contact.obj = info.object;

			#if 0
			GScene->DrawCollisionStar(contact.pos,0.1,PackedColor(Color(0,1,0)));
			GScene->DrawCollisionStar(contact.pos+contact.dirOut*contact.under,0.1,PackedColor(Color(1,1,0)));
			#endif

			//LogF("obj %d: %.3f",i,info.under);

			_objectContact=true;
		}
	} // if( object collisions enabled )
	
	// check for collisions

	GroundCollisionBuffer gCollision;
	//float softFactor = 0.2; //floatMin(4000/GetMass(),2.0);

	if( prec>=SimulateVisibleFar )
	{
		GLandscape->GroundCollisionPlane(gCollision,this,moveTrans,above,0,false);
	}
	else
	{
		GLandscape->GroundCollision(gCollision,this,moveTrans,above,0,false);
	}

	_landContact=false;
	_waterContact=false;
	for( int i=0; i<gCollision.Size(); i++ )
	{
		const UndergroundInfo &info=gCollision[i];
		ContactPoint &contact = contacts.Append();
		contact.under = info.under;
		contact.pos = info.pos;
		contact.dirOut = Vector3(0,info.dZ,1).CrossProduct(Vector3(1,info.dX,0)).Normalized();
		contact.type = info.type;
		contact.texture = info.texture;
		contact.obj = NULL;

		#if 0
		GScene->DrawCollisionStar(contact.pos,0.1,PackedColor(Color(0,1,0)));
		GScene->DrawCollisionStar(contact.pos+contact.dirOut*contact.under,0.1,PackedColor(Color(1,1,0)));
		#endif

		if (contact.type==GroundWater)
		{
			_waterContact=true;
		}
		else
		{
			//LogF("lnd %d: %.3f",i,info.under);
			_landContact=true;
		}
	}
}

void Entity::ConvertContactsToFrictions
(
	const ContactArray &contacts, FrictionArray &frictions,
	const Frame &moveTrans,
	Vector3 &offset, Vector3 &force, Vector3 &torque,
	float crash, float maxColSpeed2
)
{
	offset = VZero;
	crash = 0;
	maxColSpeed2 = 0;

	#define MAX_UNDER 0.2
	const float maxUnderForce = 0.2;

	// scan all contact points
	// from all ground points select only the one nearest
	//to the center of mass in x,z axes
	// calculate minimal distance factor
	//float minDistXZ2 = FLT_MAX;
	int outForceFactorCount = 0;
	AUTO_STATIC_ARRAY(float,outForceFactor,256);
	// calculate distance to the nearest 
	for( int i=0; i<contacts.Size(); i++ )
	{
		const ContactPoint &info=contacts[i];
		if (info.type==GroundWater) continue;
		outForceFactorCount++;
	}
	float outForceFactorSum = 0;
	outForceFactor.Realloc(contacts.Size());
	outForceFactor.Resize(contacts.Size());
	if (outForceFactorCount>0)
	{
		// calculate distribution
		for( int i=0; i<contacts.Size(); i++ )
		{
			// info.pos is world space
			// select only 
			const ContactPoint &info=contacts[i];
			outForceFactor[i] = 0;
			if (info.type==GroundWater) continue;
			float factor = info.under;
			//LogF("under %i: %.2f, max %.2f",i,info.under,maxUnderForce);
			saturate(factor,0,maxUnderForce);
			outForceFactor[i] = factor*(2.0/maxUnderForce);
			outForceFactorSum += outForceFactor[i];
		}
		// keep outForceFactor in reasonable range
		// one point emmerged to maxUnderForce should be able to push 2
		if (outForceFactorSum>2)
		{
			// some point present
			float invSum = 2/outForceFactorSum;
			for( int i=0; i<contacts.Size(); i++ ) outForceFactor[i] *= invSum;
		}
	}

	Vector3 wCenter(VFastTransform,moveTrans.ModelToWorld(),GetCenterOfMass());

	#if LOG_FRIC
	float fricFactorSum = 0;
	float accelSum = 0;
	#endif
	// when forces are balanced, under should keep on value: 0.5*maxUnderForce
	for( int i=0; i<contacts.Size(); i++ )
	{
		// info.pos is world space
		// select only 
		const ContactPoint &info=contacts[i];
		// we consider two forces

		if( info.under<0 ) continue;

		float offsetInOutDir = info.dirOut*offset;
		saturateMax(offsetInOutDir,0);
		float under = info.under - offsetInOutDir;
		if( under<0 ) continue;

		Vector3 pCenter=info.pos-wCenter;

		// one is ground "pushing" everything out
		Vector3Val dirOut = info.dirOut;

		if( info.type==GroundWater )
		{
			// simulate water forces
			// force depends only on under-water volume
			// exact volume would help

			// estimate volume corresponding to each point
			float radius = GetRadius();
			//float volume = radius*radius*radius;
			float area = radius*radius;

			float areaPerPoint = area/_shape->GeometryLevel()->NPos();

			//Vector3 pForce = dirOut*under*areaPerPoint*10000;

			Vector3 pForce = dirOut*floatMin(under,radius)*areaPerPoint*10000;

			torque += pCenter.CrossProduct(pForce);
			force += pForce;

			float contactFactor = under*8/radius;

			// add friction point
			FrictionPoint &fp = frictions.Append();
			fp.frictionCoef = floatMin(contactFactor,1);
			fp.obj = info.obj;
			fp.outDir = dirOut;
			fp.pos = info.pos;

		}
		else
		{
			#if 1 // enable offseting (forced moving out of the object)
			if( MAX_UNDER<under )
			{
				offset += info.dirOut*(under-MAX_UNDER);
			}
			#endif

			under=floatMin(under,maxUnderForce);

			// sum of all outForceFactors is in range 0 .. 2
			float contactFactor = outForceFactor[i];

			// some friction is caused by moving the land aside
			// this applies only to soft surfaces

			float accelSize = 10*contactFactor;
			Vector3 pForce = dirOut*GetMass()*accelSize;

			#if LOG_FRIC
				accelSum += accelSize;
			#endif

			torque += pCenter.CrossProduct(pForce);
			force += pForce;


			#if ARROWS
				AddForce(wCenter+pCenter,pForce*InvMass(),Color(1,1,0));
			#endif

			// add friction point
			FrictionPoint &fp = frictions.Append();
			fp.frictionCoef = floatMin(contactFactor,1);
			fp.obj = info.obj;
			fp.outDir = dirOut;
			fp.pos = info.pos;
			//fp.angularFriction = 0.1*fricFactor;
			#if LOG_FRIC
				fricFactorSum += fp.frictionCoef;
			#endif
		}
	}
	#if LOG_FRIC
	Log("fricFactorSum %.2f, accelSum %.2f",fricFactorSum,accelSum);
	#endif


}

bool Entity::IsInLandscape() const
{
	// TODO: use only new logic
	return (MoveOutState)_moveOutState==MOIn;
}

Matrix4 Entity::WorldTransform() const
{
	// check if we are in landscape
	// if not, we should check our parent
	if ((MoveOutState)_moveOutState==MOIn) return Transform();
	if (!_hierParent)
	{
		LogF("%s: no _hierParent",(const char *)GetDebugName());
		return MIdentity;
	}
	return _hierParent->ProxyWorldTransform(this);
}

Vector3 Entity::WorldSpeed() const
{
	if ((MoveOutState)_moveOutState==MOIn) return ObjectSpeed();
	if (!_hierParent)
	{
		LogF("%s: no _hierParent",(const char *)GetDebugName());
		return VZero;
	}
	// TODO: add object speed to my speed
	return _hierParent->ObjectSpeed();
}


Matrix4 Entity::WorldInvTransform() const
{
	if ((MoveOutState)_moveOutState==MOIn) return GetInvTransform();

	if (!_hierParent)
	{
		LogF("%s: no _hierParent",(const char *)GetDebugName());
		return MIdentity;
	}
	// ask parent to get my position

	return _hierParent->ProxyInvWorldTransform(this);
}

Matrix4 Entity::ProxyWorldTransform(const Object *obj) const
{
	return Transform()*obj->Transform();
}

Matrix4 Entity::ProxyInvWorldTransform(const Object *obj) const
{
	return obj->CalcInvTransform()*GetInvTransform();
}

void Entity::ResetMoveOut()
{
	DoAssert((MoveOutState)_moveOutState==MOMovedOut);
	_moveOutState=MOIn;
}

void Entity::ResetStatus()
{
	base::ResetStatus();

	for (int i=0; i<_animations.Size(); i++)
		_animations[i].Init();
}
