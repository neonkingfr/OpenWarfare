/*!
	\file
	Implementation of World class
*/

#include "wpch.hpp"
#include "dikCodes.h"

#include "world.hpp"
#include "scene.hpp"
#include "engine.hpp"
#include "keyInput.hpp"
#include "lights.hpp"
#include "speaker.hpp"
#ifdef _WIN32
  #include "joystick.hpp"
#endif
#include "clipVert.hpp"

#include "landscape.hpp"
#include "visibility.hpp"
#include <El/Common/randomGen.hpp>
#include "camEffects.hpp"
#include "titEffects.hpp"
#include "scripts.hpp"
#include "debugTrap.hpp"

#include <El/Common/perfLog.hpp>
#include "perfProf.hpp"
#include "diagModes.hpp"

#include "progress.hpp"

//#include "allVehicles.hpp"
#include "seaGull.hpp"
#include "camera.hpp"

#include "gameStateExt.hpp"

#include "shots.hpp"
// #include "editor.hpp"
#include "ai.hpp"
#include "keyLights.hpp"

//#include "loadStream.hpp"
#include "threadSync.hpp"
// #include "animator.hpp"

#include "chat.hpp"

#include "network.hpp"

#include "arcadeTemplate.hpp"
#include "uiControls.hpp"
#include "uiMap.hpp"

#include <El/QStream/QBStream.hpp>
#include "paramArchive.hpp"
#include "objectClasses.hpp"

#include "detector.hpp"

#include "stringtableExt.hpp"

/*
#if _CZECH
#include "roxxe.hpp"
#endif
*/

extern class ParamFile Res;

RString GetTmpSaveDirectory();

#undef GetObject
#undef DrawText
// #undef LoadString

#include "resincl.hpp"

bool DisableTextures=false;

extern bool LandEditor;
extern bool NoLandscape;

#if _ENABLE_REPORT
	#define LOG_ADD_REMOVE_VEHICLE 0
#endif


#define PERF_SIM 0

#if 0 //_DEBUG
	static bool timeJumpPreview=true; // TODO: move to class World
#else
	static bool timeJumpPreview=false;
#endif

extern bool EnableHWTL;

Person *World::PlayerOn() const
{
	return _playerOn;
}
void World::SwitchPlayerTo(Person *veh)
{
	_playerOn = veh;
	#if !_RELEASE
		Log("SwitchPlayerTo %s",(const char *)veh->GetDebugName());
	#endif
	SetActiveChannels();
}
Person *World::GetRealPlayer() const
{
	return _realPlayer;
}
void World::SetRealPlayer(Person *veh)
{
	_realPlayer = veh;
}

RadioChannel &World::GetRadio() {return *_radio;}
const RadioChannel &World::GetRadio() const {return *_radio;}

void World::SetSpeaker(RString speaker, float pitch)
{
	const ParamEntry &cfg = Pars >> "CfgVoice";
	const ParamEntry &entry = cfg >> speaker;
	Ref<BasicSpeaker> basic = new BasicSpeaker(entry);
	_speaker = new Speaker(basic, pitch); 
}

AIUnit *World::FocusOn() const
{
	EntityAI *vehicle=dyn_cast<EntityAI>(CameraOn());
	if( !vehicle ) return NULL;
	Person *player=PlayerOn();
	AIUnit *unit=( player ? player->CommanderUnit() : NULL );
	if( !unit || unit->GetVehicle()!=vehicle ) unit=vehicle->CommanderUnit();
	if( !unit ) return NULL;
	if( !unit->GetPerson() ) return NULL;
	return unit;
}
// command line argument

extern char LoadFile[];
extern const char DefLoadFile[];

#ifdef _MSC_VER
	#pragma warning( disable: 4355 )
#endif

World *GWorld;

#define BACKGROUND_AI 0

World::World( Engine *engine, bool editor )
:_engine(engine),_editor(editor),
_camTypeMain(CamInternal), _camType(CamInternal),
_gridOffsetX(0), _gridOffsetY(0)
{
	#define PRINT_SIZE(type) Log(#type " size %d",sizeof(type))
	PRINT_SIZE(Object);
	PRINT_SIZE(Frame);
	PRINT_SIZE(RemoveLinks);
	PRINT_SIZE(OLink<Object>);
	PRINT_SIZE(Entity);
	PRINT_SIZE(LODShapeWithShadow);
	PRINT_SIZE(Shape);
	if( editor ) _camType=_camTypeMain=CamInternal;
	#if _PIII
		Assert( ((int)this&0xf)==0 );
	#endif
	#if BACKGROUND_AI
	_secThread=new SecondaryThread;
	#endif

	_radio = new RadioChannel(CCGlobal, NULL, RNRadio);
//	_speaker = toIntFloor(1000 * GRandGen.RandomValue()) + 1;
	SetSpeaker((Pars >> "CfgVoice" >> "voices")[0], 1.0);

	_scene.Init(_engine,NULL);

	GLandscape=_scene.GetLandscape();
	GScene=GetScene();
	
	_firstFrame = false;
	_latitude = -40*(H_PI/180); // -40 - Croatia, -90 - north pole;
	_longitude = +15*(H_PI/180);

	Vector3 lightDirection(+1,-0.5,+1);
	LightSun *mainLight=new LightSun();

#ifdef _WIN32
	SYSTEMTIME time;
	GetLocalTime(&time);
	GRandGen.SetSeed(GlobalTickCount()+time.wSecond);
#else
	GRandGen.SetSeed(GlobalTickCount()+(unsigned)time(NULL));
#endif
	Glob.clock.SetTimeInYear(8*OneHour+130*OneDay);
	mainLight->Recalculate(this);
	_timeToSkip=0;

	_scene.SetMainLight(mainLight);
	_scene.MainLightChanged();
	_noDisplay = false;
	_enableSimulation = true;
	_simulationFocus = true;
	#if !_DISABLE_GUI
	if (!editor) CreateMainOptions();
	#endif

	_userInputDisabled = 0;

	Clear();

	/*
	// all centers are already created in Clear
	_eastCenter=new AICenter(TEast,AICMDisabled);
	_westCenter=new AICenter(TWest,AICMDisabled);
	_guerrilaCenter=new AICenter(TGuerrila,AICMDisabled);
	_civilianCenter=new AICenter(TCivilian,AICMDisabled);
	_logicCenter=new AICenter(TLogic,AICMDisabled);
	*/
	_mode = GModeArcade;

	_cameraExternal = false;

	_showCompass = false;
	_showWatch = false;

	_forceMap = false;

	if ( GSoundScene ) GSoundScene->Reset();
}

#ifdef _MSC_VER
	#pragma warning( default: 4355 )
#endif

void World::InitGeneral()
{
	_wantedOvercast=0.3;
	_actualOvercast=_wantedOvercast;
	_wantedFog=0.0;
	_actualFog=_wantedFog;
	_speedOvercast=1.0/(60*60);
	_speedFog=1.0/(60*60);
	_weatherTime=0;
	_nextWeatherChange=0;
	_firstFrame = true;

	Glob.clock.SetTimeInYear(8*OneHour+130*OneDay);
	LightSun *sun=_scene.MainLight();
	sun->Recalculate(this);
}

/*!
\patch 1.01 Date 6/11/2001 by Ondra.
- Fixed: initial night state for target tracking
- this bug caused much better visible enemies during night world initalization 
*/

void World::InitGeneral( const ParamEntry &cfg )
{
	_actualOvercast=cfg>>"startWeather";
	_wantedOvercast=cfg>>"forecastWeather";
	_actualFog=cfg>>"startFog";
	_wantedFog=cfg>>"forecastFog";
	_speedOvercast=fabs(_wantedOvercast-_actualOvercast)/(30*60);
	_speedFog=fabs(_wantedFog-_actualFog)/(30*60);
	_weatherTime=0;
	_nextWeatherChange=30*60;
	_nearImportanceDistributionTime=Time(0); // when distibutions were calculated
	_farImportanceDistributionTime=Time(0); // when distibutions were calculated
	_firstFrame = true;

	RString time=cfg>>"startTime";
	RString date=cfg>>"startDate";
	int year;
	float timeInYear = Glob.clock.ScanDateTime(date, time, year);
	if (year < 100) year += 1900;
	Glob.clock.SetTimeInYear(timeInYear);
	Glob.clock.SetYear(year);
	LightSun *sun=_scene.MainLight();
	sun->Recalculate(this);
	// FIX _scene.MainLightChanged() was missing
	_scene.MainLightChanged();
	Log("World set time %s, %s",(const char *)date,(const char *)time);
	// note: year is ignored

}

//static const int daysInMonth[]={31,28,31,30,31,30,31,31,30,31,30,31};
int GetDaysInMonth(int year, int month);

void World::InitGeneral( ArcadeIntel &intel )
{
	_actualOvercast=intel.weather;
	_wantedOvercast=intel.weatherForecast;
	_actualFog=intel.fog;
	_wantedFog=intel.fogForecast;
	_speedOvercast=fabs(_wantedOvercast-_actualOvercast)/(30*60);
	_speedFog=fabs(_wantedFog-_actualFog)/(30*60);
	_weatherTime=0;
	_nextWeatherChange=30*60;
	_nearImportanceDistributionTime=Time(0); // when distibutions were calculated
	_farImportanceDistributionTime=Time(0); // when distibutions were calculated

	int day = intel.day - 1;
	int year = intel.year;
	for (int m=0; m<intel.month-1; m++) day += GetDaysInMonth(year, m);
	float time = intel.hour * OneHour + intel.minute * OneMinute + day * OneDay + 0.5 * OneSecond;
	Glob.clock.SetTimeInYear(time);
	Glob.clock.SetYear(year);
	LightSun *sun=_scene.MainLight();
	sun->Recalculate(this);
	// FIX _scene.MainLightChanged() was missing
	_scene.MainLightChanged();
	// note: year is ignored
}

bool World::GetILS( Vector3 &pos, Vector3 &dir, TargetSide side )
{
	// TODO: find nearest
	pos=_ilsPosWest;
	dir=_ilsDirWest;
	return true;
}

const AutoArray<Vector3> World::GetTaxiInPath( TargetSide side )
{
	return _taxiInPathsWest;
}

const AutoArray<Vector3> World::GetTaxiOffPath( TargetSide side )
{
	return _taxiOffPathsWest;
}

#define PLAYER_ONLY 0


void World::InsertSeaGulls()
{
	return;

	#if 0

	int nGullC=100;
	while( --nGullC>=0 )
	{
		Point3 posC;
		SeaGullAuto *seaGullC=new SeaGullAuto(new SeaGullPilotCommander);
		// find some position on sea shore

		posC.Init();
		for( int attempt=100; -- attempt>=0; )
		{
			const float maxFly=LandRange*LandGrid;
			const float borderFly=LandRange/4*LandGrid;
			posC[0]=GRandGen.RandomValue()*(maxFly-2*borderFly)+borderFly;
			posC[2]=GRandGen.RandomValue()*(maxFly-2*borderFly)+borderFly;
			// find some square with shallow water flag
			int xc=toIntFloor(posC[0]*InvLandGrid);
			int zc=toIntFloor(posC[2]*InvLandGrid);
			if( !InRange(xc,zc) ) continue; // just for sure
			GeographyInfo geog=GLOB_LAND->GetGeography(xc,zc);
			if( geog.shallowWater ) break;
		}
		posC=_scene.GetLandscape()->PointOnSurface(posC[0],80,posC[2]);
		seaGullC->SetPosition(posC);
		AddAnimal(seaGullC);
		int nGullW=4;
		//int nGullW=0;
		//_cameraOn=seaGullC;
		while( --nGullW>=0 )
		{
			float offX=GRandGen.RandomValue()*20-10;
			float offZ=GRandGen.RandomValue()*20-10;
			Point3 posW=_scene.GetLandscape()->PointOnSurface(posC.X()+offX,50,posC.Z()+offZ);
			SeaGull *seaGullW=new SeaGullAuto(new SeaGullPilotWingman(seaGullC));
			seaGullW->SetPosition(posW);
			AddAnimal(seaGullW);
			//AddAnimal(seaGullW,posW);
		}
	}
	#endif
}

// define environmental sounds

const int NEnvSoundPars=6;
SoundPars EnvSoundPars[NEnvSoundPars];
SoundPars EnvSoundParsNight[NEnvSoundPars];

static void InitEnvSounds( const ParamEntry &cfg )
{
	GetValue(EnvSoundPars[0], cfg>>"Sea">>"sound");
	GetValue(EnvSoundPars[1], cfg>>"Trees">>"sound");
	GetValue(EnvSoundPars[2], cfg>>"Meadows">>"sound");
	GetValue(EnvSoundPars[3], cfg>>"Hills">>"sound");
	GetValue(EnvSoundPars[4], cfg>>"Rain">>"sound");
	GetValue(EnvSoundPars[5], cfg>>"Default">>"sound");
	GetValue(EnvSoundParsNight[0], cfg>>"Sea">>"soundNight");
	GetValue(EnvSoundParsNight[1], cfg>>"Trees">>"soundNight");
	GetValue(EnvSoundParsNight[2], cfg>>"Meadows">>"soundNight");
	GetValue(EnvSoundParsNight[3], cfg>>"Hills">>"soundNight");
	GetValue(EnvSoundParsNight[4], cfg>>"Rain">>"soundNight");
	GetValue(EnvSoundParsNight[5], cfg>>"Default">>"sound");
}

SRef<EntityAI> GDummyVehicle;
void AIGlobalInit();

//void World::InitEditor( Landscape *landscape, Entity *cursor )

void World::InitEditor( Landscape *landscape, Entity *cursor )
{
	_scene.Init(_engine,landscape);

	GScene=GetScene();
	GLandscape=_scene.GetLandscape();

	InitGeneral();


	InitFinish();
	_scene.GetLandscape()->InitObjectVehicles();

	VehicleTypes.Preload();

	EnvSoundPars[5].name = "SOUND\\$DEFAULT$.WSS";
	EnvSoundParsNight[5].name = "SOUND\\$DEFAULT$.WSS";

	const char *ext=strrchr(LoadFile,'.');
	if( ext && QIFStreamB::FileExist(LoadFile) )
	{
		if( !strcmpi(ext,".p3d") )
		{
			ReloadViewer(LoadFile,"");
			InitCameraPars();

			GLOB_ENGINE->ReinitCounters();
			return;
		}
		else
		{
			SwitchLandscape(LoadFile);
		}
	}

	{
		Vector3 cPos(2525,5,2925);
		cursor->SetPosition(cPos);
		AddVehicle(cursor);
		GetGameState()->VarSet("bis_buldozer_cursor", GameValueExt(cursor), true);
		_cameraOn=cursor;

		InitCameraPars();
		_scene.GetLandscape()->InitGeography();
	}
	
	RString initScript = RString("scripts\\editor.sqs");
	if (QIFStreamB::FileExist(initScript))
	{
		Script *script = new Script("editor.sqs", GameValue(), INT_MAX);
		AddScript(script);
		SimulateScripts();
	}
}

void World::InitCameraPars()
{
	Camera camera;
	if( _cameraOn!=NULL )
	{
		for( int i=0; i<MaxCameraType; i++ )
		{
			_cameraOn->InitVirtual((CameraType)i,_camHeading[i],_camDive[i],_camFOV[i]);
			_camHeadingWanted[i]=_camHeading[i];
			_camDiveWanted[i]=_camDive[i];
			_camFOVWanted[i]=_camFOV[i];
		}
		Matrix4 transform;

		transform = _cameraOn->Transform()*_cameraOn->InsideCamera(_camType);
		transform.SetOrientation(transform.Orientation());

		camera.SetTransform(transform);
		camera.SetSpeed(_cameraOn->ObjectSpeed());
		float fov=_camFOV[_camType];
		// normal soldier fov is about 0.85
		float cNear = 0.067f/fov;
		saturate(cNear,0.07f,0.2f);


		AspectSettings as;
		GEngine->GetAspectSettings(as);
		camera.SetPerspective(cNear,_scene.GetFogMaxRange(),fov*as.leftFOV,fov*as.topFOV);
		//LogF("Camera %s fov %.3f",_cameraOn->GetShape()->Name(),fov);


		// note: same switch as in should be applied here
		// we start with assuming internal camera

		// World::Simulate - section "no camera effect - use normal camera"

		camera.Adjust(GEngine);
		_scene.SetCamera(camera);
	}
}

void World::FreelookChange(bool active)
{
	if (!active)
	{
		// when free-look goes off, reset cursor to forward in some situations
		OLink<Object> cameraVehicle=_cameraOn;

		if
		(
			cameraVehicle &&
			!cameraVehicle->IsContinuous(_camType)
			&& !cameraVehicle->IsExternal(_camType)
			&& !cameraVehicle->IsVirtualX(_camType)
		)
		{
			if (_ui)
			{
				const Camera &camera = *GScene->GetCamera();
				_ui->SetCursorDirection(camera.Direction());
				_ui->SetCursorMode(false);
			}
		}


	}
	// 
}

void RunInitScript();

inline AICenterMode TranslateMode(GameMode gameMode)
{
	switch (gameMode)
	{
		case GModeNetware:
			return AICMNetwork;
		case GModeIntro:
			return AICMIntro;
		case GModeArcade:
		default:
			return AICMArcade;
	}
}

/*!
\patch 1.85 Date 9/16/2002 by Ondra
- Fixed: When addWeapon was followed by addMagazine, AI did not reload weapon.
\patch 1.85 Date 9/16/2002 by Ondra
- Fixed: Backup units in Resistance mission Hostages often did not fire.
*/

void World::InitCenter(AICenterMode mode, AICenter *cnt)
{
	for (int i=0; i<cnt->NGroups(); i++)
	{
		AIGroup *grp = cnt->GetGroup(i);
		if (!grp) continue;
		for (int j=0; j<MAX_UNITS_PER_GROUP; j++)
		{
			AIUnit *unit = grp->UnitWithID(j + 1);
			if (!unit) continue;
			unit->GetPerson()->GetInfo()._initExperience = unit->GetPerson()->GetInfo()._experience;
			if (unit->IsFreeSoldier()) unit->GetPerson()->ResetMovement(0);
			if (unit->IsUnit()) unit->GetVehicle()->AutoReloadAll();
		}
		grp->GetRadio().SilentProcess();
	}
	if (mode == AICMNetwork)
	{
		GetNetworkManager().CreateCenter(cnt);
	}
}
bool World::InitVehicles( GameMode gameMode, ArcadeTemplate &t )
{
	// should always be called after SwitchLandscape
	ProgressReset();
	_firstFrame = true;

	Display *disp = new Display(NULL);
	disp->Load("RscDisplayLoadMission");
	disp->SetCursor(NULL);
	CStatic *cName = static_cast<CStatic *>(disp->GetCtrl(IDC_LOAD_MISSION_NAME));
	CStatic *cDate = static_cast<CStatic *>(disp->GetCtrl(IDC_LOAD_MISSION_DATE));
	RString name;
	bool showTime;
	if (gameMode == GModeIntro)
	{
		const ParamEntry *entry = ExtParsMission.FindEntry("onLoadIntro");
		if (entry)
			name = *entry;
		else
			name = LocalizeString(IDS_LOAD_INTRO);
		entry = ExtParsMission.FindEntry("onLoadIntroTime");
		if (entry)
			showTime = *entry;
		else
			showTime = false;
	}
	else
	{
		const ParamEntry *entry = ExtParsMission.FindEntry("onLoadMission");
		if (entry)
			name = *entry;
		else
			name = LocalizeString(IDS_LOAD_MISSION);
		entry = ExtParsMission.FindEntry("onLoadMissionTime");
		if (entry)
			showTime = *entry;
		else
			showTime = true;
	}
	cName->SetText(name);

	if (showTime)
	{
		char buffer[256];
		Glob.clock.FormatDate(LocalizeString(IDS_DATE_FORMAT), buffer);	
		float time = Glob.clock.GetTimeOfDay();
		if (time > 1.0) time--;
		time *= 24;
		int hour = toIntFloor(time);
		time -= hour;
		time *= 60;
		int min = toIntFloor(time);
		sprintf(buffer + strlen(buffer), ", % 2d:%02d", hour, min);
		cDate->SetText(buffer);
	}
	else
	{
		cDate->SetText("");
	}
	
	ProgressStart(disp);

	_mode = gameMode;
	_endMission = EMContinue;
	EnableRadio();

	AdjustSubdivision(gameMode);

	ArcadeUnitInfo *uInfo = t.FindPlayer(); 
	if (uInfo)
	{
		Glob.header.playerSide = (TargetSide)uInfo->side;
	}

	if (_ui) _ui->ResetHUD();

	if (!GDummyVehicle) GDummyVehicle = NewVehicle("PaperCar");

	DestroyUserDialog();

	ProgressRefresh();

	AIGlobalInit();

	AICenterMode mode = TranslateMode(gameMode);
	ProgressRefresh();
	_eastCenter.Free();
	_westCenter.Free();
	_civilianCenter.Free();
	_guerrilaCenter.Free();
	_logicCenter.Free();

	_eastCenter = ::CreateCenter(t, TEast, mode);
	ProgressRefresh();
	Assert( CheckVehicleStructure() );

	_westCenter = ::CreateCenter(t, TWest, mode);
	ProgressRefresh();
	Assert( CheckVehicleStructure() );

	_guerrilaCenter = ::CreateCenter(t, TGuerrila, mode);
	ProgressRefresh();
	Assert( CheckVehicleStructure() );

	_civilianCenter = ::CreateCenter(t, TCivilian, mode);
	ProgressRefresh();
	Assert( CheckVehicleStructure() );

	_logicCenter = ::CreateCenter(t, TLogic, mode);
	ProgressRefresh();
	Assert( CheckVehicleStructure() );

	AUTO_STATIC_ARRAY(VehicleInitCmd, inits, 128);
	
	if (_eastCenter) _eastCenter->Init(t, inits);
	ProgressRefresh();
	if (_westCenter) _westCenter->Init(t, inits);
	ProgressRefresh();
	if (_guerrilaCenter) _guerrilaCenter->Init(t, inits);
	ProgressRefresh();
	if (_civilianCenter) _civilianCenter->Init(t, inits);
	ProgressRefresh();
	if (_logicCenter) _logicCenter->Init(t, inits);
	ProgressRefresh();
	InitNoCenters(t, inits, mode == AICMNetwork);

	Assert( CheckVehicleStructure() );

	for (int i=0; i<inits.Size(); i++)
	{
		GGameState.VarSet("this", GameValueExt(inits[i].vehicle), true);
		GGameState.Execute(inits[i].init);
		Assert( CheckVehicleStructure() );
	}

	if (gameMode == GModeArcade) RunInitScript();
	Assert( CheckVehicleStructure() );

	for (int i=0; i<inits.Size(); i++)
	{
		inits[i].vehicle->InitUnits();
		Assert( CheckVehicleStructure() );
		// init transport channel?
		//inits[i].vehicle->GetRadio().SilentProcess();
	}

	Assert( CheckVehicleStructure() );
	if (_eastCenter)
	{
		InitCenter(mode,_eastCenter);
	}
	if (_westCenter)
	{
		InitCenter(mode,_westCenter);
	}
	if (_guerrilaCenter)
	{
		InitCenter(mode,_guerrilaCenter);
	}
	if (_civilianCenter)
	{
		InitCenter(mode,_civilianCenter);
	}
	if (_logicCenter)
	{
		InitCenter(mode,_logicCenter);
	}

	Assert( CheckVehicleStructure() );

	if (gameMode == GModeNetware)
	{
		for (int i=0; i<inits.Size(); i++)
		{
			GetNetworkManager().VehicleInit(inits[i]);
		}
	}

	if (_eastCenter) _eastCenter->InitSensors();
	if (_westCenter) _westCenter->InitSensors();
	if (_guerrilaCenter) _guerrilaCenter->InitSensors();
	if (_civilianCenter) _civilianCenter->InitSensors();
	// if (_logicCenter) _logicCenter->InitSensors();
	ProgressRefresh();
	GetSensorList()->UpdateAll();
	if (_eastCenter) _eastCenter->InitSensors(true);
	if (_westCenter) _westCenter->InitSensors(true);
	if (_guerrilaCenter) _guerrilaCenter->InitSensors(true);
	if (_civilianCenter) _civilianCenter->InitSensors(true);
	// if (_logicCenter) _logicCenter->InitSensors(false);

	ProgressRefresh();
	// new data ready - adapt texture cache
	// unlock all types and textures
	VehicleTypes.UnlockAllTypes(); // locked from SwitchLandscape
	GEngine->TextBank()->UnlockAllTextures();
	extern void ManCompact();
	ManCompact();

	_engine->TextBank()->Preload();
	Shapes.OptimizeAll();

	_sensorList->UpdateAll();
	EntityAI *ai=dyn_cast<EntityAI, Object>(_cameraOn);
	if (_ui && ai)
	{
		_ui->ResetVehicle(ai);
	}
	ProgressRefresh();

	EnvSoundPars[5].name = "SOUND\\$DEFAULT$.WSS";
	EnvSoundParsNight[5].name = "SOUND\\$DEFAULT$.WSS";

	ProgressFinish();
	GLOB_ENGINE->ReinitCounters();

	return GetMaxError()<EMError;
}

void World::InitClient()
{
	_mode = GModeNetware;
	_endMission = EMContinue;
	EnableRadio();

	if (_ui) _ui->ResetHUD();

	if (!GDummyVehicle) GDummyVehicle = NewVehicle("PaperCar");

	AIGlobalInit();

	_eastCenter = NULL;
	_westCenter = NULL;
	_guerrilaCenter = NULL;
	_civilianCenter = NULL;
	_logicCenter = NULL;

	EnvSoundPars[5].name = "SOUND\\$DEFAULT$.WSS";
	EnvSoundParsNight[5].name = "SOUND\\$DEFAULT$.WSS";
}

VehicleType *NewAIType( const char *name )
{
	EntityType *vType=VehicleTypes.New(name);
	if( !vType ) return NULL;
	EntityAIType *type=dynamic_cast<EntityAIType *>(vType);
	if( !type )
	{
		ErrF("Type %s is not EntityAIType",(const char *)vType->GetName());
	}
	return type;
}

static void GetPosArray( AutoArray<Vector3> &tgt, const ParamEntry &cfg )
{
	tgt.Resize(0);
	for (int i=0; i<cfg.GetSize()-1; i+=2)
	{
		float x = cfg[i];
		float z = cfg[i+1];
		float y = GLandscape->SurfaceYAboveWater(x,z);
		Vector3 &pos = tgt[tgt.Add()];
		pos = Vector3(x,y,z);
	}
}

void World::InitLandscape( Landscape *landscape )
{
	// preload vehicle types
	_preloadedVType[VTypeStatic]=NewAIType("Static");
	_preloadedVType[VTypeBuilding]=NewAIType("Building");
	_preloadedVType[VTypeStrategic]=NewAIType("Strategic");
	_preloadedVType[VTypeNonStrategic]=NewAIType("NonStrategic");
	//_preloadedVType[VTypeObjective]=NewAIType("Objective");
	_preloadedVType[VTypeTarget]=NewAIType("Target");
	//_preloadedVType[VTypePrimaryObjective]=NewAIType("PrimaryObjective");
	//_preloadedVType[VTypeSecondaryObjective]=NewAIType("SecondaryObjective");
	_preloadedVType[VTypeAllVehicles]=NewAIType("AllVehicles");
	_preloadedVType[VTypeAir]=NewAIType("Air");
	_preloadedVType[VTypePlane]=NewAIType("Plane");
	_preloadedVType[VTypeShip]=NewAIType("Ship");
	_preloadedVType[VTypeBigShip]=NewAIType("BigShip");		
	_preloadedVType[VTypeAPC]=NewAIType("APC");		
	_preloadedVType[VTypeTank]=NewAIType("Tank");		
	_preloadedVType[VTypeCar]=NewAIType("Car");		
	_preloadedVType[VTypeMan]=NewAIType("Man");		

	// remove all vehicles
	Clear();

	if (!LandEditor || (Pars>>"CfgWorlds").FindEntry(Glob.header.worldname))
	{
		const ParamEntry &world=Pars>>"CfgWorlds">> Glob.header.worldname;
		InitGeneral(world);

		ProgressRefresh();

		// get ils position / direction
		const ParamEntry &ilsPos=world>>"ilsPosition";
		const ParamEntry &ilsDir=world>>"ilsDirection";
		Vector3 mainIlsPos; // there is currently only main airport (if any)
		Vector3 mainIlsDir;
		mainIlsPos.Init();
		mainIlsDir.Init();
		mainIlsPos[0]=ilsPos[0];
		mainIlsPos[2]=ilsPos[1];
		mainIlsPos[1]=GLOB_LAND->SurfaceY(mainIlsPos[0],mainIlsPos[2]);
		mainIlsDir[0]=ilsDir[0];
		mainIlsDir[1]=ilsDir[1];
		mainIlsDir[2]=ilsDir[2];
		mainIlsDir.Normalize();
		// TODO: multiple airports

		_ilsPosWest=mainIlsPos;
		_ilsDirWest=mainIlsDir;


		// create taxi-in and taxi-off  paths 
		GetPosArray(_taxiInPathsWest,world>>"ilsTaxiIn");
		GetPosArray(_taxiOffPathsWest,world>>"ilsTaxiOff");
	}

	_scene.Init(_engine,landscape);

	ProgressRefresh();
	
	if (!_ui) _ui=CreateInGameUI();
	
	GScene=GetScene();
	GLandscape=_scene.GetLandscape();

	if (!LandEditor || (Pars>>"CfgWorlds").FindEntry(Glob.header.worldname))
	{
		const ParamEntry &world=Pars>>"CfgWorlds">> Glob.header.worldname;
		_scene.GetLandscape()->InitDynSounds(world>>"Sounds");

		ProgressRefresh();

		_scene.GetLandscape()->InitGeography();

		ProgressRefresh();

		InitEnvSounds(world>>"EnvSounds");

		ProgressRefresh();

		InsertSeaGulls(); // sea-gulls IDs could be confusing
	}

	ProgressRefresh();

	// init all vehicles (load type information)
	InitFinish();
	_scene.GetLandscape()->InitObjectVehicles();
	//CreateMainOptions();

	ProgressRefresh();

	_showMap = false;
	_forceMap = false;
	// CreateMainMap();
	Log("InitLandscape ResetIDs");
	ResetIDs();
}

void World::InitFinish()
{
	//TrackingCamera *track=new TrackingCamera();
	//_trackingCamera=track;
	//AddVehicle(track);
	//track->LinkTo(_cameraOn);

	InitCameraPars();

	_scene.GetLandscape()->MakeShadows(_scene);

	const ParamEntry &entry=Pars>>"CfgEnvSounds">>"Sea">>"sound";
	RString waveName=GetSoundName(entry[0]);
	float duration = GSoundsys->GetWaveDuration(waveName);
	if( duration>0.1 ) _scene.GetLandscape()->SetSeaWaveSpeed(1/duration);
	else  _scene.GetLandscape()->SetSeaWaveSpeed(1/2.0);
}

/*
enum KillState {KillNone,KillExternal,KillViewKiller,KillBody};

static UITime KillTime; // TODO: move to global??
static bool Killed;
static OLink<EntityAI> Killer;
static OLink<EntityAI> KilledVehicle;
static KillState KillCamState;
*/

const GridInfo *World::GetGridInfo(float zoom) const
{
	for (int i=0; i<_gridInfo.Size(); i++)
	{
		if (zoom <= _gridInfo[i].zoomMax) return &_gridInfo[i];
	}
	Fail("Grid info");
	return NULL;
}

template <class Type>
Type LoadValue
(
	const ParamEntry &cfg, const char *name, const Type &defVal
)
{
	return cfg.ReadValue(name,defVal);
}

void World::ParseCfgWorld()
{
	const ParamEntry &cls = Pars >> "CfgWorlds" >> Glob.header.worldname;
	
	_latitude = LoadValue(cls, "latitude", -40) *(H_PI/180); // -40 - Croatia, -90 - north pole;
	_longitude = LoadValue(cls, "longitude", +15) *(H_PI/180);

	// parse grid parameters
	const ParamEntry &clsGrid = cls >> "Grid";
	_gridOffsetX = clsGrid >> "offsetX";
	_gridOffsetY = clsGrid >> "offsetY";
	_gridInfo.Resize(0);
	for (int i=0; i<clsGrid.GetEntryCount(); i++)
	{
		const ParamEntry &entry = clsGrid.GetEntry(i);
		if (entry.IsClass())
		{
			int index = _gridInfo.Add();
			GridInfo &info = _gridInfo[index];
			info.zoomMax = entry >> "zoomMax";
			info.format = entry >> "format";
			info.formatX = entry >> "formatX";
			info.formatY = entry >> "formatY";
			info.stepX = entry >> "stepX";
			info.stepY = entry >> "stepY";
			info.invStepX = info.stepX == 0 ? 0 : 1.0f / info.stepX;
			info.invStepY = info.stepY == 0 ? 0 : 1.0f / info.stepY;
		}
	}
	_gridInfo.Compact();
}

extern RString GBriefingOnPlan;
extern RString GBriefingOnNotes;
extern RString GBriefingOnGear;
extern RString GBriefingOnGroup;
extern RString GMapOnSingleClick;

// this number is dependent on CPU/HW T&L performance

//float PreferredGridSizeSWTL = 50;
//float PreferredGridSizeHWTL = 12.5;
const float PreferredGridSizeMP = 25;

void World::CleanUpDeinit()
{
	// clean up world - delete any simulation results, states and temps
	_timeToSkip=0;

	_cameraEffect.Free();
	_players.Clear();
	Log("CleanUp world");

	// title and cut effect should be destroyed when closing the missions
	// they may be used for ProgressScript
	extern Ref<Script> ProgressScript;
	if (!ProgressScript)
	{
		_titleEffect.Free();
		_cutEffect.Free();
		if ( GSoundScene ) GSoundScene->StopMusicTrack();
	}

	_scripts.Clear();
	_playerSuspended = false;

	_sensorList.Free();
	_sensorList=new SensorList;
/*
	_gameState.Free();
	_gameState=new GameStateExt;
	if (_gameState) _gameState->Reset(); // virtual initialization
*/
	GGameState.Reset();

	GBriefingOnPlan = RString();
	GBriefingOnNotes = RString();
	GBriefingOnGear = RString();
	GBriefingOnGroup = RString();
	GMapOnSingleClick = RString();
	
	GScene->CleanUp();

	GDummyVehicle = NULL;

	// destroy all non-primary vehicle objects?

	// clear all vehicles
	_vehicles.Clear();
	_fastVehicles.Clear();
	_animals.Clear();
	_buildings.Clear();
	_outVehicles.Clear();
	_cloudlets.Clear();
	// clear AI
	_eastCenter.Free();
	_westCenter.Free();
	_civilianCenter.Free();
	_guerrilaCenter.Free();
	_logicCenter.Free();

	extern RefArray<Object> MapDiags;
	MapDiags.Clear();

	// unload all currently unreferenced textures
	_engine->TextBank()->FlushTextures();

}

void ResetErrors();

void World::CleanUpInit()
{
	_nextMagazineID = 0;

	if (GLandscape)
	{
		_eastCenter=new AICenter(TEast,AICMDisabled);
		_westCenter=new AICenter(TWest,AICMDisabled);
		_guerrilaCenter=new AICenter(TGuerrila,AICMDisabled);
		_civilianCenter=new AICenter(TCivilian,AICMDisabled);
		_logicCenter=new AICenter(TLogic,AICMDisabled);
	}

	ResetErrors();
	_players.Clear();

	// DestroyMap(IDC_OK);
	_showMap = false;
	_forceMap = false;
	// delete all pilots

	// reset all variable parameters
	Glob.clock.SetTimeInYear(8*OneHour+130*OneDay);
	_scene.MainLight()->Recalculate(this);
	_scene.MainLightChanged();

	for( int i=0; i<MaxCameraType; i++ )
	{
		_camHeading[i]=_camDive[i]=0;
		_camHeadingWanted[i]=_camHeadingWanted[i]=0;
		_camNear[i]=1.0;
		_camMaxDist[i] = 1e10; // smooth camera distance
	}
	_camType=_camTypeMain=CamInternal; // reset camera
	_cameraExternal = false;
	//if( _editor ) _camType=_camTypeMain=CamInternal;
	
	_cameraEffect = NULL;

	_playerManual=false;
	_playerSuspended=false;
	
	#if PROFILE
		_acceleratedTime=0.5;
	#else
		_acceleratedTime=1.0;
	#endif

	Glob.time=Time(0);
	_channelChanged = UITIME_MIN;

	// good time to clean up allocator
	// now there should quite a lot of free memory
#ifdef _WIN32
	FastCAlloc::CleanUpAll();
#endif
}

void World::CleanUp()
{
	CleanUpDeinit();
	CleanUpInit();
}


void World::Reset()
{
	const ParamEntry &world=Pars>>"CfgWorlds">> Glob.header.worldname;
	// restore initial world state (as after Load)
	CleanUp();
	_scene.GetLandscape()->InitObjectVehicles();
	_scene.GetLandscape()->InitDynSounds(world>>"Sounds");
	InsertSeaGulls();
	GSoundScene->Reset();
}

void World::Clear()
{
	// make world clear
	CleanUp();
}

void World::DeleteAnyVehicle( Entity *vehicle )
{
	VehiclesDistributed *list=vehicle->GetList();
	if( list ) list->Delete(vehicle);
	else
	{
		LogF
		(
			"DeleteAnyVehicle: Unknown list (%s)",
			(const char *)vehicle->GetDebugName()
		);
		DeleteVehicle(vehicle);
	}
}

void World::RemoveAnyVehicle( Entity *vehicle )
{
	VehiclesDistributed *list=vehicle->GetList();
	if( list ) list->Remove(vehicle);
	else
	{
		LogF
		(
			"RemoveAnyVehicle: Unknown list (%s)",
			(const char *)vehicle->GetDebugName()
		);
		RemoveVehicle(vehicle);
	}
}

AICenter *World::GetCenter(TargetSide side)
{
	switch (side)
	{
	case TWest:
		return _westCenter;
	case TEast:
		return _eastCenter;
	case TGuerrila:
		return _guerrilaCenter;
	case TCivilian:
		return _civilianCenter;
	case TLogic:
		return _logicCenter;
	}
	return NULL;
}

AICenter *World::CreateCenter(TargetSide side)
{
	AICenterMode mode = TranslateMode(_mode);
	Ref<AICenter> center = new AICenter(side, mode);

	switch (side)
	{
	case TWest:
		_westCenter = center;
		break;
	case TEast:
		_eastCenter = center;
		break;
	case TGuerrila:
		_guerrilaCenter = center;
		break;
	case TCivilian:
		_civilianCenter = center;
		break;
	case TLogic:
		_logicCenter = center;
		break;
	default:
		Fail("invalid center");
		break;
	}
	return center;
}

void World::DeleteCenter(TargetSide side)
{
	switch (side)
	{
	case TWest:
		_westCenter = NULL;
		break;
	case TEast:
		_eastCenter = NULL;
		break;
	case TGuerrila:
		_guerrilaCenter = NULL;
		break;
	case TCivilian:
		_civilianCenter = NULL;
		break;
	case TLogic:
		_logicCenter = NULL;
		break;
	default:
		Fail("invalid center");
		break;
	}
}

void World::AddCenter(AICenter *center)
{
	switch (center->GetSide())
	{
	case TWest:
		_westCenter = center;
		break;
	case TEast:
		_eastCenter = center;
		break;
	case TGuerrila:
		_guerrilaCenter = center;
		break;
	case TCivilian:
		_civilianCenter = center;
		break;
	case TLogic:
		_logicCenter = center;
		break;
	default:
		Fail("invalid center");
		break;
	}
}

void World::RemoveCenter(AICenter *center)
{
	switch (center->GetSide())
	{
	case TWest:
		_westCenter = NULL;
		break;
	case TEast:
		_eastCenter = NULL;
		break;
	case TGuerrila:
		_guerrilaCenter = NULL;
		break;
	case TCivilian:
		_civilianCenter = NULL;
		break;
	case TLogic:
		_logicCenter = NULL;
		break;
	default:
		Fail("invalid center");
		break;
	}
}

void World::ScanPlayers(StaticArrayAuto< OLink<Person> > &players)
{
	// re-created player (Person) list
	// scan all vehicles in all lists
	// check which are players
	players.Resize(0);
	//players.Add(_playerOn);
	for (int i=0; i<NVehicles();i++)
	{
		Entity *veh = GetVehicle(i);
		Person *pers = dyn_cast<Person>(veh);
		if (!pers) continue;
		if (pers->GetRemotePlayer()!=AI_PLAYER)
		{
			players.Add(pers);
		}
	}
	for (int i=0; i<NOutVehicles();i++)
	{
		Entity *veh = GetOutVehicle(i);
		Person *pers = dyn_cast<Person>(veh);
		if (pers->GetRemotePlayer()!=AI_PLAYER)
		{
			players.Add(pers);
		}
	}
}

bool IsOutOfMemory();

World::~World()
{
	if (!IsOutOfMemory())
	{
		ProgressReset();
		ProgressStart(LocalizeString(IDS_SHUTDOWN));
	}
	DebugLog("World destruct");

	VehicleTypes.UnlockAllTypes(); // may be locked from SwitchLandscape

	CleanUpDeinit();
	if (!IsOutOfMemory())
	{
		if( _scene.GetLandscape() ) _scene.GetLandscape()->Init();
	}
}


void World::DistributeImportances
(
	VehiclesDistributed &target, VehicleList &list,
	SimulationImportance prec, const Vector3 *viewerPos, int nViewers
)
{
	// redistribute list into other lists
	SimulationImportance maxPrecG=SimulateInvisibleFar;

	int i;
	for( i=0; i<list.Size(); )
	{
		Entity *vehicle=list[i];
		//Assert( vehicle->RefCounter()==2 );
		SimulationImportance iPrec = vehicle->CalculateImportance(viewerPos,nViewers);
		if( iPrec>maxPrecG ) iPrec=maxPrecG;
		if( iPrec<SimulateVisibleNear ) iPrec=SimulateVisibleNear;
		if( iPrec==prec ) {i++;continue;}
		VehicleList *tgt=NULL;
		switch( iPrec )
		{
			case SimulateVisibleNear: tgt=&target._visibleNear;break;
			case SimulateVisibleFar: tgt=&target._visibleFar;break;
			case SimulateInvisibleNear: tgt=&target._invisibleNear;break;
			case SimulateInvisibleFar: tgt=&target._invisibleFar;break;
		}
		Assert( tgt );
		tgt->Insert(vehicle);
		list.Delete(i);
		// apply transition function if neccessary
		//vehicle->SwitchImportance(iPrec);
		// every vehicle should be once in object list and once in vehicle list
		//Assert( vehicle->RefCounter()==2 );
	}
}

/*!
\patch 1.83 Date 8/30/2002 by Jirka
- Fixed: When seagull fly more than 100 m from dead player, simulation was suspended
*/
void World::GetViewerList(StaticArrayAuto<Vector3> &viewers)
{
	if (_cameraEffect)
	{
		viewers.Add(_cameraEffect->GetTransform().Position());
		return;
	}
	if (GWorld->GetMode() != GModeNetware)
	{
		// SP: check which vehicle is followed by the camera
		if (_cameraOn!=NULL)
		{
			viewers.Add(_cameraOn->Position());
		}
	}
	else
	{
		// MP: check position of all players
		// get all players
		// TODO: incremental maintenance could  be faster
		// this should be based on remotePlayer
		// _remotePlayer can be set via functions - (by owner) or by network transfer
		AUTO_STATIC_ARRAY( OLink<Person>, players, 128);
		GWorld->ScanPlayers(players);
		RString playerNames;
		for(int i=0; i<players.Size(); i++)
		{
			Person *player = players[i];
			viewers.Add(player->Position());
			playerNames = playerNames + player->GetDebugName() + RString(" ");
		}
		//Vector3 viewerPos=_scene.GetCamera()->Position();
		//GlobalShowMessage(1000,"Players: %s",(const char *)playerNames);

		// FIX: Seagull problem - TODO: better solution - add position of all respawned players
		Vector3 cameraPos;
		if (_cameraOn != NULL) cameraPos = _cameraOn->Position();
		else cameraPos = _scene.GetCamera()->Position();
		bool found = false;
		for (int i=0; i<viewers.Size(); i++)
			if (viewers[i].Distance2(cameraPos) < Square(10))
			{
				found = true;
				break;
			}
		if (!found) viewers.Add(cameraPos);
	}
	if (viewers.Size()<=0)
	{
		viewers.Add(_scene.GetCamera()->Position());
	}
}

void World::DistributeNearImportances( VehiclesDistributed &list )
{
	// redistribute vehicles into near...far lists
	_nearImportanceDistributionTime=Glob.time; // when distibutions were calculated
	// redistribute vehicles into near...far lists
	AUTO_STATIC_ARRAY(Vector3,viewers,128)
	GetViewerList(viewers);
	DistributeImportances(list,list._visibleNear,SimulateVisibleNear,viewers.Data(),viewers.Size());
	DistributeImportances(list,list._visibleFar,SimulateVisibleFar,viewers.Data(),viewers.Size());
}

void World::DistributeFarImportances( VehiclesDistributed &list )
{
	// redistribute vehicles into near...far lists
	// far vehicles can be redistributed much less often
	// simulation of what is rest is done in Entity::SwitchImportance
	_farImportanceDistributionTime=Glob.time; // when distibutions were calculated
	/*
	Vector3 viewerPos=_scene.GetCamera()->Position();
	if( _cameraOn!=NULL ) viewerPos=_cameraOn->Position();
	if(_cameraEffect) viewerPos=_cameraEffect->GetTransform().Position();
	*/
	AUTO_STATIC_ARRAY(Vector3,viewers,128)
	GetViewerList(viewers);
	DistributeImportances(list,list._invisibleNear,SimulateInvisibleNear,viewers.Data(),viewers.Size());
	DistributeImportances(list,list._invisibleFar,SimulateInvisibleFar,viewers.Data(),viewers.Size());
}

void World::DistributeNearImportances()
{
	DistributeNearImportances(_vehicles);
	DistributeNearImportances(_animals);
	DistributeNearImportances(_buildings);
}
void World::DistributeFarImportances()
{
	DistributeFarImportances(_vehicles);
	DistributeFarImportances(_animals);
	DistributeFarImportances(_buildings);
}

void World::AddSensor( Person *vehicle )
{
	// TODO: check if the sensor will be used
	_sensorList->AddRow(vehicle);
}

void World::RemoveTarget( Entity *vehicle )
{
	// target should be removed when getting into any vehicle
	EntityAI *ai=dyn_cast<EntityAI>(vehicle);
	if( ai )
	{
		_sensorList->DeleteCol(ai);
	}
}

void World::RemoveSensor( Entity *vehicle )
{
	// this should be used when sensor is no longer present
	// like when getting into vehicle cargo, or being killed
	Person *driver=dyn_cast<Person>(vehicle);
	if( driver )
	{
		_sensorList->DeleteRow(driver);
	}
}

void World::MoveOutAndDelete( VehicleList &vehicles, float deltaT, bool applyMove )
{
	for( int i=0; i<vehicles.Size(); )
	{
		Entity *vehicle=vehicles[i];
		if( vehicle->ToDelete() )
		{
			vehicle->ResetDelete();
			vehicle->UnloadSound();
			_scene.GetLandscape()->RemoveObject(vehicle);
			RemoveTarget(vehicle);
			RemoveSensor(vehicle);
			vehicles.Delete(i);
		}
		else if( vehicle->ToMoveOut() )
		{
			//vehicle->ResetMoveOut(); // flag will be reset after moving from Out list
			LogF("Moving %s from landscape",(const char *)vehicle->GetDebugName());
//RptF("Moving %s from landscape",(const char *)vehicle->GetDebugName());
			vehicle->UnloadSound();
			DoAssert(vehicle->IsMoveOutInProgress());
			vehicle->SetMoveOutFlag();
			_outVehicles.Add(vehicle);
			_scene.GetLandscape()->RemoveObject(vehicle);
			RemoveTarget(vehicle);
			RemoveSensor(vehicle);
			vehicles.Delete(i);
			DoAssert (ValidateOutVehicle(vehicle));
		}
		else if( vehicle->ToConvertToObject() )
		{
		  // object should remain in landscape
			vehicle->UnloadSound();
			vehicle->ResetConvertToObject();
			vehicle->SetType(Primary);
			RemoveSensor(vehicle);
			vehicles.Delete(i);
		}
		else 
		{
			//if( applyMove ) vehicles[i]->ApplyMove(deltaT);
			i++;
		}
	}
}

void World::MoveOutAndDelete( VehiclesDistributed &list, float deltaT, bool applyMove )
{
	MoveOutAndDelete(list._visibleNear,deltaT,applyMove);
	MoveOutAndDelete(list._visibleFar,deltaT,applyMove);
	MoveOutAndDelete(list._invisibleNear,deltaT,applyMove);
	MoveOutAndDelete(list._invisibleFar,deltaT,applyMove);
}

void World::SimulateOnly
(
	VehicleList &vehicles,
	float deltaT, VehicleSimulation simul, Entity *insideVehicle,
	SimulationImportance prec
)
{
	// simulate all vehicles, cameras ...
	// calculate all simulation parameters
	// includes collision testing
	int i;
	int oldVehicles=vehicles.Size();
	if( prec==SimulateVisibleNear )
	{
		if (GetMode()!=GModeNetware)
		{
			// optimized single player case - no remote objects
			for( i=0; i<oldVehicles; i++ )
			{
				Entity *vehicle=vehicles[i];
				if( vehicle!=insideVehicle )
				{
					(vehicle->*simul)(deltaT,prec);
				}
				else
				{
					// simulate camera vehicle
					// good point for catching debug focus
					vehicle->SetLastImportance(SimulateCamera);
					(vehicle->*simul)(deltaT,SimulateCamera);
				}
			}
		}
		else
		{
			for( i=0; i<oldVehicles; i++ )
			{
				Entity *vehicle=vehicles[i];
				if (vehicle->IsLocal())
				{
					if( vehicle!=insideVehicle )
					{
						(vehicle->*simul)(deltaT,prec);
					}
					else
					{
						// simulate camera vehicle
						// good point for catching debug focus
						vehicle->SetLastImportance(SimulateCamera);
						(vehicle->*simul)(deltaT,SimulateCamera);
					}
				}
				else
				{
					SimulationImportance p = vehicle->GetLastImportance();
					if (p==SimulateDefault) p = prec;
					(vehicle->*simul)(deltaT,p);
				}
			}
		}
	}
	else
	{
		for( i=0; i<oldVehicles; i++ )
		{
			Assert( i<vehicles.Size() );
			Entity *vehicle=vehicles[i];
			(vehicle->*simul)(deltaT,prec);
		}
	}
	// there may be some new vehicles - missiles, explosions...
	for( i=oldVehicles; i<vehicles.Size(); i++ )
	{
		Entity *vehicle=vehicles[i];
		(vehicle->*simul)(deltaT,prec);
	}
	#if PERF_SIM
	ADD_COUNTER(aSims,vehicles.Size());
	#endif
}

inline SimulationImportance MinPrec
(
	SimulationImportance p1, SimulationImportance p2 
)
{
	if( p1>p2 ) return p1;
	return p2;
}

void World::SimulateOnly
(
	VehiclesDistributed &vehicles,
	float deltaT, VehicleSimulation simul, Entity *insideVehicle,
	SimulationImportance prec
)
{
	SimulateOnly(vehicles._visibleNear,deltaT,simul,insideVehicle,SimulateVisibleNear);
	SimulateOnly(vehicles._visibleFar,deltaT,simul,insideVehicle,SimulateVisibleFar);
	// far simulation is lightly simplified
	SimulateOnly(vehicles._invisibleNear,deltaT,simul,insideVehicle,SimulateInvisibleNear);
	SimulateOnly(vehicles._invisibleFar,deltaT,simul,insideVehicle,SimulateInvisibleFar);
}

void World::AddAttachment( AttachedOnVehicle *attach )
{
	//Log("Add attachment %x",attach);
	_attached.Add(attach);
}

void World::RemoveAttachment( AttachedOnVehicle *attach )
{
	//Log("Remove attachment %x",attach);
	int index=_attached.Find(attach);
	Assert( index>=0 );
	if( index>=0 ) _attached.Delete(index);
}


void VehicleList::Add( Entity *object )
{
	// TODO: BUG vehicle listed twice
	#if 1
		Ref<Entity> temp=object;
		int index=Find(object);
		if( index>=0 )
		{
			LogF("Entity listed twice %s",(const char *)object->GetDebugName());
			Fail("Entity listed twice.");
			return;
		}
	#endif

	object->StartFrame();
	Insert(object);
	GLOB_LAND->AddObject(object);
}

LSError VehicleList::Serialize(ParamArchive &ar)
{
	if (ar.IsSaving())
	{
		RefArray<Entity> mustBeSaved;
		for (int i=0; i<Size(); i++)
		{
			Entity *veh = Get(i);
			// do not save primary objects
			if (veh->Object::GetType()==Primary ) continue;
			if (!veh->MustBeSaved()) continue;
			mustBeSaved.Add(veh);
		}
		CHECK(ar.Serialize("Vehicles", mustBeSaved, 1))
	}
	else
	{
		CHECK(ar.Serialize("Vehicles", *(RefArray<Entity> *)this, 1))
		if (ar.GetPass() == ParamArchive::PassFirst)
		{
			for (int i=0; i<Size(); i++)
			{
				Entity *object = Set(i);
				object->StartFrame();
				GLOB_LAND->AddObject(object);
			}
		}
	}
	return LSOK;
}

void VehiclesDistributed::Clear()
{
	// clear all list identifiers
	for( int i=0; i<_visibleNear.Size(); i++ ) _visibleNear[i]->SetList(NULL);
	for( int i=0; i<_visibleFar.Size(); i++ ) _visibleFar[i]->SetList(NULL);
	for( int i=0; i<_invisibleNear.Size(); i++ ) _invisibleNear[i]->SetList(NULL);
	for( int i=0; i<_invisibleFar.Size(); i++ ) _invisibleFar[i]->SetList(NULL);
	// clear all lists
	_visibleNear.Clear();
	_visibleFar.Clear();
	_invisibleNear.Clear();
	_invisibleFar.Clear();
}

void VehiclesDistributed::Add( Entity *vehicle )
{
	// always add to visible near list
	vehicle->SetList(this);
	_visibleNear.Add(vehicle);
	#if LOG_ADD_REMOVE_VEHICLE
	LogF
	(
		"Vehicle %s added to list %x",
		(const char *)vehicle->GetDebugName(),this
	);
	#endif
}

void VehiclesDistributed::Insert( Entity *vehicle )
{
	Fail("Obsolete");
	// always add to visible near list
	vehicle->SetList(this);
	_visibleNear.Add(vehicle);
	#if LOG_ADD_REMOVE_VEHICLE
	LogF
	(
		"Vehicle %s inserted to list %x",
		(const char *)vehicle->GetDebugName(),this
	);
	#endif
}

int VehiclesDistributed::Find( Entity *vehicle ) const
{
	int offset = 0;
	int index = 0;
	#define ONE_LIST(list) \
		index = list.Find(vehicle); \
		if (index>=0) return index+offset; \
		offset += list.Size();
	ONE_LIST(_visibleNear)
	ONE_LIST(_visibleFar)
	ONE_LIST(_invisibleNear)
	ONE_LIST(_invisibleFar)
	return -1;
}

void VehiclesDistributed::Delete( Entity *vehicle )
{
	#if LOG_ADD_REMOVE_VEHICLE
	LogF
	(
		"Vehicle %s deleted from list %x",
		(const char *)vehicle->GetDebugName(),this
	);
	#endif
	DoAssert( this==vehicle->GetList() );
	int deleted=0;
	if( _visibleNear.Delete(vehicle) ) deleted++;
	if( _visibleFar.Delete(vehicle) ) deleted++;
	if( _invisibleNear.Delete(vehicle) ) deleted++;
	if( _invisibleFar.Delete(vehicle) ) deleted++;
	vehicle->SetList(NULL);
	DoAssert( deleted==1 );
	GLOB_LAND->RemoveObject(vehicle);
}

void VehiclesDistributed::Remove( Entity *vehicle )
{
	#if LOG_ADD_REMOVE_VEHICLE
	LogF
	(
		"Vehicle %s removed from list %x",
		(const char *)vehicle->GetDebugName(),this
	);
	#endif
	DoAssert( this==vehicle->GetList() );
	int deleted=0;
	if( _visibleNear.Delete(vehicle) ) deleted++;
	if( _visibleFar.Delete(vehicle) ) deleted++;
	if( _invisibleNear.Delete(vehicle) ) deleted++;
	if( _invisibleFar.Delete(vehicle) ) deleted++;
	DoAssert( deleted==1 );
	vehicle->SetList(NULL);
}

int VehiclesDistributed::Size() const
{
	return
	(
		_visibleNear.Size()+_visibleFar.Size()+
		_invisibleNear.Size()+_invisibleFar.Size()
	);
}

Entity *VehiclesDistributed::Get( int index ) const
{
	if( index<_visibleNear.Size() ) return _visibleNear[index];
	index-=_visibleNear.Size();
	if( index<_visibleFar.Size() ) return _visibleFar[index];
	index-=_visibleFar.Size();
	if( index<_invisibleNear.Size() ) return _invisibleNear[index];
	index-=_invisibleNear.Size();
	if( index<_invisibleFar.Size() ) return _invisibleFar[index];
	Fail("Entity in no importance list");
	return NULL;
}

LSError VehiclesDistributed::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("VisibleNear", _visibleNear, 1))
	CHECK(ar.Serialize("VisibleFar", _visibleFar, 1))
	CHECK(ar.Serialize("InvisibleNear", _invisibleNear, 1))
	CHECK(ar.Serialize("InvisibleFar", _invisibleFar, 1))
	if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
	{
		for (int i=0; i<_visibleNear.Size(); i++)
			if (_visibleNear[i]) _visibleNear[i]->SetList(this);
		for (int i=0; i<_visibleFar.Size(); i++)
			if (_visibleFar[i]) _visibleFar[i]->SetList(this);
		for (int i=0; i<_invisibleNear.Size(); i++)
			if (_invisibleNear[i]) _invisibleNear[i]->SetList(this);
		for (int i=0; i<_invisibleFar.Size(); i++)
			if (_invisibleFar[i]) _invisibleFar[i]->SetList(this);
	}
	return LSOK;
}

void World::SetActiveChannels()
{
	AIUnit *unit = FocusOn();
	AIGroup *grp = unit ? unit->GetGroup() : NULL;
	AICenter *center = grp ? grp->GetCenter() : NULL;

	bool enableRadio = _enableRadio;
	if (GWorld->GetMode() != GModeNetware)
	{
		AIUnit *playerUnit = _realPlayer ? _realPlayer->Brain() : NULL;
		if (!playerUnit || playerUnit->GetLifeState() != AIUnit::LSAlive) enableRadio = false;
	}

	// global channel
	_radio->SetAudible(enableRadio);

	// side channel
	if (enableRadio && center)
	{
		if (_channelSide != &center->GetRadio())
		{
			if (_channelSide) _channelSide->SetAudible(false);
			_channelSide = &center->GetRadio();
		}
		if (_channelSide) _channelSide->SetAudible(true);
	}
	else if (_channelSide)
	{
		_channelSide->SetAudible(false);
		_channelSide = NULL;
	}

	if
	(
		!enableRadio || !center || _mode == GModeIntro
	)
	{
		// nothing audible

		// vehicle intercom
		if (_channelVehicle)
		{
			_channelVehicle->SetAudible(false);
			_channelVehicle = NULL;
		}

		// group channel
		if (_channelGroup)
		{
			_channelGroup->SetAudible(false);
			_channelGroup = NULL;
		}

		return;
	}
	
	// group channel
	if (_channelGroup != &grp->GetRadio())
	{
		if( _channelGroup ) _channelGroup->SetAudible(false);
		_channelGroup = &grp->GetRadio();
	}
	if( _channelGroup ) _channelGroup->SetAudible(true);

	// vehicle intercom
	Transport *transport = unit ? unit->GetVehicleIn() : NULL;
	RadioChannel *nChannel=NULL;
	if
	(
		transport &&
		(
			transport->DriverBrain() == unit ||
			transport->CommanderBrain() == unit ||
			transport->GunnerBrain() == unit
		)
	)
	{	
		nChannel=&transport->GetRadio();
	}

	if (_channelVehicle != nChannel)
	{
		if( _channelVehicle ) _channelVehicle->SetAudible(false);
		_channelVehicle = nChannel;
	}
	if( _channelVehicle ) _channelVehicle->SetAudible(true);
}

bool World::LookAroundEnabled() const
{
	// in cargo look around always enabled
	if (GInput.lookAroundEnabled) return true;
	AIUnit *unit = FocusOn();
	if (!unit || unit->IsInCargo()) return true;
	return false;
}

bool World::MouseControlEnabled() const
{
	// enable mouse control only if camera is heading forward
	return fabs(_camHeading[_camType])<0.1 && !GInput.lookAroundEnabled;
}

void World::BrowseCamera( Object *vehicle )
{
	if( vehicle==_cameraOn ) return;
	_ui->ResetHUD();
	//Object *oldCamera=_cameraOn;
	//EntityAI *oldCamAI=dyn_cast<EntityAI>(oldCamera);
	_cameraOn=vehicle;
	SetActiveChannels();

	//Entity *trackV=_trackingCamera;
	//TrackingCamera *track=static_cast<TrackingCamera *>(trackV);
	//_trackingCamera->LinkTo(_cameraOn);
	#if 1
		// leave AI/manual as it was
		/*
		if( oldCamAI )
		{
			#if !_RELEASE
				Log("BrowseCamera SetManual(false) %s",(const char *)oldCamAI->GetDebugName());
			#endif
			SetPlayerManual(false);
			AIUnit *brain=oldCamAI->CommanderUnit();
			if( brain ) brain->RefreshMission();
		}
		else
		{
			SeaGullAuto *gull=dynamic_cast<SeaGullAuto *>(oldCamera);
			if( gull ) gull->SetManual(false);
		}
		*/
		EntityAI *ai=dyn_cast<EntityAI>(vehicle);
		if( ai && _ui ) _ui->ResetVehicle(ai);
	#endif
	// any camera switch  can result in considerable redistribution
	DistributeNearImportances();
	DistributeFarImportances();

	InitCameraPars();
}

void World::BrowseCamera( int dir )
{
	#if 1
		int i;
		Entity *nearestDown=NULL;
		Entity *nearestUp=NULL;
		Entity *mostDown=NULL;
		Entity *mostUp=NULL;
		Object *camVehicle=_cameraOn;
		for( i=0; i<NVehicles(); i++ )
		{
			Entity *vehicle=GetVehicle(i);
			if( vehicle==_cameraOn ) continue;
			//if( vehicle==_trackingCamera ) continue;
			if( vehicle->Static() ) continue;
			if( vehicle->GetType()==TypeTempVehicle ) continue;
			if (vehicle->IsDammageDestroyed()) continue;
			EntityAI *vehicleAI=dyn_cast<EntityAI>(vehicle);
			if( !vehicleAI ) continue;
			if( !vehicleAI->CommanderUnit() ) continue;
			if( vehicle>camVehicle )
			{
				if( !nearestUp || vehicle<nearestUp ) nearestUp=vehicle;
			}
			if( vehicle<camVehicle )
			{
				if( !nearestDown || vehicle>nearestDown ) nearestDown=vehicle;
			}
			if( !mostUp || vehicle>mostUp ) mostUp=vehicle;
			if( !mostDown || vehicle<mostDown ) mostDown=vehicle;
		}
		if( dir>0 )
		{
			if( nearestUp ) BrowseCamera(nearestUp);
			else if( mostDown ) BrowseCamera(mostDown);
		}
		else if( dir<0 )
		{
			if( nearestDown ) BrowseCamera(nearestDown);
			else if( mostUp ) BrowseCamera(mostUp);
		}
	#endif
}

void World::VehicleSwitched( Object *from, Object *to )
{
	if( _cameraEffect && _cameraEffect->GetObject()==from )
	{
		_cameraEffect->SetObject(to);
		_cameraEffect->Simulate(0);
	}
}

/*!
\patch 1.44 Date 2/13/2002 by Ondra
- Fixed: Free look mode now switched off when getting in/out of vehicle
or when mission is restarted.
*/

void World::SwitchCameraTo( Object *vehicle, CameraType camType )
{
	//bool reset=( _cameraOn==NULL );
	bool change=( vehicle!=_cameraOn );
	float changeDist2=1e10;
	if( vehicle && _cameraOn )
	{
		changeDist2=vehicle->Position().Distance2(_cameraOn->Position());
	}
	if( vehicle!=_cameraOn || _camTypeMain!=camType )
	{
		Object *oldCameraOn = _cameraOn;
		_cameraOn=vehicle;
		_camType=_camTypeMain=camType;
		InitCameraPars();
		SetActiveChannels();
		if (change)
		{
			EntityAI *ai=dyn_cast<EntityAI>(vehicle);
			if ( _ui && ai )
			{
				Person *player = PlayerOn();
				if (player==ai || oldCameraOn==player)
				{
					GInput.lookAroundToggleEnabled = false;
				}
				ai->ResetFF();
				_ui->ResetVehicle(ai);
			}
		}
LogF("Camera switched to %s", (const char *)vehicle->GetDebugName());
	}
	// force redistribution as soon as possible
	_nearImportanceDistributionTime=Glob.time-60; // when distibutions were calculated
	_farImportanceDistributionTime=Glob.time-60; // when distibutions were calculated
	// TODO: recreate landscape/object cache
	if( vehicle && change && changeDist2>Square(400) )
	{
		_scene.GetLandscape()->FillCache(*vehicle);
	}
}

/*
static float ScrollSpeed( float cursor )
{
	const float scrollLimit=0.5;
	if( cursor<-scrollLimit ) return (-cursor-scrollLimit)*-10;
	if( cursor>+scrollLimit ) return (+cursor-scrollLimit)*+10;
	return 0; 
}

static float AutoCenterChange( float cursor, float cam, float deltaT )
{
	const float begCenter=0.3;
	const float endCenter=0.1;
	if( cam<0 && cursor<+begCenter )
	{
		float speed=(begCenter-cursor)*(1/(begCenter-endCenter));
		saturateMin(speed,1);
		return +floatMin(speed*deltaT,-cam);
	}
	if( cam>0 && cursor>-begCenter )
	{
		float speed=(begCenter+cursor)*(1/(begCenter-endCenter));
		saturateMin(speed,1);
		return -floatMin(speed*deltaT,+cam);
	}
	return 0;
}
*/


static void KeepNZone( float &value, float cursor, float pos, float size, float fov )
{
	float minNZone=pos-size;
	float maxNZone=pos+size;
	if( cursor<minNZone )
	{
		value+=(minNZone-cursor)*fov;
	}
	else if( cursor>maxNZone )
	{
		value-=(cursor-maxNZone)*fov;
	}
}

inline Matrix3 CameraChange( float heading, float dive )
{
	return Matrix3(MRotationY,heading)*Matrix3(MRotationX,dive);
}

#if 1

	struct SecondaryContext
	{
		float deltaT;
		float noAccDeltaT;
		Entity *cameraVehicle;
		bool insideVehicle;
		World *world;
	};

	static void DoBackgroundSimulate( void *context )
	{
		SecondaryContext *sContext=static_cast<SecondaryContext *>(context);
		sContext->world->BackgroundSimulate
		(
			sContext->deltaT,sContext->noAccDeltaT,
			sContext->cameraVehicle,sContext->insideVehicle
		);
	}
#endif

void World::PrimaryAllowSwitch( int ms )
{
	// give secondary opportunity to continue
	#if BACKGROUND_AI
	if( _secThread ) _secThread->RunSecondary(ms);
	#endif
}

void World::SecondaryAllowSwitch()
{
	#if BACKGROUND_AI
	if( _secThread ) _secThread->AllowSwitch();
	#endif
	// give primary opportunity to continue
}

const float IntroRiseTimeOfDay=6*OneHour+15*OneMinute;

void World::BackgroundSimulate
(
	float deltaT, float noAccDeltaT, Entity *cameraVehicle, bool insideVehicle
)
{
	PerformAI(deltaT,noAccDeltaT);
}

void World::SkipTime(float time)
{
	_timeToSkip=time;
}


void World::SimulateLandscape( float deltaT )
{
	LightSun *sun=_scene.MainLight();
	float timeD=deltaT*OneSecond;

	if( !IsSimulationEnabled() ) timeD=0;
//	if (GInput.keys[DIK_RWIN])
#if _ENABLE_CHEATS
	{
		timeD+=deltaT*OneHour*GInput.GetCheat1(DIK_T);
		timeD-=deltaT*OneHour*GInput.GetCheat1(DIK_Y);
	}
	if( GInput.GetCheat1ToDo(DIK_G) ) timeD+=OneDay;
	if( GInput.GetCheat1ToDo(DIK_H) ) timeD-=OneDay;
#endif

	timeD+=_timeToSkip;
	_timeToSkip=0;
	// 
	if( Glob.clock.AdvanceTime(timeD) ) sun->Recalculate(this);
	float scaledTime=timeD*(1/OneSecond);
	GLOB_LAND->Simulate(scaledTime);
	_scene.MainLightChanged();

#if _ENABLE_CHEATS
	if (_mode!=GModeNetware)
	{
		if( GInput.GetCheat1ToDo(DIK_MINUS) )
		{
			GLandscape->ResampleTerrain(+1);
			//GLandscape->FillCache(*GScene->GetCamera());
			GlobalShowMessage(1000,"Terrain subdivision %d",TerrainRangeLog-LandRangeLog);
		}
		if( GInput.GetCheat1ToDo(DIK_EQUALS) )
		{
			GLandscape->SubdivideTerrain(+1);
			//GLandscape->FillCache(*GScene->GetCamera());
			GlobalShowMessage(1000,"Terrain subdivision %d",TerrainRangeLog-LandRangeLog);
		}
	}
#endif

	if( scaledTime>0 )
	{
		// change toward wanted weather
		saturate(_wantedOvercast,0,1);
		saturate(_wantedFog,0,1);
		float diffOvercast=_wantedOvercast-_actualOvercast;
		float diffFog=_wantedFog-_actualFog;
		saturate(diffOvercast,-_speedOvercast*scaledTime,+_speedOvercast*scaledTime);
		saturate(diffFog,-_speedFog*scaledTime,+_speedFog*scaledTime);
		_actualOvercast+=diffOvercast;
		_actualFog+=diffFog;

		_weatherTime+=scaledTime;
		while( _weatherTime>=_nextWeatherChange )
		{
			// create next weather change
			_actualOvercast=_wantedOvercast;
			_actualFog=_wantedFog;
			float gRand=(GRandGen.RandomValue()+GRandGen.RandomValue())*0.5;
			float time=(gRand+0.1)*12*60*60+20*60; // in sec;
			_nextWeatherChange+=time;
			const float maxChange=0.5;
			const float maxFogChange=0.2;
			_wantedOvercast+=GRandGen.RandomValue()*(maxChange*2)-maxChange;
			_wantedFog+=GRandGen.RandomValue()*(maxFogChange*2)-maxFogChange;
			_speedOvercast=fabs(_wantedOvercast-_actualOvercast)/time;
			_speedFog=fabs(_wantedFog-_actualFog)/time;
		}

	}
	GLOB_LAND->SetOvercast(_actualOvercast);
	GLOB_LAND->SetFog(_actualFog);
}

void World::SetWeather(float overcast, float fog, float time)
{
	if (overcast < 0) overcast = _actualOvercast;
	if (fog < 0) fog = _actualFog;
	saturate(overcast, 0, 1);
	saturate(fog, 0, 1);
	
	if (time <= 0)
	{
		_actualOvercast = _wantedOvercast = overcast;
		_actualFog = _wantedFog = fog;
		_nextWeatherChange = _weatherTime;
		_speedOvercast = 0;
		_speedFog = 0;

		GLandscape->SetOvercast(_actualOvercast);
		GLandscape->SetFog(_actualFog);
	}
	else
	{
		_wantedOvercast = overcast;
		_wantedFog = fog;
		_nextWeatherChange = _weatherTime + time;
		_speedOvercast = fabs(_wantedOvercast - _actualOvercast) / time;
		_speedFog = fabs(_wantedFog - _actualFog) / time;
	}
}

void World::SetDate(int year, int month, int day, int hour, int minute)
{
	day--;
	for (int m=0; m<month-1; m++) day += GetDaysInMonth(year, m);
	float time = day * OneDay + hour * OneHour + minute * OneMinute + 0.5 * OneSecond;

	Glob.clock.SetTimeInYear(time);
	Glob.clock.SetYear(year);
	
	LightSun *sun = _scene.MainLight();
	sun->Recalculate(this);
	_scene.MainLightChanged();
}

void World::ProcessNetwork()
{
	GetNetworkManager().OnSimulate();
}

/*!
\patch 1.83 Date 8/29/2002 by Jirka
- Fixed: When function disableUserInput true is called, forget all pressed keys
*/

void World::DisableUserInput(bool disable)
{
	if (disable)
	{
		if (_userInputDisabled++ == 0) GInput.ForgetKeys();
	}
	else
		_userInputDisabled--;
}

void World::SetCameraEffect( CameraEffect *effect )
{
	_cameraEffect=effect;
	_nearImportanceDistributionTime=Glob.time-60; // when distibutions were calculated
	_farImportanceDistributionTime=Glob.time-60; // when distibutions were calculated
	_playerSuspended = (effect!=NULL);
}

void World::SetTitleEffect( TitleEffect *effect )
{
	_titleEffect=effect;
	Log("SetTitleEffect %p",effect);
}

void World::SetCutEffect( TitleEffect *effect )
{
	_cutEffect=effect;
	Log("SetCutEffect %p",effect);
}

void World::AddScript(Script *script)
{
	int i = _scripts.Add(script);
	(void)i;
/*
	if (script->OnSimulate())
		_scripts.Delete(i);
*/
}

void World::TerminateScript(Script *script)
{
	// TODO: consider some safe script termination

	int index = _scripts.Find(script);
	if (index<0) return;
	_scripts.Delete(index);
}

void World::SimulateScripts()
{
	for (int i=0; i<_scripts.Size();)
	{
		if (_scripts[i]->OnSimulate())
			_scripts.Delete(i);
		else
			i++;
	}
}

void World::SetCameraScript(Script *script) {_cameraScript = script;}

void World::TerminateCameraScript()
{
	if (GetCameraScript())
	{
		// force script termination
		TerminateScript(GWorld->GetCameraScript());
	}
}

void World::StartCameraScript( Script *script )
{
	TerminateCameraScript();
	AddScript(script);
	SetCameraScript(script);
	SimulateScripts();
}

const float ZoomSpeed=8;


bool BreakIntro();
#if _ENABLE_CHEATS
void DebugOperMap();
void DebugOperMap(AIUnit *unit);
void DebugOperMapTrouble();
#endif

static bool showCinemaBorder = true;

void ShowCinemaBorder(bool show)
{
	showCinemaBorder = show;
}

GameValue ShowCinemaBorder(const GameState *state, GameValuePar oper1)
{
	showCinemaBorder = oper1;
	return GameValue();
}

// use VTune pause/resume to control profiling

// ?? move somewhere
static void DrawConnectionQuality(ConnectionQuality quality)
{
	PackedColor color;
	switch (quality)
	{
	default:
		Fail("Connection quality");
	case CQGood:
		// color = PackedColor(Color(0, 1, 0, 0.5));
		// break;
		return;
	case CQPoor:
		color = PackedColor(Color(1, 1, 0, 0.5));
		break;
	case CQBad:
		color = PackedColor(Color(1, 0, 0, 0.5));
		break;
	}
	float w = GEngine->Width2D(), h = GEngine->Height2D();
	MipInfo mip = GEngine->TextBank()->UseMipmap(NULL, 0, 0);
	GEngine->Draw2D(mip, color, Rect2DPixel(0.97 * w, 0.96 * h, 0.03 * w, 0.04 * h));
}

/*!
	\patch 1.05 Date 07/17/2001 by Ondra
	- Fixed: camera position is now clipped,
	so that camera does not look through objects.
	\patch_internal 1.08 Date 07/20/2001 by Ondra
	- Fixed: camera clipping was many times too strong.
	\patch_internal 1.16 Date 08/14/2001 by Ondra
	- Fixed: No camera clipping for group camera - made commanding too difficult.
*/

static void ClipCamera
(
	Vector3 &cam, Object *focus, Vector3Val focusPos, float &maxDistSmooth
)
{
	// find point on focus->cam line that is furthers and has no collision

	CollisionBuffer col;
	// besides of direct line check also some area around
	const float sDist = 0.5;
	static const float coefs[][4]=
	{
		// up distance, aside distance, along distance, distance coef used
		{0,0,1,1},
		{sDist,0,1.2,1.5},
		{-sDist,0,1.2,1.5},
		{0,sDist,1.2,1.5},
		{0,-sDist,1.2,1.5},
	};
	const int nCoefs = sizeof(coefs)/sizeof(*coefs);
	Vector3 dir = cam-focusPos;

	Vector3 up = VUp;
	Vector3 aside = dir.CrossProduct(up);
	aside.Normalize();
	up = dir.CrossProduct(aside);
	up.Normalize();
	/*
	LODShapeWithShadow *sphere = GScene->Preloaded(SphereModel);
	GScene->DrawDiagModel(focusPos+up*0.1,sphere);
	GScene->DrawDiagModel(focusPos+aside*0.1,sphere);
	*/

	float dirSize2 = dir.SquareSize();
	float invDirSize = InvSqrt(dirSize2);
	float dirSize = dirSize2*invDirSize;

	float minMaxDist = dirSize;
	Vector3 dirNorm = dir*invDirSize;

	int nCol = 0;
	for (int c=0; c<nCoefs; c++)
	{
		float distTest = dirSize*coefs[c][2];
		Vector3 camPosAdj = cam+up*coefs[c][0]+aside*coefs[c][1]+dirNorm*coefs[c][2];
		GLandscape->ObjectCollision(col,focus,NULL,focusPos,camPosAdj,0.2,ObjIntersectView);
		if (col.Size()<=0) continue;
		bool someCol = false;
		// find nearest t
		float minT = 1;
		for (int i=0; i<col.Size(); i++)
		{
			const CollisionInfo &info = col[i];
			// ignore obstacle that are not opaque enough
			/*
			LogF
			(
				"%s: View density %.3f",
				(const char *)info.object->GetDebugName(),info.object->ViewDensity()
			);
			*/
			if (info.object->ViewDensity()>-3.0f) continue;
			if (info.under<minT) minT = info.under;
			someCol = true;
		}

		if (someCol) nCol++;

		float maxDist = distTest*minT;
		maxDist -= 0.2f;
		saturateMax(maxDist,0.5f);
		saturateMax(maxDist,0.333f*dirSize);
		saturateMin(minMaxDist,maxDist*coefs[c][3]);
	}


	#if 0
	EntityAI *focusAI = dyn_cast<EntityAI>(focus);
	if (focusAI)
	{
		GlobalShowMessage
		(
			100,"NCol %d, Surround %.2f",
			nCol,focusAI->GetHidden()
		);
	}
	#endif

	if (nCol>3)
	{
		saturateMin(maxDistSmooth,minMaxDist);
	}
	cam = focusPos + dirNorm*floatMin(dirSize,maxDistSmooth);
}

/*!
\patch 1.50 Date 4/15/2002 by Jirka
- Fixed: Dead player (sea-gull) can communicate only on global channel
*/
bool IsPlayerDead()
{
	Person *player = GWorld->GetRealPlayer();
	if (!player) return true;
	if (!player->Brain()) return true;
	return player->Brain()->GetLifeState() == AIUnit::LSDead;
}

ChatChannel ActualChatChannel();
void SetChatChannel(ChatChannel channel);
void PrevChatChannel();
void NextChatChannel();

#if _ENABLE_CHEATS
bool forceControlsPaused = false;
#endif

/*!
\patch 1.01 Date 06/19/2001 by Jirka
- Fixed: optics was disabled when 3rd person view was disabled in difficulties
\patch 1.12 Date 08/06/2001 by Jirka
- Fixed: Right mouse buttom always zoomed
regardless of the controls customization.
\patch_internal 1.20 Date 08/15/2001 by Ondra
- Fixed: Force feedback and iFeel is stopped when simulation is off.
\patch 1.50 Date 3/31/2002 by Ondra
- Fixed: Memory leak on dedicated server during night time (when lights are on).
\patch 1.78 Date 7/18/2002 by Ondra
- Fixed: Cursor movement orientation was wrong when camera was banked.
This made plane mouse controls difficult.
*/

void World::Simulate( float deltaT, bool &enableDraw )
{
	//Assert (GLandscape->VerifyStructure());
	PROFILE_SCOPE(wSimu);
	// this function does all simulation and drawing for single frame
	float noAccDeltaT=deltaT;

	OLink<Object> cameraVehicle=_cameraOn;
	OLink<Entity> camVehicle=dyn_cast<Entity,Object>(cameraVehicle);
	OLink<EntityAI> camAI=dyn_cast<EntityAI,Entity>(camVehicle);
	OLink<Person> person = FocusOn() ? FocusOn()->GetPerson() : NULL;

	if (GInput.CheatActivated() == CheatCrash)
	{
		GInput.CheatServed();
		//ErrF("Crash requested");
		// cause access violation
		// this should trigger report generation
		volatile int a = *(int *)NULL;
    (void)a;
	}

	if (_editor)
	{
#if _ENABLE_CHEATS
		if (GInput.GetCheat1ToDo(DIK_C))
		{
			AbstractOptionsUI *CreateDebugConsole();
			if (!_options) _options = CreateDebugConsole();
		}
#endif
	}
	else
	{
		// process netware communication
		START_PROFILE(netwT);
		ProcessNetwork();
		END_PROFILE(netwT);
	
#if _ENABLE_CHEATS
		if( GInput.GetCheat1ToDo(DIK_R))
		{
			Glob.config.easyMode = !Glob.config.easyMode;
			GlobalShowMessage(500,"%s",Glob.config.easyMode ? "Cadet":"Veteran");
		}
		if( GInput.GetCheat2ToDo(DIK_Z) )
		{
			Glob.config.super = !Glob.config.super;
			GlobalShowMessage(500, "Immortality %s", Glob.config.super ? "On" : "Off");
		}
		if( GInput.GetCheat1ToDo(DIK_M) )
		{
			showCinemaBorder = !showCinemaBorder;
		}
		if (GInput.GetCheat2ToDo(DIK_SLASH))
		{
			void ExportOperMaps(RString prefix);
			ExportOperMaps(Glob.header.worldname);
		}
#endif

		if (_mode == GModeNetware)
		{
			_acceleratedTime=1;
		}
		else
#if !_ENABLE_CHEATS
		if (!_cameraEffect)
#endif
		{
			if( GInput.GetActionToDo(UATimeInc) )
			{
				_acceleratedTime*=2;
				saturate(_acceleratedTime,_TIME_ACC_MIN,_TIME_ACC_MAX);
				GlobalShowMessage(1000,LocalizeString(IDS_TIME_ACC_FORMAT),_acceleratedTime);
				//SetActiveChannels();
			}
			if( GInput.GetActionToDo(UATimeDec) )
			{
				_acceleratedTime*=0.5;
				GInput.keysToDo[DIK_PGDN]=false;
				saturate(_acceleratedTime,_TIME_ACC_MIN,_TIME_ACC_MAX);
				GlobalShowMessage(1000,LocalizeString(IDS_TIME_ACC_FORMAT),_acceleratedTime);
				//SetActiveChannels();
			}
		}
		deltaT*=_acceleratedTime;

	}

	//bool intro = _mode == GModeIntro;

	// cheats
	if (GInput.CheatActivated() == CheatSaveGame)
	{
		RString name = GetSaveDirectory() + RString("save.fps");
		GWorld->SaveBin(name, IDS_SAVE_GAME);
		GInput.CheatServed();
	}
#if _ENABLE_CHEATS
	if( GInput.GetCheat1ToDo(DIK_P) )
	{
		_enableSimulation=!_enableSimulation;
	}
	if( GInput.GetCheat1ToDo(DIK_S) )
	{
		RString dir = GetTmpSaveDirectory();
		char filename[256];
		for (int i=1; i<=99999; i++)
		{
			sprintf(filename, "%sTMP%05d.fps", (const char *)dir, i);
			if (!QIFStream::FileExists(filename))
			{
				SaveBin(filename, IDS_SAVE_GAME);
				break;
			}
		}
	}
	if( GInput.GetCheat1ToDo(DIK_L) )
	{
		RString dir = GetTmpSaveDirectory();
		char filename[256];
		sprintf(filename, "%sTMP%05d.fps", (const char *)dir, 1);
		if (QIFStream::FileExists(filename))
		{
			// some binary save exist
			for (int i=1; i<=99999; i++)
			{
				sprintf(filename, "%sTMP%05d.fps", (const char *)dir, i);
				if (!QIFStream::FileExists(filename))
				{
					if (i > 1)
					{
						sprintf(filename, "%sTMP%05d.fps", (const char *)dir, i - 1);
						LoadBin(filename, IDS_LOAD_GAME);
					}
					break;
				}
			}
		}
		else
		{
			for (int i=1; i<=99999; i++)
			{
				sprintf(filename, "%sTMP%05d.sqg", (const char *)dir, i);
				if (!QIFStream::FileExists(filename))
				{
					if (i > 1)
					{
						sprintf(filename, "%sTMP%05d.sqg", (const char *)dir, i - 1);
						Load(filename, IDS_LOAD_GAME);
					}
					break;
				}
			}
		}
	}
	if (GInput.GetCheat2ToDo(DIK_B))
	{
		DebugOperMapTrouble();
	}
	if (GInput.GetCheat2ToDo(DIK_M))
	{
		DebugOperMap();
	}
	if (GInput.GetCheat2ToDo(DIK_X))
	{
		EntityAI *veh = dyn_cast<EntityAI>(CameraOn());
		AIUnit *unit = veh ? veh->PilotUnit() : NULL;
		DebugOperMap(unit);
	}
	if (GInput.GetCheat2ToDo(DIK_PERIOD))
	{
		Pars.SaveBin("bin\\config.bin");
		Res.SaveBin("bin\\resource.bin");
	}
	if (GInput.GetCheat2ToDo(DIK_BACKSLASH))
	{
		forceControlsPaused = !forceControlsPaused;
		if (forceControlsPaused)
			GEngine->ShowMessage(500, "controls paused on");
		else
			GEngine->ShowMessage(500, "controls paused off");

	}
	/*
	if (GInput.GetCheat2ToDo(DIK_C))
	{
		// global stringtable
		GStringTable = new StringTable("bin\\stringtable.csv");

		// campaign stringtable
		RString GetBaseDirectory();
		RString filename = GetBaseDirectory() + RString("stringtable.csv"); 
		CampaignStringTable.Clear();
		if (QIFStreamB::FileExist(filename))
			CampaignStringTable.Load(filename);

		// mission stringtable
		RString GetMissionDirectory();
		filename = GetMissionDirectory() + RString("stringtable.csv");
		MissionStringTable.Clear();
		if (QIFStreamB::FileExist(filename))
			MissionStringTable.Load(filename);

		//{{ fix - enable config reload
		Pars.SetAccessModeForAll(PAReadAndWrite);
		Res.SetAccessModeForAll(PAReadAndWrite);
		//}}

		Pars.Reload();
		ExtParsCampaign.Reload();
		ExtParsMission.Reload();
		Res.Reload();
	
		for (int i=0; i<VehicleTypes.Size(); i++)
		{
			EntityType *type = VehicleTypes[i];
			if (type)
			{
				if (type->GetShape()) type->DeinitShape();
				type->Load(*type->_par);
				if (type->GetShape()) type->InitShape();
			}
		}
		for (int i=0; i<MagazineTypes.Size(); i++)
		{
			MagazineType *info = MagazineTypes[i];
			if (info) info->Init(info->GetName());
		}
		for (int i=0; i<WeaponTypes.Size(); i++)
		{
			WeaponType *info = WeaponTypes[i];
			if (info) info->Init(info->GetName());
		}
	}
	*/
	const static int cheatVar[]=
	{
		DIK_0,DIK_1,DIK_2,DIK_3,DIK_4,
		DIK_5,DIK_6,DIK_7,DIK_8,DIK_9
	};
	for( int i=0; i<sizeof(cheatVar)/sizeof(*cheatVar); i++ )
	{
		if (GInput.GetCheat1ToDo(cheatVar[i]))
		{
			char varName[64];
			sprintf(varName,"cheat%d",i);
			GetGameState()->VarSet(varName,true,false);
			break;
		}
	}
#endif

//	if( LandEditor ) _enableSimulation=true;
	
	Glob.uiTime+=noAccDeltaT; // move simulation time
	if (IsSimulationEnabled()) Glob.time += deltaT; // move simulation time

	// multiplayer chat control	
	if (GetNetworkManager().GetGameState() >= NGSCreate)
	{
		if (IsPlayerDead())
		{
			if (ActualChatChannel() != CCGlobal)
			{
				SetChatChannel(CCGlobal);
				if (_channel) _channel->ResetHUD();
				if (_voiceChat) _voiceChat->ResetHUD();
				OnChannelChanged();
			}
		}
		else
		{
			if (GInput.GetActionToDo(UAPrevChannel, true, false))
			{
				PrevChatChannel();
				if (_channel) _channel->ResetHUD();
				if (_voiceChat) _voiceChat->ResetHUD();
				OnChannelChanged();
			}

			if (GInput.GetActionToDo(UANextChannel, true, false))
			{
				NextChatChannel();
				if (_channel) _channel->ResetHUD();
				if (_voiceChat) _voiceChat->ResetHUD();
				OnChannelChanged();
			}
		}

		if (GInput.GetActionToDo(UAChat, true, false))
			CreateChat();

		if (!_voiceChat && GInput.GetActionToDo(UAVoiceOverNet, true, false))
			CreateVoiceChat();
	}
	
	if (_chat || _voiceChat || _channelChanged >= Glob.uiTime - 3.0f)
	{
		AbstractOptionsUI *CreateChannelUI();
		if (!_channel) _channel = CreateChannelUI();
	}
	else
	{
		if (_channel) _channel = NULL;
	}

	DisplayMap *map = static_cast<DisplayMap *>((AbstractOptionsUI *)_map);
	_showCompass = map && map->IsShownCompass() && GInput.GetAction(UACompass) > 0;
	_showWatch = map && map->IsShownWatch() && GInput.GetAction(UAWatch) > 0;

	// virtual camera
	//Object *camOn=_cameraOn;
	bool enableOptics = false;
	bool forceOptics = false;

	if (camAI && !_cameraEffect)
	{
		if (!camAI->DisableWeapons())
		{
			if (person)
			{
				enableOptics = camAI->GetOpticsModel(person) != NULL;
				forceOptics = camAI->GetForceOptics(person);
			}
		}
	}

	bool isNV = GEngine->GetNightVision();
	bool enableNV = false;
	bool wantNV = false;
	if (person)
	{
		enableNV = person->IsNVEnabled();
		wantNV = person->IsNVWanted();
	}
/*
	bool enableNV=_scene.MainLight()->NightEffect()>0.1;
	if( !camAI || !camAI->GetType()->GetNightVision() )
	{
		enableNV=false;
	}
*/
	if(!_cameraEffect && IsSimulationEnabled())
	{
		if (!HasMap() || _map->IsTopmost())
		{
			if (GInput.GetActionToDo(UAPersonView))
			{
				// switch internal / external view
				if (_cameraExternal)
				{
					if (_camTypeMain == CamExternal && !_showMap)
					{
						_cameraExternal = false;
						_camTypeMain = CamInternal;
					}
					else
						_camTypeMain = CamExternal;
				}
				else
				{
					if
					(
						_camTypeMain == CamInternal && !_showMap
					)
					{
						_cameraExternal = true;
						_camTypeMain = CamExternal;
					}
					else
						_camTypeMain = CamInternal;
				}
				_showMap = false;
			}
			if (GInput.GetActionToDo(UAOptics))
			{
				// toggle optics
				if (_camTypeMain == CamGunner && !_showMap)
				{
					if (_cameraExternal)
						_camTypeMain = CamExternal;
					else
						_camTypeMain = CamInternal;
				}
				else
				{
					_camTypeMain = CamGunner;
				}
				_showMap = false;
			}
			if (GInput.GetActionToDo(UATacticalView))
			{
				if (_camTypeMain == CamGroup && !_showMap)
				{
					if (_cameraExternal)
						_camTypeMain = CamExternal;
					else
						_camTypeMain = CamInternal;
				}
				else if (FocusOn() && FocusOn()->IsGroupLeader() && FocusOn()->GetGroup()->NUnits() > 1)
				{
					_camTypeMain = CamGroup;
					if (UI()) UI()->ShowMe();
				}
				_showMap = false;
			}
			if (GInput.GetActionToDo(UAMap))
			{
				if (_showMap)
				{
					_showMap = false;
				}
				else
				{
					if (_map) _map->ResetHUD();
					_showMap = true;
				}
			}
		}
	}
/*
	else
	{
		_showMap=false;
	}
*/

	if (_cameraEffect) _showMap = false;

	bool enableExternal = Glob.config.IsEnabled(DT3rdPersonView);
	if (!enableExternal)
	{
		_cameraExternal = false;
		// FIX - was = instead of == -> when external view was disabled - internal camera was forced
		if (_camTypeMain == CamExternal) _camTypeMain = CamInternal;
	}

	bool forcedZoom=false;
	if( FocusOn() )
	{
#if _ENABLE_CHEATS
		static bool enableAnyCamera=false;
		if( GInput.GetCheat2ToDo(DIK_P) )
		{
			enableAnyCamera=!enableAnyCamera;
		}
		if( enableAnyCamera )
		{
			_camTypeMain=CamGroup;
			goto CameraOK;
		}
#endif
		if (_camTypeMain == CamGroup)
		{
			if (FocusOn()->IsGroupLeader() && FocusOn()->GetGroup()->NUnits() > 1 && enableExternal)
				goto CameraOK;
			else if (_cameraExternal)
				_camTypeMain = CamExternal;
			else
				_camTypeMain = CamInternal;
		}
		// _camTypeMain != CamGroup

CameraOK:


		_camType = _camTypeMain;
		if (_camType == CamGunner)
		{
			if (enableOptics) {}
			else if (_cameraExternal)
				_camType = CamExternal;
			else
				_camType = CamInternal;
		}
		// _camType != CamGunner
		if ((_camType == CamInternal || _camType == CamExternal) && forceOptics)
		{
			_camType = CamGunner;
		}
		if (GInput.GetAction(UALockTarget) && (_camType == CamInternal || _camType == CamExternal) )
		{
			EntityAI *veh = FocusOn()->GetVehicle();
			Assert(veh);
			int curWeapon = veh->SelectedWeapon();
			bool allowZoom=true;
			if (curWeapon >= 0 && curWeapon < veh->NMagazineSlots())
			{
				const MagazineSlot &slot = veh->GetMagazineSlot(curWeapon);
				const MuzzleType *muzzle = slot._muzzle;
				bool canLock =
					muzzle->_canBeLocked == 2 ||
					muzzle->_canBeLocked == 1 && Glob.config.IsEnabled(DTAutoGuideAT);
				if (canLock) allowZoom = false;
			}
			if( allowZoom )
			{
				// zoom in
				forcedZoom=true;
			}
		}
	}
	else
	{
		// ?? other camera types ??
		if (_cameraExternal)
			_camTypeMain = CamExternal;
		else
			_camTypeMain = CamInternal;
		_camType = _camTypeMain;
		if (GInput.GetAction(UALockTarget) && (_camType == CamInternal || _camType == CamExternal))
		{
			// zoom in
			forcedZoom = true;
		}
	}

	if (!IsSimulationEnabled() || _showMap || !enableNV) wantNV = false;

	{
		if (isNV != wantNV)
		{
			GEngine->SetNightVision(wantNV);
			_scene.MainLightChanged();
		}
	}

	// time and weather changes
	{
		SimulateLandscape(deltaT);

		// change weather
//		if (GInput.keys[DIK_RWIN])
#if _ENABLE_CHEATS
		{
			if( GInput.GetCheat1(DIK_U) )
			{
				_actualOvercast+=noAccDeltaT*0.1*GInput.GetCheat1(DIK_U);
				saturate(_actualOvercast,0,1);
				_wantedOvercast=_actualOvercast;
				GLOB_LAND->SetOvercast(_actualOvercast);
			}
			if( GInput.GetCheat1(DIK_I) )
			{
				_actualOvercast-=noAccDeltaT*0.1*GInput.GetCheat1(DIK_I);
				saturate(_actualOvercast,0,1);
				_wantedOvercast=_actualOvercast;
				GLOB_LAND->SetOvercast(_actualOvercast);
			}
			if( GInput.GetCheat1(DIK_COMMA) )
			{
				_actualFog+=noAccDeltaT*0.1*GInput.GetCheat1(DIK_COMMA);
				saturate(_actualFog,0,1);
				_wantedFog=_actualFog;
				GLOB_LAND->SetFog(_actualFog);
			}
			if( GInput.GetCheat1(DIK_PERIOD) )
			{
				_actualFog-=noAccDeltaT*0.1*GInput.GetCheat1(DIK_PERIOD);
				saturate(_actualFog,0,1);
				_wantedFog=_actualFog;
				GLOB_LAND->SetFog(_actualFog);
			}

			#if _ENABLE_CHEATS
			if( !_showMap && GInput.GetCheat1ToDo(DIK_V) )
			{
				// cycle through diagnostic modes
				if (DiagMode!=0)
				{
					DiagMode = 0;
					GlobalShowMessage(100,"Diag off");
				}
				else
				{
					DiagMode = (1<<DECombat)|(1<<DEPath);
					GlobalShowMessage(100,"Diag: Combat + Path");
				}
			}
			#endif

			if( !_showMap && GInput.GetCheat1ToDo(DIK_X))
			{
				DisableTextures=!DisableTextures;
			}
		}

		if( GInput.GetCheat1ToDo(DIK_J) )
		{
			BrowseCamera(-1);
			InitCameraPars();
		}
		if( GInput.GetCheat1ToDo(DIK_K) )
		{
			BrowseCamera(+1);
			InitCameraPars();
		}
		if( GInput.GetCheat2ToDo(DIK_L) )
		{
			// find some sea-gull
			for( int i=0; i<NAnimals(); i++ )
			{
				Entity *vehicle=GetAnimal(i);
				if( !vehicle->GetName() ) continue;
				if( !strcmpi(vehicle->GetName(),"SeaGull") )
				{
					BrowseCamera(vehicle);
					break;
				}
			}
		}
		if( GInput.GetCheat2ToDo(DIK_K) )
		{
			QOFStream out;
			Object *camOn = _cameraOn;
			if (camOn)
			{
				char buf[1024];
				Vector3Val pos = camOn->Position();
				Vector3Val dir = camOn->Direction();
				float posSY = GLandscape->SurfaceYAboveWater(pos.X(),pos.Z());
				sprintf
				(
					buf,"'%s' setPos [%.3f,%.3f,%.3f]\r\n",
					(const char *)camOn->GetDebugName(),pos.X(),pos.Z(),pos.Y()-posSY
				);
				out.write(buf,strlen(buf));
				float head = atan2(dir.X(),dir.Z());
				sprintf
				(
					buf,"'%s' setDir %.0f\r\n",
					(const char *)camOn->GetDebugName(),head*(180/H_PI)
				);
				out.write(buf,strlen(buf));
				out.export_clip("clipboard.txt");

				//CONTEXT_FULL
			}

			
		}

		if( GInput.GetCheat1ToDo(DIK_F) )
		{ // display frame rate
			int fps=GLOB_ENGINE->ShowFps()+1;
			if( fps>3 ) fps=0;
			GLOB_ENGINE->ToggleFps( fps );
		}
#endif
	}

	//if( !_cameraOn ) BrowseCamera(+1);
	//bool insideVehicle=false;
	Entity *camInsideVehicle=NULL;

	if( _cameraOn!=NULL )
	{
#if _ENABLE_CHEATS
		bool manToggle=GInput.GetCheat1ToDo(DIK_SCROLL);
#endif
		bool manual=false;
		{
			Object *camOn=_cameraOn;
			EntityAI *ai=dyn_cast<EntityAI>(camOn);
			if( ai )
			{
				manual=ai->QIsManual();
#if _ENABLE_CHEATS
				if( manToggle ) 
				{
					manual=!manual;
					#if !_RELEASE
					if( PlayerOn() )
					{
						Log("SetManual(%d) %s",manual,(const char *)PlayerOn()->GetDebugName());
					}
					#endif
					if( manual )
					{
						Transport *transp = dyn_cast<Transport>(ai);
						if (transp && transp->Driver())
						{
							SwitchPlayerTo(transp->Driver());
						}
						else
						{
							Person *soldier = dyn_cast<Person>(ai);
							Assert( soldier );
							SwitchPlayerTo(soldier);
						}
					}
					else
					{
						AIUnit *unit = ai->CommanderUnit();
						if (unit)
							DoVerify(unit->SetState(AIUnit::Wait));
					}
					SetPlayerManual(manual);
				}
				KeyState.SetScrollLock(!manual);
#endif
			}
			else // non-ai - probably sea-gull cheat
			{
				CameraHolder *camHolder=dyn_cast<CameraHolder,Object>(_cameraOn);
				if( camHolder )
				{
					manual=camHolder->GetManual();
#if _ENABLE_CHEATS
					if( manToggle )
					{
						manual=!manual;
						camHolder->SetManual(manual);
					}
					KeyState.SetScrollLock(!manual);
#endif
				}
			}
		}
	}

#if _ENABLE_CHEATS
	if( GInput.GetCheat2ToDo(DIK_R) )
	{
		_noDisplay=!_noDisplay;
	}
#endif

//	if( IsDisplayEnabled() )
	if (IsSimulationEnabled())
	// condition equal to drawing
	{	
		if( _titleEffect && _titleEffect->IsTerminated() )
		{
			_titleEffect.Free();
			Log("_titleEffect.Free()");
		}

		if( _cutEffect && _cutEffect->IsTerminated() )
		{
			_cutEffect.Free();
		}

		if( _titleEffect )
		{
			_titleEffect->Simulate(deltaT);
		}
		if( _cutEffect )
		{
			_cutEffect->Simulate(deltaT);
		}
	}


#ifdef _WIN32
	/**/
	if( camVehicle && JoystickDev )
	{
		// perform FF effects on sticks
		if (IsSimulationEnabled())
		{
			FFEffects eff;
			camVehicle->PerformFF(eff);
			JoystickDev->SetEngine(eff.engineMag,eff.engineFreq);
			JoystickDev->SetStiffness(eff.stiffnessX,eff.stiffnessY);
			JoystickDev->FFOn();
		}
		else
		{
			JoystickDev->FFOff();
		}
		//if( eff.gunCount>0 )
		//{
		//	JoystickDev->PlayGun(eff.gunMag*0.2,-eff.gunMag,eff.gunFreq,eff.gunCount);
		//}
	}
	/**/
#endif

	//if( !_showMap && !_noDisplay )
	{
		SimulateScripts();
		
		if( _cameraEffect && _cameraEffect->IsTerminated() )
		{
			_cameraEffect.Free();
			_playerSuspended = false;
		}

		Camera &camera=*_scene.GetCamera();
		float fov=0.7;
		Matrix4 transform=camera.Transform();
		float cameraRotate=0;

		if( _cameraEffect )
		{
			_cameraEffect->Simulate(deltaT);
			fov=_cameraEffect->GetFOV();
			if( fov<0 )
			{	// default fov
				Object *object = _cameraEffect->GetObject();
				fov = object ? object->CamEffectFOV() : 0.7f;
			}
			transform=_cameraEffect->GetTransform();

			if( _cameraEffect->IsInside() )
			{
				camInsideVehicle = dyn_cast<Entity,Object>(_cameraEffect->GetObject());
			}
		}
		else if( cameraVehicle )
		{
			fov=_camFOV[_camType];
			transform=cameraVehicle->Transform();
			// if any vehicle control key is pressed, set to manual control
			// check vehicle control keys

			if( !HasOptions() )
//			else if (IsUIEnabled())
			{
				cameraVehicle->SimulateHUD(_camType,deltaT);
				bool isVirtual=cameraVehicle->IsVirtual(_camType);
				//// isGunner was: cameraVehicle->IsGunner(_camType);
				bool isGunner = cameraVehicle->IsGunner(_camType);
				if( isGunner || isVirtual )
				{ // gunner camera
					//if( !isGunner || camAI->GetType()->IsKindOf(GWorld->Preloaded(VTypeMan)) )
					if( _ui && !_showMap )
					{	// if user moved cursor look around
						// switch absolute/relative mode
						_ui->SetCursorMode
						(
							cameraVehicle->GetCursorRelMode(_camType)==CMouseAbs
						);
						Vector3 cursorDir=_ui->GetCursorDirection();
						float scale=_camFOV[_camType];
						float moveX = GInput.cursorMovedX*scale;
						float moveY = -GInput.cursorMovedY*scale;
						GInput.cursorMovedX=GInput.cursorMovedY=0;
						const float maxRotX=H_PI/2;
						const float maxRotY=H_PI/2;
						saturate(moveX,-maxRotX,+maxRotX);
						saturate(moveY,-maxRotY,+maxRotY);
						Vector3 rot(moveX,moveY,0);
						// user moves cursor in camera space
						// we need to convert it into world space
						Vector3 curCam = GScene->GetCamera()->Transform().Rotate(rot);

						//Matrix3 cursorOrient(MDirection,cursorDir,VUp);
						//cursorDir=cursorOrient*rot;
						cursorDir += curCam;
						cursorDir.Normalize();

						cameraVehicle->LimitCursorHard(_camType,cursorDir);
						cursorDir.Normalize();

						#if 0
						if (camVehicle)
						{
							Vector3 curCam = GScene->GetCamera()->GetInvTransform().Rotate(cursorDir);
							Vector3 curVeh = camVehicle->DirectionWorldToModel(cursorDir);
							GlobalShowMessage
							(
								500,"cursor %.4f,%.4f,%.4f, elev %.3f, rel %.3f,%.3f,%.3f",
								cursorDir.X(),cursorDir.Y(),cursorDir.Z(),
								atan2(curVeh.Y(),curVeh.SizeXZ()),
								curCam.X(),curCam.Y(),curCam.Z()
							);
						}
						#endif
						_ui->SetCursorDirection(cursorDir);
					}
				}

				if( cameraVehicle->IsVirtual(_camType) )
				{
					const float diag = 0.5 * H_SQRT2;
					float headSpeed =
					(
						GInput.GetAction(UALookLeft)
						+ GInput.GetAction(UALookLeftUp)*diag
						+ GInput.GetAction(UALookLeftDown)*diag
						- GInput.GetAction(UALookRight)
						- GInput.GetAction(UALookRightUp)*diag
						- GInput.GetAction(UALookRightDown)*diag
					);
					float diveSpeed = 
					(
						GInput.GetAction(UALookDown)
						+ GInput.GetAction(UALookLeftDown)*diag
						+ GInput.GetAction(UALookRightDown)*diag
						- GInput.GetAction(UALookUp)
						- GInput.GetAction(UALookRightUp)*diag
						- GInput.GetAction(UALookLeftUp)*diag
					);
					float headChange=noAccDeltaT*headSpeed;
					float diveChange=noAccDeltaT*diveSpeed;
					if
					(
						!cameraVehicle->IsContinuous(_camType)
						&& !cameraVehicle->IsExternal(_camType)
						&&
						(
							cameraVehicle->GetCursorRelMode(_camType)==CKeyboard ||
							GInput.JoystickActive()
						)
					)
					{
						headChange=0;
						// discrete movement
						//float dummy=0;
						float initDive,initHead,initFOV;
						cameraVehicle->InitVirtual(_camType,initHead,initDive,initFOV);
						if( GInput.GetAction(UALookLeftUp) )
						{
							_camHeadingWanted[_camType]=initHead+H_PI/4;
							diveChange=0;
						}
						else if( GInput.GetAction(UALookLeft) )
						{
							_camHeadingWanted[_camType]=initHead+H_PI/2;
							diveChange=0;
						}
						else if( GInput.GetAction(UALookLeftDown) )
						{
							_camHeadingWanted[_camType]=initHead+H_PI*0.99;
							diveChange=0;
						}
						else if( GInput.GetAction(UALookRightUp) )
						{
							_camHeadingWanted[_camType]=initHead-H_PI/4;
							diveChange=0;
						}
						else if( GInput.GetAction(UALookRight) )
						{
							_camHeadingWanted[_camType]=initHead-H_PI/2;
							diveChange=0;
						}
						else if( GInput.GetAction(UALookRightDown) )
						{
							_camHeadingWanted[_camType]=initHead-H_PI*0.99;
							diveChange=0;
						}
						else _camHeadingWanted[_camType]=initHead;
					}
					if( GInput.GetActionToDo(UALookCenter) )
					{
						cameraVehicle->InitVirtual(_camType,_camHeadingWanted[_camType],_camDiveWanted[_camType],_camFOVWanted[_camType]);
						_camNear[_camType]=1.0;
						_camMaxDist[_camType] = 1e10;
						headChange=0;
						diveChange=0;

						if (_ui)
						{
							// cursor must be set to the screen center
							_ui->SetCursorDirection(camera.Direction());
							_ui->SetCursorMode(false);
						}


					}
					// if mouse cursor is on screen edge, rotate view
					if( headChange || diveChange )
					{
						// keep ui cursor in screen range
						if( _ui)
						{
							cameraRotate=fabs(headSpeed)+fabs(diveSpeed);
							_camHeading[_camType]+=headChange;
							_camDive[_camType]+=diveChange;
						}
						_camHeadingWanted[_camType]=_camHeading[_camType];
						_camDiveWanted[_camType]=_camDive[_camType];
					}
					if( _ui && !_showMap )
					{	// if user moved cursor look around
						// switch absolute/relative mode
						bool cursorMode=_ui->GetCursorMode();
						_ui->SetCursorMode(false);
						if
						(
							cursorMode && !LandEditor
							//&& cameraVehicle->GetCursorRelMode(_camType)==CKeyboard
						)
						{ // world cursor
							// rotate camera so that cursor is in neutral zone

							Vector3 curDir = _ui->GetCursorDirection();
							cameraVehicle->LimitCursor(GetCameraType(),curDir);

							Matrix4Val camInvTransform=camera.GetInvTransform();

							Vector3 pos = camInvTransform.Rotate(curDir);
							float cursorX = 0, cursorY = 0;
							if (pos.Z() > 0)
							{
								float invZ = 1.0 / pos.Z();

								cursorX = pos.X() * invZ * camera.InvLeft();
								cursorY = - pos.Y() * invZ * camera.InvTop();

								saturate(cursorX,-0.95,+0.95);
								saturate(cursorY,-0.95,+0.95);
							}

							if( cameraVehicle->IsVirtualX(_camType) )
							{
								KeepNZone(_camHeading[_camType],cursorX,0,0.8,camera.Left());
								_camHeadingWanted[_camType]=_camHeading[_camType];
							}
							else
							{
								float initDive,initHead,initFOV;
								cameraVehicle->InitVirtual(_camType,initHead,initDive,initFOV);
								_camHeadingWanted[_camType]=initHead;
							}
							KeepNZone(_camDive[_camType],-cursorY,0,0.5,camera.Top());
							_camDiveWanted[_camType]=_camDive[_camType];
						}
						// be sure to keep cursor on screen
						// model cursor
						// limit model cursor into safe screen range

						Vector3 cursor=_ui->GetModelCursor();

						saturateMax(cursor[2],0.01);
						cursor*=1/cursor[2];
						float xLimit=camera.Left();
						float yLimit=camera.Top();
						if( !cursorMode ) xLimit*=0.8,yLimit*=0.8;
						saturate(cursor[0],-xLimit,+xLimit);
						saturate(cursor[1],-yLimit,+yLimit);
						cursor.Normalize();

						_ui->SetModelCursor(cursor);
						// return cursor mode
						_ui->SetCursorMode(cursorMode);
					}
					cameraVehicle->LimitVirtual(_camType,_camHeading[_camType],_camDive[_camType],_camFOV[_camType]);
					cameraVehicle->LimitVirtual(_camType,_camHeadingWanted[_camType],_camDiveWanted[_camType],_camFOVWanted[_camType]);
				}
			}
			if( cameraVehicle->IsExternal(_camType) )
			{
				float expChange=GInput.GetAction(UAZoomOut)-GInput.GetAction(UAZoomIn);
				if( expChange )
				{
					float change=pow(ZoomSpeed,expChange*noAccDeltaT);
					_camNear[_camType]*=change;
					if( !LandEditor ) saturate(_camNear[_camType],0.25,4);
					else saturate(_camNear[_camType],0.01,100);
				}
			}
			else
			{
				if( cameraVehicle->IsContinuous(_camType) )
				{
					float expChange=GInput.GetAction(UAZoomOut)-GInput.GetAction(UAZoomIn);
					if( expChange )
					{
						float change=pow(ZoomSpeed,expChange*noAccDeltaT);
						_camFOV[_camType]*=change;
						_camFOVWanted[_camType]=_camFOV[_camType];
						cameraVehicle->LimitVirtual(_camType,_camHeading[_camType],_camDive[_camType],_camFOV[_camType]);
						cameraVehicle->LimitVirtual(_camType,_camHeadingWanted[_camType],_camDiveWanted[_camType],_camFOVWanted[_camType]);
						//LogF("fov %.2f,w %.2f",_camFOV[_camType],_camFOVWanted[_camType]);
					}
				}
				else
				{
					float initHead,initDive,initFOV;
					cameraVehicle->InitVirtual(_camType,initHead,initDive,initFOV);
					if( GInput.GetAction(UAZoomIn) || forcedZoom ) initFOV*=0.25;
					else if( GInput.GetAction(UAZoomOut) ) initFOV*=4;
					cameraVehicle->LimitVirtual(_camType,initHead,initDive,initFOV);
					_camFOVWanted[_camType]=initFOV;
				}
			}

			// TODO: check if full 360 deg. movement is possible
			// if it is, you can use AngleDifference instead of operator - 
			float delta;
			delta=_camHeadingWanted[_camType]-_camHeading[_camType];
			saturate(delta,-4*deltaT,+4*deltaT);
			_camHeading[_camType]+=delta;

			delta=_camDiveWanted[_camType]-_camDive[_camType];
			saturate(delta,-2*deltaT,+2*deltaT);
			_camDive[_camType]+=delta;

			delta=_camFOVWanted[_camType]/_camFOV[_camType];


			float changeMax=pow(ZoomSpeed,noAccDeltaT);
			saturate(delta,1/changeMax,changeMax);
			_camFOV[_camType]*=delta;

			_camMaxDist[_camType] += deltaT;
			saturateMin(_camMaxDist[_camType],1e10);

			{ // no camera effect - use normal camera
				//LogF("Heading %.3f, dive %.3f",_camHeading[_camType],_camDive[_camType]);
				Matrix3 camChange=CameraChange(_camHeading[_camType],_camDive[_camType]);	 
				switch( _camType )
				{
					case CamGunner:
					{
						transform=cameraVehicle->Transform()*cameraVehicle->InsideCamera(_camType);
						camInsideVehicle=dyn_cast<Entity,Object>(cameraVehicle);
					}
					break;
					default: //case CamInternal:
					{
						transform=cameraVehicle->Transform()*cameraVehicle->InsideCamera(_camType);
						camInsideVehicle=dyn_cast<Entity,Object>(cameraVehicle);
						transform.SetOrientation(transform.Orientation()*camChange);
					}
					break;
					case CamExternal:
					{
						Matrix3 vehOrient;
						Vector3Val dist = cameraVehicle->ExternalCameraPosition(_camType);
						Vector3 dir = cameraVehicle->GetCameraDirection(_camType);
						vehOrient.SetUpAndDirection(VUp,dir);
						vehOrient = vehOrient * camChange;
						// FIX camera clipping
						transform.SetOrientation(vehOrient);
						Vector3 focPos = cameraVehicle->CameraPosition();
						Vector3 camPos = vehOrient*dist+focPos;
						ClipCamera
						(
							camPos,cameraVehicle,focPos,_camMaxDist[_camType]
						);
						transform.SetPosition(camPos);
						// FIX END
						camChange=M3Identity;
						camInsideVehicle=NULL;
					}
					break;
					case CamGroup:
					{
						Matrix3 vehOrient;
						vehOrient.SetUpAndDirection(VUp,_cameraOn->Direction());
						float dist=cameraVehicle->OutsideCameraDistance(_camType)*_camNear[_camType];
						Matrix3Val orient=LandEditor ? camChange : vehOrient*camChange;
						transform.SetOrientation(orient);
						// FIX camera clipping
						Vector3 focPos = cameraVehicle->CameraPosition();
						Vector3 camPos = focPos-orient.Direction()*dist;
						/*
						ClipCamera
						(
							camPos,cameraVehicle,focPos,_camMaxDist[_camType]
						);
						*/
						transform.SetPosition(camPos);
						// FIX END
						camChange=M3Identity;
						camInsideVehicle=NULL;
					}
					break;
				}
			}


		}
		{
			Vector3 camPos=transform.Position();
			float minCamY=_scene.GetLandscape()->SurfaceYAboveWater(camPos.X(),camPos.Z());
			minCamY+=0.1;
			if( camPos.Y()<minCamY ) camPos[1]=minCamY;
			transform.SetPosition(camPos);
		}
		camera.SetTransform(transform);

		if( cameraVehicle ) camera.SetSpeed(cameraVehicle->ObjectSpeed());
		else camera.SetSpeed(VZero);
		if( camVehicle )
		{
			float visualSpeed=camVehicle->Speed().SquareSize()*(1.0/900.0);
			float visualRotate=camVehicle->AngVelocity().SquareSize()*(2.5);
			saturateMax(visualSpeed,visualRotate+cameraRotate);
			Glob.dropDown=visualSpeed;
			saturate(Glob.dropDown,0,1);
		}
		Glob.fullDropDown+=Glob.fullDropDownChange;
		Glob.fullDropDown-=noAccDeltaT*0.33;
		saturate(Glob.fullDropDown,0,1);
		Glob.fullDropDownChange=0;

		// when camera was changed - FOV may change
		// near, far, left, top
		// normal soldier fov is about 0.85
		float cNear = 0.067f/fov;
		saturate(cNear,0.07f,0.2f);
		AspectSettings as;
		GEngine->GetAspectSettings(as);
		camera.SetPerspective(cNear,_scene.GetFogMaxRange(),fov*as.leftFOV,fov*as.topFOV);
		//GlobalShowMessage(100,"cNear %g",cNear);
		camera.Adjust(GEngine);
	}

	#if BACKGROUND_AI
	// start secondary thread
	SecondaryContext context;
	context.deltaT=deltaT;
	context.noAccDeltaT=noAccDeltaT;
	context.cameraVehicle=camInsideVehicle;
	context.insideVehicle=camInsideVehicle!=NULL;
	context.world=this;
	#endif

	bool clear=true; // until sky reflection problem is solved, do clear
	//bool wasSimEnabled = IsSimulationEnabled();

	if (_warningMessage)
	{
		_warningMessage->OnSimulate(NULL);
		if (_warningMessage->GetExitCode() >= 0) _warningMessage = NULL;
	}
	else
	{
		if (_voiceChat) _voiceChat->SimulateHUD(NULL);
		if (!HasOptions() && _cameraOn != NULL)
		{
			if (_userDlg) _userDlg->SimulateHUD(NULL);
			else if( _map && _showMap )
			{
				_map->SimulateHUD(camAI);
			}
		}
		if (_options) _options->SimulateHUD(NULL);
		if( !HasOptions() && _cameraOn!=NULL && !_userDlg)
		{
			if (_ui && IsUIEnabled())
			{
				if( camAI )
				{
					_ui->SimulateHUD(*_scene.GetCamera(),camAI,_camType,deltaT);
				}
				else if (camVehicle)
				{
					_ui->SimulateHUDNonAI(*_scene.GetCamera(),camVehicle,_camType,deltaT);
				}
			}
		}
	}
	GInput.cursorMovedZ = 0;

	bool quiet = false;
	//bool isSimEnabled = IsSimulationEnabled();
	//bool simEnabledEdge = isSimEnabled && !wasSimEnabled;

	if (_firstFrame /*|| simEnabledEdge*/)
	{
		//LogF("_firstFrame %d, simEnabledEdge %d",_firstFrame,simEnabledEdge);
		enableDraw = false,_firstFrame=false, quiet = true;
	}
	if (enableDraw)
	{
		enableDraw = GEngine->IsAbleToDraw();
	}
	if (enableDraw)
	{
		// clear with neutral color - in case of landscape clipping
    #if 1
		  PackedColor color(GEngine->FogColor());
    #else
		  PackedColor color(Color(0.2,0.2,0));
		  if (GScene && GScene->GetCamera() && GScene->GetCamera()->Position().Y()>300)
		  {
			  // if camera is high, use fog color as background color
			  color = PackedColor(GEngine->FogColor());
		  }
    #endif
		GEngine->InitDraw(clear,color);	
	}

	//DWORD startTime=::GlobalTickCount();
	bool doSim=IsSimulationEnabled();

	#if BACKGROUND_AI
	if( doSim )
	{
		// start background simulation
		_secThread->StartSecondary(DoBackgroundSimulate,&context);
	}
	#endif



	START_PROFILE(wDraw);

	if (enableDraw)
	{
		_scene.BeginObjects();
	}

	if( camInsideVehicle ) _scene.SelectActiveLights(camInsideVehicle);
	else _scene.SelectActiveLights(NULL);

	// draw reflections and landscape
	if( _scene.GetLandscape() )
	{
		// remove all vehicles that should be removed
		MoveOutAndDelete(_vehicles,0);
		MoveOutAndDelete(_animals,0);
		MoveOutAndDelete(_fastVehicles,0);
		MoveOutAndDelete(_buildings,0);

		if (enableDraw)
		{
			if( !_showMap && IsDisplayEnabled())
			{

				//----------------------
				{
					PROFILE_SCOPE(obPrp);
					LandBegEnd objBegEnd;
					//CalculBoundingRect(objBegEnd,camera,OBJECT_Z,ObjGrid);
					Landscape::CalculBoundingRect(objBegEnd,*_scene.GetCamera(),_scene.GetFogMaxRange(),ObjGrid);
					
					int x,z;
					int xMin=objBegEnd.xBeg,xMax=objBegEnd.xEnd;
					int zMin=objBegEnd.zBeg,zMax=objBegEnd.zEnd;
					if( GScene->GetObjectShadows() || GScene->GetVehicleShadows() )
					{
						#define SHADOW_BORDER 2
						xMin-=SHADOW_BORDER,xMax+=SHADOW_BORDER;
						zMin-=SHADOW_BORDER,zMax+=SHADOW_BORDER;
					}
					if( xMin<0 ) xMin=0;if( xMin>ObjRange-1 ) xMin=ObjRange-1;
					if( xMax<0 ) xMax=0;if( xMax>ObjRange-1 ) xMax=ObjRange-1;
					if( zMin<0 ) zMin=0;if( zMin>ObjRange-1 ) zMin=ObjRange-1;
					if( zMax<0 ) zMax=0;if( zMax>ObjRange-1 ) zMax=ObjRange-1;
					#define RECT_CLIPPERS 1
					for( z=zMin; z<=zMax; z++ ) for( x=xMin; x<=xMax; x++ )
					{
						// build if necessary
						const ObjectList &list=GLandscape->GetObjects(z,x);
						if( list.Null() ) continue;
						Vector3Val bCenter=list->GetBSphereCenter();
						float bRadius=list->GetBSphereRadius();
						//Point3 cPos(VFastTransform,GScene->ScaledInvTransform(),bCenter);
						const Camera &cam=*GScene->GetCamera();
						#if RECT_CLIPPERS
							ClipFlags andClip=cam.IsClipped(bCenter,bRadius,1);
							if( andClip && list->GetNonStaticCount()<=0 ) continue;
						#endif
						ClipFlags orClip=cam.MayBeClipped(bCenter,bRadius,1);
						int n=list.Size();
						for( int i=0; i<n; i++ )
						{
							Object *obj=list[i];
							Assert( obj );
							if( obj->Invisible() ) continue;
							// TODO: orClip may be used
							//ClipFlags clip=ClipAll;
							ClipFlags clip=orClip;
							#if RECT_CLIPPERS
							if( obj->Static() )
							{
								if( andClip ) continue;
							}
							else
							{
								clip=ClipAll;
							}
							#endif
							if( obj==camInsideVehicle )
							{
								_scene.ObjectForDrawing
								(
									obj,obj->InsideLOD(_camType),clip
								);
								#if _ENABLE_CHEATS
								if (DiagMode)
								{
									obj->DrawDiags();
								}
								#endif
							}
							else
							{
								_scene.ObjectForDrawing(obj,-1,clip);
								#if _ENABLE_CHEATS
								if (DiagMode)
								{
									obj->DrawDiags();
								}
								#endif
							}
						}
					}
					// add all cloudlets
					for( int i=0; i<NCloudlets(); i++ )
					{
						// no LOD management possible
						// no reflections
						_scene.CloudletForDrawing(GetCloudlet(i));
					}
					_scene.EndObjects(); // prepare objects for drawing

					GEngine->EnableReorderQueues(true);
				}

				//----------------------

				// note: night eye is enabled after drawing clouds/sky
				//GEngine->EnableNightEye(_scene.MainLight()->NightEffect());
				_scene.GetLandscape()->Draw(_scene);

				GEngine->EnableReorderQueues(false);
				GEngine->FlushQueues();

				_scene.ObjectsDrawn();
				GEngine->FlushQueues();
				GEngine->EnableNightEye(0);

				if( _cameraEffect)
				{
					_cameraEffect->Draw();
				}
			}
			if( !IsDisplayEnabled() )
			{
				ProgressDraw();
			}
		} // if (enableDraw)
		else
		{
			_scene.CleanUp();
		}

	}
	else
	{
		if (enableDraw)
		{
			_scene.EndObjects(); // prepare objects for drawing
			_scene.DrawObjectsAndShadowsPass1();
			_scene.DrawObjectsAndShadowsPass2();
			_scene.ObjectsDrawn();
		}
		else
		{
			_scene.CleanUp();
		}
	}

	END_PROFILE(wDraw);

	//if (GInput.keysToDo[DIK_ESCAPE])
	//{
	//	LogF("Here");
	//}

	// simulate HUD

	// ADDED in Patch 1.01 - reimplementation of WarningMessage
	/*
	if (_warningMessage)
	{
		_warningMessage->OnSimulate(NULL);
		if (_warningMessage->GetExitCode() >= 0) _warningMessage = NULL;
	}
	else
	{
		if (_voiceChat) _voiceChat->SimulateHUD(NULL);
		if (!HasOptions() && _cameraOn != NULL)
		{
			if (_userDlg) _userDlg->SimulateHUD(NULL);
			else if( _map && _showMap )
			{
				_map->SimulateHUD(camAI);
			}
		}
		if (_options) _options->SimulateHUD(NULL);
		if( !HasOptions() && _cameraOn!=NULL && !_userDlg)
		{
			if (_ui && IsUIEnabled())
			{
				if( camAI )
				{
					_ui->SimulateHUD(*_scene.GetCamera(),camAI,_camType,deltaT);
				}
				else if (camVehicle)
				{
					_ui->SimulateHUDNonAI(*_scene.GetCamera(),camVehicle,_camType,deltaT);
				}
			}
		}
	}
	GInput.cursorMovedZ = 0;
	*/

	// draw HUD
	if (enableDraw)
	{
		if( _map && (_showMap || _forceMap))
		{
			_map->DrawHUD(camAI,1);
		}

		if
		(
			_ui && IsUIEnabled() && !_cameraEffect
		)
		{
			if (camAI)
			{
				if (_camType == CamInternal)
				{
					camAI->DrawCameraCockpit();
				}

				if (person && person->IsNVWanted()) person->DrawNVOptics();

				_ui->DrawHUD(*_scene.GetCamera(),camAI,_camType);
			}
			else if (camVehicle)
			{
				camVehicle->DrawCameraCockpit();
				// seagull HUD (mouse cursor drawing)
				_ui->DrawHUDNonAI(*_scene.GetCamera(),camVehicle,_camType);
			}
		}

		NetworkGameState state = GetNetworkManager().GetGameState();
		if (state == NGSDebriefing)
		{
			GStats.DrawMPTable(1.0f);
		}
#if _ENABLE_CHEATS
		void DrawNetworkStatistics();
		DrawNetworkStatistics();
#endif

		if( _options ) _options->DrawHUD(NULL,1);
		if( _userDlg ) _userDlg->DrawHUD(NULL,1);
		if( _channel ) _channel->DrawHUD(NULL,1);
		if( _chat ) _chat->DrawHUD(NULL,1);
		if( _voiceChat ) _voiceChat->DrawHUD(NULL,1);
		GChatList.OnDraw();
		// ADDED in Patch 1.01 - reimplementation of WarningMessage
		if (_warningMessage)
		{
			_warningMessage->OnDraw(NULL, 1);
			_warningMessage->DrawCursor();
		}

		if (_mode == GModeNetware)
			DrawConnectionQuality(GetNetworkManager().GetConnectionQuality());
		
		if (IsSimulationEnabled())
		{
			if( _cutEffect ) _cutEffect->Draw();
			// titles are on top of cuts
			if( _titleEffect ) _titleEffect->Draw();
		}
/*
		if ((state == NGSPlay || state == NGSDebriefing) && GInput.GetAction(UANetworkStats, false))
		{
			GStats.DrawMPTable(1.0f);
		}
*/
		if (state == NGSPlay && GInput.GetAction(UANetworkStats, false))
		{
			GStats.DrawMPTable(1.0f);
		}
	} // if (enableDraw)

  #ifdef _WIN32
	#if _ENABLE_PERFLOG
	#define CHECK_PROFILE() \
		static __int64 ProfileCheck; \
		DEF_COUNTER_P(Check,10000); \
		__int64 Diff_Check=ReadTsc()-ProfileCheck; \
		int Diff_ICheck=Diff_Check; \
		ProfileCheck=ReadTsc(); \
		ADD_COUNTER_P(Check,Diff_ICheck)
	CHECK_PROFILE();
	#endif
  #endif
	
	if (enableDraw)
	{
		GEngine->FinishDraw();
	}

	// all vehicles are still left in their old positions

	if (doSim)
	{
		#if BACKGROUND_AI
		_secThread->FinishSecondary();
		#else

		PerformAI(deltaT,noAccDeltaT);



		//DoBackgroundSimulate(&context);
		#endif
		SimulateAllVehicles(deltaT,noAccDeltaT,camVehicle);
	}

	if( IsSimulationEnabled() )
	{
		if( camInsideVehicle ) PerformSound(camInsideVehicle,deltaT);
		else PerformSound(NULL,deltaT);
	}

	GSoundScene->AdvanceAll(deltaT,!IsSimulationEnabled() || quiet); // sort and activate sounds
	if ( GSoundsys ) GSoundsys->Commit(); // commit deferred settings

	if (enableDraw)
	{
		PROFILE_SCOPE(wFram);
		GEngine->NextFrame();
	}

#if _ENABLE_CHEATS
	static bool disableVis=false;
	if (GInput.GetCheat2ToDo(DIK_Y))
	{
		disableVis=!disableVis;
		GlobalShowMessage(500,"VisTests %s",disableVis ? "Off":"On");
	}

	#if !_RELEASE && !_DEBUG
	#define REGULAR_FOOTPRINT 1
	#endif

	#if REGULAR_FOOTPRINT
		static DWORD lastSample = 0;
		if (GlobalTickCount()>lastSample+60000)
		{
			void MemoryFootprint();
			MemoryFootprint();
			lastSample=GlobalTickCount();
		}
		if (GInput.GetCheat1ToDo(DIK_O))
		{
			void MemoryFootprint();
			MemoryFootprint();
			// avoid any more samples
			lastSample = UINT_MAX;
		}
	#else
		if (GInput.GetCheat1ToDo(DIK_O))
		{
			void MemoryFootprint();
			MemoryFootprint();
		}
	#endif

#endif

	if
	(
		doSim
#if _ENABLE_CHEATS
		&& !disableVis
#endif
	)
	{
		// TODO: we can balance how much time we would like
		// SmartUpdateAll to take
		// if we check min and average of last N frames, we can decide
		// we do not want to finish before certain time
		GetSensorList()->SmartUpdateAll();
	}

	UpdatePerfLog();
}

void World::UpdatePerfLog()
{
	#if _ENABLE_PERFLOG
	bool enable=( IsSimulationEnabled() && IsDisplayEnabled() );
	if( !enable )
	{
		GPerfCounters.Reset();
		GPerfProfilers.Reset();
	}
	GPerfCounters.Enable(enable);
	GPerfProfilers.Enable(enable);
	#endif
}

/*
void World::EnableDisplay( bool val )
{
	_noDisplay=!val;
	UpdatePerfLog();
}
void World::EnableSimulation( bool val )
{
	_enableSimulation=val;
	if (!val)
	{
		UnloadSounds();
	}
	UpdatePerfLog();
}
*/
bool World::IsDisplayEnabled() const
{
	if (LandEditor) return true;
	if (_noDisplay) return false;
	if (_showMap && _map && !_map->IsDisplayEnabled()) return false;
	if (GProgress.Active()) return false;
	if (!_options) return false;
	return _options->IsDisplayEnabled();
}

bool World::IsSimulationEnabled() const
{
	if (LandEditor) return true;
	if (_mode == GModeNetware)
	{
		return GetNetworkManager().GetGameState() >= NGSPlay;
	}
	else
	{
		if (_warningMessage) return false;
		if (!_enableSimulation) return false;
		if (!_simulationFocus) return false;
		if (!_options) return false;
		return _options->IsSimulationEnabled();
	}
}

bool World::HasOptions() const
{
	if (!_options) return false;
	return !_options->IsSimulationEnabled();
}

bool World::IsUIEnabled() const
{
	if (!IsSimulationEnabled()) return false;
	if (!_options) return false;
	return _options->IsUIEnabled();
}

bool World::HasCompass() const
{
	return _showCompass && !_showMap;
}

bool World::HasWatch() const
{
	return _showWatch && !_showMap;
}

void World::OnChannelChanged()
{
	_channelChanged = Glob.uiTime;
}

void World::CreateMainOptions()
{
	if( !_options )
	{
		_options=CreateMainOptionsUI();
		if( GEngine ) GEngine->ReinitCounters();
	}
}

void World::CreateEndOptions(int mode) {if( !_options ) _options=CreateEndOptionsUI(mode);}
void World::CreateChat()
{
	if (!_chat)
	{
		GInput.ChangeGameFocus(+1);
		_chat = CreateChatUI();
	}
}
void World::CreateVoiceChat() {if (!_voiceChat) _voiceChat = CreateVoiceChatUI();}
void World::CreateMainMap() {if( !_map ) _map=CreateMainMapUI();}

void World::CreateWarningMessage(RString text)
{
	ControlsContainer *CreateWarningMessageBox(RString text);
	if (!_warningMessage) _warningMessage = CreateWarningMessageBox(text);
}

void World::DestroyOptions( int exitCode )
{
	if( !_options ) return;
	_options.Free();
/*
	switch( exitCode )
	{
		case IDC_CANCEL:
		case IDC_MAIN_GAME:
		break;
		case IDC_MAIN_QUIT:
			Glob.exit=true;
		break;
		case IDC_OK:
			CreateMainOptions();
		break;
	}
*/
	if (exitCode == IDC_MAIN_QUIT) Glob.exit=true;

// ???
	GEngine->ReinitCounters();
}

void World::DestroyMap(int exitCode)
{
	_map.Free();
}

void World::DestroyChat(int exitCode)
{
	if (_chat)
	{
		_chat.Free();
		GInput.ChangeGameFocus(-1);
	}
}

void World::DestroyVoiceChat(int exitCode)
{
	_voiceChat.Free();
}

Transport *World::FindFreeVehicle( Person *driver ) const
{
	// find nearest vehicle I can get in
	const float maxDist=20;
	float minDist2=Square(maxDist);
	Transport *free=NULL;
	int xMin,xMax,zMin,zMax;
	Vector3Val pos=driver->Position();
	ObjRadiusRectangle(xMin,xMax,zMin,zMax,pos,pos,maxDist);
	for( int x=xMin; x<=xMax; x++ ) for( int z=zMin; z<=zMax; z++ )
	{
		const ObjectList &list=_scene.GetLandscape()->GetObjects(z,x);
		for( int i=0; i<list.Size(); i++ )
		{
			Object *obj=list[i];
			//if( obj->GetType()!=TypeVehicle ) continue;
			Transport *vehicle=dyn_cast<Transport>(obj);
			if( !vehicle ) continue;
			float dist2=vehicle->Position().Distance2(driver->Position());
			if( minDist2>dist2 )
			{
				if( vehicle->QCanIGetInAny(driver) )
				{
					Vector3Val relPos=driver->PositionWorldToModel(vehicle->Position());
//					if( relPos.Z()>0 && fabs(relPos.X())<relPos.Z() )
					if( relPos.Z()>0 )
					{
						minDist2=dist2;
						free=vehicle;
					}
				}
			}
		}
	}
	return free;
}

#if _ENABLE_CHEATS
bool disableAI=false;
bool disableUnitAI=false;
bool disableSimpleSim=false;
#endif

void World::PerformAI( float deltaT, float noAccDeltaT )
{
	PROFILE_SCOPE(aiAll);
#if _ENABLE_CHEATS
	if (GInput.GetCheat2ToDo(DIK_T))
	{
		disableAI=!disableAI;
		GlobalShowMessage(500,"Group AI %s",disableAI ? "Off":"On");
	}
	if (GInput.GetCheat2ToDo(DIK_U))
	{
		disableUnitAI=!disableUnitAI;
		GlobalShowMessage(500,"Unit AI %s",disableUnitAI ? "Off":"On");
	}
	if (GInput.GetCheat2ToDo(DIK_I))
	{
		disableSimpleSim=!disableSimpleSim;
		GlobalShowMessage(500,"Simple sim %s",disableSimpleSim ? "Off":"On");
	}
#endif
	if 
	(
		deltaT>0
#if _ENABLE_CHEATS
		&& !disableAI
#endif
	)
	{
		Log("****** Time %f *******",Glob.time.toFloat());
//		if (_helpChannel)
//			_helpChannel->Simulate(deltaT);
		GetRadio().Simulate(deltaT);
		if( _eastCenter )
		{
			_eastCenter->GetRadio().Simulate(deltaT);
			_eastCenter->Think();
			int i;
			for (i=0; i<_eastCenter->NGroups(); i++)
			{
				AIGroup *grp = _eastCenter->GetGroup(i);
				if (grp)
				{
					grp->GetRadio().Simulate(deltaT);
				}
			}
		}
		if( _westCenter )
		{
			_westCenter->GetRadio().Simulate(deltaT);
			_westCenter->Think();
			int i;
			for (i=0; i<_westCenter->NGroups(); i++)
			{
				AIGroup *grp = _westCenter->GetGroup(i);
				if (grp)
				{
					grp->GetRadio().Simulate(deltaT);
				}
			}
		}
		if( _guerrilaCenter )
		{
			_guerrilaCenter->GetRadio().Simulate(deltaT);
			_guerrilaCenter->Think();
			int i;
			for (i=0; i<_guerrilaCenter->NGroups(); i++)
			{
				AIGroup *grp = _guerrilaCenter->GetGroup(i);
				if (grp)
				{
					grp->GetRadio().Simulate(deltaT);
				}
			}
		}
		if( _civilianCenter )
		{
			_civilianCenter->GetRadio().Simulate(deltaT);
			_civilianCenter->Think();
			int i;
			for (i=0; i<_civilianCenter->NGroups(); i++)
			{
				AIGroup *grp = _civilianCenter->GetGroup(i);
				if (grp)
				{
					grp->GetRadio().Simulate(deltaT);
				}
			}
		}
		if( _logicCenter )
		{
			_logicCenter->Think();
			// do not simulate radio
		}
		
		if (_endMission == EMContinue)
		{
			if (GInput.CheatActivated() == CheatWinMission)
			{
				_endMission = EMEnd1;
				GInput.CheatServed();
			}
			else
#if _ENABLE_CHEATS
			if (GInput.GetCheat2ToDo(DIK_1))
			{
				_endMission = EMEnd1;
			}
			else if (GInput.GetCheat2ToDo(DIK_2))
			{
				_endMission = EMEnd2;
			}
			else if (GInput.GetCheat2ToDo(DIK_3))
			{
				_endMission = EMEnd3;
			}
			else if (GInput.GetCheat2ToDo(DIK_4))
			{
				_endMission = EMEnd4;
			}
			else if (GInput.GetCheat2ToDo(DIK_5))
			{
				_endMission = EMEnd5;
			}
			else if (GInput.GetCheat2ToDo(DIK_6))
			{
				_endMission = EMEnd6;
			}
			else if (GInput.GetCheat2ToDo(DIK_0))
			{
				_endMission = EMLoser;
			}
			else
#endif
			{
				switch (_mode)
				{
					case GModeArcade:
						{
							Person *veh = GetRealPlayer();
							AIUnit *unit = veh ? veh->Brain() : NULL;
							if (!unit || unit->GetLifeState() == AIUnit::LSDead)
							{
								_endMission = EMKilled;
								return;
							}
						}
					// continue
					case GModeIntro:
					case GModeNetware:
						{
							int nEnd1 = 0, cEnd1 = 0;
							int nEnd2 = 0, cEnd2 = 0;
							int nEnd3 = 0, cEnd3 = 0;
							int nEnd4 = 0, cEnd4 = 0;
							int nEnd5 = 0, cEnd5 = 0;
							int nEnd6 = 0, cEnd6 = 0;

							for (int i=0; i<sensorsMap.Size(); i++)
							{
								Entity *veh = sensorsMap[i];
								if (!veh) continue;
								Detector *sensor = dyn_cast<Detector>(veh);
								Assert(sensor);
								if (!sensor) continue;
								switch (sensor->GetAction())
								{
								case ASTLoose:
									if (sensor->IsActive())
									{
										_endMission = EMLoser;
										return;
									}
									break;
								case ASTEnd1:
									nEnd1++;
									if (sensor->IsActive()) cEnd1++;
									break;
								case ASTEnd2:
									nEnd2++;
									if (sensor->IsActive()) cEnd2++;
									break;
								case ASTEnd3:
									nEnd3++;
									if (sensor->IsActive()) cEnd3++;
									break;
								case ASTEnd4:
									nEnd4++;
									if (sensor->IsActive()) cEnd4++;
									break;
								case ASTEnd5:
									nEnd5++;
									if (sensor->IsActive()) cEnd5++;
									break;
								case ASTEnd6:
									nEnd6++;
									if (sensor->IsActive()) cEnd6++;
									break;
								}
							}
							if (nEnd1 > 0 && cEnd1 == nEnd1) _endMission = EMEnd1;
							else if (nEnd2 > 0 && cEnd2 == nEnd2) _endMission = EMEnd2;
							else if (nEnd3 > 0 && cEnd3 == nEnd3) _endMission = EMEnd3;
							else if (nEnd4 > 0 && cEnd4 == nEnd4) _endMission = EMEnd4;
							else if (nEnd5 > 0 && cEnd5 == nEnd5) _endMission = EMEnd5;
							else if (nEnd6 > 0 && cEnd6 == nEnd6) _endMission = EMEnd6;
							else _endMission = EMContinue;
						}
						break;
					default:
						Fail("Unknown mode");
						_endMission = EMContinue;
						break;
				}
			}
		}
	}
}

void World::SimulateVehicles
(
	float deltaT, VehicleSimulation simul, Entity *insideVehcile
)
{
	// TODO: remove SimulateVisibleNear
	MoveOutAndDelete(_vehicles,deltaT,false);
	MoveOutAndDelete(_animals,deltaT,false);
	SimulateOnly(_vehicles,deltaT,simul,insideVehcile,SimulateVisibleNear);
	SimulateOnly(_animals,deltaT,simul,insideVehcile,SimulateVisibleNear);

	MoveOutAndDelete(_vehicles,deltaT,true);
	MoveOutAndDelete(_animals,deltaT,true);

	#if PERF_SIM
	ADD_COUNTER(simV,_vehicles.Size());
	ADD_COUNTER(simA,_animals.Size());
	#endif
}

void World::SimulateBuildings( float deltaT, VehicleSimulation simul )
{
	MoveOutAndDelete(_buildings,deltaT,false);
	SimulateOnly(_buildings,deltaT,simul,NULL,SimulateVisibleNear);
	#if PERF_SIM
	ADD_COUNTER(simB,_buildings.Size());
	#endif
	MoveOutAndDelete(_buildings,deltaT,true);
}

void World::SimulateFastVehicles( float deltaT, VehicleSimulation simul )
{
	MoveOutAndDelete(_fastVehicles,deltaT,false);
	SimulateOnly(_fastVehicles,deltaT,simul,NULL,SimulateVisibleNear);
	#if PERF_SIM
	ADD_COUNTER(simF,_fastVehicles.Size());
	#endif
	MoveOutAndDelete(_fastVehicles,deltaT,true);
}

void World::SimulateCloudlets( float deltaT )
{
	// simulate all cloudlets ... once per frame
	SimulationImportance prec = SimulateVisibleFar;

	for( int i=0; i<_cloudlets.Size(); )
	{
		Entity *vehicle=_cloudlets[i];
		//SimulationImportance prec = vehicle->CalculateImportance(viewerPos);
		vehicle->Simulate(deltaT,prec);
		if( !vehicle->ToDelete() ) i++;
		else _cloudlets.Delete(i);
	}
}


void World::SimulateAllVehicles
(
	float deltaT, float noAccDeltaT, Entity *cameraVehicle
)
{
	PROFILE_SCOPE(wSimA);
	float farValidFor=1.5;
	if( Glob.time>_farImportanceDistributionTime+farValidFor )
	{
		DistributeFarImportances();
	}
	if( Glob.time>_nearImportanceDistributionTime+1.0 )
	{
		DistributeNearImportances();
	}
	SetActiveChannels();
	#define MAX_SIM_STEP_VEHICLES ( 1.0/15 )
	#define MAX_SIM_STEP_FAST ( 0.001 )

	// set motion blur start
	for( int i=0; i<_fastVehicles.Size(); i++ )
	{
		Entity *vehicle=_fastVehicles[i];
		vehicle->StartFrame();
	}

	// implement real time slice scheme that would allow
	// any fast / slow vehicle combinatinon easily

	// cloudlets are simulated max. once per frame
	SimulateCloudlets(deltaT);
	// normal and fast vehicles can be simulated multiple times
	float toSimVehicles=deltaT;
	float toSimFast=deltaT;
	while( toSimVehicles>MAX_SIM_STEP_VEHICLES )
	{
		SimulateVehicles(MAX_SIM_STEP_VEHICLES,&Entity::SimulateOptimized,cameraVehicle);
		toSimVehicles-=MAX_SIM_STEP_VEHICLES;
		while( toSimFast>toSimVehicles && toSimFast>MAX_SIM_STEP_FAST )
		{
			SimulateFastVehicles(MAX_SIM_STEP_FAST,&Entity::SimulateOptimized);
			toSimFast-=MAX_SIM_STEP_FAST;
		}
	}
	SimulateVehicles(toSimVehicles,&Entity::SimulateRest,cameraVehicle);
	while( toSimFast>MAX_SIM_STEP_FAST )
	{
		SimulateFastVehicles(MAX_SIM_STEP_FAST,&Entity::SimulateOptimized);
		toSimFast-=MAX_SIM_STEP_FAST;
	}
	SimulateFastVehicles(toSimFast,&Entity::SimulateRest);
	// slow vehicles are simulated max. once per frame
	SimulateBuildings(deltaT,&Entity::SimulateOptimized);

	// far simulation only with accumulated precision
	// not necessary in every frame
	for( int i=0; i<_attached.Size(); i++ )
	{
		_attached[i]->UpdatePosition(); // move to follow vehicle
	}
}

float World::Visibility( AIUnit *from, Object *to ) const
{
	EntityAI *ai=dyn_cast<EntityAI>(to);
	if( !ai ) return 1; // non-ai objects are always visible
	Person *me=from->GetPerson();
	AIUnit *aiUnit = ai->CommanderUnit();
	if
	(
		aiUnit
		|| ai->GetType()->IsKindOf(GLOB_WORLD->Preloaded(VTypeStrategic))
		|| ai->GetType()->IsKindOf(GLOB_WORLD->Preloaded(VTypeAllVehicles))
	)
	{
		// if vehicle is in assume full visibility
		if (!ai->IsInLandscape())
		{
			LogF
			(
				"Patch: vanished vehicle visibility queried (%s to %s)",
				(const char *)me->GetDebugName(),
				(const char *)ai->GetDebugName()
			);
			return 1;
		}
		if (aiUnit)
		{
			// is target alive and same group, assume full visibility
			AIGroup *grp = from->GetGroup();
			if 
			(
				aiUnit && aiUnit->GetGroup()==grp
				&& aiUnit->GetLifeState()==AIUnit::LSAlive
			)
			{
				return 1;
			}
		}

		// check actual visibility (from sensor matrix)
		return _sensorList->GetVisibility(me,ai);
	}
	// static objects are always visible
	return 1;
}

/*!
\patch 1.33 Date 11/30/2001 by Ondra
- Fixed: AI: Added "last visible time" check in visibility matrix.
This should avoid firing at target considered visible due to target sharing.
*/

Time World::VisibilityTime( AIUnit *from, Object *to ) const
{
	EntityAI *ai=dyn_cast<EntityAI>(to);
	if( !ai ) return Glob.time; // non-ai objects are always visible
	Person *me=from->GetPerson();
	AIUnit *aiUnit = ai->CommanderUnit();
	if
	(
		aiUnit
		|| ai->GetType()->IsKindOf(GLOB_WORLD->Preloaded(VTypeStrategic))
		|| ai->GetType()->IsKindOf(GLOB_WORLD->Preloaded(VTypeAllVehicles))
	)
	{
		// if vehicle is in assume full visibility
		if (!ai->IsInLandscape())
		{
			return Glob.time;
		}
		if (aiUnit)
		{
			// is target alive and same group, assume full visibility
			AIGroup *grp = from->GetGroup();
			if 
			(
				aiUnit && aiUnit->GetGroup()==grp
				&& aiUnit->GetLifeState()==AIUnit::LSAlive
			)
			{
				return Glob.time;
			}
		}

		// check actual visibility (from sensor matrix)
		return _sensorList->GetVisibilityTime(me,ai);
	}
	// static objects are always visible
	return Glob.time;
}

static const char *MapWaveSound( bool night, int index, float &v )
{
	if( index<0 ) return NULL;
	if( index>NEnvSoundPars-1 ) return NULL;
	if (v <= 0) return NULL;
	// note: return string must be lowercase
	const SoundPars &pars=night?EnvSoundParsNight[index]:EnvSoundPars[index];
	v*=pars.vol;
	return pars.name;
} 

void World::PerformSound( VehicleList &list, Entity *inside, float deltaT )
{

	
	if( inside )
	{
		for( int i=0; i<list.Size(); i++ )
		{
			Entity *vehicle=list[i];
			vehicle->Sound(inside==vehicle,deltaT);
		}
	}
	else
	{
		for( int i=0; i<list.Size(); i++ )
		{
			Entity *vehicle=list[i];
			vehicle->Sound(false,deltaT);
		}
	}
}

void World::PerformSound( VehiclesDistributed &list, Entity *inside, float deltaT )
{
	PerformSound(list._visibleNear,inside,deltaT);
	PerformSound(list._visibleFar,inside,deltaT);
	PerformSound(list._invisibleNear,inside,deltaT);
	PerformSound(list._invisibleFar,inside,deltaT);
}

void World::PerformSound( Entity *inside, float deltaT )
{
	int i;
	if( !GSoundsys ) return;
	const Camera &cam=*_scene.GetCamera();
	// check environmental properties around listener
	// check forest:

	Vector3Val pos = cam.Position();
	int x = toIntFloor(pos.X() * InvLandGrid);
	int z = toIntFloor(pos.Z() * InvLandGrid);
	GeographyInfo geogr = GLandscape->GetGeography(x, z);
	float sy = GLandscape->SurfaceYAboveWater(pos.X(),pos.Z());
	// detect forest
	#define SHOW_ENV 0
	if (geogr.u.forestInner || geogr.u.forestOuter)
	{
		SoundEnvironment env;
		env.type = SEForest;
		env.size = 38;
		env.density = 0.5;
		GSoundsys->SetEnvironment(env);
		#if SHOW_ENV
		GlobalShowMessage(100,"Forest %.1f %.1f",env.size,env.density);
		#endif
	}
	else if (geogr.u.howManyHardObjects>0)
	{
		// city
		SoundEnvironment env;
		env.type = SECity;
		// check how many object are around us		
		env.size = (4-geogr.u.howManyHardObjects)*15;
		env.density = geogr.u.howManyHardObjects*(1.0f/3);
		GSoundsys->SetEnvironment(env);
		#if SHOW_ENV
		GlobalShowMessage(100,"City %.1f %.1f",env.size,env.density);
		#endif
	}
	else if (sy>170)
	{
		// mountains
		SoundEnvironment env;
		env.type = SEMountains;
		// check how many object are around us		
		env.size = sy-120;
		saturate(env.size,50,100);
		env.density = 0.5;
		GSoundsys->SetEnvironment(env);
		#if SHOW_ENV
		GlobalShowMessage(100,"Mountains %.1f %.1f",env.size,env.density);
		#endif
	}
	else
	{
		// plain
		SoundEnvironment env;
		env.type = SEPlain;
		// check how many object are around us		
		env.size = 75-geogr.u.howManyObjects*15;
		env.density = 0.5;
		GSoundsys->SetEnvironment(env);
		#if SHOW_ENV
		GlobalShowMessage(100,"Plain %.1f %.1f",env.size,env.density);
		#endif
	}

	// check surface height (to d
	GSoundsys->SetListener
	(
		cam.Position(),cam.Speed(),
		cam.Direction(),cam.DirectionUp()
	);
	PerformSound(_vehicles,inside,deltaT);
	PerformSound(_animals,inside,deltaT);
	PerformSound(_buildings,inside,deltaT);
	PerformSound(_fastVehicles,inside,deltaT);
	// note: we assume slow vehicles do not produce sound
	#if 1
		// add enviromental sounds (no 3D positioning)
		// note they can be up to four!!
		// sounds are valid for center of each squre
		// shift coordinates by 0.5
		int s[5];
		float v[5];
		const Vector3 &camPos=cam.Position();
		float camHeight=camPos[1]-_scene.GetLandscape()->SurfaceY(camPos[0],camPos[2]);
		float canHear=1-camHeight*1.0/200;
		saturate(canHear,0,1);
		//float vCoef=0.25*canHear;
		float vCoef=0.5*canHear;

		if (stricmp(EnvSoundPars[5].name, "SOUND\\$DEFAULT$.WSS") != 0)
		{
			s[0]=5; v[0]=2.0*vCoef;
			s[1]=-1; v[1]=0;
			s[2]=-1; v[2]=0;
			s[3]=-1; v[3]=0;
		}
		else
		{
			float xGrid=camPos.X()*InvLandGrid-0.5;
			float zGrid=camPos.Z()*InvLandGrid-0.5;
			int x=toIntFloor(xGrid);
			int z=toIntFloor(zGrid);
			float xFrac=xGrid-x;
			float zFrac=zGrid-z;
			s[0]=GLOB_LAND->GetSound(x,z);
			s[1]=GLOB_LAND->GetSound(x+1,z);
			s[2]=GLOB_LAND->GetSound(x,z+1);
			s[3]=GLOB_LAND->GetSound(x+1,z+1);
			// volume: bilinear by xFrac,zFrac
			v[0]=(1-xFrac+1-zFrac)*vCoef;
			v[1]=(xFrac+1-zFrac)*vCoef;
			v[2]=(1-xFrac+zFrac)*vCoef;
			v[3]=(xFrac+zFrac)*vCoef;
		}
		s[4]=-1; v[4]=0;
		const float thold=0.1;
		float rain=_scene.GetLandscape()->GetRainDensity()-thold;
		if (rain>=0)
		{
			float coef=rain*(1/(1-thold));
			v[0]*=1-coef;
			v[1]*=1-coef;
			v[2]*=1-coef;
			v[3]*=1-coef;
			s[4]=4,v[4]=coef;
		}
		// sum of all v[x] is 1 
		// merge idenl sXX
		{
			for( int i=0; i<5; i++ ) for( int j=0; j<i; j++ ) if( s[j]==s[i] )
			{
				s[j]=-1;
				v[i]+=v[j];
			}
		}
		const char *ss[5];
		bool night = _scene.MainLight()->NightEffect() > 0.5;
		for( i=0; i<5; i++ ) ss[i]=MapWaveSound(night,s[i],v[i]);
		GSoundScene->SetEnvSound(ss[0],v[0]);
		GSoundScene->SetEnvSound(ss[1],v[1]);
		GSoundScene->SetEnvSound(ss[2],v[2]);
		GSoundScene->SetEnvSound(ss[3],v[3]);
		GSoundScene->SetEnvSound(ss[4],v[4]);
		GSoundScene->AdvanceEnvSounds(); // remove old sounds
	#endif
}

void World::UnloadSounds( VehicleList &list )
{
	for( int i=0; i<list.Size(); i++ )
	{
		list[i]->UnloadSound();
	}
}

void World::UnloadSounds( VehiclesDistributed &list )
{
	UnloadSounds(list._visibleNear);
	UnloadSounds(list._visibleFar);
	UnloadSounds(list._invisibleNear);
	UnloadSounds(list._invisibleFar);
}

void World::UnloadSounds()
{
	UnloadSounds(_vehicles);
	UnloadSounds(_buildings);
	UnloadSounds(_animals);
	UnloadSounds(_fastVehicles);
	GSoundScene->Reset(); // unload global sounds
}

RString GetUserParams();

void SetVisibility(float distance);

void World::AdjustSubdivisionGrid(float gridSize)
{
	#if _ENABLE_DATADISC

	const float invLog2 = 1/log(2);
	float coefLog = log(LandGrid/gridSize)*invLog2;
	//LogF("coefLog %.3f",coefLog);
	int coefLogInt = toInt(coefLog);
	saturate(coefLogInt,0,8);
	LogF("Terrain subdivision wanted: %d (%.3f)",coefLogInt,coefLog);
	int currentLog = TerrainRangeLog-LandRangeLog;
	int terrainChange = coefLogInt-currentLog;

	if (terrainChange>0)
	{
		//LogF("Subdiv %d",terrainChange);
		GLandscape->SubdivideTerrain(terrainChange);
	}
	else if (terrainChange<0)
	{
		GLandscape->ResampleTerrain(-terrainChange);
	}
	#endif
}

void World::AdjustSubdivision(GameMode mode)
{
	float gridSize = GScene->GetPreferredTerrainGrid();
	float viewDist = GScene->GetPreferredViewDistance();
	// same grid size for all computers required in MP
	if (mode==GModeNetware)
	{
		// for network we have to use default values
		gridSize = PreferredGridSizeMP;
		viewDist = 900;
	}
	AdjustSubdivisionGrid(gridSize);
	SetVisibility(viewDist);
}

void World::ActivateAddons(const FindArrayRStringCI &addons)
{
	_activeAddons.Clear();
	// create merged list of banks that should be activated
	// active all default addons
	const ParamEntry &def = Pars>>"CfgAddons">>"PreloadAddons";
	for (int c=0; c<def.GetEntryCount(); c++)
	{
		const ParamEntry &cc = def.GetEntry(c);
		if (!cc.IsClass()) continue;
		if (!cc.FindEntry("list")) continue;
		const ParamEntry &cl = cc>>"list";
		for (int i=0; i<cl.GetSize(); i++)
		{
			RString addon = cl[i];
			//LogF("Activating default addon %s",(const char *)addon);
			_activeAddons.Add(addon);
		}
	}
	// activate all addons from list
	for (int i=0; i<addons.Size(); i++)
	{
		RString addon = addons[i];
		LogF("Activating addon %s",(const char *)addon);
		_activeAddons.Add(addon);
	}
}

/*!
\patch 1.78 Date 7/15/2002 by Ondra
- Fixed: Dedicated server crashed when unregistered addon was used.
*/

bool World::CheckAddon(const ParamEntry &entry)
{
	bool visible = entry.CheckVisible(_activeAddons);
	if (visible) return true;
	// check if current display is able to perform last addon registration
	if (!_options) return false;
	RString addon = entry.GetOwner();
	bool registered = _options->DoUnregisteredAddonUsed(addon);
	if (registered)
	{
		_activeAddons.Add(addon);
	}
	return registered;
}

void MemoryCleanUp();

void World::SwitchLandscape( const char *name )
{
	GSoundScene->Reset();
	// stop any music that might be playing, reset sound settings
	// InitVehicles should always be called after this call
	// Serialize (Loading) may also use this function - it should unlock too
	GDebugger.NextAliveExpected(15*60*1000);
	// lock all types and textures in memory
	// locked until InitVehicles
	VehicleTypes.LockAllTypes();
	GEngine->TextBank()->LockAllTextures();
	CleanUp();
	Landscape *land=_scene.GetLandscape();
	// load corresponding landscape file
	if (!land)
	{
		Fail("No landscape");
		return;
	}

	const char *lName=land->GetName();
	if( !lName || strcmpi(name,lName) )
	{
		// change global landscape name
		char islandName[256];
		const char *fname=strrchr(name,'\\');
		if( !fname ) fname=name;
		else fname++;
		strcpy(islandName,fname);
		char *ext=strchr(islandName,'.');
		if( ext ) *ext=0;
		// 
//		RStringB cfgWorldName=Pars>>"CfgWorlds">>islandName;
		strcpy(Glob.header.worldname, islandName);

	
		const ParamEntry &cls = Pars >> "CfgWorlds" >> Glob.header.worldname;
		float grid = cls>>"LandGrid";
		// different relief/objects - load the world
		land->LoadData(name,grid);

		ParseCfgWorld();


		InitLandscape(land);
		land->RebuildIDCache();

	}
	else
	{
		// only reset object state if landscape file is same
		land->ResetState();
		Reset();
		land->FlushCache();
		if (!_map)
		{
			_showMap = false;
//			CreateMainMap();
		}
	}

	#if _ENABLE_DATADISC
	#endif

	// reset visibility distance to default

	Glob.config.tacticalZ=900;
	Glob.config.horizontZ=900;
	Glob.config.objectsZ=600;
	Glob.config.shadowsZ=250;

	GInput.lookAroundToggleEnabled = false;
	
	GLandscape->Simulate(0);
	GScene->ResetFog();
	//GLandscape->FillCache(*GScene->GetCamera());

	// unload all currently unreferenced textures
	// 
	_engine->TextBank()->FlushTextures();
	GetNetworkManager().CleanUpMemory();
	MemoryCleanUp();
}

void World::AddVehicle( Entity *vehicle )
{
	#if LOG_ADD_REMOVE_VEHICLE
	LogF("World::AddVehicle %s",(const char *)vehicle->GetDebugName());
	#endif
	DoAssert(vehicle->RefCounter()==0 || _vehicles.Find(vehicle)<0);
	_vehicles.Add(vehicle);
}
void World::RemoveVehicle( Entity *vehicle )
{
	#if LOG_ADD_REMOVE_VEHICLE
	LogF("World::RemoveVehicle %s",(const char *)vehicle->GetDebugName());
	#endif
	DoAssert(_vehicles.Find(vehicle)>=0);
	_vehicles.Remove(vehicle);
}
void World::InsertVehicle( Entity *vehicle )
{
	#if LOG_ADD_REMOVE_VEHICLE
	LogF("World::InsertVehicle %s",(const char *)vehicle->GetDebugName());
	#endif
	DoAssert(vehicle->RefCounter()==0 || _vehicles.Find(vehicle)<0);
	_vehicles.Insert(vehicle);
}
void World::DeleteVehicle( Entity *vehicle )
{
	#if LOG_ADD_REMOVE_VEHICLE
	LogF("World::DeleteVehicle %s",(const char *)vehicle->GetDebugName());
	#endif
	DoAssert(_vehicles.Find(vehicle)>=0);
	_vehicles.Delete(vehicle);
}

void World::AddOutVehicle( Entity *vehicle )
{
	#if LOG_ADD_REMOVE_VEHICLE
	LogF("World::AddOutVehicle %s",(const char *)vehicle->GetDebugName());
	#endif
	DoAssert(vehicle->RefCounter()==0 || _outVehicles.Find(vehicle)<0);
	_outVehicles.Add(vehicle);
//RptF("Moving %s from landscape",(const char *)vehicle->GetDebugName());
}

void World::RemoveOutVehicle( Entity *vehicle )
{
	#if LOG_ADD_REMOVE_VEHICLE
	LogF("World::RemoveOutVehicle %s",(const char *)vehicle->GetDebugName());
	#endif
	DoAssert(_outVehicles.Find(vehicle)>=0);
	_outVehicles.Delete(vehicle);
//RptF("Moving %s into landscape",(const char *)vehicle->GetDebugName());
}

bool World::ValidateOutVehicle(Entity *veh, bool complex) const
{
	// check if vehicle is correctly maintained in all lists
	// it should not be present in vehicle list
	bool ok = true;
	for (int i=0; i<NVehicles(); i++)
	{
		Entity *v = GetVehicle(i);
		if (v==veh)
		{
			ok = false;
			RptF
			(
				"Out Vehicle %s in normal list",
				(const char *)veh->GetDebugName()
			);
		}
	}

	// it should not be present in the landscape
	for (int zz=0; zz<LandRange; zz++)
	for (int xx=0; xx<LandRange; xx++)
	{
		const ObjectList &list=GLandscape->GetObjects(zz,xx);
		for( int i=0; i<list.Size(); i++ ) if( list[i]==veh )
		{
			RptF
			(
				"Out Vehicle %s in landscape (%d,%d)",
				(const char *)veh->GetDebugName(),xx,zz
			);
		}
	}

	return ok;
}

bool World::ValidateOutVehicles(bool complex) const
{
	bool ok = true;
	for (int i=0; i<NOutVehicles(); i++)
	{
		Entity *veh = GetOutVehicle(i);
		if (!ValidateOutVehicle(veh,complex))
		{
			ok = false;
		}
	}
	return ok;
}

bool World::CheckVehicleStructure() const
{
	#if 0 // DO_LINK_DIAGS
	// check all vehicle links
	int i;
	for( i=0; i<NVehicles(); i++ )
	{
		Entity *vehicle=GetVehicle(i);
		bool ok=vehicle->VerifyStructure();
		Assert( ok );
		if( !ok ) return false;
	}
	for( i=0; i<NOutVehicles(); i++ )
	{
		Entity *vehicle=GetOutVehicle(i);
		bool ok=vehicle->VerifyStructure();
		Assert( ok );
		if( !ok ) return false;
	}
	Log("World::CheckVehicleStructure OK");
	#endif

	bool ok = true;
	for (int i=0; i<NVehicles(); i++)
	{
		Entity *vehicle = GetVehicle(i);
		EntityAI *veh = dyn_cast<EntityAI>(vehicle);
		AIUnit *unit = veh->CommanderUnit();
		if (unit)
		{
			if (!unit->AssertValid()) ok = false;
		}
		unit = veh->PilotUnit();
		if (unit)
		{
			if (!unit->AssertValid()) ok = false;
		}
		unit = veh->GunnerUnit();
		if (unit)
		{
			if (!unit->AssertValid()) ok = false;
		}
	}
	for (int i=0; i<NOutVehicles(); i++)
	{
		Entity *vehicle = GetOutVehicle(i);
		EntityAI *veh = dyn_cast<EntityAI>(vehicle);
		AIUnit *unit = veh->CommanderUnit();
		if (unit)
		{
			if (!unit->AssertValid()) ok = false;
		}
		unit = veh->PilotUnit();
		if (unit)
		{
			if (!unit->AssertValid()) ok = false;
		}
		unit = veh->GunnerUnit();
		if (unit)
		{
			if (!unit->AssertValid()) ok = false;
		}
	}
	return ok;
}

inline bool IsPrimary(Object *vehicle)
{
	return
	(
		vehicle->GetType()==Primary ||
		vehicle->GetType()==Network
	);
}

void World::ResetIDs() const
{
	Log("World::ResetIDs");
	// reset object and vehicles id's
	_scene.GetLandscape()->ResetObjectIDs();
	int i;
	for( i=0; i<NVehicles(); i++ )
	{
		Entity *vehicle=GetVehicle(i);
		if( IsPrimary(vehicle) )
		{
			Assert( vehicle->ID()>=0 );
			continue;
		}
		int id=_scene.GetLandscape()->NewObjectID();
		vehicle->SetID(id);
	}
	for( i=0; i<NAnimals(); i++ )
	{
		Entity *vehicle=GetAnimal(i);
		if( IsPrimary(vehicle) )
		{
			Assert( vehicle->ID()>=0 );
			continue;
		}
		int id=_scene.GetLandscape()->NewObjectID();
		vehicle->SetID(id);
	}
	for( i=0; i<NBuildings(); i++ )
	{
		Entity *vehicle=GetBuilding(i);
		if( IsPrimary(vehicle) )
		{
			Assert( vehicle->ID()>=0 );
			continue;
		}
		int id=_scene.GetLandscape()->NewObjectID();
		vehicle->SetID(id);
	}
	for( i=0; i<NFastVehicles(); i++ )
	{
		Entity *vehicle=GetFastVehicle(i);
		if( IsPrimary(vehicle) )
		{
			Fail("Fast primary vehicle");
			Assert( vehicle->ID()>=0 );
			continue;
		}
		int id=_scene.GetLandscape()->NewObjectID();
		vehicle->SetID(id);
	}
	for( i=0; i<NOutVehicles(); i++ )
	{
		Entity *vehicle=GetOutVehicle(i);
		if( IsPrimary(vehicle) )
		{
			Fail("Out primary vehicle");
			Assert( vehicle->ID()>=0 );
			continue;
		}
		int id=_scene.GetLandscape()->NewObjectID();
		vehicle->SetID(id);
	}
}
void World::RemoveIDs() const
{
	Fail("Obsolete - do not use");
	Log("World::RemoveIDs");
	// reset object and vehicle id's
	_scene.GetLandscape()->ResetObjectIDs();
	// reset all vehicle IDs
	int i;
	for( i=0; i<NVehicles(); i++ )
	{
		Entity *vehicle=GetVehicle(i);
		if( IsPrimary(vehicle) ) continue;
		vehicle->SetID(-1);
	}
	for( i=0; i<NAnimals(); i++ )
	{
		Entity *vehicle=GetAnimal(i);
		if( IsPrimary(vehicle) ) continue;
		vehicle->SetID(-1);
	}
	for( i=0; i<NBuildings(); i++ )
	{
		Entity *vehicle=GetBuilding(i);
		if( IsPrimary(vehicle) ) continue;
		vehicle->SetID(-1);
	}
	for( i=0; i<NOutVehicles(); i++ )
	{
		Entity *vehicle=GetOutVehicle(i);
		if( IsPrimary(vehicle) ) continue;
		vehicle->SetID(-1);
	}
}

// top level
#include "saveVersion.hpp"

LSError World::Load(const char *name, int message)
{
	Fail("Text load obsolete");
	GDebugger.NextAliveExpected(15*60*1000);
	ParamArchiveLoad ar(name);
	ar.FirstPass();
	LSError err = Serialize(ar, message);
	if (err == LSOK)
	{
		ar.SecondPass();
		err = Serialize(ar, message);
	}
	if (err == LSOK)
	{
		// TODO: displays hierarchy
	}
	else
	{
		ErrorMessage("Cannot load '%s'. Error '%s' at '%s'.", name, ar.GetErrorName(err), (const char *)ar.GetErrorContext());
	}
	return err;
}

LSError World::Save(const char *name, int message) const
{
	Fail("Text save obsolete");
	GDebugger.NextAliveExpected(15*60*1000);
	ParamArchiveSave ar(WorldSerializeVersion);
	World *w = const_cast<World *>(this);
	CHECK(w->Serialize(ar, message))
	return ar.Save(name);
}

bool World::LoadBin(const char *name, int message)
{
	LogF("LoadBin: Start - Total allocated: %d MB",MemoryUsed()/(1024*1024));
	LSError err;
	{
		GDebugger.NextAliveExpected(15*60*1000);
		ParamArchiveLoad ar;
		bool result = ar.LoadBin(name);
		if (!result) return false;

		LogF("Load: Total allocated after ar.LoadBin: %d MB",MemoryUsed()/(1024*1024));
		ar.FirstPass();
		err = Serialize(ar, message);
		if (err == LSOK)
		{
			ar.SecondPass();
			err = Serialize(ar, message);
		}
		if (err == LSOK)
		{
			// TODO: displays hierarchy
		}
		else
		{
			ErrorMessage("Cannot load '%s'. Error '%s' at '%s'.", name, ar.GetErrorName(err), (const char *)ar.GetErrorContext());
		}
		LogF("Load: Total allocated after World::Serialize: %d MB",MemoryUsed()/(1024*1024));
	}
	LogF("Total allocated after ~ParamArchive: %d MB",MemoryUsed()/(1024*1024));
	MemoryCleanUp();
	LogF("Total allocated after MemoryCleanUp: %d MB",MemoryUsed()/(1024*1024));
	return err == LSOK;
}


bool World::SaveBin(const char *name, int message) const
{
	bool ret;
	{
		GDebugger.NextAliveExpected(15*60*1000);
		ParamArchiveSave ar(WorldSerializeVersion);
		World *w = const_cast<World *>(this);
		LogF("SaveBin: Start - Total allocated: %d MB",MemoryUsed()/(1024*1024));
		if (w->Serialize(ar, message) != LSOK) return false;
		
		// Czech CD Protection added
	/*
	#if _CZECH
		PerformRandomRoxxeTest_000(CDDrive);
	#endif
	*/

		LogF("Total allocated after World::Serialize: %d MB",MemoryUsed()/(1024*1024));
		ret = ar.SaveBin(name);
		LogF("Total allocated after ar.SaveBin: %d MB",MemoryUsed()/(1024*1024));
	}

	// during save quite a lot of memory can be often allocated
	// cleaning-up after save may help
	LogF("Total allocated after ~ParamArchive: %d MB",MemoryUsed()/(1024*1024));
	MemoryCleanUp();
	LogF("Total allocated after MemoryCleanUp: %d MB",MemoryUsed()/(1024*1024));
	return ret;
}

LSError World::SerializeVehicles(ParamArchive &ar)
{
	// TODO: CHECK(ar.Serialize("Cloudlets", _cloudlets, 1))
	CHECK(ar.Serialize("FastVehicles", _fastVehicles, 1))
	CHECK(ar.Serialize("Vehicles", _vehicles, 1))
	CHECK(ar.Serialize("Animals", _animals, 1))
	CHECK(ar.Serialize("Buildings", _buildings, 1))
	CHECK(ar.Serialize("OutVehicles", _outVehicles, 1))
	if (ar.IsLoading())
	{
		for (int i=0; i<_outVehicles.Size(); i++)
		{
			// set MoveOut flags of all "out" vehicles to true
			Entity *veh = _outVehicles[i];
			veh->SetMoveOutFlag();
			// parent should be serialized in vehicle
		}
	}
	CHECK(ar.Serialize("NearImportance", _nearImportanceDistributionTime, 1))
	CHECK(ar.Serialize("FarImportance", _farImportanceDistributionTime, 1))
	return LSOK;
}

static const EnumName GameModeNames[]=
{
	EnumName(GModeNetware, "NETWARE"),
	EnumName(GModeArcade, "ARCADE"),
	EnumName(GModeIntro, "INTRO"),
	EnumName(GModeArcade, "NORMAL"),
	EnumName(GModeArcade, "TRAINING"),
	EnumName()
};

template<>
const EnumName *GetEnumNames( GameMode dummy )
{
	return GameModeNames;
}

bool ProcessTemplateName(RString name);
bool ProcessFullName(RString name);
bool ParseMission(bool multiplayer);
void OpenEditor();
void StartIntro();
void StartMission();
//bool IsHeader();
void StartCampaign(RString campaign, Display *disp);
RString GetBaseDirectory();
RString GetBaseSubdirectory();
void SetBaseDirectory(RString dir);
void SetBaseSubdirectory(RString dir);
void SetCampaign(RString name);
void SetMission(RString world, RString mission);
RString GetCampaignSaveDirectory(RString campaign);

/*!
\patch 1.01 Date 06/11/2001 by Jirka
- Fixed: objectives was not updated after load
- after load there was errors in the list of mission objectives
\patch 1.27 Date 10/16/2001 by Ondra
- Fixed: setviewdistance value saved when saving game.
\patch 1.31 Date 11/23/2001 by Ondra
- Fixed: cadet / veteran mode was missing in save
\patch 1.89 Date 10/23/2002 by Jirka
- Fixed: onMapSingleClick handler is now saved when saving game
*/

LSError World::Serialize(ParamArchive &ar, int message)
{
	if (ar.IsSaving())
	{
		ProgressReset();
		ProgressClear(false);
//		ProgressStart(LocalizeString(IDS_SAVE_GAME));
		ProgressStart(LocalizeString(message));
		CHECK(ar.Serialize("CurrentCampaign", CurrentCampaign, 1, ""))
	}
	else if (ar.GetPass() == ParamArchive::PassFirst)
	{
		RString campaign;
		CHECK(ar.Serialize("CurrentCampaign", campaign, 1, ""))
		SetCampaign(campaign);
	}

	CHECK(ar.Serialize("CurrentBattle", CurrentBattle, 1, ""))
	CHECK(ar.Serialize("CurrentMission", CurrentMission, 1, ""))
	
	CHECK(ar.SerializeEnum("mode", _mode, 1))
	CHECK(ar.SerializeEnum("endMission", _endMission, 1, (EndMode)EMContinue))

	CHECK(ar.Serialize("cadetMode", Glob.config.easyMode, 1, false))

	CHECK(ar.Serialize("nextMagazineID", _nextMagazineID, 1, 0))

	CHECK(ar.Serialize("Radio", *_radio, 1))
//	CHECK(ar.Serialize("Map", *_map, 11))
	LSError SerializeMapInfo(ParamArchive &ar, RString name, int minVersion);
	CHECK(SerializeMapInfo(ar, "Map", 11))

	Landscape *land = _scene.GetLandscape();
	if (ar.IsSaving())
	{
		ResetIDs();
		land->RebuildIDCache();

		RString worldName = land->GetName();
		CHECK(ar.Serialize("worldName", worldName, 1))

		AutoArray<RString> addons;
		for (int i=0; i<_activeAddons.GetSize(); i++)
		{
			addons.Add(_activeAddons.Get(i));
		}
		CHECK(ar.SerializeArray("addons",addons,1))
	}
	else if (ar.GetPass() == ParamArchive::PassFirst)
	{
		// lock all vehicles and types
		// this avoid releasing shapes and textures that will be used again
		VehicleTypes.LockAllTypes();
		GEngine->TextBank()->LockAllTextures();

		Clear();
		CurrentTemplate.Clear();

		RString worldName;
		CHECK(ar.Serialize("worldName", worldName, 1))
		SwitchLandscape(worldName);
		// adjust terrain subdivision as necessary
		AdjustSubdivision(GModeArcade);

		ProgressReset();
//		ProgressStart(LocalizeString(IDS_LOAD_GAME));
		ProgressStart(LocalizeString(message));
		FindArrayRStringCI addons;
		CHECK(ar.SerializeArray("addons",addons,1))
		// add addons from mission template
		ActivateAddons(addons);
	}
	CHECK(ar.Serialize("Landscape", *land, 1))

	CHECK(SerializeVehicles(ar))
	CHECK(ar.Serialize("SensorList", _sensorList, 1))
	CHECK(ar.Serialize("Clock", Glob.clock, 1))
	CHECK(ar.Serialize("GameState", GGameState, 1))

	CHECK(ar.Serialize("actualOvercast", _actualOvercast, 1))
	CHECK(ar.Serialize("wantedOvercast", _wantedOvercast, 1))
	CHECK(ar.Serialize("actualFog", _actualFog, 1))
	CHECK(ar.Serialize("wantedFog", _wantedFog, 1))
	CHECK(ar.Serialize("speedOvercast", _speedOvercast, 1))
	CHECK(ar.Serialize("weatherTime", _weatherTime, 1))
	CHECK(ar.Serialize("nextWeatherChange", _nextWeatherChange, 1))

	CHECK(ar.Serialize("horizontZ", Glob.config.horizontZ, 1, 900))
	CHECK(ar.Serialize("tacticalZ", Glob.config.tacticalZ, 1, 900))
	CHECK(ar.Serialize("objectsZ", Glob.config.objectsZ, 1, 600))
	CHECK(ar.Serialize("shadowsZ", Glob.config.shadowsZ, 1, 250))


	CHECK(ar.SerializeRef("playerOn", _playerOn, 1))
	CHECK(ar.SerializeRef("cameraOn", _cameraOn, 1))
	CHECK(ar.SerializeRef("realPlayer", _realPlayer, 1))
	CHECK(ar.Serialize("playerManual", _playerManual, 1, true))
	CHECK(ar.Serialize("playerSuspended", _playerSuspended, 1, false))

	CHECK(ar.Serialize("EastCenter", _eastCenter, 1))
	CHECK(ar.Serialize("WestCenter", _westCenter, 1))
	CHECK(ar.Serialize("GuerrilaCenter", _guerrilaCenter, 1))
	CHECK(ar.Serialize("CivilianCenter", _civilianCenter, 1))
	CHECK(ar.Serialize("LogicCenter", _logicCenter, 1))
	CHECK(AIGlobalSerialize(ar))

	CHECK(ar.Serialize("Scripts", _scripts, 3))

	CHECK(ar.Serialize("OnMapSingleClick", GMapOnSingleClick, 1, RString()))
	
	CHECK(ar.Serialize("CameraEffect", _cameraEffect, 1))

	if (ar.IsSaving())
	{
		RString dir = GetBaseDirectory();
		CHECK(ar.Serialize("directory", dir, 1, ""))
		dir = GetBaseSubdirectory();
		CHECK(ar.Serialize("subdirectory", dir, 1, ""))
		RString mission = Glob.header.filename;
		CHECK(ar.Serialize("mission", mission, 1, ""))
		CHECK(ar.Serialize("filenameReal", Glob.header.filenameReal, 1, ""))
		ProgressFinish();
	}
	else if (ar.GetPass() == ParamArchive::PassFirst)
	{
		// TODO: remove Glob.header
		char wname[256];
		strcpy(wname, land->GetName());
		char *ext = strrchr(wname, '.');
		if (ext) *ext = 0;
		char *world = strrchr(wname, '\\');
		if (world) world++;
		else world = wname;

		RString dir;
		CHECK(ar.Serialize("directory", dir, 1, ""))
		SetBaseDirectory(dir);
		RString mission;
		CHECK(ar.Serialize("mission", mission, 1, ""))
		SetMission(world, mission);
		CHECK(ar.Serialize("subdirectory", dir, 1, ""))
		SetBaseSubdirectory(dir);
		CHECK(ar.Serialize("filenameReal", Glob.header.filenameReal, 1, ""))

		ParseMission(false);
	}
	else
	{
		_camType=_camTypeMain=CamInternal;
		InitCameraPars();

		_scene.MainLight()->Recalculate(this);
		_scene.MainLightChanged();

		AIUnit *player = _playerOn ? _playerOn->Brain() : NULL;
		AIGroup *grp = player ? player->GetGroup() : NULL;
		AICenter *center = grp ? grp->GetCenter() : NULL;
		Glob.header.playerSide = center ? center->GetSide() : TSideUnknown;

		VehicleTypes.UnlockAllTypes(); // locked from SwitchLandscape
		GEngine->TextBank()->UnlockAllTextures();
		GEngine->TextBank()->Preload();

		Shapes.OptimizeAll();

		// fixed - objectives not updated after load
		DisplayMap *map = dynamic_cast<DisplayMap *>((AbstractOptionsUI *)_map);
		if (map)
		{
			map->UpdatePlan();
		}

		ProgressFinish();
	}

	return LSOK;
}

void World::DoKeyDown( unsigned wParam, unsigned nRepCnt, unsigned nFlags )
{
	if (!IsUserInputEnabled()) return;

	if (_warningMessage)
	{
		_warningMessage->OnKeyDown(wParam, nRepCnt, nFlags);
		if (_warningMessage->GetExitCode() >= 0) _warningMessage = NULL;
		return;
	}
	if (_voiceChat)
	{
		if (_voiceChat->DoKeyDown(wParam,nRepCnt,nFlags)) return;
	}
	if (_chat)
	{
		if (_chat->DoKeyDown(wParam,nRepCnt,nFlags)) return;
	}
	if (!HasOptions())
	{
		if (_userDlg)
		{
			if (_userDlg->DoKeyDown(wParam,nRepCnt,nFlags)) return;
		}
		else if (_map && _showMap)
		{
			if (_map->DoKeyDown(wParam,nRepCnt,nFlags)) return;
		}
	}
	if (_options)
	{
		if (_options->DoKeyDown(wParam,nRepCnt,nFlags)) return;
	}
}

void World::DoKeyUp( unsigned wParam, unsigned nRepCnt, unsigned nFlags )
{
	if (!IsUserInputEnabled()) return;

	if (_warningMessage)
	{
		_warningMessage->OnKeyUp(wParam, nRepCnt, nFlags);
		if (_warningMessage->GetExitCode() >= 0) _warningMessage = NULL;
		return;
	}
	if (_voiceChat)
	{
		if (_voiceChat->DoKeyUp(wParam,nRepCnt,nFlags)) return;
	}
	if (_chat)
	{
		if (_chat->DoKeyUp(wParam,nRepCnt,nFlags)) return;
	}
	if (!HasOptions())
	{
		if (_userDlg)
		{
			if (_userDlg->DoKeyUp(wParam,nRepCnt,nFlags)) return;
		}
		else if (_map && _showMap)
		{
			if (_map->DoKeyUp(wParam,nRepCnt,nFlags)) return;
		}
	}
	if (_options)
	{
		if (_options->DoKeyUp(wParam,nRepCnt,nFlags)) return;
	}
}

void World::DoChar( unsigned nChar, unsigned nRepCnt, unsigned nFlags )
{
	if (!IsUserInputEnabled()) return;

	if (_warningMessage)
	{
		_warningMessage->OnChar(nChar, nRepCnt, nFlags);
		if (_warningMessage->GetExitCode() >= 0) _warningMessage = NULL;
		return;
	}
	if (_voiceChat)
	{
		if (_voiceChat->DoChar(nChar,nRepCnt,nFlags)) return;
	}
	if (_chat)
	{
		if (_chat->DoChar(nChar,nRepCnt,nFlags)) return;
	}
	if (!HasOptions())
	{
		if (_userDlg)
		{
			if (_userDlg->DoChar(nChar,nRepCnt,nFlags)) return;
		}
		else if (_map && _showMap)
		{
			if (_map->DoChar(nChar,nRepCnt,nFlags)) return;
		}
	}
	if (_options)
	{
		if (_options->DoChar(nChar,nRepCnt,nFlags)) return;
	}
}

void World::DoIMEChar( unsigned nChar, unsigned nRepCnt, unsigned nFlags )
{
	if (!IsUserInputEnabled()) return;

	if (_warningMessage)
	{
		_warningMessage->OnIMEChar(nChar, nRepCnt, nFlags);
		if (_warningMessage->GetExitCode() >= 0) _warningMessage = NULL;
		return;
	}
	if (_voiceChat)
	{
		if (_voiceChat->DoIMEChar(nChar,nRepCnt,nFlags)) return;
	}
	if (_chat)
	{
		if (_chat->DoIMEChar(nChar,nRepCnt,nFlags)) return;
	}
	if (!HasOptions())
	{
		if (_userDlg)
		{
			if (_userDlg->DoIMEChar(nChar,nRepCnt,nFlags)) return;
		}
		else if (_map && _showMap)
		{
			if (_map->DoIMEChar(nChar,nRepCnt,nFlags)) return;
		}
	}
	if (_options)
	{
		if (_options->DoIMEChar(nChar,nRepCnt,nFlags)) return;
	}
}

void World::DoIMEComposition( unsigned nChar, unsigned nFlags )
{
	if (!IsUserInputEnabled()) return;

	if (_warningMessage)
	{
		_warningMessage->OnIMEComposition(nChar, nFlags);
		if (_warningMessage->GetExitCode() >= 0) _warningMessage = NULL;
		return;
	}
	if (_voiceChat)
	{
		if (_voiceChat->DoIMEComposition(nChar,nFlags)) return;
	}
	if (_chat)
	{
		if (_chat->DoIMEComposition(nChar,nFlags)) return;
	}
	if (!HasOptions())
	{
		if (_userDlg)
		{
			if (_userDlg->DoIMEComposition(nChar,nFlags)) return;
		}
		else if (_map && _showMap)
		{
			if (_map->DoIMEComposition(nChar,nFlags)) return;
		}
	}
	if (_options)
	{
		if (_options->DoIMEComposition(nChar,nFlags)) return;
	}
}


void World::SaveCrash() const
{
	SaveBin("$_crash_$.fps", IDS_SAVE_GAME);
}

void SaveCrash()
{
	GWorld->SaveCrash();
}

CameraEffect::CameraEffect( Object *object )
:_object(object)
{
	// each camera effect is assinged to something
}
CameraEffect::~CameraEffect()
{
}

void CameraEffect::Draw() const
{
	// draw cinema borders
	if (showCinemaBorder)
	{
		Object cinema(GScene->Preloaded(CinemaBorder),-1);
		cinema.Draw2D(0);	
	}
}

void World::StartIntro()
{
	// load mission or save game if passed as argument
	CurrentBattle = "";
	CurrentMission = "";
	
#if _ENABLE_PARAMS
	const char *ext=strrchr(LoadFile,'.');
	if( ext && QIFStreamB::FileExist(LoadFile) )
	{
		if( !strcmpi(ext,".fps") )
		{
			// load saved game
			LoadBin(LoadFile, IDS_LOAD_GAME);
			::StartMission();
		}
		else if( !strcmpi(ext,".sqg") )
		{
			// load saved game
			Load(LoadFile, IDS_LOAD_GAME);
			::StartMission();
		}
#if _ENABLE_EDITOR
		else if( !strcmpi(ext,".sqm") )
		{
			if (ProcessFullName(LoadFile))
			{
				extern bool AutoTest;
				if (AutoTest)
				{
					bool StartAutoTest();
					StartAutoTest();
				}
				else
				{
					OpenEditor();
				}
			}
		}
#endif
	}
	else
#endif
	{
		RString campaign = Pars >> "CfgIntro" >> "firstCampaign";
/*
		if (IsHeader() || campaign.GetLength() == 0)
		{
			// start intro
			void StartRandomCutscene(RString world);
			RString world = Pars >> "CfgWorlds" >> "initWorld";
			StartRandomCutscene(world);
		}
		else
		{
			// start campaign
			StartCampaign(campaign, NULL);
		}
*/
		if
		(
			campaign.GetLength() > 0 &&
			QIFStream::FileExists(GetCampaignSaveDirectory(campaign) + RString("continue.fps"))
		)
		{
			// start campaign
			StartCampaign(campaign, NULL);
		}
		else
		{
			// start intro
			void StartRandomCutscene(RString world);
#if _FORCE_DEMO_ISLAND
			RString world = Pars >> "CfgWorlds" >> "demoWorld";
#else
			RString world = Pars >> "CfgWorlds" >> "initWorld";
#endif
			StartRandomCutscene(world);
		}
	}
}

void World::StopIntro()
{
}

void World::StartLogo()
{
}

void World::StopLogo()
{
}


