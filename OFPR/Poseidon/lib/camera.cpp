// Poseidon - camera
// (C) 1997, SUMA
#include "wpch.hpp"
#include "textbank.hpp"

#include "camera.hpp"
#include "engine.hpp"
#include <Es/Framework/debugLog.hpp>

// frame interface

Camera::Camera()
://_userClip(-1),
//_perspective(MIdentity),
_projection(MIdentity),
_scale(MIdentity),_invScale(MIdentity),
_scaledInvTransform(MIdentity),
_camNormalTrans(M3Identity),
_camInvTrans(MIdentity),
_userClip(false)
{
	//_userClipDirWorld[0]=VZero;
	//_userClipDirWorld[1]=VZero;
	//_userClipDir=VZero;
}

void Camera::SetPerspective
(
	Coord cNear, Coord cFar, Coord cLeft, Coord cTop
)
{
	_cNear=cNear,_cFar=cFar,_cLeft=cLeft,_cTop=cTop;
	_cAddNear = cNear;
	_cAddFar = cFar;
}

/*!
\patch_internal 1.21 Date 8/16/2001 by Ondra.
- New: additional clipping planes (used for clipping far grass).
*/

void Camera::Adjust( Engine *engine )
{
	//_perspective=Matrix4(MPerspective,1,1);
	_invCLeft=1/_cLeft;
	_invCTop=1/_cTop;

	_projectionNormal = MZero;
	_projectionNormal(0,0) = _invCLeft;
	_projectionNormal(1,1) = _invCTop;
	// check engine abilities
	//float ccFar = floatMax(5000,_cFar);
	float ccFar = floatMax(500,_cFar*1.01);
	//if (GEngine->GetTL() && GEngine->HasWBuffer())
	if (GEngine->HasWBuffer() && GEngine->IsWBuffer())
	{
		// far is critical for precision of w-buffer
		ccFar = floatMax(100,_cFar*1.1);
	}


	float q = ccFar/(ccFar-_cNear);
	_projectionNormal(2,2) = q;
	_projectionNormal.SetPosition(Vector3(0,0,-q*_cNear));

	_projection.SetZero();
	// see DX docs: What Is the Projection Transformation?
	// (note: DX uses row vectors)
	// our setup of DX matrix
	// |1/fovW     0   0    0    |   |x|   |   don't care     |
	// |   0    1/fowH 0    0    |   |y|   |   don't care     |
	// |   0       0   q -q*cNear| * |z| = | q*z - q*cNear    |
	// |   0       0   1    0    |   |1|   |       z          |

	// perspective:       z = (q*z - q*cNear)/z = q - q*cNear/z

	int w=engine->Width();
	int h=engine->Height();
	// y is negative - positive should go up
	// cLeft and cTop is ignored now - therefore sides of viewing frustum is viewport independent
	// this is used to simplify clipping tests

	// our setup of custom matrix
	// |  w/2   0   w/2   0    |   |x|   |  don't care      |
	// |   0   h/2  h/2   0    |   |y|   |  don't care      |
	// |   0    0    q -q*cNear| * |z| = | q*z - q*cNear    |
	// |   0    0    1    0    |   |1|   |       z          |

	// guarantee sky and clouds are visible
	// TODO: separate camera settings for sky & clouds
	//  direct projection construction
	_projection(0,0) =  w*0.5f;
	_projection(1,1) = -h*0.5f;
	_projection(2,2) = q;
	_projection(0,2) =  w*0.5f;
	_projection(1,2) =  h*0.5f;
	_projection.SetPosition(Vector3(0,0,-q*_cNear));

	_scale=Matrix4(MScale,_invCLeft,_invCTop,1);
	// after easy clipping rescale viewing frustum to get correct screen coordinates
	_invScale=Matrix4(MScale,_cLeft,_cTop,1);

	_scaledInvTransform=_scale*GetInvTransform();
	_camNormalTrans=Matrix3(MNormalTransform,_scaledInvTransform.Orientation());
	#if 0
		// TODO: BUG: _scaledInvTransform is sometimes not SR
		float mScale = _scaledInvTransform.Scale();
		_scaledInvTransform.SetUpAndAside
		(
			_scaledInvTransform.DirectionUp(),_scaledInvTransform.DirectionAside()
		);
		_scaledInvTransform.SetScale(mScale);
	#endif

	_camInvTrans=Matrix4(MInverseScaled,_scaledInvTransform);

	// calculate world space clipping planes
	Vector3 pl(+1,0,_cLeft);
	Vector3 pt(0,+1,_cTop);
	pl.Normalize();
	pt.Normalize();
	Vector3 pr(-pl[0],0,pl[2]);
	Vector3 pb(0,-pt[1],pt[2]);

	Vector3Val pos = Position();

	DirectionModelToWorld(pr,pr);
	DirectionModelToWorld(pl,pl);
	DirectionModelToWorld(pb,pb);
	DirectionModelToWorld(pt,pt);

	_rClipPlane=Plane(pr,pos);
	_lClipPlane=Plane(pl,pos);
	_tClipPlane=Plane(pt,pos);
	_bClipPlane=Plane(pb,pos);

	//_nClipPlane=Plane(Direction(),pos+Direction()*_cNear);
	//_fClipPlane=Plane(-Direction(),pos+Direction()*_cFar);

	float nearClip = floatMax(_cNear,_cAddNear);
	float farClip = floatMin(_cFar,_cAddFar);

	_nClipPlane=Plane(Direction(),pos+Direction()*nearClip);
	_fClipPlane=Plane(-Direction(),pos+Direction()*farClip);

	#if 1
	// calculate world space guard plane equations

	// (-1,-1) is projected to (0,0)
	// (+1,+1) is projected to (w,h)
	// (a,b) -> ( (a+1)*w/2, (b+1)*h/2 )

	// what is projected to (gx,gy)
	// a*w/2 + w/2 = gx,  a = (gx - w/2)/(w/2), a = gx/(w/2)-1
	// b*h/2 + h/2 = gy,  b = (gy - h/2)/(h/2), b = gy/(h/2)-1
	float invW2 = 2.0f/w;
	float invH2 = 2.0f/h;

	float minX = engine->MinGuardX()*invW2-1;
	float maxX = engine->MaxGuardX()*invW2-1;
	float minY = engine->MinGuardY()*invH2-1;
	float maxY = engine->MaxGuardY()*invH2-1;

	// TODO: verify minY, maxY - possible sign change
	// minX, minY is negative

	Vector3 gl(+1,0,-_cLeft*minX);
	Vector3 gt(0,+1,-_cTop*minY);
	Vector3 gr(-1,0,+_cLeft*maxX);
	Vector3 gb(0,-1,+_cTop*maxY);

	// left: we want to find plane that goes through points:
	//o,gl,gl+VUp
	// normal of such plane is:
	// gl x (gl+VUp)

	gl.Normalize();
	gt.Normalize();
	gr.Normalize();
	gb.Normalize();

	DirectionModelToWorld(gr,gr);
	DirectionModelToWorld(gl,gl);
	DirectionModelToWorld(gb,gb);
	DirectionModelToWorld(gt,gt);


	_rGuardPlane=Plane(gr,pos);
	_lGuardPlane=Plane(gl,pos);
	_tGuardPlane=Plane(gt,pos);
	_bGuardPlane=Plane(gb,pos);
	#else
	_rGuardPlane = _rClipPlane;
	_lGuardPlane = _lClipPlane;
	_tGuardPlane = _tClipPlane;
	_bGuardPlane = _bClipPlane;
	#endif

}

void Camera::SetAdditionalClipping(float cNear, Coord cFar )
{
	_cAddFar = cFar;
	_cAddNear = cNear;

	Adjust(GEngine);
}

void Camera::SetClipRange( Coord cNear, Coord cFar )
{
	_cNear = cNear;
	_cFar = cFar;
	// reset addional clipping planes
	_cAddNear = cNear;
	_cAddFar = cFar;
	Adjust(GEngine);
}


#pragma optimize("t",on) // optimize for speed instead of size

	// world space clipping
ClipFlags Camera::IsClipped( Vector3Par point, float radius, int userPlane ) const
{
	ClipFlags clip=ClipNone;
	//float ll=_lClipPlane.Distance(point);
	//float rr=_rClipPlane.Distance(point);
	//float tt=_tClipPlane.Distance(point);
	//float bb=_bClipPlane.Distance(point);
	if( _nClipPlane.Distance(point)<=-radius ) clip|=ClipFront;
	if( _fClipPlane.Distance(point)<=-radius ) clip|=ClipBack;
	if( _rClipPlane.Distance(point)<=-radius ) clip|=ClipRight;
	if( _lClipPlane.Distance(point)<=-radius ) clip|=ClipLeft;
	if( _tClipPlane.Distance(point)<=-radius ) clip|=ClipTop;
	if( _bClipPlane.Distance(point)<=-radius ) clip|=ClipBottom;

	/*
	if( userPlane>=0 )
	{
		Vector3Val planeDir=GetUserClipDir(userPlane);
		float planeVal=GetUserClipVal(userPlane);
		if( point*planeDir+planeVal<=-radius ) clip|=ClipUser0;
	}
	*/
	return clip;
}

ClipFlags Camera::MayBeClipped( Vector3Par point, float radius, int userPlane ) const
{
	ClipFlags clip=ClipAll;
	float dn = _nClipPlane.Distance(point);
	float df = _fClipPlane.Distance(point);
	if( dn>=+radius ) clip&=~ClipFront;
	if( df>=+radius ) clip&=~ClipBack;
	#define CHECK_GUARD_CLIP 0
	#if CHECK_GUARD_CLIP
	ClipFlags vClip = clip;
	float dr = _rClipPlane.Distance(point);
	float dl = _lClipPlane.Distance(point);
	float dt = _tClipPlane.Distance(point);
	float db = _bClipPlane.Distance(point);
	if( dr>=+radius ) vClip&=~ClipRight;
	if( dl>=+radius ) vClip&=~ClipLeft;
	if( dt>=+radius ) vClip&=~ClipTop;
	if( db>=+radius ) vClip&=~ClipBottom;
	#endif

	// we know we are clipping
	// check against guard planes to known which planes we must clip

	float gr = _rGuardPlane.Distance(point);
	float gl = _lGuardPlane.Distance(point);
	float gt = _tGuardPlane.Distance(point);
	float gb = _bGuardPlane.Distance(point);
	if( gr>=+radius ) clip&=~ClipRight;
	if( gl>=+radius ) clip&=~ClipLeft;
	if( gt>=+radius ) clip&=~ClipTop;
	if( gb>=+radius ) clip&=~ClipBottom;

	#if CHECK_GUARD_CLIP
	// if front clipping, some clipping flags may be wrong
	if ( (clip&1)==0 && (clip&~vClip)!=0)
	{
		LogF("Incorrect clip flags %2x,%2x",clip,vClip);
	}
	#endif

	/*
	if( userPlane>=0 )
	{
		Vector3Val planeDir=GetUserClipDir(userPlane);
		float planeVal=GetUserClipVal(userPlane);
		if( point*planeDir+planeVal>=+radius ) clip&=~ClipUser0;
	}
	else
	{
		clip&=~ClipUser0;
	}
	clip&=~ClipUser1; // User1 clipping plane always disabled
	*/
	return clip;
}

#pragma optimize( "", on ) // restore default setting

void Camera::CancelUserClip()
{
	_userClip = false;
}

void Camera::SetUserClipPars(Vector3Par dir, float val)
{
	_userClip = true;

	_userClipDirWorld = dir;
	_userClipValWorld = val;

	// recalculate user clipping planes to view space
	// how normals are converted?
	Matrix4Val viewTransform=_scale*GetInvTransform(); // from world to view
	Matrix4Val invViewTransform=viewTransform.InverseScaled(); // from view to world
	//Matrix4 invViewTransform=viewTransform.GeneralInverse(); // from view to world
	// let:  n=_userClipDir[i], d=_userClipVal[i]
	//       x=checked point
	//       M=invViewTransform.Orientation()
	//       m=invViewTransform.Position()

	// then:
	//       world space clipping equation is:
	//       n.x + d=0
	//
	// world space check can be applied to Mx+m
	//       n.(Mx+m) + d =0
	//       n.Mx+n.m +d =0
	// dot product can be rewritten using transpose n.x = T(n)x
	//       T(n)Mx +n.m+d =0
	//       ( T(n)M ) x +n.m+d=0
	Matrix3Val matrixM=invViewTransform.Orientation();
	Vector3Val vectorM=invViewTransform.Position();
	Vector3Val n=_userClipDirWorld;
	float d=_userClipValWorld;
	_userClipVal=n*vectorM+d;
	_userClipDir=n*matrixM;
}
