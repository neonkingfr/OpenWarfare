// precompiled header files - many files use something of Windows API
#include <El/elementpch.hpp>

#if _DEMO
	#include "demoConfig.h"
#elif _MP_DEMO
	#include "mpDemoConfig.h"
#elif _MP_TEST
	#include "mpTestConfig.h"
#elif _MPTEST_SERVER
	#include "mpTestServerConfig.h"
#elif _DED_SERVER
	#include "dedServerConfig.h"
#elif _DESIGN_SRELEASE
	#include "desReleaseConfig.h"
#elif _VBS1_SERVER
	#include "VBS1ServerConfig.h"
#elif _VBS1_DEMO
	#include "VBS1DemoConfig.h"
#elif _VBS1
	#include "VBS1Config.h"
#elif _GALATEA
	#include "galateaConfig.h"
#elif _COLD_WAR_ASSAULT
	#include "coldWarAssaultConfig.h"
#elif _RESISTANCE
	#include "resistanceConfig.h"
#elif _RESISTANCE_SERVER
	#include "resistanceServerConfig.h"
#elif _RESISTANCE_DEMO
	#include "resistanceDemoConfig.h"
#elif _SUPER_RELEASE
	#include "sReleaseConfig.h"
#elif _CONSOLE
	#include "consoleConfig.h"
#elif _USE_AFX
	#include "afxConfig.h"
#else
	#include "normalConfig.h"
#endif

#ifdef _XBOX
	// some platform specific overrides, configuration independent
	#include "xboxConfig.h"
#endif

// user general include
#include "types.hpp"
#include <El/Math/math3d.hpp>

// global diagnostics

void CCALL GlobalShowMessage( int timeMs, const char *msg, ... );
