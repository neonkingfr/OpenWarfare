#ifdef _MSC_VER
#pragma once
#endif

#ifndef _MULTISYNC_HPP
#define _MULTISYNC_HPP

#ifdef _WIN32

#include <Es/Common/win.h>

class SignaledObject
{
	private:
	// no copy
	SignaledObject(const SignaledObject &src);
	void operator =(const SignaledObject &src);

	protected:
	mutable HANDLE _handle;

	public:
	SignaledObject( HANDLE handle ):_handle(handle)
	{
		if( !_handle ) ErrorMessage("Win32 Error: Cannot create MT object.");
	}
	~SignaledObject(){if( _handle ) CloseHandle(_handle),_handle=NULL;}

	// wait/lock are synonymous
	bool Wait() const {return WaitForSingleObject(_handle,INFINITE)==WAIT_OBJECT_0;}
	bool TryWait( DWORD time=0 ) const {return WaitForSingleObject(_handle,time)==WAIT_OBJECT_0;}
	bool Lock() const {return WaitForSingleObject(_handle,INFINITE)==WAIT_OBJECT_0;}
	bool TryLock() const {return WaitForSingleObject(_handle,0)==WAIT_OBJECT_0;}

	static int WaitForMultiple( SignaledObject *events[], int n, int timeout=-1 );
};

class Event: public SignaledObject
{
	public:
	Event()
	:SignaledObject(CreateEvent(NULL,FALSE,FALSE,NULL))
	{
	}
	explicit Event(bool manualReset)
	:SignaledObject(CreateEvent(NULL,manualReset,FALSE,NULL))
	{
	}
	void Set(){if( _handle ) SetEvent(_handle);}
	void Reset(){if( _handle ) ResetEvent(_handle);}

};

class Mutex: public SignaledObject
{
	public:
	Mutex()
	:SignaledObject(CreateMutex(NULL,FALSE,NULL))
	{}
	
	void Unlock() const {ReleaseMutex(_handle);}
	bool IsLocked() const
	{
		bool ret=TryLock();
		if( ret ) Unlock();
		return ret;
	}
};

class Semaphore: public SignaledObject
{
	// example: serialize disk access between audio and texture
	// semaphore semantics would be: signaled when disk is busy
	// when texture load receives a request for loading, is Unlocks semaphore
	// (semaphore becomes signaled)
	// after processing request (load or delete), semaphore is Locked
	// and eventually becomes non-signaled
	// audio can check semaphore to see if texture loader uses disk
	// if semaphore is signaled, disk is busy
	
	public:
	Semaphore( int init=0, int max=INT_MAX )
	:SignaledObject(CreateSemaphore(NULL,init,max,NULL))
	{
	}
	
	bool Unlock( int count=1 ) const {return ReleaseSemaphore(_handle,count,NULL)!=FALSE;}
	bool IsLocked() const
	{
		bool ret=TryLock();
		if( ret ) Unlock();
		return ret;
	}
};

// multithread synchronization functionality
class CriticalSection
{
	private:
	mutable CRITICAL_SECTION _handle;
	
	public:
	CriticalSection(){InitializeCriticalSection(&_handle);}
	~CriticalSection(){DeleteCriticalSection(&_handle);}
	
	bool Lock() const {EnterCriticalSection(&_handle);return true;}
	void Unlock() const {LeaveCriticalSection(&_handle);}
};

#include <Es/Types/scopeLock.hpp>

class ScopeLockMutex: public ScopeLock<Mutex>
{
	public:
	ScopeLockMutex( Mutex &lock )
	:ScopeLock<Mutex>(lock)
	{}
	bool TryLock() const {return _lock->TryLock();}
};

typedef ScopeLock<CriticalSection> ScopeLockSection;

#else

//----------------- POSIX implementation ---------------------

#include "Es/essencepch.hpp"
#include "Es/Threads/pocritical.hpp"

class SignaledObject
{
	private:
	// no copy
	SignaledObject ( const SignaledObject &src );
	void operator = ( const SignaledObject &src );

	public:
	SignaledObject ()
	{}
	~SignaledObject()
	{}

	// wait/lock are synonymous
	bool Wait () const
	{ return TRUE; }
	
	bool TryWait ( DWORD time=0 ) const
	{ return TRUE; }
	
	bool Lock () const
	{ return TRUE; }
	
	bool TryLock () const
	{ return TRUE; }

	static int WaitForMultiple ( SignaledObject *events[], int n, int timeout=-1 );
};

class Event: public SignaledObject
{
	public:
	Event ()
	{}
	explicit Event ( bool manualReset )
	{}
	void Set ()
	{}
	void Reset ()
	{}
};

class Mutex: public SignaledObject
{
        protected:
	mutable pthread_mutex_t mutex;
	
	public:
	Mutex ()
	{
	    mutex = mutexInit;
	}
	void Unlock () const
	{
	    pthread_mutex_unlock(&mutex);
	}
	bool IsLocked () const
	{
	    bool ret = (pthread_mutex_trylock(&mutex) == 0);
	    if ( ret ) Unlock();
	    return ret;
	}
};

class Semaphore: public SignaledObject
{
        protected:
	mutable sem_t sem;
	// example: serialize disk access between audio and texture
	// semaphore semantics would be: signaled when disk is busy
	// when texture load receives a request for loading, is Unlocks semaphore
	// (semaphore becomes signaled)
	// after processing request (load or delete), semaphore is Locked
	// and eventually becomes non-signaled
	// audio can check semaphore to see if texture loader uses disk
	// if semaphore is signaled, disk is busy
	
	public:
	Semaphore ( int init=0, int max=INT_MAX )
	{
	    sem_init(&sem,0,(unsigned)init);
	}
	bool Unlock ( int count=1 ) const
	{
	    while ( count-- > 0 )
	        sem_post(&sem);
	    return TRUE;
	}
	bool IsLocked () const
	{
	    int val = 0;
	    if ( sem_getvalue(&sem,&val) != 0 ) val = 0;
	    return( val != 0 );
	}
};

// multithread synchronization functionality
class CriticalSection
{
	private:
	mutable pthread_mutex_t mutex;
	
	public:
	CriticalSection ()
	{
	    mutex = mutexInit;
	}
	~CriticalSection ()
	{
	    pthread_mutex_destroy(&mutex);
	}
	bool Lock () const
	{
	    pthread_mutex_lock(&mutex);
	    return true;
	}
	void Unlock () const
	{
	    pthread_mutex_unlock(&mutex);
	}
};

#include <Es/Types/scopeLock.hpp>

class ScopeLockMutex: public ScopeLock<Mutex>
{
	public:
	ScopeLockMutex ( Mutex &lock ) : ScopeLock<Mutex>(lock)
	{}
	bool TryLock() const
	{
	    return _lock->TryLock();
	}
};

typedef ScopeLock<CriticalSection> ScopeLockSection;

#endif

#endif
