#include "wpch.hpp"

//#define SurfaceInfoD3D @@

#if !_DISABLE_GUI

#include <El/common/perfLog.hpp>

//#include "global.hpp"
//#include "progress.hpp"
#include "txtD3D.hpp"
#include "engDD.hpp"
#include "multiSync.hpp"
#include "fileServer.hpp"
#include <El/QStream/QBStream.hpp>
#include <El/common/randomGen.hpp>
#include "keyInput.hpp"
#include "dikCodes.h"
#include "progress.hpp"
#include <Es/Algorithms/qsort.hpp>

#ifdef _XBOX
#include <xgraphics.h>
#endif

extern SIZE_T GetMemoryUsedSize();

static const char *FormatName( PacFormat format )
{
	switch( format )
	{
		case PacARGB1555: return "1555";
		case PacRGB565: return "o565";
		case PacARGB4444: return "4444";
		case PacDXT1: return "DXT1";
		case PacDXT2: return "DXT2";
		case PacDXT3: return "DXT3";
		case PacDXT4: return "DXT4";
		case PacDXT5: return "DXT5";
		default: return "xxxx";
	}
}

inline int AlignVRAM( int x )
{
	int align = 256; // must be power of 2
	int alignMask = align-1;
	return (x+alignMask)&~alignMask;
}


#define MIN_MIP_SIZE 4 // guarantee QWORD alignment (4 16-bit pixels are enough)

#define REPORT_ALLOC 0 // verbosity level 0..3

#define USE_MANAGE 0 // TODO: small/large texture bank, try manager

static PacFormat BasicFormat( const char *name )
{
	const char *ext=strrchr(name,'.');
	if( ext && !strcmpi(ext,".paa") ) return PacARGB4444;
	else return PacARGB1555;
}

#ifdef _XBOX

#include <xmmintrin.h>
#define AUTO_STATIC_ARRAY_16(Type,name,size) \
	AUTO_STATIC_ARRAY_ALIGNED(Type,name,size,__m128)

static int LoadMipmap
(
	void *dta, PacLevelMem &mip,
	const ITextureSource *src, int i
)
{
	int ret = -1;
	int bpp = 2;
	switch (mip._dFormat)
	{
		case PacARGB8888:
			bpp = 4;
		case PacARGB4444:
		case PacARGB1555:
		case PacRGB565:
		case PacAI88:
		{
			// if data is not pre-swizzled, we have to swizzle it now
			AUTO_STATIC_ARRAY_16(char,work,256*256*2);
			work.Realloc(mip._h*mip._pitch);
			work.Resize(mip._h*mip._pitch);
			ret = src->GetMipmapData(work.Data(),mip,i);
			// temp contains non-swizzled data
			XGSwizzleRect
			(
				work.Data(),0,NULL,dta,
				mip._w,mip._h,NULL,bpp
			);
			break;
		}
		default:
			// DXT and other formats are pre-swizzled
			ret = src->GetMipmapData(dta,mip,i);
			break;
	}
	return ret;
}

#endif

/*!
\patch 1.12 Date 8/07/2001 by Ondra
- Fixed: Incorrect texture source (like JPG file not loaded) crashes.
\patch 1.48 Date 3/12/2002 by Ondra
- Fixed: Incorrect texture source (like JPG file not loaded) caused shutdown.
Warning message is shown instead.
*/

int TextureD3D::LoadLevelsSys( int levelMin )
{
	TextBankD3D *bank=static_cast<TextBankD3D *>(GEngine->TextBank());
	if (levelMin>=_systemLoaded)
	{
		SysCacheUse(bank->_systemUsed);
		return 0;
	}

	int i;

	if (!_src)
	{
		RptF("No texture source for %s",Name());
		return -1;
	}
	if( _interpolate )
	{
		if (!_interpolate->_src)
		{
			RptF("No texture source for %s",_interpolate->Name());
			return -1;
		}
	}
	// open texture file
	//QIFStream in;
	//GFileServer->Open(in,Name());

	//if( in.fail() ) return -1;

	//QIFStream ipol; // interpolated texture stream
	//if( _interpolate )
	//{
	//	GFileServer->Open(ipol,_interpolate->Name());
	//
	//	if( ipol.fail() ) return -1;
	//
	//}

	// create sysmem mip-map chain

	TEXTUREDESC8 sysd;
	memset(&sysd,0,sizeof(sysd)); 
	
	PacFormat format=_mipmaps[levelMin].DstFormat();
	//GEngineDD->InitVRAMPixelFormat(sysd.ddpfPixelFormat,format,false);
	GEngineDD->InitVRAMPixelFormat(sysd.format,format,true);

	sysd.pool=D3DPOOL_SYSTEMMEM;
	sysd.nMipmaps = _nMipmaps-levelMin;

	ReleaseSystem(true);

	PacLevelMem &topMip = _mipmaps[levelMin];

	// not present -> create it and load it
	SurfaceInfoD3D systemSurf;
	sysd.w = topMip._w;
	sysd.h = topMip._h;
	bank->UseSystem(systemSurf,sysd,format);

	_sysSurface = systemSurf;

	#if _XBOX

		if (!_sysSurface.GetSurface())
		{
			__asm int 3;
		}

	#endif
	SysCacheUse(bank->_systemUsed);


	// load only mipmaps that are not already loaded
	for( i=levelMin; i<_nMipmaps; i++ )
	{ // sysmem load
		PacLevelMem &mip = _mipmaps[i];

		int aLevel = i-levelMin;
		Assert( aLevel>=0 );

		// lock surface - we will load it
		HRESULT err;
		D3DLOCKED_RECT rect;

		// note: some levels may be skipped because texture is too big
		err=_sysSurface.GetSurface()->LockRect
		(
			aLevel,&rect,NULL,0
		);

		if( err!=D3D_OK ) DDError("Cannot lock mipmap",err);
		// assume normal pitch
		// load level
		int ret;
		#if 1
			#ifdef _XBOX
				ret = LoadMipmap(rect.pBits,mip,_src,i);
			#else
				ret = _src->GetMipmapData(rect.pBits,mip,i);
			#endif

			ADD_COUNTER(ldTxP,mip._w*mip._h);
		#else
			ret = true;
		#endif

		if( _interpolate )
		{
			Assert( _interpolate->_nMipmaps==_nMipmaps );
			PacLevelMem &imip = _interpolate->_mipmaps[i];

			// perform interpolation between this and interpol using _iFactor
			#ifdef _XBOX
			AUTO_STATIC_ARRAY_16(short,mem,256*256);
			#else
			AUTO_STATIC_ARRAY(short,mem,256*256);
			#endif

			mem.Realloc(imip._w*imip._h);
			mem.Resize(imip._w*imip._h);

			#ifdef _XBOX
				ret = LoadMipmap(mem.Data(),imip,_interpolate->_src,i);
			#else
				ret = _interpolate->_src->GetMipmapData(mem.Data(),imip,i);
			#endif
			mip.Interpolate
			(
				rect.pBits,mem.Data(),
				imip,_iFactor
			);
		}
		if (!ret)
		{
			// in case of error fill mipmap with black color

			int srcPixels = mip._w*mip._h;
			switch (mip._dFormat)
			{
				case PacARGB1555:
				case PacARGB4444:
				case PacRGB565:
				{
					unsigned short *dst = (unsigned short *)rect.pBits;
					while (--srcPixels>=0) *dst++ = 0;
					break;
				}
				case PacARGB8888:
				{
					unsigned long *dst = (unsigned long *)rect.pBits;
					while (--srcPixels>=0) *dst++ = 0;
					break;
				}
			}

		}
		err=_sysSurface.GetSurface()->UnlockRect(aLevel);
		if( err!=D3D_OK ) DDError("Cannot unlock mipmap",err);
		if( !ret ) WarningMessage("Cannot load mipmap %s",Name());

		//actMipmap->GetAttachedSurface(&caps,nextMipmap.Init());
		//actMipmap=nextMipmap;
	} // if( load mipmap level)

	return 0;
}

void TextureD3D::InitDDSD( TEXTUREDESC8 &ddsd, int levelMin, bool enableDXT )
{
	memset(&ddsd,0,sizeof(ddsd)); 


	ddsd.pool = D3DPOOL_DEFAULT;
	/*
	switch (GEngineDD->GetTexLoc())
	{
		case TexLocalVidMem:
		case TexNonlocalVidMem:
		break;
		case TexSysMem:
			ddsd.pool = D3DPOOL_SYSTEMMEM;
		break;
	}
	*/

	PacFormat format=_mipmaps[levelMin].DstFormat();

	GEngineDD->InitVRAMPixelFormat(ddsd.format,format,enableDXT);

	ddsd.w = _mipmaps[levelMin]._w; 
	ddsd.h = _mipmaps[levelMin]._h; 
	ddsd.nMipmaps = _nMipmaps-levelMin;
}

static int MipmapSize( PacFormat format, int w, int h )
{
	switch (format)
	{
		case PacDXT1:
		return w*h/2;
		case PacDXT2: case PacDXT3: case PacDXT4: case PacDXT5:
		return w*h;
		case PacARGB8888:
		return w*h*4;
		default:
		return w*h*2; // assume 16-b textures
	}
	/*NOTREACHED*/
}


bool TextureD3D::CopyToVRAM( SurfaceInfoD3D &surface, int levelMin )
{
	#ifdef _XBOX
	// use system memory surface directly as video memory
	surface = _sysSurface;

	TextBankD3D *bank=static_cast<TextBankD3D *>(GEngine->TextBank());
	bank->_systemAllocated-=_sysSurface.SizeUsed();
	_sysSurface.Free(false);
	SystemReleased();

	return true;
	#else

	//TextBankD3D *bank=static_cast<TextBankD3D *>(GEngine->TextBank());

	// select best system surface
	Assert (_sysSurface.GetSurface());

	IDirect3DDevice8 *device=GEngineDD->GetDirect3DDevice();


	#ifdef _XBOX
		HRESULT err = D3D_OK;
		for (int i=0; i<surface._nMipmaps; i++)
		{
			D3DLOCKED_RECT rect;
			err = surface.GetSurface()->LockRect(i,&rect,NULL,0);
			if (err!=D3D_OK) break;
			// calculate actual size
			int size = MipmapSize(surface._format,surface._w>>i,surface._h>>i);
			memset(rect.pBits,-1,size);
			surface.GetSurface()->UnlockRect(i);
		}
	#else
		HRESULT err = device->UpdateTexture
		(
			_sysSurface.GetSurface(),surface.GetSurface()
		);
	#endif

	#if _ENABLE_REPORT || defined _XBOX
	if (err!=D3D_OK)
	{
		DDError("Copy",err);
		// check pools of both surfaces
		D3DSURFACE_DESC ssd,tsd;
		_sysSurface.GetSurface()->GetLevelDesc(0,&ssd);
		surface.GetSurface()->GetLevelDesc(0,&tsd);
		#ifndef _XBOX
		LogF("Src pool %d, Tgt pool %d",ssd.Pool,tsd.Pool);
		#endif
	}
	#endif

	return true;
	#endif	
}

void ReportGRAM( const char *name)
{
	if (GEngineDD) GEngineDD->ReportGRAM(name);
}

void ReportTextures( const char *name )
{
	EngineDD *engine = dynamic_cast<EngineDD *>(GEngine);
	if (!engine) return;
	engine->TextBankDD()->ReportTextures(name);
}



int TextureD3D::TotalSize( int levelMin ) const
{
	int totalSize=0;
	for( int i=levelMin; i<_nMipmaps; i++ )
	{
		const PacLevelMem &mip = _mipmaps[i];
		int size = MipmapSize(mip.DstFormat(),mip._w,mip._h);
		//int size=_mipmaps[i]._pitch*_mipmaps[i]._h;
		totalSize+=size;
	}
	return totalSize;
}

int TextureD3D::LoadLevels( int levelMin )
{
	// if no levels are needed, we can load nothing and still return success
	if (levelMin<0) return 0;
	TextBankD3D *bank=static_cast<TextBankD3D *>(GEngineDD->TextBank());

	int ret=0;
	//ScopeLock<AbstractTextureLoader> lock(*_bank->_loader);
	// if the texture loaded contains min..max range, let it be...

	Assert (levelMin<_nMipmaps);
	Assert (levelMin>=0);

	// assume: there is some levelCur so that for every i, i>=levelCur
	// _mipmaps[i]._memorySurf is not NULL
	if( _interpolate ) _interpolate->_inUse++;
	if( _levelLoaded>levelMin )
	{
		PacFormat format=_mipmaps[levelMin].DstFormat();

		ReleaseMemory(true); // release all old mipmap data
		//levelCur=_nMipmaps; // all levels released

		#if REPORT_ALLOC>=20
		LogF
		(
			"Load %s (%d:%d x %d)",
			(const char *)Name(),
			levelMin,_mipmaps[levelMin]._w,_mipmaps[levelMin]._h
		);
		#endif

		_inUse++;
		// some mipmaps must be added to the texture
		// create new mipmaps in system memory
		//IDirect3DDevice8 *dDraw=GEngineDD->GetDirect3DDevice();

		// create whole mipmap structure with single call in target heap

		TEXTUREDESC8 ddsd;
		InitDDSD(ddsd,levelMin,true);

		// prepare all new levels
		// create and load mipmap (per-level)

		bank->UseReleased(_surface,ddsd,format);

		if( !_surface.GetSurface() )
		{
			if( bank->_totalAllocated>bank->_limitAlocatedTextures-512*1024 )
			{
				// do not try reusing until used nearly all video memory
				bank->Reuse(_surface,ddsd,format);
			}
		}

		int totalSize=TotalSize(levelMin);
		// we reused different texture - load it
		if( !_surface.GetSurface() )
		{
			// if nothing can be reused, we have to create something new
			bank->ReserveMemory(totalSize);


			if( bank->CreateVRAMSurface(_surface,ddsd,format,totalSize)<0 )
			{
				return -1;
			}


			bank->_thisFrameAlloc++;

			ADD_COUNTER(tGRAM,totalSize);
		}

		Assert( _surface.SizeExpected()==totalSize );
		
		// all VidMem levels created
		// level 0 pointer acquired
		int ret=LoadLevelsSys(levelMin);
		if( ret )
		{
			ErrorMessage("Error loading texture");
		}

		if (!CopyToVRAM(_surface,levelMin)) ret=-1;

		_inUse--;
		// everything is loaded - lock and copy pointers

		// we load more that was used before - it is surely needed whole
		CacheUse(bank->_thisFrameWholeUsed);
		//LogF("texture %s: whole used",Name());
		_levelLoaded=levelMin;
	}
	if( _interpolate ) _interpolate->_inUse--;
	if (ret<0)
	{
		ReleaseMemory(true);
	}
	return ret;
}

void TextureD3D::ReleaseSmall( bool store, D3DSurfaceDestroyer *batch )
{
	TextBankD3D *bank=static_cast<TextBankD3D *>(GEngine->TextBank());
	if( _smallSurface.GetSurface() )
	{
		// first of all: move data to reserve bank
		if( store )
		{
			#if REPORT_ALLOC>=20
				LogF
				(
					"VID Small Add To Bank %dx%dx%s (%dB) - '%s'",
					_smallSurface._w,_smallSurface._h,FormatName(_smallSurface._format),_smallSurface.Size(),
					(const char *)Name()
				);
			#endif
			bank->AddReleased(_smallSurface);
			_smallSurface.Free(false);
		}
		else
		{
			bank->_totalAllocated-=_smallSurface.SizeUsed();
			bank->_thisFrameAlloc++;
			ADD_COUNTER(tHeap,_smallSurface.SizeUsed());
			#if REPORT_ALLOC>=10
				if (!batch || REPORT_ALLOC>=15)
				{
					LogF
					(
						"VID Small Release %dx%dx%s (%dB) - '%s', rest %d, %s",
						_smallSurface._w,_smallSurface._h,FormatName(_smallSurface._format),_smallSurface.SizeUsed(),
						(const char *)Name(),bank->_totalAllocated,
						batch ? "Batch" : ""
					);
				}
			#endif
			if (batch) batch->Add(_smallSurface);
			_smallSurface.Free(true,batch!=NULL);
		}
		_smallLoaded=_nMipmaps;
	}
}

int TextureD3D::LoadSmall()
{
	// check which level should be loaded
	// TODO: scale based on total VRAM
	// find first mipmap with max. 256 pixels
	if( _smallSurface.GetSurface() )
	{
		return 0;
	}

	//LogF("Load small %s",(const char *)Name());
	TextBankD3D *bank=static_cast<TextBankD3D *>(GEngine->TextBank());

	if( _nMipmaps<=0 ) return -1;
	int i;
	for( i=0; i<_nMipmaps; i++ )
	{
		int pixels=_mipmaps[i]._w*_mipmaps[i]._h;
		if( pixels<=bank->_maxSmallTexturePixels ) break;
	}
	int levelMin=i;
	if( levelMin>=_nMipmaps ) levelMin=_nMipmaps-1;
	// load all levelMin levels to smallSurface, scan smallHandle

	PacFormat format=_mipmaps[levelMin].DstFormat();
	TEXTUREDESC8 ddsd;
	InitDDSD(ddsd,levelMin,false);

	bank->UseReleased(_smallSurface,ddsd,format);

	int totalSize=TotalSize(levelMin);
	
	#if REPORT_ALLOC>=20
	LogF("Loading small %s %d",Name(),levelMin);
	#endif

	if( !_smallSurface.GetSurface() )
	{
		// if nothing can be reused, we have to create something new
		bank->ReserveMemory(totalSize);

		if( bank->CreateVRAMSurface(_smallSurface,ddsd,format,totalSize)<0 )
		{
			return -1;
		}
	}

	// load sysmem bitmaps
	int ret=LoadLevelsSys(levelMin);
	if( ret )
	{
		ErrorMessage("Error loading texture");
		return -1;
	}
	// we have VRAM allocated - load it
	if (CopyToVRAM(_smallSurface,levelMin))
	{
		// new mipmap pyramid builded
		_smallLoaded=levelMin;
		return 0;
	}
	return -1;
}

void TextBankD3D::Reuse
(
	SurfaceInfoD3D &surf, const TEXTUREDESC8 &ddsd, PacFormat format
)
{
	int w=ddsd.w;
	int h=ddsd.h;
	int nMipmaps=ddsd.nMipmaps;
	for
	(
		HMipCacheD3D *last=_previousUsed.Last();
		last;
		last=_previousUsed.Prev(last)
	)
	{
		// release mip-map data
		TextureD3D *texture=last->texture;
		//cannot release this one
		if( texture->_inUse ) continue;

		const SurfaceInfoD3D &surface=texture->_surface;
		if( nMipmaps!=surface._nMipmaps ) continue;
		// match size
		if( surface._w!=w ) continue;
		if( surface._h!=h ) continue;
		// match pixel format
		// if equal - this texture is perfect match
		if( surface._format!=format ) continue;
		
		// surface moved to reusable
		texture->ReuseMemory(surf);
		return;
	}
}

void TextBankD3D::ReuseSys
(
	SurfaceInfoD3D &surf, const TEXTUREDESC8 &ddsd, PacFormat format
)
{
	int w=ddsd.w;
	int h=ddsd.h;
	int nMipmaps=ddsd.nMipmaps;
	for
	(
		HSysCacheD3D *last=_systemUsed.Last();
		last;
		last=_systemUsed.Prev(last)
	)
	{
		// check mip-map data
		TextureD3D *text = last->texture;
		const SurfaceInfoD3D &surface = text->_sysSurface;
		if( nMipmaps!=surface._nMipmaps ) continue;
		// match size
		if( surface._w!=w ) continue;
		if( surface._h!=h ) continue;
		// match pixel format
		// if equal - this texture is perfect match
		if( surface._format!=format ) continue;
		
		// surface moved to reusable
		text->ReuseSystem(surf);
		return;
	}
}

int TextBankD3D::FindSurface
(
	int w, int h, int nMipmaps, PacFormat format,
	const AutoArray<SurfaceInfoD3D> &array
) const
{
	//if( format==PacAI88 && !_engine->Can88() ) format=PacARGB4444;
	int i;
	for( i=0; i<array.Size(); i++ )
	{
		const SurfaceInfoD3D &surface=array[i];
		// match mipmap structure
		if( nMipmaps!=surface._nMipmaps ) continue;
		//const PacLevelMem &pMip=pattern->_mipmaps[0];
		// match size
		if( surface._w!=w ) continue;
		if( surface._h!=h ) continue;
		// match pixel format
		if( surface._format==format )
		{
			/*
			// this texture is perfect match
			if( surface._texture==texture )
			{
				LogF("Texture %dx%d (%s) reused",w,h,(const char *)texture->Name());
			*/
			return i;
		}
		#if 0
			// we can try changing pixel format if everything else is ok
			// note: we assume here we use only 16b formats
			// note: changing pixel format is not possible in DX6
			// to change pixel format we must use explicit memory buffer
			// TODO: consider it
			#if REPORT_ALLOC>=20
				LogF("Changing pixel format of %dx%d",w,h);
			#endif
			TEXTUREDESC8 ddsd;
			ddsd.dwSize=sizeof(ddsd);
			ddsd.dwFlags=DDSD_PIXELFORMAT;
			GEngineDD->InitSysPixelFormat(ddsd.ddpfPixelFormat,format);
			HRESULT err=surface.GetSurface()->SetSurfaceDesc(&ddsd,0);
			if( err!=D3D_OK )
			{
				Log("Cannot change pixel format.");
				continue;
			}
			surface._format=format;
		#endif
	}
	return -1;
}

int TextBankD3D::DeleteLastReleased( int need, D3DSurfaceDestroyer *batch )
{
	// LRU is last
	// delete first 
	SurfaceInfoD3D &free=_freeTextures[0];
	int size=free.SizeUsed();

	_totalAllocated-=size;
	ADD_COUNTER(tHeap,size);
	_thisFrameAlloc++;

	#if REPORT_ALLOC>=10
		if (!batch || REPORT_ALLOC>=15)
		{
			LogF
			(
				"VID Released %dx%dx%s (%d B) deleted",
				free._w,free._h,FormatName(free._format),size
			);
		}
	#endif

	if (batch) batch->Add(free);
	free.Free(true,batch!=NULL);
	_freeTextures.Delete(0);

	return size;
}

int TextBankD3D::DeleteLastSystem( int need, D3DSurfaceDestroyer *batch )
{
	// LRU is last
	// delete first 
	SurfaceInfoD3D &free=_freeSysTextures[0];
	int size=free.SizeUsed();
	_systemAllocated-=size;
	#if REPORT_ALLOC>=10
		if (!batch || REPORT_ALLOC>=15)
		{
			LogF
			(
				"SYS Released %dx%dx%s (%dB) deleted",
				free._w,free._h,FormatName(free._format),size
			);
		}
	#endif
	if (batch) batch->Add(free);
	//ADD_COUNTER(syRel,1);
	free.Free(true,batch!=NULL);

	_freeSysTextures.Delete(0);
	return size;
}

void TextBankD3D::AddReleased( SurfaceInfoD3D &surf )
{
	#if REPORT_ALLOC>=40
		LogF
		(
			"Add to released %dx%dx%s (%dB)",
			surf._w,surf._h,FormatName(surf._format),surf.Size()
		);
	#endif
	#ifndef _XBOX
	_freeTextures.Add(surf);
	#endif
}

void TextBankD3D::UseReleased
(
	SurfaceInfoD3D &surf,
	const TEXTUREDESC8 &ddsd, PacFormat format
)
{
	int reuse=FindReleased(ddsd.w,ddsd.h,ddsd.nMipmaps,format);
	if( reuse<0 ) return;
	SurfaceInfoD3D &reused=_freeTextures[reuse];
	surf=reused;
	#if REPORT_ALLOC>=20
		LogF
		(
			"Released %dx%dx%s (%dB) reused.",
			reused._w,reused._h,FormatName(reused._format),reused.Size()
		);
	#endif
	reused.Free(false);
	_freeTextures.Delete(reuse);
}

int TextBankD3D::FindSystem
(
	int w, int h, int nMipmaps, PacFormat format
) const
{
	return FindSurface(w,h,nMipmaps,format,_freeSysTextures);
}

size_t MemoryFreeOnDemand(size_t size);

void TextBankD3D::UseSystem
(
	SurfaceInfoD3D &surf,
	const TEXTUREDESC8 &ddsd, PacFormat format
)
{
	int nMipmaps = ddsd.nMipmaps;
	int index=FindSystem(ddsd.w,ddsd.h,nMipmaps,format);
	if( index>=0 )
	{
		surf=_freeSysTextures[index];
		_freeSysTextures.Delete(index);
		#if REPORT_ALLOC>=30
			LogF
			(
				"Used system %dx%dx%s (%dB)",
				surf._w,surf._h,FormatName(surf._format),surf.Size()
			);
		#endif
	}
	else
	{
		/**/
		if (_systemAllocated>=_limitSystemTextures-512*1024)
		{
			// if we have plenty of system surfaces,
			// try to find any non-free surfaces that would fit
			ReuseSys(surf,ddsd,format);
			if (surf.GetSurface())
			{
				return;
			}
		}
		/**/

		TEXTUREDESC8 ssss=ddsd;
		// TODO: use any - but realloc it's surface memory
		IDirect3DDevice8 *dDraw=_engine->GetDirect3DDevice();
		int size = surf.CalculateSize(dDraw,ssss,format);
		Retry:
		ReserveSystem(size);
		// create a new surface

		//ADD_COUNTER(syCre,1);
		HRESULT err=surf.CreateSurface(dDraw,ssss,format);
		if( err!=D3D_OK )
		{
			LogF("CreateSurface - out of memory (size %d)",size);
			size_t freed = MemoryFreeOnDemand(size);
			if (freed!=0) goto Retry;
			DDError("Cannot create system memory surface",err);
		}
		else
		{
			_systemAllocated+=surf.SizeUsed();
		}
	}
}

void TextBankD3D::AddSystem( SurfaceInfoD3D &surf )
{
	// TODO: XBOX UMA texture reusing
	#ifndef _XBOX
	_freeSysTextures.Add(surf);
	#endif
	surf.Free(false);
	while( _freeSysTextures.Size()>0 && _systemAllocated>_limitSystemTextures )
	{
		DeleteLastSystem(0);
	}
}

/*!
\patch 1.03 Date 7/12/2001 by Ondra
- Improved: more robust handling of max. texture size
*/
void TextureD3D::ASetNMipmaps( int n )
{
	LoadHeadersNV();
	if (n>_nMipmaps)
	{
		ErrF("Out of range ASetNMipmaps in %s",(const char *)GetName());
		n = _nMipmaps;
	}
	_nMipmaps=n;
	PacLevelMem &mip=_mipmaps[n-1];
	saturateMax(_maxSize,mip._w);
	saturateMax(_maxSize,mip._h);
	if(_largestUsed>_nMipmaps-1) _largestUsed=_nMipmaps-1;
	if( _interpolate ) _interpolate->ASetNMipmaps(n);
}

void TextureD3D::MemoryReleased()
{
	// mark as released
	// delete from statistics

	if( _cache )
	{
		_cache->Delete();
		delete _cache;
		_cache=NULL;
	}

	_levelLoaded=_nMipmaps;
}

void TextureD3D::ReleaseMemory( bool store, D3DSurfaceDestroyer *batch )
{
	TextBankD3D *bank=static_cast<TextBankD3D *>(GEngine->TextBank());
	if( _surface.GetSurface() )
	{
		// first of all: move data to reserve bank
		if( store )
		{
			// make sure surface is restored
			// surface might be lost?
			#if REPORT_ALLOC>=20
				LogF
				(
					"VID Large Add To Bank %dx%dx%s (%dB) - '%s'",
					_surface._w,_surface._h,FormatName(_surface._format),_surface.Size(),
					(const char *)Name()
				);
			#endif
			bank->AddReleased(_surface);
		}
		else
		{
			bank->_totalAllocated-=_surface.SizeUsed();
			bank->_thisFrameAlloc++;
			ADD_COUNTER(tHeap,_surface.SizeUsed());
			#if REPORT_ALLOC>=10
				if (!batch || REPORT_ALLOC>=15)
				{
					LogF
					(
						"VID Large Release %dx%dx%s (%dB) - '%s', rest %d, %s",
						_surface._w,_surface._h,FormatName(_surface._format),_surface.SizeUsed(),
						(const char *)Name(),bank->_totalAllocated,
						batch ? "Batch" : ""
					);
				}
			#endif
			if (batch) batch->Add(_surface);
		}
		_surface.Free(!store,batch!=NULL);
		MemoryReleased();
	}
}

void TextureD3D::ReleaseSystem( bool store, D3DSurfaceDestroyer *batch )
{
	TextBankD3D *bank=static_cast<TextBankD3D *>(GEngine->TextBank());
	if( _sysSurface.GetSurface() )
	{
		if( store )
		{
			// move data to reserve bank
			#if REPORT_ALLOC>=30
				LogF
				(
					"SYS Add To Bank %dx%dx%s (%dB)",
					_sysSurface._w,_sysSurface._h,
					FormatName(_sysSurface._format),_sysSurface.Size()
				);
			#endif
			bank->AddSystem(_sysSurface);
			_sysSurface.Free(false);
		}
		else
		{
			#if REPORT_ALLOC>=15
				if (!batch || REPORT_ALLOC>=15)
				{
					LogF
					(
						"SYS Release %dx%dx%s (%dB), rest %d, %s",
						_sysSurface._w,_sysSurface._h,
						FormatName(_sysSurface._format),_sysSurface.SizeUsed(),
						bank->_systemAllocated,batch ? "Batch" : ""
					);
				}
			#endif
			bank->_systemAllocated-=_sysSurface.SizeUsed();
			//ADD_COUNTER(syRel,1);
			if (batch) batch->Add(_sysSurface);
			_sysSurface.Free(true,batch!=NULL);
		}
	}
	SystemReleased();

}

void TextureD3D::ReuseMemory( SurfaceInfoD3D &surf )
{
	// reuse immediatelly
	if( _surface.GetSurface() )
	{
		// first of all: move data to reserve bank

		surf=_surface;
		// allocated memory ammount not changed
		#if REPORT_ALLOC>=20
			LogF
			(
				"VID Directly reused %dx%dx%s (%dB) - '%s'",
				surf._w,surf._h,FormatName(surf._format),surf.Size(),Name()
			);
		#endif
		_surface.Free(false);
	}
	MemoryReleased();
}

static PacFormat DstFormat( PacFormat srcFormat, int dxt )
{
	// memory representation
	switch (srcFormat)
	{
		case PacARGB1555:
		case PacRGB565:
		case PacARGB4444:
			return srcFormat;
		case PacP8:
			return PacARGB1555;
		case PacAI88:
			if (GEngineDD->Can88()) return srcFormat;
			if (GEngineDD->Can8888()) return PacARGB8888;
			return PacARGB4444;
		case PacDXT1:
			if (GEngineDD->CanDXT(1)) return srcFormat;
			else return PacARGB1555;
		default:
			LogF("Unsupported source format %d",srcFormat);
			// TODO: fail
			return srcFormat;
	}
}

#include "debugTrap.hpp"

void TextureD3D::SetMipmapRange( int min, int max )
{
	if( min<0 ) min=0;
	if( max>_nMipmaps-1 ) max=_nMipmaps-1;
	if( min>max ) min=max;
	// disable mipmaps before min and after max
	_largestUsed = min;
	_nMipmaps = max+1;
}

/*!
\patch_internal 1.05 Date 7/16/2001 by Ondra
- Fixed: Older alpha textures without alpha taggs were not recognized,
resulting in bad renderstates while drawing. Alpha artifacts were visible.
- bug introduced while implementing JPG support.
*/

int TextureD3D::Init( const char *name )
{
	SetName(name);

	_maxSize=0x10000; // no texture size limit

	// quick check if source does exist
	ITextureSourceFactory *factory = SelectTextureSourceFactory(name);
	if (!factory || !factory->Check(name))
	{
		WarningMessage("Cannot load texture %s.",(const char *)GetName());
		_nMipmaps=0;
		return -1;
	}

	//DoInit();
	return 0;
}

void TextureD3D::PreloadHeaders()
{
	ITextureSourceFactory *factory = SelectTextureSourceFactory(Name());
	factory->PreInit(Name());
}

/*!
\patch_ 1.50 Date 4/15/2002 by Ondra
- Optimized: Texture loading is now postponed to "Get ready" screen.
*/

void TextureD3D::DoLoadHeaders()
{
	Assert (!_initialized);
	_initialized = true;

	int i=-1;
	
	PacFormat format=BasicFormat(GetName());

	bool isPaa = ( format==PacARGB4444 );

	TextBankD3D *bank=static_cast<TextBankD3D *>(GEngine->TextBank());


	// if max. size is not set yet, autodetect it
	if (_maxSize>=0x10000)
	{
		if( !CmpStartStr(Name(),"fonts\\") ) _maxSize = 1024;
		else if( !CmpStartStr(Name(),"merged\\") ) _maxSize = 2048;
		else if( bank->AnimatedNumber(Name())>=0 && IsAlpha() ) _maxSize = Glob.config.maxAnimText;
		else _maxSize=Glob.config.maxObjText;
	}

	ITextureSourceFactory *factory = SelectTextureSourceFactory(Name());
	_src = factory->Create(Name(),_mipmaps,MAX_MIPMAPS);

	if (_src)
	{

		//in.seekg(0,QIOS::beg);
		// .paa should start with format marker

		#if REPORT_ALLOC>=20
		LogF("Init texture %s",(const char *)Name());
		#endif
		format = _src->GetFormat();

		if
		(
			format==PacARGB4444 ||
			format==PacAI88 ||
			format==PacARGB8888
		)
		{
			_src->ForceAlpha();
		}


		int dxt = GEngineDD->DXTSupport();
		PacFormat dFormat = DstFormat(format,dxt);

		if( !_src->IsTransparent() && _src->GetFormat()==PacARGB1555 )
		{
			if( bank->_engine->Can565() && !isPaa ) dFormat=PacRGB565;
		}

		_largestUsed = MAX_MIPMAPS;
		int nMipmaps = _src->GetMipmapCount();
		for( i=0; i<nMipmaps; i++ )
		{
			PacLevelMem &mip=_mipmaps[i];

			mip.SetDestFormat(dFormat,8);

			if (!mip.TooLarge(_maxSize))
			{
				if (_largestUsed>i) _largestUsed=i;
			}

			// do not load too small mip-maps
			if( mip._w<MIN_MIP_SIZE ) break;
			if( mip._h<MIN_MIP_SIZE ) break;


		}

		_nMipmaps=i;
		_levelLoaded=i;
		_smallLoaded=i;
		_levelNeededThisFrame=_levelNeededLastFrame=i; // no level loaded, no needed

		return;
	}	
	
	WarningMessage("Cannot load texture %s.",(const char *)GetName());
	_nMipmaps=0;
	return;
}

void TextureD3D::LoadHeaders()
{
	if (_initialized) return;
	DoLoadHeaders();
}

Color TextureD3D::GetPixel( int level, float u, float v ) const
{
	LoadHeadersNV();

	Log("GetPixel %s (%d:%f,%f)",(const char *)Name(),level,u,v);
	//ScopeLock<AbstractTextureLoader> lock(*_bank->_loader);
	// if the texture loaded contains min..max range, let it be...

	// assume: there is some levelCur so that for every i, i>=levelCur
	// _mipmaps[i]._memorySurf is not NULL
	
	// open texture file
	QIFStream in;
	GFileServer->Open(in,Name());

	if( in.fail() ) return HWhite;

	Color icol;
	QIFStream ipol; // interpolated texture stream
	if( _interpolate )
	{
		GFileServer->Open(ipol,_interpolate->Name());

		if( ipol.fail() ) return HWhite;
	}

	PacLevelMem mip=_mipmaps[level];

	AUTO_STATIC_ARRAY(char,mem,256*256*2);
	mem.Realloc(mip._pitch*mip._h);
	mem.Resize(mip._pitch*mip._h);

	// load level
	//_mipmaps[i]._format=format;
	//mip._memData=NULL; // forget any old memData references
	_src->GetMipmapData(mem.Data(),mip,level);
	//Assert( mip._memData ); // forget any old memData references
	//if( mip._format==PacAI88 ) mip.ChangeFormat(PacARGB4444,mip._format);

	if (_interpolate)
	{
		// perform interpolation between this and interpol using _iFactor
		AUTO_STATIC_ARRAY(char,imem,256*256*2);

		PacLevelMem &imip = _interpolate->_mipmaps[level];

		imem.Realloc(imip._pitch*imip._h);
		imem.Resize(imip._pitch*imip._h);
		//imip._memData=NULL; // forget any old memData references
		_interpolate->_src->GetMipmapData(imem.Data(),imip,level);
		//Assert( imip._memData ); // forget any old memData references
		icol = imip.GetPixel(imem.Data(),u,v);
	}	
	Color col=mip.GetPixel(mem.Data(),u,v);
	if (_interpolate)
	{
		col = col*(1-_iFactor)+icol*_iFactor;
	}
	//mip._memData=NULL; // mem will be freed
	return col;
}

DEFINE_FAST_ALLOCATOR(TextureD3D);

TextureD3D::TextureD3D()
:_nMipmaps(0),
_levelLoaded(63),
_smallLoaded(63),
_systemLoaded(63),
_levelNeededThisFrame(0),_levelNeededLastFrame(0), // no level loaded, all needed
_isDetail(false),_useDetail(false),
_cache(NULL),_inUse(0),
_sysCache(NULL),
_interpolate(NULL),
_maxSize(256),
_initialized(false)
{
}

TextureD3D::~TextureD3D()
{
	bool store = !IsOutOfMemory();
	ReleaseMemory(store);
	ReleaseSmall(store);
	ReleaseSystem(store);
}

void TextureD3D::SetMaxSize( int size )
{
	if (_initialized)
	{
		// limit size by real texture size
		int maxSize = _mipmaps[0]._w;
		saturateMax(maxSize,_mipmaps[0]._h);
		saturateMin(size,maxSize);
		// set max. size
		_maxSize=size;
		// scan for correspondig mipmap level
		int nMipmaps = _src->GetMipmapCount();
		if (nMipmaps<=0) return;
		_largestUsed = MAX_MIPMAPS;
		for (int i=0; i<nMipmaps; i++)
		{
			const PacLevelMem &mip=_mipmaps[i];

			if (!mip.TooLarge(_maxSize))
			{
				if (_largestUsed>i) _largestUsed=i;
			}

			// do not load too small mip-maps
			if( mip._w<MIN_MIP_SIZE ) break;
			if( mip._h<MIN_MIP_SIZE ) break;
		}
		if(_largestUsed>_nMipmaps-1) _largestUsed=_nMipmaps-1;
	}
	else
	{
		_maxSize = size;
	}
}

void TextureD3D::SetMultitexturing( int type )
{
	if( type==1 )
	{
		_useDetail=true;
	}
}

D3DSurfaceDestroyer::D3DSurfaceDestroyer()
{
}

D3DSurfaceDestroyer::~D3DSurfaceDestroyer()
{
	DestroyAll();
}


static int CmpSurfById( const D3DSurfaceToDestroy *s1, const D3DSurfaceToDestroy *s2 )
{
	// move small ID to the end
	return s2->_id-s1->_id;
}

void D3DSurfaceDestroyer::Add( const SurfaceInfoD3D &surf )
{
	if (surf.GetSurface())
	{
		D3DSurfaceToDestroy destroy;
		destroy._surface = surf.GetSurface();
		destroy._id = surf.GetCreationID();
		_surfaces.Add(destroy);
	}
}

void D3DSurfaceDestroyer::DestroyAll()
{
	QSort(_surfaces.Data(),_surfaces.Size(),CmpSurfById);
	#if REPORT_ALLOC>=10
	for (int i=0; i<_surfaces.Size(); i++)
	{
		D3DSurfaceToDestroy &surf = _surfaces[i];
		LogF("  destroy %x, id %d",surf._surface,surf._id);
	}
	#endif

	_surfaces.Clear();
}


int SurfaceInfoD3D::CalculateSize
(
	IDirect3DDevice8 *dDraw, const TEXTUREDESC8 &desc, PacFormat format,
	int totalSize
)
{
	if (totalSize>0) return totalSize;
	int nMipmaps = desc.nMipmaps;
	int cTotalSize=0;
	int size=MipmapSize(format,desc.w,desc.h);
	for( int level=nMipmaps; --level>=0; )
	{
		cTotalSize+=size;
		size>>=2;
	}
	return cTotalSize;
}

int SurfaceInfoD3D::_nextId;

HRESULT SurfaceInfoD3D::CreateSurface
(
	IDirect3DDevice8 *dDraw, const TEXTUREDESC8 &desc, PacFormat format, int totalSize
)
{
	if (_surface)
	{
		Fail("Non-free surface created.");
	}

	// TODO: check Can88
	_w = desc.w;
	_h = desc.h;
	_nMipmaps = desc.nMipmaps;
	{
		int cTotalSize=0;
		int size=MipmapSize(format,_w,_h); // assume 16-b textures
		for( int level=_nMipmaps; --level>=0; )
		{
			cTotalSize+=size;
			size>>=2;
		}
		if( totalSize<0 ) totalSize=cTotalSize;
		Assert( totalSize==cTotalSize );
	}
	_format=format;
	_totalSize=totalSize;
	_usedSize=AlignVRAM(totalSize);

	#define LOG_WS 0

	#if LOG_WS
		SIZE_T memBefore = GetMemoryUsedSize();
	#endif
	HRESULT err=dDraw->CreateTexture
	(
		_w,_h,_nMipmaps,0,
		desc.format,desc.pool,
		_surface.Init()
	);
	#if LOG_WS
		if (desc.pool==D3DPOOL_DEFAULT)
		{
			SIZE_T memAfter = GetMemoryUsedSize();
			LogF
			(
				"WS change %d, size %d, format %d, levels %d",
				(int)(memAfter-memBefore),totalSize,format,
				_nMipmaps
			);
			static int sumChange=0;
			static int sumLevels=0;
			sumChange += memAfter-memBefore;
			sumLevels += _nMipmaps;
			LogF("Avg per mipmap %d",sumChange/sumLevels);
		}
	#endif
	if (err!=D3D_OK)
	{
		LogF("Dimensions %d:%d,%d",_w,_h,_nMipmaps);
		DDError("Create Surface Error",err);
	}
	else
	{	
		D3DSURFACE_DESC desc;
		int totSize = 0;
		// check real surface size
		for (int i=0; i<_nMipmaps; i++)
		{
			int level = i;
			_surface->GetLevelDesc(level,&desc);
			totSize += desc.Size;
		}
		_usedSize = AlignVRAM(totSize);
		_id = _nextId++;
	}
	#if REPORT_ALLOC>=10
		if( err==D3D_OK )
		{
			const char *mem="VDA";
			if( desc.ddsCaps.dwCaps&DDSCAPS_SYSTEMMEMORY ) mem="SYS";
			else if( desc.ddsCaps.dwCaps&DDSCAPS_NONLOCALVIDMEM ) mem="AGP";
			else if( desc.ddsCaps.dwCaps&DDSCAPS_LOCALVIDMEM ) mem="VID";
			LogF("%s: Created %dx%dx%s (%dB)",mem,_w,_h,FormatName(format),SizeUsed());
		}
	#endif
	return err;
}

void SurfaceInfoD3D::Free(bool lastRef, int refValue)
{
	IDirect3DTexture8 *surf = _surface;
	//GEngineDD->TextBankDD()->ReportTextures("* Free [");
	int result = _surface.Free();
	if (lastRef)
	{
		#if REPORT_ALLOC>=10
		if (surf && refValue==0)
		{
			LogF("  destroy %x, id %d, size %d",surf,_id,_usedSize);
		}
		#endif
		#if 1 //ndef _XBOX
		if (result!=refValue)
		{
			RptF("Surf: Some references to %x left (%d!=%d)",surf,result,refValue);
		}
		#endif
	}
	//GEngineDD->TextBankDD()->ReportTextures("* Free ]");
	_w=0;
	_h=0;
	_nMipmaps=0;
	_totalSize=0;
	_usedSize = 0;
}

int TextBankD3D::CreateVRAMSurface
(
	SurfaceInfoD3D &surface,
	const TEXTUREDESC8 &desc, PacFormat format, int totalSize
)
{
	// VRAM creation - mark it
	TEXTUREDESC8 ddsd=desc;

	IDirect3DDevice8 *dDraw=_engine->GetDirect3DDevice();

	bool retry = false;
	Retry:
	//ReportTextures("* Create [");

	HRESULT err=surface.CreateSurface(dDraw,ddsd,format,totalSize);

	//ReportTextures("* Create ]");
	if (retry)
	{
		if (err!=D3D_OK)
		{
			DDError("Create Surface Error",err);
		}
		else
		{
			LogF("Retry OK");
		}
	}

	if( err==E_OUTOFMEMORY || err==D3DERR_OUTOFVIDEOMEMORY )
	{
		Log("Out of video memory.");
		int rest = _freeTextureMemory-_reserveTextureMemory;
		if (rest>=totalSize)
		{
			// it seems we need more texture memory than expected
			_reserveTextureMemory+=rest-totalSize;
		}
		if( ForcedReserveMemory(totalSize) )
		{
			retry = true;
			goto Retry;
		}
		LogF("VID: No space left %d",totalSize);
		return -1; // out of memory
	}
	if( err ) DDError("Cannot create video memory mipmap.",err);
	Assert( err==D3D_OK );
	_totalAllocated += surface.SizeUsed();
	return 0;
}

bool TextureD3D::VerifyChecksum( const MipInfo &absMip ) const
{
	return true;
}

// constructor

TextBankD3D::TextBankD3D( EngineDD *engine )
:_totalAllocated(0),_systemAllocated(0)
{
	//const int mem2MB=;
	//_limitAlocatedTextures=mem2MB; // this should be good estimation on any card
	//_limitSystemTextures=( Glob.config.heapSize*(1024*1024) );
	//_limitSystemTextures=2*1024*1024;
	_limitSystemTextures=1024*1024;
	_reserveTextureMemory = 0;
	_engine=engine;

	// will set _limitAlocatedTextures and _freeTextureMemory
	CheckTextureMemory();

	_maxTextureMemory = _freeTextureMemory;

	// set small texture limit based on free memory
	// assume there will be about 1000 loaded simultaneosly
	// we want to use about 1/8 of texture memory for small textures
	// assume 16-bit textures

	int limitPixels = _limitAlocatedTextures/(2*1024*8);
	int limitPixelsPow2 = 1;
	while (limitPixelsPow2+limitPixelsPow2<limitPixels)
	{
		limitPixelsPow2+=limitPixelsPow2;
	}

	_maxSmallTexturePixels = limitPixelsPow2;
	saturateMax(_maxSmallTexturePixels,4*4);
	LogF
	(
		"Max small texture pixels: %d, %.0f",
		_maxSmallTexturePixels,
		sqrt(_maxSmallTexturePixels)
	);

	//PreCreateVRAM(512*1024,true);
}

TextBankD3D::~TextBankD3D()
{
	UnlockAllTextures();
	DeleteAllAnimated();
	_texture.Compact();
	Assert (_texture.Size()==0);
	_texture.Clear();
	_white.Free();
	_detail.Free();
	_waterBump.Free();
	_specular.Free();
	_grass.Free();

	if (!IsOutOfMemory())
	{
		// use batch destroyed to achive fast texture destruction
		// by destroying in reverse order to creation order
		// this is possible only when we have enough memory to store the batch
		D3DSurfaceDestroyer batch;
		for( int i=0; i<_freeTextures.Size(); i++ )
		{
			batch.Add(_freeTextures[i]);
		}
		for( int i=0; i<_freeSysTextures.Size(); i++ )
		{
			batch.Add(_freeSysTextures[i]);
		}
		_freeTextures.Clear();
		_freeSysTextures.Clear();
		batch.DestroyAll();
	}
	else
	{
		_freeTextures.Clear();
		_freeSysTextures.Clear();
	}
}

void TextBankD3D::Compact()
{
	_texture.Compact();
}

void TextBankD3D::StopAll()
{
	//_loader->DeleteAll();
}

static void SurfaceName( char *buf, const SurfaceInfoD3D &surface )
{
	const char *name="???";
	switch( surface._format )
	{
		case PacP8: name="PacP8";break;
		case PacAI88: name="PacAI88";break;
		case PacRGB565: name="PacRGB565";break;
		case PacARGB1555: name="PacARGB1555";break;
		case PacARGB4444: name="PacARGB4444";break;
		case PacARGB8888: name="PacARGB8888";break;
		case PacDXT1: name="PacDXT1";break;
	}
	sprintf(buf,"\t%s,%d,%d,",name,surface._w,surface._h);
}

#include "statistics.hpp"

bool TextBankD3D::VerifyChecksums()
{
	StatisticsByName stats;
	// print released textures statistics
	for( int i=0; i<_freeTextures.Size(); i++ )
	{
		const SurfaceInfoD3D &surface=_freeTextures[i];
		char name[80];
		SurfaceName(name,surface);
		stats.Count(name);
	}
	LogF("Unused texture surfaces");
	stats.Report();
	stats.Clear();

	// print used textures statistics
	LogF("Used texture surfaces");
	for( int i=0; i<_texture.Size(); i++ )
	{
		TextureD3D *texture=_texture[i];
		if( !texture ) continue;
		if( texture->_smallSurface.GetSurface() )
		{
			char name[80];
			SurfaceName(name,texture->_smallSurface);
			stats.Count(name);
		}
		if( texture->_surface.GetSurface() )
		{
			char name[80];
			SurfaceName(name,texture->_surface);
			stats.Count(name);
		}
		
	}
	stats.Report();
	stats.Clear();

	/*
	int i;
	for( i=0; i<_texture.Size(); i++ ) 
	{
		Assert( _texture[i] );
		if( !_texture[i]->VerifyChecksum(NULL) )
		{
			Log("Texture %s checksum failed",_texture[i]->Name());
			return false;
		}
	}
	*/
	return true;
}


int TextBankD3D::Find( RStringB name1, TextureD3D *interpolate )
{
	int i;
	//RString cName(name1);
	for( i=0; i<_texture.Size(); i++ )
	{
		TextureD3D *texture=_texture[i];
		if( texture )
		{
			if( name1!=texture->GetName() ) continue;
			if( texture->_interpolate!=interpolate ) continue;
			return i;
		}
	}
	return -1;
}

int TextBankD3D::FindFree()
{
	int i;
	for( i=0; i<_texture.Size(); i++ )
	{
		TextureD3D *texture=_texture[i];
		if( !texture ) return i;
	}
	return _texture.Size();
}

TextureD3D *TextBankD3D::Copy( int from )
{
	if (from<0) return NULL;
	const TextureD3D *source=_texture[from];
	if (!source) return NULL;
	const char *sName=source->Name();

	int iFree=FindFree();
	Assert( iFree>=0 );
	TextureD3D *texture=new TextureD3D;
	
	if( texture->Init(sName) )
	{
		//texture->Init("data\\default.pac");
		return NULL;
	}
	//texture->InitLoaded();
	_texture.Access(iFree);
	_texture[iFree]=texture;
	return texture;	
}

Ref<Texture> TextBankD3D::Load( RStringB name )
{
	#if 0
	return NULL;
	#else

	int i=Find(name);
	if( i>=0 ) return _texture[i].GetLink();
	int iFree=_texture.Size();

	// check if we have file to load from
	if (!QIFStreamB::FileExist(name))
	{
		LogF("Cannot load texture %s",(const char *)name);
		return NULL;
	}
	
	Ref<TextureD3D> texture=new TextureD3D;
	if( !texture ) return NULL;

	
	if (texture->Init(name))
	{
		return NULL;
	}

	_texture.Access(iFree);
	TextureD3D *txt=texture;
	_texture[iFree]=txt;
	
	return txt;
	#endif
}

Ref<Texture> TextBankD3D::LoadInterpolated( RStringB n1, RStringB n2, float factor )
{
	#if 0

	if( factor>=0.5 ) return Load(n2);
	else return Load(n1);

	#else
	// to do: real interpolation

	const float eps=1.0/256;
	if( factor>=1.0-eps ) return Load(n2);
	if( factor<=eps ) return Load(n1);
	// create name of combination

	Ref<Texture> txt2=Load(n2);
	TextureD3D *interpolate=static_cast<TextureD3D *>(txt2.GetRef());
	int index=Find(n1,interpolate);
	if( index>=0 )
	{
		TextureD3D *t=_texture[index];
		const float iPolEps=1.0/64;
		if( fabs(t->_iFactor-factor)>iPolEps )
		{
			//Log("Reinterpolate %s %s (%f)",lowName1,lowName2,factor);
			// force reinterpolation
			t->ReleaseMemory(true);
			t->ReleaseSmall(true);
			t->ReleaseSystem(true);
			//t->ReleaseGRAM();
			t->_iFactor=factor;
		}
		return t;
	}
	// load both components
	Ref<Texture> temp=Load(n1);
	int index1=Find(n1);
	// search both components
	// create an interpolated texture
	Ref<TextureD3D> t=Copy(index1);
	if (t)
	{
		t->_interpolate=interpolate;
		t->_iFactor=factor; // start as clear texture
	}
	return t.GetRef();
	#endif
}

// maintain texture cache

void TextBankD3D::ReleaseAllTextures(bool releaseSysMem)
{
	LogF("Allocated before ReleaseAllTextures %d",_totalAllocated);
	ReserveMemory(_previousUsed,0);
	ReserveMemory(_lastFramePartialUsed,0);
	ReserveMemory(_lastFrameWholeUsed,0);
	ReserveMemory(_thisFramePartialUsed,0);
	ReserveMemory(_thisFrameWholeUsed,0);
	LogF("Allocated after ReleaseAllTextures %d",_totalAllocated);
	#if !_RELEASE
	if (_totalAllocated>0)
	{
		// scan what is allocated
		for (int i=0; i<_texture.Size(); i++)
		{
			TextureD3D *texture = _texture[i];
			if (texture->GetBigSurface()) Fail((const char *)texture->Name());
			if (texture->GetSmallSurface()) Fail((const char *)texture->Name());
		}
		if( _freeTextures.Size()>0 )
		{
			Fail("FreeTextures");
		}
	}
	#endif
	if (releaseSysMem)
	{
		ReserveSystem(_systemUsed,0);
		if (_freeSysTextures.Size()>0)
		{
			Fail("Some free sys texture");
			_freeSysTextures.Clear();
		}
		if (_systemAllocated>0)
		{
			LogF("_systemAllocated %d",_systemAllocated);
		}

	}
}

bool TextBankD3D::ReserveMemory( D3DMipCacheRoot &root, int limit )
{
	#define LOG_TIME 0
	#if LOG_TIME
	DWORD start = GlobalTickCount();
	#endif

	D3DSurfaceDestroyer batchDestroy;
	bool someReleased=false;
	while( _freeTextures.Size()>0 && _totalAllocated>limit )
	{
		// free more than one at once
		int need=_totalAllocated-limit;
		DeleteLastReleased(need,&batchDestroy);
		someReleased=true;
	}
	// release as many mip-map data as neccessary to get required memory
	// get last mip-map used
	HMipCacheD3D *last=root.Last();
	while( _totalAllocated>limit )
	{
		if( !last ) break;

		// release texture data
		TextureD3D *texture=last->texture;
		if( texture->_inUse )
		{
			last=root.Prev(last);
			continue;
		}

		// release texture data from texture heap
		Assert( texture->_cache==last );
		
		someReleased=true;
		texture->ReleaseMemory(false,&batchDestroy);
		last=root.Last();
	}
	if (someReleased)
	{
		batchDestroy.DestroyAll();
		#if LOG_TIME
		DWORD end = GlobalTickCount();
		LogF("Time to release: %d",end-start);
		#endif
	}
	else
	{
		//LogF("Nothing released: total %d, limit %d",_totalAllocated,limit);
		//LogF("  _freeTextures.Size() %d",_freeTextures.Size());
		// check if all textures are in use
	}
	return someReleased;
}

void TextBankD3D::ReportTextures( const char *name)
{
	#if _ENABLE_PERFLOG
	// report all textures loaded into VRAM and sysRAM
	LogFile file;
	if (name && strchr(name,'.') && !strchr(name,'*') )
	{
		file.Open(name);
	}
	// report some global information
	{

		#ifndef _XBOX
			IDirect3DDevice8 *dDraw=_engine->GetDirect3DDevice();
			UINT dwFree = dDraw->GetAvailableTextureMem();
		#else
			UINT dwFree = 0;
		#endif

		file << " Max " << _maxTextureMemory << " Free " << (int)dwFree << "\n";
		file << "Allocated " << _totalAllocated << "\n";

		int overhead = _maxTextureMemory - dwFree - _totalAllocated;
		file << "Overhead " << overhead << "\n";
		LogF
		(
			"%s: Free %d, Overhead %d, Reserved %d",
			name,dwFree,overhead,_reserveTextureMemory
		);
		if (overhead>4000000)
		{
			// something wrong happened - very big memory overhead
			static bool once=true;
			if (once)
			{
				LogF("#### Overhead #####");
				once=false;
			}
			__asm nop;
		}
	}


	if (file.IsOpen())
	{
		int usedInSmallTextures = 0;
		int usedInBigTextures = 0;
		int usedInFreeTextures = 0;
		int usedInSysTextures = 0;
		for (int i=0; i<_texture.Size(); i++)
		{
			TextureD3D *texture = _texture[i];
			if (!texture) continue;
			if (texture->_smallSurface.GetSurface() )
			{
				usedInSmallTextures += texture->_smallSurface.SizeUsed();
			}
			if (texture->_surface.GetSurface() )
			{
				usedInBigTextures += texture->_surface.SizeUsed();
			}
			if (texture->_sysSurface.GetSurface())
			{
				usedInSysTextures+= texture->_sysSurface.SizeUsed();
			}
		}
		for (int i=0; i<_freeTextures.Size(); i++)
		{
			SurfaceInfoD3D &free = _freeTextures[i];
			if (free.GetSurface()) usedInFreeTextures+=free.SizeUsed();
		}
		int usedInTextures = usedInSmallTextures + usedInBigTextures + usedInFreeTextures;
		file << "Used in small textures: " << usedInSmallTextures << "\n";
		file << "Used in big   textures: " << usedInBigTextures   << "\n";
		file << "Used in free  textures: " << usedInFreeTextures  << "\n";
		file << "Used (total)          : " << usedInTextures      << "\n";
		file << "Used in sys   textures: " << usedInSysTextures  << "\n";

		for (int i=0; i<_texture.Size(); i++)
		{
			TextureD3D *texture = _texture[i];
			if (!texture) continue;
			file.PrintF("%-32s",texture->Name());
			file.PrintF(" mips: %d",texture->NMipmaps());
			if (texture->_smallLoaded<texture->_nMipmaps )
			{
				file.PrintF(" small: %d",texture->_smallLoaded);
			}
			if (texture->_levelLoaded<texture->_nMipmaps )
			{
				file.PrintF(" large: %d",texture->_levelLoaded);
			}
			file << "\n";
		}
	}
	#endif
}

bool TextBankD3D::ReserveMemory( int size )
{
	if (size>=INT_MAX)
	{
		// all memory will be released - reset and start again
		_reserveTextureMemory = 0;
	}
	// check size
	// never free this&last frame textures
	return ReserveMemory(_previousUsed,_limitAlocatedTextures-AlignVRAM(size));
}

// assume driver will align RAM to some reasonable number
// 256 is experimental value for GeForce
// use GetAvailableVidMem after allocating very small texture to check this
// consider: this could be done run-time

bool TextBankD3D::ForcedReserveMemory( int size )
{
	const int minRelease=4*1024;
	if( size<minRelease ) size=minRelease; // force some minimal release
	int newLimit=_totalAllocated-AlignVRAM(size);

	LogF
	(
		"ForcedReserveMemory newLimit=%d _totalAllocated=%d",
		newLimit,_totalAllocated
	);

	bool ret = ReserveMemory(_previousUsed,newLimit);

	if (!ret)
	{
		//LogF("Evicting last frame partially used texture");
		ret = ReserveMemory(_lastFramePartialUsed,newLimit);
		if (!ret) 
		{
			//LogF("Evicting last frame whole used texture");
			ret = ReserveMemory(_lastFrameWholeUsed,newLimit);

			if (!ret) 
			{
				// note: we should perform some dropdown
				// all data allocated is required
				// we have to adjust drop down to reflect this
				Glob.fullDropDownChange=0.1;

				LogF("Evicting this frame partially used texture");
				ret = ReserveMemory(_thisFramePartialUsed,newLimit);

				if (!ret) 
				{
					LogF("Evicting this frame whole used texture");
					ret = ReserveMemory(_thisFrameWholeUsed,newLimit);
				}
			}
		}
	}
	CheckTextureMemory();

	LogF
	(
		"  done %d _totalAllocated=%d",ret,_totalAllocated
	);

	#if REPORT_ALLOC>=10
	//int overhead=dwTotal-_limitAlocatedTextures;
	DDSCAPS2 ddsCaps;
	ddsCaps.dwCaps2 = 0; 
	ddsCaps.dwCaps3 = 0; 
	ddsCaps.dwCaps4 = 0; 

	ddsCaps.dwCaps=DDSCAPS_TEXTURE|DDSCAPS_LOCALVIDMEM|DDSCAPS_VIDEOMEMORY;

	IDirect3DDevice8 *dDraw=_engine->GetDirect3DDevice();
	DWORD dwTotal,dwFree;
	dDraw->GetAvailableVidMem(&ddsCaps, &dwTotal, &dwFree);
	LogF
	(
		"VID Forced %d: Total %d, free %d, allocated %d, limit %d",
		size,dwTotal,dwFree,_totalAllocated,_limitAlocatedTextures
	);
	#endif

	return ret;
}

bool TextBankD3D::ReserveSystem( D3DMipCacheRoot &root, int limit )
{
	// release as many mip-map data as neccessary to get required memory
	D3DSurfaceDestroyer batchDestroy;
	D3DSurfaceDestroyer *batch = IsOutOfMemory() ? NULL : &batchDestroy;

	bool someReleased=false;
	#if REPORT_ALLOC>=10
		if (_systemAllocated>limit)
		{
			LogF("Allocated %d, limit %d",_systemAllocated,limit);
		}
	#endif
	while( _freeSysTextures.Size()>0 && _systemAllocated>limit )
	{
		// free more than one at once
		int need=_systemAllocated-limit;
		DeleteLastSystem(need,batch);
		someReleased=true;
	}
	// get last mip-map used
	HSysCacheD3D *last=root.Last();
	while( _systemAllocated>limit )
	{
		if( !last ) break;

		// release texture data
		TextureD3D *text = last->texture;

		// release texture data from texture heap
		Assert( text->_sysCache==last );
		
		someReleased=true;
		text->ReleaseSystem(false,batch);
		last=root.Last();
	}
	return someReleased;
}

bool TextBankD3D::ReserveSystem( int size )
{
	return ReserveSystem(_systemUsed,_limitSystemTextures-size);
}

bool TextBankD3D::ForcedReserveSystem( int size )
{
	const int minRelease=4*1024;
	if( size<minRelease ) size=minRelease; // force some minimal release
	return ReserveSystem(_systemUsed,_systemAllocated-size);
}

void TextureD3D::SystemReleased()
{
	if( _sysCache )
	{
		_sysCache->Delete();
		delete _sysCache;
		_sysCache=NULL;
	}
}

void TextureD3D::ReuseSystem( SurfaceInfoD3D &surf )
{
	// reuse immediatelly
	if( _sysSurface.GetSurface() )
	{
		// first of all: move data to reserve bank

		surf=_sysSurface;
		// allocated memory ammount not changed
		#if REPORT_ALLOC>=20
			LogF
			(
				"SYS Directly reused %dx%dx%s (%dB)",
				surf._w,surf._h,FormatName(surf._format),surf.Size()
			);
		#endif
		_sysSurface.Free(false);
		if( _sysCache )
		{
			_sysCache->Delete();
			delete _sysCache;
			_sysCache=NULL;
		}
	}
}

void TextBankD3D::StartFrame()
{
	InitDetailTextures();
	//_usedGRAM=0;

	
	/*
	for
	(
		HMipCacheD3D *last=_previousUsed.Last();
		last;
		last=_previousUsed.Prev(last)
	)
	for (
	_previousUsed.Move(_lastFrameWholeUsed);
	*/

	CheckTextureMemory();
	_thisFrameCopied=0; // blit/copied textures (in pixels)
	_thisFrameAlloc=0; // VRAM allocations/deallocations (count)

#if _ENABLE_CHEATS
	if (GInput.GetCheat2ToDo(DIK_E))
	{
		ReportTextures("textures.txt");
	}
#endif
}

void TextBankD3D::FinishFrame()
{
	//LogF("******* FinishFrame");
	// texture that were only partialy used may be released
	// fully used textures must be kept
	// make history one frame older

	// make last and this frame textures history one frame older

	// verify previous textures are marked as unused
	// all this-frame textures must be in either 
	// _thisFramePartialUsed or _thisFrameWholeUsed

	for( HMipCacheD3D *tc=_thisFramePartialUsed.Last(); tc; tc=_thisFramePartialUsed.Prev(tc) )
	{
		TextureD3D *tex = tc->texture;
		tex->_levelNeededLastFrame = tex->_levelNeededThisFrame;
		tex->_levelNeededThisFrame = tex->_nMipmaps;
		tex->ResetMipmap();
	}
	for( HMipCacheD3D *tc=_thisFrameWholeUsed.Last(); tc; tc=_thisFrameWholeUsed.Prev(tc) )
	{
		TextureD3D *tex = tc->texture;
		tex->_levelNeededLastFrame = tex->_levelNeededThisFrame;
		tex->_levelNeededThisFrame = tex->_nMipmaps;
		tex->ResetMipmap();
	}
	// this will move to partial
	for( HMipCacheD3D *tc=_lastFramePartialUsed.Last(); tc; tc=_lastFramePartialUsed.Prev(tc) )
	{
		TextureD3D *tex = tc->texture;

		tex->_levelNeededLastFrame = tex->_levelNeededThisFrame;
		tex->_levelNeededThisFrame = tex->_nMipmaps;

		Assert (tex->_levelNeededThisFrame=tex->_nMipmaps);
		Assert (tex->_levelNeededLastFrame=tex->_nMipmaps);

		tex->ResetMipmap();
	}
	for( HMipCacheD3D *tc=_lastFrameWholeUsed.Last(); tc; tc=_lastFrameWholeUsed.Prev(tc) )
	{
		TextureD3D *tex = tc->texture;

		tex->_levelNeededLastFrame = tex->_levelNeededThisFrame;
		tex->_levelNeededThisFrame = tex->_nMipmaps;

		Assert (tex->_levelNeededThisFrame=tex->_nMipmaps);
		Assert (tex->_levelNeededLastFrame=tex->_nMipmaps);

		tex->ResetMipmap();
	}

	/*
	for( int i=0; i<_texture.Size(); i++ )
	{
		TextureD3D *texture=_texture[i];
		if( texture )
		{
			// mark all levels as not needed
			texture->_levelNeededLastFrame=texture->_levelNeededThisFrame;
			texture->_levelNeededThisFrame=texture->_nMipmaps;
			texture->ResetMipmap();
		}
	}
	*/

	_previousUsed.Move(_lastFrameWholeUsed);
	Assert( _lastFrameWholeUsed.Last()==NULL );

	_lastFrameWholeUsed.Move(_thisFrameWholeUsed);
	Assert( _thisFrameWholeUsed.Last()==NULL );

	_previousUsed.Move(_lastFramePartialUsed);
	Assert( _lastFramePartialUsed.Last()==NULL );

	_lastFramePartialUsed.Move(_thisFramePartialUsed);
	Assert( _thisFramePartialUsed.Last()==NULL );
	// thisFrame lists are empty now
}

DEFINE_FAST_ALLOCATOR(HMipCacheD3D);

void TextureD3D::CacheUse( D3DMipCacheRoot &list )
{
	HMipCacheD3D *first;
	if( _cache ) _cache->Delete(),first=_cache;
	else first=new HMipCacheD3D;
	first->texture=this;
	list.Insert(first);
	_cache=first;
}

//DEFINE_FAST_ALLOCATOR(HSysCacheD3D);

void TextureD3D::SysCacheUse( D3DSysCacheRoot &list )
{
	HSysCacheD3D *first;
	if( _sysCache ) _sysCache->Delete(),first=_sysCache;
	else first=new HSysCacheD3D;
	first->texture=this;
	list.Insert(first);
	_sysCache=first;
}

// assume: max reasonable allocations per frame
const int MaxAllocationsPerFrame=8;
const int MaxCopyPerFrame=32768;

/*!
\patch 1.03 Date 7/12/2001 by Ondra
- Fixed: crash when too low heap memory
*/

MipInfo TextBankD3D::UseMipmap
(
	Texture *absTexture, int level, int top
)
{
	if( !absTexture )
	{
		// with pixel shaders we need to override default black
		#if PIXEL_SHADERS
		if (_engine->UsingPixelShaders())
		{
			absTexture = _white;
		}
		else
		#endif
		{
			return MipInfo(NULL,0);
		}
	}
	// level is the level we need
	// top is the level we would like to have
	// we are sure that texture is of type Texture
	// however this cast is potentially unsafe - take care
	TextureD3D *texture=static_cast<TextureD3D *>(absTexture);
	texture->LoadHeadersNV();

	// TODO: too big level - this must be bug
	/*
	if (level>1000)
	{
		//LogF("%s level num %d",texture->Name(),level);
		level = texture->_nMipmaps-1;
		top = texture->_nMipmaps-1;
	}
	*/

	//LogF("%s: use level %d, top %d",texture->Name(),level,top);

	saturateMin(level,texture->_mipmapNeeded);
	saturateMin(top,texture->_mipmapWanted);

	if( level<0 ) level=0;
	// FIX: crash when too low heap memory is autodetected
	saturateMin(level,texture->_nMipmaps-1);
	saturateMax(top,texture->_largestUsed);

	saturateMin(top,level);

	saturateMax(level,top);


	// level is neccessary, top is wanted
	// level>=top

	// never use mipmaps smaller than some limit

	// first level smaller than _maxSmallTexturePixels is minimal used
	int limitUse = _maxSmallTexturePixels/4;
	for( ; level>0; level-- )
	{
		PacLevelMem *mipTop=&texture->_mipmaps[level];
		if( mipTop->_w*mipTop->_h>=limitUse ) break;
	}

	// gurantee level>=top (top<=level)
	saturateMin(top,level);

	
	/*
	// record which level is neccessary
	if( texture->_levelNeededThisFrame>level ) texture->_levelNeededThisFrame=level;
	// use what was neccessary in the last frame
	if( level>texture->_levelNeededLastFrame ) level=texture->_levelNeededLastFrame;
	// never load the same texture twice in one frame
	if( texture->_levelNeededThisFrame<texture->_nMipmaps ) level=texture->_levelNeededThisFrame;
	*/

	if
	(
		_thisFrameCopied>MaxCopyPerFrame ||
		_thisFrameAlloc>MaxAllocationsPerFrame
	)
	{
		// we already copied a lot, try to use what we have
		if( texture->_levelLoaded<texture->_nMipmaps )
		{
			top = level = texture->_levelLoaded;
		}
		else if( texture->_smallLoaded<texture->_nMipmaps )
		{
			top = level = texture->_smallLoaded;
		}
		
	}


	// if we are happy with small level, use it
	if( texture->_smallLoaded<=level )
	{
		if( texture->LoadSmall()<0 )
		{
			return MipInfo(texture,-1);
		}
		return MipInfo(texture,texture->_smallLoaded); // given mip-map loaded
	}


	if (texture->_levelLoaded>level)
	{
		// level is not present - you have to load something-> load top
		//LogF("loading %s: %d (%d), loaded %d",texture->Name(),top,level,texture->_levelLoaded);
		level=top;

		if (texture->_levelNeededThisFrame>level)
		{
			texture->_levelNeededThisFrame = level;
			//LogF("texture %s: need %d",texture->Name(),level);
		}

		for(;;)
		{
			int ret=texture->LoadLevels(level);
			if( ret>=0 ) break;
			LogF("Out of VID: Try next level");
			level++;
			if( level>=texture->_nMipmaps ) break;
		}
	}
	else
	{
		if (texture->_levelNeededThisFrame>level)
		{
			texture->_levelNeededThisFrame = level;
			//LogF("texture %s: need %d",texture->Name(),level);
		}

		// level already loaded
		Assert( texture->_cache );
		// check if it used fully or partially
		// if it has been used fully during last two frames, we consider is fully used
		if( texture->LevelNeeded()<=texture->_levelLoaded )
		{
			texture->CacheUse(_thisFrameWholeUsed);
			//LogF
			//(
			//	"texture %s: full used (%d, %d)",texture->Name(),
			//	texture->LevelNeeded(),texture->_levelLoaded
			//);
		}
		else
		{
			texture->CacheUse(_thisFramePartialUsed);
			//LogF
			//(
			//	"texture %s: part used (%d, %d)",texture->Name(),
			//	texture->LevelNeeded(),texture->_levelLoaded
			//);
		}
		//Assert( mip->_level==level );
	}
		
	// find best mipmap that is loaded to VRAM
	level=texture->_levelLoaded;
	if( level>=texture->_nMipmaps )
	{
		// no big level - use small if possible
		if( texture->LoadSmall()<0 )
		{
			return MipInfo(texture,-1);
		}
		return MipInfo(texture,texture->_smallLoaded); // given mip-map loaded
	}

	// mipmap must stay locked until it is used
	// heap data are not longer needed
	return MipInfo(texture,level); // given mip-map loaded
}

/*
void TextBankD3D::ReleaseMipmap()
{
	//Assert( CheckConsistency() );
	//_loader->Unlock();
	//Assert( CheckConsistency() );
}
*/

struct SurfDesc
{
	PacFormat format;
	int w,h;
	int n;
};

static const SurfDesc PreloadDesc[]=
{
	// data based on real session statistics
	// TODO: make more samples on different scenes
	PacARGB1555,16,16,309,
	PacARGB4444,16,16,251,
	PacRGB565,16,16,228,
	PacARGB4444,8,16,122,
	PacRGB565,16,8, 93,
	PacRGB565,8,16, 78,
	PacARGB4444,16,8, 77,
	PacARGB4444,32,32, 73,
	PacRGB565,32,8, 58,
	PacARGB1555,8,16, 46,
	PacARGB4444,16,32, 42,
	PacARGB1555,16,8, 33,
	PacARGB4444,64,64, 26,
	PacRGB565,4,32, 21,
	PacRGB565,8,32, 16,
	PacRGB565,32,32, 14,
	PacRGB565,32,4, 13,
	PacARGB4444,8,32, 12,
	PacRGB565,128,128, 11,
	PacARGB1555,32,8,  8,
	PacRGB565,128,32,  8,
	PacRGB565,64,64,  7,
	PacARGB4444,32,8,  6,
	PacARGB1555,32,32,  6,
	PacARGB4444,32,4,  5,
	PacRGB565,256,64,  5,
	PacRGB565,32,16,  4,
	PacARGB4444,256,32,  4,
	PacARGB1555,128,128,  3,
	PacARGB1555,32,16,  3,
	PacRGB565,8,8,  2,
	PacARGB1555,64,64,  2,
	PacARGB4444,4,32,  2,
	PacARGB4444,128,128,  2,
	PacARGB4444,128,64,  2,
	PacRGB565,32,256,  2,
	PacRGB565,16,32,  2,
	PacRGB565,64,32,  2,
	PacRGB565,16,128,  2,
	PacRGB565,256,32,  2,
	PacARGB1555,8,32,  2,
	PacRGB565,64,128,  2,
	PacARGB1555,128,64,  1,
	PacRGB565,128,64,  1,
	PacARGB4444,32,64,  1,
	PacARGB4444,16,128,  1,
	PacARGB4444,64,128,  1,
	PacARGB1555,32,4,  1,
	PacARGB1555,4,32,  1,
	PacARGB4444,16,64,  1,
	PacRGB565,64,16,  1,
	PacARGB4444,64,16,  1,
	PacRGB565,32,128,  1,
	PacARGB4444,64,32,  1,
	PacRGB565,128,256,  5,
	PacARGB1555,256,128,  2,
	PacARGB4444,128,256,  1,
	PacRGB565,256,128,  9,
	PacARGB4444,256,128,  6,
	PacARGB4444,256,256,  5,
	PacRGB565,256,256, 21,
};

const int NPreloadDesc=sizeof(PreloadDesc)/sizeof(*PreloadDesc);

extern bool UseWindow;

static PacFormat SupportedFormat( PacFormat format )
{
	//if( format==PacRGB565 && !GEngineDD->Can565() ) return PacARGB1555;
	if( format==PacRGB565 ) return PacARGB1555;
	return format;
}

void TextBankD3D::InitDetailTextures()
{
	if( _detail ) return; // already initialized

	static RStringB detailName="data\\detail_dx.paa";
	if (QIFStreamB::FileExist(detailName))
	{
		_detail=new TextureD3D;
		_detail->Init(detailName);
		//_detail->InitLoaded();
		_detail->_isDetail=true;
	}

	static RStringB specularName="data\\specular_dx.paa";
	if (QIFStreamB::FileExist(specularName))
	{
		_specular=new TextureD3D;
		_specular->Init(specularName);
		//_specular->InitLoaded();
		_specular->_isDetail=true;
	}

	static RStringB whiteName="data\\white.pac";
	if (QIFStreamB::FileExist(whiteName))
	{
		_white=new TextureD3D;
		_white->Init(whiteName);
	}

	static RStringB grassName="data\\grass_dx.paa";
	if (QIFStreamB::FileExist(grassName))
	{
		_grass=new TextureD3D;
		_grass->Init(grassName);
		_grass->SetMaxSize(1024);
		//_specular->InitLoaded();
		_grass->_isDetail=true;
	}
	static RStringB waterName="misc\\waveBumpMap.paa";
	if (QIFStreamB::FileExist(waterName))
	{
		_waterBump=new TextureD3D;
		_waterBump->Init(waterName);
		_waterBump->SetMaxSize(1024);
		//_specular->InitLoaded();
		_waterBump->_isDetail=true;
	}
}

void TextBankD3D::PreCreateVRAM( int reserve, bool randomOrder )
{
	/*
	ReportTextures("* PreCreateVRAM [");
	CheckTextureMemory();

	// check how much textures is already allocated
	int maxLoad=_limitAlocatedTextures-_totalAllocated-reserve;
	saturateMin(maxLoad,32*1024*1024);

	// preload all small textures version
	DWORD time=GlobalTickCount();
	float preloadRatio=maxLoad/9000000.0;
	LogF("Pre-create ratio %f (allocated %d)",preloadRatio,_totalAllocated);

	
	IDirectDraw7 *dDraw=_engine->GetDirect3DDevice();

	int totalCount=0;
	for( int i=0; i<NPreloadDesc; i++ )
	{
		const SurfDesc &d=PreloadDesc[i];
		if( d.w*d.h<=_maxSmallTexturePixels ) continue;
		int cnt=toIntFloor(d.n*preloadRatio);
		totalCount+=cnt;
	}

	AutoArray<SurfaceInfoD3D> reorder;
	float invTotalCount=1.0/totalCount;
	int counter=0;
	int percent=-1;
	for( int i=0; i<NPreloadDesc; i++ )
	{
		TEXTUREDESC8 ddsd;
		InitDDSD(ddsd);

		const SurfDesc &d=PreloadDesc[i];

		ddsd.dwWidth=d.w;
		ddsd.dwHeight=d.h;
		if( d.w*d.h<=_maxSmallTexturePixels ) continue;
		int minSize=d.w<d.h ? d.w : d.h;
		int nMipmaps=0;
		while( minSize>=MIN_MIP_SIZE ) minSize>>=1,nMipmaps++;
		ddsd.dwMipMapCount=nMipmaps;
		PacFormat format=SupportedFormat(d.format);
		_engine->InitVRAMPixelFormat(ddsd.ddpfPixelFormat,format,false);
		int cnt=toIntFloor(d.n*preloadRatio);
		while( --cnt>=0 )
		{
			counter++;
			float factor=invTotalCount*counter;
			int newPercent=toIntFloor(factor*30+factor*factor*70);
			if( newPercent>percent )
			{
				//ProgressAdvance(newPercent-percent);
				//ProgressRefresh();
				percent=newPercent;
			}

			SurfaceInfoD3D surf;
			// estimate size, keep some reserve
			int estSize=d.w*d.h*2*2;
			int free=FreeTextureMemory();
			if( free<estSize )
			{
				LogF
				(
					"Not enough memory to preload %s:%dx%d.",
					FormatName(format),d.w,d.h
				);
				break;
			}
			HRESULT err=surf.CreateSurface(dDraw,ddsd,format);
			if( err!=D3D_OK )
			{
				LogF("Cannot create %dx%dx%s",d.w,d.h,FormatName(format));
				DDError("Preload: Create surface",err);
				goto Break;
			}
			reorder.Add(surf);
			_totalAllocated+=AlignVRAM(surf.Size());
		}
	}
	
	Break:
	time=GlobalTickCount()-time;
	LogF
	(
		"Pre-create VID: %d in %d surfaces (max %d), took %.2f sec ",
		_totalAllocated,reorder.Size(),_limitAlocatedTextures,time*0.001
	);
	// reorder randomly from reorder to _freeTextures
	while( reorder.Size()>0 )
	{
		int index = reorder.Size()-1;
		if (randomOrder) index=toIntFloor( GRandGen.RandomValue()*reorder.Size() );
		_freeTextures.Add(reorder[index]);
		reorder.Delete(index);
	}

	CheckTextureMemory();	
	ReportTextures("* PreCreateVRAM ]");
	*/
}

void TextBankD3D::PreCreateSys()
{
	/*
	int maxLoad=_limitSystemTextures;
	saturateMin(maxLoad,8*1024*1024);

	float preloadRatio=maxLoad/8000000.0;
	LogF("Preload sys ratio %f (alloc %d)",preloadRatio,_systemAllocated);

	TEXTUREDESC8 ddsd;
	memset(&ddsd,0,sizeof(ddsd)); 
	ddsd.dwSize=sizeof(ddsd);

	ddsd.dwFlags=DDSD_CAPS|DDSD_WIDTH|DDSD_HEIGHT|DDSD_PIXELFORMAT;
	ddsd.ddsCaps.dwCaps=DDSCAPS_TEXTURE;

	ddsd.ddsCaps.dwCaps|=DDSCAPS_SYSTEMMEMORY;
	
	IDirectDraw7 *dDraw=_engine->GetDirect3DDevice();

	int totalCount=0;
	for( int i=0; i<NPreloadDesc; i++ )
	{
		const SurfDesc &d=PreloadDesc[i];
		ddsd.dwWidth=d.w;
		ddsd.dwHeight=d.h;
		if( d.w*d.h<=_maxSmallTexturePixels ) continue;
		int cnt=toIntFloor(d.n*preloadRatio);
		totalCount+=cnt;
	}

	AutoArray<SurfaceInfoD3D> reorder;
	int counter=0;
	int percent=-1;
	float invTotalCount=1.0/totalCount;
	for( int i=0; i<NPreloadDesc; i++ )
	{
		const SurfDesc &d=PreloadDesc[i];

		if( d.w*d.h<=_maxSmallTexturePixels ) continue;
		PacFormat format=SupportedFormat(d.format);
		// TODO: DXT for big surfaces
		GEngineDD->InitVRAMPixelFormat(ddsd.ddpfPixelFormat,format,true);
		int cnt=toIntFloor(d.n*preloadRatio);
		while( --cnt>=0 )
		{
			counter++;
			float factor=invTotalCount*counter;
			int newPercent=toIntFloor(factor*30+factor*factor*70);
			if( newPercent>percent )
			{
				//ProgressAdvance((newPercent-percent)*0.3);
				//ProgressRefresh();
				percent=newPercent;
			}

			ddsd.dwWidth=d.w;
			ddsd.dwHeight=d.h;
			while( ddsd.dwWidth>=MIN_MIP_SIZE && ddsd.dwHeight>=MIN_MIP_SIZE )
			{
				SurfaceInfoD3D surf;
				HRESULT err=surf.CreateSurface(dDraw,ddsd,format);
				if( err!=D3D_OK )
				{
					Log("Cannot create %dx%dx%s",d.w,d.h,FormatName(format));
					goto Break;
				}
				reorder.Add(surf);
				_systemAllocated+=surf.SizeUsed();
				ddsd.dwWidth>>=1;
				ddsd.dwHeight>>=1;
			}
		}
	}

	LogF("Preload Syst: %d (max %d)",_systemAllocated,_limitSystemTextures);
	
	Break:
	// reorder randomly from reorder to _freeTextures
	while( reorder.Size()>0 )
	{
		int index=toIntFloor( GRandGen.RandomValue()*reorder.Size() );
		_freeSysTextures.Add(reorder[index]);
		reorder.Delete(index);
	}
	*/	
}

void TextBankD3D::FlushTextures()
{
	ReserveSystem(INT_MAX);

	//ReserveMemory(INT_MAX);
}

void TextBankD3D::FlushBank(QFBank *bank)
{
	for (int i=0; i<_texture.Size(); i++)
	{
		TextureD3D *tex = _texture[i];
		if (!tex) continue;
		// check if texture is from given bank
		if (!bank->FileExists(tex->GetName())) continue;
		_texture.Delete(i);
		i--;
	}
}

void TextBankD3D::PreCreateSurfaces()
{
	//PreCreateVRAM(512*1024,true);
}

int CompareTextureFileOrder(const LLink<TextureD3D> *tl1, const LLink<TextureD3D> *tl2)
{
	TextureD3D *t1 = *tl1;
	TextureD3D *t2 = *tl2;
	const char *n1 = t1->GetName();
	const char *n2 = t2->GetName();
	QFBank *b1 = QIFStreamB::AutoBank(t1->GetName());
	QFBank *b2 = QIFStreamB::AutoBank(t2->GetName());
	if (b1>b2) return -1;
	if (b1<b2) return +1;
	Assert(b1==b2);
	if (!b1) return 0;
	int o1 = b1->GetFileOrder(n1+strlen(b1->GetPrefix()));
	int o2 = b2->GetFileOrder(n2+strlen(b2->GetPrefix()));
	return o1-o2;
}

void DisplayTextureFileOrder(TextureD3D *t1)
{
	const char *n1 = t1->GetName();
	QFBank *b1 = QIFStreamB::AutoBank(t1->GetName());
	if (!b1) return;
	int o1 = b1->GetFileOrder(n1+strlen(b1->GetPrefix()));
	LogF("texture %s, order %d",n1,o1);
}

/*!
\patch 1.78 Date 7/23/2002 by Ondra
- Fixed: Random crash during program startup.
*/

void TextBankD3D::Preload()
{
	Compact();

	DWORD start = GlobalTickCount();
	// sort textures bank / by order in bank
	QSort(_texture.Data(),_texture.Size(),CompareTextureFileOrder);
	#if 0
	// if file server is able to handle request, ask for preloading all texture files
	// note: using this causes files to be loaded in memory
	// normally memory-mapping is used instead
	for (int i=0; i<_texture.Size(); i++)
	{
		TextureD3D *tex = _texture[i];
		if (!tex) continue;
		tex->PreloadHeaders();
	}
	#endif

	// force all texture sources to be loaded
	for (int i=0; i<_texture.Size(); i++)
	{
		TextureD3D *tex = _texture[i];
		if (!tex) continue;
		tex->LoadHeadersNV();
		//DisplayTextureFileOrder(tex);
		ProgressRefresh();
	}
	DWORD end = GlobalTickCount();

	LogF("Preload %d textures - %d ms",_texture.Size(),end-start);
	
	#if 0
	ReportTextures("* PreloadTextures [");
	// switch data - flush and preload

	// remove all currently loaded textures
	ReserveSystem(INT_MAX);

	int currentlyLeft = _limitAlocatedTextures - _totalAllocated;
	if (currentlyLeft<12*1024*1024 )
	{
		LogF("Preload: VRAM low - forced release");
		// release textures - we need space
		ReserveMemory(INT_MAX);
	}

	//PreCreateSys();

	CheckTextureMemory();
	int left = _limitAlocatedTextures;
	// each texture will have small surface loaded
	left -= _maxSmallTexturePixels*3*_texture.Size();
	// estimate how many the big textures can we afford
	left -= 256*256*8*3; // keep space for 8 big 16-b textures with mipmaps

	LogF("Free space %d KB",left/1024);
	saturateMin(left,8*1024*1024); // preload max. 8MB of large textures
	LogF("    loaded %d KB",left/1024);

	#define PROGRESS 1
	#if PROGRESS
	ProgressAdd(_texture.Size());

	ProgressRefresh();
	#endif

	LogF("Preloading %d textures",_texture.Size());
	// preload as many big textures as will fit into the video memory
	// estimate texture preloading

	Temp<int> levelsVram(_texture.Size());

	for( int i=0; i<_texture.Size(); i++ ) levelsVram[i]=0;
	for( int wantedSize=1; wantedSize<512*512*2; wantedSize<<=1 )
	{
		for( int i=0; i<_texture.Size(); i++ )
		{
			#if PROGRESS
			ProgressRefresh();
			#endif
			TextureD3D *texture=_texture[i];
			// estimate 
			for( int level=0; level<texture->_nMipmaps; level++ )
			{
				PacLevelMem *mip=&texture->_mipmaps[level];
				if (mip->TooLarge(texture->_maxSize)) continue;
				int pixels = mip->_w*mip->_h;
				if (pixels<_maxSmallTexturePixels) continue;
				int size = pixels*2;
				// check if DXT will be used
				// if (DXT) size/=4;
				if( size>wantedSize ) continue;
				// maybe it is already marked for loading
				if( (levelsVram[i]&(1<<level))==0 && left>=size )
				{
					// if it will fit, mark it for preloading
					left-=size;
					levelsVram[i]|=1<<level;
				}
			}
		}
	}
	
	CheckTextureMemory();
	/*
	_engine->ReportGRAM("before small");
	LogF(" VID left %d",left);
	for( int i=0; i<_texture.Size(); i++ )
	{
		TextureD3D *texture=_texture[i];
		texture->LoadSmall();
		#if PROGRESS
		ProgressAdvance(1);
		ProgressRefresh();
		#endif
	}
	CheckTextureMemory();
	*/
	_engine->ReportGRAM("before large");
	for( int i=0; i<_texture.Size(); i++ )
	{
		TextureD3D *texture=_texture[i];
		#define DIAG_REUSE 0
		#if DIAG_REUSE
		LogF("%s",texture->Name());
		if (!texture->GetSmallSurface())
		{
			LogF("  loading small");
		}
		#endif
		texture->LoadSmall();
		// check which level should we load
		int maxLevel=1024;
		int levels = levelsVram[i];
		for (int level=0,mask=1; level<24; mask<<=1,level++ )
		{
			if (levels&mask) {maxLevel=level;break;}
		}
		// load maxLevel
		if (maxLevel<texture->NMipmaps())
		{
			// force loading, no limits
			_thisFrameCopied = 0;
			_thisFrameAlloc = 0;

			#if DIAG_REUSE
				if (texture->_levelLoaded>maxLevel)
				{
					LogF("  loading large %d",maxLevel);
				}
			#endif
			
			UseMipmap(texture,maxLevel,maxLevel);

			#if REPORT_ALLOC>=20
			LogF
			(
				"Preloaded %s:%d, Wanted %d, small %d",
				texture->Name(),
				texture->_levelLoaded,maxLevel,texture->_smallLoaded
			);
			#endif
		}
		else
		{
			#if REPORT_ALLOC>=20
			LogF("Preloading %s - no level",texture->Name());
			#endif
		}

		#if PROGRESS
		ProgressAdvance(1);
		ProgressRefresh();
		#endif
	}

	ReportTextures("flush.txt");

	CheckTextureMemory();

	LogF("After preloading - VID free %d B",FreeTextureMemory());

	//PreCreateVRAM();

	ReportTextures("* PreloadTextures ]");
	// test - release all system memory surfaces
	ReserveSystem(INT_MAX);
	#endif
}

int TextBankD3D::FreeAGPMemory()
{
	Fail("Obsolete");
	return 0;
}

int TextBankD3D::FreeTextureMemory()
{
	#ifdef _XBOX
		const int maxTextures = 8*1024*1024;
		UINT dwFree = maxTextures - _totalAllocated;
		if (dwFree<0) return 0;
		return dwFree;
	#else
		IDirect3DDevice8 *dDraw=_engine->GetDirect3DDevice();
		UINT dwFree = dDraw->GetAvailableTextureMem();
		return dwFree;
	#endif
}


void TextBankD3D::CheckTextureMemory()
{
	_freeTextureMemory = FreeTextureMemory();
	_limitAlocatedTextures = _totalAllocated + _freeTextureMemory;
	_limitAlocatedTextures -= _reserveTextureMemory;
}

void TextBankD3D::ReloadAll()
{
	Log("Reload all textures.");
	// make sure all texture state is tested
}

#endif //!_DISABLE_GUI

