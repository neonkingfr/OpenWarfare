#ifdef _MSC_VER
#pragma once
#endif

#ifndef _TIT_EFFECTS_HPP
#define _TIT_EFFECTS_HPP

// predefined static camera positions
#include <Es/Strings/rString.hpp>

DEFINE_ENUM_BEG(TitEffectName)
	TitPlain,TitPlainDown,
	TitBlack,TitBlackFaded,
	TitBlackOut,TitBlackIn,
	TitWhiteOut,TitWhiteIn,

	NTitEffects
DEFINE_ENUM_END(TitEffectName)

class TitleEffect;

TitleEffect *CreateTitleEffect( TitEffectName name, RString text, float speed=1, Ref<Font> font = NULL, float size = 0 );
TitleEffect *CreateTitleEffectObj
(
	TitEffectName name, const ParamEntry &entry, float speed=1
);
TitleEffect *CreateTitleEffectRsc
(
	TitEffectName name, const ParamEntry &entry, float speed=1
);

#endif
