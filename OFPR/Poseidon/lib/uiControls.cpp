// Implementation of control hierarchy

#include "wpch.hpp"
#include "uiControls.hpp"
#include "keyInput.hpp"
#include "engine.hpp"
#include "world.hpp"
#include "dikCodes.h"
#include "vkCodes.h"
#include "autoComplete.hpp"

#ifndef _XBOX
#include <Es/Common/win.h>
#endif

#include "resincl.hpp"
#include "paramArchive.hpp"

#include "camera.hpp"
#include "txtPreload.hpp"

#include <Es/Algorithms/qsort.hpp>

#include "mbcs.hpp"

///////////////////////////////////////////////////////////////////////////////
//
//	 Implementation
//
///////////////////////////////////////////////////////////////////////////////

/*!
\file
Implementation file for basic controls.
*/

void DrawFrame(Texture *corner, PackedColor color, const Rect2DPixel &frame);
RString FindPicture(RString name);

// for listbox and combobox
#define SCROLL_SPEED		100.0
#define SCROLL_MIN			2.0
#define SCROLL_MAX			10.0

#define ISSPACE(c) ((c) >= 0 && (c) <= 32)

#define CX(x) (toInt((x) * w) + 0.5)
#define CY(y) (toInt((y) * h) + 0.5)

#define DrawBottom(i, color) GLOB_ENGINE->DrawLine(Line2DPixel(xx + i, yy + hh - 1 - i, xx + ww - i, yy + hh - 1 - i),	color, color);
#define DrawRight(i, color)	GLOB_ENGINE->DrawLine(Line2DPixel(xx + ww - 1 - i, yy + hh - 1 - i, xx + ww - 1 - i, yy + 0 + i), color, color);
#define DrawLeft(i, color) GLOB_ENGINE->DrawLine(Line2DPixel(xx + i, yy + hh - 1 - i, xx + i, yy + i), color, color);
#define DrawTop(i, color) GLOB_ENGINE->DrawLine(Line2DPixel(xx + i, yy + i, xx + ww - 1 - i, yy + i), color, color);

extern const float CameraZoom = 2.0;
extern const float InvCameraZoom = 1.0 / CameraZoom;

///////////////////////////////////////////////////////////////////////////////
// classes IControl, Control

IControl::IControl(ControlsContainer *parent, int idc)
{
	_parent = parent;
	_idc = idc;

	_visible = true;
	_enabled = true;
	_focused = false;
	_default = false;
}

Ref<Font> IControl::_tooltipFont;
float IControl::_tooltipSize = 0.02;

IControl::IControl(ControlsContainer *parent, int idc, const ParamEntry &cls)
{
	_parent = parent;
	_idc = idc;

	_visible = true;
	_enabled = true;
	_focused = false;
	_default = false;

	const ParamEntry *entry = cls.FindEntry("tooltip");
	if (entry) _tooltip = *entry;

	if (!_tooltipFont)
		_tooltipFont = GEngine->LoadFont(GetFontID("tahomaB24"));

	_tooltipColorText = PackedWhite;
	entry = cls.FindEntry("tooltipColorText");
	if (entry) GetValue(_tooltipColorText, *entry);

	_tooltipColorBox = PackedColor(Color(1, 1, 1, 0.3));
	entry = cls.FindEntry("tooltipColorBox");
	if (entry) GetValue(_tooltipColorBox, *entry);
	
	_tooltipColorShade = PackedColor(Color(0, 0, 0, 0.3));
	entry = cls.FindEntry("tooltipColorShade");
	if (entry) GetValue(_tooltipColorShade, *entry);
}

Control::Control
(
	ControlsContainer *parent, int type, int idc,
	int style, float x, float y, float w, float h
)
	: IControl(parent, idc)
{
	_parent = parent;
	_type = type;
	_style = style;
	_x = x;
	_y = y;
	_w = w;
	_h = h;
	_timer = UITIME_MAX;
	_scale = 1.0;
}

Control::Control(ControlsContainer *parent, int type, int idc, const ParamEntry &cls)
	: IControl(parent, idc, cls)
{
	_parent = parent;
	_type = type;
	_timer = UITIME_MAX;
	_scale = 1.0;
	
	_style = cls>>"style";
	_x = cls>>"x";
	_y = cls>>"y";
	_w = cls>>"w";
	_h = cls>>"h";
}

void Control::Move(float dx, float dy)
{
	_x += dx;
	_y += dy;
}

DrawCoord SceneToScreen(Vector3Par pos);

void Control::UpdateInfo(ControlObject *object, ControlInObject &info)
{
	Vector3 posTL = object->PositionModelToWorld(info.posTL);
	Vector3 posBR = object->PositionModelToWorld(info.posBR);
	Vector3Val position = object->Position();
	Vector3Val posBase = object->GetBasePosition();

	float dz = 0.5 * (posTL.Z() + posBR.Z() - 2.0 * position.Z());
	float scale = (posBase.Z() + dz) / (position.Z() + dz);

	DrawCoord pt1 = SceneToScreen(posTL);
	DrawCoord pt2 = SceneToScreen(posBR);
	float w = pt2.x - pt1.x;
	float h = pt2.y - pt1.y;
	info._control->SetPos
	(
		pt1.x + info.x * w, pt1.y + info.y * h,
		info.w * w, info.h * h 
	);
	info._control->SetScale(scale);
}

void IControl::PlaySound(SoundPars &pars)
{
	if( pars.name.GetLength()>0 )
	{
		//float volume = GSoundsys->GetSpeechVolCoef();
		AbstractWave *wave = GSoundScene->OpenAndPlayOnce2D
		(
			pars.name,
			pars.vol, pars.freq,
			false
		);
		if( wave )
		{
			wave->SetKind(WaveMusic); // UI sounds considered music???
			wave->SetSticky(true);
			GSoundScene->AddSound(wave);
		}
	}
	GSoundScene->AdvanceAll(0, !GWorld->IsSimulationEnabled());
}

bool IControl::OnSetFocus(bool up, bool def)
{
	_focused = true;
	return true;
}

bool IControl::OnKillFocus()
{
	_focused = false;
	return true;
}

void IControl::OnTimer()
{
	_timer = UITIME_MAX;
}

IControl *IControl::GetCtrl(int idc)
{
	if (idc == _idc) return this;
	return NULL;
}

IControl *IControl::GetCtrl(float x, float y)
{
	return this;
}

bool IControl::OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	return false;
}

bool IControl::OnKeyUp(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	return false;
}

bool IControl::OnChar(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	return false;
}

bool IControl::OnIMEChar(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	return false;
}

bool IControl::OnIMEComposition(unsigned nChar, unsigned nFlags)
{
	return false;
}

void IControl::OnLButtonDown(float x, float y)
{
}

void IControl::OnLButtonUp(float x, float y)
{
}

void IControl::OnLButtonClick(float x, float y)
{
}

void IControl::OnLButtonDblClick(float x, float y)
{
}

void IControl::OnRButtonDown(float x, float y)
{
}

void IControl::OnRButtonUp(float x, float y)
{
}

void IControl::OnMouseMove(float x, float y, bool active)
{
}

void IControl::OnMouseHold(float x, float y, bool active)
{
}

void IControl::OnMouseZChanged(float dz)
{
}

bool IControl::OnCanDestroy(int code)
{
	return true;
}

void IControl::OnDestroy(int code)
{
}

/*!
\patch 1.50 Date 4/11/2002 by Jirka
- Added: tooltips for UI controls 
*/

void IControl::DrawTooltip(float x, float y)
{
	if (_tooltip.GetLength() > 0)
	{
		int w = GEngine->Width2D();
		int h = GEngine->Height2D();

		const float border = 0.005;
		float height = _tooltipFont->Height();
		float width = GEngine->GetTextWidth(_tooltipSize, _tooltipFont, _tooltip);

		float bx = x + 0.02;
//		float by = y - 0.5 * height - border;
		float by = y + 0.02;
		float ex = bx + width + 2.0 * border;
		float ey = by + height + 2.0 * border;

		MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
		GEngine->Draw2D(mip, _tooltipColorShade, Rect2DPixel(bx * w, by * h, (ex - bx) * w, (ey - by) * h));
		GEngine->DrawText(Point2DFloat(bx + border, by + border), _tooltipSize,	_tooltipFont, _tooltipColorText, _tooltip);
		GEngine->DrawLine(Line2DPixel(bx * w, by * h, ex * w, by * h), _tooltipColorBox, _tooltipColorBox);
		GEngine->DrawLine(Line2DPixel(ex * w, by * h, ex * w, ey * h), _tooltipColorBox, _tooltipColorBox);
		GEngine->DrawLine(Line2DPixel(ex * w, ey * h, bx * w, ey * h), _tooltipColorBox, _tooltipColorBox);
		GEngine->DrawLine(Line2DPixel(bx * w, ey * h, bx * w, by * h), _tooltipColorBox, _tooltipColorBox);
	}
}

RString FindShape(RString name);

ControlObject::ControlObject(ControlsContainer *parent, int idc, const ParamEntry &cls)
: Object
	(
		Shapes.New(FindShape(cls >> "model"), false, false),
		-1
	),
	IControl(parent, idc, cls)
{
	if (_shape && _shape->NLevels()>0)
	{
		_shape->LevelOpaque(0)->MakeCockpit();
		_shape->OrSpecial(BestMipmap | NoDropdown | DisableSun);
	}

	_position.Init();
	_position[0] = (cls >> "position")[0];
	_position[1] = (cls >> "position")[1];
	_position[2] = (cls >> "position")[2];
	_position[2] *= CameraZoom;
	SetPosition(_position);

	Vector3 dir, up;
	dir.Init();
	up.Init();
	dir[0] = (cls >> "direction")[0];
	dir[1] = (cls >> "direction")[1];
	dir[2] = (cls >> "direction")[2];
	up[0] = (cls >> "up")[0];
	up[1] = (cls >> "up")[1];
	up[2] = (cls >> "up")[2];
	SetOrient(dir, up);

	float scale = cls >> "scale";
	SetScale(scale);

	_offset = VZero;
	_moving = false;
}

ControlObject::~ControlObject()
{
}

int ControlObject::GetType()
{
	return CT_OBJECT;
}

int ControlObject::GetStyle()
{
	return 0;
}

Vector3 ControlObject::Convert2DTo3D(const Point2DFloat &point, float z)
{
	AspectSettings as;
	GEngine->GetAspectSettings(as);
	float scrX = point.x*(as.uiBottomRightX-as.uiTopLeftX)+as.uiTopLeftX;
	float scrY = point.y*(as.uiBottomRightY-as.uiTopLeftY)+as.uiTopLeftY;
	//
	return Vector3
	(
		(scrX - 0.5) * InvCameraZoom * as.leftFOV *z, as.topFOV * (0.5 - scrY) * InvCameraZoom *z, z
	);
}

bool ControlObject::IsInside(float x, float y)
{
	Vector3 begin = VZero;
	Vector3 end = Convert2DTo3D(Point2DFloat(x,y),2.0 * CameraZoom);
	CollisionBuffer buffer;
	Intersect(buffer, begin, end, 0, ObjIntersectGeom);
	if (buffer.Size() > 0)
	{
		_modelPos = buffer[0].pos;
		return true;
	}
	return false;
}

void ControlObject::Move(float dx, float dy)
{
	// nothing to do
}

void ControlObject::OnLButtonDown(float x, float y)
{
	_offset = PositionModelToWorld(_modelPos) - Position();
	_moving = true;
}

void ControlObject::OnLButtonUp(float x, float y)
{
	_moving = false;
}

void ControlObject::OnMouseMove(float x, float y, bool active)
{
	if (_moving)
	{
		Vector3 pos = Convert2DTo3D(Point2DFloat(x,y),Position().Z() + _offset.Z());

		pos -= _offset;
		if (_parent) _parent->OnObjectMoved(_idc, pos - _position);
		_position = pos;
		SetPosition(pos);
	}
}
	
void ControlObject::OnDraw(float alpha)
{
	PackedColor color(Color(1, 1, 1, alpha));
	SetConstantColor(color);
	int level = 0;
#if _ENABLE_CHEATS
	//if (CHECK_DIAG(DEXxxx)) level = _shape->FindGeometryLevel();
#endif
	Draw(level, ClipAll, *this);
}

ControlObjectWithZoom::ControlObjectWithZoom(ControlsContainer *parent, int idc, const ParamEntry &cls)
: ControlObject(parent, idc, cls)
{
	_positionBack.Init();
	_positionBack[0] = (cls >> "positionBack")[0];
	_positionBack[1] = (cls >> "positionBack")[1];
	_positionBack[2] = (cls >> "positionBack")[2];
	_positionBack[2] *= CameraZoom;
	for (int i=0; i<3; i++)
		_zoomMatrices[i].SetIdentity();

	_inBack = cls >> "inBack";
	_enableZoom = cls >> "enableZoom";

	_zoomDuration = cls >> "zoomDuration";

	_zooming = false;
		
	if (_inBack)
		SetPosition(_positionBack);
}

int ControlObjectWithZoom::GetType()
{
	return CT_OBJECT_ZOOM;
}

void ControlObjectWithZoom::OnLButtonDblClick(float x, float y)
{
	if (_enableZoom) Zoom(_inBack);
}

void ControlObjectWithZoom::OnMouseMove(float x, float y, bool active)
{
	if (_zooming) return;
	if (_moving)
	{
		Vector3 pos = Convert2DTo3D(Point2DFloat(x,y),Position().Z() + _offset.Z());

		pos -= _offset;
		if (_inBack)
		{
			if (_parent) _parent->OnObjectMoved(_idc, pos - _positionBack);
			_positionBack = pos;
		}
		else
		{
			if (_parent) _parent->OnObjectMoved(_idc, pos - _position);
			_position = pos;
		}
		SetPosition(pos);
	}
}

void ControlObjectWithZoom::UpdatePosition()
{
	if (_zooming)
	{
		float time = Glob.uiTime - _zoomStart;
		if (time >= _zoomTimes[2])
		{
			time = _zoomTimes[2];
			_zooming = false;
			_inBack = !_inBack;
		}
		Vector3 pos = (*_interpolator)(time).Position();
		SetPosition(pos);
		if (!_zooming) _interpolator = NULL;
	}
}

void ControlObjectWithZoom::Zoom(bool zoom)
{
	if (zoom != _inBack) return;

	_zooming = true;
	_zoomStart = Glob.uiTime;
	_zoomTimes[0] = 0;
	_zoomTimes[1] = 0.5 * _zoomDuration;
	_zoomTimes[2] = 1.0 * _zoomDuration;
	if (zoom)
	{
		_zoomMatrices[0].SetPosition(_positionBack);
		_zoomMatrices[2].SetPosition(_position);
	}
	else
	{
		_zoomMatrices[0].SetPosition(_position);
		_zoomMatrices[2].SetPosition(_positionBack);
	}
	Vector3 pos;
	pos.Init();
	pos[0] = 0.33 * _position[0] + 0.67 * _positionBack[0];
	pos[1] = 0.33 * _position[1] + 0.67 * _positionBack[1];
	pos[2] = 0.67 * _position[2] + 0.33 * _positionBack[2];
	_zoomMatrices[1].SetPosition(pos);

	_interpolator = new InterpolatorSpline(_zoomMatrices, _zoomTimes, 3);
}

LSError ControlObjectWithZoom::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("inBack", _inBack, 1))	
	_position[2] *= InvCameraZoom;
	CHECK(ar.Serialize("position", _position, 1))
	_position[2] *= CameraZoom;
	_positionBack[2] *= InvCameraZoom;
	CHECK(ar.Serialize("positionBack", _positionBack, 1))
	_positionBack[2] *= CameraZoom;

	if (ar.IsLoading())
	{
		if (_inBack)
			SetPosition(_positionBack);
		else
			SetPosition(_position);
	}
	return LSOK;
}

ControlObjectContainer::ControlObjectContainer(ControlsContainer *parent, int idc, const ParamEntry &cls)
: base(parent, idc, cls)
{
	_indexFocused = -1;
	_indexL = -1;
	_indexR = -1;
	_indexMove = -1;
	LoadControls(cls);
	SetPosition(Position());	// update controls
}

int ControlObjectContainer::GetType()
{
	return CT_OBJECT_CONTAINER;
}

IControl *ControlObjectContainer::GetCtrl(int idc)
{
	if (idc == _idc) return this;
	for (int i=0; i<_controls.Size(); i++)
	{
		IControl *control = _controls[i]._control;
		if (control)
		{
			IControl *ctrl = control->GetCtrl(idc);
			if (ctrl) return ctrl;
		}
	}
	return NULL;
}

IControl *ControlObjectContainer::GetCtrl(float x, float y)
{
	int index = FindControl(x, y);
	if (index >= 0) return _controls[index]._control;
	else return this;
}

IControl *ControlObjectContainer::GetFocused()
{
	if (_indexFocused < 0) return this;
	if (_indexFocused >= _controls.Size())
	{
		_indexFocused = -1;
		return this;
	}
	return _controls[_indexFocused]._control;
}

bool ControlObjectContainer::CanBeDefault() const
{
	int n = _controls.Size();
	for (int i=0; i<n; i++)
	{
		IControl *ctrl = _controls[i]._control;
		if (ctrl->IsVisible() && ctrl->IsEnabled() && ctrl->CanBeDefault())
			return true;
	}
	return false;
}

bool ControlObjectContainer::OnSetFocus(bool up, bool def)
{
	if (!base::OnSetFocus(up)) return false;
	int n = _controls.Size();
	if (n <= 0) return true;
	if (up)
	{
		for (int i=0; i<n; i++)
		{
			IControl *ctrl = _controls[i]._control;
			if (ctrl->IsVisible() && ctrl->IsEnabled() && (!def || ctrl->CanBeDefault()))
			{
				ctrl->OnSetFocus();
				_indexFocused = i;
				return true;
			}
		}
		_indexFocused = -1;
		return true;
	}
	else
	{
		for (int i=n-1; i>=0; i--)
		{
			IControl *ctrl = _controls[i]._control;
			if (ctrl->IsVisible() && ctrl->IsEnabled() && (!def || ctrl->CanBeDefault()))
			{
				ctrl->OnSetFocus();
				_indexFocused = i;
				return true;
			}
		}
		_indexFocused = -1;
		return true;
	}
}

void ControlObjectContainer::LoadControl(const ParamEntry &ctrlCls)
{
	int type = ctrlCls >> "type";
	int idc = ctrlCls >> "idc";
	Control *ctrl = _parent->OnCreateCtrl(type, idc, ctrlCls);
	if (ctrl != NULL)
	{
		int index = _controls.Add();
		ControlInObject &info = _controls[index];
		info._control = ctrl;
		info.x = ctrl->X();
		info.y = ctrl->Y();
		info.w = ctrl->W();
		info.h = ctrl->H();
		info.selection = ctrlCls >> "selection";
		info.posTL = _shape->MemoryPoint(info.selection + RString(" TL"));
		info.posTR = _shape->MemoryPoint(info.selection + RString(" TR"));
		info.posBL = _shape->MemoryPoint(info.selection + RString(" BL"));
		info.posBR = _shape->MemoryPoint(info.selection + RString(" BR"));
	}
}

void ControlObjectContainer::LoadControls(const ParamEntry &cls)
{
	const ParamEntry *cfg = cls.FindEntry("controls");
	if (cfg)
	{
		if (cfg->IsClass())
		{
			for (int i=0; i<cfg->GetEntryCount(); i++)
			{
				const ParamEntry &ctrlCls = cfg->GetEntry(i);
				if (!ctrlCls.IsClass()) continue;
				if (!ctrlCls.FindEntry("type")) continue;
				if (!ctrlCls.FindEntry("idc")) continue;
				LoadControl(ctrlCls);
			}
		}
		else
		{
			for (int i=0; i<cfg->GetSize(); i++)
			{
				RString ctrlName = (*cfg)[i];
				const ParamEntry &ctrlCls = cls >> ctrlName;
				LoadControl(ctrlCls);
			}
		}
	}
}

void ControlObjectContainer::SetPosition(Vector3Par pos)
{
	base::SetPosition(pos);

	// update position of _controls items
	for (int i=0; i<_controls.Size(); i++)
	{
		ControlInObject &info = _controls[i];
		info._control->UpdateInfo(this, info);
	}
}

int ControlObjectContainer::FindControl(float x, float y)
{
	for (int i=0; i<_controls.Size(); i++)
	{
		Control *ctrl = _controls[i]._control;
		Assert(ctrl);
		if
		(
			ctrl->IsVisible() &&
			ctrl->IsEnabled() &&
			ctrl->IsInside(x, y)
		) return i;
	}
	return -1;
}

void ControlObjectContainer::OnLButtonDown(float x, float y)
{
	int iFound = FindControl(x, y);
	_indexL = iFound;
	if (iFound >= 0)
	{
		SetFocus(iFound);
		_controls[iFound]._control->OnLButtonDown(x, y);
	}
	else
	{
		SetFocus(-1);
		base::OnLButtonDown(x, y);
	}
}

void ControlObjectContainer::OnLButtonDblClick(float x, float y)
{
	int iFound = FindControl(x, y);
	if (iFound == _indexL)
	{
		if (iFound >= 0)
			_controls[iFound]._control->OnLButtonDblClick(x, y);
		else
			base::OnLButtonDblClick(x, y);
	}
	else
	{
		_indexL = iFound;
		if (iFound >= 0)
			_controls[iFound]._control->OnLButtonDown(x, y);
		else
			base::OnLButtonDown(x, y);
	}
}

void ControlObjectContainer::OnLButtonUp(float x, float y)
{
	if (_indexL >= 0)
		_controls[_indexL]._control->OnLButtonUp(x, y);
	else
		base::OnLButtonUp(x, y);
}

void ControlObjectContainer::OnLButtonClick(float x, float y)
{
	if (_indexL >= 0)
		_controls[_indexL]._control->OnLButtonClick(x, y);
	else
		base::OnLButtonClick(x, y);
}

void ControlObjectContainer::OnRButtonDown(float x, float y)
{
	int iFound = FindControl(x, y);
	_indexR = iFound;
	if (iFound >= 0)
		_controls[iFound]._control->OnRButtonDown(x, y);
	else
		base::OnRButtonDown(x, y);
}

void ControlObjectContainer::OnRButtonUp(float x, float y)
{
	if (_indexR >= 0)
		_controls[_indexR]._control->OnRButtonUp(x, y);
	else
		base::OnRButtonUp(x, y);
	_indexR = -1;
}

void ControlObjectContainer::OnMouseMove(float x, float y, bool active)
{
	if (active)
	{
		int index;
		if (GInput.mouseL)
			index = _indexL;
		else if (GInput.mouseR)
			index = _indexR;
		else
			index = FindControl(x, y);
		if (_indexMove >= 0 && _indexMove != index)
			_controls[_indexMove]._control->OnMouseMove(x, y, false);
		if (index >= 0)
			_controls[index]._control->OnMouseMove(x, y);
		else
			base::OnMouseMove(x, y);
		_indexMove = index;
	}
	else
	{
		if (_indexMove >= 0)
			_controls[_indexMove]._control->OnMouseMove(x, y, false);
		else
			base::OnMouseMove(x, y, false);
		_indexMove = -1;
	}
	base::OnMouseMove(x, y, active);
}

void ControlObjectContainer::OnMouseHold(float x, float y, bool active)
{
	if (active)
	{
		int index;
		if (GInput.mouseL)
			index = _indexL;
		else if (GInput.mouseR)
			index = _indexR;
		else
			index = FindControl(x, y);
		if (_indexMove >= 0 && _indexMove != index)
			_controls[_indexMove]._control->OnMouseMove(x, y, false);
		if (index >= 0)
			_controls[index]._control->OnMouseHold(x, y);
		else
			base::OnMouseHold(x, y);
		_indexMove = index;
	}
	else
	{
		if (_indexMove >= 0)
			_controls[_indexMove]._control->OnMouseHold(x, y, false);
		else
			base::OnMouseHold(x, y, false);
		_indexMove = -1;
	}
	base::OnMouseHold(x, y, active);
}

void ControlObjectContainer::OnMouseZChanged(float dz)
{
	if (_indexMove >= 0)
		_controls[_indexMove]._control->OnMouseZChanged(dz);
	else
		base::OnMouseZChanged(dz);
}

void ControlObjectContainer::SetFocus(int i, bool def)
{
	if (i == _indexFocused) return;
	
	if (_indexFocused >= 0 && _indexFocused < _controls.Size())
	{
		if (!_controls[_indexFocused]._control->OnKillFocus())
			return;
	}

	if (i >= 0)
		_controls[i]._control->OnSetFocus(true, def);
	_indexFocused = i;
}

bool ControlObjectContainer::NextCtrl()
{
	int n = _controls.Size();
	if (n == 0) return false;
	for (int i=_indexFocused+1; i<n; i++)
	{
		IControl *ctrl = _controls[i]._control;
		if (ctrl->IsVisible() && ctrl->IsEnabled())
		{
			SetFocus(i);
			return true;
		}
	}
	SetFocus(-1);
	return false;
}

bool ControlObjectContainer::PrevCtrl()
{
	int n = _controls.Size();
	if (n == 0) return false;
	for (int i=_indexFocused-1; i>=0; i--)
	{
		IControl *ctrl = _controls[i]._control;
		if (ctrl->IsVisible() && ctrl->IsEnabled())
		{
			SetFocus(i);
			return true;
		}
	}
	SetFocus(-1);
	return false;
}

bool ControlObjectContainer::NextDefaultCtrl()
{
	int n = _controls.Size();
	if (n == 0) return false;
	for (int i=_indexFocused+1; i<n; i++)
	{
		IControl *ctrl = _controls[i]._control;
		if (ctrl->IsVisible() && ctrl->IsEnabled() && ctrl->CanBeDefault())
		{
			SetFocus(i, true);
			return true;
		}
	}
//	SetFocus(-1);
	return false;
}

bool ControlObjectContainer::PrevDefaultCtrl()
{
	int n = _controls.Size();
	if (n == 0) return false;
	for (int i=_indexFocused-1; i>=0; i--)
	{
		IControl *ctrl = _controls[i]._control;
		if (ctrl->IsVisible() && ctrl->IsEnabled() && ctrl->CanBeDefault())
		{
			SetFocus(i, true);
			return true;
		}
	}
//	SetFocus(-1);
	return false;
}

bool ControlObjectContainer::OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	int n = _controls.Size();
	if (n == 0) return false;
	if
	(
		_indexFocused >= 0 && _indexFocused < n &&
		_controls[_indexFocused]._control->OnKeyDown(nChar, nRepCnt, nFlags)
	)
	{
		return true;
	}
	else switch (nChar)
	{
		case VK_TAB:
			if (GInput.keys[DIK_LSHIFT] || GInput.keys[DIK_RSHIFT])
				return PrevCtrl();
			else
				return NextCtrl();
		case VK_LEFT:
		case VK_UP:
		case VK_PRIOR:
			if (_indexFocused >= 0 && _controls[_indexFocused]._control->CanBeDefault())
				return PrevDefaultCtrl();
			else
				return true;
		case VK_RIGHT:
		case VK_DOWN:
		case VK_NEXT:
			if (_indexFocused >= 0 && _controls[_indexFocused]._control->CanBeDefault())
				return NextDefaultCtrl();
			else
				return true;
		default:
			return false;
	}
}

bool ControlObjectContainer::OnKeyUp(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	int n = _controls.Size();
	if (n == 0) return false;
	if
	(
		_indexFocused >= 0 && _indexFocused < n &&
		_controls[_indexFocused]._control->OnKeyUp(nChar, nRepCnt, nFlags)
	)
	{
		return true;
	}
	return false;
}

bool ControlObjectContainer::OnChar(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	int n = _controls.Size();
	if (n == 0) return false;
	if
	(
		_indexFocused >= 0 && _indexFocused < n &&
		_controls[_indexFocused]._control->OnChar(nChar, nRepCnt, nFlags)
	)
	{
		return true;
	}
	return false;
}

bool ControlObjectContainer::OnIMEChar(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	int n = _controls.Size();
	if (n == 0) return false;
	if
	(
		_indexFocused >= 0 && _indexFocused < n &&
		_controls[_indexFocused]._control->OnIMEChar(nChar, nRepCnt, nFlags)
	)
	{
		return true;
	}
	return false;
}

bool ControlObjectContainer::OnIMEComposition(unsigned nChar, unsigned nFlags)
{
	int n = _controls.Size();
	if (n == 0) return false;
	if
	(
		_indexFocused >= 0 && _indexFocused < n &&
		_controls[_indexFocused]._control->OnIMEComposition(nChar, nFlags)
	)
	{
		return true;
	}
	return false;
}

void ControlObjectContainer::OnDraw(float alpha)
{
	ControlObjectWithZoom::OnDraw(alpha);

	bool isAnimated = true;
	if (isAnimated) Animate(_shape->FindMemoryLevel());
	for (int i=0; i<_controls.Size(); i++)
	{
		if (i == _indexFocused) continue;
		ControlInObject &info = _controls[i];
		Control *ctrl = info._control;
		if (ctrl->IsVisible())
		{
			if (isAnimated)
			{
				info.posTL = _shape->MemoryPoint(info.selection + RString(" TL"));
				info.posTR = _shape->MemoryPoint(info.selection + RString(" TR"));
				info.posBL = _shape->MemoryPoint(info.selection + RString(" BL"));
				info.posBR = _shape->MemoryPoint(info.selection + RString(" BR"));
				ctrl->UpdateInfo(this, info);
			}
			ctrl->OnDraw(alpha);
		}
	}
	// draw focused control at last
	if (_indexFocused >= 0 && _indexFocused < _controls.Size())
	{
		ControlInObject &info = _controls[_indexFocused];
		Control *ctrl = info._control;
		if (ctrl->IsVisible())
		{
			if (isAnimated)
			{
				info.posTL = _shape->MemoryPoint(info.selection + RString(" TL"));
				info.posTR = _shape->MemoryPoint(info.selection + RString(" TR"));
				info.posBL = _shape->MemoryPoint(info.selection + RString(" BL"));
				info.posBR = _shape->MemoryPoint(info.selection + RString(" BR"));
				ctrl->UpdateInfo(this, info);
			}
			ctrl->OnDraw(alpha);
		}
	}
	if (isAnimated) Deanimate(_shape->FindMemoryLevel());
}

void ControlObjectContainer::DrawTooltip(float x, float y)
{
	int iFound = FindControl(x, y);
	if (iFound >= 0)
	{
		_controls[iFound]._control->DrawTooltip(x, y);
	}
	else base::DrawTooltip(x, y);
}

///////////////////////////////////////////////////////////////////////////////
// class CStatic

ControlObjectContainerAnim::ControlObjectContainerAnim(ControlsContainer *parent, int idc, const ParamEntry &cls)
	: ControlObjectContainer(parent, idc, cls)
{
	_shape->SetAutoCenter(false);
	_shape->CalculateBoundingSphere();
	_shape->AllowAnimation();

	_open = false;
	_close = false;
	_animSpeed = cls >> "animSpeed";
	_invAnimSpeed = 1.0 / _animSpeed;
	_skeleton = new Skeleton();
	AnimationRTName name;
	name.name = GetAnimationName(cls >> "animation");
	name.skeleton = _skeleton;
	_animation = new AnimationRT(name, false);
	_animation->Prepare(_shape, _skeleton, _weights, false);

	int autoZoom = cls >> "autoZoom";
	_autoZoom = autoZoom != 0;
	int autoOpen = cls >> "autoOpen";
	if (autoOpen != 0)
	{
		_phase = 0;
		Open();
	}
	else
		_phase = cls >> "animPhase";
}

void ControlObjectContainerAnim::Open()
{
	if (_open) return;
	_open = true;
	_close = false;
	_animStart = Glob.uiTime - _phase * _invAnimSpeed;
/*
	if (_close)
	{
		_close = false;
		_animStart = Glob.uiTime - (1 - _phase) * _invAnimSpeed;
	}
	else
	{
		_animStart = Glob.uiTime;
	}
*/
	if (_autoZoom) Zoom(true);
}

void ControlObjectContainerAnim::Close()
{
	if (_close) return;
	_close = true;
	_open = false;
	_animStart = Glob.uiTime - (1 - _phase) * _invAnimSpeed;
/*
	if (_open)
	{
		_open = false;
		_animStart = Glob.uiTime - (1 - _phase) * _invAnimSpeed;
	}
	else
	{
		_animStart = Glob.uiTime;
	}
*/
	if (_autoZoom) Zoom(false);
}

void ControlObjectContainerAnim::Animate(int level)
{
	if (_open)
	{
		_phase = _animSpeed * (Glob.uiTime - _animStart);
		if (_phase >= 1.0)
		{
			_phase = 1.0;
			_open = false;
		}
	}
	else if (_close)
	{
		_phase = 1.0 - _animSpeed * (Glob.uiTime - _animStart);
		if (_phase <= 0)
		{
			_phase = 0;
			_close = false;
			_parent->OnCtrlClosed(IDC());
		}
	}
/*
	Shape *shape = _shape->Level(level);
	if (shape && shape->IsAnimated())
	{
		shape->SetPhase(_phase, -1);
	}
*/
	_animation->Apply(_weights, _shape, level, _phase);
}

void ControlObjectContainerAnim::Deanimate(int level)
{
	_animation->Apply(_weights, _shape, level, 0);
/*
	Shape *shape = _shape->Level(level);
	if (shape && shape->IsAnimated())
		shape->SetPhase(0, 0);
*/
}

///////////////////////////////////////////////////////////////////////////////
// class CStatic

const float textBorder = 0.005;

CStatic::CStatic
(
	ControlsContainer *parent, const ParamEntry &cls,
	float x, float y, float w, float h, RString text
)
	: Control(parent, CT_STATIC, cls >> "idc", cls >> "style", x, y, w, h)
{
	_enabled = ((_style & ST_TYPE) == ST_MULTI);
	_texture = NULL;
	_bgColor = GetPackedColor(cls>>"colorBackground");
	_ftColor = GetPackedColor(cls>>"colorText");
	_font = GLOB_ENGINE->LoadFont(GetFontID(cls>>"font"));
	const ParamEntry *entry = cls.FindEntry("size");
	if (entry) _size = (float)(*entry) * _font->Height();
	else _size = cls >> "sizeEx";
	_firstLine = 0;
	_lineSpacing = 1;
	SetText(text);

	InitColors();
}

CStatic::CStatic(ControlsContainer *parent, int idc, const ParamEntry &cls)
	: Control(parent, CT_STATIC, idc, cls)
{
	_texture = NULL;
	_bgColor = GetPackedColor(cls>>"colorBackground");
	_ftColor = GetPackedColor(cls>>"colorText");
	_font = GLOB_ENGINE->LoadFont(GetFontID(cls>>"font"));
	const ParamEntry *entry = cls.FindEntry("size");
	if (entry) _size = (float)(*entry) * _font->Height();
	else _size = cls >> "sizeEx";
	_firstLine = 0;
	_enabled = false;
	_lineSpacing = 1;
	if ((_style & ST_TYPE) == ST_MULTI)
	{
		_enabled = true;
		_lineSpacing = cls >> "lineSpacing";
	}

	RString text = cls>>"text";
	SetText(text);

	InitColors();
}

void CStatic::InitColors()
{
	const ParamEntry &clsColors = Pars >> "CfgWrapperUI" >> "Colors";
	_color1 = GetPackedColor(clsColors >> "color1");
	_color2 = GetPackedColor(clsColors >> "color2");
	_color3 = GetPackedColor(clsColors >> "color3");
	_color4 = GetPackedColor(clsColors >> "color4");
	_color5 = GetPackedColor(clsColors >> "color5");
	switch (_style & ST_TYPE)
	{
		case ST_BACKGROUND:
			_alpha = Pars >> "CfgWrapperUI" >> "Background" >> "alpha";
			break;
		case ST_GROUP_BOX:
			_alpha = Pars >> "CfgWrapperUI" >> "GroupBox" >> "alpha";
			break;
		case ST_GROUP_BOX2:
			_alpha = Pars >> "CfgWrapperUI" >> "GroupBox2" >> "alpha";
			break;
		case ST_TITLE_BAR:
			_alpha = Pars >> "CfgWrapperUI" >> "TitleBar" >> "alpha";
			break;
		default:
			_alpha = 1;
			break;
	}
}

bool CStatic::SetText(RString text)
{
	if (_text && !strcmp(_text, text)) return false;

	_text = text;
	if ((_style & ST_TYPE) == ST_PICTURE || (_style & ST_TYPE) == ST_TILE_PICTURE)
	{
		text = FindPicture(text);
		text.Lower();
		_texture = GlobLoadTexture(text);
		if (_texture) _texture->SetMaxSize(1024); // no limits
	}
	else if ((_style & ST_TYPE) == ST_MULTI)
	{
		FormatText();
		float height = _scale * _size;
		float maxLine = NLines() - _h / height;
		saturateMax(maxLine, 0);
		saturate(_firstLine, 0, toIntCeil(maxLine));
	}
	return true;
}

bool CStatic::SetText(const char *text)
{
	// avoid reallocation if possible
	if (GetText() && !strcmp(GetText(),text)) return false;
	return SetText(RString(text));
}

int CStatic::NLines() const
{
	return (_style & ST_TYPE) == ST_MULTI ? _lines.Size() : 1;
}

float CStatic::GetTextHeight() const
{
	return NLines() * _scale * _size;
}

bool CStatic::OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	if (NLines() <= 1) return false;

	switch (nChar)
	{
	case VK_UP:
		_firstLine--;
		saturateMax(_firstLine, 0);
		return true;
	case VK_DOWN:
		_firstLine++;
		float height = _scale * _size;
		float maxLine = NLines() - _h / height;
		saturateMin(_firstLine, toIntCeil(maxLine));
		return true;
	}
	return false;
}

void CStatic::FormatText()
{
	_lines.Clear();

	float lineWidth = _w - 2 * _scale * textBorder;
	float size = _scale * _size;

	const char *p = _text;
	while (*p != 0)
	{
		// begin of the line
		_lines.Add(p - (const char *)_text);
		const char *word = p;
		int n = 0;
		float width = 0;
		while (true)
		{
			const char *q = p;
			char c = *p++;
			if (c == 0) return;
			if (c == '\\' && *p == 'n')
			{
				p++;
				break;
			}
			if (ISSPACE(c))
			{
				n++;
				word = p;
			}

			if ((c & 0x80) && _font->GetLangID() == Korean && *p != 0)
			{
				// TODO: simplify
				char temp[3]; temp[0] = c; temp[1] = *p++; temp[2] = 0;
				width += GEngine->GetTextWidth(size, _font, temp);
			}
			else
			{
				// TODO: simplify
				char temp[2]; temp[0] = c; temp[1] = 0;
				width += GLOB_ENGINE->GetTextWidth(size, _font, temp);
			}

			if (width > lineWidth)
			{
				if (n > 0)
				{
					p = word;
					break;
				}
				else
				{
					p = q;
					break;
				}
			}
		}
	}
}

inline PackedColor ModAlpha( PackedColor color, float alpha )
{
	int a=toInt(alpha*color.A8());
	saturate(a,0,255);
	return PackedColorRGB(color,a);
}

RString CStatic::GetLine(int i) const
{

	if ((_style & ST_TYPE) != ST_MULTI) return "";
	if (i < 0) return "";
	if (i >= NLines()) return "";

	int from = _lines[i];
	int to = i + 1 < NLines() ? _lines[i + 1] : strlen(_text);
	return _text.Substring(from,to);
}

void CStatic::OnDraw(float alpha)
{
	const int w = GLOB_ENGINE->Width2D();
	const int h = GLOB_ENGINE->Height2D();

	float xx = toInt(_x * w);
	float yy = toInt(_y * h);
	float ww = toInt((_w+_x) * w)-xx;
	float hh = toInt((_h+_y) * h)-yy;

	xx += 0.5;
	yy += 0.5;

	int type = _style & ST_TYPE;
	
	PackedColor ftColor = ModAlpha(_ftColor, alpha);
	PackedColor bgColor = ModAlpha(_bgColor, alpha);

	float size = _scale * _size;

	switch (type)
	{
		case ST_HUD_BACKGROUND:
		{
			Texture *corner = GLOB_SCENE->Preloaded(Corner);
			DrawFrame
			(
				corner,
				bgColor,
				Rect2DPixel(xx, yy, ww, hh)
			);
			break;
		}
		case ST_BACKGROUND:
		{
			// background
			Texture *background = GLOB_SCENE->Preloaded(DialogBackground);
			MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap
			(
				background, 0, 0
			);
			float invH = 1, invW = 1;
			if (background)
			{
				invH = 1.0 / background->AHeight();
				invW = 1.0 / background->AWidth();
			}
			Rect2DPixel rect;
			rect.w = ww;
			rect.h = hh;
			rect.x = xx;
			rect.y = yy;
			Draw2DPars pars;
			pars.mip = mip;
			pars.SetColor(bgColor);
			pars.spec = NoZBuf|IsAlpha|NoClamp|IsAlphaFog;
			pars.SetU(0, ww * invW );
			pars.SetV(0, hh * invH );
			GLOB_ENGINE->Draw2D(pars, rect);
			//GLOB_ENGINE->TextBank()->ReleaseMipmap();

			// frame
			PackedColor bgColor1 = ModAlpha(_color1, alpha * _alpha);
			PackedColor bgColor2 = ModAlpha(_color2, alpha * _alpha);
			PackedColor bgColor3 = ModAlpha(_color3, alpha * _alpha);
			PackedColor bgColor4 = ModAlpha(_color4, alpha * _alpha);
			PackedColor bgColor5 = ModAlpha(_color5, alpha * _alpha);

			DrawLeft(0, bgColor5);
			DrawTop(0, bgColor5);
			DrawLeft(1, bgColor4);
			DrawTop(1, bgColor4);
			DrawLeft(2, bgColor3);
			DrawTop(2, bgColor3);
			DrawLeft(3, bgColor3);
			DrawTop(3, bgColor3);
			DrawLeft(4, bgColor2);
			DrawTop(4, bgColor2);
			DrawLeft(5, bgColor1);
			DrawTop(5, bgColor1);

			DrawBottom(0, bgColor1);
			DrawRight(0, bgColor1);
			DrawBottom(1, bgColor2);
			DrawRight(1, bgColor2);
			DrawBottom(2, bgColor3);
			DrawRight(2, bgColor3);
			DrawBottom(3, bgColor3);
			DrawRight(3, bgColor3);
			DrawBottom(4, bgColor4);
			DrawRight(4, bgColor4);
			DrawBottom(5, bgColor5);
			DrawRight(5, bgColor5);
			break;
		}
		case ST_FRAME:
		{
			if ( _text.GetLength() >0 )
			{
				float width = GLOB_ENGINE->GetTextWidth(size, _font, _text);
				float heigth = size;
				const float border = _scale * 0.01;
				if (border + width + border <= _w)
				{
					GLOB_ENGINE->DrawText
					(
						Point2DFloat(_x + border, _y), size,
						Rect2DFloat(_x, _y, _w, _h),
						_font, ftColor, _text
					);
					float top = yy + 0.5 * heigth * h;
					GLOB_ENGINE->DrawLine
					(
						Line2DPixel(xx, top, xx + border * w, top), ftColor, ftColor
					);
					GLOB_ENGINE->DrawLine
					(
						Line2DPixel(xx + (border + width) * w, top, xx + ww - 1, top), ftColor, ftColor
					);
					GLOB_ENGINE->DrawLine
					(
						Line2DPixel(xx, top, xx, yy + hh - 1), ftColor, ftColor
					);
					GLOB_ENGINE->DrawLine
					(
						Line2DPixel(xx + ww - 1, top, xx + ww - 1, yy + hh - 1), ftColor, ftColor
					);
					DrawBottom(0, ftColor);
					return;
				}
			}

			DrawLeft(0, ftColor);
			DrawTop(0, ftColor);
			DrawBottom(0, ftColor);
			DrawRight(0, ftColor);
			return;
		}
		case ST_WITH_RECT:
		{
			DrawLeft(0, ftColor);
			DrawTop(0, ftColor);
			DrawBottom(0, ftColor);
			DrawRight(0, ftColor);
			break;
		}
		case ST_LINE:
		{
			GLOB_ENGINE->DrawLine(Line2DPixel(xx, yy, xx + ww, yy + hh), ftColor, ftColor);
			break;
		}
		case ST_GROUP_BOX:
		{
			PackedColor grpColor2 = ModAlpha(_color2, alpha * _alpha);
			PackedColor grpColor3 = ModAlpha(_color3, alpha * _alpha);
			PackedColor grpColor4 = ModAlpha(_color4, alpha * _alpha);

			Texture *background = GLOB_SCENE->Preloaded(TextureWhite);
			MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(background, 0, 0);
			GLOB_ENGINE->Draw2D
			(
				mip, grpColor3, Rect2DPixel(xx + 1, yy + 1, ww - 2, hh - 2)
			);
			//GLOB_ENGINE->TextBank()->ReleaseMipmap();

			DrawLeft(0, grpColor2);
			DrawTop(0, grpColor2);
			DrawBottom(0, grpColor4);
			DrawRight(0, grpColor4);
			break;
		}
		case ST_GROUP_BOX2:
		{
			PackedColor grpColor2 = ModAlpha(_color2, alpha * _alpha);
			PackedColor grpColor3 = ModAlpha(_color3, alpha * _alpha);
			PackedColor grpColor4 = ModAlpha(_color4, alpha * _alpha);

			Texture *background = GLOB_SCENE->Preloaded(DialogGroup);
			MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(background, 0, 0);
			GLOB_ENGINE->Draw2D
			(
				mip, bgColor, Rect2DPixel(xx + 1, yy + 1, ww - 2, hh - 2)
			);
			//GLOB_ENGINE->TextBank()->ReleaseMipmap();

			DrawLeft(0, grpColor2);
			DrawTop(0, grpColor2);
			DrawBottom(0, grpColor4);
			DrawRight(0, grpColor4);
			break;
		}
		case ST_PICTURE:
		case ST_TILE_PICTURE:
		{
			if (_texture)
			{
				MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(_texture, 0, 0);
				GLOB_ENGINE->Draw2D(mip, ftColor, Rect2DPixel(xx, yy, ww, hh));
				//GLOB_ENGINE->TextBank()->ReleaseMipmap();
			}
			return;	// do not draw any text (text field content texture name)
		}
		case ST_TITLE_BAR:
		{
			PackedColor barColor2 = ModAlpha(_color2, alpha * _alpha);
			PackedColor barColor3 = ModAlpha(_color3, alpha * _alpha);
			PackedColor barColor4 = ModAlpha(_color4, alpha * _alpha);

			Texture *background = GLOB_SCENE->Preloaded(TextureWhite);
			MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(background, 0, 0);
			GLOB_ENGINE->Draw2D
			(
				mip, barColor3, Rect2DPixel(xx + 5, yy + 1, ww - 10, hh - 2)
			);
			//GLOB_ENGINE->TextBank()->ReleaseMipmap();
			GLOB_ENGINE->DrawLine
			(
				Line2DPixel(xx + 5, yy, xx + ww - 5, yy),
				barColor4, barColor4
			);
			GLOB_ENGINE->DrawLine
			(
				Line2DPixel(xx + ww - 8, yy + 5, xx + 7, yy + 5),
				barColor2, barColor2
			);
			GLOB_ENGINE->DrawLine
			(
				Line2DPixel(xx + 7, yy + 5, xx + 7, yy + hh - 6),
				barColor2, barColor2
			);
			GLOB_ENGINE->DrawLine
			(
				Line2DPixel(xx + 7, yy + hh - 6, xx + ww - 7, yy + hh - 6),
				barColor4, barColor4
			);
			GLOB_ENGINE->DrawLine
			(
				Line2DPixel(xx + ww - 8, yy + hh - 6, xx + ww - 8, yy + 5),
				barColor4, barColor4
			);
			GLOB_ENGINE->DrawLine
			(
				Line2DPixel(xx + 5, yy + hh - 1, xx + ww - 5, yy + hh - 1),
				barColor2, barColor2
			);
			break;
		}
		default:
		{
			if (bgColor.A8()>0)
			{
				Texture *texture = GLOB_SCENE->Preloaded(TextureWhite);
				MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(texture, 0, 0);
				GLOB_ENGINE->Draw2D
				(
					mip, bgColor, Rect2DPixel(xx, yy, ww, hh) 
				);
			}
			break;
		}
	}
	
	if (!IsText())
		return;
	
	float height = size * _lineSpacing;
	if (type == ST_MULTI)
	{
		if (IsFocused() && (_style & ST_NO_RECT) == 0)
		{
			DrawLeft(0, ftColor);
			DrawTop(0, ftColor);
			DrawBottom(0, ftColor);
			DrawRight(0, ftColor);
		}

		int fl = toIntFloor(_firstLine);
		float top = _y - (_firstLine - fl) * height;
		for (int i=fl; i<_lines.Size(); i++)
		{
			RString text = GetLine(i);
			char *p = text.MutableData();
			while ((p = strchr(p, '\\'))!=0)
			{
				if (*(++p) == 'n')
				{
					*(p-1) = 0;
					break;
				}
			}
			DrawText(text, top, alpha);
			top += height;
			if (top >= _y + _h) break;
		}
	}
	else
	{
		float top = _y + 0.5 * (_h - height);
		DrawText(_text, top, alpha);
	}
}

bool CStatic::IsText()
{
	return _text.GetLength() >0;
}

void CStatic::DrawText(const char *text, float top, float alpha)
{
	int pos = _style & ST_HPOS;
	float size = _scale * _size;
	float left;
	bool vertical = false;
	const float height = size;
	const float offsetX = 0.075 * height;
	const float offsetY = 0.1 * height;

	switch (pos)
	{
		case ST_UP:
			left = _x + 0.5 * (_w - height);
			top = _y + _scale * textBorder;
			vertical = true;
			break;
		case ST_VCENTER:
			left = _x + 0.5 * (_w - height);
			top = _y + 0.5 * (_h - GLOB_ENGINE->GetTextWidth(size, _font, text));
			vertical = true;
			break;
		case ST_DOWN:
			left = _x + 0.5 * (_w - height);
			top = _y + _h - GLOB_ENGINE->GetTextWidth(size, _font, text) - _scale * textBorder;
			vertical = true;
			break;
		case ST_RIGHT:
			left = _x + _w - GLOB_ENGINE->GetTextWidth(size, _font, text) - _scale * textBorder;
			break;
		case ST_CENTER:
			left = _x + 0.5 * (_w - GLOB_ENGINE->GetTextWidth(size, _font, text));
			break;
		default:
			Assert(pos == ST_LEFT)
			left = _x + _scale * textBorder;
			break;
	}

	bool shadow = (_style & ST_SHADOW) != 0;

	PackedColor ftColor = ModAlpha(_ftColor, alpha);
	PackedColor shadowColor = PackedColor(Color(0, 0, 0, alpha));
	
	if (vertical)
	{
		if (shadow)
			GLOB_ENGINE->DrawTextVertical
			(
				Point2DFloat(left + offsetX, top + offsetY), size,
				Rect2DFloat(_x, _y,	_w, _h),
				_font, shadowColor, text
			);
		GLOB_ENGINE->DrawTextVertical
		(
			Point2DFloat(left, top), size,
			Rect2DFloat(_x, _y,	_w, _h),
			_font, ftColor, text
		);
	}
	else
	{
		if (shadow)
			GLOB_ENGINE->DrawText
			(
				Point2DFloat(left + offsetX, top + offsetY), size,
				Rect2DFloat(_x, _y,	_w, _h),
				_font, shadowColor, text
			);
		GLOB_ENGINE->DrawText
		(
			Point2DFloat(left, top), size,
			Rect2DFloat(_x, _y,	_w, _h),
			_font, ftColor, text
		);
	}
}

bool CStaticTime::IsText()
{
	return (_style & ST_TYPE) != ST_MULTI;
}

void CStaticTime::DrawText(const char *text, float top, float alpha)
{
	float time = _time.GetTimeOfDay();
	if (time > 1.0) time--;
	time *= 24;
	int hour = toIntFloor(time);
	time -= hour;
	time *= 60;
	int min = toIntFloor(time);
	time -= min;
	time *= 60;
	int sec = toIntFloor(time);
	bool colon = !_blink || (sec % 2 == 0);
	
	char bufHour[4], bufMin[4], bufSec[4];
	sprintf(bufHour, "% 2d", hour);
	sprintf(bufMin, "%02d", min);
	sprintf(bufSec, "%02d", sec);

	const float coefH = 0.8;
	const float coefW = 0.5;
	float size = _scale * _size;
	float size2 = size * 0.75;

	float diffH = coefH * (size - size2);
	float diffW = coefW * GLOB_ENGINE->GetTextWidth(size2, _font, " ");

	float width = 
		GLOB_ENGINE->GetTextWidth(size, _font, bufHour) +
		GLOB_ENGINE->GetTextWidth(size, _font, ":") +
		GLOB_ENGINE->GetTextWidth(size, _font, bufMin) +
		diffW + GLOB_ENGINE->GetTextWidth(size2, _font, bufSec);

	int pos = _style & ST_HPOS;
	float left;
	switch (pos)
	{
		case ST_RIGHT:
			left = _x + _w - width - _scale * textBorder;
			break;
		case ST_CENTER:
			left = _x + 0.5 * (_w - width);
			break;
		default:
			Assert(pos == ST_LEFT)
			left = _x + _scale * textBorder;
			break;
	}
	
	const float offsetX = 0.075 * size;
	const float offsetY = 0.1 * size;

	bool shadow = (_style & ST_SHADOW) != 0;

	PackedColor ftColor = ModAlpha(_ftColor, alpha);
	PackedColor shadowColor = PackedColor(Color(0, 0, 0, alpha));
	
	if (shadow)
	{
		float left2 = left;
		GLOB_ENGINE->DrawText
		(
			Point2DFloat(left2 + offsetX, top + offsetY), size,
			Rect2DFloat(_x, _y,	_w, _h),
			_font, shadowColor, bufHour
		);
		left2 += GLOB_ENGINE->GetTextWidth(size, _font, bufHour);
		if (colon)
		{
			GLOB_ENGINE->DrawText
			(
				Point2DFloat(left2 + offsetX, top + offsetY), size,
				Rect2DFloat(_x, _y,	_w, _h),
				_font, shadowColor, ":"
			);
		}
		left2 += GLOB_ENGINE->GetTextWidth(size, _font, ":");
		GLOB_ENGINE->DrawText
		(
			Point2DFloat(left2 + offsetX, top + offsetY), size,
			Rect2DFloat(_x, _y,	_w, _h),
			_font, shadowColor, bufMin
		);
		left2 += GLOB_ENGINE->GetTextWidth(size, _font, bufMin) + diffW;
		GLOB_ENGINE->DrawText
		(
			Point2DFloat(left2 + offsetX, top + diffH + offsetY), size2,
			Rect2DFloat(_x, _y,	_w, _h),
			_font, shadowColor, bufSec
		);
	}
	GLOB_ENGINE->DrawText
	(
		Point2DFloat(left, top), size,
		Rect2DFloat(_x, _y,	_w, _h),
		_font, ftColor, bufHour
	);
	left += GLOB_ENGINE->GetTextWidth(size, _font, bufHour);
	if (colon)
	{
		GLOB_ENGINE->DrawText
		(
			Point2DFloat(left, top), size,
			Rect2DFloat(_x, _y,	_w, _h),
			_font, ftColor, ":"
		);
	}
	left += GLOB_ENGINE->GetTextWidth(size, _font, ":");
	GLOB_ENGINE->DrawText
	(
		Point2DFloat(left, top), size,
		Rect2DFloat(_x, _y,	_w, _h),
		_font, ftColor, bufMin
	);
	left += GLOB_ENGINE->GetTextWidth(size, _font, bufMin) + diffW;
	GLOB_ENGINE->DrawText
	(
		Point2DFloat(left, top + diffH), size2,
		Rect2DFloat(_x, _y,	_w, _h),
		_font, ftColor, bufSec
	);
}

///////////////////////////////////////////////////////////////////////////////
// class CSkewStatic

CSkewStatic::CSkewStatic(ControlsContainer *parent, int idc, const ParamEntry &cls)
	: Control(parent, CT_STATIC_SKEW, idc, cls)
{
	_enabled = false;
	_color = GetPackedColor(cls >> "color");
	_xTL = cls >> "TL" >> "x";
	_yTL = cls >> "TL" >> "y";
	_alphaTL = cls >> "TL" >> "alpha";
	_xTR = cls >> "TR" >> "x";
	_yTR = cls >> "TR" >> "y";
	_alphaTR = cls >> "TR" >> "alpha";
	_xBL = cls >> "BL" >> "x";
	_yBL = cls >> "BL" >> "y";
	_alphaBL = cls >> "BL" >> "alpha";
	_xBR = cls >> "BR" >> "x";
	_yBR = cls >> "BR" >> "y";
	_alphaBR = cls >> "BR" >> "alpha";
	
	const ParamEntry &clsColors = Pars >> "CfgWrapperUI" >> "Colors";
	_lineColors[0] = GetPackedColor(clsColors >> "color1");
	_lineColors[1] = GetPackedColor(clsColors >> "color2");
	_lineColors[2] = GetPackedColor(clsColors >> "color3");
	_lineColors[3] = GetPackedColor(clsColors >> "color4");
	_lineColors[4] = GetPackedColor(clsColors >> "color5");

	_x = floatMin(_xTL, _xBL);
	_w = floatMax(_xTR, _xBR) - _x;
	_y = floatMin(_yTL, _yTR);
	_h = floatMax(_yBL, _yBR) - _y;
}

void CSkewStatic::Move(float dx, float dy)
{
	Control::Move(dx, dy);
	_xTL += dx;
	_xTR += dx;
	_xBL += dx;
	_xBR += dx;
	_yTL += dy;
	_yTR += dy;
	_yBL += dy;
	_yBR += dy;
}
	
void DrawLine
(
	float xs, float ys, float as,
	float xe, float ye, float ae, PackedColor color
)
{
	if (as <= 0)
	{
		if (ae <= 0) return;
		float coef = ae / (ae - as);
		xs = xe - coef * (xe - xs);
		ys = ye - coef * (ye - ys);
		// as = 0 == ae - coef * (ae - as)
		as = 0;
	}
	else if (ae <= 0)
	{
		float coef = as / (as - ae);
		xe = xs - coef * (xs - xe);
		ye = ys - coef * (ys - ye);
		// ae = 0 == as - coef * (as - ae)
		ae = 0;
	}
	GEngine->DrawLine
	(
		Line2DPixel(xs, ys, xe, ye),
		ModAlpha(color, as), ModAlpha(color, ae)
	);
}

/*
#ifdef _MSC_VER
void DrawPoly
(
	MipInfo &mip,
	Vertex2D *vertices, float *alphas, int n,
	Rect2D &clipRect = Rect2DClip()
)
#else
static Rect2DClip DefaultClipRect;

void DrawPoly
(
	MipInfo &mip,
	Vertex2D *vertices, float *alphas, int n,
	Rect2D &clipRect = DefaultClipRect
)
#endif
{
	// simplified implementation
	for (int i=0; i<n-1; i++)
	{
		int iplus = i;
		int iminus = i;
		if (alphas[i] < 0)
		{
			if (alphas[i + 1] <= 0) continue;
			iplus++;
		}
		else if (alphas[i + 1] < 0)
		{
			if (alphas[i] == 0) continue;
			iminus++;
		}
		else continue;

		float coef = alphas[iplus] / (alphas[iplus] - alphas[iminus]);
		Vertex2D &vplus = vertices[iplus];
		Vertex2D &vminus = vertices[iminus];
		vminus.x = vplus.x - coef * (vplus.x - vminus.x);
		vminus.y = vplus.y - coef * (vplus.y - vminus.y);
		vminus.u = vplus.u - coef * (vplus.u - vminus.u);
		vminus.v = vplus.v - coef * (vplus.v - vminus.v);
		alphas[iminus] = 0;
	}

	for (int i=0; i<n; i++)
	{
		vertices[i].color = ModAlpha(vertices[i].color, alphas[i]);
	}

	GEngine->DrawPoly(mip, vertices, n, clipRect);
}
*/

void CSkewStatic::OnDraw(float alpha)
{
	Fail("Obsolete");
	/*
	const int w = GLOB_ENGINE->Width2D();
	const int h = GLOB_ENGINE->Height2D();

	switch (_style & ST_TYPE)
	{
		case ST_BACKGROUND:
		{
			if (_yTL >= _yBL || _yTR >= _yBR) break;

			// background
			Texture *background = GLOB_SCENE->Preloaded(DialogBackground);
			MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(background, 0, 0);
			float invH=1, invW=1;
			if (background)
			{
				invH = 1.0 / background->AHeight();
				invW = 1.0 / background->AWidth();
			}
			const int n = 4;
			Vertex2D vs[n];
			float as[n];
			// 0
			vs[0].x = _xTL * w;
			vs[0].y = _yTL * h;
			vs[0].u = (_xTL - _x) * w * invW;
			vs[0].v = 0;
			vs[0].color = _color;
			as[0] = alpha * _alphaTL;
			// 1
			vs[1].x = _xTR * w;
			vs[1].y = _yTR * h;
			vs[1].u = (_xTR - _x) * w * invW;
			vs[1].v = 0;
			vs[1].color = _color;
			as[1] = alpha * _alphaTR;
			// 2
			vs[2].x = _xBR * w;
			vs[2].y = _yBR * h;
			vs[2].u = (_xBR - _x) * w * invW;
			vs[2].v = _h * h * invH;
			vs[2].color = _color;
			as[2] = alpha * _alphaBR;
			// 3
			vs[3].x = _xBL * w;
			vs[3].y = _yBL * h;
			vs[3].u = (_xBL - _x) * w * invW;
			vs[3].v = _h * h * invH;
			vs[3].color = _color;
			as[3] = alpha * _alphaBL;

			DrawPoly(mip, vs, as, n);
			
			float xs, ys, xe, ye;
			float alphas, alphae;
			float dleft = (_xBL - _xTL) / (_yBL - _yTL);
			float dright = (_xBR - _xTR) / (_yBR - _yTR);
			
			// left border
			xs = toInt(_xBL * w) + 0.5;
			ys = toInt(_yBL * h) + 0.5;
			alphas = _alphaBL * alpha;
			xe = toInt(_xTL * w) + 0.5;
			ye = toInt(_yTL * h) + 0.5;
			alphae = _alphaTL * alpha;
			for (int i=0; i<5; i++)
			{
				DrawLine
				(
					xs + i, ys, alphas,
					xe + i, ye, alphae,
					_lineColors[4 - i]
				);
			}
			// right border
			xs = toInt(_xBR * w) + 0.5;
			ys = toInt(_yBR * h) + 0.5;
			alphas = _alphaBR * alpha;
			xe = toInt(_xTR * w) + 0.5;
			ye = toInt(_yTR * h) + 0.5;
			alphae = _alphaTR * alpha;
			for (int i=0; i<5; i++)
			{
				DrawLine
				(
					xs - i, ys, alphas,
					xe - i, ye, alphae,
					_lineColors[i]
				);
			}
			// top border
			xs = toInt(_xTL * w) + 0.5;
			ys = toInt(_yTL * h) + 0.5;
			alphas = _alphaTL * alpha;
			xe = toInt(_xTR * w) + 0.5;
			ye = toInt(_yTR * h) + 0.5;
			alphae = _alphaTR * alpha;
			for (int i=0; i<5; i++)
			{
				DrawLine
				(
					xs + i + toInt(i * dleft), ys + i, alphas,
					xe - i + toInt(i * dright), ye + i, alphae,
					_lineColors[4 - i]
				);
			}
			// bottom border
			xs = toInt(_xBL * w) + 0.5;
			ys = toInt(_yBL * h) + 0.5;
			alphas = _alphaBL * alpha;
			xe = toInt(_xBR * w) + 0.5;
			ye = toInt(_yBR * h) + 0.5;
			alphae = _alphaBR * alpha;
			for (int i=0; i<5; i++)
			{
				DrawLine
				(
					xs + i - toInt((4 - i) * dleft), ys - i, alphas,
					xe - i - toInt((4 - i) * dright), ye - i, alphae,
					_lineColors[i]
				);
			}
			break;
		}
		case ST_GROUP_BOX:
		{
			const int n = 4;
			Vertex2D vs[n];
			float as[n];
			// 0
			vs[0].x = _xTL * w;
			vs[0].y = _yTL * h;
			vs[0].u = 0;
			vs[0].v = 0;
			vs[0].color = _lineColors[2];
			as[0] = alpha * _alphaTL;
			// 1
			vs[1].x = _xTR * w;
			vs[1].y = _yTR * h;
			vs[1].u = 0;
			vs[1].v = 0;
			vs[1].color = _lineColors[2];
			as[1] = alpha * _alphaTR;
			// 2
			vs[2].x = _xBR * w;
			vs[2].y = _yBR * h;
			vs[2].u = 0;
			vs[2].v = 0;
			vs[2].color = _lineColors[2];
			as[2] = alpha * _alphaBR;
			// 3
			vs[3].x = _xBL * w;
			vs[3].y = _yBL * h;
			vs[3].u = 0;
			vs[3].v = 0;
			vs[3].color = _lineColors[2];
			as[3] = alpha * _alphaBL;

			MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
			DrawPoly(mip, vs, as, n);

			DrawLine
			(
				_xTL * w, _yTL * h, _alphaTL * alpha,
				_xBL * w, _yBL * h, _alphaBL * alpha,
				_lineColors[3]
			);
			DrawLine
			(
				_xTL * w, _yTL * h, _alphaTL * alpha,
				_xTR * w, _yTR * h, _alphaTR * alpha,
				_lineColors[1]
			);
			DrawLine
			(
				_xTR * w, _yTR * h, _alphaTR * alpha,
				_xBR * w, _yBR * h, _alphaBR * alpha,
				_lineColors[1]
			);
			DrawLine
			(
				_xBL * w, _yBL * h, _alphaBL * alpha,
				_xBR * w, _yBR * h, _alphaBR * alpha,
				_lineColors[3]
			);
			break;
		}
		default:
		{
			const int n = 4;
			Vertex2D vs[n];
			float as[n];
			// 0
			vs[0].x = _xTL * w;
			vs[0].y = _yTL * h;
			vs[0].u = 0;
			vs[0].v = 0;
			vs[0].color = _color;
			as[0] = alpha * _alphaTL;
			// 1
			vs[1].x = _xTR * w;
			vs[1].y = _yTR * h;
			vs[1].u = 0;
			vs[1].v = 0;
			vs[1].color = _color;
			as[1] = alpha * _alphaTR;
			// 2
			vs[2].x = _xBR * w;
			vs[2].y = _yBR * h;
			vs[2].u = 0;
			vs[2].v = 0;
			vs[2].color = _color;
			as[2] = alpha * _alphaBR;
			// 3
			vs[3].x = _xBL * w;
			vs[3].y = _yBL * h;
			vs[3].u = 0;
			vs[3].v = 0;
			vs[3].color = _color;
			as[3] = alpha * _alphaBL;

			MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
			DrawPoly(mip, vs, as, n);
			break;
		}
	}
	*/
}

///////////////////////////////////////////////////////////////////////////////
// class CEditContainer

CEditContainer::CEditContainer(const ParamEntry &cls)
{
	_ftColor = GetPackedColor(cls >> "colorText");
	_selColor = GetPackedColor(cls >> "colorSelection");
	_font = GLOB_ENGINE->LoadFont(GetFontID(cls>>"font"));

	_enableToolip = false;
	_firstVisible = 0;
	_blockBegin = 0;
	_blockEnd = 0;

	_maxChars = INT_MAX;

	_size = 0;

	RString autoComplete = cls>>"autocomplete";
	SetAutoComplete(CreateAutoComplete(autoComplete));
}

void CEditContainer::SetAutoComplete(IAutoComplete *ac)
{
	_autoComplete = ac;
}

void CEditContainer::SetText(RString text)
{
	if (text.GetLength() <= _maxChars) _text = text;
	else _text = text.Substring(0, _maxChars);
	
	FormatText();

	int n = strlen(text);
	saturateMin(_blockBegin, n);
	saturateMin(_blockEnd, n);
	saturateMin(_firstVisible, n);
	EnsureVisible(_blockEnd);
}

void CEditContainer::SetCaretPos(int pos)
{
	int n = strlen(GetText());
	saturate(pos, 0, n);
	_blockBegin = pos;
	_blockEnd = pos;
	EnsureVisible(pos);
}

int CEditContainer::NextPos(int pos)
{
	if (_font->GetLangID() == Korean)
	{
		const char *text = GetText();
		if ((text[pos] & 0x80) && pos + 2 <= strlen(text))
			return pos + 2;
		else return pos + 1;
	}
	else return pos + 1;
}

int CEditContainer::PrevPos(int pos)
{
	if (_font->GetLangID() == Korean)
	{
		const char *text = GetText();
		int i = pos - 1;
		while (i > 0 && (text[i] & 0xc0)) i--;
		int j = i;
		while (i < pos)
		{
			j = i;
			i = NextPos(i);
		}
		return j;
	}
	else return pos - 1;
}

/*!
\patch 1.28 Date 10/31/2001 by Jirka
- Fixed: Clipboard operations enabled in chat line
\patch 1.30 Date 11/01/2001 by Ondra
- Fixed: Clipboard operations not working in Win9x (since 1.28)
*/
inline bool IsKey(int nVirtKey)
{
	#if !defined _WIN32 || defined _XBOX
	return false;
	#else
	return (GetKeyState(nVirtKey) & 0x80) != 0;
	#endif
}

#define DIAG_KOREAN 0

bool CEditContainer::DoKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	int n = strlen(GetText());
	Assert(_blockBegin >= 0);
	Assert(_blockBegin <= n);
	Assert(_blockEnd >= 0);
	Assert(_blockEnd <= n);
	switch (nChar)
	{
	case VK_LEFT:
		if (_blockEnd > 0)
		{
			_blockEnd = PrevPos(_blockEnd);
			if
			(
				_blockEnd > 0 &&
				(IsKey(VK_CONTROL))
			)
			{
				const char *text = GetText();
				while (true)
				{
					if
					(
						ISSPACE(text[_blockEnd - 1]) &&
						!ISSPACE(text[_blockEnd])
					) break;
					_blockEnd = PrevPos(_blockEnd);
					if (_blockEnd <= 0) break;
				}
			}
			EnsureVisible(_blockEnd);
			_enableToolip = false;
		}
		if (!IsKey(VK_SHIFT))
			_blockBegin = _blockEnd;
		return true;
	case VK_RIGHT:
		if (_blockEnd < n)
		{
			_blockEnd = NextPos(_blockEnd);
			if
			(
				_blockEnd < n &&
				(IsKey(VK_CONTROL))
			)
			{
				const char *text = GetText();
				while (true)
				{
					if
					(
						ISSPACE(text[_blockEnd - 1]) &&
						!ISSPACE(text[_blockEnd])
					) break;
					_blockEnd = NextPos(_blockEnd);
					if (_blockEnd >= n) break;
				}
			}
			EnsureVisible(_blockEnd);
			_enableToolip = false;
		}
		if (!IsKey(VK_SHIFT))
			_blockBegin = _blockEnd;
		return true;
	case VK_UP:
		if (IsMulti())
		{
			int line = CurLine();
			if (line > 0)
			{
				RString text = GetLine(line);
				int pos = _blockEnd - _lines[line];
				float x = PosToX(text, pos);
				line--;
				text = GetLine(line);
				pos = XToPos(text, x);
				_blockEnd = _lines[line] + pos;
				EnsureVisible(_blockEnd);
				_enableToolip = false;
				if (!IsKey(VK_SHIFT))
					_blockBegin = _blockEnd;
			}
			return true;
		}
		else
			return false;
	case VK_DOWN:
		if (IsMulti())
		{
			int line = CurLine();
			if (line < _lines.Size() - 1)
			{
				RString text = GetLine(line);
				int pos = _blockEnd - _lines[line];
				float x = PosToX(text, pos);
				line++;
				text = GetLine(line);
				pos = XToPos(text, x);
				_blockEnd = _lines[line] + pos;
				EnsureVisible(_blockEnd);
				_enableToolip = false;
				if (!IsKey(VK_SHIFT))
					_blockBegin = _blockEnd;
			}
			return true;
		}
		else
			return false;
	case VK_HOME:
		_blockEnd = 0;
		_enableToolip = false;
		EnsureVisible(_blockEnd);
		if (!IsKey(VK_SHIFT))
			_blockBegin = _blockEnd;
		return true;
	case VK_END:
		_blockEnd = n;
		_enableToolip = true;
		EnsureVisible(_blockEnd);
		if (!IsKey(VK_SHIFT))
			_blockBegin = _blockEnd;
		return true;
	case VK_BACK:
		if (_blockBegin != _blockEnd)
		{
			BlockDelete();
			return true;
		}
		if (_blockEnd <= 0) return true;
		_blockEnd = PrevPos(_blockEnd);
		_blockBegin = _blockEnd;
		_enableToolip = true;
		goto DeleteChar;
	case VK_DELETE:
		if (_blockBegin != _blockEnd)
		{
			if (IsKey(VK_SHIFT))
				BlockCut();
			else
				BlockDelete();
			return true;
		}
		if (_blockEnd >= n) return true;
		_enableToolip = true;
DeleteChar:
		{
			SetText
			(
				_text.Substring(0, _blockEnd) +
				_text.Substring(NextPos(_blockEnd), _text.GetLength())
			);
			// EnsureVisible is called from SetText
		}

#if DIAG_KOREAN
		RptF("KEY DOWN: BACKSPACE / DELETE");
		char buffer[1024]; buffer[0] = 0;
		for (int i=0; i<GetText().GetLength(); i++)
		{
			sprintf(buffer + strlen(buffer), "%02x ", (int)GetText()[i] & 0xff);
		}
		RptF("Whole text: %s, cursor %d - %d", buffer, _blockBegin, _blockEnd);
#endif

		return true;
	case VK_INSERT:
		if (IsKey(VK_SHIFT))
		{
			if (_blockBegin != _blockEnd) BlockDelete();
			BlockPaste();
		}
		else if (IsKey(VK_CONTROL))
			BlockCopy();
		return true;
	case VK_TAB:
	{
		if (_blockBegin!=_blockEnd || !_autoComplete || !_enableToolip) return false;
		bool certain;
		RString beg;
		RString tip = _autoComplete->Guess(_text,_blockEnd,certain,beg);
		if (tip.GetLength()<=0) return false;

		if (tip.GetLength() + _text.GetLength() <= _maxChars)
		{
			int start = _blockEnd-beg.GetLength();
			saturateMax(start,0);
			RString text =
				_text.Substring(0, start) +
				tip + 
				_text.Substring(_blockEnd, _text.GetLength());
			SetText(text);
			_blockBegin = _blockEnd = start + tip.GetLength();
			EnsureVisible(_blockEnd);
		}

		return true;
	}
		
	case 'X':
		if (IsKey(VK_CONTROL))
		{
			BlockCut();
			return true;
		}
		break;
	case 'C':
		if (IsKey(VK_CONTROL))
		{
			BlockCopy();
			return true;
		}
		break;
	case 'V':
		if (IsKey(VK_CONTROL))
		{
			if (_blockBegin != _blockEnd) BlockDelete();
			BlockPaste();
			return true;
		}
		break;
	}

	return false;
}

bool CEditContainer::DoChar(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	if (nChar >= 0x80 && GetLangID() == Korean) return true; // TODO: sound

	int n = strlen(GetText());
	if (n >= _maxChars) return true; // TODO: sound

	Assert(_blockBegin >= 0);
	Assert(_blockBegin <= n);
	Assert(_blockEnd >= 0);
	Assert(_blockEnd <= n);
	char newChar[2];
	newChar[0]=nChar;
	newChar[1]=0;

	if (_blockBegin != _blockEnd) BlockDelete();

	RString data =
	(
		GetText().Substring(0, _blockEnd) +
		RString(newChar) +
		GetText().Substring(_blockEnd, n)
	);

	SetText(data);
	_blockEnd++;
	_blockBegin = _blockEnd;
	if (_autoComplete) _autoComplete->AfterChar(_text,_blockEnd);
	_enableToolip = true;
	EnsureVisible(_blockEnd);

#if DIAG_KOREAN
	RptF("CHAR: %02x", nChar);
	char buffer[1024]; buffer[0] = 0;
	for (int i=0; i<GetText().GetLength(); i++)
	{
		sprintf(buffer + strlen(buffer), "%02x ", (int)GetText()[i] & 0xff);
	}
	RptF("Whole text: %s, cursor %d - %d", buffer, _blockBegin, _blockEnd);
#endif

	return true;
}

bool CEditContainer::DoIMEChar(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	int n = strlen(GetText());
	if (n + 2 > _maxChars) return true; // TODO: sound

	Assert(_blockBegin >= 0);
	Assert(_blockBegin <= n);
	Assert(_blockEnd >= 0);
	Assert(_blockEnd <= n);
	char newChar[3];
	newChar[0] = (nChar >> 8) & 0xff;
	newChar[1] = nChar & 0xff;
	newChar[2] = 0;

	if (_blockBegin != _blockEnd) BlockDelete();

	RString data =
	(
		GetText().Substring(0, _blockEnd) +
		RString(newChar) +
		GetText().Substring(_blockEnd, n)
	);

	SetText(data);
	_blockEnd += 2;
	_blockBegin = _blockEnd;
	if (_autoComplete) _autoComplete->AfterChar(_text,_blockEnd);
	_enableToolip = true;
	EnsureVisible(_blockEnd);

#if DIAG_KOREAN
	RptF("IME CHAR: %02x %02x", (int)newChar[0] & 0xff, (int)newChar[1] & 0xff);
	char buffer[1024]; buffer[0] = 0;
	for (int i=0; i<GetText().GetLength(); i++)
	{
		sprintf(buffer + strlen(buffer), "%02x ", (int)GetText()[i] & 0xff);
	}
	RptF("Whole text: %s, cursor %d - %d", buffer, _blockBegin, _blockEnd);
#endif

	return true;
}

bool CEditContainer::DoIMEComposition(unsigned nChar, unsigned nFlags)
{
	int n = strlen(GetText());
	if (nChar != 0 && n + 2 > _maxChars) return true; // TODO: sound

	Assert(_blockBegin >= 0);
	Assert(_blockBegin <= n);
	Assert(_blockEnd >= 0);
	Assert(_blockEnd <= n);

	if (_blockBegin != _blockEnd) BlockDelete();
	if (nChar == 0)
	{

#if DIAG_KOREAN
		RptF("IME COMPOSITION: 00 00");
		char buffer[1024]; buffer[0] = 0;
		for (int i=0; i<GetText().GetLength(); i++)
		{
			sprintf(buffer + strlen(buffer), "%02x ", (int)GetText()[i] & 0xff);
		}
		RptF("Whole text: %s, cursor %d - %d", buffer, _blockBegin, _blockEnd);
#endif

		return true;
	}

	char newChar[3];
	newChar[0] = (nChar >> 8) & 0xff;
	newChar[1] = nChar & 0xff;
	newChar[2] = 0;

	RString data =
	(
		GetText().Substring(0, _blockEnd) +
		RString(newChar) +
		GetText().Substring(_blockEnd, n)
	);

	SetText(data);
	_blockBegin = _blockEnd;
	_blockEnd += 2;
	if (_autoComplete) _autoComplete->AfterChar(_text,_blockEnd);
	_enableToolip = true;
	EnsureVisible(_blockEnd);

#if DIAG_KOREAN
	RptF("IME COMPOSITION: %02x %02x", (int)newChar[0] & 0xff, (int)newChar[1] & 0xff);
	char buffer[1024]; buffer[0] = 0;
	for (int i=0; i<GetText().GetLength(); i++)
	{
		sprintf(buffer + strlen(buffer), "%02x ", (int)GetText()[i] & 0xff);
	}
	RptF("Whole text: %s, cursor %d - %d", buffer, _blockBegin, _blockEnd);
#endif

	return true;
}

int CEditContainer::CurLine() const
{
	int n = _lines.Size();
	for (int i=1; i<n; i++)
		if (_blockEnd < _lines[i])
		{
			return i - 1;
		}
	return n - 1;
}

int CEditContainer::FirstLine() const
{
	int n = _lines.Size();
	for (int i=1; i<n; i++)
		if (_firstVisible < _lines[i])
		{
			return i - 1;
		}
	return n - 1;
}

RString CEditContainer::GetLine(int i) const
{
	if (i < 0) return "";
	//Assert((_style & ST_TYPE) == ST_MULTI);
	Assert(i < _lines.Size());

	int from = _lines[i];
	int to = i + 1 < _lines.Size() ? _lines[i + 1] : _text.GetLength();
	return _text.Substring(from, to);
}

void CEditContainer::BlockDelete()
{
	if (_blockBegin == _blockEnd) return;

	int from, to;
	if (_blockBegin < _blockEnd)
	{
		from = _blockBegin;
		to = _blockEnd;
	}
	else
	{
		from = _blockEnd;
		to = _blockBegin;
	}
	RString text = _text.Substring(0, from) + _text.Substring(to, _text.GetLength());
	_blockBegin = _blockEnd = from;
	SetText(text);
}

/*!
\patch_internal 1.44 Date 2/11/2002 by Ondra
- Fixed: Windows clipboard copy sometimes did not work correctly
when non-text data were already stored in cliboard.
*/

void CEditContainer::BlockCopy()
{
	if (_blockBegin == _blockEnd) return;

	int from, to;
	if (_blockBegin < _blockEnd)
	{
		from = _blockBegin;
		to = _blockEnd;
	}
	else
	{
		from = _blockEnd;
		to = _blockBegin;
	}
	RString text = _text.Substring(from, to);

	#if defined _WIN32 && !defined _XBOX
	extern HWND hwndApp;
	if (OpenClipboard(hwndApp))
	{
		EmptyClipboard();
		int n = text.GetLength();
		HANDLE handle = GlobalAlloc(GMEM_MOVEABLE | GMEM_DDESHARE, n + 1);
		if (handle)
		{
			char *mem = (char *)GlobalLock(handle);
			if (mem)
			{
				strcpy(mem, text);
				mem[n] = 0;
			}
			GlobalUnlock(handle);
			SetClipboardData(CF_TEXT,handle);
		}
		CloseClipboard();
	}
	#endif
}

void CEditContainer::BlockCut()
{
	BlockCopy();
	BlockDelete();
}

/*!
\patch 1.23 Date 9/17/2001 by Jirka
- Fixed: Do not allow paste End of Line character in edit control.
*/
void CEditContainer::BlockPaste()
{
	#if defined _WIN32 && !defined _XBOX
	extern HWND hwndApp;
	if (OpenClipboard(hwndApp))
	{
		HANDLE handle = GetClipboardData(CF_TEXT);
		if (handle)
		{
			char *mem = (char *)GlobalLock(handle);
			if (mem)
			{
				int n = strlen(mem);
				for (int i=0; i<n; i++)
				{
					switch (mem[i])
					{
					case '\n':
					case '\r':
					case '\t':
						mem[i] = ' ';
						break;
					}
				}
				if (n + _text.GetLength() <= _maxChars)
				{
					RString text =
						_text.Substring(0, _blockEnd) +
						RString(mem) + 
						_text.Substring(_blockEnd, _text.GetLength());
					SetText(text);
					_blockBegin = _blockEnd = _blockEnd + n;
					EnsureVisible(_blockEnd);
				}
			} 
			GlobalUnlock(handle);
		}
		CloseClipboard();
	}
	#endif
}

///////////////////////////////////////////////////////////////////////////////
// class CEdit

CEdit::CEdit(ControlsContainer *parent, int idc, const ParamEntry &cls)
	: Control(parent, CT_EDIT, idc, cls), CEditContainer(cls)
{
	const ParamEntry *entry = cls.FindEntry("size");
	if (entry) _size = (float)(*entry) * _font->Height();
	else _size = cls >> "sizeEx";

	RString text = cls>>"text";
	SetText(text);
	// note: SetText does EnsureVisible
}

bool CEdit::IsMulti() const
{
	return (_style & ST_TYPE) == ST_MULTI;
}

float CEdit::PosToX(RString text, int pos) const
{
	float left;
	int style = ST_LEFT;
	if ((_style & ST_TYPE) == ST_MULTI || _firstVisible == 0)
		style = _style & ST_HPOS;
	float size = _scale * _size;
	switch (style)
	{
		case ST_RIGHT:
			left = _x + _w - GLOB_ENGINE->GetTextWidth(size, _font, text) - _scale * textBorder;
			break;
		case ST_CENTER:
			left = _x + 0.5 * (_w - GLOB_ENGINE->GetTextWidth(size, _font, text));
			break;
		default:
			Assert((_style & ST_HPOS) == ST_LEFT)
			left = _x + _scale * textBorder;
			break;
	}

	RString str = text.Substring(0, pos);
	return left + GLOB_ENGINE->GetTextWidth(size, _font, str);
}

int CEdit::XToPos(RString text, float x) const
{
	float left;
	int style = ST_LEFT;
	if ((_style & ST_TYPE) == ST_MULTI || _firstVisible == 0)
		style = _style & ST_HPOS;
	float size = _scale * _size;
	switch (style)
	{
		case ST_RIGHT:
			left = _x + _w - GLOB_ENGINE->GetTextWidth(size, _font, text) - _scale * textBorder;
			break;
		case ST_CENTER:
			left = _x + 0.5 * (_w - GLOB_ENGINE->GetTextWidth(size, _font, text));
			break;
		default:
			Assert((_style & ST_HPOS) == ST_LEFT)
			left = _x + _scale * textBorder;
			break;
	}

	if (x < left) return 0;
	
	int n = strlen(text);
	for (int i=0; i<n; i++)
	{
		int j = i;
		char c = text[i];
		float cw;
		if ((c & 0x80) && _font->GetLangID() == Korean && i + 1 < n)
		{
			char c2 = text[++i];
			char temp[3]; temp[0] = c; temp[1] = c2; temp[2] = 0;
			cw = GLOB_ENGINE->GetTextWidth(size, _font, temp);
		}
		else
		{
			char temp[2]; temp[0] = c; temp[1] = 0;
			cw = GLOB_ENGINE->GetTextWidth(size, _font, temp);
		}
		if (left + cw > x)
		{
			if (x - left < left + cw - x)
				return j;
			else
				return i + 1;
		}
		left += cw;
	}
	return n;
}

void CEdit::DrawText(const char *text, int offset, float top, float alpha)
{
	int pos = ST_LEFT;
	if ((_style & ST_TYPE) == ST_MULTI || _firstVisible == 0)
		pos = _style & ST_HPOS;
	float size = _scale * _size;
	float left;
	switch (pos)
	{
		case ST_RIGHT:
			left = _x + _w - GLOB_ENGINE->GetTextWidth(size, _font, text) - _scale * textBorder;
			break;
		case ST_CENTER:
			left = _x + 0.5 * (_w - GLOB_ENGINE->GetTextWidth(size, _font, text));
			break;
		default:
			Assert(pos == ST_LEFT)
			left = _x + _scale * textBorder;
			break;
	}
	
	if (_blockBegin != _blockEnd)
	{
		int from, to;
		if (_blockBegin < _blockEnd)
		{
			from = _blockBegin;
			to = _blockEnd;
		}
		else
		{
			from = _blockEnd;
			to = _blockBegin;
		}
		if (from < offset + strlen(text) && to > offset)
		{
			PackedColor selColor = ModAlpha(_selColor, alpha);
			float x1 = 0;
//			char buffer[1024];
			if (from > offset)
			{
				RString prefix(text, from - offset);
				x1 = GLOB_ENGINE->GetTextWidth(size, _font, prefix);
			}
			float x2 = 0;
			if (to < offset + strlen(text))
			{
				RString prefix(text, to - offset);
				x2 = GLOB_ENGINE->GetTextWidth(size, _font, prefix);
			}
			else
				x2 = GLOB_ENGINE->GetTextWidth(size, _font, text);

			const int w = GLOB_ENGINE->Width2D();
			const int h = GLOB_ENGINE->Height2D();

			MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
			GLOB_ENGINE->Draw2D
			(
				mip, selColor,
				Rect2DPixel((left + x1) * w, top * h, (x2 - x1) * w, size * h),
				Rect2DPixel((_x + _scale * textBorder) * w, _y * h,
				(_w - 2.0 * _scale * textBorder) * w, _h * h)
			);
		}
	}


	PackedColor ftColor = ModAlpha(_ftColor, alpha);
	GLOB_ENGINE->DrawText
	(
		Point2DFloat(left, top), size,
		Rect2DFloat(_x + _scale * textBorder, _y,
		_w - 2.0 * _scale * textBorder, _h),
		_font, ftColor, text
	);
}

void CEdit::OnDraw(float alpha)
{
	const int w = GLOB_ENGINE->Width2D();
	const int h = GLOB_ENGINE->Height2D();

	float xx = toInt(_x * w) + 0.5;
	float yy = toInt(_y * h) + 0.5;
	float ww = toInt(_w * w);
	float hh = toInt(_h * h);

	PackedColor ftColor = ModAlpha(_ftColor, alpha);

	DrawLeft(0, ftColor);
	DrawTop(0, ftColor);
	DrawBottom(0, ftColor);
	DrawRight(0, ftColor);
	
	float size = _scale * _size;
	float height = size;

	if (_text.GetLength() > 0)
	{
		if ((_style & ST_TYPE) == ST_MULTI)
		{
			float top = _y;
			for (int i=FirstLine(); i<_lines.Size(); i++)
			{
				DrawText(GetLine(i), _lines[i], top, alpha);
				top += height;
				if (top >= _y + _h) break;
			}
		}
		else
		{
			float top = _y + 0.5 * (_h - height);
			DrawText
			(
				_text.Substring(_firstVisible, _text.GetLength()),
				_firstVisible,
				top, alpha
			);
		}
	}

	// draw caret
	if (!IsFocused()) return;
	float top, left;
	if ((_style & ST_TYPE) == ST_MULTI)
	{
		int cur = CurLine();
		int first = FirstLine();
		top = _y + (cur - first) * height;
		left = CX(PosToX
		(
			GetLine(cur),
			_blockEnd - (cur < 0 ? 0 : _lines[cur])
		));
	}
	else
	{
		top = _y + 0.5 * (_h - height);
		left = CX(PosToX
		(
			_text.Substring(_firstVisible, _text.GetLength()),
			_blockEnd - _firstVisible
		));
	}
	bool state = ((Glob.uiTime.toFloat() - toIntFloor(Glob.uiTime.toFloat())) < 0.5);
	if (state)
	{
		GEngine->DrawLine
		(
			Line2DPixel(left, CY(top), left, CY(top + height)),
			ftColor, ftColor
		);
	}
	// draw autocomplete
	if (_blockBegin==_blockEnd && _autoComplete && _enableToolip)
	{
		bool certain = false;
		RString beg;
		RString tip = _autoComplete->Guess(_text,_blockEnd, certain, beg);
		if (tip.GetLength() > 0)
		{
			int w = GEngine->Width2D();
			int h = GEngine->Height2D();

			float x = left/w;
			float y = top;
			const float border = 0.005;
			float height = _tooltipFont->Height();
			float width = GEngine->GetTextWidth(_tooltipSize, _tooltipFont, tip);
			float begW = GEngine->GetTextWidth(_tooltipSize, _tooltipFont, beg);

			PackedColor shade(Color(0, 0, 0, 0.3));
			PackedColor colorText = PackedWhite;
			PackedColor color(Color(1, 1, 1, 0.3));
			if (!certain)
			{
				colorText = PackedBlack;
				shade = PackedColor(Color(0.95,0.95,0.95, 0.3));
			}
			
			
			float bx = x - begW;
	//		float by = y - 0.5 * height - border;
			float by = y - height - 2.0 * border;
			float ex = bx + width + 2.0 * border;
			float ey = by + height + 2.0 * border;

			MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
			GEngine->Draw2D(mip, shade, Rect2DPixel(bx * w, by * h, (ex - bx) * w, (ey - by) * h));
			GEngine->DrawText(Point2DFloat(bx + border, by + border), _tooltipSize,	_tooltipFont, colorText, tip);
			GEngine->DrawLine(Line2DPixel(bx * w, by * h, ex * w, by * h), color, color);
			GEngine->DrawLine(Line2DPixel(ex * w, by * h, ex * w, ey * h), color, color);
			GEngine->DrawLine(Line2DPixel(ex * w, ey * h, bx * w, ey * h), color, color);
			GEngine->DrawLine(Line2DPixel(bx * w, ey * h, bx * w, by * h), color, color);
		}
	}

}

bool CEdit::OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	return CEditContainer::DoKeyDown(nChar, nRepCnt, nFlags);
}

bool CEdit::OnChar(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	return CEditContainer::DoChar(nChar, nRepCnt, nFlags);
}

bool CEdit::OnIMEChar(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	return CEditContainer::DoIMEChar(nChar, nRepCnt, nFlags);
}

bool CEdit::OnIMEComposition(unsigned nChar, unsigned nFlags)
{
	return CEditContainer::DoIMEComposition(nChar, nFlags);
}

int CEdit::FindPos(float x, float y)
{
	if ((_style & ST_TYPE) == ST_MULTI)
	{
		if (_text.GetLength() == 0)
		{
			return 0;
		}
		else
		{
			float size = _scale * _size;
			float height = size;
			int line = FirstLine() + toIntFloor((y - _y) / height);
			saturate(line, 0, _lines.Size() - 1);
			return _lines[line] + XToPos
			(
				GetLine(line), x
			);
		}
	}
	else
	{
		return _firstVisible + XToPos
		(
			_text.Substring(_firstVisible, _text.GetLength()), x
		);
	}
}

void CEdit::OnLButtonDown(float x, float y)
{
	int pos = FindPos(x, y);
	if (!GInput.keys[DIK_LSHIFT] && !GInput.keys[DIK_RSHIFT])
	{
		_blockBegin = pos;
	}
	_blockEnd = pos;
	EnsureVisible(_blockEnd);
}

void CEdit::OnLButtonUp(float x, float y)
{
/*
	_blockEnd = FindPos(x, y);
	EnsureVisible(_blockEnd);
*/
}

void CEdit::OnMouseMove(float x, float y, bool active)
{
	if (GInput.mouseL)
	{
		_blockEnd = FindPos(x, y);
		EnsureVisible(_blockEnd);
	}
}

void CEdit::EnsureVisible(int pos)
{
	float size = _scale * _size;

	if ((_style & ST_TYPE) == ST_MULTI)
	{
		if (_text.GetLength() == 0)
		{
			_firstVisible = 0;
			return;
		}

		int cur = CurLine();
		int first = FirstLine();
		saturateMin(first, cur);

		float height = size;
		int maxlines = toIntFloor(_h / height);
		
		int lines = _lines.Size() - first;
		if (lines <= maxlines)
		{
			first += lines - maxlines;
			saturateMax(first, 0);
		}
		else if (first + maxlines <= cur)
		{
			first = cur - maxlines + 1;
		}
		_firstVisible = _lines[first];
	}
	else
	{
		saturateMin(_firstVisible, _blockEnd);
		float wlimit = _w - 2.0 * _scale * textBorder; 
		float w = GLOB_ENGINE->GetTextWidth
		(
			size, _font,
			_text.Substring(_firstVisible, _text.GetLength())
		);
		if (w <= wlimit)
		{
			while (_firstVisible > 0)
			{
				int prev = PrevPos(_firstVisible);
				w += GLOB_ENGINE->GetTextWidth
				(
					size, _font,
					_text.Substring(prev, _firstVisible)
				);
				if (w > wlimit) break;
				_firstVisible = prev;
			}
		}
		else
		{
			w = GLOB_ENGINE->GetTextWidth
			(
				size, _font,
				_text.Substring(_firstVisible, _blockEnd)
			);
			while (w > wlimit)
			{
				int prev = _firstVisible;
				_firstVisible = NextPos(_firstVisible);
				w -= GLOB_ENGINE->GetTextWidth
				(
					size, _font,
					_text.Substring(prev, _firstVisible)
				);
			}
		}
	}
}

void CEdit::FormatText()
{
	if ((_style & ST_TYPE) != ST_MULTI) return;

	_lines.Clear();

	float lineWidth = _w - 2 * _scale * textBorder;
	float size = _scale * _size;

	_lines.Add(0);

	const char *p = _text;
	while (*p != 0)
	{
		// begin of the line
		const char *word = p;
		int n = 0;
		float width = 0;
		while (true)
		{
			const char *q = p;
			char c = *p++;
			if (c == 0) return;
			if (ISSPACE(c))
			{
				n++;
				word = p;
			}

			if ((c & 0x80) && _font->GetLangID() == Korean && *p != 0)
			{
				// TODO: simplify
				char temp[3]; temp[0] = c; temp[1] = *p++; temp[2] = 0;
				width += GEngine->GetTextWidth(size, _font, temp);
			}
			else
			{
				// TODO: simplify
				char temp[2]; temp[0] = c; temp[1] = 0;
				width += GLOB_ENGINE->GetTextWidth(size, _font, temp);
			}

			if (width > lineWidth)
			{
				if (n > 0)
				{
					p = word;
				}
				else
				{
					p = q;
				}
				_lines.Add(p - (const char *)_text);
				break;
			}
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
// class CButton

CButton::CButton(ControlsContainer *parent, const ParamEntry &cls, float x, float y, float w, float h)
	: Control(parent, CT_BUTTON, cls >> "idc", cls >> "style", x, y, w, h)
{
	_text = cls>>"text";
	_ftColor = GetPackedColor(cls >> "colorText");
	_font = GLOB_ENGINE->LoadFont(GetFontID(cls>>"font"));
	const ParamEntry *entry = cls.FindEntry("size");
	if (entry) _size = (float)(*entry) * _font->Height();
	else _size = cls >> "sizeEx";
	_state = false;

	GetValue(_pushSound, cls >> "soundPush");
	GetValue(_clickSound, cls >> "soundClick");
	GetValue(_escapeSound, cls >> "soundEscape");

	InitColors();

	entry = cls.FindEntry("action");
	if (entry) _action = *entry;
}

CButton::CButton(ControlsContainer *parent, int idc, const ParamEntry &cls)
	: Control(parent, CT_BUTTON, idc, cls)
{
	_text = cls>>"text";
	_ftColor = GetPackedColor(cls >> "colorText");
	_font = GLOB_ENGINE->LoadFont(GetFontID(cls>>"font"));
	const ParamEntry *entry = cls.FindEntry("size");
	if (entry) _size = (float)(*entry) * _font->Height();
	else _size = cls >> "sizeEx";
	_state = false;

	GetValue(_pushSound, cls >> "soundPush");
	GetValue(_clickSound, cls >> "soundClick");
	GetValue(_escapeSound, cls >> "soundEscape");

	InitColors();

	entry = cls.FindEntry("action");
	if (entry) _action = *entry;
}

void CButton::InitColors()
{
	const ParamEntry &clsButton = Pars >> "CfgWrapperUI" >> "Button";
	_color1 = GetPackedColor(clsButton >> "color1");
	_color2 = GetPackedColor(clsButton >> "color2");
	_color3 = GetPackedColor(clsButton >> "color3");
	_color4 = GetPackedColor(clsButton >> "color4");
	_color5 = GetPackedColor(clsButton >> "color5");
}

bool CButton::OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	if (nChar == VK_RETURN)
	{
		if (!_state)
		{
			_state = true;
			PlaySound(_pushSound);
		}
		return true;
	}
	return false;
}

bool CButton::OnKeyUp(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	if (nChar == VK_RETURN)
	{
		_state = false;
		PlaySound(_clickSound);
		if (_action.GetLength() > 0)
		{
			GameState *gstate = GWorld->GetGameState();
			gstate->Execute(_action);
			GWorld->SimulateScripts();
		}
		OnClicked();
		_parent->OnButtonClicked(IDC());
		return true;
	}
	return false;
}

void CButton::OnLButtonDown(float x, float y)
{
	_state = true;
	PlaySound(_pushSound);
}

void CButton::OnLButtonUp(float x, float y)
{
	_state = false;
	if (IsInside(x, y))
	{
		PlaySound(_clickSound);
		if (_action.GetLength() > 0)
		{
			GameState *gstate = GWorld->GetGameState();
			gstate->Execute(_action);
			GWorld->SimulateScripts();
		}
		OnClicked();
		_parent->OnButtonClicked(IDC());
	}
	else
	{
		PlaySound(_escapeSound);
	}
}

void CButton::OnDraw(float alpha)
{
	const int w = GLOB_ENGINE->Width2D();
	const int h = GLOB_ENGINE->Height2D();

	float xx = toInt(_x * w) + 0.5;
	float yy = toInt(_y * h) + 0.5;
	float ww = toInt(_w * w);
	float hh = toInt(_h * h);

	int pos = _style & ST_HPOS;
	bool selected = IsFocused() || IsDefault() && !_parent->GetFocused()->CanBeDefault();
	bool pushed = IsEnabled() && _state;

	float offsetX = pushed ? 0.003 : 0;
	float offsetY = pushed ? 0.004 : 0;

	PackedColor color1 = ModAlpha(_color1, alpha);
	PackedColor color2 = ModAlpha(_color2, alpha);
	PackedColor color3 = ModAlpha(_color3, alpha);
	PackedColor color4 = ModAlpha(_color4, alpha);
	PackedColor color5 = ModAlpha(_color5, alpha);
	PackedColor ftColor = ModAlpha(_ftColor, alpha);

	if (pushed)
	{
		DrawLeft(0, color1);
		DrawTop(0, color1);
		DrawLeft(1, color2);
		DrawTop(1, color2);
		DrawBottom(1, color4);
		DrawRight(1, color4);
		DrawBottom(0, color5);
		DrawRight(0, color5);
	}
	else
	{
		DrawLeft(0, color5);
		DrawTop(0, color5);
		DrawLeft(1, color4);
		DrawTop(1, color4);
		DrawBottom(1, color2);
		DrawRight(1, color2);
		DrawBottom(0, color1);
		DrawRight(0, color1);
	}

	Texture *background = GLOB_SCENE->Preloaded(TextureWhite);
	MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(background, 0, 0);
	GLOB_ENGINE->Draw2D
	(
		mip, color3, Rect2DPixel(xx + 2, yy + 2, ww - 4, hh - 4)
	);
	//GLOB_ENGINE->TextBank()->ReleaseMipmap();

	if (selected)
	{
		float ox = toInt(offsetX * w);
		float oy = toInt(offsetY * h);
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel(xx + 6 + ox, yy + 6 + oy, xx + ww - 7 + ox, yy + 6 + oy),
			ftColor, ftColor
		);
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel(xx + ww - 7 + ox, yy + 6 + oy, xx + ww - 7 + ox, yy + hh - 7 + oy),
			ftColor, ftColor
		);
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel(xx + ww - 7 + ox, yy + hh - 7 + oy, xx + 6 + ox, yy + hh - 7 + oy),
			ftColor, ftColor
		);
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel(xx + 6 + ox, yy + hh - 7 + oy, xx + 6 + ox, yy + 6 + oy),
			ftColor, ftColor
		);
	}

	float left, top;
	switch (pos)
	{
		case ST_RIGHT:
			left = _x + _w  - GLOB_ENGINE->GetTextWidth(_size, _font, _text);
			break;
		case ST_CENTER:
			left = _x + 0.5 * (_w - GLOB_ENGINE->GetTextWidth(_size, _font, _text));
			break;
		default:
			Assert(pos == ST_LEFT)
			left = _x;
			break;
	}
	top = _y + 0.5 * (_h - _size);
	GLOB_ENGINE->DrawText
	(
		Point2DFloat(left + offsetX, top + offsetY), _size,
		Rect2DFloat(_x, _y, _w, _h),
		_font, ftColor, _text
	);
}

void CButton::OnClicked()
{
}

///////////////////////////////////////////////////////////////////////////////
// class CActiveText

CActiveText::CActiveText(ControlsContainer *parent, int idc, const ParamEntry &cls)
	: Control(parent, CT_ACTIVETEXT, idc, cls)
{
	_text = cls>>"text";
	_font = GLOB_ENGINE->LoadFont(GetFontID(cls>>"font"));
	const ParamEntry *entry = cls.FindEntry("size");
	if (entry) _size = (float)(*entry) * _font->Height();
	else _size = cls >> "sizeEx";
	_color = GetPackedColor(cls >> "color");
	_colorActive = GetPackedColor(cls >> "colorActive");

	GetValue(_enterSound, cls >> "soundEnter");
	GetValue(_pushSound, cls >> "soundPush");
	GetValue(_clickSound, cls >> "soundClick");
	GetValue(_escapeSound, cls >> "soundEscape");

	_active = false;

	entry = cls.FindEntry("action");
	if (entry) _action = *entry;
}

bool CActiveText::OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	if (nChar == VK_RETURN)
	{
		if (IsEnabled())
		{
			PlaySound(_pushSound);
			return true;
		}
	}
	return false;
}

bool CActiveText::OnKeyUp(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	if (nChar == VK_RETURN)
	{
		if (IsEnabled())
		{
			PlaySound(_clickSound);
			if (_action.GetLength() > 0)
			{
				GameState *gstate = GWorld->GetGameState();
				gstate->Execute(_action);
				GWorld->SimulateScripts();
			}
			_parent->OnButtonClicked(IDC());
			return true;
		}
	}
	return false;
}

void CActiveText::OnMouseMove(float x, float y, bool active)
{
	OnMouseHold(x, y, active);
}

void CActiveText::OnMouseHold(float x, float y, bool active)
{
	bool newActive = active && IsInside(x, y);
	if (newActive && !_active) PlaySound(_enterSound);
	_active = newActive;
}

void CActiveText::OnLButtonDown(float x, float y)
{
	PlaySound(_pushSound);
}

void CActiveText::OnLButtonUp(float x, float y)
{
	if (IsInside(x, y))
	{
		PlaySound(_clickSound);
		if (_action.GetLength() > 0)
		{
			GameState *gstate = GWorld->GetGameState();
			gstate->Execute(_action);
			GWorld->SimulateScripts();
		}
		_parent->OnButtonClicked(IDC());
	}
	else
	{
		PlaySound(_escapeSound);
	}
}

void CActiveText::OnDraw(float alpha)
{
	float size = _scale * _size;
	bool vertical = false;
	float height = size;
	float left = _x + 0.5 * (_w - height);
	float top = _y + 0.5 * (_h - height);
	float wText = GLOB_ENGINE->GetTextWidth(size, _font, _text);
	switch (_style & ST_HPOS)
	{
		case ST_UP:
			top = _y;
			vertical = true;
			break;
		case ST_VCENTER:
			top = _y + 0.5 * (_h - wText);
			vertical = true;
			break;
		case ST_DOWN:
			top = _y + _h - wText;
			vertical = true;
			break;
		case ST_RIGHT:
			left = _x + _w  - wText;
			break;
		case ST_CENTER:
			left = _x + 0.5 * (_w - wText);
			break;
		default:
			Assert((_style & ST_HPOS) == ST_LEFT)
			left = _x;
			break;
	}
	PackedColor color = ModAlpha(_active ? _colorActive : _color, alpha);
	if (!IsEnabled()) color = ModAlpha(color, 0.25);
	bool focused = IsFocused();
	bool selected = IsDefault() && !_parent->GetFocused()->CanBeDefault();
	const float w = GLOB_ENGINE->Width2D();
	const float h = GLOB_ENGINE->Height2D();
	if (_text.GetLength() > 0)
	{
		if (vertical)
		{
			if (focused || selected)
			{
				PackedColor col;
				if (focused) col = color;
				else col = ModAlpha(color, 0.5);
				GEngine->DrawLine
				(
					Line2DPixel(_x * w, top * h, _x * w, top * h + wText * w),
					col, col
				);
			}
			GLOB_ENGINE->DrawTextVertical
			(
				Point2DFloat(left, top), size,
				Rect2DFloat(_x, _y,	_w, _h),
				_font, color, _text
			);
		}
		else
		{
			if (focused || selected)
			{
				PackedColor col;
				if (focused) col = color;
				else col = ModAlpha(color, 0.5);
				GEngine->DrawLine
				(
					Line2DPixel(left * w, (_y + _h) * h, (left + wText) * w, (_y + _h) * h),
					col, col
				);
			}
			GLOB_ENGINE->DrawText
			(
				Point2DFloat(left, top), size,
				Rect2DFloat(_x, _y,	_w, _h),
				_font, color, _text
			);
		}
	}
	else
	{
		if (selected)
		{
			GEngine->DrawLine
			(
				Line2DPixel(_x * w, _y * h, _x * w, (_y + _h) * h),
				color, color
			);
			GEngine->DrawLine
			(
				Line2DPixel(_x * w, (_y + _h) * h, (_x + _w) * w, (_y + _h) * h),
				color, color
			);
			GEngine->DrawLine
			(
				Line2DPixel((_x + _w) * w, (_y + _h) * h, (_x + _w) * w, _y * h),
				color, color
			);
			GEngine->DrawLine
			(
				Line2DPixel((_x + _w) * w, _y * h, _x * w, _y * h),
				color, color
			);
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
// class CToolBox

CToolBox::CToolBox(ControlsContainer *parent, int idc, const ParamEntry &cls)
	: Control(parent, CT_TOOLBOX, idc, cls)
{
	int i, n = (cls>>"strings").GetSize();
	_strings.Resize(n);
	for (i=0; i<n; i++) _strings[i] = (cls>>"strings")[i];

	_rows = cls>>"rows";
	_columns = cls>>"columns";

	_ftColor = GetPackedColor(cls >> "colorText");
	_bgColor = GetPackedColor(cls >> "color");
	_ftSelectColor = GetPackedColor(cls >> "colorTextSelect");
	_bgSelectColor = GetPackedColor(cls >> "colorSelect");
	_ftDisabledColor = GetPackedColor(cls >> "colorTextDisable");
	_bgDisabledColor = GetPackedColor(cls >> "colorDisable");
	_font = GLOB_ENGINE->LoadFont(GetFontID(cls>>"font"));
	const ParamEntry *entry = cls.FindEntry("size");
	if (entry) _size = (float)(*entry) * _font->Height();
	else _size = cls >> "sizeEx";

	SetCurSel(0);
}

bool CToolBox::IsSelected(int i) const
{
	return i == _selected;
}

void CToolBox::ChangeSelection(int i)
{
	SetCurSel(i);
	if (_parent) _parent->OnToolBoxSelChanged(_idc, _selected);
}

bool CToolBox::OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	switch (nChar)
	{
	case VK_LEFT:
	case VK_UP:
		ChangeSelection(_selected - 1);
		return true;
	case VK_RIGHT:
	case VK_DOWN:
		ChangeSelection(_selected + 1);
		return true;
	}
	return false;
}

void CToolBox::OnLButtonDown(float x, float y)
{
	float w = _w / _columns;
	float h = _h / _rows;

	int c = toIntFloor((x - _x) / w);
	int r = toIntFloor((y - _y) / h);
	if (c < 0 || c >= _columns) return;
	if (r < 0 || r >= _rows) return;

	int i = r * _columns + c;
	if (i >= 0 && i < _strings.Size())
	{
		ChangeSelection(i);
	}
}

void CToolBox::OnDraw(float alpha)
{
	PackedColor colorBg;
	if (!IsEnabled())
	{
		colorBg = ModAlpha(_bgDisabledColor, alpha);
	}
	else if (IsFocused())
	{
		colorBg = ModAlpha(_bgSelectColor, alpha);
	}
	else
	{
		colorBg = ModAlpha(_bgColor, alpha);
	}
	float wScr = GLOB_ENGINE->Width2D();
	float hScr = GLOB_ENGINE->Height2D();

	float w = _w / _columns;
	float h = _h / _rows;

	const float border = 0.005;
	int sel = GetCurSel();
	int row = sel / _columns;
	int col = sel % _columns;
	float yy = _y + row * h + border;
	float xx = _x + col * w + border;
	float ww = w - 2 * border;
	float hh = h - 2 * border;
	GLOB_ENGINE->DrawLine
	(
		Line2DPixel(wScr * xx, hScr * yy, wScr * (xx + ww), hScr * yy),
		colorBg, colorBg
	);
	GLOB_ENGINE->DrawLine
	(
		Line2DPixel(wScr * (xx + ww), hScr * yy, wScr * (xx + ww), hScr * (yy + hh)),
		colorBg, colorBg
	);
	GLOB_ENGINE->DrawLine
	(
		Line2DPixel(wScr * (xx + ww), hScr * (yy + hh), wScr * xx, hScr * (yy + hh)),
		colorBg, colorBg
	);
	GLOB_ENGINE->DrawLine
	(
		Line2DPixel(wScr * xx, hScr * (yy + hh), wScr * xx, hScr * yy),
		colorBg, colorBg
	);
	
	
	float hText = _size;

	int i = 0, n = _strings.Size();
	for (int y=0; y<_rows; y++)
		for (int x=0; x<_columns; x++)
		{
			if (i >= n) return;

			float leftBound = _x + x * w;
			float left = leftBound;
			switch (_style & ST_HPOS)
			{
				case ST_RIGHT:
					left += w - GLOB_ENGINE->GetTextWidth(_size, _font, _strings[i]);
					break;
				case ST_CENTER:
					left += 0.5 * (w - GLOB_ENGINE->GetTextWidth(_size, _font, _strings[i]));
					break;
				default:
					Assert((_style & ST_HPOS) == ST_LEFT)
					break;
			}
			float topBound = _y + y * h;
			float top = topBound + 0.5 * (h - hText);

			PackedColor colorFt;
			if (IsSelected(i))
				colorFt = ModAlpha(_ftSelectColor, alpha);
			else
				colorFt = ModAlpha(_ftColor, alpha);
			GLOB_ENGINE->DrawText
			(
				Point2DFloat(left, top), _size,
				Rect2DFloat(leftBound, topBound, w, h),
				_font, colorFt, _strings[i]
			);
			i++;
		}
}

///////////////////////////////////////////////////////////////////////////////
// class CCheckBoxes

CCheckBoxes::CCheckBoxes(ControlsContainer *parent, int idc, const ParamEntry &cls)
: CToolBox(parent, idc, cls)
{
	_type = CT_CHECKBOXES;
}

bool CCheckBoxes::OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	switch (nChar)
	{
	case VK_LEFT:
	case VK_UP:
		SetCurSel(_selected - 1);
		return true;
	case VK_RIGHT:
	case VK_DOWN:
		SetCurSel(_selected + 1);
		return true;
	case VK_SPACE:
		ChangeSelection(_selected);
		return true;
	}
	return false;
}

bool CCheckBoxes::IsSelected(int i) const
{
	return GetArray().Get(i);
}

void CCheckBoxes::ChangeSelection(int i)
{
	_array.Toggle(i);
	SetCurSel(i);
	if (_parent) _parent->OnCheckBoxesSelChanged(_idc, _selected, _array.Get(i));
}

///////////////////////////////////////////////////////////////////////////////
// class CSlider

CSliderContainer::CSliderContainer(const ParamEntry &cls)
{
	SetRange(0, 10);
	SetThumbPos(0);
	SetSpeed(1, 3);
	_thumbLocked = false;

	_color = GetPackedColor(cls >> "color");
}

CSlider::CSlider(ControlsContainer *parent, int idc, const ParamEntry &cls)
	: Control(parent, CT_SLIDER, idc, cls), CSliderContainer(cls)
{
}

void CSlider::OnLButtonDown(float x, float y)
{
	if ((_style & SL_DIR) == SL_VERT)
	{
		float spinHeight = (1.33 * 0.6) * _w;
		const float thumbHeight = 0.02;
		float top = _y + spinHeight + 0.5 * thumbHeight;
		float fieldHeight = _h - 2 * spinHeight - thumbHeight;

		float coef = 0;
		if (_maxPos > _minPos)
			coef = 1.0 / (_maxPos - _minPos);
		float thumbPos = top + fieldHeight * (_curPos - _minPos) * coef;

		if (y - _y < spinHeight)
		{
			SetThumbPos(_curPos - _lineStep);
			if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
		}
		else if (y - _y > _h - spinHeight)
		{
			SetThumbPos(_curPos + _lineStep);
			if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
		}
		else if (y < thumbPos - 0.5 * thumbHeight)
		{
			SetThumbPos(_curPos - _pageStep);
			if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
		}
		else if (y > thumbPos + 0.5 * thumbHeight)
		{
			SetThumbPos(_curPos + _pageStep);
			if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
		}
		else
		{
			_thumbLocked = true;
			_thumbOffset = y - thumbPos;
		}
	}
	else
	{
		float spinWidth = (0.75 * 0.6) * _h;
		const float thumbWidth = 0.015;
		float left = _x + spinWidth + 0.5 * thumbWidth;
		float fieldWidth = _w - 2 * spinWidth - thumbWidth;

		float coef = 0;
		if (_maxPos > _minPos)
			coef = 1.0 / (_maxPos - _minPos);
		float thumbPos = left + fieldWidth * (_curPos - _minPos) * coef;

		if (x - _x < spinWidth)
		{
			SetThumbPos(_curPos - _lineStep);
			if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
		}
		else if (x - _x > _w - spinWidth)
		{
			SetThumbPos(_curPos + _lineStep);
			if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
		}
		else if (x < thumbPos - 0.5 * thumbWidth)
		{
			SetThumbPos(_curPos - _pageStep);
			if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
		}
		else if (x > thumbPos + 0.5 * thumbWidth)
		{
			SetThumbPos(_curPos + _pageStep);
			if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
		}
		else
		{
			_thumbLocked = true;
			_thumbOffset = x - thumbPos;
		}
	}
}

void CSlider::OnLButtonUp(float x, float y)
{
	_thumbLocked = false;
}

void CSlider::OnMouseMove(float x, float y, bool active)
{
	if (_thumbLocked)
	{
		if ((_style & SL_DIR) == SL_VERT)
		{
			float spinHeight = (1.33 * 0.6) * _w;
			const float thumbHeight = 0.02;
			float top = _y + spinHeight + 0.5 * thumbHeight;
			float fieldHeight = _h - 2 * spinHeight - thumbHeight;

			float coef = 0;
			if (_maxPos > _minPos)
				coef = _maxPos - _minPos;

			if (y - _thumbOffset < top)
			{
				SetThumbPos(_minPos);
				if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
			}
			else if (y - _thumbOffset > top + fieldHeight)
			{
				SetThumbPos(_maxPos);
				if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
			}
			else
			{
				SetThumbPos(_minPos + (y - _thumbOffset - top) * coef / fieldHeight);
				if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
			}
		}
		else
		{
			float spinWidth = (0.75 * 0.6) * _h;
			const float thumbWidth = 0.015;
			float left = _x + spinWidth + 0.5 * thumbWidth;
			float fieldWidth = _w - 2 * spinWidth - thumbWidth;

			float coef = 0;
			if (_maxPos > _minPos)
				coef = _maxPos - _minPos;

			if (x - _thumbOffset < left)
			{
				SetThumbPos(_minPos);
				if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
			}
			else if (x - _thumbOffset > left + fieldWidth)
			{
				SetThumbPos(_maxPos);
				if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
			}
			else
			{
				SetThumbPos(_minPos + (x - _thumbOffset - left) * coef / fieldWidth);
				if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
			}
		}
	}
}

void CSlider::OnDraw(float alpha)
{
	const int w = GLOB_ENGINE->Width2D();
	const int h = GLOB_ENGINE->Height2D();

	PackedColor color = ModAlpha(_color, alpha);

	if ((_style & SL_DIR) == SL_VERT)
	{
		float bigLine = 0.6 * _w;
		float smallLine = 0.3 * _w;

		float spinHeight = 1.33 * bigLine;
		const float thumbHeight = 0.02;
		float fieldHeight = _h - 2 * spinHeight - thumbHeight;
		
		float top = _y + spinHeight + 0.5 * thumbHeight;
		float bottom = top + fieldHeight;
		float right = _x + bigLine;

		float coef = 0;
		if (_maxPos > _minPos)
			coef = 1.0 / (_maxPos - _minPos);
		float thumbPos = top + fieldHeight * (_curPos - _minPos) * coef;

		// draw top spin
		float center = _x + 0.5 * bigLine;
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel(center * w, _y * h, _x * w, (_y + spinHeight) * h),
			color, color
		);
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel(_x * w, (_y + spinHeight) * h, (_x + bigLine) * w, (_y + spinHeight) * h),
			color, color
		);
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel((_x + bigLine) * w, (_y + spinHeight) * h, center * w, _y * h),
			color, color
		);

		// draw right spin
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel(center * w, (_y + _h) * h, _x * w, (_y + _h - spinHeight) * h),
			color, color
		);
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel(_x * w, (_y + _h - spinHeight) * h, (_x + bigLine) * w, (_y + _h - spinHeight) * h),
			color, color
		);
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel((_x + bigLine) * w, (_y + _h - spinHeight) * h, center * w, (_y + _h) * h),
			color, color
		);

		// draw background
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel( toInt(right * w) + 0.5, toInt(top * h) + 0.5, toInt(right * w), toInt(bottom * h) + 1.5),
			color, color
		);
		int lines = 1;
		float lineDist = fieldHeight;
		while (lineDist > 0.02)
		{
			lines *= 2;
			lineDist *= 0.5;
		}

		for (int i=0; i<=lines; i++)
		{
			float pos = toInt((top + i * lineDist) * h) + 0.5;
			float left = right - ((i % 2 == 0 || lines == 1) ? bigLine : smallLine);
			GLOB_ENGINE->DrawLine
			(
				Line2DPixel(left * w, pos, right * w, pos),
				color, color
			);
		}

		// draw thumb
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel((_x + bigLine) * w, thumbPos * h, (_x + _w) * w, (thumbPos - 0.5 * thumbHeight) * h),
			color, color
		);
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel((_x + _w) * w, (thumbPos - 0.5 * thumbHeight) * h, (_x + _w) * w, (thumbPos + 0.5 * thumbHeight) * h),
			color, color
		);
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel((_x + _w) * w, (thumbPos + 0.5 * thumbHeight) * h, (_x + bigLine) * w, thumbPos * h), 
			color, color
		);
	}
	else
	{
		float bigLine = 0.6 * _h;
		float smallLine = 0.3 * _h;
		float space = 0.15 * _h;

		float spinWidth = 0.75 * bigLine;
		const float thumbWidth = 0.015;
		float fieldWidth = _w - 2 * spinWidth - thumbWidth;
		
		float left = _x + spinWidth + 0.5 * thumbWidth;
		float right = left + fieldWidth;
		float bottom = _y + bigLine;

		float coef = 0;
		if (_maxPos > _minPos)
			coef = 1.0 / (_maxPos - _minPos);
		float thumbPos = left + fieldWidth * (_curPos - _minPos) * coef;

		// draw left spin
		float center = _y + 0.5 * bigLine;
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel(_x * w, center * h, (_x + spinWidth) * w, _y * h),
			color, color
		);
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel((_x + spinWidth) * w, _y * h,(_x + spinWidth) * w, (_y + bigLine) * h),
			color, color
		);
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel((_x + spinWidth) * w, (_y + bigLine) * h,_x * w, center * h),
			color, color
		);

		// draw right spin
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel((_x + _w) * w, center * h, (_x + _w - spinWidth) * w, _y * h),
			color, color
		);
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel((_x + _w - spinWidth) * w, _y * h, (_x + _w - spinWidth) * w, (_y + bigLine) * h),
			color, color
		);
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel((_x + _w - spinWidth) * w, (_y + bigLine) * h, (_x + _w) * w, center * h),
			color, color
		);

		// draw background
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel(toInt(left * w) + 0.5, toInt(bottom * h) + 0.5, toInt(right * w) + 1.5, toInt(bottom * h) + 0.5),
			color, color
		);
		int lines = 1;
		float lineDist = fieldWidth;
		while (lineDist > 0.015)
		{
			lines *= 2;
			lineDist *= 0.5;
		}

		for (int i=0; i<=lines; i++)
		{
			float pos = toInt((left + i * lineDist) * w) + 0.5;
			float top = bottom - ((i % 2 == 0 || lines == 1) ? bigLine : smallLine);
			GLOB_ENGINE->DrawLine
			(
				Line2DPixel(pos, toInt(top * h) + 0.5, pos, toInt(bottom * h) + 0.5),
				color, color
			);
		}

		// draw thumb
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel(thumbPos * w, (_y + bigLine + space) * h, (thumbPos - 0.5 * thumbWidth) * w, (_y + _h) * h),
			color, color
		);
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel((thumbPos - 0.5 * thumbWidth) * w, (_y + _h) * h, (thumbPos + 0.5 * thumbWidth) * w, (_y + _h) * h),
			color, color
		);
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel((thumbPos + 0.5 * thumbWidth) * w, (_y + _h) * h, thumbPos * w, (_y + bigLine + space) * h), 
			color, color
		);
	}
}

///////////////////////////////////////////////////////////////////////////////
// class CScrollBar (used in ListBox etc.)

const float sbWidth = 0.02;

CScrollBar::CScrollBar()
{
	_thumbLocked = false;
}

void CScrollBar::OnLButtonDown(float x, float y)
{
	float spinSize = 0.8 * _w;
	if (2.0 * spinSize >= _h) return; // too width scrollbar
	if (_maxPos <= _minPos) return;

	y -= _y;
	float invRange = (_h - 2.0f * spinSize) / (_maxPos - _minPos);
	float thumbSize = _page * invRange;
	float thumbPos = spinSize + _curPos * invRange;

	if (y <= spinSize)
	{
		// lineUp
		_curPos -= _lineStep;
	}
	else if (y <= thumbPos)
	{
		// pageUp
		_curPos -= _pageStep;
	}
	else if (y <= thumbPos + thumbSize)
	{
		// thumb
		_thumbLocked = true;
		_thumbOffset = y - thumbPos;
	}
	else if (y <= _h - spinSize)
	{
		// pageDown
		_curPos += _pageStep;
	}
	else
	{
		// lineDown
		_curPos += _lineStep;
	}
	saturate(_curPos, _minPos, _maxPos - _page);
}

void CScrollBar::OnLButtonUp(float x, float y)
{
	_thumbLocked = false;
}

void CScrollBar::OnMouseHold(float x, float y)
{
	if (_thumbLocked)
	{
		float spinSize = 0.8 * _w;
		if (2.0 * spinSize >= _h) return; // too width scrollbar
		if (_maxPos <= _minPos) return;

		y -= _y;
		float range = (_maxPos - _minPos) / (_h - 2.0f * spinSize);
		float thumbPos = y - _thumbOffset;
		_curPos = (thumbPos - spinSize) * range;
		saturate(_curPos, _minPos, _maxPos - _page);
	}
}

static void DrawRect(float x1, float y1, float x2, float y2, PackedColor color)
{
	GEngine->DrawLine(Line2DPixel(x1, y1, x2, y1), color, color);
	GEngine->DrawLine(Line2DPixel(x2, y1, x2, y2), color, color);
	GEngine->DrawLine(Line2DPixel(x2, y2, x1, y2), color, color);
	GEngine->DrawLine(Line2DPixel(x1, y2, x1, y1), color, color);
}

void CScrollBar::OnDraw(float alpha)
{
	const int w = GLOB_ENGINE->Width2D();
	const int h = GLOB_ENGINE->Height2D();

	PackedColor color = ModAlpha(_color, alpha);

	// frame
	float xx = toInt(_x * w) + 0.5;
	float yy = toInt(_y * h) + 0.5;
	float ww = toInt(_w * w);
	float hh = toInt(_h * h);
	
	DrawRect(xx, yy, xx + ww, yy + hh, color);

	// spins
	float spinSize = 0.8 * _w;
	if (2.0 * spinSize >= _h) return; // too width scrollbar
	float borderX = toInt(0.25 * _w * w);
	float centerX = toInt(0.5 * _w * w);
	float borderY = toInt(0.2 * _w * h);
	float top = _y + spinSize;
	float tt = toInt(top * h) + 0.5;
	GEngine->DrawLine
	(
		Line2DPixel(xx, tt, xx + ww, tt),
		color, color
	);
	GEngine->DrawLine
	(
		Line2DPixel(xx + borderX, tt - borderY, xx + centerX, yy + borderY),
		color, color
	);
	GEngine->DrawLine
	(
		Line2DPixel(xx + centerX, yy + borderY, xx + ww - borderX, tt - borderY),
		color, color
	);
	float bottom = _y + _h - spinSize; 
	float bb = toInt(bottom * h) + 0.5;
	GEngine->DrawLine
	(
		Line2DPixel(xx, bb, xx + ww, bb),
		color, color
	);
	GEngine->DrawLine
	(
		Line2DPixel(xx + borderX, bb + borderY, xx + centerX, yy + hh - borderY),
		color, color
	);
	GEngine->DrawLine
	(
		Line2DPixel(xx + centerX, yy + hh - borderY, xx + ww - borderX, bb + borderY),
		color, color
	);

	// thumb
	if (_maxPos <= _minPos) return;
	float invRange = (bottom - top) / (_maxPos - _minPos);
	float thumbSize = _page * invRange;
	float thumbPos = top + _curPos * invRange;
	DrawRect
	(
		xx + 2, thumbPos * h + 2,
		xx + ww - 2, (thumbPos + thumbSize) * h - 2,
		color
	);
}

///////////////////////////////////////////////////////////////////////////////
// class CProgressBar

CProgressBar::CProgressBar(ControlsContainer *parent, int idc, const ParamEntry &cls)
	: Control(parent, CT_PROGRESS, idc, cls)
{
	SetRange(0, 1);
	SetPos(0);

	_frameColor = GetPackedColor(cls >> "colorFrame");
	_barColor = GetPackedColor(cls >> "colorBar");
}

void CProgressBar::OnDraw(float alpha)
{
	const int w = GLOB_ENGINE->Width2D();
	const int h = GLOB_ENGINE->Height2D();

	float xx = toInt(_x * w) + 0.5;
	float yy = toInt(_y * h) + 0.5;
	float ww = toInt(_w * w);
	float hh = toInt(_h * h);

	PackedColor frameColor = ModAlpha(_frameColor, alpha);
	PackedColor barColor = ModAlpha(_barColor, alpha);

	// draw frame
	DrawLeft(0, frameColor);
	DrawTop(0, frameColor);
	DrawBottom(0, frameColor);
	DrawRight(0, frameColor);

	// draw bar
	if (_minPos < _curPos && _curPos <= _maxPos)
	{
		float coef = (_curPos - _minPos) / (_maxPos - _minPos);
		int width = toInt(coef * (ww - 2));

		Texture *texture = GLOB_SCENE->Preloaded(TextureWhite);
		MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(texture, 0, 0);
		GLOB_ENGINE->Draw2D
		(
			mip, barColor,
			Rect2DPixel(xx + 1, yy + 1, width, hh - 2)
		);
		//GLOB_ENGINE->TextBank()->ReleaseMipmap();
	}
}

///////////////////////////////////////////////////////////////////////////////
// class CListBox

CListBoxContainer::CListBoxContainer(const ParamEntry &cls)
{
	_ftColor = GetPackedColor(cls >> "colorText");
	_selColor = GetPackedColor(cls >> "colorSelect");

	_selString = -1;
	_topString = 0;

	_showSelected = true;
	_readOnly = false;
}

static int CompareItems( const CListBoxItem *str0, const CListBoxItem *str1 )
{
	return stricmp(str0->text, str1->text);
}

void CListBoxContainer::SortItems()
{
	QSort(Data(), Size(), CompareItems);
}

static int CompareItemsByValues( const CListBoxItem *str0, const CListBoxItem *str1 )
{
	int diff = str0->value - str1->value;
	if (diff != 0) return diff;
	return stricmp(str0->text, str1->text);
}

void CListBoxContainer::SortItemsByValue()
{
	QSort(Data(), Size(), CompareItemsByValues);
}

void CListBoxContainer::InsertString(int i, RString text)
{
	if (i < 0 || i > GetSize())
		return;
	Insert(i);
	Set(i).text = text;
	Set(i).data = "";
	Set(i).value = 0;
	Set(i).ftColor = _ftColor;
	Set(i).selColor = _selColor;
	if (i <= _selString)
	{
		_selString++;
	}
}

void CListBoxContainer::DeleteString(int i)
{
	if (i < 0 || i >= GetSize())
		return;
	Delete(i);
	if (i == GetSize())
	{
		_selString = GetSize() - 1;
		if (i == 0)
		{
			_topString = 0;
		}
	}
}

void CListBoxContainer::OnMouseZChanged(float dz)
{
	if (dz == 0) return;
	if (_topString > 0 && dz > 0)
	{
		// scroll up
		float scroll = -SCROLL_SPEED * dz;
		saturate(scroll, -SCROLL_MAX, -SCROLL_MIN);
		_topString += 0.1 * scroll;
		if (_topString <= 1.0)
			_topString = 0;
	}
	else if (_topString + NRows() <= GetSize() && dz < 0)
	{
		// scroll down
		float scroll = -SCROLL_SPEED * dz;
		saturate(scroll, SCROLL_MIN, SCROLL_MAX);
		_topString += 0.1 * scroll;
		if (_topString <= 1.0)
			_topString += 1.0;
		if (_topString + NRows() > GetSize())
			_topString = GetSize() - NRows();
	}
}

void CListBoxContainer::Check()
{
	saturateMin(_topString, GetSize() - NRows());
	saturateMax(_topString, 0);
}

CListBox::CListBox(ControlsContainer *parent, int idc, const ParamEntry &cls)
	: Control(parent, CT_LISTBOX, idc, cls), CListBoxContainer(cls)
{
	_font = GLOB_ENGINE->LoadFont(GetFontID(cls>>"font"));
	const ParamEntry *entry = cls.FindEntry("size");
	if (entry) _size = (float)(*entry) * _font->Height();
	else _size = cls >> "sizeEx";
	float height = cls >> "rowHeight";
	SetRowHeight(height);

	_scrollUp = false;
	_scrollDown = false;

	_showStrings = _h / _rowHeight;
	saturateMax(_showStrings, 0);

	_scrollbar.SetPosition
	(
		(_x + _w - sbWidth) * _scale, _y * _scale,
		sbWidth * _scale, _h * _scale
	);
	_scrollbar.SetColor(_ftColor);
	_scrollbar.SetRange(0, 0, _showStrings);	
	_scrollbar.SetPos(0);
	_scrollbar.SetSpeed(1, floatMax(_showStrings - 1, 1));	
}

void CListBox::SetPos(float x, float y, float w, float h)
{
	Control::SetPos(x, y, w, h);
	// update _showStrings
	_showStrings = _h / _rowHeight;
	saturateMax(_showStrings, 0);
	_scrollbar.SetSpeed(1, floatMax(_showStrings - 1, 1));	
}

#ifdef _XBOX
inline int max( int a, int b)
{
	return a>b ? a : b;
}
#endif

void CListBox::SetCurSel(int sel, bool sendUpdate)
{
	if (!sendUpdate && sel == _selString)
	{
		saturateMin(_topString, GetSize() - _showStrings);
		saturateMax(_topString, 0);
		return;
	}

	if (GetSize() == 0)
		_selString = -1;
	else if (sel < 0)
		_selString = 0;
	else if (sel >= GetSize())
		_selString = GetSize() - 1;
	else
		_selString = sel;

	if (_selString < _topString)
	{
		_topString = _selString;
		saturateMin(_topString, GetSize() - _showStrings);
		saturateMax(_topString, 0);
	}
	else if (_selString > _topString + _showStrings - 1)
		_topString = _selString - _showStrings + 1;

	if (sendUpdate)
	{
		OnSelChanged(_selString);
		_parent->OnLBSelChanged(IDC(), _selString);
	}
	Check();
}

bool CListBox::OnKillFocus()
{
	return Control::OnKillFocus();
}

bool CListBox::OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	if (IsReadOnly()) return false;

	switch (nChar)
	{
	case VK_UP:
		SetCurSel(GetCurSel() - 1);
		if (GetCurSel() < _topString)
			_topString = GetCurSel();
		return true;
	case VK_DOWN:
		SetCurSel(GetCurSel() + 1);
		if (GetCurSel() > _topString + _showStrings - 1)
			_topString = GetCurSel() - _showStrings + 1;
		return true;
	}
	return false;
}

void CListBox::OnMouseMove(float x, float y, bool active)
{
//	if (IsReadOnly()) return;

	if (!GInput.mouseL) return;

	OnMouseHold(x, y, active);
}

void CListBox::OnMouseHold(float x, float y, bool active)
{
//	if (IsReadOnly()) return;

	if (!GInput.mouseL) return;

	if (_scrollbar.IsEnabled() && _scrollbar.IsLocked())
	{
		_scrollbar.SetPos(_topString);
		_scrollbar.OnMouseHold(x, y);
		_topString = _scrollbar.GetPos();
	}
	else if (_scrollbar.IsEnabled() && _scrollbar.IsInside(x, y))
	{
	}
	else if (!IsReadOnly())
	{
		float index = (y - _y) / _rowHeight;
		{
			_scrollUp = _scrollDown = false;
			if (index >= 0 && index < _showStrings)
			{
				SetCurSel(toIntFloor(_topString + index));
			}
		}
	}
}

void CListBox::OnMouseZChanged(float dz)
{
//	if (IsReadOnly()) return;

	CListBoxContainer::OnMouseZChanged(dz);
}

void CListBox::OnLButtonDown(float x, float y)
{
//	if (IsReadOnly()) return;

	if (_scrollbar.IsEnabled() && _scrollbar.IsInside(x, y))
	{
		_scrollbar.SetPos(_topString);
		_scrollbar.OnLButtonDown(x, y);
		_topString = _scrollbar.GetPos();
	}
}

void CListBox::OnLButtonUp(float x, float y)
{
//	if (IsReadOnly()) return;

	if (_scrollbar.IsLocked())
	{
		_scrollbar.OnLButtonUp(x, y);
	}
}

void CListBox::OnLButtonDblClick(float x, float y)
{
	if (IsReadOnly()) return;

	if (_selString < 0) return;
	_parent->OnLBDblClick(IDC(), _selString);
}

void CListBox::OnDraw(float alpha)
{
	const int w = GLOB_ENGINE->Width2D();
	const int h = GLOB_ENGINE->Height2D();

	float xx = toInt(_x * w) + 0.5;
	float yy = toInt(_y * h) + 0.5;
	float ww = toInt(_w * w);
	float hh = toInt(_h * h);

	PackedColor ftColor = ModAlpha(_ftColor, alpha);
	PackedColor selColor = ModAlpha(_selColor, alpha);

	// draw list
	DrawRect(xx, yy, xx + ww, yy + hh, ftColor);

	// draw scrollbar
	Rect2DFloat rect(_x, _y, _w, _h);
	if (GetSize() > _showStrings)
	{
		_scrollbar.Enable(true);
		_scrollbar.SetPosition
		(
			(_x + _w - sbWidth) * _scale, _y * _scale,
			sbWidth * _scale, _h * _scale
		);
		_scrollbar.SetRange(0, GetSize(), _showStrings);
		_scrollbar.SetPos(_topString);
		_scrollbar.OnDraw(alpha);
		rect.w -= sbWidth; 
	}
	else
	{
		_scrollbar.Enable(false);
	}
	int i = toIntFloor(_topString);
	float top = _y - (_topString - i) * _rowHeight;
	bool canSelect = IsEnabled()/* && !IsReadOnly()*/;
	while (top < _y + _h && i < GetSize())
	{
		DrawItem
		(
			alpha, i, i == GetCurSel() && canSelect,
			top, rect
		);
		top += _rowHeight;
		i++;
	}
}

void CListBox::DrawItem
(
	float alpha, int i, bool selected,
	float top, Rect2DFloat &rect
)
{
	PackedColor ftColor = ModAlpha(GetFtColor(i), alpha);
	PackedColor selColor = ModAlpha(GetSelColor(i), alpha);
	PackedColor color;

	const int w = GLOB_ENGINE->Width2D();
	const int h = GLOB_ENGINE->Height2D();
	const float border = 0.005;

	if (selected && _showSelected)
	{
		MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
		GLOB_ENGINE->Draw2D
		(
			mip, ftColor, Rect2DPixel(rect.x * w, top * h, rect.w * w, _rowHeight * h),
			Rect2DPixel(rect.x * w, rect.y * h, rect.w * w, rect.h * h)
		);
		color = selColor;
	}
	else
	{
		color = ftColor;
	}
	float left = _x + border;
	
	Texture *texture = GetTexture(i);
	if (texture)
	{
		MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(texture, 0, 0);
		float width = _rowHeight *
			(texture->AWidth() * h) / (texture->AHeight() * w);
		GLOB_ENGINE->Draw2D
		(
			mip, PackedWhite,
			Rect2DPixel(rect.x * w, toInt(top * h) + 0.5,
			toInt(width * w), toInt(_rowHeight * h)),
			Rect2DPixel(rect.x * w, rect.y * h, rect.w * w, rect.h * h)
		);
		left += width;
	}

	const char *text = GetText(i);
	if (text)
	{
		float t = top + 0.5 * (_rowHeight - _size);
		GLOB_ENGINE->DrawText(Point2DFloat(left, t), _size,
		rect,
		_font, color, text);
	}
}

void CListBox::OnSelChanged(int curSel)
{
}

///////////////////////////////////////////////////////////////////////////////
// class CCombo

CCombo::CCombo(ControlsContainer *parent, int idc, const ParamEntry &cls)
	: Control(parent, CT_COMBO, idc, cls), CListBoxContainer(cls)
{
	_mouseSel = _selString;
	_wholeHeight = cls>>"wholeHeight";
	_bgColor = GetPackedColor(cls >> "colorBackground");
	_font = GLOB_ENGINE->LoadFont(GetFontID(cls>>"font"));
	const ParamEntry *entry = cls.FindEntry("size");
	if (entry) _size = (float)(*entry) * _font->Height();
	else _size = cls >> "sizeEx";

	_expanded = false;
	_scrollUp = false;
	_scrollDown = false;

	_showStrings = (_wholeHeight - _h) / (_size);
	if (_showStrings < 0)
		_showStrings = 0;

	_scrollbar.SetPosition
	(
		(_x + _w - sbWidth) * _scale, (_y + _h) * _scale,
		sbWidth * _scale, (_wholeHeight - _h) * _scale
	);
	_scrollbar.SetColor(_ftColor);
	_scrollbar.SetRange(0, 0, _showStrings);	
	_scrollbar.SetPos(0);
	_scrollbar.SetSpeed(1, floatMax(_showStrings - 1, 1));	
}

void CCombo::SetPos(float x, float y, float w, float h)
{
	Control::SetPos(x, y, w, h);
	// update _showStrings
	_showStrings = (_wholeHeight - _h) / (_size);
	saturateMax(_showStrings, 0);
	_scrollbar.SetSpeed(1, floatMax(_showStrings - 1, 1));	
}

const float border = 0.005;

bool CCombo::IsInsideExt(float x, float y)
{
	if (!_expanded) return false;

	float height = floatMin
	(
		_wholeHeight - _h,
		GetSize() * _size
	) + _h;

	float width = 0;
	for (int i=0; i<GetSize(); i++)
	{
		const char *text = GetText(i);
		if (text)
			saturateMax(width, GLOB_ENGINE->GetTextWidth(_size, _font, text));
	}
	width += 2 * border;
	if (GetSize() > _showStrings)
		width += sbWidth; 
	saturateMax(width, _w);
	return x >= _x && x < _x + width && y >= _y + _h && y < _y + height;
}

bool CCombo::IsInside(float x, float y)
{
/*
	return Control::IsInside(x, y) || IsInsideExt(x, y);
*/
	if (_expanded) return true;
	else return Control::IsInside(x, y);
}

void CCombo::SetTopString()
{
	if (_mouseSel < _topString)
		_topString = max(_mouseSel, 0);
	else if (_mouseSel > _topString + _showStrings - 1)
		_topString = _mouseSel - _showStrings + 1;
}

void CCombo::SetCurSel(int sel, bool sendUpdate)
{
	if (GetSize() == 0)
		_selString = -1;
	else if (sel < 0)
		_selString = 0;
	else if (sel >= GetSize())
		_selString = GetSize() - 1;
	else
		_selString = sel;

	_mouseSel = _selString;

	SetTopString();

	if (sendUpdate)
	{
		OnSelChanged(_selString);
		_parent->OnComboSelChanged(IDC(), _selString);
	}
	Check();
}

bool CCombo::OnKillFocus()
{
	_expanded = false;
	return Control::OnKillFocus();
}

bool CCombo::OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	if (!_expanded && _showStrings > 0 && nChar == VK_SPACE)
	{
		_expanded = true;
		SetTopString();
		return true;
	}
	if (_expanded && !GInput.mouseL)
	{
		switch (nChar)
		{
		case VK_SPACE:
			_expanded = false;
			return true;
		case VK_UP:
			SetCurSel(GetCurSel() - 1);
			return true;
		case VK_DOWN:
			SetCurSel(GetCurSel() + 1);
			return true;
		}
	}
	return false;
}

void CCombo::OnLButtonDown(float x, float y)
{
	if (_expanded && _scrollbar.IsEnabled() && _scrollbar.IsInside(x, y))
	{
		_scrollbar.SetPos(_topString);
		_scrollbar.OnLButtonDown(x, y);
		_topString = _scrollbar.GetPos();
	}
	else if (_showStrings > 0 && !IsInsideExt(x, y))
	{
		_expanded = !_expanded;
		SetTopString();
	}
}

void CCombo::OnLButtonUp(float x, float y)
{
	if (_scrollbar.IsLocked())
	{
		_scrollbar.OnLButtonUp(x, y);
	}
	else if (_scrollbar.IsEnabled() && _scrollbar.IsInside(x, y))
	{
	}
	else
	{
		if (_expanded && IsInsideExt(x, y))
		{
			float index = (y - (_y + _h)) / (_size);
			if (index >= 0 && index < _showStrings)
			{
				SetCurSel(toIntFloor(_topString + index));
			}
		}
		if (!Control::IsInside(x, y))
			_expanded = false;
	}
}

void CCombo::OnMouseMove(float x, float y, bool active)
{
	OnMouseHold(x, y, active);
}

void CCombo::OnMouseHold(float x, float y, bool active)
{
	if (_expanded)
	{
		if (_scrollbar.IsEnabled() && _scrollbar.IsLocked())
		{
			_scrollUp = _scrollDown = false;
			_scrollbar.SetPos(_topString);
			_scrollbar.OnMouseHold(x, y);
			_topString = _scrollbar.GetPos();
		}
		else if (_scrollbar.IsEnabled() && _scrollbar.IsInside(x, y))
		{
			_scrollUp = _scrollDown = false;
		}
		else
		{
			float dy;
			if 
			(
				GInput.mouseL && _topString > 0 && 
				(dy = _y + _h - y) > 0
			)
			{
				_scrollDown = false;
				// scroll up
				if (_scrollUp)
				{
					float scroll = SCROLL_SPEED * dy;
					saturate(scroll, SCROLL_MIN, SCROLL_MAX);
					_topString -= scroll * (Glob.uiTime - _scrollTime);
					saturateMax(_topString, 0);
					_scrollTime = Glob.uiTime;
				}
				else
				{
					_scrollUp = true;
					_scrollTime = Glob.uiTime;
				}
			}
			else if
			(
				GInput.mouseL && _topString + _showStrings < GetSize() &&
				(dy = y - (_y + _wholeHeight)) > 0
			)
			{
				_scrollUp = false;
				// scroll down
				if (_scrollDown)
				{
					float scroll = SCROLL_SPEED * dy;
					saturate(scroll, SCROLL_MIN, SCROLL_MAX);
					_topString += scroll * (Glob.uiTime - _scrollTime);
					saturateMin(_topString, GetSize() - _showStrings);
					_scrollTime = Glob.uiTime;
				}
				else
				{
					_scrollDown = true;
					_scrollTime = Glob.uiTime;
				}
			}
			else
			{
				_scrollUp = _scrollDown = false;
				if (IsInsideExt(x, y))
				{
					float index = (y - (_y + _h)) / (_size);
					if (index >= 0 && index < _showStrings)
					{
						_mouseSel = toIntFloor(_topString + index);
						if (GetSize() == 0) _mouseSel = -1;
						else saturate(_mouseSel, 0, GetSize() - 1);
					}
				}
			}
		}
	}
	else
		_scrollUp = _scrollDown = false;
}

void CCombo::OnMouseZChanged(float dz)
{
	if (dz == 0 || !_expanded) return;
	
	if (_topString > 0 && dz > 0)
	{
		// scroll up
		float scroll = -SCROLL_SPEED * dz;
		saturate(scroll, -SCROLL_MAX, -SCROLL_MIN);
		_topString += 0.1 * scroll;
		saturateMax(_topString, 0);
	}
	else if (_topString + _showStrings < GetSize() && dz < 0)
	{
		// scroll down
		float scroll = -SCROLL_SPEED * dz;
		saturate(scroll, SCROLL_MIN, SCROLL_MAX);
		_topString += 0.1 * scroll;
		saturateMin(_topString, GetSize() - _showStrings);
	}
}

void CCombo::OnDraw(float alpha)
{
	const int w = GLOB_ENGINE->Width2D();
	const int h = GLOB_ENGINE->Height2D();

	float xx = toInt(_x * w) + 0.5;
	float yy = toInt(_y * h) + 0.5;
	float ww = toInt(_w * w);
	float hh = toInt(_h * h);

	PackedColor ftColor = ModAlpha(_ftColor, alpha);
	PackedColor bgColor = ModAlpha(_bgColor, alpha);
	PackedColor selColor = ModAlpha(_selColor, alpha);

	PackedColor color;
	Texture *texture = GLOB_SCENE->Preloaded(TextureWhite);

	// draw bar
	DrawLeft(0, ftColor);
	DrawTop(0, ftColor);
	DrawBottom(0, ftColor);
	DrawRight(0, ftColor);

	if (IsFocused())
	{
		MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(texture, 0, 0);
		GLOB_ENGINE->Draw2D
		(
			mip, ftColor, Rect2DPixel(xx, yy, ww, hh)
		);
		color = selColor;
	}
	else
	{
		color = ftColor;
	}
	const char *text = GetText(GetCurSel());
	if (text)
	{
		float top = _y + 0.5 * (_h - _size);
		GLOB_ENGINE->DrawText
		(
			Point2DFloat(_x + border, top), _size,
			Rect2DFloat(_x, _y, _w, _h),
			_font, color, text
		);
	}

	// draw list
	if (_expanded)
	{
		float height = _size;
		float top = _y + _h;
		float curHeight = floatMin(_wholeHeight - _h, GetSize() * height);

		yy = toInt(top * h) + 0.5;
		hh = toInt(curHeight * h);

		float w2 = 0;
		for (int i=0; i<GetSize(); i++)
		{
			const char *text = GetText(i);
			if (text)
				saturateMax(w2, GLOB_ENGINE->GetTextWidth(_size, _font, text));
		}
		w2 += 2 * border;
		float w2SB = w2;
		if (GetSize() > _showStrings)
		{
			w2 += sbWidth;
			saturateMax(w2, _w);
			w2SB = w2 - sbWidth;
			_scrollbar.Enable(true);
			_scrollbar.SetPosition
			(
				(_x + w2SB) * _scale, top * _scale,
				sbWidth * _scale, curHeight * _scale
			);
			_scrollbar.SetRange(0, GetSize(), _showStrings);
			_scrollbar.SetPos(_topString);
		}
		else
		{
			saturateMax(w2, _w);
			w2SB = w2;
			_scrollbar.Enable(false);
		}
		ww = toInt(w2 * w);
		float wwSB = toInt(w2SB * w);

		MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(texture, 0, 0);
		GLOB_ENGINE->Draw2D
		(
			mip, bgColor, Rect2DPixel(xx, yy, ww, hh)
		);
		
		DrawLeft(0, ftColor);
		DrawTop(0, ftColor);
		DrawBottom(0, ftColor);
		DrawRight(0, ftColor);

		if (GetSize() > _showStrings)
			_scrollbar.OnDraw(alpha);

		float topClip = top;
		float bottomClip = _y + _h + curHeight;

		int i = toIntFloor(_topString);

		top = topClip - (_topString - i) * height;
		while (top < bottomClip && i < GetSize())
		{
			if (i == _mouseSel)
			{
				float realTop = floatMax(top, topClip);
				float realHeight = floatMin(top + height, bottomClip) - realTop;
				saturateMin(realHeight, height);
				MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(texture, 0, 0);
				GLOB_ENGINE->Draw2D
				(
					mip, ftColor, Rect2DPixel(xx, realTop * h, wwSB, realHeight * h)
				);
				color = selColor;
			}
			else
			{
				color = ftColor;
			}
			const char *text = GetText(i);
			if (text)
				GLOB_ENGINE->DrawText(Point2DFloat(_x + border, top), _size,
				Rect2DFloat(_x, topClip, w2, bottomClip - topClip),
				_font, color, text);
			top += height;
			i++;
		}
	}
	else
		_scrollbar.Enable(false);
}

void CCombo::OnSelChanged(int curSel)
{
}

///////////////////////////////////////////////////////////////////////////////
// class Control3D

Control3D::Control3D(ControlsContainer *parent, int type, int idc, const ParamEntry &cls)
: Control(parent, type, idc, cls)
{
	_position = VZero;
	_right = VZero;
	_down = VZero;

	float angle = cls >> "angle";
	_angle = (H_PI / 180.0) * angle;

	_u = _v = 0;
}

void Control3D::UpdateInfo(ControlObject *object, ControlInObject &info)
{
	Control::UpdateInfo(object, info);
	SetScale(1);

	Vector3 right = info.posTR - info.posTL;
	Vector3 down = info.posBL - info.posTL;
	
	Matrix3 matrix; matrix.SetUpAndAside(-down, right);
	Matrix3 invMatrix(MInverseRotation, matrix); 
	Matrix3 rotZ(MRotationZ, _angle);
	Matrix3 rotation = matrix * rotZ * invMatrix;

	Vector3 rightS = info.w * right;
	Vector3 downS = info.h * down;

	_position = object->PositionModelToWorld
	(
		info.posTL + info.x * right + info.y * down
	);
	_right = object->DirectionModelToWorld
	(
		rightS * rotation
	);
	_down = object->DirectionModelToWorld
	(
		downS * rotation
	);
}

bool Control3D::IsInside(float x, float y)
{
	const float EPSILON = 1e-9;
	_u = 0; _v = 0;
	if (_right.SquareSize() < EPSILON) return false;
	if (_down.SquareSize() < EPSILON) return false;

	Vector3 b = ControlObject::Convert2DTo3D(Point2DFloat(x,y),1);
	/*
	AspectSettings as;
	GEngine->GetAspectSettings(as);

	Vector3 b = Vector3
	(
		(x - 0.5) * InvCameraZoom * as.leftFOV, as.topFOV * (0.5 - y) * InvCameraZoom, 1.0
	);
	*/
	float pxy = _position.Y() * b.X() - _position.X() * b.Y();
	float rxy = _right.Y() * b.X() - _right.X() * b.Y();
	float dxy = _down.Y() * b.X() - _down.X() * b.Y();
	float pxz = _position.Z() * b.X() - _position.X() * b.Z();
	float rxz = _right.Z() * b.X() - _right.X() * b.Z();
	float dxz = _down.Z() * b.X() - _down.X() * b.Z();

	float dr = dxy * rxz - dxz * rxy;
	float pr = pxy * rxz - pxz * rxy;

	_v = - pr / dr;
	_u = - (pxy + dxy * _v) / rxy;
	return _u >= 0 && _u <= 1 && _v >= 0 && _v <= 1;
}

///////////////////////////////////////////////////////////////////////////////
// class C3DStatic

C3DStatic::C3DStatic(ControlsContainer *parent, int idc, const ParamEntry &cls)
: Control3D(parent, CT_3DSTATIC, idc, cls)
{
	_enabled = false;
	_texture = NULL;
	_font = GLOB_ENGINE->LoadFont(GetFontID(cls >> "font"));
	_color = GetPackedColor(cls >> "color");
	if ((_style & ST_TYPE) == ST_BACKGROUND) _bgColor = GetPackedColor(cls >> "colorBackground");
	else _bgColor = PackedBlack;
	_hCoef = 1;
	if (cls.FindEntry("h2"))
	{
		float h = cls >> "h";
		float h2 = cls >> "h2";
		if (h > 0) _hCoef = h2 / h;
	}

	if ((_style & ST_TYPE) == ST_MULTI)
		_maxLines = cls >> "lines";
	else
		_maxLines = 1;

	RString text = cls >> "text";
	SetText(text);
}

void C3DStatic::SetText(RString text)
{
	if (!strcmp(_text, text)) return;

	_text = text;
	if ((_style & ST_TYPE) == ST_PICTURE)
	{
		text = FindPicture(text);
		text.Lower();
		_texture = GlobLoadTexture(text);
		if (_texture) _texture->SetMaxSize(1024); // no limits
	}
	else if ((_style & ST_TYPE) == ST_MULTI)
	{
		FormatText();
	}
}

void C3DStatic::FormatText()
{
	if ((_style & ST_TYPE) != ST_MULTI) return;

	_lines.Clear();
	_lines.Add(0);

	Vector3	down = (1.0 / _maxLines) * _down;
	Vector3 up = -down;
	Vector3 right = 0.75 * up.Size() * _right.Normalized();

	float lineWidth = _right.Size();

	const char *p = _text;
	while (*p != 0)
	{
		// begin of the line
		const char *word = p;
		int n = 0;
		float width = 0;
		while (true)
		{
			const char *q = p;
			char c = *p++;
			if (c == 0) return;
			if (ISSPACE(c))
			{
				n++;
				word = p;
			}

			if ((c & 0x80) && _font->GetLangID() == Korean && *p != 0)
			{
				// TODO: simplify
				char temp[3]; temp[0] = c; temp[1] = *p++; temp[2] = 0;
				width += GEngine->GetText3DWidth(right, _font, temp).Size();
			}
			else
			{
				// TODO: simplify
				char temp[2]; temp[0] = c; temp[1] = 0;
				width += GEngine->GetText3DWidth(right, _font, temp).Size();
			}

			if (width > lineWidth)
			{
				if (n > 0)
				{
					p = word;
				}
				else
				{
					p = q;
				}
				_lines.Add(p - (const char *)_text);
				break;
			}
		}
	}
}

void C3DStatic::OnDraw(float alpha)
{
	PackedColor color = ModAlpha(_color, alpha);
	Vector3 normal = _down.CrossProduct(_right).Normalized(); 
	Vector3 position = _position - 0.002 * normal;

	switch (_style & ST_TYPE)
	{
	case ST_PICTURE:
		GEngine->Draw3D
		(
			position, _down, _right, ClipAll, color, DisableSun, _texture
		);
		break;
	case ST_BACKGROUND:
		{
			PackedColor bgColor = ModAlpha(_bgColor, alpha);
			GEngine->Draw3D
			(
				_position, _down, _right, ClipAll, bgColor, DisableSun, NULL
			);

			GEngine->DrawLine3D(position, position + _right, color, DisableSun);
			GEngine->DrawLine3D(position + _right, position + _right + _down, color, DisableSun);
			GEngine->DrawLine3D(position + _right + _down, position + _down, color, DisableSun);
			GEngine->DrawLine3D(position + _down, position, color, DisableSun);

			if (_text.GetLength() > 0) DrawText(_text, position, _down, color);
		}
		break;
	case ST_FRAME:
		if (_text.GetLength() > 0)
		{
			Vector3 up = -_hCoef * _down;
			Vector3 right = 0.75 * up.Size() * _right.Normalized();

			Vector3 width = GEngine->GetText3DWidth(right, _font, _text);
			Vector3 borderH = -up;
			Vector3 borderW = up.Size() * right.Normalized();
			if ((2 * borderW + width + 2 * borderW).Size() > _right.Size()) goto NoText;
			GEngine->DrawText3D(position + 2 * borderW, up, right, ClipAll, _font, color, DisableSun, _text);
			Vector3 top = position + 0.5 * borderW + 0.5 * borderH;
			Vector3 down = _down - borderH;
			right = _right - borderW;
			GEngine->DrawLine3D(top, position + 2 * borderW + 0.5 * borderH, color, DisableSun);
			GEngine->DrawLine3D(position + 2 * borderW + 0.5 * borderH + width, top + right, color, DisableSun);
			GEngine->DrawLine3D(top + right, top + right + down, color, DisableSun);
			GEngine->DrawLine3D(top + right + down, top + down, color, DisableSun);
			GEngine->DrawLine3D(top + down, top, color, DisableSun);
/*
			float width = GLOB_ENGINE->GetTextWidth(size, _font, _text);
			float heigth = size;
			const float border = _scale * 0.01;
			if (border + width + border <= _w)
			{
				GLOB_ENGINE->DrawText
				(
					_x + border, _y, size,
					_x, _y, _w, _h,
					_font, ftColor, _text
				);
				float top = yy + 0.5 * heigth * h;
				GLOB_ENGINE->DrawLine
				(
					xx, top, xx + border * w, top, ftColor, ftColor
				);
				GLOB_ENGINE->DrawLine
				(
					xx + (border + width) * w, top, xx + ww - 1, top, ftColor, ftColor
				);
				GLOB_ENGINE->DrawLine
				(
					xx, top, xx, yy + hh - 1, ftColor, ftColor
				);
				GLOB_ENGINE->DrawLine
				(
					xx + ww - 1, top, xx + ww - 1, yy + hh - 1, ftColor, ftColor
				);
				DrawBottom(0, ftColor);
				return;
			}
*/
		}
		else
		{
NoText:
			GEngine->DrawLine3D(position, position + _right, color, DisableSun);
			GEngine->DrawLine3D(position + _right, position + _right + _down, color, DisableSun);
			GEngine->DrawLine3D(position + _right + _down, position + _down, color, DisableSun);
			GEngine->DrawLine3D(position + _down, position, color, DisableSun);
		}
		break;
	case ST_MULTI:
		{
			Vector3 top = position;
			Vector3 down = (1.0 / _maxLines) * _down;
			for (int i=0; i<_lines.Size() && i<_maxLines; i++)
			{
				DrawText(GetLine(i), top, down, color);
				top += down;
			}
		}
		break;
	case ST_WITH_RECT:
		{
			GEngine->DrawLine3D(position, position + _right, color, DisableSun);
			GEngine->DrawLine3D(position + _right, position + _right + _down, color, DisableSun);
			GEngine->DrawLine3D(position + _right + _down, position + _down, color, DisableSun);
			GEngine->DrawLine3D(position + _down, position, color, DisableSun);
			// continue - draw text
		}
	default:
		DrawText(_text, position, _down, color);
		break;
	}
}

RString C3DStatic::GetLine(int i) const
{
	if (i < 0) return "";
	Assert(i < _lines.Size());

	int from = _lines[i];
	int to = i + 1 < _lines.Size() ? _lines[i + 1] : _text.GetLength();
	return _text.Substring(from, to);
}

void C3DStatic::DrawText(const char *text, Vector3Par top, Vector3Par down, PackedColor color)
{
	// formatting
	Vector3 pos = top;
	Vector3 up = -down;
	Vector3 right = 0.75 * up.Size() * _right.Normalized();

	Vector3 offset = VZero;
	Vector3 width = GEngine->GetText3DWidth
	(
		right, _font, _text
	);
	switch (_style & ST_HPOS)
	{
		case ST_RIGHT:
			offset = _right - width;
			break;
		case ST_CENTER:
			offset = 0.5 * (_right - width);
			break;
		default:
			Assert((_style & ST_HPOS) == ST_LEFT)
			break;
	}
	pos += offset;

	float invRSize = 1.0 / right.Size();
	float x1c = 0, x2c = _right.Size() * invRSize;
	if (width.SquareSize() > _right.SquareSize())
	{
		float offsetSize = offset.Size() * invRSize;
		x1c += offsetSize;
		x2c += offsetSize;
	}

	GEngine->DrawText3D
	(
		pos, up, right, ClipAll, _font, color, DisableSun, text,
		x1c, 0, x2c, 1
	);
}

///////////////////////////////////////////////////////////////////////////////
// class C3DEdit

C3DEdit::C3DEdit(ControlsContainer *parent, int idc, const ParamEntry &cls)
: Control3D(parent, CT_3DEDIT, idc, cls), CEditContainer(cls)
{
	_size = cls >> "size";

	RString text = cls>>"text";
	SetText(text);
	// note: SetText does EnsureVisible

	if ((_style & ST_TYPE) == ST_MULTI)
		_maxLines = cls >> "lines";
	else
		_maxLines = 1;
}

bool C3DEdit::IsMulti() const
{
	return (_style & ST_TYPE) == ST_MULTI;
}

void C3DEdit::OnDraw(float alpha)
{
	PackedColor ftColor = ModAlpha(_ftColor, alpha);
	Vector3 normal = _down.CrossProduct(_right).Normalized(); 
	Vector3 position = _position - 0.002 * normal;

	// frame
	GEngine->DrawLine3D(position, position + _right, ftColor, DisableSun);
	GEngine->DrawLine3D(position + _right, position + _right + _down, ftColor, DisableSun);
	GEngine->DrawLine3D(position + _right + _down, position + _down, ftColor, DisableSun);
	GEngine->DrawLine3D(position + _down, position, ftColor, DisableSun);

	if (_text.GetLength() > 0)
	{
		if ((_style & ST_TYPE) == ST_MULTI)
		{
			Vector3 top = position;
			Vector3 down = (1.0 / _maxLines) * _down;
			for (int i=FirstLine(),j=0; i<_lines.Size()&&j<_maxLines ; i++,j++)
			{
				DrawText(GetLine(i), _lines[i], top, down, alpha);
				top += down;
			}
		}
		else
		{
			DrawText
			(
				_text.Substring(_firstVisible, _text.GetLength()),
				_firstVisible,
				position, _down, alpha
			);
		}
	}

	// draw caret
	if (!IsFocused()) return;
	bool state = ((Glob.uiTime.toFloat() - toIntFloor(Glob.uiTime.toFloat())) < 0.5);
	if (!state) return;

	Vector3 top;
	Vector3 down;
	Vector3 border = 0.02 * _right;
	float rightSize = (_right - border).Size();
	if ((_style & ST_TYPE) == ST_MULTI)
	{
		down = (1.0 / _maxLines) * _down;
		int cur = CurLine();
		int first = FirstLine();
		if (cur - first < 0 || cur - first >= _maxLines) return;
		top = position + (cur - first) * down;
		Vector3 dir = PosToDir
		(
			GetLine(cur), _blockEnd - (cur < 0 ? 0 : _lines[cur])
		);
		if (dir.Size() > rightSize) return;
		top += dir;
	}
	else
	{
		down = _down;
		top = position;
		Vector3 dir = PosToDir
		(
			_text.Substring(_firstVisible, _text.GetLength()),
			_blockEnd - _firstVisible
		);
		if (dir.Size() > rightSize) return;
		top += dir;
	}
	top += 0.5 * (1.0 - _size) * down;
	down *= _size;
	
	GEngine->DrawLine3D(top, top + down, ftColor, DisableSun);
}

bool C3DEdit::OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	return CEditContainer::DoKeyDown(nChar, nRepCnt, nFlags);
}

bool C3DEdit::OnChar(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	return CEditContainer::DoChar(nChar, nRepCnt, nFlags);
}

bool C3DEdit::OnIMEChar(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	return CEditContainer::DoIMEChar(nChar, nRepCnt, nFlags);
}

bool C3DEdit::OnIMEComposition(unsigned nChar, unsigned nFlags)
{
	return CEditContainer::DoIMEComposition(nChar, nFlags);
}

void C3DEdit::OnLButtonDown(float x, float y)
{
	IsInside(x, y);
	int pos = FindPos(_u, _v);
	if (!GInput.keys[DIK_LSHIFT] && !GInput.keys[DIK_RSHIFT])
	{
		_blockBegin = pos;
	}
	_blockEnd = pos;
	EnsureVisible(_blockEnd);
}

void C3DEdit::OnLButtonUp(float x, float y)
{
}

void C3DEdit::OnMouseMove(float x, float y, bool active)
{
	if (GInput.mouseL)
	{
		IsInside(x, y);
		_blockEnd = FindPos(_u, _v);
		EnsureVisible(_blockEnd);
	}
}

int C3DEdit::FindPos(float x, float y)
{
	if ((_style & ST_TYPE) == ST_MULTI)
	{
		if (_text.GetLength() == 0)
		{
			return 0;
		}
		else
		{
			int line = FirstLine() + toIntFloor(y / _maxLines);
			saturate(line, 0, _lines.Size() - 1);
			return _lines[line] + XToPos
			(
				GetLine(line), x
			);
		}
	}
	else
	{
		return _firstVisible + XToPos
		(
			_text.Substring(_firstVisible, _text.GetLength()), x
		);
	}
}

Vector3 C3DEdit::PosToDir(RString text, int pos) const
{
	Vector3 down;
	if ((_style & ST_TYPE) == ST_MULTI)
		down = (1.0 / _maxLines) * _down;
	else
		down = _down;
	Vector3 up = -_size * down;
	Vector3 right = 0.75 * up.Size() * _right.Normalized();
	Vector3 border = 0.02 * _right;

	Vector3 dir;
	int style = ST_LEFT;
	if ((_style & ST_TYPE) == ST_MULTI || _firstVisible == 0)
		style = _style & ST_HPOS;
	switch (style)
	{
	case ST_RIGHT:
		{
			Vector3 width = GEngine->GetText3DWidth(right, _font, text);
			dir = _right - width - border;
		}
		break;
	case ST_CENTER:
		{
			Vector3 width = GEngine->GetText3DWidth(right, _font, text);
			dir = 0.5 * (_right - width);
		}
		break;
	default:
		Assert((_style & ST_HPOS) == ST_LEFT)
		dir = border;
		break;
	}

	RString str = text.Substring(0, pos);
	return dir + GEngine->GetText3DWidth(right, _font, str);
}

float C3DEdit::PosToX(RString text, int pos) const
{
	Vector3 dir = PosToDir(text, pos);
	return dir.Size() / _right.Size();
}

int C3DEdit::XToPos(RString text, float x) const
{
	Vector3 down;
	if ((_style & ST_TYPE) == ST_MULTI)
		down = (1.0 / _maxLines) * _down;
	else
		down = _down;
	Vector3 up = -_size * down;
	Vector3 right = 0.75 * up.Size() * _right.Normalized();
	Vector3 border = 0.02 * _right;

	Vector3 dir;
	int style = ST_LEFT;
	if ((_style & ST_TYPE) == ST_MULTI || _firstVisible == 0)
		style = _style & ST_HPOS;
	switch (style)
	{
	case ST_RIGHT:
		{
			Vector3 width = GEngine->GetText3DWidth(right, _font, text);
			dir = _right - width - border;
		}
		break;
	case ST_CENTER:
		{
			Vector3 width = GEngine->GetText3DWidth(right, _font, text);
			dir = 0.5 * (_right - width);
		}
		break;
	default:
		Assert((_style & ST_HPOS) == ST_LEFT)
		dir = border;
		break;
	}

	x *= _right.Size();
	float dirSize = dir.Size();
	if (x < dirSize) return 0;

	int n = strlen(text);
	for (int i=0; i<n; i++)
	{
		int j = i;
		char c = text[i];
		Vector3 cw;
		if ((c & 0x80) && _font->GetLangID() == Korean && i + 1 < n)
		{
			char c2 = text[++i];
			char temp[3]; temp[0] = c; temp[1] = c2; temp[2] = 0;
			cw = GEngine->GetText3DWidth(right, _font, temp);;
		}
		else
		{
			char temp[2]; temp[0] = c; temp[1] = 0;
			cw = GEngine->GetText3DWidth(right, _font, temp);
		}
		
		float cwSize = cw.Size();
		if (dirSize + cwSize > x)
		{
			if (x - dirSize < dirSize + cwSize - x)
				return j;
			else
				return i + 1;
		}
		dirSize += cwSize;
	}
	return n;
}

void C3DEdit::EnsureVisible(int pos)
{
	if ((_style & ST_TYPE) == ST_MULTI)
	{
		if (_text.GetLength() == 0)
		{
			_firstVisible = 0;
			return;
		}

		int cur = CurLine();
		int first = FirstLine();
		saturateMin(first, cur);

		int lines = _lines.Size() - first;
		if (lines <= _maxLines)
		{
			first += lines - _maxLines;
			saturateMax(first, 0);
		}
		else if (first + _maxLines <= cur)
		{
			first = cur - _maxLines + 1;
		}
		_firstVisible = _lines[first];
	}
	else
	{
		Vector3 up = -_size * _down;
		Vector3 right = 0.75 * up.Size() * _right.Normalized();
		Vector3 border = 0.02 * _right;

		saturateMin(_firstVisible, _blockEnd);
		float wlimit = (_right - 2.0 * border).Size(); 
		float w = GEngine->GetText3DWidth
		(
			right, _font,
			_text.Substring(_firstVisible, _text.GetLength())
		).Size();
		if (w <= wlimit)
		{
			while (_firstVisible > 0)
			{
				int prev = PrevPos(_firstVisible);
				w += GEngine->GetText3DWidth
				(
					right, _font,
					_text.Substring(prev, _firstVisible)
				).Size();
				if (w > wlimit) break;
				_firstVisible = prev;
			}
		}
		else
		{
			w = GEngine->GetText3DWidth
			(
				right, _font,
				_text.Substring(_firstVisible, _blockEnd)
			).Size();
			while (w > wlimit)
			{
				int prev = _firstVisible;
				_firstVisible = NextPos(_firstVisible);
				w -= GEngine->GetText3DWidth
				(
					right, _font,
					_text.Substring(prev, _firstVisible)
				).Size();
			}
		}
	}
}

void C3DEdit::DrawText(const char *text, int offset, Vector3Par top, Vector3Par down, float alpha)
{
	// formatting
	Vector3 position = top + 0.5 * (1.0 - _size) * down;
	Vector3 up = -_size * down;
	Vector3 right = 0.75 * up.Size() * _right.Normalized();
	Vector3 border = 0.02 * _right;
	float rightSize = (_right - 2.0 * border).Size();
	float x2c = rightSize / right.Size();

	int pos = ST_LEFT;
	if ((_style & ST_TYPE) == ST_MULTI || _firstVisible == 0)
		pos = _style & ST_HPOS;
	switch (pos)
	{
	case ST_RIGHT:
		{
			Vector3 width = GEngine->GetText3DWidth(right, _font, text);
			position += _right - width - border;
		}
		break;
	case ST_CENTER:
		{
			Vector3 width = GEngine->GetText3DWidth(right, _font, text);
			position += 0.5 * (_right - width);
		}
		break;
	default:
		Assert(pos == ST_LEFT)
		position += border;
		break;
	}

	// block
	if (_blockBegin != _blockEnd)
	{
		int from, to;
		if (_blockBegin < _blockEnd)
		{
			from = _blockBegin;
			to = _blockEnd;
		}
		else
		{
			from = _blockEnd;
			to = _blockBegin;
		}
		if (from < offset + strlen(text) && to > offset)
		{
			PackedColor selColor = ModAlpha(_selColor, alpha);
			Vector3 x1 = VZero;
			char buffer[1024];
			if (from > offset)
			{
				strcpy(buffer, text);
				buffer[from - offset] = 0;
				x1 = GEngine->GetText3DWidth(right, _font, buffer);
			}
			strcpy(buffer, text);
			if (to < offset + strlen(text))
				buffer[to - offset] = 0;
			Vector3 x2 = GEngine->GetText3DWidth(right, _font, buffer);
			if (x2.Size() > rightSize) x2 = _right - 2.0 * border;
			GEngine->Draw3D
			(
				position + x1, -up, x2 - x1,
				ClipAll, selColor, DisableSun, NULL,
				0, 0, 1, 1
			);
		}
	}
	
	// text
	PackedColor ftColor = ModAlpha(_ftColor, alpha);
	GEngine->DrawText3D
	(
		position, up, right, ClipAll, _font, ftColor, DisableSun, text,
		0, 0, x2c, 1
	);
}

void C3DEdit::FormatText()
{
	if ((_style & ST_TYPE) != ST_MULTI) return;

	_lines.Clear();
	_lines.Add(0);

	Vector3	down = (1.0 / _maxLines) * _down;
	Vector3 up = -_size * down;
	Vector3 right = 0.75 * up.Size() * _right.Normalized();
	Vector3 border = 0.02 * _right;

	float lineWidth = (_right - 2.0 * border).Size();

	const char *p = _text;
	while (*p != 0)
	{
		// begin of the line
		const char *word = p;
		int n = 0;
		float width = 0;
		while (true)
		{
			const char *q = p;
			char c = *p++;
			if (c == 0) return;
			if (ISSPACE(c))
			{
				n++;
				word = p;
			}

			if ((c & 0x80) && _font->GetLangID() == Korean && *p != 0)
			{
				// TODO: simplify
				char temp[3]; temp[0] = c; temp[1] = *p++; temp[2] = 0;
				width += GEngine->GetText3DWidth(right, _font, temp).Size();
			}
			else
			{
				// TODO: simplify
				char temp[2]; temp[0] = c; temp[1] = 0;
				width += GEngine->GetText3DWidth(right, _font, temp).Size();
			}

			if (width > lineWidth)
			{
				if (n > 0)
				{
					p = word;
				}
				else
				{
					p = q;
				}
				_lines.Add(p - (const char *)_text);
				break;
			}
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
// class C3DActiveText

C3DActiveText::C3DActiveText(ControlsContainer *parent, int idc, const ParamEntry &cls)
: Control3D(parent, CT_3DACTIVETEXT, idc, cls)
{
	_texture = NULL;
	RString text = cls >> "text";
	SetText(text);
	_font = GLOB_ENGINE->LoadFont(GetFontID(cls >> "font"));
	_color = GetPackedColor(cls >> "color");
	_colorActive = GetPackedColor(cls >> "colorActive");

	GetValue(_enterSound, cls >> "soundEnter");
	GetValue(_pushSound, cls >> "soundPush");
	GetValue(_clickSound, cls >> "soundClick");
	GetValue(_escapeSound, cls >> "soundEscape");

	_active = false;

	const ParamEntry *entry = cls.FindEntry("action");
	if (entry) _action = *entry;
}

void C3DActiveText::SetText(RString text)
{
	if (!strcmp(_text, text)) return;

	_text = text;
	if ((_style & ST_TYPE) == ST_PICTURE)
	{
		text = FindPicture(text);
		text.Lower();
		_texture = GlobLoadTexture(text);
		if (_texture) _texture->SetMaxSize(1024); // no limits
	}
}

bool C3DActiveText::OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	if (nChar == VK_RETURN)
	{
		PlaySound(_pushSound);
		return true;
	}
	return false;
}

bool C3DActiveText::OnKeyUp(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	if (nChar == VK_RETURN)
	{
		PlaySound(_clickSound);
		if (_action.GetLength() > 0)
		{
			GameState *gstate = GWorld->GetGameState();
			gstate->Execute(_action);
			GWorld->SimulateScripts();
		}
		_parent->OnButtonClicked(IDC());
		return true;
	}
	return false;
}

void C3DActiveText::OnMouseMove(float x, float y, bool active)
{
	OnMouseHold(x, y, active);
}

void C3DActiveText::OnMouseHold(float x, float y, bool active)
{
	bool newActive = active && IsInside(x, y);
	if (newActive && !_active) PlaySound(_enterSound);
	_active = newActive;
}

void C3DActiveText::OnLButtonDown(float x, float y)
{
	PlaySound(_pushSound);
}

void C3DActiveText::OnLButtonUp(float x, float y)
{
	if (IsInside(x, y))
	{
		PlaySound(_clickSound);
		if (_action.GetLength() > 0)
		{
			GameState *gstate = GWorld->GetGameState();
			gstate->Execute(_action);
			GWorld->SimulateScripts();
		}
		_parent->OnButtonClicked(IDC());
	}
	else
	{
		PlaySound(_escapeSound);
	}
}

void C3DActiveText::OnDraw(float alpha)
{
	PackedColor color = ModAlpha(_active ? _colorActive : _color, alpha);
	Vector3 normal = _down.CrossProduct(_right).Normalized(); 
	Vector3 position = _position - 0.002 * normal;

	switch (_style & ST_TYPE)
	{
	case ST_PICTURE:
		GEngine->Draw3D
		(
			position, _down, _right, ClipAll, color, DisableSun, _texture
		);
		break;
	default:
		{
			Vector3 up = -_down;
			Vector3 right = 0.75 * up.Size() * _right.Normalized();

			Vector3 offset = VZero;
			Vector3 width = GEngine->GetText3DWidth
			(
				right, _font, _text
			);
			switch (_style & ST_HPOS)
			{
				case ST_RIGHT:
					offset = _right - width;
					break;
				case ST_CENTER:
					offset = 0.5 * (_right - width);
					break;
				default:
					Assert((_style & ST_HPOS) == ST_LEFT)
					break;
			}
			position += offset;

			float invRSize = 1.0 / right.Size();
			float x1c = 0, x2c = _right.Size() * invRSize;
			bool clip = width.SquareSize() > _right.SquareSize();
			if (clip)
			{
				float offsetSize = offset.Size() * invRSize;
				x1c += offsetSize;
				x2c += offsetSize;
			}
			
			bool focused = IsFocused();
			bool selected = IsDefault() && !_parent->GetFocused()->CanBeDefault();
			if (focused || selected)
			{
				PackedColor col;
				if (focused) col = color;
				else col = ModAlpha(color, 0.5);

				if (clip)
					GEngine->DrawLine3D
					(
						position - offset + _down, position - offset + _down + _right,
						col, DisableSun
					);
				else
					GEngine->DrawLine3D
					(
						position + _down, position + _down + width,
						col, DisableSun
					);
			}

			GEngine->DrawText3D
			(
				position, up, right, ClipAll, _font, color, DisableSun, _text,
				x1c, 0, x2c, 1 
			);
		}
		break;
	}
}

///////////////////////////////////////////////////////////////////////////////
// class C3DSlider

C3DSlider::C3DSlider(ControlsContainer *parent, int idc, const ParamEntry &cls)
: Control3D(parent, CT_3DSLIDER, idc, cls), CSliderContainer(cls)
{
}

void C3DSlider::OnLButtonDown(float x, float y)
{
	IsInside(x, y);		// calculate _u, _v

	if ((_style & SL_DIR) == SL_VERT)
	{
/*
		float spinHeight = (1.33 * 0.6) * _w;
		const float thumbHeight = 0.02;
		float top = _y + spinHeight + 0.5 * thumbHeight;
		float fieldHeight = _h - 2 * spinHeight - thumbHeight;

		float coef = 0;
		if (_maxPos > _minPos)
			coef = 1.0 / (_maxPos - _minPos);
		float thumbPos = top + fieldHeight * (_curPos - _minPos) * coef;

		if (y - _y < spinHeight)
		{
			SetThumbPos(_curPos - _lineStep);
			if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
		}
		else if (y - _y > _h - spinHeight)
		{
			SetThumbPos(_curPos + _lineStep);
			if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
		}
		else if (y < thumbPos - 0.5 * thumbHeight)
		{
			SetThumbPos(_curPos - _pageStep);
			if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
		}
		else if (y > thumbPos + 0.5 * thumbHeight)
		{
			SetThumbPos(_curPos + _pageStep);
			if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
		}
		else
		{
			_thumbLocked = true;
			_thumbOffset = y - thumbPos;
		}
*/
	}
	else
	{
		float ratio = _down.Size() / _right.Size();
		float spinWidth = 0.6 * ratio;
		float thumbWidth = 1.0 * ratio;
		float left = spinWidth + 0.5 * thumbWidth;
		float fieldWidth = 1.0 - 2 * spinWidth - thumbWidth;

		float coef = 0;
		if (_maxPos > _minPos)
			coef = 1.0 / (_maxPos - _minPos);
		float thumbPos = left + fieldWidth * (_curPos - _minPos) * coef;

		if (_u < spinWidth)
		{
			SetThumbPos(_curPos - _lineStep);
			if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
		}
		else if (_u > 1.0 - spinWidth)
		{
			SetThumbPos(_curPos + _lineStep);
			if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
		}
		else if (_u < thumbPos - 0.5 * thumbWidth)
		{
			SetThumbPos(_curPos - _pageStep);
			if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
		}
		else if (_u > thumbPos + 0.5 * thumbWidth)
		{
			SetThumbPos(_curPos + _pageStep);
			if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
		}
		else
		{
			_thumbLocked = true;
			_thumbOffset = _u - thumbPos;
		}

/*
		float spinWidth = (0.75 * 0.6) * _h;
		const float thumbWidth = 0.015;
		float left = _x + spinWidth + 0.5 * thumbWidth;
		float fieldWidth = _w - 2 * spinWidth - thumbWidth;

		float coef = 0;
		if (_maxPos > _minPos)
			coef = 1.0 / (_maxPos - _minPos);
		float thumbPos = left + fieldWidth * (_curPos - _minPos) * coef;

		if (x - _x < spinWidth)
		{
			SetThumbPos(_curPos - _lineStep);
			if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
		}
		else if (x - _x > _w - spinWidth)
		{
			SetThumbPos(_curPos + _lineStep);
			if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
		}
		else if (x < thumbPos - 0.5 * thumbWidth)
		{
			SetThumbPos(_curPos - _pageStep);
			if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
		}
		else if (x > thumbPos + 0.5 * thumbWidth)
		{
			SetThumbPos(_curPos + _pageStep);
			if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
		}
		else
		{
			_thumbLocked = true;
			_thumbOffset = x - thumbPos;
		}
*/
	}
}

void C3DSlider::OnLButtonUp(float x, float y)
{
	_thumbLocked = false;
}

void C3DSlider::OnMouseMove(float x, float y, bool active)
{
	IsInside(x, y);		// calculate _u, _v

	if (_thumbLocked)
	{
		if ((_style & SL_DIR) == SL_VERT)
		{
/*
			float spinHeight = (1.33 * 0.7) * _w;
			const float thumbHeight = 0.02;
			float top = _y + spinHeight + 0.5 * thumbHeight;
			float fieldHeight = _h - 2 * spinHeight - thumbHeight;

			float coef = 0;
			if (_maxPos > _minPos)
				coef = _maxPos - _minPos;

			if (y - _thumbOffset < top)
			{
				SetThumbPos(_minPos);
				if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
			}
			else if (y - _thumbOffset > top + fieldHeight)
			{
				SetThumbPos(_maxPos);
				if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
			}
			else
			{
				SetThumbPos(_minPos + (y - _thumbOffset - top) * coef / fieldHeight);
				if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
			}
*/
		}
		else
		{
			float ratio = _down.Size() / _right.Size();
			float spinWidth = 0.6 * ratio;
			float thumbWidth = 1.0 * ratio;
			float left = spinWidth + 0.5 * thumbWidth;
			float fieldWidth = 1.0 - 2 * spinWidth - thumbWidth;

			float coef = 0;
			if (_maxPos > _minPos)
				coef = _maxPos - _minPos;

			float thumbPos = _u - _thumbOffset;
			if (thumbPos < left)
				SetThumbPos(_minPos);
			else if (thumbPos > left + fieldWidth)
				SetThumbPos(_maxPos);
			else
				SetThumbPos(_minPos + (thumbPos - left) * coef / fieldWidth);
			if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);

/*
			float spinWidth = (0.75 * 0.7) * _h;
			const float thumbWidth = 0.015;
			float left = _x + spinWidth + 0.5 * thumbWidth;
			float fieldWidth = _w - 2 * spinWidth - thumbWidth;

			float coef = 0;
			if (_maxPos > _minPos)
				coef = _maxPos - _minPos;

			if (x - _thumbOffset < left)
			{
				SetThumbPos(_minPos);
				if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
			}
			else if (x - _thumbOffset > left + fieldWidth)
			{
				SetThumbPos(_maxPos);
				if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
			}
			else
			{
				SetThumbPos(_minPos + (x - _thumbOffset - left) * coef / fieldWidth);
				if (_parent) _parent->OnSliderPosChanged(IDC(), _curPos);
			}
*/
		}
	}
}

void C3DSlider::OnDraw(float alpha)
{
	PackedColor color = ModAlpha(_color, alpha);
	Vector3 normal = _down.CrossProduct(_right).Normalized(); 
	Vector3 position = _position - 0.002 * normal;

	if ((_style & SL_DIR) == SL_VERT)
	{
		Vector3 bigLine = 0.6 * _right;
		Vector3 smallLine = 0.3 * _right;
		Vector3 space = 0.15 * _right;
		Vector3 thumbWidth = _right - bigLine - space;

		float w = _right.Size();
		Vector3 downNorm = _down.Normalized();
		Vector3 spinHeight = 0.6 * w * downNorm;
		Vector3 thumbHeight = w * downNorm;
		Vector3 fieldHeight = _down - 2.0 * spinHeight - thumbHeight;

		Vector3 top = position + spinHeight + 0.5 * thumbHeight;
		Vector3 bottom = top + fieldHeight;

		float coef = 0;
		if (_maxPos > _minPos)
			coef = 1.0 / (_maxPos - _minPos);
		Vector3 thumbPos = top + fieldHeight * (_curPos - _minPos) * coef + bigLine + space;

		// draw left spin
		Vector3 center = position + 0.5 * bigLine;
		GEngine->DrawLine3D(center, position + spinHeight, color, DisableSun);
		GEngine->DrawLine3D(position + spinHeight, position + spinHeight + bigLine, color, DisableSun);
		GEngine->DrawLine3D(position + spinHeight + bigLine, center, color, DisableSun);

		// draw right spin
		center += _down;
		GEngine->DrawLine3D(center, position + _down - spinHeight, color, DisableSun);
		GEngine->DrawLine3D(position + _down - spinHeight, position + _down - spinHeight + bigLine, color, DisableSun);
		GEngine->DrawLine3D(position + _down - spinHeight + bigLine, center, color, DisableSun);

		// draw background
		GEngine->DrawLine3D(top + bigLine, bottom + bigLine, color, DisableSun);
		int lines = 1;
		float lineDist = fieldHeight.Size();
		while (lineDist > w)
		{
			lines *= 2;
			lineDist *= 0.5;
		}
		Vector3 pos = top + bigLine;
		Vector3 diff = lineDist * downNorm;
		for (int i=0; i<=lines; i++)
		{
			Vector3 right = (i % 2 == 0 || lines == 1) ? bigLine : smallLine;
			GEngine->DrawLine3D(pos, pos - right, color, DisableSun);
			pos += diff;
		}

		// draw thumb
		GEngine->DrawLine3D(thumbPos, thumbPos + thumbWidth - 0.5 * thumbHeight, color, DisableSun);
		GEngine->DrawLine3D(thumbPos + thumbWidth - 0.5 * thumbHeight, thumbPos + thumbWidth + 0.5 * thumbHeight, color, DisableSun);
		GEngine->DrawLine3D(thumbPos + thumbWidth + 0.5 * thumbHeight, thumbPos, color, DisableSun);
	}
	else
	{
		Vector3 bigLine = 0.6 * _down;
		Vector3 smallLine = 0.3 * _down;
		Vector3 space = 0.15 * _down;
		Vector3 thumbHeight = _down - bigLine - space;

		float h = _down.Size();
		Vector3 rightNorm = _right.Normalized();
		Vector3 spinWidth = 0.6 * h * rightNorm;
		Vector3 thumbWidth = h * rightNorm;
		Vector3 fieldWidth = _right - 2.0 * spinWidth - thumbWidth;

		Vector3 left = position + spinWidth + 0.5 * thumbWidth;
		Vector3 right = left + fieldWidth;

		float coef = 0;
		if (_maxPos > _minPos)
			coef = 1.0 / (_maxPos - _minPos);
		Vector3 thumbPos = left + fieldWidth * (_curPos - _minPos) * coef + bigLine + space;

		// draw left spin
		Vector3 center = position + 0.5 * bigLine;
		GEngine->DrawLine3D(center, position + spinWidth, color, DisableSun);
		GEngine->DrawLine3D(position + spinWidth, position + spinWidth + bigLine, color, DisableSun);
		GEngine->DrawLine3D(position + spinWidth + bigLine, center, color, DisableSun);

		// draw right spin
		center += _right;
		GEngine->DrawLine3D(center, position + _right - spinWidth, color, DisableSun);
		GEngine->DrawLine3D(position + _right - spinWidth, position + _right - spinWidth + bigLine, color, DisableSun);
		GEngine->DrawLine3D(position + _right - spinWidth + bigLine, center, color, DisableSun);

		// draw background
		GEngine->DrawLine3D(left + bigLine, right + bigLine, color, DisableSun);
		int lines = 1;
		float lineDist = fieldWidth.Size();
		while (lineDist > h)
		{
			lines *= 2;
			lineDist *= 0.5;
		}
		Vector3 pos = left + bigLine;
		Vector3 diff = lineDist * rightNorm;
		for (int i=0; i<=lines; i++)
		{
			Vector3 down = (i % 2 == 0 || lines == 1) ? bigLine : smallLine;
			GEngine->DrawLine3D(pos, pos - down, color, DisableSun);
			pos += diff;
		}

		// draw thumb
		GEngine->DrawLine3D(thumbPos, thumbPos + thumbHeight - 0.5 * thumbWidth, color, DisableSun);
		GEngine->DrawLine3D(thumbPos + thumbHeight - 0.5 * thumbWidth, thumbPos + thumbHeight + 0.5 * thumbWidth, color, DisableSun);
		GEngine->DrawLine3D(thumbPos + thumbHeight + 0.5 * thumbWidth, thumbPos, color, DisableSun);
	}
}

///////////////////////////////////////////////////////////////////////////////
// class C3DScrollBar (used in 3DListBox etc.)

C3DScrollBar::C3DScrollBar()
{
	_thumbLocked = false;
}

void C3DScrollBar::OnLButtonDown(float v)
{
	float down = _down.Size();

	// spins
	float spinSize = 0.8 * _right.Size();
	if (2.0 * spinSize >= down) return; // too width scrollbar
	if (_maxPos <= _minPos) return;

	float y = down * v;

	float invRange = (down - 2.0f * spinSize) / (_maxPos - _minPos);
	float thumbSize = _page * invRange;
	float thumbPos = spinSize + _curPos * invRange;

	if (y <= spinSize)
	{
		// lineUp
		_curPos -= _lineStep;
	}
	else if (y <= thumbPos)
	{
		// pageUp
		_curPos -= _pageStep;
	}
	else if (y <= thumbPos + thumbSize)
	{
		// thumb
		_thumbLocked = true;
		_thumbOffset = y - thumbPos;
	}
	else if (y <= down - spinSize)
	{
		// pageDown
		_curPos += _pageStep;
	}
	else
	{
		// lineDown
		_curPos += _lineStep;
	}
	saturate(_curPos, _minPos, _maxPos - _page);
}

void C3DScrollBar::OnLButtonUp()
{
	_thumbLocked = false;
}

void C3DScrollBar::OnMouseHold(float v)
{
	if (_thumbLocked)
	{
		float down = _down.Size();

		// spins
		float spinSize = 0.8 * _right.Size();
		if (2.0 * spinSize >= down) return; // too width scrollbar
		if (_maxPos <= _minPos) return;

		float y = down * v;
		float range = (_maxPos - _minPos) / (down - 2.0f * spinSize);
		float thumbPos = y - _thumbOffset;
		_curPos = (thumbPos - spinSize) * range;
		saturate(_curPos, _minPos, _maxPos - _page);
	}
}

void C3DScrollBar::OnDraw(float alpha)
{
	PackedColor color = ModAlpha(_color, alpha);

	// frame
	Vector3 posTL = _position;
	Vector3 posTR = posTL + _right;
	Vector3 posBL = posTL + _down;
	Vector3 posBR = posTR + _down;
	GEngine->DrawLine3D(posTL, posTR, color, DisableSun);
	GEngine->DrawLine3D(posTR, posBR, color, DisableSun);
	GEngine->DrawLine3D(posBR, posBL, color, DisableSun);
	GEngine->DrawLine3D(posBL, posTL, color, DisableSun);

	// spins
	float spinSize = 0.8 * _right.Size();
	if (2.0 * spinSize >= _down.Size()) return; // too width scrollbar

	Vector3 borderX = 0.25 * _right;
	Vector3 centerX = 0.5 * _right;
	Vector3 borderY = 0.25 * spinSize * _down.Normalized();
	Vector3 top = _position + spinSize * _down.Normalized();
	GEngine->DrawLine3D(top, top + _right, color, DisableSun);
	GEngine->DrawLine3D(top + borderX - borderY, _position + centerX + borderY, color, DisableSun);
	GEngine->DrawLine3D(_position + centerX + borderY, top + _right - borderX - borderY, color, DisableSun);
	Vector3 bottom = _position + _down - spinSize * _down.Normalized();
	GEngine->DrawLine3D(bottom, bottom + _right, color, DisableSun);
	GEngine->DrawLine3D(bottom + borderX + borderY, _position + _down + centerX - borderY, color, DisableSun);
	GEngine->DrawLine3D(_position + _down + centerX - borderY, bottom + _right - borderX + borderY, color, DisableSun);

	// thumb
	if (_maxPos <= _minPos) return;
	Vector3 invRange = (bottom - top) * ( 1/ (_maxPos - _minPos) );
	Vector3 thumbSize = _page * invRange;
	Vector3 thumbPos = top + _curPos * invRange;

	posTL = thumbPos + 0.2 * borderX + 0.2 * borderY;
	posTR = posTL + (_right - 0.4 * borderX);
	posBL = posTL + (thumbSize - 0.4 * borderY);
	posBR = posTR + (thumbSize - 0.4 * borderY);
	GEngine->DrawLine3D(posTL, posTR, color, DisableSun);
	GEngine->DrawLine3D(posTR, posBR, color, DisableSun);
	GEngine->DrawLine3D(posBR, posBL, color, DisableSun);
	GEngine->DrawLine3D(posBL, posTL, color, DisableSun);
}

///////////////////////////////////////////////////////////////////////////////
// class C3DListBox

C3DListBox::C3DListBox(ControlsContainer *parent, int idc, const ParamEntry &cls)
	: Control3D(parent, CT_3DLISTBOX, idc, cls), CListBoxContainer(cls)
{
	_selBgColor = GetPackedColor(cls >> "colorSelectBackground");

	_font = GLOB_ENGINE->LoadFont(GetFontID(cls>>"font"));
	_size = cls >> "size";
	_rows = cls >> "rows";

	_scrollUp = false;
	_scrollDown = false;
	_dragging = false;

	_sb3DWidth = 0.10;

	_scrollbar.SetPosition
	(
		_position + (1.0 - _sb3DWidth) * _right,
		_sb3DWidth * _right,
		_down
	);
	_scrollbar.SetColor(_ftColor);
	_scrollbar.SetRange(0, 0, _rows);	
	_scrollbar.SetPos(0);
	_scrollbar.SetSpeed(1, floatMax(_rows - 1, 1));	

	_colorPicture = false;
}

/*!
\patch 1.36 Date 12/12/2001 by Jirka
- Fixed: Some rows in listboxes was sometimes invisible. (Secondary campaign list bug.)
*/
void C3DListBox::SetCurSel(int sel, bool sendUpdate)
{
	if (!sendUpdate && sel == _selString)
	{
		saturateMin(_topString, GetSize() - _rows);
		saturateMax(_topString, 0);
		return;
	}

	if (GetSize() == 0)
		_selString = -1;
	else if (sel < 0)
		_selString = 0;
	else if (sel >= GetSize())
		_selString = GetSize() - 1;
	else
		_selString = sel;

	if (_selString < _topString)
	{
		_topString = _selString;
		saturateMin(_topString, GetSize() - _rows);
		saturateMax(_topString, 0);
	}
	else if (_selString > _topString + _rows - 1)
		_topString = _selString - _rows + 1;

	if (sendUpdate)
	{
		OnSelChanged(_selString);
		_parent->OnLBSelChanged(IDC(), _selString);
	}

	Check();
}

bool C3DListBox::OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	if (IsReadOnly()) return false;

	switch (nChar)
	{
	case VK_UP:
		SetCurSel(GetCurSel() - 1);
		return true;
	case VK_DOWN:
		SetCurSel(GetCurSel() + 1);
		return true;
	}
	return false;
}

/*!
\patch 1.12 Date 08/08/2001 by Jirka
- Fixed: drag & drop empty item in listbox disabled
*/
void C3DListBox::OnLButtonDown(float x, float y)
{
	// if (IsReadOnly()) return;

	IsInside(x, y);
	if (_scrollbar.IsEnabled() && _u > (1.0 - _sb3DWidth))
	{
		_scrollbar.SetPos(_topString);
		_scrollbar.OnLButtonDown(_v);
		_topString = _scrollbar.GetPos();
	}
	else if (_parent)
	{
		float index = _v * _rows;
		// FIX
		if (index >= 0 && index < _rows && _topString + index < GetSize())
		{
			_parent->OnLBDrag(IDC(), toIntFloor(_topString + index));
			_dragging = true;
		}
	}
}

void C3DListBox::OnLButtonUp(float x, float y)
{
	if (_scrollbar.IsLocked())
	{
		_scrollbar.OnLButtonUp();
	}
	else if (!IsReadOnly() && IsInside(x, y))
	{
		if (_scrollbar.IsEnabled() && _u > (1.0 - _sb3DWidth))
		{
		}
		else
		{
			float index = _v * _rows;
			if (index >= 0 && index < _rows)
			{
				SetCurSel(toIntFloor(_topString + index));
			}
		}
	}

	if (_dragging)
	{
		if (_parent) _parent->OnLBDrop(x, y);
		_dragging = false;
	}
}

void C3DListBox::OnMouseZChanged(float dz)
{
	// if (IsReadOnly()) return;

	CListBoxContainer::OnMouseZChanged(dz);
}

void C3DListBox::OnMouseMove(float x, float y, bool active)
{
	// if (IsReadOnly()) return;

	if (!GInput.mouseL) return;

	OnMouseHold(x, y, active);
}

void C3DListBox::OnMouseHold(float x, float y, bool active)
{
	if (!GInput.mouseL) return;

	IsInside(x, y);

	if (_scrollbar.IsEnabled() && _scrollbar.IsLocked())
	{
		_scrollbar.SetPos(_topString);
		_scrollbar.OnMouseHold(_v);
		_topString = _scrollbar.GetPos();
	}
	else if (_scrollbar.IsEnabled() && _u > (1.0 - _sb3DWidth))
	{
	}
/*
	// avoid multiple SetCurSel
	else if (!IsReadOnly())
	{
		float index = _v * _rows;
		if (index >= 0 && index < _rows)
		{
			SetCurSel(toIntFloor(_topString + index));
		}
	}
*/

	if (_dragging)
	{
		if (active && _parent) _parent->OnLBDragging(x, y);
	}
}

void C3DListBox::OnLButtonDblClick(float x, float y)
{
	if (IsReadOnly()) return;

	if (_selString < 0) return;

	IsInside(x, y);
	if (_scrollbar.IsEnabled() && _u > (1.0 - _sb3DWidth)) return;

	_parent->OnLBDblClick(IDC(), _selString);
}

void C3DListBox::SetCurSel(float x, float y, bool sendUpdate)
{
	if (!IsInside(x, y)) return;

	float index = _v * _rows;
	if (index >= 0 && index < _rows)
	{
		SetCurSel(toIntFloor(_topString + index), sendUpdate);
	}
}

void C3DListBox::OnDraw(float alpha)
{
	Vector3 normal = _down.CrossProduct(_right).Normalized(); 

	PackedColor ftColor = ModAlpha(_ftColor, alpha);
	Vector3 posTL = _position - 0.002 * normal;
	Vector3 posTR = posTL + _right;
	Vector3 posBL = posTL + _down;
	Vector3 posBR = posTR + _down;

	GEngine->DrawLine3D(posTL, posTR, ftColor, DisableSun);
	GEngine->DrawLine3D(posTR, posBR, ftColor, DisableSun);
	GEngine->DrawLine3D(posBR, posBL, ftColor, DisableSun);
	GEngine->DrawLine3D(posBL, posTL, ftColor, DisableSun);
	
	if (GetSize() > _rows)
	{
		_scrollbar.Enable(true);
		_scrollbar.SetPosition
		(
			posTL + (1.0 - _sb3DWidth) * _right,
			_sb3DWidth * _right,
			_down
		);
		_scrollbar.SetRange(0, GetSize(), _rows);
		_scrollbar.SetPos(_topString);
		_scrollbar.OnDraw(alpha);
	}
	else
	{
		_scrollbar.Enable(false);
	}

	float invItems = 1.0 / _rows;
	Vector3 down = invItems * _down;

	const float eps = 1e-6;
	int i = toIntFloor(_topString + eps);
	int iMax = toIntCeil(floatMin(_topString + _rows, GetSize()) - eps);
	Vector3 position = _position - (_topString - i) * down;

	saturateMin(iMax, GetSize());

	while (i < iMax)
	{
		DrawItem
		(
			position, down, i, alpha
		);
		position += down;
		i++;
	}
}

void C3DListBox::DrawItem
(
	Vector3Par position, Vector3Par down, int i, float alpha
)
{
	float y1c = 0;
	float y2c = 1;
	if (i < _topString) y1c = _topString - i;
	if (i > _topString + _rows - 1) y2c = _topString + _rows - i;
	
	Vector3 normal = _down.CrossProduct(_right).Normalized(); 

	Vector3 rightSB = _right;
	if (GetSize() > _rows)
		rightSB = (1.0 - _sb3DWidth) * _right;
	float rightSBSize = rightSB.Size();
	Vector3 border = 0.02 * _right;

	Vector3 curPos = position - 0.002 * normal;

	bool selected = i == GetCurSel() && IsEnabled();
	PackedColor color;
	if (selected && _showSelected)
	{
		PackedColor selBgColor = ModAlpha(_selBgColor, alpha);
		GEngine->Draw3D
		(
			curPos, down, rightSB, ClipAll, selBgColor, DisableSun, NULL,
			0, y1c, 1, y2c
		);
		color = ModAlpha(GetSelColor(i), alpha);
	}
	else
		color = ModAlpha(GetFtColor(i), alpha);

	curPos = position - 0.003 * normal;

	Texture *texture = GetTexture(i);
	if (texture)
	{
		PackedColor picColor = color;
		if (_colorPicture) picColor = ModAlpha(PackedWhite, alpha);

		Vector3 right = (float)texture->AWidth() / (float)texture->AHeight() * down.Size() * _right.Normalized();
		float rightSize = right.Size();
		float x2c = 1;
		if (rightSize > rightSBSize) x2c = rightSBSize / rightSize;
		GEngine->Draw3D
		(
			curPos, down, right, ClipAll, picColor, // PackedColor(Color(1, 1, 1, alpha)),
			DisableSun, texture,
			0, y1c, x2c, y2c
		);
		curPos += right;
		rightSBSize -= rightSize;
		if (rightSBSize <= 0) return;
	}

	RString text = GetText(i);
	float top = 0.5 * (1.0 - _size);
	Vector3 pos = curPos + top * down + border;
	Vector3 up = -_size * down;
	Vector3 right = 0.75 * up.Size() * _right.Normalized();
	float x2c = (rightSBSize - 2.0 * border.Size()) / right.Size();
	float y1ct = 0;
	float y2ct = 1;
	if (y1c > top)
		y1ct = (y1c - top) / _size;
	if (y2c < top + _size)
		y2ct = (y2c - top) / _size;
	GEngine->DrawText3D
	(
		pos, up, right, ClipAll, _font, color, DisableSun, text,
		0, y1ct, x2c, y2ct
	);
}

void C3DListBox::UpdateInfo(ControlObject *object, ControlInObject &info)
{
	Control3D::UpdateInfo(object, info);

}

