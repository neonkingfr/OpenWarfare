#include "wpch.hpp"

#include "world.hpp"
#include "landscape.hpp"
#include <El/ParamFile/paramFile.hpp>
#include "vehicleAI.hpp"
#include "ai.hpp"
#include "arcadeTemplate.hpp"
#include "global.hpp"
#include "visibility.hpp"
#include "keyInput.hpp"
#include "dikCodes.h"
#include "gameStateExt.hpp"

extern SRef<VehicleWithAI> GDummyVehicle;
void AIGlobalInit();

class ObjectViewer: public Vehicle
{
	typedef Vehicle base;

	float _animPhase,_animSpeed;

	public:
	ObjectViewer( LODShapeWithShadow *shape, RString name );
	~ObjectViewer();

	void Simulate( float deltaT, SimulationImportance prec );
	void Draw( int forceLOD, ClipFlags clipFlags, const FrameBase &pos );

	void Animate( int level );
	void Deanimate( int level );

	bool IsAnimated( int level ) const {return true;}
	bool IsAnimatedShadow( int level ) const {return true;}

	float GetPhase() const {if (_animSpeed<0.01) return _animPhase;else return -1;}
	void SetPhase( float phase ) {_animPhase=phase,_animSpeed=0;}
};

ObjectViewer::ObjectViewer( LODShapeWithShadow *shape, RString name )
:base(shape,VehicleTypes.New(name),-1)
{
	_animPhase =0;
	_animSpeed = 0.5;
}

ObjectViewer::~ObjectViewer()
{
}

class CameraViewer: public Vehicle
{
	OLink<ObjectViewer> _tgt;
	typedef Vehicle base;

	public:
	CameraViewer( ObjectViewer *tgt, LODShapeWithShadow *shape, RString name );
	~CameraViewer();

	ObjectViewer *GetTarget() const {return _tgt;}
	void SetTarget( ObjectViewer *tgt ) {_tgt=tgt;}

	void Simulate( float deltaT, SimulationImportance prec );
	void Draw( int forceLOD, ClipFlags clipFlags, const FrameBase &pos );

	void LimitVirtual
	(
		CameraType camType, float &heading, float &dive, float &fov
	) const;
	void InitVirtual
	(
		CameraType camType, float &heading, float &dive, float &fov
	) const;
	bool IsContinuous( CameraType camType ) const {return true;}
};

CameraViewer::CameraViewer( ObjectViewer *tgt, LODShapeWithShadow *shape, RString name )
:base(shape,VehicleTypes.New(name),-1),
_tgt(tgt)
{
}

CameraViewer::~CameraViewer()
{
}

static Vector3 InitCamPos=Vector3(0,0,-10);

void CameraViewer::Simulate( float deltaT, SimulationImportance prec )
{
	float scale = GInput.keys[DIK_LCONTROL] || GInput.keys[DIK_RCONTROL] ? 0.1 : 1;
	float moveX = 0;
	float moveY = 0;
	float moveZ = 0;

	deltaT*=scale;

	float turnY = 0;

	if (GInput.mouseL)
	{
		moveX = +GInput.mouseX * 5*deltaT;
		moveZ = -GInput.mouseY * 5*deltaT;
	}

	moveX += (GInput.keys[DIK_RIGHT]-GInput.keys[DIK_LEFT])*4*deltaT;
	moveY += (GInput.keys[DIK_UP]-GInput.keys[DIK_DOWN])*4*deltaT;

	moveZ +=
	(
		GInput.keys[DIK_E]*8+
		GInput.keys[DIK_W]*2-
		GInput.keys[DIK_S]*2
	)*deltaT;
	moveY +=
	(
		GInput.keys[DIK_Q]*2-
		GInput.keys[DIK_Z]*2
	)*deltaT;
	moveX +=
	(
		GInput.keys[DIK_C]*2-
		GInput.keys[DIK_X]*2
	)*deltaT;
	turnY +=
	(
		GInput.keys[DIK_D]*2-
		GInput.keys[DIK_A]*2
	)*deltaT;

	Vector3 pos = Position();
	Matrix3 orient = Orientation();

	pos += moveX*DirectionAside();
	pos += moveY*DirectionUp();
	pos += moveZ*Direction();

	Matrix3 rot(MRotationY,-turnY);
	orient = orient*rot;
	if (GInput.keys[DIK_NUMPAD5])
	{
		if (_tgt)
		{
			pos = _tgt->Position()+InitCamPos;
		}
		orient.SetIdentity();
	}

	SetPosition(pos);
	SetOrientation(orient);
}

void CameraViewer::Draw( int forceLOD, ClipFlags clipFlags, const FrameBase &pos )
{
}

#define TEST_TEXT 0

#if TEST_TEXT
#include "engine.hpp"
#endif

void ObjectViewer::Animate( int level )
{
	Shape *shape = _shape->Level(level);
	if (shape->IsAnimated())
	{
		shape->SetPhase(_animPhase,0);
	}
}

void ObjectViewer::Deanimate( int level )
{
}

void ObjectViewer::Draw( int forceLOD, ClipFlags clipFlags, const FrameBase &pos )
{
	base::Draw(forceLOD,clipFlags,pos);
}

void ObjectViewer::Simulate( float deltaT, SimulationImportance prec )
{
	float scale = GInput.keys[DIK_LCONTROL] || GInput.keys[DIK_RCONTROL] ? 0.1 : 1;

	deltaT*=scale;

	_animPhase += deltaT*_animSpeed;
	_animPhase = fastFmod(_animPhase,1);

	// react to keys
	// mouse rotate object

	const float speedKeyb=2;
	const float speedMouse=0.01;

	float speedX = 0;
	float speedY = 0;

	Matrix3 rot=M3Identity;

	if (GInput.mouseR)
	{
		speedX = +GInput.mouseX * speedMouse*scale;
		speedY = -GInput.mouseY * speedMouse*scale;
	}

	speedX += (GInput.keys[DIK_NUMPAD4]-GInput.keys[DIK_NUMPAD6])*speedKeyb*deltaT;
	speedY += (GInput.keys[DIK_NUMPAD8]-GInput.keys[DIK_NUMPAD2])*speedKeyb*deltaT;


	if (_animSpeed>0 )
	{
		_animSpeed *=pow(2,deltaT*(GInput.keys[DIK_RBRACKET]-GInput.keys[DIK_LBRACKET]));
		saturate(_animSpeed,1e-3,10);
	}
	else
	{
		if (GInput.keys[DIK_RBRACKET]-GInput.keys[DIK_LBRACKET])
		{
			_animSpeed=0.5;
		}
	}

	// calculate heading and dive from current orienation
	float head = -atan2(Direction().X(),Direction().Z());
	float xzSize = Direction().SizeXZ();
	float dive = -atan2(Direction().Y(),xzSize);

	head += speedX;
	dive += speedY;

	if (GInput.keys[DIK_NUMPAD5])
	{
		dive = head = 0;
	}

	saturate(dive,-H_PI/2*0.9999,+H_PI/2*0.9999);

	rot= Matrix3(MRotationY,head);
	rot= rot * Matrix3(MRotationX,dive);

	SetOrientation(rot);
}

void CameraViewer::LimitVirtual
(
	CameraType camType, float &heading, float &dive, float &fov
) const
{
	heading=AngleDifference(heading,0);
	saturate(dive,-H_PI*0.3,+H_PI*0.3);
	switch (camType)
	{
		case CamInternal:
			saturate(fov,0.01,2);
		break;
		default:
			base::LimitVirtual(camType,heading,dive,fov);
		break;
	}
}

void CameraViewer::InitVirtual
(
	CameraType camType, float &heading, float &dive, float &fov
) const
{
	base::InitVirtual(camType,heading,dive,fov);
}

extern bool ObjViewer;

void World::SetViewerPhase( float time )
{
	Object *cobj=_cameraOn;
	ObjectViewer *oldViewer=dynamic_cast<ObjectViewer *>(cobj);
	CameraViewer *camViewer=dynamic_cast<CameraViewer *>(cobj);
	if (camViewer) oldViewer = camViewer->GetTarget();
	if (oldViewer) oldViewer->SetPhase(time);
}

float World::GetViewerPhase() const
{
	Object *cobj=_cameraOn;
	ObjectViewer *oldViewer=dynamic_cast<ObjectViewer *>(cobj);
	CameraViewer *camViewer=dynamic_cast<CameraViewer *>(cobj);
	if (camViewer) oldViewer = camViewer->GetTarget();
	if (oldViewer) return oldViewer->GetPhase();
	return 0;
}


void World::ReloadViewer( void *buf, int len, const char *classDesc )
{
	//LogF("Start reload");
	ObjViewer=true;

	VehicleTypeBank tempBank;
	// create temporary type
	// check property for config class name
	QIStream sFile((char *)buf,len);

	// reversed or no?
	// how should I no before I load shape?
	Ref<LODShapeWithShadow> shape=new LODShapeWithShadow(sFile,true);

	//type->_shapeReversed);
	const char *viewClass="";
	if( shape->NLevels()>1 )
	{
		viewClass=shape->PropertyValue(".viewclass");
	}
	//LogF("Reload levels %d",shape->NLevels());
	//LogF("Reload class %s",viewClass);

	// check camera object
	Object *obj=_cameraOn;
	VehicleWithAI *camAI=dyn_cast<VehicleWithAI>(obj);
	const char *camName = "";
	if( camAI ) camName = camAI->GetType()->GetName();
	if( !strcmpi(camName,viewClass) )
	{
		sFile.seekg(0,QIOS::beg);
		if( camAI )
		{
			// if there is already an object with the same class, we can only reload shape
			const VehicleType *type=camAI->GetType();
			const_cast<VehicleType *>(type)->ReloadShape(sFile);
			return;
		}
		else
		{
			Object *cobj=_cameraOn;
			ObjectViewer *oldViewer=dynamic_cast<ObjectViewer *>(cobj);
			CameraViewer *camViewer=dynamic_cast<CameraViewer *>(cobj);
			if (camViewer) oldViewer = camViewer->GetTarget();
			if (oldViewer)
			{
				// reload simple object
				Frame oldPos=*oldViewer;
				DeleteVehicle(oldViewer);	
				Ref<ObjectViewer> obj=new ObjectViewer(shape,"ObjView");
				obj->SetTransform(oldPos.Transform());
				AddVehicle(obj);
				Object *cobj=obj;
				GetGameState()->VarSet("bis_buldozer_zero", GameValueExt(cobj), true);
				// camera deleted - reasign it
				if (camViewer)
				{
					// no change in camera
					camViewer->SetTarget(obj);
				}
				else
				{
					_cameraOn = cobj;
					GetGameState()->VarSet("bis_buldozer_cursor", GameValueExt(cobj), true);
					_camType = _camTypeMain = CamInternal;
				}

				//_camType = _camTypeMain = CamGroup;
				return;
			}
		}
	}

	Vehicle *camVeh = dynamic_cast<Vehicle *>((Object *)_cameraOn);
	if( camVeh )
	{
		// note: camVeh need not be in Vehicle list
		DeleteVehicle(camVeh);
	}
	_vehicles.Clear();

	// if possible object is previewed on training island
	extern bool NoLandscape;
	if( !NoLandscape )
	{
		SwitchLandscape(GetWorldName("Intro"));
	}
	else
	{
		//LogF("Reset landscape");
		Reset();
	}

	float mid = GLandscape->GetLandRange()*GLandscape->GetLandGrid()*0.5;
	Vector3 cPos(mid,50,mid);

	if( !*viewClass )
	{
		// preview simple object
		Ref<ObjectViewer> obj=new ObjectViewer(shape,"ObjView");
		Ref<CameraViewer> cam=new CameraViewer(obj,shape,"ObjView");
		obj->SetPosition(cPos);
		cam->SetPosition(cPos+InitCamPos);
		AddVehicle(obj);
		GetGameState()->VarSet("bis_buldozer_zero", GameValueExt(obj.GetRef()), true);
		AddVehicle(cam);
		GetGameState()->VarSet("bis_buldozer_cursor", GameValueExt(cam.GetRef()), true);
		Object *cobj=cam;
		_cameraOn = cobj;
		_camType = _camTypeMain = CamInternal;

		//LogF("End reload - simple ");
		return;
	}

	const ParamEntry &vehicles=Pars>>"CfgVehicles";
	if( !vehicles.FindEntry(viewClass) )
	{
		return;
	}

	if( (vehicles>>viewClass>>"reversed").GetInt()==0 )
	{
		// reload shape nonreversed
		shape=new LODShapeWithShadow(sFile,false);
		viewClass=shape->PropertyValue(".viewclass");
	}

	VehicleNonAIType *vType=tempBank.New(viewClass);

	VehicleType *type=dynamic_cast<VehicleType *>(vType);
	if( !type ) return;

	type->AttachShape(shape);


	// TODO: Detach shape after creating the vehicle

	// if possible vehicle is created from object
	ArcadeUnitInfo info;


	// TODO: use default side
	//info.side = type->_typicalSide;
	info.side = TWest;
	info.vehicle = type->GetName();
	info.type = type;
	info.position = cPos;

	TargetSide side = info.side;

	_mode = GModeArcade;
	_endMission = EMContinue;
	EnableRadio();

	Glob.header.playerSide = side;

	if (_ui) _ui->ResetHUD();

	if (!GDummyVehicle) GDummyVehicle = NewVehicle("PaperCar");

	AIGlobalInit();


	switch( side )
	{
		case TWest:
			_westCenter = new AICenter(TWest,AICMArcade);
			_westCenter->InitPreview(info);
		break;
		case TEast:
			_eastCenter = new AICenter(TEast,AICMArcade);
			_eastCenter->InitPreview(info);
		break;
		case TCivilian:
			_civilianCenter = new AICenter(TCivilian,AICMArcade);
			_civilianCenter->InitPreview(info);
		break;
		case TGuerrila:
			_guerrilaCenter = new AICenter(TGuerrila,AICMArcade);
			_guerrilaCenter->InitPreview(info);
		break;

	}

	//InitNoCenters(t);

	if (_eastCenter) _eastCenter->InitSensors();
	if (_westCenter) _westCenter->InitSensors();
	if (_guerrilaCenter) _guerrilaCenter->InitSensors();
	if (_civilianCenter) _civilianCenter->InitSensors();
	// if (_logicCenter) _logicCenter->InitSensors();
	GetSensorList()->UpdateAll();
	if (_eastCenter) _eastCenter->InitSensors(true);
	if (_westCenter) _westCenter->InitSensors(true);
	if (_guerrilaCenter) _guerrilaCenter->InitSensors(true);
	if (_civilianCenter) _civilianCenter->InitSensors(true);
	// if (_logicCenter) _logicCenter->InitSensors(false);

	// new data ready - adapt texture cache

	//_engine->TextBank()->FlushTextures();

	_sensorList->UpdateAll();
	VehicleWithAI *ai=dyn_cast<VehicleWithAI, Object>(_cameraOn);
	if (_ui && ai)
	{
		_ui->ResetVehicle(ai);
	}

	//LogF("End reload");
}

void World::ReloadViewer( const char *filename, const char *classDesc )
{
	QIFStream file;
	file.open(filename);
	ReloadViewer((char *)file.act(),file.rest(),classDesc);
}

