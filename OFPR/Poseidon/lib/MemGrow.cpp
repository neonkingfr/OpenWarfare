#include "wpch.hpp"

#if !defined USE_MALLOC && !defined MFC_NEW
#include "MemGrow.hpp"
#include "global.hpp"
#include <Es/Common/win.h>

// use _DISABLE_VMEM to force using fixed memory instead of virtual

void MemGrow::DoConstruct()
{
	_data=NULL;
	_reserved=0;
	_commited=0;
	_error = false;
	#if !_DISABLE_VMEM
		#ifdef _XBOX
			_pageSize=64*1024;
		#elif defined(_WIN32)
			SYSTEM_INFO info;
			GetSystemInfo(&info);
			// we do not need to know page size exactly,
			// but if we know it, we can better commit pages
			// we do not care if actual page size is power of two
			// we ony want to be sure that our _pageSize is power of two
			_pageSize=info.dwPageSize;
			// minimal grow - it is not good to resize swap file many times about 4 KB
			#define MIN_GROW (256*1024)
			if( _pageSize<MIN_GROW ) _pageSize=MIN_GROW;
			int pageSizeLog=0;
			while( (1<<pageSizeLog)<_pageSize ) pageSizeLog++;
			_pageSize=(1<<pageSizeLog);
		#else
			_pageSize=4*1024;
		#endif
	#else
		_pageSize=4*1024;
	#endif
}

void MemGrow::DoConstruct( int size )
{
	DoConstruct();
	Reserve(size);
}

void MemGrow::DoDestruct()
{
	if( _data )
	{
		#if defined(USE_MALLOC) && USE_MALLOC
			LogF("Total memory usage: %d of %d.",_commited,_reserved);
			LogF("  Memory released.");
			::free(_data);
		#elif _DISABLE_VMEM
			LogF("Total memory usage: %d of %d.",_commited,_reserved);
			LogF("  Memory released.");
			::GlobalFree(_data);
		#else
			LogF("Total memory usage: %d commited (%d reserved).",_commited,_reserved);
			::VirtualFree(_data,_commited,MEM_DECOMMIT);
			LogF("  Virtual memory decommited.");
			::VirtualFree(_data,0,MEM_RELEASE);
			LogF("  Virtual memory released.");
		#endif
		_data=NULL;
		_commited=_reserved=0;
	}
}

void MemoryErrorReported();

void MemGrow::Reserve( int size )
{
	//saturateMin(size,40*1024*1024);
	DoDestruct();
	size+=_pageSize-1;
	size&=~(_pageSize-1);
	#if defined(USE_MALLOC) && USE_MALLOC
		_data=malloc(size);
		if( !_data ) ErrorMessage("Cannot allocate memory (%d MB).",size/(1024*1024));
		else _reserved=size;
	#elif _DISABLE_VMEM
		_data=::GlobalAlloc(GMEM_FIXED,size);
		if( !_data ) ErrorMessage("Cannot allocate memory (%d MB).",size/(1024*1024));
		else _reserved=size;
	#else
		_data=::VirtualAlloc(NULL,size,MEM_RESERVE,PAGE_READWRITE);
		if( !_data ) ErrorMessage("Cannot reserve virtual memory (%d MB).",size/(1024*1024));
		else _reserved=size;
	#endif
}

inline int ConvertToMB(int x)
{
	const int oneMB=1024*1024;
	return (x+oneMB-1)/oneMB;
}

bool MemGrow::Commit( int size )
{
	#if defined(USE_MALLOC) && USE_MALLOC
		if (size>_reserved)
		{
			const int oneMB=1024*1024;
			// unbind debugger - call stack may not be generated
			MemoryErrorReported();
			ErrorMessage
			(
				"Cannot commit %d MB of memory.\n"
				"(%d MB allocated.)\n",
				ConvertToMB(size),ConvertToMB(_reserved)
			);
		}
		else
		{
			_commited = size;
		}
	#elif _DISABLE_VMEM
		if (size>_reserved)
		{
			const int oneMB=1024*1024;
			// unbind debugger - call stack may not be generated
			MemoryErrorReported();
			ErrorMessage
			(
				"Cannot commit %d MB of memory.\n"
				"(%d MB allocated.)\n",
				ConvertToMB(size),ConvertToMB(_reserved)
			);
		}
		else
		{
			_commited = size;
		}
	#else
		size+=_pageSize-1;
		size&=~(_pageSize-1);
		if( _commited<size )
		{
			if (size>_reserved)
			{
				_error = true;
				MemoryErrorReported();
				ErrorMessage
				(
					"Cannot allocate more than was reserved (%d MB)",_reserved/(1024*1024)
				);
				return false;
			}
			//if( size>90*1024*1024 || ::VirtualAlloc(_data,size,MEM_COMMIT,PAGE_READWRITE)!=_data )
            //if ( size > (80<<20) )
            //    ErrF("Allocating %d MB of virtual memory",ConvertToMB(size));
			if(::VirtualAlloc(_data,size,MEM_COMMIT,PAGE_READWRITE)!=_data )
			{
				// unbind debugger - call stack may not be generated
				_error = true;
				MemoryErrorReported();
				MEMORYSTATUS mstat;
				mstat.dwLength = sizeof(mstat);
				GlobalMemoryStatus(&mstat);
				//if (mstat.dwAvailPageFile<oneMB)
				//{
				ErrorMessage
				(
					"Cannot increase memory pool to %d MB.\n"
					"Current memory pool size is %d MB.\n"
					"You probably have your hard disk full.\n"
					"If you control your swap fille size manually,\n"
					"consider setting it bigger.\n\n"
					"Current swap-file settings:\n"
					"   Total:     %6d MB\n"
					"   Available: %6d MB\n",
					"   Used:      %6d MB\n",
					ConvertToMB(size),
					ConvertToMB(_commited),
					ConvertToMB(mstat.dwTotalPageFile),
					ConvertToMB(mstat.dwAvailPageFile),
					ConvertToMB(mstat.dwTotalPageFile-mstat.dwAvailPageFile)
				);
				return false;
			}
			else _commited=size;
			Log("Commited heap size %d (Reserved %d)",_commited,_reserved);
		}
	#endif
	return true;
}

#endif
