#ifdef _MSC_VER
#pragma once
#endif

#ifndef _CHAT_HPP
#define _CHAT_HPP

#include <Es/Strings/rString.hpp>
#include "global.hpp"
#include "engine.hpp"
#include "speaker.hpp"

/*!
\file
Interface file for chat processing - chat edit, chat (radio message) list
*/

//! channel for chat
DEFINE_ENUM_BEG(ChatChannel)
	CCNone = -1,
	CCGlobal,
	CCSide,
	CCGroup,
	CCVehicle,
	CCDirect,
	CCN
DEFINE_ENUM_END(ChatChannel)

ChatChannel ActualChatChannel();

//! single row of chat history
struct ChatItem
{
	ChatChannel channel;
	RString from;
	Clock time;
	RString text;
	UITime uiTime;
	bool playerMsg;
	bool forceDisplay;
};
TypeIsMovableZeroed(ChatItem);

class NetworkObject;
class RadioSentence;

//! Chat history
/*!
	This class encapsulates both data and drawing of chat history.
*/
class ChatList : public AutoArray<ChatItem>
{
protected:
	// chat parameters
	//@{
	//! position (top) of chat list
	float _x;			// top
	//! position (left) of chat list
	float _y;			// left
	//! width of chat list
	float _w;			// width
	//! height of single row of chat list
	float _h;			// row height
	//! maximal number of displayed rows
	int _rows;

	//! colors of texts for each channel
	PackedColor _colors[CCN];
	//! background color
	PackedColor _bgColor;
	//! used font
	Ref<Font> _font;
	//! font size
	float _size;
	//! if is drawing enabld
	bool _enable;

	//! offset for browsing
	int _offset;

public:
	//! costructor
	ChatList();

	//! returns color of texts for given channel
	PackedColor GetColor(int channel) {return _colors[channel];}

	//! sets placement of chat list
	void SetRect(float x, float y, float w, float h);
	//! sets maximal number of displayed rows of chat list
	void SetRows(int rows) {_rows = rows;}
	
	//! adds single message to chat list
	/*!
		\param channel on which channel communication runs
		\param from sender name
		\param text text of message
		\param playerMsg if message has enforcement for player
		\param forceDisplay 
		\patch 1.01 Date 7/10/2001 by Jirka
		- Fixed: display chat messages even when radio protocol is disabled
		\patch_internal 1.01 Date 7/10/2001 by Jirka
		- parameter forceDisplay added
	*/
	void Add(ChatChannel channel, RString from, RString text, bool playerMsg, bool forceDisplay);
	//! adds single message to chat list
	/*!
		\param channel on which channel communication runs
		\param sender sender unit
		\param text text of message
		\param playerMsg if message has enforcement for player
		\param forceDisplay 
		\patch_internal 1.01 Date 7/10/2001 by Jirka
		- display chat messages even when radio protocol is disabled
		- parameter forceDisplay added
	*/
	void Add(ChatChannel channel, AIUnit *sender, RString text, bool playerMsg, bool forceDisplay);
	
	//! enables / disables displaying of chat list
	void Enable(bool enable) {_enable = enable;}
	//! returns if displaying of chat list is enabled
	bool Enabled() const {return _enable;}

	//! draws chat list
	void OnDraw();

	//! browse line up
	void BrowseUp();
	//! browse line down
	void BrowseDown();
	//! cancel browse mode
	void BrowseReset();
};

void SendRadioChat(ChatChannel channel, AIUnit *sender, NetworkObject *object, RString text, RadioSentence &sentence);
void SendRadioChatWave(ChatChannel channel, NetworkObject *object, RString wave, AIUnit *sender, RString senderName);
void SendRadioChatWave(ChatChannel channel, RString wave, AIUnit *sender, RString senderName);
void SendMarker(ChatChannel channel, AIUnit *sender, ArcadeMarkerInfo &marker);

extern ChatList GChatList;

#endif
