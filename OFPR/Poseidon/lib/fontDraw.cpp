
// font implementation common to all engines
// (C) 1997, Suma
#include "wpch.hpp"

#include "engine.hpp"
#include "scene.hpp"
#include "font.hpp"
#include "textbank.hpp"
#include <El/QStream/QBStream.hpp>
#include <Es/Strings/bstring.hpp>
#include "global.hpp"
#include "scene.hpp"
#include "camera.hpp"
#include <stdarg.h>

#include "mbcs.hpp"

#define KOREAN_STANDARD_CHAR	0x0060

void Engine::DrawText
(
	const Point2DAbs &pos, float size,
	Font *font, PackedColor color, const char *text
)
{
	DrawText(pos,size,Rect2DClipAbs,font,color,text);
}

void Engine::DrawText
(
	const Point2DFloat &pos, float size, Font *font, PackedColor color, const char *text
)
{
	Rect2DFloat clip(-1000,-1000,2000,2000);
	DrawText(pos,size,clip,font,color,text);
}

void Engine::DrawText
(
	const Point2DFloat &pos, float sizeEx,
	const Rect2DFloat &clip,
	Font *font, PackedColor color, const char *text
)
{
	if (!*text) return;

	Point2DAbs posA;
	Convert(posA,pos);
	posA.x = toInt(posA.x) + 0.5f;
	posA.y = toInt(posA.y) + 0.5f;
	Rect2DAbs clipA;
	Convert(clipA,clip);
	DrawText(posA,sizeEx,clipA,font,color,text);
}

void Engine::DrawText
(
	const Point2DAbs &pos, float sizeEx,
	const Rect2DAbs &clip,
	Font *font, PackedColor color, const char *text
)
{
	Point2DAbs posA = pos;
	if (!*text) return;
	Temp<char> buffer;
	if (font->GetLangID() == Korean)
	{
		buffer.Realloc(strlen(text) + 1);
		MultiByteStringToPseudoCodeString(text, buffer);
		text = buffer;
	}

#if _ENABLE_PERFLOG 
	extern bool LogStatesOnce;
	if (LogStatesOnce) LogF("Draw text %s",text);
#endif
	float h=Height2D();
	AspectSettings as;
	GetAspectSettings(as);
	float sizeH = h * sizeEx * (1.0 / 600) / font->Height();
	float sizeW = sizeH * Width()* as.topFOV/(Height()*as.leftFOV);

	// use font to draw given text

	// convert clipping coordinates to pixel coordinates
	Draw2DPars pars;
	//pars.spec=NoZBuf|IsAlpha|ClampU|ClampV|IsAlphaFog;
	pars.spec=NoZBuf|IsAlpha|ClampU|ClampV|IsAlphaFog;
/*
	if (font->GetLangID() == Korean)
	{
		pars.spec |= PointSampling;
	}
*/
	pars.SetColor(color);
	pars.SetU(0,1); // u,v range
	pars.SetV(0,1);

	for( int c; (c=*text++)!=0; )
	{
		c&=0xff;
		if( c<=Font::StartChar )
		{
			//x+=font->_width[0]*size*0.8; // add space width
			posA.x+=toInt(font->_infos['o'-Font::StartChar].width*sizeW); // add space width
			continue;
		}

		if (font->GetLangID() == Korean && c >= 0x80)
		{
			text = DrawComposedChar
			(
				posA,
				pars, clip,
				sizeH, sizeW, font,
				text - 1
			);
			continue;
		}

		c-=Font::StartChar;

		Ref<Texture> texture=_fonts.Load(font,font->_infos[c].nameTex);
		Assert( texture );


		float tx=font->_infos[c].xTex;
		float ty=font->_infos[c].yTex;
		float th=font->_infos[c].hTex;
		float tw=font->_infos[c].wTex;
		float sh=font->_infos[c].hTex*sizeH;
		float sw=font->_infos[c].wTex*sizeW;
		// texture may be resized - original texture size is _widthPow2
		// only part of texture is actually occupied by character - _width

		float ah=font->_infos[c].height;
		float aw=font->_infos[c].width;

		float xPart = aw/tw;
		float yPart = ah/th;

		pars.mip=TextBank()->UseMipmap(texture,0,0);
		if( !pars.mip.IsOK() ) continue;
		// call wrapped function
		// draw tx,ty,th,tw region of texture
		int texW=1, texH=1;
		if (texture)
		{
			texW=texture->AWidth();
			texH=texture->AHeight();
		}

		// for single char texture: tw==texW, th==texH, tx==0, ty==0
		float invTW=1.0/texW;
		float invTH=1.0/texH;
		pars.SetU(tx*invTW,(tx+tw*xPart)*invTW); // u,v range
		pars.SetV(ty*invTH,(ty+th*yPart)*invTH);
		
		Draw2D(pars,Rect2DAbs(posA.x,posA.y,sw*xPart,sh*yPart),clip);

		posA.x+=toInt(font->_infos[c].width*sizeW);
	}
}

const char *Engine::DrawComposedChar
(
	Point2DAbs &pos,
	Draw2DPars &pars, const Rect2DAbs &clip,
	float sizeH, float sizeW, Font *font,
	const char *text
)
{
	float h = font->_infos[KOREAN_STANDARD_CHAR].height * sizeH;
	float w = font->_infos[KOREAN_STANDARD_CHAR].width * sizeW;

	float oldY = pos.y;
	// change pos.y temporary
	if (h > 0.025 * Height2D())
	{
		pos.y += 0.125 * h;
		w *= 0.75;
		h *= 0.75;
	}
	h = toInt(h);
	w = toInt(w);

	short picturecode[3];
	if (!PseudoCodeToPictureCode(text, picturecode))
	{
		pos.x += w;
		// consistency with GetTextWidth
		return *(text + 1) ? text + 2 : text + 1;
	}

	if (picturecode[0] != 0x0060)
		DrawComposedChar
		(
			pos, w, h,
			pars, clip, sizeH, sizeW, font,
			picturecode[0]
		);
	if (picturecode[1] != 0x0060)
		DrawComposedChar
		(
			pos, w, h,
			pars, clip, sizeH, sizeW, font,
			picturecode[1]
		);
	if (picturecode[2] != 0x0060)
		DrawComposedChar
		(
			pos, w, h,
			pars, clip, sizeH, sizeW, font,
			picturecode[2]
		);

	pos.x += w;
	pos.y = oldY;
	return text + 2;
}

void Engine::DrawComposedChar
(
	const Point2DAbs &pos, float w, float h,
	Draw2DPars &pars, const Rect2DAbs &clip, float sizeH, float sizeW, Font *font,
	short c
)
{
	Ref<Texture> texture = _fonts.Load(font, font->_infos[c].nameTex);
	Assert(texture);

	float tx = font->_infos[c].xTex;
	float ty = font->_infos[c].yTex;
	float th = font->_infos[c].hTex;
	float tw = font->_infos[c].wTex;

	float ah = font->_infos[c].height;
	float aw = font->_infos[c].width;

	float xPart = aw / tw;
	float yPart = ah / th;

	pars.mip = TextBank()->UseMipmap(texture, 0, 0);
	if (pars.mip.IsOK())
	{
		// call wrapped function
		// draw tx,ty,th,tw region of texture
		int texW = 1, texH = 1;
		if (texture)
		{
			texW = texture->AWidth();
			texH = texture->AHeight();
		}

		// for single char texture: tw==texW, th==texH, tx==0, ty==0
		float invTW = 1.0 / texW;
		float invTH = 1.0 / texH;

		float u0 = tx * invTW;
		float u1 = (tx + tw * xPart) * invTW;
		float v0 = ty * invTH;
		float v1 = (ty + th * yPart) * invTH;

		pars.SetU(u0, u1);
		pars.SetV(v0, v1);
		
		Draw2D(pars, Rect2DAbs(pos, w, h), clip);
	}
}

/*
void Engine::DrawSection
(
	const FaceArray &face, Offset beg, Offset end
)
{
	for( Offset i=beg; i<end; face.Next(i) )
	{
		const Poly &f=face[i];
		DrawPolygon(f.GetVertexList(),f.N());
	}
}
*/

void Engine::Draw3D
(
	Vector3Par pos, Vector3Par down, Vector3Par dir, ClipFlags clip,
	PackedColor color, int spec, Texture *tex,
	float x1c, float y1c, float x2c, float y2c
)
{
	static Ref<ObjectColored> obj;
	if (!obj)
	{

		Ref<LODShapeWithShadow> lShape=new LODShapeWithShadow();
		Shape *shape=new Shape;
		lShape->AddShape(shape,0);

		// initalize lod level
		shape->ReallocTable(4);
		const ClipFlags clip=ClipAll;
		shape->SetPos(0)=VZero;
		shape->SetPos(1)=VZero;
		shape->SetPos(2)=VZero;
		shape->SetPos(3)=VZero;
		shape->SetClip(0,clip);
		shape->SetClip(1,clip);
		shape->SetClip(2,clip);
		shape->SetClip(3,clip);
		shape->SetNorm(0)=VUp;
		shape->SetNorm(1)=VUp;
		shape->SetNorm(2)=VUp;
		shape->SetNorm(3)=VUp;
		// precalculate hints for possible optimizations
		shape->CalculateHints();
		lShape->CalculateHints();
		// change face parameters
		Poly face;
		face.Init();
		face.SetN(4);
		face.Set(0,0);
		face.Set(1,1);
		face.Set(2,3);
		face.Set(3,2);
		face.SetSpecial(ClampU|ClampV);
		shape->AddFace(face);
		//shape->OrSpecial(IsAlpha|IsAlphaFog);
		shape->OrSpecial(IsAlpha|IsAlphaFog|BestMipmap);
		
		//shape->RegisterTexture(tex,NULL);
		Assert( shape->NPos()==4 );
		Assert( shape->NNorm()==4 );
		Assert( shape->NFaces()==1 );

		lShape->CalculateMinMax(true);
		lShape->OrSpecial(IsColored|IsOnSurface);

		obj=new ObjectColored(lShape,-1);
		obj->SetOrientation(M3Identity);
	}
	LODShape *lShape = obj->GetShape();

	obj->SetConstantColor(color);
	obj->SetSpecial(spec);
	// use global object
	Shape *shape = lShape->Level(0);
	Poly &face = shape->Face(shape->BeginFaces());

	shape->SetPos(0) = x1c * dir + y1c * down;
	shape->SetPos(1) = x2c * dir + y1c * down;
	shape->SetPos(2) = x1c * dir + y2c * down;
	shape->SetPos(3) = x2c * dir + y2c * down;
		
	Vector3 normal = down.CrossProduct(dir).Normalized();		
	shape->SetNorm(0)=normal;
	shape->SetNorm(1)=normal;
	shape->SetNorm(2)=normal;
	shape->SetNorm(3)=normal;

	lShape->SetAutoCenter(false);
	lShape->CalculateMinMax(true);

	if (tex)
	{
		float u0 = tex->UToPhysical(x1c);
		float u1 = tex->UToPhysical(x2c);
		float v0 = tex->VToPhysical(y1c);
		float v1 = tex->VToPhysical(y2c);
		shape->SetUV(0,u0,v0);
		shape->SetUV(1,u1,v0);
		shape->SetUV(2,u0,v1);
		shape->SetUV(3,u1,v1);
	}
	else
	{
		shape->SetUV(0,0,0);
		shape->SetUV(1,1,0);
		shape->SetUV(2,0,1);
		shape->SetUV(3,1,1);
	}

	face.SetTexture(tex);

	shape->FindSections(); // TODO: we know it is singular section

	obj->SetPosition(pos);
	obj->Draw(0, clip, *obj);
}

#include "objLine.hpp"


// TODO: move to some header

void PrepareTexture( Texture *texture, float z2, int special, float areaOTex );

void Engine::DrawLine3D
(
	Vector3Par start, Vector3Par end,
	PackedColor color, int spec
)
{
	// TODO: implement 3D drawing
	static Ref<LODShapeWithShadow> shape;
	static Ref<ObjectColored> obj;

	if (!shape)
	{
		shape = ObjectLine::CreateShape();
	}

	if (!obj) obj = new ObjectLineDiag(shape);
	obj->SetSpecial(spec);
	obj->SetPosition(start);

	// note: line A width meaning changed in 1.50 to support thicker 3D lines
	// apply color thickness factor to maintain DrawLine3D compatibility 
	int a8 = toInt(color.A8()*1.0/10);
	// if line width was non-zero, avoid being it zero now
	if (color.A8()>0 && a8<=0 ) a8 = 1;
	else
	{
		saturate(a8,0,255);
	}
	color.SetA8(a8);

	obj->SetConstantColor(color);
	ObjectLine::SetPos(shape, VZero, end - start);
	obj->Draw(0, ClipAll, *obj);
}

/*!
\patch_internal 1.59 Date 5/23/2002 by Ondra
- Fixed: Fonts in some dialogs or briefing were sometimes blurred.
\patch 1.78 Date 7/17/2002 by Ondra
- Fixed: Texts in various dialogs were sometimes invisible or dimmed.
\patch 1.90 Date 11/1/2002 by Ondra
- Fixed: Improved text drawing in atypic resolutions like 1600x900.
*/

void Engine::DrawText3D
(
	Vector3Par pos, Vector3Par up, Vector3Par dir, ClipFlags clip,
	Font *font, PackedColor color, int spec, const char *text,
	float x1c, float y1c, float x2c, float y2c
)
{
	if (!*text) return;

	Temp<char> buffer;
	if (font->GetLangID() == Korean)
	{
		buffer.Realloc(strlen(text) + 1);
		MultiByteStringToPseudoCodeString(text, buffer);
		text = buffer;
	}

#if _ENABLE_PERFLOG 
	extern bool LogStatesOnce;
	if (LogStatesOnce) LogF("Draw text %s",text);
#endif

	// TODO: build multiple characters buffer and change vertex positions
	// simple implementation (single polygon mesh for each character)
	static Ref<ObjectColored> objText3D;
	if (!objText3D)
	{

		Ref<LODShapeWithShadow> lShape=new LODShapeWithShadow();
		Shape *shape=new Shape;
		lShape->AddShape(shape,0);

		// initalize lod level
		shape->ReallocTable(4);
		const ClipFlags clip=ClipAll;
		shape->SetClip(0,clip);
		shape->SetClip(1,clip);
		shape->SetClip(2,clip);
		shape->SetClip(3,clip);
		shape->SetNorm(0)=VUp;
		shape->SetNorm(1)=VUp;
		shape->SetNorm(2)=VUp;
		shape->SetNorm(3)=VUp;
		// precalculate hints for possible optimizations
		shape->CalculateHints();
		lShape->CalculateHints();
		// change face parameters
		Poly face;
		face.Init();
		face.SetN(4);
		face.Set(0,0);
		face.Set(1,1);
		face.Set(2,3);
		face.Set(3,2);
		face.SetSpecial(ClampU|ClampV);
		shape->AddFace(face);
		shape->OrSpecial(IsAlpha|IsAlphaFog|NoZWrite);
		Assert( shape->NPos()==4 );
		Assert( shape->NNorm()==4 );
		Assert( shape->NFaces()==1 );

		lShape->OrSpecial(IsColored|IsOnSurface);

		objText3D=new ObjectColored(lShape,-1);
		objText3D->SetOrientation(M3Identity);
	}

	objText3D->SetConstantColor(color);
	objText3D->SetSpecial(spec);
	// use global char object
	LODShape *lShape = objText3D->GetShape();
	Shape *shape = lShape->Level(0);
	for (int i=0; i<shape->NVertex(); i++)
	{
		//ClipFlags c = shape->Clip(i);
		shape->SetClip(i,clip|ClipAll);
	}
	shape->CalculateHints();

	// calculate z2 factor
	float z2=GScene->GetCamera()->Position().Distance2(pos);

	float fontMH = font->_maxHeight;
	float invMH = 1.0 / fontMH; // TODO: precalc
	float y1cf = y1c * fontMH;
	float y2cf = y2c * fontMH;

	Char3DContext ctx;
	ctx.dir = dir;
	ctx.up = up;
	ctx.font = font;
	ctx.obj = objText3D;
	ctx.z2 = z2;
	ctx.x1c = x1c;
	ctx.x2c = x2c;
	ctx.y1c = y1c;
	ctx.y2c = y2c;
	ctx.clip = clip;
	ctx.spec = spec;

	float xPos = 0;
	Vector3 lPos = pos;
	for( int c; (c=*text++)!=0; )
	{
		c &= 0xff;
		if( c<=Font::StartChar )
		{
			// white character
			const int cc = 'o'-Font::StartChar;
			float w = 0.8 * font->_infos[cc].width * invMH; // add space width
			xPos += w;
			lPos += w * dir;
			continue;
		}

		if (font->GetLangID() == Korean && c >= 0x80)
		{
			text = DrawComposedChar3D(xPos, lPos, ctx, text - 1);
			continue;
		}

		c-=Font::StartChar;

		float wp = font->_infos[c].width * invMH;
		if (xPos + wp >= x1c && xPos <= x2c)
		{
			float x1cf = 0;
			float x2cf = 1;
			if (x1c > xPos) x1cf = (x1c - xPos) * (1.0 / wp);
			if (x2c < xPos + wp) x2cf = (x2c - xPos) * (1.0 / wp);

			Poly &face = shape->Face(shape->BeginFaces());

			Ref<Texture> texture=_fonts.Load(font,font->_infos[c].nameTex);

			//float th=font->_hTex[c];
			//float tw=font->_wTex[c];

			// whole texture size
			int texW=1, texH=1;
			if (texture)
			{
				texW=texture->AWidth();
				texH=texture->AHeight();
			}

			float tx=font->_infos[c].xTex; // single character position
			float ty=font->_infos[c].yTex;
			//float sh=font->_hTex[c]; // whole texture dimension
			//float sw=font->_wTex[c];

			// only part of texture is actually occupied by character - _width
			float ah=font->_infos[c].height;
			float aw=font->_infos[c].width;

			// draw tx,ty,ah,aw region of texture
			float invTW=1.0/texW;
			float invTH=1.0/texH;


			float u0=0, v0=0, u1=0, v1=0;
			if (texture)
			{
				float bottom = floatMin(y2cf, ah);
				u0 = texture->UToPhysical((tx+x1cf*aw)*invTW);
				v0 = texture->VToPhysical((ty+y1cf)*invTH);
				u1 = texture->UToPhysical((tx+x2cf*aw)*invTW);
				v1 = texture->VToPhysical((ty+bottom)*invTH);

				// calculate texture area and area
				float area = dir.CrossProduct(up).Size();
				//float areaOTex = area*invMH*invMH *texW*texH*0.33;
				float uvArea = (u1-u0)*(v1-v0);
				float areaOTex = area/uvArea;
				//float uvAre2 = 1/(invMH*invMH *texW*texH*3);
				// note: areaOTex should be constant accross whole font
				
				PrepareTexture(texture,z2,spec,areaOTex);
			}

			face.SetTexture(texture);


			float rh = -floatMin(ah * invMH, y2c);
			float rw = aw * invMH;
			shape->SetPos(0) = dir * (x1cf * rw) - up * y1c;
			shape->SetPos(1) = dir * (x2cf * rw) - up * y1c;
			shape->SetPos(2) = dir * (x1cf * rw) + up * rh;
			shape->SetPos(3) = dir * (x2cf * rw) + up * rh;

			Vector3 normal = dir.CrossProduct(up).Normalized();		
			shape->SetNorm(0)=normal;
			shape->SetNorm(1)=normal;
			shape->SetNorm(2)=normal;
			shape->SetNorm(3)=normal;

			shape->SetUV(0,u0,v0);
			shape->SetUV(1,u1,v0);
			shape->SetUV(2,u0,v1);
			shape->SetUV(3,u1,v1);
			shape->FindSections(); // TODO: we know it is singular
			// for each letter:
			lShape->SetAutoCenter(false);
			lShape->CalculateMinMax(true);
			objText3D->SetPosition(lPos);
			objText3D->Draw(0,clip&ClipAll,*objText3D);
		}
		xPos += wp;
		lPos += wp * dir;
	}

}

const char *Engine::DrawComposedChar3D
(
	float &xPos, Vector3 &lPos,
	Char3DContext &ctx,
	const char *text
)
{
	float fontMH = ctx.font->_maxHeight;
	float invMH = 1.0 / fontMH; // TODO: precalc

	float h = ctx.font->_infos[KOREAN_STANDARD_CHAR].height * invMH;
	float w = ctx.font->_infos[KOREAN_STANDARD_CHAR].width * invMH;

	short picturecode[3];
	if (!PseudoCodeToPictureCode(text, picturecode))
	{
		xPos += w;
		lPos += w * ctx.dir;
		// consistency with GetTextWidth
		return *(text + 1) ? text + 2 : text + 1;
	}

	if (picturecode[0] != 0x0060)
		DrawComposedChar3D
		(
			xPos, lPos, w, h, ctx, picturecode[0]
		);
	if (picturecode[1] != 0x0060)
		DrawComposedChar3D
		(
			xPos, lPos, w, h, ctx, picturecode[1]
		);
	if (picturecode[2] != 0x0060)
		DrawComposedChar3D
		(
			xPos, lPos, w, h, ctx, picturecode[2]
		);

	xPos += w;
	lPos += w * ctx.dir;
	return text + 2;
}

void Engine::DrawComposedChar3D
(
	float xPos, Vector3Par lPos,
	float w, float h,
	Char3DContext &ctx,
	short c
)
{
	if (xPos + w >= ctx.x1c && xPos <= ctx.x2c)
	{
		LODShape *lShape = ctx.obj->GetShape();
		Shape *shape = lShape->Level(0);

		float fontMH = ctx.font->_maxHeight;
		// float invMH = 1.0 / fontMH; // TODO: precalc
		float y1cf = ctx.y1c * fontMH;
		float y2cf = ctx.y2c * fontMH;

		float x1cf = 0;
		float x2cf = 1;
		if (ctx.x1c > xPos) x1cf = (ctx.x1c - xPos) * (1.0 / w);
		if (ctx.x2c < xPos + w) x2cf = (ctx.x2c - xPos) * (1.0 / w);

		Poly &face = shape->Face(shape->BeginFaces());

		Ref<Texture> texture=_fonts.Load(ctx.font, ctx.font->_infos[c].nameTex);

		//float th=font->_hTex[c];
		//float tw=font->_wTex[c];

		// whole texture size
		int texW=1, texH=1;
		if (texture)
		{
			texW=texture->AWidth();
			texH=texture->AHeight();
		}

		float tx = ctx.font->_infos[c].xTex; // single character position
		float ty = ctx.font->_infos[c].yTex;
		//float sh=font->_hTex[c]; // whole texture dimension
		//float sw=font->_wTex[c];

		// only part of texture is actually occupied by character - _width
		float ah = ctx.font->_infos[c].height;
		float aw = ctx.font->_infos[c].width;

		// draw tx,ty,ah,aw region of texture
		float invTW=1.0/texW;
		float invTH=1.0/texH;


		float u0=0, v0=0, u1=0, v1=0;
		if (texture)
		{
			float bottom = floatMin(y2cf, ah);
			u0 = texture->UToPhysical((tx+x1cf*aw)*invTW);
			v0 = texture->VToPhysical((ty+y1cf)*invTH);
			u1 = texture->UToPhysical((tx+x2cf*aw)*invTW);
			v1 = texture->VToPhysical((ty+bottom)*invTH);

			// calculate texture area and area
			float area = ctx.dir.CrossProduct(ctx.up).Size();
			// float areaOTex = area*invMH*invMH *texW*texH;
			float uvArea = (u1-u0)*(v1-v0);
			float areaOTex = area/uvArea;
			// note: areaOTex should be constant accross whole font
			
			PrepareTexture(texture,ctx.z2,ctx.spec,areaOTex);
		}

		face.SetTexture(texture);

		float rh = -floatMin(h, ctx.y2c);
		float rw = w;

		shape->SetPos(0) = ctx.dir * (x1cf * rw) - ctx.up * ctx.y1c;
		shape->SetPos(1) = ctx.dir * (x2cf * rw) - ctx.up * ctx.y1c;
		shape->SetPos(2) = ctx.dir * (x1cf * rw) + ctx.up * rh;
		shape->SetPos(3) = ctx.dir * (x2cf * rw) + ctx.up * rh;

		Vector3 normal = ctx.dir.CrossProduct(ctx.up).Normalized();		
		shape->SetNorm(0)=normal;
		shape->SetNorm(1)=normal;
		shape->SetNorm(2)=normal;
		shape->SetNorm(3)=normal;

		shape->SetUV(0,u0,v0);
		shape->SetUV(1,u1,v0);
		shape->SetUV(2,u0,v1);
		shape->SetUV(3,u1,v1);

		shape->FindSections(); // TODO: we know it is singular
		// for each letter:
		lShape->SetAutoCenter(false);
		lShape->CalculateMinMax(true);

		// for each letter:
		ctx.obj->SetPosition(lPos);
		ctx.obj->Draw(0,ctx.clip&ClipAll,*ctx.obj);
	}
}

Vector3 Engine::GetText3DWidth
(
	Vector3Par dir, Font *font, const char *text
)
{
	float width = 0;
	for (int c; (c=*text++)!=0;)
	{
		c&=0xff;
		if (c <= Font::StartChar)
		{
			const int cc = 'o' - Font::StartChar;
			width += 0.8 * font->_infos[cc].width; // add space width
		}
		else if (font->GetLangID() == Korean && c >= 0x80)
		{
			const int cc = KOREAN_STANDARD_CHAR;
			width += font->_infos[cc].width;
			if (*text) text++;
		}
		else
		{
			const int cc = c - Font::StartChar;
			width += font->_infos[cc].width;
		}
	}
	float invMH = 1.0 / font->_maxHeight; // TODO: precalc
	return width * invMH * dir;
}

Vector3 CCALL Engine::GetText3DWidthF
(
	Vector3Par dir, Font *font, const char *text, ...
)
{
	BString<2048> buf;
	va_list arglist;
	va_start(arglist, text);
	vsprintf(buf, text, arglist);
	va_end(arglist);
	return GetText3DWidth(dir, font, buf);
}

void CCALL Engine::DrawTextF
(
	const Point2DAbs &pos, float size,
	Font *font, PackedColor color, const char *text, ...
)
{
	BString<2048> buf;
	va_list arglist;
	va_start( arglist, text );
	vsprintf( buf, text, arglist );
	va_end( arglist );
	DrawText(pos,size,font,color,buf);
}

void CCALL Engine::DrawTextF
(
	const Point2DFloat &pos, float size,
	Font *font, PackedColor color, const char *text, ...
)
{
	BString<2048> buf;
	va_list arglist;
	va_start( arglist, text );
	vsprintf( buf, text, arglist );
	va_end( arglist );
	DrawText(pos,size,font,color,buf);
}


void Engine::DrawText3DF
(
	Vector3Par pos, Vector3Par up, Vector3Par dir, ClipFlags clip,
	Font *font, PackedColor color, int spec, const char *text, ...
)
{
	BString<2048> buf;
	va_list arglist;
	va_start( arglist, text );
	vsprintf( buf, text, arglist );
	va_end( arglist );
	DrawText3D(pos,up,dir,clip,font,color,spec,buf);
}

void CCALL Engine::DrawTextF
(
	const Point2DFloat &pos, float size,
	const Rect2DFloat &clip,
	Font *font, PackedColor color, const char *text, ...
)
{
	BString<2048> buf;
	va_list arglist;
	va_start( arglist, text );
	vsprintf( buf, text, arglist );
	va_end( arglist );
	DrawText(pos,size,clip,font,color,buf);
}

float CCALL Engine::GetTextWidthF
(
	float size, Font *font, const char *text, ...
)
{
	BString<2048> buf;
	va_list arglist;
	va_start( arglist, text );
	vsprintf( buf, text, arglist );
	va_end( arglist );
	return GetTextWidth(size,font,buf);
}

void Engine::DrawTextVertical
(
	const Point2DFloat &pos, float size, Font *font, PackedColor color, const char *text
)
{
	Rect2DFloat clip(-1000,-1000,2000,2000);
	DrawTextVertical(pos,size,clip,font,color,text);
}

void Engine::DrawTextVertical
(
	const Point2DFloat &pos, float sizeEx,
	const Rect2DFloat &clip,
	Font *font, PackedColor color, const char *text
)
{
	Fail("Obsolete, sizeH and sizeW calculation missing");
	#if 0
	float h=Height2D();
	//float w=Width();
	//float x=xx*w;
	//float y=yy*h;
	float size = h * sizeEx * (1.0 / 600) / font->Height();
	// use font to draw given text

	// convert clipping coordinates to pixel coordinates
	Point2DAbs posA;
	Convert(posA,pos);
	Rect2DAbs clipA;
	Convert(clipA,clip);

	Draw2DPars pars;
	//pars.spec=NoZBuf|IsAlpha|ClampU|ClampV|IsAlphaFog|PointFilter;
	pars.spec=NoZBuf|IsAlpha|ClampU|ClampV|IsAlphaFog;
	pars.SetColor(color);
	pars.SetU(0,1); // u,v range
	pars.SetV(0,1);

	Matrix4Val mrot = Matrix4(MRotationY, -0.5 * H_PI);
	Matrix4Val mtrans1 = Matrix4(MTranslation, Vector3(-0.5, 0, -0.5));
	Matrix4Val mtrans2 = Matrix4(MTranslation, Vector3(0.5, 0, 0.5));
	Matrix4Val m = mtrans2 * mrot * mtrans1;
	Vector3 tl, tr, bl, br;
	tl = m * Vector3(0, 0, 0);
	bl = m * Vector3(0, 0, 1);
	tr = m * Vector3(1, 0, 0);
	br = m * Vector3(1, 0, 1);

	for( int c; (c=*text++)!=0; )
	{
		c&=0xff;
		if( c<=Font::StartChar )
		{
			posA.y+=font->_infos['o'-Font::StartChar].width*size; // add space width
			continue;
		}

		c-=Font::StartChar;

		Ref<Texture> texture=_fonts.Load(font,font->_infos[c].nameTex);
		Assert( texture );


		float tx=font->_infos[c].xTex;
		float ty=font->_infos[c].yTex;
		float th=font->_infos[c].hTex;
		float tw=font->_infos[c].wTex;
		float sh=font->_infos[c].hTex*size;
		float sw=font->_infos[c].wTex*size;
		// texture may be resized - original texture size is _widthPow2
		// only part of texture is actually occupied by character - _width

		pars.mip=TextBank()->UseMipmap(texture,0,0);
		if( !pars.mip.IsOK() ) continue;
		// call wrapped function
		// draw tx,ty,th,tw region of texture
		int texW=1, texH=1;
		if (texture)
		{
			texW=texture->AWidth();
			texH=texture->AHeight();
		}

		// for single char texture: tw==texW, th==texH, tx==0, ty==0
		float invTW=1.0/texW;
		float invTH=1.0/texH;

		pars.uTL = (tx + tl[0] * tw) * invTW;
		pars.vTL = (ty + tl[2] * th) * invTH;
		pars.uTR = (tx + tr[0] * tw) * invTW;
		pars.vTR = (ty + tr[2] * th) * invTH;
		pars.uBL = (tx + bl[0] * tw) * invTW;
		pars.vBL = (ty + bl[2] * th) * invTH;
		pars.uBR = (tx + br[0] * tw) * invTW;
		pars.vBR = (ty + br[2] * th) * invTH;

		Draw2D(pars,Rect2DAbs(posA,sh,sw),clipA);

		posA.y+=font->_infos[c].width*size;
	}
	#endif
}

void CCALL Engine::DrawTextVerticalF
(
	const Point2DFloat &pos, float size,
	Font *font, PackedColor color, const char *text, ...
)
{
	char buf[2048];
	va_list arglist;
	va_start( arglist, text );
	vsnprintf( buf, sizeof(buf), text, arglist );
	va_end( arglist );
	DrawTextVertical(pos,size,font,color,buf);
}

void CCALL Engine::DrawTextVerticalF
(
	const Point2DFloat &pos, float size,
	const Rect2DFloat &clip,
	Font *font, PackedColor color, const char *text, ...
)
{
	char buf[2048];
	va_list arglist;
	va_start( arglist, text );
	vsnprintf( buf, sizeof(buf), text, arglist );
	va_end( arglist );
	DrawTextVertical(pos,size,clip,font,color,buf);
}

float Engine::GetTextWidth
(
	float sizeEx, Font *font, const char *text
)
{
	float h=Height2D();
	float w=Width2D();
	float x=0;
	// use font to draw given text
	AspectSettings as;
	GetAspectSettings(as);
	float sizeH = h * sizeEx * (1.0 / 600) / font->Height();
	float sizeW = sizeH * Width()* as.topFOV/(Height()*as.leftFOV);

	for( int c; (c=*text++)!=0; )
	{
		c&=0xff;
		if( c<=Font::StartChar )
		{
			x+=toInt(font->_infos['o'-Font::StartChar].width*sizeW); // add space width
			//x+=font->_width[0]*size*0.8; // add space width
		}
		else if (font->GetLangID() == Korean && c >= 0x80)
		{
			float h = font->_infos[KOREAN_STANDARD_CHAR].height * sizeH;
			float w = font->_infos[KOREAN_STANDARD_CHAR].width * sizeW;
			if (h > 0.025 * Height2D())
			{
				w *= 0.75;
			}
			x += toInt(w);
			if (*text) text++;
		}
		else
		{
			c-=Font::StartChar;
			x+=toInt(font->_infos[c].width*sizeW);
		}
	}
	return x/w;
}


