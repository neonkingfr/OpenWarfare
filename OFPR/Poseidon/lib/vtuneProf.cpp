// add VTune directory to compiler directiories
// default: C:\Program Files\Intel\VTune45
// default: E:\Program Files\Intel\VTune45

#include "wpch.hpp"

#if 0 //ndef _XBOX
#include <vtuneapi.h>
#include <startapi.h>

#include "vtuneProf.hpp"

typedef BOOL WINAPI SamplingF(void); 
typedef void WINAPI AnalysisF( long id ); 

static SamplingF *DoStartSampling;
static SamplingF *DoStopSampling;

static AnalysisF *DoStartStopAnalysis;

static HINSTANCE VTuneLib;
static HINSTANCE StartLib;

static int SamplingRunning;
static int AnalysisRunning;
static int AnalysisId;


void InitSampling()
{
	static bool init=false;
	if( init ) return;
	init=true;
	VTuneLib = LoadLibrary("vtuneapi.dll");
	if( VTuneLib )
	{
		DoStopSampling=(SamplingF *)GetProcAddress(VTuneLib,"VtPauseSampling");
		DoStartSampling=(SamplingF *)GetProcAddress(VTuneLib,"VtResumeSampling");
	}

}

void InitAnalysis()
{
	static bool init=false;
	if( init ) return;
	init=true;
	StartLib = LoadLibrary("startapi.dll");
	if( StartLib )
	{
		DoStartStopAnalysis=(AnalysisF *)GetProcAddress(StartLib,"DllStartAPI");
	}
}


void StartSampling()
{
	InitSampling();
	if( SamplingRunning++==0 )
	{
		if( DoStartSampling ) DoStartSampling();		
	}
}
void StopSampling()
{
	if( --SamplingRunning==0 )
	{
		if( DoStopSampling ) DoStopSampling();
	}
}

// dynamic analysis

void StartAnalysis()
{
	InitAnalysis();
	if( AnalysisRunning++==0 )
	{
		AnalysisId++;
		if( DoStartStopAnalysis ) DoStartStopAnalysis(AnalysisId);
	}
}
void StopAnalysis()
{
	if( --AnalysisRunning==0 )
	{
		if( DoStartStopAnalysis ) DoStartStopAnalysis(AnalysisId);
	}
}

#else

void InitSampling()
{
}

void InitAnalysis() {}


void StartSampling() {}
void StopSampling() {}

// dynamic analysis

void StartAnalysis() {}
void StopAnalysis() {}

#endif
