#include "wpch.hpp"
#include "netTransport.hpp"
#include "strFormat.hpp"

#ifndef DWORD_PTR
  #define DWORD_PTR DWORD
#endif

#if _ENABLE_MP
	#define VOICE_OVER_NET	1
	#include <dplay8.h>
#else
	#define VOICE_OVER_NET	0
	typedef int DPNID;
	typedef void *DPNHANDLE;
	#include "Es/Common/win.h"
#endif

#if VOICE_OVER_NET
#include <dvoice.h>
#endif

#ifndef _XBOX
#include <dxerr8.h>
#endif

#ifndef _XBOX
#pragma comment(lib,"dxerr8")
#endif

#define SEND_TIMEOUT	500

#if _ENABLE_MP
#include <initguid.h>
#if _VBS1
//! VBS1 Application GUID
/*!
- 9C7A82C7-E4FF-4159-9E28-2F27C055E75F
*/
DEFINE_GUID
(
	APP_GUID, 
	0x9c7a82c7, 0xe4ff, 0x4159,
	0x9e, 0x28, 0x2f, 0x27, 0xc0, 0x55, 0xe7, 0x5f
);
#else
//! Status Quo Application GUID
/*!
- F7249860-D0BE-11D2-95EA-00A0C9A57F0B
*/
DEFINE_GUID
(
	APP_GUID, 
	0xF7249860, 0xD0BE, 0x11D2,
	0x95, 0xEA, 0x00, 0xa0, 0xc9, 0xa5, 0x7f, 0x0b
);
#endif
#endif

/*!
\file
DirectPlay implementation of network transport classes
*/

/*****************************************************************************/
/*                                                                           */
/*                                Interface                                  */
/*                                                                           */
/*****************************************************************************/

//! Release COM object
#define SAFE_RELEASE(p)							{ if(p) {(p)->Release(); (p) = NULL;} }

#define MAX_SESSIONS				16
#define LEN_SESSION_NAME		256

struct DPSessionPacket : public MPVersionInfo
{
  bool equalModRequired;
};

#if _ENABLE_MP
TypeIsSimple(IDirectPlay8Address *)
#endif

//! Description of single session enumerated through _dp->EnumHosts
struct SessionDescription
{
	#if _ENABLE_MP
	//! DirectPlay session description
		DPN_APPLICATION_DESC session;
	//! URL of host
		IDirectPlay8Address *address;
	#endif
	//! name of sesion
	char name[LEN_SESSION_NAME];
	//! last update time 
	DWORD lastTime;
	//! current version of application on host
	int actualVersion;
	//! required version of application on host
	int requiredVersion;
	//! name of running mission on host
	char mission[40];
	//! state of game on host
	int gameState;
	//! current number of players
	int numPlayers;
	//! maximal number of players
	int maxPlayers;
  //! list of mods on host
  char mod[MOD_LENGTH];
  //! equal mods are required by host
  bool equalModRequired;
};

//! Container (thread safe) for list of session
class SessionDescriptions
{
protected:
	//! current number of contained sessions
	int _size;
	//! sessions
	SessionDescription _data[MAX_SESSIONS];

public:
	//! Constructor
	SessionDescriptions() {_size = 0;}
	//! Destructor
	~SessionDescriptions() {Clear();}
	//! Return current number of contained sessions
	int Size() {return _size;}
	//! Add new session
	/*!
	\return index of added session or -1 if session doesn't fit into container
	*/
	int Add();
	//! Delete session
	void Delete(int i);
	//! Delete all sessions
	void Clear();
	//! Access to single session
	SessionDescription &operator [] ( int i ) {return _data[i];}
	//! Access to single session
	const SessionDescription &operator [] ( int i ) const {return _data[i];}
};

//! structure sent inside DPN_MSGID_CREATE_PLAYER system message to tell server if connected client is bot client
struct DPPlayerInfoOld
{
	//! if connected client is bot client
	bool botClient;
};

struct DPPlayerInfo : public DPPlayerInfoOld
{
  //! mod list on client
  char mod[MOD_LENGTH];
};

//! Thread safe allocator
class MemAllocWin
{
	public:
	#if ALLOC_DEBUGGER
	static void *Alloc( int &size, const char *file, int line, const char *postfix )
	{
		return GlobalAlloc(GMEM_FIXED,size);
	}
	#else
	static void *Alloc( int &size )
	{
		// size is not changed
		return GlobalAlloc(GMEM_FIXED,size);
	}
	#endif
	static void Free( void *mem, int size )
	{
		GlobalFree((HGLOBAL)mem);
	}
	static void Free( void *mem )
	{
		GlobalFree((HGLOBAL)mem);
	}
	static void Unlink( void *mem ) {}
};

//! Info about user message
/*!
Used for transfer message from receiving thread to main application thread.
*/
struct UserMessageInfo
{
	//! ID of sender
	int from;
	//! size of message
	int messageSize;
	//! message itself
	char *message;
	//! handle of memory allocated by DirectPlay (used for ReturnBuffer call)
	DWORD messageHandle;
};
TypeIsSimple(UserMessageInfo)

//! DirectPlay implementation of NetTranspSessionEnum class
class DPlaySessionEnum : public NetTranspSessionEnum
{
protected:
	#if _ENABLE_MP
	 //! DirectPlay client interface used for EnumHosts
		IDirectPlay8Client *_dp;
	 //! EnumHosts handles
		AutoArray<DPNHANDLE> _enumHosts;
	#endif

	//! list of enumerated sessions
	SessionDescriptions _sessions;
	//! lock for _sessions
	CRITICAL_SECTION _sessionsLock;

public:
	//! Constructor
	DPlaySessionEnum();
	//! Destructor
	~DPlaySessionEnum();

	bool Init( int magic =0 );
	void Done();

	RString IPToGUID(RString ip, int port);

	#if _ENABLE_MP
	//! Callback function called from DirectPlay whenever session is enumerated
	void OnEnumSession(const DPN_APPLICATION_DESC *session, IDirectPlay8Address *address);
	#endif

	bool RunningEnumHosts();
	bool StartEnumHosts(RString ip, int port, AutoArray<RemoteHostAddress> *hosts);
	void StopEnumHosts();

	int NSessions();
	void GetSessions(AutoArray<SessionInfo> &sessions);
};

//! DirectPlay implementation of NetTranspClient class
class DPlayClient : public NetTranspClient
{
protected:

#if _ENABLE_MP
	//! DirectPlay client interface
	IDirectPlay8Client *_dp;
#endif

#if VOICE_OVER_NET
	//! DirectPlay voice client interface
	IDirectPlayVoiceClient *_dpvoice;
#endif

	//! when last message was received
	DWORD _lastMsgReceived;
	//! when "no message arrived" was reported to user
	DWORD _lastMsgReceivedReported;
	//! messages received through DirectPlay
	AutoArray<UserMessageInfo, MemAllocWin> _receivedUserMessages;
	//! completed messages infos
	AutoArray<SendCompleteInfo, MemAllocWin> _sendComplete;
	//! terminate session message arrived
	bool _terminateSession;
	//! why session terminated
	NetTerminationReason _whyTerminatedSession;

#if VOICE_OVER_NET
	//! voice is currently recording 
	bool _recording;
	//! voice is currently playing
	FindArray<int, MemAllocWin> _playing;
#endif

#if _ENABLE_MP
	//! lock to _lastMsgReceived
	CRITICAL_SECTION _lastMsgReceivedLock;
	//! lock to _receivedUserMessages
	CRITICAL_SECTION _receivedUserMessagesLock;
	//! lock to _sendComplete
	CRITICAL_SECTION _sendCompleteLock;
	//! lock to _terminateSession
	CRITICAL_SECTION _terminateSessionLock;
#endif

#if VOICE_OVER_NET
	//! lock for _recording 
	CRITICAL_SECTION _recordingLock;
	//! lock for _playing
	CRITICAL_SECTION _playingLock;
#endif

public:
	//! Constructor
	DPlayClient();
	//! Destructor
	~DPlayClient();

	ConnectResult Init
	(
		RString address, RString password, bool botClient, int &port,
		RString player, MPVersionInfo &versionInfo, int magic =0
	);
	//! Create and initialize DirectPlay voice client
	bool InitVoice();
	void Done();

	bool SendMsg(BYTE *buffer, int bufferSize, DWORD &msgID, NetMsgFlags flags);
	void GetSendQueueInfo(int &nMsg, int &nBytes, int &nMsgG, int &nBytesG);
	bool GetConnectionInfo(int &latencyMS, int &throughputBPS);
	bool GetConnectionInfoRaw(int &latencyMS, int &throughputBPS);

	float GetLastMsgAge();
	float GetLastMsgAgeReliable() {return 0;}
	float GetLastMsgAgeReported();
	void LastMsgAgeReported();

	bool IsSessionTerminated();
	NetTerminationReason GetWhySessionTerminated();

	void ProcessUserMessages(UserMessageClientCallback *callback, void *context);
	void RemoveUserMessages();
	void ProcessSendComplete(SendCompleteCallback *callback, void *context);
	void RemoveSendComplete();

	bool IsVoicePlaying(int player);
	bool IsVoiceRecording();
	NetTranspSound3DBuffer *Create3DSoundBuffer(int player);

	HRESULT ReceiveMessage(DWORD type, void *message);
#if VOICE_OVER_NET
	//! Process system message from DirectPlay voice client
	HRESULT ReceiveVoiceMessage(DWORD dwMessageType, LPVOID lpMessage);
#endif

	virtual RString GetStatistics();
};

//! DirectPlay implementation of NetTranspServer class
class DPlayServer : public NetTranspServer
{
protected:

#if _ENABLE_MP
	//! DirectPlay server interface
	IDirectPlay8Server *_dp;
#endif

#if VOICE_OVER_NET
	//! DirectPlay voice server interface
	IDirectPlayVoiceServer *_dpvoice;
#endif

	//! port on which session is running
	int _sessionPort;
	//! name of session (contain name of player and IP address of host)
	RString _sessionName;

	int _versionActual;
	int _versionRequired;
  RString _mod;
  bool _equalModRequired;

	//! messages received through DirectPlay
	AutoArray<UserMessageInfo, MemAllocWin> _receivedUserMessages;
	//! completed messages infos
	AutoArray<SendCompleteInfo, MemAllocWin> _sendComplete;
	//! new players infos
	AutoArray<CreatePlayerInfo, MemAllocWin> _createPlayers;
	//! removed players infos
	AutoArray<DeletePlayerInfo, MemAllocWin> _deletePlayers;

#if VOICE_OVER_NET
	//! connected Voice over net clients
	AutoArray<int, MemAllocWin>	_voiceUsers;
#endif

#if _ENABLE_MP
	//! lock to _receivedUserMessages
	CRITICAL_SECTION _receivedUserMessagesLock;
	//! lock to _sendComplete
	CRITICAL_SECTION _sendCompleteLock;
	//! lock to _createPlayers and _deletePlayers
	CRITICAL_SECTION _playersLock;
#endif

#if VOICE_OVER_NET
	//! lock for _voiceUsers
	CRITICAL_SECTION _voiceUsersLock;
#endif

public:
	//! Constructor
	DPlayServer();
	//! Destructor
	~DPlayServer();

	bool Init
	(
		int port, RString password, char *hostname, int maxPlayers,
		RString sessionNameInit, RString sessionNameFormat, RString playerName,
    MPVersionInfo &versionInfo, bool equalModRequired, int magic =0
	);
	//! Create and initialize DirectPlay voice server
	bool InitVoice();
	void Done();

	//! Set port on which session is running
	void SetSessionPort(int port) {_sessionPort = port;}
	//! Set name of session (contain name of player and IP address of host)
	void SetSessionName(RString name) {_sessionName = name;}
	int GetSessionPort() {return _sessionPort;}
	RString GetSessionName() {return _sessionName;}

	bool SendMsg(int to, BYTE *buffer, int bufferSize, DWORD &msgID, NetMsgFlags flags);
	void CancelAllMessages();
	void GetSendQueueInfo(int to, int &nMsg, int &nBytes, int &nMsgG, int &nBytesG);
	bool GetConnectionInfo(int to, int &latencyMS, int &throughputBPS);
	bool GetConnectionInfoRaw(int to, int &latencyMS, int &throughputBPS);
	float GetLastMsgAgeReliable(int player) {return 0;}
	void UpdateSessionDescription(int state, RString mission);
	bool GetURL(char *address, DWORD addressLen);
	void KickOff(int player, NetTerminationReason reason);

	void GetTransmitTargets(int from, AutoArray<int, MemAllocSA> &to);
	void SetTransmitTargets(int from, AutoArray<int, MemAllocSA> &to);

	void ProcessUserMessages(UserMessageServerCallback *callback, void *context);
	void RemoveUserMessages();
	void ProcessSendComplete(SendCompleteCallback *callback, void *context);
	void RemoveSendComplete();
	void ProcessPlayers(CreatePlayerCallback *callbackCreate, DeletePlayerCallback *callbackDelete, void *context);
	void RemovePlayers();
	void ProcessVoicePlayers(CreateVoicePlayerCallback *callback, void *context);
	void RemoveVoicePlayers();

	HRESULT ReceiveMessage(DWORD type, void *message);
#if VOICE_OVER_NET
	//! Process system message from DirectPlay voice client
	HRESULT ReceiveVoiceMessage(DWORD dwMessageType, LPVOID lpMessage);
#endif

	virtual RString GetStatistics(int player);
};

class DPlaySound3DBuffer : public NetTranspSound3DBuffer
{
protected:
	int _player;

#if VOICE_OVER_NET
	//! DirectPlay voice server interface
	IDirectPlayVoiceClient *_dpVoice;
	IDirectSound3DBuffer *_buffer;
#endif

public:
	//! Constructor
#if VOICE_OVER_NET
	DPlaySound3DBuffer(IDirectPlayVoiceClient *dpVoice, int player);
#endif
	//! Destructor
	~DPlaySound3DBuffer();

	void SetPosition(float x, float y, float z);

	bool IsValid(); 
};

/*****************************************************************************/
/*                                                                           */
/*                                Implementation                             */
/*                                                                           */
/*****************************************************************************/

NetTranspSessionEnum *CreateDPlaySessionEnum() {return new DPlaySessionEnum();}
NetTranspClient *CreateDPlayClient() {return new DPlayClient();}
NetTranspServer *CreateDPlayServer() {return new DPlayServer();}

int SessionDescriptions::Add()
{
	if (_size >= MAX_SESSIONS) return -1;
	return _size++;
}

void SessionDescriptions::Delete(int i)
{
	_size--;
	#if _ENABLE_MP
	SAFE_RELEASE(_data[i].address);
	#endif
	for (int j=i; j<_size; j++) _data[j] = _data[j + 1];
}

void SessionDescriptions::Clear()
{
	for (int i=0; i<_size; i++)
	{
		#if _ENABLE_MP
		SAFE_RELEASE(_data[i].address);
		#endif
	}
	_size = 0;
}

//! Handler called by DirectPlay when answer to session enumeration arrives
static HRESULT WINAPI OnEnumSession(void *context, DWORD messageType, void *message)
{
	#if _ENABLE_MP
	if (messageType == DPN_MSGID_ENUM_HOSTS_RESPONSE)
	{
		DPNMSG_ENUM_HOSTS_RESPONSE *msg = (DPNMSG_ENUM_HOSTS_RESPONSE *) message;
		DPlaySessionEnum *mgr = (DPlaySessionEnum *)context;
		mgr->OnEnumSession(msg->pApplicationDescription, msg->pAddressSender);
	}
	#endif
	return S_OK;
}

#if _ENABLE_MP
void DPlaySessionEnum::OnEnumSession(const DPN_APPLICATION_DESC *session, IDirectPlay8Address *address)
{
	EnterCriticalSection(&_sessionsLock);

	// Search for existing record for this session, if 
	int iFound = -1;
	for (int i=0; i<_sessions.Size(); i++)
	{
		if (_sessions[i].session.guidInstance == session->guidInstance)
		{
			iFound = i;
			break;
		}
	}
	
	if (iFound >= 0)
	{
		SAFE_RELEASE(_sessions[iFound].address);
	}
	else
	{
		iFound = _sessions.Add();
		if (iFound < 0)
		{
			LeaveCriticalSection(&_sessionsLock);
			return;
		}
	}

	#if _ENABLE_MP
	HRESULT hr = address->QueryInterface
	(
		IID_IDirectPlay8Address,
		(LPVOID *) &_sessions[iFound].address
	);
	if (FAILED(hr) || !_sessions[iFound].address)
	{
		_sessions.Delete(iFound);
		LeaveCriticalSection(&_sessionsLock);
		return;
	}
  memcpy(&_sessions[iFound].session, session, sizeof(DPN_APPLICATION_DESC));
	if (session->pwszSessionName)
	{
		WideCharToMultiByte
		(
			CP_ACP, 0, session->pwszSessionName, -1,
			_sessions[iFound].name, LEN_SESSION_NAME, NULL, NULL
		);
	}
	else
		_sessions[iFound].name[0] = 0;
	#endif

	_sessions[iFound].actualVersion = 0;
	_sessions[iFound].requiredVersion = 0;
	_sessions[iFound].gameState = 0;
	_sessions[iFound].mission[0] = 0;
  _sessions[iFound].mod[0] = 0;
  if (session->dwApplicationReservedDataSize >= sizeof(MPVersionInfoOld))
  {
    MPVersionInfoOld *versionInfo = (MPVersionInfoOld *)(session->pvApplicationReservedData);
		_sessions[iFound].actualVersion = versionInfo->versionActual;
		_sessions[iFound].requiredVersion = versionInfo->versionRequired;
		_sessions[iFound].gameState = versionInfo->gameState;
		strcpy(_sessions[iFound].mission, versionInfo->mission);
	}
  if (session->dwApplicationReservedDataSize >= sizeof(DPSessionPacket))
  {
    DPSessionPacket *versionInfo = (DPSessionPacket *)(session->pvApplicationReservedData);
    strcpy(_sessions[iFound].mod, versionInfo->mod);
    _sessions[iFound].equalModRequired = versionInfo->equalModRequired;
  }

	_sessions[iFound].lastTime = GetTickCount();

	_sessions[iFound].numPlayers = session->dwCurrentPlayers;
	_sessions[iFound].maxPlayers = session->dwMaxPlayers;
	
	LeaveCriticalSection(&_sessionsLock);
}
#endif

DPlaySessionEnum::DPlaySessionEnum()
{
	#if _ENABLE_MP
	CoInitialize(NULL);
	_dp = NULL;
	#endif
	InitializeCriticalSection(&_sessionsLock);
}

bool DPlaySessionEnum::Init( int magic )
{
	#if _ENABLE_MP
  HRESULT hr;
	if (!_dp)
	{
		hr = CoCreateInstance
		(
			CLSID_DirectPlay8Client, NULL, 
      CLSCTX_ALL, IID_IDirectPlay8Client,
      (LPVOID*)&_dp
		);
		if (FAILED(hr))
		{
			SAFE_RELEASE(_dp);
			return false;
		}

		hr = _dp->Initialize(this, ::OnEnumSession, 0);
		if (FAILED(hr))
		{
			SAFE_RELEASE(_dp);
			return false;
		}
	}
	#endif

	return true;
}

void DPlaySessionEnum::Done()
{
	EnterCriticalSection(&_sessionsLock);
	
	_sessions.Clear();
	
	LeaveCriticalSection(&_sessionsLock);

	StopEnumHosts();

	#if _ENABLE_MP
	if (_dp) _dp->Close(0);
	SAFE_RELEASE(_dp);
	#endif
}

DPlaySessionEnum::~DPlaySessionEnum()
{
	Done();
  DeleteCriticalSection(&_sessionsLock);

#if _ENABLE_MP
	CoUninitialize();
#endif
}

bool DPlaySessionEnum::RunningEnumHosts()
{
	#if _ENABLE_MP
	return _enumHosts.Size() > 0;
	#else
	return 0;
	#endif
}

void DPlaySessionEnum::StopEnumHosts()
{
	#if _ENABLE_MP
	if (!_dp) return;

/*
	if (_enumHosts)
	{
		Verify(!FAILED(_dp->CancelAsyncOperation(_enumHosts, 0)));
		_enumHosts = NULL;
	}
*/
	for (int i=0; i<_enumHosts.Size(); i++)
		Verify(!FAILED(_dp->CancelAsyncOperation(_enumHosts[i], 0)));
	_enumHosts.Resize(0);

	#endif
}

/*!
\patch 1.24 Date 09/19/2001 by Jirka
- Changed: all sessions (on all ports) are enumerated at once
\patch 1.27 Date 10/16/2001 by Jirka
- Added: enable multiple enumerations of hosts.
Now two enumerations are provided - on default DirectPlay port and on port given by player.
This fixes problems with enumeration with most firewalls.
*/
bool DPlaySessionEnum::StartEnumHosts(RString ip, int port, AutoArray<RemoteHostAddress> *hosts)
{
	#if _ENABLE_MP
	if (!_dp) return true;

	// If an enumeration is already running, cancel it then we'll start a new one.
	StopEnumHosts();

	int iplen = ip.GetLength();

  DPN_APPLICATION_DESC session;

	ComRef<IDirectPlay8Address> addressLocal;
	ComRef<IDirectPlay8Address> addressHost;

	// Create the local device address object
	HRESULT hr = CoCreateInstance
	(
		CLSID_DirectPlay8Address, NULL,
		CLSCTX_ALL, IID_IDirectPlay8Address,
		(LPVOID *)addressLocal.Init()
	);
	if (FAILED(hr)) return false;
	
	// Set IP service provider
	hr = addressLocal->SetSP(&CLSID_DP8SP_TCPIP);
	if (FAILED(hr)) return false;

	// Create the remote host address object
	hr = CoCreateInstance
	(
		CLSID_DirectPlay8Address, NULL,
		CLSCTX_ALL, IID_IDirectPlay8Address,
		(LPVOID *)addressHost.Init()
	);
	if (FAILED(hr)) return false;

	// Set IP service provider
	hr = addressHost->SetSP(&CLSID_DP8SP_TCPIP);
	if (FAILED(hr)) return false;

	// Set the remote host name (if provided)
	if (iplen > 0)
	{
		if (iplen >= 512) return false;
		WCHAR hostName[512];
		MultiByteToWideChar(CP_ACP, 0, ip, -1, hostName, 512);
		hostName[iplen] = 0;
		hr = addressHost->AddComponent
		(
			DPNA_KEY_HOSTNAME, hostName, (iplen + 1) * sizeof(WCHAR),
			DPNA_DATATYPE_STRING
		);
		if (FAILED(hr)) return false;
	}

	ZeroMemory(&session, sizeof(DPN_APPLICATION_DESC));
	session.dwSize = sizeof(DPN_APPLICATION_DESC);
	session.guidApplication = APP_GUID;

	// Enumerate all hosts running on IP service providers
	int index = _enumHosts.Add();
	DPNHANDLE &handle = _enumHosts[index];
	hr = _dp->EnumHosts
	(
		&session, addressHost, addressLocal,
		NULL, 0, INFINITE, 0, INFINITE, NULL, 
		&handle, 0
	);
	if (FAILED(hr)) _enumHosts.Delete(index);

	// Set given port
	hr = addressHost->AddComponent(DPNA_KEY_PORT, &port, sizeof(port), DPNA_DATATYPE_DWORD);
	if (!FAILED(hr))
	{
		int index = _enumHosts.Add();
		DPNHANDLE &handle = _enumHosts[index];
		hr = _dp->EnumHosts
		(
			&session, addressHost, addressLocal,
			NULL, 0, INFINITE, 0, INFINITE, NULL, 
			&handle, 0
		);
		if (FAILED(hr)) _enumHosts.Delete(index);
	}

	if (hosts)
	{
		for (int i=0; i<hosts->Size(); i++)
		{
			const RemoteHostAddress &address = hosts->Get(i);

			WCHAR hostName[512];
			MultiByteToWideChar(CP_ACP, 0, address.ip, -1, hostName, 512);
			hostName[address.ip.GetLength()] = 0;
			hr = addressHost->AddComponent
			(
				DPNA_KEY_HOSTNAME, hostName, (address.ip.GetLength() + 1) * sizeof(WCHAR),
				DPNA_DATATYPE_STRING
			);
			if (FAILED(hr)) continue;
			hr = addressHost->AddComponent
			(
				DPNA_KEY_PORT, &address.port, sizeof(address.port), DPNA_DATATYPE_DWORD
			);
			if (FAILED(hr)) continue;

			int index = _enumHosts.Add();
			DPNHANDLE &handle = _enumHosts[index];
			hr = _dp->EnumHosts
			(
				&session, addressHost, addressLocal,
				NULL, 0, INFINITE, 0, INFINITE, NULL, 
				&handle, 0
			);
			if (FAILED(hr)) _enumHosts.Delete(index);
		}
	}

	return _enumHosts.Size() > 0;

	#else
	return false;
	#endif
}

int DPlaySessionEnum::NSessions()
{
	EnterCriticalSection(&_sessionsLock);
	int n = _sessions.Size();
	LeaveCriticalSection(&_sessionsLock);
	return n;
}

void DPlaySessionEnum::GetSessions(AutoArray<SessionInfo> &sessions)
{
	EnterCriticalSection(&_sessionsLock);

	int n = _sessions.Size();
	for (int i=0; i<n;)
	{
		const SessionDescription &desc = _sessions[i];
		float age = 0.001 * (GetTickCount() - desc.lastTime);
		if (age > 15)
		{
			_sessions.Delete(i);
			n--;
		}
		else
		{
			i++;
		}
	}	
	
	sessions.Resize(n);
	for (int i=0; i<n; i++)
	{
		const SessionDescription &src = _sessions[i];
		SessionInfo &dst = sessions[i];
		char buffer[512]; buffer[0] = 0;
	#if _ENABLE_MP
		DWORD bufferLen = sizeof(buffer);
		if (src.address)
		{
			HRESULT hr = src.address->GetURLA(buffer, &bufferLen);
			if (FAILED(hr))
			{
				Fail("URL");
			}
		}
		else
		{
			Fail("NULL address");
		}
	#endif
		dst.guid = buffer;
		dst.name = src.name;
		dst.lastTime = src.lastTime;
	#if _ENABLE_MP
		dst.password = (src.session.dwFlags & DPNSESSION_REQUIREPASSWORD) != 0;
	#endif
		dst.actualVersion = src.actualVersion;
		dst.requiredVersion = src.requiredVersion;
		dst.badActualVersion = false;
		dst.badRequiredVersion = false;
		dst.gameState = src.gameState;
		dst.mission = src.mission;
		
		dst.numPlayers = src.numPlayers;
		dst.maxPlayers = src.maxPlayers;
		// TODO: fill this values
		dst.ping = 0;
		dst.timeleft = 0;

    dst.mod = src.mod;
    dst.equalModRequired = src.equalModRequired;
    dst.badMod = false;
  }

	LeaveCriticalSection(&_sessionsLock);
}

DPlayClient::DPlayClient()
{
#if _ENABLE_MP
	CoInitialize(NULL);
	_dp = NULL;
#endif

#if VOICE_OVER_NET
	_dpvoice = NULL;
#endif

	_lastMsgReceived = GetTickCount();
	_lastMsgReceivedReported = _lastMsgReceived;

	_terminateSession = false;
	_whyTerminatedSession = NTROther;

#if VOICE_OVER_NET
	_recording = false;
#endif

#if _ENABLE_MP
	InitializeCriticalSection(&_lastMsgReceivedLock);
	InitializeCriticalSection(&_receivedUserMessagesLock);
	InitializeCriticalSection(&_sendCompleteLock);
	InitializeCriticalSection(&_terminateSessionLock);
#endif

#if VOICE_OVER_NET
	InitializeCriticalSection(&_playingLock);
	InitializeCriticalSection(&_recordingLock);
#endif
}

DPlayClient::~DPlayClient()
{
	Done();

#if _ENABLE_MP
	DeleteCriticalSection(&_lastMsgReceivedLock);
	DeleteCriticalSection(&_receivedUserMessagesLock);
	DeleteCriticalSection(&_sendCompleteLock);
	DeleteCriticalSection(&_terminateSessionLock);
#endif

#if VOICE_OVER_NET
	DeleteCriticalSection(&_playingLock);
  DeleteCriticalSection(&_recordingLock);
#endif

#if _ENABLE_MP
	CoUninitialize();
#endif
}

//! Handler called by DirectPlay when message arrived
HRESULT WINAPI ReceiveClientMessage(void *context, DWORD type, void *message)
{
	DPlayClient *client = (DPlayClient *)context;
	return client->ReceiveMessage(type, message);
}

ConnectResult DPlayClient::Init
(
	RString address, RString password, bool botClient, int &port,
	RString player, MPVersionInfo &versionInfo, int magic
)
{
	ConnectResult error = CRError;

	#if _ENABLE_MP
#if _DEBUG
	DWORD dwFlags = 0;
#else
	DWORD dwFlags = DPNINITIALIZE_DISABLEPARAMVAL;
#endif
	ComRef<IDirectPlay8Address> addressLocal;
	ComRef<IDirectPlay8Address> addressHost;

	// create directplay client
	HRESULT hr;
	hr = CoCreateInstance
	(
		CLSID_DirectPlay8Client, NULL, 
    CLSCTX_ALL, IID_IDirectPlay8Client,
    (LPVOID*)&_dp
	);
	if (FAILED(hr)) goto ClientInitFailed;

	hr = _dp->Initialize(this, ::ReceiveClientMessage, dwFlags);
	if (FAILED(hr)) goto ClientInitFailed;

	// create the local device address object
	hr = CoCreateInstance
	(
		CLSID_DirectPlay8Address, NULL,
		CLSCTX_ALL, IID_IDirectPlay8Address,
		(LPVOID *)addressLocal.Init()
	);
	if (FAILED(hr)) goto ClientInitFailed;
	
	// set IP service provider
	hr = addressLocal->SetSP(&CLSID_DP8SP_TCPIP);
	if (FAILED(hr)) goto ClientInitFailed;

	// create the remote host address object
	hr = CoCreateInstance
	(
		CLSID_DirectPlay8Address, NULL,
		CLSCTX_ALL, IID_IDirectPlay8Address,
		(LPVOID *)addressHost.Init()
	);
	if (FAILED(hr)) goto ClientInitFailed;

	// fill address
	if (address.GetLength() > 0)
	{
		hr = addressHost->BuildFromURLA((char *)(const char *)address);
		if (FAILED(hr)) goto ClientInitFailed;
		DWORD size, type;
		size = sizeof(port);
		addressHost->GetComponentByName(DPNA_KEY_PORT, &port, &size, &type);
	}
	else
	{
		// Set IP service provider
		hr = addressHost->SetSP(&CLSID_DP8SP_TCPIP);
		if (FAILED(hr)) goto ClientInitFailed;

		// Set given port
		hr = addressHost->AddComponent(DPNA_KEY_PORT, &port, sizeof(port), DPNA_DATATYPE_DWORD);
		if (FAILED(hr)) goto ClientInitFailed;

		// Set the remote host to localhost (127.0.0.1)
		WCHAR *hostName = L"127.0.0.1";
		hr = addressHost->AddComponent
		(
			DPNA_KEY_HOSTNAME, hostName, (wcslen(hostName) + 1) * sizeof(WCHAR),
			DPNA_DATATYPE_STRING
		);
		if (FAILED(hr)) goto ClientInitFailed;
	}

	// set player info
	WCHAR playerName[512];
	MultiByteToWideChar(CP_ACP, 0, player, -1, playerName, 512);
	DPPlayerInfo info;
	info.botClient = botClient;
  strncpy(info.mod, versionInfo.mod, MOD_LENGTH);
  info.mod[MOD_LENGTH - 1] = 0;
	DPN_PLAYER_INFO playerInfo;
	ZeroMemory(&playerInfo, sizeof(DPN_PLAYER_INFO));
	playerInfo.dwSize = sizeof(DPN_PLAYER_INFO);
	playerInfo.dwInfoFlags = DPNINFO_NAME | DPNINFO_DATA;
	playerInfo.pwszName = playerName;
	playerInfo.pvData = &info;
	playerInfo.dwDataSize = sizeof(DPPlayerInfo);
	hr = _dp->SetClientInfo(&playerInfo, NULL, NULL, DPNSETCLIENTINFO_SYNC);

	// join session
	DPN_APPLICATION_DESC session;
	ZeroMemory(&session, sizeof(DPN_APPLICATION_DESC));
	session.dwSize = sizeof(DPN_APPLICATION_DESC);
	session.guidApplication = APP_GUID;

	WCHAR passwordWide[512];
	if (password.GetLength() > 0)
	{
		session.dwFlags |= DPNSESSION_REQUIREPASSWORD;
		MultiByteToWideChar(CP_ACP, 0, password, -1, passwordWide, 512);
		session.pwszPassword = passwordWide;
	}

	hr = _dp->Connect
	(
		&session, addressHost, addressLocal,
		NULL, NULL, &versionInfo, sizeof(versionInfo), NULL, NULL,
		DPNOP_SYNC
	);
	if (!FAILED(hr)) return CROK;

	if (hr == DPNERR_INVALIDPASSWORD) error = CRPassword;
	else if (hr == DPNERR_SESSIONFULL) error = CRSessionFull;
	else if (hr == DPNERR_HOSTREJECTEDCONNECTION) error = CRVersion;

ClientInitFailed:
	SAFE_RELEASE(_dp);
	#endif
	
	return error;
}

void DPlayClient::Done()
{
#if VOICE_OVER_NET
	if (_dpvoice) _dpvoice->Disconnect(DVFLAGS_SYNC);
	SAFE_RELEASE(_dpvoice);
#endif

#if _ENABLE_MP
	if (_dp) _dp->Close(0);
	SAFE_RELEASE(_dp);
#endif
}

bool DPlayClient::SendMsg(BYTE *buffer, int bufferSize, DWORD &msgID, NetMsgFlags flags)
{
	#if _ENABLE_MP
		Assert(_dp);

		DPN_BUFFER_DESC desc;
		desc.pBufferData = buffer;
		desc.dwBufferSize = bufferSize;

		DWORD timeout = (flags&NMFGuaranteed) ? 0 : SEND_TIMEOUT;

		DWORD dxFlags = 0;
		if (flags&NMFGuaranteed) dxFlags |= DPNSEND_GUARANTEED;
		if (flags&NMFHighPriority) dxFlags |= DPNSEND_PRIORITY_HIGH;
		return !FAILED(_dp->Send(&desc, 1, timeout, this, &msgID, dxFlags));
	#else
		return false;
	#endif
}

void DPlayClient::GetSendQueueInfo(int &nMsg, int &nBytes, int &nMsgG, int &nBytesG)
{
#if _ENABLE_MP
	_dp->GetSendQueueInfo((DWORD *)&nMsg, (DWORD *)&nBytes, 0);
    nMsgG = nBytesG = -1;                   // this info is not available
#endif
}

bool DPlayClient::GetConnectionInfoRaw(int &latencyMS, int &throughputBPS)
{
#if _ENABLE_MP
	DPN_CONNECTION_INFO info;
	ZeroMemory(&info, sizeof(DPN_CONNECTION_INFO));
	info.dwSize = sizeof(DPN_CONNECTION_INFO);
	HRESULT hr = _dp->GetConnectionInfo(&info, 0);
	latencyMS = info.dwRoundTripLatencyMS;
	throughputBPS = info.dwThroughputBPS;
	return !FAILED(hr);
#else
	return false;
#endif
}

bool DPlayClient::GetConnectionInfo(int &latencyMS, int &throughputBPS)
{
	bool result = GetConnectionInfoRaw(latencyMS, throughputBPS);
	if (result)
	{
		throughputBPS *= 2;
		throughputBPS += 10000;
	}
	return result;
}

HRESULT DPlayClient::ReceiveMessage(DWORD type, void *message)
{
#if _ENABLE_MP
	if (type != DPN_MSGID_SEND_COMPLETE)
	{
		EnterCriticalSection(&_lastMsgReceivedLock);
		_lastMsgReceived = GetTickCount();
		_lastMsgReceivedReported = _lastMsgReceived;
		LeaveCriticalSection(&_lastMsgReceivedLock);
	}

	switch (type)
	{
	case DPN_MSGID_RECEIVE:
		{
			DPNMSG_RECEIVE *dpMsg = (DPNMSG_RECEIVE *)message;
			EnterCriticalSection(&_receivedUserMessagesLock);
			int index = _receivedUserMessages.Add();
			UserMessageInfo &info = _receivedUserMessages[index];
			info.from = dpMsg->dpnidSender;
			info.messageSize = dpMsg->dwReceiveDataSize;
			info.message = (char *)dpMsg->pReceiveData;
			info.messageHandle = dpMsg->hBufferHandle;
			LeaveCriticalSection(&_receivedUserMessagesLock);
		}
		return DPNSUCCESS_PENDING;
	case DPN_MSGID_SEND_COMPLETE:
		{
			DPNMSG_SEND_COMPLETE *msg = (DPNMSG_SEND_COMPLETE *)message;
			EnterCriticalSection(&_sendCompleteLock);
			int index = _sendComplete.Add();
			SendCompleteInfo &info = _sendComplete[index];
			info.msgID = msg->hAsyncOp;
			info.ok = !FAILED(msg->hResultCode);
			LeaveCriticalSection(&_sendCompleteLock);
		}
		return S_OK;
	case DPN_MSGID_TERMINATE_SESSION:
		{
			EnterCriticalSection(&_terminateSessionLock);
			DPNMSG_TERMINATE_SESSION *dpMsg = (DPNMSG_TERMINATE_SESSION *)message;
			_terminateSession = true;
			NetTerminationReason reason = NTROther;
			if (dpMsg->dwTerminateDataSize>=4)
			{
				reason = NetTerminationReason(*(int *)dpMsg->pvTerminateData);
			}
			// get additional data from the message
			_whyTerminatedSession = reason;
			LeaveCriticalSection(&_terminateSessionLock);
		}
		return S_OK;
	default:
		// do not process this message
		return S_OK;
	}
#else
	return E_FAIL;
#endif
}

float DPlayClient::GetLastMsgAge()
{
	float age = 0;
#if _ENABLE_MP
	EnterCriticalSection(&_lastMsgReceivedLock);
	age = 0.001 * (GetTickCount() - _lastMsgReceived);
	LeaveCriticalSection(&_lastMsgReceivedLock);
#endif
	return age;
}

float DPlayClient::GetLastMsgAgeReported()
{
	float age = 0;
#if _ENABLE_MP
	EnterCriticalSection(&_lastMsgReceivedLock);
	age = 0.001 * (GetTickCount() - _lastMsgReceivedReported);
	LeaveCriticalSection(&_lastMsgReceivedLock);
#endif
	return age;
}

void DPlayClient::LastMsgAgeReported()
{
#if _ENABLE_MP
	EnterCriticalSection(&_lastMsgReceivedLock);
	_lastMsgReceivedReported = GetTickCount();
	LeaveCriticalSection(&_lastMsgReceivedLock);
#endif
}

bool DPlayClient::IsSessionTerminated()
{
	bool term = true;
#if _ENABLE_MP
	EnterCriticalSection(&_terminateSessionLock);
	term = _terminateSession;
	LeaveCriticalSection(&_terminateSessionLock);
#endif
	return term;
}

NetTerminationReason DPlayClient::GetWhySessionTerminated()
{
	NetTerminationReason reason = NTROther;
#if _ENABLE_MP
	EnterCriticalSection(&_terminateSessionLock);
	reason = _whyTerminatedSession;
	LeaveCriticalSection(&_terminateSessionLock);
#endif
	return reason;
}

void DPlayClient::ProcessUserMessages(UserMessageClientCallback *callback, void *context)
{
#if _ENABLE_MP
	EnterCriticalSection(&_receivedUserMessagesLock);
	for (int i=0; i<_receivedUserMessages.Size(); i++)
	{
		UserMessageInfo &info = _receivedUserMessages[i];
		callback(info.message, info.messageSize, context);
		_dp->ReturnBuffer(info.messageHandle, 0);
	}
	_receivedUserMessages.Clear();
	LeaveCriticalSection(&_receivedUserMessagesLock);
#endif
}

void DPlayClient::RemoveUserMessages()
{
#if _ENABLE_MP
	EnterCriticalSection(&_receivedUserMessagesLock);
	for (int i=0; i<_receivedUserMessages.Size(); i++)
	{
		UserMessageInfo &info = _receivedUserMessages[i];
		_dp->ReturnBuffer(info.messageHandle, 0);
	}
	_receivedUserMessages.Clear();
	LeaveCriticalSection(&_receivedUserMessagesLock);
#endif
}

void DPlayClient::ProcessSendComplete(SendCompleteCallback *callback, void *context)
{
#if _ENABLE_MP
	EnterCriticalSection(&_sendCompleteLock);
	for (int i=0; i<_sendComplete.Size(); i++)
	{
		SendCompleteInfo &info = _sendComplete[i];
		callback(info.msgID, info.ok, context);
	}
	_sendComplete.Clear();
	LeaveCriticalSection(&_sendCompleteLock);
#endif
}

void DPlayClient::RemoveSendComplete()
{
#if _ENABLE_MP
	EnterCriticalSection(&_sendCompleteLock);
	_sendComplete.Clear();
	LeaveCriticalSection(&_sendCompleteLock);
#endif
}

RString DPlayClient::GetStatistics()
{
#if _ENABLE_MP
	int msg, size, msgG, sizeG;
	GetSendQueueInfo(msg, size, msgG, sizeG);
	int ping, bandwidth;
	GetConnectionInfo(ping, bandwidth);
  return Format("ping %4dms bandwidth %4dkb queue %3d (%5dB)", ping, bandwidth / (1024 / 8), msg, size);
#else
	return RString();
#endif
}

#if VOICE_OVER_NET

//! Handler called by DirectPlay when voice message arrived
HRESULT FAR PASCAL ReceiveClientVoiceMessage(LPVOID pvUserContext, DWORD dwMessageType, LPVOID lpMessage)
{
	DPlayClient *client = (DPlayClient *)pvUserContext;
	client->ReceiveVoiceMessage(dwMessageType, lpMessage);
	return DV_OK;
}

HRESULT DPlayClient::ReceiveVoiceMessage(DWORD dwMessageType, LPVOID lpMessage)
{
	switch (dwMessageType)
	{
		case DVMSGID_RECORDSTART:
			EnterCriticalSection(&_recordingLock);
			_recording = true;
			LeaveCriticalSection(&_recordingLock);
			break;
		case DVMSGID_RECORDSTOP:
			EnterCriticalSection(&_recordingLock);
			_recording = false;
			LeaveCriticalSection(&_recordingLock);
			break;
		case DVMSGID_PLAYERVOICESTART:
			{
				DVMSG_PLAYERVOICESTART *msg = (DVMSG_PLAYERVOICESTART *)lpMessage;
				EnterCriticalSection(&_playingLock);
				_playing.AddUnique(msg->dvidSourcePlayerID);
				LeaveCriticalSection(&_playingLock);
			}
			break;
		case DVMSGID_PLAYERVOICESTOP:
			{
				DVMSG_PLAYERVOICESTOP *msg = (DVMSG_PLAYERVOICESTOP *)lpMessage;
				EnterCriticalSection(&_playingLock);
				int index = _playing.Find(msg->dvidSourcePlayerID);
				if (index >= 0) _playing.DeleteAt(index);
				LeaveCriticalSection(&_playingLock);
			}
			break;
	}
	return DV_OK;
}

extern HWND hwndApp;

bool DPlayClient::InitVoice()
{
	IDirectPlayVoiceTest *setup;
	HRESULT hr = CoCreateInstance
	(
		CLSID_DirectPlayVoiceTest, NULL, 
    CLSCTX_ALL, IID_IDirectPlayVoiceTest,
		(VOID**)&setup
	);
	if (FAILED(hr))
	{
		SAFE_RELEASE(setup);
		return false;
	}
	hr = setup->CheckAudioSetup(NULL, NULL, NULL, DVFLAGS_QUERYONLY);
	SAFE_RELEASE(setup);
	if (FAILED(hr)) return false;

	hr = CoCreateInstance
	(
		CLSID_DirectPlayVoiceClient, NULL, 
    CLSCTX_INPROC_SERVER, IID_IDirectPlayVoiceClient,
		(VOID**)&_dpvoice
	);
	if (FAILED(hr))
	{
		SAFE_RELEASE(_dpvoice);
		return false;
	}

	hr = _dpvoice->Initialize(_dp, ::ReceiveClientVoiceMessage, this, NULL, 0);
	if (FAILED(hr))
	{
		SAFE_RELEASE(_dpvoice);
		return false;
	}

	DVSOUNDDEVICECONFIG soundConfig;
	ZeroMemory(&soundConfig, sizeof(DVSOUNDDEVICECONFIG));
	soundConfig.dwSize = sizeof(DVSOUNDDEVICECONFIG);
	soundConfig.dwFlags = 0;
	soundConfig.guidPlaybackDevice = DSDEVID_DefaultVoicePlayback; 
	soundConfig.lpdsPlaybackDevice = NULL;
	soundConfig.guidCaptureDevice = DSDEVID_DefaultVoiceCapture; 
	soundConfig.lpdsCaptureDevice = NULL;
	soundConfig.hwndAppWindow = hwndApp;
	soundConfig.lpdsMainBuffer = NULL;
	soundConfig.dwMainBufferFlags = 0;
	soundConfig.dwMainBufferPriority = 0;
	DVCLIENTCONFIG clientConfig;
	ZeroMemory(&clientConfig, sizeof(DVCLIENTCONFIG));
	clientConfig.dwSize = sizeof(DVCLIENTCONFIG);
	clientConfig.dwFlags = DVCLIENTCONFIG_AUTOVOICEACTIVATED | DVCLIENTCONFIG_AUTORECORDVOLUME;
	clientConfig.lPlaybackVolume = DVPLAYBACKVOLUME_DEFAULT;
	clientConfig.lRecordVolume = DVRECORDVOLUME_LAST;
	clientConfig.dwThreshold = DVTHRESHOLD_UNUSED;
	clientConfig.dwBufferQuality = DVBUFFERQUALITY_DEFAULT;
	clientConfig.dwBufferAggressiveness = DVBUFFERAGGRESSIVENESS_DEFAULT;
	clientConfig.dwNotifyPeriod = 0;

	hr = _dpvoice->Connect(&soundConfig, &clientConfig, DVFLAGS_SYNC);
	if (FAILED(hr))
	{
		SAFE_RELEASE(_dpvoice);
		return false;
	}
	return true;
}

bool DPlayClient::IsVoicePlaying(int player)
{
	EnterCriticalSection(&_playingLock);
	bool result = _playing.Find(player) >= 0;
	LeaveCriticalSection(&_playingLock);
	return result;
}

bool DPlayClient::IsVoiceRecording()
{
	EnterCriticalSection(&_recordingLock);
	bool result = _recording;
	LeaveCriticalSection(&_recordingLock);
	return result;
}

NetTranspSound3DBuffer *DPlayClient::Create3DSoundBuffer(int player)
{
	DPlaySound3DBuffer *buffer = new DPlaySound3DBuffer(_dpvoice, player);
	if (buffer->IsValid()) return buffer;
	delete buffer;
	return NULL;
}

#else

bool DPlayClient::InitVoice()
{
	return true;
}

bool DPlayClient::IsVoicePlaying(int player)
{
	return false;
}

bool DPlayClient::IsVoiceRecording()
{
	return false;
}

NetTranspSound3DBuffer *DPlayClient::Create3DSoundBuffer(int player)
{
	return NULL;
}

#endif

DPlayServer::DPlayServer()
{
#if _ENABLE_MP
	CoInitialize(NULL);
	_dp = NULL;
#endif

#if VOICE_OVER_NET
	_dpvoice = NULL;
#endif

	_versionActual = 0;
	_versionRequired = 0;
  _equalModRequired = false;

#if _ENABLE_MP
	InitializeCriticalSection(&_receivedUserMessagesLock);
	InitializeCriticalSection(&_sendCompleteLock);
	InitializeCriticalSection(&_playersLock);
#endif

#if VOICE_OVER_NET
	InitializeCriticalSection(&_voiceUsersLock);
#endif
}

DPlayServer::~DPlayServer()
{
	Done();

#if _ENABLE_MP
	DeleteCriticalSection(&_receivedUserMessagesLock);
	DeleteCriticalSection(&_sendCompleteLock);
	DeleteCriticalSection(&_playersLock);
#endif

#if VOICE_OVER_NET
	DeleteCriticalSection(&_voiceUsersLock);
#endif

#if _ENABLE_MP
	CoUninitialize();
#endif
}

void DPlayServer::Done()
{
#if VOICE_OVER_NET
	if (_dpvoice) _dpvoice->StopSession(DVFLAGS_NOHOSTMIGRATE);
	SAFE_RELEASE(_dpvoice);
#endif

#if _ENABLE_MP
	if (_dp) _dp->Close(0);
	SAFE_RELEASE(_dp);
#endif
}

static HRESULT WINAPI ReceiveServerMessage(void *context, DWORD type, void *message)
{
	DPlayServer *server = (DPlayServer *)context;
	return server->ReceiveMessage(type, message);
}

/*!
\patch 1.41 Date 1/3/2002 by Jirka
- Fixed: Support for Windows Me ICS, Windows XP ICS, and UPnP-compliant NATs
*/

bool DPlayServer::Init
(
	int port, RString password, char *hostname, int maxPlayers,
	RString sessionNameInit, RString sessionNameFormat, RString playerName,
  MPVersionInfo &versionInfo, bool equalModRequired, int magic
)
{
	#if _ENABLE_MP
	if (!_dp)
	{
		// thread safe - DirectPlay thread is not running yet
		_versionActual = versionInfo.versionActual;
		_versionRequired = versionInfo.versionRequired;
    _mod = versionInfo.mod;
	}
  _equalModRequired = equalModRequired;

	HRESULT hr;
	IDirectPlay8Address *addressLocal = NULL;
#if _DEBUG
	DWORD dwFlags = 0;
#else
	DWORD dwFlags = DPNINITIALIZE_DISABLEPARAMVAL;
#endif

	// create directplay client
	hr = CoCreateInstance
	(
		CLSID_DirectPlay8Server, NULL, 
    CLSCTX_INPROC_SERVER, IID_IDirectPlay8Server,
    (LPVOID*)&_dp
	);
	if (FAILED(hr)) goto ServerInitFailed;

	hr = _dp->Initialize(this, ::ReceiveServerMessage, dwFlags);
	if (FAILED(hr)) goto ServerInitFailed;

	// create the local device address object
	hr = CoCreateInstance
	(
		CLSID_DirectPlay8Address, NULL,
		CLSCTX_ALL, IID_IDirectPlay8Address,
		(LPVOID *)&addressLocal
	);
	if (FAILED(hr)) goto ServerInitFailed;
	
	// set IP service provider
	hr = addressLocal->SetSP(&CLSID_DP8SP_TCPIP);
	if (FAILED(hr)) goto ServerInitFailed;

	// set given port
	hr = addressLocal->AddComponent(DPNA_KEY_PORT, &port, sizeof(port), DPNA_DATATYPE_DWORD);
	if (FAILED(hr)) goto ServerInitFailed;

	// create session description
	WCHAR nameWide[512];
	WCHAR passwordWide[512];
	DPN_APPLICATION_DESC session;
	ZeroMemory(&session, sizeof(DPN_APPLICATION_DESC));
	session.dwSize = sizeof(DPN_APPLICATION_DESC);
	session.dwFlags = DPNSESSION_CLIENT_SERVER;
	session.guidApplication = APP_GUID;
	session.dwMaxPlayers = maxPlayers;

	{
		MultiByteToWideChar(CP_ACP, 0, sessionNameInit, -1, nameWide, 512);
		session.pwszSessionName = nameWide;
	}

	if (password.GetLength() > 0)
	{
		session.dwFlags |= DPNSESSION_REQUIREPASSWORD;
		MultiByteToWideChar(CP_ACP, 0, password, -1, passwordWide, 512);
		session.pwszPassword = passwordWide;
	}

  DPSessionPacket packet;
  (MPVersionInfo &)packet = versionInfo;
  packet.equalModRequired = equalModRequired;
  session.pvApplicationReservedData = &packet;
  session.dwApplicationReservedDataSize = sizeof(packet);

	// create session
	hr = _dp->Host(&session, &addressLocal, 1, NULL, NULL, NULL, 0);
	if (FAILED(hr))
	{
		// allow DPlay select port:

		// create local address without port
		hr = addressLocal->Clear();
		if (FAILED(hr)) goto ServerInitFailed;
		// set IP service provider
		hr = addressLocal->SetSP(&CLSID_DP8SP_TCPIP);
		if (FAILED(hr)) goto ServerInitFailed;
		// create session
		hr = _dp->Host(&session, &addressLocal, 1, NULL, NULL, NULL, 0);
		if (FAILED(hr)) goto ServerInitFailed;
	}

	SetSessionPort(port);

	// cleanup
	SAFE_RELEASE(addressLocal);

	// IMPROVED
	{
		// retreive local address
		DWORD nAddresses = 0;
		HRESULT hr = _dp->GetLocalHostAddresses(NULL, &nAddresses, 0);
		if (!FAILED(hr) || hr == DPNERR_BUFFERTOOSMALL)
		{
			DoAssert(nAddresses >= 1);
			AutoArray<IDirectPlay8Address *> addressHost;
			addressHost.Resize(nAddresses);
			for (int i=0; i<nAddresses; i++) addressHost[i] = NULL;

			hr = _dp->GetLocalHostAddresses(addressHost.Data(), &nAddresses, 0);
			if (!FAILED(hr))
			{
				DWORD size, type;
				size = sizeof(port);
				addressHost[0]->GetComponentByName(DPNA_KEY_PORT, &port, &size, &type);
				SetSessionPort(port);
				if (hostname[0] == 0)
				{
					// whole address is now in addressHost[0]
					WCHAR hostnameWide[512];
					size = sizeof(hostnameWide);
					addressHost[0]->GetComponentByName(DPNA_KEY_HOSTNAME, &hostnameWide, &size, &type);
					WideCharToMultiByte(CP_ACP, 0, hostnameWide, -1, hostname, 512, NULL, NULL);
//					if (port != DefaultNetworkPort)
					sprintf(hostname + strlen(hostname), ":%d", port);
				}

				if (playerName.GetLength() > 0)
				{
					char name[512];
					sprintf
					(
						name, sessionNameFormat,
						(const char *)playerName, hostname
					);
					SetSessionName(name);
					MultiByteToWideChar(CP_ACP, 0, name, -1, nameWide, 512);
				}
				else
				{
					SetSessionName(hostname);
					MultiByteToWideChar(CP_ACP, 0, hostname, -1, nameWide, 512);
				}

				size = 0;
				hr = _dp->GetApplicationDesc(NULL, &size, 0);
				Temp<char> buffer(size);
				DPN_APPLICATION_DESC *s = (DPN_APPLICATION_DESC *)buffer.Data();
				ZeroMemory(s, size);
				s->dwSize = sizeof(DPN_APPLICATION_DESC);
				hr = _dp->GetApplicationDesc(s, &size, 0);
				s->pwszSessionName = nameWide;
				hr = _dp->SetApplicationDesc(s, 0);
			}

			for (int i=0; i<nAddresses; i++) SAFE_RELEASE(addressHost[i]);
		}
	}
	return true;

ServerInitFailed:
	// cleanup
	SAFE_RELEASE(addressLocal);
	SAFE_RELEASE(_dp);

	#endif
	return false;
}

bool DPlayServer::SendMsg(int to, BYTE *buffer, int bufferSize, DWORD &msgID, NetMsgFlags flags)
{
#if _ENABLE_MP
	Assert(_dp);

	DPN_BUFFER_DESC desc;
	desc.pBufferData = buffer;
	desc.dwBufferSize = bufferSize;

	DWORD timeout = (flags&NMFGuaranteed) ? 0 : SEND_TIMEOUT;
	DWORD dxFlags = 0;
	if (flags&NMFGuaranteed) dxFlags |= DPNSEND_GUARANTEED;
	if (flags&NMFHighPriority) dxFlags |= DPNSEND_PRIORITY_HIGH;
	return !FAILED(_dp->SendTo(to, &desc, 1, timeout, this, &msgID, dxFlags));
#else
	return false;
#endif
}

void DPlayServer::CancelAllMessages()
{
#if _ENABLE_MP
	_dp->CancelAsyncOperation(NULL, DPNCANCEL_SEND);
#endif
}

void DPlayServer::GetSendQueueInfo(int to, int &nMsg, int &nBytes, int &nMsgG, int &nBytesG)
{
#if _ENABLE_MP
	_dp->GetSendQueueInfo(to, (DWORD *)&nMsg, (DWORD *)&nBytes, 0);
    nMsgG = nBytesG = -1;                   // this info is not available
#endif
}

bool DPlayServer::GetConnectionInfoRaw(int to, int &latencyMS, int &throughputBPS)
{
#if _ENABLE_MP
	DPN_CONNECTION_INFO info;
	ZeroMemory(&info, sizeof(DPN_CONNECTION_INFO));
	info.dwSize = sizeof(DPN_CONNECTION_INFO);
	HRESULT hr = _dp->GetConnectionInfo(to, &info, 0);
	latencyMS = info.dwRoundTripLatencyMS;
	throughputBPS = info.dwThroughputBPS;
	return !FAILED(hr);
#else
	return false;
#endif
}

bool DPlayServer::GetConnectionInfo(int to, int &latencyMS, int &throughputBPS)
{
	bool result = GetConnectionInfoRaw(to, latencyMS, throughputBPS);
	if (result)
	{
		throughputBPS *= 2;
		throughputBPS += 10000;
	}
	return result;
}

bool DPlayServer::GetURL(char *address, DWORD addressLen)
{
	#if _ENABLE_MP
	DWORD nAddresses = 0;
	HRESULT hr = _dp->GetLocalHostAddresses(NULL, &nAddresses, 0);
	if (FAILED(hr) && hr != DPNERR_BUFFERTOOSMALL)
		return false;

	Assert(nAddresses >= 1);

	AutoArray<IDirectPlay8Address *> addressHost;
	addressHost.Resize(nAddresses);
	for (int i=0; i<nAddresses; i++) addressHost[i] = NULL;

	hr = _dp->GetLocalHostAddresses(addressHost.Data(), &nAddresses, 0);
	if (FAILED(hr))
	{
		for (int i=0; i<nAddresses; i++) SAFE_RELEASE(addressHost[i]);
		return false;
	}

	addressHost[0]->GetURLA(address, &addressLen);

	for (int i=0; i<nAddresses; i++) SAFE_RELEASE(addressHost[i]);
	#endif
	return true;
}

void DPlayServer::UpdateSessionDescription(int state, RString mission)
{
	#if _ENABLE_MP
	// receive description
	DWORD size = 0;
	HRESULT hr = _dp->GetApplicationDesc(NULL, &size, 0);
	Temp<char> buffer(size);
	DPN_APPLICATION_DESC *s = (DPN_APPLICATION_DESC *)buffer.Data();
	ZeroMemory(s, size);
	s->dwSize = sizeof(DPN_APPLICATION_DESC);
	hr = _dp->GetApplicationDesc(s, &size, 0);
	
	// update description
  if (s->dwApplicationReservedDataSize >= sizeof(MPVersionInfoOld))
	{
    MPVersionInfoOld *userData = (MPVersionInfoOld *)s->pvApplicationReservedData;
		userData->gameState = state;
		strncpy(userData->mission, mission, sizeof(userData->mission));
		userData->mission[sizeof(userData->mission) - 1] = 0;
	}

	// send description
	hr = _dp->SetApplicationDesc(s, 0);
	#endif
}

void DPlayServer::KickOff(int player, NetTerminationReason reason)
{
	#if _ENABLE_MP
	int data = reason;
	_dp->DestroyClient(player, &data, sizeof (data), 0);
	#endif
}

HRESULT DPlayServer::ReceiveMessage(DWORD type, void *message)
{
#if _ENABLE_MP
	switch (type)
	{
	case DPN_MSGID_RECEIVE:
		{
			DPNMSG_RECEIVE *dpMsg = (DPNMSG_RECEIVE *)message;
			EnterCriticalSection(&_receivedUserMessagesLock);
			int index = _receivedUserMessages.Add();
			UserMessageInfo &info = _receivedUserMessages[index];
			info.from = dpMsg->dpnidSender;
			info.messageSize = dpMsg->dwReceiveDataSize;
			info.message = (char *)dpMsg->pReceiveData;
			info.messageHandle = dpMsg->hBufferHandle;
			LeaveCriticalSection(&_receivedUserMessagesLock);
		}
		return DPNSUCCESS_PENDING;
	case DPN_MSGID_SEND_COMPLETE:
		{
			DPNMSG_SEND_COMPLETE *msg = (DPNMSG_SEND_COMPLETE *)message;
			EnterCriticalSection(&_sendCompleteLock);
			int index = _sendComplete.Add();
			SendCompleteInfo &info = _sendComplete[index];
			info.msgID = msg->hAsyncOp;
			info.ok = !FAILED(msg->hResultCode);
			LeaveCriticalSection(&_sendCompleteLock);
		}
		return S_OK;
	case DPN_MSGID_INDICATE_CONNECT:
    {
      DPNMSG_INDICATE_CONNECT *msg = (DPNMSG_INDICATE_CONNECT *)message;
      if (msg->dwUserConnectDataSize >= sizeof(MPVersionInfo) && _equalModRequired)
      {
        // check mods compatibility
        MPVersionInfo *versionInfo = (MPVersionInfo *)(msg->pvUserConnectData);
        if (stricmp(versionInfo->mod, _mod) != 0) return DPNERR_HOSTREJECTEDCONNECTION;
      }
      if (msg->dwUserConnectDataSize >= sizeof(MPVersionInfoOld))
      {
        MPVersionInfoOld *versionInfo = (MPVersionInfoOld *)(msg->pvUserConnectData);
        if
          (
          versionInfo->versionActual >= _versionRequired &&
          _versionActual >= versionInfo->versionRequired
          ) return S_OK;
      }
      return DPNERR_HOSTREJECTEDCONNECTION;
    }
	case DPN_MSGID_CREATE_PLAYER:
		{
			DPNMSG_CREATE_PLAYER *msg = (DPNMSG_CREATE_PLAYER *)message;
			DPNID player = msg->dpnidPlayer;
      // get the player info and extract name
      DWORD size = 0;
      DPN_PLAYER_INFO *playerInfo = NULL;
      HRESULT hr = _dp->GetClientInfo(player, playerInfo, &size, 0); 
      if (FAILED(hr) && hr != DPNERR_BUFFERTOOSMALL) return S_OK;
			playerInfo = (DPN_PLAYER_INFO *)GlobalAlloc(GMEM_FIXED, size);
      ZeroMemory(playerInfo, size);
      playerInfo->dwSize = sizeof(DPN_PLAYER_INFO);
      hr = _dp->GetClientInfo(player, playerInfo, &size, 0);
      if (FAILED(hr))
			{
				GlobalFree((HGLOBAL)playerInfo);
				return S_OK;
			}
			char playerName[512];
			WideCharToMultiByte(CP_ACP, 0, playerInfo->pwszName, -1, playerName, 512, NULL, NULL);

      RString mod;
      bool botClient = false;
      if (playerInfo->dwDataSize == sizeof(DPPlayerInfo))
      {
        DPPlayerInfo *info = (DPPlayerInfo *)playerInfo->pvData;
        mod = info->mod;
        botClient = info->botClient;
      }
      else if (playerInfo->dwDataSize == sizeof(DPPlayerInfoOld))
      {
        DPPlayerInfoOld *info = (DPPlayerInfoOld *)playerInfo->pvData;
        botClient = info->botClient;
      }
      else
      {
        Fail("Wrong size");
      }
			GlobalFree((HGLOBAL)playerInfo);

			{
//LogF("CREATE_PLAYER: Trying player lock");
				EnterCriticalSection(&_playersLock);
//LogF("CREATE_PLAYER: Enter player lock");
				int index = _createPlayers.Add();
				CreatePlayerInfo &info = _createPlayers[index];
				info.player = player;
				info.botClient = botClient;
				strncpy(info.name, playerName, sizeof(info.name));
				info.name[sizeof(info.name) - 1] = 0;
        strncpy(info.mod, mod, sizeof(info.mod));
        info.mod[sizeof(info.mod) - 1] = 0;
//LogF("CREATE_PLAYER: Leave player lock");
				LeaveCriticalSection(&_playersLock);
			}
		}
		return S_OK;
	case DPN_MSGID_DESTROY_PLAYER:
		{
			DPNMSG_DESTROY_PLAYER *msg = (DPNMSG_DESTROY_PLAYER *)message;
			DPNID player = msg->dpnidPlayer;
//LogF("DESTROY_PLAYER: Trying player lock");
			EnterCriticalSection(&_playersLock);
//LogF("DESTROY_PLAYER: Enter player lock");
			bool found = false;
			for (int i=0; i<_createPlayers.Size(); i++)
			{
				if (_createPlayers[i].player == player)
				{
					_createPlayers.Delete(i);
					found = true;
					break;
				}
			}
			if (!found)
			{
				int index = _deletePlayers.Add();
				DeletePlayerInfo &info = _deletePlayers[index];
				info.player = player;
			}
//LogF("DESTROY_PLAYER: Leave player lock");
			LeaveCriticalSection(&_playersLock);
		}
		return S_OK;
	default:
		// do not process this message
		return S_OK;
	}
#else
	return E_FAIL;
#endif
}

void DPlayServer::ProcessUserMessages(UserMessageServerCallback *callback, void *context)
{
	#if _ENABLE_MP
	EnterCriticalSection(&_receivedUserMessagesLock);
	for (int i=0; i<_receivedUserMessages.Size(); i++)
	{
		UserMessageInfo &info = _receivedUserMessages[i];
		callback(info.from, info.message, info.messageSize, context);
		_dp->ReturnBuffer(info.messageHandle, 0);
	}
	_receivedUserMessages.Clear();
	LeaveCriticalSection(&_receivedUserMessagesLock);
	#endif
}

void DPlayServer::RemoveUserMessages()
{
	#if _ENABLE_MP
	EnterCriticalSection(&_receivedUserMessagesLock);
	for (int i=0; i<_receivedUserMessages.Size(); i++)
	{
		UserMessageInfo &info = _receivedUserMessages[i];
		_dp->ReturnBuffer(info.messageHandle, 0);
	}
	_receivedUserMessages.Clear();
	LeaveCriticalSection(&_receivedUserMessagesLock);
	#endif
}

void DPlayServer::ProcessSendComplete(SendCompleteCallback *callback, void *context)
{
	#if _ENABLE_MP
	EnterCriticalSection(&_sendCompleteLock);
	for (int i=0; i<_sendComplete.Size(); i++)
	{
		SendCompleteInfo &info = _sendComplete[i];
		callback(info.msgID, info.ok, context);
	}
	_sendComplete.Clear();
	LeaveCriticalSection(&_sendCompleteLock);
	#endif
}

void DPlayServer::RemoveSendComplete()
{
	#if _ENABLE_MP
	EnterCriticalSection(&_sendCompleteLock);
	_sendComplete.Clear();
	LeaveCriticalSection(&_sendCompleteLock);
	#endif
}

void DPlayServer::ProcessPlayers(CreatePlayerCallback *callbackCreate, DeletePlayerCallback *callbackDelete, void *context)
{
	#if _ENABLE_MP
//LogF("ProcessPlayers: Trying player lock");
	EnterCriticalSection(&_playersLock);
//LogF("ProcessPlayers: Enter player lock");
	for (int i=0; i<_deletePlayers.Size(); i++)
	{
		DeletePlayerInfo &info = _deletePlayers[i];
		callbackDelete(info.player, context);
	}
	_deletePlayers.Clear();
	for (int i=0; i<_createPlayers.Size(); i++)
	{
		CreatePlayerInfo &info = _createPlayers[i];
		callbackCreate(info.player, info.botClient, info.name, info.mod, context);
	}
	_createPlayers.Clear();
//LogF("ProcessPlayers: Leave player lock");
	LeaveCriticalSection(&_playersLock);
	#endif
}

void DPlayServer::RemovePlayers()
{
	#if _ENABLE_MP
//LogF("RemovePlayers: Trying player lock");
	EnterCriticalSection(&_playersLock);
//LogF("RemovePlayers: Enter player lock");
	_createPlayers.Clear();
	_deletePlayers.Clear();
//LogF("RemovePlayers: Leave player lock");
	LeaveCriticalSection(&_playersLock);
	#endif
}

void DPlayServer::ProcessVoicePlayers(CreateVoicePlayerCallback *callback, void *context)
{
#if VOICE_OVER_NET
	EnterCriticalSection(&_voiceUsersLock);
	for (int i=0; i<_voiceUsers.Size();)
	{
		if (callback(_voiceUsers[i], context)) _voiceUsers.Delete(i);
		else i++;
	}
	LeaveCriticalSection(&_voiceUsersLock);
#endif
}

void DPlayServer::RemoveVoicePlayers()
{
#if VOICE_OVER_NET
	EnterCriticalSection(&_voiceUsersLock);
	_voiceUsers.Clear();
	LeaveCriticalSection(&_voiceUsersLock);
#endif
}

void DPlayServer::GetTransmitTargets(int from, AutoArray<int, MemAllocSA> &to)
{
#if VOICE_OVER_NET
	if (!_dpvoice) return;
	DWORD size = 0;
	_dpvoice->GetTransmitTargets(from, NULL, &size, 0);
	to.Resize(size);
	_dpvoice->GetTransmitTargets(from, (DWORD *)to.Data(), &size, 0);
	DoAssert(to.Size() == size);
#endif
}

void DPlayServer::SetTransmitTargets(int from, AutoArray<int, MemAllocSA> &to)
{
#if VOICE_OVER_NET
	if (!_dpvoice) return;
	if (to.Size() == 0)
		_dpvoice->SetTransmitTargets(from, NULL, 0, 0);
	else
		_dpvoice->SetTransmitTargets(from, (DWORD *)to.Data(), to.Size(), 0);
#endif
}

RString DPlayServer::GetStatistics(int player)
{
#if _ENABLE_MP
	int msg, size, msgG, sizeG;
	GetSendQueueInfo(player, msg, size, msgG, sizeG);
	int ping, bandwidth;
	GetConnectionInfo(player, ping, bandwidth);
  return Format("ping %4dms bandwidth %4dkb queue %3d (%5dB)", ping, bandwidth / (1024 / 8), msg, size);
#else
	return RString();
#endif
}

#if VOICE_OVER_NET

//! Handler called by DirectPlay when voice message arrived
HRESULT FAR PASCAL ReceiveServerVoiceMessage(LPVOID pvUserContext, DWORD dwMessageType, LPVOID lpMessage)
{
	DPlayServer *server = (DPlayServer *)pvUserContext;
	return server->ReceiveVoiceMessage(dwMessageType, lpMessage);
}

HRESULT DPlayServer::ReceiveVoiceMessage(DWORD dwMessageType, LPVOID lpMessage)
{
	#if _ENABLE_MP
	switch (dwMessageType)
	{
	case DVMSGID_CREATEVOICEPLAYER:
		{
			DVMSG_CREATEVOICEPLAYER *msg = (DVMSG_CREATEVOICEPLAYER *)lpMessage;
			EnterCriticalSection(&_voiceUsersLock);
			_voiceUsers.Add(msg->dvidPlayer);
			LeaveCriticalSection(&_voiceUsersLock);
			LogF("Voice over net: player %d added", msg->dvidPlayer);
		}
		break;
	}
	#endif
	return DV_OK;
}

bool DPlayServer::InitVoice()
{
	#if _ENABLE_MP
	Assert(_dp);

	HRESULT hr = CoCreateInstance
	(
		CLSID_DirectPlayVoiceServer, NULL, 
    CLSCTX_ALL, IID_IDirectPlayVoiceServer,
		(VOID**)&_dpvoice
	);
	if (FAILED(hr)) return false;

	hr = _dpvoice->Initialize(_dp, ::ReceiveServerVoiceMessage, this, NULL, 0);
	if (FAILED(hr)) return false;
	DVSESSIONDESC desc;
	desc.dwSize = sizeof(DVSESSIONDESC);
	desc.dwFlags = DVSESSION_NOHOSTMIGRATION | DVSESSION_SERVERCONTROLTARGET;
	//desc.dwSessionType = DVSESSIONTYPE_MIXING;
	//desc.dwSessionType = DVSESSIONTYPE_PEER;
	desc.dwSessionType = DVSESSIONTYPE_FORWARDING; 
	desc.guidCT = DPVCTGUID_DEFAULT;
	desc.dwBufferQuality = DVBUFFERQUALITY_DEFAULT;
	desc.dwBufferAggressiveness = DVBUFFERAGGRESSIVENESS_DEFAULT;
	hr = _dpvoice->StartSession(&desc, 0);
	if (FAILED(hr)) return false;
	#endif
	return true;
}

#else

bool DPlayServer::InitVoice()
{
	return true;
}

#endif

#if VOICE_OVER_NET

DPlaySound3DBuffer::DPlaySound3DBuffer(IDirectPlayVoiceClient *dpVoice, int player)
{
	_player = player;

	_dpVoice = dpVoice;
	_buffer = NULL;
	if (dpVoice)
	{
		_dpVoice->AddRef();

		HRESULT hr = _dpVoice->Create3DSoundBuffer
		(
			player, NULL, 0,
//		DSBCAPS_GETCURRENTPOSITION2 | DSBCAPS_CTRL3D,
			0, &_buffer
		);
		if (!_buffer)
		{
			DXTRACE_ERR_NOMSGBOX("Create3DSoundBuffer", hr);
		}
	}
}

#endif

DPlaySound3DBuffer::~DPlaySound3DBuffer()
{
#if VOICE_OVER_NET
	if (_dpVoice)
	{
		if (_buffer) _dpVoice->Delete3DSoundBuffer(_player, &_buffer); 
		_dpVoice->Release();
	}
#endif
}

void DPlaySound3DBuffer::SetPosition(float x, float y, float z)
{
#if VOICE_OVER_NET
	if (_buffer) _buffer->SetPosition(x, y, z, 0);
#endif
}

bool DPlaySound3DBuffer::IsValid()
{
#if VOICE_OVER_NET
	return _buffer != NULL;
#else
	return true;
#endif
}

//! Transfer IP address and port into DirectPlay URL address
RString DPlaySessionEnum::IPToGUID(RString ip, int port)
{
#if _ENABLE_MP
	ComRef<IDirectPlay8Address> address;

	// create the local device address object
	HRESULT hr = CoCreateInstance
	(
		CLSID_DirectPlay8Address, NULL,
		CLSCTX_ALL, IID_IDirectPlay8Address,
		(LPVOID *)address.Init()
	);
	if (FAILED(hr)) return "";
	
	// Set IP service provider
	hr = address->SetSP(&CLSID_DP8SP_TCPIP);
	if (FAILED(hr)) return "";

	// Set given port
	// port = FLASHPOINT_SERVER_PORT;
	hr = address->AddComponent(DPNA_KEY_PORT, &port, sizeof(port), DPNA_DATATYPE_DWORD);
	if (FAILED(hr)) return "";

	// Set the remote host
	WCHAR hostName[512];
	MultiByteToWideChar(CP_ACP, 0, ip, -1, hostName, 512);
	hr = address->AddComponent
	(
		DPNA_KEY_HOSTNAME, hostName, (wcslen(hostName) + 1) * sizeof(WCHAR),
		DPNA_DATATYPE_STRING
	);
	if (FAILED(hr)) return "";

	char buffer[512];
	DWORD bufferLen = 512;
	hr = address->GetURLA(buffer, &bufferLen);
	if (FAILED(hr)) return "";

	return buffer;
#else
	return "";
#endif
}
