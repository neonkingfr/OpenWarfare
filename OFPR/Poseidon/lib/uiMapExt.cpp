// Implementation of mission editor
#include "wpch.hpp"
#include "uiMap.hpp"
#include <Es/Common/win.h>
#include "dikCodes.h"

#include "resincl.hpp"
#include "keyInput.hpp"
#include "strFormat.hpp"
#include <El/QStream/QBStream.hpp>

#include "landscape.hpp"
#include "txtPreload.hpp"

#include "packFiles.hpp"

#if defined _WIN32 && !defined _XBOX
#include <mapi.h>
#endif

#include "debugTrap.hpp"
#include "stringtableExt.hpp"

#include <Es/Strings/bstring.hpp>

/*!
\file
Implementation file for particular displays (maps, briefing, debriefing, mission editor).
*/

ArcadeTemplate GClipboard;

static void DeleteROFile(RString name)
{
#ifdef _WIN32
	DWORD attr = ::GetFileAttributes(name);
	attr &= ~FILE_ATTRIBUTE_READONLY;
	::SetFileAttributes(name, attr);
	::DeleteFile(name);
#else
	LocalPath(fn,(const char *)name);
	chmod(fn,S_IREAD|S_IWRITE);
	unlink(fn);
#endif
}

void CStaticMap::DrawEllipse(Vector3Val position, float a, float b, float angle, PackedColor color)
{
	DrawCoord posMap = WorldToScreen(position);
	float cx = posMap.x * _wScreen;
	float cy = posMap.y * _hScreen;

	const float invSizeLand = InvLandSize;
	float aMap = a * invSizeLand * _invScaleX * _wScreen;
	float bMap = b * invSizeLand * _invScaleY * _hScreen;
	
	// TODO: improve clipping
	float r = floatMax(aMap, bMap);
	if (cx + r < _clipRect.x) return;
	if (cy + r < _clipRect.y) return;
	if (cx - r > _clipRect.x + _clipRect.w) return;
	if (cy - r > _clipRect.y + _clipRect.h) return;

	float angleRad = HDegree(angle);
	float s = sin(angleRad);
	float c = cos(angleRad);

	static const float linePts = 5.0f;
	int nSteps = toIntCeil(2 * H_PI * aMap * (1.0f / linePts));
	saturate(nSteps, 6, 720);
	float step = 2 * H_PI * (1.0f / nSteps);
	float lastX=cx, lastY=cy;
	bool lastValid = false;
	float st = 0;
	float ct = 1;
	float sstep = sin(step);
	float cstep = cos(step);
	for (float t=0; t<2*H_PI + 0.5 * step; t+=step)
	{
		float sta = st * aMap;
		float ctb = ct * bMap;
		float x = cx + c * sta + s * ctb;
		float y = cy + s * sta - c * ctb;
		if (lastValid)
		{
			GLOB_ENGINE->DrawLine
			(
				Line2DPixel(lastX, lastY, x, y),
				color, color, _clipRect
			);
		}
		lastX = x;
		lastY = y;
		lastValid = true;
		float stnew = st * cstep + ct * sstep;
		float ctnew = ct * cstep - st * sstep;
		st = stnew; ct = ctnew;
	}
}

void CStaticMap::DrawRectangle(Vector3Val position, float a, float b, float angle, PackedColor color)
{
	DrawCoord posMap = WorldToScreen(position);
	float cx = posMap.x * _wScreen;
	float cy = posMap.y * _hScreen;

	const float invSizeLand = InvLandSize;
	float aMap = a * invSizeLand * _invScaleX * _wScreen;
	float bMap = b * invSizeLand * _invScaleY * _hScreen;

	// TODO: improve clipping
	float r = sqrt(Square(aMap) + Square(bMap));
	if (cx + r < _clipRect.x) return;
	if (cy + r < _clipRect.y) return;
	if (cx - r > _clipRect.x + _clipRect.w) return;
	if (cy - r > _clipRect.y + _clipRect.h) return;

	float angleRad = HDegree(angle);
	float s = sin(angleRad);
	float c = cos(angleRad);

	float x1 = cx + c * aMap + s * bMap;
	float y1 = cy + s * aMap - c * bMap;
	float x2 = cx + c * aMap + s * (-bMap);
	float y2 = cy + s * aMap - c * (-bMap);
	float x3 = cx + c * (-aMap) + s * (-bMap);
	float y3 = cy + s * (-aMap) - c * (-bMap);
	float x4 = cx + c * (-aMap) + s * bMap;
	float y4 = cy + s * (-aMap) - c * bMap;

	GLOB_ENGINE->DrawLine(Line2DPixel(x1, y1, x2, y2), color, color, _clipRect);
	GLOB_ENGINE->DrawLine(Line2DPixel(x2, y2, x3, y3), color, color, _clipRect);
	GLOB_ENGINE->DrawLine(Line2DPixel(x3, y3, x4, y4), color, color, _clipRect);
	GLOB_ENGINE->DrawLine(Line2DPixel(x4, y4, x1, y1), color, color, _clipRect);
}

inline PackedColor ModAlpha( PackedColor color, float alpha )
{
	int a=toIntFloor(alpha*color.A8());
	saturate(a,0,255);
	return PackedColorRGB(color,a);
}

const static int NoClamp2D=(DefSpecFlags2D|NoClamp)&~(ClampU|ClampV);

void CStaticMap::FillEllipse(Vector3Val position, float a, float b, float angle, PackedColor color, Texture *texture)
{
	DrawCoord posMap = WorldToScreen(position);
	float cx = posMap.x * _wScreen;
	float cy = posMap.y * _hScreen;

	const float invSizeLand = InvLandSize;
	float aMap = a * invSizeLand * _invScaleX * _wScreen;
	float bMap = b * invSizeLand * _invScaleY * _hScreen;
	
	// TODO: improve clipping
	float r = floatMax(aMap, bMap);
	if (cx + r < _clipRect.x) return;
	if (cy + r < _clipRect.y) return;
	if (cx - r > _clipRect.x + _clipRect.w) return;
	if (cy - r > _clipRect.y + _clipRect.h) return;

	float angleRad = HDegree(angle);
	float s = sin(angleRad);
	float c = cos(angleRad);

	PackedColor fillColor = color;

	float coefU = 0;
	float coefV = 0;
	float offsetX = (_mapX + _x) * _wScreen;
	float offsetY = (_mapY + _y) * _hScreen;
	if (texture)
	{
		coefU = 1.0 / texture->AWidth();
		coefV = 1.0 / texture->AHeight();
	}
	else
	{
		// special case
		fillColor = ModAlpha(color, 0.5);
	}
	MipInfo mip = GEngine->TextBank()->UseMipmap(texture, 0, 0);

	const int n = 3;
	Vertex2DPixel vs[n];
	// 0
	vs[0].x = cx;
	vs[0].y = cy;
	vs[0].u = coefU * (vs[0].x - offsetX);
	vs[0].v = coefV * (vs[0].y - offsetY);
	vs[0].color = fillColor;
	// 1
	vs[1].color = fillColor;
	// 2
	vs[2].color = fillColor;

	static const float linePts = 5.0f;
	int nSteps = toIntCeil(2 * H_PI * aMap * (1.0f / linePts));
	saturate(nSteps, 6, 720);
	float step = 2 * H_PI * (1.0f / nSteps);
	float lastX = cx, lastY = cy;
	bool lastValid = false;
	float st = 0;
	float ct = 1;
	float sstep = sin(step);
	float cstep = cos(step);
	for (float t=0; t<2*H_PI + 0.5 * step; t+=step)
	{
		float sta = st * aMap;
		float ctb = ct * bMap;
		float x = cx + c * sta + s * ctb;
		float y = cy + s * sta - c * ctb;
		if (lastValid)
		{
			// 1
			vs[1].x = lastX;
			vs[1].y = lastY;
			vs[1].u = coefU * (vs[1].x - offsetX);
			vs[1].v = coefV * (vs[1].y - offsetY);
			// 2
			vs[2].x = x;
			vs[2].y = y;
			vs[2].u = coefU * (vs[2].x - offsetX);
			vs[2].v = coefV * (vs[2].y - offsetY);
			GEngine->DrawPoly(mip, vs, n, _clipRect,NoClamp2D);
		}
		lastX = x;
		lastY = y;
		lastValid = true;
		float stnew = st * cstep + ct * sstep;
		float ctnew = ct * cstep - st * sstep;
		st = stnew; ct = ctnew;
	}
}

void CStaticMap::FillRectangle(Vector3Val position, float a, float b, float angle, PackedColor color, Texture *texture)
{
	DrawCoord posMap = WorldToScreen(position);
	float cx = posMap.x * _wScreen;
	float cy = posMap.y * _hScreen;

	const float invSizeLand = InvLandSize;
	float aMap = a * invSizeLand * _invScaleX * _wScreen;
	float bMap = b * invSizeLand * _invScaleY * _hScreen;

	// TODO: improve clipping
	float r = sqrt(Square(aMap) + Square(bMap));
	if (cx + r < _clipRect.x) return;
	if (cy + r < _clipRect.y) return;
	if (cx - r > _clipRect.x + _clipRect.w) return;
	if (cy - r > _clipRect.y + _clipRect.h) return;

	float angleRad = HDegree(angle);
	float s = sin(angleRad);
	float c = cos(angleRad);

	PackedColor fillColor = color;

	float coefU = 0;
	float coefV = 0;
	float offsetX = (_mapX + _x) * _wScreen;
	float offsetY = (_mapY + _y) * _hScreen;
	if (texture)
	{
		coefU = 1.0 / texture->AWidth();
		coefV = 1.0 / texture->AHeight();
	}
	else
	{
		// special case
		fillColor = ModAlpha(color, 0.5);
	}

	const int n = 4;
	Vertex2DPixel vs[n];
	// 0
	vs[0].x = cx + c * aMap + s * bMap;
	vs[0].y = cy + s * aMap - c * bMap;
	vs[0].u = coefU * (vs[0].x - offsetX);
	vs[0].v = coefV * (vs[0].y - offsetY);
	vs[0].color = fillColor;
	// 1
	vs[1].x = cx + c * aMap + s * (-bMap);
	vs[1].y = cy + s * aMap - c * (-bMap);
	vs[1].u = coefU * (vs[1].x - offsetX);
	vs[1].v = coefV * (vs[1].y - offsetY);
	vs[1].color = fillColor;
	// 2
	vs[2].x = cx + c * (-aMap) + s * (-bMap);
	vs[2].y = cy + s * (-aMap) - c * (-bMap);
	vs[2].u = coefU * (vs[2].x - offsetX);
	vs[2].v = coefV * (vs[2].y - offsetY);
	vs[2].color = fillColor;
	// 3
	vs[3].x = cx + c * (-aMap) + s * bMap;
	vs[3].y = cy + s * (-aMap) - c * bMap;
	vs[3].u = coefU * (vs[3].x - offsetX);
	vs[3].v = coefV * (vs[3].y - offsetY);
	vs[3].color = fillColor;

	MipInfo mip = GEngine->TextBank()->UseMipmap(texture, 0, 0);
	GEngine->DrawPoly(mip, vs, n, _clipRect, NoClamp2D);
}

///////////////////////////////////////////////////////////////////////////////
// Arcade Map control

#if _ENABLE_EDITOR

CStaticMapArcadeViewer:: CStaticMapArcadeViewer
(
	ControlsContainer *parent, int idc, const ParamEntry &cls,
	float scaleMin, float scaleMax, float scaleDefault
)
: CStaticMap(parent, idc, cls, scaleMin, scaleMax, scaleDefault)
{
	CreateObjectList();
	_sizeEmptyMarker = 32;
}

AIUnit *CStaticMapArcadeViewer::GetMyUnit()
{
	return NULL;
}

AICenter *CStaticMapArcadeViewer::GetMyCenter()
{
	return NULL;
}

void CStaticMapArcadeViewer::CreateObjectList()
{
	_objects.Clear();

	const VehicleType *type = GWorld->Preloaded(VTypeStatic);
	int i, n = GWorld->NBuildings();
	for (i=0; i<n; i++)
	{
		Vehicle *veh = GWorld->GetBuilding(i);
		VehicleWithAI *vehai = dyn_cast<VehicleWithAI>(veh);
		if (!vehai) continue;
		if (!vehai->GetType()->IsKindOf(type)) continue;

		_objects.Add(vehai);
	}
}

VehicleWithAI *CStaticMapArcadeViewer::FindObject(int id)
{
	int n = _objects.Size();
	for (int i=0; i<n; i++)
	{
		VehicleWithAI *veh = _objects[i];
		if (!veh) continue;
		if (veh->ID() == id) return veh;
	}
	return NULL;
}

// drawing

void CStaticMapArcadeViewer::DrawObjects()
{
}

TypeIsSimple(DrawCoord)

void CStaticMapArcadeViewer::DrawExt(float alpha)
{
	CStaticMap::DrawExt(alpha);

#if _ENABLE_CHEATS
	if (!_show) return;
#endif

	DrawObjects();

	// update waypoint positions
	int i, n = _template->groups.Size();
	for (i=0; i<n; i++)
	{
		ArcadeGroupInfo &gInfo = _template->groups[i];
		int j, m = gInfo.waypoints.Size();
		for (j=0; j<m; j++)
		{
			ArcadeWaypointInfo &wpInfo = gInfo.waypoints[j];
			if (wpInfo.id >= 0)
			{
				int idGroup, idUnit;
				ArcadeUnitInfo *uInfo = _template->FindUnit(wpInfo.id, idGroup, idUnit);
				if (uInfo)
				{
					wpInfo.position = uInfo->position;
				}
			}
		}
	}

	// draw synchronizations
	AutoArray< AutoArray<DrawCoord> > synchroMap;
	AutoArray< AutoArray<bool> > synchroSel;
	synchroMap.Resize(_template->nextSyncId);
	synchroSel.Resize(_template->nextSyncId);
	for (i=0; i<n; i++)
	{
		ArcadeGroupInfo &gInfo = _template->groups[i];
		int j, m = gInfo.waypoints.Size();
		for (j=0; j<m; j++)
		{
			ArcadeWaypointInfo &wpInfo = gInfo.waypoints[j];
			bool isActive1 = wpInfo.selected;
			DrawCoord ptMap = WorldToScreen(wpInfo.position);
			ptMap.x *= _wScreen;
			ptMap.y *= _hScreen;
			int p = wpInfo.synchronizations.Size();
			for (int l=0; l<p; l++)
			{
				int sync = wpInfo.synchronizations[l];
				Assert(sync >= 0);
				int o = synchroMap[sync].Size();
				for (int k=0; k<o; k++)
				{
					bool isActive2 = synchroSel[sync][k];

					PackedColor color = isActive1 || isActive2 ? _colorSync : InactiveColor(_colorSync);
					GLOB_ENGINE->DrawLine
					(
						Line2DPixel(ptMap.x, ptMap.y,
						synchroMap[sync][k].x, synchroMap[sync][k].y),
						color, color, _clipRect
					);
				}
				synchroMap[sync].Add(ptMap);
				synchroSel[sync].Add(isActive1);
			}
		}
	}
	for (i=0; i<n; i++)
	{
		ArcadeGroupInfo &gInfo = _template->groups[i];
		int j, m = gInfo.sensors.Size();
		for (j=0; j<m; j++)
		{
			ArcadeSensorInfo &sInfo = gInfo.sensors[j];
			bool isActive1 = sInfo.selected;
			DrawCoord ptMap = WorldToScreen(sInfo.position);
			ptMap.x *= _wScreen;
			ptMap.y *= _hScreen;
			int p = sInfo.synchronizations.Size();
			for (int l=0; l<p; l++)
			{
				int sync = sInfo.synchronizations[l];
				Assert(sync >= 0);
				int o = synchroMap[sync].Size();
				for (int k=0; k<o; k++)
				{
					bool isActive2 = synchroSel[sync][k];
					PackedColor color = isActive1 || isActive2 ? _colorSync : InactiveColor(_colorSync);
					GLOB_ENGINE->DrawLine
					(
						Line2DPixel(ptMap.x, ptMap.y,
						synchroMap[sync][k].x, synchroMap[sync][k].y),
						color, color, _clipRect
					);
				}
			}
		}
	}
	int j, m = _template->sensors.Size();
	for (j=0; j<m; j++)
	{
		ArcadeSensorInfo &sInfo = _template->sensors[j];
		bool isActive1 = sInfo.selected;
		DrawCoord ptMap = WorldToScreen(sInfo.position);
		ptMap.x *= _wScreen;
		ptMap.y *= _hScreen;
		int p = sInfo.synchronizations.Size();
		for (int l=0; l<p; l++)
		{
			int sync = sInfo.synchronizations[l];
			Assert(sync >= 0);
			int k, o = synchroMap[sync].Size();
			for (k=0; k<o; k++)
			{
				bool isActive2 = synchroSel[sync][k];
				PackedColor color = isActive1 || isActive2 ? _colorSync : InactiveColor(_colorSync);
				GLOB_ENGINE->DrawLine
				(
					Line2DPixel(ptMap.x, ptMap.y,
					synchroMap[sync][k].x, synchroMap[sync][k].y),
					color, color, _clipRect
				);
			}
		}
	}

	// draw units / waypoints		
	for (i=0; i<n; i++)
	{
		ArcadeGroupInfo &gInfo = _template->groups[i];
		bool activeGroup = false;
		for (int j=0; j<gInfo.units.Size(); j++)
			if (gInfo.units[j].selected)
			{
				activeGroup = true; break;
			}
		if (!activeGroup) for (int j=0; j<gInfo.waypoints.Size(); j++)
			if (gInfo.waypoints[j].selected)
			{
				activeGroup = true; break;
			}

		DrawCoord ptLeader(0, 0);
		bool leaderVisible = false;
		int j, m = gInfo.units.Size();
		for (j=0; j<m; j++)
		{
			ArcadeUnitInfo &uInfo = gInfo.units[j];
			if (uInfo.leader)
			{
				ptLeader = WorldToScreen(uInfo.position);
				ptLeader.x *= _wScreen;
				ptLeader.y *= _hScreen;
				leaderVisible = HasFullRights() || uInfo.side == Glob.header.playerSide || uInfo.age != AAUnknown;
				break;
			}
		}
		 
		// Draw waypoints
		if (leaderVisible)
		{
			DrawCoord pt1 = ptLeader;
			m = gInfo.waypoints.Size();
			for (j=0; j<m; j++)
			{
				ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
				// draw line
				PackedColor color = activeGroup ? _colorActiveMission : InactiveColor(_colorActiveMission);
				DrawCoord pt2 = WorldToScreen(wInfo.position);
				pt2.x *= _wScreen;
				pt2.y *= _hScreen;
				GLOB_ENGINE->DrawLine
				(
					Line2DPixel(pt1.x, pt1.y, pt2.x, pt2.y),
					color, color, _clipRect
				);
				// arrow
				float dx = pt2.x - pt1.x;
				float dy = pt2.y - pt1.y;
				float size2 = Square(dx) + Square(dy);
				float invSize = InvSqrt(size2);
				dx *= 12.0 * invSize; dy *= 12.0 * invSize;
				float x = pt2.x - dx + 0.5 * dy;
				float y = pt2.y - dy - 0.5 * dx;
				GEngine->DrawLine
				(
					Line2DPixel(pt2.x, pt2.y, x, y),
					color, color, _clipRect
				);
				x = pt2.x - dx - 0.5 * dy;
				y = pt2.y - dy + 0.5 * dx;
				GEngine->DrawLine
				(
					Line2DPixel(pt2.x, pt2.y, x, y),
					color, color, _clipRect
				);
				pt1 = pt2;
			}
			for (j=0; j<m; j++)
			{
				ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
				PackedColor color = wInfo.selected ?
					_infoWaypoint.color : InactiveColor(_infoWaypoint.color);
				DrawCoord pt = WorldToScreen(wInfo.position);
				DrawSign
				(
					_infoWaypoint.icon, color, pt,
					_infoWaypoint.size, _infoWaypoint.size, 0
				);
				if (wInfo.placement > 0)
					DrawEllipse(wInfo.position, wInfo.placement, wInfo.placement, 0, color);
				if (wInfo.HasEffect())
				{
					PackedColor color = activeGroup ?
						_colorCamera : InactiveColor(_colorCamera);
					pt.x += 0.02;
					DrawSign(_iconCamera, color, pt, 12, 12, 0);
				}
			}
		}

		// Draw sensors
		m = gInfo.sensors.Size();
		if (leaderVisible) for (j=0; j<m; j++)
		{
			ArcadeSensorInfo &sInfo = gInfo.sensors[j];
			Assert(sInfo.idStatic == -1);
			Assert(sInfo.idVehicle == -1);
			
			PackedColor color;
			if (sInfo.selected)
			{
				color = _colorActiveGroup;
			}
			else
			{
				color = _colorGroups;
			}

			// draw line
			DrawCoord pt = WorldToScreen(sInfo.position);
			pt.x *= _wScreen;
			pt.y *= _hScreen;
			GLOB_ENGINE->DrawLine
			(
				Line2DPixel(ptLeader.x, ptLeader.y, pt.x, pt.y),
				color, color, _clipRect
			);
		}

		for (j=0; j<m; j++)
		{
			ArcadeSensorInfo &sInfo = gInfo.sensors[j];

			PackedColor color = _colorSensor;
			if (!sInfo.selected)
			{
				color.SetA8(color.A8() / 2);
			}
			
			if (sInfo.rectangular)
				DrawRectangle(sInfo.position, sInfo.a, sInfo.b, sInfo.angle, color);
			else
				DrawEllipse(sInfo.position, sInfo.a, sInfo.b, sInfo.angle, color);
			DrawSign
			(
				_iconSensor, color,
				sInfo.position,
				16, 16, 0
			);
		}

		// draw groups
		m = gInfo.units.Size();
		if (leaderVisible) for (j=0; j<m; j++)
		{
			ArcadeUnitInfo &uInfo = gInfo.units[j];
			if (uInfo.leader) continue;
			PackedColor color = activeGroup ? _colorActiveGroup : _colorGroups;
			DrawCoord pt = WorldToScreen(uInfo.position);
			pt.x *= _wScreen;
			pt.y *= _hScreen;
			GLOB_ENGINE->DrawLine
			(
				Line2DPixel(ptLeader.x, ptLeader.y, pt.x, pt.y),
				color, color, _clipRect
			);
		}

		// draw units
		for (j=0; j<m; j++)
		{
			ArcadeUnitInfo &uInfo = gInfo.units[j];
			if
			(
				uInfo.side != Glob.header.playerSide &&
				uInfo.age == AAUnknown &&
				!HasFullRights()
			) continue;

			DrawCoord pt = WorldToScreen(uInfo.position);
			DrawCoord pt1(pt.x * _wScreen, pt.y * _hScreen);

			bool activeUnit = uInfo.selected;

			int o = uInfo.markers.Size();
			for (int k=0; k<o; k++)
			{
				ArcadeMarkerInfo *mInfo = _template->FindMarker(uInfo.markers[k]);
				if (mInfo)
				{
					DrawCoord pt2 = WorldToScreen(mInfo->position);
					pt2.x *= _wScreen;
					pt2.y *= _hScreen;
					PackedColor color = activeUnit ? _colorActiveGroup : _colorGroups;
					GLOB_ENGINE->DrawLine
					(
						Line2DPixel(pt1.x, pt1.y, pt2.x, pt2.y),
						color, color, _clipRect
					);
				}
			}

			PackedColor color;
			if (uInfo.side == TEmpty)
				color = _colorUnknown;
			else if (uInfo.side == TCivilian || uInfo.side == TLogic)
				color = _colorCivilian;
			else
			{
				float friends = _template->intel.friends[Glob.header.playerSide][uInfo.side];
				if (friends >= 0.5)
					color = _colorFriendly;
				else
					color = _colorEnemy;
			}
			if (!activeUnit)
			{
				color.SetA8(color.A8() / 2);
			}

			const float invSizeLand = InvLandSize;
			float size = uInfo.size * invSizeLand * _invScaleX * 640;
			saturateMax(size, 16);

			if (uInfo.icon)
				DrawSign
				(
					uInfo.icon, color,
					pt,
					size, size, HDegree(uInfo.azimut)
				);
			if (uInfo.placement > 0)
				DrawEllipse(uInfo.position, uInfo.placement, uInfo.placement, 0, color);
			switch (uInfo.player)
			{
			case APNonplayable:
				break;
			case APPlayerCommander:
			case APPlayerDriver:
			case APPlayerGunner:
				DrawSign
				(
					_iconPlayer, _colorMe,
					pt,
					16, 16, 0
				);
				break;
			case APPlayableC:
			case APPlayableD:
			case APPlayableG:
			case APPlayableCD:
			case APPlayableCG:
			case APPlayableDG:
			case APPlayableCDG:
				DrawSign
				(
					_iconPlayer, _colorPlayable,
					pt,
					16, 16, 0
				);
				break;
			}
		}
	}

	// draw empty vehicles
	n = _template->emptyVehicles.Size();
	for (i=0; i<n; i++)
	{
		ArcadeUnitInfo &uInfo = _template->emptyVehicles[i];
		if
		(
			uInfo.age == AAUnknown &&
			!HasFullRights()
		) continue;

		Assert(uInfo.side == TEmpty);

		bool activeUnit = uInfo.selected;

		DrawCoord pt = WorldToScreen(uInfo.position);
		DrawCoord pt1(pt.x * _wScreen, pt.y * _hScreen);
		int o = uInfo.markers.Size();
		for (int k=0; k<o; k++)
		{
			ArcadeMarkerInfo *mInfo = _template->FindMarker(uInfo.markers[k]);
			if (mInfo)
			{
				DrawCoord pt2 = WorldToScreen(mInfo->position);
				pt2.x *= _wScreen;
				pt2.y *= _hScreen;
				PackedColor color = activeUnit ? _colorActiveGroup : _colorGroups;
				GLOB_ENGINE->DrawLine
				(
					Line2DPixel(pt1.x, pt1.y, pt2.x, pt2.y),
					color, color, _clipRect
				);
			}
		}

		PackedColor color = _colorUnknown;
		if (!activeUnit)
			color.SetA8(color.A8() / 2);

		const float invSizeLand = InvLandSize;
		float size = uInfo.size * invSizeLand * _invScaleX * 640;
		saturateMax(size, 16);

		if (uInfo.icon)
			DrawSign
			(
				uInfo.icon, color,
				pt,
				size, size, HDegree(uInfo.azimut)
			);
		if (uInfo.placement > 0)
			DrawEllipse(uInfo.position, uInfo.placement, uInfo.placement, 0, color);
	}

	// draw sensors
	n = _template->sensors.Size();
	for (i=0; i<n; i++)
	{
		ArcadeSensorInfo &sInfo = _template->sensors[i];
		bool active = sInfo.selected;

		if (sInfo.idStatic >= 0)
		{
			Object *obj = FindObject(sInfo.idStatic);
			if (obj)
			{
				PackedColor color = active ? _colorActiveGroup : _colorGroups;
				DrawCoord pt1 = WorldToScreen(sInfo.position);
				pt1.x *= _wScreen;
				pt1.y *= _hScreen;
				DrawCoord pt2 = WorldToScreen(obj->Position());
				pt2.x *= _wScreen;
				pt2.y *= _hScreen;
				GLOB_ENGINE->DrawLine
				(
					Line2DPixel(pt1.x, pt1.y, pt2.x, pt2.y),
					color, color, _clipRect
				);
			}
		}
		else if (sInfo.idVehicle >= 0)
		{
			int indexUnit, indexGroup;
			ArcadeUnitInfo *uInfo = _template->FindUnit
			(
				sInfo.idVehicle, indexUnit, indexGroup
			);
			if (uInfo)
			{
				PackedColor color = active ? _colorActiveGroup : _colorGroups;
				DrawCoord pt1 = WorldToScreen(sInfo.position);
				pt1.x *= _wScreen;
				pt1.y *= _hScreen;
				DrawCoord pt2 = WorldToScreen(uInfo->position);
				pt2.x *= _wScreen;
				pt2.y *= _hScreen;
				GLOB_ENGINE->DrawLine
				(
					Line2DPixel(pt1.x, pt1.y, pt2.x, pt2.y),
					color, color, _clipRect
				);
			}
		}

		PackedColor color = _colorSensor;
		if (!active) color.SetA8(color.A8() / 2);

		if (sInfo.rectangular)
			DrawRectangle(sInfo.position, sInfo.a, sInfo.b, sInfo.angle, color);
		else
			DrawEllipse(sInfo.position, sInfo.a, sInfo.b, sInfo.angle, color);
		DrawSign
		(
			_iconSensor, color,
			sInfo.position,
			16, 16, 0
		);
	}

	// draw markers
	if (GetMode() == IMMarkers)
	{
		n = _template->markers.Size();
		for (i=0; i<n; i++)
		{
			ArcadeMarkerInfo &mInfo = _template->markers[i];

			PackedColor color = mInfo.color;
			if (!mInfo.selected)
			{
				color.SetA8(color.A8() / 2);
			}

			switch (mInfo.markerType)
			{
			case MTIcon:
				{
					float size = mInfo.size;
					if (size == 0) size = _sizeEmptyMarker;
					if (!mInfo.icon) break;
					DrawSign
					(
						mInfo.icon, color,
						mInfo.position,
						size * mInfo.a, size * mInfo.b,
						mInfo.angle * (H_PI / 180.0),
						Localize(mInfo.text)
					);
				}
				break;
			case MTRectangle:
				FillRectangle
				(
					mInfo.position, mInfo.a, mInfo.b, mInfo.angle,
					color, mInfo.fill
				);
				break;
			case MTEllipse:
				FillEllipse
				(
					mInfo.position, mInfo.a, mInfo.b, mInfo.angle,
					color, mInfo.fill
				);
				break;
			}
		}
	}

	DrawLabel(_infoMove, _colorInfoMove);
	return;
}

void CStaticMapArcade::DrawExt(float alpha)
{
	CStaticMapArcadeViewer::DrawExt(alpha);

#if _ENABLE_CHEATS
	if (!_show) return;
#endif

	if (_dragging)
	{
		DrawCoord pt;
		if (GetMode() == IMGroups)
		{
			switch (_infoClick._type)
			{
			case signArcadeUnit:
				if (_infoClick._indexGroup >= 0)
				{
					Assert(_infoClick._indexGroup < _template->groups.Size());
					ArcadeGroupInfo &gInfo = _template->groups[_infoClick._indexGroup];
					Assert(_infoClick._index < gInfo.units.Size());
					ArcadeUnitInfo &uInfo = gInfo.units[_infoClick._index];
					pt = WorldToScreen(uInfo.position);
				}
				else
				{
					ArcadeUnitInfo &uInfo = _template->emptyVehicles[_infoClick._index];
					pt = WorldToScreen(uInfo.position);
				}
				break;
			case signArcadeSensor:
				if (_infoClick._indexGroup >= 0)
				{
					ArcadeGroupInfo &gInfo = _template->groups[_infoClick._indexGroup];
					ArcadeSensorInfo &sInfo = gInfo.sensors[_infoClick._index];
					pt = WorldToScreen(sInfo.position);
				}
				else
				{
					ArcadeSensorInfo &sInfo = _template->sensors[_infoClick._index];
					pt = WorldToScreen(sInfo.position);
				}
				break;
			case signArcadeMarker:
				{
					ArcadeMarkerInfo &mInfo = _template->markers[_infoClick._index];
					pt = WorldToScreen(mInfo.position);
				}
				break;
			case signStatic:
				{
					Object *obj = _infoClick._id;
					Assert(obj);
					if (!obj) return;
					pt = WorldToScreen(obj->Position());
				}
				break;
			default:
				return;
			}
		}
		else if (GetMode() == IMSynchronize)
		{
			switch (_infoClick._type)
			{
			case signArcadeWaypoint:
				{
					ArcadeGroupInfo &gInfo = _template->groups[_infoClick._indexGroup];
					ArcadeWaypointInfo &wInfo = gInfo.waypoints[_infoClick._index];
					pt = WorldToScreen(wInfo.position);
				}
				break;
			case signArcadeSensor:
				if (_infoClick._indexGroup >= 0)
				{
					ArcadeGroupInfo &gInfo = _template->groups[_infoClick._indexGroup];
					ArcadeSensorInfo &sInfo = gInfo.sensors[_infoClick._index];
					pt = WorldToScreen(sInfo.position);
				}
				else
				{
					ArcadeSensorInfo &sInfo = _template->sensors[_infoClick._index];
					pt = WorldToScreen(sInfo.position);
				}
				break;
			default:
				return;
			}
		}
		else return;

		DrawCoord pt2 = WorldToScreen(_special);

		GEngine->DrawLine
		(
			Line2DPixel(pt.x * _wScreen, pt.y * _hScreen,
			pt2.x * _wScreen, pt2.y * _hScreen),
			_colorDragging, _colorDragging, _clipRect
		);
	}
	else if (_selecting)
	{
		PackedColor color = PackedColor(Color(0,1,0,1));
		DrawCoord pt1 = WorldToScreen(_special);
		DrawCoord pt2 = WorldToScreen(_lastPos);
		GEngine->DrawLine
		(
			Line2DPixel(pt1.x * _wScreen, pt1.y * _hScreen,
			pt2.x * _wScreen, pt1.y * _hScreen),
			color, color, _clipRect
		);
		GEngine->DrawLine
		(
			Line2DPixel(pt2.x * _wScreen, pt1.y * _hScreen,
			pt2.x * _wScreen, pt2.y * _hScreen),
			color, color, _clipRect
		);
		GEngine->DrawLine
		(
			Line2DPixel(pt2.x * _wScreen, pt2.y * _hScreen,
			pt1.x * _wScreen, pt2.y * _hScreen),
			color, color, _clipRect
		);
		GEngine->DrawLine
		(
			Line2DPixel(pt1.x * _wScreen, pt2.y * _hScreen,
			pt1.x * _wScreen, pt1.y * _hScreen),
			color, color, _clipRect
		);
	}
}

// simulation

void CStaticMapArcadeViewer::Center()
{
	ArcadeUnitInfo *uInfo = _template->FindPlayer();
	Point3 pt = _defaultCenter;
	if (uInfo) pt = uInfo->position;
	CStaticMap::Center(pt);
}

Vector3 GClipboardPos;

void SelectLeader(ArcadeGroupInfo &gInfo);

void CStaticMapArcade::ClipboardDelete()
{
	for (int i=0; i<_template->emptyVehicles.Size();)
	{
		ArcadeUnitInfo &uInfo = _template->emptyVehicles[i];
		if (uInfo.selected)
			_template->emptyVehicles.Delete(i);
		else
			i++;
	}
	for (int i=0; i<_template->sensors.Size();)
	{
		ArcadeSensorInfo &sInfo = _template->sensors[i];
		if (sInfo.selected)
			_template->sensors.Delete(i);
		else
			i++;
	}
	for (int i=0; i<_template->markers.Size();)
	{
		ArcadeMarkerInfo &mInfo = _template->markers[i];
		if (mInfo.selected)
			_template->markers.Delete(i);
		else
			i++;
	}
	for (int i=0; i<_template->groups.Size();)
	{
		ArcadeGroupInfo &gInfo = _template->groups[i];
		for (int j=0; j<gInfo.units.Size();)
		{
			ArcadeUnitInfo &uInfo = gInfo.units[j];
			if (uInfo.selected)
				gInfo.units.Delete(j);
			else
				j++;
		}
		if (gInfo.units.Size() == 0)
		{
			_template->groups.Delete(i);
			continue;
		}
		for (int j=0; j<gInfo.sensors.Size();)
		{
			ArcadeSensorInfo &sInfo = gInfo.sensors[j];
			if (sInfo.selected)
				gInfo.sensors.Delete(j);
			else
				j++;
		}
		for (int j=0; j<gInfo.waypoints.Size();)
		{
			ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
			if (wInfo.selected)
				gInfo.waypoints.Delete(j);
			else
				j++;
		}
		SelectLeader(gInfo);
		i++;
	}
	_template->CheckSynchro();
	_template->Compact();
}

void CStaticMapArcade::ClipboardCut()
{
	ClipboardCopy();
	ClipboardDelete();
}

void CStaticMapArcade::ClipboardCopy()
{
	GClipboard.Clear();
	float mouseX = 0.5 + GInput.cursorX * 0.5;
	float mouseY = 0.5 + GInput.cursorY * 0.5;
	GClipboardPos = ScreenToWorld(DrawCoord(mouseX, mouseY));
	for (int i=0; i<_template->emptyVehicles.Size(); i++)
	{
		ArcadeUnitInfo &uInfo = _template->emptyVehicles[i];
		if (uInfo.selected)
			GClipboard.emptyVehicles.Add(uInfo);
	}
	for (int i=0; i<_template->sensors.Size(); i++)
	{
		ArcadeSensorInfo &sInfo = _template->sensors[i];
		if (sInfo.selected)
			GClipboard.sensors.Add(sInfo);
	}
	for (int i=0; i<_template->markers.Size(); i++)
	{
		ArcadeMarkerInfo &mInfo = _template->markers[i];
		if (mInfo.selected)
			GClipboard.markers.Add(mInfo);
	}
	for (int i=0; i<_template->groups.Size(); i++)
	{
		ArcadeGroupInfo &gInfo = _template->groups[i];
		bool grpSelected = false;
		for (int j=0; j<gInfo.units.Size(); j++)
		{
			ArcadeUnitInfo &uInfo = gInfo.units[j];
			if (uInfo.selected)
			{
				grpSelected = true;
				break;
			}
		}
		if (!grpSelected) continue;
		int index = GClipboard.groups.Add();
		ArcadeGroupInfo &gDest = GClipboard.groups[index];
		for (int j=0; j<gInfo.units.Size(); j++)
		{
			ArcadeUnitInfo &uInfo = gInfo.units[j];
			if (uInfo.selected)
				gDest.units.Add(uInfo);
		}
		for (int j=0; j<gInfo.sensors.Size(); j++)
		{
			ArcadeSensorInfo &sInfo = gInfo.sensors[j];
			if (sInfo.selected)
				gDest.sensors.Add(sInfo);
		}
		for (int j=0; j<gInfo.waypoints.Size(); j++)
		{
			ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
			if (wInfo.selected)
				gDest.waypoints.Add(wInfo);
		}
		SelectLeader(gDest);
	}
	GClipboard.nextVehId = _template->nextVehId;
	GClipboard.nextSyncId = _template->nextSyncId;
	GClipboard.CheckSynchro();
	GClipboard.Compact();
}

void CStaticMapArcade::ClipboardPaste()
{
	_template->ClearSelection();

	float mouseX = 0.5 + GInput.cursorX * 0.5;
	float mouseY = 0.5 + GInput.cursorY * 0.5;
	Vector3 pos = ScreenToWorld(DrawCoord(mouseX, mouseY));

	// offset inserted objects
	GClipboard.AddOffset(pos - GClipboardPos);
	GClipboardPos = pos;
	
	_template->Merge(GClipboard);
}

void CStaticMapArcade::ClipboardPasteAbsolute()
{
	_template->ClearSelection();
	_template->Merge(GClipboard);
}

InsertMode CStaticMapArcadeViewer::GetMode()
{
	return IMUnits;
}

InsertMode CStaticMapArcade::GetMode()
{
	if (_parent->IDD() == IDD_ARCADE_MAP)
	{
		DisplayArcadeMap *disp = dynamic_cast<DisplayArcadeMap *>(_parent);
		return disp->_mode;
	}
	else if (_parent->IDD() == IDD_INTEL_GETREADY)
	{
		// TODO: IMWaypoints / IMSynchronize - switching
		return IMWaypoints;
/*
		CToolBox *ctrl = dynamic_cast<CToolBox *>(_parent->GetCtrl(IDC_GETREADY_EDITMODE));
		switch (ctrl->GetCurSel())
		{
		case 0:
			return IMWaypoints;
		case 1:
			return IMSynchronize;
		default:
			Fail("Selection");
			return IMWaypoints;
		}
*/
	}
	else
		return IMUnits;
}

bool CStaticMapArcade::IsAdvanced()
{
	Assert(_parent->IDD() == IDD_ARCADE_MAP);
	if (_parent->IDD() == IDD_ARCADE_MAP)
	{
		DisplayArcadeMap *disp = dynamic_cast<DisplayArcadeMap *>(_parent);
		return disp->_advanced;
	}
	else
		return false;
}

ArcadeUnitInfo *CStaticMapArcade::GetLastUnit()
{
	Assert(_parent->IDD() == IDD_ARCADE_MAP);
	if (_parent->IDD() == IDD_ARCADE_MAP)
	{
		DisplayArcadeMap *disp = dynamic_cast<DisplayArcadeMap *>(_parent);
		return &disp->_lastUnit;
	}
	else
		return NULL;
}

bool CStaticMapArcade::HasRight(int ig)
{
	switch (_editRights)
	{
		case ERNone:
			return false;
		case ERGroupWP:
			{
				if (ig < 0) return false;
				ArcadeGroupInfo &gInfo = _template->groups[ig];
				int i, n = gInfo.units.Size();
				for (i=0; i<n; i++)
				{
					switch (gInfo.units[i].player)
					{
					case APPlayerCommander:
					case APPlayerDriver:
					case APPlayerGunner:
						return true;
					}
				}
				return false;
			}
		case ERSideWP:
			{
				if (ig < 0) return false;
				ArcadeGroupInfo &gInfo = _template->groups[ig];
				return gInfo.side == Glob.header.playerSide;
			}
		case ERFull:
			return true;;
		default:
			Fail("Edit rights");
			return false;
	}
}

bool CStaticMapArcadeViewer::OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
#ifdef _WIN32
	switch (nChar)
	{
		case VK_F1:
			{
				if (_parent->IDD() != IDD_INTEL_GETREADY) break;
				CToolBox *box = dynamic_cast<CToolBox *>(_parent->GetCtrl(IDC_GETREADY_EDITMODE));
				if (!box) break;
				box->SetCurSel(0);
				return true;
			}
		case VK_F2:
			{
				if (_parent->IDD() != IDD_INTEL_GETREADY) break;
				CToolBox *box = dynamic_cast<CToolBox *>(_parent->GetCtrl(IDC_GETREADY_EDITMODE));
				if (!box) break;
				box->SetCurSel(1);
				return true;
			}
/*
		case '/':
			if (GWorld->HasOptions())
			{
				_parent->CreateChild(new DisplayChat(_parent));
				return true;
			}
			break;
*/
	}
#endif
	return CStaticMap::OnKeyDown(nChar, nRepCnt, nFlags);
}

bool CStaticMapArcade::OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
#ifdef _WIN32
	switch (nChar)
	{
		case VK_NUMPAD5:
			{
				const float sizeLand = LandGrid * LandRange;
				const float invSizeLand = 1.0 / sizeLand;
				Vector3 curPos = GetCenter();
				ArcadeUnitInfo *uInfo = _template->FindPlayer();
				Vector3 pos = Vector3(0.5 * sizeLand, 0, 0.5 * sizeLand);
				if (uInfo) pos = uInfo->position;
				float diff = curPos.Distance(pos);
				float scale = 2.0 * diff * invSizeLand;
				if (scale > _scaleX)
				{
					float time = log(scale / _scaleX);
					ClearAnimation();
					AddAnimationPhase(time, scale, curPos);
					AddAnimationPhase(1.0, scale, pos);
					AddAnimationPhase(time, _scaleX, pos);
					CreateInterpolator();
				}
				else
				{
					float time = diff * invSizeLand * _invScaleX;
					ClearAnimation();
					AddAnimationPhase(time, _scaleX, pos);
					CreateInterpolator();
				}
			}
			return true;
		case VK_F1:
		{
			if (_parent->IDD() != IDD_ARCADE_MAP) break;

			DisplayArcadeMap *disp = dynamic_cast<DisplayArcadeMap *>(_parent);
			disp->_mode = IMUnits;
			CToolBox *box = dynamic_cast<CToolBox *>(_parent->GetCtrl(IDC_ARCMAP_MODE));
			Assert(box);
			box->SetCurSel(disp->_mode);
			return true;
		}
		case VK_F2:
		{
			if (_parent->IDD() != IDD_ARCADE_MAP) break;

			DisplayArcadeMap *disp = dynamic_cast<DisplayArcadeMap *>(_parent);
			disp->_mode = IMGroups;
			CToolBox *box = dynamic_cast<CToolBox *>(_parent->GetCtrl(IDC_ARCMAP_MODE));
			Assert(box);
			box->SetCurSel(disp->_mode);
			return true;
		}
		case VK_F3:
		{
			if (_parent->IDD() != IDD_ARCADE_MAP) break;

			DisplayArcadeMap *disp = dynamic_cast<DisplayArcadeMap *>(_parent);
			disp->_mode = IMSensors;
			CToolBox *box = dynamic_cast<CToolBox *>(_parent->GetCtrl(IDC_ARCMAP_MODE));
			Assert(box);
			box->SetCurSel(disp->_mode);
			return true;
		}
		case VK_F4:
		{
			if (_parent->IDD() != IDD_ARCADE_MAP) break;

			DisplayArcadeMap *disp = dynamic_cast<DisplayArcadeMap *>(_parent);
			disp->_mode = IMWaypoints;
			CToolBox *box = dynamic_cast<CToolBox *>(_parent->GetCtrl(IDC_ARCMAP_MODE));
			Assert(box);
			box->SetCurSel(disp->_mode);
			return true;
		}
		case VK_F5:
		{
			if (_parent->IDD() != IDD_ARCADE_MAP) break;

			DisplayArcadeMap *disp = dynamic_cast<DisplayArcadeMap *>(_parent);
			disp->_mode = IMSynchronize;
			CToolBox *box = dynamic_cast<CToolBox *>(_parent->GetCtrl(IDC_ARCMAP_MODE));
			Assert(box);
			box->SetCurSel(disp->_mode);
			return true;
		}
		case VK_F6:
		{
			if (_parent->IDD() != IDD_ARCADE_MAP) break;

			DisplayArcadeMap *disp = dynamic_cast<DisplayArcadeMap *>(_parent);
			disp->_mode = IMMarkers;
			CToolBox *box = dynamic_cast<CToolBox *>(_parent->GetCtrl(IDC_ARCMAP_MODE));
			Assert(box);
			box->SetCurSel(disp->_mode);
			return true;
		}
		case VK_DELETE:
		{
			if (GInput.keys[DIK_LSHIFT] || GInput.GetKey(DIK_RSHIFT))
			{
				if (HasFullRights())
				{
					ClipboardCut();
				}
			}
			else switch (_infoMove._type)
			{
				case signArcadeUnit:
					if (HasFullRights())
					{
						_template->UnitDelete(_infoMove._indexGroup, _infoMove._index);
						_infoClick._type = signNone;
						_infoClickCandidate._type = signNone;
						_infoMove._type = signNone;
						_dragging = false;
						_selecting = false;
					}
					break;
				case signArcadeWaypoint:
					if (HasRight(_infoMove._indexGroup))
					{
						_template->WaypointDelete(_infoMove._indexGroup, _infoMove._index);
						_infoClick._type = signNone;
						_infoClickCandidate._type = signNone;
						_infoMove._type = signNone;
						_dragging = false;
						_selecting = false;
					}
					break;
				case signArcadeSensor:
					if (HasFullRights())
					{
						_template->SensorDelete(_infoMove._indexGroup, _infoMove._index);
						_infoClick._type = signNone;
						_infoClickCandidate._type = signNone;
						_infoMove._type = signNone;
						_dragging = false;
						_selecting = false;
					}
					break;
				case signArcadeMarker:
					if (HasFullRights())
					{
						_template->MarkerDelete(_infoMove._index);
						_infoClick._type = signNone;
						_infoClickCandidate._type = signNone;
						_infoMove._type = signNone;
						_dragging = false;
						_selecting = false;
					}
					break;
			}

			if (_parent->IDD() == IDD_ARCADE_MAP)
			{
				DisplayArcadeMap *disp = dynamic_cast<DisplayArcadeMap *>(_parent);
				Assert(disp);
				disp->ShowButtons();
			}
			return true;
		}
		case VK_INSERT:
			if (GInput.keys[DIK_LCONTROL] || GInput.keys[DIK_RCONTROL])
			{
				if (HasFullRights())
				{
					ClipboardCopy();
					return true;
				}
			}
			else if (GInput.keys[DIK_LSHIFT] || GInput.GetKey(DIK_RSHIFT))
			{
				if (HasFullRights())
				{
					ClipboardPaste();
					return true;
				}
			}
			break;
		case 'X':
			if (GInput.keys[DIK_LCONTROL] || GInput.keys[DIK_RCONTROL])
			{
				if (HasFullRights())
				{
					ClipboardCut();
					return true;
				}
			}
			break;
		case 'C':
			if (GInput.keys[DIK_LCONTROL] || GInput.keys[DIK_RCONTROL])
			{
				if (HasFullRights())
				{
					ClipboardCopy();
					return true;
				}
			}
			break;
		case 'V':
			if (GInput.keys[DIK_LCONTROL] || GInput.keys[DIK_RCONTROL])
			{
				if (HasFullRights())
				{
					if (GInput.keys[DIK_LSHIFT] || GInput.GetKey(DIK_RSHIFT))
						ClipboardPasteAbsolute();
					else
						ClipboardPaste();
					return true;
				}
			}
			break;
	}
#endif
	return CStaticMapArcadeViewer::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CStaticMapArcade::ProcessCheats()
{
#if _ENABLE_CHEATS
	if (GInput.GetActionToDo(UAFire))
	{
		QOFStream out;

		float mouseX = 0.5 + GInput.cursorX * 0.5;
		float mouseY = 0.5 + GInput.cursorY * 0.5;
		Vector3 pos = ScreenToWorld(DrawCoord(mouseX, mouseY));

		char buf[1024];
		sprintf
		(
			buf, "[%.3f,%.3f,%.3f]\r\n", pos.X(), pos.Z(), pos.Y()
		);
		out.write(buf,strlen(buf));
		
		out.export_clip("clipboard.txt");
	}
#endif

	CStaticMapArcadeViewer::ProcessCheats();
}

static int CharRPos(const char *t, char c)
{
	const char *cp = strrchr(t,c);
	if (!cp) return 0;
	return cp-t;
}

static RString ShortExpress(RString ex)
{
	if (ex.GetLength()<32) return ex;
	// try to shorten it in some reasonable way first
	RString shortEx = ex.Substring(0,29);
	int cp;
	if ((cp=CharRPos(shortEx,';'))>22)
	{
		return ex.Substring(0,cp+1)+RString("...");
	}
	return ex.Substring(0,26)+RString("...");
}

static RString CatWords(RString cond, RString cond2)
{
	if (cond.GetLength()>0 && cond2.GetLength()>0)
	{
		return cond+RString(" ")+cond2;
	}
	else
	{
		return cond+cond2;
	}
}

/*!
\patch 1.50 Date 4/3/2002 by Jirka
- Added: Label of waypoint in mission editor contains name of group leader
\patch 1.61 Date 5/30/2002 by Ondra
- Added: Label of waypoint, unit or trigger in mission editor
now contains extended information.
*/

void CStaticMapArcadeViewer::DrawLabel(struct SignInfo &info, PackedColor color)
{
	RString text;
	// additional text
	AutoArray<RString> addText;

	Point3 pos;
	switch (info._type)
	{
	case signNone:
		break;
	case signArcadeUnit:
		{
			ArcadeUnitInfo *uInfo = NULL;
			if (info._indexGroup == -1)
			{
				// empty vehicle
				uInfo = &_template->emptyVehicles[info._index];
			}
			else
			{
				ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
				uInfo = &gInfo.units[info._index];
			}
			text = Pars >> "CfgVehicles" >> uInfo->vehicle >> "displayName";
			if (uInfo->name && uInfo->name.GetLength() > 0)
			{
				text = text + RString(" - ");
				text = text + uInfo->name;
			}
			pos = uInfo->position;

			RString cond;
			if (uInfo->presence<0.9999f)
			{
				cond = Format("%d %%",toInt(uInfo->presence*100));
			}
			if (strcmp(uInfo->presenceCondition,"true"))
			{
				cond = CatWords(cond,RString("? ")+ShortExpress(uInfo->presenceCondition));
			}
			if (cond.GetLength()>0)
			{
				addText.Add(cond);
			}
			if (uInfo->init.GetLength()>0)
			{
				addText.Add(ShortExpress(uInfo->init));
			}

			break;
		}
	case signArcadeWaypoint:
		{
			Assert(info._indexGroup >= 0);
			Assert(info._indexGroup < _template->groups.Size());
			ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
			Assert(info._index < gInfo.waypoints.Size());
			ArcadeWaypointInfo &wInfo = gInfo.waypoints[info._index];
			text = LocalizeString(IDS_AC_MOVE + wInfo.type - ACMOVE);
			
			ArcadeUnitInfo *leader = NULL;
			for (int i=0; i<gInfo.units.Size(); i++)
			{
				ArcadeUnitInfo &uInfo = gInfo.units[i];
				if (uInfo.leader)
				{
					leader = &uInfo;
					break;
				}
			}
			if (leader && leader->name.GetLength() > 0)
			{
				text = text + RString(" (") + leader->name + RString(")");
			}

			pos = wInfo.position;
			//if (wInfo.type!=ACMOVE) addText.Add(LocalizeString(IDS_AC_MOVE + wInfo.type - ACMOVE));
			if (wInfo.combatMode>=0) addText.Add(LocalizeString(IDS_IGNORE + wInfo.combatMode));
			if (wInfo.combat>CMUnchanged) addText.Add(LocalizeString(IDS_COMBAT_UNCHANGED+wInfo.combat));
			if (wInfo.formation>=0) addText.Add(LocalizeString(IDS_COLUMN+wInfo.formation));
			if (wInfo.speed>SpeedUnchanged) addText.Add(LocalizeString(IDS_SPEED_UNCHANGED+wInfo.speed));
			if (wInfo.expActiv.GetLength()>0)
			{
				addText.Add(ShortExpress(wInfo.expActiv));
			}
			break;
		}
	case signArcadeSensor:
		{
			ArcadeSensorInfo *sInfo = NULL;
			if (info._indexGroup < 0)
			{
				sInfo = &_template->sensors[info._index];
			}
			else
			{
				ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
				sInfo = &gInfo.sensors[info._index];
			}
			if (sInfo->text && sInfo->text.GetLength() > 0)
			{
				text = sInfo->text;
			}
			else
			{
				text = LocalizeString(IDS_SENSOR);
			}
			if (strcmp(sInfo->expCond,"this"))
			{
				addText.Add(RString("? ")+ShortExpress(sInfo->expCond));
			}
			else if (sInfo->activationBy>ASANone && sInfo->activationBy<ASAStatic)
			{
				const static int act[]=
				{
					IDS_SENSORACTIV_NONE,
					IDS_EAST,
					IDS_WEST,
					IDS_GUERRILA,
					IDS_CIVILIAN,
					IDS_LOGIC,
					IDS_SENSORACTIV_ANYBODY,
					IDS_SENSORACTIV_ALPHA,
					IDS_SENSORACTIV_BRAVO,
					IDS_SENSORACTIV_CHARLIE,
					IDS_SENSORACTIV_DELTA,
					IDS_SENSORACTIV_ECHO,
					IDS_SENSORACTIV_FOXTROT,
					IDS_SENSORACTIV_GOLF,
					IDS_SENSORACTIV_HOTEL,
					IDS_SENSORACTIV_INDIA,
					IDS_SENSORACTIV_JULIET,
				};
				RString at = LocalizeString(act[sInfo->activationBy]);
				/*
				activationType does not have IDS 
				if (sInfo->activationType!=ASATPresent)
				{
					at = at + RString(" ") + LocalizeString(IDS_xxx+sInfo->activationType);
				}
				*/
				addText.Add(at);
			}
			if (sInfo->type>ASTNone)
			{
				addText.Add(LocalizeString(IDS_SENSORTYPE_NONE+sInfo->type));
			}

			if (sInfo->expActiv.GetLength()>0)
			{
				addText.Add(ShortExpress(sInfo->expActiv));
			}
			pos = sInfo->position;
			break;
		}
	case signArcadeMarker:
		{
			ArcadeMarkerInfo &mInfo = _template->markers[info._index];
			text = mInfo.name;
			pos = mInfo.position;
			break;
		}
	case signStatic:
		{
			Object *obj = info._id;
			VehicleWithAI *veh = dyn_cast<VehicleWithAI>(obj);
			if (veh)
			{
				text = veh->GetType()->GetDisplayName();
				pos = veh->Position();
			}
		}
		break;
	}

	if (text.GetLength()>0)
	{
		DrawCoord posMap = WorldToScreen(pos);

		// place label
		float w = GEngine->GetTextWidth(_sizeLabel, _fontLabel, text);
		float h = _sizeLabel;
		float ha = 0;
		float wa = 0;
		float offset = 0.01;

		const int maxAddTexts = 6;
		if (addText.Size()>maxAddTexts)
		{
			addText.Resize(maxAddTexts);
			addText[maxAddTexts-1] = "...";
		}
		for (int i=0; i<addText.Size(); i++)
		{
			float wt = GEngine->GetTextWidth(_sizeLabel, _fontLabel, addText[i]);
			saturateMax(wa,wt);
			ha += _sizeLabel;
		}


		posMap.x -= 0.5 * w;
		if (posMap.y<0.7)
		{
			posMap.y += offset;
		}
		else
		{
			posMap.y -= offset+h+ha;
		}

		Texture *texture = GLOB_SCENE->Preloaded(TextureWhite);
		MipInfo mip = GEngine->TextBank()->UseMipmap(texture, 0, 0);
		GEngine->Draw2D
		(
			mip, _colorLabelBackground,
			Rect2DPixel(posMap.x * _wScreen, posMap.y * _hScreen,
			w * _wScreen, h * _hScreen),
			_clipRect
		);
		if (ha>0) GEngine->Draw2D
		(
			mip, PackedColorRGB(_colorLabelBackground,_colorLabelBackground.A8()*3/4),
			Rect2DPixel(posMap.x * _wScreen, (posMap.y+h) * _hScreen,
			wa * _wScreen, ha * _hScreen),
			_clipRect
		);

		GEngine->DrawText
		(
			posMap, _sizeLabel,
			Rect2DFloat(_x, _y, _w, _h),
			_fontLabel, color, text
		);
		for (int i=0; i<addText.Size(); i++)
		{
			GLOB_ENGINE->DrawText
			(
				Point2DFloat(posMap.x, posMap.y + h + _sizeLabel*i),
				_sizeLabel,
				Rect2DFloat(_x, _y, _w, _h),
				_fontLabel, color, addText[i]
			);
		}
	}
}

SignInfo CStaticMapArcadeViewer::FindSign(float x, float y)
{
	bool reverse = GInput.keys[DIK_LSHIFT] > 0 || GInput.GetKey(DIK_RSHIFT) > 0;

	struct SignInfo info;
	info._type = signNone;
	info._unit = NULL;
	info._id = NULL;

	Point3 pt = ScreenToWorld(DrawCoord(x, y));
	const float sizeLand = LandGrid * LandRange;
	float dist, minDist = 0.02 * sizeLand * _scaleX;
	minDist *= minDist;

	int i, n = _template->groups.Size();
	if (!reverse) // search in waypoints first
	{
		for (i=0; i<n; i++)
		{
			ArcadeGroupInfo &gInfo = _template->groups[i];
			int j, m;
			m = gInfo.waypoints.Size();
			for (j=0; j<m; j++)
			{
				ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
				dist = (pt - wInfo.position).SquareSizeXZ();
				if (dist < minDist)
				{
					minDist = dist;
					info._type = signArcadeWaypoint;
					info._indexGroup = i;
					info._index = j;
				}
			}
		}
	}

	// units, flags and sensors in groups
	for (i=0; i<n; i++)
	{
		ArcadeGroupInfo &gInfo = _template->groups[i];
		int j, m;
		m = gInfo.units.Size();
		for (j=0; j<m; j++)
		{
			ArcadeUnitInfo &uInfo = gInfo.units[j];
			if
			(
				uInfo.side != Glob.header.playerSide &&
				uInfo.age == AAUnknown &&
				!HasFullRights()
			) continue;

			dist = (pt - uInfo.position).SquareSizeXZ();
			if (dist < minDist)
			{
				minDist = dist;
				info._type = signArcadeUnit;
				info._indexGroup = i;
				info._index = j;
			}
		}

		m = gInfo.sensors.Size();
		for (j=0; j<m; j++)
		{
			ArcadeSensorInfo &sInfo = gInfo.sensors[j];
			dist = (pt - sInfo.position).SquareSizeXZ();
			if (dist < minDist)
			{
				minDist = dist;
				info._type = signArcadeSensor;
				info._indexGroup = i;
				info._index = j;
			}
		}
	}

	// empty units
	n = _template->emptyVehicles.Size();
	for (i=0; i<n; i++)
	{
		ArcadeUnitInfo &uInfo = _template->emptyVehicles[i];
		if
		(
			uInfo.age == AAUnknown &&
			!HasFullRights()
		) continue;

		dist = (pt - uInfo.position).SquareSizeXZ();
		if (dist < minDist)
		{
			minDist = dist;
			info._type = signArcadeUnit;
			info._indexGroup = -1;
			info._index = i;
		}
	}
	
	// sensors
	n = _template->sensors.Size();
	for (i=0; i<n; i++)
	{
		ArcadeSensorInfo &sInfo = _template->sensors[i];
		dist = (pt - sInfo.position).SquareSizeXZ();
		if (dist < minDist)
		{
			minDist = dist;
			info._type = signArcadeSensor;
			info._indexGroup = -1;
			info._index = i;
		}
	}

	// markers
	n = _template->markers.Size();
	for (i=0; i<n; i++)
	{
		ArcadeMarkerInfo &mInfo = _template->markers[i];
		dist = (pt - mInfo.position).SquareSizeXZ();
		if (dist < minDist)
		{
			minDist = dist;
			info._type = signArcadeMarker;
			info._index = i;
		}
	}

	// buildings
	n = _objects.Size();
	for (i=0; i<n; i++)
	{
		VehicleWithAI *veh = _objects[i];
		if (!veh) continue;
		dist = (pt - veh->Position()).SquareSizeXZ();
		if (dist < minDist)
		{
			minDist = dist;
			info._type = signStatic;
			info._id = veh;
		}
	}

	if (reverse) // search in waypoints last
	{
		n = _template->groups.Size();
		for (i=0; i<n; i++)
		{
			ArcadeGroupInfo &gInfo = _template->groups[i];
			int j, m;
			m = gInfo.waypoints.Size();
			for (j=0; j<m; j++)
			{
				ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
				dist = (pt - wInfo.position).SquareSizeXZ();
				if (dist < minDist)
				{
					minDist = dist;
					info._type = signArcadeWaypoint;
					info._indexGroup = i;
					info._index = j;
				}
			}
		}
	}

	return info;
}

bool CStaticMapArcadeViewer::IsSelected(const SignInfo &info) const
{
	switch (info._type)
	{
	case signArcadeUnit:
		{
			ArcadeUnitInfo *uInfo = NULL;
			if (info._indexGroup == -1)
			{
				uInfo = &_template->emptyVehicles[info._index];
			}
			else
			{
				ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
				uInfo = &gInfo.units[info._index];
			}
			Assert(uInfo);
			return uInfo->selected;
		}
	case signArcadeSensor:
		{
			ArcadeSensorInfo *sInfo = NULL;
			if (info._indexGroup == -1)
			{
				sInfo = &_template->sensors[info._index];
			}
			else
			{
				ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
				sInfo = &gInfo.sensors[info._index];
			}
			Assert(sInfo);
			return sInfo->selected;
		}
	case signArcadeMarker:
		{
			ArcadeMarkerInfo &mInfo = _template->markers[info._index];
			return mInfo.selected;
		}
	case signArcadeWaypoint:
		{
			ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
			ArcadeWaypointInfo &wInfo = gInfo.waypoints[info._index];
			return wInfo.selected;
		}
	default:
		return false;
	}
}

void CStaticMapArcadeViewer::InvertSelection(const SignInfo &info)
{
	switch (info._type)
	{
	case signArcadeUnit:
		{
			ArcadeUnitInfo *uInfo = NULL;
			if (info._indexGroup == -1)
			{
				uInfo = &_template->emptyVehicles[info._index];
			}
			else
			{
				ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
				uInfo = &gInfo.units[info._index];
			}
			Assert(uInfo);
			uInfo->selected = !uInfo->selected;
			break;
		}
	case signArcadeSensor:
		{
			ArcadeSensorInfo *sInfo = NULL;
			if (info._indexGroup == -1)
			{
				sInfo = &_template->sensors[info._index];
			}
			else
			{
				ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
				sInfo = &gInfo.sensors[info._index];
			}
			Assert(sInfo);
			sInfo->selected = !sInfo->selected;
			break;
		}
	case signArcadeMarker:
		{
			ArcadeMarkerInfo &mInfo = _template->markers[info._index];
			mInfo.selected = !mInfo.selected;
			break;
		}
	case signArcadeWaypoint:
		{
			ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
			ArcadeWaypointInfo &wInfo = gInfo.waypoints[info._index];
			wInfo.selected = !wInfo.selected;
			break;
		}
	}
}

void CStaticMapArcadeViewer::OnLButtonDown(float x, float y)
{
	_infoClick = FindSign(x, y);

	// set selection
	//if (!GInput.keys[DIK_LCONTROL])
	_template->ClearSelection();
	InvertSelection(_infoClick);
}

void CStaticMapArcade::OnLButtonDown(float x, float y)
{
	SignInfo info = FindSign(x, y);
	_infoClickCandidate = info;
	_lastPos = ScreenToWorld(DrawCoord(x, y));
}

void CStaticMapArcade::OnLButtonUp(float x, float y)
{
	if (_dragging)
	{
		_dragging = false;
		if (GetMode() != IMGroups && GetMode() != IMSynchronize) return;

		SignInfo info = FindSign(x, y);

		if (GetMode() == IMGroups)
		{
			switch (_infoClick._type)
			{
			case signArcadeUnit:
				if (!HasFullRights()) return;
				if (info._type == signArcadeUnit)
				{
					// try to assign unit into another group
					if (_infoClick._indexGroup >= 0 && info._indexGroup >= 0)
					{
						bool ok = _template->UnitChangeGroup
						(
							_infoClick._indexGroup, _infoClick._index,
							info._indexGroup
						);
						(void)ok;
					}
				}
				else if (info._type == signNone)
				{
					// unassign from group
					if (_infoClick._indexGroup >= 0)
					{
						bool ok = _template->UnitChangeGroup
						(
							_infoClick._indexGroup, _infoClick._index,
							-1
						);
						(void)ok;
					}
				}
				else if (info._type == signArcadeSensor)
				{
					ArcadeUnitInfo *uInfo = NULL;
					if (_infoClick._indexGroup >= 0)
					{
						ArcadeGroupInfo &gInfo = _template->groups[_infoClick._indexGroup];
						uInfo = &gInfo.units[_infoClick._index];
					}
					else
					{
						uInfo = &_template->emptyVehicles[_infoClick._index];
					}
					_template->SensorChangeVehicle
					(
						info._indexGroup, info._index,
						uInfo->id
					);
					// TODO: vehicles are not sent over network
				}
				else if (info._type == signArcadeMarker)
				{
					_template->UnitAddMarker
					(
						_infoClick._indexGroup, _infoClick._index,
						info._index
					);
					// TODO: markers are not sent over network
				}
				break;
			case signArcadeSensor:
				if (info._type == signArcadeUnit)
				{
					ArcadeUnitInfo *uInfo = NULL;
					if (info._indexGroup >= 0)
					{
						ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
						uInfo = &gInfo.units[info._index];
					}
					else
					{
						uInfo = &_template->emptyVehicles[info._index];
					}
					_template->SensorChangeVehicle
					(
						_infoClick._indexGroup, _infoClick._index,
						uInfo->id
					);
					// TODO: vehicles are not sent over network
				}
				else if (info._type == signStatic)
				{
					Object *obj = info._id;
					Assert(obj);
					if (!obj) return;
					_template->SensorChangeStatic
					(
						_infoClick._indexGroup, _infoClick._index,
						obj->ID()
					);
					// TODO: buildings are not sent over network
				}
				else if (info._type == signNone)
				{
					// unassign
					//ArcadeSensorInfo *sInfo = NULL;
					if (_infoClick._indexGroup >= 0)
					{
						_template->SensorChangeGroup
						(
							_infoClick._indexGroup, _infoClick._index,
							-1
						);
					}
					else
					{
						ArcadeSensorInfo &sInfo = _template->sensors[_infoClick._index];
						if (sInfo.idStatic >= 0)
						{
							_template->SensorChangeStatic
							(
								_infoClick._indexGroup, _infoClick._index,
								-1
							);
							// TODO: buildings are not sent over network
						}
						else if (sInfo.idVehicle >= 0)
						{
							_template->SensorChangeVehicle
							(
								_infoClick._indexGroup, _infoClick._index,
								-1
							);
							// TODO: vehicles are not sent over network
						}
					}
				}
				break;
			case signArcadeMarker:
				if (info._type == signArcadeUnit)
				{
					_template->UnitAddMarker
					(
						info._indexGroup, info._index,
						_infoClick._index
					);
					// TODO: markers are not sent over network
				}
				else if (info._type == signNone)
				{
					_template->RemoveMarker
					(
						_infoClick._index
					);
					// TODO: markers are not sent over network
				}
				break;
			case signStatic:
				if (info._type == signArcadeSensor)
				{
					Object *obj = _infoClick._id;
					Assert(obj);
					if (!obj) return;
					_template->SensorChangeStatic
					(
						info._indexGroup, info._index,
						obj->ID()
					);
					// TODO: buildings are not sent over network
				}
				// remove from this side is too hard - do not use it
				break;
			}
		}
		else // GetMode() == IMSynchronize
		{
			switch (_infoClick._type)
			{
			case signArcadeWaypoint:
				if (info._type == signArcadeWaypoint)
				{
					if (HasRight(_infoClick._indexGroup) || HasRight(info._indexGroup))
					{
						_template->WaypointChangeSynchro
						(
							_infoClick._indexGroup, _infoClick._index,
							info._indexGroup, info._index
						);
					}
				}
				else if (info._type == signArcadeSensor)
				{
					if (HasRight(_infoClick._indexGroup))
					{
						_template->SensorChangeSynchro
						(
							info._indexGroup, info._index,
							_infoClick._indexGroup, _infoClick._index
						);
					}
				}
				else if (info._type == signNone)
				{
					if (HasRight(_infoClick._indexGroup))
					{
						_template->WaypointChangeSynchro
						(
							_infoClick._indexGroup, _infoClick._index,
							-1, -1
						);
					}
				}
				break;
			case signArcadeSensor:
				if (info._type == signArcadeWaypoint)
				{
					if (HasRight(info._indexGroup))
					{
						_template->SensorChangeSynchro
						(
							_infoClick._indexGroup, _infoClick._index,
							info._indexGroup, info._index
						);
					}
				}
				else if (info._type == signNone)
				{
					if (HasFullRights())
					{
						_template->SensorChangeSynchro
						(
							_infoClick._indexGroup, _infoClick._index,
							-1, -1
						);
					}
				}
				break;
			}
		}

		_infoClick._type = signNone;
		_infoClickCandidate._type = signNone;
		_infoMove._type = signNone;
	}
	else if (_selecting)
	{
		_selecting = false;
		if (!GInput.keys[DIK_LCONTROL] && !GInput.keys[DIK_RCONTROL])
			_template->ClearSelection();

		float xMin = floatMin(_special.X(), _lastPos.X());
		float xMax = floatMax(_special.X(), _lastPos.X());
		float zMin = floatMin(_special.Z(), _lastPos.Z());
		float zMax = floatMax(_special.Z(), _lastPos.Z());

		for (int i=0; i<_template->emptyVehicles.Size(); i++)
		{
			ArcadeUnitInfo &info = _template->emptyVehicles[i];
			if
			(
				info.position.X() >= xMin && info.position.X() < xMax &&
				info.position.Z() >= zMin && info.position.Z() < zMax
			) info.selected = !info.selected;
		}
		for (int i=0; i<_template->sensors.Size(); i++)
		{
			ArcadeSensorInfo &info = _template->sensors[i];
			if
			(
				info.position.X() >= xMin && info.position.X() < xMax &&
				info.position.Z() >= zMin && info.position.Z() < zMax
			) info.selected = !info.selected;
		}
		for (int i=0; i<_template->markers.Size(); i++)
		{
			ArcadeMarkerInfo &info = _template->markers[i];
			if
			(
				info.position.X() >= xMin && info.position.X() < xMax &&
				info.position.Z() >= zMin && info.position.Z() < zMax
			) info.selected = !info.selected;
		}
		for (int i=0; i<_template->groups.Size(); i++)
		{
			ArcadeGroupInfo &gInfo = _template->groups[i];
			for (int j=0; j<gInfo.units.Size(); j++)
			{
				ArcadeUnitInfo &info = gInfo.units[j];
				if
				(
					info.position.X() >= xMin && info.position.X() < xMax &&
					info.position.Z() >= zMin && info.position.Z() < zMax
				) info.selected = !info.selected;
			}
			for (int j=0; j<gInfo.sensors.Size(); j++)
			{
				ArcadeSensorInfo &info = gInfo.sensors[j];
				if
				(
					info.position.X() >= xMin && info.position.X() < xMax &&
					info.position.Z() >= zMin && info.position.Z() < zMax
				) info.selected = !info.selected;
			}
			for (int j=0; j<gInfo.waypoints.Size(); j++)
			{
				ArcadeWaypointInfo &info = gInfo.waypoints[j];
				if
				(
					info.position.X() >= xMin && info.position.X() < xMax &&
					info.position.Z() >= zMin && info.position.Z() < zMax
				) info.selected = !info.selected;
			}
		}
	}
}

void CStaticMapArcade::OnLButtonClick(float x, float y)
{
	bool canSelectGroup = false;
	switch (_infoClickCandidate._type)
	{
	case signArcadeUnit:
	case signArcadeSensor:
		canSelectGroup = _infoClickCandidate._indexGroup >= 0;
	case signArcadeMarker:
		_infoClick = _infoClickCandidate;
		_dragging = GInput.mouseL && HasFullRights();
		break;		
	case signStatic:
		_infoClick = _infoClickCandidate;
		_dragging = false;
		break;		
	case signArcadeWaypoint:
		_infoClick = _infoClickCandidate;
		_dragging = GInput.mouseL && HasRight(_infoClick._indexGroup);
		canSelectGroup = true;
		{
			ArcadeGroupInfo &gInfo = _template->groups[_infoClick._indexGroup];
			ArcadeWaypointInfo &wInfo = gInfo.waypoints[_infoClick._index];
			if (wInfo.id >= 0)
			{
				int idGroup, idUnit;
				_template->FindUnit(wInfo.id, idGroup, idUnit);
				_infoClick._type = signArcadeUnit;
				_infoClick._indexGroup = idGroup;
				_infoClick._index = idUnit;
			}
			else if (wInfo.idStatic >= 0)
			{
				_dragging = false;
			}
		}
		break;
	case signNone:
		if (GInput.mouseL)
		{
			_dragging = false;
			_selecting = true;
			_special = _lastPos;
			return;
		}
		break;
	}

	bool selected = IsSelected(_infoClickCandidate);
	if (!_dragging || !selected)
	{
		// change selection
		if (!GInput.keys[DIK_LCONTROL] && !GInput.keys[DIK_RCONTROL])
		{
			_template->ClearSelection();
			selected = false;
		}
		if ((GInput.keys[DIK_LSHIFT] || GInput.GetKey(DIK_RSHIFT)) && canSelectGroup)
		{
			ArcadeGroupInfo &gInfo =
				_template->groups[_infoClickCandidate._indexGroup];
			gInfo.Select(!selected);
		}
		else
			InvertSelection(_infoClickCandidate);
	}

	CStatic::OnLButtonClick(x, y);
}

/*!
\patch 1.50 Date 4/8/2002 by Jirka
- Fixed: Mission editor - list of markers is not longer in default settings for newly added units.
*/

void CStaticMapArcade::OnLButtonDblClick(float x, float y)
{
	ClearAnimation();
	_moveKey = 0;
	_mouseKey = 0;

	SignInfo info = FindSign(x, y);

	if (info._type == signArcadeWaypoint)
	{
		if (HasRight(info._indexGroup))
		{
			// Edit waypoint
			ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
			ArcadeWaypointInfo &wInfo = gInfo.waypoints[info._index];
			_parent->CreateChild(new DisplayArcadeWaypoint
			(
				_parent,
				_template,
				info._indexGroup, info._index,
				wInfo, IsAdvanced()
			));
		}
	}
	else if
	(
		GetMode() == IMWaypoints &&
		(
			_infoClick._type == signArcadeUnit && _infoClick._indexGroup >= 0 ||
			_infoClick._type == signArcadeWaypoint
		)
	)
	{
		if (HasRight(_infoClick._indexGroup))
		{
			// Insert waypoint
			//ArcadeGroupInfo &gInfo = _template->groups[_infoClick._indexGroup];
			ArcadeWaypointInfo wInfo;
			if (info._type == signArcadeUnit)
			{
				ArcadeUnitInfo *uInfo = NULL;
				if (info._indexGroup == -1)
				{
					uInfo = &_template->emptyVehicles[info._index];
				}
				else
				{
					ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
					uInfo = &gInfo.units[info._index];
				}
				Assert(uInfo);
				wInfo.position = uInfo->position;
				wInfo.id = uInfo->id;
			}
			else if (info._type == signStatic)
			{
				Object *obj = info._id;
				Assert(obj);
				wInfo.position = obj->Position();
				wInfo.idStatic = obj->ID();
			}
			else
			{
				wInfo.position = ScreenToWorld(DrawCoord(x, y));
				wInfo.position[1] = GLOB_LAND->RoadSurfaceY(wInfo.position[0], wInfo.position[2]);
			}
			_parent->CreateChild(new DisplayArcadeWaypoint
			(
				_parent,
				_template,
				_infoClick._indexGroup, -1,
				wInfo, IsAdvanced()
			));
		}
	}
	else if (info._type == signArcadeSensor)
	{
		if (HasFullRights())
		{
			// Edit sensor
			ArcadeSensorInfo *sInfo = NULL;
			if (info._indexGroup == -1)
			{
				sInfo = &_template->sensors[info._index];
			}
			else
			{
				ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
				sInfo = &gInfo.sensors[info._index];
			}
			Assert(sInfo);
			_parent->CreateChild(new DisplayArcadeSensor
			(
				_parent,
				info._indexGroup, info._index,
				*sInfo, _template, IsAdvanced()
			));
		}
	}
	else if (info._type == signArcadeMarker)
	{
		if (HasFullRights())
		{
			// Edit marker
			ArcadeMarkerInfo &mInfo = _template->markers[info._index];
			_parent->CreateChild(new DisplayArcadeMarker
			(
				_parent,
				info._index,
				mInfo,
				_template,
				IsAdvanced()
			));
		}
	}
	else if (info._type == signArcadeUnit)
	{
		if (HasFullRights())
		{
			// Edit unit
			ArcadeUnitInfo *uInfo = NULL;
			if (info._indexGroup == -1)
			{
				uInfo = &_template->emptyVehicles[info._index];
			}
			else
			{
				ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
				uInfo = &gInfo.units[info._index];
			}
			Assert(uInfo);

			_parent->CreateChild(new DisplayArcadeUnit
			(
				_parent,
				info._indexGroup, info._index,
				*uInfo,
				_template, IsAdvanced()
			));
		}
	}
	else if (GetMode() == IMSensors)
	{
		// Insert sensor
		if (HasFullRights())
		{
			ArcadeSensorInfo sInfo;
			sInfo.position = ScreenToWorld(DrawCoord(x, y));
			sInfo.position[1] = GLOB_LAND->RoadSurfaceY(sInfo.position[0], sInfo.position[2]);
			_parent->CreateChild(new DisplayArcadeSensor
			(
				_parent,
				-1, -1,
				sInfo, _template, IsAdvanced()
			));
		}
	}
	else if (GetMode() == IMMarkers)
	{
		// Insert marker
		if (HasFullRights())
		{
			ArcadeMarkerInfo mInfo;
			mInfo.position = ScreenToWorld(DrawCoord(x, y));
			mInfo.position[1] = GLOB_LAND->RoadSurfaceY(mInfo.position[0], mInfo.position[2]);
			_parent->CreateChild(new DisplayArcadeMarker
			(
				_parent,
				-1,
				mInfo,
				_template,
				IsAdvanced()
			));
		}
	}
	else if (GetMode() == IMGroups)
	{
		// Insert group
		if (HasFullRights())
		{
			Vector3 position = ScreenToWorld(DrawCoord(x, y));
			position[1] = GLandscape->RoadSurfaceY(position[0], position[2]);
			_parent->CreateChild
			(
				new DisplayArcadeGroup(_parent, position, _lastGroupSide, _lastGroupType, _lastGroupName, 0)
			);
		}
	}
	else if (GetMode() == IMUnits)
	{
		if (HasFullRights())
		{
			// Insert unit
			ArcadeUnitInfo uInfo, *lastInfo = GetLastUnit();
			if (lastInfo) uInfo = *lastInfo;

			uInfo.position = ScreenToWorld(DrawCoord(x, y));
			uInfo.position[1] = GLOB_LAND->RoadSurfaceY(uInfo.position[0], uInfo.position[2]);

			uInfo.name = "";
			uInfo.init = "";

			uInfo.player = APNonplayable;
			if (!_template->FindPlayer())
				uInfo.player = APPlayerCommander;
			
			if (uInfo.player != APNonplayable && uInfo.side == TEmpty) uInfo.side = TWest;

			uInfo.health = 1.0;
			uInfo.fuel = 1.0;
			uInfo.ammo = 1.0;
			uInfo.presence = 1.0;
			uInfo.placement = 0;
			
			uInfo.markers.Clear();

			_parent->CreateChild
			(
				new DisplayArcadeUnit(_parent, -1, -1, uInfo, _template, IsAdvanced())
			);
		}
	}
}

void CStaticMapArcade::OnMouseHold(float x, float y, bool active)
{
	if (_dragging)
	{
		Vector3 curPos = ScreenToWorld(DrawCoord(x, y));
		switch (GetMode())
		{
		case IMGroups:
			switch (_infoClick._type)
			{
			case signArcadeUnit:
			case signArcadeSensor:
			case signArcadeMarker:
			case signStatic:
				_special = curPos;
				break;
			default:
				goto MoveSelection;
			}
			break;
		case IMSynchronize:
			switch (_infoClick._type)
			{
			case signArcadeSensor:
			case signArcadeWaypoint:
				_special = curPos;
				break;
			default:
				goto MoveSelection;
			}
			break;
		default:
		MoveSelection:
			if (GInput.keys[DIK_LSHIFT] > 0 || GInput.keys[DIK_RSHIFT] > 0)
			{
				// rotate
				Vector3 sum = VZero;
				int count = 0;
				_template->CalculateCenter(sum, count, true);
				if (count > 0)
				{
					Vector3 center = (1.0f / count) * sum;
					Vector3 oldDir = _lastPos - center;
					Vector3 dir = curPos - center;
					float angle = AngleDifference(atan2(dir.X(), dir.Z()), atan2(oldDir.X(), oldDir.Z()));
					_template->Rotate(center, angle, true);
				}
			}
			else
			{
				Vector3 offset = curPos - _lastPos;
				// move all selected items by offset
				for (int i=0; i<_template->emptyVehicles.Size(); i++)
					if (_template->emptyVehicles[i].selected)
					{
						Vector3 pos = _template->emptyVehicles[i].position + offset;
						pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);
						if (_template->UnitChangePosition(-1, i, pos))
						{
						}
					}
				for (int i=0; i<_template->sensors.Size(); i++)
					if (_template->sensors[i].selected)
					{
						Vector3 pos = _template->sensors[i].position + offset;
						pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);
						if (_template->SensorChangePosition(-1, i, pos))
						{
						}
					}
				for (int i=0; i<_template->markers.Size(); i++)
					if (_template->markers[i].selected)
					{
						Vector3 pos = _template->markers[i].position + offset;
						pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);
						if (_template->MarkerChangePosition(i, pos))
						{
						}
					}
				for (int i=0; i<_template->groups.Size(); i++)
				{
					ArcadeGroupInfo &gInfo = _template->groups[i];
					for (int j=0; j<gInfo.units.Size(); j++)
						if (gInfo.units[j].selected)
						{
							Vector3 pos = gInfo.units[j].position + offset;
							pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);
							if (_template->UnitChangePosition(i, j, pos))
							{
							}
						}
					for (int j=0; j<gInfo.sensors.Size(); j++)
						if (gInfo.sensors[j].selected)
						{
							Vector3 pos = gInfo.sensors[j].position + offset;
							pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);
							if (_template->SensorChangePosition(i, j, pos))
							{
							}
						}
					for (int j=0; j<gInfo.waypoints.Size(); j++)
						if (gInfo.waypoints[j].selected)
						{
							Vector3 pos = gInfo.waypoints[j].position + offset;
							pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);
							if (_template->WaypointChangePosition(i, j, pos))
							{
							}
						}
				}
			}
		}

		_lastPos = curPos;
	}
	else if (_selecting)
	{
		_lastPos = ScreenToWorld(DrawCoord(x, y));
	}

	CStaticMap::OnMouseHold(x, y, active);
}

// Arcade map display

RString GetMissionsDirectory();
RString GetMissionDirectory();

DisplayArcadeMap::DisplayArcadeMap(ControlsContainer *parent, bool multiplayer)
: DisplayMapEditor(parent)
{
	_enableSimulation = false;
	_enableDisplay = false;
//	GWorld->EnableDisplay(false);

	_multiplayer = multiplayer;
	_running = false;

	_alwaysShow = true;
	_mode = IMUnits;

	Load("RscDisplayArcadeMap");

	if (*Glob.header.filename)
	{
		char buffer[256];

		sprintf
		(
			buffer, "%smission.sqm",
			(const char *)GetMissionDirectory()
		);

		LoadTemplates(buffer);

		ArcadeUnitInfo *uInfo = _templateMission.FindPlayer(); 
		if (uInfo)
		{
			Glob.header.playerSide = (TargetSide)uInfo->side;
		}
	}

	if (_map)
	{
		_map->SetScale(-1); // default
		_map->Center();
		_map->Reset();
	}

	LoadParams();
	SetAdvancedMode(_advanced);
	ShowButtons();
	UpdateIdsButton();
	UpdateTexturesButton();
}

#include "saveVersion.hpp"
RString GetUserParams();
bool IsVBS();

/*!
\patch_internal 1.78 Date 7/15/2002 by Jirka
- Fixed: In VBS default is advanced editor mode
*/

void DisplayArcadeMap::LoadParams()
{
	ParamArchiveLoad ar(GetUserParams());
	bool defaultAdvanced = IsVBS();
	if (ar.Serialize("advancedEditor", _advanced, 1, defaultAdvanced) != LSOK)
	{
		WarningMessage("Cannot load user paremeters.");
	}
}

void DisplayArcadeMap::SaveParams()
{
	ParamArchiveSave ar(UserInfoVersion);
	ar.Parse(GetUserParams());
	if (ar.Serialize("advancedEditor", _advanced, 1) != LSOK)
	{
		// TODO: save failed
	}
	if (ar.Save(GetUserParams()) != LSOK)
	{
		// TODO: save failed
	}
}

LSError DisplayArcadeMap::SerializeAll(ParamArchive &ar, bool merge)
{
	ATSParams params;
	ar.SetParams(&params);
	if (merge)
	{
		ArcadeTemplate t;
		CHECK(ar.Serialize("Mission", t, 1))
		_templateMission.Merge(t);
		CHECK(ar.Serialize("Intro", t, 1))
		_templateIntro.Merge(t);
		CHECK(ar.Serialize("OutroWin", t, 1))
		_templateOutroWin.Merge(t);
		CHECK(ar.Serialize("OutroLoose", t, 1))
		_templateOutroLoose.Merge(t);
		// do not merge cutscenes
	}
	else
	{
		CHECK(ar.Serialize("Mission", _templateMission, 1))
		CHECK(ar.Serialize("Intro", _templateIntro, 1))
		CHECK(ar.Serialize("OutroWin", _templateOutroWin, 1))
		CHECK(ar.Serialize("OutroLoose", _templateOutroLoose, 1))
		if (ar.IsLoading() && ar.GetArVersion() < 7)
		{
			ArcadeIntel intel;
			CHECK(ar.Serialize("Intel", intel, 1))
			_templateMission.intel = intel;
			_templateIntro.intel = intel;
			_templateOutroWin.intel = intel;
			_templateOutroLoose.intel = intel;
		}
	}
	return LSOK;
}

bool DisplayArcadeMap::LoadTemplates(const char *filename)
{
	QIFStreamB test;
	test.AutoOpen(filename);
	if (test.get() == 0)
	{
		// binary file
		return false;
	}

	ParamArchiveLoad ar(filename);
	LSError result = SerializeAll(ar);
	if (result != LSOK)
	{
		if (result == LSNoAddOn)
		{
			RString message = LocalizeString(IDS_MSG_ADDON_MISSING);
			bool first = true;
			for (int i=0; i<_templateMission.missingAddOns.Size(); i++)
			{
				if (first) first = false;
				else message = message + RString(", ");
				message = message + _templateMission.missingAddOns[i];
			}
			for (int i=0; i<_templateIntro.missingAddOns.Size(); i++)
			{
				if (first) first = false;
				else message = message + RString(", ");
				message = message + _templateIntro.missingAddOns[i];
			}
			for (int i=0; i<_templateOutroWin.missingAddOns.Size(); i++)
			{
				if (first) first = false;
				else message = message + RString(", ");
				message = message + _templateOutroWin.missingAddOns[i];
			}
			for (int i=0; i<_templateOutroLoose.missingAddOns.Size(); i++)
			{
				if (first) first = false;
				else message = message + RString(", ");
				message = message + _templateOutroLoose.missingAddOns[i];
			}
			CreateMsgBox(MB_BUTTON_OK, message);
		}
		else
			CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_LOAD_TEMPL_FAIL));
		_templateMission.Clear();
		_templateIntro.Clear();
		_templateOutroWin.Clear();
		_templateOutroLoose.Clear();
		return false;
	}

	_currentTemplate = &_templateMission;
	CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCMAP_SECTION));
	if (combo) combo->SetCurSel(0);
	_map->SetTemplate(_currentTemplate);

	// loaded

	// warning for rad only files
	if (QIFStream::FileReadOnly(filename))
	{
		CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_TEMPL_READONLY));
	}

	return true;
}

bool DisplayArcadeMap::MergeTemplates(const char *filename)
{
	QIFStreamB test;
	test.AutoOpen(filename);
	if (test.get() == 0)
	{
		// binary file
		return false;
	}

	ParamArchiveLoad ar(filename);
	LSError result = SerializeAll(ar, true);
	if (result == LSNoAddOn)
	{
		RString message = LocalizeString(IDS_MSG_ADDON_MISSING);
		bool first = true;
		for (int i=0; i<_templateMission.missingAddOns.Size(); i++)
		{
			if (first) first = false;
			else message = message + RString(", ");
			message = message + _templateMission.missingAddOns[i];
		}
		for (int i=0; i<_templateIntro.missingAddOns.Size(); i++)
		{
			if (first) first = false;
			else message = message + RString(", ");
			message = message + _templateIntro.missingAddOns[i];
		}
		for (int i=0; i<_templateOutroWin.missingAddOns.Size(); i++)
		{
			if (first) first = false;
			else message = message + RString(", ");
			message = message + _templateOutroWin.missingAddOns[i];
		}
		for (int i=0; i<_templateOutroLoose.missingAddOns.Size(); i++)
		{
			if (first) first = false;
			else message = message + RString(", ");
			message = message + _templateOutroLoose.missingAddOns[i];
		}
		CreateMsgBox(MB_BUTTON_OK, message);
		return false;
	}
	else if (result != LSOK)
	{
		CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_LOAD_TEMPL_FAIL));
		return false;
	}

//	_currentTemplate = &_templateMission;

	return true;
}

bool DisplayArcadeMap::SaveTemplates(const char *filename)
{
	ParamArchiveSave ar(MissionsVersion);
	if (SerializeAll(ar) != LSOK) return false;
	LSError err = ar.Save(filename);
	if (err != LSOK)
	{
		if (err == LSAccessDenied)
			CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_TEMPL_ACCESSDENIED));
		else
			CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_SAVE_TEMPL_FAIL));
		return false;
	}
	return true;
}

WeatherState DisplayArcadeMap::GetWeather()
{
	return GetWeatherState(_currentTemplate->intel.weather);
}

// static const int daysInMonth[]={31,28,31,30,31,30,31,31,30,31,30,31};
int GetDaysInMonth(int year, int month);

Clock DisplayArcadeMap::GetTime()
{
	int day = _currentTemplate->intel.day - 1;
	int year = _currentTemplate->intel.year;
	for (int m=0; m<_currentTemplate->intel.month-1; m++) day += GetDaysInMonth(year, m);
	
	Clock clock;
	clock.SetTimeInYear
	(
		OneDay * day +
		OneHour * _currentTemplate->intel.hour +
		OneMinute * _currentTemplate->intel.minute + 0.5 * OneSecond
	);
	clock.SetYear(year);
	return clock;
}

bool DisplayArcadeMap::GetPosition(Point3 &pos)
{
	ArcadeUnitInfo *me = _currentTemplate->FindPlayer();
	if (me)
	{
		pos = me->position;
		return true;
	}
	else
		return false;
}

bool BreakIntro(unsigned nChar)
{
#ifdef _WIN32
	if (nChar == 0)
	{
		// called from OnSimulate
		if (GInput.mouseLToDo)
		{
			GInput.mouseLToDo = false;
			return true;
		}
		if (GInput.mouseRToDo)
		{
			GInput.mouseRToDo = false;
			return true;
		}
		return false;
	}
	else
	{
		// called from OnKeyDown
		if (nChar == VK_ESCAPE)
		{
			return true;
		}
		if (nChar == VK_SPACE)
		{
			return true;
		}
		return false;
	}
#else
    return false;
#endif
}

void DisplayArcadeMap::ShowButtons()
{
	IControl *ctrl = GetCtrl(IDC_ARCMAP_PREVIEW);
	if (ctrl)
	{
		bool show = _currentTemplate->IsConsistent(NULL, _multiplayer);
		if (show && _multiplayer) show = Glob.header.filename[0] != 0;
		ctrl->ShowCtrl(show);
	}
	ctrl = GetCtrl(IDC_ARCMAP_CONTINUE);
	if (ctrl) ctrl->ShowCtrl(!_multiplayer && (GWorld->GetMode() == GModeArcade));
}

void DisplayArcadeMap::SetAdvancedMode(bool advanced)
{
	_advanced = advanced;
	CActiveText *txt = static_cast<CActiveText *>(GetCtrl(IDC_ARCMAP_DIFF));
	if (txt)
	{
		if (advanced)
			txt->SetText(LocalizeString(IDS_EDITOR_ADVANCED));
		else
			txt->SetText(LocalizeString(IDS_EDITOR_EASY));
	}
	
	IControl *ctrl = GetCtrl(IDC_ARCMAP_MERGE);
	if (ctrl) ctrl->ShowCtrl(_advanced);
	ctrl = GetCtrl(IDC_ARCMAP_SECTION);
	if (ctrl) ctrl->ShowCtrl(_advanced);
	ctrl = GetCtrl(IDC_ARCMAP_IDS);
	if (ctrl) ctrl->ShowCtrl(_advanced);
	if (!_advanced)
	{
		_map->ShowIds(false);
		UpdateIdsButton();
	}
	
	if (!_advanced && _currentTemplate != &_templateMission)
	{
		CCombo *combo = static_cast<CCombo *>(GetCtrl(IDC_ARCMAP_SECTION));
		if (combo) combo->SetCurSel(0);
	}
}

///////////////////////////////////////////////////////////////////////////////
// Arcade Map display

Control *DisplayArcadeMap::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
	case IDC_MAP:
		{
			_map = new CStaticMapArcade(this, idc, cls, ERFull, 0.001, 1.0, 0.1);
			_map->SetTemplate(&_templateMission);
			return _map;
		}	
	case IDC_ARCMAP_SECTION:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			combo->AddString(LocalizeString(IDS_SECTION_MISSION));
			combo->AddString(LocalizeString(IDS_SECTION_INTRO));
			combo->AddString(LocalizeString(IDS_SECTION_OUTRO_WIN));
			combo->AddString(LocalizeString(IDS_SECTION_OUTRO_LOOSE));
			combo->SetCurSel(0);
			return combo;
		}
	default:
		return DisplayMapEditor::OnCreateCtrl(type, idc, cls);
	}
}

/*!
\patch 1.53 Date 4/30/2002 by Jirka
- Added: mission editor - buttons "Show IDs" and "Show Textures"
*/

void DisplayArcadeMap::OnButtonClicked(int idc)
{
	switch (idc)
	{
		case IDC_ARCMAP_DIFF:
			SetAdvancedMode(!_advanced);
			SaveParams();
			return;
		case IDC_ARCMAP_LOAD:
			CreateChild(new DisplayTemplateLoad(this));
			return;
		case IDC_ARCMAP_MERGE:
			CreateChild(new DisplayTemplateLoad(this, true));
			return;
		case IDC_ARCMAP_SAVE:
			CreateChild(new DisplayTemplateSave(this));
			return;
		case IDC_ARCMAP_INTEL:
			CreateChild(new DisplayIntel(this, _currentTemplate->intel, _advanced));
			return;
		case IDC_ARCMAP_CLEAR:
			CreateMsgBox
			(
				MB_BUTTON_OK | MB_BUTTON_CANCEL,
				LocalizeString(IDS_SURE),
				IDD_MSG_CLEARTEMPLATE
			);
			return;
		case IDC_CANCEL:
			CreateMsgBox
			(
				MB_BUTTON_OK | MB_BUTTON_CANCEL,
				LocalizeString(IDS_SURE),
				IDD_MSG_EXITTEMPLATE
			);
			return;
		case IDC_ARCMAP_PREVIEW:
			if (_running) return;
			_running = true;
			if (_currentTemplate->IsConsistent(this, _multiplayer)) 
			{
				_currentTemplate->ScanRequiredAddons();
				if (_multiplayer)
				{
					// save current version
					RString directory;
					char buffer[256];
					strcpy(buffer, GetMissionsDirectory());
					::CreateDirectory(buffer, NULL);
					strcat(buffer, Glob.header.filename);
					strcat(buffer, ".");
					strcat(buffer, Glob.header.worldname);
					directory = buffer;
					::CreateDirectory(directory, NULL);
					strcat(buffer, "\\mission.sqm");
					SaveTemplates(buffer);

					Exit(IDC_OK);
					return;
				}

				CurrentTemplate = *_currentTemplate;
				_alwaysShow = false;

				RString weapons = GetSaveDirectory() + RString("weapons.cfg");
				::DeleteFile(weapons);

				GWorld->SwitchLandscape(GetWorldName(Glob.header.worldname));
				GWorld->ActivateAddons(CurrentTemplate.addOns);
				GWorld->InitGeneral(CurrentTemplate.intel);
				if (_currentTemplate == &_templateMission)
				{
					GStats.ClearAll();
					bool ok = GWorld->InitVehicles(GModeArcade, CurrentTemplate);

					if (ok)
					{
						RString weapons = GetSaveDirectory() + RString("weapons.cfg");
						::DeleteFile(weapons);

						// remove continue.fps
						RString dir = GetSaveDirectory();
						RString save = dir + RString("continue.fps");
						::DeleteFile(save);
						save = dir + RString("autosave.fps");
						::DeleteFile(save);
						save = dir + RString("save.fps");
						::DeleteFile(save);

						if 
						(
							(GInput.keys[DIK_LSHIFT] > 0 || GInput.keys[DIK_RSHIFT] > 0) &&
							GetBriefingFile().GetLength() > 0
						)
						{
							CreateChild(new DisplayGetReady(this));
							_running = false;
							return;
						}
						CreateChild(new DisplayMission(this, true));
					}
					else
					{
						_alwaysShow = true;
					}
				}
				else
				{
					bool ok = GWorld->InitVehicles(GModeIntro, CurrentTemplate);
					if (ok)
					{
						CreateChild(new DisplayIntro(this, DisplayIntro::noInit));
					}
					else
					{
						_alwaysShow = true;
					}
				}
			}
			_running = false;
			return;
		// TODO: remove continue button
		case IDC_ARCMAP_CONTINUE:
			if (GWorld->GetMode() == GModeArcade)
			{
				_alwaysShow = false;
//				GWorld->EnableDisplay(true);
				CreateChild(new DisplayMission(this));
			}
			return;
		case IDC_ARCMAP_IDS:
			if (_map)
			{
				_map->ShowIds(!_map->IsShowingIds());
				UpdateIdsButton();
			}
			break;
		case IDC_ARCMAP_TEXTURES:
			if (_map)
			{
				_map->ShowScale(!_map->IsShowingScale());
				UpdateTexturesButton();
			}
			break;
		default:
			DisplayMapEditor::OnButtonClicked(idc);
			break;
	}
}

void DisplayArcadeMap::UpdateIdsButton()
{
	CButton *button = dynamic_cast<CButton *>(GetCtrl(IDC_ARCMAP_IDS));
	if (button && _map)
	{
		if (_map->IsShowingIds())
			button->SetText(LocalizeString(IDS_ARCMAP_HIDE_IDS));
		else
			button->SetText(LocalizeString(IDS_ARCMAP_SHOW_IDS));
	}
}

void DisplayArcadeMap::UpdateTexturesButton()
{
	CButton *button = dynamic_cast<CButton *>(GetCtrl(IDC_ARCMAP_TEXTURES));
	if (button && _map)
	{
		if (_map->IsShowingScale())
			button->SetText(LocalizeString(IDS_ARCMAP_SHOW_TEXTURES));
		else
			button->SetText(LocalizeString(IDS_ARCMAP_HIDE_TEXTURES));
	}
}

void DisplayArcadeMap::OnComboSelChanged(int idc, int curSel)
{
	switch (idc)
	{
		case IDC_ARCMAP_SECTION:
			if (curSel == 0)
				_currentTemplate = &_templateMission;
			else if (curSel == 1)
				_currentTemplate = &_templateIntro;
			else if (curSel == 2)
				_currentTemplate = &_templateOutroWin;
			else if (curSel == 3)
				_currentTemplate = &_templateOutroLoose;
			_map->_infoClick._type = signNone;
			_map->_infoClickCandidate._type = signNone;
			_map->_infoMove._type = signNone;
			_map->SetTemplate(_currentTemplate);
			// Intel does not change
			ArcadeUnitInfo *uInfo = _currentTemplate->FindPlayer(); 
			if (uInfo)
			{
				Glob.header.playerSide = (TargetSide)uInfo->side;
			}
			ShowButtons();
			break;
	}
	Display::OnComboSelChanged(idc, curSel);
}

void DisplayArcadeMap::OnToolBoxSelChanged(int idc, int curSel)
{
	if (idc == IDC_ARCMAP_MODE)
	{
		_mode = (InsertMode)curSel;
	}
}

#if _ENABLE_CHEATS
// TODO: Place in standalone hpp and cpp file
// hpp {
struct Argument
{
	RString name;
	RString value;
};
TypeIsMovableZeroed(Argument)

template <class Type>
inline RString CreateArgument(const Type &value)
{
	return FindEnumName(value);
}

template <>
inline RString CreateArgument(const RString &value)
{
	return value;
}

template <>
inline RString CreateArgument(const float &value)
{
	return Format("%g", value);
}

template <>
inline RString CreateArgument(const int &value)
{
	return Format("%d", value);
}

template <>
inline RString CreateArgument(const bool &value)
{
	if (value)
		return "true";
	else
		return "false";
}

template <>
inline RString CreateArgument(const Vector3 &value)
{
	return Format("[%g, %g]", value.X(), value.Z());
}

template <>
inline RString CreateArgument(const TargetSide &value)
{
	switch (value)
	{
	case TWest:
		return "west";
	case TEast:
		return "east";
	case TCivilian:
		return "civilian";
	case TGuerrila:
		return "resistance";
	case TLogic:
		return "sideLogic";
	default:
		Fail("Side");
		return "ERROR";
	}
}

// TODO: use hash map instead
class Arguments : public AutoArray<Argument>
{
private:
	typedef AutoArray<Argument> base;

public:
	RString Get(RString name) const
	{
		for (int i=0; i<Size(); i++)
			if (base::Get(i).name == name) return base::Get(i).value;
		return RString();
	}
	template <class Type>
	int Set(RString name, const Type &value)
	{
		return SetRaw(name, CreateArgument(value));
	};

protected:
	int SetRaw(RString name, RString value)
	{
		int index = Add();
		base::Set(index).name = name;
		base::Set(index).value = value;
		return index;
	};
};

enum EditorParamSource
{
	EPSDialog,
	EPSPosition,
	EPSLink,
	EPSId
};

struct EditorParam
{
	EditorParamSource source;
	RString name;
	RString type;
	RString subtype;
	bool hasDefValue;
	RString defValue;

	EditorParam(EditorParamSource src, RString n, RString t, RString subt, bool hasDefVal, RString defVal)
	{
		source = src;
		name = n;
		type = t;
		subtype = subt;
		hasDefValue = hasDefVal;
		defValue = defVal;
	}
};
TypeIsMovableZeroed(EditorParam)

// TODO: use hash map instead
class EditorParams : public AutoArray<EditorParam>
{
public:
	void Load(const ParamEntry &cfg, RString name);
	const EditorParam *Find(RString name) const
	{
		for (int i=0; i<Size(); i++)
			if (Get(i).name == name) return &Get(i);
		return NULL;
	}
};

class EditorObjectType
{
protected:
	RString _name;
	EditorParams _params;
	AutoArray<RString> _create;
	int _nextID;

public:
	EditorObjectType(RString name);

	void WriteToScript(QOStream &out, const Arguments &args) const;
	RString NextVarName() {return Format("_%s_%d", (const char *)_name, _nextID++);}
};

// }

// cpp {

void EditorParams::Load(const ParamEntry &cfg, RString name)
{
	EditorParam defaults[] =
	{
		EditorParam(EPSId, "VARIABLE_NAME", name, "", false, "")
	};
	int nDefaults = sizeof(defaults) / sizeof(EditorParam);

	int n = cfg.GetEntryCount();
	Realloc(n + nDefaults);
	Resize(n + nDefaults);
	for (int i=0; i<n; i++)
	{
		const ParamEntry &entry = cfg.GetEntry(i);

		const ParamEntry *check = entry.FindEntry("source");
		if (check)
		{
			RString source = *check;
			if (stricmp(source, "link") == 0) Set(i).source = EPSLink;
			else if (stricmp(source, "position") == 0) Set(i).source = EPSPosition;
			else
			{
				DoAssert(stricmp(source, "dialog") == 0);
				Set(i).source = EPSDialog;
			}
		}
		else Set(i).source = EPSDialog;
		
		Set(i).name = entry.GetName();
		Set(i).type = entry >> "type";

		check = entry.FindEntry("subtype");
		if (check) Set(i).subtype = *check;

		check = entry.FindEntry("default");
		if (check)
		{
			Set(i).hasDefValue = true;
			Set(i).defValue = *check;
		}
		else Set(i).hasDefValue = false;
	}

	// default parameters
	for (int i=0; i<nDefaults; i++)
		Set(n + i) = defaults[i];
}

EditorObjectType::EditorObjectType(RString name)
{
	_name = name;
	_nextID = 0;

	ParamFile file;
	file.Parse(Format("Editor\\%s.hpp", (const char *)name));
	_params.Load(file >> "Params", _name);

	const ParamEntry &array = file >> "create";
	int n = array.GetSize();
	_create.Realloc(n);
	_create.Resize(n);
	for (int i=0; i<n; i++) _create[i] = array[i];
}

inline void WriteLine(QOStream &out, RString line)
{
	out.write(line, strlen(line));
	out.write("\r\n", 2);
}

static RString NormalizeString(const char *src)
{
	static const int len = 1024;
	char dst[len];
	int i=0;
	for (const char *ptr=src; *ptr; ptr++)
	{
		if (i >= len - 1)
		{
			Fail("Buffer overflow");
			break;
		}
		dst[i++] = *ptr;
		if (*ptr == '"')
		{
			if (i >= len - 1)
			{
				Fail("Buffer overflow");
				break;
			}
			dst[i++] = *ptr;
		}
	}
	dst[i] = 0;
	return dst;
}

static RString ExtractValue(RString name, const EditorParams &params, const Arguments &args, bool &required)
{
	const EditorParam *param = params.Find(name);
	if (!param)
	{
		ErrF("Undeclared parameter: %s", (const char *)name);
		return "ERROR";
	}

	switch (param->source)
	{
	case EPSLink:
		{
			if (stricmp(param->subtype, "single") == 0)
			{
				// no default value
				required = true;
				return args.Get(name);
			}
			else
			{
				RString value = "[";
				bool first = true;
				for (int i=0; i<args.Size(); i++)
				{
					if (args[i].name != name) continue;
					if (first) first = false;
					else value = value + ", ";
					value = value + args[i].value;
				}
				if (!first) required = true;
				return value + "]";
			}
		}
	default:
		{
			RString value = args.Get(name);

			if (!param->hasDefValue || value != param->defValue) required = true;
			
			if (stricmp(param->type, "text") == 0)
				return RString("\"") + NormalizeString(value) + RString("\"");
			else if (stricmp(param->type, "enum") == 0)
				return RString("\"") + value + RString("\"");
			else if (stricmp(param->type, "config") == 0)
				return RString("\"") + value + RString("\"");
			return value;
		}
	}
}

#define PARSING_COMMENT	"."

typedef void (*OnRequiredFunction)(void *context);
typedef RString (*OnValueFunction)(RString name, const EditorParams &params, void *context);
typedef RString (*OnExpressionFunction)(RString name, const EditorParams &params, void *context);

static RString ParseLine
(
	RString format, const EditorParams &params,
	OnRequiredFunction onRequired, OnValueFunction onValue, OnExpressionFunction onExpression,
	void *context
)
{
	static const int len = 1024;
	char dst[len];
	int i=0;
	for (const char *ptr=format; *ptr; ptr++)
	{
		if (*ptr == '%')
		{
			ptr++;
			if (!*ptr)
			{
				Fail("Syntax error");
				break;
			}
			else if (*ptr == '%')
			{
				dst[i++] = *ptr;
				if (i >= len - 1)
				{
					Fail("Buffer overflow");
					break;
				}
			}
			else if (*ptr == '!')
			{
				onRequired(context);
			}
			else if (*ptr == '(')
			{
				ptr++;
				const char *begin = ptr;
				while (*ptr && *ptr != ')') ptr++;
				RString name(begin, ptr - begin);
				if (!*ptr) ptr--;

				// expression
				RString value = onExpression(name, params, context);
				int n = value.GetLength();
				if (i + n >= len - 1)
				{
					Fail("Buffer overflow");
					break;
				}
				strcpy(dst + i, value); i += n;
			}
			else
			{
				const char *begin = ptr;
				while (*ptr && __iscsym(*ptr)) ptr++;
				RString name(begin, ptr - begin);
				ptr--;

				// value
				RString value = onValue(name, params, context);
				int n = value.GetLength();
				if (i + n >= len - 1)
				{
					Fail("Buffer overflow");
					break;
				}
				strcpy(dst + i, value); i += n;
			}
		}
		else
		{
			dst[i++] = *ptr;
			if (i >= len - 1)
			{
				Fail("Buffer overflow");
				break;
			}
		}
	}
	dst[i] = 0;
	return dst;
}

struct CreateContext
{
	const Arguments *args;
	bool some;
	bool required;
};

void CreateOnRequired(void *context)
{
	CreateContext *ctx = (CreateContext *)context;
	ctx->required = true;
}

RString CreateOnValue(RString name, const EditorParams &params, void *context)
{
	CreateContext *ctx = (CreateContext *)context;
	ctx->some = true;
	return
		RString("/*"PARSING_COMMENT) + name + RString("*/") +
		ExtractValue(name, params, *ctx->args, ctx->required) +
		RString("/*"PARSING_COMMENT"*/");
}

RString CreateOnExpression(RString name, const EditorParams &params, void *context)
{
	CreateContext *ctx = (CreateContext *)context;
	ctx->some = true;
	return ExtractValue(name, params, *ctx->args, ctx->required);
}

void EditorObjectType::WriteToScript(QOStream &out, const Arguments &args) const
{
	CreateContext ctx;
	ctx.args = &args;
	
	RString varName = args.Get("VARIABLE_NAME");
	WriteLine(out, Format("/*"PARSING_COMMENT"_OBJECT %s %s {*/", (const char *)_name, (const char *)varName));
	for (int i=0; i<_create.Size(); i++)
	{
		ctx.some = false;
		ctx.required = false;
		RString line = ParseLine
		(
			_create[i], _params,
			CreateOnRequired, CreateOnValue, CreateOnExpression,
			&ctx
		);
		if (ctx.required || !ctx.some) WriteLine(out, line);
	}
	WriteLine(out, "/*"PARSING_COMMENT"}*/");
}

// }

static void WriteLine(QOStream &out, const char *format, ...)
{
	va_list arglist;
	va_start(arglist, format);

	BString<1024> line;
	vsprintf(line, format, arglist);
	out.write(line, strlen(line));
	out.write("\r\n", 2);

	va_end(arglist);
}

struct CenterInfo
{
	TargetSide side;
	RString variable;
};
TypeIsMovableZeroed(CenterInfo)

struct MarkerInfo
{
	RString name;
	RString variable;
};
TypeIsMovableZeroed(MarkerInfo)

struct VehicleInfo
{
	int id;
	RString variable;
};
TypeIsMovableZeroed(VehicleInfo)

struct SyncInfo
{
	int sync;
	RString variable1;
	RString variable2;
};
TypeIsMovableZeroed(SyncInfo)

#include "network.hpp"
#include "soldierOld.hpp"
#include "invisibleVeh.hpp"

static void WriteEffects(Arguments &args, const ArcadeEffects &effects)
{
	args.Set("EFFECT_CONDITION", effects.condition);
	args.Set("CAMERA_EFFECT", effects.cameraEffect);
	args.Set("CAMERA_EFFECT_POSITION", effects.cameraPosition);
	args.Set("SOUND_EFFECT", effects.sound);
	args.Set("VOICE_EFFECT", effects.voice);
	args.Set("SOUND_ENV_EFFECT", effects.soundEnv);
	args.Set("SOUND_DET_EFFECT", effects.soundDet);
	args.Set("MUSIC_EFFECT", effects.track);
	args.Set("TITLE_EFFECT_TYPE", effects.titleType);
	args.Set("TITLE_EFFECT_EFFECT", effects.titleEffect);
	args.Set("TITLE_EFFECT_TITLE", effects.title);
}

static void WriteTrigger
(
	QOStream &out, const ArcadeSensorInfo &sensor,
	EditorObjectType &typeTrigger, EditorObjectType &typeGuardedPoint,
	AutoArray<SyncInfo> &waypointMap, AutoArray<VehicleInfo> &vehicleMap
)
{
	TargetSide side = TSideUnknown;
	switch (sensor.type)	// special cases - guarded by ...
	{
		case ASTEastGuarded:
			side = TEast;
			break;
		case ASTWestGuarded:
			side = TWest;
			break;
		case ASTGuerrilaGuarded:
			side = TGuerrila;
			break;
	}
	if (side == TSideUnknown)
	{
		// add real trigger
		Arguments args;

		args.Set("POSITION", sensor.position);
		args.Set("OBJECT", sensor.object);
		args.Set("A", sensor.a);
		args.Set("B", sensor.b);
		args.Set("ANGLE", sensor.angle);
		args.Set("RECTANGULAR", sensor.rectangular);
		args.Set("ACTIVATION_BY", sensor.activationBy);
		args.Set("ACTIVATION_TYPE", sensor.activationType);
		args.Set("REPEATING", sensor.repeating);
		args.Set("TYPE", sensor.type);
		args.Set("TIMEOUT_MIN", sensor.timeoutMin);
		args.Set("TIMEOUT_MID", sensor.timeoutMid);
		args.Set("TIMEOUT_MAX", sensor.timeoutMax);
		args.Set("INTERRUPTABLE", sensor.interruptable);
		args.Set("ID_STATIC", sensor.idStatic);
		if (sensor.idVehicle >= 0)
		{
			bool found = false;
			for (int i=0; i<vehicleMap.Size(); i++)
			{
				if (sensor.idVehicle == vehicleMap[i].id)
				{
					args.Set("VEHICLE", vehicleMap[i].variable);
					found = true;
					break;
				}
			}
			DoAssert(found);
		}
		args.Set("TEXT", sensor.text);
		args.Set("EXP_COND", sensor.expCond);
		args.Set("EXP_ACTIV", sensor.expActiv);
		args.Set("EXP_DESACTIV", sensor.expDesactiv);
		args.Set("AGE", sensor.age);
		args.Set("NAME", sensor.name);
		WriteEffects(args, sensor.effects);

		RString varName = typeTrigger.NextVarName();
		args.Set("VARIABLE_NAME", varName);

		// synchronizations
		for (int s=0; s<sensor.synchronizations.Size(); s++)
		{
			int sync = sensor.synchronizations[s];
			RString name;
			for (int k=0; k<waypointMap.Size(); k++)
			{
				if (sync == waypointMap[k].sync)
				{
					DoAssert(waypointMap[k].variable2.GetLength() == 0);
					name = waypointMap[k].variable1;
					waypointMap[k].variable2 = varName;
					break;
				}
			}
			DoAssert(name.GetLength() > 0);
			args.Set("SYNC", name);
		}

		typeTrigger.WriteToScript(out, args);
	}
	else
	{
		// add guarded point
		Arguments args;

		args.Set("POSITION", sensor.position);
		args.Set("SIDE", side);
		args.Set("ID_STATIC", sensor.idStatic);
		if (sensor.idVehicle >= 0)
		{
			bool found = false;
			for (int i=0; i<vehicleMap.Size(); i++)
			{
				if (sensor.idVehicle == vehicleMap[i].id)
				{
					args.Set("VEHICLE", vehicleMap[i].variable);
					found = true;
					break;
				}
			}
			DoAssert(found);
		}

		RString varName = typeGuardedPoint.NextVarName();
		args.Set("VARIABLE_NAME", varName);

		typeGuardedPoint.WriteToScript(out, args);
	}
}

RString CreateCondition(RString presenceCondition, float presence)
{
	if (presence < 1)
	{
		if (strcmp(presenceCondition, "true") == 0) return Format("%g > random 1", presence);
		return Format("(%s) and (%g > random 1)", (const char *)presenceCondition, presence);
	}
	else return presenceCondition;
}

static RString WriteVehicle
(
	QOStream &out, const ArcadeUnitInfo &unit,
	EditorObjectType &typeSoundSource, EditorObjectType &typeMine, EditorObjectType &typeVehicle,
	AutoArray<MarkerInfo> &markerMap, AutoArray<VehicleInfo> &vehicleMap
)
{
	RString vehClass = Pars >> "CfgVehicles" >> unit.vehicle >> "vehicleClass";

	if (stricmp(vehClass, "Sounds") == 0)
	{
		// sound source
		Arguments args;

		RString condition = CreateCondition(unit.presenceCondition, unit.presence);
		args.Set("PRESENCE_CONDITION", condition);
		args.Set("POSITION", unit.position);
		args.Set("TYPE", unit.vehicle);
		for (int i=0; i<unit.markers.Size(); i++)
			for (int j=0; j<markerMap.Size(); j++)
				if (markerMap[j].name == unit.markers[i])
				{
					args.Set("MARKER", markerMap[j].variable);
					break;
				}
		args.Set("PLACEMENT", unit.placement);

		RString varName = typeSoundSource.NextVarName();
		args.Set("VARIABLE_NAME", varName);

		typeSoundSource.WriteToScript(out, args);

		return RString();
	}
	else if (stricmp(vehClass, "Mines") == 0)
	{
		// mine
		Arguments args;

		RString condition = CreateCondition(unit.presenceCondition, unit.presence);
		args.Set("PRESENCE_CONDITION", condition);
		args.Set("POSITION", unit.position);
		args.Set("TYPE", unit.vehicle);
		for (int i=0; i<unit.markers.Size(); i++)
			for (int j=0; j<markerMap.Size(); j++)
				if (markerMap[j].name == unit.markers[i])
				{
					args.Set("MARKER", markerMap[j].variable);
					break;
				}
		args.Set("PLACEMENT", unit.placement);

		RString varName = typeMine.NextVarName();
		args.Set("VARIABLE_NAME", varName);

		typeMine.WriteToScript(out, args);

		return RString();
	}
	else
	{
		// regular vehicle
		Arguments args;

		RString condition = CreateCondition(unit.presenceCondition, unit.presence);
		args.Set("PRESENCE_CONDITION", condition);
		args.Set("POSITION", unit.position);
		args.Set("TYPE", unit.vehicle);
		for (int i=0; i<unit.markers.Size(); i++)
			for (int j=0; j<markerMap.Size(); j++)
				if (markerMap[j].name == unit.markers[i])
				{
					args.Set("MARKER", markerMap[j].variable);
					break;
				}
		args.Set("PLACEMENT", unit.placement);
		args.Set("SPECIAL", unit.special);
		args.Set("AZIMUT", unit.azimut);
		args.Set("NAME", unit.name);
		args.Set("HEALTH", unit.health);
		args.Set("FUEL", unit.fuel);
		args.Set("AMMO", unit.ammo);
		args.Set("ID", unit.id);
		args.Set("AGE", unit.age);
		args.Set("LOCK", unit.lock);
		args.Set("INIT", unit.init);

		RString varName = typeVehicle.NextVarName();
		args.Set("VARIABLE_NAME", varName);

		typeVehicle.WriteToScript(out, args);

		if (unit.id >= 0)
		{
			int index = vehicleMap.Add();
			vehicleMap[index].id = unit.id;
			vehicleMap[index].variable = varName;
		}

		return varName;
	}
}

static void WriteUnit
(
	QOStream &out, RString grpName, const ArcadeUnitInfo &unit,
	EditorObjectType &typeUnit,
	AutoArray<MarkerInfo> &markerMap, AutoArray<VehicleInfo> &vehicleMap
)
{
	// free soldier
	Arguments args;

	RString condition = CreateCondition(unit.presenceCondition, unit.presence);
	args.Set("PRESENCE_CONDITION", condition);
	args.Set("POSITION", unit.position);
	args.Set("GROUP", grpName);
	args.Set("TYPE", unit.vehicle);
	for (int i=0; i<unit.markers.Size(); i++)
		for (int j=0; j<markerMap.Size(); j++)
			if (markerMap[j].name == unit.markers[i])
			{
				args.Set("MARKER", markerMap[j].variable);
				break;
			}
	args.Set("PLACEMENT", unit.placement);
	args.Set("SPECIAL", unit.special);
	args.Set("AZIMUT", unit.azimut);
	args.Set("NAME", unit.name);
	args.Set("HEALTH", unit.health);
	args.Set("AMMO", unit.ammo);
	args.Set("ID", unit.id);
	args.Set("AGE", unit.age);
	args.Set("INIT", unit.init);
	args.Set("RANK", unit.rank);
	args.Set("SKILL", unit.skill);

	RString varName = typeUnit.NextVarName();
	args.Set("VARIABLE_NAME", varName);

	if (unit.player == APPlayerCommander) args.Set("PLAYER", varName);
	if (unit.leader) args.Set("LEADER", grpName);

	typeUnit.WriteToScript(out, args);

	if (unit.id >= 0)
	{
		int index = vehicleMap.Add();
		vehicleMap[index].id = unit.id;
		vehicleMap[index].variable = varName;
	}
}

static void WriteUnit
(
	QOStream &out, EditorObjectType &typeUnit, RString type,
	RString grpName, Vector3Par position,
	RString name, Rank rank, float skill,
	RString vehicle, GetInPosition pos,
	bool player, bool leader
)
{
	// soldier in vehicle
	Arguments args;

	args.Set("PRESENCE_CONDITION", Format("not isNull %s", (const char *)vehicle));
	args.Set("POSITION", position);
	args.Set("GROUP", grpName);
	args.Set("TYPE", type);
	args.Set("PLACEMENT", 0);
	args.Set("SPECIAL", RString("NONE"));
	args.Set("AZIMUT", 0);
	args.Set("NAME", name);
	args.Set("HEALTH", 1);
	args.Set("AMMO", 1);
	args.Set("ID", -1);
	args.Set("AGE", RString("UNKNOWN"));
	args.Set("INIT", RString());
	args.Set("RANK", rank);
	args.Set("SKILL", skill);
	switch (pos)
	{
	case GIPCommander:
		args.Set("COMMANDER", vehicle);
		break;
	case GIPDriver:
		args.Set("DRIVER", vehicle);
		break;
	case GIPGunner:
		args.Set("GUNNER", vehicle);
		break;
	case GIPCargo:
		args.Set("CARGO", vehicle);
		break;
	}

	RString varName = typeUnit.NextVarName();
	args.Set("VARIABLE_NAME", varName);

	if (player) args.Set("PLAYER", varName);
	if (leader) args.Set("LEADER", grpName);

	typeUnit.WriteToScript(out, args);
}

static void ExportMission(ArcadeTemplate &t, RString filename)
{
	// Create templates
	EditorObjectType typeIntel("intel");
	EditorObjectType typeCenter("center");
	EditorObjectType typeMarker("marker");
	EditorObjectType typeTrigger("trigger");
	EditorObjectType typeGuardedPoint("guardedPoint");
	EditorObjectType typeSoundSource("soundSource");
	EditorObjectType typeMine("mine");
	EditorObjectType typeVehicle("vehicle");
	EditorObjectType typeUnit("unit");
	EditorObjectType typeGroup("group");
	EditorObjectType typeWaypoint("waypoint");

	// Translation
	AutoArray<CenterInfo> centerMap;
	AutoArray<MarkerInfo> markerMap;
	AutoArray<VehicleInfo> vehicleMap;
	AutoArray<SyncInfo> waypointMap;
	AutoArray<RString> groupMap;

	QOFStream out(filename);
	WriteLine(out, "/*"PARSING_COMMENT"_MISSION_EDITOR {*/");
	WriteLine(out, "/* DO NOT MANUALLY EDIT THIS CODE! */");
	WriteLine(out, "");

	// general init
	RString line = "activateAddons [";
	bool first = true;
	for (int i=0; i<t.addOns.Size(); i++)
	{
		if (first) first = false;
		else line = line + RString(", ");
		line = line + RString("\"") + t.addOns[i] + RString("\"");
	}
	line = line + "];";
	if (!first) WriteLine(out, line);

	// intel
	{
		Arguments args;
		args.Set("OVERCAST", t.intel.weather);
		args.Set("OVERCAST_WANTED", t.intel.weatherForecast);
		args.Set("FOG", t.intel.fog);
		args.Set("FOG_WANTED", t.intel.fogForecast);
		args.Set("YEAR", t.intel.year);
		args.Set("MONTH", t.intel.month);
		args.Set("DAY", t.intel.day);
		args.Set("HOUR", t.intel.hour);
		args.Set("MINUTE", t.intel.minute);

		RString varName = typeIntel.NextVarName();
		args.Set("VARIABLE_NAME", varName);

		typeIntel.WriteToScript(out, args);
	}
	WriteLine(out, "");
	
	// centers
	for (int i=0; i<TSideUnknown; i++)
	{
		bool found = false;
		for (int j=0; j<t.groups.Size(); j++)
		{
			if (t.groups[j].side == i)
			{
				found = true;
				break;
			}
		}
		if (!found) continue;

		Arguments args;
		args.Set("SIDE", (TargetSide)i);
		if (i == TCivilian)
		{
			args.Set("FRIEND_EAST", t.intel.friends[TGuerrila][TEast]);
			args.Set("FRIEND_WEST", t.intel.friends[TGuerrila][TWest]);
			args.Set("FRIEND_RESISTANCE", t.intel.friends[TGuerrila][TGuerrila]);
		}
		else
		{
			args.Set("FRIEND_EAST", t.intel.friends[i][TEast]);
			args.Set("FRIEND_WEST", t.intel.friends[i][TWest]);
			args.Set("FRIEND_RESISTANCE", t.intel.friends[i][TGuerrila]);
		}
		args.Set("FRIEND_CIVILIAN", 1.0f);

		RString varName = typeCenter.NextVarName();
		args.Set("VARIABLE_NAME", varName);

		typeCenter.WriteToScript(out, args);

		int index = centerMap.Add();
		centerMap[index].side = (TargetSide)i;
		centerMap[index].variable = varName;
	}
	{
		// game logic
		bool found = false;
		for (int j=0; j<t.groups.Size(); j++)
		{
			if (t.groups[j].side == TLogic)
			{
				found = true;
				break;
			}
		}
		if (found)
		{
			Arguments args;
			args.Set("SIDE", TLogic);
			args.Set("FRIEND_EAST", 1.0f);
			args.Set("FRIEND_WEST", 1.0f);
			args.Set("FRIEND_RESISTANCE", 1.0f);
			args.Set("FRIEND_CIVILIAN", 1.0f);

			RString varName = typeCenter.NextVarName();
			args.Set("VARIABLE_NAME", varName);

			typeCenter.WriteToScript(out, args);

			int index = centerMap.Add();
			centerMap[index].side = TLogic;
			centerMap[index].variable = varName;
		}
	}
	WriteLine(out, "");

	// markers
	for (int j=0; j<t.markers.Size(); j++)
	{
		const ArcadeMarkerInfo &marker = t.markers[j];

		Arguments args;
		args.Set("POSITION", marker.position);
		args.Set("NAME", marker.name);
		args.Set("TEXT", marker.text);
		args.Set("MARKER_TYPE", marker.markerType);
		args.Set("TYPE", marker.type);
		args.Set("COLOR", marker.colorName);
		args.Set("FILL", marker.fillName);
		args.Set("A", marker.a);
		args.Set("B", marker.b);
		args.Set("ANGLE", marker.angle);

		RString varName = typeMarker.NextVarName();
		args.Set("VARIABLE_NAME", varName);

		typeMarker.WriteToScript(out, args);

		int index = markerMap.Add();
		markerMap[index].name = marker.name;
		markerMap[index].variable = varName;
	}
	WriteLine(out, "");

	// group units
	groupMap.Realloc(t.groups.Size());
	groupMap.Resize(t.groups.Size());
	RString player = "objNull";
	for (int i=0; i<t.groups.Size(); i++)
	{
		const ArcadeGroupInfo &group = t.groups[i];

		Arguments args;
		for (int j=0; j<centerMap.Size(); j++)
			if (centerMap[j].side == group.side)
			{
				args.Set("CENTER", centerMap[j].variable);
				break;
			}
		RString grpName = typeGroup.NextVarName();
		args.Set("VARIABLE_NAME", grpName);
		groupMap[i] = grpName;
		typeGroup.WriteToScript(out, args);

		// units
		for (int j=0; j<group.units.Size(); j++)
		{
			const ArcadeUnitInfo &unit = group.units[j];
			if (unit.special == ASpCargo) continue;

			Ref<EntityType> type = VehicleTypes.New(unit.vehicle);
			TransportType *transportType = dynamic_cast<TransportType *>(type.GetRef());
			
			if (transportType)
			{
				RString vehName = WriteVehicle(out, unit, typeSoundSource, typeMine, typeVehicle, markerMap, vehicleMap);
				
				// crew
				RString crewType = transportType->GetCrew();
				int commanderOffset = 1;
				int gunnerOffset = -1;
				// for vehicle where gunner is commander gunner rank
				// should be higher that driver
				bool driverIsCommander = transportType->DriverIsCommander();
				if (!driverIsCommander)
				{
					commanderOffset = 2;
					gunnerOffset = 1;
				}

				bool hasCommander = transportType->HasCommander();
				bool hasDriver = transportType->HasDriver();
				bool hasGunner = transportType->HasGunner();

				bool leaderCommander = false;
				bool leaderDriver = false;
				bool leaderGunner = false;
				if (unit.leader)
				{
					if (hasCommander) leaderCommander = true;
					else if (driverIsCommander)
					{
						if (hasDriver) leaderDriver = true;
						else leaderGunner = true;
					}
					else
					{
						if (hasGunner) leaderGunner = true;
						else leaderDriver = true;
					}
				}

				if (hasDriver)
				{
					RString name;
					if (unit.name.GetLength() > 0) name = unit.name + RString("d");
					WriteUnit
					(
						out, typeUnit, crewType, grpName, unit.position,
						name, unit.rank, unit.skill,
						vehName, GIPDriver, unit.player == APPlayerDriver, leaderDriver
					);
				}
				
				if (hasCommander)
				{
					int commanderRank = unit.rank + commanderOffset;
					saturate(commanderRank, 0, NRanks - 1);
					RString name;
					if (unit.name.GetLength() > 0) name = unit.name + RString("c");
					WriteUnit
					(
						out, typeUnit, crewType, grpName, unit.position,
						name, (Rank)commanderRank, unit.skill,
						vehName, GIPCommander, unit.player == APPlayerCommander, leaderCommander
					);
				}

				if (hasGunner)
				{
					int gunnerRank = unit.rank + gunnerOffset;
					saturate(gunnerRank, 0, NRanks - 1);
					RString name;
					if (unit.name.GetLength() > 0) name = unit.name + RString("g");
					WriteUnit
					(
						out, typeUnit, crewType, grpName, unit.position,
						name, (Rank)gunnerRank, unit.skill,
						vehName, GIPGunner, unit.player == APPlayerGunner, leaderGunner
					);
				}
			}
			else if (dynamic_cast<ManType *>(type.GetRef()) || dynamic_cast<InvisibleVehicleType *>(type.GetRef()))
			{
				WriteUnit(out, grpName, unit, typeUnit, markerMap, vehicleMap);
			}
			else
			{
				ErrF("Unit in group is not Man nor Transport");
				WriteVehicle(out, unit, typeSoundSource, typeMine, typeVehicle, markerMap, vehicleMap);
			}
		}
		// units
		for (int j=0; j<group.units.Size(); j++)
		{
			const ArcadeUnitInfo &unit = group.units[j];
			if (unit.special != ASpCargo) continue;

			Ref<EntityType> type = VehicleTypes.New(unit.vehicle);
			DoAssert(dynamic_cast<ManType *>(type.GetRef()));

			WriteUnit(out, grpName, unit, typeUnit, markerMap, vehicleMap);
		}
		WriteLine(out, "");
	}

	// empty vehicles
	for (int j=0; j<t.emptyVehicles.Size(); j++)
		WriteVehicle(out, t.emptyVehicles[j], typeSoundSource, typeMine, typeVehicle, markerMap, vehicleMap);
	WriteLine(out, "");
	
	// group waypoints
	for (int i=0; i<t.groups.Size(); i++)
	{
		const ArcadeGroupInfo &group = t.groups[i];
		RString grpName = groupMap[i];

		for (int j=0; j<group.waypoints.Size(); j++)
		{
			const ArcadeWaypointInfo &waypoint = group.waypoints[j];
			Arguments args;
			args.Set("POSITION", waypoint.position);
			args.Set("PLACEMENT", waypoint.placement);
			args.Set("GROUP", grpName);
			args.Set("TYPE", waypoint.type);
			if (waypoint.id >= 0)
			{
				bool found = false;
				for (int k=0; k<vehicleMap.Size(); k++)
					if (waypoint.id == vehicleMap[k].id)
					{
						args.Set("VEHICLE", vehicleMap[k].variable);
						found = true;
						break;
					}
				DoAssert(found);
			}
			args.Set("ID_STATIC", waypoint.idStatic);
			args.Set("HOUSE_POS", waypoint.housePos);
			args.Set("COMBAT_MODE", waypoint.combatMode);
			args.Set("FORMATION", waypoint.formation);
			args.Set("SPEED", waypoint.speed);
			args.Set("COMBAT", waypoint.combat);
			args.Set("DESCRIPTION", waypoint.description);
			args.Set("EXP_COND", waypoint.expCond);
			args.Set("EXP_ACTIV", waypoint.expActiv);
			args.Set("SCRIPT", waypoint.script);
			args.Set("TIMEOUT_MIN", waypoint.timeoutMin);
			args.Set("TIMEOUT_MID", waypoint.timeoutMid);
			args.Set("TIMEOUT_MAX", waypoint.timeoutMax);
			args.Set("SHOW", waypoint.showWP);
			WriteEffects(args, waypoint.effects);

			RString wpName = typeWaypoint.NextVarName();
			args.Set("VARIABLE_NAME", wpName);

			// synchronizations
			for (int s=0; s<waypoint.synchronizations.Size(); s++)
			{
				int sync = waypoint.synchronizations[s];
				RString name;
				for (int k=0; k<waypointMap.Size(); k++)
				{
					if (sync == waypointMap[k].sync)
					{
						DoAssert(waypointMap[k].variable2.GetLength() == 0);
						name = waypointMap[k].variable1;
						waypointMap[k].variable2 = wpName;
						break;
					}
				}
				if (name.GetLength() == 0)
				{
					int index = waypointMap.Add();
					waypointMap[index].sync = sync;
					waypointMap[index].variable1 = wpName;
				}
				else
				{
					args.Set("SYNC", name);
				}
			}

			typeWaypoint.WriteToScript(out, args);
		}
	}

	// sensors
	for (int j=0; j<t.sensors.Size(); j++)
		WriteTrigger(out, t.sensors[j], typeTrigger, typeGuardedPoint, waypointMap, vehicleMap);
	WriteLine(out, "");

	WriteLine(out, "/*"PARSING_COMMENT"} _MISSION_EDITOR*/");
	out.close();
}
#endif

void DisplayArcadeMap::OnSimulate(EntityAI *vehicle)
{
	DisplayMapEditor::OnSimulate(vehicle);
	_map->ProcessCheats();
#if _ENABLE_CHEATS
	if (GInput.GetCheat1ToDo(DIK_Z) && _currentTemplate)
		ExportMission(*_currentTemplate, GetMissionDirectory() + RString("mission.sqf"));
#endif
}

bool DisplayArcadeMap::OnUnregisteredAddonUsed( RString addon )
{
	_currentTemplate->addOns.AddUnique(addon);
	return true;
}

#ifdef _WIN32

struct SendViaEmailBackgroundContext
{
	HMODULE hLib;
	MapiMessage *msg;
};

static DWORD WINAPI SendViaEmailBackground(void *ctxv)
{
	SendViaEmailBackgroundContext *ctx = (SendViaEmailBackgroundContext *)ctxv;
	LPMAPISENDMAIL fnc = (LPMAPISENDMAIL)GetProcAddress(ctx->hLib, "MAPISendMail");
	if (fnc)
	{
		ULONG res = (*fnc)
		(
			0, 0, ctx->msg, MAPI_DIALOG | MAPI_NEW_SESSION, 0
		);
		return res;
	}
	return SUCCESS_SUCCESS;
}

void ProcessMessagesNoWait();

static ULONG SendViaEmailProcessMessages(HMODULE hLib, MapiMessage &msg)
{
	SendViaEmailBackgroundContext ctx;
	ctx.hLib = hLib;
	ctx.msg = &msg;
	DWORD threadId;
	HANDLE threadHandle = ::CreateThread
	(
		NULL,64*1024,SendViaEmailBackground,&ctx,CREATE_SUSPENDED,&threadId
	);

	if (!threadHandle)
	{
		return SendViaEmailBackground(&ctx);
	}
	else
	{
		::ResumeThread(threadHandle);
		for (;;)
		{
			DWORD ret = ::MsgWaitForMultipleObjects(1,&threadHandle,FALSE,500,QS_ALLEVENTS);
			// check if thread terminated
			if (ret==WAIT_OBJECT_0) break;
			ProcessMessagesNoWait();
		}
		ULONG ret = 0;
		BOOL ok = GetExitCodeThread(threadHandle,&ret);
		if (!ok) ret = 0;
		::CloseHandle(threadHandle);
		return ret;
	}
}

#endif

void EnableDesktopCursor(bool enable);

/*!
\patch 1.85 Date 9/18/2002 by Ondra
- Fixed: Save/Send by e-mail did not change focus to e-mail application.
*/

static void SendViaEmail(const char *fileName)
{
#ifdef _WIN32
	// send file
	char currentDirectory[1024];
	GetCurrentDirectory(1024, currentDirectory);

/*
	MapiRecipDesc MapiRecp;
	memset(&MapiRecp, 0, sizeof(MapiRecp));
	MapiRecp.ulRecipClass = MAPI_TO;
	MapiRecp.lpszName = "Jiri Martinek";
	MapiRecp.lpszAddress = "SMTP:martinek@jrc.cz";
*/

	MapiFileDesc MapiFile;
	memset(&MapiFile, 0, sizeof(MapiFile));
	MapiFile.nPosition = 0xFFFFFFFF;
	char buffer[1024];
	strcpy(buffer, currentDirectory);
	strcat(buffer, "\\");
	strcat(buffer, fileName);
	MapiFile.lpszPathName = buffer;


	MapiMessage MapiMsg;
	memset(&MapiMsg, 0, sizeof(MapiMsg));
#if _VBS1
	MapiMsg.lpszSubject = "VBS1 Mission";
#elif _COLD_WAR_ASSAULT
	MapiMsg.lpszSubject = "Cold War Assault Mission";
#else
	MapiMsg.lpszSubject = "Operation Flashpoint Mission";
#endif
	MapiMsg.lpszNoteText = "Copy to your mission directory.";
//							MapiMsg.nRecipCount = 1;
//							MapiMsg.lpRecips = &MapiRecp;
	MapiMsg.nFileCount = 1;
	MapiMsg.lpFiles = &MapiFile;

	
	GDebugger.PauseCheckingAlive();
	EnableDesktopCursor(true);

	HINSTANCE hLib = LoadLibrary("mapi32.dll");
	if (hLib != NULL)
	{
		ULONG res = SendViaEmailProcessMessages(hLib,MapiMsg);
		if (res != SUCCESS_SUCCESS)
		{
			WarningMessage("MAPI error, mail not sent");
		}

		FreeLibrary(hLib);
	}
	else
	{
		WarningMessage("MAPI library not found");
	}
	EnableDesktopCursor(false);
	GDebugger.ResumeCheckingAlive();

	// delete file
	SetCurrentDirectory(currentDirectory);
#endif
}

/*!
\patch 1.28 Date 10/22/2001 by Jirka
- Fixed: Editing of waypoints in simple mode do not change order now.
\patch 1.43 Date 1/29/2002 by Jirka
- Added: Enable briefing in mission editor preview (hold SHIFT when pressing "Preview" button)
\patch 1.77 Date 6/26/2002 by Jirka
- Fixed: Mission editor - after load mission from other island object labels was not updated
\patch_internal 1.85 Date 9/18/2002 by Ondra
- Fixed: Send by e-mail caused application was detected as frozen and terminated.
*/

void DisplayArcadeMap::OnChildDestroyed(int idd, int exit)
{
	switch (idd)
	{
		case IDD_INTEL_GETREADY:
			{
				DisplayMapEditor::OnChildDestroyed(idd, exit);
				if (exit != IDC_CANCEL)
				{
					CreateChild(new DisplayMission(this));
				}
			}
			break;
		case IDD_MISSION:
		{
			DisplayMapEditor::OnChildDestroyed(idd, exit);
			ParamEntry *entry = ExtParsMission.FindEntry("debriefing");
			if (entry && !(bool)(*entry))
			{
				GStats.Update();
				goto StartOutro;
			}
			else
				CreateChild(new DisplayDebriefing(this, false));
		}
		break;
		case IDD_DEBRIEFING:
		case IDD_INTRO:
			DisplayMapEditor::OnChildDestroyed(idd, exit);
			GWorld->DestroyMap(IDC_OK);
StartOutro:
			_alwaysShow = true;
			ShowButtons();
			break;
		case IDD_ARCADE_UNIT:
			if (exit == IDC_OK)
			{
				DisplayArcadeUnit *display = dynamic_cast<DisplayArcadeUnit *>((ControlsContainer *)_child);
				Assert(display);

				int ig = display->_indexGroup;
				int iu = display->_index;

				_lastUnit = display->_unit;

				_currentTemplate->ClearSelection();
				display->_unit.selected = true;
				_currentTemplate->UnitUpdate
				(
					ig, iu,
					display->_unit
				);

				_currentTemplate->IsConsistent(this, _multiplayer);
				ShowButtons();

				_map->_infoClick._type = signArcadeUnit;
				_map->_infoClick._indexGroup = ig;
				_map->_infoClick._index = iu;
			}
			DisplayMapEditor::OnChildDestroyed(idd, exit);
			break;
		case IDD_ARCADE_GROUP:
			if (exit == IDC_OK)
			{
				DisplayArcadeGroup *display = dynamic_cast<DisplayArcadeGroup *>((ControlsContainer *)_child);
				Assert(display);

				Vector3Val position = display->_position;

				float azimut = 0;
				CEdit *edit = dynamic_cast<CEdit *>(display->GetCtrl(IDC_ARCGRP_AZIMUT));
				if (edit) azimut = edit->GetText() ? atof(edit->GetText()) : 0;

				CCombo *combo = dynamic_cast<CCombo *>(display->GetCtrl(IDC_ARCGRP_SIDE));
				if (combo && combo->GetCurSel() >= 0)
				{
					RString side = combo->GetData(combo->GetCurSel());
	
					combo = dynamic_cast<CCombo *>(display->GetCtrl(IDC_ARCGRP_TYPE));
					if (combo && combo->GetCurSel() >= 0)
					{
						RString type = combo->GetData(combo->GetCurSel());

						combo = dynamic_cast<CCombo *>(display->GetCtrl(IDC_ARCGRP_NAME));
						if (combo && combo->GetCurSel() >= 0)
						{
							RString name = combo->GetData(combo->GetCurSel());

							_currentTemplate->ClearSelection();
							_currentTemplate->AddGroup(Pars >> "CfgGroups" >> side >> type >> name, position);
							int index = _currentTemplate->groups.Size() - 1;
							_currentTemplate->groups[index].Rotate(position, (H_PI / 180.0f) * azimut, false);

							_map->SetLastGroup(side, type, name);

							_currentTemplate->IsConsistent(this, _multiplayer);
							ShowButtons();

							_map->_infoClick._type = signNone;
							_map->_infoClickCandidate._type = signNone;
						}
					}
				}
			}
			DisplayMapEditor::OnChildDestroyed(idd, exit);
			break;
		case IDD_ARCADE_SENSOR:
			if (exit == IDC_OK)
			{
				DisplayArcadeSensor *display = dynamic_cast<DisplayArcadeSensor *>((ControlsContainer *)_child);
				Assert(display);

				_currentTemplate->ClearSelection();
				display->_sensor.selected = true;
				_currentTemplate->SensorUpdate
				(
					display->_ig, display->_index,
					display->_sensor
				);

				_map->_infoClick._type = signArcadeSensor;
				_map->_infoClick._indexGroup = display->_ig;
				_map->_infoClick._index = display->_index;
			}
			DisplayMapEditor::OnChildDestroyed(idd, exit);
			break;
		case IDD_ARCADE_MARKER:
			if (exit == IDC_OK)
			{
				DisplayArcadeMarker *display = dynamic_cast<DisplayArcadeMarker *>((ControlsContainer *)_child);
				Assert(display);

				_currentTemplate->ClearSelection();
				display->_marker.selected = true;
				_currentTemplate->MarkerUpdate
				(
					display->_index,
					display->_marker
				);

				_map->_infoClick._type = signArcadeMarker;
				_map->_infoClick._index = display->_index;
			}
			DisplayMapEditor::OnChildDestroyed(idd, exit);
			break;
		case IDD_ARCADE_WAYPOINT:
			if (exit == IDC_OK)
			{
				DisplayArcadeWaypoint *display = dynamic_cast<DisplayArcadeWaypoint *>((ControlsContainer *)_child);
				Assert(display);

				int ig = display->_indexGroup;
				int iw = display->_index;

				int iwnew = iw < 0 ? _currentTemplate->groups[ig].waypoints.Size() : iw;
				CCombo *combo = dynamic_cast<CCombo *>(display->GetCtrl(IDC_ARCWP_SEQ));
				if (combo)
				{
					iwnew = combo->GetCurSel();
					if (iw < 0 && iwnew < 0) iwnew = combo->GetSize() - 1;
				}

				_currentTemplate->ClearSelection();
				display->_waypoint.selected = true;
				_currentTemplate->WaypointUpdate
				(
					ig, iw, iwnew,
					display->_waypoint
				);

				_map->_infoClick._type = signArcadeWaypoint;
				_map->_infoClick._indexGroup = ig;
				_map->_infoClick._index = iwnew;
			}
			DisplayMapEditor::OnChildDestroyed(idd, exit);
			break;
		case IDD_TEMPLATE_SAVE:
			if (exit == IDC_OK)
			{
				CEdit *edit = dynamic_cast<CEdit *>(_child->GetCtrl(IDC_TEMPL_NAME));
				Assert(edit);
				RString text = edit->GetText();
				if (text.GetLength() > 0)
				{
					if (_multiplayer)
						SetMission(Glob.header.worldname, text, MPMissionsDir);
					else
						SetMission(Glob.header.worldname, text, MissionsDir);

					RString directory;
					char buffer[256];
					strcpy(buffer, GetMissionsDirectory());
					::CreateDirectory(buffer, NULL);
					strcat(buffer, text);
					strcat(buffer, ".");
					strcat(buffer, Glob.header.worldname);
					directory = buffer;
					::CreateDirectory(directory, NULL);
					strcat(buffer, "\\mission.sqm");
					SaveTemplates(buffer);

					CCombo *combo = dynamic_cast<CCombo *>(_child->GetCtrl(IDC_TEMPL_MODE));
					Assert(combo);
					switch (combo->GetCurSel())
					{
					case 0:
						// user mission
						break;
					case 1:
						// single mission
						{
							RString fileName =
								RString("Missions\\") +
								text + RString(".") +
								RString(Glob.header.worldname) + RString(".pbo");
							DeleteROFile(fileName);
							FileBankManager mgr;
							mgr.Create(fileName, directory, true);
						}
						break;
					case 2:
						// multiplayer mission
						{
							RString fileName =
								RString("MPMissions\\") +
								text + RString(".") +
								RString(Glob.header.worldname) + RString(".pbo");
							DeleteROFile(fileName);
							FileBankManager mgr;
							mgr.Create(fileName, directory, true);
						}
						break;
					case 3:
						// send by e-mail
						#ifdef _WIN32
						{
							// create file
							RString fileName = directory + RString(".pbo");
							DeleteROFile(fileName);
							FileBankManager mgr;
							mgr.Create(fileName, directory, true);

							SendViaEmail(fileName);
							DeleteROFile(fileName);
						}
						#endif
						break;
					}
					ShowButtons();
				}
			}
			DisplayMapEditor::OnChildDestroyed(idd, exit);
			break;
		case IDD_TEMPLATE_LOAD:
			if (exit == IDC_OK)
			{
				CCombo *combo = dynamic_cast<CCombo *>(_child->GetCtrl(IDC_TEMPL_ISLAND));
				Assert(combo);
				int index = combo->GetCurSel();
				Assert(index >= 0);
				RString island = combo->GetData(index);
				combo = dynamic_cast<CCombo *>(_child->GetCtrl(IDC_TEMPL_NAME));
				Assert(combo);
				int i = combo->GetCurSel();
				if (i >= 0)
				{
					RString name = combo->GetText(i);
					RString filename = GetMissionsDirectory() +
						name + RString(".") +
						island + RString("\\mission.sqm");
					DisplayTemplateLoad *display = dynamic_cast<DisplayTemplateLoad *>((ControlsContainer *)_child);
					Assert(display);
					if (display->_merge)
						MergeTemplates(filename);
					else
					{
						if (stricmp(island, Glob.header.worldname) != 0)
						{
							GWorld->SwitchLandscape(GetWorldName(island));
							_map->CreateObjectList();
						}

						if (_multiplayer)
							SetMission(island, name, MPMissionsDir);
						else
							SetMission(island, name, MissionsDir);
						LoadTemplates(filename);
						ArcadeUnitInfo *uInfo = _currentTemplate->FindPlayer();
						if (uInfo)
						{
							Glob.header.playerSide = (TargetSide)uInfo->side;
						}
					}
					if (_map)
					{
						_map->SetScale(-1); // default
						_map->Center();
						_map->Reset();
					}
				}
				ShowButtons();
			}
			DisplayMapEditor::OnChildDestroyed(idd, exit);
			break;
		case IDD_INTEL:
			if (exit == IDC_OK)
			{
				DisplayIntel *display = dynamic_cast<DisplayIntel *>((ControlsContainer *)_child);
				Assert(display);
				_currentTemplate->intel = display->_intel;
			}
			DisplayMapEditor::OnChildDestroyed(idd, exit);
			break;
		case IDD_MSG_CLEARTEMPLATE:
			if (exit == IDC_OK)
			{
				_currentTemplate->Clear();
				if (_multiplayer)
					SetMission(Glob.header.worldname, "", MPMissionsDir);
				else
					SetMission(Glob.header.worldname, "", MissionsDir);
				if (_map)
				{
					_map->_infoClick._type = signNone;
					_map->_infoClickCandidate._type = signNone;
					_map->_infoMove._type = signNone;
				}
				ShowButtons();
			}
			DisplayMapEditor::OnChildDestroyed(idd, exit);
			break;
		case IDD_MSG_EXITTEMPLATE:
			if (exit == IDC_OK)
			{
				Exit(IDC_CANCEL);
			}
			DisplayMapEditor::OnChildDestroyed(idd, exit);
			break;
		default:
			DisplayMapEditor::OnChildDestroyed(idd, exit);
			break;
	}
}

bool DisplayArcadeMap::CanDestroy()
{
	if (!DisplayMapEditor::CanDestroy()) return false;

	if (_exit == IDC_OK) 
		return _templateMission.IsConsistent(this, _multiplayer);
	else
		return true;
}

void DisplayArcadeMap::Destroy()
{
	DisplayMapEditor::Destroy();
//	GWorld->EnableDisplay(true);
}

#endif // #if _ENABLE_EDITOR
