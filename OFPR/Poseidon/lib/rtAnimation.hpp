#ifdef _MSC_VER
#pragma once
#endif

#ifndef _RT_ANIMATION_HPP
#define _RT_ANIMATION_HPP

//#include <class/smallArray.hpp>
#include "Shape.hpp"

#ifdef PREPROCESS_DOCUMENTATION
#include "networkObject.hpp"
#endif

struct AnimationRTPair
{
	//int sel;
	//float weight;
	char _sel; // matrix index 
	unsigned char _weight; // matrix used with given weigth
	#define WeightScale 128.0f
	#define InvWeightScale (1.0f/128)
	AnimationRTPair(){}
	AnimationRTPair( int s, float w ):_sel(s),_weight(toIntFloor(w*WeightScale)){}
	int GetSel() const {return _sel;}
	float GetWeight() const {return _weight*InvWeightScale;}
	void SetWeight( float w ) {_weight=toIntFloor(w*WeightScale);}
};

TypeIsSimple(AnimationRTPair);

#include <Es/Containers/smallArray.hpp>

//class AnimationRTWeight: public AutoArray<AnimationRTPair>
// max. 4 matrices per vertex - much less memory used

const int ARTWMaxSize = sizeof(AnimationRTPair)*4+sizeof(int);

class AnimationRTWeight: public VerySmallArray<AnimationRTPair,ARTWMaxSize>
{
	public:
	void Normalize();
	//bool GetSimple() const {return _simple;}
};

TypeIsMovable(AnimationRTWeight);

class AnimationRTWeights
{
	friend class AnimationRT;

	AutoArray<AnimationRTWeight> _data;
	FindArray<int> _selections; // which selections are already processed
	bool _needNormalize; // after adding selection normalize is necessary
	bool _isSimple; // each point has exactly one selection 

	public:
	// pre-processed weighting information
	AnimationRTWeights();
	AnimationRTWeights( Shape *shape ){Init(shape);}
	~AnimationRTWeights();

	void Init( Shape *shape );
	void AddSelection( Shape *shape, int selIndex, int matrixIndex );
	int	MatrixIndex( RStringB name ) const;
	void Normalize();
	bool IsSimple() const {return _isSimple;}
	const AnimationRTWeight &operator [] ( int i )const {return _data[i];}
	const AnimationRTWeight *Data() const {return _data.Data();}

};

class Skeleton;

class AnimationRTPhase // single phase
{
	friend class AnimationRT;
	AutoArray<Matrix4> _matrix; // what matrix

	public:
	AnimationRTPhase();
	~AnimationRTPhase();

	// Load returns animation time
	float Load( QIStream &in, Skeleton *skelet, int nSel, bool reversed=false );

	// skeleton included in f context
	void Reverse();
	void SerializeBin(SerializeBinStream &f);
};

TypeIsMovableZeroed(AnimationRTPhase);

struct WeightInfoName
{
	Link<LODShapeWithShadow> shape;
	Link<Skeleton> skeleton;
	WeightInfoName(){shape=NULL,skeleton=NULL;}
};

class WeightInfo: public RefCount
{
	WeightInfoName _name;
	AnimationRTWeights _data[MAX_LOD_LEVELS];
	//Ref<Skeleton> _skeleton;

	public:
	WeightInfo();
	WeightInfo( const WeightInfoName &name );

	const WeightInfoName &GetName() const {return _name;}

	AnimationRTWeights &operator [] (int i){return _data[i];}
	const AnimationRTWeights &operator [] (int i) const {return _data[i];}
	//int NewBone( RStringB name ) {return _skeleton->NewBone(name);}
	//int FindBone( RStringB name ) const {return _skeleton->FindBone(name);}
	//int NBones() const {return _skeleton->NBones();}
	//RStringB GetBone( int i ) const {return _skeleton->GetBone(i);}
};


template<>
struct BankTraits<WeightInfo>
{
	typedef const WeightInfoName &NameType;
	static int CompareNames( const WeightInfoName &n1, const WeightInfoName &n2 )
	{
		// compare pointers
		int d = (char *)n1.shape.GetTypeRef()-(char *)n2.shape.GetTypeRef();
		if (d) return d;
		d = (char *)n1.skeleton.GetTypeRef()-(char *)n2.skeleton.GetTypeRef();
		return d;
	}
	// default container is RefArray
	typedef RefArray<WeightInfo> ContainerType;
};

class Skeleton: public RefCountWithLinks
{
	RStringB _name;
	AutoArray<RStringB> _matrixNames; // matrix names should be shared by all animations

	public:
	Skeleton();
	Skeleton( RStringB name );
	RStringB GetName() const {return _name;}

	//int AddBone( int index, RStringB name );
	//int FindBone( RStringB name ) const;
	int NewBone( RStringB name );
	int FindBone( RStringB name ) const;
	int NBones() const {return _matrixNames.Size();}
	RStringB GetBone( int i ) const {return _matrixNames[i];}

	//! prepare shape to skeleton weighting 
	void Prepare(LODShape *lShape, WeightInfo &weights);
};

template<>
struct BankTraits<Skeleton>
{
	typedef const RStringB &NameType;
	static int CompareNames( NameType n1, NameType n2 )
	{
		return n1!=n2;
	}
	// default container is RefArray
	typedef RefArray<Skeleton> ContainerType;
};

extern BankArray<Skeleton> Skeletons;

typedef StaticArrayAuto<Matrix4> Matrix4Array;

#define MATRIX_4_ARRAY(name,size) AUTO_STATIC_ARRAY(Matrix4,name,size)


#include <Es/Memory/normalNew.hpp>
#include <Es/Memory/fastAlloc.hpp>

struct BlendAnimInfo
{
	//RStringB name;
	int matrixIndex;
	float factor;
};
TypeIsSimple(BlendAnimInfo)

struct AnimationRTName
{
	RStringB name;
	// while animation referencing given skeletion exists
	// skeleton must exist as well
	Ref<Skeleton> skeleton;
};

//! Real Time skeletal animation
class AnimationRT: public RefCount, public CLDLink
{
	AutoArray<AnimationRTPhase> _phases; //!< animation keyframes
	AutoArray<float> _phaseTimes; //!< for faster Find access
	Vector3 _step; //!< movement per single animation cycle
	AutoArray<RStringB> _selections; //!< which selections are used in this animation
	AnimationRTName _name; //!< animation name (usally file name)
	
	bool _looped; //!< looping flag
	bool _reversed; //! animation should be reversed during loading

	int _preloadCount; //!< animation should be always loaded
	int _loadCount; //!< load "reference" counting
	int _nPhases; //!< number of keyframes in full animation

	protected:
	void Find( int &prevIndex, int &nextIndex, float time ) const;
	static void ApplyMatricesComplex
	(
		const AnimationRTWeights &lWeights, LODShape *lShape, int level,
		const Matrix4Array &matrices
	);
	static void ApplyMatricesSimple
	(
		const AnimationRTWeights &lWeights, LODShape *lShape, int level,
		const Matrix4Array &matrices
	);
	static void AnimationRT::ApplyMatricesIdentity
	(
		LODShape *lShape, int level
	);

	private:
	AnimationRT( const AnimationRT &src ); // no copy
	void operator = ( const AnimationRT &src );

	public:
	AnimationRT();
	AnimationRT( const AnimationRTName &name, bool reversed=true );
	~AnimationRT();
	//! preload attribute is used to mark persistent animation
	//! same AnimationRT may be shared between several states with different
	//! preload attribute, it is therefore implemented as counter here
	void AddPreloadCount();
	void ReleasePreloadCount();
	void Load
	(
		QIStream &in, Skeleton *skelet,
		const char *name, bool reversed
		 // user friendly name
	);
	//! load full data (usually after AddLoadCount is called)
	void FullLoad(Skeleton *skelet, const char *file);
	//! release full data - keep only minimal representation
	void FullRelease();

	//! increase load count - make animation loaded
	void AddLoadCount();
	//! release load count - let animation be freed
	void ReleaseLoadCount();

	// ScopeLock compatible interface for AnimationRT
	void Lock() const
	{
		const_cast<AnimationRT *>(this)->AddLoadCount();
	}
	//! release load count - let animation be freed
	void Unlock() const
	{
		const_cast<AnimationRT *>(this)->ReleaseLoadCount();
	}

	void Load
	(
		Skeleton *skelet,
		const char *file, bool reversed=true
	);
	// skeleton included in f context
	void Reverse();
	void SerializeBin(SerializeBinStream &f);

	bool LoadOptimized(Skeleton *skelet, QIStream &f); // true if OK
	void SaveOptimized(Skeleton *skelet, QOStream &f); // true if OK

	// preparation of matrices
	static void TransformMatrix
	(
		Matrix4 &mat,
		const AnimationRTWeights &lWeights, Shape *shape,
		Matrix4Par trans,
		const BlendAnimInfo *blend, int nBlend, int index
	);
	static void TransformPoint
	(
		Vector3 &pos,
		const AnimationRTWeights &lWeights, Shape *shape,
		Matrix4Par trans,
		const BlendAnimInfo *blend, int nBlend, int index
	);
	static void CombineTransform
	(
		const WeightInfo &lWeights, LODShape *lShape, int level,
		Matrix4Array &matrices, Matrix4Par trans,
		const BlendAnimInfo *blend, int nBlend
	);
	void PrepareMatrices
	(
		Matrix4Array &matrices, float time, float factor
	) const;
	static void ApplyMatrices
	(
		const WeightInfo &lWeights, LODShape *lShape, int level,
		const Matrix4Array &matrices
	);
	static Vector3 ApplyMatricesPoint
	(
		const AnimationRTWeights &lWeights, LODShape *lShape, int level,
		const Matrix4Array &matrices, int pointIndex
	);

	// mesh animation
	void Apply
	(
		const WeightInfo &lWeights, LODShape *lShape, int level, float time
	) const;
	// point animation
	void Matrix
	(
		Matrix4 &mat, float time, const AnimationRTWeight &wgt
	) const;
	Vector3 Point
	(
		const WeightInfo &lWeights, LODShape *lShape, int level,
		float time, int index
	) const;
	const AnimationRTName &GetName() const {return _name;}
	const char *Name() const {return _name.name;}
	float GetStepLength() const {return _step.Z();}
	float GetStepLengthX() const {return _step.X();}
	Vector3Val GetStep() const {return _step;}
	int GetKeyframeCount() const {return _nPhases;}
	const Skeleton *GetSkeleton() const {return _name.skeleton;}

	//! PrepareSkeleton and SetLooped - obsolete
	void Prepare
	(
		LODShape *lShape, Skeleton *skelet, WeightInfo &weights, bool looped
	);
	//! set looping - may be called before or after loading
	void RemoveLoopFrame();
	void ForceMatrixOrientation
	(
		int matIndex, const Matrix3 &orient, float factor
	);
	void IntroduceStep();
	void IntroduceXStep();

	void SetLooped( bool looped );
	bool GetLooped() const {return _looped;}
	
	USE_FAST_ALLOCATOR
};

class AnimationRTLoad
{
	AnimationRT *_anim;


};
#include <Es/Memory/debugNew.hpp>

#endif
