#include "wpch.hpp"

#include "soundScene.hpp"
#include "global.hpp"
#include "paramFileExt.hpp"
#include "speaker.hpp"
#include <El/Common/perfLog.hpp>

#include "diagModes.hpp"
#include <Es/Containers/staticArray.hpp>
#include <Es/Algorithms/qsort.hpp>

SoundScene *GSoundScene;
AbstractSoundSystem *GSoundsys;

/*!
\patch 1.93 Date 8/26/2003 by Ondra
- Fixed: Crash when "variants" field in the voice config was empty. 
*/

SoundScene::SoundScene()
{
	int i;
	for( i=0; i<MaxEnvSounds; i++ ) _envSoundRenewed[i]=false;
	// init speakers
	const ParamEntry &cfg=Pars>>"CfgVoice";
	const ParamEntry &list=cfg>>"voices";
	for( i=0; i<list.GetSize(); i++ )
	{
		const ParamEntry &speaker=cfg>>RString(list[i]);
		Ref<BasicSpeaker> basic=new BasicSpeaker(speaker);
		const ParamEntry &variants=speaker>>"variants";
		for( int i=0; i<variants.GetSize(); i++ )
		{
			_speakers.Add(new Speaker(basic,variants[i]));
		}
	}
	{
		RString nameSpeaker = cfg >> "voicePlayer";
		const ParamEntry &speaker=cfg>>nameSpeaker;
		Ref<BasicSpeaker> basic=new BasicSpeaker(speaker);
		const ParamEntry &variants=speaker>>"variants";
    if (variants.GetSize() >= 1)
    {
		  _speakerPlayer = new Speaker(basic,variants[0]); 
    }
    else
    {
		  _speakerPlayer = new Speaker(basic,1.0f); 
    }
	}
	_earAccomodation=0.5;
}

SoundScene::~SoundScene()
{
}

AbstractWave *SoundScene::Open( const char *filename, bool is3D, bool accom )
{
	//ADD_COUNTER(wavOp,1);
	// TODO: more consitent speech channel preallocation
	bool prealloc=!is3D && !accom;
	AbstractWave *wave = GSoundsys->CreateWave(filename,is3D,prealloc);
	
	if( !wave ) return wave;
	if( is3D ) _all3DSounds.Add(wave);
	else _all2DSounds.Add(wave);
	wave->EnableAccomodation(accom);
	if( accom ) wave->SetAccomodation(_earAccomodation*_soundVolume);
	//Assert( VerifyLinks(wave) );
	return wave;

}
void SoundScene::AddSound( AbstractWave *wave )
{
	if( wave )
	{
		//Log("%f: Add global sound %s",Glob.time,wave->Name());
		_globalSounds.Add(wave);
		//Assert( wave->RefCounter()==1 );
		//Assert( VerifyLinks(wave) );
	}
}

void SoundScene::DeleteSound( AbstractWave *wave )
{
	if( wave )
	{
		//Log("%f: Delete global sound %s",Glob.time,wave->Name());
		_globalSounds.Delete(wave);
	}
}

struct SortSound
{
	AbstractWave *_wave;
	float _intensity;
	float _distance2;

	SortSound(){}
	SortSound( AbstractWave *wave, Vector3Val pos );
	AbstractWave *operator ->() const {return _wave;}
	operator AbstractWave *() const {return _wave;}
	bool LouderThan( float accom, float v ) const {return _intensity*accom>=v*_distance2;}
	// estimate loudness
	float Loudness() const
	{
		if( _distance2<=(_intensity*100) ) return 10.0f;
		else return (_intensity*1000)/_distance2;
	}
};

TypeIsSimple(SortSound);

SortSound::SortSound( AbstractWave *wave, Vector3Val pos )
:_wave(wave)
{
	_intensity=_wave->GetVolume();
	// no 2D sounds here
	_distance2=_wave->GetPosition().Distance2(pos);
	// TODO: corrent initial parameters
	//saturateMax(_distance2,0.4);
}

static int CompareSounds( const SortSound *w0, const SortSound *w1 )
{
	float diff=w0->_intensity*w1->_distance2-w1->_intensity*w0->_distance2;
	if( diff>0 ) return -1;
	if( diff<0 ) return +1;
	return 0;
}

class HistoryTracker
{ // Value type should be movable
	struct HistoryItem
	{
		float value;
		float howOld;
		ClassIsSimple(HistoryItem);
	};
	AutoArray<HistoryItem> _history;
	float _maxTime;
	int _maxItems;

	public:
	HistoryTracker( float maxTime, int maxItems );
	void Add( float value, float howOld=0 );
	void Evaluate
	(
		float &max, float &min, float &avg,
		float minOld=0, float maxOld=FLT_MAX
	);
	void SetLimits( float maxTime, int maxItems )
	{
		_maxTime=maxTime;
		_maxItems=maxItems;
		GuardLimits();
	}
	void AdvanceTime( float deltaT );
	void GuardLimits();
};

HistoryTracker::HistoryTracker( float maxTime, int maxItems )
{
	_maxTime=maxTime;
	_maxItems=maxItems;
}

void HistoryTracker::GuardLimits()
{
	while
	(
		_history.Size()>_maxItems
		|| _history.Size()>0 && _history[0].howOld>_maxTime
	)
	{
		_history.Delete(0);
	}
}

void HistoryTracker::AdvanceTime( float deltaT )
{
	for( int i=0; i<_history.Size(); i++ ) _history[i].howOld+=deltaT;
	GuardLimits();
}

void HistoryTracker::Add( float value, float howOld )
{
	HistoryItem item;
	item.value=value;
	item.howOld=howOld;
	_history.Add(item);
	GuardLimits();
}

void HistoryTracker::Evaluate
(
	float &max, float &min, float &avg,
	float minOld, float maxOld
)
{
	float lMax=FLT_MIN;
	float lMin=FLT_MAX;
	float sum=0;
	int n=0;
	for( int i=0; i<_history.Size(); i++ )
	{
		const HistoryItem &item=_history[i];
		if( item.howOld>maxOld || item.howOld<minOld ) continue;
		float value=item.value;
		sum+=value;
		n++;
		saturateMin(lMin,value);
		saturateMax(lMax,value);
	}
	min=lMin;
	max=lMax;
	avg=( n>0 ? sum/n : avg );
}

/*!
\patch 1.34 Date 12/03/2001 by Ondra
- Fixed: When radio volume was turned off in sound control panel,
messages were not transmitted al all.
\patch_internal 1.47 Date 3/11/2002 by Ondra
- Optimized: Inaudible sounds are muted in less distance from the camera.
\patch 1.79 Date 7/29/2002 by Ondra
- Improved: Compression of sound dynamic range less agressive.
*/

void SoundScene::AdvanceAll( float deltaT, bool paused )
{
	int i;
	//LogF("%6f: AdvanceAll",Glob.time-Time(0));
	for( i=0; i<_globalSounds.Size(); )
	{ // remove terminated global sounds
		AbstractWave *wave=_globalSounds[i];
		if( !wave || wave->IsTerminated() )
		{
			//LogF("Delete sound %s",(const char *)wave->Name());
			_globalSounds.Delete(i);
		}
		else i++;
	}
	_all3DSounds.Compact();
	_all2DSounds.Compact();
	Vector3Val pos = GSoundsys->GetListenerPosition();

	AUTO_STATIC_ARRAY(SortSound,sort,128);
	// sort sounds by intensity (as heared by the listener) 
	for( i=0; i<_all3DSounds.Size(); i++ )
	{
		AbstractWave *wave=_all3DSounds[i];
		Assert( wave );
		SortSound ss(wave,pos);
		if
		(
			( !paused || wave->GetSticky() ) &&
			ss.LouderThan(_earAccomodation*_soundVolume,1e-8) &&
			!wave->IsWaiting() && !wave->IsTerminated()
		)
		{
			sort.Add(ss);
		}
		else
		{
			wave->Stop();
			wave->Skip(deltaT); // waiting is also stopped
		}
	}
	// play only first N 3D sounds - all other should be stopped and advanced
	QSort(sort.Data(),sort.Size(),CompareSounds);

	int played=sort.Size();
	int maxPlayed=Glob.config.maxSounds;
	if( maxPlayed<16 ) maxPlayed=16;
	if( played>maxPlayed ) played=maxPlayed;
	//GlobalShowMessage(100,"Audible %d from %d, 2D %d",played,sort.Size(),_all2DSounds.Size());
	float totLoudness=0;
	int nLoud=0;
	// first stop all chaneles that should be muted
	for( i=played; i<sort.Size(); i++ )
	{
		AbstractWave *wave=sort[i];
		/*
		LogF
		(
			"  stop %s: volume %.5f, i %.5f, d2 %.5f",
			(const char *)wave->Name(),
			sort[i].Loudness(),sort[i]._intensity,sort[i]._distance2
		);
		*/
		wave->Stop();
		wave->Skip(deltaT);
	}
	for( i=0; i<played; i++ )
	{
		AbstractWave *wave=sort[i];
		/*
		LogF
		(
			"  wave %s: volume %.5f, i %.5f, d2 %.5f",
			(const char *)wave->Name(),
			sort[i].Loudness(),sort[i]._intensity,sort[i]._distance2
		);
		*/
		if( wave->AccomodationEnabled() )
		{
			wave->SetAccomodation(_earAccomodation*_soundVolume);
			float loudness=sort[i].Loudness();
			totLoudness+=loudness;
			nLoud++;
		}
		wave->Play();
		if( wave->IsStopped() )
		{ // there may be no soundsystem channels left
			wave->Skip(deltaT);
		}
	}

	// play all 2D sounds
	for( i=0; i<_all2DSounds.Size(); i++ )
	{
		AbstractWave *wave=_all2DSounds[i];
		if( wave->AccomodationEnabled() )
		{
			wave->SetAccomodation(_earAccomodation*_soundVolume);
			float loudness=wave->GetVolume()/Square(wave->Distance2D());
			totLoudness+=loudness;
		}
		if ( !paused || wave->GetSticky() )
		{
			//if (!wave->IsMuted())
			//{
				//LogF("  play 2D %s",(const char *)wave->Name());
				wave->Play();
			//}
			/*
			else
			{
				LogF("  muted 2D %s",(const char *)wave->Name());
			}
			*/
			wave->Advance(deltaT); // advance sounds that may be waiting
		}
		else
		{
			wave->Stop();
		}
	}

	if (!paused || _musicTrack && _musicTrack->GetSticky())
	{
		// musicTrack playback (Play/Stop) is controlled the same way
		// as any other 2D sound (musicTrack is listed in _all2DSounds)
		// simulate fade in/out
		if (Glob.time>=_musicTargetTime)
		{
			_musicVolume = _musicTargetVolume;
		}
		else
		{
			float timeRest = _musicTargetTime-Glob.time;
			float chSpeed = (_musicTargetVolume-_musicVolume)/timeRest;
			_musicVolume += deltaT*chSpeed;
		}
	}
	if (!paused)
	{
		// simulate fade in/out
		if (Glob.time>=_soundTargetTime)
		{
			_soundVolume = _soundTargetVolume;
		}
		else
		{
			float timeRest = _soundTargetTime-Glob.time;
			float chSpeed = (_soundTargetVolume-_soundVolume)/timeRest;
			_soundVolume += deltaT*chSpeed;
		}
	}
	if (_musicTrack)
	{
		_musicTrack->SetVolume
		(
			_musicInternalVolume*_musicVolume,_musicInternalFrequency
		);
	}
	#if 1
		// compress dynamics
		// total loudness is calculated
		// adjust volume to accomodate

		// aggregated loudness
		// scan environmental sounds
		for( i=0; i<MaxEnvSounds; i++ )
		{
			AbstractWave *wave=_envSounds[i];
			if( !wave ) continue;
			float loudness=wave->GetVolume()/Square(wave->Distance2D());
			totLoudness+=loudness;
		}

		static HistoryTracker history(2.5f,500);
		history.Add(totLoudness);

		float maxLoud,minLoud,avgLoud;
		history.AdvanceTime(deltaT);
		history.Evaluate(maxLoud,minLoud,avgLoud);
		
		float aggLoud = maxLoud;

		float compressedLoudness=floatMax(aggLoud,1e-20);

		//float cLoudnessWanted=0.4;
		// very loud volume should be still loud after compression
		// very soft volume should be still soft after compression
		// 10->2
		// 0.1->0.4

		float cLoudnessWanted = Interpolativ(aggLoud,0.01,10,0.1,2);

		float accLoudness=compressedLoudness*_earAccomodation*(1.0/cLoudnessWanted);
		float logAccLoudness=-1000;
		if( accLoudness>0 ) logAccLoudness=-log(accLoudness);
		saturate(logAccLoudness,-0.5f*deltaT,+0.1f*deltaT);
		float eaWanted=_earAccomodation*exp(logAccLoudness);
		
		#if _ENABLE_CHEATS
		if (CHECK_DIAG(DESound))
		{
			GlobalShowMessage
			(
				200,"Accom %.4f, abs vol %.4f, acommodated to %.4f, wanted %.4f",
				log(_earAccomodation),log(aggLoud),log(aggLoud*_earAccomodation),
				log(cLoudnessWanted)
			);
		}
		#endif



		const float maxEarAccomodation=4;
		const float minEarAccomodation=1.0/256;
		saturate(eaWanted,minEarAccomodation,maxEarAccomodation);
		_earAccomodation=eaWanted;

	#endif
}

AbstractWave *SoundScene::OpenAndPlay
(
	const char *filename, Vector3Par pos, Vector3Par speed,
	float volume, float frequency
)
{
	AbstractWave *wave=Open(filename);
	if( wave )
	{
		wave->SetVolume(volume,frequency);
		wave->SetPosition(pos,speed,true);
	}
	return wave;
}
AbstractWave *SoundScene::OpenAndPlayOnce
(
	const char *filename,
	Vector3Par pos, Vector3Par speed,
	float volume, float frequency
)
{
	AbstractWave *handle=Open(filename);
	if( handle )
	{
		handle->SetVolume(volume,frequency);
		handle->SetPosition(pos,speed,true);
		handle->Repeat(1);
	}
	return handle;
}

#include "scene.hpp"
#include "camera.hpp"
#include "engine.hpp"

/*!
\patch 1.28 Date 10/19/2001 by Ondra
- Fixed: no speed of sound delay on any sounds (bug since 1.27)
*/

void SoundScene::SimulateSpeedOfSound(AbstractWave *sound)
{
	float distance=sound->GetPosition().Distance(GScene->GetCamera()->Position());
	// sound will not start playing between frames
	// it is therefore more accurate to subtract half of expected frame duration
	// from wait time. This way we are effectively rounding to nearest frame instead
	// of rounding to next one
	float halfFrame = GEngine->GetAvgFrameDuration(4)*(0.001f*0.5f);
	float skip = -distance*InvSpeedOfSound + halfFrame;
	// avoid skipping sound start, only delay is allowed
	if (skip<0) sound->Skip(skip);
}

AbstractWave *SoundScene::OpenAndPlayOnce2D
(
	const char *filename,	float volume, float frequency, bool accomodate
)
{
	AbstractWave *handle=Open(filename,false,accomodate);
	if( handle )
	{
		handle->SetVolume(volume,frequency,true);
		handle->Repeat(1);
	}
	return handle;
}

AbstractWave *SoundScene::OpenAndPlay2D
(
	const char *filename,	float volume, float frequency, bool accomodate
)
{
	AbstractWave *handle=Open(filename,false,accomodate);
	if( handle )
	{
		handle->SetVolume(volume,frequency,true);
	}
	return handle;
}

void SoundScene::Reset()
{
	_earAccomodation=0.5;
	for( int i=0; i<MaxEnvSounds; i++ ) _envSounds[i].Free(),_envSoundRenewed[i]=false;
	_globalSounds.Clear();

	_musicVolume = 0.5; // middle value
	_musicTargetVolume = 0.5;
	_musicTrack.Free(); // remove musical track
	_musicTargetTime = TIME_MIN;


	_soundVolume = 1; // middle value
	_soundTargetVolume = 1;
	_soundTargetTime = TIME_MIN;
}

const float EnvMagicConstant = 30;

float SoundScene::GetMusicVolume() const
{
	return _musicTargetVolume;
}

void SoundScene::SetMusicVolume(float vol, float time)
{
	_musicTargetVolume = vol;
	_musicTargetTime = Glob.time+time;
	if (time<=0.01)
	{
		_musicVolume = _musicTargetVolume;
	}
}

float SoundScene::GetSoundVolume() const
{
	return _soundTargetVolume;
}

void SoundScene::SetSoundVolume(float vol, float time)
{
	_soundTargetVolume = vol;
	_soundTargetTime = Glob.time+time;
	if (time<=0.01)
	{
		_soundVolume = _soundTargetVolume;
	}
}

void SoundScene::StartMusicTrack(const SoundPars &pars, float from)
{
	AbstractWave *wave = GSoundScene->OpenAndPlayOnce2D
	(
		pars.name,pars.vol,pars.freq,false
	);
	if (!wave) return;
	wave->SetKind(WaveMusic);
	// register is at musical track
	_musicTrack=wave;
	_musicInternalVolume = pars.vol;
	_musicInternalFrequency = pars.freq;
	wave->Skip(from);
	//LogF("Wave %s started",(const char *)wave->Name());
	//LogF("  pars.vol %g",pars.vol);
	//LogF("  Wave::GetVolume %g",wave->GetVolume());
}

void SoundScene::StopMusicTrack()
{
	// TODO: fade out
	_musicTrack.Free();
}

/*!
\patch 1.43 Date 1/31/2002 by Ondra
- Fixed: Long environmental sounds streaming was not working.
*/

void SoundScene::SetEnvSound( const char *name, float volume )
{
	// see if sound can be reused
	if( !name || !*name ) return; // no sound
	char lowName[256];
	strcpy(lowName,name);
	strlwr(lowName);
	int i;
	for( i=0; i<MaxEnvSounds; i++ )
	{
		AbstractWave *snd=_envSounds[i];
		if( snd && !strcmp(snd->Name(),lowName) )
		{
			// sound matched
			// set volume and return
			snd->SetVolume(volume*EnvMagicConstant);
			snd->SetAccomodation(_earAccomodation*_soundVolume);
			// call play, this ensures streaming sounds are streaming
			snd->Play();
			_envSoundRenewed[i]=true;
			//Log("Renewed sound %s (%f) at %d",name,volume,i);
			return;
		}
	}
	// no sound found, we have to set new
	// there must be one free slot
	for( i=0; i<MaxEnvSounds; i++ ) if( !_envSounds[i] )
	{
		AbstractWave *snd = GSoundsys->CreateWave(lowName,false);
		if( snd )
		{
			snd->SetAccomodation(_earAccomodation*_soundVolume);
			snd->SetVolume(volume*EnvMagicConstant);
			snd->Play();
			_envSounds[i]=snd;
			_envSoundRenewed[i]=true;
			//Log("New sound %s (%f) at %d",name,volume,i);
		}
		break;
	}
}

void SoundScene::AdvanceEnvSounds()
{
	for( int i=0; i<MaxEnvSounds; i++ )
	{
		// sounds that were not renewed die
		if( !_envSoundRenewed[i] && _envSounds[i] )
		{
			_envSounds[i].Free();
		}
		_envSoundRenewed[i]=false;
	}
}


AbstractWave *SoundScene::Say( int speaker, RString id )
{
	//ADD_COUNTER(rcSay,1);
	if (speaker < 0)
	{
		Assert(_speakerPlayer);
		return _speakerPlayer->Say(id);
	}
	speaker%=_speakers.Size();
	Assert( _speakers[speaker] );
	return _speakers[speaker]->Say(id);
}

AbstractWave *SoundScene::SayPause( float duration )
{
	AbstractWave *wave = GSoundsys->CreateEmptyWave(duration);
	if (wave)
	{
		_all2DSounds.Add(wave);
	}
	return wave;
}
