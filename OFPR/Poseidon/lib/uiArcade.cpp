// Implementation of mission editor dialogs
#include "wpch.hpp"
#include "uiMap.hpp"
#include <Es/Common/win.h>
#include "dikCodes.h"

#include "resincl.hpp"
extern class ParamFile Res;
#include "keyInput.hpp"
#include <El/QStream/QBStream.hpp>
#include <Es/Algorithms/qsort.hpp>

//#include "landscape.hpp"
#include <El/Common/randomGen.hpp>
#include "camEffects.hpp"
#include "titEffects.hpp"

#include <El/Evaluator/express.hpp>
#include "house.hpp"

#include "stringtableExt.hpp"

/*!
\file
Implementation file for particular displays (mission editor).
*/

///////////////////////////////////////////////////////////////////////////////
// Arcade Map subdisplays

#if _ENABLE_EDITOR

void CStaticAzimut::OnDraw(float alpha)
{
	CStatic::OnDraw(alpha);

	CEdit *edit = dynamic_cast<CEdit *>(_parent->GetCtrl(_idcEdit));
	Assert(edit);
	if (!edit) return;
	float azimut = edit->GetText() ? atof(edit->GetText()) : 0;
	azimut = HDegree(azimut);
	
	PackedColor color(Color(0.08, 0.08, 0.12, alpha));
	float xc = _x + 0.5 * _w;
	float zc = _y + 0.5 * _h;
	const float r1 = 0.32;
	const float r2 = 0.26;
	const float diff = 0.08;
	float x1 = xc + sin(azimut) * r1 * _w;
	float z1 = zc - cos(azimut) * r1 * _h;
	float x2 = xc + sin(azimut - diff) * r2 * _w;
	float z2 = zc - cos(azimut - diff) * r2 * _h;
	float x3 = xc + sin(azimut + diff) * r2 * _w;
	float z3 = zc - cos(azimut + diff) * r2 * _h;

	float w = GLOB_ENGINE->Width2D();
	float h = GLOB_ENGINE->Height2D();

	int xx1 = toIntFloor(x1 * w + 0.5);
	int zz1 = toIntFloor(z1 * h + 0.5);
	int xx2 = toIntFloor(x2 * w + 0.5);
	int zz2 = toIntFloor(z2 * h + 0.5);
	int xx3 = toIntFloor(x3 * w + 0.5);
	int zz3 = toIntFloor(z3 * h + 0.5);

	GLOB_ENGINE->DrawLine(Line2DPixel(xx1, zz1, xx2, zz2), color, color);
	GLOB_ENGINE->DrawLine(Line2DPixel(xx2, zz2, xx3, zz3), color, color);
	GLOB_ENGINE->DrawLine(Line2DPixel(xx3, zz3, xx1, zz1), color, color);
}

void CStaticAzimut::OnLButtonClick(float x, float y)
{
	CStatic::OnLButtonClick(x, y);

	CEdit *edit = dynamic_cast<CEdit *>(_parent->GetCtrl(_idcEdit));
	Assert(edit);
	if (!edit) return;

	float xc = _x + 0.5 * _w;
	float yc = _y + 0.5 * _h;
	float azimut = atan2(x - xc, -y + yc) * 180.0 / H_PI;
	int roundAzimut = 5 * toInt(0.2 * azimut);
	if (roundAzimut < 0) roundAzimut += 360;

	char buffer[16];
	sprintf(buffer, "%d", roundAzimut);
	edit->SetText(buffer);
}

void DisplayArcadeUnit::OnComboSelChanged(int idc, int curSel)
{
	switch (idc)
	{
		case IDC_ARCUNIT_SIDE:
			{
				CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_SIDE));
				int side = combo ? combo->GetValue(curSel) : TWest;
				bool enable = side == TWest || side == TEast || side == TGuerrila || side == TCivilian;
				IControl *ctrl;
				ctrl = GetCtrl(IDC_ARCUNIT_RANK);
				if (ctrl) ctrl->ShowCtrl(enable);
				ctrl = GetCtrl(IDC_ARCUNIT_CTRL);
				if (ctrl) ctrl->ShowCtrl(enable);

				RString vehicle;
				combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_VEHICLE));
				if (combo)
				{
					int i = combo->GetCurSel();
					if (i < 0)
						vehicle = "";
					else
						vehicle = combo->GetData(i);
				}
				combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_CLASS));
				UpdateClasses(combo, vehicle);
			}
			// continue
		case IDC_ARCUNIT_CLASS:
			{
				CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_VEHICLE));
				if (combo)
				{
					int i = combo->GetCurSel();
					RString vehicle;
					if (i < 0)
						vehicle = "";
					else
						vehicle = combo->GetData(i);
					UpdateVehicles(combo, vehicle);
				}
			}
			break;
		case IDC_ARCUNIT_VEHICLE:
			{
				CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_CTRL));
				if (combo)
				{
					UpdatePlayer(combo, (ArcadeUnitPlayer)combo->GetValue(combo->GetCurSel()));
				}
			}
			break;
		case IDC_ARCUNIT_CTRL:
			{
				CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_SIDE));
				if (combo)
				{
					if (curSel != 0 && combo->GetSize() == 4)
					{
						// Playable
						// combo content WEST, EAST, GUERRILA, CIVILIAN - OK
						break;
					}
					if (curSel == 0 && combo->GetSize() == 6)
					{
						// non Playable
						// combo content WEST, EAST, GUERRILA, CIVILIAN, LOGIC, EMPTY - OK
						break;
					}
					int sel = combo->GetCurSel();
					combo->ClearStrings();
					int index;
					index = combo->AddString(LocalizeString(IDS_WEST));
					combo->SetValue(index, TWest);
					index = combo->AddString(LocalizeString(IDS_EAST));
					combo->SetValue(index, TEast);
					index = combo->AddString(LocalizeString(IDS_GUERRILA));
					combo->SetValue(index, TGuerrila);
					index = combo->AddString(LocalizeString(IDS_CIVILIAN));
					combo->SetValue(index, TCivilian);
					if (curSel == 0)
					{
						index = combo->AddString(LocalizeString(IDS_LOGIC));
						combo->SetValue(index, TLogic);
						index = combo->AddString(LocalizeString(IDS_EMPTY));
						combo->SetValue(index, TEmpty);
					}
					if (sel >= combo->GetSize()) sel = 0;
					combo->SetCurSel(sel);
				}
			}
			break;
	}
	Display::OnComboSelChanged(idc, curSel);
}

	// note: if "men" class is present, we want it to be first
	// other classes should be sorted alphabetically
static int CmpClass(const RStringB *a, const RStringB *b)
{
	if (!strcmpi(*a,*b)) return 0;
	if (!strcmpi(*a,"men")) return -1;
	if (!strcmpi(*b,"men")) return +1;
	return strcmpi(*a,*b);
}

/*!
\patch 1.82 Date 8/22/2002 by Ondra
- Improved: Editor: Vehicle classes (Armored, Car, ...) are now fully dynamic.
Any addon maker can now introduce his own classes.
\patch 1.82 Date 8/22/2002 by Ondra
- Fixed: Some empty objects could also be inserted as Civilian, using group slots
and causing confusion inside of the editor.
*/


void DisplayArcadeUnit::UpdateClasses(CCombo *combo, const char *vehicle)
{
	CCombo *combo2 = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_SIDE));
	if (!combo2) return;
	int side = combo2->GetValue(combo2->GetCurSel());

	const ParamEntry &clsVeh = Pars >> "CfgVehicles";
	int n = clsVeh.GetEntryCount();
	FindArrayRStringBCI vehClasses;
	const ParamClass *manCls = static_cast<const ParamClass *>(clsVeh.FindEntry("Man"));
	// scan all CfgVehicles for vehicleClass list
	for (int i=0; i<n; i++)
	{
		const ParamEntry &vehEntry = clsVeh.GetEntry(i);
		if (!vehEntry.IsClass()) continue;
		int scope = vehEntry >> "scope";
		if (scope != 2) continue;
		int vehSide = vehEntry >> "side";
		if (side == TEmpty)
		{
			if (vehSide == TLogic) continue;
			const ParamClass *vehCls = static_cast<const ParamClass *>(&vehEntry);
			if (vehCls->IsDerivedFrom(*manCls)) continue;
		}
		else
		{
			if (side != vehSide) continue;
			// note: some objects may be inserted only as empty
			bool hasDriver = vehEntry>>"hasDriver";
			bool hasGunner = vehEntry>>"hasGunner";
			bool hasCommander = vehEntry>>"hasCommander";
			if (!hasDriver && !hasGunner && !hasCommander)
			{
				// simulation should be static or thing
				RStringB sim = vehEntry>>"simulation";
				#if _ENABLE_REPORT
				if
				(
					strcmpi(sim,"house") && strcmpi(sim,"thing") &&
					strcmpi(sim,"fire") && strcmpi(sim,"fountain") &&
					strcmpi(sim,"flagcarrier") && strcmpi(sim,"church")
				)
				{
					RptF
					(
						"Empty vehicle %s - simulation %s (this simulation is considered non-empty only)",
						(const char *)vehEntry.GetName(),(const char *)sim
					);
				}
				#endif
				continue;
			}
		}
		RStringB name = vehEntry >> "vehicleClass";
		if (name.GetLength() == 0) continue;
		int index = vehClasses.FindKey(name);
		if (index<0) index = vehClasses.AddUnique(name);
	}
	
	RStringB vehicleClass;
	if (vehicle && *vehicle)
	{
		vehicleClass = Pars >> "CfgVehicles" >> vehicle >> "vehicleClass";
	}

	// note: if "men" class is present, we want it to be first
	// other classes should be sorted alphabetically
	QSort(vehClasses.Data(),vehClasses.Size(),CmpClass);

	combo->ClearStrings();
	int sel = 0;
	n = vehClasses.Size();
	for (int i=0; i<n; i++)
	{
		//if (classCounts[i] == 0) continue;
		RStringB name = vehClasses[i];
		int index = combo->AddString(name);
		combo->SetData(index, name);
		if (vehicleClass == name) sel = index;
	}
	combo->SetCurSel(sel);
}

/*!
\patch 1.66 Date 6/6/2002 by Jirka
- Improved: Mission editor - sort vehicle types in Insert unit dialog
*/

void DisplayArcadeUnit::UpdateVehicles(CCombo *combo, const char *vehicle)
{
	bool isVehicle = vehicle && *vehicle;

	CCombo *combo2 = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_SIDE));
	Assert(combo2);
	int side = combo2->GetValue(combo2->GetCurSel());

	combo2 = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_CLASS));
	Assert(combo2);
	int sel2 = combo2->GetCurSel();
	if (sel2 < 0) return;
	RString vehicleClass = combo2->GetData(sel2);

	combo->ClearStrings();
	const ParamClass *cls = Pars.GetClass("CfgVehicles");
	RString firstVehicle;
	for (int i=0; i<cls->GetEntryCount(); i++)
	{
		const ParamEntry &vehEntry = cls->GetEntry(i);
		const ParamClass *vehCls = dynamic_cast<const ParamClass *>(&vehEntry);
		if (!vehCls) continue;
		int scope = vehEntry >> "scope";
		if (scope != 2) continue;
		int vehSide = vehEntry >> "side";
		if (side == TEmpty)
		{
			if (vehSide == TLogic) continue;
			if (vehCls->IsDerivedFrom(*cls->GetClass("Man"))) continue;
		}
		else if (side != vehSide) continue;

		RString name = vehEntry >> "vehicleClass";
		if ( name.GetLength() == 0) continue;
		if (strcmp(name, vehicleClass) != 0) continue;

		RString displayName = vehEntry >> "displayName";
		int index = combo->AddString(displayName);
		combo->SetData(index, vehEntry.GetName());
		
		if (firstVehicle.GetLength() == 0)
			firstVehicle = vehEntry.GetName();
	}
	combo->SortItems();

	int sel = -1;
	if (isVehicle)
		for (int i=0; i<combo->GetSize(); i++)
		{
			if (strcmp(combo->GetData(i), vehicle) == 0)
			{
				sel = i;
				break;
			}
		}
	if (sel < 0 && firstVehicle.GetLength() > 0)
		for (int i=0; i<combo->GetSize(); i++)
		{
			if (strcmp(combo->GetData(i), firstVehicle) == 0)
			{
				sel = i;
				break;
			}
		}
	saturateMax(sel, 0);
	combo->SetCurSel(sel);
}

void DisplayArcadeUnit::UpdatePlayer(CCombo *combo, ArcadeUnitPlayer player)
{
	combo->ClearStrings();
	int index = combo->AddString(LocalizeString(IDS_NONPLAYABLE));
	combo->SetValue(index, APNonplayable);

	CCombo *combo2 = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_SIDE));
	if (!combo2) return;
	int side = combo2->GetValue(combo2->GetCurSel());
	if (side != TWest && side != TEast && side != TGuerrila && side != TCivilian)
	{
		combo->SetCurSel(0);
		return;
	}

	// West, East, Guerrila, Civilian
	combo2 = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_VEHICLE));
	if (!combo2) return;
	int i = combo2->GetCurSel();
	if (i < 0) return;
	VehicleType *type = dynamic_cast<VehicleType *>(VehicleTypes.New(combo2->GetData(i)));
	if( !type )
	{
		Fail("Non-AI type");
		return;
	}
	
	int sel = 0;
	bool commander = type->HasCommander();
	bool gunner = type->HasGunner();
	bool driver = type->HasDriver();
	bool air = type->IsKindOf(GWorld->Preloaded(VTypeAir));

	// find nearest valid position
	if (!commander)
	{
		if (!gunner)
			switch (player)
			{
			case APPlayerDriver:
			case APPlayerGunner:
				player = APPlayerCommander; break;
			case APPlayableC:
			case APPlayableD:
			case APPlayableG:
			case APPlayableCD:
			case APPlayableCG:
			case APPlayableDG:
				player = APPlayableCDG; break;
			}
		else
			switch (player)
			{
			case APPlayerCommander:
				player = APPlayerDriver; break;
			case APPlayableC:
				player = APPlayableD; break;
			case APPlayableCD:
				player = APPlayableD; break;
			case APPlayableCG:
				player = APPlayableG; break;
			case APPlayableCDG:
				player = APPlayableDG; break;
			}
	}
	else if (!gunner)
	{
		switch (player)
		{
		case APPlayerGunner:
			player = APPlayerDriver; break;
		case APPlayableG:
			player = APPlayableD; break;
		case APPlayableCG:
			player = APPlayableC; break;
		case APPlayableDG:
			player = APPlayableD; break;
		case APPlayableCDG:
			player = APPlayableCG; break;
		}
	}
	if (!driver && gunner)
	{
		if (player!=APNonplayable)
		{
			switch(player)
			{
				case APPlayerDriver:
				case APPlayerGunner:
				case APPlayerCommander:
					player = APPlayerGunner; break;
				default:
					player = APPlayableG; break;
			}
		}
	}

	if (commander)
	{
		// Player as commander
		index = combo->AddString(LocalizeString(IDS_PLAYER_COMMANDER));
		combo->SetValue(index, APPlayerCommander);
		if (player == APPlayerCommander) sel = index;
		// Player as pilot / driver
		if (driver)
		{
			if (air)
				index = combo->AddString(LocalizeString(IDS_PLAYER_PILOT));
			else
				index = combo->AddString(LocalizeString(IDS_PLAYER_DRIVER));
			combo->SetValue(index, APPlayerDriver);
			if (player == APPlayerDriver) sel = index;
		}
		// Player as gunner
		if (gunner)
		{
			index = combo->AddString(LocalizeString(IDS_PLAYER_GUNNER));
			combo->SetValue(index, APPlayerGunner);
			if (player == APPlayerGunner) sel = index;
		}
		// Playable as commander
		index = combo->AddString(LocalizeString(IDS_PLAYABLE_C));
		combo->SetValue(index, APPlayableC);
		if (player == APPlayableC) sel = index;
		if (driver)
		{
			// Playable as pilot / driver
			if (air)
				index = combo->AddString(LocalizeString(IDS_PLAYABLE_P));
			else
				index = combo->AddString(LocalizeString(IDS_PLAYABLE_D));
			combo->SetValue(index, APPlayableD);
			if (player == APPlayableD) sel = index;
		}
		// Playable as gunner
		if (gunner)
		{
			index = combo->AddString(LocalizeString(IDS_PLAYABLE_G));
			combo->SetValue(index, APPlayableG);
			if (player == APPlayableG) sel = index;
		}
		if (driver)
		{
			// Playable as commander and pilot / driver
			if (air)
				index = combo->AddString(LocalizeString(IDS_PLAYABLE_CP));
			else
				index = combo->AddString(LocalizeString(IDS_PLAYABLE_CD));
			combo->SetValue(index, APPlayableCD);
			if (player == APPlayableCD) sel = index;
		}
		if (gunner)
		{
			// Playable as commander and gunner
			index = combo->AddString(LocalizeString(IDS_PLAYABLE_CG));
			combo->SetValue(index, APPlayableCG);
			if (player == APPlayableCG) sel = index;
			// Playable as pilot / driver and gunner
			if (driver)
			{
				if (air)
					index = combo->AddString(LocalizeString(IDS_PLAYABLE_PG));
				else
					index = combo->AddString(LocalizeString(IDS_PLAYABLE_DG));
				combo->SetValue(index, APPlayableDG);
				if (player == APPlayableDG) sel = index;
				// Playable as commander, pilot / driver and gunner
				if (air)
					index = combo->AddString(LocalizeString(IDS_PLAYABLE_CPG));
				else
					index = combo->AddString(LocalizeString(IDS_PLAYABLE_CDG));
				combo->SetValue(index, APPlayableCDG);	// all available
				if (player == APPlayableCDG) sel = index;
			}
		}
		else
		{
			combo->SetValue(index, APPlayableCDG);	// all available
			if (player == APPlayableCDG) sel = index;
		}
	}
	else if (gunner)
	{
		// Player as pilot / driver
		if (driver)
		{
			if (air)
				index = combo->AddString(LocalizeString(IDS_PLAYER_PILOT));
			else
				index = combo->AddString(LocalizeString(IDS_PLAYER_DRIVER));
			combo->SetValue(index, APPlayerDriver);
			if (player == APPlayerDriver) sel = index;
		}
		// Player as gunner
		index = combo->AddString(LocalizeString(IDS_PLAYER_GUNNER));
		combo->SetValue(index, APPlayerGunner);
		if (player == APPlayerGunner) sel = index;
		if (driver)
		{
			// Playable as pilot / driver
			if (air)
				index = combo->AddString(LocalizeString(IDS_PLAYABLE_P));
			else
				index = combo->AddString(LocalizeString(IDS_PLAYABLE_D));
			combo->SetValue(index, APPlayableD);
			if (player == APPlayableD) sel = index;
		}
		// Playable as gunner
		index = combo->AddString(LocalizeString(IDS_PLAYABLE_G));
		combo->SetValue(index, APPlayableG);
		if (player == APPlayableG) sel = index;
		// Playable as pilot / driver and gunner
		if (driver)
		{
			if (air)
				index = combo->AddString(LocalizeString(IDS_PLAYABLE_PG));
			else
				index = combo->AddString(LocalizeString(IDS_PLAYABLE_DG));
			combo->SetValue(index, APPlayableCDG);	// all available
			if (player == APPlayableCDG) sel = index;
		}
	}
	else
	{
		// Player
		index = combo->AddString(LocalizeString(IDS_PLAYER));
		combo->SetValue(index, APPlayerCommander);
		if (player == APPlayerCommander) sel = index;
		// Playable
		index = combo->AddString(LocalizeString(IDS_PLAYABLE));
		combo->SetValue(index, APPlayableCDG);	// all available
		if (player == APPlayableCDG) sel = index;
	}

	combo->SetCurSel(sel);
}

Control *DisplayArcadeUnit::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
	case IDC_ARCUNIT_TITLE:
		{
			CStatic *text = new CStatic(this, idc, cls);
			if (_index < 0)
				text->SetText(LocalizeString(IDS_ARCUNIT_TITLE2));
			else
				text->SetText(LocalizeString(IDS_ARCUNIT_TITLE4));
			return text;
		}
	case IDC_ARCUNIT_AZIMUT_PICTURE:
		return new CStaticAzimut(this, idc, cls, IDC_ARCUNIT_AZIMUT);
	case IDC_ARCUNIT_SIDE:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			int index, sel = 0;
			index = combo->AddString(LocalizeString(IDS_WEST));
			combo->SetValue(index, TWest);
			if (_unit.side == TWest) sel = index;
			index = combo->AddString(LocalizeString(IDS_EAST));
			combo->SetValue(index, TEast);
			if (_unit.side == TEast) sel = index;
			index = combo->AddString(LocalizeString(IDS_GUERRILA));
			combo->SetValue(index, TGuerrila);
			if (_unit.side == TGuerrila) sel = index;
			index = combo->AddString(LocalizeString(IDS_CIVILIAN));
			combo->SetValue(index, TCivilian);
			if (_unit.side == TCivilian) sel = index;
			if (_unit.player == APNonplayable)
			{
				index = combo->AddString(LocalizeString(IDS_LOGIC));
				combo->SetValue(index, TLogic);
				if (_unit.side == TLogic) sel = index;
				index = combo->AddString(LocalizeString(IDS_EMPTY));
				combo->SetValue(index, TEmpty);
				if (_unit.side == TEmpty) sel = index;
			}
			combo->SetCurSel(sel);
			if (_index >= 0)
			{
				// cannot change side when edited
				combo->EnableCtrl(false);
			}
			return combo;
		}
	case IDC_ARCUNIT_CLASS:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			UpdateClasses(combo, _unit.vehicle);
			return combo;
		}
	case IDC_ARCUNIT_VEHICLE:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			UpdateVehicles(combo, _unit.vehicle);
			return combo;
		}
	case IDC_ARCUNIT_TEXT:
		{
			CEdit *text = new CEdit(this, idc, cls);
			text->SetText(_unit.name);
			return text;
		}
	case IDC_ARCUNIT_INIT:
		{
			CEdit *text = new CEdit(this, idc, cls);
			text->SetText(_unit.init);
			return text;
		}
	case IDC_ARCUNIT_RANK:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			for (int i=RankPrivate; i<=RankColonel; i++)
			{
				RString rank = LocalizeString(IDS_PRIVATE + i);
				combo->AddString(rank);
			}
			combo->SetCurSel(_unit.rank - RankPrivate);
			if (_unit.side == TEmpty || _unit.side == TLogic)
			{
				combo->SetCurSel(0);
				combo->ShowCtrl(false);
			}
			return combo;
		}
	case IDC_ARCUNIT_AZIMUT:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			char buffer[256];
			sprintf(buffer, "%g", _unit.azimut);
			edit->SetText(buffer);
			return edit;
		}
	case IDC_ARCUNIT_SPECIAL:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			combo->AddString(LocalizeString(IDS_SPECIAL_NONE));
			combo->AddString(LocalizeString(IDS_SPECIAL_CARGO));
			combo->AddString(LocalizeString(IDS_SPECIAL_FLYING));
			combo->AddString(LocalizeString(IDS_SPECIAL_FORM));
			combo->SetCurSel(_unit.special);
			return combo;
		}
	case IDC_ARCUNIT_CTRL:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			UpdatePlayer(combo, _unit.player);
			return combo;
		}
	case IDC_ARCUNIT_AGE:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			for (int i=0; i<AAN; i++)
				combo->AddString(LocalizeString(IDS_AGE_ACTUAL + i));
			combo->SetCurSel(_unit.age);
			return combo;
		}
	case IDC_ARCUNIT_SKILL:
		{
			CSlider *slider = new CSlider(this, idc, cls);
			slider->SetRange(0.2, 1);
			slider->SetSpeed(0.01, 0.1);
			slider->SetThumbPos(_unit.skill);
			return slider;
		}
	case IDC_ARCUNIT_HEALTH:
		{
			CSlider *slider = new CSlider(this, idc, cls);
			slider->SetRange(0, 1);
			slider->SetSpeed(0.01, 0.1);
			slider->SetThumbPos(_unit.health);
			return slider;
		}
	case IDC_ARCUNIT_FUEL:
		{
			CSlider *slider = new CSlider(this, idc, cls);
			slider->SetRange(0, 1);
			slider->SetSpeed(0.01, 0.1);
			slider->SetThumbPos(_unit.fuel);
			return slider;
		}
	case IDC_ARCUNIT_AMMO:
		{
			CSlider *slider = new CSlider(this, idc, cls);
			slider->SetRange(0, 1);
			slider->SetSpeed(0.01, 0.1);
			slider->SetThumbPos(_unit.ammo);
			return slider;
		}
	case IDC_ARCUNIT_PRESENCE:
		{
			CSlider *slider = new CSlider(this, idc, cls);
			slider->SetRange(0, 1);
			slider->SetSpeed(0.01, 0.1);
			slider->SetThumbPos(_unit.presence);
			return slider;
		}
	case IDC_ARCUNIT_PRESENCE_COND:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			edit->SetText(_unit.presenceCondition);
			return edit;
		}
	case IDC_ARCUNIT_PLACE:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			char buffer[256];
			sprintf(buffer, "%g", _unit.placement);
			edit->SetText(buffer);
			return edit;
		}
	case IDC_ARCUNIT_LOCK:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			int sel = 1; // default
			int index = combo->AddString(LocalizeString(IDS_VEHICLE_UNLOCKED));
			combo->SetValue(index, LSUnlocked);
			if (_unit.lock == LSUnlocked) sel = index;
			index = combo->AddString(LocalizeString(IDS_VEHICLE_DEFAULT));
			combo->SetValue(index, LSDefault);
			if (_unit.lock == LSDefault) sel = index;
			index = combo->AddString(LocalizeString(IDS_VEHICLE_LOCKED));
			combo->SetValue(index, LSLocked);
			if (_unit.lock == LSLocked) sel = index;
			combo->SetCurSel(sel);
			return combo;
		}
	default:
		return Display::OnCreateCtrl(type, idc, cls);
	}
}

bool DisplayArcadeUnit::CanDestroy()
{
	if (!Display::CanDestroy()) return false;

	if (_exit == IDC_OK)
	{
		CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCUNIT_TEXT));
		if (edit)
		{
			RString text = edit->GetText();
			if (text.GetLength() > 0)
			{
				if (!GWorld->GetGameState()->IdtfGoodName(text))
				{
					CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_VEH_NAME));
					return false;
				}
				// check if text is not used
				for (int i=0; i<_template->groups.Size(); i++)
				{
					ArcadeGroupInfo &gInfo = _template->groups[i];
					for (int j=0; j<gInfo.units.Size(); j++)
					{
						if (i == _indexGroup && j == _index) continue;
						if (stricmp(text, gInfo.units[j].name) == 0)
						{
							CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_VEH_TEXT_USED));
							return false;
						}
					}
					for (int j=0; j<gInfo.sensors.Size(); j++)
					{
						if (stricmp(text, gInfo.sensors[j].name) == 0)
						{
							CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_VEH_TEXT_USED));
							return false;
						}
					}
				}
				for (int j=0; j<_template->emptyVehicles.Size(); j++)
				{
					if (_indexGroup == -1 && j == _index) continue;
					if (stricmp(text, _template->emptyVehicles[j].name) == 0)
					{
						CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_VEH_TEXT_USED));
						return false;
					}
				}
				for (int j=0; j<_template->sensors.Size(); j++)
				{
					if (stricmp(text, _template->sensors[j].name) == 0)
					{
						CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_VEH_TEXT_USED));
						return false;
					}
				}
			}
		}

		GameState *gstate = GWorld->GetGameState();

		edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCUNIT_INIT));
		if (edit)
		{
			if (!gstate->CheckExecute(edit->GetText()))
			{
				FocusCtrl(IDC_ARCUNIT_INIT);
				CreateMsgBox(MB_BUTTON_OK, gstate->GetLastErrorText());
				edit->SetCaretPos(gstate->GetLastErrorPos());
				return false;
			}
		}

		edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCUNIT_PRESENCE_COND));
		if (edit)
		{
			if (!gstate->CheckEvaluateBool(edit->GetText()))
			{
				FocusCtrl(IDC_ARCUNIT_PRESENCE_COND);
				CreateMsgBox(MB_BUTTON_OK, gstate->GetLastErrorText());
				edit->SetCaretPos(gstate->GetLastErrorPos());
				return false;
			}
		}

		CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_VEHICLE));
		if (combo)
		{
			if (combo->GetCurSel() < 0)
			{
				CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_NO_VEH_SELECT));
				return false;
			}
		}
	}
	return true;
}

void DisplayArcadeUnit::Destroy()
{
	Display::Destroy();
	
	if (_exit != IDC_OK) return;

	CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_SIDE));
	if (combo)
		_unit.side = (TargetSide)combo->GetValue(combo->GetCurSel());
	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_CTRL));
	if (combo)
	{
		_unit.player = (ArcadeUnitPlayer)combo->GetValue(combo->GetCurSel());
		if (_unit.side == TEmpty || _unit.side == TLogic) _unit.player = APNonplayable;
	}
	CSlider *slider = dynamic_cast<CSlider *>(GetCtrl(IDC_ARCUNIT_HEALTH));
	if (slider)
		_unit.health = slider->GetThumbPos();
	slider = dynamic_cast<CSlider *>(GetCtrl(IDC_ARCUNIT_FUEL));
	if (slider)
		_unit.fuel = slider->GetThumbPos();
	slider = dynamic_cast<CSlider *>(GetCtrl(IDC_ARCUNIT_AMMO));
	if (slider)
		_unit.ammo = slider->GetThumbPos();
	slider = dynamic_cast<CSlider *>(GetCtrl(IDC_ARCUNIT_SKILL));
	if (slider)
		_unit.skill = slider->GetThumbPos();
	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_VEHICLE));
	if (combo)
	{
		int indexVehicle = combo->GetCurSel();
		Assert(indexVehicle >= 0);
		_unit.vehicle = combo->GetData(indexVehicle);
	}
	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_RANK));
	if (combo)
	{
		_unit.rank = (Rank)(RankPrivate + combo->GetCurSel());
		if (_unit.side == TEmpty || _unit.side == TLogic) _unit.rank = RankPrivate;
	}
	CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCUNIT_AZIMUT));
	if (edit)
		_unit.azimut = edit->GetText() ? atof(edit->GetText()) : 0;
	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_SPECIAL));
	if (combo)
		_unit.special = (ArcadeUnitSpecial)combo->GetCurSel();
	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_AGE));
	if (combo)
		_unit.age = (ArcadeUnitAge)combo->GetCurSel();
	slider = dynamic_cast<CSlider *>(GetCtrl(IDC_ARCUNIT_PRESENCE));
	if (slider)
		_unit.presence = slider->GetThumbPos();
	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCUNIT_PRESENCE_COND));
	if (edit)
		_unit.presenceCondition = edit->GetText();
	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCUNIT_PLACE));
	if (edit)
		_unit.placement = edit->GetText() ? atof(edit->GetText()) : 0;
	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCUNIT_TEXT));
	if (edit)
		_unit.name = edit->GetText();
	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCUNIT_INIT));
	if (edit)
		_unit.init = edit->GetText();
	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_LOCK));
	if (combo)
	{
		int sel = combo->GetCurSel();
		if (sel >= 0) _unit.lock = (LockState)combo->GetValue(sel);
	}
}

DisplayArcadeGroup::DisplayArcadeGroup
(
	ControlsContainer *parent, Vector3Par position,
	RString side, RString type, RString name,
	float azimut
)
	: Display(parent)
{
	_enableSimulation = false;
	_enableDisplay = false;

	_position = position;
	_azimut = azimut;

	Load("RscDisplayArcadeGroup");

	UpdateTypes();
	UpdateNames();

	CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCGRP_SIDE));
	if (combo) for (int i=0; i<combo->GetSize(); i++)
	{
		if (combo->GetData(i) == side)
		{
			combo->SetCurSel(i); break;
		}
	}
	
	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCGRP_TYPE));
	if (combo) for (int i=0; i<combo->GetSize(); i++)
	{
		if (combo->GetData(i) == type)
		{
			combo->SetCurSel(i); break;
		}
	}

	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCGRP_NAME));
	if (combo) for (int i=0; i<combo->GetSize(); i++)
	{
		if (combo->GetData(i) == name)
		{
			combo->SetCurSel(i); break;
		}
	}
}

Control *DisplayArcadeGroup::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
	case IDC_ARCGRP_SIDE:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			const ParamEntry &groups = Pars >> "CfgGroups";
			for (int i=0; i<groups.GetEntryCount(); i++)
			{
				const ParamEntry &entry = groups.GetEntry(i);
				int index = combo->AddString(entry >> "name");
				combo->SetData(index, entry.GetName());
			}
			combo->SetCurSel(0);
			return combo;
		}
	case IDC_ARCGRP_AZIMUT:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			char buffer[256];
			sprintf(buffer, "%g", _azimut);
			edit->SetText(buffer);
			return edit;
		}
	case IDC_ARCGRP_AZIMUT_PICTURE:
		return new CStaticAzimut(this, idc, cls, IDC_ARCGRP_AZIMUT);
	}
	return Display::OnCreateCtrl(type, idc, cls);
}

void DisplayArcadeGroup::OnComboSelChanged(int idc, int curSel)
{
	switch (idc)
	{
	case IDC_ARCGRP_SIDE:
		UpdateTypes();
		UpdateNames();
		break;
	case IDC_ARCGRP_TYPE:
		UpdateNames();
		break;
	}
}

void DisplayArcadeGroup::UpdateTypes()
{
	CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCGRP_SIDE));
	if (!combo) return;
	int i = combo->GetCurSel();
	if (i < 0) return;
	RString side = combo->GetData(i);

	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCGRP_TYPE));
	if (!combo) return;
	combo->ClearStrings();
	const ParamEntry &types = Pars >> "CfgGroups" >> side;
	for (int i=0; i<types.GetEntryCount(); i++)
	{
		const ParamEntry &entry = types.GetEntry(i);
		if (!entry.IsClass()) continue;
		int index = combo->AddString(entry >> "name");
		combo->SetData(index, entry.GetName());
	}
	combo->SetCurSel(0);
}

void DisplayArcadeGroup::UpdateNames()
{
	CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCGRP_SIDE));
	if (!combo) return;
	int i = combo->GetCurSel();
	if (i < 0) return;
	RString side = combo->GetData(i);

	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCGRP_TYPE));
	if (!combo) return;
	i = combo->GetCurSel();
	if (i < 0) return;
	RString type = combo->GetData(i);

	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCGRP_NAME));
	if (!combo) return;
	combo->ClearStrings();
	const ParamEntry &names = Pars >> "CfgGroups" >> side >> type;
	for (int i=0; i<names.GetEntryCount(); i++)
	{
		const ParamEntry &entry = names.GetEntry(i);
		if (!entry.IsClass()) continue;
		int index = combo->AddString(entry >> "name");
		combo->SetData(index, entry.GetName());
	}
	combo->SetCurSel(0);
}

Control *DisplayArcadeSensor::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
	case IDC_ARCSENS_TITLE:
		{
			CStatic *text = new CStatic(this, idc, cls);
			if (_index < 0)
				text->SetText(LocalizeString(IDS_ARCSENS_TITLE1));
			else
				text->SetText(LocalizeString(IDS_ARCSENS_TITLE2));
			return text;
		}
	case IDC_ARCSENS_A:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			char buffer[256];
			sprintf(buffer, "%g", _sensor.a);
			edit->SetText(buffer);
			return edit;
		}
	case IDC_ARCSENS_B:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			char buffer[256];
			sprintf(buffer, "%g", _sensor.b);
			edit->SetText(buffer);
			return edit;
		}
	case IDC_ARCSENS_ANGLE:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			char buffer[256];
			sprintf(buffer, "%g", _sensor.angle);
			edit->SetText(buffer);
			return edit;
		}
	case IDC_ARCSENS_RECT:
		{
			CToolBox *toolbox = new CToolBox(this, idc, cls);
			if (_sensor.rectangular)
				toolbox->SetCurSel(1);
			else
				toolbox->SetCurSel(0);
			return toolbox;
		}
	case IDC_ARCSENS_ACTIV:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			if (_ig >= 0)
			{
				combo->AddString(LocalizeString(IDS_SENSORACTIV_GROUP));
				combo->SetCurSel(0);
			}
			else if (_sensor.idStatic >= 0)
			{
				combo->AddString(LocalizeString(IDS_SENSORACTIV_STATIC));
				combo->SetCurSel(0);
			}
			else if (_sensor.idVehicle >= 0)
			{
				int ig;
				int index;
				if (!_t->FindUnit(_sensor.idVehicle, ig, index))
				{
					// vehicle doesn't exist
					_sensor.idVehicle = -1;
					goto NoVehicle;
				}
				else if (ig >= 0)
				{
					// vehicle in group
					combo->AddString(LocalizeString(IDS_SENSORACTIV_VEHICLE));
					combo->AddString(LocalizeString(IDS_SENSORACTIV_GROUP));
					combo->AddString(LocalizeString(IDS_SENSORACTIV_LEADER));
					combo->AddString(LocalizeString(IDS_SENSORACTIV_MEMBER));
					int sel = _sensor.activationBy - ASAVehicle;
					saturate(sel, 0, combo->GetSize() - 1);
					combo->SetCurSel(sel);
				}
				else
				{
					// empty vehicle
					combo->AddString(LocalizeString(IDS_SENSORACTIV_VEHICLE));
					combo->SetCurSel(0);
				}
			}
			else
			{
NoVehicle:
				combo->AddString(LocalizeString(IDS_SENSORACTIV_NONE));
				combo->AddString(LocalizeString(IDS_EAST));
				combo->AddString(LocalizeString(IDS_WEST));
				combo->AddString(LocalizeString(IDS_GUERRILA));
				combo->AddString(LocalizeString(IDS_CIVILIAN));
				combo->AddString(LocalizeString(IDS_LOGIC));
				combo->AddString(LocalizeString(IDS_SENSORACTIV_ANYBODY));
				combo->AddString(LocalizeString(IDS_SENSORACTIV_ALPHA));
				combo->AddString(LocalizeString(IDS_SENSORACTIV_BRAVO));
				combo->AddString(LocalizeString(IDS_SENSORACTIV_CHARLIE));
				combo->AddString(LocalizeString(IDS_SENSORACTIV_DELTA));
				combo->AddString(LocalizeString(IDS_SENSORACTIV_ECHO));
				combo->AddString(LocalizeString(IDS_SENSORACTIV_FOXTROT));
				combo->AddString(LocalizeString(IDS_SENSORACTIV_GOLF));
				combo->AddString(LocalizeString(IDS_SENSORACTIV_HOTEL));
				combo->AddString(LocalizeString(IDS_SENSORACTIV_INDIA));
				combo->AddString(LocalizeString(IDS_SENSORACTIV_JULIET));
				if (_sensor.activationBy > ASAJuliet) _sensor.activationBy = ASANone;
				combo->SetCurSel(_sensor.activationBy);
			}
			return combo;
		}
	case IDC_ARCSENS_PRESENCE:
		{
			CToolBox *toolbox = new CToolBox(this, idc, cls);
			toolbox->SetCurSel(_sensor.activationType);
			return toolbox;
		}
	case IDC_ARCSENS_REPEATING:
		{
			CToolBox *toolbox = new CToolBox(this, idc, cls);
			if (_sensor.repeating)
				toolbox->SetCurSel(1);
			else
				toolbox->SetCurSel(0);
			return toolbox;
		}
	case IDC_ARCSENS_INTERRUPT:
		{
			CToolBox *toolbox = new CToolBox(this, idc, cls);
			if (_sensor.interruptable)
				toolbox->SetCurSel(1);
			else
				toolbox->SetCurSel(0);
			return toolbox;
		}
	case IDC_ARCSENS_TIMEOUT_MIN:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			char buffer[256];
			sprintf(buffer, "%g", _sensor.timeoutMin);
			edit->SetText(buffer);
			return edit;
		}
	case IDC_ARCSENS_TIMEOUT_MID:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			char buffer[256];
			sprintf(buffer, "%g", _sensor.timeoutMid);
			edit->SetText(buffer);
			return edit;
		}
	case IDC_ARCSENS_TIMEOUT_MAX:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			char buffer[256];
			sprintf(buffer, "%g", _sensor.timeoutMax);
			edit->SetText(buffer);
			return edit;
		}
	case IDC_ARCSENS_TYPE:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			for (int i=0; i<ASTN; i++)
			{
				combo->AddString(LocalizeString(IDS_SENSORTYPE_NONE + i));
			}
			combo->SetCurSel(_sensor.type);
			return combo;
		}
	case IDC_ARCSENS_OBJECT:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			const ParamEntry &objects = Pars >> "CfgDetectors" >> "objects";
			int sel = 0;
			for (int i=0; i<objects.GetSize(); i++)
			{
				RString objectName = objects[i];
				if (stricmp(objectName, _sensor.object) == 0) sel = i;
				const ParamEntry &objectCls = Pars >> "CfgNonAIVehicles" >> objectName;
				RString displayName = objectCls >> "displayName";
				int index = combo->AddString(displayName);
				combo->SetData(index, objectName);
			}
			combo->SetCurSel(sel);
			return combo;
		}
	case IDC_ARCSENS_TEXT:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			edit->SetText(_sensor.text);
			return edit;
		}
	case IDC_ARCSENS_NAME:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			edit->SetText(_sensor.name);
			return edit;
		}
	case IDC_ARCSENS_EXPCOND:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			edit->SetText(_sensor.expCond);
			return edit;
		}
	case IDC_ARCSENS_EXPACTIV:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			edit->SetText(_sensor.expActiv);
			return edit;
		}
	case IDC_ARCSENS_EXPDESACTIV:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			edit->SetText(_sensor.expDesactiv);
			return edit;
		}
	case IDC_ARCSENS_AGE:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			for (int i=0; i<AAN; i++)
				combo->AddString(LocalizeString(IDS_AGE_ACTUAL + i));
			combo->SetCurSel(_sensor.age);
			return combo;
		}
	}

	return Display::OnCreateCtrl(type, idc, cls);
}

void DisplayArcadeSensor::OnButtonClicked(int idc)
{
	if (idc == IDC_ARCSENS_EFFECTS)
	{
		CreateChild(new DisplayArcadeEffects(this, _sensor.effects, _advanced));
	}
	else
	{
		Display::OnButtonClicked(idc);
	}
}

void DisplayArcadeSensor::OnChildDestroyed(int idd, int exit)
{
	if (idd == IDD_ARCADE_EFFECTS && exit == IDC_OK)
	{
		DisplayArcadeEffects *display = dynamic_cast<DisplayArcadeEffects *>((ControlsContainer *)_child);
		Assert(display);
		_sensor.effects = display->_effects;
	}
	Display::OnChildDestroyed(idd, exit);
}

bool DisplayArcadeSensor::CanDestroy()
{
	if (_exit != IDC_OK) return true;

	GameState *gstate = GWorld->GetGameState();

	CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_EXPCOND));
	if (edit)
	{
		if (!gstate->CheckEvaluateBool(edit->GetText()))
		{
			FocusCtrl(IDC_ARCSENS_EXPCOND);
			CreateMsgBox(MB_BUTTON_OK, gstate->GetLastErrorText());
			edit->SetCaretPos(gstate->GetLastErrorPos());
			return false;
		}
	}
	
	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_EXPACTIV));
	if (edit)
	{
		if (!gstate->CheckExecute(edit->GetText()))
		{
			FocusCtrl(IDC_ARCSENS_EXPACTIV);
			CreateMsgBox(MB_BUTTON_OK, gstate->GetLastErrorText());
			edit->SetCaretPos(gstate->GetLastErrorPos());
			return false;
		}
	}
	
	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_EXPDESACTIV));
	if (edit)
	{
		if (!gstate->CheckExecute(edit->GetText()))
		{
			FocusCtrl(IDC_ARCSENS_EXPDESACTIV);
			CreateMsgBox(MB_BUTTON_OK, gstate->GetLastErrorText());
			edit->SetCaretPos(gstate->GetLastErrorPos());
			return false;
		}
	}

	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_NAME));
	if (edit)
	{
		RString text = edit->GetText();
		if (text.GetLength() > 0)
		{
			if (!GWorld->GetGameState()->IdtfGoodName(text))
			{
				CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_VEH_NAME));
				return false;
			}
			// check if text is not used
			for (int i=0; i<_t->groups.Size(); i++)
			{
				ArcadeGroupInfo &gInfo = _t->groups[i];
				for (int j=0; j<gInfo.units.Size(); j++)
				{
					if (stricmp(text, gInfo.units[j].name) == 0)
					{
						CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_VEH_TEXT_USED));
						return false;
					}
				}
				for (int j=0; j<gInfo.sensors.Size(); j++)
				{
					if (i == _ig && j == _index) continue;
					if (stricmp(text, gInfo.sensors[j].name) == 0)
					{
						CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_VEH_TEXT_USED));
						return false;
					}
				}
			}
			for (int j=0; j<_t->emptyVehicles.Size(); j++)
			{
				if (stricmp(text, _t->emptyVehicles[j].name) == 0)
				{
					CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_VEH_TEXT_USED));
					return false;
				}
			}
			for (int j=0; j<_t->sensors.Size(); j++)
			{
				if (_ig == -1 && j == _index) continue;
				if (stricmp(text, _t->sensors[j].name) == 0)
				{
					CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_VEH_TEXT_USED));
					return false;
				}
			}
		}
	}

	return true;
}

void DisplayArcadeSensor::Destroy()
{
	Display::Destroy();

	if (_exit != IDC_OK) return;

	CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_A));
	if (edit)
		_sensor.a = edit->GetText() ? atof(edit->GetText()) : 0;
	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_B));
	if (edit)
		_sensor.b = edit->GetText() ? atof(edit->GetText()) : 0;
	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_ANGLE));
	if (edit)
		_sensor.angle = edit->GetText() ? atof(edit->GetText()) : 0;
	CToolBox *toolbox = dynamic_cast<CToolBox *>(GetCtrl(IDC_ARCSENS_RECT));
	if (toolbox)
		_sensor.rectangular = toolbox->GetCurSel() == 1;

	CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCSENS_ACTIV));
	if (combo)
	{
		if (_ig >= 0)
		{
			_sensor.activationBy = ASAGroup;
		}
		else if (_sensor.idStatic >= 0)
		{
			_sensor.activationBy = ASAStatic;
		}
		else if (_sensor.idVehicle >= 0)
		{
			_sensor.activationBy = (ArcadeSensorActivation)(ASAVehicle + combo->GetCurSel());
		}
		else
		{
			_sensor.activationBy = (ArcadeSensorActivation)(combo->GetCurSel());
		}
	}
	toolbox = dynamic_cast<CToolBox *>(GetCtrl(IDC_ARCSENS_PRESENCE));
	if (toolbox)
		_sensor.activationType = (ArcadeSensorActivationType)(toolbox->GetCurSel());
	toolbox = dynamic_cast<CToolBox *>(GetCtrl(IDC_ARCSENS_REPEATING));
	if (toolbox)
		_sensor.repeating = toolbox->GetCurSel() == 1;
	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_TIMEOUT_MIN));
	if (edit)
		_sensor.timeoutMin = edit->GetText() ? atof(edit->GetText()) : 0;
	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_TIMEOUT_MID));
	if (edit)
		_sensor.timeoutMid = edit->GetText() ? atof(edit->GetText()) : 0;
	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_TIMEOUT_MAX));
	if (edit)
		_sensor.timeoutMax = edit->GetText() ? atof(edit->GetText()) : 0;
	toolbox = dynamic_cast<CToolBox *>(GetCtrl(IDC_ARCSENS_INTERRUPT));
	if (toolbox)
		_sensor.interruptable = toolbox->GetCurSel() == 1;
	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCSENS_TYPE));
	if (combo)
		_sensor.type = (ArcadeSensorType)(combo->GetCurSel());
	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCSENS_OBJECT));
	if (combo)
		_sensor.object = combo->GetData(combo->GetCurSel());
	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_TEXT));
	if (edit)
		_sensor.text = edit->GetText();
	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_NAME));
	if (edit)
		_sensor.name = edit->GetText();
	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_EXPCOND));
	if (edit)
		_sensor.expCond = edit->GetText();
	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_EXPACTIV));
	if (edit)
		_sensor.expActiv = edit->GetText();
	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_EXPDESACTIV));
	if (edit)
		_sensor.expDesactiv = edit->GetText();
	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCSENS_AGE));
	if (combo)
		_sensor.age = (ArcadeUnitAge)(combo->GetCurSel());
}

Control *DisplayArcadeMarker::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
	case IDC_ARCMARK_TITLE:
		{
			CStatic *text = new CStatic(this, idc, cls);
			if (_index < 0)
				text->SetText(LocalizeString(IDS_ARCMARK_TITLE1));
			else
				text->SetText(LocalizeString(IDS_ARCMARK_TITLE2));
			return text;
		}
	case IDC_ARCMARK_NAME:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			edit->SetText(_marker.name);
			return edit;
		}
	case IDC_ARCMARK_TEXT:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			edit->SetText(_marker.text);
			return edit;
		}
	case IDC_ARCMARK_MARKER:
		{
			CToolBox *ctrl = new CToolBox(this, idc, cls);
			ctrl->SetCurSel(_marker.markerType);
			return ctrl;
		}
	case IDC_ARCMARK_TYPE:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			const ParamEntry &cls = Pars >> "CfgMarkers";
			int n = cls.GetEntryCount();
			int sel = 0;
			for (int i=0; i<n; i++)
			{
				const ParamEntry &entry = cls.GetEntry(i);
				int index = combo->AddString(entry >> "name");
				combo->SetData(index, entry.GetName());
				if (stricmp(_marker.type, entry.GetName()) == 0) sel = index;
			}
			combo->SetCurSel(sel);
			return combo;
		}
	case IDC_ARCMARK_COLOR:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			const ParamEntry &cls = Pars >> "CfgMarkerColors";
			int n = cls.GetEntryCount();
			int sel = 0;
			for (int i=0; i<n; i++)
			{
				const ParamEntry &entry = cls.GetEntry(i);
				int index = combo->AddString(entry >> "name");
				combo->SetData(index, entry.GetName());
				if (stricmp(_marker.colorName, entry.GetName()) == 0) sel = index;
			}
			combo->SetCurSel(sel);
			return combo;
		}
	case IDC_ARCMARK_FILL:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			const ParamEntry &cls = Pars >> "CfgMarkerBrushes";
			int n = cls.GetEntryCount();
			int sel = 0;
			for (int i=0; i<n; i++)
			{
				const ParamEntry &entry = cls.GetEntry(i);
				int index = combo->AddString(entry >> "name");
				combo->SetData(index, entry.GetName());
				if (stricmp(_marker.fillName, entry.GetName()) == 0) sel = index;
			}
			combo->SetCurSel(sel);
			return combo;
		}
	case IDC_ARCMARK_A:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			char buffer[256];
			sprintf(buffer, "%g", _marker.a);
			edit->SetText(buffer);
			return edit;
		}
	case IDC_ARCMARK_B:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			char buffer[256];
			sprintf(buffer, "%g", _marker.b);
			edit->SetText(buffer);
			return edit;
		}
	case IDC_ARCMARK_ANGLE:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			char buffer[256];
			sprintf(buffer, "%g", _marker.angle);
			edit->SetText(buffer);
			return edit;
		}
	}

	return Display::OnCreateCtrl(type, idc, cls);
}

bool DisplayArcadeMarker::CanDestroy()
{
	if (!Display::CanDestroy()) return false;

	if (_exit == IDC_OK) 
	{
		Assert(dynamic_cast<CEdit *>(GetCtrl(IDC_ARCMARK_NAME)));
		CEdit *edit = static_cast<CEdit *>(GetCtrl(IDC_ARCMARK_NAME));
		if (edit)
		{
			RString name = edit->GetText();
			if (name.GetLength() == 0)
			{
				CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_MARKER_EMPTY));
				return false;
			}
			int n = _template->markers.Size();
			for (int i=0; i<n; i++)
			{
				if (i == _index) continue;
				if (stricmp(name, _template->markers[i].name) == 0)
				{
					CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_MARKER_EXIST));
					return false;
				}
			}
		}
	}

	return true;
}

void DisplayArcadeMarker::Destroy()
{
	Display::Destroy();

	if (_exit != IDC_OK) return;

	CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCMARK_NAME));
	if (edit)
		_marker.name = edit->GetText();

	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCMARK_TEXT));
	if (edit)
		_marker.text = edit->GetText();

	float defSize = 1;

	CToolBox *ctrl = dynamic_cast<CToolBox *>(GetCtrl(IDC_ARCMARK_MARKER));
	if (ctrl)
	{
		_marker.markerType = (MarkerType)ctrl->GetCurSel();
		if (_marker.markerType != MTIcon) defSize = 20;
	}

	CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCMARK_TYPE));
	if (combo)
	{
		_marker.type = combo->GetData(combo->GetCurSel());
		_marker.OnTypeChanged();
	}

	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCMARK_COLOR));
	if (combo)
	{
		_marker.colorName = combo->GetData(combo->GetCurSel());
		_marker.OnColorChanged();
	}

	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCMARK_FILL));
	if (combo)
	{
		_marker.fillName = combo->GetData(combo->GetCurSel());
		_marker.OnFillChanged();
	}

	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCMARK_A));
	if (edit)
		_marker.a = edit->GetText() ? atof(edit->GetText()) : defSize;
	
	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCMARK_B));
	if (edit)
		_marker.b = edit->GetText() ? atof(edit->GetText()) : defSize;

	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCMARK_ANGLE));
	if (edit)
		_marker.angle = edit->GetText() ? atof(edit->GetText()) : 0;
}

void DisplayArcadeMarker::OnToolBoxSelChanged(int idc, int curSel)
{
	if (idc == IDC_ARCMARK_MARKER)
		ChangeType(curSel);
	else
		Display::OnToolBoxSelChanged(idc, curSel);
}

void DisplayArcadeMarker::ChangeType(int curSel)
{
	switch (curSel)
	{
	case MTIcon:
		{
			IControl *ctrl = GetCtrl(IDC_ARCMARK_TYPE);
			if (ctrl) ctrl->ShowCtrl(true);
			ctrl = GetCtrl(IDC_ARCMARK_FILL);
			if (ctrl) ctrl->ShowCtrl(false);
			CStatic *text = dynamic_cast<CStatic *>(GetCtrl(IDC_ARCMARK_TYPE_TEXT));
			if (text) text->SetText(LocalizeString(IDS_ARCMARK_TYPE1));
			if (_oldType != MTIcon)
			{
				CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCMARK_A));
				if (edit)
				{
					float value = edit->GetText() ? atof(edit->GetText()) : 1;
					char buffer[256];
					sprintf(buffer, "%g", value / 20.0f);
					edit->SetText(buffer);
				}
				edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCMARK_B));
				if (edit)
				{
					float value = edit->GetText() ? atof(edit->GetText()) : 1;
					char buffer[256];
					sprintf(buffer, "%g", value / 20.0f);
					edit->SetText(buffer);
				}
			}
		}
		break;
	case MTRectangle:
	case MTEllipse:
		{
			IControl *ctrl = GetCtrl(IDC_ARCMARK_TYPE);
			if (ctrl) ctrl->ShowCtrl(false);
			ctrl = GetCtrl(IDC_ARCMARK_FILL);
			if (ctrl) ctrl->ShowCtrl(true);
			CStatic *text = dynamic_cast<CStatic *>(GetCtrl(IDC_ARCMARK_TYPE_TEXT));
			if (text) text->SetText(LocalizeString(IDS_ARCMARK_TYPE2));
			if (_oldType == MTIcon)
			{
				CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCMARK_A));
				if (edit)
				{
					float value = edit->GetText() ? atof(edit->GetText()) : 1;
					char buffer[256];
					sprintf(buffer, "%g", value * 20.0f);
					edit->SetText(buffer);
				}
				edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCMARK_B));
				if (edit)
				{
					float value = edit->GetText() ? atof(edit->GetText()) : 1;
					char buffer[256];
					sprintf(buffer, "%g", value * 20.0f);
					edit->SetText(buffer);
				}
			}
		}
		break;
	default:
		Fail("Bad marker type");
		break;
	}
	_oldType = curSel;
}

DisplayArcadeEffects::DisplayArcadeEffects
(
	ControlsContainer *parent, ArcadeEffects effects,
	bool advanced
)
	: Display(parent)
{
	_enableSimulation = false;
	_enableDisplay = false;
	_effects = effects;
	if (advanced)
		Load("RscDisplayArcadeEffects");
	else
	{
		Load("RscDisplayArcadeEffectsSimple");
		_effects.titleType = TitleText;
		ChangeTitleType(TitleText);
	}

	ChangeTitleType(_effects.titleType);
}

/*!
\patch 1.56 Date 5/15/2002 by Ondra
- Changed: CfgMusic no longer requires "tracks" list. This makes music addons easier to use.
*/
/*!
\patch 1.61 Date 5/27/2002 by Ondra
- Changed: CfgSounds, RscTitles and CfgSFX no longer require array listing class names.
This enables addons to add new entries that can be used in mission editor.
*/

Control *DisplayArcadeEffects::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
/*
	case IDC_ARCEFF_PLAYERONLY:
		{
			CToolBox *toolbox = new CToolBox(this, idc, cls);
			if (_effects.playerOnly)
				toolbox->SetCurSel(1);
			else
				toolbox->SetCurSel(0);
			return toolbox;
		}
*/
	case IDC_ARCEFF_CONDITION:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			edit->SetText(_effects.condition);
			return edit;
		}
	case IDC_ARCEFF_CAMERA:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			int sel = -1;
			int index = combo->AddString(LocalizeString(IDS_CAMEFFECT_NONE));
			combo->SetData(index, "");
			if (_effects.cameraEffect.GetLength() == 0) sel = index;
			index = combo->AddString(LocalizeString(IDS_CAMEFFECT_TERMINATE));
			combo->SetData(index, "$TERMINATE$");
			if (stricmp(_effects.cameraEffect, "$TERMINATE$") == 0) sel = index;
			RString effect = _effects.cameraEffect;
			if (effect[0] == '@') effect = (const char *)effect + 1;
			const ParamEntry *cls = Pars.FindEntry("CfgCameraEffects");
			if (cls)
			{
				const ParamEntry &ar = (*cls) >> "Array";
				int n = ar.GetEntryCount();
				for (int i=0; i<n; i++)
				{
					const ParamEntry &entry = ar.GetEntry(i);
					index = combo->AddString(entry >> "name");
					combo->SetData(index, entry.GetName());
					if (stricmp(effect, entry.GetName()) == 0) sel = index;
				}
			}
			cls = ExtParsCampaign.FindEntry("CfgCameraEffects");
			if (cls)
			{
				const ParamEntry &ar = (*cls) >> "Array";
				int n = ar.GetEntryCount();
				for (int i=0; i<n; i++)
				{
					const ParamEntry &entry = ar.GetEntry(i);
					index = combo->AddString(entry >> "name");
					combo->SetData(index, entry.GetName());
					if (stricmp(effect, entry.GetName()) == 0) sel = index;
				}
			}
			cls = ExtParsMission.FindEntry("CfgCameraEffects");
			if (cls)
			{
				const ParamEntry &ar = (*cls) >> "Array";
				int n = ar.GetEntryCount();
				for (int i=0; i<n; i++)
				{
					const ParamEntry &entry = ar.GetEntry(i);
					index = combo->AddString(entry >> "name");
					combo->SetData(index, entry.GetName());
					if (stricmp(effect, entry.GetName()) == 0) sel = index;
				}
			}
			combo->SetCurSel(sel);
			return combo;
		}
	case IDC_ARCEFF_CAMPOS:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			int i;
			for (i=0; i<NCamEffectPositions; i++)
			{
				combo->AddString(LocalizeString(IDS_CAMEFFECT_TOP + i));
			}
			combo->SetCurSel(_effects.cameraPosition);
			return combo;
		}
	case IDC_ARCEFF_SOUND:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			int index = combo->AddString(LocalizeString(IDS_SOUND_NONE));
			combo->SetData(index, "$NONE$");
			int sel = index;
			RString sound = _effects.sound;
			if (sound[0] == '@') sound = (const char *)sound + 1;
			const ParamEntry *cls = Pars.FindEntry("CfgSounds");
			if (cls)
			{
				int n = cls->GetEntryCount();
				for (int i=0; i<n; i++)
				{
					const ParamEntry &ci = cls->GetEntry(i);
					if (!ci.IsClass()) continue;
					if (!ci.FindEntry("name")) continue;
					RString displayName = ci >> "name";
					int index = combo->AddString(displayName);
					combo->SetData(index, ci.GetName());
					if (stricmp(ci.GetName(), sound) == 0) sel = index;
				}
			}
			cls = ExtParsCampaign.FindEntry("CfgSounds");
			if (cls)
			{
				int n = cls->GetEntryCount();
				for (int i=0; i<n; i++)
				{
					const ParamEntry &ci = cls->GetEntry(i);
					if (!ci.IsClass()) continue;
					if (!ci.FindEntry("name")) continue;
					RString displayName = ci >> "name";
					int index = combo->AddString(displayName);
					combo->SetData(index, ci.GetName());
					if (stricmp(ci.GetName(), sound) == 0) sel = index;
				}
			}
			cls = ExtParsMission.FindEntry("CfgSounds");
			if (cls)
			{
				int n = cls->GetEntryCount();
				for (int i=0; i<n; i++)
				{
					const ParamEntry &ci = cls->GetEntry(i);
					if (!ci.IsClass()) continue;
					if (!ci.FindEntry("name")) continue;
					RString displayName = ci >> "name";
					int index = combo->AddString(displayName);
					combo->SetData(index, ci.GetName());
					if (stricmp(ci.GetName(), sound) == 0) sel = index;
				}
			}
			combo->SetCurSel(sel);
			return combo;
		}
	case IDC_ARCEFF_VOICE:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			int index = combo->AddString(LocalizeString(IDS_SOUND_NONE));
			combo->SetData(index, "");
			int sel = index;
			RString sound = _effects.voice;
			if (sound[0] == '@') sound = (const char *)sound + 1;
			const ParamEntry *cls = Pars.FindEntry("CfgSounds");
			if (cls)
			{
				int n = cls->GetEntryCount();
				for (int i=0; i<n; i++)
				{
					const ParamEntry &ci = cls->GetEntry(i);
					if (!ci.IsClass()) continue;
					if (!ci.FindEntry("name")) continue;
					RString displayName = ci >> "name";
					int index = combo->AddString(displayName);
					combo->SetData(index, ci.GetName());
					if (stricmp(ci.GetName(), sound) == 0) sel = index;
				}
			}
			cls = ExtParsCampaign.FindEntry("CfgSounds");
			if (cls)
			{
				int n = cls->GetEntryCount();
				for (int i=0; i<n; i++)
				{
					const ParamEntry &ci = cls->GetEntry(i);
					if (!ci.IsClass()) continue;
					if (!ci.FindEntry("name")) continue;
					RString displayName = ci >> "name";
					int index = combo->AddString(displayName);
					combo->SetData(index, ci.GetName());
					if (stricmp(ci.GetName(), sound) == 0) sel = index;
				}
			}
			cls = ExtParsMission.FindEntry("CfgSounds");
			if (cls)
			{
				int n = cls->GetEntryCount();
				for (int i=0; i<n; i++)
				{
					const ParamEntry &ci = cls->GetEntry(i);
					if (!ci.IsClass()) continue;
					if (!ci.FindEntry("name")) continue;
					RString displayName = ci >> "name";
					int index = combo->AddString(displayName);
					combo->SetData(index, ci.GetName());
					if (stricmp(ci.GetName(), sound) == 0) sel = index;
				}
			}
			combo->SetCurSel(sel);
			return combo;
		}
	case IDC_ARCEFF_SOUND_ENV:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			int index = combo->AddString(LocalizeString(IDS_SOUND_NONE));
			combo->SetData(index, "");
			int sel = index;
			RString sound = _effects.soundEnv;
			if (sound[0] == '@') sound = (const char *)sound + 1;
			const ParamEntry *cls = Pars.FindEntry("CfgEnvSounds");
			if (cls)
			{
				int n = cls->GetEntryCount();
				for (int i=0; i<n; i++)
				{
					const ParamEntry &ci = cls->GetEntry(i);
					if (!ci.IsClass()) continue;
					if (!ci.FindEntry("name")) continue;
					RString displayName = ci >> "name";
					int index = combo->AddString(displayName);
					combo->SetData(index, ci.GetName());
					if (stricmp(ci.GetName(), sound) == 0) sel = index;
				}
			}
			cls = ExtParsCampaign.FindEntry("CfgEnvSounds");
			if (cls)
			{
				int n = cls->GetEntryCount();
				for (int i=0; i<n; i++)
				{
					const ParamEntry &ci = cls->GetEntry(i);
					if (!ci.IsClass()) continue;
					if (!ci.FindEntry("name")) continue;
					RString displayName = ci >> "name";
					int index = combo->AddString(displayName);
					combo->SetData(index, ci.GetName());
					if (stricmp(ci.GetName(), sound) == 0) sel = index;
				}
			}
			cls = ExtParsMission.FindEntry("CfgEnvSounds");
			if (cls)
			{
				int n = cls->GetEntryCount();
				for (int i=0; i<n; i++)
				{
					const ParamEntry &ci = cls->GetEntry(i);
					if (!ci.IsClass()) continue;
					if (!ci.FindEntry("name")) continue;
					RString displayName = ci >> "name";
					int index = combo->AddString(displayName);
					combo->SetData(index, ci.GetName());
					if (stricmp(ci.GetName(), sound) == 0) sel = index;
				}
			}
			combo->SetCurSel(sel);
			return combo;
		}
	case IDC_ARCEFF_SOUND_DET:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			int index = combo->AddString(LocalizeString(IDS_SOUND_NONE));
			combo->SetData(index, "");
			int sel = index;
			RString sound = _effects.soundDet;
			if (sound[0] == '@') sound = (const char *)sound + 1;
			const ParamEntry *cls = Pars.FindEntry("CfgSFX");
			if (cls)
			{
				int n = cls->GetEntryCount();
				for (int i=0; i<n; i++)
				{
					const ParamEntry &ci = cls->GetEntry(i);
					if (!ci.IsClass()) continue;
					if (!ci.FindEntry("name")) continue;
					RString displayName = ci >> "name";
					int index = combo->AddString(displayName);
					combo->SetData(index, ci.GetName());
					if (stricmp(ci.GetName(), sound) == 0) sel = index;
				}
			}
			cls = ExtParsCampaign.FindEntry("CfgSFX");
			if (cls)
			{
				int n = cls->GetEntryCount();
				for (int i=0; i<n; i++)
				{
					const ParamEntry &ci = cls->GetEntry(i);
					if (!ci.IsClass()) continue;
					if (!ci.FindEntry("name")) continue;
					RString displayName = ci >> "name";
					int index = combo->AddString(displayName);
					combo->SetData(index, ci.GetName());
					if (stricmp(ci.GetName(), sound) == 0) sel = index;
				}
			}
			cls = ExtParsMission.FindEntry("CfgSFX");
			if (cls)
			{
				int n = cls->GetEntryCount();
				for (int i=0; i<n; i++)
				{
					const ParamEntry &ci = cls->GetEntry(i);
					if (!ci.IsClass()) continue;
					if (!ci.FindEntry("name")) continue;
					RString displayName = ci >> "name";
					int index = combo->AddString(displayName);
					combo->SetData(index, ci.GetName());
					if (stricmp(ci.GetName(), sound) == 0) sel = index;
				}
			}
			combo->SetCurSel(sel);
			return combo;
		}
	case IDC_ARCEFF_MUSIC:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			RString sound = _effects.track;
			int index = combo->AddString(LocalizeString(IDS_MUSIC_NONE));
			combo->SetData(index, "$NONE$");
			int sel = index;
			index = combo->AddString(LocalizeString(IDS_MUSIC_SILENCE));
			combo->SetData(index, "$STOP$");
			if (stricmp("$STOP$", sound) == 0) sel = index;
			const ParamEntry *cls = Pars.FindEntry("CfgMusic");
			if (cls)
			{
				int n = cls->GetEntryCount();
				for (int i=0; i<n; i++)
				{
					const ParamEntry &ci = cls->GetEntry(i);
					if (!ci.IsClass()) continue;
					if (!ci.FindEntry("name")) continue;
					RString displayName = ci >> "name";
					int index = combo->AddString(displayName);
					combo->SetData(index, ci.GetName());
					if (stricmp(ci.GetName(), sound) == 0) sel = index;
				}
			}
			cls = ExtParsCampaign.FindEntry("CfgMusic");
			if (cls)
			{
				int n = cls->GetEntryCount();
				for (int i=0; i<n; i++)
				{
					const ParamEntry &ci = cls->GetEntry(i);
					if (!ci.IsClass()) continue;
					if (!ci.FindEntry("name")) continue;
					RString displayName = ci >> "name";
					int index = combo->AddString(displayName);
					combo->SetData(index, ci.GetName());
					if (stricmp(ci.GetName(), sound) == 0) sel = index;
				}
			}
			cls = ExtParsMission.FindEntry("CfgMusic");
			if (cls)
			{
				int n = cls->GetEntryCount();
				for (int i=0; i<n; i++)
				{
					const ParamEntry &ci = cls->GetEntry(i);
					if (!ci.IsClass()) continue;
					if (!ci.FindEntry("name")) continue;
					RString displayName = ci >> "name";
					int index = combo->AddString(displayName);
					combo->SetData(index, ci.GetName());
					if (stricmp(ci.GetName(), sound) == 0) sel = index;
				}
			}
			combo->SetCurSel(sel);
			return combo;
		}
	case IDC_ARCEFF_TITTYPE:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			combo->AddString(LocalizeString(IDS_TITTYPE_NONE));
			combo->AddString(LocalizeString(IDS_TITTYPE_OBJECT));
			combo->AddString(LocalizeString(IDS_TITTYPE_RESOURCE));
			combo->AddString(LocalizeString(IDS_TITTYPE_TEXT));
			combo->SetCurSel(_effects.titleType);
			return combo;
		}
	case IDC_ARCEFF_TITEFF:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			int i;
			for (i=0; i<NTitEffects; i++)
			{
				combo->AddString(LocalizeString(IDS_TITEFFECT_PLAIN + i));
			}
			combo->SetCurSel(_effects.titleEffect);
			return combo;
		}
	case IDC_ARCEFF_TITTEXT:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			edit->SetText(_effects.title ? _effects.title : "");
			return edit;
		}
	case IDC_ARCEFF_TITRES:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			int sel = 0;
			const ParamEntry *cls = Res.FindEntry("RscTitles");
			if (cls)
			{
				int n = cls->GetEntryCount();
				for (int i=0; i<n; i++)
				{
					const ParamEntry &ci = cls->GetEntry(i);
					if (!ci.IsClass()) continue;
					if (!ci.FindEntry("name")) continue;
					RString displayName = ci >> "name";
					int index = combo->AddString(displayName);
					combo->SetData(index, ci.GetName());
					if (stricmp(ci.GetName(), _effects.title) == 0) sel = index;
				}
			}
			cls = ExtParsCampaign.FindEntry("RscTitles");
			if (cls)
			{
				int n = cls->GetEntryCount();
				for (int i=0; i<n; i++)
				{
					const ParamEntry &ci = cls->GetEntry(i);
					if (!ci.IsClass()) continue;
					if (!ci.FindEntry("name")) continue;
					RString displayName = ci >> "name";
					int index = combo->AddString(displayName);
					combo->SetData(index, ci.GetName());
					if (stricmp(ci.GetName(), _effects.title) == 0) sel = index;
				}
			}
			cls = ExtParsMission.FindEntry("RscTitles");
			if (cls)
			{
				int n = cls->GetEntryCount();
				for (int i=0; i<n; i++)
				{
					const ParamEntry &ci = cls->GetEntry(i);
					if (!ci.IsClass()) continue;
					if (!ci.FindEntry("name")) continue;
					RString displayName = ci >> "name";
					int index = combo->AddString(displayName);
					combo->SetData(index, ci.GetName());
					if (stricmp(ci.GetName(), _effects.title) == 0) sel = index;
				}
			}
			combo->SetCurSel(sel);
			return combo;
		}
	case IDC_ARCEFF_TITOBJ:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			const ParamEntry &cls = Pars >> "CfgTitles";
			int sel = 0;
			int i, n = (cls >> "titles").GetSize();
			for (i=0; i<n; i++)
			{
				RString name = (cls >> "titles")[i];
				RString displayName = cls >> name >> "name";
				int index = combo->AddString(displayName);
				combo->SetData(index, name);
				if (stricmp(name, _effects.title) == 0) sel = index;
			}
			combo->SetCurSel(sel);
			return combo;
		}
	default:
		return Display::OnCreateCtrl(type, idc, cls);
	}
}

void DisplayArcadeEffects::OnComboSelChanged(int idc, int curSel)
{
	if (idc == IDC_ARCEFF_TITTYPE)
	{
		ChangeTitleType(curSel);
	}

	Display::OnComboSelChanged(idc, curSel);
}

void DisplayArcadeEffects::Destroy()
{
	Display::Destroy();

	if (_exit != IDC_OK) return;

	CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCEFF_CONDITION));
	if (edit)
		_effects.condition = edit->GetText();
	CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCEFF_CAMERA));
	if (combo)
		_effects.cameraEffect = combo->GetData(combo->GetCurSel());
	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCEFF_CAMPOS));
	if (combo)
		_effects.cameraPosition = (CamEffectPosition)combo->GetCurSel();
	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCEFF_SOUND));
	if (combo)
		_effects.sound = combo->GetData(combo->GetCurSel());
	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCEFF_VOICE));
	if (combo)
		_effects.voice = combo->GetData(combo->GetCurSel());
	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCEFF_SOUND_ENV));
	if (combo)
		_effects.soundEnv = combo->GetData(combo->GetCurSel());
	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCEFF_SOUND_DET));
	if (combo)
		_effects.soundDet = combo->GetData(combo->GetCurSel());
	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCEFF_MUSIC));
	if (combo)
		_effects.track = combo->GetData(combo->GetCurSel());
	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCEFF_TITTYPE));
	if (combo)
		_effects.titleType = (TitleType)combo->GetCurSel();
	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCEFF_TITEFF));
	if (combo)
		_effects.titleEffect = (TitEffectName)combo->GetCurSel();
	_effects.title = "";
	switch (_effects.titleType)
	{
	case TitleObject:
		combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCEFF_TITOBJ));
		if (combo)
			_effects.title = combo->GetData(combo->GetCurSel());
		break;
	case TitleResource:
		combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCEFF_TITRES));
		if (combo)
			_effects.title = combo->GetData(combo->GetCurSel());
		break;
	case TitleText:
		{
			CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCEFF_TITTEXT));
			if (edit)
			{
				_effects.title = edit->GetText();
				if (_effects.title.GetLength() == 0) _effects.titleType = TitleNone;
			}
		}
		break;
	}
}

void DisplayArcadeEffects::ChangeTitleType(int type)
{
	bool showText = false;
	bool showRes = false;
	bool showObj = false;

	switch (type)
	{
	case TitleNone:
		break;
	case TitleObject:
		showObj = true;
		break;
	case TitleResource:
		showRes = true;
		break;
	case TitleText:
		showText = true;
		break;
	}

	IControl *ctrl;
	ctrl = GetCtrl(IDC_ARCEFF_TITTEXT);
	if (ctrl) ctrl->ShowCtrl(showText);
	ctrl = GetCtrl(IDC_ARCEFF_TITRES);
	if (ctrl) ctrl->ShowCtrl(showRes);
	ctrl = GetCtrl(IDC_ARCEFF_TITOBJ);
	if (ctrl) ctrl->ShowCtrl(showObj);
	ctrl = GetCtrl(IDC_ARCEFF_TEXT_TITTEXT);
	if (ctrl) ctrl->ShowCtrl(showObj || showRes || showText);
}

DisplayArcadeWaypoint::DisplayArcadeWaypoint
(
	ControlsContainer *parent, ArcadeTemplate *templ,
	int indexGroup, int index, ArcadeWaypointInfo &waypoint,
	bool advanced
)
	: Display(parent)
{
	_enableSimulation = false;
	_enableDisplay = false;

	_template = templ;
	_indexGroup = indexGroup;
	_index = index;
	_waypoint = waypoint;

	_advanced = advanced;

	if (advanced)
		Load("RscDisplayArcadeWaypoint");
	else
		Load("RscDisplayArcadeWaypointSimple");
	if (!GetCtrl(IDC_ARCWP_HOUSEPOS))
		GetCtrl(IDC_ARCWP_HOUSEPOSTEXT)->ShowCtrl(false);
}

Control *DisplayArcadeWaypoint::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
	case IDC_ARCWP_TITLE:
		{
			CStatic *text = new CStatic(this, idc, cls);
			if (_index < 0)
				text->SetText(LocalizeString(IDS_ARCWP_TITLE1));
			else
				text->SetText(LocalizeString(IDS_ARCWP_TITLE2));
			return text;
		}
	case IDC_ARCWP_TYPE:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			ArcadeGroupInfo &gInfo = _template->groups[_indexGroup];
			int sel = 0;
			if (gInfo.side == TLogic)
			{
				for (int i=ACLOGIC; i<ACLOGICN; i++)
				{
					int index = combo->AddString(LocalizeString(IDS_AC_AND + i - ACAND));
					combo->SetValue(index, i);
					if (i == _waypoint.type) sel = index;
				}
			}
			else
			{
				for (int i=ACMOVE; i<ACN; i++)
				{
					int index = combo->AddString(LocalizeString(IDS_AC_MOVE + i - ACMOVE));
					combo->SetValue(index, i);
					if (i == _waypoint.type) sel = index;
				}
			}
			combo->SetCurSel(sel);
			return combo;
		}
	case IDC_ARCWP_SEMAPHORE:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			combo->AddString(LocalizeString(IDS_NO_CHANGE));
			int i;
			for (i=0; i<AI::NSemaphores; i++)
			{
				combo->AddString(LocalizeString(IDS_IGNORE + i));
			}
			combo->SetCurSel(_waypoint.combatMode + 1);
			return combo;
		}
	case IDC_ARCWP_FORM:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			combo->AddString(LocalizeString(IDS_NO_CHANGE));
			int i;
			for (i=0; i<AI::NForms; i++)
			{
				combo->AddString(LocalizeString(IDS_COLUMN + i));
			}
			combo->SetCurSel(_waypoint.formation + 1);
			return combo;
		}
	case IDC_ARCWP_SPEED:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			combo->AddString(LocalizeString(IDS_SPEED_UNCHANGED));
			combo->AddString(LocalizeString(IDS_SPEED_LIMITED));
			combo->AddString(LocalizeString(IDS_SPEED_NORMAL));
			combo->AddString(LocalizeString(IDS_SPEED_FULL));
			combo->SetCurSel(_waypoint.speed);
			return combo;
		}
	case IDC_ARCWP_COMBAT:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			combo->AddString(LocalizeString(IDS_COMBAT_UNCHANGED));
			combo->AddString(LocalizeString(IDS_COMBAT_CARELESS));
			combo->AddString(LocalizeString(IDS_COMBAT_SAFE));
			combo->AddString(LocalizeString(IDS_COMBAT_AWARE));
			combo->AddString(LocalizeString(IDS_COMBAT_COMBAT));
			combo->AddString(LocalizeString(IDS_COMBAT_STEALTH));
			combo->SetCurSel(_waypoint.combat);
			return combo;
		}
	case IDC_ARCWP_SEQ:
		{
			CCombo *combo = new CCombo(this, idc, cls);

			char buffer[256];
			ArcadeGroupInfo &gInfo = _template->groups[_indexGroup];
			int i, n = gInfo.waypoints.Size();
			for (i=0; i<n; i++)
			{
				ArcadeWaypointInfo &info = gInfo.waypoints[i];
				sprintf
				(
					buffer, "%d: %s %s",
					i,
					(const char *)LocalizeString(IDS_AC_MOVE + info.type - ACMOVE),
					info.description ? (const char *)info.description : ""
				);
				combo->AddString(buffer);
			}
			sprintf
			(
				buffer, "%d:",
				n
			);
			combo->AddString(buffer);

			if (_index < 0)
				combo->SetCurSel(n);
			else
				combo->SetCurSel(_index);
			return combo;
		}
	case IDC_ARCWP_DESC:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			edit->SetText(_waypoint.description ? _waypoint.description : "");
			return edit;
		}
	case IDC_ARCWP_EXPCOND:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			edit->SetText(_waypoint.expCond);
			return edit;
		}
	case IDC_ARCWP_EXPACTIV:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			edit->SetText(_waypoint.expActiv);
			return edit;
		}
	case IDC_ARCWP_SCRIPT:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			edit->SetText(_waypoint.script);
			return edit;
		}
	case IDC_ARCWP_PLACE:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			char buffer[256];
			sprintf(buffer, "%g", _waypoint.placement);
			edit->SetText(buffer);
			return edit;
		}
	case IDC_ARCWP_TIMEOUT_MIN:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			char buffer[256];
			sprintf(buffer, "%g", _waypoint.timeoutMin);
			edit->SetText(buffer);
			return edit;
		}
	case IDC_ARCWP_TIMEOUT_MID:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			char buffer[256];
			sprintf(buffer, "%g", _waypoint.timeoutMid);
			edit->SetText(buffer);
			return edit;
		}
	case IDC_ARCWP_TIMEOUT_MAX:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			char buffer[256];
			sprintf(buffer, "%g", _waypoint.timeoutMax);
			edit->SetText(buffer);
			return edit;
		}
	case IDC_ARCWP_HOUSEPOS:
		{
			if (_waypoint.idStatic < 0) return NULL;
			Vehicle *veh = NULL;
			int n = GWorld->NBuildings();
			for (int i=0; i<n; i++)
			{
				Vehicle *v = GWorld->GetBuilding(i);
				if (v && v->ID() == _waypoint.idStatic)
				{
					veh = v;
					break;
				}
			}
			if (!veh) return NULL;
			if (!veh->GetShape()) return NULL;
			if (veh->GetShape()->FindPaths() < 0) return NULL;
			const IPaths *build = veh->GetIPaths();
			if (!build) return NULL;
			n = build->NPos();
			if (n == 0) return NULL;
			CCombo *combo = new CCombo(this, idc, cls);
			for (int i=0; i<n; i++)
			{
				char buffer[256];
				sprintf(buffer, LocalizeString(IDS_HOUSE_POSITION), i + 1);
				combo->AddString(buffer);
			}
			combo->SetCurSel(_waypoint.housePos);
			return combo;
		}
	case IDC_ARCWP_SHOW:
		{
			CToolBox *toolbox = new CToolBox(this, idc, cls);
			toolbox->SetCurSel(_waypoint.showWP);
/*
			if (_waypoint.show)
				toolbox->SetCurSel(1);
			else
				toolbox->SetCurSel(0);
*/
			return toolbox;
		}
	default:
		return Display::OnCreateCtrl(type, idc, cls);
	}
}

void DisplayArcadeWaypoint::OnButtonClicked(int idc)
{
	if (idc == IDC_ARCWP_EFFECTS)
	{
		CreateChild(new DisplayArcadeEffects(this, _waypoint.effects, _advanced));
	}
	else
	{
		Display::OnButtonClicked(idc);
	}
}

void DisplayArcadeWaypoint::OnChildDestroyed(int idd, int exit)
{
	if (idd == IDD_ARCADE_EFFECTS && exit == IDC_OK)
	{
		DisplayArcadeEffects *display = dynamic_cast<DisplayArcadeEffects *>((ControlsContainer *)_child);
		Assert(display);
		_waypoint.effects = display->_effects;
	}
	Display::OnChildDestroyed(idd, exit);
}

bool DisplayArcadeWaypoint::CanDestroy()
{
	if (_exit != IDC_OK) return true;

	GameState *gstate = GWorld->GetGameState();

	CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCWP_EXPCOND));
	if (edit)
	{
		if (!gstate->CheckEvaluateBool(edit->GetText()))
		{
			FocusCtrl(IDC_ARCWP_EXPCOND);
			CreateMsgBox(MB_BUTTON_OK, gstate->GetLastErrorText());
			edit->SetCaretPos(gstate->GetLastErrorPos());
			return false;
		}
	}

	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCWP_EXPACTIV));
	if (edit)
	{
		if (!gstate->CheckExecute(edit->GetText()))
		{
			FocusCtrl(IDC_ARCWP_EXPACTIV);
			CreateMsgBox(MB_BUTTON_OK, gstate->GetLastErrorText());
			edit->SetCaretPos(gstate->GetLastErrorPos());
			return false;
		}
	}

	return true;
}

void DisplayArcadeWaypoint::Destroy()
{
	Display::Destroy();

	if (_exit != IDC_OK) return;

	CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCWP_TYPE));
	if (combo)
		_waypoint.type = (ArcadeWaypointType)combo->GetValue(combo->GetCurSel());
	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCWP_SEMAPHORE));
	if (combo)
		_waypoint.combatMode = (AI::Semaphore)(combo->GetCurSel() - 1);
	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCWP_FORM));
	if (combo)
		_waypoint.formation = (AI::Formation)(combo->GetCurSel() - 1);
	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCWP_SPEED));
	if (combo)
		_waypoint.speed = (SpeedMode)combo->GetCurSel();
	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCWP_COMBAT));
	if (combo)
		_waypoint.combat = (CombatMode)combo->GetCurSel();
	CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCWP_DESC));
	if (edit)
		_waypoint.description = edit->GetText();
	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCWP_EXPCOND));
	if (edit)
		_waypoint.expCond = edit->GetText();
	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCWP_EXPACTIV));
	if (edit)
		_waypoint.expActiv = edit->GetText();
	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCWP_SCRIPT));
	if (edit)
		_waypoint.script = edit->GetText();
	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCWP_PLACE));
	if (edit)
		_waypoint.placement = edit->GetText() ? atof(edit->GetText()) : 0;
	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCWP_TIMEOUT_MIN));
	if (edit)
		_waypoint.timeoutMin = edit->GetText() ? atof(edit->GetText()) : 0;
	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCWP_TIMEOUT_MID));
	if (edit)
		_waypoint.timeoutMid = edit->GetText() ? atof(edit->GetText()) : 0;
	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCWP_TIMEOUT_MAX));
	if (edit)
		_waypoint.timeoutMax = edit->GetText() ? atof(edit->GetText()) : 0;
	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCWP_HOUSEPOS));
	if (combo)
		_waypoint.housePos = combo->GetCurSel();
	else
		_waypoint.housePos = -1;

	CToolBox *toolbox = dynamic_cast<CToolBox *>(GetCtrl(IDC_ARCWP_SHOW));
	if (toolbox)
//		_waypoint.show = toolbox->GetCurSel() == 1;
		_waypoint.showWP = (AWPShow)toolbox->GetCurSel();
}

Control *DisplayTemplateSave::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
	case IDC_TEMPL_NAME:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			edit->SetText(Glob.header.filename);
			return edit;
		}
	case IDC_TEMPL_MODE:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			combo->AddString(LocalizeString(IDS_EXPORT_NONE));
			combo->AddString(LocalizeString(IDS_EXPORT_SINGLE));
			combo->AddString(LocalizeString(IDS_EXPORT_MULTI));
			combo->AddString(LocalizeString(IDS_EXPORT_MAIL));
			combo->SetCurSel(0);
			return combo;
		}
	default:
		return Display::OnCreateCtrl(type, idc, cls);
	}
}

/*!
	\patch_internal 1.01 Date 07/09/2001 by Jirka
	- added - offers only islands where wrp file exists
*/
Control *DisplayTemplateLoad::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	if (idc == IDC_TEMPL_TITLE)
	{
		CStatic *text = new CStatic(this, idc, cls);
		if (_merge)
		{
			text->SetText(LocalizeString(IDS_TEMPL_MERGE));
		}
		return text;
	}
	else if (idc == IDC_TEMPL_ISLAND)
	{
		CCombo *combo = new CCombo(this, idc, cls);
		int sel = 0;
		// int n = (Pars>>"CfgWorlds">>"worlds").GetSize();
		int n = (Pars >> "CfgWorldList").GetEntryCount();
		for (int i=0; i<n; i++)
		{
			const ParamEntry &entry = (Pars >> "CfgWorldList").GetEntry(i);
			if (!entry.IsClass()) continue;
			RString name = entry.GetName();
			// RString name = (Pars>>"CfgWorlds">>"worlds")[i];

			// ADDED - check if wrp file exists
			RString fullname = GetWorldName(name);
			if (!QIFStreamB::FileExist(fullname)) continue;
			
			int index = combo->AddString
			(
				Pars >> "CfgWorlds" >> name >> "description"
			);
			combo->SetData(index, name);

			if (stricmp(name, Glob.header.worldname) == 0)
				sel = index;
		}
		combo->SetCurSel(sel);
		return combo;
	}
/*
	else if (idc == IDC_TEMPL_NAME)
	{
		CCombo *combo = new CCombo(this, idc, cls);
		char buffer[256];
		sprintf
		(
			buffer, "%s*.%s",
			(const char *)GetMissionsDirectory(),
			Glob.header.worldname
		);
		WIN32_FIND_DATA info;
		HANDLE h = FindFirstFile(buffer, &info);
		if (h != INVALID_HANDLE_VALUE)
		{
			do
			{
				if 
				(
					(info.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0 &&
					info.cFileName[0] != '.'
				)
				{
					char name[256];
					strcpy(name, info.cFileName);
					char *ext = strrchr(name, '.');
					if (ext) *ext = 0;
					combo->AddString(name);
				}
			}
			while (FindNextFile(h, &info));
			FindClose(h);
		}
		combo->SortItems();
		combo->SetCurSel(0);
		return combo;
	}
*/
	return Display::OnCreateCtrl(type, idc, cls);
}

void DisplayTemplateLoad::OnComboSelChanged(int idc, int curSel)
{
	if (idc == IDC_TEMPL_ISLAND) OnIslandChanged();

	Display::OnComboSelChanged(idc, curSel);
}

void DisplayTemplateLoad::OnIslandChanged()
{
//	Assert(dynamic_cast<CCombo *>(GetCtrl(IDC_TEMPL_ISLAND)));
	CCombo *combo = static_cast<CCombo *>(GetCtrl(IDC_TEMPL_ISLAND));
	if (!combo) return;
	int index = combo->GetCurSel();
	if (index < 0) return;
	RString island = combo->GetData(index);

	Assert(dynamic_cast<CCombo *>(GetCtrl(IDC_TEMPL_NAME)));
	combo = static_cast<CCombo *>(GetCtrl(IDC_TEMPL_NAME));
	if (!combo) return;

	combo->ClearStrings();
	#ifdef _WIN32
	char buffer[256];
	sprintf
	(
		buffer, "%s*.%s",
		(const char *)GetMissionsDirectory(),
		(const char *)island
	);
	WIN32_FIND_DATA info;
	HANDLE h = FindFirstFile(buffer, &info);
	if (h != INVALID_HANDLE_VALUE)
	{
		do
		{
			if 
			(
				(info.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0 &&
				info.cFileName[0] != '.'
			)
			{
				char name[256];
				strcpy(name, info.cFileName);
				char *ext = strrchr(name, '.');
				if (ext) *ext = 0;
				combo->AddString(name);
			}
		}
		while (FindNextFile(h, &info));
		FindClose(h);
	}
	#endif
	combo->SortItems();
	combo->SetCurSel(0);
}

// static const int daysInMonth[]={31,28,31,30,31,30,31,31,30,31,30,31};
int GetDaysInMonth(int year, int month);

void DisplayIntel::UpdateDays(CCombo *combo, int day)
{
	CCombo *monthCombo = dynamic_cast<CCombo *>(GetCtrl(IDC_INTEL_MONTH));
	Assert(monthCombo);
	int month = monthCombo->GetCurSel();
	Assert(month >= 0);

	int curSel = day > 0 ? day - 1 : combo->GetCurSel();
	combo->ClearStrings();
	char buffer[4];
	for (int i=0; i<GetDaysInMonth(_intel.year, month); i++)
	{
		sprintf(buffer, "%d", i + 1);
		combo->AddString(buffer);
	}
	if (curSel < 0) curSel = 0;
	if (curSel >= combo->GetSize()) curSel = combo->GetSize() - 1;
	combo->SetCurSel(curSel);
}

void DisplayIntel::OnComboSelChanged(int idc, int curSel)
{
	switch (idc)
	{
		case IDC_INTEL_MONTH:
			{
				CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_INTEL_DAY));
				if (combo)
				{
					UpdateDays(combo);
				}
			}
			break;
	}
}

Control *DisplayIntel::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
		case IDC_INTEL_MONTH:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			int i;
			for (i=0; i<12; i++)
				combo->AddString(LocalizeString(IDS_JANUARY + i));
			combo->SetCurSel(_intel.month - 1);
			return combo;
		}
		case IDC_INTEL_DAY:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			UpdateDays(combo, _intel.day);
			return combo;
		}
		case IDC_INTEL_HOUR:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			char buffer[4];
			for (int i=0; i<24; i++)
			{
				sprintf(buffer, "%d", i);
				combo->AddString(buffer);
			}
			combo->SetCurSel(_intel.hour);
			return combo;
		}
		case IDC_INTEL_MINUTE:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			char buffer[4];
			for (int i=0; i<60; i+=5)
			{
				sprintf(buffer, "%02d", i);
				combo->AddString(buffer);
			}
			combo->SetCurSel(toInt(_intel.minute / 5.0));
			return combo;
		}
		case IDC_INTEL_RESISTANCE:
		{
			CToolBox *ctrl = new CToolBox(this, idc, cls);
			if (_intel.friends[TGuerrila][TEast] < 0.5)
				if (_intel.friends[TGuerrila][TWest] < 0.5)
					ctrl->SetCurSel(0);
				else
					ctrl->SetCurSel(1);
			else
				if (_intel.friends[TGuerrila][TWest] < 0.5)
					ctrl->SetCurSel(2);
				else
					ctrl->SetCurSel(3);
			return ctrl;
		}
/*
		{
			CSlider *slider = new CSlider(this, idc, cls);
			slider->SetRange(0, 1);
			slider->SetSpeed(0.01, 0.1);
			slider->SetThumbPos(_intel.friends[TGuerrila][TWest]);
			return slider;
		}
*/
		case IDC_INTEL_WEATHER:
		{
			CSlider *slider = new CSlider(this, idc, cls);
			slider->SetRange(0, 1);
			slider->SetSpeed(0.01, 0.1);
			slider->SetThumbPos(1.0 - _intel.weather);
			return slider;
		}
		case IDC_INTEL_FOG:
		{
			CSlider *slider = new CSlider(this, idc, cls);
			slider->SetRange(0, 1);
			slider->SetSpeed(0.01, 0.1);
			slider->SetThumbPos(_intel.fog);
			return slider;
		}
		case IDC_INTEL_WEATHER_FORECAST:
		{
			CSlider *slider = new CSlider(this, idc, cls);
			slider->SetRange(0, 1);
			slider->SetSpeed(0.01, 0.1);
			slider->SetThumbPos(1.0 - _intel.weatherForecast);
			return slider;
		}
		case IDC_INTEL_FOG_FORECAST:
		{
			CSlider *slider = new CSlider(this, idc, cls);
			slider->SetRange(0, 1);
			slider->SetSpeed(0.01, 0.1);
			slider->SetThumbPos(_intel.fogForecast);
			return slider;
		}
		case IDC_INTEL_BRIEFING_NAME:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			edit->SetText(_intel.briefingName);
			return edit;
		}
		case IDC_INTEL_BRIEFING_DESC:
		{
			CEdit *edit = new CEdit(this, idc, cls);
			edit->SetText(_intel.briefingDescription);
			return edit;
		}
	}
	return Display::OnCreateCtrl(type, idc, cls);
}

void DisplayIntel::Destroy()
{
	Display::Destroy();

	if (_exit != IDC_OK) return;
				
	CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_INTEL_MONTH));
	if (combo)
		_intel.month = combo->GetCurSel() + 1;
	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_INTEL_DAY));
	if (combo)
		_intel.day = combo->GetCurSel() + 1;
	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_INTEL_HOUR));
	if (combo)
		_intel.hour = combo->GetCurSel();
	combo = dynamic_cast<CCombo *>(GetCtrl(IDC_INTEL_MINUTE));
	if (combo)
		_intel.minute = 5 * combo->GetCurSel();
/*
	CSlider *slider = dynamic_cast<CSlider *>(GetCtrl(IDC_INTEL_RESISTANCE));
	Assert(slider);
	float resistance = slider->GetThumbPos();
	_intel.friends[TWest][TGuerrila] = _intel.friends[TGuerrila][TWest] = resistance;
	_intel.friends[TEast][TGuerrila] = _intel.friends[TGuerrila][TEast] = 1.0 - resistance;
*/
	CToolBox *ctrl = dynamic_cast<CToolBox *>(GetCtrl(IDC_INTEL_RESISTANCE));
	if (ctrl)
		switch (ctrl->GetCurSel())
		{
		case 0:
			_intel.friends[TWest][TGuerrila] = _intel.friends[TGuerrila][TWest] = 0.0;
			_intel.friends[TEast][TGuerrila] = _intel.friends[TGuerrila][TEast] = 0.0;
			break;
		case 1:
			_intel.friends[TWest][TGuerrila] = _intel.friends[TGuerrila][TWest] = 1.0;
			_intel.friends[TEast][TGuerrila] = _intel.friends[TGuerrila][TEast] = 0.0;
			break;
		case 2:
			_intel.friends[TWest][TGuerrila] = _intel.friends[TGuerrila][TWest] = 0.0;
			_intel.friends[TEast][TGuerrila] = _intel.friends[TGuerrila][TEast] = 1.0;
			break;
		case 3:
			_intel.friends[TWest][TGuerrila] = _intel.friends[TGuerrila][TWest] = 1.0;
			_intel.friends[TEast][TGuerrila] = _intel.friends[TGuerrila][TEast] = 1.0;
			break;
		}
	CSlider *slider = dynamic_cast<CSlider *>(GetCtrl(IDC_INTEL_WEATHER));
	if (slider)
		_intel.weather = 1.0 - slider->GetThumbPos();
	slider = dynamic_cast<CSlider *>(GetCtrl(IDC_INTEL_FOG));
	if (slider)
		_intel.fog = slider->GetThumbPos();
	slider = dynamic_cast<CSlider *>(GetCtrl(IDC_INTEL_WEATHER_FORECAST));
	if (slider)
		_intel.weatherForecast = 1.0 - slider->GetThumbPos();
	slider = dynamic_cast<CSlider *>(GetCtrl(IDC_INTEL_FOG_FORECAST));
	if (slider)
		_intel.fogForecast = slider->GetThumbPos();
	CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_INTEL_BRIEFING_NAME));
	if (edit)
		_intel.briefingName = edit->GetText();
	edit = dynamic_cast<CEdit *>(GetCtrl(IDC_INTEL_BRIEFING_DESC));
	if (edit)
		_intel.briefingDescription = edit->GetText();
}

#endif // #if _ENABLE_EDITOR

//! returns current (topmost) display
Display *CurrentDisplay()
{
	if (!GWorld) return NULL;

	Display *disp = dynamic_cast<Display *>(GWorld->Options());
	if (!disp) return NULL;

	ControlsContainer *ptr = disp;
	while (ptr->Child())
		ptr = ptr->Child();
	return dynamic_cast<Display *>(ptr);
}

//! creates message box in topmost display (NOT CURRENTLY USED)
bool MsgBox(RString text, int flags, int idd)
{
	Display *disp = CurrentDisplay();
	if (!disp) return false;

	disp->CreateMsgBox(flags, text, idd);
	return true;
}

//! creates message box in topmost display (NOT CURRENTLY USED)
bool MsgBox(int ids, int flags, int idd)
{
	return MsgBox(LocalizeString(ids), flags, idd);
}

//! parse name of template (only file name) and sets current mission
bool ProcessTemplateName(RString name, RString dir)
{
	char buffer[256];
	strcpy(buffer, name);

	// parse mission name

	// mission
	char *world = strchr(buffer, '.');
	if (!world) return false;
	*world = 0;
	world++;

	// world
	char *ext = strchr(world, '.');
	if (ext)
	{
		*ext = 0;
		ext++;
		Assert(stricmp(ext, "sqm") == 0);
	}

	SetMission(world, buffer, dir);
	return true;
}

//! parse name of template (only file name) and sets current mission
bool ProcessTemplateName(RString name)
{
	return ProcessTemplateName(name, MissionsDir);
}

//! parse name of template (full path) and sets current base directory and mission
bool ProcessFullName(RString name)
{
	char buffer[256];
	strcpy(buffer, name);
	// parse filename

	// "mission.sqm"
	char *ext = strrchr(buffer, '\\');
	if (!ext) return false;
	Assert(stricmp(ext + 1, "mission.sqm") == 0);
	*ext = 0;

	// world
	ext = strrchr(buffer, '.');
	if (!ext) return false;
	Assert(ext);
	RString world = ext + 1;
	*ext = 0;

	// mission
	ext = strrchr(buffer, '\\');
	if (!ext) return false;

	RString mission = ext + 1;
	*ext = 0;

	// base directory
	RString dir;
	ext = strrchr(buffer, '\\');
	if (!ext)
	{
		Assert(stricmp(buffer, "missions") == 0);
		dir = "";
	}
	else
	{
		Assert(stricmp(ext + 1, "missions") == 0);
		*(ext + 1)  = 0;
		dir = buffer;
	}

	CurrentCampaign = "";
	CurrentBattle = "";
	CurrentMission = "";
	SetBaseDirectory(dir);
	SetMission(world, mission);
	return true;
}

//! reads mission file (mission.sqm) and parse given cutscene / mission into CurrentTemplate
bool ParseCutscene(RString cutscene, bool multiplayer)
{
	RString name = GetMissionDirectory() + RString("mission.sqm");

	ParamArchiveLoad ar;
	if (!ar.LoadBin(name) && ar.Load(name) != LSOK)
	{
		CurrentTemplate.Clear();
		return false;
	}
	ATSParams params;
	ar.SetParams(&params);
	LSError result = ar.Serialize(cutscene, CurrentTemplate, 1);
	if (result == LSNoAddOn)
	{
		RString message = LocalizeString(IDS_MSG_ADDON_MISSING);
		bool first = true;
		for (int i=0; i<CurrentTemplate.missingAddOns.Size(); i++)
		{
			if (first) first = false;
			else message = message + RString(", ");
			message = message + CurrentTemplate.missingAddOns[i];
		}
		WarningMessage(message);
	}
	else if (result != LSOK)
	{
		WarningMessage("Cannot load mission");
	}
	if (ar.GetArVersion() < 7)
	{
		if (ar.Serialize("Intel", CurrentTemplate.intel, 1) != LSOK)
		{
			WarningMessage("Cannot load intel (old mission format)");
		}
	}

	if
	(
		CurrentTemplate.groups.Size() == 0 ||
		!CurrentTemplate.IsConsistent(NULL, multiplayer)
	)
	{
		CurrentTemplate.Clear();
		return false;
	}

	ArcadeUnitInfo *uInfo = CurrentTemplate.FindPlayer(); 
	if (uInfo)
	{
		Glob.header.playerSide = (TargetSide)uInfo->side;
	}
	return true;
}

//! reads mission file (mission.sqm) and parse intro cutscene into CurrentTemplate
bool ParseIntro()
{
	return ParseCutscene("Intro", false);
}

//! reads mission file (mission.sqm) and parse mission into CurrentTemplate
bool ParseMission(bool multiplayer)
{
	return ParseCutscene("Mission", multiplayer);
}

