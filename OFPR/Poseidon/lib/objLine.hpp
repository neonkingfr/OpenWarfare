#ifdef _MSC_VER
#pragma once
#endif

#ifndef _OBJ_LINE_HPP
#define _OBJ_LINE_HPP

#include "object.hpp"

class ObjectLine
{
	public:
	static LODShapeWithShadow *CreateShape();
	static void SetPos( LODShapeWithShadow *lShape, Vector3Par beg, Vector3Par end );
};

#include <Es/Memory/normalNew.hpp>

// TODO: move ObjectLineDiag from vehicleAI.cpp
class ObjectLineDiag: public ObjectColored
{
	typedef ObjectColored base;
	 
	public:
	ObjectLineDiag( LODShapeWithShadow *shape );
	void Draw( int level, ClipFlags clipFlags, const FrameBase &pos ); // virtual

	USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

#endif
