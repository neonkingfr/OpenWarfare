#ifdef _MSC_VER
#pragma once
#endif

#ifndef _PACK_FILES_HPP
#define _PACK_FILES_HPP

#include <Es/Containers/array.hpp>
#include <El/QStream/QStream.hpp>
#include <El/QStream/fileinfo.h>

class SOFStream;

#ifndef _WIN32
  #undef Zero

#endif

class IFilebankEncryption;

struct FileInfoExt
{
	RString name;
	int compressedMagic;
	int uncompressedSize;
	long startOffset;
	long time;
	long length;
	int priority;

	void Zero(){memset(this,0,sizeof(*this));}
	const char *GetKey() const {return name;}

	// >= 0 gives order of processing
	// small numbers should be included first
	// < 0 means file should be excluded
	FileInfoExt(){priority=0;}
};

TypeIsMovableZeroed(FileInfoExt)



extern const char *DefFileBankNoCompress[];
extern const char *DefFileBankEncrypt[];

struct QFProperty;

//! utility to create file banks (pbo files)
class FileBankManager
{
	AutoArray<FileInfoExt> files;
	int newestFile;
	int size;

	public:
	FileBankManager();
	~FileBankManager();
	//! create a pbo file bank
	LSError Create
	(
		const char *tgt, const char *src,
		bool compress=false, bool optimize=true,
		const char *logFile=NULL,
		const char **doNotCompress=DefFileBankNoCompress,
		const QFProperty *properties=NULL, int nProperties=0
	);
	// optimize creates pbo file with short header
	// set false on if you need to create old pbf
	void Create
	(
		QOStream &out, const char *src, 
		bool compress=false, bool optimize=true,
		const char *logFile=NULL,
		const char **doNotCompress=DefFileBankNoCompress,
		const QFProperty *properties=NULL, int nProperties=0
	);
	//! create encrypted pbo file
	void Create
	(
		QOStream &out, const char *src, 
		IFilebankEncryption *encrypt,
		const QFProperty *properties, int nProperties,
		const char **encryptExts=DefFileBankEncrypt
	);
	void ParseMasks(const char *logFile);
	void SortAndRemove(bool remove);

	// create bank in memory
	void ScanDir( RString dir, RString rel);

	void SaveHeadersOpt( QOStream &out);
	void SaveHeadersOpt( SOFStream &out);

	void SaveProperties( QOStream &out, const QFProperty *prop, int nProp );
	void SaveProperties( SOFStream &out, const QFProperty *prop, int nProp );
};
// create a pbo file
// tgt - fully qualified target name c:\temp\bank.pbo
// src - path to source directory

#endif
