// Interface of control hierarchy, displays and dialogs
#ifdef _MSC_VER
#pragma once
#endif

#ifndef _UI_CONTROLS_HPP
#define _UI_CONTROLS_HPP

//#include "wpch.hpp"
#include "optionsUI.hpp"
#include "colors.hpp"
#include "paramFileExt.hpp"
#include <Es/Containers/boolArray.hpp>

#include "time.hpp"

#include "font.hpp"
#include "global.hpp"

#include "object.hpp"
#include "interpol.hpp"
#include "rtAnimation.hpp"

#include <Es/Types/lLinks.hpp>

/*!
\file
Interface file for Controls and Control Containers hierarchy.
*/

///////////////////////////////////////////////////////////////////////////////
// class Control

class ControlsContainer;
class ControlObject;
struct ControlInObject;

//! Base abstract class for Controls hierarchy
/*!
Provides basic interface for all types of controls:
	- plain 2D controls
	- objects used as controls (or control containers)
	- 3D controls (controls mapped into selections of object control containers)
*/
class IControl
{
protected:
	//! control container by which this control is owned
	ControlsContainer *_parent;
	//! id of control
	int _idc;

	//! control is visible
	bool _visible;
	//! control is enabled (process user input)
	bool _enabled;
	//! control has input focus
	bool _focused;
	//! control is default for _parent container
	bool _default;

	//! time when OnTimer action will be performed
	UITime _timer;

	//! tooltip text
	RString _tooltip;

	//! tooltip text color
	PackedColor _tooltipColorText;
	//! tooltip box color
	PackedColor _tooltipColorBox;
	//! tooltip background color
	PackedColor _tooltipColorShade;

	//! tooltip font
	static Ref<Font> _tooltipFont;
	//! tooltip font size
	static float _tooltipSize;

public:
	//! constructor
	/*!
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	IControl(ControlsContainer *parent, int idc, const ParamEntry &cls);
	//! constructor
	/*!
		\param parent control container by which this control is owned
		\param idc id of control
	*/
	IControl(ControlsContainer *parent, int idc);
	//! destructor
	virtual ~IControl() {}

	//! returns id of control
	int IDC() {return _idc;}
	//! returns type of control
	virtual int GetType() = NULL;
	//! returns style of controls
	/*!
		Style is combination of flags used for display and behaviour of control.
	*/
	virtual int GetStyle() = NULL;

	//! returns if control is visible
	bool IsVisible() {return _visible;}
	//! sets visible flag of control
	/*!
		\param show new value of visible flag
	*/
	void ShowCtrl(bool show = true) {_visible = show;}
	//! returns if control is enabled (process user input)
	bool IsEnabled() {return _enabled;}
	//! sets enabled flag of control
	/*!
		\param enable new value of enabled flag
	*/
	void EnableCtrl(bool enable = true) {_enabled = enable;}
	//! returns if control has input focus
	bool IsFocused() {return _focused;}
	//! returns subcontrol with input focus
	/*!
		Used in controls used as containers of other controls.
		In this case return subcontrol with input focus. In the other case returns itself.
	*/
	virtual IControl *GetFocused() {return this;}
	//! event handler - performed if focus is gained
	/*!
		Called by framework if control (or some subcontrol) gains input focus.
		\param up true if focus is gaines as result of Next Control action (TAB)
		\param def true if only button like controls (defaultable) can gain focus
		\return false if focus cannot be gained
	*/
	virtual bool OnSetFocus(bool up = true, bool def = false);	// return false if focus cannot not be gained
	//! event handler - performed if focus is lost
	/*!
		Called by framework if control (or some subcontrol) loose input focus.
		\return false if focus cannot be lost
	*/
	virtual bool OnKillFocus();	// return false if focus cannot be killed
	//! returns true if control can be default (true for button like controls)
	virtual bool CanBeDefault() const {return false;}
	//! returns if control is default
	bool IsDefault() {return _default;}
	//! set control as default
	void SetDefault() {_default = true;}
	//! Return tooltip text
	RString GetTooltip() const {return _tooltip;}
	//! Set tooltip text
	void SetTooltip(RString text) {_tooltip = text;}
	//! returns time when timer will expire
	UITime GetTimer() {return _timer;}
	//! set time when timer will expire
	/*!
		\param uiTime new value of time when OnTimer will be performed
	*/
	void SetTimer(UITime uiTime) {_timer = uiTime;}
	//! event handler - performed if timer expired
	virtual void OnTimer();

	//! returns if point (<x>, <y>) is inside control's area
	//! x, y are Point2DFloat (2d viewport) coordinates
	virtual bool IsInside(float x, float y) = NULL;
	//! moves control by (<dx>, <dy>)
	//! dx, dy are relative Point2DFloat (2d viewport) coordinates
	virtual void Move(float dx, float dy) = NULL;

	//! returns subcontrol with given id
	/*!
		Used in controls used as containers of other controls.
		In this case return subcontrol with given id or itself if id matched.
		Otherwise returns NULL.
		\param idc id of wanted control
	*/
	virtual IControl *GetCtrl(int idc);
	//! returns subcontrol with given coordinates
	/*!
		Used in controls used as containers of other controls.
		In this case return subcontrol on position (<x>, <y>).
		Otherwise returns itself.
	*/
	virtual IControl *GetCtrl(float x, float y);

	//! event handler - performed if key is pressed (see Windows SDK)
	virtual bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	//! event handler - performed if key is depressed (see Windows SDK)
	virtual bool OnKeyUp(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	//! event handler - performed if char is entered (see Windows SDK)
	virtual bool OnChar(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	//! event handler - performed if IME char is entered (see Windows SDK)
	virtual bool OnIMEChar(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	//! event handler - performed if IME composition is entered (see Windows SDK)
	virtual bool OnIMEComposition(unsigned nChar, unsigned nFlags);
	//! event handler - performed if left mouse button is pressed
	/*!
		Mouse position is (<x>, <y>).
	*/
	virtual void OnLButtonDown(float x, float y);
	//! event handler - performed if left mouse button is depressed
	/*!
		Mouse position is (<x>, <y>).
	*/
	virtual void OnLButtonUp(float x, float y);
	//! event handler - performed if left mouse button is clicked
	/*!
		Mouse position is (<x>, <y>).
	*/
	virtual void OnLButtonClick(float x, float y);
	//! event handler - performed if left mouse button is double clicked
	/*!
		Mouse position is (<x>, <y>).
	*/
	virtual void OnLButtonDblClick(float x, float y);
	//! event handler - performed if right mouse button is pressed
	/*!
		Mouse position is (<x>, <y>).
	*/
	virtual void OnRButtonDown(float x, float y);
	//! event handler - performed if right mouse button is depressed
	/*!
		Mouse position is (<x>, <y>).
	*/
	virtual void OnRButtonUp(float x, float y);
	//! event handler - performed if mouse cursor is moved
	/*!
		Mouse position is (<x>, <y>).
		\param active true if mouse is in control's area
	*/
	virtual void OnMouseMove(float x, float y, bool active = true);
	//! event handler - performed if mouse cursor is not moved
	/*!
		Mouse position is (<x>, <y>).
		\param active true if mouse is in control's area
	*/
	virtual void OnMouseHold(float x, float y, bool active = true);
	//! event handler - performed if mouse wheel is moved
	/*!
		\param dz amount how much wheel was moved
	*/
	virtual void OnMouseZChanged(float dz);

	//! event handler - performed when control must be drawn
	/*!
		\param alpha transparency
	*/
	virtual void OnDraw(float alpha) = NULL;

	//! event handler - performed when tooltip is to draw
	/*!
		Mouse position is (<x>, <y>).
	*/
	virtual void DrawTooltip(float x, float y);

	//! event handler - performed to ask control if control container can be closed
	/*!
		\param code exit code of controls container
		\return true if control container can be closed
	*/
	virtual bool OnCanDestroy(int code);
	//! event handler - performed when control container is destroying
	/*!
		\param code exit code of controls container
	*/
	virtual void OnDestroy(int code);

	//! plays 2D sound (usually as reaction to some user action)
	static void PlaySound(SoundPars &pars);
};

//! Base class for plain 2D controls
class Control : public RemoveOLinks, public IControl
{
protected:
	//! type of control
	int _type;
	//! style of control (see GetStyle)
	int _style;
	//@{
	//! placement of control
	float _x;
	float _y;
	float _w;
	float _h;
	//@}

	//! size of control
	float _scale;

protected:
	//! constructor
	/*!
		Used only when control is created directly in program.
		(For example in message box.)
		Initial placement is <x>, <y>, <w>, <h>
		\param parent control container by which this control is owned
		\param type type of control
		\param idc id of control
		\param style style of control
	*/
	Control(ControlsContainer *parent, int type, int idc,
		int style, float x, float y, float w, float h);

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param type type of control
		\param idc id of control
		\param cls resource template
	*/
	Control(ControlsContainer *parent, int type, int idc, const ParamEntry &cls);

	int GetType() {return _type;}
	int GetStyle() {return _style;}
	virtual bool IsInside(float x, float y)
		{ return x >= _x && x <= _x + _w && y >= _y && y <= _y + _h;}
	//@{
	//! returns placement of control
	float X() const {return _x;}
	float Y() const {return _y;}
	float W() const {return _w;}
	float H() const {return _h;}
	//@}

	//! returns size of control
	float GetScale() const {return _scale;}
	//! set size of control
	void SetScale(float scale) {_scale = scale;}

	virtual void Move(float dx, float dy);
	//! set new placement of control
	virtual void SetPos(float x, float y, float w, float h)
	{_x = x; _y = y; _w = w; _h = h;}

	//! called by framework if parent object updates
	virtual void UpdateInfo(ControlObject *object, ControlInObject &info);
};

struct Point2DFloat;

//! Base class for object used as control (object control)
class ControlObject : public Object, public IControl
{
protected:
	//! position of object if object is in foreground (zoomed)
	Vector3 _position;
	//! model position of point where mouse cursor drags object
	Vector3 _modelPos;
	//! difference between _modelPos and _position
	Vector3 _offset;

	//! control is moving (by user)
	bool _moving;

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	ControlObject(ControlsContainer *parent, int idc, const ParamEntry &cls);
	//! destructor
	~ControlObject();

	int GetType();
	int GetStyle();
	//! returns position of control if in foreground (zoomed)
	Vector3Val GetBasePosition() const {return _position;}

	bool IsInside(float x, float y);
	//! convert 2D point to 3D point with given z-coordinate
	static Vector3 Convert2DTo3D(const Point2DFloat &point, float z);

	void Move(float dx, float dy);
	
	void OnLButtonDown(float x, float y);
	void OnLButtonUp(float x, float y);
	void OnMouseMove(float x, float y, bool active = true);
	void OnDraw(float alpha);

	//! called by framework whenever position of control changed
	virtual void UpdatePosition() {}
};

//! Object control with zoom
/*!
	Added zooming functionality to ControlObject class.
	Zoom is implemented as spline interpolation of positions.
*/
class ControlObjectWithZoom : public ControlObject, public SerializeClass
{
protected:
	//! position of object if object is in background (unzoomed)
	Vector3 _positionBack;
	
	//! control is unzoomed
	bool _inBack;
	//! control is zooming / unzooming
	bool _zooming;
	//! zoom is enabled
	bool _enableZoom;

	//! duration of zoom / unzoom action
	float _zoomDuration;

	//@{
	//! zoom implementation
	UITime _zoomStart;
	Matrix4 _zoomMatrices[3];
	float _zoomTimes[3];
	SRef<M4Function> _interpolator;
	//@}

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	ControlObjectWithZoom(ControlsContainer *parent, int idc, const ParamEntry &cls);

	int GetType();

	void OnLButtonDblClick(float x, float y);
	void OnMouseMove(float x, float y, bool active = true);

	void UpdatePosition();
	//! serialization of control's state
	LSError Serialize(ParamArchive &ar);

	//! performs zoom / unzoom action
	/*!
		\param zoom true, if zoom action is performed, false if unzoom
	*/
	void Zoom(bool zoom);
};

//! This structure describe how 2D / 3D control is mapped on object control.
struct ControlInObject
{
	//! control itself
	Ref<Control> _control;
	//! position in selection
	float x, y, w, h;
	//! name of selection
	RString selection;
	//! actual placement of control
	Vector3 posTL, posTR, posBL, posBR;
};
TypeIsMovableZeroed(ControlInObject);

//! Object control used as control container
/*!
	This class allows map 2D / 3D controls on object control.
*/
class ControlObjectContainer : public ControlObjectWithZoom
{
protected:
	typedef ControlObjectWithZoom base;

	//! array of subcontrols
	AutoArray<ControlInObject> _controls;
	//! focused subcontrol
	int _indexFocused;
	int _indexL;
	int _indexR;
	int _indexMove;

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	ControlObjectContainer(ControlsContainer *parent, int idc, const ParamEntry &cls);

	int GetType();
	IControl *GetCtrl(int idc);
	IControl *GetCtrl(float x, float y);

	IControl *GetFocused();
	bool OnSetFocus(bool up = true, bool def = false);
	bool CanBeDefault() const;

	void OnLButtonDown(float x, float y);
	void OnLButtonUp(float x, float y);
	void OnLButtonClick(float x, float y);
	void OnLButtonDblClick(float x, float y);
	void OnRButtonDown(float x, float y);
	void OnRButtonUp(float x, float y);
	void OnMouseMove(float x, float y, bool active = true);
	void OnMouseHold(float x, float y, bool active = true);
	void OnMouseZChanged(float dz);
	bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	bool OnKeyUp(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	bool OnChar(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	bool OnIMEChar(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	bool OnIMEComposition(unsigned nChar, unsigned nFlags);
	
	void OnDraw(float alpha);
	void DrawTooltip(float x, float y);

	virtual void SetPosition(Vector3Par pos);
protected:
	//! creates one owned control
	void LoadControl(const ParamEntry &ctrlCls);
	//! creates owned controls
	void LoadControls(const ParamEntry &cls);
	//! find subcontrol on given position (<x>, <y>)
	int FindControl(float x, float y);

	//! sets input focus to subcontrol 
	/*!
		Called by framework if some subcontrol gains input focus.
		\param i index of subcontrol
		\param def true if only button like controls (defaultable) can gain focus
	*/
	void SetFocus(int i, bool def = false);
	//! focus next subcontrol
	bool NextCtrl();
	//! focus previous subcontrol
	bool PrevCtrl();
	//! focus next defaultable subcontrol
	bool NextDefaultCtrl();
	//! focus previous defaultable subcontrol
	bool PrevDefaultCtrl();
};

//! Object control with animation
/*!
	Added possibility attach realtime animation to object control.
*/
class ControlObjectContainerAnim : public ControlObjectContainer
{
protected:
	//! open animation proceed
	bool _open;
	//! close animation proceed
	bool _close;
	//! zoom is performed automaticly after control is created
	bool _autoZoom;
	//@{
	//! animation implementation
	UITime _animStart;
	Ref<AnimationRT> _animation;
	WeightInfo _weights;
	Ref<Skeleton> _skeleton;
	//@}

	//! current animation phase
	float _phase;
	//@{
	//! animation speed
	float _animSpeed;
	float _invAnimSpeed;
	//@}

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	ControlObjectContainerAnim(ControlsContainer *parent, int idc, const ParamEntry &cls);

	void Animate(int level);
	void Deanimate(int level);
	bool IsAnimated(int level) const {return true;}

	//! performs open animation
	void Open();
	//! performs close animation
	void Close();

	//! returns actual animation phase
	float GetPhase() const {return _phase;}
	//! returns if control is open
	bool IsOpen() {return !_open && !_close && _phase >= 0.5;}
	//! returns if control is closed
	bool IsClosed() {return !_open && !_close && _phase <= 0;}
};

//! 2D vector
/*!
	Used especially for screen coordinates.
*/

struct Point2DFloat;

#define DrawCoord Point2DFloat
/*
{
	float x;
	float y;
	DrawCoord(float xx, float yy) {x = xx; y = yy;}
	DrawCoord() {} // no init
};
TypeIsSimple(DrawCoord);
*/

///////////////////////////////////////////////////////////////////////////////
// class CStatic

//! 2D Static control
/*!
	Used especially as static text control. Can be used also for frames or static pictures.
	Subtype of control is defined by style (see GetStyle()).
*/
class CStatic : public Control
{
friend class MsgBox;	// Message Box created static directly
private:
	//! text or picture filename
	RString _text;
protected:
	//! background color
	PackedColor _bgColor;
	//! foreground (text) color
	PackedColor _ftColor;
	//! used font
	Ref<Font> _font;
	//! font size
	float _size;
	//! picture
	Ref<Texture> _texture;

	//@{
	//! frame colors and transparency
	PackedColor _color1;
	PackedColor _color2;
	PackedColor _color3;
	PackedColor _color4;
	PackedColor _color5;
	float _alpha;
	//@}

	//@{
	//! properties of multiline text control
	AutoArray<int> _lines;
	float _firstLine;
	float _lineSpacing;
	//@}

protected:
	//! constructor
	/*!
		Used only when control is created directly in program.
		(For example in message box.)
		Initial placement is <x>, <y>, <w>, <h>
		\param parent control container by which this control is owned
		\param text initial text
		\param cls aditional resource template
	*/
	CStatic
	(
		ControlsContainer *parent, const ParamEntry &cls,
		float x, float y, float w, float h, RString text
	);

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	CStatic(ControlsContainer *parent, int idc, const ParamEntry &cls);
	
	virtual void OnDraw(float alpha);
	virtual bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	//! returns currently contained text
	RString GetText() const {return _text;}
	//! single line of multiline text
	/*!
		\param i index of line
	*/
	RString GetLine(int i) const;
	//@{
	//! sets actual text
	virtual bool SetText(RString text);	// returns true if changed
	virtual bool SetText(const char *text);	// returns true if changed
	//@}
	//! returns background color
	PackedColor GetBgColor() {return _bgColor;}
	//! sets background color
	void SetBgColor(PackedColor color) {_bgColor = color;}
	//! returns foreground color
	PackedColor GetFtColor() {return _ftColor;}
	//! sets foreground color
	void SetFtColor(PackedColor color) {_ftColor = color;}

	//! returns number of lines of multiline text
	int NLines() const;
	//! calculates total height of contained text
	float GetTextHeight() const;

protected:
	//! read frame colors from config
	void InitColors();
	//! draw single line of text
	virtual void DrawText(const char *text, float top, float alpha);
	//! returns if text is not empty
	virtual bool IsText();

	//! format multiline text
	/*!
		Split multiline text into individual lines.
	*/
	void FormatText();
};

//! Static control for display of time.
class CStaticTime : public CStatic
{
protected:
	//! displayed time
	Clock _time;
	//! blink with colon
	bool _blink;
public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
		\param blink blink with colon
	*/
	CStaticTime(ControlsContainer *parent, int idc, const ParamEntry &cls, bool blink)
		: CStatic(parent, idc, cls) {_blink = blink;}
	//! returns time
	Clock GetTime() const {return _time;}
	//! sets time
	void SetTime(Clock time) {_time = time;}

protected:
	void DrawText(const char *text, float top, float alpha);
	bool IsText();
};

///////////////////////////////////////////////////////////////////////////////
// class CStatic

//! NOT USED CURRENTLY
class CSkewStatic : public Control
{
protected:
	PackedColor _color;
	PackedColor _lineColors[5];

	float _xTL;
	float _yTL;
	float _alphaTL;
	float _xTR;
	float _yTR;
	float _alphaTR;
	float _xBL;
	float _yBL;
	float _alphaBL;
	float _xBR;
	float _yBR;
	float _alphaBR;
public:
	CSkewStatic(ControlsContainer *parent, int idc, const ParamEntry &cls);
	
	virtual void Move(float dx, float dy);

	virtual void OnDraw(float alpha);
};

///////////////////////////////////////////////////////////////////////////////
// class CHTML

//! HTML style of text
enum HTMLFormat
{
	HFError = -1,
	HFP = 0,
	HFH1,
	HFH2,
	HFH3,
	HFH4,
	HFH5,
	HFH6,
	HFImg,
};

//! Alignment of text
enum HTMLAlign
{
	HALeft,
	HACenter,
	HARight
};

//! Single text field
struct HTMLField
{
	bool nextline;
	bool bottom;
	bool bold;
	bool exclude;
	RString text;
	HTMLFormat format;
	HTMLAlign align;
	RString href;
	// image parameters
	RString condition;
	Ref<Texture> texture1;
	Ref<Texture> texture2;
	float width;
	float height;
	float indent;
	float tableWidth;

	Texture *GetTexture();
};
TypeIsMovableZeroed(HTMLField);

//! Single HTML row
struct HTMLRow
{
	HTMLAlign align;
	bool bottom;
	float width;
	float height;
	int firstField;
	int firstPos;
	int lastField;
	int lastPos;
};
TypeIsMovableZeroed(HTMLRow);

//! HTML section
struct HTMLSection
{
	//! fields in section
	AutoArray <HTMLField> fields;
	//! names of section
	AutoArray <RString> names;
	//! rows info
	AutoArray <HTMLRow> rows;
};
TypeIsMovable(HTMLSection);

//! Data part of HTML control
/*!
	Used both for 2D and 3D HTML controls.
*/
class CHTMLContainer
{
protected:
	//! array of sections
	AutoArray <HTMLSection> _sections;
	//! array of bookmarks
	AutoArray <RString> _bookmarks;
	//! currently displayed section
	int _currentSection;
	//! field with mouse over it
	int _activeField;

	//! background color
	PackedColor _bgColor;
	//! text (foreground) color
	PackedColor _textColor;
	//! color of bold text
	PackedColor _boldColor;
	//! color of link
	PackedColor _linkColor;
	//! color of active link (mouse over it)
	PackedColor _activeLinkColor;

	//@{
	//! fonts
	Ref<Font> _fontH1;
	Ref<Font> _fontH1Bold;
	float _sizeH1;
	Ref<Font> _fontH2;
	Ref<Font> _fontH2Bold;
	float _sizeH2;
	Ref<Font> _fontH3;
	Ref<Font> _fontH3Bold;
	float _sizeH3;
	Ref<Font> _fontH4;
	Ref<Font> _fontH4Bold;
	float _sizeH4;
	Ref<Font> _fontH5;
	Ref<Font> _fontH5Bold;
	float _sizeH5;
	Ref<Font> _fontH6;
	Ref<Font> _fontH6Bold;
	float _sizeH6;
	Ref<Font> _fontP;
	Ref<Font> _fontPBold;
	float _sizeP;
	//@}

	//! name of source file
	RString _filename;
	//! current indent - used only during parsing
	float _indent;
public:
	//! constructor
	/*!
		\param cls resource template
	*/
	CHTMLContainer(const ParamEntry &cls);

	// implementation
	//! returns number of section
	int NSections() const {return _sections.Size();}
	//! returns single section
	const HTMLSection &GetSection(int s) const {return _sections[s];}

	//! returns current indent
	float GetIndent() const {return _indent;}
	//! sets current indent
	void SetIndent(float indent) {_indent = indent;}

	//! add section
	/*!
		\return index of added section
	*/
	int AddSection();
	//! clear content of section
	void InitSection(int section);
	//! remove section
	void RemoveSection(int s);
	//! copy section
	void CopySection(int from, int to);
	//! find section by name
	int FindSection(const char *name);
	//! switch to section identified by name
	void SwitchSection(const char *name);
	//! returns index of currently displayed section
	int CurrentSection() const {return _currentSection;}

	//! add end of line to the end of given section
	void AddBreak(int section, bool bottom);
	//! add text to the end of given section
	void AddText
	(
		int section, RString text,
		HTMLFormat format, HTMLAlign align, bool bottom, bool bold,
		RString href, float tableWidth = 0
	);
	//! add name to given section
	void AddName(int section, RString name);
	//! add picture to the end of given section
	HTMLField *AddImage
	(
		int section, RString image, HTMLAlign align, bool bottom,
		float w, float h, RString href, RString text = RString(),
		float tableWidth = 0
	);
	//! add bookmark to control
	void AddBookmark(RString link);

	//! clear whole content of control
	void Init();
	//! loads HTML file
	/*!
		\param filename name of source file
		\param add true if content of file is added, otherwise current content is replaced
	*/
	void Load(const char *filename, bool add = false);

	//! index of bookmark current section is on
	int ActiveBookmark();

	//! returns active field (with mouse over it)
	const HTMLField *GetActiveField() const;

	//! returns background color
	PackedColor GetBgColor() const {return _bgColor;}
	//! set background color
	void SetBgColor(PackedColor color) {_bgColor = color;}
	//! returns text (foreground) color
	PackedColor GetTextColor() const {return _textColor;}
	//! set text (foreground) color
	void SetTextColor(PackedColor color) {_textColor = color;}
	//! returns link color
	PackedColor GetLinkColor() const {return _linkColor;}
	//! sets link color
	void SetLinkColor(PackedColor color) {_linkColor = color;}

	//! Split section into individual rows
	void FormatSection(int s);
	//! Split section into pages (each page becomes individual section)
	void SplitSection(int s);

	//! returns total width of page
	virtual float GetPageWidth() const = NULL;
	//! returns total height of page
	virtual float GetPageHeight() const = NULL;

	//! Return text width
	virtual float GetTextWidth(float size, Font *font, const char *text) const = NULL;

	//! returns height of paragraph text
	float GetPHeight() const {return _sizeP;}
protected:	
	//! find field with given control coordinates
	virtual int FindField(float x, float y);
};

//! 2D HTML control
class CHTML : public Control, public CHTMLContainer
{
public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	CHTML(ControlsContainer *parent, int idc, const ParamEntry &cls);
	
	virtual void OnDraw(float alpha);
	virtual void OnLButtonDown(float x, float y);
	virtual void OnMouseMove(float x, float y, bool active = true);

	float GetPageWidth() const;
	float GetPageHeight() const;
	float GetTextWidth(float size, Font *font, const char *text) const;

protected:	
	int FindField(float x, float y);
};

class IAutoComplete;

#if __ICL
	#include "autoComplete.hpp"
#endif

///////////////////////////////////////////////////////////////////////////////
// class CEdit

//! Data part of Edit control
/*!
	Used both for 2D and 3D Edit controls.
*/
class CEditContainer
{
protected:
	//! current text content
	RString _text;

	Ref<IAutoComplete> _autoComplete;

	//! text (foreground) color
	PackedColor _ftColor;
	//! color of selection
	PackedColor _selColor;
	//! used font
	Ref<Font> _font;
	//! font size
	float _size;

	//! first visible char
	int _firstVisible;
	//! beginning of selection
	int _blockBegin;
	//! end of selection
	int _blockEnd;

	//! limit for chars number
	int _maxChars;
	//! tooltip should be enabled only after editing
	bool _enableToolip;

	//! description of lines for multiline edit
	AutoArray<int> _lines;

public:
	//! constructor
	/*!
		\param cls resource template
	*/
	CEditContainer(const ParamEntry &cls);

	//! sets contained text
	virtual void SetText(RString text);
	//! sets autocomplete type
	virtual void SetAutoComplete(IAutoComplete *ac);
	//! returns contained text
	RString GetText() const {return _text;}
	//! sets maximal number of contained chars
	void SetMaxChars(int value) {_maxChars = value;}
	//! returns maximal number of contained chars
	int GetMaxChars() const {return _maxChars;}
	//! sets position of caret
	void SetCaretPos(int pos);
	//! generic reaction on key pressed
	bool DoKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	//! generic reaction on char entered
	bool DoChar(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	//! generic reaction on IME char entered
	bool DoIMEChar(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	//! generic reaction on IME composition entered
	bool DoIMEComposition(unsigned nChar, unsigned nFlags);

protected:
//	virtual int FindPos(float x, float y);
	//! returns if edit is multiline
	virtual bool IsMulti() const = NULL;

	//@{
	//! conversion between character end control position
	virtual float PosToX(RString text, int pos) const = NULL;
	virtual int XToPos(RString text, float x) const = NULL;
	//@}

	//! forced position to be visible (scroll content)
	virtual void EnsureVisible(int pos) = NULL;

	//! current line of multiline edit
	int CurLine() const;
	//! first visible line of multiline edit
	int FirstLine() const;
	//! returns single line of multiline edit
	RString GetLine(int i) const;
	//! format multiline text
	/*!
		Split multiline text into individual lines.
	*/
	virtual void FormatText() = NULL;

	//@{
	//! operations with selection
	void BlockDelete();
	void BlockCopy();
	void BlockCut();
	void BlockPaste();
	//@}

	//! find next char position
	int NextPos(int pos);
	//! find previous char position
	int PrevPos(int pos);
};

//! 2D edit control
class CEdit : public Control, public CEditContainer
{
public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	CEdit(ControlsContainer *parent, int idc, const ParamEntry &cls);

	virtual void OnDraw(float alpha);
	virtual bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	virtual bool OnChar(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	virtual bool OnIMEChar(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	virtual bool OnIMEComposition(unsigned nChar, unsigned nFlags);
	virtual void OnLButtonDown(float x, float y);
	virtual void OnLButtonUp(float x, float y);
	virtual void OnMouseMove(float x, float y, bool active = true);

protected:
	bool IsMulti() const;

	//! find position in text for coordinates (<x>, <y>)
	int FindPos(float x, float y);

	float PosToX(RString text, int pos) const;
	int XToPos(RString text, float x) const;

	void EnsureVisible(int pos);
	//! draw single line of text
	void DrawText(const char *text, int offset, float top, float alpha);

	void FormatText();
};

///////////////////////////////////////////////////////////////////////////////
// class CButton

//! 2D Button control 
class CButton : public Control
{
friend class MsgBox;
protected:
	//! displayed text
	RString _text;
	//! text (foreground) color
	PackedColor _ftColor;
	//! used font
	Ref<Font> _font;
	//! font size
	float _size;

	//@{
	//! background colors
	PackedColor _color1;
	PackedColor _color2;
	PackedColor _color3;
	PackedColor _color4;
	PackedColor _color5;
	//@}

	//@{
	//! sounds
	SoundPars _pushSound;
	SoundPars _clickSound;
	SoundPars _escapeSound;
	//@}

	//! current state (pressed / depressed)
	bool _state;

	//! user action (expression) executed when button is pressed
	RString _action;

protected:
	//! constructor
	//! constructor
	/*!
		Used only when control is created directly in program.
		(For example in message box.)
		Initial placement is <x>, <y>, <w>, <h>
		\param parent control container by which this control is owned
		\param cls aditional resource template
	*/
	CButton(ControlsContainer *parent, const ParamEntry &cls, float x, float y, float w, float h);

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	CButton(ControlsContainer *parent, int idc, const ParamEntry &cls);

	virtual bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	virtual bool OnKeyUp(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	virtual void OnLButtonDown(float x, float y);
	virtual void OnLButtonUp(float x, float y);
	virtual void OnDraw(float alpha);

	//! event handler - performed if button is clicked
	virtual void OnClicked();

	//! returns displayed text
	RString GetText() {return _text;}
	//! sets displayed text
	void SetText(RString text) {_text = text;}
	//! returns text (foreground) color
	PackedColor GetFtColor() {return _ftColor;}
	//! sets text (foreground) color
	void SetFtColor(PackedColor color) {_ftColor = color;}

	//! returns user action
	RString GetAction() {return _action;}
	//! sets user action
	void SetAction(RString action) {_action = action;}

	bool CanBeDefault() const {return true;}

protected:
	//! read background colors from config
	void InitColors();
};

///////////////////////////////////////////////////////////////////////////////
// class CActiveText

//! 2D Active text control
class CActiveText : public Control
{
protected:
	//! displayed text
	RString _text;
	//! used font
	Ref<Font> _font;
	//! font size
	float _size;
	//! text color
	PackedColor _color;
	//! activated text color (if mouse cursor is over control)
	PackedColor _colorActive;

	//! if mouse is over control
	bool _active;

	//@{
	//! sounds
	SoundPars _enterSound;
	SoundPars _pushSound;
	SoundPars _clickSound;
	SoundPars _escapeSound;
	//@}

	//! user action (expression) executed when button is pressed
	RString _action;

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	CActiveText(ControlsContainer *parent, int idc, const ParamEntry &cls);

	virtual bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	virtual bool OnKeyUp(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	virtual void OnMouseMove(float x, float y, bool active = true);
	virtual void OnMouseHold(float x, float y, bool active = true);
	virtual void OnLButtonDown(float x, float y);
	virtual void OnLButtonUp(float x, float y);
	virtual void OnDraw(float alpha);

	//! returns displayed text
	RString GetText() {return _text;}
	//! sets displayed text
	void SetText(RString text) {_text = text;}

	//! returns user action
	RString GetAction() {return _action;}
	//! sets user action
	void SetAction(RString action) {_action = action;}

	//! returns if control is active (mouse cursor is over control)
	bool IsActive() const {return _active;}

	bool CanBeDefault() const {return true;}
};

///////////////////////////////////////////////////////////////////////////////
// class CSlider

//! Data part of Slider control
/*!
	Used both for 2D and 3D Slider controls.
*/
class CSliderContainer
{
protected:
	//! minimal value represented by slider
	float _minPos;
	//! maximal value represented by slider
	float _maxPos;
	//! current value represented by slider
	float _curPos;
	//! value difference performed by moving by one line
	float _lineStep;
	//! value difference performed by moving by one page
	float _pageStep;

	//! mouse position on thumb
	float _thumbOffset;
	//! thumb is moving
	bool _thumbLocked;

	//! foreground (lines) color
	PackedColor _color;

public:
	//! constructor
	/*!
		\param cls resource template
	*/
	CSliderContainer(const ParamEntry &cls);

	//! returns current value
	float GetThumbPos() {return _curPos;}
	//! retrieves minimal and maximal value
	void GetRange(float &min, float &max) {min = _minPos; max = _maxPos;}
	//! returns difference between maximal and minimal value
	float GetRange() {return _maxPos - _minPos;}
	//! retrieves amount of change processed after user input
	void GetSpeed(float &line, float &page) {line = _lineStep; page = _pageStep;}
	//! sets current value
	void SetThumbPos(float pos)
	{
		if (pos <= _minPos)
			_curPos = _minPos;
		else if (pos >= _maxPos)
			_curPos = _maxPos;
		else
			_curPos = pos;
	}
	//! sets minimal and maximal value
	void SetRange(float min, float max)
	{
		if (min <= max)
		{
			_minPos = min;
			_maxPos = max;
		}
		if (_curPos < _minPos)
			_curPos = _minPos;
		else if (_curPos > _maxPos)
			_curPos = _maxPos;
	}
	//! sets amount of change processed after user input
	void SetSpeed(float line, float page) {_lineStep = line; _pageStep = page;}
};

//! 2D slider control
class CSlider : public Control, public CSliderContainer
{
public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	CSlider(ControlsContainer *parent, int idc, const ParamEntry &cls);

	virtual void OnLButtonDown(float x, float y);
	virtual void OnLButtonUp(float x, float y);
	virtual void OnMouseMove(float x, float y, bool active = true);
	virtual void OnDraw(float alpha);
};

///////////////////////////////////////////////////////////////////////////////
// class CScrollBar (used in ListBox etc.)

//! Encapsulates properies of scroll bar in some controls
/*!
	Used in listbox and combo box (selector).
*/
class CScrollBar
{
	//@{
	//! placement
	float _x;
	float _y;
	float _w;
	float _h;
	//@}

	//! minimal represented value
	float _minPos;
	//! maximal represented value
	float _maxPos;

	//! current value
	float _curPos;
	//! thumb size
	float _page;
	//! value difference performed by moving by one line
	float _lineStep;
	//! value difference performed by moving by one page
	float _pageStep;

	//! mouse position on thumb
	float _thumbOffset;
	//! thumb is moving
	bool _thumbLocked;
	
	//! is enabled (false if parent control has no items)
	bool _enabled;

	//! foreground (lines) color
	PackedColor _color;

public:
	//! constructor
	CScrollBar();

	//@{
	//! event handler - events received from control
	void OnLButtonDown(float x, float y);
	void OnLButtonUp(float x, float y);
	void OnMouseHold(float x, float y);
	void OnDraw(float alpha);
	//@}

	//! returns if scrolling is enabled
	bool IsEnabled() const {return _enabled;}
	//! enables / disables scrolling
	void Enable(bool enable = true) {_enabled = enable;}
	//! check if given position (<x>, <y>) is inside scroll bar
	bool IsInside(float x, float y)
		{ return x >= _x && x < _x + _w && y >= _y && y < _y + _h;}
	//! returns current value
	float GetPos() {return _curPos;}
	//! sets current value
	void SetPos(float pos)
	{
		if (pos <= _minPos)
			_curPos = _minPos;
		else if (pos >= _maxPos)
			_curPos = _maxPos;
		else
			_curPos = pos;
	}
	//! sets minimal and maximal value
	void SetRange(float min, float max, float page)
	{
		_minPos = min;
		_maxPos = max;
		_page = page;
		saturateMax(_maxPos, _minPos + page);
		saturate(_curPos, _minPos, _maxPos - page);
	}
	//! sets amount of change processed after user input
	void SetSpeed(float line, float page) {_lineStep = line; _pageStep = page;}
	//! sets scroll bar placement
	void SetPosition(float x, float y, float w, float h)
	{
		_x = x; _y = y; _w = w; _h = h;
	}
	//! sets foreground (lines) color
	void SetColor(PackedColor color) {_color = color;}
	//! returns if thumb is moving
	bool IsLocked() const {return _thumbLocked;}
};

///////////////////////////////////////////////////////////////////////////////
// class CProgressBar

//! 2D Progress Bar control
class CProgressBar : public Control
{
	//! minimal value
	float _minPos;
	//! maximal value
	float _maxPos;
	//! current value
	float _curPos;

	//! color of frame
	PackedColor _frameColor;
	//! color of bar
	PackedColor _barColor;

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	CProgressBar(ControlsContainer *parent, int idc, const ParamEntry &cls);

	virtual void OnDraw(float alpha);

	//! returns current value
	float GetPos() {return _curPos;}
	//! sets current value
	void SetPos(float pos)
	{
		if (pos <= _minPos)
			_curPos = _minPos;
		else if (pos >= _maxPos)
			_curPos = _maxPos;
		else
			_curPos = pos;
	}
	//! retrieves minimal and maximal value
	void GetRange(float &min, float &max) {min = _minPos; max = _maxPos;}
	//! returns difference between maximal and minimal value
	float GetRange() {return _maxPos - _minPos;}
	//! sets minimal and maximal value
	void SetRange(float min, float max)
	{
		if (min <= max)
		{
			_minPos = min;
			_maxPos = max;
		}
		if (_curPos < _minPos)
			_curPos = _minPos;
		else if (_curPos > _maxPos)
			_curPos = _maxPos;
	}

	//! returns color of frame
	PackedColor GetFrameColor() const {return _frameColor;}
	//! sets color of frame
	void SetFrameColor(PackedColor color) {_frameColor = color;}
	//! returns color of bar
	PackedColor GetBarColor() const {return _barColor;}
	//! sets color of frame
	void SetBarColor(PackedColor color) {_barColor = color;}
};

///////////////////////////////////////////////////////////////////////////////
// class CListBox

//! represents one row of List Box or Combo Box control
struct CListBoxItem
{
	RString text;
	RString data;
	int value;
	PackedColor ftColor;
	PackedColor selColor;
	Ref<Texture> texture;
};
TypeIsMovableZeroed(CListBoxItem);

//! Data part of List Box control
/*!
	Used both for 2D and 3D List Box controls.
*/
class CListBoxContainer : public AutoArray<CListBoxItem>
{
protected:
	//! text (foreground) color
	PackedColor _ftColor;
	//! text (foreground) color of selected row
	PackedColor _selColor;
	//! index of selected row
	int _selString;
	//! index (and offset) topmost row
	float _topString;
	//! hilite selection
	bool _showSelected;
	//! hilite selection
	bool _readOnly;

public:
	//! constructor
	/*!
		\param cls resource template
	*/
	CListBoxContainer(const ParamEntry &cls);

	//! returns number of rows
	virtual int GetSize() {return Size();}
	//! returns index of selected row
	int GetCurSel() {return _selString;};

	//! returns if selected row will be hilited
	bool IsReadOnly() const {return _readOnly;}
	//! sets if selected row will be hilited
	void SetReadOnly(bool set = true) {_readOnly = set;}

	//! returns text (foreground) color of selected row
	PackedColor GetSelColor() {return _selColor;}
	//! sets text (foreground) color of selected row
	void SetSelColor(PackedColor color) {_selColor = color;}
	//! returns text (foreground) color
	PackedColor GetFtColor() {return _ftColor;}
	//! sets text (foreground) color
	void SetFtColor(PackedColor color) {_ftColor = color;}
	//! sets if selected row will be hilited
	void ShowSelected(bool show = true) {_showSelected = show;}

	//! returns text of given row
	virtual RString GetText(int i)
	{
		if (i < 0 || i >= GetSize())
			return NULL;
		return Get(i).text;
	}
	//! returns data of given row
	virtual RString GetData(int i)
	{
		if (i < 0 || i >= GetSize())
			return NULL;
		return Get(i).data;
	}
	//! returns value of given row
	virtual int GetValue(int i)
	{
		if (i < 0 || i >= GetSize())
			return 0;
		return Get(i).value;
	}
	//! returns text color of given row
	PackedColor GetFtColor(int i)
	{
		if (i < 0 || i >= GetSize())
			return PackedBlack;
		return Get(i).ftColor;
	}
	//! returns selected text color of given row
	PackedColor GetSelColor(int i)
	{
		if (i < 0 || i >= GetSize())
			return PackedBlack;
		return Get(i).selColor;
	}
	//! returns picture on given row
	Texture *GetTexture(int i)
	{
		if (i < 0 || i >= GetSize())
			return NULL;
		return Get(i).texture;
	}
	//! removes all rows
	virtual void ClearStrings()
	{
		Clear();
	}
	//! removes all rows
	virtual void RemoveAll()
	{
		ClearStrings();
		_selString = -1;
		_topString = 0;
	}
	//! add new row and set text for it
	virtual int AddString(RString text)
	{
		int index = Add();
		Set(index).text = text;
		Set(index).data = "";
		Set(index).value = 0;
		Set(index).ftColor = _ftColor;
		Set(index).selColor = _selColor;
		return index;
	}
	//! sets data of given row
	void SetData(int i, RString data)
	{
		if (i < 0 || i >= GetSize()) return;
		Set(i).data = data;
	}
	//! sets value of given row
	void SetValue(int i, int value)
	{
		if (i < 0 || i >= GetSize()) return;
		Set(i).value = value;
	}
	//! sets text color of given row
	void SetFtColor(int i, PackedColor color)
	{
		if (i < 0 || i >= GetSize()) return;
		Set(i).ftColor = color;
	}
	//! sets selected text color of given row
	void SetSelColor(int i, PackedColor color)
	{
		if (i < 0 || i >= GetSize()) return;
		Set(i).selColor = color;
	}
	//! sets picture on given row
	void SetTexture(int i, Texture *texture)
	{
		if (i < 0 || i >= GetSize()) return;
		Set(i).texture = texture;
	}
	//! insert new row and set text for it
	void InsertString(int i, RString text);
	//! delete row
	void DeleteString(int i);
	//! sorts rows by their texts
	void SortItems();
	//! sorts rows by their values
	void SortItemsByValue();

	//! event handler - performed if mouse wheel is moved
	/*!
		\param dz amount how much wheel was moved
	*/
	void OnMouseZChanged(float dz);
	//! returns number of rows that fits into control at once
	virtual	float NRows() const = NULL;
	
	//! checks if listbox is properly scrolled after items changed
	void Check();
};

struct Rect2DFloat;

//! 2D List Box control
class CListBox : public Control, public CListBoxContainer
{
protected:
	//! used font
	Ref<Font> _font;
	//! font size
	float _size;
	//! height of single row
	float _rowHeight;

	//! number of rows that fits into control at once
	float _showStrings;

	//@{
	//! scrolling implementation
	bool _scrollUp;
	bool _scrollDown;
	UITime _scrollTime;
	//@}

	//! scroll bar used by control
	CScrollBar _scrollbar;
public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	CListBox(ControlsContainer *parent, int idc, const ParamEntry &cls);

	void SetPos(float x, float y, float w, float h);

	virtual bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	virtual void OnMouseMove(float x, float y, bool active = true);
	virtual void OnMouseHold(float x, float y, bool active = true);
	virtual void OnMouseZChanged(float dz);
	virtual void OnLButtonDown(float x, float y);
	virtual void OnLButtonUp(float x, float y);
	virtual void OnLButtonDblClick(float x, float y);
	virtual void OnDraw(float alpha);

	//! event handler - performed if selection is changed
	virtual void OnSelChanged(int curSel);
	virtual bool OnKillFocus();	// return false if focus cannot be killed

	//! sets selected row
	void SetCurSel(int sel, bool sendUpdate = true);
	//! sets height of single row
	void SetRowHeight(float height)
	{
		_rowHeight = floatMax(_size, height);
	}
	//! draws single row
	void DrawItem
	(
		float alpha, int i, bool selected, float top,
		Rect2DFloat &rect
//		float topClip, float bottomClip
	); 
	float NRows() const {return _showStrings;}
};

///////////////////////////////////////////////////////////////////////////////
// class CTree

//! represents one item of Tree control
struct CTreeItem : public RemoveLLinks
{
friend class CTree;
protected:
	//! item is expanded
	bool expanded;	// use CTree::Expand() to set

public:
	//! level (generation) of item - 0 for root
	int level;
	//! link to parent item - NULL for root
	LLink<CTreeItem> parent;
	//! list of all child items
	RefArray<CTreeItem> children;

	//! visible text
	RString text;
	//! additional text attached to item
	RString data;
	//! additional value attached to item
	int value;

	//! constructor
	/*!
		\param level level (generation) of item
		\param parent parent item
	*/
	CTreeItem(int level, CTreeItem *parent);
	//! creates new item and adds it into child list
	CTreeItem *AddChild();
	//! creates new item and inserts it into child list
	CTreeItem *InsertChild(int pos);
	//! sorts child list by contained texts
	/*!
		\param reversed true for descending sort
	*/
	void SortChildren(bool reversed = false);

	//! expand / collapse item and all subitems recursivly
	/*!
		\param expand true for expansion, false for collapsion
	*/
	void ExpandTree(bool expand = true);
	//! returns next accessible (child of expanded) item
	CTreeItem *GetNextVisible();
	//! returns previous accessible (child of expanded) item
	CTreeItem *GetPrevVisible();
	//! returns last accessible (child of expanded) item
	CTreeItem *GetLastVisible();
	//! returns younger brother of item
	CTreeItem *GetYBrother();
	//! returns older brother of item
	CTreeItem *GetOBrother();
	//! returns total size of accessible (child of expanded) items
	int NVisibleItems();
};

//! 2D Tree control
class CTree : public Control
{
protected:
	//! root item
	Ref<CTreeItem> _root;
	//! selected item
	LLink<CTreeItem> _selected;
	//! first accessible item
	LLink<CTreeItem> _firstVisible;
	
	//! background color
	PackedColor _bgColor;
	//! foreground (text) color
	PackedColor _ftColor;
	//! foreground (text) color of selected item
	PackedColor _selColor;

	//! used font
	Ref<Font> _font;
	//! font size
	float _size;

	//@{
	//! scrolling implementation
	bool _scrollUp;
	bool _scrollDown;
	UITime _scrollTime;
	float _offset;
	//@}

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	CTree(ControlsContainer *parent, int idc, const ParamEntry &cls);
	
	//! returns root item
	CTreeItem *GetRoot() const {return _root;}
	//! returns selected item
	CTreeItem *GetSelected() const {return _selected;}
	//! sets selected item
	void SetSelected(CTreeItem *item);
	//! expand / collapse given item
	/*!
		\param expand true for expansion, false for collapsion
	*/
	void Expand(CTreeItem *item, bool expand = true);
	//! expand items to ensure given item will be accessible
	void EnsureVisible(CTreeItem *item);

	//! remove all items
	void RemoveAll();

	void OnDraw(float alpha);
	void OnLButtonDown(float x, float y);
	void OnLButtonDblClick(float x, float y);
	void OnMouseMove(float x, float y, bool active = true);
	void OnMouseHold(float x, float y, bool active = true);
	void OnMouseZChanged(float dz);
	bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	bool OnChar(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	
	//! draw single item
	void DrawItem
	(
		float alpha, CTreeItem *item,
		float top, float topClip, float bottomClip, bool first
	); 

protected:
	//! find item on given screen coordinates (<x>, <Y>)
	CTreeItem *FindItem(float x, float y);
	//! find first accessible item
	CTreeItem *FindFirstVisible();
};

///////////////////////////////////////////////////////////////////////////////
// class CToolBox

//! 2D Tool Box control
/*!
	Used for selection of one value from choice.
	Behave as group of Radio Buttons in Windows.
*/
class CToolBox : public Control
{
protected:
	//! all texts
	AutoArray<RString> _strings;
	//! number of rows
	int _rows;
	//!number of columns
	int _columns;

	//! foreground (text) color
	PackedColor _ftColor;
	//! background color
	PackedColor _bgColor;
	//! foreground (text) color if control has input focus
	PackedColor _ftSelectColor;
	//! background color if control has input focus
	PackedColor _bgSelectColor;
	//! foreground (text) color if control is disabled
	PackedColor _ftDisabledColor;
	//! background color if control is disabled
	PackedColor _bgDisabledColor;
	//! used font
	Ref<Font> _font;
	//! font size
	float _size;

	//! currently selected text
	int _selected;

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	CToolBox(ControlsContainer *parent, int idc, const ParamEntry &cls);

	virtual bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	virtual void OnLButtonDown(float x, float y);
	virtual void OnDraw(float alpha);

	//! returns given text
	RString GetText(int i)
	{
		if (i < 0 || i >= _strings.Size())
			return NULL;
		return _strings[i];
	}
	//! sets given text
	void SetText(int i, RString text)
	{
		if (i < 0 || i >= _strings.Size())
			return;
		_strings[i] = text;
	}
	//! returns index of selected text
	int GetCurSel() {return _selected;}
	//! sets index of selected text
	void SetCurSel(int sel)
	{
		if (sel < 0) sel = 0;
		if (sel >= _strings.Size()) sel = _strings.Size() - 1;
		_selected = sel;
	}
	
	//! returns foreground (text) color
	PackedColor GetFtColor() {return _ftColor;}
	//! sets foreground (text) color
	void SetFtColor(PackedColor color) {_ftColor = color;}

	//! returns if given item is selected
	virtual bool IsSelected(int i) const;
	//! change selection of given item
	virtual void ChangeSelection(int i);
};

///////////////////////////////////////////////////////////////////////////////
// class CCheckBoxes

//! 2D Check Boxes control
/*!
	Used for selection of several values from choice.
	Behave as group of Check Boxes in Windows.
*/
class CCheckBoxes : public CToolBox
{
protected:
	//! selection description
	/*!
		\todo usage of PackedBoolArray limits number of texts to 8 * sizeof(int) 
	*/
	PackedBoolArray _array;

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	CCheckBoxes(ControlsContainer *parent, int idc, const ParamEntry &cls);

	virtual bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);

	virtual bool IsSelected(int i) const;
	virtual void ChangeSelection(int i);

	//! returns selection description
	virtual PackedBoolArray GetArray() const {return _array;}
	//! sets selection description
	void SetArray(PackedBoolArray array) {_array = array;}
};

///////////////////////////////////////////////////////////////////////////////
// class CCombo

//! 2D Selector (Combo Box) control
class CCombo : public Control, public CListBoxContainer
{
protected:
	//! height of control if expanded
	float _wholeHeight;
	//! background color
	PackedColor _bgColor;
	//! used font
	Ref<Font> _font;
	//! font size
	float _size;

	//! number of rows that fits into control at once
	float _showStrings;

	//! if control is expanded
	bool _expanded;
	//@{
	//! scrolling implementation
	bool _scrollUp;
	bool _scrollDown;
	UITime _scrollTime;
	//@}

	//! item over which is mouse cursor
	int _mouseSel;
	//! scroll bar used by control
	CScrollBar _scrollbar;
public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	CCombo(ControlsContainer *parent, int idc, const ParamEntry &cls);

	virtual bool IsInside(float x, float y);
	void SetPos(float x, float y, float w, float h);

	virtual bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	virtual void OnLButtonDown(float x, float y);
	virtual void OnLButtonUp(float x, float y);
	virtual void OnMouseMove(float x, float y, bool active = true);
	virtual void OnMouseHold(float x, float y, bool active = true);
	virtual void OnMouseZChanged(float dz);
	virtual void OnDraw(float alpha);

	//! event handler - performed if selection is changed
	virtual void OnSelChanged(int curSel);
	virtual bool OnKillFocus();	// return false if focus cannot be killed

	//! returns default text (foreground) color for selected item
	PackedColor GetSelColor() {return _selColor;}
	//! sets default text (foreground) color for selected item
	void SetSelColor(PackedColor color) {_selColor = color;}
	//! returns default text (foreground) color
	PackedColor GetFtColor() {return _ftColor;}
	//! sets default text (foreground) color
	void SetFtColor(PackedColor color) {_ftColor = color;}
	//! returns default background color
	PackedColor GetBgColor() {return _bgColor;}
	//! sets default background color
	void SetBgColor(PackedColor color) {_bgColor = color;}

	//! sets selected row
	void SetCurSel(int sel, bool sendUpdate = true);
	void RemoveAll()
	{
		CListBoxContainer::RemoveAll();
		_expanded = false;
	}
	void DeleteString(int i)
	{
		CListBoxContainer::DeleteString(i);
		if (GetSize() == 0) _expanded = false;
	}

	float NRows() const {return _showStrings;}

protected:
	//! ensures that mouse selection is visible (scroll if needed)
	void SetTopString();
	//! returns if point (<x>, <y>) is inside extended control area
	bool IsInsideExt(float x, float y);
};

///////////////////////////////////////////////////////////////////////////////
// class Control3D

//! Base class for 3D controls (controls mapped to some selection on object control)
class Control3D : public Control
{
protected:
	//! position of top left point of control
	Vector3 _position;
	//@{
	//! orientation (and size) of control
	Vector3 _right;
	Vector3 _down;
	//@}

	//! rotation of control
	float _angle;
	//@{
	//! position of point in control coordinates
	/*!
		This value is set by IsInside function and can be used later.
	*/
	float _u;
	float _v;
	//@}

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param type type of control
		\param idc id of control
		\param cls resource template
	*/
	Control3D(ControlsContainer *parent, int type, int idc, const ParamEntry &cls);

	void UpdateInfo(ControlObject *object, ControlInObject &info);
	bool IsInside(float x, float y);

	//! returns point in center of control
	Vector3 GetCenter() {return _position + 0.5 * (_right + _down);}
};

///////////////////////////////////////////////////////////////////////////////
// class C3DStatic

//! 3D Static control
/*!
	Used especially as static text control. Can be used also for frames or static pictures.
	Subtype of control is defined by style (see GetStyle()).
*/
class C3DStatic : public Control3D
{
protected:
	//! text or picture filename
	RString _text;
	//! picture
	Ref<Texture> _texture;
	//! used font
	Ref<Font> _font;
	//! text (foreground) color
	PackedColor _color;
	//! background color
	PackedColor _bgColor;
	//! how much of whole control height is text height
	float _hCoef;

	//! description of lines for multiline control
	AutoArray<int> _lines;
	//! number of lines visible concurrently in control
	int _maxLines;

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	C3DStatic(ControlsContainer *parent, int idc, const ParamEntry &cls);

	//! returns currently contained text
	RString GetText() {return _text;}
	//! sets actual text
	void SetText(RString text);
	//! format multiline text
	/*!
		Split multiline text into individual lines.
	*/
	void FormatText();

	void OnDraw(float alpha);

protected:
	//! draw single line of text
	void DrawText(const char *text, Vector3Par top, Vector3Par down, PackedColor color);

	//! single line of multiline text
	/*!
		\param i index of line
	*/
	RString GetLine(int i) const;
};

///////////////////////////////////////////////////////////////////////////////
// class C3DEdit

//! 3D Edit control
class C3DEdit : public Control3D, public CEditContainer
{
protected:
	//! number of lines visible concurrently in control
	int _maxLines;

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	C3DEdit(ControlsContainer *parent, int idc, const ParamEntry &cls);

	void OnDraw(float alpha);
	bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	bool OnChar(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	bool OnIMEChar(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	bool OnIMEComposition(unsigned nChar, unsigned nFlags);
	void OnLButtonDown(float x, float y);
	void OnLButtonUp(float x, float y);
	void OnMouseMove(float x, float y, bool active = true);

protected:
	bool IsMulti() const;

	//! calculates position (character) in control of point (<x>, <y>) in control coordinates
	int FindPos(float x, float y);

	//! calculates offset of given text position in control
	Vector3 PosToDir(RString text, int pos) const;
	float PosToX(RString text, int pos) const;
	int XToPos(RString text, float x) const;

	void EnsureVisible(int pos);
	//! draw single line of text
	void DrawText(const char *text, int offset, Vector3Par top, Vector3Par down, float alpha);

	void FormatText();
};

///////////////////////////////////////////////////////////////////////////////
// class C3DActiveText

//! 3D Active text control
class C3DActiveText : public Control3D
{
protected:
	//! displayed text or picture filename
	RString _text;
	//! picture
	Ref<Texture> _texture;
	//! used font
	Ref<Font> _font;
	//! foreground (text) color
	PackedColor _color;
	//! active foreground (text) color  (if mouse cursor is over control)
	PackedColor _colorActive;

	//! if mouse is over control
	bool _active;

	//@{
	//! sounds
	SoundPars _enterSound;
	SoundPars _pushSound;
	SoundPars _clickSound;
	SoundPars _escapeSound;
	//@}

	//! user action (expression) executed when button is pressed
	RString _action;

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	C3DActiveText(ControlsContainer *parent, int idc, const ParamEntry &cls);

	virtual bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	virtual bool OnKeyUp(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	virtual void OnMouseMove(float x, float y, bool active = true);
	virtual void OnMouseHold(float x, float y, bool active = true);
	virtual void OnLButtonDown(float x, float y);
	virtual void OnLButtonUp(float x, float y);
	virtual void OnDraw(float alpha);

	//! returns displayed text
	RString GetText() {return _text;}
	//! sets displayed text
	void SetText(RString text);

	//! returns user action
	RString GetAction() {return _action;}
	//! sets user action
	void SetAction(RString action) {_action = action;}

	//! returns if control is active (mouse cursor is over control)
	bool IsActive() const {return _active;}

	bool CanBeDefault() const {return true;}
};

///////////////////////////////////////////////////////////////////////////////
// class C3DSlider

//! 3D Slider control
class C3DSlider : public Control3D, public CSliderContainer
{
public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	C3DSlider(ControlsContainer *parent, int idc, const ParamEntry &cls);

	void OnLButtonDown(float x, float y);
	void OnLButtonUp(float x, float y);
	void OnMouseMove(float x, float y, bool active = true);
	void OnDraw(float alpha);
};

///////////////////////////////////////////////////////////////////////////////
// class C3DScrollBar (used in 3DListBox etc.)

//! Encapsulates properies of 3D scroll bar in some controls
/*!
	Used in 3D listbox and 3D combo box (selector).
*/
class C3DScrollBar
{
	//! position of top left point
	Vector3 _position;
	//@{
	//! orientation (and size)
	Vector3 _right;
	Vector3 _down;
	//@}

	//! minimal represented value
	float _minPos;
	//! maximal represented value
	float _maxPos;
	//! current value
	float _curPos;
	//! thumb size
	float _page;
	//! value difference performed by moving by one line
	float _lineStep;
	//! value difference performed by moving by one page
	float _pageStep;

	//! mouse position on thumb
	float _thumbOffset;
	//! thumb is moving
	bool _thumbLocked;
	
	//! is enabled (false if parent control has no items)
	bool _enabled;

	//! foreground (lines) color
	PackedColor _color;

public:
	C3DScrollBar();

	void OnLButtonDown(float v);
	void OnLButtonUp();
	void OnMouseHold(float v);
	void OnDraw(float alpha);

	//! returns if scrolling is enabled
	bool IsEnabled() const {return _enabled;}
	//! enables / disables scrolling
	void Enable(bool enable = true) {_enabled = enable;}
	//! returns current value
	float GetPos() {return _curPos;}
	//! sets current value
	void SetPos(float pos)
	{
		if (pos <= _minPos)
			_curPos = _minPos;
		else if (pos >= _maxPos)
			_curPos = _maxPos;
		else
			_curPos = pos;
	}
	//! sets minimal and maximal value
	void SetRange(float min, float max, float page)
	{
		_minPos = min;
		_maxPos = max;
		_page = page;
		saturateMax(_maxPos, _minPos + page);
		saturate(_curPos, _minPos, _maxPos - page);
	}
	//! sets amount of change processed after user input
	void SetSpeed(float line, float page) {_lineStep = line; _pageStep = page;}
	//! sets scroll bar placement
	void SetPosition(Vector3Par pos, Vector3Par right, Vector3Par down)
	{
		_position = pos;
		_right = right;
		_down = down;
	}
	//! sets foreground (lines) color
	void SetColor(PackedColor color) {_color = color;}
	//! returns if thumb is moving
	bool IsLocked() const {return _thumbLocked;}
};

///////////////////////////////////////////////////////////////////////////////
// class C3DListBox

//! 3D List Box control
class C3DListBox : public Control3D, public CListBoxContainer
{
protected:
	//! background color of selected item
	PackedColor _selBgColor;
	//! used font
	Ref<Font> _font;
	//! how much of whole row height is text height
	float _size;
	//! number of rows visible concurrently
	float _rows;

	//! ignore color for icon
	bool _colorPicture;
	//! item is dragging (by drag & drop operation)
	bool _dragging;
	//@{
	//! scrolling and implementation
	bool _scrollUp;
	bool _scrollDown;
	UITime _scrollTime;
	//@}

	//! scroll bar used by control
	C3DScrollBar _scrollbar;

	//! width of scroll bar (1.0 is with of whole control)
	float _sb3DWidth;

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	C3DListBox(ControlsContainer *parent, int idc, const ParamEntry &cls);

	virtual bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	virtual void OnMouseMove(float x, float y, bool active = true);
	virtual void OnMouseHold(float x, float y, bool active = true);
	virtual void OnMouseZChanged(float dz);
	virtual void OnLButtonDown(float x, float y);
	virtual void OnLButtonUp(float x, float y);
	virtual void OnLButtonDblClick(float x, float y);
	virtual void OnDraw(float alpha);

	//! event handler - performed if selection is changed
	virtual void OnSelChanged(int curSel) {}

	//! sets selected row
	void SetCurSel(int sel, bool sendUpdate = true);
	//! sets row on given position (<x>, <y>) as selected row
	void SetCurSel(float x, float y, bool sendUpdate = true);

	//! draws single row
	virtual void DrawItem
	(
		Vector3Par position, Vector3Par down, int i, float alpha
	);

	void UpdateInfo(ControlObject *object, ControlInObject &info);

	float NRows() const {return _rows;}

	//! set if color for icon will be ignore 
	void SetColorPicture(bool set) {_colorPicture = set;}
};

///////////////////////////////////////////////////////////////////////////////
// class C3DHTML

//! 3D HTML control
class C3DHTML : public Control3D, public CHTMLContainer
{
protected:
	//@{
	//! precalculated size of control
	float _width;
	float _height;
	float _invWidth;
	float _invHeight;
	//@}

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	C3DHTML(ControlsContainer *parent, int idc, const ParamEntry &cls);
	
	virtual void OnDraw(float alpha);
	virtual void OnLButtonDown(float x, float y);
	virtual void OnMouseMove(float x, float y, bool active = true);

	float GetPageWidth() const {return _width;}
	float GetPageHeight() const {return _height;}
	float GetTextWidth(float size, Font *font, const char *text) const;

	void UpdateInfo(ControlObject *object, ControlInObject &info);

protected:	
	int FindField(float x, float y);
};

///////////////////////////////////////////////////////////////////////////////
// class ControlsContainer
class MsgBox;

//! Identifier of control list (layer) in ControlsContainer
/*!
	There are three lists (layers) of controls in ControlsContainer:
	- objetcs - contains control objects
	- foreground - contains 2D controls ahead of objects
	- background - contains 2D controls behind objects
*/
enum ControlList
{
	CLNone,
	CLBackground,
	CLObjects,
	CLForeground
};

//! Unique identifier of control in ControlsContainer
struct ControlId
{
	//! list identifier
	ControlList list;
	//! index of control in list
	int id;

	//! constructor
	ControlId() {list = CLNone; id = -1;}
	//! constructor
	ControlId(ControlList l, int i) {list = l; id = i;}
	//! assignment operator
	void operator =(const ControlId &src)
	{
		list = src.list;
		id = src.id;
	}
	//! equality operator
	bool operator ==(const ControlId &with) const
	{
		return with.list == list && with.id == id;
	}
	//! inequality operator
	bool operator !=(const ControlId &with) const
	{
		return with.list != list || with.id != id;
	}

	//! returns nil id (points to no control)
	static ControlId Null() {return ControlId();}
	//! returns if id is nil (points to no control)
	bool IsNull() const {return list == CLNone;}
};

//! Implements basic UI object (single display) that contains controls
class ControlsContainer : public RefCount
{
protected:
	//! unique id of container
	int _idd;
	//! parent container
	ControlsContainer *_parent;

	//@{
	//! mouse cursor
	Ref<Texture> _cursorTexture;
	float _cursorX;
	float _cursorY;
	float _cursorW;
	float _cursorH;
	RString _cursorName;
	PackedColor _cursorColor;
	//@}

	//! background 2D controls layer
	RefArray<Control> _controlsBackground;
	//! object controls layer
	RefArray<ControlObject> _objects;
	//! foreground 2D controls layer
	RefArray<Control> _controlsForeground;
	//! control with input focus
	ControlId _indexFocused;
	//! control captured with left mouse button
	ControlId _indexLCaptured;
	//! control captured with right mouse button
	ControlId _indexRCaptured;
	//! default control
	ControlId _indexDefault;
	//! control with mouse cursor over
	ControlId _indexMouseOver;

// last mouse states
	//! mouse <x> coordinate in last simulation step
	float _lastX;
	//! mouse <y> coordinate in last simulation step
	float _lastY;
	//@{
	//! double click identification
	UITime _dblClkTimeout;
	float _dblClkX;
	float _dblClkY;
	//@}
	//! mouse left button state in last simulation step
	bool _lastLButton;
	//! mouse right button state in last simulation step
	bool _lastRButton;
	
	//! draw display even if is not on the top of displays hierarchy
	bool _alwaysShow;
	//! display can be moved by user
	bool _movingEnable;
	//! display is moving by user
	bool _moving;

	//! game simulation is enabled if display is on top
	bool _enableSimulation;
	//! game display is enabled if display is on top
	bool _enableDisplay;
	//! InGameUI (HUD) is enabled if display is on top
	bool _enableUI;
	//! Function Destroy() was already called
	bool _destroyed;
	//! exit code
	int _exit;

	UITime _tooltipTime;
	
public:
	//! constructor
	/*!
		\param parent parent controls container
	*/
	ControlsContainer(ControlsContainer *parent);
	//! destructor
	~ControlsContainer();
	//! initialization
	void Init();

	//! returns unique id of container
	int IDD() const {return _idd;}
	//! returns exit code of container
	int GetExitCode() const {return _exit;}
	//! returns if display is drawn even if is not on the top of displays hierarchy
	bool AlwaysShow() const {return _alwaysShow;}
	//! returns if game simulation is enabled if display is on top
	bool SimulationEnabled() const {return _enableSimulation;}
	//! returns if game display is enabled if display is on top
	bool DisplayEnabled() const {return _enableDisplay;}
	//! returns if InGameUI (HUD) is enabled if display is on top
	bool UIEnabled() const {return _enableUI;}
	//! returns control with given id
	IControl *GetCtrl(int idc);
	//! returns control on given position
	IControl *GetCtrl(float x, float y);
	//! returns focused control
	IControl *GetFocused();
	//! returns parent container
	ControlsContainer *Parent() {return _parent;}
	//@{
	//! returns child container
	virtual ControlsContainer *Child() {return NULL;}
	virtual const ControlsContainer *Child() const {return NULL;}
	//@}
	//! attach child container
	virtual void CreateChild(ControlsContainer *child) {}
	//! returns child message box
	virtual MsgBox *GetMsgBox() {return NULL;}
	//! creates message box
	/*!
		\param text displayed text
		\param flags combination of folowwing flags:
		- MB_BUTTON_OK - message box contains OK button
		- MB_BUTTON_CANCEL - message box contains CANCEL button
	*/
	virtual void CreateMsgBox(int flags, RString text, int idd = -1) {}
	//! destroys child message box
	virtual void DestroyMsgBox() {}
	//! returns if display is on top of displays hierarchy
	virtual bool IsTop() const {return true;}

	//! returns name of class of mouse cursor
	RString GetCursorName() const {return _cursorName;}
	//! returns mouse cursor style
	Texture *GetCursorTexture() const {return _cursorTexture;}
	//@{
	//! returns mouse cursor placement
	float GetCursorX() const {return _cursorX;}
	float GetCursorY() const {return _cursorY;}
	float GetCursorW() const {return _cursorW;}
	float GetCursorH() const {return _cursorH;}
	//@}
	//! returns mouse cursor color
	PackedColor GetCursorColor() const {return _cursorColor;}
	//! sets mouse cursor
	/*!
		\param name name of class in config.cpp::CfgWrapperUI::Cursors with cursor description
	*/
	void SetCursor(const char *name);

	//! creates one elements of display's content from resource template
	void LoadControl(const ParamEntry &ctrlCls);

	//! creates one elements of display's content from resource template
	void LoadObject(const ParamEntry &ctrlCls);

	//! creates one elements of display's content from resource template
	void LoadControlBackground(const ParamEntry &ctrlCls);

	//! creates display's content from resource template
	void Load(const ParamEntry &clsEntry);
	//! create display's content from resource template resource.cpp::<clsName>
	void Load(const char *clsName);
	//! creates display's content from Windows resource - not used nor maintained
	void Load(int idd);
	//! moves focus to next control
	void NextCtrl();
	//! moves focus to previous control
	void PrevCtrl();
	//! moves focus to next default control
	void NextDefaultCtrl();
	//! moves focus to previous default control
	void PrevDefaultCtrl();
	//! returns if display can be destroyed (ask all controls)
	virtual bool CanDestroy();
	//! display destruction
	virtual void Destroy();
	//! ask for destruction of control with given exit code
	void Exit(int code)
	{
		if (_exit < 0)
			_exit = code;
	}
	//! force sets exit code
	void ForceExit(int code)
	{
		_exit = code;
	}

	//! sets focus to given control
	void FocusCtrl(int idc);

	//! move whole display (all controls) by (<dx>, <dy>)
	void Move(float dx, float dy);

	//! event handler - performed by Load when control is to be created
	/*!
		\param type type of control
		\param idc id of control
		\param cls resource template for control
	*/
	virtual Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	//! event handler - performed by Load when object control is to be created
	/*!
		\param type type of control
		\param idc id of control
		\param cls resource template for control
	*/
	virtual ControlObject *OnCreateObject(int type, int idc, const ParamEntry &cls);
	//! event handler - performed when display must be drawn
	/*!
		\param vehicle with camera on
		\param alpha transparency
	*/
	virtual void OnDraw(EntityAI *vehicle, float alpha);
	//! event handler - performed when display is simulated
	virtual void OnSimulate(EntityAI *vehicle);
	//! event handler - performed if key is pressed (see Windows SDK)
	virtual bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	//! event handler - performed if key is depressed (see Windows SDK)
	virtual bool OnKeyUp(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	//! event handler - performed if char is entered (see Windows SDK)
	virtual bool OnChar(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	//! event handler - performed if IME char is entered (see Windows SDK)
	virtual bool OnIMEChar(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	//! event handler - performed if IME composition is entered (see Windows SDK)
	virtual bool OnIMEComposition(unsigned nChar, unsigned nFlags);
	//! called by world simulation when addon is used that has not been registered
	virtual bool OnUnregisteredAddonUsed(RString addon);
	//! event handler - performed if child display is destroyed
	virtual void OnChildDestroyed(int idd, int exit) {}

	//! event handler - performed if owned button was clicked
	/*!
		\param idc id of control
	*/
	virtual void OnButtonClicked(int idc);
	//! event handler - performed if owned List Box changed selected row
	/*!
		\param idc id of control
		\param curSel index of new selected row
	*/
	virtual void OnLBSelChanged(int idc, int curSel) {}
	//! event handler - performed if owned List Box obtains double click event
	/*!
		\param idc id of control
		\param curSel index of selected row
	*/
	virtual void OnLBDblClick(int idc, int curSel) {}
	//! event handler - performed if owned List Box starts drag & drop action
	/*!
		\param idc id of control
		\param curSel index of dragging row
	*/
	virtual void OnLBDrag(int idc, int curSel) {}
	//! event handler - performed if item is dragging
	virtual void OnLBDragging(float x, float y) {}
	//! event handler - performed if dragging item is released
	virtual void OnLBDrop(float x, float y) {}
	//! event handler - performed if owned Selector changed selected row
	/*!
		\param idc id of control
		\param curSel index of new selected row
	*/
	virtual void OnComboSelChanged(int idc, int curSel) {}
	//! event handler - performed if owned Tree control changed selected item
	/*!
		\param idc id of control
	*/
	virtual void OnTreeSelChanged(int idc) {}
	//! event handler - performed if owned Tree control obtains double click event
	/*!
		\param idc id of control
		\param sel selected item
	*/
	virtual void OnTreeDblClick(int idc, CTreeItem *sel) {}
	//! event handler - performed if owned Tool Box control changed selected item
	/*!
		\param idc id of control
		\param curSel index of new selected item
	*/
	virtual void OnToolBoxSelChanged(int idc, int curSel) {}
	//! event handler - performed if owned Check Boxes changed selected item
	/*!
		\param idc id of control
		\param curSel index of new selected item
	*/
	virtual void OnCheckBoxesSelChanged(int idc, int curSel, bool value) {}
	//! event handler - performed if owned HTML control's link is activated
	/*!
		\param idc id of control
		\param link link address (URL)
	*/
	virtual void OnHTMLLink(int idc, RString link) {}
	//! event handler - performed if owned Slider changed actual value
	/*!
		\param idc id of control
		\param pos new value
	*/
	virtual void OnSliderPosChanged(int idc, float pos) {}
	//! event handler - performed if owned ControlObjectContainerAnim finished close animation
	/*!
		\param idc id of control
	*/
	virtual void OnCtrlClosed(int idc) {}
	//! event handler - performed if owned ControlObject moves
	/*!
		\param idc id of control
	*/
	virtual void OnObjectMoved(int idc, Vector3Par offset) {}

	//! draws mouse cursor
	/*!
		\patch_internal 1.01 Date 07/09/2001 by Jirka - extracted from Display::DrawHUD
	*/
	void DrawCursor();

// implementation
private:
	//! implements change of input focus
	/*!
		\param id id of control gains input focus
		\param up true if focus is gaines as result of Next Control action (TAB)
		\param def true if only button like controls (defaultable) can gain focus
		\return false if focus cannot be gained
	*/
	bool SetFocus(ControlId &id, bool up = true, bool def = false);
	//! returns control with given identifier
	IControl *Ctrl(ControlId &id);
	//! find next control (in TAB order)
	ControlId Next(ControlId &id);
	//! find previous control (in TAB order)
	ControlId Prev(ControlId &id);
	//! sorts object controls by z coordinate
	void SortObjects();
};

///////////////////////////////////////////////////////////////////////////////
// class MsgBox

//! message box class
class MsgBox : public ControlsContainer
{
	//! displayed text
	RString _text;
public:
	//! constructor
	/*!
		\param parent parent container (display)
		\param flags see (ControlsContainer::CreateMsgBox)
		\param text text to be displayed
		\param idd id of created container
	*/
	MsgBox(ControlsContainer *parent, int flags, RString text, int idd);
};

///////////////////////////////////////////////////////////////////////////////
// class Display

//! Display class
/*!
	Adds implementation of displays hierarchy to ControlContainer.
	Implements AbstractOptionsUI interface.
*/
class Display : public ControlsContainer, public AbstractOptionsUI
{
protected:
	//! child container (display)
	Ref<ControlsContainer> _child;
	//! child mesage box
	Ref<MsgBox> _msgBox;

public:
	//! constructor
	/*!
		\param parent parent container (display)
	*/
	Display(ControlsContainer *parent);
	//! destructor
	~Display();

	ControlsContainer *Child() {return _child;}
	const ControlsContainer *Child() const {return _child;}
	void CreateChild(ControlsContainer *child) {_child = child;}
	virtual void OnChildDestroyed(int idd, int exit) {_child = NULL;}
	//! returns if display is on top of displays hierarchy
	bool IsTopmost() const {return _child == NULL && _msgBox == NULL;}
	bool IsTop() const {return _child == NULL && _msgBox == NULL;}

	//! called by framework when displays hierarchy must be drawn
	/*!
		\param vehicle with camera on
		\param alpha transparency
	*/
	void DrawHUD(VehicleWithAI *vehicle, float alpha);
	//! called by framework when displays hierarchy is simulated
	void SimulateHUD(VehicleWithAI *vehicle);
	//! called by framework if key is pressed (see Windows SDK)
	bool DoKeyDown( unsigned nChar, unsigned nRepCnt, unsigned nFlags );
	//! called by framework if key is depressed (see Windows SDK)
	bool DoKeyUp( unsigned nChar, unsigned nRepCnt, unsigned nFlags );
	//! called by framework if char is entered (see Windows SDK)
	bool DoChar( unsigned nChar, unsigned nRepCnt, unsigned nFlags );
	//! called by framework if IME char is entered (see Windows SDK)
	bool DoIMEChar( unsigned nChar, unsigned nRepCnt, unsigned nFlags );
	//! called by framework if IME composition is entered (see Windows SDK)
	bool DoIMEComposition( unsigned nChar, unsigned nFlags );
	//! called by world simulation when addon is used that has not been registered
	bool DoUnregisteredAddonUsed( RString addon );

	MsgBox *GetMsgBox() {return _msgBox;}
	void CreateMsgBox(int flags, RString text, int idd = -1)
	{_msgBox = new MsgBox(this, flags, text, idd);}
	void DestroyMsgBox()
	{_msgBox = NULL;}
	//! returns if game simulation is enabled
	bool IsSimulationEnabled() const;
	//! returns if game display is enabled
	bool IsDisplayEnabled() const;
	//! returns if InGameUI (HUD) is enabled
	bool IsUIEnabled() const;
};

#endif
