#ifndef _EDITOR_HPP
#define _EDITOR_HPP

#include "vehicle.hpp"
#include "MessageDll.h"

#ifdef _WIN32
  #include <winsock2.h>
#else
  #include <sys/socket.h>
  typedef int SOCKET;
  #define SOCKET_ERROR   -1
  #define INVALID_SOCKET -1
  #define closesocket(s) ::close(s)
#endif

class EditCursor;

class CSelection
{
	public:
	FindArray< int > m_data;
	EditCursor *m_pOwner;
	
	public:
		CSelection() {m_pOwner = NULL;}
	int AddObject (Object *pObj)
	{
		return AddID(pObj->ID());
	}
	int AddID(int nID);
	bool RemoveAddObject (Object *pObj)
	{
		return RemoveID(pObj->ID());
	}

	bool RemoveID(int nID);

	void Remove(int index);
	void RemoveAll();
	int Size() const {return m_data.Size();}
	int operator [] ( int i ) const {return m_data[i];}
	AutoArray< int > &GetData() {return m_data;}
};

class EditCursor: public Entity
{
	typedef Entity base;

private:

	// describe orientation (Euler parameters)
	float _heading,_dive,_bank;

	float _camDistance;
	float _camHeight;
//	Matrix4 _mTrans,_mInvTrans; // user orientation
//	enum EditMode {EditObjs,EditNets,EditLand} _editMode;
	bool _visible;

	// Flags and modes
	bool m_bCtrl;		// CTRL key was down when LBUTTON DOWN
	bool m_bOldMouseL;	// last state of left mouse button
	bool m_bOldMouseR;	// last state of right mouse button
	WORD m_wFlags;		// how move will be interpreted and other flags
	bool _showNode;
	
	bool _cameraOnEdit;
	OLink<Object> _animCamera;
	Time _animCameraMoved;
	Time _animCameraUpdated;

	// Actual selection
	CSelection m_Selection;
	Ref<Object> _drawRectangle;

	// State for moving, selecting and rotating
	Point3 m_ptStart;	// start point
	Point3 m_ptCurrent;	// current point
	int m_nPrimaryObject;
	bool m_bPrimarySelection;	// selection state of primary object
	AutoArray< int > m_MouseSelection;	// Objects in mouse selection cube

	// Last cursor position
	Matrix4	m_posLast;

	// communication
	SOCKET _socketSend;
	RString _ip;
	bool _connected;
	
	// Magnetism
	bool m_bMagnetize;
	bool m_bMoved;
	AutoArray< Matrix4 > m_transPure;
	
private:
	// Helper functions
	void CheckMouseSelection(Point3& ptMin, Point3& ptMax);

	void UpdateTerrain(Vector3 &position, float change);

	bool ReceiveMessage(SPosMessage	&sMsg);
	void HandleError(bool send);

public:
	bool ProcessEvents();
	void CCALL SendEvent(int nMsgID, ...);

	// Vehicle implementation
	void Simulate( float deltaT, SimulationImportance prec );
	void Draw( int forceLOD, ClipFlags clipFlags, const FrameBase &pos );

	void Sound( bool inside, float deltaT ){}
	void UnloadSound(){}
	Matrix4 InsideCamera( CameraType camType ) const {return Matrix4(MTranslation,Vector3(0,0,-_camDistance));}
	float OutsideCameraDistance( CameraType camType ) const {return _camDistance;}
	//bool InsideVisible() const {return _visible;}
	//bool InsideShadowVisible() const {return _visible;}
	int InsideLOD( CameraType camType ) const {return 0;} // added by ONDRA 

	SimulationImportance WorstImportance() const {return SimulateVisibleNear;}
	// SimulationImportance BestImportance() const {return SimulateCamera;}

	void LimitVirtual
	(
		CameraType camType, float &heading, float &dive, float &fov
	) const;
	void InitVirtual
	(
		CameraType camType, float &heading, float &dive, float &fov
	) const;
	bool IsContinuous( CameraType camType ) const {return true;}

	void SwitchCamera();

	EditCursor(RString ip);
	~EditCursor();
};

#endif

