// Poseidon - HUD UI drawing

#include "wpch.hpp"
#include "world.hpp"
#include "ai.hpp"
#include "keyInput.hpp"
#include "InGameUIImpl.hpp"
#include "engine.hpp"
#include "camera.hpp"
#include "visibility.hpp"
#include "landscape.hpp"
#include "txtPreload.hpp"
#include "diagModes.hpp"
#include "network.hpp"
#include <Es/Strings/bstring.hpp>

#include "resincl.hpp"

#include "move_actions.hpp"
#include "integrity.hpp"

#include "stringtableExt.hpp"

extern class ParamFile Res;

#define INV_H_PI (1/H_PI)

#if _RUSSIAN
#define STR_POS_DIST "%.0f �"
#define STR_OBJ_DIST "%s (%.0f �)"
#else
#define STR_POS_DIST "%.0f m"
#define STR_OBJ_DIST "%s (%.0f m)"
#endif

AIUnit *GetSelectedUnit(int i);
void SetSelectedUnit(int i, AIUnit *unit);
void ClearSelectedUnits();
bool IsEmptySelectedUnits();
PackedBoolArray ListSelectedUnits();

#if _ENABLE_CHEATS
static bool tdCheat;
#endif

void DrawFrame(Texture *corner, PackedColor color, const Rect2DPixel &frame)
{
	const int screenW = GLOB_ENGINE->Width2D();
	const int screenH = GLOB_ENGINE->Height2D();

	MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(corner, 0, 0);
	float invCornerH=1, invCornerW=1;
	if (corner)
	{
		invCornerH = 1200.0 * (1.0 / (screenH * corner->AHeight()));
		invCornerW = 1600.0 * (1.0 / (screenW * corner->AWidth()));
	}
	Draw2DPars pars;
	Rect2DPixel rect;
	rect.w = 0.5 * frame.w;
	rect.h = 0.5 * frame.h;
	pars.mip = mip;
	pars.SetColor(color);
	pars.spec = NoZBuf|IsAlpha|ClampU|ClampV|IsAlphaFog;
	float coefX = 0.5 * frame.w * invCornerW;
	float coefY = 0.5 * frame.h * invCornerH;
	
	rect.x = frame.x;
	rect.y = frame.y;
	pars.SetU(0,coefX);
	pars.SetV(0,coefY);
	GLOB_ENGINE->Draw2D(pars, rect);

	rect.x = frame.x + rect.w;
	rect.y = frame.y;
	pars.SetU(coefX,0);
	pars.SetV(0,coefY);
	GLOB_ENGINE->Draw2D(pars, rect);

	rect.x = frame.x;
	rect.y = frame.y + rect.h;
	pars.SetU(0,coefX);
	pars.SetV(coefY,0);
	GLOB_ENGINE->Draw2D(pars, rect);

	rect.x = frame.x + rect.w;
	rect.y = frame.y + rect.h;
	pars.SetU(coefX,0);
	pars.SetV(coefY,0);
	GLOB_ENGINE->Draw2D(pars, rect);

	//GLOB_ENGINE->TextBank()->ReleaseMipmap();
}

struct DrawActionInfo
{
	RString text;
//	PackedColor color;
	bool selected;
};
TypeIsMovableZeroed(DrawActionInfo);

static void FormatWeapon
(
	char *buffer, const char *format, EntityAI *veh, int weapon
)
{
	if (veh)
	{
		if (weapon >= 0 && weapon < veh->NMagazineSlots())
		{
			const MagazineSlot &slot = veh->GetMagazineSlot(weapon);
			RStringB displayName = slot._muzzle->_displayName;
			const WeaponModeType *mode = veh->GetWeaponMode(weapon);
			if (mode) displayName = mode->GetDisplayName();
			sprintf
			(
				buffer, format,
				(const char *)displayName
			);
		}
		else
		{
			strcpy(buffer, "Weapon Error");
			RptF("Weapon action - bad weapon %d",weapon);
		}
	}
}

#define CX(x) (toInt((x) * w) + 0.5)
#define CY(y) (toInt((y) * h) + 0.5)

/*!
\patch 1.95 Date 1/2/2004 by Ondra
- Fixed: AI units could not take any magazines.
*/

RString UIAction::GetDisplayName(AIUnit *unit) const
{
	char buffer[1024]; buffer[0] = 0;
	switch (type)
	{
	case ATGetInCommander:
		if (!target)
		{
			RptF("Get in action - missing target");
		}
		else
		{
			sprintf
			(
				buffer, LocalizeString(IDS_ACTION_GETIN_COMMANDER),
				(const char *)target->GetDisplayName()
			);
		}
		break;
	case ATGetInDriver:
		if (!target)
		{
			RptF("Get in action - missing target");
		}
		else
		{
			if (target->GetType()->IsKindOf(GWorld->Preloaded(VTypeAir)))
				sprintf
				(
					buffer, LocalizeString(IDS_ACTION_GETIN_PILOT),
					(const char *)target->GetDisplayName()
				);
			else
				sprintf
				(
					buffer, LocalizeString(IDS_ACTION_GETIN_DRIVER),
					(const char *)target->GetDisplayName()
				);
		}
		break;
	case ATGetInGunner:
		if (!target)
		{
			RptF("Get in action - missing target");
		}
		else
		{
			sprintf
			(
				buffer, LocalizeString(IDS_ACTION_GETIN_GUNNER),
				(const char *)target->GetDisplayName()
			);
		}
		break;
	case ATGetInCargo:
		if (!target)
		{
			RptF("Get in action - missing target");
		}
		else
		{
			sprintf
			(
				buffer, LocalizeString(IDS_ACTION_GETIN_CARGO),
				(const char *)target->GetDisplayName()
			);
		}
		break;
	case ATHeal:
		if (!target)
		{
			RptF("Heal action - missing target");
		}
		else
		{
			sprintf
			(
				buffer, LocalizeString(IDS_ACTION_HEAL),
				(const char *)target->GetDisplayName()
			);
		}
		break;
	case ATRepair:
		if (!target)
		{
			RptF("Repair action - missing target");
		}
		else
		{
			sprintf
			(
				buffer, LocalizeString(IDS_ACTION_REPAIR),
				(const char *)target->GetDisplayName()
			);
		}
		break;
	case ATRefuel:
		if (!target)
		{
			RptF("Refuel action - missing target");
		}
		else
		{
			sprintf
			(
				buffer, LocalizeString(IDS_ACTION_REFUEL),
				(const char *)target->GetDisplayName()
			);
		}
		break;
	case ATRearm:
		if (!target)
		{
			RptF("Rearm action - missing target");
		}
		else
		{
			sprintf
			(
				buffer, LocalizeString(IDS_ACTION_REARM),
				(const char *)target->GetDisplayName()
			);
		}
		break;
	case ATTakeWeapon:
		{
			EntityAI *veh = target;
			if (veh)
			{
				Ref<WeaponType> weapon = WeaponTypes.New(param3);
				AUTO_STATIC_ARRAY(Ref<const WeaponType>, conflict, 16);
				if (veh->FindWeapon(weapon) && (!unit || unit->GetPerson()->CheckWeapon(weapon, conflict)))
				{
					char drop[1024]; drop[0] = 0;
					int n = 0;
					for (int i=0; i<conflict.Size(); i++)
					{
						if (n > 0) strcat(drop, ", ");
						strcat(drop, (const char *)conflict[i]->GetDisplayName());
						n++;
					}
					if (n > 0)
						sprintf
						(
							buffer, LocalizeString(IDS_ACTION_DROPTAKEWEAPON),
							(const char *)weapon->GetDisplayName(),
							drop
						);
					else
						sprintf
						(
							buffer, LocalizeString(IDS_ACTION_TAKEWEAPON),
							(const char *)weapon->GetDisplayName()
						);
				}
				else
				{
					RptF
					(
						"Take weapon action - bad weapon %x %s",
						(void*)weapon,
						weapon ? (const char *)weapon->GetName() : ""
					);
				}
			}
			else
			{
				// no target
				Ref<WeaponType> weapon = WeaponTypes.New(param3);
				sprintf
				(
					buffer, LocalizeString(IDS_ACTION_TAKEWEAPON),
					(const char *)weapon->GetDisplayName()
				);
			}
		}
		break;
	case ATTakeMagazine:
		{
			EntityAI *veh = target;
			if (veh)
			{
				Ref<const Magazine> magazine = veh->FindMagazine(param3);
				AUTO_STATIC_ARRAY(Ref<const Magazine>, conflict, 16);
				if (magazine && (!unit || unit->GetPerson()->CheckMagazine(magazine, conflict)))
				{
					char drop[1024]; drop[0] = 0;
					int n = 0;
					for (int i=0; i<conflict.Size(); i++)
					{
						if (n > 0) strcat(drop, ", ");
						if (conflict[i]->_ammo > 0 && conflict[i]->_type != magazine->_type)
						{
							strcat(drop, (const char *)conflict[i]->_type->GetDisplayName());
							n++;
						}
					}
					if (n > 0)
						sprintf
						(
							buffer, LocalizeString(IDS_ACTION_DROPTAKEMAGAZINE),
							(const char *)magazine->_type->GetDisplayName(),
							drop
						);
					else
						sprintf
						(
							buffer, LocalizeString(IDS_ACTION_TAKEMAGAZINE),
							(const char *)magazine->_type->GetDisplayName()
						);
				}
				else
				{
					RptF
					(
						"Take magazine action - bad magazine %x %s",
						(const void*)magazine,
						magazine ? (const char *)magazine->_type->GetName() : ""
					);
				}
			}
		}
		break;
	case ATDropWeapon:
		{
			Ref<WeaponType> weapon = WeaponTypes.New(param3);
			Assert(weapon);
			if (target)
				sprintf
				(
					buffer,
					LocalizeString(IDS_ACTION_PUT_WEAPON),
					(const char *)weapon->GetDisplayName(),
					(const char *)target->GetDisplayName()
				);
			else
				sprintf
				(
					buffer,
					LocalizeString(IDS_ACTION_DROP_WEAPON),
					(const char *)weapon->GetDisplayName()
				);
			break;
		}
	case ATDropMagazine:
		{
			Ref<MagazineType> type = MagazineTypes.New(param3);
			Assert(type);
			if (target)
				sprintf
				(
					buffer,
					LocalizeString(IDS_ACTION_PUT_MAGAZINE),
					(const char *)type->GetDisplayName(),
					(const char *)target->GetDisplayName()
				);
			else
				sprintf
				(
					buffer,
					LocalizeString(IDS_ACTION_DROP_MAGAZINE),
					(const char *)type->GetDisplayName()
				);
			break;
		}
	case ATGetOut:
		strcpy(buffer, LocalizeString(IDS_ACTION_GETOUT));
		break;
	case ATLightOn:
		strcpy(buffer, LocalizeString(IDS_ACTION_LIGHTON));
		break;
	case ATLightOff:
		strcpy(buffer, LocalizeString(IDS_ACTION_LIGHTOFF));
		break;
	case ATEngineOn:
		strcpy(buffer, LocalizeString(IDS_ACTION_ENGINEON));
		break;
	case ATEngineOff:
		strcpy(buffer, LocalizeString(IDS_ACTION_ENGINEOFF));
		break;
	case ATSwitchWeapon:
		FormatWeapon
		(
			buffer,LocalizeString(IDS_ACTION_WEAPON),target,param
		);
		break;
	case ATUseWeapon:
		{
			EntityAI *veh = target;
			if (veh)
			{
				const WeaponModeType *mode = veh->GetWeaponMode(param);
				if (mode)
				{
					int left = 0;
					const MagazineSlot &slot = veh->GetMagazineSlot(param);
					const Magazine *magazine = slot._magazine;
					// count magazines
					for (int i=0; i<veh->NMagazines(); i++)
					{
						const Magazine *m = veh->GetMagazine(i);
						if (!m || m->_ammo == 0) continue;
						if (m->_type == magazine->_type) left++;
					}
					sprintf
					(
						buffer, mode->_useActionTitle,
						(const char *)mode->GetDisplayName(),
						left
					);
				}
				else
				{
					strcpy(buffer, "Weapon Error");
					RptF("Weapon action - bad weapon %d", param);
				}
			}
		}
		break;
	case ATUseMagazine:
		{
			EntityAI *veh = target;
			if (veh)
			{
				const Magazine *magazine = veh->FindMagazine(param, param2);
				if (magazine)
				{
					// count magazines
					int left = 0;
					for (int i=0; i<veh->NMagazines(); i++)
					{
						const Magazine *m = veh->GetMagazine(i);
						if (!m || m->_ammo == 0) continue;
						if (m->_type == magazine->_type) left++;
					}
					sprintf
					(
						buffer, magazine->_type->_useActionTitle,
						(const char *)magazine->_type->GetDisplayName(),
						left
					);
				}
				else
					RptF("Use magazine action - bad magazine %d:%d", param, param2);
			}
		}
		break;
	case ATEject:
		strcpy(buffer, LocalizeString(IDS_ACTION_EJECT));
		break;
	case ATLoadMagazine:
		{
			EntityAI *veh = target;
			if (veh)
			{
				const Magazine *magazine = veh->FindMagazine(param, param2);
				if (magazine)
					sprintf
					(
						buffer, LocalizeString(IDS_ACTION_MAGAZINE),
						(const char *)magazine->_type->GetDisplayName()
					);
				else
					RptF("Load magazine action - bad magazine %d:%d", param, param2);
			}
		}
		break;
	case ATTakeFlag:
		strcpy(buffer, LocalizeString(IDS_ACTION_TAKEFLAG));
		break;
	case ATReturnFlag:
		strcpy(buffer, LocalizeString(IDS_ACTION_RETURNFLAG));
		break;
	case ATTurnIn:
		strcpy(buffer, LocalizeString(IDS_ACTION_TURNIN));
		break;
	case ATTurnOut:
		strcpy(buffer, LocalizeString(IDS_ACTION_TURNOUT));
		break;
	case ATSitDown:
		return LocalizeString(IDS_ACTION_SITDOWN);
	case ATSalute:
		return LocalizeString(IDS_ACTION_SALUTE);
	case ATHideBody:
		return LocalizeString(IDS_ACTION_HIDE_BODY);
	case ATNVGoggles:
		if (unit->GetPerson()->IsNVWanted())
			return LocalizeString(IDS_ACTION_TAKEOFF_GOGGLES);
		else
			return LocalizeString(IDS_ACTION_TAKEON_GOGGLES);
		break;
	default:
		if (target) return target->GetActionName(*this);
		break;
	}
	return buffer;
}

void UIActions::OnDraw()
{
	AIUnit *unit = GWorld->FocusOn();
	if (!unit) return;

	int n = Size();
	saturate(n, 1, _rows);

	float alpha = GetAlpha();
	if (alpha<=0.01) return;

	PackedColor bgColorA = PackedColorRGB(_bgColor, toIntFloor(_bgColor.A8() * alpha));
	PackedColor selColorA = PackedColorRGB(_selColor, toIntFloor(_selColor.A8() * alpha));
	PackedColor textColorA = PackedColorRGB(_textColor, toIntFloor(_textColor.A8() * alpha));

	bool canUp = false;
	bool canDown = false;

	AUTO_STATIC_ARRAY(DrawActionInfo, array, 16);
	array.Resize(n);
	if (Size() == 0)
	{
		array[0].text = LocalizeString(IDS_NO_ACTION);
		array[0].selected = false;
	}
	else
	{
		int selected = FindSelected();
		Assert(selected >= 0);
		int offset = 0;
		if (selected >= n) offset = selected - n + 1;
		canUp = offset > 0;
		canDown = Size() > offset + _rows;
		for (int i=0; i<n; i++)
		{
			const UIAction &action = Get(offset + i);
			
			array[i].text = action.GetDisplayName(unit);
			array[i].selected = offset + i == selected;
		}
	}

	const int w = GLOB_ENGINE->Width2D();
	const int h = GLOB_ENGINE->Height2D();
	const float border = 0.005;

	float height = n * _h + 2 * border;
	float top = _bottom - height;
	float width = 0;
	for (int i=0; i<n; i++)
		saturateMax(width, GEngine->GetTextWidth(_size, _font, array[i].text));
	width += 2 * border;
	float left = _right - width;

	DrawFrame
	(
		GLOB_SCENE->Preloaded(Corner),
		bgColorA,
		Rect2DPixel(left * w, top * h, width * w, height * h)
	);
	top += border;

	const float hA = 0.015;
	const float wA = 0.7 * hA;
	const float wA2 = 0.5 * wA;
	if (canUp)
	{
		GEngine->DrawLine
		(
			Line2DPixel(CX(_right + border), CY(top + hA),
			CX(_right + border + wA), CY(top + hA)),
			textColorA, textColorA
		);
		GEngine->DrawLine
		(
			Line2DPixel(CX(_right + border + wA), CY(top + hA),
			CX(_right + border + wA2), CY(top)),
			textColorA, textColorA
		);
		GEngine->DrawLine
		(
			Line2DPixel(CX(_right + border + wA2), CY(top),
			CX(_right + border), CY(top + hA)),
			textColorA, textColorA
		);
	}
	if (canDown)
	{
		float bottom = _bottom - border;
		GEngine->DrawLine
		(
			Line2DPixel(CX(_right + border), CY(bottom - hA),
			CX(_right + border + wA), CY(bottom - hA)),
			textColorA, textColorA
		);
		GEngine->DrawLine
		(
			Line2DPixel(CX(_right + border + wA), CY(bottom - hA),
			CX(_right + border + wA2), CY(bottom)),
			textColorA, textColorA
		);
		GEngine->DrawLine
		(
			Line2DPixel(CX(_right + border + wA2), CY(bottom),
			CX(_right + border), CY(bottom - hA)),
			textColorA, textColorA
		);
	}

	for (int i=0; i<n; i++)
	{
		PackedColor color = array[i].selected ? selColorA : textColorA;	
		GEngine->DrawText(Point2DFloat(left + border, top), _size, _font, color, array[i].text);
		top += _h;
		if (array[i].selected)
		{
			float wT = GEngine->GetTextWidth(_size, _font, array[i].text);
			GEngine->DrawLine
			(
				Line2DPixel(CX(left + border), CY(top),
				CX(left + border + wT), CY(top)),
				color, color
			);
		}
	}
}

PackedColor InGameUI::ColorFromHit(float hit)
{
	const float halfValue = 0.4;
	const float minValue = 0.1;
	const float fullValue = 0.9;

	if (hit>=fullValue) return tankColorFullDammage;
	if (hit<=minValue) return tankColor;

	if (hit<halfValue)
	{
		float f = (hit-minValue)/(halfValue-minValue);
		Color c = Color((ColorVal)tankColor)*(1-f)+Color((ColorVal)tankColorHalfDammage)*f;
		return PackedColor(c);
	}
	else
	{
		float f = (hit-halfValue)/(fullValue-halfValue);
		Color c = Color((ColorVal)tankColorHalfDammage)*(1-f)+Color((ColorVal)tankColorFullDammage)*f;
		return PackedColor(c);
	}
}

/*!
\patch 1.30 Date 11/20/2001 by Ondra.
- Fixed: Left and right tank track dammage display was swapped.
*/

void InGameUI::DrawTankDirection(const Camera &camera)
{
	if (_tankPos >= 1) return;
/*
	if (veh->NMagazineSlots() <= 0)
		return;

	float tankLeft = tankX + (1 - tankX) * _tankPos;
*/
	AIUnit *unit = GWorld->FocusOn();
	Assert(unit);
	EntityAI *veh = unit->GetVehicle();

	float tankLeft = tankX;
	
	const int w = GLOB_ENGINE->Width2D();
	const int h = GLOB_ENGINE->Height2D();

/*
	Texture *corner = GLOB_SCENE->Preloaded(Corner);
	DrawFrame
	(
		corner,
		bgColor,
		tankLeft * w, tankY * h, tankW * w, tankH * h
	);
*/

	Matrix4Val camInvTransform = camera.GetInvTransform();
	Vector3 dirHull(VRotate, camInvTransform, veh->Direction());
	Vector3 dirTurret(VRotate, camInvTransform, veh->GetWeaponDirection(0));
	Vector3 dirObsTurret(VRotate, camInvTransform, veh->GetEyeDirection());

	Matrix4 mrot(MZero);
	Matrix4 mtrans1 = Matrix4(MTranslation, Vector3(-0.5, 0, -0.5));
	Matrix4 mtrans2 = Matrix4(MTranslation, Vector3(0.5, 0, 0.5));

	Draw2DPars pars;
	pars.spec = NoZBuf|IsAlpha|IsAlphaFog|ClampU|ClampV;

	//Rect2D rect(tankLeft * w, tankY * h, tankW * w, tankH * h);
	
	const float size = 0.67;

	Rect2DPixel rect
	(
		w * (tankLeft + tankW * 0.5 - tankW*size*0.5),
		h * (tankY + tankH * 0.5 - tankH*size*0.5),
		tankW * w * size, tankH * h * size
	);

	#define v000 VZero
	#define v001 VForward
	#define v100 VAside
	#define v101 Vector3(1, 0, 1)



	mrot.SetUpAndDirection(VUp, dirHull);
	Matrix4 m = mtrans2 * mrot * mtrans1;
	Vector3 uv = m.FastTransform(v000);
	pars.uTR = uv[2];
	pars.vTR = uv[0];
	uv = m.FastTransform(v001);
	pars.uTL = uv[2];
	pars.vTL = uv[0];
	uv = m.FastTransform(v100);
	pars.uBR = uv[2];
	pars.vBR = uv[0];
	uv = m.FastTransform(v101);
	pars.uBL = uv[2];
	pars.vBL = uv[0];

	// draw all hull components
	pars.SetColor(ColorFromHit(veh->GetHitForDisplay(0)));
	pars.mip = GEngine->TextBank()->UseMipmap(_imageHull, 0, 0);
	GEngine->Draw2D(pars, rect, rect);

	pars.SetColor(ColorFromHit(veh->GetHitForDisplay(1)));
	pars.mip = GEngine->TextBank()->UseMipmap(_imageEngine, 0, 0);
	GEngine->Draw2D(pars, rect, rect);

	pars.SetColor(ColorFromHit(veh->GetHitForDisplay(3)));
	pars.mip = GEngine->TextBank()->UseMipmap(_imageLTrack, 0, 0);
	GEngine->Draw2D(pars, rect, rect);

	pars.SetColor(ColorFromHit(veh->GetHitForDisplay(2)));
	pars.mip = GEngine->TextBank()->UseMipmap(_imageRTrack, 0, 0);
	GEngine->Draw2D(pars, rect, rect);

	mrot.SetUpAndDirection(VUp, dirTurret);
	m = mtrans2 * mrot * mtrans1;
	uv = m.FastTransform(v000);
	pars.uTR = uv[2];
	pars.vTR = uv[0];
	uv = m.FastTransform(v001);
	pars.uTL = uv[2];
	pars.vTL = uv[0];
	uv = m.FastTransform(v100);
	pars.uBR = uv[2];
	pars.vBR = uv[0];
	uv = m.FastTransform(v101);
	pars.uBL = uv[2];
	pars.vBL = uv[0];

	// draw all turret components
	pars.SetColor(ColorFromHit(veh->GetHitForDisplay(4)));
	pars.mip = GEngine->TextBank()->UseMipmap(_imageTurret, 0, 0);
	GEngine->Draw2D(pars, rect, rect);

	pars.SetColor(ColorFromHit(veh->GetHitForDisplay(5)));
	pars.mip = GEngine->TextBank()->UseMipmap(_imageGun, 0, 0);
	GEngine->Draw2D(pars, rect, rect);

	Vector3 offset(0.05 * tankW, 0, 0);
	offset = mrot.FastTransform(offset);
	
	const float size2 = 0.25;
	//const float offX2 = 0.1;
	//const float offY2 = 0.0;

	Rect2DPixel rect2
	(
		w * (tankLeft + tankW * 0.5 - tankW*size2*0.5 + offset.X()),
		h * (tankY + tankH * 0.5 - tankH*size2*0.5) - w * offset.Z(),
		tankW * w * size2, tankH * h * size2
	);

	// observer turret
	pars.SetColor(ColorFromHit(veh->GetHitForDisplay(4)));
	mrot.SetUpAndDirection(VUp, dirObsTurret);
	m = mtrans2 * mrot * mtrans1;
	uv = m.FastTransform(v000);
	pars.uTR = uv[2];
	pars.vTR = uv[0];
	uv = m.FastTransform(v001);
	pars.uTL = uv[2];
	pars.vTL = uv[0];
	uv = m.FastTransform(v100);
	pars.uBR = uv[2];
	pars.vBR = uv[0];
	uv = m.FastTransform(v101);
	pars.uBL = uv[2];
	pars.vBL = uv[0];
	pars.mip = GEngine->TextBank()->UseMipmap(_imageObsTurret, 0, 0);
	GEngine->Draw2D(pars, rect2, rect2);

}

void InGameUI::DrawMenu()
{
	if (_tmPos >= 1.0) return;

	Texture *textureDef = GLOB_SCENE->Preloaded(TextureWhite);
	const int w = GLOB_ENGINE->Width2D();
	const int h = GLOB_ENGINE->Height2D();
	const float border = 0.005;
	float size = 0.02;

	float tmLeft = tmX + (1 - tmX) * _tmPos;
	Texture *corner = GLOB_SCENE->Preloaded(Corner);
	DrawFrame
	(
		corner,
		bgColor,
		Rect2DPixel(tmLeft * w, tmY * h, tmW * w, tmH * h)
	);

	MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(textureDef, 0, 0);
	float width = GLOB_ENGINE->GetTextWidth(size, _font24, _menuCurrent->_text) + 2 * border;
	float height = size + 2 * border;
	float left = tmLeft + tmW - border - width;
	float top = tmY + tmH;
	GLOB_ENGINE->Draw2D(mip, capBgColor, Rect2DPixel(left * w, top * h, width * w, height * h));
	//GLOB_ENGINE->TextBank()->ReleaseMipmap();
	GLOB_ENGINE->DrawText(Point2DFloat(left + border, top + border), size,
		_font24, capFtColor, _menuCurrent->_text);

	top = tmY + border;
	height = size;
	PackedColor color;
	for (int i=0; i<_menuCurrent->_items.Size(); i++)
	{
		MenuItem* item = _menuCurrent->_items[i];
		if (!item->_visible)
			continue;

		if (item->_cmd == CMD_SEPARATOR)
		{
			top += border;
			GEngine->DrawLine
			(
				Line2DPixel((tmLeft + border) * w, top * h, (tmLeft + tmW - border) * w, top * h),
				menuDisabledColor, menuDisabledColor
			);
			top += border;
			continue;
		}

		if (item->_check)
			color = menuCheckedColor;
		else if (item->_enable)
			color = menuEnabledColor;
		else
			color = menuDisabledColor;
		
		bool bottom = item->_key == DIK_BACK;
		float t = bottom ? tmY + tmH - border - height : top;
		
		// check if char is short or long
		GLOB_ENGINE->DrawText
		(
			Point2DFloat(tmLeft + border ,t), size,	_font24, color, item->_char
		);

		float ow = GEngine->GetTextWidth(size, _font24, item->_char);
		float left = tmLeft + floatMax(0.02,ow+0.01);

		GLOB_ENGINE->DrawText
		(
			Point2DFloat(left, t), size, _font24, color, item->_text
		);
		if (!bottom) top += height;
	}
}

#define MIN_X			(-1.0 * H_PI)
#define MAX_X			(1.0 * H_PI)
#define MIN_Y			(-0.25 * H_PI)
#define MAX_Y			(0.25 * H_PI)
#define TOT_W			(MAX_X - MIN_X)
#define TOT_H			(MAX_Y - MIN_Y)
#define INV_W			(1.0 / TOT_W)
#define INV_H			(1.0 / TOT_H)

void InGameUI::DrawTacticalDisplay
(
	const Camera &camera, AIUnit *unit, const TargetList &list
)
{
	Texture *textureDef = GLOB_SCENE->Preloaded(TextureWhite);
	MipInfo mip;
	const int w = GLOB_ENGINE->Width2D();
	const int h = GLOB_ENGINE->Height2D();
	AICenter *center = unit->GetGroup()->GetCenter();
	Assert(center);

	Texture *corner = GLOB_SCENE->Preloaded(Corner);
	DrawFrame
	(
		corner,
		bgColor,
		Rect2DPixel(tdX * w, tdY * h, tdW * w, tdH * h)
	);

	Vector3 dir = Vector3(0, 0, 1);
	float xAngle = atan2(dir.X(), dir.Z());
	float yAngle = atan2(dir.Y(), dir.SizeXZ());
	float leftAngle = atan(camera.Left());
	float topAngle = atan(camera.Top());
	float xMin = xAngle - leftAngle;
	float xMax = xAngle + leftAngle;
	float yMin = yAngle - topAngle;
	float yMax = yAngle + topAngle;
	if (xMax >= MIN_X && xMin <= MAX_X &&
		yMax >= MIN_Y && yMin <= MAX_Y)
	{
		if (yMin < MIN_Y) yMin = MIN_Y;
		if (yMax > MAX_Y) yMax = MAX_Y;
		mip = GLOB_ENGINE->TextBank()->UseMipmap(textureDef, 0, 0);
		if (xMin < MIN_X)
		{
			xMin += TOT_W;
			GLOB_ENGINE->Draw2D(mip, cameraColor,
				Rect2DPixel((tdX + 0.5 * tdW + xMin * tdW * INV_W) * w, (tdY + 0.5 * tdH - yMax * tdH * INV_H) * h,
				((MAX_X - xMin) * tdW * INV_W) * w, ((yMax - yMin) * tdH * INV_H) * h));
			GLOB_ENGINE->Draw2D(mip, cameraColor,
				Rect2DPixel((tdX + 0.5 * tdW + MIN_X * tdW * INV_W) * w, (tdY + 0.5 * tdH - yMax * tdH * INV_H) * h,
				((xMax - MIN_X) * tdW * INV_W) * w, ((yMax - yMin) * tdH * INV_H) * h));
		}
		else if (xMax > MAX_X)
		{
			xMax -= TOT_W;
			GLOB_ENGINE->Draw2D(mip, cameraColor,
				Rect2DPixel((tdX + 0.5 * tdW + xMin * tdW * INV_W) * w, (tdY + 0.5 * tdH - yMax * tdH * INV_H) * h,
				((MAX_X - xMin) * tdW * INV_W) * w, ((yMax - yMin) * tdH * INV_H) * h));
			GLOB_ENGINE->Draw2D(mip, cameraColor,
				Rect2DPixel((tdX + 0.5 * tdW + MIN_X * tdW * INV_W) * w, (tdY + 0.5 * tdH - yMax * tdH * INV_H) * h,
				((xMax - MIN_X) * tdW * INV_W) * w, ((yMax - yMin) * tdH * INV_H) * h));
		}
		else
		{
			GLOB_ENGINE->Draw2D(mip, cameraColor,
				Rect2DPixel((tdX + 0.5 * tdW + xMin * tdW * INV_W) * w, (tdY + 0.5 * tdH - yMax * tdH * INV_H) * h,
				((xMax - xMin) * tdW * INV_W) * w, ((yMax - yMin) * tdH * INV_H) * h));
		}
		//GLOB_ENGINE->TextBank()->ReleaseMipmap();
	}

	Matrix4Val camInvTransform=camera.GetInvTransform();

	const VehicleType *nonstrategic = GWorld->Preloaded(VTypeNonStrategic);

	int n = list.Size();
	PackedColor color;
	bool modeRadar = true;
#if _ENABLE_CHEATS
	if (_showAll) modeRadar = false;
	if (tdCheat) modeRadar = false;
#endif

	EntityAI *myVeh = unit->GetVehicle();
	const VehicleType *manType=GWorld->Preloaded(VTypeMan);

	for (int i=0; i<n; i++)
	{
		Target &tar = *list[i];
		EntityAI *vehExact = tar.idExact;

		if (unit->GetVehicle() == vehExact ) continue;
		if (tar.vanished) continue;

		// tactical display has two modes, radar and show targets
		// each mode displays different information
		// info is distinguished by different "visible" calculation
		float visible = 0;
		float tDim = 0.004;
		Vector3 pos;
		TargetSide side = TSideUnknown;

		if (modeRadar)
		{
			// check landscape visibility and range
			if (!vehExact) continue;
			pos = vehExact->AimingPosition();

			// check visibility to target
			// (only for non-static)
			float dist2 = myVeh->Position().Distance2(pos);
			visible = myVeh->CalcVisibility(vehExact,dist2);
			/*
			LogF
			(
				"Radar: %s (%s) from %s (%s) - %g",
				(const char *)vehExact->GetDebugName(),vehExact->GetShape()->Name(),
				(const char *)myVeh->GetDebugName(),myVeh->GetShape()->Name(),
				visible
			);
			*/

			// check if it is (or might be) man
			//if (vehExact->GetType()->IsKindOf(manType) )
			if (vehExact)
			{
				// should be detected when:
				// is IR target and we have IR scanner
				// or is laser target and we have laser scanner
				// IR scanner can be assumed as always present -
				// irTarget || laserTarget && laserScanner
				// !(!irTarget && (!laserTarget || !laserScanner))

				if
				(
					!vehExact->GetType()->_irTarget &&
					// only laser scanner can see laser targets
					(!vehExact->GetType()->GetLaserTarget() || !myVeh->GetType()->GetLaserScanner())
				)
				{
					tDim = 0.002;
	#if _ENABLE_CHEATS
					if (!tdCheat)
	#endif
						continue;
				}
			}

			side = RadarTargetSide(unit,tar);
		}
		else
		{
			if
			(
				!tar.IsKnownBy(unit)
#if _ENABLE_CHEATS
				&& !_showAll
#endif
			) continue;

			pos =
			(
#if _ENABLE_CHEATS
				_showAll && tar.idExact ?
				tar.idExact->AimingPosition() :
#endif
				tar.LandAimingPosition()
			);
			visible=tar.FadingSpotability();
			//float visible=tar.FadingVisibility();
#if _ENABLE_CHEATS
			if( _showAll )
			{
				visible=1;
				if (vehExact && !vehExact->GetType()->_irTarget)
				{
					tDim = 0.002;
				}
			}
			else
#endif
			{
				if
				(
					tar.type->IsKindOf(manType) || tar.FadingAccuracy()<manType->GetAccuracy()
					|| vehExact && !vehExact->GetType()->_irTarget
				)
				{
					tDim = 0.002;
#if _ENABLE_CHEATS
					if (!tdCheat)
#endif
						continue;
				}
			}

			side = tar.type->_typicalSide;
		}
		
		if (visible<=0.01) continue;


		if (vehExact && vehExact->GetType()->IsKindOf(nonstrategic)) continue;

		dir=camInvTransform.Rotate(pos - camera.Position());

		xAngle = atan2(dir.X(), dir.Z());
		yAngle = atan2(dir.Y(), dir.SizeXZ());
		if (xAngle >= MIN_X && xAngle <= MAX_X &&
			yAngle >= MIN_Y && yAngle <= MAX_Y)
		{
			float tDim2 = tDim * 2;
			float xScreen = (tdX + 0.5 * tdW + xAngle * tdW * INV_W - tDim) * w;
			float yScreen = (tdY + 0.5 * tdH - yAngle * tdH * INV_H - tDim) * h;


			if (tar.idExact && !tar.idExact->EngineIsOn())
			{
				AIGroup *grp = tar.idExact->GetGroup();
				if (grp!=unit->GetGroup())
				{
					side = TCivilian; // empty marked as civilian
				}
			}

			if
			(
				Glob.config.IsEnabled(DTEnemyTag)
#if _ENABLE_CHEATS
				|| _showAll
#endif
			)
			{
				side = tar.side;
				if (tar.destroyed) side = TCivilian;
			}

			if (side == TCivilian)
				color = civilianColor;
			else if (center->IsFriendly(side))
				color = friendlyColor;
			else if (center->IsEnemy(side))
				color = enemyColor;
			else if (center->IsNeutral(side))
				color = neutralColor;
			else
				color = unknownColor;

			//float visible=tar.visibility;
			saturateMin(visible,1);
			color.SetA8(toIntFloor(color.A8()*visible));
			mip = GLOB_ENGINE->TextBank()->UseMipmap(textureDef, 0, 0);
			GLOB_ENGINE->Draw2D
			(
				mip, color,
				Rect2DPixel(xScreen, yScreen, tDim2 * w, tDim2 * h),
				Rect2DPixel(tdX * w, tdY * h, tdW * w, tdH * h)
			);
			//GLOB_ENGINE->TextBank()->ReleaseMipmap();
		}
	}
}

void InGameUI::DrawCompass(EntityAI *vehicle)
{
	//Texture *textureDef = GLOB_SCENE->Preloaded(TextureWhite);
	Texture *texture;
	MipInfo mip;
	const int w = GLOB_ENGINE->Width2D();
	const int h = GLOB_ENGINE->Height2D();

	Texture *corner = GLOB_SCENE->Preloaded(Corner);
	DrawFrame
	(
		corner,
		bgColor,
		Rect2DPixel(coX * w, coY * h, coW * w, coH * h)
	);

//	Vector3Val dir = vehicle->Direction();
	Camera &cam = *GLOB_SCENE->GetCamera();
	Vector3Val dir = cam.Direction();

	// avoid singularity when heading up
	float xAngle;
	if( fabs(dir.X())+fabs(dir.Z())>1e-3 )
		xAngle = atan2(dir.X(), dir.Z());
	else
		xAngle=0;
	xAngle *= 2 * INV_H_PI;
	for (int i=-2; i<=2; i++)
	{
		float xLeft = xAngle + i;
		int xBase = toIntFloor(xLeft);
		if (xLeft - xBase >= 1.0) xBase++;
		int xBaseMod4 = xBase&(4-1);
		xLeft = xLeft + (xBaseMod4-xBase);
		xBase = xBaseMod4;
		PreloadedTexture seg[4]={Compass180,Compass270,Compass000,Compass090};
		texture = GLOB_SCENE->Preloaded(seg[xBase]);
		float xOffset = xLeft - xBase;
		mip = GLOB_ENGINE->TextBank()->UseMipmap(texture, 0, 0);
		float xScreen = (coX + 0.5 * coW + 0.25 * coW * (i - xOffset)) * w;
		GLOB_ENGINE->Draw2D(mip, compassColor, Rect2DPixel(xScreen, coY * h, 0.25 * coW * w, coH * h), Rect2DPixel(coX * w, coY * h, coW * w, coH * h));
		//GLOB_ENGINE->TextBank()->ReleaseMipmap();
	}

	Rect2DPixel compassClip(coX*w,0,coW*w,h);
	float ySize=coH*h*0.5;
	float xSize=ySize*w/h*(3.0/4);
	float xPos=(coX+coW*0.5)*w;
	PackedColor col=compassDirColor;
	GLOB_ENGINE->DrawLine
	(
		Line2DPixel(xPos,coY*h,xPos+xSize,coY*h-ySize),
		col,col,compassClip
	);
	GLOB_ENGINE->DrawLine
	(
		Line2DPixel(xPos,coY*h,xPos-xSize,coY*h-ySize),
		col,col,compassClip
	);

	int weapon = vehicle->SelectedWeapon();
	if (weapon >= 0)
	{
		// draw turret direction

		Vector3Val dir = vehicle->DirectionWorldToModel
		(
			vehicle->GetWeaponDirection(weapon)
		);

		if( dir.SquareSizeXZ()>0.1 )
		{
			xAngle = atan2(dir.X(), dir.Z());
			float xPos = (coX + 0.5 * coW + xAngle * coW * INV_W) * w;
			PackedColor col=compassTurretDirColor;
			GLOB_ENGINE->DrawLine
			(
				Line2DPixel(xPos,coY*h,xPos+xSize,coY*h-ySize),
				col,col,compassClip
			);
			GLOB_ENGINE->DrawLine
			(
				Line2DPixel(xPos,coY*h,xPos-xSize,coY*h-ySize),
				col,col,compassClip
			);
		}
	}
}

void InGameUI::DrawUnitInfo(EntityAI *vehicle)
{
	if (!_unitInfo) return;

	AIUnit *unit = GWorld->FocusOn();
	Assert(unit);
	AISubgroup *subgroup = unit->GetSubgroup();
	Assert(subgroup);
	AIGroup *group = subgroup->GetGroup();
	Assert(group);
	vehicle = unit->IsInCargo() ? unit->GetPerson() : unit->GetVehicle();
	const VehicleType *type = vehicle->GetType();
	Transport *transport = dyn_cast<Transport>(vehicle);

	PackedColor color, barBlinkColor;
	if (_blinkState)
		barBlinkColor = barBlinkOnColor;
	else
		barBlinkColor = barBlinkOffColor;

	UnitInfoType unitInfoType = type->GetUnitInfoType();
	if (unitInfoType == UnitInfoCar) return; // car has no info
	if (unitInfoType != _lastUnitInfoType)
	{
		Assert(unitInfoType >= 0);
		RString name = (Res >> "RscInGameUI" >> "unitInfoTypes")[unitInfoType];
		_unitInfo->Reload(Res >> "RscInGameUI" >> name);
		_lastUnitInfoType = unitInfoType;
	}

	char buffer[256];
	Rank rank = unit->GetPerson()->GetRank();
	bool dirty = false;	

	if (_unitInfo->time)
	{
		_unitInfo->time->SetTime(Glob.clock);
		dirty = true; // ???
	}
	if (_unitInfo->date)
	{
		Glob.clock.FormatDate(LocalizeString(IDS_DATE_FORMAT), buffer);	
		if (_unitInfo->date->SetText(buffer)) dirty = true;
	}
	if (_unitInfo->name)
	{
		if (_unitInfo->name->SetText(unit->GetPerson()->GetInfo()._name)) dirty = true;
	}
	if (_unitInfo->unit)
	{
		sprintf
		(
			buffer,
			"%d %s: %s",
			unit->ID(),
			group->GetName(),
			(const char *)LocalizeString(IDS_PRIVATE + rank)
		);
		if (_unitInfo->unit->SetText(buffer)) dirty = true;
	}
	if (_unitInfo->vehicle)
	{
		if (_unitInfo->vehicle->SetText(vehicle->GetType()->GetDisplayName())) dirty = true;
	}
	if (_unitInfo->speed)
	{
		sprintf
		(
			buffer,
			LocalizeString(IDS_UI_SPEED),
			3.6 * vehicle->ModelSpeed().Z()
		);
		if (_unitInfo->speed->SetText(buffer)) dirty = true;
	}
	if (_unitInfo->alt)
	{
		Vector3Val pos = vehicle->Position();
		float y = pos.Y() + vehicle->GetShape()->Min().Y();
		float y0 = GLOB_LAND->RoadSurfaceYAboveWater(pos.X(), pos.Z()); 
		sprintf
		(
			buffer,
			LocalizeString(IDS_UI_ALT),
			y - y0
		);
		if (_unitInfo->alt->SetText(buffer)) dirty = true;
	}
	if (_unitInfo->valueExp)
	{
		float expCur = unit->GetPerson()->GetExperience() - AI::ExpForRank(rank);
		float expMax = rank >= RankColonel ?
			AI::ExpForRank(RankColonel) :
			AI::ExpForRank((Rank)(rank + 1)) - AI::ExpForRank(rank);
		Assert(expMax > 0);
		float expCoef = 0.9 * (expCur / expMax);
		saturate(expCoef, 0, 1);
		_unitInfo->valueExp->SetPos(expCoef);
	}
	if (_unitInfo->formation)
	{
		if (_unitInfo->formation->SetText(LocalizeString(IDS_COLUMN + subgroup->GetFormation()))) dirty = true;
	}
	if (_unitInfo->combatMode)
	{
		if (_unitInfo->combatMode->SetText(LocalizeString(IDS_IGNORE + unit->GetSemaphore()))) dirty = true;
	}
	if (_unitInfo->valueHealth)
	{
		float health = 0;
		Vehicle *person = unit->GetPerson();
		if (person)
		{
			health = 1 - person->GetTotalDammage();
			saturate(health, 0, 1);
		}
		if (health > 0.5)
			color = barGreenColor;
		else if (health > 0.3)
			color = barYellowColor;
		else if (health > 0.15)
			color = barRedColor;
		else
			color = barBlinkColor;
		_unitInfo->valueHealth->SetPos(health);
		_unitInfo->valueHealth->SetBarColor(color);
	}
	if (_unitInfo->valueArmor)
	{
		float armor = 1 - vehicle->GetTotalDammage();
		saturate(armor, 0, 1);
		if (armor > 0.5)
			color = barGreenColor;
		else if (armor > 0.3)
			color = barYellowColor;
		else if (armor > 0.15)
			color = barRedColor;
		else
			color = barBlinkColor;
		_unitInfo->valueArmor->SetPos(armor);
		_unitInfo->valueArmor->SetBarColor(color);
	}
	if (_unitInfo->valueFuel)
	{
		float fuel = 0;
		float fuelCapacity = vehicle->GetType()->GetFuelCapacity();
		if (fuelCapacity > 0)
		{
			fuel = vehicle->GetFuel() / fuelCapacity;
			saturate(fuel, 0, 1);
		}
		if (fuel > 0.5)
			color = barGreenColor;
		else if (fuel > 0.3)
			color = barYellowColor;
		else if (fuel > 0.15)
			color = barRedColor;
		else
			color = barBlinkColor;
		_unitInfo->valueFuel->SetPos(fuel);
		_unitInfo->valueFuel->SetBarColor(color);
	}

	if (_unitInfo->cargoMan)
	{
		if (transport && transport->GetMaxManCargo() > 0)
		{
			_unitInfo->cargoMan->ShowCtrl(true);
			sprintf(buffer, LocalizeString(IDS_UI_CARGO_INF), transport->GetManCargoSize());
			if (_unitInfo->cargoMan->SetText(buffer)) dirty = true;
		}
		else
		{
			_unitInfo->cargoMan->ShowCtrl(false);
		}
	}
	if (_unitInfo->cargoFuel)
	{
		if (transport && transport->GetMaxFuelCargo() > 0)
		{
			_unitInfo->cargoFuel->ShowCtrl(true);
			sprintf(buffer, LocalizeString(IDS_UI_CARGO_FUEL), transport->GetFuelCargo());
			if (_unitInfo->cargoFuel->SetText(buffer)) dirty = true;
		}
		else
		{
			_unitInfo->cargoFuel->ShowCtrl(false);
		}
	}
	if (_unitInfo->cargoRepair)
	{
		if (transport && transport->GetMaxRepairCargo() > 0)
		{
			_unitInfo->cargoRepair->ShowCtrl(true);
			sprintf(buffer, LocalizeString(IDS_UI_CARGO_REPAIR), transport->GetRepairCargo());
			if (_unitInfo->cargoRepair->SetText(buffer)) dirty = true;
		}
		else
		{
			_unitInfo->cargoRepair->ShowCtrl(false);
		}
	}
	if (_unitInfo->cargoAmmo)
	{
		if (transport && transport->GetMaxAmmoCargo() > 0)
		{
			_unitInfo->cargoAmmo->ShowCtrl(true);
			sprintf(buffer, LocalizeString(IDS_UI_CARGO_AMMO), transport->GetAmmoCargo());
			if (_unitInfo->cargoAmmo->SetText(buffer)) dirty = true;
		}
		else
		{
			_unitInfo->cargoAmmo->ShowCtrl(false);
		}
	}

	int weapon = vehicle->SelectedWeapon();
	int ValidateWeapon(EntityAI *vehicle, int weapon);
	weapon = ValidateWeapon(vehicle, weapon);
	if (weapon >= 0 && weapon < vehicle->NMagazineSlots())
	{
		// non-default weapon is always shown
		if (weapon != 0) dirty = true;

		int ammoCur = 0;
		int maxAmmoCur = 0;
		bool noAmmo = true;
//		int ammoSum = 0;
//		int maxAmmoSum = 0;
		int magazines = 0;
		float reload = 0;
		RString displayName;

		const MagazineSlot &slot = vehicle->GetMagazineSlot(weapon);
		displayName = slot._muzzle->_displayName;
		const Magazine *magazine = slot._magazine;
    const WeaponModeType *mode = NULL;
    if (magazine)
    {
      const MagazineType *type = magazine->_type;
      if (type && slot._mode >= 0 && slot._mode < type->_modes.Size())
        mode = type->_modes[slot._mode];
    }
    if (mode)
		{
			displayName = mode->_displayName;
			if (mode->_ammo)
			{
/*
				ammoSum = ammoCur = magazine->_ammo;
				maxAmmoSum = maxAmmoCur = magazine->_type->_maxAmmo;
*/
				ammoCur = magazine->_ammo;
				maxAmmoCur = magazine->_type->_maxAmmo;
				if (mode->_reloadTime == 0) reload = 0;
				else reload = (magazine->_reloadMagazine + magazine->_reload) / mode->_reloadTime;

				// reserve magazines
				for (int i=0; i<vehicle->NMagazines(); i++)
				{
					const Magazine *reserve = vehicle->GetMagazine(i);
					if (reserve == magazine) continue;
					if (reserve->_type == magazine->_type)
					{
/*
						ammoSum += reserve->_ammo;
						maxAmmoSum += reserve->_type->_maxAmmo;
*/
						if (reserve->_ammo > 0) magazines++;
					}
				}
			}
			noAmmo = ammoCur == 0;
			if (magazine->_type->_maxAmmo == 1)
			{
				ammoCur = magazines;
				if (magazine->_ammo > 0) ammoCur++;
				magazines = 0;
			}
		}
		else if (slot._muzzle->_magazines.Size() > 0)
		{
			maxAmmoCur = slot._muzzle->_magazines[0]->_maxAmmo;
		}

		if (maxAmmoCur > 0)
		{
			if (/*ammoCur == 0*/noAmmo || vehicle->IsActionInProgress(MFReload))
				color = barRedColor;
			else if (reload <= 0)
				color = barGreenColor;
			else if (reload <= 0.2)
				color = barYellowColor;
			else
				color = barRedColor;

			if (_unitInfo->weapon)
			{
				_unitInfo->weapon->ShowCtrl(true);
				_unitInfo->weapon->SetFtColor(color);
				if (_unitInfo->weapon->SetText(displayName)) dirty = true;
			}
			if (_unitInfo->ammo)
			{
				_unitInfo->ammo->ShowCtrl(true);
				_unitInfo->ammo->SetFtColor(color);
//				sprintf(buffer, "%d", ammoSum);
				if (magazines > 0)
					sprintf(buffer, "%d | %d", ammoCur, magazines);
				else
					sprintf(buffer, "%d", ammoCur);
				if (_unitInfo->ammo->SetText(buffer)) dirty = true;
			}
		}
		else
		{
			// no ammo or no muzzle
			if (_unitInfo->weapon)
			{
				_unitInfo->weapon->ShowCtrl(true);
				_unitInfo->weapon->SetFtColor(barGreenColor);
				if (_unitInfo->weapon->SetText(displayName)) dirty = true;
			}
			if (_unitInfo->ammo)
			{
				if (_unitInfo->ammo->SetText("")) dirty = true;
			}
		}
	}
	else
	{
		_unitInfo->weapon->ShowCtrl(false);
		_unitInfo->ammo->ShowCtrl(false);
	}

	if (dirty) _lastUnitInfoTime = Glob.uiTime;

	float age = 0;
	if (vehicle->GetType()->_hideUnitInfo)
		age = Glob.uiTime - _lastUnitInfoTime;
	if (age < piDimEndTime)
	{
		float alpha = 1.0;
		if (age > piDimStartTime)
			alpha = (piDimEndTime - age) / (piDimEndTime - piDimStartTime);
		_unitInfo->DrawHUD(vehicle, alpha);
	
		_hintTop = _unitInfo->background->Y() + _unitInfo->background->H() + 0.02;
	}
}

void InGameUI::DrawGroupDir(const Camera &camera, AIGroup *grp)
{
	if (!grp) return;

	float age = Glob.uiTime - _lastGroupDirTime;
	if (age >= groupDirDimEndTime) return;

	float alpha = 1.0;
	if (age > groupDirDimStartTime)
		alpha = (groupDirDimEndTime - age) / (groupDirDimEndTime - groupDirDimStartTime);

	PackedColor bgColorA=PackedColorRGB(bgColor,toIntFloor(bgColor.A8()*alpha));
	PackedColor ftColorA=PackedColorRGB(ftColor,toIntFloor(ftColor.A8()*alpha));

	const int w = GLOB_ENGINE->Width2D();
	const int h = GLOB_ENGINE->Height2D();

	Texture *corner = GLOB_SCENE->Preloaded(Corner);
	DrawFrame
	(
		corner, bgColorA,
		Rect2DPixel(gdX * w, gdY * h, gdW * w, gdH * h)
	);

	Matrix4Val camInvTransform = camera.GetInvTransform();
	Vector3 dir
	(
		VRotate, camInvTransform,
		grp->MainSubgroup()->GetFormationDirection()
	);
	dir[0] = -dir[0];

	Matrix4 mrot(MZero);
	Matrix4 mtrans1 = Matrix4(MTranslation, Vector3(-0.5, 0, -0.5));
	Matrix4 mtrans2 = Matrix4(MTranslation, Vector3(0.5, 0, 0.5));

	Draw2DPars pars;
	pars.spec = NoZBuf|IsAlpha|IsAlphaFog|ClampU|ClampV;

	const float size = 1.1;

	Rect2DPixel rect
	(
		w * (gdX + gdW * 0.5 - gdW * size * 0.5),
		h * (gdY + gdH * 0.5 - gdH * size * 0.5),
		gdW * w * size, gdH * h * size
	);

	#define v000 VZero
	#define v001 VForward
	#define v100 VAside
	#define v101 Vector3(1, 0, 1)

	mrot.SetUpAndDirection(-VUp, -dir);
	Matrix4 m = mtrans2 * mrot * mtrans1;
	Vector3 uv = m.FastTransform(v000);
	pars.uTR = uv[2];
	pars.vTR = uv[0];
	uv = m.FastTransform(v001);
	pars.uTL = uv[2];
	pars.vTL = uv[0];
	uv = m.FastTransform(v100);
	pars.uBR = uv[2];
	pars.vBR = uv[0];
	uv = m.FastTransform(v101);
	pars.uBL = uv[2];
	pars.vBL = uv[0];

	pars.SetColor(ftColorA);
	pars.mip = GEngine->TextBank()->UseMipmap(_imageGroupDir, 0, 0);
	GEngine->Draw2D(pars, rect, rect);
}

void InGameUI::DrawHint()
{
	if (_hint->GetHint().GetLength() == 0) return;
	float age = Glob.uiTime - _hintTime;
	if (age < hintDimEndTime)
	{
		float alpha = 1.0;
		if (age > hintDimStartTime)
			alpha = (hintDimEndTime - age) / (hintDimEndTime - hintDimStartTime);
		_hint->SetPosition(_hintTop);
		_hint->DrawHUD(NULL, alpha);
	}
}

/*!
\patch 1.56 Date 5/14/2002 by Jirka
- Improved: Ingame UI - icons in unit list
*/

void InGameUI::DrawGroupUnit(AIUnit *u, float xScreen, float yScreen, float alpha, int align)
{
	Assert(u);
	int id = u->ID();
	UnitDescription &info = _groupInfo[id - 1];

	char unitID[8];
#if !_RELEASE
	if (info.leader >= 0)
		sprintf(unitID, "%d(%d)", id, info.leader);
	else
#endif
		sprintf(unitID, "%d", id);

	const int w = GLOB_ENGINE->Width2D();
	const int h = GLOB_ENGINE->Height2D();
	float size = 0.02;
	const float border = 0.005;

	float height = size;
	float width = GEngine->GetTextWidth(size, _font24, unitID);
	switch (align)
	{
	case ST_LEFT: break;
	case ST_CENTER: xScreen -= 0.5 * width; break;
	case ST_RIGHT: xScreen -= width; break;
	}

	PackedColor color;
	if (info.player || info.selected)
	{
		if (info.player)
			color = uiColorPlayer;
		else
			color = uiColorSelected;
		color.SetA8(toIntFloor(color.A8() * alpha));
		float x1 = (xScreen - border) * w;
		float x2 = (xScreen + width + border) * w;
		float y1 = yScreen * h;
		float y2 = (yScreen + height) * h;
		GEngine->DrawLine(Line2DPixel(x1, y1, x2, y1), color, color);
		GEngine->DrawLine(Line2DPixel(x2, y1, x2, y2), color, color);
		GEngine->DrawLine(Line2DPixel(x2, y2, x1, y2), color, color);
		GEngine->DrawLine(Line2DPixel(x1, y2, x1, y1), color, color);
	}
	
	color = teamColors[info.team];
	color.SetA8(toIntFloor(color.A8() * alpha));
	
	GEngine->DrawText
	(
		Point2DFloat(xScreen, yScreen), size, _font24, color, unitID
	);
}

Team GetTeam(int i);

/*!
\patch_internal 1.01 Date 6/26/2001 by Ondra. Add protection message.
*/
void InGameUI::DrawGroupInfo(EntityAI *vehicle)
{
	AIUnit *unit = GWorld->FocusOn();
	Assert(unit);
	AISubgroup *subgroup = unit->GetSubgroup();
	Assert(subgroup);
	AIGroup *group = subgroup->GetGroup();
	Assert(group);

	bool changed = false;
	bool isValid = false;
	int i;
	for (i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *u = group->UnitWithID(i + 1);
		UnitDescription &info = _groupInfo[i];
		bool valid =
		(
			u && u->GetSubgroup() && !group->GetReportedDown(u)
			&& unit->IsGroupLeader() 
		);
		if (valid != info.valid)
		{
			info.valid = valid;
			changed = true;
		}
		if (!valid)
			continue;

		isValid = true;
		EntityAI *veh = u->GetVehicle();
		Transport *trans = u->GetVehicleIn();
		//const VehicleType *type = veh->GetType();

		if (trans != info.vehicle)
		{
			info.vehicle = trans;
			changed = true;
		}
		
		if (u->GetPerson() != info.person)
		{
			info.person = u->GetPerson();
			changed = true;
		}
	
		AIUnit::ResourceState problemStatus = group->GetWorstStateReported(u);	
		bool problems = problemStatus>=AIUnit::RSCritical;
		/*
		bool problems = group()
		(
			u->GetPerson()->GetTotalDammage() > 0.5 ||
			!u->IsSoldier() &&
			(
				veh->GetTotalDammage() > 0.5 ||
				veh->GetFuel() < 0.5 * type->GetFuelCapacity()
			)
		);
		*/
		if (problems != info.problems)
		{
			info.problems = problems;
			changed = true;
		}

		if (u->GetSemaphore() != info.semaphore)
		{
			info.semaphore = u->GetSemaphore();
			changed = true;
		}

		UnitDescription::Status status;
		Command::Message cmd = Command::NoCommand;
		int commander = -1;
		if (u->IsCommander())
		{
			status = UnitDescription::commander;
		}
		else if (u->IsGunner())
		{
			status = UnitDescription::gunner;
		}
		else if (u->IsInCargo())
		{
			status = UnitDescription::cargo;
			if
			(
				veh->CommanderUnit() &&
				veh->CommanderUnit()->GetGroup() == group
			)
			{
				commander = veh->CommanderUnit()->ID();
			}
		}
		else if (u->GetSubgroup() == group->MainSubgroup())
		{
			if (u->IsAway())
				status = UnitDescription::away;
			else
				status = UnitDescription::none;
		}
		else if (!u->GetSubgroup()->HasCommand())
			status = UnitDescription::wait;
		else
		{
			cmd = u->GetSubgroup()->GetCommand()->_message;
			status = UnitDescription::command;
		}

		if (status != info.status)
		{
			info.status = status;
			changed = true;
		}
		if (cmd != info.cmd)
		{
			info.cmd = cmd;
			changed = true;
		}
		if (commander != info.vehCommander)
		{
			info.vehCommander = commander;
			changed = true;
		}

		bool player = u == unit;
		if (player != info.player)
		{
			info.player = player;
			changed = true;
		}

		bool playerVehicle;
		if (status == UnitDescription::cargo)
			playerVehicle = player;
		else
			playerVehicle = veh == unit->GetVehicle();
		if (playerVehicle != info.playerVehicle)
		{
			info.playerVehicle = playerVehicle;
			changed = true;
		}

		bool selected = GetSelectedUnit(i) != NULL;
		if (selected != info.selected)
		{
			info.selected = selected;
			changed = true;
		}

		Team team = GetTeam(i);
		if (team != info.team)
		{
			info.team = team;
			changed = true;
		}

#if !_RELEASE
		int leader;
		if (u->GetSubgroup() == u->GetGroup()->MainSubgroup())
			leader = -1;
		else
			leader = u->GetSubgroup()->Leader()->ID();
		if (leader != info.leader)
		{
			info.leader = leader;
			changed = true;
		}
#endif
	}

	if (!isValid)
		return;

	if (changed)
	{
		_lastGroupInfoTime = Glob.uiTime;
	}
	float age = Glob.uiTime - _lastGroupInfoTime;
	const float startDim=90, endDim=95;
	float Alpha=1;
	if (age > startDim)
	{
		// interpolate dim
		if(age < endDim)
		{
			float factor=(age-startDim)*(1/(endDim-startDim));
			Alpha=(1-factor)+groupInfoDim*factor;
		}
		else Alpha=groupInfoDim;
		// update alpha
	}
	PackedColor bgColorA=PackedColorRGB(bgColor,toIntFloor(bgColor.A8()*Alpha));
	PackedColor capLnColorA=PackedColorRGB(ftColor,toIntFloor(capLnColor.A8()*Alpha));
	PackedColor ftColorA=PackedColorRGB(ftColor,toIntFloor(ftColor.A8()*Alpha));

	//Texture *textureDef = GLOB_SCENE->Preloaded(TextureWhite);
	Texture *texture;
	MipInfo mip;
	const int w = GLOB_ENGINE->Width2D();
	const int h = GLOB_ENGINE->Height2D();
	const float border = 0.005;
	float size = 0.02;

	Texture *corner = GLOB_SCENE->Preloaded(Corner);
	DrawFrame
	(
		corner,
		bgColorA,
		Rect2DPixel(uiX * w, uiY * h, uiW * w, uiH * h)
	);

	// ADD protection

	#if PROTECTION_ENABLED
		float fadeTime = 10.0f;
		if (Glob.uiTime>_timeToPlay && Glob.uiTime<_timeToPlay+fadeTime*2.0f)
		{
			float fade = 1-fabs(Glob.uiTime-_timeToPlay-fadeTime)*(1.0f/fadeTime);
			IF_FADE()
			{
				// preparing FADE message
				#define ENCODE_CHAR(c,i) (char)(c*2+i)

				static const char FadeMessage[]=
				{
					ENCODE_CHAR('O',0),
					ENCODE_CHAR('r',1),
					ENCODE_CHAR('i',2),
					ENCODE_CHAR('g',3),
					ENCODE_CHAR('i',4),
					ENCODE_CHAR('n',5),
					ENCODE_CHAR('a',6),
					ENCODE_CHAR('l',7),
					ENCODE_CHAR(' ',8),
					ENCODE_CHAR('g',9),
					ENCODE_CHAR('a',10),
					ENCODE_CHAR('m',11),
					ENCODE_CHAR('e',12),
					ENCODE_CHAR('s',13),
					ENCODE_CHAR(' ',14),
					ENCODE_CHAR('d',15),
					ENCODE_CHAR('o',16),
					ENCODE_CHAR(' ',17),
					ENCODE_CHAR('n',18),
					ENCODE_CHAR('o',19),
					ENCODE_CHAR('t',20),
					ENCODE_CHAR(' ',21),
					ENCODE_CHAR('f',22),
					ENCODE_CHAR('a',23),
					ENCODE_CHAR('d',24),
					ENCODE_CHAR('e',25),
					ENCODE_CHAR('.',26),
					ENCODE_CHAR(0,27),
				};

				const int len = sizeof(FadeMessage);
				const char *src = FadeMessage;
				char decode[len];
				for (int i=0; i<len; i++)
				{
					decode[i] = ((unsigned char)(src[i])-i)>>1;
				}
				float size = 0.03;
				float w = GEngine->GetTextWidth(size,_font36,decode);
				static const PackedColor colorB=PackedBlack;
				static const PackedColor colorT=PackedWhite;
				int fadeA = toIntFloor(255*fade*0.5);
				saturate(fadeA,0,255);
				PackedColor colorBA=PackedColorRGB(colorB,fadeA);
				PackedColor colorTA=PackedColorRGB(colorT,fadeA);
				float offX = 0.001;
				float offY = 0.001;
				GEngine->DrawText(Point2DFloat(0.5-w*0.5,0.5),size,_font36,colorBA,decode);
				GEngine->DrawText(Point2DFloat(0.5-w*0.5-offX,0.5-offY),size,_font36,colorTA,decode);

			}
		}
	#endif


	float left = uiX + border;
	float width = (uiW - (MAX_UNITS_PER_GROUP + 1) * border)
							* (1.0 / MAX_UNITS_PER_GROUP);
	float height = width * 2.0 / 3.0;
	PackedColor color;
	for (i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		UnitDescription &info = _groupInfo[i];
		if (!info.valid) continue;
		if (info.status == UnitDescription::commander)
		{
			if (info.vehicle->DriverBrain()) continue;
		}
		if (info.status == UnitDescription::gunner)
		{
			if (info.vehicle->DriverBrain() || info.vehicle->CommanderBrain()) continue;
		}

		// draw rectangle
		/*
		if (info.playerVehicle)
		{
			mip = GLOB_ENGINE->TextBank()->UseMipmap(textureDef, 0, 0);
			float x1 = (left - 0.5 * border) * w;
			float x2 = (left + width + 0.5 * border) * w;
			float y1 = (uiY + 0.5 * border) * h;
			float y2 = (uiY + uiH - border) * h;
			GLOB_ENGINE->Draw2D(mip, capLnColorA, x1, y1, 1, y2 - y1);
			GLOB_ENGINE->Draw2D(mip, capLnColorA, x2, y1, 1, y2 - y1);
			GLOB_ENGINE->Draw2D(mip, capLnColorA, x1, y1, x2 - x1, 1);
			GLOB_ENGINE->Draw2D(mip, capLnColorA, x1, y2, x2 - x1, 1);
			//GLOB_ENGINE->TextBank()->ReleaseMipmap();
		}
		*/

		// draw picture
		Texture *picture1 = NULL;
		Texture *picture2 = NULL;

		Assert(info.person);
		if (info.vehicle == NULL || info.status == UnitDescription::cargo)
		{
			int index = info.person->FindWeaponType(MaskSlotSecondary);
			if (index < 0) index = info.person->FindWeaponType(MaskSlotPrimary);
			if (index < 0) index = info.person->FindWeaponType(MaskSlotHandGun);

			if (index >= 0)
			{
				const WeaponType *weapon = info.person->GetWeaponSystem(index);
				picture1 = weapon->GetPicture();
				if (!picture1) picture1 = _imageDefaultWeapons;
			}
			else picture1 = _imageNoWeapons;

			picture2 = info.person->GetPicture();
		}
		else
			picture1 = info.vehicle->GetPicture();

		if (picture1)
		{
			if (info.problems)
				color = pictureProblemsColor;
			else
				color = pictureColor;
			color.SetA8(toIntFloor(color.A8() * Alpha));
			mip = GLOB_ENGINE->TextBank()->UseMipmap(picture1, 0, 0);
			GLOB_ENGINE->Draw2D(mip, color, Rect2DPixel(left * w, (uiY + border) * h, width * w, height * h));
		}
		if (picture2)
		{
			if (info.problems)
				color = pictureProblemsColor;
			else
				color = pictureColor;
			color.SetA8(toIntFloor(color.A8() * Alpha));
			mip = GLOB_ENGINE->TextBank()->UseMipmap(picture2, 0, 0);
			
			float hh = 0.5 * height;
			float ww = 0.75 * hh;
			float xx = left + 0.5 * ww;
			float yy = (uiY + border) + 0.4 * height;

			GLOB_ENGINE->Draw2D(mip, color, Rect2DPixel(xx * w, yy * h, ww * w, hh * h));
		}

		// draw semaphore
		switch (info.semaphore)
		{
		case AI::SemaphoreBlue:
		case AI::SemaphoreGreen:
		case AI::SemaphoreWhite:
			// hold fire indication
			color = holdFireColor;
			color.SetA8(toIntFloor(holdFireColor.A8() * Alpha));
			mip = GLOB_ENGINE->TextBank()->UseMipmap(_imageSemaphore, 0, 0);
			GLOB_ENGINE->Draw2D(mip, color,
				Rect2DPixel(left * w, (uiY + border) * h, semW * w, semH * h));
			break;
		}

		// draw status
		const char *text = NULL;
		switch (info.status)
		{
		case UnitDescription::none:
			break;
		case UnitDescription::wait:
			text = LocalizeString(IDS_STATE_WAIT);
			goto DrawCommand;
		case UnitDescription::away:
			text = LocalizeString(IDS_AWAY);
			goto DrawCommand;
		case UnitDescription::command:
		{
			int icmd = info.cmd;
			if (icmd==Command::AttackAndFire) icmd = Command::Attack;
			text = LocalizeString(IDS_STATE_NOCOMMAND + icmd);
		}
DrawCommand:
			if (text)
			{
//				float widthText = GLOB_ENGINE->GetTextWidth(size, _font24, text);
				GLOB_ENGINE->DrawText
				(
					Point2DFloat(left + semW + border, uiY + 0.5 * border),
					size, _font24, ftColorA, text
				);
			}
			break;
		case UnitDescription::cargo:
			{
				if (info.vehicle)
					texture = info.vehicle->GetPicture();
				else
				{
					// in multiplayer, flag InCargo may be set but vehicle is NULL yet
					// Fail("No vehicle");
					texture = NULL;
				}
				float picW = 0.35 * width;
				float picH = 0.35 * height;
				if (texture)
				{
					mip = GLOB_ENGINE->TextBank()->UseMipmap(texture, 0, 0);
					GLOB_ENGINE->Draw2D(mip, ftColorA,
						Rect2DPixel((left + width - picW) * w, (uiY + border) * h, picW * w, picH * h));
					//GLOB_ENGINE->TextBank()->ReleaseMipmap();
					texture = NULL;
				}
				if (info.vehCommander >= 0)
				{
					float widthText = GLOB_ENGINE->GetTextWidthF(size, _font24, "%d", info.vehCommander);
					GLOB_ENGINE->DrawTextF
					(
						Point2DFloat(left + width - 0.5 * (picW + widthText), uiY + border + picH),
						size, _font24, ftColorA, "%d", info.vehCommander
					);
				}
			}
			break;
		}

		// draw units No
		float yScreen = uiY + uiH - size - border;

		// commander
		const TransportType *type = info.vehicle ? info.vehicle->Type() :NULL;
		if
		(
			info.status != UnitDescription::cargo && type &&
			type->HasCommander()
		)
		{
			AIUnit *u = info.vehicle->CommanderBrain();
			if (u && u->GetGroup() == group)
			{
				float xScreen = left + 2 * border;
				DrawGroupUnit(u, xScreen, yScreen, Alpha, ST_LEFT);
			}
		}

		// driver
		float driverPos = left + width - 2 * border;
		float gunnerPos = left + 0.5 * width;
		int driverAlign = ST_RIGHT;
		int gunnerAlign = ST_CENTER;

		if
		(
			!type || type->DriverIsCommander() || !type->HasGunner()
		)
		{
			swap(driverPos,gunnerPos);
			swap(driverAlign,gunnerAlign);
		}

		{
			AIUnit *u = !info.vehicle || info.status == UnitDescription::cargo ?
				info.person->Brain() :
				info.vehicle->DriverBrain();
			if (u && u->GetGroup() == group)
			{
				//float xScreen = left + 0.5 * width;
				DrawGroupUnit(u, driverPos, yScreen, Alpha, driverAlign);
			}
		}

		// gunner
		if
		(
			info.status != UnitDescription::cargo && type && type->HasGunner()
		)
		{
			AIUnit *u = info.vehicle->GunnerBrain();
			if (u && u->GetGroup() == group)
			{
				//float xScreen = left + width - 2 * border;
				DrawGroupUnit(u, gunnerPos, yScreen, Alpha, gunnerAlign);
			}
		}

		left += width + border;
	}


}

bool InGameUI::DrawMouseCursor(const Camera &camera, AIUnit *unit, bool td)
{
	EntityAI *vehicle = unit->GetVehicle();

	PreloadedTexture cursor = CursorStrategy;
	PreloadedTexture cursor2 = CursorStrategyMove;
	Texture *cursorTex = NULL;
	Texture *cursorTex2 = NULL;
	bool strategic = true;
	switch (_modeAuto)
	{
		case UIFire:
		case UIFirePosLock:
			// check weapon cursor
			cursorTex = vehicle->GetCursorTexture(unit->GetPerson());
			cursorTex2 = cursorTex;
			cursor = cursor2 = CursorAim; strategic = false;
			break;
		case UIStrategy: cursor = CursorStrategyMove; break;
		case UIStrategyMove: cursor = CursorStrategy; break;
		case UIStrategySelect: cursor = CursorStrategySelect; break;
		case UIStrategyAttack: cursor = CursorStrategyAttack; break;
		case UIStrategyFire: cursor = CursorStrategyAttack; break;
		case UIStrategyGetIn: cursor = CursorStrategyGetIn; break;
		case UIStrategyWatch: cursor = CursorStrategyWatch; break; // TODO: new cursor
	}

	// dim cursor with time when non-active
	float age=GetCursorAge();
	const float startDim=5,endDim=10;
	float cursorA=1;
	if( age>startDim )
	{
		// interpolate dim
		if( age<endDim )
		{
			float factor=(age-startDim)*(1/(endDim-startDim));
			cursorA=(1-factor)+cursorDim*factor;
		}
		else cursorA=cursorDim;
		// update alpha
	}
	
	int weapon = vehicle->SelectedWeapon();
	if
	(
		_showCursors &&
		_lockTarget.IdExact() != NULL && weapon >= 0 &&
		(vehicle->CommanderUnit() == unit || vehicle->GunnerUnit() == unit)
	)
	{
		// find locked target info
		Vector3 relPos=_lockTarget->LandAimingPosition()-camera.Position();
		float aimed=vehicle->GetAimed(weapon,_lockTarget);
		// check actual visibility
		DrawTargetInfo
		(
			camera,unit,relPos,
			GPreloadedTextures.New(CursorTarget),
			GPreloadedTextures.New(aimed>0.25 ? CursorLocked : CursorTarget),
			cursorColor,1,aimed,
			_lockTarget,-1,
			_lockTarget.IdExact()!=_target.IdExact(),false, td
		);
	}
	// keep cursor on screen
	// assign cursor direction
	//LogF("World %.1f,%.1f,%.1f",_worldCursor[0],_worldCursor[1],_worldCursor[2]);
	//Matrix4Val camInvTransform=camera.GetInvTransform();

	Vector3 curDir = GetCursorDirection();
	vehicle->LimitCursor(GWorld->GetCameraType(),curDir);


	Vector3 newDir = curDir;
	vehicle->OverrideCursor(GWorld->GetCameraType(),newDir);

	if (_showCursors)
	{
		float textA = cursorA;
		if
		(
#if _ENABLE_CHEATS
			!_showAll &&
#endif
			!vehicle->ShowCursor(weapon,GWorld->GetCameraType()) &&
			!strategic
		)
		{
			cursorA = 0;
		}
		PackedColor color=PackedColorRGB(cursorColor,toIntFloor(cursorColor.A8()*cursorA));

		bool drawInfo = strategic;
		if (!strategic && _target && !_target->type->IsKindOf(GWorld->Preloaded(VTypeStatic)))
		{
			if (_target->side == TCivilian)
				drawInfo = false;
			else if (unit->GetGroup()->GetCenter()->IsFriendly(_target->side))
				drawInfo = Glob.config.IsEnabled(DTFriendlyTag);
			else
				drawInfo = Glob.config.IsEnabled(DTEnemyTag);
		}

		if (!cursorTex) cursorTex = GPreloadedTextures.New(cursor);
		if (!cursorTex2) cursorTex2 = GPreloadedTextures.New(cursor2);
		DrawTargetInfo
		(
			camera,unit,newDir,
			cursorTex,cursorTex2,
			color,textA,textA,_target,_housePos,
			drawInfo,ModeIsStrategy(_mode), td
		);
	}

	return true;
}

float HowMuchInteresting(AIUnit *unit, const Target *tgt);

bool InGameUI::DrawTargetInfo
(
	const Camera &camera, AIUnit *unit, Vector3Par dir,
	Texture *cursor, Texture *cursor2,
	PackedColor color, float cursorA, float cursor2A,
	const Target *target, int housePos,
	bool info, bool extended, bool td
)
{
	EntityAI *vehicle = unit->GetVehicle();

	Matrix4Val camInvTransform=camera.GetInvTransform();

	Vector3Val mDir=camInvTransform.Rotate(dir);

	// width/height assume fovLeft/fovTop = 1/0.75
	AspectSettings asp;
	GEngine->GetAspectSettings(asp);

	const float mScrH=32.0f/(800*asp.topFOV);
	const float mScrW=32.0f/(800*asp.leftFOV);

	float cx,cy;
	{
		Point3 pos = camInvTransform.Rotate(dir);
		if (pos.Z() <= 0) return false;
		float invZ = 1.0 / pos.Z();
		cx = pos.X() * invZ * camera.InvLeft();
		cy = - pos.Y() * invZ * camera.InvTop();
	}

	float mScrX=cx*0.5+0.5-mScrW*0.5;
	float mScrY=cy*0.5+0.5-mScrH*0.5;
	const int w2d = GLOB_ENGINE->Width2D();
	const int h2d = GLOB_ENGINE->Height2D();
	
	const int w3d = GLOB_ENGINE->Width();
	const int h3d = GLOB_ENGINE->Height();

	int mx = toInt(mScrX * w3d);
	int my = toInt(mScrY * h3d);
	int mw = toInt(mScrW * w3d);
	int mh = toInt(mScrH * h3d);

	float xAngle = atan2(mDir.X(), mDir.Z());
	float yAngle = atan2(mDir.Y(), mDir.SizeXZ());
	bool tactical = td && xAngle >= MIN_X && xAngle <= MAX_X &&
		yAngle >= MIN_Y && yAngle <= MAX_Y;

	float xScreen = 0, yScreen = 0;
	if (tactical)
	{
		xScreen = toInt((tdX + 0.5 * tdW + xAngle * tdW * INV_W - 0.5 * tdCurW) * w2d);
		yScreen = toInt((tdY + 0.5 * tdH - yAngle * tdH * INV_H - 0.5 * tdCurH) * h2d);
	}

	Texture *textureDef = GLOB_SCENE->Preloaded(TextureWhite);
	Texture *texture;
	MipInfo mip;
	float size = 0.02;
	AICenter *center = unit->GetGroup()->GetCenter();
	if (vehicle->CommanderUnit() && vehicle->CommanderUnit()->GetGroup())
	{
		center = vehicle->CommanderUnit()->GetGroup()->GetCenter();
	}
	Assert(center);

	if (cursor2!=cursor)
	{ // all strategy cursors use the same background
		PackedColor colorBlack = PackedColorRGB(PackedBlack,toIntFloor(color.A8()*cursor2A));
		texture = cursor2;
		mip = GLOB_ENGINE->TextBank()->UseMipmap(texture,0,0);

		GLOB_ENGINE->Draw2D(mip,colorBlack,Rect2DAbs(mx+1,my+1,mw,mh));
		//GLOB_ENGINE->TextBank()->ReleaseMipmap();
	}

	//if (cursor!=CursorStrategyMove)
	{ // mode dependent cursor
		texture = cursor;
		mip=GLOB_ENGINE->TextBank()->UseMipmap(texture,0,0);

		// draw black to see cursor on light background
		PackedColor colorBlack=PackedColorRGB(PackedBlack, color.A8());
		GLOB_ENGINE->Draw2D(mip,colorBlack,Rect2DAbs(mx+1,my+1,mw,mh));
		
		GLOB_ENGINE->Draw2D(mip,color,Rect2DAbs(mx,my,mw,mh));
		if (tactical)
			GLOB_ENGINE->Draw2D
			(
				mip, tdCursorColor,
				Rect2DPixel(xScreen, yScreen, tdCurW * w2d, tdCurH * h2d),
				Rect2DPixel(tdX * w2d, tdY * h2d, tdW * w2d, tdH * h2d)
			);
		//GLOB_ENGINE->TextBank()->ReleaseMipmap();
	}

	if (cursor2!=cursor)
	{ // all strategy cursors use the same background
		PackedColor color2A=PackedColorRGB(color,toIntFloor(color.A8()*cursor2A));
		texture = cursor2;
		mip=GLOB_ENGINE->TextBank()->UseMipmap(texture,0,0);

		GLOB_ENGINE->Draw2D(mip,color2A,Rect2DAbs(mx,my,mw,mh));
		if (tactical)
			GLOB_ENGINE->Draw2D
			(
				mip, tdCursorColor,
				Rect2DPixel(xScreen, yScreen, tdCurW * w2d, tdCurH * h2d),
				Rect2DPixel(tdX * w2d, tdY * h2d, tdW * w2d, tdH * h2d)
			);
	}



	if( !info ) return true;

	bool reallyExtended=false;
	Object *vObj=target ? target->idExact : NULL;
	EntityAI *v = dyn_cast<EntityAI>(vObj);

	if( extended && v && v->CommanderUnit() && vehicle->CommanderUnit() )
	{
		AIGroup *vGroup=v->CommanderUnit()->GetGroup();
		AIGroup *vehGroup=vehicle->CommanderUnit()->GetGroup();
		// BUG: in VC++5.0
		//reallyExtended=( v->Brain()->GetGroup()==vehicle->Brain()->GetGroup() );
		reallyExtended=( vGroup==vehGroup );
	}
	// draw information texts

	Point2DAbs pos = Point2DAbs((mScrX + mScrW)*w3d, mScrY*h3d);
	GEngine->PixelAlignXY(pos);

	float width, height;
	if( reallyExtended )
	{ // extened info expected
		width = 0.1;
		height = size;
	}
	else
	{ // simple info only
		width = 0;
		height = 0;
	}

	// dimmed colors
	PackedColor bgColorA=PackedColorRGB(cursorBgColor,toIntFloor(cursorBgColor.A8()*cursorA));
	PackedColor ftColorA=PackedColorRGB(ftColor,toIntFloor(ftColor.A8()*cursorA));

	RString vName;
	if( v )
	{
		Assert( target->type );
		vName=target->type->GetDisplayName();
		if (housePos >= 0)
		{
			char buffer[256];
			sprintf(buffer, LocalizeString(IDS_HOUSE_POSITION), housePos + 1);
			vName = vName + RString(" ") + RString(buffer);
		}
		AIUnit *u = v->CommanderUnit();
		if (u)
		{
			Person *person = u->GetPerson();
			if (person->IsNetworkPlayer())
				vName = vName + RString(" (") + person->GetInfo()._name + RString(")");
#if _ENABLE_CHEATS
			else if (CHECK_DIAG(DECombat) && v->GetVarName().GetLength() > 0)
				vName = vName + RString(" (") + v->GetVarName() + RString(")");
#endif
		}
		float vLen=GLOB_ENGINE->GetTextWidthF( size, _font24, vName);
		saturateMax(width,vLen);
		height += size;
	}
	bool distanceValid=_groundPointValid;
	float distance=_groundPointDistance;
/*
	if( v )
	{
		distanceValid=true;
		distance=v->Position().Distance(vehicle->Position());
	}
*/
	if (v)
	{
		distanceValid = true;
#if _ENABLE_CHEATS
		if (_showAll && target->idExact)
		{
			distance = target->idExact->AimingPosition().Distance(vehicle->Position());
		}
		else
#endif
		{
			distance = target->LandAimingPosition().Distance(vehicle->Position());
		}
	}
	
	if( distanceValid )
	{
		// make space for distance info
		float dLen=GLOB_ENGINE->GetTextWidthF( size, _font24, STR_POS_DIST,distance);
		height += size;
		saturateMax(width,dLen);
	}

	mip=GLOB_ENGINE->TextBank()->UseMipmap(textureDef,0,0);
	GLOB_ENGINE->Draw2D(mip, bgColorA, Rect2DAbs(pos.x, pos.y, width * w2d, height * h2d));
	//GLOB_ENGINE->TextBank()->ReleaseMipmap();

	if (distanceValid)
	{
			GLOB_ENGINE->DrawTextF(pos, size,
				_font24, ftColorA, STR_POS_DIST, distance);
		pos.y += GEngine->PixelAlignedY(size*h2d);
	}
	if( v )
	{
		// get AI info
		TargetSide side = target->type->_typicalSide;
		if (!target->idExact->EngineIsOn())
		{
			AIGroup *grp = v->GetGroup();
			if (grp!=unit->GetGroup())
			{
				side = TCivilian; // empty marked as civilian
			}
		}

		if
		(
			Glob.config.IsEnabled(DTEnemyTag)
#if _ENABLE_CHEATS
			|| _showAll
#endif
		)
		{
			side = target->side;
			if (target->destroyed) side = TCivilian;
		}
		if( !center ) color = civilianColor;
		else if (side == TCivilian) color = civilianColor;
		else if (center->IsFriendly(side)) color = friendlyColor;
		else if (center->IsEnemy(side)) color = enemyColor;
		else if (center->IsNeutral(side)) color = neutralColor;
		else color = unknownColor;
		PackedColor colorA=PackedColorRGB(color,toIntFloor(color.A8()*cursorA));

#if _ENABLE_CHEATS
		// do not use stringtable - debug texts only
		if( CHECK_DIAG(DECombat) )
		{
			RString extVName =
			(
				vName + RString(" ") +
				target->type->GetDisplayName() + RString(" ") +
				FindEnumName(target->side) + RString(target->sideChecked ? "" : "?")
			);
			GLOB_ENGINE->DrawText(pos, size, _font24, colorA, extVName);
			pos.y += GEngine->PixelAlignedY(size*h2d); 
		}
		else
#endif
		{
			GLOB_ENGINE->DrawText(pos, size, _font24, colorA, vName);
			pos.y += GEngine->PixelAlignedY(size*h2d); 
		}
		if( reallyExtended )
		{
			AIUnit *u = v->CommanderUnit();
			// extended target info
			GLOB_ENGINE->DrawTextF
			(
				pos, size, _font24, ftColorA,
				"%d: %s",
				u->ID(),(const char *)LocalizeString(IDS_PRIVATE + u->GetPerson()->GetInfo()._rank)
			);
			pos.y += GEngine->PixelAlignedY(size*h2d);
		}
#if _ENABLE_CHEATS
		// do not use stringtable - debug texts only
		if( CHECK_DIAG(DECombat) )
		{
			GLOB_ENGINE->DrawText(pos, size,
				_font24, ftColorA, v->DiagText());
			pos.y += GEngine->PixelAlignedY(size*h2d); 

			if (target->vanished)
			{
				GLOB_ENGINE->DrawText(pos, size,
					_font24, ftColorA, "Vanished");
				pos.y += GEngine->PixelAlignedY(size*h2d); 
			}
			if (target->destroyed)
			{
				GLOB_ENGINE->DrawText(pos, size,
					_font24, ftColorA, "Destroyed");
				pos.y += GEngine->PixelAlignedY(size*h2d); 
			}
			if (GWorld->GetMode() == GModeNetware)
			{
				// draw network ID
				GLOB_ENGINE->DrawTextF
				(
					pos, size,
					_font24, ftColorA,
					"%s: %x",
					v->IsLocal() ? "Local" : "Remote", v->GetNetworkId()
				);
				pos.y += GEngine->PixelAlignedY(size*h2d); 
			}

			AIUnit *u = v->CommanderUnit();
			if( u )
			{

				GLOB_ENGINE->DrawTextF(pos, size,
					_font24, ftColorA, "TTL %.2f",u->GetTimeToLive());
				pos.y += GEngine->PixelAlignedY(size*h2d); 
				char state[256] = "";
				switch (u->GetState())
				{
				case AIUnit::Wait:
					strcpy(state, "Unit - WAIT");
					break;
				case AIUnit::Init:
					strcpy(state, "Unit - INIT");
					break;
				case AIUnit::Busy:
					strcpy(state, "Unit - BUSY");
					break;
				case AIUnit::Completed:
					strcpy(state, "Unit - OK");
					break;
				case AIUnit::Delay:
					sprintf(state, "Unit - DELAY %.1f", u->GetDelay()-Glob.time);
					break;
				case AIUnit::InCargo:
					strcpy(state, "Unit - IN CARGO");
					break;
				case AIUnit::Stopping:
					strcpy(state, "Unit - STOPPING");
					break;
				case AIUnit::Stopped:
					{
						Transport *trans = dyn_cast<Transport>(v);
						float t = trans ? trans->GetGetInTimeout() - Glob.time : 0;
						sprintf(state, "Unit - STOPPED %.1f", t);
					}
					break;
				case AIUnit::Replan:
					strcpy(state, "Unit - REPLAN");
					break;
				case AIUnit::Sending:
					strcpy(state, "Unit - SENDING");
					break;
				default:
					strcpy(state, "Unit - UNDEF");
					break;
				}
				if (*state)
				{
					GLOB_ENGINE->DrawText(pos, size,
						_font24, ftColorA, state);
					pos.y += GEngine->PixelAlignedY(size*h2d); 
				}
				*state=0;
				AISubgroup *subgrp = u->GetSubgroup();
				switch (subgrp->GetMode())
				{				
				case AISubgroup::Wait:
					strcpy(state, "Subgrp - WAIT");
					break;
				case AISubgroup::PlanAndGo:
					strcpy(state, "Subgrp - PLAN&GO");
					break;
				case AISubgroup::DirectGo:
					sprintf(state, "Subgrp - DIRECT GO");
					break;
				default:
					strcpy(state, "Subgrp - UNDEF");
					break;
				}
				if (subgrp->GetCurrent())
				{
					strcat(state, " ");
					strcat(state, LocalizeString(IDS_NOCOMMAND + subgrp->GetCommand()->_message));
					strcat(state, " ");
					strcat(state, subgrp->GetCurrent()->_fsm->GetStateName());
				}
				if (*state)
				{
					GLOB_ENGINE->DrawText(pos, size,
						_font24, ftColorA, state);
					pos.y += GEngine->PixelAlignedY(size*h2d); 
				}
				// semaphore
				*state=0;
				switch (u->GetSemaphore())
				{
					case AI::SemaphoreBlue:
						strcpy(state, "Never Fire, Keep Formation");
						break;
					case AI::SemaphoreGreen:
						strcpy(state, "Hold Fire, Keep Formation");
						break;
					case AI::SemaphoreWhite:
						strcpy(state, "Hold Fire, Loose Formation");
						break;
					case AI::SemaphoreYellow:
						strcpy(state, "Open Fire, Keep Formation");
						break;
					case AI::SemaphoreRed:
						strcpy(state, "Open Fire, Loose Formation");
						break;
				}
				if (*state)
				{
					GLOB_ENGINE->DrawText(pos, size,
						_font24, ftColorA, state);
					pos.y += GEngine->PixelAlignedY(size*h2d); 
				}

				// behaviour
				*state=0;
				switch (u->GetCombatMode())
				{
					case CMCareless:
						strcpy(state, "Careless");
						break;
					case CMSafe:
						strcpy(state, "Safe");
						break;
					case CMAware:
						strcpy(state, "Aware");
						break;
					case CMCombat:
						strcpy(state, "Combat");
						break;
					case CMStealth:
						strcpy(state, "Stealth");
						break;
				}
				if (*state)
				{
					GLOB_ENGINE->DrawText(pos, size,
						_font24, ftColorA, state);
					pos.y += GEngine->PixelAlignedY(size*h2d); 
				}

				AIGroup *grp = subgrp->GetGroup();
				if (grp && grp->GetCurrent())
				{
					strcpy(state, "Grp - ");
					strcat(state, grp->GetCurrent()->_fsm->GetStateName());
					if (*state)
					{
						GLOB_ENGINE->DrawText(pos, size,
							_font24, ftColorA, state);
						pos.y += GEngine->PixelAlignedY(size*h2d); 
					}
				}
			}
		}
#endif

#if _ENABLE_CHEATS
		if( CHECK_DIAG(DECombat) )
		{
			// do not use stringtable - debug texts only
			
			// draw sensor information
			GEngine->DrawTextF
			(
				pos, size, _font24, ftColorA,
				"uncertainity %.2f %.0f",
				target->FadingAccuracy(),Glob.time.Diff(target->accuracyTime)
			);
			pos.y += GEngine->PixelAlignedY(size*h2d);
			GLOB_ENGINE->DrawTextF
			(
				pos, size, _font24, ftColorA,
				"side Uncertainity %.2f %.0f",
				target->FadingSideAccuracy(),Glob.time.Diff(target->sideAccuracyTime)
			);
			pos.y += GEngine->PixelAlignedY(size*h2d);
			/*
			GLOB_ENGINE->DrawTextF
			(
				left, top, size, _font24, ftColorA,
				"visibility %.2f %.0f",
				target->FadingVisibility(),Glob.time.Diff(target->visibilityTime)
			);
			top += size;
			*/
			GLOB_ENGINE->DrawTextF
			(
				pos, size, _font24, ftColorA,
				"spotability %.2f %.0f",
				target->FadingSpotability(),Glob.time.Diff(target->spotabilityTime)
			);
			pos.y += GEngine->PixelAlignedY(size*h2d);
			GLOB_ENGINE->DrawTextF
			(
				pos, size, _font24, ftColorA,
				"seen %.1f k=%c (d=%.1f,sd=%.1f)",
				Glob.time.Diff(target->lastSeen),
				target->isKnown ? 'Y' : 'N',
				target->delay.Diff(Glob.time),
				target->delaySensor.Diff(Glob.time)
			);
			pos.y += GEngine->PixelAlignedY(size*h2d);
			GEngine->DrawTextF
			(
				pos, size, _font24, ftColorA,
				"Error %.0f",target->posError.Size()
			);
			pos.y += GEngine->PixelAlignedY(size*h2d);
			GEngine->DrawTextF
			(
				pos, size, _font24, ftColorA,
				"Pattern %.2f, move %.2f, v. fire %.2f, aud %.2f, a. fire %.2f",
				v->GetHidden(),v->VisibleMovement(),v->VisibleFire(),
				v->Audible(),v->AudibleFire()
			);
			pos.y += GEngine->PixelAlignedY(size*h2d);

			GEngine->DrawTextF
			(
				pos, size, _font24, ftColorA,
				"Interesting %.2f, interest %.2f",
				HowMuchInteresting(unit,target),
				HowMuchInteresting(unit,target)/unit->Position().Distance(target->position)
			);
			pos.y += GEngine->PixelAlignedY(size*h2d);
			//
			AIUnit *u = v->CommanderUnit();
			Person *vb=( u ? u->GetPerson() : NULL );
			Person *vbMe=
			(
				vehicle->CommanderUnit() ? vehicle->CommanderUnit()->GetPerson() : NULL
			);
			//SensorRowID rIdMe=vbMe->GetSensorRowID();
			SensorRowID rId=vb ? vb->GetSensorRowID() : SensorRowID(-1);
			SensorColID cId=v->GetSensorColID();
			SensorList *list=GWorld->GetSensorList();

			{
				RString diag="Vis ignored";
				if
				(
					vehicle->CommanderUnit() ||
					vehicle->GetType()->IsKindOf(GWorld->Preloaded(VTypeStrategic)) ||
					vehicle->GetType()->IsKindOf(GWorld->Preloaded(VTypeAllVehicles))
				)
				{
					if( vbMe && v )
					{
						// do not check acutal visibility of non-strategic targets
						diag=list->DiagText(vbMe,v);
					}
				}
				GLOB_ENGINE->DrawText
				(
					pos, size, _font24, ftColorA,
					diag
				);
				pos.y += GEngine->PixelAlignedY(size*h2d);
			}
			GLOB_ENGINE->DrawTextF
			(
				pos, size, _font24, ftColorA,
				"land vis %.2f, fire vis %.2f",
				GLandscape->Visible(vehicle,v),
				GLandscape->Visible(vehicle,v,1,ObjIntersectFire)
			);
			pos.y += GEngine->PixelAlignedY(size*h2d);
			
			if (u)
			{
				GLOB_ENGINE->DrawTextF
				(
					pos, size, _font24, ftColorA,
					"Form: %.0f, %.0f",
					u->GetFormationRelative().X(), u->GetFormationRelative().Z()
				);
				pos.y += GEngine->PixelAlignedY(size*h2d);
			}

			GLOB_ENGINE->DrawTextF
			(
				pos, size, _font24, ftColorA,
				"Row %d, Col %d",rId,cId
			);
			/*
			GLOB_ENGINE->DrawTextF
			(
				left, top, size, _font24, ftColorA,
				"cell R %d",list->CellDirtyR(rIdMe,cId)
			);
			pos.y += GEngine->PixelAlignedY(size*h2d);
			GLOB_ENGINE->DrawTextF
			(
				left, top, size, _font24, ftColorA,
				"cell C %d",list->CellDirtyC(rIdMe,cId)
			);
			pos.y += GEngine->PixelAlignedY(size*h2d);
			GLOB_ENGINE->DrawTextF
			(
				left, top, size, _font24, ftColorA,
				"rMe %d time %.1f",rIdMe,Glob.time-list->RowUpdatedTime(rIdMe)
			);
			pos.y += GEngine->PixelAlignedY(size*h2d);
			GLOB_ENGINE->DrawTextF
			(
				left, top, size, _font24, ftColorA,
				"r %d time %.1f",rId,Glob.time-list->RowUpdatedTime(rId)
			);
			pos.y += GEngine->PixelAlignedY(size*h2d);
			GLOB_ENGINE->DrawTextF
			(
				left, top, size, _font24, ftColorA,
				"c %d time %.1f",cId,Glob.time-list->ColUpdatedTime(cId)
			);
			pos.y += GEngine->PixelAlignedY(size*h2d);
			GLOB_ENGINE->DrawTextF
			(
				left, top, size, _font24, ftColorA,
				"rp time %.1f",Glob.time-list->RowPosUpdatedTime(rId)
			);
			pos.y += GEngine->PixelAlignedY(size*h2d);
			GLOB_ENGINE->DrawTextF
			(
				left, top, size, _font24, ftColorA,
				"cp time %.1f",Glob.time-list->ColPosUpdatedTime(cId)
			);
			pos.y += GEngine->PixelAlignedY(size*h2d);
			GLOB_ENGINE->DrawTextF
			(
				left, top, size, _font24, ftColorA,
				"r move %.1f",list->RowPosMoved(rId)
			);
			pos.y += GEngine->PixelAlignedY(size*h2d);
			GLOB_ENGINE->DrawTextF
			(
				left, top, size, _font24, ftColorA,
				"c move %.1f",list->ColPosMoved(cId)
			);
			pos.y += GEngine->PixelAlignedY(size*h2d);
			*/
		}
#endif
	}
	return true;
}

void InGameUI::DrawCursor
(
	const Camera &camera, EntityAI *vehicle,
	Vector3Val dir, float size,
	Texture *texture, float width, float height,
	PackedColor color, bool drawInTD,
	CursorTexts texts
)
{
	// width/height assume fovLeft/fovTop = 1/0.75
	AspectSettings asp;
	GEngine->GetAspectSettings(asp);
	width *= 1.0f/asp.leftFOV;
	height *= 0.75f/asp.topFOV;

	const int w3d = GLOB_ENGINE->Width();
	const int h3d = GLOB_ENGINE->Height();

	const int w2d = GLOB_ENGINE->Width2D();
	const int h2d = GLOB_ENGINE->Height2D();

	Matrix4Val camInvTransform=camera.GetInvTransform();
	Vector3Val dirCam = camInvTransform.Rotate(dir);
	if (drawInTD)
	{
		float xAngle = atan2(dirCam.X(), dirCam.Z());
		float yAngle = atan2(dirCam.Y(), dirCam.SizeXZ());
		if
		(
			xAngle >= MIN_X && xAngle <= MAX_X &&
			yAngle >= MIN_Y && yAngle <= MAX_Y
		)
		{
			float xScreen = toInt((tdX + 0.5 * tdW + xAngle * tdW * INV_W - 0.5 * tdCurW) * w2d);
			float yScreen = toInt((tdY + 0.5 * tdH - yAngle * tdH * INV_H - 0.5 * tdCurH) * h2d);
			MipInfo mip=GLOB_ENGINE->TextBank()->UseMipmap(texture,0,0);
			GLOB_ENGINE->Draw2D
			(
				mip, color,
				Rect2DPixel(xScreen, yScreen, tdCurW * w2d, tdCurH * h2d),
				Rect2DPixel(tdX * w2d, tdY * w2d, tdW * w2d, tdH * h2d)
			);
			//GLOB_ENGINE->TextBank()->ReleaseMipmap();
		}
	}

	float invSize = dirCam.InvSize();
	float coef;
	if (size > 0)
	{
		coef = size * invSize * camera.InvLeft();
		saturate(coef, actMin, actMax);
	}
	else if (size > -0.1)
		coef = actMin;
	else
		coef = 1;

	float xMax=1-width * coef;
	float yMax=1-height * coef;
	float xScreen = 0, yScreen = 0;
	bool visibleX = true;
	bool visibleY = true;
	bool rightEdge = false;
	if (dirCam.Z() > 0)
	{
		float invZ= 1/dirCam.Z();
		xScreen = 0.5 * (1.0 + dirCam.X() * invZ * camera.InvLeft() - width * coef);
		yScreen = 0.5 * (1.0 - dirCam.Y() * invZ * camera.InvTop() - height * coef);
		if( xScreen<0 ) xScreen=0,visibleX=false;
		if( xScreen>xMax ) xScreen=xMax,visibleX=false,rightEdge=true;
		if( yScreen<0 ) yScreen=0,visibleY=false;
		if( yScreen>yMax ) yScreen=yMax,visibleY=false;
	}
	else
	{
		visibleX = visibleY = false;
	}

	if( !visibleX )
	{
		Vector3 dir = dirCam;
		// saturate x to fit in screen
		// calculation:
		// xs = dirCam.X()/dirCam.Z() * camera.InvLeft();
		// assume dirCam.Z()==1
		// xs should be in (-0.5,+0.5)
		// xs = dirCam.X()*camera.InvLeft();
		// dirCam.X() = xs/camera.InvLeft() = xs*camera.Left()
		float xAngle = atan2(dir[0],dir[2]);
		// wanted angle
		float wxAngle = atan2(fSign(dir[0])*camera.Left(),1);
		Matrix3 rotY(MRotationY,xAngle-wxAngle);
		dir = rotY*dir;
		rightEdge = ( dir[0]>0 );

		coef = actMin;
		xMax=1-width * coef;
		yMax=1-height * coef;

		float invZ= 1/dir.Z();
		xScreen = 0.5 * (1.0 + dir.X() * invZ * camera.InvLeft() - width * coef);
		yScreen = 0.5 * (1.0 - dir.Y() * invZ * camera.InvTop() - height * coef);

		saturate(xScreen,0,xMax);
		saturate(yScreen,0,yMax);

		texture = GLOB_SCENE->Preloaded(CursorOutArrow);
		visibleX = true;
		//visibleY = true;
	}
	
	if (visibleX)
	{
		Draw2DPars pars;
		pars.mip = GLOB_ENGINE->TextBank()->UseMipmap(texture,0,0);
		pars.spec = NoZBuf|IsAlpha|ClampU|ClampV|IsAlphaFog;
		if( rightEdge )
			pars.SetU(1,0);
		else
			pars.SetU(0,1);
		pars.SetV(0,1);

		Rect2DAbs rect;
		rect.x = xScreen*w3d;
		rect.y = yScreen*h3d;
		rect.w = width*coef*w3d;
		rect.h = height*coef*h3d;

		#if 1 // shadowed weapon cursor
		rect.x += 1;
		rect.y += 1;

		PackedColor colorBlack=PackedColorRGB(PackedBlack, color.A8());
		pars.SetColor(colorBlack);
		GLOB_ENGINE->Draw2D(pars,rect);

		rect.x -= 1;
		rect.y -= 1;
		#endif
		pars.SetColor(color);

		GLOB_ENGINE->Draw2D(pars,rect);

		//GLOB_ENGINE->TextBank()->ReleaseMipmap();

		for (int i=0; i<texts.Size(); i++)
		{
			RString text = texts[i].text;
			if (!text || text.GetLength() == 0) continue;
			float upDown = texts[i].upDown;

			float yScreenText = yScreen + height * coef * upDown;
			float xScreenText = xScreen;
			float textSize = 0.02;
			float textW = GEngine->GetTextWidth(textSize, _font24, text)*w2d/w3d;
			if (rightEdge)
			{
				xScreenText -= textW + 0.003;
			}
			else
			{
				xScreenText += width * coef;
			}

			MipInfo mip=GLOB_ENGINE->TextBank()->UseMipmap(GLOB_SCENE->Preloaded(TextureWhite),0,0);
			GLOB_ENGINE->Draw2D
			(
				mip, cursorBgColor,
				Rect2DAbs(xScreenText * w3d, yScreenText * h3d, textW * w3d, textSize * h2d)
			);
			//GLOB_ENGINE->TextBank()->ReleaseMipmap();

			Point2DAbs pos(xScreenText*w3d , yScreenText*h3d);
			GEngine->PixelAlignXY(pos);
			GEngine->DrawText(pos, textSize, _font24, color, text);
		}
	}
}

void InGameUI::DrawCommand(const Command &cmd, AIUnit *unit, const Camera &camera, bool td, float alpha)
{
	Vector3 pos;
	float size = 0;

	switch (cmd._message)
	{
	case Command::Move:
	case Command::Heal:
	case Command::Repair:
	case Command::Refuel:
	case Command::Rearm:
	case Command::Support:
		pos = cmd._destination;
		break;
	case Command::Attack:
	#if ENABLE_HOLDFIRE_FIX
	case Command::AttackAndFire:
	#endif
	case Command::Fire:
	case Command::GetIn:
		// TODO: only known position
		{
			Target *target=cmd._targetE;
			if( !target )
			{
				Object *tgt = cmd._target;
				const TargetList &visibleList=*VisibleList();
				int n = visibleList.Size();
				for (int i=0; i<n; i++)
				{
					Target *tar = visibleList[i];
					if (tgt == tar->idExact)
					{
						target = tar;
						break;
					}
				}
			}
			if (!target) return;
			pos = target->LandAimingPosition();
			size = target->VisibleSize();
		}
		break;
	case Command::GetOut:
		{
			pos = unit->AimingPosition();
			size = unit->VisibleSize();
		}
		break;
	case Command::Join:
		{
			AISubgroup *s = cmd._joinToSubgroup;
			if (!s) s = unit->GetGroup()->MainSubgroup();
			Assert(s);
			Assert(s != unit->GetSubgroup());
			AIUnit *u = s->Leader();
			if (!u) return;
			pos = u->AimingPosition();
			size = u->VisibleSize();
		}
		break;
	default:
		return;
	}

	Vector3 dir = pos - camera.Position();
	Vector3 dirU = pos - unit->AimingPosition();

	char buffer[256];
	sprintf
	(
		buffer, STR_OBJ_DIST,
		(const char *)LocalizeString(IDS_NOCOMMAND + cmd._message),
		dirU.Size()
	);
	CursorTexts texts;
	texts.Add(buffer, 0);
	PackedColor color = PackedColorRGB
	(
		missionColor,
		toIntFloor(missionColor.A8() * alpha)
	);
	DrawCursor
	(
		camera, unit->GetVehicle(), dir,
		size,
		_iconMission, actW, actH,
		color, td,
		texts
	);
}

void InGameUI::DrawHUDNonAI
(
	const Camera &camera, Entity *vehicle, CameraType cam
)
{
	if (!GWorld->HasOptions())
	{

		// dim cursor with time when non-active
		float age=GetCursorAge();
		const float startDim=5,endDim=10;
		float cursorA=1;
		if( age>startDim )
		{
			// interpolate dim
			if( age<endDim )
			{
				float factor=(age-startDim)*(1/(endDim-startDim));
				cursorA=(1-factor)+cursorDim*factor;
			}
			else cursorA=cursorDim;
			// update alpha
		}
		
		Vector3 curDir = GetCursorDirection();
		vehicle->LimitCursor(GWorld->GetCameraType(),curDir);

		Vector3 newDir = curDir;
		vehicle->OverrideCursor(GWorld->GetCameraType(),newDir);

		if (_showCursors)
		{
			PackedColor color=PackedColorRGB(cursorColor,toIntFloor(cursorColor.A8()*cursorA));

			Texture *cursor1 = GPreloadedTextures.New(CursorAim);

			//{{
			// adapted (simplified) from DrawTargetInfo

			Matrix4Val camInvTransform=camera.GetInvTransform();

			const float mScrH=32.0/600;
			const float mScrW=32.0/800;

			float cx,cy;
			Vector3 pos = camInvTransform.Rotate(newDir);
			if (pos.Z() > 0)
			{
				float invZ = 1.0 / pos.Z();
				cx = pos.X() * invZ * camera.InvLeft();
				cy = - pos.Y() * invZ * camera.InvTop();

				float mScrX=cx*0.5+0.5-mScrW*0.5;
				float mScrY=cy*0.5+0.5-mScrH*0.5;
				const int w = GLOB_ENGINE->Width();
				const int h = GLOB_ENGINE->Height();
				
				int mx = toInt(mScrX * w);
				int my = toInt(mScrY * h);
				int mw = toInt(mScrW * w);
				int mh = toInt(mScrH * h);

				MipInfo mip;

				mip=GLOB_ENGINE->TextBank()->UseMipmap(cursor1,0,0);

				// draw black to see cursor on light background
				PackedColor colorBlack=PackedColorRGB(PackedBlack, color.A8());
				GLOB_ENGINE->Draw2D(mip,colorBlack,Rect2DAbs(mx+1,my+1,mw,mh));
				
				GLOB_ENGINE->Draw2D(mip,color,Rect2DAbs(mx,my,mw,mh));
			}

			////////////////////}}

		}

	}
}

void InGameUI::DrawHUD
(
	const Camera &camera, EntityAI *vehicle, CameraType cam
)
{
	// draw overlay - called before FinishDraw
	// draw global information
	Texture *texture;
	PackedColor color;

	// draw unit related information
	AIUnit *unit = GWorld->FocusOn();
	if (!unit) return;
	AISubgroup *subgroup = unit->GetSubgroup();
	if (!subgroup) return;
	// BUG:
	if (!subgroup->Leader()) return;
	AIGroup *group = subgroup->GetGroup();
	if (!group) return;
	vehicle=unit->GetVehicle();
	if( !vehicle ) return;

	bool isMap = GWorld->HasMap();
	AIUnit *commanderUnit = vehicle->CommanderUnit();
	if (!commanderUnit) commanderUnit = unit;

	if (_showGroupInfo && unit->IsGroupLeader() && group->NUnits() > 1)
		DrawGroupInfo(vehicle);

	if (_showMenu)
		DrawMenu();

	_hintTop = piY;
	if (_showUnitInfo && !isMap)
	{
		DrawUnitInfo(vehicle);
	}

	DrawHint();

	if (GetNetworkManager().IsControlsPaused())
	{
		float age = GetNetworkManager().GetLastMsgAgeReliable();
		BString<256> text;
		sprintf(text, LocalizeString(IDS_MP_NO_MESSAGE), age);

		float w = GEngine->GetTextWidth(_clSize,_clFont,text);
		float h = _clSize;
		GEngine->DrawText
		(
			Point2DFloat(_clX + 0.5f * (_clW - w), _clY + 0.5f * (_clH - h)), _clSize,
			_clFont, _clColor, text
		);
	}

	if (isMap) return;

	if (_showCursors && !GWorld->GetPlayerSuspended())
		_actions.OnDraw();

	// TD cheat
#if _ENABLE_CHEATS
	if (GInput.GetCheat1ToDo(DIK_D))
	{
		tdCheat=!tdCheat;
		GlobalShowMessage(500,"ShowDBase %s",tdCheat ? "On":"Off");
	}
#endif

	Transport *transport = dyn_cast<Transport>(vehicle);
	bool canSeeTD = false;
	bool canSeeCompass = false;
	if (transport)
	{
		int canSee = 0;
		if (unit == transport->ObserverUnit()) canSee = transport->Type()->_commanderCanSee;
		else if (unit == transport->PilotUnit()) canSee = transport->Type()->_driverCanSee;
		else if (unit == transport->GunnerUnit()) canSee = transport->Type()->_gunnerCanSee;
		canSeeTD = (canSee&CanSeeRadar)!=0;
		canSeeCompass = (canSee&CanSeeCompass)!=0;
	}
	bool td = 
#if _ENABLE_CHEATS
		tdCheat || _showAll ||
#endif
		_showTacticalDisplay && canSeeTD;
	if (td)
	{
		const TargetList &visibleList=*VisibleList();
		DrawTacticalDisplay(camera, unit, visibleList);
		// note: tactical display does not necessary imply seeing compass
	}
	if (canSeeCompass)
	{
		DrawCompass(vehicle);
	}

	if (Glob.config.IsEnabled(DTClockIndicator))
		DrawGroupDir(camera, group);

	if (!canSeeTD && !Glob.config.IsEnabled(DTHUD))
	{
		if (_showCursors)
		{
			float age = Glob.uiTime - _lastFormTime;
			if (age < formDimEndTime)
			{
				float alpha = 1.0;
				if (age > formDimStartTime)
					alpha = (formDimEndTime - age) / (formDimEndTime - formDimStartTime);
				// - formation position
				if (commanderUnit != subgroup->Commander() && subgroup->Commander() != NULL)
				{
					if (!unit->IsInCargo())
					{
						PackedColor colorA = PackedColorRGB(missionColor, toIntFloor(missionColor.A8() * alpha));
						Vector3 dir = commanderUnit->GetFormationAbsolute() - camera.Position();
						DrawCursor
						(
							camera, vehicle, dir,
							0,							// size - default
							_iconMission,
							actW, actH,
							colorA,
							false						// do not draw in tactical display
						);
					}
				}
				// - leader
				if (!commanderUnit->IsGroupLeader() && group->Leader() != NULL)
				{
					PackedColor colorA = PackedColorRGB(leaderColor, toIntFloor(missionColor.A8() * alpha));
					Vector3 dir = group->Leader()->AimingPosition() - camera.Position();
					Vector3 dirU = group->Leader()->AimingPosition() - commanderUnit->Position();
					char buffer[256];
					sprintf
					(
						buffer, STR_OBJ_DIST,
						(const char *)LocalizeString(IDS_LEADER),
						dirU.Size()
					);
					CursorTexts texts;
					texts.Add(buffer, 0.7);
					DrawCursor
					(
						camera, vehicle, dir,
						group->Leader()->VisibleSize(),
						_iconLeader, actW, actH,
						colorA, td,
						texts
					);
				}
			}

			// - target
			Target *target = unit->GetTargetAssigned();
			if (target)
			{
				float age = Glob.uiTime - _lastTargetTime;
				if (age < targetDimEndTime)
				{
					float alpha = 1.0;
					if (age > targetDimStartTime)
						alpha = (targetDimEndTime - age) / (targetDimEndTime - targetDimStartTime);
					PackedColor colorA = PackedColorRGB(missionColor, toIntFloor(missionColor.A8() * alpha));

					Vector3 dir = target->LandAimingPosition() - camera.Position();
					Vector3 dirU = target->LandAimingPosition() - unit->AimingPosition();
					char buffer[256];
					
					sprintf
					(
						buffer, STR_OBJ_DIST,
						(const char *)LocalizeString(IDS_MENU_TARGET),
						dirU.Size()
					);
					CursorTexts texts;
					texts.Add(buffer, 0);
					DrawCursor
					(
						camera, vehicle, dir,
						0, // size
						_iconMission, actW, actH,
						colorA,
						td, texts
					);
				}
			}

			// - command
			const Command *cmd = subgroup->GetCommand();
			if (cmd)
			{
				if (cmd->_id != _lastCmdId)
				{
					_lastCmdId = cmd->_id;
					_lastCmdTime = Glob.uiTime;
				}
				float age = Glob.uiTime - _lastCmdTime;
				if (cmd && age < cmdDimEndTime)
				{
					float alpha = 1.0;
					if (age > cmdDimStartTime)
						alpha = (cmdDimEndTime - age) / (cmdDimEndTime - cmdDimStartTime);

					if (commanderUnit != subgroup->Commander() && subgroup->Commander() != NULL)
					{
						if
						(
							cmd->_message == Command::Attack ||
							cmd->_message == Command::AttackAndFire ||
							cmd->_message == Command::Fire
						)
						{
							DrawCommand(*cmd, commanderUnit, camera, td, alpha);
						}
					}
					else
					{
						if (cmd->_context != Command::CtxMission)
							DrawCommand(*cmd, commanderUnit, camera, td, alpha);
					}
				}
			}

			// - selected units
			for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
			{
				AIUnit *u = GetSelectedUnit(i);
				if (!u) continue;
				if (u == unit && GWorld->GetCameraType() < CamExternal) continue;
				float age = Glob.uiTime - _lastSelTime[i];
				if (age >= meDimEndTime) continue;

				float alpha = 1.0;
				if (age > meDimStartTime)
					alpha = (meDimEndTime - age) / (meDimEndTime - meDimStartTime);
				PackedColor colorA = PackedColorRGB(selectColor, toIntFloor(selectColor.A8() * alpha));

				Vector3 dir = u->AimingPosition() - camera.Position();
				char buffer[256];
				CursorTexts texts;
				float dist = u->AimingPosition().Distance(unit->AimingPosition());
				if (dist >= 50.0f)
				{
					sprintf
					(
						buffer, STR_POS_DIST,
						dist
					);
					texts.Add(buffer, 0);
				}
				sprintf
				(
					buffer, "%d",
					i + 1
				);
				texts.Add(buffer, 0.7);
				DrawCursor
				(
					camera, vehicle, dir,
					u->VisibleSize(),
					_iconSelect, actW, actH,
					colorA, td,
					texts
				);
			}
		}
		//goto MouseCursor;
	}
	
	if (_showTankDirection)
		DrawTankDirection(camera);

	if (_showCursors && (canSeeCompass || Glob.config.IsEnabled(DTHUD)))
	{
		// draw active targets
		// - ourself
		if (GWorld->GetCameraType() >= CamExternal)
		{
			float age = Glob.uiTime - _lastMeTime;
			float alpha = 1;
			if (age > meDimStartTime)
			{
				// interpolate dim
				if (age < meDimEndTime)
				{
					float factor = (age - meDimStartTime) * (1 / (meDimEndTime - meDimStartTime));
					alpha = (1 - factor) + meDim * factor;
				}
				else alpha = meDim;
				// update alpha
			}
			PackedColor colorA = PackedColorRGB(meColor, toIntFloor(meColor.A8() * alpha));
			Vector3 dir = commanderUnit->AimingPosition() - camera.Position();
			DrawCursor
			(
				camera, vehicle, dir,
				commanderUnit->VisibleSize(),
				_iconMe, actW, actH,
				colorA, td
			);
		}

		// - selected units
		for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
		{
			AIUnit *u = GetSelectedUnit(i);
			if (!u) continue;
			if (u == unit && GWorld->GetCameraType() < CamExternal) continue;

			Vector3 dir = u->AimingPosition() - camera.Position();
			char buffer[256];
			CursorTexts texts;
			float dist = u->AimingPosition().Distance(unit->AimingPosition());
			if (dist >= 50.0f)
			{
				sprintf
				(
					buffer, STR_POS_DIST,
					dist
				);
				texts.Add(buffer, 0);
			}
			sprintf
			(
				buffer, "%d",
				i + 1
			);
			texts.Add(buffer, 0.7);
			DrawCursor
			(
				camera, vehicle, dir,
				u->VisibleSize(),
				_iconSelect, actW, actH,
				selectColor, td,
				texts
			);
		}
		
		// - leader
		if (!commanderUnit->IsGroupLeader() && group->Leader()!=NULL )
		{
			Vector3 dir = group->Leader()->AimingPosition() - camera.Position();
			Vector3 dirU = group->Leader()->AimingPosition() - commanderUnit->Position();
			char buffer[256];
			sprintf
			(
				buffer, STR_OBJ_DIST,
				(const char *)LocalizeString(IDS_LEADER),
				dirU.Size()
			);
			CursorTexts texts;
			texts.Add(buffer, 0.7);
			DrawCursor
			(
				camera, vehicle, dir,
				group->Leader()->VisibleSize(),
				_iconLeader, actW, actH,
				leaderColor, td,
				texts
			);
		}

		// - target
		Target *target = unit->GetTargetAssigned();
		if (target)
		{
			Vector3 dir = target->LandAimingPosition() - camera.Position();
			Vector3 dirU = target->LandAimingPosition() - unit->AimingPosition();
			char buffer[256];
			
			sprintf
			(
				buffer, STR_OBJ_DIST,
				(const char *)LocalizeString(IDS_MENU_TARGET),
				dirU.Size()
			);
			CursorTexts texts;
			texts.Add(buffer, 0);
			DrawCursor
			(
				camera, vehicle, dir,
				0, // size
				_iconMission, actW, actH,
				missionColor,
				td, texts
			);
		}

		const Mission *mis = group->GetMission();
		if (commanderUnit != subgroup->Commander() && subgroup->Commander() != NULL)
		{
			// no subgroup leader
			Vector3 dir = subgroup->Leader()->AimingPosition() - camera.Position();
			Vector3 dirU = subgroup->Leader()->AimingPosition() - commanderUnit->AimingPosition();
			char buffer[256];
			
			// draw FOLLOW ME
			sprintf
			(
				buffer, STR_OBJ_DIST,
				(const char *)LocalizeString(IDS_JOIN),
				dirU.Size()
			);
			if( subgroup->Leader()!=group->Leader() )
			{
				CursorTexts texts;
				texts.Add(buffer, 0);
				DrawCursor
				(
					camera, vehicle, dir,
					subgroup->Leader()->VisibleSize(),
					_iconMission, actW, actH,
					missionColor,
					td, texts
				);
			}

			// draw FORMATION position
			if (!unit->IsInCargo())
			{
				dir = commanderUnit->GetFormationAbsolute() - camera.Position();
				dirU = commanderUnit->GetFormationAbsolute() - commanderUnit->AimingPosition();
				if (dirU.SquareSizeXZ() > Square(5))
				{
					DrawCursor
					(
						camera, vehicle, dir,
						0,							// size - default
						_iconMission,
						actW, actH,
						missionColor,
						false						// do not draw in tactical display
					);
				}
			}

			// draw COMMAND
			const Command *cmd = subgroup->GetCommand();
			if (cmd)
			{
				if
				(
					cmd->_message == Command::Attack ||
					cmd->_message == Command::AttackAndFire ||
					cmd->_message == Command::Fire
				)
				{
					DrawCommand(*cmd, commanderUnit, camera, td);
				}
			}
		}
		else
		{
			// subgroup leader
			if (group->Leader())
			{
				const Command *cmd = subgroup->GetCommand();
				if
				(
					cmd &&
					cmd->_message != Command::Wait &&
					(!commanderUnit->IsGroupLeader() || cmd->_context != Command::CtxMission)
				)
				{
					DrawCommand(*cmd, commanderUnit, camera, td);
				}
			}
			
			if (commanderUnit->IsGroupLeader() && mis)	
			{
				// group leader
				Assert(mis->_action == Mission::Arcade);
				int &index = group->GetCurrent()->_fsm->Var(0);
				int &waiting = group->GetCurrent()->_fsm->Var(4);
				if (index < group->NWaypoints())
				{
					const ArcadeWaypointInfo &wInfo = group->GetWaypoint(index);
					int type = wInfo.type;
					Point3 pos = mis->_destination;
					if
					(
						//type == ACDESTROY ||
						type == ACGETIN ||
						type == ACJOIN ||
						type == ACLEADER
					)
					{
						// TODO: mission should reference (Target *)
						//if (mis->_targetE != NULL) pos = mis->_target->AimingPosition();
						//else
						if (mis->_target != NULL) pos = mis->_target->Position();
					}

					RString description;
					if (waiting) description = LocalizeString(IDS_SYNC_WAITING);
					else
					{
						if (wInfo.description.GetLength() > 0) description = Localize(wInfo.description);
						else description = LocalizeString(IDS_AC_MOVE + type - ACMOVE);
					}

					Vector3 dir = pos - camera.Position();
					Vector3 dirU = pos - commanderUnit->AimingPosition();
					char buffer[256];
					sprintf
					(
						buffer, STR_OBJ_DIST,
						(const char *)description,
						dirU.Size()
					);
					CursorTexts texts;
					texts.Add(buffer, 0);
					DrawCursor
					(
						camera, vehicle, dir,
						0,
						_iconMission, 2 * actW, 2 * actH,
						missionColor, td,
						texts
					);
				}
			}
		}
	}

//MouseCursor:

	CameraType type = GWorld->GetCameraType();
	if (_showCursors)
	{
		// draw weapon cursor
		const float mouseScrH=32.0/600;
		const float mouseScrW=32.0/800;

		int weapon = vehicle->SelectedWeapon();
		if
		(
			weapon >= 0 &&
			weapon < vehicle->NMagazineSlots() &&
			vehicle->GetMagazineSlot(weapon)._magazine &&
			vehicle->ShowAim(weapon,type) &&
			(vehicle->CommanderUnit() == unit || vehicle->GunnerUnit() == unit)
		)
		{
			if( !ModeIsStrategy(_mode) || _lockTarget!=NULL || _lockAimValidUntil>=Glob.uiTime )
			{
				if (canSeeTD || Glob.config.IsEnabled(DTWeaponCursor))
				{
					texture = vehicle->GetCursorAimTexture(unit->GetPerson());
					if (!texture) texture = GPreloadedTextures.New(CursorWeapon);
					DrawCursor
					(
						camera, vehicle, vehicle->GetWeaponDirection(weapon),
						-1,
						texture, mouseScrW, mouseScrH,
						cursorColor, td
					);
				}
			}
		}

#if _ENABLE_CHEATS
		if (!unit->IsAnyPlayer() && CHECK_DIAG(DECombat))
		{
			// draw watch direction (green)
			texture=GLOB_SCENE->Preloaded(CursorTarget);
			static const PackedColor watchColorTgt(Color(0,1,0,0.5));
			static const PackedColor watchColorPos(Color(1,1,0,0.5));
			static const PackedColor watchColorDir(Color(0,1,1,0.5));
			static const PackedColor watchColorFrm(Color(0,1,1,0.25));

			static const PackedColor targetColor(Color(1,0,0));

			PackedColor color;
			switch (unit->GetWatchMode())
			{
				case AIUnit::WMTgt: color = watchColorTgt; break;
				case AIUnit::WMPos: color = watchColorPos; break;
				case AIUnit::WMNo:  color = watchColorFrm; break;
				//case AIUnit::WMDir:
				default: color = watchColorDir; break;
			}
			DrawCursor
			(
				camera, vehicle, unit->GetWatchDirection(),
				-1,
				texture, mouseScrW, mouseScrH,
				color, td
			);
			texture=GLOB_SCENE->Preloaded(CursorLocked);
			DrawCursor
			(
				camera, vehicle, unit->GetWatchHeadDirection(),
				-1,
				texture, mouseScrW, mouseScrH,
				color, td
			);


			// draw target (red)
		}
#endif
	}

	if (!GWorld->HasOptions())
	{
		if (ModeIsStrategy(_mode) || canSeeTD || Glob.config.IsEnabled(DTWeaponCursor))
			DrawMouseCursor(camera, unit, td);
	}

	if (_dragging)
	{
		Matrix4Val camInvTransform = camera.GetInvTransform();
		Point3 posStart = camInvTransform.Rotate(_startSelection);
		Point3 posEnd = camInvTransform.Rotate(_endSelection);
		if (posStart.Z() > 0 && posEnd.Z() > 0)
		{
			float invZ = 1.0 / posStart.Z();
			float x1 = 0.5 * (1.0 + posStart.X() * invZ * camera.InvLeft());
			float y1 = 0.5 * (1.0 - posStart.Y() * invZ * camera.InvTop());
			invZ = 1.0 / posEnd.Z();
			float x2 = 0.5 * (1.0 + posEnd.X() * invZ * camera.InvLeft());
			float y2 = 0.5 * (1.0 - posEnd.Y() * invZ * camera.InvTop());
			const int w = GLOB_ENGINE->Width();
			const int h = GLOB_ENGINE->Height();
			saturate(x1, 0, 1); x1 *= w;
			saturate(y1, 0, 1); y1 *= h;
			saturate(x2, 0, 1); x2 *= w;
			saturate(y2, 0, 1); y2 *= h;
			GEngine->DrawLine(Line2DPixel(x1, y1, x2, y1), dragColor, dragColor);
			GEngine->DrawLine(Line2DPixel(x2, y1, x2, y2), dragColor, dragColor);
			GEngine->DrawLine(Line2DPixel(x2, y2, x1, y2), dragColor, dragColor);
			GEngine->DrawLine(Line2DPixel(x1, y2, x1, y1), dragColor, dragColor);
		}
	}
}

