#include "wpch.hpp"

#include "progress.hpp"
#include "engine.hpp"
//#include "scene.hpp"
#include "global.hpp"
#include "debugTrap.hpp"
#include "optionsUI.hpp"
#include "paramFileExt.hpp"
#include "world.hpp"
#include "txtPreload.hpp"
#include "scripts.hpp"

#include "mbcs.hpp"

// progress bar

const PackedColor ProgressBackground(0);
#if _ENABLE_DEDICATED_SERVER
bool IsDedicatedServer();
#endif

Ref<Script> ProgressScript;

Progress::Progress()
{
	Reset();
}

Progress::~Progress()
{
}

void Progress::Reset()
{
	_progressTot=0;
	_progressCur=0;
	_progressDrawn=0;

	_clear = true;
}

void Progress::Add( float ammount )
{
	// calculate total estimation
	_progressTot+=ammount;
}

void Progress::Advance( float ammount )
{
	// really advance - change display
	_progressCur+=ammount;	
}
void Progress::SetPassed( float value )
{
	// really advance - change display
	_progressCur=value;	
}
void Progress::SetRest( float value )
{
	// really advance - change display
	_progressCur=_progressTot-value;	
}


bool Progress::Active() const
{
	return GEngine->TextBank() &&
		(_progressTitle.GetLength() > 0 || _progressDisplay);
}


void Progress::Draw()
{
	if (!GEngine->IsAbleToDraw()) return;

	static DWORD lastTime;
	DWORD time = ::GlobalTickCount();
	int deltaMs = time-lastTime;
	lastTime = time;

	if (deltaMs>300) deltaMs=300;
	float deltaT = deltaMs*0.001f;

	if (ProgressScript)
	{

		/*
		if (_progressTot>0)
		{
			float factor=_progressCur/_progressTot;
			GlobalShowMessage(100,"factor %.3f",factor);
		}
		*/

		if (ProgressScript->Simulate(deltaT))
		{
			//ProgressScript.Free();
		}
		
		// draw title and cut effects (if any)
		TitleEffect *tit = GWorld->GetTitleEffect();
		if (tit)
		{
			tit->Simulate(deltaT);
			tit->Draw();
			if (tit->IsTerminated()) GWorld->SetTitleEffect(NULL);
		}
		TitleEffect *cut = GWorld->GetCutEffect();
		if (cut)
		{
			cut->Simulate(deltaT);
			cut->Draw();
			if (cut->IsTerminated()) GWorld->SetCutEffect(NULL);
		}

		if( GEngine->TextBank() && _progressTot>0 )
		{
			float factor=_progressCur/_progressTot;
			_progressDrawn=factor;

		}

		GDebugger.NextAliveExpected(15*60*1000); // no alive expected
	}
	else
	{
		// TODO: offline font loading
		// draw activity indicator
		
		if( _progressTitle.GetLength()>0 && GEngine->TextBank() )
		{
			DWORD time = ::GlobalTickCount()-_progressRefreshBase;
			float factor = fastFmod(time*(1.0f/3000),2);
			if (factor>0.001)
			{
				Draw2DPars pars;
				Texture *texture=GScene->Preloaded(TextureWhite);
				pars.mip=GEngine->TextBank()->UseMipmap(texture,0,0);
				pars.SetU(0,1);
				pars.SetV(0,1);
				pars.spec = NoZBuf|IsAlpha|IsAlphaFog|NoClamp;
				float h=GEngine->Height2D()*0.01;
				float w=GEngine->Width2D()*0.3;
				float ww=w*0.05;
				float x=GEngine->Width2D()*0.5-w*0.5;
				float y=GEngine->Height2D()*0.60-h*0.5;
				
				if (factor>1) factor=2-factor;
				static PackedColor color(Color(0.1,0.1,0.05));
				Rect2DPixel rectA(x+(w-ww)*factor,y,ww,h);
				pars.SetColor(color);
				GEngine->Draw2D(pars,rectA);
			}

			GDebugger.NextAliveExpected(15*60*1000); // no alive expected
			Assert( _progressFont );
			float textW=GEngine->GetTextWidth
			(
				_progressFontSize,_progressFont,_progressTitle
			);
			GEngine->DrawText
			(
				Point2DFloat(0.5-textW*0.5,0.5),
				_progressFontSize,_progressFont,PackedColor((unsigned)-1),_progressTitle
			);
			Draw2DPars pars;
			if( GEngine->TextBank() && _progressTot>0 )
			{
				Texture *texture=GScene->Preloaded(TextureWhite);
				pars.mip=GEngine->TextBank()->UseMipmap(texture,0,0);
				pars.SetU(0,1);
				pars.SetV(0,1);
				pars.spec = NoZBuf|IsAlpha|IsAlphaFog|NoClamp;

				float factor=_progressCur/_progressTot;
				float h=GEngine->Height2D()*0.01;
				float w=GEngine->Width2D()*0.3;
				float x=GEngine->Width2D()*0.5-w*0.5;
				float y=GEngine->Height2D()*0.57-h*0.5;

				_progressDrawn=factor;

				Rect2DPixel rect(x,y,w,h);
				static PackedColor white(Color(1,1,1));
				static PackedColor gray(Color(0.1,0.1,0.1));
				pars.SetColor(gray);
				GEngine->Draw2D(pars,rect);

				Rect2DPixel rectA(x,y,w*factor,h);
				pars.SetColor(white);
				GEngine->Draw2D(pars,rectA);

				//GEngine->TextBank()->ReleaseMipmap();
			}
		}

		if (_progressDisplay)
		{
			_progressDisplay->DrawHUD(NULL,1);
		}

	}	
}

void ProcessMessagesNoWait();

void Progress::Frame()
{
	// no progress drawing on dedicated server
	if (GEngine->InitDrawDone())
	{
		Draw();
		GEngine->FinishDraw();
		GEngine->NextFrame();
	}
	if (GEngine->IsAbleToDraw())
	{
		GEngine->InitDraw(_clear, ProgressBackground);
	}
	ProcessMessagesNoWait();
}

static class GlobalAliveImplementation : GlobalAliveInterface
{
public:
	GlobalAliveImplementation() {Set(this);}
	virtual void Alive();
} GGlobalAliveImplementation;

void GlobalAliveImplementation::Alive()
{
#if _ENABLE_DEDICATED_SERVER
	if (IsDedicatedServer()) return;
#endif
	GProgress.Refresh();
}

Progress GProgress;

void Progress::Refresh()
{
#if _ENABLE_DEDICATED_SERVER
	if (IsDedicatedServer()) return;
#endif
	if (!GEngine->TextBank()) return;
	if (_progressTitle.GetLength()<=0 && !ProgressScript) return;
	// some progress information is diplayed

	// do not update too often
	DWORD cTime=GlobalTickCount();
	int minTime = 100; // max 10 per sec
	float changeRel = fabs(_progressDrawn*_progressTot-_progressCur);
	if( changeRel>0.01*_progressTot ) 
	{
		minTime = 20;
		if( changeRel>0.10*_progressTot ) 
		{
			minTime = 10;
		}
	}
	if( cTime-_lastProgressRefresh<minTime ) return;
	_lastProgressRefresh=cTime;
	Frame();
}

// execute in pairs - no nesting allowed
void Progress::Start( RString title, RString format )
{
//	if (GWorld) GWorld->DisableUserInput(true);

	Start(title,format,NULL,-1);
}

void Progress::Start( RString title, RString format, Ref<Font> font, float size )
{
#if _ENABLE_DEDICATED_SERVER
	if (IsDedicatedServer()) return;
#endif
//	if (GWorld) GWorld->DisableUserInput(true);

	GDebugger.NextAliveExpected(15*60*1000); // no alive expected
	//Assert( _progressTitle.GetLength()==0 );
	//Assert( _progressFormat.GetLength()==0 );
	_progressTitle=title;
	_progressFormat=format;
	_progressCur=0;
	_progressStartTime=Glob.uiTime;
	if( font )
	{
		_progressFont=font;
	}
	else
	{
		const ParamEntry &fontPars=Pars>>"CfgInGameUI">>"ProgressFont";
		_progressFont=GEngine->LoadFont(GetFontID(fontPars>>"font"));
	}
	if (size < 0 ) _progressFontSize = 0.75 * 0.05;
	else _progressFontSize = size * _progressFont->Height();

	// fill both back and front buffer with new content
	Frame();
	Frame();
	Frame();
	_progressRefreshBase=GlobalTickCount();
}

void Progress::Start(AbstractOptionsUI *display)
{
#if _ENABLE_DEDICATED_SERVER
	if (IsDedicatedServer()) return;
#endif
//	if (GWorld) GWorld->DisableUserInput(true);

	GDebugger.NextAliveExpected(15*60*1000); // no alive expected
	_progressDisplay = display;
	_progressCur=0;
	_progressStartTime=Glob.uiTime;
	_progressTitle=NULL;
	_progressFormat=NULL;
//	GWorld->EnableDisplay(false);
	// fill both back and front buffer with new content
	Frame();
	Frame();
	Frame();
}

void Progress::Finish()
{
//	GWorld->EnableDisplay(true);
	_progressTitle=NULL;
	_progressFormat=NULL;
	_progressDisplay = NULL;

	_clear = true;

//	if (GWorld) GWorld->DisableUserInput(false);
}

