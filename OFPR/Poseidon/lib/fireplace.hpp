#ifdef _MSC_VER
#pragma once
#endif

#ifndef _FIREPLACE_HPP
#define _FIREPLACE_HPP

#include "vehicleAI.hpp"
#include "smokes.hpp"
#include "lights.hpp"
#include "dynSound.hpp"

class Fireplace: public VehicleWithAI
{
	typedef VehicleWithAI base;

protected:
	Ref<LightPointOnVehicle> _light;
	Color _lightColor;
	SmokeSource _smoke;
	Ref<SoundObject> _sound;
	bool _burning;

public:
	Fireplace(VehicleType *name);

	bool Burning() const {return _burning;}
	void Inflame(bool fire = true) {_burning = fire;}

	void Simulate( float deltaT, SimulationImportance prec );

	void Sound( bool inside, float deltaT );
	void UnloadSound();

	bool QIsManual() const {return false;}

	bool IsAnimated( int level ) const;
	bool IsAnimatedShadow( int level ) const;
	void Animate( int level );
	void Deanimate( int level );

	void GetActions(UIActions &actions, AIUnit *unit, bool now);
	RString GetActionName(const UIAction &action);
	void PerformAction(const UIAction &action, AIUnit *unit);

	LSError Serialize(ParamArchive &ar);

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx);
	
	USE_CASTING(base)
};

#endif
