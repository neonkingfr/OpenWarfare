#ifdef _MSC_VER
#pragma once
#endif

#ifndef _OPTIONS_UI_HPP
#define _OPTIONS_UI_HPP

#include "types.hpp"
#include <El/Math/math3d.hpp>
#include <Es/Strings/rString.hpp>

//! Basic interface for displays hierarchy
class AbstractOptionsUI : public SerializeClass
{
	public:
	//! Draws displays hierarchy
	/*!
		Draw overlay - called before FinishDraw.
		\param vehicle vehicle with camera on
		\param alpha transparency
	*/
	virtual void DrawHUD(VehicleWithAI *vehicle, float alpha)=NULL;

	//! Single simulation step of displays hierarchy
	/*!
		Simulate controls - called during simulation.
		\param vehicle vehicle with camera on
	*/
	virtual void SimulateHUD(VehicleWithAI *vehicle)=NULL;

	//! destructor
	virtual ~AbstractOptionsUI() {}

	//! Destroys display hierarchy
	/*!
		In most cases removes reference from GWorld.
		\param exit exit code
	*/
	virtual void DestroyHUD(int exit) {}
	//! Initialize display
	virtual void ResetHUD() {};

	//! called by framework if key is pressed (see Windows SDK)
	virtual bool DoKeyDown( unsigned nChar, unsigned nRepCnt, unsigned nFlags ) {return false;}
	//! called by framework if key is depressed (see Windows SDK)
	virtual bool DoKeyUp( unsigned nChar, unsigned nRepCnt, unsigned nFlags ) {return false;}
	//! called by framework if char is entered (see Windows SDK)
	virtual bool DoChar( unsigned nChar, unsigned nRepCnt, unsigned nFlags ) {return false;}
	//! called by framework if IME char is entered (see Windows SDK)
	virtual bool DoIMEChar( unsigned nChar, unsigned nRepCnt, unsigned nFlags ) {return false;}
	//! called by framework if IME composition is entered (see Windows SDK)
	virtual bool DoIMEComposition( unsigned nChar, unsigned nFlags ) {return false;}

	//! called by world simulation when addon is used that has not been registered
	virtual bool DoUnregisteredAddonUsed( RString addon ) {return false;}

	//! returns if display is on top of displays hierarchy
	virtual bool IsTopmost() const {return true;}
	//! returns if game simulation is enabled
	virtual bool IsSimulationEnabled() const {return true;}
	//! returns if game display is enabled
	virtual bool IsDisplayEnabled() const {return true;}
	//! returns if InGameUI (HUD) is enabled
	virtual bool IsUIEnabled() const {return true;}

	virtual LSError Serialize(ParamArchive &ar);
};

AbstractOptionsUI *CreateMainOptionsUI();
AbstractOptionsUI *CreateTurnOptionsUI();
AbstractOptionsUI *CreateEndOptionsUI(int mode);
AbstractOptionsUI *CreateChatUI();
AbstractOptionsUI *CreateVoiceChatUI();
AbstractOptionsUI *CreatePlayerKilledUI();

AbstractOptionsUI *CreatePrepareTurnUI();
AbstractOptionsUI *CreateMainMapUI();

#endif
