#ifndef _MEMCHECK_HPP
#define _MEMCHECK_HPP

// memory allocation tracking - find un-freed blocks
#include <Es/Containers/cachelist.hpp>

#define SPARE_DWORD 4

#define GUARD_SIZE 16

#define GUARD_VAL 0x6a
#define FREE_VAL 0xdd
#define NEW_VAL 0xbb

class MemoryInfo: public CLDLink
{
	private:
	// record all memory blocks
	void *_mem;
	int _size;
	//const char *_file;
	char _file[128-20];
	int _line;

	public:
	MemoryInfo( void *m, int s )
	{
		_mem=m,_size=s;
		_file[0]=0,_line=0;
	}
	MemoryInfo( void *m, int s, const char *file, int line )
	{
		_mem=m,_size=s;
		strncpy(_file,file,sizeof(_file));
		_file[sizeof(_file)-1]=0;
		_line=line;
	}

	void Report() const
	{
		//guardedSize-=sizeof(MemoryInfo **)+sizeof(int); // record memory info pointer

		const int textSize=64;
		char text[textSize+1];
		strncpy(text,(char *)_mem,textSize);
		text[textSize]=0;
		if( textSize>_size ) text[_size]=0;
		if( _file && *_file )
		{
			if( strchr(_file,'.') )
			{
				LogF("%s(%d): Memory %8p:%6d '%s'",_file,_line,_mem,_size,text);
			}
			else
			{
				// _file is probably class name
				LogF("Memory %8p:%6d: '%s':%d",_mem,_size,_file,_line);
			}
		}
		else
		{
			LogF("Memory %8p:%6d '%s'",_mem,_size,text);
		}
	}

	bool Valid() const {return _mem!=NULL && _size>=0;}
	void Invalidate() {_mem=NULL,_size=-1;}

	void *Addr() const {return _mem;}
	int Size() const {return _size;}
	const char *File() const {return _file;}
	int Line() const {return _line;}

	// override memory allocation
	// this array must not use the resources it monitors
	void *operator new ( size_t size ) {return malloc(size);}
	void operator delete ( void *mem ) {free(mem);}
};

class MemList: public CLList<MemoryInfo>
{
	public:
	void *operator new ( size_t size ) {return malloc(size);}
	void operator delete ( void *mem ) {free(mem);}
};

static MemList *PAllocated=new MemList;

#define Allocated (*PAllocated)



inline void *PrepareFree( void *mem )
{
	int *block=(int *)mem-SPARE_DWORD;
	int magic=block[1];
	Assert( magic==15879634 );
	if( magic!=15879634 ) return block;
	MemoryInfo *info=(MemoryInfo *)block[0];
	Assert( info->Valid() );
	if( info->Valid() )
	{
		int fullSize=info->Size()+GUARD_SIZE+SPARE_DWORD*sizeof(int);
		char *guard=(char *)block+fullSize-GUARD_SIZE;
		for( int i=0; i<GUARD_SIZE; i++ )
		{
			if( guard[i]!=GUARD_VAL )
			{
				ErrF("GUARD (after %x) Memory changed.",mem);
				info->Report();
			}
		}
		guard = (char *)(block+2);
		for( int i=0; i<(SPARE_DWORD-2)*sizeof(int); i++ )
		{
			if( guard[i]!=GUARD_VAL )
			{
				ErrF("GUARD (before %x) Memory changed.",mem);
				info->Report();
			}
		}

		memset(block+2,GUARD_VAL,sizeof(int)*(SPARE_DWORD-2));

		// fill invalid memory with FREE_VAL
		info->Delete();
		memset(block,FREE_VAL,fullSize);
		info->Invalidate();
	}
	return block;
}

inline int PrepareAlloc( int size )
{
	return size+sizeof(int)*SPARE_DWORD+GUARD_SIZE; // record memory info pointer
}

inline void *FinishAlloc( void *ret, int size, const char *file="", int line=0 )
{
	if( ret )
	{
		int noGuardSize=size-GUARD_SIZE;
		int origSize=noGuardSize-SPARE_DWORD*sizeof(int);
		int *block=(int *)ret;
		MemoryInfo *info=new MemoryInfo(block+SPARE_DWORD,origSize,file,line);
		Allocated.Insert(info);
		block[0]=(int)info;
		block[1]=15879634;
		memset(block+2,GUARD_VAL,sizeof(int)*(SPARE_DWORD-2));
		void *guard=(char *)ret+noGuardSize;
		ret=block+SPARE_DWORD;
		memset(ret,NEW_VAL,origSize);
		memset(guard,GUARD_VAL,GUARD_SIZE);
	}
	return ret;
}

inline void ReportAllocated()
{
	// report all allocated blocks
	for
	(
		MemoryInfo *info=Allocated.First();
		info;
		info=Allocated.Next(info)
	)
	{
		// report:
		info->Report();
	}
}

#endif
