/*!
\file
Interface for EntityAI class
*/

#ifdef _MSC_VER
#pragma once
#endif

#ifndef _VEHICLE_AI_HPP
#define _VEHICLE_AI_HPP

#include "object.hpp"
#include "vehicle.hpp"
#include "aiTypes.hpp"
#include "animation.hpp"
// #include "paramFile.hpp"
#include "recoil.hpp"

class ParamEntry;

#if _DEBUG
	template <class basic>
	class derived
	{
		int _val;
		public:
		operator basic() const {return _val;}
		operator basic&() {return _val;}
		explicit derived( basic val ){_val=val;}
	};
	class SensorRowID: public derived<int>
	{
		public:
		explicit SensorRowID( int val ):derived<int>(val){}
	};
	class SensorColID: public derived<int>
	{
		public:
		explicit SensorColID( int val ):derived<int>(val){}
	};
#else
	typedef int SensorRowID;
	typedef int SensorColID;
#endif

#include "weapons.hpp"

class UIActions;
class UIAction;

DECL_ENUM(MoveFinishF)
// note: value zero is reserved as no action

enum VehicleKind {VSoft,VArmor,VAir,NVehicleKind};

class Threat
{
	float _data[NVehicleKind];

	public:
	Threat(){_data[0]=_data[1]=_data[2]=0;}
	Threat( float soft, float armor, float air )
	{
		_data[VSoft]=soft,_data[VArmor]=armor,_data[VAir]=air;
	}

	float operator [] ( VehicleKind kind ) const {return _data[kind];}
	float &operator [] ( VehicleKind kind ) {return _data[kind];}

	void operator += ( const Threat &a )
	{
		_data[0]+=a._data[0];
		_data[1]+=a._data[1];
		_data[2]+=a._data[2];
	}
	Threat operator * ( float c ) const
	{
		return Threat(_data[VSoft]*c,_data[VArmor]*c,_data[VAir]*c);
	}
};

enum UnitInfoType
{
	UnitInfoSoldier,
	UnitInfoTank,
	UnitInfoCar,
	UnitInfoShip,
	UnitInfoAirplane,
	UnitInfoHelicopter,
	NUnitInfoType
};

enum LightType
{
	LightTypeMarker,
	LightTypeMarkerBlink,
};

struct LightInfo
{
	LightType type;
	Vector3 position;
	Vector3 direction;
	Color color;
	Color ambient;
	float brightness;
};
TypeIsSimple(LightInfo);

class HitPoint
{
	int _selection; // selection index (in Hitpoints LOD)
	float _armor; // hit point armor
	float _invArmor; // hit point armor
	float _passThrough; // how much of the hit will this part pass
	// to further processing
	int _index; // index in EntityAIType
	int _material; // material index
	FindArray<int> _indexCC; // index of corresponding convex component

	public:
	HitPoint();
	HitPoint
	(
		Shape *shape, const char *name, const char *altName, float armor,
		int material
	);

	HitPoint( Shape *shape, const ParamEntry &par, float armor );
	
	__forceinline int GetSelection() const {return _selection;}
	__forceinline float GetArmor() const {return _armor;}
	__forceinline float GetInvArmor() const {return _invArmor;}
	__forceinline float GetPassThrough() const {return _passThrough;}

	__forceinline void SetIndex(int index){_index=index;}
	__forceinline void AddIndexCC(int indexCC){_indexCC.AddUnique(indexCC);}
	__forceinline void SetIndexCC(const FindArray<int> &array){_indexCC = array;}

	__forceinline int GetIndex() const {return _index;}
	__forceinline int GetMaterial() const {return _material;}
	__forceinline bool IsConnectedCC(int i) const {return _indexCC.Find(i)>=0;}
};

TypeIsSimple(HitPoint)

typedef AutoArray< InitPtr<HitPoint> > HitPointList;

#define DEF_HIT(shape,var,name,altName,armor) \
	var=HitPoint(shape->HitpointsLevel(),name,altName,armor,-1), \
	var.SetIndex(_hitPoints.Add(&var))

#define DEF_HIT_CFG(shape,var,par,armor) \
	var=HitPoint(shape->HitpointsLevel(),par,armor), \
	var.SetIndex(_hitPoints.Add(&var))

struct ExtSoundInfo
{
	RStringB name;
	AutoArray<SoundPars> pars;
};
TypeIsMovableZeroed(ExtSoundInfo);

struct ReflectorInfo
{
	Color color;
	Color colorAmbient;
	int positionIndex;
	int directionIndex;
	Vector3 position;
	Vector3 direction;
	float size;
	float brightness;
	AnimationSection selection; // used to Hide/Unhide
	HitPoint hitPoint;

	void Load(EntityAIType &type, const ParamEntry &cls, float armor);
};
TypeIsMovableZeroed(ReflectorInfo)

//#define WeaponInfo		WeaponModeType
//#define AmmoInfo			MagazineType

template <class Type>
struct EncryptedTraits
{
  typedef Type EncryptedType;
  enum {Key=0xa5a5a5a5};
  static EncryptedType Encrypt(Type val)
  {
    return val^Key;
  }
  static Type Decrypt(EncryptedType val)
  {
    return val^Key;
  }
};

#if 0 //_ENABLE_REPORT
// declare specialization - used to avoid compilation during developement
template <>
struct EncryptedTraits<int>
{
  typedef int EncryptedType;
  static int Encrypt(int val);
  static int Decrypt(int val);
};

#endif

template <class Type, class Traits=EncryptedTraits<Type> >
class Encrypted
{
  typedef typename Traits::EncryptedType EncryptedType;
  
  EncryptedType _value;

  static EncryptedType Encrypt(Type value) {return Traits::Encrypt(value);}
  static Type Decrypt(EncryptedType value) {return Traits::Decrypt(value);}
  
  public:
  operator Type() const {return Decrypt(_value);}
  Encrypted(){}
  Encrypted(Type val){_value=Encrypt(val);}

  Type operator += (Type val)
  {
    Type res = Decrypt(_value)+val;
    _value = Encrypt(res);
    return res;
  }
  Type operator -= (Type val)
  {
    Type res = Decrypt(_value)-val;
    _value = Encrypt(res);
    return res;
  }

};

#include <Es/Memory/normalNew.hpp>

class Magazine : public RefCount
{
public:
	const Ref<MagazineType> _type;	
	Encrypted<int> _ammo;
	int _burstLeft; // how many shots are there in the burst (auto fired)
	float _reload;
	float _reloadMagazine;

	int _creator;
	int _id;

private:
	Magazine(const Magazine &src); // no copy - _id would be the same
	void operator =(const Magazine &src); // no copy - _id would be the same

public:
	Magazine(const MagazineType *type); // each magazine must have a type
	~Magazine();

	LSError Serialize(ParamArchive &ar);
	static Magazine *CreateObject(ParamArchive &ar);

	static Magazine *CreateObject(NetworkMessageContext &ctx);
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);

	USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

//#define AmmoState			Magazine
//#define MuzzleState		Magazine

class MagazineSlot
{
public:
	// magazine - can be NULL
	Ref<Magazine> _magazine;
	int _mode;

	// weapon
	Ref<WeaponType> _weapon;
	Ref<MuzzleType> _muzzle;

public:
	MagazineSlot();
	// TODO: serialization, network transport
};
TypeIsMovableZeroed(MagazineSlot)

struct WeaponCargoItem
{
	WeaponType *weapon;
	int count;
};
TypeIsMovableZeroed(WeaponCargoItem)

struct MagazineCargoItem
{
	MagazineType *magazine;
	int count;
};
TypeIsMovableZeroed(MagazineCargoItem)

typedef AutoArray<WeaponCargoItem> WeaponCargo;
typedef AutoArray<MagazineCargoItem> MagazineCargo;

// different units have access to different sensors

enum CanSee
{
	CanSeeRadar=1,
	CanSeeEye=2,
	CanSeeOptics=4,
	CanSeeEar=8,
	CanSeeCompass=16,
	CanSeeAll=~0
};

// camera parameters
struct ViewPars
{
	float _initAngleY,_minAngleY,_maxAngleY;
	float _initAngleX,_minAngleX,_maxAngleX;
	float _initFov,_minFov,_maxFov;

	void Load( const ParamEntry &cfg );
	void InitVirtual
	(
		CameraType camType, float &heading, float &dive, float &fov
	) const;
	void LimitVirtual
	(
		CameraType camType, float &heading, float &dive, float &fov
	) const;
};

class PlateInfo
{
public:
	Vector3 _center;
	Vector3 _normal;
	float _size;
	Offset _face;

public:
//	PlateInfo();
	void Init(Shape *shape, Offset face);
//	bool Invalid() const {return _face<Offset(0);}
};
TypeIsSimpleZeroed(PlateInfo)

#include "font.hpp"

class PlateInfos
{
protected:
	AutoArray<PlateInfo> _plates[MAX_LOD_LEVELS];
	Ref<Font> _font;
	PackedColor _color;

public:
	void Init(LODShape *shape, RString name, FontID font, PackedColor color);
	void Draw(int level, ClipFlags clipFlags, const FrameBase &pos, const char *text) const;
};

struct ReloadAnimationType
{
	Ref<WeaponType> weapon;
	Ref<AnimationType> animation;
	float multiplier;
};

TypeIsMovableZeroed(ReloadAnimationType)

struct UserTypeAction
{
	RString displayName;
	Vector3 modelPosition;
	float radius;
	RString condition;
	RString statement;
};

TypeIsMovableZeroed(UserTypeAction)

#include <El/Common/enumNames.hpp>

//! entity event enum defined using enum factory
#define ENTITY_EVENT_ENUM(type,prefix,XX) \
	XX(type, prefix, Killed) /*object:killer*/ \
	XX(type, prefix, Hit) /*object:causedBy,scalar:howmuch*/ \
	XX(type, prefix, Engine) /*bool:engineState*/ \
	\
	XX(type, prefix, GetIn) /*string:position,object:unit*/ \
	XX(type, prefix, GetOut) /*string:position,object:unit*/ \
	\
	XX(type, prefix, Fired) /*string:weapon,string:muzzle,string:mode,string:ammo*/ \
	XX(type, prefix, IncomingMissile) /*string:ammo,object:whoFired*/ \
	XX(type, prefix, Dammaged) /*string:name,scalar:howmuch*/ \
	XX(type, prefix, Gear) /*bool:gearState*/ \
	XX(type, prefix, Fuel) /*bool:fuelState*/ \
	\
	XX(type, prefix, AnimChanged) /*string:animation*/ \
	\
	XX(type, prefix, Init) /**/

DECLARE_ENUM(EntityEvent,EE,ENTITY_EVENT_ENUM)

//! Information shared between EntityAI objects of the same type

class EntityAIType: public EntityType
{
	typedef EntityType base;

	// information common to all vehicles of the same type
	friend class EntityAI;
	friend class VehicleTypeBank;
	friend class Transport;

	protected:
	
	HitPointList _hitPoints;

	Ref<Texture> _picture; // UI picture
	Ref<Texture> _icon;		 // UI Map icon

	// side markings
	AnimationSection _unitNumber,_groupSign,_sectorSign,_sideSign;
	AnimationSection _clan;
	AnimationSection _dashboard;

	AnimationSection _backLights;

	RefArray<WeaponType> _weapons;					// weapons (types)
	RefArray<MagazineType> _magazines;			// magazines (types)

	PlateInfos _squadTitles;
	
	AutoArray<ReloadAnimationType> _reloadAnimations;
	RString _eventHandlers[NEntityEvent];

	public: // tired of writing access functions
	// all attributes made public
	bool _shapeReversed;

	bool _forceSupply;
	bool _showWeaponCargo;

	float _camouflage; // some targets are very difficult to see
	float _audible; // audible/visible ratio (0..1)

	float _spotableNightLightsOff; // night spotability coeficients
	float _spotableNightLightsOn;
		
	float _visibleNightLightsOff; // night target recognition 
	float _visibleNightLightsOn;

	int _commanderCanSee;
	int _driverCanSee;
	int _gunnerCanSee;

	RString _displayName;
	RString _nameSound;
	TargetSide _typicalSide; // which side uses it normally
	DestructType _destrType;
	// typical destruction type
	// DestructDefault means autodetect

	int _weaponSlots;
	
	Vector3 _supplyPoint;
	float _supplyRadius;
	Vector3 _extCameraPosition;
	Vector3 _extCameraUp;

	float _armor;
	float _invArmor;
	float _logArmor;
	float _cost;
	float _fuelCapacity;

	float _structuralDammageCoef;

	float _secondaryExplosion;

	float _sensitivity; // sensitivity compared to man
	float _sensitivityEar; // sensitivity compared to man

	float _brakeDistance;
	float _precision;
	float _formationX,_formationZ;
	float _formationTime;
	float _invFormationTime;
	
	float _steerAheadSimul;
	float _steerAheadPlan;

	float _predictTurnSimul;
	float _predictTurnPlan;

	float _minFireTime;

	float _irScanRangeMin; //! min. IR scanner range
	float _irScanRangeMax; //! max. IR scanner range
	float _irScanToEyeFactor; //! ratio of IR scanner range to eye distance

	bool _laserScanner; // equipped with lasser scanner?

	bool _irTarget; // is IR lock possible?
	bool _laserTarget; // is laser lock possible (used only for virtual laser target)

	bool _irScanGround; // IR capable of tracking ground targets

	bool _attendant;
	bool _nightVision;
		
	bool _preferRoads;
	bool _hideUnitInfo;

	ViewPars _viewPilot;

	// other properties
	UnitInfoType _unitInfoType;

	float _maxFuelCargo;
	float _maxRepairCargo;
	float _maxAmmoCargo;
	int _maxWeaponsCargo;
	int _maxMagazinesCargo;
	WeaponCargo _weaponCargo;
	MagazineCargo _magazineCargo;
	VehicleKind _kind;
	Threat _threat;

	float _minCost;
	float _maxSpeed; // max speed - level road

	SoundPars _mainSound;
	SoundPars _envSound;
	SoundPars _dmgSound;
	SoundPars _crashSound;
	SoundPars _getInSound,_getOutSound;
	SoundPars _servoSound;
	SoundPars _landCrashSound;
	SoundPars _waterCrashSound;
	AutoArray<ExtSoundInfo> _extEnvSounds;

	AutoArray<ReflectorInfo> _reflectors;

	AutoArray<LightInfo> _lights;
	AnimationSection _showDmg;
	Vector3 _showDmgPoint;

	AutoArray<AnimationSection> _hiddenSelections;

	AutoArray<UserTypeAction> _userTypeActions;

	public:
	EntityAIType( const ParamEntry *param );
	~EntityAIType();

	virtual void Load(const ParamEntry &par);

	float GetCamouflage() const {return _camouflage;}
	float GetAudible() const {return _audible;}

	float GetSpotableNightLightsOff() const {return _spotableNightLightsOff;}
	float GetSpotableNightLightsOn() const {return _spotableNightLightsOn;}
		
	float GetVisibleNightLightsOff() const {return _visibleNightLightsOff;}
	float GetVisibleNightLightsOn() const {return _visibleNightLightsOn;}

	TargetSide GetTypicalSide() const {return _typicalSide;}
	DestructType GetDestructType() const {return _destrType;}

	virtual void InitShape(); // after shape is loaded
	virtual void DeinitShape(); // before shape is unloaded

	__forceinline float GetCost() const {return _cost;}
	__forceinline float GetArmor() const {return _armor;}
	__forceinline float GetInvArmor() const {return _invArmor;}
	__forceinline float GetLogArmor() const {return _logArmor;}
	__forceinline float GetFuelCapacity() const {return _fuelCapacity;}

	__forceinline bool GetIRTarget() const {return _irTarget;} // is IR lock possible?
	__forceinline bool GetLaserTarget() const {return _laserTarget;} // is laser lock possible?
	float GetIRScanRange() const;
	__forceinline bool GetLaserScanner() const {return _laserScanner;}
	__forceinline bool GetIRScanGround() const {return _irScanGround;} // IR tracks ground targets

	__forceinline bool GetNightVision() const {return _nightVision;}

	__forceinline bool PreferRoads() const {return _preferRoads;}
	__forceinline UnitInfoType GetUnitInfoType() const {return _unitInfoType;}

	__forceinline float GetMaxSpeed() const {return _maxSpeed;}
	float GetTypSpeed() const {return _maxSpeed*0.66f;}

	float GetMaxSpeedMs() const {return _maxSpeed*(1.0f/3.6f);}
	float GetTypSpeedMs() const {return _maxSpeed*(1.0f/3.6f)*0.66f;}

	__forceinline float GetMinCost() const {return _minCost;}

	__forceinline float GetSteerAheadSimul() const {return _steerAheadSimul;}
	__forceinline float GetSteerAheadPlan() const {return _steerAheadPlan;}

	__forceinline float GetPredictTurnSimul() const {return _predictTurnSimul;}
	__forceinline float GetPredictTurnPlan() const {return _predictTurnPlan;}

	__forceinline float GetMinFireTime() const {return _minFireTime;}	

	virtual bool HasDriver() const {return true;}
	virtual bool HasGunner() const {return false;}
	virtual bool HasCommander() const {return false;}
	virtual bool HasCargo() const {return false;}

	__forceinline float GetBrakeDistance() const {return _brakeDistance;}
	__forceinline float GetPrecision() const {return _precision;}
	__forceinline float GetFormationX() const {return _formationX;}
	__forceinline float GetFormationZ() const {return _formationZ;}
	__forceinline float GetFormationTime() const {return _formationTime;}
	__forceinline float GetInvFormationTime() const {return _invFormationTime;}
	
	__forceinline Vector3Val GetSupplyPoint() const {return _supplyPoint;}
	__forceinline float GetSupplyRadius() const {return _supplyRadius;}

	__forceinline float GetMaxFuelCargo() const {return _maxFuelCargo;}
	__forceinline float GetMaxRepairCargo() const {return _maxRepairCargo;}
	__forceinline float GetMaxAmmoCargo() const {return _maxAmmoCargo;}
	__forceinline int GetMaxWeaponsCargo() const {return _maxWeaponsCargo;}
	__forceinline int GetMaxMagazinesCargo() const {return _maxMagazinesCargo;}

	__forceinline const WeaponCargo &GetWeaponCargo() const {return _weaponCargo;}
	__forceinline const MagazineCargo &GetMagazineCargo() const {return _magazineCargo;}
	__forceinline bool IsAttendant() const {return _attendant;}

	__forceinline VehicleKind GetKind() const {return _kind;}
	__forceinline Threat GetThreat() const {return _threat;}
	virtual Threat GetDammagePerMinute
	(
		float distance2, float visibility, EntityAI *vehicle=NULL
	) const;
	virtual Threat GetStrategicThreat( float distance2, float visibility, float cosAngle ) const;


	__forceinline float GetStructuralDammageCoef() const {return _structuralDammageCoef;}
	__forceinline const HitPointList &GetHitPoints() const {return _hitPoints;}
	__forceinline HitPointList &GetHitPoints() {return _hitPoints;}

	__forceinline const SoundPars &GetMainSound() const {return _mainSound;}
	__forceinline const SoundPars &GetEnvSound() const {return _envSound;}
	__forceinline const SoundPars &GetDmgSound() const {return _dmgSound;}
	__forceinline const SoundPars &GetCrashSound() const {return _crashSound;}
	__forceinline const SoundPars &GetGetInSound() const {return _getInSound;}
	__forceinline const SoundPars &GetGetOutSound() const {return _getOutSound;}
	__forceinline const SoundPars &GetServoSound() const {return _servoSound;}
	__forceinline const SoundPars &GetLandCrashSound() const {return _landCrashSound;}
	__forceinline const SoundPars &GetWaterCrashSound() const {return _waterCrashSound;}
	const SoundPars &GetEnvSoundExt(RStringB name) const;
	const SoundPars &GetEnvSoundExtRandom(RStringB name) const;

	//RString GetName() const {return _parClass->GetName();}
	
	__forceinline const RString &GetDisplayName() const {return _displayName;}
	__forceinline const RString &GetNameSound() const {return _nameSound;}

	__forceinline Texture *GetIcon() const {return _icon;}

	// weapons and magazines
	int NWeaponSystems() const {return _weapons.Size();}
	const WeaponType *GetWeaponSystem(int i) const {return _weapons[i];}
	int NMagazines() const {return _magazines.Size();}
	const MagazineType *GetMagazine(int i) const {return _magazines[i];}

	int AddWeapon(RStringB name);
	int AddMagazine(RStringB name);

	protected:
	void AddHitPoints(const ParamEntry &par);
	
};

typedef EntityAIType VehicleType;

LSError Serialize(ParamArchive &ar, RString name, const EntityType * &value, int minVersion);
LSError Serialize(ParamArchive &ar, RString name, const EntityType * &value, int minVersion, const EntityType *defValue);

LSError Serialize(ParamArchive &ar, RString name, const EntityAIType * &value, int minVersion);
LSError Serialize(ParamArchive &ar, RString name, const EntityAIType * &value, int minVersion, const EntityAIType *defValue);

const float MaxDammageWorking=0.75; // can move/attack

class Person; // basic driver type

EntityAI *NewVehicleWithAI( RString name );

struct UNIT_STATE;

//! recalculate and reuse if possible line of sight to target

class VisibilityTracker
{
	friend class VisibilityTrackerCache;

	OLink<EntityAI> _obj;
	Time _lastTime;
	float _lastValue;

	public:
	VisibilityTracker();
	VisibilityTracker( EntityAI *obj );
	~VisibilityTracker();

	//! get visibility value or calulate a new one if recent is not available
	float Value
	(
		const EntityAI *sensor, int weapon, float reserve, float maxDelay
	);
};

TypeIsGeneric(VisibilityTracker);

//! calculate and cache visibility value for several targets

class VisibilityTrackerCache
{
	AutoArray<VisibilityTracker> _trackers;

	public:
	VisibilityTrackerCache();
	~VisibilityTrackerCache();

	void Clear();

	//! calculate and cache visibility value
	float Value( const EntityAI *sensor, int weapon, EntityAI *obj, float reserve=1.0, float maxDelay=0.3 );
	//! calculate and cache visibility value unless too many values are already cached
	float KnownValue( const EntityAI *sensor,  int weapon, EntityAI *obj, float reserve=1.0, float maxDelay=0.3 );
};

class Path;

typedef OLink<TargetType> TargetId;

struct Target;
class TargetList;

class LinkTarget: public LLink<Target>
{
	public:
	LinkTarget(){}
	LinkTarget( Target *tgt ):LLink<Target>(tgt){}

	TargetType *IdExact() const;
};

DECL_ENUM(OperItemType)

class ClothObject;

class Flag : public Entity
{
protected:
	typedef Entity base;
	SRef<ClothObject> _cloth;

	AnimationSection _fabric; //,_flagstaff;

	Ref<Texture> _texture;

public:
	Flag(EntityType *name, int id);
	~Flag();

	void Init( Matrix4Par pos );
	void Draw( int level, ClipFlags clipFlags, const FrameBase &pos );
	void FlagSimulate( Matrix4Par pos,float deltaT, SimulationImportance prec );

	bool IsAnimated(int level) const {return true;}
	bool IsAnimatedShadow(int level) const {return true;}

	void FlagAnimate(FrameBase &frame,int level);
	void FlagDeanimate(FrameBase &frame,int level);

	void SetFlagTexture(Texture *texture);

	USE_CASTING(base)
};

//!< Combat state of target

enum TargetState
{
	TargetDestroyed, //! Any target destroyed
	TargetAlive, //! Any target alive
	TargetEnemyEmpty, //! Enemy target, cannot move
	TargetEnemy, //! Enemy target, cannot fire (no weapons or weapons broken)
	TargetEnemyCombat, //! Enemy target, can both move and fire
};

//! Variables neccessary to decide what target should we fire at
/*!
	The structure exists mainly to enable same function decide for
	Transport commander and gunner
*/
struct FireDecision
{
	int _fireMode; //!< What weapon should be used
	LinkTarget _fireTarget; //!< What target vehicle should fire at
	bool _firePrepareOnly; //!< Do not fire, watch only
	Time _nextWeaponSwitch; //!< Time of next enabled weapon change
	//! Time when acquiring target is enabled if no target no target is acquired
	Time _nextTargetAquire;
	//! Time when changing target is enabled
	Time _nextTargetChange;

	//! Initial state of target.
	//! We continue to fire until state is decreased.
	TargetState _initState;
	
	//! Constructor
	FireDecision();
	//! Change target
	void SetTarget(AIUnit *sensor, Target *tgt);
	//! Check if target changed state (and we can stop firing at it)
	bool GetTargetFinished(AIUnit *sensor) const;
};

struct ActionContextBase: public RefCount
{
	MoveFinishF function;
	ActionContextBase();
};

DECL_ENUM(UIActionType)

//! description of user (designer) defined action
struct UserActionDescription
{
	int id;
	RString text;
	RString script;

	LSError Serialize(ParamArchive &ar);
};
TypeIsMovableZeroed(UserActionDescription)

class LightReflectorOnVehicle;

class IndicesUpdateEntityAIWeapons;

//! general EntityAI state indices 

class IndicesUpdateVehicleAI : public IndicesUpdateVehicle
{
	typedef IndicesUpdateVehicle base;

public:
	int fireTarget;
	int pilotLight;
	IndicesUpdateEntityAIWeapons *weapons;

	IndicesUpdateVehicleAI();
	~IndicesUpdateVehicleAI();
	NetworkMessageIndices *Clone() const {return new IndicesUpdateVehicleAI;}
	void Scan(NetworkMessageFormatBase *format);
};

//! dammage EntityAI state indices

class IndicesUpdateDammageVehicleAI : public IndicesUpdateDammageObject
{
	typedef IndicesUpdateDammageObject base;

public:
	int isDead;
	int hit;

	IndicesUpdateDammageVehicleAI();
	~IndicesUpdateDammageVehicleAI();
	NetworkMessageIndices *Clone() const {return new IndicesUpdateDammageVehicleAI;}
	void Scan(NetworkMessageFormatBase *format);
};

class GameValue;
//enum UserAction;

//! AI entity - subject or object of AI activity
/*!
Principal class for any entity that is controlled by AI or noticed by AI.
\patch_internal 1.01 Date 7/10/2001 by Jirka
- _allowDammage disabled if !_ENABLE_CHEATS
\patch_internal 1.12 Date 8/7/2001 by Ondra
- removed obsolete Force Feedback veriables.
*/

class EntityAI: public Entity
{
	typedef Entity base;

	public:

	//! State during pausing for fire
	enum FireState
	{
		FireInit, //!< Fire decision was just made
		FireAim, //!< During aiming
		FireAimed, //!< Aimed, wait for shot
		FireDone, //!< Success or time-out, continue normal activity
	};

	protected:	
	RString _varName;	//!< Name of scripting variable	

	Vector3 _stratGoToPos; //!< Position the vehicle is trying to reach

	AutoArray<float> _hit; //!< Hitpoints (local dammage)

	bool _isDead; //!< Entity dead and no longer usable
	bool _isStopped; //!< Entity is stable - no simulation required
	bool _inFormation; //!< Pilot has reached destination
	bool _showFlag; //!< Draw flag (Capture the flag support)

	bool _isUpsideDown; //!< Vehicle is upside down (crew must get out)
	bool _lockedSoldier; //!< Type of lock (all vehicles or all vehicles but soldiers)

	bool _userStopped; //!< Stopped by user
	bool _pilotLight; //!< Light on
	// patched
#if _ENABLE_CHEATS
	bool _allowDammage; //!< Dammage enabled
#endif

	// locking
	bool _locked; //!< Should be locked
	bool _tempLocked; //!< Locks are acutally locked in locking map

	float _lockedRadius; //!< How large is the lock?
	SRef<AILocker> _locker; //!< locker helper
	Vector3 _lockedBeg; //!< Where is the lock?

	RString _surfaceSound; //!< Sound on current surface (used in Man)

	Time _lastMovement; //!< When last movement prevented stopping

	float _shootAudible; //!< How much is our weapon firing audible (0 default)
	float _shootVisible; //!< How much is our weapon firing visible (0 default)
	float _shootTimeRest; //!< How long will current fire visibility last (before returning to 1.0)
	OLink<TargetType> _shootTarget; //!< What we fired at
	OLink<Entity> _lastShot; //!< Last shot fired
	Time _lastShotAtAssignedTarget; //!< Time of last shot fired at assigned target
	Time _lastShotTime; //!< Time of last shot fired

	OLink<EntityAI> _lastDammage; //!< Last entity causing dammage to this
	Time _lastDammageTime; //!< Time of last dammage caused by crash

	Link<AbstractWave> _reloadSound; //!< Playing sound of reloading weapon
	Link<AbstractWave> _reloadMagazineSound; //!< Playing sound of reloading magazine

	SensorColID _sensorColID; //!< Column is SensorList matrix

	int _currentWeapon; //!< Index of selected weapon (in magazine slots)
	int _forceFireWeapon; //!< Index of weapon to fire ordered by GameStateExt::ObjFire

	RefArray<WeaponType> _weapons;					//!< Weapons
	AutoArray<MagazineSlot> _magazineSlots;	//!< Modes on magazines on weapons 
	RefArray<Magazine> _magazines;					//!< Magazines (both on weapons and reserve)

	LinkArray<AbstractWave> _weaponFired; //!< Playing sound of the weapon
	AutoArray<Time> _weaponFiredTime; //!< Time when the sound _weaponFired started

	//! Continuos visibility tracker - remmember results and reuse them
	//! Normal visibility (SensorList) is not precise enough when firing
	mutable VisibilityTrackerCache _visTracker;
	float _rearmCredit; //!< Used during rearm (when using cost instead of magazine)
	
	// GoTo/FireAt
	float _avoidSpeed; //!< If we decide to brake, we will brake for some time
	Time _avoidSpeedTime; //!< When we decided to brake
	float _avoidAside; //< Obstacle avoidance offset (current)
	float _avoidAsideWanted; //< Obstacle avoidance offset (target value)
	float _limitSpeed; //!< Speed limit (used to slow down formation leader)

	//! Last time when simple path was succesfully found
	//! Used in order to avoid repeating simple path test too often
	Time _lastSimplePath;

	//! Fire decision state for gunner
	FireDecision _fire;

	// so that it can be easily moved to AIUnit
	FireState _fireState;
	Time _fireStateDelay;
	// soonest times of different decisions allowed

	//! laser target generated by laser targeting weapon
	OLink<EntityAI> _laserTarget;
	//! laser target master switch (on/off)
	bool _laserTargetOn;

	mutable Time _lastWeaponReady; //!< Time when last "Ready to fire" was reported
	mutable Time _lastWeaponNotReady; //!< Time when "Cannot fire was reported"

	/*!
	\name Hide decision
	Current hide status, set during Hide command
	//\todo This should be probably moved to AIUnit or command context
	*/
	//@{
	mutable LinkTarget _hideTarget; //!< Which target are we hiding from
	mutable OLink<Object> _hideBehind; //!< What obstacle are we hiding near
	mutable Time _hideRefreshTime; //!< Last time when HideThink executed
	//@}

	/*!
	\name Engage decision
	Current attack test status, set during engage
	//\todo This should be probably moved to AIUnit
	*/

	//@{
	mutable LinkTarget _attackTarget; //! <Which target are we engaging
	//! Time when thinking about engaging this target thinking stared
	mutable Time _attackEngageTime;
	mutable Time _attackRefreshTime; //!< Last refresh of _attackXXXResult
	mutable Vector3 _attackAggresivePos; //!< Best position for desperate attack
	mutable Vector3 _attackEconomicalPos; //!< Best position for carefull attack
	mutable FireResult _attackAggresiveResult;
	mutable FireResult _attackEconomicalResult;
	//@}
	
	//FFEffects _ff; // result of ff simulation - only in SimCamera mode

	//! Time when function EntityAI::TrackTargets(TargetList &res, bool initialize)
	// was last performed
	Time _trackTargetsTime;
	//! Time when function EntityAI::NewTargets was last performed
	Time _newTargetsTime;
	//! Time when function EntityAI::TrackNearTargets was last performed
	Time _trackNearTargetsTime;

	//! Distance to nearest known enemy (used to change helicopter behaviour)
	//\todo Use CombatMode instead
	float _nearestEnemy;
	
	//! Light of all lights on vehicle
	RefArray<LightReflectorOnVehicle> _reflectors;

	//! Flag (support for Capture the flag)
	Ref<Flag> _flag;

	//! Squad texture
	Ref<Texture> _squadTexture;

	//! Textures on hidden selections
	RefArray<Texture> _hiddenSelectionsTextures;
	
	//! user defined actions
	/*!
		\patch 1.01 Date 06/15/2001 by Jirka
		- Added: user (designer) defined actions
	*/
	//! List of all user defined actions
	AutoArray<UserActionDescription> _userActions;
	//! Id for next action generated by this entity
	int _nextUserActionId;
	//! all event handlers for given event
	AutoArray<RString> _eventHandlers[NEntityEvent];

	//! last request AskForAimWeapon sent to server
	AutoArray<Vector3> _aimWeaponAsked;
	//! last request AskForAimObserver sent to server
	Vector3 _aimObserverAsked;
	/*!
		\name Recoil
		\patch_internal 1.12 Date 08/07/2001 by Ondra
		- Moved: was in Man class
	*/
	//@{
	Ref<RecoilFunction> _recoil;
	float _recoilTime; //!< current position in recoil
	float _recoilFactor; //!< recoil factor - used when prone or crouch
	// convert recoil to force feedback
	int _recoilFFIndex; // which recoil ramp is currently played

	//! Start recoil effect
	void StartRecoil( RecoilFunction *recoil, float recoilFactor );
	//! Start one force feedback ramp based on recoil
	void StartRecoilFF();

	//@}

	private:
	EntityAI(); //! disable default constructor
	EntityAI( const EntityAI &src ); //<! disable copying
	void operator = ( const EntityAI &src );	//<! disable copying
	
	public:
	// constructor
	EntityAI(EntityAIType *type, bool fullCreate=true);
	// destructor
	~EntityAI();

	//! Get name of object in script environment
	RString GetVarName() const {return _varName;}
	//! Remmember name of object in script environment
	void SetVarName(RString name) {_varName = name;}

	//! Enable drawing flag
	void ShowFlag(bool showFlag = true) {_showFlag = showFlag;}

	//! Set target position unit is moving to
	void GoToStrategic( Vector3Par pos );
	//! Set if unit should stop when target position is reached
	/*! Used to brake before reaching.*/
	virtual bool StopAtStrategicPos() const;

	//! check if dammaging object is enabled
	bool GetAllowDammage() const
#if _ENABLE_CHEATS
	{return _allowDammage;}
#else
	{return true;}
#endif
	//! Allow dammaging object
	//!\todo Remove this function. Note demo mission ending will be unplayable then.
	void SetAllowDammage(bool val)
#if _ENABLE_CHEATS
	{_allowDammage=val;}
#else
	{}
#endif

	//! Notify entity formation has been changed
	void FormationChanged() {_inFormation=true;}

	//! Place in steady position
	virtual void PlaceOnSurface(Matrix4 &trans);

	//! Set speed limit (used to limit speed of formation leader).
	virtual void LimitSpeed( float speed );
	//! Check how long must unit fire on single target,
	//! before enabling switching to another.
	virtual float FireValidTime() const {return 15;}

	//! Check if HUD should be shown
	//! \deprecated Obsolete function -  never used. Use config instead.
	virtual bool HasHUD() const {return false;}
	//! Check if weapons are enabled (especially firing)
	virtual bool DisableWeapons() const;

	//@{ Actions
	//! Get user friendly name of action
	virtual RString GetActionName(const UIAction &action);
	//! Perform action
	virtual void PerformAction(const UIAction &action, AIUnit *unit);
	//! Get list of available actions
	virtual void GetActions(UIActions &actions, AIUnit *unit, bool now);

	//! Check if some action is beign processed
	//! (used for actions connected to animation)
	virtual bool CheckActionProcessing(UIActionType action, AIUnit *unit) const;
	//! Start processing action (start playing animationif necessary)
	virtual void StartActionProcessing(const UIAction &action, AIUnit *unit);

	//! Add new user action
	int AddUserAction(RString text, RString script);
	//! Remove user action
	void RemoveUserAction(int id);
	//! Find user action
	const UserActionDescription *FindUserAction(int id) const;
	//@}

	//@{ Event handlers
	//! add event handler (return handle
	int AddEventHandler(EntityEvent event, RString expression);
	//! remove given event handler
	void RemoveEventHandler(EntityEvent event, int handle);
	//! remove all event handlers
	void ClearEventHandlers(EntityEvent event);
	//! get list of event handlers
	const AutoArray<RString> &GetEventHandlers(EntityEvent event) const;
	//! generic event handler with typical parameter sets
	void OnEvent(EntityEvent event, const GameValue &pars);
	//! generic event handler with typical parameter sets
	void OnEvent(EntityEvent event, EntityAI *par1);
	//! generic event handler with typical parameter sets
	void OnEvent(EntityEvent event, EntityAI *par1, float par2);
	//! generic event handler with typical parameter sets
	void OnEvent(EntityEvent event, bool par1);
	//! generic event handler with typical parameter sets
	void OnEvent(EntityEvent event, RString par1);
	//! generic event handler with typical parameter sets
	void OnEvent(EntityEvent event, RString par1, EntityAI *par2);
	//! generic event handler with typical parameter sets
	void OnEvent(EntityEvent event, RString par1, float par2);
	//! generic event handler with no parameters
	void OnEvent(EntityEvent event);

	//! check if there is some event handler
	bool IsEventHandler(EntityEvent event) const;
	//@}

	//! Handgun is selected (works as primary weapon)	
	virtual bool IsHandGunSelected() const {return false;}
	//! Select handgun to work as primary weapon
	virtual void SelectHandGun(bool set = true) {}

	//! Get index of first most primary weapon in magazineSlots
	int FirstWeapon() const;
	
	//! Check if slot is empty
	bool EmptySlot(const MagazineSlot &slot) const;
	//! Check max. primary level of non-empty weapons
	int MaxPrimaryLevel() const;
	//! Switch to next primary weapon
	int NextWeapon(int weapon) const;
	//! Switch to previous primary weapon
	int PrevWeapon(int weapon) const;

	//! Find weapon fitting in given slots
	int FindWeaponType(int maskInclude, int maskExclude = 0) const;

	//! Check if entity is placed on road (works for both AI and manual) 
	bool IsOnRoad() const;
	//! Check if entity is placed on road and is moving
	bool IsOnRoadMoving( float minSpeed=3 ) const;

	//! Check if fire state is preparation only (watch target)
	bool IsFirePrepare() const {return _fire._firePrepareOnly;}
	//! Set if fire state is preparation only (watch target)
	void SetFirePrepare(bool prepare) {_fire._firePrepareOnly = prepare;}

#if _ENABLE_AI
	//! Adjust controling values accordingly to planned path of driver
	virtual bool PathPilot
	(
		float &speedWanted, float &headChange, float &turnPredict, float speedCoef = 1.0f
	);
	//! Perform all (including path planning) so that unit is moving toward given position
	virtual void PositionPilot
	(
		float &speedWanted, float &headChange, float &turnPredict,
		Vector3Par pos, Vector3Par dir, float precision
	);
	//! Perform all (including path planning) so that unit is staying in formation
	virtual void FormationPilot
	(
		float &speedWanted, float &headChange, float &turnPredict
	);
	//! Perform all (including path planning) so that unit is moving toward subgroup
	//! target point.
	void LeaderPathPilot
	(
		AIUnit *unit, float &speedWanted, float &headChange, float &turnPredict,
		float speedCoef = 1.0f
	);
	//! Hide if neccessary, otherwise perform LeaderPathPilot
	virtual void LeaderPilot
	(
		float &speedWanted, float &headChange, float &turnPredict
	);
#endif

	//! Stop hiding - unit is not leader now (and was before)
	virtual void SwitchToFormation();
	//! Stop hiding - unit is leader now (and was not before)
	virtual void SwitchToLeader();

	//! Get if unit is far away from formation leader
	virtual bool IsAway( float factor=1 );
	//! Set commander unit away state accordingly IsAway result
	void CheckAway();

	//! Diagnostic only: get speed from planned path.
	virtual float PilotSpeed() const;

	//@Spotability related functions
	//! Check how much are ligths of this entity visible
	virtual float VisibleLights() const;
	//! Check how much is movement of this entity visible
	virtual float VisibleMovement() const;
	//! Check how much is sound of this entity audible
	virtual float Audible() const;
	//! Check how much entity hidden by surroundings
	virtual float GetHidden() const;
	//! Check how much is firing weapons from this entity visible
	float VisibleFire() const;
	//! Check how much is firing weapons from this entity audible
	float AudibleFire() const;
	//! Check what is this entity firing at
	TargetType *FiredAt() const;

	virtual RString HitpointName(int i) const;

	virtual float DirectLocalHit(int component, float val);
	void ChangeHit( int i, float newHit);
	virtual float LocalHit( Vector3Par pos, float val, float valRange );
	virtual void DoDammage
	(
		EntityAI *owner, Vector3Par pos,
		float val, float valRange, RString ammo
	);

	virtual void ShowDammage(int part);

	virtual void HitBy( EntityAI *owner, float howMuch, RString ammo );
	virtual void Destroy( EntityAI *killer, float overkill, float minExp, float maxExp );
	virtual bool IsDammageDestroyed() const;
	virtual void Repair( float ammount=1.0 );
	virtual void SetDammage(float dammage);
	//! Used to notify enitity it has been dammaged
	//!and may need to update its state
	virtual void ReactToDammage();

	virtual float GetExplosives() const; // how much explosives is in

	//! Get dammage state of given hitpoint (0 or 1)
	float GetHit( const HitPoint &hitpoint ) const;
	//! Get dammage state of given hitpoint (continuos) 
	float GetHitCont( const HitPoint &hitpoint ) const; // used for indication

	//! Get dammage state for vehicle state ingame-ui display
	virtual float GetHitForDisplay(int kind) const;
	//! Check if can fire (if not dammaged to much)
	bool CanFire() const;

	//! Check if can move (if not dammaged to much)
	virtual bool IsAbleToMove() const;
	//! Check if can move (if not dammaged to much or out of ammo)
	virtual bool IsAbleToFire() const;

	//! Check if can be moving on the road and lights can be on
	virtual bool IsCautious() const;

	//! Same as IsCautious, but react not only to combat mode,
	//! but also to Danger state
	bool IsCautiousOrDanger() const;

	//! Get entity cost (from config)
	virtual float CalculateTotalCost() const;
	//! Calculate exposure of given field depending on commander combat mode
	float CalculateExposure(int x, int z) const;

	//! Calculate visibility to given unit
	float CalcVisibility( EntityAI *ai, float dist2, float *audibility=NULL, bool assumeLOS=false );

	//! Track near targets
	virtual void TrackNearTargets(TargetList &res);

	//! Track all targets that might be visiblee (used regulary, about 1-5 sec)
	virtual void TrackTargets
	(
		TargetList &res, bool initialize, float trackTargetsPeriod
	);
	//! Low level target tracking
	void TrackTargets
	(
		TargetList &res, AIUnit *unit, int canSee,
		bool initialize, float maxDist, float trackTargetsPeriod
	);
	//! Add targets that might be visible (are in range) to list 
	void AddNewTargets(TargetList &res, bool initialize);
	//! Perform target tracking
	void WhatIsVisible(TargetList &res, bool initialize);
	
	//! Lock fiels in locker map at current position (low-level)
	void PerformLock();
	//! Unlock fiels in locker map that have been locked by PerformLock
	void PerformUnlock();

	//! When vehicle is stopped or moving slow, it calls this function
	//! to maintain corresponding locker map fields locked as necessary
	void LockPosition();
	//! When vehicle moving significanly it calls this function
	//! to stop maintaing locker map fields locked
	void UnlockPosition();

	//! Perform force feedback effects on FF input device
	virtual void PerformFF( FFEffects &effects );
	//! Cancel any outstanding force feedback effects on FF input device
	virtual void ResetFF();

	//! Check it enity has priority over other entity
	bool HasPriorityOver( EntityAI *who ) const;

	//! Avoid collision with other EntityAI moving near
	void AvoidCollision
	(
		float deltaT, float &speedWanted, float &headChange
	);
	//! Guarentee currently planned path is a fresh one (max. 5 sec old allowed)
	void CreateFreshPlan();

	//! Get steering point (point on planned path in near future
	//!vehicle whould be steering to)
	Vector3 SteerPoint( float spdTime, float costTime );

	//! Find position to stop (landing spot)
	virtual void FindStopPosition(){}

	//! Get picture to show in group list UI
	Texture *GetPicture() const {return GetType()->_picture;}
	//! Get icon to draw in mission editor / map
	Texture *GetIcon() const {return GetType()->_icon;}

	//! Get sign correspondign to current side
	Texture *GetSideSign() const;

	// IAnimator interface implementation
	void GetMaterial(TLMaterial &mat, int index) const;


	virtual Vector3 ExternalCameraPosition( CameraType camType ) const;

	virtual void LimitVirtual
	(
		CameraType camType, float &heading, float &dive, float &fov
	) const;
	virtual void InitVirtual
	(
		CameraType camType, float &heading, float &dive, float &fov
	) const;

	virtual void DrawDiags();
	//! Get diagnostics text (some info about entity state - especially path)
	virtual RString DiagText() const;

	//! Get matrix that should be used to draw given proxy
	virtual Matrix4 AnimateProxyMatrix( int level, const ProxyObject &proxy ) const;
	virtual int GetProxyComplexity
	(
		int level, const FrameBase &pos, float dist2
	) const;
	virtual void DrawProxies
	(
		int level, ClipFlags clipFlags,
		const Matrix4 &transform, const Matrix4 &invTransform,
		float dist2, float z2, const LightList &lights
	);

	//! Get texture for main mouse cursor
	virtual Texture *GetCursorTexture(Person *person);
	//! Get texture dot cursor (indicating weapon aim)
	virtual Texture *GetCursorAimTexture(Person *person);
	//! Get mouse cursor color
	virtual PackedColor GetCursorColor(Person *person);

	//! Get model to be drawn as screen overlay
	virtual LODShapeWithShadow *GetOpticsModel(Person *person);
	//! Get color of optics overlay (color multiplication is applied)
	virtual PackedColor GetOpticsColor(Person *person);
	//! Check if optics must be used
	virtual bool GetForceOptics(Person *person) const; 

	//! Get texture that is currently used to draw flag
	//! This function is implemented directly in Flag
	virtual Texture *GetFlagTextureInternal();

	//! Get texture that should be used to draw flag
	virtual Texture *GetFlagTexture();
	//! Change flag texture
	virtual void SetFlagTexture(RString name);

	//! Change texture in hiddenSelections
	void SetObjectTexture(int index, Texture *texture);

	/*!
	\name Flag support
	Interface to various functions of FlagCarrier
	*/
	//@{
	virtual Person *GetFlagOwner();
	virtual void SetFlagOwner(Person *veh);
	virtual TargetSide GetFlagSide() const;
	virtual void SetFlagSide(TargetSide side);
	//@}

	bool AutoReload(int weapon); //!< Start auto-reload of empty slot
	int AutoReloadAll(); //!< Start auto-reload of all empty slots

	//! Simulation of mostly weapon related things
	void SimulateWeaponActivity( float deltaT, SimulationImportance prec );
	//! Simulation
	void Simulate( float deltaT, SimulationImportance prec );

	//! Draw, including squad title if necessary
	void Draw( int level, ClipFlags clipFlags, const FrameBase &pos );

	//! Perform sounds
	void Sound( bool inside, float deltaT );
	//! Unload any sounds currently playing
	void UnloadSound();

	bool IsAnimated( int level ) const;
	bool IsAnimatedShadow( int level ) const;
	void Animate( int level );
	void Deanimate( int level );
	
	//! Fire missile - perform actual releasing
	bool FireMissile
	(
		int weapon,
		Vector3Par offset, Vector3Par direction, Vector3Par initSpeed,
		TargetType *target
	);
	//! Fire shell, throw grenade
	bool FireShell
	(
		int weapon,
		Vector3Par offset, Vector3Par direction,
		TargetType *target
	);
	//! Fire bullet
	bool FireMGun
	(
		int weapon,
		Vector3Par offset, Vector3Par direction,
		TargetType *target
	);
	//! Fire laser designator (toggle designating on/off)
	void FireLaser(int weapon,TargetType *target);

	//! calculate laser targeting impact point
	bool CalculateLaser(Vector3 &pos, Vector3 &dir, int weapon) const;
	//! track laser designator target, create target if necessary
	void TrackLaser(int weapon);
	//! remove laser target
	void StopLaser();

	/*!
	\name Weapon processing
	Function related to adding, removing or finding weapons,
	magazines, muzzles, modes ...
	*/
	//@{
	//! Count guided missiles
	int CountMissiles() const ;
	//! missile shape for proxy drawing
	LODShapeWithShadow *GetMissileShape() const ;
	//! find weapon of given type
	virtual bool FindWeapon(const WeaponType *weapon) const;
	//! check if entity has given magazine
	virtual bool FindMagazine(const Magazine *magazine) const;
	//! find magazine of given (network) id
	virtual const Magazine *FindMagazine(int creator, int id) const;
	//! find magazine of given type
	virtual const Magazine *FindMagazine(RString name) const;
	//! check if given magazine is used (loaded) in some weapon
	bool IsMagazineUsed(const Magazine *magazine) const;
	//! check if weapon can be added and retrieves weapons must be removed
	bool CheckWeapon
	(
		const WeaponType *weapon,
		AutoArray<Ref<const WeaponType>, MemAllocSA> &conflict
	) const;
	//! check if magazine can be added and retrieves magazines must be removed
	bool CheckMagazine
	(
		const Magazine *magazine,
		AutoArray<Ref<const Magazine>, MemAllocSA> &conflict
	) const;
	//! check if magazine fits in some weapon
	bool IsMagazineUsable(const MagazineType *magazine) const;
	//! find best (the most full) magazine of given type
	int FindBestMagazine(const MagazineType *type, int ammo) const;

	//! check all magazines for unique id (for testing purposes)
	bool CheckMagazines();

	//! add new weapon
	int AddWeapon(RStringB name, bool force = false, bool reload = true, bool checkSelected = true);							// force == do not check slots
	//! add new weapon
	int AddWeapon(WeaponType *weapon, bool force = false, bool reload = true, bool checkSelected = true);	// force == do not check slots
	//! remove weapon
	void RemoveWeapon(RStringB name, bool checkSelected = true);
	//! remove weapon
	void RemoveWeapon(const WeaponType *weapon, bool checkSelected = true);
	//! remove all weapons
	void RemoveAllWeapons();

	//! add new magazine
	int AddMagazine(RStringB name, bool force = false);						// force == do not check slots
	//! add new magazine
	int AddMagazine(Magazine *magazine, bool force = false, bool autoload=false);// force == do not check slots

	//! event handler: some weapon added
	virtual void OnWeaponAdded();
	//! event handler: some weapon removed
	virtual void OnWeaponRemoved();
	//! event handler: some weapon changed
	virtual void OnWeaponChanged();

	//! event callback: danger detected
	virtual void OnDanger();

	//! event handler: new recoil started, but old is still playing
	virtual void OnRecoilAbort();

	//! ask vehicle about recoil factor
	virtual float GetRecoilFactor() const;

	virtual void OnAddImpulse(Vector3Par force, Vector3Par torque);

	//! remove magazine
	void RemoveMagazine(RStringB name);
	//! remove magazine
	void RemoveMagazine(const Magazine *magazine);
	//! remove all magazines of given type
	void RemoveMagazines(RStringB name);
	//! remove all magazines
	void RemoveAllMagazines();

	//! adds only minimal equipment for given class
	virtual void MinimalWeapons();

	//! attach magazine to given muzzle (reload low level implemtation)
	void AttachMagazine(const MuzzleType *muzzle, Magazine *magazine);

protected:
	//! translate slot index into <muzzle index, mode of weapon>
	bool FindWeapon(int weapon, int &slot, int &mode) const;

public:
	//! number of weapons
	int NWeaponSystems() const {return _weapons.Size();}
	//! return weapon with index i
	const WeaponType *GetWeaponSystem(int i) const {return _weapons[i];}

	//! number of slots for magazines (each weapon mode of each muzzle of each weapon is single slot)
	int NMagazineSlots() const {return _magazineSlots.Size();}
	//! return slot for magazine with index i
	const MagazineSlot &GetMagazineSlot(int i) const {return _magazineSlots[i];}
	//! number of magazines
	int NMagazines() const {return _magazines.Size();}
	//! return magazine with index i
	const Magazine *GetMagazine(int i) const {return _magazines[i];}
	//! return magazine with index i
	Magazine *GetMagazine(int i) {return _magazines[i];}
	//! return weapon mode for slot i
	const WeaponModeType *GetWeaponMode(int i) const
	{
		if (i < 0 || i >= _magazineSlots.Size()) return NULL;
		const MagazineSlot &slot = _magazineSlots[i];
		const Magazine *magazine = slot._magazine;
		if (!magazine) return NULL;
		if (!magazine->_type) return NULL;
    if (magazine->_type->_modes.Size() == 0) return NULL;
    if (slot._mode < 0 || slot._mode >= magazine->_type->_modes.Size()) return NULL;
		return magazine->_type->_modes[slot._mode];
	}

	//! reload implementation
	bool ReloadMagazineTimed(int s, int m, bool afterAnimation);

	//! find magazine - candidate for automatic reload
	int FindMagazineByType(const MuzzleType *muzzle, const MagazineType *oldMagazineType = NULL);

	//! find and reload magazine
	virtual bool ReloadMagazine(int slotIndex);
	//! reload magazine implementation (hi level implementation)
	virtual bool ReloadMagazine(int slotIndex, int iMagazine);

	//! play sound for magazine reload
	void PlayReloadMagazineSound(int weapon, const MuzzleType *muzzle);
	//! play "dry" sound
	void PlayEmptyMagazineSound(int weapon);
	//@}

	virtual bool IsActionInProgress(MoveFinishF action) const;

	//! Check if weapon manipulation is enabled
	virtual bool EnableWeaponManipulation() const;

	//! Check if entity is able to use optics
	virtual bool EnableViewThroughOptics() const;

	/*!
	\name Config parameters
	Various paramters from config file.
	*/
	//@{
	virtual float GetFormationTime() const; //!<Time to reach pos. in formation
	virtual float GetInvFormationTime() const; //<Inverse GetFormationTime()

	virtual float GetSteerAheadSimul() const {return GetType()->GetSteerAheadSimul();}
	virtual float GetSteerAheadPlan() const {return GetType()->GetSteerAheadPlan();}

	float GetMinFireTime() const {return GetType()->GetMinFireTime();}

	float GetPredictTurnSimul() const {return GetType()->GetPredictTurnSimul();}
	float GetPredictTurnPlan() const {return GetType()->GetPredictTurnPlan();}

	//! Recommended left-right distance in formation
	float GetFormationX() const {return GetType()->GetFormationX();}
	//! Recommended front-back distance in formation
	float GetFormationZ() const {return GetType()->GetFormationZ();}

	//! How is the entity able to be precise when moving
	//! to given target
	virtual float GetPrecision() const {return GetType()->GetPrecision();}
	//@}

	//! Check how much is this vehicle afraid of collision
	virtual float AfraidOfCollision( VehicleKind with ) const;

	//! Check height the center of the vehicle is normaly moving in
	virtual float GetCombatHeight() const;
	//! Check min. allowed combat height (see GetCombatHeight)
	virtual float GetMinCombatHeight() const;
	//! Check max. allowed combat height (see GetCombatHeight)
	virtual float GetMaxCombatHeight() const;
	
	//! Cancel any fire decision in progress
	virtual void ForgetAimTarget();

	//! Calculate direction of the weapon necessary to hit the target
	virtual bool CalculateAimWeapon( int weapon, Vector3 &dir, Target *target ){return false;}
	//! Weapon aiming interface from UI (manual)
	virtual void AimWeaponManDir( int weapon, Vector3Par direction );

	//! Aim weapon to given direction
	virtual bool AimWeapon( int weapon, Vector3Par direction ){return false;}
	//! Aim weapon to given target
	virtual bool AimWeapon( int weapon, Target *target );
	//! Aim weapon - response to fire procedure
	virtual bool AimWeaponForceFire(int weapon);

	//! Calculate direction of the eye necessary to watch target
	virtual bool CalculateAimObserver(Vector3 &dir, Target *target ){return false;}
	//! Aim eye in given direction
	virtual bool AimObserver(Vector3Par direction){return false;}
	//! Aim eye at given target
	virtual bool AimObserver(Target *target);

	//! Set direction driver wants to maintain
	virtual void AimDriver(Vector3Par direction);

	//! Adjust the weapon elevation depending on fov
	virtual void AdjustWeapon
	(
		int weapon, CameraType camType, float fov, Vector3 &camDir
	);

	//! Interface to motion capture animations - current animation name
	virtual RString GetCurrentMove() const;
	//! Interface to motion capture animations - smooth change
	virtual void PlayMove(RStringB move, ActionContextBase *context=NULL);
	//! Interface to motion capture animations - immediate change
	virtual void SwitchMove(RStringB move, ActionContextBase *context=NULL);

	//! Get main direction entity wants to be watching (set using AimObserver)
	virtual Vector3 GetEyeDirectionWanted() const;
	//! Get wanted direction of weapon (set using AimObserver)
	virtual Vector3 GetWeaponDirectionWanted( int weapon ) const;

	//! if necessary, ask server to aim weapon
	void AskForAimWeapon(int weapon, Vector3Val dir);
	//! if necessary, ask server to aim observer
	void AskForAimObserver(Vector3Val dir);

	//! Get main direction entity is watching
	virtual Vector3 GetEyeDirection() const;
	//! Get direction of weapon
	virtual Vector3 GetWeaponDirection( int weapon ) const;
	//! Get center of weapon rotation
	virtual Vector3 GetWeaponCenter( int weapon ) const;

	//! Get weapon position (where the projectiles leave the weapon)
	virtual Vector3 GetWeaponPoint( int weapon ) const;
	//! Get weapon dumped cartridge position
	virtual bool GetWeaponCartridgePos
	(
		int weapon, Matrix4 &pos, Vector3 &vel
	) const; // how should be cartridge disposed

	//! Check if weapon is loaded
	virtual bool GetWeaponLoaded( int weapon ) const;
	//! Check if AI is ready to fire next shot.
	virtual bool GetWeaponReady( int weapon, Target *target ) const;
	//! Check probability weapon will hit target
	virtual float GetAimed( int weapon, Target *target ) const;
	//! Safety check to avoid friendly fire
	virtual bool CheckFriendlyFire( int weapon, Target *target ) const;
	//! Check if target is in weapon fire angle
	virtual float FireInRange( int weapon, float &timeToAim, const Target &target ) const {return 1;}
	//! Check if direction is in weapon fire angle
	virtual float FireAngleInRange( int weapon, Vector3Par rel ) const;

	/*!
	\name Fire result estimation
	*/
	//@{
	bool BestFireResult
	(
		FireResult &result, const Target &target,
		float &bestDist, float &minDist, float &maxDist,
		float timeToShoot, bool enableAttack
	) const;
	bool WhatShootResult
	(
		FireResult &result, const Target &target, int weapon,
		float inRange, float timeToAim, float timeToLive,
		float visibility, float distance, float timeToShoot,
		bool considerIndirect
	) const;
	bool WhatAttackResult
	(
		FireResult &result, const Target &target, float timeToShoot
	) const;
	bool WhatFireResult
	(
		FireResult &result, const Target &target, float timeToShoot
	) const;
	bool WhatFireResult
	(
		FireResult &result, const Target &target, int weapon, float timeToShoot
	) const;
	//@}

	//! SelectFireWeapon variant for gunner
	void SelectFireWeapon(); // common weapon selection
	//! Select target to fire at/watch and weapon to use
	void SelectFireWeapon(FireDecision &fire);

	//! Set request for forced fire (result of scripting)
	void ForceFire(int weapon) {_forceFireWeapon = weapon;}

	/*!
	\name Engage decision
	*/
	//@{
	//! Start thinking about engaging target
	void BegAttack( Target *target );
	//! Stop thinking about engaging target
	void EndAttack();
	//! Perform thinking about engaging target
	//\return false when attack failed
	bool AttackThink( FireResult &result, Vector3 &pos );
	bool AttackReady(); //!< Check if some attack position is ready
	//! Bit mask for return value from EstimateAttack
	enum EstResult {EstImproved=1,EstVisibility=2};

	//! One estimation iteration
	int EstimateAttack( const Vector3 &hPos, float height, const EntityAI *who ) const;
	//! One estimation iteration
	int EstimateAttack( const Vector3 &hPos, float height ) const;
	//@}

	/*!
	\name Hide decision
	*/
	//@{
	Vector3 HideFrom() const; // what position do we hide from
	void FindHideBehind( Vector3 pos, float maxDist );
	void FindHideBehind();
	void BegHide();
	void EndHide();
	void HideThink();
	//@}


	bool GetAIFireEnabled(Target *tgt) const; //!< Check if commander have fire enabled
	void ReportFireReady() const; //!< Report theat we would like to fire
	//! Report theat we have been order to fire but we cannot fire
	void ReportFireNotReady() const;


	virtual void SelectWeaponCommander(AIUnit *unit, int weapon);

	void SelectWeapon(int weapon, bool changed = false);
	int SelectedWeapon() const {return _currentWeapon;}
	Target *GetFireTarget() const {return _fire._fireTarget;}
	int GetFireMode() const {return _fire._fireMode;}

	Entity *GetLastShot() const {return _lastShot;}
	Time GetLastShotAtAssignedTarget() const {return _lastShotAtAssignedTarget;}

	Target *GetHideTarget() const {return _hideTarget;}
	Object *GetHideBehind() const {return _hideBehind;}

	// check if fire line is clear
	bool CheckFireWeapon( int weapon, TargetType *target, Vector3Par weaponPos );
	virtual bool FireWeapon( int weapon, TargetType *target );
	// perform effects after weapon is actually fired
	virtual void FireWeaponEffects
	(
		int weapon, const Magazine *magazine, EntityAI *target
	);
	
	// cost: time necessary for 1m travel
	virtual float GetCost( const GeographyInfo &info ) const {return 1.0;}
	// cost: 16 segments
	virtual float GetCostTurn( int difDir ) const {return 1.0;}
	virtual float GetFieldCost( const GeographyInfo &info ) const {return 1.0;}
	virtual float GetTypeCost(OperItemType type) const;
	
	virtual float GetPathCost( const GeographyInfo &info, float dist ) const;
	virtual void FillPathCost( Path &path ) const;

	virtual void Refuel( float ammount ){}

	virtual float GetFuel() const {return 0;}

	float GetAmmoCost() const; // weapon resources
	float GetAmmoHit() const; // weapon resources

	float GetMaxAmmoCost() const;
	float GetMaxAmmoHit() const;

	float Rearm( float resources ); // transfer resources

	//!\name Implementation of Object interface
	//@{
	virtual float GetArmor() const {return GetType()->_armor;} // armor in mm
	virtual float GetInvArmor() const {return GetType()->_invArmor;} // armor in mm
	virtual float GetLogArmor() const {return GetType()->_logArmor;} // armor in mm
	//@}
	
	//! Easy access to commander properties (see AIUnit::GetGroup)
	AIGroup* GetGroup() const;
	//! Easy access to commander properties (see AIUnit::GetInvAbility)
	float GetInvAbility() const; // returns from 5 (unable) to 1 (maximal)
	//! Easy access to commander properties (see AIUnit::GetAbility)
	float GetAbility() const; // returns from 1 (maximal) to 0.2 (unable)

	__forceinline TargetSide GetVehicleTargetSide() const {return Entity::GetTargetSide();}

	__forceinline const EntityAIType *GetType() const
	{
		return static_cast<const EntityAIType *>(_type.GetRef());
	}
	virtual TargetSide GetTargetSide() const;

	__forceinline RString GetDisplayName() const {return GetType()->GetDisplayName();}
	__forceinline RString GetNameSound() const {return GetType()->GetNameSound();}
	bool IsMoveTarget() const;

	const EntityAIType *GetTypeAtLeast( float accuracy ) const;
	const EntityAIType *GetType( float accuracy ) const;
	TargetSide GetTargetSide( float accuracy ) const;
	
	RString GetDebugName() const;

	// getting in/out of vehicles
	// how long vehicle must be without movement before stopped
	void IsMoved(); // move condition detected
	void StopDetected(); // stop condition detected
	virtual float TimeToStop() const {return 5;}

	void Stop() {_isStopped=true;}
	void CancelStop() {_isStopped=false;}
	bool GetStopped() const {return _isStopped;}

	void UserStop(bool stop) {_userStopped = stop;}
	bool IsUserStopped() const {return _userStopped;}

	virtual bool EngineIsOn() const {return true;}
	virtual void EngineOn() {}
	virtual float MakeAirborne() {return 0;}
	virtual void EngineOff() {}

	virtual void SetFlyingHeight(float val) {}

	virtual void EngineOnAction() {EngineOn();}
	virtual void EngineOffAction() {EngineOff();}

	virtual void AddDefaultWeapons(); // some weapons are always present
	virtual void Init( Matrix4Par pos );
	virtual void InitUnits();

	virtual bool IsPilotLight() const {return _pilotLight;}
	void SetPilotLight(bool on) {_pilotLight = on;}
	
	void SwitchLight(bool on);

	virtual bool LockPossible( const AmmoType *ammo ) const;
	virtual bool CanLock(TargetType *type, int weapon=-1) const;
	
	virtual bool QIsManual() const = NULL;

	virtual float NeedsAmbulance() const; // support need (0..1)
	virtual float NeedsRepair() const;
	virtual float NeedsRefuel() const;
	virtual float NeedsRearm() const;
	virtual float NeedsInfantryRearm() const;

	virtual float NeedsLoadFuel() const; // cargo filling need (0..1) (from static only?)
	virtual float NeedsLoadAmmo() const;
	virtual float NeedsLoadRepair() const;
	
	void SetSensorColID( SensorColID sensorColID ) {_sensorColID=sensorColID;}
	SensorColID GetSensorColID() const {return _sensorColID;}

	virtual AIUnit *ObserverUnit() const {return NULL;}
	virtual AIUnit *CommanderUnit() const {return NULL;}
	virtual AIUnit *PilotUnit() const {return NULL;}
	virtual AIUnit *GunnerUnit() const {return NULL;}
	virtual AIUnit *EffectiveGunnerUnit() const {return GunnerUnit();}

	virtual LSError Serialize(ParamArchive &ar);

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	static void CreateFormatWeapons(NetworkMessageFormat &format);
	TMError TransferMsg(NetworkMessageContext &ctx);
	TMError TransferMsgWeapons(NetworkMessageContext &ctx, IndicesUpdateEntityAIWeapons *indices);
	float CalculateError(NetworkMessageContext &ctx);

	virtual void ResetStatus();

	USE_CASTING(base)
};

typedef EntityAI VehicleWithAI;

LSError Serialize(ParamArchive &ar, RString name, EntityAI::FireState &value, int minVersion);

typedef OLink<EntityAI> LinkVehicleWithAI;
typedef OLink<EntityAI> LinkEntityAI;

template Ref<EntityAI>;

typedef float (EntityAI::*SupportCheckF)() const;

class IndicesResourceSupply
{
public:
	int fuelCargo;
	int repairCargo;
	int ammoCargo;
	//int infantryAmmoCargo;
	int supplying;
	int alloc;
	int action;
	int actionParam;
	int actionParam2;
	int actionParam3;

	IndicesResourceSupply();
	void Scan(NetworkMessageFormatBase *format);
};

class ResourceSupply: public RefCount
{
	private:
	OLink<EntityAI> _parent;

	float _fuelCargo,_repairCargo,_ammoCargo;
	RefArray<WeaponType> _weaponCargo;
	RefArray<Magazine> _magazineCargo;
	OLink<EntityAI> _supplying;
	OLink<EntityAI> _alloc;
	UIActionType _action;
	int _actionParam;
	int _actionParam2;
	RString _actionParam3;
//	Time _lastSupportTime;
	
	public:
	ResourceSupply( EntityAI *vehicle );

//	EntityAI *Scan( SupportCheckF check, float limit=0.05 ) const;
	bool Check(EntityAI *vehicle, SupportCheckF check, float limit, bool now) const;

	void Simulate( float deltaT, SimulationImportance prec );

	void SetAlloc(EntityAI *vehicle) {_alloc=vehicle;}
	EntityAI *GetAlloc() const {return _alloc;}

	bool Supply(EntityAI *vehicle, UIActionType action, int param, int param2, RString param3);
	EntityAI *GetSupplying() const {return _supplying;}

	float GetFuelCargo() const {return _fuelCargo;}
	float GetRepairCargo() const {return _repairCargo;}
	float GetAmmoCargo() const {return _ammoCargo;}
	int GetWeaponCargoSize() const {return _weaponCargo.Size();}
	const WeaponType *GetWeaponCargo(int weapon) const {return _weaponCargo[weapon];}
	int GetMagazineCargoSize() const {return _magazineCargo.Size();}
	const Magazine *GetMagazineCargo(int magazine) const {return _magazineCargo[magazine];}

	int GetFreeWeaponCargo() const {return _parent->GetType()->_maxWeaponsCargo - _weaponCargo.Size();}
	int GetFreeMagazineCargo() const {return _parent->GetType()->_maxMagazinesCargo - _magazineCargo.Size();}

	void LoadFuelCargo( float cargo ) {_fuelCargo+=cargo;}
	void LoadRepairCargo( float cargo ) {_repairCargo+=cargo;}
	void LoadAmmoCargo( float cargo ) {_ammoCargo+=cargo;}
	void ClearWeaponCargo();
	int AddWeaponCargo(WeaponType *weapon, int count, bool deleteWhenFull=false);
	bool RemoveWeaponCargo(WeaponType *weapon);
	void ClearMagazineCargo();
	int AddMagazineCargo(Magazine *magazine, bool deleteWhenFull=false);
	int AddMagazineCargo(MagazineType *type, int count, bool deleteWhenFull=false);
	bool RemoveMagazineCargo(Magazine *magazine);

	void GetActions(UIActions &actions, AIUnit *unit, bool now);

	bool FindWeapon(const WeaponType *weapon) const;
	bool FindMagazine(const Magazine *magazine) const;

	const Magazine *FindMagazine(int creator, int id) const;
	const Magazine *FindMagazine(RString name) const;

	LSError Serialize(ParamArchive &ar);
	static ResourceSupply *CreateObject(ParamArchive &ar) {return new ResourceSupply();}

	static void CreateFormat(NetworkMessageFormat &format);
	TMError TransferMsg(NetworkMessageContext &ctx, const IndicesResourceSupply *indices);
	float CalculateError(NetworkMessageContext &ctx, const IndicesResourceSupply *indices);
	
	void SetParent(EntityAI *vehicle) {_parent = vehicle;}
private:
	ResourceSupply(); // used for serialization only

};

class IndicesUpdateVehicleSupply : public IndicesUpdateVehicleAI
{
	typedef IndicesUpdateVehicleAI base;

public:
	IndicesResourceSupply supply;

	IndicesUpdateVehicleSupply();
	NetworkMessageIndices *Clone() const {return new IndicesUpdateVehicleSupply;}
	void Scan(NetworkMessageFormatBase *format);
};

class VehicleSupply: public EntityAI
{
	typedef EntityAI base;

	protected:
	Ref<ResourceSupply> _supply;

	mutable OLinkArray<AIUnit> _supplyUnits;

	public:
	VehicleSupply(EntityAIType *name, bool fullCreate=true);
	
	void SupplyStarted( AIUnit *unit );
	void SupplyFinished( AIUnit *unit );
	void WaitForSupply(AIUnit *unit);

	const OLinkArray<AIUnit> &GetSupplyUnits() const {return _supplyUnits;}

	void UpdateStop(); // something has been changed
	virtual bool CanCancelStop() const;

	void Simulate( float deltaT, SimulationImportance prec );

	virtual LSError Serialize(ParamArchive &ar);

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx);

	void SetAllocSupply( EntityAI *vehicle );
	EntityAI *GetAllocSupply() const;

	virtual bool Supply(EntityAI *vehicle, UIActionType action, int param, int param2, RString param3);
	EntityAI *GetSupplying() const;

	float GetFuelCargo() const {return _supply ? _supply->GetFuelCargo() : 0;}
	float GetRepairCargo() const {return _supply ? _supply->GetRepairCargo() : 0;}
	float GetAmmoCargo() const {return _supply ? _supply->GetAmmoCargo() : 0;}

	int GetFreeMagazineCargo() const {return _supply ? _supply->GetFreeMagazineCargo() : 0;}
	int GetFreeWeaponCargo() const {return _supply ? _supply->GetFreeWeaponCargo() : 0;}

	virtual float GetExplosives() const; // how much explosives is in

	int GetWeaponCargoSize() const {return _supply ? _supply->GetWeaponCargoSize() : 0;}
	const WeaponType *GetWeaponCargo(int weapon) const {return _supply ? _supply->GetWeaponCargo(weapon) : NULL;}
	int GetMagazineCargoSize() const {return _supply ? _supply->GetMagazineCargoSize() : 0;}
	const Magazine *GetMagazineCargo(int magazine) const {return _supply ? _supply->GetMagazineCargo(magazine) : NULL;}

	void LoadFuelCargo( float cargo ) {if( _supply ) _supply->LoadFuelCargo(cargo);}
	void LoadRepairCargo( float cargo ) {if( _supply ) _supply->LoadRepairCargo(cargo);}
	void LoadAmmoCargo( float cargo ) {if( _supply ) _supply->LoadAmmoCargo(cargo);}

	void ClearWeaponCargo() {if (_supply) _supply->ClearWeaponCargo();}
	int AddWeaponCargo(WeaponType *weapon, int count = 1, bool deleteWhenFull=false)
	{
		return _supply ? _supply->AddWeaponCargo(weapon, count,deleteWhenFull) : -1;
	}
	bool RemoveWeaponCargo(WeaponType *weapon) {return _supply ? _supply->RemoveWeaponCargo(weapon) : false;}
	void ClearMagazineCargo() {if (_supply) _supply->ClearMagazineCargo();}
	int AddMagazineCargo(Magazine *magazine, bool deleteWhenFull=false)
	{
		return _supply ? _supply->AddMagazineCargo(magazine,deleteWhenFull) : -1;
	}
	int AddMagazineCargo(MagazineType *type, int count, bool deleteWhenFull=false)
	{
		return _supply ? _supply->AddMagazineCargo(type, count,deleteWhenFull) : -1;
	}
	bool RemoveMagazineCargo(Magazine *magazine) {return _supply ? _supply->RemoveMagazineCargo(magazine) : false;}

	virtual RString GetActionName(const UIAction &action);
	virtual void PerformAction(const UIAction &action, AIUnit *unit);

	virtual void GetActions(UIActions &actions, AIUnit *unit, bool now);

	bool FindWeapon(const WeaponType *weapon) const;
	bool FindMagazine(const Magazine *magazine) const;

	const Magazine *FindMagazine(int creator, int id) const;
	const Magazine *FindMagazine(RString name) const;

	virtual void ResetStatus();

	USE_CASTING(base)
};

typedef VehicleSupply EntitySupply;

DECL_ENUM(UnitPosition)

enum Rank
{
	RankUndefined = -1,
	RankPrivate,
	RankCorporal,
	RankSergeant,
	RankLieutnant,
	RankCaptain,
	RankMajor,
	RankColonel,
	NRanks
};

struct AIUnitInfo : public SerializeClass
{
	RString _identityContext;
	RString _name;
	RString _face;
	RString _glasses;
	RString _speaker;
	float _pitch;
	float _experience;
	float _initExperience;
	Rank _rank;
	Ref<Texture> _squadPicture;
	RString _squadTitle;

	LSError Serialize(ParamArchive &ar);
};

/*
struct ActionContextUI: public ActionContextBase, public UIAction
{
};
*/

class IndicesUpdateVehicleBrain : public IndicesUpdateVehicleSupply
{
	typedef IndicesUpdateVehicleSupply base;

public:
	int remotePlayer;

	IndicesUpdateVehicleBrain();
	NetworkMessageIndices *Clone() const {return new IndicesUpdateVehicleBrain;}
	void Scan(NetworkMessageFormatBase *format);
};

#include <Es/Memory/normalNew.hpp>

//!Target status (position, spotability etc.)
struct Target: public RemoveLLinks
{
	Vector3 position;
	Vector3 posError; // audible results are very inaccurate
	Vector3 speed;
	Vector3 posReported; // which position was reported

	TargetSide side; // side info merged from side and type
	bool sideChecked; // side info obtained from observation
	const EntityAIType *type;

	float spotability; // fading
	Time spotabilityTime;
	float accuracy; // fading
	Time accuracyTime;
	float sideAccuracy; // fading
	Time sideAccuracyTime;
	Time lastSeen;

	Time delay; // delay for all
	Time delaySensor; // delay for sensor (if bigger that delay -> invalid)

	Time timeReported;

	bool isKnown; // do we remmember it (potential)
	bool vanished; // target vanished - probably GetIn
	bool destroyed; // target is dead
	
	TargetId idExact;
	OLink<Person> idSensor; // who sees the target best
	OLink<EntityAI> idKiller; // who killed this target
	OLink<AIGroup> group;
	
	float dammagePerMinute;
	float subjectiveCost;

	// functions
	
	//float FadingVisibility() const;
	float FadingSideAccuracy() const;
	float FadingAccuracy() const;
	float FadingSpotability() const;

	bool IsKnownBy( AIUnit *unit ) const;
	bool IsKnownByAll() const; // known by all group member
	bool IsKnownBySome() const; // known by all group member

	// TODO: remove IsKnown member - compatibilty only
	bool IsKnown() const;

	void LimitError( float error );

	LSError Serialize(ParamArchive &ar);
	static Target *CreateObject(ParamArchive &ar) {return new Target(NULL);}
	static Target *LoadRef(ParamArchive &ar);
	LSError SaveRef(ParamArchive &ar);
	
	void Init();
	Target( AIGroup *group );
	
	float VisibleSize() const;
	Vector3 AimingPosition() const;
	Vector3 ExactAimingPosition() const;
	Vector3 LandAimingPosition() const;

	TargetState State(AIUnit *sensor) const;

	USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

inline TargetType *LinkTarget::IdExact() const
{
	Target *link=GetLink();
	if( link ) return link->idExact;
	return NULL;
}

class TargetList: public RefArray<Target>
{
	public:
	void Manage( AIGroup *group );
	void Manage(); // player visible list managenemt
	int Find( TargetType *target ) const;
};

// TODO: move to better place
#include "person.hpp"

#endif
