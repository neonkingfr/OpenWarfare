#ifdef _MSC_VER
#pragma once
#endif

#ifndef _SHOTS_HPP
#define _SHOTS_HPP

#include "vehicle.hpp"
#include "lights.hpp"

#include "weapons.hpp"
#include "smokes.hpp"

#include <Es/Memory/normalNew.hpp>


class Shot: public Vehicle
{
	typedef Vehicle base;

	protected:
	OLink<EntityAI> _parent;
	Ref<AbstractWave> _sound;
	float _timeToLive;

	public:
	Shot( EntityAI *parent, const AmmoType *type );
	void SetParent( EntityAI *parent );

	const AmmoType *Type() const
	{
		return static_cast<const AmmoType *>(GetNonAIType());
	}

	bool OcclusionFire() const {return false;}
	bool OcclusionView() const {return false;}

	#if _RELEASE
		bool Invisible() const;
	#endif
	float VisibleSize() const {return 0;}
	void Sound( bool inside, float deltaT );
	void UnloadSound();

	float Rigid() const {return 0;}

	EntityAI *GetOwner() const {return _parent;}
	
	LSError Serialize(ParamArchive &ar);

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	static Shot *CreateObject(NetworkMessageContext &ctx);
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx);
	float GetMaxPredictionTime(NetworkMessageContext &ctx) const;

	USE_CASTING(base)
	USE_FAST_ALLOCATOR
};

class TimeBomb : public Shot
{
	typedef Shot base;

public:
	TimeBomb( EntityAI *parent, const AmmoType *type );
	void Simulate( float deltaT, SimulationImportance prec );

	USE_CASTING(base)
	USE_FAST_ALLOCATOR
};

class PipeBomb : public Shot
{
	typedef Shot base;

protected:
	bool _explosion;

public:
	PipeBomb( EntityAI *parent, const AmmoType *type );
	void Simulate( float deltaT, SimulationImportance prec );

	void Explode() {_explosion = true;}
	void SetTimer(float ttl = 20) {_timeToLive = ttl;}
	float GetTimer() const {return _timeToLive;}

	USE_CASTING(base)
	USE_FAST_ALLOCATOR
};

class Mine : public Shot
{
	typedef Shot base;

protected:
	bool _active;

public:
	Mine( EntityAI *parent, const AmmoType *type );
	void Simulate( float deltaT, SimulationImportance prec );

	bool IsActive() const {return _active;}
	void SetActive(bool active) {_active = active;}

	LSError Serialize(ParamArchive &ar);

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx);
	
	USE_CASTING(base)
	USE_FAST_ALLOCATOR
};

class ShotShell: public Shot // tank shell
{
	typedef Shot base;

protected:
	float _airFriction;
	float _initDelay;
	
	float _coefGravity;

public:
	ShotShell( EntityAI *parent, const AmmoType *type );
	void Simulate( float deltaT, SimulationImportance prec );
	bool Invisible() const;
	void Sound( bool inside, float deltaT );

	USE_CASTING(base)
	USE_FAST_ALLOCATOR
};


class ShotBullet: public ShotShell
{
	typedef ShotShell base;

	Vector3 _beg,_end; // world space line

	public:
	ShotBullet( EntityAI *parent, const AmmoType *type );

	float GetMass() const {return 0.05;}
	float GetInvMass() const {return 1.0/0.05;}

	void Simulate( float deltaT, SimulationImportance prec );
	void Sound( bool inside, float deltaT );
	void UnloadSound(){}

	void StartFrame(); // start frame - used for motion blur
	bool Invisible() const {return false;}

	int PassNum( int lod );

	void DoDraw( int level, ClipFlags clipFlags, const FrameBase &pos );
	void Draw( int level, ClipFlags clipFlags, const FrameBase &pos );
	#if ALPHA_SPLIT
	void DrawAlpha( int level, ClipFlags clipFlags, const FrameBase &pos );
	#endif

	void SetBeg( Vector3Val beg );
	void SetEnd( Vector3Val end );

	bool IsAnimated( int level ) const;
	bool IsAnimatedShadow( int level ) const;
	void Animate( int level );
	void Deanimate( int level );

	void AnimatedMinMax( int level, Vector3 *minMax );
	void AnimatedBSphere( int level, Vector3 &bCenter, float &bRadius, bool isAnimated );
	// TODO: ?? LSError Serialize(ParamArchive &ar);

	USE_CASTING(base)
	USE_FAST_ALLOCATOR
};

class Missile: public Shot // guided ATM or AAM
{
	typedef Shot base;

public:
	enum EngineState {Init,Thrust,Fly};
	enum LockState {Locked,Lost};

private:
	Ref<AbstractWave> _soundEngine;

	CloudletSource _cloudlets;

	float _initTime,_thrustTime;

	Color _lightColor;
	Ref<LightPointOnVehicle> _light;

	float _thrust;
	EngineState _engine;
	LockState _lock;
	OLink<Object> _target;

	Vector3 _controlDirection; // manual missile control
	bool _controlDirectionSet; // manual control activated
	
	public:
	Missile
	(
		EntityAI *parent, const AmmoType *type, Object *target
	);
	~Missile();
	void Sound( bool inside, float deltaT );
	void UnloadSound();

	//void SetSmoke( LODShapeWithShadow *shape, float interval, float duration );
	void SetLight( ColorVal color );

	virtual void Animate( int level );
	virtual void Deanimate( int level );
	virtual void Simulate( float deltaT, SimulationImportance prec );
	// control direction - used for manual missile control
	void SetControlDirection( Vector3 dir );

	LSError Serialize(ParamArchive &ar);

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	float CalculateError(NetworkMessageContext &ctx);
	float GetMaxPredictionTime(NetworkMessageContext &ctx) const;

	USE_CASTING(base)
	USE_FAST_ALLOCATOR
};

class IlluminatingShell: public ShotShell
{
	typedef ShotShell base;

protected:
	Color _lightColor;
	Ref<LightPointOnVehicle> _light;

public:
	IlluminatingShell( EntityAI *parent, const AmmoType *type );
	void Simulate( float deltaT, SimulationImportance prec );

	void SetLight( ColorVal color );

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;

	LSError Serialize(ParamArchive &ar);

	USE_CASTING(base)
	USE_FAST_ALLOCATOR
};

class SmokeShell: public ShotShell
{
	typedef ShotShell base;

protected:
	SmokeSource _smoke;

public:
	SmokeShell( EntityAI *parent, const AmmoType *type );
	void Simulate( float deltaT, SimulationImportance prec );

	LSError Serialize(ParamArchive &ar);

	USE_CASTING(base)
	USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

Shot *NewShot( EntityAI *parent, const AmmoType *type, Object *target );


#endif
