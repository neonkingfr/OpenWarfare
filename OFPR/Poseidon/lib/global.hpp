#ifdef _MSC_VER
#pragma once
#endif

#ifndef _GLOBAL_HPP
#define _GLOBAL_HPP

#include <Es/Framework/debugLog.hpp>
#include <Es/Strings/rString.hpp>

#include "time.hpp"

// global variables
// all simulated items have access to them and can use them freely

enum DifficultyType
{
	DTArmor,
	DTFriendlyTag,
	DTEnemyTag,
	DTHUD,
	DTAutoSpot,
	DTMap,
	DTWeaponCursor,
	DTAutoGuideAT,
	DTClockIndicator,
	DT3rdPersonView,
	DTTracers,
	DTUltraAI,
	DTN	// terminator
};

struct DifficultyDesc
{
	const char *name;
	int &desc;	// IDS of string

	bool defaultCadet;
	bool defaultVeteran;

	bool enabledInVeteran;

	DifficultyDesc(const char *n, int &d, bool cadet, bool veteran, bool enabled)
	: desc(d)
	{
		name = n;
		defaultCadet = cadet; defaultVeteran = veteran; enabledInVeteran = enabled;
	}
};

struct Config
{
	int heapSize;
	int fileHeapSize;
	float horizontZ,objectsZ; // max. distance for drawing landscape and objects
	float tacticalZ,radarZ; // visibility limits 
	float shadowsZ; // max. distance for drawing shadows	
	int maxObjects; //maxShadows; //,maxReflections;
	float trackTimeToLive;
	float invTrackTimeToLive;
	float benchmark;

	int maxLights;

	float lodCoef; // LOD bias for object geometry
	float objectLODLimit;
	float shadowLODLimit;
	//float reflectLODLimit;

	int maxCockText,maxLandText,maxObjText,maxAnimText,autoDropText;
	
	int lights;

	int maxSounds;

	//bool reflections;

	//bool background;

	//bool inGameMusic;
	//bool softOnly; // D3D: use system memory z-buffer (will use MMX if possible)

	//bool threadTextures;
	bool showTitles;
	bool singleVoice;

	bool easyMode;
#if _ENABLE_CHEATS
	bool super;			//	player is undestructible
#endif

	//! show blood
	bool blood;

	int wantBpp,wantW,wantH; // preferred screen parameters

	static DifficultyDesc diffDesc[DTN];
	bool cadetDifficulty[DTN];
	bool veteranDifficulty[DTN];

	bool IsEnabled(DifficultyType type);
	void InitDifficulties();
	void LoadDifficulties();
	void SaveDifficulties();
};

#ifndef __GNUC__
enum TargetSide;
#endif
struct GameHeader
{
	// game info	
	char filename[80];
	char worldname[80];
	RString filenameReal;

	// player info
	RString playerName;
	RString playerFace;
	RString playerGlasses;
	RString playerSpeaker;
	float playerPitch;
#ifdef __GNUC__
	int playerSide;
#else
	TargetSide playerSide;
#endif
};

const float OneDay=1.0f/365;
const float OneHour=OneDay/24;
const float OneMinute=OneHour/60;
const float OneSecond=OneMinute/60;

class Clock : public SerializeClass
{
protected:
	// use single float representation
	// 1.0 is one year
	float _timeInYear;
	float _changeTime; // actual time relative to _time - _time precision to small
	float _timeOfDay;	
	int _year;
	//float _dayOfTheYear; // move earth on orbit

public:
	Clock() : _timeInYear(0), _changeTime(0), _year(1985)
	{
		SetTimeInYear(OneHour*10+OneDay*120);
	}

	float GetTimeInYear() const {return _timeInYear;}
	float GetTimeOfDay() const {return _timeOfDay;}
	int GetYear() const {return _year;}

	void SetTimeInYear( float time )
	{
		_timeInYear = time;
		_changeTime = 0;
		_timeOfDay=fastFmod(_timeInYear,OneDay)*(1.0f/OneDay);
	}
	void SetYear(int year)
	{
		_year = year;
	}

	bool AdvanceTime( float deltaT );

	void FormatDate(const char *format, char *buffer);
	static float ScanDateTime(const char *date, const char *time, int &year);

	#ifndef ACCESS_ONLY
	LSError Serialize(ParamArchive &ar);
	#else
	LSError Serialize(ParamArchive &ar) {return LSOK;}
	#endif
};

//#include "randomGen.hpp"
//#include "fileServer.hpp"

DWORD GlobalTickCount();
//bool IsOutOfMemory();

struct Globals
{
	Time time;
	UITime uiTime;
	Clock clock;

	float NetTimeOffset;
	
	Time NetTime() const {return RealToNetTime(time);}
	Time GetTime() const {return time;}

	Time RealToNetTime( Time time ) const {return time+NetTimeOffset;}
	Time NetToRealTime( Time time ) const {return time-NetTimeOffset;}

	
	float dropDown; // moving fast - low down scene quality (from 0.0 to 1.0)
	float fullDropDown;
	float fullDropDownChange;

	float drawTreshold,shadowTreshold,reflectTreshold;

	Config config;
	GameHeader header;
	
	void Init();

	void Clear();

	// pause menu commands
	bool newGame;
	bool exit;
	bool demo;
};

#include <Es/Framework/appFrame.hpp>

//! get most critical error level since last ResetErrors
ErrorMessageLevel GetMaxError();

//! reset error state - used in conjunction with GetMaxError()
void ResetErrors();

//! get most critical error message since last ResetErrors
RString GetMaxErrorMessage();

#define LIGHT_EXPLO 1
#define LIGHT_MISSILE 2
#define LIGHT_STATIC 4

extern Globals Glob;

#endif

