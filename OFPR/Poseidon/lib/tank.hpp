#ifdef _MSC_VER
#pragma once
#endif

#ifndef _TANK_HPP
#define _TANK_HPP

#include "tankOrCar.hpp"
//#include "ai.hpp"
#include "shots.hpp"

#include <El/ParamFile/paramFile.hpp>

// every tank mainains a list of tracksteps

class TankType: public TankOrCarType
{
	typedef TankOrCarType base;
	friend class Tank;
	friend class TankWithAI;

	protected:
	Point3 _leftLightPos,_rightLightPos;
	Vector3 _leftLightDir,_rightLightDir;
	
	Point3 _gunnerPilotPos; // camera position - gunner view

	Point3 _missilePos,_missileDir; // missile position and direction
	//Point3 _shellPos,_shellDir; // shell position and direction
	Point3 _gunPos,_gunDir; // to do: gun position and direction

	Vector3 _cargoLightPos;	// light in cargo

	TurretType _mainTurret;
	TurretType _comTurret;

	Indicator _radarIndicator;
	Indicator _turretIndicator;
	IndicatorWatch _watch;
	

	// some tanks have observer turret mounted on the main turret
	// while some not
	bool _comTurretOnMainTurret;

	AnimationUV _leftOffset,_rightOffset;
	//Ref<Texture> _trackStill,_trackMove;
	//Animation _hatch, _hatch2;
	Hatch _hatchDriver, _hatchCommander, _hatchGunner;

	AnimationAnimatedTexture _animFire;

	// wheels movement animation
	// some wheels move up/down
	// all rotate around the X-axis
	AutoArray<AnimationWithCenter> _wheelsRotL;
	AutoArray<AnimationWithCenter> _wheelsRotR;
	AutoArray<AnimationWithCenter> _wheelsUpDownL;
	AutoArray<AnimationWithCenter> _wheelsUpDownR;
	AutoArray<AnimationWithCenter> _tracksUpDownL;
	AutoArray<AnimationWithCenter> _tracksUpDownR;

	HitPoint _hullHit;
	HitPoint _trackLHit,_trackRHit;
	HitPoint _turretHit,_gunHit;

	public:
	
	TankType( const ParamEntry *param );
	virtual void Load(const ParamEntry &par);
	void InitShape();
};


/*!
\patch 1.17 Date 08/14/2001 by Jirka
- Fixed: fire weapon effects in multiplayer
*/
class Tank: public TankOrCar
{
	typedef TankOrCar base;
	// basic catterpillar vehicle (tank/APC) simulation
	protected:
	
	float _randFrequency;

	//! forward ey is used for braking
	bool _forwardUsedAsBrake:1;
	//! backward key is used for braking
	bool _backwardUsedAsBrake:1;
	//! braking when vehicle is almost stopped
	bool _parkingBrake:1;

	bool _doGearSound:1;

	WaterSource _leftWater,_rightWater;
	DustSource _fireDust;
	float _invFireDustTimeTotal;
	float _fireDustTimeLeft;
		
	WeaponFireSource _gunFire;
	WeaponCloudsSource _gunClouds;

	WeaponLightSource _mGunFire;
	int _mGunFireFrames;
	UITime _mGunFireTime;
	int _mGunFirePhase;
//	WeaponFireSource _mGunFire;
	WeaponCloudsSource _mGunClouds;

	float _phaseL,_phaseR; // texture animation
	
	TrackOptimized _track;

	Turret _mainTurret;
	Turret _comTurret;

	Ref<LightPointOnVehicle> _cargoLight;

	// thrust is relative, in range (-1.0,1.0)
	// actually it is engine RPM?
	float _thrustLWanted,_thrustRWanted; // accelerate (left), accelerate (right)
	float _thrustL,_thrustR; // accelerate (left), accelerate (right)

	public:
	Tank( VehicleType *name, Person *driver );

	const TankType *Type() const
	{
		return static_cast<const TankType *>(GetType());
	}

	void ObjectContact
	(
		Frame &moveTrans,
		Vector3Par wCenter,
		float deltaT,
		Vector3 &torque, Vector3 &friction, Vector3 &torqueFriction,
		Vector3 &totForce, float &crash

	);
	void LandFriction
	(
		Vector3 &friction, Vector3 &torqueFriction,
		Vector3 &torque,
		bool brakeFriction, Vector3Par fSpeed, Vector3Par speed,
		Vector3Par pCenter,
		float coefNPoints, Texture *texture
	);

	
	Vector3 Friction( Vector3Par speed );

	void MoveWeapons(float deltaT);

	bool IsAbleToMove() const;
	bool IsAbleToFire() const;
	bool IsPossibleToGetIn() const;

	void StabilizeTurrets
	(
		Matrix3Val oldTrans, Matrix3Val newTrans,
		Matrix3Val oldTurretTrans
	);


	bool UseSimpleSimulation(SimulationImportance prec) const;

	void SimulateOptimized( float deltaT, SimulationImportance prec );
	void PlaceOnSurface(Matrix4 &trans);
	void Simulate( float deltaT, SimulationImportance prec );

	float GetEngineVol( float &freq ) const;
	float GetEnvironVol( float &freq ) const;

	void PerformFF( FFEffects &effects );

	float GetHitForDisplay(int kind) const;

/*
	void GetNetState( UNIT_STATE &state );
	void SetNetState( const UNIT_STATE &state );
*/

	void Sound( bool inside, float deltaT );
	void UnloadSound();

	bool IsTurret( CameraType camType ) const;
	bool HasFlares( CameraType camType ) const;

	void InitVirtual
	(
		CameraType camType, float &heading, float &dive, float &fov
	) const;
	void LimitVirtual
	(
		CameraType camType, float &heading, float &dive, float &fov
	) const;
	void LimitCursorHard(CameraType camType, Vector3 &dir) const;

	float OutsideCameraDistance( CameraType camType ) const {return 25;}
	Matrix4 TurretTransform() const;
	Matrix4 GunTurretTransform() const;
	Matrix4 ObsTransform() const;
	Matrix4 ObsGunTurretTransform() const;

	bool AnimateTexture
	(
		int level,
		float phaseL, float phaseR, float speedL, float speedR
	);
	//bool AnimateTexture( int level, bool shadow );
	//bool DeanimateTexture( int level, bool shadow );
	bool IsAnimated( int level ) const; // appearence changed with Animate
	bool IsAnimatedShadow( int level ) const; // shadow changed with Animate
	void Animate( int level );
	void Deanimate( int level );

	Vector3 AnimatePoint( int level, int index ) const;
	void AnimateMatrix(Matrix4 &mat, int level, int selection) const;

	RString DiagText() const;

	void Draw( int level, ClipFlags clipFlags, const FrameBase &pos );
	#if ALPHA_SPLIT
	void DrawAlpha( int level, ClipFlags clipFlags, const FrameBase &pos );
	#endif

	float Thrust() const {return (fabs(_thrustL)+fabs(_thrustR))*0.5;}
	float ThrustWanted() const {return (fabs(_thrustLWanted)+fabs(_thrustRWanted))*0.5;}

	float TrackingSpeed() const {return 80;}

	// all tank weapons are linked
	// special case is guided missile, which is linked, but have weapon lock
	bool CalculateAimWeapon( int weapon, Vector3 &dir, Target *target );
	bool AimWeapon(int weapon, Vector3Par direction);
	bool AimWeapon(int weapon, Target *target);
	bool CalculateAimObserver(Vector3 &dir, Target *target);
	bool AimObserver(Vector3Val dir);

	float GetAimed( int weapon, Target *target ) const;
	Vector3 GetWeaponDirection( int weapon ) const;
	Vector3 GetWeaponCenter( int weapon ) const;

	Vector3 GetEyeDirection() const;

	Vector3 GetWeaponDirectionWanted( int weapon ) const;
	Vector3 GetEyeDirectionWanted() const;

	bool FireWeapon( int weapon, TargetType *target );
	void FireWeaponEffects(int weapon, const Magazine *magazine,EntityAI *target);

	void Eject(AIUnit *unit);
	void JoystickPilot( float deltaT );
	void SuspendedPilot(AIUnit *unit, float deltaT );
	void KeyboardPilot(AIUnit *unit, float deltaT );
	void FakePilot( float deltaT );
#if _ENABLE_AI
	void AIPilot(AIUnit *unit, float deltaT ){}
#endif

	bool HasHUD() const {return true;}
};

class TankWithAI: public Tank
{
	typedef Tank base;

	public:
	TankWithAI( VehicleType *name, Person *driver );
	~TankWithAI();

	float GetFieldCost( const GeographyInfo &info ) const;
	float GetCost( const GeographyInfo &info ) const;
	float GetCostTurn( int difDir ) const;
	float GetTypeCost(OperItemType type) const;

	float FireAngleInRange( int weapon, Vector3Par rel ) const;
	float FireInRange( int weapon, float &timeToAim, const Target &target ) const;

	void Simulate( float deltaT, SimulationImportance prec );
#if _ENABLE_AI
	void AIPilot(AIUnit *unit, float deltaT );
	void AIGunner(AIUnit *unit, float deltaT );
#endif

	virtual LSError Serialize(ParamArchive &ar);

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx);
};

#endif

