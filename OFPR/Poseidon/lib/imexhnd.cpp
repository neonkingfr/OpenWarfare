//-----------------------------------------------------------------------------
//
// FILE: IMEXHND.CPP
// 
// Matt Pietrek
// Microsoft Systems Journal, April 1997
//
// (c) Interactive Magic (1997)
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------
// Version tracking information
//-----------------------------------------------------------------------

#include "wpch.hpp"

#include <tchar.h>
#include <time.h>
#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "imexhnd.h"
#include "debugTrap.hpp"
#include "crc.hpp"

#include "mapFile.hpp"
#include "versionNo.h"

#if _ENABLE_REPORT
#define DEBUGGER_DETECTION 1
#else
#define DEBUGGER_DETECTION 1
#endif


//============================== Global Variables =============================

//
// Declare the static variables of the DebugExceptionTrap class
//

#if USE_IMAGE

DebugExceptionTrap::SYMINITIALIZEPROC DebugExceptionTrap::_SymInitialize = 0;
DebugExceptionTrap::SYMCLEANUPPROC DebugExceptionTrap::_SymCleanup = 0;
DebugExceptionTrap::STACKWALKPROC DebugExceptionTrap::_StackWalk = 0;

DebugExceptionTrap::SYMFUNCTIONTABLEACCESSPROC
                            DebugExceptionTrap::_SymFunctionTableAccess = 0;

DebugExceptionTrap::SYMGETMODULEBASEPROC
                            DebugExceptionTrap::_SymGetModuleBase = 0;

DebugExceptionTrap::SYMGETSYMFROMADDRPROC
                                    DebugExceptionTrap::_SymGetSymFromAddr = 0;

#endif

DebugExceptionTrap GDebugExceptionTrap;  // Declare global instance of class

//============================== Class Methods =============================

//=============
// Constructor 
//=============
DebugExceptionTrap :: DebugExceptionTrap( )
{
	#if DEBUGGER_DETECTION
	if (!GDebugger.IsDebugger())
	#endif
	{
		m_header = false; // header printer
		m_configHeader = false;
    // Install the unhandled exception filter function
    m_previousFilter = SetUnhandledExceptionFilter(ExceptionCallback);

    // Figure out what the report file will be named, and store it away
		#ifndef _XBOX
			GetModuleFileName( 0, m_szLogFileName, MAX_PATH );
		#else
			strcpy(m_szLogFileName,"Current game");
		#endif

    // Look for the '.' before the "EXE" extension.  Replace the extension
    // with "RPT"
    PTSTR pszDot = _tcsrchr( m_szLogFileName, _T('.') );
    if ( pszDot )
    {
        pszDot++;   // Advance past the '.'
        if ( _tcslen(pszDot) >= 3 )
            _tcscpy( pszDot, _T("RPT") );   // "RPT" -> "Report"
    }
	}
}

//============
// Destructor 
//============
DebugExceptionTrap :: ~DebugExceptionTrap( )
{
	if (m_previousFilter)
	{
    SetUnhandledExceptionFilter( m_previousFilter );
	}
}

//==============================================================
// Lets user change the name of the report file to be generated 
//==============================================================
void DebugExceptionTrap :: SetLogFileName( const char *pszLogFileName )
{
    _tcscpy( m_szLogFileName, pszLogFileName );
}

extern void PrintMissionInfo( HANDLE file );
extern bool PrintConfigInfo( HANDLE file );
extern void PrintNetworkInfo( HANDLE file );

// file operations
void DebugExceptionTrap::OpenLogFile()
{
	#if DEBUGGER_DETECTION
	if (!GDebugger.IsDebugger())
	#endif
	{
		m_hReportFile = CreateFile
		(
			m_szLogFileName,
			GENERIC_WRITE,
			0,
			0,
			OPEN_ALWAYS, //CREATE_ALWAYS,
			FILE_FLAG_WRITE_THROUGH,
			0
		);
	}

  if ( m_hReportFile )
  {
      SetFilePointer( m_hReportFile, 0, 0, FILE_END );

			PrintHeader();
	}
}

void DebugExceptionTrap :: PrintHeader()
{
	if( !m_header )
	{
		m_header=true;
		// scan session information

		#ifndef _XBOX
		TCHAR exeName[MAX_PATH];
    GetModuleFileName( 0, exeName, MAX_PATH );

		_tprintf( _T("\r\n") );
		_tprintf( _T("\r\n") );
    _tprintf( _T("=====================================================================\r\n") );
		_tprintf( _T("== %s\r\n"), exeName );
    _tprintf( _T("=====================================================================\r\n") );

		struct _stat exeStat;
		if( _stat(exeName,&exeStat)>=0 )
		{
			_tprintf( _T("Exe version: %s\r\n"), ctime(&exeStat.st_mtime) );
		}
		PrintConfig();

		#endif
	}
}

void DebugExceptionTrap :: CloseLogFile()
{
  if ( m_hReportFile )
	{
    CloseHandle( m_hReportFile );
    m_hReportFile = 0;
	}
}

const int MaxTextReported = 256;
static char TextReported[256][MaxTextReported];
static int NTextReported;

void DebugExceptionTrap::ReportContext( const char *text, CONTEXT *context )
{
	#if !_ENABLE_REPORT
	GDebugExceptionTrap.IntelStackSave(context);
	#endif

	OpenLogFile();
	
  if( text ) _tprintf( _T("===%s====>>>>>>BEG\r\n"), text );

	PrintContext((void *)context->Eip,context);
	#if _ENABLE_REPORT
	IntelStackWalk(context);
	#endif

  if( text ) _tprintf( _T("===%s====>>>>>>END\r\n"), text );
	
	CloseLogFile();
}

void DebugExceptionTrap :: LogFSP( void *eip, DWORD *esp, const char *text, bool stack )
{
	OpenLogFile();
	_tprintf( _T("%s\r\n"), text);
	if( stack )
	{
		#if DEBUGGER_DETECTION
		if (GDebugger.IsDebugger())
		{
			BREAK();
		}
		#endif

		// check if this text was already reported
		int i;
		for( i=0; i<NTextReported; i++ )
		{
			if( !strcmp(TextReported[i],text) ) break;
		}

		if( i>=NTextReported )
		{
			if (NTextReported<MaxTextReported)
			{
				strcpy(TextReported[NTextReported++],text);
				// print mission header
				PrintConfig();
				if (m_hReportFile) PrintMissionInfo(m_hReportFile);

				// generate stack report
				IntelStackWalk((DWORD)eip,esp,(DWORD *)m_stackBottom);
			}
		}
	}
	
	CloseLogFile();
}

#pragma warning(disable:4035)

__forceinline void *GetEIP()
{
	__asm
	{
		LabelHere:
		lea eax,LabelHere
	};
}

#pragma warning(default:4035)

void DebugExceptionTrap :: LogFFF( const char *text, bool stack )
{
	// get eip, esp
	int stackvar=0;
	//
	void *eip = GetEIP();
	//GDebugExceptionTrap.LogFSP(LogF,(DWORD *)&text,text,stack);
	GDebugExceptionTrap.LogFSP(eip,(DWORD *)&stackvar,text,stack);
}

void DebugExceptionTrap :: LogLine(const char *text, ...)
{
	char szBuff[256];
  va_list argptr;
        
  va_start( argptr, text );
  wvsprintf( szBuff, text, argptr );
  va_end( argptr );
	OpenLogFile();
	_tprintf( _T("%s\r\n"), szBuff);
	CloseLogFile();
}

void DebugExceptionTrap :: SaveContext()
{
	#ifndef _XBOX
	CONTEXT context;
	context.ContextFlags = (
		CONTEXT_INTEGER | CONTEXT_FLOATING_POINT | 
		CONTEXT_CONTROL | CONTEXT_SEGMENTS
	);
	GetThreadContext(GetCurrentThread(),&context);
	context.Eip = (DWORD)GetEIP();
  IntelStackSave(&context);
	#endif
}


//===========================================================
// Entry point where control comes on an unhandled exception 
//===========================================================

LONG WINAPI DebugExceptionTrap::ExceptionCallback( PEXCEPTION_POINTERS pExceptionInfo )
{
	return GDebugExceptionTrap.UnhandledExceptionFilter(pExceptionInfo);
}


LONG DebugExceptionTrap :: UnhandledExceptionFilter( PEXCEPTION_POINTERS pExceptionInfo )
{
	// no alive will arive any more
	GDebugger.NextAliveExpected(INT_MAX);

  OutputDebugString(_T("Unhandled exception."));
	OpenLogFile();
  if ( m_hReportFile )
  {
    GenerateExceptionReport( pExceptionInfo );
  }
	CloseLogFile();

  if ( m_previousFilter )
    return m_previousFilter( pExceptionInfo );
  else
    return EXCEPTION_CONTINUE_SEARCH;
}

void DebugExceptionTrap::PrintBanner()
{
	if (!m_hReportFile) return;

	char tmpBuf[32];
	// Start out with a banner
	_tprintf( _T("=======================================================\r\n") );
	_strdate(tmpBuf);
	_tprintf( _T("Date: %s  "), tmpBuf);
	_strtime(tmpBuf);
	_tprintf( _T("Time: %s\r\n"), tmpBuf);
	_tprintf( _T("-------------------------------------------------------\r\n") );
}

void DebugExceptionTrap::PrintConfig()
{
	if (m_configHeader) return;
	if (!m_hReportFile) return;

	if (PrintConfigInfo(m_hReportFile)) m_configHeader = true;
}

void DebugExceptionTrap::PrintContext( void *ip, CONTEXT *ctx )
{
	PrintConfig();

	TCHAR szFaultingModule[MAX_PATH];
	DWORD section, offset;
	GetLogicalAddress(  ip,
											szFaultingModule,
											sizeof( szFaultingModule ),
											section, offset );

	_tprintf( _T("Version %s\r\n"), APP_VERSION_TEXT );
	_tprintf( _T("Fault address:  %08X %02X:%08X %s\r\n"),
							ip,
							section, offset, szFaultingModule );

	if (m_hReportFile) PrintMissionInfo(m_hReportFile);

	if (!IsBadReadPtr((char *)ip-16,32))
	{
		char code[1024];

		*code = 0;
		for (int i=0; i<16; i++)
		{
			sprintf(code+strlen(code)," %02X",((unsigned char *)ip-16)[i]);
		}
		_tprintf( _T("Prev. code bytes:%s\r\n"), code );

		*code = 0;
		for (int i=0; i<16; i++)
		{
			sprintf(code+strlen(code)," %02X",((unsigned char *)ip)[i]);
		}
		_tprintf( _T("Fault code bytes:%s\r\n"), code );
	}

  // Show the registers
  #ifdef _M_IX86  // Intel Only!
  _tprintf( _T("\r\nRegisters:\r\n") );

  _tprintf(_T("EAX:%08X EBX:%08X\r\nECX:%08X EDX:%08X\r\nESI:%08X EDI:%08X\r\n"),
          ctx->Eax, ctx->Ebx, ctx->Ecx, ctx->Edx, ctx->Esi, ctx->Edi );

  _tprintf( _T("CS:EIP:%04X:%08X\r\n"), ctx->SegCs, ctx->Eip );
  _tprintf( _T("SS:ESP:%04X:%08X  EBP:%08X\r\n"),
              ctx->SegSs, ctx->Esp, ctx->Ebp );
	#ifndef _XBOX
  _tprintf( _T("DS:%04X  ES:%04X  FS:%04X  GS:%04X\r\n"),
              ctx->SegDs, ctx->SegEs, ctx->SegFs, ctx->SegGs );
	#endif
  _tprintf( _T("Flags:%08X\r\n"), ctx->EFlags );

  #endif
}

//===========================================================================
// Open the report file, and write the desired information to it.  Called by 
// UnhandledExceptionFilter                                               
//===========================================================================

/*!
\patch 1.02 Date 7/6/2001 by Ondra.
- New: Save context.bin on crash for post-mortem analysis.
\patch_internal 1.42 Date 1/8/2002 by Ondra
- Fixed: Removed double Fault address line in Flashpoint.rpt"
*/

void DebugExceptionTrap :: GenerateExceptionReport(
    PEXCEPTION_POINTERS pExceptionInfo )
{

	PrintBanner();

	PrintNetworkInfo(m_hReportFile);

  PEXCEPTION_RECORD pExceptionRecord = pExceptionInfo->ExceptionRecord;

  // First print information about the type of fault
  _tprintf(   _T("Exception code: %08X %s at %08X\r\n"),
              pExceptionRecord->ExceptionCode,
              GetExceptionString(pExceptionRecord->ExceptionCode),
							pExceptionRecord->ExceptionAddress
							);

	// save stack first - in case map file operation will fail
  IntelStackSave( pExceptionInfo->ContextRecord );

  // Now print information about where the fault occured
	/*
  TCHAR szFaultingModule[MAX_PATH];
  DWORD section, offset;
  GetLogicalAddress(  pExceptionRecord->ExceptionAddress,
                      szFaultingModule,
                      sizeof( szFaultingModule ),
                      section, offset );
	*/

	PrintContext(pExceptionRecord->ExceptionAddress,pExceptionInfo->ContextRecord);
  _tprintf( _T("=======================================================\r\n") );

	#if _ENABLE_REPORT
    // Walk the stack using x86 specific code
    IntelStackWalk( pExceptionInfo->ContextRecord );

    _tprintf( _T("\r\n") );
	#endif
}

//======================================================================
// Given an exception code, returns a pointer to a static string with a 
// description of the exception                                         
//======================================================================
LPTSTR DebugExceptionTrap :: GetExceptionString( DWORD dwCode )
{
    #define EXCEPTION( x ) case EXCEPTION_##x: return _T(#x);

    switch ( dwCode )
    {
        EXCEPTION( ACCESS_VIOLATION )
        EXCEPTION( DATATYPE_MISALIGNMENT )
        EXCEPTION( BREAKPOINT )
        EXCEPTION( SINGLE_STEP )
        EXCEPTION( ARRAY_BOUNDS_EXCEEDED )
        EXCEPTION( FLT_DENORMAL_OPERAND )
        EXCEPTION( FLT_DIVIDE_BY_ZERO )
        EXCEPTION( FLT_INEXACT_RESULT )
        EXCEPTION( FLT_INVALID_OPERATION )
        EXCEPTION( FLT_OVERFLOW )
        EXCEPTION( FLT_STACK_CHECK )
        EXCEPTION( FLT_UNDERFLOW )
        EXCEPTION( INT_DIVIDE_BY_ZERO )
        EXCEPTION( INT_OVERFLOW )
        EXCEPTION( PRIV_INSTRUCTION )
        EXCEPTION( IN_PAGE_ERROR )
        EXCEPTION( ILLEGAL_INSTRUCTION )
        EXCEPTION( NONCONTINUABLE_EXCEPTION )
        EXCEPTION( STACK_OVERFLOW )
        EXCEPTION( INVALID_DISPOSITION )
        EXCEPTION( GUARD_PAGE )
        EXCEPTION( INVALID_HANDLE )
    }

    // If not one of the "known" exceptions, try to get the string
    // from NTDLL.DLL's message table.

		#ifndef _XBOX
    static TCHAR szBuffer[512] = { 0 };

    FormatMessage(  FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_FROM_HMODULE,
                    GetModuleHandle( _T("NTDLL.DLL") ),
                    dwCode, 0, szBuffer, sizeof( szBuffer ), 0 );

    return szBuffer;
		#else
    return "UNKNOWN_EXCEPTION";
		#endif

}

//==============================================================================
// Given a linear address, locates the module, section, and offset containing  
// that address.                                                               
//                                                                             
// Note: the szModule paramater buffer is an output buffer of length specified 
// by the len parameter (in characters!)                                       
//==============================================================================
BOOL DebugExceptionTrap :: GetLogicalAddress(
        PVOID addr, PTSTR szModule, DWORD len, DWORD& section, DWORD& offset )
{
    MEMORY_BASIC_INFORMATION mbi;

		strcpy(szModule,"Unknown module");

    if ( !VirtualQuery( addr, &mbi, sizeof(mbi) ) )
        return FALSE;

    DWORD hMod = (DWORD)mbi.AllocationBase;
		// we need to get read access
		// if not, it is not a module and we will not try to get its name
    // Point to the DOS header in memory
    PIMAGE_DOS_HEADER pDosHdr = (PIMAGE_DOS_HEADER)hMod;

		if (IsBadReadPtr(pDosHdr,sizeof(PIMAGE_DOS_HEADER)))
		{
			offset = (DWORD)addr;
			section = 0;
			return FALSE;
		}

		#ifndef _XBOX
    if ( !GetModuleFileName( (HMODULE)hMod, szModule, len ) )
        return FALSE;
		#else
		strcpy(szModule,"Some module");
		#endif


    // From the DOS header, find the NT (PE) header
    PIMAGE_NT_HEADERS pNtHdr = (PIMAGE_NT_HEADERS)(hMod + pDosHdr->e_lfanew);
		if (IsBadReadPtr(pNtHdr,sizeof(PIMAGE_NT_HEADERS)))
		{
			offset = (DWORD)addr;
			section = 0;
			return FALSE;
		}


    PIMAGE_SECTION_HEADER pSection = IMAGE_FIRST_SECTION( pNtHdr );

		if (IsBadReadPtr(pSection,sizeof(PIMAGE_NT_HEADERS)*pNtHdr->FileHeader.NumberOfSections))
		{
			offset = (DWORD)addr;
			section = 0;
			return FALSE;
		}

    DWORD rva = (DWORD)addr - hMod; // RVA is offset from module load address

    // Iterate through the section table, looking for the one that encompasses
    // the linear address.
    for (   unsigned i = 0;
            i < pNtHdr->FileHeader.NumberOfSections;
            i++, pSection++ )
    {
        DWORD sectionStart = pSection->VirtualAddress;
        DWORD sectionEnd = sectionStart
                    + max(pSection->SizeOfRawData, pSection->Misc.VirtualSize);

        // Is the address in this section???
        if ( (rva >= sectionStart) && (rva <= sectionEnd) )
        {
            // Yes, address is in the section.  Calculate section and offset,
            // and store in the "section" & "offset" params, which were
            // passed by reference.
            section = i+1;
            offset = rva - sectionStart;
            return TRUE;
        }
    }

    return FALSE;   // Should never get here!
}

//============================================================
// Walks the stack, and writes the results to the report file 
//============================================================
void DebugExceptionTrap :: IntelPCPrint( MapFile &map, int pc, bool doEol)
{
  TCHAR szModule[MAX_PATH] = _T("");
  DWORD section = 0, offset = 0;

  GetLogicalAddress((PVOID)pc, szModule,sizeof(szModule),section,offset );

	int nameValue=0;
	const char *name=map.MapNameFromLogical(offset,&nameValue);
	int nameOffset=offset-nameValue;

	
	if( section==1 && nameValue!=0 )
	{
		_tprintf( _T("%8X %8X %8X + %s"),
              pc, nameValue, nameOffset, name );
	}
	else
	{
		_tprintf( _T("%08X %04X:%08X       %s"),
							 pc, section, offset, szModule );
	}
	if (doEol)
	{
		_tprintf( _T("\r\n") );
	}
}


// Called may contain 0 when unknown indirect call took place

/*!
\patch_internal 1.01 Date 6/28/2001 by Ondra.
- Fixed: disabled call stack walk on exception in SuperRelease build.
- This should make crashes to behave more consistently.
*/

void DebugExceptionTrap :: IntelStackWalk
(
	DWORD eip, DWORD *pStackTop, DWORD *pStackBot
)
{
	#if _ENABLE_REPORT
	if( pStackBot<pStackTop ) pStackBot=pStackTop+4*1024; // in DWORDS

  _tprintf( _T("\r\nCall stack:\r\n") );
  _tprintf( _T("\r\nStack %08X %08X\r\n"), pStackTop,pStackBot );

  _tprintf( _T("Address  Logical            Function\r\n") );

  DWORD pc = eip;

	static MapFile map;
	map.ParseMapFile();

  _tprintf( _T("mapfile: %s (empty %d)\r\n"), map.GetName(), map.Empty() );


	
	int minPc=map.MinLogicalAddress();
	int maxPc=map.MaxLogicalAddress();
	
	int offset=map.MinPhysicalAddress()-map.MinLogicalAddress();
	if( map.Empty() ) return;
	/*
	IntelPCPrint(map,pc);

	
	// first of all dump call stack (old way)
	for( PDWORD stack=pStackTop; stack<pStackBot; stack++ )
	{
    // Can two DWORDs be read from the supposed frame address?          
		if( IsBadReadPtr(stack,4) ) break;
    int abspc = *stack;

		DWORD logpc=abspc-offset;
		if( logpc<minPc || logpc>=maxPc ) continue;

		// check start of the function
    if ( IsBadCodePtr((int (__stdcall*)())abspc) ) continue;

		// log the value
		IntelPCPrint(map,abspc, true);

  }
	*/

	// two pass processing
	_calls = 0;

	int functionStart=0;
	map.MapNameFromLogical(pc-offset,&functionStart);

	for( PDWORD stack=pStackTop; stack<pStackBot; stack++ )
	{
		if( IsBadReadPtr(stack,4) ) break;
    int abspc = *stack;

		DWORD logpc=abspc-offset;
		if( logpc<minPc || logpc>=maxPc ) continue;

		// check start of the function
		int functionStart=0;

		map.MapNameFromLogical(logpc,&functionStart);

    // Can two DWORDs be read from the supposed frame address?          
    if ( IsBadCodePtr((int (__stdcall*)())abspc) ) continue;

		// try to verify if we could be called from this address
		// check for known call instructions

		DWORD calledAddress = -1; // -1 -> no call

		// read some bytes from the abspc

		BYTE *pcCode = (BYTE *)abspc;

		if( !IsBadReadPtr(pcCode-6,6) )
		{
			// note: cs is not the same as ds
			// this may result in some conversions necessary?
			// advance PC validation
			if (pcCode[-5]==0xe8)
			{
				// simple relative call instruction
				LONG relativeOffset = *(LONG *)(pcCode-4);
				DWORD addr = abspc+relativeOffset;
				DWORD logAddr = addr-offset;
				calledAddress = logAddr;
			}
			else
			{
				// check 2..6 B call instruction
				for (int b=-2; b>=-6; b--)
				{
					// check R/M byte
					// bits 5,4,3 of R/M should be 010
					if (pcCode[b]!=0xff) continue;
					if ( ((pcCode[b+1]>>3)&7)!=2) continue;
					calledAddress = 0;
					break;
				}
			}
		}

		if (calledAddress!=-1)
		{
			// some call detected

			if (_calls<MaxCalls)
			{
				// store call information
				_pcValue[_calls] = logpc; // abspc logical address
				_fStart[_calls] = functionStart; // abspc function start logical address
				_called[_calls] = calledAddress; // called logical address

				_calls++;
			}
		}

  }

  //_tprintf( _T("\r\n------- Begin:: info callstack:\r\n") );
	PrintCalls(map,pc);
  //_tprintf( _T("\r\n------- End  :: info callstack:\r\n") );


  _tprintf( _T("\r\n------- Begin:: Optimized callstack:\r\n") );
	OptimizeCalls(functionStart);
	PrintCalls(map,pc);
  _tprintf( _T("\r\n------- End  :: Optimized callstack:\r\n") );
	#endif
}

void DebugExceptionTrap :: DeleteCall(int i)
{
	for (int j=i+1; j<_calls; j++)
	{
		_pcValue[j-1] = _pcValue[j]; // pc logical address
		_fStart[j-1] = _fStart[j]; // pc logical address
		_called[j-1] = _called[j]; // pc logical address
	}
	_calls--;
}

void DebugExceptionTrap :: OptimizeCalls(int functionStart)
{
	// remove functions that are sure to be skipped
	for (int i=0; i<_calls; i++)
	{
		// check if we know where was this place called from
		int calledFrom = -1;
		for (int j=i+1; j<_calls; j++)
		{
			// in case of recursion we cannot proceed
			if (_fStart[j]==_fStart[i]) break;
			// check if this is the call-site
			if (_called[j]==_fStart[i]) {calledFrom=j;break;}
		}
		if (calledFrom<0) continue; // we do not know the call site
		// remove anything between i dan calledFrom
		for (int j=i+1; j<calledFrom; )
		{
			DeleteCall(j);
			calledFrom--; // calledFrom index is moved
		}

	}
	// remove calls that are impossible
	for (int i=0; i<_calls; i++)
	{
		if (_called[i]==0) continue; // indirect call - cannot remove
		bool callPossible = functionStart==_called[i];
		for (int j=0; j<i; j++)
		{
			if (_fStart[j]==_called[i]) {callPossible = true;break;}
		}
		if (callPossible) continue;
		DeleteCall(i);
		i--;
	}

	// TODO: remove impossible indirect calls
}

void DebugExceptionTrap::PrintCalls( MapFile &map, int pc )
{
	IntelPCPrint(map,pc);

	//int minPc=map.MinLogicalAddress();
	//int maxPc=map.MaxLogicalAddress();
	
	int offset=map.MinPhysicalAddress()-map.MinLogicalAddress();

	int functionStart=0;
	map.MapNameFromLogical(pc-offset,&functionStart);

	int lastFunctionStart = functionStart;
	for (int i=0; i<_calls; i++)
	{
		int pc = _pcValue[i];
		int called = _called[i];

		int nameValue=0;
		const char *name=map.MapNameFromLogical(pc,&nameValue);
		int nameOffset = pc-nameValue;

	
		_tprintf( _T("%8X %8X %8X + %-20s"),
              pc+offset, nameValue, nameOffset, name );
		

		//IntelPCPrint(map,pc, false);

		// print call information

		if (!called)
		{
			_tprintf( _T("     (Indirect)\r\n") );
		}

		else if (lastFunctionStart!=called)
		{
			_tprintf( _T("     -> %8X\r\n"), called );
		}
		else
		{
			_tprintf( _T("\r\n") );
		}
		lastFunctionStart = nameValue;

		
	}
}

void DebugExceptionTrap :: IntelStackWalk( PCONTEXT pContext )
{
	IntelStackWalk(pContext->Eip,(PDWORD)pContext->Esp,(PDWORD)m_stackBottom);
}


/*!
\patch 1.04 Date 7/14/2001 by Ondra.
- Improved: Crash info context.bin contains more information.
- CPU registers saved
- version info saved
\patch 1.12 Date 8/8/2001 by Ondra.
- Improved: context.bin contains more info about process memory.
\patch_internal 1.28 Date 10/18/2001 by Ondra.
- Improved: context.bin contains information about configuration (Server, Czech)
*/

#include "versionNo.h"

void DebugExceptionTrap :: IntelStackSave( PCONTEXT pContext )
{
	//DWORD eip = pContext->Eip;
	DWORD *pStackTop = (DWORD *)pContext->Esp;
	DWORD *pStackBot = (DWORD *)m_stackBottom;

	if( pStackBot<pStackTop ) pStackBot=pStackTop+4*1024; // in DWORDS

	// dump stack to external file
	int stackF = open("context.bin",_O_CREAT|_O_TRUNC|_O_BINARY|_O_WRONLY,_S_IREAD|_S_IWRITE);
	if (stackF>=0)
	{
		// write EIP
		static const char magic[]="STK4";
		// save context
		write(stackF,magic,sizeof(magic));
		int ver = APP_VERSION_NUM;
		write(stackF,&ver,sizeof(ver));
		write(stackF,pContext,sizeof(*pContext));
		// save some info about exe file
		#ifndef _XBOX
			char exePath[1024];
			GetModuleFileName(NULL,exePath,sizeof(exePath));
			int exeHandle = open(exePath,O_RDONLY);
			int exeSize = filelength(exeHandle);
			close(exeHandle);
		#else
			int exeSize = 0;
		#endif
		write(stackF,&exeSize,sizeof(exeSize));
		char mapExt[3];
		mapExt[0] = 0;
		#if _FORCE_DS_CONTEXT
			strcpy(mapExt,"DS");
		#elif _CZECH
			strcpy(mapExt,"CZ");
		#elif _RUSSIAN
			strcpy(mapExt,"RU");
		#endif
		write(stackF,mapExt,2);
		// save CRC info about process memory
		#ifndef _XBOX
    PIMAGE_DOS_HEADER pDosHdr = (PIMAGE_DOS_HEADER)GetModuleHandle(NULL);
		#else
    PIMAGE_DOS_HEADER pDosHdr = (PIMAGE_DOS_HEADER)0x400000;
		#endif
    // From the DOS header, find the NT (PE) header
    PIMAGE_NT_HEADERS pNtHdr = (PIMAGE_NT_HEADERS)((char *)pDosHdr + pDosHdr->e_lfanew);
    PIMAGE_SECTION_HEADER pSection = IMAGE_FIRST_SECTION( pNtHdr );
		// save crc of each 4KB page
		const int pagesize = 4*1024;
		int codeSize = pSection->SizeOfRawData;
		char *codeData = (char *)pDosHdr+pSection->PointerToRawData;
		write(stackF,&codeSize,sizeof(codeSize));

		CRCCalculator crc;
		while (codeSize>=pagesize)
		{
			unsigned int crcResult = crc.CRC(codeData,pagesize);
			write(stackF,&crcResult,sizeof(crcResult));
			codeSize -= pagesize;
			codeData += pagesize;
		}

		write(stackF,&pStackBot,sizeof(pStackBot));
		write(stackF,pStackTop,(int)pStackBot-(int)pStackTop);
		close(stackF);
	}
}

//============================================================
// Walks the stack, and writes the results to the report file 
//============================================================
#if USE_IMAGE

void DebugExceptionTrap :: ImagehlpStackWalk( PCONTEXT pContext )
{
    _tprintf( _T("\r\nCall stack (IMAGEHLP.DLL):\r\n") );

    _tprintf( _T("Address   Frame\r\n") );

    // Could use SymSetOptions here to add the SYMOPT_DEFERRED_LOADS flag

    STACKFRAME sf;
    memset( &sf, 0, sizeof(sf) );

    // Initialize the STACKFRAME structure for the first call.  This is only
    // necessary for Intel CPUs, and isn't mentioned in the documentation.
    sf.AddrPC.Offset       = pContext->Eip;
    sf.AddrPC.Mode         = AddrModeFlat;
    sf.AddrStack.Offset    = pContext->Esp;
    sf.AddrStack.Mode      = AddrModeFlat;
    sf.AddrFrame.Offset    = pContext->Ebp;
    sf.AddrFrame.Mode      = AddrModeFlat;

    while ( 1 )
    {
        if ( ! _StackWalk(  IMAGE_FILE_MACHINE_I386,
                            GetCurrentProcess(),
                            GetCurrentThread(),
                            &sf,
                            pContext,
                            0,
                            _SymFunctionTableAccess,
                            _SymGetModuleBase,
                            0 ) )
            break;

        if ( 0 == sf.AddrFrame.Offset ) // Basic sanity check to make sure
            break;                      // the frame is OK.  Bail if not.

        _tprintf( _T("%08X  %08X  "), sf.AddrPC.Offset, sf.AddrFrame.Offset );

        // IMAGEHLP is wacky, and requires you to pass in a pointer to a
        // IMAGEHLP_SYMBOL structure.  The problem is that this structure is
        // variable length.  That is, you determine how big the structure is
        // at runtime.  This means that you can't use sizeof(struct).
        // So...make a buffer that's big enough, and make a pointer
        // to the buffer.  We also need to initialize not one, but TWO
        // members of the structure before it can be used.

        BYTE symbolBuffer[ sizeof(IMAGEHLP_SYMBOL) + 512 ];
        PIMAGEHLP_SYMBOL pSymbol = (PIMAGEHLP_SYMBOL)symbolBuffer;
        pSymbol->SizeOfStruct = sizeof(symbolBuffer);
        pSymbol->MaxNameLength = 512;
                        
        DWORD symDisplacement = 0;  // Displacement of the input address,
                                    // relative to the start of the symbol

        if ( _SymGetSymFromAddr(GetCurrentProcess(), sf.AddrPC.Offset,
                                &symDisplacement, pSymbol) )
        {
            _tprintf( _T("%hs+%X\r\n"), pSymbol->Name, symDisplacement );
            
        }
        else    // No symbol found.  Print out the logical address instead.
        {
            TCHAR szModule[MAX_PATH] = _T("");
            DWORD section = 0, offset = 0;

            GetLogicalAddress(  (PVOID)sf.AddrPC.Offset,
                                szModule, sizeof(szModule), section, offset );

            _tprintf( _T("%04X:%08X %s\r\n"),
                        section, offset, szModule );
        }
    }

}

#endif

//============================================================================
// Helper function that writes to the report file, and allows the user to use 
// printf style formating                                                     
//============================================================================
int __cdecl DebugExceptionTrap :: _tprintf(const TCHAR * format, ...)
{
	int retValue=0;
	if (m_hReportFile)
	{
    TCHAR szBuff[1024];
    DWORD cbWritten;
    va_list argptr;
          
    va_start( argptr, format );
    retValue = wvsprintf( szBuff, format, argptr );
    va_end( argptr );

    WriteFile( m_hReportFile, szBuff, retValue * sizeof(TCHAR), &cbWritten, 0 );
	}
	return retValue;
}

#if USE_IMAGE

//=========================================================================
// Load IMAGEHLP.DLL and get the address of functions in it that we'll use 
//=========================================================================
BOOL DebugExceptionTrap :: InitImagehlpFunctions( void )
{
    HMODULE hModImagehlp = LoadLibrary( _T("IMAGEHLP.DLL") );
    if ( !hModImagehlp )
        return FALSE;

    _SymInitialize = (SYMINITIALIZEPROC)GetProcAddress( hModImagehlp,
                                                        "SymInitialize" );
    if ( !_SymInitialize )
        return FALSE;

    _SymCleanup = (SYMCLEANUPPROC)GetProcAddress( hModImagehlp, "SymCleanup" );
    if ( !_SymCleanup )
        return FALSE;

    _StackWalk = (STACKWALKPROC)GetProcAddress( hModImagehlp, "StackWalk" );
    if ( !_StackWalk )
        return FALSE;

    _SymFunctionTableAccess = (SYMFUNCTIONTABLEACCESSPROC)
                GetProcAddress( hModImagehlp, "SymFunctionTableAccess" );

    if ( !_SymFunctionTableAccess )
        return FALSE;

    _SymGetModuleBase = (SYMGETMODULEBASEPROC)GetProcAddress( hModImagehlp,
                                                        "SymGetModuleBase");
    if ( !_SymGetModuleBase )
        return FALSE;

    _SymGetSymFromAddr = (SYMGETSYMFROMADDRPROC)GetProcAddress( hModImagehlp,
                                                        "SymGetSymFromAddr" );
    if ( !_SymGetSymFromAddr )
        return FALSE;

    if ( !_SymInitialize( GetCurrentProcess(), 0, TRUE ) )
        return FALSE;

    return TRUE;        
}

#endif
