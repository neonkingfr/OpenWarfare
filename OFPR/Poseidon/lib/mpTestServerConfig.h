// Configuration parameters for internal release / debug version

#define _VERIFY_KEY								0
#define _ENABLE_EDITOR						0
#define _ENABLE_ALL_MISSIONS			0
#define _ENABLE_MP								1
#define _ENABLE_AI								1
#define _ENABLE_CAMPAIGN					0
#define _ENABLE_SINGLE_MISSION		0
#define _ENABLE_BRIEF_EQ					1
#define _ENABLE_BRIEF_GRP					1
#define _ENABLE_PARAMS						1
#define _ENABLE_CHEATS						0
#define _ENABLE_ADDONS						1
#define _ENABLE_DEDICATED_SERVER	1
#define _ENABLE_GAMESPY						1
#define _FORCE_DEMO_ISLAND				0
#define _FORCE_SINGLE_VOICE				0
#define _ENABLE_AIRPLANES					1
#define _ENABLE_HELICOPTERS				1
#define _ENABLE_SHIPS							1
#define _ENABLE_CARS							1
#define _ENABLE_TANKS							1
#define _ENABLE_MOTORCYCLES				1
#define _ENABLE_PARACHUTES				1
#define _ENABLE_DATADISC					0
#define _ENABLE_VBS								0
#define _DISABLE_CRC_PROTECTION   1
#define _FORCE_DS_CONTEXT         1

#define _DISABLE_GUI							1 // no GUI (mouse, output, keyboard)

#define _TIME_ACC_MIN							1.0
#define _TIME_ACC_MAX							1.0

#define _MACRO_CDP							  0 // no macrovision protection

// Registry key for saving player's name, IP address of server etc.
#define ConfigApp "Software\\Codemasters\\Operation Flashpoint"

// Application name
#define AppName "Operation Flashpoint MP Test"

// Preferences program
#define PreferencesExe	"OpFlashPreferences.exe"

#define _MOD_PATH_DEFAULT "OFPMPTest"