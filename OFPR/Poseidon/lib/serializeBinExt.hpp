#ifdef _MSC_VER
#pragma once
#endif

#ifndef _SERIALIZE_BIN_EXT_HPP
#define _SERIALIZE_BIN_EXT_HPP

#include <El/QStream/serializeBin.hpp>

void operator << ( SerializeBinStream &s, Vector3 &data );
void operator << ( SerializeBinStream &s, Matrix3 &data );
void operator << ( SerializeBinStream &s, Matrix4 &data );

#endif
