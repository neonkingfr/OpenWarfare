#include "wpch.hpp"

/*!
\file
Implementation file for Man and Soldier classes
*/

#include "soldierOld.hpp"
#include "transport.hpp"
#include "shots.hpp"
#include "scene.hpp"
#include "world.hpp"
#include "landscape.hpp"
#include "keyInput.hpp"
#include <El/Common/randomGen.hpp>
#include "camera.hpp"
#include "tracks.hpp"
#include <El/Common/perfLog.hpp>
#include "perfProf.hpp"
#include "proxyWeapon.hpp"
#include "SpecLods.hpp"
#include "house.hpp"
#include "detector.hpp"
#include "txtPreload.hpp"
#include "diagModes.hpp"
#include <Es/Strings/bstring.hpp>

#include "move_actions.hpp"
#include "integrity.hpp"

#include "dikCodes.h"

#include "network.hpp"

#include <El/QStream/QBStream.hpp>

#include "operMap.hpp"

#include "uiActions.hpp"

#include "gameStateExt.hpp"
#include "scripts.hpp"

#include "seaGull.hpp"
#include <Es/Algorithms/qsort.hpp>

#include <El/Common/enumNames.hpp>

#include "chat.hpp"

//#include "strIncl.hpp"
#include "stringtableExt.hpp"

#include "cdapfncond.h"

/*
#if _CZECH
#include "roxxe.hpp"
#endif
*/

#if !_RELEASE
	#define ARROWS 1
#endif

#define DNAME() ( (const char *)GetDebugName() )

#define NAME(i) ( (const char *)GetMoveName(i) )
#define NAME_T(t,i) ( (const char *)t->GetMoveName(i) )

//bool VerifyFloat( float x );

// clever edge representation

// fastest is NxN matrix (N is number of moves)
// can be quite space consuming?
// we can expect 100-1000 moves
// matrix would do for max. 256 moves - if we have more, we should use some other represent.
// we implement it as sparse matrix

#include <Es/Memory/normalNew.hpp>

// static const Matrix4 M4HeadBase(MDirection, -VAside, VUp);

struct ActionContextDefault: public ActionContextBase
{
	int param;
	int param2;
	RString param3;

	ActionContextDefault();
	~ActionContextDefault();

	USE_FAST_ALLOCATOR;
};

struct ActionContextUIAction: public ActionContextBase
{
	UIAction action;

	USE_FAST_ALLOCATOR;
	ActionContextUIAction(const UIAction &uiAction);
};

struct ActionContextGetIn: public ActionContextBase
{
	OLink<Transport> vehicle;
	UIActionType position;

	USE_FAST_ALLOCATOR;
	ActionContextGetIn(Transport *veh, UIActionType pos);
};

ActionContextBase *CreateGetInActionContext(Transport *veh, UIActionType pos)
{
	return new ActionContextGetIn(veh, pos);
}

#include <Es/Memory/debugNew.hpp>

DEFINE_FAST_ALLOCATOR(ActionContextDefault)

ActionContextDefault::ActionContextDefault()
{
	static const RStringB empty = "";
	param = 0;
	param2 = 0;
	param3 = empty;
}

ActionContextDefault::~ActionContextDefault()
{
}


DEFINE_FAST_ALLOCATOR(ActionContextUIAction)

ActionContextUIAction::ActionContextUIAction(const UIAction &uiAction)
:action(uiAction)
{
	function = MFUIAction;
}

DEFINE_FAST_ALLOCATOR(ActionContextGetIn)

ActionContextGetIn::ActionContextGetIn(Transport *veh, UIActionType pos)
{
	vehicle = veh;
	position = pos;
}

const static MotionEdge NoEdge;


MotionPath::MotionPath()
{
	//static StaticStorage<MoveId> storage;
	//SetStorage(storage.Init(128));
}

#include <Es/Containers/bankArray.hpp>

template<>
struct BankTraits<AnimationRT>
{
	typedef const AnimationRTName &NameType;
	static int CompareNames( const AnimationRTName &n1, const AnimationRTName &n2 )
	{
		// compare pointers
		int d = strcmpi(n1.name,n2.name);
		if (d) return d;
		d = (char *)n1.skeleton.GetRef()-(char *)n2.skeleton.GetRef();
		return d;
	}
	// default container is RefArray
	typedef RefArray<AnimationRT> ContainerType;
};

static BankArray<AnimationRT> AnimationRTBank;
static BankArray<WeightInfo> WeigthBank;


MotionPath::~MotionPath()
{
}

MotionType::MotionType()
{
	_entry = NULL;
	_skeleton = new Skeleton("");
}
MotionType::~MotionType()
{
}

void MotionType::AssignSkeleton( RStringB name )
{
	_skeleton = new Skeleton(name);
}

RStringB MotionType::GetMoveName( MoveId id ) const
{
	if (id==MoveIdNone) return RStringB("<none>");
	return _moveIds.GetName(id);
}

MoveId MotionType::GetMoveId( RStringB name ) const
{
	int value = _moveIds.GetValue(name);
	if (value<0 && name.GetLength()>0)
	{
		LogF("Warning: unknown value '%s'",(const char *)name);
	}
	return MoveId(value);
}

struct UsedContext
{
	AutoArray<bool> _indexUsed;
};

const MotionEdge &MotionType::Edge( MoveId a, MoveId b ) const
{
	// find edge in edge list
	Assert( a>=0 );	
	Assert( b>=0 );	
	const MotionEdges &edges=_vertex[a];
	for( int i=0; i<edges.Size(); i++ ) if( edges[i].target==b ) return edges[i];
	return NoEdge;
}

int MotionType::EdgeCost( MoveId a, MoveId b ) const
{
	const MotionEdge &edge=Edge(a,b);
	if( edge.cost<0 ) return INT_MAX;
	return edge.cost;
}

void MotionType::AddEdge(MoveId a, MoveId b, MotionEdgeType type, float cost)
{
	int iCost = toInt(cost*50);
	// resize matrix so that is contains both a and b
	// check if egde is already present
	MotionEdges &edges = _vertex[a];
	for( int i=0; i<edges.Size(); i++ )
	{
		MotionEdge &edge = edges[i];
		if( edge.target==b )
		{
			edge.type=type;
			edge.cost=iCost;
			return;
		}
	}
	edges.Add(MotionEdge(b,iCost,type));
}

void MotionType::DeleteEdge( MoveId a, MoveId b )
{
	MotionEdges &edges = _vertex[a];
	for( int i=0; i<edges.Size(); i++ )
	{
		MotionEdge &edge = edges[i];
		if( edge.target==b )
		{
			edges.Delete(i);
			return;
		}
	}
}

#define LOG_PATH 0

bool MotionType::FindPath( MotionPath &path, MoveId from, MotionPathItem to) const
{
	//LogF("State search from %s to %s",NAME(from),NAME(to.id));
	// check direct connection
	const MotionEdge &edge = Edge(from,to.id);
	if ((MotionEdgeType)edge.type!=MEdgeNone)
	{
		// if direct coonection is found, use it and do not search for shortest path

		path.Resize(0);

		// finished: check if we have some path found
		path.Add(to);
		#if LOG_PATH
		LogF("Direct path from %s to %s",NAME(from),NAME(to.id));
		LogF("  insert %s, cost %d",NAME(to.id),edge.cost);
		#endif
		return true;
	}


	path.Resize(0);
	// perform Dijkstra's search

	// see very nice description at: http://www.ee.uwa.edu.au/~plsd210/ds/dijkstra.html

	// TODO: use heap array for vs (need fast extract min)
	/*
	// static storage
	int vsStorage[1024];
	MemAllocSA vsAlloc;
	vsAlloc.Init(vsStorage,sizeof(vsStorage);
	HeapArray<int,MemAllocSA> vs;
	vs.SetStorage(vsAlloc);
	*/

	AUTO_STATIC_ARRAY(int,vs,1024);
	AUTO_STATIC_ARRAY(int,d,1024);
	AUTO_STATIC_ARRAY(int,p,1024);
	int nVerts=_vertex.Size();
	d.Resize(nVerts);
	p.Resize(nVerts);
	for( int i=0; i<nVerts; i++ ) d[i]=INT_MAX,p[i]=-1; // no paths yet
	// add all vertices except source
	//for( int i=0; i<nVerts; i++ ) if( i!=fromI ) vs.Add(i);
	for( int i=0; i<nVerts; i++ )
	{
		// only add vertices that have some edges connected to them
		//if (_vertex[i].Size()<=0) continue;
		vs.Add(i);
	}
	// source has trivial solution
	d[from]=0;
	const int unaccessible=INT_MAX/16;

	for(;;)
	{
		// extract cheapest: may be faster using heap sort, but we do not care
		int minI=-1,minVal=unaccessible;
		for( int i=0; i<vs.Size(); i++ )
		{
			int index=vs[i];
			int val=d[index];
			if( minVal>val ) minVal=val,minI=i;
		}
		if( minI<0 ) break; // nothing to add
		int v=vs[minI];
		vs.Delete(minI); // extraction
		//s.Add(v); // consider: do we need to maintain s?
		int vCost=d[v];
		// relax all adjacent vertices
		const MotionEdges &e=_vertex[v];
		for( int i=0; i<e.Size(); i++ )
		{
			const MotionEdge &ei=e[i];
			int target=ei.target;
			if( target<0 ) continue;
			int costToTarget=vCost+ei.cost;
			if( costToTarget<d[target] ) d[target]=costToTarget,p[target]=v;
		}
	}

	if( p[to.id]<0 )
	{
		//LogF("no path from %s to %s",NAME(from),NAME(to.id));
		return false;
	}

	// finished: check if we have some path found
	path.Add(to);
	#if LOG_PATH
	LogF("State search from %s to %s",NAME(from),NAME(to.id));
	LogF("  insert %s, cost %d",NAME(to.id),d[to.id]);
	#endif
	for(;;)
	{
		Assert( p[to.id]>=0 && p[to.id]<_moveIds.FirstInvalidValue() );
		to = MotionPathItem((MoveId)p[to.id]);
		#if !_RELEASE
		if( to.id<0 )
		{
			Fail("Dijkstra buggy");
			return false;
		}
		#endif
		if( to.id==from )
		{
			#if LOG_PATH
			LogF("  found %s, cost %d",NAME(from),d[from]);
			#endif
			return true;
		}
		path.Insert(0, to);
		#if LOG_PATH
		LogF("  insert %s, cost %d",NAME(to.id),d[to.id]);
		#endif
	}
	/*NOTREACHED*/
}

/*!
 mapping of action names to action ids
\patch 1.01 Date 6/13/2001 by Ondra
 - Introduced to enable creating add-ons without requiring modifing exe.
\patch_internal 1.01 Date 6/13/2001 by Ondra
 - Many changes to Transport, Person and Man classes were necessary
*/

ActionVehMap::ActionVehMap()
{
}

/*!
 mapping of action names to action ids
*/
void ActionVehMap::Load(const MotionType *motion, const ParamEntry &map)
{
	RString prefix = "ManAct";
	int nAct = GActionVehNames.FirstInvalidValue();
	_actionMoves.Realloc(nAct);
	_actionMoves.Resize(nAct);
	for (int id=0; id<nAct; id++)
	{
		_actionMoves[id] = MoveIdNone;
	}

	for (int i=0; i<map.GetEntryCount(); i++)
	{
		const ParamEntry &entry = map.GetEntry(i);
		RStringB name = entry.GetName();
		RStringB value = entry;
		RString fullName = prefix+name;
		const char *str = fullName;
		int idInt = GActionVehNames.GetValue(str);
		if (idInt<0)
		{
			RptF("Bad action %s in %s",(const char *)str,(const char *)map.GetName());
			continue;
		}
		ManVehAction id = (ManVehAction)idInt;
		Assert(_actionMoves.Size()>id);
		//_actionMoves.Access(id);
		_actionMoves[id] = motion->GetMoveId(value);
		if (_actionMoves[id]==MoveIdNone)
		{
			RptF
			(
				"Invalid move %s in action %s (%s)",
				(const char *)value,
				(const char *)name,(const char *)map.GetContext()
			);
		}
	}
}


void InitMan()
{
	const ParamEntry &map = Pars>>"CfgVehicleActions";
	RString prefix = "ManAct";
	for (int i=0; i<map.GetEntryCount(); i++)
	{
		const ParamEntry &entry = map.GetEntry(i);
		RStringB name = entry.GetName();
		GActionVehNames.AddValue(prefix+name);
	}
}

ActionMap *MotionType::NewActionMap( const ParamEntry *cfg )
{
	ActionMapName name;
	name.motion = this;
	name.entry = cfg;
	return _actionMaps.New(name);
}


BlendAnimType *MotionType::NewBlendAnimType(const ParamEntry &cfg)
{
	BlendAnimTypeName name;
	name.motion = this;
	name.cfg = &cfg;
	return _blendAnimTypes.New(name);
}


void MotionType::InitNoActions(const ParamEntry *cfg)
{
	_noActions = NewActionMap(cfg);
}

static int GetUpDegree( CombatMode cm, bool handGun )
{
	if (cm==CMSafe || cm==CMCareless) return ManPosStand;
	if (handGun) return ManPosHandGunStand;
	return ManPosCombat;
}


MoveId MotionType::GetDefaultMove( int upDegree ) const
{
	// search for any ActionMap that correspons to upDegree
	for (int i=0; i<_actionMaps.Size(); i++)
	{
		const ActionMap *map = _actionMaps[i];
		if (map->GetUpDegree()==upDegree)
		{
			return map->GetAction(ManActDefault);
		}
	}
	return MoveIdNone;
}

MoveId MotionType::GetMove(int upDegree, ManAction action) const
{
	// search for any ActionMap that correspons to upDegree
	for (int i=0; i<_actionMaps.Size(); i++)
	{
		const ActionMap *map = _actionMaps[i];
		if (map->GetUpDegree() == upDegree)
		{
			return map->GetAction(action);
		}
	}
	return MoveIdNone;
}

static void CheckArrayMod
(
	const ParamEntry &entry, int mod, const ParamEntry *context=NULL
)
{
	if (entry.GetSize()%mod!=0)
	{
		RptF
		(
			"%s: item count not multiple of %d (is %d)",
			context ? (const char *)context->GetContext(entry.GetName())
			: (const char *)entry.GetName(),
			mod,entry.GetSize()
		);
	}
}


static void BadMove
(
	const ParamEntry &entry, const char *name, const ParamEntry *context=NULL
)
{
	RptF
	(
		"  %s: Bad move %s",
		context ? (const char *)context->GetContext(entry.GetName())
		: (const char *)entry.GetName(),
		(const char *)name
	);
}

void MotionType::Load( const ParamEntry &entry )
{
	/*
	LogF
	(
		"Load motion type %s",
		(const char *)entry.GetName()
	);
	*/
	// assign skeleton
	_skeleton = Skeletons.New(entry.GetName());
	_entry = &entry;
	const ParamEntry &states = entry>>"States";

	// add all entries of class entry
	_moveIds.Clear();
	for (int i=0; i<states.GetEntryCount(); i++ )
	{
		const ParamEntry &sub = states.GetEntry(i);
		_moveIds.AddValue(sub.GetName());
	}
	_moveIds.Close();

	// resize MotionType to contain all moves
	int moveIdN = MoveIdN();
	_vertex.Realloc(moveIdN);
	_vertex.Resize(moveIdN);

	//LogF("Load motion matrix");

	// load interpolations information
	const ParamEntry &inter = entry>>"Interpolations";
	for( int i=0; i<inter.GetEntryCount(); i++ )
	{
		const ParamEntry &group = inter.GetEntry(i);
		float fCost = group[0];
		// insert all possible pairs of states
		for( int s=1; s<group.GetSize(); s++ )
		{
			RStringB sName = group[s];
			MoveId sMove = GetMoveId(sName);
			if( sMove==MoveIdNone ) continue;

			for( int r=1; r<group.GetSize(); r++ ) if( r!=s )
			{
				RStringB rName = group[r];
				MoveId rMove = GetMoveId(rName);
				if( rMove==MoveIdNone ) continue;

				AddEdge( rMove,sMove, MEdgeInterpol, fCost );
			}
		}		
	}

	{ // interpolated transitions
		const ParamEntry &trans = entry>>"transitionsInterpolated";
		CheckArrayMod(trans,3,&entry);
		for( int i=0; i<trans.GetSize(); i+=3 )
		{
			RStringB a = trans[i];
			RStringB b = trans[i+1];
			float fCost = trans[i+2];
			MoveId aId = GetMoveId(a);
			MoveId bId = GetMoveId(b);
			if( aId==MoveIdNone || bId==MoveIdNone )
			{
				RptF("Bad ipol transition from %s to %s",(const char *)a,(const char *)b);
				continue;
			}
			// get move id
			AddEdge( aId,bId,MEdgeInterpol,fCost );
		}
	}
	{ // simple transitions
		const ParamEntry &trans = entry>>"transitionsSimple";

		CheckArrayMod(trans,3,&entry);
		for( int i=0; i<trans.GetSize(); i+=3 )
		{
			RStringB a = trans[i];
			RStringB b = trans[i+1];
			float fCost = trans[i+2];
			MoveId aId = GetMoveId(a);
			MoveId bId = GetMoveId(b);
			if( aId==MoveIdNone || bId==MoveIdNone )
			{
				RptF("Bad simple transition from %s to %s",(const char *)a,(const char *)b);
				continue;
			}
			// get move id
			AddEdge( aId,bId,MEdgeSimple,fCost );
		}
	}

	// scan all states for inline connection information
	for (int i=0; i<moveIdN; i++)
	{
		// get state entry
		const ParamEntry &state = states.GetEntry(i);
		// get connectFrom
		const ParamEntry &conFrom = state>>"connectFrom";
		CheckArrayMod(conFrom,2,&state);
		for (int s=0; s<conFrom.GetSize()-1; s+=2)
		{
			RStringB name = conFrom[s];
			float cost = conFrom[s+1];
			MoveId id = GetMoveId(name);
			if (id==MoveIdNone)
			{
				BadMove(conFrom,name,&state);
			}
			else
			{
				AddEdge( id, (MoveId)i, MEdgeSimple, cost );
			}
		}
		const ParamEntry &conTo = state>>"connectTo";
		CheckArrayMod(conTo,2,&state);
		for (int s=0; s<conTo.GetSize()-1; s+=2)
		{
			RStringB name = conTo[s];
			float cost = conTo[s+1];
			MoveId id = GetMoveId(name);
			if (id==MoveIdNone)
			{
				BadMove(conTo,name,&state);
			}
			else
			{
				AddEdge( (MoveId)i, id, MEdgeSimple, cost );
			}
		}

		const ParamEntry &ipolW = state>>"interpolateWith";
		CheckArrayMod(ipolW,2,&state);
		for (int s=0; s<ipolW.GetSize()-1; s+=2)
		{
			RStringB name = ipolW[s];
			float cost = ipolW[s+1];
			MoveId id = GetMoveId(name);
			if (id==MoveIdNone)
			{
				BadMove(ipolW,name,&state);
			}
			else
			{
				AddEdge( (MoveId)i, id, MEdgeInterpol, cost );
				AddEdge( id, (MoveId)i, MEdgeInterpol, cost );
			}
		}

		const ParamEntry &ipolT = state>>"interpolateTo";
		CheckArrayMod(ipolT,2,&state);
		for (int s=0; s<ipolT.GetSize()-1; s+=2)
		{
			RStringB name = ipolT[s];
			float cost = ipolT[s+1];
			MoveId id = GetMoveId(name);
			if (id==MoveIdNone)
			{
				BadMove(ipolT,name,&state);
			}
			else
			{
				AddEdge( (MoveId)i, id, MEdgeInterpol, cost );
			}
		}

		const ParamEntry &ipolF = state>>"interpolateFrom";
		CheckArrayMod(ipolF,2,&state);
		for (int s=0; s<ipolF.GetSize()-1; s+=2)
		{
			RStringB name = ipolF[s];
			float cost = ipolF[s+1];
			MoveId id = GetMoveId(name);
			if (id==MoveIdNone)
			{
				BadMove(ipolF,name,&state);
			}
			else
			{
				AddEdge( id, (MoveId)i, MEdgeInterpol, cost );
			}
		}

	}

	// scan all states for equivalency information
	for (int i=0; i<moveIdN; i++)
	{
		const ParamEntry &state = states.GetEntry(i);
		const RStringB &asState = state>>"connectAs";
		if (asState.GetLength()<=0) continue;
		// use all connection information from as
		MoveId as = GetMoveId(asState);
		if (as==MoveIdNone) continue;
		// scan line
		for (int j=0; j<moveIdN; j++)
		{
			const MotionEdge &sEdge = Edge(as,(MoveId)j);
			if ((MotionEdgeType)sEdge.type==MEdgeNone) continue;
			// check if this egde already exists
			const MotionEdge &tEdge = Edge((MoveId)i,(MoveId)j);
			if ((MotionEdgeType)tEdge.type!=MEdgeNone) continue;
			// take edge from as state
			AddEdge((MoveId)i,(MoveId)j,sEdge.type,sEdge.GetCost());
		}
		// scan columnt
		for (int j=0; j<moveIdN; j++)
		{
			const MotionEdge &sEdge = Edge((MoveId)j,as);
			if ((MotionEdgeType)sEdge.type==MEdgeNone) continue;
			// check if this egde already exists
			const MotionEdge &tEdge = Edge((MoveId)j,(MoveId)i);
			if ((MotionEdgeType)tEdge.type!=MEdgeNone) continue;
			// take edge from as state
			AddEdge((MoveId)j,(MoveId)i,sEdge.type,sEdge.GetCost());
		}
	}

	{ // disabled transitions
		const ParamEntry &trans = entry>>"transitionsDisabled";
		CheckArrayMod(trans,2,&entry);

		for( int i=0; i<trans.GetSize(); i+=2 )
		{
			RStringB a = trans[i];
			RStringB b = trans[i+1];
			MoveId aId = GetMoveId(a);
			MoveId bId = GetMoveId(b);
			if( aId==MoveIdNone || bId==MoveIdNone )
			{
				RptF("Bad disabled transition from %s to %s",(const char *)a,(const char *)b);
				continue;
			}
			// get move id
			DeleteEdge( aId,bId );
		}
	}

	// compact memory
	_vertex.Compact();
	for (int i=0; i<_vertex.Size(); i++)
	{
		_vertex[i].Compact();
	}

	#if 0
		FILE *matrix = fopen("matrix.txt","w");
		if (matrix)
		{
			for (int i=0; i<_vertex.Size(); i++)
			{
				const MotionEdges &edges = _vertex[i];
				for (int j=0; j<edges.Size(); j++)
				{
					const MotionEdge &edge = edges[j];
					fprintf
					(
						matrix,"%-20s to %-20s: %d, %.3f\n",
						NAME(MoveId(i)),NAME(MoveId(edge.target)),
						edge.type,edge.GetCost()
					);
				}
			}
			fclose(matrix);
		}
	#endif

	RStringB vehicleActionsName = entry>>"vehicleActions";
	_actionVehMap.Load(this,Pars>>vehicleActionsName);
}

void MotionType::Unload()
{
	_moveIds.Clear();
	//_actionMaps.Clear();
	_vertex.Clear();
	_entry = NULL;
}


// class Man

//const float TimeToRun=3;
//const float TimeToCrawl=5;
const float TimeToRun=6;
const float TimeToCrawl=8;
const float WaitBeforeStandUp=TimeToRun+TimeToCrawl;

int Man::GetActUpDegree() const
{
	// helper function
	ActionMap *map = Type()->GetActionMap(_primaryMove.id);
	return map ? map->GetUpDegree() : ManPosStand;
}

DEFINE_CASTING(Man)

Man::Man(VehicleType *name,bool fullCreate)
:Person(name,fullCreate),

//_animationTransform(MIdentity),
_turnToDo(0),
_walkToggle(false),
_inBuilding(false),

_doSoundStep(false),
_freeFallUntil(Glob.time-60),
_stillMoveQueueEnd(MoveIdNone),
_variantTime(Glob.time),

//_launchDelay(3),

_aimInaccuracyX(0),_aimInaccuracyY(0),
_aimInaccuracyDist(0),
_lastInaccuracyTime(Glob.time),
_lastInaccuracyDistTime(Glob.time),

_lastObjectContactTime(Glob.time),
_lastMovementTime(Glob.time),

_waterDepth(0),
_unitPos(UPAuto),

_hideBody(0), _hideBodyWanted(0), // hide body state

_primaryMove((MoveId)MoveIdNone),
_secondaryMove((MoveId)MoveIdNone),
_externalMove((MoveId)MoveIdNone),
_externalMoveFinished(false),
_forceMove((MoveId)MoveIdNone),
_hasNVG(false),

_upDegreeChangeTime(Glob.time-5),
_upDegreeStable(ManPosStand),

_posWanted(ManPosStand),
_posWantedTime(TIME_MAX), // no change waiting

_primaryFactor(1),
_primaryTime(0),_secondaryTime(0),
//_aimingRigid(1),

//_rndSpeed(1),
//_walkSpeed(0),
_walkSpeedWanted(0),

_aimingPositionWorld(VZero),
_cameraPositionWorld(VZero),

_whenKilled(0),
_whenScreamed(TIME_MAX),

_tired(0),_canMoveFast(true),

_showPrimaryWeapon(true),
_showSecondaryWeapon(true),
_showHead(true),
_manScale(1),

_nvg(false),

_gunTrans(MIdentity),
_headTrans(MIdentity),
_headTransIdent(true),_gunTransIdent(true),

_correctBankSin(0),
_correctBankCos(1),
_legTrans(MIdentity),

// turret controls
_gunYRot(0),_gunYRotWanted(0),
_gunXRot(0),_gunXRotWanted(0),
_gunXSpeed(0),_gunYSpeed(0),

_headYRot(0),_headYRotWanted(0),
_headXRot(0),_headXRotWanted(0),
//_headXSpeed(0),_headYSpeed(0),

_lookForwardTimeLeft(0),
_lookTargetTimeLeft(0),

//_sideSpeed(0),_sideSpeedWanted(0),

// initialize ladder state
_ladderBuilding(NULL),
_ladderIndex(-1),
_ladderPosition(0), // 0..1 - position on ladder, 0 = bottom

_handGun(false),

_turnWanted(0),
_head(Type()->_head, _shape)
{
	SetSimulationPrecision(1.0f/5); // 5 times per sec is enough
	RecalcGunTransform();

	_destrType=DestructMan;

	_mGunClouds.Load((*Type()->_par) >> "MGunClouds");
	_gunClouds.Load((*Type()->_par) >> "GunClouds");
	_mGunFireFrames = 0;
	_mGunFireTime = UITIME_MIN;
	_mGunFirePhase = 0;

	//_manScale = GRandGen.PlusMinus(1,0.05f);
	//_manScale = GRandGen.PlusMinus(1,0.9f);

	//SetScale(_manScale);
	_head.SetMimicMode("neutral");
	ScanNVG();
}

Man::~Man()
{
}

#pragma warning(disable:4065)

int Man::InsideLOD( CameraType camType ) const
{
	return Type()->_insideView;
}

void Man::BlendMatrix(Matrix4 &mat,const Matrix4 &trans,float factor) const
{
	if (factor>=0.99f)
	{
		mat = trans * mat;
	}
	else if (factor>=0.01f)
	{
		// interpolation necessary
		mat = (trans*factor + MIdentity*(1-factor))*mat;
	}

}

void Man::AnimateMatrix( Matrix4 &mat, const AnimationRTWeight &wgt ) const
{
	const ManType *type=Type();

	// search in any animation RT

	Matrix4 res = MZero;
	if( _primaryFactor>0.01f )
	{
		res = mat;
		AnimationRT *anim=type->GetAnimation(_primaryMove.id);
		if (anim)
		{
			anim->Matrix(res,_primaryTime,wgt);
			// TODO: operator *=
		}
		res = res*_primaryFactor;
	}
	if( _primaryFactor<0.99f )
	{
		// check for simple cases
		AnimationRT *anim=type->GetAnimation(_secondaryMove.id);
		Matrix4 temp = mat;
		if (anim)
		{
			anim->Matrix(temp,_secondaryTime,wgt);
		}
		res += temp*(1-_primaryFactor);
	}
	res.Orthogonalize();
	mat = res;
}

const AnimationRTWeight &Man::GetSelWeights( int level, int selection ) const
{
	const ManType *type=Type();
	const WeightInfo &weights = type->GetWeights();
	const AnimationRTWeights &wgt = weights[level];

	// check which matrix
	Shape *sShape = _shape->Level(level);
	const NamedSelection &sel = sShape->NamedSel(selection);
	int point = sel[0];
	Assert( sel.Size()>0 );
	return wgt[point];
}

const AnimationRTWeight &Man::GetProxyWeights( int level, const ProxyObject &proxy ) const
{
	return GetSelWeights(level,proxy.selection);
}

#define BLEND_ANIM(name) \
	BlendAnimSelectionsStorage name##Storage; \
	BlendAnimSelections name; \
	AUTO_STORAGE_ALIGNED(BlendAnimInfo,name##Memory,32,int); \
	name##Storage.Init(name##Memory,sizeof(name##Memory)); \
	name.SetStorage(name##Storage);


/*!
\patch_internal 1.01 Date 6/26/2001 by Ondra. Fixed: animate matrix connected with head.
- this was required because of drawing Night Vision.
*/
void Man::AnimateMatrix( Matrix4 &mat, int level, int selection ) const
{
	const AnimationRTWeight &wg = GetSelWeights(level,selection);

	// apply MC animation
	AnimateMatrix(mat,wg);

	// now we have weighting information
	// check primary selection - to know if we are in aiming selection
	int matIndex = wg[0].GetSel();

	if (!_gunTransIdent)
	{
		BLEND_ANIM(aiming);
		const BlendAnimSelections &aimingRes = GetAiming(aiming);

		float aimingFactor = 0;
		for ( int bi = 0; bi<aimingRes.Size(); bi++)
		{
			const BlendAnimInfo &blend = aimingRes[bi];
			if (matIndex==blend.matrixIndex) {aimingFactor=blend.factor;break;}
		}

		// check matrix aiming factor
		BlendMatrix(mat,_gunTrans,aimingFactor);
	}

	// note: legs are ignored during matrix animation
	// no known matrix is influenced by legs
	/*
	{
		BLEND_ANIM(legs);
		const BlendAnimSelections &legsRes = GetLegs(legs);

		float legsFactor = 0;
		for ( int bi = 0; bi<legsRes.Size(); bi++)
		{
			const BlendAnimInfo &blend = legsRes[bi];
			if (matIndex==blend.matrixIndex) {legsFactor=blend.factor;break;}
		}

		BlendMatrix(mat,_legTrans,legsFactor);
	}
	*/

	if (!_headTransIdent)
	{	
		BLEND_ANIM(head);
		const BlendAnimSelections &headRes = GetHead(head);

		float headFactor = 0;
		for ( int bi = 0; bi<headRes.Size(); bi++)
		{
			const BlendAnimInfo &blend = headRes[bi];
			if (matIndex==blend.matrixIndex) {headFactor=blend.factor;break;}
		}

		// check matrix aiming factor
		BlendMatrix(mat,_headTrans,headFactor);
	}
}


Vector3 Man::COMPosition() const
{
	// COM is animated, it si somewhere near aiming position
	return AimingPosition();
}

Vector3 Man::AnimatePoint( int level, int index ) const
{
	const ManType *type=Type();
	Vector3 res=VZero;
	const WeightInfo &rtw = type->GetWeights();
	if( _primaryFactor>0.01f )
	{
		AnimationRT *anim=type->GetAnimation(_primaryMove.id);
		if (anim)
		{
			res = anim->Point(rtw,_shape,level,_primaryTime,index)*_primaryFactor;
		}
	}
	if( _primaryFactor<0.99f )
	{
		// check for simple cases
		AnimationRT *anim=type->GetAnimation(_secondaryMove.id);
		if (anim)
		{
			res += anim->Point(rtw,_shape,level,_secondaryTime,index)*(1-_primaryFactor);
		}
	}

	if (type->_aimingAxisPoint>=0 && !_gunTransIdent)
	{
		BLEND_ANIM(aiming);
		const BlendAnimSelections &aimingRes = GetAiming(aiming);
		AnimationRT::TransformPoint
		(
			res,rtw[level],_shape->Level(level),_gunTrans,
			aimingRes.Data(),aimingRes.Size(),index
		);
	}

	/*
	if (type->_headAxisPoint>=0 && !_headTransIdent)
	{
		BLEND_ANIM(head);
		GetAiming(head);
		AnimationRT::TransformPoint
		(
			res,rtw[level],_shape->Level(level),_headTrans,
			head.Data(),head.Size(),index
		);
	}
	*/

	// combine with transformation of res by legs
	BLEND_ANIM(legs);
	const BlendAnimSelections &legsRes = GetLegs(legs);
	AnimationRT::TransformPoint
	(
		res,rtw[level],_shape->Level(level),_legTrans,
		legsRes.Data(),legsRes.Size(),index
	);

	return res;
}

bool Man::HasFlares( CameraType camType ) const
{
	// check current weapon
	if (camType==CamGunner)
	{
		// check current weapon
		const MagazineSlot &slot = GetMagazineSlot(_currentWeapon);
		return slot._muzzle->_opticsFlare;
	}
	return base::HasFlares(camType);
}


Matrix4 Man::InsideCamera( CameraType camType ) const
{
	int level=_shape->FindMemoryLevel();

	//if (level==LOD_INVISIBLE) return MIdentity;

	if( camType==CamGunner )
	{
		return GetWeaponRelTransform(_currentWeapon);
	}

	//Matrix3 relDir(MRotationZ,-_correctBank);
	int selIndex=Type()->_pilotPoint;
	if( selIndex<0 ) return MIdentity;

	const Selection &sel=_shape->LevelOpaque(level)->NamedSel(selIndex);
	Vector3Val pilotPos=AnimatePoint(level,sel[0]);
	Matrix4 transform;

	//Vector3 relUp = DirectionWorldToModel(VUp);
	Vector3 relUp = WorldToModel().DirectionUp();

	transform.SetDirectionAndUp(VForward,relUp);
	//transform.SetOrientation(M3Identity);
	transform.SetPosition(pilotPos);
	// calculate recoil camera angle
	if (_recoil)
	{
		_recoil->ApplyRecoil(_recoilTime,transform,0.2f*_recoilFactor);
		// normalize transform
		transform.Orthogonalize();
	}
	return transform;
}

Vector3 Man::GetCameraDirection(CameraType cam) const
{
	// used only for 3rd persion camera (CamExternal)
	return Direction();
}

Vector3 Man::ExternalCameraPosition( CameraType camType ) const
{
	return Type()->_extCameraPosition;
}

Vector3 Man::GetSpeakerPosition() const
{
	return CameraPosition();
}

bool Man::IsVirtual( CameraType camType ) const
{
	return true;
}
bool Man::IsVirtualX( CameraType camType ) const
{
	if (GInput.lookAroundEnabled) return true;
	return camType!=CamInternal && camType!=CamExternal;
}
bool Man::IsGunner( CameraType camType ) const
{
	return camType==CamGunner || camType==CamInternal || camType==CamExternal;
}

void Man::OverrideCursor
(
	CameraType camType, Vector3 &dir
) const
{
	#if 0
	if (!QIsManual()) return;
	switch (camType)
	{
		case CamInternal:
		case CamGunner:
		case CamExternal:
			break;
		default:
			return;
	}
	// get cursor direction from weapon direction

	Vector3 weapon = GetEyeDirection(0);

	dir = weapon;
	#endif
}

void Man::LimitCursor
(
	CameraType camType, Vector3 &dir
) const
{
	if (!QIsManual()) return;
	if (GetCursorRelMode(camType)!=CKeyboard) return;

	switch (camType)
	{
		case CamInternal:
		case CamExternal:
		case CamGunner:
			break;
		default:
			return;
	}

	// calculate direction from gunWanted and inaccuracy


	if (!GInput.lookAroundEnabled)
	{
		float xRot = _gunXRotWanted;
		float yRot = _gunYRotWanted;
		saturate(yRot,Type()->_minGunTurn,Type()->_maxGunTurn);
		saturate(xRot,Type()->_minGunElev,Type()->_maxGunElev);
		Vector3 relDir =
		(
			Matrix3(MRotationY,yRot)*Matrix3(MRotationX,-xRot).Direction()
		);
		// apply _correctBank
		/*
		Matrix3 correct(MRotationZ,-_correctBank);
		relDir = correct*relDir;
		*/

		DirectionModelToWorld(dir,relDir);
	}
	else
	{
		/*
		xRot = _headXRotWanted;
		yRot = _headYRotWanted;
		*/
	}


}

bool Man::IsCommander( CameraType camType ) const {return true;}
bool Man::ShowAim( int weapon, CameraType camType ) const
{
	//return !QIsManual();
	if (WeaponsDisabled()) return false;
	if (_laserTargetOn) return true;
	if (!ShowWeaponAim()) return false;
	return camType!=CamGunner;
}
bool Man::ShowCursor( int weapon, CameraType camType ) const
{
	//return !QIsManual();
#if _ENABLE_CHEATS
	if (CHECK_DIAG(DECombat)) return true;
#endif
	return camType!=CamGunner;
}

void Man::InitVirtual( CameraType camType, float &heading, float &dive, float &fov ) const
{
	if (camType==CamExternal) camType=CamInternal;
	base::InitVirtual(camType,heading,dive,fov);
	switch( camType )
	{
		case CamGunner:
			fov=0.21f;
			dive=0;
		break;
	}
}

void Man::LimitVirtual( CameraType camType, float &heading, float &dive, float &fov ) const
{
	if (camType==CamExternal) camType=CamInternal;
	base::LimitVirtual(camType,heading,dive,fov);
	switch( camType )
	{
		case CamGunner:
		{
			// check current weapon
			int curWeapon = SelectedWeapon();
			if (curWeapon >= 0 && curWeapon < NMagazineSlots())
			{
				const MagazineSlot &slot = GetMagazineSlot(curWeapon);
				const MuzzleType *muzzle = slot._muzzle;
				saturate(fov, muzzle->_opticsZoomMin, muzzle->_opticsZoomMax);
			}
			else
			{
				saturate(fov,0.21f,0.21f);
			}
			saturate(heading,0,0);
			saturate(dive,-0.7f,+0.8f);

		}
		break;
		case CamInternal:
		break;
	}
}

const float WalkSpeed=1.5f;
const float WalkBackSpeed=1.5f;
const float RunSpeed=4.9f;
const float FastRunSpeed=7.2f;
const float TiredRunSpeed=4.9f;
const float InvFastRunSpeed=1/FastRunSpeed;
//const float InvWalkSpeed=1/WalkSpeed;
//const float InvWalkBackSpeed=1/WalkBackSpeed;
//const float InvRunSpeed=1/RunSpeed;

const float TurnSpeed=1.0f;
const float TurnBackSpeed=0.7f;

#define GRAD_0_SPEED 1.0f
#define GRAD_1_SPEED 1.0f
#define GRAD_2_SPEED 1.1f
#define GRAD_3_SPEED 1.2f
#define GRAD_4_SPEED 1.5f
#define GRAD_5_SPEED 2.0f
#define GRAD_6_SPEED 3.0f

static const float GradPenalty[7]=
{
	GRAD_0_SPEED,GRAD_1_SPEED,GRAD_2_SPEED,
	GRAD_3_SPEED,GRAD_4_SPEED,GRAD_5_SPEED,GRAD_6_SPEED
};
static const float InvGradPenalty[7]=
{
	1/GRAD_0_SPEED,1/GRAD_1_SPEED,1/GRAD_2_SPEED,
	1/GRAD_3_SPEED,1/GRAD_4_SPEED,1/GRAD_5_SPEED,1/GRAD_6_SPEED
};

inline float GradientPenalty( int grad )
{
	if (grad>=7) return 1e30f;
	return GradPenalty[grad];
}
inline float GradientMaxSpeed( int grad )
{
	if (grad>=7) return 0.1f;
	return InvGradPenalty[grad];
}

AnimationRT *MovesType::GetAnimation( MoveId move ) const
{
	if( move==MoveIdNone ) return NULL; // no animation
	return _moves[move];
}

ActionMap *MovesType::GetActionMap( MoveId move ) const
{
	if( move==MoveIdNone ) return NULL; // no animation
	return _moves[move].GetActionMap();
}

const MoveInfo *MovesType::GetMoveInfo( MoveId move ) const
{
	if( move==MoveIdNone ) return NULL; // no animation
	return &_moves[move];
}

MoveId MovesType::GetEquivalent( MoveId move ) const
{
	const MoveInfo *info = GetMoveInfo(move);
	if (!info) return MoveIdNone;
	MoveId id = info->GetEquivalentTo();
	if (id==MoveIdNone) return move;
	return id;
}


inline bool Man::IsDead() const
{
	return GetActUpDegree()==ManPosDead;
}

void Man::SetPrimaryMove(MotionPathItem item)
{
	int oldUD = GetActUpDegree();
	_primaryMove = item;

	int newUD = GetActUpDegree();
	if( oldUD!=newUD )
	{
		_upDegreeChangeTime = Glob.time;
	}
}

#define DIAG_QUEUE 0

void Man::RefreshMoveQueue(bool enableVariants)
{ // select random variants
	if (Glob.time>=_variantTime)
	{
		const MoveInfo *stillInfo = Type()->GetMoveInfo(_stillMoveQueueEnd);
		// we may select another move instead of this one
		// playing move long enough - select variant
		if (stillInfo)
		{
			MoveId id = stillInfo->GetEquivalentTo();
			if (enableVariants)
			{
				id = QIsManual() ? stillInfo->RandomVariantPlayer() : stillInfo->RandomVariantAI();
			}
			const MoveInfo *newInfo = Type()->GetMoveInfo(id);
			if (newInfo)
			{
				float wait = newInfo->GetVariantAfter();
				_variantTime = Glob.time+wait;
				MotionPathItem item(id);
				// TODO: keep same termination function
				// now we assume there is no termination function
				
				//LogF("%s: REFRESH refresh variant, wait %.2f",(const char *)GetDebugName(),wait);
				#if DIAG_QUEUE
				LogF
				(
					"%s: Switch to variant %s of %s for %.2f",
					DNAME(),NAME_T(Type(),id),NAME_T(Type(),_stillMoveQueueEnd),
					_variantTime-Glob.time
				);
				#endif
				ChangeMoveQueue(item);
				return;
			}
		}
		// no random variant - you wait inifitelly
		_variantTime = Glob.time+600;
	}
}

bool Man::ChangeMoveQueue(MotionPathItem item)
{
	#if DIAG_QUEUE
	LogF("ChangeMoveQueue %s",NAME_T(Type(),item.id));
	#endif

	// check for trivial case - soldier already performs required move
	if( item.id==_primaryMove.id )
	{
		#if DIAG_QUEUE
		LogF("%s: Reset queue %s",DNAME(),NAME_T(Type(),item.id));
		#endif
		// we are in target state
		if (item.context)
		{
			_primaryMove = item;
		}

		if (_queueMove.Size() > 0)
		{
			_queueMove.Resize(0);
			OnEvent(EEAnimChanged, GetCurrentMove());
		}
		return true;
	}


	const MoveInfo *info = Type()->GetMoveInfo(_primaryMove.id);
	if (info && info->GetTerminal())
	{
		// we are in terminal state - nowhere to change
		return false;
	}

	if (item.id==_secondaryMove.id && _primaryFactor<1 && !_primaryMove.context)
	{
		// revert primary / secondary
		// check if interpolation is possible
		const MotionEdge &edge = Type()->Edge(_secondaryMove.id,_primaryMove.id);
		if ((MotionEdgeType)edge.type==MEdgeInterpol)
		{
			swap(_secondaryMove,_primaryMove);
			swap(_secondaryTime,_primaryTime);
			_primaryFactor = 1-_primaryFactor;
			_queueMove.Resize(0);

			if (item.context)
			{
				_primaryMove.context = item.context;
			}
			OnEvent(EEAnimChanged, GetCurrentMove());
			return true;
		}
	}

	int qSize = _queueMove.Size();
	if( qSize>0 && _queueMove[qSize-1].id==item.id )
	{
		#if DIAG_QUEUE
		LogF("%s: Queue already valid - %s",DNAME(),NAME_T(Type(),item.id));
		#endif
		// current path is valid
		return true;
	}

	#if DIAG_QUEUE
	LogF("%s: Set queue %s",DNAME(),NAME_T(Type(),item.id));
	#endif

	// find shortest path from current primary move to target move
	if (_primaryMove.id==MoveIdNone) return false;

	bool ret = Type()->FindPath(_queueMove, _primaryMove.id, item);
	if (ret) OnEvent(EEAnimChanged, GetCurrentMove());
	return ret;
}

bool Man::SetMoveQueue(MotionPathItem item, bool enableVariants)
{
	#if 0 //_ENABLE_REPORT
		if (ToMoveOut() && GWorld->GetMode()==GModeNetware)
		{
			LogF
			(
				"%s: SetMoveQueue %s",
				(const char *)GetDebugName(),
				NAME_T(Type(),item.id)
			);
		}
	#endif

	#if DIAG_QUEUE>=2
	LogF("SetMoveQueue %s",NAME_T(Type(),item.id));
	#endif
	
	if( item.id==MoveIdNone )
	{
		// invalid move - movement not possible

		if (item.context)
		{
			ProcessMoveFunction(item.context);
		}

		#if DIAG_QUEUE
		LogF("Invalid move - no action");
		#endif
		_queueMove.Resize(0);
		return false;
	}
	MoveId equivItemId = Type()->_moveType->GetEquivalent(item.id);
	MoveId equivStillId = Type()->_moveType->GetEquivalent(_stillMoveQueueEnd);
	//if (_queueMove.Size()<=0) equivStillId = MoveIdNone;
	if( equivItemId!=equivStillId )
	{
		// equivalent primary move changed - mark it
		_stillMoveQueueEnd = item.id;
		const MoveInfo *info = Type()->GetMoveInfo(item.id);
		float wait = info->GetVariantAfter();
		_variantTime = Glob.time + wait;

		//LogF("%s: CHANGE refresh variant, wait %.2f",(const char *)GetDebugName(),wait);

		#if DIAG_QUEUE
		LogF
		(
			"%s: Eq move changed - %s (eq %s)",
			DNAME(),NAME_T(Type(),item.id),NAME_T(Type(),equivItemId)
		);
		#endif
	}
	else
	{
		#if DIAG_QUEUE>=2
		LogF
		(
			"%s: Same eq move changed - %s (eq %s)",
			DNAME(),NAME_T(Type(),item.id),NAME_T(Type(),_stillMoveQueueEnd)
		);
		#endif
	}
	// check time for which current equivalent animation is being played

	if (Glob.time>=_variantTime)
	{
		const MoveInfo *stillInfo = Type()->GetMoveInfo(_stillMoveQueueEnd);
		// we may select another move instead of this one
		// playing move long enough - select variant
		MoveId id = stillInfo->GetEquivalentTo();
		if (enableVariants)
		{
			id = QIsManual() ? stillInfo->RandomVariantPlayer() : stillInfo->RandomVariantAI();
		}
		if (id!=MoveIdNone)
		{
			const MoveInfo *newInfo = Type()->GetMoveInfo(id);

			float wait = newInfo->GetVariantAfter();

			_variantTime = Glob.time+wait;

			//LogF("%s: STILL refresh variant, wait %.2f",(const char *)GetDebugName(),wait);

			item.id = id;
			#if DIAG_QUEUE
			LogF
			(
				"%s: Switch to variant %s of %s for %.2f",
				DNAME(),NAME_T(Type(),id),NAME_T(Type(),equivItemId),
				_variantTime-Glob.time
			);
			#endif
		}
		else
		{
			// no random variant - you wait inifitelly
			_variantTime = Glob.time+600;
		}
	}
	else
	{
		if( equivItemId==equivStillId )
		{
			#if DIAG_QUEUE>=2
			LogF
			(
				"  equivalent state %s and %s",
				NAME_T(Type(),item.id),
				NAME_T(Type(),_stillMoveQueueEnd)
			);
			#endif
			// our target state is equivalent to wanted state - be happy with it
			// change target state function if necessary


			if (item.context)
			{
				if(_queueMove.Size()>0)
				{
					MotionPathItem &lastItem = _queueMove[_queueMove.Size()-1];
					lastItem.context = item.context;
				}
				else
				{
					if (!ChangeMoveQueue(item))
					{
						//_stillMoveQueueEnd = MoveIdNone;
					}
					return true;
				}
			}
			return true;
		}
	}
	bool ret = ChangeMoveQueue(item);
	// if (!ret) _stillMoveQueueEnd = MoveIdNone;
	return ret;
}



void Man::NextExternalQueue()
{
	if (_externalQueue.Size()>0)
	{
		_externalMove = _externalQueue[0];
		_externalMoveFinished= false;
		_externalQueue.Delete(0);
	}
}

void Man::AdvanceExternalQueue()
{
	if (_externalMoveFinished || _externalMove.id==MoveIdNone)
	{
		NextExternalQueue();
	}
}

static Object *DirectHitCheck( Man *man, Vector3Par beg, Vector3 &hit, const AmmoType *ammo)
{
	/*
	LODShapeWithShadow *collisionShape=Shapes.New("data3d\\colision.p3d",false,false);
	{
		Ref<Object> star=new ObjectColored(collisionShape,-1);
		star->SetTransform(M4Identity);
		star->SetPosition(beg);
		star->SetScale(0.4);
		star->SetConstantColor(PackedColor(Color(0,1,0)));
		GLandscape->AddObject(star);
	}

	{
		Ref<Object> star=new ObjectColored(collisionShape,-1);
		star->SetTransform(M4Identity);
		star->SetPosition(hit);
		star->SetScale(0.4);
		star->SetConstantColor(PackedColor(Color(1,1,0)));
		GLandscape->AddObject(star);
	}
	*/

	// check if there is some direct hit
	CollisionBuffer retVal;
	GLandscape->ObjectCollision(retVal,man,man,beg,hit,0);
	// seek first hit
	if (retVal.Size()<=0) return NULL;
	int minI = -1;
	float minT = 1e10;
	for (int i=0; i<retVal.Size(); i++)
	{
		if (minT>retVal[i].under)
		{
			minI = i;
			minT = retVal[i].under;
		}
	}
	const CollisionInfo &info = retVal[minI];
	// change hit position if necessary
	hit = info.object->PositionModelToTop(info.pos);
	/*
	LogF("Direct hit %s",(const char *)info.object->GetDebugName());

	Ref<Object> star=new ObjectColored(collisionShape,-1);
	star->SetTransform(M4Identity);
	star->SetPosition(hit);
	star->SetScale(0.4);
	star->SetConstantColor(PackedColor(Color(1,0,0)));
	GLandscape->AddObject(star);
	*/

	return info.object;
}

void Man::ThrowGrenadeAction(int weapon)
{
	if (GetNetworkManager().IsControlsPaused()) return;
	// depending on weapon this may be throw grenade or strike

	//Matrix4Val shootTrans=GunTransform();
	// it is hand grenade
	// start animation and throw in right moment
	TargetType *target = NULL; // unknown target
	const WeaponModeType *mode = GetWeaponMode(weapon);

	// check ammo simulation
	if (!mode) return;
	const AmmoType *ammo = mode->_ammo;
	if (!ammo) return;
	if (ammo->_simulation==AmmoShotStroke)
	{
		// perform ExplosionDammage
		// check for direct hit
		// indirect hit range is from 0.2 to 0.4
		float distance = ammo->indirectHitRange*5+0.1f;
		//LogF("Hit %s, dist %.2f",(const char *)ammo->GetName(),distance);
		Vector3Val begPos = AimingPosition();
		Vector3 hitPos = begPos+Direction()*distance;
		Object *directHit = DirectHitCheck(this,begPos,hitPos,ammo);
		GLandscape->ExplosionDammage(this,NULL,directHit,hitPos,Direction(),ammo);
		return;
	}
	/*
	Vector3 dir = VForward;
	if (QIsManual())
	{
		// manual: throw slightly up
		const static Vector3 upDir = Vector3(0,0.5,1).Normalized();
		dir = upDir;
	}
	*/
	bool fired=FireShell
	(
		weapon,
		GetWeaponPoint(weapon),
		GetWeaponRelDirection(weapon),
		//shootTrans.Rotate(dir),
		target
	);
	if( fired )
	{
		EntityAI::FireWeapon(weapon, target);
		// note: we may need to cancel forceFire request
		if (_forceFireWeapon==weapon)
		{
			_forceFireWeapon = -1;
		}
	}
}

void Man::ProcessMoveFunction( ActionContextBase *context )
{

	switch (context->function)
	{
	case MFGetIn:
		{
			AIUnit *unit = Brain();
			//LogF("ProcessGetIn %s",(const char *)unit->GetDebugName());
			ActionContextGetIn *ctx = static_cast<ActionContextGetIn *>(context);
			if (unit) unit->ProcessGetIn2(ctx->vehicle, ctx->position);
		}
		break;
	case MFReload:
		{
			ActionContextDefault *cd = static_cast<ActionContextDefault *>(context);
			int iMagazine = -1;
			for (int i=0; i<NMagazines(); i++)
			{
				const Magazine *mag = GetMagazine(i);
				if (!mag) continue;
				if
				(
					mag->_creator == cd->param &&
					mag->_id == cd->param2
				)
				{
					iMagazine = i;
					break;
				}
			}
			if (iMagazine < 0) break;

			const char *separator = strchr(cd->param3, '|');
			if (!separator) break;;
			int separatorPos = separator - cd->param3;
			RString weapon = cd->param3.Substring(0, separatorPos);
			RString muzzle = cd->param3.Substring(separatorPos + 1, INT_MAX);
			int iSlot = -1;
			for (int i=0; i<NMagazineSlots(); i++)
			{
				const MagazineSlot &slot = GetMagazineSlot(i);
				if (slot._weapon->GetName() == weapon && slot._muzzle->GetName() == muzzle)
				{
					iSlot = i;
					break;
				}
			}
			if (iSlot < 0) break;

			ReloadMagazineTimed(iSlot, iMagazine, true);
		}
		break;
	case MFThrowGrenade:
		{
			// actually fire weapon
			ActionContextDefault *cd = static_cast<ActionContextDefault *>(context);
			ThrowGrenadeAction(cd->param);
		}
		break;
	case MFUIAction:
		{
			// actually fire weapon
			ActionContextUIAction *cd = static_cast<ActionContextUIAction *>(context);
			ProcessUIAction(cd->action);
		}
		break;
	case MFDead:
#if _ENABLE_DATADISC
		bool NoBlood();
		if (!NoBlood() && Glob.config.blood && GRandGen.RandomValue() <= 0.3f)
		{
			// create slop of blood
			LODShapeWithShadow *shape = GLOB_SCENE->Preloaded(SlopBlood);
			float azimut = GRandGen.RandomValue() * H_PI * 2;

			Matrix4 transform(MIdentity);
			transform.SetOrientation(Matrix3(MRotationY, azimut));

			Vector3Val pos = Position();
			float surfY = GLandscape->RoadSurfaceY(pos+VUp*0.5f);
			Vector3 offset = transform.Rotate(shape->BoundingCenter());
			transform.SetPosition(Vector3(pos[0], surfY, pos[2]) + offset);

			// create vehicle
			float ttl = 180.0f + 420.0f * GRandGen.RandomValue();
			Slop *slop = new Slop(shape, VehicleTypes.New("slop"), transform, ttl);
			slop->SetTransform(transform);
			slop->SetAlpha(0.7f);
			GWorld->AddAnimal(slop);
		}
#endif
		break;
	}
}

/*!
\patch_interal 1.90 Date 11/4/2002 by Ondra
- Fixed: Footstep marks reintroduced (Missing since April 2000)
*/

bool Man::AdvanceMoveQueue
(
	float deltaT, float adjustSpeed, float &moveX, float &moveZ,
	SimulationImportance prec
)
{
	// return true if position of any bone has changed
	bool change = false;
	bool finished = false;
	// return value: animation phase change

	// get speed from primary and secondary (interpolation)
	const MoveInfo *prim = Type()->GetMoveInfo(_primaryMove.id);
	#if 0
	LogF("%s AdvanceMoveQueue",DNAME());
	#endif
	if (prim)
	{
		const AnimationRT *primA = *prim;
		float priSpeed = prim->GetSpeed();
		if (priSpeed>1e5)
		{
			// static looped animation
			// no sound
			// no movement
			_primaryTime=0;
			finished = true;
		}
		else
		{
			change = true;

			float priPhaseChng = deltaT*priSpeed*adjustSpeed;
			float oldPrimTime = _primaryTime;
			_primaryTime += priPhaseChng;

			if( _primaryTime>=1 )
			{
				// check if move is looped
				finished = true;
				if( primA->GetLooped() )
				{
					_primaryTime-=1;
					// _moveTime should be < 1, if not it's moving too fast
					if( _primaryTime>=1 ) _primaryTime=0.5f;
				}
				else
				{
					// non-looped - stay at the end of the animation
					priPhaseChng -= _primaryTime-1;
					_primaryTime=1;
				}
			}

			float moveFactor = priPhaseChng*_primaryFactor;

			moveZ -= moveFactor * primA->GetStepLength();
			moveX -= moveFactor * primA->GetStepLengthX();
			#if 0
			LogF
			(
				"  pri %20s: deltaT %.3f, priSpeed %.3f, priPhaseChng %.3f",
				NAME_T(Type(),_primaryMove),
				deltaT,priSpeed,priPhaseChng
			);
			#endif

			// set step sound
			if (prim->GetSoundEnabled())
			{
				// avoid sound when stand
				float edge1Time = prim->GetSoundEdge1();
				float edge2Time = prim->GetSoundEdge2();
				bool step = false;
				bool left = false;
				if (oldPrimTime<edge1Time && oldPrimTime+priPhaseChng>=edge1Time)
				{
					_doSoundStep = true;
					step = true;
					left = true;
					_soundStepOverride = prim->GetSoundOverride();
				}
				else if (oldPrimTime<edge2Time && oldPrimTime+priPhaseChng>=edge2Time)
				{
					_doSoundStep = true;
					step = true;
					_soundStepOverride = prim->GetSoundOverride();
				}
				if (step && EnableVisualEffects(prec) && ValueTest(&MoveInfo::OnLand)<0.5f)
				{
					int index=( left ? Type()->_stepLIndex : Type()->_stepRIndex );
					// check foot position
					if( index>=0 )
					{
						Vector3 mPos=AnimatePoint(_shape->FindMemoryLevel(),index);
						Vector3 pos=PositionModelToWorld(mPos);
						// leave stepmark
						float timeToLive=10;
						//timeToLive*=0.3;
						//Ref<Mark> mark=new Mark(shape,0.15,timeToLive);
						float ys = GLandscape->SurfaceY(pos.X(),pos.Z());
						float yr = GLandscape->RoadSurfaceYAboveWater(pos);
						if (yr<=ys+0.2f)
						{
							LODShapeWithShadow *shape=GScene->Preloaded(left ? FootStepL : FootStepR);
							Ref<Mark> mark=new Mark(shape,0.25f,timeToLive);
							mark->SetDirectionAndUp(Direction(),VUp);
							mark->SetPosition(pos);
							GWorld->AddAnimal(mark);
						}
					}
				}
				//GlobalShowMessage
				//(
				//	100,"time %.3f, edges %.3f,%.3f",
				//	oldPrimTime,edge1Time,edge2Time
				//);
			}
		}
	}

	if( _primaryFactor<0.99f )
	{
		const MoveInfo *sec = Type()->GetMoveInfo(_secondaryMove.id);
		if (sec)
		{
			const AnimationRT *secA = *sec;
			float secSpeed = sec->GetSpeed();

			if (secSpeed>1e5)
			{
				// simple case: static animation
				_secondaryTime=0;
			}
			else
			{
				change = true;

				float secPhaseChng = deltaT*secSpeed*adjustSpeed;

				_secondaryTime += secPhaseChng;

				if( _secondaryTime>=1 )
				{
					// check if move is looped
					if( secA->GetLooped() )
					{
						_secondaryTime-=1;
						// _moveTime should be < 1, if not it's moving too fast
						if( _secondaryTime>=1 ) _secondaryTime=0.5f;
					}
					else
					{
						// non-looped - stay at the end of the animation
						secPhaseChng -= _secondaryTime-1;
						_secondaryTime=1;
					}
				}

				float moveFactor = secPhaseChng*(1-_primaryFactor);

				moveZ -= moveFactor*secA->GetStepLength();
				moveX -= moveFactor*secA->GetStepLengthX();

				#if 0
				LogF
				(
					"  sec %20s: deltaT %.3f, secSpeed %.3f, secPhaseChng %.3f",
					NAME_T(Type(),_secondaryMove),
					deltaT,secSpeed,secPhaseChng
				);
				#endif
			}
		}
	}

	if( _primaryFactor<0.99f )
	{
		// first finish transition to primary (should be quick)
		//const MoveInfo *pri = Type()->GetMoveInfo(_primaryMove.id);
		//float interpolSpeed = pri ? pri->GetInterpolSpeed() : 6;
		float interpolSpeed = GetInterpolSpeed();
		_primaryFactor += deltaT * interpolSpeed;
		saturateMin(_primaryFactor,1);
		//LogF("Advance: waiting for primary %s",NAME_T(Type(),_primaryMove.id));

		return true;
	}
	_primaryFactor=1;

	if (_primaryMove.id==_forceMove.id)
	{
		_forceMove = MotionPathItem((MoveId)MoveIdNone); // forced move running as primary (no secondary)
	}

	if (finished && _primaryMove.id==_externalMove.id)
	{
		_externalMoveFinished=true;
	}


	if (finished && _primaryMove.context)
	{
		ProcessMoveFunction(_primaryMove.context);
		_primaryMove.context = NULL;

	}

	if (_externalMoveFinished)
	{
		_externalMove = MotionPathItem((MoveId)MoveIdNone);
	}

	// get next move from move queue
	if( _queueMove.Size()<=0 )
	{
		//LogF("Advance: no queue");
		return change; // no more moves - stay in current
	}

	if (prim->GetTerminal())
	{
		return change; // terminal state - no more advance possible
	}

	MotionPathItem nextMove = _queueMove[0];
	if (_primaryMove.id == nextMove.id)
	{
		// identical move in queue may be safely ignored
		// we need to transfer context
		_primaryMove = nextMove;
	}
	else
	{
		const MotionEdge &edge = Type()->Edge(_primaryMove.id,nextMove.id);
		switch( (MotionEdgeType)edge.type )
		{
			case MEdgeNone:
				LogF
				(
					"No edge from %s to %s",
					NAME_T(Type(),_primaryMove.id),NAME_T(Type(),nextMove.id)
				);
				SetPrimaryMove(nextMove);
				// _primary time should be played from the beginning
				_primaryTime = 0;
				_primaryFactor = 1;
				change = true;

			break;
			case MEdgeInterpol:
			{
				// if transition to this move may be interpolated, interpolate
				_secondaryMove = _primaryMove; // from
				_secondaryTime = _primaryTime;
				SetPrimaryMove(nextMove); // to
				// _primary time should be played from the beginning

				const MoveInfo *pri = Type()->GetMoveInfo(_primaryMove.id);
				if (pri->GetInterpolRestart())
				{
					//LogF("Pri %s restart",NAME_T(Type(),_primaryMove.id));
					_primaryTime = 0;
				}
				_primaryFactor = 0;
			}
			break;
			case MEdgeSimple:
				// wait until primary move is finished
				if( !finished )
				{
					//LogF("Advance: waiting for finish of %s",NAME_T(Type(),_primaryMove.id));
					return change;
				}
				// otherwise we have to perform move as it is
				SetPrimaryMove(nextMove);
				_primaryFactor = 1;
				_primaryTime = 0;
				// transition should be smooth - no change here
			break;
		}
	}
	// remove from queue
	//LogF("Advance: done %s",NAME_T(Type(),_queueMove[0].id));
	_queueMove.Delete(0);
	if (_queueMove.Size()<=0)
	{
		_stillMoveQueueEnd = MoveIdNone;
	}
	return change;
}

void Man::PlayMove( RStringB move, ActionContextBase *context)
{
	// get move id
	// id may be MoveIdNone -> in that case movement control will be returned
	MoveId id = Type()->GetMoveId(move);
	_externalQueue.Add(MotionPathItem(id, context));
}

/*!
\patch 1.43 Date 1/29/2002 by Ondra
- Fixed: MP: When moving during get in animation, player could see himself
as running on top of vehicle after getting in.
*/

void Man::SwitchMove(MoveId id, ActionContextBase *context)
{
	if (_externalMove.id!=MoveIdNone)
	{
		// return _externMove (if any) to queue
		_externalQueue.Insert(0, _externalMove);
	}
	_externalMove = MotionPathItem(id, context);
	_externalMoveFinished= false;
	if (id==MoveIdNone)
	{
		id = GetDefaultMove();
	}
	_primaryMove = _externalMove;
	_secondaryMove = _externalMove;
	_queueMove.Clear();
	_stillMoveQueueEnd = id; // be ready to select any variant
	_variantTime = Glob.time;
	_primaryFactor = 1;
	_primaryTime = 0;
	_secondaryTime = 0;
	_gunXRotWanted = _gunXRot = 0;
	_gunYRotWanted = _gunYRot = 0;
	_gunXSpeed = 0;
	_gunYSpeed = 0;
	_headXRotWanted = _headXRot = 0;
	_headYRotWanted = _headYRot = 0;
	//_headXSpeed = 0;
	//_headYSpeed = 0;
	//LogF("%s: SwitchMove %s",(const char *)GetDebugName(),NAME_T(Type(),_primaryMove.id));
	RecalcGunTransform();
}

RString Man::GetCurrentMove() const
{
	int n = _queueMove.Size();
	MoveId id = (n == 0 ? _primaryMove.id : _queueMove[n - 1].id);
	return id < 0 ? RString() : Type()->GetMoveName(id);
}

void Man::SwitchMove( RStringB move, ActionContextBase *context)
{
	// get move id
	// id may be MoveIdNone -> in that case movement control will be returned
	MoveId id = Type()->GetMoveId(move);
	SwitchMove(id,context);
}

bool Man::PlayAction(ManAction action, ActionContextBase *context)
{
	MoveId id = GetMove((ManAction)action);
	if (id == MoveIdNone)
	{	
		if (context)
		{
			ProcessMoveFunction(context);
		}
		return false;
	}
	_externalQueue.Add(MotionPathItem(id, context));
	return true;
}

bool Man::SwitchAction(ManAction action, ActionContextBase *context)
{
	MoveId id = GetMove((ManAction)action);
	if (id == MoveIdNone)
	{
		if (context)
		{
			ProcessMoveFunction(context);
		}
		return false;
	}
	SwitchMove(id,context);
	return true;
}

/*!
\patch 1.17 Date 8/14/2001 by Ondra.
- Fixed: Crew leg position after mounting vehicle on the hillside.
*/

void Man::SwitchVehicleAction(ManVehAction action)
{
	MoveId id = GetVehMove(action);
	//LogF("SwitchVehicleAction %d:%s",action,NAME_T(Type(),id));
	if (id == MoveIdNone)
	{
		return;
	}
	SwitchMove(id,NULL);
	// reset leg transform to identity
	_legTrans = MIdentity;

}

bool Man::CheckActionProcessing(UIActionType action, AIUnit *unit) const
{
	// check if this action is being processed
	// TODO: check action context
	return IsActionInProgress(MFUIAction);
}

// Macrovision CD Protection
void __cdecl CDPHeal(Person *me, const UIAction &action);
CDAPFN_DECLARE_GLOBAL(CDPHeal, CDAPFN_OVERHEAD_L1, CDAPFN_CONSTRAINT_NONE);

void __cdecl CDPHeal(Person *me, const UIAction &action)
{
	// force target to perform some action
	Person *person = dyn_cast<Person,EntityAI>(action.target);
	if (person && person != me)
	{
		Vector3 dir = (me->Position() - person->Position()).Normalized();

		// move me (change orientation only)
		Matrix4 trans;
		trans.SetUpAndDirection(VUp, -dir);
		trans.SetPosition(me->Position());
		me->Move(trans);
		
		// move medic
		Vector3 pos = me->Position() - dir;
		pos[1] = GLandscape->RoadSurfaceYAboveWater(pos+VUp*0.5f);
		trans.SetUpAndDirection(VUp, dir);
		trans.SetPosition(pos);
		person->Move(trans);

		person->PlayAction(ManActMedic);
		person->PlayAction(ManActUp);
	}

	// some actions are postponed
	Ref<ActionContextUIAction> context = new ActionContextUIAction(action);
	me->PlayAction(ManActTreated,context);
	me->PlayAction(ManActUp);

	CDAPFN_ENDMARK(CDPHeal);

	// Czech CD Protection removed
/*
#if _CZECH
	PerformRandomRoxxeTest_000(CDDrive);
#endif
*/
}

/*!
\patch 1.21 Date 8/22/2001 by Ondra.
- Fixed: Soldier now cannot run away while taking down the a flag.
*/

void Man::StartActionProcessing(const UIAction &action, AIUnit *unit)
{
	switch (action.type)
	{
		case ATHeal:
		{
			CDPHeal(this, action);
			break;
		}
		case ATRepair:
		case ATRefuel:
		case ATRearm:
		case ATTakeWeapon:
		case ATTakeMagazine:
		case ATDropWeapon:
		case ATDropMagazine:
		case ATSetTimer:
		case ATDeactivate:
		case ATUseWeapon:
		case ATUseMagazine:
		case ATHideBody:
		case ATFireInflame:
		case ATFirePutDown:
		case ATDeactivateMine:
		case ATTakeMine:
		{
			// some actions are postponed
			Ref<ActionContextUIAction> context = new ActionContextUIAction(action);
			PlayAction(ManActPutDown,context);
			break;
		}
		case ATTakeFlag:
		case ATReturnFlag:
		{
			// manipulation with pole flag requires different animation
			Ref<ActionContextUIAction> context = new ActionContextUIAction(action);
			if (!dyn_cast<FlagCarrier,EntityAI>(context->action.target))
			{
				PlayAction(ManActPutDown,context);
			}
			else
			{
				PlayAction(ManActTakeFlag,context);
			}
			break;
		}
		case ATLadderOnDown:
		case ATLadderDown:
		{
			Ref<ActionContextUIAction> context = new ActionContextUIAction(action);
			PlayAction(ManActLadderOnDown,context);
			break;
		}
		case ATLadderOff:
		{
			action.Process(unit);
			//Ref<ActionContextUIAction> context = new ActionContextUIAction(action);
			PlayAction(ManActLadderOff);
			break;
		}
		case ATLadderOnUp:
		case ATLadderUp:
		{
			Ref<ActionContextUIAction> context = new ActionContextUIAction(action);
			PlayAction(ManActLadderOnUp,context);
			break;
		}
		default:	
		// some are processed directly
			action.Process(unit);
			break;
	}
}

bool Man::IsActionInProgress(MoveFinishF action) const
{
	// check _primary move
	if
	(
		_primaryMove.context && _primaryMove.context->function == action
	)
	{
		return true;
	}
	// check moves waiting in queues
	if
	(
		_externalMove.context && _externalMove.context->function == action
	)
	{
		return true;
	}
	for (int i=0; i<_externalQueue.Size(); i++)
	{
		ActionContextBase *context = _externalQueue[i].context;
		if (context && context->function == action) return true;
	}
	return false;
}

bool Man::ReloadMagazine(int slotIndex, int iMagazine)
{
	const MagazineSlot &slot = GetMagazineSlot(slotIndex);
	const MuzzleType *muzzle = slot._muzzle;
	
	bool ret = false;
	// reload action
	if (!IsActionInProgress(MFReload))
	{
		bool reloadOk = false;
		AIUnit *unit = CommanderUnit();
		if (unit && !muzzle->_autoReload)
		{
			const Magazine *magazine = GetMagazine(iMagazine);
			RString muzzleID = slot._weapon->GetName() + RString("|") + slot._muzzle->GetName();
			Ref<ActionContextDefault> context = new ActionContextDefault;

			context->function = MFReload;
			context->param = magazine->_creator;
			context->param2 = magazine->_id;
			context->param3 = muzzleID;

			reloadOk = unit->GetPerson()->PlayAction(magazine->_type->_reloadAction, context);
			if (reloadOk)
			{
				PlayReloadMagazineSound(slotIndex,muzzle);
				ret = true;
			}
		}
		if (!reloadOk)
			ret = ReloadMagazineTimed(slotIndex, iMagazine, false);
	}
	return ret;
}

bool Man::EnableWeaponManipulation() const
{
	if (IsActionInProgress(MFReload)) return false;
	// during some operations weapon manipulation is not possible
	// like during burst or during reload animation
	if (DisableWeaponsLong()) return false;
	return base::EnableWeaponManipulation();
}

/*!
\patch 1.01 Date 6/25/2001 by Ondra.
- Fixed: optics during crouch.
- in 1.00 it was impossible to move with optics view when crouching
- optics now work like mode, once set, they appear when you stop moving
\patch_internal 1.01 Date 6/25/2001 by Ondra. new function EnableOptions
*/

bool Man::EnableViewThroughOptics() const
{
	// check if unit is able to use optics
	if (BinocularSelected() && !EnableBinocular()) return false;
	// FIX
	return EnableOptics();
	//return true;
}

/*!
\patch 1.01 Date 06/11/2001 by Ondra.
- Fixed: freeze after getout (in code see FIX)
- soldier sometimes stayed frozen in the air
after getting out of vehicle, until moved.
*/

void Man::ResetMovement(float speed, int action)
{
	if (action < 0) action = ManActDefault;
	if (_isDead) action = ManActDie;
	ManAction act = (ManAction)action;
	// check if current move is default move
	MoveId move = GetDefaultMove(act);

	int primaryIndex = FindWeaponType(MaskSlotPrimary);
	int handGunIndex = FindWeaponType(MaskSlotHandGun);
	if (handGunIndex>=0 && primaryIndex<0)
	{
		SelectHandGun(true);
	}
	if (primaryIndex>=0 && handGunIndex<0)
	{
		SelectHandGun(false);
	}

	AIUnit *unit = Brain();
	int upDegreeWanted;
	if (unit) upDegreeWanted = GetUpDegree(unit->GetCombatMode(), IsHandGunSelected());
	else upDegreeWanted = IsHandGunSelected() ? ManPosHandGunStand : ManPosCombat;
	
	if (_primaryMove.id != MoveIdNone)
	{
		const ActionMap *map = Type()->GetActionMap(_primaryMove.id);
		if (map->GetUpDegree()==upDegreeWanted)
		{
			MoveId moveWanted = map->GetAction(act);
			if (moveWanted!=MoveIdNone) move = moveWanted;
		}
	}
	_queueMove.Clear();
	SetPrimaryMove(MotionPathItem(move));

	//_walkSpeed=speed;
	//_walkSpeedWanted=speed;
	_turnWanted=0;
	_landContact=false;
	// FIX freeze after getting out
	_objectContact=false;
	_tired = 0;
	_secondaryMove = _primaryMove;
	_primaryTime = _secondaryTime = 0;
	_primaryFactor=1;
	_speed=VZero;
	_angMomentum=VZero;
	_impulseForce=VZero;
	_impulseTorque=VZero;


	if( unit )
	{
		// TODO: move to AIUnit member
		Path &path=unit->GetPath();
		path.SetSearchTime(Glob.time-60);
	}
	_modelSpeed=VZero;
	// reset external animation queue
	_externalQueue.Clear();
	_externalMove = MotionPathItem((MoveId)MoveIdNone);
	//SetScale(_manScale);

	_gunXRotWanted = _gunXRot = 0;
	_gunYRotWanted = _gunYRot = 0;
	_gunXSpeed = 0;
	_gunYSpeed = 0;
	_headXRotWanted = _headXRot = 0;
	_headYRotWanted = _headYRot = 0;

	RecalcGunTransform();
	if (IsInLandscape())
	{
		RecalcPositions(*this);
	}
}

/*!
\patch 1.78 Date 7/22/2002 by Jirka
- Added: MP respawn in base - enable several respawn markers for each side / vehicle
*/

static ArcadeMarkerInfo *FindMarker(RString name)
{
	AUTO_STATIC_ARRAY(int, indices, 16);
	int len = name.GetLength();
	for (int i=0; i<markersMap.Size(); i++)
	{
		ArcadeMarkerInfo &mInfo = markersMap[i];
		if (strnicmp(mInfo.name, name, len) == 0) indices.Add(i);
	}
	if (indices.Size() == 0) return NULL;
	int index = toIntFloor(indices.Size() * GRandGen.RandomValue());
	return &markersMap[indices[index]];
}

static ArcadeMarkerInfo *FindRespawnMarker(Person *veh)
{
	if (veh && veh->GetVarName().GetLength() > 0)
	{
		RString name = RString("respawn_") + veh->GetVarName();
		ArcadeMarkerInfo *info = FindMarker(name);
		if (info) return info;
	}

	AIUnit *unit = veh ? veh->Brain() : NULL;
	AIGroup *grp = unit ? unit->GetGroup() : NULL;
	if (grp)
	{
		unit = grp->Leader();
		veh = unit ? unit->GetPerson() : NULL;
		if (veh && veh->GetVarName().GetLength() > 0)
		{
			RString name = RString("respawn_") + veh->GetVarName();
			ArcadeMarkerInfo *info = FindMarker(name);
			if (info) return info;
		}

		AICenter *center = grp->GetCenter();
		if (center)
		{
			RString name;
			switch (center->GetSide())
			{
			case TWest:
				name = "respawn_west"; goto SideFound;
			case TEast:
				name = "respawn_east"; goto SideFound;
			case TGuerrila:
				name = "respawn_guerrila"; goto SideFound;
			case TCivilian:
				name = "respawn_civilian"; goto SideFound;
			SideFound:
				{
					ArcadeMarkerInfo *info = FindMarker(name);
					if (info) return info;
				}
				break;
			}
		}
	}

	return FindMarker("respawn");
}

static Person *FindGroupRespawn(Person *who, bool &leader)
{
	AIUnit *unit = who->Brain();
	if (!unit)
	{
		Fail("Respawning unit with no brain");
		return NULL;
	}
	AIGroup *grp = unit->GetGroup();
	if (!grp)
	{
		Fail("Respawning unit with no group");
		return NULL;
	}
	// TODO: two units may want to respawn to same unit
		// patch - enable respawn only for group leader
		// if (!unit->IsGroupLeader()) return NULL;
	// this needs to be overworked
	if (unit->IsGroupLeader())
	{
		// if we are leader, select new group leader 
		// predict result of leader elections
		AIUnit *respawn = grp->LeaderCandidate(unit);
		if
		(
			respawn && respawn->IsLocal() && !respawn->GetPerson()->IsNetworkPlayer() &&
			respawn->GetLifeState() == AIUnit::LSAlive &&
			!respawn->GetPerson()->IsDammageDestroyed()
		)
		{
			leader = true;
			return respawn->GetPerson();
		}
	}
	// select any non-leader, prefer nearest experience
	float minExpDif = 1e10;
	AIUnit *respawn = grp->Leader();
	if
	(
		respawn->GetPerson()->IsNetworkPlayer() || respawn == unit || respawn->GetLifeState() != AIUnit::LSAlive
	) respawn = NULL;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *u = grp->UnitWithID(i + 1);
		if (!u || u==unit || u->GetLifeState() != AIUnit::LSAlive) continue;
		if (u->IsGroupLeader()) continue;
		Person *person = u->GetPerson();
		if (!person->IsLocal()) continue;
		if (person->IsNetworkPlayer()) continue;
		if (person->IsDammageDestroyed()) continue;
		float expDif = person->GetExperience()-who->GetExperience();
		if (fabs(expDif)<minExpDif)
		{
			minExpDif = expDif;
			respawn = u;
		}
	}
	if (respawn)
	{
		return respawn->GetPerson();
	}
	return NULL;
}

static bool RunRespawnScript
(
	const char *script,
	Person *man, EntityAI *killer, GameValue par3
)
{
	// problem: script may be already running
	// we have to terminate it first
	GWorld->TerminateCameraScript();

	RString name = script;
	if (QIFStreamB::FileExist(RString("scripts\\") + name))
	{
		GameArrayType arguments;
		arguments.Add(GameValueExt(man));
		arguments.Add(GameValueExt(killer));
		arguments.Add(par3);
		Script *script = new Script(name, GameValue(arguments));
		GWorld->StartCameraScript(script);
		return true;
	}
	return false;
}

Person *ProcessGroupRespawn(Person *person, int player)
{
	AISubgroup *subgrp = person->Brain()->GetSubgroup();
	Assert(subgrp);
	AIGroup *grp = subgrp->GetGroup();
	Assert(grp);
	Assert(grp->IsLocal());

	bool leader = false;
	Person *respawn = FindGroupRespawn(person, leader);
	if (!respawn) return NULL;
	respawn->SetRemotePlayer(player);
	if (leader)
	{
		grp->SelectLeader(respawn->Brain());
	}
	AIUnitInfo &info = person->GetInfo();
	saturateMax(info._experience, info._initExperience);
	respawn->SetInfo(info);
	respawn->SetFace(info._face, info._name);
	respawn->SetGlasses(info._glasses);
	GetNetworkManager().CopyUnitInfo(person, respawn);
	// make this vehicle player
	GetNetworkManager().SelectPlayer
	(
		player, respawn, true
	);
	//grp->SetReportedDown(person->Brain(), true);
	grp->SendUnitDown(respawn->Brain(), person->Brain());
	
	if (leader)
	{
		GetNetworkManager().UpdateObject(subgrp);
		GetNetworkManager().UpdateObject(grp);
	}
	return respawn;
}

void GroupRespawnDone(Person *person, EntityAI *killer, Person *respawn)
{
	bool script = RunRespawnScript
	(
		"onPlayerRespawnOtherUnit.sqs",
		person,killer,GameValueExt(respawn->Brain()->GetVehicle())
	);
	if (!script)
	{
		GWorld->SwitchCameraTo(respawn->Brain()->GetVehicle(), CamExternal);
	}
}

/*!
\patch 1.90 Date 11/08/2002 by Jirka
- Fixed: Multiplayer - when leader was respawned to seagull and group has no AI members, new leader was not selected
*/
void ProcessSeagullRespawn(Person *person, EntityAI *killer)
{
	Vector3 pos = person->WorldPosition() + 20.0f * VUp;
	SeaGullAuto *seagull = dyn_cast<SeaGullAuto>
	(
		NewNonAIVehicle("seagull", NULL)
	);
	if (seagull)
	{
		DoAssert(pos.IsFinite());
		seagull->SetPosition(pos);
		seagull->SetManual(true);
		seagull->MakeAirborne(20);
		GWorld->AddAnimal(seagull); // insert vehicle to both landscape and world
		GetNetworkManager().CreateVehicle(seagull, VLTAnimal, "", -1);
	}
	bool script = RunRespawnScript
	(
		"onPlayerRespawnAsSeagull.sqs",
		person,killer,GameValueExt(seagull)
	);
	if (!script && seagull)
	{
		GWorld->SwitchCameraTo(seagull, CamExternal);
	}

	AIUnit *brain = person->Brain();
	if (brain)
	{
		AIGroup *grp = brain->GetGroup();
		if (grp && grp->Leader() == brain)
		{
			int total = 0, players = 0;
			for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
			{
				AIUnit *unit = grp->UnitWithID(i + 1);
				if (!unit) continue;
				if (unit == brain) continue;
				total++;
				if (unit->IsAnyPlayer()) players++;
			}
			if (players > 0 && total == players)
			{
				grp->SetReportedDown(brain, true);
				AISubgroup *subgrp = brain->GetSubgroup();
				subgrp->ReceiveAnswer(brain, AI::UnitDestroyed);
			}
		}
		brain->SetLifeState(AIUnit::LSDead);
	}

	if (person->IsLocal()) GetNetworkManager().DisposeBody(person);
}

/*!
\patch 1.31 Date 11/08/2001 by Jirka
- Fixed: when player was killed in vehicle in MP (respawn = "BIRD")
seagull appears at wrong place
\patch 1.32 Date 11/26/2001 by Jirka
- Fixed: respawn in group did not properly switch camera if unit respawn into was in vehicle
\patch 1.33 Date 12/03/2001 by Jirka
- Improved: friendly kills are distinguished in MP messages
\patch 1.34 Date 12/7/2001 by Ondra
- Fixed: Player was considered alive by AI leader after respawned to sea-gull.
\patch 1.36 Date 12/13/2001 by Jirka
- Fixed: Group respawn enabled also for nonleader units.
\patch 1.58 Date 5/20/2002 by Jirka
- Fixed: Group respawn - sometimes two nonleader units respawns into single body
*/

void Man::KilledBy( EntityAI *owner )
{
	Assert(_brain);
	if (!_brain) return;
	AIUnit::LifeState state = _brain->GetLifeState();
	if (state == AIUnit::LSDead || state == AIUnit::LSDeadInRespawn) return;
	bool player = this == GWorld->GetRealPlayer();
	bool playable = _brain->IsPlayable();
	RespawnMode mode = RespawnNone;
	if (GWorld->GetMode() == GModeNetware)
	{
		mode = GetNetworkManager().GetRespawnMode();
		if (player)
		{
			char message[256];
			AIUnit *killerUnit = owner ? owner->CommanderUnit() : NULL;

			AIGroup *myGroup = _brain->GetGroup();
			AICenter *myCenter = myGroup ? myGroup->GetCenter() : NULL;
			AIGroup *killerGroup = killerUnit ? killerUnit->GetGroup() : NULL;
			AICenter *killerCenter = killerGroup ? killerGroup->GetCenter() : NULL;
			bool friendly = myCenter &&
				killerCenter &&
				killerCenter->IsFriendly(myCenter->GetSide()) &&
				owner->GetTargetSide() != TEnemy && GetTargetSide() != TEnemy && owner != this;

			if (killerUnit && killerUnit->GetPerson()->IsRemotePlayer())
			{
				sprintf
				(
					message, friendly ? LocalizeString(IDS_KILLED_BY_FRIENDLY) : LocalizeString(IDS_KILLED_BY),
					(const char *)_brain->GetPerson()->GetInfo()._name,
					myGroup ? (const char *)myGroup->GetName() : "",
					_brain->ID(),
					(const char *)killerUnit->GetPerson()->GetInfo()._name
				);
			}
			else
			{
				sprintf
				(
					message, friendly ? LocalizeString(IDS_KILLED_FRIENDLY) : LocalizeString(IDS_KILLED),
					(const char *)_brain->GetPerson()->GetInfo()._name,
					myGroup ? (const char *)myGroup->GetName() : "",
					_brain->ID()
				);
			}
			GChatList.Add(CCGlobal, NULL, message, false, true);
			RefArray<NetworkObject> units;
			void WhatUnits(RefArray<NetworkObject> &units, ChatChannel channel, NetworkObject *object);
			WhatUnits(units, CCGlobal, NULL);
			RadioSentence rs = RadioSentence();
			GetNetworkManager().RadioChat(CCGlobal, NULL, units, message, rs);
		}
	}
	switch (mode)
	{
		default:
			Fail("Respawn mode");
		case RespawnNone:
			if (player)
			{
				if (GWorld->GetMode() != GModeNetware) GChatList.Clear();
				RString name = "onPlayerKilled.sqs";
				if (QIFStreamB::FileExist(RString("scripts\\") + name))
				{
					GWorld->EnableEndDialog(false);
					GameArrayType arguments;
					arguments.Add(GameValueExt(this));
					arguments.Add(GameValueExt(owner));
					Script *script = new Script(name, GameValue(arguments));
					GWorld->StartCameraScript(script);
				}
			}
			break;
		case RespawnToGroup:
GroupRespawn:
			if (player)
			{
				Brain()->SetLifeState(AIUnit::LSDead);
				GetNetworkManager().UpdateObject(this);
				GetNetworkManager().UpdateObject(Brain());
				// find suitable candidate for respawning
				AIGroup *grp = Brain()->GetGroup();
				if (grp->IsLocal())
				{
					Person *respawn = ProcessGroupRespawn(this, GetNetworkManager().GetPlayer());
// RptF("Group respawn processed locally: %s -> %s", (const char *)GetDebugName(), respawn ? (const char *)respawn->GetDebugName() : "NULL");
					if (respawn) GroupRespawnDone(this, owner, respawn);
					else ProcessSeagullRespawn(this, owner);
				}
				else
				{
// RptF("Group respawn: %s ask for respawn", (const char *)GetDebugName());
					GetNetworkManager().AskForGroupRespawn(this, owner);
					// ?? script
				}
				if (IsLocal()) GetNetworkManager().DisposeBody(this);
				return;
			}
			break;
		case RespawnToFriendly:
			Fail("RespawnToFriendly (respawn=side) not implemented");
			goto GroupRespawn;
		case RespawnSeaGull:
			if (player)
			{
				ProcessSeagullRespawn(this, owner);
				return;
			}
			break;
		case RespawnInBase:
			if (playable)
			{
				Transport *veh = _brain->GetVehicleIn();
				if (veh)
				{
					_brain->ProcessGetOut(false);						
				}
				Vector3 pos = Position();
				ArcadeMarkerInfo *info = FindRespawnMarker(this);
				if (info)
				{
					if (info->markerType == MTIcon || info->a == 0 || info->b == 0)
					{
						pos = info->position;
					}
					else if (info->markerType == MTRectangle)
					{
						float a = (2.0f * GRandGen.RandomValue() - 1.0f) * info->a;
						float b = (2.0f * GRandGen.RandomValue() - 1.0f) * info->b;
						float angleRad = -HDegree(info->angle);
						float s = sin(angleRad);
						float c = cos(angleRad);
						pos = info->position;
						pos[0] += c * a + s * b;
						pos[2] += s * a - c * b;
					}
					else
					{
						Assert(info->markerType == MTEllipse)
						float e = sqrt(Square(info->a) - Square(info->b));
						float a, b;
						do
						{
							a = (2.0f * GRandGen.RandomValue() - 1.0f) * info->a;
							b = (2.0f * GRandGen.RandomValue() - 1.0f) * info->b;
						} while
						(
							sqrt(Square(a - e) + Square(b)) +
							sqrt(Square(a + e) + Square(b)) >
							2.0f * info->a
						);

						float angleRad = -HDegree(info->angle);
						float s = sin(angleRad);
						float c = cos(angleRad);
						pos = info->position;
						pos[0] += c * a + s * b;
						pos[2] += s * a - c * b;
					}
				}
				_brain->FindNearestEmpty(pos);
				pos[1] = GLandscape->RoadSurfaceYAboveWater(pos+VUp*0.5f);
				if (player)
				{
					RunRespawnScript
					(
						"onPlayerRespawn.sqs",this,owner,GetNetworkManager().GetRespawnDelay()
					);
				}
				GetNetworkManager().Respawn(this, pos);
				_brain->SetLifeState(AIUnit::LSDeadInRespawn);
				return;
			}
			break;					
		case RespawnAtPlace:
			if (playable)
			{
				Transport *veh = _brain->GetVehicleIn();
				if (veh)
				{
					_brain->ProcessGetOut(false);
				}
				if (player)
				{
					RunRespawnScript
					(
						"onPlayerRespawn.sqs",this,owner,GetNetworkManager().GetRespawnDelay()
					);
				}
				GetNetworkManager().Respawn(this, Position());
				_brain->SetLifeState(AIUnit::LSDeadInRespawn);
				return;
			}
			break;
	}

	Transport *veh=_brain->VehicleAssigned();
	if( veh ) veh->UpdateStop();
	GLOB_WORLD->RemoveSensor(this);
	AIUnit *unit=_brain;
	//_brain.Free();
	unit->SendAnswer(AI::UnitDestroyed);

	if (IsLocal()) GetNetworkManager().DisposeBody(this);
}

/*!
\patch 1.85 Date 9/9/2002 by Jirka
- Fixed: Healing of dead unit sometimes ends in loop
*/
float Man::NeedsAmbulance() const
{
	// FIX: no ambulance can help already
	if (_isDead) return 0;

	if( GetTotalDammage()<=0.05f ) return 0;
	if ( GetHitCont(Type()->_handsHit)>=0.3f ) return GetHitCont(Type()->_handsHit);
	if ( GetHitCont(Type()->_legsHit)>=0.3f) return GetHitCont(Type()->_legsHit);
	if ( GetHitCont(Type()->_headHit)>=0.3f) return GetHitCont(Type()->_headHit);
	if ( GetHitCont(Type()->_bodyHit)>=0.3f) return GetHitCont(Type()->_bodyHit);
	return GetTotalDammage();
}

float Man::NeedsRepair() const {return 0;}
float Man::NeedsRefuel() const {return 0;}
float Man::NeedsInfantryRearm() const
{
	if (IsDammageDestroyed()) return 0;
	AIUnit *unit = Brain();
	if (!unit) return 0;

	const MuzzleType *muzzle1 = NULL;
	const MuzzleType *muzzle2 = NULL;
	int slots1 = 0, slots2 = 0, slots3 = 0;
	unit->CheckAmmo(muzzle1, muzzle2, slots1, slots2, slots3);

	int slotsMax = GetItemSlotsCount(GetType()->_weaponSlots);
	if (slotsMax == 0) return 0;

	int slotsMaxPri = 0;
	if (muzzle1) slotsMaxPri = slotsMax < 4 ? slotsMax : 4;
	int slotsMaxSec = 0;
	if (muzzle2) slotsMaxSec = slotsMax - slotsMaxPri;
	int slotsMaxOth = slotsMax - slotsMaxPri - slotsMaxSec;

	return (4.0f * slots1 + 2.0f * slots2 + 1.0f * slots1) /
		(4.0f * slotsMaxPri + 2.0f * slotsMaxSec + 1.0f * slotsMaxOth);
}

void Man::SetDammage(float dammage)
{
	bool wasDead = _isDead;
	base::SetDammage(dammage);
	if (wasDead && !_isDead)
	{
		AIUnit *unit = Brain();
		if (unit)
		{
			unit->SetLifeState(AIUnit::LSAlive);
			ResetMovement(0);
			AIGroup *grp = unit->GetGroup();
			if (grp) grp->RessurectUnit(unit);
		}
		else
		{
			_isDead = true;
		}
	}
}

void Man::ReactToDammage()
{
	if (!_isDead)
	{
		if (IsDammageDestroyed()) _isDead=true;
		if (GetHit(Type()->_headHit)>0.9f) _isDead = true;
		if (GetHit(Type()->_bodyHit)>0.9f) _isDead = true;
	}
	if( _isDead )
	{
		_turnWanted=0;
		//_walkSpeedWanted=0;
	}
}

bool Man::BinocularSelected() const
{
	if (_currentWeapon < 0 || _currentWeapon >= NMagazineSlots()) return false;
	const MagazineSlot &slot = GetMagazineSlot(_currentWeapon);
	const WeaponType *type = slot._weapon;
//	if (!(type->_weaponType&MaskSlotBinocular)) return false;
	if (stricmp(type->GetName(), "binocular") != 0) return false;
	return true;
}

/*
bool Man::HandGunSelected() const
{
	if (_currentWeapon < 0) return false;
	const MagazineSlot &slot = GetMagazineSlot(_currentWeapon);
	const WeaponType *type = slot._weapon;
	if (!(type->_weaponType & MaskSlotHandGun)) return false;
	return true;
}
*/

bool Man::IsHandGunInMove() const
{
	int actPos = GetActUpDegree();
	return
	(
		actPos==ManPosHandGunLying || actPos==ManPosHandGunCrouch || actPos==ManPosHandGunStand
	);
}

bool Man::IsPrimaryWeaponInMove() const
{
	int actPos = GetActUpDegree();
	return
	(
		actPos==ManPosLying || actPos==ManPosCrouch || actPos==ManPosCombat
	);
}

/*!
\patch 1.02 Date 07/11/2001 by Ondra
- Fixed: empty LAW optics looking to sky.
*/

const WeaponModeType *Man::GetCurrentWeaponMode() const
{
	if (_currentWeapon < 0) return NULL;
	const WeaponModeType *mode = GetWeaponMode(_currentWeapon);
	if (mode) return mode;
	// FIX
	// if there is no mode, use some other way to check if it is LAW
	// check weapon typical magazine
	const MagazineSlot &slot = GetMagazineSlot(_currentWeapon);
	const MuzzleType *muzzle = slot._muzzle;
	if (!muzzle) return NULL;
	if (muzzle->_magazines.Size()<=0) return NULL;
	MagazineType *magazine = muzzle->_magazines[0];
	if (!magazine)
	{
		magazine = muzzle->_typicalMagazine;
	}
	if (!magazine) return NULL;
	// check first magazine mode
	if (magazine->_modes.Size()<=0) return NULL;
	return magazine->_modes[0];
}

bool Man::LauncherSelected() const
{
	const WeaponModeType *mode = GetCurrentWeaponMode();
	if (!mode) return false;
	if (!mode->_ammo) return false;
	if (mode->_ammo->_simulation == AmmoShotMissile) return true;
	if (mode->_ammo->_simulation == AmmoShotLaser) return true;
	return false;
}

/*!
\patch 1.37 Date 12/17/2001 by Ondra
- Fixed: AI: Missile launcher is not prepared
if there is no reason to prepare it.
\patch_internal 1.37 Date 12/17/2001 by Ondra
- Fixed: AI laucher is not selected when:
1) there is no current target or current target armor is less than 10 
and 
2) there is no nearby enemy or unknown target with armor higher than 50
*/

bool Man::LauncherWanted() const
{
	// check if there is some reason to have missile ready
	// reason can be: some armored target is selected to be fired at
	AIUnit *unit = _brain;
	if (!unit) return false;
	if (unit->GetSemaphore()<=AI::SemaphoreGreen)
	{
		Target *tgt = unit->GetTargetAssigned();
		if (tgt)
		{
			if (tgt->type->_armor>10) return true;
		}
		return false;
	}
	// check if some known enemy or unidentified target
	// is worth attacking by launcher
	Target *tgt = unit->GetTargetAssigned();
	if (tgt)
	{
		if (tgt->type->_armor>10) return true;
		if (tgt->type->IsKindOf(GWorld->Preloaded(VTypeMan))) return false;
	}
	// check all targets
	AIGroup *grp = GetGroup();
	if (!grp) return false;
	AICenter *center = grp->GetCenter();
	if (!center) return false;
	const TargetList &list = grp->GetTargetList();
	for (int i=0; i<list.Size(); i++)
	{
		Target *tgt = list[i];
		if (tgt->type->_armor<50) continue;
		if (!tgt->IsKnownBy(unit)) continue;
		if (!center->IsEnemy(tgt->side) && !center->IsUnknown(tgt->side))
		{
			continue;
		}
		return true;
	}
	return false;
}

bool Man::LaserSelected() const
{
	const WeaponModeType *mode = GetCurrentWeaponMode();
	if (!mode) return false;
	if (!mode->_ammo) return false;
	if (mode->_ammo->_simulation == AmmoShotLaser) return true;
	return false;
}

bool Man::LauncherFire() const
{
	Assert( !QIsManual() );
	if( !LauncherSelected() ) return false;
	if( !_fire._fireTarget || _fire._firePrepareOnly ) return false;
	return ( _fireState==FireAim || _fireState==FireAimed );
}

bool Man::LauncherReady() const
{
	if( !LauncherSelected() ) return false;
	const Magazine *magazine = GetMagazineSlot(_currentWeapon)._magazine;
	if (!magazine || magazine->_ammo == 0) return false;
	if( _fire._fireTarget ) return true;
	//if( _launchDelay>0 ) return false;
	return true;
}


void Man::ResetLauncher()
{
	/*
	for( int w=0; w<NWeapons(); w++ )
	{
		const WeaponInfo &info=GetWeapon(w);
		if( info._ammo && info._ammo->_simulation==AmmoShotMissile )
		{
			AmmoState &state=GetAmmoState(w);
			MuzzleState &mState=GetMuzzleState(w);
			mState._reload=info._reloadTime;
		}
	}
	*/
}

void Man::GetRelSpeedRange( float &speedZ, float &minSpd, float &maxSpd )
{
	speedZ = 0;
	minSpd = 0, maxSpd = 1000;
	const MoveInfo *pri = Type()->GetMoveInfo(_primaryMove.id);
	if (pri)
	{
		speedZ = pri->GetSpeed() * -(*pri)->GetStepLength();
		saturateMax(minSpd,pri->GetRelSpeedMin());
		saturateMin(maxSpd,pri->GetRelSpeedMax());
		if( _primaryFactor<0.99f )
		{
			const MoveInfo *sec = Type()->GetMoveInfo(_secondaryMove.id);
			if (sec)
			{
				float secSpeedZ = sec->GetSpeed() * -(*sec)->GetStepLength();
				speedZ = (1-_primaryFactor)*secSpeedZ + _primaryFactor*speedZ;
				saturateMax(minSpd,sec->GetRelSpeedMin());
				saturateMin(maxSpd,sec->GetRelSpeedMax());

			}
		}
	}
}

bool Man::SimulateAnimations
(
	float &turn, float &moveX, float &moveZ, float deltaT, SimulationImportance prec
)
{
	bool change = false;

	if (_primaryMove.id == MoveIdNone)
	{
		// doing nothing: switch to action stand depending on current UpDegree
		_primaryMove = MotionPathItem(GetDefaultMove());
	}


	float maxRunTired = _tired*TiredRunSpeed+(1-_tired)*FastRunSpeed;

	//float delta;

	/*
	delta=_walkSpeedWanted*_rndSpeed-_walkSpeed;
	Limit(delta,-10*deltaT,+8*deltaT);
	_walkSpeed+=delta;
	Limit(_walkSpeed,-2,maxRunTired);

	delta=_sideSpeedWanted*_rndSpeed-_sideSpeed;
	Limit(delta,-10*deltaT,+8*deltaT);
	_sideSpeed+=delta;
	Limit(_sideSpeed,-5,+5);
	*/

	// set move speed coeficient
	// to move exactly at given speed
	float speedZ, minSpd, maxSpd;
	GetRelSpeedRange(speedZ,minSpd,maxSpd);

	float adjustTimeCoef = 1.0f;
	if (!QIsManual())
	{
		// now we can adjust the speed
		if (fabs(speedZ)>1e-6)
		{
			adjustTimeCoef = _walkSpeedWanted/speedZ;
			saturate(adjustTimeCoef,minSpd,maxSpd);
		}
		/*
		if (this==GWorld->CameraOn())
		{
			LogF
			(
				"adjustTimeCoef %.2f, min %.2f, max %.2f",
				adjustTimeCoef,minSpd,maxSpd
			);
		}
		*/
	}
	else
	{
		// limit movement by tired in range <TiredRunSpeed,FastRunSpeed>


		// now we can adjust the speed
		if (fabs(speedZ)>1e-6)
		{
			if (speedZ>maxRunTired)
			{
				adjustTimeCoef = maxRunTired/speedZ;
				saturate(adjustTimeCoef,minSpd,maxSpd);
			}
		}
		/*
		if (this==GWorld->CameraOn())
		{
			LogF
			(
				"adjustTimeCoef %.2f, min %.2f, max %.2f",
				adjustTimeCoef,minSpd,maxSpd
			);
		}
		*/
	}

	turn=0;
	moveX=0;
	moveZ=0;
	// apply current animation

	//float priPhaseChange,secPhaseChange;
	// note: adjustTimeCoef also determines speed
	change = AdvanceMoveQueue(deltaT,adjustTimeCoef,moveX,moveZ,prec);

	// simulate recoil effects

	if (_recoil)
	{
		change = true;
	}


	// note: suspended soldiers need not recalculate Gun transform
	// unless they are aiming at something
	// RecalcGunTransform is done in MoveWeapons 
	//RecalcGunTransform();

	if( _turnToDo )
	{
		turn = -fSign(_turnToDo)*32*deltaT;
		if( fabs(turn)>=fabs(_turnToDo) )
		{
			turn=-_turnToDo;
			_turnToDo=0;
		}
	}
	else
	{
		turn = -_turnWanted*deltaT;
	}


	ActionMap *map = Type()->GetActionMap(_primaryMove.id);
	float turnSpeed = map ? map->GetTurnSpeed() : 0;
	saturate(turn,-turnSpeed*deltaT,+turnSpeed*deltaT);

	if (fabs(turn)>1e-3)
	{
		change = true;
	}

#if _ENABLE_CHEATS
	if( this==GWorld->CameraOn() && CHECK_DIAG(DEAnimation))
	{
		char buf[1024];
		*buf=0;
		#if 1
		ActionMap *map = Type()->GetActionMap(_primaryMove.id);
		sprintf
		(
			buf+strlen(buf),
			" deg %d",map ? map->GetUpDegree() : ManPosStand
		);
		AnimationRT *pri = Type()->GetAnimation(_primaryMove.id);
		sprintf
		(
			buf+strlen(buf),
			" %s - Prim %s:%.2f, Sec %s:%.2f, PCoef %.2f ",
			pri ? (const char *)pri->Name() : "NULL",
			NAME_T(Type(),_primaryMove.id),_primaryTime,
			NAME_T(Type(),_secondaryMove.id),_secondaryTime,
			_primaryFactor
		);
		if( _queueMove.Size()>0 )
		{
			MoveId nextMove = _queueMove[0].id;
			sprintf
			(
				buf+strlen(buf),
				"Next %s ",NAME_T(Type(),nextMove)
			);
		}
		#else
		sprintf(buf+strlen(buf),"In %d ",InsideLOD(GWorld->GetCameraType()));
		#endif

		GlobalShowMessage
		(
			200,buf
		);
	}
#endif

	return change;
}

void Man::BasicSimulationCore( float deltaT, SimulationImportance prec )
{
	_head.Simulate(Type()->_head,deltaT, prec, _isDead);

	if( _isDead )
	{
		_turnWanted=0;
		//_walkSpeedWanted=0;
		//_sideSpeedWanted=0;

		ActionMap *map = Type()->GetActionMap(_primaryMove.id);
		// get which action is it (based on action map)
		if (map)
		{
			MoveId moveWanted = map->GetAction(ManActDie);
			if (moveWanted!=MoveIdNone)
			{
				Ref<ActionContextDefault> context = new ActionContextDefault;
				context->function = MFDead;
				SetMoveQueue(MotionPathItem(moveWanted, context),prec<=SimulateVisibleFar);
			}
			_forceMove = MotionPathItem((MoveId)MoveIdNone);
			_externalQueue.Clear();
			_externalMove = MotionPathItem((MoveId)MoveIdNone);
		}
		_head.SetMimicMode("dead");
	}
	else
	{
		// 
		AIUnit *unit = _brain;
		if (unit)
		{
			CombatMode mode = unit->GetCombatMode();
			if (unit->IsDanger())
			{
				_head.SetMimicMode("danger");
			}
			else if (_whenScreamed<Glob.time && _whenScreamed>Glob.time-5)
			{
				_head.SetMimicMode("hurt");
			}
			else switch (mode)
			{
				case CMAware:
					if (FindWeaponType(MaskSlotPrimary|MaskSlotHandGun)>=0)
					{
						_head.SetMimicMode("aware");
					}
					else
					{
						_head.SetMimicMode("safe");
					}
					break;
				case CMCombat:
				case CMStealth:
					_head.SetMimicMode("combat");
					break;
				default:
					_head.SetMimicMode("safe");
					break;
			}
		}
	}
}

float Man::GetLegPhase() const
{
	return _primaryTime;
}

void Man::BasicSimulation( float deltaT, SimulationImportance prec, float speedFactor )
{
	//  animation should also run inside of vehicle
	float turn,moveX,moveZ;

	RefreshMoveQueue(prec<=SimulateVisibleFar);

  bool forceRecalcMatrix = MoveHead(deltaT);
	if (forceRecalcMatrix)
	{
		RecalcGunTransform();
	}
	SimulateAnimations(turn,moveX,moveZ,deltaT*speedFactor,prec);
	// note: results are ignored

	BasicSimulationCore(deltaT,prec);

}

void Man::PlaceOnSurface(Matrix4 &trans)
{
	// assume landcontact point is always at y=0
	// calculate animated point position
	// place in steady position
	Vector3 pos=trans.Position();
	Matrix3 orient=trans.Orientation();
	
	pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos+VUp*0.5f);

	trans.SetPosition(pos);

}

void Man::RecalcPositions(const Frame &moveTrans)
{
	_aimingPositionWorld = CalculateAimingPosition(moveTrans);
	_cameraPositionWorld = CalculateCameraPosition(moveTrans);
	DoAssert (_aimingPositionWorld.SquareSize()>Square(10));
	DoAssert (_cameraPositionWorld.SquareSize()>Square(10));

	// animate reflectors position
	int level = _shape->FindMemoryLevel();
	if (level >= 0)
	{
		for (int i=0; i<_reflectors.Size(); i++)
		{
			const ReflectorInfo &info = Type()->_reflectors[i];
			LightReflectorOnVehicle *light = _reflectors[i];
			AttachedOnVehicle *attached = light;
			if (info.positionIndex >= 0)
			{
				Vector3Val position = AnimatePoint(level, info.positionIndex);
				Vector3 direction = VForward;
				if (info.directionIndex >= 0)
				{
					direction = AnimatePoint(level, info.directionIndex) - position;
					direction.Normalize();
				}
				attached->SetAttachedPos(position, direction);
			}
		}
	}
}

/*!
\patch 1.85 Date 9/13/2002 by Jirka
- Fixed: In some cutscenes in Cold War Crisis campaign soldiers "levitate" above surface
*/

void Man::OnPositionChanged()
{
	_landContact = false;
	_objectContact = false;
	RecalcPositions(*this);
}

/*!
\patch 1.01 Date 06/11/2001 by Ondra.
- Fixed: moving other soldier up/down
\patch 1.01 Date 06/11/2001 by Ondra.
- Fixed: being underground when colliding with truck
\patch 1.27 Date 10/12/2001 by Ondra.
- Fixed: soldiers can no longer stay under water.
\patch 1.70 Date 6/5/2002 by Ondra
- Fixed: Improved dead body collisons.
\patch 1.82 Date 8/25/2002 by Ondra
- Fixed: Walking upstairs was very hard, almost impossible
(bug introduced in 1.75).
\patch 1.85 Date 9/9/2002 by Ondra
- Fixed: While climbing ladder with flag, flag was left on the ladder end.
\patch 1.85 Date 9/9/2002 by Ondra
- Fixed: While climbing ladder, player was immortal.
*/

void Man::Simulate( float deltaT, SimulationImportance prec )
{

	if (_ladderBuilding && _ladderIndex>=0)
	{
		// check keyboard pilot / AI pilot
		if (IsDammageDestroyed())
		{
			DropLadder(_ladderBuilding,_ladderIndex);
			//Move(Position()-Direction());
			//SetSpeed(Direction()*-1);
			//_landContact=false;
		}
		else
		{
			SimLadderMovement(deltaT,prec);
		}

		base::Simulate(deltaT,prec);
		return;
	}

	float turn,moveX,moveZ;

	bool change = SimulateAnimations(turn,moveX,moveZ,deltaT,prec);

	// FIX underground when colliding with truck
	if (_objectContact) change = true;

	BasicSimulationCore(deltaT, prec);

	
	// if object is frozen, do not simulate it
	if (!CheckPredictionFrozen())
	{
		
		Vector3 position=Position();

		// simulate interaction with land
		Vector3 friction(VZero),torqueFriction(VZero);
		Vector3 force(VZero),torque(VZero);
		Vector3 pForce(VZero),pCenter(VZero);
		bool freeFall=false;

		float landLegsFactor = 0;
		float bankCorrectFactor = 0;

		Frame moveTrans = *this; // store old position
		// check if we changed position or animation in this simulation step

		//float slope = 0;

		if (!_landContact && !_objectContact)
		{
			_freeFallUntil = Glob.time+0.3f;
		}
		if( Glob.time<_freeFallUntil )
		{
			freeFall=true;

			Vector3Val speed=ModelSpeed();
			friction[0]=speed[0]*fabs(speed[0])*0.002f*GetMass()+speed[0]*0.01f*GetMass();
			friction[1]=speed[1]*fabs(speed[1])*0.002f*GetMass()+speed[1]*0.01f*GetMass();
			friction[2]=speed[2]*fabs(speed[2])*0.002f*GetMass()+speed[2]*0.01f*GetMass();

			// actual movement
			Matrix4 movePos;
			ApplySpeed(movePos,deltaT);
			moveTrans.SetTransform(movePos);

			//SetScale(_manScale);

			#if ARROWS
				AddForce(Position(),friction*InvMass());
			#endif

			// apply gravity
			pForce[0]=0;
			pForce[1]=-G_CONST*GetMass();;
			pForce[2]=0;
			force+=pForce;

			#if ARROWS
				AddForce(Position(),pForce*InvMass());
			#endif
			saturate(_angMomentum[0],-10,+10);
			saturate(_angMomentum[1],-10,+10);
			saturate(_angMomentum[2],-10,+10);
			if( _landContact )
			{
				torqueFriction=_angMomentum*1;
				friction[0]+=fSign(speed[0])*100+speed[0]*0.5f*GetMass();
				friction[1]+=fSign(speed[1])*1000+speed[1]*5*GetMass();
				friction[2]+=fSign(speed[2])*100+speed[2]*0.5f*GetMass();
			}
			else
			{
				torqueFriction=_angMomentum*0.3f;
				float surfaceY=GLOB_LAND->RoadSurfaceY(Position()+VUp*0.5f);
				if( Position().Y()-surfaceY>5 )
				{
					pCenter=Vector3(-0.1f,0.4f,0.2f);
					torque+=pCenter.CrossProduct(friction);
				}
			}

			friction=DirectionModelToWorld(friction);
			torque=DirectionModelToWorld(torque);

			change = true;
		}
		else if( IsDead() )
		{
			if( _whenKilled==Time(0) )
			{
				_whenKilled=Glob.time;
				IsMoved();
				_lastMovementTime=Glob.time;
			}
			_speed=VZero;
			_angMomentum=VZero;
			Matrix3 orient;
			float dx,dz;
			GLOB_LAND->RoadSurfaceY(Position()+VUp*0.5f,&dx,&dz);
			Vector3Val landUp=Vector3(-dx,1,-dz).Normalized();
			Vector3 toUp=landUp-DirectionUp();
			float maxD=1*deltaT;
			saturate(toUp[0],-maxD,+maxD);
			saturate(toUp[1],-maxD,+maxD);
			saturate(toUp[2],-maxD,+maxD);
			orient.SetUpAndDirection(DirectionUp()+toUp,Direction());
			//orient.SetScale(_manScale);


			moveTrans.SetPosition(Position());
			moveTrans.SetOrientation(orient);

		}
		else if (!change)
		{
			// optimize easy case - soldier is static
			_speed = VZero;
			_angMomentum=VZero;
		}
		else
		{
			// correct speedX
			Vector3 xzSpeed;
			float invDeltaT=1/deltaT;
			DirectionModelToWorld(xzSpeed,Vector3(moveX,0,moveZ));
			xzSpeed[1]=0;
			_speed = xzSpeed*invDeltaT;
			_angMomentum=VZero;
			position += xzSpeed;


			float onLand = ValueTest(&MoveInfo::OnLand);
			/*
			const MoveInfo *priInfo = Type()->GetMoveInfo(_primaryMove.id);
			const MoveInfo *secInfo = Type()->GetMoveInfo(_secondaryMove.id);
			float priOnLand = priInfo ? priInfo->OnLand(_primaryTime) : 0;
			float secOnLand = secInfo ? secInfo->OnLand(_secondaryTime) : 0;


			// create leg transform
			// copy current slope (dx,dz)
			// leg transform is skew of legs
			// transform dx,dz to model space
			float onLand = priOnLand*_primaryFactor + secOnLand*(1-_primaryFactor);
			*/
			Vector3 landUp=VUp;
			landLegsFactor = 1-onLand;
			if( onLand>0 )
			{
				float dx,dz;
				GLOB_LAND->RoadSurfaceY(position+VUp*0.5f,&dx,&dz);
				// limit skew to certain extent
				landUp=Vector3(-dx,1,-dz).Normalized()*onLand+VUp*(1-onLand);

				// leg transform is decreased
				// it is used during collision detection
				//_legTrans(1,0) *= landLegsFactor;
				//_legTrans(1,2) *= landLegsFactor;
			}
			bankCorrectFactor = GetLimitGunMovement(); //1;

			 
			// check current up and turn
			if (fabs(turn)>1e-6 || landUp.Distance2(DirectionUp())>1e-6)
			{
				Matrix3 orient;
				// some orientation change - set orientation in matrix
				// note: we want to rotate around point in landcontact


				Matrix3 rotate(MRotationY,turn);
				orient.SetUpAndDirection(landUp,rotate*Direction());
				//orient.SetScale(_manScale);
				#if 0
					// experience show this correction is not necessary
					int landC = _shape->FindLandContactLevel();
					Vector3 center = AnimatePoint(landC,0);
					Vector3 oldCenter = Orientation()*center;
					Vector3 newCenter = orient*center;
					// we have to adapt moveTrans position by rotation
					position += oldCenter-newCenter;
				#endif

				moveTrans.SetOrientation(orient);

				// center in model-space
			}
			else
			{
				// check scale should not be changed
				moveTrans.SetOrientation(Orientation());
			}

			moveTrans.SetPosition(position);
		}

		// how much work is current activity
		// consider speed
		// when laying, speed is more important
		
		// TODO: hills
		//float speedUz = _speed.Y();

		const MoveInfo *priInfo = Type()->GetMoveInfo(_primaryMove.id);
		const MoveInfo *secInfo = Type()->GetMoveInfo(_secondaryMove.id);
		float priDuty = priInfo ? priInfo->GetDuty() : 0;
		float secDuty = secInfo ? secInfo->GetDuty() : 0;

		float duty = priDuty*_primaryFactor + secDuty*(1-_primaryFactor);
		
		#if 0
		if( this==GWorld->CameraOn() )
		{
			GlobalShowMessage(200,"Tired %.2f, duty %.2f",_tired,duty);
		}
		#endif

		// personality duration
		float personality = GetInvAbility();
		if (duty<0) personality = 1; // refreshing is same speed for all soldiers
		// "bad" soldier are tired much faster
		_tired += duty *(1.0f/20)*personality*deltaT;
		saturate(_tired,0,1);


		Vector3 wCenter(NoInit);
		moveTrans.PositionModelToWorld(wCenter,GetCenterOfMass());

		float underBias=0.5f;
		if( !_landContact || freeFall && Speed().Y()>0.5f) underBias=0;

		bool wasLandContact=_landContact;
		_landContact=false;

		// here we expect surface normal
		bool steadyBody = false;
		float landDX=0,landDZ=0;
		{
			// check collision with ground
			
			float maxUnder=-underBias;
			if( !_isDead )
			{
				if (!change)
				{
					// remain static
					_landContact=true;
					maxUnder = 0;
					// landDX?
				}
				else
				{
					// calculate landcontact point position

					#if !_RELEASE
						int level=GetShape()->FindLandContactLevel();
						DoAssert( level>=0 );
						// verify this level has just one point
						//Shape *lc=GetShape()->LevelOpaque(level);
						//DoAssert( lc->NPos()==1 );
					#endif


					_waterDepth=0;
					GroundCollisionBuffer retVal;
					GLOB_LAND->GroundCollision(retVal,this,moveTrans,underBias,0,true,true);
					ADD_COUNTER(sGndC,retVal.Size());
					bool waterContact = false;
					if( retVal.Size()>0 )
					{
						for( int i=0; i<retVal.Size(); i++ )
						{
							const UndergroundInfo &info=retVal[i];
							if( info.under<0 ) continue;
							if( info.type==GroundSolid )
							{
								float under = info.under-underBias;
								if (maxUnder<under)
								{
									maxUnder = under;
									_landContact=true;
									landDX=info.dX,landDZ=info.dZ;
									if (info.texture) _surfaceSound = info.texture->GetSoundEnv();
									//LogF("ground %.2f,%.2f",landDX,landDZ);
								}
							}
							else if( info.type==GroundWater )
							{
								saturateMax(_waterDepth,info.under-underBias);
								waterContact = true;
							}
						}
					}
					if (waterContact)
					{
						const SurfaceInfo &info = GLandscape->GetWaterSurface();
						_surfaceSound = info._soundEnv;
					}
				}
			}
			else
			{
				if
				(
					IsDead() &&
					_whenKilled<Glob.time-5 && _lastMovementTime<Glob.time-5 &&
					wasLandContact && _hideBody>=_hideBodyWanted
				)
				{
					// simulation suspended - dead body is in steady position
					_landContact=true;
					steadyBody = true;
					maxUnder=0;
					if (_hideBody >= 1 && !_brain) SetDelete();
				}
				else
				{
					float delta = _hideBodyWanted-_hideBody;
					Limit(delta,-0.1f*deltaT,+0.1f*deltaT);
					_hideBody += delta;

					float under = underBias-_hideBody;

					_waterDepth=0;
					GroundCollisionBuffer retVal;
					GLOB_LAND->GroundCollision(retVal,this,moveTrans,under,0,true,true);
					ADD_COUNTER(sGndD,retVal.Size());
					for( int i=0; i<retVal.Size(); i++ )
					{
						const UndergroundInfo &info=retVal[i];
						if( info.under<0 ) continue;
						if( info.type==GroundSolid )
						{
							saturateMax(maxUnder,info.under-underBias);
							_landContact=true;
							landDX=info.dX,landDZ=info.dZ;
							//LogF("dead ground %.2f,%.2f",landDX,landDZ);
						}
						else if( info.type==GroundWater )
						{
							saturateMax(_waterDepth,info.under-underBias);
						}
					}
				}
			}

			// check land hit speed
			if( _landContact && Glob.time>_disableDammageUntil )
			{
				float maxCrashSpeed=10;
				if( _speed.SquareSize()>=Square(maxCrashSpeed) )
				{
					float dammage=(_speed.Size()-maxCrashSpeed)*0.1f;
					float armorCoef = GetInvArmor()*3;
					dammage *= armorCoef;
					LocalDammage(NULL,this,VZero,dammage,2.0f);
				}
			}
			// avoid underland situation
			Vector3 position=moveTrans.Position();
			position[1]+=maxUnder;
			moveTrans.SetPosition(position);
		}

		// collision testing
		if (deltaT>0 && (change || prec<=SimulateVisibleNear))
		{
			// check collision on new position
			Vector3 totForce(VZero);

			float crash=0;
			//Vector3Val speed=ModelSpeed();

			_objectContact=false;
			if (QIsManual())
			{
				// special handling of collisions for player
				// do not allow running through some object
				Vector3 oldPos = AimingPosition();
				Vector3 relAim = PositionWorldToModel(oldPos);
				Vector3 newPos = moveTrans.FastTransform(relAim);
				// check collision on line oldPos / newPos


				//GScene->DrawCollisionStar(oldPos,0.1,PackedColor(Color(0,1,0)));
				//GScene->DrawCollisionStar(newPos,0.1,PackedColor(Color(1,1,0)));
				CollisionBuffer collision;
				// AI soldiers have no collision with static object
				// if they have, it is probably result of bug
				// consider dead bodies?
				// check if we have some substantial distance
				if (newPos.Distance2(oldPos)>Square(0.1f))
				{
					GLandscape->ObjectCollision
					(
						collision,this,NULL,oldPos,newPos,0.5f,ObjIntersectGeom
					);
				}
				if (collision.Size()>0)
				{
					// check at what distance will be the first collision
					float minT = 1e10;
					for (int i=0; i<collision.Size(); i++)
					{
						const CollisionInfo &info = collision[i];
						Object *obj = info.object;
						if (!obj) continue;
						if (obj->IsPassable()) continue;
						saturateMin(minT,info.under);
#if _ENABLE_CHEATS
						if (CHECK_DIAG(DECollision))
						{
							LogF("Component %d",info.component);
							// draw corresponding component
							Shape *geom = obj->GetShape()->GeometryLevel();
							BString<256> compo;
							sprintf(compo,"Component%02d",info.component);
							int ns = geom->FindNamedSel(compo);
							if (ns>=0)
							{
								const NamedSelection &sel = geom->NamedSel(ns);
								for (int s=0; s<sel.Size(); s++)
								{
									Vector3Val v = geom->Pos(sel[s]);
									Vector3Val w = obj->PositionModelToWorld(v);
									GScene->DrawCollisionStar(w,0.5f,PackedColor(Color(0,1,0)));
								}
							}
						}
#endif
					}
					if (minT<1)
					{
						// t is from 0 to 1, 0 is beg, 1 is end
						// alow some minimal movement (to avoid being stuck in)
						saturateMax(minT,0.01f);
						//LogF("Col %g",minT);
						Vector3 col=Position()*(1-minT)+moveTrans.Position()*minT;
						
						moveTrans.SetPosition(col);
#if _ENABLE_CHEATS
						if (CHECK_DIAG(DECollision))
						{
							Vector3 rcol=oldPos*(1-minT)+newPos*minT;
							GScene->DrawCollisionStar(col,0.02f,PackedColor(Color(1,1,0)));
							GScene->DrawCollisionStar(rcol,0.02f,PackedColor(Color(1,0.5,0)));
							GScene->DrawCollisionStar(oldPos,0.05f,PackedColor(Color(0.5,0.5,0)));
							GScene->DrawCollisionStar(newPos,0.05f,PackedColor(Color(0.5,0.5,0)));
						}
						// check nearest poi
#endif
					}

				}

			}
			if (prec<=SimulateVisibleFar && !steadyBody)
			{
				#define MAX_IN 0.05f
				CollisionBuffer collision;
				// AI soldiers have no collision with static object
				// if they have, it is probably result of bug
				// consider dead bodies?
				bool onlyVehicles = !QIsManual() && !freeFall;
				GLandscape->ObjectCollision(collision,this,moveTrans,onlyVehicles);

				Vector3 offset = VZero;

				// check at which level was collision encoutered first
				int minHierLevel = INT_MAX;

				for (int i=0; i<collision.Size(); i++)
				{
					saturateMin(minHierLevel,collision[i].hierLevel);
				}

				for( int i=0; i<collision.Size(); i++ )
				{
					// info.pos is relative to object
					const CollisionInfo &info=collision[i];
					// FIX
					// when there is some high level collision, ignore all proxy collisions
					if (info.hierLevel>minHierLevel) continue;
					// FIX END 
					if (!info.object) continue;
					if (info.object->IsPassable()) continue;
					// if we have no geometry, and it is another soldier, we should not collide
					if (!HasGeometry() && !freeFall && dyn_cast<Man,Object>(info.object)) continue;

					// check if collision point is near below the roadway
					// if it is, ignore it, as roadway collision are sufficient to handle it
					if (info.object->GetShape()->FindRoadwayLevel()>=0)
					{
						// check roadway collision for given point
						float under = GLandscape->UnderRoadSurface(info.object,info.pos,0);
						//GlobalShowMessage(500,"Under %.3f",under);
						if (under>0 && under<1.0f) continue;
					}

					_objectContact=true;
					_lastObjectContactTime = Glob.time;

					Point3 pos=info.object->PositionModelToWorld(info.pos);
					Vector3 dirOut=info.object->DirectionModelToWorld(info.dirOut);
					
					// if info.under is bigger than MAX_IN, move out
					// note: several points may be under
					// this may need to be updated
					// check offset already applied
					float offsetInOutDir = dirOut*offset;
					saturateMax(offsetInOutDir,0);
					float under = info.under - offsetInOutDir;

					if (under>MAX_IN)
					{
						float moveOut=under-MAX_IN;
						Vector3 offsetAdd = dirOut*moveOut;
						offset += offsetAdd;
						/*
						LogF
						(
							"%s moved %s: add offset %.2f: %.2f, %.2f, %.2f",
							(const char *)info.object->GetDebugName(),
							(const char *)GetDebugName(),
							moveOut,offsetAdd[0],offsetAdd[1],offsetAdd[2]
						);
						Man *w = dyn_cast<Man,Object>(info.object);
						if (w)
						{
							LogF
							(
								"his pos %.2f,%.2f,%.2f, anim %s (%.2f), %s",
								w->Position()[0],
								w->Position()[1],
								w->Position()[2],
								NAME_T(Type(),w->_primaryMove.id),w->_primaryFactor,
								NAME_T(Type(),w->_secondaryMove.id)
							);
						}
						LogF
						(
							"my  pos %.2f,%.2f,%.2f , anim %s (%.2f), %s",
							moveTrans.Position()[0],
							moveTrans.Position()[1],
							moveTrans.Position()[2],
							NAME_T(Type(),_primaryMove.id),_primaryFactor,
							NAME_T(Type(),_secondaryMove.id)
						);
						*/

						// TODO: landDX, landDZ calculation
						//landDX = landDZ = 0; //dirOut.dX,landDZ = dirOut.dZ;

						// slow down if necessary
						Vector3Val objSpeed=info.object->ObjectSpeed();
						Vector3 colSpeed=_speed-objSpeed;

						if (info.object->ObjectSpeed().SquareSize()<Square(7))
						{
							// limit relative speed to object we crashed into
							const float maxRelSpeed=0.5f;
							if( colSpeed.SquareSize()>Square(maxRelSpeed) )
							{
								// adapt _speed to match criterion
								crash+=(colSpeed.Size()-maxRelSpeed)*0.3f;
								colSpeed.Normalize();
								colSpeed*=maxRelSpeed;
							}
							_speed=objSpeed+colSpeed;
						}
					}
				} // for all objects
				
				if (offset.SquareSize()>Square(0.01f))
				{
					/*
					LogF
					(
						"** offset %.2f: %.2f, %.2f, %.2f",
						offset.Size(),offset[0],offset[1],offset[2]
					);
					*/

					Matrix4 transform = moveTrans.Transform();
					Point3 newPos = transform.Position();
					newPos += offset;

					transform.SetPosition(newPos);
					moveTrans.SetTransform(transform);

					_lastMovementTime = Glob.time;

					const float crashLimit=0.3f;
					float moveSize = offset.Size();
					if( moveSize>crashLimit ) crash += moveSize-crashLimit;
					// FIX moving other soldier up/down
					change = true;
					//LogF("Collision offset");
				}
			} // if( object collisions enabled )
			if (_landContact || _objectContact)
			{
				// calculate y speed from x,z to follow surface
				_speed[1]=_speed[0]*landDX+_speed[2]*landDZ;
			}
		}

		// recalculate legs transform
		if (change)
		{
			//GlobalShowMessage(100,"Slope %.2f,%.2f",landDX,landDZ);
			//LogF("Slope %.2f,%.2f",landDX,landDZ);

			float legDX = landDX;
			float legDZ = landDZ;
			saturate(legDX,-0.5f,+0.5f);
			saturate(legDZ,-0.5f,+0.5f);

			Vector3 dxdz(legDX,0,legDZ);
			DirectionWorldToModel(dxdz,dxdz);

			_legTrans(1,0) = dxdz.X()*landLegsFactor;
			_legTrans(1,2) = dxdz.Z()*landLegsFactor;

			// calculate bank correction
			if (QIsManual())
			{
				Vector3 relUp =
				(
					WorldToModel().DirectionUp()*bankCorrectFactor + 
					VUp*(1-bankCorrectFactor)
				);

				/*
				float angle = -asin(relUp.X());

				_correctBankSin = sin(angle);
				_correctBankCos = cos(angle);
				*/
				_correctBankSin = -relUp.X();
				_correctBankCos = sqrt(1-_correctBankSin*_correctBankSin);
			}
			else
			{
				_correctBankSin = 0;
				_correctBankCos = 1;
			}
		}

		float impulse2=_impulseForce.SquareSize();
		if (!_landContact && !_objectContact || impulse2>Square(GetMass()*5))
		{
			_lastMovementTime=Glob.time;
			// too strong impulse - dead
			if (impulse2>Square(GetMass()*5))
			{
				//LogF("impulse %.2f",sqrt(impulse2));
				float contact=sqrt(impulse2)*GetInvMass()*(1.0f/5)-1;
				float armorCoef = GetInvArmor()*3;
				contact *= armorCoef;
				if( contact>0.1f )
				{
					//LogF("Contact %.2f",contact);
					saturateMin(contact,5);
					LocalDammage(NULL,this,VZero,contact,1.0f);
				}
				if( impulse2>Square(GetMass()*60) )
				{
					_impulseForce=_impulseForce.Normalized()*(GetMass()*60);
				}
			}
			// force shock - long free fall simulation
			_freeFallUntil=Glob.time+1.0f;
			freeFall=true;
		}
		if( freeFall )
		{
			ApplyForces(deltaT,force,torque,friction,torqueFriction);
			_lastMovementTime=Glob.time;
		}
		else
		{
			// calculate acceleration
			if( _speed.SquareSize()>=Square(0.1f) ) _lastMovementTime=Glob.time;
			_acceleration = VZero;
			_angMomentum = VZero;
			_angVelocity = VZero;
			_impulseForce = VZero;
			_impulseTorque = VZero;

		}


		Move(moveTrans);
		DirectionWorldToModel(_modelSpeed,_speed);

		// if position or animation changed, we need to recalculate some
		// world space / model space properties
		if (change)
		{
			RecalcPositions(moveTrans);
			
		}

		if (_waterDepth>0)
		{
			_waterContact = true;
			//GlobalShowMessage(200,"Water %.2f",_waterDepth);
			float maxSafeDepth = 1.2f;
			if (IsDown()) maxSafeDepth = 0.6f;
			if( _waterDepth>maxSafeDepth )
			{
				float drown = (_waterDepth-maxSafeDepth)*(1.0f/0.5f);
				saturateMin(drown,2);
				// do some dammage
				LocalDammage(NULL,this,VZero,0.05f*deltaT*drown,1.0f);
			}
		}
		else
		{
			_waterContact = false;
		}
	} // if (!CheckPredictionFrozen())

	if (_mGunClouds.Active() || _mGunFire.Active() || _gunClouds.Active())
	{
		Vector3 gunPos=PositionModelToWorld(GetWeaponPoint(_currentWeapon));
		Vector3 dir = GetWeaponDirection(_currentWeapon);
		_mGunFire.Simulate(gunPos,deltaT);
		_mGunClouds.Simulate(gunPos,Speed()*0.7f+dir*5.0f,0.35f,deltaT);
		_gunClouds.Simulate(gunPos,Speed()*0.7f+dir*2.0f,0.35f,deltaT);
		//CancelStop();
	}

	if (IsLocal())
	{
		if (QIsManual())
		{
			// advanced missile control
			// check if last shot is missile
			if (LauncherSelected())
			{
				Missile *missile = dyn_cast<Missile,Vehicle>(_lastShot);
				if (missile)
				{
					// check cursor position
					Vector3 dir = Position()+GetEyeDirection()*1000;
					missile->SetControlDirection(dir);
				}
			}

		}

		if (!LaserSelected())
		{
			_laserTargetOn = false;
		}
	}

	// if animation state has been changed, force gun matrix recalculation
	MoveWeapons(deltaT,change);

	base::Simulate(deltaT,prec);
}

void Man::SetFace(RString name, RString player)
{
	if (name.GetLength() <= 0)
	{
		Fail("Face");
		name = "Default";
	}
	_head.SetFace(Type()->_head, IsWoman(), _shape, name, player);
}

void Man::SetGlasses(RString name)
{
	if (name.GetLength() <= 0)
	{
		Fail("Glasses");
		name = "None";
	}
	_head.SetGlasses(Type()->_head, _shape, name);
}

void Man::AddDefaultWeapons()
{
	AddWeapon("Throw");
	AddWeapon("Put");
	AddWeapon("StrokeFist");
}

void Man::MinimalWeapons()
{
	RemoveAllWeapons();
	RemoveAllMagazines();
	AddDefaultWeapons();
	if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == this)
		GWorld->UI()->ResetVehicle(this);
}

void Man::ScanNVG()
{
	bool found =  false;
	for (int i=0; i<NWeaponSystems(); i++)
	{
		const WeaponType *w = GetWeaponSystem(i);
		if (!w) continue;
		if (!strcmpi(w->GetName(),"NVGoggles")) found =true;
	}
	_hasNVG = found;
	if (!_hasNVG)
	{
		_nvg = false;
	}
}

void Man::OnWeaponAdded()
{
	base::OnWeaponAdded();
	ScanNVG();

	// check handgun consistency
	if
	(
		IsHandGunSelected() &&
		FindWeaponType(MaskSlotHandGun) < 0
	) SelectHandGun(false);
	else if
	(
		!IsHandGunSelected() &&
		FindWeaponType(MaskSlotHandGun) >= 0 &&
		FindWeaponType(MaskSlotPrimary) < 0
	) SelectHandGun(true);
}

void Man::OnWeaponRemoved()
{
	ScanNVG();

	// check handgun consistency
	if
	(
		IsHandGunSelected() &&
		FindWeaponType(MaskSlotHandGun) < 0
	) SelectHandGun(false);
	else if
	(
		!IsHandGunSelected() &&
		FindWeaponType(MaskSlotHandGun) >= 0 &&
		FindWeaponType(MaskSlotPrimary) < 0
	) SelectHandGun(true);

	base::OnWeaponRemoved();
}

void Man::OnWeaponChanged()
{
	base::OnWeaponChanged();
	ScanNVG();

	// check handgun consistency
	if
	(
		IsHandGunSelected() &&
		FindWeaponType(MaskSlotHandGun) < 0
	) SelectHandGun(false);
	else if
	(
		!IsHandGunSelected() &&
		FindWeaponType(MaskSlotHandGun) >= 0 &&
		FindWeaponType(MaskSlotPrimary) < 0
	) SelectHandGun(true);

}

void Man::OnDanger()
{
	float reaction = 0.3f;
	if (_lookForwardTimeLeft>reaction)
	{
		_lookForwardTimeLeft = reaction;
	}
}

void Man::Init( Matrix4Par pos )
{
	// if soldier has only handgun and not rifle, mark it as using handgun
	// assume position is already set
	ReactToDammage();
	ResetMovement(0);
	_aimingPositionWorld = CalculateAimingPosition(pos);
	_cameraPositionWorld = CalculateCameraPosition(pos);
	base::Init(pos);
	ScanNVG();
}

bool Man::IsOnLadder(Building *obj, int ladder) const
{
	return _ladderBuilding==obj && ladder==_ladderIndex;
}

/*!
\patch 1.55 Date 5/7/2002 by Ondra
- Fixed: Soliders now climb ladders fasters.
*/

void Man::SimLadderMovement(float deltaT, SimulationImportance prec)
{
	float turn,moveX,moveZ;

	//bool change = 
	SimulateAnimations(turn,moveX,moveZ,deltaT,prec);

	BasicSimulationCore(deltaT, prec);

	// might drop ladder during SimulateAnimations

	if (!_ladderBuilding) return;

	// check ladder positions
	Building *obj = _ladderBuilding;
	const BuildingType *type = obj->Type();
	const Ladder &ladder = type->GetLadder(_ladderIndex);
	// get top/bottom positions
	int mem = obj->GetShape()->FindMemoryLevel();
	Vector3 top = obj->AnimatePoint(mem,ladder._top);
	Vector3 bottom = obj->AnimatePoint(mem,ladder._bottom);

	// check AI / keyboard speed
	float speed = 0;
	if (IsLocal())
	{
		if (QIsManual())
		{
			// only up/down checked now
			speed =
			(
				GInput.keyMoveForward+
				GInput.keyMoveFastForward+GInput.keyMoveUp-
				GInput.keyMoveDown-GInput.keyMoveBack
			);
			saturate(speed,-1,+1);
		}
		else
		{
			speed = _ladderAIDir; // 
		}
	}

	// check distance between top/bottom
	float invDist = (top-bottom).InvSize();
	float relChange = speed*deltaT*invDist;
	_ladderPosition += relChange;
	bool topEnd = _ladderPosition>=1 && relChange>0;
	bool bottomEnd = _ladderPosition<=0 && relChange<0;
	// TODO: corresponding animation
	saturate(_ladderPosition,0,1);


	MoveId id = MoveId(MoveIdNone);
	ActionMap *map = Type()->GetActionMap(_primaryMove.id);
	// check if we are still on ladder

	if (!EnableTest(&MoveInfo::OnLadder))
	{
		//LogF("Off ladder");
		_ladderBuilding = NULL;
		_ladderIndex = -1;
		MoveId id = map->GetAction(ManActLadderOffTop);
		if (id!=MoveIdNone)
		{
			SetMoveQueue(id,false);
		}
		return;
	}
	if (map)
	{
		if (speed<-0.1f)
		{
			id = map->GetAction(ManActDown);
		}
		else if (speed>0.1f)
		{
			id = map->GetAction(ManActUp);
		}
		else
		{
			id = map->GetAction(ManActStop);
		}
		if (topEnd)
		{
			id = map->GetAction(ManActLadderOffTop);
		}
		if (bottomEnd)
		{
			id = map->GetAction(ManActLadderOffBottom);
		}
	}
	if (id!=MoveIdNone)
	{
		SetMoveQueue(id,false);
	}

	//LogF("speed %.3f, %s",speed,NAME_T(Type(),id));

	// current positon is given by top..bottom
	Vector3 aPos = (top-bottom)*_ladderPosition+bottom;
	// TODO: calculate orientation
	Frame moveTrans = *this; // store old position
	moveTrans.SetPosition(aPos);
	Move(moveTrans);


	RecalcGunTransform();
	RecalcPositions(moveTrans);
}

void Man::CatchLadder(Building *obj, int ladderIndex, bool up)
{
	if (obj)
	{
		const BuildingType *type = obj->Type();
		const Ladder &ladder = type->GetLadder(ladderIndex);
		int mem = obj->GetShape()->FindMemoryLevel();
		Vector3 top = obj->AnimatePoint(mem,ladder._top);
		Vector3 bottom = obj->AnimatePoint(mem,ladder._bottom);
//		Vector3 ladderPos = up ? bottom : top;
		Vector3 ladderDir = top - bottom;
		Vector3 ladderAside = ladderDir.CrossProduct(VUp).Normalized();
		if (ladderAside.SquareSize()<0.1f)
		{
			ladderAside = Vector3(1,0,0);
			RptF("Ladder in %s is vertical",obj->GetShape()->Name());
		}
		Vector3 ladderForward = VUp.CrossProduct(ladderAside).Normalized();
		Matrix4 trans;
		trans.SetPosition(Position());
		trans.SetUpAndDirection(VUp, ladderForward);
		Move(trans);
	}
	
	// perform actual catch/drop ladder
	_ladderBuilding = obj;
	_ladderIndex = ladderIndex;
	_ladderPosition = up;
	_ladderAIDir = up ? -1 : +1;
	//LogF("catch %d",_ladderAIDir);
}

void Man::DropLadder(Building *obj, int ladder)
{
	// perform actual catch/drop ladder
	_ladderBuilding = NULL;
	_ladderIndex = -1;
}

void Man::AutoGuide( float deltaT, bool brake, bool avoid )
{
}

void Man::Sound( bool inside, float deltaT )
{
	#if 1
	if( _doSoundStep )
	{

		const SoundPars *pars = NULL;
		RStringB surface = _surfaceSound;
		if (_soundStepOverride.GetLength()>0)
		{
			surface = _soundStepOverride;
		}

		pars= &Type()->GetEnvSoundExtRandom(surface);
		if
		(
			!_soundStep ||
			stricmp(pars->name, _soundStep->Name()) != 0
		)
		{ // load sound if necessary
			if (pars->name.GetLength() > 0)
			{
				_soundStep = GSoundScene->Open(pars->name);
				//LogF("Step %s",(const char *)pars->name);
			}
			else
				_soundStep = NULL;
		}
		if (_soundStep)
		{
			float volume = GRandGen.RandomValue() * 0.4f + 0.8f;
			float frequency = GRandGen.RandomValue() * 0.2f + 0.9f;
//			const SoundPars &pars = Type()->GetMainSound();
			Vector3 land = Position();
			land[1] = GLandscape->RoadSurfaceY(land+VUp*0.5f);
			_soundStep->SetVolume(pars->vol * volume, pars->freq * frequency);
			_soundStep->SetPosition(land, VZero);
			_soundStep->Repeat(1);
			_soundStep->Restart();
		}
		_doSoundStep=false;
		_soundStepOverride = RStringBEmpty;
	}

	Vector3 absSoundPos = Position();
	if (inside)
	{
		static Vector3 relSoundPos(0,0.2f,0.3f);
		absSoundPos = CameraPosition();
		absSoundPos += GScene->GetCamera()->DirectionModelToWorld(relSoundPos);
	}

	if (_isDead)
	{
		_soundBreath = NULL;
	}
	else
	{
		const SoundPars &pars = Type()->GetMainSound();
		if (!_soundBreath)
		{
			_soundBreath = GSoundScene->OpenAndPlay
			(
				pars.name, Position(), Speed()
			);
		}
		if (_soundBreath)
		{
			float vol = pars.vol * _tired;
			float freq = pars.freq * (_tired + 1.0f) * 0.5f;
			_soundBreath->SetVolume(vol, freq);		// volume, frequency
			_soundBreath->SetPosition(absSoundPos, Speed());
			//_soundBreath->Set3D(!inside);
		}
	}


	float envSpeed = _speed.Size()*0.2f;
	if (envSpeed<=1e-1 || !inside)
	{
		_soundEnv.Free();
	}
	else
	{
		const SoundPars &pars = Type()->_addSound;
		//const SoundPars &pars = Type()->GetEnvSound();
		if (!_soundEnv)
		{
			_soundEnv = GSoundScene->OpenAndPlay
			(
				pars.name, AimingPosition(), Speed()
			);
		}
		if (_soundEnv)
		{
			float freq = envSpeed;
			saturate(freq,0.5f,1.5f);
			_soundEnv->SetVolume(pars.vol*envSpeed,pars.freq*freq);
			_soundEnv->SetPosition(absSoundPos, Speed());
		}
	}
	#endif
	base::Sound(inside,deltaT);
}

void Man::UnloadSound()
{
	_soundEnv.Free();
	_soundBreath.Free();
	_soundStep.Free();
	base::UnloadSound();
}

/*!
\patch 1.75 Date 2/13/2002 by Jirka
- Added: Blood slops under dead bodies
*/

void Man::Destroy( EntityAI *owner, float overkill, float minExp, float maxExp )
{
	// check: man can never explode
	base::Destroy(owner,overkill,minExp,maxExp);
	
	if( _brain ) KilledBy(owner);
}

void Man::Scream(EntityAI *killer)
{
	if
	(
		GetHit(Type()->_headHit)<=0.9f &&
		( _whenScreamed<Glob.time-3 || _whenScreamed>Glob.time )
	)
	{
		//LogF("Hit %.3f",GetHit(Type()->_headHit));
		_whenScreamed = Glob.time;
		const SoundPars &pars=Type()->_hitSound.SelectSound(GRandGen.RandomValue());
		Vector3Val pos=Position();
		float rndFreq = GRandGen.RandomValue()*0.1f+0.95f;
		float vol = pars.vol;
		bool isOut = IsInLandscape();
		if (!isOut) vol *= 0.2f;
		AbstractWave *sound=GSoundScene->OpenAndPlayOnce
		(
			pars.name,pos,VZero,
			vol,pars.freq*rndFreq
		);
		if( sound )
		{
			GSoundScene->SimulateSpeedOfSound(sound);
			GSoundScene->AddSound(sound);
		}
		// if we scream, somebody may hear it
		if (isOut && killer)
		{
			// disclose only is not in any vehicle
			GLandscape->Disclose(killer,Position(),80,true,false);
		}
	}
}

void Man::ShowDammage(int part)
{
	AIUnit *brain = Brain();
	bool wasAlive = brain && brain->GetLifeState()==AIUnit::LSAlive;
	if (wasAlive)
	{
		Scream(NULL);
	}
	base::ShowDammage(part);
}

void Man::HitBy(EntityAI *owner, float howMuch, RString ammo)
{
	base::HitBy(owner,howMuch, ammo);

	ReactToDammage();
}

RString Man::DiagText() const
{
	const char *weapon="Prep";
	if( !_fire._firePrepareOnly && _fire._fireMode>=0 )
	{
		const WeaponModeType *mode = GetWeaponMode(_fire._fireMode);
		if (mode) weapon = mode->GetDisplayName();
	}
	const char *fire="Go";
	switch( _fireState )
	{
		case FireInit: fire="Init";break;
		case FireDone: fire="Run";break;
		case FireAim: fire="Aim";break;
		case FireAimed: fire="Fire";break;
	}
	char buffer[256];
	sprintf
	(
		buffer,
		"%s %s %.1f, spdw %.1f ",
		weapon,fire,Glob.time.Diff(_fireStateDelay),_walkSpeedWanted*3.6
	);
	return RString(buffer)+base::DiagText();
}

void Man::DrawDiags()
{

	base::DrawDiags();
}

bool Man::CastProxyShadow(int level, int index) const
{
	return true;
}

/*!
\patch 1.05 Date 7/17/2001 by Ondra.
- Fixed: Weapon shadows now reflect actual weapon held.
\patch_internal 1.05 Date 7/17/2001 by Ondra.
- New: GetProxy is now returning shape
*/

Object *Man::GetProxy
(
	LODShapeWithShadow *&shape,
	int level,
	Matrix4 &transform, Matrix4 &invTransform,
	const FrameBase &parentPos, int i
) const
{
	// draw visible guns / weapons
	Shape *sShape=_shape->LevelOpaque(level);
	const ManType *type = Type();

	const WeightInfo &weights = type->GetWeights();
	const AnimationRTWeights &wgt = weights[level];

	if (Type()->_nvGogglesProxyIndex[level]==i)
	{
		// night vision
		// check if night vision should be shown
		if (!IsNVWanted() || !IsNVEnabled()) return NULL;
	}
	const ProxyObject &proxy=sShape->Proxy(i);
	// check which matrix
	// TODO: RPG hidden via proxy changing, not here
	const NamedSelection &sel = sShape->NamedSel(proxy.selection);
	int point = sel[0];
	Assert( sel.Size()>0 );
	const AnimationRTWeight &wg=wgt[point];
	// now we have weighting information
	// check primary selection - to know if we are in aiming selection
	int matIndex = wg[0].GetSel();

	const WeaponType *weapon = NULL;
	LODShapeWithShadow *pshape = NULL;
	if (matIndex==type->_rpgMatIndex)
	{
		if (!_showSecondaryWeapon) return NULL;

		int index = FindWeaponType(MaskSlotSecondary, MaskSlotPrimary);
		if (index>=0)
		{
			weapon = GetWeaponSystem(index);
			pshape = weapon->_model;
			if (_currentWeapon >= 0)
			{
				const MagazineSlot &slot = GetMagazineSlot(_currentWeapon);
				if (slot._weapon == weapon && slot._magazine && slot._magazine->_ammo > 0)
				{
					LODShapeWithShadow *special = slot._magazine->_type->_model;
					if (special)
					{
						pshape = special;
					}
				}
			}
			weapon = NULL;	// store only primary weapon
		}
	}
	else if (matIndex==type->_gunMatIndex)
	{
		if (!_showPrimaryWeapon) return NULL;

		int index = FindWeaponType(MaskSlotPrimary);
		if (index>=0)
		{
			weapon = GetWeaponSystem(index);
			pshape = weapon->_model;
			if (_currentWeapon >= 0)
			{
				const MagazineSlot &slot = GetMagazineSlot(_currentWeapon);
				if (slot._weapon == weapon && slot._magazine && slot._magazine->_ammo > 0)
				{
					LODShapeWithShadow *special = slot._magazine->_type->_model;
					if (special)
					{
						pshape = special;
					}
				}
			}
		}
	}
	else if (matIndex==type->_handMatIndex)
	{
		if (ShowItemInHand())
		{
			const WeaponType *weapon = NULL;
			for (int i=0; i<NWeaponSystems(); i++)
			{
				if (GetWeaponSystem(i)->_weaponType & MaskSlotBinocular)
				{
					weapon = GetWeaponSystem(i);
					break;
				}
			}
			if (weapon) pshape = weapon->_model;
		}
	}
	else if (matIndex==type->_rightHandMatIndex)
	{
		if (ShowItemInRightHand())
		{
			const WeaponType *weapon = NULL;
			for (int i=0; i<NWeaponSystems(); i++)
			{
				if (GetWeaponSystem(i)->_weaponType & MaskSlotBinocular)
				{
					weapon = GetWeaponSystem(i);
					break;
				}
			}
			if (weapon) pshape = weapon->_model;
		}
		if (ShowHandGun())
		{
			const WeaponType *weapon = NULL;
			for (int i=0; i<NWeaponSystems(); i++)
			{
				if (GetWeaponSystem(i)->_weaponType & MaskSlotHandGun)
				{
					weapon = GetWeaponSystem(i);
					break;
				}
			}
			if (weapon) pshape = weapon->_model;
		}
	}

	if (!pshape) return NULL;

	Matrix4 animTransform = AnimateProxyMatrix(level,proxy);

	animTransform.Orthogonalize();
	// TODO: smart clipping
	transform=transform*animTransform;

	// if shape is reversed, reverse orientation
	if (pshape->Remarks()&REM_REVERSED)
	{
		Matrix4 swapM(MScale,-1,+1,-1);
		transform = transform*swapM;
	}
	invTransform = transform.InverseScaled();

	shape = pshape;
	return proxy.obj;
}

int Man::GetProxyComplexity
(
	int level, const FrameBase &pos, float dist2
) const
{
	int nFaces = base::GetProxyComplexity(level,pos,dist2);
	// TODO: calculate weapons
	return nFaces;
}

/*!
\patch 1.01 Date 6/26/2001 by Ondra.
- Fixed: draw Night Vision when viewed from external camera.
\patch_internal 1.52 Date 4/19/2002 by Jirka
- Fixed: gun fire support for multiple weapons (was only for primary weapon)
*/

void Man::DrawProxies
(
	int level, ClipFlags clipFlags,
	const Matrix4 &transform, const Matrix4 &invTransform,
	float dist2, float z2, const LightList &lights
)
{
	base::DrawProxies(level, clipFlags, transform, invTransform, dist2, z2, lights);

	// draw visible guns / weapons
	Shape *sShape=_shape->LevelOpaque(level);
	const ManType *type = Type();

	const WeightInfo &weights = type->GetWeights();
	const AnimationRTWeights &wgt = weights[level];

	// draw all proxy objects
	for( int i=0; i<sShape->NProxies(); i++ )
	{
		if (Type()->_nvGogglesProxyIndex[level]==i)
		{
			// night vision
			// check if night vision should be shown
			if (!IsNVWanted() || !IsNVEnabled() || !_showHead ) continue;
		}
		const ProxyObject &proxy=sShape->Proxy(i);
		// check which matrix
		// TODO: RPG hidden via proxy changing, not here
		const NamedSelection &sel = sShape->NamedSel(proxy.selection);
		int point = sel[0];
		Assert( sel.Size()>0 );
	  const AnimationRTWeight &wg=wgt[point];
		// now we have weighting information
		// check primary selection - to know if we are in aiming selection
		int matIndex = wg[0].GetSel();

		const WeaponType *weapon = NULL;
		LODShapeWithShadow *pshape = NULL;
		const AnimationAnimatedTexture *animFire = NULL;
		if (matIndex==type->_rpgMatIndex)
		{
			if (!_showSecondaryWeapon) continue;

			int index = FindWeaponType(MaskSlotSecondary, MaskSlotPrimary);
			if (index>=0)
			{
				weapon = GetWeaponSystem(index);
				pshape = weapon->_model;
				animFire = &weapon->_animFire;
				if (_currentWeapon >= 0)
				{
					const MagazineSlot &slot = GetMagazineSlot(_currentWeapon);
					if (slot._weapon == weapon && slot._magazine && slot._magazine->_ammo > 0)
					{
						LODShapeWithShadow *special = slot._magazine->_type->_model;
						if (special)
						{
							pshape = special;
							animFire = &slot._magazine->_type->_animFire;
						}
					}
				}
				weapon = NULL;	// store only primary weapon
			}
		}
		else if (matIndex==type->_gunMatIndex)
		{
			if (!_showPrimaryWeapon) continue;

			int index = FindWeaponType(MaskSlotPrimary);
			if (index>=0)
			{
				weapon = GetWeaponSystem(index);
				pshape = weapon->_model;
				animFire = &weapon->_animFire;
				if (_currentWeapon >= 0)
				{
					const MagazineSlot &slot = GetMagazineSlot(_currentWeapon);
					if (slot._weapon == weapon && slot._magazine && slot._magazine->_ammo > 0)
					{
						LODShapeWithShadow *special = slot._magazine->_type->_model;
						if (special)
						{
							pshape = special;
							animFire = &slot._magazine->_type->_animFire;
						}
					}
				}
			}
		}
		else if (matIndex==type->_handMatIndex)
		{
			if (ShowItemInHand())
			{
				for (int i=0; i<NWeaponSystems(); i++)
				{
					if (GetWeaponSystem(i)->_weaponType & MaskSlotBinocular)
					{
						weapon = GetWeaponSystem(i);
						break;
					}
				}
				if (weapon) pshape = weapon->_model;
			}
		}
		else if (matIndex==type->_rightHandMatIndex)
		{
			if (ShowItemInRightHand())
			{
				for (int i=0; i<NWeaponSystems(); i++)
				{
					if (GetWeaponSystem(i)->_weaponType & MaskSlotBinocular)
					{
						weapon = GetWeaponSystem(i);
						break;
					}
				}
				if (weapon) pshape = weapon->_model;
			}
			if (ShowHandGun())
			{
				for (int i=0; i<NWeaponSystems(); i++)
				{
					if (GetWeaponSystem(i)->_weaponType & MaskSlotHandGun)
					{
						weapon = GetWeaponSystem(i);
						break;
					}
				}
				if (weapon)
				{
					pshape = weapon->_model;
					animFire = &weapon->_animFire;
					if (_currentWeapon >= 0)
					{
						const MagazineSlot &slot = GetMagazineSlot(_currentWeapon);
						if (slot._weapon == weapon && slot._magazine && slot._magazine->_ammo > 0)
						{
							LODShapeWithShadow *special = slot._magazine->_type->_model;
							if (special)
							{
								pshape = special;
								animFire = &slot._magazine->_type->_animFire;
							}
						}
					}
				}
			}
		}
		else if (Type()->_nvGogglesProxyIndex[level]==i)
		{
			pshape = proxy.obj->GetShape();
		}


		if (!pshape) continue;

		Matrix4 animTransform = AnimateProxyMatrix(level,proxy);

		// TODO: smart clipping
		Matrix4 pTransform=transform*animTransform;

		// if shape is reversed, reverse orientation
		if (pshape->Remarks()&REM_REVERSED)
		{
			Matrix4 swapM(MScale,-1,+1,-1);
			pTransform = pTransform*swapM;
		}
		Matrix4Val invPTransform=pTransform.InverseScaled();

		// LOD detection
		// if soldier cockpit is used, use also weapon cockpit
		int pLevel = -1;
		if (!pshape) pshape = proxy.obj->GetShape();
		if (level==type->_insideView)
		{
			// find inside view
			pLevel = pshape->FindSpecLevel(VIEW_PILOT);
		}
		if (pLevel<0)
		{
			pLevel=GScene->LevelFromDistance2
			(
				pshape,dist2,pTransform.Scale(),
				pTransform.Direction(),GScene->GetCamera()->Direction()
			);
		}
		if( pLevel==LOD_INVISIBLE ) continue;

		if (animFire)
		{
			if (weapon == _mGunFireWeapon && (_mGunFireFrames > 0 || Glob.uiTime < _mGunFireTime + 0.05f))
			{
				animFire->Unhide(pshape, pLevel);
				animFire->SetPhase(pshape, pLevel, _mGunFirePhase);
			}
			else
			{
				animFire->Hide(pshape, pLevel);
			}
		}

		if (weapon && weapon->_revolving.GetSelection(pLevel) >= 0)
		{
			const MagazineSlot *slot = NULL;
			for (int i=0; i<NMagazineSlots(); i++)
			{
				const MagazineSlot &s = GetMagazineSlot(i);
				if (s._weapon == weapon)
				{
					slot = &s;
					break;
				}
			}

			if (slot)
			{
				const Magazine *magazine = slot->_magazine;
				if (magazine)
				{
					const MagazineType *type = magazine->_type;
					if (type->_modes.Size() > 0)
					{
						float reload = magazine->_reload / type->_modes[0]->_reloadTime;
						saturate(reload, 0, 1);
						float coef = (magazine->_ammo + reload) / type->_maxAmmo;
						weapon->_revolving.Rotate(pshape, -2.0f * H_PI * coef, pLevel);
						// LogF("reload %.2f, ammo %d, coef %.2f", reload, magazine->_ammo, coef);
					}
				}
			}
		}

		Shape *shape=pshape->LevelOpaque(pLevel);
		shape->PrepareTextures(z2,shape->Special());
		shape->Draw
		(
			this,
			lights,
			ClipAll,shape->Special(),
			pTransform,invPTransform
		);
	}

	_mGunFireFrames--;
}

void Man::DrawNVOptics()
{
	if (IsNVEnabled())
	{
		Ref<WeaponType> goggles = WeaponTypes.New("NVGoggles");
		const MuzzleType *muzzle = goggles->_muzzles[0];
		LODShapeWithShadow *oShape = muzzle->_opticsModel;
		if (oShape)
		{
			int phase = toIntFloor(5.0f * GRandGen.RandomValue());
			muzzle->_animFire.SetPhase(oShape, 0, phase);
			Draw2D(oShape, 0, PackedWhite);
		}
	}
}

void Man::DrawCameraCockpit()
{
	//base::DrawCameraCockpit();
	// if in wound effect, draw red overlay
	const float timeDelay = 0.3f;
	const float timePlus = 0.3f;
	const float timeMinus = 2;
	if (_whenScreamed<Glob.time+timeDelay && _whenScreamed>=Glob.time-(timeDelay+timePlus+timeMinus))
	{
		float redTime = Glob.time-_whenScreamed-timeDelay;
		float redFactor;
		if (redTime>timePlus)
		{
			redFactor = 1-(redTime-timePlus)*(1.0f/timeMinus);
		}
		else
		{
			redFactor = redTime*(1.0f/timePlus);
		}
		// apply computed red factor
		if (redFactor>0)
		{
			float alpha = redFactor*0.5f;
			PackedColor color = PackedColor(Color(0.5f, 0, 0, alpha));
			const float w = GEngine->Width();
			const float h = GEngine->Height();
			MipInfo mip = GEngine->TextBank()->UseMipmap(NULL, 0, 0);
			GEngine->Draw2D(mip, color, Rect2DAbs(0, 0, w, h));
		}
	}

}

int Man::PassNum( int lod )
{
	if (GEngine->CanGrass())
	{
		if( !_shape ) return 0;
		Shape *shape=_shape->Level(lod);
		if( !shape ) return 0;
		int spec=shape->Special()|GetObjSpecial();
		// soldier cockpit is pass 2 (alpha-transparent objects)
		// it may often interact with the scene, especially with grass
		// therefore it should not be drawn in pass 3 as other cockpits
		if( spec&NoDropdown )
		{
			if (GWorld->GetCameraType()==CamGunner) return 3;
			if (spec&IsAlpha) return 2;
			return 1;
		}
	}
	return base::PassNum(lod);
}

void Man::Draw( int level, ClipFlags clipFlags, const FrameBase &pos )
{
	if( level==LOD_INVISIBLE ) return; // invisible LOD
#if _ENABLE_CHEATS
	if (GWorld->CameraOn() == this)
		{
			if (CHECK_DIAG(DETransparent))
			{
				return;
			}
		}
#endif
	if (GWorld->GetCameraType() == CamGunner && GWorld->CameraOn() == this)
	{
		if (_currentWeapon >= 0)
		{
			WeaponType *weapon = GetMagazineSlot(_currentWeapon)._weapon;
			MuzzleType *muzzle = GetMagazineSlot(_currentWeapon)._muzzle;
			if (muzzle)
			{
				LODShapeWithShadow *oShape = GetOpticsModel(this);
				if (oShape)
				{
					bool showFire = false;
					if (_mGunFireFrames > 0 || Glob.uiTime < _mGunFireTime + 0.05f)
					{
						if ((weapon->_weaponType & MaskSlotPrimary) != 0)
							showFire = true;
					}

					if (showFire)
					{
						muzzle->_animFire.Unhide(oShape, 0);
						muzzle->_animFire.SetPhase(oShape, 0, _mGunFirePhase);
					}
					else
					{
						muzzle->_animFire.Hide(oShape, 0);
					}
					_mGunFireFrames--;
					Draw2D(oShape, 0, GetOpticsColor(this));
				}
			}
		}
/*
		if (IsNVWanted())
		{
			DrawNVOptics();
		}
*/
		return;
	}

	// apply base

	base::Draw(level,clipFlags,pos);
/*
	if (IsNVWanted())
	{
		DrawNVOptics();
	}
*/
}

#if ALPHA_SPLIT

void Man::DrawAlpha( int level, ClipFlags clipFlags )
{
	if( level==LOD_INVISIBLE ) return; // invisible LOD
	float resol=_shape->Resolution(level);
	if
	(
		resol>=VIEW_GUNNER*0.99f && resol<=VIEW_GUNNER*1.01f
	)
	{
		Draw2D(level);
		return;
	}
	base::DrawAlpha(level,clipFlags);
}
#endif

bool Man::IsAnimated( int level ) const {return true;}
bool Man::IsAnimatedShadow( int level ) const {return true;}

void Man::AttachWave(AbstractWave *wave, float freq)
{
	_head.AttachWave(wave, freq);
}

float Man::GetSpeaking() const
{
	if (_head._randomLip || _head._lipInfo)
	{
		return 1;
	}
	return 0;
}

void Man::SetRandomLip(bool set)
{
	_head.SetRandomLip(set);
}

void Man::ShowHead(int level, bool show)
{
	_showHead = show;
	if (show)
	{
		Type()->_headHide.Unhide(_shape,level);
		Type()->_neckHide.Unhide(_shape,level);
	}
	else
	{
		Type()->_headHide.Hide(_shape,level);
		Type()->_neckHide.Hide(_shape,level);
	}
}

void Man::ShowWeapons(bool showPrimary, bool showSecondary)
{
	_showPrimaryWeapon = showPrimary;
	_showSecondaryWeapon = showSecondary;
}

const float MaxStandSlope = 0.9f;
const float MinStandSlope = -0.9f;

const float MaxSprintSlope = 0.2f;
const float MinSprintSlope = -0.5f;

const float MinRunSlope = -0.7f;
const float MaxRunSlope = 0.4f;

inline bool CanSprint(float slope)
{
	return slope>=MinSprintSlope && slope<=MaxSprintSlope;
}
inline bool CanRun(float slope)
{
	return slope>=MinRunSlope && slope<=MaxRunSlope;
}
inline bool CanStand(float slope)
{
	return slope>=MinStandSlope && slope<=MaxStandSlope;
}

float Man::LandSlope(bool &forceStand) const
{
	float dx,dz;
	Texture *tex = NULL;
	Object *obj = NULL;
	forceStand = false;
	GLandscape->RoadSurfaceY(Position()+VUp*0.5f,&dx,&dz,&tex,&obj);
	if (obj) forceStand = true;
	// calculate change along direction forward
	float slope = dx*Direction().X()+dz*Direction().Z();
	//GlobalShowMessage
	//(
	//	100, "Slope %.2f,%.2f, slope %.2f, dir %.2f,%.2f",
	//	dx,dz, slope, Direction().X(),Direction().Z()
	//);
	return slope;
}

bool Man::IsAbleToStand() const
{
	if( GetHit(Type()->_legsHit)>=0.9f ) return false;
	// check landscape slope
	return true;
}

bool Man::IsAbleToFire() const
{
	//if( GetHit(Type()->_handsHit)>=0.9f ) return false;
	return true;
}

/*!
\patch 1.01 Date 6/25/2001 by Ondra.
- Fixed: Movement possible while using optics during crouch.
*/

LODShapeWithShadow *Man::GetOpticsModel(Person *person)
{
	LODShapeWithShadow *ret = base::GetOpticsModel(person);
	if (!ret) return ret;
	if (BinocularSelected() && !EnableBinocular()) return NULL;
	if (LauncherSelected() && !EnableMissile()) return NULL;
	if (!EnableOptics()) return NULL;
	return ret;
}

bool Man::GetForceOptics(Person *person) const
{
	bool ret = base::GetForceOptics(person);
	if (!ret) return false;
	// check if we are already in binoc mode
	if (!EnableBinocular()) return false;
	return true;
}

/*!
\patch 1.05 Date 7/18/2001 by Ondra.
- Improved: blood appears before the injury is critical.
Soldiers also reports sooner for medical assistance.
*/

const float ManHitLimit = 0.7f;
const float ManHitLimitHead = 0.8f;


void Man::WoundsAnimation( int level )
{
	bool NoBlood();
	if (NoBlood()) return;
	if (!Glob.config.blood) return;

	const ManType * type = Type();
	bool bodyWound = GetHitCont(type->_bodyHit)>=ManHitLimit;
	bool headWound = GetHitCont(type->_headHit)>=ManHitLimitHead;
	bool handsWound = GetHitCont(type->_handsHit)>=ManHitLimit;
	bool legsWound = GetHitCont(type->_legsHit)>=ManHitLimit;
	// scan corresponding wound
	if (bodyWound)
	{
		type->_bodyWound.Apply(_shape,level);
	}
	if (/*_showHead && */headWound)
	{
		type->_headWound.ApplyModified
		(
			_shape,level,type->_head._textureOrig,_head._textureWounded
		);
	}
	if (handsWound)
	{
		type->_lArmWound.ApplyModified
		(
			_shape,level,type->_head._textureOrig,_head._textureWounded
		);
		type->_rArmWound.ApplyModified
		(
			_shape,level,type->_head._textureOrig,_head._textureWounded
		);
	}
	if (legsWound)
	{
		type->_lLegWound.Apply(_shape,level);
		type->_rLegWound.Apply(_shape,level);
	}
}

void Man::WoundsDeanimation( int level )
{
	bool NoBlood();
	if (NoBlood()) return;
	if (!Glob.config.blood) return;

	const ManType * type = Type();
	bool bodyWound = GetHitCont(type->_bodyHit)>=ManHitLimit;
	bool headWound = GetHitCont(type->_headHit)>=ManHitLimitHead;
	bool handsWound = GetHitCont(type->_handsHit)>=ManHitLimit;
	bool legsWound = GetHitCont(type->_legsHit)>=ManHitLimit;
	if (bodyWound) type->_bodyWound.Restore(_shape,level);
	if (/*_showHead && */headWound) type->_headWound.Restore(_shape,level);
	if (handsWound)
	{
		type->_lArmWound.Restore(_shape,level);
		type->_rArmWound.Restore(_shape,level);
	}
	if (legsWound)
	{
		type->_lLegWound.Restore(_shape,level);
		type->_rLegWound.Restore(_shape,level);
	}
}

void Man::BasicAnimation( int level )
{
	const ManType *type=Type();

	Shape *shape = _shape->Level(level);
	if (shape)
	{
		// if (_showHead)
		{
			Matrix4 headOrient(MDirection, -VAside, VUp);
			// Matrix4 headOrient(M4HeadBase);
			AnimateMatrix(headOrient,Type()->_headOnlyWeight);
			Matrix3 headTrans;
			if (_headTransIdent && _gunTransIdent)
			{
				headTrans = headOrient.Orientation();
			}
			else
			{
				headTrans = headOrient.Orientation() * GunTransform().Orientation() * _headTrans.Orientation();
			}
			_head.Animate(type->_head, _shape, level, _isDead, headTrans, !_showHead);
		}

		// is wounded animate wounds
		WoundsAnimation(level);

		// set minmax box and sphere
		Vector3 min = shape->MinOrig();
		Vector3 max = shape->MaxOrig();
		Vector3 bCenter = shape->BSphereCenterOrig();
		float bRadius = shape->BSphereRadiusOrig();
		// enlarge twice to be sure soldier will fit in
		Vector3 factor(2,1.2f,3);
		float sFactor = 3;
		min = bCenter+factor.Modulate(min-bCenter);
		max = bCenter+factor.Modulate(max-bCenter);
		bRadius *= sFactor;
		shape->SetMinMax(min,max,bCenter,bRadius);
	}
	base::Animate(level);
}

void Man::AnimatedMinMax( int level, Vector3 *minMax )
{
	Shape *shape = _shape->Level(level);

	Vector3 min = shape->MinOrig();
	Vector3 max = shape->MaxOrig();
	Vector3 bCenter = shape->BSphereCenterOrig();
	// enlarge twice to be sure soldier will fit it
	float factor = 2;
	minMax[0] = bCenter+(min-bCenter)*factor;
	minMax[1] = bCenter+(max-bCenter)*factor;
	// call slow but robust implementation
	//base::AnimatedMinMax(level,minMax);
}

void Man::AnimatedBSphere( int level, Vector3 &bCenter, float &bRadius, bool isAnimated )
{
	Shape *shape = _shape->Level(level);

	bCenter = shape->BSphereCenterOrig();
	// enlarge twice to be sure soldier will fit it
	float factor = 2;
	bRadius = shape->BSphereRadiusOrig()*factor;
}


void Man::BasicDeanimation( int level )
{
	const ManType *type = Type();
	Shape *shape = _shape->Level(level);
	if (shape)
	{
		WoundsDeanimation(level);

		// if (_showHead)
		{
			Matrix4 headOrient(MDirection, -VAside, VUp);
			// Matrix4 headOrient(M4HeadBase);
			AnimateMatrix(headOrient,Type()->_headOnlyWeight);

			Matrix3 headTrans;
			if (_headTransIdent && _gunTransIdent)
			{
				headTrans = headOrient.Orientation();
			}
			else
			{
				headTrans = headOrient.Orientation() * GunTransform().Orientation() * _headTrans.Orientation();
			}
			_head.Deanimate(type->_head, _shape, level, _isDead, headTrans, !_showHead);
		}
	}

	base::Deanimate(level);
}

UnitPosition Man::GetUnitPosition() const
{
	return _unitPos;
}

void Man::SetUnitPosition(UnitPosition status)
{
	_unitPos = status;
}


void Man::ApplyAnimation( int level, RStringB move, float time )
{
	const ManType *type = Type();
	WeightInfo &weights = type->GetWeights();
	MoveId moveId = type->GetMoveId(move);
	AnimationRT *anim = type->GetAnimation(moveId);
	if (anim) anim->Apply(weights,GetShape(),level,time);
	BasicAnimation(level);
}

void Man::ApplyDeanimation( int level )
{
	BasicDeanimation(level);
}

float Man::GetAnimSpeed(RStringB move)
{
	const ManType *type = Type();
	MoveId moveId = type->GetMoveId(move);
	const MoveInfo *info = Type()->GetMoveInfo(moveId);
	if (info) return info->GetSpeed();
	return 1;
}

Vector3 Man::GetPilotPosition(CameraType camType) const
{
	const ManType *type = Type();
	int level = _shape->FindMemoryLevel();

	int selIndex = type->_pilotPoint;
	const NamedSelection &sel = _shape->Level(level)->NamedSel(selIndex);
	if (sel.Size() <= 0) return VZero;
	return AnimatePoint(level,sel[0]);
}


void Man::Animate( int level )
{
	Shape *shape=_shape->Level(level);
	if( !shape ) return;
	const ManType *type=Type();

	// check for special case: animating one-point level
	if (shape->NPos()==1)
	{
		// singular case
		shape->SaveOriginalPos();
		shape->SetPos(0) = AnimatePoint(level,0);
		return;
	}
	MATRIX_4_ARRAY(matrix,128);

	if( _primaryFactor>0.01f )
	{
		AnimationRT *anim=type->GetAnimation(_primaryMove.id);
		if (anim) anim->PrepareMatrices(matrix,_primaryTime,_primaryFactor);
	}
	if( _primaryFactor<0.99f )
	{
		AnimationRT *anim=type->GetAnimation(_secondaryMove.id);
		if (anim) anim->PrepareMatrices(matrix,_secondaryTime,1-_primaryFactor);
	}

	if (matrix.Size()>0)
	{
		int memory = _shape->FindMemoryLevel();
		if (type->_headAxisPoint>=0 && !_headTransIdent)
		{
			BLEND_ANIM(head);
			const BlendAnimSelections &headRes = GetHead(head);

			AnimationRT::CombineTransform
			(
				type->GetWeights(),_shape,memory,
				matrix,_headTrans,
				headRes.Data(),headRes.Size()
			);
		}

		if (type->_aimingAxisPoint>=0 && !_gunTransIdent)
		{
			BLEND_ANIM(aiming);
			const BlendAnimSelections &aimingRes = GetAiming(aiming);
	
			AnimationRT::CombineTransform
			(
				type->GetWeights(),_shape,memory,
				matrix,_gunTrans,
				aimingRes.Data(),aimingRes.Size()
			);
		}

		// legs selection needs to be applied only to graphical lods
		if (_shape->Resolution(level)<1000 || level == type->_insideView)
		{
			BLEND_ANIM(legs);
			const BlendAnimSelections &legsRes = GetLegs(legs);

			// apply legs selections
			AnimationRT::CombineTransform
			(
				type->GetWeights(),_shape,memory,
				matrix,_legTrans,
				legsRes.Data(),legsRes.Size()
			);
		}

		AnimationRT::ApplyMatrices(type->GetWeights(),_shape,level,matrix);
	}

	BasicAnimation(level);
}

void Man::Deanimate( int level )
{
	// Man is always animated - no need to deanimate
	Shape *shape=_shape->Level(level);
	if( !shape ) return;
	if (shape->NPos()==1) return; // singular case

	BasicDeanimation(level);
}

DEFINE_FAST_ALLOCATOR(ActionMap)

static const EnumName ManActionNames[]=
{
	#define ACTION(x) EnumName(ManAct##x,#x),
	#include "../cfg/manActions.hpp"
	#undef ACTION
	EnumName()
};

template<>
const EnumName *GetEnumNames(ManAction action) {return ManActionNames;}

ActionMap::ActionMap( const ActionMapName &name )
{
	_name = name;
	// load action map from an entry
	const ParamEntry &e=*_name.entry;

	_turnSpeed = e>>"turnSpeed";
	_upDegree = e>>"upDegree";
	_limitFast = e>>"limitFast";
	
	//#define LOAD(id,name) 
	#define ACTION(x) _actions[ManAct##x]=_name.motion->GetMoveId(e>>#x);
	#include "../cfg/manActions.hpp"
	#undef ACTION
	//#undef LOAD
}


MovesType::MovesType( const MovesTypeName &name )
{
	_name = name;

	// load RT animations
	_moves.Realloc(_name.motionType->MoveIdN());
	_moves.Resize(_name.motionType->MoveIdN());

	const ParamEntry &moves=_name.motionType->GetEntry();
	const ParamEntry &states=moves>>"States";

	const ParamEntry *statesExt = moves.FindEntry("StatesExt");

	// load appropriate skeleton?
	WeightInfoName wname;
	wname.shape = _name.shape;
	wname.skeleton = _name.motionType->GetSkeleton();

	LODShapeWithShadow *shape = _name.shape;
	_weights=WeigthBank.New(wname);
	for( int i=0; i<_moves.Size(); i++ )
	{
		MoveId id = MoveId(i);
		RStringB moveName = _name.motionType->GetMoveName(id);
		if (!states.FindEntry(moveName) ) continue;
		const ParamEntry *entry = &(states >> moveName);
		if (statesExt)
		{
			const ParamEntry *entryExt = statesExt->FindEntry(moveName);
			if (entryExt) entry = entryExt;
		}
		MoveInfo &moveI = _moves[i];
		moveI = MoveInfo(_name.motionType,*entry);

		AnimationRT *anim=moveI;
		if( anim )
		{
			_name.motionType->GetSkeleton()->Prepare(shape,GetWeights());
			anim->SetLooped((*entry)>>"looped");
			//anim->Prepare(shape,_name.motionType->GetSkeleton(),GetWeights(),entry>>"looped");
		}
	}

}

MovesType::~MovesType()
{
}


DynEnum GActionVehNames;

void ManCompact()
{
	MovesTypes.Compact();
}

void ManCleanUp()
{
	AnimationRTBank.Clear();
	WeigthBank.Clear();
	MovesTypes.Clear();
}


BlendAnimSelections::BlendAnimSelections()
{
}

static int CompBlendAnim( const BlendAnimInfo *i1, const BlendAnimInfo *i2 )
{
	return i1->matrixIndex-i2->matrixIndex;
}

void BlendAnimSelections::Load( Skeleton *skelet, const ParamEntry &cfg )
{
	Clear();
	Realloc(cfg.GetSize()/2);
	for (int i=0; i<cfg.GetSize()-1; i+=2 )
	{
		RStringB name = cfg[i];
		float factor = cfg[i+1];
		BlendAnimInfo &info = Set(Add());
		// search skeleton
		int index = skelet->FindBone(name);

		//info.name = name;
		info.matrixIndex = index;
		info.factor = factor;
	}	
	// sort by matrix index
	QSort(Data(),Size(),CompBlendAnim);
}

void BlendAnimSelections::AddOther( const BlendAnimSelections &src, float factor )
{
	// walk through this and src
	// both are sorted by size
	// if we always process the lower one, we stay in sync
	int i = 0, srcI = 0;

	while (i<Size() && srcI<src.Size())
	{
		BlendAnimInfo &info = Set(i);
		const BlendAnimInfo &srcInfo = src.Get(srcI);
		if (info.matrixIndex==srcInfo.matrixIndex)
		{
			// same matrix - blend and advance in both 
			// blen
			info.factor += srcInfo.factor*factor;
			i++;
			srcI++;
		}
		else if (info.matrixIndex>srcInfo.matrixIndex)
		{
			// src should be processed now
			BlendAnimInfo &set = Set(Add());
			set.matrixIndex = srcInfo.matrixIndex;
			set.factor = srcInfo.factor*factor;
			srcI++;
		}
		else
		{
			// skip thisI, no corresponding entry in src 
			i++;
		}
	}
	// if anything is left in src, add it to this
	for (; srcI<src.Size(); srcI++)
	{
		const BlendAnimInfo &srcInfo = src.Get(srcI);
		BlendAnimInfo &set = Set(Add());
		set.matrixIndex = srcInfo.matrixIndex;
		set.factor = srcInfo.factor*factor;
	}
}

BlendAnimType::BlendAnimType(const BlendAnimTypeName &name)
{
	_name = name;
	//const ParamEntry &par = name.motion->GetEntry()>>name.type;
	Load(name.motion->GetSkeleton(),*name.cfg);
}

MoveInfo::MoveInfo( MotionType *motion, const ParamEntry &entry )
{
	_motion = motion;

	const ParamEntry &actionMaps = motion->GetEntry()>>"Actions";
	const ParamEntry &blendAnimTypes = motion->GetEntry()>>"BlendAnims";

	motion->InitNoActions(&(actionMaps>>"NoActions"));

	RStringB file = entry>>"file";
	if( file.GetLength()>0 )
	{
		AnimationRTName name;
		name.name = GetAnimationName(file);
		name.skeleton = motion->GetSkeleton();

		bool preload = entry>>"preload";
		_move = AnimationRTBank.New(name);
		if (preload) _move->AddPreloadCount();

	}
	RStringB actionsName = entry>>"Actions";
	_actions = motion->NewActionMap(&(actionMaps>>actionsName));
	_disableWeapons = entry>>"disableWeapons";
	_disableWeaponsLong = entry>>"disableWeaponsLong";
	_enableOptics = entry>>"enableOptics";
	_showWeaponAim = entry>>"showWeaponAim";
	_enableMissile = entry>>"enableMissile";
	_enableBinocular = entry>>"enableBinocular";

	//_enableFistStroke = entry>>"_enableFistStroke";;
	//_enableGunStroke = entry>>"_enableGunStroke";;

	_showItemInHand = entry>>"showItemInHand";
	_showItemInRightHand = entry>>"showItemInRightHand";
#if _ENABLE_DATADISC
	_showHandGun = entry>>"showHandGun";
#else
	_showHandGun = false;
#endif

	_onLandBeg = entry>>"onLandBeg";
	_onLandEnd = entry>>"onLandEnd";
	_onLadder = entry>>"onLadder";

	_speed = entry>>"speed";
	if (_speed<0) _speed=-1/_speed;
	_duty = entry>>"duty";
	_soundEnabled = entry>>"soundEnabled";

	_soundOverride = entry>>"soundOverride"; // default - no override
	_soundEdge1 = entry>>"soundEdge1"; // default - no override
	_soundEdge2 = entry>>"soundEdge2"; // default - no override

	_relSpeedMin = entry>>"relSpeedMin";
	_relSpeedMax = entry>>"relSpeedMax";
	//_aimingRigid = entry>>"aimingRigid";
	// load aiming blending information
	RStringB aimingName = entry>>"aiming";
	RStringB legsName = entry>>"legs";
	RStringB headName = entry>>"head";
	_aiming = motion->NewBlendAnimType(blendAnimTypes>>aimingName);
	_legs = motion->NewBlendAnimType(blendAnimTypes>>legsName);
	_head = motion->NewBlendAnimType(blendAnimTypes>>headName);

	//_aiming.Load(motion->GetSkeleton(),entry>>"aiming");
	//_legs.Load(motion->GetSkeleton(),entry>>"legs");
	//_head.Load(motion->GetSkeleton(),entry>>"head");

	_equivalentTo = motion->GetMoveId(entry>>"equivalentTo");
	_variantAfterMin = (entry>>"variantAfter")[0];
	_variantAfterMid = (entry>>"variantAfter")[1];
	_variantAfterMax = (entry>>"variantAfter")[2];
	_interpolSpeed = entry>>"interpolationSpeed";
	_interpolRestart = entry>>"interpolationRestart";

	//_neutralHeadXRot = float(entry>>"neutralHeadXRot")*(H_PI/180.0f);
	_limitGunMovement = entry>>"limitGunMovement";

	_terminal = entry>>"terminal"; // all movement stops here

	_aimPrecision = entry>>"aimPrecision";

	_visibleSize = entry>>"visibleSize";
	
	LoadVariants(_variantsPlayer,NULL,entry>>"variantsPlayer");
	LoadVariants(_variantsAI,&_variantsPlayer,entry>>"variantsAI");
}

void MoveInfo::LoadVariants
(
	AutoArray<MoveVariant> &vars, AutoArray<MoveVariant> *defaultVars,
	const ParamEntry &cfg
) const
{
	// check first name
	// if the array is {""}, use default variants (if any)
	if (cfg.GetSize()==1)
	{
		RStringB single = cfg[0];
		if (single.GetLength()==0)
		{
			if (defaultVars)
			{
				vars = *defaultVars;
				return;
			}
			else
			{
				RptF("No default vars in %s",(const char *)cfg.GetContext());
				return;
			}
		}
	}


	float totalP = 0;
	for (int i=0; i<cfg.GetSize(); i+=2)
	{
		RStringB name = cfg[i];
		float prop = i+1<cfg.GetSize() ? cfg[i+1] : 1-totalP;
		MoveVariant &var = vars[vars.Add()];
		var._move = _motion->GetMoveId(name);
		var._probab = prop;
		totalP += prop;
	}
}

MoveId MoveInfo::RandomVariant( const AutoArray<MoveVariant> &vars, float rnd ) const
{
	int i=0;
	for (;i<vars.Size(); i++)
	{
		float prop = vars[i]._probab;
		rnd -= prop;
		if (rnd<=0) break;
	}
	if (i>=vars.Size()) return MoveIdNone;
	return vars[i]._move;
}

float MoveInfo::GetVariantAfter() const
{
	return GRandGen.Gauss(_variantAfterMin,_variantAfterMid,_variantAfterMax);
}

MoveId MoveInfo::RandomVariantPlayer() const
{
	float rnd = GRandGen.RandomValue();
	return RandomVariant(_variantsPlayer,rnd);
}

MoveId MoveInfo::RandomVariantAI() const
{
	float rnd = GRandGen.RandomValue();
	return RandomVariant(_variantsAI,rnd);
}
	


ManType::ManType( const ParamEntry *param )
:VehicleType(param)
{
	_scopeLevel=1;
}

void ManType::Load(const ParamEntry &par)
{
	base::Load(par);

	_lightPos=VZero;
	_lightDir=VZero;

	_isMan = par>>"isMan";
	_minGunElev=(float)(par>>"minGunElev")*(H_PI/180);
	_maxGunElev=(float)(par>>"maxGunElev")*(H_PI/180);
	_minGunTurn=(float)(par>>"minGunTurn")*(H_PI/180);
	_maxGunTurn=(float)(par>>"maxGunTurn")*(H_PI/180);
	_minGunTurnAI=(float)(par>>"minGunTurnAI")*(H_PI/180);
	_maxGunTurnAI=(float)(par>>"maxGunTurnAI")*(H_PI/180);

	_minTriedrElev=-30*(H_PI/180);
	_maxTriedrElev=+60*(H_PI/180);
	_minTriedrTurn=-30*(H_PI/180);
	_maxTriedrTurn=+30*(H_PI/180);

	_minHeadTurnAI = (float)(par>>"minHeadTurnAI")*(H_PI/180);
	_maxHeadTurnAI = (float)(par>>"maxHeadTurnAI")*(H_PI/180);

	_canHideBodies = par>>"canHideBodies";
	_canDeactivateMines = par>>"canDeactivateMines";

	_head.Load(par);

	// _moves will be initialized during InitShape

	_hitSound.Load(par,"hitSounds");

	GetValue(_addSound, par >> "additionalSound");

	_woman = false;
	const ParamEntry *entry = par.FindEntry("woman");
	if (entry) _woman = *entry;
}

/*!
\patch_internal 1.01 Date 6/26/2001 by Ondra. Fixed: getting NVG proxy index.
*/

void ManType::InitShape()
{
	_scopeLevel=2;

	base::InitShape();

	const ParamEntry &par=*_par;

	// turret animations
	//_gun.Init(_shape,"mireni",NULL,"osa mireni");
	//_bazooka.Init(_shape,"mireni2","mireni","osa mireni2","osa mireni");

	//_nvGoggles.Init(_shape, "nvg", NULL);

	for (int l=0; l<MAX_LOD_LEVELS; l++)
	{
		_nvGogglesProxyIndex[l] = -1;
		if (l>=_shape->NLevels()) continue;
		Shape *shape = _shape->Level(l);
		if (!shape) continue;
		for (int p=0; p<shape->NProxies(); p++)
		{
			const ProxyObject &po = shape->Proxy(p);
			// FIX: nvg instead of nvg_proxy
			if (strcmpi(po.name,"nvg_proxy")) continue;
			_nvGogglesProxyIndex[l] = p;
			break;
		}
	}


	Shape *mem=_shape->MemoryLevel();
	Assert( mem );
	
	_cameraPoint = mem ? mem->PointIndex("zamerny") : -1;
	_pilotPoint = mem ? mem->FindNamedSel("pilot") : -1;

	_gunPosIndex=mem ? mem->PointIndex("usti hlavne") : -1;
	_gunEndIndex=mem ? mem->PointIndex("konec hlavne") : -1;
	//Assert( _gunPosIndex>=0 );
	//_gunFire.Init(_shape,"usti hlavne",NULL);

	// find matrix index of gun proxy

	// search in any animation RT
	WoundInfo woundInfo;
	woundInfo.LoadAndRegister(_shape,par>>"wounds");
	// TODO: share texture list between wounds
	_headWound.Init(_shape,woundInfo,"head injury",NULL);
	_bodyWound.Init(_shape,woundInfo,"body injury",NULL);
	_lArmWound.Init(_shape,woundInfo,"l arm injury",NULL);
	_rArmWound.Init(_shape,woundInfo,"r arm injury","p arm injury");
	_lLegWound.Init(_shape,woundInfo,"l leg injury",NULL);
	_rLegWound.Init(_shape,woundInfo,"r leg injury","p leg injury");

	_head.InitShape(par, _shape);

	_headHide.Init(_shape,"hlava",NULL);
	_neckHide.Init(_shape,"krk",NULL);

	// light positions
	_lightPos=_shape->MemoryPoint("L svetlo");
	_lightDir=_shape->MemoryPoint("konec L svetla")-_lightPos;
	_lightDir.Normalize();

	_insideView=0;
	_gunnerView=0;
	//_layDownView=0;
	//_crawlView=0;
	//_launcherView=0;

	_stepLIndex=mem ? mem->PointIndex("stopaL") : -1;
	_stepRIndex=mem ? mem->PointIndex("stopaP") : -1;

	_aimPoint=-1;
	_aimingAxisPoint=-1;
	_headAxisPoint=-1;
	if( mem )
	{
		_aimPoint=mem->PointIndex("zamerny");
		if( _aimPoint<0 )
		{
			LogF("No aim point in %s",(const char *)GetName());
		}
		_aimingAxisPoint=mem->PointIndex("osa mireni");
		_headAxisPoint=mem->PointIndex("osa otaceni");
		if (_headAxisPoint<0) _headAxisPoint = _aimingAxisPoint;
	}

	int level;
	level=_shape->FindSpecLevel(VIEW_PILOT);
	if( level>=0 )
	{
		_insideView=level;
		Shape *shape=_shape->LevelOpaque(level);
		shape->MakeCockpit();
	}
	/*
	level=_shape->FindSpecLevel(VIEW_COMMANDER);
	if( level>=0 )
	{
		_insideView=level;
		Shape *shape=_shape->LevelOpaque(level);
		shape->MakeCockpit();
	}
	*/
	level=_shape->FindSpecLevel(VIEW_GUNNER);
	if( level>=0 )
	{
		_gunnerView=level;
		Shape *shape=_shape->LevelOpaque(level);
		shape->MakeCockpit();
		//_gunnerPilotPos=shape->NamedPosition("pilot");
	}

	//_triedr.Init(_shape,"triedr",NULL);
	//_rpgLauncher.Init(_shape,"roura",NULL);
	//_primaryWeapon.Init(_shape,"zbran",NULL);

	RStringB moveName = par>>"Moves";
	const ParamEntry &moves=Pars>>moveName;
	//const ParamEntry &states=moves>>"States";
	
	MotionType::Load(moves);

	MovesTypeName name;
	name.shape = _shape;
	name.motionType = this;
	_moveType = MovesTypes.New(name);

	// scan animation weights for _gunMatIndex
	if (_isMan)
	{
		_gunMatIndex = _moveType->GetSkeleton()->FindBone("zbran");
		_rpgMatIndex = _moveType->GetSkeleton()->FindBone("roura");
		_handMatIndex = _moveType->GetSkeleton()->FindBone("lruka");
		_rightHandMatIndex = _moveType->GetSkeleton()->FindBone("pruka");
		if (_gunMatIndex<0) ErrF("Bone 'zbran' not found in %s",(const char *)_shape->GetName());
		if (_rpgMatIndex<0) ErrF("Bone 'roura' not found in %s",(const char *)_shape->GetName());
		if (_handMatIndex<0) ErrF("Bone 'lruka' not found in %s",(const char *)_shape->GetName());
		if (_rightHandMatIndex<0) ErrF("Bone 'pruka' not found in %s",(const char *)_shape->GetName());
	}
	else
	{
		_gunMatIndex = -1;
		_rpgMatIndex = -1;
		_handMatIndex = -1;
		_rightHandMatIndex = -1;
	}

	// TODO: search proxies in memory
	Shape *proxyContainer = _shape->MemoryLevel();
	_priWeaponIndex = -1; // index of corresponding proxy
	_secWeaponIndex = -1;
	_handGunIndex = -1;
	if (proxyContainer) for (int i=0; i<proxyContainer->NProxies(); i++)
	{
		const ProxyObject &proxy = proxyContainer->Proxy(i);
		if (dyn_cast<ProxySecWeapon,Object>(proxy.obj)) _secWeaponIndex = i;
		else if (dyn_cast<ProxyHandGun,Object>(proxy.obj)) _handGunIndex = i;
		else if (dyn_cast<ProxyWeapon,Object>(proxy.obj)) _priWeaponIndex = i;
	}
	
	int headIndex = GetSkeleton()->FindBone("hlava");
	if (headIndex>=0)
	{
		_headOnlyWeight.Add(AnimationRTPair(headIndex,1));
	}
	else
	{
		if (_isMan)
		{
			ErrF("Bone 'hlava' not found in %s",(const char *)_shape->GetName());
		}
	}

	
	DEF_HIT(_shape,_headHit,"hlava",NULL,GetArmor()*(float)(par>>"armorHead"));
	DEF_HIT(_shape,_bodyHit,"telo",NULL,GetArmor()*(float)(par>>"armorBody"));
	DEF_HIT(_shape,_handsHit,"ruce",NULL,GetArmor()*(float)(par>>"armorHands"));
	DEF_HIT(_shape,_legsHit,"nohy",NULL,GetArmor()*(float)(par>>"armorLegs"));
}


void ManType::DeinitShape()
{
	_headWound.Unload();
	_bodyWound.Unload();
	_lArmWound.Unload();
	_rArmWound.Unload();
	_lLegWound.Unload();
	_rLegWound.Unload();
	
	MotionType::Unload();
	_moveType = NULL;
	// scan bank and remove and movetypes not attached to any shape
	// TODO: compact MovesTypes
}

ActionMap *ManType::GetActionMap( MoveId move ) const
{
	ActionMap *map = _moveType->GetActionMap(move);
	if (!map) map = MotionType::GetNoActions();
	return map;
}

void Soldier::FakePilot( float deltaT )
{
	_turnWanted = 0;
	_walkSpeedWanted = 0;
}

void Soldier::SuspendedPilot(float deltaT, SimulationImportance prec)
{
	_turnWanted=0;
	//_sideSpeedWanted=0;
	_walkSpeedWanted=0;

	// set primary move to stand

	AdvanceExternalQueue();

	if (_externalMove.id!=MoveIdNone)
	{
		if (SetMoveQueue(_externalMove,prec<=SimulateVisibleFar))
		{
			//AdvanceExternalQueue(false);
		}
	}
	else
	{
		// stand still
		ActionMap *map = Type()->GetActionMap(_primaryMove.id);
		MoveId moveWanted = map ? map->GetAction(ManActStop) : GetDefaultMove();
		SetMoveQueue(MotionPathItem(moveWanted),prec<=SimulateVisibleFar);
	}
}

CursorMode Man::GetCursorRelMode(CameraType camType) const
{
	// relative cursor only in virtual cockpit/view
	// and only when mouse is not active
	if (camType==CamInternal || camType==CamExternal)
	{
		if
		(
			IsVirtual(camType) &&
			!GInput.lookAroundEnabled &&
			!GInput.MouseTurnActive()
			//&& GWorld->MouseControlEnabled())
		)
		{
			return CKeyboard;
		}
		else
		{
			return CMouseAbs;
		}	
	}
	else if (camType==CamGunner) return CMouseAbs;
	else
	{
		return base::GetCursorRelMode(camType);
	}
}

bool Man::IsWeaponInMove() const
{
	int actPos = GetActUpDegree();
	return
	(
		actPos!=ManPosNoWeapon && actPos!=ManPosLyingNoWeapon
	);
}

bool Man::IsBinocularInMove() const
{
	int actPos = GetActUpDegree();
	return
	(
		actPos==ManPosBinoc || actPos==ManPosBinocLying || actPos==ManPosBinocStand
	);
}

/*
bool Man::IsOpticsInMove() const
{
	int actPos = GetActUpDegree();
	return
	(
		actPos==ManPosCombatOptics
	);
}
*/

void Man::UnselectLauncher()
{
	if (!LauncherSelected()) return;
	int weapon = FirstWeapon();
	if (weapon==SelectedWeapon()) weapon = -1;
	SelectWeapon(weapon);
}

/*!
\patch 1.01 Date 6/25/2001 by Ondra.
- Fixed: reloading weapon on back disabled.
\patch 1.01 Date 6/25/2001 by Ondra.
- Fixed: turning enabled while reloading.
\patch_internal 1.04 Date 6/25/2001 by Ondra
- Fixed: wanted camera checked instead of actual when determining
if optics moves should be used.
\patch 1.05 Date 7/17/2001 by Ondra.
- New: Toggle walking mode with 'F'.
\patch 1.22 Date 8/30/2001 by Ondra.
- Fixed: Player character was sprinting when running up very steep hill.
- Improved: Walking is used when running up or down steep hill to slow down.
*/

void Soldier::KeyboardPilot(float deltaT, SimulationImportance prec)
{
	#if 0 //_ENABLE_REPORT
	if (ToMoveOut() && GWorld->GetMode()==GModeNetware)
	{
		LogF("No keyboard pilot should be called now");
	//	return;
	}
	#endif
	AIUnit *unit = Brain();
	float turnWanted=0;
	if (!_isDead)
	{
		bool internalCamera = IsGunner(GWorld->GetCameraType()) && GWorld->CameraOn()==this;
		//if( GInput.MouseTurnActive() && internalCamera )
		if (internalCamera && GInput.MouseTurnActive() && !GInput.lookAroundEnabled)
		// last input from mouse - use mouse controls
		{
			_turnWanted=0;
			if( _gunYRotWanted<Type()->_minGunTurn )
			{
				// force turn right
				_turnToDo=Type()->_minGunTurn-_gunYRotWanted;
				_gunYRotWanted=Type()->_minGunTurn;
			}
			else if( _gunYRotWanted>Type()->_maxGunTurn )
			{
				// force turn left
				_turnToDo=Type()->_maxGunTurn-_gunYRotWanted;
				_gunYRotWanted=Type()->_maxGunTurn;
			}
		}
		else
		{
			// turn with arrows
			turnWanted=(GInput.GetAction(UAMoveRight)-GInput.GetAction(UAMoveLeft))*1.5f;

			if (GInput.lookAroundEnabled)
			{
				saturate(_gunYRotWanted,Type()->_minGunTurnAI,Type()->_maxGunTurnAI);
			}
			else
			{
				saturate(_gunYRotWanted,Type()->_minGunTurn,Type()->_maxGunTurn);
			}
		}
	}

	// FIX first check turning, then external move
	AdvanceExternalQueue();
	if (_externalMove.id!=MoveIdNone)
	{
		_turnWanted=0;
		//_sideSpeedWanted=0;
		//_walkSpeedWanted=0;
		if (SetMoveQueue(_externalMove,prec<=SimulateVisibleFar))
		{
			//AdvanceExternalQueue(false);
		}
		return;
	}

	if( !unit ) return;

	if (GInput.GetActionToDo(UASlow))
	{
		_walkToggle = !_walkToggle;
	}

	float fbWanted=
	(
		(GInput.keyMoveFastForward>0)*2.0f+
		(GInput.keyMoveForward>0)*1.0f+
		(GInput.keyMoveSlowForward>0)*0.5f+
		(GInput.keyMoveBack>0)*-1.0f
	);

	bool turbo = GInput.GetAction(UATurbo) > 0.5f;

	float rlWanted = GInput.GetAction(UATurnRight)-GInput.GetAction(UATurnLeft);

	{
		float delta=turnWanted-_turnWanted;
		saturate(delta,-3*deltaT,+3*deltaT);
		_turnWanted+=delta;

		if (GInput.lookAroundEnabled)
		{
			saturate(_gunYRotWanted,Type()->_minGunTurnAI,Type()->_maxGunTurnAI);
		}
		else
		{
			saturate(_gunYRotWanted,Type()->_minGunTurn,Type()->_maxGunTurn);
		}

		saturate(_turnWanted,-3,+3);
	}

	ManAction selAction = ManActStop;
	bool forceAction = false;

	if (GInput.GetActionToDo(UAReloadMagazine))
	{
		// FIX if weapons are long term disabled, take weapon in hand
		if (DisableWeaponsLong())
		{
			if (_currentWeapon<0)
			{
				SelectWeapon(FirstWeapon());
			}
			if (_currentWeapon>=0)
			{
				FireAttemptWhenNotPossible();
			}
		}
		else
		// END OF FIX

		if (_currentWeapon >= 0 && EnableWeaponManipulation())
		{
			const MagazineSlot &slot = GetMagazineSlot(_currentWeapon);
			const Magazine *magazine = slot._magazine;
			const MagazineType *mType = magazine ? magazine->_type : NULL;
			int best = FindMagazineByType(slot._muzzle, mType);
			if (best >= 0)
			{
				const Magazine *magazine = GetMagazine(best);
				RString muzzleID = slot._weapon->GetName() + RString("|") + slot._muzzle->GetName();
				Ref<ActionContextDefault> context = new ActionContextDefault;
				context->function = MFReload;
				context->param = magazine->_creator;
				context->param2 = magazine->_id;
				context->param3 = muzzleID;
				if (PlayAction(magazine->_type->_reloadAction, context))
				{
					PlayReloadMagazineSound(_currentWeapon,slot._muzzle);
				}
				else
				{
					// reload now
					ReloadMagazineTimed(_currentWeapon, best, false);
				}
			}
		} 
	}

	if (GInput.GetActionToDo(UABinocular))
	{
		if (BinocularSelected())
		{
			int slot = IsHandGunSelected() ? MaskSlotHandGun : MaskSlotPrimary;
			bool found = false;
			for (int i=0; i<_magazineSlots.Size(); i++)
			{
				const WeaponType *type = _magazineSlots[i]._weapon;
				if (!type) continue;
				if (type->_weaponType & slot)
				{
					SelectWeapon(i);
					found = true;
					break;
				}
			}
			if (!found) SelectWeapon(-1);
		}
		else
		{
			Ref<WeaponType> binocular = WeaponTypes.New("Binocular");
			Assert(binocular);
			for (int i=0; i<_magazineSlots.Size(); i++)
			{
				const WeaponType *type = _magazineSlots[i]._weapon;
				if (type == binocular)
				{
					SelectWeapon(i);
					break;
				}
			}
		}
	}

	if (GInput.GetActionToDo(UAHandgun))
	{
		if (IsHandGunSelected() && !IsHandGunInMove())
		{
			PlayAction(ManActHandGunOn);
		}
		else
		{
			int index = -1;
			if (IsHandGunSelected())
				index = FindWeaponType(MaskSlotPrimary);
			else
			{
				index = FindWeaponType(MaskSlotHandGun);
				if (GetActUpDegree() == ManPosStand) PlayAction(ManActHandGunOn);
			}

			if (index >= 0)
			{
				const WeaponType *weapon = GetWeaponSystem(index);
				for (int i=0; i<NMagazineSlots(); i++)
					if (GetMagazineSlot(i)._weapon == weapon)
					{
						SelectWeapon(i);
						break;
					}
			}
		}
	}
	
	bool forceStand = false;
	float slope = LandSlope(forceStand);

	ActionMap *map = Type()->GetActionMap(_primaryMove.id);

	// check reaction for no weapon

	int primaryIndex = FindWeaponType(MaskSlotPrimary);
	int handGunIndex = FindWeaponType(MaskSlotHandGun);

	bool isWeapon = primaryIndex >= 0 || handGunIndex >= 0;
	int actPos = GetActUpDegree();

	bool wantSlow = false;
	if (GWorld->FocusOn() == unit)
	{
		// change
		wantSlow = _walkToggle || GWorld->GetCameraTypeWanted()==CamGunner;
	}

	// check if we are using binocular
	#ifdef _XBOX
	if( GInput.GetActionToDo(UAMoveUp) )
	#else
	if( GInput.keyMoveUp>0.1f )
	#endif
	{
		selAction = ManActUp;
		forceAction = true;
		// if missile is selected, de-select it
		UnselectLauncher();
		if (IsHandGunSelected() && !IsHandGunInMove())
		{
			if (actPos == ManPosStand)
				selAction = ManActHandGunOn;
			else
				SelectPrimaryWeapon();
		}
	}
	#ifdef _XBOX
	else if( GInput.GetActionToDo(UAMoveDown) )
	#else
	else if( GInput.keyMoveDown>0.1f )
	#endif
	{
		selAction = ManActDown;
		forceAction = true;
		// if missile is selected, de-select it
		UnselectLauncher();
		if (IsHandGunSelected() && !IsHandGunInMove())
		{
			if (actPos == ManPosStand)
				selAction = ManActHandGunOn;
			else
				SelectPrimaryWeapon();
		}
	}
	else if (BinocularSelected())
	{
		selAction = ManActBinocOn; // use binocular
	}
	else if (IsBinocularInMove())
	{
		selAction = ManActBinocOff; // do not use binocular
	}
	else if (isWeapon!=IsWeaponInMove() && !LauncherSelected())
	{
		if (IsWeaponInMove())
		{
			if (actPos==ManPosLying) selAction = ManActCivilLying;
			else selAction = ManActCivil;
		}
		else if (IsHandGunSelected())
		{
			selAction = ManActHandGunOn;
		}
		else
		{
			if (actPos==ManPosLyingNoWeapon) selAction = ManActLying;
			else selAction = ManActCombat;
		}
	}
	else if
	(
		LauncherSelected() && GetActUpDegree()!=ManPosWeapon
	)
	{
		selAction = ManActWeaponOn;
		forceAction = true;
	}
	else if( GetActUpDegree()==ManPosWeapon && !LauncherSelected() )
	{
		selAction = IsHandGunSelected() ? ManActHandGunOn : ManActWeaponOff;
		forceAction = true;
	}
	else if(!IsAbleToStand() && !IsDown() && !LauncherSelected())
	{
		if (IsHandGunSelected())
		{
/*			
			if (actPos == ManPosLying || actPos == ManPosHandGunLying)
				selAction = ManActDown;
			else
				selAction = ManActUp;
*/
			selAction = ManActDown;
		}
		else selAction = ManActLying;
		forceAction = true;
		Scream(NULL);
	}
	else if(!forceStand && !CanStand(slope) && !IsDown() && !LauncherSelected())
	{
		if (IsHandGunSelected())
		{
/*
			if (actPos == ManPosLying || actPos == ManPosHandGunLying)
				selAction = ManActDown;
			else
				selAction = ManActUp;
*/
			selAction = ManActDown;
		}
		else selAction = ManActLying;
		forceAction = true;
	}
	else if (IsHandGunSelected() && IsPrimaryWeaponInMove())
	{
		selAction = ManActHandGunOn; // use hand gun
	}
	else if (!IsHandGunSelected() && IsHandGunInMove())
	{
		if (actPos == ManPosHandGunCrouch)
		{
			if (primaryIndex < 0)
				selAction = ManActCivil;
			else
				selAction = ManActCrouch;
		}
		else if (actPos == ManPosHandGunLying)
		{
			if (primaryIndex < 0)
				selAction = ManActCivilLying;
			else
				selAction = ManActLying;
		}
		else
		{
			Assert(actPos == ManPosHandGunStand);
			if (primaryIndex < 0)
				selAction = ManActCivil;
			else
				selAction = ManActCombat;
		}
	}
	else if (!forceAction)
	{
		// check which action we should take - based on keys
		int fastFlag = turbo;
		int rlFlag = 0, fbFlag = 0;
		if( fabs(rlWanted)>+1.5f ) fastFlag = 1;
		if( fabs(fbWanted)>+1.5f ) fastFlag = 1;

		if( rlWanted>+0.5f ) rlFlag = +1;
		else if( rlWanted<-0.5f ) rlFlag = -1;
		if( fbWanted>+0.5f ) fbFlag = +1;
		else if( fbWanted<-0.5f ) fbFlag = -1;

		if (IsDown())
		{
			if (!_canMoveFast) fastFlag = 0;
			fastFlag += 1;
		}
		else
		{
			//GlobalShowMessage(100, "Slope %.2f",slope);
			if (!_canMoveFast || !CanSprint(slope)) fastFlag = 0;
			fastFlag += 1;
			if (wantSlow && fastFlag==1 || !CanRun(slope)) fastFlag = 0;
		}


		// map flags to action
		// check which action map should be used now
		// suppose we are doing _primaryMove
		// map to eight directions/two levels of stress based on values
#ifdef __GNUC__
		static const int selectAction[3][3][3]=
#else
		static const ManAction selectAction[3][3][3]=
#endif
		{
			{ // walk action
				{ManActWalkLB,ManActWalkB,ManActWalkRB},// back L|R 
				{ManActWalkL,ManActStop,ManActWalkR},// L|R 
				{ManActWalkLF,ManActWalkF,ManActWalkRF},// front L|R 
			},
			{ // slow action
				{ManActSlowLB,ManActSlowB,ManActSlowRB},// back L|R 
				{ManActSlowL,ManActStop,ManActSlowR},// L|R 
				{ManActSlowLF,ManActSlowF,ManActSlowRF},// front L|R 
			},
			{ // fast action
				{ManActFastLB,ManActFastB,ManActFastRB},// back L|R 
				{ManActFastL,ManActStop,ManActFastR},// L|R 
				{ManActFastLF,ManActFastF,ManActFastRF},// front L|R 
			},
		};
		// get action ID
		selAction = (ManAction)selectAction[fastFlag][fbFlag+1][rlFlag+1];

	}

	/*
	if( selAction!=ManActWeaponOn && GetActUpDegree()==ManPosWeapon )
	{
		selAction = ManActWeaponOff;
		forceAction = true;
	}
	*/

	/*
	if( selAction!=ManActStop )
	{
		LogF("Action %s",GetActionName(selAction));
	}
	*/

	//_walkSpeedWanted = 0;

	//if( fbWanted>1.5 ) _walkSpeedWanted = FastRunSpeed;
	//else if( fbWanted>0.75 ) _walkSpeedWanted = RunSpeed;
	//else if( fbWanted>0 ) _walkSpeedWanted = WalkSpeed;
	//else if( fbWanted<0 ) _walkSpeedWanted = -WalkBackSpeed;

	//_sideSpeedWanted=rlWanted*2;
	//if( rlWanted ) _walkSpeedWanted=0;

	// get which action is it (based on action map)

	if (selAction==ManActStop)
	{
		if( fabs(_turnWanted)>0.5f)
		{
			selAction = turnWanted<0 ? ManActTurnL : ManActTurnR;
		}
	}

	MoveId moveWanted = map ? map->GetAction(selAction) : GetDefaultMove();
	//LogF("%s: Move selected %s",DNAME(),NAME(moveWanted));
	// 

	// some actions are edge sensitive - must be executed
	if( _forceMove.id!=MoveIdNone )
	{
		SetMoveQueue(_forceMove,prec<=SimulateVisibleFar);
	}
	else
	{
		MotionPathItem item = MotionPathItem(moveWanted);
		if (SetMoveQueue(item,prec<=SimulateVisibleFar))
		{
			if( forceAction ) _forceMove = item;
		}
	}

	AutoGuide(deltaT); // brake and avoid


	AIGroup *g = GetGroup();
	if (g && g->Leader() == unit)
	{
		// track upDegree change

		int actDegree = GetActUpDegree();
		// got to combat - can be quite quick
		saturate(actDegree,ManPosLying,ManPosStand);
		if (actDegree<=_upDegreeStable)
		{
			_upDegreeStable = actDegree;
			_upDegreeChangeTime = Glob.time;
		}
		else if (_upDegreeChangeTime<Glob.time-10)
		{
			// up degree is higher for quite a long time
			// might slowly recover to safer degree
			_upDegreeStable++;
			_upDegreeChangeTime = Glob.time;
		}

		// autodetect combat mode depending on UpDegree
		// SetCombatModeMajor() // set by waypoint
		// SetCombatModeMinor() // autodetection
		// map from ManPos to CombatMode
		CombatMode autoCM = g->GetCombatModeMinor();
		if( _upDegreeStable<ManPosNormalMin ) autoCM = CMCombat;
		else if( _upDegreeStable<=ManPosLying ) autoCM = CMCombat;
		else if( _upDegreeStable<=ManPosCombat ) autoCM = CMAware;
		else if (_upDegreeStable<ManPosNormalMax) autoCM = CMSafe;
		//LogF("SetCombatMode %d",autoCM);
		g->SetCombatModeMinor(autoCM);

	}

	CheckAway();
}

int Man::GetAutoUpDegree() const
{
	AIUnit *unit = Brain();
	if (!unit) return ManPosStand;
	return GetUpDegree(unit->GetCombatMode(), IsHandGunSelected());
}

MoveId Man::GetDefaultMove() const
{
	int deg = GetAutoUpDegree();
	return Type()->GetDefaultMove(deg);
}

MoveId Man::GetDefaultMove(ManAction action) const
{
	if (_isDead)
	{
		action = ManActDie;
	}
	int deg = GetAutoUpDegree();
	return Type()->GetMove(deg, action);
}

MoveId Man::GetMove(ManAction action) const
{
	// get action map corresponding to current move
	const ActionMap *map = Type()->GetActionMap(_primaryMove.id);
	if (map)
	{
		return map->GetAction(action);
	}

	int deg = GetActUpDegree();
	//if (action<ManActNormN) return Type()->GetMove(deg, action);
	// TODO: use some other action list
	return Type()->GetMove(deg, action);
}

MoveId Man::GetVehMove(ManVehAction action) const
{
	const ActionVehMap &map = Type()->GetActionVehMap();
	if (action<0)
	{
		Fail("Vehicle action ManVehActNone used");
		return MoveIdNone;
	}
	if (action>=map.GetMaxAction())
	{
		ErrF("Invalid vehicle action %d (should be 0 to %d)",action,map.GetMaxAction());
		return MoveIdNone;
	}
	return Type()->GetActionVehMap().GetAction(action);
}
 // based on actual move

Texture *Man::GetCursorTexture(Person *person)
{
	// check if target designator is active
	/*
	if (_laserTargetOn)
	{
		return GPreloadedTextures.New(CursorLocked);

	}
	*/
	if (WeaponsDisabled()) return NULL;
	// check if 
	return base::GetCursorTexture(person);
}

Texture *Man::GetCursorAimTexture(Person *person)
{
	// check if target designator is active
	if (_laserTargetOn)
	{
		return GPreloadedTextures.New(CursorLocked);

	}
	if (WeaponsDisabled()) return NULL;
	// check if 
	return base::GetCursorAimTexture(person);
}

Texture *Man::GetFlagTexture()
{
	if (_flagCarrier) return _flagCarrier->GetFlagTextureInternal();
	return NULL;
}

void Man::SetFlagOwner(Person *veh)
{
	if (_flagCarrier)
		_flagCarrier->SetFlagOwner(veh);
}

bool CheckSupply(EntityAI *vehicle, EntityAI *parent, SupportCheckF check, float limit, bool now);

static RString FormatWeaponSystem
(
	const char *format, EntityAI *veh, int weaponIndex
)
{
	if (veh)
	{
		if (weaponIndex >= 0 && weaponIndex < veh->NWeaponSystems())
		{
			const WeaponType *weapon = veh->GetWeaponSystem(weaponIndex);
			RStringB displayName = weapon->_displayName;
			char buffer[512];
			sprintf
			(
				buffer, format,
				(const char *)displayName
			);
			return buffer;
		}
		else
		{
			RptF("Weapon action - bad weapon %d", weaponIndex);
			return "Weapon Error";
		}
	}
	else
	{
		RptF("Weapon action - bad target");
		return "Weapon Error";
	}
}

RString Man::GetActionName(const UIAction &action)
{
	switch (action.type)
	{
		case ATTouchOff:
			{
				int n = 0;
				for (int i=0; i<_pipeBombs.Size();)
					if (_pipeBombs[i])
					{
						if (_pipeBombs[i]->Position().Distance2(Position()) <= Square(300)) n++;
						i++;
					}
					else _pipeBombs.Delete(i);
				char buffer[256];
				sprintf(buffer, LocalizeString(IDS_ACTION_TOUCH_OFF), n);
				return buffer;
			}
		case ATSetTimer:
			{
				float minDist2 = Square(3);
				PipeBomb *nearest = NULL;
				for (int i=0; i<_pipeBombs.Size();)
				{
					Vehicle *veh = _pipeBombs[i];
					PipeBomb *bomb = dyn_cast<PipeBomb>(veh);
					if (!bomb)
					{
						_pipeBombs.Delete(i);
						continue;
					}
					Assert(bomb->GetOwner() == this);
					float dist2 = Position().Distance2(bomb->Position());
					if (dist2 < minDist2)
					{
						nearest = bomb;
						minDist2 = dist2;
					}
					i++;
				}
				if (nearest)
				{
					float time = nearest->GetTimer();
					char buffer[256];
					if (time > 1e6)
						sprintf(buffer, LocalizeString(IDS_ACTION_START_TIMER), 30.0f);
					else
						sprintf(buffer, LocalizeString(IDS_ACTION_SET_TIMER), 30.0f, time);
					return buffer;
				}
				else
					return "Error - bomb not found";
			}
		case ATDeactivate:
			return LocalizeString(IDS_ACTION_DEACTIVATE);
		case ATWeaponOnBack:
			return FormatWeaponSystem
			(
				LocalizeString(IDS_ACTION_WEAPONONBACK),this,action.param
			);
		case ATWeaponInHand:
			return FormatWeaponSystem
			(
				LocalizeString(IDS_ACTION_WEAPONINHAND),this,action.param
			);

		case ATStrokeFist:
			return LocalizeString(IDS_ACTION_STROKEFIST);
		case ATStrokeGun:
			return LocalizeString(IDS_ACTION_STROKEGUN);
		case ATHandGunOn:
			{
				int index = FindWeaponType(MaskSlotHandGun);
				if (GetActUpDegree() == ManPosStand)
					return FormatWeaponSystem
					(
						LocalizeString(IDS_ACTION_WEAPONINHAND), this, index
					);
				else
					return FormatWeaponSystem
					(
						LocalizeString(IDS_ACTION_WEAPON), this, index
					);
			}
			// return LocalizeString(IDS_ACTION_HANDGUNON);
		case ATHandGunOff:
			{
				int index = FindWeaponType(MaskSlotPrimary);
				if (GetActUpDegree() == ManPosStand)
					return FormatWeaponSystem
					(
						LocalizeString(IDS_ACTION_WEAPONINHAND), this, index
					);
				else
					return FormatWeaponSystem
					(
						LocalizeString(IDS_ACTION_WEAPON), this, index
					);
			}
			// return LocalizeString(IDS_ACTION_HANDGUNOFF);
		case ATDeactivateMine:
			return LocalizeString(IDS_ACTION_DEACTIVATE_MINE);
		case ATTakeMine:
			return LocalizeString(IDS_ACTION_TAKE_MINE);
		default:
			return base::GetActionName(action);
	}
}

void Man::ProcessUIAction(const UIAction &action)
{
	// this function is called when MFUIAction is processed
	// (actions peformed after some animation is played)
	AIUnit *unit = Brain();
	action.Process(unit);
}

/*!
\patch 1.05 Date 7/18/2001 by Jirka
- Fixed: hide body sometimes did not work in MP
*/
void Man::PerformAction(const UIAction &action, AIUnit *unit)
{
	if (!unit || unit->GetLifeState()!=AIUnit::LSAlive) return;
	// note: action is performed by target of the action
	// unit is who activated the action
	switch (action.type)
	{
		case ATHideBody:
		{
			EntityAI *tgt = action.target;
			if (tgt)
			{
				Man *man = dyn_cast<Man>(tgt);
				if (man)
				{
					// FIX
					if (man->IsLocal())
						man->_hideBodyWanted = 1;
					else
						GetNetworkManager().AskForHideBody(man);
				}
			}
			break;
		}
		case ATTouchOff:
		{
		
			_lastShotAtAssignedTarget = Glob.time;
			for (int i=0; i<_pipeBombs.Size();)
			{
				Vehicle *veh = _pipeBombs[i];
				PipeBomb *bomb = dyn_cast<PipeBomb>(veh);
				if (!bomb)
				{
					_pipeBombs.Delete(i);
					continue;
				}
				Assert(bomb->GetOwner() == this);
				if (Position().Distance2(bomb->Position()) > Square(300.0f))
				{
					i++;
					continue;
				}
				bomb->Explode();
				_pipeBombs.Delete(i);
			}
			break;
		}
		case ATSetTimer:
		{
		
			_lastShotAtAssignedTarget = Glob.time;
			float minDist2 = Square(3);
			PipeBomb *nearest = NULL;
			for (int i=0; i<_pipeBombs.Size();)
			{
				Vehicle *veh = _pipeBombs[i];
				PipeBomb *bomb = dyn_cast<PipeBomb>(veh);
				if (!bomb)
				{
					_pipeBombs.Delete(i);
					continue;
				}
				Assert(bomb->GetOwner() == this);
				float dist2 = Position().Distance2(bomb->Position());
				if (dist2 < minDist2)
				{
					nearest = bomb;
					minDist2 = dist2;
				}
				i++;
			}
			if (nearest)
			{
				float time = nearest->GetTimer();
				if (time > 1e6) time = 0;
				nearest->SetTimer(time + 30);
			}
			break;
		}
		case ATDeactivate:
			{
				float minDist2 = Square(3);
				PipeBomb *nearest = NULL;
				int index = -1;
				for (int i=0; i<_pipeBombs.Size();)
				{
					Vehicle *veh = _pipeBombs[i];
					PipeBomb *bomb = dyn_cast<PipeBomb>(veh);
					if (!bomb)
					{
						_pipeBombs.Delete(i);
						continue;
					}
					Assert(bomb->GetOwner() == this);
					float dist2 = Position().Distance2(bomb->Position());
					if (dist2 < minDist2)
					{
						nearest = bomb;
						index = i;
						minDist2 = dist2;
					}
					i++;
				}
				if (nearest)
				{
					nearest->SetDelete();
					_pipeBombs.Delete(index);
					AddMagazine("PipeBomb", false);
				}
			}
			break;
		case ATDeactivateMine:
			{
				float minDist2 = Square(3);
				Mine *nearest = NULL;

				int xMin, xMax, zMin, zMax;
				ObjRadiusRectangle(xMin, xMax, zMin, zMax, Position(), Position(), 3);
				for (int x=xMin; x<=xMax; x++) for (int z=zMin; z<=zMax; z++)
				{
					const ObjectList &list = GLandscape->GetObjects(z, x);
					int n = list.Size();
					for (int i=0; i<n; i++)
					{
						Object *obj = list[i];
						if (!obj) continue;

						float dist2 = obj->Position().Distance2(Position());
						if (dist2 > minDist2) continue;

						Mine *mine = dyn_cast<Mine>(obj);
						if (!mine) continue;
						if (!mine->IsActive()) continue;
						
						nearest = mine;
						minDist2 = dist2;
					}
				}
				if (nearest)
				{
					if (nearest->IsLocal())
						nearest->SetActive(false);
					else
						GetNetworkManager().AskForActivateMine(nearest, false);
				}
			}
			break;
		case ATTakeMine:
			{
				float minDist2 = Square(3);
				Mine *nearest = NULL;

				int xMin, xMax, zMin, zMax;
				ObjRadiusRectangle(xMin, xMax, zMin, zMax, Position(), Position(), 3);
				for (int x=xMin; x<=xMax; x++) for (int z=zMin; z<=zMax; z++)
				{
					const ObjectList &list = GLandscape->GetObjects(z, x);
					int n = list.Size();
					for (int i=0; i<n; i++)
					{
						Object *obj = list[i];
						if (!obj) continue;

						float dist2 = obj->Position().Distance2(Position());
						if (dist2 > minDist2) continue;

						Mine *mine = dyn_cast<Mine>(obj);
						if (!mine) continue;
						
						nearest = mine;
						minDist2 = dist2;
					}
				}
				if (nearest)
				{
					const AmmoType *ammo = nearest->Type();
					if (ammo)
					{
						RString name = ammo->_defaultMagazine;
						if (name.GetLength() > 0)
						{
							Ref<MagazineType> magazineType = MagazineTypes.New(name);
							Ref<Magazine> magazine = new Magazine(magazineType);
							magazine->_ammo = magazineType->_maxAmmo;
					
							AUTO_STATIC_ARRAY(Ref<const Magazine>, conflict, 16);
							if (CheckMagazine(magazine, conflict) && conflict.Size() == 0)
							{
								if (nearest->IsLocal())
									nearest->SetDelete();
								else
									GetNetworkManager().AskForDeleteVehicle(nearest);
								AddMagazine(magazine, false, true);
							}
						}
					}
				}
			}
			break;
/*
		case ATNVGoggles:
			_nvg = !_nvg;
			break;
*/
		case ATWeaponInHand:
			Assert(action.target == this);
			if (IsHandGunSelected())
				PlayAction(ManActHandGunOn);
			else
				PlayAction(ManActCombat);
			break;
		case ATWeaponOnBack:
			Assert(action.target == this);
			// deselect missile launcher if necessary
			if (LauncherSelected())
			{
				UnselectLauncher();
			}
			PlayAction(ManActStand);
			break;
		case ATSitDown:
			Assert(action.target == this);
			PlayAction(ManActSitDown);
			break;
		case ATSalute:
			Assert(action.target == this);
			PlayAction(ManActSalute);
			break;
		case ATStrokeFist:
			Assert(action.target == this);
			PlayAction(ManActStrokeFist);
			break;			
		case ATStrokeGun:
			Assert(action.target == this);
			PlayAction(ManActStrokeGun);
			break;			
		case ATHandGunOn:
			if (!IsHandGunSelected())
			{
				SelectHandGunWeapon();
				if (GetActUpDegree() == ManPosStand) PlayAction(ManActHandGunOn);
			}
			break;
		case ATHandGunOff:
			if (IsHandGunSelected())
			{
				SelectPrimaryWeapon();
				if (GetActUpDegree() == ManPosStand) PlayAction(ManActCombat);
			}
			break;
		default:
			base::PerformAction(action,unit);
			break;
	}
}

static bool CheckEnemySoldier(AIUnit *unit,Vector3Par pos, Vector3Par dir)
{
	float hitDistance = 0.7f;
	float hitRadius = 0.8f;

	// check circular area (pos+dir*hitDistance,hitRadius)
	// scan targets? or vehicles? or objects?

	Vector3 center = pos+dir*hitDistance;
	int xMin,xMax,zMin,zMax;
	ObjRadiusRectangle(xMin,xMax,zMin,zMax,center,center,hitRadius);
	AIGroup *myGroup = unit->GetGroup();
	if (!myGroup) return false;
	AICenter *myCenter = myGroup->GetCenter();
	if (!myCenter) return false;

	for (int x=xMin; x<=xMax; x++) for (int z=zMin; z<=zMax; z++)
	{
		const ObjectList &list = GLandscape->GetObjects(z, x);
		for (int i=0; i<list.Size(); i++)
		{
			Object *obj = list[i];
			if (obj->Position().DistanceXZ2(center)>hitRadius) continue;
			Man *man = dyn_cast<Man>(obj);
			if (!man) continue;
			// TODO: consider checking vehicle armor
			// enable hitting any soft object
			if (man->IsDown()) continue;
			if (!myCenter->IsEnemy(man->GetTargetSide())) continue;
			return true;
			
		}
	}
	return false;


}

/*!
\patch_internal 1.01 Date 6/25/2001 by Ondra.
Changes necessary because EnableWeaponManipulation function has been changed.
*/

void Man::GetActions(UIActions &actions, AIUnit *unit, bool now)
{
	if (_hideBody>0) return;

	base::GetActions(actions, unit, now);
	
	if (!unit) return;
	bool free = unit->IsFreeSoldier();

	Person *veh = unit->GetPerson();

	if (GetAllocSupply() && GetAllocSupply() != veh) return; // wait

	if (_isDead && CheckSupply(veh, this, NULL, 0, now))
	{
		if (free)
		{
			bool reloading = unit->GetPerson()->IsActionInProgress(MFReload);
			if (!reloading)
			{
				// corpse
				for (int i=0; i<NWeaponSystems(); i++)
				{
					const WeaponType *weapon = GetWeaponSystem(i);
					if (weapon->_scope < 2) continue;
					AUTO_STATIC_ARRAY(Ref<const WeaponType>, conflict, 16);
					if (!veh->CheckWeapon(weapon, conflict)) continue;
					actions.Add(ATTakeWeapon, this, 0.52f, 0, true, false, 0, weapon->GetName());
				}

				int n = NMagazines();
				AUTO_STATIC_ARRAY(bool, checked, 16);
				checked.Resize(n);
				for (int i=0; i<n; i++) checked[i] = false;
				for (int i=0; i<n; i++)
				{
					if (checked[i]) continue;
					checked[i] = true;
					const Magazine *magazine = GetMagazine(i);
					if (magazine->_type->_scope < 2) continue;
					if (magazine->_ammo == 0) continue;
					if (IsMagazineUsed(magazine)) continue;
					if (!veh->IsMagazineUsable(magazine->_type)) continue;
					for (int j=i+1; j<n; j++)
					{
						if (GetMagazine(j)->_type == magazine->_type)
						{
							checked[j] = true;
							if (GetMagazine(j)->_ammo > magazine->_ammo && !IsMagazineUsed(GetMagazine(j)))
								magazine = GetMagazine(j);
						}
					}
					AUTO_STATIC_ARRAY(Ref<const Magazine>, conflict, 16);
					if (!veh->CheckMagazine(magazine, conflict)) continue;
					actions.Add(ATTakeMagazine, this, 0.53f, 0, true, false, 0, magazine->_type->GetName());
				} // for all magazines

				// check if Person is able to hide bodies
				if (_hideBodyWanted==0) // body not hidden yet
				{
					Man *vehMan = dyn_cast<Man>(veh);
					if (vehMan && vehMan->Type()->_canHideBodies)
					{
						actions.Add(ATHideBody, this, 0.51f);
					}
				}


			} // if (!reloading)
		}

		if (_flagCarrier && GetFlagTexture())
		{
			TargetSide side = _flagCarrier->GetFlagSide();
			AIGroup *grp = unit->GetGroup();
			AICenter *center = grp ? grp->GetCenter() : NULL;
			if (center)
			{
				if (center->IsEnemy(side))
				{
					actions.Add(ATTakeFlag, this, 0.99f, 0, true, true); //***
				}
				else if (center->IsFriendly(side))
				{
					actions.Add(ATReturnFlag, this, 0.99f, 0, true, true);
				}
			}
		}
	} // dead body
	else
	{
		// living
		if (unit->GetPerson() == this)
		{
			// some actions to himseft
			if (free)	// is free soldier
			{
				const ActionMap *map = Type()->GetActionMap(_primaryMove.id);
				if (QIsManual())
				{
					// check if there is some primary or secondary weapon
					int index = IsHandGunSelected() ? FindWeaponType(MaskSlotHandGun) : FindWeaponType(MaskSlotPrimary);
					if (index < 0) index = FindWeaponType(MaskSlotSecondary);
					if (index>=0)
					{
						// check if weapon can be armed / given to resting position
						// check current position
						if (GetActUpDegree()<ManPosStand)
						{
							if (EnableWeaponManipulation())
							{
								actions.Add(ATWeaponOnBack, this, 0.3f, index);
							}
						}
						else if (GetActUpDegree()==ManPosStand)
						{
							actions.Add(ATWeaponInHand, this, 2.0f, index);
						}
					}
					/*						
					if (EnableWeaponManipulation())
					{
						if (map && map->GetAction(ManActStrokeGun)!=MoveIdNone)
						{
							if (CheckEnemySoldier(unit,Position(),Direction()))
							{
								actions.Add(ATStrokeGun,this,0.1f,0);
							}
						}

						// check if there are stroke actions present
						if (map && map->GetAction(ManActStrokeFist)!=MoveIdNone)
						{
							// strike only when there is some enemy in stroke range
							if (CheckEnemySoldier(unit,Position(),Direction()))
							{
								actions.Add(ATStrokeFist,this,0.1f,0);
							}
						}
					}
					*/
				
					if (IsHandGunSelected())
					{
						int index = FindWeaponType(MaskSlotPrimary);
						if (index >= 0) actions.Add(ATHandGunOff, this, 0.4f, index);
					}
					else
					{
						int index = FindWeaponType(MaskSlotHandGun);
						if (index >= 0) actions.Add(ATHandGunOn, this, 0.4f, index);
					}
				} // QIsManual

				// add sit down only when there is such action available
				if (map && map->GetAction(ManActSitDown)!=MoveIdNone)
				{
					actions.Add(ATSitDown,this,0.1f,0);
				}
				if (map && map->GetAction(ManActSalute)!=MoveIdNone)
				{
					actions.Add(ATSalute, this, 0.11f, 0);
				}
				// FIX: only manual now
				/*
				if (QIsManual())
				{
					if (IsNVEnabled()) actions.Add(ATNVGoggles, this, 0.511f, 0);
				}
				*/
			} // soldier

			// pipe bombs
			bool isNear = false;
			bool isVeryNear = false;
			for (int i=0; i<_pipeBombs.Size();)
				if (_pipeBombs[i])
				{
					float dist2 = _pipeBombs[i]->Position().Distance2(Position());
					if (!isNear)
						isNear = dist2 <= Square(300);
					if (!isVeryNear)
						isVeryNear = dist2 < Square(3);
					i++;
				}
				else _pipeBombs.Delete(i);

			if (isNear) actions.Add(ATTouchOff, this, 0.515f, 0);
			if (free && isVeryNear)
			{
				actions.Add(ATSetTimer, this, 0.516f, 0, true, false);
				actions.Add(ATDeactivate, this, 0.514f, 0, true, false);
			}

			// mines
			if (Type()->_canDeactivateMines)
			{
				int xMin, xMax, zMin, zMax;
				ObjRadiusRectangle(xMin, xMax, zMin, zMax, Position(), Position(), 3);
				for (int x=xMin; x<=xMax; x++) for (int z=zMin; z<=zMax; z++)
				{
					const ObjectList &list = GLandscape->GetObjects(z, x);
					int n = list.Size();
					for (int i=0; i<n; i++)
					{
						Object *obj = list[i];
						if (!obj) continue;

						float dist2 = obj->Position().Distance2(Position());
						if (dist2 > Square(3)) continue;

						Mine *mine = dyn_cast<Mine>(obj);
						if (!mine) continue;
						if (mine->IsActive())
							actions.Add(ATDeactivateMine, this, 0.513f, 0, true);

						const AmmoType *ammo = mine->Type();
						if (!ammo) continue;						
						RString name = ammo->_defaultMagazine;
						if (name.GetLength() == 0) continue;

						Ref<MagazineType> magazineType = MagazineTypes.New(name);
						Ref<Magazine> magazine = new Magazine(magazineType);
						magazine->_ammo = magazineType->_maxAmmo;
				
						AUTO_STATIC_ARRAY(Ref<const Magazine>, conflict, 16);
						if (CheckMagazine(magazine, conflict) && conflict.Size() == 0)
							actions.Add(ATTakeMine, this, 0.512f, 0, true);
					}
				}
			}

		} // if (himself)
		
	} // if (dead) else
}

/*!
\patch 1.01 Date 6/26/2001 by Ondra.
- Fixed: display NV from external camera for dead soldier.
*/

bool Man::IsNVEnabled() const
{
	if (QIsManual())
	{
		if (IsDammageDestroyed()) return false;
		if (GWorld->GetCameraType() == CamGroup) return false;
		if (GWorld->HasMap()) return false;
	}
	return _hasNVG;
}

bool Man::IsNVWanted() const
{
	return _nvg;
}

void Man::SetNVWanted(bool set)
{
	_nvg = set;
}

void CutScene(const char *name);

struct MagazineInWeapon
{
	Ref<MuzzleType> muzzle;
	Ref<Magazine> magazine;
};
TypeIsMovableZeroed(MagazineInWeapon)


/*!
\patch 1.34 Date 12/03/2001 by Ondra
- Fixed: AI was not able to reload after taking magazine from corpse
when it was completely out of ammo before.
\patch 1.92 Date 8/1/2003 by Jirka
- Fixed: Game sometimes freeze when AI rearms 
*/

bool Man::Supply(EntityAI *vehicle, UIActionType action, int param, int param2, RString param3)
{
	if (action == ATTakeWeapon)
	{
		Man *man = dyn_cast<Man>(vehicle);
		if (!man) return false;
		// find weapon		
		Ref<WeaponType> weapon = WeaponTypes.New(param3);
		AUTO_STATIC_ARRAY(Ref<const WeaponType>, conflict, 16);
		if (FindWeapon(weapon) && man->CheckWeapon(weapon, conflict))
		{
			// 1. find magazines in weapons
			AUTO_STATIC_ARRAY(MagazineInWeapon, miwConflict, 16);
			for (int i=0; i<conflict.Size(); i++)
			{
				const WeaponType *weapon = conflict[i];
				for (int j=0; j<weapon->_muzzles.Size(); j++)
				{
					MuzzleType *muzzle = weapon->_muzzles[j];
					for (int k=0; k<man->NMagazineSlots(); k++)
					{
						const MagazineSlot &slot = man->GetMagazineSlot(k);
						if (slot._muzzle == muzzle)
						{
							if (slot._magazine && slot._magazine->_ammo > 0)
							{
								int index = miwConflict.Add();
								miwConflict[index].muzzle = slot._muzzle;
								miwConflict[index].magazine = slot._magazine;
							}
							break;
						}
					}
				}
			}
			AUTO_STATIC_ARRAY(MagazineInWeapon, miwWeapon, 16);
			for (int j=0; j<weapon->_muzzles.Size(); j++)
			{
				MuzzleType *muzzle = weapon->_muzzles[j];
				for (int k=0; k<NMagazineSlots(); k++)
				{
					const MagazineSlot &slot = GetMagazineSlot(k);
					if (slot._muzzle == muzzle)
					{
						if (slot._magazine && slot._magazine->_ammo > 0)
						{
							int index = miwWeapon.Add();
							miwWeapon[index].muzzle = slot._muzzle;
							miwWeapon[index].magazine = slot._magazine;
						}
						break;
					}
				}
			}
			// 2. remove weapons
			for (int i=0; i<conflict.Size(); i++)
			{
				const WeaponType *wt = conflict[i];
				man->RemoveWeapon(wt, false);
				AddWeapon(const_cast<WeaponType *>(wt), true, false, false);
			}
			// 3. add weapon
			RemoveWeapon(weapon, false);
			Verify(man->AddWeapon(weapon, false, false, false) >= 0);
			if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == man)
				GWorld->UI()->ResetVehicle(man);
			// 4. remove magazines in weapons
			for (int i=0; i<miwConflict.Size(); i++)
			{
				Ref<Magazine> magazine = miwConflict[i].magazine;
				if (!magazine) continue;
				man->RemoveMagazine(magazine);
				AddMagazine(magazine, true);
				AttachMagazine(miwConflict[i].muzzle, magazine);
			}
			// 5. remove unusable magazines
			for (int i=0; i<man->NMagazines();)
			{
				Ref<Magazine> magazine = man->GetMagazine(i);
				if (!magazine || man->IsMagazineUsable(magazine->_type))
				{
					i++;
					continue;
				}
				man->RemoveMagazine(magazine);
				if (magazine->_ammo >= 0)
					AddMagazine(magazine, true);
			}
			// 6. add magazines in weapons
			for (int i=0; i<miwWeapon.Size(); i++)
			{
				Ref<Magazine> magazine = miwWeapon[i].magazine;
				if (!magazine) continue;
				AUTO_STATIC_ARRAY(Ref<const Magazine>, conflict, 16);
				if (man->CheckMagazine(magazine, conflict))
				{
					for (int j=0; j<conflict.Size(); j++)
					{
						const Magazine *m = conflict[j];
						man->RemoveMagazine(m);
						if (m->_ammo >= 0)
							AddMagazine(const_cast<Magazine *>(m), true);
					}
					RemoveMagazine(magazine);
					if (magazine->_ammo >= 0)
					{
						Verify(man->AddMagazine(magazine) >= 0);
						man->AttachMagazine(miwWeapon[i].muzzle, magazine);
					}
				}
			}
			// 7. add usable magazines
			for (int i=0; i<weapon->_muzzles.Size(); i++)
			{
				MuzzleType *muzzle = weapon->_muzzles[i];
				for (int j=0; j<muzzle->_magazines.Size(); j++)
				{
					MagazineType *type = muzzle->_magazines[j];
					for (int k=0; k<NMagazines();)
					{
						Ref<Magazine> magazine = GetMagazine(k);
						if (!magazine || magazine->_type != type)
						{
							k++;
							continue;
						}
						if (IsMagazineUsed(magazine))
						{
							k++;
							continue;
						}
						// add magazine
						AUTO_STATIC_ARRAY(Ref<const Magazine>, conflict, 16);
						if (!man->CheckMagazine(magazine, conflict) || conflict.Size() > 0)
							goto EnoughMagazines;
						RemoveMagazine(magazine);
						Verify(man->AddMagazine(magazine) >= 0);
					}
				}
			}
EnoughMagazines:
			;
		}
		
		man->OnWeaponChanged();
		OnWeaponChanged();
		if (man == GWorld->CameraOn()) CutScene("TakeWeapon");
		if (!IsLocal()) GetNetworkManager().UpdateWeapons(this);

		void UpdateWeaponsInBriefing();
		UpdateWeaponsInBriefing();

		return true;
	}
	else if (action == ATTakeMagazine)
	{
		Man *man = dyn_cast<Man>(vehicle);
		if (!man) return false;
		// find magazine		
		Ref<const Magazine> magazine = FindMagazine(param3);
		if (!magazine) return false;
		AUTO_STATIC_ARRAY(Ref<const Magazine>, conflict, 16);
		if (man->CheckMagazine(magazine, conflict))
		{
			for (int i=0; i<conflict.Size(); i++)
			{
				const Magazine *m = conflict[i];
				man->RemoveMagazine(m);
				if (m->_ammo >= 0)
					AddMagazine(const_cast<Magazine *>(m), true);
			}
			RemoveMagazine(magazine);
			Verify(man->AddMagazine(const_cast<Magazine *>(magazine.GetRef()),false,true) >= 0);
		}
		if (man == GWorld->CameraOn()) CutScene("TakeMagazine");
		if (!IsLocal()) GetNetworkManager().UpdateWeapons(this);

		void UpdateWeaponsInBriefing();
		UpdateWeaponsInBriefing();

		return true;
	}
	else if (action == ATRearm)
	{
		Man *man = dyn_cast<Man>(vehicle);
		if (!man) return false;
		AIUnit *unit = man->Brain();
		if (!unit) return false;
		// which magazines we needed
		const MuzzleType *muzzle1 = NULL;
		const MuzzleType *muzzle2 = NULL;
		int slots1 = 0, slots2 = 0, slots3 = 0;
		unit->CheckAmmo(muzzle1, muzzle2, slots1, slots2, slots3);
		for (int i=0; i<NMagazines();)
		{
			Ref<const Magazine> magazine = GetMagazine(i);
			if (!magazine || magazine->_ammo == 0)
			{
				i++;
				continue;
			}
			if (IsMagazineUsed(magazine))
			{
				i++;
				continue;
			}
			// check if magazine can be used
			const MagazineType *type = magazine->_type;
			int slots = GetItemSlotsCount(type->_magazineType);

			bool add = false;
			if (muzzle1 && muzzle1->CanUse(type))
			{
				if (slots <= slots1)
				{
					slots1 -= slots;
					add = true;
				}
			}
			else if (muzzle2 && muzzle2->CanUse(type))
			{
				if (slots <= slots2)
				{
					slots2 -= slots;
					add = true;
				}
			}
			else if (man->IsMagazineUsable(type))
			{
				if (slots <= slots3)
				{
					slots3 -= slots;
					add = true;
				}
			}
			if (add)
			{
				// add magazine
				AUTO_STATIC_ARRAY(Ref<const Magazine>, conflict, 16);
				if (man->CheckMagazine(magazine, conflict))
				{
					for (int j=0; j<conflict.Size(); j++)
					{
						const Magazine *m = conflict[j];
						man->RemoveMagazine(m);
						AddMagazine(const_cast<Magazine *>(m), true);
					}
					RemoveMagazine(magazine);
					Verify(	man->AddMagazine(const_cast<Magazine *>(magazine.GetRef()),false,true) >= 0);
				}
        else i++;
			}
			else i++;
		}
		if (!IsLocal()) GetNetworkManager().UpdateWeapons(this);

		void UpdateWeaponsInBriefing();
		UpdateWeaponsInBriefing();

		return false;
	}
	return base::Supply(vehicle, action, param, param2, param3);
}

float Man::Rigid() const
{
 // how much energy is transfered in collision
 	return 0.6f;
}

bool Man::HasGeometry() const
{
	return !_isDead;
}

void LogFVector( Vector3Val val )
{
	LogF("  %10.5f,%10.5f,%10.5f",val.X(),val.Y(),val.Z());
}

void LogFMatrix( Matrix4Val val )
{
	LogFVector(val.DirectionAside());
	LogFVector(val.DirectionUp());
	LogFVector(val.Direction());
	LogFVector(val.Position());
}

static Matrix3 CreateZRotation(float sinr, float cosr)
{
	Matrix3 ret = M3Identity;

	ret(0,0) = +cosr,ret(0,1) = -sinr;
	ret(1,0) = +sinr,ret(1,1) = +cosr;
	return ret;
}

//#include "statistics.hpp"

void Man::RecalcGunTransform()
{
	Vector3 gunAxis=GetWeaponCenter(0);
	if (QIsManual())
	{
		Matrix4 correctBank;
		correctBank.SetOrientation(CreateZRotation(_correctBankSin,_correctBankCos));
		correctBank.SetPosition(VZero);
		_gunTrans=
		(
			Matrix4(MTranslation,gunAxis)*
			Matrix4(MRotationY,_gunYRot)*
			Matrix4(MRotationX,-_gunXRot)*
			correctBank*
			Matrix4(MTranslation,-gunAxis)
		);
		_gunTransIdent = false;
	}
	else
	{
		if (fabs(_gunXRot)+fabs(_gunYRot)>1e-6)
		{
			_gunTrans=
			(
				Matrix4(MTranslation,gunAxis)*
				Matrix4(MRotationY,_gunYRot)*
				Matrix4(MRotationX,-_gunXRot)*
				Matrix4(MTranslation,-gunAxis)
			);
			_gunTransIdent = false;
		}
		else
		{
			_gunTrans = MIdentity; // no aim - avoid matrix multiply
			_gunTransIdent = true;
		}
	}

	// note: rotation must be around head up direction axis
	Matrix4 headOrient(MIdentity);
	Vector3 headAxis=GetHeadCenter();

	headOrient.SetPosition(headAxis);

	Matrix4 headOrientInv(MInverseScaled,headOrient);

	if (fabs(_headYRot)+fabs(_headXRot)>1e-6)
	{
		_headTrans=
		(
			headOrient*
			Matrix4(MRotationY,_headYRot)*
			Matrix4(MRotationX,-_headXRot)*
			headOrientInv
		);
		_headTransIdent = false;
	}
	else
	{
		_headTrans = MIdentity;
		_headTransIdent = true;
	}

	// include recoil in gun transform
	if (_recoil)
	{
		_recoil->ApplyRecoil(_recoilTime,_gunTrans,_recoilFactor);
		_gunTransIdent = false;
	}

	/*
	static StatEventRatio eventGun("Gun  Ident");
	eventGun.Count(_gunTransIdent);
	static StatEventRatio eventHead("Head Ident");
	eventHead.Count(_headTransIdent);
	*/
}

bool Man::VerifyStructure() const
{
	if (DirectionAside().SquareSize()<0.01f) {Fail("DirectionAside zero");return false;}
	if (DirectionUp().SquareSize()<0.01f) {Fail("DirectionUp zero");return false;}
	if (Direction().SquareSize()<0.01f) {Fail("Direction zero");return false;}

	// check for indefinite numbers
	if (!DirectionAside().IsFinite()) {Fail("DirectionAside inf");return false;}
	if (!DirectionUp().IsFinite()) {Fail("DirectionUp inf");return false;}
	if (!Direction().IsFinite()) {Fail("Direction inf");return false;}
	if (!Position().IsFinite()) {Fail("Position inf");return false;}

	Matrix4Val it = GetInvTransform();

	if (!it.DirectionAside().IsFinite()) {Fail("inv DirectionAside inf");return false;}
	if (!it.DirectionUp().IsFinite()) {Fail("inv DirectionUp inf");return false;}
	if (!it.Direction().IsFinite()) {Fail("inv Direction inf");return false;}
	if (!it.Position().IsFinite()) {Fail("inv Position inf");return false;}

	return true;
}

/*!
\patch 1.52 Date 4/20/2002 by Ondra
- Fixed: Soldiers sometimes turned their head around completely.
*/

bool Man::MoveHead(float deltaT)
{
  bool forceRecalcMatrix = false;
	float headXRotWanted=_headXRotWanted;
	float headYRotWanted=_headYRotWanted;

	if (_isDead) headXRotWanted = headYRotWanted = 0;

	float ability = floatMax(GetAbility(),0.5f);
	//float maxV = 10*GetAbility();
	//saturate(maxV,4,8);
	float maxV = 8*ability;

	// avoid turning head too fast when in safe mode
	AIUnit *unit = Brain();
	if (unit && !QIsManual())
	{
		if (unit->GetCombatMode()<=CMSafe) saturateMin(maxV,2);
	}

	float headXRotChange = headXRotWanted-_headXRot;
	float headYRotChange = headYRotWanted-_headYRot;
	Limit(headXRotChange,-maxV*deltaT,+maxV*deltaT);
	Limit(headYRotChange,-maxV*deltaT,+maxV*deltaT);


	if (fabs(headXRotChange)>1e-6) forceRecalcMatrix = true;
	else headXRotChange = 0;
	if (fabs(headYRotChange)>1e-6) forceRecalcMatrix = true;
	else headYRotChange = 0;

	_headXRot += headXRotChange;
	_headYRot += headYRotChange;
	/*
	LogF
	(
		"%s: head %g,%g->%g,%g, max %g,%g",
		(const char *)GetDebugName(),
		_headXRot,_headYRot,
		headXRotWanted,headYRotWanted,
		maxV*deltaT,maxV*deltaT
	);
	*/
  return forceRecalcMatrix;
}

void Man::MoveWeapons( float deltaT, bool forceRecalcMatrix )
{
	//static StatEventRatio eventMove("MovedMan");
	//eventMove.Count(forceRecalcMatrix);

	float f = GetLimitGunMovement();
	float ability = floatMax(GetAbility(),0.5f);
	if (f<0.01f)
	{
		// no gun movement - reset all gun properties
		_gunXSpeed = 0;
		_gunYSpeed = 0;

		if (fabs(_gunXRot)>1e-5) forceRecalcMatrix = true;
		if (fabs(_gunYRot)>1e-5) forceRecalcMatrix = true;

		_gunXRot = 0;
		_gunYRot = 0;

	}
	else
	{


		float gunXRotWanted=_gunXRotWanted;
		float gunYRotWanted=_gunYRotWanted;

		Limit(gunXRotWanted,Type()->_minGunElev*f,Type()->_maxGunElev*f);
		Limit(gunYRotWanted,Type()->_minGunTurnAI*f,Type()->_maxGunTurnAI*f);

		float weaponDexterity = 1;

		int weapon = SelectedWeapon();
		if (weapon>=0 && weapon<NMagazineSlots())
		{
			const WeaponType *info = GetMagazineSlot(weapon)._weapon;
			if (info) weaponDexterity = info->_dexterity;

		}

		// limited acceleration model
		float delta;
		float speed;
		float deltaPos;

		const float maxV = 5*ability*weaponDexterity;
		const float maxA = 20*weaponDexterity;

		deltaPos = gunXRotWanted-_gunXRot;
		speed=deltaPos*8*weaponDexterity;
		if (fabs(speed)*deltaT>fabs(deltaPos))
		{
			speed = deltaPos/deltaT;
		}
		delta=speed-_gunXSpeed;
		Limit(delta,-maxA*deltaT,+maxA*deltaT);
		_gunXSpeed+=delta;

		deltaPos = gunYRotWanted-_gunYRot;
		speed=deltaPos*6*weaponDexterity;
		if (fabs(speed)*deltaT>fabs(deltaPos))
		{
			speed = deltaPos/deltaT;
		}
		delta=speed-_gunYSpeed;
		Limit(delta,-maxA*deltaT,+maxA*deltaT);
		_gunYSpeed+=delta;

		Limit(_gunXSpeed,-maxV,+maxV);
		Limit(_gunYSpeed,-maxV,+maxV);

		float gunXRotChange = _gunXSpeed*deltaT;
		float gunYRotChange = _gunYSpeed*deltaT;

		if (fabs(gunXRotChange)>=1e-6) forceRecalcMatrix = true;
		else gunXRotChange = 0;
		if (fabs(gunYRotChange)>=1e-6) forceRecalcMatrix = true;
		else gunYRotChange = 0;

		_gunXRot += gunXRotChange;
		_gunYRot += gunYRotChange;

		Limit(_gunXRot,Type()->_minGunElev*f,Type()->_maxGunElev*f);
		Limit(_gunYRot,Type()->_minGunTurnAI*f,Type()->_maxGunTurnAI*f);
	}

  bool headMoved = MoveHead(deltaT);
  if (headMoved) forceRecalcMatrix = true;

	//static StatEventRatio eventGun("RecalcGun");
	//eventGun.Count(forceRecalcMatrix);
	if (forceRecalcMatrix)
	{
		RecalcGunTransform();
	}
}

inline bool IsDown(int pos)
{
	return pos==ManPosLying || pos==ManPosBinocLying ||
		pos==ManPosLyingNoWeapon || pos==ManPosHandGunLying;
}
inline bool IsLaunchDown(int pos)
{
	return pos==ManPosWeapon;
}

inline bool Man::IsDown() const
{
	return ::IsDown(GetActUpDegree());
}


inline bool Man::IsLaunchDown() const
{
	return ::IsLaunchDown(GetActUpDegree());
}


bool Man::EnableTest(TestEnable func) const
{
	const MoveInfo *info=Type()->GetMoveInfo(_primaryMove.id);
	if (info && !(info->*func)()) return false;
	if (_primaryFactor<0.99f)
	{
		const MoveInfo *sec=Type()->GetMoveInfo(_secondaryMove.id);
		if (sec && !(sec->*func)()) return false;
	}
	return true;
}

float Man::ValueTest(TestValue func, float defValue) const
{
	const MoveInfo *info=Type()->GetMoveInfo(_primaryMove.id);
	float primValue = info ? (info->*func)() : 0;
	if (_primaryFactor<0.99f)
	{
		const MoveInfo *sec=Type()->GetMoveInfo(_secondaryMove.id);
		float secValue = sec ? (sec->*func)() : defValue;
		primValue = _primaryFactor*primValue + (1-_primaryFactor)*secValue;
	}
	return primValue;
}

float Man::ValueTest(TestValueTimed func, float defValue) const
{
	const MoveInfo *info=Type()->GetMoveInfo(_primaryMove.id);
	float primValue = info ? (info->*func)(_primaryTime) : 0;
	if (_primaryFactor<0.99f)
	{
		const MoveInfo *sec=Type()->GetMoveInfo(_secondaryMove.id);
		float secValue = sec ? (sec->*func)(_secondaryTime) : defValue;
		primValue = _primaryFactor*primValue + (1-_primaryFactor)*secValue;
	}
	return primValue;
}


bool Man::DisableWeapons() const
{
	return WeaponsDisabled();
}

bool Man::EnableMissile() const
{
	const MoveInfo *info=Type()->GetMoveInfo(_primaryMove.id);
	if (info && !info->MissileEnabled()) return false;
	if (_primaryFactor<0.99f)
	{
		const MoveInfo *sec=Type()->GetMoveInfo(_secondaryMove.id);
		if (sec && !sec->MissileEnabled()) return false;
	}
	return true;
}

const BlendAnimSelections &Man::GetBlendAnim
(
	BlendAnimSelections &tgt, BlendAnimFunc func
) const
{
	const ManType *type = Type();
	// blend aiming to tgt buffer
	// check primary and secondary factors
	if (_primaryFactor>0.99f)
	{
		const MoveInfo *move = type->GetMoveInfo(_primaryMove.id);
		if (move) return (move->*func)();
		return tgt;
	}

	const BlendAnimSelections *anim1 = NULL;
	const BlendAnimSelections *anim2 = NULL;

	const MoveInfo *move1 = type->GetMoveInfo(_primaryMove.id);
	//if (move) tgt.AddOther((move->*func)(),_primaryFactor);
	const MoveInfo *move2 = type->GetMoveInfo(_secondaryMove.id);
	//if (move) tgt.AddOther((move->*func)(),1-_primaryFactor);
	if (move1) anim1 = &(move1->*func)();
	if (move2) anim2 = &(move2->*func)();
	if (anim1==anim2)
	{
		// if both are the same, blending is trivial
		return *anim1;
	}
	tgt.AddOther(*anim1,_primaryFactor);
	tgt.AddOther(*anim1,1-_primaryFactor);
	return tgt;
}

const BlendAnimSelections &Man::GetAiming( BlendAnimSelections &tgt ) const
{
	return GetBlendAnim(tgt,&MoveInfo::GetAiming);
}
const BlendAnimSelections &Man::GetLegs( BlendAnimSelections &tgt ) const
{
	return GetBlendAnim(tgt,&MoveInfo::GetLegs);
}
const BlendAnimSelections &Man::GetHead( BlendAnimSelections &tgt ) const
{
	return GetBlendAnim(tgt,&MoveInfo::GetHead);
}

float Man::VisibleMovement() const
{
	if( _shootVisible>1 ) return _shootVisible;
	float vis=base::VisibleMovement();

	const MoveInfo *pri = Type()->GetMoveInfo(_primaryMove.id);
	if (pri)
	{
		vis *= pri->GetVisibleSize();
	}
	if (_hideBody>0)
	{
		float bodyNotHidden = floatMax(1-_hideBody,0);
		vis *= bodyNotHidden;
	}

	return vis;
}

/*!
	\patch 1.01 Date 06/11/2001 by Ondra
	- Fixed: too low audibility of near soldiers
	- AI would not hear soldier even when moving very near.
*/

float Man::Audible() const
{
	// modified EntityAI::Audible
	float vis=0.2f;
	// screaming target is better audible
	// currently it is 30 sec after scream
	if (_whenScreamed<Glob.time && _whenScreamed>Glob.time-30)
	{
		saturateMax(vis,1.2f);
	}

	// FIX
	float rSpeed=Speed().SizeXZ()*0.5f;
	// moving target is easier to hear
	saturateMin(rSpeed,4);
	saturateMax(vis,rSpeed);
	// TODO: if light is on at night, vehicle is much more visible
	//if( IsDown() ) vis*=0.25f;
	vis *= GetType()->GetAudible();
	saturateMax(vis,_shootAudible);
	return vis;
}

float Man::GetHidden() const
{
	// track hiding in trees/bushes etc.
	return 1-_surround.Track(this,Position(),50,0.5f);
	//return 1;
}

float Man::VisibleSize() const
{
	float bodyNotHidden = floatMax(1-_hideBody,0);
	float vis=GetShape()->GeometrySphere()*bodyNotHidden;
	//if( IsDown() ) return size*0.3f;

	const MoveInfo *pri = Type()->GetMoveInfo(_primaryMove.id);
	if (pri)
	{
		vis *= pri->GetVisibleSize();
	}

	return vis;
}

float Man::CollisionSize() const
{
	if( IsDown() ) return GetShape()->GeometrySphere();
	return base::CollisionSize();
}

Vector3 Man::VisiblePosition() const
{
	return AimingPosition();
}

Vector3 Man::CalculateAimingPosition(Matrix4Par pos) const
{
	if( Type()->_aimPoint>=0 )
	{
		int level=_shape->FindMemoryLevel();
		return pos.FastTransform(AnimatePoint(level,Type()->_aimPoint));
	}
	else
	{
		LogF("Obsolete aiming position");
		//  if laid down, position is different
		// TODO: correct calculation of position
		Vector3 basePos=base::AimingPosition();
		if( IsDown() ) return basePos-Vector3(0,1.25f,0);
		return basePos;
	}
}

Vector3 Man::CalculateCameraPosition(Matrix4Par pos) const
{
	// fallback - no explicit camera point
	int level=_shape->FindMemoryLevel();
	int selIndex=Type()->_pilotPoint;
	if( selIndex<0 ) return CalculateAimingPosition(pos);
	const Selection &sel=_shape->LevelOpaque(level)->NamedSel(selIndex);
	return pos.FastTransform(AnimatePoint(level,sel[0]));
}

Vector3 Man::AimingPosition() const
{
	return _aimingPositionWorld;
}

Vector3 Man::CameraPosition() const
{
	return _cameraPositionWorld;
}


const float DownArmorCoef=2;

float Man::GetArmor() const
{
	float armor=base::GetArmor();
	if( IsDown() ) return armor*DownArmorCoef;
	return armor;
}
float Man::GetInvArmor() const
{
	float iArmor=base::GetInvArmor();
	if( IsDown() ) return iArmor*(1/DownArmorCoef);
	return iArmor;
}

float Man::FireAngleInRange( int weapon, Vector3Par rel ) const
{
	const ManType *type=Type();
	float dist2=rel.SquareSizeXZ();
	float y2=Square(rel.Y());
	// x>0: atan(x)<x
	// atan(y/sqrt(dist2)) < type->_maxGunElev
	// y/sqrt(dist2) < type->_maxGunElev
	// y < type->_maxGunElev * sqrt(dist2)
	// y2 < type->_maxGunElev^2 * dist2
	// we need to have correct signs
	Assert( type->_maxGunElev>=0 );
	Assert( type->_minGunElev<=0 );
	float ret=0;
	if( rel.Y()>=0 )
	{
		// fire up
		if( y2>dist2*Square(type->_maxGunElev) ) return 0;
		ret=rel.Y()*InvSqrt(dist2)*(1/type->_maxGunElev);
	}
	else
	{
		// fire down
		if( y2>dist2*Square(type->_minGunElev) ) return 0;
		ret=rel.Y()*InvSqrt(dist2)*(1/type->_minGunElev);
	}
	return floatMin(1,2-ret*2);
}

/*!
\patch 1.05 Date 7/18/2001 by Ondra.
- New: even small hand injury causes some weapon trembling.
- Improved: Increased weapon trembling caused by heavy breath.
*/

float Man::RifleInaccuracy() const
{
	// weapon cursor movement when moving
	// riffle more accurate when prone
	// if man is wounded, riffle is much less accurate
	float woundFactor = GetHitCont(Type()->_handsHit)*10 + 1;
	woundFactor += floatMin(_tired*2,1)*6;
	float speedXZ = _speed.SizeXZ();
	float ret = floatMin(speedXZ*0.5f,1)*0.05f+0.005f;
	// get current move
	const MoveInfo *prim = Type()->GetMoveInfo(_primaryMove.id);
	if (prim)
	{
		ret *= prim->GetAimPrecision();
	}
	// some weapons (weapon modes) are more accurate
	const WeaponModeType *mode = GetCurrentWeaponMode();
	// TODO: make coeficient part of WeaponModeType
	if (mode && mode->_ammo && mode->_ammo->_simulation==AmmoShotLaser)
	{
		ret *= 0.2f;
	}
	/*
	if (this==(const Object *)GWorld->CameraOn())
	{
		GlobalShowMessage(100,"Inacc %.3f, %.3f",ret,woundFactor);
	}
	*/
	return ret*woundFactor;
}

static void LimitLead( Vector3 &spd, float maxLead )
{
	float size2=spd.SquareSize();
	if( size2>Square(maxLead) )
	{
		spd.Normalize();
		spd*=maxLead;
	}
}

void Man::AimHeadForward()
{
	// head should follow gun
	_headYRotWanted = 0;
	_headXRotWanted = 0;
}

bool Man::AimObserver(Vector3Par direction)
{
  AimHead(direction);
  return false;
}

void Man::AimHead(Vector3Par direction)
{
	// move head accordingly to direction
	Vector3 relDir(VMultiply,DirWorldToModel(),direction);
	// calculate current gun direction
	// compensate for aiming (is applied before head)
	relDir = GunTransform().InverseRotation().Rotate(relDir);
	//

	float yRotWanted=-atan2(relDir.X(),relDir.Z());
	float sizeXZ=sqrt(Square(relDir.X())+Square(relDir.Z()));
	float xRotWanted=atan2(relDir.Y(),sizeXZ);

	// limit with natural head limits
	// the more y rot, the less x rot enabled
	const float maxYRot = H_PI/2;
	if (!QIsManual())
	{
		Limit(yRotWanted,Type()->_minHeadTurnAI,Type()->_maxHeadTurnAI);
	}
	Limit(yRotWanted,-maxYRot,+maxYRot);
	float yRotF = fabs(yRotWanted)*(1.0f/maxYRot);
	float maxXRot = (1-yRotF)*H_PI/4;
	Limit(xRotWanted,-maxXRot,+maxXRot);


	_headYRotWanted = yRotWanted;
	_headXRotWanted = xRotWanted;
}

void Man::AimHeadAI(Vector3Par direction, float deltaT)
{
	//static StatEventRatio ratio("AimHeadF");
	if (_lookTargetTimeLeft>0)
	{
		AimHead(direction);

		//ratio.Count(false);
	}
	else
	{
		if (direction.Distance2(Direction())<Square(0.05f))
		{
			// when direction is almost forward, make forward looking phase much shorter
			_lookForwardTimeLeft -= 2*deltaT;
		}
		AimHeadForward();
		//ratio.Count(true);
	}
}

const static Vector3 GrenadeDir = Vector3(0,0.4f,1).Normalized();
const float GrenadeAtan2 = atan2(GrenadeDir.Y(),GrenadeDir.SizeXZ());

void Man::AimWeaponAI( int weapon, Vector3Par direction, float deltaT )
{
	// TODO: move deltaT funct. to other place
	// merge with AimWeapon

	AimHeadAI(direction,deltaT);

	SelectWeapon(weapon);
	// move turret/gun accordingly to direction
	Vector3 relDir(VMultiply,DirWorldToModel(),direction);

	const Magazine *magazine = GetMagazineSlot(weapon)._magazine;
	if (!magazine) return;

	// calculate current gun direction
	// compensate for neutral gun position

	float inacc=RifleInaccuracy()*GetInvAbility();

	// calculate actual neutral rotation
	// for this: animate gun points as necessary
	float yRotWanted=-atan2(relDir.X(),relDir.Z());
	float sizeXZ=sqrt(Square(relDir.X())+Square(relDir.Z()));
	float xRotWanted=atan2(relDir.Y(),sizeXZ);


	const WeaponModeType *mode = GetWeaponMode(weapon);
	const AmmoType *ammo = mode ? mode->_ammo : NULL;
	const MagazineType *mag = magazine->_type;

	if ( ammo->_simulation==AmmoShotShell && mag->_initSpeed<30)
	{
		xRotWanted -= GrenadeAtan2;
	}

	//float size2 = direction.SquareSize();

	// aim slowly - depending on ability
	//float maxSpeed=GetAbility()*0.2f*deltaT;
	// when target is near, ability is always bettter
	// on 100 m 
	float ability = GetAbility();
	//float minAbility 
	float weaponDexterity = 1;

	if (weapon>=0 && weapon<NMagazineSlots())
	{
		const WeaponType *info = GetMagazineSlot(weapon)._weapon;
		if (info) weaponDexterity = info->_dexterity;

	}

	float maxSpeed=ability*0.2f*deltaT*weaponDexterity;
	float delta;

	float coef = 0.5f;
	xRotWanted+=_aimInaccuracyX*coef;
	yRotWanted+=_aimInaccuracyY*coef;

	Limit(xRotWanted,Type()->_minGunElev*1.5f,Type()->_maxGunElev*1.5f);
	Limit(yRotWanted,Type()->_minGunTurnAI*1.5f,Type()->_maxGunTurnAI*1.5f);


	delta=yRotWanted-_gunYRotWanted;
	saturate(delta,-maxSpeed,+maxSpeed);
	_gunYRotWanted+=delta;


	delta=xRotWanted-_gunXRotWanted;
	saturate(delta,-maxSpeed,+maxSpeed);
	_gunXRotWanted+=delta;

	if (IsFirePrepare())
	{
		// when not aiming for fire, set maximaal error
		// this avoids recalculating _gunTrans, which is quite expensive
		// at the same time it prevents aiming accuretelly in prepare mode
		// which could make problems when switching from prepare to fire mode
		float maxAimInacc=floatMin(inacc,0.2f);
		float maxDistIA=(GetInvAbility()-1)*0.1f;

		_aimInaccuracyX = maxAimInacc;
		_aimInaccuracyY = maxAimInacc;
		_aimInaccuracyDist = maxDistIA;
	}
	else
	{
		// tremble hands
		float iTime=Glob.time-_lastInaccuracyTime;
		saturateMin(iTime,2);
		while( (iTime-=0.1f)>=0 )
		{
			float maxAimInacc=floatMin(inacc,0.2f);
			float randX=GRandGen.RandomValue()-0.5f;
			float randY=GRandGen.RandomValue()-0.5f;
			_aimInaccuracyX+=randX*inacc*2;
			_aimInaccuracyY+=randY*inacc*2;
			saturate(_aimInaccuracyX,-maxAimInacc,+maxAimInacc);
			saturate(_aimInaccuracyY,-maxAimInacc,+maxAimInacc);
			_lastInaccuracyTime=Glob.time;
		}

		// distance estimation error
		float dTime=Glob.time-_lastInaccuracyDistTime;
		while( (dTime-=1.0f)>=0 )
		{
			// distance error in percent of distance
			float maxDistIA=(GetInvAbility()-1)*0.1f;
			float randZ=GRandGen.RandomValue()-0.5f;
			_aimInaccuracyDist+=randZ*0.25f;
			saturate(_aimInaccuracyDist,-maxDistIA,+maxDistIA);
			_lastInaccuracyDistTime=Glob.time;
		}
	}
}

void Man::AimWeaponAI( int weapon, Target *target, float deltaT )
{
	if (weapon < 0)
	{
		if (NMagazineSlots() <= 0) return;
		weapon = 0;
	}

	Vector3 dir;
	if (!CalculateAimWeapon(weapon,dir,target)) return;
	_fire.SetTarget(CommanderUnit(),target);
	AimWeaponAI(weapon,dir,deltaT);
}


void Man::AimWeaponManDir( int weapon, Vector3Par direction )
{
	AimWeapon(weapon,direction);
}

void Man::SimulateHUD(CameraType camType, float deltaT)
{
}

void Man::AimWeaponManSpeed( int weapon, float moveX, float moveY )
{
}

bool Man::AimWeapon( int weapon, Vector3Par direction )
{
	AimHead(direction);

	if (QIsManual() && GInput.lookAroundEnabled)
	{
		// no weapon aiming when looking around
		return false;
	}
	SelectWeapon(weapon);
	// move turret/gun accordingly to direction
	Vector3 relDir(VMultiply,DirWorldToModel(),direction);
	// calculate current gun direction
	// compensate for neutral gun position
	// apply bank compensation
	//Matrix3 correctBank(MRotationZ,-_correctBank);

	//relDir = correctBank*relDir;


	const WeaponModeType *mode = weapon>=0 ? GetWeaponMode(weapon) : NULL;
	const AmmoType *aType = mode ? mode->_ammo : NULL;

	float inacc=RifleInaccuracy()*GetInvAbility();

	// calculate actual neutral rotation
	// for this: animate gun points as necessary
	float yRotWanted=-atan2(relDir.X(),relDir.Z());
	float sizeXZ=sqrt(Square(relDir.X())+Square(relDir.Z()));
	float xRotWanted=atan2(relDir.Y(),sizeXZ);

	// note: this function should be used only for manual units
	// but it may be used for AI when forceFire function is used

	const float coef = 0.25f;
	const float invCoef = 1/coef;

	float minInaccY = floatMin(0,(Type()->_minGunTurn-yRotWanted)*invCoef);
	float maxInaccY = floatMax(0,(Type()->_maxGunTurn-yRotWanted)*invCoef);

	saturate(_aimInaccuracyY,minInaccY,maxInaccY);

	float inaccX = _aimInaccuracyX*coef;
	float inaccY = _aimInaccuracyY*coef;

	_gunXRotWanted = xRotWanted + inaccX;
	_gunYRotWanted = yRotWanted + inaccY;

	// tremble hands
	float iTime=Glob.time-_lastInaccuracyTime;
	while( (iTime-=0.2f)>=0 )
	{
		float randX=GRandGen.RandomValue()-0.5f;
		float randY=GRandGen.RandomValue()-0.5f;
		_aimInaccuracyX+=randX*inacc;
		_aimInaccuracyY+=randY*inacc;
		saturate(_aimInaccuracyX,-inacc,+inacc);
		saturate(_aimInaccuracyY,-inacc,+inacc);
		_lastInaccuracyTime=Glob.time;
	}
	_aimInaccuracyDist=0;
	/*
	GlobalShowMessage
	(
		100,"Aim XY %+.4f,%+.4f, max %+.4f",
		_aimInaccuracyX,_aimInaccuracyY,inacc
	);
	*/
	if (_forceFireWeapon>=0)
	{
		// note: this check is only required when performing _forceFire
		// TODO: create different function for this situation

		if (aType)
		{
			switch (aType->_simulation)
			{
				case AmmoShotPipeBomb:
				case AmmoShotTimeBomb:
				case AmmoShotMine:
				case AmmoShotLaser:
					return true;
			}
		}
		// check if we have already aimed
		float f = GetLimitGunMovement();
		Limit(_gunXRotWanted,Type()->_minGunElev*f,Type()->_maxGunElev*f);
		Limit(_gunYRotWanted,Type()->_minGunTurnAI*f,Type()->_maxGunTurnAI*f);
		return
		(
			fabs(_gunXRotWanted-_gunXRot)<0.01f &&
			fabs(_gunYRotWanted-_gunYRot)<0.01f
		);
	}
	return true;
}

bool Man::AimWeaponForceFire(int weapon)
{
	Vector3 dir = Direction();
	dir[1] = 10;
	// different direction depending on weapon type
	const WeaponModeType *mode = weapon>=0 ? GetWeaponMode(weapon) : NULL;
	if (!mode) return false;
	const MagazineSlot &slot = GetMagazineSlot(weapon);
	const AmmoType *aType = mode->_ammo;
	const Magazine *magazine = slot._magazine;
	if (!magazine) return false;
	const MagazineType *aInfo = magazine->_type;
	if (aType)
	{
		switch (aType->_simulation)
		{
			case AmmoShotPipeBomb:
			case AmmoShotTimeBomb:
			case AmmoShotMine:
				return true;
			case AmmoShotLaser:
				return false;
			case AmmoShotShell:
				if (aInfo->_initSpeed<30) dir[1]=0.5f;
				else dir[1]=1;
				break;
		}

	}
	dir.Normalize();
	return AimWeapon(_currentWeapon, dir);
}

/*!
\patch 1.52 Date 4/21/2002 by Ondra
- Fixed: Units sometimes aimed into air when searching for unknown target.
*/

bool Man::CalculateAimWeapon( int weapon, Vector3 &dir, Target *target )
{

	if (weapon<0) return false;

	const MagazineSlot &slot = GetMagazineSlot(weapon);

	const Magazine *magazine = slot._magazine;
	if (!magazine) return false;
	const MagazineType *aInfo = magazine->_type;
	Assert(aInfo);
	const WeaponModeType *mode = GetWeaponMode(weapon);
	Assert(mode);
	//Vector3 weaponPos=GetWeaponCenter(weapon);
	Vector3 tgtPos=target->LandAimingPosition();
	float dist=tgtPos.Distance(Position());
	// distance estimation is quite bad for soldiers
	// introduce some distance estimation error

	// verbose only for sniper

	// scale factor according to distance
	float distFactor = dist<40 ? 0 : dist-40;

	dist+=_aimInaccuracyDist*distFactor;

	// never shoot grenades to very short range
	saturateMax(dist,10);
	float time=dist*aInfo->_invInitSpeed;
	float balFactor = 0.5f*G_CONST;
	float leadFactor = 1;
	//weaponPos=GunTransform()*weaponPos;
	if (mode && mode->_ammo) switch (mode->_ammo->_simulation)
	{
		case AmmoShotBullet:
		break;
		case AmmoShotShell:
			// grenade launcher balistics
			// adjusted to work at 100 m
			if (aInfo->_initSpeed<30) time*=1.3f;
			else time*=1.1f;
		break;
		case AmmoShotMissile:
			time=dist/mode->_ammo->maxSpeed+0.1f;
			balFactor *= 0.37f;
			leadFactor = 1.4f;
		break;
		default:
			time=0;
		break;
	}
	else time=0;
	saturateMin(time,4);
	Vector3 myPos=PositionModelToWorld(GetWeaponPoint(weapon));

	float fall = Square(time)*balFactor;
	// calculate balistics
	tgtPos[1]+=fall; // consider balistics

	const float predTime=0.2f;
	//tgtPos+=target->speed*predTime;
	myPos+=Speed()*predTime;

	// lead target
	Vector3 speedEst=target->speed;
	if (aInfo)
	{
		float maxLead = aInfo->_maxLeadSpeed*GetAbility();
		//LogF("Speed %.2f,max %.2f, time %.3f",speedEst.Size(),maxLead,time);
		LimitLead(speedEst,maxLead);
		tgtPos+=speedEst*time*leadFactor;
	}

	dir = tgtPos-myPos;
	return true;
}

bool Man::AimWeapon( int weapon, Target *target )
{
	Vector3 dir;
	if (!CalculateAimWeapon(weapon,dir,target)) return false;
	_fire.SetTarget(CommanderUnit(),target);
	return AimWeapon(weapon,dir);
}

void Man::AdjustWeapon
(
	int weapon, CameraType camType, float fov, Vector3 &camDir
)
{
	// adjust weapon for some distance

	// interpolate between adjustment distances depending on fov
	// get current muzzle
	if (weapon<0 || weapon>=NMagazineSlots()) return;

	const MagazineSlot &mag = GetMagazineSlot(weapon);
	const Magazine *magazine = mag._magazine;
	if (!magazine) return;
	const MagazineType *aInfo = magazine->_type;
	if (!aInfo) return;

	const MuzzleType *muzzle = mag._muzzle;

	// calculate adjustment distance
	float distance=Interpolativ
	(
		fov,
		muzzle->_opticsZoomMin,muzzle->_opticsZoomMax,
		muzzle->_distanceZoomMin,muzzle->_distanceZoomMax
	);
	// calculate adjustment angle from distance
	// imagine we will be shooting to given distance
	// it depends on ammo fired


	float time = distance*aInfo->_invInitSpeed;
	float fallPerM = time*aInfo->_invInitSpeed*0.5f*G_CONST;
	// this angle should be applied to weapon aiming
	Vector3 relDir = DirectionWorldToModel(camDir);
	// apply correction
	relDir[1]+=fallPerM;

	camDir = DirectionModelToWorld(relDir);
}

Vector3 Man::GetWeaponRelDirection( int weapon ) const
{
	if (weapon >= 0 && weapon < NMagazineSlots())
	{
		int weaponIndex = -1;
		const WeaponType *info = GetMagazineSlot(weapon)._weapon;
		if (info->_weaponType & MaskSlotPrimary) weaponIndex = Type()->_priWeaponIndex;
		else if (info->_weaponType & MaskSlotSecondary) weaponIndex = Type()->_secWeaponIndex;
		else if (info->_weaponType & MaskSlotHandGun) weaponIndex = Type()->_handGunIndex;
		if (weaponIndex >= 0)
		{
			Shape *proxyContainer = _shape->MemoryLevel();
			const ProxyObject &proxy = proxyContainer->Proxy(weaponIndex);

			const MuzzleType *muzzle = GetMagazineSlot(weapon)._muzzle;
			Vector3Val dir = muzzle->_muzzleDir;

			// apply proxy transformation
			Matrix4Val animTrans = AnimateProxyMatrix(_shape->FindMemoryLevel(),proxy);
			// TODO: normalize proxy transformations
			return animTrans.Rotate(dir);

		}
		// check for hand grenades
		const WeaponModeType *mode = GetWeaponMode(weapon);
		const AmmoType *ammo = mode ? mode->_ammo : NULL;
		if (ammo && ammo->_simulation==AmmoShotShell)
		{
			return GunTransform().Rotate(GrenadeDir);
		}
	}

	return GunTransform().Direction();
}

Matrix4 Man::GetWeaponRelTransform( int weapon ) const
{
	if (weapon >= 0 && weapon < NMagazineSlots())
	{
		int weaponIndex = -1;
		const WeaponType *info = GetMagazineSlot(weapon)._weapon;
		if (info->_weaponType & MaskSlotPrimary) weaponIndex = Type()->_priWeaponIndex;
		else if (info->_weaponType & MaskSlotSecondary) weaponIndex = Type()->_secWeaponIndex;
		else if (info->_weaponType & MaskSlotHandGun) weaponIndex = Type()->_handGunIndex;
		if (weaponIndex >= 0)
		{
			Shape *proxyContainer = _shape->MemoryLevel();
			const ProxyObject &proxy = proxyContainer->Proxy(weaponIndex);

			const MuzzleType *muzzle = GetMagazineSlot(weapon)._muzzle;
			Vector3Val dir = muzzle->_muzzleDir;

			ProxyWeapon *weaponObj = dyn_cast<ProxyWeapon,Object>(proxy.obj);
			if (weaponObj)
			{
				Vector3Val pos = weaponObj->Type()->GetCameraPos();

				// apply proxy transformation
				Matrix4Val animTrans = AnimateProxyMatrix(_shape->FindMemoryLevel(),proxy);
				Matrix4 trans;
				trans.SetDirectionAndUp(animTrans.Rotate(dir),animTrans.DirectionUp());
				trans.SetPosition(animTrans.FastTransform(pos));
				return trans;
			}
		}
	}

	// if we have nothing else, use eye position (binoc. etc)

	int level = _shape->FindMemoryLevel();
	Vector3 pos;
	int selIndex=Type()->_pilotPoint;
	if (selIndex<0)
	{
		pos = VZero;
	}
	else
	{
		const Selection &sel=_shape->LevelOpaque(level)->NamedSel(selIndex);
		pos = AnimatePoint(level,sel[0]);
	}

	Matrix4 trans;
	trans.SetPosition(pos);
	trans.SetOrientation(GunTransform().Orientation());
	return trans;
}

Vector3 Man::GetEyeDirection() const
{
	// get ideal weapon direction - do not consider animation
	Vector3 dir = _headTrans.Direction();
	dir = GunTransform().Rotate(dir);
	DirectionModelToWorld(dir,dir);
	return dir;
}

Matrix3 Man::GetHeadRelOrientation() const
{
	// get ideal weapon direction - do not consider animation
	return GunTransform().Orientation();
}

Vector3 Man::GetWeaponDirection( int weapon ) const
{
	Vector3 dir = GetWeaponRelDirection(weapon);
	DirectionModelToWorld(dir,dir);
	return dir;
}

Vector3 Man::GetHeadCenter() const
{
	int sel = Type()->_headAxisPoint;
	if (sel<0) return VZero;
	int level=_shape->FindMemoryLevel();
	if (level<0) return VZero;
	return AnimatePoint(level,sel);
}

Vector3 Man::GetWeaponCenter( int weapon ) const
{
	int sel = Type()->_aimingAxisPoint;
	if (sel<0) return VZero;
	int level=_shape->FindMemoryLevel();
	if (level<0) return VZero;
	return AnimatePoint(level,sel);
}

Vector3 Man::GetWeaponPoint( int weapon ) const
{
	if (weapon >= 0 && weapon < NMagazineSlots())
	{
		int weaponIndex = -1;
		const WeaponType *info = GetMagazineSlot(weapon)._weapon;
		if (info->_weaponType & MaskSlotPrimary) weaponIndex = Type()->_priWeaponIndex;
		else if (info->_weaponType & MaskSlotSecondary) weaponIndex = Type()->_secWeaponIndex;
		else if (info->_weaponType & MaskSlotHandGun) weaponIndex = Type()->_handGunIndex;
		if (weaponIndex >= 0)
		{
			Shape *proxyContainer = _shape->MemoryLevel();
			const ProxyObject &proxy = proxyContainer->Proxy(weaponIndex);
			Matrix4 animTrans = AnimateProxyMatrix(_shape->FindMemoryLevel(),proxy);

			const MuzzleType *muzzle = GetMagazineSlot(weapon)._muzzle;
			Vector3Val pos = muzzle->_muzzlePos;

			// apply proxy transformation
			// apply gun transformation
			return animTrans.FastTransform(pos);
		}
	}
	// missile is not fired from any known weapon
	// convert neutral gun position
	return PositionWorldToModel(AimingPosition());
}

bool Man::GetWeaponCartridgePos
(
	int weapon, Matrix4 &pos, Vector3 &vel
) const
{
	if (weapon >= 0 && weapon < NMagazineSlots())
	{
		int weaponIndex = -1;
		const WeaponType *info = GetMagazineSlot(weapon)._weapon;
		if (info->_weaponType & MaskSlotPrimary) weaponIndex = Type()->_priWeaponIndex;
		else if (info->_weaponType & MaskSlotSecondary) weaponIndex = Type()->_secWeaponIndex;
		else if (info->_weaponType & MaskSlotHandGun) weaponIndex = Type()->_handGunIndex;
		if (weaponIndex >= 0)
		{
			Shape *proxyContainer = _shape->MemoryLevel();
			const ProxyObject &proxy = proxyContainer->Proxy(weaponIndex);

			// apply proxy transformation
			Matrix4 animTrans = AnimateProxyMatrix(_shape->FindMemoryLevel(),proxy);

			const MuzzleType *muzzle = GetMagazineSlot(weapon)._muzzle;

			if
			(
				muzzle->_cartridgeOutPosIndex<0 ||
				muzzle->_cartridgeOutEndIndex<0
			)
			{
				return false;
			}

			Vector3Val cDir = muzzle->_muzzleDir;
			Vector3Val cPos = muzzle->_cartridgeOutPos;
			Vector3Val cVel = muzzle->_cartridgeOutVel;

			float randomSpd = GRandGen.RandomValue()*0.5f+0.75f;

			// create matrix for cartridge position
			Matrix4 trans;
			trans.SetDirectionAndUp(cDir,VUp);
			trans.SetPosition(cPos);
			// TODO: consider precalculation trans in MuzzleType

			pos = animTrans * trans;
			vel = animTrans.Rotate(cVel*randomSpd);

			return true;
		}
	}
	// missile is not fired from any known weapon
	// convert neutral gun position
	LogF("cartridge pos from no known weapon");
	return base::GetWeaponCartridgePos(weapon,pos,vel);
}

void Man::SelectPrimaryWeapon()
{
	int index = FindWeaponType(MaskSlotPrimary);
	if (index >= 0)
	{
		const WeaponType *weapon = GetWeaponSystem(index);
		for (int i=0; i<NMagazineSlots(); i++)
			if (GetMagazineSlot(i)._weapon == weapon)
			{
				SelectWeapon(i);
				break;
			}
	}
}

void Man::SelectHandGunWeapon()
{
	int index = FindWeaponType(MaskSlotHandGun);
	if (index >= 0)
	{
		const WeaponType *weapon = GetWeaponSystem(index);
		for (int i=0; i<NMagazineSlots(); i++)
			if (GetMagazineSlot(i)._weapon == weapon)
			{
				SelectWeapon(i);
				break;
			}
	}
}

void Man::FireAttemptWhenNotPossible()
{
	ActionMap *map = Type()->GetActionMap(_primaryMove.id);
	MoveId moveWanted = map->GetAction(ManActFireNotPossible);
	if (moveWanted != MoveIdNone)
	{
		if (IsHandGunSelected() && !IsHandGunInMove())
		{
			if (GetActUpDegree() == ManPosStand)
				moveWanted = map->GetAction(ManActHandGunOn);
			else
				SelectPrimaryWeapon();
		}
		_forceMove = MotionPathItem(moveWanted,NULL);
	}
}


/*!
\patch 1.27 Date 10/12/2001 by Ondra.
- Fixed: soldiers can no fire under water.
\patch 1.53 Date 4/26/2002 by Ondra
- Fixed: When satchel charge was placed under some walkable surface,
it appeared on top of it.
\patch 1.79 Date 7/26/2002 by Ondra
- New: MP: Player who is disconnected from server cannot fire.
*/

bool Soldier::FireWeapon( int weapon, TargetType *target )
{
	if (GetNetworkManager().IsControlsPaused()) return false;
	if (weapon >= NMagazineSlots()) return false;
	if (!IsAbleToFire()) return false;

	if (_waterContact)
	{
		float maxFireDepth = 0.9f;
		if (IsDown()) maxFireDepth = 0.2f;
		if( _waterDepth>maxFireDepth ) return false;
	}
	const MagazineSlot &slot = GetMagazineSlot(weapon);
	Magazine *magazine = slot._magazine;
	if (!magazine)
	{
		if (WeaponsDisabled()) return false;
		if (slot._weapon)
		{
			LogF("Fire weapon %s",(const char *)slot._weapon->GetName());
		}
		if (slot._muzzle)
		{
			LogF("Fire muzzle %s",(const char *)slot._muzzle->GetName());
		}
		if (slot._mode)
		{
			const WeaponModeType *mode = GetWeaponMode(weapon);
			if (mode)
			{
				LogF("Fire mode %d : %s",slot._mode,(const char *)mode->GetName());
			}
			else
			{
				LogF("Fire mode %d",slot._mode);
			}
		}
		PlayEmptyMagazineSound(weapon);
		return false;
	}
	const MagazineType *aInfo = magazine ? magazine->_type : NULL;
	Assert(aInfo);
	const WeaponModeType *mode = GetWeaponMode(weapon);
	Assert(mode);
	
	if( !GetWeaponLoaded(weapon) )
	{
		if (WeaponsDisabled()) return false;

		bool playSound = false;
		if (mode->_ammo && mode->_ammo->_simulation != AmmoNone)
		{
			playSound = 
				magazine->_ammo <= 0 ||
				magazine->_reloadMagazine > 0 ||
				IsActionInProgress(MFReload);
		}

		if (playSound) PlayEmptyMagazineSound(weapon);
		return false;
	}

	bool fired=false;

	if (!mode->_ammo)
	{
		return false;
	}

	switch (mode->_ammo->_simulation)
	{
		case AmmoShotMissile:
		{
			if( !EnableMissile() ) return false;
			// check relative missile direction
			// should be forward animated with GunTransform

			Vector3 dir = GetWeaponRelDirection(weapon);

			fired=FireMissile
			(
				weapon,
				GetWeaponPoint(weapon),
				dir,dir*aInfo->_initSpeed,
				target
			);
			#if PROTECTION_ENABLED
				CheckGenuine();
				#if CHECK_FADE
					FILE *f = fopen("bin\\fade.txt","a");
					if (f)
					{
						fprintf(f,"Fade crash check %.2f\n",Glob.uiTime.toFloat());
						fclose(f);
					}
				#endif
			#endif
		}
		break;
		case AmmoShotStroke:
			if (!IsActionInProgress(MFThrowGrenade))
			{
				MoveId moveWanted = MoveId(MoveIdNone);
				ActionMap *map = Type()->GetActionMap(_primaryMove.id);
				moveWanted = map->GetAction(ManActStrokeGun);
				if (moveWanted==MoveIdNone)
				{
					moveWanted = map->GetAction(ManActStrokeFist);
				}
				if (moveWanted != MoveIdNone)
				{

					Ref<ActionContextDefault> context = new ActionContextDefault;
					context->function = MFThrowGrenade;
					context->param = weapon;
					_forceMove = MotionPathItem(moveWanted,context);
				}
			}
		break;
			
		case AmmoShotShell:
		case AmmoShotSmoke:
		case AmmoShotIlluminating:
		{
			if( WeaponsDisabled() ) return false;
			// check for hand-grenades
			if (aInfo->_initSpeed<30)
			{
				if (!IsActionInProgress(MFThrowGrenade))
				{
					ActionMap *map = Type()->GetActionMap(_primaryMove.id);
					// get which action is it (based on action map)
					ManAction selAction = ManActThrowGrenade;
					MoveId moveWanted = map->GetAction(selAction);
					if (moveWanted != MoveIdNone)
					{
						Ref<ActionContextDefault> context = new ActionContextDefault;
						context->function = MFThrowGrenade;
						context->param = weapon;
						_forceMove = MotionPathItem(moveWanted,context);
					}
				}
				break;
			}
			// launched with GrenadeLauncher or Mortar
			//_gunClouds.Start(0.1f);

			Matrix4Val shootTrans=GunTransform();
			fired=FireShell
			(
				weapon,
				GetWeaponPoint(weapon),
				shootTrans.Direction(),
				target
			);
		}
		break;
		case AmmoShotTimeBomb:
		case AmmoShotPipeBomb:
		case AmmoShotMine:
		{
			//if( WeaponsDisabled() ) return false;
			Vector3 pos = Position() + 0.5f * Direction() + VUp*0.5f;
			pos[1] = GLandscape->RoadSurfaceY(pos);
			fired = FireShell
			(
				weapon,
				PositionWorldToModel(pos),
				Direction(),
				target
			);
			if (fired && mode->_ammo->_simulation == AmmoShotPipeBomb)
				_pipeBombs.Add(_lastShot);
		}
		break;
		case AmmoShotBullet:
		{
			if (DisableWeaponsLong())
			{
				FireAttemptWhenNotPossible();
				return false;
			}
			if( WeaponsDisabled() ) return false;
			Vector3Val pos = GetWeaponPoint(weapon);
			Vector3 dir = GetWeaponRelDirection(weapon);

			if (QIsManual())
			{
				float fov = GScene->GetCamera()->Left();
				// let vehicle adjust the weapon depending on fov
				CameraType camType = GWorld->GetCameraType();
				AdjustWeapon(weapon,camType,fov,dir);
			}

			fired=FireMGun(weapon,pos,dir,target);
		}
		break;
		case AmmoNone:
		break;
		case AmmoShotLaser:
			FireLaser(weapon,target);
			break;
		default:
			Fail("Unknown ammo used.");
		break;
	}

	if( fired )
	{
		base::FireWeapon(weapon, target);
		return true;
	}
	return false;
}

float Soldier::GetRecoilFactor() const
{
	return GetAimPrecision();
}

void Soldier::OnRecoilAbort()
{
	// if there is already some recoil running, apply it to XRotWanted
	float rotX = _recoil->GetRecoilRotX(_recoilTime)*_recoilFactor;
	_gunXRot += rotX;
	_gunXRotWanted += rotX;
	// this does not happen very often
	RecalcGunTransform();
}

/*!
\patch 1.05 Date 7/18/2001 by Ondra.
- New: Weaker recoil when crouching.
\patch_internal 1.05 Date 7/18/2001 by Ondra.
- Changed: Recoil handling no longer supports separate recoil definition.
As recoil factor aimPrecision is used instread.
*/
void Soldier::FireWeaponEffects
(
	int weapon, const Magazine *magazine,EntityAI *target
)
{
	if (weapon<0 || weapon>=NMagazineSlots()) return;
	const MagazineSlot &slot = GetMagazineSlot(weapon);
	if (!magazine || slot._magazine!=magazine) return;

	const MagazineType *aInfo = magazine ? magazine->_type : NULL;
	const WeaponModeType *mode = GetWeaponMode(weapon);
	if (!mode) return;
	if (!mode->_ammo) return;
	
	switch (mode->_ammo->_simulation)
	{
		case AmmoShotMissile:
		break;
		case AmmoShotShell:
		case AmmoShotSmoke:
		case AmmoShotIlluminating:
			if (aInfo->_initSpeed<30) break;
			_gunClouds.Start(0.1f);
		break;
		case AmmoShotBullet:
		{
			_mGunFireWeapon = slot._weapon;
			_mGunClouds.Start(0.1f);
			_mGunFire.Start(0.05f,0.004f,true);
			_mGunFireFrames = 1;
			_mGunFireTime = Glob.uiTime;
			int newPhase;
			while ((newPhase = toIntFloor(GRandGen.RandomValue() * 3)) == _mGunFirePhase);
			_mGunFirePhase = newPhase;
			// TODO: recoil from weapon config
			// check FF params
		}
		break;
	}

	base::FireWeaponEffects(weapon, magazine,target);
}


/////////////////////////////////////////////////////
//
// Soldier implementation
//
/////////////////////////////////////////////////////

DEFINE_CASTING(Soldier)

Soldier::Soldier(VehicleType *name, bool fullCreate)
: base(name,fullCreate)
{
	//SetSimulationPrecision(1.0f/10);
	_brain = new AIUnit(this);
}

Soldier::~Soldier()
{
}

static const float ObjPenalty[4][4]=
{
	{1.0f,1.05f,1.2f,1.5f}, // safe
	{1.0f, 0.7f,0.8f,1.2f}, // aware
	{1.2f, 0.8f,0.5f,1.2f}, // combat
	{1.2f, 0.8f,0.5f,1.2f} // stealth
};

static const float RoadPenalty[4]=
{
	1.0f/1.5f,1.0f/1.2f,1.5f,1.5f
};

static const float ForestPenalty[4]=
{
	1.5f,1.2f,0.7f,0.5f
};

float Soldier::GetFieldCost( const GeographyInfo &info ) const
{
	AIUnit *unit=Brain();
	int modeIndex = 0;
	if (unit)
	{
		modeIndex = unit->GetCombatMode() - CMSafe;
		saturate(modeIndex, 0, 3);
	}

	float cost=1;
	// road fields are expected to be slightly faster
	if( info.u.road || info.u.track ) cost*=RoadPenalty[modeIndex];

	if (info.u.forestOuter)
	{
		cost *= ForestPenalty[modeIndex];
	}
	else
	{
		// fields with objects will be passed through slower
		int nObj=info.u.howManyObjects;
		Assert( nObj<=3 );
		cost *= ObjPenalty[modeIndex][nObj];
	}
	return cost;
}

float Soldier::GetCost( const GeographyInfo &geogr ) const
{
	// avoid any deep water
	if (!(geogr.u.road || geogr.u.track))
	{
		if(geogr.u.waterDepth>=2 || geogr.u.full && !geogr.u.forestOuter) return 1e30f;
	}
	// avoid forests
	// penalty for objects
	float cost=InvFastRunSpeed; // basic running speed
	Assert( geogr.u.howManyObjects<=3 );
	//cost *= ObjPenalty[nObj];
	// avoid steep hills
	// penalty for hills
	cost *= GradientPenalty(geogr.u.gradient);
	// penalty for water
	cost += (float)(int)geogr.u.waterDepth*1.0f; // 1 m/s in water
	return cost;
}

float Soldier::GetCostTurn( int difDir ) const
{
	// in sec
	if( difDir==0 ) return 0;
	float aDir=fabs(difDir);
	float cost=aDir*0.025f;
	if( difDir<0 ) return cost*0.8f;
	return cost;
}

float Soldier::GetTypeCost(OperItemType type) const
{
	// consider using tables
	switch (type)
	{
	case OITNormal:
	default:
		return 1;
	case OITSpaceBush:
		return 2;
	case OITSpaceTree:
	case OITSpace:
	case OITWater:
	case OITSpaceRoad:
		return SET_UNACCESSIBLE;
	case OITAvoidBush:
	case OITAvoidTree:
	case OITAvoid:
		{
			AIUnit *unit = Brain();
			if (unit)
			{
				int cmode = unit->GetCombatMode();
				Assert( cmode>=CMCareless && cmode<=CMStealth );
				// CMCareless, CMSafe, CMAware, CMCombat, CMStealth
				//static float costs[5] = {1.0,1.0,0.5,0.3,0.15};
				static const float costs[5] = {1.0,1.0,0.5,0.25,0.15};
				return costs[cmode-CMCareless];
			}
		}
	}
	return 1;
}

float Soldier::FireInRange( int weapon, float &timeToAim, const Target &target ) const
{
	timeToAim=0;
	Vector3 relDir=PositionWorldToModel(target.position);
	return FireAngleInRange(weapon,relDir);
}

void Soldier::Simulate( float deltaT, SimulationImportance prec )
{
	PROFILE_SCOPE(simSo);
	ADD_COUNTER(simSo,1);
	if( !_isDead )
	{
		if (!_ladderBuilding)
		{
			if( QIsManual() )
			{
				if (GWorld->GetPlayerSuspended() ) SuspendedPilot(deltaT,prec);
				else KeyboardPilot(deltaT,prec);
			}
			// else if (IsRemotePlayer()) FakePilot(deltaT);
			else if (!IsLocal()) FakePilot(deltaT);
#if _ENABLE_AI
			else if ( Brain() )
			{
				if (Brain()->GetAIDisabled() & AIUnit::DAAnim)
					DisabledPilot(deltaT,prec);
				else
					AIPilot(deltaT,prec);
			}
#endif
		}
	}
	else
	{
		//ActionMap *map = Type()->GetActionMap(_primaryMove);
		// get which action is it (based on action map)
		//ManAction selAction = ManActDie;
		//MoveId moveWanted = map->GetAction(selAction);

		// some actions are edge sensitive - must be executed
		if( _forceMove.id!=MoveIdNone )
		{
			SetMoveQueue(MotionPathItem(_forceMove),prec<=SimulateVisibleFar);
		}		
	}
	base::Simulate(deltaT,prec);
}

/*!
\patch 1.31 Date 11/23/2001 by Ondra
- Fixed: AI: Grenade launcher could be fired even when not aimed properly.
*/

float Man::GetAimed( int weapon, Target *target ) const
{
	// check if weapon is aimed
	if (weapon < 0 || weapon >= NMagazineSlots()) return 0;
	if( !target ) return 0;
	if( !target->idExact ) return 0;
	if( WeaponsDisabled() ) return 0;
	float visible=_visTracker.Value(this,_currentWeapon,target->idExact,0.9f);
	if( visible<0.3f ) return 0;

	// check if target was seen recently
	if (target->lastSeen<Glob.time-5) return 0;
	// check if we know exact target position


	const Magazine *magazine = GetMagazineSlot(weapon)._magazine;
	const MagazineType *aInfo = magazine ? magazine->_type : NULL;
	const WeaponModeType *mode = GetWeaponMode(weapon);
	const AmmoType *ammo = mode ? mode->_ammo : NULL;

	if (target->posError.SquareSize() > Square(ammo->indirectHitRange*2)) return 0;

	Vector3 ap=target->AimingPosition();
	if
	(
		ammo &&
		( ammo->_simulation != AmmoShotMissile || ammo->maxControlRange<10 )
	)
	{
		switch (mode->_ammo->_simulation)
		{
			case AmmoShotTimeBomb:
			case AmmoShotMine:
			case AmmoShotPipeBomb:
			{
				// check if we are in good range
				float dist = ap.Distance(Position());
				float rad = target->idExact ? target->idExact->GetRadius()*0.5f : 5;
				float range = (mode->_ammo->midRange*0.7f+mode->_ammo->maxRange*0.3f);
				return( dist<range+rad )*visible;
			}
		}
		
		// 0.6 visibility means 0.8 unaimed
		visible=1-(1-visible)*0.5f;

		// predict shot result
		Vector3 wPos=PositionModelToWorld(GetWeaponPoint(weapon));

		float dist=ap.Distance(wPos);
		float distFactor = dist<40 ? 0 : dist-40;
		dist+=_aimInaccuracyDist*distFactor;
		saturateMax(dist,20);

		float time=dist*aInfo->_invInitSpeed;
		float balFactor = 1;
		float leadFactor = 1;
		if (mode->_ammo->_simulation == AmmoShotMissile)
		{
			time=dist/mode->_ammo->maxSpeed+0.1f;
			balFactor = 0.35f;
			leadFactor = 1.4f;
			if (mode->_ammo->maxControlRange>10) time += 0.3f;
		}
		else if (mode->_ammo->_simulation == AmmoShotLaser)
		{
			balFactor = 0;
			leadFactor = 0;
			time = 0;
		}

		Vector3 leadSpeed=target->speed;
		LimitLead(leadSpeed,aInfo->_maxLeadSpeed*GetAbility());
		Vector3 estPos=ap+leadSpeed*time*leadFactor;

		Vector3 wDir=GetWeaponDirection(weapon);

		float eDist=estPos.Distance(wPos);

		// note: when balistic curve is very steep
		// calculation may be more difficult

		Vector3 hit = wPos+wDir*eDist;
		hit[1] -= 0.5f*G_CONST*balFactor*time*time;
		// adapt hit to be in correct distance

		// distance should be eDist
		Vector3 norm = (hit-wPos);
		hit = norm.Normalized()*eDist+wPos;

		#if _ENABLE_CHEATS
			float distHit = hit.Distance(wPos);
		#endif

		Vector3 hError = hit-estPos;

		if (aInfo && mode->_ammo->_simulation == AmmoShotShell)
		{
			if (aInfo->_initSpeed >=30)
			{
				// grenade launcher - mistake in y-axis may have fatal results
				hError[1] *= 4;
			}
		}
		float error = hError.Size();
		float aimPrecision=(GetInvAbility()-1)*1.5f+1;

		if (eDist<70)
		{
			// when target is very near, increase ability
			float distAcc = 1+floatMax(eDist*1.0f/70,0.2f);
			//LogF("aimPrecision %.3f, distAcc %.3f",aimPrecision,distAcc);
			saturateMin(aimPrecision,distAcc);
		}

		if (aInfo && mode->_ammo->_simulation == AmmoShotShell)
		{
			if(aInfo->_initSpeed < 30)
			{
				// hand grenade
				error *= 0.5f;
				// we are not able to throw grenade very precisely
				saturateMax(aimPrecision,5);
			}
			// grenade launcher
		}
		else if (mode->_ammo->_simulation==AmmoShotMissile)
		{
			error *= 2;
		}
		else
		{
			// adjust error (make it smaller)
			error *= 0.75f;
		}

		float tgtSize=target->idExact->VisibleSize()*aimPrecision*0.25f;
		const MuzzleType *muzzle = GetMagazineSlot(weapon)._muzzle;
		float dCoef=floatMax(muzzle->_aiDispersionCoefX,muzzle->_aiDispersionCoefY);
		tgtSize+=dist*mode->_dispersion*dCoef*aimPrecision*0.5f;

#if _ENABLE_CHEATS
		if ((Object *)this==GWorld->CameraOn() && CHECK_DIAG(DECombat))
		{
			GlobalShowMessage
			(
				2000,
				"Error %.1f, tgtSize %.1f, time %.2f, "
				"ePos %.1f,%.1f,%.1f, distErr %.1f",
				error,tgtSize,time,
				hError[0],hError[1],hError[2],
				distHit-eDist
			);
		}
#endif

		if( error>=tgtSize*2 ) return 0;
		if (!CheckFriendlyFire(weapon,target)) return 0;
		if( error<=tgtSize ) return visible;
		return ( 2*tgtSize-error )/tgtSize*visible;
	}
	else
	{
		if( visible<0.6f ) return 0;

		// 0.6 visibility means 0.8 unaimed
		visible=1-(1-visible)*0.5f;
		Vector3 relPos=ap-Position();
		// check if target is in front of us
		if( relPos.SquareSize()<=Square(30) ) return 0; // missile fire impossible
		// check if target position is withing missile lock cone
		Vector3Val wDir=GetWeaponDirection(weapon);

		float wepCos=relPos.CosAngle(wDir);
		float misAlignCoef  = 30;
		float wepAimed=1-(1-wepCos)*misAlignCoef;

		// unguided missile must be better aligned

		saturateMax(wepAimed,0);

#if _ENABLE_CHEATS
		if ((Object *)this==GWorld->CameraOn() && CHECK_DIAG(DECombat))
		{
			GlobalShowMessage
			(
				2000,
				"AT visible %.3f, aimed %.3f",visible,wepAimed
			);
		}
#endif

		if (!CheckFriendlyFire(weapon,target)) return 0;
		return visible*wepAimed;
	}
}

int Soldier::MissileIndex() const
{
	for (int i=0; i<NMagazineSlots(); i++)
	{
		const WeaponModeType *mode = GetWeaponMode(i);
		const AmmoType *ammo = mode ? mode->_ammo : NULL;
		if (ammo && ammo->_simulation == AmmoShotMissile) return i;
	}
	return -1;
}

#if _ENABLE_AI

void Soldier::AIFire( float deltaT )
{
#if _ENABLE_CHEATS
	extern bool disableUnitAI;
	if( disableUnitAI ) return;
#endif
	AIUnit *unit = Brain();
	if( !unit ) return;

	// periodically change between lookign forward and on target
	if (_lookTargetTimeLeft>0)
	{
		_lookTargetTimeLeft -= deltaT;
	}
	else
	{
		_lookForwardTimeLeft -= deltaT;
		if (_lookForwardTimeLeft<=0)
		{
			if (BinocularSelected())
			{
				_lookTargetTimeLeft = 4+GRandGen.RandomValue()*4;
				_lookForwardTimeLeft = 30+GRandGen.RandomValue()*5;
			}
			else if (unit->GetCombatMode()<=CMSafe || FindWeaponType(MaskSlotPrimary)<0)
			{
				if (fabs(_walkSpeedWanted)<0.1f)
				{
					// safe and static - almost no need to look forward
					_lookTargetTimeLeft = 4+GRandGen.RandomValue()*8;
					_lookForwardTimeLeft = 2+GRandGen.RandomValue()*3;
				}
				else
				{
					// safe, but walking - look forward often
					_lookTargetTimeLeft = 4+GRandGen.RandomValue()*8;
					_lookForwardTimeLeft = 3+GRandGen.RandomValue()*5;
				}
			}
			else
			{	
				_lookTargetTimeLeft = 1+GRandGen.RandomValue()*2;
				_lookForwardTimeLeft = 15+GRandGen.RandomValue()*5;
			}
		}

	}

	SelectFireWeapon();
	if( !GetFireTarget() || !GetFireTarget()->idExact )
	{
		// keep gun in neutral position
		if (_forceFireWeapon<0)
		{
			_gunXRotWanted = 0;
			_gunYRotWanted = 0;
			// follow watch direction
			Vector3Val dir = unit->GetWatchHeadDirection();

			if (dir.SquareSize()>0.1f)
			{
				// check if we have any
				AimHeadAI(dir,deltaT);

			}
			else
			{
				_headXRotWanted = 0;
				_headYRotWanted = 0;
			}
		}
		_laserTargetOn = false;
		return;
	}
	SelectWeapon(_fire._fireMode);
	AimWeaponAI(_fire._fireMode,GetFireTarget(),deltaT);
	if( _fire._fireMode>=0 )
	{
		if (_fire._fireMode>=NMagazineSlots())
		{
			ErrF
			(
				"%s: Bad weapon selected (%d of %d)",
				(const char *)GetDebugName(),
				_fire._fireMode,NMagazineSlots()
			);
			_fire._fireMode = -1;
		}
		if( _fire._firePrepareOnly ) return;
		if
		(
			GetWeaponLoaded(_fire._fireMode) &&
			GetWeaponReady(_fire._fireMode,GetFireTarget()) &&
			GetAimed(_fire._fireMode,GetFireTarget())>=0.5f &&
			!WeaponsDisabled()
		)
		{
			if (!GetAIFireEnabled(GetFireTarget())) ReportFireReady();
			else
			{
				const WeaponModeType *mode = GetWeaponMode(_fire._fireMode);				
				if (mode && mode->_useAction)
				{
					UIAction action;
					action.type = ATUseWeapon;
					action.target = this;
					action.param = _fire._fireMode;
					action.param2 = 0;
					action.priority = 0;
					action.showWindow = false;
					action.hideOnUse = false;
					Ref<ActionContextUIAction> context = new ActionContextUIAction(action);
					PlayAction(ManActPutDown, context);
				}
				else if (mode->_ammo && mode->_ammo->_simulation==AmmoShotLaser)
				{
					Target *target = GetFireTarget();
					if (!target->destroyed)
					{
						if (!_laserTargetOn)
						{
							//LogF("%s: start laser",(const char *)GetDebugName());
							FireWeapon(_fire._fireMode, target->idExact);
						}
						return;
					}
				}
				else
				{
					FireWeapon(_fire._fireMode, GetFireTarget()->idExact);
				}
			}
		}
	}
	_laserTargetOn = false;
}

#endif //_ENABLE_AI

//! random decision model
/*!
Function should return true during ratio*period time.
Average one decision change should be made per period.
Function requires no storage.
*/

bool RandomDecision(AIUnit *unit, float period, float ratio)
{
	// part of seed is based on unit pointer or ID
	int seed = unit->ID()*2568;
	float timeMod = fastFmod(Glob.time.toFloat()+seed,period);
	return timeMod<ratio*period;
}

#define DIAG_FIRE 0

#if _ENABLE_AI

/*!
\patch 1.97 Date 6/4/2004 by Jirka
- Added: disableAI option for disable automatic animation selection
*/

void Soldier::DisabledPilot(float deltaT, SimulationImportance prec)
{
	_turnWanted=0;
	//_sideSpeedWanted=0;
	_walkSpeedWanted=0;

	// set primary move to stand

	AdvanceExternalQueue();

	if (_externalMove.id!=MoveIdNone)
	{
		if (SetMoveQueue(_externalMove,prec<=SimulateVisibleFar))
		{
			//AdvanceExternalQueue(false);
		}
	}
}

/*!
\patch 1.01 Date 6/26/2001 by Ondra.
- Fixed: Weapon (AT missile) activation when ordered to stand-up or lay down.
\patch 1.05 Date 7/17/2001 by Ondra.
- New: AI uses walking with riffle animation when appropriate.
\patch 1.50 Date 4/11/2002 by Ondra
- Fixed: Minor visual bug in AI soldier turning animation interpolation.
\patch 1.50 Date 4/13/2002 by Ondra
- New: AI uses crouch position in some situations.
*/

void Soldier::AIPilot(float deltaT, SimulationImportance prec)
{
	AdvanceExternalQueue();
	if (_externalMove.id != MoveIdNone)
	{
		_turnWanted=0;
		//_sideSpeedWanted=0;
		_walkSpeedWanted=0;
		if (SetMoveQueue(_externalMove,prec<=SimulateVisibleFar))
		{
			//AdvanceExternalQueue(false);
		}
		return;
	}

#if _ENABLE_CHEATS
	if (this==GWorld->CameraOn())
	{
		// enable placing breakpoint at camera vehicle AI
		__asm nop;
	}
	extern bool disableUnitAI;
	if( disableUnitAI ) return;
#endif

	// TODO: limit AIPilot simulation rate (10 ps)
	AIUnit *unit=Brain();
	Assert( unit );
	Assert( unit->GetSubgroup() );
	bool isLeader=unit->IsSubgroupLeader();

	//Vector3Val speed=ModelSpeed();
	
	float headChange=0;
	float speedWanted=0;
	float turnPredict=0;

	// if we are near the target we have to operate more precisely
	// aim to current target
	if( !_isDead ) AIFire(deltaT);
	
	if( !_fire._fireTarget || _fire.GetTargetFinished(unit) )
	{
		_fire._fireMode = -1;
		_fire._fireTarget = NULL;
	}

	#if DIAG_FIRE
		LogF("Fire %d state %d prepare %d",_fireMode,_fireState,_firePrepareOnly);
	#endif

	if( unit->GetState()==AIUnit::Stopping )
	{
		unit->SendAnswer(AI::StepCompleted);
		unit=Brain();
		speedWanted=0;
		return;
	}
	else if( unit->GetState()==AIUnit::Stopped )
	{
		speedWanted=0;
	}
	else if
	(
		_fire._fireMode>=0 &&
		(!_fire._firePrepareOnly || !unit->IsKeepingFormation()) &&
		_fireState!=FireDone &&
		_fire._fireTarget && _fire._fireTarget->IsKnownBy(unit) &&
		_fire._fireTarget->idExact && // ???
		unit->IsFireEnabled(_fire._fireTarget) &&
		GetMagazineSlot(_fire._fireMode)._magazine &&
		GetMagazineSlot(_fire._fireMode)._magazine->_reloadMagazine<=1
	)
	{
		// we must fire - stop
		// check if we can aim when we stop
		const WeaponModeType *mode = GetWeaponMode(_fire._fireMode);
		if (mode && mode->_ammo)
		{
			switch (mode->_ammo->_simulation)
			{
				case AmmoShotTimeBomb:
				case AmmoShotPipeBomb:
					goto NormalMove;
			}
		}
		if( _fireState==FireInit  )
		{
			if (Glob.time>_fireStateDelay)
			{
				float maxDist = 1000;
				if (mode && mode->_ammo)
				{
					maxDist = mode->_ammo->midRange*0.4f+mode->_ammo->maxRange*0.6f;
				}
				float visVal = _visTracker.Value(this,_currentWeapon,_fire._fireTarget->idExact,0.9f);
				if
				(
					visVal>=0.5f &&
					(Position()-_fire._fireTarget->AimingPosition()).SquareSize()<Square(maxDist)
				)
				{
					_fireState = FireAim;
					_fireStateDelay = Glob.time+20;
					//LogF("%s:    aim: visVal %.3f",(const char *)GetDebugName(),visVal);
				}
				else
				{
					// retry init phase after some time
					_fireStateDelay = Glob.time+0.5;
					//LogF("%s: no aim: visVal %.3f",(const char *)GetDebugName(),visVal);
					//_firePrepareOnly=true;
				}
			}
			goto NormalMove;
		}
		else if( _fireState==FireAim )
		{
			Vector3 aimDir=PositionWorldToModel(_fire._fireTarget->LandAimingPosition());
			headChange=atan2(aimDir.X(),aimDir.Z());
			speedWanted=0;
			if( fabs(headChange)<=0.05f )
			{
				float reloadTime=10;
				if (_fire._fireMode >= 0 && mode) reloadTime=mode->_reloadTime;
				_fireState=FireAimed,_fireStateDelay=Glob.time+reloadTime*3+10;
				//LogF("%s: Aimed",(const char *)GetDebugName());
			}
			else if( Glob.time>_fireStateDelay )
			{
				// fail
				_fireState=FireDone;
				_fireStateDelay=Glob.time+10; // delay before next fire pause
				//LogF("%s: FireDone",(const char *)GetDebugName());
			}
		}
		else if( _fireState==FireAimed )
		{
			Vector3 aimDir=PositionWorldToModel(_fire._fireTarget->LandAimingPosition());
			headChange=atan2(aimDir.X(),aimDir.Z());
			speedWanted=0;
			if( Glob.time>_fireStateDelay )
			{
				#if DIAG_FIRE
					LogF("%x: FireDone",this);
				#endif
				_fireState=FireDone;
				_fireStateDelay=Glob.time+5; // delay before next fire pause
				//LogF("%s: FireDone",(const char *)GetDebugName());
			}
		}
		else
		{
			Fail("FireState");
			goto NormalMove;
		}
		// if stopped and not aimed, continue
	}
	else
	{
		_fireState=FireDone;
		NormalMove:
		if( !isLeader )
		{
			FormationPilot(speedWanted,headChange,turnPredict);
		}
		else
		{
			LeaderPilot(speedWanted,headChange,turnPredict);
		}
		// if there is some path, check if it uses building
		// if yes, mark it
		// TODO: check current position
		const Path &path = unit->GetPath();
		if (path.Size()>0)
		{
			bool building = false;
			for (int i=0; i<path.Size(); i++)
			{
				if (path[i]._house) {building = true;break;}
			}
			_inBuilding = building;
		}
	}

	//if (goBack) speedWanted=-speedWanted;
	AvoidCollision(deltaT,speedWanted,headChange);


	{
		// slow down when turning
		float maxSpeed=Type()->GetMaxSpeedMs();
		float limitSpeed=Interpolativ(fabs(headChange),0,H_PI/4,maxSpeed,0);
		saturate(speedWanted,-limitSpeed,+limitSpeed);
	}
	
	// make appropriate reaction based on speedWanted, headChange and wantedPosition

	float turnWanted=headChange*3;
	float delta=turnWanted-_turnWanted;
	saturate(delta,-5*deltaT,+5*deltaT);

	_turnWanted+=delta;
	saturate(_turnWanted,-5,+5);

	// if movement is necessary, stand up

	ManPos minPos = ManPosLying; // default is: we can be in any position
	ManPos maxPos = ManPosStand;

	// convert from minPos,maxPos to old scheme
	AIGroup *g=GetGroup();
	AISubgroup *sg=unit->GetSubgroup();
	//AIUnit *sgLeader = sg->Leader();
	//AIUnit *gLeader = g->Leader();

	float delay = 0.5f;	

	if (IsLaunchDown())
	{
		// we have missile ready - wait with it for a while
		delay = 3;
	}

	// FIX weapon activation before _unitPos check
	if( sg && sg->GetMode()==AISubgroup::DirectGo )
	{
		minPos = ManPosCombat; // disable lying - force run
	}
	else if( g && g->GetFlee() )
	{
		minPos = ManPosCombat; // disable lying - force run
	}
	else if( LauncherFire() )
	{
		// if RPG soldier is ready to fire, force weapon activation
		minPos = ManPosWeapon;
		maxPos = ManPosWeapon;
	}
	else if
	(
		LauncherSelected() && LauncherReady() && fabs(speedWanted)<0.5f &&
		unit->GetCombatMode()>=CMCombat && LauncherWanted()
	)
	{
		// check if we should have launcher prepared
		delay = 2;
		// then delay before giving it up
		minPos = ManPosWeapon;
		maxPos = ManPosWeapon;
	}
	else if (_unitPos==UPAuto)
	{
		if (unit->IsDanger() && !_inBuilding)
		{
			maxPos = ManPosLying; // force lying
		}
		else if( _hideBehind )
		{
			const Path &path=unit->GetPath();
			// check distance from target
			if( path.Size()>=2 )
			{
				float distance2 = path.End().Distance2(Position());
				if( distance2>Square(5) )
				{
					if( distance2>Square(30) )
					{
						minPos = ManPosCombat;
					}
					maxPos = ManPosCombat; // enable lying, but do not force
				}
				else
				{
					maxPos = ManPosLying; // force lying
				}
			}
			else if (speedWanted<1.0f)
			{
				if (!IsDown() && RandomDecision(unit,30,0.7f))
				{
					maxPos = ManPosCrouch;
				}
				else
				{
					maxPos = ManPosLying;
				}
			}
		}
		else if( unit->GetCombatMode() == CMAware )
		{
			minPos = ManPosCombat;
			maxPos = ManPosCombat;
		}
		else if( unit->GetCombatMode() <= CMSafe )
		{
			minPos = ManPosStand;
			maxPos = ManPosStand;
		}
		else if( unit->GetCombatMode() == CMCombat )
		{
			// check speed wanted
			if (speedWanted<2.0f && !_inBuilding)
			{
				// if we are not lying yet, consider crouching
				if (!IsDown() && RandomDecision(unit,30,0.3f))
				{
					maxPos = ManPosCrouch;
				}
				else
				{
					maxPos = ManPosLying;
				}
			}
			else
			{
				minPos = maxPos = ManPosCombat;
			}
		}
		else if( unit->GetCombatMode() == CMStealth )
		{
			// if there is no enemy known or expected, do not lay down

			int x = toIntFloor(Position().X() * InvLandGrid);
			int z = toIntFloor(Position().Z() * InvLandGrid);

			float exposure;
			if (unit->IsHoldingFire())
			{
				exposure = GetGroup()->GetCenter()->GetExposurePessimistic(x, z);
			}
			else
			{
				exposure = GetGroup()->GetCenter()->GetExposureOptimistic(x, z);
			}

			if (exposure<400 && speedWanted>2.0f)
			{
				minPos = maxPos = ManPosCombat;
			}
			else
			{
				minPos = maxPos = ManPosLying;
			}
		}
	} // if (auto lay down)
	else if (_unitPos==UPUp)
	{
		minPos = ManPosCombat;
	}
	else if (_unitPos==UPDown)
	{
		maxPos = ManPosLying;
	}


	// TODO: check if I am in the house

	// check if we would like to use binocular

	if (maxPos==ManPosLying)
	{
		//LogF("%s: force down",(const char *)GetDebugName());
		if (_fire._fireTarget && !_fire._firePrepareOnly)
		{
			// from some reason we are forced to lay down
			// we are aiming to some very near target - do not force lying down
			float dist2 = _fire._fireTarget->AimingPosition().Distance2(Position());
			if (dist2<Square(60))
			{
				// we might crouch
				if (!_inBuilding && RandomDecision(unit,60,0.8f))
				{
					maxPos = ManPosCrouch;
				}
				else
				{
					maxPos = ManPosCombat;
				}
				//LogF("  enable up");
			}
		}
	}

	bool forceStand = false;
	float slope = LandSlope(forceStand);
	if (!IsAbleToStand() || (!forceStand && !CanStand(slope)))
	{
		if (LauncherSelected()) minPos = ManPosWeapon;
		else minPos = ManPosLying;
		maxPos = ManPosLying;
	}

	/*
	// we may force lying also when the slope is too steep

	// consider terrain when selecting movement
	float dx,dz;
	GLandscape->SurfaceY(Position().X(),Position().Z(),&dx,&dz);
	// if surface is 
	*/



	// prepare for new animation
	// check which action map is used now
	ActionMap *map = Type()->GetActionMap(_primaryMove.id);

	ManAction selAction = ManActN;

	int upDegree = map ? map->GetUpDegree() : ManPosStand;
	if (_posWanted<minPos || _posWantedTime>Glob.time+30)
	{
		_posWanted = minPos;
		_posWantedTime = Glob.time+(GetInvAbility()*delay+delay*0.5f)*GRandGen.RandomValue();
	}
	else if( _posWanted>maxPos )
	{
		_posWanted = maxPos;
		_posWantedTime = Glob.time+(GetInvAbility()*delay+delay*0.5f)*GRandGen.RandomValue();
	}

	// check if we have some primary weapon
	int primaryIndex = FindWeaponType(MaskSlotPrimary);
	int handGunIndex = FindWeaponType(MaskSlotHandGun);

	bool isWeapon = primaryIndex >= 0 || handGunIndex >= 0;
	if (!isWeapon)
	{
		if (_posWanted==ManPosCombat || _posWanted==ManPosStand)
		{
			_posWanted = ManPosNoWeapon;
			Time maxTime = Glob.time;
			if (_posWantedTime>maxTime) _posWantedTime=maxTime;
		}
		else if (_posWanted==ManPosLying)
		{
			_posWanted = ManPosLyingNoWeapon;
			Time maxTime = Glob.time;
			if (_posWantedTime>maxTime) _posWantedTime=maxTime;
		}
	}

	//LogF("_posWanted %d",_posWanted);

	if (_posWanted==ManPosWeapon && _posWantedTime<=Glob.time)
	{
		selAction = ManActWeaponOn;
		//LogF("%s: action weapon",(const char *)GetDebugName());
	}
	else if (BinocularSelected() && _lookTargetTimeLeft>0)
	{
		selAction = ManActBinocOn;
	}
	else if (IsHandGunSelected() && IsPrimaryWeaponInMove())
	{
		selAction = ManActHandGunOn; // use hand gun
	}
	else if (!IsHandGunSelected() && IsHandGunInMove())
	{
		int actPos = GetActUpDegree();
		if (actPos == ManPosHandGunCrouch)
		{
			if (primaryIndex < 0)
				selAction = ManActCivil;
			else
				selAction = ManActCrouch;
		}
		else if (actPos == ManPosHandGunLying)
		{
			if (primaryIndex < 0)
				selAction = ManActCivilLying;
			else
				selAction = ManActLying;
		}
		else
		{
			Assert(actPos == ManPosHandGunStand);
			if (primaryIndex < 0)
				selAction = ManActCivil;
			else
				selAction = ManActCombat;
		}
	}
	else if (upDegree!=_posWanted && _posWantedTime<=Glob.time)
	{
		//_posWantedTime = TIME_MAX; // cancel request
		if (upDegree==ManPosWeapon)
		{
			if (_posWanted!=ManPosWeapon)
			{
				selAction = IsHandGunSelected() ? ManActHandGunOn : ManActWeaponOff;
			}
			else
			{
				selAction = ManActWeaponOn;
			}
		}
		else if (_posWanted<ManPosNormalMin)
		{
		}
		else
		{
			if (_posWanted == ManPosLying)
			{
				if (upDegree != ManPosHandGunLying)
				{
					if (IsHandGunSelected())
					{
						if (upDegree == ManPosHandGunCrouch || upDegree == ManPosHandGunStand)
							selAction = ManActDown;
						else
							selAction = ManActHandGunOn;
					}
					else selAction = ManActLying;
				}
			}
			else if (_posWanted == ManPosCrouch)
			{
				if (upDegree != ManPosWeapon && upDegree != ManPosHandGunCrouch)
				{
					if (IsHandGunSelected())
					{
						if (upDegree == ManPosHandGunLying || upDegree == ManPosHandGunStand)
							selAction = ManActDown;
						else
							selAction = ManActHandGunOn;
					}
					else selAction = ManActCrouch;
				}
			}
			else if (_posWanted == ManPosCombat)
			{
				// note: some upDegrees may be valid
				if (upDegree != ManPosWeapon && upDegree != ManPosHandGunStand)
				{
					if (IsHandGunSelected())
					{
						if (upDegree == ManPosHandGunLying)
							selAction = ManActDown;
						else if (upDegree == ManPosHandGunCrouch || upDegree == ManPosHandGunStand)
							selAction = ManActUp;
						else
							selAction = ManActHandGunOn;
					}
					else selAction = ManActCombat;
				}
			}
			else if( _posWanted==ManPosStand ) selAction = ManActStand;
			else if( _posWanted==ManPosNoWeapon ) selAction = ManActCivil;
			else if( _posWanted==ManPosLyingNoWeapon ) selAction = ManActCivilLying;
			//LogF("%s: action ch,   %d->%d",DNAME(),upDegree,_posWanted);
		}
	}
	if (selAction==ManActN)
	{
		selAction=ManActStop;
		// normal movement - check speedWanted (maybe also speedAside?)
		float limitFast = map ? map->GetLimitFast() : 5;
		float limitSlow = map ? map->GetLimitFast()*0.6f : 1;

		if (!_canMoveFast || !CanSprint(slope) && !IsDown()) limitFast = 100; // never true
		if (!CanRun(slope) && !IsDown()) limitSlow = 100; // never true
		if( speedWanted>limitFast ) selAction = ManActFastF;
		else if( speedWanted>limitSlow ) selAction = ManActSlowF;
		else if( speedWanted>0.1f ) selAction = ManActWalkF;
		else if( speedWanted<-0.1f ) selAction = ManActWalkB;
		else if( speedWanted<-limitSlow ) selAction = ManActSlowB;
		if( speedWanted<-limitFast ) selAction = ManActFastB;

		if (unit->GetCombatMode()==CMAware)
		{
			// when in aware, do not use walking - looks to much combat'ish
			if (selAction==ManActWalkF) selAction = ManActSlowF;
			if (selAction==ManActWalkB) selAction = ManActSlowB;
		}
	}

	if (selAction==ManActStop)
	{
		if( fabs(_turnWanted)>0.5f)
		{
			selAction = _turnWanted<0 ? ManActTurnL : ManActTurnR;
			if (unit->GetCombatMode()==CMAware)
			{
				// first choice - relaxed
				ManAction relAction = _turnWanted<0 ? ManActTurnLRelaxed : ManActTurnRRelaxed;
				if (map->GetAction(relAction)!=MoveIdNone)
				{
					selAction = relAction;
				}
			}
			/*
			LogF
			(
				"Turn %.1f, action %s",_turnWanted,(const char *)FindEnumName(selAction)
			);
			*/
		}
		else if (unit->GetCombatMode()==CMAware)
		{
			if (map->GetAction(ManActStopRelaxed)!=MoveIdNone)
			{
				selAction = ManActStopRelaxed;
			}
		}
	}

	// get which action is it (based on action map)
	MoveId moveWanted = map ? map->GetAction(selAction) : GetDefaultMove();
	
	// LogF("%s: moveWanted %s (action %s)",DNAME(),NAME_T(Type(), moveWanted), (const char *)ManActionNames[selAction].name);


	if( _forceMove.id!=MoveIdNone )
	{
		SetMoveQueue(_forceMove,prec<=SimulateVisibleFar);
	}
	else
	{
		SetMoveQueue(MotionPathItem(moveWanted),prec<=SimulateVisibleFar);
	}

	_walkSpeedWanted=speedWanted;
	
	//AutoGuide(deltaT,true,false); // brake, do not avoid
}
#endif //_ENABLE_AI

LSError Man::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))

	// TODO: default value for SerializeEnumValue
	// CHECK(ar.SerializeEnumValue("primaryMove", _primaryMove.id, 1, Type()->GetMoveIdNames() ))
	// CHECK(ar.SerializeEnumValue("secondaryMove", _secondaryMove.id, 1, Type()->GetMoveIdNames() ))

	if (!IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
	{
		if (ar.IsSaving())
		{
			RString name = _primaryMove.id == MoveIdNone ? "" : Type()->GetMoveName(_primaryMove.id);
			CHECK(ar.Serialize("primaryMove", name, 1, ""))
			name = _secondaryMove.id == MoveIdNone ? "" : Type()->GetMoveName(_secondaryMove.id);
			CHECK(ar.Serialize("secondaryMove", name, 1, ""))
		}
		else if (ar.GetPass() == ParamArchive::PassFirst)
		{
			RString name;
			CHECK(ar.Serialize("primaryMove", name, 1, ""))
			if (name.GetLength() == 0)
				_primaryMove = MotionPathItem(MoveId(MoveIdNone));
			else
				_primaryMove = MotionPathItem(Type()->GetMoveId(name));
			CHECK(ar.Serialize("secondaryMove", name, 1, ""))
			if (name.GetLength() == 0)
				_secondaryMove = MotionPathItem(MoveId(MoveIdNone));
			else
				_secondaryMove = MotionPathItem(Type()->GetMoveId(name));
		}
			
		CHECK(ar.Serialize("primaryFactor", _primaryFactor, 1, 1 ))
		CHECK(ar.Serialize("moveTime", _primaryTime, 1, 1 ))
		CHECK(ar.Serialize("secMoveTime", _secondaryTime, 1, 1 ))

		//CHECK(ar.Serialize("rndSpeed", _rndSpeed, 1))
		//CHECK(ar.Serialize("walkSpeed", _walkSpeed, 1, 0))
		CHECK(ar.Serialize("walkSpeedWanted", _walkSpeedWanted, 1, 0))
		//CHECK(ar.Serialize("sideSpeed", _sideSpeed, 1, 0))
		//CHECK(ar.Serialize("sideSpeedWanted", _sideSpeedWanted, 1, 0))

		CHECK(ar.Serialize("whenScreamed", _whenScreamed, 1, TIME_MAX))

		CHECK(ar.Serialize("tired", _tired, 1, 0))
		CHECK(ar.Serialize("hideBody", _hideBody, 1, 0))
		CHECK(ar.Serialize("hideBodyWanted", _hideBodyWanted, 1, 0))
		CHECK(ar.SerializeEnum("unitPos", _unitPos, 1, (UnitPosition)UPAuto))

		CHECK(ar.SerializeRef("flagCarrier", _flagCarrier, 1))
		CHECK(ar.SerializeRef("laserTarget", _laserTarget, 1))
		CHECK(ar.Serialize("laserTargetOn", _laserTargetOn, 1, false))

		CHECK(ar.SerializeRef("ladderBuilding", _ladderBuilding, 1))
		CHECK(ar.Serialize("ladderIndex", _ladderIndex, 1, -1))
		CHECK(ar.Serialize("ladderPosition", _ladderPosition, 1, -1))

		CHECK(ar.SerializeRefs("pipeBombs",_pipeBombs,1 ))

		CHECK(ar.Serialize("handGun", _handGun, 1, false))
	
		if (ar.IsLoading())
		{
			// we could serialize _hasNVG, but then we would need to care
			// about older version files
			ScanNVG();
		}

		if (ar.IsLoading() )
		{
			RecalcGunTransform();
			if (IsInLandscape())
			{
				RecalcPositions(*this);
			}
			else
			{
				// TODO: check for parent

				_aimingPositionWorld = VZero;
				_cameraPositionWorld = VZero;
			}
		}
	}
	return LSOK;

/* TODO: ?? Serialize
	float _gunYRot,_gunYRotWanted;
	float _gunXRot,_gunXRotWanted;
	float _gunXSpeed,_gunYSpeed;

	Ref<AbstractWave> _sound;

	bool _doSoundStep:1;

	bool _pilotLayDown:1,_pilotStandUp:1,_pilotLaunchDown:1;
	bool _endSpecMode:1; // transition from spec mode

	float _timeToCrawl; // crawl/run time left
	float _timeToRun; // crawl/run time left

	Time _freeFallUntil;

	Time _layDownTime;
	Time _standUpTime;
	float _launchDelay;
	float _standUpDelay; // (group leader is standing - stand too)

	float _aimInaccuracyX,_aimInaccuracyY;
	Time _lastInaccuracyTime;
	float _aimInaccuracyDist;
	Time _lastInaccuracyDistTime;

	float _waterDepth;

	Time _whenKilled; // helpers for suspending dead body after a while
	Time _lastMovementTime;

	float _turnWanted;
*/
}

NetworkMessageType Man::GetNMType(NetworkMessageClass cls) const
{
	switch (cls)
	{
	case NMCUpdateGeneric:
		return NMTUpdateMan;
	case NMCUpdatePosition:
		return NMTUpdatePositionMan;
	default:
		return base::GetNMType(cls);
	}
}

class IndicesUpdateMan : public IndicesUpdateVehicleBrain
{
	typedef IndicesUpdateVehicleBrain base;

public:
	int hideBodyWanted;
	int tired;
	//int rndSpeed;
	int walkSpeedWanted;
	//int sideSpeedWanted;
	int unitPos;
	int flagCarrier;
	int nvg;
	int ladderBuilding;
	int ladderIndex;
	int ladderPosition;

	IndicesUpdateMan();
	NetworkMessageIndices *Clone() const {return new IndicesUpdateMan;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesUpdateMan::IndicesUpdateMan()
{
	hideBodyWanted = -1;
	tired = -1;
	//rndSpeed = -1;
	walkSpeedWanted = -1;
	//sideSpeedWanted = -1;
	unitPos = -1;
	flagCarrier = -1;
	nvg = -1;
	ladderBuilding = -1;
	ladderIndex = -1;
	ladderPosition = -1;
}

void IndicesUpdateMan::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);

	SCAN(hideBodyWanted)
	SCAN(tired)
	//SCAN(rndSpeed)
	SCAN(walkSpeedWanted)
	//SCAN(sideSpeedWanted)
	SCAN(unitPos)
	SCAN(flagCarrier)
	SCAN(nvg)
	SCAN(ladderBuilding);
	SCAN(ladderIndex);
	SCAN(ladderPosition);
}

NetworkMessageIndices *GetIndicesUpdateMan() {return new IndicesUpdateMan();}


class IndicesUpdatePositionMan : public IndicesUpdatePositionVehicle
{
	typedef IndicesUpdatePositionVehicle base;

public:
	/*
	int gunXRotWanted;
	int gunYRotWanted;
	int headXRotWanted;
	int headYRotWanted;
	*/
	int manUpdPos;
	int move;

	IndicesUpdatePositionMan();
	NetworkMessageIndices *Clone() const {return new IndicesUpdatePositionMan;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesUpdatePositionMan::IndicesUpdatePositionMan()
{
	manUpdPos = -1;
	/*
	gunXRotWanted = -1;
	gunYRotWanted = -1;
	headXRotWanted = -1;
	headYRotWanted = -1;
	*/
	move = -1;
}

void IndicesUpdatePositionMan::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);

	/*
	SCAN(gunXRotWanted)
	SCAN(gunYRotWanted)
	SCAN(headXRotWanted)
	SCAN(headYRotWanted)
	*/
	SCAN(manUpdPos)
	SCAN(move)
}

NetworkMessageIndices *GetIndicesUpdatePositionMan() {return new IndicesUpdatePositionMan();}

/*!
\patch 1.27 Date 10/11/2001 by Ondra.
- Optimized: Soldier update more compressed.
*/

NetworkMessageFormat &Man::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	switch (cls)
	{
	case NMCUpdateGeneric:
		base::CreateFormat(cls, format);

		format.Add("hideBodyWanted", NDTFloat, NCTFloat0To1, DEFVALUE(float, 0), DOC_MSG("Wanted state of body (1 .. fully hidden)"), ET_ABS_DIF, ERR_COEF_VALUE_MINOR);

		format.Add("tired", NDTFloat, NCTFloat0To1, DEFVALUE(float, 0), DOC_MSG("How man is tired"));
		//format.Add("rndSpeed", NDTFloat, NCTNone, DEFVALUE(float, 0));
		format.Add("walkSpeedWanted", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Wanted speed"));
		//format.Add("sideSpeedWanted", NDTFloat, NCTNone, DEFVALUE(float, 0));
		format.Add("unitPos", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, UPAuto), DOC_MSG("Up / down state"));
		format.Add("flagCarrier", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Carried flag"), ET_NOT_EQUAL, ERR_COEF_MODE);
		format.Add("nvg", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Night vision is active"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);

		format.Add("ladderBuilding", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Ladder ID"), ET_NOT_EQUAL, ERR_COEF_MODE);
		format.Add("ladderIndex", NDTInteger, NCTSmallSigned, DEFVALUE(int, -1), DOC_MSG("Ladder ID"), ET_NOT_EQUAL, ERR_COEF_VALUE_MAJOR);
		format.Add("ladderPosition", NDTFloat, NCTFloat0To1, DEFVALUE(float, 0), DOC_MSG("Position on ladder"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);

		break;
	case NMCUpdatePosition:
		base::CreateFormat(cls, format);
		format.Add("manUpdPos", NDTRawData, NCTNone, DEFVALUERAWDATA, DOC_MSG("Encoded position"), ET_UPD_MAN_POS, 1);
		format.Add("move", NDTString, NCTStringMove, DEFVALUE(RString, "Stand"), DOC_MSG("Current animation"), ET_NOT_EQUAL, ERR_COEF_MODE);
		break;
	default:
		base::CreateFormat(cls, format);
		break;
	}
	return format;
}

TMError Man::TransferMsg(NetworkMessageContext &ctx)
{
	switch (ctx.GetClass())
	{
	case NMCUpdateGeneric:
		TMCHECK(base::TransferMsg(ctx))
		{
			Assert(dynamic_cast<const IndicesUpdateMan *>(ctx.GetIndices()))
			const IndicesUpdateMan *indices = static_cast<const IndicesUpdateMan *>(ctx.GetIndices());

			ITRANSF(hideBodyWanted)
			ITRANSF(tired)

			//ITRANSF(rndSpeed)
			ITRANSF(walkSpeedWanted)
			//ITRANSF(sideSpeedWanted)
			ITRANSF_ENUM(unitPos)
#if LOG_FLAG_CHANGES
			if (!ctx.IsSending())
			{
				EntityAI *veh = _flagCarrier;
				ITRANSF_REF(flagCarrier);
				if (_flagCarrier != veh) RptF
				(
					"Flags: Remote %s: set flag carrier to %s",
					(const char *)GetDebugName(),
					_flagCarrier ? (const char *)_flagCarrier->GetDebugName() : "NULL"
				);
			}
			else ITRANSF_REF(flagCarrier);
#else
			ITRANSF_REF(flagCarrier)
#endif
			ITRANSF(nvg)
			ITRANSF_REF(ladderBuilding)
			ITRANSF(ladderIndex)
			ITRANSF(ladderPosition)
		}
		break;
	case NMCUpdatePosition:
		TMCHECK(base::TransferMsg(ctx))
		{
			Assert(dynamic_cast<const IndicesUpdatePositionMan *>(ctx.GetIndices()))
			const IndicesUpdatePositionMan *indices = static_cast<const IndicesUpdatePositionMan *>(ctx.GetIndices());

			if (ctx.IsSending())
			{
				NetworkUpdManPos pos;
				pos.gunXRotWantedC = EncodeRot8b(_gunXRotWanted);
				pos.gunYRotWantedC = EncodeRot8b(_gunYRotWanted);
				pos.headXRotWantedC = EncodeRot8b(_headXRotWanted);
				pos.headYRotWantedC = EncodeRot8b(_headYRotWanted);
				TMCHECK(ctx.IdxSendRaw(indices->manUpdPos, &pos, sizeof(pos)));
			}
			else
			{
				void *data;
				int size;
				TMCHECK(ctx.IdxGetRaw(indices->manUpdPos, data, size));
				if (size == sizeof(NetworkUpdManPos))
				{
					// get access to data
					const NetworkUpdManPos &pos = *(NetworkUpdManPos *)data;
					_gunXRotWanted = DecodeRot8b(pos.gunXRotWantedC);
					_gunYRotWanted = DecodeRot8b(pos.gunYRotWantedC);
					_headXRotWanted = DecodeRot8b(pos.headXRotWantedC);
					_headYRotWanted = DecodeRot8b(pos.headYRotWantedC);
				}
				else
				{
					Fail("Bad size of NetworkUpdManPos field");
				}
			}
			//ITRANSF(manUpdPos)
			/*
			ITRANSF(gunXRotWanted)
			ITRANSF(gunYRotWanted)
			ITRANSF(headXRotWanted)
			ITRANSF(headYRotWanted)
			*/
			
			if (ctx.IsSending())
			{
				int n = _queueMove.Size();
				MoveId id = ( n == 0 ? _primaryMove.id : _queueMove[n - 1].id );
				RString name = id<0 ? "" : Type()->GetMoveName(id);
				TMCHECK(ctx.IdxTransfer(indices->move, name))
			}
			else
			{
				if (IsInLandscape())
				{
					// avoid updates of animations if inside vehicle
					// old message (from time when soldier is outside) can be processed
					RString name;
					TMCHECK(ctx.IdxTransfer(indices->move, name))
					if (name.GetLength() > 0)
						ChangeMoveQueue(MotionPathItem(Type()->GetMoveId(name)));

					RecalcPositions(*this);
				}
				else
				{
					// TODO: check for parent

					_aimingPositionWorld = VZero;
					_cameraPositionWorld = VZero;
				}
			}
		}
		break;
	default:
		return base::TransferMsg(ctx);
	}
	#if 0 //_ENABLE_CHEATS
		if (this==GWorld->CameraOn() && !QIsManual())
		{
			char buffer[1024];
			sprintf
			(
				buffer,
				"%s: %s update class %d, time sent %.3f",
				ctx.IsSending() ? "Sent" : "Received",
				(const char *)GetDebugName(),
				ctx.GetClass(),ctx.GetMsgTime().toFloat()
			);
			GlobalShowMessage(500,buffer);
			LogF(buffer);
		}
	#endif
	return TMOK;
}

float Man::CalculateError(NetworkMessageContext &ctx)
{
	float error = 0;
	switch (ctx.GetClass())
	{
	case NMCUpdateGeneric:
		{
			error += base::CalculateError(ctx);

			Assert(dynamic_cast<const IndicesUpdateMan *>(ctx.GetIndices()))
			const IndicesUpdateMan *indices = static_cast<const IndicesUpdateMan *>(ctx.GetIndices());

			ICALCERR_NEQREF(EntityAI, flagCarrier, ERR_COEF_MODE)
			ICALCERR_NEQREF(EntityAI, ladderBuilding, ERR_COEF_MODE)
			ICALCERRE_NEQ(int,ladderIndex, _ladderIndex,ERR_COEF_VALUE_MAJOR);
			ICALCERRE_NEQ(float,ladderPosition, _ladderPosition,ERR_COEF_VALUE_MAJOR);
			// TODO: implementation
		}
		break;
	case NMCUpdatePosition:
		{
			error += base::CalculateError(ctx);

			Assert(dynamic_cast<const IndicesUpdatePositionMan *>(ctx.GetIndices()))
			const IndicesUpdatePositionMan *indices = static_cast<const IndicesUpdatePositionMan *>(ctx.GetIndices());

			int n = _queueMove.Size();
			MoveId id = ( n == 0 ? _primaryMove.id : _queueMove[n - 1].id );
			RString name = id<0 ? "" : Type()->GetMoveName(id);
			//ICALCERRE_NEQSTR(move, name, ERR_COEF_MODE)
			ICALCERRE_NEQSTR(move, name, ERR_COEF_STRUCTURE)
			
			{
				AutoArray<char> temp;
				if
				(
					ctx.IdxTransfer(indices->manUpdPos, temp) == TMOK &&
					temp.Size()==sizeof(NetworkUpdManPos)
				)
				{
					NetworkUpdManPos &d = *(NetworkUpdManPos *)temp.Data();
					error += fabs(DecodeRot8b(d.headXRotWantedC) - _headXRotWanted) *ERR_COEF_VALUE_MINOR;
					error += fabs(DecodeRot8b(d.headYRotWantedC) - _headYRotWanted) *ERR_COEF_VALUE_MINOR;
					error += fabs(DecodeRot8b(d.gunXRotWantedC) - _gunXRotWanted) *ERR_COEF_VALUE_MINOR;
					error += fabs(DecodeRot8b(d.gunYRotWantedC) - _gunYRotWanted) *ERR_COEF_VALUE_MINOR;
				}
			}
			/*
			ICALCERRE_ABSDIF(float,headXRotWanted, _headXRotWanted,ERR_COEF_VALUE_MINOR);
			ICALCERRE_ABSDIF(float,headYRotWanted, _headYRotWanted,ERR_COEF_VALUE_MINOR);
			ICALCERRE_ABSDIF(float,gunXRotWanted, _gunXRotWanted,ERR_COEF_VALUE_MINOR);
			ICALCERRE_ABSDIF(float,gunYRotWanted, _gunYRotWanted,ERR_COEF_VALUE_MINOR);
			*/
		}
		break;
	default:
		error += base::CalculateError(ctx);
		break;
	}
	return error;
}

void Man::SetFlagCarrier(EntityAI *veh)
{
	_flagCarrier = veh;

#if LOG_FLAG_CHANGES
	RptF
	(
		"Flags: Local %s: set flag carrier to %s",
		(const char *)GetDebugName(),
		veh ? (const char *)veh->GetDebugName() : "NULL"
	);
#endif
}

// OnFinished actions
// !!! reset _externalMove, _onFinishExternal, _onFinishPrimary

void Man::ProcessGetIn()
{
	Fail("obsolete");
}

MovesTypeBank MovesTypes;


template Link<MovesType>;
template Link<BlendAnimType>;

template Ref<NetworkObject>;
