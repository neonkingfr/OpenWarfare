// AI - implementation of field locking

#include "wpch.hpp"
#include "aiTypes.hpp"
#include "lockCache.hpp"
#include "operMap.hpp"
#include "landscape.hpp"

#include "global.hpp"

#define SOLDIER_LOCK_MASK	0x0f
#define VEHICLE_LOCK_MASK	0xf0

/*!
\patch_internal 1.45 Date 2/20/2002 by Ondra
- New: LandRange dependecies isolated - preparation of variable LandRange.
New interfaces iintroduced: IAIPathPlanner, ILockCache, IOperCache
*/


DEFINE_FAST_ALLOCATOR(LockField)

LockField::LockField(int x, int z)
{
	Assert(InRange(z, x));
	memset(_locks, 0, sizeof(_locks));
	_next = _prev = NULL;
	_x = (WORD)x;
	_z = (WORD)z;
	_lock = 1;
}

LockField::~LockField()
{
	if (_prev)
		_prev->_next = _next;
	if (_next)
		_next->_prev = _prev;
}

bool LockField::IsLocked(int x, int z, bool soldier)
{
	Assert(x >= 0 && x < OperItemRange && z >= 0 && z < OperItemRange);
	return
		soldier ?
		(_locks[z][x] & VEHICLE_LOCK_MASK) > 0 :	// soldier - soldier lock disabled
		_locks[z][x] > 0;
}

void LockField::Lock(int x, int z, bool soldier, bool lock)
{
	Assert(x >= 0 && x < OperItemRange && z >= 0 && z < OperItemRange);
	if (soldier)
	{
		if (lock)
		{
			if ((_locks[z][x] & SOLDIER_LOCK_MASK) < 0x0f)
				_locks[z][x]++;
		}
		else
		{
			if ((_locks[z][x] & SOLDIER_LOCK_MASK) > 0)
				_locks[z][x]--;
		}
	}
	else
	{
		if (lock)
		{
			if ((_locks[z][x] & VEHICLE_LOCK_MASK) < 0xf0)
				_locks[z][x]+=0x10;
		}
		else
		{
			if ((_locks[z][x] & VEHICLE_LOCK_MASK) > 0)
				_locks[z][x]-=0x10;
		}
	}
}

//#define FieldsRange		(LandRange / BigFieldSize)

// note: class definition is in cpp file, as interface ILockCache
// is used in other sources

TypeIsSimple(LockField*)
TypeIsSimple(OperField*)

class LockCache: public ILockCache
{
private:
	Array2D<LockField*> _lockFields;
	int _searchID;

public:
	LockCache(Landscape *land){_searchID = 0; Init(land);};
	~LockCache() {Destroy();}

	int SearchID() {return ++_searchID;}

	bool IsLocked(int x, int z, bool soldier);
	LockField* GetLockField(int x, int z); 
	void ReleaseLockField(int x, int z);
	bool IsEmpty() const;

	LockField* FindLockField(int x, int z); // use for diagnostics

private:
	void Init(Landscape *land);
	void Destroy();
};

// note: class definition is in cpp file, as interface ILockCache
// is used in other sources

class OperCache: public IOperCache, public MemoryFreeOnDemandHelper
{
private:
	CLRefList<OperField> _operFields;
	int _count; // current count
	// fast operField searching
	Array2D<OperField *> _index; // 1/4 MB
	// consider: operCache access well encapsulated
	// we may work with pointers instead of links

public:
	OperField* GetOperField(int x, int z, int mask);
	void RemoveField(OperField*fld);
	void RemoveField(int x, int z);
	Ref<OperField> RemoveOld(float limit, int untilSize); // return some removed entry for recycling

	OperCache(Landscape *land);
	~OperCache();

	// implementation of MemoryFreeOnDemandHelper abstract interface
	virtual size_t FreeOneItem();
	virtual float Priority();

	int NFields() const {return _operFields.Size();}
};

ILockCache *CreateLockCache(Landscape *land){return new LockCache(land);}
IOperCache *CreateOperCache(Landscape *land){return new OperCache(land);}


bool LockCache::IsLocked(int x, int z, bool soldier)
{
	if (x < 0 || x >= OperItemRange * LandRange || z < 0 || z >= OperItemRange * LandRange)
		return false;

	int x1 = x / OperItemRange;
	int z1 = z / OperItemRange;
	int x0 = x1 / BigFieldSize;
	int z0 = z1 / BigFieldSize;
	LockField *fld = NULL;
	LockField *p = _lockFields(x0,z0);
	while (p)
	{
		if ((p->_x == x1) && (p->_z == z1))
		{
			fld = p;
			break;
		}
		p = p->_next;
	}

	if (!fld)
		return false;

	return fld->IsLocked(x - x1 * OperItemRange, z - z1 * OperItemRange, soldier);
}

bool LockCache::IsEmpty() const
{
	bool empty = true;
	const int rngX = _lockFields.GetXRange();
	const int rngZ = _lockFields.GetYRange();
	for (int i=0; i<rngZ; i++) for (int j=0; j<rngX; j++)
	{
		LockField *p = _lockFields(j,i);
		if (p)
		{
			empty = false;
			LogF("Lock field %d,%d not empty",i,j);
		}
	}
	return empty;
}

LockField* LockCache::GetLockField(int x, int z) 
{
	if (!InRange(z, x))
		return NULL;

	int x0 = x / BigFieldSize;
	int z0 = z / BigFieldSize;
	LockField *fld = NULL;
	LockField *p = _lockFields(x0,z0);
	while (p)
	{
		if ((p->_x == x) && (p->_z == z))
		{
			fld = p;
			break;
		}
		p = p->_next;
	}

	if (fld)
	{
		fld->_lock++;
		return fld;
	}

	fld = new LockField(x, z);
	fld->_next = _lockFields(x0,z0);
	_lockFields(x0,z0) = fld;
	if (fld->_next)
		fld->_next->_prev = fld;

	return fld;
}

void LockCache::ReleaseLockField(int x, int z)
{
	if (!InRange(z, x))
		return;

	int x0 = x / BigFieldSize;
	int z0 = z / BigFieldSize;
	LockField *fld = NULL;
	LockField *p = _lockFields(x0,z0);
	while (p)
	{
		if ((p->_x == x) && (p->_z == z))
		{
			fld = p;
			break;
		}
		p = p->_next;
	}

	if (fld == NULL)
	{
		Fail("Cannot unlock field that was not locked.");
		return;
	}

	if (--fld->_lock <=0)
	{
		if (fld == _lockFields(x0,z0))
			_lockFields(x0,z0) = fld->_next;
		delete fld;
	}
}

LockField* LockCache::FindLockField(int x, int z)
{
	if (!InRange(z, x))
		return NULL;

	int x0 = x / BigFieldSize;
	int z0 = z / BigFieldSize;
	LockField *p = _lockFields(x0,z0);
	while (p)
	{
		if ((p->_x == x) && (p->_z == z))
		{
			return p;
		}
		p = p->_next;
	}

	return NULL;
}

void LockCache::Init(Landscape *land)
{
	int fieldsRange = land->GetLandRange()/BigFieldSize;

	_lockFields.Dim(fieldsRange,fieldsRange);
	for (int i=0; i<fieldsRange; i++)
		for (int j=0; j<fieldsRange; j++)
			_lockFields(j,i) = NULL;
}

void LockCache::Destroy()
{
	const int rngX = _lockFields.GetXRange();
	const int rngZ = _lockFields.GetYRange();
	LockField *p, *p2;
	for (int i=0; i<rngZ; i++)
		for (int j=0; j<rngX; j++)
		{
			p = _lockFields(j,i);
			#if _ENABLE_REPORT
			if (p)
			{
				LogF("Lock field %d,%d not empty",i,j);
			}
			#endif
			while (p)
			{
				p2 = p;
				p = p->_next;
				delete p2;
			}
			_lockFields(j,i) = NULL;
		}
}

// TODO: make these values scalable
// static (based on system memory)
// dynamic (based on free system memory)

const int OperCacheMinSize = 64;
const int OperCacheNormalSize = 256;
const int OperCacheMaxSize = 1024;

OperField* OperCache::GetOperField(int x, int z, int mask)
{
	if (!InRange(z, x))
		return NULL;
	OperField *fld;
	fld = _index(x,z);
	if (fld)
	{
		Assert( fld->_x == x);
		Assert( fld->_z == z);
		Ref<OperField> temp = fld;
		_operFields.Delete(fld); // remove from current location
		_operFields.Insert(fld); // insert as first
		fld->_lastUsed = Glob.time;
		return fld;
	}
	
	// _data is sorted by time when it was used
	Ref<OperField> entry;
	if (_count >= OperCacheMinSize)
	{
		// we have more entries than we would like - remove only very old entries
		entry = RemoveOld(60,OperCacheMinSize);
		if (_count >= OperCacheNormalSize)
		{
			// we have to much entries - remove more recent entries
			entry = RemoveOld(5,OperCacheNormalSize);
			if (_count >= OperCacheMaxSize)
			{
				// we must release something - memory usage critical
				// we have to remove last field
				// try to reuse entry if possible
				// remove last entry
				entry = _operFields.Last();
				_index(entry->_x,entry->_z)=NULL;
				_operFields.Delete(entry);
				_count--;
			}
		}
	}
	if (!entry)
	{
		entry = new OperField(x, z, mask);
	}
	else
	{
		entry->Init(x, z, mask);
	}

	// create a new entry
	_operFields.Insert(entry);
	_count++;
	_index(x,z) = entry;
	entry->_lastUsed = Glob.time;
	return entry;
}

void OperCache::RemoveField(OperField*fld)
{
	_index(fld->_x,fld->_z)=NULL;
	_operFields.Delete(fld);
	_count--;
}

void OperCache::RemoveField(int x, int z)
{
	if (!InRange(z, x)) return;
	OperField *fld = _index(x,z);
	if (!fld) return;
	RemoveField(fld);
}

Ref<OperField> OperCache::RemoveOld(float limit, int untilSize)
{
	Ref<OperField> ret;
	// last used fields are at the end of the list
	for
	(
		OperField *fld =_operFields.Last(), *prv;
		fld;
		fld = prv
	)
	{
		prv = _operFields.Prev(fld);
		// if we find some too new entry, we may be sure all other will be newer
		if (fld->_lastUsed >= Glob.time - limit) break;
		if (!ret) ret = fld; // recycle first entry removed
		RemoveField(fld);
		// check if we have reached the limit
		if (_count<=untilSize ) return ret;
	}
	return ret;
}

OperCache::OperCache(Landscape *land)
{
	_count = 0; // current count
	const int landRange = land->GetLandRange();
	_index.Dim(landRange,landRange);
	for( int z=0; z<landRange; z++ ) for( int x=0; x<landRange; x++ )
	{
		_index(x,z)=NULL;
	}
	RegisterMemoryFreeOnDemand(this);
}

OperCache::~OperCache()
{
}

// implementation of MemoryFreeOnDemandHelper abstract interface

const int OperFieldMemSize = sizeof(OperField);

size_t OperCache::FreeOneItem()
{
	OperField *fld =_operFields.Last();
	if (!fld) return 0;
	RemoveField(fld);
	return OperFieldMemSize;
}

float OperCache::Priority()
{
	// estimated time to create OperField (CPU cycles)
	const float itemTime = 100000;
	// estimated time per byte
	return itemTime/OperFieldMemSize;
}

