#ifdef _MSC_VER
#pragma once
#endif

#ifndef _NETWORK_IMPL_HPP
#define _NETWORK_IMPL_HPP


#include "network.hpp"
#include <Es/Common/win.h>
#include "MemHeap.hpp"

#include "speaker.hpp"
#include "ArcadeWaypoint.hpp"

#include "paramFileExt.hpp"

/*!
\file
Internal interface file for multiplayer game (included only from inside of multiplayer files)
*/

//! Magic for distinguish applications
#if _VBS1

#define MAGIC_APP		0x11111111

#else

#define MAGIC_APP		0x00000000

#endif


#define LOCAL_PLAYER						1
#define TO_SERVER								1

#define MAX_PLAYERS	50

bool IsUseSockets();

extern const int DiagLevel;
bool DiagFilter(NetworkMessageType type);
extern const char *NetworkMessageTypeNames[];

//! Uninitialized camera position
const Vector3 InvalidCamPos(-FLT_MAX, -FLT_MAX, -FLT_MAX);

///////////////////////////////////////////////////////////////////////////////
// Network Messages

//! Encapsulation for block of data transferred over network.
/*!
Implement methods nedded for transfer of NetworkMessage into raw data.
\todo Implement compression methods.
\todo Work with bits, no bytes.
*/
class NetworkMessageRaw
{
protected:
	//! raw data itself
	AutoArray<char> _buffer;
	//! read / write position
	int _pos;

	//! external raw data
	char *_externalBuffer;
	//! size of external raw data
	int _externalBufferSize;
public:
	//! Constructor
	NetworkMessageRaw()
	{_pos = 0; _buffer.Realloc(1024); _externalBuffer = NULL; _externalBufferSize = 0;}
	//! Constructor
	/*!
		\param buffer external data
		\param size size of external data
	*/
	NetworkMessageRaw(char *buffer, int size)
	{_pos = 0; _externalBuffer = buffer; _externalBufferSize = size;}

	//! Return total size of valid data
	int GetSize() const {return _externalBuffer ? _externalBufferSize : _buffer.Size();}
	//! Set new size of internal buffer, sets position to 0
	void SetSize(int size) {_buffer.Resize(size); _pos = 0;}
	//! Get (read only) pointer to valid data
	const char *GetData() const {return _externalBuffer ? _externalBuffer : _buffer.Data();}
	//! Get (read and write) pointer to internal buffer
	char *SetData() {return _buffer.Data();}
	//! Return current read / write position in message
	int GetPos() const {return _pos;}
	
	//@{
	//! Write single value to buffer
	/*!
	\param value value to write
	\param compression compression algorithm
	*/
	void Put(bool value, NetworkCompressionType compression);
	void Put(char value, NetworkCompressionType compression);
	void Put(int value, NetworkCompressionType compression);
	void Put(float value, NetworkCompressionType compression);
	void Put(RString value, NetworkCompressionType compression);
	void Put(Time value, NetworkCompressionType compression);
	void Put(Vector3Par value, NetworkCompressionType compression);
	void Put(Matrix3Par value, NetworkCompressionType compression);
	void Put(NetworkId &value, NetworkCompressionType compression);
	//@}

	//@{
	//! Read single value from buffer
	/*!
	\param value reference to variable read into
	\param compression compression algorithm
	\return true if value was read, otherwise false
	*/
	bool GetNoCompression(RString &value);

	bool Get(bool &value, NetworkCompressionType compression);
	bool Get(char &value, NetworkCompressionType compression);
	bool Get(int &value, NetworkCompressionType compression);
	bool Get(float &value, NetworkCompressionType compression);
	bool Get(RString &value, NetworkCompressionType compression);
	bool Get(Time &value, NetworkCompressionType compression);
	bool Get(Vector3 &value, NetworkCompressionType compression);
	bool Get(Matrix3 &value, NetworkCompressionType compression);
	bool Get(NetworkId &value, NetworkCompressionType compression);
	//@}

protected:
	//! Write raw data to buffer
	/*!
	\param buffer raw data
	\param size raw data size
	*/
	void Write(const void *buffer, int size)
	{
		int minSize = _pos + size;
		if (_buffer.Size() < minSize) _buffer.Resize(minSize);
		memcpy(_buffer.Data() + _pos, buffer, size);
		_pos += size;
	}
	//! Read raw data from buffer
	/*!
	\param buffer where read raw data
	\param size size of data to read
	\return true if data available in buffer, otherwise false
	*/
	bool Read(void *buffer, int size)
	{
		int minSize = _pos + size;
		if (_externalBuffer)
		{
			if (_externalBufferSize < minSize) return false;
			memcpy(buffer, _externalBuffer + _pos, size);
		}
		else
		{
			if (_buffer.Size() < minSize) return false;
			memcpy(buffer, _buffer.Data() + _pos, size);
		}
		_pos += size;
		return true;
	}
};
TypeIsMovable(NetworkMessageRaw)

//! Encapsulate NetworkData with run-time format
/*!
Used whenever some member of message can contain different data type.
- for example type of defValue in NetworkMessageFormatItem is given
by type of this item and is different for different instances of NetworkMessageFormatItem
*/
struct NetworkDataWithFormat : public NetworkData
{
	//! describe type of stored data
	NetworkMessageFormatItem format;

	//! Constructor
	/*!
	\param type data type
	\param compression compression algorithm
	\param d data itself
	*/
	NetworkDataWithFormat
	(
		NetworkDataType type, 
		NetworkCompressionType compression,
		const RefNetworkData &d
	);

	/*
	float CalculateError(NetworkMessageErrorType type, NetworkData *value2, NetworkMessageFormatItem &item, float dt);
	int CalculateSize(NetworkCompressionType compression);
	*/
};

class RefNetworkDataWithFormat: public RefNetworkData
{
	public:
	RefNetworkDataWithFormat(){}
	RefNetworkDataWithFormat
	(
		NetworkDataType type, 
		NetworkCompressionType compression,
		const RefNetworkData &d
	)
	{
		_data = new NetworkDataWithFormat(type,compression,d);
	}
	NetworkDataWithFormat *operator ->()
	{
		return static_cast<NetworkDataWithFormat *>(_data.GetRef());
	}
	const NetworkDataWithFormat *operator ->() const
	{
		return static_cast<const NetworkDataWithFormat *>(_data.GetRef());
	}

	float CalculateError(NetworkMessageErrorType type, const RefNetworkDataWithFormat &with, NetworkMessageFormatItem &item, float dt) const;
	int CalculateSize(NetworkCompressionType compression, const NetworkMessageFormatItem &item) const;
};

//! Info about player sent by server to player itself
/*!
This message is sent to player after DPN_MSGID_CREATE_PLAYER system message arrived to server.
Message contains info not known to player itself.
*/
struct PlayerMessage : public NetworkSimpleObject
{
	//! DirectPlay ID of player
	int player;
	//! name of player (some suffix can be added in case of duplicity of names)
	RString name;
//{ DEDICATED SERVER SUPPORT
	//! bot client on dedicated server
	bool server;

	//! Constructor
	PlayerMessage() {player = 0; server = false;}
	//! Constructor
	/*!
	\param p DirectPlay ID of player
	\param n name of player (some suffix can be added in case of duplicity of names)
	\param s bot client on dedicated server
	*/
	PlayerMessage(int p, RString n, bool s) {player = p; name = n; server = s;}
//}
	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTPlayer;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message sent by server whenever game state changes (see NetworkGameState)
/*!
Message can be sent also by client whenever player is ready (confirm something in some dialog).
*/
struct ChangeGameState : public NetworkSimpleObject
{
	//! new state or ready state
	NetworkGameState gameState;

	//! Constructor
	/*!
	\param state new state or ready state
	*/
	ChangeGameState(NetworkGameState state = NGSNone) {gameState = state;}
	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTGameState;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message sent to all players when some player logged out from game
struct LogoutMessage : public NetworkSimpleObject
{
	//! logged out player
	int dpnid;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTLogout;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

#include "messageFactory.hpp"

#define PUB_VAR_MSG(XX) \
	XX(RString, name, NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Variable name"), IdxTransfer) \
	XX(AutoArray<char>, value, NDTRawData, NCTNone, DEFVALUERAWDATA, DOC_MSG("Variable value"), IdxTransfer)

DECLARE_NET_MESSAGE(PublicVariable,PUB_VAR_MSG)

#define GROUP_SYNC_MSG(XX) \
	XX(OLink<AIGroup>, group, NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Synchronized group"), IdxTransferRef) \
	XX(int, synchronization, NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("Index of synchronization"), IdxTransfer) \
	XX(bool, active, NDTBool, NCTNone, DEFVALUE(bool, true), DOC_MSG("Active / inactive"), IdxTransfer)

DECLARE_NET_MESSAGE(GroupSynchronization,GROUP_SYNC_MSG)

#define DET_ACT_MSG(XX) \
	XX(OLink<Detector>, detector, NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Detector"), IdxTransferRef) \
	XX(bool, active, NDTBool, NCTNone, DEFVALUE(bool, true), DOC_MSG("Active / inactive"), IdxTransfer)

DECLARE_NET_MESSAGE(DetectorActivation,DET_ACT_MSG)

#define CREATE_UNIT_MSG(XX) \
	XX(OLink<AIGroup>, group, NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Group, where unit will be added"), IdxTransferRef) \
	XX(RString, type, NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Entity type"), IdxTransfer) \
	XX(Vector3, position, NDTVector, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Position"), IdxTransfer) \
	XX(RString, init, NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Initialization statement"), IdxTransfer) \
	XX(float, skill, NDTFloat, NCTNone, DEFVALUE(float, 0.5), DOC_MSG("Initial skill"), IdxTransfer) \
	XX(int, rank, NDTInteger, NCTNone, DEFVALUE(int, RankPrivate), DOC_MSG("Initial rank"), IdxTransfer)

DECLARE_NET_MESSAGE(AskForCreateUnit, CREATE_UNIT_MSG)

#define DELETE_VEHICLE_MSG(XX) \
	XX(OLink<Entity>, vehicle, NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Vehicle to destroy"), IdxTransferRef)

DECLARE_NET_MESSAGE(AskForDeleteVehicle, DELETE_VEHICLE_MSG)

#define UNIT_ANSWER_MSG(XX) \
	XX(OLink<AIUnit>, from, NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Sender unit"), IdxTransferRef) \
	XX(OLink<AISubgroup>, to, NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Receiving subgroup"), IdxTransferRef) \
	XX(int, answer, NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Answer"), IdxTransfer)

DECLARE_NET_MESSAGE(AskForReceiveUnitAnswer,UNIT_ANSWER_MSG)

#define GROUP_RESPAWN_MSG(XX) \
	XX(OLink<Person>, person, NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Killed person"), IdxTransferRef) \
	XX(OLink<EntityAI>, killer, NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Killer"), IdxTransferRef) \
	XX(OLink<AIGroup>, group, NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Group, where unit will respawn"), IdxTransferRef) \
	XX(int, from, NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("Client (player) ID of sender"), IdxTransfer)

DECLARE_NET_MESSAGE(AskForGroupRespawn,GROUP_RESPAWN_MSG)

#define COPY_UNIT_INFO_MSG(XX) \
	XX(OLink<Person>, from, NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Source unit"), IdxTransferRef) \
	XX(OLink<Person>, to, NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Destination unit"), IdxTransferRef)

DECLARE_NET_MESSAGE(CopyUnitInfo,COPY_UNIT_INFO_MSG)

#define GROUP_RESPAWN_DONE_MSG(XX) \
	XX(OLink<Person>, person, NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Killed person"), IdxTransferRef) \
	XX(OLink<EntityAI>, killer, NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Killer"), IdxTransferRef) \
	XX(OLink<Person>, respawn, NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Respawned person"), IdxTransferRef) \
	XX(int, to, NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("Client (player) ID of receiver"), IdxTransfer)

DECLARE_NET_MESSAGE(GroupRespawnDone,GROUP_RESPAWN_DONE_MSG)

#define MISSION_PARAMS_MSG(XX) \
	XX(float, param1, NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Mission parameter"), IdxTransfer) \
	XX(float, param2, NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Mission parameter"), IdxTransfer)

DECLARE_NET_MESSAGE(MissionParams, MISSION_PARAMS_MSG)

#define ACTIVATE_MINE_MSG(XX) \
	XX(OLink<Mine>, mine, NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Activated mine"), IdxTransferRef) \
	XX(bool, activate, NDTBool, NCTNone, DEFVALUE(bool, true), DOC_MSG("Activate / disactivate"), IdxTransfer)

DECLARE_NET_MESSAGE(AskForActivateMine,ACTIVATE_MINE_MSG)

#define INFLAME_FIRE_MSG(XX) \
	XX(OLink<Fireplace>, fireplace, NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Fireplace"), IdxTransferRef) \
	XX(bool, fire, NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Light / put out fire"), IdxTransfer)

DECLARE_NET_MESSAGE(AskForInflameFire,INFLAME_FIRE_MSG)

#define ANIMATION_PHASE_MSG(XX) \
	XX(OLink<Entity>, vehicle, NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Animated vehicle"), IdxTransferRef) \
	XX(RString, animation, NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Animation ID"), IdxTransfer) \
	XX(float, phase, NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Animation state (phase)"), IdxTransfer)

DECLARE_NET_MESSAGE(AskForAnimationPhase,ANIMATION_PHASE_MSG)

#define VEHICLE_DAMAGED_MSG(XX) \
	XX(OLink<EntityAI>, damaged, NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Damaged entity"), IdxTransferRef) \
	XX(OLink<EntityAI>, killer, NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Entity, causing damage"), IdxTransferRef) \
	XX(float, damage, NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Amount of damage"), IdxTransfer) \
	XX(RString, ammo, NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Used ammunition type"), IdxTransfer)

DECLARE_NET_MESSAGE(VehicleDamaged, VEHICLE_DAMAGED_MSG)

#define INCOMING_MISSILE_MSG(XX) \
	XX(OLink<EntityAI>, target, NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Missile target"), IdxTransferRef) \
	XX(RString, ammo, NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Missile (ammunition) type"), IdxTransfer) \
	XX(OLink<EntityAI>, owner, NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Firing entity"), IdxTransferRef)

DECLARE_NET_MESSAGE(IncomingMissile, INCOMING_MISSILE_MSG)

//! Text chat message
struct ChatMessage : public NetworkSimpleObject
{
	//! chat channel
	int channel;
	//! sender unit
	AIUnit *sender;
	//! receiving units
	RefArray<NetworkObject> units;
	//! sender name
	RString name;
	//! chat text
	RString text;

	//! Constructor
	ChatMessage() {channel = 0; sender = NULL;}
	//! Constructor
	/*!
	\param ch chat channel
	\param n sender name
	\param txt chat text
	*/
	ChatMessage(int ch, RString n, RString txt)
	{channel = ch; sender = NULL; name = n; text = txt;}
	//! Constructor
	/*!
	\param ch chat channel
	\param se sender unit
	\param u receiving units
	\param n sender name
	\param txt chat text
	*/
	ChatMessage(int ch, AIUnit *se, RefArray<NetworkObject> &u, RString n, RString txt)
	{channel = ch; sender = se; units = u; name = n; text = txt;}
	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTChat;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Radio chat message (radio message sent over network)
struct RadioChatMessage : public NetworkSimpleObject
{
	//! chat channel
	int channel;
	//! sender unit
	AIUnit *sender;
	//! receiving units
	RefArray<NetworkObject> units;
	//! chat text
	RString text;
	//! radio sentence (array of words) to say
	RadioSentence sentence;

	//! Constructor
	RadioChatMessage() {channel = 0; sender = NULL;}
	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTRadioChat;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Text (and sound) radio chat message
struct RadioChatWaveMessage : public NetworkSimpleObject
{
	//! chat channel
	int channel;
	//! sender unit
	AIUnit *sender;
	//! receiving units
	RefArray<NetworkObject> units;
	//! sender name
	RString senderName;
	//! name of class from CfgRadio containing sound and title
	RString wave;

	//! Constructor
	RadioChatWaveMessage() {channel = 0; sender = NULL;}
	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTRadioChatWave;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Set Voice Over Net channel message
struct SetVoiceChannelMessage : public NetworkSimpleObject
{
	//! chat channel
	int channel;
	//! receiving units
	RefArray<NetworkObject> units;

	//! Constructor
	/*!
	\param ch chat channel
	*/
	SetVoiceChannelMessage(int ch) {channel = ch;}
	//! Constructor
	/*!
	\param ch chat channel
	\param u receiving units
	*/
	SetVoiceChannelMessage(int ch, RefArray<NetworkObject> &u)
	{channel = ch; units = u;}
	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTSetVoiceChannel;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Assign player speaking on Direct channel to sound source object
struct SetSpeakerMessage : public NetworkSimpleObject
{
	//! DirectPlay ID of speaking player
	int player;
	//! is player currently speaking
	bool on;
	//! sound source object
	NetworkId object;

	//! Constructor
	SetSpeakerMessage() {player = 0; on = false;}
	//! Constructor
	/*!
	\param pl DirectPlay ID of speaking player
	\param o is player currently speaking
	\param obj sound source object
	*/
	SetSpeakerMessage(int pl, bool o, NetworkId &obj) {player = pl; on = o; object = obj;}
	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTSetSpeaker;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Select person as player message
struct SelectPlayerMessage : public NetworkSimpleObject
{
	//! DirectX player id
	int player;
	//! player's person
	NetworkId person;
	//! player's position
	Vector3 position;
	//! play "ressurect" cutscene
	bool respawn;

	//! Constructor
	SelectPlayerMessage() {player = AI_PLAYER; position = Vector3(-FLT_MAX, -FLT_MAX, -FLT_MAX); respawn = false;}
	//! Constructor
	/*!
	\param pl DirectX player ID
	\param pe player's person
	\param pos player's position
	\param resp play "ressurect" cutscene
	*/
	SelectPlayerMessage(int pl, NetworkId pe, Vector3Par pos, bool resp) {player = pl; person = pe; position = pos, respawn = resp;}
	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTSelectPlayer;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message is sent when owner of some object changes
struct ChangeOwnerMessage : public NetworkSimpleObject
{
	//! object which owner changes
	NetworkId object;
	//! DirectX ID of new owner
	int owner;

	//! Constructor
	ChangeOwnerMessage() {owner = AI_PLAYER;}
	//! Constructor
	/*!
	\param obj object which owner changes
	\param ow DirectX ID of new owner
	*/
	ChangeOwnerMessage(NetworkId obj, int ow) {object = obj; owner = ow;}
	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTChangeOwner;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message sent to clients to play sound
struct PlaySoundMessage : public NetworkSimpleObject
{
	//! name of sound file
	RString name;
	//! position source position
	Vector3 position;
	//! speed source speed
	Vector3 speed;
	//! volume sound volume
	float volume;
	//! freq sound frequency
	float freq;
	//! unique id of client where sound originate
	int creator;
	//! unique id of sound on client where sound originate
	int soundId;
	
	//! Constructor
	PlaySoundMessage() {}
	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTPlaySound;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message sent to clients to change state of played sound
struct SoundStateMessage : public NetworkSimpleObject
{
	//! new state of sound
	SoundStateType state;
	//! unique id of client where sound originate
	int creator;
	//! unique id of sound on client where sound originate
	int soundId;

	//! Constructor
	SoundStateMessage() {}
	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTSoundState;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message announcing destroying of network object
struct DeleteObjectMessage : public NetworkSimpleObject
{
	//! object to destroy
	NetworkId object;

	//! Constructor
	DeleteObjectMessage() {}
	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTDeleteObject;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message announcing destroying of command
struct DeleteCommandMessage : public NetworkSimpleObject
{
	//! command to destroy
	NetworkId object;
	//! subgroup that owns command
	AISubgroup *subgrp;
	//! index of command in subgroup
	int index;

	//! Constructor
	DeleteCommandMessage() {subgrp = NULL; index = -1;}
	//! Constructor
	/*!
	\param s subgroup that owns command
	\param i index of command in subgroup
	\param obj command to destroy
	*/
	DeleteCommandMessage(AISubgroup *s, int i, NetworkId obj) {subgrp = s; index = i; object = obj;}
	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTDeleteCommand;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for ask object owner for damage of object
struct AskForDammageMessage : public NetworkSimpleObject
{
	//! damaged object
	Object *who;
	//! who is responsible for damage
	EntityAI *owner;
	//! position of damage
	Vector3 modelPos;
	//! amount of damage
	float val;
	//! range of damage
	float valRange;
	//! ammunition type
	RString ammo;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTAskForDammage;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for ask object owner for set of total damage of object
struct AskForSetDammageMessage : public NetworkSimpleObject
{
	//! damaged object
	Object *who;
	//! new value of total damage
	float dammage;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTAskForSetDammage;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for ask vehicle owner for get in person
struct AskForGetInMessage : public NetworkSimpleObject
{
	//! who is getting in
	Person *soldier;
	//! vehicle to get in
	Transport *vehicle;
	//! position in vehicle to get in
	GetInPosition position;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTAskForGetIn;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for ask vehicle owner for get out person
struct AskForGetOutMessage : public NetworkSimpleObject
{
	//! who is getting out
	Person *soldier;
	//! vehicle to get out
	Transport *vehicle;
	//! parachute or plain ejection
	bool parachute;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTAskForGetOut;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for ask vehicle owner for change person position
struct AskForChangePositionMessage : public NetworkSimpleObject
{
	//! who is changing position
	Person *soldier;
	//! vehicle where position is changed
	Transport *vehicle;
	//! performed action
	UIActionType type;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTAskForChangePosition;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for ask vehicle owner for aim weapon
struct AskForAimWeaponMessage : public NetworkSimpleObject
{
	//! vehicle which weapon is aiming
	EntityAI *vehicle;
	//! aiming weapon index
	int weapon;
	//! direction to aim
	Vector3 dir;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTAskForAimWeapon;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for ask vehicle owner for aim observer turret
struct AskForAimObserverMessage : public NetworkSimpleObject
{
	//! vehicle which turret is aiming
	EntityAI *vehicle;
	//! direction to aim
	Vector3 dir;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTAskForAimObserver;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for ask vehicle owner for select weapon
struct AskForSelectWeaponMessage : public NetworkSimpleObject
{
	//! vehicle which weapon is selecting
	EntityAI *vehicle;
	//! selected weapon index
	int weapon;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTAskForSelectWeapon;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for ask vehicle owner for change ammo state
struct AskForAmmoMessage : public NetworkSimpleObject
{
	//! vehicle which ammo is changing
	EntityAI *vehicle;
	//! weapon index
	int weapon;
	//! amount of ammo to decrease
	int burst;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTAskForAmmo;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for ask vehicle owner for add impulse
struct AskForAddImpulseMessage : public NetworkSimpleObject
{
	//! vehicle impulse is applied to
	Vehicle *vehicle;
	//! applied force
	Vector3 force;
	//! applied torque
	Vector3 torque;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTAskForAddImpulse;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for ask object owner for move object
struct AskForMoveVectorMessage : public NetworkSimpleObject
{
	//! moving object
	Object *vehicle;
	//! new position
	Vector3 pos;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTAskForMoveVector;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for ask object owner for move object
struct AskForMoveMatrixMessage : public NetworkSimpleObject
{
	//! moving object
	Object *vehicle;
	//! new position
	Vector3 pos;
	//! new orientation
	Matrix3 orient;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTAskForMoveMatrix;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for ask group owner for join other group
struct AskForJoinGroupMessage : public NetworkSimpleObject
{
	//! joined group
	AIGroup *join;
	//! joining group
	AIGroup *group;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTAskForJoinGroup;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for ask group owner for join other units
struct AskForJoinUnitsMessage : public NetworkSimpleObject
{
	//! joined group
	AIGroup *join;
	//! joining units
	OLinkArray<AIUnit> units;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTAskForJoinUnits;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for ask person owner for hide body
struct AskForHideBodyMessage : public NetworkSimpleObject
{
	//! body to hide
	Person *vehicle;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTAskForHideBody;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for transfer explosion effects (explosion, smoke, etc.) to other clients
struct ExplosionDammageEffectsMessage : public NetworkSimpleObject
{
	//! shot owner (who is responsible for explosion)
	EntityAI *owner;
	//! shot
	Shot *shot;
	//! hitted object
	Object *directHit;
	//! explosion position
	Vector3 pos;
	//! explosion direction
	Vector3 dir;
	//! ammunition
	RString type;
	//! some enemy was damaged
	bool enemyDammage;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTExplosionDammageEffects;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for transfer fire effects (sound, fire, smoke, recoil effect, etc.) to other clients
struct FireWeaponMessage : public NetworkSimpleObject
{
	//! firing vehicle
	EntityAI *vehicle;
	//! aimed target
	EntityAI *target;
	//! firing weapon index
	int weapon;
	//! fired magazine id
	int magazineCreator;
	//! fired magazine id
	int magazineId;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTFireWeapon;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for ask vehicle to add weapon into cargo
struct AddWeaponCargoMessage : public NetworkSimpleObject
{
	//! asked vehicle
	VehicleSupply *vehicle;
	//! name of weapon type to add
	RString weapon;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTAddWeaponCargo;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for ask vehicle to remove weapon from cargo
struct RemoveWeaponCargoMessage : public NetworkSimpleObject
{
	//! asked vehicle
	VehicleSupply *vehicle;
	//! name of weapon type to remove
	RString weapon;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTRemoveWeaponCargo;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for ask vehicle to add magazine into cargo
struct AddMagazineCargoMessage : public NetworkSimpleObject
{
	//! asked vehicle
	VehicleSupply *vehicle;
	//! magazine to add
	Ref<Magazine> magazine;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTAddMagazineCargo;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for ask vehicle to remove magazine from cargo
struct RemoveMagazineCargoMessage : public NetworkSimpleObject
{
	//! asked vehicle
	VehicleSupply *vehicle;
	//! id of magazine to remove
	int creator;
	//! id of magazine to remove
	int id;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTRemoveMagazineCargo;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message is sent to update of weapons to vehicle owner
struct UpdateWeaponsMessage : public NetworkSimpleObject
{
	//! vehicle to update
	EntityAI *vehicle;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTUpdateWeapons;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for transfer message who is responsible to destroy vehicle to other clients
struct VehicleDestroyedMessage : public NetworkSimpleObject
{
	//! destroyed vehicle
	EntityAI *killed;
	//! who is responsible for destroying
	EntityAI *killer;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTVehicleDestroyed;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for transfer info about (user made) marker was deleted to other clients
struct MarkerDeleteMessage : public NetworkSimpleObject
{
	//! name of marker
	RString name;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTMarkerDelete;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for transfer info about (user made) marker creation to other clients
struct MarkerCreateMessage : public NetworkSimpleObject
{
	//! chat channel (who will see the marker)
	int channel;
	//! sender unit
	AIUnit *sender;
	//! receiving units
	RefArray<NetworkObject> units;
	//! marker itself
	ArcadeMarkerInfo marker;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTMarkerCreate;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for transfer file to other clients
struct TransferFileMessage : public NetworkSimpleObject
{
	//! destination file path
	RString path;
	//! segment of file content
	AutoArray<char> data;

	//! total size of file
	int totSize;
	//! offset of segment in file
	int offset;

	//! total count of segments
	int totSegments;
	//! index of segment
	int curSegment;

/*
	//@{
	//! date and time of last file modification
	int timeL;
	int timeH;
	//@}
*/

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTTransferFile;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for transfer mission pbo file to other clients
struct TransferMissionFileMessage : public TransferFileMessage
{
	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTTransferMissionFile;}
};

//! Message for transfer file to server
struct TransferFileToServerMessage : public TransferFileMessage
{
	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTTransferFileToServer;}
};

//! Message sent to server to answer if mission pbo file on client is actual (valid)
struct AskMissionFileMessage : public NetworkSimpleObject
{
	//! if mission file is actual (valid)
	bool valid;

	//! Constructor
	AskMissionFileMessage() {valid = false;}
	//! Constructor
	/*!
	\param v if mission file is actual (valid)
	*/
	AskMissionFileMessage(bool v) {valid = v;}

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTAskMissionFile;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for ask flag (carrier) owner for assign new owner
struct SetFlagOwnerMessage : public NetworkSimpleObject
{
	//! new owner
	Person *owner;
	//! flag carrier
	EntityAI *carrier;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTSetFlagOwner;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for ask client owns flag owner for change of flag ownership
struct SetFlagCarrierMessage : public SetFlagOwnerMessage
{
	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTSetFlagCarrier;}
};

//! Message for ask person owner for show target
struct ShowTargetMessage : public NetworkSimpleObject
{
	//! player person
	Person *vehicle;
	//! target to show
	TargetType *target;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTShowTarget;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for ask person owner for show group direction
struct ShowGroupDirMessage : public NetworkSimpleObject
{
	//! player person
	Person *vehicle;
	//! direction to show
	Vector3 dir;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTShowGroupDir;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Single message in agregated message
struct NetworkMessageQueueItem
{
	//! message type
	NetworkMessageType type;
	//! message itself
	Ref<NetworkMessage> msg;
};
TypeIsMovableZeroed(NetworkMessageQueueItem)

//! Agregated message
/*!
Message joins several small messages to decrease framework overhead.
*/
class NetworkMessageQueue : public NetworkSimpleObject, public AutoArray<NetworkMessageQueueItem>
{
public:
	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTMessages;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Encapsulates info needed for messages sent directly (inside process) instead through DirectPlay
struct NetworkLocalMessageInfo
{
	//! DirectPlay ID of sender
	int from;
	//! message type
	NetworkMessageType type;
	//! message itself
	Ref<NetworkMessage> msg;
};
TypeIsMovableZeroed(NetworkLocalMessageInfo)

///////////////////////////////////////////////////////////////////////////////
// Network Components

class NetworkManager;

//! Info about file currently receiving
struct ReceivingFile
{
	//! name of receiving file
	RString fileName;
	//! info about which segments already received
	AutoArray<bool> fileSegments;
	//! content of receiving file
	AutoArray<char> fileData;
	//! total amount of bytes already received (used for statistics)
	int received;
};
TypeIsMovable(ReceivingFile)

#if _ENABLE_CHEATS
//! Statistic info about messages of given type
/*!
\patch_internal 1.08 Date 07/26/2001 by Jirka
- Improved: network diagnostics
*/
struct NetworkStatisticsItem
{
	//! message type
	int message;
	//! count of sent messages
	int msgSent;
	//! size of sent messages
	int sizeSent;
	//! count of received messages
	int msgReceived;
	//! size of received messages
	int sizeReceived;

	//! Constructor
	NetworkStatisticsItem()
	{
		message = 0;
		msgSent = 0;
		sizeSent = 0;
		msgReceived = 0;
		sizeReceived = 0;
	};
};
TypeIsMovableZeroed(NetworkStatisticsItem)
#endif

#if _ENABLE_CHEATS || _ENABLE_DEDICATED_SERVER
//! Statistic info about messages physically send / received by players
struct NetworkRawStatisticsItem
{
	//! player ID
	int player;
	//! count of sent messages
	int msgSent;
	//! size of sent messages
	int sizeSent;
	//! count of received messages
	int msgReceived;
	//! size of received messages
	int sizeReceived;

	//! Constructor
	NetworkRawStatisticsItem()
	{
		player = 0;
		msgSent = 0;
		sizeSent = 0;
		msgReceived = 0;
		sizeReceived = 0;
	};
};
TypeIsMovableZeroed(NetworkRawStatisticsItem)
#endif

//! Basic network component (client or server)
class NetworkComponent : public INetworkComponent
{
protected:
	//! DirectPlay ID of attached player
	int _player;
	//! creating network manager object
	NetworkManager *_parent;	// _parent must be valid
	//! current state of component (multiplayer game)
	NetworkGameState _state;
	//! id of next created network object in this component
	int _nextId;

	//! squads of logged players
	RefArray<SquadIdentity> _squads;
	//! info about logged players
	AutoArray<PlayerIdentity> _identities;

	//! info about current mission
	MissionHeader _missionHeader;
	//! role slots
	AutoArray<PlayerRole> _playerRoles;
	//! parameter of current mission
	float _param1;
	//! parameter of current mission
	float _param2;
	
	//! messages received local (inside process)
	AutoArray<NetworkLocalMessageInfo> _receivedLocalMessages;

	//! files currently receiving
	AutoArray<ReceivingFile> _files;
	
#if _ENABLE_CHEATS
	//! Statistics about messages by types
	AutoArray<NetworkStatisticsItem> _statistics;
#endif

#if _ENABLE_DEDICATED_SERVER || _ENABLE_CHEATS
	//! Statistics about messages by players
	AutoArray<NetworkRawStatisticsItem> _rawStatistics;
#endif

public:
	//! Constructor
	/*!
	\param parent creating network manager object
	*/
	NetworkComponent(NetworkManager *parent);
	//! Destructor
	~NetworkComponent();

	//! Add message to local received messages
	/*!
	\param from DirectPlay ID of sender
	\param type message type
	\param msg message itself
	\return index of message in array of local received messages
	*/
	int AddLocalMessage(int from, NetworkMessageType type, NetworkMessage *msg)
	{
		int index = _receivedLocalMessages.Add();
		_receivedLocalMessages[index].from = from;
		_receivedLocalMessages[index].type = type;
		_receivedLocalMessages[index].msg = msg;

//if (type == 9)
//	Log("Message %d, type %d", index, type);
		return index;
	}

	//! Return DirectPlay ID of attached player
	int GetPlayer() const {return _player;}
	//! Implementation of direct message send through DirectPlay
	/*!
	\param to DirectPlay ID of message receiver
	\param rawMsg message content
	\param msgID out parameter to store DirectPlay ID of message
	\param dwFlags DirectPlay flags
	*/
	virtual bool DXSendMsg(int to, NetworkMessageRaw &rawMsg, DWORD &msgID, NetMsgFlags dwFlags) = NULL;

	//! Component simulation (process received messages, send update messages etc.)
	virtual void OnSimulate() = NULL;
	//! Process (user) message
	/*!
	\param from DirectPlay ID of sender
	\param msg message itself
	\param type message type
	*/
	virtual void OnMessage(int from, NetworkMessage *msg, NetworkMessageType type) = NULL;

	//! perform regular memory clean-up
	virtual unsigned CleanUpMemory() = NULL;

	//! Return if given client is loval on server's computer
	virtual bool IsLocalClient(int client) const {return false;}
	
	//{ DEDICATED SERVER SUPPORT
	#if _ENABLE_DEDICATED_SERVER
	//! Called when message is received to calculate transfer rate on dedicated server
	virtual void OnMonitorIn(int size) {}
	#endif
	//}

	//! Hi-level send of message
	/*!
	\param to DirectPlay ID of message receiver
	\param object message object
	\param dwFlags DirectPlay flags
	*/
	virtual DWORD SendMsg
	(
		int to,
		NetworkSimpleObject *object, /*int creator, int id,*/
		NetMsgFlags dwFlags
	);

	//! Return current state of game
	NetworkGameState GetGameState() const {return _state;}
	//! Return mission header
	const MissionHeader *GetMissionHeader() const {return &_missionHeader;}
	//! Return number of role slots for current mission
	int NPlayerRoles() const {return _playerRoles.Size();}
	//! Return given role slot 
	const PlayerRole *GetPlayerRole(int role) const
	{
		if (role >= 0 && role < NPlayerRoles()) return &_playerRoles[role];
		return NULL;
	}
	//! Find slot for given client
	const PlayerRole *FindPlayerRole(int player) const;
	//! Return role slot for local client
	const PlayerRole *GetMyPlayerRole() const {return FindPlayerRole(_player);}
	//! Search for identity with given player id
	const PlayerIdentity *FindIdentity(int dpnid) const;
	//! Search for identity with given player id
	PlayerIdentity *FindIdentity(int dpnid);
	//! Return array of identities
	const AutoArray<PlayerIdentity> *GetIdentities() const {return &_identities;}
	//! Retrieves parameters for current mission
	void GetParams(float &param1, float &param2) const {param1 = _param1; param2 = _param2;}

	//! Return component type ("server" / "client") for debugging purposes
	virtual const char *GetDebugName() const = NULL;

	//! Decode (and process) message from raw data
	/*!
	\param from DirectPlay ID of sender
	\param src source raw data
	*/
	void DecodeMessage(int from, NetworkMessageRaw &src);
	//! Calculate size of message after serialization and compression
	/*!
	\param msg calculated message
	\param type type of calculated message
	*/
	int CalculateMessageSize(NetworkMessage *msg, NetworkMessageType type);

	//! File transfer
	/*!
	Split file into segments and send segments using standard messages.
	\param to DirectPlay ID of file receiver
	\param dest filename on destination computer
	\param source local filename
	*/
	void TransferFile(int to, RString dest, RString source);
	//! Transfer player's face from server to client
	/*!
	\param to DirectPlay ID of face receiver
	\param player player which face is transfered
	*/
	void TransferFace(int to, RString player);
	//! Transfer player's custom radio messages from server to client
	/*!
	\param to DirectPlay ID of messages receiver
	\param player player which messages are transfered
	*/
	void TransferCustomRadio(int to, RString player);

#if _ENABLE_CHEATS
	//! Write into statistics info about sent message
	/*!
	\param msg message type
	\param size size of sent message
	*/
	void StatMsgSent(int msg, int size);
	//! Write into statistics info about received message
	/*!
	\param msg message type
	\param size size of received message
	*/
	void StatMsgReceived(int msg, int size);
	//! Return statistics about messages by types
	AutoArray<NetworkStatisticsItem> &GetStatistics() {return _statistics;}
#endif

#if _ENABLE_DEDICATED_SERVER || _ENABLE_CHEATS
	//! Write raw statistics info about sent message
	/*!
	\param msg message type
	\param to player
	*/
	void StatRawMsgSent(int to, int size);
	//! Write raw statistics info about received message
	/*!
	\param msg message type
	\param from player
	*/
	void StatRawMsgReceived(int from, int size);
#endif

protected:
	//! Encode message into raw data
	/*!
	\param dst destination data
	\param msg source message
	\param format format of source message
	*/
	void EncodeMsg(NetworkMessageRaw &dst, NetworkMessage *msg, NetworkMessageFormatBase *format);
	//! Encode single message item into raw data
	/*!
	\param dst destination data
	\param msg source message
	\param val source message item
	\param formatItem format of source message item
	*/
	void EncodeMsgItem(NetworkMessageRaw &dst, NetworkMessage *msg, const RefNetworkData &val, const NetworkMessageFormatItem &formatItem);
	//! Decode message from raw data
	/*!
	\param msg destination message
	\param src source data
	\param format format of destination message
	*/
	void DecodeMsg(NetworkMessage *msg, NetworkMessageRaw &src, NetworkMessageFormatBase *format);
	//! Decode single message item from raw data
	/*!
	\param src source data
	\param msg destination message
	\param formatItem format of destination message item
	\return destination message item
	*/
	RefNetworkData DecodeMsgItem(NetworkMessageRaw &src, NetworkMessage *msg, NetworkMessageFormatItem &formatItem);

	//! Calculate size of message after serialization and compression
	/*!
	\param msg calculated message
	\param format format of calculated message
	*/
	int CalculateMsgSize(NetworkMessage *msg, NetworkMessageFormatBase *format);
	//! Calculate size of single message item after serialization and compression
	/*!
	\param msg calculated message
	\param val calculated message item
	\param formatItem format of calculated message item
	*/
	int CalculateMsgItemSize(NetworkMessage *msg, const RefNetworkData &val, const NetworkMessageFormatItem &formatItem);

	//! Process all received messages (local, system and user)
	void ReceiveLocalMessages();


	//! Send single network message
	/*!
	This function determine, whic type of send will be used - local send, agregation or send using DirectPlay
	\param to DirectPlay ID of message receiver
	\param msg message itself
	\param type type of message
	\param dwFlags DirectPlay flags
	*/
	DWORD SendMsg
	(
		int to, NetworkMessage *msg,
		NetworkMessageType type, NetMsgFlags dwFlags
	);
	//! Send single network message using DirectPlay
	/*!
	\param to DirectPlay ID of message receiver
	\param msg message itself
	\param type type of message
	\param dwFlags DirectPlay flags
	*/
	DWORD SendMsgRemote
	(
		int to, NetworkMessage *msg,
		NetworkMessageType type, NetMsgFlags dwFlags
	);
	//! Send (part of) message queue as single DirectPlay message
	/*!
	\param to DirectPlay ID of message receiver
	\param queue messages to agregate
	\param begin index of first message to send
	\param end index of first message not to send
	\param dwFlags DirectPlay flags
	*/
	DWORD SendMsgQueue
	(
		int to, NetworkMessageQueue &queue,
		int begin, int end, NetMsgFlags dwFlags
	);
	//! Lo-level data send using DirectPlay
	/*!
	\param to DirectPlay ID of message receiver
	\param rawMsg data to send
	\param dwFlags DirectPlay flags
	\param diagLevel level of diagnostics about message
	*/
	DWORD SendMsgRaw(int to, NetworkMessageRaw &rawMsg, NetMsgFlags dwFlags, int diagLevel);
	//! Send single guaranteed network message to message queue (candidate for agregation)
	/*!
	\param to DirectPlay ID of message receiver
	\param msg message itself
	\param type type of message
	*/
	virtual void EnqueueMsg
	(
		int to, NetworkMessage *msg,
		NetworkMessageType type
	) = NULL;
	//! Send single nonguaranteed network message to message queue (candidate for agregation)
	/*!
	\param to DirectPlay ID of message receiver
	\param msg message itself
	\param type type of message
	*/
	virtual void EnqueueMsgNonGuaranteed
	(
		int to, NetworkMessage *msg,
		NetworkMessageType type
	) = NULL;

	//! Called when file segment is received
	/*!
	\param msg segment of file
	\return 1 if whole file is received (and saved)
	\return -1 if whole file is received, but with errors (save failed)
	*/
	int ReceiveFileSegment(TransferFileMessage &msg);
};

//! Info about single type of update of network object
struct NetworkUpdateInfo
{
	//! last created update message
	Ref<NetworkMessage> lastCreatedMsg;
	//! DirectPlay ID of last created update message
	DWORD lastCreatedMsgId;
	//! send time of created update message
	DWORD lastCreatedMsgTime;
	//! last sent update message (sent was confirmed by DPN_MSGID_SEND_COMPLETE system message)
	Ref<NetworkMessage> lastSentMsg;
	//! message can be canceled (false for initial update message)
	bool canCancel;

	// update structure - pending message send was completed
	bool OnSendComplete(bool ok);
};

//! Info about local object on client
struct NetworkLocalObjectInfo //: public RefCount
{
	//! ID of object
	NetworkId id;
	//! object itself
	OLink<NetworkObject> object;
	//! state of updates
	NetworkUpdateInfo updates[NMCUpdateN];
};
TypeContainsOLink(NetworkLocalObjectInfo)
//TypeIsGeneric(NetworkLocalObjectInfo)

//! Info about remote object on client
struct NetworkRemoteObjectInfo
{
	//! ID of object
	NetworkId id;
	//! object itself
	OLink<NetworkObject> object;
};
TypeContainsOLink(NetworkRemoteObjectInfo)

struct RespawnQueueItem;

//! map sounds transferred over network to their unique ids
struct PlaySoundInfo
{
	//! unique id of sound
	int creator,id;
	//! sound itself
	Link<AbstractWave> wave;
};
TypeIsGeneric(PlaySoundInfo)

//! map directly speaing player to object and sound buffer
struct Network3DSoundBuffer
{
	//! DirectPlay ID of speaking player
	int player;
	//! DirectSound buffer used for speaking player
	SRef<NetTranspSound3DBuffer> buffer;
	//! sound source object
	OLink<NetworkObject> object;
};
TypeContainsOLink(Network3DSoundBuffer)

//! Class used for transfer of client info (for example camera position) to server
/*!
\patch_internal 1.27 Date 10/11/2001 by Jirka
- Improved: camera position transfer in multiplayer game
*/
class ClientInfoObject : public NetworkObject
{
protected:
	NetworkId _id;

public:
	Vector3 _cameraPosition;

public:
	// Constructor
	ClientInfoObject() {_cameraPosition = InvalidCamPos;}

	void SetNetworkId(NetworkId &id) {_id = id;}
	NetworkId GetNetworkId() const {return _id;}
	Vector3 GetCurrentPosition() const {return VZero;}
	bool IsLocal() const {return true;}
	void SetLocal(bool local = true) {}
	void DestroyObject() {}
	
	NetworkMessageType GetNMType(NetworkMessageClass cls = NMCCreate) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx);
	static float CalculateErrorCoef(Vector3Par position, Vector3Par cameraPosition) {return 1.0;}
	RString GetDebugName() const;
};

struct UpdateLocalObjectInfo;

struct BodyInfo
{
	OLink<Person> body;
	Time hideTime;
	float value;
};
TypeContainsOLink(BodyInfo)

//! Network Client
class NetworkClient : public NetworkComponent
{
protected:
	//! low-level client implementation
	SRef<NetTranspClient> _client;
	
	//! info about objects local on client
	AutoArray<NetworkLocalObjectInfo> _localObjects;
	//! info about objects non local (remote) on client
	AutoArray<NetworkRemoteObjectInfo> _remoteObjects;
	//! units waiting for respawn
	AutoArray<RespawnQueueItem> _respawnQueue;
	//! dead bodies (max MAX_LOCAL_BODIES can be simulated)
	AutoArray<BodyInfo> _bodies;

	//! message formats
	AutoArray<NetworkMessageFormatBase> _formats;
	//! message queues (candidates for agregation)
	NetworkMessageQueue _messageQueue, _messageQueueNonGuaranteed;

	//! map directly speaing player to object and sound buffer
	AutoArray<Network3DSoundBuffer> _soundBuffers;

	//! connection quality to server
	ConnectionQuality _connectionQuality;
	//! result of _dp->Connect
	ConnectResult _connectResult;

	//! current state on server
	NetworkGameState _serverState;

	//! mission pbo file is valid (actual)
	bool _missionFileValid;

	//! player is game master of dedicated server
	bool _gameMaster;
	//! gamemaster is only voted admin (cannot do shutdown etc.)
	bool _admin;
	//! show "select mission" dialog for dedicated server
	bool _selectMission;
	//! show "vote mission" dialog for dedicated server
	bool _voteMission;

	//! Game is paused due to disconnection state of game
	bool _controlsPaused;

	//! missions on dedicated server
	AutoArray<RString> _serverMissions;

	//! name of local player
	RString _localPlayerName;

	//! id of next sound - incremented for each PlaySound
	int _soundId;
	//! list of sent sounds
	AutoArray<PlaySoundInfo> _sentSounds;
	//! list of received sounds
	AutoArray<PlaySoundInfo> _receivedSounds;

	//! sound introducing chat message
	SoundPars _chatSound;

	//! used for transfer of client info (for example camera position) to server
	Ref<ClientInfoObject> _clientInfo;

	//! number of bodies we want to hide
	int _hideBodies;

public:
	//! Constructor
	/*!
	\param parent creating network manager object
	\param address URL address of server
	\param password password of session
	\param botClient client is bot client
	*/
	NetworkClient(NetworkManager *parent, RString address, RString password, bool botClient);
	//! Destructor
	~NetworkClient();

	//! Connect to server
	/*!
	\param address URL address of server
	\param password password of session
	\param botClient client is bot client
	\return true if client is connected to server
	*/
	bool Init(RString address, RString password, bool botClient);
	//! Disconnect from server
	void Done();

	bool IsValid() const {return _client != NULL;}

	//! Return name of local player
	RString GetLocalPlayerName() const {return _localPlayerName;}
	//! Return result of _dp->Connect
	ConnectResult GetConnectResult() const {return _connectResult;}

	//! Return if player is game master of dedicated server
	bool IsGameMaster() const {return _gameMaster;}

	//! Return if gamemaster is only voted admin (cannot do shutdown etc.)
	bool IsAdmin() const {return _admin;}

	//! Return if client is bot client
	inline bool IsBotClient() const;

	//! Return connection quality to server
	ConnectionQuality GetConnectionQuality() const {return _connectionQuality;}

	//! Return current state on server
	NetworkGameState GetServerState() const {return _serverState;}

	//! Return recommended chat priorty based on current game state
	NetMsgFlags GetChatPriority() const;

	//! Set parameters for current mission
	void SetParams(float param1, float param2);

	void OnSimulate();
	void OnSendComplete(DWORD msgID, bool ok);
	void OnMessage(int from, NetworkMessage *msg, NetworkMessageType type);

	//! perform regular memory clean-up
	unsigned CleanUpMemory();

	//! Process received system messages
	void ReceiveSystemMessages();
	//! Process received user messages
	void ReceiveUserMessages();
	//! Destroy all received system messages
	void RemoveSystemMessages();
	//! Destroy all received user messages
	void RemoveUserMessages();

	
	//! Hi-level send of message to server
	/*!
	\param object message object
	\param dwFlags DirectPlay flags
	*/
	DWORD SendMsg
	(
		NetworkSimpleObject *object, NetMsgFlags dwFlags
	);
	bool DXSendMsg(int to, NetworkMessageRaw &rawMsg, DWORD &msgID, NetMsgFlags dwFlags);
	NetworkMessageFormatBase *GetFormat(/*int client, */int type);

	//! Retrieve list of players
	void GetPlayers(AutoArray<NetPlayerInfo, MemAllocSA> &players);
	//! Assign player to role slot
	void AssignPlayer(int role, int player);

	//! Select person as player
	/*!
	\param player DirectX player id
	\param person player's person
	\param respawn play "ressurect" cutscene
	*/
	void SelectPlayer(int player, Person *person, bool respawn = false);
	//! Attach person to unit
	/*!
	Send message to server, which person has which brain.
	\param person person which brain is attached to itself
	*/
	void AttachPerson(Person *person);
	//! Play sound on all clients (except itself)
	/*!
	\param name name of sound file
	\param position source position
	\param speed source speed
	\param volume sound volume
	\param freq sound frequency
	\param wave local sound object (for mapping local sounds to sent sounds)
	*/
	void PlaySound
	(
		RString name, Vector3Par position, Vector3Par speed, 
		float volume, float freq, AbstractWave *wave
	);
	//! Change state of played sound on all clients (except itself)
	/*!
	\param wave local sound object
	\param state state to set
	*/
	void SoundState(AbstractWave *wave, SoundStateType state);
	//! Register vehicle on client and send message to server (and other clients)
	/*!
	\param veh registered vehicle
	\param type id of vehicle list to add into
	\param name name of variable vehicle is stored in
	\param idVeh id of vehicle (assigned in mission editor)
	*/
	bool CreateVehicle(Vehicle *veh, VehicleListType type, RString name, int idVeh);
	//! Register whole AI structure for given AICenter (center, groups, subgroups, units)
	bool CreateCenter(AICenter *center);	// whole AI structure
	//! Register generic network object and send message to server (and other clients)
	bool CreateObject(NetworkObject *object);
	//! Send first update of all created objects over network (as guaranteed message)
	void CreateAllObjects();
	//! Unregister all objects, both local and remote
	void DestroyAllObjects();
	//! Register command as network object, send message to others
	bool CreateCommand(AISubgroup *subgrp, int index, Command *cmd);
	//! Unregister command, send message to others
	void DeleteCommand(AISubgroup *subgrp, int index, Command *cmd);
	//! Send message to server that player changed his state
	void ClientReady(NetworkGameState state);

	//! Ask object owner for damage of object
	/*!
	\param who damaged object
	\param owner who is responsible for damage
	\param modelPos position of damage
	\param val amount of damage
	\param valRange range of damage
	\param ammunition type
	*/
	void AskForDammage
	(
		Object *who, EntityAI *owner,
		Vector3Par modelPos, float val, float valRange, RString ammo
	);
	//! Ask object owner for set of total damage of object
	/*!
	\param who damaged object
	\param damage new value of total damage
	*/
	void AskForSetDammage
	(
		Object *who, float dammage
	);
	//! Ask vehicle owner for get in person
	/*!
	\param soldier who is getting in
	\param vehicle vehicle to get in
	\param position position in vehicle to get in
	*/
	void AskForGetIn
	(
		Person *soldier, Transport *vehicle,
		GetInPosition position
	);
	//! Ask vehicle owner for get out person
	/*!
	\param soldier who is getting out
	\param vehicle vehicle to get out
	\param getOutTo vehicle to get in
	*/
	void AskForGetOut
	(
		Person *soldier, Transport *vehicle, bool parachute
	);
	//! Ask vehicle owner for change person position
	/*!
	\param soldier who is changing position
	\param vehicle vehicle where position is changed
	\param type performed action
	*/
	void AskForChangePosition
	(
		Person *soldier, Transport *vehicle, UIActionType type
	);
	//! Ask vehicle owner for aim weapon
	/*!
	\param vehicle vehicle which weapon is aiming
	\param weapon aiming weapon index
	\param dir direction to aim
	*/
	void AskForAimWeapon
	(
		EntityAI *vehicle, int weapon, Vector3Par dir
	);
	//! Ask vehicle owner for aim observer turret
	/*!
	\param vehicle vehicle which turret is aiming
	\param dir direction to aim
	*/
	void AskForAimObserver
	(
		EntityAI *vehicle, Vector3Par dir
	);
	//! Ask vehicle owner for select weapon
	/*!
	\param vehicle vehicle which weapon is selecting
	\param weapon selected weapon index
	*/
	void AskForSelectWeapon
	(
		EntityAI *vehicle, int weapon
	);
	//! Ask vehicle owner for change ammo state
	/*!
	\param vehicle vehicle which ammo is changing
	\param weapon weapon index
	\param burst amount of ammo to decrease
	*/
	void AskForAmmo
	(
		EntityAI *vehicle, int weapon, int burst
	);
	//! Ask vehicle owner for add impulse
	/*!
	\param vehicle vehicle impulse is applied to
	\param force applied force
	\param torque applied torque
	*/
	void AskForAddImpulse
	(
		Vehicle *vehicle, Vector3Par force, Vector3Par torque
	);
	//! Ask object owner for move object
	/*!
	\param vehicle moving object
	\param pos new position
	*/
	void AskForMove
	(
		Object *vehicle, Vector3Par pos
	);
	//! Ask object owner for move object
	/*!
	\param vehicle moving object
	\param trans new transformation matrix
	*/
	void AskForMove
	(
		Object *vehicle, Matrix4Par trans
	);
	//! Ask group owner for join groups
	/*!
	\param join joined group
	\param group joining group
	*/
	void AskForJoin
	(
		AIGroup *join, AIGroup *group
	);
	//! Ask group owner for join groups
	/*!
	\param join joined group
	\param units joining units
	*/
	void AskForJoin
	(
		AIGroup *join, OLinkArray<AIUnit> &units
	);
	//! Ask person owner for hide body
	/*!
	\param vehicle body to hide
	*/
	void AskForHideBody(Person *vehicle);
	//! Transfer explosion effects (explosion, smoke, etc.) to other clients
	/*!
	\param owner shot owner (who is responsible for explosion)
	\param shot shot
	\param directHit hitted object
	\param pos explosion position
	\param dir explosion direction
	\param type ammunition
	\param enemyDammage some enemy was damaged
	*/
	void ExplosionDammageEffects
	(
		EntityAI *owner, Shot *shot,
		Object *directHit, Vector3Par pos, Vector3Par dir, const AmmoType *type,
		bool enemyDammage
	);
	//! Transfer fire effects (sound, fire, smoke, recoil effect, etc.) to other clients
	/*!
	\param vehicle firing vehicle
	\param weapon firing weapon index
	\param magazine fired magazine
	\param target aimed target
	*/
	void FireWeapon
	(
		EntityAI *vehicle, int weapon, const Magazine *magazine, EntityAI *target
	);
	//! Send update of weapons to vehicle owner
	/*!
	\param vehicle vehicle to update
	*/
	void UpdateWeapons(EntityAI *vehicle);
	//! Ask vehicle to add weapon into cargo
	/*!
	\param vehicle asked vehicle
	\param weapon name of weapon type to add
	*/
	void AddWeaponCargo(VehicleSupply *vehicle, RString weapon);
	//! Ask vehicle to remove weapon from cargo
	/*!
	\param vehicle asked vehicle
	\param weapon name of weapon type to remove
	*/
	void RemoveWeaponCargo(VehicleSupply *vehicle, RString weapon);
	//! Ask vehicle to add magazine into cargo
	/*!
	\param vehicle asked vehicle
	\param magazine magazine to add
	*/
	void AddMagazineCargo(VehicleSupply *vehicle, const Magazine *magazine);
	//! Ask vehicle to remove magazine from cargo
	/*!
	\param vehicle asked vehicle
	\param creator id of magazine to remove
	\param id id of magazine to remove
	*/
	void RemoveMagazineCargo(VehicleSupply *vehicle, int creator, int id);
	//! Transfer init expression to other clients and execute it
	/*!
	\param init vehicle init expression description
	*/
	void VehicleInit(VehicleInitCmd &init);
	//! Transfer message who is responsible to destroy vehicle to other clients
	/*!
	\param killed destroyed vehicle
	\param killer who is responsible for destroying
	*/
	void OnVehicleDestroyed(EntityAI *killed, EntityAI *killer);
	//! Transfer message about damage of vehicle to other clients
	/*!
	\param damaged damaged vehicle
	\param damage amount of damage hit
	\param ammo ammunition type
	*/
	void OnVehicleDamaged(EntityAI *damaged, EntityAI *killer, float damage, RString ammo);
	//! Transfer message about fired missile to other clients
	/*!
	\param target missile target
	\param ammo ammunition type
	\param owner missile owner
	*/
	void OnIncomingMissile(EntityAI *target, RString ammo, EntityAI *owner);
	//! Transfer info about (user made) marker creation to other clients
	/*!
	\param channel chat channel (who will see the marker)
	\param sender sender unit
	\param units receiving units
	\param info marker itself
	*/
	void MarkerCreate(int channel, AIUnit *sender, RefArray<NetworkObject> &units, ArcadeMarkerInfo &info);
	//! Transfer info about (user made) marker was deleted to other clients
	/*!
	\param name name of marker
	*/
	void MarkerDelete(RString name);
	//! Ask flag (carrier) owner for assign new owner
	/*!
	\param owner new owner
	\param carrier flag carrier
	*/
	void SetFlagOwner
	(
		Person *owner, EntityAI *carrier
	);
	//! Ask client owns flag owner for change of flag ownership
	/*!
	\param owner new or old flag owner
	\param carrier flag carrier
	*/
	void SetFlagCarrier
	(
		Person *owner, EntityAI *carrier
	);

	//! Public variable to other clients
	/*!
	\param name variable name
	*/
	void PublicVariable(RString name);
	//! Send chat message
	/*!
	\param channel chat channel
	\param text chat text
	*/
	void Chat(int channel, RString text);
	//! Send chat message
	/*!
	\param channel chat channel
	\param sender sender unit
	\param units receiving units
	\param text chat text
	*/
	void Chat(int channel, AIUnit *sender, RefArray<NetworkObject> &units, RString text);
	//! Send chat message
	/*!
	\param channel chat channel
	\param sender sender name
	\param units receiving units
	\param text chat text
	*/
	void Chat(int channel, RString sender, RefArray<NetworkObject> &units, RString text);
	//! Send radio message as chat (text and sentence)
	/*!
	\param channel chat channel
	\param sender sender name
	\param units receiving units
	\param text chat text
	\param sentence - list of words to be spoken
	*/
	void RadioChat(int channel, AIUnit *sender, RefArray<NetworkObject> &units, RString text, RadioSentence &sentence);
	//! Send text radio message as chat (sound and title)
	/*!
	\param channel chat channel
	\param units receiving units
	\param wave name of class from CfgRadio containing sound and title
	\param sender sender unit
	\param senderName sender name
	*/
	void RadioChatWave(int channel, RefArray<NetworkObject> &units, RString wave, AIUnit *sender, RString senderName);
	//! Set channel for Voice Over Net
	/*!
	\param channel chat channel
	*/
	void SetVoiceChannel(int channel);
	//! Set channel for Voice Over Net
	/*!
	\param channel chat channel
	\param units receiving units
	*/
	void SetVoiceChannel(int channel, RefArray<NetworkObject> &units);

	//! Transfer file to server
	/*!
	\param dest destination filename
	\param source source filename
	*/
	void TransferFileToServer(RString dest, RString source);
	//! Retrieve state of file transfer
	/*!
	\param curBytes amount of transfered bytes
	\param totBytes total amount bytes to transfer
	*/
	void GetTransferStats(int &curBytes, int &totBytes);
	//! Ask player to show target
	/*!
	\param vehicle player person
	\param target target to show
	*/
	void ShowTarget(Person *vehicle, TargetType *target);
	//! Ask player to show group direction
	/*!
	\param vehicle player person
	\param dir rirection to show
	*/
	void ShowGroupDir(Person *vehicle, Vector3Par dir);
	//! Transfer activation of group synchronization
	/*
	\param grp synchronized group
	\param active state of synchronization
	*/
	void GroupSynchronization(AIGroup *grp, int synchronization, bool active);
	//! Transfer activation of detector (through radio)
	/*
	\param grp synchronized group
	\param active state of synchronization
	*/
	void DetectorActivation(Detector *det, bool active);
	//! Ask group owner to create new unit
	/*!
	\param group group unit will be added to
	\param type name of vehicle type
	\param position position create at
	\param skill initial skill
	\param rank initial rank
	*/
	void AskForCreateUnit(AIGroup *group, RString type, Vector3Par position, RString init, float skill, Rank rank);
	//! Ask vehicle owner to destroy vehicle
	/*!
	\param vehicle vehicle to destroy
	*/
	void AskForDeleteVehicle(Entity *veh);

	//! Ask subgroup to receive answer from unit
	void AskForReceiveUnitAnswer
	(
		AIUnit *from, AISubgroup *to, int answer
	);
	//! Ask group owner to respawn player
	/*!
	\param person killed player
	\param killer killer entity
	*/
	void AskForGroupRespawn(Person *person, EntityAI *killer);
	//! Ask mine owner to activate it
	/*!
	\param mine mine
	\param activate activate / desactivate
	*/
	void AskForActivateMine(Mine *mine, bool activate);
	//! Ask fireplace to inflame / put down
	/*!
	\param fireplace fireplace
	\param fire inflame / put down
	*/
	void AskForInflameFire(Fireplace *fireplace, bool fire);
	//! Ask vehicle for user defined animation
	/*!
	\param vehicle animated vehicle
	\param animation animation name
	\param phase wanted animation phase
	*/
	void AskForAnimationPhase(Entity *vehicle, RString animation, float phase);
	//! Copy unit info from one person to other
	/*!
	\param from copy source
	\param to copy destination
	*/
	void CopyUnitInfo(Person *from, Person *to);
	//! Returns respawn mode
	RespawnMode GetRespawnMode() const {return _missionHeader.respawn;}
	//! Returns respawn delay
	float GetRespawnDelay() const {return _missionHeader.respawnDelay;}
	//! Add person to respawn queue (respawn after delay)
	/*!
	\param soldier person to respawn
	\param pos position where respawn
	*/
	void Respawn(Person *soldier, Vector3Par pos);

	//! Checks if gamemaster is to select mission on dedicated server
	bool CanSelectMission() const {return _selectMission;}
	//! Checks if client is to vote mission on dedicated server
	bool CanVoteMission() const {return _voteMission;}
	//! Returns array of missions available on dedicated server
	const AutoArray<RString> &GetServerMissions() const {return _serverMissions;}
	//! Select mission on dedicated server
	/*!
	\param mission name of selected mission
	\param cadetMode cadet / veteran mode
	*/
	void SelectMission(RString mission, bool cadetMode);
	//! Vote mission on dedicated server
	/*!
	\param mission name of selected mission
	\param cadetMode cadet / veteran mode
	*/
	void VoteMission(RString mission, bool cadetMode);

	NetworkObject *GetObject(NetworkId &id);
	//! Unregister given network object, send message to others
	void DeleteObject(NetworkId &id);

	//! Process chat command for remote control of dedicated server
	/*!
	\param command chat command
	*/
	bool ProcessCommand(RString command);

	//! Ask server to kick off player
	/*!
	\param player DirectPlay ID of player to kick off
	*/
	void SendKick(int player);

	//! Ask server to disable/enable connection of further clients
	/*!
	\param lock true to disable connection
	*/
	void SendLockSession(bool lock = true);

	//! player must disconnect due to some mission loading error
	void Disconnect(RString message);

	//! Force update of network object
	/*!
	\param object object to update
	\param dwFlags DirectPlay flags
	*/
	void UpdateObject(NetworkObject *object, NetMsgFlags dwFlags);

	const char *GetDebugName() const {return "Client";}

	//! Return estimated end of mission time
	Time GetEstimatedEndTime() const;

	//! Body can be hidden (for better performance)
	void DisposeBody(Person *body);

	//! Game is paused due to disconnection state of game
	bool IsControlsPaused() {return _controlsPaused;}

	//! Last received message's age in seconds (used to eliminate "disconnect cheat")
	float GetLastMsgAgeReliable();

	// implementation
protected:
	//! Find info about local object with given id (NULL if not found)
	NetworkLocalObjectInfo *GetLocalObjectInfo(NetworkId &id);
	//! Find info about remote object with given id (NULL if not found)
	NetworkRemoteObjectInfo *GetRemoteObjectInfo(NetworkId &id);

	//! Register network object as local object, assign id to them
	NetworkId CreateLocalObject(NetworkObject *object);

	//! Force update of network object (single type of update)
	/*!
	\param object object to update
	\param cls type of update
	\param dwFlags DirectPlay flags
	*/
	int UpdateObject(NetworkObject *object, NetworkMessageClass cls, NetMsgFlags dwFlags);
	//! Update all network objects in AI structure for given AICenter (center, groups, subgroups, units)
	bool UpdateCenter(AICenter *center);

	//! Prepare multiplayer game
	/*!
	Switch landscape, parse mission sqm file, initialize GWorld.
	*/
	bool PrepareGame();

	//! Respawn unit by info given by item of respawn queue
	void DoRespawn(RespawnQueueItem &item);

	//! Create network object and register as remote object
	/*!
	\param ctx network message context of create message
	\param dummy dummy argument for specification type of created network object
	*/
	template <class Type>
	bool CreateRemoteObject(NetworkMessageContext &ctx, Type *dummy)
	{
		if (_state < NGSLoadIsland) return false; // updates from the last session

		// check if object already exist
		Assert(dynamic_cast<const IndicesNetworkObject *>(ctx.GetIndices()))
		const IndicesNetworkObject *indices = static_cast<const IndicesNetworkObject *>(ctx.GetIndices());
		if (!indices) return false;

		NetworkId id;
		ctx.IdxTransfer(indices->objectCreator, id.creator);
		ctx.IdxTransfer(indices->objectId, id.id);
		
		if (GetObject(id))
		{
			RptF("Object %d:%d already exists", id.creator, id.id);
			return false;
		}

		// create object
		ctx.SetClass(NMCCreate);
		NetworkObject *obj = Type::CreateObject(ctx);
		if (!obj)
		{
			RptF("Cannot create object %d:%d", id.creator, id.id);
			return false;
		}

		// add to remote objects
		int index = _remoteObjects.Add();
		NetworkRemoteObjectInfo &info = _remoteObjects[index];
		info.id = id;
		info.object = obj;
		if (DiagLevel >= 1)
			DiagLogF("Client: remote object created %d:%d", id.creator, id.id);
		return true;
	}
	//! Unregister and destroy remote network object
	void DestroyRemoteObject(NetworkId id);

	//! Send all agregated and update messages
	void SendMessages();
	//! Calculate limits for message sending
	void EstimateBandwidth(int &nMsgMax, int &nBytesMax);
	//! Calculate errors and place object in list sorted by error
	/*!
	\param objects output sorted list of objects
	*/
	void CreateObjectsList(AutoArray<UpdateLocalObjectInfo, MemAllocSA> &objects);
	//! Prepare next object update message to message queue
	bool PrepareNextUpdate(AutoArray<UpdateLocalObjectInfo, MemAllocSA> &objects, int &next);

	//! Return number of auto destroyed local objects - used for debugging purposes
	int NLocalObjectsNULL() const;
	//! Check database of local objects - used for debugging purposes
	bool CheckLocalObjects() const;

	void EnqueueMsg
	(
		int to, NetworkMessage *msg,
		NetworkMessageType type
	);
	void EnqueueMsgNonGuaranteed
	(
		int to, NetworkMessage *msg,
		NetworkMessageType type
	);
};

//! Info about single type of update of network object
struct NetworkCurrentInfo
{
	//! type of message
	NetworkMessageType type;
	//! sender of message
	int from;
	//! last received state of object
	Ref<NetworkMessage> message;
};

#include <Es/Memory/normalNew.hpp>

//! info about state of sent updates of network object for single client
struct NetworkPlayerObjectInfo: public RefCount
{
	//! ID of player
	int player;
	//! state of updates
	NetworkUpdateInfo updates[NMCUpdateN];
	//! allocated quite often
	USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

//! Info about network object on server
/*!
\patch_internal 1.23 Date 9/18/2001 by Jirka
- Improved: storage of PlayerObjectInfo changes
*/
#include <Es/Memory/normalNew.hpp>

struct NetworkObjectInfo: public RefCount
{
	//! ID of object
	NetworkId id;
	//! DirectPlay ID of owner client
	int owner;
	//! state of last received updates
	NetworkCurrentInfo current[NMCUpdateN];
	//! state of last sent updates (for each player)
	RefArray<NetworkPlayerObjectInfo> playerObjects;

	//! Find info about network object for given player
	NetworkPlayerObjectInfo *GetPlayerObjectInfo(int player);
	//! Create info (register) of new network object for given player
	NetworkPlayerObjectInfo *CreatePlayerObjectInfo(int player);
	//! Delete info (unregister) network object for given player
	void DeletePlayerObjectInfo(int player);
	//! Objects of this type will be allocated quite often
	USE_FAST_ALLOCATOR
};
//TypeIsMovable(NetworkObjectInfo)

#include <Es/Memory/debugNew.hpp>

//! stores info about integrity question
struct IntegrityQuestion
{
	//! question name
	RString name;
	//! region in question
	int offset,size;
	//! Constructor
	/*!
	\param n question name
	\param o region in question
	\param s region in question
	*/
	IntegrityQuestion(const RString &n, int o, int s)
	:name(n),offset(o),size(s)
	{
	}
	//! Constructor
	IntegrityQuestion():name(""),offset(0),size(INT_MAX)
	{
	}
};

class NetworkServer;
struct NetworkPlayerInfo;

//! inteface for more detailed integrity check tests
/*!
when integrity check failed, we may wish to peform more detailed test
*/
class IntegrityInvestigation: public RefCount
{
	public:
	//! after test is finished, return test result information (if any)
	virtual RString GetResult() const = NULL;
	//! perform test iteration, return false if test finished
	virtual bool Proceed(NetworkServer *server, NetworkPlayerInfo *pi) = NULL;
	//! check if investigation asked given question
	virtual bool QuestionMatching(const IntegrityQuestion &q) const = NULL;
	// tell investigation its question have been answered
	virtual void QuestionAnswered(bool answerOK) = NULL;
};

class IntegrityInvestigationConfig: public IntegrityInvestigation
{
	//! class where investigation started
	const ParamClass *_root;
	//! class currently being tested 
	const ParamClass *_class;
	//! test result, if test is not fisnished yet, empty string
	RString _result;
	//! last question - we are expecting an aswer to it
	IntegrityQuestion _question;
	//! when we are expecting an answer to our question
	DWORD _questionTimeout;

	public:

	IntegrityInvestigationConfig(const char *path);
	virtual RString GetResult() const;
	virtual bool Proceed(NetworkServer *server, NetworkPlayerInfo *pi);
	virtual bool QuestionMatching(const IntegrityQuestion &q) const;
	virtual void QuestionAnswered(bool answerOK);
};

//! calculate min, max and average from set of values
class CalculateMinMaxAvg
{
	float _min,_max,_sum;
	float _weight;

	public:
	CalculateMinMaxAvg();
	//! add sample
	void Sample(float value, float weight=1);
	//! reset 
	void Reset();
	//! get current results (min)
	float GetMin() const {return _weight>0 ? _min : 0;}
	//! get current results (max)
	float GetMax() const {return _weight>0 ? _max : 0;}
	//! get current results (average)
	float GetAvg() const {return _weight>0 ? _sum/_weight :0;}
};

//! complete information about one question asked
struct IntegrityQuestionInfo
{
	int id;
	IntegrityQuestionType type;
	IntegrityQuestion q;
	//! Timeout for questions, UINT_MAX if question was never asked
	DWORD timeout;
	//! constructor
	IntegrityQuestionInfo()
	{
		id = 0;
		timeout = UINT_MAX;
	}
};

TypeIsMovable(IntegrityQuestionInfo)

//! Server info about player
/*!
\patch_internal 1.12 Date 08/06/2001 by Jirka
- Added: Integrity check - config, exe, data
*/
struct NetworkPlayerInfo
{
	//! DirectPlay ID of player (client)
	int dpid;
	//! Voice over net ID of player (client)
	int dvid;

	//! chat channel player transmits
	int channel;	

	//! name of player
	RString name;
	//! game state for given player
	NetworkGameState state;
	//! message queue (candidates for agregation)
	NetworkMessageQueue _messageQueue, _messageQueueNonGuaranteed;

	//! ping statistics
	CalculateMinMaxAvg _ping;
	//! bandwidth statistics
	CalculateMinMaxAvg _bandwidth;
	//! desync statistics
	CalculateMinMaxAvg _desync; 

	//! player's person
	NetworkId person;
	//! player's unit
	NetworkId unit;
	//! player's group
	NetworkId group;

	//! client has actual (valid) mission pbo file
	bool missionFileValid;

	//! kick-off already requested
	bool kickedOff;

	//! Connection loosing (no message for 10 seconds) was already reported to other users
	bool connectionProblemsReported;

	/*!
	\name Integrity check 
	*/
	//@{
	//! Questions asked
	AutoArray<IntegrityQuestionInfo> integrityQuestions;
	//IntegrityQuestion integrityQuestion[IntegrityQuestionTypeCount];
	//DWORD integrityQuestionTimeout[IntegrityQuestionTypeCount];
	//! when integrity check failed, we may wish to ask more detailed question
	Ref<IntegrityInvestigation> integrityInvestigation[IntegrityQuestionTypeCount];
	//! time when next random integrity check should be performed
	DWORD integrityCheckNext;
	//@}

	//! position of camera on given client
	Vector3 cameraPosition;
	//! time of last update of cameraPosition
	Time cameraPositionTime;

//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
	//! next message of the day index (-1 if done)
	int motdIndex;
	//! next message of the day time
	UITime motdTime;
#endif

	int nextQuestionId;

	int FindIntegrityQuestion(IntegrityQuestionType type, const IntegrityQuestion &q) const;
	int FindIntegrityQuestion(IntegrityQuestionType type) const;
	int FindIntegrityQuestion(int id) const;
	
//}
};
TypeIsMovable(NetworkPlayerInfo)

//! map person to unit in server database
struct PersonUnitPair
{
	//! id of person
	NetworkId person;
	//! id of unit
	NetworkId unit;
};
TypeIsMovable(PersonUnitPair)

//! free Win32 allocated memory when variable is destructed

class AutoFree
{
	//! handle to Win32 memory obtained by GlobalAlloc call
	void *_mem;

	private:
	//! no copy constructor
	AutoFree(const AutoFree &src);

	public:
	//! attach Win32 memory handle
	/*!
	\param mem handle to Win32 memory obtained by GlobalAlloc call
	*/
	void operator = (void *mem)
	{
		if (mem==_mem) return;
		Free();
		_mem = mem;
	}
	//! copy - reassing Win32 handle to new object
	/*! source will be NULL after assignement */
	void operator = (AutoFree &src)
	{
		if (src._mem == _mem) return;
		Free();
		_mem = src._mem;
		src._mem = NULL;
	}
	//! empty constructor
	AutoFree(){_mem = NULL;}
	//! construct from Win32 memory handle
	AutoFree(void *mem){_mem = mem;}
	//! convert to pointer
	operator void * () {return _mem;}
	//! destruct - free pointer
	~AutoFree(){Free();}
#ifdef _WIN32
	//! free pointer explicitelly
	void Free(){if (_mem) GlobalFree(_mem),_mem = NULL;}
#else
	//! free pointer explicitelly
	void Free(){if (_mem) free(_mem),_mem = NULL;}
#endif
};

//! context structure for DownloadToFile function
struct DownloadToFileContext
{
	const char *url; //!< in: url to download from
	const char *file; //!< in: path of download destination
	bool result; //!< out: true when donwload was successful
	Event *event; //!< in: event that should be signalled when download terminated
	const char *proxy; //! in: address of proxy server
};

//! context structure for DownloadToMem function
/*!
\patch_internal 1.24 Date 09/19/2001 by Ondra
- Fix: more robust handling of memory deallocation.
May avoid memory leaks in case of download errors.
*/

struct DownloadToMemContext
{
	const char *url; //! in: url to download from
	AutoFree result; //! out: memory with downloaded file
	size_t *size; //! in: pointer to variable that will receive size of downloaded file
	Event *event; //! in: event that should be signalled when download terminated
	const char *proxy; //! in: address of proxy server
};

//! Single vote in votings
struct Vote
{
	//! DirectPlay ID of player
	int player;
	//! Voted value
	AutoArray<char> value;

	//! Equality operator
	/*!
	\param val voted value
	\param valueSize size of voted value
	*/
	bool HasValue(const char *val, int valueSize) const;
};
TypeIsMovable(Vote)

//! Single voting
class Voting : public AutoArray<Vote>
{
	friend class Votings;
	typedef AutoArray<Vote> base;

protected:
	//! Unique id of voting
	AutoArray<char> _id;
	//! Threshold when voting is valid
	float _threshold;
	//! Check if more than half players has the same value
	bool _selection;

public:
	//! Add new vote
	/*!
	\param player DirectPlay ID of voting player
	\param value voted value (NULL if none)
	\param valueSize size of voted value
	*/
	int Add(int player, char *value = NULL, int valueSize = 0);

	//! Return ID of Voting
	const AutoArray<char> &GetID() const {return _id;}

	//! Check if voting is valid
	bool Check(const AutoArray<PlayerIdentity> &identities) const;
	//! Get voted value
	/*!
	\param n number of votes
	*/
	const AutoArray<char> *GetValue(int *n = NULL, int *n2=NULL) const;

	//! Equality operator
	/*!
	\param id unique identification of voting
	\param idSize size of unique identification
	*/
	bool HasID(char *id, int idSize) const;
};
TypeIsMovable(Voting)

/// information for client code verification
struct ExeCRCBlock
{
  int offset,size,crc;
};

class NetworkServer;

//! Voting system
class Votings : public AutoArray<Voting>
{
	typedef AutoArray<Voting> base;

public:
	//! Add new vote
	/*!
	\param server hosting server
	\param id unique identification of voting
	\param idSize size of unique identification
	\param threshold threshold when voting is valid
	\param player DirectPlay ID of voting player
	\param value voted value (NULL if none)
	\param valueSize size of voted value
	*/
	void Add(NetworkServer *server, char *id, int idSize, float threshold, int player, char *value = NULL, int valueSize = 0, bool selection = false);

	//! Check all votings (apply if satisfied)
	/*!
	\param server hosting server
	*/
	void Check(NetworkServer *server);
};

//! pending download of squad information
class CheckSquadObject
{
friend class NetworkServer;

protected:
	//! identify player 
	PlayerIdentity _identity;
	 //! out: squad information
	Ref<SquadIdentity> _squad;
	//! out: squad was created during download
	bool _newSquad;

	//! signalled when particular download step is done
	Event _stateDone;
	enum State {DownloadingSquad,DownloadingLogo,AllDone};
	//! state of download
	State _state;
	//! XML data of the squad
	AutoFree _squadXMLData;
	//! size of XML data
	size_t _squadXMLSize;
	//! URL of squad logo picture
	RString _logoUrl;
	//! path to squad logo picture (download destination)
	RString _logoFile;
	//! address of proxy server
	RString _proxy;

	//! state specific information (download logo file)
	DownloadToFileContext _logoContext;
	//! state specific information (donwload squad XML to memory)
	DownloadToMemContext _squadContext;

public:
	//! start download
	CheckSquadObject(PlayerIdentity &identity, Ref<SquadIdentity> squad, bool newSquad, RString proxy);
	//! destruct - wait until current download step is terminated
	~CheckSquadObject();
	//! check if download is terminated
	bool IsDone();

	//! start XML data background download
	void StartDownloadingXMLSource();
	//! end XML data background download
	void EndDownloadingXMLSource();

	//! start logo file background download
	void StartDownloadingLogo();

	//! process XML data, set default in case of error
	void ProcessXML();

protected:
	//! process XML data
	//! \return true if data are present and good
	bool DoProcessXML();
};

struct UpdateObjectInfo;

//! Network Server
class NetworkServer : public NetworkComponent
{
protected:
	//! low-level server implementation
	SRef<NetTranspServer> _server;

	//! list of players (server specific info)
	AutoArray<NetworkPlayerInfo> _players;
	//! list of network objects (actual state)
	RefArray<NetworkObjectInfo> _objects;

	//! used to store pending messages
	struct NetPendingMessage
	{
		NetworkObjectInfo *info;
		NetworkPlayerObjectInfo *player;
		NetworkUpdateInfo *update;
		DWORD msgID;
		ClassIsSimple(NetPendingMessage)
	};

	//! list of pending messages - used for fast message lookup
	AutoArray<NetPendingMessage> _pendingMessages;
	//! map persons to units
	AutoArray<PersonUnitPair> _mapPersonUnit;
	//! DirectPlay ID of bot client
	int _botClient;
	//! original name of current mission
	RString _originalName;

	//! no other players are enabled to connect
	bool _sessionLocked;
	
	//! password protected session
	bool _password;

  //! equal mod list required by host
  bool _equalModRequired;

//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
	//! DirectPlay ID of logged gamemaster (AI_PLAYER if no gamemaster)
	int _gameMaster;
	//! selected mission name
	RString _mission;
	//! selected mission cadet / veteran mode
	bool _cadetMode;

	//! gamemaster is only voted admin (cannot do shutdown etc.)
	bool _admin;

	//! kick off players with duplicate ID
	bool _kickDuplicate;

	//! is server dedicated
	bool _dedicated;
	//! server is asked to restart mission
	bool _restart;
	//! server is asked to reassign players
	bool _reassign;
	//! index of next mission in dedicated server config file
	int _missionIndex;
	//! preloaded dedicated server config file
	ParamFile _serverCfg;

	//@{
	//! Info about network trafic on dedicated server
	float _monitorInterval;
	DWORD _monitorNext;
	int _monitorFrames;
	int _monitorIn;
	int _monitorOut;
	//@}

	
	DWORD _debugNext;
	float _debugInterval;
	//! switched on debug flags
	FindArrayKey<int> _debugOn;

	//! messages of the day
	AutoArray<RString> _motd;
	//! messages of the day interval
	float _motdInterval;

	//! URL of Query & Reporting master server
	RString _reportingIP;

#endif
//}

	//! time when next ping status should be broadcasted
	DWORD _pingUpdateNext;

	//! full path to mission pbo file
	RString _missionBank;

	//! global (static) ban list
	FindArray<__int64> _banListGlobal;
	//! local (dynamic) ban list
	FindArray<__int64> _banListLocal;

	//! actually tested identities (squad competence)
	AutoArray< SRef<CheckSquadObject> > _squadChecks;

	//! next available (short) id for player
	int _nextPlayerId;

	//! votings currently in progress
	Votings _votings;

	//! threshold for votings
	float _voteThreshold;

	//! address of proxy server
	RString _proxy;

public:
	//! Constructor
	NetworkServer(NetworkManager *parent, int port, RString password);
	//! Destructor
	~NetworkServer();

	//! Creation of DirectPlay server and MP session
	/*!
	\param port IP port where server comunicate
	\param password session password
	*/
	bool Init(int port, RString password);
	//! Destroying of MP session and DirectPlay server
	void Done();

	//! Kick off given player from game
	void KickOff(int dpnid, KickOffReason reason);
	//! Ban given player (kick off + add to dynamic ban list)
	void Ban(int dpnid);

	//! Return if session was created
	bool IsValid() const {return _server != NULL;}

	//! Set DirectPlay ID of bot client
	void SetBotClient(int client) {_botClient = client;}

	bool IsDedicatedBotClient(int dpnid) const;

	bool IsLocalClient(int client) const
	{
		// Assume bot client is always local
		return client == _botClient;
	}

//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
	//! Mark server as dedicated
	/*!
	\param config name (path) of config file
	*/
	void SetDedicated(RString config);
#endif
//}

	//! disable/enable connecting of further clients 
	void LockSession(bool lock = true){_sessionLocked = lock;}

	//! Retrieves URL of server
	bool GetURL(char *address, DWORD addressLen);

	//! Creates pbo file for selected mission
	void CreateMission(RString mission, RString world);
	//! Initialize mission
	/*!
	- create and send mission header structure (including difficulty settings)
	- create and send slots for sides and roles
	*/
	void InitMission(bool cadetMode);

	void OnSimulate();
	void OnSendComplete(DWORD msgID, bool ok);
	void OnSendStarted(DWORD msgID, const NetworkMessageQueueItem &item);
	void OnCreatePlayer(int player, bool botClient, const char *name);
	bool OnCreateVoicePlayer(int player);
	void OnMessage(int from, NetworkMessage *msg, NetworkMessageType type);

	//! perform regular memory clean-up
	unsigned CleanUpMemory();

	//! Process received system messages
	void ReceiveSystemMessages();
	//! Process received user messages
	void ReceiveUserMessages();
	//! Destroy all received system messages
	void RemoveSystemMessages();
	//! Destroy all received user messages
	void RemoveUserMessages();
	//! Perform all active integrity investigations
	void PerformIntegrityInvestigations();
	//! Perform initial (overall) integrity check
	void PerformInitialIntegrityCheck(NetworkPlayerInfo &pi);
	//! Perform quick random integrity check
	void PerformRandomIntegrityCheck(NetworkPlayerInfo &pi);
	//! perform check on single file
	void PerformFileIntegrityCheck(const char *file, int dpid=-1);
  /// perform check on exe image in the memory
  void PerformExeIntegrityCheck(const char *location, int dpid=-1);

	//! Check if given question is part of any investigation
	bool IntegrityAnswerReceived
	(
		NetworkPlayerInfo *pi,
		IntegrityQuestionType qType,
		const IntegrityQuestion &q, bool answerOK
	);

	DWORD SendMsg
	(
		int to, NetworkSimpleObject *object, NetMsgFlags dwFlags
	);
	bool DXSendMsg(int to, NetworkMessageRaw &rawMsg, DWORD &msgID, NetMsgFlags dwFlags);

	//! Retrieve list of players
	void GetPlayers(AutoArray<NetPlayerInfo, MemAllocSA> &players);

	NetworkMessageFormatBase *GetFormat(/*int client, */int type);

	//! Set current state of game (+ send to clients)
	void SetGameState(NetworkGameState state);
	//! Return game state for given player
	NetworkGameState GetPlayerState(int dpid);
	//! Return name for given player
	RString GetPlayerName(int dpid);
	//! Return camera position for given player
	Vector3 GetCameraPosition(int dpid);

	NetworkObject *GetObject(NetworkId &id);

	//! Unregister all objects
	void DestroyAllObjects();

	//! Send mission pbo file to clients
	void SendMissionFile();

	//! Check if all internal structures are in correct state
	bool CheckIntegrity() const;

	//! Check if pending message structures are in correct state
	bool CheckIntegrityOfPendingMessages() const;

	const char *GetDebugName() const {return "Server";}

	//! Ask client for check of integrity
	/*!
	\param dpnid DirectPlay ID of asked client
	\param type type of integrity check
	\param q parameters of integrity check
	*/
	bool IntegrityCheck(int dpnid, IntegrityQuestionType type, const IntegrityQuestion &q);
	//! Reaction when check of integrity failed for given client
	/*!
	\param dpnid DirectPlay ID of asked client
	\param type type of integrity check
	\param info additonal information
	\param final check is final and should be displayed to all players
	*/
	void OnIntegrityCheckFailed
	(
		int dpnid, IntegrityQuestionType type,
		const char *info, bool final
	);

	//! Creates player identity
	/*!
	Called asynchronously when identity is confirmed
	\param ident player identity
	\param squad player's squad
	*/
	void CreateIdentity(PlayerIdentity &ident, Ref<SquadIdentity> squad);

	//! Process result of voting when voting is valid
	/*!
	\param id unique identification of voting
	\param value voted value
	*/
	void ApplyVoting(const AutoArray<char> &id, const AutoArray<char> *value);

	//! some admin state change - scan admin status of all players
	void UpdateAdminState();

	//! Delete player info (unregister player)
	void OnPlayerDestroy(int dpid);

	//! Set estimated end of mission time
	/*!
	\param time estimated time
	*/
	void SetEstimatedEndTime(Time time);

	//! Return maximal allowed number of players on server
	int GetMaxPlayers();

protected:
	//! Find info about given player
	NetworkPlayerInfo *GetPlayerInfo(int dpid);
	//! Create player info (register player)
	NetworkPlayerInfo *OnPlayerCreate(int dpid, const char *name);

	//! find pending message in pending message list based on ID
	int FindPendingMessage(DWORD msgID) const;
	//! find pending message in pending message list based on update
	int FindPendingMessage(NetworkUpdateInfo *update) const;
	//! add new pending message
	void AddPendingMessage
	(
		DWORD msgID, NetworkObjectInfo *info,
		NetworkUpdateInfo *update, NetworkPlayerObjectInfo *player
	);

	//! Find info about given network object
	NetworkObjectInfo *GetObjectInfo(NetworkId &id);
	//! Create object info (register object)
	NetworkObjectInfo *OnObjectCreate(NetworkId &id, int owner);
	//! Delete object info (unregister object), return owner id
	int PerformObjectDestroy(const NetworkId &id);
	//! Delete object info (unregister object) and send message about destruction
	void OnObjectDestroy(const NetworkId &id);

	//! Called when update message received
	/*!
	\param id network object ID
	\param from sender DirectPlay ID
	\param msg update message
	\param type update message type
	\param cls update message class
	*/
	NetworkObjectInfo *OnObjectUpdate(NetworkId &id, int from, NetworkMessage *msg, NetworkMessageType type, NetworkMessageClass cls);
	//! Send update message
	/*!
	\param pInfo info about receiving player
	\param oInfo info about object to update
	\param cls update message class
	\param dwFlags DirectPlay flags
	*/
	int UpdateObject(NetworkPlayerInfo *pInfo, NetworkObjectInfo *oInfo, NetworkMessageClass cls, NetMsgFlags dwFlags);
	//! Calculate error multiplier
	/*!
	\param type update message type
	\param cameraPosition position of camera on receiving client
	\param position updated object position
	*/
	float CalculateErrorCoef
	(
		NetworkMessageType type, Vector3Par cameraPosition, Vector3Val position
	);
	//! Calculate error (difference) between two stored states (messages)
	/*!
	\param type update message type
	\param dpid1 sender of first message
	\param msg1 first message
	\param dpid2 sender of second message
	\param msg2 second message
	*/
	float CalculateError
	(
		NetworkMessageType type,
		int dpid1, NetworkMessage *msg1,
		int dpid2, NetworkMessage *msg2
	);

	//! Owner of some object changes
	/*!
	\param id ID of network object
	\param from DirectPlay ID of old owner
	\param to DirectPlay ID of new owner
	*/
	void ChangeOwner(NetworkId &id, int from, int to);
	//! Check and update objects ownership when group leader changes
	/*!
	\param group id of group
	\param leader id of new leader
	*/
	void UpdateGroupLeader(NetworkId &group, NetworkId &leader);

	//! Send mission info (mission heade, side slots and role slots) to client
	/*!
	\param DirectPlay ID of receiving client
	\param onlyPlayers do not send mission header
	*/
	void SendMissionInfo(int to, bool onlyPlayers = false);

	//! Set game state of given player
	void SetPlayerState(int dpid, NetworkGameState state);

	//! Send all agregated and update messages
	void SendMessages();
	//! Check if fade out players are to kicked off
	void CheckFadeOut();
	//! Calculate limits for message sending
	void EstimateBandwidth(NetworkPlayerInfo &pInfo, int nPlayers, int &nMsgMax, int &nBytesMax);
	//! Calculate errors and place object in list sorted by error
	/*!
	\param objects output sorted list of objects
	\param pInfo info about receiving player
	*/
	void CreateObjectsList(AutoArray<UpdateObjectInfo, MemAllocSA> &objects, NetworkPlayerInfo &pInfo);
	//! Prepare next object update message to message queue
	bool PrepareNextUpdate(NetworkPlayerInfo &pInfo, AutoArray<UpdateObjectInfo, MemAllocSA> &objects, int &next);

	//! Dedicated server simulation
	void SimulateDS();

	//! Show message in both console and chat
	void ServerMessage(const char *text);

	//! Find unit id for given person id
	NetworkId PersonToUnit(NetworkId &person);
	//! Find person id for given unit id
	NetworkId UnitToPerson(NetworkId &unit);
	//! Add pair to map persons to unit
	int AddPersonUnitPair(NetworkId &person, NetworkId &unit);

	//! Find players on chat channel
	/*!
	\param players output array of players
	\param units chat target units 
	\param from DirectPlay ID of sender
	\param voice true for Voice Over Net transfer
	*/
	void GetPlayersOnChannel(AutoArray<int, MemAllocSA> &players, AutoArray<NetworkId> units, DWORD from, bool voice);
	//! Find players on chat channel
	/*!
	\param players output array of players
	\param channel chat channel
	\param from DirectPlay ID of sender
	\param voice true for Voice Over Net transfer
	*/
	void GetPlayersOnChannel(AutoArray<int, MemAllocSA> &players, int channel, DWORD from, bool voice);

	/*!
	\name GameSpy Query & Reporting SDK support
	*/
	//@{
//{ QUERY & REPORTING SDK
#if _ENABLE_DEDICATED_SERVER && _ENABLE_GAMESPY
	//! Initialize Query & Reporting SDK support
	void InitQR();
	//! Close Query & Reporting SDK support
	void DoneQR();
	//! Simulation step of Query & Reporting SDK support
	void SimulateQR();
	//! Information about change of inner game state
	void ChangeStateQR();
public:
	//! Create answer to Basic question
	/*!
	\param outbuf output buffer
	\param maxlen size of output buffer
	*/
	void OnQueryBasic(char *outbuf, int maxlen);
	//! Create answer to Info question
	/*!
	\param outbuf output buffer
	\param maxlen size of output buffer
	*/
	void OnQueryInfo(char *outbuf, int maxlen);
	//! Create answer to Rules question
	/*!
	\param outbuf output buffer
	\param maxlen size of output buffer
	*/
	void OnQueryRules(char *outbuf, int maxlen);
	//! Create answer to Players question
	/*!
	\param outbuf output buffer
	\param maxlen size of output buffer
	*/
	void OnQueryPlayers(char *outbuf, int maxlen);
protected:
#endif
//}
	//@}

	//! Update session description server sending
	void UpdateSessionDescription();
	void EnqueueMsg
	(
		int to, NetworkMessage *msg,
		NetworkMessageType type
	);
	void EnqueueMsgNonGuaranteed
	(
		int to, NetworkMessage *msg,
		NetworkMessageType type
	);

//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
	//! Set interval in which dedicated server sends info about transfer rates to gamemaster
	void Monitor(float interval);
	void OnMonitorIn(int size);
	
	void DebugAsk(RString str, int from, bool fullAccess);
	void DebugAnswer(RString str);

	int ConsoleF(const char *format, ...);
#endif
//}

	//! Process message NMTPlayerRole
	void OnMessagePlayerRole(int from, NetworkMessageType type, NetworkMessageContext &ctx);
};

///////////////////////////////////////////////////////////////////////////////
// Network Manager

//! Implementation of basic network functions
class NetworkManager : public INetworkManager
{
friend class NetworkComponent;

protected:
	SRef<NetTranspSessionEnum> _sessionEnum;
	//! time of last call of EnumHosts
	UITime _lastEnumHosts;
	//! IP address of computer (empty for local network) where search for sessions
	RString _ip;
	//! port where search for sessions
	int _port;
	//! additional remote hosts
	AutoArray<RemoteHostAddress> _hosts;

	//! network server component
	SRef<NetworkServer> _server;
	//! network client component
	SRef<NetworkClient> _client;

public:
	//! Constructor
	NetworkManager();
	//! Destructor
	~NetworkManager();

	bool IsServer() const {return _server != NULL;}
	//! Return if client is created
	bool IsClient() const {return _client != NULL;}

	//! Access to server component
	NetworkServer *GetServer() {return _server;}
	//! Access to client component
	NetworkClient *GetClient() {return _client;}

	bool Init(RString ip, int port, bool startEnum);
	void Done();

	void GetSessions(AutoArray<SessionInfo> &sessions);
	RString IPToGUID(RString ip, int port);
	bool CreateSession(int port, RString password);
	ConnectResult JoinSession(RString guid, RString password);
	bool WaitForSession();
	void GetPlayers(AutoArray<NetPlayerInfo, MemAllocSA> &players);
	void CreateMission(RString mission, RString world);
	void InitMission(bool cadetMode);
	void Close();
	unsigned CleanUpMemory();

	void KickOff(int dpnid, KickOffReason reason);
	void Ban(int dpnid);
	void LockSession(bool lock = true);

	void OnSimulate();

	NetworkGameState GetGameState() const;
	NetworkGameState GetServerState() const;
	void SetGameState(NetworkGameState state);
	bool IsGameMaster() const;
	bool IsAdmin() const;
	ConnectionQuality GetConnectionQuality() const;
	void GetParams(float &param1, float &param2) const;
	void SetParams(float param1, float param2);
	const MissionHeader *GetMissionHeader() const;
	int GetPlayer() const;
	int NPlayerRoles() const;
	const PlayerRole *GetPlayerRole(int role) const;
	const PlayerRole *GetMyPlayerRole() const;
	const PlayerIdentity *FindIdentity(int dpnid) const;
	const AutoArray<PlayerIdentity> *GetIdentities() const;
	void AssignPlayer(int role, int player);
	void UnassignPlayer(int role);
	void SelectPlayer(int player, Person *person, bool respawn = false);
	void PlaySound
	(
		RString name, Vector3Par position, Vector3Par speed, 
		float volume, float freq, AbstractWave *wave
	);
	void SoundState(AbstractWave *wave, SoundStateType state);
	NetworkGameState GetPlayerState(int dpid);
	RString GetPlayerName(int dpid);
	Vector3 GetCameraPosition(int dpid);
	bool CreateVehicle(Vehicle *veh, VehicleListType type, RString name, int idVeh);
	bool CreateCenter(AICenter *center);	// whole AI structure
	bool CreateObject(NetworkObject *object);
	void CreateAllObjects();
	void DeleteObject(NetworkId &id);
	void DestroyAllObjects();
	bool CreateCommand(AISubgroup *subgrp, int index, Command *cmd);
	void DeleteCommand(AISubgroup *subgrp, int index, Command *cmd);
	void ClientReady(NetworkGameState state);
	void AskForDammage
	(
		Object *who, EntityAI *owner,
		Vector3Par modelPos, float val, float valRange, RString ammo
	);
	void AskForSetDammage
	(
		Object *who, float dammage
	);
	void AskForGetIn
	(
		Person *soldier, Transport *vehicle,
		GetInPosition position
	);
	void AskForGetOut
	(
		Person *soldier, Transport *vehicle, bool parachute
	);
	void AskForChangePosition
	(
		Person *soldier, Transport *vehicle, UIActionType type
	);
	void AskForAimWeapon
	(
		EntityAI *vehicle, int weapon, Vector3Par dir
	);
	void AskForAimObserver
	(
		EntityAI *vehicle, Vector3Par dir
	);
	void AskForSelectWeapon
	(
		EntityAI *vehicle, int weapon
	);
	void AskForAmmo
	(
		EntityAI *vehicle, int weapon, int burst
	);
	void AskForAddImpulse
	(
		Vehicle *vehicle, Vector3Par force, Vector3Par torque
	);
	void AskForMove
	(
		Object *vehicle, Vector3Par pos
	);
	void AskForMove
	(
		Object *vehicle, Matrix4Par trans
	);
	void AskForJoin
	(
		AIGroup *join, AIGroup *group
	);
	void AskForJoin
	(
		AIGroup *join, OLinkArray<AIUnit> &units
	);
	void AskForHideBody(Person *vehicle);
	void ExplosionDammageEffects
	(
		EntityAI *owner, Shot *shot,
		Object *directHit, Vector3Par pos, Vector3Par dir, const AmmoType *type,
		bool enemyDammage
	);
	void FireWeapon
	(
		EntityAI *vehicle, int weapon, const Magazine *magazine, EntityAI *target
	);
	void UpdateWeapons(EntityAI *vehicle);
	void AddWeaponCargo(VehicleSupply *vehicle, RString weapon);
	void RemoveWeaponCargo(VehicleSupply *vehicle, RString weapon);
	void AddMagazineCargo(VehicleSupply *vehicle, const Magazine *magazine);
	void RemoveMagazineCargo(VehicleSupply *vehicle, int creator, int id);
	void VehicleInit(VehicleInitCmd &init);
	void OnVehicleDestroyed(EntityAI *killed, EntityAI *killer);
	void OnVehicleDamaged(EntityAI *damaged, EntityAI *killer, float damage, RString ammo);
	void OnIncomingMissile(EntityAI *target, RString ammo, EntityAI *owner);
	void MarkerCreate(int channel, AIUnit *sender, RefArray<NetworkObject> &units, ArcadeMarkerInfo &info);
	void MarkerDelete(RString name);
	void SetFlagOwner
	(
		Person *owner, EntityAI *carrier
	);
	void SetFlagCarrier
	(
		Person *owner, EntityAI *carrier
	);
	void SendRadioMessage(NetworkSimpleObject *msg);

	void PublicVariable(RString name);
	void Chat(int channel, RString text);
	void Chat(int channel, AIUnit *sender, RefArray<NetworkObject> &units, RString text);
	void Chat(int channel, RString sender, RefArray<NetworkObject> &units, RString text);
	void RadioChat(int channel, AIUnit *sender, RefArray<NetworkObject> &units, RString text, RadioSentence &sentence);
	void RadioChatWave(int channel, RefArray<NetworkObject> &units, RString wave, AIUnit *sender, RString senderName);
	void SetVoiceChannel(int channel);
	void SetVoiceChannel(int channel, RefArray<NetworkObject> &units);

	void TransferFile(RString dest, RString source);
	void SendMissionFile();
	void GetTransferStats(int &curBytes, int &totBytes);

	void ShowTarget(Person *vehicle, TargetType *target);
	void ShowGroupDir(Person *vehicle, Vector3Par dir);

	void GroupSynchronization(AIGroup *grp, int synchronization, bool active);
	void DetectorActivation(Detector *det, bool active);

	void AskForCreateUnit(AIGroup *group, RString type, Vector3Par position, RString init, float skill, Rank rank);
	void AskForDeleteVehicle(Entity *veh);
	void AskForReceiveUnitAnswer
	(
		AIUnit *from, AISubgroup *to, int answer
	);
	void AskForGroupRespawn(Person *person, EntityAI *killer);
	void AskForActivateMine(Mine *mine, bool activate);
	void AskForInflameFire(Fireplace *fireplace, bool fire);
	void AskForAnimationPhase(Entity *vehicle, RString animation, float phase);
	void CopyUnitInfo(Person *from, Person *to);

	RespawnMode GetRespawnMode() const;
	float GetRespawnDelay() const;
	void Respawn(Person *soldier, Vector3Par pos);

	bool ProcessCommand(RString command);

	void SendKick(int player);
	void SendLockSession(bool lock = true);

	bool CanSelectMission() const;
	bool CanVoteMission() const;
	const AutoArray<RString> &GetServerMissions() const;
	void SelectMission(RString mission, bool cadetMode);
	void VoteMission(RString mission, bool cadetMode);

	void UpdateObject(NetworkObject *object);

	//! Cancel _dp->EnumHost operation
	void StopEnumHosts();
	//! Start _dp->EnumHost operation
	bool StartEnumHosts();

	//! Return original name of local player (without decoration used when several players vith the same name are connected)
	RString GetLocalPlayerName() const;

	Time GetEstimatedEndTime() const;
	void SetEstimatedEndTime(Time time);

	void DisposeBody(Person *body);
	bool IsControlsPaused();
	float GetLastMsgAgeReliable();
};

extern NetworkManager GNetworkManager;
extern NetworkMessageFormat *GMsgFormats[NMTN];

extern RString ServerTmpDir;
RString GetServerTmpDir();

//! total number of diagnostic types
/*!
\patch_internal 1.08 Date 07/26/2001 by Jirka
- Improved: network diagnostics
*/
const int nOutputDiags = 4;
extern int outputDiags;
extern bool outputLogs;

// transfer parameters
//! When update error is bellow this threshold, update message is not sent
extern float MinErrorToSend;
//! Maximal number of messages sent in single simulation step
extern int MaxMsgSend;
//! Maximal number of messages sent in single simulation step on dedicated server
extern int DSMaxMsgSend;
//! Maximal size of agregated guaranteed message
extern int MaxSizeGuaranteed;
//! Maximal size of agregated guaranteed message
extern int MaxSizeNonguaranteed;
//! Limit (minimum) for bandwidth estimation
extern int MinBandwidth;
//! Limit (minimum) for bandwidth estimation on dedicated server
extern int DSMinBandwidth;
//! Limit (maximum) for bandwidth estimation
extern int MaxBandwidth;
//! How many guarenteed message over current bandwith estimation
//! should be passed to low-level network layer if necessary
//! Increasing this value may make mision download faster
extern float ThrottleGuaranteed;

//! == default MinErrorToSend / 2, after 2 s update is forced
#define ERR_COEF_TIME_POSITION				0.005
//! == default MinErrorToSend / 5, after 5 s update is forced
#define ERR_COEF_TIME_GENERIC					0.002

#define MSGID_REPLACE	0

void DiagLogF(const char *format, ...);
void WriteDiagOutput(bool server);
int GetDiagLevel(NetworkMessageType type, bool remote);

#ifdef _WIN32
HRESULT WINAPI ReceiveMessage(void *context, DWORD type, void *message);
#endif
void InitMsgFormats();
unsigned int IntegrityCheckAnswer(
	IntegrityQuestionType type, const IntegrityQuestion &q, bool server = false
);
const char *FormatVal(float val, char *buffer);

RString CreateMPMissionBank(RString filename, RString island);
void RemoveBank(const char *prefix);

int GetNetworkPort();

extern HWND hwndApp;

void DeleteDirectoryStructure(const char *name, bool deleteDir);
void CreatePath(RString path);
bool ParseMission(bool multiplayer);

//! Release COM object
#define SAFE_RELEASE(p)							{ if(p) {(p)->Release(); (p) = NULL;} }

// Multiplayer version info

#include "versionNo.h"

//! Actual multiplayer version
#define MP_VERSION_ACTUAL						APP_VERSION_NUM
//! The earliest supported multiplayer version
#define MP_VERSION_REQUIRED					    MP_VERSION_ACTUAL

//! Integrity question message
/*!
This message is sent by server to clients to check consistency of their data (to avoid cheaters).
*/
struct IntegrityQuestionMessage : public NetworkSimpleObject
{
	//! question unique identifier
	int id;
	//! type of question
	IntegrityQuestionType type;
	//! question itself
	IntegrityQuestion q;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTIntegrityQuestion;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Answer to integrity question message
struct IntegrityAnswerMessage : public NetworkSimpleObject
{
	//! question unique identifier
	int id;
	//! type of question
	IntegrityQuestionType type;
	//! answer value (CRC of selected data)
	int answer;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTIntegrityAnswer;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message used for distribution of players' states to clients
struct PlayerStateMessage : public NetworkSimpleObject
{
	//! DirectPlay ID of player
	int player;
	//! new state of player
	NetworkGameState state;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTPlayerState;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Message for attach person to unit
struct AttachPersonMessage : public NetworkSimpleObject
{
	//! person to attach
	OLink<Person> person;
	//! unit to attach
	OLink<AIUnit> unit;

	//! Constructor
	AttachPersonMessage() {}
	//! Constructor
	/*!
	\param person person which brain is attached to itself
	*/
	AttachPersonMessage(Person *p);
	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTAttachPerson;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! This class encapsulates block of memory and introduces methods for simple transfer from/to them
class SimpleStream : public AutoArray<char>
{
protected:
	//! read / write position
	int _pos;

public:
	//! Constructor
	SimpleStream() {_pos = 0;}

	//! Return read / write position
	int GetPos() const {return _pos;}

	//! Write block of memory
	/*!
	\param buffer data to write
	\param size size of data
	*/
	void Write(const void *buffer, int size)
	{
		int minSize = _pos + size;
		if (Size() < minSize) Resize(minSize);
		memcpy(Data() + _pos, buffer, size);
		_pos += size;
	}
	//! Read block of memory
	/*!
	\param buffer buffer for read data
	\param size size of data to read
	*/
	bool Read(void *buffer, int size)
	{
		int minSize = _pos + size;
		if (Size() < minSize) return false;
		memcpy(buffer, Data() + _pos, size);
		_pos += size;
		return true;
	}
	//! Write string
	/*!
	\param str string to write
	*/
	void WriteString(RString str)
	{
		int size = str.GetLength() + 1;
		int minSize = _pos + size;
		if (Size() < minSize) Resize(minSize);
		memcpy(Data() + _pos, str, size);
		_pos += size;
	}
	//! return read string
	RString ReadString()
	{
		int size = strlen(Data() + _pos) + 1;
		if (Size() < _pos + size) return "";
		RString result = Data() + _pos;
		_pos += size;
		return result;
	}
};

//! Types of Network command messages
enum NetworkCommandMessageType
{
	NCMTLogin,
	NCMTLogged,
	NCMTLogout,
	NCMTKick,
	NCMTRestart,
	NCMTMission,
	NCMTMissions,
	NCMTShutdown,
	NCMTReassign,
	NCMTMonitorAsk,
	NCMTMonitorAnswer,
	NCMTVote,
	NCMTVoteMission,
	NCMTMissionTimeElapsed,
	NCMTAdmin,
	NCMTInit,
	NCMTLockSession,
	NCMTLoggedOut,
	NCMTDebugAsk,
	NCMTDebugAnswer,
};

//! Message for transfer of Network Command
struct NetworkCommandMessage : public NetworkSimpleObject
{
	//! type of command
	NetworkCommandMessageType type;
	//! parameters of command
	SimpleStream content;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTNetworkCommand;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

//! network message indices for PlayerRole class
class IndicesPlayerRole : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int index;
	int side;
	int group;
	int unit;
	int vehicle;
	int position;
	int leader;
	int roleLocked;
	int player;
	//@}

	//! Constructor
	IndicesPlayerRole();
	NetworkMessageIndices *Clone() const {return new IndicesPlayerRole;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for PlayerIdentity class
class IndicesPlayerUpdate : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int dpnid;
	int minPing,avgPing,maxPing;
	int minBandwidth,avgBandwidth,maxBandwidth;
	int desync;
	int rights;
	//@}

	//! Constructor
	IndicesPlayerUpdate();
	NetworkMessageIndices *Clone() const {return new IndicesPlayerUpdate;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for AttachPersonMessage class
class IndicesAttachPerson : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int person;
	int unit;
	//@}

	//! Constructor
	IndicesAttachPerson();
	NetworkMessageIndices *Clone() const {return new IndicesAttachPerson;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for AskForDammageMessage class
class IndicesAskForDammage : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int who;
	int owner;
	int modelPos;
	int val;
	int valRange;
	int ammo;
	//@}

	//! Constructor
	IndicesAskForDammage();
	NetworkMessageIndices *Clone() const {return new IndicesAskForDammage;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for AskForSetDammageMessage class
class IndicesAskForSetDammage : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int who;
	int dammage;
	//@}

	//! Constructor
	IndicesAskForSetDammage();
	NetworkMessageIndices *Clone() const {return new IndicesAskForSetDammage;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for AskForGetInMessage class
class IndicesAskForGetIn : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int soldier;
	int vehicle;
	int position;
	//@}

	//! Constructor
	IndicesAskForGetIn();
	NetworkMessageIndices *Clone() const {return new IndicesAskForGetIn;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for AskForGetOutMessage class
class IndicesAskForGetOut : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int soldier;
	int vehicle;
	int parachute;
	//@}

	//! Constructor
	IndicesAskForGetOut();
	NetworkMessageIndices *Clone() const {return new IndicesAskForGetOut;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for AskForChangePositionMessage class
class IndicesAskForChangePosition : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int soldier;
	int vehicle;
	int type;
	//@}

	//! Constructor
	IndicesAskForChangePosition();
	NetworkMessageIndices *Clone() const {return new IndicesAskForChangePosition;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for AskForAimWeaponMessage class
class IndicesAskForAimWeapon : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int vehicle;
	int weapon;
	int dir;
	//@}

	//! Constructor
	IndicesAskForAimWeapon();
	NetworkMessageIndices *Clone() const {return new IndicesAskForAimWeapon;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for AskForAimObserverMessage class
class IndicesAskForAimObserver : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int vehicle;
	int dir;
	//@}

	//! Constructor
	IndicesAskForAimObserver();
	NetworkMessageIndices *Clone() const {return new IndicesAskForAimObserver;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for AskForSelectWeaponMessage class
class IndicesAskForSelectWeapon : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int vehicle;
	int weapon;
	//@}

	//! Constructor
	IndicesAskForSelectWeapon();
	NetworkMessageIndices *Clone() const {return new IndicesAskForSelectWeapon;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for AskForAddImpulseMessage class
class IndicesAskForAddImpulse : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int vehicle;
	int force;
	int torque;
	//@}

	//! Constructor
	IndicesAskForAddImpulse();
	NetworkMessageIndices *Clone() const {return new IndicesAskForAddImpulse;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for AskForAmmoMessage class
class IndicesAskForAmmo : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int vehicle;
	int weapon;
	int burst;
	//@}

	//! Constructor
	IndicesAskForAmmo();
	NetworkMessageIndices *Clone() const {return new IndicesAskForAmmo;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for AskForMoveVectorMessage class
class IndicesAskForMoveVector : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int vehicle;
	int pos;
	//@}

	//! Constructor
	IndicesAskForMoveVector();
	NetworkMessageIndices *Clone() const {return new IndicesAskForMoveVector;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for AskForMoveMatrixMessage class
class IndicesAskForMoveMatrix : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int vehicle;
	int pos;
	int orient;
	//@}

	//! Constructor
	IndicesAskForMoveMatrix();
	NetworkMessageIndices *Clone() const {return new IndicesAskForMoveMatrix;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for AskForJoinGroupMessage class
class IndicesAskForJoinGroup : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int join;
	int group;
	//@}

	//! Constructor
	IndicesAskForJoinGroup();
	NetworkMessageIndices *Clone() const {return new IndicesAskForJoinGroup;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for AskForJoinUnitsMessage class
class IndicesAskForJoinUnits : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int join;
	int units;
	//@}

	//! Constructor
	IndicesAskForJoinUnits();
	NetworkMessageIndices *Clone() const {return new IndicesAskForJoinUnits;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for AskForHideBodyMessage class
class IndicesAskForHideBody : public NetworkMessageIndices
{
public:
	//! index of field in message format
	int vehicle;

	//! Constructor
	IndicesAskForHideBody();
	NetworkMessageIndices *Clone() const {return new IndicesAskForHideBody;}
	void Scan(NetworkMessageFormatBase *format);
};

class IndicesUpdateEntityAIWeapons;

//! network message indices for UpdateWeaponsMessage class
class IndicesUpdateWeapons : public NetworkMessageIndices
{
public:
	//! index of field in message format
	int vehicle;
	//! indices of fields in message format
	IndicesUpdateEntityAIWeapons *weapons;

	//! Constructor
	IndicesUpdateWeapons();
	//! Destructor
	~IndicesUpdateWeapons();
	NetworkMessageIndices *Clone() const {return new IndicesUpdateWeapons;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for SetFlagOwnerMessage and SetFlagCarrierMessage classes
class IndicesSetFlagOwner : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int owner;
	int carrier;
	//@}

	//! Constructor
	IndicesSetFlagOwner();
	NetworkMessageIndices *Clone() const {return new IndicesSetFlagOwner;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for DeleteObjectMessage class
class IndicesDeleteObject : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int creator;
	int id;
	//@}

	//! Constructor
	IndicesDeleteObject();
	NetworkMessageIndices *Clone() const {return new IndicesDeleteObject;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for DeleteCommandMessage class
class IndicesDeleteCommand : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int creator;
	int id;
	int subgrp;
	int index;
	//@}

	//! Constructor
	IndicesDeleteCommand();
	NetworkMessageIndices *Clone() const {return new IndicesDeleteCommand;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for ChatMessage class
class IndicesChat : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int channel;
	int sender;
	int units;
	int name;
	int text;
	//@}

	//! Constructor
	IndicesChat();
	NetworkMessageIndices *Clone() const {return new IndicesChat;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for RadioChatMessage class
class IndicesRadioChat : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int channel;
	int sender;
	int units;
	int text;
	int sentence;
	//@}

	//! Constructor
	IndicesRadioChat();
	NetworkMessageIndices *Clone() const {return new IndicesRadioChat;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for RadioChatWaveMessage class
class IndicesRadioChatWave : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int channel;
	int units;
	int wave;
	int sender;
	int senderName;
	//@}

	//! Constructor
	IndicesRadioChatWave();
	NetworkMessageIndices *Clone() const {return new IndicesRadioChatWave;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for SetVoiceChannelMessage class
class IndicesSetVoiceChannel : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int channel;
	int units;
	//@}

	//! Constructor
	IndicesSetVoiceChannel();
	NetworkMessageIndices *Clone() const {return new IndicesSetVoiceChannel;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for MarkerCreateMessage class
class IndicesMarkerCreate : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int channel;
	int sender;
	int units;
	IndicesMarker *marker;
	//@}

	//! Constructor
	IndicesMarkerCreate();
	~IndicesMarkerCreate();
	NetworkMessageIndices *Clone() const {return new IndicesMarkerCreate;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for ShowTargetMessage class
class IndicesShowTarget : public NetworkMessageIndices
{
public:
	//! index of field in message format
	//@{
	int vehicle;
	int target;
	//@}

	//! Constructor
	IndicesShowTarget();
	NetworkMessageIndices *Clone() const {return new IndicesShowTarget;}
	void Scan(NetworkMessageFormatBase *format);
};

//! network message indices for ShowGroupDirMessage class
class IndicesShowGroupDir : public NetworkMessageIndices
{
public:
	//! index of field in message format
	//@{
	int vehicle;
	int dir;
	//@}

	//! Constructor
	IndicesShowGroupDir();
	NetworkMessageIndices *Clone() const {return new IndicesShowGroupDir;}
	void Scan(NetworkMessageFormatBase *format);
};

inline bool NetworkClient::IsBotClient() const
{
	return _parent->IsServer();
}

#endif
