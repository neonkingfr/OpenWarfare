#ifdef _MSC_VER
#pragma once
#endif

#ifndef _THREAD_SYNC_HPP
#define _THREAD_SYNC_HPP

//#include "multisync.hpp"

typedef void BackgroundFunction( void *context );

/*
class SecondaryThread
{
	Event _terminate;

	HANDLE _secondary;
	HANDLE _primary;
	DWORD _secondaryAllowedUntil;
	BackgroundFunction *_secondaryFunction;
	void *_secondaryContext;
	Event _secondaryDone;
	Event _secondaryStarted;

	private:
	void SelectPrimary();
	void SelectSecondary();
			
	public:
	// interface used from primary thread
	SecondaryThread();
	~SecondaryThread();

	void StartSecondaryBackground();

	void StartSecondary( BackgroundFunction *function, void *context );
	void RunSecondary( DWORD ms );
	void FinishSecondary();

	// interface used from secondary thread
	void AllowSwitch(); // give primary opportunity to continue
	void SecondaryIsFinished(); // no longer necessary to run - waits for next frame
};
*/

#endif
