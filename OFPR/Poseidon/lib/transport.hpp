#ifdef _MSC_VER
#pragma once
#endif

#ifndef _TRANSPORT_HPP
#define _TRANSPORT_HPP

/*!
\file
interface for Transport class
*/

#include "vehicleAI.hpp"
#include "lights.hpp"
#include "pilotHead.hpp"
#include "speaker.hpp"


struct ManCargoItem
{
	Ref<Person> man;
	//float animTime;
};
TypeIsMovableZeroed(ManCargoItem)

//typedef RefArray<Person> ManCargo;
class ManCargo : public AutoArray<ManCargoItem>
{
	typedef AutoArray<ManCargoItem> base;
public:
	Person *operator [](int i) const
	{return base::operator [](i).man;}
};

class TurretType
{
	public:

	friend class Turret;
	float _minElev,_maxElev; // gun movement
	float _minTurn,_maxTurn; // turret movement
	SoundPars _servoSound;

	int _xAxisIndex; // index in memory
	int _yAxisIndex;

	Vector3 _yAxis; // rotate around this point
	Vector3 _xAxis;

	Vector3 _pos,_dir; // gun position and direction
	float _neutralXRot; // initial turret position (in model)
	float _neutralYRot; // initial turret position (in model)
	Animation _body,_gun;

	TurretType();
	void Load(const ParamEntry &cfg);
	void InitShape(const ParamEntry &cfg, LODShape *shape);

	int GetBodySelection(int level) const {return _body.GetSelection(level);}
	int GetGunSelection(int level) const {return _gun.GetSelection(level);}
};

// turret/gun hierarchy
// each turret/gun has center of rotation
// relative to his parent

class Turret
{
	public:
	float _yRot,_yRotWanted;
	float _xRot,_xRotWanted;
	float _xSpeed,_ySpeed;


	Ref<AbstractWave> _servoSound;
	float _servoVol;
	bool _gunStabilized; // compensate any orientation changes

	Turret();

	void UnloadSound();
	void Sound
	(
		const TurretType &type, bool inside, float deltaT,
		FrameBase &pos, Vector3Val speed // parent position
	);

	void MoveWeapons(const TurretType &type, AIUnit *unit, float deltaT );
	void Stop(const TurretType &type);
	void GunBroken(const TurretType &type);
	void TurretBroken(const TurretType &type);

	void Stabilize
	(
		const Object *obj,
		const TurretType &type, Matrix3Val oldTrans, Matrix3Val newTrans
	);
	bool Aim(const TurretType &type, Vector3Val relDir);
	Matrix3 GetAimWanted() const;

	LSError Serialize(ParamArchive &ar);
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx);

	Matrix4 TurretTransform(const TurretType &type) const;
	Matrix4 GunTransform(const TurretType &type) const;

	Vector3 GetCenter(const TurretType &type) const;

	void Animate
	(
		const TurretType &type, const Object *object, int level
	);
	void Deanimate(const TurretType &type, LODShape *shape, int level);

	void AnimatePoint
	(
		const TurretType &type, Vector3 &pos,
		const Object *obj, int level, int selection
	) const;
	void AnimateMatrix
	(
		const TurretType &type, Matrix4 &mat, const Object *obj, int level, int selection
	) const;
};

class Hatch
{
protected:
	AnimationRotation _animation;
	float _openAngle;

public:
	int GetSelection(int level) const {return _animation.GetSelection(level);}
	Hatch();
	void Init(LODShape *shape, const ParamEntry &par);
	void Open(Matrix4Par parent, LODShape *shape, int level, float value) const;
	void Restore(LODShape *shape, int level) const;
};

enum RadioMessageVehicleType
{
	RMTFirstVehicle=0x100,
	RMTVehicleMove=RMTFirstVehicle,
	RMTVehicleFire,
	RMTVehicleFormation,
	RMTVehicleCeaseFire,
	RMTVehicleSimpleCommand,
	RMTVehicleTarget,
	RMTVehicleLoad,
	RMTVehicleAzimut,
	RMTVehicleStopTurning,
	RMTVehicleFireFailed,
};

RadioMessage *CreateVehicleMessage(int type);

class IndicesVMessage : public NetworkMessageIndices
{
public:
	int vehicle;

	IndicesVMessage();
	void Scan(NetworkMessageFormatBase *format);
};

class RadioMessageWithTarget: public RadioMessage, public NetworkSimpleObject
{
protected:
	OLink<Transport> _vehicle;
	LinkTarget _target;

public:
	RadioMessageWithTarget() {}
	RadioMessageWithTarget( Transport *vehicle, Target *target );
	LSError Serialize(ParamArchive &ar);
	AIUnit *GetSender() const;

	Target *GetTarget() const {return _target;}
	void SetTarget(Target *target) {_target = target;}

	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

class RadioMessageVFire: public RadioMessageWithTarget
{
	typedef RadioMessageWithTarget base;
public:
	RadioMessageVFire() {}
	RadioMessageVFire( Transport *vehicle, Target *target );
	//LSError Serialize(ParamArchive &ar);
	void Transmitted();
	const char *GetPriorityClass() {return "UrgentCommand";}
	int GetType() const {return RMTVehicleFire;}
	//AIUnit *GetSender() const;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	//static NetworkMessageFormat &CreateFormat
	//(
	//	NetworkMessageClass cls,
	//	NetworkMessageFormat &format
	//);
	//TMError TransferMsg(NetworkMessageContext &ctx);

	void Send();	// send itself to radio protocol

protected:
	const char *PrepareSentence(SentenceParams &params);
};

class RadioMessageVTarget: public RadioMessageWithTarget
{
	typedef RadioMessageWithTarget base;

public:
	RadioMessageVTarget() {}
	RadioMessageVTarget( Transport *vehicle, Target *target );
	//LSError Serialize(ParamArchive &ar);
	void Transmitted();
	const char *GetPriorityClass() {return "UrgentCommand";}
	int GetType() const {return RMTVehicleTarget;}
	//AIUnit *GetSender() const;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	//static NetworkMessageFormat &CreateFormat
	//(
	//	NetworkMessageClass cls,
	//	NetworkMessageFormat &format
	//);
	//TMError TransferMsg(NetworkMessageContext &ctx);

	void Send();	// send itself to radio protocol

protected:
	const char *PrepareSentence(SentenceParams &params);
};

class RadioMessageVMove: public RadioMessage, public NetworkSimpleObject
{
protected:
	OLink<Transport> _vehicle;
	Vector3 _destination;

public:
	RadioMessageVMove() {}
	RadioMessageVMove( Transport *vehicle, Vector3Par pos );
	LSError Serialize(ParamArchive &ar);
	void Transmitted();
	const char *GetPriorityClass() {return "NormalCommand";}
	int GetType() const {return RMTVehicleMove;}
	AIUnit *GetSender() const;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);

	void Send();	// send itself to radio protocol

protected:
	const char *PrepareSentence(SentenceParams &params);
};

class RadioMessageVAzimut: public RadioMessage, public NetworkSimpleObject
{
protected:
	OLink<Transport> _vehicle;
	float _azimut;

public:
	RadioMessageVAzimut() {}
	RadioMessageVAzimut(Transport *vehicle, float azimut);
	LSError Serialize(ParamArchive &ar);
	void Transmitted();
	const char *GetPriorityClass() {return "NormalCommand";}
	int GetType() const {return RMTVehicleAzimut;}
	AIUnit *GetSender() const;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);

	void Send();	// send itself to radio protocol

protected:
	const char *PrepareSentence(SentenceParams &params);
};

enum SimpleCommand
{
	SCForward,
	SCStop,
	SCBackward,
	SCFaster,
	SCSlower,
	SCLeft,
	SCRight,
	SCFire,
	SCCeaseFire,
	SCKeyUp,
	SCKeyDown,
	SCKeySlow,
	SCKeyFast,
	SCKeyFire,
};

class RadioMessageVSimpleCommand: public RadioMessage, public NetworkSimpleObject
{
protected:
	OLink<Transport> _vehicle;
	SimpleCommand _cmd;

public:
	RadioMessageVSimpleCommand() {}
	RadioMessageVSimpleCommand( Transport *vehicle, SimpleCommand cmd );
	LSError Serialize(ParamArchive &ar);
	void Transmitted();
	const char *GetPriorityClass()
	{
		return _cmd == SCFire || _cmd == SCCeaseFire || _cmd == SCKeyFire ? "UrgentCommand" : "NormalCommand";
	}
	int GetType() const {return RMTVehicleSimpleCommand;}
	SimpleCommand GetCommand() const {return _cmd;}
	AIUnit *GetSender() const;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);

	void Send();	// send itself to radio protocol

protected:
	const char *PrepareSentence(SentenceParams &params);
};

class RadioMessageVStopTurning: public RadioMessage, public NetworkSimpleObject
{
protected:
	OLink<Transport> _vehicle;
	float _azimut;

public:
	RadioMessageVStopTurning() {}
	RadioMessageVStopTurning(Transport *vehicle, float azimut);
	LSError Serialize(ParamArchive &ar);
	void Transmitted();
	const char *GetPriorityClass() {return "NormalCommand";}
	int GetType() const {return RMTVehicleStopTurning;}
	AIUnit *GetSender() const;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);

	void Send();	// send itself to radio protocol

protected:
	const char *PrepareSentence(SentenceParams &params);
};

class RadioMessageVFormation:  public RadioMessage, public NetworkSimpleObject
{
protected:
	OLink<Transport> _vehicle;

public:
	RadioMessageVFormation() {}
	RadioMessageVFormation( Transport *vehicle );
	LSError Serialize(ParamArchive &ar);
	void Transmitted();
	const char *GetPriorityClass() {return "NormalCommand";}
	int GetType() const {return RMTVehicleFormation;}
	AIUnit *GetSender() const;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);

	void Send();	// send itself to radio protocol

protected:
	const char *PrepareSentence(SentenceParams &params);
};

// obsolete
class RadioMessageVCeaseFire:  public RadioMessage
{
protected:
	OLink<Transport> _vehicle;

public:
	RadioMessageVCeaseFire() {}
	RadioMessageVCeaseFire( Transport *vehicle );
	LSError Serialize(ParamArchive &ar);
	void Transmitted();
	const char *GetPriorityClass() {return "UrgentCommand";}
	int GetType() const {return RMTVehicleCeaseFire;}
	AIUnit *GetSender() const;

protected:
	const char *PrepareSentence(SentenceParams &params);
};

class RadioMessageVLoad: public RadioMessage, public NetworkSimpleObject
{
protected:
	OLink<Transport> _vehicle;
	int _weapon;

public:
	RadioMessageVLoad() {}
	RadioMessageVLoad(Transport *vehicle, int weapon);
	LSError Serialize(ParamArchive &ar);
	void Transmitted();
	const char *GetPriorityClass() {return "UrgentCommand";}
	int GetType() const {return RMTVehicleLoad;}
	int GetWeapon() const {return _weapon;}
	void SetWeapon(int weapon) {_weapon = weapon;}
	AIUnit *GetSender() const;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);

	void Send();	// send itself to radio protocol

protected:
	const char *PrepareSentence(SentenceParams &params);
};

class RadioMessageVFireFailed: public RadioMessage, public NetworkSimpleObject
{
protected:
	OLink<Transport> _vehicle;

public:
	RadioMessageVFireFailed() {}
	RadioMessageVFireFailed(Transport *vehicle);
	LSError Serialize(ParamArchive &ar);
	void Transmitted();
	const char *GetPriorityClass() {return "Failure";}
	int GetType() const {return RMTVehicleFireFailed;}
	AIUnit *GetSender() const;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);

	void Send();	// send itself to radio protocol

protected:
	const char *PrepareSentence(SentenceParams &params);
};

#include "pathPlanner.hpp"
	
enum VehicleMoveMode
{
	VMMFormation,
	VMMMove,
	VMMBackward,
	VMMStop,
	VMMSlowForward,
	VMMForward,
	VMMFastForward,
};

enum VehicleTurnMode
{
	VTMNone,
	VTMLeft,
	VTMRight,
	VTMAbs,
};

typedef RStringB ManAnimationType;

struct ManProxy
{
	Matrix4 transform;
	int selection; // copied over from proxyObject

	ManProxy();
	bool Present() const {return selection>=0;}
};

TypeIsMovable(ManProxy)

struct LevelProxies
{
	ManProxy _driverProxy; // positioning of different crew members
	ManProxy _gunnerProxy;
	ManProxy _commanderProxy;
	AutoArray<ManProxy> _cargoProxy;
};

class TransportType: public VehicleType
{
	friend class Transport;

	typedef VehicleType base;

	LevelProxies _proxies[MAX_LOD_LEVELS];
	RString _crew;
	ManAction _getInAction;
	ManAction _getOutAction;


	//@{
	//!Action id corresponding to crew position
	// \note: crew actions correspond to ActionVehMap, not ActionMap
	ManVehAction _driverAction;
	ManVehAction _gunnerAction;
	ManVehAction _commanderAction;

	ManVehAction _driverInAction; // actions inside vehicle (turn in)
	ManVehAction _gunnerInAction;
	ManVehAction _commanderInAction;
	//@}

	bool _gunnerUsesPilotView;
	bool _commanderUsesPilotView;

	bool _castDriverShadow;
	bool _castGunnerShadow;
	bool _castCommanderShadow;
	bool _castCargoShadow;

	bool _ejectDeadDriver;
	bool _ejectDeadGunner;
	bool _ejectDeadCommander;
	bool _ejectDeadCargo;

	bool _hideWeaponsDriver;
	bool _hideWeaponsGunner;
	bool _hideWeaponsCommander;
	bool _hideWeaponsCargo;

	AutoArray<ManVehAction> _cargoAction;
	AutoArray<bool> _cargoCoDriver;

	AutoArray<Vector3> _commanderGetInPos;
	AutoArray<Vector3> _driverGetInPos;
	AutoArray<Vector3> _gunnerGetInPos;
	AutoArray<Vector3> _cargoGetInPos;
	AutoArray<Vector3> _coDriverGetInPos;
	float _getInRadius;


	float _insideSoundCoef;

	bool _hideProxyInCombat;
	bool _forceHideGunner;
	bool _forceHideDriver;
	bool _forceHideCommander;

	bool _outGunnerMayFire;
	bool _viewGunnerInExternal;
	bool _unloadInCombat;

	bool _hasDriver;
	bool _hasGunner;
	bool _hasCommander;
	// if there is no commander, commander may be driver or gunner
	bool _driverIsCommander;

	// _viewDriver is member of VehicleType
	ViewPars _viewCargo;
	ViewPars _viewGunner;
	ViewPars _viewCommander;
	ViewPars _viewOptics;

	int _driverOpticsPos;
	int _gunnerOpticsPos;
	int _commanderOpticsPos;

	Ref<LODShapeWithShadow> _driverOpticsModel;
	Ref<LODShapeWithShadow> _gunnerOpticsModel;
	Ref<LODShapeWithShadow> _commanderOpticsModel;

	RefArray<VehicleType> _typicalCargo;

	PackedColor _driverOpticsColor;
	PackedColor _gunnerOpticsColor;
	PackedColor _commanderOpticsColor;

	int _maxManCargo;

	void AddProxy
	(
		ManProxy &mProxy, const ProxyObject &proxy, ManAnimationType anim
	);

public:
	TransportType( const ParamEntry *param );
	virtual void Load(const ParamEntry &par);
	void InitShape();
	void DeinitShape(); // before shape is unloaded

	ManAction GetGetInAction() const {return _getInAction;}
	ManAction GetGetOutAction() const {return _getOutAction;}

	ManVehAction GetDriverAction() const {return _driverAction;}
	ManVehAction GetGunnerAction() const {return _gunnerAction;}
	ManVehAction GetCommanderAction() const {return _commanderAction;}

	ManVehAction GetCargoAction(int pos) const {return _cargoAction[pos];}
	bool GetCargoIsCoDriver(int pos) const {return _cargoCoDriver[pos];}

	ManVehAction GetDriverInAction() const {return _driverInAction;}
	ManVehAction GetGunnerInAction() const {return _gunnerInAction;}
	ManVehAction GetCommanderInAction() const {return _commanderInAction;}

	bool GetOutGunnerMayFire() const {return _outGunnerMayFire;}
	bool GetViewGunnerInExternal() const {return _viewGunnerInExternal;}
	
	bool GetUnloadInCombat() const {return _unloadInCombat;}

	__forceinline int NCommanderGetInPos() const {return _commanderGetInPos.Size();}
	__forceinline int NDriverGetInPos() const {return _driverGetInPos.Size();}
	__forceinline int NGunnerGetInPos() const {return _gunnerGetInPos.Size();}
	__forceinline int NCargoGetInPos() const {return _cargoGetInPos.Size();}
	__forceinline int NCoDriverGetInPos() const {return _coDriverGetInPos.Size();}

	__forceinline Vector3Val GetCommanderGetInPos(int i) const {return _commanderGetInPos[i];}
	__forceinline Vector3Val GetDriverGetInPos(int i) const {return _driverGetInPos[i];}
	__forceinline Vector3Val GetGunnerGetInPos(int i) const {return _gunnerGetInPos[i];}
	__forceinline Vector3Val GetCargoGetInPos(int i) const {return _cargoGetInPos[i];}
	__forceinline Vector3Val GetCoDriverGetInPos(int i) const {return _coDriverGetInPos[i];}

	__forceinline float GetGetInRadius() const {return _getInRadius;}

	__forceinline int GetMaxManCargo() const {return _maxManCargo;}

	bool HasDriver() const {return _hasDriver;}
	bool HasGunner() const {return _hasGunner;}
	bool HasCommander() const {return _hasCommander;}
	bool HasCargo() const {return _maxManCargo>0;}
	__forceinline bool DriverIsCommander() const {return _driverIsCommander;}

	virtual Threat GetStrategicThreat( float distance2, float visibility, float cosAngle ) const;
	virtual Threat GetDammagePerMinute
	(
		float distance2, float visibility, EntityAI *vehicle=NULL
	) const;

	RString GetCrew() const {return _crew;}
};

typedef TransportType VehicleTransportType;

DECL_ENUM(LockState)

class IndicesUpdateTransport : public IndicesUpdateVehicleSupply
{
	typedef IndicesUpdateVehicleSupply base;

public:
	int commander;
	int driver;
	int gunner;
	int effCommander;
	int manCargo;
	int comFireTarget;
	int lock;
	int driverHiddenWanted;
	int fuel;
	int engineOff;

	IndicesUpdateTransport();
	NetworkMessageIndices *Clone() const {return new IndicesUpdateTransport;}
	void Scan(NetworkMessageFormatBase *format);
};

//! base class for all vehicles capable of hosting crew
/*!
	all common stuff related to transporting personel or cargo 
	is implemented in this class	
*/

class Transport: public VehicleSupply
{
	typedef VehicleSupply base;

public:
	enum LandingMode
	{
		LMNone,
		LMLand,
		LMGetIn,
		LMGetOut,
	};

protected:
	Vector3 _driverPos; // commander gave command (VZero->invalid)
	Vector3 _dirWanted;
	VehicleMoveMode _moveMode;
	VehicleTurnMode _turnMode;
	bool _fireEnabled;
	bool _showDmg;
	//! vehicle commander has direct control over the weapons
	bool _manualFire;
	//! vehicle engine is off
	bool _engineOff;

	//! fuel tank state
	float _fuel;

	//! when we collide with some hard object, we revert turret to neutral position
	//! so that we can get out of the crash
	Time _turretFrontUntil;

	LockState _lock;

	LandingMode _landing;

	float _azimutWanted;
	
	float _randomizer;

	Vector3 _mouseDirWanted; // driver mouse controls 

	Ref<Person> _driver; // store driver handle (when in)
	Ref<Person> _gunner; // store gunner handle (when in)
	Ref<Person> _commander; // store commander handle ("observer")
	OLink<Person> _effCommander; // who is giving commands
	
	Link<AbstractWave> _doorSound;
	Ref<AbstractWave> _engineSound,_envSound;
	Ref<AbstractWave> _dmgSound;

	// commander must remember his decision
	FireDecision _commanderFire;

	float _commanderHidden;
	float _driverHidden;
	float _gunnerHidden;
	float _commanderHiddenWanted;
	float _driverHiddenWanted;
	float _gunnerHiddenWanted;

	PilotHead _head; // head movement simulation

	ManCargo _manCargo; // transported soldiers

	// crash sound parameters
	enum CrashType {CrashNone,CrashLand,CrashWater,CrashObject};
	CrashType _doCrash;
	float _crashVolume;
	// time of last crash sound (will not repeat for some time)
	Time _timeCrash;
	//float _crewDammage; // how much the crew is affected by dammage
	Time _getOutAfterDammage;
	Time _explosionTime; // vehicle is going to explode

	// assined to ...
	OLink<AIGroup> _groupAssigned;
	OLink<AIUnit> _driverAssigned;
	OLink<AIUnit> _gunnerAssigned;
	OLink<AIUnit> _commanderAssigned;
	OLinkArray<AIUnit> _cargoAssigned;

	mutable OLinkArray<AIUnit> _getoutUnits;
	mutable OLinkArray<AIUnit> _getinUnits;
	mutable Time _getinTime; // get-in time out
	mutable Time _getoutTime; // next get out allowed (door free)

	RadioChannel _radio;

	RefArray<LightPointOnVehicle> _markers;
	RefArray<LightPointOnVehicle> _markersBlink;
	Time _markersOn;

	Time _showDmgValid;

	//! consume some fuel (used during simulation), return false if tank is empty
	bool ConsumeFuel(float ammount);

	public:
	Transport(VehicleType *name, Person *driver, bool fullCreate=true);

	const TransportType *Type() const
	{
		return static_cast<const TransportType *>(GetType());
	}
	
	void Simulate( float deltaT, SimulationImportance prec );

	float GetFuel() const {return _fuel;}
	void Refuel(float ammount);

	void PlaceOnSurface(Matrix4 &trans);

	virtual Vector3 GetCameraDirection( CameraType camType ) const;
	virtual Matrix4 InsideCamera( CameraType camType ) const;
	virtual int InsideLOD( CameraType camType ) const;

	CursorMode GetCursorRelMode(CameraType camType) const;
	bool IsVirtual( CameraType camType ) const;
	bool IsVirtualX( CameraType camType ) const;

	virtual bool IsGunner( CameraType camType ) const;

	void LimitCursor(CameraType camType, Vector3 &dir) const;

	virtual void LimitVirtual
	(
		CameraType camType, float &heading, float &dive, float &fov
	) const;
	virtual void InitVirtual
	(
		CameraType camType, float &heading, float &dive, float &fov
	) const;

	void EngineOn();
	void EngineOff();
	bool EngineIsOn() const {return !_engineOff;}
	//! some transport types may have no engine or may have it always on
	bool EngineCanBeOff() const {return true;}

	void Init( Matrix4Par pos );
	void InitUnits();

	bool ValidateCrew(Person *crew, bool complex=true) const;
	bool Validate(bool complex=true) const;
	virtual void TrackTargets
	(
		TargetList &res, bool initialize, float trackTargetsPeriod
	);

	void SetDammage(float dammage);
	void ReactToDammage();

	//! eject given crew member
	bool EjectCrew(Person *man);
	//! eject given crew member if he is dead
	bool EjectIfDead(Person *man);
	//! eject given crew member if he is alive
	bool EjectIfAlive(Person *man);
	//! eject all crew member that are not fixed
	void EjectAllNotFixed();

	bool SimulateUnits( float deltaT );	// false if no pilot
	void CrashDammage( float ammount, const Vector3 &pos=VZero );

	virtual void SelectWeaponCommander(AIUnit *unit, int weapon);

#if _ENABLE_AI
	virtual void AICommander(AIUnit *unit, float deltaT);
	virtual void AIPilot(AIUnit *unit, float deltaT) {}
	virtual void AIGunner(AIUnit *unit, float deltaT);
#endif

	virtual void KeyboardPilot(AIUnit *unit, float deltaT) {}
	virtual void SuspendedPilot(AIUnit *unit, float deltaT) {}
	virtual void KeyboardAny(AIUnit *unit, float deltaT);
	// KeyboardCommander and KeyboardGunner are realized in InGameUI

	virtual void AimDriver(Vector3Par direction);

	virtual	void FakePilot( float deltaT ) {};

	// prepare for drawing
	bool IsAnimated( int level ) const; // appearence changed with Animate
	void Animate( int level );
	void Deanimate( int level );

	virtual void AnimateManProxyMatrix
	(
		int level, const ManProxy &proxy, Matrix4 &proxyTransform
	) const;
	
	bool IsManualFire() const {return _manualFire;}

	// TODO: remove
	bool IsProxyHidden() const {return IsCommanderHidden();}

	bool IsCommanderHidden() const {return _commanderHidden > 0.5;}
	bool IsDriverHidden() const {return _driverHidden > 0.5;}
	bool IsGunnerHidden() const {return _gunnerHidden > 0.5;}
	bool IsPersonHidden(Person *person) const;
	void HideCommander(float hide = 1.0) {_commanderHiddenWanted = hide;}
	void HideDriver(float hide = 1.0) {_driverHiddenWanted = hide;}
	void HideGunner(float hide = 1.0) {_gunnerHiddenWanted = hide;}
	void HidePerson(Person *person, float hide = 1.0);

	//@{
	//!Get action corresponding to given position in vehicle
	virtual ManVehAction DriverAction() const;
	virtual ManVehAction CommanderAction() const;
	virtual ManVehAction GunnerAction() const;
	virtual ManVehAction CargoAction(int position) const;

	virtual float DriverAnimSpeed() const;
	virtual float CommanderAnimSpeed() const;
	virtual float GunnerAnimSpeed() const;
	virtual float CargoAnimSpeed(int position) const;
	//@}


	virtual LODShapeWithShadow *GetOpticsModel(Person *person);
	virtual PackedColor GetOpticsColor(Person *person);
	virtual bool GetForceOptics(Person *person) const; 

	virtual void DrawDiags();
	void DrawCameraCockpit();

	// draw single crew member
	void DrawCrewMember
	(
		int level,
		ClipFlags clipFlags,
		const Matrix4 &transform, const Matrix4 &invTransform,
		float dist2, float z2, const LightList &lights,
		const ManProxy &proxy, Person *man,
		bool hideWeapons
	);
	int GetCrewMemberComplexity
	(
		int level, const FrameBase &pos, float dist2,
		const ManProxy &proxy, Person *man
	) const;
	// draw crew
	void DrawProxies
	(
		int level, ClipFlags clipFlags,
		const Matrix4 &transform, const Matrix4 &invTransform,
		float dist2, float z2, const LightList &lights
	);
	virtual int GetProxyComplexity
	(
		int level, const FrameBase &pos, float dist2
	) const;
	Vector3 FindMissilePos(int index, bool &found) const;

	int PassNum( int lod );
	void Draw( int level, ClipFlags clipFlags, const FrameBase &pos );

	// proxy access
	virtual bool CastProxyShadow(int level, int index) const;

	virtual int GetProxyCount(int level) const;
	virtual Object *GetProxy
	(
		LODShapeWithShadow *&shape,
		int level,
		Matrix4 &transform, Matrix4 &invTransform,
		const FrameBase &parentPos, int i
	) const;

	virtual Matrix4 ProxyWorldTransform(const Object *obj) const;
	virtual Matrix4 ProxyInvWorldTransform(const Object *obj) const;


	Texture *GetFlagTexture();

//	Vector3 GetCameraPosition(CameraType camType) const;
	virtual bool GetOpticsCamera(Matrix4 &transf, CameraType camType) const; // returns true if transf ok
	bool GetProxyCamera(Matrix4 &transf, CameraType camType) const; // returns true if transf ok

	void PerformFF( FFEffects &effects );

	void Sound( bool inside, float deltaT );
	void UnloadSound();

	virtual float GetEngineVol( float &freq ) const {freq=1;return 0;}
	virtual float GetEnvironVol( float &freq ) const {freq=1;return 0;}
	virtual Vector3 GetEnginePos() const; // sound source position in model coord
	virtual Vector3 GetEnvironPos() const; // sound source position in model coord
		
	AIUnit *DriverBrain() const;
	AIUnit *GunnerBrain() const;
	AIUnit *CommanderBrain() const;

	virtual AIUnit *ObserverUnit() const;
	virtual AIUnit *CommanderUnit() const;
	virtual AIUnit *PilotUnit() const;
	virtual AIUnit *GunnerUnit() const;
	virtual AIUnit *EffectiveGunnerUnit() const;

	TargetSide GetTargetSide() const;

	float VisibleLights() const; // visibility

	bool QIsManual() const;
	bool QIsManual(const AIUnit *unit) const;

	RadioChannel &GetRadio() {return _radio;}
	const RadioChannel &GetRadio() const {return _radio;}

	VehicleMoveMode GetMoveMode() {return _moveMode;}
	bool IsFireEnabled();

//	bool IsLocked() const {return _locked;}
//	void SetLocked(bool lock) {_locked = lock;}
	LockState GetLock() const {return _lock;}
	void SetLock(LockState lock) {_lock = lock;}

	virtual bool StopAtStrategicPos() const;

#if _ENABLE_AI
	virtual void FormationPilot( float &speedWanted, float &headChange, float &turnPredict );
	virtual void LeaderPilot( float &speedWanted, float &headChange, float &turnPredict );
	// void PathPilot(float &speedWanted, float &headChange, float &turnPredict, float speedCoef = 1.0f);
	void CommandPilot(float &speedWanted, float &headChange, float &turnPredict);
#endif

	private:
	float CalculateDriverPos(float &headChange, float minDist);

	public:

	// void UpdatePath(Vector3Par dest);

	void ShowDammage(int part);

	// crew channel xmit done
	void ReceivedMove( Vector3Par tgt );
	void ReceivedTarget(Target *tgt);
	void ReceivedFire(Target *tgt);
	void ReceivedJoin();
	void ReceivedCeaseFire();
	void ReceivedSimpleCommand( SimpleCommand cmd );
	void ReceivedLoad(int weapon);
	void ReceivedAzimut(float azimut);
	void ReceivedStopTurning(float azimut);

	// xmit into crew channel
	void SendMove( Vector3Par pos );
	void SendTarget(Target *tgt);
	void SendFire(Target *tgt);
	void SendJoin();
//	void SendCeaseFire();
	void SendSimpleCommand( SimpleCommand cmd );
	void SendLoad(int weapon);
	void SendAzimut(float azimut);
	void SendStopTurning(float azimut);
	void SendFireFailed();

	OLinkArray<AIUnit> &WhoIsGettingOut() {return _getoutUnits;}
	OLinkArray<AIUnit> &WhoIsGettingIn() {return _getinUnits;}

	const OLinkArray<AIUnit> &WhoIsGettingOut() const {return _getoutUnits;}
	const OLinkArray<AIUnit> &WhoIsGettingIn() const {return _getinUnits;}

	void GetInStarted( AIUnit *unit );
	void GetOutStarted( AIUnit *unit );
	void GetInFinished( AIUnit *unit ); // done/canceled
	void GetOutFinished( AIUnit *unit ); // done/canceled

	void WaitForGetIn(AIUnit *unit);
	void WaitForGetOut(AIUnit *unit);
	void LandStarted(LandingMode landing);
	void LandFinished() {_landing = LMNone;}

	virtual bool IsStopped() const;
	bool CanCancelStop() const;

	Time GetGetOutTime() const {return _getoutTime;}
	void SetGetOutTime( Time time ){_getoutTime=time;}

	Time GetGetInTimeout() const {return _getinTime;}
	void SetGetInTimeout(Time time) {_getinTime = time;}

	Vector3 GetUnitGetInPos(AIUnit *unit);
	Vector3 GetUnitGetOutPos(AIUnit *unit);

	AIGroup *GetGroupAssigned() {return _groupAssigned;}
	AIUnit *GetDriverAssigned() {return _driverAssigned;}
	AIUnit *GetGunnerAssigned() {return _gunnerAssigned;}
	AIUnit *GetCommanderAssigned() {return _commanderAssigned;}
	int NCargoAssigned() {return _cargoAssigned.Count();}
	AIUnit *GetCargoAssigned(int i)
	{_cargoAssigned.Compact(); return _cargoAssigned[i];}
	void RemoveAssignement(AIUnit *unit);
	void AssignGroup(AIGroup *grp) {_groupAssigned = grp;}
	void AssignDriver(AIUnit *unit) {_driverAssigned = unit;}
	void AssignGunner(AIUnit *unit) {_gunnerAssigned = unit;}
	void AssignCommander(AIUnit *unit) {_commanderAssigned = unit;}
	void AssignCargo(AIUnit *cargo) {_cargoAssigned.Add(cargo);}
	void EmptyCargo() {_cargoAssigned.Clear();}

	//void GetIn( Person *driver ) {_driver=driver;}
	Person *Driver() const {return _driver;}
	Person *Commander() const {return _commander;}
	Person *Gunner() const {return _gunner;}
	//void GetOut() {_driver.Free();}

	void DriverConstruct( Person *driver );

	private:
	void GetInAny(Person *driver, bool sound, const char *name);
	void GetOutAny(const Matrix4 & outPos, Person *driver, bool sound, const char *name);

protected:
	virtual Vector3 GetDriverGetInPos(Person *person, Vector3Par pos) const;
	virtual Vector3 GetCommanderGetInPos(Person *person, Vector3Par pos) const;
	virtual Vector3 GetGunnerGetInPos(Person *person, Vector3Par pos) const;
	virtual Vector3 GetCargoGetInPos(Person *person, Vector3Par pos) const;
	virtual Vector3 GetCoDriverGetInPos(Person *person, Vector3Par pos) const;

	virtual Vector3 GetDriverGetOutPos(Person *person) const;
	virtual Vector3 GetCommanderGetOutPos(Person *person) const;
	virtual Vector3 GetGunnerGetOutPos(Person *person) const;
	virtual Vector3 GetCargoGetOutPos(Person *person) const;

	float GetGetInRadius() const {return Type()->GetGetInRadius();}

	public:
	virtual Vector3 GetStopPosition() const;

	void GetInDriver( Person *driver, bool sound=true);
	void GetOutDriver( const Matrix4 & outPos, bool sound=true);
	bool QIsDriverIn() const {return _driver!=NULL;}

	void GetInGunner( Person *man, bool sound=true );
	void GetOutGunner( const Matrix4 &outPos, bool sound=true);
	bool QIsGunnerIn() const {return _gunner!=NULL;}

	void GetInCommander( Person *man, bool sound=true);
	void GetOutCommander( const Matrix4 &outPos, bool sound=true);
	bool QIsCommanderIn() const {return _commander!=NULL;}

	void GetInCargo( Person *driver, bool sound=true, int posIndex=-1);
	void GetOutCargo( Person *driver, const Matrix4 &outPos, bool sound=true);
	const ManCargo &GetManCargo() const {return _manCargo;}
	int GetManCargoSize() const;
	int GetFreeManCargo() const;

	int GetMaxManCargo() const {return Type()->_maxManCargo;}
	float GetMaxFuelCargo() const {return Type()->_maxFuelCargo;}
	float GetMaxRepairCargo() const {return Type()->_maxRepairCargo;}
	float GetMaxAmmoCargo() const {return Type()->_maxAmmoCargo;}
	bool IsAttendant() const {return Type()->_attendant;}

	float GetFreeFuelCargo() const {return Type()->_maxFuelCargo-GetFuelCargo();}
	float GetFreeRepairCargo() const {return Type()->_maxRepairCargo-GetRepairCargo();}
	float GetFreeAmmoCargo() const {return Type()->_maxAmmoCargo-GetAmmoCargo();}

	virtual float GetExplosives() const; // how much explosives is in

	float NeedsLoadFuel() const;
	float NeedsLoadAmmo() const;
	float NeedsLoadRepair() const;

	// if there is no driver, you can get in
	virtual bool IsPossibleToGetIn() const;
	virtual bool IsWorking() const;
	virtual bool IsAbleToMove() const;
	virtual bool IsAbleToFire() const;

	virtual float CalculateTotalCost() const;
	
	virtual RString GetActionName(const UIAction &action);
	virtual void PerformAction(const UIAction &action, AIUnit *unit);

	virtual void GetActions(UIActions &actions, AIUnit *unit, bool now);

	void ChangePosition(UIActionType type, Person *soldier);

	bool QCanIBeIn( Person *who ) const;

	virtual bool QCanIGetIn( Person *who=NULL ) const; // TODO: remove who argument
	virtual bool QCanIGetInGunner( Person *who=NULL ) const; // TODO: remove who argument
	virtual bool QCanIGetInCommander( Person *who=NULL ) const; // TODO: remove who argument
	virtual bool QCanIGetInCargo( Person *who=NULL ) const; // both cargo and driver will do
	virtual bool QCanIGetInAny( Person *who=NULL ) const; // both cargo and driver will do

	virtual void DammageCrew( EntityAI *killer, float howMuch, RString ammo );
	virtual void HitBy( EntityAI *owner, float howMuch, RString ammo );
	virtual void Destroy( EntityAI *killer, float overkill, float minExp, float maxExp  ); // overkill==1 means no overkill
	virtual void Eject( AIUnit *unit ); // eject is possible
	virtual void Land(); // start landing autopilot
	virtual void CancelLand(); // start landing autopilot

	virtual LSError Serialize(ParamArchive &ar);

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx);
	void DestroyObject();

	virtual void ResetStatus();

	void UpdateStopTimeout();

	USE_CASTING(base)
};

typedef Transport VehicleTransport;

#endif
