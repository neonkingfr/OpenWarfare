/*!
\file
Implementation for Object class
Implements dammage related functions
*/
#include "wpch.hpp"

#include "object.hpp"
#include "shots.hpp"
#include "global.hpp"
#include <El/Common/randomGen.hpp>
#include "world.hpp"
#include "landscape.hpp"
#include "vehicleAI.hpp"
#include "ai.hpp"

//#include "loadStream.hpp"
#include "network.hpp"

#define DAMMAGE_DIAGS 0


Vehicle *Object::GetSmoke() const
{
	if( !_dammage ) return NULL;
	return _dammage->_smoke;
}
void Object::SetSmoke( Vehicle *smoke )
{
	if( !_dammage ) _dammage=new DammageRegions;
	_dammage->_smoke=smoke;
	_canSmoke=false;
}

float Object::GetExplosives() const
{
	return 0;
	// how much explosives is in
}

bool Object::HasGeometry() const
{
	if (!_isDestroyed) return true;
	return (DestructType)_destrType!=DestructTree && (DestructType)_destrType!=DestructTent;
}


#if DAMMAGE_DIAGS
#define ReportRegion(reg,text) \
LogF("Dammage %s %.3f:(%.1f,%.1f,%.1f x %.1f)", \
text,reg.Dammage(),reg.Center()[0],reg.Center()[1],reg.Center()[2],reg.Radius() \
);
#else
#define ReportRegion(reg,text)
#endif


DEFINE_FAST_ALLOCATOR(DammageRegions)

DammageRegions::DammageRegions()
://_maxRegions(2),_totalDammageDirty(false),
//_radius(100),_center(VZero),
_totalDammage(0)
{
}


float DammageRegions::GetTotalDammage() const
{
	return _totalDammage;
}

void DammageRegions::SetTotalDammage( float val )
{
	// used for fake units
	//Assert( _regions.Size()==0 );
	//_totalDammageDirty=false;
	_totalDammage=val;
}

float DammageRegions::Repair( float ammount )
{
	_totalDammage-=ammount;
	saturate(_totalDammage,0,1);
	return _totalDammage;
}

bool DammageRegions::MustBeSaved() const
{
	return _totalDammage>0;
	//return _regions.Size()>0;
}

inline float CalcDammage( float distance2, float valRange2 )
{
	if( distance2<=valRange2 ) return 1;
	else return Square(valRange2)/Square(distance2);
}

//const int maxTotDammage=180;
//const int maxPVDammage=220;

void Object::IndirectDammage
(
	Shot *shot, EntityAI *owner, Vector3Par pos, float val, float valRange
)
{
	if( val<=0 ) return;	
	if( (ObjectType)_type==Temporary ) return; // no dammage for temporary objects
	if( (ObjectType)_type==TypeTempVehicle ) return;
	//if( (ObjectType)_type==Network ) return;
	if( GetDestructType()==DestructNo ) return;
	// calculate simple per-object dammage
	// calculate dammage in three points - near, middle and far
	Matrix4Val invTransform=GetInvTransform();
	Vector3Val center=invTransform.FastTransform(pos);
	// use armor
	val*=GetInvArmor();
	Assert( valRange>0 );
	#if DAMMAGE_DIAGS
	LogF
	(
		"Indirect dammage %s,%s armor %.1f: %.3f (range %.1f)",
		_shape->Name(),(const char *)GetDebugName(),GetArmor(),val,valRange
	);
	#endif
	LocalDammage(shot,owner,center,val,valRange);
}

void Object::DirectDammage
(
	Shot *shot, EntityAI *owner, Vector3Par pos, float val
)
{
	if( val<=0 ) return;	
	if( (ObjectType)_type==Temporary ) return; // no dammage for temporary objects
	if( (ObjectType)_type==TypeTempVehicle ) return;
	//if( _type==Network ) return;
	if( GetDestructType()==DestructNo ) return;
	if( !GetShape() ) return;

	//Matrix4Val invTransform = GetInvTransform();
	Matrix4Val invTransform = WorldInvTransform(); //GetInvTransform();
	Vector3Val center = invTransform.FastTransform(pos);
	// use armor
	float valRange=sqrt(val)*0.27;
	val*=GetInvArmor();
	#if DAMMAGE_DIAGS
	LogF
	(
		"Direct dammage %s armor %.1f: %.3f:(%.1f)",
		_shape->Name(),GetArmor(),val,valRange
	);
	#endif
	// TODO: some reasonable calculation of dammage radius
	LocalDammage(shot,owner,center,val,-valRange);
}

float Object::LocalHit(Vector3Par pos, float val, float valRange)
{
	return 1;
}

float Object::DirectLocalHit(int component, float val)
{
	return 1;
}

void Object::LocalDammage
(
	Shot *shot, EntityAI *owner, Vector3Par modelPos, float val, float valRange
)
{
	if (shot)
	{
		// no dammage from fake (remote) shots
		if (!shot->IsLocal()) return;
	}
	else if (owner)
	{
		// no dammage from fake (remote) vehicles
		if (!owner->IsLocal()) return;
	}
	else
	{
		//Fail("No owner nor shot");
	}


	RString name = shot ? shot->Type()->ParClass().GetName() : RString();
	if (IsLocal())
	{
		// process dammage
		DoDammage(owner, modelPos, val, valRange, name);
		if (GetNetworkId().creator == 1)
		{
			// broadcast dammage of static object over network
			GetNetworkManager().AskForDammage(this, owner, modelPos, val, valRange, name);
		}
	}
	else
	{
		// ask owner for dammage
		GetNetworkManager().AskForDammage(this, owner, modelPos, val, valRange, name);
	}
}

#include "shots.hpp"

/*!
	Perform LocalHit, set total (structural) dammage and call HitBy.
	Call also Destroy when neccessary.
*/


void Object::DoDammage
(
	EntityAI *owner, Vector3Par pos,
	float val, float valRange, RString ammo
)
{
#if _ENABLE_REPORT
	if (dyn_cast<Shot>(this))
	{
		RptF("Dammaged shot %s",(const char *)GetDebugName());
	}
#endif
	bool diffArmor = Glob.config.IsEnabled(DTArmor);

	if
	(
#if _ENABLE_CHEATS
		Glob.config.super ||
#endif
		diffArmor
	)
	{
		VehicleWithBrain *player = GWorld->PlayerOn();
		AIUnit *playerUnit = player ? player->Brain() : NULL;
		if (playerUnit)
		{
			if (playerUnit->GetVehicle() == this)
			{
#if _ENABLE_CHEATS
				if (Glob.config.super) val = 0;
				else 
#endif
					val *= 1.0 / 3.0;
			}
			else if (diffArmor)
			{
				AIGroup *playerGroup = playerUnit->GetGroup();
				if (playerGroup) for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
				{
					AIUnit *unit = playerGroup->UnitWithID(i + 1);
					if (unit && unit->GetVehicle() == this)
					{
						val *= 1.0 / 1.5;
						break;
					}
				}
			}
		}
	}

	/*
	if (valRange<0)
	{
		// check which component is this hit in
		// call DirectLocalHit for hit
		// TODO: pass index as argument to this function
		// or find the component here

		int index = -1;

		val *= DirectLocalHit(index,val * GetArmor());

	}
	*/

	val *= LocalHit(pos, val * GetArmor(), valRange);

	saturateMin(val,2000);

	#if DAMMAGE_DIAGS
	LogF
	(
		"  %s: Do dammage %.3f (range %.1f)",
		(const char *)GetDebugName(),val,valRange
	);
	#endif

	// TODO: some reasonable calculation of dammage radius
	// TODO: consider pre-calculating of dammage sum
	float objRadius = _shape->GeometrySphere();
	float dist2ToCenter=pos.SquareSize();

	float oldDammage = GetRawTotalDammage();
	bool oldDestroyed = IsDammageDestroyed();
	if( valRange<=0 )
	{
		// direct hit
		// some reasonable calculation of hit area for direct hit
		if( valRange==0  )
		{
			Fail("Obsolete direct dammage value");
			valRange=2*val;
			saturateMin(valRange,objRadius);
		}
		else
		{
			valRange=-valRange;
		}
		// some examples:
		// 6 m for bullet (3mm direct) on soldier (1mm) - do not care, he is dead anyway
		// 0.2m  for grenade (20mm direct) on M60 (250mm)
		// 1.6m  for AT3 (200mm direct) on M60 (250mm)

		#if DAMMAGE_DIAGS
		LogF("  val %.3f valRange %.3f objRadius %.3f",val,valRange,objRadius);
		#endif
		
		// calculate total armor mass
		float avgDammage=val*Square(valRange/objRadius);
		if( avgDammage>1e-3 )
		{
			SetTotalDammage(GetTotalDammage()+avgDammage);
		}
	}
	else
	{
		// sum of regions in central area will be val*1.33
		//val*=(1.0/1.33);
		// calculate dammage at sample points: nearest, center, furthest
		float distToCenter=sqrt(dist2ToCenter);
		// TODO: consider more sample points?
		float minDist=floatMax(distToCenter-objRadius,0);
		float valRange2=Square(valRange);

		float minDammage=CalcDammage(Square(minDist),valRange2);
		if (minDammage*val<=1e-3)
		{
			#if DAMMAGE_DIAGS
			LogF("  Dist: min %.3f",minDist);
			LogF("  Damm: min %.3f (ignored)",minDammage);
			#endif
		}
		else
		{
			float midDist=distToCenter;
			float maxDist=distToCenter+objRadius;
			float midDammage=CalcDammage(Square(midDist),valRange2);
			float maxDammage=CalcDammage(Square(maxDist),valRange2);
			#if DAMMAGE_DIAGS
			LogF("  Dist: min %.3f mid %.3f max %.3f",minDist,midDist,maxDist);
			LogF("  Damm: min %.3f mid %.3f max %.3f",minDammage,midDammage,maxDammage);
			#endif

			float avgDammage=(minDammage+midDammage+maxDammage)*(0.33*val);
			if( avgDammage>1e-3 )
			{
				SetTotalDammage(GetTotalDammage()+avgDammage);
			}
		}
	}

	// simple cummulative dammage
	// this region may be important

	#if DAMMAGE_DIAGS
	LogF("  Total %.4f",GetRawTotalDammage());
	#endif
	HitBy(owner,GetRawTotalDammage()-oldDammage,ammo);
	if( !oldDestroyed && IsDammageDestroyed() )
	{
		// can be never destroy
		//Assert( !!IsDestroyed() );
		Destroy(owner,GetRawTotalDammage(),0.05,0.1);
	}
}

/*!
	Set only value. Do not change any other part of dammage state
	(like smoke).
*\todo Remove Object::SetTotalDammage or Object::SetTotalDammageValue. 
	Both functions seem to be identical.
*/
void Object::SetTotalDammage( float val )
{
	if( fabs(val-GetTotalDammage())<1e-6 ) return;
	if( val>0 )
	{
		// dammage may already be set - it may contain some smoke sourece
		if (!_dammage) _dammage=new DammageRegions;
		_dammage->SetTotalDammage(val);
	}
	else _dammage.Free();
}

/*!
	Set only value. Do not change any other part of dammage state
	(like smoke).
*/
void Object::SetTotalDammageValue( float val )
{
	if( fabs(val-GetTotalDammage())<1e-6 ) return;
	if (val>0)
	{
		if (!_dammage)
		{
			_dammage=new DammageRegions;
		}
		_dammage->SetTotalDammage(val);
	}
	else
	{
		_dammage.Free();
	}
}

void Object::SetDammageNetAware(float dammage)
{
	// set dammage for local objects only
	if (IsLocal())
	{
		SetDammage(dammage);
		/*
		if (GetNetworkId().creator == STATIC_OBJECT)
			// no automatic update is performed - update manually
			GetNetworkManager().AskForSetDammage(this, dammage);
		*/
	}
	else
	{
		GetNetworkManager().AskForSetDammage(this, dammage);
	}
}

void Object::SetDammage(float dammage)
{
	bool oldDestroyed = IsDammageDestroyed();

	SetTotalDammageValue(dammage);

	if (!oldDestroyed)
	{
		if (IsDammageDestroyed())
		{
			EntityAI *owner = dyn_cast<EntityAI>(this);
			if (!owner)
			{
				ErrF("SetDammage applied on non-EntityAI %s",(const char *)GetDebugName());
			}
			Destroy(owner, dammage, 0.05, 0.1);
		}
	}
	else if (GetTotalDammage() < 1.0)
	{
		Vehicle *smoke=GetSmoke();
		if (smoke) smoke->SetDelete();
		_canSmoke = true;
		_isDestroyed = false;
		SetDestroyPhase(0);
	}
}

#include "operMap.hpp"

void Object::SetDestroyPhase(int phase)
{
	// adjust matrix according to destroy phase

	if (GetDestructType()==DestructTree && phase!=_destroyPhase)
	{
		// apply destruction to the object transformation matrix
		// simulate non-linear destruction function
		float newAngle = sin(phase*H_PI/2/255)*(H_PI/2);
		float oldAngle = sin(_destroyPhase*H_PI/2/255)*(H_PI/2);
		float animChange = newAngle-oldAngle;
		// rotate around x-axis (suitable for walls)
		Vector3 bCenter = GetShape()->BoundingCenter();
		// object rotates around -bCenter
		Matrix4 rot=
		(
			Matrix4(MTranslation,-bCenter)*
			Matrix4(MRotationX,animChange)*
			Matrix4(MTranslation,bCenter)
		);
		// apply rotation to object matrix
		Matrix4 trans = Transform() * rot;
		// note: Move only when object is in landscape
		// how can we know it?
		Move(trans);
		GScene->GetShadowCache().ShadowChanged(this);
	}

	_destroyPhase=phase;
}

void Object::SetDestroyed( float anim )
{
	int phase=toIntFloor(anim*255);
	saturate(phase,0,255);
	SetDestroyPhase(phase);
	bool wasDestroyed = _isDestroyed;
	_isDestroyed = phase>0;
	if (_isDestroyed && !wasDestroyed)
	{
		// check if field where this object is in field cache
		if (GetType()==Primary)
		{
			int xLand = toIntFloor(Position().X() * InvLandGrid);
			int zLand = toIntFloor(Position().Z() * InvLandGrid);
			for (int x=xLand-1; x<=xLand+1; x++)
				for (int z=zLand-1; z<=zLand+1; z++)
					GLandscape->OperationalCache()->RemoveField(x,z);
		}
	}
}

void Object::Repair( float ammount )
{
	if( GetTotalDammage()<=0 && ammount>=0 )
	{
		return;
	}
	if( !_dammage ) _dammage=new DammageRegions;
	if( _dammage->Repair(ammount)<=0 )
	{
		_dammage.Free();
	}
	
	float tot=GetTotalDammage();
	if( tot<0.90 )
	{
		Vehicle *smoke=GetSmoke();
		if( smoke ) smoke->SetDelete();
		_canSmoke=true;
	}
	_isDestroyed=false;
	SetDestroyPhase(0);
}

void Object::HitBy( EntityAI *killer, float howMuch, RString ammo )
{
}

bool Object::IsDammageDestroyed() const
{
	return GetRawTotalDammage()>=1;
}

void Object::Destroy( EntityAI *owner, float overkill, float minExp, float maxExp )
{
	// start destruction
	Vector3Val com=_shape->CenterOfMass();
	float objSize=GetRadius();
	float density=objSize*(1.0/3);
	float size=objSize*(1.0/3);
	saturate(density,0.25,2.0);
	saturate(size,0.25,4.0);
	size*=GRandGen.RandomValue()*0.4+0.8;
	density*=GRandGen.RandomValue()*0.4+0.8;
	float rndTime=GRandGen.RandomValue()*0.4+0.8;
	switch( GetDestructType() )
	{
		case DestructTree:
		case DestructTent:
		{
			const ParamEntry &par=Pars>>"CfgDestroy">>"TreeHit";
			SoundPars soundPars;
			GetValue(soundPars, par>>"sound");
			// temporary destruction vehicle
			float time = 3;
			if (GetDestructType()==DestructTent) time = 1.5;
			ObjectDestructed *destroyer=new ObjectDestructed(this,soundPars,time);
			destroyer->SetPosition(Position());
			GLOB_WORLD->AddAnimal(destroyer);
			GetNetworkManager().CreateVehicle(destroyer, VLTAnimal, "", -1);
		}
		break;
		case DestructBuilding:
		case DestructDefault:
		{
			const ParamEntry &par=Pars>>"CfgDestroy">>"BuildingHit";
			SoundPars soundPars;
			GetValue(soundPars, par>>"sound");
			// temporary destruction vehicle
			ObjectDestructed *destroyer=new ObjectDestructed(this,soundPars,2);
			destroyer->SetPosition(Position());
			GLOB_WORLD->AddAnimal(destroyer);
			GetNetworkManager().CreateVehicle(destroyer, VLTAnimal, "", -1);
		}
		break;
		case DestructEngine:
		{
			if( CanSmoke() && !GetSmoke() )
			{
				//obj->NeverDestroy();
				// start smoke
				LODShapeWithShadow *smokeShape=GLOB_SCENE->Preloaded(CloudletBasic);
				// acquire CenterOfMass
				// attach smoke to vehicle
				SmokeSourceVehicle *smoke=new SmokeSourceOnVehicle(smokeShape,density,size,owner,this,com);
				smoke->SetClimbRate(-0.8,0,5);
				float smokeSourceCoef=0.2;
				rndTime*=smokeSourceCoef;
				smoke->SetSourceTimes(5*rndTime,30*rndTime,30*rndTime);
				float color=GRandGen.RandomValue();
				smoke->SetColor(Color(0.15,0.15,0.10)*color);
				smoke->SetPosition(PositionModelToWorld(com));
				smoke->SetTimes(0.5,3);
				//smoke->SetFades(1.5,1,3);
				smoke->SetFades(1.5,0.5,3);
				GLOB_WORLD->AddAnimal(smoke);
				SetSmoke(smoke);
				GetNetworkManager().CreateVehicle(smoke, VLTAnimal, "", -1);
				SoundPars soundPars;
				soundPars.name=RString(NULL); // no sound
				soundPars.vol=soundPars.freq=0;
				ObjectDestructed *destroyer=new ObjectDestructed(this,soundPars,1);
				destroyer->SetPosition(Position());
				GLOB_WORLD->AddAnimal(destroyer);
				GetNetworkManager().CreateVehicle(destroyer, VLTAnimal, "", -1);
			}
		}
		break;
	}
}

DEF_RSB(id)
DEF_RSB(canSmoke)
DEF_RSB(isDestroyed)
DEF_RSB(destroyed)
DEF_RSB(destroyPhase)
DEF_RSB(dammage)

LSError Object::Serialize(ParamArchive &ar)
{
	if (ar.IsSaving()) CHECK(ar.Serialize(RSB(id), _id, 1))
	SerializeBitBool(ar, RSB(canSmoke), _canSmoke, 1, true)
	SerializeBitBool(ar, RSB(isDestroyed), _isDestroyed, 1, false)
	if (ar.IsSaving())
	{
		float destroyed = GetDestroyed();
		CHECK(ar.Serialize(RSB(destroyed), destroyed, 1, 0))
		float dammage = GetRawTotalDammage();
		// note: dammage is sometimes infinite
		if (!_finite(dammage))
		{
			RptF("Saving inifinite dammage in %s",(const char *)GetDebugName());
		}
		CHECK(ar.Serialize(RSB(dammage), dammage, 1, 0))
	}
	else if (ar.GetPass() == ParamArchive::PassFirst)
	{
		float destroyed;
		CHECK(ar.Serialize(RSB(destroyed), destroyed, 1, 0))
		SetDestroyed(destroyed);
		float dammage;
		CHECK(ar.Serialize(RSB(dammage), dammage, 1, 0))
		SetTotalDammage(dammage);
	}

	{
		int t=_destroyPhase;
		CHECK(ar.Serialize(RSB(destroyPhase), t, 1, 0));
		SetDestroyPhase(t);
	}

	return LSOK;

	// set in creation: _shape
	// only for non-primary vehicles (in Vehicle::Serialize) _id
	// ?? _constantColor

	// TODO: check and remove: Time _lastAnimTime;
	// TODO: check and remove: OLink<ObjectMerger> _merger;

	// set in creation: _type
	// set in creation: _destrType
	
	// set in creation: _static
	// temporary: _inList

	// set in creation: _scale
}

Object *Object::CreateObject(ParamArchive &ar)
{
	int id;
	if (ar.Serialize(RSB(id), id, 1) != LSOK) return NULL;
	return GLandscape->FindObject(id);
}

/*!\note
	Object does not have to be saved when all properties have default values.
*/

bool Object::MustBeSaved() const
{
	if( _isDestroyed ) return true;
	if( !_canSmoke ) return true;
	if( _dammage && _dammage->MustBeSaved() ) return true;
	return false;
}

NetworkId Object::GetNetworkId() const
{
	if ( (ObjectType)_type != Primary && (ObjectType)_type != Network )
	{
		ErrF
		(
			"Type is not primary for object %s",
			(const char *)GetDebugName()
		);
	}
	return NetworkId(STATIC_OBJECT, _id);
}

void Object::SetNetworkId(NetworkId &id)
{
	Fail("Cannot set network id");
}

bool Object::IsLocal() const
{
	return true;
}

void Object::SetLocal(bool local)
{
	Fail("Object is always local");
}

NetworkMessageType Object::GetNMType(NetworkMessageClass cls) const
{
	switch (cls)
	{
	case NMCCreate:
		return NMTCreateObject;
	case NMCUpdateGeneric:
		return NMTUpdateObject;
	case NMCUpdateDammage:
		return NMTUpdateDammageObject;
	default:
		return NetworkObject::GetNMType(cls);
	}
}

IndicesCreateObject::IndicesCreateObject()
{
}

void IndicesCreateObject::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);
}

NetworkMessageIndices *GetIndicesCreateObject() {return new IndicesCreateObject();}

IndicesUpdateObject::IndicesUpdateObject()
{
	canSmoke = -1;
	destroyed = -1;
}

void IndicesUpdateObject::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);

	SCAN(canSmoke);
	SCAN(destroyed);
}

NetworkMessageIndices *GetIndicesUpdateObject() {return new IndicesUpdateObject();}

IndicesUpdateDammageObject::IndicesUpdateDammageObject()
{
	isDestroyed = -1;
	dammage = -1;
}

void IndicesUpdateDammageObject::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);

	SCAN(isDestroyed);
	SCAN(dammage);
}

NetworkMessageIndices *GetIndicesUpdateDammageObject() {return new IndicesUpdateDammageObject();}

NetworkMessageFormat &Object::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	switch (cls)
	{
	case NMCCreate:
		NetworkObject::CreateFormat(cls, format);
//		format.Add("id", NDTInteger, NCTNone, DEFVALUE(int, 0));
		break;
	case NMCUpdateDammage:
		NetworkObject::CreateFormat(cls, format);
		format.Add("isDestroyed", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Object is already destructed"), ET_NOT_EQUAL, ERR_COEF_STRUCTURE);
		format.Add("dammage", NDTFloat, NCTFloatMostly0To1, DEFVALUE(float, 0.0f), DOC_MSG("Total damage of object"), ET_ABS_DIF, ERR_COEF_MODE);
		break;
	case NMCUpdateGeneric:
		NetworkObject::CreateFormat(cls, format);
		format.Add("canSmoke", NDTBool, NCTNone, DEFVALUE(bool, true), DOC_MSG("Object may smoke when destructed"));
		format.Add("destroyed", NDTFloat, NCTFloat0To1, DEFVALUE(float, 0.0f), DOC_MSG("Shape destruction state"));
		break;
	default:
		NetworkObject::CreateFormat(cls, format);
		break;
	}
	return format;
}

Object *Object::CreateObject(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesNetworkObject *>(ctx.GetIndices()))
	const IndicesNetworkObject *indices = static_cast<const IndicesNetworkObject *>(ctx.GetIndices());

	int id;
	if (ctx.IdxTransfer(indices->objectId, id) != TMOK) return NULL;
	return GLandscape->FindObject(id);
}

void Object::DestroyObject()
{
	Fail("Cannot remove primary object");
}

TMError Object::TransferMsg(NetworkMessageContext &ctx)
{
	switch (ctx.GetClass())
	{
	case NMCCreate:
/*
		if (ctx.IsSending())
		{
			TRANSF(id)
		}
*/
		TMCHECK(NetworkObject::TransferMsg(ctx))
		break;
	case NMCUpdateDammage:
		{
			Assert(dynamic_cast<const IndicesUpdateDammageObject *>(ctx.GetIndices()))
			const IndicesUpdateDammageObject *indices = static_cast<const IndicesUpdateDammageObject *>(ctx.GetIndices());
			TMCHECK(NetworkObject::TransferMsg(ctx))
			ITRANSF_BITBOOL(isDestroyed)
			if (ctx.IsSending())
			{
				float dammage = GetRawTotalDammage();
				TMCHECK(ctx.IdxTransfer(indices->dammage, dammage))
			}
			else
			{
				float dammage;
				TMCHECK(ctx.IdxTransfer(indices->dammage, dammage))
				SetTotalDammageValue(dammage);
			}
		}
		break;
	case NMCUpdateGeneric:
		{
			Assert(dynamic_cast<const IndicesUpdateObject *>(ctx.GetIndices()))
			const IndicesUpdateObject *indices = static_cast<const IndicesUpdateObject *>(ctx.GetIndices());

			TMCHECK(NetworkObject::TransferMsg(ctx))
			ITRANSF_BITBOOL(canSmoke)
			if (ctx.IsSending())
			{
				float destroyed = GetDestroyed();
				TMCHECK(ctx.IdxTransfer(indices->destroyed, destroyed))
			}
			else
			{
				float destroyed;
				TMCHECK(ctx.IdxTransfer(indices->destroyed, destroyed))
				SetDestroyed(destroyed);
			}
		}
		break;
	default:
		TMCHECK(NetworkObject::TransferMsg(ctx))
		break;
	}
	return TMOK;
}

float Object::CalculateError(NetworkMessageContext &ctx)
{
	// TODO: implementation
	float error = 0;
	switch (ctx.GetClass())
	{
	case NMCUpdateDammage:
		{
			error += NetworkObject::CalculateError(ctx);

			Assert(dynamic_cast<const IndicesUpdateDammageObject *>(ctx.GetIndices()))
			const IndicesUpdateDammageObject *indices = static_cast<const IndicesUpdateDammageObject *>(ctx.GetIndices());

			ICALCERR_NEQ(bool, isDestroyed, ERR_COEF_STRUCTURE)
			ICALCERRE_ABSDIF(float, dammage, GetRawTotalDammage(), ERR_COEF_VALUE_MAJOR)
		}
		break;
	case NMCUpdateGeneric:
		{
			error += NetworkObject::CalculateError(ctx);

			//Assert(dynamic_cast<const IndicesUpdateObject *>(ctx.GetIndices()))
			//const IndicesUpdateObject *indices = static_cast<const IndicesUpdateObject *>(ctx.GetIndices());

		}
		break;
	default:
		error += NetworkObject::CalculateError(ctx);
		break;
	}
	return error;
}

void Object::ResetStatus()
{
	_dammage.Free();
	Vehicle *smoke=GetSmoke();
	if( smoke ) smoke->SetDelete();
	_canSmoke=true;
	_isDestroyed=false;
	SetDestroyPhase(0);
}

void Object::OnTimeSkipped()
{
}

void Object::OnPositionChanged()
{
}

