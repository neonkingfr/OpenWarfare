#ifdef _MSC_VER
#pragma once
#endif

#ifndef _GAME_STATE_EXT_HPP
#define _GAME_STATE_EXT_HPP

#include <El/Evaluator/express.hpp>

const GameType GameObject(0x100);
const GameType GameVector(0x200);
const GameType GameTrans(0x400);
const GameType GameOrient(0x800);
const GameType GameSide(0x1000);
const GameType GameGroup(0x2000);
#if _ENABLE_VBS
const GameType GameFile(0x4000);
#endif

typedef TargetSide GameSideType;

typedef OLink<Object> GameObjectType;
typedef OLink<AIGroup> GameGroupType;
typedef Vector3 GameVectorType;
typedef Matrix4 GameTransType;
typedef Matrix3 GameOrientType;

#if _ENABLE_VBS
class GameFileType
{
private:
	int _index;

public:
	bool readOnly;

	GameFileType() {_index = -1; readOnly = false;}
	GameFileType(const GameFileType &src);
	~GameFileType();

	void operator = (const GameFileType &src);

	int GetIndex() const {return _index;}
	void SetIndex(int index);
};
#endif

#include <Es/Memory/normalNew.hpp>

class GameDataObject: public GameData
{
	typedef GameData base;

	GameObjectType _value;

	public:
	GameDataObject():_value(0){}
	GameDataObject( GameObjectType value ):_value(value){}
	~GameDataObject(){}

	GameType GetType() const {return GameObject;}
	GameObjectType GetObject() const {return _value;}

	RString GetText() const;
	bool IsEqualTo(const GameData *data) const;
	const char *GetTypeName() const {return "object";}
	GameData *Clone() const {return new GameDataObject(*this);}

	LSError Serialize(ParamArchive &ar);	

	USE_FAST_ALLOCATOR
};

class GameDataGroup: public GameData
{
	typedef GameData base;

	GameGroupType _value;

	public:
	GameDataGroup():_value(0){}
	GameDataGroup( GameGroupType value ):_value(value){}
	~GameDataGroup(){}

	GameType GetType() const {return GameGroup;}
	GameGroupType GetGroup() const {return _value;}

	RString GetText() const;
	bool IsEqualTo(const GameData *data) const;
	const char *GetTypeName() const {return "group";}
	GameData *Clone() const {return new GameDataGroup(*this);}

	LSError Serialize(ParamArchive &ar);	

	USE_FAST_ALLOCATOR
};


class GameDataSide: public GameData
{
	typedef GameData base;

	GameSideType _value;

	public:
	GameDataSide():_value(TSideUnknown){}
	GameDataSide( GameSideType value ):_value(value){}
	~GameDataSide(){}

	GameType GetType() const {return GameSide;}
	GameSideType GetSide() const {return _value;}

	RString GetText() const;
	bool IsEqualTo(const GameData *data) const;
	const char *GetTypeName() const {return "side";}
	GameData *Clone() const {return new GameDataSide(*this);}

	LSError Serialize(ParamArchive &ar);

	USE_FAST_ALLOCATOR
};

#if _ENABLE_VBS
class GameDataFile: public GameData
{
	typedef GameData base;

	GameFileType _value;

	public:
	GameDataFile(){}
	GameDataFile( GameFileType value ):_value(value){}
	~GameDataFile(){}

	GameType GetType() const {return GameFile;}
	GameFileType GetFile() const {return _value;}

	RString GetText() const;
	bool IsEqualTo(const GameData *data) const;
	const char *GetTypeName() const {return "file";}
	GameData *Clone() const {return new GameDataFile(*this);}

	LSError Serialize(ParamArchive &ar);

	USE_FAST_ALLOCATOR
};
#endif

#include <Es/Memory/debugNew.hpp>

inline TargetSide GetSide( GameValuePar oper )
{
	if( oper.GetType()!=GameSide ) return TSideUnknown;
	return static_cast<GameDataSide *>(oper.GetData())->GetSide();
}

typedef bool (AICenter::*IsSide)( TargetSide side) const;

class EntityType;

/*
#define DECLARE_COMMAND(Xxx) \
	GameValue VehDo##Xxx( GameValuePar oper1, GameValuePar oper2 ) const; \
	GameValue VehCommand##Xxx( GameValuePar oper1, GameValuePar oper2 ) const; \
	GameValue Veh##Xxx( GameValuePar oper1, GameValuePar oper2, bool silent ) const;

#define DEFINE_COMMAND(Xxx) \
	GameValue GameStateExt::VehDo##Xxx( GameValuePar oper1, GameValuePar oper2 ) const \
	{ \
		return Veh##Xxx(oper1,oper2,true); \
	} \
	GameValue GameStateExt::VehCommand##Xxx( GameValuePar oper1, GameValuePar oper2 ) const \
	{ \
		return Veh##Xxx(oper1,oper2,false); \
	}

#define DECLARE_COMMAND_S(Xxx) \
	GameValue VehDo##Xxx( GameValuePar oper1 ) const; \
	GameValue VehCommand##Xxx( GameValuePar oper1 ) const; \
	GameValue Veh##Xxx( GameValuePar oper1, bool silent ) const;

#define DEFINE_COMMAND_S(Xxx) \
	GameValue GameStateExt::VehDo##Xxx( GameValuePar oper1 ) const \
	{ \
		return Veh##Xxx(oper1,true); \
	} \
	GameValue GameStateExt::VehCommand##Xxx( GameValuePar oper1 ) const \
	{ \
		return Veh##Xxx(oper1,false); \
	}
*/

/*
class GameStateExt: public GameState
{
	typedef GameState base;

	public:

	// base class overloads
	GameStateExt();
	~GameStateExt();

	// expression operator declaration
	GameValue ObjNull() const;
	GameValue GrpNull() const;
	GameValue ObjIsNull( GameValuePar oper1 ) const;
	GameValue GrpIsNull( GameValuePar oper1 ) const;
	GameValue ObjAlive( GameValuePar oper1 ) const;
	GameValue ObjCanMove( GameValuePar oper1 ) const;
	GameValue ObjCanFire( GameValuePar oper1 ) const;
	GameValue ObjIsLocal( GameValuePar oper1 ) const;
	GameValue ObjFuel( GameValuePar oper1 ) const;
	GameValue ObjSetFuel( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjSetFuelCargo( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjSetRepairCargo( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjSetAmmoCargo( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjSetInfantryAmmoCargo( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjClearWeaponCargo( GameValuePar oper1 ) const;
	GameValue ObjClearMagazineCargo( GameValuePar oper1 ) const;
	GameValue ObjAddWeaponCargo( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjAddMagazineCargo( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue PoolAddWeapon( GameValuePar oper1 ) const;
	GameValue PoolAddMagazine( GameValuePar oper1 ) const;
	GameValue PoolGetWeapons( GameValuePar oper1 ) const;
	GameValue PoolSetWeapons( GameValuePar oper1 ) const;
	GameValue PoolClearWeapons() const;
	GameValue PoolClearMagazines() const;
	GameValue PoolQueryWeapons( GameValuePar oper1 ) const;
	GameValue PoolQueryMagazines( GameValuePar oper1 ) const;
	
	GameValue ObjSelectWeapon( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue ObjList( GameValuePar oper1 ) const;
	GameValue ObjSomeAmmo( GameValuePar oper1 ) const;
	GameValue ObjExperience( GameValuePar oper1 ) const;
	GameValue ObjAddExperience( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjFlee( GameValuePar oper1 ) const;

	GameValue ObjSetCaptive(GameValuePar oper1,GameValuePar oper2) const;
	GameValue ObjCaptive(GameValuePar oper1) const;

	GameValue ObjSide(GameValuePar oper1) const;
	GameValue ObjName(GameValuePar oper1) const;

	GameValue ObjGetPos(GameValuePar oper1) const;
	GameValue ObjSetPos(GameValuePar oper1, GameValuePar oper2) const;
	GameValue ObjGetDir(GameValuePar oper1) const;
	GameValue ObjSetDir(GameValuePar oper1, GameValuePar oper2) const;
	GameValue ObjSetFlyingHeight(GameValuePar oper1, GameValuePar oper2) const;

	GameValue ObjGetSpeed(GameValuePar oper1) const;

	GameValue ObjGetDammage(GameValuePar oper1) const;
	GameValue ObjSetDammage(GameValuePar oper1,GameValuePar oper2) const;
	GameValue ObjAllowDammage(GameValuePar oper1,GameValuePar oper2) const;

	GameValue ObjCanStand(GameValuePar oper1) const;
	GameValue ObjHandsHit(GameValuePar oper1) const;

	GameValue ObjSetIdentity( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjSetFaceAnimation( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjSetFace( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjSetMimic( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue ObjGetFlag(GameValuePar oper1) const;
	GameValue ObjGetFlagOwner(GameValuePar oper1) const;
	GameValue ObjSetFlagOwner( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjSetFlagTexture( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjSetFlagSide( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue ObjGetNearestBuilding(GameValuePar oper1) const;
	GameValue ObjGetBuildingPos(GameValuePar oper1, GameValuePar oper2) const;

	GameValue GetNearestObject(GameValuePar oper1) const;

	GameValue ObjSwitchLight(GameValuePar oper1, GameValuePar oper2) const;
	GameValue ObjInflame(GameValuePar oper1, GameValuePar oper2) const;
	GameValue ObjInflamed( GameValuePar oper1 ) const;
	GameValue ObjLightSwitched( GameValuePar oper1 ) const;

	GameValue ObjGetScudState( GameValuePar oper1 ) const;

	GameValue ObjDistance( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjIn( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjAmmo( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjListIn( GameValuePar oper1 ) const;
	GameValue ObjHasWeapon( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjAddWeapon( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjRemoveWeapon( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjRemoveAllWeapons( GameValuePar oper1 ) const;
	GameValue ObjAddMagazine( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjRemoveMagazine( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjRemoveMagazines( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue ObjFire( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjFireEx( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjSay( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjGlobalRadio( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjSideRadio( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjGroupRadio( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjVehicleRadio( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjGlobalChat( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjSideChat( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjGroupChat( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjVehicleChat( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjPlayMove( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjSwitchMove( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjCameraEffect( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue CutText(GameValuePar oper1 ) const;
	GameValue CutRsc(GameValuePar oper1 ) const;
	GameValue CutObj(GameValuePar oper1 ) const;

	bool ParseEffect
	(
		GameStringType &effect, GameStringType &str, float &speed,
		GameValuePar oper1
	) const;

	GameValue TitleText(GameValuePar oper1 ) const;
	GameValue TitleRsc(GameValuePar oper1 ) const;
	GameValue TitleObj(GameValuePar oper1 ) const;
	GameValue PlaySound(GameValuePar oper1) const;
	GameValue PlayMusic(GameValuePar oper1) const;
	GameValue SetMusicVolume(GameValuePar oper1,GameValuePar oper2) const;
	GameValue GetMusicVolume() const;

	GameValue SetSoundVolume(GameValuePar oper1,GameValuePar oper2) const;
	GameValue GetSoundVolume() const;

	GameValue ObjLocked( GameValuePar oper1) const;
	GameValue ObjLock( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue ObjSetUnitPos( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue ObjStopped( GameValuePar oper1) const;
	GameValue ObjStop( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue ObjDisableAI( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue ObjLand( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue ObjAssignAsCommander( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjAssignAsDriver( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjAssignAsGunner( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjAssignAsCargo( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjUnassignVehicle( GameValuePar oper1 ) const;
	GameValue GrpLeaveVehicle( GameValuePar oper1, GameValuePar oper2 ) const;
	
	GameValue ObjMoveInCommander( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjMoveInDriver( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjMoveInGunner( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjMoveInCargo( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue GrpAllowFleeing( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue ObjAddScore( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjGetScore( GameValuePar oper1 ) const;

	GameValue ObjCmpNE( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjCmpE( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue GrpCmpNE( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue GrpCmpE( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue ObjBehaviour( GameValuePar oper1 ) const;
	GameValue GrpCombatMode( GameValuePar oper1 ) const;
	GameValue GrpFormation( GameValuePar oper1 ) const;
	GameValue GrpSpeedMode( GameValuePar oper1 ) const;
	GameValue GrpSetBehaviour( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue GrpSetCombatMode( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue GrpSetFormation( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue GrpSetSpeedMode( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue GrpLockWP( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue GrpSetFormDir( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue GrpMove( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue GrpSetIdentity( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue GrpKnowsAbout( GameValuePar oper1, GameValuePar oper2 ) const;

	// commands
	GameValue VehDone(GameValuePar oper1) const;
	GameValue GrpReveal(GameValuePar oper1, GameValuePar oper2) const;

	DECLARE_COMMAND(Move)
	DECLARE_COMMAND(Watch)
	DECLARE_COMMAND(Target)
	DECLARE_COMMAND(Follow)
	DECLARE_COMMAND(Fire)

	DECLARE_COMMAND_S(Stop)

	GameValue VehProcessAction( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue VehAddUserAction( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue VehRemoveUserAction( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue VehCreate( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue UnitCreate( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue VehDelete( GameValuePar oper1 ) const;

	protected:
	// helpers
	int ListCountSideH( const GameArrayType &array, TargetSide side ) const;
	int ListCountSideH( const GameArrayType &array, Object *obj, IsSide func ) const;
	int ListCountTypeH( const GameArrayType &array, const EntityType *type ) const;

	public:


	GameValue ListCountEnemy( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ListCountFriendly( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ListCountUnknown( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue ListCountSide( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ListCountType( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue ListAllowGetIn( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ListOrderGetIn( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue ListJoin( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue Player() const;
	GameValue Time() const;
	GameValue DayTime() const;

	GameValue Benchmark() const;
	GameValue CadetMode() const;

	GameValue SaveVar(GameValuePar oper1) const;

	GameValue GetAcceleratedTime() const;
	GameValue SetAcceleratedTime(GameValuePar oper1) const;

	GameValue ObjLeader( GameValuePar oper1 ) const;
	GameValue ObjGroupLeader( GameValuePar oper1 ) const;
	GameValue ObjVehicle( GameValuePar oper1 ) const;
	GameValue ObjDriver( GameValuePar oper1 ) const;
	GameValue ObjCommander( GameValuePar oper1 ) const;
	GameValue ObjGunner( GameValuePar oper1 ) const;

	GameValue ObjGroup( GameValuePar oper1 ) const;
	GameValue GrpLeader( GameValuePar oper1 ) const;
	GameValue GrpUnits( GameValuePar oper1 ) const;

	GameValue CamCreate( GameValuePar oper1,  GameValuePar oper2 ) const;
	GameValue CamCommited( GameValuePar oper1 ) const;
	GameValue CamDestroy( GameValuePar oper1 ) const;
	GameValue CamSetPos( GameValuePar oper1,  GameValuePar oper2 ) const;
	GameValue CamSetRelPos( GameValuePar oper1,  GameValuePar oper2 ) const;
	GameValue CamSetFOV( GameValuePar oper1,  GameValuePar oper2 ) const;
	GameValue CamSetFOVRange( GameValuePar oper1,  GameValuePar oper2 ) const;
	GameValue CamSetDive( GameValuePar oper1,  GameValuePar oper2 ) const;
	GameValue CamSetBank( GameValuePar oper1,  GameValuePar oper2 ) const;
	GameValue CamSetDir( GameValuePar oper1,  GameValuePar oper2 ) const;
	GameValue CamSetTargetObj( GameValuePar oper1,  GameValuePar oper2 ) const;
	GameValue CamSetTargetVec( GameValuePar oper1,  GameValuePar oper2 ) const;
	GameValue CamCommand( GameValuePar oper1,  GameValuePar oper2 ) const;
	GameValue CamCommit( GameValuePar oper1,  GameValuePar oper2 ) const;

	GameValue ObjSwitchCamera( GameValuePar oper1,  GameValuePar oper2 ) const;

	//GameValue SetDayTime( GameValuePar oper1 ) const;
	GameValue SkipDayTime( GameValuePar oper1 ) const;
	GameValue SetVisibility( GameValuePar oper1 ) const;

	GameValue SetOvercast(GameValuePar oper1, GameValuePar oper2) const;
	GameValue SetFog(GameValuePar oper1, GameValuePar oper2) const;

	GameValue SetObjectiveStatus(GameValuePar oper1,  GameValuePar oper2) const;
	
	GameValue MapForce( GameValuePar oper1 ) const;
	GameValue MapAnimClear() const;
	GameValue MapAnimAdd( GameValuePar oper1 ) const;
	GameValue MapAnimCommit() const;
	GameValue MapAnimDone() const;

	GameValue IsMapShown() const;
	GameValue IsWatchShown() const;
	GameValue IsCompassShown() const;
	GameValue IsWalkieTalkieShown() const;
	GameValue IsNotepadShown() const;
	GameValue IsWarrantShown() const;
	GameValue IsGPSShown() const;

	GameValue ShowMap( GameValuePar oper1 ) const;
	GameValue ShowWatch( GameValuePar oper1 ) const;
	GameValue ShowCompass( GameValuePar oper1 ) const;
	GameValue ShowWalkieTalkie( GameValuePar oper1 ) const;
	GameValue ShowNotepad( GameValuePar oper1 ) const;
	GameValue ShowWarrant( GameValuePar oper1 ) const;
	GameValue ShowGPS( GameValuePar oper1 ) const;
	GameValue EnableRadio( GameValuePar oper1 ) const;
	GameValue SetRadioMessage(GameValuePar oper1, GameValuePar oper2) const;

	GameValue ShowHint( GameValuePar oper1 ) const;
	GameValue ShowHintCadet( GameValuePar oper1 ) const;
	GameValue ShowHintC( GameValuePar oper1 ) const;

	GameValue ShowCinemaBorder( GameValuePar oper1 ) const;

	GameValue DisableUserInput( GameValuePar oper1 ) const;

	GameValue PublicVariable( GameValuePar oper1 ) const;

	GameValue SideWest() const;
	GameValue SideEast() const;
	GameValue SideCivilian() const;
	GameValue SideResistance() const;

	GameValue SideCmpNE( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue SideCmpE( GameValuePar oper1, GameValuePar oper2 ) const;
	
	GameValue SaveGame() const;

	GameValue StrFormat( GameValuePar oper1 ) const;
	GameValue StrLocalize( GameValuePar oper1 ) const;

	GameValue ScriptExecute(GameValuePar oper1, GameValuePar oper2) const;
	GameValue ScriptExit() const;
	GameValue ScriptGoto(GameValuePar oper1) const;

	GameValue TextLog(GameValuePar oper1) const;
	GameValue TextDebugLog(GameValuePar oper1) const;

	GameValue EnableEndDialog() const;
	GameValue ForceEnd() const;

	GameValue MarkerGetPos( GameValuePar oper1 ) const;
	GameValue MarkerSetPos( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue MarkerGetType( GameValuePar oper1 ) const;
	GameValue MarkerSetType( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue MarkerGetSize( GameValuePar oper1 ) const;
	GameValue MarkerSetSize( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue MarkerGetColor( GameValuePar oper1 ) const;
	GameValue MarkerSetColor( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue WpGetPos( GameValuePar oper1 ) const;
	GameValue WpSetPos( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue RequiredVersion( GameValuePar oper1 ) const;

	GameValue EstimatedTimeLeft( GameValuePar oper1 ) const;

	// save / load object status
	GameValue ObjSaveStatus( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjLoadStatus( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue ObjSaveIdentity( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjLoadIdentity( GameValuePar oper1, GameValuePar oper2 ) const;
	
	// user dialog manipulation functions
	GameValue DialogCreate( GameValuePar oper1 ) const;
	GameValue DialogClose( GameValuePar oper1 ) const;

	GameValue CtrlVisible( GameValuePar oper1 ) const;
	GameValue CtrlEnabled( GameValuePar oper1 ) const;
	GameValue CtrlShow( GameValuePar oper1 ) const;
	GameValue CtrlEnable( GameValuePar oper1 ) const;

  GameValue ParticleDrop( GameValuePar oper1 ) const;
	
	GameValue CtrlGetText( GameValuePar oper1 ) const;
	GameValue CtrlSetText( GameValuePar oper1 ) const;

	GameValue ButtonGetAction( GameValuePar oper1 ) const;
	GameValue ButtonSetAction( GameValuePar oper1 ) const;

	GameValue LBGetSize( GameValuePar oper1 ) const;
	GameValue LBGetCurSel( GameValuePar oper1 ) const;
	GameValue LBSetCurSel( GameValuePar oper1 ) const;
	GameValue LBClear( GameValuePar oper1 ) const;
	GameValue LBAdd( GameValuePar oper1 ) const;
	GameValue LBDelete( GameValuePar oper1 ) const;
	GameValue LBGetText( GameValuePar oper1 ) const;
	GameValue LBGetData( GameValuePar oper1 ) const;
	GameValue LBSetData( GameValuePar oper1 ) const;
	GameValue LBGetValue( GameValuePar oper1 ) const;
	GameValue LBSetValue( GameValuePar oper1 ) const;
	GameValue LBGetPicture( GameValuePar oper1 ) const;
	GameValue LBSetPicture( GameValuePar oper1 ) const;
	GameValue LBGetColor( GameValuePar oper1 ) const;
	GameValue LBSetColor( GameValuePar oper1 ) const;

	GameValue BriefingOnPlan( GameValuePar oper1 ) const;
	GameValue BriefingOnNotes( GameValuePar oper1 ) const;
	GameValue BriefingOnGear( GameValuePar oper1 ) const;
	GameValue BriefingOnGroup( GameValuePar oper1 ) const;

	GameValue ObjChangeModel( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue ObjAnimate( GameValuePar oper1, GameValuePar oper2 ) const;
	GameValue ObjAnimationPhase( GameValuePar oper1, GameValuePar oper2 ) const;

	GameValue ObjWeaponsFromPool( GameValuePar oper1 ) const;

	// overloaded functions and their helpers
	void InitExt();
	virtual void Init();

//	virtual GameData *CreateGameData( GameType type ) const;
//	virtual RString GetTypeName(GameType type) const;
};
*/

class GameValueExt: public GameValue
{
	public:

	GameValueExt( AIGroup *value ) {_data=new GameDataGroup(value);}
	GameValueExt( Object *value ) {_data=new GameDataObject(value);}
	GameValueExt( GameSideType value ) {_data=new GameDataSide(value);}
	GameValueExt( GameVectorType value );
	GameValueExt( GameTransType value );
	GameValueExt( GameOrientType value );
#if _ENABLE_VBS
	GameValueExt( GameFileType value ) {_data=new GameDataFile(value);}
#endif
};

inline GameValue CreateGameSide( TargetSide side ) {return GameValueExt(side);}


#endif

