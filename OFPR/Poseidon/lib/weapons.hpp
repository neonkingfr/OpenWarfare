#ifdef _MSC_VER
#pragma once
#endif

#ifndef _WEAPONS_HPP
#define _WEAPONS_HPP

#include "paramFileExt.hpp"
#include "vehicle.hpp"
#include "animation.hpp"

enum AmmoSimulation
{ // basic ammo types
	AmmoNone,
	AmmoShotShell,
	AmmoShotMissile,
	AmmoShotRocket,
	AmmoShotBullet,
	AmmoShotIlluminating,
	AmmoShotSmoke,
	AmmoShotTimeBomb,
	AmmoShotPipeBomb,
	AmmoShotMine,
	AmmoShotStroke,
	AmmoShotLaser, //!< laser designator
};

struct SoundProbab: public SoundPars
{
	float _probability;
};

TypeIsMovableZeroed(SoundProbab);

class RandomSound
{
	AutoArray<SoundProbab> _pars;

	public:
	void Load( const ParamEntry &base, const char *name );
	const SoundPars &SelectSound( float probab ) const;
};

class AmmoType: public VehicleNonAIType
{
	typedef VehicleNonAIType base;

	public:
	float hit,indirectHit,indirectHitRange;
	float minRange,minRangeProbab;
	float midRange,midRangeProbab;
	float maxRange,maxRangeProbab;

	float invMidRangeMinusMinRange; // precalculated inverse
	float invMidRangeMinusMaxRange;

	float maxControlRange; // missile parameters
	float maneuvrability;
	float initTime; //!< time to ignition
	float thrustTime; //!< time of burning
	float thrust; //!< how strong the engine is
	float sideAirFriction; //!< air friction (used for missiles / bombs)

	float cost;
	float maxSpeed; // max speed of missiles
	float simulationStep;
	bool irLock; // ir lock only
	bool laserLock; // laser lock only
	bool airLock; // air lock possible
	bool manualControl; // no control out of maxControlRange
	bool explosive;

	Ref<Texture> _texture;
	AmmoSimulation _simulation;
	Ref<VehicleType> _cartridgeType; // what kind of cartridges to we use (effect)
	//! shape that should be used when drawing proxy on vehicle
	Ref<LODShapeWithShadow> _proxyShape;

	PackedColor _tracerColor;   //! tracer color with alpha
	PackedColor _tracerColorR;  //!< tracer color in realistic mode

	float audibleFire;
	float visibleFire;
	float visibleFireTime;

	RandomSound _hitGround;
	RandomSound _hitMan;
	RandomSound _hitArmor;
	RandomSound _hitBuilding;

	//SoundPars _soundHit;
	SoundPars _soundFly;
	SoundPars _soundEngine;
	
	RString _defaultMagazine;
	
	AmmoType( const ParamEntry *name );

	void InitShape();
	void DeinitShape();

	virtual void Load(const ParamEntry &par);

	//const char *GetName() const {return _parClass->GetName();}

	const ParamEntry &ParClass() const {return *_par;}
	const ParamEntry &ParClass( const char *name ) const
	{
		return (*_par)>>name;
	}
};

class WeaponModeType : public RefCount
{
public:
	InitPtr<const ParamEntry> _parClass;
	Ref<const AmmoType> _ammo;	// ammo
	RStringB _displayName;			// used in InGameUI
	int _mult;									// take this from ammo store (bullets in small burst)
	int _burst;
	SoundPars _sound;						// shot sound
	bool _soundContinuous;
	float _reloadTime;					// how long it takes to reload
	float _dispersion;					// dispersion
	int _ffCount;
	RStringB _recoilName;
	//RStringB _recoilNameFixed;  // recoil when lying down
	bool _autoFire;							// full auto weapons
	float _aiRateOfFire; // AI rate of fire at given distance
	float _aiRateOfFireDistance;

	bool _useAction;
	RString _useActionTitle;

public:
	WeaponModeType();
	void Init(const ParamEntry &cls);
	const RStringB &GetName() const {return _parClass->GetName();}
	RStringB GetDisplayName() const {return _displayName;}
};

DECL_ENUM(ManAction)

class MagazineType : public RefCount
{
public:
	InitPtr<const ParamEntry> _parClass;
	int _scope;									// public / protected for briefing
	RStringB _displayName;			// used in action menu
	RStringB _picName;
	RStringB _shortName;				// used in briefing
	RStringB _nameSound;				// used for "LOAD <_nameSound>" sentence
	int _magazineType;					// slots occupied
	int _maxAmmo;								// how much ammo is available
	float _maxLeadSpeed;				// max lead
	float _initSpeed;						// initial ammo speed
	float _invInitSpeed;				// initial ammo speed
	//! used in MP game for decision which body to hide
	float _value;
	RefArray<WeaponModeType> _modes;

	ManAction _reloadAction;
	mutable Ref<LODShapeWithShadow> _model; // model
	mutable AnimationAnimatedTexture _animFire;
	mutable int _modelRefCount;

	mutable Ref<LODShapeWithShadow> _modelMagazine; // weapon model
	mutable int _magazineShapeRef; // how many times is this type weapon actually used?

	bool _useAction;
	RString _useActionTitle;
	
public:
	MagazineType();
	void Init(const char *name);
	const RStringB &GetName() const {return _parClass->GetName();}
	RStringB GetPictureName() const;

	void InitShape() const; // force all used ammo types to load shape
	void DeinitShape() const; // allow all used ammo types to release shape

	void AmmoAddRef() const; // force all used ammo types to load shape
	void AmmoRelease() const; // allow all used ammo types to release shape

	void InitMagazineShape() const; // force all used ammo types to load shape
	void DeinitMagazineShape() const; // allow all used ammo types to release shape

	void MagazineShapeAddRef() const; // force model to be loaded
	void MagazineShapeRelease() const; // allow model to be released

	LSError Serialize(ParamArchive &ar);
	static MagazineType *CreateObject(ParamArchive &ar);

	// TODO: network transfer

	RStringB GetDisplayName() const {return _displayName;}
	RStringB GetNameSound() const {return _nameSound;}
};
TypeIsMovable(MagazineType)

class WeaponType;

class MuzzleType : public RefCount
{
public:
	InitPtr<const ParamEntry> _parClass;
	RStringB _displayName;			// used in InGameUI (when no magazine loaded)
	float _magazineReloadTime;
	SoundPars _sound;						// no shot sound
	SoundPars _reloadSound;
	SoundPars _reloadMagazineSound;
	float _reloadSoundDuration; 
	float _reloadMagazineSoundDuration;
	float _aiDispersionCoefX;
	float _aiDispersionCoefY;

	bool _soundContinuous; // weapon sound is continuous
	bool _enableAttack;
	bool _optics;
	bool _showEmpty;
	bool _autoReload;
	bool _backgroundReload;
	bool _opticsFlare;
	bool _forceOptics;

	int _canBeLocked;
	int _primary; // primary level

	float _opticsZoomMin,_opticsZoomMax;
	float _distanceZoomMin,_distanceZoomMax;

	Ref<LODShapeWithShadow> _opticsModel; // optics model
	Ref<Texture> _cursorTexture;
	Ref<Texture> _cursorAimTexture;

	AnimationAnimatedTexture _animFire;

	// position and direction of muzzle
	Vector3 _muzzlePos, _muzzleDir;
	// position and velocity of outgoing empty cartridge
	Vector3 _cartridgeOutPos, _cartridgeOutVel;
	// point indices
	int _cartridgeOutPosIndex, _cartridgeOutEndIndex;

	int _nModes;												// # of modes
	RefArray<MagazineType> _magazines;	// which ammo can be used
	Ref<MagazineType> _typicalMagazine;	// for type (expected magazine)

public:
	MuzzleType();
	~MuzzleType();

	void Init(const ParamEntry &cls, const WeaponType *weapon);
	void InitShape(const WeaponType *weapon);
	void DeinitShape();

	bool CanUse(const MagazineType *type) const;

	const RStringB &GetName() const {return _parClass->GetName();}
};
TypeIsMovable(MuzzleType)

#define MaskSlotPrimary			0x00000001	// primary weapons
#define MaskSlotSecondary		0x00000010	// secondary weapons
#define MaskSlotItem				0x00000F00	// items
#define MaskSlotBinocular		0x00003000	// binocular
#define MaskHardMounted			0x00010000	// hard mounted
#define MaskSlotHandGun			0x00000002	// hand gun
#define MaskSlotHandGunItem	0x000000E0	// hand gun magazines

inline int GetItemSlotsCount(int slots)
{
	return (slots & MaskSlotItem) >> 8;	// =  / 0x0100 
}

inline int GetHandGunItemSlotsCount(int slots)
{
	return (slots & MaskSlotHandGunItem) >> 5;	// =  / 0x0020
}

/*!
\patch 1.22 Date 09/04/2001 by Jirka
- Added: Support for revolving drum for weapons.
*/
class WeaponType : public RefCount
{
public:
	InitPtr<const ParamEntry> _parClass;
	int _scope;									// public / protected for briefing
	RStringB _displayName;			// used in briefing
	RStringB _picName;			// used in briefing
	Ref<Texture> _picture;			// used in group info (InGameUI)
	int _weaponType;						// slots occupied
	bool _shotFromTurret;
	bool _canDrop;							// can be dropped
	//! infantry weapon have different mass and can influence dexterity
	float _dexterity;
	//! used in MP game for decision which body to hide
	float _value;
	RefArray<MuzzleType> _muzzles;

	mutable Ref<LODShapeWithShadow> _model; // weapon model
	mutable AnimationAnimatedTexture _animFire;
	mutable int _shapeRef; // how many times is this type weapon actually used?

	mutable AnimationRotation _revolving;

public:
	WeaponType();
	void Init(const char *name);
	const RStringB &GetName() const {return _parClass->GetName();}
	RStringB GetPictureName() const;
	Texture *GetPicture() const {return _picture;}

	RStringB GetDisplayName() const {return _displayName;}

	LSError Serialize(ParamArchive &ar);
	static WeaponType *CreateObject(ParamArchive &ar);

	void ShapeAddRef() const; // force model to be loaded
	void ShapeRelease() const; // allow model to be released
	void InitShape() const;
	void DeinitShape() const;
	// TODO: network transfer
};

typedef BankInitArray<MagazineType> MagazineTypeBank;
typedef BankInitArray<WeaponType> WeaponTypeBank;

extern MagazineTypeBank MagazineTypes;
extern WeaponTypeBank WeaponTypes;

template Ref<WeaponModeType>;
template Ref<MuzzleType>;
template Ref<WeaponType>;

#endif

