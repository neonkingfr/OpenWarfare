#ifdef _MSC_VER
#pragma once
#endif

#ifndef _PROGRESS_HPP
#define _PROGRESS_HPP

#include "time.hpp"
#include "font.hpp"

class Progress
{
	// progress bar status
	float _progressTot; // should be in sec (estimated) if possible
	float _progressCur; // current state (starts at 0)
	float _progressDrawn; // state drawn (0..1)

	UITime _progressStartTime; // Glob.uiTime when started
	RString _progressTitle,_progressFormat;
	Ref<Font> _progressFont;
	float _progressFontSize;

	SRef<AbstractOptionsUI> _progressDisplay;

	DWORD _lastProgressRefresh;
	DWORD _progressRefreshBase;

	bool _clear;

	public:
	Progress();
	~Progress();

	private:
	Progress( const Progress &src );
	const Progress &operator = ( const Progress &src );

	public:
	void Reset();
	void Add( float ammount ); // calculate total estimation
	void Advance( float ammount ); // really advance

	void SetPassed( float value ); // set state 
	void SetRest( float value ); // set state 

	// execute in pairs - no nesting allowed
	void Start( RString title, RString format="%s" );
	void Start( RString title, RString format, Ref<Font> font, float size=-1.0 );
	void Start(AbstractOptionsUI *display);
	void Finish();
	void Draw();

	void Frame(); // FinishDraw // Draw // InitDraw
	void Refresh(); // Frame (if neccessary)FinishDraw // ProgressDraw // InitDraw

	bool Active() const;

	void EnableClear(bool enable = true) {_clear = enable;}
};

extern Progress GProgress;

// global progress access
inline void ProgressReset() {GProgress.Reset();}

inline void ProgressAdd( float ammount ) {GProgress.Add(ammount);}
inline void ProgressAdvance( float ammount ) {GProgress.Advance(ammount);}

inline void ProgressSetPassed( float value ) {GProgress.SetPassed(value);}
inline void ProgressSetRest( float value ) {GProgress.SetRest(value);}

// execute in pairs - no nesting allowed
inline void ProgressStart( RString title, RString format="%s" )
{
	GProgress.Start(title,format);
}
inline void ProgressStart( RString title, RString format, Ref<Font> font, float size=-1.0 )
{
	GProgress.Start(title,format,font,size);
}
inline void ProgressStart(AbstractOptionsUI *display)
{
	GProgress.Start(display);
}
inline void ProgressFinish() {GProgress.Finish();}
inline void ProgressDraw() {GProgress.Draw();}

inline void ProgressFrame() {GProgress.Frame();}
inline void ProgressRefresh() {GProgress.Refresh();}

inline void ProgressClear(bool clear = true) {GProgress.EnableClear(clear);}

#endif
