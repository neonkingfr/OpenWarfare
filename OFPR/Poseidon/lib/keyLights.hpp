#ifdef _MSC_VER
#pragma once
#endif

#ifndef _KEYLIGHTS_HPP
#define _KEYLIGHTS_HPP

class KeyLights
{
	bool _globalScrollLock; // normal windows state
	bool _globalCapsLock;

	bool _acquired;

	private:
	static bool GetLock( int code );
	static void ToggleLock( int code );
	
	public:
	static bool GetScrollLock();
	static bool GetCapsLock();
	void SetScrollLock( bool state );
	void SetCapsLock( bool state );

	void GetGlobalState();
	void RestoreGlobalState();
	KeyLights();
	~KeyLights();
};

extern KeyLights KeyState;

#endif
