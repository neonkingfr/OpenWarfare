#ifdef _MSC_VER
#pragma once
#endif

#ifndef _AI_RADIO_HPP
#define _AI_RADIO_HPP

#ifdef PREPROCESS_DOCUMENTATION
#include "networkObject.hpp"
#endif

/******************************************/
/*                                        */
/*              RadioMessages             */
/*                                        */
/******************************************/

enum RadioMessageType
{
	RMTCommand,
	RMTFormation,
	RMTSemaphore,
	RMTBehaviour,
	RMTOpenFire,
	RMTCeaseFire,
	RMTLooseFormation,
	RMTReportStatus,
	RMTTarget,
	RMTGroupAnswer,
	RMTSubgroupAnswer,
	RMTReturnToFormation,
	RMTFireStatus,
	RMTUnitAnswer,
	RMTUnitKilled,
	RMTText,
	RMTReportTarget,
	RMTObjectDestroyed,
	RMTContact, RMTUnderFire, RMTClear,
	RMTRepeatCommand,
	RMTWhereAreYou,
	RMTNotifyCommand,
	RMTCommandConfirm,
	RMTWatchAround,RMTWatchDir,RMTWatchPos,RMTWatchTgt,RMTWatchAuto,
	RMTPosition,
	RMTFormationPos,
	RMTTeam,
	RMTAskSupply,
	RMTSupportAsk,
	RMTSupportConfirm,
	RMTSupportReady,
	RMTSupportDone,
	RMTJoin,
	RMTJoinDone,
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageUnitKilled

class RadioMessageUnitKilled : public RadioMessage
{
protected:
	//OLink<AIGroup> _from;
	OLink<AIUnit> _from;
	OLink<AIUnit> _who;

public:
	RadioMessageUnitKilled(AIUnit *from, AIUnit *who)
	{_from = from; _who = who;}
	RadioMessageUnitKilled() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted();
	int GetType() const {return RMTUnitKilled;}
	AIUnit *GetSender() const {return _from;}
	AIUnit *GetWhoKilled() const {return _who;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageAskSupply

class RadioMessageAskSupply : public RadioMessage
{
protected:
	OLink<AIUnit> _from;
	Command::Message _message;

public:
	RadioMessageAskSupply(AIUnit *from, Command::Message message)
	{_from = from; _message = message;}
	RadioMessageAskSupply() {_message = Command::NoCommand;}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted();
	int GetType() const {return RMTAskSupply;}
	AIUnit *GetSender() const {return _from;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageSupportAsk

// used when group asked for support
class RadioMessageSupportAsk : public RadioMessage
{
protected:
	OLink<AIGroup> _from;
	UIActionType _type;

public:
	RadioMessageSupportAsk(AIGroup *from, UIActionType type)
	{_from = from; _type = type;}
	RadioMessageSupportAsk() {_type = (UIActionType)0;}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted();
	int GetType() const {return RMTSupportAsk;}
	AIUnit *GetSender() const {return _from ? _from->Leader() : NULL;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageSupportConfirm

// used when support group is assigned
class RadioMessageSupportConfirm : public RadioMessage
{
protected:
	OLink<AIGroup> _from;

public:
	RadioMessageSupportConfirm(AIGroup *from) {_from = from;}
	RadioMessageSupportConfirm() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted();
	int GetType() const {return RMTSupportConfirm;}
	AIUnit *GetSender() const {return _from ? _from->Leader() : NULL;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageSupportReady

// used when support group reached rendezvous place
class RadioMessageSupportReady : public RadioMessage
{
protected:
	OLink<AIGroup> _from;

public:
	RadioMessageSupportReady(AIGroup *from)
	{_from = from;}
	RadioMessageSupportReady() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted();
	int GetType() const {return RMTSupportReady;}
	AIUnit *GetSender() const {return _from ? _from->Leader() : NULL;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageSupportDone

// used when supported group already doesn't need support
class RadioMessageSupportDone : public RadioMessage
{
protected:
	OLink<AIGroup> _from;

public:
	RadioMessageSupportDone(AIGroup *from)
	{_from = from;}
	RadioMessageSupportDone() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted();
	int GetType() const {return RMTSupportDone;}
	AIUnit *GetSender() const {return _from ? _from->Leader() : NULL;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageJoin

class RadioMessageJoin : public RadioMessage
{
protected:
	OLink<AIGroup> _from;
	OLinkArray<AIUnit> _to;

public:
	RadioMessageJoin(AIGroup *from, PackedBoolArray list);
	RadioMessageJoin() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted() {}
	int GetType() const {return RMTJoin;}
	AIUnit *GetSender() const {return _from ? _from->Leader() : NULL;}

protected:
	bool IsTo(AIUnit *unit) const;
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageJoinDone

// used when supported group already doesn't need support
class RadioMessageJoinDone : public RadioMessage
{
protected:
	OLink<AIUnit> _from;
	OLink<AIGroup> _to;

public:
	RadioMessageJoinDone(AIUnit *from, AIGroup *to)
	{_from = from; _to = to;}
	RadioMessageJoinDone() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted() {}
	int GetType() const {return RMTJoinDone;}
	AIUnit *GetSender() const {return _from;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageUnitAnswer

class RadioMessageUnitAnswer : public RadioMessage
{
protected:
	OLink<AIUnit> _from;
	OLink<AISubgroup> _to;
	AI::Answer _answer;

public:
	RadioMessageUnitAnswer(AIUnit *from, AISubgroup *to, AI::Answer answer)
	{_from = from; _to = to; _answer = answer;}
	RadioMessageUnitAnswer() {}
	AIUnit *GetFrom() const {return _from;}
	AI::Answer GetAnswer() const {return _answer;}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted();
	int GetType() const {return RMTUnitAnswer;}
	AIUnit *GetSender() const {return _from;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageFireStatus

class RadioMessageFireStatus : public RadioMessage
{
protected:
	OLink<AIUnit> _from;
	bool _answer;

public:
	RadioMessageFireStatus(AIUnit *from, bool answer)
	{_from = from; _answer = answer;}
	RadioMessageFireStatus() {}
	AIUnit *GetFrom() const {return _from;}
	bool GetAnswer() const {return _answer;}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted();
	int GetType() const {return RMTFireStatus;}
	AIUnit *GetSender() const {return _from;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageSubgroupAnswer

class RadioMessageSubgroupAnswer : public RadioMessage
{
protected:
	OLink<AISubgroup> _from;
	OLink<AIUnit> _leader;
	OLink<AIGroup> _to;
	AI::Answer _answer;
	Command _cmd;
	bool _display;
	bool _forceLeader;

public:
	RadioMessageSubgroupAnswer
	(
		AISubgroup *from, AIGroup *to, AI::Answer answer, Command *cmd, bool display = true,
		AIUnit *leader = NULL
	);
	RadioMessageSubgroupAnswer();
	const char *GetPriorityClass();
	LSError Serialize(ParamArchive &ar);
	void Transmitted();
	int GetType() const {return RMTSubgroupAnswer;}
	AISubgroup *GetFrom() const {return _from;}
	AIGroup *GetTo() const {return _to;}
	const Command &GetCommand() const {return _cmd;}
	AIUnit *GetSender() const;

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageGroupAnswer

class RadioMessageGroupAnswer : public RadioMessage
{
protected:
	OLink<AIGroup> _from;
	OLink<AICenter> _to;
	AI::Answer _answer;

public:
	RadioMessageGroupAnswer(AIGroup *from, AICenter *to, AI::Answer answer)
	{_from = from; _to = to; _answer = answer;}
	RadioMessageGroupAnswer() {}
	const char *GetPriorityClass();
	LSError Serialize(ParamArchive &ar);
	void Transmitted();
	int GetType() const {return RMTGroupAnswer;}
	AIUnit *GetSender() const {return _from ? _from->Leader() : NULL;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageReturnToFormation

class RadioMessageReturnToFormation : public RadioMessage
{
protected:
	OLink<AISubgroup> _from;
	OLink<AIUnit> _leader;
	OLink<AIUnit> _to;

public:
	RadioMessageReturnToFormation(AISubgroup *from, AIUnit *to)
	{
		_from = from; _leader = from->Leader(); _to = to;
	}
	RadioMessageReturnToFormation() {}
	const char *GetPriorityClass();
	LSError Serialize(ParamArchive &ar);
	void Transmitted();
	int GetType() const {return RMTReturnToFormation;}
	AIUnit *GetSender() const {return _from && _from->Leader() ? _from->Leader() : (AIUnit*)_leader;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageReply
// abstract class for all reply information

class RadioMessageReply : public RadioMessage
{
protected:
	OLink<AIUnit> _from;
	OLink<AIGroup> _to;

public:
	RadioMessageReply(AIUnit *from, AIGroup *to);
	RadioMessageReply() {}
	LSError Serialize(ParamArchive &ar);
	AIUnit *GetFrom() const {return _from;}
	AIGroup *GetTo() const {return _to;}
	AIUnit *GetSender() const {return _from;}
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageReportTarget

//! report target in group
struct ReportTargetInfo
{
	//! link to group target database
	LLink<Target> tgt; // link 
	//TargetId id;
	//TargetSide side;
	//const VehicleType *type;
	LSError Serialize(ParamArchive &ar);
};
TypeIsMovable(ReportTargetInfo);

class RadioMessageReportTarget : public RadioMessageReply
{
	typedef RadioMessageReply base;

protected:
	ReportSubject _subject;
	int _x;
	int _z;
	AutoArray<ReportTargetInfo> _targets;

public:
	RadioMessageReportTarget(AIUnit *from, AIGroup *to, ReportSubject subject, Target &target);
	//RadioMessageReportTarget(AIUnit *from, AIGroup *to, ReportSubject subject, AITargetInfo &target);
	RadioMessageReportTarget() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted();
	int GetType() const {return RMTReportTarget;}
	ReportSubject GetSubject() {return _subject;}
	bool IsTarget(TargetType *id) const;
	bool HasType(const VehicleType *type) const;
	bool IsInside(Vector3Val pos) const;
	void DeleteTarget(TargetType *id);
	void AddTarget(Target &target);
	//void AddTarget(AITargetInfo &target);

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageNotifyCommand

class RadioMessageNotifyCommand : public RadioMessageReply
{
	typedef RadioMessageReply base;

protected:
	Command _command;

public:
	RadioMessageNotifyCommand(AIUnit *from, AIGroup *to, Command &command);
	RadioMessageNotifyCommand() {}
	const char *GetPriorityClass();
	LSError Serialize(ParamArchive &ar);
	void Transmitted();
	int GetType() const {return RMTNotifyCommand;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageObjectDestroyed

class RadioMessageObjectDestroyed : public RadioMessageReply
{
	typedef RadioMessageReply base;

protected:
	const VehicleType *_vehicleType;

public:
	RadioMessageObjectDestroyed(AIUnit *from, AIGroup *to, const VehicleType *type);
	RadioMessageObjectDestroyed() {_vehicleType = NULL;}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted();
	int GetType() const {return RMTObjectDestroyed;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageContact

class RadioMessageContact : public RadioMessageReply
{
	typedef RadioMessageReply base;
public:
	RadioMessageContact(AIUnit *from, AIGroup *to);
	RadioMessageContact() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted();
	int GetType() const {return RMTContact;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageUnderFire

class RadioMessageUnderFire : public RadioMessageReply
{
	typedef RadioMessageReply base;

public:
	RadioMessageUnderFire(AIUnit *from, AIGroup *to);
	RadioMessageUnderFire() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted();
	int GetType() const {return RMTUnderFire;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageClear

class RadioMessageClear : public RadioMessageReply
{
	typedef RadioMessageReply base;

public:
	RadioMessageClear(AIUnit *from, AIGroup *to);
	RadioMessageClear() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted();
	int GetType() const {return RMTClear;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageRepeatCommand

class RadioMessageRepeatCommand : public RadioMessageReply
{
	typedef RadioMessageReply base;

public:
	RadioMessageRepeatCommand(AIUnit *from, AIGroup *to);
	RadioMessageRepeatCommand() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted();
	int GetType() const {return RMTRepeatCommand;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageWhereAreYou

class RadioMessageWhereAreYou : public RadioMessageReply
{
	typedef RadioMessageReply base;

public:
	RadioMessageWhereAreYou(AIUnit *from, AIGroup *to);
	RadioMessageWhereAreYou() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted();
	int GetType() const {return RMTWhereAreYou;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageCommandConfirm

class RadioMessageCommandConfirm : public RadioMessageReply
{
	typedef RadioMessageReply base;

protected:
	Command _command;

public:
	RadioMessageCommandConfirm(AIUnit *from, AIGroup *to, Command &command);
	RadioMessageCommandConfirm() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted();
	int GetType() const {return RMTCommandConfirm;}
	const Command &GetCommand() const {return _command;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageFormation

class RadioMessageFormation : public RadioMessage
{
protected:
	OLink<AIGroup> _from;
	OLink<AISubgroup> _to;
	AI::Formation _formation;

public:
	RadioMessageFormation(AIGroup *from, AISubgroup *to, AI::Formation formation)
	{_from = from; _to = to; _formation = formation;}
	RadioMessageFormation() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted();
	int GetType() const {return RMTFormation;}

	AIGroup *GetFrom() {return _from;}
	AISubgroup *GetTo() {return _to;}
	AI::Formation GetFormation() {return _formation;}
	void SetFormation(AI::Formation f) {_formation = f;}
	AIUnit *GetSender() const {return _from ? _from->Leader() : NULL;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageState
// abstract class for all state information

class RadioMessageState: public RadioMessage
{
	typedef RadioMessage base;

protected:
	OLink<AIGroup> _from;
	OLinkArray<AIUnit> _to;

public:
	RadioMessageState(AIGroup *from, PackedBoolArray list);
	RadioMessageState();
	virtual RadioMessageState *Clone() const = NULL; 
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	
	AIGroup *GetFrom() const {return _from;}
	bool IsTo(AIUnit *unit) const;
	bool IsOnlyTo(AIUnit *unit) const;
	void DeleteTo(AIUnit *unit);
	void AddTo(AIUnit *unit);
	void ClearTo();
	bool IsToSomeone() const;

	AIUnit *GetSender() const {return _from ? _from->Leader() : NULL;}
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageSemaphore

class RadioMessageSemaphore : public RadioMessageState
{
	typedef RadioMessageState base;

protected:
	AI::Semaphore _semaphore;

public:
	RadioMessageSemaphore(AIGroup *from, PackedBoolArray list, AI::Semaphore semaphore);
	RadioMessageSemaphore() {}
	RadioMessageState *Clone() const {return new RadioMessageSemaphore(*this);}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted();
	int GetType() const {return RMTSemaphore;}
	
	AI::Semaphore GetSemaphore() {return _semaphore;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageBehaviour

class RadioMessageBehaviour : public RadioMessageState
{
	typedef RadioMessageState base;

protected:
	CombatMode _behaviour;

public:
	RadioMessageBehaviour(AIGroup *from, PackedBoolArray list, CombatMode behaviour);
	RadioMessageBehaviour() {}
	RadioMessageState *Clone() const {return new RadioMessageBehaviour(*this);}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted();
	int GetType() const {return RMTBehaviour;}
	
	CombatMode GetBehaviour() {return _behaviour;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageOpenFire

enum OpenFireState;
AI::Semaphore ApplyOpenFire(AI::Semaphore s, OpenFireState open);

class RadioMessageOpenFire : public RadioMessageState
{
	typedef RadioMessageState base;

protected:
	OpenFireState _open;

public:
	RadioMessageOpenFire(AIGroup *from, PackedBoolArray list, OpenFireState open);
	RadioMessageOpenFire() {}
	RadioMessageState *Clone() const {return new RadioMessageOpenFire(*this);}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted();
	int GetType() const {return RMTOpenFire;}
	
	OpenFireState GetOpenFireState() {return _open;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageCeaseFire

class RadioMessageCeaseFire : public RadioMessage
{
	typedef RadioMessage base;

protected:
	OLink<AIUnit> _from;
	OLink<AIUnit> _to;
	bool _insideGroup;

public:
	RadioMessageCeaseFire(AIUnit *from, AIUnit *to, bool insideGroup);
	RadioMessageCeaseFire() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted();
	int GetType() const {return RMTCeaseFire;}
	AIUnit *GetSender() const {return _from;}
	AIUnit *GetTo() const {return _to;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageLooseFormation

AI::Semaphore ApplyLooseFormation(AI::Semaphore s, bool loose);

class RadioMessageLooseFormation : public RadioMessageState
{
	typedef RadioMessageState base;

protected:
	bool _loose;

public:
	RadioMessageLooseFormation(AIGroup *from, PackedBoolArray list, bool loose);
	RadioMessageLooseFormation() {}
	RadioMessageState *Clone() const {return new RadioMessageLooseFormation(*this);}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted();
	int GetType() const {return RMTLooseFormation;}
	
	bool IsLooseFormation() {return _loose;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageUnitPos

class RadioMessageUnitPos : public RadioMessageState
{
	typedef RadioMessageState base;

protected:
	UnitPosition _state;

public:
	RadioMessageUnitPos(AIGroup *from, PackedBoolArray list, UnitPosition state);
	RadioMessageUnitPos() {}
	RadioMessageState *Clone() const {return new RadioMessageUnitPos(*this);}
	LSError Serialize(ParamArchive &ar);
	void Transmitted();
	int GetType() const {return RMTPosition;}
	
	UnitPosition GetSemaphore() {return _state;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageUnitPos

class RadioMessageFormationPos : public RadioMessageState
{
	typedef RadioMessageState base;

protected:
	AI::FormationPos _state;

public:
	RadioMessageFormationPos(AIGroup *from, PackedBoolArray list, AI::FormationPos state);
	RadioMessageFormationPos() {}
	RadioMessageState *Clone() const {return new RadioMessageFormationPos(*this);}
	LSError Serialize(ParamArchive &ar);
	void Transmitted();
	int GetType() const {return RMTFormationPos;}
	
	AI::FormationPos GetFormationPos() {return _state;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageWatchAround

class RadioMessageWatchAround : public RadioMessageState
{
	typedef RadioMessageState base;

public:
	RadioMessageWatchAround(AIGroup *from, PackedBoolArray list);
	RadioMessageWatchAround() {}
	LSError Serialize(ParamArchive &ar);
	void Transmitted();
	int GetType() const {return RMTWatchAround;}
	RadioMessageState *Clone() const {return new RadioMessageWatchAround(*this);}
	
protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageWatchAuto

class RadioMessageWatchAuto : public RadioMessageState
{
	typedef RadioMessageState base;

public:
	RadioMessageWatchAuto(AIGroup *from, PackedBoolArray list);
	RadioMessageWatchAuto() {}
	RadioMessageState *Clone() const {return new RadioMessageWatchAuto(*this);}
	LSError Serialize(ParamArchive &ar);
	void Transmitted();
	int GetType() const {return RMTWatchAuto;}
	
protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageWatchDir

class RadioMessageWatchDir : public RadioMessageState
{
	typedef RadioMessageState base;

protected:
	Vector3 _dir;

public:
	RadioMessageWatchDir(AIGroup *from, PackedBoolArray list, Vector3Val dir);
	RadioMessageWatchDir() {}
	RadioMessageState *Clone() const {return new RadioMessageWatchDir(*this);}
	LSError Serialize(ParamArchive &ar);
	void Transmitted();
	int GetType() const {return RMTWatchDir;}
	Vector3Val GetWatchDirection() const {return _dir;}
	
protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageWatchPos

class RadioMessageWatchPos : public RadioMessageState
{
	typedef RadioMessageState base;

protected:
	Vector3 _pos;

public:
	RadioMessageWatchPos(AIGroup *from, PackedBoolArray list, Vector3Val pos);
	RadioMessageWatchPos() {}
	RadioMessageState *Clone() const {return new RadioMessageWatchPos(*this);}
	LSError Serialize(ParamArchive &ar);
	void Transmitted();
	int GetType() const {return RMTWatchPos;}
	Vector3Val GetWatchPosition() const {return _pos;}
	
protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageWatchTgt

class RadioMessageWatchTgt : public RadioMessageState
{
	typedef RadioMessageState base;

protected:
	LinkTarget _tgt;

public:
	RadioMessageWatchTgt(AIGroup *from, PackedBoolArray list, Target *tgt);
	RadioMessageWatchTgt() {}
	RadioMessageState *Clone() const {return new RadioMessageWatchTgt(*this);}
	LSError Serialize(ParamArchive &ar);
	void Transmitted();
	int GetType() const {return RMTWatchTgt;}
	Target *GetWatchTarget() const {return _tgt;}
	
protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageReportStatus

class RadioMessageReportStatus : public RadioMessageState
{
	typedef RadioMessageState base;

public:
	RadioMessageReportStatus(AIGroup *from, PackedBoolArray list);
	RadioMessageReportStatus() {}
	RadioMessageState *Clone() const {return new RadioMessageReportStatus(*this);}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted();
	int GetType() const {return RMTReportStatus;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageTarget

class RadioMessageTarget : public RadioMessageState
{
	typedef RadioMessageState base;

protected:
	LinkTarget _target;
	bool _engage;
	bool _fire;

public:
	RadioMessageTarget
	(
		AIGroup *from, PackedBoolArray list,Target *target,
		bool engage, bool fire
	);
	RadioMessageTarget();
	RadioMessageState *Clone() const {return new RadioMessageTarget(*this);}
	LSError Serialize(ParamArchive &ar);
	void Transmitted();
	void Canceled();
	int GetType() const {return RMTTarget;}
	Target *GetTarget() const {return _target;}
	const char *GetPriorityClass();

	bool GetEngage() const {return _engage;}
	bool GetFire() const {return _fire;}

	void SetEngage(bool val) {_engage=val;}
	void SetFire(bool val) {_fire=val;}
	
protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageTeam

class RadioMessageTeam : public RadioMessage
{
protected:
	OLink<AIGroup> _from;
	OLinkArray<AIUnit> _to;
	Team _team;

public:
	RadioMessageTeam(AIGroup *from, PackedBoolArray list, Team team);
	RadioMessageTeam() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted();
	int GetType() const {return RMTTeam;}
	AIGroup *GetFrom() const {return _from;}
	bool IsTo(AIUnit *unit) const;
	Team GetTeam() const {return _team;}
	AIUnit *GetSender() const {return _from ? _from->Leader() : NULL;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageCommand

class RadioMessageCommand : public RadioMessage
{
protected:
	OLink<AIGroup> _from;
	OLinkArray<AIUnit> _to;
	Command _command;
	bool _toMainSubgroup;
	bool _transmit;

public:
	RadioMessageCommand(AIGroup *from, PackedBoolArray list, Command &command, bool toMainSubgroup = false, bool transmit = true);
	RadioMessageCommand() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted();
	int GetType() const {return RMTCommand;}
	AIGroup *GetFrom() {return _from;}
	bool IsTo(AIUnit *unit);
	void DeleteTo(AIUnit *unit);
	bool IsToMainSubgroup() const {return _toMainSubgroup;}
	Command::Message GetMessage() {return _command._message;}
	Command::Context GetContext() {return _command._context;}
	AIUnit *GetSender() const {return _from ? _from->Leader() : NULL;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageText

class RadioMessageText : public RadioMessage
{
protected:
	RString _wave;
	OLink<AIUnit> _sender;
	RString _senderName;
	float _timeToLive;

public:
	RadioMessageText(RString wave, RString senderName, float ttl=2.0) {_wave = wave; _senderName = senderName; _timeToLive=ttl;}
	RadioMessageText(RString wave, AIUnit *sender, float ttl=2.0) {_wave = wave; _sender = sender; _timeToLive=ttl;}
	RadioMessageText() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted();
	RString GetWave();
	int GetType() const {return RMTText;}
	float GetDuration() const;
	AIUnit *GetSender() const {return _sender;}
	RString GetSenderName() {return _senderName;}

protected:
	const char *PrepareSentence(SentenceParams &params);
};

#endif


