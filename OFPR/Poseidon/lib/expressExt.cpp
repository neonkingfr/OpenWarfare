
#include "wpch.hpp"
#include <El/Evaluator/express.hpp>

#include <El/ParamFile/paramFile.hpp>
#include "paramArchive.hpp"
//#include "strIncl.hpp"
#include "stringtableExt.hpp"

//! class of callback functions
class ParamArchiveFunctions : public ArchiveFunctions
{
public:
	//! callback function to serialize boolean value
	virtual LSError Serialize(ParamArchive &ar, const RStringB &name, bool &value, int minVersion)
	{return ar.Serialize(name, value, minVersion);}
	//! callback function to serialize boolean value
	virtual LSError Serialize(ParamArchive &ar, const RStringB &name, bool &value, int minVersion, bool defValue)
	{return ar.Serialize(name, value, minVersion, defValue);}
	//! callback function to serialize GameType value
	virtual LSError Serialize(ParamArchive &ar, const RStringB &name, GameType &value, int minVersion)
	{return ar.Serialize(name, value, minVersion);}
	//! callback function to serialize float value
	virtual LSError Serialize(ParamArchive &ar, const RStringB &name, float &value, int minVersion)
	{return ar.Serialize(name, value, minVersion);}
	//! callback function to serialize string value
	virtual LSError Serialize(ParamArchive &ar, const RStringB &name, RString &value, int minVersion)
	{return ar.Serialize(name, value, minVersion);}
	//! callback function to serialize array value
	virtual LSError Serialize(ParamArchive &ar, const RStringB &name, AutoArray<GameValue> &value, int minVersion)
	{return ar.Serialize(name, value, minVersion);}
	//! callback function to serialize game data value
	virtual LSError Serialize(ParamArchive &ar, const RStringB &name, Ref<GameData> &value, int minVersion)
	{return ar.Serialize(name, value, minVersion);}
	//! callback function to serialize game state
	virtual LSError Serialize(ParamArchive &ar, GameState *value);
	//! callback function to create game data
	virtual GameData *CreateGameData(ParamArchive &ar, GameType type);
	//! callback function to check load/save status of archive
	virtual bool IsSaving(ParamArchive &ar)
	{return ar.IsSaving();}
	
	ParamArchiveFunctions() {GameState::SetDefaultArchiveFunctions(this);}
} GParamArchiveFunctions;

LSError ParamArchiveFunctions::Serialize(ParamArchive &ar, GameState *value)
{
	void *old = ar.GetParams();
	ar.SetParams(value);
	CHECK(ar.Serialize("Variables", value->GetVariables(), 1))
	ar.SetParams(old);
	return LSOK;
}

GameData *ParamArchiveFunctions::CreateGameData(ParamArchive &ar, GameType type)
{
	GameState *context = reinterpret_cast<GameState *>(ar.GetParams());
	return context->CreateGameData(type);
}

//! class of callback functions
class GameStateStringtableInfoFunctions : public GameStateInfoFunctions
{
public:

	//! callback function to return error description
	virtual const char *GetErrorString(EvalError error) const;
	//! callback function to return type name
	virtual RString GetTypeName(GameType type) const;
	//! callback function to display error message
	virtual void DisplayErrorMessage(const char *position, const char *error) const;

	GameStateStringtableInfoFunctions() {GameState::SetDefaultInfoFunctions(this);}
} GGameStateStringtableInfoFunctions;

const char *GameStateStringtableInfoFunctions::GetErrorString(EvalError error) const
{
	return LocalizeString(IDS_EVAL_GEN + error - EvalGen);
}

static void CatType(RString &a, RString b)
{
	if (a.GetLength()<=0) a = b;
	else if (b.GetLength()<=0) ;
	else a = a + RString(",") + b;
}

RString GameStateStringtableInfoFunctions::GetTypeName(GameType type) const
{
	if (type==GameVoid) return LocalizeString(IDS_EVAL_TYPEANY);
	RString ret="";
	if (type&GameScalar) CatType(ret,LocalizeString(IDS_EVAL_TYPESCALAR));
	if (type&GameArray) CatType(ret,LocalizeString(IDS_EVAL_TYPEARRAY));
	if (type&GameBool) CatType(ret,LocalizeString(IDS_EVAL_TYPEBOOL));
	if (type&GameString) CatType(ret,LocalizeString(IDS_EVAL_TYPESTRING));
	if (type&GameNothing) CatType(ret,LocalizeString(IDS_EVAL_TYPENOTHING));
	if (type&GameIf) CatType(ret,"if");
	if (type&GameWhile) CatType(ret,"while");
	return ret;
}

void GameStateStringtableInfoFunctions::DisplayErrorMessage(const char *position, const char *error) const
{
	GlobalShowMessage(10000,"'%s': Error %s", position, error);
}
