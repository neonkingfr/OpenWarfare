#ifdef _MSC_VER
#pragma once
#endif

#ifndef _TRACKS_HPP
#define _TRACKS_HPP

#include "vehicle.hpp"

enum {TrackLODLevels=1}; // auto build several LOD levels

#include <Es/Memory/normalNew.hpp>

class TrackStep: public Vehicle //,public CLTLink<1>
{
	private:
	Time _startTime;
	float _alpha; // current alpha value
	Ref<Texture> _texture;

	public:

	TrackStep(float alpha, Texture *texture);
	~TrackStep();

	void Change
	(
		int level,
		Vector3Par lastLeft, Vector3Par lastRight, float lastV,
		Vector3Par left, Vector3Par right, float v
	);
	void Final(); // perform surface split
	void StartTime();
	Time GetStartTime() const {return _startTime;}
	//bool Disappear( float deltaT ); // disappear a litte
	//void AnimateAlpha( int level );
	void Simulate( float deltaT, SimulationImportance prec );

	void UpdateAlpha();

	bool IsAnimated( int level ) const; // appearence changed with Animate
	bool IsAnimatedShadow( int level ) const; // shadow changed with Animate
	void Animate( int level );
	void Deanimate( int level );

	bool MustBeSaved() const {return false;}

	int PassNum( int lod );

	USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

//! mark left by tracks (tanks and other caterpillar vehicles)

class TrackDraw: public RefCount
{
	struct TrackLODLevel
	{
		bool _initDone;
		Vector3 _lastL,_lastR;
		//Vector3 _currL,_currR;
		float _lastV; // update v depending on distance
		TrackLODLevel():_initDone(false),_lastV(0){}
	};
	TrackLODLevel _lods[TrackLODLevels];
	 
	Ref<TrackStep> _lastPart;
	
	bool _offsets;
	Vector3 _lOffset,_rOffset; // points in object model
	
	Texture *_texture;
	float _alpha;
	
	public:
	TrackDraw();
	void SetOffsets
	(
		Vector3Par lOffset, Vector3Par rOffset,
		Texture *texture, float alpha
	);
	TrackStep *Update( const Frame &pos, bool force );

	Vector3Val LeftPos() const {return _lOffset;}
	Vector3Val RightPos() const {return _rOffset;}
	const Vector3 CenterPos() const
	{
		Vector3 sum = _lOffset+_rOffset;
		return sum*0.5;
	}
	
	void Skip( const Frame &pos );
};

#include <Es/Memory/normalNew.hpp>

//! footstep mark left by men walking
class Mark: public Vehicle
{
	float _alpha,_alphaSpeed;

	public:
	Mark( LODShapeWithShadow *shape, float alpha=1.0, float timeToLive=10 );
	~Mark();

	void Simulate( float deltaT, SimulationImportance prec );

	void UpdateAlpha();

	bool IsAnimated( int level ) const;
	bool IsAnimatedShadow( int level ) const;
	int PassNum( int lod );
	void Animate( int level );
	void Deanimate( int level );

	bool MustBeSaved() const {return false;}

	USE_FAST_ALLOCATOR;
};

#include <Es/Memory/debugNew.hpp>

class TrackAccumulator
{
	protected:
	Ref<TrackStep> _accumulate;
	bool Merge( TrackStep *merge );
	void Terminate();
};

class TrackOptimized: public TrackAccumulator
{
	TrackDraw _left,_right;
	
	public:
	TrackOptimized( const LODShape *lShape );
	void Update( const Frame &pos, float deltaT, bool terminate );

	Vector3 LeftPos() const {return _left.CenterPos();}
	Vector3 RightPos() const {return _right.CenterPos();}
};

class TrackOptimizedFour: public TrackAccumulator
{
	Ref<TrackStep> _accumulate;
	TrackDraw _fLeft,_fRight;
	TrackDraw _bLeft,_bRight;
	
	public:
	TrackOptimizedFour( const LODShape *lShape );
	void Update( const Frame &pos, float deltaT, bool terminate );

	Vector3 BackLeftPos() const {return _bLeft.CenterPos();}
	Vector3 BackRightPos() const {return _bRight.CenterPos();}
	Vector3 FrontLeftPos() const {return _fLeft.CenterPos();}
	Vector3 FrontRightPos() const {return _fRight.CenterPos();}
};

#endif
