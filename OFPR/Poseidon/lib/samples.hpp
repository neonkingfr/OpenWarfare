// DirectX sound system
// (C) 1996, SUMA
#ifndef _SAMPLES_HPP
#define _SAMPLES_HPP

// Function declarations
#if defined _WIN32
  #if !defined _XBOX
    #include <mmsystem.h>
  #endif
#else

typedef struct
{
    WORD        wFormatTag;         /* format type */
    WORD        nChannels;          /* number of channels (i.e. mono, stereo...) */
    DWORD       nSamplesPerSec;     /* sample rate */
    DWORD       nAvgBytesPerSec;    /* for buffer estimation */
    WORD        nBlockAlign;        /* block size of data */
    WORD        wBitsPerSample;     /* number of bits per sample of mono data */
    WORD        cbSize;             /* the count in bytes of the size of */
				    /* extra information (after cbSize) */
} WAVEFORMATEX;

#endif

#include <El/QStream/fileCompress.hpp>

#include <Es/Memory/normalNew.hpp>

class QFBank;

class WaveBuffer
{
	Ref<IFileBuffer> _buffer;
	int _skip;
	int _size;
	int _deltaPack;

	// enable streaming delta-pack
	mutable int _lastWriteOffset;
	mutable int _lastValue;

	public:
	WAVEFORMATEX _format; // carry wave format with it

	public:
	//WaveBuffer( int size, const WAVEFORMATEX &format );

	WaveBuffer
	(
		IFileBuffer *buf, const WAVEFORMATEX &format, int start, int size, int deltaPack
	);


	char *Data() {return (char *)_buffer->GetData()+_skip;}
	const char *Data() const {return (char *)_buffer->GetData()+_skip;}
	int PackedSize() const {return _size;}
	int GetFormat() const {return _deltaPack;}
	void GetWaveFormat(WAVEFORMATEX &format) const {format=_format;}

	bool IsFromBank(QFBank *bank) const;

	int UnpackedSize() const
	{
		int size=_size;
		if( _deltaPack==8 ) return size*2;
		if( _deltaPack==4 ) return size*4;
		return size;
	}
	// unpack whole buffer
	void Unpack( void *dest, int destSize ) const;
	// unpack part of the buffer
	int UnpackPart(void *dest, int offset, int size) const;

	USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

// **********************************************
// class WaveStream
// interface to stream data from any source
// implemenatation of some basic functions

class WaveStream: public RefCount
{
	public:
	// get some specific part of data
	// note: function should return 0 when reading after end of the data
	// it should anyway fill data with silence

	virtual int GetUncompressedSize() const = NULL;
	virtual void GetFormat(WAVEFORMATEX &format) const = NULL;
	virtual int GetData(void *data, int offset, int size) const = NULL;
	virtual bool IsFromBank(QFBank *bank) const = NULL;

	// clipped data retreival - zeroes otside of valid range
	virtual int GetDataClipped(void *data, int offset, int size) const;
	// when end is reached, loop
	virtual int GetDataLooped(void *data, int offset, int size) const;
};



// file sounds.cpp

//WaveBuffer *SSSLoadFile( const char *name );

// file wave.cpp

WaveStream *WaveLoadFile( const char *name );
WaveStream *WSSLoadFile( const char *name );

WaveStream *SoundLoadFile(const char *name);

#endif
