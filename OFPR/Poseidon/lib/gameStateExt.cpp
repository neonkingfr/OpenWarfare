#include "wpch.hpp"

#include "object.hpp"
#include "detector.hpp"
#include "objectClasses.hpp"
#include "fireplace.hpp"

#include "gameStateExt.hpp"

#include "world.hpp"
#include "landscape.hpp"
#include "global.hpp"
#include "vehicleAI.hpp"
#include "ai.hpp"
#include "aiRadio.hpp"
#include "uiMap.hpp"
#include "soldierOld.hpp"
#include "car.hpp"
#include "house.hpp"
#include "dynSound.hpp"
#include "cameraHold.hpp"
#include "network.hpp"
#include "resincl.hpp"

#include "chat.hpp"
#include "stringtableExt.hpp"

#include "uiActions.hpp"

#include <ctype.h>
#include <time.h>
#ifdef _WIN32
  #include <io.h>
#endif

#include "keyInput.hpp"
#include "dikCodes.h"

#include <El/QStream/QBStream.hpp>
#include <El/PreprocC/Preproc.h>

/*!
\patch 1.21 Date 08/20/2001 by Jirka
- Added: functions requiredVersion
- Added: functions getMarkerType, getMarkerSize, getMarkerColor, getWpPos
- Added: functions setMarkerSize, setMarkerColor, setWpPos
\patch 1.27 Date 10/12/2001 by Jirka
- Added: functions forceMap, mapAnimClear, mapAnimAdd, mapAnimCommit, mapAnimDone
\patch 1.27 Date 10/16/2001 by Jirka
- Added: function selectWeapon
\patch 1.28 Date 10/22/2001 by Jirka
- Added: function scudState
\patch 1.34 Date 12/05/2001 by Jirka
- Added: functions createVehicle, createUnit, deleteVehicle
\patch 1.35 Date 12/10/2001 by Jirka
- Added: function estimatedTimeLeft
\patch 1.75 Date 1/31/2002 by Jirka
- Added: user defined dialogs
	- functions createDialog, closeDialog
	- functions ctrlVisible, ctrlEnabled, ctrlShow, ctrlEnable, ctrlText, ctrlSetText
	- functions buttonAction, buttonSetAction
	- functions lbSize, lbCurSel, lbSetCurSel, lbClear, lbAdd, lbDelete
	- functions lbText, lbData, lbSetData, lbValue, lbSetValue, lbPicture, lbSetPicture, lbColor, lbSetColor
\patch 1.75 Date 2/4/2002 by Jirka
- Added: functions saveStatus, loadStatus
\patch 1.75 Date 2/4/2002 by Jirka
- Added: functions addWeaponPool, addMagazinePool
- Added: functions putWeaponPool, pickWeaponPool
- Added: functions clearWeaponPool, clearMagazinePool
- Added: functions queryWeaponPool, queryMagazinePool
\patch 1.75 Date 2/8/2002 by Vladimir
- Added: function drop 
\patch 1.75 Date 2/11/2002 by Jirka
- Added: functions onBriefingPlan, onBriefingNotes, onBriefingGear, onBriefingGroup
\patch 1.75 Date 3/6/2002 by Jirka
- Added: functions saveIdentity, loadIdentity
\patch 1.75 Date 3/7/2002 by Jirka
- Added: functions animate, animationPhase 
\patch 1.75 Date 3/7/2002 by Jirka
- Added: function fillWeaponsFromPool
\patch 1.75 Date 4/12/2002 by Jirka
- Added: function skill, setSkill
- Added: function weapons, magazines
\patch 1.75 Date 4/15/2002 by Jirka
- Added: function primaryWeapons, secondaryWeapons
\patch 1.75 Date 4/23/2002 by Jirka
- Added: function setObjectTexture
\patch 1.75 Date 4/29/2002 by Jirka
- Added: function object
\patch 1.53 Date 5/2/2002 by Jirka
- Added: new version of say: <object> say [<sound>, <maxTitlesDistance>] 
\patch 1.53 Date 4/26/2002 by Ondra
- New: Scripting function <time> setRain <rain_density>.
\patch 1.56 Date 5/10/2002 by Jirka
- Added: function cheatsEnabled
\patch 1.56 Date 5/10/2002 by Jirka
- Added: function cameraOn
- Added: functions deleteStatus, deleteIdentity
\patch 1.62 Date 6/3/2002 by Jirka
- Added: optional parameter for function say - titles speed 
\patch 1.78 Date 7/16/2002 by Jirka
- Added: functions (nular) sideEnemy, sideFriendly
\patch 1.78 Date 7/18/2002 by Jirka
- Added: function (nular) dialog
\patch_internal 1.79 Date 7/30/2002 by Jirka
- Added: functions for file manipulations: newConfig, openConfig, saveConfig, openClass, addClass, getValue, addValue
\patch 1.79 Date 7/31/2002 by Jirka
- Added: functions sliderPosition, sliderSetPosition, sliderRange, sliderSetRange, sliderSpeed, sliderSetSpeed
\patch 1.80 Date 8/1/2002 by Jirka
- Added: functions velocity, setVelocity
\patch 1.80 Date 8/6/2002 by Jirka
- Added: functions missionName, missionStart, playersNumber
\patch_internal 1.80 Date 8/6/2002 by Jirka
- Added: VBS functions VBS_addHeader, VBS_addEvent, VBS_addFooter, VBS_kills, VBS_killed, VBS_injuries
\patch_internal 1.81 Date 8/7/2002 by Jirka
- Added: function listConfigNames
\patch 1.82 Date 8/9/2002 by Ondra
- New: Scripting: Functions loadFile and preprocessFile added.
\patch_internal 1.85 Date 9/6/2002 by Jirka
- Added: functions DBG_switchLandscape, DBG_screenshot
\patch 1.86 Date 10/9/2002 by Jirka
- Added: function sideLogic 
\patch_internal 1.86 Date 10/10/2002 by Jirka
- Added: functions setDate, createCenter, deleteCenter, setFriend, createGroup, deleteGroup
- Added: functions createMarker, deleteMarker, setMarkerText, setMarkerShape, setMarkerBrush, setMarkerDir
- Added: functions createTrigger, setTriggerArea, setTriggerActivation, setTriggerType, setTriggerTimeout
- Added: functions setTriggerText, setTriggerStatements, triggerAttachObject, triggerAttachVehicle
- Added: functions setEffectCondition, setCameraEffect, setSoundEffect, setMusicEffect, setTitleEffect, synchronizeWaypoint
- Added: function createGuardedPoint
- Added: functions addWaypoint, setWaypointPosition, setWaypointType, waypointAttachVehicle, waypointAttachObject
- Added: functions setWaypointHousePosition, setWaypointCombatMode, setWaypointFormation, setWaypointSpeed
- Added: functions setWaypointBehaviour, setWaypointDescription, setWaypointStatements, setWaypointScript
- Added: functions setWaypointTimeout, showWaypoint, deleteWaypoint
\patch 1.90 Date 10/31/2002 by Jirka
- Added: functions isEngineOn, engineOn
\patch 1.97 Date 4/1/2004 by Jirka
- Added: functions getPosASL, setPosASL
- Added: function getWorld
*/


#define NOTHING GameValue()

DEFINE_FAST_ALLOCATOR(GameDataObject)

RString GameDataObject::GetText() const
{
	if( _value==NULL ) return "<NULL-object>";
	return _value->GetDebugName();
}

bool GameDataObject::IsEqualTo(const GameData *data) const
{
	const Object *val1 = GetObject();
	const Object *val2 = static_cast<const GameDataObject *>(data)->GetObject();
	return val1==val2;
}

LSError GameDataObject::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar));
	CHECK(ar.SerializeRef("value", _value, 1))
	return LSOK;
}

DEFINE_FAST_ALLOCATOR(GameDataGroup)

RString GameDataGroup::GetText() const
{
	if( _value==NULL ) return "<NULL-group>";
	return _value->GetDebugName();
}

bool GameDataGroup::IsEqualTo(const GameData *data) const
{
	const AIGroup *val1 = GetGroup();
	const AIGroup *val2 = static_cast<const GameDataGroup *>(data)->GetGroup();
	return val1==val2;
}

LSError GameDataGroup::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar));
	CHECK(ar.SerializeRef("value", _value, 1))
	return LSOK;
}

DEFINE_FAST_ALLOCATOR(GameDataSide)

RString GameDataSide::GetText() const
{
	return FindEnumName(_value);
}

bool GameDataSide::IsEqualTo(const GameData *data) const
{
	const TargetSide val1 = GetSide();
	const TargetSide val2 = static_cast<const GameDataSide *>(data)->GetSide();
	return val1==val2;
}

LSError GameDataSide::Serialize(ParamArchive &ar)
{
	CHECK( base::Serialize(ar) );
	CHECK( ar.SerializeEnum("value", _value, 1));
	return LSOK;
}

#if _ENABLE_VBS
struct ConfigFileEntry
{
	ParamClass *cls;
	int refCount;

	ConfigFileEntry() {cls = NULL; refCount = 0;}
	ConfigFileEntry(ParamClass *c) {cls = c; refCount = 0;}
};
TypeIsMovableZeroed(ConfigFileEntry)
static class ConfigFileEntries : public AutoArray<ConfigFileEntry>
{
	typedef AutoArray<ConfigFileEntry> base;
public:
	int Add(const ConfigFileEntry &item);
}
GFileEntries;

int ConfigFileEntries::Add(const ConfigFileEntry &item)
{
	for (int i=0; i<Size(); i++)
	{
		if (Get(i).cls == NULL)
		{
			Assert(Get(i).refCount == 0);
			Set(i) = item;
			return i;
		}
	}
	return base::Add(item);
}

static void FileEntryAddRef(ParamClass *cls)
{
	Assert(cls);
	const ParamClass *file = cls->GetFile();
	for (int i=0; i<GFileEntries.Size(); i++)
	{
		if (GFileEntries[i].cls == file)
		{
			GFileEntries[i].refCount++;
			break;
		}
	}
}

static void FileEntryRelease(ParamClass *cls)
{
	Assert(cls);
	const ParamClass *file = cls->GetFile();
	for (int i=0; i<GFileEntries.Size(); i++)
	{
		if (GFileEntries[i].cls == file)
		{
			if (--GFileEntries[i].refCount <= 0)
			{
				for (int j=0; j<GFileEntries.Size(); j++)
				{
					if (GFileEntries[j].cls && GFileEntries[j].cls->GetFile() == file)
					{
						GFileEntries[j].cls = NULL;
						Assert(GFileEntries[j].refCount == 0);
					}
				}
				delete static_cast<const ParamFile *>(file);
			}
			break;
		}
	}
}

GameFileType::GameFileType(const GameFileType &src)
{
	readOnly = src.readOnly;
	_index = -1;
	SetIndex(src.GetIndex());
}

void GameFileType::operator = (const GameFileType &src)
{
	readOnly = src.readOnly;
	SetIndex(src.GetIndex());
}

void GameFileType::SetIndex(int index)
{
	if (_index >= 0)
	{
		ParamClass *cls = GFileEntries[_index].cls;
		if (cls) FileEntryRelease(cls);
	}

	_index = index;
	
	if (index >= 0)
	{
		ParamClass *cls = GFileEntries[index].cls;
		if (cls) FileEntryAddRef(cls);
	}
}

GameFileType::~GameFileType()
{
	if (_index >= 0)
	{
		ParamClass *cls = GFileEntries[_index].cls;
		if (cls) FileEntryRelease(cls);
	}
}

DEFINE_FAST_ALLOCATOR(GameDataFile)

RString GameDataFile::GetText() const
{
	if (_value.GetIndex() < 0) return "No file";
	ParamClass *entry = GFileEntries[_value.GetIndex()].cls;
	if (!entry) return "Destructed file";
	return entry->GetName();
}

bool GameDataFile::IsEqualTo(const GameData *data) const
{
	const GameFileType val1 = GetFile();
	const GameFileType val2 = static_cast<const GameDataFile *>(data)->GetFile();
	return val1.GetIndex() == val2.GetIndex();
}

LSError GameDataFile::Serialize(ParamArchive &ar)
{
	CHECK( base::Serialize(ar) );
	// do not serialize
	if (ar.IsLoading())
	{
		_value.SetIndex(-1);
		_value.readOnly = false;
	}

	return LSOK;
}
#endif
/*
GameStateExt::GameStateExt()
{
	InitExt();
}

GameStateExt::~GameStateExt()
{
}
*/

const GameType GameObjectOrGroup(GameObject|GameGroup);
const GameType GameObjectOrArray(GameObject|GameArray);

GameData *CreateGameDataObject() {return new GameDataObject();}
GameData *CreateGameDataGroup() {return new GameDataGroup();}
GameData *CreateGameDataSide() {return new GameDataSide();}
#if _ENABLE_VBS
GameData *CreateGameDataFile() {return new GameDataFile();}
#endif
// TODO: vector, transform and orientation

GameValue CreateGameObject(Object *obj) {return GameValueExt(obj);}

#define OBJECT_NULL GameValueExt((Object *)NULL)
#define GROUP_NULL GameValueExt((AIGroup *)NULL)

#define DEFINE_COMMAND(Xxx) \
	GameValue VehDo##Xxx( const GameState *state, GameValuePar oper1, GameValuePar oper2 ) \
	{ \
		return Veh##Xxx(state, oper1, oper2, true); \
	} \
	GameValue VehCommand##Xxx( const GameState *state, GameValuePar oper1, GameValuePar oper2 ) \
	{ \
		return Veh##Xxx(state, oper1, oper2, false); \
	}

#define DEFINE_COMMAND_S(Xxx) \
	GameValue VehDo##Xxx( const GameState *state, GameValuePar oper1 ) \
	{ \
		return Veh##Xxx(state, oper1, true); \
	} \
	GameValue VehCommand##Xxx( const GameState *state, GameValuePar oper1 ) \
	{ \
		return Veh##Xxx(state, oper1, false); \
	}

/*
GameData *GameStateExt::CreateGameData( GameType type ) const
{
	if( type==GameObject ) return new GameDataObject();
	else if( type==GameGroup ) return new GameDataGroup();
	else if( type==GameSide ) return new GameDataSide();
	//else if( type==GameVector ) return new GameDataVector();
	// TODO: vector, transform and orientation
	else
	{
		return base::CreateGameData(type);
	}	
}
*/

/*
static void CatType(RString &a, RString b)
{
	if (a.GetLength()<=0) a = b;
	else if (b.GetLength()<=0) ;
	else a = a + RString(",") + b;
}
RString GameStateExt::GetTypeName(GameType type) const
{
	if (type==GameVoid) return LocalizeString(IDS_EVAL_TYPEANY);
	RString ret = "";
	if (type&GameObject ) CatType(ret,LocalizeString(IDS_EVAL_TYPEOBJECT));
	if (type&GameGroup ) CatType(ret,LocalizeString(IDS_EVAL_TYPEGROUP));
	if (type&GameSide ) CatType(ret,LocalizeString(IDS_EVAL_TYPESIDE));
	if (type&GameVector) CatType(ret,LocalizeString(IDS_EVAL_TYPEVECTOR));
	if (type&GameTrans) CatType(ret,LocalizeString(IDS_EVAL_TYPETRANS));
	if (type&GameOrient) CatType(ret,LocalizeString(IDS_EVAL_TYPEORIENT));
	CatType(ret,base::GetTypeName(type));
	return ret;
}
*/

GameValue ObjNull(const GameState *state)
{
	return OBJECT_NULL;
}

GameValue GrpNull(const GameState *state)
{
	return GROUP_NULL;
}

GameValue ObjIsNull( const GameState *state, GameValuePar oper1 )
{
	if (oper1.GetType() == GameObject)
	{
		Object *obj = static_cast<GameDataObject *>(oper1.GetData())->GetObject();
		return !obj;
	}
	else return true;
}

GameValue GrpIsNull( const GameState *state, GameValuePar oper1 )
{
	if (oper1.GetType() == GameGroup)
	{
		AIGroup *grp = static_cast<GameDataGroup *>(oper1.GetData())->GetGroup();
		return !grp;
	}
	else return true;
}

Object *GetObject( GameValuePar oper )
{
	if (oper.GetNil()) return NULL;
	if( oper.GetType()==GameObject )
	{
		return static_cast<GameDataObject *>(oper.GetData())->GetObject();
	}
	if( oper.GetType()==GameGroup )
	{
		AIGroup *grp = static_cast<GameDataGroup *>(oper.GetData())->GetGroup();
		if (!grp) return NULL;
		AIUnit *leader = grp->Leader();
		if (!leader) return NULL;
		return leader->GetPerson();
	}
	return NULL;
}

static AIGroup *GetGroup( GameValuePar oper )
{
	if (oper.GetNil()) return NULL;
	if( oper.GetType()==GameGroup )
	{
		return static_cast<GameDataGroup *>(oper.GetData())->GetGroup();
	}
	if( oper.GetType()==GameObject )
	{
		Object *obj = static_cast<GameDataObject *>(oper.GetData())->GetObject();
		EntityAI *vai = dyn_cast<EntityAI>(obj);
		if (!vai) return NULL;
		AIUnit *unit = vai->CommanderUnit();
		if (!unit) return NULL;
		return unit->GetGroup();
	}
	return NULL;
}

static Target *FindTargetReveal(AIGroup *grp, EntityAI *veh)
{
	Target *tgt = grp->FindTarget(veh);
	if (tgt) return tgt;
	// reveal target
	return grp->AddTarget(veh,0.3,0.3,3.0f);
}


GameValue ObjAlive( const GameState *state, GameValuePar oper1 )
{
	Object *obj=GetObject(oper1);
	if( !obj ) return false;
	return (!obj->IsDammageDestroyed());
}

GameValue ObjCanMove( const GameState *state, GameValuePar oper1 )
{
	Object *obj=GetObject(oper1);
	if( !obj ) return false;
	if (obj->IsDammageDestroyed()) return false;
	EntityAI *ai=dyn_cast<EntityAI>(obj);
	if( !ai ) return false;
	return ai->IsAbleToMove();
}

GameValue ObjCanFire( const GameState *state, GameValuePar oper1 )
{
	Object *obj=GetObject(oper1);
	if( !obj ) return false;
	if (obj->IsDammageDestroyed()) return false;
	EntityAI *ai=dyn_cast<EntityAI>(obj);
	if( !ai ) return false;
	return ai->IsAbleToFire();
}

GameValue ObjIsLocal( const GameState *state, GameValuePar oper1 )
{
	Object *obj=GetObject(oper1);
	if (!obj) return false;
	return obj->IsLocal();
}

GameValue ObjFlee( const GameState *state, GameValuePar oper1 )
{
	Object *obj=GetObject(oper1);
	if( !obj ) return false;
	if (obj->IsDammageDestroyed()) return false;
	EntityAI *ai=dyn_cast<EntityAI>(obj);
	if( !ai ) return false;
	AIGroup *grp=ai->GetGroup();
	if( !grp ) return false;
	return grp->GetFlee();
}


GameValue ObjSetFuel( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj=GetObject(oper1);
	if( !obj ) return NOTHING;
	if (obj->IsDammageDestroyed()) return NOTHING;
	EntityAI *ai=dyn_cast<EntityAI>(obj);
	if( !ai ) return NOTHING;
	float maxFuel = ai->GetType()->GetFuelCapacity();
	float ammount = oper2;
	ai->Refuel(ammount*maxFuel-ai->GetFuel());
	return NOTHING;
}

GameValue ObjFuel( const GameState *state, GameValuePar oper1 )
{
	Object *obj=GetObject(oper1);
	if( !obj ) return 0.0f;
	if (obj->IsDammageDestroyed()) return 0.0f;
	EntityAI *ai=dyn_cast<EntityAI>(obj);
	if( !ai ) return 0.0f;
	float maxFuel=ai->GetType()->GetFuelCapacity();
	float fuel=maxFuel<=0 ? 1 : ai->GetFuel()/maxFuel;
	return fuel;
}

GameValue ObjSetFuelCargo( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj=GetObject(oper1);
	if( !obj ) return NOTHING;
	if (obj->IsDammageDestroyed()) return NOTHING;
	VehicleSupply *veh=dyn_cast<VehicleSupply>(obj);
	if( !veh ) return NOTHING;
	float max = veh->GetType()->GetMaxFuelCargo();
	float current = veh->GetFuelCargo();
	float ammount = oper2;
	veh->LoadFuelCargo(ammount * max - current);
	return NOTHING;
}

GameValue ObjSetRepairCargo( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj=GetObject(oper1);
	if( !obj ) return NOTHING;
	if (obj->IsDammageDestroyed()) return NOTHING;
	VehicleSupply *veh=dyn_cast<VehicleSupply>(obj);
	if( !veh ) return NOTHING;
	float max = veh->GetType()->GetMaxRepairCargo();
	float current = veh->GetRepairCargo();
	float ammount = oper2;
	veh->LoadRepairCargo(ammount * max - current);
	return NOTHING;
}

GameValue ObjSetAmmoCargo( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj=GetObject(oper1);
	if( !obj ) return NOTHING;
	if (obj->IsDammageDestroyed()) return NOTHING;
	VehicleSupply *veh=dyn_cast<VehicleSupply>(obj);
	if( !veh ) return NOTHING;
	float max = veh->GetType()->GetMaxAmmoCargo();
	float current = veh->GetAmmoCargo();
	float ammount = oper2;
	veh->LoadAmmoCargo(ammount * max - current);
	return NOTHING;
}

GameValue ObjSetInfantryAmmoCargo( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	/*
	Object *obj=GetObject(oper1);
	if( !obj ) return NOTHING;
	if (obj->IsDammageDestroyed()) return NOTHING;
	VehicleSupply *veh=dyn_cast<VehicleSupply>(obj);
	if( !veh ) return NOTHING;
	float max = veh->GetType()->GetMaxInfantryAmmoCargo();
	float current = veh->GetInfantryAmmoCargo();
	float ammount = oper2;
	veh->LoadInfantryAmmoCargo(ammount * max - current);
	*/
	return NOTHING;
}

GameValue ObjClearWeaponCargo( const GameState *state, GameValuePar oper1 )
{
	Object *obj = GetObject(oper1);
	if (!obj) return NOTHING;
	if (obj->IsDammageDestroyed()) return NOTHING;
	VehicleSupply *veh = dyn_cast<VehicleSupply>(obj);
	if (!veh) return NOTHING;
	veh->ClearWeaponCargo();
	return NOTHING;
}

GameValue ObjClearMagazineCargo( const GameState *state, GameValuePar oper1 )
{
	Object *obj = GetObject(oper1);
	if (!obj) return NOTHING;
	if (obj->IsDammageDestroyed()) return NOTHING;
	VehicleSupply *veh = dyn_cast<VehicleSupply>(obj);
	if (!veh) return NOTHING;
	veh->ClearMagazineCargo();
	return NOTHING;
}

GameValue ObjAddWeaponCargo( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj = GetObject(oper1);
	if (!obj) return NOTHING;
	if (obj->IsDammageDestroyed()) return NOTHING;
	VehicleSupply *veh = dyn_cast<VehicleSupply>(obj);
	if (!veh) return NOTHING;
	// add cargo only to local objects
	// it will be transferred to remote
	// if (!veh->IsLocal()) return NOTHING;

	const GameArrayType &array = oper2;
	if (array.Size() != 2) return NOTHING;
	if
	(
		array[0].GetType() != GameString ||
		array[1].GetType() != GameScalar
	) return NOTHING;

	RString name = array[0];
	Ref<WeaponType> weapon = WeaponTypes.New(name);
	if (!weapon) return NOTHING;
	int count = toInt((float)array[1]);
	veh->AddWeaponCargo(weapon, count, true);

	return NOTHING;
}

GameValue ObjAddMagazineCargo( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj = GetObject(oper1);
	if (!obj) return NOTHING;
	if (obj->IsDammageDestroyed()) return NOTHING;
	VehicleSupply *veh = dyn_cast<VehicleSupply>(obj);
	if (!veh) return NOTHING;

	const GameArrayType &array = oper2;
	if (array.Size() != 2) return NOTHING;
	if
	(
		array[0].GetType() != GameString ||
		array[1].GetType() != GameScalar
	) return NOTHING;

	RString name = array[0];
	Ref<MagazineType> magazine = MagazineTypes.New(name);
	if (!magazine) return NOTHING;
	int count = toInt((float)array[1]);
	veh->AddMagazineCargo(magazine, count,true);
	return NOTHING;
}

GameValue ObjSelectWeapon( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj = GetObject(oper1);
	if (!obj) return NOTHING;
	if (obj->IsDammageDestroyed()) return NOTHING;
	EntityAI *veh = dyn_cast<EntityAI>(obj);
	if (!veh) return NOTHING;

	RString name = oper2;
	for (int i=0; i<veh->NMagazineSlots(); i++)
	{
		const MuzzleType *muzzle = veh->GetMagazineSlot(i)._muzzle;
		if (muzzle && stricmp(muzzle->GetName(), name) == 0)
		{
			veh->SelectWeapon(i);
			return NOTHING;
		}
	}
	return NOTHING;
}

GameValue ObjExperience( const GameState *state, GameValuePar oper1 )
{
	Object *obj=GetObject(oper1);
	if( !obj ) return 0.0f;
	if (obj->IsDammageDestroyed()) return 0.0f;
	EntityAI *ai=dyn_cast<EntityAI>(obj);
	if( !ai ) return 0.0f;
	AIUnit *unit=ai->CommanderUnit();
	if( !unit ) return 0.0f;
	return unit->GetPerson()->GetExperience();
}

GameValue ObjGetSkill( const GameState *state, GameValuePar oper1 )
{
	Object *obj=GetObject(oper1);
	if( !obj ) return 0.0f;
	if (obj->IsDammageDestroyed()) return 0.0f;
	EntityAI *ai=dyn_cast<EntityAI>(obj);
	if( !ai ) return 0.0f;
	AIUnit *unit=ai->CommanderUnit();
	if( !unit ) return 0.0f;
	return unit->GetAbility();
}

GameValue ObjGetPrimaryWeapon( const GameState *state, GameValuePar oper1 )
{
	Object *obj=GetObject(oper1);
	if( !obj ) return "";
	EntityAI *ai=dyn_cast<EntityAI>(obj);
	if( !ai ) return "";

	for (int i=0; i<ai->NWeaponSystems(); i++)
	{
		const WeaponType *weapon = ai->GetWeaponSystem(i);
		if (!weapon) continue;
		if ((weapon->_weaponType & MaskSlotPrimary) != 0)
			return weapon->GetName();
	}

	return "";
}

GameValue ObjGetSecondaryWeapon( const GameState *state, GameValuePar oper1 )
{
	Object *obj=GetObject(oper1);
	if( !obj ) return "";
	EntityAI *ai=dyn_cast<EntityAI>(obj);
	if( !ai ) return "";

	for (int i=0; i<ai->NWeaponSystems(); i++)
	{
		const WeaponType *weapon = ai->GetWeaponSystem(i);
		if (!weapon) continue;
		if
		(
			(weapon->_weaponType & MaskSlotSecondary) != 0 &&
			(weapon->_weaponType & MaskSlotPrimary) == 0
		)
			return weapon->GetName();
	}

	return "";
}

GameValue ObjGetAllWeapons( const GameState *state, GameValuePar oper1 )
{
	GameValue value = state->CreateGameValue(GameArray);
	GameArrayType &array = value;

	Object *obj=GetObject(oper1);
	if( !obj ) return value;
	EntityAI *ai=dyn_cast<EntityAI>(obj);
	if( !ai ) return value;

	for (int i=0; i<ai->NWeaponSystems(); i++)
	{
		const WeaponType *weapon = ai->GetWeaponSystem(i);
		if (!weapon) continue;
		if (weapon->_weaponType == 0) continue;
		array.Add(weapon->GetName());
	}

	return value;
}

GameValue ObjGetAllMagazines( const GameState *state, GameValuePar oper1 )
{
	GameValue value = state->CreateGameValue(GameArray);
	GameArrayType &array = value;

	Object *obj=GetObject(oper1);
	if( !obj ) return value;
	EntityAI *ai=dyn_cast<EntityAI>(obj);
	if( !ai ) return value;

	for (int i=0; i<ai->NMagazines(); i++)
	{
		const Magazine *magazine = ai->GetMagazine(i);
		if (!magazine) continue;
		const MagazineType *type = magazine->_type;
		if (type) array.Add(type->GetName());
	}

	return value;
}

GameValue GetObject( const GameState *state, GameValuePar oper1 )
{
	int id = toInt((float)oper1);
	Object *obj = GLandscape->GetObject(id);
	if (!obj) return GameValueExt((Object *)NULL);
	if (obj->GetType()!=Primary && obj->GetType()!=Network)
	{
		return GameValueExt((Object *)NULL);
	}
	return GameValueExt(obj);
}

GameValue ObjSetCaptive(const GameState *state, GameValuePar oper1,GameValuePar oper2)
{
	Object *obj=GetObject(oper1);
	if( !obj ) return NOTHING;
	if (obj->IsDammageDestroyed()) return NOTHING;
	EntityAI *ai=dyn_cast<EntityAI>(obj);
	if( !ai ) return NOTHING;
	AIUnit *unit=ai->CommanderUnit();
	if( !unit ) return NOTHING;
	unit->SetCaptive(oper2);
	return NOTHING;
}

GameValue ObjCaptive(const GameState *state, GameValuePar oper1)
{
	Object *obj=GetObject(oper1);
	if( !obj ) return false;
	if (obj->IsDammageDestroyed()) return false;
	EntityAI *ai=dyn_cast<EntityAI>(obj);
	if( !ai ) return false;
	AIUnit *unit=ai->CommanderUnit();
	if( !unit ) return false;
	return unit->GetCaptive();
}

GameValue ObjAddExperience(const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj=GetObject(oper1);
	if( !obj ) return NOTHING;
	if (obj->IsDammageDestroyed()) return NOTHING;
	EntityAI *ai=dyn_cast<EntityAI>(obj);
	if( !ai ) return NOTHING;
	AIUnit *unit=ai->CommanderUnit();
	if( !unit ) return NOTHING;
	float exp = oper2;
	unit->GetPerson()->GetInfo()._experience += exp;
	return NOTHING;
}

GameValue ObjSetSkill( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj=GetObject(oper1);
	if( !obj ) return NOTHING;
	if (obj->IsDammageDestroyed()) return NOTHING;
	EntityAI *ai=dyn_cast<EntityAI>(obj);
	if( !ai ) return NOTHING;
	AIUnit *unit=ai->CommanderUnit();
	if( !unit ) return NOTHING;
	float skill = oper2;
	saturate(skill, 0, 1);
	unit->SetAbility(skill);
	return NOTHING;
}

GameValue ObjAddScore( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj=GetObject(oper1);
	if( !obj ) return NOTHING;
	EntityAI *ai=dyn_cast<EntityAI>(obj);
	if( !ai ) return NOTHING;
	AIUnit *unit=ai->CommanderUnit();
	if( !unit ) return NOTHING;
	int score = toInt((float)oper2);
	TargetSide playerSide = TSideUnknown;
	AIGroup *grp = unit->GetGroup();
	if (grp)
	{
		AICenter *center = grp->GetCenter();
		if (center) playerSide = center->GetSide();
	}
	GStats._mission.AddMPScore(unit->GetPerson()->GetInfo()._name, playerSide, score);
	return NOTHING;
}

GameValue ObjGetScore( const GameState *state, GameValuePar oper1 )
{
	Object *obj=GetObject(oper1);
	if( !obj ) return 0.0f;
	EntityAI *ai=dyn_cast<EntityAI>(obj);
	if( !ai ) return 0.0f;
	AIUnit *unit=ai->CommanderUnit();
	if( !unit ) return 0.0f;
	RString name = unit->GetPerson()->GetInfo()._name;
	for (int i=0; i<GStats._mission._tableMP.Size(); i++)
	{
		const AIStatsMPRow &row = GStats._mission._tableMP[i];
		if (stricmp(name, row.player) == 0) return (float)row.killsTotal;
	}
	return 0.0f;
}

GameValue ObjSomeAmmo( const GameState *state, GameValuePar oper1 )
{
	Object *obj=GetObject(oper1);
	if( !obj ) return false;
	if (obj->IsDammageDestroyed()) return false;
	EntityAI *ai=dyn_cast<EntityAI>(obj);
	if( !ai ) return false;
	for (int i=0; i<ai->NMagazines(); i++)
	{
		Magazine *magazine = ai->GetMagazine(i);
		if (!magazine) continue;
		if (magazine->_ammo > 0) return true;
	}
	return false;
}

GameValue ObjDistance( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj1=GetObject(oper1);
	Object *obj2=GetObject(oper2);
	if( !obj1 || !obj2 ) return 1e10f;

	Vector3 pos1 = obj1->WorldPosition();
	Vector3 pos2 = obj2->WorldPosition();

	return pos1.Distance(pos2);
}

GameValue ObjListIn( const GameState *state, GameValuePar oper1 )
{
	GameValue value = state->CreateGameValue(GameArray);
	GameArrayType &array = value;

	Object *obj1=GetObject(oper1);
	if( !obj1 ) return value;
	
	Transport *trans=dyn_cast<Transport>(obj1);
	if( !trans )
	{
		array.Add(GameValueExt(obj1));
		return value;
	}
	// add crew and cargo
	#define ADD_MAN(man) {Object *obj=man;if(obj) array.Add(GameValueExt(obj));}
	ADD_MAN(trans->Driver());
	ADD_MAN(trans->Commander());
	ADD_MAN(trans->Gunner());
	const ManCargo &cargo = trans->GetManCargo();
	for( int i=0; i<cargo.Size(); i++ ) ADD_MAN(cargo[i]);
	#undef ADD_MAN

	return value;

}

GameValue ObjIn( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj1=GetObject(oper1);
	Object *obj2=GetObject(oper2);
	if( !obj1 || !obj2 ) return false;
	
	EntityAI *ai=dyn_cast<EntityAI>(obj1);
	EntityAI *trans=dyn_cast<EntityAI>(obj2);
	if( !ai || !trans ) return false;

	AIUnit *unit=ai->CommanderUnit();
	if( !unit ) return false; // unit is dead - cannot be in

	return ( unit->GetVehicle()==trans );
}

GameValue ObjAmmo( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	EntityAI *ai=dyn_cast<EntityAI>(GetObject(oper1));
	if( !ai ) return 0.0f;
	GameStringType weapon=oper2;
	// scan all weapons
	for (int i=0; i<ai->NMagazineSlots(); i++)
	{
		const MagazineSlot &slot = ai->GetMagazineSlot(i);
		if (!slot._magazine) continue;
		if (strcmpi(slot._muzzle->GetName(), weapon) != 0) continue;
		return float(slot._magazine->_ammo);
	}
	return 0.0f;
}

GameValue ObjHasWeapon( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	EntityAI *veh = dyn_cast<EntityAI>(GetObject(oper1));
	if (!veh) return false;

	GameStringType name = oper2;
	for (int i=0; i<veh->NWeaponSystems(); i++)
	{
		const WeaponType *weapon = veh->GetWeaponSystem(i);
		if (!weapon) continue;
		if (stricmp(weapon->GetName(), name) == 0) return true;
	}
	return false;
}

GameValue ObjAddWeapon( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	EntityAI *ai=dyn_cast<EntityAI>(GetObject(oper1));
	if( !ai ) return NOTHING;
	GameStringType weapon=oper2;
	// TODO: do not add hardmounted weapons
	// if (!ai->IsLocal()) return NOTHING;
	ai->AddWeapon(weapon);
	ai->AutoReloadAll();
	return NOTHING;
}

GameValue ObjRemoveWeapon( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	EntityAI *ai=dyn_cast<EntityAI>(GetObject(oper1));
	if( !ai ) return NOTHING;
	// if (!ai->IsLocal()) return NOTHING;
	GameStringType weapon=oper2;
	ai->RemoveWeapon(weapon);
	return NOTHING;
}

GameValue ObjRemoveAllWeapons( const GameState *state, GameValuePar oper1 )
{
	EntityAI *ai=dyn_cast<EntityAI>(GetObject(oper1));
	if( !ai ) return NOTHING;
	// if (!ai->IsLocal()) return NOTHING;
	ai->MinimalWeapons();
	return NOTHING;
}

GameValue ObjAddMagazine( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	EntityAI *ai=dyn_cast<EntityAI>(GetObject(oper1));
	if( !ai ) return NOTHING;
	// if (!ai->IsLocal()) return NOTHING;
	GameStringType magazine=oper2;
	// TODO: do not add hardmounted weapons
	ai->AddMagazine(magazine);
	ai->AutoReloadAll();
	return NOTHING;
}

GameValue ObjRemoveMagazine( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	EntityAI *ai=dyn_cast<EntityAI>(GetObject(oper1));
	if( !ai ) return NOTHING;
	// if (!ai->IsLocal()) return NOTHING;
	GameStringType magazine=oper2;
	ai->RemoveMagazine(magazine);
	return NOTHING;
}

GameValue ObjRemoveMagazines( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	EntityAI *ai=dyn_cast<EntityAI>(GetObject(oper1));
	if( !ai ) return NOTHING;
	// if (!ai->IsLocal()) return NOTHING;
	GameStringType magazine=oper2;
	ai->RemoveMagazines(magazine);
	return NOTHING;
}

GameValue ObjFire( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj = GetObject(oper1);
	if (!obj) return NOTHING;
	EntityAI *veh = dyn_cast<EntityAI>(obj);
	if (!veh) return NOTHING;
	// if (!veh->IsLocal()) return NOTHING;

	GameStringType muzzle = oper2;

	// scan all muzzles
	for (int i=0; i<veh->NMagazineSlots(); i++)
	{
		const MagazineSlot &slot = veh->GetMagazineSlot(i);
		if (stricmp(slot._muzzle->GetName(), muzzle) != 0) continue;

		// muzzle found
		if (!slot._magazine) return NOTHING;
		if
		(
			slot._magazine->_type->_maxAmmo > 0 &&
			slot._magazine->_ammo == 0
		) return NOTHING;
		
		veh->ForceFire(i);
		return NOTHING;
	}
	return NOTHING;
}

GameValue ObjFireEx( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj = GetObject(oper1);
	if (!obj) return NOTHING;
	EntityAI *veh = dyn_cast<EntityAI>(obj);
	if (!veh) return NOTHING;
	// if (!veh->IsLocal()) return NOTHING;

	const GameArrayType &array = oper2;
	GameStringType muzzle;
	GameStringType mode;
	GameStringType magazine;

	switch (array.Size())
	{
	case 3:
		if (array[2].GetType() != GameString) return NOTHING;
		magazine = array[2];
	case 2:
		if (array[0].GetType() != GameString) return NOTHING;
		muzzle = array[0];
		if (array[1].GetType() != GameString) return NOTHING;
		mode = array[1];
		break;
	default:
		return NOTHING;
	}

	// scan all muzzles
	for (int i=0; i<veh->NMagazineSlots(); i++)
	{
		const MagazineSlot &slot = veh->GetMagazineSlot(i);
		if (stricmp(slot._muzzle->GetName(), muzzle) != 0) continue;

		// muzzle found
		if (magazine.GetLength() > 0)
		{
			Ref<MagazineType> type = MagazineTypes.New(magazine);
			int best = veh->FindBestMagazine(type, 0);
			if (best >= 0)
			{
				veh->AttachMagazine(slot._muzzle, veh->GetMagazine(best));
			}
		}
		if (!slot._magazine) return NOTHING;

		// check mode
		const WeaponModeType *modeType = veh->GetWeaponMode(i);
		Assert(modeType);
		if (stricmp(modeType->GetName(), mode) != 0) continue;

		if
		(
			slot._magazine->_type->_maxAmmo > 0 &&
			slot._magazine->_ammo == 0
		) return NOTHING;
		
		veh->ForceFire(i);
		return NOTHING;
	}
	return NOTHING;
}

GameValue ObjCmpE( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj1=GetObject(oper1);
	Object *obj2=GetObject(oper2);
	if( !obj1 || !obj2 ) return false;
	return obj1==obj2;
}

GameValue ObjCmpNE( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj1=GetObject(oper1);
	Object *obj2=GetObject(oper2);
	if( !obj1 || !obj2 ) return true;
	return obj1!=obj2;
}

GameValue GrpCmpE( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	AIGroup *obj1=GetGroup(oper1);
	AIGroup *obj2=GetGroup(oper2);
	if( !obj1 || !obj2 ) return false;
	return obj1==obj2;
}

GameValue GrpCmpNE( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	AIGroup *obj1=GetGroup(oper1);
	AIGroup *obj2=GetGroup(oper2);
	if( !obj1 || !obj2 ) return true;
	return obj1!=obj2;
}

GameValue ObjBehaviour( const GameState *state, GameValuePar oper1 )
{
	Object *obj=GetObject(oper1);
	if (!obj) return "ERROR";
	EntityAI *veh = dyn_cast<EntityAI>(obj);
	if (!veh) return "ERROR";
	AIUnit *unit=veh->CommanderUnit();
	if (!unit) return "ERROR";
	CombatMode mode = unit->GetCombatMode();
	const EnumName *names = GetEnumNames(mode);
	for (int i=0; names[i].IsValid(); i++)
	{
		if (names[i].value == mode) return names[i].name;
	}
	return "ERROR";
}

GameValue GrpCombatMode( const GameState *state, GameValuePar oper1 )
{
	AIGroup *grp1=GetGroup(oper1);
	if (!grp1) return "ERROR";
	AI::Semaphore sem = grp1->GetSemaphore();
	const EnumName *names = GetEnumNames(sem);
	for (int i=0; names[i].IsValid(); i++)
	{
		if (names[i].value == sem) return names[i].name;
	}
	return "ERROR";
}

GameValue GrpFormation( const GameState *state, GameValuePar oper1 )
{
	AIGroup *grp = GetGroup(oper1);
	if (!grp) return "ERROR";
	AISubgroup *subgrp = grp->MainSubgroup();
	if (!subgrp) return "ERROR";
	AI::Formation form = subgrp->GetFormation();
	const EnumName *names = GetEnumNames(form);
	for (int i=0; names[i].IsValid(); i++)
	{
		if (names[i].value == form) return names[i].name;
	}
	return "ERROR";
}

GameValue GrpSpeedMode( const GameState *state, GameValuePar oper1 )
{
	AIGroup *grp = GetGroup(oper1);
	if (!grp) return "ERROR";
	AISubgroup *subgrp = grp->MainSubgroup();
	if (!subgrp) return "ERROR";
	SpeedMode mode = subgrp->GetSpeedMode();
	const EnumName *names = GetEnumNames(mode);
	for (int i=0; names[i].IsValid(); i++)
	{
		if (names[i].value == mode) return names[i].name;
	}
	return "ERROR";
}

GameValue GrpSetBehaviour( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	AIGroup *grp1=GetGroup(oper1);
	if (!grp1) return NOTHING;
	GameStringType str=oper2;
	CombatMode mode = CMUnchanged;	// avoid warning
	const EnumName *names = GetEnumNames(mode);
	for (int i=0; names[i].IsValid(); i++)
	{
		if (stricmp(names[i].name, str) == 0)
		{
			mode = (CombatMode)names[i].value;
			grp1->SetCombatModeMajor(mode);
			return NOTHING;
		}
	}
	return NOTHING;
}

GameValue ObjSetUnitPos( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj = GetObject(oper1);
	if (!obj) return NOTHING;

	Person *veh = dyn_cast<Person>(obj);
	if (!veh) return NOTHING;

	AIUnit *unit = veh->Brain();
	if (!unit) return NOTHING;

	GameStringType str = oper2;
	UnitPosition pos = UPAuto;	// avoid warning
	const EnumName *names = GetEnumNames(pos);
	for (int i=0; names[i].IsValid(); i++)
	{
		if (stricmp(names[i].name, str) == 0)
		{
			pos = (UnitPosition)names[i].value;
			unit->SetUnitPosition(pos);
			return NOTHING;
		}
	}
	return NOTHING;
}

GameValue GrpSetCombatMode( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	AIGroup *grp1=GetGroup(oper1);
	if (!grp1) return NOTHING;
	GameStringType str=oper2;
	AI::Semaphore sem = AI::SemaphoreBlue;	// avoid warning
	const EnumName *names = GetEnumNames(sem);
	for (int i=0; names[i].IsValid(); i++)
	{
		if (stricmp(names[i].name, str) == 0)
		{
			sem = (AI::Semaphore)names[i].value;
			grp1->SetSemaphore(sem);
			//grp1->SendSemaphore(sem, grp1->MainSubgroup()->GetUnitsList());

			PackedBoolArray all;
			for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
			{
				AIUnit *unit = grp1->UnitWithID(i + 1);
				if (!unit) continue;
				all.Set(i,true);
			}
			grp1->SendSemaphore(sem, all);

			return NOTHING;
		}
	}
	return NOTHING;
}

GameValue GrpSetFormation( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	AIGroup *grp = GetGroup(oper1);
	if (!grp) return NOTHING;
	AISubgroup *subgrp = grp->MainSubgroup();
	if (!subgrp) return NOTHING;
	GameStringType str = oper2;
	AI::Formation form = AI::FormColumn;	// avoid warning
	const EnumName *names = GetEnumNames(form);
	for (int i=0; names[i].IsValid(); i++)
	{
		if (stricmp(names[i].name, str) == 0)
		{
			form = (AI::Formation)names[i].value;
			grp->SendFormation(form, subgrp);
			return NOTHING;
		}
	}
	return NOTHING;
}

GameValue GrpSetSpeedMode( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	AIGroup *grp = GetGroup(oper1);
	if (!grp) return NOTHING;
	AISubgroup *subgrp = grp->MainSubgroup();
	if (!subgrp) return NOTHING;
	GameStringType str = oper2;
	SpeedMode mode = SpeedNormal;	// avoid warning
	const EnumName *names = GetEnumNames(mode);
	for (int i=0; names[i].IsValid(); i++)
	{
		if (stricmp(names[i].name, str) == 0)
		{
			mode = (SpeedMode)names[i].value;
			subgrp->SetSpeedMode(mode);
			return NOTHING;
		}
	}
	return NOTHING;
}

GameValue GrpLockWP( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	AIGroup *grp1=GetGroup(oper1);
	if (!grp1) return NOTHING;
	grp1->LockWP(oper2);
	return NOTHING;
}

GameValue GrpSetFormDir( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	// set formation direction of main subgroup
	Ref<AIGroup> grp = GetGroup(oper1);
	if (!grp) return NOTHING;
	AISubgroup *sub = grp->MainSubgroup();
	float angle = oper2;
	Matrix3 rotY(MRotationY, -HDegree(angle));
	if (sub) sub->SetDirection(rotY.Direction());
	return NOTHING;
}

GameValue Player(const GameState *state)
{
	Person *soldier=GWorld->PlayerOn();
	OLink<Object> player=(Object *)soldier;
	return GameValueExt(player.GetLink());
}

GameValue CameraOn(const GameState *state)
{
	Object *object = GWorld->CameraOn();
	return GameValueExt(object);
}

GameValue GameTime(const GameState *state)
{
	return Glob.time-::Time(0);
}

GameValue DayTime(const GameState *state)
{
	return Glob.clock.GetTimeOfDay()*24.0f;
}

GameValue Benchmark(const GameState *state)
{
	return Glob.config.benchmark;
}

GameValue CadetMode(const GameState *state)
{
	return Glob.config.easyMode;
}

GameValue SaveVar(const GameState *state, GameValuePar oper1)
{
	RString name = oper1;
	name.Lower();
	GameVariable &var = const_cast<GameVariable &>(state->GetVariables()[name]);
	if (state->IsNull(var)) return NOTHING;

	GStats._campaign.AddVariable(var);

	return NOTHING;
}

GameValue GetAcceleratedTime(const GameState *state)
{
	return GWorld->GetAcceleratedTime();
}

GameValue SetAcceleratedTime(const GameState *state, GameValuePar oper1)
{
	GWorld->SetAcceleratedTime(oper1);
	return NOTHING;
}

int ListCountSideH( const GameArrayType &array, TargetSide side )
{
	int count=0;
	for( int i=0; i<array.Size(); i++ )
	{
		const GameValue &val=array[i];
		Object *obj=GetObject(val);
		if( !obj ) continue;
		EntityAI *ai=dyn_cast<EntityAI>(obj);
		if( !ai ) continue;
		if( ai->GetTargetSide()==side ) count++;
	}
	return count;
}

int ListCountSideH( const GameArrayType &array, Object *obj, IsSide func )
{
	EntityAI *ai = dyn_cast<EntityAI>(obj);
	if( !ai ) return 0;
	AIGroup *grp = ai->GetGroup();
	if( !grp ) return 0;

	int count = 0;
	AICenter *center = grp->GetCenter();
	for( int i=0; i<array.Size(); i++ )
	{
		Object *obj = GetObject(array[i]);
		if( !obj ) continue;
		EntityAI *ai=dyn_cast<EntityAI>(obj);
		if( !ai ) continue;

		const AITargetInfo *ctgt = center->FindTargetInfo(ai);
		TargetSide side=TSideUnknown;
		if( ctgt ) side=ctgt->_side;
		/*
		Target *tgt=grp->FindTarget(ai);
		if( !tgt ) continue;
		*/
		if( (center->*func)(side) ) count++;
	}
	return count;
}

int ListCountTypeH( const GameArrayType &array, const EntityType *type )
{
	int count = 0;
	for( int i=0; i<array.Size(); i++ )
	{
		Object *obj = GetObject(array[i]);
		if( !obj ) continue;
		EntityAI *ai=dyn_cast<EntityAI>(obj);
		if( !ai ) continue;

		if( ai->GetType()->IsKindOf(type) ) count++;
	}
	return count;
}

GameValue ListCountSide( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	return (float)ListCountSideH(oper2,GetSide(oper1));
}

GameValue ListCountEnemy( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	return (float)ListCountSideH(oper2,GetObject(oper1),&AICenter::IsEnemy);
}

GameValue ListCountFriendly( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	return (float)ListCountSideH(oper2,GetObject(oper1),&AICenter::IsFriendly);
}
GameValue ListCountUnknown( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	return (float)ListCountSideH(oper2,GetObject(oper1),&AICenter::IsUnknown);
}

/*!
\patch 1.91 Date 12/2/2002 by Ondra
- New: New scripting function typeOf <object>.
*/

GameValue ObjGetType( const GameState *state, GameValuePar oper1)
{
	Object *obj = GetObject(oper1);
	Entity *ent = dyn_cast<Entity>(obj);
	RString type = "";
	if (ent)
	{
		const EntityType *etype = ent->GetNonAIType();
		if (etype) type = etype->GetName();
	}
	return type;
}

GameValue ListCountType( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	RString name=oper1;
	const EntityType *type=VehicleTypes.New(name);
	return (float)ListCountTypeH((const GameArrayType&)oper2,type);
}

GameValue ListAllowGetIn( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	const GameArrayType &array = oper1;
	bool allow = oper2;
	for (int i=0; i<array.Size(); i++)
	{
		Object *obj = GetObject(array[i]);
		if (!obj) continue;
		EntityAI *ai = dyn_cast<EntityAI>(obj);
		if (!ai) continue;
		AIUnit *unit = ai->CommanderUnit();
		if (!unit) continue;
		unit->AllowGetIn(allow);
	}

	return NOTHING;
}

GameValue ListOrderGetIn( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	const GameArrayType &array = oper1;
	bool allow = oper2;
	for (int i=0; i<array.Size(); i++)
	{
		Object *obj = GetObject(array[i]);
		if (!obj) continue;
		EntityAI *ai = dyn_cast<EntityAI>(obj);
		if (!ai) continue;
		AIUnit *unit = ai->CommanderUnit();
		if (!unit) continue;
		unit->OrderGetIn(allow);
	}

	return NOTHING;
}

/*!
\patch 1.36 Date 12/14/2001 by Jirka
- Fixed: Crash when joining more than 12 units into group
\patch 1.43 Date 1/28/2002 by Jirka
- Fixed: Crash in joining to empty group 
*/

void ProcessJoinGroups(OLinkArray<AIUnit> &units, AIGroup *group)
{
	Ref<AIGroup> grp = group;

	Person *player = GWorld->GetRealPlayer();
	AIUnit *playerUnit = player ? player->Brain() : NULL;

	AUTO_STATIC_ARRAY(AIUnit *,joined, 32)
	if (grp)
	{
		// add units to group
		for (int i=0; i<units.Size(); i++)
		{
			if (grp->NUnits() >= MAX_UNITS_PER_GROUP) break;

			Ref<AIUnit> unit = (AIUnit*)units[i];
			if (!unit) continue;
			AIGroup *g = unit->GetGroup();
			if (!g) continue;
			// send only when not player or near units
			bool sendJoin = grp->IsAnyPlayerGroup() || !grp->Leader() ||
				grp->Leader()->Position().Distance2(unit->Position()) < Square(200);

			int id = -1;
			if (GWorld->GetMode() != GModeNetware && unit == playerUnit) id = unit->ID();
			unit->ForceRemoveFromGroup();
			grp->AddUnit(unit, id);
			joined.Add(unit);

			if (!sendJoin)
			{
				AISubgroup *subgrp = new AISubgroup();
				grp->AddSubgroup(subgrp);
				subgrp->AddUnit(unit);
				subgrp->SelectLeader(unit);
				if (GWorld->GetMode() == GModeNetware)
				{
					GetNetworkManager().CreateObject(subgrp);
					GetNetworkManager().UpdateObject(subgrp);
				}
			}
			if (g->NUnits() == 0)
			{
				g->RemoveFromCenter();
			}
		}
	}
	else
	{
		AICenter *center = NULL;
		for (int i=0; i<units.Size(); i++)
		{
			AIUnit *unit = units[i];
			if (!unit) continue;
			AIGroup *g = unit->GetGroup();
			if (!g) continue;
			AICenter *c = g->GetCenter();
			if (!c) continue;
			center = c;
			break;
		}
		// no units to add
		if (!center) return;
		// cannot add group
		if (center->NGroups() >= MaxGroups) return;

		grp = new AIGroup();
		center->AddGroup(grp);
		
		// send mission
		Mission mis;
		mis._action = Mission::Arcade;
		center->SendMission(grp, mis);

		// add units to main subgroup
		for (int i=0; i<units.Size(); i++)
		{
			if (grp->NUnits() >= MAX_UNITS_PER_GROUP) break;

			Ref<AIUnit> unit = (AIUnit*)units[i];
			if (!unit) continue;
			AIGroup *g = unit->GetGroup();
			if (!g) continue;
			int id = -1;
			if (GWorld->GetMode() != GModeNetware && unit == playerUnit) id = unit->ID();
			unit->ForceRemoveFromGroup();
			grp->AddUnit(unit, id);
			joined.Add(unit);
			if (g->NUnits() == 0)
			{
				g->RemoveFromCenter();
			}
		}
		if (GWorld->GetMode() == GModeNetware)
		{
			GetNetworkManager().CreateObject(grp);
			for (int i=0; i<grp->NSubgroups(); i++)
				if (grp->GetSubgroup(i))
					GetNetworkManager().CreateObject(grp->GetSubgroup(i));
		}
	}

	// update values for flee
	grp->CalculateMaximalStrength();

	if (grp->NUnits() == 0) return;

	// select leader
	AICenter *center = grp->GetCenter();
	if (!grp->Leader()) center->SelectLeader(grp);
	Assert(grp->Leader());

	if (grp->NWaypoints() == 0)
		grp->AddFirstWaypoint(grp->Leader()->Position());

	// special case - formation leader inside vehicle
	if (!grp->MainSubgroup()->Leader() || !grp->MainSubgroup()->Leader()->IsUnit())
		grp->MainSubgroup()->SelectLeader();

	// radio
	GWorld->SetActiveChannels();
	PackedBoolArray list;
	Command cmd; cmd._message = Command::Join; cmd._context = Command::CtxMission;
	for (int i=0; i<joined.Size(); i++) list.Set(joined[i]->ID() - 1, true);
	grp->GetRadio().Transmit
	(
		new RadioMessageJoin(grp, list),
		grp->GetCenter()->GetLanguage()
	);
	for (int i=0; i<joined.Size(); i++)
		if (joined[i] != playerUnit)
		{
			grp->GetRadio().Transmit
			(
				new RadioMessageJoinDone(joined[i], grp),
				grp->GetCenter()->GetLanguage()
			);
		}

	// allow get in by leader
	bool allow = grp->Leader()->IsGetInAllowed();
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = grp->UnitWithID(i + 1);
		if (unit) unit->AllowGetIn(allow);
	}

	if (GWorld->GetMode() == GModeNetware)
	{
		GetNetworkManager().UpdateObject(grp->MainSubgroup());
		GetNetworkManager().UpdateObject(grp);
	}
}

TypeIsSimple(AIUnit *)

GameValue ListJoin( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	const GameArrayType &array = oper1;
	AIGroup *grp = GetGroup(oper2);

	OLinkArray<AIUnit> units;
	for (int i=0; i<array.Size(); i++)
	{
		Object *obj = GetObject(array[i]);
		if (!obj) continue;
		EntityAI *ai = dyn_cast<EntityAI>(obj);
		if (!ai) continue;
		AIUnit *unit = ai->CommanderUnit();
		if (!unit) continue;
		units.Add(unit);
	}

	if (!grp || grp->IsLocal())
		ProcessJoinGroups(units, grp);
	else
		GetNetworkManager().AskForJoin(grp, units);

	return NOTHING;
}

static bool GetVector( Vector3 &ret, GameValuePar oper )
{
	const GameArrayType &array = oper;
	if (array.Size()!=3) return false;
	if
	(
		array[0].GetType() != GameScalar ||
		array[1].GetType() != GameScalar ||
		array[2].GetType() != GameScalar
	)
	{
		return false;
	}

	float x = array[0];
	float z = array[1];
	float y = array[2];
	ret=Vector3(x,y,z);
	return true;	
}

static bool GetRelPos( const GameState *state, Vector3 &ret, GameValuePar oper )
{
	Object *obj = GetObject(oper);
	if (obj)
	{
		ret = obj->Position();
		// convert to relative position?
		ret[1] -= GLandscape->SurfaceYAboveWater(ret.X(),ret.Z());
		return true;
	}
	const GameArrayType &array = oper;
	if (array.Size() < 2 || array.Size() > 3)
	{
		if (state) state->SetError(EvalDim,array.Size(),3);
		return false;
	}
	if (array[0].GetType() != GameScalar)
	{
		if (state) state->TypeError(GameScalar,array[0].GetType());
		return false;
	}
	if (array[1].GetType() != GameScalar
	)
	{
		if (state) state->TypeError(GameScalar,array[1].GetType());
		return false;
	}

	float x = array[0];
	float z = array[1];
	float y = 0;
	if (array.Size()==3)
	{
		if (array[2].GetType() != GameScalar)
		{
			if (state) state->TypeError(GameScalar,array[2].GetType());
			return false;
		}
		y=array[2];
	}
	ret = Vector3(x,y,z);
	return true;
}

static bool GetPos( const GameState *state, Vector3 &ret, GameValuePar oper )
{
	Vector3 rel;
	bool ok = GetRelPos(state,rel,oper);
	if (!ok) return ok;
	rel[1] += GLandscape->SurfaceYAboveWater(rel.X(),rel.Z());
	ret = rel;
	return true;	
}

bool GetPos(Vector3 &ret, GameValuePar oper)
{
	return GetPos(NULL,ret,oper);
}

GameValue GrpMove( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	AIGroup *grp = GetGroup(oper1);
	if (!grp) return NOTHING;
	Vector3 pos;
	if (!GetPos(state,pos, oper2)) return NOTHING;

	grp->Move
	(
		grp->MainSubgroup(),		// who
		pos,										// where
		Command::Undefined			// how
	);

	return NOTHING;
}

GameValue GrpSetIdentity( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	AIGroup *grp = GetGroup(oper1);
	if (!grp) return NOTHING;

	RString name;
	RString color;
	RString picture;

	const GameArrayType &array = oper2;
	switch (array.Size())
	{
	case 3:
		if (array[2].GetType() != GameString)
		{
			state->TypeError(GameString,array[2].GetType());
			return NOTHING;
		}
		picture = array[2];
	case 2:
		if (array[0].GetType() != GameString)
		{
			state->TypeError(GameString,array[0].GetType());
			return NOTHING;
		}
		if (array[1].GetType() != GameString)
		{
			state->TypeError(GameString,array[1].GetType());
			return NOTHING;
		}
		name = array[0];
		color = array[1];
		break;
	default:
		state->SetError(EvalDim,array.Size(),3);
		return NOTHING;
	}

	grp->SetIdentity(name, color, picture);
	return NOTHING;
}

GameValue GrpKnowsAbout( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	AIGroup *grp = GetGroup(oper1);
	if (!grp) return 0.0f;

	Object *obj = GetObject(oper2);
	EntityAI *veh = dyn_cast<EntityAI>(obj);
	if (!veh) return 0.0f;
	
	Target *target = grp->FindTarget(veh);
	if (!target) return 0.0f;

	return target->FadingSideAccuracy();
}

static AIUnit *GetUnit( const GameState *state, GameValuePar oper1 )
{
	Object *obj = GetObject(oper1);
	EntityAI *veh = dyn_cast<EntityAI>(obj);
	if (!veh) return NULL;
	AIUnit *unit = veh->CommanderUnit();
	//if (!unit) return NULL;
	return unit;
}

static AIGroup *GetUnits( const GameState *state, GameValuePar oper1, PackedBoolArray &list )
{
	if (oper1.GetType()==GameObject)
	{
		// check if oper1 is object or array
		AIUnit *unit = GetUnit(state, oper1);
		if (!unit) return NULL;
		AIGroup *grp = unit->GetGroup();
		if (!grp) return NULL;
		list.Set(unit->ID()-1,true);
		return grp;
	}
	else
	{
		const GameArrayType &array = oper1;
		AIGroup *grp = NULL;
		for (int i=0; i<array.Size(); i++)
		{
			AIUnit *unit = GetUnit(state, array[i]);
			if (!unit) continue;
			AIGroup *g = unit->GetGroup();
			if (!g) continue;
			if (grp && g!=grp) continue; // different group
			grp = g;
			list.Set(unit->ID()-1,true);
		}
		return grp;
	}
}

GameValue VehDone( const GameState *state, GameValuePar oper1 )
{
	PackedBoolArray list;
	AIGroup *grp = GetUnits(state, oper1, list);
	if (!grp) return true;
	// check for all units
	for (int u=0; u<MAX_UNITS_PER_GROUP; u++)
	{
		AIUnit *unit = grp->UnitWithID(u + 1);
		if (!unit) continue;
		if (!list[u]) continue;
		AISubgroup *subgrp = unit->GetSubgroup();
		if (!subgrp) continue;
		if (!unit->IsSubgroupLeader()) continue;
		if (!subgrp->HasCommand()) continue;
		// some busy unit found
		return false;
	}
	return true;
}

GameValue GrpReveal(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	AIGroup *grp = GetGroup(oper1);
	if (!grp) return NOTHING;
	EntityAI *veh = dyn_cast<EntityAI>(GetObject(oper2));
	if (!veh) return NOTHING;
	// full immediate reveal
	grp->AddTarget(veh,1,1,0);
	return NOTHING;
}

GameValue VehMove( const GameState *state, GameValuePar oper1, GameValuePar oper2, bool silent )
{
	PackedBoolArray list;
	AIGroup *grp = GetUnits(state, oper1, list);
	if (!grp) return NOTHING;
	Vector3 pos;
	if (!GetPos(state,pos,oper2))
	{
		//state->SetError(EvalGen);
		return NOTHING;
	}
	// now we have group and unit list
	// create a command and send it to unit (via Send or Issue)
	Command cmd;
	cmd._message = Command::Move;
	cmd._destination = pos;
	cmd._discretion = Command::Undefined;
	cmd._context = Command::CtxMission;
	if (silent)
	{
		cmd._id = grp->GetNextCmdId();
		grp->IssueCommand(cmd,list);
	}
	else
	{
		grp->SendCommand(cmd,list);
	}

	return NOTHING;
}

DEFINE_COMMAND(Move)

GameValue VehFire( const GameState *state, GameValuePar oper1, GameValuePar oper2, bool silent )
{
	PackedBoolArray list;
	AIGroup *grp = GetUnits(state, oper1, list);
	if (!grp) return NOTHING;

	Object *obj = GetObject(oper2);
	EntityAI *veh = dyn_cast<EntityAI>(obj);

	Target *tgt = veh ? FindTargetReveal(grp,veh) : NULL;
	grp->SendState
	(
		new RadioMessageTarget(grp,list,tgt,false,true),
		silent
	);
	return NOTHING;
}

DEFINE_COMMAND(Fire)

GameValue VehStop( const GameState *state, GameValuePar oper1, bool silent )
{
	PackedBoolArray list;
	AIGroup *grp = GetUnits(state, oper1, list);
	if (!grp) return NOTHING;
	// now we have group and unit list
	// create a command and send it to unit (via Send or Issue)
	Command cmd;
	cmd._message = Command::Stop;
	cmd._destination = VZero;
	cmd._discretion = Command::Undefined;
	cmd._context = Command::CtxMission;
	if (silent)
	{
		cmd._id = grp->GetNextCmdId();
		grp->IssueCommand(cmd,list);
	}
	else
	{
		grp->SendCommand(cmd,list);
	}

	return NOTHING;
}

DEFINE_COMMAND_S(Stop)


GameValue VehFollow( const GameState *state, GameValuePar oper1, GameValuePar oper2, bool silent )
{
	PackedBoolArray list;
	AIGroup *grp = GetUnits(state, oper1, list);
	if (!grp) return NOTHING;
	Vector3 pos;
	Object *obj = GetObject(oper2);
	EntityAI *veh = dyn_cast<EntityAI>(obj);
	if (!veh) return NOTHING;
	AIUnit *follow = veh->CommanderUnit();
	if (!follow)
	{
		LogF("Cannot follow empty vehicle");
		return NOTHING;
	}
	if (follow->GetGroup()!=grp)
	{
		LogF("Cannot follow unit from other group");
		return NOTHING;
	}
	// now we have group and unit list
	// create a command and send it to unit (via Send or Issue)
	Command cmd;
	cmd._message = Command::Join;
	cmd._destination = VZero;
	cmd._discretion = Command::Undefined;
	cmd._context = Command::CtxMission;
	cmd._joinToSubgroup = follow->GetSubgroup();
	if (silent)
	{
		cmd._id = grp->GetNextCmdId();
		grp->IssueCommand(cmd,list);
	}
	else
	{
		grp->SendCommand(cmd,list);
	}

	return NOTHING;
}

DEFINE_COMMAND(Follow)

GameValue VehWatch( const GameState *state, GameValuePar oper1, GameValuePar oper2, bool silent )
{
	PackedBoolArray list;
	AIGroup *grp = GetUnits(state, oper1, list);
	if (!grp) return NOTHING;
	Vector3 pos;
	Target *tgt = NULL;
	Object *obj = NULL;
	bool noTgt = false;
	if (oper2.GetType()==GameObject)
	{
		obj  = GetObject(oper2);
		if (!obj) noTgt = true;
		else
		{
			EntityAI *veh = dyn_cast<EntityAI>(obj);
			if (!veh ) pos = obj->Position();
			else
			{
				tgt = FindTargetReveal(grp,veh);
				if (!tgt) pos = obj->Position();
			}
		}
	}
	else if (!GetPos(state,pos,oper2))
	{
		//state->SetError(EvalGen);
		return NOTHING;
	}
	// now we have group and unit list
	// send state information
	if (noTgt)
	{
		//LogF("Watch no");
		grp->SendState(new RadioMessageWatchAuto(grp,list),silent);
	}
	else if (tgt)
	{
		//LogF("Watch tgt %s",(const char *)tgt->idExact->GetDebugName());
		grp->SendState(new RadioMessageWatchTgt(grp,list,tgt),silent);
	}
	else
	{
		//LogF("Watch pos");
		grp->SendState(new RadioMessageWatchPos(grp,list,pos),silent);
	}
	return NOTHING;
}

DEFINE_COMMAND(Watch)

GameValue VehTarget( const GameState *state, GameValuePar oper1, GameValuePar oper2, bool silent )
{
	PackedBoolArray list;
	AIGroup *grp = GetUnits(state, oper1, list);
	if (!grp) return NOTHING;
	Vector3 pos;
	Target *tgt = NULL;
	Object *obj = GetObject(oper2);
	EntityAI *veh = dyn_cast<EntityAI>(obj);
	if (veh)
	{
		tgt = FindTargetReveal(grp,veh);
	}

	// now we have group and unit list
	// send state information
	grp->SendTarget(tgt,false,false,list,silent);
	return NOTHING;
}

DEFINE_COMMAND(Target)

GameValue VehProcessAction( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	EntityAI *veh = dyn_cast<EntityAI>(GetObject(oper1));
	if (!veh) return NOTHING;

	UIAction action;
	action.type = ATNone;
	action.target = veh;
	action.param = 0;
	action.param2 = 0;
	
	action.priority = 0;
	action.hideOnUse = true;
	action.showWindow = false;
	
	const GameArrayType &array = oper2;
	int n = array.Size();
	switch (n)
	{
	case 5:
		action.param3 = array[4];
	case 4:
		action.param2 = array[3];
	case 3:
		action.param = array[2];
	case 2:
		action.target = dyn_cast<EntityAI>(GetObject(array[1]));
	case 1:
		{
			GameStringType type = array[0];
			action.type = GetEnumValue<UIActionType>((const char *)type);
			if (action.type==INT_MIN) action.type = ATNone;
		}
		veh->StartActionProcessing(action,veh->CommanderUnit());
		return NOTHING;
	default:
		state->SetError(EvalDim,array.Size(),5);
		return NOTHING;
	}
}

GameValue VehAddUserAction( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	EntityAI *veh = dyn_cast<EntityAI>(GetObject(oper1));
	if (!veh) return -1.0f;

	const GameArrayType &array = oper2;
	if (array.Size() != 2)
	{
		state->SetError(EvalDim,array.Size(),2);
		return -1.0f;
	}
	if (array[0].GetType() != GameString)
	{
		state->TypeError(GameString, array[0].GetType());
		return -1.0f;
	}
	if (array[1].GetType() != GameString)
	{
		state->TypeError(GameString, array[1].GetType());
		return -1.0f;
	}
	return (float)veh->AddUserAction(array[0], array[1]);
}

/*!
\patch 1.11 Date 08/02/2001 by Jirka
	- Fixed: bug in function "removeAction"
*/
GameValue VehRemoveUserAction( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	EntityAI *veh = dyn_cast<EntityAI>(GetObject(oper1));
	if (!veh) return NOTHING;

	// FIX
	veh->RemoveUserAction(toInt((float)oper2));
	return NOTHING;
}

static EntityEvent GetEvent(const GameValue &oper)
{
	RString name = oper;
	int event = GetEnumValue<EntityEvent>((const char *)name);
	if (event<0 || event>=NEntityEvent) return NEntityEvent;
	return (EntityEvent)event;
}

/*!
\patch 1.82 Date 8/19/2002 by Ondra
- New: Scripting: New functions addEventHandler, removeEventHandler, removeAllEventHandlers.
*/

GameValue VehAddEventHandler(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	const GameArrayType &array = oper2;
	if (array.Size() != 2)
	{
		state->SetError(EvalDim,array.Size(),2);
		return -1.0f;
	}
	if (array[0].GetType() != GameString)
	{
		state->TypeError(GameString, array[0].GetType());
		return -1.0f;
	}
	if (array[1].GetType() != GameString)
	{
		state->TypeError(GameString, array[1].GetType());
		return -1.0f;
	}
	Object *obj = GetObject(oper1);
	EntityAI *ai = dyn_cast<EntityAI>(obj);
	EntityEvent event = GetEvent(array[0]);
	RString handler = array[1];
	if (ai && event<NEntityEvent)
	{
		return (float)ai->AddEventHandler(event,handler);
	}
	return -1.0f;
	
}
GameValue VehRemoveEventHandler(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	const GameArrayType &array = oper2;
	if (array.Size() != 2)
	{
		state->SetError(EvalDim,array.Size(),2);
		return NOTHING;	
	}
	if (array[0].GetType() != GameString)
	{
		state->TypeError(GameString, array[0].GetType());
		return NOTHING;	
	}
	if (array[1].GetType() != GameScalar)
	{
		state->TypeError(GameScalar, array[1].GetType());
		return NOTHING;	
	}
	Object *obj = GetObject(oper1);
	EntityAI *ai = dyn_cast<EntityAI>(obj);
	EntityEvent event = GetEvent(array[0]);
	int handle = toInt((float)array[1]);
	if (ai && event<NEntityEvent)
	{
		ai->RemoveEventHandler(event,handle);
	}
	return NOTHING;	
}

GameValue VehRemoveAllEventHandlers(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	Object *obj = GetObject(oper1);
	EntityAI *ai = dyn_cast<EntityAI>(obj);
	EntityEvent event = GetEvent(oper2);
	if (ai && event<NEntityEvent)
	{
		ai->ClearEventHandlers(event);
	}
	return NOTHING;
}

/*!
	\patch_internal 1.23 Date 9/12/2001 by Ondra
	- Fixed: positioning on objects containing proxies might be incorrect.
*/
GameValue ObjGetPos( const GameState *state, GameValuePar oper1 )
{
	Object *obj1 = GetObject(oper1);
	GameValue value = state->CreateGameValue(GameArray);
	GameArrayType &array = value;
	array.Resize(3);
	if (obj1)
	{
		Vector3Val pos = obj1->WorldPosition();
		array[0] = pos.X();
		array[1] = pos.Z();
		// check position corresponding to (0,0,0)
		Matrix4 trans = obj1->WorldTransform();
		EntityAI *veh = dyn_cast<EntityAI>(obj1);
		if (veh)
		{
			veh->PlaceOnSurface(trans);
			array[2] = pos.Y()-trans.Position().Y();
		}
		else
		{
			float yOffset = 0;
			LODShape *shape = obj1->GetShape();
			Shape *geom = shape->LandContactLevel();
			if (!geom) geom = shape->GeometryLevel();
			if (!geom) geom = shape->Level(0);
			if (geom) yOffset = geom->Min().Y();
			else yOffset = shape->Min().Y();

			array[2] = pos.Y() - GLandscape->SurfaceYAboveWater(pos.X(), pos.Z()) - yOffset;
		}
	}
	else
	{
		array[0] = 0.0f; array[1] = 0.0f; array[2] = 0.0f;
	}
	return value;
}

GameValue ObjGetPosASL(const GameState *state, GameValuePar oper1)
{
	Object *obj = GetObject(oper1);
	GameValue value = state->CreateGameValue(GameArray);
	GameArrayType &array = value;
	array.Resize(3);
	if (obj)
	{
		Vector3Val pos = obj->WorldPosition();
		array[0] = pos.X();
		array[1] = pos.Z();
		array[2] = pos.Y();
	}
	else
	{
		array[0] = 0.0f; array[1] = 0.0f; array[2] = 0.0f;
	}
	return value;
}

GameValue ObjSetPosASL(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	Object *obj = GetObject(oper1);
	if (!obj) return NOTHING;

	Vector3 pos;
	if (!GetVector(pos, oper2))
	{
		//state->SetError(EvalGen);
		return NOTHING;
	}
	if (obj->GetType()==Primary || obj->GetType()==Network)
	{
		return NOTHING;
	}

	// let vehicle adjust position

	Matrix4 trans = obj->Transform();
	trans.SetPosition(pos);

	EntityAI *veh = dyn_cast<EntityAI>(obj);
	if (veh)
	{
		veh->IsMoved();
	}

	Person *soldier = dyn_cast<Person>(veh);
	if (soldier)
	{
		AIUnit *unit = soldier->Brain();
		if (unit)
		{
			Transport *transport = unit->GetVehicleIn();
			if (transport)
			{
				bool isFocused = unit == GWorld->FocusOn();
				Matrix4 transform = soldier->Transform();
				transform.SetPosition(pos);
				transport->GetOutFinished(unit);
				if (soldier == transport->Driver())
				{
					transport->GetOutDriver(transform);
					if (unit->GetGroup())
						unit->GetGroup()->CalculateMaximalStrength();
				}
				else if (soldier == transport->Commander())
					transport->GetOutCommander(transform);
				else if (soldier == transport->Gunner())
					transport->GetOutGunner(transform);
				else
					transport->GetOutCargo(soldier, transform);
				soldier->SetSpeed(VZero);
				if (unit->GetState() == AIUnit::InCargo)
					unit->SetState(AIUnit::Wait);
				if (isFocused)
					GWorld->SwitchCameraTo(soldier, GWorld->GetCameraType());
				return NOTHING;
			}
		}
	}

	obj->MoveNetAware(trans);
	obj->OnPositionChanged();
	GScene->GetShadowCache().ShadowChanged(obj);
	return NOTHING;
}

/*!
\patch 1.61 Date 5/28/2002 by Ondra
- Fixed: SetPos scripting command now changes vehicle orientation
accordingly to terrain.
*/
/*!
\patch 1.90 Date 11/7/2002 by Ondra
- Fixed: MP: Cheating opportunity fixed:
setPos can be no longer used to change position of objects that are part of landscape.
*/

GameValue ObjSetPos( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj1 = GetObject(oper1);
	if (!obj1) return NOTHING;

	Vector3 pos;
	if( !GetRelPos(state,pos,oper2) )
	{
		//state->SetError(EvalGen);
		return NOTHING;
	}
	if (obj1->GetType()==Primary || obj1->GetType()==Network)
	{
		return NOTHING;
	}

	// let vehicle adjust position

	Matrix4 trans = obj1->Transform();

	EntityAI *veh = dyn_cast<EntityAI>(obj1);
	if (veh)
	{
		trans.SetPosition(pos);
		veh->PlaceOnSurface(trans);
		Vector3 surfPos = trans.Position();
		surfPos[1] += pos[1];
		trans.SetPosition(surfPos);
		if (veh->GetType()->HasDriver() && pos[1]>0.1f)
		{
			veh->IsMoved();
		}
	}
	else
	{
		LODShape *shape = obj1->GetShape();
		Shape *geom = shape->LandContactLevel();
		if (!geom) geom = shape->GeometryLevel();
		if (!geom) geom = shape->Level(0);
		if (geom) pos[1] += geom->Min().Y();
		else pos[1] += shape->Min().Y();
		trans.SetPosition(pos);
	}

	Person *soldier = dyn_cast<Person>(veh);
	if (soldier)
	{
		AIUnit *unit = soldier->Brain();
		if (unit)
		{
			Transport *transport = unit->GetVehicleIn();
			if (transport)
			{
				bool isFocused = unit == GWorld->FocusOn();
				Matrix4 transform = soldier->Transform();
				transform.SetPosition(pos);
				transport->GetOutFinished(unit);
				if (soldier == transport->Driver())
				{
					transport->GetOutDriver(transform);
					if (unit->GetGroup())
						unit->GetGroup()->CalculateMaximalStrength();
				}
				else if (soldier == transport->Commander())
					transport->GetOutCommander(transform);
				else if (soldier == transport->Gunner())
					transport->GetOutGunner(transform);
				else
					transport->GetOutCargo(soldier, transform);
				soldier->SetSpeed(VZero);
				if (unit->GetState() == AIUnit::InCargo)
					unit->SetState(AIUnit::Wait);
				if (isFocused)
					GWorld->SwitchCameraTo(soldier, GWorld->GetCameraType());
				return NOTHING;
			}
		}
	}

	obj1->MoveNetAware(trans);
	obj1->OnPositionChanged();
	GScene->GetShadowCache().ShadowChanged(obj1);
	return NOTHING;
}

GameValue ObjGetDir( const GameState *state, GameValuePar oper1 )
{
	Object *obj1 = GetObject(oper1);
	if (obj1)
	{
		Vector3Val dir = obj1->Direction();
		float azimut = atan2(dir.X(), dir.Z()) * (180.0f / H_PI);
		if (azimut < 0) azimut += 360.0f;
		return azimut;
	}
	else
		return 0.0f;
}

GameValue ObjSetDir( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj1 = GetObject(oper1);
	if (!obj1) return NOTHING;

	float azimut = oper2;
	Matrix3 rotY(MRotationY, -HDegree(azimut));
	obj1->SetOrient(rotY);
	return NOTHING;
}

GameValue ObjGetVelocity( const GameState *state, GameValuePar oper1 )
{
	Object *obj = GetObject(oper1);
	Entity *veh = dyn_cast<Entity>(obj);
	GameValue value = state->CreateGameValue(GameArray);
	GameArrayType &array = value;
	array.Resize(3);
	if (veh)
	{
		Vector3Val speed = veh->Speed();
		array[0] = speed.X();
		array[1] = speed.Z();
		array[2] = speed.Y();
	}
	else
	{
		array[0] = 0.0f; array[1] = 0.0f; array[2] = 0.0f;
	}
	return value;
}

GameValue ObjSetVelocity( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj = GetObject(oper1);
	Entity *veh = dyn_cast<Entity>(obj);
	if (!veh) return NOTHING;

	const GameArrayType &array = oper2;
	if (array.Size() != 3)
	{
		state->SetError(EvalDim,array.Size(),3);
		return NOTHING;
	}
	if (array[0].GetType() != GameScalar)
	{
		state->TypeError(GameScalar, array[0].GetType());
		return NOTHING;
	}
	if (array[1].GetType() != GameScalar)
	{
		state->TypeError(GameScalar, array[1].GetType());
		return NOTHING;
	}
	if (array[2].GetType() != GameScalar)
	{
		state->TypeError(GameScalar, array[2].GetType());
		return NOTHING;
	}

	Vector3 speed(array[0], array[2], array[1]);
	veh->SetSpeed(speed);
	return NOTHING;
}

GameValue ObjSetFlyingHeight(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	Object *obj = GetObject(oper1);
	EntityAI *veh = dyn_cast<EntityAI>(obj);
	if (!veh) return NOTHING;
	veh->SetFlyingHeight(oper2);
	return NOTHING;
}

GameValue ObjGetSpeed(const GameState *state, GameValuePar oper1)
{
	Object *obj = GetObject(oper1);
	Vehicle *veh = dyn_cast<Vehicle>(obj);
	if (!veh) return 0.0f;

	return 3.6f * veh->ModelSpeed().Z();
}

GameValue ObjGetDammage(const GameState *state, GameValuePar oper1)
{
	Object *obj = GetObject(oper1);
	if (!obj) return 0.0f;

	return obj->GetTotalDammage();
}

GameValue ObjSetDammage(const GameState *state, GameValuePar oper1,GameValuePar oper2)
{
	Object *obj = GetObject(oper1);
	if (!obj) return NOTHING;

	float dammage = oper2;
	obj->SetDammageNetAware(dammage);	
	return NOTHING;
}


GameValue ObjAllowDammage(const GameState *state, GameValuePar oper1,GameValuePar oper2)
{
	Object *obj = GetObject(oper1);
	if (!obj) return NOTHING;

	bool allow = oper2;
	EntityAI *ent = dyn_cast<EntityAI>(obj);
	if (!ent) return NOTHING;
	ent->SetAllowDammage(allow);
	return NOTHING;
}

GameValue ObjCanStand(const GameState *state, GameValuePar oper1)
{
	Object *obj = GetObject(oper1);
	Man *man = dyn_cast<Man>(obj);
	if (!man) return false;

	return man->IsAbleToStand();
}

GameValue ObjHandsHit(const GameState *state, GameValuePar oper1)
{
	Object *obj = GetObject(oper1);
	Man *man = dyn_cast<Man>(obj);
	if (!man) return 1.0f;

	return man->GetHandsHit();
}

GameValue ObjSetFaceAnimation( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj1 = GetObject(oper1);
	if (!obj1) return NOTHING;

	Man *soldier = dyn_cast<Man>(obj1);
	if (!soldier) return NOTHING;

	int phase = toInt((float)oper2);
	soldier->SetFaceAnimation(phase);
	return NOTHING;
}

const ParamEntry *FindIdentity(RString name)
{
	// find in mission
	const ParamEntry *cls = ExtParsMission.FindEntry("CfgIdentities");
	const ParamEntry *entry = cls ? cls->FindEntry(name) : NULL;
	if (entry) return entry;
	
	// find in campaign
	cls = ExtParsCampaign.FindEntry("CfgIdentities");
	entry = cls ? cls->FindEntry(name) : NULL;
	return entry;
}

GameValue ObjSetIdentity( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj = GetObject(oper1);
	if (!obj) return NOTHING;

	VehicleWithBrain *person = dyn_cast<VehicleWithBrain>(obj);
	if (!person) return NOTHING;

	GameStringType name = oper2;
	const ParamEntry *entry = FindIdentity(name);
	if (entry)
	{
		AIUnitInfo &info = person->GetInfo();
		info._identityContext = RString();
		info._name = (*entry) >> "name";
		info._face = (*entry) >> "face";
		info._glasses = (*entry) >> "glasses";
		info._speaker = (*entry) >> "speaker";
		info._pitch = (*entry) >> "pitch";

		person->SetFace(info._face);
		person->SetGlasses(info._glasses);
		if (person->Brain()) person->Brain()->SetSpeaker(info._speaker, info._pitch);
	}
	
	return NOTHING;
}

GameValue ObjSetFace( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj1 = GetObject(oper1);
	if (!obj1) return NOTHING;

	Man *soldier = dyn_cast<Man>(obj1);
	if (!soldier) return NOTHING;

	GameStringType name = oper2;
	soldier->SetFace(name);
	return NOTHING;
}

GameValue ObjSetMimic( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj1 = GetObject(oper1);
	if (!obj1) return NOTHING;

	Man *soldier = dyn_cast<Man>(obj1);
	if (!soldier) return NOTHING;

	GameStringType name = oper2;
	soldier->SetMimic(name);
	return NOTHING;
}

GameValue ObjGetFlagOwner(const GameState *state, GameValuePar oper1)
{
	Object *obj = GetObject(oper1);
	if (!obj) return OBJECT_NULL;

	EntityAI *veh = dyn_cast<EntityAI>(obj);
	if (!veh) return OBJECT_NULL;

	return GameValueExt(veh->GetFlagOwner());
}

GameValue ObjGetFlag(const GameState *state, GameValuePar oper1)
{
	Object *obj = GetObject(oper1);
	if (!obj) return OBJECT_NULL;

	Person *veh = dyn_cast<Person>(obj);
	if (!veh) return OBJECT_NULL;

	return GameValueExt(veh->GetFlagCarrier());
}

GameValue ObjSetFlagOwner( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj1 = GetObject(oper1);
	if (!obj1) return NOTHING;

	EntityAI *flag = dyn_cast<EntityAI>(obj1);
	if (!flag) return NOTHING;

	Object *obj2 = GetObject(oper2);
	Person *owner = dyn_cast<Person>(obj2);
	flag->SetFlagOwner(owner);

	return NOTHING;
}

GameValue ObjSetFlagTexture( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj1 = GetObject(oper1);
	if (!obj1) return NOTHING;

	EntityAI *veh = dyn_cast<EntityAI>(obj1);
	if (!veh) return NOTHING;

	GameStringType name = oper2;
	veh->SetFlagTexture(name);
	return NOTHING;
}

GameValue ObjSetFlagSide( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj = GetObject(oper1);
	if (!obj) return NOTHING;

	FlagCarrier *flag = dyn_cast<FlagCarrier>(obj);
	if (!flag) return NOTHING;

	TargetSide side = GetSide(oper2);
	flag->SetFlagSide(side);
	return NOTHING;
}

GameValue ObjSetTexture( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	const GameArrayType &array = oper2;
	if (array.Size() != 2)
	{
		state->SetError(EvalDim, array.Size(), 2);
		return NOTHING;
	}
	if (array[0].GetType() != GameScalar)
	{
		state->TypeError(GameScalar, array[0].GetType());
		return NOTHING;
	}
	if (array[1].GetType() != GameString)
	{
		state->TypeError(GameString, array[1].GetType());
		return NOTHING;
	}

	Object *obj = GetObject(oper1);
	if (!obj) return NOTHING;

	EntityAI *veh = dyn_cast<EntityAI>(obj);
	if (!veh) return NOTHING;

	int index = toInt((float)array[0]);
	RString name = array[1];

	RString FindPicture (RString name);
	Ref<Texture> texture = GlobLoadTexture(FindPicture(name));
	veh->SetObjectTexture(index, texture);
	return NOTHING;
}

GameValue ObjGetNearestBuilding( const GameState *state, GameValuePar oper1 )
{
	Object *obj = GetObject(oper1);
	if (!obj) return ObjNull(state);

	Vector3 pos = obj->Position();
	Vehicle *house = NULL;
	float minDist2 = FLT_MAX;
	for (int i=0; i<GWorld->NBuildings(); i++)
	{
		Vehicle *veh = GWorld->GetBuilding(i);
		if (!veh) continue;
		if (!veh->GetShape()) continue;
		if (veh->GetShape()->FindPaths() < 0) continue;
		if (!veh->GetIPaths()) continue;
		float dist2 = veh->Position().Distance2(pos);
		if (dist2 < minDist2)
		{
			minDist2 = dist2;
			house = veh;
		}
	}
	return GameValueExt(house);
}

GameValue ObjGetBuildingPos( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	GameValue value = state->CreateGameValue(GameArray);
	GameArrayType &array = value;
	array.Resize(3);
	array[0] = 0.0f; array[1] = 0.0f; array[2] = 0.0f;

	Object *obj = GetObject(oper1);
	if (obj)
	{
		const IPaths *house = obj->GetIPaths();
		if (house)
		{
			int housePos = toInt((float)oper2);
			if (housePos >= 0 && housePos < house->NPos())
			{
				Vector3Val pos = house->GetPosition(house->GetPos(housePos));
				array[0] = pos.X();
				array[1] = pos.Z();
				array[2] =
					pos.Y() -	GLandscape->SurfaceYAboveWater(pos.X(), pos.Z());
			}
		}
	}
	return value;
}

/*!
\patch 1.76 Date 6/21/2002 by Ondra
- Fixed: Scripting command nearestObject (pos,typeName) does not work.
*/

GameValue GetNearestObject(const GameState *state, GameValuePar oper1)
{
	Vector3 pos;
	RString typeName;

	if (!GetPos(state,pos, oper1))
	{
		const GameArrayType &array = oper1;
		if (array.Size() != 2) return OBJECT_NULL;
		if (!GetPos(state,pos, array[0])) return OBJECT_NULL;
		if (array[1].GetType() != GameString) return OBJECT_NULL;
		typeName = array[1];
		state->SetError(EvalOK);
	}

	int xMin, xMax, zMin, zMax;
	const float limit = 50;
	ObjRadiusRectangle(xMin, xMax, zMin, zMax, pos, pos, limit);
	Object *object = NULL;
	float minDist2 = Square(limit);
	for (int x=xMin; x<=xMax; x++) for (int z=zMin; z<=zMax; z++)
	{
		const ObjectList &list = GLandscape->GetObjects(z, x);
		int n = list.Size();
		for (int i=0; i<n; i++)
		{
			Object *obj = list[i];

			float dist2 = obj->Position().Distance2(pos);
			if (dist2 >= minDist2) continue;
			if (typeName.GetLength() > 0)
			{
				// TODO: virtual const char *Object::GetName()
				Vehicle *veh = dyn_cast<Vehicle>(obj);
				if (!veh) continue;
				if (stricmp(veh->GetName(),typeName) != 0) continue;
			}

			object = obj;
			minDist2 = dist2;
		}
	}

	return GameValueExt(object);
}

GameValue ObjSwitchLight(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	Object *obj = GetObject(oper1);

	StreetLamp *lamp = dyn_cast<StreetLamp>(obj);
	if (lamp)
	{
		RString name = oper2;
		StreetLamp::LightState state = GetEnumValue<StreetLamp::LightState>((const char *)name);
		if (state==INT_MIN) state = StreetLamp::LSAuto;
		lamp->SwitchLight(state);
	}

	return NOTHING;
}

GameValue ObjInflame(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	Object *obj = GetObject(oper1);

	Fireplace *fire = dyn_cast<Fireplace>(obj);
	if (fire)
	{
		if (fire->IsLocal())
			fire->Inflame(oper2);
		else
			GetNetworkManager().AskForInflameFire(fire, oper2);
	}

	return NOTHING;
}

/*!
\patch 1.04 Date 07/16/2001 by Jirka
- Added: procedure "inflamed"
*/
GameValue ObjInflamed( const GameState *state, GameValuePar oper1 )
{
	Object *obj = GetObject(oper1);

	Fireplace *fire = dyn_cast<Fireplace>(obj);
	if (fire) return fire->Burning();

	return false;
}

/*!
\patch 1.04 Date 07/16/2001 by Jirka
- Added: procedure "lightSwitched"
*/
GameValue ObjLightSwitched( const GameState *state, GameValuePar oper1 )
{
	Object *obj = GetObject(oper1);

	StreetLamp *lamp = dyn_cast<StreetLamp>(obj);
	if (lamp)
	{
		StreetLamp::LightState state = lamp->GetLightState();
		return FindEnumName(state);
	}
	return "ERROR";
}

GameValue ObjGetScudState( const GameState *state, GameValuePar oper1 )
{
	Object *obj = GetObject(oper1);

	Car *scud = dyn_cast<Car>(obj);
	if (scud) return scud->GetScudState();
	return 0.0f;
}

GameValue ObjList( const GameState *state, GameValuePar oper1 )
{
	Object *obj1=GetObject(oper1);

	Detector *detect = dyn_cast<Detector>(obj1);
	if( !detect ) return GameArrayType();

	return detect->GetGameValue();
}

GameValue ObjLeader( const GameState *state, GameValuePar oper1 )
{
	Object *obj1=GetObject(oper1);
	EntityAI *ai=dyn_cast<EntityAI>(obj1);
	if( !ai ) return OBJECT_NULL;
	AIUnit *unit=ai->CommanderUnit();
	if( !unit ) return OBJECT_NULL;
	AISubgroup *subgrp = unit->GetSubgroup();
	if( !subgrp ) return OBJECT_NULL;
	if( !subgrp->Leader() ) return OBJECT_NULL;
	EntityAI *leader=subgrp->Leader()->GetPerson();
	return GameValueExt(leader);
}

GameValue ObjGroupLeader( const GameState *state, GameValuePar oper1 )
{
	Object *obj1 = GetObject(oper1);
	EntityAI *ai = dyn_cast<EntityAI>(obj1);
	if( !ai ) return OBJECT_NULL;
	AIUnit *unit = ai->CommanderUnit();
	if( !unit ) return OBJECT_NULL;
	AIGroup *grp = unit->GetGroup();
	if( !grp ) return OBJECT_NULL;
	if( !grp->Leader() ) return OBJECT_NULL;
	EntityAI *leader = grp->Leader()->GetPerson();
	return GameValueExt(leader);
}

GameValue ObjGroup( const GameState *state, GameValuePar oper1 )
{
	Object *obj1 = GetObject(oper1);
	EntityAI *ai = dyn_cast<EntityAI>(obj1);
	if( !ai ) return GROUP_NULL;
	AIUnit *unit = ai->CommanderUnit();
	if( !unit ) return GROUP_NULL;
	AIGroup *grp = unit->GetGroup();
	return GameValueExt(grp);
}

GameValue GrpLeader( const GameState *state, GameValuePar oper1 )
{
	AIGroup *grp1 = GetGroup(oper1);
	if (!grp1) return OBJECT_NULL;
	AIUnit *leader = grp1->Leader();
	if (!leader) return OBJECT_NULL;
	EntityAI *leaderVeh = leader->GetPerson();
	return GameValueExt(leaderVeh);
}

GameValue GrpUnits( const GameState *state, GameValuePar oper1 )
{
	AIGroup *grp1 = GetGroup(oper1);
	// create list of units

	GameValue value = state->CreateGameValue(GameArray);
	GameArrayType &array = value;
	if (grp1)
	{
		array.Realloc(grp1->NUnits());
		for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
		{
			AIUnit *unit = grp1->UnitWithID(i+1);
			if (!unit) continue;
			EntityAI *veh = unit->GetPerson();
			array.Add(GameValueExt(veh));
		}
	}
	return value;
}

GameValue ObjVehicle( const GameState *state, GameValuePar oper1 )
{
	Object *obj1=GetObject(oper1);
	EntityAI *ai=dyn_cast<EntityAI>(obj1);
	if( !ai ) return oper1;
	AIUnit *unit=ai->CommanderUnit();
	if( !unit ) return oper1;
	EntityAI *vehicle=unit->GetVehicle();
	return GameValueExt(vehicle);
}

GameValue ObjDriver( const GameState *state, GameValuePar oper1 )
{
	Object *obj1=GetObject(oper1);
	Transport *trans=dyn_cast<Transport>(obj1);
	if( !trans ) return oper1;
	EntityAI *vehicle=trans->Driver();
	return GameValueExt(vehicle);
}

GameValue ObjCommander( const GameState *state, GameValuePar oper1 )
{
	Object *obj1=GetObject(oper1);
	Transport *trans=dyn_cast<Transport>(obj1);
	if( !trans ) return oper1;
	EntityAI *vehicle=trans->Commander();
	return GameValueExt(vehicle);
}

GameValue ObjGunner( const GameState *state, GameValuePar oper1 )
{
	Object *obj1=GetObject(oper1);
	Transport *trans=dyn_cast<Transport>(obj1);
	if( !trans ) return oper1;
	EntityAI *vehicle=trans->Gunner();
	return GameValueExt(vehicle);
}

GameValue ObjSay( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj1=GetObject(oper1);
	GameStringType voice;
	float distance = 100.0f;
	float speed = 1.0f;
	if (oper2.GetType() == GameString)
		voice = oper2;
	else
	{
		Assert(oper2.GetType() == GameArray);
		const GameArrayType &array = oper2;

		switch (array.Size())
		{
		case 3:
			if (array[2].GetType() != GameScalar)
			{
				state->TypeError(GameScalar, array[2].GetType());
				return NOTHING;
			}
			speed = array[2];

		case 2:
			if (array[1].GetType() != GameScalar)
			{
				state->TypeError(GameScalar, array[1].GetType());
				return NOTHING;
			}
			distance = array[1];

			if (array[0].GetType() != GameString)
			{
				state->TypeError(GameString, array[0].GetType());
				return NOTHING;
			}
			voice = array[0];
			break;
		default:
			state->SetError(EvalDim, array.Size(), 2);
			return NOTHING;
		}

	}

	if( !obj1 ) return NOTHING;
	if (obj1->IsDammageDestroyed()) return NOTHING;

	Vehicle *veh = new SoundOnVehicle(voice, obj1, distance, speed);
	veh->SetPosition(obj1->Position());
	GWorld->AddBuilding(veh);
	return NOTHING;
}

GameValue PlaySound( const GameState *state, GameValuePar oper1)
{
	GameStringType voice = oper1;
	Vehicle *veh = new SoundOnVehicle(voice, NULL);
	GWorld->AddBuilding(veh);
	return NOTHING;
}

const ParamEntry *FindMusic(RString name, SoundPars &pars);

GameValue PlayMusic(const GameState *state, GameValuePar oper1)
{
	if (oper1.GetType() == GameArray)
	{
		// track and position is given

		const GameArrayType &array = oper1;
		if(array.Size() != 2)
		{
			state->SetError(EvalDim,array.Size(),2);
			return NOTHING;
		}
		if( array[0].GetType() != GameString)
		{
			state->TypeError(GameString,array[0].GetType());
			return NOTHING;
		}
		if(array[1].GetType() != GameScalar)
		{
			state->TypeError(GameScalar,array[1].GetType());
			return NOTHING;
		}
		GameStringType voice = array[0];
		GameScalarType pos = array[1];


		if (voice.GetLength()<=0)
		{
			GSoundScene->StopMusicTrack();
			return NOTHING;
		}

		SoundPars sound;
		if (FindMusic(voice, sound))
		{
			// start sound as musical track
			GSoundScene->StartMusicTrack(sound,pos);
		}
		return NOTHING;
	}

	GameStringType voice = oper1;

	if (voice.GetLength()<=0)
	{
		GSoundScene->StopMusicTrack();
		return NOTHING;
	}

	SoundPars sound;
	if (FindMusic(voice, sound))
	{
		// start sound as musical track
		GSoundScene->StartMusicTrack(sound);
	}
	return NOTHING;
}
GameValue SetMusicVolume(const GameState *state, GameValuePar oper1,GameValuePar oper2)
{
	GSoundScene->SetMusicVolume(oper2,oper1);
	return NOTHING;
}
GameValue GetMusicVolume(const GameState *state)
{
	return GSoundScene->GetMusicVolume();
}
GameValue SetSoundVolume(const GameState *state, GameValuePar oper1,GameValuePar oper2)
{
	GSoundScene->SetSoundVolume(oper2,oper1);
	return NOTHING;
}
GameValue GetSoundVolume(const GameState *state)
{
	return GSoundScene->GetSoundVolume();
}

static bool SendRadio(RadioChannel &channel, GameValuePar oper1, GameValuePar oper2)
{
	GameStringType wave = oper2;
	Object *obj = GetObject(oper1);
	EntityAI *veh = dyn_cast<EntityAI>(obj);
	AIUnit *unit = veh ? veh->CommanderUnit() : NULL;
	channel.Transmit
	(
		new RadioMessageText(wave, unit), 0
	);
	return true;
}

/*
\patch 1.50 Date 4/15/2002 by Jirka
- Fixed: HQ names in radio messages
*/
static bool SendRadio(RadioChannel &channel, RString identity, GameValuePar oper2)
{
	GameStringType wave = oper2;
	if (!(Pars >> "CfgHQIdentities").FindEntry(identity)) return false;
	// send class name, translate in RadioChannel::Say
	channel.Transmit
	(
		new RadioMessageText(wave, identity), 0
	);
	return true;
}

GameValue ObjGlobalRadio( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	if (!SendRadio(GWorld->GetRadio(), oper1, oper2)) state->SetError(EvalGen);
	return NOTHING;
}

GameValue ObjSideRadio( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	if (oper1.GetType() == GameArray)
	{
		const GameArrayType &array = oper1;
		if(array.Size() != 2)
		{
			state->SetError(EvalDim,array.Size(),2);
			return NOTHING;
		}
		if (array[0].GetType() != GameSide)
		{
			state->TypeError(GameSide,array[0].GetType());
			return NOTHING;
		}
		if (array[1].GetType() != GameString)
		{
			state->TypeError(GameString,array[1].GetType());
			return NOTHING;
		}
		AICenter *center = GWorld->GetCenter(GetSide(array[0]));
		if (!center) return NOTHING;
		GameStringType identity = array[1];
		if (!SendRadio(center->GetRadio(), identity, oper2)) state->SetError(EvalGen);
	}
	else
	{
		AIGroup *grp = GetGroup(oper1);
		if (!grp) return NOTHING;
		AICenter *center = grp->GetCenter();
		if (!center) return NOTHING;
		if (!SendRadio(center->GetRadio(), oper1, oper2)) state->SetError(EvalGen);
	}
	return NOTHING;
}

GameValue ObjGroupRadio( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	AIGroup *grp = GetGroup(oper1);
	if (!grp) return NOTHING;
	if (!SendRadio(grp->GetRadio(), oper1, oper2)) state->SetError(EvalGen);
	return NOTHING;
}

GameValue ObjVehicleRadio( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	AIUnit *unit = GetUnit(state, oper1);
	if (!unit) return NOTHING;
	Transport *veh = unit->GetVehicleIn();
	if (!veh) return NOTHING;
	if (!SendRadio(veh->GetRadio(), oper1, oper2)) state->SetError(EvalGen);
	return NOTHING;
}

void WhatUnits(RefArray<NetworkObject> &units, ChatChannel channel, NetworkObject *object);
template Ref<NetworkObject>;

static bool SendChat(ChatChannel channel, NetworkObject *object, AIUnit *sender, RString message, bool audible)
{
	if (audible) GChatList.Add(channel, sender, message, false, true);
// Do not send over network - used as title effects etc.
// - activated on each client
/*
	RefArray<NetworkObject> units;
	WhatUnits(units, channel, object);
	GetNetworkManager().Chat(channel, sender, units, message);
*/
	return true;
}

/*
\patch 1.50 Date 4/15/2002 by Jirka
- Fixed: HQ names in chat messages
*/
static bool SendChat(ChatChannel channel, NetworkObject *object, RString identity, RString message, bool audible)
{
	const ParamEntry *cfg = (Pars >> "CfgHQIdentities").FindEntry(identity);
	if (!cfg) return false;
	
	// used only in GChatList.Add - write directly display name
	RString sender = *cfg >> "name";

	if (audible) GChatList.Add(channel, sender, message, false, true);
// Do not send over network - used as title effects etc.
// - activated on each client
/*
	RefArray<NetworkObject> units;
	WhatUnits(units, channel, object);
	GetNetworkManager().Chat(channel, sender, units, message);
*/
	return true;
}

GameValue ObjGlobalChat( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	EntityAI *veh = dyn_cast<EntityAI>(GetObject(oper1));
	if (!veh) return NOTHING;
	AIUnit *unit = veh->CommanderUnit();
	if (!unit) return NOTHING;
	bool audible = GWorld->GetRadio().IsAudible();
	if (!SendChat(CCGlobal, NULL, unit, oper2, audible)) state->SetError(EvalGen);
	return NOTHING;
}

GameValue ObjSideChat( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	if (oper1.GetType() == GameArray)
	{
		const GameArrayType &array = oper1;
		if(array.Size() != 2)
		{
			state->SetError(EvalDim,array.Size(),2);
			return NOTHING;
		}
		if (array[0].GetType() != GameSide)
		{
			state->TypeError(GameSide,array[0].GetType());
			return NOTHING;
		}
		if (array[1].GetType() != GameString)
		{
			state->TypeError(GameString,array[1].GetType());
			return NOTHING;
		}
		AICenter *center = GWorld->GetCenter(GetSide(array[0]));
		if (!center) return NOTHING;
		RString identity = array[1];
		bool audible = center->GetRadio().IsAudible();
		if (!SendChat(CCSide, NULL, identity, oper2, audible)) state->SetError(EvalGen);
	}
	else
	{
		EntityAI *veh = dyn_cast<EntityAI>(GetObject(oper1));
		if (!veh) return NOTHING;
		AIUnit *unit = veh->CommanderUnit();
		if (!unit) return NOTHING;
		AIGroup *grp = unit->GetGroup();
		if (!grp) return NOTHING;
		AICenter *center = grp->GetCenter();
		if (!center) return NOTHING;
		bool audible = center->GetRadio().IsAudible();
		if (!SendChat(CCSide, center, unit, oper2, audible)) state->SetError(EvalGen);
	}
	return NOTHING;
}

GameValue ObjGroupChat( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	EntityAI *veh = dyn_cast<EntityAI>(GetObject(oper1));
	if (!veh) return NOTHING;
	AIUnit *unit = veh->CommanderUnit();
	if (!unit) return NOTHING;
	AIGroup *grp = unit->GetGroup();
	if (!grp) return NOTHING;

	bool audible = grp->GetRadio().IsAudible();
	if (!SendChat(CCGroup, grp, unit, oper2, audible)) state->SetError(EvalGen);
	return NOTHING;
}

GameValue ObjVehicleChat( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Transport *veh = dyn_cast<Transport>(GetObject(oper1));
	if (!veh) return NOTHING;
	AIUnit *unit = veh->CommanderUnit();
	if (!unit) return NOTHING;
	bool audible = veh->GetRadio().IsAudible();
	if (!SendChat(CCVehicle, veh, unit, oper2, audible)) state->SetError(EvalGen);
	return NOTHING;
}

GameValue ObjPlayMove( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	EntityAI *obj=dyn_cast<EntityAI>(GetObject(oper1));
	GameStringType move = oper2;

	if( !obj ) return NOTHING;
	obj->PlayMove(move);
	return NOTHING;
}

GameValue ObjSwitchMove( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	EntityAI *obj=dyn_cast<EntityAI>(GetObject(oper1));
	GameStringType move = oper2;

	if( !obj ) return NOTHING;
	obj->SwitchMove(move);
	return NOTHING;
}


GameValue ObjCameraEffect( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj1=GetObject(oper1);
	if (!obj1) return NOTHING;

	const GameArrayType &array = oper2;
	if(array.Size() != 2)
	{
		state->SetError(EvalDim,array.Size(),2);
		return NOTHING;
	}
	if (array[0].GetType() != GameString)
	{
		state->TypeError(GameString,array[0].GetType());
		return NOTHING;
	}
	if (array[1].GetType() != GameString)
	{
		state->TypeError(GameString,array[1].GetType());
		return NOTHING;
	}

	GameStringType effect = array[0];
	GameStringType str = array[1];
	CamEffectPosition pos = CamEffectTop;
	const EnumName *names = GetEnumNames(pos);
	for (int i=0; names[i].IsValid(); i++)
	{
		if (stricmp(names[i].name, str) == 0)
		{
			pos = (CamEffectPosition)names[i].value;
			// if we are starting camera effect on camera holder
			// it is script controlled and should be infinite
			// also intro camera effects are infinite
			bool infinite =
			(
				dyn_cast<CameraHolder>(obj1)!=NULL ||
				GWorld->GetMode()==GModeIntro
			);
			GWorld->SetCameraEffect
			(
				CreateCameraEffect(obj1, effect, pos, infinite)
			);
			return NOTHING;
		}
	}

	return NOTHING;
}

GameValue CamCreate( const GameState *state, GameValuePar oper1,  GameValuePar oper2 )
{
	GameStringType effect = oper1;
	Vector3 pos;
	if( !GetPos(state,pos,oper2) )
	{
		//state->SetError(EvalGen);
		return OBJECT_NULL;
	}

	RString type = effect;
	// only some non-ai vehicle may be created this way
	if
	(
		!strcmpi(type,"camera") && !strcmpi(type,"seagull")
	)
	{
		return OBJECT_NULL;
	}
	Vehicle *veh = NewNonAIVehicle(type,NULL);
	if( !veh ) return OBJECT_NULL;

	veh->SetPosition(pos);

	GWorld->AddAnimal(veh); // insert vehicle to both landscape and world

	return GameValueExt(veh);
}

GameValue CamDestroy( const GameState *state, GameValuePar oper1 )
{
	CameraHolder *cam = dyn_cast<CameraHolder>(GetObject(oper1));
	if( !cam ) return NOTHING;
	// we have camera holder
	// we can safely destroy it
	cam->SetDelete();
	return NOTHING;
}

GameValue CamCommited( const GameState *state, GameValuePar oper1 )
{
	CameraHolder *cam = dyn_cast<CameraHolder>(GetObject(oper1));
	if( !cam ) return NOTHING;
	return cam->GetCommited();
}

GameValue CamSetPos( const GameState *state, GameValuePar oper1,  GameValuePar oper2 )
{
	CameraHolder *cam = dyn_cast<CameraHolder>(GetObject(oper1));
	if( !cam ) return NOTHING;

	Vector3 pos;
	if( !GetPos(state,pos,oper2) )
	{
		//state->SetError(EvalGen);
		return NOTHING;
	}
	cam->SetPos(pos);

	return NOTHING;
}

GameValue CamSetRelPos( const GameState *state, GameValuePar oper1,  GameValuePar oper2 )
{
	CameraHolder *cam = dyn_cast<CameraHolder>(GetObject(oper1));
	if( !cam ) return NOTHING;

	Vector3 pos;
	if( !GetVector(pos,oper2) )
	{
		state->SetError(EvalGen);
		return NOTHING;
	}
	cam->SetPos(cam->GetTarget().PositionRelToAbs(pos));

	return NOTHING;
}

GameValue CamSetFOV( const GameState *state, GameValuePar oper1,  GameValuePar oper2 )
{
	CameraHolder *cam = dyn_cast<CameraHolder>(GetObject(oper1));
	if( !cam ) return NOTHING;
	cam->SetFOV(oper2);
	return NOTHING;
}

GameValue CamSetFOVRange( const GameState *state, GameValuePar oper1,  GameValuePar oper2 )
{
	/*
	CameraHolder *cam = dyn_cast<CameraHolder>(GetObject(oper1));
	if( !cam ) return NOTHING;
	cam->SetFOV(oper2);
	*/
	return NOTHING;
}

GameValue CamSetDive( const GameState *state, GameValuePar oper1,  GameValuePar oper2 )
{
	CameraHolder *cam = dyn_cast<CameraHolder>(GetObject(oper1));
	if( !cam ) return NOTHING;
	cam->SetDive(oper2);
	return NOTHING;
}

GameValue CamSetBank( const GameState *state, GameValuePar oper1,  GameValuePar oper2 )
{
	CameraHolder *cam = dyn_cast<CameraHolder>(GetObject(oper1));
	if( !cam ) return NOTHING;
	cam->SetBank(oper2);
	return NOTHING;
}

GameValue CamSetDir( const GameState *state, GameValuePar oper1,  GameValuePar oper2 )
{
	CameraHolder *cam = dyn_cast<CameraHolder>(GetObject(oper1));
	if( !cam ) return NOTHING;
	cam->SetHeading(oper2);
	return NOTHING;
}

GameValue CamSetTargetObj( const GameState *state, GameValuePar oper1,  GameValuePar oper2 )
{
	CameraHolder *cam = dyn_cast<CameraHolder>(GetObject(oper1));
	if( !cam ) return NOTHING;
	cam->SetTarget(GetObject(oper2));
	return NOTHING;
}

GameValue CamSetTargetVec( const GameState *state, GameValuePar oper1,  GameValuePar oper2 )
{
	CameraHolder *cam = dyn_cast<CameraHolder>(GetObject(oper1));
	if( !cam ) return NOTHING;
	Vector3 pos;
	if( !GetPos(state,pos,oper2) )
	{
		//state->SetError(EvalGen);
		return NOTHING;
	}
	cam->SetTarget(pos);
	return NOTHING;
}

GameValue CamCommand( const GameState *state, GameValuePar oper1,  GameValuePar oper2 )
{
	Object *obj1=GetObject(oper1);
	CameraHolder *cam = dyn_cast<CameraHolder>(obj1);
	if( !cam ) return NOTHING;
	cam->Command(oper2);
	return NOTHING;
}

GameValue CamCommit( const GameState *state, GameValuePar oper1,  GameValuePar oper2 )
{
	Object *obj1=GetObject(oper1);
	CameraHolder *cam = dyn_cast<CameraHolder>(obj1);
	if( !cam ) return NOTHING;

	float time = oper2;
	cam->Commit(time);
	return NOTHING;
}

// TODO: other placement
static const EnumName CameraTypeNames[]=
{
	EnumName(CamInternal, "INTERNAL"),
	EnumName(CamGunner, "GUNNER"),
	EnumName(CamExternal, "EXTERNAL"),
	EnumName(CamGroup, "GROUP"),
	EnumName(CamInternal, "CARGO"), // TODO: remove - obsolete
	EnumName()
};
template<>
const EnumName *GetEnumNames(CameraType)
{
	return CameraTypeNames;
}

GameValue ObjSwitchCamera( const GameState *state, GameValuePar oper1,  GameValuePar oper2 )
{
	Object *obj = GetObject(oper1);
	if (!obj) return NOTHING;

	GameStringType str = oper2;
	CameraType type = CamInternal;
	const EnumName *names = GetEnumNames(type);
	for (int i=0; names[i].IsValid(); i++)
	{
		if (stricmp(names[i].name, str) == 0)
		{
			type = (CameraType)names[i].value;
			GWorld->SwitchCameraTo(obj, type);
			return NOTHING;
		}
	}

	return NOTHING;
}

bool ParseEffect
(
	const GameState *state,
	GameStringType &effect, GameStringType &str, float &speed,
	GameValuePar oper1
)
{
	const GameArrayType &array = oper1;
	if(array.Size()<2 || array.Size()>3)
	{
		state->SetError(EvalDim,array.Size(),2);
		return false;
	}
	if (array[0].GetType() != GameString)
	{
		state->TypeError(GameString,array[0].GetType());
		return false;
	}
	if (array[1].GetType() != GameString)
	{
		state->TypeError(GameString,array[1].GetType());
		return false;
	}

	effect = array[0];
	str = array[1];
	speed = 1;
	if (array.Size()==3)
	{
		if (array[2].GetType() != GameScalar)
		{
			state->TypeError(GameScalar,array[2].GetType());
			return false;
		}
		speed = array[2];
	}
	return true;
}

static TitEffectName TitleType( const GameStringType &str )
{
	TitEffectName name = NTitEffects;
	const EnumName *names = GetEnumNames(name);
	for (int i=0; names[i].IsValid(); i++)
	{
		if (stricmp(names[i].name, str) == 0)
		{
			name = (TitEffectName)names[i].value;
			break;
		}
	}
	return name;
}

#define TITLE_PREFIX \
	float speed; \
	GameStringType effect,str; \
	if (!ParseEffect(state,effect,str,speed,oper1)) \
	{ \
		return NOTHING; \
	} \
	TitEffectName name = TitleType(str); \
	if (name==NTitEffects) return NOTHING;

GameValue GameTitleText( const GameState *state, GameValuePar oper1 )
{
	TITLE_PREFIX

	GWorld->SetTitleEffect(CreateTitleEffect(name, effect, speed));
	return NOTHING;
}

const ParamEntry *FindRscTitle(RString name);

GameValue GameTitleRsc( const GameState *state, GameValuePar oper1 )
{
	TITLE_PREFIX

	const ParamEntry *cls = FindRscTitle(effect);
	if (cls) GWorld->SetTitleEffect
	(
		CreateTitleEffectRsc(name, *cls)
	);
	return NOTHING;
}

GameValue GameTitleObj( const GameState *state, GameValuePar oper1 )
{
	TITLE_PREFIX

	GWorld->SetTitleEffect
	(
		CreateTitleEffectObj(name, Pars >> "CfgTitles" >> effect)
	);
	return NOTHING;
}

GameValue CutText( const GameState *state, GameValuePar oper1 )
{
	TITLE_PREFIX

	GWorld->SetCutEffect(CreateTitleEffect(name, effect, speed));
	return NOTHING;
}

GameValue CutRsc( const GameState *state, GameValuePar oper1 )
{
	TITLE_PREFIX

	const ParamEntry *cls = FindRscTitle(effect);
	if (cls) GWorld->SetCutEffect
	(
		CreateTitleEffectRsc(name, *cls)
	);
	return NOTHING;
}

GameValue CutObj( const GameState *state, GameValuePar oper1 )
{
	TITLE_PREFIX

	GWorld->SetCutEffect
	(
		CreateTitleEffectObj(name, Pars >> "CfgTitles" >> effect)
	);
	return NOTHING;
}


GameValue SkipDayTime( const GameState *state, GameValuePar oper1 )
{
	float time=oper1;

	// change time
	// TODO: implement smooth change in world

	if (Glob.clock.AdvanceTime(time * OneHour))
	{
		GScene->MainLight()->Recalculate();
		GScene->MainLightChanged();
		GLandscape->OnTimeSkipped();
	}

	return NOTHING;
}

#include "camera.hpp"

void SetVisibility(float distance)
{
	saturate(distance, 500, 5000);
	LogF("Visibility set to %f",distance);
	Glob.config.tacticalZ = distance;
	Glob.config.horizontZ = distance;
	Glob.config.objectsZ = distance*(600.0/900);
	Glob.config.shadowsZ = distance*(250.0/900);
	saturateMin(Glob.config.objectsZ,3000);
	saturateMin(Glob.config.shadowsZ,500);
	GLandscape->Simulate(0);
	GScene->ResetFog();
}

/*!
\patch 1.58 Date 7/15/2002 by Ondra
- New: New scripting function setTerrainGrid.
*/

GameValue SetTerrainGrid( const GameState *state, GameValuePar oper1 )
{
	float preferredGrid = oper1;
	// TODO: Add World function taking grid argument
	GWorld->AdjustSubdivisionGrid(preferredGrid);
	GLandscape->FillCache(*GScene->GetCamera());
	return NOTHING;
}

GameValue SetViewDistance( const GameState *state, GameValuePar oper1 )
{
	float distance = oper1;
	SetVisibility(distance);
	GLandscape->FillCache(*GScene->GetCamera());
	return NOTHING;
}

GameValue SetOvercast(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	GWorld->SetWeather(oper2, -1, oper1);
	return NOTHING;
}


GameValue SetRain(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	GLandscape->SetRain(oper2, oper1);
	return NOTHING;
}

GameValue SetFog(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	GWorld->SetWeather(-1, oper2, oper1);
	return NOTHING;
}

GameValue SetObjectiveStatus(const GameState *state, GameValuePar oper1,  GameValuePar oper2)
{
	GameStringType name = GameStringType("OBJ_") + oper1;
	GameStringType str = oper2;
	ObjectiveStatus status = OSActive;	// avoid warning
	const EnumName *names = GetEnumNames(status);
	for (int i=0; names[i].IsValid(); i++)
	{
		if (stricmp(names[i].name, str) == 0)
		{
			status = (ObjectiveStatus)names[i].value;
			break;
		}
	}
	int value = status;
	int oldValue = toInt((float)state->VarGet(name));
	if (oldValue == value) return NOTHING;
	RString message = status == OSDone ? LocalizeString(IDS_OBJECTIVE_DONE) : LocalizeString(IDS_OBJECTIVE_UPDATED);
	if (Glob.config.easyMode && status != OSHidden) GWorld->UI()->ShowHint(message);
	const_cast<GameState *>(state)->VarSet(name, GameValue((float)value), true);
	DisplayMap *map = dynamic_cast<DisplayMap *>(GWorld->Map());
	if (map)
	{
		GameVarSpace local;
		state->BeginContext(&local);
		map->UpdatePlan();
		state->EndContext();
	}
	return NOTHING;
}

GameValue MapForce( const GameState *state, GameValuePar oper1 )
{
	GWorld->ForceMap(oper1);
	return NOTHING;
}

GameValue MapAnimClear(const GameState *state)
{
	DisplayMap *disp = dynamic_cast<DisplayMap *>(GWorld->Map());
	if (!disp) return NOTHING;
	CStaticMap *map = disp->GetMap();
	if (!map) return NOTHING;

	map->ClearAnimation();
	return NOTHING;
}

GameValue MapAnimAdd( const GameState *state, GameValuePar oper1 )
{
	const GameArrayType &array = oper1;
	if (array.Size() != 3)
	{
		state->SetError(EvalDim, array.Size(), 3);
		return NOTHING;
	}
	
	if (array[0].GetType() != GameScalar)
	{
		state->TypeError(GameScalar, array[0].GetType());
		return NOTHING;
	}
	float time = array[0];
	
	if (array[1].GetType() != GameScalar)
	{
		state->TypeError(GameScalar, array[1].GetType());
		return NOTHING;
	}
	float zoom = array[1];

	Vector3 pos;
	if (!GetPos(state,pos, array[2]))
	{
		//state->SetError(EvalGen);
		return NOTHING;
	}

	DisplayMap *disp = dynamic_cast<DisplayMap *>(GWorld->Map());
	if (!disp) return NOTHING;
	CStaticMap *map = disp->GetMap();
	if (!map) return NOTHING;
	
	map->AddAnimationPhase(time, zoom, pos);
	return NOTHING;
}

GameValue MapAnimCommit(const GameState *state)
{
	DisplayMap *disp = dynamic_cast<DisplayMap *>(GWorld->Map());
	if (!disp) return NOTHING;
	CStaticMap *map = disp->GetMap();
	if (!map) return NOTHING;
	
	map->CreateInterpolator();
	return NOTHING;
}

GameValue MapAnimDone(const GameState *state)
{
	DisplayMap *disp = dynamic_cast<DisplayMap *>(GWorld->Map());
	if (!disp) return true;
	CStaticMap *map = disp->GetMap();
	if (!map) return true;

	return !map->HasAnimation();
}

GameValue IsMapShown(const GameState *state)
{
	DisplayMap *map = dynamic_cast<DisplayMap *>(GWorld->Map());
	if (!map) return false;
	return map->IsShownMap();
}

GameValue IsWatchShown(const GameState *state)
{
	DisplayMap *map = dynamic_cast<DisplayMap *>(GWorld->Map());
	if (!map) return false;
	return map->IsShownWatch();
}

GameValue IsCompassShown(const GameState *state)
{
	DisplayMap *map = dynamic_cast<DisplayMap *>(GWorld->Map());
	if (!map) return false;
	return map->IsShownCompass();
}

GameValue IsWalkieTalkieShown(const GameState *state)
{
	DisplayMap *map = dynamic_cast<DisplayMap *>(GWorld->Map());
	if (!map) return false;
	return map->IsShownWalkieTalkie();
}

GameValue IsNotepadShown(const GameState *state)
{
	DisplayMap *map = dynamic_cast<DisplayMap *>(GWorld->Map());
	if (!map) return false;
	return map->IsShownNotepad();
}

GameValue IsWarrantShown(const GameState *state)
{
	DisplayMap *map = dynamic_cast<DisplayMap *>(GWorld->Map());
	if (!map) return false;
	return map->IsShownWarrant();
}

GameValue IsGPSShown(const GameState *state)
{
	DisplayMap *map = dynamic_cast<DisplayMap *>(GWorld->Map());
	if (!map) return false;
	return map->IsShownGPS();
}

GameValue ObjSide( const GameState *state, GameValuePar oper1 )
{
	Object *obj1=GetObject(oper1);
	EntityAI *ai=dyn_cast<EntityAI>(obj1);
	if( !ai ) return CreateGameSide(TSideUnknown);
	return CreateGameSide(ai->GetTargetSide());
}

GameValue ObjName( const GameState *state, GameValuePar oper1 )
{
	Object *obj1=GetObject(oper1);
	EntityAI *veh = dyn_cast<EntityAI>(obj1);
	if (!veh) return "Error: No vehicle";
	AIUnit *unit = veh->CommanderUnit();
	if (!unit) unit = veh->PilotUnit();
	if (!unit) unit = veh->GunnerUnit();
	if (!unit) return "Error: No unit";
	return unit->GetPerson()->GetInfo()._name;
}

GameValue SideWest(const GameState *state) {return CreateGameSide(TWest);}
GameValue SideEast(const GameState *state) {return CreateGameSide(TEast);}
GameValue SideCivilian(const GameState *state) {return CreateGameSide(TCivilian);}
GameValue SideResistance(const GameState *state) {return CreateGameSide(TGuerrila);}
GameValue SideLogic(const GameState *state) {return CreateGameSide(TLogic);}
GameValue SideEnemy(const GameState *state) {return CreateGameSide(TEnemy);}
GameValue SideFriendly(const GameState *state) {return CreateGameSide(TFriendly);}

GameValue SideCmpNE( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	TargetSide side1=GetSide(oper1);
	TargetSide side2=GetSide(oper2);
	return side1!=side2;
}

GameValue SideCmpE( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	TargetSide side1=GetSide(oper1);
	TargetSide side2=GetSide(oper2);
	return side1==side2;
}

RString GetSaveDirectory();

GameValue SaveGame(const GameState *state)
{
	// do not save when player is dead
	Person *player = GWorld->GetRealPlayer();
	if (!player || player->IsDammageDestroyed()) return NOTHING;

	RString name = GetSaveDirectory() + RString("autosave.fps");
	GWorld->SaveBin(name, IDS_AUTOSAVE_GAME);
	return NOTHING;
}

GameValue ShowMap( const GameState *state, GameValuePar oper1 )
{
	DisplayMap *map = dynamic_cast<DisplayMap *>(GWorld->Map());
	if (map) map->ShowMap(oper1); 
	return NOTHING;
}

GameValue ShowWatch( const GameState *state, GameValuePar oper1 )
{
	DisplayMap *map = dynamic_cast<DisplayMap *>(GWorld->Map());
	if (map) map->ShowWatch(oper1); 
	return NOTHING;
}

GameValue ShowCompass( const GameState *state, GameValuePar oper1 )
{
	DisplayMap *map = dynamic_cast<DisplayMap *>(GWorld->Map());
	if (map) map->ShowCompass(oper1); 
	return NOTHING;
}

GameValue ShowWalkieTalkie( const GameState *state, GameValuePar oper1 )
{
	DisplayMap *map = dynamic_cast<DisplayMap *>(GWorld->Map());
	if (map) map->ShowWalkieTalkie(oper1); 
	return NOTHING;
}

GameValue ShowNotepad( const GameState *state, GameValuePar oper1 )
{
	DisplayMap *map = dynamic_cast<DisplayMap *>(GWorld->Map());
	if (map) map->ShowNotepad(oper1); 
	return NOTHING;
}

GameValue ShowWarrant( const GameState *state, GameValuePar oper1 )
{
	DisplayMap *map = dynamic_cast<DisplayMap *>(GWorld->Map());
	if (map) map->ShowWarrant(oper1); 
	return NOTHING;
}

GameValue ShowGPS( const GameState *state, GameValuePar oper1 )
{
	DisplayMap *map = dynamic_cast<DisplayMap *>(GWorld->Map());
	if (map) map->ShowGPS(oper1); 
	return NOTHING;
}

GameValue EnableRadio( const GameState *state, GameValuePar oper1 )
{
	GWorld->EnableRadio(oper1);
	return NOTHING;
}

GameValue SetRadioMessage(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	float temp = oper1;
	ArcadeSensorActivation activation;
	switch (toInt(temp))
	{
		case 1:
			activation = ASAAlpha; break;
		case 2:
			activation = ASABravo; break;
		case 3:
			activation = ASACharlie; break;
		case 4:
			activation = ASADelta; break;
		case 5:
			activation = ASAEcho; break;
		case 6:
			activation = ASAFoxtrot; break;
		case 7:
			activation = ASAGolf; break;
		case 8:
			activation = ASAHotel; break;
		case 9:
			activation = ASAIndia; break;
		case 10:
			activation = ASAJuliet; break;
		default:
			return NOTHING;
	}
	GameStringType message = oper2;
	for (int i=0; i<sensorsMap.Size(); i++)
	{
		Vehicle *veh = sensorsMap[i];
		Detector *detector = dyn_cast<Detector>(veh);
		if (!detector) continue;
		if (detector->GetActivationBy() == activation)
		{
			detector->SetText(message);
		}
	}

	return NOTHING;
}

GameValue ShowHint( const GameState *state, GameValuePar oper1 )
{
	GWorld->UI()->ShowHint(oper1);
	return NOTHING;
}

GameValue ShowHintCadet( const GameState *state, GameValuePar oper1 )
{
	if (Glob.config.easyMode) GWorld->UI()->ShowHint(oper1);
	return NOTHING;
}

GameValue ShowHintC( const GameState *state, GameValuePar oper1 )
{
	Display *CurrentDisplay();
	
	Display *disp = CurrentDisplay();
	if (!disp || disp->IDD() != IDD_MISSION) return NOTHING;
	Assert(dynamic_cast<DisplayMission *>(disp));
	DisplayMission *mission = static_cast<DisplayMission *>(disp);
	mission->ShowHint(oper1);
	return NOTHING;
}

GameValue DisableUserInput( const GameState *state, GameValuePar oper1 )
{
	GWorld->DisableUserInput(oper1);
	return NOTHING;
}

GameValue PublicVariable( const GameState *state, GameValuePar oper1 )
{
	GetNetworkManager().PublicVariable(oper1);
	return NOTHING;
}

GameValue ObjLocked( const GameState *state, GameValuePar oper1)
{
	Object *obj=GetObject(oper1);
	if( !obj ) return false;
	Transport *veh=dyn_cast<Transport>(obj);
	if( !veh ) return false;
	return veh->GetLock() == LSLocked;
}

GameValue ObjLock( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj=GetObject(oper1);
	if( !obj ) return NOTHING;
	Transport *veh=dyn_cast<Transport>(obj);
	if( !veh ) return NOTHING;
	bool lock = oper2;
	veh->SetLock(lock ? LSLocked : LSDefault);
	return NOTHING;
}

GameValue ObjStopped( const GameState *state, GameValuePar oper1)
{
	Object *obj=GetObject(oper1);
	if( !obj ) return false;
	EntityAI *veh=dyn_cast<EntityAI>(obj);
	if( !veh ) return false;
	return veh->IsUserStopped();
}

GameValue ObjStop( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj=GetObject(oper1);
	if( !obj ) return NOTHING;
	EntityAI *veh=dyn_cast<EntityAI>(obj);
	if( !veh ) return NOTHING;
	veh->UserStop(oper2);
	return NOTHING;
}

GameValue ObjDisableAI( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj=GetObject(oper1);
	if (!obj) return NOTHING;
	EntityAI *veh=dyn_cast<EntityAI>(obj);
	if (!veh) return NOTHING;
	AIUnit *unit=veh->CommanderUnit();
	if (!unit) return NOTHING;
	// get value identified by string
	GameStringType str = oper2;
	const char *ss = str;
	AIUnit::DisabledAI s = GetEnumValue<AIUnit::DisabledAI>(ss);
	int dai = unit->GetAIDisabled();
	if (s==INT_MIN) s = (AIUnit::DisabledAI)0;
	unit->SetAIDisabled(dai|s);
	return NOTHING;
}

/*!
\patch_internal 1.42 Date 1/4/2002 by Ondra
- Fixed: Better handling of wrong enum values in scripting.
*/

GameValue ObjLand( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj = GetObject(oper1);
	if (!obj) return NOTHING;
	Transport *veh = dyn_cast<Transport>(obj);
	if (!veh) return NOTHING;
	GameStringType name = oper2;
	Transport::LandingMode mode = GetEnumValue<Transport::LandingMode>((const char *)name);
	if (mode==INT_MIN) mode = Transport::LMLand;
	veh->LandStarted(mode);
	return NOTHING;
}

GameValue ObjAssignAsCommander( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	// first argument - unit
	Object *obj1 = GetObject(oper1);
	if (!obj1) return NOTHING;
	Person *soldier = dyn_cast<Person>(obj1);
	if (!soldier) return NOTHING;
	AIUnit *unit = soldier->Brain();
	if (!unit) return NOTHING;

	// second argument - vehicle
	Object *obj2 = GetObject(oper2);
	if (!obj2) return NOTHING;
	Transport *veh = dyn_cast<Transport>(obj2);
	if (!veh) return NOTHING;

	unit->AssignAsCommander(veh);
	return NOTHING;
}

GameValue ObjAssignAsDriver( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	// first argument - unit
	Object *obj1 = GetObject(oper1);
	if (!obj1) return NOTHING;
	Person *soldier = dyn_cast<Person>(obj1);
	if (!soldier) return NOTHING;
	AIUnit *unit = soldier->Brain();
	if (!unit) return NOTHING;

	// second argument - vehicle
	Object *obj2 = GetObject(oper2);
	if (!obj2) return NOTHING;
	Transport *veh = dyn_cast<Transport>(obj2);
	if (!veh) return NOTHING;

	unit->AssignAsDriver(veh);
	return NOTHING;
}

GameValue ObjAssignAsGunner( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	// first argument - unit
	Object *obj1 = GetObject(oper1);
	if (!obj1) return NOTHING;
	Person *soldier = dyn_cast<Person>(obj1);
	if (!soldier) return NOTHING;
	AIUnit *unit = soldier->Brain();
	if (!unit) return NOTHING;

	// second argument - vehicle
	Object *obj2 = GetObject(oper2);
	if (!obj2) return NOTHING;
	Transport *veh = dyn_cast<Transport>(obj2);
	if (!veh) return NOTHING;

	unit->AssignAsGunner(veh);
	return NOTHING;
}

GameValue ObjAssignAsCargo( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	// first argument - unit
	Object *obj1 = GetObject(oper1);
	if (!obj1) return NOTHING;
	Person *soldier = dyn_cast<Person>(obj1);
	if (!soldier) return NOTHING;
	AIUnit *unit = soldier->Brain();
	if (!unit) return NOTHING;

	// second argument - vehicle
	Object *obj2 = GetObject(oper2);
	if (!obj2) return NOTHING;
	Transport *veh = dyn_cast<Transport>(obj2);
	if (!veh) return NOTHING;

	unit->AssignAsCargo(veh);
	return NOTHING;
}

GameValue ObjUnassignVehicle( const GameState *state, GameValuePar oper1 )
{
	// first argument - unit
	Object *obj = GetObject(oper1);
	if (!obj) return NOTHING;
	Person *soldier = dyn_cast<Person>(obj);
	if (!soldier) return NOTHING;
	AIUnit *unit = soldier->Brain();
	if (!unit) return NOTHING;

	unit->UnassignVehicle();
	return NOTHING;
}

GameValue GrpLeaveVehicle( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	// first argument - group
	AIGroup *grp = GetGroup(oper1);
	if (!grp) return NOTHING;

	// second argument - vehicle
	Object *obj = GetObject(oper2);
	if (!obj) return NOTHING;
	Transport *veh = dyn_cast<Transport>(obj);
	if (!veh) return NOTHING;

	grp->UnassignVehicle(veh);
	return NOTHING;
}

static bool PrepareMoveIn( AIUnit *unit, Transport *target)
{
	// check if soldier is in anything
	Transport *in = unit->GetVehicleIn();
	if (in==target) return false;
	if (in)
	{
		// TODO: MoveOut of any vehicle
		RptF
		(
			"MoveIn: Soldier %s already in vehicle %s",
			(const char *)unit->GetDebugName(),
			(const char *)in->GetDebugName()
		);
		return false;
	}
	return true;
}

CameraType ValidateCamera(CameraType cam);

/*!
\patch 1.08 Date 07/26/2001 by Jirka
- Fixed: procedure "moveInCommander" is now MP safe
\patch 1.82 Date 8/9/2002 by Jirka
- Fixed: functions moveIn... can be called only for local soldiers now (ignored for remote soldiers)
*/

GameValue ObjMoveInCommander( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	// first argument - unit
	Object *obj1 = GetObject(oper1);
	if (!obj1) return NOTHING;
	Person *soldier = dyn_cast<Person>(obj1);
	if (!soldier) return NOTHING;
	if (!soldier->IsLocal()) return NOTHING;
	AIUnit *unit = soldier->Brain();
	if (!unit) return NOTHING;

	// second argument - vehicle
	Object *obj2 = GetObject(oper2);
	if (!obj2) return NOTHING;
	Transport *veh = dyn_cast<Transport>(obj2);
	if (!veh) return NOTHING;
	if (!PrepareMoveIn(unit,veh)) return NOTHING;

	if (veh->QCanIGetInCommander(soldier))
	{
		// FIXED
		if (veh->IsLocal())
		{
			veh->GetInCommander(soldier,false);
		}
		else
		{
			GetNetworkManager().AskForGetIn(soldier, veh, GIPCommander);
		}
		soldier->Brain()->AssignAsCommander(veh);
		soldier->Brain()->OrderGetIn(true);
		if (GWorld->FocusOn() == soldier->Brain() && veh->IsLocal())
			GWorld->SwitchCameraTo(veh, ValidateCamera(GWorld->GetCameraType()));
	}
	return NOTHING;
}

/*!
\patch 1.08 Date 07/26/2001 by Jirka
- Fixed: procedure "moveInDriver" is now MP safe
*/
GameValue ObjMoveInDriver( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	// first argument - unit
	Object *obj1 = GetObject(oper1);
	if (!obj1) return NOTHING;
	Person *soldier = dyn_cast<Person>(obj1);
	if (!soldier) return NOTHING;
	if (!soldier->IsLocal()) return NOTHING;
	AIUnit *unit = soldier->Brain();
	if (!unit) return NOTHING;

	// second argument - vehicle
	Object *obj2 = GetObject(oper2);
	if (!obj2) return NOTHING;
	Transport *veh = dyn_cast<Transport>(obj2);
	if (!veh) return NOTHING;
	if (!PrepareMoveIn(unit,veh)) return NOTHING;
/*
RptF
(
	"Move in driver (procedure): %s (%s, id %d:%d) into %s (%s, id %d:%d)",
	(const char *)soldier->GetDebugName(), soldier->IsLocal() ? "LOCAL" : "REMOTE",
	soldier->GetNetworkId().creator, soldier->GetNetworkId().id,
	(const char *)veh->GetDebugName(), veh->IsLocal() ? "LOCAL" : "REMOTE",
	veh->GetNetworkId().creator, veh->GetNetworkId().id
);
*/

	if (veh->QCanIGetIn(soldier))
	{
		// FIXED
		if (veh->IsLocal())
		{
			veh->GetInDriver(soldier,false);
		}
		else
		{
			GetNetworkManager().AskForGetIn(soldier, veh, GIPDriver);
		}
		soldier->Brain()->AssignAsDriver(veh);
		soldier->Brain()->OrderGetIn(true);
		if (GWorld->FocusOn() == soldier->Brain() && veh->IsLocal())
			GWorld->SwitchCameraTo(veh, ValidateCamera(GWorld->GetCameraType()));
	}
	return NOTHING;
}

/*!
\patch 1.08 Date 07/26/2001 by Jirka
- Fixed: procedure "moveInGunner" is now MP safe
*/
GameValue ObjMoveInGunner( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	// first argument - unit
	Object *obj1 = GetObject(oper1);
	if (!obj1) return NOTHING;
	Person *soldier = dyn_cast<Person>(obj1);
	if (!soldier) return NOTHING;
	if (!soldier->IsLocal()) return NOTHING;
	AIUnit *unit = soldier->Brain();
	if (!unit) return NOTHING;

	// second argument - vehicle
	Object *obj2 = GetObject(oper2);
	if (!obj2) return NOTHING;
	Transport *veh = dyn_cast<Transport>(obj2);
	if (!veh) return NOTHING;
	if (!PrepareMoveIn(unit,veh)) return NOTHING;

	if (veh->QCanIGetInGunner(soldier))
	{
		// FIXED
		if (veh->IsLocal())
		{
			veh->GetInGunner(soldier,false);
		}
		else
		{
			GetNetworkManager().AskForGetIn(soldier, veh, GIPGunner);
		}
		soldier->Brain()->AssignAsGunner(veh);
		soldier->Brain()->OrderGetIn(true);
		if (GWorld->FocusOn() == soldier->Brain() && veh->IsLocal())
			GWorld->SwitchCameraTo(veh, ValidateCamera(GWorld->GetCameraType()));
	}
	return NOTHING;
}

/*!
\patch 1.08 Date 07/26/2001 by Jirka
- Fixed: procedure "moveInCargo" is now MP safe
*/
GameValue ObjMoveInCargo( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	// first argument - unit
	Object *obj1 = GetObject(oper1);
	if (!obj1) return NOTHING;
	Person *soldier = dyn_cast<Person>(obj1);
	if (!soldier) return NOTHING;
	if (!soldier->IsLocal()) return NOTHING;
	AIUnit *unit = soldier->Brain();
	if (!unit) return NOTHING;

	// second argument - vehicle
	Object *obj2 = GetObject(oper2);
	if (!obj2) return NOTHING;
	Transport *veh = dyn_cast<Transport>(obj2);
	if (!veh) return NOTHING;
	if (!PrepareMoveIn(unit,veh)) return NOTHING;

	if (veh->QCanIGetInCargo(soldier))
	{
		// FIXED
		if (veh->IsLocal())
		{
			veh->GetInCargo(soldier,false);
		}
		else
		{
			GetNetworkManager().AskForGetIn(soldier, veh, GIPCargo);
		}
		soldier->Brain()->SetState(AIUnit::InCargo);
		soldier->Brain()->AssignAsCargo(veh);
		soldier->Brain()->OrderGetIn(true);
		if (GWorld->FocusOn() == soldier->Brain() && veh->IsLocal())
			GWorld->SwitchCameraTo(veh, ValidateCamera(GWorld->GetCameraType()));
	}
	return NOTHING;
}

GameValue GrpAllowFleeing( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	// first argument - group
	AIGroup *grp = GetGroup(oper1);
	if (!grp) return NOTHING;

	grp->AllowFleeing(oper2);
	return NOTHING;
}

/*!
\patch 1.90 Date 10/25/2002 by Ondra
- Fixed: createVehicle may be used to create shells and other non-ai vehicles.
*/

GameValue VehCreate( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	GameStringType type = oper1;

	Vector3 pos, normal = VUp;
	if (!GetPos(state,pos, oper2))
	{
		//state->SetError(EvalGen);
		return OBJECT_NULL;
	}

	Ref<Entity> veh = NewNonAIVehicle(type,NULL);
	if (!veh) return OBJECT_NULL;
	EntityAI *vehAI = dyn_cast<EntityAI>(veh.GetRef());
	if (vehAI)
	{

		if (AIUnit::FindFreePosition(pos, normal, false, vehAI))
		{
			float dx,dz;
			pos[1] = GLOB_LAND->SurfaceYAboveWater(pos[0], pos[2], &dx, &dz);
			normal = Vector3(-dx,1,-dz);
		}
	}

	Matrix3 dir;
	Matrix4 transform;

	transform.SetPosition(pos);
	dir.SetUpAndDirection(normal,VForward);
	transform.SetOrientation(dir);

	if (vehAI)
	{
		veh->PlaceOnSurface(transform);
	}

	veh->SetTransform(transform);
	veh->Init(transform);

	// if vehicle is static, add it to building list

	if (vehAI)
	{
		if( veh->GetNonAIType()->IsKindOf(GWorld->Preloaded(VTypeStatic)) )
		{
			GWorld->AddBuilding(veh);
			if (GWorld->GetMode() == GModeNetware)
				GetNetworkManager().CreateVehicle(veh, VLTBuilding, "", -1);
		}
		else
		{
			GWorld->AddVehicle(veh);
			if (GWorld->GetMode() == GModeNetware)
				GetNetworkManager().CreateVehicle(veh, VLTVehicle, "", -1);
		}
	}
	else
	{
		GWorld->AddAnimal(veh);
		if (GWorld->GetMode() == GModeNetware)
			GetNetworkManager().CreateVehicle(veh, VLTAnimal, "", -1);
	}

	return GameValueExt(veh.GetRef());
}

/*!
\patch 1.43 Date 1/28/2002 by Ondra
- Fixed: CreateUnit failed when init was specified and could cause crash.
\patch 1.48 Date 7/17/2002 by Ondra
- Fixed: createUnit was broken in MP (unit was created only partially on remote computers).
\patch 1.89 Date 10/23/2002 by Jirka
- Fixed: creatUnit init string was not transferred in MP games
*/

void CreateUnit(AIGroup *group, RString type, Vector3Par position, RString init, float skill, Rank rank)
{
	if (group->NUnits() >= MAX_UNITS_PER_GROUP) return;
	
	Ref<EntityAI> veh = NewVehicle(type);
	Person *soldier = dyn_cast<Person>(veh.GetRef());
	if (!soldier) return;

	Vector3 normal, pos = position;
	if (AIUnit::FindFreePosition(pos, normal, true, veh))
	{
		float dx,dz;
		pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2], &dx, &dz);
	}

	Matrix3 dir;
	Matrix4 transform;

	transform.SetPosition(pos);
	dir.SetUpAndDirection(VUp,VForward);
	transform.SetOrientation(dir);

	veh->PlaceOnSurface(transform);

	veh->SetTransform(transform);
	veh->Init(transform);

	AICenter *center = group->GetCenter();
	Assert(center);

	veh->SetTargetSide(center->GetSide());

  GameValue varValue = GameValueExt(veh);
	GameVarSpace local;
	GWorld->GetGameState()->BeginContext(&local);
  GWorld->GetGameState()->VarSet("this", varValue);
  GWorld->GetGameState()->Execute(init);
	GWorld->GetGameState()->EndContext();

	// if vehicle is static, add it to building list
	GWorld->AddVehicle(veh);
	if (GWorld->GetMode() == GModeNetware)
	{
		GetNetworkManager().CreateVehicle(veh, VLTVehicle, "", -1);

		VehicleInitCmd cmd;
		cmd.vehicle = veh;
		cmd.init = init;
		GetNetworkManager().VehicleInit(cmd);
	}

	// soldier is free soldier - we should add sensor
	GWorld->AddSensor(soldier);

	AIUnit *unit = soldier->Brain();

	unit->Load(center->NextSoldierIdentity(soldier->IsWoman()));

	AIUnitInfo &aiInfo = soldier->GetInfo();
	aiInfo._rank = rank;
	aiInfo._initExperience = aiInfo._experience = AI::ExpForRank(rank);
	unit->SetAbility(skill);

	// Add into AIGroup
	AISubgroup *subgroup = group->MainSubgroup();
	group->AddUnit(unit);
	if (!subgroup)
	{
		Assert(group->MainSubgroup());
		GetNetworkManager().CreateObject(group->MainSubgroup());
	}
	GetNetworkManager().CreateObject(unit);
	if (!group->Leader()) center->SelectLeader(group);
}

GameValue UnitCreate( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	GameStringType type = oper1;
	const GameArrayType &array = oper2;
	
	Vector3 pos;
	AIGroup *grp;
	float skill = 0.5;
	Rank rank = RankPrivate;
	RString init;
	switch (array.Size())
	{
		case 5:
			{
				if (array[4].GetType() != GameString)
				{
					state->TypeError(GameString, array[4].GetType());
					return NOTHING;
				}
				GameStringType rankName = array[4];
				rank = GetEnumValue<Rank>((const char *)rankName);
				if (rank==INT_MIN) rank = RankPrivate;
			}
		case 4:
			{
				if (array[3].GetType() != GameScalar)
				{
					state->TypeError(GameScalar, array[3].GetType());
					return NOTHING;
				}
				skill = array[3];
			}
		case 3:
			{
				if (array[2].GetType() != GameString)
				{
					state->TypeError(GameString, array[2].GetType());
					return NOTHING;
				}
				init = array[2];
			}
		case 2:
			{
				if (!GetRelPos(state,pos, array[0]))
				{
					//state->SetError(EvalGen);
					return NOTHING;
				}
				grp = GetGroup(array[1]);
				if (!grp) return NOTHING;
			}
			break;
		default:
			state->SetError(EvalDim, array.Size(), 5);
			return NOTHING;
	}

	if (grp->IsLocal())
		CreateUnit(grp, type, pos, init, skill, rank);
	else
		GetNetworkManager().AskForCreateUnit(grp, type, pos, init, skill, rank);

	return NOTHING;
}

static void Eject(Person *man, Transport *trans)
{
	if (!man) return;

	AIUnit *unit = man->Brain();
	if (!unit)
	{
		man->SetDelete();
		return;
	}
/*
	trans->Eject(unit);
*/
	unit->DoGetOut(trans, false);

	AIGroup *group = unit->GetGroup();
	if (group) group->UnassignVehicle(trans);
}

void DeleteVehicle(Entity *veh)
{
	// only regular entities may be deleted
	if (veh->Object::GetType()!=TypeVehicle) return;

	Person *man = dyn_cast<Person>(veh);
	if (man)
	{
		AIUnit *unit = man->Brain();
		if (unit)
		{
			if (unit->IsAnyPlayer()) return;
			Transport *transport = unit->VehicleAssigned();
			if (transport) transport->UpdateStop();
			GWorld->RemoveSensor(man);
			
			Ref<AISubgroup> subgrp = unit->GetSubgroup();
			Ref<AIGroup> grp = subgrp ? subgrp->GetGroup() : NULL;
			if (grp)
			{
				bool groupLeader = unit == grp->Leader();
				bool subgroupLeader = unit == subgrp->Leader();
				unit->RemoveFromGroup();
				if (grp->NUnits() > 0 && groupLeader) grp->GetCenter()->SelectLeader(grp);
				if (subgrp->NUnits() == 0)
				{
					if (subgrp != grp->MainSubgroup()) subgrp->RemoveFromGroup();
				}
				else if (subgroupLeader) subgrp->SelectLeader();
			}
			man->SetBrain(NULL);
		}
	}
	else
	{
		Transport *transport = dyn_cast<Transport>(veh);
		if (transport)
		{
			for (int i=transport->GetManCargo().Size(); --i>=0;)
			{
				Eject(transport->GetManCargo()[i], transport);
			}
			Eject(transport->Driver(), transport);
			Eject(transport->Gunner(), transport);
			Eject(transport->Commander(), transport);
			GetNetworkManager().UpdateObject(transport);
		}
	}
	veh->SetDelete();
}

GameValue VehDelete( const GameState *state, GameValuePar oper1 )
{
	Object *obj = GetObject(oper1);
	if (!obj) return NOTHING;

	Entity *veh = dyn_cast<Entity>(obj);
	if (!veh) return NOTHING;

	if (veh->IsLocal())
		DeleteVehicle(veh);
	else
		GetNetworkManager().AskForDeleteVehicle(veh);
	
	return NOTHING;
}

GameValue MarkerGetPos( const GameState *state, GameValuePar oper1 )
{
	GameStringType name = oper1;

	GameValue value = state->CreateGameValue(GameArray);
	GameArrayType &array = value;
	array.Resize(3);
	array[0] = 0.0f;
	array[1] = 0.0f;
	array[2] = 0.0f;
	
	for (int i=0; i<markersMap.Size(); i++)
	{
		ArcadeMarkerInfo &mInfo = markersMap[i];
		if (stricmp(mInfo.name, name) == 0)
		{
			array[0] = mInfo.position.X();
			array[1] = mInfo.position.Z();
			break;
		}
	}
	return value;
}

GameValue MarkerSetPos( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	GameStringType name = oper1;

	Vector3 pos;
	if( !GetPos(state,pos,oper2) )
	{
		//state->SetError(EvalGen);
		return NOTHING;
	}

	for (int i=0; i<markersMap.Size(); i++)
	{
		ArcadeMarkerInfo &mInfo = markersMap[i];
		if (stricmp(mInfo.name, name) == 0)
		{
			mInfo.position = pos;
			break;
		}
	}
	return NOTHING;
}

GameValue MarkerGetType( const GameState *state, GameValuePar oper1 )
{
	GameStringType name = oper1;

	for (int i=0; i<markersMap.Size(); i++)
	{
		ArcadeMarkerInfo &mInfo = markersMap[i];
		if (stricmp(mInfo.name, name) == 0)
			return mInfo.type;
	}
	return GameStringType("");
}

GameValue MarkerSetType( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	GameStringType name = oper1;
	GameStringType type = oper2;

	for (int i=0; i<markersMap.Size(); i++)
	{
		ArcadeMarkerInfo &mInfo = markersMap[i];
		if (stricmp(mInfo.name, name) == 0)
		{
			mInfo.type = type;
			mInfo.OnTypeChanged();
			break;
		}
	}
	return NOTHING;
}

GameValue MarkerGetSize( const GameState *state, GameValuePar oper1 )
{
	GameStringType name = oper1;

	GameValue value = state->CreateGameValue(GameArray);
	GameArrayType &array = value;
	array.Resize(2);
	array[0] = 0.0f;
	array[1] = 0.0f;

	for (int i=0; i<markersMap.Size(); i++)
	{
		ArcadeMarkerInfo &mInfo = markersMap[i];
		if (stricmp(mInfo.name, name) == 0)
		{
			array[0] = mInfo.a;
			array[1] = mInfo.b;
		}
	}
	return value;
}

GameValue MarkerSetSize( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	GameStringType name = oper1;

	const GameArrayType &size = oper2;
	if (size.Size() != 2 || size[0].GetType() != GameScalar || size[1].GetType() != GameScalar)
	{
		state->SetError(EvalGen);
		return NOTHING;
	}

	for (int i=0; i<markersMap.Size(); i++)
	{
		ArcadeMarkerInfo &mInfo = markersMap[i];
		if (stricmp(mInfo.name, name) == 0)
		{
			mInfo.a = size[0];
			mInfo.b = size[1];
			break;
		}
	}
	return NOTHING;
}

GameValue MarkerGetColor( const GameState *state, GameValuePar oper1 )
{
	GameStringType name = oper1;

	for (int i=0; i<markersMap.Size(); i++)
	{
		ArcadeMarkerInfo &mInfo = markersMap[i];
		if (stricmp(mInfo.name, name) == 0)
		{
			return mInfo.colorName;
		}
	}
	return GameStringType("");
}

GameValue MarkerSetColor( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	GameStringType name = oper1;
	GameStringType color = oper2;

	for (int i=0; i<markersMap.Size(); i++)
	{
		ArcadeMarkerInfo &mInfo = markersMap[i];
		if (stricmp(mInfo.name, name) == 0)
		{
			mInfo.colorName = color;
			mInfo.OnColorChanged();
			break;
		}
	}
	return NOTHING;
}

GameValue WpGetPos( const GameState *state, GameValuePar oper1 )
{
	GameValue value = state->CreateGameValue(GameArray);
	GameArrayType &array = value;
	array.Resize(3);
	array[0] = 0.0f;
	array[1] = 0.0f;
	array[2] = 0.0f;
	
	const GameArrayType &wp = oper1;
	if (wp.Size() != 2)
	{
		state->SetError(EvalGen);
		return value;
	}

	AIGroup *grp = GetGroup(wp[0]);
	if (!grp) return value;

	if (wp[1].GetType() != GameScalar)
	{
		state->SetError(EvalGen);
		return value;
	}
	int index = toInt((float)wp[1]);

	if (index < 0 || index >= grp->NWaypoints()) return value;
	ArcadeWaypointInfo &wInfo = grp->GetWaypoint(index);
	array[0] = wInfo.position.X();
	array[1] = wInfo.position.Z();

	return value;
}

GameValue WpSetPos( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	const GameArrayType &wp = oper1;
	if (wp.Size() != 2)
	{
		state->SetError(EvalGen);
		return NOTHING;
	}

	AIGroup *grp = GetGroup(wp[0]);
	if (!grp) return NOTHING;

	if (wp[1].GetType() != GameScalar)
	{
		state->SetError(EvalGen);
		return NOTHING;
	}
	int index = toInt((float)wp[1]);

	Vector3 pos;
	if (!GetPos(state,pos,oper2))
	{
		//state->SetError(EvalGen);
		return NOTHING;
	}

	if (index < 0 || index >= grp->NWaypoints()) return NOTHING;
	ArcadeWaypointInfo &wInfo = grp->GetWaypoint(index);
	wInfo.position = pos;

	int &cur = grp->GetCurrent()->_fsm->Var(0);
	if (cur == index)
	{
		AIGroupContext ctx(grp);
		ctx._fsm = grp->GetCurrent()->_fsm;
		ctx._task = const_cast<Mission *>(grp->GetMission());
		ctx._fsm->SetState(1, &ctx);
	}

	return NOTHING;
}

RString GetAppVersion();

#if 0
float VersionToFloat(const char *ptr);
#else
int VersionToInt(const char *ptr);
#endif

GameValue RequiredVersion( const GameState *state, GameValuePar oper1 )
{
	RString strVersion = GetAppVersion();
#if 0
	float version = VersionToFloat(strVersion);
#else
	int version = VersionToInt(strVersion);
#endif

	RString strRequired = oper1;
#if 0
	float required = VersionToFloat(strRequired);
#else
	int required = VersionToInt(strRequired);
#endif
	if (required > version)
	{
		WarningMessage
		(
			LocalizeString(IDS_MSG_MISSION_VERSION), (const char *)strRequired
		);
		return false;
	}
	return true;
}

GameValue EstimatedTimeLeft( const GameState *state, GameValuePar oper1 )
{
	float t = oper1;
	GetNetworkManager().SetEstimatedEndTime((t <= 7200) ? (Glob.time + t) : ::TIME_MIN);
	return NOTHING;
}

GameValue StrFormat( const GameState *state, GameValuePar oper1 )
{
	const GameArrayType &array = oper1;
	if (array.Size() < 1 ||	array[0].GetType() != GameString) return "";
	RString format = array[0];

	int nParams = array.Size() - 1;
	char result[2048], c;
	const char *src = format;
	char *dst = result;

	while ((c = *src)!=0)
	{
		if (c == '%')
		{
			src++;
			int index = 0;
			while (isdigit(*src))
			{
				index *= 10;
				index += *src - '0';
				src++;
			}
			if (index < 1 || index > nParams) continue;
			const GameValue &value = array[index];
			if (value.GetType()==GameString)
			{
				RString text = value.GetData()->GetString();
				strcpy(dst, text);
				dst += text.GetLength();
			}
			else
			{
				RString text = value.GetText();
				strcpy(dst, text);
				dst += text.GetLength();
			}
		}
		else
		{
			*dst = c;
			src++;
			dst++;
		}
	}
	*dst = 0;
	return result;
}

GameValue StrLocalize( const GameState *state, GameValuePar oper1 )
{
	GameStringType name = oper1;
	return LocalizeString(name);
}

#if _ENABLE_CHEATS
extern AutoArray<RString> DebugConsoleLog;
#endif

GameValue TextLog(const GameState *state, GameValuePar oper1)
{
#if _ENABLE_CHEATS
	DebugConsoleLog.Add(oper1.GetText());
#endif
	return NOTHING;
}

GameValue TextDebugLog(const GameState *state, GameValuePar oper1)
{
#if _ENABLE_CHEATS
	DebugConsoleLog.Add(oper1.GetDebugText());
#endif
	return NOTHING;
}

GameValue EnableEndDialog(const GameState *state)
{
	GWorld->EnableEndDialog(true);
	return NOTHING;
}

GameValue ForceEnd(const GameState *state)
{
	GWorld->ForceEnd(true);
	return NOTHING;
}

// save / load object status

#include "saveVersion.hpp"

GameValue ObjSaveStatus( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj = GetObject(oper1);
	if (!obj) return false;

	Person *person = dyn_cast<Person>(obj);
	if (person)
	{
		RString identity = person->GetInfo()._identityContext;
		if (identity.GetLength() > 0)
		{
			void AddDeadIdentity(RString identity);
			AddDeadIdentity(identity);
		}
	}

	RString name = oper2;
	if (name.GetLength() == 0) return false;
	
	RString GetCampaignSaveDirectory(RString campaign);
	RString filename = GetCampaignSaveDirectory(CurrentCampaign) + RString("objects.sav");

	ParamArchiveSave ar(UnitStatusVersion);
	if (!ar.ParseBin(filename) && ar.Parse(filename) != LSOK) return false;
	
	ParamArchive arObjects;
	if (!ar.OpenSubclass("Objects", arObjects)) return false;

	ParamArchive arCls;
	if (!arObjects.OpenSubclass(name, arCls)) return false;
	
	if (obj->Serialize(arCls) != LSOK) return false;

	arObjects.CloseSubclass(arCls);
	ar.CloseSubclass(arObjects);
#if _ENABLE_CHEATS
	return ar.Save(filename) == LSOK;
#else
	return ar.SaveBin(filename);
#endif
}

GameValue DeleteStatus( const GameState *state, GameValuePar oper1)
{
	RString name = oper1;
	if (name.GetLength() == 0) return false;
	
	RString GetCampaignSaveDirectory(RString campaign);
	RString filename = GetCampaignSaveDirectory(CurrentCampaign) + RString("objects.sav");

	ParamFile file;
	if (!file.ParseBin(filename) && file.Parse(filename) != LSOK) return false;

	ParamEntry *cls = file.FindEntry("Objects");
	if (cls) cls->Delete(name);
#if _ENABLE_CHEATS
	return file.Save(filename) == LSOK;
#else
	return file.SaveBin(filename);
#endif
}

GameValue ObjLoadStatus( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj = GetObject(oper1);
	if (!obj) return false;

	RString name = oper2;
	if (name.GetLength() == 0) return false;
	
	RString GetCampaignSaveDirectory(RString campaign);
	RString filename = GetCampaignSaveDirectory(CurrentCampaign) + RString("objects.sav");

	ParamArchiveLoad ar;
	if (!ar.LoadBin(filename) && ar.Load(filename) != LSOK) return false;
	
	ParamArchive arObjects;
	if (!ar.OpenSubclass("Objects", arObjects)) return false;

	ParamArchive arCls;
	if (!arObjects.OpenSubclass(name, arCls)) return false;
	
	arCls.FirstPass();
	if (obj->Serialize(arCls) != LSOK) return false;
	arCls.SecondPass();
	if (obj->Serialize(arCls) != LSOK) return false;

	arObjects.CloseSubclass(arCls);
	ar.CloseSubclass(arObjects);
	return true;
}

GameValue ObjSaveIdentity( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj = GetObject(oper1);
	Person *person = dyn_cast<Person>(obj);
	if (!person) return false;

	RString identity = person->GetInfo()._identityContext;
	if (identity.GetLength() > 0)
	{
		void AddDeadIdentity(RString identity);
		AddDeadIdentity(identity);
	}
	
	RString name = oper2;
	if (name.GetLength() == 0) return false;
	
	RString GetCampaignSaveDirectory(RString campaign);
	RString filename = GetCampaignSaveDirectory(CurrentCampaign) + RString("objects.sav");

	ParamArchiveSave ar(UnitStatusVersion);
	if (!ar.ParseBin(filename) && ar.Parse(filename) != LSOK) return false;
	
	ParamArchive arIdentities;
	if (!ar.OpenSubclass("Identities", arIdentities)) return false;

	ParamArchive arCls;
	if (!arIdentities.OpenSubclass(name, arCls)) return false;
	
	if (person->SerializeIdentity(arCls) != LSOK) return false;

	arIdentities.CloseSubclass(arCls);
	ar.CloseSubclass(arIdentities);

#if _ENABLE_CHEATS
	return ar.Save(filename) == LSOK;
#else
	return ar.SaveBin(filename);
#endif
}

GameValue DeleteIdentity( const GameState *state, GameValuePar oper1)
{
	RString name = oper1;
	if (name.GetLength() == 0) return false;
	
	RString GetCampaignSaveDirectory(RString campaign);
	RString filename = GetCampaignSaveDirectory(CurrentCampaign) + RString("objects.sav");

	ParamFile file;
	if (!file.ParseBin(filename) && file.Parse(filename) != LSOK) return false;

	ParamEntry *cls = file.FindEntry("Identities");
	if (cls) cls->Delete(name);
#if _ENABLE_CHEATS
	return file.Save(filename) == LSOK;
#else
	return file.SaveBin(filename);
#endif
}

GameValue ObjLoadIdentity( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj = GetObject(oper1);
	Person *person = dyn_cast<Person>(obj);
	if (!person) return false;

	RString name = oper2;
	if (name.GetLength() == 0) return false;
	
	RString GetCampaignSaveDirectory(RString campaign);
	RString filename = GetCampaignSaveDirectory(CurrentCampaign) + RString("objects.sav");

	ParamArchiveLoad ar;
	if (!ar.LoadBin(filename) && ar.Load(filename) != LSOK) return false;
	
	ParamArchive arIdentities;
	if (!ar.OpenSubclass("Identities", arIdentities)) return false;

	ParamArchive arCls;
	if (!arIdentities.OpenSubclass(name, arCls)) return false;
	
	arCls.FirstPass();
	if (person->SerializeIdentity(arCls) != LSOK) return false;
	arCls.SecondPass();
	if (person->SerializeIdentity(arCls) != LSOK) return false;

	arIdentities.CloseSubclass(arCls);
	ar.CloseSubclass(arIdentities);
	return true;
}

//! find description of resource in configuration files
/*!
	Description is class in configuaration file
	Order of searching:
	- description.ext of current mission
	- description.ext of current campaign
	- global resource
	\param name name of class
	\return description class
*/
static const ParamEntry *FindResource(RString name)
{
	// find in mission
	const ParamEntry *entry = ExtParsMission.FindEntry(name);
	if (entry) return entry;
	
	// find in campaign
	entry = ExtParsCampaign.FindEntry(name);
	if (entry) return entry;

	// find in resource
	entry = Res.FindEntry(name);
	if (entry) return entry;

	WarningMessage("Resource %s not found", (const char *)name);
	return NULL;
}

static ControlsContainer *GetUserDialog()
{
	ControlsContainer *dlg = dynamic_cast<ControlsContainer *>(GWorld->UserDialog());
	if (dlg)
	{
		while (dlg->Child()) dlg = dlg->Child();
	}
	return dlg;
}

static IControl *GetCtrl( const GameState *state, GameValuePar oper1)
{
	int idc = toInt((float)oper1);
	ControlsContainer *dlg = GetUserDialog();
	if (!dlg) return NULL;
	IControl *ctrl = dlg->GetCtrl(idc);
	return ctrl;
}

class UserDisplay : public Display
{
protected:
	//@{
	//! partial exit code
	int _exitDIK;
	int _exitVK;
	//@}
public:
	UserDisplay(ControlsContainer *parent) : Display(parent)
	{
		GInput.ChangeGameFocus(+1);
		_exitDIK = -1;
		_exitVK = -1;
	}
	~UserDisplay() {GInput.ChangeGameFocus(-1);}
	void DestroyHUD(int exit) { GWorld->DestroyUserDialog(); }

	void OnButtonClicked(int idc);
	void OnSimulate(EntityAI *vehicle);
};

void UserDisplay::OnButtonClicked(int idc)
{
	switch (idc)
	{
		case IDC_CANCEL:
			_exitVK = idc;
			if (_exitDIK == idc) Exit(idc);
			break;
		default:
			Display::OnButtonClicked(idc);
			break;
	}
}

void UserDisplay::OnSimulate(EntityAI *vehicle)
{
	// !!! Do not use GetKeysToDo - returns always false in this context
	if (GInput.keysToDo[DIK_ESCAPE])
	{
		GInput.keysToDo[DIK_ESCAPE] = false;
		_exitDIK = IDC_CANCEL;
		if (_exitVK == IDC_CANCEL) Exit(IDC_CANCEL);
		return;
	}
	Display::OnSimulate(vehicle);
}

AbstractOptionsUI *CreateUserDialog(RString name)
{
	Display *dlg = new UserDisplay(NULL);
	dlg->Load(name);
	return dlg;
}

GameValue DialogCreate( const GameState *state, GameValuePar oper1 )
{
	GameStringType name = oper1;
	const ParamEntry *cls = FindResource(name);
	if (!cls) return false;

	ControlsContainer *parent = GetUserDialog();
	
	Display *dlg = NULL;
	if (parent) dlg = new Display(parent);
	else dlg = new UserDisplay(parent);
	dlg->Load(*cls);
	if (parent) parent->CreateChild(dlg);
	else GWorld->SetUserDialog(dlg);
	return true;
}

GameValue DialogClose( const GameState *state, GameValuePar oper1 )
{
	ControlsContainer *dlg = GetUserDialog();
	if (dlg)
	{
		int idc = toInt((float)oper1);
		dlg->Exit(idc);
	}
	return NOTHING;
}

GameValue IsDialog(const GameState *state)
{
	ControlsContainer *dlg = GetUserDialog();
	return dlg != NULL;
}

GameValue CtrlVisible( const GameState *state, GameValuePar oper1 )
{
	IControl *ctrl = GetCtrl(state, oper1);
	if (!ctrl) return false;
	return ctrl->IsVisible();
}

GameValue CtrlEnabled( const GameState *state, GameValuePar oper1 )
{
	IControl *ctrl = GetCtrl(state, oper1);
	if (!ctrl) return false;
	return ctrl->IsEnabled();
}

GameValue CtrlShow( const GameState *state, GameValuePar oper1 )
{
	const GameArrayType &array = oper1;
	if
	(
		array.Size() != 2 ||
		array[0].GetType() != GameScalar ||
		array[1].GetType() != GameBool
	) return NOTHING;

	IControl *ctrl = GetCtrl(state, array[0]);
	if (!ctrl) return NOTHING;
	ctrl->ShowCtrl(array[1]);
	return NOTHING;
}

GameValue CtrlEnable( const GameState *state, GameValuePar oper1 )
{
	const GameArrayType &array = oper1;
	if
	(
		array.Size() != 2 ||
		array[0].GetType() != GameScalar ||
		array[1].GetType() != GameBool
	) return NOTHING;

	IControl *ctrl = GetCtrl(state, array[0]);
	if (!ctrl) return NOTHING;
	ctrl->EnableCtrl(array[1]);
	return NOTHING;
}

GameValue CtrlGetText( const GameState *state, GameValuePar oper1 )
{
	IControl *ctrl = GetCtrl(state, oper1);
	if (!ctrl) return "";

	int type = ctrl->GetType();
	switch (type)
	{
		case CT_STATIC:
			{
				CStatic *s = static_cast<CStatic *>(ctrl);
				return s ? s->GetText() : "";
			}
		case CT_BUTTON:
			{
				CButton *s = static_cast<CButton *>(ctrl);
				return s ? s->GetText() : "";
			}
		case CT_EDIT:
			{
				CEdit *s = static_cast<CEdit *>(ctrl);
				return s ? s->GetText() : "";
			}
		case CT_ACTIVETEXT:
			{
				CActiveText *s = static_cast<CActiveText *>(ctrl);
				return s ? s->GetText() : "";
			}
		case CT_3DSTATIC:
			{
				C3DStatic *s = static_cast<C3DStatic *>(ctrl);
				return s ? s->GetText() : "";
			}
		case CT_3DACTIVETEXT:
			{
				C3DActiveText *s = static_cast<C3DActiveText *>(ctrl);
				return s ? s->GetText() : "";
			}
		case CT_3DEDIT:
			{
				C3DEdit *s = static_cast<C3DEdit *>(ctrl);
				return s ? s->GetText() : "";
			}
		default:
			return "";
	}
}

GameValue CtrlSetText( const GameState *state, GameValuePar oper1 )
{
	const GameArrayType &array = oper1;
	if
	(
		array.Size() != 2 ||
		array[0].GetType() != GameScalar ||
		array[1].GetType() != GameString
	) return NOTHING;

	IControl *ctrl = GetCtrl(state, array[0]);
	if (!ctrl) return NOTHING;

	RString text = array[1];

	int type = ctrl->GetType();
	switch (type)
	{
		case CT_STATIC:
			{
				CStatic *s = static_cast<CStatic *>(ctrl);
				s->SetText(text);
				break;
			}
		case CT_BUTTON:
			{
				CButton *s = static_cast<CButton *>(ctrl);
				s->SetText(text);
				break;
			}
		case CT_EDIT:
			{
				CEdit *s = static_cast<CEdit *>(ctrl);
				s->SetText(text);
				break;
			}
		case CT_ACTIVETEXT:
			{
				CActiveText *s = static_cast<CActiveText *>(ctrl);
				s->SetText(text);
				break;
			}
		case CT_3DSTATIC:
			{
				C3DStatic *s = static_cast<C3DStatic *>(ctrl);
				s->SetText(text);
				break;
			}
		case CT_3DACTIVETEXT:
			{
				C3DActiveText *s = static_cast<C3DActiveText *>(ctrl);
				s->SetText(text);
				break;
			}
		case CT_3DEDIT:
			{
				C3DEdit *s = static_cast<C3DEdit *>(ctrl);
				s->SetText(text);
				break;
			}
		default:
			break;
	}
	return NOTHING;
}

GameValue ButtonGetAction( const GameState *state, GameValuePar oper1 )
{
	IControl *ctrl = GetCtrl(state, oper1);
	if (!ctrl) return "";

	int type = ctrl->GetType();
	switch (type)
	{
		case CT_BUTTON:
			{
				CButton *s = static_cast<CButton *>(ctrl);
				return s ? s->GetAction() : "";
			}
		case CT_ACTIVETEXT:
			{
				CActiveText *s = static_cast<CActiveText *>(ctrl);
				return s ? s->GetAction() : "";
			}
		case CT_3DACTIVETEXT:
			{
				C3DActiveText *s = static_cast<C3DActiveText *>(ctrl);
				return s ? s->GetAction() : "";
			}
		default:
			return "";
	}
}

GameValue ButtonSetAction( const GameState *state, GameValuePar oper1 )
{
	const GameArrayType &array = oper1;
	if
	(
		array.Size() != 2 ||
		array[0].GetType() != GameScalar ||
		array[1].GetType() != GameString
	) return NOTHING;

	IControl *ctrl = GetCtrl(state, array[0]);
	if (!ctrl) return NOTHING;

	RString text = array[1];

	int type = ctrl->GetType();
	switch (type)
	{
		case CT_BUTTON:
			{
				CButton *s = static_cast<CButton *>(ctrl);
				s->SetAction(text);
				break;
			}
		case CT_ACTIVETEXT:
			{
				CActiveText *s = static_cast<CActiveText *>(ctrl);
				s->SetAction(text);
				break;
			}
		case CT_3DACTIVETEXT:
			{
				C3DActiveText *s = static_cast<C3DActiveText *>(ctrl);
				s->SetAction(text);
				break;
			}
		default:
			break;
	}
	return NOTHING;
}

static CListBoxContainer *GetListbox(const GameState *state, GameValuePar oper1)
{
	IControl *ctrl = GetCtrl(state, oper1);
	if (!ctrl) return NULL;
	
	int type = ctrl->GetType();
	switch (type)
	{
		case CT_COMBO:
			{
				CCombo *lb = static_cast<CCombo *>(ctrl);
				return lb;
			}
		case CT_LISTBOX:
			{
				CListBox *lb = static_cast<CListBox *>(ctrl);
				return lb;
			}
		case CT_3DLISTBOX:
			{
				C3DListBox *lb = static_cast<C3DListBox *>(ctrl);
				return lb;
			}
		default:
			return NULL;
	}
}

GameValue LBGetSize( const GameState *state, GameValuePar oper1 )
{
	CListBoxContainer *lb = GetListbox(state, oper1);
	if (!lb) return 0.0f;

	return (float)lb->GetSize();
}

GameValue LBGetCurSel( const GameState *state, GameValuePar oper1 )
{
	CListBoxContainer *lb = GetListbox(state, oper1);
	if (!lb) return -1.0f;

	return (float)lb->GetCurSel();
}

GameValue LBSetCurSel( const GameState *state, GameValuePar oper1 )
{
	const GameArrayType &array = oper1;
	if
	(
		array.Size() != 2 ||
		array[0].GetType() != GameScalar ||
		array[1].GetType() != GameScalar
	) return NOTHING;

	IControl *ctrl = GetCtrl(state, array[0]);
	if (!ctrl) return NOTHING;
	
	int sel = toInt((float)array[1]);
	int type = ctrl->GetType();
	switch (type)
	{
		case CT_COMBO:
			{
				CCombo *lb = static_cast<CCombo *>(ctrl);
				lb->SetCurSel(sel);
				break;
			}
		case CT_LISTBOX:
			{
				CListBox *lb = static_cast<CListBox *>(ctrl);
				lb->SetCurSel(sel);
				break;
			}
		case CT_3DLISTBOX:
			{
				C3DListBox *lb = static_cast<C3DListBox *>(ctrl);
				lb->SetCurSel(sel);
				break;
			}
	}
	return NOTHING;
}

GameValue LBClear( const GameState *state, GameValuePar oper1 )
{
	CListBoxContainer *lb = GetListbox(state, oper1);
	if (!lb) return NOTHING;

	lb->ClearStrings();
	return NOTHING;
}

GameValue LBAdd( const GameState *state, GameValuePar oper1 )
{
	const GameArrayType &array = oper1;
	if
	(
		array.Size() != 2 ||
		array[0].GetType() != GameScalar ||
		array[1].GetType() != GameString
	) return 0.0f;

	CListBoxContainer *lb = GetListbox(state, array[0]);
	if (!lb) return 0.0f;

	RString text = array[1];
	return (float)lb->AddString(text);
}

GameValue LBDelete( const GameState *state, GameValuePar oper1 )
{
	const GameArrayType &array = oper1;
	if
	(
		array.Size() != 2 ||
		array[0].GetType() != GameScalar ||
		array[1].GetType() != GameScalar
	) return NOTHING;

	CListBoxContainer *lb = GetListbox(state, array[0]);
	if (!lb) return NOTHING;

	int index = toInt((float)array[1]);
	lb->DeleteString(index);
	return NOTHING;
}

GameValue LBGetText( const GameState *state, GameValuePar oper1 )
{
	const GameArrayType &array = oper1;
	if
	(
		array.Size() != 2 ||
		array[0].GetType() != GameScalar ||
		array[1].GetType() != GameScalar
	) return "";

	CListBoxContainer *lb = GetListbox(state, array[0]);
	if (!lb) return "";

	int index = toInt((float)array[1]);
	return lb->GetText(index);
}

GameValue LBGetData( const GameState *state, GameValuePar oper1 )
{
	const GameArrayType &array = oper1;
	if
	(
		array.Size() != 2 ||
		array[0].GetType() != GameScalar ||
		array[1].GetType() != GameScalar
	) return "";

	CListBoxContainer *lb = GetListbox(state, array[0]);
	if (!lb) return "";

	int index = toInt((float)array[1]);
	return lb->GetData(index);
}

GameValue LBSetData( const GameState *state, GameValuePar oper1 )
{
	const GameArrayType &array = oper1;
	if
	(
		array.Size() != 3 ||
		array[0].GetType() != GameScalar ||
		array[1].GetType() != GameScalar ||
		array[2].GetType() != GameString
	) return NOTHING;

	CListBoxContainer *lb = GetListbox(state, array[0]);
	if (!lb) return NOTHING;

	int index = toInt((float)array[1]);
	RString text = array[2];
	lb->SetData(index, text);
	return NOTHING;
}

GameValue LBGetValue( const GameState *state, GameValuePar oper1 )
{
	const GameArrayType &array = oper1;
	if
	(
		array.Size() != 2 ||
		array[0].GetType() != GameScalar ||
		array[1].GetType() != GameScalar
	) return 0.0f;

	CListBoxContainer *lb = GetListbox(state, array[0]);
	if (!lb) return 0.0f;

	int index = toInt((float)array[1]);
	return (float)lb->GetValue(index);
}

GameValue LBSetValue( const GameState *state, GameValuePar oper1 )
{
	const GameArrayType &array = oper1;
	if
	(
		array.Size() != 3 ||
		array[0].GetType() != GameScalar ||
		array[1].GetType() != GameScalar ||
		array[2].GetType() != GameScalar
	) return NOTHING;

	CListBoxContainer *lb = GetListbox(state, array[0]);
	if (!lb) return NOTHING;

	int index = toInt((float)array[1]);
	int value = toInt((float)array[2]);
	lb->SetValue(index, value);
	return NOTHING;
}

GameValue LBGetPicture( const GameState *state, GameValuePar oper1 )
{
	const GameArrayType &array = oper1;
	if
	(
		array.Size() != 2 ||
		array[0].GetType() != GameScalar ||
		array[1].GetType() != GameScalar
	) return "";

	CListBoxContainer *lb = GetListbox(state, array[0]);
	if (!lb) return "";

	int index = toInt((float)array[1]);
	Texture *texture = lb->GetTexture(index);
	return texture ? texture->GetName() : "";
}

GameValue LBSetPicture( const GameState *state, GameValuePar oper1 )
{
	const GameArrayType &array = oper1;
	if
	(
		array.Size() != 3 ||
		array[0].GetType() != GameScalar ||
		array[1].GetType() != GameScalar ||
		array[2].GetType() != GameString
	) return NOTHING;

	CListBoxContainer *lb = GetListbox(state, array[0]);
	if (!lb) return NOTHING;

	int index = toInt((float)array[1]);
	RString FindPicture (RString name);
	RString name = FindPicture(array[2]);
	if (name.GetLength() > 0)
	{
		name.Lower();
		lb->SetTexture(index, GlobLoadTexture(name));
	}
	return NOTHING;
}

static bool ReadColor(PackedColor &color, GameValuePar value)
{
	const GameArrayType &array = value;
	if
	(
		array.Size() != 4 ||
		array[0].GetType() != GameScalar ||
		array[1].GetType() != GameScalar ||
		array[2].GetType() != GameScalar ||
		array[3].GetType() != GameScalar
	) return false;

	color = PackedColor(Color(array[0], array[1], array[2], array[3]));
	return true;
}

static void WriteColor(GameArrayType &array, PackedColor color)
{
	const float coef = 1.0f / 255.0f;
	
	array.Clear();
	array.Add(coef * color.R8());
	array.Add(coef * color.G8());
	array.Add(coef * color.B8());
	array.Add(coef * color.A8());
}

GameValue LBGetColor( const GameState *state, GameValuePar oper1 )
{
	GameValue value = state->CreateGameValue(GameArray);
	GameArrayType &result = value;

	const GameArrayType &array = oper1;
	if
	(
		array.Size() != 2 ||
		array[0].GetType() != GameScalar ||
		array[1].GetType() != GameScalar
	) return value;

	CListBoxContainer *lb = GetListbox(state, array[0]);
	if (!lb) return value;

	int index = toInt((float)array[1]);
	PackedColor color = lb->GetFtColor(index);
	WriteColor(result, color);

	return value;
}

GameValue LBSetColor( const GameState *state, GameValuePar oper1 )
{
	const GameArrayType &array = oper1;
	if
	(
		array.Size() != 3 ||
		array[0].GetType() != GameScalar ||
		array[1].GetType() != GameScalar ||
		array[2].GetType() != GameArray
	) return NOTHING;

	CListBoxContainer *lb = GetListbox(state, array[0]);
	if (!lb) return NOTHING;

	int index = toInt((float)array[1]);
	PackedColor color;
	if (!ReadColor(color, array[2])) return NOTHING;

	lb->SetFtColor(index, color);
	lb->SetSelColor(index, color);
	return NOTHING;
}

static CSliderContainer *GetSlider(const GameState *state, GameValuePar oper1)
{
	IControl *ctrl = GetCtrl(state, oper1);
	if (!ctrl) return NULL;
	
	int type = ctrl->GetType();
	switch (type)
	{
		case CT_SLIDER:
			{
				CSlider *slider = static_cast<CSlider *>(ctrl);
				return slider;
			}
		case CT_3DSLIDER:
			{
				C3DSlider *slider = static_cast<C3DSlider *>(ctrl);
				return slider;
			}
		default:
			return NULL;
	}
}

GameValue SliderGetPosition(const GameState *state, GameValuePar oper1)
{
	CSliderContainer *slider = GetSlider(state, oper1);
	if (!slider) return 0.0f;

	return slider->GetThumbPos();
}

GameValue SliderSetPosition(const GameState *state, GameValuePar oper1)
{
	const GameArrayType &array = oper1;
	if
	(
		array.Size() != 2 ||
		array[0].GetType() != GameScalar ||
		array[1].GetType() != GameScalar
	) return NOTHING;

	CSliderContainer *slider = GetSlider(state, array[0]);
	if (slider) slider->SetThumbPos(array[1]);
	return NOTHING;
}

GameValue SliderGetRange(const GameState *state, GameValuePar oper1)
{
	GameValue value = state->CreateGameValue(GameArray);
	GameArrayType &result = value;
	
	float minPos = 0, maxPos = 0;

	CSliderContainer *slider = GetSlider(state, oper1);
	if (slider) slider->GetRange(minPos, maxPos);
	
	result.Add(minPos);
	result.Add(maxPos);
	return result;
}

GameValue SliderSetRange(const GameState *state, GameValuePar oper1)
{
	const GameArrayType &array = oper1;
	if
	(
		array.Size() != 3 ||
		array[0].GetType() != GameScalar ||
		array[1].GetType() != GameScalar ||
		array[2].GetType() != GameScalar
	) return NOTHING;

	CSliderContainer *slider = GetSlider(state, array[0]);
	if (slider) slider->SetRange(array[1], array[2]);
	return NOTHING;
}

GameValue SliderGetSpeed(const GameState *state, GameValuePar oper1)
{
	GameValue value = state->CreateGameValue(GameArray);
	GameArrayType &result = value;
	
	float line = 0, page = 0;

	CSliderContainer *slider = GetSlider(state, oper1);
	if (slider) slider->GetSpeed(line, page);
	
	result.Add(line);
	result.Add(page);
	return result;
}

GameValue SliderSetSpeed(const GameState *state, GameValuePar oper1)
{
	const GameArrayType &array = oper1;
	if
	(
		array.Size() != 3 ||
		array[0].GetType() != GameScalar ||
		array[1].GetType() != GameScalar ||
		array[2].GetType() != GameScalar
	) return NOTHING;

	CSliderContainer *slider = GetSlider(state, array[0]);
	if (slider) slider->SetSpeed(array[1], array[2]);
	return NOTHING;
}

//! script name - callback
RString GBriefingOnPlan;
//! script name - callback
RString GBriefingOnNotes;
//! script name - callback
RString GBriefingOnGear;
//! script name - callback
RString GBriefingOnGroup;
//! expression callback when map is single clicked (not called when double clicked)
RString GMapOnSingleClick;

GameValue BriefingOnPlan( const GameState *state, GameValuePar oper1 )
{
	GBriefingOnPlan = oper1;
	return NOTHING;
}

GameValue BriefingOnNotes( const GameState *state, GameValuePar oper1 )
{
	GBriefingOnNotes = oper1;
	return NOTHING;
}

GameValue BriefingOnGear( const GameState *state, GameValuePar oper1 )
{
	GBriefingOnGear = oper1;
	return NOTHING;
}

GameValue BriefingOnGroup( const GameState *state, GameValuePar oper1 )
{
	GBriefingOnGroup = oper1;
	return NOTHING;
}

/*!
\patch 1.85 Date 9/10/2002 by Ondra
- New: Scripting: Added function onMapSingleClick <command-string>. 
*/

GameValue MapOnSingleClick( const GameState *state, GameValuePar oper1 )
{
	GMapOnSingleClick = oper1;
	return NOTHING;
}

GameValue ObjAnimate( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj = GetObject(oper1);
	Entity *veh = dyn_cast<Entity>(obj);
	if (!veh) return NOTHING;

	const GameArrayType &array = oper2;
	if (array.Size() != 2)
	{
		state->SetError(EvalDim, array.Size(), 2);
		return NOTHING;
	}
	if (array[0].GetType() != GameString)
	{
		state->TypeError(GameString, array[0].GetType());
		return NOTHING;
	}
	if (array[1].GetType() != GameScalar)
	{
		state->TypeError(GameScalar, array[1].GetType());
		return NOTHING;
	}

	RString animation = array[0];
	float phase = array[1];
	if (veh->IsLocal())
	{	
		veh->SetAnimationPhase(animation, phase);
		NetworkId id = veh->GetNetworkId();
		if (id.creator == STATIC_OBJECT)
			// no automatic update is performed - update manually
			GetNetworkManager().AskForAnimationPhase(veh, animation, phase);
	}
	else
		GetNetworkManager().AskForAnimationPhase(veh, animation, phase);
	return NOTHING;
}

GameValue ObjAnimationPhase( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj = GetObject(oper1);
	Entity *veh = dyn_cast<Entity>(obj);
	if (!veh) return 0.0f;
	RString animation = oper2;
	return veh->GetAnimationPhase(animation);
}

GameValue ObjChangeModel( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	Object *obj = GetObject(oper1);
	if (!obj) return NOTHING;

	RString name = oper2;
	if (name.GetLength() == 0) return NOTHING;

	if (!QIFStreamB::FileExist(name)) return NOTHING;
	
	LODShape *shape = obj->GetShape();
	bool reversed = (shape->Remarks() & REM_REVERSED) != 0;
  *shape = LODShape(name, reversed);

	shape->InternalTransform(M4Identity);

	obj->GetShape()->ShadowChanged();
	return NOTHING;
}

bool EnabledWeaponPool();
void CampaignLoadWeaponPool(WeaponsInfo &pool);
void CampaignSaveWeaponPool(WeaponsInfo &pool);

GameValue ObjWeaponsFromPool( const GameState *state, GameValuePar oper1 )
{
	Object *obj = GetObject(oper1);
	Person *person = dyn_cast<Person>(obj);
	if (!person) return NOTHING;

	if (!EnabledWeaponPool()) return NOTHING;

	WeaponsInfo pool;
	CampaignLoadWeaponPool(pool);
	
	// move all magazines to pool
	for (int i=0; i<person->NMagazines(); i++) pool._magazinesPool.Add(person->GetMagazine(i));
	person->RemoveAllMagazines();

	// find primary and secondary weapon
	const WeaponType *primary = NULL;
	const WeaponType *secondary = NULL;
	int index = person->FindWeaponType(MaskSlotPrimary);
	if (index >= 0) primary = person->GetWeaponSystem(index);
	index = person->FindWeaponType(MaskSlotSecondary);
	if (index >= 0) secondary = person->GetWeaponSystem(index);
	
	int free = GetItemSlotsCount(person->GetType()->_weaponSlots);
	if (primary && primary->_muzzles.Size() > 0 && primary->_muzzles[0]->_magazines.Size() > 0)
	{
		const MagazineType *def = primary->_muzzles[0]->_magazines[0];
		int size = GetItemSlotsCount(def->_magazineType);
		int count = (free < 4 ? free : 4) / size;
		for (int i=0; i<count; i++)
		{
			Ref<Magazine> magazine = pool.RemovePoolMagazine(def);
			if (!magazine) break;
			person->AddMagazine(magazine);
			free -= size;
		}
	}
	if (secondary && secondary->_muzzles.Size() > 0 && secondary->_muzzles[0]->_magazines.Size() > 0)
	{
		const MagazineType *def = secondary->_muzzles[0]->_magazines[0];
		int size = GetItemSlotsCount(def->_magazineType);
		int count = free / size;
		for (int i=0; i<count; i++)
		{
			Ref<Magazine> magazine = pool.RemovePoolMagazine(def);
			if (!magazine) break;
			person->AddMagazine(magazine);
			free -= size;
		}
	}
	if (free > 0)
	{
		const MagazineType *def = MagazineTypes.New("HandGrenade");
		int size = GetItemSlotsCount(def->_magazineType);
		int count = free / size;
		for (int i=0; i<count; i++)
		{
			Ref<Magazine> magazine = pool.RemovePoolMagazine(def);
			if (!magazine) break;
			person->AddMagazine(magazine);
			free -= size;
		}
	}

	const WeaponType *handGun = NULL;
	index = person->FindWeaponType(MaskSlotHandGun);
	if (index >= 0) handGun = person->GetWeaponSystem(index);
	if (handGun && handGun->_muzzles.Size() > 0 && handGun->_muzzles[0]->_magazines.Size() > 0)
	{
		int free = GetHandGunItemSlotsCount(person->GetType()->_weaponSlots);

		const MagazineType *def = handGun->_muzzles[0]->_magazines[0];
		int size = GetHandGunItemSlotsCount(def->_magazineType);
		int count = free / size;
		for (int i=0; i<count; i++)
		{
			Ref<Magazine> magazine = pool.RemovePoolMagazine(def);
			if (!magazine) break;
			person->AddMagazine(magazine);
			free -= size;
		}
	}
	person->AutoReloadAll();
	
	CampaignSaveWeaponPool(pool);
	return NOTHING;
}

GameValue IsCheatsEnabled(const GameState *state)
{
#if _ENABLE_CHEATS
	return true;
#else
	return false;
#endif
}

GameValue VehIsEngineOn(const GameState *state, GameValuePar oper1)
{
	Object *obj = GetObject(oper1);
	EntityAI *veh = dyn_cast<EntityAI>(obj);
	if (!veh) return false;
	return veh->EngineIsOn();
}

GameValue VehEngineOn(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	Object *obj = GetObject(oper1);
	EntityAI *veh = dyn_cast<EntityAI>(obj);
	if (!veh) return NOTHING;
	bool on = oper2;
	if (on)
		veh->EngineOn();
	else
		veh->EngineOff();
	return NOTHING;
}

#if _ENABLE_VBS
static RString ConfigFullName(RString filename)
{
	if (filename.GetLength() == 0) return RString();
	
	// avoid path in filename
	if (strchr(filename, '\\')) return RString();
	if (strchr(filename, '/')) return RString();

	RString GetUserDirectory();
	return GetUserDirectory() + RString("Config\\") + filename;
}

static GameFileType GetFile(GameValuePar oper)
{
	Assert(oper.GetType() == GameFile);
	return static_cast<GameDataFile *>(oper.GetData())->GetFile();
}

GameValue ConfigNew(const GameState *state)
{
	GameFileType file;
	file.SetIndex(GFileEntries.Add(new ParamFile));
	file.readOnly = false;
	return GameValueExt(file);
}

GameValue ConfigLoad(const GameState *state, GameValuePar oper1)
{
	GameFileType file;

	RString fullname = ConfigFullName(oper1);
	if (fullname.GetLength() > 0)
	{
		// valid config name
		ParamFile *cfg = new ParamFile();
		if (cfg->Parse(fullname) == LSOK)
			file.SetIndex(GFileEntries.Add(cfg));
		else
			delete cfg;
	}
	return GameValueExt(file);
}

GameValue ConfigSave(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
// saveConfig disabled in VBS1 demo
#if !_VBS1_DEMO

	RString fullname = ConfigFullName(oper2);
	if (fullname.GetLength() == 0) return NOTHING;

	GameFileType file = GetFile(oper1);
	if (file.GetIndex() < 0) return NOTHING;
	if (file.readOnly) return NOTHING;
	ParamClass *cfg = GFileEntries[file.GetIndex()].cls;
	if (!cfg) return NOTHING;
	if (cfg->GetFile() != cfg) return NOTHING;

	void CreatePath(RString path);
	CreatePath(fullname);
	static_cast<ParamFile *>(cfg)->Save(fullname);

#endif

	return NOTHING;
}

GameValue ClassOpen(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	GameFileType file = GetFile(oper1);
	
	GameFileType result;
	result.readOnly = file.readOnly;

	if (file.GetIndex() < 0) return GameValueExt(result);
	ParamClass *cfg = GFileEntries[file.GetIndex()].cls;
	if (!cfg) return GameValueExt(result);

	RString name = oper2;
	ParamClass *cls = const_cast<ParamClass *>(cfg->GetClass(name));
	if (cls) result.SetIndex(GFileEntries.Add(cls));

	return GameValueExt(result);
}

GameValue ClassAdd(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	GameFileType file = GetFile(oper1);
	
	GameFileType result;
	result.readOnly = file.readOnly;

	if (file.GetIndex() < 0) return GameValueExt(result);
	if (file.readOnly) return GameValueExt(result);
	ParamClass *cfg = GFileEntries[file.GetIndex()].cls;
	if (!cfg) return GameValueExt(result);

	RString name = oper2;
	ParamClass *cls = cfg->AddClass(name);
	if (cls) result.SetIndex(GFileEntries.Add(cls));

	return GameValueExt(result);
}

static GameValue EntryToGameValue(const GameState *state, const IParamArrayValue &entry)
{
	if (entry.IsTextValue())
		return entry.GetValueRaw();
	else if (entry.IsFloatValue() || entry.IsIntValue())
		return entry.GetFloat();
	else if (entry.IsArrayValue())
	{
		GameValue value = state->CreateGameValue(GameArray);
		GameArrayType &array = value;
		array.Resize(entry.GetItemCount());
		for (int i=0; i<entry.GetItemCount(); i++)
		{
			array[i] = EntryToGameValue(state, *entry.GetItem(i));
		}
		return value;
	}
	else
		return RString();
}

static GameValue EntryToGameValue(const GameState *state, const ParamEntry &entry)
{
	if (entry.IsArray())
	{
		GameValue value = state->CreateGameValue(GameArray);
		GameArrayType &array = value;
		array.Resize(entry.GetSize());
		for (int i=0; i<entry.GetSize(); i++)
		{
			array[i] = EntryToGameValue(state, entry[i]);
		}
		return value;
	}
	else if (entry.IsClass())
		return RString();
	else if (entry.IsTextValue())
		return entry.GetValueRaw();
	else if (entry.IsFloatValue() || entry.IsIntValue())
		return (float)entry;
	else
		return RString();
}

GameValue ValueGet(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	GameFileType file = GetFile(oper1);
	if (file.GetIndex() < 0) return RString();
	ParamClass *cfg = GFileEntries[file.GetIndex()].cls;
	if (!cfg) return RString();

	RString name = oper2;
	const ParamEntry *entry = cfg->FindEntry(name);
	if (!entry) return RString();

	return EntryToGameValue(state, *entry);
}

void GameValueToEntry(IParamArrayValue &entry, GameValuePar value)
{
	switch (value.GetType())
	{
	case GameScalar:
		{
			float val = value;
			entry.AddValue(val);
		}
		break;
	case GameString:
		{
			RString val = value;
			entry.AddValue(val);
		}
		break;
	case GameArray:
		{
			const GameArrayType &array = value;
			IParamArrayValue *subEntry = entry.AddArrayValue();
			if (!subEntry) break;
			for (int i=0; i<array.Size(); i++)
				GameValueToEntry(*subEntry, array[i]);
		}
		break;
	default:
		ErrF("Unsupported type %d", value.GetType());
		break;
	}
}

void GameValueToEntry(ParamEntry &entry, GameValuePar value)
{
	switch (value.GetType())
	{
	case GameScalar:
		{
			float val = value;
			entry.AddValue(val);
		}
		break;
	case GameString:
		{
			RString val = value;
			entry.AddValue(val);
		}
		break;
	case GameArray:
		{
			const GameArrayType &array = value;
			IParamArrayValue *subEntry = entry.AddArrayValue();
			if (!subEntry) break;
			for (int i=0; i<array.Size(); i++)
				GameValueToEntry(*subEntry, array[i]);
		}
		break;
	default:
		ErrF("Unsupported type %d", value.GetType());
		break;
	}
}

void GameValueToEntry(ParamClass &cls, RString name, GameValuePar value)
{
	switch (value.GetType())
	{
	case GameBool:
	case GameScalar:
		{
			float val = value;
			cls.Add(name, val);
		}
		break;
	case GameString:
		{
			RString val = value;
			cls.Add(name, val);
		}
		break;
	case GameArray:
		{
			const GameArrayType &array = value;
			ParamEntry *entry = cls.AddArray(name);
			for (int i=0; i<array.Size(); i++)
				GameValueToEntry(*entry, array[i]);
		}
		break;
	default:
		ErrF("Unsupported type %d", value.GetType());
		break;
	}
}

GameValue ValueAdd(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	const GameArrayType &array = oper2;
	if (array.Size() != 2) return NOTHING;
	if (array[0].GetType() != GameString) return NOTHING;

	GameFileType file = GetFile(oper1);
	if (file.GetIndex() < 0) return NOTHING;
	if (file.readOnly) return NOTHING;
	ParamClass *cfg = GFileEntries[file.GetIndex()].cls;
	if (!cfg) return NOTHING;

	RString name = array[0];
	GameValueToEntry(*cfg, name, array[1]);
	return NOTHING;
}

GameValue ConfigListNames(const GameState *state)
{
	GameValue value = state->CreateGameValue(GameArray);
	GameArrayType &array = value;

	RString GetUserDirectory();
	RString mask = GetUserDirectory() + RString("Config\\*.*");

	_finddata_t info;
	long h = _findfirst(mask, &info);
	if (h != -1)
	{
		do
		{
			if ((info.attrib & _A_SUBDIR) == 0)
			{
				array.Add(RString(info.name));
			}
		}
		while (0==_findnext(h, &info));
		_findclose(h);
	}

	return value;
}

GameValue VBS_AddHeader(const GameState *state, GameValuePar oper1)
{
	GStats.VBSAddHeader(oper1);
	return NOTHING;
}

GameValue VBS_AddEvent(const GameState *state, GameValuePar oper1)
{
	GStats.VBSAddEvent(oper1);
	return NOTHING;
}

GameValue VBS_AddFooter(const GameState *state, GameValuePar oper1)
{
	GStats.VBSAddFooter(oper1);
	return NOTHING;
}

void FillUnitInfo(const GameState *state, GameArrayType &value, VBSUnitInfo &unit)
{
	value.Realloc(5);
	value.Resize(5);

	value[0] = unit.name;
	value[1] = unit.player;
	value[2] = state->CreateGameValue(GameArray);
	GameArrayType &pos = value[2];
	pos.Realloc(2);
	pos.Resize(2);
	pos[0] = unit.position.X();
	pos[1] = unit.position.Z();
	value[3] = GameValueExt(unit.side);
	value[4] = unit.type;
}

GameValue VBS_GetKills(const GameState *state, GameValuePar oper1)
{
	GameValue value = state->CreateGameValue(GameArray);
	GameArrayType &array = value;

	RString name = oper1;
	for (int i=0; i<GStats._mission._vbsEvents.Size(); i++)
	{
		VBSStatsEvent &event = GStats._mission._vbsEvents[i];
		if (event.type != VBSETKill) continue;
		if (stricmp(event.unit2.name, name) != 0) continue;

		int index = array.Add(state->CreateGameValue(GameArray));
		GameArrayType &row = array[index];
		row.Add(event.t.toFloat());
		index = row.Add(state->CreateGameValue(GameArray));
		FillUnitInfo(state, row[index], event.unit1);
		index = row.Add(state->CreateGameValue(GameArray));
		FillUnitInfo(state, row[index], event.unit2);
		row.Add(event.unit1.position.Distance(event.unit2.position));
	} 

	return value;
}

GameValue VBS_GetKilled(const GameState *state, GameValuePar oper1)
{
	GameValue value = state->CreateGameValue(GameArray);
	GameArrayType &array = value;

	RString name = oper1;
	for (int i=0; i<GStats._mission._vbsEvents.Size(); i++)
	{
		VBSStatsEvent &event = GStats._mission._vbsEvents[i];
		if (event.type != VBSETKill) continue;
		if (stricmp(event.unit1.name, name) != 0) continue;

		int index = array.Add(state->CreateGameValue(GameArray));
		GameArrayType &row = array[index];
		row.Add(event.t.toFloat());
		index = row.Add(state->CreateGameValue(GameArray));
		FillUnitInfo(state, row[index], event.unit1);
		index = row.Add(state->CreateGameValue(GameArray));
		FillUnitInfo(state, row[index], event.unit2);
		row.Add(event.unit1.position.Distance(event.unit2.position));
	} 

	return value;
}

GameValue VBS_GetInjuries(const GameState *state, GameValuePar oper1)
{
	GameValue value = state->CreateGameValue(GameArray);
	GameArrayType &array = value;

	RString name = oper1;
	for (int i=0; i<GStats._mission._vbsEvents.Size(); i++)
	{
		VBSStatsEvent &event = GStats._mission._vbsEvents[i];
		if (event.type != VBSETInjury) continue;
		if (stricmp(event.unit1.name, name) != 0) continue;

		int index = array.Add(state->CreateGameValue(GameArray));
		GameArrayType &row = array[index];
		row.Add(event.t.toFloat());
		index = row.Add(state->CreateGameValue(GameArray));
		FillUnitInfo(state, row[index], event.unit1);
		index = row.Add(state->CreateGameValue(GameArray));
		FillUnitInfo(state, row[index], event.unit2);
		row.Add(event.value);
		row.Add(event.text);
	} 

	return value;
}


#endif

#if _ENABLE_CHEATS
GameValue DBG_SwitchLandscape(const GameState *state, GameValuePar oper1)
{
	RString landscape = oper1;
	strcpy(Glob.header.worldname, landscape);
	GWorld->SwitchLandscape(GetWorldName(landscape));
	void InitWorld();
	InitWorld();
	return NOTHING;
}

GameValue DBG_Screenshot(const GameState *state, GameValuePar oper1)
{
	GEngine->Screenshot(oper1);
	return NOTHING;
}
#endif

/*!
\patch_internal 1.97 Date 04/01/2004 by Jirka
- Changed: mission editor functions enabled for VBS1
- Added: function getMarkerDir
- Added: function getMove
*/

#if _ENABLE_CHEATS || _ENABLE_VBS
inline bool CheckSize(const GameState *state, GameArrayType array, int size)
{
	if (array.Size() == size) return true;
	state->SetError(EvalDim, array.Size(), size);
	return false;
}

inline bool CheckType(const GameState *state, GameValuePar oper, GameType type)
{
	if (oper.GetType() == type) return true;
	state->TypeError(type, oper.GetType());
	return false;
}

GameValue SetDate(const GameState *state, GameValuePar oper1)
{
	const GameArrayType &array = oper1;
	if (!CheckSize(state, array, 5)) return NOTHING;
	for (int i=0; i<5; i++)
		if (!CheckType(state, array[i], GameScalar)) return NOTHING;

	GWorld->SetDate(toInt(array[0]), toInt(array[1]), toInt(array[2]), toInt(array[3]), toInt(array[4]));
	return NOTHING;
}

GameValue CenterCreate(const GameState *state, GameValuePar oper1)
{
	GameSideType side = GetSide(oper1);
	if (!GWorld->GetCenter(side))
	{
		AICenter *center = GWorld->CreateCenter(side);
		if (center)
			GetNetworkManager().CreateObject(center);
	}
	return oper1;
}

GameValue CenterDelete(const GameState *state, GameValuePar oper1)
{
	GameSideType side = GetSide(oper1);
	AICenter *center = GWorld->GetCenter(side);
	if (center && center->NGroups() == 0)
		GWorld->DeleteCenter(side);
	return NOTHING;
}

GameValue CenterSetFriend(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	const GameArrayType &array = oper2;
	if (!CheckSize(state, array, 2)) return NOTHING;
	if (!CheckType(state, array[0], GameSide)) return NOTHING;
	if (!CheckType(state, array[1], GameScalar)) return NOTHING;

	GameSideType side = GetSide(oper1);
	AICenter *center = GWorld->GetCenter(side);
	if (center)
	{
		GameSideType side = GetSide(array[0]);
		center->SetFriendship(side, array[1]);

		GetNetworkManager().UpdateObject(center);
	}
	return NOTHING;
}

GameValue GroupCreate(const GameState *state, GameValuePar oper1)
{
	GameSideType side = GetSide(oper1);
	AICenter *center = GWorld->GetCenter(side);
	if (center && center->NGroups() < MaxGroups)
	{
		Ref<AIGroup> group = new AIGroup();
		center->AddGroup(group);

		group->AddFirstWaypoint(VZero);

		Mission mis;
		mis._action = Mission::Arcade;
		center->SendMission(group, mis);

		GetNetworkManager().CreateObject(group);
		return GameValueExt(group);
	}
	return GROUP_NULL;
}

GameValue GroupDelete(const GameState *state, GameValuePar oper1)
{
	GameGroupType group = GetGroup(oper1);
	if (group && group->NUnits() == 0) group->RemoveFromCenter();
	return NOTHING;
}

GameValue MarkerCreate(const GameState *state, GameValuePar oper1)
{
	const GameArrayType &array = oper1;
	if (!CheckSize(state, array, 2)) return NOTHING;
	if (!CheckType(state, array[0], GameString)) return NOTHING;

	GameStringType name = array[0];
	Vector3 pos;
	if (!GetPos(state, pos, array[1])) return RString();

	for (int i=0; i<markersMap.Size(); i++)
		if (stricmp(markersMap[i].name, name) == 0) return RString();

	int index = markersMap.Add();
	ArcadeMarkerInfo &marker = markersMap[index];
	marker.name = name;
	marker.position = pos;

/*
	RefArray<NetworkObject> dummy;
	GetNetworkManager().MarkerCreate(CCGlobal, NULL, dummy, marker);
*/

	return array[0];
}

GameValue MarkerDelete(const GameState *state, GameValuePar oper1)
{
	GameStringType name = oper1;
	for (int i=0; i<markersMap.Size(); i++)
		if (stricmp(markersMap[i].name, name) == 0)
		{
			markersMap.Delete(i);
/*
			GetNetworkManager().MarkerDelete(name);
*/
			break;
		}
	return NOTHING;
}

GameValue MarkerSetText(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	GameStringType name = oper1;
	for (int i=0; i<markersMap.Size(); i++)
		if (stricmp(markersMap[i].name, name) == 0)
		{
			markersMap[i].text = oper2;
/*
			RefArray<NetworkObject> dummy;
			GetNetworkManager().MarkerCreate(CCGlobal, NULL, dummy, markersMap[i]);
*/
			break;
		}
	return NOTHING;
}

GameValue MarkerSetShape(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	GameStringType name = oper1;
	for (int i=0; i<markersMap.Size(); i++)
		if (stricmp(markersMap[i].name, name) == 0)
		{
			GameStringType type = oper2;
			markersMap[i].markerType = GetEnumValue<MarkerType>((const char *)type);
/*
			RefArray<NetworkObject> dummy;
			GetNetworkManager().MarkerCreate(CCGlobal, NULL, dummy, markersMap[i]);
*/
			break;
		}
	return NOTHING;
}

GameValue MarkerSetBrush(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	GameStringType name = oper1;
	for (int i=0; i<markersMap.Size(); i++)
		if (stricmp(markersMap[i].name, name) == 0)
		{
			markersMap[i].fillName = oper2;
			markersMap[i].OnFillChanged();
/*
			RefArray<NetworkObject> dummy;
			GetNetworkManager().MarkerCreate(CCGlobal, NULL, dummy, markersMap[i]);
*/
			break;
		}
	return NOTHING;
}

GameValue MarkerGetDir(const GameState *state, GameValuePar oper1)
{
	GameStringType name = oper1;
	for (int i=0; i<markersMap.Size(); i++)
		if (stricmp(markersMap[i].name, name) == 0)
		{
			return markersMap[i].angle;
		}
	return 0.0f;
}

GameValue MarkerSetDir(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	GameStringType name = oper1;
	for (int i=0; i<markersMap.Size(); i++)
		if (stricmp(markersMap[i].name, name) == 0)
		{
			markersMap[i].angle = oper2;
/*
			RefArray<NetworkObject> dummy;
			GetNetworkManager().MarkerCreate(CCGlobal, NULL, dummy, markersMap[i]);
*/
			break;
		}
	return NOTHING;
}

GameValue GuardedPointCreate(const GameState *state, GameValuePar oper1)
{
	const GameArrayType &array = oper1;
	if (!CheckSize(state, array, 4)) return NOTHING;
	if (!CheckType(state, array[0], GameSide)) return NOTHING;
	if (!CheckType(state, array[2], GameScalar)) return NOTHING;
	if (!CheckType(state, array[3], GameObject)) return NOTHING;

	Vector3 pos;
	if (!GetPos(state, pos, array[1])) return NOTHING;
	pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);

	AICenter *center = GWorld->GetCenter(GetSide(array[0]));
	if (!center) return NOTHING;

	int idStatic = toInt(array[2]);
	if (idStatic >= 0)
	{
		Object *obj = GLandscape->FindObject(idStatic);
		if (obj) pos = obj->Position();
	}
	else
	{
		EntityAI *veh = dyn_cast<EntityAI>(GetObject(array[3]));
		if (veh) pos = veh->Position();
	}

	center->AddGuardedPoint(pos);
	return NOTHING;
}

GameValue TriggerCreate(const GameState *state, GameValuePar oper1)
{
	const GameArrayType &array = oper1;
	if (!CheckSize(state, array, 2)) return OBJECT_NULL;
	if (!CheckType(state, array[0], GameString)) return OBJECT_NULL;

	GameStringType type = array[0];
	Vector3 pos;
	if (!GetPos(state, pos, array[1])) return OBJECT_NULL;
	pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);

	Ref<Vehicle> vehicle = NewNonAIVehicle(type);
	if (!vehicle) return OBJECT_NULL;

	// position is on sea level now
	if (vehicle->GetShape())
	{
		pos += vehicle->GetShape()->BoundingCenter();
	}

	// TODO: random azimut
	Vector3 normal = VUp;
	Matrix4 transform;
	transform.SetUpAndDirection(normal, Vector3(0,0,1));
	transform.SetPosition(pos);
	vehicle->SetTransform(transform);

	// Add into World
	GWorld->AddBuilding(vehicle);
	if (GWorld->GetMode() == GModeNetware)
		GetNetworkManager().CreateVehicle(vehicle, VLTBuilding, "", -1);

	sensorsMap.Add(vehicle.GetRef());

	return GameValueExt(vehicle);
}

inline Detector *GetDetector(GameValuePar oper)
{
	Object *obj = GetObject(oper);
	return dyn_cast<Detector>(obj);
}

GameValue TriggerSetArea(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	const GameArrayType &array = oper2;
	if (!CheckSize(state, array, 4)) return NOTHING;
	if (!CheckType(state, array[0], GameScalar)) return NOTHING;
	if (!CheckType(state, array[1], GameScalar)) return NOTHING;
	if (!CheckType(state, array[2], GameScalar)) return NOTHING;
	if (!CheckType(state, array[3], GameBool)) return NOTHING;

	Detector *det = GetDetector(oper1);
	if (det) det->SetArea(array[0], array[1], array[2], array[3]);

	return NOTHING;
}

GameValue TriggerSetActivation(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	const GameArrayType &array = oper2;
	if (!CheckSize(state, array, 3)) return NOTHING;
	if (!CheckType(state, array[0], GameString)) return NOTHING;
	if (!CheckType(state, array[1], GameString)) return NOTHING;
	if (!CheckType(state, array[2], GameBool)) return NOTHING;

	Detector *det = GetDetector(oper1);
	if (det)
	{
		RString by = array[0];
		RString type = array[1];
		det->SetActivation
		(
			GetEnumValue<ArcadeSensorActivation>((const char *)by),
			GetEnumValue<ArcadeSensorActivationType>((const char *)type),
			array[2]
		);
	}

	return NOTHING;
}

GameValue TriggerSetType(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	Detector *det = GetDetector(oper1);
	if (det)
	{
		RString type = oper2;
		det->SetTriggerType(GetEnumValue<ArcadeSensorType>((const char *)type));
	}

	return NOTHING;
}

GameValue TriggerSetTimeout(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	const GameArrayType &array = oper2;
	if (!CheckSize(state, array, 4)) return NOTHING;
	if (!CheckType(state, array[0], GameScalar)) return NOTHING;
	if (!CheckType(state, array[1], GameScalar)) return NOTHING;
	if (!CheckType(state, array[2], GameScalar)) return NOTHING;
	if (!CheckType(state, array[3], GameBool)) return NOTHING;

	Detector *det = GetDetector(oper1);
	if (det)
		det->SetTimeout(array[0], array[1], array[2], array[3]);

	return NOTHING;
}

GameValue TriggerSetText(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	Detector *det = GetDetector(oper1);
	if (det)
		det->SetText(oper2);

	return NOTHING;
}

GameValue TriggerSetStatements(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	const GameArrayType &array = oper2;
	if (!CheckSize(state, array, 3)) return NOTHING;
	if (!CheckType(state, array[0], GameString)) return NOTHING;
	if (!CheckType(state, array[1], GameString)) return NOTHING;
	if (!CheckType(state, array[2], GameString)) return NOTHING;

	Detector *det = GetDetector(oper1);
	if (det)
		det->SetStatements(array[0], array[1], array[2]);

	return NOTHING;
}

GameValue TriggerAttachObject(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	Detector *det = GetDetector(oper1);
	if (det)
		det->AssignStatic(toInt(oper2));

	return NOTHING;
}

GameValue TriggerAttachVehicle(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	const GameArrayType &array = oper2;
	if (array.Size() > 1)
	{
		state->SetError(EvalDim, array.Size(), 1);
		return NOTHING;
	}
	EntityAI *vehicle = NULL;
	if (array.Size() == 1)
	{
		if (!CheckType(state, array[0], GameObject)) return NOTHING;
		Object *obj = GetObject(array[0]);
		vehicle = dyn_cast<EntityAI>(obj);
	}

	Detector *det = GetDetector(oper1);
	if (det)
		det->AttachVehicle(vehicle);

	return NOTHING;
}

static ArcadeWaypointInfo *GetWaypoint(const GameState *state, GameValuePar oper, AIGroup **group = NULL, int *index = NULL)
{
	const GameArrayType &array = oper;
	if (!CheckSize(state, array, 2)) return NULL;
	if (!CheckType(state, array[0], GameGroup)) return NULL;
	if (!CheckType(state, array[1], GameScalar)) return NULL;

	AIGroup *grp = GetGroup(array[0]);
	if (!grp) return NULL;

	int i = toInt(array[1]);
	if (i < 0) return NULL;
	if (i >= grp->NWaypoints()) return NULL;

	if (group) *group = grp;
	if (index) *index = i;

	return &grp->GetWaypoint(i);
}

GameValue TriggerSynchronize(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	// check array structure
	const GameArrayType &array = oper2;
	for (int i=0; i<array.Size(); i++)
	{
		if (!CheckType(state, array[i], GameArray)) return NOTHING;
		
		const GameArrayType &subarray = array[i];
		if (!CheckSize(state, subarray, 2)) return NOTHING;
		if (!CheckType(state, subarray[0], GameGroup)) return NOTHING;
		if (!CheckType(state, subarray[1], GameScalar)) return NOTHING;
	}
	
	Detector *det = GetDetector(oper1);
	if (!det) return NOTHING;

	// TODO: optimize

	// delete old synchronizations
	for (int i=0; i<synchronized.Size(); i++)
	{
		SynchronizedItem &sync = synchronized[i];
		DoAssert(sync.sensors.Size() <= 1);
		if (sync.sensors.Size() == 1 && sync.sensors[0].sensor == det)
		{
			sync.sensors.Clear();
			DoAssert(sync.groups.Size() == 1);
			AIGroup *grp = sync.groups[0].group;
			if (grp)
			{
				// remove synchronization from waypoint
				for (int j=0; j<grp->NWaypoints(); j++)
				{
					ArcadeWaypointInfo &wp = grp->GetWaypoint(j);
					bool found = false;
					for (int s=0; s<wp.synchronizations.Size(); s++)
						if (wp.synchronizations[s] == i)
						{
							wp.synchronizations.Delete(s);
							found = true;
							break;
						}
					if (found) break;
				}
			}
			sync.groups.Clear();
			sync.sensors.Clear();
		}
	}

	// create new synchronizations
	AutoArray<int> sync;
	
	for (int i=0; i<array.Size(); i++)
	{
		const GameArrayType &subarray = array[i];

		AIGroup *grp = GetGroup(subarray[0]);
		if (!grp) continue;

		int index = toInt(subarray[1]);
		if (index < 0) continue;
		if (index >= grp->NWaypoints()) continue;

		int s = synchronized.Add();
		synchronized[s].Add(det);
		synchronized[s].Add(grp);

		sync.Add(s);

		ArcadeWaypointInfo &wp = grp->GetWaypoint(index);
		wp.synchronizations.Add(s);
	}

	sync.Compact();
	det->SetSynchronizations(sync);

	return NOTHING;
}

GameValue WaypointSynchronize(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	// check array structure
	const GameArrayType &array1 = oper1;
	if (!CheckSize(state, array1, 2)) return NOTHING;
	if (!CheckType(state, array1[0], GameGroup)) return NOTHING;
	if (!CheckType(state, array1[1], GameScalar)) return NOTHING;

	// check array structure
	const GameArrayType &array2 = oper2;
	for (int i=0; i<array2.Size(); i++)
	{
		if (!CheckType(state, array2[i], GameArray)) return NOTHING;
		
		const GameArrayType &subarray = array2[i];
		if (!CheckSize(state, subarray, 2)) return NOTHING;
		if (!CheckType(state, subarray[0], GameGroup)) return NOTHING;
		if (!CheckType(state, subarray[1], GameScalar)) return NOTHING;
	}
	
	AIGroup *grp1 = GetGroup(array1[0]);
	if (!grp1) return NOTHING;
	int index1 = toInt(array1[1]);
	if (index1 < 0) return NOTHING;
	if (index1 >= grp1->NWaypoints()) return NOTHING;
	ArcadeWaypointInfo &wp1 = grp1->GetWaypoint(index1);

	// TODO: optimize

	// delete old synchronizations
	for (int i=0; i<wp1.synchronizations.Size();)
	{
		int sIndex = wp1.synchronizations[i];
		SynchronizedItem &sync = synchronized[sIndex];
		DoAssert(sync.sensors.Size() <= 1);
		if (sync.sensors.Size() == 1)
		{
			// synchronization waypoint - trigger
			DoAssert(sync.groups.Size() == 1);
			DoAssert(sync.groups[0].group == grp1)
			i++;
			continue;
		}

		// synchronization waypoint - waypoint
		DoAssert(sync.groups.Size() == 2);
		if (sync.groups[1].group != grp1)
		{
			// maintained by second waypoint
			DoAssert(sync.groups[0].group == grp1);
			i++;
			continue;
		}

		// remove from first waypoint
		wp1.synchronizations.Delete(i);

		// remove from second waypoint
		AIGroup *grp2 = sync.groups[0].group;
		if (grp2)
		{
			// remove synchronization from waypoint
			for (int j=0; j<grp2->NWaypoints(); j++)
			{
				ArcadeWaypointInfo &wp2 = grp2->GetWaypoint(j);
				bool found = false;
				for (int s=0; s<wp2.synchronizations.Size(); s++)
					if (wp2.synchronizations[s] == sIndex)
					{
						wp2.synchronizations.Delete(s);
						found = true;
						break;
					}
				if (found) break;
			}
		}

		// remove from list of synchronizations
		sync.groups.Clear();
	}

	// create new synchronizations
	for (int i=0; i<array2.Size(); i++)
	{
		const GameArrayType &subarray = array2[i];

		AIGroup *grp2 = GetGroup(subarray[0]);
		if (!grp2) continue;

		int index2 = toInt(subarray[1]);
		if (index2 < 0) continue;
		if (index2 >= grp2->NWaypoints()) continue;

		ArcadeWaypointInfo &wp2 = grp2->GetWaypoint(index2);

		int s = synchronized.Add();
		synchronized[s].Add(grp2);
		synchronized[s].Add(grp1);	// must be in group[1]

		wp1.synchronizations.Add(s);
		wp2.synchronizations.Add(s);
	}

	return NOTHING;
}

static ArcadeEffects *GetEffects(const GameState *state, GameValuePar oper)
{
	if (oper.GetType() == GameObject)
	{
		Detector *det = GetDetector(oper);
		if (det) return &det->GetEffects();
		return NULL;
	}
	else if (oper.GetType() == GameArray)
	{
		ArcadeWaypointInfo *wp = GetWaypoint(state, oper);
		if (wp) return &wp->effects;
		return NULL;
	}

	state->TypeError(GameObject | GameArray, oper.GetType());
	return NULL;
}

GameValue EffectSetCondition(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	ArcadeEffects *effects = GetEffects(state, oper1);
	if (effects)
		effects->condition = oper2;
	return NOTHING;
}

GameValue EffectSetCamera(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	const GameArrayType &array = oper2;
	if (!CheckSize(state, array, 2)) return NOTHING;
	if (!CheckType(state, array[0], GameString)) return NOTHING;
	if (!CheckType(state, array[1], GameString)) return NOTHING;

	ArcadeEffects *effects = GetEffects(state, oper1);
	if (effects)
	{
		effects->cameraEffect = array[0];
		RString pos = array[1];
		effects->cameraPosition = GetEnumValue<CamEffectPosition>((const char *)pos);
	}
	return NOTHING;
}

GameValue EffectSetSound(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	const GameArrayType &array = oper2;
	if (!CheckSize(state, array, 4)) return NOTHING;
	if (!CheckType(state, array[0], GameString)) return NOTHING;
	if (!CheckType(state, array[1], GameString)) return NOTHING;
	if (!CheckType(state, array[2], GameString)) return NOTHING;
	if (!CheckType(state, array[3], GameString)) return NOTHING;

	ArcadeEffects *effects = GetEffects(state, oper1);
	if (effects)
	{
		effects->sound = array[0];
		effects->voice = array[1];
		effects->soundEnv = array[2];
		effects->soundDet = array[3];
	}
	return NOTHING;
}

GameValue EffectSetMusic(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	ArcadeEffects *effects = GetEffects(state, oper1);
	if (effects)
		effects->track = oper2;
	return NOTHING;
}

GameValue EffectSetTitle(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	const GameArrayType &array = oper2;
	if (!CheckSize(state, array, 3)) return NOTHING;
	if (!CheckType(state, array[0], GameString)) return NOTHING;
	if (!CheckType(state, array[1], GameString)) return NOTHING;
	if (!CheckType(state, array[2], GameString)) return NOTHING;

	ArcadeEffects *effects = GetEffects(state, oper1);
	if (effects)
	{
		RString type = array[0];
		effects->titleType = GetEnumValue<enum TitleType>((const char *)type);
		RString name = array[1];
		effects->titleEffect = GetEnumValue<TitEffectName>((const char *)name);
		effects->title = array[2];
	}
	return NOTHING;
}

GameValue WaypointAdd(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	GameValue retValue = state->CreateGameValue(GameArray);
	GameArrayType &retArray = retValue;
	retArray.Realloc(2);
	retArray.Resize(2);
	retArray[0] = GROUP_NULL;
	retArray[1] = -1.0f;

	AIGroup *group = GetGroup(oper1);
	if (!group) return retValue;

	const GameArrayType &array = oper2;
	if (!CheckSize(state, array, 2)) return retValue;

	Vector3 pos;
	if (!GetPos(state, pos, array[0])) return retValue;

	if (!CheckType(state, array[1], GameScalar)) return retValue;
	float placement = array[1];

	if (placement > 0)
	{
		Vector3 FindWaypointPosition(Vector3Par center, float radius);
		// set random position
		pos = FindWaypointPosition(pos, placement);
	}
	
	int index = group->AddWaypoint();
	ArcadeWaypointInfo &wp = group->GetWaypoint(index);
	wp.position = pos;
	wp.placement = placement;

	retArray[0] = GameValueExt(group);
	retArray[1] = (float)index;

	GetNetworkManager().UpdateObject(group);

	return retArray;
}

GameValue WaypointDelete(const GameState *state, GameValuePar oper1)
{
	const GameArrayType &array = oper1;
	if (!CheckSize(state, array, 2)) return NOTHING;
	if (!CheckType(state, array[0], GameGroup)) return NOTHING;
	if (!CheckType(state, array[1], GameScalar)) return NOTHING;

	AIGroup *grp = GetGroup(array[0]);
	if (!grp) return NOTHING;

	int index = toInt(array[1]);
	if (index < 0) return NOTHING;
	if (index >= grp->NWaypoints()) return NOTHING;

	grp->DeleteWaypoint(index);

	GetNetworkManager().UpdateObject(grp);

	return NOTHING;
}

GameValue WaypointSetPosition(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	AIGroup *group = NULL;
	ArcadeWaypointInfo *wp = GetWaypoint(state, oper1, &group);
	if (!wp) return NOTHING;

	const GameArrayType &array = oper2;
	if (!CheckSize(state, array, 2)) return NOTHING;

	Vector3 pos;
	if (!GetPos(state, pos, array[0])) return NOTHING;

	if (!CheckType(state, array[1], GameScalar)) return NOTHING;
	float placement = array[1];

	if (placement > 0)
	{
		Vector3 FindWaypointPosition(Vector3Par center, float radius);
		// set random position
		pos = FindWaypointPosition(pos, placement);
	}

	wp->position = pos;
	wp->placement = placement;

	GetNetworkManager().UpdateObject(group);

	return NOTHING;
}

GameValue WaypointSetType(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	AIGroup *group = NULL;
	ArcadeWaypointInfo *wp = GetWaypoint(state, oper1, &group);
	if (!wp) return NOTHING;

	RString type = oper2;
	wp->type = GetEnumValue<ArcadeWaypointType>((const char *)type);

	void OnWaypointsUpdated(AIGroupContext *context);
	AIGroupContext context(group);
	if (group->GetCurrent())
	{
		context._task = group->GetCurrent()->_task;
		context._fsm = group->GetCurrent()->_fsm;

		OnWaypointsUpdated(&context);
	}

	GetNetworkManager().UpdateObject(group);

	return NOTHING;
}

GameValue WaypointAttachVehicle(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	AIGroup *group = NULL;
	ArcadeWaypointInfo *wp = GetWaypoint(state, oper1, &group);
	if (!wp) return NOTHING;

	const GameArrayType &array = oper2;
	if (array.Size() > 1)
	{
		state->SetError(EvalDim, array.Size(), 1);
		return NOTHING;
	}
	int id = -1;
	if (array.Size() == 1)
	{
		if (!CheckType(state, array[0], GameObject)) return NOTHING;
		Object *obj = GetObject(array[0]);

		if (obj) for (int i=0; i<vehiclesMap.Size(); i++)
			if (obj == vehiclesMap[i])
			{
				id = i;
				break;
			}
	}

	wp->id = id;

	GetNetworkManager().UpdateObject(group);

	return NOTHING;
}

GameValue WaypointAttachObject(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	AIGroup *group = NULL;
	ArcadeWaypointInfo *wp = GetWaypoint(state, oper1, &group);
	if (!wp) return NOTHING;

	wp->idStatic = toInt(oper2);

	GetNetworkManager().UpdateObject(group);

	return NOTHING;
}

GameValue WaypointSetHousePos(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	AIGroup *group = NULL;
	ArcadeWaypointInfo *wp = GetWaypoint(state, oper1, &group);
	if (!wp) return NOTHING;

	wp->housePos = toInt(oper2);

	GetNetworkManager().UpdateObject(group);

	return NOTHING;
}

GameValue WaypointSetCombatMode(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	AIGroup *group = NULL;
	ArcadeWaypointInfo *wp = GetWaypoint(state, oper1, &group);
	if (!wp) return NOTHING;

	RString str = oper2;
	wp->combatMode = GetEnumValue<AI::Semaphore>((const char *)str);

	GetNetworkManager().UpdateObject(group);

	return NOTHING;
}

GameValue WaypointSetFormation(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	AIGroup *group = NULL;
	ArcadeWaypointInfo *wp = GetWaypoint(state, oper1, &group);
	if (!wp) return NOTHING;

	RString str = oper2;
	wp->formation = GetEnumValue<AI::Formation>((const char *)str);
	
	GetNetworkManager().UpdateObject(group);

	return NOTHING;
}

GameValue WaypointSetSpeed(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	AIGroup *group = NULL;
	ArcadeWaypointInfo *wp = GetWaypoint(state, oper1, &group);
	if (!wp) return NOTHING;

	RString str = oper2;
	wp->speed = GetEnumValue<SpeedMode>((const char *)str);
	
	GetNetworkManager().UpdateObject(group);

	return NOTHING;
}

GameValue WaypointSetBehaviour(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	AIGroup *group = NULL;
	ArcadeWaypointInfo *wp = GetWaypoint(state, oper1, &group);
	if (!wp) return NOTHING;

	RString str = oper2;
	wp->combat = GetEnumValue<CombatMode>((const char *)str);
	
	GetNetworkManager().UpdateObject(group);

	return NOTHING;
}

GameValue WaypointSetDescription(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	AIGroup *group = NULL;
	ArcadeWaypointInfo *wp = GetWaypoint(state, oper1, &group);
	if (!wp) return NOTHING;

	wp->description = oper2;
	
	GetNetworkManager().UpdateObject(group);

	return NOTHING;
}

GameValue WaypointSetStatements(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	AIGroup *group = NULL;
	ArcadeWaypointInfo *wp = GetWaypoint(state, oper1, &group);
	if (!wp) return NOTHING;

	const GameArrayType &array = oper2;
	if (!CheckSize(state, array, 2)) return NOTHING;
	if (!CheckType(state, array[0], GameString)) return NOTHING;
	if (!CheckType(state, array[1], GameString)) return NOTHING;

	wp->expCond = array[0];
	wp->expActiv = array[1];
	
	GetNetworkManager().UpdateObject(group);

	return NOTHING;
}

GameValue WaypointSetScript(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	AIGroup *group = NULL;
	ArcadeWaypointInfo *wp = GetWaypoint(state, oper1, &group);
	if (!wp) return NOTHING;

	wp->script = oper2;
	
	GetNetworkManager().UpdateObject(group);

	return NOTHING;
}

GameValue WaypointSetTimeout(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	AIGroup *group = NULL;
	ArcadeWaypointInfo *wp = GetWaypoint(state, oper1, &group);
	if (!wp) return NOTHING;

	const GameArrayType &array = oper2;
	if (!CheckSize(state, array, 3)) return NOTHING;
	if (!CheckType(state, array[0], GameScalar)) return NOTHING;
	if (!CheckType(state, array[1], GameScalar)) return NOTHING;
	if (!CheckType(state, array[2], GameScalar)) return NOTHING;

	wp->timeoutMin = array[0];
	wp->timeoutMid = array[1];
	wp->timeoutMax = array[2];
	
	GetNetworkManager().UpdateObject(group);

	return NOTHING;
}

GameValue WaypointShow(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	AIGroup *group = NULL;
	ArcadeWaypointInfo *wp = GetWaypoint(state, oper1, &group);
	if (!wp) return NOTHING;

	RString str = oper2;
	wp->showWP = GetEnumValue<AWPShow>((const char *)str);
	
	GetNetworkManager().UpdateObject(group);

	return NOTHING;
}

GameValue ObjGetMove(const GameState *state, GameValuePar oper1)
{
	EntityAI *obj = dyn_cast<EntityAI>(GetObject(oper1));
	if (!obj) return RString();
	return obj->GetCurrentMove();
}

#endif

GameValue MissionName(const GameState *state)
{
	const MissionHeader *header = GetNetworkManager().GetMissionHeader();
	if (!header) return Glob.header.filenameReal;
	if (header->name.GetLength() == 0) return Glob.header.filenameReal;
	return header->name;
/*
	if (header->fileName.GetLength() == 0) return Glob.header.filenameReal;
	const char *ext = strrchr(header->fileName, '.');
	if (!ext) return header->fileName;
	return header->fileName.Substring(0, ext - (const char *)header->fileName);
*/
}

GameValue WorldName(const GameState *state)
{
	return Glob.header.worldname;
}

GameValue MissionStart(const GameState *state)
{
	GameValue value = state->CreateGameValue(GameArray);
	GameArrayType &array = value;
	array.Resize(6);
	
	tm *t = localtime(&GStats._mission._time);
	array[0] = (float)(t->tm_year + 1900);
	array[1] = (float)(t->tm_mon + 1);
	array[2] = (float)t->tm_mday;
	array[3] = (float)t->tm_hour;
	array[4] = (float)t->tm_min;
	array[5] = (float)t->tm_sec;

	return value;
}

GameValue IsServer(const GameState *state)
{
	return GetNetworkManager().IsServer();
}

int PlayersNumber(TargetSide side)
{
	int count = 0;
	for (int i=0; i<GetNetworkManager().NPlayerRoles(); i++)
	{
		const PlayerRole *role = GetNetworkManager().GetPlayerRole(i);
		if (!role) continue;
		if (role->side != side) continue;

		if (role->player != NO_PLAYER) count++;
	}
	return count;
}

GameValue PlayersNumber(const GameState *state, GameValuePar oper1)
{
	return (float)PlayersNumber(GetSide(oper1));
}

#define TABLE_COMMAND(xxx,Xxx,RightType) \
	GameOperator(GameNothing,"command" #xxx,function,VehCommand##Xxx,GameObjectOrArray,RightType),\
	GameOperator(GameNothing,"do" #xxx,function,VehDo##Xxx,GameObjectOrArray,RightType),

#define TABLE_COMMAND_S(xxx,Xxx) \
	GameFunction(GameNothing,"command" #xxx,VehCommand##Xxx,GameObjectOrArray),\
	GameFunction(GameNothing,"do" #xxx,VehDo##Xxx,GameObjectOrArray),

bool OpenScript(QIFStreamB &in, RString name);

GameValue ScriptExecute(const GameState *state, GameValuePar oper1, GameValuePar oper2);
GameValue ScriptExit(const GameState *state);
GameValue ScriptGoto(const GameState *state, GameValuePar oper1);

GameValue PoolAddWeapon(const GameState *state, GameValuePar oper1);
GameValue PoolAddMagazine(const GameState *state, GameValuePar oper1);
GameValue PoolGetWeapons(const GameState *state, GameValuePar oper1);
GameValue PoolSetWeapons(const GameState *state, GameValuePar oper1);
GameValue PoolClearWeapons(const GameState *state);
GameValue PoolClearMagazines(const GameState *state);
GameValue PoolQueryWeapons(const GameState *state, GameValuePar oper1);
GameValue PoolQueryMagazines(const GameState *state, GameValuePar oper1);

GameValue ShowCinemaBorder(const GameState *state, GameValuePar oper1);

GameValue ParticleDrop(const GameState *state, GameValuePar oper1);

GameValue StringLoad(const GameState *state, GameValuePar oper1)
{
	QIFStreamB in;
	if (OpenScript(in,oper1))
	{
		return RString(in.act(),in.rest());
	}
	return RString();
}

class FilePreprocessor : public Preproc
{
protected:
	QIStream *OnEnterInclude(const char *filename)
	{
		if (!QIFStreamB::FileExist(filename))
			return NULL;
		QIFStreamB *stream = new QIFStreamB();
		stream->AutoOpen(filename);
		return stream;
	}
	void OnExitInclude(QIStream *stream)
	{
		if (stream) delete (QIFStreamB *)stream;
	}
};

RString FindScript(RString name);

GameValue StringPreprocess(const GameState *state, GameValuePar oper1)
{
	RString filename = FindScript(oper1);
	FilePreprocessor preproc;
	QOStream processed;
	if (preproc.Process(&processed,filename))
	{
		return RString(processed.str(),processed.pcount());
	}
	return RString();
}

static const GameNular ExtNular[]=
{
	GameNular(GameObject,"player",Player),
	GameNular(GameObject,"objNull",ObjNull),
	GameNular(GameGroup,"grpNull",GrpNull),
	GameNular(GameScalar,"time",GameTime),
	GameNular(GameScalar,"dayTime",DayTime),
	GameNular(GameBool,"cadetMode",CadetMode),
	GameNular(GameScalar,"benchmark",Benchmark),
	GameNular(GameScalar,"accTime",GetAcceleratedTime),
	
	GameNular(GameBool,"shownMap",IsMapShown),
	GameNular(GameBool,"shownWatch",IsWatchShown),
	GameNular(GameBool,"shownCompass",IsCompassShown),
	GameNular(GameBool,"shownRadio",IsWalkieTalkieShown),
	GameNular(GameBool,"shownPad",IsNotepadShown),
	GameNular(GameBool,"shownWarrant",IsWarrantShown),
	GameNular(GameBool,"shownGps",IsGPSShown),

	GameNular(GameSide,"west",SideWest),
	GameNular(GameSide,"east",SideEast),
	GameNular(GameSide,"civilian",SideCivilian),
	GameNular(GameSide,"resistance",SideResistance),
	GameNular(GameSide,"sideLogic",SideLogic),
	GameNular(GameSide,"sideEnemy",SideEnemy),
	GameNular(GameSide,"sideFriendly",SideFriendly),

	GameNular(GameNothing,"saveGame",SaveGame),
	GameNular(GameNothing,"exit",ScriptExit),

	GameNular(GameNothing,"enableEndDialog",EnableEndDialog),
	GameNular(GameNothing,"forceEnd",ForceEnd),
	GameNular(GameScalar,"musicVolume",GetMusicVolume),
	GameNular(GameScalar,"soundVolume",GetSoundVolume),

	GameNular(GameNothing,"mapAnimClear",MapAnimClear),
	GameNular(GameNothing,"mapAnimCommit",MapAnimCommit),
	GameNular(GameBool,"mapAnimDone",MapAnimDone),

#if _ENABLE_DATADISC
	GameNular(GameNothing,"clearWeaponPool",PoolClearWeapons),
	GameNular(GameNothing,"clearMagazinePool",PoolClearMagazines),

	GameNular(GameNothing,"cheatsEnabled",IsCheatsEnabled),

	GameNular(GameBool,"dialog",IsDialog),
#endif

	GameNular(GameObject,"cameraOn",CameraOn),

#if _ENABLE_VBS
	GameNular(GameFile,"newConfig",ConfigNew),
	GameNular(GameArray,"listConfigNames",ConfigListNames),
#endif
	GameNular(GameString,"missionName",MissionName),
	GameNular(GameArray,"missionStart",MissionStart),
	GameNular(GameString,"getWorld",WorldName),
	GameNular(GameBool,"isServer",IsServer),
};


static const GameFunction ExtUnary[]=
{
	GameFunction(GameBool,"isNull",ObjIsNull,GameObject),
	GameFunction(GameBool,"isNull",GrpIsNull,GameGroup),
	GameFunction(GameBool,"alive",ObjAlive,GameObject),
	GameFunction(GameBool,"local",ObjIsLocal,GameObject),
	GameFunction(GameBool,"requiredVersion",RequiredVersion,GameString),
	GameFunction(GameArray,"getPos",ObjGetPos,GameObject),
	GameFunction(GameArray,"position",ObjGetPos,GameObject),
	GameFunction(GameArray,"getPosASL",ObjGetPosASL,GameObject),
	GameFunction(GameScalar,"getDir",ObjGetDir,GameObject),
	GameFunction(GameScalar,"direction",ObjGetDir,GameObject),
	GameFunction(GameArray,"velocity",ObjGetVelocity,GameObject),
	GameFunction(GameScalar,"getDammage",ObjGetDammage,GameObject),
	GameFunction(GameScalar,"damage",ObjGetDammage,GameObject),
	GameFunction(GameScalar,"speed",ObjGetSpeed,GameObject),
	GameFunction(GameArray,"getMarkerPos",MarkerGetPos,GameString),
	GameFunction(GameArray,"markerPos",MarkerGetPos,GameString),
	GameFunction(GameString,"getMarkerType",MarkerGetType,GameString),
	GameFunction(GameString,"markerType",MarkerGetType,GameString),
	GameFunction(GameArray,"getMarkerSize",MarkerGetSize,GameString),
	GameFunction(GameArray,"markerSize",MarkerGetSize,GameString),
	GameFunction(GameString,"getMarkerColor",MarkerGetColor,GameString),
	GameFunction(GameString,"markerColor",MarkerGetColor,GameString),
	GameFunction(GameArray,"getWPPos",WpGetPos,GameArray),
	GameFunction(GameArray,"waypointPosition",WpGetPos,GameArray),
	GameFunction(GameObject,"nearestBuilding",ObjGetNearestBuilding,GameObject),
	GameFunction(GameObject,"nearestObject",GetNearestObject,GameArray),
	GameFunction(GameBool,"canMove",ObjCanMove,GameObject),
	GameFunction(GameBool,"canFire",ObjCanFire,GameObject),
	GameFunction(GameBool,"canStand",ObjCanStand,GameObject),
	GameFunction(GameScalar,"handsHit",ObjHandsHit,GameObject),
	GameFunction(GameBool,"fleeing",ObjFlee,GameObject),
	GameFunction(GameBool,"someAmmo",ObjSomeAmmo,GameObject),
	GameFunction(GameScalar,"fuel",ObjFuel,GameObject),
	GameFunction(GameScalar,"rating",ObjExperience,GameObject),
	GameFunction(GameScalar,"score",ObjGetScore,GameObject),
	GameFunction(GameObject,"formLeader",ObjLeader,GameObject),
	GameFunction(GameObject,"leader",ObjGroupLeader,GameObject),
	GameFunction(GameObject,"vehicle",ObjVehicle,GameObject),
	GameFunction(GameObject,"driver",ObjDriver,GameObject),
	GameFunction(GameObject,"commander",ObjCommander,GameObject),
	GameFunction(GameObject,"gunner",ObjGunner,GameObject),

	GameFunction(GameObject,"leader",GrpLeader,GameGroup),
	GameFunction(GameGroup,"group",ObjGroup,GameObject),
	GameFunction(GameArray,"units",GrpUnits,GameGroup),
	GameFunction(GameArray,"units",GrpUnits,GameObject),

	GameFunction(GameArray,"crew",ObjListIn,GameObject),

	GameFunction(GameObject,"flagOwner",ObjGetFlagOwner,GameObject),
	GameFunction(GameObject,"flag",ObjGetFlag,GameObject),

	GameFunction(GameBool,"inflamed",ObjInflamed,GameObject),
	GameFunction(GameString,"lightIsOn",ObjLightSwitched,GameObject),

	GameFunction(GameScalar,"scudState",ObjGetScudState,GameObject),

	GameFunction(GameArray,"list",ObjList,GameObject),
	GameFunction(GameSide,"side",ObjSide,GameObjectOrGroup),
	GameFunction(GameString,"name",ObjName,GameObject),
	GameFunction(GameString,"behaviour",ObjBehaviour,GameObject),
	GameFunction(GameString,"combatMode",GrpCombatMode,GameObjectOrGroup),
	GameFunction(GameString,"formation",GrpFormation,GameObjectOrGroup),
	GameFunction(GameString,"speedMode",GrpSpeedMode,GameObjectOrGroup),
	GameFunction(GameNothing,"titleCut",CutText,GameArray),

	GameFunction(GameNothing,"cutText",CutText,GameArray),
	GameFunction(GameNothing,"cutRsc",CutRsc,GameArray),
	GameFunction(GameNothing,"cutObj",CutObj,GameArray),

	GameFunction(GameNothing,"titleText",GameTitleText,GameArray),
	GameFunction(GameNothing,"titleRsc",GameTitleRsc,GameArray),
	GameFunction(GameNothing,"titleObj",GameTitleObj,GameArray),

	GameFunction(GameNothing,"playSound",PlaySound,GameString),
	GameFunction(GameNothing,"playMusic",PlayMusic,GameString),
	GameFunction(GameNothing,"playMusic",PlayMusic,GameArray),

	GameFunction(GameBool,"locked",ObjLocked,GameObject),
	GameFunction(GameBool,"stopped",ObjStopped,GameObject),
	GameFunction(GameBool,"captive",ObjCaptive,GameObject),

	GameFunction(GameNothing,"unassignVehicle",ObjUnassignVehicle,GameObject),

	GameFunction(GameNothing,"removeAllWeapons",ObjRemoveAllWeapons,GameObject),

	GameFunction(GameNothing,"clearWeaponCargo",ObjClearWeaponCargo,GameObject),
	GameFunction(GameNothing,"clearMagazineCargo",ObjClearMagazineCargo,GameObject),

	GameFunction(GameNothing,"deleteVehicle",VehDelete,GameObject),

	GameFunction(GameNothing,"showMap",ShowMap,GameBool),
	GameFunction(GameNothing,"showWatch",ShowWatch,GameBool),
	GameFunction(GameNothing,"showCompass",ShowCompass,GameBool),
	GameFunction(GameNothing,"showRadio",ShowWalkieTalkie,GameBool),
	GameFunction(GameNothing,"showPad",ShowNotepad,GameBool),
	GameFunction(GameNothing,"showWarrant",ShowWarrant,GameBool),
	GameFunction(GameNothing,"showGps",ShowGPS,GameBool),
	GameFunction(GameNothing,"enableRadio",EnableRadio,GameBool),

	GameFunction(GameNothing,"showCinemaBorder",ShowCinemaBorder,GameBool),
	GameFunction(GameNothing,"disableUserInput",DisableUserInput,GameBool),
	GameFunction(GameNothing,"publicVariable",PublicVariable,GameString),

	GameFunction(GameNothing,"hint",ShowHint,GameString),
	GameFunction(GameNothing,"hintCadet",ShowHintCadet,GameString),
	GameFunction(GameNothing,"hintC",ShowHintC,GameString),

	GameFunction(GameString,"format",StrFormat,GameArray),
	GameFunction(GameString,"localize",StrLocalize,GameString),

	GameFunction(GameNothing,"skipTime",SkipDayTime,GameScalar),
	GameFunction(GameNothing,"setViewDistance",SetViewDistance,GameScalar),
	GameFunction(GameNothing,"setTerrainGrid",SetTerrainGrid,GameScalar),

	GameFunction(GameNothing,"goto",ScriptGoto,GameString),

	GameFunction(GameNothing,"textLog",TextLog,GameVoid),
	GameFunction(GameNothing,"debugLog",TextDebugLog,GameVoid),

	GameFunction(GameNothing,"camDestroy",CamDestroy,GameObject),
	GameFunction(GameBool,"camCommitted",CamCommited,GameObject),

	GameFunction(GameBool,"unitReady",VehDone,GameObjectOrArray),

	GameFunction(GameNothing,"saveVar",SaveVar,GameString),

	GameFunction(GameNothing,"setAccTime",SetAcceleratedTime,GameScalar),

	GameFunction(GameNothing,"forceMap",MapForce, GameBool),
	GameFunction(GameNothing,"mapAnimAdd",MapAnimAdd, GameArray),

	GameFunction(GameNothing,"estimatedTimeLeft",EstimatedTimeLeft, GameScalar),

#if _ENABLE_DATADISC
	GameFunction(GameBool,"createDialog",DialogCreate, GameString),
	GameFunction(GameNothing,"closeDialog",DialogClose, GameScalar),

	GameFunction(GameBool,"ctrlVisible",CtrlVisible, GameScalar),
	GameFunction(GameBool,"ctrlEnabled",CtrlEnabled, GameScalar),
	GameFunction(GameNothing,"ctrlShow",CtrlShow, GameArray),
	GameFunction(GameNothing,"ctrlEnable",CtrlEnable, GameArray),
	
	GameFunction(GameString,"ctrlText",CtrlGetText, GameScalar),
	GameFunction(GameNothing,"ctrlSetText",CtrlSetText, GameArray),

	GameFunction(GameString,"buttonAction",ButtonGetAction, GameScalar),
	GameFunction(GameNothing,"buttonSetAction",ButtonSetAction, GameArray),

	GameFunction(GameScalar,"lbSize",LBGetSize, GameScalar),
	GameFunction(GameScalar,"lbCurSel",LBGetCurSel, GameScalar),
	GameFunction(GameNothing,"lbSetCurSel",LBSetCurSel, GameArray),
	GameFunction(GameNothing,"lbClear",LBClear, GameScalar),
	GameFunction(GameScalar,"lbAdd",LBAdd, GameArray),
	GameFunction(GameNothing,"lbDelete",LBDelete, GameArray),
	GameFunction(GameString,"lbText",LBGetText, GameArray),
	GameFunction(GameString,"lbData",LBGetData, GameArray),
	GameFunction(GameNothing,"lbSetData",LBSetData, GameArray),
	GameFunction(GameScalar,"lbValue",LBGetValue, GameArray),
	GameFunction(GameNothing,"lbSetValue",LBSetValue, GameArray),
	GameFunction(GameString,"lbPicture",LBGetPicture, GameArray),
	GameFunction(GameNothing,"lbSetPicture",LBSetPicture, GameArray),
	GameFunction(GameArray,"lbColor",LBGetColor, GameArray),
	GameFunction(GameNothing,"lbSetColor",LBSetColor, GameArray),

	GameFunction(GameScalar, "sliderPosition", SliderGetPosition, GameScalar),
	GameFunction(GameNothing, "sliderSetPosition", SliderSetPosition, GameArray),
	GameFunction(GameArray, "sliderRange", SliderGetRange, GameScalar),
	GameFunction(GameNothing, "sliderSetRange", SliderSetRange, GameArray),
	GameFunction(GameArray, "sliderSpeed", SliderGetSpeed, GameScalar),
	GameFunction(GameNothing, "sliderSetSpeed", SliderSetSpeed, GameArray),

	GameFunction(GameNothing,"addWeaponPool",PoolAddWeapon, GameArray),
	GameFunction(GameNothing,"addMagazinePool",PoolAddMagazine, GameArray),

	GameFunction(GameNothing,"putWeaponPool",PoolGetWeapons, GameObject),
	GameFunction(GameNothing,"pickWeaponPool",PoolSetWeapons, GameObject),

	GameFunction(GameScalar,"queryWeaponPool",PoolQueryWeapons, GameString),
	GameFunction(GameScalar,"queryMagazinePool",PoolQueryMagazines, GameString),

	GameFunction(GameNothing,"drop",ParticleDrop, GameArray),

	GameFunction(GameNothing,"onBriefingPlan",BriefingOnPlan, GameString),
	GameFunction(GameNothing,"onBriefingNotes",BriefingOnNotes, GameString),
	GameFunction(GameNothing,"onBriefingGear",BriefingOnGear, GameString),
	GameFunction(GameNothing,"onBriefingGroup",BriefingOnGroup, GameString),

	GameFunction(GameNothing,"onMapSingleClick",MapOnSingleClick, GameString),

	GameFunction(GameNothing,"fillWeaponsFromPool",ObjWeaponsFromPool, GameObject),

	GameFunction(GameScalar,"skill",ObjGetSkill,GameObject),
	
	GameFunction(GameString,"primaryWeapon",ObjGetPrimaryWeapon,GameObject),
	GameFunction(GameString,"secondaryWeapon",ObjGetSecondaryWeapon,GameObject),
	GameFunction(GameArray,"weapons",ObjGetAllWeapons,GameObject),
	GameFunction(GameArray,"magazines",ObjGetAllMagazines,GameObject),

	GameFunction(GameObject,"object",GetObject,GameScalar),

	GameFunction(GameBool,"deleteStatus",DeleteStatus,GameString),
	GameFunction(GameBool,"deleteIdentity",DeleteIdentity,GameString),

#endif

#if _ENABLE_VBS
	GameFunction(GameFile,"loadConfig",ConfigLoad,GameString),

	GameFunction(GameNothing,"VBS_addHeader",VBS_AddHeader,GameString),
	GameFunction(GameNothing,"VBS_addEvent",VBS_AddEvent,GameString),
	GameFunction(GameNothing,"VBS_addFooter",VBS_AddFooter,GameString),

	GameFunction(GameArray,"VBS_kills",VBS_GetKills,GameString),
	GameFunction(GameArray,"VBS_killed",VBS_GetKilled,GameString),
	GameFunction(GameArray,"VBS_injuries",VBS_GetInjuries,GameString),
#endif

	GameFunction(GameString,"loadFile",StringLoad,GameString),
	GameFunction(GameString,"preprocessFile",StringPreprocess,GameString),

	GameFunction(GameScalar,"playersNumber",PlayersNumber,GameSide),

	GameFunction(GameBool,"isEngineOn",VehIsEngineOn,GameObject),

	TABLE_COMMAND_S(stop,Stop)

#if _ENABLE_CHEATS
	GameFunction(GameNothing,"DBG_switchLandscape",DBG_SwitchLandscape,GameString),
	GameFunction(GameNothing,"DBG_screenshot",DBG_Screenshot,GameString),
#endif

#if _ENABLE_CHEATS || _ENABLE_VBS

	GameFunction(GameNothing,"setDate",SetDate,GameArray),
	GameFunction(GameSide,"createCenter",CenterCreate,GameSide),
	GameFunction(GameNothing,"deleteCenter",CenterDelete,GameSide),
	GameFunction(GameGroup,"createGroup",GroupCreate,GameSide),
	GameFunction(GameNothing,"deleteGroup",GroupDelete,GameGroup),

	GameFunction(GameString,"createMarker",MarkerCreate,GameArray),
	GameFunction(GameNothing,"deleteMarker",MarkerDelete,GameString),
	GameFunction(GameScalar,"getMarkerDir",MarkerGetDir,GameString),

	GameFunction(GameObject,"createTrigger",TriggerCreate,GameArray),
	GameFunction(GameNothing,"createGuardedPoint",GuardedPointCreate,GameArray),

	GameFunction(GameNothing,"deleteWaypoint",WaypointDelete,GameArray),

	GameFunction(GameString,"getMove",ObjGetMove,GameObject),
#endif

	GameFunction(GameString,"typeOf",ObjGetType,GameObject),
};


static const GameOperator ExtBinary[]=
{
	GameOperator(GameNothing,"setCaptive",function,ObjSetCaptive,GameObject,GameBool),
	GameOperator(GameNothing,"setIdentity",function,ObjSetIdentity,GameObject,GameString),
	GameOperator(GameNothing,"setFaceanimation",function,ObjSetFaceAnimation,GameObject,GameScalar),
	GameOperator(GameNothing,"setFace",function,ObjSetFace,GameObject,GameString),
	GameOperator(GameNothing,"setMimic",function,ObjSetMimic,GameObject,GameString),
	GameOperator(GameNothing,"setFlagTexture",function,ObjSetFlagTexture,GameObject,GameString),
	GameOperator(GameNothing,"setFlagSide",function,ObjSetFlagSide,GameObject,GameSide),
	GameOperator(GameNothing,"setFlagOwner",function,ObjSetFlagOwner,GameObject,GameObject),
	GameOperator(GameArray,"buildingPos",function,ObjGetBuildingPos,GameObject,GameScalar),
	GameOperator(GameNothing,"switchLight",function,ObjSwitchLight,GameObject,GameString),
	GameOperator(GameNothing,"inflame",function,ObjInflame,GameObject,GameBool),
	GameOperator(GameNothing,"addRating",function,ObjAddExperience,GameObject,GameScalar),
	GameOperator(GameNothing,"addScore",function,ObjAddScore,GameObject,GameScalar),
	GameOperator(GameScalar,"distance",function,ObjDistance,GameObject,GameObject),
	GameOperator(GameNothing,"setPos",function,ObjSetPos,GameObject,GameArray),
	GameOperator(GameNothing,"setPosASL",function,ObjSetPosASL,GameObject,GameArray),
	GameOperator(GameNothing,"setDir",function,ObjSetDir,GameObject,GameScalar),
	GameOperator(GameNothing,"setVelocity",function,ObjSetVelocity,GameObject,GameArray),
	GameOperator(GameNothing,"setFormDir",function,GrpSetFormDir,GameObjectOrGroup,GameScalar),
	GameOperator(GameNothing,"setDammage",function,ObjSetDammage,GameObject,GameScalar),
	GameOperator(GameNothing,"setDamage",function,ObjSetDammage,GameObject,GameScalar),
	GameOperator(GameNothing,"allowDammage",function,ObjAllowDammage,GameObject,GameBool),
	GameOperator(GameNothing,"flyInHeight",function,ObjSetFlyingHeight,GameObject,GameScalar),
	GameOperator(GameNothing,"setMarkerPos",function,MarkerSetPos,GameString,GameArray),
	GameOperator(GameNothing,"setMarkerType",function,MarkerSetType,GameString,GameString),
	GameOperator(GameNothing,"setMarkerSize",function,MarkerSetSize,GameString,GameArray),
	GameOperator(GameNothing,"setMarkerColor",function,MarkerSetColor,GameString,GameString),
	GameOperator(GameNothing,"setWPPos",function,WpSetPos,GameArray,GameArray),
	GameOperator(GameBool,"in",function,ObjIn,GameObject,GameObject),
	GameOperator(GameScalar,"ammo",function,ObjAmmo,GameObject,GameString),
	GameOperator(GameBool,"hasWeapon",function,ObjHasWeapon,GameObject,GameString),
	GameOperator(GameNothing,"addWeapon",function,ObjAddWeapon,GameObject,GameString),
	GameOperator(GameNothing,"removeWeapon",function,ObjRemoveWeapon,GameObject,GameString),
	GameOperator(GameNothing,"addMagazine",function,ObjAddMagazine,GameObject,GameString),
	GameOperator(GameNothing,"removeMagazine",function,ObjRemoveMagazine,GameObject,GameString),
	GameOperator(GameNothing,"removeMagazines",function,ObjRemoveMagazines,GameObject,GameString),
	GameOperator(GameNothing,"selectWeapon",function,ObjSelectWeapon,GameObject,GameString),
	GameOperator(GameNothing,"fire",function,ObjFire,GameObject,GameString),
	GameOperator(GameNothing,"fire",function,ObjFireEx,GameObject,GameArray),
	GameOperator(GameNothing,"land",function,ObjLand,GameObject,GameString),
	GameOperator(GameScalar,"knowsAbout",function,GrpKnowsAbout,GameObjectOrGroup,GameObject),
	GameOperator(GameNothing,"say",function,ObjSay,GameObject,GameString),
	GameOperator(GameNothing,"say",function,ObjSay,GameObject,GameArray),
	GameOperator(GameNothing,"globalRadio",function,ObjGlobalRadio,GameObject,GameString),
	GameOperator(GameNothing,"sideRadio",function,ObjSideRadio,GameObjectOrArray,GameString),
	GameOperator(GameNothing,"groupRadio",function,ObjGroupRadio,GameObject,GameString),
	GameOperator(GameNothing,"vehicleRadio",function,ObjVehicleRadio,GameObject,GameString),
	GameOperator(GameNothing,"globalChat",function,ObjGlobalChat,GameObject,GameString),
	GameOperator(GameNothing,"sideChat",function,ObjSideChat,GameObjectOrArray,GameString),
	GameOperator(GameNothing,"groupChat",function,ObjGroupChat,GameObject,GameString),
	GameOperator(GameNothing,"vehicleChat",function,ObjVehicleChat,GameObject,GameString),
	GameOperator(GameNothing,"playMove",function,ObjPlayMove,GameObject,GameString),
	GameOperator(GameNothing,"switchMove",function,ObjSwitchMove,GameObject,GameString),
	GameOperator(GameNothing,"setRadioMsg",function,SetRadioMessage,GameScalar,GameString),
	GameOperator(GameBool,"==",function,ObjCmpE,GameObject,GameObject),
	GameOperator(GameBool,"!=",function,ObjCmpNE,GameObject,GameObject),
	GameOperator(GameBool,"==",function,GrpCmpE,GameGroup,GameGroup),
	GameOperator(GameBool,"!=",function,GrpCmpNE,GameGroup,GameGroup),
	GameOperator(GameBool,"==",function,SideCmpE,GameSide,GameSide),
	GameOperator(GameBool,"!=",function,SideCmpNE,GameSide,GameSide),
	GameOperator(GameScalar,"countEnemy",function,ListCountEnemy,GameObject,GameArray),
	GameOperator(GameScalar,"countFriendly",function,ListCountFriendly,GameObject,GameArray),
	GameOperator(GameScalar,"countUnknown",function,ListCountUnknown,GameObject,GameArray),
	GameOperator(GameScalar,"countType",function,ListCountType,GameString,GameArray),
	GameOperator(GameScalar,"countSide",function,ListCountSide,GameSide,GameArray),
	GameOperator(GameNothing,"allowGetIn",function,ListAllowGetIn,GameArray,GameBool),
	GameOperator(GameNothing,"orderGetIn",function,ListOrderGetIn,GameArray,GameBool),
	GameOperator(GameNothing,"join",function,ListJoin,GameArray,GameObjectOrGroup),
	GameOperator(GameNothing,"move",function,GrpMove,GameObjectOrGroup,GameArray),
	GameOperator(GameNothing,"setGroupid",function,GrpSetIdentity,GameObjectOrGroup,GameArray),
	GameOperator(GameNothing,"setBehaviour",function,GrpSetBehaviour,GameObjectOrGroup,GameString),
	GameOperator(GameNothing,"setCombatMode",function,GrpSetCombatMode,GameObjectOrGroup,GameString),
	GameOperator(GameNothing,"setFormation",function,GrpSetFormation,GameObjectOrGroup,GameString),
	GameOperator(GameNothing,"setSpeedMode",function,GrpSetSpeedMode,GameObjectOrGroup,GameString),
	GameOperator(GameNothing,"setUnitPos",function,ObjSetUnitPos,GameObject,GameString),
	GameOperator(GameNothing,"lockWp",function,GrpLockWP,GameObjectOrGroup,GameBool),
	GameOperator(GameNothing,"lock",function,ObjLock,GameObject,GameBool),
	GameOperator(GameNothing,"stop",function,ObjStop,GameObject,GameBool),
	GameOperator(GameNothing,"disableAI",function,ObjDisableAI,GameObject,GameString),
	GameOperator(GameNothing,"assignAsCommander",function,ObjAssignAsCommander,GameObject,GameObject),
	GameOperator(GameNothing,"assignAsDriver",function,ObjAssignAsDriver,GameObject,GameObject),
	GameOperator(GameNothing,"assignAsGunner",function,ObjAssignAsGunner,GameObject,GameObject),
	GameOperator(GameNothing,"assignAsCargo",function,ObjAssignAsCargo,GameObject,GameObject),
	GameOperator(GameNothing,"leaveVehicle",function,GrpLeaveVehicle,GameObject,GameObject),
	GameOperator(GameNothing,"leaveVehicle",function,GrpLeaveVehicle,GameGroup,GameObject),
	GameOperator(GameNothing,"moveInCommander",function,ObjMoveInCommander,GameObject,GameObject),
	GameOperator(GameNothing,"moveInDriver",function,ObjMoveInDriver,GameObject,GameObject),
	GameOperator(GameNothing,"moveInGunner",function,ObjMoveInGunner,GameObject,GameObject),
	GameOperator(GameNothing,"moveInCargo",function,ObjMoveInCargo,GameObject,GameObject),
	GameOperator(GameNothing,"allowFleeing",function,GrpAllowFleeing,GameObjectOrGroup,GameScalar),
	GameOperator(GameNothing,"objStatus",function,SetObjectiveStatus,GameString,GameString),
	GameOperator(GameNothing,"exec",function,ScriptExecute,GameVoid,GameString),

	GameOperator(GameNothing,"setOvercast",function,SetOvercast,GameScalar, GameScalar),
	GameOperator(GameNothing,"setFog",function,SetFog,GameScalar, GameScalar),
	GameOperator(GameNothing,"setRain",function,SetRain,GameScalar, GameScalar),

	GameOperator(GameNothing,"setFuel",function,ObjSetFuel,GameObject,GameScalar),
	GameOperator(GameNothing,"setFuelCargo",function,ObjSetFuelCargo,GameObject,GameScalar),
	GameOperator(GameNothing,"setRepairCargo",function,ObjSetRepairCargo,GameObject,GameScalar),
	GameOperator(GameNothing,"setAmmoCargo",function,ObjSetAmmoCargo,GameObject,GameScalar),
	GameOperator(GameNothing,"addWeaponCargo",function,ObjAddWeaponCargo,GameObject,GameArray),
	GameOperator(GameNothing,"addMagazineCargo",function,ObjAddMagazineCargo,GameObject,GameArray),

	GameOperator(GameObject,"createVehicle",function,VehCreate,GameString,GameArray),
	GameOperator(GameNothing,"createUnit",function,UnitCreate,GameString,GameArray),

	GameOperator(GameNothing,"fadeMusic",function,SetMusicVolume,GameScalar,GameScalar),
	GameOperator(GameNothing,"fadeSound",function,SetSoundVolume,GameScalar,GameScalar),

	GameOperator(GameNothing,"cameraEffect",function,ObjCameraEffect,GameObject, GameArray),
	GameOperator(GameObject,"camCreate",function,CamCreate,GameString, GameArray),
	GameOperator(GameNothing,"camSetPos",function,CamSetPos,GameObject, GameArray),
	GameOperator(GameNothing,"camSetRelPos",function,CamSetRelPos,GameObject, GameArray),
	GameOperator(GameNothing,"camSetFov",function,CamSetFOV,GameObject, GameScalar),
	GameOperator(GameNothing,"camSetFovRange",function,CamSetFOVRange,GameObject, GameArray),

	GameOperator(GameNothing,"camSetDive",function,CamSetDive,GameObject, GameScalar),
	GameOperator(GameNothing,"camSetBank",function,CamSetDive,GameObject, GameScalar),
	GameOperator(GameNothing,"camSetDir",function,CamSetDive,GameObject, GameScalar),
	GameOperator(GameNothing,"camCommit",function,CamCommit,GameObject, GameScalar),
	GameOperator(GameNothing,"camSetTarget",function,CamSetTargetObj,GameObject, GameObject),
	GameOperator(GameNothing,"camSetTarget",function,CamSetTargetVec,GameObject, GameArray),
	GameOperator(GameNothing,"camCommand",function,CamCommand,GameObject, GameString),

	GameOperator(GameNothing,"switchCamera",function,ObjSwitchCamera, GameObject, GameString),

	TABLE_COMMAND(Move,Move,GameArray)
	TABLE_COMMAND(Watch,Watch,GameArray)
	TABLE_COMMAND(Watch,Watch,GameObject)
	TABLE_COMMAND(Target,Target,GameObject)
	TABLE_COMMAND(Follow,Follow,GameObject)
	TABLE_COMMAND(Fire,Fire,GameObject)

	GameOperator(GameNothing,"action",function,VehProcessAction,GameObject, GameArray),
	GameOperator(GameNothing|GameScalar,"addAction",function,VehAddUserAction,GameObject, GameArray),
	GameOperator(GameNothing,"removeAction",function,VehRemoveUserAction,GameObject, GameScalar),
	GameOperator(GameNothing,"reveal",function,GrpReveal,GameObjectOrGroup, GameObject),

	GameOperator(GameNothing|GameScalar,"addEventHandler",function,VehAddEventHandler,GameObject, GameArray),
	GameOperator(GameNothing,"removeEventHandler",function,VehRemoveEventHandler,GameObject, GameArray),
	GameOperator(GameNothing,"removeAllEventHandlers",function,VehRemoveAllEventHandlers,GameObject,GameString),

	GameOperator(GameNothing,"engineOn",function,VehEngineOn,GameObject,GameBool),

#if _ENABLE_DATADISC
	GameOperator(GameBool,"saveStatus",function,ObjSaveStatus,GameObject, GameString),
	GameOperator(GameBool,"loadStatus",function,ObjLoadStatus,GameObject, GameString),

	GameOperator(GameBool,"saveIdentity",function,ObjSaveIdentity,GameObject, GameString),
	GameOperator(GameBool,"loadIdentity",function,ObjLoadIdentity,GameObject, GameString),

	GameOperator(GameNothing,"animate",function,ObjAnimate,GameObject, GameArray),
	GameOperator(GameScalar,"animationPhase",function,ObjAnimationPhase,GameObject, GameString),

//	GameOperator(GameNothing,"changeModel",function,ObjChangeModel,GameObject, GameString),

	GameOperator(GameNothing,"setSkill",function,ObjSetSkill,GameObject,GameScalar),

	GameOperator(GameNothing,"setObjectTexture",function,ObjSetTexture,GameObject,GameArray),
#endif

#if _ENABLE_VBS
	GameOperator(GameNothing,"saveConfig",function,ConfigSave,GameFile,GameString),
	GameOperator(GameFile,"openClass",function,ClassOpen,GameFile,GameString),
	GameOperator(GameFile,"addClass",function,ClassAdd,GameFile,GameString),
	GameOperator(GameVoid,"getValue",function,ValueGet,GameFile,GameString),
	GameOperator(GameNothing,"addValue",function,ValueAdd,GameFile,GameArray),
#endif

#if _ENABLE_CHEATS || _ENABLE_VBS
	GameOperator(GameNothing,"setFriend",function,CenterSetFriend,GameSide,GameArray),

	GameOperator(GameNothing,"setMarkerText",function,MarkerSetText,GameString,GameString),
	GameOperator(GameNothing,"setMarkerShape",function,MarkerSetShape,GameString,GameString),
	GameOperator(GameNothing,"setMarkerBrush",function,MarkerSetBrush,GameString,GameString),
	GameOperator(GameNothing,"setMarkerDir",function,MarkerSetDir,GameString,GameScalar),

	GameOperator(GameNothing,"setTriggerArea",function,TriggerSetArea,GameObject,GameArray),
	GameOperator(GameNothing,"setTriggerActivation",function,TriggerSetActivation,GameObject,GameArray),
	GameOperator(GameNothing,"setTriggerType",function,TriggerSetType,GameObject,GameString),
	GameOperator(GameNothing,"setTriggerTimeout",function,TriggerSetTimeout,GameObject,GameArray),
	GameOperator(GameNothing,"setTriggerText",function,TriggerSetText,GameObject,GameString),
	GameOperator(GameNothing,"setTriggerStatements",function,TriggerSetStatements,GameObject,GameArray),
	GameOperator(GameNothing,"triggerAttachObject",function,TriggerAttachObject,GameObject,GameScalar),
	GameOperator(GameNothing,"triggerAttachVehicle",function,TriggerAttachVehicle,GameObject,GameArray),

	GameOperator(GameNothing,"setEffectCondition",function,EffectSetCondition,GameObject|GameArray,GameString),
	GameOperator(GameNothing,"setCameraEffect",function,EffectSetCamera,GameObject|GameArray,GameArray),
	GameOperator(GameNothing,"setSoundEffect",function,EffectSetSound,GameObject|GameArray,GameArray),
	GameOperator(GameNothing,"setMusicEffect",function,EffectSetMusic,GameObject|GameArray,GameString),
	GameOperator(GameNothing,"setTitleEffect",function,EffectSetTitle,GameObject|GameArray,GameArray),
	GameOperator(GameNothing,"synchronizeWaypoint",function,WaypointSynchronize,GameArray,GameArray),
	GameOperator(GameNothing,"synchronizeWaypoint",function,TriggerSynchronize,GameObject,GameArray),

	GameOperator(GameArray,"addWaypoint",function,WaypointAdd,GameGroup,GameArray),
	GameOperator(GameNothing,"setWaypointPosition",function,WaypointSetPosition,GameArray,GameArray),
	GameOperator(GameNothing,"setWaypointType",function,WaypointSetType,GameArray,GameString),
	GameOperator(GameNothing,"waypointAttachVehicle",function,WaypointAttachVehicle,GameArray,GameObject),
	GameOperator(GameNothing,"waypointAttachObject",function,WaypointAttachObject,GameArray,GameScalar),
	GameOperator(GameNothing,"setWaypointHousePosition",function,WaypointSetHousePos,GameArray,GameScalar),
	GameOperator(GameNothing,"setWaypointCombatMode",function,WaypointSetCombatMode,GameArray,GameString),
	GameOperator(GameNothing,"setWaypointFormation",function,WaypointSetFormation,GameArray,GameString),
	GameOperator(GameNothing,"setWaypointSpeed",function,WaypointSetSpeed,GameArray,GameString),
	GameOperator(GameNothing,"setWaypointBehaviour",function,WaypointSetBehaviour,GameArray,GameString),
	GameOperator(GameNothing,"setWaypointDescription",function,WaypointSetDescription,GameArray,GameString),
	GameOperator(GameNothing,"setWaypointStatements",function,WaypointSetStatements,GameArray,GameArray),
	GameOperator(GameNothing,"setWaypointScript",function,WaypointSetScript,GameArray,GameString),
	GameOperator(GameNothing,"setWaypointTimeout",function,WaypointSetTimeout,GameArray,GameArray),
	GameOperator(GameNothing,"showWaypoint",function,WaypointShow,GameArray,GameString),
#endif
};


#include <El/Modules/modules.hpp>

INIT_MODULE(GameStateExt, 2)
{
	// stringtable must be loaded
	GGameState.Init();

	GGameState.NewType("OBJECT",GameObject, CreateGameDataObject, LocalizeString(IDS_EVAL_TYPEOBJECT));
	GGameState.NewType("VECTOR",GameVector, NULL, LocalizeString(IDS_EVAL_TYPEVECTOR));
	GGameState.NewType("TRANS",GameTrans, NULL, LocalizeString(IDS_EVAL_TYPETRANS));
	GGameState.NewType("ORIENT",GameOrient, NULL, LocalizeString(IDS_EVAL_TYPEORIENT));
	GGameState.NewType("SIDE",GameSide, CreateGameDataSide, LocalizeString(IDS_EVAL_TYPESIDE));
	GGameState.NewType("GROUP",GameGroup, CreateGameDataGroup, LocalizeString(IDS_EVAL_TYPEGROUP));
#if _ENABLE_VBS
	GGameState.NewType("FILE",GameFile, CreateGameDataFile, /*LocalizeString(IDS_EVAL_TYPEGROUP)*/"File");
#endif

	for( int i=0; i<sizeof(ExtUnary)/sizeof(*ExtUnary); i++ )
	{
		GGameState.NewFunction(ExtUnary[i]);
	}
	for( int i=0; i<sizeof(ExtBinary)/sizeof(*ExtBinary); i++ )
	{
		GGameState.NewOperator(ExtBinary[i]);
	}
	for( int i=0; i<sizeof(ExtNular)/sizeof(*ExtNular); i++ )
	{
		GGameState.NewNularOp(ExtNular[i]);
	}
};
