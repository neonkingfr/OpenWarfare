#ifdef _MSC_VER
#pragma once
#endif

#ifndef _SCRIPTS_HPP
#define _SCRIPTS_HPP

#include <Es/Strings/rString.hpp>
#include <Es/Containers/array.hpp>
#include "object.hpp"
#include <El/Evaluator/express.hpp>

struct ScriptLine
{
	RString waitUntil; // wait until condition is met
	RString suspendUntil; // wait until time reach this value
	RString condition; // skip if condition not met
	RString statement; // execute

	LSError Serialize(ParamArchive &ar);
};
TypeIsMovableZeroed(ScriptLine);

struct ScriptLabel
{
	RString name;
	int line;

	LSError Serialize(ParamArchive &ar);
};
TypeIsMovableZeroed(ScriptLabel);

#include <Es/Types/lLinks.hpp>

class Script : public RemoveLLinks
{
protected:
//	OLink<Object> _vehicle;
	GameValue _argument;
	Time _baseTime;
	int _currentLine;
	AutoArray<ScriptLine> _lines;
	AutoArray<ScriptLabel> _labels;
	GameVarSpace _vars;
	float _time;
	float _suspendedUntil;
	int _maxLinesAtOnce;
	RString _name;

public:
	Script();
	Script(RString name, GameValuePar argument, int maxLines=-1);
	Script(const AutoArray<RString> &lines, GameValuePar argument, int maxLines=-1);
	~Script();

	RString GetDebugName() const {return _name;}
	void SetName(RString name) {_name=name;}

	LSError Serialize(ParamArchive &ar);
	static Script *CreateObject(ParamArchive &ar) {return new Script();}
	
	bool SimulateBody();
	bool OnSimulate();
	bool Simulate(float deltaT);
	bool IsTerminated() const;
	void GoTo(RString label);
	void Exit();

	GameValuePar VarGet(const char *name) const;
	void VarSet(const char *name, GameValuePar value, bool readOnly = false);

protected:
	void ProcessLine(const char *line);
};
//TypeContainsOLink(Script)

#endif


