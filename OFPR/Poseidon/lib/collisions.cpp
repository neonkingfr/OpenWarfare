// Landscape - generalized landscape drawing
// (C) 1996, SUMA
#include "wpch.hpp"
#include "landscape.hpp"
#include "scene.hpp"
#include "world.hpp"
#include "vehicle.hpp"
#include "shots.hpp"
#include "camera.hpp"
#include "vehicleAI.hpp"
#include "ai.hpp"
//#include "engine.hpp"
#include <El/Common/randomGen.hpp>
#include "global.hpp"
#include <El/Common/perfLog.hpp>
#include "network.hpp"
#include "thing.hpp"
#include "diagModes.hpp"

#define PROFILE_COLLISIONS 1 // extact performance evaluation

#if PROFILE_COLLISIONS
	#include "perfProf.hpp"
#else
	#define START_PROFILE(name)
	#define END_PROFILE(name)
	#define PROFILE_SCOPE(name)
#endif

#if _ENABLE_CHEATS
#define STARS 1
#endif


void ObjRadiusRectangle
(
	int &xMin, int &xMax, int &zMin, int &zMax,
	Vector3Par oPos, Vector3Par nPos, float radius
)
{
	const float maxObjRadius=25;
	radius+=maxObjRadius;
	float xMinF=floatMin(nPos.X(),oPos.X())-radius;
	float xMaxF=floatMax(nPos.X(),oPos.X())+radius;
	float zMinF=floatMin(nPos.Z(),oPos.Z())-radius;
	float zMaxF=floatMax(nPos.Z(),oPos.Z())+radius;
	xMin=toIntFloor(xMinF*InvObjGrid);
	xMax=toIntFloor(xMaxF*InvObjGrid);
	zMin=toIntFloor(zMinF*InvObjGrid);
	zMax=toIntFloor(zMaxF*InvObjGrid);
	// make sure always at least one square is valid
	if( xMin<0 ) xMin=0;if( xMin>ObjRange-1 ) xMin=ObjRange-1;
	if( xMax<0 ) xMax=0;if( xMax>ObjRange-1 ) xMax=ObjRange-1;
	if( zMin<0 ) zMin=0;if( zMin>ObjRange-1 ) zMin=ObjRange-1;
	if( zMax<0 ) zMax=0;if( zMax>ObjRange-1 ) zMax=ObjRange-1;
}


void ObjRadiusRectangle
(
	int &xMin, int &xMax, int &zMin, int &zMax,
	Vector3Par oPos, float radius
)
{
	float xMinF = oPos.X()-radius;
	float xMaxF = oPos.X()+radius;
	float zMinF = oPos.Z()-radius;
	float zMaxF = oPos.Z()+radius;
	xMin=toIntFloor(xMinF*InvObjGrid);
	xMax=toIntFloor(xMaxF*InvObjGrid);
	zMin=toIntFloor(zMinF*InvObjGrid);
	zMax=toIntFloor(zMaxF*InvObjGrid);
	// make sure always at least one square is valid
	if( xMin<0 ) xMin=0;if( xMin>ObjRange-1 ) xMin=ObjRange-1;
	if( xMax<0 ) xMax=0;if( xMax>ObjRange-1 ) xMax=ObjRange-1;
	if( zMin<0 ) zMin=0;if( zMin>ObjRange-1 ) zMin=ObjRange-1;
	if( zMax<0 ) zMax=0;if( zMax>ObjRange-1 ) zMax=ObjRange-1;
}

extern Ref<Object> DrawDiagLine(Vector3Par from, Vector3Par to, PackedColor color);

#define DIAG 0
#define DIAG_LAND 0
#define DIAG_VISIBLE_THROUGH 0

void Landscape::ObjectCollision
(
	CollisionBuffer &retVal,
	Object *with, const Frame &withPos, bool onlyVehicles
) const
{ // check with all object
	// AI soldier test collisions only with non-static objects
	if( !with->GetShape()->GeometryLevel() ) return; // no geometry - no test
	PROFILE_SCOPE(lObj1);
	Vector3Val nPos=withPos.Position();
	Vector3Val oPos=with->Position();
	float radius=with->GetRadius();
	int xMin,xMax,zMin,zMax;
	ObjRadiusRectangle(xMin,xMax,zMin,zMax,oPos,nPos,radius);
	int x,z;
	for( x=xMin; x<=xMax; x++ ) for( z=zMin; z<=zMax; z++ )
	{
		const ObjectList &list=_objects(x,z);
		int n=list.Size();
		for( int i=0; i<n; i++ )
		{
			Object *obj=list[i];
			if( obj==with ) continue;
			if( obj->GetType()==Network && obj->GetShape()->GeometryLevel()<0) continue; // no collisions with roads
			if( obj->GetType()==Temporary ) continue; // no collisions with temporary
			if( obj->GetType()==TypeTempVehicle ) continue;
			if( onlyVehicles )
			{
				if( obj->Static() ) continue;
			}
			// do not collide with "soft" dead objects (man bodies, trees)
			if( !obj->HasGeometry() ) continue;
			//ObjectCollision(retVal,obj,with,withPos,prec);
			obj->Intersect(retVal,with);
		}
	}
}

struct CheckObjectCollisionContext
{
	Object *with, *ignore;
	Vector3 beg, end;
	float radius;
	ObjIntersect type;
};

void Landscape::CheckObjectCollision
(
	int x, int z, 
	CollisionBuffer &retVal,
	CheckObjectCollisionContext &context
) const
{
	if (!this_InRange(x,z)) return;
	const ObjectList &list=_objects(x,z);
	int n=list.Size();
	for( int i=0; i<n; i++ )
	{
		Object *obj=list[i];
		if( obj==context.with ) continue;
		if( obj==context.ignore ) continue;
		if( obj->GetType()==Network && obj->GetShape()->GeometryLevel()<0) continue; // no collisions with roads
		if( obj->GetType()==Temporary ) continue; // no collisions with temporary
		if( obj->GetType()==TypeTempVehicle ) continue;
		obj->Intersect(retVal,context.beg,context.end,context.radius,context.type);
	}
}

void Landscape::ObjectCollision
(
	CollisionBuffer &retVal,
	Object *with, Object *ignore, Vector3Par beg, Vector3Par end, float radius,
	ObjIntersect type
) const
{ // check with all object
	PROFILE_SCOPE(lObj2);
	int xMin,xMax,zMin,zMax;
	ObjRadiusRectangle(xMin,xMax,zMin,zMax,beg,end,radius);
	int x,z;
	// create beg-end boundingbox
	Vector3 boundingCenter=(end+beg)*0.5;
	float boundingSphere=end.Distance(beg)*0.5;
	//Log("boundingSphere %f",boundingSphere);

	#define DDA_OPTIMIZED 0
	#if DDA_OPTIMIZED
	if (boundingSphere<50)
	#endif
	{
		// line is short - perform direct sphere test 
		for( x=xMin; x<=xMax; x++ ) for( z=zMin; z<=zMax; z++ )
		{
			const ObjectList &list=_objects(x,z);
			int n=list.Size();
			for( int i=0; i<n; i++ )
			{
				Object *obj=list[i];
				if( obj==with ) continue;
				if( obj==ignore ) continue;
				if( obj->GetType()==Network && obj->GetShape()->GeometryLevel()<0) continue; // no collisions with roads
				if( obj->GetType()==Temporary ) continue; // no collisions with temporary
				if( obj->GetType()==TypeTempVehicle ) continue;
				// check if obj is in min-max of tested trajectory
				float dist2=obj->Position().Distance2Inline(boundingCenter);
				float maxDist2=Square(boundingSphere+obj->GetRadius());
				if( dist2>maxDist2 ) continue;
				obj->Intersect(retVal,beg,end,radius,type);
			}
		}
	}
	#if DDA_OPTIMIZED
	else
	{
		CheckObjectCollisionContext context;
		context.beg = beg;
		context.end = end;
		context.ignore = ignore;
		context.with = with;
		context.radius = radius;
		context.type = type;

		// if line is long, it is better to consider only some squares on the line
		// DDA-like test (see Landscape::Visible)
		//const int border=4; // how much squares are reserved
		// assume max. 50 m objects present
		int border=toIntFloor(1.5+50*_invLandGrid);

		int ox=toIntFloor(end.X()*_invLandGrid);
		int oz=toIntFloor(end.Z()*_invLandGrid);
		int sx=toIntFloor(beg.X()*_invLandGrid);
		int sz=toIntFloor(beg.Z()*_invLandGrid);
		int aox=abs(ox-sx),aoz=abs(oz-sz);
		const float sBorder=1.5;

		if( aox>aoz )
		{
			// mainly horizontal - primary coordinate is x, derived is z
			float dd=float(oz-sz)/aox;
			int pd=ox>sx ? +1 : -1;
			int p=sx-pd*border;
			float d=sz-dd*border;
			int plen=aox+2*border+1;
			for( int i=plen; --i>=0; )
			{
				int dis=toIntFloor(d-sBorder);
				int die=toIntFloor(d+sBorder);
				for( int di=dis; di<=die; di++ )
				{
					CheckObjectCollision(p,di,retVal,context);
				}
				d+=dd;
				p+=pd;
			}
		}
		else if( aoz!=0 )
		{
			// mainly vertical - primary coordinate is z, derived is x
			float dd=float(ox-sx)/aoz;
			int pd=oz>sz ? +1 : -1;
			int p=sz-pd*border;
			Assert( abs(p)<2000 );
			float d=sx-dd*border;
			int plen=aoz+2*border+1;
			for( int i=plen; --i>=0; )
			{
				int dis=toIntFloor(d-sBorder);
				int die=toIntFloor(d+sBorder);
				for( int di=dis; di<=die; di++ )
				{
					CheckObjectCollision(p,di,retVal,context);
				}
				d+=dd;
				p+=pd;
			}
		}
		else
		{

			int ssx,ssz;
			for( ssx=-border; ssx<=border; ssx++ )
			for( ssz=-border; ssz<=border; ssz++ )
			{
				CheckObjectCollision(ox+ssx,oz+ssz,retVal,context);
			}
		}
	}
	#endif



}

/*!
\param dX [out] world space ground plane differential
\param dZ [out] world space ground plane differential
\patch 1.89 Date 10/25/2002 by Ondra
- Fixed: It was impossible to climb some very steep stairways.
\patch 1.91 Date 11/30/2002 by Ondra
- Fixed: Vehicle movements slow downhill on some roads.
*/

float Landscape::UnderRoadSurface
(
	const Object *obj,
	Vector3Par modelPos, float bumpy, float *dX, float *dZ,
	Texture **texture
) const
{
	// if we are on road, return road surface parameters
	LODShape *lShape=obj->GetShape();
	// check again topplane equation
	// road top plane not correct
	//const Plane &topPlane=lShape->RoadTopPlane();
	//if( topPlane.Distance(modelPos)<0 ) return -10; // no colision is possible
	// collision is possible
	Shape *shape=lShape->RoadwayLevel();
	Assert( shape );
	//if( !shape ) shape=lShape->Level(0);
	// only one polygon should incide if we look from the top
	if( !(shape->GetOrHints()&ClipLandOn) )
	{
		float maxY=-1e10;
		float maxDX=0, maxDZ=0;
		Texture *maxTexture=NULL;
		int i=0;
		shape->InitPlanes();
		for( Offset f=shape->BeginFaces(),e=shape->EndFaces(); f<e; shape->NextFace(f),i++ )
		{
			const Poly &face=shape->Face(f);
			const Plane &plane=shape->GetPlane(i);
			// check if pos can be under face
			// check using face plane equation
			if (plane.Normal().Y()>-0.2f)
			{
				// ignore roadways that are too steep
				continue;
			}
			float y,dX,dZ;
			float under=plane.Distance(modelPos);
			// max 1 m step allowed
			#if STARS
				if (CHECK_DIAG(DECollision))
				{
					Vector3 sPos;
					LogF("ModelPos %.1f,%.1f,%.1f",modelPos[0],modelPos[1],modelPos[2]);
					sPos = modelPos;
					obj->PositionModelToWorld(sPos,sPos);
					LogF("      -- world %.1f,%.1f,%.1f",sPos[0],sPos[1],sPos[2]);
					_world->GetScene()->DrawCollisionStar(sPos,0.05,PackedColor(Color(0,0.5,0)));
					// calculate nearest point on the plane
					sPos = modelPos - plane.Normal()*under;
					LogF("ModelPos+under %.1f,%.1f,%.1f",sPos[0],sPos[1],sPos[2]);
					obj->PositionModelToWorld(sPos,sPos);
					LogF("      -- world %.1f,%.1f,%.1f",sPos[0],sPos[1],sPos[2]);
					_world->GetScene()->DrawCollisionStar(sPos,0.035,PackedColor(Color(0,0.5,0.5)));
				}
			#endif
			if( under<=-0.5f|| under>1.0f ) continue;
			//if( under<0 || under>1.0f ) continue;
			// use max test for this
			if( face.InsideFromTop(*shape,plane,modelPos,&y,&dX,&dZ) )
			{
				// the face is not sure to be a triangle
				#if STARS
					if (CHECK_DIAG(DECollision))
					{
						Vector3 sPos = modelPos;
						sPos[1] = y;
						LogF("result     %.1f,%.1f,%.1f",sPos[0],sPos[1],sPos[2]);
						obj->PositionModelToWorld(sPos,sPos);
						LogF("  -- world %.1f,%.1f,%.1f",sPos[0],sPos[1],sPos[2]);
						_world->GetScene()->DrawCollisionStar(sPos,0.025,PackedColor(Color(0.5,0.5,0)));
					}
				#endif
				if( maxY<y )
				{
					maxY=y,maxDX=dX,maxDZ=dZ;
					maxTexture=face.GetTexture();
				}
			}
		}
		if( maxY>=-1e3 )
		{
			//Vector3 retModelPos(modelPos[0],maxY,modelPos[0]);
			//modelPos[1]=maxY;
			if (dX || dZ)
			{
				// create world space normal
				// to calculate world space differential
				Vector3 normal(-maxDX,1,-maxDZ);
				obj->DirectionModelToWorld(normal,normal);
				// deduce DX,DZ from the world space normal
				if (fabs(normal.Y())>1e-2)
				{
					maxDX = -normal.X()/normal.Y();
					maxDZ = -normal.Z()/normal.Y();
				}
				else
				{
					maxDX = 0;
					maxDZ = 0;
				}

				//Point3 ret=obj->PositionModelToWorld(modelPos);
				if( dX ) *dX=maxDX;
				if( dZ ) *dZ=maxDZ;
			}
			if( texture ) *texture=maxTexture;
			return maxY-modelPos.Y();
		}
	}
	else
	{
		float minUnder = 1e10;
		float maxDX=0, maxDZ=0;
		Texture *maxTexture=NULL;
		int i=0;
		shape->InitPlanes();
		for( Offset f=shape->BeginFaces(),e=shape->EndFaces(); f<e; shape->NextFace(f),i++ )
		{
			const Poly &face=shape->Face(f);
			const Plane &plane=shape->GetPlane(i);
			// check if pos can be under face
			// check using face plane equation
			float y,dX,dZ;
			// get landscape parameters
			if( face.InsideFromTop(*shape,plane,modelPos,&y,&dX,&dZ) )
			{
				Vector3 wPos=obj->PositionModelToWorld(modelPos);
				float y=SurfaceY(wPos[0],wPos[2],&dX,&dZ);
				// use max test for this
				// max 1 m step allowed
				float under=y-wPos.Y();
				#if STARS
					if (CHECK_DIAG(DECollision))
					{
						Vector3 spos = wPos;
						spos[1] = y;
						_world->GetScene()->DrawCollisionStar(spos,0.025,PackedColor(Color(0.5,0.5,0)));
					}
				#endif
			//Log("under %f",under);
				if( under<-1.0f ) continue; // avoid checking landscape right above roadway
				// the face is not sure to be a triangle
				if( minUnder>under )
				{
					minUnder=under,maxDX=dX,maxDZ=dZ;
					maxTexture=face.GetTexture();
				}
			}
		}
		if( minUnder<1e3 )
		{
			//Vector3 retModelPos(modelPos[0],maxY,modelPos[0]);
			//modelPos[1]=maxY;
			//Point3 ret=obj->PositionModelToWorld(modelPos);
			if( dX ) *dX=maxDX;
			if( dZ ) *dZ=maxDZ;
			if( texture ) *texture=maxTexture;
			return minUnder;
		}
	}
	// otherwise return no collision
	return -10;
}

//static StaticStorage<UndergroundInfo> GroundCollisionStorage;


void Landscape::GroundCollision
(
	GroundCollisionBuffer &retVal,
	Object *with, const Frame &withPos, float above, float bumpy,
	bool enableLandcontact, bool soldier
) const
{
	PROFILE_SCOPE(lGnd);
	// changed: all vehicles can now use all roadways
	// only soldier can walk on houses and other objects
	//bool testObjects=soldier;

	Vector3Val wPos=withPos.Position();

	// check which points are under the ground level
	// for normal orientation use LandContact LOD level
	int level=with->GetShape()->FindLandContactLevel();
	if (!soldier)
	{
		// soldier uses always landcontact
		if (!enableLandcontact || level<0)
		{
			level=with->GetShape()->FindGeometryLevel();
		}
		else
		{
			float dx,dz;
			float posY=SurfaceY( wPos.X(),wPos.Z(), &dx, &dz );
			(void)posY;

			Vector3 normal(-dx,1,-dz);
			if (posY<GetSeaLevel()) normal = VUp;

			float surfaceAngle=normal.CosAngle(withPos.DirectionUp());
			if( surfaceAngle<0.86  )
			{
				level=with->GetShape()->FindGeometryLevel();
			}
		}
	}
	if( level<0 ) return;
	Shape *withShape=with->GetShape()->LevelOpaque(level);
	if (!withShape)
	{
		Fail("!withShape");
		return;
	}

	int xMin,xMax,zMin,zMax;
	float wRad=with->GetRadius();
	ObjRadiusRectangle(xMin,xMax,zMin,zMax,wPos,wPos,wRad);
	int x,z;
	// check against landscape
	// TODO: check special case: one point ground-collision level
	with->Animate(level);
	Matrix4Val withTrans=withPos.Transform();
	for( int j=0; j<withShape->NPos(); j++ )
	{
		Point3 pos(VFastTransform,withTrans,withShape->Pos(j));
		float dX,dZ;

		Texture *texture=NULL;
		float surfaceY;
		float bump = 0;
		if( bumpy>0 ) surfaceY=BumpySurfaceY(pos[0],pos[2],dX,dZ,texture,bumpy,bump);
		else surfaceY=SurfaceY(pos[0],pos[2],&dX,&dZ,&texture);
		//surfaceY += bump;
		float underLevel = surfaceY+above-pos[1];
		if( surfaceY<_seaLevelWave )
		{
			// simulate water "bump" waves
			float bumpySea=_seaLevelWave;
			if( bumpy )
			{
				float bumpX=pos[0]*0.3;
				float bumpZ=pos[2]*0.3;
				int iBumpX=toIntFloor(bumpX);
				int iBumpZ=toIntFloor(bumpZ);
				float bump00=_randGen.RandomValue(_randGen.GetSeed(iBumpX,iBumpZ));
				float bump01=_randGen.RandomValue(_randGen.GetSeed(iBumpX+1,iBumpZ));
				float bump10=_randGen.RandomValue(_randGen.GetSeed(iBumpX,iBumpZ+1));
				float bump11=_randGen.RandomValue(_randGen.GetSeed(iBumpX+1,iBumpZ+1));

				bumpX-=iBumpX; // relative in-bump coordinates
				bumpZ-=iBumpZ;
				// bilinear interpolation of bumpY
				float bump0=bump00+(bump01-bump00)*bumpX;
				float bump1=bump10+(bump11-bump10)*bumpX;
				float bump=bump0+(bump1-bump0)*bumpZ; // result in range 0 .. 1
				bumpySea-=bump*0.5;
			}

			// water on level bumpySea
			float underWater=above+bumpySea-pos[1];
			if( underWater<=0 ) continue; // above water, must be above ground
			UndergroundInfo &info=retVal.Append();
			info.texture=texture;
			info.obj = NULL;
			info.under=underWater;
			info.pos=pos;
			info.pos[1]=bumpySea; // applied directly on surface
			info.dX=0;
			info.dZ=0;
			info.vertex=j;
			info.level=level;
			info.type=GroundWater;
		}

		// Now we know we are in contact with the ground. Take bump into account.
		underLevel += bump;
		if( underLevel<=0 ) continue;
		#if 0 //STARS
			if (CHECK_DIAG(DECollision))
			{
				_world->GetScene()->DrawCollisionStar(pos,0.05,PackedColor(Color(1,1,0)));
			}
		#endif
		UndergroundInfo &info=retVal.Append();
		info.texture=texture;
		info.obj = NULL;
		info.under=underLevel;
		info.pos=pos;
		info.dX=dX;
		info.dZ=dZ;
		info.vertex=j;
		info.level=level;
		info.type=GroundSolid;
	}
	// check against all near roads
	int nLands=retVal.Size();
	for( z=zMin; z<=zMax; z++ ) for( x=xMin; x<=xMax; x++ )
	{
		//Point3 pos(xC,0,zC);
		const ObjectList &list=_objects(x,z);
		int n=list.Size();
		for( int i=0; i<n; i++ )
		{
			Object *obj=list[i];
			// check only roads in range
			LODShape *objShape=obj->GetShape();
			if( !objShape ) continue;
			int roadwayLevel=objShape->FindRoadwayLevel();

			if( roadwayLevel<0 ) continue;
			/*
			if( !testObjects && obj->GetType()!=Network )
			{
				// roadway of some objects should be used for vehicles as well
				const EntityType *type = obj->GetVehicleType();
				if (!type || !type->GetUseRoadwayForVehicles())
				{
					continue;
				}
			}
			*/
			if( obj==with ) continue;

			float oRad=obj->GetRadius();
			if( obj->Position().Distance2Inline(wPos)>Square(oRad+wRad) ) continue;

			obj->Animate(roadwayLevel);
			objShape->RoadwayLevel()->RecalculateNormalsAsNeeded();
			

			Matrix4Val fromWithToObj=obj->GetInvTransform()*withTrans;

			for( int j=0; j<withShape->NPos(); j++ )
			{
				Vector3Val wPos=withShape->Pos(j);
				// check against topplane equation
				//if( wTopPlane.Distance(wPos)<0 ) continue; // no colision is possible
				Point3 pos(VFastTransform,fromWithToObj,wPos-Vector3(0,above,0));
				//Vector3 modelPos=obj->PositionWorldToModel(pos+Vector3(0,above,0));
				float dX,dZ; // plane differential in obj model space
				Texture *texture=NULL;
				float underLevel=UnderRoadSurface(obj,pos,bumpy,&dX,&dZ,&texture);
				if( underLevel<=-1.5f ) continue;
				#if 0 //STARS
					if (CHECK_DIAG(DECollision))
					{
						Vector3 spos = obj->PositionModelToWorld(pos);
						_world->GetScene()->DrawCollisionStar(spos,0.05,PackedColor(Color(1,0,0)));
					}
				#endif
				// search if there is already some collision with the same vertex
				if (!soldier && underLevel<-0.15) continue;
				UndergroundInfo info;
				info.texture=texture;
				info.obj = obj;
				info.under=underLevel;
				info.pos=obj->PositionModelToWorld(pos);
				// dX, dZ is always world space, already normalized
				info.dX = dX;
				info.dZ = dZ;

				info.vertex=j;
				info.level=level;
				info.type=GroundSolid;
				// prefer road to deal with landscape bumps
				float preferRoad = bumpy * 0.5;
				// check with landscape collisions
				bool noAdd = false;
				for( int p=0; p<nLands; p++ )
				{
					UndergroundInfo &pInfo=retVal[p];
					if( pInfo.vertex!=j ) continue;
					// use the higher one, prefer road
					// to achieve this delete land info that is already there
					if( pInfo.under<=underLevel+preferRoad )
					{
						retVal.Delete(p);
						nLands--;
					}
					else
					{
						noAdd = true;
					}
					break;
				}
				// check with other road collisions
				for( int p=nLands; p<retVal.Size(); p++ )
				{
					UndergroundInfo &pInfo=retVal[p];
					if( pInfo.vertex!=j ) continue;
					// both are roads - use the higher one
					if( pInfo.under<=underLevel )
					{
						retVal.Delete(p);
					}
					else
					{
						noAdd = true;
					}
					break;
				}
				if (!noAdd)
				{
					retVal.Add(info);
				}
			}
			obj->Deanimate(roadwayLevel);
		}
	}
	with->Deanimate(level);

	#if STARS
		if (CHECK_DIAG(DECollision))
		{
			int i=0;
			for (; i<nLands; i++)
			{
				UndergroundInfo &info=retVal[i];
				_world->GetScene()->DrawCollisionStar(info.pos,0.05,PackedColor(Color(1,1,0)));
			}
			for (; i<retVal.Size(); i++)
			{
				UndergroundInfo &info=retVal[i];
				_world->GetScene()->DrawCollisionStar(info.pos,0.05,PackedColor(Color(1,0,0)));
			}
		}
	#endif
}

void Landscape::GroundCollisionPlane
( // faster (less acuurate) version
	GroundCollisionBuffer &retVal,
	Object *with, const Frame &withPos, float above, float bumpy,
	bool enableLandcontact
)
{
	// for normal orientation use LandContact LOD level
	int level=with->GetShape()->FindLandContactLevel();
	if( level<0 || withPos.DirectionUp().Y()<0.7 || !enableLandcontact )
	{
		level=with->GetShape()->FindGeometryLevel();
	}
	if( level<0 ) return;
	Shape *withShape=with->GetShape()->LevelOpaque(level);
	Assert( withShape );

	with->Animate(level);

	float dX,dZ;
	Texture *texture=NULL;
	// check COM-corresponding position on the ground
	Vector3 comDown = with->GetCenterOfMass();
	comDown[1] = withShape->Min().Y()+above;
	Vector3 pos=withPos.Transform().FastTransform(comDown);
	pos[1] += 0.5; // max. 0.5 m above lowest object point
	Object *obj = NULL;
	float surfaceY=RoadSurfaceY(pos,&dX,&dZ,&texture,&obj);
	// we also need to know object
	// create plane equation
	Vector3 normal(-dX,1,-dZ);
	Vector3 point(pos.X(),surfaceY+above,pos.Z());
	normal.Normalize();
#if _ENABLE_CHEATS
	if (CHECK_DIAG(DECollision))
	{
		GScene->DrawCollisionStar(pos,0.06,PackedColor(Color(1,0,0)));
		GScene->DrawCollisionStar(point,0.1,PackedColor(Color(1,0.5,0)));
	}
#endif

	Plane plane(normal,point);

	// check all object points against plane

	// check which points are under the ground level

	// check against landscape

	float waterY=GetSeaLevel();

	Matrix4Val withTrans=withPos.Transform();
	for( int j=0; j<withShape->NPos(); j++ )
	{
		//Vector3 pos(VFastTransform,withTrans,withShape->Pos(j));
		Vector3 jpos=withTrans*withShape->Pos(j);
		// handle water
		if( jpos.Y()<waterY )
		{
			UndergroundInfo &info=retVal.Append();
			info.texture=texture;
			info.obj = NULL;
			info.under=waterY-jpos.Y();
			info.pos=jpos;
			//info.pos[1]=bumpySea; // applied directly on surface
			info.pos[1]=waterY; // applied directly on surface
			info.dX=0;
			info.dZ=0;
			info.vertex=j;
			info.level=level;
			info.type=GroundWater;
		}
		//Vector3 pos=withShape->Pos(j);

		float underLevel=-plane.Distance(jpos);

		underLevel+=CalculateBump(jpos.X(),jpos.Z(),texture,bumpy);
		//LogF("Under %.3f (pos %.1f,%.1f,%.1f)",underLevel,jpos[0],jpos[1],jpos[2]);

		if( underLevel<=0 ) continue;
		#if 0 //STARS
			_world->GetScene()->DrawCollisionStar(jpos);
		#endif
		UndergroundInfo &info=retVal.Append();
		info.texture=texture;
		info.obj = obj;
		info.under=underLevel;
		info.pos=jpos;
		info.dX=dX;
		info.dZ=dZ;
		info.vertex=j;
		info.level=level;
		info.type=GroundSolid;
		//retVal.Add(info);
	}
	with->Deanimate(level);
}

/*!
\patch 1.92 Date 7/10/2003 by Jirka
- Fixed: Crashes in MP when somebody use ammunition which does not exist on other computers (for example through mods)
*/

void Landscape::ExplosionDammageEffects
(
	EntityAI *owner, Shot *shot,
	Object *directHit, Vector3Par pos, Vector3Par dir, const AmmoType *type,
	bool enemyDammage
)
{
  if (!type) return;

	// enemyDammage - some enemy was dammaged, reveal actual unit side
	
	AIUnit *ownerUnit = owner ? owner->CommanderUnit() : NULL;
	AIGroup *ownerGroup = ownerUnit ? ownerUnit->GetGroup() : NULL;
	AICenter *ownerCenter = ownerGroup ? ownerGroup->GetCenter() : NULL;

	// perform hit sound
	float rndFreq=GRandGen.RandomValue()*0.1+0.95;
	// select sound based on 
	const RandomSound *hitSounds=&type->_hitGround;
	if( directHit )
	{
		hitSounds=&type->_hitBuilding;
		// different hit types for different objects
		EntityAI *hitAI=dyn_cast<EntityAI>(directHit);
		if( hitAI )
		{
			const VehicleType *typeAI=hitAI->GetType();
			if( typeAI->IsKindOf(GWorld->Preloaded(VTypeMan)) )
			{
				hitSounds=&type->_hitMan;
			}
			else if( typeAI->IsKindOf(GWorld->Preloaded(VTypeAllVehicles)) )
			{
				hitSounds=&type->_hitArmor;
			}
		}
	}
	const SoundPars &pars=hitSounds->SelectSound(GRandGen.RandomValue());
	if( pars.name.GetLength()>0 )
	{
		AbstractWave *sound=GSoundScene->OpenAndPlayOnce
		(
			pars.name,pos,VZero,
			pars.vol,pars.freq*rndFreq
		);
		if( sound )
		{
			GSoundScene->SimulateSpeedOfSound(sound);
			GSoundScene->AddSound(sound);
		}
	}

	// small/big explosion
	bool smallExplosion=!type->explosive;
	bool water = pos.Y() < _seaLevelWave + 0.01;
	const float craterTimeCoef=5;
	if( smallExplosion )
	{
		// crater visible - draw it
		float landAlpha=floatMin(type->hit*(1.0/100),1);
		float scale=landAlpha*0.2*(GRandGen.RandomValue()*0.3+0.8);
		float alpha=10*landAlpha*(GRandGen.RandomValue()*0.3+0.7);
		saturateMin(alpha,1);
		saturateMin(scale,0.05);

		float timeToLive=60*scale;
		timeToLive*=craterTimeCoef;
		saturate(timeToLive,10,30);

		if( directHit )
		{
			#if 1
			// TODO: do not draw crater on men
			//LODShapeWithShadow *shape=GLOB_SCENE->Preloaded(CraterBullet);
			LODShapeWithShadow *shape=NULL;
			//LODShapeWithShadow *shape=GLandscape->NewShape("data3d\\colision.p3d",false,false);


			// crater on object/vehicle
			// note: directHit may be a part of some hierarchy

			Vehicle *vehicleHit=dyn_cast<Vehicle>(directHit);
			Matrix4Val toModel=directHit->WorldInvTransform();
			Vector3 rPos=toModel.FastTransform(pos);
			// TODO: find nearest face of finest LOD and use its normal
			// dir is relative to directHit object
			//LogF("Dir %.1f,%.1f,%.1f",dir[0],dir[1],dir[2]);
			Vector3 rDir=dir;
			Crater *crater=NULL;
			if( vehicleHit )
			{
				CraterOnVehicle *cov = new CraterOnVehicle
				(
					shape,VehicleTypes.New("crateronvehicle"),timeToLive,scale*0.5,
					directHit,rPos,rDir
				);
				crater = cov;
			}
			else
			{
				crater=new Crater(shape,VehicleTypes.New("crater"),timeToLive,scale*0.5, false, false, water);

				Matrix4 transform;
				Matrix4 toWorld = directHit->WorldTransform();
				Vector3Val wDir=toWorld.Rotate(rDir);
				Vector3 pDir=fabs(wDir.Y())<0.9 ? VUp : VForward;
				transform.SetDirectionAndUp(wDir,pDir);
				transform.SetScale(scale*0.5);
				Vector3 wPos=toWorld.FastTransform(rPos)-wDir*0.05;
				transform.SetPosition(wPos);
				crater->SetTransform(transform);
			}
			crater->SetAlpha(alpha);

			GLOB_WORLD->AddAnimal(crater);
			//GetNetworkManager().CreateVehicle(crater, VLTAnimal, "", -1);
			#endif
		}
		else
		{
			//LODShapeWithShadow *shape=GLandscape->NewShape("data3d\\colision.p3d",false,false);
			LODShapeWithShadow *shape=GLOB_SCENE->Preloaded(CraterShell);
			// crater on ground
			float azimut=GRandGen.RandomValue()*H_PI*2;

			Matrix4 transform(MIdentity);
			transform.SetOrientation(Matrix3(MRotationY,azimut));
			transform.SetScale(scale*0.5);

			transform.SetPosition(pos);

			// create vehicle
			Crater *crater=new Crater(shape,VehicleTypes.New("crater"),timeToLive,scale*0.5, false, false, water);
			//Crater *crater=new Crater(shape,30,1);
			crater->SetTransform(transform);
			crater->SetAlpha(alpha);
			GLOB_WORLD->AddAnimal(crater);
			//GetNetworkManager().CreateVehicle(crater, VLTAnimal, "", -1);
		}
	}
	else
	{
		float maxY=floatMax(type->indirectHitRange*2,0.5);
		float surfY=RoadSurfaceY(pos);
		float y=pos[1]-surfY;
		if( y<maxY )
		{
			// place explosion crater on the ground
			float hit=floatMax(type->indirectHit * type->indirectHitRange, 5);
			float landAlpha=(1-y/maxY)*hit*(1.0/200);
			//if( landAlpha>0.05 )
			if( landAlpha>0.001 )
			{
				// crater visible - draw it
				saturateMin(landAlpha, 2);
				// use smoke simulation for drawing
				float scale = landAlpha*(GRandGen.RandomValue()*0.3+0.8);
				float alpha = 0.5*landAlpha*(GRandGen.RandomValue()*0.3+0.7);
				float azimut = GRandGen.RandomValue()*H_PI*2;
				Matrix4 transform(MIdentity);
				transform.SetOrientation(Matrix3(MRotationY,azimut));
				transform.SetScale(scale*0.5);
				LODShapeWithShadow *shape=GLOB_SCENE->Preloaded(CraterShell);
				// small/big explosion

				Vector3 offset=transform.Rotate(shape->BoundingCenter());
				transform.SetPosition(Vector3(pos[0],surfY,pos[2])+offset);

				float timeToLive=scale*60;
				timeToLive*=craterTimeCoef;
				saturateMin(timeToLive, 120);

				// create vehicle
//LogF("Crater: ammo %s, hit %.0f, y %.3f, landAlpha %.3f, scale %.3f", (const char *)type->GetName(), hit, y, landAlpha, scale);
				Crater *crater=new Crater(shape,VehicleTypes.New("crater"),timeToLive,scale*0.5, true, false, water);
				crater->SetTransform(transform);
				crater->SetAlpha(alpha);
				GLOB_WORLD->AddAnimal(crater);
				//GetNetworkManager().CreateVehicle(crater, VLTAnimal, "", -1);

				#if 1
				// check if eplosion is near the camera
				float distance2ToCamera = GScene->GetCamera()->Position().Distance2(transform.Position());
				if (distance2ToCamera<Square(500))
				{
					int count = toIntFloor(landAlpha*10);
					while (--count>=0)
					{
						// some ground effects flying out
						Vector3 vel
						(
							(GRandGen.RandomValue()*15-7.5)*landAlpha,
							(GRandGen.RandomValue()*10)*landAlpha,
							(GRandGen.RandomValue()*15-7.5)*landAlpha
						);
						Vector3 posOffset
						(
							(GRandGen.RandomValue()*2-1)*scale,
							(GRandGen.RandomValue()*1)*scale,
							(GRandGen.RandomValue()*2-1)*scale
						);
						Matrix4 fxTrans = transform;
						// random orientation
						fxTrans.SetOrientation
						(
							Matrix3(MRotationX,GRandGen.RandomValue()*(2*H_PI))*
							Matrix3(MRotationY,GRandGen.RandomValue()*(2*H_PI))
						);
						fxTrans.SetPosition(transform.Position()+posOffset);

						ThingEffectKind kind = dyn_cast<Transport>(directHit) ? TEArmor: TEGround;
						Entity *fx = CreateThingEffect(kind,fxTrans,vel);
						if (fx)
						{
							// perform additional effects on fx (set angular momentum)
						}
					}
				}
				#endif

			}
		}
	}


	if( ownerCenter )
	{
		// if fire is visible, disclose position
		// otherwise assume explosion position
		float maxDist=type->hit*0.5f+type->indirectHit*type->indirectHitRange*0.5f;
		float maxRange = floatMax(300,TACTICAL_VISIBILITY);
		saturate(maxDist,3,maxRange);
		// if fire is visible, dislose owners position
		Disclose(owner,pos,maxDist,enemyDammage,type->visibleFire>1);
	}
}

/*!
\patch 1.45 Date 2/23/2002 by Ondra
- Fixed: Vehicles explosions caused no dammage to near soldiers / objects.
*/

void Landscape::ExplosionDammage
(
	EntityAI *owner, Shot *shot,
	Object *directHit, Vector3Par pos, Vector3Par dir, const AmmoType *type
)
{
	//DoAssert (shot);

	//ADD_COUNTER(explo,1);
	bool enemyDammage=false; // detect if we dammaged some enemy unit
	
	AIUnit *ownerUnit = owner ? owner->CommanderUnit() : NULL;
	AIGroup *ownerGroup = ownerUnit ? ownerUnit->GetGroup() : NULL;
	AICenter *ownerCenter = ownerGroup ? ownerGroup->GetCenter() : NULL;

	const float impulseKinScale=1;
	const float impulseExplScale=600;
	// simulate direct hit
	if( directHit )
	{
		float hitVal=type->hit-type->indirectHit;

		directHit->DirectDammage(shot,owner,pos,hitVal);
		// if directHit is vehicle, add force impulse
		Vehicle *veh=dyn_cast<Vehicle>(directHit);
		if( veh && shot )
		{
			if( ownerCenter && ownerCenter->IsEnemy(veh->GetTargetSide()) )
			{
				enemyDammage=true;
			}
			float factor=veh->GetShape()->GeometrySphere()*(1.0/6);
			Vector3 relPos=pos-veh->COMPosition();
			Vector3 forcePos=relPos+Vector3
			(
				GRandGen.RandomValue()*2*factor-factor,
				GRandGen.RandomValue()*2*factor-factor,
				GRandGen.RandomValue()*2*factor-factor
			);
			// calculate force
			// direct hit - transfer all intertia plus direct explosion
			Vector3 force = shot->Speed()*(shot->GetMass()*impulseKinScale);
			// note: some ammo is not explosive
			// this applied especially for bullets
			if (type->explosive)
			{
				force += relPos.Normalized()*(hitVal*factor*-impulseExplScale);
			}
			veh->AddImpulseNetAware(force,forcePos.CrossProduct(force));
		}
	}
	// dammage all objects in maxRadius
	// dammage drops with the square of distance
	const float maxRadius=type->indirectHitRange*4;
	const float hit=type->indirectHit;
	int xMin,xMax,zMin,zMax;
	#if 0
	LogF
	(
		"%s: Indirect dammage max range %.2f, dammage %.1f",
		(const char *)type->GetName(),
		maxRadius,hit
	);
	#endif
	ObjRadiusRectangle(xMin,xMax,zMin,zMax,pos,pos,maxRadius);
	int x,z;
	for( x=xMin; x<=xMax; x++ ) for( z=zMin; z<=zMax; z++ )
	{
		const ObjectList &list=_objects(x,z);
		int n=list.Size();
		for( int i=0; i<n; i++ )
		{
			Object *obj=list[i];
			if( !obj->GetShape() ) continue;
			if( obj->GetType()==Temporary ) continue;
			if( obj->GetType()==TypeTempVehicle ) continue;
			if (obj==shot) continue;
			float dist2=obj->Position().Distance2(pos);
			float maxDist2=Square(maxRadius+obj->GetRadius());
			/*
			LogF
			(
				"%s test range %.2f, range %.2f",
				(const char *)obj->GetDebugName(),sqrt(maxDist2),sqrt(dist2)
			);
			*/
			if( dist2>maxDist2 ) continue;
			// obj is probably effected by an explosion
			#if 0
			LogF
			(
				"%s: Explosion dammage %.2f range %.1f dist %.1f",
				(const char *)obj->GetDebugName(),
				hit,type->indirectHitRange,sqrt(dist2)
			);
			#endif
			// check direct visibility line between obj and pos
			float unshielded = 1;
			if (obj!=directHit)
			{
				// no shield checking on short distance
				float objectSize=obj->VisibleSizeRequired()*0.8;
				unshielded = Visible
				(
					pos,obj->Position(),objectSize,directHit,obj,ObjIntersectIFire
				);
				#if 0
				LogF
				(
					"%s unshielded %.2f, directHit %s, owner %s, shot %s",
					(const char *)obj->GetDebugName(),unshielded,
					directHit ? (const char *)directHit->GetDebugName() : "<null>",
					owner ? (const char *)owner->GetDebugName() : "<null>",
					shot ? (const char *)shot->GetDebugName() : "<null>"
				);
				#endif
			}
			/*
			if (unshielded<1)
			{
				LogF("%s unshielded %.2f",(const char *)obj->GetDebugName(),unshielded);
			}
			*/

			float unshieldedHit = hit*unshielded; 

			obj->IndirectDammage(shot,owner,pos,unshieldedHit,type->indirectHitRange);
			Vehicle *veh=dyn_cast<Vehicle>(obj);
			if( veh && shot )
			{
				if( ownerCenter && ownerCenter->IsEnemy(veh->GetTargetSide()) )
				{
					enemyDammage=true;
				}
				float factor=veh->GetShape()->GeometrySphere()*(1.0/6);
				Vector3 relPos=pos-veh->COMPosition();
				Vector3 forcePos=relPos+Vector3
				(
					GRandGen.RandomValue()*2*factor-factor,
					floatMin(0,-GRandGen.RandomValue()*2*factor-factor), // cause force to lead up
					GRandGen.RandomValue()*2*factor-factor
				);
				// calculate force
				float relDist = relPos.SquareSize();
				float minRelDist = Square(type->indirectHitRange);
				float hitFactor = relDist>minRelDist ? minRelDist/relDist : 1;
				float hitVal = unshieldedHit*hitFactor;
				// indirect hit - only explosion
				Vector3 force = relPos.Normalized()*(hitVal*factor*-impulseExplScale);
				// random explosion momentum
				veh->AddImpulseNetAware(force,forcePos.CrossProduct(force));
			}
		}
	}	

	ExplosionDammageEffects
	(
		owner,shot,directHit,pos,dir,type,enemyDammage
	);
	GetNetworkManager().ExplosionDammageEffects
	(
		owner,shot,directHit,pos,dir,type,enemyDammage
	);
}

#include <Es/Containers/staticArray.hpp>

void Landscape::Disclose
(	
	EntityAI *owner, Vector3Par pos, float maxDist,
	bool discloseSide, bool disclosePosition
)
{
	AUTO_STATIC_ARRAY(Ref<AIGroup>,disclose,32);

	// tell all near enemy vehicles they are disclosed
	if (!owner)
	{
		// owner may not exist (for example mines from mission editor)
		LogF("No owner");
		return;
	}
	AIUnit *ownerUnit=owner->CommanderUnit();
	if( !ownerUnit ) return;
	AIGroup *ownerGroup=ownerUnit->GetGroup();
	if( !ownerGroup ) return;

	#if 0
	LogF
	(
		"Disclose %s, maxDist %g, disclosePosition %d",
		(const char *)owner->GetDebugName(),maxDist,disclosePosition
	);
	#endif
	AICenter *myCenter=ownerGroup->GetCenter();
	// 
	int xMin,xMax,zMin,zMax;
	ObjRadiusRectangle(xMin,xMax,zMin,zMax,pos,pos,maxDist);
	for( int x=xMin; x<=xMax; x++ ) for( int z=zMin; z<=zMax; z++ )
	{
		const ObjectList &list=_objects(x,z);
		int n=list.Size();
		for( int i=0; i<n; i++ )
		{
			Object *obj=list[i];
			if( obj->GetType()!=TypeVehicle ) continue;
			EntityAI *veh=dyn_cast<EntityAI>(obj);
			if( !veh ) continue;
			if( veh->Position().Distance2(pos)>Square(maxDist) ) continue;
			AIUnit *vehBrain=veh->CommanderUnit();
			if( vehBrain && vehBrain->GetLifeState()==AIUnit::LSAlive)
			{
				AIGroup *vehGroup=vehBrain->GetGroup();
				if( vehGroup && myCenter!=vehGroup->GetCenter() )
				{
					if( myCenter->IsEnemy(veh->GetTargetSide()) )
					{
						disclose.Add(vehGroup);
						vehBrain->Disclose(false);
						if (veh->PilotUnit() && veh->PilotUnit() != vehBrain)
							veh->PilotUnit()->Disclose(false);
						if (veh->GunnerUnit() && veh->GunnerUnit() != vehBrain)
							veh->GunnerUnit()->Disclose(false);
					}
					// add owner to group sensor list
					float delay = 3.0f*veh->GetInvAbility();
					if (vehBrain->GetTargetAssigned())
					{
						// vehicle already busy
						delay *= 2;
					}
					#if 0
					LogF
					(
						"  Disclosed by %s (dist %g)",
						(const char *)veh->GetDebugName(),veh->Position().Distance(pos)
					);
					#endif

					float accuracy = 0.1f;
					float sideAcc = discloseSide ? 1.5f : 0.1f;
					if( disclosePosition )
					{
						vehGroup->AddTarget(owner,accuracy,sideAcc,delay);
					}
					else
					{
						// assume owner is at explosion position 
						vehGroup->AddTarget(owner,accuracy,sideAcc,delay,&pos);
					}
				}
			}
		}
	}
	// first disclose units, then disclose groups
	// this guarantees all units are disclosed
	// it has to be done this way - group Disclose set Combat behaviour,
	// which prevents units doing Danger reaction
	for( int i=0; i<disclose.Size(); i++ )
	{
		AIGroup *vehGroup=disclose[i];
		vehGroup->Disclose(NULL);
	}
}


static void NearestPoint
(
	float &t1, float &t2,
	Vector3Val b1, Vector3Val d1,
	Vector3Val b2, Vector3Val d2
)
{
	// find nearest point on lines 1 and 2
	// note: this function is not tested, results may be unreliable
	// idea:
	// search for pt1, pt2 so that line pt1,pt2 is perpendicular to both
	// (.)
	// p1=b1+d1*t1
	// p2=b2+d2*t2
	// (p1-p2)*d1=0
	// (p1-p2)*d2=0
	// (.)
	// b=b1-b2
	// (b+d1*t1-d2*t2)*d1=0
	// (b+d1*t1-d2*t2)*d2=0
	// (.)
	// (b*d1)+(d1*d1)*t1-(d2*d1)*t2=0
	// (b*d2)+(d1*d2)*t1-(d2*d2)*t2=0
	// (.)
	// t2=(b*d2+d1*d2*t1)/(d2*d2)
	// (b*d1)*(d2*d2)+(d1*d1)*(d2*d2)*t1-(d2*d1)*(d2*d2)*t2=0
	// (.)
	// (b*d1)*(d2*d2)+(d1*d1)*(d2*d2)*t1-(d2*d1)*((b*d2)+(d1*d2)*t1)=0
	// (.)
	// (b*d1)*(d2*d2)+(d1*d1)*(d2*d2)*t1-(d2*d1)*(b*d2)-(d2*d1)*(d1*d2)*t1=0
	// (.)
	// ((d1*d1)*(d2*d2)-(d2*d1)*(d1*d2))*t1=(d2*d1)*(b*d2)-(b*d1)*(d2*d2)
	Vector3 b=b1-b2;
	float d2d2=d2*d2;
	float d1d1=d1*d1;
	float d1d2=d1*d2;
	float denom=d1d1*d2d2-d1d2*d1d2;
	float bd2=b*d2;
	float bd1=b*d1;
	t1=t2=0;
	if( fabs(denom)>1e-5 ) // for parallel any t1 will do
	{ // not parallel
		t1=(d1d2*bd2-bd1*d2d2)/denom;
	}
	if( d2d2>1e-5 )
	{ // t2 can be solved
		t2=(bd2+d1d2*t1)/d2d2;
	}
}

static float NearestPoint
(
	Vector3Val b1, Vector3Val d1,
	Vector3Val b2, Vector3Val d2
)
{
	Vector3 b=b1-b2;
	Vector3 d=d1-d2;
	// search for t so that |b+d*t| is minimal
	// min ( b + d*t )*( b + d*t )
	// (.)
	// sure: this functions has one local minimum, no local maximum
	// the point with derivation = 0 is our solution
	// f(x)=( b + d*t )*( b + d*t )
	// f(x)=( b + d*t )*b + (b + d*t)*d*t
	// f(x)=( b*b + b*d*t ) + (b*d*t + d*t*d*t)
	// f(x)=( b*b + 2*b*d*t + d*d*t*t)
	// f'(x)=2*(b*d) + 2*(d*d)*t
	// (.)
	// 2*(b*d) + 2*(d*d)*t =0
	// t = -(b*d)/(d*d)
	float denom=d*d;
	if( denom>1e-5 ) return -(b*d)/denom;
	return 0;
}


void Landscape::PredictCollision
(
	VehicleCollisionBuffer &ret,
	const Vehicle *vehicle, float maxTime, float gap, float maxDistance
) const
{
	PROFILE_SCOPE(lPred);
	//float posDistance=1e10; // distance for pos point
	// TODO: allow exclusions
	// exclusions are required for objects like bridges or gas-stations
	// predict collision with all near objects and vehicles
	// use bounding sphere test only
	float vRadius=vehicle->CollisionSize()*0.8;
	//Vector3Val beg=vehicle->Position();
	float maxDist2=vehicle->Speed().SquareSize()*Square(maxTime);
	saturateMax(maxDist2,Square(maxDistance));
	//Vector3 end=vehicle->Position()+vehicle->Speed()*maxTime;

	float radius = maxDist2*InvSqrt(maxDist2);
	int xMin,xMax,zMin,zMax;
	Vector3Val vehPos = vehicle->Position();
	ObjRadiusRectangle(xMin,xMax,zMin,zMax,vehPos,radius);
	for (int x=xMin; x<=xMax; x++) for (int z=zMin; z<=zMax; z++)
	{
		const ObjectListFull *list = _objects(x,z).GetList();
		if (!list || list->GetNonStaticCount()<=0) continue; // no vehicles in this list
		for (int i=0; i<list->Size(); i++)
		{
			const Object *obj = list->Get(i);
			if (obj->GetType()!=TypeVehicle) continue;
			if (obj->Position().Distance2Inline(vehPos)>maxDist2) continue;
			const EntityAI *ai = dyn_cast<const EntityAI>(obj);
			if (!ai) continue;

	/*
	for( int i=0; i<GWorld->NVehicles(); i++ )
	{
		//const Object *obj=list[i];
		const Vehicle *obj=GWorld->GetVehicle(i);
		// normal object (primary)
		if( obj->GetType()!=TypeVehicle && obj->GetType()!=Primary ) continue;
		if( obj->Position().Distance2Inline(vehicle->Position())>maxDist2 ) continue;
		*/
		if( !obj->GetShape() ) continue;
		if( !obj->GetShape()->GeometryLevel() ) continue;
		if( !obj->HasGeometry() ) continue;
		// special test for vehicle collisions
		// run prediction loop
		if( obj==vehicle ) continue;
		// find analytical solution
		float minTime=NearestPoint
		(
			vehicle->Position(),vehicle->Speed(),
			obj->Position(),obj->ObjectSpeed()
		);
		saturate(minTime,0,maxTime);
		Vector3 pt1=vehicle->Position()+vehicle->Speed()*minTime;
		Vector3 pt2=obj->Position()+obj->ObjectSpeed()*minTime;
		float minDist2=pt1.Distance2Inline(pt2);

		if( minDist2<Square(maxDistance) )
		{
			float cRadius=obj->CollisionSize();
			Vector3Val norm = (obj->Position()-vehicle->Position());
			Vector3 relDir=norm.Normalized();
			float relSpeed = relDir*(vehicle->Speed()-obj->ObjectSpeed());
			// if relative speed is small, there is no need to have big gap?
			float iGap=Interpolativ(relSpeed,0,10,gap*0.2,gap);
			float sumR=vRadius+cRadius+iGap;
			// search for nearest collision
			float minDist=sqrt(minDist2);

			if( minDist<sumR )
			{
				/*
				if( (Vehicle *)vehicle==GWorld->CameraOn() )
				{
					LogF
					(
						"minDist %s,%s: %.2f, max %.2f, time %.3f, rad %.1f,%.1f",
						(const char *)vehicle->GetDebugName(),
						(const char *)obj->GetDebugName(),
						sqrt(minDist2),maxDistance,
						minTime,vRadius,cRadius
					);
				}
				*/

				VehicleCollision &info=ret.Append();
				// calculate collision point
				info.pos=pt2;
				info.who=ai;
				info.distance=minDist-vRadius-cRadius;
				info.time=minTime;
			}
		}
	}
	}
}

// TODO: move InterpolativC into global declarations

inline float InterpolativC
(
	float control, const float cMin, const float cMax,
	const float vMin, float vMax
)
{
	if( control<cMin ) return vMin;
	if( control>cMax ) return vMax;
	return (control-cMin)*(1/(cMax-cMin))*(vMax-vMin)+vMin;
}

struct VisCheckContext
{
	const Object *skip1,*target;
	Vector3 sensorPos;
	Vector3 objectPos;
	Vector3 lineDist;
	Vector3 lineDir;
	float lineSize;
	float objectSize;
	float objectSeem;
	float objVis;
};

/*!
\patch 1.27 Date 10/12/2001 by Ondra.
- Improved: visibility through bushes and other semitransparent object more accurate.
\patch_internal 1.27 Date 10/18/2001 by Ondra.
- Fixed: AI can see through fully opaque objects (since 1.27).
\patch 1.28 Date 10/22/2001 by Ondra.
- Improved: bushes were too easy to see through for AI.
*/

bool Landscape::CheckVisibility
(
	int x, int z, VisCheckContext &context, ObjIntersect isect
) const
{
	// note: return value is never used
	if( !this_InRange(x,z) ) return false;
	const ObjectListFull *list=_objects(x,z).GetList();
	if (!list) return false;
	int no=list->Size();
	ADD_COUNTER(viTst,no);
	for( int oi=0; oi<no; oi++ )
	{
		Object *obstacle=list->Get(oi);
		LODShape *obstacleShape=obstacle->GetShape();
		if( !obstacleShape ) continue;
		if (isect==ObjIntersectFire || isect==ObjIntersectIFire)
		{
			if( !obstacle->OcclusionFire() ) continue;
		}
		else
		{
			if( !obstacle->OcclusionView() ) continue;
		}
		if( obstacle==context.skip1 || obstacle==context.target ) continue;
		if( obstacle->GetType()==TypeTempVehicle ) continue;
		if( obstacle->GetType()==Network && obstacleShape->GeometryLevel()<0) continue;
		if( obstacle->GetDestructType()==DestructTree && obstacle->IsDestroyed() ) continue;
		Vector3Val obstaclePos=obstacle->VisiblePosition();
		// calculate distance of obj from object..sensor line
		float t=context.lineDir * (obstaclePos-context.sensorPos);
		saturate(t,0.01,context.lineSize);
		Vector3 nearest=t*context.lineDir+context.sensorPos;
		float dist2=obstaclePos.Distance2Inline(nearest);
		float obstacleSize=obstacle->VisibleSize();
		// check object distance from line
		if( dist2>Square(obstacleSize) ) continue;
		// some obstacles may be ignored - this is used for laser target designation
		if (context.target && context.target->IgnoreObstacle(obstacle,isect)) continue;
		// object is near - may intersect
		CollisionBuffer result;
		obstacle->Intersect(result,context.objectPos,context.sensorPos,0,isect);
		if( result.Size()>0 )
		{
			float visibleThrough = 1;
			if (isect==ObjIntersectIFire)
			{
				// indirect fire dammage
				// check if we can fire through - check object armor / distance through
				// sum all object travel distance along the line
				float distanceThrough = 0;
				for (int i=0; i<result.Size(); i++)
				{
					 distanceThrough += result[i].dirOut.Size();
				}
				float obstacleLogArmor = obstacle->GetLogArmor();

				// armor protection function is:
				// c^distanceThrough, where c is given by object armor
				//float unshielded = pow(1/obstacleArmor,distanceThrough);
				float unshielded = exp(-obstacleLogArmor*distanceThrough)*3;
				// calculate armor density
				/*
				LogF
				(
					"%s: distanceThrough %.3f, armor %.3f, unshielded %.3f",
					(const char *)obstacle->GetDebugName(),
					distanceThrough,obstacle->GetArmor(),unshielded
				);
				*/

				visibleThrough = floatMin(unshielded,1);

			}
			else if (isect==ObjIntersectFire)
			{
				// direct fire dammage
				visibleThrough = 0;
				#if DIAG_VISIBLE_THROUGH
					float distanceThrough = 0;
					for (int i=0; i<result.Size(); i++)
					{
						 distanceThrough += result[i].dirOut.Size();
					}
					LogF
					(
						"  fire obstacle, through %.2f",
						distanceThrough
					);
				#endif
			}
			else 
			{
				// normal visibility test
				float obstacleAlpha=obstacleShape->Color().A8()*(1.0f/255);
				// make exponential fog of obstacle alpha density
				// check if we can see through
				if (obstacleAlpha>0.975f*0.5f)
				{
					visibleThrough = 0;
					#if DIAG_VISIBLE_THROUGH
					if (context.skip1 && context.target)
					{
						float distanceThrough = 0;
						for (int i=0; i<result.Size(); i++)
						{
							 distanceThrough += result[i].dirOut.Size();
						}
						LogF
						(
							"  %s to %s: %s, solid, obstacleAlpha %.3f, through %.2f",
							(const char *)context.skip1->GetDebugName(),
							(const char *)context.target->GetDebugName(),
							(const char *)obstacleShape->Name(),
							obstacleAlpha,distanceThrough
						);
					}
					#endif
				}
				else
				{
					// consider object alpha
					// sum all object travel distance along the line
					float distanceThrough = 0;
					for (int i=0; i<result.Size(); i++)
					{
						 distanceThrough += result[i].dirOut.Size();
					}
					if (distanceThrough>0 && obstacleAlpha>0)
					{
						float viewDensity = obstacle->ViewDensity();
						float vtLog = viewDensity*distanceThrough;
						if (vtLog<=-4.6) // -4.6 gives 0.01 visibility
						{
							visibleThrough=0;
							#if DIAG_VISIBLE_THROUGH
							if (context.skip1 && context.target)
							{
								LogF
								(
									"  %s to %s: %s, opaque, distance %.1f, viewDensity %.2f",
									(const char *)context.skip1->GetDebugName(),
									(const char *)context.target->GetDebugName(),
									(const char *)obstacleShape->Name(),
									distanceThrough,viewDensity
								);
							}
							#endif
						}
						else
						{
							visibleThrough = exp(vtLog);
							saturateMin(visibleThrough,1);
							#if DIAG_VISIBLE_THROUGH
							if (context.skip1 && context.target)
							{
								LogF
								(
									"  %s to %s: %s, distance %.1f, visibleThrough %.2f, dens %.2f, a %.2f",
									(const char *)context.skip1->GetDebugName(),
									(const char *)context.target->GetDebugName(),
									(const char *)obstacleShape->Name(),
									distanceThrough,visibleThrough,viewDensity,obstacleAlpha
								);
							}
							#endif
						}
					}
				}
			}
			context.objVis *= visibleThrough;
			// note: multiplication is commutative and asociative
			if( context.objVis<=0.025 )
			{
				context.objVis=0;
				return true; // invisible
			}
		}

	}
	return false;
}


float Landscape::CheckUnderLand
(
	Vector3Par beg, Vector3Par dir, float tMin, float tMax, int x, int z
) const
{

	// beg+tMin*dir and beg+tMax*dir must be in square (x,z)

	float y00,y01,y10,y11;
	#if !USE_SWIZZLED_ARRAYS
		if( !this_TerrainInRange(z,x) || !this_TerrainInRange(z+1,x+1) )
		{
			y00=y01=y10=y11=-100;
		}
		else
		{
			y00=GetData(x,z);
			y01=GetData(x+1,z);
			y10=GetData(x,z+1);
			y11=GetData(x+1,z+1);
		}
	#else
		if( !this_TerrainInRange(z,x) || !this_TerrainInRange(z+1,x+1) ) return false; // no bump outside landscape
		RawType y4[2][2];
		_data.GetFour(y4,x,z);
		y00=RawToHeight(y4[0][0]);
		y01=RawToHeight(y4[0][1]);
		y10=RawToHeight(y4[1][0]);
		y11=RawToHeight(y4[1][1]);
	#endif

	Vector3 begPoint = beg+tMin*dir;
	Vector3 endPoint = beg+tMax*dir;

	// draw diagnostic line
	#if DIAG

		Ref<Object> obj = DrawDiagLine
		(
			begPoint,endPoint,PackedColor(Color(1,1,0,1))
		);
		const_cast<Landscape *>(this)->ShowObject(obj);

	#endif

	#if DIAG_LAND
	LogF
	(
		"Check %.1f,%.1f,%.1f to %.1f,%.1f,%.1f (t = %.1f,%.1f)",
		begPoint[0],begPoint[1],begPoint[2],
		endPoint[0],endPoint[1],endPoint[2],
		tMin,tMax
	);
	#endif
	float maxSurfY = floatMax( floatMax(y00,y01), floatMax(y10,y11) );
	float minLineY = floatMin( begPoint.Y(), endPoint.Y() );
	if (minLineY>maxSurfY )
	{
		#if DIAG_LAND
		LogF("  minmax failed");
		#endif
		return 0; // no intersection guaranteeed
	}
	// each face is divided to two triangles
	// determine which triangle contains point
	Vector3 normal1;
	Vector3 normal2;
	//
	//bool isInA=( xIn<=1-zIn );
	// triangle 00,01,10
	normal1.Init();
	normal2.Init();
	normal1[0]=(y01-y00)*-_invTerrainGrid;
	normal1[1]=1;
	normal1[2]=(y10-y00)*-_invTerrainGrid;
	// triangle 01,10,11
	normal2[0]=(y11-y10)*-_invTerrainGrid;
	normal2[1]=1;
	normal2[2]=(y11-y01)*-_invTerrainGrid;
	// 
	Vector3 point((x+1)*_terrainGrid,y01,z*_terrainGrid);
	// size of normal1 and normal2 is nearly 1
	Plane plane1(normal1,point);
	Plane plane2(normal2,point);

	// calculate intersection with plane under point pos
	// distance above is positive
	// note: each of beg point, end point may be in any of plane1, plane2
	// check which plane is beg 1
	Plane *begPlane, *endPlane;

	{
		// check if beg in triangle 1
		float xIn=begPoint.X()*_invTerrainGrid-x; // relative 0..1 in square
		float zIn=begPoint.Z()*_invTerrainGrid-z;
		begPlane = xIn<=1-zIn ? &plane1 : &plane2;
	}

	{
		// check if end in triangle 1
		float xIn=endPoint.X()*_invTerrainGrid-x; // relative 0..1 in square
		float zIn=endPoint.Z()*_invTerrainGrid-z;
		endPlane = xIn<=1-zIn ? &plane1 : &plane2;
	}

	float dist1 = -begPlane->Distance(begPoint);
	float dist2 = -endPlane->Distance(endPoint);
	#if DIAG_LAND
	LogF("  Dist %.3f,%.3f",dist1,dist2);
	#endif
	// TODO: avoid division
	// note: dir should be normalized
	float under1 = tMin>0 && dist1>0 ? dist1/tMin : 0;
	float under2 = tMax>0 && dist2>0 ? dist2/tMax : 0;
	float under = floatMax(under1,under2);
	#if DIAG_LAND
	LogF("  under %.3f,%.3f",under1,under2);
	#endif
	// check intersection with diagonal
	float squareX = x*_terrainGrid, squareZ = z*_terrainGrid;
	float diagNom = _terrainGrid-beg.X()-beg.Z()+squareX+squareZ;
	float diagDenom = dir.X()+dir.Z();
	// note: diagDenom is constant accross many iterations
	// note: diagDenom may be <0
	// we need it>=0 to be able to avoid division
	if (diagDenom<0) diagNom = -diagNom, diagDenom=-diagDenom;
	//float diagT = diagNom/diagDenom;
	//if (diagT>=tMin && diagT<=tMax)
	if (diagNom>=tMin*diagDenom && diagNom<=tMax*diagDenom)
	// avoid division
	{
		// note: we need both diagT and 1/diagT
		float diagT = diagNom/diagDenom;
		// calculate position on diagonal
		Vector3 diagPoint = beg+diagT*dir;
		float distD1 = -plane1.Distance(diagPoint);
		#if DIAG_LAND
		// distance from both planes should be nearly the same
		float distD2 = -plane2.Distance(diagPoint);
		if (fabs(distD1-distD2)>1e-3)
		{
			LogF("  Dist diag %.3f vs %.3f",distD1,distD2);
		}
		// verify it is on diagonal
		float diagX = diagPoint.X()-squareX;
		float diagZ = diagPoint.Z()-squareZ;
		if( fabs(diagX+diagZ-_terrainGrid)>1e-3 )
		{
			LogF("  Diag %.3f,%.3f, sum %.3f",diagX,diagZ,diagX+diagZ);
		}
		#endif
		float underD = diagT>0 ? distD1/diagT : 0;
		saturateMax(under,underD);
		#if DIAG_LAND
		LogF("  distD %.3f",distD1);
		LogF("  underD %.3f",underD);
		#endif
	}
	return under;
}

float Landscape::Visible
(
	Vector3Par from, Vector3Par to, float toRadius,
	const Object *skip1, const Object *target,
	ObjIntersect isect
) const
{
	#if DIAG
	LogF
	(
		"***From %s to %s, type %d",
		skip1 ? (const char *)skip1->GetDebugName() : "<null>",
		target ? (const char *)target->GetDebugName() : "<null>",
		isect
	);
	#endif
	PROFILE_SCOPE(lndVi);
	// maximum sampling is _landGrid
	float objectSize=toRadius;
	Vector3Val objectPos=to;
	Vector3Val sensorPos=from;


	//const int maxVisSam=32;
	Vector3 objDir=objectPos-sensorPos;
	float dist2=objDir.SquareSize();
	float invDist=InvSqrt(dist2);
	float dist=dist2*invDist;
	objDir*=invDist;



	Vector3 pos = sensorPos;
	Vector3 tgt = objectPos+Vector3(0,-objectSize,0);
	Vector3 norm = (tgt-pos);
	Vector3 dNorm = norm.Normalized();
	float deltaX = dNorm.X();
	float deltaZ = dNorm.Z();

	float minVis = 1;

	{
		int xInt = toIntFloor(pos.X()*_invTerrainGrid);
		int zInt = toIntFloor(pos.Z()*_invTerrainGrid);

		float invDeltaX = fabs(deltaX)<1e-10 ? 1e10 : 1/deltaX;
		float invDeltaZ = fabs(deltaZ)<1e-10 ? 1e10 : 1/deltaZ;
		int ddx = deltaX>=0 ? 1 : -1;
		int ddz = deltaZ>=0 ? 1 : -1;
		float dnx = deltaX>=0 ? _terrainGrid : 0;
		float dnz = deltaZ>=0 ? _terrainGrid : 0;

		// maintaing beg, end on current square
		float tRest = dist;
		int maxIter = toInt(Glob.config.horizontZ*_invTerrainGrid*15);
		float tMin = 0, tMax = 0;
		float maxNUnder = 0;
		while (tRest>0)
		{
			if (--maxIter<0)
			{
				LogF
				(
					"From %s to %s, type %d",
					skip1 ? (const char *)skip1->GetDebugName() : "<null>",
					target ? (const char *)target->GetDebugName() : "<null>",
					isect
				);
				LogF
				(
					"Max iters failed (dist %.1f), from %.1f,%.1f,%.1f, to %.1f,%.1f,%.1f",
					dist,
					sensorPos[0],sensorPos[1],sensorPos[2],
					objectPos[0],objectPos[1],objectPos[2]
				);
				LogF("hz %.0f, hz*5 %.0f",Glob.config.horizontZ,Glob.config.horizontZ*5);
				break;
			}
			//Vector3 beg = pos;
			tMin = tMax;

			//advance to next relevant neighbourgh
			int xio = xInt, zio = zInt;
			Vector3 tPoint = pos+dNorm*tMin;
			float tx = (xInt*_terrainGrid+dnx-tPoint.X()) * invDeltaX;
			float tz = (zInt*_terrainGrid+dnz-tPoint.Z()) * invDeltaZ;
			if (tx<=tz)
			{
				saturateMin(tx,tRest);
				xInt += ddx;
				tMax += tx;
				tRest -= tx;
			}
			else
			{
				saturateMin(tz,tRest);
				zInt += ddz;
				tRest -= tz;
				tMax += tz;
			}

			// check end-beg segment
			float maxUnder = CheckUnderLand(sensorPos,dNorm,tMin,tMax,xio,zio);
			// max. angle under ground
			saturateMax(maxNUnder,maxUnder);
			// check if something of the object is visible still
			// object visibility would be 1 - maxNUnder/(objectSize*2*invDist);
			// if this is <=0, object is fully occluded
			// 
			//if (1 - maxNUnder/(objectSize*2*invDist)<=0)
			if ((objectSize*2*invDist)<=maxNUnder)
			{
				#if DIAG
				LogF("  fully covered by ground");
				#endif
				// no need for any more calculations - already fully covered
				return 0;
			}

		}
		// maxNUnder is max. angle under ground
		// object angular size is objectSize*2*invDist
		minVis = 1 - maxNUnder/(objectSize*2*invDist);

		// if landscape fully covers, no need to test objects
		if (minVis<1e-3)
		{
			return minVis;
		}

	}

	saturate(minVis,0,1);
	
	// consider objects
	// check all objects on sensor..object line

	VisCheckContext context;
	context.lineDist=objectPos-sensorPos;
	context.lineDir=objDir;
	context.lineSize=dist;
	context.objectSeem=objectSize*invDist;
	context.objectPos=objectPos;
	context.sensorPos=sensorPos;
	context.skip1=skip1;
	context.target=target;
	context.objectSize=objectSize;
	context.objVis=1;

	// perform DDA los traversal

	//const int border=4; // how much squares are reserved
	int border=toIntFloor(1.5+objectSize*_invLandGrid);

	int ox=toIntFloor(objectPos.X()*_invLandGrid);
	int oz=toIntFloor(objectPos.Z()*_invLandGrid);
	int sx=toIntFloor(sensorPos.X()*_invLandGrid);
	int sz=toIntFloor(sensorPos.Z()*_invLandGrid);
	int aox=abs(ox-sx),aoz=abs(oz-sz);
	const float sBorder=1.5;
	if( aox>aoz )
	{
		// mainly horizontal - primary coordinate is x, derived is z
		float dd=float(oz-sz)/aox;
		int pd=ox>sx ? +1 : -1;
		int p=sx-pd*border;
		float d=sz-dd*border;
		int plen=aox+2*border+1;
		for( int i=plen; --i>=0; )
		{
			int dis=toIntFloor(d-sBorder);
			int die=toIntFloor(d+sBorder);
			for( int di=dis; di<=die; di++ )
			{
				CheckVisibility(p,di,context,isect);
				if( context.objVis<=0 ) return 0;
			}
			d+=dd;
			p+=pd;
		}
	}
	else if( aoz!=0 )
	{
		// mainly vertical - primary coordinate is z, derived is x
		float dd=float(ox-sx)/aoz;
		int pd=oz>sz ? +1 : -1;
		int p=sz-pd*border;
		Assert( abs(p)<2000 );
		float d=sx-dd*border;
		int plen=aoz+2*border+1;
		for( int i=plen; --i>=0; )
		{
			int dis=toIntFloor(d-sBorder);
			int die=toIntFloor(d+sBorder);
			for( int di=dis; di<=die; di++ )
			{
				CheckVisibility(di,p,context,isect);
				if( context.objVis<=0 ) return 0;
			}
			d+=dd;
			p+=pd;
		}
	}
	else
	{

		int ssx,ssz;
		for( ssx=-border; ssx<=border; ssx++ )
		for( ssz=-border; ssz<=border; ssz++ )
		{
			CheckVisibility(sx+ssx,sz+ssz,context,isect);
			if( context.objVis<=0 ) return 0;
		}
	}


	return minVis*context.objVis; // partially visible
}

void Landscape::IsInside
(
	StaticArrayAuto< OLink<Object> > &objects, Object *ignore,
	Vector3Par pos, ObjIntersect isect
)
{
	objects.Clear();
	int xMin,xMax,zMin,zMax;
	ObjRadiusRectangle(xMin,xMax,zMin,zMax,pos,25);
	int x,z;
	for( x=xMin; x<=xMax; x++ ) for( z=zMin; z<=zMax; z++ )
	{
		const ObjectList &list=_objects(x,z);
		int n=list.Size();
		for( int i=0; i<n; i++ )
		{
			Object *obj=list[i];
			if( obj==ignore ) continue;
			if (obj->IsInside(pos,isect))
			{
				objects.Add(obj);
			}
		}
	}
}

static RString ShapeName(const Object *obj)
{
	if (!obj) return RString();
	if (!obj->GetShape()) return RString();
	return obj->GetShape()->GetName();
}

static RString ShapeDebugName(const Object *obj)
{
	if (!obj) return RString("<NULL>");
	return ShapeName(obj)+RString(" ")+obj->GetDebugName();
}

float Landscape::Visible
(
	Vector3Par sensorPos, const Object *sensor, const Object *object,
	float reserve, ObjIntersect isect
) const
{
	// maximum sampling is _landGrid
	LODShape *objectShape=object->GetShape();
	//LODShape *sensorShape=sensor->GetShape();
	if( !objectShape ) return 0;
	float objectSize=object->VisibleSizeRequired()*reserve*0.8;
	// assume some minimal size
	// calculations are based on area covered
	// this would fail if area would be zero
	saturateMax(objectSize,0.01);

	Vector3Val objectPos=object->AimingPosition();
	//Vector3Val sensorPos=sensor->Position();

	// TODO: remove test

	//const int maxVisSam=32;
	float dist2 = objectPos.Distance2(sensorPos);
	#if 0
	if (dist2>Square(Glob.config.horizontZ*1.5))
	{
		// one of them may be soldier in vehicle
		if (!sensor->IsInLandscape() || !object->IsInLandscape())
		{
			RptF
			(
				"Dist %.1f, between %s (%.1f,%.1f,%.1f):%s and %s (%.1f,%.1f,%.1f):%s" ,
				sqrt(dist2),
				(const char *)ShapeDebugName(sensor),
				sensorPos.X(),sensorPos.Y(),sensorPos.Z(),
				sensor->IsInLandscape() ? "Land" : "Veh",
				(const char *)ShapeDebugName(object),
				objectPos.X(),objectPos.Y(),objectPos.Z(),
				object->IsInLandscape() ? "Land" : "Veh"
			);
			return 0;
		}

	}
	#endif
	if (dist2<Square(Glob.config.horizontZ*8))
	{
		return Visible(sensorPos,objectPos,objectSize,sensor,object,isect);
	}
	RptF
	(
		"Dist %.1f, between %s (%.1f,%.1f,%.1f) and %s (%.1f,%.1f,%.1f)",
		sqrt(dist2),
		(const char *)ShapeDebugName(sensor),
		sensorPos.X(),sensorPos.Y(),sensorPos.Z(),
		(const char *)ShapeDebugName(object),
		objectPos.X(),objectPos.Y(),objectPos.Z()
	);
	// BUG: may not be visible
	// probably soldier in vehicle
	return 0;
}

float Landscape::Visible
(
	const Object *sensor, const Object *object,
	float reserve, ObjIntersect isect
) const
{
	return Visible(sensor->CameraPosition(),sensor,object,reserve,isect);
}

float Landscape::VisibleStrategic( int xs, int zs, int xe, int ze ) const
{
	Fail("Not implemented.");
	// TODO: real landscape/forest (GeographyInfo) check
	// todo: 
	return 1;
}

float Landscape::VisibleStrategic( Vector3Par from, Vector3Par to ) const
{
	#if 1
	// landscape/forest (GeographyInfo) check

	//const int maxVisSam=16;
	float dist=to.Distance(from);
	int samples=toIntFloor(dist*_invTerrainGrid*2);
	if (samples<=0) return 1;
	float invSamples = 1.0/samples;
	Vector3 step=(to-from)*invSamples;
	Point3 pt;
	int i;
	int tLog = GetTerrainRangeLog()-GetLandRangeLog();
	for( i=samples,pt=from; --i>=0; pt+=step )
	{
		// TODO: use rough SurfaceY estimation only
		int ix=toIntFloor(pt.X()*_invTerrainGrid);
		int iz=toIntFloor(pt.Z()*_invTerrainGrid);
		if( !this_TerrainInRange(ix,iz) ) continue;
		if( !this_TerrainInRange(ix+1,iz+1) ) continue;
		float y00=GetData(ix,iz);
		float y01=GetData(ix,iz+1);
		float y10=GetData(ix+1,iz);
		float y11=GetData(ix+1,iz+1);
		float y=y00;
		saturateMin(y,y01);
		saturateMin(y,y10);
		saturateMin(y,y11);
		//float y=SurfaceY(pt.X(),pt.Z());
		//saturateMax(y,GetSeaLevel());
		saturateMax(y,0);
		// consider GeographyInfo as part of surface
		y-=3; // be a little bit pesimistic
		int ixt = ix>>tLog;
		int izt = iz>>tLog;
		GeographyInfo g=GetGeography(ixt,izt);
		if( g.u.full ) y+=15;
		static const float objectsHeight[]={0,0,2,5};
		y+=objectsHeight[g.u.howManyObjects];
		if( y>pt[1] ) return 0;
	}
	#endif
	return 1;
}

bool Landscape::CheckIntersection
(
	Vector3Par beg, Vector3Par end, int x, int z, float &tRet
) const
{
	// beg and end must be in square (x,z)
	// calculate surface level on given coordinates

	float y00,y01,y10,y11;
	#if !USE_SWIZZLED_ARRAYS
		if( !this_TerrainInRange(z,x) || !this_TerrainInRange(z+1,x+1) )
		{
			y00=y01=y10=y11=-100;
		}
		else
		{
			y00=GetData(x,z);
			y01=GetData(x+1,z);
			y10=GetData(x,z+1);
			y11=GetData(x+1,z+1);
		}
	#else
		if( !this_TerrainInRange(z,x) || !this_TerrainInRange(z+1,x+1) ) return false; // no bump outside landscape
		RawType y4[2][2];
		_data.GetFour(y4,x,z);
		y00=RawToHeight(y4[0][0]);
		y01=RawToHeight(y4[0][1]);
		y10=RawToHeight(y4[1][0]);
		y11=RawToHeight(y4[1][1]);
	#endif

	float maxSurfY = floatMax( floatMax(y00,y01), floatMax(y10,y11) );
	float minLineY = floatMin( beg.Y(), end.Y() );
	if (minLineY>maxSurfY ) return false;
	// each face is divided to two triangles
	// determine which triangle contains point
	Vector3 normal1;
	Vector3 normal2;
	normal1.Init();
	normal2.Init();
	//
	// triangle 00,01,10
	normal1[0]=(y01-y00)*-_invTerrainGrid;
	normal1[1]=1;
	normal1[2]=(y10-y00)*-_invTerrainGrid;
	// triangle 01,10,11
	normal2[0]=(y11-y10)*-_invTerrainGrid;
	normal2[1]=1;
	normal2[2]=(y11-y01)*-_invTerrainGrid;
	// 
	Vector3 point((x+1)*_terrainGrid,y01,z*_terrainGrid);
	// size of normal1 and normal2 is nearly 1
	Plane plane1(normal1,point);
	Plane plane2(normal2,point);

	// precision of in A / in B check
	const float inEps=_invTerrainGrid*0.01;

	// calculate intersection with plane under point pos
	Vector3 direction = end-beg;
	float dist1 = plane1.Distance(beg);
	if (dist1<0)
	{ // beg already under surface
		// check if beg in this triangle
		float xIn=beg.X()*_invTerrainGrid-x; // relative 0..1 in square
		float zIn=beg.Z()*_invTerrainGrid-z;
		if( xIn<=1-zIn+inEps )
		{
			tRet=0;
			return true;
		}
	}
	float dist2 = plane2.Distance(beg);
	if (dist2<0)
	{ // point already under surface
		float xIn=beg.X()*_invTerrainGrid-x; // relative 0..1 in square
		float zIn=beg.Z()*_invTerrainGrid-z;
		if( xIn>=1-zIn-inEps )
		{
			tRet=0;
			return true;
		}
	}

	float denom1=-plane1.Normal()*direction;
	if( fabs(denom1)>1e-10 )
	//if( denom1>1e-10 )
	{
		float t = dist1/denom1;
		if( t>=0 && t<=1 )
		{
			Vector3 nPos=beg+t*direction;

			float xIn=nPos.X()*_invTerrainGrid-x; // relative 0..1 in square
			float zIn=nPos.Z()*_invTerrainGrid-z;
			if( xIn<=1-zIn+inEps )
			{
				tRet=t;
				return true;
			}
		}
	}

	float denom2=-plane2.Normal()*direction;
	if( denom2>1e-10 )
	{
		float t = dist2/denom2;
		if( t>=0 && t<=1 )
		//if( dist2<=denom2 )
		{
			//float t = dist2/denom2;
			Vector3 nPos=beg+t*direction;

			float xIn=nPos.X()*_invTerrainGrid-x; // relative 0..1 in square
			float zIn=nPos.Z()*_invTerrainGrid-z;
			if( xIn>=1-zIn-inEps  )
			{
				tRet=t;
				return true;
			}
		}
	}
	return false;
}

inline float FloatSign(float x)
{
	if (x>=0) return 1;
	return -1;
}


float Landscape::IntersectWithGround
(
	Vector3 *ret,
	Vector3Par from, Vector3Par dir,
	float minDist, float maxDist
) const
{
	// return time from minDist to maxDist (when intersection is found)
	PROFILE_SCOPE(lndIG);
	float maxDist0 = maxDist*1.1f;
	//Vector3 direction=dir.Normalized();
	saturateMin(maxDist,Glob.config.horizontZ);
	// find first intersection with ground
	// optimized landscape traversal
	// initialize
	Vector3 pos = from;

	Vector3 dNorm = dir.Normalized();
	float deltaX = dNorm.X();
	float deltaZ = dNorm.Z();

	int xInt = toIntFloor(pos.X()*_invTerrainGrid);
	int zInt = toIntFloor(pos.Z()*_invTerrainGrid);

	float invDeltaX = fabs(deltaX)<1e-10 ? FloatSign(deltaX)*1e10 : 1/deltaX;
	float invDeltaZ = fabs(deltaZ)<1e-10 ? FloatSign(deltaZ)*1e10 : 1/deltaZ;
	int ddx = deltaX>=0 ? 1 : -1;
	int ddz = deltaZ>=0 ? 1 : -1;
	float dnx = deltaX>=0 ? _terrainGrid : 0;
	float dnz = deltaZ>=0 ? _terrainGrid : 0;

	// maintaing beg, end on current square
	float tRest = maxDist;
	int maxIter = toInt(Glob.config.horizontZ*_invTerrainGrid*15);
	while (tRest>0)
	{
		if (--maxIter<0)
		{
			LogF("IntersectWithGround: Max iters failed (dist %.1f)",maxDist);
			break;
		}
		Vector3 beg = pos;

		//advance to next relevant neighbourgh
		int xio = xInt, zio = zInt;
		float tx = (xInt*_terrainGrid+dnx-pos.X()) * invDeltaX;
		float tz = (zInt*_terrainGrid+dnz-pos.Z()) * invDeltaZ;
		Assert( tx>=-0.01 );
		Assert( tz>=-0.01 );
		if (tx<=tz)
		{
			saturateMin(tx,tRest);
			xInt += ddx;
			tRest -= tx;
			pos += dNorm*tx;
		}
		else
		{
			saturateMin(tz,tRest);
			zInt += ddz;
			tRest -= tz;
			pos += dNorm*tz;
		}

		// check end-beg segment
		float t;
		bool col = CheckIntersection(beg,pos,xio,zio,t);
		if (col)
		{
			if (ret) *ret = beg*(1-t)+pos*t;
			return t;
		}
	}
	if (ret) *ret = from+dNorm*maxDist0;
	return maxDist0;
}


float Landscape::IntersectWithGroundOrSea
(
	Vector3 *ret, bool &sea,
	Vector3Par from, Vector3Par dir,
	float minDist, float maxDist
) const
{
	// first check intersection with sea level
	sea = false;
	float seaLevel = _seaLevelWave;
	// X: from+t*dir
	// X1 == seaLevel
	// X1: from1+t*dir1
	// seaLevel = from1+t*dir1
	// (seaLevel-from1)/dir1 = t
	float tSea = maxDist*2;
	float denom = dir[1];
	float nom = seaLevel-from[1];
	if (nom>0)
	{
		// already under sea
		if (ret) *ret = from+minDist*dir;
		sea = true;
		return minDist;
	}
	if (denom<-1e-6)
	{
		// check only when dir goes down
		tSea = nom/denom;

		if (tSea<minDist)
		{
			if (ret) *ret = from+minDist*dir;
			sea = true;
			return minDist;
		}
	}
	saturateMin(maxDist,tSea); // check only earlier intersection

	// calculate when 
	float t = IntersectWithGround(ret,from,dir,minDist,maxDist);
	if (t>tSea)
	{
		if (ret) *ret = from+dir*tSea;
		sea = true;
		return tSea;
	}
	return t;
}

float Landscape::IntersectWithGroundOrSea
(
	Vector3 *ret,
	Vector3Par from, Vector3Par dir,
	float minDist, float maxDist
) const
{
	bool sea;
	return IntersectWithGroundOrSea(ret,sea,from,dir,minDist,maxDist);
}

Vector3 Landscape::IntersectWithGround
(
	Vector3Par from, Vector3Par dir, float minDist, float maxDist
) const
{
	Vector3 ret;
	IntersectWithGround(&ret,from,dir,minDist,maxDist);
	return ret;
}

Vector3 Landscape::IntersectWithGroundOrSea
(
	Vector3Par from, Vector3Par dir, float minDist, float maxDist
) const
{
	Vector3 ret;
	IntersectWithGroundOrSea(&ret,from,dir,minDist,maxDist);
	return ret;
}


Object *Landscape::PreviewFire
(
	const Object *ignore,
	Vector3Par from, Vector3Par speed, Vector3 accel,
	float timeToLive
) const
{
	// this function is never used and need major redesign
	/*
	LogF("Preview fire %s",(const char *)ignore->GetDebugName());
	// return what will be hit if we will fire this way
	const float deltaT=0.01;
	// simulate fire trajectory
	Vector3 speedT=deltaT*speed;
	Vector3 accelT=deltaT*accel;
	Vector3 pos=from;
	while( timeToLive>0 )
	{	
		float surfaceY=SurfaceY(pos[0],pos[2]);
		if( pos[1]<=surfaceY )
		{
			// TODO: check indirect hit (explosion)
			return NULL;
		}
		Vector3 newPos=pos+speedT;
		// check object hit between pos and newPos

		CollisionBuffer hit;
		ObjectCollision(hit,NULL,pos,newPos,0.5);
		Object *firstHit=NULL;
		float minT=1e10;
		for( int i=0; i<hit.Size(); i++ )
		{
			const CollisionInfo &info=hit[i];
			if( info.object==ignore ) continue;
			DebugLog("  hit %s",(const char *)info.object->GetDebugName());
			if( minT>info.under )
			{
				minT=info.under;
				firstHit=info.object;
			}
		}
		// if some hit detected, return it
		if( firstHit ) return firstHit;
		// 
		pos=newPos;
		speedT+=accelT;
		timeToLive-=deltaT;
	}
	// no hit - return 
	*/
	return NULL;
}

// collison buffer static storage


CollisionBuffer::CollisionBuffer()
{
	static StaticStorage<CollisionInfo> storage;
	SetStorage(storage.Init(128));
}

CollisionBuffer::~CollisionBuffer()
{
}

VehicleCollisionBuffer::VehicleCollisionBuffer()
{
	static StaticStorage<VehicleCollision> storage;
	SetStorage(storage.Init(128));
}

VehicleCollisionBuffer::~VehicleCollisionBuffer()
{
}

GroundCollisionBuffer::GroundCollisionBuffer()
{
	static StaticStorage<UndergroundInfo> storage;
	SetStorage(storage.Init(512));
}

GroundCollisionBuffer::~GroundCollisionBuffer()
{
}
