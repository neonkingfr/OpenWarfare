#include "wpch.hpp"

#if !_DISABLE_GUI
#include "engdd.hpp"


#include "txtD3D.hpp"
#include "poly.hpp"
#include "tlVertex.hpp"
#include "txtPreload.hpp"
#include "shape.hpp"
#include "clip2D.hpp"
#include "clip2D.hpp"
#include <El/common/perfLog.hpp>
#include "global.hpp"
#include <El/QStream/qbstream.hpp>

#ifndef _XBOX
	#include <winuser.h>
	#include <Dxerr8.h>
	#define NoSysLockFlag D3DLOCK_NOSYSLOCK
	#define NoClipUsageFlag D3DUSAGE_DONOTCLIP
	#define DiscardLockFlag D3DLOCK_DISCARD
#else
	#define NoSysLockFlag 0
	#define NoClipUsageFlag 0
	#define DiscardLockFlag 0
	#define NO_FOG 0
#endif

#define PERF_PROF 1

#if PERF_PROF
#include "perfProf.hpp"
#define PROFILE_DX_SCOPE(name) PROFILE_SCOPE(d3d)
//#define PROFILE_DX_SCOPE(name) PROFILE_SCOPE(name)
#else
#define PROFILE_DX_SCOPE(name)
#endif

#ifndef _XBOX
	#define DX_CHECK(hr) \
		if (hr!=D3D_OK) \
		{ \
			DXTRACE_ERR_NOMSGBOX("Check",hr); \
			Assert(false); \
		}

	#define DX_CHECKN(n,hr) \
		if (hr!=D3D_OK) \
		{ \
			DXTRACE_ERR_NOMSGBOX(n,hr); \
			Assert(false); \
		}
#else
	#define DX_CHECK(hr) \
		if (hr!=D3D_OK) \
		{ \
			Assert(false); \
		}

	#define DX_CHECKN(n,hr) \
		if (hr!=D3D_OK) \
		{ \
			Assert(false); \
		}
#endif

#if VERTEX_SHADERS

#include "shaders/vsDecl.h"

#endif

#if _DEBUG
	#define WRITEONLYVB D3DUSAGE_WRITEONLY
	#define WRITEONLYIB 0
#else
	#define WRITEONLYVB D3DUSAGE_WRITEONLY
	#define WRITEONLYIB D3DUSAGE_WRITEONLY
#endif
//#define WRITEONLYVB 0
//#define WRITEONLYIB 0

#define FAST_D3DRGBA(r, g, b, a) \
  (((toIntFloor((a) * 255)) << 24) | ((toIntFloor((r) * 255)) << 16) | \
  ((toIntFloor((g) * 255)) << 8) | toIntFloor((b) * 255)) 

#define FAST_D3DRGB(r, g, b) \
  (0xff000000 | ((toIntFloor((r) * 255)) << 16) | \
  ((toIntFloor((g) * 255)) << 8) | toIntFloor((b) * 255)) 

#define FAST_D3DA(a) \
  ( (toIntFloor((a) * 255)) << 24 ) 


#define VALIDATE 0

#define LOG_LOCKS 0

#if _PIII
#define MYFVF (D3DFVF_XYZRHW|D3DFVF_DIFFUSE|D3DFVF_SPECULAR|D3DFVF_TEX3)
#else
#define MYFVF (D3DFVF_XYZRHW|D3DFVF_DIFFUSE|D3DFVF_SPECULAR|D3DFVF_TEX2)
#endif

#define TL_OBJ_BUF 1

// source vertex buffer format
#define MYSFVF (D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_TEX1)

#if 0
	#define ValidateCoord(c)
#else
	#define LOG_INVALID 0
	inline void ValidateCoord( float &c, float max )
	{
		#if LOG_INVALID
			static int total=0;
			static int valid=0;
			total++;
		#endif
		#if 0
		const float minC=-2047,maxC=+2048;
		// coordinate must be within range -2047...2048
		if( c<minC ) c=minC;
		else if( c>maxC ) c=maxC;
		#else
		if( c<0 ) c=0;
		else if( c>max ) c=max;
		#endif

		#if LOG_INVALID
		else valid++;
		if( total>=100000 )
		{
			if( valid<total ) LogF("Invalid %d of %d",total-valid,total);
			valid=0;
			total=0;
		}
		#endif
	}
#endif

#if _ENABLE_PERFLOG && _ENABLE_CHEATS
#define DO_TEX_STATS 1
#endif

#if _ENABLE_PERFLOG 
bool LogStatesOnce = false;
#endif

#if DO_TEX_STATS
#include "statistics.hpp"
#include "keyInput.hpp"
#include "dikCodes.h"
static StatisticsByName TexStats;
bool EnableTexStats = false;
#endif

#if PIXEL_SHADERS
#include "shaders/psDecl.h"
#endif

extern int D3DAdapter;
extern bool EnableHWTL;

//StatisticsByName vbStats;

static void ConvertMatrix( D3DMATRIX &mat, Matrix4Val src )
{
	mat._11 = src.DirectionAside()[0];
	mat._12 = src.DirectionAside()[1];
	mat._13 = src.DirectionAside()[2];

	mat._21 = src.DirectionUp()[0];
	mat._22 = src.DirectionUp()[1];
	mat._23 = src.DirectionUp()[2];

	mat._31 = src.Direction()[0];
	mat._32 = src.Direction()[1];
	mat._33 = src.Direction()[2];

	mat._41 = src.Position()[0];
	mat._42 = src.Position()[1];
	mat._43 = src.Position()[2];

	mat._14 = 0;
	mat._24 = 0;
	mat._34 = 0;
	mat._44 = 1;
}

static void ConvertMatrixTransposed( D3DMATRIX &mat, Matrix4Val src )
{
	mat._11 = src.DirectionAside()[0];
	mat._21 = src.DirectionAside()[1];
	mat._31 = src.DirectionAside()[2];
	mat._41 = 0;

	mat._12 = src.DirectionUp()[0];
	mat._22 = src.DirectionUp()[1];
	mat._32 = src.DirectionUp()[2];
	mat._42 = 0;

	mat._13 = src.Direction()[0];
	mat._23 = src.Direction()[1];
	mat._33 = src.Direction()[2];
	mat._43 = 0;

	mat._14 = src.Position()[0];
	mat._24 = src.Position()[1];
	mat._34 = src.Position()[2];
	mat._44 = 1;
}

void EngineDD::GetZCoefs(float &zAdd, float &zMult)
{
	int zBias = _bias;
	/*
	if (_useDXTL)
	{
		zMult = 1.0f-zBias*1e-7f;
		zAdd = -zBias*1e-6f;
	}
	else
	*/
	{
		zMult = 1.0f-zBias*1e-7f;
		zAdd = zBias*-2e-7f;
	}
}


static void ConvertProjectionMatrix
(
	D3DMATRIX &mat, Matrix4Val src, float zBias
)
{
	// note: D3D notation _11 is element (0,0) in C notation
	mat._11 = src(0,0);
	mat._12 = 0;
	mat._13 = 0;
	mat._14 = 0;

	mat._21 = 0;
	mat._22 = src(1,1);
	mat._23 = 0;
	mat._24 = 0;

	/*
	// note: D3D projection calculation
	w = 1/z
	z = src(2,2)+src.Pos(2)/z
	*/

	float c = src(2,2);
	float d = src.Position()[2];
	if (zBias>0)
	{
		if (GEngine->GetTL() && GEngine->HasWBuffer() && GEngine->IsWBuffer())
		{
			// w-buffer adjustments based on near / far values
			// calculate near and far
			float n = -d/c;
			float f = d/(1-c);
			// adjust near and far depending on z-bias
			//float mult = 1.0f-zBias*1e-6f;
			float add = 0; //zBias*1e-6f;
			float mult = 1+zBias*1e-5f;
			//float add = 0;

			f *= mult;
			n *= mult;
			n += add;

			// set adjusted coefs
			c = f/(f-n);
			d = -c*n;
		}
		else
		{
			// values used for SW T&L
			//float zMult = 1.0f-zBias*1e-7f;
			//float zAdd = zBias*-2e-7f;
			float zMult = 1.0f-zBias*1e-7f;
			float zAdd = -zBias*1e-6f;
			
			c = src(2,2)*zMult+zAdd;
			d = src.Position()[2]*zMult;
		}
	}

	mat._31 = 0;
	mat._32 = 0;
	mat._33 = c;
	mat._34 = 1;

	mat._41 = 0;
	mat._42 = 0;
	mat._43 = d;
	mat._44 = 0;
	// source w is 1
	// result homogenous z: x*m13 + y*m23 + z*m33 + m43
	// result homogenous w:                 z
	// m13 and m23 is zero
	// result projected  z: m33 + m43/z

	#if DO_TEX_STATS
	if (LogStatesOnce)
	{
		LogF("Set z-bias %d",zBias);

		LogF("  now: z1 = %10g/z0 + %10g",mat._43,mat._33);
		LogF("  def: z1 = %10g/z0 + %10g",src.Position()[2],src.Direction()[2]);
	}
	#endif

}

// TODO: avoid scene here
#include "scene.hpp"
#include "lights.hpp"
#include "camera.hpp"


void LogTextureF( const char *t, const TextureD3D *x )
{
	LogF("Texture %s %s",t,x ? x->Name() : "--");
}

#if 0
	#define LogTexture(t,x) LogTextureF(t,x)
#else
	#define LogTexture(t,x)
#endif

bool EngineDD::CanZBias() const
{
	return _canZBias;
}

#if DO_TEX_STATS
void LogSetProj(const D3DMATRIX &mat)
{
	if (LogStatesOnce)
	{
		LogF
		(
			"Set Projection %g,%g,%g,%g",
			mat._11,mat._22,mat._33,mat._43
		);
	}
	/*
	float c = mat._33;
	float d = mat._43;
	float n = -d/c;
	float f = d/(1-c);

	float Q = f/(f-n);
	float mQZn = -Q*n;
	LogF("Set c=%g (%g), d=%g (%g), n=%.2f, f=%.1f",c,Q,d,mQZn,n,f);
	*/

}
#else

#define LogSetProj(mat)

#endif

void EngineDD::SetBias( int bias )
{
	// some devices support z-bias
	if (bias==_bias) return;
	_bias=bias;
	/**/
	if (_canZBias)
	{
		FlushAllQueues(_queueNo);
		int biasRs = bias;
		//int biasRs = bias/4;
		//saturate(biasRs,0,15);
		D3DSetRenderState(D3DRS_ZBIAS,biasRs);
		return;
	}
	/**/
	// simulate z-bias using perspective matrix
	// maybe better: simulate it using view-matrix
	if (_tlActive)
	{
		/**/
		D3DMATRIX projMatrix;
		// set projection (based on camera)
		Camera *camera = GScene->GetCamera();
		ConvertProjectionMatrix(projMatrix,camera->ProjectionNormal(),bias);

		_d3DDevice->SetTransform( D3DTS_PROJECTION , &projMatrix );
		LogSetProj(projMatrix);
		/**/
	}

}

void EngineDD::DoSetGrassParamsPS()
{
	#if PIXEL_SHADERS
	float grassCoefs[8]=
	{
		0,0,0,_grassParam[0],
		0,0,0,_grassParam[1],
	};
	_d3DDevice->SetPixelShaderConstant( GRASS_COEF_1, grassCoefs, 2 );
	#endif
}
void EngineDD::DoSetGrassParamsVS()
{
	#if VERTEX_SHADERS
	float grassVCoefs[4]=
	{
		_grassParam[2],0.25,0.5,1
	};
	_d3DDevice->SetVertexShaderConstant( GRASS_PARAM, grassVCoefs, 2 );
	#endif
}

void EngineDD::SetGrassParams(float a1, float a2, float a3, float a4)
{
	if
	(
		fabs(_grassParam[0]-a1)<0.001 &&
		fabs(_grassParam[1]-a2)<0.001 &&
		fabs(_grassParam[2]-a3)<0.001 &&
		fabs(_grassParam[3]-a4)<0.001
	)
	{
		return;
	}

	_grassParam[0] = a1;
	_grassParam[1] = a2;
	_grassParam[2] = a3;
	_grassParam[3] = a4;
	#if PIXEL_SHADERS
	if (_pixelShaderSel==PSGrass) DoSetGrassParamsPS();
	#endif
	#if VERTEX_SHADERS
	if (_vertexShaderSel==VSDayGrass) DoSetGrassParamsVS();
	#endif
}

void EngineDD::EnableReorderQueues( bool enableReorder )
{
	if (_enableReorder==enableReorder) return;
	_enableReorder = enableReorder;
	if (!_enableReorder)
	{
		FlushQueues();
	}
}
void EngineDD::FlushQueues()
{
	FlushAndFreeAllQueues(_queueNo);
	//FlushAndFreeAllQueues(_queueTL);
}

#ifdef _XBOX
#define NO_SET_TEX 0
#else
#define NO_SET_TEX 0
#endif


#define NO_SET_TSS 0
#define NO_SET_RS 0

extern void ReleaseVBuffers();
extern void RestoreVBuffers();

void EngineDD::PreReset(bool hard)
{
	FreeAllQueues(_queueNo);

	_iBufferLast.Free(); // forget any VB
	_vBufferLast.Free();

	_d3DDevice->SetTexture( 1,NULL );
	_d3DDevice->SetTexture( 0,NULL );
	
	// destroy all video memory surfaces
	_backBuffer.Free();
	_frontBuffer.Free();
	//_zBuffer.Free();

	_textBank->ReleaseAllTextures(hard); // release all textures
	// release all vertex buffers
	ReleaseVBuffers();
	#if PIXEL_SHADERS
	DeinitPixelShaders();
	#endif

	DestroyVB();
	DestroyVBTL();
	_lastQueueSource = NULL;
}

void EngineDD::PostReset()
{
	CreateVB();
	CreateVBTL();

	RestoreVBuffers();

	// reset all render/texture states to undefined
	for (int tmu=0; tmu<MaxStages; tmu++)
	{
		_textureStageState[tmu].Clear();
	}
	_renderState.Clear();
	Init3DState();
	// TODO: reset fog
}

bool EngineDD::Reset()
{
	bool inScene = _d3dFrameOpen;
	if (inScene) FinishDraw();

	PreReset(false);

	// Recreate all surfaces

	HRESULT ok = _d3DDevice->Reset(&_pp);
	if (ok!=D3D_OK)
	{
		LogF("Reset failed.");
		// wait for a while and try again
		Sleep(1000);
		HRESULT ok = _d3DDevice->Reset(&_pp);
		if (ok!=D3D_OK)
		{
			Sleep(5000);
			ok = _d3DDevice->Reset(&_pp);
			if (ok!=D3D_OK)
			{
				LogF("Reset failed after sleep");
				return false;
			}
		}

	}

	PostReset();
	if (inScene) InitDraw();
	return true;
}

bool EngineDD::ResetHard()
{
	bool inScene = _d3dFrameOpen;
	if (inScene) FinishDraw();
	PreReset(true);

	// destroy surfaces and device
	DestroySurfaces();

	Init3D();
	DoSetGamma();

	// direct-color mode required
	D3DConstruct();

	PostReset();
	if (inScene) InitDraw();
	return true;
}

bool EngineDD::IsAbleToDrawCheckOnly()
{
	HRESULT lost = _d3DDevice->TestCooperativeLevel();
	if (lost==D3D_OK)
	{
		_resetNeeded = false;
	}
	else
	{
		_resetNeeded = true;
	}
	return !_resetNeeded;
}

bool EngineDD::IsAbleToDraw()
{
	if (!_d3DDevice) return false;
	#ifndef _XBOX
	HRESULT lost = _d3DDevice->TestCooperativeLevel();
	if (lost==D3DERR_DEVICENOTRESET)
	{
		bool ok = Reset();
		if (ok) _resetNeeded = false;
		return true;
	}
	else if (lost==D3D_OK)
	{
		_resetNeeded = false;
		return true;
	}
	else
	{
		_resetNeeded = true;
		return false;
	}
	#else
		return true;
	#endif
}

void EngineDD::InitDraw( bool clear, PackedColor color )
{
	if( _d3dFrameOpen )
	{
		LogF("InitDraw done twice");
		return;
	}

	#if _ENABLE_CHEATS
		if (GInput.GetCheat1ToDo(DIK_APOSTROPHE))
		{
			_usePixelShaders = !_usePixelShaders;
			GlobalShowMessage(100,"Pixel shaders %s",_usePixelShaders ? "On" : "Off");
		  if (!_usePixelShaders) DoSelectPixelShader(PSNone,PSMDay,PSSNormal);
			else DoSelectPixelShader(PSNone,PSMDay,PSSNormal);
			Init3DState();
		}

	#endif

	IsAbleToDrawCheckOnly();

	D3DRECT rect;
	rect.x1=0,rect.y1=0;
	rect.x2=_w,rect.y2=_h;
	int flags=D3DCLEAR_ZBUFFER;
	if (_hasStencilBuffer) flags|=D3DCLEAR_STENCIL;
	if( clear ) flags|=D3DCLEAR_TARGET;
	
	if (!_resetNeeded)
	{
		PROFILE_DX_SCOPE(3clr);
		_d3DDevice->Clear(0,NULL,flags,color,1,0);
	}

	_textBank->StartFrame();	
	
	base::InitDraw();

	#if LOG_LOCKS
	LogF("BeginScene");
	#endif

	#if DO_TEX_STATS
		LogStatesOnce = false;
		if (GInput.GetCheat1ToDo(DIK_E))
		{
			LogF("---------------------------------");
			LogF("Report texture switch totals");
			TexStats.Report();
			LogF("---------------------------------");
			TexStats.Clear();
			EnableTexStats = true;
			#if 1
			LogStatesOnce = true;
			#endif
		}
		if (EnableTexStats) TexStats.Count("** BeginScene **");
		if (LogStatesOnce)
		{
			LogF("---- BeginScene");
		}
	#endif
	D3DBeginScene();

	_d3dFrameOpen = true;

	TLMaterial invalidMat;
	invalidMat.diffuse = Color(-1,-1,-1,-1);
	invalidMat.ambient = Color(-1,-1,-1,-1);
	invalidMat.forcedDiffuse = Color(-1,-1,-1,-1);
	invalidMat.emmisive = Color(-1,-1,-1,-1);
	invalidMat.specFlags = 0;

	LightList lights;
	DoSetMaterial(invalidMat,lights,0);
}

void EngineDD::FinishDraw()
{
	if( _d3dFrameOpen )
	{
		//_sMesh =NULL;
		base::FinishDraw();

		base::DrawFinishTexts();

		CloseAllQueues(_queueNo);
		//CloseAllQueues(_queueTL);
		_d3dFrameOpen=false;
		DiscardVB();

		#if DO_TEX_STATS
		if (LogStatesOnce)
		{
			LogF("---- EndScene");
		}
		#endif


		// fill shadows (using stecil buffer)

		#if LOG_LOCKS
			LogF("[[[ EndScene");
		#endif
		D3DEndScene();
		#if LOG_LOCKS
			LogF("]]] EndScene");
		#endif

		_textBank->FinishFrame();

		// TODO: check - it might be better to call BackToFront before BeginScene
		// experimental - several BackToFront positions in code possible
		//BackToFront();		
	}
}

void EngineDD::NextFrame()
{
	// swap frames - get ready for next frame
	// experimental - several BackToFront positions in code possible
	BackToFront();
	base::NextFrame();
}

bool EngineDD::InitDrawDone()
{
	return _d3dFrameOpen;
}

void EngineDD::TextureDestroyed( Texture *tex )
{
/*
	if( _lastTexture==tex )
	{
		_lastTexture=NULL;
		_lastMipmap=NULL;
	}
*/
}


void EngineDD::D3DEndScene()
{
	//_sBuffer = NULL;

	PROFILE_DX_SCOPE(3eScn);
	HRESULT err=_d3DDevice->EndScene();
	if( err!=D3D_OK ) DDError("Cannot end scene",err);
}
void EngineDD::D3DBeginScene()
{
	LogTexture("XXX Start XXX",NULL);
	// check cooperative level (surfaces lost)
	PROFILE_DX_SCOPE(3bScn);
	HRESULT err=_d3DDevice->BeginScene();
	if( err!=D3D_OK )
	{
		DDError("Cannot begin scene",err);
	}

}

void EngineDD::PrepareMesh( int spec )
{
	// prepare internal variables
	SwitchTL(false);
	Camera *cam = GScene->GetCamera();
	Matrix4Val proj = cam->ProjectionNormal();
	ChangeWDepth(proj(2,2),proj.Position()[2]);
	ChangeClipPlanes();
}


struct VBSectionInfo
{
	int beg,end;
	int begVertex,endVertex; // used vertex range
};

TypeIsSimple(VBSectionInfo);


VertexStaticData::VertexStaticData()
{
	_vBufferSize = 0; // reserved sizes of resources
	_iBufferSize = 0;

	_vBufferUsed = 0; // used sizes
	_iBufferUsed = 0;

	_nShapes = 0;
}

VertexStaticData::~VertexStaticData()
{
	Dealloc();
}

/*!
\patch 1.95 Date 10/14/2003 by Ondra
- Fixed: More improvements in virtual address range handling.
*/

void VertexStaticData::Init(int vertices, int indices, IDirect3DDevice8 *dev)
{
	if (vertices>0)
	{
		_vBufferSize = 0;
		_vBufferUsed = 0;
		int byteSize = vertices*sizeof(SVertex);
		RetryV:
		HRESULT err = dev->CreateVertexBuffer
		(
			byteSize,WRITEONLYVB,MYSFVF,D3DPOOL_DEFAULT,
			_vBuffer.Init()
		);
		if (err!=D3D_OK)
		{
			if (err==D3DERR_OUTOFVIDEOMEMORY)
			{
      	if (QFBank::FreeUnusedBanks(byteSize+64*1024)) goto RetryV;
				if (GEngineDD->TextBankDD()->ForcedReserveMemory(byteSize))
				{
					goto RetryV;
				}
			}
      else if (err==E_OUTOFMEMORY)
      {
    		if (QFBank::FreeUnusedBanks(byteSize+64*1024)) goto RetryV;
      }
			DDError("CreateVertexBuffer",err);
      ErrorMessage("CreateVertexBuffer (static) failed %x - out of memory?",err);
			return;
			// 
		}
		_vBufferSize = vertices;
	}
	if (indices>0)
	{
		_iBufferSize = 0;
		_iBufferUsed = 0;
		int byteSize = indices*sizeof(VertexIndex);
		RetryI:
		HRESULT err = dev->CreateIndexBuffer
		(
			byteSize,WRITEONLYIB,D3DFMT_INDEX16,D3DPOOL_DEFAULT,
			_iBuffer.Init()
		);
		if (err!=D3D_OK)
		{
			if (err==D3DERR_OUTOFVIDEOMEMORY)
			{
      	if (QFBank::FreeUnusedBanks(byteSize+64*1024)) goto RetryI;
				if (GEngineDD->TextBankDD()->ForcedReserveMemory(byteSize))
				{
					goto RetryI;
				}
			}
      else if (err==E_OUTOFMEMORY)
      {
    		if (QFBank::FreeUnusedBanks(byteSize+64*1024)) goto RetryI;
      }
			DDError("CreateIndexBuffer",err);
      ErrorMessage("CreateIndexBuffer (static) failed %x - out of memory?",err);
			return;
			// 
		}
		_iBufferSize = indices;
	}
}

#define V_OFFSET_CUSTOM 1


void VertexStaticData::Dealloc()
{
	if (_nShapes>0)
	{
		LogF("VertexStaticData:: %d shapes not removed",_nShapes);
	}
	_vBuffer.Free();
	_iBuffer.Free();
}

/*!
\patch 1.24 Date 9/21/2001 by Ondra
- Fixed: Lighting of polygons by point lights was reversed on HW T&L.
\patch 1.32 Date 11/26/2001 by Ondra
- Fixed: Destroyed buildings did not collapse with HW T&L.
*/


bool VertexStaticData::AddShape(const Shape &src, int &vIndex, int &iIndex, bool dynamic)
{

	// calculate how many triangle we will need to render this buffer
	int indices = 0;
	for (Offset o=src.BeginFaces(); o<src.EndFaces(); src.NextFace(o))
	{
		const Poly &poly = src.Face(o);
		if (poly.N()<3) continue;
		indices += (poly.N()-2)*3;
	}

	if (_iBufferUsed+indices>_iBufferSize)
	{
		Log("No more space in index buffer - %d shapes",_nShapes);
		Log
		(
			"  used: I %d of %d, V %d of %d",
			_iBufferUsed,_iBufferSize,
			_vBufferUsed,_vBufferSize
		);
		return false;
	}
	if (!dynamic && _vBufferUsed+src.NVertex()>_vBufferSize)
	{
		Log("No more space in vertex buffer - %d shapes",_nShapes);
		Log
		(
			"  used: I %d of %d, V %d of %d",
			_iBufferUsed,_iBufferSize,
			_vBufferUsed,_vBufferSize
		);
		return false; 
	}

	_nShapes++;
	// when adding dynamic data, add only indices

	DWORD flags = NoSysLockFlag;


	iIndex = _iBufferUsed;


	if (!dynamic)
	{
		vIndex = _vBufferUsed;
		RetryV:
		BYTE *data = NULL;
		HRESULT err;
		{
			PROFILE_DX_SCOPE(3vbLS);
			err = _vBuffer->Lock
			(
				_vBufferUsed*sizeof(SVertex),src.NVertex()*sizeof(SVertex),
				&data,flags
			);
		}

		if (err!=D3D_OK || !data)
		{
			if (QFBank::FreeUnusedBanks(1024*1024)) goto RetryV;
			ErrF("VB Lock failed, %s",DXGetErrorString8(err));
			return false;
		}

		SVertex *sData = (SVertex *)data;
		const UVPair *uv = &src.UV(0);
		const Vector3 *pos = &src.Pos(0);
		const Vector3 *norm = &src.Norm(0);
		for (int i=src.NVertex(); --i>=0;)
		{
			#if _KNI //|| _T_MATH
			sData->pos = Vector3P(pos->X(),pos->Y(),pos->Z());
			sData->norm = Vector3P(-norm->X(),-norm->Y(),-norm->Z());
			#else
			sData->pos = *pos;
			sData->norm = -*norm;
			#endif
			pos++,norm++;
			sData->t0 = *uv;
			//sData->t1 = *uv;
			uv++;
			sData++;
		}
		_vBuffer->Unlock();
		_vBufferUsed += src.NVertex();
	}
	else
	{
		vIndex = 0;
	}


	if (indices>0)
	{
		RetryI:
		BYTE *data = NULL;
		HRESULT err;
		{
			PROFILE_DX_SCOPE(3ibLS);
			err = _iBuffer->Lock
			(
				_iBufferUsed*sizeof(VertexIndex),indices*sizeof(VertexIndex),
				&data,NoSysLockFlag
			);
			
		}
		if (err!=D3D_OK || !data)
		{
			if (QFBank::FreeUnusedBanks(1024*1024)) goto RetryI;
			ErrF("IB Lock failed, %s",DXGetErrorString8(err));
			return false;
		}

		VertexIndex *iData = (VertexIndex *)data;
		for (Offset o=src.BeginFaces(); o<src.EndFaces(); src.NextFace(o))
		{
			const Poly &poly = src.Face(o);
			if (poly.N()<3) continue;
			for (int i=2; i<poly.N(); i++)
			{
				#if V_OFFSET_CUSTOM
					*iData++ = poly.GetVertex(0);
					*iData++ = poly.GetVertex(i-1);
					*iData++ = poly.GetVertex(i);
				#else
					*iData++ = poly.GetVertex(0)+vIndex;
					*iData++ = poly.GetVertex(i-1)+vIndex;
					*iData++ = poly.GetVertex(i)+vIndex;
				#endif
			}
		}
		
		_iBuffer->Unlock();
		_iBufferUsed += indices;
	}
	return true;
}

void VertexStaticData::RemoveShape(const Shape &src)
{
	_nShapes--;
	if (_nShapes==0)
	{
		LogF("All static shapes released, resetting");
		_vBufferUsed = 0;
		_iBufferUsed = 0;
	}
}

VertexStaticData *EngineDD::AddShape(const Shape &src, int &vIndex, int &iIndex, bool dynamic)
{
	// check if we will fit in some existing buffer
	for (int i=0; i<_statVBuffer.Size(); i++)
	{
		VertexStaticData *sd = _statVBuffer[i];
		if (sd->AddShape(src,vIndex,iIndex,dynamic))
		{
			return sd;
		}
	}
	VertexStaticData *sd = new VertexStaticData;
	sd->Init(16*1024,40*1024,_d3DDevice);
	if (!sd->_iBuffer || !sd->_vBuffer)
	{
    delete sd;
		return NULL;
	}
	_statVBuffer.Add(sd);
	if (sd->AddShape(src,vIndex,iIndex,dynamic))
	{
		return sd;
	}
	return NULL;
}


VertexDynamicData::VertexDynamicData()
{
}

void VertexDynamicData::Init(int size, IDirect3DDevice8 *dev)
{
	DWORD usage = WRITEONLYVB|D3DUSAGE_DYNAMIC;
	int byteSize = size*sizeof(SVertex);
	RetryV:
	HRESULT err=dev->CreateVertexBuffer
	(
		byteSize,usage,MYSFVF,D3DPOOL_DEFAULT,
		_vBuffer.Init()
	);
	if (err!=D3D_OK)
	{
		if (err==D3DERR_OUTOFVIDEOMEMORY)
		{
    	if (QFBank::FreeUnusedBanks(byteSize+64*1024)) goto RetryV;
			if (GEngineDD->TextBankDD()->ForcedReserveMemory(byteSize))
			{
				goto RetryV;
			}
		}
    else if (err==E_OUTOFMEMORY)
    {
    	if (QFBank::FreeUnusedBanks(byteSize+64*1024)) goto RetryV;
    }
		DDError("CreateVertexBuffer",err);
    ErrorMessage("CreateVertexBuffer failed - out of memory?");
		return;
		// 
	}
	_vBufferSize = size;
	_vBufferUsed = 0;
}
void VertexDynamicData::Dealloc()
{
	_vBuffer.Free();
}

int VertexDynamicData::AddVertices(const Shape &src)
{
	DWORD flags = NoSysLockFlag;
	int n = src.NVertex();
	if (_vBufferUsed+n>=_vBufferSize)
	{
		// discard old - start a new buffer
		flags |= DiscardLockFlag;
		_vBufferUsed = 0;
		if (src.NVertex()>_vBufferSize)
		{
			LogF
			(
				"Cannot copy shape - vbuffer to small (%d<%d)",
				_vBufferSize,src.NVertex()
			);
		}
		
	}
	else
	{
		// fit in current buffer
		flags |= D3DLOCK_NOOVERWRITE;
	}

	int size = sizeof(SVertex)*n;
	int offset = sizeof(SVertex)*_vBufferUsed;
	Retry:
	BYTE *data = NULL;
	HRESULT err;
	{
		PROFILE_DX_SCOPE(3vbLD);
		err = _vBuffer->Lock(offset,size,&data,flags);
	}
	if (err!=D3D_OK || !data)
	{
		if (QFBank::FreeUnusedBanks(1024*1024)) goto Retry;
		ErrF("VB Lock failed, %s",DXGetErrorString8(err));
		return 0;
	}
	int ret = _vBufferUsed;
	_vBufferUsed += n;

	if( err!=D3D_OK )
	{
		DDError("Cannot lock vertex buffer.",err);
	}
	else
	{
		SVertex *sData = (SVertex *)data;
		const UVPair *uv = &src.UV(0);
		const Vector3 *pos = &src.Pos(0);
		const Vector3 *norm = &src.Norm(0);
		for (int i=src.NVertex(); --i>=0;)
		{
			#if _KNI //  || _T_MATH
			// TODO: better template math support
			sData->pos = Vector3P(pos->X(),pos->Y(),pos->Z());
			sData->norm = Vector3P(-norm->X(),-norm->Y(),-norm->Z());
			#else
			sData->pos = *pos;
			sData->norm = -*norm;
			#endif
			pos++,norm++;
			sData->t0 = *uv;
			//sData->t1 = *uv;
			uv++;
			sData++;
		}

		err = _vBuffer->Unlock();
		if( err!=D3D_OK )
		{
			DDError("Cannot unlock vertex buffer.",err);
		}
	}

	return ret;
}


class VertexBufferD3D: public VertexBuffer
{
	friend class EngineDD;

	private:

	bool _separate;
	bool _dynamic;

	AutoArray<VBSectionInfo> _sections;
	int _iOffset; // where are indices placed in shared static buffer
	int _vOffset; // vertex index offsetting done by D3D

	// following variables make sense only for "separate" vertex buffers
	ComRef<IDirect3DVertexBuffer8> _vBuffer;
	ComRef<IDirect3DIndexBuffer8> _iBuffer;
	int _iBufferSize;
	int _vBufferSize;

	// following are valid only for "shared" (!_separate)
	Link<VertexStaticData> _shared;

	protected:
	void CopyData( const Shape &src );
	void AllocateIndices(int indices);
	void AllocateVertices(int vertices);

	public:
	VertexBufferD3D();
	~VertexBufferD3D();

	bool Init(const Shape &src, VBType type);

	//! update buffer, force vertex updates if necessary
	void Update( const Shape &src, bool dynamic );
};

VertexBufferD3D::VertexBufferD3D()
{
	_iOffset = 0;
	_vOffset = 0; // offsetting done by D3D
}

VertexBufferD3D::~VertexBufferD3D()
{
	if (!_separate)
	{
		if (_shared)
		{
			_shared->RemoveShape(*(Shape *)NULL);
		}
	}
	#if 0
	if (_vBuffer)
	{
		char name[256];
		sprintf(name,"VB %x (%x)",_vBuffer,this);
		vbStats.Count(name,-1);

		//LogF("Destroyed VB %x (%x)",_vBuffer,this);
	}
	#endif
	/*
	if (_iBuffer)
	{
		//LogF("Destroy IB %x",_iBuffer);
	}
	*/
	//_vBuffer.Free();
	//_iBuffer.Free();
}

VertexBuffer *EngineDD::CreateVertexBuffer(const Shape &src, VBType type)
{
	#if TL_OBJ_BUF
	if (!_useDXTL) return NULL;
	VertexBufferD3D *buffer = new VertexBufferD3D;
	if (buffer->Init(src,type))
	{
		return buffer;
	}
	else
	{
		delete buffer;
		return NULL;
	}
	#else
	return NULL;
	#endif
}

int EngineDD::CompareBuffers(const Shape &s1, const Shape &s2)
{
	if (!EnableHWTL || !_useDXTL) return 0;
	// check if there are some vertex buffers
	const VertexBufferD3D *b1 = static_cast<const VertexBufferD3D *>(s1.GetVertexBuffer());
	const VertexBufferD3D *b2 = static_cast<const VertexBufferD3D *>(s2.GetVertexBuffer());
	int dd;
	if (!b1 && !b2) return 0;
	if (!b1) return -1;
	if (!b2) return +1;
	dd = b1->_separate && b2->_separate;
	if (dd) return dd;
	dd = b1->_dynamic-b2->_dynamic;
	if (dd) return dd;
	return (char *)b1->_shared.GetTypeRef()-(char *)b2->_shared.GetTypeRef();
}

void VertexBufferD3D::AllocateIndices(int indices)
{
	_iBufferSize = 0;
	Assert (!_iBuffer);
	_iBuffer.Free();
	IDirect3DDevice8 *device = GEngineDD->GetDirect3DDevice();

	if (indices>0)
	{

		int byteSize = indices*sizeof(VertexIndex);
		RetryI:
		HRESULT err = device->CreateIndexBuffer
		(
			byteSize,WRITEONLYIB,D3DFMT_INDEX16,D3DPOOL_DEFAULT,
			_iBuffer.Init()
		);

		if (err!=D3D_OK)
		{
			if (err==D3DERR_OUTOFVIDEOMEMORY)
			{
      	if (QFBank::FreeUnusedBanks(byteSize+64*1024)) goto RetryI;
				if (GEngineDD->TextBankDD()->ForcedReserveMemory(byteSize))
				{
					goto RetryI;
				}
			}
      else if (err==E_OUTOFMEMORY)
      {
    		if (QFBank::FreeUnusedBanks(byteSize+64*1024)) goto RetryI;
      }
			DDError("CreateIndexBuffer",err);
      ErrorMessage("CreateIndexBuffer failed %x - out of memory?",err);
			return;
		}
		//LogF("Created IB %x",_iBuffer);
	}
	_iBufferSize = indices;
}

void VertexBufferD3D::AllocateVertices(int vertices)
{
	//_vBufferSize = 0;

	//Assert (!_vBuffer);

	//_vBuffer.Free();

	IDirect3DDevice8 *device = GEngineDD->GetDirect3DDevice();


	int byteSize = vertices*sizeof(SVertex);
	RetryV:
	// only static data are stored in static buffer
	HRESULT err=device->CreateVertexBuffer
	(
		byteSize,WRITEONLYVB,MYSFVF,D3DPOOL_DEFAULT,
		_vBuffer.Init()
	);

	if (err!=D3D_OK)
	{
		if (err==D3DERR_OUTOFVIDEOMEMORY)
		{
    	if (QFBank::FreeUnusedBanks(byteSize+64*1024)) goto RetryV;
			if (GEngineDD->TextBankDD()->ForcedReserveMemory(byteSize))
			{
				goto RetryV;
			}
		}
    else if (err==E_OUTOFMEMORY)
    {
    	if (QFBank::FreeUnusedBanks(byteSize+64*1024)) goto RetryV;
    }
		DDError("CreateVertexBuffer",err);
    ErrorMessage("CreateVertexBuffer (static, separate) failed %x - out of memory?",err);
		return;
		// 
	}
	#if 0
	LogF("Created VB %x (%x)",_vBuffer,this);
	char name[256];
	sprintf(name,"VB %x (%x)",_vBuffer,this);
	vbStats.Count(name,+1);
	#endif
	_vBufferSize = vertices;

}

bool VertexBufferD3D::Init(const Shape &src, VBType type)
{
	// note: when device is reset, all buffers need to be released and created again
	if (src.NVertex()<=0)
	{
		LogF("Empty vertices.");
		return false;
	}
	// currently only dynamic buffers are used 
	//dynamic = true;

	// create and fill index buffer
	// note: index buffer is almost never changed
	switch (type)
	{
/*
		case VBSmallDiscardable:
			_dynamic = true;
			_separate = true;
			break;
*/
		case VBStatic:
			_dynamic = false;
			_separate = false;
			break;
		case VBDynamic:
			_dynamic = true;
			_separate = false;
			break;
		case VBBigDiscardable:
			_dynamic = false;
			_separate = true;
			break;
		default:
			_dynamic = true;
			_separate = true;
			break;
	}
	_iOffset = 0;
	_vOffset = 0;

	int vOffset = 0;
	if (!_separate)
	{
		_shared = GEngineDD->AddShape(src,vOffset,_iOffset,_dynamic);
		if (!_shared)
		{
			// mark fall-back
			_separate = true;
		}
		_vOffset = vOffset;
	}
	if (_shared)
	{

		// scan sections
		_sections.Realloc(src.NSections());
		_sections.Resize(src.NSections());
		int start = 0;
		for (int i=0; i<src.NSections(); i++)
		{
			int minV = INT_MAX;
			int maxV = 0;
			const ShapeSection &sec = src.GetSection(i);
			// calculate how much indices is used for this section
			int size = 0;
			for (Offset o=sec.beg; o<sec.end; src.NextFace(o))
			{
				const Poly &face = src.Face(o);
				Assert(face.N()>=3);
				// check vertex indices used
				for (int vv=0; vv<face.N(); vv++)
				{
					int vi = face.GetVertex(vv);
					saturateMin(minV,vi);
					saturateMax(maxV,vi);
				}
				size += (face.N()-2)*3;
			}
			// record section info
			_sections[i].beg = start;
			_sections[i].end = start+size;
			#if V_OFFSET_CUSTOM
			_sections[i].begVertex = minV;
			_sections[i].endVertex = maxV+1;
			#else
			_sections[i].begVertex = minV+vOffset;
			_sections[i].endVertex = maxV+vOffset+1;
			#endif
			start += size;
		}
	}
	else // !_shared
	{
		// not shared - not required or no space for sharing
		if (!_dynamic)
		{
			AllocateVertices(src.NVertex());
			if (!_vBuffer)
			{
				// vertex buffer allocation failed - 
				return false;
			}
			CopyData(src);
		}

		// calculate how many triangle we will need to render this buffer
		int indices = 0;
		for (Offset o=src.BeginFaces(); o<src.EndFaces(); src.NextFace(o))
		{
			const Poly &poly = src.Face(o);
			Assert(poly.N()>=3);
			indices += (poly.N()-2)*3;
		}


		if (indices>0)
		{
			AllocateIndices(indices);

			Retry:
			BYTE *data = NULL;
			HRESULT err;
			{
				PROFILE_DX_SCOPE(3ibLS);
				err = _iBuffer->Lock(0,indices*sizeof(VertexIndex),&data,NoSysLockFlag);
			}
			if (err!=D3D_OK || !data)
			{
				if (QFBank::FreeUnusedBanks(1024*1024)) goto Retry;
				ErrF("IB Lock failed, %s",DXGetErrorString8(err));
				return false;
			}

			VertexIndex *iData = (VertexIndex *)data;
			for (Offset o=src.BeginFaces(); o<src.EndFaces(); src.NextFace(o))
			{
				const Poly &poly = src.Face(o);
				Assert(poly.N()>=3);
				for (int i=2; i<poly.N(); i++)
				{
					*iData++ = poly.GetVertex(0);
					*iData++ = poly.GetVertex(i-1);
					*iData++ = poly.GetVertex(i);
				}
			}
			
			_iBuffer->Unlock();

			// scan sections
			_sections.Realloc(src.NSections());
			_sections.Resize(src.NSections());
			int start = 0;
			for (int i=0; i<src.NSections(); i++)
			{
				const ShapeSection &sec = src.GetSection(i);
				// calculate how much indices is used for this section
				int size = 0;
				int minV = INT_MAX;
				int maxV = 0;
				for (Offset o=sec.beg; o<sec.end; src.NextFace(o))
				{
					const Poly &face = src.Face(o);
					Assert(face.N()>=3);
					size += (face.N()-2)*3;

					for (int vv=0; vv<face.N(); vv++)
					{
						int vi = face.GetVertex(vv);
						saturateMin(minV,vi);
						saturateMax(maxV,vi);
					}
				}
				// record section info
				_sections[i].beg = start;
				_sections[i].end = start+size;
				_sections[i].begVertex = minV;
				_sections[i].endVertex = maxV+1;
				start += size;
			}
		}
		_iOffset = 0;
		_vOffset = 0;
		// vertex offset is acutall determined dynamically
		// after vertices are placed to dynamic vertex buffer
	}

	// index buffer remains constant - animation is done on vertices
	// or whole sections
	return true;
	
}

void VertexBufferD3D::CopyData( const Shape &src )
{
	Assert (_separate);
	Assert (!_dynamic);
	if (_vBufferSize<=0) return;
	// copy source data from the mesh
	//DWORD size;

	DWORD flags = NoSysLockFlag;

	Retry:
	BYTE *data = NULL;
	HRESULT err;
	{
		PROFILE_DX_SCOPE(3vbLS);
		err = _vBuffer->Lock(0,_vBufferSize*sizeof(SVertex),&data,flags);
	}

	if (err!=D3D_OK || !data)
	{
		if (QFBank::FreeUnusedBanks(1024*1024)) goto Retry;
		ErrF("VB Lock failed, %s",DXGetErrorString8(err));
		return;
	}

	SVertex *sData = (SVertex *)data;
	const UVPair *uv = &src.UV(0);
	const Vector3 *pos = &src.Pos(0);
	const Vector3 *norm = &src.Norm(0);
	for (int i=src.NVertex(); --i>=0;)
	{
		#if _KNI //|| _T_MATH
			sData->pos = Vector3P(pos->X(),pos->Y(),pos->Z());
			sData->norm = Vector3P(-norm->X(),-norm->Y(),-norm->Z());
		#else
			sData->pos = *pos;
			sData->norm = -*norm;
		#endif
		pos++,norm++;
		sData->t0 = *uv;
		//sData->t1 = *uv;
		uv++;
		sData++;
	}
	_vBuffer->Unlock();

}


void VertexBufferD3D::Update( const Shape &src, bool dynamic  )
{
	#if !V_OFFSET_CUSTOM
		dynamic = false;
	#endif
	if (!_dynamic && !dynamic)
	{
		// use static buffer as source
		GEngineDD->SetVSourceStatic(*this);
	}
	else
	{
		// update dynamic buffer - and use it as source
		GEngineDD->SetVSourceDynamic(src,*this);
	}

	// note: no need to set indices / vertices when drawing same shape again
	// this can be quite common

}

void EngineDD::SetVSourceStatic
(
	const VertexBufferD3D &buf
)
{
	PROFILE_DX_SCOPE(3vbS);
	if (!buf._shared)
	{
		if (_vBufferLast!=buf._vBuffer)
		{
			ADD_COUNTER(d3xVB,1);
			_d3DDevice->SetStreamSource(0,buf._vBuffer,sizeof(SVertex));
			_vBufferLast = buf._vBuffer;
			_lastQueueSource = NULL;
			_vBufferSize = buf._vBufferSize;

			#if DO_TEX_STATS
			if (LogStatesOnce)
			{
				LogF("SetVBSourceStatic separate");
			}
			#endif

		}
		int vOffset = 0;
		if (_iBufferLast!=buf._iBuffer || _vOffsetLast!=vOffset)
		{
			ADD_COUNTER(d3xIB,1);
			_d3DDevice->SetIndices(buf._iBuffer,vOffset);
			_iBufferLast = buf._iBuffer;
			_vOffsetLast = vOffset;
			_lastQueueSource = NULL;

			#if DO_TEX_STATS
			if (LogStatesOnce)
			{
				LogF("SetIBSourceStatic separate");
			}
			#endif
		}
		_iOffset = buf._iOffset;
	}
	else
	{
		VertexStaticData *dta = buf._shared;
		if (_vBufferLast!=dta->_vBuffer)
		{
			ADD_COUNTER(d3xVB,1);
			_d3DDevice->SetStreamSource(0,dta->_vBuffer,sizeof(SVertex));
			_vBufferLast = dta->_vBuffer;
			_lastQueueSource = NULL;
			_vBufferSize = dta->_vBufferSize;
			#if DO_TEX_STATS
			if (LogStatesOnce)
			{
				LogF("SetVBSourceStatic shared");
			}
			#endif
		}
		#if V_OFFSET_CUSTOM
		int vOffset = buf._vOffset;
		#else
		int vOffset = 0;
		#endif

		if (_iBufferLast!=dta->_iBuffer || _vOffsetLast!=vOffset)
		{
			ADD_COUNTER(d3xIB,1);
			_d3DDevice->SetIndices(dta->_iBuffer,vOffset);
			_iBufferLast = dta->_iBuffer;
			_vOffsetLast = vOffset;
			_lastQueueSource = NULL;
			#if DO_TEX_STATS
			if (LogStatesOnce)
			{
				LogF("SetIBSourceStatic shared");
			}
			#endif
		}
		_iOffset = buf._iOffset;
	}
}

void EngineDD::SetVSourceDynamic
(
	const Shape &src, const VertexBufferD3D &buf
)
{
	PROFILE_DX_SCOPE(3vbD);
	int vOffset = _dynVBuffer.AddVertices(src);

	if (_vBufferLast!=_dynVBuffer._vBuffer)
	{
		#if DO_TEX_STATS
		if (LogStatesOnce)
		{
			LogF("SetVBSourceDynamic");
		}
		#endif
		ADD_COUNTER(d3xVB,1);
		_d3DDevice->SetStreamSource(0,_dynVBuffer._vBuffer,sizeof(SVertex));
		_vBufferLast = _dynVBuffer._vBuffer;
		_lastQueueSource = NULL;
		_vBufferSize = _dynVBuffer._vBufferSize;
	}

	if (buf._separate)
	{
		
		if (_iBufferLast!=buf._iBuffer || vOffset!=_vOffsetLast)
		{

			#if DO_TEX_STATS
			if (LogStatesOnce)
			{
				LogF("SetIBSourceDynamic separate");
			}
			#endif

			ADD_COUNTER(d3xIB,1);
			_d3DDevice->SetIndices(buf._iBuffer,vOffset);
			_iBufferLast = buf._iBuffer;
			_vOffsetLast = vOffset;
			_lastQueueSource = NULL;
		}
		_iOffset = buf._iOffset;
		//_vOffset = buf._vOffset;
	}
	else
	{
		VertexStaticData *dta = buf._shared;
		if (_iBufferLast!=dta->_iBuffer || vOffset!=_vOffsetLast)
		{
			#if DO_TEX_STATS
			if (LogStatesOnce)
			{
				LogF("SetIBSourceDynamic shared");
			}
			#endif

			ADD_COUNTER(d3xIB,1);
			_d3DDevice->SetIndices(dta->_iBuffer,vOffset);
			_iBufferLast = dta->_iBuffer;
			_vOffsetLast = vOffset;
			_lastQueueSource = NULL;
		}
		_iOffset = buf._iOffset;
		//_vOffset = buf._vOffset;
	}
}

#define SET_COLOR(d,s) d.r = s.R(),d.g = s.G(),d.b = s.B(),d.a = s.A()
#define SET_WHITE(d,av) d.r = d.g = d.b = 1, d.a = av
#define SET_BLACK(d,av) d.r = d.g = d.b = 0, d.a = av
#define SET_POS(d,s) d.x = s.X(),d.y = s.Y(),d.z = s.Z()

/*!
\patch 1.20 Date 8/16/2001 by Ondra
- Fixed: Night vision with HW T&L has no longer colored areas near light.
\patch_internal 1.24 Date 9/27/2001 by Ondra
- Fixed: HW T&L spotlight parameters adjusted.
\patch 1.24 Date 9/27/2001 by Ondra
- Fixed: Brightness factor was applied twice in HW T&L daytime lighting.
*/
static void ConvertLight
(
	D3DLIGHT8 &desc, const Light *light, float night,
	const TLMaterial &mat
)
{
	LightDescription ldesc;
	light->GetDescription(ldesc);
	// convert description to D3D format
	switch (ldesc.type)
	{
		default: desc.Type = D3DLIGHT_DIRECTIONAL; break;
		case LTPoint: desc.Type = D3DLIGHT_POINT; break;
		case LTSpotLight: desc.Type = D3DLIGHT_SPOT; break;
	}

	SET_POS(desc.Position,ldesc.pos);
	SET_POS(desc.Direction,ldesc.dir);
	//SET_COLOR(desc.Ambient,HBlack);
	SET_COLOR(desc.Diffuse,(ldesc.diffuse*night*mat.diffuse));
	//SET_COLOR(desc.Specular,HBlack);
	SET_COLOR(desc.Specular,(ldesc.diffuse*night*0.025f));
	desc.Falloff = 1;
	desc.Theta = ldesc.theta;
	desc.Phi = ldesc.phi;
	desc.Range = ldesc.startAtten*10;
	desc.Attenuation0 = 0;
	desc.Attenuation1 = 0;
	if (ldesc.type==LTSpotLight)
	{
		desc.Attenuation2 = 4.0f/Square(ldesc.startAtten);
		SET_COLOR(desc.Ambient,(ldesc.ambient*night*mat.ambient*0.3));
	}
	else
	{
		desc.Attenuation2 = 1.0f/Square(ldesc.startAtten);
		SET_COLOR(desc.Ambient,(ldesc.ambient*night*mat.ambient));
	}
}

bool EngineDD::IsWBuffer() const
{
	return _useWBuffer;
}

bool EngineDD::CanWBuffer() const
{
	return _canWBuffer && (_depthBpp<=16 || _canWBuffer32);
}

void EngineDD::SetWBuffer(bool val)
{
	bool change = false;
	if (_userEnabledWBuffer != val) change  = true;
	_userEnabledWBuffer = val;
	if (CanWBuffer())
	{
		if (_useWBuffer != val) change  = true;
		_useWBuffer = val;
		if (change) ResetHard();
	}
}

/*!
\patch 1.01 Date 6/18/2001 by Ondra
- Fixed: T&L lights in daytime
*/

void EngineDD::SetupLights(const TLMaterial &mat, const LightList &lights, int spec)
{
	// TODO: lights persistent
	// disable all other lights
	// setup sun
	LightSun *sun = GScene->MainLight();

	// set directional light and material
	D3DLIGHT8 tllight;
	memset(&tllight,0,sizeof(tllight));
	tllight.Type = D3DLIGHT_DIRECTIONAL;

	Color dif = sun->Diffuse()*mat.diffuse;
	Color amb = sun->Ambient()*mat.ambient+sun->Diffuse()*mat.forcedDiffuse;
	Vector3 dir = sun->Direction();
	SET_COLOR(tllight.Specular,sun->Diffuse());
	SET_COLOR(tllight.Diffuse,dif);
	//SET_COLOR(tllight.Diffuse,Color(HBlack));
	SET_COLOR(tllight.Ambient,amb);
	SET_POS(tllight.Position,VZero);
	SET_POS(tllight.Direction,dir);

	_d3DDevice->SetLight(0,&tllight);

	#if VERTEX_SHADERS
	if (_vertexShaders>=0x100)
	{
		float lightDir[4];
		lightDir[0] = dir[0];
		lightDir[1] = dir[1];
		lightDir[2] = dir[2];
		lightDir[3] = 0;

		_d3DDevice->SetVertexShaderConstant(LIGHT_SUN_DIFFUSE,(float *)&tllight.Diffuse,1);
		_d3DDevice->SetVertexShaderConstant(LIGHT_SUN_AMBIENT,(float *)&tllight.Ambient,1);
		_d3DDevice->SetVertexShaderConstant(LIGHT_SUN_DIRECTION,lightDir,1);
	}
	#endif

	#if DO_TEX_STATS
	if (LogStatesOnce)
	{
		LogF
		(
			"Set light 0 dif %.2f,%.2f,%.2f amb %.2f,%.2f,%.2f, xfer %.2f,%.2f,%.2f",
			dif.R(),dif.G(),dif.B(),
			amb.R(),amb.G(),amb.B(),
			mat.forcedDiffuse.R(),mat.forcedDiffuse.G(),mat.forcedDiffuse.B()
		);
	}
	#endif

	_d3DDevice->LightEnable(0,_sunEnabled);

	// first simple implementation: no light reusing
	// all lights set-up per object again and again
	int nLights = lights.Size();
	// FIX T&L lights in daytime
	float night = GScene->MainLight()->NightEffect();
	if ((mat.specFlags|spec)&DisableSun) night = 1;
	if (night<=0) nLights = 0;
	// limit lights by device caps
	saturateMin(nLights,_maxLights-1);

	int oLights = _lights.Size();
	_lights.Resize(nLights);
	for (int i=0; i<nLights; i++)
	{
		const Light *light = lights[i];
		LightDescription ldesc;
		D3DLIGHT8 desc;
		ConvertLight(desc,light,night,mat);

		#if DO_TEX_STATS
		if (LogStatesOnce)
		{
			LogF
			(
				"Set light %d dif %.2f,%.2f,%.2f amb %.2f,%.2f,%.2f",
				i+1,
				desc.Diffuse.r,desc.Diffuse.g,desc.Diffuse.b,
				desc.Ambient.r,desc.Ambient.g,desc.Ambient.b
			);
		}
		#endif
		_d3DDevice->SetLight(i+1,&desc);
		if (i>=oLights || !_lights[i]) _d3DDevice->LightEnable(i+1,TRUE);
		_lights[i] = const_cast<Light *>(light);

	}
	#if DO_TEX_STATS
	if (LogStatesOnce)
	{
		LogF("No more lights");
	}
	#endif
	// turn off all unused lights
	for (int i=nLights; i<oLights; i++)
	{
		_d3DDevice->LightEnable(i+1,FALSE);
	}
}

inline float PlaneDistance2(const Plane &p1, const Plane &p2)
{
	float d = Square(p1.D()-p2.D());
	d += p1.Normal().Distance2(p2.Normal());
	return d;
}

void EngineDD::ChangeClipPlanes()
{
	// clip planes currently supported only via pixel shaders

	/*
	// 0 is near additional clip plane
	// 1 is far additional clip plane
	Camera *cam = GScene->GetCamera();
	float cANear = cam->GetAdditionalClippingNear();
	float cAFar = cam->GetAdditionalClippingFar();
	float cNear = cam->ClipNear();
	float cFar = cam->ClipFar();

	bool enableNear = cANear>cNear;
	bool enableFar = cAFar<cFar;
	if (enableNear)
	{
		const Plane &p = cam->GetNearClipPlane();
		if (!_clipANearEnabled || PlaneDistance2(p,_clipANear)>1e-6)
		{
			_d3DDevice->SetClipPlane(0,(float *)&p);
			_clipANear = p;
		}
 	}
	if (enableFar)
	{
		const Plane &p = cam->GetFarClipPlane();
		if (!_clipAFarEnabled || PlaneDistance2(p,_clipAFar)>1e-6)
		{
			_d3DDevice->SetClipPlane(1,(float *)&p);
			_clipAFar = p;
		}
 	}
	if (enableNear!=_clipANearEnabled || enableFar!=_clipAFarEnabled)
	{
		DWORD planesEnabled = int(enableNear)|(int(enableFar)<<1);
		_d3DDevice->SetRenderState(D3DRS_CLIPPLANEENABLE,planesEnabled);
		_clipANearEnabled = enableNear;
		_clipAFarEnabled = enableFar;
	}
	*/
}

void EngineDD::ChangeWDepth(float matC, float matD)
{
	if (_matC==matC && _matD==matD) return;
	_matC = matC, _matD = matD;
	if (!_useWBuffer) return;


	if (_tlActive)
	{
		Fail("ChangeWDepth in not compatible with HW T&L");
		D3DMATRIX projMatrix;
		// set projection (based on camera)
		int bias = _bias;
		if (_canZBias) bias = 0;

		Camera *camera = GScene->GetCamera();
		ConvertProjectionMatrix(projMatrix,camera->ProjectionNormal(),bias);

		// adjust z scale
		projMatrix._33 = matC;
		projMatrix._43 = matD;

		_d3DDevice->SetTransform( D3DTS_PROJECTION , &projMatrix );
		LogSetProj(projMatrix);
	}
	else
	{

		D3DMATRIX mat;

		mat._11 = 1;
		mat._12 = 0;
		mat._13 = 0;
		mat._14 = 0;

		mat._21 = 0;
		mat._22 = 1;
		mat._23 = 0;
		mat._24 = 0;

		mat._31 = 0;
		mat._32 = 0;
		mat._33 = matC;
		mat._34 = 1;

		mat._41 = 0;
		mat._42 = 0;
		mat._43 = matD;
		mat._44 = 0;

		// we have to set projection matrix - D3D needs wNear a wFar from it

		_d3DDevice->SetTransform( D3DTS_PROJECTION , &mat );
		LogSetProj(mat);
	}

}

void EngineDD::DoSwitchTL( bool active )
{
	FlushAndFreeAllQueues(_queueNo);
	//FlushAndFreeAllQueues(_queueTL);
	_tlActive = active;
	if (active)
	{
		PROFILE_DX_SCOPE(3eTL);
		#if DO_TEX_STATS
		if (LogStatesOnce)
		{
			LogF("SetVertexShader T&L");
		}
		#endif
		#if VERTEX_SHADERS
		if (_vertexShaders>=0x100)
		{
			DoSelectVertexShader(_vertexShaderSel);
			//Log("Current T&L Vertex shader %x selected",_vertexShader[_vertexShaderSel]);
		}
		else
		#endif
		{
			_d3DDevice->SetVertexShader(MYSFVF);
			//Log("Default T&L Vertex shader %x selected",MYSFVF);
		}
		// we must reset all material and lights

		D3DSetRenderState(D3DRS_LIGHTING,TRUE);
		#ifndef _XBOX
		D3DSetRenderState(D3DRS_CLIPPING,TRUE);
		#endif

		// these two are scene global
		Camera *camera = GScene->GetCamera();
		D3DMATRIX viewMatrix;
		// set also world to view (based on camera)
		ConvertMatrix(viewMatrix,camera->InverseScaled());
		_d3DDevice->SetTransform( D3DTS_VIEW, &viewMatrix );

		D3DMATRIX projMatrix;
		// set projection (based on camera)
		int bias = _bias;
		if (_canZBias) bias = 0;
		ConvertProjectionMatrix(projMatrix,camera->ProjectionNormal(),bias);
		// check z scale

		_d3DDevice->SetTransform( D3DTS_PROJECTION , &projMatrix );
		LogSetProj(projMatrix);

    float wFogStart      = ( 0.2*(camera->ClipFar()-camera->ClipNear()) ) + camera->ClipNear();
    float wFogEnd        = camera->ClipFar();

		float fogDens = 1.0;

		D3DSetRenderState(D3DRS_FOGSTART,*(DWORD *)&wFogStart);
		D3DSetRenderState(D3DRS_FOGEND,*(DWORD *)&wFogEnd);
		D3DSetRenderState(D3DRS_FOGDENSITY,*(DWORD *)&fogDens);

		#ifndef _XBOX
		D3DSetRenderState(D3DRS_FOGTABLEMODE,D3DFOG_NONE);
		D3DSetRenderState(D3DRS_FOGVERTEXMODE,D3DFOG_LINEAR);
		#else
		D3DSetRenderState(D3DRS_FOGTABLEMODE,D3DFOG_LINEAR);
		#endif
		D3DSetRenderState(D3DRS_RANGEFOGENABLE,TRUE);

	}
	else
	{
		#if DO_TEX_STATS
		if (LogStatesOnce)
		{
			LogF("SetVertexShader dummy");
		}
		#endif
		PROFILE_DX_SCOPE(3dTL);
		_d3DDevice->SetVertexShader(MYFVF);
		//Log("Default non-T&L vertex shader %x selected",MYFVF);
		D3DSetRenderState(D3DRS_LIGHTING,FALSE);
		#ifndef _XBOX
		D3DSetRenderState(D3DRS_CLIPPING,FALSE);
		#else
		D3DSetRenderState(D3DRS_FOGTABLEMODE,D3DFOG_NONE);
		#endif
		_iBufferLast.Free(); // forget any VB
		_vBufferLast.Free();
	}

}

void EngineDD::DoSetMaterial(const TLMaterial &mat, const LightList &lights, int spec)
{
	_materialSet = mat;
	_materialSetSpec = spec;
	// set corresponding render states
	// combine material with light


	PROFILE_DX_SCOPE(3mat);

	D3DMATERIAL8 tlmat;
	// only emmisive and specular is actually used from material
	// all other components are propagated to lights
	SET_WHITE(tlmat.Diffuse,mat.diffuse.A());
	SET_WHITE(tlmat.Ambient,mat.ambient.A());
	SET_COLOR(tlmat.Specular,mat.specular);
	SET_COLOR(tlmat.Emissive,mat.emmisive);
	tlmat.Power=mat.specularPower;
	_d3DDevice->SetMaterial(&tlmat);
	if (mat.specularPower>0)
	{
		D3DSetRenderState(D3DRS_SPECULARENABLE,TRUE);
		D3DSetRenderState(D3DRS_LOCALVIEWER,TRUE);
		if (_usePixelShaders)
		{
			SelectPixelShaderSpecular(PSSSpecular);
		}
	}
	else
	{
		D3DSetRenderState(D3DRS_SPECULARENABLE,FALSE);
		if (_usePixelShaders)
		{
			SelectPixelShaderSpecular(PSSNormal);
		}
	}
	#if DO_TEX_STATS
	if (LogStatesOnce)
	{
		LogF
		(
			"Set material da %.2f, aa %.2f, em %.2f,%.2f,%.2f",
			mat.diffuse.A(),mat.ambient.A(),
			mat.emmisive.R(),mat.emmisive.G(),mat.emmisive.B()
		);
	}
	#endif

	// adapt lights based on mat. forcedDiffuse

	SetupLights(mat,lights,spec);
}

TypeIsSimple(Light *);

void EngineDD::SetMaterial(const TLMaterial &mat, const LightList &lights, int spec)
{
	// compare material with the one we are currently using
	spec &= DisableSun;
	if (mat==_materialSet && _materialSetSpec==spec)
	{
		// even when material is the same, light list may have been changed
		int nLights = lights.Size();
		// FIX T&L lights in daytime
		if (((mat.specFlags|spec)&DisableSun)==0)
		{
			if (GScene->MainLight()->NightEffect()<=0 ) nLights = 0;
		}
		if (_lights.Size()==0 && nLights==0)
		{
			return;
		}
		#if 0
		/**/
		// TODO: better response to changing lights
		// turn on/off as neccessary
		// check which lights from current list should stay on
		AUTO_STATIC_ARRAY(Light *,pLights,32);

		// limit lights by device caps
		saturateMin(nLights,_maxLights-1);
		pLights.Resize(nLights);
		for (int i=0; i<nLights; i++)
		{
			pLights[i] = const_cast<Light *>(lights[i].GetRef());
		}

		// mark lights as used
		for (int i=0; i<_lights.Size(); i++)
		{
			Light *li = _lights[i];
			int pIndex=-1;
			for (int pi=0; pi<pLights.Size(); pi++)
			{
				if (pLights[pi]==li) {pIndex=pi;break;}
			}
			if (pIndex>=0)
			{
				// light already set-up
				pLights.Delete(pIndex);
				continue;
			}
			_lights[i] = NULL;
			_d3DDevice->LightEnable(i+1,FALSE);
		}
		// if any lights are left in pLights, we have to add them
		for (int i=0; i<pLights.Size(); i++)
		{
			Light *light = pLights[i];
			int index = AddLight(light);
			// adjust light with material
			D3DLIGHT8 desc;
			ConvertLight(desc,light,mat);
			_d3DDevice->SetLight(index+1,&desc);
			_d3DDevice->LightEnable(index+1,TRUE);
		}
		/**/
		return;
		#endif

	}
	DoSetMaterial(mat,lights,spec);
}

void EngineDD::EnableSunLight(bool enable)
{
	if (_sunEnabled==enable) return;
	_sunEnabled=enable;
	if (!_tlActive) return;
	_d3DDevice->LightEnable(0,_sunEnabled);

	_materialSetSpec = -1; // set invalid spec
	// set invalid material to force setting lights
	_materialSet.diffuse = Color(-1,-1,-1,-1);
	_materialSet.ambient = Color(-1,-1,-1,-1);
	_materialSet.forcedDiffuse = Color(-1,-1,-1,-1);
	_materialSet.emmisive = Color(-1,-1,-1,-1);
	_materialSet.specFlags = 0;

}

class Matrix4x4
{
	public:
	float _o[4][4];
	//       c  r
	// columns corresponds to aside,up,dir,pos

	// Matrix3 operator has identical argument ordering: row column
	float &Set(int r, int c) {return _o[c][r];}

	float &operator () (int r, int c) {return _o[c][r];}
	const float &operator () (int r, int c) const {return _o[c][r];}

	void SetZero()
	{
		for (int i=0; i<4; i++)
		{
			_o[i][0]=_o[i][1]=_o[i][2]=_o[i][3] = 0;
		}
	}
	void SetIdentity()
	{
		SetZero();
		_o[0][0]=_o[1][1]=_o[2][2]=_o[3][3] = 1;
	}

	void SetNormal(const Matrix4 &m)
	{
		Set(0,0) = m(0,0);
		Set(1,0) = m(1,0);
		Set(2,0) = m(2,0);
		Set(3,0) = 0;

		Set(0,1) = m(0,1);
		Set(1,1) = m(1,1);
		Set(2,1) = m(2,1);
		Set(3,1) = 0;

		Set(0,2) = m(0,2);
		Set(1,2) = m(1,2);
		Set(2,2) = m(2,2);
		Set(3,2) = 0;

		Set(0,3) = m.Position()[0];
		Set(1,3) = m.Position()[1];
		Set(2,3) = m.Position()[2];;
		Set(3,3) = 1;
	}
	void SetPerspective(const Matrix4 &m)
	{
		Set(0,0) = m(0,0);
		Set(1,0) = m(1,0);
		Set(2,0) = m(2,0);
		Set(3,0) = 0;

		Set(0,1) = m(0,1);
		Set(1,1) = m(1,1);
		Set(2,1) = m(2,1);
		Set(3,1) = 0;

		Set(0,2) = m(0,2);
		Set(1,2) = m(1,2);
		Set(2,2) = m(2,2);
		Set(3,2) = 1;

		Set(0,3) = m.Position()[0];
		Set(1,3) = m.Position()[1];
		Set(2,3) = m.Position()[2];;
		Set(3,3) = 0;
	}
	void SetMultiply(const Matrix4x4 &a, const Matrix4x4 &b)
	{
		for (int i=0; i<4; i++ ) for(int j=0; j<4; j++)
		{
			Set(i,j) =
			(
				a(i,0)*b(0,j)+
				a(i,1)*b(1,j)+
				a(i,2)*b(2,j)+
				a(i,3)*b(3,j)
			);
		}
		
	}
	void Transpose()
	{
		swap(Set(0,1),Set(1,0));swap(Set(0,2),Set(2,0));swap(Set(0,3),Set(3,0));
		swap(Set(1,2),Set(2,1));swap(Set(1,3),Set(3,1));
		swap(Set(2,3),Set(3,2));
	}
};



void EngineDD::PrepareMeshTL( const LightList &lights, const Matrix4 &modelToWorld,  int spec )
{
	// different model matrix - queue flush needed
	//FlushAndFreeAllQueues(_queueTL);

	// prepare internal variables

	{
		PROFILE_DX_SCOPE(3trn);
		D3DMATRIX worldMatrix;
		// we need model to world matrix
		ConvertMatrix(worldMatrix,modelToWorld);
		_d3DDevice->SetTransform( D3DTS_WORLD, &worldMatrix );
		#if VERTEX_SHADERS
		Camera *cam = GScene->GetCamera();
		Matrix4Val proj = cam->ProjectionNormal();
		if (_vertexShaders>=0x100)
		{

			Matrix4 worldView = cam->InverseScaled() *modelToWorld;
			Matrix4x4 worldView4;
			Matrix4x4 proj4;
			Matrix4x4 mat4;

			worldView4.SetNormal(worldView);
			proj4.SetPerspective(proj);
			mat4.SetMultiply(proj4,worldView4);
			
			mat4.Transpose();
			worldView4.Transpose();

			_d3DDevice->SetVertexShaderConstant(CAMERA_MAT_0,&worldView4,4);
			_d3DDevice->SetVertexShaderConstant(TRANS_MAT_0,&mat4,4);
		  float fogStart = ( 0.2*(cam->ClipFar()-cam->ClipNear()) ) + cam->ClipNear();
			float fogEnd   = cam->ClipFar();
			float fogParam[4]=
			{
				fogEnd,1/(fogEnd-fogStart),
				0,0
			};
			_d3DDevice->SetVertexShaderConstant(FOG_PARAM,&fogParam,1);
		} 
		#endif
	}

	SwitchTL(true);
	EnableSunLight((spec&DisableSun)==0);

	//ChangeWDepth(proj(2,2),proj.Position()[2]);
	ChangeClipPlanes();

}

void EngineDD::BeginMeshTL( const Shape &sMesh, int spec, bool dynamic )
{

	// update if necessary

	sMesh.GetVertexBuffer()->Update(sMesh,dynamic);
}

int EngineDD::AddLight(Light *light)
{
	for(int i=0; i<_lights.Size(); i++)
	{
		if (!_lights[i])
		{
			_lights[i] = light;
			return i;
		}
	}
	return _lights.Add(light);
}

void EngineDD::ClearLights()
{
	for(int i=0; i<_lights.Size(); i++)
	{
		if (_lights[i])
		{
			_d3DDevice->LightEnable(i+1,FALSE);
		}
	}
	_lights.Resize(0); // currently active lights
	if (_lights.MaxSize()>64) _lights.Clear();
}

void EngineDD::EndMeshTL( const Shape &sMesh )
{
	//_sBuffer = NULL;
	// turn off all lights
	ClearLights();

}


void EngineDD::DiscardVB()
{
}

void EngineDD::AddVertices( const TLVertex *v, int n )
{
	if( n<=0 ) return;
	if (n>MeshBufferLength)
	{
		RptF("Needed %d vertices, %d available",n,MeshBufferLength);
		ErrorMessage("Vertex Buffer too small");
	}
	// keep meshbuffer allocated as long as possible
	// fill vertex buffer with vertices
	HRESULT err;

	BYTE *data = NULL;
	int size=sizeof(TLVertex)*n;

	#if 1
	if (_queueNo._vertexBufferUsed+n<=MeshBufferLength && !_queueNo._firstVertex)
	{
		#if LOG_LOCKS
		LogF("add D3DLOCK_NOOVERWRITE %d + %d",_queueNo._vertexBufferUsed,n);
		#endif
		int offset = _queueNo._vertexBufferUsed*sizeof(TLVertex);
		Retry:
		{
			PROFILE_DX_SCOPE(3vbLA);
			err=_queueNo._meshBuffer->Lock
			(
				offset,size,&data,NoSysLockFlag|D3DLOCK_NOOVERWRITE
			);
		}
		if( err!=D3D_OK )
		{
			if (err!=D3D_OK || !data)
			{
				if (QFBank::FreeUnusedBanks(1024*1024)) goto Retry;
				ErrF("VB Lock failed, %s",DXGetErrorString8(err));
				return;
			}
		}
		else
		{
			memcpy(data,v,size);
			err=_queueNo._meshBuffer->Unlock();
			if( err!=D3D_OK )  DDError("Cannot unlock vertex buffer.",err);
		}

		_queueNo._meshBase = _queueNo._vertexBufferUsed;
		_queueNo._meshSize = n;
		_queueNo._vertexBufferUsed += n;
	}
	else
	#endif
	{
		_queueNo._firstVertex = false;

		// start a new vertex buffer
		#if DO_TEX_STATS
		if (LogStatesOnce)
		{
			LogF("Vertex buffer full");
		}
		#endif

		FlushAndFreeAllQueues(_queueNo);
		// check if we will fit into actual vertex buffer
		// if not, flush old vertex buffer
		#if LOG_LOCKS
		LogF("add D3DLOCK_DISCARD %d + %d",0,n);
		#endif
		{
			PROFILE_DX_SCOPE(3vbLA);
			err=_queueNo._meshBuffer->Lock
			(
				0,size,&data,NoSysLockFlag|DiscardLockFlag
			);
		}
		if( err!=D3D_OK ) DDError("Cannot lock vertex buffer.",err);
		else
		{
			if (data) memcpy(data,v,size);
			err=_queueNo._meshBuffer->Unlock();
			if( err!=D3D_OK )  DDError("Cannot unlock vertex buffer.",err);
		}
		_queueNo._meshBase = 0;
		_queueNo._meshSize = n;
		_queueNo._vertexBufferUsed = n;
	}
}

void EngineDD::BeginMesh( TLVertexTable &mesh, int spec )
{
	SwitchTL(false);
	_mesh = &mesh;

	AddVertices(mesh.VertexData(),mesh.NVertex());
}

void EngineDD::EndMesh( TLVertexTable &mesh )
{
	_mesh = NULL;
}

Queue::Queue()
{
	for (int i=0; i<MaxTriQueues; i++) _triUsed[i] = false;
	_usedCounter = 0;
	_vertexBufferUsed = 0;
	_indexBufferUsed = 0;
	_meshBase = 0;
	_meshSize = 0;
	_actTri = -1;
	_firstVertex = true;
	_firstIndex = true;
}

int Queue::Allocate
(
	TextureD3D *tex, int level, int spec,
	int minI, int maxI, int tip
)
{
	int index = -1;
	// slot shown by tip may (but need not) contain the setup we need
	if (tip>=minI && tip<maxI && _triUsed[tip])
	{
		TriQueue &triq = _tri[tip];
		if (tex==triq._texture && spec==triq._special) index = tip;
	}


	int free = -1;
	if (index<0)
	{
		for (int i=minI; i<maxI; i++)
		{
			if (_triUsed[i])
			{
				TriQueue &triq = _tri[i];
				if (tex!=triq._texture) continue;
				if (spec!=triq._special) continue;
				index = i;
			}
			else
			{
				if (free<0) free = i;
			}
		}
	}
	_usedCounter++;
	if (index>=0)
	{
		TriQueue &triq = _tri[index];
		// append to queue index
		saturateMin(triq._level,level);
		triq._lastUsed = _usedCounter;
		Assert (_triUsed[index]);
		/*
		if (LogStatesOnce)
		{
			LogF("Allocated old queue %d: %s",index,tex ? tex->Name() : "<NULL>");
		}
		*/
		return index;
	}
	if (free>=0)
	{
		TriQueue &triq = _tri[free];
		//triq._queueBase = _meshBase;
		//triq._queueSize = 0;
		triq._special = spec;
		triq._texture = tex;
		triq._level = level;
		triq._lastUsed = _usedCounter;
		Assert (triq._triangleQueue.Size()==0);
		triq._triangleQueue.Resize(0);
		_triUsed[free] = true;
		#if PIXEL_SHADERS && _ENABLE_REPORT
			if (!tex && GEngineDD->UsingPixelShaders())
			{
				LogF("Warning: null texture black with pixel shaders");
			}
		#endif
		#if DO_TEX_STATS
		if (LogStatesOnce)
		{
			LogF("Allocated new queue %d: %s",free,tex ? tex->Name() : "<NULL>");
		}
		#endif
	}
	return free;
}

void Queue::Free( int i )
{
	Assert (_tri[i]._triangleQueue.Size()==0);
	Assert( _triUsed[i] );
	_triUsed[i] = false;
	#if DO_TEX_STATS
	if (LogStatesOnce)
	{
		LogF("Released queue %d",i);
	}
	#endif
}

void EngineDD::DoSwitchRenderMode(RenderMode mode)
{
	#if DO_TEX_STATS
	if (LogStatesOnce)
	{
		LogF("DoSwitchRenderMode %d",mode);
	}
	#endif
	FlushAndFreeAllQueues(_queueNo);
	_renderMode = mode;
}

WORD *EngineDD::QueueAdd( Queue &queue, int n )
{
	Assert (queue._actTri>=0);
	Assert (queue._triUsed[queue._actTri]);
	TriQueue &triq = queue._tri[queue._actTri];
	if( triq._triangleQueue.Size()+n>TriQueueSize )
	{
		// flush but keep allocated
		#if DO_TEX_STATS
		if (LogStatesOnce)
		{
			LogF("Queue full %d",queue._actTri);
		}
		#endif
		FlushQueue(queue,queue._actTri);
	}

	int index = triq._triangleQueue.Size();
	/*
	if (index==0)
	{
		// starting a new queue content
		triq._queueBase = queue._meshBase;
		triq._queueSize = 0;
	}
	*/
	triq._triangleQueue.Resize(index+n);
	return triq._triangleQueue.Data()+index;
}

void EngineDD::QueueFan( const VertexIndex *ii, int n )
{
	int addN = (n-2)*3;
	//TriQueue &triq = _queueNo._tri[_queueNo._actTri];
	WORD *tgt = QueueAdd(_queueNo,addN);

	if (!tgt)
	{
		Fail("No target in QueueFan");
		return;
	}
	if (!ii)
	{
		Fail("No source in QueueFan");
		return;
	}

	int offset = _queueNo._meshBase;

	Assert( offset>=0 );
	#if DIAG_QUEUE
	LogF
	(
		"Queue fan %d - meshBase %d, queueBase %d, offset %d",
		n,_queueNo._meshBase,triq._queueBase,offset
	);
	for (int i=0; i<n; i++)
	{
		LogF("  %d -> %d",ii[0],ii[0]+offset);
	}
	#endif

	int oii0 = ii[0]+offset;
	int oiip = ii[1]+offset;

	//saturateMax(triq._queueSize,oii0+1);
	//saturateMax(triq._queueSize,oiip+1);

	// add all triangles
	/*
	if (LogStatesOnce)
	{
		LogF("  queue %d",n);
		for( int i=0; i<n; i++ )
		{
			LogF("    queue %d - %d",ii[0],ii[0]+offset);
		}
	}
	*/
	// BUG: crash:
	// tgt (eax) == NULL, ii (ecx) == NULL
	// i (esi) ==2, n (edi) == 4
	// optimize calculations
	const VertexIndex *vi = ii+2;
	for( int i=2; i<n; i++ )
	{
		int oiin = (*vi++) + offset;
		tgt[0] = oii0;
		tgt[1] = oiip;
		tgt[2] = oiin;
		tgt += 3;
		//saturateMax(triq._queueSize,oiin+1);
		oiip = oiin;
	}
}

int EngineDD::AllocateQueue(Queue &queue, TextureD3D *tex, int level, int spec)
{
	// scan if there is some queue with same attributes
	bool alpha = tex && tex->IsAlpha() || !_enableReorder;

	int minI = 0;
	int maxI = MaxTriQueues-1;
	if (alpha)
	{
		minI = MaxTriQueues-1, maxI = MaxTriQueues;
		// flush all other queues
		FlushAllQueues(queue,MaxTriQueues-1);
	}
	/*
	else
	{
		minI = MaxTriQueues-1, maxI = MaxTriQueues;
		// flush all other queues
		FlushAllQueues(queue,MaxTriQueues-1);
	}
	*/

	int index = queue.Allocate(tex,level,spec,minI,maxI,queue._actTri);
	if (index>=0)
	{
		Assert(queue._triUsed[index]);
		return index;
	}
	// we must free some queue
	// hard to tell which, select random
	//int random = GRandGen.RandomValue()&MaxTriQueues;
	// find LRU
	int minUsed = INT_MAX;
	for (int i=minI; i<maxI; i++)
	{
		int used = queue._tri[i]._lastUsed;
		if (used<minUsed)
		{
			minUsed = used;
			index = i;
		}
	}
	if (index<0)
	{	
		Fail("LRU failed");
		index=0;
	}
	FlushAndFreeQueue(queue,index);

	index = queue.Allocate(tex,level,spec,minI,maxI,index);

	Assert (index>=0); // some queues is free now (after FlushAndFreeQueue)

	Assert(queue._triUsed[index]);

	return index;
}

void EngineDD::FreeQueue(Queue &queue, int index)
{
	// queue is already flushed
	Assert(!queue._tri[index]._triangleQueue.Size());
	queue.Free(index);
}

void EngineDD::FlushAllQueues(Queue &queue, int skip)
{
	#if DO_TEX_STATS>=2
	if (LogStatesOnce)
	{
		LogF("FlushAllQueues");
	}
	#endif
	for (int i=0; i<MaxTriQueues; i++) if (i!=skip)
	{
		if (queue._triUsed[i])
		{
			FlushQueue(queue,i);
		}
		Assert(queue._tri[i]._triangleQueue.Size()==0);
	}
}

void EngineDD::FreeAllQueues(Queue &queue)
{
	#if DO_TEX_STATS>=2
	if (LogStatesOnce)
	{
		LogF("FreeAllQueues");
	}
	#endif
	for (int i=0; i<MaxTriQueues; i++)
	{
		if (queue._triUsed[i])
		{
			// simulate flushing - ignore rest of the queue
			queue._tri[i]._triangleQueue.Clear();
			FreeQueue(queue,i);
		}
		else
		{
			Assert(queue._tri[i]._triangleQueue.Size()==0);
		}
		Assert(!queue._triUsed[i]);
		Assert(queue._tri[i]._triangleQueue.Size()==0);
	}
}

void EngineDD::FlushAndFreeAllQueues(Queue &queue, bool nonEmptyOnly)
{
	#if DO_TEX_STATS>=2
	if (LogStatesOnce)
	{
		LogF("FlushAndFreeAllQueues");
	}
	#endif
	for (int i=0; i<MaxTriQueues; i++)
	{
		if
		(
			queue._triUsed[i] &&
			(!nonEmptyOnly || queue._tri[i]._triangleQueue.Size()>0)
		)
		{
			FlushAndFreeQueue(queue,i);
		}
		else
		{
			Assert(queue._tri[i]._triangleQueue.Size()==0);
		}
		Assert(nonEmptyOnly || !queue._triUsed[i]);
		Assert(queue._tri[i]._triangleQueue.Size()==0);
	}
	//queue._actTri = -1; // some queue may be allocated
}

void EngineDD::CloseAllQueues(Queue &queue)
{
	FlushAndFreeAllQueues(queue);
	queue._usedCounter = 0; // reset counter to avoid overflow
	queue._firstVertex = true; // reset counter to avoid overflow
}

void EngineDD::FlushQueue(Queue &queue, int index)
{
	//SwitchTL(false);
	TriQueue &triq = queue._tri[index];
	int n = triq._triangleQueue.Size();
	if( n>0 )
	{
		if (index==MaxTriQueues-1)
		{
			FlushAllQueues(queue,index); // avoid recursion
		}
		#if _ENABLE_PERFLOG && DO_TEX_STATS
		if (LogStatesOnce)
		{
			LogF
			(
				"Flush queue %d:%s %x",
				index,triq._texture ? triq._texture->Name() : "<NULL>",triq._special
			);
		}
		#endif
		DoPrepareTriangle(triq._texture,triq._level,triq._special);
		//if( triq._queueType!=QueuePoints )
		{
			#if DIAG_QUEUE
			LogF("Flushing tri queue: base %d, size %d",_queue._queueBase,_queue._queueSize);
			#endif
			// set index stream / vertex buffer
			// queue._meshBuffer
			// triq._triangleQueue.Data(),triq._triangleQueue.Size(),

			HRESULT ret;

			// create index buffer
			int size = n*sizeof(VertexIndex);


			int indexOffset = 0;
			BYTE *data;
			if (n+queue._indexBufferUsed<=IndexBufferLength && !queue._firstIndex)
			{
				#if LOG_LOCKS
				LogF("Index add D3DLOCK_NOOVERWRITE %d + %d",queue._indexBufferUsed,n);
				#endif
				// append data
				indexOffset = queue._indexBufferUsed;
				ret = queue._indexBuffer->Lock
				(
					indexOffset*sizeof(VertexIndex),size,&data,
					NoSysLockFlag|D3DLOCK_NOOVERWRITE
				);
			}
			else
			{
				#if LOG_LOCKS
				LogF("Index add D3DLOCK_DISCARD %d + %d",0,n);
				#endif
				queue._firstIndex = false;
				// reset index buffer
				indexOffset = 0;
				ret = queue._indexBuffer->Lock
				(
					indexOffset*sizeof(VertexIndex),size,&data,
					NoSysLockFlag|DiscardLockFlag
				);
			}
			queue._indexBufferUsed = indexOffset+n;
			if (ret==D3D_OK)
			{
				memcpy(data,triq._triangleQueue.Data(),size);
				queue._indexBuffer->Unlock();
			}
			else
			{
				DDError("iBuffer->Lock",ret);
			}
			// fill index buffer
			if (_lastQueueSource!=&queue)
			{
				_lastQueueSource=&queue;
				_iBufferLast.Free();
				_vBufferLast.Free();
				_vOffsetLast = 0;
				ADD_COUNTER(d3xVB,1);
				ADD_COUNTER(d3xIB,1);
				_d3DDevice->SetIndices(queue._indexBuffer,0);
				_d3DDevice->SetStreamSource(0,queue._meshBuffer,sizeof(TLVertex));
				_vBufferSize = 0;
			}

			PROFILE_DX_SCOPE(3drIP);
			DoAssert(_d3dFrameOpen);
			ret =_d3DDevice->DrawIndexedPrimitive
			(
				D3DPT_TRIANGLELIST,
				0,
				//triq._queueSize,
				queue._vertexBufferUsed,
				indexOffset,n/3
			);
			DX_CHECKN("DIP",ret);
			#if _ENABLE_PERFLOG && DO_TEX_STATS
			if (LogStatesOnce)
			{
				LogF("  Draw .... %d tris",triq._triangleQueue.Size()/3);
			}
			#endif
			ADD_COUNTER(tris,n/3);
			ADD_COUNTER(dPrim,1);
			//triq._queueSize = 0;
		}
		/*
		else
		{
			// TODO: point drawing
			triq._queueSize = 0;
		}
		*/
		triq._triangleQueue.Clear();
	}
	//triq._queueBase = queue._meshBase;
	//triq._queueSize = 0;
	Assert(!triq._triangleQueue.Size());
}

void EngineDD::FlushAndFreeQueue(Queue &queue, int index)
{
	FlushQueue(queue,index);
	FreeQueue(queue,index);
}

void EngineDD::Queue2DPoly( const TLVertex *v0, int n )
{

	int addN = (n-2)*3;

	Assert( _queueNo._actTri>=0 );
	Assert (_queueNo._triUsed[_queueNo._actTri]);
	WORD *tgt = QueueAdd(_queueNo,addN);

	//TriQueue &triq = _queueNo._tri[_queueNo._actTri];
	int offset = _queueNo._meshBase;
	Assert( offset>=0 );
	//saturateMax(triq._queueSize,0+offset+1);
	//saturateMax(triq._queueSize,1+offset+1);

	// add all triangles
	for( int i=2; i<n; i++ )
	{
		*tgt++ = 0+offset;
		*tgt++ = i-1+offset;
		*tgt++ = i+offset;
		//saturateMax(triq._queueSize,i+offset+1);
	}
}

#if _RELEASE
	#define DO_COUNTERS 0
#else
	#define DO_COUNTERS 0
#endif

#if DO_COUNTERS
struct OptimizeCounter
{
	const char *name;
	int done,skipped;
	int counter;
	OptimizeCounter( const char *n):name(n),done(0),skipped(0),counter(0){}
	~OptimizeCounter(){ReportSingle();}

	void Skip( int i=1 ){skipped+=i,counter+=i;}
	void Perform( int i=1 ){done+=i,counter+=i;ReportEvery();}
	void ReportSingle()
	{
		LogF("%s: %d optimized to %d",name,skipped+done,done);
		skipped=0;
		done=0;
		counter=0;
	}
	void ReportEvery( int n=1000 )
	{
		if( counter>n ) ReportSingle();
	}
};
#endif

#if RS_DIAGS
HRESULT EngineDD::D3DSetTextureStageStateName
(
	DWORD stage, D3DTEXTURESTAGESTATETYPE state, DWORD value, const char *name, bool optimize
)
#else
HRESULT EngineDD::D3DSetTextureStageState
(
	DWORD stage, D3DTEXTURESTAGESTATETYPE state, DWORD value, bool optimize
)
#endif
{
	#if DO_COUNTERS
		static OptimizeCounter opt("TextureStageState");
	#endif
	// assume small values of state (in DX6 max. state was about 25)
	AutoArray<TextureStageStateInfo> &stageState=_textureStageState[stage];
	stageState.Access(state);
	TextureStageStateInfo &info=stageState[state];
	if( info.value==value && optimize )
	{
		#if DO_COUNTERS
			opt.Skip();
		#endif
		return D3D_OK;
	}
	info.value=value;
	#if DO_COUNTERS
		opt.Perform();
	#endif
	#if _ENABLE_PERFLOG && DO_TEX_STATS
	if (LogStatesOnce)
	{
		#if RS_DIAGS
		LogF("SetTextureStageState %d,%s,%d",stage,name,value);
		#else
		LogF("SetTextureStageState %d,%d,%d",stage,state,value);
		#endif
	}
	#endif
	#if !NO_SET_TSS
	PROFILE_DX_SCOPE(3sTSS);
	HRESULT ret = _d3DDevice->SetTextureStageState(stage,state,value);
	DX_CHECKN("SetTSS",ret);
	return ret;
	#else
	return 0;
	#endif
}

void EngineDD::PrepareSingleTexDiffuseA()
{
	EnableDetailTexGen(_tlActive ? TGNone : TGFixed,true);
	#if PIXEL_SHADERS
	if (_usePixelShaders && _pixelShaders>=0x100)
	{
		SelectPixelShader(PSNormal);
		return;
	}
	#endif
	// setup using fixed-function pipeline
	D3DSetTextureStageState(0,D3DTSS_ALPHAOP,D3DTOP_SELECTARG1);
	D3DSetTextureStageState(0,D3DTSS_COLOROP,D3DTOP_MODULATE);

	D3DSetTextureStageState(1,D3DTSS_COLOROP,D3DTOP_DISABLE);
	D3DSetTextureStageState(1,D3DTSS_ALPHAOP,D3DTOP_DISABLE);
}

void EngineDD::PrepareSingleTexModulateA()
{
	EnableDetailTexGen(_tlActive ? TGNone : TGFixed,true);
	#if PIXEL_SHADERS
	if (_usePixelShaders && _pixelShaders>=0x100)
	{
		SelectPixelShader(PSNormal);
		return;
	}
	#endif
	// setup using fixed-function pipeline
	D3DSetTextureStageState(0,D3DTSS_ALPHAOP,D3DTOP_MODULATE);
	D3DSetTextureStageState(0,D3DTSS_COLOROP,D3DTOP_MODULATE);

	D3DSetTextureStageState(1,D3DTSS_COLOROP,D3DTOP_DISABLE);
	D3DSetTextureStageState(1,D3DTSS_ALPHAOP,D3DTOP_DISABLE);
}

void EngineDD::DoEnableDetailTexGen(TexGenMode mode, bool optimize)
{
	static const D3DMATRIX matTrans32=
	{
		32,0,0, 0,
		0,32,0, 0,
		0,0,32, 0,
		0,0,0,  1,
	};
	static const D3DMATRIX matTrans64=
	{
		64,0,0, 0,
		0,64,0, 0,
		0,0,64, 0,
		0,0,0,  1,
	};
	static const D3DMATRIX matTrans1=
	{
		1,0,0, 0,
		0,1,0, 0,
		0,0,1, 0,
		0,0,0, 1,
	};
	#if 1
	if (mode==TGFixed)
	{
		#if VERTEX_SHADERS
		SelectVertexShader(VSNormal);
		#endif
		// disable tex-coordinate generator
		D3DSetTextureStageStateOpt(0, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_DISABLE, optimize );
		D3DSetTextureStageStateOpt(1, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_DISABLE, optimize );
		D3DSetTextureStageStateOpt(0, D3DTSS_TEXCOORDINDEX, 0,optimize);
		D3DSetTextureStageStateOpt(1, D3DTSS_TEXCOORDINDEX, 1,optimize);
	}
	else if (mode==TGNone)
	{
		#if VERTEX_SHADERS
		SelectVertexShader(VSNormal);
		#endif
		// disable tex-coordinate generator
		D3DSetTextureStageStateOpt(0, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_DISABLE, optimize );
	  D3DSetTextureStageStateOpt(0, D3DTSS_TEXCOORDINDEX, 0,optimize);
		D3DSetTextureStageStateOpt(1, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_DISABLE, optimize );
	  D3DSetTextureStageStateOpt(1, D3DTSS_TEXCOORDINDEX, 0,optimize);
	}
	else if (mode==TGDetail)
	{
		// enable tex-coordinate generator
		#if VERTEX_SHADERS
		SelectVertexShader(VSNormal);
		#endif
		_d3DDevice->SetTransform(D3DTS_TEXTURE1,&matTrans32);
		D3DSetTextureStageStateOpt(0, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_DISABLE, optimize );
	  D3DSetTextureStageStateOpt(0, D3DTSS_TEXCOORDINDEX, 0,optimize);
		D3DSetTextureStageStateOpt(1, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_COUNT2, optimize );
		D3DSetTextureStageStateOpt(1, D3DTSS_TEXCOORDINDEX, 0,optimize);
	}
	else if (mode==TGGrass)
	{
		// enable tex-coordinate generator
		#if VERTEX_SHADERS
		SelectVertexShader(VSDayGrass);
		#endif
		_d3DDevice->SetTransform(D3DTS_TEXTURE1,&matTrans32);
		D3DSetTextureStageStateOpt(0, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_DISABLE, optimize );
		D3DSetTextureStageStateOpt(1, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_COUNT2, optimize );
		#if VERTEX_SHADERS
		if (_vertexShaders>=0x100)
		{
		  D3DSetTextureStageStateOpt(0, D3DTSS_TEXCOORDINDEX, 0,optimize);
			D3DSetTextureStageStateOpt(1, D3DTSS_TEXCOORDINDEX, 1,optimize);
		}
		else
		#endif
		{
		  D3DSetTextureStageStateOpt(0, D3DTSS_TEXCOORDINDEX, 0,optimize);
			D3DSetTextureStageStateOpt(1, D3DTSS_TEXCOORDINDEX, 0,optimize);
		}
	}
	else if (mode==TGWater)
	{
		#if VERTEX_SHADERS
		SelectVertexShader(VSNormal);
		#endif
		D3DMATRIX zoomAndMove = matTrans64;
		D3DMATRIX move = matTrans1;


		// x,z usually from 0 to LandRange
		float mw1 = sin(Glob.time.toFloat()*0.04);
		float mw2 = fastFmod(Glob.time.toFloat()*0.3+sin(Glob.time.toFloat()*0.5)*0.5,2);

		move._31 = mw1*0.5;
		move._32 = mw1;

		zoomAndMove._31 = mw2*0.5;
		zoomAndMove._32 = mw2;

		_d3DDevice->SetTransform(D3DTS_TEXTURE0,&move);
		D3DSetTextureStageStateOpt(0, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_COUNT2, optimize );
	  D3DSetTextureStageStateOpt(0, D3DTSS_TEXCOORDINDEX, 0,optimize);

		_d3DDevice->SetTransform(D3DTS_TEXTURE1,&zoomAndMove);
		D3DSetTextureStageStateOpt(1, D3DTSS_TEXCOORDINDEX, 0,optimize);
		D3DSetTextureStageStateOpt(1, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_COUNT2, optimize );
	}
	#endif

	_texGenMode=mode;

}
void EngineDD::PrepareDetailTex(bool water, bool grass)
{
	if (!IsMultitexturing())
	{
		PrepareSingleTexDiffuseA();
		return;
	}

	if (_tlActive)
	{
		EnableDetailTexGen(water ? TGWater : (grass ? TGGrass : TGDetail),true);
	}
	else
	{
		EnableDetailTexGen(TGFixed,true);
	}

	#if PIXEL_SHADERS
	if (_usePixelShaders && _pixelShaders>=0x100)
	{
		if (water) SelectPixelShader(PSWater);
		else if (grass) SelectPixelShader(PSGrass);
		else SelectPixelShader(PSDetail);
		return;
	}
	#endif
	// modulate with diffuse and modulate singed 2X
	D3DSetTextureStageState( 0, D3DTSS_COLOROP, D3DTOP_MODULATE);
	D3DSetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	D3DSetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);
	D3DSetTextureStageState( 0, D3DTSS_ALPHAOP,   D3DTOP_MODULATE);
	D3DSetTextureStageState( 0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	D3DSetTextureStageState( 0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);

	D3DSetTextureStageState( 1, D3DTSS_COLOROP, D3DTOP_MODULATE2X);
	D3DSetTextureStageState( 1, D3DTSS_COLORARG1, D3DTA_CURRENT);
	D3DSetTextureStageState( 1, D3DTSS_COLORARG2, D3DTA_TEXTURE | D3DTA_ALPHAREPLICATE);
	D3DSetTextureStageState( 1, D3DTSS_ALPHAOP,   D3DTOP_DISABLE);

	D3DSetTextureStageState(2,D3DTSS_COLOROP,D3DTOP_DISABLE);
	D3DSetTextureStageState(2,D3DTSS_ALPHAOP,D3DTOP_DISABLE);

}

void EngineDD::DoStencilExclusion( bool enable, bool optimize )
{
	if (!_hasStencilBuffer) return;
	_stencilExclusionEnabled=enable;
	if (enable)
	{
		// stencil buffer shadows
		// increment stencil
		// (we could use any state that would make the value non-zero)
		D3DSetRenderStateOpt(D3DRS_STENCILPASS,D3DSTENCILOP_INCRSAT,optimize);
		// write only if stencil is zero
		D3DSetRenderStateOpt(D3DRS_STENCILFUNC,D3DCMP_EQUAL,optimize);
	}
	else
	{
		// always set to zero
		D3DSetRenderStateOpt(D3DRS_STENCILFUNC,D3DCMP_ALWAYS,optimize);
		D3DSetRenderStateOpt(D3DRS_STENCILPASS,D3DSTENCILOP_REPLACE,optimize);
	}
}

void EngineDD::SetMultiTexturing( VFormatSet format )
{
	if( _formatSet==format ) return;

	_formatSet=format;
	switch (format)
	{
		case SingleTex:
			// disable second stage
			//D3DSetTextureStageState(0,D3DTSS_ALPHAOP,D3DTOP_SELECTARG1);
		break;
		case DetailTex:
		{
			TextureD3D *detail = _textBank->GetDetailTexture();
			_textBank->UseMipmap(detail,0,0);

			#if !NO_SET_TEX
			TextureD3DHandle tHandle=detail ? detail->GetHandle() : NULL;
			_d3DDevice->SetTexture( 1,tHandle );
			#endif
			#if LOG_STATES
			LogF("Flush: sec texture change to %s",detail->Name());
			#endif
		}
		break;
		case GrassTex:
		{
			TextureD3D *detail = _textBank->GetGrassTexture();
			_textBank->UseMipmap(detail,0,0);

			#if !NO_SET_TEX
			TextureD3DHandle tHandle=detail ? detail->GetHandle() : NULL;
			_d3DDevice->SetTexture( 1,tHandle );
			#endif
			#if LOG_STATES
			LogF("Flush: sec texture change to %s",detail->Name());
			#endif
		}
		break;
		case SpecularTex:
		{
			TextureD3D *detail = _textBank->GetSpecularTexture();
			/*
			if (_usePixelShaders)
			{
				detail = _textBank->GetWaterBumpMap();
			}
			*/
			_textBank->UseMipmap(detail,0,0);

			#if !NO_SET_TEX
			TextureD3DHandle tHandle=detail ? detail->GetHandle() : NULL;
			_d3DDevice->SetTexture( 1,tHandle );
			#endif
			#if LOG_STATES
			LogF("Flush: sec texture change to %s",detail->Name());
			#endif
		}
		break;
	}
}

void EngineDD::EnablePointSampling(bool enable, bool optimize)
{
	if (!enable)
	{
		D3DSetTextureStageStateOpt(0,D3DTSS_MINFILTER,D3DTEXF_LINEAR,optimize);
		D3DSetTextureStageStateOpt(0,D3DTSS_MAGFILTER,D3DTEXF_LINEAR,optimize);	
	}
	else
	{
		D3DSetTextureStageStateOpt(0,D3DTSS_MINFILTER,D3DTEXF_POINT,optimize);
		D3DSetTextureStageStateOpt(0,D3DTSS_MAGFILTER,D3DTEXF_POINT,optimize);	
	}
}

void EngineDD::SetTexture( const TextureD3D *tex, int specFlags )
{
	TextureD3DHandle tHandle=tex ? tex->GetHandle() : NULL;
	if( _lastHandle!=tHandle )
	{
		#if DO_TEX_STATS
		if (EnableTexStats) TexStats.Count(tex ? tex->Name() : "<NULL>");
		if (LogStatesOnce)
		{
			LogF("Switch %s",tex ? tex->Name() : "<NULL>");
		}
		#endif
		#if LOG_STATES
		LogF("Flush: texture change to %s",tex ? tex->Name() : "<NULL>");
		#endif
		//LogTexture("Tri",tex);
		#if !NO_SET_TEX
		HRESULT err=_d3DDevice->SetTexture(0,tHandle);
		ADD_COUNTER(tChng,1);
		if( err ) DDError("Cannot set texture",err);
		#endif
		_lastHandle = tHandle;
	}

	VFormatSet format=SingleTex;
	if( tex )
	{
		// set clamping flags
		// only some spec modes are relevant for triangle drawing
		if( specFlags&(DetailTexture|SpecularTexture|GrassTexture) )
		{
			if( specFlags&GrassTexture) format=GrassTex;
			else if( specFlags&DetailTexture) format=DetailTex;
			else format=SpecularTex;
		}
	}
	SetMultiTexturing(format);
}

#if RS_DIAGS
HRESULT EngineDD::D3DSetRenderStateName
(
	D3DRENDERSTATETYPE state, DWORD value, const char *name, bool optimize
)
#else
HRESULT EngineDD::D3DSetRenderState
(
	D3DRENDERSTATETYPE state, DWORD value, bool optimize
)
#endif
{
	#if DO_COUNTERS
		static OptimizeCounter opt("RenderState");
	#endif
	// assume small values of state (in DX6 max. state was about 40)
	_renderState.Access(state);
	RenderStateInfo &info=_renderState[state];

	if( info.value==value && optimize )
	{
		#if DO_COUNTERS
			opt.Skip();
		#endif
		return D3D_OK;
	}

	info.value=value;
	#if DO_COUNTERS
		opt.Perform();
	#endif
	#if DO_TEX_STATS
	if (LogStatesOnce)
	{
		#if RS_DIAGS
		LogF("SetRenderState %s,%d",name,value);
		#else
		LogF("SetRenderState %d,%d",state,value);
		#endif
	}
	#endif
	#if !NO_SET_RS
	PROFILE_DX_SCOPE(3sRS);
	HRESULT err=_d3DDevice->SetRenderState(state,value);
	//HRESULT err=D3D_OK;
	DX_CHECKN("SetRS",err);
	return err;
	#else
	return 0;
	#endif
}

void EngineDD::DoPrepareTriangle( TextureD3D *tex, int level, int spec )
{
	bool clampU=false,clampV=false;

	//Assert( (spec&(NoClamp|ClampU|ClampV))!=0 );
	// all triangles are marked for clamping
	if( spec&ClampU ) clampU=true;
	if( spec&ClampV ) clampV=true;
	if( _lastClampU!=clampU )
	{
		D3DTEXTUREADDRESS address=( clampU ? D3DTADDRESS_CLAMP : D3DTADDRESS_WRAP );
		D3DSetTextureStageState(0,D3DTSS_ADDRESSU,address);
		_lastClampU=clampU;
	}
	if( _lastClampV!=clampV )
	{
		D3DTEXTUREADDRESS address=( clampV ? D3DTADDRESS_CLAMP : D3DTADDRESS_WRAP );
		D3DSetTextureStageState(0,D3DTSS_ADDRESSV,address);
		_lastClampV=clampV;
	}
	
	int specFlags=spec&
	(
		NoZBuf|NoZWrite|
		IsAlphaFog|IsShadow|IsAlpha|IsTransparent|
		IsWater|IsLight|IsOnSurface|OnSurface|
		DetailTexture|SpecularTexture|GrassTexture|PointSampling
	);

	if( _lastSpec!=specFlags )
	{
		EnablePointSampling((specFlags&PointSampling)!=0,true);
		if( specFlags&IsShadow )
		{
			// shadow
			D3DSetRenderState(D3DRS_ZFUNC,D3DCMP_LESSEQUAL);
			if (!_hasStencilBuffer)
			{
				D3DSetRenderState(D3DRS_ZWRITEENABLE,TRUE);
			}
			else
			{
				D3DSetRenderState(D3DRS_ZWRITEENABLE,FALSE);
			}
		}
		else if( specFlags&NoZBuf )
		{
			D3DSetRenderState(D3DRS_ZFUNC,D3DCMP_ALWAYS);
			D3DSetRenderState(D3DRS_ZWRITEENABLE,FALSE);
			//D3DSetRenderState(D3DRS_ZENABLE,FALSE);
		}
		else if( specFlags&NoZWrite )
		{
			// road
			//D3DSetRenderState(D3DRS_ZFUNC,D3DCMP_GREATEREQUAL);
			D3DSetRenderState(D3DRS_ZFUNC,D3DCMP_LESSEQUAL);
			D3DSetRenderState(D3DRS_ZWRITEENABLE,FALSE);
			//D3DSetRenderState(D3DRS_ZENABLE,TRUE);
		}
		else
		{
			D3DSetRenderState(D3DRS_ZFUNC,D3DCMP_LESSEQUAL);
			//D3DSetRenderState(D3DRS_ZFUNC,D3DCMP_GREATEREQUAL);
			D3DSetRenderState(D3DRS_ZWRITEENABLE,TRUE);
			//D3DSetRenderState(D3DRS_ZENABLE,TRUE);
		}

		if( specFlags&IsShadow )
		{
			D3DSetRenderState(D3DRS_SRCBLEND,D3DBLEND_ZERO);
			D3DSetRenderState(D3DRS_DESTBLEND,D3DBLEND_INVSRCALPHA);
			D3DSetRenderState(D3DRS_FOGENABLE,FALSE);
			D3DSetRenderState(D3DRS_ALPHAREF,(_shadowFactor*7)>>4);
			D3DSetRenderState(D3DRS_ALPHATESTENABLE,TRUE);
			D3DSetRenderState(D3DRS_ALPHABLENDENABLE,TRUE);
			StencilExclusion(true);
			PrepareSingleTexModulateA();
		}
		else if( specFlags&IsLight )
		{
			D3DSetRenderState(D3DRS_FOGENABLE,FALSE);
			D3DSetRenderState(D3DRS_ALPHAREF,1);
			D3DSetRenderState(D3DRS_ALPHATESTENABLE,TRUE);
			D3DSetRenderState(D3DRS_SRCBLEND,D3DBLEND_SRCALPHA);
			D3DSetRenderState(D3DRS_DESTBLEND,D3DBLEND_ONE);
			D3DSetRenderState(D3DRS_ALPHABLENDENABLE,TRUE);
			StencilExclusion(false);
			PrepareSingleTexModulateA();
		}
		else if( specFlags&IsWater )
		{
			//D3DSetRenderState(D3DRS_SRCBLEND,D3DBLEND_SRCALPHA);
			//D3DSetRenderState(D3DRS_DESTBLEND,D3DBLEND_INVSRCALPHA);
			StencilExclusion(false);
			#if !NO_FOG			
			D3DSetRenderState(D3DRS_FOGENABLE,TRUE);
			#endif
			D3DSetRenderState(D3DRS_ALPHABLENDENABLE,FALSE);
			D3DSetRenderState(D3DRS_ALPHATESTENABLE,FALSE);
			PrepareDetailTex(true,false);
		}
		else if( specFlags&IsAlphaFog )
		{
			// clouds or other transparent and distant objects
			// use fog component instead of alpha
			D3DSetRenderState(D3DRS_FOGENABLE,FALSE);
			D3DSetRenderState(D3DRS_ALPHAREF,1);
			D3DSetRenderState(D3DRS_ALPHATESTENABLE,TRUE);
			D3DSetRenderState(D3DRS_SRCBLEND,D3DBLEND_SRCALPHA);
			D3DSetRenderState(D3DRS_DESTBLEND,D3DBLEND_INVSRCALPHA);
			D3DSetRenderState(D3DRS_ALPHABLENDENABLE,TRUE);
			StencilExclusion(false);
			PrepareSingleTexModulateA();
		}
		else
		{
			if (specFlags&(DetailTexture|SpecularTexture|GrassTexture))
			{
				PrepareDetailTex(false,specFlags&GrassTexture);
			}
			else
			{
				PrepareSingleTexDiffuseA();
			}

			#if !NO_FOG			
			D3DSetRenderState(D3DRS_FOGENABLE,TRUE);
			#endif
			if( specFlags&IsAlpha )
			{
				D3DSetRenderState(D3DRS_ALPHAREF,1);
				D3DSetRenderState(D3DRS_ALPHATESTENABLE,TRUE);
				D3DSetRenderState(D3DRS_SRCBLEND,D3DBLEND_SRCALPHA);
				D3DSetRenderState(D3DRS_DESTBLEND,D3DBLEND_INVSRCALPHA);
				D3DSetRenderState(D3DRS_ALPHABLENDENABLE,TRUE);
			}
			else
			{
				// check if alpha testing is required
				D3DSetRenderState(D3DRS_ALPHAREF,0xc0);
				if (specFlags&IsTransparent)
				{
					D3DSetRenderState(D3DRS_ALPHATESTENABLE,TRUE);
				}
				else
				{
					D3DSetRenderState(D3DRS_ALPHATESTENABLE,FALSE);
				}

				D3DSetRenderState(D3DRS_ALPHABLENDENABLE,FALSE);
			}

			StencilExclusion(false);
		}
		_lastSpec=specFlags;
	}	
	
	#if 1
	TextureD3DHandle tHandle=tex ? tex->GetHandle() : NULL;
	if( _lastHandle!=tHandle )
	{
		LogTexture("Tri",tex);
		SetTexture(tex,spec);
		//HRESULT err=D3D_OK;
		//if( err ) DDError("Cannot set texture",err);
	}
	#endif
}

void EngineDD::DoPrepareTriangle( const MipInfo &absMip, int specFlags )
{
	TextureD3D *tex = static_cast<TextureD3D *>(absMip._texture);
	int level = absMip._level;
	// allocate some queue
	DoPrepareTriangle(tex,level,specFlags);
}

void EngineDD::QueuePrepareTriangle( const MipInfo &absMip, int specFlags )
{
	TextureD3D *tex = static_cast<TextureD3D *>(absMip._texture);
	int level = absMip._level;
	_queueNo._actTri = AllocateQueue(_queueNo,tex,level,specFlags);
	Assert (_queueNo._triUsed[_queueNo._actTri]);
}

void EngineDD::PrepareTriangle
(
	const MipInfo &absMip, int specFlags0
)
{
	TextureD3D *tex = static_cast<TextureD3D *>(absMip._texture);
	SwitchRenderMode(RMTris);
	// allocate some queue
	SwitchTL(false);
	int level = absMip._level;
	_queueNo._actTri = AllocateQueue(_queueNo,tex,level,specFlags0);
	Assert (_queueNo._triUsed[_queueNo._actTri]);
	_prepSpec = specFlags0;

}

void EngineDD::PrepareTriangleTL(const MipInfo &mip, int specFlags)
{
	Assert(_tlActive);
	TextureD3D *tex = static_cast<TextureD3D *>(mip._texture);
	int level = mip._level;
	DoPrepareTriangle(tex,level,specFlags);
}

/*!
\patch 1.33 Date 11/28/2001 by Ondra
- Fixed: D3D rendering queue bug that could cause polygons being dropped.
*/

void EngineDD::DrawDecal
(
	Vector3Par screen, float rhw, float sizeX, float sizeY, PackedColor color,
	const MipInfo &mip, int specFlags
)
{

	float vx=screen.X();
	float vy=screen.Y();
	float z=screen.Z();

	float oow=rhw;

	// perform simple clipping
	float xBeg=vx-sizeX;
	float xEnd=vx+sizeX;
	float yBeg=vy-sizeY;
	float yEnd=vy+sizeY;
	float uBeg=0;
	float vBeg=0;
	float uEnd=1;
	float vEnd=1;

	if( xBeg<0 )
	{
		// -xBeg is out, side length is 2*sizeX
		uBeg=-xBeg/(2*sizeX);
		xBeg=0;
	}
	if( xEnd>_w )
	{
		// xEnd-_w is out, side length is 2*sizeX
		uEnd=1-(xEnd-_w)/(2*sizeX);
		xEnd=_w;
	}
	if( yBeg<0 )
	{
		// -yBeg is out, side length is 2*sizeY
		vBeg=-yBeg/(2*sizeY);
		yBeg=0;
	}
	if( yEnd>_h )
	{
		// yEnd-_h is out, side length is 2*sizeY
		vEnd=1-(yEnd-_h)/(2*sizeY);
		yEnd=_h;
	}
		
	if( xBeg>=xEnd || yBeg>=yEnd ) return;
		
	TLVertex v[4];

	// set vertex 0
	v[0].pos[0]=xBeg;
	v[0].pos[1]=yBeg;
	v[0].pos[2]=z;
	v[0].t0.u=uBeg;
	v[0].t0.v=vBeg;
		// set vertex 1
	v[1].pos[0]=xEnd;
	v[1].pos[1]=yBeg;
	v[1].pos[2]=z;
	v[1].t0.u=uEnd;
	v[1].t0.v=vBeg;
	// set vertex 2
	v[2].pos[0]=xEnd;
	v[2].pos[1]=yEnd;
	v[2].pos[2]=z;
	v[2].t0.u=uEnd;
	v[2].t0.v=vEnd;
	// set vertex 3
	v[3].pos[0]=xBeg;
	v[3].pos[1]=yEnd;
	v[3].pos[2]=z;
	v[3].t0.u=uBeg;
	v[3].t0.v=vEnd;

	// add vertices to vertex buffer

	// check active queue

	if( specFlags&IsAlphaFog )
	{
		v[0].color=color;
		v[0].specular=PackedColor(0xff000000);
	}
	else
	{
		// use fog with z
		v[0].specular=PackedColor(0xff000000-(color&0xff000000));
		v[0].color=PackedColor(color|0xff000000);
	}

	int i;
	for( i=0; i<4; i++ )
	{
		v[i].rhw=oow;
		v[i].pos[2]=z;
		v[i].color=v[0].color;
		v[i].specular=v[0].specular;
	}

	SwitchRenderMode(RMTris);
	// allocate some queue
	SwitchTL(false);

	AddVertices(v,4);

	QueuePrepareTriangle(mip,specFlags);
	static const VertexIndex indices[4] = {0,1,2,3};
	QueueFan(indices,4);
}

void EngineDD::DrawPolygon( const VertexIndex *ii, int n )
{
	QueueFan(ii,n);
}

void EngineDD::DrawSection
(
	const FaceArray &face, Offset beg, Offset end
)
{
	if (_resetNeeded) return;
	for( Offset i=beg; i<end; face.Next(i) )
	{
		const Poly &f=face[i];
		QueueFan(f.GetVertexList(),f.N());
	}
}

void EngineDD::DrawSectionTL
(
	const Shape &sMesh, int beg,int end
)
{
	if (_resetNeeded) return;
	// no queueing, direct drawing
	VertexBufferD3D *buf = static_cast<VertexBufferD3D *>(sMesh.GetVertexBuffer());

	const VBSectionInfo &siBeg = buf->_sections[beg];
	const VBSectionInfo &siEnd = buf->_sections[end-1];
	// scan sections to provide reasonable values for vertex range
	int begVertex = siBeg.begVertex, endVertex = siBeg.endVertex;
	for (int i=beg+1; i<end; i++)
	{
		const VBSectionInfo &si = buf->_sections[i];
		saturateMin(begVertex,si.begVertex);
		saturateMax(endVertex,si.endVertex);
	}
	Assert (end>beg);
	Assert (endVertex>begVertex);

	// all attributes prepared

	#if DO_TEX_STATS
	if (LogStatesOnce)
	{
		if (LogStatesOnce)
		{
			LogF
			(
				"  Draw TL .... %d tris (%d..%d,%s)",
				(siEnd.end-siBeg.beg)/3,
				beg,end,
				(const char *)sMesh.GetSection(beg).properties.GetDebugText()
			);
		}
	}
	#endif

	//const ShapeSection &sec = sMesh.GetSection(section);
	// actualy draw all data
	// note: min index, max index is not used by HW T&L
	//LogF("TLStart");
	// get vbuffer size
	PROFILE_DX_SCOPE(3drTL);
	#if !_RELEASE
	if (endVertex+_vOffsetLast>_vBufferSize)
	{
		LogF("vrange %d..%d",begVertex,endVertex);
		LogF("offset %d",_vOffsetLast);
		LogF("vb size %d",_vBufferSize);
		Fail("Vertex out of range");
		return;
	}
	#endif
	DoAssert(_d3dFrameOpen);
	HRESULT ret =_d3DDevice->DrawIndexedPrimitive
	(
		D3DPT_TRIANGLELIST,
		begVertex,endVertex-begVertex,
		siBeg.beg+_iOffset,(siEnd.end-siBeg.beg)/3
	);
	//LogF("TLEnd");
	#if _ENABLE_REPORT
	if (ret!=D3D_OK)
	{
		// log vertex indices from index buffer
		LogF("  Vertex offset %d",_vOffsetLast);
		LogF("  Vertex buffer supposed size %d",_vBufferSize);
		LogF
		(
			"  Vertex range %d..%d (count %d)",
			begVertex,endVertex,endVertex-begVertex
		);
		#ifndef _XBOX
		D3DVERTEXBUFFER_DESC desc;
		_vBufferLast->GetDesc(&desc);
		LogF("  Vertex buffer actual size %d",desc.Size/sizeof(SVertex));
		#endif
		int startIB = siBeg.beg+_iOffset;
		int lenIB = siEnd.end-siBeg.beg;
		LogF("  Index buffer range %d (%d)",startIB,lenIB);
		#if !WRITEONLYIB
			BYTE *data;
			_iBufferLast->Lock
			(
				startIB*sizeof(VertexIndex),lenIB*sizeof(VertexIndex),
				&data,D3DLOCK_READONLY
			);
			if (data)
			{
				// read index data
				VertexIndex *iData = (VertexIndex *)data;
				for (int i=0; i<lenIB; i++)
				{
					#if V_OFFSET_CUSTOM
						int ido = iData[i]+_vOffsetLast;
					#else
						int ido = iData[i];
					#endif
					if (ido>=endVertex)
					{
						LogF("  Overflow %4d : %5d",i,iData[i]);
					}
				}
				_iBufferLast->Unlock();
			}
		#endif
		#ifndef _XBOX
			DWORD sh;
			_d3DDevice->GetVertexShader(&sh);
			LogF("  VBuffer FVF %x, current shader %x",desc.FVF,sh);
		#endif
		Fail("DIP failed.");
	}
	#endif
	DX_CHECKN("DIP T&L",ret);
	ADD_COUNTER(dPrim,1);
	ADD_COUNTER(tris,(siEnd.end-siBeg.beg)/3);
}


inline int FracAlpha( float a )
{
	int ia=toInt(a);
	saturate(ia,0,255);
	return ia;
}

void EngineDD::DrawPoints(const TLVertex *vs, int nVertex)
{
	if (_resetNeeded) return;
	// now we are drawing quads directly
	for( int i=0; i<nVertex; i++ )
	{
		const TLVertex &v = vs[i];
		PackedColor color=v.color;
		if( color.A8()<8 ) continue; // do not draw stars that are not visible

		// calculate alpha in TL, TR, BL, BR corners
		// v is used as TL corner
		// we have to simulate TR, BL, BR corners
		int xI = toIntFloor(v.pos[0]);
		int yI = toIntFloor(v.pos[1]);
		float xFrac = v.pos[0]-xI;
		float yFrac = v.pos[1]-yI;
		float ixFrac = 1-xFrac;
		float iyFrac = 1-yFrac;

    // quad clipping may be required
    if (xI<0 || xI+2>_w || yI<0 || yI+2>_h)
    {
      // we need all four corners to be on screen
      // otherwise we need to apply clipping
      // we discard whole point instead
      continue;
    }

		float a=color.A8();

    float aTL = FracAlpha(ixFrac*iyFrac*a);
    float aTR = FracAlpha(xFrac*iyFrac*a);
    float aBR = FracAlpha(xFrac*yFrac*a);
    float aBL = FracAlpha(ixFrac*yFrac*a);
		TLVertex vs[4];
		vs[0] = v; // TL
		vs[0].pos[0] = xI+0.5f;
		vs[0].pos[1] = yI+0.5f;
		vs[0].color = PackedColorRGB(color,aTL);
		vs[0].specular = PackedColor(0);

		vs[1] = vs[0]; // TR
		vs[1].pos[0] = xI+2.5f;
		vs[1].color = PackedColorRGB(color,aTR);
		// 
		vs[2] = vs[1]; // BR
		vs[2].pos[1] = yI+2.5f;
		vs[2].color = PackedColorRGB(color,aBR);

		vs[3] = vs[2]; // BL
		vs[3].pos[0] = vs[0].pos[0];
		vs[3].color = PackedColorRGB(color,aBL);

		static const VertexIndex indices[6]={0,1,2,3};

		AddVertices(vs,4);
		QueueFan(indices,4);
	}
		
}

void EngineDD::DrawPoints( int beg, int end )
{
	// now we are drawing quads directly
  #if 0
	const TLVertex &vBeg = _mesh->GetVertex(beg);
  DrawPoints(&vBeg,end-beg);
  #else

	if (_resetNeeded) return;

	for( int i=beg; i<end; i++ )
	{
		if (_mesh->Clip(i)&ClipAll) continue;

		const TLVertex &v = _mesh->GetVertex(i);
		PackedColor color=v.color;
		if( color.A8()<8 ) continue; // do not draw stars that are not visible

		// calculate alpha in TL, TR, BL, BR corners
		// v is used as TL corner
		// we have to simulate TR, BL, BR corners
		int xI = toIntFloor(v.pos[0]);
		int yI = toIntFloor(v.pos[1]);
		float xFrac = v.pos[0]-xI;
		float yFrac = v.pos[1]-yI;
		float ixFrac = 1-xFrac;
		float iyFrac = 1-yFrac;

		float a=color.A8();

		TLVertex vs[4];
		vs[0] = v; // TL
		vs[0].pos[0] = xI+0.5f;
		vs[0].pos[1] = yI+0.5f;
		vs[0].color = PackedColorRGB(color,FracAlpha(ixFrac*iyFrac*a));
		vs[0].specular = PackedColor(0);

		vs[1] = vs[0]; // TR
		vs[1].pos[0] = xI+2.5f;
		vs[1].color = PackedColorRGB(color,FracAlpha(xFrac*iyFrac*a));
		// 
		vs[2] = vs[1]; // BR
		vs[2].pos[1] = yI+2.5f;
		vs[2].color = PackedColorRGB(color,FracAlpha(xFrac*yFrac*a));

		vs[3] = vs[2]; // BL
		vs[3].pos[0] = vs[0].pos[0];
		vs[3].color = PackedColorRGB(color,FracAlpha(ixFrac*yFrac*a));

		static const VertexIndex indices[6]={0,1,2,3};

		AddVertices(vs,4);
		QueueFan(indices,4);
	}
  #endif		
}

void EngineDD::SetD3DFog( ColorVal fog )
{
	if( _d3DDevice )
	{
		D3DCOLOR fogColor=FAST_D3DRGBA(fog.R(),fog.G(),fog.B(),1);
		D3DSetRenderState(D3DRS_FOGCOLOR,fogColor);
	}
}

bool EngineDD::CanGrass() const
{
	return false; //_useDXTL && _usePixelShaders && _textBank->GetGrassTexture();
}


#if PIXEL_SHADERS
static const
#include "shaders/psNormal.h"
static const
#include "shaders/psSpecular.h"
static const
#include "shaders/psDetail.h"
static const
#include "shaders/psDetailSpecular.h"
static const
#include "shaders/psGrass.h"
static const
#include "shaders/night.h"
static const
#include "shaders/psNightSpecular.h"
static const
#include "shaders/nightDetail.h"
static const
#include "shaders/psNightDetailSpecular.h"
static const
#include "shaders/psWaterMap.h"
static const
#include "shaders/psWaterMapNight.h"

void EngineDD::InitPixelShaders()
{

	if (_pixelShaders<0x100) return;

	// variouse pixel shaders  experiments
	static const DWORD *const psList
	[NPixelShaderSpecular][NPixelShaderModes][NPixelShaders]=
	{
		{
			{
				// normal shaders
				dwPsSpecularPixelShader,
				dwPsDetailSpecularPixelShader,
				dwPsGrassPixelShader,
				dwPsWaterMapPixelShader,
			},
			{
				// night shaders
				//dwNightPixelShader,
				dwPsNightSpecularPixelShader,
				dwPsNightDetailSpecularPixelShader,
				dwPsNightDetailSpecularPixelShader,
				dwPsWaterMapNightPixelShader,
			}
		},
		{
			{
				// normal shaders
				dwPsNormalPixelShader,
				dwPsDetailPixelShader,
				dwPsGrassPixelShader,
				dwPsWaterMapPixelShader,
			},
			{
				// night shaders
				//dwNightPixelShader,
				dwNightPixelShader,
				dwNightDetailPixelShader,
				dwNightDetailPixelShader,
				dwPsWaterMapNightPixelShader,
			}
		},
	};

	for (int s=0; s<NPixelShaderSpecular; s++)
	for (int m=0; m<NPixelShaderModes; m++)
	for (int i=0; i<NPixelShaders; i++)
	{
		const DWORD *psData = psList[s][m][i];
		if (psData) _d3DDevice->CreatePixelShader(psData,&_pixelShader[s][m][i]);
		else _pixelShader[s][m][i] = NULL;
	}

	if (_usePixelShaders)
	{
		DoSelectPixelShader(PSNormal,PSMDay,PSSNormal);
	}

}


void EngineDD::DeinitPixelShaders()
{
	if (_pixelShaders<0x100) return;
	DoSelectPixelShader(PSNone,PSMDay,PSSSpecular);
	for (int s=0; s<NPixelShaderSpecular; s++)
	for (int m=0; m<NPixelShaderModes; m++)
	for (int i=0; i<NPixelShaders; i++)
	{
		 if (_pixelShader[s][m][i]) _d3DDevice->DeletePixelShader(_pixelShader[s][m][i]);
	}
}


void EngineDD::DoSelectPixelShader
(
	PixelShaderID ps, PixelShaderMode mode, PixelShaderSpecular spec
)
{
	if (_pixelShaders<0x100) return;
	
	if (ps<PSNone) 
	{
		#if DO_TEX_STATS
			if (LogStatesOnce)
			{
				LogF("Set pixel shader %d,%d",ps,mode);
			}
		#endif
		_d3DDevice->SetPixelShader( _pixelShader[spec][mode][ps] );
		if (ps==PSGrass)
		{
			DoSetGrassParamsPS();
		}
		else if (ps==PSWater)
		{
			LightSun *sun = GScene->MainLight();
			FLOAT lightDir[4] =
			{
				sun->SunDirection().X(),
				sun->SunDirection().Y(),
				sun->SunDirection().Z(),
				0
			};
			/*
			Vector3 normal(0,-1,0);
			FLOAT normalDir[4] =
			{
				//normal.X()*0.5+0.5,normal.Y()*0.5+0.5,normal.Z()*0.5+0.5,
				normal.X(),normal.Y(),normal.Z(),
				0
			};
			*/
			_d3DDevice->SetPixelShaderConstant( PS_LIGHT_DIR, lightDir, 1 );
			//_d3DDevice->SetPixelShaderConstant( 3, normalDir, 1 );
		}
	}
	else
	{
		#if DO_TEX_STATS
			if (LogStatesOnce)
			{
				LogF("Set pixel shader NULL");
			}
		#endif
		_d3DDevice->SetPixelShader(NULL);
	}
	_pixelShaderSel = ps;
	_pixelShaderModeSel = mode;
	_pixelShaderSpecularSel = spec;
}

#endif

#if VERTEX_SHADERS

static const
#include "shaders/vsDayGrass.h"


// default source vertex (SVertex)

static const float GrassConst[]=
{
	32,0.5,1,0
};

#define VS_CONST(x) \
		*(DWORD *)&x[0],*(DWORD *)&x[1],*(DWORD *)&x[2],*(DWORD *)&x[3]

static const DWORD SVertexVSDecl[] =
{
	D3DVSD_CONST(CONST_32_05_1_0,1),VS_CONST(GrassConst),
	D3DVSD_STREAM( 0 ),
	D3DVSD_REG( D3DVSDE_POSITION,  D3DVSDT_FLOAT3 ),
	D3DVSD_REG( D3DVSDE_NORMAL,    D3DVSDT_FLOAT3 ),
	D3DVSD_REG( D3DVSDE_TEXCOORD0, D3DVSDT_FLOAT2 ),
	D3DVSD_END()
};

void EngineDD::InitVertexShaders()
{
	if (_vertexShaders<0x100) return;

	static const DWORD *const vsList[NVertexShaders]=
	{
		#if _RELEASE
			dwVsDayGrassVertexShader,
		#else
			NULL,
		#endif
		dwVsDayGrassVertexShader
	};
	for (int i=0; i<NVertexShaders; i++)
	{
		const DWORD *vs = vsList[i];
		if (vs)
		{
			HRESULT hr = _d3DDevice->CreateVertexShader
			(
				SVertexVSDecl,vs,&_vertexShader[i],0
			);
			if (FAILED(hr))
			{
				LogF("Error in Vertex shader creation");
				_vertexShader[i] = MYSFVF;
			}

		}
		else
		{
			_vertexShader[i] = MYSFVF;
		}
	}
	//DoSelectVertexShader(VSNormal);
}

void EngineDD::DeinitVertexShaders()
{
	for (int i=0; i<NVertexShaders; i++)
	{
		if (_vertexShader[i]) _d3DDevice->DeleteVertexShader(_vertexShader[i]);
	}
}

void EngineDD::DoSelectVertexShader(VertexShader mode)
{
	if (_vertexShaders<0x100) return;
	_vertexShaderSel = mode;
	if (_tlActive)
	{
		_d3DDevice->SetVertexShader(_vertexShader[mode]);
		//Log("Vertex shader %x selected",_vertexShader[mode]);
		if (mode==VSDayGrass)
		{
			DoSetGrassParamsVS();
		}
	}
}

#endif

void EngineDD::EnableNightEye(float night)
{
	if (_nightVision) night = 0;
	// select normal or night pixel shader
	if (fabs(_nightEye-night)<0.01f) return;
	FlushQueues();
	_nightEye = night;
	#if PIXEL_SHADERS
	PixelShaderMode mode = _nightEye>0.01 ? PSMNight : PSMDay;
	SelectPixelShaderMode(mode);

	if (mode==PSMNight)
	{
		FLOAT rgbEyeConstant[4] =
		{
			// standart day weights
			//0.299f, 0.587f, 0.114f, 0.0f
			// most night
			// last (a) component controls ammount of night added
			0.2f, 0.9f, 0.4f, 1-_nightEye
		}; // rgb values
		_d3DDevice->SetPixelShaderConstant( RGB_EYE_COEF, rgbEyeConstant, 1 );
		#if DO_TEX_STATS
			if (LogStatesOnce)
			{
				LogF("Night eye set to %g",_nightEye);
			}
		#endif
	}
	#endif

}

/*!
\patch 1.01 Date 6/25/2001 by Ondra.
- Fixed: Enabled guard band clipping.
- this solves some issues with clipping sun on nVidia cards
- it also increases performance on TnT family cards
\patch 1.16 Date 8/10/2001 by Ondra
- New: Pixel shader (1.0) used during night rendering.
*/

void EngineDD::Init3DState()
{
	HRESULT err;
	_matC = 0, _matD = 0; // w-buffer limits set
	_bias = 0;
	_clipANearEnabled = false,_clipAFarEnabled = false;


	_dxtFormats = 0;
	D3DDEVICE_CREATION_PARAMETERS cpars;
	_d3DDevice->GetCreationParameters(&cpars);

	UINT adapter = cpars.AdapterOrdinal;

	_d3DDevice->GetRenderTarget(_backBuffer.Init());

	D3DSURFACE_DESC bdesc;
	err=_backBuffer->GetDesc(&bdesc);
	if (err != D3D_OK) {DDError("GetSurfaceDesc failed.",err);return;}


	D3DFORMAT bbFormat = bdesc.Format;

	D3DDEVTYPE devType = D3DDEVTYPE_HAL;
	#if 1 //ndef _XBOX
	err = _direct3D->CheckDeviceFormat
	(
		adapter,devType,bbFormat,0,D3DRTYPE_TEXTURE,D3DFMT_DXT1
	);
	if (err==D3D_OK) _dxtFormats |= 1<<1;
	err = _direct3D->CheckDeviceFormat
	(
		adapter,devType,bbFormat,0,D3DRTYPE_TEXTURE,D3DFMT_DXT2
	);
	if (err==D3D_OK) _dxtFormats |= 1<<2;
	err = _direct3D->CheckDeviceFormat
	(
		adapter,devType,bbFormat,0,D3DRTYPE_TEXTURE,D3DFMT_DXT3
	);
	if (err==D3D_OK) _dxtFormats |= 1<<3;
	err = _direct3D->CheckDeviceFormat
	(
		adapter,devType,bbFormat,0,D3DRTYPE_TEXTURE,D3DFMT_DXT4
	);
	if (err==D3D_OK) _dxtFormats |= 1<<4;
	err = _direct3D->CheckDeviceFormat
	(
		adapter,devType,bbFormat,0,D3DRTYPE_TEXTURE,D3DFMT_DXT5
	);
	if (err==D3D_OK) _dxtFormats |= 1<<5;

	LogF("DXT support %x",_dxtFormats);
	#endif

	err = _direct3D->CheckDeviceFormat
	(
		adapter,devType,bbFormat,0,D3DRTYPE_TEXTURE,D3DFMT_A8R8G8B8
	);
	_can8888 = ( err==D3D_OK );
	if (_can8888) LogF("Can 8888 textures");
	// note: 1555 and 4444 support required
	err = _direct3D->CheckDeviceFormat
	(
		adapter,devType,bbFormat,0,D3DRTYPE_TEXTURE,tex_A1R5G5B5
	);
	if (err!=D3D_OK)
	{
		ErrorMessage("Texture format ARGB1555 required.");
	}
	err = _direct3D->CheckDeviceFormat
	(
		adapter,devType,bbFormat,0,D3DRTYPE_TEXTURE,D3DFMT_A4R4G4B4
	);
	if (err!=D3D_OK)
	{
		ErrorMessage("Texture format ARGB4444 required.");
	}

	D3DCAPS8 caps;
	_d3DDevice->GetDeviceCaps(&caps);
	if( caps.DevCaps&D3DDEVCAPS_TEXTUREVIDEOMEMORY )
	{
		_texLoc = TexLocalVidMem;
	}
	else if( caps.DevCaps&D3DDEVCAPS_TEXTURENONLOCALVIDMEM )
	{
		_texLoc = TexNonlocalVidMem;
	}
	else
	{
		_texLoc = TexSysMem;
	}

	_useHWTL = (caps.DevCaps&D3DDEVCAPS_HWTRANSFORMANDLIGHT)!=0;


	_w=bdesc.Width;
	_h=bdesc.Height;

	_minGuardX = 0; // guard band clipping properties
	_maxGuardX = _w;
	_minGuardY = 0;
	_maxGuardY = _h;

	_nightEye = 0;

	// check actual guard band clipping capabilities

	_pixelSize = 32;
	switch (bdesc.Format)
	{
		case D3DFMT_X1R5G5B5:
		case D3DFMT_R5G6B5:
		case D3DFMT_A1R5G5B5:
			_pixelSize = 16;
			break;
	}	

	_depthBpp = 32;

	D3DSURFACE_DESC ddesc;
	ComRef<IDirect3DSurface8> zBuffer;
	_d3DDevice->GetDepthStencilSurface(zBuffer.Init());
	if (zBuffer)
	{
		zBuffer->GetDesc(&ddesc);
		switch (ddesc.Format)
		{
			#ifndef _XBOX
			case D3DFMT_D16_LOCKABLE:
			case D3DFMT_D15S1:
			#endif
			case D3DFMT_D16:
				_depthBpp = 16;
				break;
		}	
	}
	LogF("Bpp %d, depth %d",_pixelSize,_depthBpp);

	// fix: enabled guard band clipping
	#if 1
	float minGuardX = caps.GuardBandLeft;
	float maxGuardX = caps.GuardBandRight;
	float minGuardY = caps.GuardBandTop;
	float maxGuardY = caps.GuardBandBottom;
	float maxBand = 1024*4;
	saturate(minGuardX,-maxBand,0);
	saturate(minGuardY,-maxBand,0);
	saturate(maxGuardX,_w,_w+maxBand);
	saturate(maxGuardY,_h,_h+maxBand);
	_minGuardX = toInt(minGuardX), _maxGuardX = toInt(maxGuardX);
	_minGuardY = toInt(minGuardY), _maxGuardY = toInt(maxGuardY);

	LogF
	(
		"Guard band x %d to %d, y %d to %d",
		_minGuardX,_maxGuardX,_minGuardY,_maxGuardY
	);
	#endif

	if( caps.TextureCaps&D3DPTEXTURECAPS_SQUAREONLY )
	{
		ErrorMessage("Only square texture supported by HW.");
	}
	//_vidMemTextures=false; // AGP uses single heap model - n
	// for soft and MMX avoid bilinear/trilinear filtering

	// now make a Viewport
	// Setup the viewport for a reasonable viewing area
	D3DVIEWPORT8 viewData;
	memset(&viewData,0,sizeof(viewData));
	viewData.X = 0;
	viewData.Y = 0;
	//viewData.dwWidth	= _wWork;
	//viewData.dwHeight = _hWork;
	viewData.Width	= _w;
	viewData.Height = _h;
	viewData.MinZ = 0.0f;
	viewData.MaxZ = 1.0f;

	err = _d3DDevice->SetViewport(&viewData);
	if( err!=D3D_OK ) DDError("Cannot set 3D Viewport.",err);
	
	// Set default render state
	D3DSetRenderStateOpt(D3DRS_ZFUNC,D3DCMP_LESSEQUAL,false);
	D3DSetRenderStateOpt(D3DRS_ZWRITEENABLE,TRUE,false);
	if (_canWBuffer && (_depthBpp<=16 || _canWBuffer32) && _userEnabledWBuffer)
	{
		D3DSetRenderStateOpt(D3DRS_ZENABLE,D3DZB_USEW,false);
		LogF("Using w-buffer");
		_useWBuffer = true;
	}
	else
	{
		D3DSetRenderStateOpt(D3DRS_ZENABLE,D3DZB_TRUE,false);
		_useWBuffer = false;
	}

	D3DSetRenderStateOpt(D3DRS_SHADEMODE,D3DSHADE_GOURAUD,false);
	D3DSetRenderStateOpt(D3DRS_CULLMODE,D3DCULL_CCW,false);

	#ifndef _XBOX
	D3DSetRenderStateOpt(D3DRS_CLIPPING,FALSE,false);
	#endif
	D3DSetRenderStateOpt(D3DRS_LIGHTING,FALSE,false);

	#if NO_SET_RS
		_d3DDevice->SetRenderState(D3DRS_LIGHTING,FALSE);
		_d3DDevice->SetRenderState(D3DRS_CLIPPING,FALSE);
	#endif

	// on 16-bit devices use dithering
	D3DSetRenderStateOpt(D3DRS_DITHERENABLE,_pixelSize<=16,false);

	D3DSetRenderStateOpt(D3DRS_ALPHATESTENABLE,FALSE,false);
	D3DSetRenderStateOpt(D3DRS_ALPHAFUNC,D3DCMP_GREATER,false);
	D3DSetRenderStateOpt(D3DRS_ALPHAREF,0x1,false);

	D3DSetRenderStateOpt(D3DRS_ALPHABLENDENABLE,TRUE,false);
	D3DSetRenderStateOpt(D3DRS_SRCBLEND,D3DBLEND_SRCALPHA,false);
	D3DSetRenderStateOpt(D3DRS_DESTBLEND,D3DBLEND_INVSRCALPHA,false);
	

	_lastClampU=_lastClampV=false;

	D3DCOLOR fogColor=FAST_D3DRGBA(_fogColor.R(),_fogColor.G(),_fogColor.B(),1);

	#if !NO_FOG			
	D3DSetRenderStateOpt(D3DRS_FOGENABLE,TRUE,false);
	#endif
	D3DSetRenderStateOpt(D3DRS_FOGCOLOR,fogColor,false);

	// to do: we should not use bilinear filtering on devices
	// which do not support clamping
	

	// try to use trilinear interpolation if possible
	// always use bilinear interpolation

	err=D3DSetTextureStageStateOpt(0,D3DTSS_MINFILTER,D3DTEXF_LINEAR,false);
	if( err!=D3D_OK ) DDError("D3DTSS_MINFILTER",err);
	err=D3DSetTextureStageStateOpt(0,D3DTSS_MAGFILTER,D3DTEXF_LINEAR,false);
	if( err!=D3D_OK ) DDError("D3DTSS_MAGFILTER",err);
	D3DSetTextureStageStateOpt(0,D3DTSS_MIPFILTER,D3DTEXF_POINT,false);

  D3DSetTextureStageStateOpt( 0, D3DTSS_TEXCOORDINDEX, 0,false);
	
  D3DSetTextureStageStateOpt( 1, D3DTSS_ADDRESSU, D3DTADDRESS_WRAP,false);
  D3DSetTextureStageStateOpt( 1, D3DTSS_ADDRESSV, D3DTADDRESS_WRAP,false);
	D3DSetTextureStageStateOpt( 1, D3DTSS_TEXCOORDINDEX, 0,false);
  D3DSetTextureStageStateOpt( 1, D3DTSS_MAGFILTER, D3DTEXF_LINEAR,false);
  D3DSetTextureStageStateOpt( 1, D3DTSS_MINFILTER, D3DTEXF_LINEAR,false);
  D3DSetTextureStageStateOpt( 1, D3DTSS_MIPFILTER, D3DTEXF_POINT,false);
  D3DSetTextureStageStateOpt( 1, D3DTSS_COLORARG1, D3DTA_TEXTURE,false);
  D3DSetTextureStageStateOpt( 1, D3DTSS_COLORARG2, D3DTA_CURRENT,false);
  D3DSetTextureStageStateOpt( 1, D3DTSS_COLOROP,D3DTOP_DISABLE,false);

	D3DSetTextureStageStateOpt(1, D3DTSS_ALPHAARG1, D3DTA_TEXTURE,false);
	D3DSetTextureStageStateOpt(1, D3DTSS_ALPHAARG2, D3DTA_CURRENT,false);

	// moved constant values from DoStencil exclusion
	D3DSetRenderStateOpt(D3DRS_STENCILENABLE,TRUE,false);
	D3DSetRenderStateOpt(D3DRS_STENCILREF,0,false);

	DoStencilExclusion(false,false);

	#if VERTEX_SHADERS
	InitVertexShaders();
	#endif

	// some pixel shader experiments
	#if PIXEL_SHADERS
	InitPixelShaders();
	#endif

	DoEnableDetailTexGen(_tlActive ? TGNone : TGFixed,false);


	_d3DDevice->SetTexture( 1,NULL );
	_d3DDevice->SetTexture( 0,NULL );

	_lastSpec = 0;
	_prepSpec = 0;
	_lastHandle=NULL;

	_lastQueueSource = NULL;

	_sunEnabled = true;
	DoSwitchTL(false);

	_materialSet.ambient = HBlack;
	_materialSet.diffuse = HBlack;
	_materialSet.forcedDiffuse = HBlack;
	_materialSet.emmisive = HBlack;
	_materialSet.specFlags = 0;
	_materialSetSpec = 0;


}

#ifdef _XBOX
#define ENABLE_DXTO 1 // enable for opaque / transparent
#else
#define ENABLE_DXTO 1 // enable for opaque / transparent
#endif

#define ENABLE_DXTA 0 // enable for alpha

#define CAN_DXT(i) ( (_dxtFormats&(1<<(i)))!=0 )
void EngineDD::InitVRAMPixelFormat( D3DFORMAT &pf, PacFormat format, bool enableDXT )
{
	switch( format )
	{
		case PacDXT1:
			#if ENABLE_DXTO
			if (CAN_DXT(1))
			{
				pf = D3DFMT_DXT1;
			}
			else
			#endif
			{
				pf = tex_A1R5G5B5;
			}
			break;
		case PacARGB1555:
			pf = tex_A1R5G5B5;
		break;
		case PacRGB565:
			pf = tex_R5G6B5;
		break;
		case PacAI88:
			if (Can88())
			{
				// TODO: correct AI format
				pf = tex_A8L8;
				break;
			}
		// note: previous case may fall through
		case PacARGB8888:
			if (Can8888())
			{
				pf = tex_A8R8G8B8;
				break;
			}
		// note: previous case may fall through
		case PacARGB4444:
			#if ENABLE_DXTA
			if( enableDXT && CAN_DXT(2) )
			{
				pf = D3D_FMTDXT2;
			}
			else if( CAN_DXT(3) )
			{
				pf = D3D_FMTDXT3;
			}
			else
			#endif
			{
				pf = tex_A4R4G4B4;
			}
		break;
		case PacP8:
			pf = D3DFMT_P8;
			Fail("Palette textures obsolete");
		break;
		default:
			ErrorMessage("Texture has bad pixel format (VRAM).");
		break;
	}
}

#include "debugTrap.hpp"


static const char *SurfaceFormatName(int format)
{
	#define FN(x) case D3DFMT_##x: return #x
	switch (format)
	{
		FN(UNKNOWN);

		#ifndef _XBOX
    FN(R8G8B8);
    FN(R3G3B2);
    FN(A8P8);
    FN(A4L4);
    FN(W11V11U10);
    FN(X4R4G4B4);
    FN(A8R3G3B2);
    FN(X8L8V8U8);
    FN(D32);
    FN(D15S1);
    FN(D24X8);
    FN(D24X4S4);

		// following formats map to some existing format
    FN(DXT3);
    FN(DXT5);
    FN(D16_LOCKABLE);
		#endif
    FN(A8R8G8B8);
    FN(X8R8G8B8);
    FN(R5G6B5);
    FN(X1R5G5B5);
    FN(A1R5G5B5);
    FN(A4R4G4B4);
    FN(A8);

    FN(P8);

    FN(L8);
    FN(A8L8);

    FN(V8U8);
    FN(L6V5U5);
    FN(Q8W8V8U8);
    FN(V16U16);

    FN(UYVY);
    FN(YUY2);
    FN(DXT1);
    FN(DXT2);
    FN(DXT4);

    FN(D24S8);
    FN(D16);

		#ifdef _XBOX
			FN(LIN_A1R5G5B5);
			FN(LIN_A4R4G4B4);
			FN(LIN_A8      );
			FN(LIN_A8B8G8R8);
			FN(LIN_A8R8G8B8);
			FN(LIN_B8G8R8A8);
			FN(LIN_G8B8    );
			FN(LIN_R4G4B4A4);
			FN(LIN_R5G5B5A1);
			FN(LIN_R5G6B5  );
			FN(LIN_R6G5B5  );
			FN(LIN_R8B8    );
			FN(LIN_R8G8B8A8);
			FN(LIN_X1R5G5B5);
			FN(LIN_X8R8G8B8);

			FN(LIN_A8L8);
			FN(LIN_AL8 );
			FN(LIN_L16 );
			FN(LIN_L8  );
		#endif
	}
	return "OTHER";
}

typedef IDirect3D8 *WINAPI Direct3DCreate8F(UINT SDKVersion);

RString EngineDD::GetDebugName() const
{
	RString ret = "Direct3D ";
	if ( _useHWTL) ret = ret + "HW T&L ";

	if (!_d3DDevice)
	{
		ret = ret + ", Device: <null>";
	}
	else
	{
		D3DDEVICE_CREATION_PARAMETERS cp;
		_d3DDevice->GetCreationParameters(&cp);
		D3DADAPTER_IDENTIFIER8 id;
		_direct3D->GetAdapterIdentifier(cp.AdapterOrdinal,D3DENUM_NO_WHQL_LEVEL,&id);
		char driverVersion[256];
		sprintf
		(
			driverVersion,"%d.%d.%d.%d",
			HIWORD(id.DriverVersion.HighPart),LOWORD(id.DriverVersion.HighPart),
			HIWORD(id.DriverVersion.LowPart),LOWORD(id.DriverVersion.LowPart)
		);
		ret = ret + ", Device: " + RString(id.Description);
		ret = ret + ", Driver:" + RString(id.Driver) + RString(" ") + RString(driverVersion);
	}

	return ret;
}

void EngineDD::CreateD3D()
{
	if (_direct3D) return;
	#ifndef _XBOX
	HMODULE lib = LoadLibrary("d3d8.dll");
	if (!lib)
	{
		ErrorMessage("DX8 required.");
		return;
	}

	Direct3DCreate8F *create = (Direct3DCreate8F *)GetProcAddress
	(
		lib,"Direct3DCreate8"
	);
	if (!create)
	{
		ErrorMessage("Direct3DCreate8 required.");
		return;
	}
	_direct3D = create(D3D_SDK_VERSION);

	// we know DX 8.1 features are not used
	// if 8.1 creating will fail, try creating 8.0
	#define D3D_SDK_VERSION_80 120
	if (!_direct3D)
	{
		_direct3D = create(D3D_SDK_VERSION_80);
	}

	#else
	_direct3D = Direct3DCreate8(D3D_SDK_VERSION);
	#endif

	if (!_direct3D)
	{
		ErrorMessage("DX8 SDK version %d required.",D3D_SDK_VERSION);
		return;
	}
}

void EngineDD::DestroyD3D()
{
	_direct3D.Free();
}

#if _MSC_VER < 1300
typedef struct tagMONITORINFO {  
    DWORD  cbSize; 
    RECT   rcMonitor; 
    RECT   rcWork; 
    DWORD  dwFlags; 
} MONITORINFO, *LPMONITORINFO; 
#endif

typedef BOOL WINAPI GetMonitorInfoT(HMONITOR mon, LPMONITORINFO mi);


void EngineDD::SearchMonitor(int &x, int &y, int &w, int &h)
{
	#ifndef _XBOX
	MONITORINFO mi;
	int adapter = D3DAdapter;
	if (adapter<0) return;


	HMONITOR mon = _direct3D->GetAdapterMonitor(adapter);
// if running in windowed mode on multimon window, search all monitors
	HMODULE lib = LoadLibrary("user32.dll");
	if (!lib) return;
	GetMonitorInfoT *getmon = (GetMonitorInfoT *)GetProcAddress(lib,"GetMonitorInfoA");
	if (getmon)
	{
		memset(&mi,0,sizeof(mi));
		mi.cbSize = sizeof(mi);
		getmon(mon,&mi);
		// we have monitor information, adjust window position
		if (x<mi.rcWork.left) x = mi.rcWork.left;
		if (y<mi.rcWork.top) y = mi.rcWork.top;
	}
	FreeLibrary(lib);
	#endif
}

void EngineDD::FindMode(int adapter)
{
	// prepare presentation parameters based on _w, _h, _pixelSize
	// first select backbuffer format
	D3DDISPLAYMODE bestDisplayMode;
	int bestCost = INT_MAX;
	bestDisplayMode.Format = D3DFMT_X8R8G8B8;
	bestDisplayMode.RefreshRate = D3DPRESENT_RATE_DEFAULT;
	bestDisplayMode.Width = _w;
	bestDisplayMode.Height = _h;
	if (_windowed)
	{
		_direct3D->GetAdapterDisplayMode(adapter,&bestDisplayMode);
		// check 
	}
	else
	{
		for (int mode=0; mode<_direct3D->GetAdapterModeCount(adapter); mode++)
		{
			D3DDISPLAYMODE displayMode;
			_direct3D->EnumAdapterModes(adapter,mode,&displayMode);
			
			int bpp = 16;
			int cost = 0;
			#if 0 //def _XBOX
			LogF
			(
				"Display Format %dx%d (%d) %s supported",
				displayMode.Width,displayMode.Height,displayMode.RefreshRate,
				SurfaceFormatName(displayMode.Format)
			);
			#endif
			switch (displayMode.Format)
			{
				case D3DFMT_X8R8G8B8: cost += 0; bpp = 32; break;
				case D3DFMT_R5G6B5: cost += 100; bpp = 16; break;
				case D3DFMT_X1R5G5B5: case D3DFMT_A1R5G5B5: cost += 110; bpp = 16; break;

				case D3DFMT_A8R8G8B8: cost += 180; bpp = 32; break;
				#ifndef _XBOX
				case D3DFMT_R8G8B8: cost += 200; bpp = 24; break;
				#endif
				#ifdef _XBOX
				case D3DFMT_LIN_X8R8G8B8: cost += 1000; bpp = 32; break;
				case D3DFMT_LIN_A8R8G8B8: cost += 1180; bpp = 32; break;
				case D3DFMT_LIN_R5G6B5: cost += 1100; bpp = 16; break;
				case D3DFMT_LIN_X1R5G5B5: case D3DFMT_LIN_A1R5G5B5: cost += 1110; bpp = 16; break;
				#endif

				default: cost += 500; break; // unknown format - last resort
			}
			cost +=
			(
				abs(_w*_h-displayMode.Width*displayMode.Height)*10 + 
				abs(_w-displayMode.Width)*10 + 
				abs(_h-displayMode.Height)*10 +
				abs(_pixelSize-bpp)*200
			);
			if (_refreshRate>0)
			{
				cost += abs(_refreshRate-displayMode.RefreshRate);
			}
			else
			{
				cost += abs(75-displayMode.RefreshRate); // higher refresh rate is always better
			}
			if (cost<bestCost)
			{
				bestCost = cost;
				bestDisplayMode = displayMode;
			}
		}
		LogF
		(
			"mode %dx%d (%d Hz) (%s)",
			bestDisplayMode.Width,bestDisplayMode.Height,
			bestDisplayMode.RefreshRate,
			SurfaceFormatName(bestDisplayMode.Format)
		);
	}

	
	// select suitable depthbuffer formats
	#if 0
		D3DFORMAT depthFormat = D3DFMT_D24S8;
	#else

	D3DFORMAT depthFormat = D3DFMT_D16;
	const static D3DFORMAT depthFormats[]=
	{
		#if 1 //!_DEBUG
			D3DFMT_D24S8,
			#ifndef _XBOX
				D3DFMT_D24X4S4,
				D3DFMT_D15S1, // stencil formats
				D3DFMT_D32,
				D3DFMT_D24X8,
				D3DFMT_D16_LOCKABLE,
			#endif
		#endif
		D3DFMT_D16 // normal depth formats
	};

	D3DDEVTYPE devType = D3DDEVTYPE_HAL;

	D3DCAPS8 caps;
	_direct3D->GetDeviceCaps(adapter,devType,&caps);
	if (caps.StencilCaps)
	{
		LogF("Stencil caps %d",caps.StencilCaps);
	}

	#ifndef _SUPER_RELEASE
	for (int di=0; di<sizeof(depthFormats)/sizeof(*depthFormats); di++)
	{
		D3DFORMAT format = depthFormats[di];

		HRESULT ok = _direct3D->CheckDeviceFormat
		(
			adapter,devType,bestDisplayMode.Format,
			D3DUSAGE_DEPTHSTENCIL,D3DRTYPE_SURFACE,
			format
		);
		if (ok==D3D_OK)
		{
			LogF("Depth %s supported",SurfaceFormatName(format));
			ok = _direct3D->CheckDepthStencilMatch
			(
				adapter,devType,
				bestDisplayMode.Format,bestDisplayMode.Format,
				format
			);
			if (ok!=D3D_OK)
			{
				LogF
				(
					"DepthStencilMatch check (%s/%s) failed",
					SurfaceFormatName(bestDisplayMode.Format),SurfaceFormatName(format)
				);
			}
		}
	}
	#endif
	for (int di=0; di<sizeof(depthFormats)/sizeof(*depthFormats); di++)
	{
		D3DFORMAT format = depthFormats[di];

		HRESULT ok = _direct3D->CheckDeviceFormat
		(
			adapter,devType,bestDisplayMode.Format,
			D3DUSAGE_DEPTHSTENCIL,D3DRTYPE_SURFACE,
			format
		);
		if (ok==D3D_OK)
		{
			if (format==D3DFMT_D16_LOCKABLE)
			{
				LogF("Lockable z-buffer support present");
			}
			ok = _direct3D->CheckDepthStencilMatch
			(
				adapter,devType,
				bestDisplayMode.Format,bestDisplayMode.Format,
				format
			);
			if (ok==D3D_OK)
			{
				LogF
				(
					"Display Format %s/%s",
					SurfaceFormatName(bestDisplayMode.Format),
					SurfaceFormatName(format)
				);
				depthFormat = format;
				break;
			}
		}
	}
	#endif

	switch (depthFormat)
	{
    case D3DFMT_D24S8:
		#ifndef _XBOX
		case D3DFMT_D24X4S4: case D3DFMT_D15S1:
		#endif
			_hasStencilBuffer = true;
			break;
		default:
			_hasStencilBuffer = false;
			break;
	}



	if (_windowed)
	{
		_pp.BackBufferWidth = _w;
		_pp.BackBufferHeight = _h;
		_pp.FullScreen_RefreshRateInHz = 0;
		_refreshRate = 0;
	}
	else
	{
		_pp.BackBufferWidth = bestDisplayMode.Width;
		_pp.BackBufferHeight = bestDisplayMode.Height;
		_pp.FullScreen_RefreshRateInHz = bestDisplayMode.RefreshRate;
		_w = bestDisplayMode.Width;
		_h = bestDisplayMode.Height;
		_refreshRate = bestDisplayMode.RefreshRate;
	}


	_pp.BackBufferFormat = bestDisplayMode.Format;

	//_pp.BackBufferCount = 3;
	_pp.BackBufferCount = 2;
	_pp.MultiSampleType = D3DMULTISAMPLE_NONE;
	_pp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	_pp.hDeviceWindow = _hwndApp;
	_pp.Windowed = _windowed;
	_pp.EnableAutoDepthStencil = true;
	_pp.AutoDepthStencilFormat = depthFormat;
	_pp.Flags = 0;
	if (!_pp.Windowed)
	{
		// wait for first v-sync
		_pp.FullScreen_PresentationInterval = D3DPRESENT_INTERVAL_ONE;
		_pp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	}
	else
	{
		_pp.FullScreen_PresentationInterval = 0;
	}
}


/*!
\patch 1.21 Date 8/23/2001 by Ondra
- Improved: Added more information to message "Cannot create 3D device"
- Improved: W-buffer used on nVidia cards to impove z-buffer precision
  in 16b modes.
\patch 1.30 Date 11/02/2001 by Ondra
- New: Ground multitexturing can be turned off in Video options.
This can be used to resolve compatibility issues
with some graphics adapters ("white ground problem").
\patch 1.34 Date 12/06/2001 by Ondra
- Fixed: W-buffer support was broken.
- New: W-buffer support enabled on nVidia cards in both 16b and 32b modes.
\patch 1.44 Date 2/13/2002 by Ondra
- Fixed: When no display adapter is selected in preferences
and no adapter is recognized, select first one.
This solves error message "Cannot create 3D device: Adapter -1"
*/

void EngineDD::Init3D()
{
	// try to create device (from best to worst)
	HRESULT err;

	int adapter = D3DAdapter;
	#ifndef _XBOX
	if (adapter<0 || adapter>=_direct3D->GetAdapterCount())
	{
		// autodetect adapter

		int maxLevel = 0;
		for (int ad=0; ad<_direct3D->GetAdapterCount(); ad++)
		{
			D3DADAPTER_IDENTIFIER8 id;
			_direct3D->GetAdapterIdentifier(ad,D3DENUM_NO_WHQL_LEVEL,&id);
			static const struct {const char *text;int level;} AdLevel[]=
			{
				"GeForce3",400,
				"Gladiac 920",400,
				"Voodoo5",300,
				"G850",270,
				"G800",250,
				"GeForce2",250,
				"Voodoo4",200,
				"GeForce",150,
				"KYRO",140,
				"G450",120,
				"TnT2",115, "TNT2",115,
				"TnT",110, "TNT",110,
				"Voodoo3",100,
				"G400",50,
				"Savage4",30,
				"Voodoo2",25,
				"740",20,
				"Voodoo",10,
			};

			for (int i=0; i<sizeof(AdLevel)/sizeof(*AdLevel); i++)
			{
				if (strstr(id.Description,AdLevel[i].text))
				{
					int level = AdLevel[i].level;
					LogF
					(
						"%s: Recognized as %s - %d",
						id.Description,AdLevel[i].text,level
					);
					if (level>maxLevel)
					{
						maxLevel = level;
						adapter = ad;
					}
					break;
				}
			}

		}
		if (adapter<0)
		{
			LogF("No adapter recognized by name");
			// if no adapter is recognized, select the first one
			adapter = 0;
		}
	}
	#endif

	FindMode(adapter);

	D3DDEVTYPE devType = D3DDEVTYPE_HAL;
	//D3DDEVTYPE devType = D3DDEVTYPE_REF;

	D3DCAPS8 caps;
	_direct3D->GetDeviceCaps(adapter,devType,&caps);

	DWORD behavior = D3DCREATE_SOFTWARE_VERTEXPROCESSING;

	#if !VERTEX_SHADERS || _RELEASE
	if (caps.DevCaps&D3DDEVCAPS_HWTRANSFORMANDLIGHT)
	{
		behavior = D3DCREATE_HARDWARE_VERTEXPROCESSING;
		#ifndef _XBOX
		if (caps.DevCaps&D3DDEVCAPS_PUREDEVICE)
		{
			behavior |= D3DCREATE_PUREDEVICE;
			LogF("T&L HW detected (Pure)");
		}
		else
		{
			LogF("T&L HW detected");
		}
		#endif
	}
	#endif

	err = _direct3D->CreateDevice
	(
		adapter,devType,_hwndApp,behavior,
		&_pp,_d3DDevice.Init()
	);

	if (_d3DDevice)
	{
	}
	else
	{
		_pp.BackBufferCount = 1;
	}

	if (err!=D3D_OK || !_d3DDevice)
	{
		// number of back buffer may be adjusted - try again
		err = _direct3D->CreateDevice
		(
			adapter,devType,_hwndApp,behavior,
			&_pp,_d3DDevice.Init()
		);
	}
	if (err!=D3D_OK || !_d3DDevice)
	{
		DDError("Cannot create 3D device",err);
		RptF
		(
			"Resolution failed: %dx%dx (%d Hz)",
			_pp.BackBufferWidth,_pp.BackBufferHeight,_pp.FullScreen_RefreshRateInHz
		);
		#ifndef _XBOX
		D3DADAPTER_IDENTIFIER8 id;
		_direct3D->GetAdapterIdentifier(adapter,D3DENUM_NO_WHQL_LEVEL,&id);
		ErrorMessage
		(
			"Cannot create 3D device:\n"
			"  Adapter %d (%s) %s\n"
			"  Resolution %dx%d, format %s/%s, refresh %d Hz.\n"
			"  Error %s",
			(int)adapter,(const char *)id.Description,
			_pp.Windowed ? "Windowed" : "Fullscreen",
			_pp.BackBufferWidth,_pp.BackBufferHeight,
			SurfaceFormatName(_pp.BackBufferFormat),
			SurfaceFormatName(_pp.AutoDepthStencilFormat),
			_pp.FullScreen_RefreshRateInHz,
			DXGetErrorString8(err)
		);
		#endif
	}
	LogF("Backbuffers: %d",_pp.BackBufferCount);

	_canZBias = false;
	if (!_hasStencilBuffer)
	{
		// no stencil buffer - we need z-bias badly
		_canZBias = (caps.RasterCaps&D3DPRASTERCAPS_ZBIAS)!=0;
		if (_canZBias)
		{
			LogF("Can z-bias");
		}
	}
	_canWBuffer = (caps.RasterCaps&D3DPRASTERCAPS_WBUFFER)!=0;
	_canWBuffer32 = _canWBuffer;

	#if PIXEL_SHADERS
	_vertexShaders = 0; // pixel shader version
	_pixelShaders = 0;
	_usePixelShaders = false;
	_vertexShaders = caps.VertexShaderVersion&0xffff;
	_pixelShaders = caps.PixelShaderVersion&0xffff;
	if (_vertexShaders)
	{
		LogF("Vertex shaders version %d.%d",_vertexShaders>>8,_vertexShaders&0xff);
	}
	if (_pixelShaders)
	{
		LogF("Pixel shaders version %d.%d",_pixelShaders>>8,_pixelShaders&0xff);
	}
	if (_pixelShaders>0x100)
	{
		_usePixelShaders = true;
	}
	#endif

	if (_canWBuffer)
	{
		LogF("Can w-buffer");
	}

	// check if detail texturing can be enabled
	// generic test
	_canDetailTex = true;
	if (caps.MaxTextureBlendStages<2 || caps.MaxSimultaneousTextures<2)
	{
		_canDetailTex = false;
		LogF("Not enough texture stages for detail texturing");
	}
	else
	{
		// check for required blending ops

		/*
		D3DSetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
		D3DSetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);
		D3DSetTextureStageState( 0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
		D3DSetTextureStageState( 0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);
		D3DSetTextureStageState( 1, D3DTSS_COLORARG1, D3DTA_CURRENT);
		D3DSetTextureStageState( 1, D3DTSS_COLORARG2, D3DTA_TEXTURE | D3DTA_ALPHAREPLICATE);
		*/

		const int requiredOps = D3DTEXOPCAPS_MODULATE|D3DTEXOPCAPS_MODULATE2X;
		if ((caps.TextureOpCaps&requiredOps)!=requiredOps)
		{
			_canDetailTex = false;
			LogF("Texture ops not supported");
		}

	}

	if (!_canDetailTex)
	{
		Engine::SetMultitexturing(false);
	}

	// test for some known adapters
	// (based on driver name?)



	#if 1
	if (EnableHWTL && (caps.DevCaps&D3DDEVCAPS_HWTRANSFORMANDLIGHT))
	{
		// this should be removed
		_useDXTL = true;
	}
	#endif

	_maxLights = 8;
	if (_useDXTL)
	{
		saturateMin(_maxLights,caps.MaxActiveLights);
	}
	// patch: detection of known device capabilities
	#ifndef _XBOX
	const int vendor_nVidia = 0x10de;
	const int vendor_3Dfx = 0x121a;
	D3DADAPTER_IDENTIFIER8 id;
	_direct3D->GetAdapterIdentifier(adapter,D3DENUM_NO_WHQL_LEVEL,&id);

	if (id.VendorId==vendor_3Dfx)
	{
		const int voodoo3 = 5;
		if (id.DeviceId==voodoo3)
		{
			LogF("Voodoo3 detected - detail texture disabled");
			_canDetailTex = false;
		}
	}

	// on nVidia cards do not use w-buffer, it is broken
	if (id.VendorId==vendor_nVidia)
	#endif
	{
		//_canWBuffer32 = false;
		if (_canZBias)
		{
			// note: z-bias was not supported with w-buffering in GeForce, GeForce2
			LogF("nVidia detected - z-bias disabled");
			_canZBias = false;
		}
	}
	#ifdef _XBOX
		D3DDISPLAYMODE dmode;
		_d3DDevice->GetDisplayMode(&dmode);
		LogF
		(
			"Format %s:%dx%d",
			SurfaceFormatName(dmode.Format),dmode.Width,dmode.Height
		);
	#endif

	// check actual device capabilities
	Init3DState();
}

AbstractTextBank *EngineDD::TextBank() {return _textBank;}

void EngineDD::CreateTextBank()
{
	if( !_textBank )
	{
		_textBank=new TextBankD3D(this);
		if( !_textBank )
		{
			ErrorMessage("Cannot create texture bank.");
			return;
		}
	}
}




void EngineDD::D3DDrawTexts()
{
}

//#define SPEC_2D (NoZBuf|IsAlpha|ClampU|ClampV|IsAlphaFog)

void EngineDD::Draw2D
(
	const Draw2DPars &pars, const Rect2DAbs &rect, const Rect2DAbs &clip
)
{
	//Draw2DPars pars = _pars;
	//Texture *tex = GPreloadedTextures.New(TextureBlack);
	//pars.mip = _textBank->UseMipmap(tex,0,0);

	Assert( pars.mip.IsOK() );
	if( !pars.mip.IsOK() ) return;

	if (_resetNeeded) return;

	// perform simple clipping
	float xBeg=rect.x-0.5f,xEnd=xBeg+rect.w;
	float yBeg=rect.y-0.5f,yEnd=yBeg+rect.h;

	float uBeg=0;
	float vBeg=0;
	float uEnd=1;
	float vEnd=1;

	float xc=floatMax(clip.x,0);
	float yc=floatMax(clip.y,0);
	float xec=floatMin(clip.x+clip.w,_w);
	float yec=floatMin(clip.y+clip.h,_h);

	if( xBeg<xc )
	{
		// -xBeg is out, side length is 2*sizeX
		uBeg=(xc-xBeg)/rect.w;
		xBeg=xc;
	}
	if( xEnd>xec )
	{
		// xEnd-_w is out, side length is 2*sizeX
		uEnd=1-(xEnd-xec)/rect.w;
		xEnd=xec;
	}
	if( yBeg<yc )
	{
		// -yBeg is out, side length is 2*sizeY
		vBeg=(yc-yBeg)/rect.h;
		yBeg=yc;
	}
	if( yEnd>yec )
	{
		// yEnd-_h is out, side length is 2*sizeY
		vEnd=1-(yEnd-yec)/rect.h;
		yEnd=yec;
	}
	
	if( xBeg>=xEnd || yBeg>=yEnd ) return;

	TLVertex pos[4];
	pos[0].rhw=1;
	pos[0].color=pars.colorTL;
	pos[0].specular=PackedColor(0xff000000);
	pos[0].pos[2]=0.5;

	pos[1].rhw=1;
	pos[1].color=pars.colorTR;
	pos[1].specular=PackedColor(0xff000000);
	pos[1].pos[2]=0.5;

	pos[2].rhw=1;
	pos[2].color=pars.colorBR;
	pos[2].specular=PackedColor(0xff000000);
	pos[2].pos[2]=0.5;

	pos[3].rhw=1;
	pos[3].color=pars.colorBL;
	pos[3].specular=PackedColor(0xff000000);
	pos[3].pos[2]=0.5;

	float uTL=pars.uTL+uBeg*(pars.uTR-pars.uTL)+vBeg*(pars.uBL-pars.uTL);
	float uTR=pars.uTL+uEnd*(pars.uTR-pars.uTL)+vBeg*(pars.uBL-pars.uTL);
	float uBL=pars.uTL+uBeg*(pars.uTR-pars.uTL)+vEnd*(pars.uBL-pars.uTL);
	float uBR=pars.uTL+uEnd*(pars.uTR-pars.uTL)+vEnd*(pars.uBL-pars.uTL);

	float vTL=pars.vTL+uBeg*(pars.vTR-pars.vTL)+vBeg*(pars.vBL-pars.vTL);
	float vTR=pars.vTL+uEnd*(pars.vTR-pars.vTL)+vBeg*(pars.vBL-pars.vTL);
	float vBL=pars.vTL+uBeg*(pars.vTR-pars.vTL)+vEnd*(pars.vBL-pars.vTL);
	float vBR=pars.vTL+uEnd*(pars.vTR-pars.vTL)+vEnd*(pars.vBL-pars.vTL);
	

	pos[0].pos[0]=xBeg,pos[0].pos[1]=yBeg;
	pos[1].pos[0]=xEnd,pos[1].pos[1]=yBeg;
	pos[2].pos[0]=xEnd,pos[2].pos[1]=yEnd;
	pos[3].pos[0]=xBeg,pos[3].pos[1]=yEnd;
	pos[0].t0.u=uTL,pos[0].t0.v=vTL;
	pos[1].t0.u=uTR,pos[1].t0.v=vTR;
	pos[2].t0.u=uBR,pos[2].t0.v=vBR;
	pos[3].t0.u=uBL,pos[3].t0.v=vBL;

	SwitchRenderMode(RM2DTris);
	SwitchTL(false);

	AddVertices(pos,4); // note: may flush all queues

	QueuePrepareTriangle(pars.mip,pars.spec);
	
	Queue2DPoly(pos,4);
}

void EngineDD::DrawLine
(
	const Line2DAbs &line,
	PackedColor c0, PackedColor c1,
	const Rect2DAbs &clip
)
{
	float x0 = line.beg.x;
	float y0 = line.beg.y;
	float x1 = line.end.x;
	float y1 = line.end.y;
	// use line texture
	Texture *tex = GPreloadedTextures.New(TextureLine);
	//Texture *tex = GPreloadedTextures.New(SkyBright);
	//Texture *tex = GPreloadedTextures.New(TextureWhite);
	//Texture *tex = NULL;
	//Texture *tex = GPreloadedTextures.New(TextureBlack);
	const MipInfo &mip = _textBank->UseMipmap(tex,1,1);
	
	// convert line to poly;
	int specFlags = NoZBuf|IsAlpha|ClampU|ClampV|IsAlphaFog;
	float dx = x1-x0;
	float dy = y1-y0;
	float dSize2 = dx*dx+dy*dy;
	float invDSize = dSize2 >0 ? InvSqrt(dSize2) : 1;

	// direction perpendicular dx, dy
	// TODO: use color alpha as width
	// 2D line drawing
	float pdx = +dy*invDSize, pdy = -dx*invDSize;
	float w = 3.0f;
	x0 -= pdx*(w*0.5);
	x1 -= pdx*(w*0.5);
	y0 -= pdy*(w*0.5);
	y1 -= pdy*(w*0.5);
	float x0Side = x0+pdx*w, y0Side = y0+pdy*w;
	float x1Side = x1+pdx*w, y1Side = y1+pdy*w;


	Vertex2DAbs vertices[4];
	float off = 0;
	//float off = 0.5f;
	vertices[0].x = x0-off;
	vertices[0].y = y0-off;
	vertices[0].u = 0;
	vertices[0].v = 0.25;
	vertices[0].color = c0;

	vertices[1].x = x0Side-off;
	vertices[1].y = y0Side-off;
	vertices[1].u = 0;
	vertices[1].v = 1;
	vertices[1].color = c0;

	vertices[3].x = x1-off;
	vertices[3].y = y1-off;
	vertices[3].u = 0.1;
	vertices[3].v = 0.25;
	vertices[3].color = c1;

	vertices[2].x = x1Side-off;
	vertices[2].y = y1Side-off;
	vertices[2].u = 0.1;
	vertices[2].v = 1;
	vertices[2].color = c1;

	DrawPoly(mip,vertices,4,clip,specFlags);
}

void EngineDD::DrawPoly
(
	const MipInfo &mip, const Vertex2DPixel *vertices, int n,
	const Rect2DPixel &clipRect, int specFlags
)
{
	if (_resetNeeded) return;

	//Texture *tex = GPreloadedTextures.New(TextureBlack);
	//const MipInfo &mip = _textBank->UseMipmap(tex,0,0);
	const int maxN=32;

	// reject poly if fully outside or invalid
	ClipFlags orClip=0;
	ClipFlags andClip=ClipAll;
	ClipFlags clipV[maxN];
	for( int i=0; i<n; i++ )
	{
		const Vertex2DPixel &vs=vertices[i];
		float x=vs.x;
		float y=vs.y;
		ClipFlags clip=0;
		if( x<clipRect.x ) clip|=ClipLeft;
		else if( x>clipRect.x+clipRect.w ) clip|=ClipRight;
		if( y<clipRect.y ) clip|=ClipTop;
		else if( y>clipRect.y+clipRect.h ) clip|=ClipBottom;
		clipV[i]=clip;
		orClip|=clip;
		andClip&=clip;
	}
	if( andClip ) return;
	Vertex2DPixel clippedVertices1[maxN]; // temporay buffer to keep clipped result
	Vertex2DPixel clippedVertices2[maxN]; // temporay buffer to keep clipped result
	// 2D clipping (with orClip flags)
	if( orClip )
	{
		Vertex2DPixel *free=clippedVertices1;
		Vertex2DPixel *used=clippedVertices2;
		// perform clipping
		for( int i=0; i<n; i++ ) used[i]=vertices[i];
		if( orClip&ClipTop ) n=Clip2D(clipRect,free,used,n,InsideTopPixel),swap(free,used);
		if( orClip&ClipBottom ) n=Clip2D(clipRect,free,used,n,InsideBottomPixel),swap(free,used);
		if( orClip&ClipLeft ) n=Clip2D(clipRect,free,used,n,InsideLeftPixel),swap(free,used);
		if( orClip&ClipRight ) n=Clip2D(clipRect,free,used,n,InsideRightPixel),swap(free,used);
		// use result
		if (n<3) return; // nothing to draw
		vertices=used;
	}
	
	if( n>maxN )
	{
		n=maxN;
		Fail("Poly: Too much vertices");
	}
	
	TLVertex gv[maxN];

	float x2d = Left2D();
	float y2d = Top2D();

	for( int i=0; i<n; i++ )
	{
		TLVertex *v=&gv[i];
		const Vertex2DPixel &vs=vertices[i];

		v->pos[0]=vs.x+x2d;
		v->pos[1]=vs.y+y2d;
		v->pos[2]=vs.z;
		v->rhw=vs.w;
		v->color=vs.color;
		v->specular=PackedColor(0xff000000);
		//v->fog=0;
		v->t0.u=vs.u;
		v->t0.v=vs.v;
		// tmu1vtx?
	}

	SwitchRenderMode(RM2DTris);
	SwitchTL(false);
	AddVertices(gv,n); // note: may flush all queues
	QueuePrepareTriangle(mip,specFlags);
	Queue2DPoly(gv,n);
}

void EngineDD::DrawPoly
(
	const MipInfo &mip, const Vertex2DAbs *vertices, int n,
	const Rect2DAbs &clipRect, int specFlags
)
{
	if (_resetNeeded) return;

	//Texture *tex = GPreloadedTextures.New(TextureBlack);
	//const MipInfo &mip = _textBank->UseMipmap(tex,0,0);
	const int maxN=32;

	// reject poly if fully outside or invalid
	ClipFlags orClip=0;
	ClipFlags andClip=ClipAll;
	ClipFlags clipV[maxN];
	for( int i=0; i<n; i++ )
	{
		const Vertex2DAbs &vs=vertices[i];
		float x=vs.x;
		float y=vs.y;
		ClipFlags clip=0;
		if( x<clipRect.x ) clip|=ClipLeft;
		else if( x>clipRect.x+clipRect.w ) clip|=ClipRight;
		if( y<clipRect.y ) clip|=ClipTop;
		else if( y>clipRect.y+clipRect.h ) clip|=ClipBottom;
		clipV[i]=clip;
		orClip|=clip;
		andClip&=clip;
	}
	if( andClip ) return;
	Vertex2DAbs clippedVertices1[maxN]; // temporay buffer to keep clipped result
	Vertex2DAbs clippedVertices2[maxN]; // temporay buffer to keep clipped result
	// 2D clipping (with orClip flags)
	if( orClip )
	{
		Vertex2DAbs *free=clippedVertices1;
		Vertex2DAbs *used=clippedVertices2;
		// perform clipping
		for( int i=0; i<n; i++ ) used[i]=vertices[i];
		if( orClip&ClipTop ) n=Clip2D(clipRect,free,used,n,InsideTopAbs),swap(free,used);
		if( orClip&ClipBottom ) n=Clip2D(clipRect,free,used,n,InsideBottomAbs),swap(free,used);
		if( orClip&ClipLeft ) n=Clip2D(clipRect,free,used,n,InsideLeftAbs),swap(free,used);
		if( orClip&ClipRight ) n=Clip2D(clipRect,free,used,n,InsideRightAbs),swap(free,used);
		// use result
		if (n<3) return; // nothing to draw
		vertices=used;
	}
	
	if( n>maxN )
	{
		n=maxN;
		Fail("Poly: Too much vertices");
	}
	
	TLVertex gv[maxN];

	for( int i=0; i<n; i++ )
	{
		TLVertex *v=&gv[i];
		const Vertex2DAbs &vs=vertices[i];

		v->pos[0]=vs.x;
		v->pos[1]=vs.y;
		v->pos[2]=vs.z;
		v->rhw=vs.w;
		v->color=vs.color;
		v->specular=PackedColor(0xff000000);
		//v->fog=0;
		v->t0.u=vs.u;
		v->t0.v=vs.v;
		// tmu1vtx?
	}

	SwitchRenderMode(RM2DTris);
	SwitchTL(false);
	AddVertices(gv,n); // note: may flush all queues
	QueuePrepareTriangle(mip,specFlags);
	Queue2DPoly(gv,n);
}

void EngineDD::DrawLine( int beg, int end )
{
	const TLVertex &v0 = _mesh->GetVertex(beg);
	const TLVertex &v1 = _mesh->GetVertex(end);

	float x0 = v0.pos.X();
	float y0 = v0.pos.Y();
	float x1 = v1.pos.X();
	float y1 = v1.pos.Y();

	float z0 = v0.pos.Z();
	float z1 = v1.pos.Z();
	float w0 = v0.rhw;
	float w1 = v1.rhw;

	// use line texture
	Texture *tex = GPreloadedTextures.New(TextureLine);
	const MipInfo &mip = _textBank->UseMipmap(tex,1,1);
	
	// convert line to poly;
	int specFlags = NoZWrite|IsAlpha|ClampU|ClampV|IsAlphaFog;
	float dx = x1-x0;
	float dy = y1-y0;
	float dSize2 = dx*dx+dy*dy;
	float invDSize = dSize2 >0 ? InvSqrt(dSize2) : 1;

	float dSize = dSize2*invDSize;

	// direction perpendicular dx, dy
	// TODO: use color alpha as width
	// 2D line drawing
	float pdx = +dy*invDSize, pdy = -dx*invDSize;
	float w = 3.0f;
	x0 -= pdx*(w*0.5);
	x1 -= pdx*(w*0.5);
	y0 -= pdy*(w*0.5);
	y1 -= pdy*(w*0.5);
	float x0Side = x0+pdx*w, y0Side = y0+pdy*w;
	float x1Side = x1+pdx*w, y1Side = y1+pdy*w;

	Vertex2DAbs vertices[4];
	float off = 0.0f;
	//float off = 0.5f;
	vertices[0].x = x0-off;
	vertices[0].y = y0-off;
	vertices[0].z = z0;
	vertices[0].w = w0;
	vertices[0].u = 0;
	vertices[0].v = 0.25;
	vertices[0].color = v0.color;

	vertices[1].x = x0Side-off;
	vertices[1].y = y0Side-off;
	vertices[1].z = z0;
	vertices[1].w = w0;
	vertices[1].u = 0;
	vertices[1].v = 1;
	vertices[1].color = v0.color;

	vertices[2].x = x1Side-off;
	vertices[2].y = y1Side-off;
	vertices[2].z = z1;
	vertices[2].w = w1;
	vertices[2].u = dSize;
	vertices[2].v = 1;
	vertices[2].color = v1.color;

	vertices[3].x = x1-off;
	vertices[3].y = y1-off;
	vertices[3].z = z1;
	vertices[3].w = w1;
	vertices[3].u = dSize;
	vertices[3].v = 0.25;
	vertices[3].color = v1.color;

	Rect2DAbs clip(0,0,_w,_h);

	DrawPoly(mip,vertices,4,clip,specFlags);
}

void EngineDD::CreateVB()
{
	ReportGRAM("Before CreateVertexBuffer");
	HRESULT err=_d3DDevice->CreateVertexBuffer
	(
		MeshBufferLength*sizeof(TLVertex),
		NoClipUsageFlag|D3DUSAGE_DYNAMIC|D3DUSAGE_WRITEONLY,
		MYFVF,
		D3DPOOL_DEFAULT,
		_queueNo._meshBuffer.Init()
	);
	if( err!=D3D_OK || !_queueNo._meshBuffer)
	{
		DDError("Cannot create vertex buffer.",err);
    ErrorMessage("CreateVertexBuffer (dynamic) failed %x - out of memory?",err);
		return;
	}
	err = _d3DDevice->CreateIndexBuffer
	(
		IndexBufferLength*sizeof(VertexIndex),
		D3DUSAGE_WRITEONLY|D3DUSAGE_DYNAMIC|NoClipUsageFlag,
		D3DFMT_INDEX16,
		D3DPOOL_DEFAULT,
		_queueNo._indexBuffer.Init()
	);
	if (err!=D3D_OK || !_queueNo._indexBuffer)
	{
		DDError("CreateIndexBuffer",err);
    ErrorMessage("CreateIndexBuffer (dynamic) failed %x - out of memory?",err);
		return;
	}

	ReportGRAM("After CreateVertexBuffer");
	_queueNo._vertexBufferUsed = 0;
	_queueNo._indexBufferUsed = 0;
}	

void EngineDD::DestroyVB()
{
	_queueNo._meshBuffer.Free();
	_queueNo._indexBuffer.Free();
	_lastQueueSource = NULL;
}

void EngineDD::CreateVBTL()
{
	//HRESULT err;
	_dynVBuffer.Init(16*1024,_d3DDevice);
}	

void EngineDD::DestroyVBTL()
{
	//_queueTL._meshBuffer.Free();
	//_queueTL._indexBuffer.Free();
	_dynVBuffer.Dealloc();
	_statVBuffer.Clear();
}

void EngineDD::D3DConstruct()
{
	_d3dFrameOpen=false;

	static StaticStorage<WORD> TriangleQueueStorageNo[MaxTriQueues];
	for (int i=0; i<MaxTriQueues; i++)
	{
		TriQueue &triqNo = _queueNo._tri[i];
		triqNo._triangleQueue.SetStorage(TriangleQueueStorageNo[i].Init(TriQueueSize));
	}

	CreateVB();
	CreateVBTL();

	CreateTextBank();
}

void EngineDD::D3DDestruct()
{
	_fonts.Clear();
	if( _textBank ) delete _textBank,_textBank=NULL;

	#if PIXEL_SHADERS
	DeinitPixelShaders();
	#endif
	#if VERTEX_SHADERS
	DeinitVertexShaders();
	#endif
	DestroyVBTL();
	DestroyVB();
}

void EngineDD::Restore()
{
	_renderState.Clear();
	int i;
	for( i=0; i<MaxStages; i++ )
	{
		_textureStageState[i].Clear();
	}
}

bool EngineDD::CanRestore()
{
	return false;
	//return _frontBuffer->IsLost()==D3D_OK;
}

#if _ENABLE_CHEATS

#include <Es/Memory/normalNew.hpp>
#include <d3dx8tex.h>
#include <Es/Memory/debugNew.hpp>
#pragma comment(lib,"d3dx8.lib")
void EngineDD::Screenshot(RString filename)
{
	HRESULT result = D3DXSaveSurfaceToFile(filename, D3DXIFF_BMP, _backBuffer, NULL, NULL);
	if (result != D3D_OK) DDError("Screenshot failed", result);
}

#else

void EngineDD::Screenshot(RString filename)
{
}

#endif

#include "engineDll.hpp"

Engine *CreateEngineD3D( CreateEnginePars &pars )
{
	//DestroyEngine();
	//InitEngine(hInst,hPrev,sw,w,h);

	if( pars.UseWindow )
	{
		pars.NoRedrawWindow = false;
		pars.HideCursor = false;
		return new EngineDD
		(
			pars.hInst,pars.hPrev,pars.sw,
			pars.w,pars.h,true,pars.bpp
		);
	}
	else
	{
		pars.NoRedrawWindow=true;
		#ifndef _XBOX
			pars.HideCursor=true;
			ShowCursor(FALSE);
		#else
			pars.HideCursor=false;
		#endif
		return new EngineDD
		(
			pars.hInst,pars.hPrev,pars.sw,
			pars.w,pars.h,false,pars.bpp
		);
	}
}

//template Ref<VertexStaticData>;
#endif