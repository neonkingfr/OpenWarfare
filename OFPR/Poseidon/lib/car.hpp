#ifdef _MSC_VER
#pragma once
#endif

#ifndef _CAR_HPP
#define _CAR_HPP

#include "tankOrCar.hpp"

#include "transport.hpp"
#include "shots.hpp"

#include "rtAnimation.hpp"

enum CarWheel
{
	FrontWheels,
	FLWheel=FrontWheels,FRWheel,
	FL2Wheel,FR2Wheel,
	BackWheels,
	MLWheel=BackWheels,MRWheel,
	BLWheel,BRWheel,
	MaxWheels,
	NoWheel=MaxWheels
};

#include "wounds.hpp"

struct ScudProxy
{
	LLink<Object> obj;
	int selection; // copied over from proxyObject

	ScudProxy() {selection = -1;}
	bool IsPresent() const {return selection >= 0;}
};

class CarType: public TankOrCarType
{
	typedef TankOrCarType base;
	friend class Car;

	protected:
	
	Matrix4 _toWheelAxis; // transformation
	AnimationWithCenter _drivingWheel;

	Vector3 _steeringPoint;
	float _wheelCircumference;
	float _turnCoef;
	float _terrainCoef;

	float _damperSize;
	float _damperForce;

	AnimationAnimatedTexture _animFire;
	TurretType _turret;
	bool _hasTurret;

	AnimationWithCenter _frontLeftWheel,_frontRightWheel;
	AnimationWithCenter _front2LeftWheel,_front2RightWheel;
	AnimationWithCenter _midLeftWheel,_midRightWheel;
	AnimationWithCenter _backLeftWheel,_backRightWheel;
	Buffer<CarWheel> _whichWheelContact; // conversion from landcontact
	Buffer<CarWheel> _whichWheelGeometry; // conversion from landcontact

	WoundTextureSelections _glassDammageHalf;
	WoundTextureSelections _glassDammageFull;

	AnimationWithCenter *_wheels[MaxWheels];

	PlateInfos _plateInfos;

	HitPoint _glassRHit;
	HitPoint _glassLHit;
	HitPoint _bodyHit;
	HitPoint _fuelHit;

	HitPoint _wheelLFHit;
	HitPoint _wheelRFHit;

	HitPoint _wheelLF2Hit;
	HitPoint _wheelRF2Hit;

	HitPoint _wheelLMHit;
	HitPoint _wheelRMHit;

	HitPoint _wheelLBHit;
	HitPoint _wheelRBHit;

	Ref<WeightInfo> _weights; // rt animation weighting info	
	Ref<Skeleton> _skeleton;

	ScudProxy _proxies[MAX_LOD_LEVELS];

	Ref<AnimationRT> _scudLaunch;
	Ref<AnimationRT> _scudStart;

	SoundPars _scudSound;
	SoundPars _scudSoundElevate;

	Ref<LODShapeWithShadow> _scudModel;
	Ref<LODShapeWithShadow> _scudModelFire;

	void ScanWheels(Buffer<CarWheel> &buffer, int contactLevel);

	public:
	CarType( const ParamEntry *param );
	virtual void Load(const ParamEntry &par);
	void InitShape();
};

class Car: public TankOrCar
{
	typedef TankOrCar base;

	// basic catterpillar vehicle (car/APC) simulation
	protected:
	Link<AbstractWave> _hornSound;
	Ref<AbstractWave> _scudSound;

	WeaponCloudsSource _scudSmoke;
	Vector3 _scudPos;
	Vector3 _scudSpeed;

	Turret _turret;

	WeaponLightSource _mGunFire;
	int _mGunFireFrames;
	UITime _mGunFireTime;
	int _mGunFirePhase;
	WeaponCloudsSource _mGunClouds;

	RString _plateNumber;
	
	float _wheelPhase;
	float _dampers[MaxWheels];
	
	float _reverseTimeLeft; // force reverse state
	float _forwardTimeLeft; // force forward state

	TrackOptimizedFour _track;
	
	// simulate gear box
	
	// thrust is relative, in range (-1.0,1.0)
	// actually it is engine RPM?
	float _thrustWanted,_thrust; // accelerate 

	// assume steering point the middle of front whee axe (see CarType)
	// _turn specifies side offset of steering point
	// when car moves 1 m forward - and therefore it can be viewed
	// as tangens of angle of front wheels
	// each time angular velocity change is calculated,
	// actual radial velocity of

	float _turnWanted,_turn; // turn

	float _turnIncreaseSpeed,_turnDecreaseSpeed; // keyboard different from stick/auto
	

	float _scudState;
	// 0..1 = init
	// 1..2 = launching
	// 2..3 = ready
	// 3..4 = starting 

	public:
	Car( VehicleType *name, Person *driver );

	const CarType *Type() const
	{
		return static_cast<const CarType *>(GetType());
	}

	Vector3 Friction( Vector3Par speed );

	float Rigid() const {return 0.3;} // how much energy is transfered in collision
	bool HasTurret() const {return Type()->_hasTurret;}
	bool IsBlocked() const;

	float GetScudState() const {return _scudState;}

	void MoveWeapons(float deltaT);
	void Simulate( float deltaT, SimulationImportance prec );
	Matrix4 InsideCamera( CameraType camType ) const;
	Vector3 ExternalCameraPosition( CameraType camType ) const;
	int InsideLOD( CameraType camType ) const;
	bool IsGunner( CameraType camType ) const;
	bool IsContinuous( CameraType camType ) const;
	bool HasFlares( CameraType camType ) const;
	void SimulateHUD(CameraType camType,float deltaT);
	void Draw( int level, ClipFlags clipFlags, const FrameBase &pos );
	void DrawProxies
	(
		int level, ClipFlags clipFlags,
		const Matrix4 &transform, const Matrix4 &invTransform,
		float dist2, float z2, const LightList &lights
	);
	void AnimateMatrix(Matrix4 &mat, int level, int selection) const;

	RString GetActionName(const UIAction &action);
	void PerformAction(const UIAction &action, AIUnit *unit);
	void GetActions(UIActions &actions, AIUnit *unit, bool now);

	Matrix4 TurretTransform() const;
	Matrix4 GunTurretTransform() const;

	//float OutsideCameraDistance( CameraType camType ) const {return 8.3;}
	float OutsideCameraDistance( CameraType camType ) const {return 20;}

	float GetGlassBroken() const;

	void DammageAnimation( int level );
	void DammageDeanimation( int level );

	bool IsAnimated( int level ) const; // appearence changed with Animate
	bool IsAnimatedShadow( int level ) const; // shadow changed with Animate
	void Animate( int level );
	void Deanimate( int level );

	Vector3 AnimatePoint( int level, int index ) const;

	RString GetPlateNumber() const {return _plateNumber;}
	void SetPlateNumber( RString plate ) {_plateNumber=plate;}

	void Sound( bool inside, float deltaT );
	void UnloadSound();

	bool IsAbleToMove() const;
	bool IsPossibleToGetIn() const;
	bool IsCautious() const;

	float GetEngineVol( float &freq ) const;
	float GetEnvironVol( float &freq ) const;

	float Thrust() const {return fabs(_thrust);}
	float ThrustWanted() const {return fabs(_thrustWanted);}

	float TrackingSpeed() const {return 80;}

	void PerformFF(FFEffects &eff);
	void Eject(AIUnit *unit);
	void JoystickPilot( float deltaT );
	void SuspendedPilot(AIUnit *unit, float deltaT );
	void KeyboardPilot(AIUnit *unit, float deltaT );
	void FakePilot( float deltaT );
#if _ENABLE_AI
	void AIPilot(AIUnit *unit, float deltaT );
#endif

	RString DiagText() const;

	float GetPathCost( const GeographyInfo &info, float dist ) const;
	void FillPathCost( Path &path ) const;

	float GetFieldCost( const GeographyInfo &info ) const;
	float GetCost( const GeographyInfo &info ) const;
	float GetCostTurn( int difDir ) const;

	bool FireWeapon( int weapon, TargetType *target );
	void FireWeaponEffects(int weapon, const Magazine *magazine,EntityAI *target);

	Vector3 GetCameraDirection( CameraType camType ) const;
	void LimitCursor( CameraType camType, Vector3 &dir ) const;

	// horn is always aimed
	bool AimWeapon( int weapon, Vector3Par direction );
	bool AimWeapon( int weapon, Target *target );
	Vector3 GetWeaponDirection( int weapon ) const;
	Vector3 GetWeaponCenter( int weapon ) const;
	Vector3 GetWeaponDirectionWanted( int weapon ) const;

	virtual LSError Serialize(ParamArchive &ar);
	
	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx);
};

#endif

