#include "wpch.hpp"

#if !_DISABLE_GUI && !defined _XBOX

#include <El/QStream/qbstream.hpp>
#include "fileServer.hpp"
#include "global.hpp"
#include "txtGlide.hpp"
#include "engGlide.hpp"
#include "progress.hpp"
#include "scene.hpp" // TODO: no dependency on scene
#include <El/common/perfLog.hpp>
#include <Es/Common/filenames.hpp>

// pure full saturated magenta and cyan is used for transparency

#define MIN_MIP_SIZE 4

#define MAX_PERMANENT_SIZE 16*16 // non-discardable textures

// inline functions that can not be implemented in class declaration

inline bool MipmapLevelGlideInfo::Permanent() const
{
	return( TotalSize()<=MAX_PERMANENT_SIZE*4 );
}

// different class functions

// Glide requires QWORD (8B) alignement
#define GRAM_ALIGN 8

// system heap are QWORD aligned
#define SYS_ALIGN 8

// bigger alignmenent means less fragmentation, but worse memory usage
// we do this trick: we load all texture fragments lesser than 64x32 first
// and make them resident
// all bigger fragments use allocation units at least least 2*64*32 = 4096 B

DEFINE_FAST_ALLOCATOR(GRAMItem);

void GRAMHeap::Init( GRAMOffset begin, GRAMOffset end )
{
	Heap<GRAMOffset,int>::Init(begin,end-begin,GRAM_ALIGN);
}

GRAMHeap::~GRAMHeap()
{
}

// Video RAM heap (_vram)
// there is list of textures loaded during this frame construction as well as
// last frame textures
// these are never discarded
// other textures create LRU list

// system heap (_heap)
// small textures (w*h<=MAX_PERMANENT_SIZE) are preloaded and marked as permanent
// other textures are loaded as needed

int TextureGlide::LoadPart(int level, bool noFree)
{
	TextBankGlide *bank=static_cast<TextBankGlide *>(GLOB_ENGINE->TextBank());
	MemoryItem *mem;
	MipmapLevelGlideInfo *mip=&_mipmapsInfo[level];
	{
		if( mip->Permanent() ) CacheUse(&bank->_permanentUsed,level);
		else CacheUse(&bank->_lastUsed,level);
		
		// it should not be discarded until it is copied to GRAM
		_usedCount++;
		int size=mip->TotalSize();
		if( !noFree ) bank->ReserveMemory(size);
		mem=bank->_heap.Alloc(size);
	}
	bool ret=true;
	if (mem && _src)
	{
		byte *data=mem->Memory();
		for( int i=level; i<_nMipmaps; i++ )
		{
			PacLevelMem *loadMip=&_mipmaps[i];
			//LogF("Load level %s (%dx%d)",(const char *)Name(),loadMip->_w,loadMip->_h);
			//loadMip->_memData=NULL; // same mipmap can be loaded twice
			// load paa or pac format
			ret = _src->GetMipmapData(data,*loadMip,i);
			Assert( ret );
			if( !ret )
			{
				Log("Texture load failed: %s (%dx%d)",Name(),loadMip->_w,loadMip->_h);
				break;
			}
			//Assert( mip.VerifyChecksum() );
			int size=loadMip->Size();
			data+=size;
			ADD_COUNTER(tHeap,_mipmapsInfo[i].TotalSize());
		}
		//Log("Loading %s,%d (ret=%d)",Name(),part->_minLevel,ret);
	}
	else
	{
		//Fail("Part load error.");
		ret=-1;
		//SkipPart(in,level);
	}

	if( _memory ) bank->_heap.Free(_memory),_memory=NULL,_memLevel=-1;
	if (ret)
	{
		_memory=mem;
		_memLevel=level;
		mip->_info.data=mem->Memory();
	}
	else
	{
		LogF("Part not loaded %s, %d",Name(),level);
		//Fail("Part not loaded.");
		_cache->Delete(),delete _cache,_cache=NULL;
		if( mem ) bank->_heap.Free(mem);
		Assert( _memory==NULL );
		//Assert( mip->_info.data==NULL );
	}
	_usedCount--;
	return ret ? 0 : -1;
}

/*!
\patch_internal 1.03 Date 7/12/2001 by Ondra.
- Fixed Glide sky bug introduced in 1.02
*/


int TextureGlide::Init( RStringB name )
{
	//TextBankGlide *bank=static_cast<TextBankGlide *>(GLOB_ENGINE->TextBank());

	SetName(name);
	
	int i=-1;

	ITextureSourceFactory *factory = SelectTextureSourceFactory(Name());
	if (factory) _src = factory->Create(Name(),_mipmaps,MAX_MIPMAPS);
	if (_src)
	{

		//in.seekg(0,QIOS::beg);
		// .paa should start with format marker

		if( !CmpStartStr(Name(),"fonts\\") ) _maxSize=256;
		else _maxSize=Glob.config.maxObjText;
		if( AbstractTextBank::AnimatedNumber(Name())>=0 && IsAlpha() ) _maxSize=Glob.config.maxAnimText;
		// Glide (Voodoo2,3) is not able to use textures larger than 256

		saturateMin(_maxSize,256);

		PacFormat format = _src->GetFormat();
		// fix
		if (format==PacP8)
		{
			if (!IsTransparent()) format=PacRGB565;
			else format=PacARGB1555;
		}
		else if
		(
			format==PacARGB4444 ||
			format==PacAI88 ||
			format==PacARGB8888
		)
		{
			_src->ForceAlpha();
		}

		int maxMips = _src->GetMipmapCount();
		for( i=0; i<maxMips; i++ )
		{
			PacLevelMem &mip=_mipmaps[i];
			mip.SetDestFormat(format,1);
			// do not load too small mip-maps
			if( mip._w<MIN_MIP_SIZE ) break;
			if( mip._h<MIN_MIP_SIZE ) break;
		}

		_nMipmaps=i;
		//RptF("Texture %s: %d mipmaps",(const char *)name,_nMipmaps);
		SetupInfo();

		return 0;
	}
	
	_nMipmaps=0;
	WarningMessage("Cannot load texture %s.",(const char *)name);
	return -1;
}

int TextureGlide::Preload( int level )
{
	// preload single level
	// without parameter level it was:
	// try to preload coarse mip-maps of all textures
	// do not care for any errors - preloading only
	//Log("Preload %s,%d",Name(),level);
	if (!_src) return -1;

	int i;
	// preload to system memory
	for( i=0; i<_nMipmaps; i++ )
	{
		int ret=0;
		PacLevelMem *mip=&_mipmaps[i];
		// load whole part at once
		if( mip->TooLarge(_maxSize) || (level&(1<<i))==0 ) {} //ret=mip->Skip(in);
		else
		{
			//DebugLog("Preload %s (%dx%d)",Name(),mip->_w,mip->_h);
			int ret=LoadPart(i,true); // contains all smaller mipmaps
			if( ret ) return ret;
			return 0;
		}
		if( ret ) return ret;
	}
	return 0;
}

int TextureGlide::PreloadGRAM( int level )
{
	TextBankGlide *bank=static_cast<TextBankGlide *>(GLOB_ENGINE->TextBank());
	// preload to GRAM
	#if 1
	int i;
	if( _memLevel<0 ) return 0; // nothing to preload from
	//PacLevelMem *mipMemory=&_mipmaps[_memLevel];
	for( i=0; i<_nMipmaps; i++ )
	{
		PacLevelMem *mip=&_mipmaps[i];
		MipmapLevelGlideInfo *mipInfo=&_mipmapsInfo[i];
		if( mip->TooLarge(_maxSize) ) continue;
		if( _gramLevel>=0 && _gramLevel<=level ) continue; // already loaded
		if( (level&(1<<i)) )
		{
			ReleaseGRAM();
			//DebugLog("Preload GRAM %s (%dx%d)",Name(),mip->_w,mip->_h);
			int size=mipInfo->TotalSize();
			int tmu=SelectTMU();
			Assert( tmu>=0 && tmu<256 );
			TextBankGlide::TMUCache &tmuCache=bank->_tmu[tmu];
			_isGRAM=tmuCache._vram.Alloc(size);
			if( !_isGRAM ) return -1;
			_tmu=tmu;
			_gramLevel=i;
			static_cast<EngineGlide *>(GEngine)->CopyMipmap(this,_gramLevel,_memLevel);
			GRAMUse(&tmuCache._previousGRAM,_gramLevel);
			break; // contains also all smaller mip-maps
		}
	}
	#endif
	return 0;
}

static bool Verbose = false;

int TextureGlide::Load( int minLevel, int maxLevel )
{
	TextBankGlide *bank=static_cast<TextBankGlide *>(GLOB_ENGINE->TextBank());

	// if we can load any mip-map, we can continue
	
	// Glide specific: due to per-pixel mip-mapping
	// we always load all mipmaps coarser than fromLevel

	// load everything from minLevel to _nMimpmaps-1
	bool someMip=false;
	bool someLoad=false;
	//Glob.diskAccessLock.Lock(); // serialize disk access
	//Log("Texture: Disk lock %s (%d..%d)",Name(),minLevel,maxLevel);
	
	if( _interpolate )
	{
		// load interpolation target
		if( _interpolate->_memLevel<0 || _interpolate->_memLevel>minLevel )
		{
			// load is necessary
			int ok = _interpolate->Load(minLevel,maxLevel);
			if (ok<0) goto Error;
			Assert( _interpolate->_memLevel>=0 );
		}
		_interpolate->_usedCount++;
	}

	if (!_src) goto Error;

	int i;
	// first release old data
	ReleaseMemory();
	// then load new data
	for( i=0; i<_nMipmaps; i++ )
	{
		int ret=0;
		PacLevelMem *mip=&_mipmaps[i];
		// load only selected mipmap
		if( mip->TooLarge(_maxSize) || i<minLevel )
		{
			//ret=mip->Skip(in);
		}
		else
		{
			ret=LoadPart(i),someLoad=true;
			if( ret ) goto Error;
			if( _interpolate )
			{
				Assert( _interpolate->_nMipmaps==_nMipmaps );
				if( _interpolate->_nMipmaps==_nMipmaps )
				{
					byte *data = _memory->Memory();
					byte *iData = _interpolate->_memory->Memory();
					Assert (i== _memLevel);

					// skip any superfluous data in iData
					for (int ii=_interpolate->_memLevel; ii<i; i++)
					{
						iData += _mipmapsInfo[ii]._size;
					}
					// 
					for( int l=i; l<_nMipmaps; l++ )
					{
						// perform interpolation between this and interpol using _iFactor
						// check where is it in memory
						_mipmaps[l].Interpolate
						(
							data,iData,
							_interpolate->_mipmaps[l],_iFactor
						);
						data += _mipmapsInfo[l]._size;
						iData += _mipmapsInfo[l]._size;
					}
				}
			}
			someMip=true;
			break;
		}
	}

	if( someLoad ) bank->_fromDiskToHeap++;
	if( _interpolate ) _interpolate->_usedCount--;
	if( !someMip ) return -1;
	return 0;

	Error:
	RptF("Texture load error (%s)",(const char *)Name());
	if( _interpolate ) _interpolate->_usedCount--;
	if( !someMip ) return -1;
	return 0;
}

bool TextureGlide::VerifyChecksum( const MipInfo &absMip ) const
{
	return true;
}

DEFINE_FAST_ALLOCATOR(TextureGlide);

TextureGlide::TextureGlide()
:_nMipmaps(0),
_isDetail(false),_useDetail(false),
_maxSize(256),
_isGRAM(NULL),_memory(NULL),
_gramLevel(-1),_memLevel(-1),
_cache(NULL),_cacheGRAM(NULL),_cacheGRAMRoot(NULL),
_gramUsedCount(0),_usedCount(0),
_tmu(-1),
_interpolate(NULL)
{
}

TextureGlide::~TextureGlide()
{
	//Log("Destruct texture %s",(const char *)Name());
	ReleaseGRAM();
	ReleaseMemory();
	_nMipmaps=0;
	EngineGlide *engine=static_cast<EngineGlide *>(GLOB_ENGINE);
	engine->TextureDestroyed(this);
}

int TextBankGlide::NTMUs() const
{
	return _engine->NTMUs();
}

// constructor

void TextBankGlide::InitGRAM()
{
	#define LIMIT_MAX_ADDR ( 1024*1024*64 )
	#define NO_CROSS ( 2*1024*1024 )
	#define LIMIT_MIN_ADDR ( 0  )

	int nTMUs=NTMUs();
	for( int tmu=0; tmu<nTMUs; tmu++ )
	{
		TMUCache &tmuCache=_tmu[tmu];
		int min=_engine->TexMinAddress(tmu);
		int max=_engine->TexMaxAddress(tmu);
		Log("Tex mem %d: %d...%d",tmu,min,max);
		if( max>LIMIT_MAX_ADDR ) max=LIMIT_MAX_ADDR;
		if( min<LIMIT_MIN_ADDR ) min=LIMIT_MIN_ADDR;
		tmuCache._vram.Init(min,max);
		int low=min,high=max;
		while( high>low )
		{
			int boundary=high/NO_CROSS*NO_CROSS;
			if( boundary<=low ) break;
			int crossSize=256;
			if( crossSize>high-boundary ) crossSize=high-boundary;
			Assert( high>=boundary+crossSize );
			GRAMItem *cross=tmuCache._vram.Alloc(boundary,crossSize); // allocate something on the boundary
			Log("Disable boundary on %d",cross->Memory());
			Assert( cross );
			Assert( cross->Memory()==boundary );
			high-=NO_CROSS;
		}
	}
	
	_fromHeapToGRAM=0; // init stats
	_fromDiskToHeap=0;
}

TextBankGlide::TextBankGlide( EngineGlide *engine)
{
	_engine=engine;

	_selectTMU=0;
	
	_heap.Init(GLIDE_HEAP_SIZE);

	InitGRAM();
	//_heap.Init(256*1024); // caution: stress memory
	//_vram.Init(0,256*1024);
}

void TextBankGlide::RecreateGRAM()
{
	int nTMUs=NTMUs();
	for( int tmu=0; tmu<nTMUs; tmu++ )
	{
		ReserveGRAM(tmu,INT_MAX,true); // free all items allocated to GRAM
	}
	/*
	// GRAM size has been changed (e.g. resolution change)
	{
		TMUCache &tmuCache=_tmu[tmu];
		tmuCache._vram.Init(_engine->TexMinAddress(tmu),_engine->TexMaxAddress(tmu)); // reinit allocation info
	}
	*/
}

void TextBankGlide::HeapMoved( int offset )
{
	// relocate whole heap
	// that will move all pointers to texture or palette data
	_heap.Move(offset);
}


TextBankGlide::~TextBankGlide()
{
	UnlockAllTextures();
	DeleteAllAnimated();
	#if 0 //!_RELEASE
		// TODO: load/free fonts when necessary
		_texture.Compact();
		Assert( _texture.Size()==0 );
	#endif
	_texture.Clear();
	_detail.Free();
	_specular.Free();
	Log("TextBankGlide::~TextBankGlide done");
}

void TextBankGlide::StopAll()
{
	//_loader->DeleteAll();
}


int TextBankGlide::FindFree()
{
	// check for texture limits
	_texture.Compact();
	//Assert( _texture.Size()<MAX_BANKTEXTURES );
	return _texture.Add();
}

int TextBankGlide::Find( RStringB name1, TextureGlide *interpolate )
{
	int i;
	for( i=0; i<_texture.Size(); i++ )
	{
		TextureGlide *texture=_texture[i];
		if( !texture ) continue;
		if( texture->GetName()!=name1 ) continue;
		if( texture->_interpolate!=interpolate ) continue;
		return i;
	}
	return -1;
}

void TextureGlide::InitLoaded()
{
}
	

Ref<Texture> TextBankGlide::Load( RStringB name )
{
	//return NULL;

	// assert name is low case
	#if !_RELEASE
		char lowName[128];
		strcpy(lowName,name);
		strlwr(lowName);
		Assert( !strcmp(name,lowName) );
	#endif

	// check if we have file to load from
	if (!QIFStreamB::FileExist(name))
	{
		LogF("Cannot load texture %s",(const char *)name);
		return NULL;
	}

	int index=Find(name);
	if( index>=0 ) return _texture[index];

	int iFree=FindFree();

	Ref<TextureGlide> texture=new TextureGlide;
	
	if( texture->Init(name) )
	{
		//texture->Init("data\\default.pac");
		//Fail("No texture");
		return NULL;
	}
	texture->InitLoaded();
	_texture[iFree]=texture.GetRef();
	return texture;

}

void TextureGlide::SetMultitexturing( int type )
{
	if( type==1 )
	{
		_useDetail=true;
	}
}


TextureGlide *TextBankGlide::Copy( int from )
{
	Assert( from>=0 );
	const TextureGlide *source=_texture[from];
	RStringB sName=source->GetName();

	int iFree=FindFree();
	Assert( iFree>=0 );
	TextureGlide *texture=new TextureGlide;
	//Log("Copy texture %s",(const char *)sName);
	
	if( texture->Init(sName) )
	{
		Fail("No texture file");
		return NULL;
		//texture->Init("data\\default.pac");
	}
	texture->InitLoaded();
	_texture[iFree]=texture;
	return texture;	
}

Ref<Texture> TextBankGlide::LoadInterpolated
(
	RStringB n1, RStringB n2, float factor
)
{
	#if 0

	if( factor>=0.5 ) return Load(n2);
	else return Load(n1);

	#else
	// to do: real interpolation

	const float eps=1.0/64;
	if( factor>=1.0-eps ) return Load(n2);
	if( factor<=eps ) return Load(n1);
	// create name of combination

	#if !_RELEASE
		char lowName1[80],lowName2[80];
		strcpy(lowName1,n1),strlwr(lowName1);
		strcpy(lowName2,n2),strlwr(lowName2);
		Assert( !strcmp(n1,lowName1) );
		Assert( !strcmp(n2,lowName2) );
	#endif

	Ref<Texture> base=Load(n2);
	Ref<TextureGlide> interpolate=static_cast<TextureGlide *>(base.GetRef());
	int index=Find(n1,interpolate);
	if( index>=0 )
	{
		TextureGlide *t=_texture[index];
		const float iPolEps=1.0/32;
		if( fabs(t->_iFactor-factor)>iPolEps )
		{
			//Log("Reinterpolate %s %s (%f)",lowName1,lowName2,factor);
			// force reinterpolation
			t->ReleaseMemory();
			t->ReleaseGRAM();
			t->_iFactor=factor;
		}
		return t;
	}
	// load both components
	Ref<Texture> temp=Load(n1);
	int index1=Find(n1);
	// search both components
	// create an interpolated texture
	TextureGlide *t=Copy(index1);
	t->_interpolate=interpolate;
	t->_iFactor=factor; // start as clear texture
	return t;
	#endif
}

void TextureGlide::SetMipmapRange( int min, int max )
{
	if( min<0 ) min=0;
	if( max>_nMipmaps-1 ) max=_nMipmaps-1;
	if( min>max ) min=max;
	int minW=_mipmaps[min]._w;
	int minH=_mipmaps[min]._h;
	_maxSize=( minW<minH ? minH : minW );
	// set top limit
	_nMipmaps=max+1;
	SetupInfo();
}

void TextureGlide::SetMaxSize( int size )
{
	_maxSize=( size<=256 ? size : 256 );
	SetupInfo();
}

// maintain texture cache

DEFINE_FAST_ALLOCATOR(HMipCacheGlide);

void TextBankGlide::ReserveMemory( int size, HMipCacheRoot *root )
{
	// reserve memory, can release textures from given list
	HMipCacheGlide *last=root->Last();
	while( _heap.MaxFreeLeft()<size )
	{
		if( !last ) break; // nothing to release

		// release mip-map data
		TextureGlide *texture=last->texture;
		if( texture->_usedCount>0 )
		{
			last=root->Prev(last);
			continue;
		}

		// release texture data from texture heap
		//DebugLog("Relase sys %s (%dx%d)",texture->Name(),mip->_w,mip->_h);
		Assert( texture->_memory );
		Assert( texture->_cache==last );
		texture->ReleaseMemory(); // also deletes from statistics
		last=root->Last();
	}
}

void TextBankGlide::ReserveMemory( int size )
{
	// release as many mip-map data as neccessary to get required memory
	// get last mip-map used
	//ScopeLock<TextureLoader> lock(*_loader);
	ReserveMemory(size,&_lastUsed);
	ReserveMemory(size,&_permanentUsed);
}

bool TextBankGlide::ReserveGRAM( int tmu, int size, HMipCacheRoot *root )
{
	TMUCache &tmuCache=_tmu[tmu];
	// release as many mip-map data as neccessary to get required memory
	HMipCacheGlide *last=root->Last();
	while( tmuCache._vram.MaxFreeLeft()<size )
	{
		// get last mip-map used

		if( !last ) return false; // nothing to release
		
		// release mip-map data
		TextureGlide *texture=last->texture;
		// search for currently allocated mipmap
		Assert( texture->_isGRAM );
		Assert( texture->_cacheGRAM );
		Assert( texture->_cacheGRAMRoot );

		if( texture->_gramUsedCount>0 )
		{
			last=root->Prev(last);
			continue;
		}

		// release texture data from GRAM
		//DebugLog("Release GRAM %s (%dx%d)",texture->Name(),mip->_w,mip->_h);
		Assert( texture->_tmu==tmu );
		Assert( texture->_cacheGRAM==last );
		//Assert( texture->_cacheGRAMRoot==root );
		#if !_RELEASE
		if( texture->_cacheGRAMRoot!=root )
		{
			if( texture->_cacheGRAMRoot==&tmuCache._thisFrameGRAM )
			{
				Log("texture->_cacheGRAMRoot is _thisFrameGRAM");
			}
			if( texture->_cacheGRAMRoot==&tmuCache._lastFrameGRAM )
			{
				Log("texture->_cacheGRAMRoot is _lastFrameGRAM");
			}
			if( texture->_cacheGRAMRoot==&tmuCache._previousGRAM )
			{
				Log("texture->_cacheGRAMRoot is _previousGRAM");
			}
			if( root==&tmuCache._thisFrameGRAM )
			{
				Log("root is _thisFrameGRAM");
			}
			if( root==&tmuCache._lastFrameGRAM )
			{
				Log("root is _lastFrameGRAM");
			}
			if( root==&tmuCache._previousGRAM )
			{
				Log("root is _previousGRAM");
			}
			Fail("Invalid root");
			// check if it is really in
		}
		#endif

		
		texture->ReleaseGRAM();

		last=root->Last();
	}
	return true;
}

bool TextBankGlide::ReserveGRAM( int tmu, int size, bool needed )
{
	//ScopeLock<TextureLoader> lock(*_loader);
	TMUCache &tmuCache=_tmu[tmu];
	if( ReserveGRAM(tmu,size,&tmuCache._previousGRAM) ) return true;
	if( needed || size>=INT_MAX )
	{
		// if not discarding all keep last frame textures
		Log("Reserve needed");
		if( ReserveGRAM(tmu,size,&tmuCache._lastFrameGRAM) ) return true;
		if( ReserveGRAM(tmu,size,&tmuCache._thisFrameGRAM) ) return true;
	}
	return false;
}

int TextureGlide::SelectTMU() const
{
	TextBankGlide *bank=static_cast<TextBankGlide *>(GLOB_ENGINE->TextBank());
	if( bank->NTMUs()<=1 ) return 0;
	if( _isDetail ) return 1;
	else if( _useDetail ) return 0;
	return bank->SelectTMU();
}

int TextBankGlide::SelectTMU() const
{
	// find the TMU with most free memory
	// cycle through TMUs - just in case both are full
	int nTMUs=NTMUs();
	if( nTMUs<=1 ) return 0;
	int select=_selectTMU+1;
	if( select>=nTMUs ) select=0;
	int maxFree=0;
	for( int tmu=0; tmu<nTMUs; tmu++ )
	{
		const TMUCache &tmuCache=_tmu[tmu];
		int free=tmuCache._vram.MaxFreeLeft();
		if( maxFree<free ) maxFree=free,select=tmu;
	}
	_selectTMU=select;
	return select;
}

void TextureGlide::GRAMUse( HMipCacheRoot *list, int level )
{
	// does not contain any reference to mipmap - only texture and level
	if( _cacheGRAM )
	{
		_cacheGRAM->Delete();
		Assert( _cacheGRAM->texture==this );
		if( _cacheGRAMRoot!=list || _cacheGRAM->level>level )
		{
			// find best level in one frame
			_cacheGRAM->level=level;
		}
	}
	else
	{
		_cacheGRAM=new HMipCacheGlide;
		_cacheGRAM->texture=this;
		_cacheGRAM->level=level;
	}
	list->Insert(_cacheGRAM);
	_cacheGRAMRoot=list;
}

void TextureGlide::CacheUse( HMipCacheRoot *list, int level )
{
	HMipCacheGlide *first;
	if( _cache ) _cache->Delete(),first=_cache;
	else first=new HMipCacheGlide;
	first->texture=this;
	first->level=level;
	list->Insert(first);
	_cache=first;
}

void TextureGlide::ASetNMipmaps( int n )
{
	_nMipmaps=n;
	PacLevelMem &mip=_mipmaps[n-1];
	saturateMax(_maxSize,mip._w);
	saturateMax(_maxSize,mip._h);
	saturateMin(_maxSize,256);
	if( _interpolate ) _interpolate->ASetNMipmaps(n);
	SetupInfo();
}

float TextureGlide::UToPhysical( float u ) const
{
	int uf,vf;
	int aRatio=_mipmapsInfo[0]._info.aspectRatioLog2;
	if( aRatio>0 ) uf=256,vf=256>>aRatio;
	else vf=256,uf=256>>-aRatio;
	return u*uf;
}

float TextureGlide::VToPhysical( float v ) const
{
	int uf,vf;
	int aRatio=_mipmapsInfo[0]._info.aspectRatioLog2;
	if( aRatio>0 ) uf=256,vf=256>>aRatio;
	else vf=256,uf=256>>-aRatio;
	return v*vf;
}

float TextureGlide::UToLogical( float u ) const
{
	int uif,vif;
	int aRatio=_mipmapsInfo[0]._info.aspectRatioLog2;
	if( aRatio>0 ) uif=1,vif=1<<aRatio;
	else vif=1,uif=1<<-aRatio;
	// TODO: no division
	return u*(1.0f/256)*uif;
}

float TextureGlide::VToLogical( float v ) const
{
	int uif,vif;
	int aRatio=_mipmapsInfo[0]._info.aspectRatioLog2;
	if( aRatio>0 ) uif=1,vif=1<<aRatio;
	else vif=1,uif=1<<-aRatio;
	// TODO: no division
	return v*(1.0f/256)*vif;
}

void TextureGlide::SetupInfo()
{
	// prepare _base information
	int w=Width(0);
	int h=Height(0);
	int biggerSize=( h>w ? h : w );
	int lodIndex=GR_LOD_LOG2_256;
	// determine lod size, Glide defines
	//#define GR_LOD_256 0x0
	//#define GR_LOD_1   0x8
	if (biggerSize==0)
	{
		ErrF("Texture %s: singular biggerSize",(const char *)Name());
	}
	else
	{
		while( biggerSize>256 ) biggerSize>>=1,lodIndex++;
		while( biggerSize<256 ) lodIndex--,biggerSize<<=1;
	}
	if( h==w ) _aRatio=GR_ASPECT_LOG2_1x1;
	else if( h*2==w ) _aRatio=GR_ASPECT_LOG2_2x1;
	else if( h==w*2 ) _aRatio=GR_ASPECT_LOG2_1x2;
	else if( h*4==w ) _aRatio=GR_ASPECT_LOG2_4x1;
	else if( h==w*4 ) _aRatio=GR_ASPECT_LOG2_1x4;
	else if( h*8==w ) _aRatio=GR_ASPECT_LOG2_8x1;
	else if( h==w*8 ) _aRatio=GR_ASPECT_LOG2_1x8;
	else ErrorMessage("Invalid texture aspect ratio (%dx%d)",w,h);

	GrTextureFormat_t format;
	switch( _mipmaps[0].DstFormat() )
	{
		case PacARGB4444:
			format=GR_TEXFMT_ARGB_4444;
		break;
		case PacAI88:
			format=GR_TEXFMT_ALPHA_INTENSITY_88;
		break;
		case PacDXT1: // support for DXT1 decompression
		case PacARGB1555:
			format=GR_TEXFMT_ARGB_1555;
		break;
		case PacRGB565:
			format=GR_TEXFMT_RGB_565;
		break;
		default:
			Fail("Bad texture format.");
			format=GR_TEXFMT_ARGB_1555;
		break;
	}
	int i;
	for( i=0; i<_nMipmaps; i++ )
	{
		MipmapLevelGlideInfo *mip=&_mipmapsInfo[i];
		mip->_info.smallLodLog2=0;
		mip->_info.largeLodLog2=0;
		mip->_info.aspectRatioLog2=_aRatio;
		mip->_info.format=format;
		mip->_info.data=NULL;
	}
	
	int level=0;
	// skip levels that are too large
	while( lodIndex>GR_LOD_LOG2_256 && level<_nMipmaps )
	{
		lodIndex--,level++;
	}

	// calculate worst LOD index
	//int biggestLOD=lodIndex;
	int smallestLOD=lodIndex;
	// for lodIndex<=2 (256..64) we can use the same procedure
	while( lodIndex>=GR_LOD_LOG2_2 && level<_nMipmaps )
	{
		MipmapLevelGlideInfo *mip=&_mipmapsInfo[level];
		smallestLOD=lodIndex;
		mip->_info.smallLodLog2=lodIndex;
		mip->_info.largeLodLog2=lodIndex;
		mip->_info.aspectRatioLog2=_aRatio;
		mip->_info.format=format;
		mip->_info.data=NULL;
		lodIndex--;
		level++;
	}

	/*
	_texInfo.smallLodLog2=biggestLOD;
	_texInfo.largeLodLog2=smallestLOD;
	_texInfo.aspectRatioLog2=_aRatio;
	_texInfo.format=format;
	_texInfo._size=grTexTextureMemRequired(GR_MIPMAPLEVELMASK_BOTH,&mip->_info);
	_texInfo.data=NULL;
	*/

	for( i=0; i<level; i++ )
	{
		MipmapLevelGlideInfo *mip=&_mipmapsInfo[i];
		mip->_info.smallLodLog2=smallestLOD;
		mip->_size=grTexTextureMemRequired
		(
			GR_MIPMAPLEVELMASK_BOTH,&mip->_info
		);
	}
}

void TextureGlide::ReleaseMemory()
{
	if( _memLevel<0 ) return;
	TextBankGlide *bank=static_cast<TextBankGlide *>(GLOB_ENGINE->TextBank());
	Assert( _memory );
	Assert( _cache );
	_memLevel=-1;
	bank->_heap.Free(_memory),_memory=NULL;
	_cache->Delete(),delete _cache,_cache=NULL;
}

void TextureGlide::ReleaseGRAM()
{
	if( _gramLevel<0 ) return;
	TextBankGlide *bank=static_cast<TextBankGlide *>(GLOB_ENGINE->TextBank());
	DoAssert( _isGRAM );
	DoAssert( _cacheGRAM );
	DoAssert( _cacheGRAMRoot );
	_gramLevel=-1;
	TextBankGlide::TMUCache &tmuCache=bank->_tmu[_tmu];
	tmuCache._vram.Free(_isGRAM),_isGRAM=NULL;
	_cacheGRAM->Delete(),delete _cacheGRAM,_cacheGRAM=NULL;
	_cacheGRAMRoot=NULL;
	_tmu=-1;
}

#define TRANSFER_DROP_DOWN 0.5

MipInfo TextBankGlide::UseMipmap
(
	Texture *absTexture, int startLevel, int topLevel
)
{
	if( !absTexture )
	{
		return MipInfo(NULL,0);
	}
	// we are sure that texture is of type Texture
	// however this cast is potentially unsafe - take care
	TextureGlide *texture=static_cast<TextureGlide *>(absTexture);
	if( texture->_nMipmaps<=0 )
	{
		Fail("Texture has no mipmap levels");
		return MipInfo();
	}

	saturateMin(startLevel,texture->_mipmapNeeded);
	saturateMin(topLevel,texture->_mipmapWanted);

	// never use mipmaps smaller than some limit
	#define SMALLEST_MIP 16
	// avoid startLevel<0
	if( startLevel<0 ) startLevel=0;
	else if( startLevel>texture->_nMipmaps-1 ) startLevel=texture->_nMipmaps-1;
	for( ; startLevel>0; startLevel-- )
	{
		PacLevelMem *mipTop=&texture->_mipmaps[startLevel];
		if( mipTop->_w>=SMALLEST_MIP ) break;
		if( mipTop->_h>=SMALLEST_MIP ) break;
	}
		
	// skip too fine mipmaps
	for(;;)
	{
		PacLevelMem *mipTop=&texture->_mipmaps[startLevel];
		if( !mipTop->TooLarge(texture->_maxSize) ) break;
		startLevel++;
		if( startLevel>=texture->_nMipmaps )
		{
			Fail("No coarse mipmap levels");
			return MipInfo();
		}
	}

	if( topLevel>startLevel ) topLevel=startLevel;
	int mipGRAMLevel=texture->_nMipmaps;
	int mipSysLevel=texture->_nMipmaps;
	int mipWantedLevel=startLevel;
	
	// if there is good mipmap ready in GRAM, use it
	if( texture->_gramLevel>=0 )
	{
		//mipGRAM=&texture->_mipmaps[texture->_gramLevel];
		mipGRAMLevel=texture->_gramLevel;
	}
	
	//Assert( (!mipGRAM)==(texture->_isGRAM==NULL) );

	if( mipGRAMLevel<=mipWantedLevel )
	{
		// we have mipmap ready which is good enough
		// use it, mark needed level as used - if possible
		// note: there is only one GRAM mipmap stored
		//if( mipGRAM==mipWanted )
		Assert( texture->_cacheGRAM ); // it must be already cached
		Assert( texture->_cacheGRAMRoot );
		TMUCache &tmuCache=_tmu[texture->_tmu];
		texture->GRAMUse(&tmuCache._thisFrameGRAM,mipWantedLevel);
		return MipInfo(texture,mipGRAMLevel);
	}

	// find some mipmap that is in system heap
	if( texture->_memLevel>=0 )
	{
		//mipSys=&texture->_mipmaps[texture->_memLevel];
		mipSysLevel=texture->_memLevel;
	}

	if( mipSysLevel>mipWantedLevel )
	{
		// level not ready in system heap - load it
		//int better=mipWanted->_level-1;
		//if( better<startLevel ) better=startLevel;
		texture->_usedCount++; // this mipmap must not be released
		// note: during Load any _memory that is not marked as used can be released
		int ok = texture->Load(mipWantedLevel,mipWantedLevel);
		texture->_usedCount--;
		if (ok<0)
		{
			RptF("Texture %s not loaded",texture->Name());
			return MipInfo(NULL,0);
		}
		//Assert( texture->_usedCount==0 );
		Assert( texture->_memLevel==mipWantedLevel );
		//mipSys=mipWanted;
		mipSysLevel=mipWantedLevel;

		Assert( texture->_memory );
	}

	// texture is now in texture heap, we can copy it into GRAM cache
	//Assert( mipSys );
	Assert( texture->_memLevel==mipSysLevel );
	Assert( texture->_memory );
	// we copy from mipSys to mip
	// copy target (mipWanted) cannot be better then mipSys
	//if( mipWanted->_level<mipSys->_level ) mipWanted=mipSys;
	Assert( mipWantedLevel>=mipSysLevel );
	if( mipGRAMLevel>mipWantedLevel )
	{
		// select some TMU to download the texture to

		// detail texturing: detail texture must be in tmu 1, basic in tmu 0
		int tmu;
		bool forceTmu=false;
		if( texture->_isDetail ) tmu=1,forceTmu=true;
		else if( texture->_useDetail ) tmu=0,forceTmu=true;
		else tmu=SelectTMU();
		Assert( tmu>=0 && tmu<256 );
		TMUCache &tmuCache=_tmu[tmu];

		// avoid releasing GRAM during allocation
		texture->_gramUsedCount++;
		MipmapLevelGlideInfo *mipW=&texture->_mipmapsInfo[mipWantedLevel];
		ReserveGRAM(tmu,mipW->TotalSize(),false);
		GRAMItem *gram=tmuCache._vram.Alloc(mipW->TotalSize());
		if( !gram )
		{
			if( mipGRAMLevel>=texture->_nMipmaps )
			{
				// we need something - force release	
				Log("VRAM needed.");
				Assert( mipWantedLevel<texture->_nMipmaps );
				for(;;)
				{
					MipmapLevelGlideInfo *mipW=&texture->_mipmapsInfo[mipWantedLevel];
					ReserveGRAM(tmu,mipW->TotalSize(),true);
					gram=tmuCache._vram.Alloc(mipW->TotalSize());
					if( gram ) break;
					mipWantedLevel++;
					if( mipWantedLevel>=texture->_nMipmaps )
					{
						Fail("VRAM not available.");
						break;
					}
				}
			}
			else
			{
				// no more texture can be loaded this frame
				// we do not mind - we have some mipmap ready
				// try other tmus
				int defTmu=tmu;
				if( !forceTmu )
				{
					MipmapLevelGlideInfo *mipW=&texture->_mipmapsInfo[mipWantedLevel];
					int nTMUs=NTMUs();
					for( tmu=0; tmu<nTMUs; tmu++ ) if( tmu!=defTmu )
					{
						ReserveGRAM(tmu,mipW->TotalSize(),false);
						gram=tmuCache._vram.Alloc(mipW->TotalSize());
						if( gram ) break;
					}
				}
				if( !gram )
				{
					// all TMUs are full of data necessary for this frame
					// we have to adjust drop down to reflect we cannot 
					Glob.fullDropDownChange=0.1;
					tmu=defTmu;
					mipWantedLevel=texture->_nMipmaps;
				}
			}
		}
		texture->_gramUsedCount--;
		if( mipWantedLevel<texture->_nMipmaps )
		{
			texture->ReleaseGRAM();
			texture->_isGRAM=gram;
			texture->_tmu=tmu;
			texture->GRAMUse(&tmuCache._thisFrameGRAM,mipWantedLevel);

			Assert( texture->_isGRAM );
			Assert( texture->_memory );
			// we have target in mipWanted and source in mipSys
			//Log("Load GRAM %s (%dx%d)",texture->Name(),mip->_w,mip->_h);
			Assert( mipWantedLevel>=mipSysLevel );
			texture->_gramLevel=mipWantedLevel;
			_engine->CopyMipmap(texture,mipWantedLevel,mipSysLevel);
			MipmapLevelGlideInfo *mipW=&texture->_mipmapsInfo[mipWantedLevel];
			#if _ENABLE_PERFLOG
			ADD_COUNTER(tGRAM,mipW->TotalSize());
			#endif
			_fromHeapToGRAM += mipW->TotalSize();
			if( _fromHeapToGRAM>=(tmuCache._vram.Size()>>2) )
			{
				// drop down - too heavy memory -> GRAM transfers
				Glob.fullDropDownChange=0.1;
			}
		}
	}

	if( mipWantedLevel>=texture->_nMipmaps )
	{
		mipWantedLevel=mipGRAMLevel;
	}
	if( mipWantedLevel>=texture->_nMipmaps )
	{
		Fail("fatal error: No mipmap available, no memory available.");
		Log("No mipmap available %s",texture->Name());
		//_loader->Unlock();
		return MipInfo();
	}

	if( mipWantedLevel==mipSysLevel )
	{
		// texture was necessary - move it in front of cache list
		MipmapLevelGlideInfo *mipS=&texture->_mipmapsInfo[mipSysLevel];
		if( mipS->Permanent() ) texture->CacheUse(&_permanentUsed,mipSysLevel);
		else texture->CacheUse(&_lastUsed,mipSysLevel);
	}

	Assert( texture->_isGRAM );
	// mipmap used - move it in priority order
	// was necessary - too good mipmaps were resolved first
	TMUCache &tmuCache=_tmu[texture->_tmu];
	texture->GRAMUse(&tmuCache._thisFrameGRAM,mipWantedLevel);
	//_usedGRAM+=mipWanted->TotalSize();
	return MipInfo(texture,mipWantedLevel); // given mip-map loaded
}

/*
void TextBankGlide::ReleaseMipmap()
{
	//_loader->Unlock();
}
*/

void TextBankGlide::Compact()
{
	_texture.Compact();
}

void TextBankGlide::Preload()
{
	Compact();
	//ReserveMemory(INT_MAX);
	// flush - release all textures
	// mark all textures as old
	/*
	for( int tmu=0; tmu<NTMUs(); tmu++ )
	{
		ReserveGRAM(tmu,INT_MAX,true);
	}
	*/
	// preload all known textures
	PreloadTextures();
}

void TextBankGlide::PreloadTextures()
{
	Compact();
	//ScopeLock<TextureLoader> lock(*_loader);
	
	Temp<int> levelsHeap(_texture.Size());
	Temp<int> levelsVram(_texture.Size());

	ProgressAdd(_texture.Size());
	ProgressRefresh();

	int i;
	// first preload into heap as much as possible
	int heapLeft,vramLeft;
	int level,wantedSize;

	// there is probably obloha.pac loaded in memory
	//ReserveMemory(INT_MAX);
	//ReserveGRAM(INT_MAX,true);
	// preview preloading to determine how much we can load
	heapLeft=_heap.Size();
	// int tmu left=
	vramLeft=0;
	int nTMUs=NTMUs();
	for( int tmu=0; tmu<nTMUs; tmu++ )
	{
		TMUCache &tmuCache=_tmu[tmu];
		//int left=tmuCache._vram.TotalFreeLeft();
		int left=tmuCache._vram.Size();
		//if( vramLeft>left ) vramLeft=left;
		vramLeft+=left;
	}
	// leave some space free
	vramLeft=vramLeft*7/8-512*1024;
	if( vramLeft>heapLeft ) vramLeft=heapLeft;
	//vramLeft=2*1024*1024; // to do: detect actual vram
	DWORD sTime = ::GlobalTickCount();
	LogF("Preload preview %d,%d",heapLeft,vramLeft);
	// we cannot preload to vram more then to heap
	// start from worst levels, continue to better
	for( i=0; i<_texture.Size(); i++ ) levelsHeap[i]=levelsVram[i]=0;
	for( wantedSize=1; wantedSize<512*512*2; wantedSize<<=1 )
	{
		for( i=0; i<_texture.Size(); i++ )
		{
			TextureGlide *texture=_texture[i];
			for( level=0; level<texture->_nMipmaps; level++ )
			{
				PacLevelMem *mip=&texture->_mipmaps[level];
				if( mip->TooLarge(texture->_maxSize) ) continue;
				int size=mip->Size();
				if( size>wantedSize ) continue;
				// maybe it is already marked for loading
				//int sSize=(size+SYS_ALIGN-1)&~(SYS_ALIGN-1);
				int gSize=(size+GRAM_ALIGN-1)&~(GRAM_ALIGN-1);
				/*
				if( (levelsHeap[i]&(1<<level))==0 && heapLeft>sSize )
				{
					// if it will fit, mark it for preloading
					if( texture->_memLevel<0 || texture->_memLevel>level )
					{
						heapLeft-=sSize;
						levelsHeap[i]|=1<<level;
					}
				}
				*/
				if( (levelsVram[i]&(1<<level))==0 && vramLeft>gSize )
				{
					if( texture->_gramLevel<0 || texture->_gramLevel>level )
					{
						// if it will fit, mark it for preloading
						vramLeft-=gSize;
						levelsVram[i]|=1<<level;
					}
				}
			}
		}
		I_AM_ALIVE();
	}
	LogF("Preloading (time=%d)",::GlobalTickCount()-sTime);

	// preload
	#if 1 //!_DEBUG
	// without preloading there is a better chance of an error
	for( i=0; i<_texture.Size(); i++ )
	{
		TextureGlide *texture=_texture[i];
		ProgressAdvance(1);
		ProgressRefresh();
		/*
		if( levelsHeap[i] )
		{
			if( texture->Preload(levelsHeap[i])<0 )
			{
				//Fail("Sys preload.");
			}
		}
		*/
		//if( levelsVram[i] )
		{
			UseMipmap(texture,levelsVram[i],levelsVram[i]);
			//if( texture->PreloadGRAM(levelsVram[i])<0 )
			{
				//Fail("GRAM preload.");
			}
		}
	}

	LogF("  Preloaded Sys %d, max %d",_heap.Size()-_heap.TotalFreeLeft(),_heap.Size());
	for( int tmu=0; tmu<nTMUs; tmu++ )
	{
		TMUCache &tmuCache=_tmu[tmu];
		LogF("  Preloaded VRAM %d",tmuCache._vram.Size()-tmuCache._vram.TotalFreeLeft());
	}
	LogF("Preloaded (time=%d)",::GlobalTickCount()-sTime);
	#endif

}

void TextBankGlide::FlushTextures()
{
}

void TextBankGlide::FlushBank(QFBank *bank)
{
	for (int i=0; i<_texture.Size(); i++)
	{
		TextureGlide *tex = _texture[i];
		if (!tex) continue;
		// check if texture is from given bank
		if (!bank->FileExists(tex->GetName())) continue;
		_texture.Delete(i);
		i--;
	}
}

void TextBankGlide::InitDetailTextures()
{
	if( NTMUs()<2 ) return; // no detail texturing
	if( _detail ) return; // already initialized

	static RStringB detailName="data\\detail.pac";
	_detail=new TextureGlide;
	_detail->Init(detailName);
	_detail->InitLoaded();
	_detail->_isDetail=true;

	static RStringB specularName="data\\specular.pac";
	_specular=new TextureGlide;
	_specular->Init(specularName);
	_specular->InitLoaded();
	_specular->_isDetail=true;
}


void TextBankGlide::StartFrame()
{
	//_usedGRAM=0;
	InitDetailTextures();

	for( int i=0; i<_texture.Size(); i++ )
	{
		TextureGlide *texture=_texture[i];
		if( texture )
		{
			// mark all levels as not needed
			//texture->_levelNeededLastFrame=texture->_levelNeededThisFrame;
			//texture->_levelNeededThisFrame=texture->_nMipmaps;
			texture->ResetMipmap();
		}
	}
}

static void ChangeRoot( HMipCacheRoot *root, const HMipCacheRoot *set )
{
	HMipCacheGlide *item;
	for( item=root->First(); item; item=root->Next(item) )
	{
		item->texture->SetCacheGRAMRoot(set);
	}
}
 
void TextBankGlide::FinishFrame()
{
	// make history one frame older
	int nTMUs=NTMUs();
	for( int tmu=0; tmu<nTMUs; tmu++ )
	{
		TMUCache &tmuCache=_tmu[tmu];
		// mark last as previous
		ChangeRoot(&tmuCache._lastFrameGRAM,&tmuCache._previousGRAM);
		tmuCache._previousGRAM.Move(tmuCache._lastFrameGRAM);
		Assert( tmuCache._lastFrameGRAM.Last()==NULL );

		ChangeRoot(&tmuCache._thisFrameGRAM,&tmuCache._lastFrameGRAM);
		tmuCache._lastFrameGRAM.Move(tmuCache._thisFrameGRAM);
		Assert( tmuCache._thisFrameGRAM.Last()==NULL );

		#if 0
			LogF
			(
				"TMU %d: busy %d, free %d, max block %d",
				tmu,tmuCache._vram.CalcTotalBusy(),
				tmuCache._vram.CalcTotalFreeLeft(),tmuCache._vram.MaxFreeLeft()
			);
		#endif
	}
	/*
	_needed._previousGRAM.Move(_needed._lastFrameGRAM);
	Assert( _needed._lastFrameGRAM.Last()==NULL );
	_needed._lastFrameGRAM.Move(_needed._thisFrameGRAM);
	Assert( _needed._thisFrameGRAM.Last()==NULL );
	*/
}

void TextBankGlide::ReloadAll()
{
	// Glide - GRAM cache is permanent, no need to restore
	
	// app is still not in fullscreen mode - cannot reload
	// therefore pretend that we need all GRAM
	/*
	ReserveGRAM(INT_MAX,true);
	Log("GRAM Released.");
	*/
	/*
	// reload all from RAM to GRAM
	ScopeLock<TextureLoader> lock(*_loader);
	int i;
	for( i=0; i<MAX_BANKTEXTURES; i++ )
	{
		TextureGlide *texture=_texture[i];
		if( !texture ) continue;
		for( int level=0; level<texture->_nMipmaps; level++ )
		{
			PacLevelMem *mip=&texture->_mipmaps[level];
			#if CHECKSUMS
				DoAssert( texture->VerifyChecksum(mip) );
			#endif
			if( mip->_isGRAM )
			{
				//if( part->_memory ) _engine->CopyMipmap(part);
				if( mip->_memory ) _engine->FillMipmap(mip,0xff00);
				else _engine->FillMipmap(mip,0x8000);
			}
		}
	}
	*/
}


/*
bool TextBankGlide::CheckConsistency()
{
	_loader->Lock();
	// for debugging purposes
	bool error=false;
	int i;
	for( i=0; i<MAX_BANKTEXTURES; i++ )
	{
		TextureGlide *texture=_texture[i];
		if( !texture ) continue;
		int l;
		for( l=0; l<texture->_nMipmaps; l++ )
		{
			PacLevelMem *mip=&texture->_mipmaps[l];
			MultibasePart *part=mip->_base;
			#define TFail() \
			{\
				Fail("Texture invalid.");\
				Log("Texture invalid: %s %d (%dx%d)",texture->Name(),l,mip->_w,mip->_h);\
			}
			//if( l<part->_minLevel ) {TFail();error=true;}
			//if( l>part->_maxLevel ) {TFail();error=true;}
			//if( part->_memory && !part->_cache ) {TFail();error=true;}
			//if( l!=mip->_level ) {TFail();error=true;}
			//if( texture!=mip->_texture ) {TFail();error=true;}
		}
	}
	_loader->Unlock();
	return !error;
}
*/

bool TextBankGlide::VerifyChecksums()
{
	return true;
}

int TextBankGlide::UsedGRAM( HMipCacheRoot *root )
{
	//ScopeLock<TextureLoader> lock(*_loader);
	// GRAM used to draw this and last frame
	HMipCacheGlide *item;
	int sum=0;
	for( item=root->First(); item; item=root->Next(item) )
	{
		MipmapLevelGlideInfo &mip=item->texture->_mipmapsInfo[item->level];
		sum+=mip.TotalSize();
	}
	return sum;
}

int TextBankGlide::LastFrameGRAM()
{
	// GRAM used to draw this and last frame
	//return _usedGRAM;
	int sum=0;
	int nTMUs=NTMUs();
	for( int tmu=0; tmu<nTMUs; tmu++ )
	{
		TMUCache &tmuCache=_tmu[tmu];
		sum+=UsedGRAM(&tmuCache._thisFrameGRAM)+UsedGRAM(&tmuCache._lastFrameGRAM);
	}
	return sum;
}

Color TextureGlide::GetPixel( int level, float u, float v ) const
{
	#if 1
	int mipSysLevel = _nMipmaps;
	if (_memLevel>=0 ) mipSysLevel=_memLevel;
	if( mipSysLevel>level )
	{
		TextureGlide *cThis = const_cast<TextureGlide *>(this);
		// level not ready in system heap - load it
		// note: during Load any _memory that is not marked as used can be released
		cThis->_usedCount++; // this mipmap must not be released
		cThis->Load(level,level);
		cThis->_usedCount--; // this mipmap must not be released
	}
	#endif
	if( level<_memLevel ) level=_memLevel;
	if( level>=0 && level<_nMipmaps )
	{
		// check where are the data
		byte *data = _memory->Memory();
		for (int ii=_memLevel; ii<level; ii++)
		{
			data += _mipmapsInfo[level]._size;
		}
		Color color = _mipmaps[level].GetPixel(data,u,v);
		return color;
	}
	// fall back - no texture loaded
	return HWhite;
}

#endif //!_DISABLE_GUI
