/**
    @file   netTransportNet.cpp
    @brief  NetTransport implementation using Net* library

    Copyright &copy; 2001-2003 by BIStudio (www.bistudio.com)
    @author PE
    @date   14.10.2003
*/

#include "wpch.hpp"
#include "netTransport.hpp"
#include <El/ParamFile/paramFile.hpp>

#if !_ENABLE_MP

NetTranspSessionEnum *CreateNetSessionEnum ( const ParamEntry &cfg )
{
    return NULL;
}

NetTranspClient *CreateNetClient ( const ParamEntry &cfg )
{
    return NULL;
}

NetTranspServer *CreateNetServer ( const ParamEntry &cfg )
{
    return NULL;
}

#else

#include "MemHeap.hpp"
#include "strFormat.hpp"
#include "El/Network/netpch.hpp"
#include "El/Network/peerfactory.hpp"
#include "El/Network/netpeer.hpp"
#include "El/Network/netchannel.hpp"

//---------------------------------------------------------------------------
//  interface:

#ifndef DPNID_ALL_PLAYERS_GROUP
#  define DPNID_ALL_PLAYERS_GROUP   0
#endif

#define MAX_SESSIONS             16
#define LEN_SESSION_NAME        256
#define LEN_MISSION_NAME         40

#define NUM_PORTS_TO_TRY         15
#define PORT_INTERVAL            12
#define UNIQUE_TO_TRY            12

#define LEN_PLAYER_NAME          40
#define LEN_PASSWORD_NAME        40

#if !_SUPER_RELEASE
#  undef DEDICATED_STAT_LOG
#endif

#ifdef DEDICATED_STAT_LOG
#  define CONSOLE_LOG_INTERVAL 2000000
#endif

extern int GetNetworkPort();
#if _ENABLE_DEDICATED_SERVER
  extern RString GetPidFileName ();
  bool IsDedicatedServer ();
  int ConsoleF ( const char *format, ... );
#endif

#ifdef NET_LOG_TRANSP_STAT
#  define STAT_LOG_INTERVAL     80000
#endif

/// Maximum enumeration message age (in milliseconds).
#define MAX_ENUM_AGE          15000
/// Minimum enumeration retry time (in milliseconds).
#define MIN_ENUM_RETRY         4000
/// Maximum time to wait for AckPlayer response (in milliseconds).
#define ACK_PLAYER_TIMEOUT     8000
/// Re-send time for MAGIC_CREATE_PLAYER and MAGIC_RECONNECT_PLAYER (in milliseconds).
#define CREATE_PLAYER_RESEND   2000
/// Time interval to check network response (NetClient::Init(), in milliseconds).
#define NET_CHECK_WAIT          100
/// Time to wait in network destructors (for last message departure, in milliseconds).
#define DESTRUCT_WAIT           500
/// Time to wait for the higher-layer to process player-delete operation (in milliseconds).
#define DESTROY_WAIT            100
/// Send-timeout for common messages (in micro-seconds).
#define SEND_TIMEOUT            800000

/// Tag for messages starting with 32-bit "magic" number.
#define MSG_MAGIC_FLAG          0x0001

/// Define this symbol if both legacy and new enumeration responses will be sent.
//#define ENUM_LEGACY

    // magic numbers (sent in little-endian format /Intel/):

/// Enumeration request (client:broadcast -> server:broadcast).
#define MAGIC_ENUM_REQUEST      0xeee191ae
/// Enumeration response (server:broadcast -> client:broadcast).
#define MAGIC_ENUM_RESPONSE     0xfff1e8ac
/// Enumeration response - legacy version (server:broadcast -> client:broadcast).
#define MAGIC_ENUM_RESPONSE_LEGACY 0xfff4fe12
/// CreatePlayer request (client:normal -> server:broadcast). Not VIM. Duplicate messages should be ignored.
#define MAGIC_CREATE_PLAYER     0xccca1e12
/// AckPlayer response (server:both -> client:normal). I need some acknowledgement for broadcast sender (in far future).
#define MAGIC_ACK_PLAYER        0xaaa51a7e
/// ReconnectPlayer request (client:normal -> server:broadcast).
#define MAGIC_RECONNECT_PLAYER  0x111044ec
/// Terminate the session (server:normal -> clients:normal). Not VIM.
#define MAGIC_TERMINATE_SESSION 0x777814a1
/// DestroyPlayer message (client:normal -> server:normal). Not VIM.
#define MAGIC_DESTROY_PLAYER    0xddd15072

#pragma pack (push,netPackets,1)

/// Universal packet starting with 32-bit magic number.
struct MagicPacket {

    /// Magic number (MAGIC_*).
    unsigned32 magic;

    } PACKED;

/// Enumeration request (will be sent through network).
struct EnumPacket : public MagicPacket {

    /// Application-specific magic number.
    int32 magicApplication;

    } PACKED;

/// Session description packet (will be sent through network).
struct SessionPacket : public MagicPacket {

    /// Name of sesion.
    char name[LEN_SESSION_NAME];

    /// Current version of application on host.
    int32 actualVersion;

    /// Required version of application on host.
    int32 requiredVersion;

    /// Name of running mission on host.
    char mission[LEN_MISSION_NAME];

    /// State of game on host.
    int32 gameState;

    /// Maximal number of players + 2 (or 0 if not restricted)
    int32 maxPlayers;

    /// Password is required.
    int16 password;

    /// Receiving port.
    int16 port;

    /// Actual number of players.
    int32 numPlayers;

    /// Serial number of enumeration request.
    MsgSerial request;

    /// list of mods
    char mod[MOD_LENGTH];

    /// equal mod are required for clients
    int16 equalModRequired;

} PACKED;

const size_t SESSION_PACKET_1 = offsetof(SessionPacket,numPlayers); // LEGACY_SESSION_PACKET
const size_t SESSION_PACKET_2 = offsetof(SessionPacket,mod);
const size_t SESSION_PACKET_3 = sizeof(SessionPacket);
const size_t SESSION_PACKET_SIZE = SESSION_PACKET_3;

/// Description of single session (will be stored within enumeration object).
struct NetSessionDescription : public SessionPacket {

    /// Full address (string format).
    char address[32];

    /// IP adress in network format.
    unsigned32 ip;

    /// Last update time in milliseconds.
    unsigned32 lastTime;

    /// Actual ping (RTT) time.
    unsigned32 pingTime;

    } PACKED;

/// Create player packet (will be sent from NetTranspClient to NetTranspServer).
struct CreatePlayerPacket : public MagicPacket {

    /// Application-specific magic number.
    int32 magicApplication;

    char name[LEN_PLAYER_NAME];

    char password[LEN_PASSWORD_NAME];

    char mod[MOD_LENGTH];

    int32 actualVersion;

    int32 requiredVersion;

    int16 botClient;

    int32 uniqueID;                         ///< Unique player ID (derived from system time and acknowledged by the server).

    } PACKED;

/// Server response to CreatePlayer message (AckPlayer)
struct AckPlayerPacket : public MagicPacket {

    /// Connection result (enum ConnectResult)
    int32 result;

    /// Player number (for future reconnection).
    int32 playerNo;

    } PACKED;

/// Reconnect player packet (sent from NetTranspClient to NetTranspServer).
struct ReconnectPlayerPacket : public MagicPacket {

    /// Player number (from first player acknowledgement).
    int32 playerNo;

    } PACKED;

#pragma pack (pop,netPackets)

//---------------------------------------------------------------------------
//  NetSessionDescriptions:

/// Container for list of sessions.
class NetSessionDescriptions {

protected:

    /// Current number of contained sessions.
    int _size;

    /// Sessions.
    NetSessionDescription _data[MAX_SESSIONS];

public:

    /// Constructor
    NetSessionDescriptions () { _size = 0; }

    /// Destructor
    ~NetSessionDescriptions () { Clear(); }

    /// Return current number of contained sessions
    int Size () { return _size; }

    /**
        Add new session.
        @return index of added session or -1 if session doesn't fit into container.
    */
    int Add ();

    /// Delete one session.
    void Delete ( int i );

    /// Delete all sessions.
    void Clear ();

    /// R/W access to single session.
    NetSessionDescription &operator [] ( int i )
    { return _data[i]; }

    /// R/O access to single session.
    const NetSessionDescription &operator [] ( int i ) const
    { return _data[i]; }

    };

//---------------------------------------------------------------------------
//  NetSessionEnum:

/// Net implementation of NetTranspSessionEnum class
class NetSessionEnum : public NetTranspSessionEnum {

protected:

    /// List of enumerated sessions.
    NetSessionDescriptions _sessions;

    /// Critical section for _sessions.
    PoCriticalSection _cs;

    /// Enter the critical section.
    inline void enter ()
    { _cs.enter(); }

    /// Leave the critical section.
    inline void leave ()
    { _cs.leave(); }

    /// Enumeration is running.
    bool _running;

    /// Magic number for application separation.
    int32 magicApp;

    /**
        Sends batch of requests to the given host address.
        @param  br Existing (and well-configured) broadcast channel.
        @param  addr IP address of the host (network endian).
        @param  port First port number to try (host endian).
    */
    void sendRequest ( NetChannel *br, struct sockaddr_in &addr, unsigned16 port );
    
    /**
        Determines if the server needs to be requested (for enumeration retries).
        @param  addr IP address of the host (network endian).
        @param  port First port number to try (host endian).
        @return <code>true</code> if the address needs to be requested again.
    */
    bool needsRequest ( struct sockaddr_in &addr, unsigned16 port );

    friend NetStatus enumReceive ( NetMessage *msg, NetStatus event, void *data );

public:

    /// Constructor
    NetSessionEnum ();

    /// Destructor
    virtual ~NetSessionEnum ();

    /// Initializes enumeration data.
    virtual bool Init ( int magic =0 );

    /// Releases enumeration data.
    virtual void Done ();

    virtual RString IPToGUID ( RString ip, int port );

    /// Session enumeration is running?
    virtual bool RunningEnumHosts ()
    { return _running; }

    /**
        Starts enumeration of remote sessions.
        @param  ip Distant IP address to try (or NULL for broadcast only).
        @param  port Distant port to try.
        @param  hosts Array of distant hosts to try (or NULL if none).
    */
    virtual bool StartEnumHosts ( RString ip, int port, AutoArray<RemoteHostAddress> *hosts );

    /// Cancel session enumeration.
    virtual void StopEnumHosts ();

    /// Number of actually enumerated sessions.
    virtual int NSessions ();

    /// Retrieve all enumerated sessions.
    virtual void GetSessions ( AutoArray<SessionInfo> &sessions );

    };

//---------------------------------------------------------------------------
//  NetClient:

/// Interface for network transport client class.
class NetClient : public NetTranspClient {

protected:

    /// Last reported message time in seconds.
    unsigned64 lastMsgReported;

    /// Associated communication channel (peer-to-peer point).
    Ref<NetChannel> channel;

    /// AckPlayer result (CRNone if not finished yet).
    int ackPlayer;

    /// PlayerNo (playerID on the server - for reconnection purposes).
    int playerNo;

    /// Am I bot-client?
    bool amIBot;

    /// The session was terminated (by the server or channel drop)..
    bool sessionTerminated;

    /// The session was terminated (by the server or channel drop)..
    NetTerminationReason whySessionTerminated;

    /// Linked list of received messages (can be NULL). Sorted by serial number.
    Ref<NetMessage> received;

    /// Inserts a new received message into 'received' sorted list..
    void insertReceived ( NetMessage *msg );

    /// Linked list of received message-parts to be merged.
    Ref<NetMessage> split;

    /// -> last received split message-part.
    NetMessage *lastSplit;

    /// Linked list of received message-parts to be merged.
    Ref<NetMessage> splitUrgent;

    /// -> last received split message-part.
    NetMessage *lastSplitUrgent;

    /// Critical section for received.
    PoCriticalSection rcvCs;

    /// Enter the critical section for received messages.
    inline void enterRcv ()
    { rcvCs.enter(); }

    /// Leave the critical section for received messages.
    inline void leaveRcv ()
    { rcvCs.leave(); }

    /// Linked list of sent messages (can be NULL).
    Ref<NetMessage> sent;

    /// Critical section for sent.
    PoCriticalSection sndCs;

    /// Enter the critical section for sent messages.
    inline void enterSnd ()
    { sndCs.enter(); }

    /// Leave the critical section for sent messages.
    inline void leaveSnd ()
    { sndCs.leave(); }

    /// Magic number for application separation.
    int32 magicApp;

    friend NetStatus clientReceive ( NetMessage *msg, NetStatus event, void *data );
    friend NetStatus enumReceive ( NetMessage *msg, NetStatus event, void *data );
    friend NetStatus clientSendComplete ( NetMessage *msg, NetStatus event, void *data );

#ifdef NET_LOG_INFO
    int oldLatency;
    int oldThroughput;
    bool printAge;
#endif

#ifdef NET_LOG_TRANSP_STAT
    unsigned64 nextStatLog;
#endif

public:

    /// Constructor. Does virtually no initialization.
    NetClient ();

    /// Destructor.
    virtual ~NetClient ();

    /**
        Initialize the Client object.
        @param  address IP address:port of the distant server ("server.domain.tld:port").
        @param  password Password used to join the session.
        @param  botClient Is this "bot-client" (default client)?
        @param  port Remote port number! The correct used port number will be returned.
        @param  player Player name.
        @param  versionInfo Version info of the remote session.
        @param  magic Magic number to separate different applications.
    */
    virtual ConnectResult Init ( RString address, RString password, bool botClient, int &port,
                                 RString player, MPVersionInfo &versionInfo, int magic =0 );

    /**
        Reconnects the client after IP:port change (re-dial).
    */
    virtual ConnectResult NetClient::ReInit ();

    /// Create and initialize DirectPlay voice client (dummy).
    virtual bool InitVoice ();
    virtual bool IsVoicePlaying ( int player );
    virtual bool IsVoiceRecording ();
    virtual NetTranspSound3DBuffer *Create3DSoundBuffer ( int player );

    /// Sends the given user (raw) message to server.
    virtual bool SendMsg ( BYTE *buffer, int bufferSize, DWORD &msgID, NetMsgFlags flags );

    /// Gets actual send-queue statistics.
    virtual void GetSendQueueInfo ( int &nMsg, int &nBytes, int &nMsgG, int &nBytesG );

    /// Gets actual I/O channel statistics.
    virtual bool GetConnectionInfo ( int &latencyMS, int &throughputBPS );

    /// Sets internal params for underlying NetChannel object.
    virtual void SetNetworkParams ( const ParamEntry &cfg );

    /// Last received message's age in seconds.
    virtual float GetLastMsgAge ();

    /// Last received message's age in seconds (used to eliminate "disconnect cheat")
        virtual float GetLastMsgAgeReliable() {return GetLastMsgAge();}

    /// Last reported message's age in seconds.
    virtual float GetLastMsgAgeReported ();

    /// Remember time of reported message.
    virtual void LastMsgAgeReported ();

    virtual bool IsSessionTerminated ();

    virtual NetTerminationReason GetWhySessionTerminated ();

    /// Processes received user messages.
    virtual void ProcessUserMessages ( UserMessageClientCallback *callback, void *context );

    /// Removes all received user messages.
    virtual void RemoveUserMessages ();

    /// Processes sent (and acknowledged) user messages.
    virtual void ProcessSendComplete ( SendCompleteCallback *callback, void *context );

    /// Removes all sent messages.
    virtual void RemoveSendComplete ();

    virtual RString GetStatistics ();

    virtual unsigned FreeMemory ();

    };


/*!
\patch 1.81 Date 8/7/2002 by Ondra
- Fixed: MP: Ghost might be left after user disconnected.
*/

/// Empty key value (internal).
int ExplicitMapTraits<int,RefD<NetChannel> >::keyNull = 0;

/// For removed values (internal).
int ExplicitMapTraits<int,RefD<NetChannel> >::zombie = -1;

//---------------------------------------------------------------------------
//  NetServer:

class ChannelSupport
{
public:

  /// Player (channel) id.
  int m_id;

  /// Linked list of received message-parts to be merged.
  Ref<NetMessage> m_split;

  /// -> last received split message-part.
  NetMessage *m_lastSplit;

  /// Linked list of received urgent message-parts to be merged.
  Ref<NetMessage> m_splitUrgent;

  /// -> last received urgent split message-part.
  NetMessage *m_lastSplitUrgent;

  ChannelSupport ()
  {
    m_id = 0;
    m_lastSplit = m_lastSplitUrgent = NULL;
  }

  ChannelSupport ( const ChannelSupport &from )
  {
    m_id              = from.m_id;
    m_split           = from.m_split;
    m_lastSplit       = from.m_lastSplit;
    m_splitUrgent     = from.m_splitUrgent;
    m_lastSplitUrgent = from.m_lastSplitUrgent;
  }

  bool operator== ( const ChannelSupport &sec ) const
  { return( m_id == sec.m_id ); }

};

TypeIsMovable(ChannelSupport);

unsigned64 ExplicitMapTraits<unsigned64,ChannelSupport>::keyNull = (unsigned64)-1;
unsigned64 ExplicitMapTraits<unsigned64,ChannelSupport>::zombie  = (unsigned64)-2;
ChannelSupport ExplicitMapTraits<unsigned64,ChannelSupport>::null;

/// Socket implementation for network transport server class.
class NetServer : public NetTranspServer {

protected:

    /// Session password
    RString _password;

    /// Session info in network format.
    SessionPacket session;

    /// Critical section for user map, session info, added & deleted users.
    PoCriticalSection usrCs;

    /// Enter the critical section for user map.
    inline void enterUsr ()
    { usrCs.enter(); }

    /// Leave the critical section for user map.
    inline void leaveUsr ()
    { usrCs.leave(); }

    /// Port on which the session is running.
    unsigned16 sessionPort;

    /// Name of session (contains name of player and IP address of host).
    RString sessionName;

    /// Linked list of received messages (can be NULL). Sorted by serial number.
    Ref<NetMessage> received;

    /// Inserts a new received message into 'received' sorted list..
    void insertReceived ( NetMessage *msg );

    /// Split lists for individual players, etc.
    ExplicitMap<unsigned64,ChannelSupport,true,MemAllocSafe> m_support;

    /// Critical section for received.
    PoCriticalSection rcvCs;

    /// Enter the critical section for received messages.
    inline void enterRcv ()
    { rcvCs.enter(); }

    /// Leave the critical section for received messages.
    inline void leaveRcv ()
    { rcvCs.leave(); }

    /// Linked list of sent messages (can be NULL).
    Ref<NetMessage> sent;

    /// Critical section for sent.
    PoCriticalSection sndCs;

    /// Enter the critical section for sent messages.
    inline void enterSnd ()
    { sndCs.enter(); }

    /// Leave the critical section for sent messages.
    inline void leaveSnd ()
    { sndCs.leave(); }

    /// Mapping from user ID# to NetChannels.
    ExplicitMap<int,RefD<NetChannel>,true,MemAllocSafe> users;

    /// ID of 'bot' client or 0.
    int botId;

    /// Info about new players.
    AutoArray<CreatePlayerInfo,MemAllocSafe> _createPlayers;

    /// Info about removed players.
    AutoArray<DeletePlayerInfo,MemAllocSafe> _deletePlayers;

    /// Magic number for application separation.
    int32 magicApp;

    /**
        Maps NetChannel to player ID#.
        @return Player # or <code>-1</code> if not found.
    */
    int channelToPlayer ( NetChannel *ch );

    friend NetStatus ctrlReceive ( NetMessage *msg, NetStatus event, void *data );
    friend NetStatus serverReceive ( NetMessage *msg, NetStatus event, void *data );
    friend NetStatus destroyPlayerCallback ( NetMessage *msg, NetStatus event, void *data );
    friend NetStatus serverSendComplete ( NetMessage *msg, NetStatus event, void *data );

    /// Complete player-destroy sequence (with notification message to the player).
    void destroyPlayer ( NetChannel *ch, NetTerminationReason reason );

    /// Final player-destroying (w/o notification message to the player).
    void finishDestroyPlayer ( int player );

    /// diagnostics - log current state of "users" variable
    void logUsers();

#ifdef NET_LOG_INFO
    int oldLatency;
    int oldThroughput;
#endif

#ifdef DEDICATED_STAT_LOG
    unsigned64 nextConsoleLog;
#endif

#ifdef NET_LOG_TRANSP_STAT
    unsigned64 nextStatLog;
    bool forceLog;
    bool inGetConnection;
#endif

public:

    /// Constructor. Does virtually no initialization.
    NetServer ();

    /// Destructor.
    virtual ~NetServer ();

    /**
        Initialize the server object.
        @param  port Local port to work with.
    */
    virtual bool Init ( int port, RString password, char *hostname, int maxPlayers,
                        RString sessionNameInit, RString sessionNameFormat, RString playerName,
                        MPVersionInfo &versionInfo, bool equalModRequired, int magic =0 );

    /// Create and initialize DirectPlay voice server (dummy).
    virtual bool InitVoice ();
    virtual void ProcessVoicePlayers ( CreateVoicePlayerCallback *callback, void *context );
    virtual void RemoveVoicePlayers ();
    virtual void GetTransmitTargets ( int from, AutoArray<int,MemAllocSA> &to );
    virtual void SetTransmitTargets ( int from, AutoArray<int,MemAllocSA> &to );

    virtual int GetSessionPort ()
    { return sessionPort; }

    virtual RString GetSessionName ()
    { return sessionName; }

    virtual bool SendMsg ( int to, BYTE *buffer, int bufferSize, DWORD &msgID, NetMsgFlags flags );

    virtual void CancelAllMessages ();

    virtual void GetSendQueueInfo ( int to, int &nMsg, int &nBytes, int &nMsgG, int &nBytesG );

    virtual bool GetConnectionInfo ( int to, int &latencyMS, int &throughputBPS );

    /// Last received message's age in seconds (used to eliminate "disconnect cheat")
        virtual float GetLastMsgAgeReliable( int player);

    /// Sets internal params for underlying NetChannel object.
    virtual void SetNetworkParams ( const ParamEntry &cfg );

    virtual void UpdateSessionDescription ( int state, RString mission );

    virtual bool GetURL ( char *address, DWORD addressLen );

    virtual void KickOff ( int player, NetTerminationReason reason );

    virtual void ProcessUserMessages ( UserMessageServerCallback *callback, void *context );

    virtual void RemoveUserMessages ();

    virtual void ProcessSendComplete ( SendCompleteCallback *callback, void *context );

    virtual void RemoveSendComplete ();

    virtual void ProcessPlayers ( CreatePlayerCallback *callbackCreate, DeletePlayerCallback *callbackDelete, void *context );

    virtual void RemovePlayers ();

    virtual RString GetStatistics ( int player );

    virtual unsigned FreeMemory ();

    };

//===========================================================================
//  implementation:

static void LoadNetworkParams ( NetworkParams &data, const ParamEntry &cfg )
{
    data = defaultNetworkParams;
    if ( cfg.FindEntry("sockets") )
    {
        const ParamEntry &c = cfg>>"sockets";

        #define GET_PAR(x) if (c.FindEntry(#x)) data.x = c>>#x
        #define GET_PAR_U(x) if (c.FindEntry(#x)) data.x = (int)(c>>#x)

        GET_PAR(winsockVersion);
        GET_PAR_U(rcvBufSize);
        GET_PAR_U(maxPacketSize);
        GET_PAR_U(dropGap);
        GET_PAR_U(ackTimeoutA);                   ///< Multiplicative coefficient for actual ack-timeout computation.
        GET_PAR_U(ackTimeoutB);                   ///< Additive coefficient for actual ack-timeout computation (in microseconds).
        GET_PAR_U(ackRedundancy);
        GET_PAR_U(initBandwidth);
        GET_PAR_U(minBandwidth);
        GET_PAR_U(maxBandwidth);
        GET_PAR_U(minActivity);
        GET_PAR_U(initLatency);
        GET_PAR_U(minLatencyUpdate);
        GET_PAR(minLatencyMul);
        GET_PAR_U(minLatencyAdd);
        GET_PAR(goodAckBandFade);
        GET_PAR_U(outWindow);
        GET_PAR_U(ackWindow);
        GET_PAR_U(maxChannelBitMask);
        GET_PAR(lostLatencyMul);
        GET_PAR(lostLatencyAdd);
        GET_PAR_U(maxOutputAckMask);
        GET_PAR_U(minAckHistory);
        GET_PAR(maxDropouts);
        GET_PAR(midDropouts);
        GET_PAR(okDropouts);
        GET_PAR(minDropouts);
        GET_PAR(latencyOverMul);
        GET_PAR(latencyOverAdd);
        GET_PAR(latencyWorseMul);
        GET_PAR(latencyWorseAdd);
        GET_PAR(latencyOkMul);
        GET_PAR(latencyOkAdd);
        GET_PAR(latencyBestMul);
        GET_PAR(latencyBestAdd);
        GET_PAR(maxBandOverGood);
        GET_PAR(safeMaxBandOverGood);

        const int nGrowEntries = 2 * MAX_GROW_STATE + 2;
        const ParamEntry *gcc = c.FindEntry("grow");
        if ( gcc && gcc->GetSize() == nGrowEntries*2 )
        {
            const ParamEntry &gc = *gcc;
            for (int i=0; i<nGrowEntries; i++)
            {
                data.grow[i].mul = gc[i*2];
                data.grow[i].add = gc[i*2+1];
            }
        }
        #undef GET_PAR
        #undef GET_PAR_U
    }
#ifdef NET_LOG
    static bool printFirst = true;
    if ( printFirst )
    {
      printFirst = false;
      char buf[264] = "Par: ";
      data.printParams1(buf+5);
      NetLog(buf);
      data.printParams2(buf+5);
      NetLog(buf);
      data.printParams3(buf+5);
      NetLog(buf);
    }
#endif
}

NetTranspSessionEnum *CreateNetSessionEnum ( const ParamEntry &cfg )
{
    NetworkParams data;
    LoadNetworkParams(data,cfg);
    NetChannelBasic::setGlobalNetworkParams(data);
    return new NetSessionEnum;
}

NetTranspClient *CreateNetClient ( const ParamEntry &cfg )
{
    NetworkParams data;
    LoadNetworkParams(data,cfg);
    NetChannelBasic::setGlobalNetworkParams(data);
    return new NetClient;
}

NetTranspServer *CreateNetServer ( const ParamEntry &cfg )
{
    NetworkParams data;
    LoadNetworkParams(data,cfg);
    NetChannelBasic::setGlobalNetworkParams(data);
    return new NetServer;
}

//---------------------------------------------------------------------------
//  NetSessionDescriptions:

int NetSessionDescriptions::Add ()
{
    if ( _size >= MAX_SESSIONS ) return -1;
    return _size++;
}

void NetSessionDescriptions::Delete ( int i )
{
    _size--;
    for ( int j = i; j < _size; j++ )
        _data[j] = _data[j + 1];
}

void NetSessionDescriptions::Clear ()
{
    _size = 0;
}

//---------------------------------------------------------------------------
//  Global pools and peers etc.:

NetStatus enumReceive ( NetMessage *msg, NetStatus event, void *data );
NetStatus ctrlReceive ( NetMessage *msg, NetStatus event, void *data );
NetStatus clientReceive ( NetMessage *msg, NetStatus event, void *data );
NetStatus serverReceive ( NetMessage *msg, NetStatus event, void *data );

static PoCriticalSection LockDecl(poolLock,"::poolLock");
                                            ///< locking of 'pool', 'clientPeer', 'serverPeer'

static RefD<NetPool> pool;                  ///< Global network pool.

static RefD<NetPeer> clientPeer;            ///< NetPeer for enumerator and client.

static RefD<NetPeer> serverPeer;            ///< NetPeer for server.

static NetPool *getPool ()
    // should be called inside of poolLock.enter()
{
    if ( !pool )
        pool = new NetPool( new PeerChannelFactoryUDP, NULL, NULL );
    return pool;
}

static void destroyPool ()
    // should be called inside of poolLock.enter()
{
    if ( clientPeer ) {
        if ( pool )
            pool->deletePeer(clientPeer);
        else
            clientPeer->close();
        clientPeer = NULL;
        }
    if ( serverPeer ) {
        if ( pool )
            pool->deletePeer(serverPeer);
        else
            serverPeer->close();
        serverPeer = NULL;
        }
    if ( pool )
        pool = NULL;
}

static void setupPortBitMask ( BitMask &mask, bool server )
{
    mask.empty();
    int port = GetNetworkPort() + (server ? 0 : 2);
    int i;
#if _ENABLE_DEDICATED_SERVER
    if ( server && GetPidFileName().GetLength() )
      mask.on(port);
    else
#endif
    for ( i = 0; i++ < NUM_PORTS_TO_TRY; port += PORT_INTERVAL )
        mask.on(port);
}

static NetPeer *getClientPeer ()
    // should be called inside of poolLock.enter()
{
    if ( !clientPeer )
    {
        BitMask portMask;
        setupPortBitMask(portMask,false);
        clientPeer = getPool()->createPeer(&portMask);
        if ( clientPeer )
        {
          NetChannel *ctrl = clientPeer->getBroadcastChannel();
          if ( ctrl ) ctrl->setProcessRoutine(enumReceive);
        }
    }
    return clientPeer;
}

static NetPeer *getServerPeer ()
    // should be called inside of poolLock.enter()
{
    if ( !serverPeer )
    {
        BitMask portMask;
        setupPortBitMask(portMask,true);
        serverPeer = getPool()->createPeer(&portMask);
        if ( serverPeer )
        {
          NetChannel *ctrl = serverPeer->getBroadcastChannel();
          if ( ctrl ) ctrl->setProcessRoutine(ctrlReceive);
        }
    }
    return serverPeer;
}

void decodeURLAddress ( RString address, RString &ip, int &port )
{
    const char *ptr = strrchr(address,':');
    if ( !ptr ) {
        ip = address;
        return;
        }
    ip = address.Substring(0,ptr-address);
    port = atoi(ptr+1);
}

//---------------------------------------------------------------------------
//  Receiving routines:

/// Actual enumeration instance (can be NULL).
static NetSessionEnum *_enum = NULL;

/// Actual client instance (can be NULL).
static NetClient *_client = NULL;

/// NetServer instance to process control messages.
static NetServer *_server = NULL;

/// Sets actual enumeration instance (can be NULL).
void setEnum ( NetSessionEnum *_en )
{
    poolLock.enter();
    _enum = _en;
    if ( _en ) {
        getPool();
        getClientPeer();
        }
    poolLock.leave();
}

/// Sets actual client instance (can be NULL).
void setClient ( NetClient *_cl )
{
    poolLock.enter();
    _client = _cl;
    if ( _cl ) {
        getPool();
        getClientPeer();
        }
    poolLock.leave();
}

/// Sets actual server instance (can be NULL).
void setServer ( NetServer *_srv )
{
    poolLock.enter();
    _server = _srv;
    if ( _srv ) {
        getPool();
        getServerPeer();
        }
    poolLock.leave();
}

/// Checks _enum, _client and _server. If everything is NULL, destroys the global NetPool!
static void checkPool ()
{
    poolLock.enter();
    if ( !_enum && !_client && !_server )
        destroyPool();
    poolLock.leave();
}

Ref<NetMessage> mergeMessageList ( NetMessage *msg )
{
    if ( !msg ) return NULL;
    unsigned size = 0;
    NetMessage *ptr = msg;
    NetMessage *last;
    do {
        size += ptr->getLength();
        last = ptr;
        } while ( (ptr = ptr->next) );
    Ref<NetMessage> composite = NetMessagePool::pool()->newMessage(size,msg->getChannel());
    if ( !composite ) return NULL;
    composite->setFrom(last);
    unsigned8 *data = (unsigned8*)composite->getData();
    for ( ptr = msg; ptr; ptr = ptr->next ) {
        memcpy(data,ptr->getData(),ptr->getLength());
        data += ptr->getLength();
        }
    composite->setLength(size);
    return composite;
}

/// Regular data received by the NetServer...
NetStatus serverReceive ( NetMessage *msg, NetStatus event, void *data )
{
        // thread-safe
    if ( !_server || !msg ) return nsNoMoreCallbacks; // nothing to do
    unsigned len = msg->getLength();
    if ( !len ) return nsNoMoreCallbacks;   // message is too small
    struct sockaddr_in dist;
    msg->getDistant(dist);
    unsigned flags = msg->getFlags();
    bool isMagic = (flags & MSG_MAGIC_FLAG) != 0;
    if ( isMagic ) {                        // magic-messages:
        if ( len < 4 )
            return nsNoMoreCallbacks;       // magic-message is too small
        unsigned32 magic = *(unsigned32*)msg->getData();

#ifdef NET_LOG_CTRL_RECEIVE
#  ifdef NET_LOG_BRIEF
        NetLog("Pe(%u):rCtrlG(%u.%u.%u.%u:%u,%u,%x)",
               getServerPeer()->getPeerId(),
               (unsigned)IP4(dist),(unsigned)IP3(dist),(unsigned)IP2(dist),(unsigned)IP1(dist),(unsigned)PORT(dist),
               msg->getLength(),magic);
#  else
        NetLog("Peer(%u)::serverReceive: received control message (from=%u.%u.%u.%u:%u, len=%3u, serial=%4u, flags=%04x, magic=%x)",
               getServerPeer()->getPeerId(),
               (unsigned)IP4(dist),(unsigned)IP3(dist),(unsigned)IP2(dist),(unsigned)IP1(dist),(unsigned)PORT(dist),
               msg->getLength(),msg->getSerial(),flags,magic);
#  endif
#endif

        switch ( magic ) {

            case MAGIC_DESTROY_PLAYER: {    // the player itself is disconnecting..
                _server->enterUsr();        // due to 'channelToPlayer()'
                int player = _server->channelToPlayer(msg->getChannel());
                if ( player < 0 )
                    RptF("No player found for channel %u - MAGIC_DESTROY_PLAYER message ignored",(unsigned)msg->getChannel());
                else
                    _server->finishDestroyPlayer(player);
                _server->leaveUsr();
                break;                      // don't need to lock this..
                }

            }

        return nsNoMoreCallbacks;           // unknown message => don't care
        }

        // process regular (user) message:
#ifdef NET_LOG_SERVER_RECEIVE
    NetLog("Peer(%u)::serverReceive: received user message (from=%u.%u.%u.%u:%u, len=%3u, serial=%4u, flags=%04x, ID=%x)",
           getServerPeer()->getPeerId(),
           (unsigned)IP4(dist),(unsigned)IP3(dist),(unsigned)IP2(dist),(unsigned)IP1(dist),(unsigned)PORT(dist),
           len,msg->getSerial(),flags,msg->id);
#endif
    _server->enterRcv();
        // merge partial messages:
    if ( flags & MSG_PART_FLAG ) {
        bool closing = (flags & MSG_CLOSING_FLAG) > 0;
        ChannelSupport *sup = _server->m_support.get(sockaddrKey(dist));
        Assert( sup );
        if ( flags & MSG_URGENT_FLAG )
            if ( closing ) {                // the last message-part to be merged..
                DoAssert( sup->m_splitUrgent.NotNull() );
                sup->m_lastSplitUrgent->next = msg;
                msg->next = NULL;
                msg = mergeMessageList(sup->m_splitUrgent);
                sup->m_splitUrgent = NULL;
                }
            else {                          // another message-part => remember it!
                if ( !sup->m_splitUrgent )
                    sup->m_splitUrgent = msg;
                else
                    sup->m_lastSplitUrgent->next = msg;
                (sup->m_lastSplitUrgent = msg)->next = NULL;
                _server->leaveRcv();
                return nsNoMoreCallbacks;
                }
        else
            if ( closing ) {                // the last message-part to be merged..
                DoAssert( sup->m_split.NotNull() );
                sup->m_lastSplit->next = msg;
                msg->next = NULL;
                msg = mergeMessageList(sup->m_split);
                sup->m_split = NULL;
                }
            else {                          // another message-part => remember it!
                if ( !sup->m_split )
                    sup->m_split = msg;
                else
                    sup->m_lastSplit->next = msg;
                (sup->m_lastSplit = msg)->next = NULL;
                _server->leaveRcv();
                return nsNoMoreCallbacks;
                }
#ifdef NET_LOG_MERGE
#  ifdef NET_LOG_BRIEF
        NetLog("Pe(%u):rMerS(%u,%u,%x,%x)",
               getServerPeer()->getPeerId(),msg->getLength(),msg->getSerial(),(unsigned)msg->getFlags(),msg->id);
#  else
        NetLog("Peer(%u)::serverReceive: merging user message (len=%3u, serial=%4u, flags=%04x, ID=%x)",
               getServerPeer()->getPeerId(),msg->getLength(),msg->getSerial(),(unsigned)msg->getFlags(),msg->id);
#  endif
#endif
        }

    _server->insertReceived(msg);

    _server->leaveRcv();

    return nsNoMoreCallbacks;
}

/*!
\patch_internal 1.51 Date 4/17/2002 by Pepca
- Fixed: Bugs in player registration at the server (multiple registration can occur). [Sockets]
- Fixed: Disconnected user is now accepted by the server immediately when [s]he makes another try.
[Sockets]
\patch_internal 1.51 Date 4/18/2002 by Pepca
- Added: Each player has its "unique identifier" based on current system time (and verified in
initial player<->server negotiation). [Sockets]
\patch_internal 1.51 Date 4/18/2002 by Pepca
- Fixed: Player-count limit is now interpreted correctly by the server (it was ignored in previous
versions). [Sockets]
*/

/// Control (broadcast) data received by the NetServer...
NetStatus ctrlReceive ( NetMessage *msg, NetStatus event, void *data )
{
        // thread-critical: NetMessagePool::pool()->newMessage(), NetMessage::send(), strncmp(),
        //                  getServerPeer()->getPool()->createChannel(), AutoArray::Add()
    if ( !_server || !msg || msg->getLength() < 4 ||
         !(msg->getFlags() & MSG_MAGIC_FLAG) )
        return nsNoMoreCallbacks;           // fatal message error

    unsigned32 magic = *(unsigned32*)msg->getData();
    struct sockaddr_in distant;
    msg->getDistant(distant);

#ifdef NET_LOG_CTRL_RECEIVE
#  ifdef NET_LOG_BRIEF
    NetLog("Pe(%u):rCtrlB(%u.%u.%u.%u:%u,%u,%x)",
           getServerPeer()->getPeerId(),
           (unsigned)IP4(distant),(unsigned)IP3(distant),(unsigned)IP2(distant),(unsigned)IP1(distant),(unsigned)PORT(distant),
           msg->getLength(),magic);
#  else
    NetLog("Peer(%u)::ctrlReceive: received control message (from=%u.%u.%u.%u:%u, len=%3u, serial=%4u, flags=%04x, magic=%x)",
           getServerPeer()->getPeerId(),
           (unsigned)IP4(distant),(unsigned)IP3(distant),(unsigned)IP2(distant),(unsigned)IP1(distant),(unsigned)PORT(distant),
           msg->getLength(),msg->getSerial(),(unsigned)msg->getFlags(),magic);
#  endif
#endif

    switch ( magic ) {

        case MAGIC_ENUM_REQUEST:            // enumeration request
            if ( msg->getLength() == sizeof(EnumPacket) ) {
                EnumPacket *ep = (EnumPacket*)msg->getData();
                _server->enterUsr();
                if ( ep->magicApplication == _server->magicApp ) {
                    Ref<NetMessage> out = NetMessagePool::pool()->newMessage(SESSION_PACKET_SIZE,msg->getChannel());
                    if ( out ) {
                        out->setDistant(distant);
                        out->setFlags(MSG_ALL_FLAGS,MSG_TO_BCAST_FLAG|MSG_MAGIC_FLAG);
                        _server->session.magic      = MAGIC_ENUM_RESPONSE;
                        _server->session.request    = msg->getSerial();
                        _server->session.numPlayers = _server->users.card() - 1;
                        out->setData((unsigned8*)&(_server->session),SESSION_PACKET_SIZE);
                        out->send(true);    // urgent message
                        }
#ifdef ENUM_LEGACY
                    out = NetMessagePool::pool()->newMessage(SESSION_PACKET_1,msg->getChannel());
                    if ( out ) {
                        out->setDistant(distant);
                        out->setFlags(MSG_ALL_FLAGS,MSG_TO_BCAST_FLAG|MSG_MAGIC_FLAG);
                        _server->session.magic = MAGIC_ENUM_RESPONSE_LEGACY;
                        out->setData((unsigned8*)&(_server->session),SESSION_PACKET_1);
                        out->send(true);    // urgent message
                        }
#endif
                    }
                _server->leaveUsr();
                }
            break;

        case MAGIC_CREATE_PLAYER:           // create player
            if ( msg->getLength() == sizeof(CreatePlayerPacket) ) {
                CreatePlayerPacket *cpp = (CreatePlayerPacket*)msg->getData();
                _server->enterUsr();        // lock the user, session etc. structures for a long time..
                if ( cpp->magicApplication != _server->magicApp ) {
                    _server->leaveUsr();
                    break;
                    }
                ConnectResult result;
                    // check new player's attributes:
                if ( cpp->actualVersion < _server->session.requiredVersion ||
                     _server->session.actualVersion < cpp->requiredVersion ||
                     (_server->session.equalModRequired & 1) && stricmp(_server->session.mod, cpp->mod) != 0)
                    result = CRVersion;
                else
                    if ( strncmp(cpp->password,_server->_password,LEN_PASSWORD_NAME) != 0 )
                        result = CRPassword;
                    else
                        result = CROK;
                    // create a new player:
                int player = 0;             // dummy player ID
                int playerEnd;
                NetChannel *ch = NULL;
                if ( result == CROK ) {
                    poolLock.enter();
                    NetPeer *peer = getServerPeer();
                    Assert( peer );
                    RefD<NetChannel> findCh;
                    ch = peer->findChannel(distant);
                    if ( ch ) {             // this channel is being used!
                        player = cpp->uniqueID;
                        playerEnd = player - UNIQUE_TO_TRY;
                        do
                            if ( _server->users.get(player,findCh) && (NetChannel*)findCh == ch ) break;
                        while ( --player != playerEnd );
                        if ( player != playerEnd ) { // found => refuse it!
                            poolLock.leave();
                            _server->leaveUsr();
                            break;          // duplicate CreatePlayer message => refuse it
                            }
                            // new connection from the same IP:port => kick off the old player
                        _server->finishDestroyPlayer(_server->channelToPlayer(ch));
                        ch = NULL;
                        poolLock.leave();
                        _server->leaveUsr();
                        SLEEP_MS(DESTROY_WAIT); // waiting till my boss destroys the old player (to be sure)..
                        _server->enterUsr();
                        poolLock.enter();
                        }
                        // creating a new player: first determine its ID..
                    player = cpp->uniqueID;
                    playerEnd = player + UNIQUE_TO_TRY;
                    while ( player != playerEnd &&
                            _server->users.get(player,findCh) ) player++;
                    if ( player == playerEnd ||
                         _server->session.maxPlayers > 0 &&
                         _server->users.card() + 2 > _server->session.maxPlayers )
                        result = CRSessionFull;
                    else {                  // OK <= new player (at least new FlashPoint run)
                        ch = peer->getPool()->createChannel(distant,peer);
                        if ( !ch ) {
                            result = CRError; // undetermined error
                            RptF("NetServer: createChannel() failed => cannot insert a new player");
                            }
                        else {              // NetChannel is OK
#ifdef NET_LOG_SERVER
#  ifdef NET_LOG_BRIEF
                            NetLog("Ch(%u):acc(%u.%u.%u.%u:%u,'%s',%d,%d)",
                                   ch->getChannelId(),(unsigned)IP4(distant),(unsigned)IP3(distant),(unsigned)IP2(distant),(unsigned)IP1(distant),(unsigned)PORT(distant),
                                   cpp->name,cpp->uniqueID,player);
#  else
                            NetLog("Channel(%u): new player was accepted (from %u.%u.%u.%u:%u), name = '%s', sentID = %d, validID = %d",
                                   ch->getChannelId(),(unsigned)IP4(distant),(unsigned)IP3(distant),(unsigned)IP2(distant),(unsigned)IP1(distant),(unsigned)PORT(distant),
                                   cpp->name,cpp->uniqueID,player);
#  endif
#endif
                            _server->users.put(player,ch);
                            ChannelSupport sup;
                            sup.m_id = player;
                            _server->m_support.put(sockaddrKey(distant),sup);
                            //_server->logUsers();
                            DoAssert(_server->users.checkIntegrity());
                            int index = _server->_createPlayers.Add();
                            CreatePlayerInfo &info = _server->_createPlayers[index];
                            info.player = player;
                            if ( (info.botClient = (cpp->botClient != 0)) )
                                _server->botId = player;
                            strncpy(info.name,cpp->name,sizeof(info.name));
                            info.name[sizeof(info.name)-1] = (char)0;
                            if ( strcmp(cpp->name,info.name) )
                                RptF("NetServer: name of a new player is too long => truncating to '%S'",info.name);
                            RptF("NetServer: new player (waiting for ProcessPlayers) - session.numPlayers=%d, playerId=%d, bot=%d, name='%s', |users|=%u",
                                 _server->session.numPlayers,info.player,(int)info.botClient,info.name,_server->users.card());
                            strncpy(info.mod,cpp->mod,sizeof(info.mod));
                            info.mod[sizeof(info.mod)-1] = (char)0;
                            ch->setProcessRoutine(serverReceive);
                            }
                        }
                    poolLock.leave();
                    }
                _server->leaveUsr();
                    // .. and finally send acknowledgement back to the new user
                AckPlayerPacket app;
                app.magic = MAGIC_ACK_PLAYER;
                app.result = result;
                app.playerNo = player;      // for future reconnections..
                Ref<NetMessage> out = NetMessagePool::pool()->newMessage(sizeof(AckPlayerPacket),ch ? ch : msg->getChannel());
                if ( out ) {
                    if ( !ch ) out->setDistant(distant);
                    out->setFlags(MSG_ALL_FLAGS,MSG_MAGIC_FLAG|(ch?MSG_VIM_FLAG:MSG_FROM_BCAST_FLAG));
                    out->setData((unsigned8*)&app,sizeof(app));
                    out->send(true);        // urgent message
                    }
                if ( ch )                   // initial hand-shake (to initialize band-width..).
                    ch->checkConnectivity(0);
                }
            break;

        case MAGIC_RECONNECT_PLAYER:        // reconnect player
            if ( msg->getLength() == sizeof(ReconnectPlayerPacket) ) {
                ReconnectPlayerPacket *rpp = (ReconnectPlayerPacket*)msg->getData();
                _server->enterUsr();
                RefD<NetChannel> ch;
                ConnectResult result = CRError;
                if ( _server->users.get(rpp->playerNo,ch) &&  // player with this ID exists => reconnect it
                     ch->reconnect(distant) == nsOK ) result = CROK;
                _server->leaveUsr();
                    // .. send either acknowledgement back to the user
                AckPlayerPacket app;
                app.magic = MAGIC_ACK_PLAYER;
                app.result = result;
                app.playerNo = rpp->playerNo;
                Ref<NetMessage> out = NetMessagePool::pool()->newMessage(sizeof(AckPlayerPacket),(result==CROK) ? (NetChannel*)ch : msg->getChannel());
                if ( out ) {
                    if ( result != CROK ) out->setDistant(distant);
                    out->setFlags(MSG_ALL_FLAGS,MSG_MAGIC_FLAG|((result==CROK)?MSG_VIM_FLAG:MSG_FROM_BCAST_FLAG));
                    out->setData((unsigned8*)&app,sizeof(app));
                    out->send(true);        // urgent message
                    }
                if ( result == CROK )       // initial hand-shake (to re-initialize band-width..).
                    ch->checkConnectivity(0);
                }
            break;

        }

    return nsNoMoreCallbacks;
}

/// Control (broadcast) data received by the enumerator and client...
NetStatus enumReceive ( NetMessage *msg, NetStatus event, void *data )
{
        // thread-critical: sprintf(), AutoArray::Add(), strncpy()
    if ( (!_enum || !_enum->_running) && !_client ||
         !msg || msg->getLength() < 4 || !(msg->getFlags() & MSG_MAGIC_FLAG) )
        return nsNoMoreCallbacks;           // fatal message error

    unsigned32 magic = *(unsigned32*)msg->getData();
    struct sockaddr_in distant;
    msg->getDistant(distant);
    unsigned32 ip = ntohl(distant.sin_addr.s_addr);
    unsigned16 port = ntohs(distant.sin_port);

#ifdef NET_LOG_ENUM_RECEIVE
    NetLog("Peer(%u)::enumReceive: received control message (from=%u.%u.%u.%u:%u, len=%3u, serial=%4u, flags=%04x, magic=%x)",
           getClientPeer()->getPeerId(),
           (unsigned)IP4(distant),(unsigned)IP3(distant),(unsigned)IP2(distant),(unsigned)IP1(distant),(unsigned)PORT(distant),
           msg->getLength(),msg->getSerial(),(unsigned)msg->getFlags(),magic);
#endif

    switch ( magic ) {

        case MAGIC_ENUM_RESPONSE:           // enumeration response
          {
            if ( msg->getLength() != SESSION_PACKET_2 &&
                 msg->getLength() != SESSION_PACKET_3 )
              break; // wrong size

              // default mod required:
            char mod[MOD_LENGTH];
            mod[0] = 0;
            bool equalModRequired = false;
            if ( msg->getLength() >= SESSION_PACKET_3 )
            {
              strncpy(mod, ((SessionPacket *)msg->getData())->mod, MOD_LENGTH);
              mod[MOD_LENGTH-1] = (char)0;
              equalModRequired = (((SessionPacket *)msg->getData())->equalModRequired & 1) != 0;
            }

            SessionPacket *s = (SessionPacket *)msg->getData();
            {
                _enum->enter();

                    // search for existing session record
                int iFound = -1;
                for ( int i = 0; i < _enum->_sessions.Size(); i++ )
                    if ( _enum->_sessions[i].ip == ip && _enum->_sessions[i].port == port ) {
                        iFound = i;
                        break;
                        }
    
                unsigned64 reqTime = msg->getChannel()->getMessageTime(s->request);
                    // actual ping time in milliseconds:
                unsigned pingTime = reqTime ? (unsigned)( (msg->getTime() - reqTime) / 1000 ) : 0;

                if ( iFound < 0 ) {                     // not found
                    iFound = _enum->_sessions.Add();
                    if ( iFound < 0 ) {                 // too much sessions encountered
                        _enum->leave();
                        return nsNoMoreCallbacks;
                        }
                    NetSessionDescription &ndesc = _enum->_sessions[iFound];
                    ndesc.ip       = ip;
                    ndesc.port     = port;
                    ndesc.pingTime = pingTime;
                    sprintf(ndesc.address,"%s:%u",inet_ntoa(distant.sin_addr),(unsigned)port);
                    }

                NetSessionDescription &desc = _enum->_sessions[iFound];
                sprintf(desc.name,s->name,desc.address);
                desc.actualVersion   = s->actualVersion;
                desc.requiredVersion = s->requiredVersion;
                strncpy(desc.mission,s->mission,LEN_MISSION_NAME);
                desc.mission[LEN_MISSION_NAME-1] = (char)0;
                strncpy(desc.mod,mod,MOD_LENGTH);
                desc.mod[MOD_LENGTH-1] = (char)0;
                desc.equalModRequired = equalModRequired ? 1 : 0;
                desc.gameState       = s->gameState;
                desc.maxPlayers      = s->maxPlayers;
                desc.numPlayers      = s->numPlayers;
                desc.password        = s->password;
                desc.lastTime        = (unsigned32)(getSystemTime() / 1000);
                desc.pingTime        = (3*desc.pingTime + pingTime + 2) >> 2;

                _enum->leave();
                }
          }
          break;

        }

    return nsNoMoreCallbacks;
}

//---------------------------------------------------------------------------
//  NetSessionEnum:

NetSessionEnum::NetSessionEnum ()
    : LockInit(_cs,"NetSessionEnum::_cs",true)
{
    _running = false;
    magicApp = 0;
    setEnum(this);
#ifdef NET_LOG_SESSION_ENUM
    NetLog("Peer(%u): creating NetSessionEnum instance",getClientPeer()->getPeerId());
#endif
}

NetSessionEnum::~NetSessionEnum ()
{
    setEnum(NULL);
    Done();
#ifdef NET_LOG_SESSION_ENUM
    NetLog("Peer(%u): destroying NetSessionEnum instance [%08x]",getClientPeer()->getPeerId());
#endif
    checkPool();
}

RString NetSessionEnum::IPToGUID ( RString ip, int port )
{
    char buffer[256];
    sprintf(buffer,"%s:%d",(const char*)ip,port);
    return buffer;
}

bool NetSessionEnum::Init ( int magic )
{
#ifdef NET_LOG_SESSION_ENUM
    NetLog("Peer(%u): NetSessionEnum::Init(%d)",
           getClientPeer()->getPeerId(),magic);
#endif
    magicApp = magic;
    setEnum(this);
    return true;
}

void NetSessionEnum::Done ()
{
    enter();
    StopEnumHosts();
    _sessions.Clear();
    leave();
}

bool NetSessionEnum::needsRequest ( struct sockaddr_in &addr, unsigned16 port )
{
    bool needs = true;
    enter();
    unsigned32 ip = ntohl(addr.sin_addr.s_addr);
    for ( int i = 0; i < _sessions.Size(); i++ ) {
        const NetSessionDescription &src = _sessions[i];
        if ( src.ip == ip && src.port >= port && src.port < port + NUM_PORTS_TO_TRY * PORT_INTERVAL &&
             (src.port - port) % PORT_INTERVAL == 0 ) {
            unsigned32 now = (unsigned32)(getSystemTime() / 1000);
            if ( (now - src.lastTime) < MIN_ENUM_RETRY )
                needs = false;
            break;
            }
        }
    leave();
    return needs;
}

void NetSessionEnum::sendRequest ( NetChannel *br, struct sockaddr_in &addr, unsigned16 port )
{
    Assert( br );
    unsigned16 p;
    EnumPacket request;
    request.magic = MAGIC_ENUM_REQUEST;
    request.magicApplication = magicApp;
    int i;

    for ( p = port, i = 0; i++ < NUM_PORTS_TO_TRY; p += PORT_INTERVAL ) {
        addr.sin_port = htons(p);
        Ref<NetMessage> msg = NetMessagePool::pool()->newMessage(sizeof(EnumPacket),br);
        if ( msg ) {
            msg->setDistant(addr);
            msg->setFlags(MSG_ALL_FLAGS,MSG_TO_BCAST_FLAG|MSG_MAGIC_FLAG);
            msg->setData((unsigned8*)&request,sizeof(EnumPacket));
            msg->send();
            }
        }
}

bool NetSessionEnum::StartEnumHosts ( RString ip, int port, AutoArray<RemoteHostAddress> *hosts )
{                                           // don't need to lock this..
    _running = true;
    bool anything = false;
    setEnum(this);
#ifdef NET_LOG_START_ENUM
    NetLog("Peer(%u): NetSessionEnum::StartEnumHosts(%s,%u,%d)",
           getClientPeer()->getPeerId(),(const char*)ip,(unsigned)port,hosts?hosts->Size():0);
#endif
    NetChannel *br = getClientPeer()->getBroadcastChannel();
    Assert( br );
    struct sockaddr_in addr;
    RString ipaddr;

    if ( !ip.GetLength() ) {                // if "ip" is not defined, try LAN broadcast first ..
        getHostAddress(addr,NULL,port);
        if ( needsRequest(addr,port) ) {
            sendRequest(br,addr,port);      // broadcast
            anything = true;
            }
        getLocalAddress(addr,port);
        if ( needsRequest(addr,port) ) {
            sendRequest(br,addr,port);      // localhost (for XP)
            anything = true;
            }
        }
    else {
        decodeURLAddress(ip,ipaddr,port);
        if ( getHostAddress(addr,ipaddr,port) && // .. else try explicit IP address
             needsRequest(addr,port) ) {
            sendRequest(br,addr,port);
            anything = true;
            }
        }
    
    int i;
    if ( hosts )                            // .. and finally try all addresses from the given list
        for ( i = 0; _running && i < hosts->Size(); i++ ) {
            const RemoteHostAddress &address = hosts->Get(i);
            port = address.port;
            decodeURLAddress(address.ip,ipaddr,port);
            if ( getHostAddress(addr,ipaddr,port) &&
                 needsRequest(addr,port) ) {
                sendRequest(br,addr,port);
                anything = true;
                } 
            }
    
    return anything;
}

void NetSessionEnum::StopEnumHosts ()
{
#ifdef NET_LOG_STOP_ENUM
    NetLog("Peer(%u): NetSessionEnum::StopEnumHosts",getClientPeer()->getPeerId());
#endif
    _running = false;
}

int NetSessionEnum::NSessions ()
{
    enter();
    int n = _sessions.Size();
    leave();
#ifdef NET_LOG_NSESSIONS
    NetLog("Peer(%u): NetSessionEnum::NSessions: N=%d",getClientPeer()->getPeerId(),n);
#endif
    return n;
}

void NetSessionEnum::GetSessions ( AutoArray<SessionInfo> &sessions )
{
    enter();
//LogF("GetSessions: Current time %d", GetTickCount());

    int i;
    int n = _sessions.Size();
    for ( i = 0; i < n; ) {
        const NetSessionDescription &desc = _sessions[i];
        unsigned32 age = (unsigned32)(getSystemTime() / 1000) - desc.lastTime;
        if ( age > MAX_ENUM_AGE ) {         // message age in milliseconds
//LogF("Session %s deleted - age %d", desc.address, age);
						_sessions.Delete(i);
            n--;
            }
        else
            i++;
        }   
    
    sessions.Resize(n);
    for ( i = 0; i < n; i++ ) {
        const NetSessionDescription &src = _sessions[i];
        SessionInfo &dst = sessions[i];
        dst.guid               = src.address;
        dst.name               = src.name;
        dst.lastTime           = GetTickCount() - ((unsigned32)(getSystemTime() / 1000) - src.lastTime);
/*LogF
(
	"Session %s listed - age %d, last time %d",
	src.address, (unsigned32)(getSystemTime() / 1000) - src.lastTime, dst.lastTime
);*/
        dst.password           = (src.password != 0);
        dst.actualVersion      = src.actualVersion;
        dst.requiredVersion    = src.requiredVersion;
        dst.badActualVersion   = false;
        dst.badRequiredVersion = false;
        dst.gameState          = src.gameState;
        dst.mission            = src.mission;
        dst.numPlayers         = src.numPlayers;
        dst.maxPlayers         = src.maxPlayers;
        dst.ping               = src.pingTime;
        dst.timeleft           = 0;

        dst.mod = src.mod;
        dst.equalModRequired = (src.equalModRequired & 1) != 0;
        dst.badMod = false;
        }

    leave();
}

//---------------------------------------------------------------------------
//  NetClient:

NetClient::NetClient ()
    : LockInit(sndCs,"NetClient::sndCs",true),
      LockInit(rcvCs,"NetClient::rcvCs",true)
{
        // Message times:
    lastMsgReported = getSystemTime();
        // NetChannel: not set yet
    channel = NULL;
        // to be sure:
    received = NULL;
    split = lastSplit = NULL;
    splitUrgent = lastSplitUrgent = NULL;
    sent = NULL;
    sessionTerminated = false;
    whySessionTerminated = NTROther;
    magicApp = 0;
#ifdef NET_LOG_INFO
    oldLatency = -1;
    oldThroughput = -1;
    printAge = false;
#endif
#ifdef NET_LOG_TRANSP_STAT
    nextStatLog = lastMsgReported;
#endif
    setClient(this);
#ifdef NET_LOG_CLIENT
#  ifdef NET_LOG_BRIEF
    NetLog("Pe(%u):cli",getClientPeer()->getPeerId());
#  else
    NetLog("Peer(%u): creating NetClient instance",getClientPeer()->getPeerId());
#  endif
#endif
}

NetClient::~NetClient ()
{
    enterSnd();
    if ( !sessionTerminated ) {  // I must notify my server..
        sessionTerminated = true;
        whySessionTerminated = NTRDisconnected;
            // send MAGIC_DESTROY_PLAYER message to server:
        if ( channel ) {
            Ref<NetMessage> out = NetMessagePool::pool()->newMessage(4,channel);
            if ( out ) {
                unsigned32 magic = MAGIC_DESTROY_PLAYER;
                out->setFlags(MSG_ALL_FLAGS,MSG_MAGIC_FLAG);
                out->setData((unsigned8*)&magic,4);
                out->send(true);
                SLEEP_MS(DESTRUCT_WAIT);
                }
            }
        }

    setClient(NULL);
        // recycle all pending NetMessages:
    RemoveUserMessages();
    RemoveSendComplete();
        // destroy the NetChannel:
#ifdef NET_LOG_CLIENT
#  ifdef NET_LOG_BRIEF
    NetLog("Ch(%u):~cli",
           channel?channel->getChannelId():0);
#  else
    NetLog("Channel(%u): destroying NetClient instance",
           channel?channel->getChannelId():0);
#  endif
#endif
    if ( channel ) {
        NetChannel *old = channel;
        channel = NULL;
        leaveSnd();
        getPool()->deleteChannel(old);
        }
    else
        leaveSnd();
    checkPool();
		FreeMemory();
}

bool NetClient::InitVoice ()
{
    return true;
}

bool NetClient::IsVoicePlaying ( int player )
{
    return false;
}

bool NetClient::IsVoiceRecording ()
{
    return false;
}

NetTranspSound3DBuffer *NetClient::Create3DSoundBuffer ( int player )
{
    return NULL;
}

NetStatus clientReceive ( NetMessage *msg, NetStatus event, void *data )
{
        // thread-safe
    if ( !_client || !msg ) return nsNoMoreCallbacks; // fatal error
    _client->lastMsgReported = msg->getTime();
    unsigned len = msg->getLength();
    if ( !len ) return nsNoMoreCallbacks;   // message is too small
#if defined(NET_LOG_CLIENT_RECEIVE) || defined(NET_LOG_MERGE)
    NetChannel *channel = (NetChannel*)data;
    Assert( channel );
#endif
    unsigned flags = msg->getFlags();
    bool isMagic = (flags & MSG_MAGIC_FLAG) != 0;
    if ( isMagic ) {                        // magic-messages:
        if ( len < 4 )
            return nsNoMoreCallbacks;       // magic-message is too small
        unsigned32 magic = *(unsigned32*)msg->getData();
#ifdef NET_LOG_CLIENT_RECEIVE
        NetLog("Channel(%u)::clientReceive: received control message (len=%3u, serial=%4u, flags=%04x, magic=%x)",
               channel->getChannelId(),len,msg->getSerial(),flags,magic);
#endif

        switch ( magic ) {

            case MAGIC_ACK_PLAYER:          // AckPlayer (in case of success)
                if ( len == sizeof(AckPlayerPacket) ) {
                    AckPlayerPacket *app = (AckPlayerPacket*)msg->getData();
                    _client->enterSnd();
                    if ( _client->ackPlayer == CRNone ) { // refuse duplicate messages
                        _client->playerNo = app->playerNo;
                        _client->ackPlayer = app->result;
                        }
                    _client->leaveSnd();
                    }
                break;                      // don't need to lock this..

            case MAGIC_TERMINATE_SESSION:   // Session terminate
                // check additional data (if present)
                NetTerminationReason reason = NTROther;
                if ( len >= 2*sizeof(unsigned32) )
                    reason = (NetTerminationReason)((unsigned32*)msg->getData())[1];
                _client->enterSnd();
                _client->sessionTerminated = true;
                _client->whySessionTerminated = reason;
                _client->leaveSnd();
                break;

            }

        return nsNoMoreCallbacks;
        }

        // process regular (user) message:
#ifdef NET_LOG_CLIENT_RECEIVE
    NetLog("Channel(%u)::clientReceive: received user message (len=%3u, serial=%4u, flags=%04x, ID=%x)",
           channel->getChannelId(),len,msg->getSerial(),flags,msg->id);
#endif
    _client->enterRcv();
        // merge partial messages:
    if ( flags & MSG_PART_FLAG ) {
        bool closing = (flags & MSG_CLOSING_FLAG) > 0;
        if ( flags & MSG_URGENT_FLAG )
            if ( closing ) {                // the last message-part to be merged..
                DoAssert( _client->splitUrgent.NotNull() );
                _client->lastSplitUrgent->next = msg;
                msg->next = NULL;
                msg = mergeMessageList(_client->splitUrgent);
                _client->splitUrgent = NULL;
                }
            else {                          // another message-part => remember it!
                if ( !_client->splitUrgent )
                    _client->splitUrgent = msg;
                else
                    _client->lastSplitUrgent->next = msg;
                (_client->lastSplitUrgent = msg)->next = NULL;
                _client->leaveRcv();
                return nsNoMoreCallbacks;
                }
        else
            if ( closing ) {                // the last message-part to be merged..
                DoAssert( _client->split.NotNull() );
                _client->lastSplit->next = msg;
                msg->next = NULL;
                msg = mergeMessageList(_client->split);
                _client->split = NULL;
                }
            else {                          // another message-part => remember it!
                if ( !_client->split )
                    _client->split = msg;
                else
                    _client->lastSplit->next = msg;
                (_client->lastSplit = msg)->next = NULL;
                _client->leaveRcv();
                return nsNoMoreCallbacks;
                }
#ifdef NET_LOG_MERGE
#  ifdef NET_LOG_BRIEF
        NetLog("Ch(%u):rMerC(%u,%u,%x,%x)",
               channel->getChannelId(),msg->getLength(),msg->getSerial(),(unsigned)msg->getFlags(),msg->id);
#  else
        NetLog("Channel(%u)::clientReceive: merging user message (len=%3u, serial=%4u, flags=%04x, ID=%x)",
               channel->getChannelId(),msg->getLength(),msg->getSerial(),(unsigned)msg->getFlags(),msg->id);
#  endif
#endif
        }

    _client->insertReceived(msg);

    _client->leaveRcv();

    return nsNoMoreCallbacks;
}

ConnectResult NetClient::ReInit ()
{
    enterSnd();
    if ( !channel ) {                       // not connected => error
        leaveSnd();
        return CRError;
        }
        // put together the "ReconnectPlayer" message:
    ReconnectPlayerPacket packet;
    packet.magic = MAGIC_RECONNECT_PLAYER;
    packet.playerNo = playerNo;
        // .. and send it to the server:
    ackPlayer = CRNone;
    // server will receive this message via it's control channel so I must not use VIM flag!
    // I'll send it multiple times if necessary..

        // now I have to wait to server's response..
    unsigned64 now = getSystemTime();
    unsigned64 next = now;                  // re-send time (init = the 1st try)
    unsigned64 timeout = now + 1000 * ACK_PLAYER_TIMEOUT;
    do {
        if ( now >= next ) {
            Ref<NetMessage> msg = NetMessagePool::pool()->newMessage(sizeof(packet),channel);
            if ( !msg ) return CRError;
            msg->setFlags(MSG_ALL_FLAGS,MSG_TO_BCAST_FLAG|MSG_MAGIC_FLAG);
            msg->setData((unsigned8*)&packet,sizeof(packet));
            msg->send(true);                // urgent
            next = now + 1000 * CREATE_PLAYER_RESEND;
            }
        leaveSnd();
        SLEEP_MS(NET_CHECK_WAIT);
        enterSnd();
        now = getSystemTime();
        } while ( ackPlayer == CRNone && now < timeout );


    if ( ackPlayer != CRNone )              // initial hand-shake (to re-initialize band-width)
        channel->checkConnectivity(0);

#ifdef NET_LOG_CLIENT
#  ifdef NET_LOG_BRIEF
    NetLog("Ch(%u):cli-re(%s,%.3f,%d)",
           channel->getChannelId(),(ackPlayer == CRNone)?"failed":"reconnected",
           1.e-6*(now-timeout+1000*ACK_PLAYER_TIMEOUT),playerNo);
#  else
    NetLog("Channel(%u): NetClient::ReInit: %s after %.3f seconds (playerID=%d)",
           channel->getChannelId(),(ackPlayer == CRNone)?"failed":"reconnected",
           1.e-6*(now-timeout+1000*ACK_PLAYER_TIMEOUT),playerNo);
#  endif
#endif
    ConnectResult result = (ackPlayer == CRNone) ? CRError : (ConnectResult)ackPlayer;
    leaveSnd();
    return result;
}

ConnectResult NetClient::Init ( RString address, RString password, bool botClient, int &port,
                                RString player, MPVersionInfo &versionInfo, int magic )
{
#ifdef NET_LOG_CLIENT
#  ifdef NET_LOG_BRIEF
    NetLog("Cli(%s,%d)",
           address.GetLength()?(const char*)address:"none",port);
#  else
    NetLog("NetClient::Init: address=%s, port=%d",
           address.GetLength()?(const char*)address:"none",port);
#  endif
#endif
    enterSnd();
    if ( channel ) {                        // already connected => error
        leaveSnd();
        return CRError;
        }
    magicApp = magic;
    RString ip;
    struct sockaddr_in daddr;
    if ( address.GetLength() == 0 ) {       // use "localhost"
        char buf[64];
        sprintf(buf,"127.0.0.1:%d",port);
        address = buf;
        }
    decodeURLAddress(address,ip,port);
    if ( !getHostAddress(daddr,(const char*)ip,port) ) {
        leaveSnd();
        return CRError;
        }
        // now "daddr" contains valid IP address..
    sessionTerminated = false;
    whySessionTerminated = NTROther;

        // create a new NetChannel for client <-> server communication:
    poolLock.enter();
    if ( getPool() )
        channel = getPool()->createChannel(daddr,getClientPeer());
    if ( !channel ) {
        poolLock.leave();
        leaveSnd();
        return CRError;
        }
    poolLock.leave();

    channel->setProcessRoutine(clientReceive,channel);
    
    amIBot = botClient;

        // put together the "CreatePlayer" message:
    CreatePlayerPacket packet;
    packet.magic = MAGIC_CREATE_PLAYER;
    packet.magicApplication = magicApp;
    strncpy(packet.name,player,LEN_PLAYER_NAME);
    packet.name[LEN_PLAYER_NAME-1] = (char)0;
    strncpy(packet.password,password,LEN_PASSWORD_NAME);
    packet.password[LEN_PASSWORD_NAME-1] = (char)0;
    packet.actualVersion = versionInfo.versionActual;
    packet.requiredVersion = versionInfo.versionRequired;
    strncpy(packet.mod,versionInfo.mod,MOD_LENGTH);
    packet.mod[MOD_LENGTH-1] = (char)0;
    packet.botClient = botClient ? 1 : 0;
    packet.uniqueID = (int32)(getSystemTime() & 0x7fffffff) + UNIQUE_TO_TRY;
#ifdef NET_LOG_CLIENT
#  ifdef NET_LOG_BRIEF
    NetLog("Ch(%u):cli(%u.%u.%u.%u:%u,'%s',%d,%d)",
           channel->getChannelId(),
           (unsigned)IP4(daddr),(unsigned)IP3(daddr),(unsigned)IP2(daddr),(unsigned)IP1(daddr),(unsigned)PORT(daddr),
           (const char*)player,botClient?1:0,packet.uniqueID);
#  else
    NetLog("Channel(%u): NetClient::Init: server=%u.%u.%u.%u:%u, player='%s', bot=%d, magic=%d, playerID=%d",
           channel->getChannelId(),
           (unsigned)IP4(daddr),(unsigned)IP3(daddr),(unsigned)IP2(daddr),(unsigned)IP1(daddr),(unsigned)PORT(daddr),
           (const char*)player,botClient?1:0,magic,packet.uniqueID);
#  endif
#endif
        // .. and send it to the server:
    ackPlayer = CRNone;
    // server will receive this message via it's control channel so I must not use VIM flag!
    // I'll send it multiple times if necessary..

        // now I have to wait to server's response..
    unsigned64 now = getSystemTime();
    unsigned64 next = now;                  // re-send time (init = the 1st try)
    unsigned64 timeout = now + 1000 * ACK_PLAYER_TIMEOUT;
    do {
        if ( now >= next ) {
            Ref<NetMessage> msg = NetMessagePool::pool()->newMessage(sizeof(packet),channel);
            if ( !msg ) {
                leaveSnd();
                return CRError;
                }
            msg->setFlags(MSG_ALL_FLAGS,MSG_TO_BCAST_FLAG|MSG_MAGIC_FLAG);
            msg->setData((unsigned8*)&packet,sizeof(packet));
            msg->send(true);                // urgent
            next = now + 1000 * CREATE_PLAYER_RESEND;
            }
        leaveSnd();
        SLEEP_MS(NET_CHECK_WAIT);
        enterSnd();
        now = getSystemTime();
        } while ( ackPlayer == CRNone && now < timeout );

    if ( ackPlayer != CRNone ) {            // initial hand-shake (to initialize band-width)
        channel->checkConnectivity(0);
        struct sockaddr_in distant;
        channel->getDistantAddress(distant);
        port = ntohs(distant.sin_port);
        }

#ifdef NET_LOG_CLIENT
#  ifdef NET_LOG_BRIEF
    NetLog("Ch(%u):cli(%s,%.3f,%d)",
           channel->getChannelId(),
           (ackPlayer == CRNone)?"failed":"connected",1.e-6*(now-timeout+1000*ACK_PLAYER_TIMEOUT),playerNo);
#  else
    NetLog("Channel(%u): NetClient::Init: %s after %.3f seconds (playerID=%d)",
           channel->getChannelId(),
           (ackPlayer == CRNone)?"failed":"connected",1.e-6*(now-timeout+1000*ACK_PLAYER_TIMEOUT),playerNo);
#  endif
#endif
    ConnectResult result = (ackPlayer == CRNone) ? CRError : (ConnectResult)ackPlayer;
    leaveSnd();
    return result;
}

NetStatus clientSendComplete ( NetMessage *msg, NetStatus event, void *data )
    // data -> NetClient instance
    // thread-safe
{
    NetClient *client = (NetClient*)data;
    if ( !client || !msg || client->sessionTerminated ) return nsNoMoreCallbacks;    // fatal error
#ifdef NET_LOG_CLIENT_COMPLETE
    NetLog("Channel(%u)::clientSendComplete: status=%d, len=%3u, serial=%4u, flags=%04x, msgId=%x",
           client->channel->getChannelId(),(int)msg->getStatus(),msg->getLength(),
           msg->getSerial(),(unsigned)msg->getFlags(),msg->id);
#endif
    client->enterSnd();
    msg->next = client->sent;
    client->sent = msg;
    client->leaveSnd();
    return nsNoMoreCallbacks;
}

/*!
\patch_internal 1.58 Date 5/20/2002 by Pepca
- Fixed: Too large messages are now splitted automatically in NetTransp* layer. [Sockets]
*/

bool NetClient::SendMsg ( BYTE *buffer, int bufferSize, DWORD &msgID, NetMsgFlags flags )
{
    bool vim = (flags & NMFGuaranteed) > 0;
    bool urgent = (flags & NMFHighPriority) > 0;
    enterSnd();
    if ( !channel || !buffer || bufferSize <= 0 ) {
        leaveSnd();
        return false;
        }
    int maxMessage = channel->maxMessageData();
    if ( !vim && bufferSize > maxMessage ) {
        leaveSnd();
#ifdef NET_LOG_MERGE
#  ifdef NET_LOG_BRIEF
        NetLog("Ch(%u):tooLargeC(%d,%x)",
               channel->getChannelId(),bufferSize,(unsigned)flags);
#  else
        NetLog("Channel(%u): NetClient::SendMsg: trying to send too large non-guaranteed message (len=%3d, flags=%x)",
               channel->getChannelId(),bufferSize,(unsigned)flags);
#  endif
#endif
        RptF("NetClient: trying to send too large non-guaranteed message (%d bytes long)",bufferSize);
        return false;
        }
    Ref<NetMessage> msg;
    if ( vim ) {                            // guaranteed message
        unsigned fl = MSG_VIM_FLAG | (urgent ? MSG_URGENT_FLAG : 0);
        if ( bufferSize > maxMessage ) {    // split too big message:
#ifdef NET_LOG_MERGE
#  ifdef NET_LOG_BRIEF
            NetLog("Ch(%u):sMerC(%d,%x)",
                   channel->getChannelId(),bufferSize,(unsigned)flags);
#  else
            NetLog("Channel(%u): NetClient::SendMsg: splitting user message (len=%3d, flags=%x)",
                   channel->getChannelId(),bufferSize,(unsigned)flags);
#  endif
#endif
            int toSent = bufferSize;
            int packet;
            fl |= MSG_PART_FLAG;
            do {
                packet = (toSent > maxMessage) ? maxMessage : toSent;
                toSent -= packet;
                msg = NetMessagePool::pool()->newMessage(packet,channel);
                if ( !msg ) {
                    leaveSnd();
                    return false;
                    }
                msg->setFlags(MSG_ALL_FLAGS,fl | (toSent ? 0 : MSG_CLOSING_FLAG));
                msg->setOrderedPrevious();
                msg->setData((unsigned8*)buffer,packet);
                buffer += packet;
                msg->send(urgent);
                } while ( toSent );
            }
        else {                              // small message
            msg = NetMessagePool::pool()->newMessage(bufferSize,channel);
            msg->setFlags(MSG_ALL_FLAGS,fl);
            msg->setOrderedPrevious();
            msg->setData((unsigned8*)buffer,bufferSize);
            msg->send(urgent);
            }
        }
    else {                                  // common message
        msg = NetMessagePool::pool()->newMessage(bufferSize,channel);
        if ( !msg ) {
            leaveSnd();
            return false;
            }
        msg->setCallback(clientSendComplete,nsOutputSent,this);
        msg->setSendTimeout(SEND_TIMEOUT);
        msg->setData((unsigned8*)buffer,bufferSize);
        msg->send();
        }
    msgID = (DWORD)msg->id;
#ifdef NET_LOG_CLIENT_SEND
    NetLog("Channel(%u): NetClient::SendMsg: len=%3d, flags=%x, msgID=%x, hdr=%08x%08x",
           channel->getChannelId(),bufferSize,(unsigned)flags,msgID,
           ((unsigned*)msg->getData())[0],((unsigned*)msg->getData())[1]);
#endif
    leaveSnd();
    return true;
}

void NetClient::GetSendQueueInfo ( int &nMsg, int &nBytes, int &nMsgG, int &nBytesG )
{
    enterSnd();
    if ( channel )
        channel->getOutputQueueStatistics(nMsg,nBytes,nMsgG,nBytesG);
    else {
        nMsg = nBytes = nMsgG = nBytesG = 0;
        }
    leaveSnd();
}

bool NetClient::GetConnectionInfo ( int &latencyMS, int &throughputBPS )
{
    enterSnd();
    if ( !channel ) {
        leaveSnd();
        return false;
        }
    latencyMS     = (int)(channel->getLatency() / 1000);
    throughputBPS = (int)channel->getOutputBandWidth();;
#ifdef NET_LOG_INFO
    if ( latencyMS != oldLatency ||
         throughputBPS != oldThroughput ) {
        NetLog("Channel(%u): NetClient::GetConnectionInfo: %d ms,%d B/s",
               channel->getChannelId(),latencyMS,throughputBPS);
        oldLatency = latencyMS;
        oldThroughput = throughputBPS;
        printAge = true;
        }
#endif
    leaveSnd();
    return true;
}

template <class Type>
Type LoadValue
(
    const ParamEntry &cfg, const char *name, const Type &defVal
)
{
    const ParamEntry *entry = cfg.FindEntry(name);
    if (!entry)
    {
        return defVal;
    }
    else
    {
        return *entry;
    }
}

void NetClient::SetNetworkParams ( const ParamEntry &cfg )
{
    NetworkParams data;
    LoadNetworkParams(data,cfg);
    enterSnd();
    if ( !channel ) {
        leaveSnd();
        return;
        }
    channel->setNetworkParams(data);
    leaveSnd();
}

float NetClient::GetLastMsgAge ()
{
    enterSnd();
    float result = 1.0;
    if ( channel )
    {
        unsigned64 last = channel->getLastMessageArrival();
        unsigned64 now = getSystemTime();
        if ( now < last )
        {
            LogF("Negative time delta in NetClient::GetLastMsgAge: now = %8x%08x, last = %8x%08x",
                 (unsigned)(now>>32),(unsigned)(now&0xffffffff),
                 (unsigned)(last>>32),(unsigned)(last&0xffffffff));
            result = 0;
        }
        else
        {
            unsigned64 delta = now - last;
#if _ENABLE_REPORT
            if ( delta > 200000000 )
                RptF("Overflow in NetClient::GetLastMsgAge: now = %8x%08x, last = %8x%08x, delta = %.0f",
                     (unsigned)(now>>32),(unsigned)(now&0xffffffff),
                     (unsigned)(last>>32),(unsigned)(last&0xffffffff),
                     (double)(1.e-6f * delta));
#endif
            result = 1.e-6f * (unsigned)delta;
        }
    }
    leaveSnd();
    return result;
}

float NetClient::GetLastMsgAgeReported ()
{
    return( 1.e-6f * (getSystemTime() - lastMsgReported) );
}

void NetClient::LastMsgAgeReported ()
{
    lastMsgReported = getSystemTime();
}

/*!
\patch_internal 1.54 Date 4/5/2002 by Pepca
- Fixed: Bot-client cannot be disconnected by itself (due to line-drop). [Sockets]
*/

bool NetClient::IsSessionTerminated ()
{
    enterSnd();
    if ( !channel )
    {
        leaveSnd();
        return true;
    }
        // check NetChannel connectivity:
    if ( !amIBot && channel->dropped() )
    {
        sessionTerminated = true;
        whySessionTerminated = NTRTimeout;
    }
    leaveSnd();
    return sessionTerminated;
}

NetTerminationReason NetClient::GetWhySessionTerminated ()
{
    NetTerminationReason reason;
    enterSnd();
    reason = whySessionTerminated;

    leaveSnd();
    return reason;
}

/*!
\patch_internal 1.92 Date 7/31/2003 by Pepca
- Improved: less aggressive locking in ProcessUserMessages (both NetServer and NetClient)
  gives chance to receive further messages while the actual one is being processed.
\patch 1.92 Date 7/31/2003 by Pepca
- Improved: MP: faster response in MP-game setup (mission setup, roles assignment, connecting to server, ..)
*/

void NetClient::ProcessUserMessages ( UserMessageClientCallback *callback, void *context )
{
    if ( !callback ) return;
    Ref<NetMessage> msg;
    enterRcv();
#ifdef NET_LOG_CLIENT_PROCESS
    int n = 0;
    msg = received;
    while ( msg ) {
        n++;
        msg = msg->next;
        }
    if ( n ) {
        NetLog("Channel(%u): NetClient::ProcessUserMessages: processed %d messages:",channel->getChannelId(),n);
        msg = received;
        while ( msg ) {
            NetLog("Channel(%u): Processed: len=%3u, serial=%4u, flags=%04x, msgID=%x, hdr=%08x%08x",
                   channel->getChannelId(),msg->getLength(),msg->getSerial(),(unsigned)msg->getFlags(),msg->id,
                   ((unsigned*)msg->getData())[0],((unsigned*)msg->getData())[1]);
            msg = msg->next;
            }
        }
#endif
    while ( received )
    {
        msg = received;
        received = msg->next;
        msg->next = NULL;
        leaveRcv();
        (*callback)((char*)msg->getData(),msg->getLength(),context);
        enterRcv();
    }
    leaveRcv();
}

void NetClient::insertReceived ( NetMessage *msg )
    // must be called inside enterRcv()
{
    if ( received ) {
        MsgSerial s = msg->getSerial();
        if ( s < received->getSerial() ) {
            msg->next = received;
            received = msg;
            }
        else {
            NetMessage *ptr = received;
            while ( ptr->next && ptr->next->getSerial() < s )
                ptr = ptr->next;
            msg->next = ptr->next;
            ptr->next = msg;
            }
        }
    else {
        msg->next = NULL;
        received = msg;
        }
}

void NetClient::RemoveUserMessages ()
{
    enterRcv();
    Ref<NetMessage> tmp;
    while ( received ) {
        tmp = received->next;
        received->next = NULL;              // don't need this message anymore, somebody will recycle it later
        received = tmp;
        }
    leaveRcv();
}

void NetClient::ProcessSendComplete ( SendCompleteCallback *callback, void *context )
{
    if ( !callback ) return;
    enterSnd();
    NetMessage *msg;
#ifdef NET_LOG_CLIENT_COMPLETE
    int n = 0;
    msg = sent;
    while ( msg ) {
        n++;
        msg = msg->next;
        }
    if ( n ) {
        NetLog("Channel(%u): NetClient::ProcessSendComplete: processed %d messages:",channel->getChannelId(),n);
        msg = sent;
        while ( msg ) {
            NetLog("Channel(%u): Processed: ok=%d(%d), len=%3u, serial=%4u, flags=%04x, MsgID=%x, hdr=%08x%08x",
                   channel->getChannelId(),(int)(msg->getStatus()!=nsOutputObsolete),(int)msg->getStatus(),
                   msg->getLength(),msg->getSerial(),(unsigned)msg->getFlags(),msg->id,
                   ((unsigned*)msg->getData())[0],((unsigned*)msg->getData())[1]);
            msg = msg->next;
            }
        }
#endif
    msg = sent;
    while ( msg ) {
        (*callback)((DWORD)msg->id,msg->wasSent(),context);
        msg = msg->next;
        }
    RemoveSendComplete();
    leaveSnd();
}

void NetClient::RemoveSendComplete ()
{
    enterSnd();
    Ref<NetMessage> tmp;
    while ( sent ) {
        tmp = sent->next;
        sent->next = NULL;                  // don't need this message anymore, somebody will recycle it later
        sent = tmp;
        }
    leaveSnd();
}

/*!
\patch_internal 1.53 Date 4/24/2002 by Pepca
- Added: NetClient::GetStatistics, NetServer::GetStatistics - internal NetChannel
statistics in textual form. Alt+;
*/

#ifdef NET_LOG_TRANSP_STAT
static int getStatisticsCount = 0;
#endif

RString NetClient::GetStatistics ()
{
    enterSnd();
    if ( !channel ) {
        leaveSnd();
        return RString("No NetChannel is connected to this NetClient");
        }
    int latencyAve;
    unsigned latencyAct,latencyMin;
    int throughputAve;
    latencyAve = (int)(channel->getLatency(&latencyAct,&latencyMin) / 1000);
    latencyAct /= 1000;
    latencyMin /= 1000;
    EnhancedBWInfo enhanced;
    throughputAve = (int)channel->getOutputBandWidth(&enhanced);
    int nMsg, nBytes, nMsgG, nBytesG;
    channel->getOutputQueueStatistics(nMsg,nBytes,nMsgG,nBytesG);
    ChannelStatistics stat;                 // internal statistics of the NetChannel
    Zero(stat);
    channel->getInternalStatistics(stat);
    leaveSnd();
    char buf[256];
    bool kbps = (throughputAve < (9<<17));
    sprintf(buf,"ping%4dms(%4u,%4u) BW%c%c%c%5d%cb(%4u,%4u,%4u) lost%4.1f%%%%(%3u) queue%4dB(%4d) ackWait%3u(%3.1f,%3.1f)",
                latencyAve,latencyAct,latencyMin,(char)(enhanced.growMode+'c'),(char)(enhanced.growModePing+'c'),
                (char)(enhanced.growModeLost+'c'),kbps?((throughputAve+64)>>7):((throughputAve+65536)>>17),
                kbps?'K':'M',(enhanced.actBW+64)>>7,(enhanced.goodBW+64)>>7,(enhanced.sentBW+64)>>7,
                stat.ackTotal?(stat.ackLost*100.0)/stat.ackTotal:0.0,stat.ackLost,
                nBytes,nBytesG,stat.revisitedNo,1e-6*stat.revisitedAveAge,1e-6*stat.revisitedMaxAge);
#ifdef NET_LOG_TRANSP_STAT
    unsigned64 now = getSystemTime();
    if ( (!amIBot && now >= nextStatLog) || getStatisticsCount > 100 ) {
        NetLog("Channel(%u): NetClient(%d)[%d] - %s",channel->getChannelId(),playerNo,getStatisticsCount,buf);
        getStatisticsCount = 0;
        nextStatLog = now + STAT_LOG_INTERVAL;
        }
    else
        getStatisticsCount++;
#endif
    return RString(buf);
}

unsigned NetClient::FreeMemory ()
{
    if ( !NetMessagePool::pool() ) return 0;
    unsigned ret = NetMessagePool::pool()->freeMemory();
		SafeMemoryCleanUp();
    return ret;
}

//---------------------------------------------------------------------------
//  NetServer:

NetServer::NetServer ()
    : LockInit(usrCs,"NetServer::usrCs",true),
      LockInit(rcvCs,"NetServer::rcvCs",true),
      LockInit(sndCs,"NetServer::sndCs",true)
{
        // to be sure:
    enterUsr();
    received = NULL;
    sent = NULL;
    session.actualVersion = 0;
    session.requiredVersion = 0;
    session.gameState = 0;
    session.maxPlayers = 0;
    session.numPlayers = 0;
    session.password = false;
    session.port = 0;
    session.name[0] = (char)0;
    session.mission[0] = (char)0;
    session.mod[0] = 0;
    session.equalModRequired = 0;
    botId = 0;
    magicApp = 0;
#ifdef NET_LOG_INFO
    oldLatency = -1;
    oldThroughput = -1;
#endif
#ifdef NET_LOG_TRANSP_STAT
    nextStatLog = getSystemTime();
    forceLog = inGetConnection = false;
#endif
    setServer(this);
        // create a new NetPeer:
    poolLock.enter();
    sessionPort = getServerPeer() ? getServerPeer()->getPort() : 0;
    poolLock.leave();
    leaveUsr();
#ifdef NET_LOG_SERVER
#  ifdef NET_LOG_BRIEF
    NetLog("Pe(%u):srv",getServerPeer()? getServerPeer()->getPeerId() : 0);
#  else
    NetLog("Peer(%u): creating NetServer instance",getServerPeer()? getServerPeer()->getPeerId() : 0);
#  endif
#endif
}

/*!
\patch_internal 1.53 Date 4/26/2002 by Pepca
- Fixed: Casual crashes in game-server termination. [Sockets]
*/

NetServer::~NetServer ()
{
        // kick-off all the players:
    IteratorState it;
    enterUsr();
    RefD<NetChannel> ch;
    if ( users.getFirst(it,ch) )
        do
            destroyPlayer(ch,NTRDisconnected);
        while ( users.getNext(it,ch) );
    leaveUsr();
    SLEEP_MS(DESTRUCT_WAIT);                // waits till all players are gone..

#ifdef NET_LOG_SERVER
#  ifdef NET_LOG_BRIEF
    NetLog("Pe(%u):~srv",getServerPeer()->getPeerId());
#  else
    NetLog("Peer(%u): destroying NetServer instance",getServerPeer()->getPeerId());
#  endif
#endif
    CancelAllMessages();
    setServer(NULL);
        // recycle all pending NetMessages:
    RemoveUserMessages();
    RemoveSendComplete();
        // remove other data:
    RemovePlayers();
    RemoveVoicePlayers();
    checkPool();
		FreeMemory();
}

void NetServer::GetSendQueueInfo ( int to, int &nMsg, int &nBytes, int &nMsgG, int &nBytesG )
{
    enterUsr();
    RefD<NetChannel> channel;
    if ( users.get(to,channel) )
        channel->getOutputQueueStatistics(nMsg,nBytes,nMsgG,nBytesG);
    else {
        nMsg = nBytes = nMsgG = nBytesG = 0;
        }
    leaveUsr();
}

/*!
\patch_internal 1.53 Date 4/29/2002 by Pepca
- Added: DEDICATED_STAT_LOG - net-statistics logging on dedicated server. [Sockets]
*/

bool NetServer::GetConnectionInfo ( int to, int &latencyMS, int &throughputBPS )
{
    enterUsr();
    RefD<NetChannel> channel;
    if ( !users.get(to,channel) ) {
        leaveUsr();
        return false;
        }
    latencyMS     = (int)(channel->getLatency() / 1000);
    throughputBPS = (int)channel->getOutputBandWidth();
#ifdef NET_LOG_INFO
    if ( latencyMS != oldLatency ||
         throughputBPS != oldThroughput ) {
        NetLog("Channel(%u): NetServer::GetConnectionInfo(player=%d): %d ms,%d B/s",
               channel->getChannelId(),to,latencyMS,throughputBPS);
        oldLatency = latencyMS;
        oldThroughput = throughputBPS;
        }
#endif
        // check the channel condition:
    bool dropped = (to != botId) && channel->dropped();
    if ( dropped )
        finishDestroyPlayer(to);
#ifdef DEDICATED_STAT_LOG
    if ( IsDedicatedServer() ) {
        unsigned64 now = getSystemTime();
#ifdef NET_LOG_TRANSP_STAT
        if ( true ) {
            const bool doConsole = (now >= nextConsoleLog);
#else
        if ( now >= nextConsoleLog ) {
            const bool doConsole = true;
#endif
            if ( doConsole )
                nextConsoleLog = now + CONSOLE_LOG_INTERVAL;
            IteratorState it;
            if ( users.getFirst(it,channel,&to) )
                do
                    if ( to != botId ) {
                        RString stat = GetStatistics(to);
                        if ( doConsole )
                            ConsoleF("Player %d: %s",to,(const char*)stat);
                        }
                while ( users.getNext(it,channel,&to) );
            }
        }
#else
#  ifdef NET_LOG_TRANSP_STAT
    IteratorState it;
    forceLog = false;
    inGetConnection = true;
    for ( users.getFirst(it,channel,&to); channel; users.getNext(it,channel,&to) )
        if ( to != botId ) GetStatistics(to);
    inGetConnection = false;
#  endif
#endif
    leaveUsr();
    return true;
}

float NetServer::GetLastMsgAgeReliable ( int player )
{
    float result = 1.0;

    enterUsr();
    RefD<NetChannel> channel;
    if ( users.get(player,channel) )
    {
        unsigned64 last = channel->getLastMessageArrival();
        unsigned64 now = getSystemTime();
        if ( now < last )
        {
            LogF("Negative time delta in NetServer::GetLastMsgAgeReliable: now = %8x%08x, last = %8x%08x",
                 (unsigned)(now>>32),(unsigned)(now&0xffffffff),
                 (unsigned)(last>>32),(unsigned)(last&0xffffffff));
            result = 0;
        }
        else
        {
            unsigned64 delta = now - last;
#if _ENABLE_REPORT
            if ( delta > 200000000 )
                RptF("Overflow in NetServer::GetLastMsgAgeReliable: now = %8x%08x, last = %8x%08x, delta = %.0f",
                     (unsigned)(now>>32),(unsigned)(now&0xffffffff),
                     (unsigned)(last>>32),(unsigned)(last&0xffffffff),
                     (1.e-6 * delta));
#endif
            result = 1.e-6f * (unsigned)delta;
        }
    }

    leaveUsr();
    return result;
}

void NetServer::SetNetworkParams ( const ParamEntry &cfg )
{
    NetworkParams data;
    LoadNetworkParams(data,cfg);
    enterUsr();
    IteratorState it;
    RefD<NetChannel> channel;
    if ( !users.getFirst(it,channel) ) {
        leaveUsr();
        return;
        }
    channel->setNetworkParams(data);
    leaveUsr();
}

NetStatus destroyPlayerCallback ( NetMessage *msg, NetStatus event, void *data )
    // data = player number
{
    poolLock.enter();
    if ( _server )
        _server->finishDestroyPlayer((int)data);
    poolLock.leave();
    return nsNoMoreCallbacks;
}

/*!
\patch 1.82 Date 8/19/2002 by Ondra
- Fixed: MP: Reason of disconnection was often not displayed on clients.
*/

void NetServer::destroyPlayer ( NetChannel *ch, NetTerminationReason reason )
{
    Assert( ch );
#ifdef NET_LOG_SERVER_KICKOFF
    NetLog("Channel(%u): NetServer::destroyPlayer: player=%d",ch->getChannelId(),channelToPlayer(ch));
#endif
        // send MAGIC_TERMINATE_SESSION message to the client:
    unsigned32 magic[2];
    Ref<NetMessage> out = NetMessagePool::pool()->newMessage(sizeof(magic),ch);
    if ( !out ) return;
    magic[0] = MAGIC_TERMINATE_SESSION;
    magic[1] = reason;
    out->setFlags(MSG_ALL_FLAGS,MSG_MAGIC_FLAG);
    out->setData((unsigned8*)&magic,sizeof(magic));
    //RptF("NetServer::destroyPlayer: player=%d",channelToPlayer(ch));
    out->setCallback(destroyPlayerCallback,nsOutputSent,(void*)channelToPlayer(ch));
    out->send(true);
}

void NetServer::logUsers()
{
    char usersText[1024];

    IteratorState it;
    int key;
    RefD<NetChannel> channel;
    sprintf(usersText,"%d: ",users.card());
    if ( users.getFirst(it,channel,&key) )
      do
        sprintf(usersText+strlen(usersText)," %d",key);
      while ( users.getNext(it,channel,&key) );
    RptF("NetServer::logUsers %s",usersText);
}

void NetServer::finishDestroyPlayer ( int player )
{
    enterUsr();
    RefD<NetChannel> ch;
    if ( !users.get(player,ch) ) {
        RptF("NetServer::finishDestroyPlayer(%d): users.get failed",player);
        //logUsers();
        DoAssert(users.checkIntegrity());
        leaveUsr();
        return;
        }
        // delete player from my structures:
    users.remove(player);
    struct sockaddr_in daddr;
    ch->getDistantAddress(daddr);
    m_support.remove(sockaddrKey(daddr));
    //logUsers();
    DoAssert(users.checkIntegrity());
    getPool()->deleteChannel(ch);
    int index;
    for ( index = 0; index < _createPlayers.Size(); index++ )
        if ( _createPlayers[index].player == player ) {
            RptF("NetServer::finishDestroyPlayer(%d): DESTROY immediately after CREATE, both cancelled",player);
            _createPlayers.Delete(index);
            leaveUsr();
            return;
            }
    index = _deletePlayers.Add();
    DeletePlayerInfo &info = _deletePlayers[index];
    info.player = player;
    RptF("NetServer:finishDestroyPlayer (waiting for ProcessPlayers) - session.numPlayers=%d, playerId=%d, |users|=%u",
         session.numPlayers,info.player,users.card());
    leaveUsr();
}

void NetServer::KickOff ( int player, NetTerminationReason reason )
{
    enterUsr();
    RefD<NetChannel> channel;
    if ( !users.get(player,channel) ) {
        RptF("NetServer::KickOff: player=%d - !users.get",player);
        leaveUsr();
        return;
        }
#ifdef NET_LOG_SERVER_KICKOFF
    NetLog("Channel(%u): NetServer::KickOff: player=%d",channel->getChannelId(),player);
#endif
    //RptF("NetServer::KickOff: player=%d",player);
    destroyPlayer(channel,reason);
    leaveUsr();
}

bool NetServer::InitVoice ()
{
    return false;
}

void NetServer::GetTransmitTargets ( int from, AutoArray<int,MemAllocSA> &to )
{
}

void NetServer::SetTransmitTargets ( int from, AutoArray<int,MemAllocSA> &to )
{
}

void NetServer::ProcessVoicePlayers ( CreateVoicePlayerCallback *callback, void *context )
{
}

void NetServer::RemoveVoicePlayers ()
{
}

bool NetServer::Init ( int port, RString password, char *hostname, int maxPlayers,
                       RString sessionNameInit, RString sessionNameFormat, RString playerName,
                       MPVersionInfo &versionInfo, bool equalModRequired, int magic )
{
  if ( !sessionPort )
  {
#if _ENABLE_DEDICATED_SERVER
    if ( IsDedicatedServer() )
    {
      ConsoleF("Cannot start server on port %d",port);
    }
#endif
    return false;
  }
        // initialize session attributes:
    enterUsr();
    _password = password;
    magicApp = magic;
    session.actualVersion = versionInfo.versionActual;
    session.requiredVersion = versionInfo.versionRequired;
    session.equalModRequired = equalModRequired ? 1 : 0;
    session.maxPlayers = maxPlayers;
    session.numPlayers = 0;
    session.password = password.GetLength() > 0;
    session.port = sessionPort;
    char localName[128];
    if ( !hostname || !hostname[0] )        // get more friendly form of localhost
        if ( getLocalName(localName,128) )
            hostname = localName;
        else
            hostname = NULL;
    char buf[512];
    if ( hostname )
        if ( playerName.GetLength() > 0 )
            snprintf(buf,512,(const char*)sessionNameFormat,(const char*)playerName,hostname);
        else
            strncpy(buf, hostname, 512);
    else
        strncpy(buf, sessionNameInit, 512);
    buf[511] = (char)0;
    strncpy(session.name,buf,sizeof(session.name));
    session.name[sizeof(session.name)-1] = (char)0;
    sessionName = buf;

    strncpy(session.mod, versionInfo.mod, sizeof(session.mod));
    session.mod[sizeof(session.mod)-1] = (char)0;

    session.mission[0] = 0;
    session.gameState = 0;
#ifdef DEDICATED_STAT_LOG
    nextConsoleLog = getSystemTime();
#endif
#ifdef NET_LOG_SERVER
#  ifdef NET_LOG_BRIEF
    NetLog("Pe(%u):srv(%d,'%s','%s','%s',%d)",
           getServerPeer()->getPeerId(),sessionPort,hostname,session.name,(const char*)playerName,magic);
#  else
    NetLog("Peer(%u): NetServer::Init: local port=%d, hostname='%s', sessionName='%s', playerName='%s', magic=%d",
           getServerPeer()->getPeerId(),sessionPort,hostname,session.name,(const char*)playerName,magic);
#  endif
#endif
    leaveUsr();
    return true;
}

NetStatus serverSendComplete ( NetMessage *msg, NetStatus event, void *data )
    // data -> NetServer instance
    // thread-safe
{
    NetServer *server = (NetServer*)data;
    if ( !server || !msg ) return nsNoMoreCallbacks;    // fatal error
#ifdef NET_LOG_SERVER_COMPLETE
    NetLog("Channel(%u)::serverSendComplete: status=%d, len=%3u, serial=%4u, flags=%04x, msgId=%x",
           msg->getChannel()->getChannelId(),
           (int)msg->getStatus(),msg->getLength(),msg->getSerial(),(unsigned)msg->getFlags(),msg->id);
#endif
    server->enterSnd();
    msg->next = server->sent;
    server->sent = msg;
    server->leaveSnd();
    return nsNoMoreCallbacks;
}

bool NetServer::SendMsg ( int to, BYTE *buffer, int bufferSize, DWORD &msgID, NetMsgFlags flags )
{
    if ( !buffer || bufferSize <= 0 )
    {
      RptF("NetServer: trying to send empty message to %d",to);
      return false;
    }
    RefD<NetChannel> channel;
    Ref<NetMessage> msg;
    bool vim = (flags & NMFGuaranteed) > 0;
    bool urgent = (flags & NMFHighPriority) > 0;
    int maxMessage = getServerPeer()->maxMessageData();
    if ( (!vim || to == DPNID_ALL_PLAYERS_GROUP) && bufferSize > maxMessage ) {
#ifdef NET_LOG_MERGE
        unsigned chId = 0;
        if ( to != DPNID_ALL_PLAYERS_GROUP &&
             users.get(to,channel) )
          chId = channel->getChannelId();
#  ifdef NET_LOG_BRIEF
        NetLog("Ch(%u):tooLargeS(%d,%d,%x)",
               chId,to,bufferSize,(unsigned)flags);
#  else
        NetLog("Channel(%u): NetServer::SendMsg: trying to send too large non-guaranteed message (to=%d, len=%3d, flags=%x)",
               chId,to,bufferSize,(unsigned)flags);
#  endif
#endif
        RptF("NetServer: trying to send too large non-guaranteed message (%d bytes long)",bufferSize);
        return false;
        }
    unsigned16 fl = 0;
    if ( vim )    fl |= MSG_VIM_FLAG;
    if ( urgent ) fl |= MSG_URGENT_FLAG;

    enterUsr();
    if ( to == DPNID_ALL_PLAYERS_GROUP ) {  // broadcast to all players
#ifdef NET_LOG_SERVER_SEND
        NetLog("Peer(%u): NetServer::SendMsg: broadcast to all players",getServerPeer()->getPeerId());
#endif
        IteratorState it;
        if ( users.getFirst(it,channel) )
            do {
                msg = NetMessagePool::pool()->newMessage(bufferSize,channel);
                if ( !msg ) {
                    leaveUsr();
                    RptF("NetServer: pool()->newMessage failed when sending to %d",to);
                    return false;
                    }
                msg->setFlags(MSG_ALL_FLAGS,fl);
                if ( fl )
                    msg->setOrderedPrevious();  // VIM is set ORDERED automatically
                else {                          // common messages
                    msg->setCallback(serverSendComplete,nsOutputSent,this);
                    msg->setSendTimeout(SEND_TIMEOUT);
                    }
                msg->setData((unsigned8*)buffer,bufferSize);
                msg->send(urgent);
#ifdef NET_LOG_SERVER_SEND
                NetLog("Channel(%u): NetServer::SendMsg: to=%d, len=%3d, flags=%x, msgID=%x, hdr=%08x%08x",
                       channel->getChannelId(),to,bufferSize,(unsigned)flags,(unsigned)msg->id,
                       ((unsigned*)msg->getData())[0],((unsigned*)msg->getData())[1]);
#endif
                } while ( users.getNext(it,channel) );
        }

    else {                                  // single recipient
        if ( !users.get(to,channel) ) {
#ifdef NET_LOG_SERVER_SEND
            NetLog("Peer(%u): NetServer::SendMsg: cannot find channel #%d, users.card=%u",
                   getServerPeer()->getPeerId(),to,users.card());
#endif
            RptF("NetServer::SendMsg: cannot find channel #%d, users.card=%u",
                 to,users.card());
            RptF("NetServer: users.get failed when sending to %d",to);
            //logUsers();
            DoAssert(users.checkIntegrity());
            leaveUsr();
            return false;
            }
        if ( bufferSize > maxMessage ) {    // split too big message:
#ifdef NET_LOG_MERGE
#  ifdef NET_LOG_BRIEF
            NetLog("Ch(%u):sMerS(%d,%d,%x)",
                   channel->getChannelId(),to,bufferSize,(unsigned)flags);
#  else
            NetLog("Channel(%u): NetServer::SendMsg: splitting user message (to=%d, len=%3d, flags=%x)",
                   channel->getChannelId(),to,bufferSize,(unsigned)flags);
#  endif
#endif
            int toSent = bufferSize;
            int packet;
            fl |= MSG_PART_FLAG;
            do {
                packet = (toSent > maxMessage) ? maxMessage : toSent;
                toSent -= packet;
                msg = NetMessagePool::pool()->newMessage(packet,channel);
                if ( !msg ) {
                    leaveSnd();
                    RptF("NetServer: pool()->newMessage failed when sending to %d",to);
                    return false;
                    }
                msg->setFlags(MSG_ALL_FLAGS,fl | (toSent ? 0 : MSG_CLOSING_FLAG));
                msg->setOrderedPrevious();
                msg->setData((unsigned8*)buffer,packet);
                buffer += packet;
                msg->send(urgent);
                } while ( toSent );
            }
        else {                              // small message
            msg = NetMessagePool::pool()->newMessage(bufferSize,channel);
            if ( !msg ) {
                leaveUsr();
                RptF("NetServer: pool()->newMessage failed when sending to %d",to);
                return false;
                }
            msg->setFlags(MSG_ALL_FLAGS,fl);
            if ( fl )
                msg->setOrderedPrevious();      // VIM is set ORDERED automatically
            else {                              // common messages
                msg->setCallback(serverSendComplete,nsOutputSent,this);
                msg->setSendTimeout(SEND_TIMEOUT);
                }
            msg->setData((unsigned8*)buffer,bufferSize);
            msg->send(urgent);
            }
#ifdef NET_LOG_SERVER_SEND
        NetLog("Channel(%u): NetServer::SendMsg: to=%d, len=%3d, flags=%x, msgID=%x, hdr=%08x%08x",
               channel->getChannelId(),to,bufferSize,(unsigned)flags,(unsigned)msg->id,
               ((unsigned*)msg->getData())[0],((unsigned*)msg->getData())[1]);
#endif
        }

    msgID = (DWORD)msg->id;
    leaveUsr();
    return true;
}

void NetServer::CancelAllMessages ()
{
    poolLock.enter();
    if ( getServerPeer() )
        getServerPeer()->cancelAllMessages();
    poolLock.leave();
}

void NetServer::UpdateSessionDescription ( int state, RString mission )
{
    enterUsr();
    session.gameState = state;
    strncpy(session.mission,mission,LEN_MISSION_NAME);
    session.mission[LEN_MISSION_NAME-1] = (char)0;
    leaveUsr();
}

bool NetServer::GetURL ( char *address, DWORD addressLen )
{
    poolLock.enter();
    bool result = (getServerPeer() != NULL);
    if ( result ) {
        Assert( address );
        struct sockaddr_in local;
        getLocalAddress(local,sessionPort);
        sprintf(address,"%u.%u.%u.%u:%u",
                (unsigned)IP4(local),(unsigned)IP3(local),(unsigned)IP2(local),(unsigned)IP1(local),(unsigned)sessionPort);
        }
    poolLock.leave();
    return result;
}

int NetServer::channelToPlayer ( NetChannel *ch )
{
    if ( !ch ) return -1;
    IteratorState it;
    int i;
    enterUsr();
    RefD<NetChannel> itch;
    if ( users.getFirst(it,itch,&i) )
        do
            if ( (NetChannel*)itch == ch ) break;
        while ( users.getNext(it,itch,&i) );
    if ( !itch ) i = -1;
    leaveUsr();
    return i;
}

void NetServer::ProcessUserMessages ( UserMessageServerCallback *callback, void *context )
{
    if ( !callback ) return;
    Ref<NetMessage> msg;
    enterRcv();
#ifdef NET_LOG_SERVER_PROCESS
    int n = 0;
    msg = received;
    while ( msg ) {
        n++;
        msg = msg->next;
        }
    if ( n ) {
        NetLog("Peer(%u): NetServer::ProcessUserMessages: processed %d messages:",getServerPeer()->getPeerId(),n);
        msg = received;
        while ( msg ) {
            NetLog("Channel(%u): NetServer::ProcessUserMessages: len=%3u, serial=%4u, flags=%04x, msgID=%x, hdr=%08x%08x",
                   msg->getChannel()->getChannelId(),msg->getLength(),msg->getSerial(),(unsigned)msg->getFlags(),msg->id,
                   ((unsigned*)msg->getData())[0],((unsigned*)msg->getData())[1]);
            msg = msg->next;
            }
        }
#endif
    while ( received )
    {
        msg = received;
        received = msg->next;
        msg->next = NULL;
          // process the "msg" message:
        enterUsr();
        int player = channelToPlayer(msg->getChannel());
        leaveUsr();
        leaveRcv();
        if ( player == -1 )
          RptF("No player found for channel %u - message ignored",(unsigned)msg->getChannel());
        else
          (*callback)(player,(char*)msg->getData(),msg->getLength(),context);
        enterRcv();
    }
    leaveRcv();
}

void NetServer::insertReceived ( NetMessage *msg )
    // must be called inside enterRcv()
{
    if ( received ) {
        MsgSerial s = msg->getSerial();
        if ( s < received->getSerial() ) {
            msg->next = received;
            received = msg;
            }
        else {
            NetMessage *ptr = received;
            while ( ptr->next && ptr->next->getSerial() < s )
                ptr = ptr->next;
            msg->next = ptr->next;
            ptr->next = msg;
            }
        }
    else {
        msg->next = NULL;
        received = msg;
        }
}

void NetServer::RemoveUserMessages ()
{
    enterRcv();
    Ref<NetMessage> tmp;
    while ( received ) {
        tmp = received->next;
        received->next = NULL;              // don't need this message anymore, somebody will recycle it later
        received = tmp;
        }
    leaveRcv();
}

void NetServer::ProcessSendComplete ( SendCompleteCallback *callback, void *context )
{
    if ( !callback ) return;
    enterSnd();
    NetMessage *msg;
#ifdef NET_LOG_SERVER_COMPLETE
    int n = 0;
    msg = sent;
    while ( msg ) {
        n++;
        msg = msg->next;
        }
    if ( n ) {
        NetLog("Peer(%u): NetServer::ProcessSendComplete: processed %d messages:",getServerPeer()->getPeerId(),n);
        msg = sent;
        while ( msg ) {
            NetLog("Channel(%u): NetServer::ProcessSendComplete: ok=%d(%d), len=%4u, serial=%3u, flags=%04x, MsgID=%x, hdr=%08x%08x",
                   msg->getChannel()->getChannelId(),(int)(msg->getStatus()!=nsOutputObsolete),(int)msg->getStatus(),
                   msg->getLength(),msg->getSerial(),(unsigned)msg->getFlags(),msg->id,
                   ((unsigned*)msg->getData())[0],((unsigned*)msg->getData())[1]);
            msg = msg->next;
            }
        }
#endif
    msg = sent;
    while ( msg ) {
        (*callback)((DWORD)msg->id,msg->wasSent(),context);
        msg = msg->next;
        }
    RemoveSendComplete();
    leaveSnd();
}

void NetServer::RemoveSendComplete ()
{
    enterSnd();
    Ref<NetMessage> tmp;
    while ( sent ) {
        tmp = sent->next;
        sent->next = NULL;                  // don't need this message anymore, somebody will recycle it later
        sent = tmp;
        }
    leaveSnd();
}

void NetServer::ProcessPlayers ( CreatePlayerCallback *callbackCreate, DeletePlayerCallback *callbackDelete, void *context )
{
    int i;
    enterUsr();
    if ( _deletePlayers.Size() || _createPlayers.Size() )
        RptF("NetServer::ProcessPlayers(): users.card=%u, session.numPlayers=%d, created=%d, deleted=%d",
             users.card(),session.numPlayers,_createPlayers.Size(),_deletePlayers.Size());
    for ( i = 0; i < _deletePlayers.Size(); i++ ) {
        DeletePlayerInfo &info = _deletePlayers[i];
        //RptF("NetServer::ProcessPlayers(): delete=%d",info.player);
        callbackDelete(info.player,context);
        session.numPlayers--;
        }
    if ( session.numPlayers < 0 )
        session.numPlayers = 0;
    _deletePlayers.Clear();
    for ( i = 0; i < _createPlayers.Size(); i++ ) {
        CreatePlayerInfo &info = _createPlayers[i];
        //RptF("NetServer::ProcessPlayers(): create=%d,%s",info.player,info.name);
        callbackCreate(info.player,info.botClient,info.name,info.mod,context);
        session.numPlayers++;
        }
    _createPlayers.Clear();
    leaveUsr();
}

void NetServer::RemovePlayers ()
{
    enterUsr();
    _createPlayers.Clear();
    _deletePlayers.Clear();
    leaveUsr();
}

RString NetServer::GetStatistics ( int player )
{
    enterUsr();
    RefD<NetChannel> channel;
    if ( !users.get(player,channel) ) {
        leaveUsr();
        return Format("Unknown player ID = %d",player);
        }
    int latencyAve;
    unsigned latencyAct,latencyMin;
    int throughputAve;
    latencyAve = (int)(channel->getLatency(&latencyAct,&latencyMin) / 1000);
    latencyAct /= 1000;
    latencyMin /= 1000;
    EnhancedBWInfo enhanced;
    throughputAve = (int)channel->getOutputBandWidth(&enhanced);
    int nMsg, nBytes, nMsgG, nBytesG;
    channel->getOutputQueueStatistics(nMsg,nBytes,nMsgG,nBytesG);
    ChannelStatistics stat;                 // internal statistics of the NetChannel
    Zero(stat);
    channel->getInternalStatistics(stat);
    leaveUsr();
    char buf[256];
    bool kbps = (throughputAve < (9<<17));
    sprintf(buf,"ping%4dms(%4u,%4u) BW%c%c%c%5d%cb(%4u,%4u,%4u) lost%4.1f%%%%(%3u) queue%4dB(%4d) ackWait%3u(%3.1f,%3.1f)",
                latencyAve,latencyAct,latencyMin,(char)(enhanced.growMode+'c'),(char)(enhanced.growModePing+'c'),
                (char)(enhanced.growModeLost+'c'),kbps?((throughputAve+64)>>7):((throughputAve+65536)>>17),
                kbps?'K':'M',(enhanced.actBW+64)>>7,(enhanced.goodBW+64)>>7,(enhanced.sentBW+64)>>7,
                stat.ackTotal?(stat.ackLost*100.0)/stat.ackTotal:0.0,stat.ackLost,
                nBytes,nBytesG,stat.revisitedNo,1e-6*stat.revisitedAveAge,1e-6*stat.revisitedMaxAge);
#ifdef NET_LOG_TRANSP_STAT
    unsigned64 now = getSystemTime();
    if ( player != botId && (inGetConnection && forceLog || now >= nextStatLog) ) {
        forceLog = true;
        NetLog("Channel(%u): NetServer(%d)[%d] - %s",channel->getChannelId(),player,getStatisticsCount,buf);
        getStatisticsCount = 0;
        if ( now >= nextStatLog )
            nextStatLog = now + STAT_LOG_INTERVAL;
        }
    else
        getStatisticsCount++;
#endif
    return RString(buf);
}

unsigned NetServer::FreeMemory ()
{
    if ( !NetMessagePool::pool() ) return 0;
    unsigned ret = NetMessagePool::pool()->freeMemory();
		SafeMemoryCleanUp();
    return ret;
}

#endif  // _ENABLE_MP
