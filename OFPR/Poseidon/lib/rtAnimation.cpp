// Poseidon - shape management
// (C) 1998, SUMA

#include "wpch.hpp"
#include "rtAnimation.hpp"
#include <El/QStream/QBStream.hpp>
#include <Es/Types/scopeLock.hpp>

#include <El/Math/math3dP.hpp>

template Ref<AnimationRT>; // bug is MSVC 5.0 - force instantiation
//template Ref<Skeleton>; // bug is MSVC 5.0 - force instantiation
//template Ref<WeightEntry>; // bug is MSVC 5.0 - force instantiation

void AnimationRTWeight::Normalize()
{
	int n=Size();
	if( n==0 ) return;
	else if( n==1 )
	{
		Set(0).SetWeight(1);
	}
	else
	{
		float sum=0;
		for( int i=0; i<n; i++ ) sum+=Get(i).GetWeight();
		if( sum>0 )
		{
			float invSum=1/sum;
			float rest=1.001;
			for( int i=1; i<n; i++ )
			{
				AnimationRTPair &pair = Set(i);
				float w = pair.GetWeight()*invSum;
				pair.SetWeight(w);
				// SetWeight may incure rounding error
				w = pair.GetWeight();
				rest -= w;
			}
			AnimationRTPair &pair = Set(0);
			pair.SetWeight(rest);

			// verify normalization

			float sum=0;
			for( int i=0; i<n; i++ ) sum+=Get(i).GetWeight();
			Assert( sum>=0.999 );
		}
		else
		{
			Fail("Sum zero");
		}
		// avoid rounding errors
	}
}

AnimationRTWeights::~AnimationRTWeights()
{
}

AnimationRTWeights::AnimationRTWeights()
:_needNormalize(false),_isSimple(false)
{
}

void AnimationRTWeights::Init( Shape *shape )
{
	//LogF("Shape %x",shape);
	//LogF("this %x",this);
	//LogF("Shape %d",shape->NPos());
	_data.Realloc(shape->NPos());
	_data.Resize(shape->NPos());
}

Skeleton::Skeleton()
{
}

Skeleton::Skeleton( RStringB name )
{
	_name = name;
}

int Skeleton::FindBone( RStringB name ) const
{
	// check if bone already exists
	for (int i=0; i<_matrixNames.Size(); i++)
	{
		if (name==_matrixNames[i]) return i;
	}
	//ErrF("Bone not found %s",(const char *)name);
	return -1;
}

int Skeleton::NewBone( RStringB name )
{
	// check if bone already exists
	for (int i=0; i<_matrixNames.Size(); i++)
	{
		if (name==_matrixNames[i]) return i;
	}
	return _matrixNames.Add(name);
}

void AnimationRTWeights::AddSelection( Shape *shape, int selIndex, int matrixIndex )
{
	if( _selections.Find(selIndex)>=0 ) return; // selection already in
	// use weighted selections
	const NamedSelection &sel=shape->NamedSel(selIndex);
	//const char *name=sel.Name();
	for( int si=0; si<sel.Size(); si++ )
	{
		int index=sel[si];
		AnimationRTWeight &entry=_data[index];
		float selW = sel.Weight(si)*(1.0f/255);
		AnimationRTPair pair(matrixIndex,selW);
		int w = entry.Add(pair);
		if (w<0)
		{
			// try to find bone with less influence than this one
			float minIW = selW;
			int minI = -1;
			for (int i=0; i<entry.Size(); i++)
			{
				float iw = entry[i].GetWeight();
				if (minIW>iw) minIW = iw, minI = i;
			}
			if (minI>0)
			{
				entry[minI] = pair;
			}
			//LogF("Too complex skinning, selection %s",(const char *)sel.Name());
		}
	}
	// record we have mixed-in another selection
	_selections.Add(selIndex);
	_needNormalize=true;
}

void AnimationRTWeights::Normalize()
{
	if( !_needNormalize ) return;
	_isSimple=true;
	for( int i=0; i<_data.Size(); i++ )
	{
		//_data[i].Compact();
		_data[i].Normalize();
		if( _data[i].Size()!=1 ) _isSimple=false;
	}
	_data.Compact();
	_needNormalize=false;
}

WeightInfo::WeightInfo()
{
	// empty non-shared skeleton
	//_skeleton = new Skeleton("");
}

WeightInfo::WeightInfo( const WeightInfoName &name )
{
	_name=name;
	// weights is by default empty
	// we need to attach skeleton
	// if we have a skeleton name, we will attach to it
}

AnimationRTPhase::AnimationRTPhase()
{
}

AnimationRTPhase::~AnimationRTPhase()
{
}

// skeleton included in f context
void AnimationRTPhase::SerializeBin(SerializeBinStream &f)
{
}

float AnimationRTPhase::Load
(
	QIStream &in, Skeleton *skelet, int nSel, bool reversed
)
{
  Assert(_matrix.Size()>=nSel);
  Assert(_matrix.Size()<=skelet->NBones());
	//_matrix.Realloc(skelet->NBones());
	//_matrix.Resize(skelet->NBones());
	// set all matrices to identity - not all matrices are set
	for (int i=0; i<_matrix.Size(); i++)
	{
		_matrix[i] = MIdentity;
	}
	// load single phase
	float time = 0;
	in.read((char *)&time,sizeof(time));
	if (in.fail())
	{
		// cannol load - skip and return
		RptF("Broken animation file");
		return 0;
	}
	for( int i=0; i<nSel; i++ )
	{
		char name[32];
		Matrix4P transformP;
		Matrix4 transform;
		in.read(name,sizeof(name));
		// get matrix index from corresponding skeleton

		strlwr(name);
		int index = skelet->FindBone(name);

		in.read((char *)&transformP,sizeof(transformP));
		transform=ConvertToM(transformP);
		if( reversed )
		{
			//const static Matrix4P swapMatrix(MRotationY,H_PI);
			const static Matrix4 swapMatrix(MScale,-1,1,-1);
			transform=swapMatrix*transform*swapMatrix;
		}
		if (index<0 || index>=_matrix.Size())
		{
			RptF("Bad bone %s",name);
		}
		else
		{
			_matrix[index]=transform;
		}
	}
	return time;
}

#define RTMMagicLen 8
static const char RTMMagic100[RTMMagicLen+1]="RTM_0100";
static const char RTMMagic101[RTMMagicLen+1]="RTM_0101";

DEFINE_FAST_ALLOCATOR(AnimationRT)

AnimationRT::AnimationRT()
{
  _loadCount = 0;
	_preloadCount=0;
	_nPhases = 0;
}

AnimationRT::AnimationRT( const AnimationRTName &name, bool reversed )
{
  _loadCount = 0;
	_preloadCount=0;
	_name = name;
	_nPhases = 0;
	Load(name.skeleton,name.name,reversed);
}

// skeleton included in f context
void AnimationRT::SerializeBin(SerializeBinStream &f)
{
	
}

//! Smart animation loading/unloading
/*!
Any AnimationRT has called AddLoadCount/ReleaseLoadCount
when assigned/unassigned to any RefLoadAnimationRT object.
*/

class RefLoadAnimationRT: public Ref<AnimationRT>
{
	typedef Ref<AnimationRT> base;

	public:

	//! copy constructor
	RefLoadAnimationRT(AnimationRT *source)
	:base(source)
	{
		if (source) source->AddLoadCount();
	}
	//! copy constructor
	RefLoadAnimationRT(const Ref<AnimationRT> &source)
	:base(source)
	{
		if (source.NotNull()) source->AddLoadCount();
	}

	//! assignment operator
  void operator = (AnimationRT *source)
	{
		if (GetRef()) GetRef()->ReleaseLoadCount();
		base::operator = (source);
		if (source) source->AddLoadCount();
	}
	//! assignment operator
  void operator = (const Ref<AnimationRT> &source)
	{
		if (GetRef()) GetRef()->ReleaseLoadCount();
		base::operator = (source);
		if (source) source->AddLoadCount();
	}

	//! destructor
	~RefLoadAnimationRT()
	{
		if (GetRef()) GetRef()->ReleaseLoadCount();
	}
};

TypeIsMovableZeroed(RefLoadAnimationRT)

#include "El/FreeOnDemand/memFreeReq.hpp"

class AnimationRTCache: public MemoryFreeOnDemandHelper
{
	CLList<AnimationRT> _loaded;
	int _count;
	int _countMatrices;

	public:
	AnimationRTCache();
	~AnimationRTCache();

	void Load(AnimationRT *anim);
	void Delete(AnimationRT *anim);

	size_t FreeOneItem();
	float Priority();

};

size_t AnimationRTCache::FreeOneItem()
{
	AnimationRT *item = _loaded.Last();
	if (!item) return 0;
	int nMatrices = item->GetKeyframeCount()*item->GetSkeleton()->NBones();
	Delete(item);
	return nMatrices*sizeof(Matrix4);
}

float AnimationRTCache::Priority()
{
	const int AnimationRTCacheItemSize = 100*1024;
	const float AnimationRTCacheItemTime = 10000;

	return AnimationRTCacheItemTime/AnimationRTCacheItemSize;
}

/*!
\patch 1.45 Date 2/18/2002 by Ondra
- Optimized: Memory usage for MC animations optimized.
Most MC Data loaded only when needed.
*/


//! max. number of animations held in cache
const int MaxAnimationRTItemCount = 4*1024;
//! max. number of animation matrices held in cache
const int MaxAnimationRTMatrixCount = 32000;

AnimationRTCache::AnimationRTCache()
{
	_count = 0;
	_countMatrices = 0;
}
AnimationRTCache::~AnimationRTCache()
{
	Assert(_count==_loaded.Size());
}

/*!
\patch_internal 1.59 Date 5/23/2002 by Ondra
- Fixed: Bug in MC data cache: Animation size was underestimated.
\patch_internal 1.59 Date 5/23/2002 by Ondra
- Fixed: Bug in MC data cache: Releasing animation sometimes accessed freed memory.
*/
void AnimationRTCache::Delete(AnimationRT *anim)
{
	Assert(anim->IsInList());
	_countMatrices -= anim->GetKeyframeCount()*anim->GetSkeleton()->NBones();
	_count--;
#if _DEBUG
	if (_count==0)
	{
		Assert(_countMatrices==0);
	}
#endif
	anim->ReleaseLoadCount();
	_loaded.Delete(anim);
}

void AnimationRTCache::Load(AnimationRT *anim)
{
	if (anim->IsInList())
	{
		RefLoadAnimationRT tempRef = anim;
		_loaded.Delete(anim);
		_loaded.Insert(anim);
	}
	else
	{
    // make sure cache does not overflow
		if
		(
			_count>MaxAnimationRTItemCount ||
			_countMatrices>MaxAnimationRTMatrixCount
		)
		{
			if (_loaded.Last()) Delete(_loaded.Last());
		}

		// check if there is some free space in animation cache
		// when added to list, increase LoadCount
		anim->AddLoadCount();

		_loaded.Insert(anim);

		_countMatrices += anim->GetKeyframeCount()*anim->GetSkeleton()->NBones();
		_count++;
		Assert(_count==_loaded.Size());

	}
}

static AnimationRTCache GAnimationRTCache;

/*!
\patch_internal 1.43 Date 2/1/2002 by Ondra
- Optimized: animation loading changed to be more suitable for delayed loading.
*/

#define LOG_ANIM_LOAD 0

void AnimationRT::FullLoad(Skeleton *skelet, const char *file)
{
	#if LOG_ANIM_LOAD
	LogF("Animation %s fully loaded",file);
	#endif
	QIFStreamB in;
	in.AutoOpen(file);
	if (in.fail() || in.rest()<=0)
	{
		RptF("Animation %s not found or empty",file);
		return;
	}

	char magic[RTMMagicLen+1];
	in.read(magic,RTMMagicLen);
	magic[RTMMagicLen]=0;
	if( !strcmp(magic,RTMMagic100) )
	{
		// skip stepZ
		in.seekg(sizeof(float),QIOS::cur);
	}
	else if( !strcmp(magic,RTMMagic101) )
	{
		// skip stepX, stepY, stepZ
		in.seekg(sizeof(float)+sizeof(float)+sizeof(float),QIOS::cur);
	}
	else
	{
		ErrorMessage("Bad animation file format in file '%s'.",file);
		return;
	}
	int nSel=0, nAnim=0;
	in.read((char *)&nAnim,sizeof(nAnim));
	in.read((char *)&nSel,sizeof(nSel));
	Assert(_nPhases==nAnim);
	Assert(_selections.Size()==nSel)
	// scan all selection and update weights as necessary
	in.seekg(32*nSel,QIOS::cur);

	Assert(_nPhases>=1);
	// very short animations should be preloaded
	_phases.Resize(_nPhases);
	_phaseTimes.Resize(_nPhases);
  // check max. used bone index
  int bonesUsed = 0;
  for (int i=0; i<_selections.Size(); i++)
  {
    int bone = skelet->FindBone(_selections[i]);
    if (bonesUsed<bone+1) bonesUsed = bone+1;
  }
	// read complete animation data
	for( int i=0; i<_nPhases; i++ )
	{
    _phases[i]._matrix.Realloc(bonesUsed);
    _phases[i]._matrix.Resize(bonesUsed);
		float time = _phases[i].Load(in,skelet,_selections.Size(),_reversed);
		_phaseTimes[i] = time;
	}
	RemoveLoopFrame();
}

AnimationRT::~AnimationRT()
{
	if (IsInList())
	{
		GAnimationRTCache.Delete(this);
	}
}

void AnimationRT::FullRelease()
{
	#if LOG_ANIM_LOAD
	LogF("Animation %s released",(const char *)_name.name);
	#endif
	_phases.Clear();
}

const int PersistantLimit = 3;

void AnimationRT::AddPreloadCount()
{
	// small animations are always persistant - we cannot change it
	if (_nPhases<=PersistantLimit) return;
	_preloadCount++;
	#if LOG_ANIM_LOAD>=2
		LogF("Animation %s preloaded - AddPreloadCount()",Name());
	#endif
	AddLoadCount();
}
void AnimationRT::ReleasePreloadCount()
{
	if (_nPhases<=PersistantLimit) return;
	_preloadCount--;
	#if LOG_ANIM_LOAD
		LogF("Animation %s unpreloaded - ReleasePreloadCount()",Name());
	#endif
	ReleaseLoadCount();
}

/*!
\patch 1.95 Date 10/22/2003 by Ondra
- Fixed: Bug in animation loading.
As a result some 3rd party addons with custom animation could cause a crash.
*/

void AnimationRT::Load
(
	QIStream &in, Skeleton *skelet,
	const char *name, bool reversed
)
{
	_loadCount = 0;
	_looped = true;
	_reversed = reversed;

	_step=VZero;
	_phases.Clear();
	char magic[RTMMagicLen+1];
	in.read(magic,RTMMagicLen);
	magic[RTMMagicLen]=0;
	if( !strcmp(magic,RTMMagic100) )
	{
		float stepZ=0;
		in.read((char *)&stepZ,sizeof(stepZ));
		_step=Vector3(0,0,stepZ);
	}
	else if( !strcmp(magic,RTMMagic101) )
	{
		float stepX=0, stepY=0, stepZ=0;
		in.read((char *)&stepX,sizeof(stepX));
		in.read((char *)&stepY,sizeof(stepY));
		in.read((char *)&stepZ,sizeof(stepZ));
		_step=Vector3(stepX,stepY,stepZ);
	}
	else
	{
		_nPhases = 0;
		ErrorMessage("Bad animation file format in file '%s'.",name);
		return;
	}
	int nSel=0, nAnim=0;
	in.read((char *)&nAnim,sizeof(nAnim));
	in.read((char *)&nSel,sizeof(nSel));
	_nPhases = nAnim;
	// scan all selection and update weights as necessary
	for( int i=0; i<nSel; i++ )
	{
		char name[32];
		in.read(name,sizeof(name));
		strlwr(name);
		_selections.Add(name);
		skelet->NewBone(name);
	}

	if (_preloadCount>0 || nAnim<=PersistantLimit)
	{
		// very short animations can always be preloaded
		_phases.Realloc(nAnim);
		_phases.Resize(nAnim);
		_phaseTimes.Realloc(nAnim);
		_phaseTimes.Resize(nAnim);
    // check max. used bone index
    int bonesUsed = 0;
    for (int i=0; i<_selections.Size(); i++)
    {
      int bone = skelet->FindBone(_selections[i]);
      if (bonesUsed<bone+1) bonesUsed = bone+1;
    }
		// scan animation data
		for( int i=0; i<nAnim; i++ )
		{
      _phases[i]._matrix.Realloc(bonesUsed);
      _phases[i]._matrix.Resize(bonesUsed);
			float time = _phases[i].Load(in,skelet,nSel,reversed);
			_phaseTimes[i] = time;
		}
		// mark it as it load count in increased
		_loadCount = 1;
		#if 0 // LOG_ANIM_LOAD
			LogF("Animation %s preloaded %s",Name(),_preloadCount>0 ? "(Explicit)" :"");
		#endif
		if (_preloadCount==0) _preloadCount++;
	}
	else
	{
		// when animation is not preloaded,
		// nothing about the animation is initialized
		// even total time is not known
		_phases.Realloc(nAnim);
		_phaseTimes.Realloc(nAnim);
		/*
		_phases.Resize(1);
		_phaseTimes.Resize(1);
		// scan animation data
		float time = _phases[0].Load(in,skelet,nSel,reversed);
		_phaseTimes[0] = time;
		*/
		// nothing about the animation is initialized
		// even total time is not known
	}
}

void AnimationRT::AddLoadCount()
{
	if (_loadCount++!=0) return;
	// was zero - we have to load it
	FullLoad(_name.skeleton,_name.name);
}
void AnimationRT::ReleaseLoadCount()
{
	if (--_loadCount!=0) return;
	// is now zero - we have to release it
	FullRelease();
}

void AnimationRT::Load
(
	Skeleton *skelet, const char *file,
	bool reversed
)
{
	QIFStreamB in;
	in.AutoOpen(file);
	if (in.fail() || in.rest()<=0)
	{
		RptF("Animation %s not found or empty",file);
		// create an empty (all identities) animation
		_phases.Realloc(1);
		_phases.Resize(1);
		_phaseTimes.Realloc(1);
		_phaseTimes.Resize(1);
		// scan animation data

		AnimationRTPhase &phase = _phases[0];
		phase._matrix.Realloc(skelet->NBones());
		phase._matrix.Resize(skelet->NBones());
		// set all matrices to identity
		for (int i=0; i<skelet->NBones(); i++)
		{
			phase._matrix[i] = MIdentity;
		}
		_phaseTimes[0] = 0;
		// we have to initialize bones
		_step = VZero;
		_looped = true;
		_nPhases = 1;
    _loadCount = 0;

		return;
	}
	//_name=file;
	Load(in,skelet,file,reversed);
}

void AnimationRT::CombineTransform
(
	const WeightInfo &weights, LODShape *lShape,
	int level,
	Matrix4Array &matrices, Matrix4Par trans,
	const BlendAnimInfo *blend, int nBlend
)
{
	// transformation is given in actual coordinates
	// convert it to original (bounding center) coordinates
	Assert (lShape->BoundingCenter().SquareSize()<1e-10);
	for (int i=0; i<nBlend; i++)
	{
		const BlendAnimInfo &info = blend[i];
		// convert to matrix index 
		int mIndex = info.matrixIndex;
		if (mIndex<0) continue; // no corresponding selection found

		// animation present - blend it
		Matrix4 &mt = matrices[mIndex];
		float factor = info.factor;
		// interpolate matrix between trans and identity
		// factor is quite often 1;
		if (factor>0.99)
		{
			mt = trans * mt;
		}
		else
		{
			//Matrix4 ipol = trans*factor+Matrix4(MScale,1-factor);
			//mt = ipol * mt;

			// one matrix multiplication
			mt = trans*mt*factor+mt*(1-factor);
		}
	}

	//mb*x = (mt*(x+bc)-bc)
	//mb*x = T-bc*mt*Tbc*x
}

void AnimationRT::TransformPoint
(
	Vector3 &pos,
	const AnimationRTWeights &lWeights, Shape *shape,
	Matrix4Par trans,
	const BlendAnimInfo *blend, int nBlend, int index
)
{
	// note: no actual data from animation are needed
	// check which selection are we in
	if (index<0) return;

  const AnimationRTWeight &wg=lWeights[index];
  if( wg.Size()<=0 ) return; // this point is not animated
	
	Vector3 sPos = pos;

	pos = VZero;
	for (int i=0; i<wg.Size(); i++)
	{
		const AnimationRTPair &pair = wg[i];
		int matIndex = pair.GetSel(); // note: sel is matrix index
		float ww = pair.GetWeight();
		// check which selection corresponds to this matrix		
		// we need selection index
		int blendIndex = -1;
		for ( int bi = 0; bi<nBlend; bi++)
		{
			if (matIndex==blend[bi].matrixIndex) {blendIndex=bi;break;}
		}
		if (blendIndex<0)
		{
			// matIndex matrix not affected - factor 0
			pos += sPos*ww;
			continue;
		}

		float factor=blend[blendIndex].factor;
		// animation present - blend it
		// interpolate matrix between trans and identity
		Vector3 tPos = trans.FastTransform(sPos);
		// blend multiple matrices from wg

		pos += tPos*(factor*ww) + sPos*((1-factor)*ww);
		//LogF("  source %.3f,%.3f,%.3f",pos[0],pos[1],pos[2]);
		//LogF("    tpos %.3f,%.3f,%.3f",tPos[0],tPos[1],tPos[2]);
		//LogF("    res  %.3f,%.3f,%.3f",pos[0],pos[1],pos[2]);
	}

}

void AnimationRT::TransformMatrix
(
	Matrix4 &mat,
	const AnimationRTWeights &lWeights, Shape *shape,
	Matrix4Par trans,
	const BlendAnimInfo *blend, int nBlend, int index
)
{
	// check which selection are we in
	if (index<0) return;

  const AnimationRTWeight &wg=lWeights[index];
  if( wg.Size()<=0 ) return; // this point is not animated
	
	Fail("Function needs revision - see Transform Point");

	#if 0
	for (int i=0; i<wg.Size(); i++)
	{
		const AnimationRTPair &pair = wg[i];
		int matIndex = pair.GetSel();
		// check which selection corresponds to this matrix

		int blendIndex = -1;
		for ( int bi = 0; bi<nBlend; bi++)
		{
			if (matIndex==blend[bi].matrixIndex) {blendIndex=bi;break;}
		}
		if (blendIndex<0) continue;

		float factor=blend[blendIndex].factor;
		// animation present - blend it
		// interpolate matrix between trans and identity
		Matrix4 tMat = trans * mat;
		mat = tMat*factor + mat*(1-factor);
	}
	#endif

}

void AnimationRT::RemoveLoopFrame()
{
	if (_looped && _loadCount>0)
	{
		// remove keyframe with time>1
		for( int i=0; i<_phases.Size(); i++ )
		{
			if (_phaseTimes[i]>0.999 && i>0 )
			{
				_phases.Resize(i);
				_phaseTimes.Resize(i);
				break;
			}
		}
	}
}

void AnimationRT::SetLooped( bool looped )
{
	if (_looped==looped) return;
	_looped = looped;
	RemoveLoopFrame();
}

void Skeleton::Prepare(LODShape *lShape, WeightInfo &weights)
{
	// note: this is no longer used
	// move weigth preparation to Skeleton
	for( int level=0; level<lShape->NLevels(); level++ )
	{
		Shape *shape=lShape->Level(level);
		if( !shape ) continue;
		weights[level].Init(shape);
		for( int i=0; i<NBones(); i++ )
		{
			RStringB name = GetBone(i);
			int selIndex=shape->FindNamedSel(name);
			if( selIndex>=0 ) weights[level].AddSelection(shape,selIndex,i);
		}
		// normalize loaded weighting information
		weights[level].Normalize();
	}
}

/*!
For compatibility only.
Use Skeleton::PrepareSkeleton and AnimationRT::SetLooped instead.
*/

void AnimationRT::Prepare
(
	LODShape *lShape, Skeleton *skelet, WeightInfo &weights, bool looped
)
{
	skelet->Prepare(lShape,weights);
	SetLooped(looped);
}

/*
void AnimationRT::ForceMatrixOrientation
(
	int matIndex, const Matrix3 &orient, float factor
)
{
	if (factor<=0.01) return;
	// blend all orientation matrices in all phases with orient
	for (int p=0; p<_phases.Size(); p++)
	{
		AnimationRTPhase &phase=_phases[p];
		Matrix4 &mat = phase._matrix[matIndex];
		Matrix3 mOrient = mat.Orientation();
		mOrient = mOrient*(1-factor)+orient*factor;
		mat.SetOrientation(mOrient);
	}
}

void AnimationRT::IntroduceXStep()
{
	// introduce step back into the animation
	if( _phases.Size()<=1 ) return;
	//float invPS=1.0/_phases.Size();
	Vector3 xStep(_step[0],0,0);
	for( int i=0; i<_phases.Size(); i++ )
	{
		AnimationRTPhase &phase=_phases[i];
		Vector3 rStep=xStep*-_phaseTimes[i];
		for( int m=0; m<phase._matrix.Size(); m++ )
		{
			Matrix4 &matrix=phase._matrix[m];
			matrix.SetPosition(matrix.Position()+rStep);
		}
	}
	_step[0]=0;
}
*/

void AnimationRT::IntroduceStep()
{
	// introduce step back into the animation
	DoAssert(_preloadCount);
	if( _phases.Size()<=1 ) return;
	for( int i=0; i<_phases.Size(); i++ )
	{
		AnimationRTPhase &phase=_phases[i];
		Vector3 rStep=_step*-_phaseTimes[i];
		for( int m=0; m<phase._matrix.Size(); m++ )
		{
			Matrix4 &matrix=phase._matrix[m];
			matrix.SetPosition(matrix.Position()+rStep);
		}
	}
	_step=VZero;
}

void AnimationRT::Find( int &nextIndex, int &prevIndex, float time ) const
{
	DoAssert(_loadCount>0);
	int nIndex;
	for( nIndex=0; nIndex<_phases.Size(); nIndex++ )
	{
		float pTime = _phaseTimes[nIndex];
		if (pTime>time) break;
	}
	int pIndex=nIndex-1;
	if( pIndex<0 ) pIndex=_looped ? _phases.Size()-1 : 0;
	if( nIndex>=_phases.Size() ) nIndex=_looped ? 0 : _phases.Size()-1;
	Assert( pIndex>=0 );
	nextIndex=nIndex;
	prevIndex=pIndex;
}
	
Vector3 AnimationRT::ApplyMatricesPoint
(
	const AnimationRTWeights &lWeights, LODShape *lShape, int level,
	const Matrix4Array &matrices, int pointIndex
)
{
	Shape *shape=lShape->Level(level);
	if( !shape ) return VZero;
	shape->SaveOriginalPos();

  // apply transformations to current animation phasis
	//int npos=shape->NPos();
	Assert (lShape->BoundingCenter().SquareSize()<1e-10);
	//Vector3Val bc=lShape->BoundingCenter();
  const AnimationRTWeight &wg=lWeights[pointIndex];
	int wsize=wg.Size();
	// note: animation is calculated before BC centering

  Vector3Val pos=shape->OrigPos(pointIndex);

	// following loop certainly executes at least once
	// and typically once in simple LOD levels
	Vector3 res;
	if( wsize==1 )
	{
		const AnimationRTPair &pw=wg[0];
		// optimized for wg.Size==1
		Assert( fabs(pw.GetWeight()-1)<1e-6 );
		res=matrices[pw.GetSel()]*pos;
	}
	else if( wsize<=0 )
	{
		res=pos;
	}
	else
	{
		const AnimationRTPair &pw=wg[0];
		// do compromises between weighted transformations
		res=matrices[pw.GetSel()]*pos*pw.GetWeight();
		for( int w=1; w<wsize; w++ )
		{
			const AnimationRTPair &pw=wg[w];
			res+=matrices[pw.GetSel()]*pos*pw.GetWeight();
		}
	}
	return res;
}

/*
class Quaternion
{
	public:
	float _x,_y,_z,_w;

	Quaternion();
	Quaternion( float x, float y, float z, float w )
	:_x(x),_y(y),_z(z),_w(w)
	{}
	Quaternion operator * ( const Quaternion &val ) const;

	Matrix3 ConvertToMatrix() const;
};

Quaternion Quaternion::operator * ( const Quaternion &val ) const
{
	const Quaternion *q1 = this;
	const Quaternion *q2 = &val;

	float A, B, C, D, E, F, G, H;

	A = (q1->_w + q1->_x)*(q2->_w + q2->_x);
	B = (q1->_z - q1->_y)*(q2->_y - q2->_z);
	C = (q1->_x - q1->_w)*(q2->_y + q2->_z);
	D = (q1->_y + q1->_z)*(q2->_x - q2->_w);
	E = (q1->_x + q1->_z)*(q2->_x + q2->_y);
	F = (q1->_x - q1->_z)*(q2->_x - q2->_y);
	G = (q1->_w + q1->_y)*(q2->_w - q2->_z);
	H = (q1->_w - q1->_y)*(q2->_w + q2->_z);

	return Quaternion
	(
		 B + (-E - F + G + H) *0.5,
		 A - (E + F + G + H)*0.5,
		-C + (E - F + G - H)*0.5,
		- D + (E - F - G + H)*0.5
	);
}
*/

void AnimationRT::ApplyMatricesComplex
(
	const AnimationRTWeights &weights, LODShape *lShape, int level,
	const Matrix4Array &matrices
)
{
	Shape *shape=lShape->Level(level);
	if( !shape ) return;
	shape->SaveOriginalPos();

  // apply transformations to current animation phasis
	int npos=shape->NPos();
	Assert (lShape->BoundingCenter().SquareSize()<1e-10);
	//Vector3Val bc=lShape->BoundingCenter();
  for( int i=0; i<npos; i++ )
  {
    const AnimationRTWeight &wg=weights[i];
		int wsize=wg.Size();
		// note: animation is calculated before BC centering

    Vector3Val pos=shape->OrigPos(i);
		Vector3Val norm = shape->OrigNorm(i);
		#if _KNI
		_mm_prefetch((char *)(&shape->OrigPos(i)+2),_MM_HINT_NTA);
		_mm_prefetch((char *)(&shape->OrigNorm(i)+2),_MM_HINT_NTA);
		#endif

		// following loop certainly executes at least once
		// and typically once in simple LOD levels
		Vector3 res,resNorm;
		/*
		if( wsize==1 )
		{
			const AnimationRTPair &pw=wg[0];
			// optimized for wg.Size==1
			Assert( fabs(pw.GetWeight()-1)<1e-6 );
			Matrix4Val mat = matrices[pw.GetSel()];
			res.SetMultiply(mat,pos);
			resNorm.SetMultiply(mat.Orientation(),norm);
		}
		else
		*/
		if( wsize>0 )
		{
			const AnimationRTPair &pw=wg[0];
			// do compromises between weighted transformations
			Matrix4Val mat = matrices[pw.GetSel()];
			res.SetMultiply(mat,pos);
			resNorm.SetMultiply(mat.Orientation(),norm);
			float pww = pw.GetWeight();
			res *= pww;
			resNorm *= pww;
			for( int w=1; w<wsize; w++ )
			{
				const AnimationRTPair &pw=wg[w];
				Matrix4Val mat = matrices[pw.GetSel()];
				Vector3 add,addNorm;
				add.SetMultiply(mat,pos);
				addNorm.SetMultiply(mat.Orientation(),norm);
				float pww = pw.GetWeight();
				add *= pww;
				addNorm *= pww;
				res += add;
				resNorm += addNorm;
			}
		}
		else 
		{ // wsize ==0 
			res = pos;
			resNorm = norm;
		}


		shape->SetPos(i)=res;
		shape->SetNorm(i)=resNorm;
  }
	shape->InvalidateBuffer();
	shape->InvalidateNormals();
	lShape->InvalidateConvexComponents(level);
	/*
	if( level==lShape->FindGeometryLevel() ) lShape->InvalidateGeomComponents();
	if( level==lShape->FindFireGeometryLevel() ) lShape->InvalidateFireComponents();
	if( level==lShape->FindViewGeometryLevel() ) lShape->InvalidateViewComponents();
	*/
}

void AnimationRT::ApplyMatricesIdentity
(
	LODShape *lShape, int level
)
{
	Shape *shape=lShape->Level(level);
	if( !shape ) return;
	shape->SaveOriginalPos();

  // apply transformations to current animation phasis
	int npos=shape->NPos();
  for( int i=0; i<npos; i++ )
  {
		shape->SetPos(i)=shape->OrigPos(i);
		shape->SetNorm(i)=shape->OrigNorm(i);
  }

	shape->InvalidateBuffer();
	shape->InvalidateNormals();
	if( level==lShape->FindGeometryLevel() ) lShape->InvalidateGeomComponents();
	if( level==lShape->FindFireGeometryLevel() ) lShape->InvalidateFireComponents();
	if( level==lShape->FindViewGeometryLevel() ) lShape->InvalidateViewComponents();
}

void AnimationRT::ApplyMatricesSimple
(
	const AnimationRTWeights &weights, LODShape *lShape, int level,
	const Matrix4Array &matrices
)
{
	Shape *shape=lShape->Level(level);
	if( !shape ) return;
	shape->SaveOriginalPos();

  // apply transformations to current animation phasis
	int npos=shape->NPos();
	Assert (lShape->BoundingCenter().SquareSize()<1e-10);
	//Vector3Val bc=lShape->BoundingCenter();
	const AnimationRTWeight *a=weights.Data();
	#if 1
  for( int i=0; i<npos; i++ )
  {
		int pwsel=(*a)[0].GetSel();
    Vector3Val pos=shape->OrigPos(i);
    Vector3Val norm=shape->OrigNorm(i);
		Matrix4Val val = matrices[pwsel];
		shape->SetPos(i)=val*pos;
		shape->SetNorm(i)=val.Orientation()*norm;
		a++;
  }
	#else
	// alternative order
	int nSel=a->_selections.Size();
	for( int i=0; i<nSel; i++ )
	{
		// scan all points from this selection
		const NamedSel &sel=a->_selections[i];
		Matrix4Val matrix=matrices[i];
		// get matrix index from any selection data?
		// all points have to be assigned to the same matrix
	}
	#endif

	shape->InvalidateBuffer();
	shape->InvalidateNormals();
	if( level==lShape->FindGeometryLevel() ) lShape->InvalidateGeomComponents();
	if( level==lShape->FindFireGeometryLevel() ) lShape->InvalidateFireComponents();
	if( level==lShape->FindViewGeometryLevel() ) lShape->InvalidateViewComponents();
}


void AnimationRT::ApplyMatrices
(
	const WeightInfo &lWeights, LODShape *lShape, int level,
	const Matrix4Array &matrices
)
{
	if( matrices.Size()<=0 )
	{
		// restore original shape position
		ApplyMatricesIdentity(lShape,level);
		return;
	}
	const AnimationRTWeights &weights=lWeights[level];
	if( weights.IsSimple() )
	{
		ApplyMatricesSimple(weights,lShape,level,matrices);
	}
	else
	{
		ApplyMatricesComplex(weights,lShape,level,matrices);
	}
}

void AnimationRT::Apply
(
	const WeightInfo &lWeights, LODShape *lShape, int level, float time
) const
{
	if (_nPhases<=0) return; // not animated

	Shape *shape=lShape->Level(level);
	if( !shape ) return;

	MATRIX_4_ARRAY(matrix,128);
	PrepareMatrices(matrix,time,1);

	ApplyMatrices(lWeights,lShape,level,matrix);
}

/*!
\patch 1.96 Date 1/24/2004 by Ondra
- Fixed: SCUD animation caused strange visual behaviour (introduced in 1.95)
*/

void AnimationRT::PrepareMatrices
(
	Matrix4Array &matrix, float time, float factor
) const
{
	if (_nPhases<=0) return; // not animated
	if (factor<0.01f) return;

	ScopeLock<AnimationRT> load(*this);
	// cache this animation
	GAnimationRTCache.Load(const_cast<AnimationRT *>(this));

	int nextIndex,prevIndex;

	// select nearest animation phasis
	Find(nextIndex,prevIndex,time);
	
	// interpolate between phases
	const AnimationRTPhase &prevPhaseO=_phases[prevIndex];
	const AnimationRTPhase &nextPhaseO=_phases[nextIndex];

	int nMatricesSkelet = _name.skeleton->NBones();
  int nMatrices = prevPhaseO._matrix.Size();
	bool first=false;
	if( matrix.Size()==0 )
	{
		//matrix.Realloc(nMatrices);
		matrix.Resize(nMatricesSkelet);
		#if _KNI
			DoAssert( ((int)matrix.Data()&15)==0 );
		#endif
		first=true;
		//LogF("First anim %s, %d",(const char *)GetName(),nMatrices);
	}
  else if (nMatricesSkelet>matrix.Size())
  {
    // this animation needs some matrices not allocated yet
    int oldSize = matrix.Size();
    matrix.Resize(nMatricesSkelet);
    for (int i=oldSize; i<nMatricesSkelet; i++)
    {
      matrix[i] = MIdentity;
    }
  }
  // some matrices may be allocated which are not used by this animation
  // this does not matter - they are already initialized and will be left untouched
  
	float nextTime = _phaseTimes[nextIndex];
	float prevTime = _phaseTimes[prevIndex];
	if( nextTime<=time ) nextTime+=1;
	if( prevTime>time ) prevTime-=1;

	float interpol = nextTime>prevTime ? (time-prevTime)/(nextTime-prevTime) : 1;

	// create interpolated matrices
	float f1=(1-interpol)*factor;
	float f2=interpol*factor;

	if( first )
	{
		for( int i=0; i<nMatrices; i++ )
		{
			matrix[i].InlineSetMultiply(prevPhaseO._matrix[i],f1);
			matrix[i].InlineAddMultiply(nextPhaseO._matrix[i],f2);
		}
		for( int i=nMatrices; i<nMatricesSkelet; i++ )
    {
			matrix[i].InlineSetMultiply(MIdentity,factor);
    }
	}
	else
	{
		for( int i=0; i<nMatrices; i++ )
		{
			matrix[i].InlineAddMultiply(prevPhaseO._matrix[i],f1);
			matrix[i].InlineAddMultiply(nextPhaseO._matrix[i],f2);
		}
		for( int i=nMatrices; i<nMatricesSkelet; i++ )
    {
			matrix[i].InlineAddMultiply(MIdentity,factor);
    }
	}
}

Vector3 AnimationRT::Point
(
	const WeightInfo &lWeights, LODShape *lShape, int level,
	float time, int index
) const
{
	if (index<0) return VZero;
	Shape *shape=lShape->LevelOpaque(level);
	if (_nPhases<=0) return shape->Pos(index); // not animated

	const AnimationRTWeights &weights=lWeights[level];
  const AnimationRTWeight &wg=weights[index];
  if( wg.Size()<=0 ) return shape->Pos(index); // this point is not animated

	ScopeLock<AnimationRT> load(*this);
	// cache this animation
	GAnimationRTCache.Load(const_cast<AnimationRT *>(this));

	// select nearest animation phasis
	int nextIndex,prevIndex;
	Find(nextIndex,prevIndex,time);
	// interpolate between phases
	const AnimationRTPhase &prevPhase=_phases[prevIndex];
	const AnimationRTPhase &nextPhase=_phases[nextIndex];

	float nextTime = _phaseTimes[nextIndex];
	float prevTime = _phaseTimes[prevIndex];
	if( nextTime<=time ) nextTime+=1;
	if( prevTime>time ) prevTime-=1;
	//Assert( nextTime>=time );
	//Assert( prevTime<=time );
	float interpol = nextTime>prevTime ? (time-prevTime)/(nextTime-prevTime) : 1;

	// create interpolated matrices (only those needed)
	// and immediatelly apply them
	shape->SaveOriginalPos();

	Assert (lShape->BoundingCenter().SquareSize()<1e-10);
  Vector3 pos=shape->OrigPos(index);

  Vector3 res=VZero;
	float f1=1-interpol;
	float f2=interpol;
  for( int w=0; w<wg.Size(); w++ )
  {
		const AnimationRTPair &pw=wg[w];
		int i=pw.GetSel();
		float wg=pw.GetWeight();
		//res += (prevPhase._matrix[i]*(f1*wg)+nextPhase._matrix[i]*(f2*wg))*pos;
		// optimized temporary allocation
		Matrix4 pm,nm,cm;
		pm.InlineSetMultiply(prevPhase._matrix[i],f1*wg);
		nm.InlineSetMultiply(nextPhase._matrix[i],f2*wg);
		cm.InlineSetAdd(pm,nm);
		// TODO: optimize more
		res += cm*pos;

  }

	Assert (lShape->BoundingCenter().SquareSize()<1e-10);
	return res;
}

void AnimationRT::Matrix
(
	Matrix4 &mat, float time, const AnimationRTWeight &wgt
) const
{
	if (wgt.Size()<0) return;
	if (_nPhases<=0) return; // not animated

	ScopeLock<AnimationRT> load(*this);
	// cache this animation
	GAnimationRTCache.Load(const_cast<AnimationRT *>(this));

	// select nearest animation phasis
	int nextIndex,prevIndex;
	Find(nextIndex,prevIndex,time);
	// interpolate between phases
	const AnimationRTPhase &prevPhase=_phases[prevIndex];
	const AnimationRTPhase &nextPhase=_phases[nextIndex];

	float nextTime = _phaseTimes[nextIndex];
	float prevTime = _phaseTimes[prevIndex];
	if( nextTime<=time ) nextTime+=1;
	if( prevTime>time ) prevTime-=1;
	//Assert( nextTime>=time );
	//Assert( prevTime<=time );
	float interpol = nextTime>prevTime ? (time-prevTime)/(nextTime-prevTime) : 1;

	// create interpolated matrices (only those needed)
	// and immediatelly apply them

  Vector3 res=VZero;
	float f1=1-interpol;
	float f2=interpol;
	Matrix4 oMat = mat;
	mat = MZero;
	for (int w=0; w<wgt.Size(); w++)
	{
		const AnimationRTPair &pw=wgt[w];
		int i=pw.GetSel();
		float wg=pw.GetWeight();

    if (i>=prevPhase._matrix.Size())
    {
		  mat += oMat*wg;
    }
    else
    {
		  // optimized temporary allocation
		  Matrix4 pm,nm,cm;
		  pm.InlineSetMultiply(prevPhase._matrix[i],f1*wg);
		  nm.InlineSetMultiply(nextPhase._matrix[i],f2*wg);
		  cm.InlineSetAdd(pm,nm);
		  mat += cm*oMat;
    }
	}
}

BankArray<Skeleton> Skeletons;
