#ifdef _MSC_VER
#pragma once
#endif

#ifndef _ENV_TRACKER_HPP
#define _ENV_TRACKER_HPP

#include "time.hpp"

#ifdef PREPROCESS_DOCUMENTATION
#include "networkObject.hpp"
#endif

class SurroundTracker
{
	Vector3 _lastPos;
	Time _lastTime;
	float _value;

	public:
	SurroundTracker();

	void Update( const Object *who, Vector3Par pos, float radius, float minObstacle );

	float Track( const Object *who, Vector3Par pos, float radius, float minObstacle );
	float GetDensity() const {return _value;}
};

#endif
