/*!
	\file
	Implementation of EntityAI functions related to target tracking,
	target list management and weapon selection
*/

#include "wpch.hpp"

#include "vehicleAI.hpp"
#include "person.hpp"
#include "global.hpp"
#include <El/Common/randomGen.hpp>
#include "paramArchive.hpp"
#include "world.hpp"
#include "ai.hpp"
#include "landscape.hpp"
#include "visibility.hpp"

#include <El/Common/perfLog.hpp>
#include "perfProf.hpp"

#if _ENABLE_CHEATS

#define DIAG_RESULT 0
#define DIAG_SHOOT_RESULT 0
#define DIAG_ATTACK 0

#define DIAG_TARGET 0 // SelectFireWeapon function (0..3)

#define LOG_TARGETS 0

#endif

const float ValidTime=5.0f;

/*!
\patch 1.28 Date 10/24/2001 by Ondra.
- Fixed: AI was not able to fire at armored training target.
*/
const float MinVisibleFire=0.63f;

float Target::FadingSpotability() const
{
	// apply accuracy rules
	float old=Glob.time-spotabilityTime-ValidTime-1;
	if( old<0 ) return spotability;
	float ret=spotability-old*(1.0f/30);
	saturate(ret,0,4);
	return ret;
}

/*!
\patch 1.01 Date 6/26/2001 by Ondra.
- Fixed: slower forgeting target type and side.
*/

float Target::FadingAccuracy() const
{
	// apply accuracy rules
	// FIX: 1.01: was 120 instead of 240
	float fade=1-(Glob.time-accuracyTime-ValidTime-1)*(1.0f/240);
	saturate(fade,0,1);
	return accuracy*fade;
}

float Target::FadingSideAccuracy() const
{
	// apply accuracy rules
	// FIX: 1.01: was 120 instead of 240
	float fade=1-(Glob.time-sideAccuracyTime-ValidTime-1)*(1.0f/240);
	saturate(fade,0,1);
	//Assert( sideAccuracy*fade+0.01>=FadingAccuracy() );
	return sideAccuracy*fade;
}

DEFINE_FAST_ALLOCATOR(Target)

Target::Target( AIGroup *grp )
:group(grp)
{
	Init();
}

void Target::Init()
{
	position=VZero;
	posError=Vector3
	(
		GRandGen.RandomValue()*400-200,
		GRandGen.RandomValue()*20-10,
		GRandGen.RandomValue()*400-200
	);
	speed=VZero;
	side=TSideUnknown;
	sideChecked = false;
	type=NULL;
	//for( int i=0; i<MAX_UNITS_PER_GROUP; i++ ) visibilityCheck[i]=Time(0);
	spotability=0; // fading
	spotabilityTime=Time(0);
	//visibility=0; // fading
	//visibilityTime=Time(0);
	accuracy=0; // fading
	accuracyTime=Time(0);
	sideAccuracy=0; // fading
	sideAccuracyTime=Time(0);
	isKnown=false; // do we remmember it
	//isKnownDelayed=false; // do we remmember it
	posReported=VZero;
	timeReported = TIME_MIN;
	vanished=false; // send disappeared units until disappearConfirmed
	destroyed=false; // send confirmed kill
	idExact=NULL;
	idSensor=NULL;
	delay=TIME_MAX; // never seen
	delaySensor=TIME_MAX;
	lastSeen=TIME_MIN;
	dammagePerMinute=0;
	subjectiveCost=0;
}

bool Target::IsKnownBy( AIUnit *unit ) const
{
	// known by specific group member
	if( !isKnown ) return false;
	if( delay<=Glob.time ) return true;
	EntityAI *vehicle = unit->GetPerson();
	if( vehicle!=idSensor ) return false;
	return delaySensor<=Glob.time;
}

bool Target::IsKnownByAll() const
{
	// known by all group members
	return isKnown && delay<Glob.time;
}

bool Target::IsKnownBySome() const
{
	// know by at least one group member
	return isKnown && ( delay<=Glob.time || delaySensor<=Glob.time );
}

bool Target::IsKnown() const
{
	return IsKnownBySome();
}

void Target::LimitError( float error )
{
	if( posError.SquareSize()>Square(error)  )
	{
		posError=posError.Normalized()*error*0.999f;
		#if 0
		LogF
		(
			"Limit error %s, %.1f",
			idExact ? (const char *)idExact->GetDebugName() : "<null>",
			error
		);
		#endif
	}
}

// TODO: member functions

Vector3 Target::ExactAimingPosition() const
{
	// calculate position directly is target is fresh known
	// if it is never seen (not isKnown), position is complete crap
	Object *exact=idExact;
	if( exact )
	{
		// seen very recently? If yes, track actual position
		// if it is friendly, the delay may be much larger
		if( Glob.time<lastSeen+1.0f ) return exact->AimingPosition();
	}
	// estimate target position
	float timeFromSeen=Glob.time.Diff(lastSeen);
	saturateMin(timeFromSeen,10);
	return position+speed*timeFromSeen;
}

/*!
\patch 1.17	Date 8/14/2001 by Ondra.
- Fixed: AI not attacking on weapon-less targets.
*/
static bool IsCombatUnit(EntityAI *veh)
{
	if (veh->NMagazines()>0) return true;
	if (veh->NMagazineSlots()<=0) return false;
	// we do not have magazines, but we have some magazine slots
	// check if we have some real weapon
	for (int i=0; i<veh->NMagazineSlots(); i++)
	{
		const MagazineSlot &slot = veh->GetMagazineSlot(i);
		//int mode = slot._mode;
		const MuzzleType *muzzle = slot._muzzle;
		if (muzzle->_magazines.Size()>0)
		{
			return true;
		}

	}
	return false;
}

TargetState Target::State(AIUnit *sensor) const
{
	// check if target is alive
	if (destroyed) return TargetDestroyed;
	if (vanished) return TargetDestroyed;
	if (!sensor) return TargetAlive;
	AIGroup *grp = sensor->GetGroup();
	if (!grp) return TargetDestroyed;
	AICenter *center = grp->GetCenter();
	if (!center) return TargetDestroyed;
	if (!center->IsEnemy(side)) return TargetAlive;
	if (!idExact) return TargetEnemyEmpty;
	if (!idExact->IsAbleToMove()) return TargetEnemyEmpty;
	// { FIX weapon-less target
	// if target has no weapons, ignore it cannot fire
	// and still consider it combatant
	if (IsCombatUnit(idExact))
	// } FIX
	{
		if (!idExact->IsAbleToFire()) return TargetEnemy;
	}
	return TargetEnemyCombat;
}

Vector3 Target::LandAimingPosition() const
{
	Object *exact=idExact;
	if( exact )
	{
		// check if we see it actualy
		if( Glob.time<lastSeen+1.0f ) return exact->AimingPosition();
	}

	Vector3 pos = ExactAimingPosition();
	// check last actually seen position above surface
	float aboveSurface=position.Y()-GLandscape->SurfaceYAboveWater(position.X(),position.Z());
	pos+=posError;
	pos[1]=GLandscape->SurfaceYAboveWater(pos.X(),pos.Z())+aboveSurface;
	return pos;
}

Vector3 Target::AimingPosition() const
{
	// calculate position directly is target is fresh known
	return ExactAimingPosition()+posError;
}

float Target::VisibleSize() const
{
	Object *exact=idExact;
	if( exact )
	{
		return exact->VisibleSize();
	}
	LODShapeWithShadow *shape = type->GetShape();
	if( !shape )
	{
		RptF("type %s - no shape",(const char *)type->GetName());
		return 0.5f;
	}
	return shape->BoundingSphere()*0.5f;
}

LSError Target::Serialize(ParamArchive &ar)
{
	if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst) Init();

	CHECK(ar.SerializeRef("idExact", idExact, 1))
	CHECK(ar.SerializeRef("idSensor", idSensor, 1))
	CHECK(ar.SerializeRef("idKiller", idKiller, 1))
	CHECK(ar.SerializeRef("group", group, 1))

	CHECK(ar.Serialize("position", position, 1))
	CHECK(ar.Serialize("posError", posError, 1, VZero))
	CHECK(ar.Serialize("speed", speed, 1, VZero))
	CHECK(ar.SerializeEnum("side", side, 1, TSideUnknown))
	CHECK(ar.Serialize("sideChecked", sideChecked, 1, true))
	CHECK(::Serialize(ar, "type", type, 1))


	CHECK(ar.Serialize("spotability",spotability,1,4.0));
	CHECK(ar.Serialize("spotabilityTime",spotabilityTime,1));

	//CHECK(ar.Serialize("visibility",visibility,1,4.0));
	//CHECK(ar.Serialize("visibilityTime",visibilityTime,1));

	CHECK(ar.Serialize("accuracy",accuracy,1,4.0));
	CHECK(ar.Serialize("accuracyTime",accuracyTime,1));

	CHECK(ar.Serialize("sideAccuracy",sideAccuracy,1,4.0));
	CHECK(ar.Serialize("sideAccuracyTime",sideAccuracyTime,1));

	CHECK(ar.Serialize("delay",delay,1, Time(0)));
	CHECK(ar.Serialize("delaySensor",delaySensor,1, Time(0)));
	CHECK(ar.Serialize("lastSeen",lastSeen,1, TIME_MIN));

	//CHECK(ar.Serialize("isKnownDelayed",isKnownDelayed,1, false));

	CHECK (ar.Serialize("isKnown",isKnown,1, false));
	CHECK (ar.Serialize("vanished",vanished,1, false));
	CHECK (ar.Serialize("destroyed",destroyed,1, false));
	CHECK (ar.Serialize("timeReported",timeReported,1, TIME_MIN));
	CHECK (ar.Serialize("posReported",posReported,1, VZero));

	CHECK(ar.Serialize("dammagePerMinute",dammagePerMinute,1,0.0));
	CHECK(ar.Serialize("subjectiveCost",subjectiveCost,1,0.0));
	
	return LSOK;
}

Target *Target::LoadRef(ParamArchive &ar)
{
	TargetSide side = TSideUnknown;
	int idGroup;
	int index;
	if (ar.SerializeEnum("side", side, 1) != LSOK) return NULL;
	if (ar.Serialize("idGroup", idGroup, 1) != LSOK) return NULL;
	if (ar.Serialize("index", index, 1) != LSOK) return NULL;
	AICenter *center = GWorld->GetCenter(side);
	if (!center) return NULL;
	AIGroup *group = NULL;
	for (int i=0; i<center->NGroups(); i++)
	{
		AIGroup *grp = center->GetGroup(i);
		if (grp && grp->ID() == idGroup)
		{
			group = grp;
			break; 
		}
	}
	if (!group) return NULL;
	if (index<0) return NULL;
	const TargetList &list=group->GetTargetList();
	return list[index];
}

LSError Target::SaveRef(ParamArchive &ar)
{
	AIGroup *grp = group;
	AICenter *center = grp ? grp->GetCenter() : NULL;
	TargetSide side = center ? center->GetSide() : TSideUnknown;
	int idGroup = grp ? grp->ID() : -1;
	int index = -1;
	if (grp)
	{
		const TargetList &list=grp->GetTargetList();
		for (int i=0; i<list.Size(); i++)
		{
			Target *tgt=list[i];
			if (tgt==this)
			{
				index = i;
				break;
			}
		}
	}
	CHECK(ar.SerializeEnum("side", side, 1))
	CHECK(ar.Serialize("idGroup", idGroup, 1))
	CHECK(ar.Serialize("index", index, 1))
	return LSOK;
}

void TargetList::Manage( AIGroup *group )
{
	for( int s=0; s<Size(); )
	{
		Target *tVis=Get(s);

		// check if information is not too old
		EntityAI *ai = tVis->idExact;
		if( !ai )
		{
			// target object disappeared
			Delete(s);
			continue;
		}

		// check if some group vehicle can see it
		bool canSee=false;
		for( int u=1; u<=MAX_UNITS_PER_GROUP; u++ )
		{
			AIUnit *unit=group->UnitWithID(u);
			if( !unit ) continue;
			if( !unit->IsUnit() ) continue;
			EntityAI *veh=unit->GetVehicle();
			if (!veh)
			{
				ErrF("Unit %s has no vehicle",(const char *)unit->GetDebugName());
				continue;
			}
			float dist2=veh->Position().Distance2(ai->Position());

			float irRange = veh->GetType()->GetIRScanRange();
			if (!ai || !ai->GetType()->GetIRTarget()) irRange =0;
			else if (!veh->GetType()->GetIRScanGround() && !ai->Airborne()) irRange = 0;

			float maxDistance2=Square(floatMax(irRange,TACTICAL_VISIBILITY));
			if( dist2<maxDistance2 )
			{
				canSee=true;
				break;
			}
		}
		if( !canSee )
		{
			Delete(s);
			continue;
		}

		if( tVis->isKnown )
		{
			// handle vanishing units
			tVis->vanished = !ai->IsInLandscape();
		}
		else
		{
			// delete unknown units that got-in something
			if( !ai->IsInLandscape() )
			{
				Delete(s);
				continue;
			}
		}
		s++;
	}
}

int TargetList::Find( TargetType *obj ) const
{
	for( int i=0; i<Size(); i++ )
	{
		if( Get(i)->idExact==obj ) return i;
	}
	return -1;
}



void EntityAI::AddNewTargets(TargetList &res, bool initialize)
{
	float nearestEnemyDist2 = 1e10;

	_newTargetsTime=Glob.time;

	PROFILE_SCOPE(tgNew);

	AIUnit *brain=CommanderUnit();
	if( !brain ) return;
	AIGroup *group=brain->GetGroup();
	if( !group ) return;
	AICenter *center=group->GetCenter();
	if( !center ) return;

	//Person *me=brain->GetPerson();

	// check which vehicles are visible from this one

	float irRange=GetType()->GetIRScanRange();
	const float maxVisibility2=Square(floatMax(TACTICAL_VISIBILITY,irRange));

	//float nearestEnemy2=1e10;
	int nv=GLOB_WORLD->NVehicles();
	int nb=GLOB_WORLD->NBuildings();
	for( int i=0; i<nv+nb; i++ )
	{
		Vehicle *vehicle=i<nv ? GLOB_WORLD->GetVehicle(i) : GLOB_WORLD->GetBuilding(i-nv);
		//if( vehicle->GetType()!=TypeVehicle ) continue; // no collisions with temporaries
		float dist2=vehicle->Position().Distance2Inline(Position());
		if( dist2>maxVisibility2 ) continue;
		// note: max range may be different for different vehicles

		// ignore tempraty vehicles (tracks...)
		EntityAI *ai=dyn_cast<EntityAI>(vehicle);
		if( !ai ) continue;

		const EntityAIType *type = ai->GetType();
		if(dist2>Square(TACTICAL_VISIBILITY))
		{
			// far target - check if it can be seen by radar
			if
			(
				!type->GetIRTarget() ||
				!GetType()->GetIRScanGround() && !ai->Airborne()
			)
			{
				continue;
			}
		}

		// record all (even invisible)  vehicles
		// out vehicles - if not in list, no need to add them
		if (!ai->IsInLandscape()) continue;
		// check if it is enemy
		if (center->IsEnemy(ai->GetTargetSide()))
		{
			saturateMin(nearestEnemyDist2,dist2);
		}

		// check if it is already in list
		int index=0;
		for( ; index<res.Size(); index++ )
		{
			Target *target=res[index];
			EntityAI *obj=target->idExact;
			if( obj==ai ) break;
		}
		if( index>=res.Size() )
		{
			Target *target=new Target(group);
			res.Add(target);
			// it is visible - update state information
			target->position=ai->AimingPosition();
			target->speed=ai->Speed();
			// TODO: error based on visibility
			target->sideChecked = false;
			target->side = TSideUnknown;
			target->type=ai->GetType(0); // unknown type
			//target->dammage=ai->GetTotalDammage();
			//target->idSensor=this;
			target->idExact=ai;
			//target->visibilityTime=Glob.time-60;
			target->spotabilityTime=Glob.time-60;
			target->accuracyTime=Glob.time-60;
			target->sideAccuracyTime=Glob.time-60;
			//target->visibility=0;
			target->accuracy=0;
			target->sideAccuracy=0;
			target->spotability=0;
			target->isKnown=false;
			//target->isKnownDelayed=false;
			target->timeReported=TIME_MIN;
			target->posReported = VZero;
			target->destroyed=false;
			target->vanished=false;
			#if LOG_TARGETS
				LogF
				(
					"New target %s %s",
					(const char *)GetDebugName(),(const char *)ai->GetDebugName()
				);
			#endif
		}
	}
	brain->SetNearestEnemyDist2(nearestEnemyDist2);
}

static void TargetSeesItself
(
	Target * target, EntityAI *ai, AIUnit *unit, float trackTargetsPeriod
)
{ // anyone sees himself perfectly
	target->type=ai->GetType(4);
	target->side = ai->GetTargetSide(4);
	target->sideChecked = true;
	target->spotability=4;
	target->accuracy=4;
	target->sideAccuracy=4;
	target->spotabilityTime=Glob.time;
	target->accuracyTime=Glob.time;
	target->sideAccuracyTime=Glob.time;
	target->isKnown=true;
	// it will almost certainly see itself until next sample
	target->lastSeen=Glob.time+trackTargetsPeriod;
	target->idSensor=unit->GetPerson();
	target->delay=Glob.time-5; // no delay
	target->delaySensor=Glob.time-5; // no delay
	target->position=ai->AimingPosition();
	target->posError=VZero;
	target->speed=ai->Speed();
	target->timeReported=Glob.time; // no need to report itself
	target->posReported = target->position;
}

/*!
\param ai target considered
\param dist2 square distance to target
\param audibility (optional) pointer receiving target audibility
\return
0 when full occluded, 1 when fully visible
Multiplied by inverze square distance and constant factor
\patch 1.22 Date 8/27/2001 by Ondra.
- Fixed: AI Sound perception less decreased by obstructions.
\patch 1.27 Date 10/18/2001 by Ondra.
- Fixed: AI hearing through solid objects was too good.
\patch_internal 1.22 Date 8/27/2001 by Ondra.
- New: Separate audibility / visibility calculation.
*/

float EntityAI::CalcVisibility
(
	EntityAI *ai, float dist2,  float *audibility, bool assumeLOS
)
{
	if (audibility) *audibility = 0;

	AIUnit *brain=CommanderUnit();
	if (!brain || brain->GetLifeState()!=AIUnit::LSAlive)
	{
		// dead unit cannot see anything
		return 0;
	}

	float vis = 1;
	const EntityAIType *type = GetType();
	float irRange = type->GetIRScanRange();
	if (ai->GetType()->GetLaserTarget())
	{
		// laser targets require special processing
		if (!type->GetLaserScanner()) return 0;
	}
	bool irScan= dist2<Square(irRange) && ai->GetType()->GetIRTarget();
	if (!type->GetIRScanGround() && !ai->Airborne()) irScan = false;
	if( !irScan )
	{ // no IR - use eye
		// check three zones
		// all dir zone (360 deg)
		// eye zone (120 deg)
		// optics zone (4 deg???)
		if( dist2>Square(TACTICAL_VISIBILITY) ) vis=0;
		vis=1-GLOB_SCENE->TacticalFog8(dist2)*(1.0f/255);
	}
	else
	{ // use IR
		if( dist2>Square(irRange) ) vis=0;
		else
		{
			float dist=dist2*InvSqrt(dist2); // sqrt(dist2)
			vis=1-dist/irRange; // TODO: avoid division
		}
		//saturate(vis,0,1);
	}

	// Note: aiRadius shoule really be VisibleSize
	// but it was discovered too late (1.28)
	// and impact of fixing it would be too unpredictable
	//float aiRadius=ai->VisibleSize();
	// size is used in calculations from VisibleMovement instead
	// see also Man::VisibleMovement and Man::VisibleSize
	float aiRadius=ai->GetRadius();
	float landVis=assumeLOS ? 1 : GWorld->Visibility(brain,ai);

	if (ai->GetType()->GetLaserTarget())
	{
		aiRadius = 160;
	}
	float radiusCoef=5000;
	if( irScan ) radiusCoef*=25;
	float sizeVis = dist2 > 0 ? aiRadius * radiusCoef / dist2 : 100;
	// FIX: near hearing/seeing
	saturateMin(sizeVis,40);

	vis*=sizeVis;
	vis*=landVis;

	if (audibility)
	{
		// calculate audibility
		float aud = 1;

		// units can hear even behind obstructions to some extent
		// we assume landVis is between 0 and 1
		Assert (landVis>=0 && landVis<=1);
		float landAud = 1 - (1-landVis)*0.90f;
		float audRadiusCoef=5000;
		float audRadis = ai->GetRadius();
		float sizeAud = dist2 > 0 ? audRadis * audRadiusCoef / dist2 : 1000;
		saturateMin(sizeAud,240);

		aud*=sizeAud;
		aud*=landAud;

		*audibility = aud;
	}
	return vis;
}

//#if _ENABLE_CHEATS
//#include "statistics.hpp"
//#endif

/*!
\patch 1.01 Date 06/11/2001 by Ondra
- Fixed: too low visibility/spotability of near targets.
- Most problems were related to too strict clamping of near values
- Following updates were made:
- near spotability (day & night) is more sensitive, especially on lying soldiers
- night vision is more sensitive
- recognition in night is less sensitive
\patch 1.01	Date 6/26/2001 by Ondra.
- Fixed: Too fast target forgetting.
\patch 1.01	Date 6/26/2001 by Ondra.
- Improved: AI takes NV goggles on/off.
\patch 1.27	Date 10/18/2001 by Ondra.
- Fixed: AI ability to distinguish enemy from friendly by ear was too good.
\patch 1.28	Date 10/19/2001 by Ondra.
- Improved: AI reaction to weapon sound.
\patch 1.28	Date 10/22/2001 by Ondra.
- Fixed: AI: When hearing target that has been seen and then not seen
for some time, position information was too accurate.
\patch 1.28	Date 10/22/2001 by Ondra.
- Fixed: AI: When target was very loud, is was sometimes
considered audible but not visible.
\patch 1.30 Date 11/02/2001 by Ondra.
- Fixed: Vehicles destroyed and later repaired with setdammage 0
were considered dead by AI.
\patch 1.33 Date 11/28/2001 by Ondra.
- Fixed: Detecting laser targets not in front of vehicle.
\patch 1.33 Date 11/30/2001 by Ondra.
- Improved: Low skill soldier have less sensitive eyes and ears.
This make game easier when playing against low skill enemy and
makes difference between low and high skill more important.
\patch_internal 1.59 Date 5/22/2002 by Ondra
- Fixed: AI sensitivity was very high, seeing even back (introduced in 1.58).
\patch 1.85 Date 9/21/2002 by Ondra
- Fixed: More realistic side detection by AI, should make stealing enemy equipment easier.
\patch 1.85 Date 9/21/2002 by Ondra
- Fixed: AI often fired on empty targets.
*/

void EntityAI::TrackTargets
(
	TargetList &res, AIUnit *unit, int canSee,
	bool initialize, float maxDist,
	float trackTargetsPeriod
)
{
	if (!canSee) return; // nothing to test

	PROFILE_SCOPE(tgTrk);

	// track targets for single unit (gunner or commander)
	// check config to know who sees what
	AIGroup *group=unit->GetGroup();
	if( !group ) return;
	AICenter *center=group->GetCenter();
	if( !center ) return;

	// check which vehicles are visible from this one
	//const float maxVisibility=TACTICAL_VISIBILITY;
	
	Assert( GLOB_WORLD->CheckVehicleStructure() );

	//int id=brain->ID()-1;


	// set default zoom - corresponding to r-button zoom
	float opticsFov = 0.30;
	float opticsZoom = 1;
	float cosOpt = 0.95393920142; // no optics - cos 17 deg
	float cosEye=0.7f; // cos 45 deg
	float cosEyeFocus = 0.96592582629f; // cos 15 deg (r-button zoom ability)

	if (canSee&CanSeeOptics)
	{
		// different vehicles have different optics zoom
		// check max. zoom or check zoom of current weapon
		//if (unit->IsPlayer())
		if (_currentWeapon>=0 && EnableViewThroughOptics())
		{
			// if we are manual, check current optics
			// TODO: check if current weapon is binocular
			// if it is, check if binoculars are enabled
			const MagazineSlot &slot = GetMagazineSlot(_currentWeapon);
			if (slot._muzzle) opticsFov = slot._muzzle->_opticsZoomMax;
		}

		if (opticsFov<0.3)
		{
			// calculate max. cos based on fov
			// fov is sin of angle
			cosOpt = sqrt(1-opticsFov*opticsFov);
			// calculate zoom in terms of area
			opticsZoom = Square(floatMax(0.30/opticsFov,1));
			/*
			LogF
			(
				"%s: fov %.2f, zoom %.2f, cos %.3f, ang %.3f",
				(const char *)GetDebugName(),
				opticsFov,opticsZoom,cosOpt,acos(cosOpt)*(180/H_PI)
			);
			*/
		}
	}

	if (!(canSee&CanSeeEye))
	{
		// some units cannot see with eye, disable eye to optics level
		// if there is no optices, eye is disabled completely
		cosEye = cosOpt;
	}

	float sensitivityEar = GetType()->_sensitivityEar;
	if (!(canSee&CanSeeEar))
	{
		sensitivityEar = 0;
	}

	bool notManual = unit->HasAI() && !unit->GetPerson()->IsRemotePlayer();
	float nearestEnemy2=1e10;

	float irRange = GetType()->GetIRScanRange();
	if (!(canSee&CanSeeRadar)) irRange=0;

	saturateMax(maxDist,irRange);
	saturateMax(maxDist,TACTICAL_VISIBILITY);


	const LightSun *sun = GScene->MainLight();
	float night=sun->NightEffect();
	if (night>0)
	{
		// check light intensity
		float dark = 1.02-sun->Ambient().Brightness()-sun->Diffuse().Brightness();
		saturateMin(night,dark);

		// check if soldier has NV goggles
		//bool hasNV = unit->GetPerson()->IsNVEnabled() || GetType()->GetNightVision();
		Person *person = unit->GetPerson();
		bool hasNV =
		(
			person->IsNVEnabled() && person->IsNVWanted() || GetType()->GetNightVision()
		);

		if (hasNV)
		{
			saturateMin(night,0.75f);
		}
		else
		{
			saturateMin(night,0.97f);
		}
	}

	Vector3 wepDir = Direction();
	if (_currentWeapon>=0)
	{
		wepDir = GetWeaponDirection(_currentWeapon);
	}


	for( int index=0; index<res.Size(); index++ )
	{
		Target *target=res[index];
		EntityAI *ai=target->idExact;
		if( !ai ) continue;


		// update visibility information
		// distant objects are less visible
		if( ai==this )
		{
			TargetSeesItself(target,ai,unit,trackTargetsPeriod);
			continue;
		}
		if (!ai->IsInLandscape()) continue; // cannot be visible - is not in landscape

		// check if target is within tracked range
		float dist2=ai->Position().Distance2Inline(Position());
		if (dist2>Square(maxDist)) continue;

		// AI FIX in 1.33
		// {{ distance is increased for units with low skill
		float incDist2 = 1+(unit->GetInvAbility()-1)*0.4f;
		dist2 *= incDist2;
		if (dist2>Square(maxDist)) continue;
		// }}

		bool laserContact =
		(
			ai->GetType()->GetLaserTarget() && GetType()->GetLaserScanner()
		);
		bool radarContact=
		(
			dist2<Square(irRange) && ai->GetType()->GetIRTarget() &&
			(GetType()->GetIRScanGround() || ai->Airborne())
		);

		// variable spotability limit depending on unit "busy" status
		Target *assigned = unit->GetTargetAssigned();
		float spotTreshold = assigned ? 0.75f : 0.5f;
		if (!notManual)
		{
			// player auto-spotting has more strict conditions
			// currently condition for veteran mode is never auto-spot
			// we can autospot friendly or neutral targets
			// they are never reported in radio
			TargetSide aiSide = ai->GetTargetSide();

			spotTreshold = Glob.config.IsEnabled(DTAutoSpot) ? 0.75f : 100.0f; 

			if
			(
				center->IsFriendly(aiSide) ||
				center->IsNeutral(aiSide)
			)
			{
				// override player auto-report
				// but leave thold a little bit stricter than normal
				saturateMin(spotTreshold,0.6);
			}
		}

		float movement = ai->VisibleMovement();
		float aiAudible = ai->Audible();

		#if 0
		// simple heuristics to determine max. possible spotability
		// Note: optimization like this may be quite important
		// but is is risky, as it could introduce hard-to-debug bugs

		if (!radarContact && !laserContact)
		{

			float audibility = 0;
			float visibility = 0;
			if (!ai->GetType()->GetLaserTarget() || GetType()->GetLaserScanner())
			{
				visibility = CalcVisibility(ai,dist2,&audibility,true);
			}

			float actSpotabilityEye=visibility;
			float actSpotabilityEar=audibility;

			saturate(visibility,0,10);
			saturate(audibility,0,120);

			// check if object is visible
			
			const VehicleType *aiType=ai->GetType();
			float spotCoef=1;
			float lightCoef=1;

			if (night>0)
			{
				
				float lightsOnOff=ai->VisibleLights();
				float nightSpotable=
				(
					aiType->GetSpotableNightLightsOff()*(1-lightsOnOff)+
					aiType->GetSpotableNightLightsOn()*lightsOnOff
				);
				lightCoef=nightSpotable*night+(1-night);
			}

			float visibleFire=visibility*ai->VisibleFire();
			float audibleFire=audibility*ai->AudibleFire()*0.3f;

			float spotOptics = floatMax(opticsZoom*0.5,1);
			spotCoef = movement*spotOptics*GetType()->_sensitivity;
			spotCoef*=lightCoef;

			actSpotabilityEye *= spotCoef;
			actSpotabilityEar *= aiAudible*sensitivityEar;

			// select if we better hear or see
			float actSpotability = floatMax(actSpotabilityEye,actSpotabilityEar);
			float sensorFire = floatMax(visibleFire,audibleFire);

			/*#if _ENABLE_CHEATS
				static StatEventRatio ratio("Vis test reduced");
				ratio.Count(false);
			#endif*/
			if (actSpotability<spotTreshold && sensorFire<0.8f)
			{
				// this target can never be seen
				// no need to test it
				/*#if _ENABLE_CHEATS
					ratio.Count(false);
				#endif*/
				continue;
			}
			/*#if _ENABLE_CHEATS
				ratio.Count(true);
			#endif*/

		}
		#endif

		float audibility = 0;
		float visibility = 0;
		if (!ai->GetType()->GetLaserTarget() || GetType()->GetLaserScanner())
		{
			visibility = CalcVisibility(ai,dist2,&audibility);
		}

		/*
		LogF
		(
			"Track: %s from %s - %g, night %g",
			(const char *)ai->GetDebugName(),
			(const char *)GetDebugName(),
			visibility,night
		);
		*/

		// small object are less visible
		// if sensor is moving fast, visibility is decreased
		if (!radarContact && !laserContact)
		{
			float mySpeed=_speed.Size();
			float speedCoef=mySpeed*(1.0f/200);
			saturateMin(speedCoef,0.8f);
			visibility -= speedCoef;
			audibility -= speedCoef;
		}

		float actSpotabilityEye=visibility;
		float actSpotabilityEar=audibility;

		//LogF("  actSpotability %g",actSpotability);

		if (!radarContact && !laserContact)
		{
			saturate(visibility,0,10);
		}
		saturate(audibility,0,120);

		// update visible movement/fire
		// spot
		float visibleAccuracy=visibility*3;
		saturateMin(visibleAccuracy,30);

		float audibleAccuracy=audibility*3;

		Vector3Val eDir=GetEyeDirection();
		float cosFiEye=eDir.CosAngle(ai->Position()-Position());
		float cosFiWep=cosFiEye;

		if (_currentWeapon>=0)
		{
			cosFiWep = wepDir.CosAngle(ai->Position()-Position());
		}		

		//LogF("  pre visibleAccuracy %g",visibleAccuracy);
		// FIX: target recognition accuracy in night
		float cosEyeAdjusted = cosEye;
		if (ai->GetType()->GetLaserTarget()) cosEyeAdjusted = -1;

		float nightAccCoef = 1-night;
		if (radarContact || laserContact) visibleAccuracy*=GetType()->_sensitivity;
		else if (cosFiWep>cosOpt) visibleAccuracy*=GetType()->_sensitivity*opticsZoom*nightAccCoef; // optics
		else if (cosFiEye>cosEyeFocus) visibleAccuracy*=GetType()->_sensitivity*nightAccCoef;
		else if (cosFiEye>cosEyeAdjusted) visibleAccuracy*=GetType()->_sensitivity*0.5f*nightAccCoef;
		else visibleAccuracy = 0;

		audibleAccuracy *= sensitivityEar*aiAudible*0.25;
		//LogF("  post visibleAccuracy %g",visibleAccuracy);

		// we can never recognize "real" side only by hearing
		// real side is recognized when accuracy is 1.5 or higher
		float audibleSideAccuracy = floatMin(audibleAccuracy*0.5f,1.4f);
		float visibleSideAccuracy = floatMin(visibleAccuracy*0.15f,4);

		saturateMin(visibleAccuracy,4);
		saturateMin(audibleAccuracy,4);

		float fadingAccuracy=target->FadingAccuracy();
		float fadingSideAccuracy=target->FadingSideAccuracy();
		float sensorAccuracy = floatMax(audibleAccuracy,visibleAccuracy);
		if (initialize)
		{
			// during init assume all friendly units within range are fully known
			if (center->IsFriendly(ai->GetTargetSide()))
			{
				sensorAccuracy = 4;
				visibleAccuracy = 4;
				audibleAccuracy = 4;
				if (ai->CommanderUnit())
				{
					saturateMax(visibleSideAccuracy,1.6);
					saturateMax(audibleSideAccuracy,1.6);
				}
			}
		}
		//LogF("  accuracy: aud %.2f, vis %.2f",audibleAccuracy,visibleAccuracy);
		if (sensorAccuracy>fadingAccuracy)
		{
			target->accuracy = sensorAccuracy;
			target->accuracyTime = Glob.time;
			fadingAccuracy = sensorAccuracy;
		}

		if (target->destroyed)
		{
			// special handling of destroyed targets
			// dead body or vehicle is always civilian - and not checked, as it is not "real" side
			// when unit is respawn, it may become enemy again
			target->side = TCivilian;
			target->sideChecked = false;
		}
		else
		{
			float sensorSideAccuracy = floatMax(audibleSideAccuracy,visibleSideAccuracy);
			if (sensorSideAccuracy>=fadingSideAccuracy)
			{
				TargetSide tSide = ai->GetTargetSide(sensorSideAccuracy);
				// special check for "stolen" vehicle
				bool checked = sensorSideAccuracy>=1.5f;
				if (center->IsFriendly(tSide) || tSide==TCivilian)
				{
					if (sensorSideAccuracy>=1.35f && center->IsEnemy(ai->GetTargetSide()))
					{
						// unless we know for sure it is enemy, we consider stolen vehicles as unknown
						// as they are not supposed to be moving
						tSide = TSideUnknown;
						checked = true;
					}
				}
				target->side = tSide;
				target->sideChecked = checked;
				target->sideAccuracy=sensorSideAccuracy;
				target->sideAccuracyTime=Glob.time;
				fadingSideAccuracy=sensorSideAccuracy;
			}
		}
		// check if object is visible
		//float visibility=target->FadingVisibility();
		float spotability=target->FadingSpotability();			
		
		const VehicleType *aiType=ai->GetType();
		float spotCoef=1;
		float spotError=0;
		float lightCoef=1;

		if (!radarContact && !laserContact && night>0)
		{
			
			float lightsOnOff=ai->VisibleLights();
			float nightSpotable=
			(
				aiType->GetSpotableNightLightsOff()*(1-lightsOnOff)+
				aiType->GetSpotableNightLightsOn()*lightsOnOff
			);
			lightCoef=nightSpotable*night+(1-night);
		}
		float delayCoef = 1;
		float hidden = ai->GetHidden(); // actually oposite of hidden
		// pattern hides is lower on short distance
		const float patternFullDistance = 60;
		if (dist2<Square(patternFullDistance) && hidden<1)
		{
			float dist=dist2*InvSqrt(dist2); // sqrt(dist2)
			float patternFactor = dist/patternFullDistance;
			// if unit is not moving, this effect is not so strong
			float moveFactor = movement;
			saturate(moveFactor,0,1);
			// the bigger patternFactor, the better is unit concealed in patterns
			//LogF(" pattern %.3f, move %.3f, hidden %.3f",patternFactor,moveFactor,hidden);
			patternFactor = 1-(1-patternFactor)*moveFactor;
			hidden = 1 - (1-hidden)*patternFactor;
			//LogF("   patternFactor %.3f, result %.3f",patternFactor,hidden);
		}

		float visibleFire=visibility*ai->VisibleFire();
		float audibleFire=audibility*ai->AudibleFire()*0.3f;

		if (radarContact || laserContact)
		{
			spotCoef = GetType()->_sensitivity*8;
			delayCoef = 0.5f; // faster reaction
		}
		else if (cosFiWep>cosOpt)
		{
			float spotOptics = floatMax(opticsZoom*0.5,1);
			spotCoef = movement*spotOptics*GetType()->_sensitivity;
			spotCoef*=lightCoef;
			spotCoef*=hidden;
			delayCoef = 1.0f;
		}
		else if (cosFiEye>cosEyeAdjusted)
		{
			spotCoef = movement*GetType()->_sensitivity;
			spotCoef*=lightCoef;
			spotCoef*=hidden;
			delayCoef = 1.5f; // slightly slower reaction
		}
		else
		{
			spotCoef = 0;
			visibleFire = 0;
		}

		actSpotabilityEye *= spotCoef;
		actSpotabilityEar *= aiAudible*sensitivityEar;

		// select if we better hear or see
		float actSpotability = actSpotabilityEye;
		if (actSpotability<1 && actSpotability<actSpotabilityEar)
		{
			spotError = ai->Position().Distance(Position())*0.8;
			delayCoef = 2.5f; // slower reaction
			actSpotability =  actSpotabilityEar;
			hidden = 1;
#if 0 //_ENABLE_CHEATS
			if (ai->CommanderUnit())
			{
				LogF
				(
					"%s heard by %s, spot %.3f, spotEye %.3f",
					(const char *)ai->GetDebugName(),
					(const char *)GetDebugName(),
					actSpotabilityEar,actSpotabilityEye
				);
			}
#endif
		}

		//LogF("  spotCoef %g, actSpotability %g",spotCoef,actSpotability);
		saturateMin(actSpotability,4);
		float newSpotability=actSpotability;

		float newSideAccuracy=fadingSideAccuracy;

		// consider if unit is firing
		float sensorFire = floatMax(visibleFire,audibleFire);
		if( sensorFire>0.8f ) // we can see somebody firing
		{
			//LogF("Fire vis %.2f, aud %.2f",visibleFire,audibleFire);
			TargetType *fireTarget=ai->FiredAt();
			// check what we know about the target
			int index=res.Find(fireTarget);
			if( index>=0 )
			{
				Target *fireTVis=res[index];
				saturateMax(newSideAccuracy,fireTVis->FadingAccuracy());
				saturateMax(newSideAccuracy,fireTVis->FadingSideAccuracy());
			}
			// if we are not in combat - this means danger?
			if
			(
				unit->GetCombatMode() < CMCombat &&
				group != ai->GetGroup() && !group->IsAnyPlayerGroup()
			)
			{
				if (!unit->IsDanger())
				{
					unit->SetDanger();
				}
			}
		}

		if( newSideAccuracy>fadingSideAccuracy )
		{
			//LogF("  set sideAccuracy from newSideAccuracy %g",newSideAccuracy);
			target->side = ai->GetTargetSide(newSideAccuracy);
			target->sideChecked = true;
			target->sideAccuracy=newSideAccuracy;
			target->sideAccuracyTime=Glob.time;
			fadingSideAccuracy=newSideAccuracy;
		}


		//target->side=ai->GetTargetSide(fadingSideAccuracy);
		target->type=ai->GetType(fadingAccuracy);
		if (!target->sideChecked && !target->destroyed)
		{
			TargetSide side = target->type->_typicalSide;
			if (side!=TSideUnknown)
			{
				AIUnit *aiUnit = ai->CommanderUnit();
				if (aiUnit)
				{
					if (aiUnit->GetCaptive()) side = TCivilian;
					else
					{
						// check if it is some vehicle friendly units have stolen
						TargetSide realSide = ai->GetTargetSide(4);
						if (center->IsFriendly(realSide)) side = realSide;
					}
				}
				else
				{
					// no commander unit - it is probably empty
					if (!ai->EngineIsOn() && center->IsEnemy(side))
					{
						// if it is empty, recognize it magically
						side = TCivilian;
					}
				}
				target->side = side;
			}
		}
		if (!target->destroyed && ai->IsDammageDestroyed())
		{
			// not reported as destroyed yet
			// set target for reporting
			target->destroyed=true;
			target->timeReported = TIME_MIN;
			target->posReported = VZero;
			target->side = TCivilian;
			target->sideChecked = false;
		}

		if
		(
			center->IsEnemy(target->side) && target->isKnown &&
			target->delay<Glob.time &&
			!target->destroyed && !target->vanished
		)
		{
			if
			(
				Position().Distance2(target->position)<Square(20*VisibleSize()) &&
				// FIX guerantee we are dislosed only by enemy target
				ai->CommanderUnit() &&
				center->IsEnemy(ai->GetTargetSide())
			)
			{
				// enemy is very near - we are probably disclosed
				// open fire and change behaviour to danger
				if (unit->IsLocal() && !unit->IsPlayer())
				{
					/*
					LogF
					(
						"%s disclosed by %s",
						(const char *)GetDebugName(),(const char *)ai->GetDebugName()
					);
					*/
					unit->Disclose();
				}
			}
		}

		float myDelay = 1.0f*GetInvAbility()*delayCoef;

		if (radarContact || laserContact)
		{
			// radar equipped vehicles have much shorter reaction time
			if (dist2<Square(1000) )
			{
				float maxTime = dist2*0.000003f+0.6f*GetInvAbility();
				saturateMin(myDelay,maxTime);
			}			
		}
		else
		{
			if (dist2<Square(100) )
			{
				if ( spotError<1 )
				{
					// visible target is very close - improve reaction time
					float maxTime = dist2*0.0002f+0.02f*GetInvAbility();
					saturateMin(myDelay,maxTime);
				}
				else
				{
					// audible target very close - improve reaction time
					float maxTime = dist2*0.001f+0.1f*GetInvAbility();
					saturateMin(myDelay,maxTime);
				}
			}
		}

		if( target->isKnown )
		{
			if (initialize)
			{
				target->delay = Glob.time;
				target->delaySensor = Glob.time;
				myDelay = 0;
			}
			else if (newSpotability>target->FadingSpotability())
			{
				// unit may improve delay
				Target *assigned = unit->GetTargetAssigned();
				if (assigned && assigned->IsKnownBy(unit))
				{
					// we are already busy - longer reaction time
					myDelay *= 3;
				}
				if (notManual)
				{
					Time newDelay = Glob.time+myDelay;
					if (newDelay<target->delaySensor)
					{
						target->delaySensor = newDelay;
						target->idSensor = unit->GetPerson();
					}
				}
			}
		}
		else
		{
			// target not known yet
			// idSensor should be the unit that sees it best
			// we take unit with best spotability
			if( newSpotability>spotTreshold )
			{
				#if 0
				LogF
				(
					"Set init error %s to %s, %.1f",
					(const char *)GetDebugName(),(const char *)ai->GetDebugName(),
					spotError
				);
				#endif
				target->posError=Vector3
				(
					GRandGen.RandomValue()*spotError*2-spotError,
					GRandGen.RandomValue()*spotError*0.2f-spotError*0.1f,
					GRandGen.RandomValue()*spotError*2-spotError
				);

				target->isKnown=true;
				target->position=ai->AimingPosition();
				target->speed=ai->Speed();

				// reveal to me
				float groupDelay = 2.5f*myDelay+2.5f;
				if (assigned && assigned->IsKnownBy(unit))
				{
					// we are already busy - longer reaction time
					myDelay *= 3;
				}

				// max. 10 sec. radio delay
				saturateMin(groupDelay,myDelay+10.0f);
				// reveal to rest of the group
				target->delaySensor = Glob.time + myDelay;
				target->delay = Glob.time + groupDelay;
				target->idSensor = unit->GetPerson();
			}
		}

		if( actSpotability>0.05 )
		{
			target->LimitError(spotError);
		}

		if( target->isKnown && actSpotability>0.05 )
		{
			// track position and speed
			target->position=ai->AimingPosition();
			target->speed=ai->Speed();
			AIUnit *aiUnit = ai->CommanderUnit();
			if (notManual || !aiUnit || aiUnit->GetLifeState()!=AIUnit::LSDead)
			{
				// player should not report dead bodies as seen
				// AI should
				// because he's not able to report they are dead
				// it will almost certainly see it until next sample
				target->lastSeen=Glob.time+trackTargetsPeriod;
			}

			// check if it is my group dead unit
			if
			(
				target->destroyed ||
				aiUnit && aiUnit->GetLifeState()==AIUnit::LSDead &&
				(notManual || Glob.config.IsEnabled(DTAutoSpot))
			)
			{
				// TODO: report all units in it as dead
				if (aiUnit && aiUnit->GetGroup()==group)
				{
					// verify it is dead
					AIUnit::LifeState ls = aiUnit->GetLifeState();

					if (ls==AIUnit::LSDead || ls==AIUnit::LSDeadInRespawn)
					{
						// report that unit is dead
						// only local unit may report
						if (unit->IsLocal())
						{
							// only group leader may report remote units
							if (aiUnit->IsLocal() || unit->IsGroupLeader())
							{
								group->SendUnitDown(unit,aiUnit);
							}
						}
					}
				}
			}
		}
		if( newSpotability>spotability )
		{
			target->spotability = newSpotability;
			target->spotabilityTime = Glob.time;
		}
		float minSpotability = !notManual ? 0.01f : 0.06f;
		if( target->FadingSpotability()<minSpotability )
		{
			// target forgotten
			// forget targets only when you do not seen them for a long time
			if (target->lastSeen<Glob.time-120)
			{
				target->isKnown=false;
			}
		}

		if 
		(
			target->isKnown && target->delay<=Glob.time
			&& target->lastSeen<Glob.time-10 && target->lastSeen>TIME_MIN
		)
		{
			// if we have not seen target for some time
			// position error must be introduced again
			float sinceLastSeen = Glob.time-target->lastSeen;
			float minRadius = sinceLastSeen;
			if (target->posError.SquareSize()<Square(minRadius))
			{
				float radiusXZ = minRadius*2;
				float radiusY = minRadius*0.2;
				#if 0
				LogF
				(
					"Set forget error %s to %s, %.1f",
					(const char *)GetDebugName(),(const char *)ai->GetDebugName(),
					minRadius
				);
				#endif
				target->posError = Vector3
				(
					GRandGen.RandomValue()*radiusXZ-radiusXZ*0.5,
					GRandGen.RandomValue()*radiusY-radiusY*0.5,
					GRandGen.RandomValue()*radiusXZ-radiusXZ*0.5
				);
				if (target->posError.SquareSize()<Square(minRadius*1.5))
				{
					target->posError.Normalize();
					target->posError *= minRadius*1.5;
				}
			}
		}


		//Assert( target->FadingSideAccuracy()<3 || target->side!=TSideUnknown );

		// check nearest known enemy
		if
		(
			center->IsEnemy(target->side) && target->isKnown &&
			(ai->GetType()->GetIRTarget() || !ai->GetType()->GetLaserTarget())
		)
		{
			float dist2=target->position.Distance2(Position());
			saturateMin(nearestEnemy2,dist2);
		}

	} // for( all vehicles )


	// check AICenter for _nearestEnemy
	for( int i=0; i<center->NTargets(); i++ )
	{
		const AITargetInfo &target=center->GetTarget(i);
		if( center->IsEnemy(target._side) )
		{
			EntityAI *ai = target._idExact;
			if (ai && ( ai->GetType()->GetIRTarget() || !ai->GetType()->GetLaserTarget()) )
			{
				float dist2=target._realPos.Distance2Inline(Position());
				saturateMin(nearestEnemy2,dist2);
			}
		}

	}
	_nearestEnemy=sqrt(nearestEnemy2);

}

/*!
\param res target list to track
\param initialize set when initializing. When true, situation is considered steady
and all values should be set as it was the same for very long time.
\param trackTargetsPeriod how long it is expected
until next TrackTargets will execute
*/

void EntityAI::TrackTargets
(
	TargetList &res, bool initialize, float trackTargetsPeriod
)
{
	const VehicleType *type = GetType();
	AIUnit *unit=CommanderUnit();
	if (unit)
	{
		TrackTargets
		(
			res,unit,type->_commanderCanSee,initialize,1e10,trackTargetsPeriod
		);
	}

	// ignore fast vehicles - shots and smokes
	_trackTargetsTime=Glob.time;
}

/*!\note
When some enemy is near, it is called often,
in every AIGroup::Think (about 0.1 sec)
*/

void EntityAI::TrackNearTargets(TargetList &res)
{
	const VehicleType *type = GetType();
	AIUnit *unit=CommanderUnit();
	if (unit) TrackTargets(res,unit,type->_commanderCanSee,false,100,0);

	// ignore fast vehicles - shots and smokes
	_trackNearTargetsTime=Glob.time;
}

/*!
	Perform target tracking
	Update target list to maintain actual visibility and spotability
	information about all potentially visible targets.
	(uses EntityAI::AddNewTargets and EntityAI::TrackTargets)
*/

void EntityAI::WhatIsVisible( TargetList &res, bool initialize )
{
	if (initialize)
	{
		AddNewTargets(res,initialize);
		TrackTargets(res,initialize,0);
		return;
	}
	// note:
	// this function is called only from CreateTargetList
	// CreateTargetList itself is called 
	// once per 1 second in Combat mode

	AIUnit *unit = CommanderUnit();
	if (!unit) return;

	float newTargetsPeriod = 1.0;
	float trackTargetsPeriod = 1.0;
	if (GetType()->_irScanRangeMax<=0) newTargetsPeriod = 5.0;

	float trackRange = floatMax(GetType()->GetIRScanRange(),TACTICAL_VISIBILITY);
	// check if there is some enemy in tracked range
	float trackCoef = 1;
	if (unit->GetNearestEnemyDist2()>Square(trackRange))
	{
		// track 5x less when no enemy is near
		trackCoef = 5;
	}

	trackTargetsPeriod *= trackCoef;
	newTargetsPeriod *= trackCoef;

	if (Glob.time>_newTargetsTime+newTargetsPeriod)
	{
		AddNewTargets(res,initialize);
	}
	if (Glob.time>_trackTargetsTime+trackTargetsPeriod)
	{
		//LogF
		//(
		//	"%s: track targets (per %.2f)",(const char *)GetDebugName(),trackTargetsPeriod
		//);
		TrackTargets(res,initialize,trackTargetsPeriod);
	}

	// note: new targets are added once for all units the vehicle
}


VisibilityTracker::VisibilityTracker()
{
}

VisibilityTracker::VisibilityTracker( EntityAI *obj )
:_obj(obj),_lastTime(Glob.time-60)
{
}

VisibilityTracker::~VisibilityTracker()
{
}


/*!
\patch 1.31 Date 11/23/2001 by Ondra.
- Fixed: When firing, line of fire between weapon and target is checked.
Line between eye and target was checked before, which sometimes
could lead to friendly fire.
*/

float VisibilityTracker::Value
(
	const EntityAI *sensor, int weapon, float reserve, float maxDelay
)
{
	// sensor and weapon are assumed const (tracker hard linked to sensor)
	if (!_obj)
	{
		LogF("VisibilityTracker expired");
		return _lastValue;
	}
	if( Glob.time-_lastTime>maxDelay )
	{
		_lastTime=Glob.time;

		// check if it is within tactical range
		EntityAI *ai = _obj;
		float irRange = sensor->GetType()->GetIRScanRange();
		if (!ai->GetType()->GetIRTarget()) irRange = 0;
		else if (!sensor->GetType()->GetIRScanGround() && !ai->Airborne()) irRange = 0;
		float maxDistance2=Square(floatMax(irRange,TACTICAL_VISIBILITY));
		float visible=0;
		if( sensor->Position().Distance2(ai->Position())<maxDistance2 )
		{
			// first check visiblity matrix to see if gunner can see target
			Time lastVisTime = GWorld->VisibilityTime
			(
				sensor->CommanderUnit(),ai
			);
			if (lastVisTime>=Glob.time-30)
			{
				// it was seen in near past. Check if we can fire.
				Vector3 weaponPos;
				if (weapon<0)
				{
					weaponPos = sensor->CameraPosition();
				}
				else
				{
					weaponPos = sensor->PositionModelToWorld(sensor->GetWeaponPoint(weapon));
				}
				visible=GLandscape->Visible
				(
					weaponPos,sensor,ai,reserve,ObjIntersectFire
				);
			}
		}

		_lastValue=visible;
		/*
		Log
		(
			"tracker %s to %s updated (weapon %d), %.3f",
			(const char *)sensor->GetDebugName(),
			(const char *)ai->GetDebugName(),
			weapon,visible
		);
		*/
		return visible;
	}
	return _lastValue;
}

VisibilityTrackerCache::VisibilityTrackerCache()
{
}

void VisibilityTrackerCache::Clear()
{
	_trackers.Clear();
}

VisibilityTrackerCache::~VisibilityTrackerCache()
{
}

/*!
\patch 1.27 Date 10/12/2001 by Ondra.
- Fixed: bug in visibility management
  that could cause excensive memory allocation during mission.
*/

float VisibilityTrackerCache::KnownValue
(
	const EntityAI *sensor, int weapon, EntityAI *obj, float reserve, float maxDelay
)
{
	DoAssert( obj );
	//if( fabs(maxDelay-0.3)<1e-6 ) maxDelay=1.2;
	// it is safe to disard tracker that is older than this limit
	const float discardAfter=1.0;
	Assert( maxDelay<discardAfter );
	// check cached results
	VisibilityTracker *found = NULL;
	for( int i=0; i<_trackers.Size(); i++ )
	{
		VisibilityTracker &tracker=_trackers[i];
		if( tracker._obj==obj )
		{
			// check this one
			if( Glob.time-tracker._lastTime<=maxDelay ) return tracker._lastValue;
			found = &tracker;
		}
		else if( Glob.time-tracker._lastTime>discardAfter || !tracker._obj )
		{
			// if tracker is out of date, remove it
			_trackers.Delete(i),i--;
		}
	}
	if (found)
	{
		// we found it, but it was not recent enough
		return found->Value(sensor,weapon,reserve,maxDelay);
	}
	if (_trackers.Size()<=4)
	{
		// create a new tracker
		VisibilityTracker &tracker=_trackers[_trackers.Add(obj)];
		return tracker.Value(sensor,weapon,reserve,maxDelay);
	}
	return -1;
}

float VisibilityTrackerCache::Value
(
	const EntityAI *sensor, int weapon, EntityAI *obj, float reserve, float maxDelay
)
{
	DoAssert( obj );
	//if( fabs(maxDelay-0.3)<1e-6 ) maxDelay=1.2;
	// it is safe to disard tracker that is older than this limit
	const float discardAfter=1.0;
	Assert( maxDelay<discardAfter );
	// check cached results
	for( int i=0; i<_trackers.Size(); i++ )
	{
		VisibilityTracker &tracker=_trackers[i];
		if( tracker._obj==obj )
		{
			// check this one
			if( Glob.time-tracker._lastTime<=maxDelay ) return tracker._lastValue;
		}
		else if( Glob.time-tracker._lastTime>discardAfter || !tracker._obj )
		{
			// if tracker is out of date, remove it
			_trackers.Delete(i),i--;
		}
	}
	// create a new tracker
	VisibilityTracker &tracker=_trackers[_trackers.Add(obj)];
	return tracker.Value(sensor,weapon,reserve,maxDelay);
}


inline float MaxProbability( const AmmoType &aInfo )
{
	return floatMax
	(
		floatMax(aInfo.midRangeProbab,aInfo.minRangeProbab),
		aInfo.maxRangeProbab
	);
}

static float BestDistance( const AmmoType &aInfo )
{
	if( aInfo.midRangeProbab>aInfo.minRangeProbab )
	{
		if( aInfo.maxRangeProbab>aInfo.midRangeProbab )
		{
			return aInfo.maxRange;
		}
		return aInfo.midRange;
	}
	return aInfo.minRange;
}

static float BestDistance( const AmmoType &aInfo, float &propab )
{
	if( aInfo.midRangeProbab>aInfo.minRangeProbab )
	{
		if( aInfo.maxRangeProbab>aInfo.midRangeProbab )
		{
			propab=aInfo.maxRangeProbab;
			return aInfo.maxRange;
		}
		propab=aInfo.midRangeProbab;
		return aInfo.midRange;
	}
	propab=aInfo.minRangeProbab;
	return aInfo.minRange;
}

static float HitProbability( float dist, const AmmoType &aInfo )
{
	if( dist<aInfo.midRange )
	{
		if( dist<aInfo.minRange ) return 0;
		return
		(
			(dist-aInfo.minRange)*aInfo.invMidRangeMinusMinRange+aInfo.minRangeProbab
		);
	}
	else
	{
		if( dist>aInfo.maxRange ) return 0;
		return
		(
			(dist-aInfo.maxRange)*aInfo.invMidRangeMinusMaxRange+aInfo.maxRangeProbab
		);
	}
}

/*!
\patch 1.08 Date 7/21/2001 by Ondra.
- Fixed: tank commander weapon selection more stable.
Weapons were changed too often with no obvious reason to do so,
this happened especially when engaging cars.
- Fixed: when there were no dangerous enemies visible,
imporance of weak targets was overestimated.
As a result much stronger weapon was selected than necessary.
\patch 1.78 Date 7/19/2002 by Ondra
- Improved: AI: Smarter target selection, especially for planes.
*/

bool EntityAI::WhatShootResult
(
	FireResult &result, const Target &target, int weapon,
	float inRange, float timeToAim, float timeToLive,
	float visible, float dist, float timeToShoot,
	bool considerIndirect
) const
{
	if( !IsAbleToFire() ) return false;

	if (target.destroyed) return false;
	if (target.vanished) return false;

	EntityAI *tgt=target.idExact;
	if( !tgt ) return false;

	#if DIAG_SHOOT_RESULT
		LogF
		(
			"   WhatShootResult %s to %s (with %d)",
			(const char *)GetDebugName(),(const char *)target.type->GetName(),
			weapon
		);
	#endif

	const Magazine *magazine = GetMagazineSlot(weapon)._magazine;
	if (!magazine) return false;
	const MagazineType *ammoInfo = magazine->_type;
	#if DIAG_SHOOT_RESULT
	LogF("    weapon name %s",(const char *)ammoInfo->GetName());
	#endif
	Assert(ammoInfo);
	const WeaponModeType *mode = GetWeaponMode(weapon);
	Assert(mode);
	const AmmoType *aInfo = mode->_ammo;
//	const WeaponModeType &wInfo = *mode;
	if (!aInfo) return false;

	if (magazine->_ammo <= 0)
	{
		#if DIAG_SHOOT_RESULT
			LogF("    x: no ammo.");
		#endif
		return false;
	}

	if( dist>aInfo->maxRange || dist<aInfo->minRange )
	{
		#if DIAG_SHOOT_RESULT
			LogF("    x: distance (%.1f <> %.1f..%.1f)",dist,aInfo->minRange,aInfo->maxRange);
		#endif
		return false;
	}
	EntityAI *ai = tgt;

	if( !ai->LockPossible(aInfo) )
	{
		// check if lock is possible
		#if DIAG_SHOOT_RESULT
			LogF("    x: cannot lock.");
		#endif
		return false;
	}
	float myCost=GetType()->_cost;
	// estimate time to live

	float mySecondCost=myCost/timeToLive;
	result.cost=aInfo->cost;

	//saturateMin(timeToShoot,15*60); // max 15 minutes of shooting
	saturateMin(timeToShoot,2*60); // consider max 2 minutes of shooting

	result.loan= timeToAim*mySecondCost;
	//result.loan=(timeToAim-timeToShoot)*mySecondCost;
	float hitProbab=HitProbability(dist,*aInfo);
	// check if target vehicle is visible
	if( visible<MinVisibleFire )
	{
		#if DIAG_SHOOT_RESULT
			LogF("    x: visibility %.3f",visible);
		#endif
		return false;
	}
	else hitProbab*=Square(visible);
	if( hitProbab<0.05 )
	{
		#if DIAG_SHOOT_RESULT
			LogF("    x: hit probability %.3f",hitProbab);
		#endif
		return false; // do not waste ammo
	}

	hitProbab*=inRange;

	LODShape *tgtShape=target.type->GetShape();
	float tgtRadius=tgtShape ? tgtShape->GeometrySphere() : 3;

	// if target is moving fast it is difficult to lead it
	float ammoSpeed=floatMax(floatMax(ammoInfo->_initSpeed,aInfo->maxSpeed),0.5);
	if( Square(ammoInfo->_maxLeadSpeed)<target.speed.SquareSize() )
	{
		float tgtSpeed=target.speed.Size();
		float leadMiss=tgtSpeed-ammoInfo->_maxLeadSpeed;
		float leadError=(dist/ammoSpeed+0.3f)*leadMiss; // time to fly * speed error
		float maxError=(aInfo->indirectHitRange+tgtRadius)*0.3f; // max distance to ignore lead error
		if( leadError>maxError )
		{
			float considerLead=maxError/leadError; // 0 <= considerLead <= 1
			#if DIAG_SHOOT_RESULT
			LogF
			(
				"    lead %.1f, spd %.1f, error %.1f, max %.1f, factor %.4f",
				ammoInfo->_maxLeadSpeed,tgtSpeed,
				leadError,maxError,considerLead
			);
			#endif
			if( considerLead<0.2 )
			{
				#if DIAG_SHOOT_RESULT
					LogF("    x: lead %.3f",considerLead);
				#endif
				return false;
			}
			hitProbab*=considerLead;
		}
	}
	// it is more difficult to hit fast moving vehicles
	// it is easier to hit with faster ammunition
	if (target.speed.SquareSize()>Square(0.5))
	{
		saturateMax(ammoSpeed,2); // assume at least speed of crawling man
		float considerSpeed = ammoSpeed*target.speed.InvSize()*0.03f;
		saturate(considerSpeed,0,1);
		#if DIAG_SHOOT_RESULT
		LogF
		(
			"    speed %.1f, ammo %.1f, factor %.4f",
			target.speed.Size(),ammoSpeed,
			considerSpeed
		);
		#endif
		hitProbab*=considerSpeed;
	}
	float danger=0; // how much armor is the enemy able to destroy on us
	// check how much dangerous is the target
	// use visible and dist for
	// check all enemy weapons
	if( tgt->GetTotalDammage()<MaxDammageWorking )
	{
		// predict dangerousness with regard to our movement
		// assume enemy will become dangerous if we will move closer to it
		const float predictTime = 2.0f;
		float distPredict = dist - GetType()->GetTypSpeedMs()*predictTime;
		float dist2 = Square(floatMax(distPredict,0));
		Threat threat=target.type->GetDammagePerMinute(dist2,visible);
		VehicleKind myKind=GetType()->GetKind();
		float dammagePerMinute=threat[myKind];
		if( dammagePerMinute>0 )
		{
			// my expected time to live when engaged by this target
			float lTimeToLive=GetArmor()*60/dammagePerMinute;
			// FIX
			// if he will not get us even after two hours fight,
			// consider his dangerousness marginal
			if (lTimeToLive<2*3600)
			{
			// FIX END


				danger=timeToLive/lTimeToLive;
				// note: lTimeToLive may be higher that timeToLive if tgt is relatively new
				#if DIAG_SHOOT_RESULT
					LogF
					(
						"    TTL %.2f, L TTL %.2f, raw danger %.2f, dpm %.2f",
						timeToLive,lTimeToLive,danger,dammagePerMinute
					);
				#endif
				saturateMin(danger,10);
				danger*=myCost;
			}
		}
	}
	
	float targetInvArmor=target.type->GetInvArmor();
	// larger targets are less hit
	// check ammo strenght (single point)
	float relStrength = aInfo->hit*targetInvArmor;
	if (relStrength<0.1)
	{
		#if DIAG_SHOOT_RESULT
			LogF("    x: armor strength %.4f",relStrength);
		#endif
		return false;
	}
	float changeDammage=Square(aInfo->hit)*targetInvArmor*Square(0.27f/tgtRadius);
	
	// very low dammage is not cummulated
	if( changeDammage<0.004 )
	{
		#if DIAG_SHOOT_RESULT
			LogF("    x: dammage change %.4f",changeDammage);
		#endif
		return false;
	}
	saturateMin(changeDammage,4);
	changeDammage*=hitProbab;

	float targetCost=target.type->GetCost()+danger;
	//result.dammage=target.dammage+changeDammage;
	timeToShoot-=timeToAim;
	// check if we will have time for another shot
	float cumChangeDammage=changeDammage;
	if( result.dammage+cumChangeDammage<1 && timeToShoot>=mode->_reloadTime )
	{
		int maxShots=mode->_reloadTime>0 ? toIntFloor(timeToShoot/mode->_reloadTime) : 10000;
		saturateMin(maxShots,magazine->_ammo-1);
		float fNeedShots=(1-result.dammage)/changeDammage-1;
		saturateMin(fNeedShots,maxShots);
		int needShots=toIntCeil(fNeedShots);
		cumChangeDammage+=changeDammage*needShots;
		result.cost+=aInfo->cost*needShots;
		result.loan+=mode->_reloadTime*mySecondCost*needShots;
		#if DIAG_SHOOT_RESULT
			LogF
			(
				"    WhatShootResult: needShots %d, cumChangeDammage %.2f",
				needShots,cumChangeDammage
			);
		#endif
	}
	saturateMin(cumChangeDammage,1.01f-result.dammage);
	// FIX: improved dammage clipping
	float oldDammage = result.dammage; 
	result.dammage += cumChangeDammage;
	if (result.dammage>=1)
	{
		result.gain += targetCost*0.5f;
		result.dammage = 1;
	}
	result.gain=(result.dammage-oldDammage)*targetCost*0.5f;
	// FIX END 
	result.weapon=weapon;
	#if DIAG_SHOOT_RESULT
		LogF
		(
			"    probab %.4f, inRange %.2f, visible %.3f, danger %.2f, time %.2f",
			hitProbab,inRange,visible,danger,timeToShoot
		);
		LogF
		(
			"    %c: dammage %.4f, gain %.0f, cost %.0f, loan %.0f, weapon %d",
			result.gain>0 ? '#' : 'x',
			result.dammage,result.gain,result.cost,result.loan,result.weapon
		);
	#endif
	if (considerIndirect && result.gain>0)
	{
		// some dammage will be done
		// consider also secondary (indirect) dammage on other targets
		// some range estimation
		float ihRange = aInfo->indirectHitRange;
		float secRange =  ihRange * aInfo->indirectHit*(1.0/20);
		if (secRange>=4)
		{
			#define DIAG_SEC 0
			#if DIAG_SEC
			LogF
			(
				"%s: Analyze indirect dammage on %s - range %.1f",
				(const char *)GetDebugName(),
				(const char *)target.idExact->GetDebugName(),
				secRange
			);
			#endif
			// we have some significant indirect hit
			// check group target list for other near targets
			AIUnit *unit = CommanderUnit();
			if (!unit) return false;
			AIGroup *group = GetGroup();
			AICenter *center = group->GetCenter();
			Vector3 secCenter = target.AimingPosition();
			
			const TargetList &tgt = group->GetTargetList();
			for (int i=0; i<tgt.Size(); i++)
			{
				const Target *tgtI = tgt[i];
				if (tgtI->vanished) continue;
				if (tgtI->destroyed) continue;
				if (tgtI->idExact==target.idExact) continue;
				// check if unit is in range
				float dist2 = tgtI->AimingPosition().Distance2(secCenter);
				if (dist2>=Square(secRange)) continue;
				if (!tgtI->IsKnownBy(unit)) continue;
				#if DIAG_SEC
					LogF
					(
						"  %s in range",
						(const char *)tgtI->idExact->GetDebugName()
					);
				#endif
				TargetSide side = tgtI->side;
				// calculate gain coeficient by side
				float coef = 1;
				if (center->IsEnemy(side)) coef = 1;
				else if (center->IsFriendly(side)) coef = -6;
				else if (side==TCivilian) coef = -6;
				else if (side==TSideUnknown) coef = -0.5;
				// rough hit estimation
				const VehicleType *typeI = tgtI->type;
				float rangeCoef = dist2<Square(ihRange) ? 1 : Square(ihRange)/dist2;
				float dammage = aInfo->indirectHit*typeI->GetInvArmor()*rangeCoef;
				saturate(dammage,0,1);
				if (coef>0)
				{
					result.gain += dammage*coef*typeI->GetCost();
				}
				else
				{
					result.cost += dammage*-coef*typeI->GetCost();
				}
				#if DIAG_SEC
				LogF
				(
					"  dammage - %.1f, gain %.1f",
					dammage,dammage*coef*typeI->GetCost()
				);
				#endif

			}
		}

	}
	return result.gain>0;
}

inline void UseMax( int &val, int v ) {if( val<v ) val=v;}

const float TimeToAttack=5; // attack aim time
const float MinTimeToLive=30;

/*!
\patch_internal 1.22 Date 9/10/2001 by Ondra.
- Fixed: GetIRScanGround returned float instead of bool.
	This caused unnecessary conversions.
\patch 1.36 Date 12/13/2001 by Ondra
- Fixed: Random crash when AI was engaging target and target was deleted.
*/

int EntityAI::EstimateAttack
(
	const Vector3 &hPos, float height, const EntityAI *who
) const
{
	//ADD_COUNTER(attck,1);

	#if DIAG_ATTACK
	LogF("Est attack at %.1f,%.1f - %.1f",hPos.X(),hPos.Z(),hPos.Y());
	LogF("  econom %.0f",_attackEconomicalResult.Surplus());
	LogF("  aggres %.0f",_attackAggresiveResult.CleanSurplus());
	#endif
	Matrix4 transform; // predict what position will the vehicle have

	Vector3 hePos = hPos;
	AIUnit *unit = CommanderUnit();
	if( unit ) unit->FindNearestEmpty(hePos);

	float dx,dz;
	float posY=GLandscape->RoadSurfaceYAboveWater(hePos.X(),hePos.Z(),&dx,&dz);
	Vector3 normal(-dx,1,-dz);
	if( who->GetType()->GetKind()==VAir ) normal=VUp;
	transform.SetPosition(Vector3(hePos.X(),posY+height,hePos.Z()));
	Vector3Val pos=transform.Position();
	Vector3Val tgtPos=_attackTarget->LandAimingPosition();

	Vector3Val tgtRel=tgtPos-pos;
	transform.SetUpAndDirection(normal,tgtRel);
	Matrix4 invTransform(MInverseRotation,transform);

	// check is pos is not unreachable
	int ix=toIntFloor(pos.X()*InvLandGrid),iz=toIntFloor(pos.Z()*InvLandGrid);
	GeographyInfo info=GLOB_LAND->GetGeography(ix,iz);
	float typicalCost=who->GetCost(info);
	if( typicalCost>1e10 )
	{
		#if DIAG_ATTACK
		LogF("Field unreachable.");
		#endif
		return false;
	}

	int improved=0;
	float bestDist,minRange,maxRange;
	FireResult bestFire;
	who->BestFireResult(bestFire,*_attackTarget,bestDist,minRange,maxRange,30,true);
	// estimate attack at 
	// check if xPos,zPos is in circle 
	float dist2=tgtRel.SquareSize();
	if( dist2>Square(minRange) && dist2<Square(maxRange) )
	{
		// estimate surplus	
		// check if it is within tactical range
		EntityAI *ai = _attackTarget->idExact;
		float irRange = GetType()->GetIRScanRange();
		if (!ai) return 0;
		if (!ai->GetType()->GetIRTarget()) irRange = 0;
		else if (!GetType()->GetIRScanGround() && !ai->Airborne()) irRange = 0;

		float maxDistance2=Square(floatMax(irRange,TACTICAL_VISIBILITY));
		if( dist2>maxDistance2 ) return false; // too far to see it - cannot fire 

		float inRange=who->FireAngleInRange(0,invTransform*tgtPos);
		if( inRange<0.5 ) goto Done;
		
		Vector3Val weaponPos=transform*GetWeaponCenter(0);
		float visible=GLOB_LAND->Visible(weaponPos,who,ai,1.5);

		improved|=EstVisibility;

		// TODO: not working on fences?
		#if DIAG_ATTACK
		LogF("  est attack vis %.2f",visible);
		#endif
		if( visible<MinVisibleFire )
		{
			#if DIAG_ATTACK
			LogF("Est attack failed");
			#endif
			return improved; // not suitable for firing
		}
		// estimate time to live at given position
		//int x = toIntFloor(Position().X() * InvLandGrid);
		//int z = toIntFloor(Position().Z() * InvLandGrid);
		//float timeToLive=CommanderUnit()->GetTimeToLive();

		AIGroup *grp = unit->GetGroup();
		if (!grp)
		{
			#if DIAG_ATTACK
			LogF("Est attack failed - no group");
			#endif
			return improved; // not suitable for firing
		}
		AICenter *center = grp->GetCenter();
		float distance = pos.Distance(Position());
		float exposure = center->GetExposureOptimistic(pos);
		float timeToLive = 120;
		if (exposure>0)
		{
			timeToLive = 120*who->GetType()->GetArmor()/exposure;
			saturate(timeToLive,5,120);
		}

		float timeToAim=TimeToAttack+distance*typicalCost;
		#if DIAG_ATTACK
		LogF("  exposure %.1f",exposure);
		LogF("  timeToLive %.1f",timeToLive);
		LogF("  distance %.1f",distance);
		LogF("  timeToAim %.1f",timeToAim);
		#endif
		// check if vehicle is not too low/high
		// assume all weapons are usable in the same range
		// align vehicle with surface at given point
		// check for all weapons
		float dist=dist2*InvSqrt(dist2);
		for( int w=0; w<who->NMagazineSlots(); w++ )
		{
			FireResult result;
			#if DIAG_ATTACK
			LogF("  weapon %d WhatShootResult",w);
			#endif
			if
			(
				who->WhatShootResult
				(
					result,*_attackTarget,w,inRange,
					timeToAim,timeToLive,
					visible,dist,30+timeToAim,false // TODO: time estimation
				)
			)
			{
				#if DIAG_ATTACK
				LogF("  est attack shoot %d",result.weapon);
				LogF("  sursplus %.1f",result.Surplus());
				LogF("  clean sursplus %.1f",result.CleanSurplus());
				LogF("  gain %.1f, cost %.1f, loan %.1f",result.gain,result.cost,result.loan);
				#endif
				if( this!=who )
				{
					// cargo attack planned
					result.weapon=-1;
				}

				// select most economical possibility
				const float conservative=0.7f;
				if( _attackEconomicalResult.Surplus()<result.Surplus()*conservative )
				{
					_attackEconomicalResult=result;
					_attackEconomicalPos=transform.Position();
					improved|=EstImproved;
					#if DIAG_ATTACK
					LogF
					(
						"  est attack econom improved %.0f, gain %.0f, cost %.0f, loan %.0f",
						result.Surplus(),result.gain,result.cost,result.loan
					);
					#endif
				}
				if( _attackAggresiveResult.CleanSurplus()<result.CleanSurplus()*conservative )
				{
					_attackAggresiveResult=result;
					_attackAggresivePos=transform.Position();
					improved|=EstImproved;
					#if DIAG_ATTACK
					LogF
					(
						"  est attack aggres improved %.0f, gain %.0f, cost %.0f, loan %.0f",
						result.CleanSurplus(),result.gain,result.cost,result.loan
					);
					#endif
				}
			}
			else
			{
				#if DIAG_ATTACK
				LogF("  impossible");
				#endif
			}
		}
	}
	else
	{
		#if DIAG_ATTACK
		LogF("  out of range");
		#endif
	}
	Done:
	#if 1
	// plan attack using cargo soldiers
	if( !(improved&EstImproved) && who->GetType()->HasCargo() )
	{
		Assert( dyn_cast<const Transport>(who) );
		const Transport *transport=static_cast<const Transport *>(who);
		// consider also cargo soldier weapons (if any)
		// select 'best' soldier in cargo
		// if there is any LAW soldier, use him
		const ManCargo &cargo=transport->GetManCargo();
		//Person *bestSoldier=NULL;
		for( int c=0; c<cargo.Size(); c++ )
		{
			Person *soldier=cargo[c];
			if( !soldier ) continue;
			if( soldier->GetGroup()!=GetGroup() ) continue;
			improved|=EstimateAttack(pos,1.1f,soldier);
			break;
		}
	}
	#endif
	#if DIAG_ATTACK
	LogF("Est attack done");
	#endif
	return improved;
}

int EntityAI::EstimateAttack( const Vector3 &hPos, float height ) const
{
	return EstimateAttack(hPos,height,this);
}

void EntityAI::BegAttack( Target *target )
{
	Assert( target );
	if( !_attackTarget || _attackTarget->idExact!=target->idExact )
	{
		_attackTarget=target;
		#if DIAG_ATTACK	
		LogF("Engage target.");
		#endif
		// target changed - reset information
		FireResult bestFire;
		float bestDist,minDist,maxDist;
		BestFireResult(bestFire,*target,bestDist,minDist,maxDist,30,true);
		//Vector3Val tgtPos=target->AimingPosition();
		Vector3Val tgtPos=_attackTarget->LandAimingPosition();
		Vector3Val relTgt=tgtPos-Position();
		Vector3Val dir=relTgt.Normalized(); // direction from this to target
		float dist=dir*relTgt; // distance to target
		if( minDist>maxDist || dist<maxDist )
		{ // near attack - start some reasonable estimations
			_attackAggresivePos=(Position()+tgtPos)*0.5;
			_attackEconomicalPos=Position();
		}
		else
		{
			// far attack - start near target
			_attackEconomicalPos=tgtPos-dir*maxDist;
			_attackAggresivePos=tgtPos-dir*(maxDist+minDist)*0.5;
		}
		_attackEngageTime=Glob.time;
		_attackRefreshTime=Glob.time-60;
		_attackEconomicalResult.Reset();
		_attackAggresiveResult.Reset();
		EstimateAttack(Position(),GetCombatHeight());
	}
	_attackTarget=target; // update pos. info
}

void EntityAI::EndAttack()
{
	_attackTarget=NULL;
	_attackEconomicalResult.Reset();
	_attackAggresiveResult.Reset();
	_attackAggresivePos=VZero;
	_attackEconomicalPos=VZero;
}

bool EntityAI::AttackThink( FireResult &result, Vector3 &pos )
{ // true when attack is planned
	PROFILE_SCOPE(thAtk);
	if( !_attackTarget ) return false;
	Object *tgt=_attackTarget->idExact;
	if( !tgt ) return false;
	if (tgt->IsDammageDestroyed())
	{
		EndAttack();
		return false;
	}
	AIUnit *brain=CommanderUnit();
	if( !brain ) return false;
	Assert( brain->GetSubgroup() );
	Assert( brain->GetSubgroup()->GetGroup() );
	if ( _attackTarget->IsKnown() && !_attackTarget->IsKnownBy(brain) )
	{
		// is known - but not yet by me
		#if DIAG_ATTACK
		LogF("Target not known by unit");
		#endif
		pos=Position();
		return true;
	}
	if( !_attackTarget->IsKnown() || _attackTarget->lastSeen<Glob.time-20 )
	{
		// we have not seen it for quite a long time
		pos=_attackTarget->LandAimingPosition();
		return true;
	}
	// update target information
	// check best fire possible
	float bestDist,minDist,maxDist;
	FireResult bestFire;
	BestFireResult(bestFire,*_attackTarget,bestDist,minDist,maxDist,30,true);

	#if 1
	// plan attack using cargo soldiers
	if( minDist>maxDist && GetType()->HasCargo() )
	{
		Assert( dyn_cast<const Transport>(this) );
		const Transport *transport=static_cast<const Transport *>(this);
		// consider also cargo soldier weapons (if any)
		// select 'best' soldier in cargo
		// if there is any LAW soldier, use him
		const ManCargo &cargo=transport->GetManCargo();
		//Person *bestSoldier=NULL;
		for( int c=0; c<cargo.Size(); c++ )
		{
			Person *soldier=cargo[c];
			if( !soldier ) continue;
			if( soldier->GetGroup()!=GetGroup() ) continue;
			soldier->BestFireResult(bestFire,*_attackTarget,bestDist,minDist,maxDist,30,true);
			// cargo attack - no weapon
			bestFire.weapon = -1;
			break;
		}
	}
	#endif

	#if DIAG_ATTACK
	LogF("EngageThink best gain %.1f, cost %.1f",bestFire.gain,bestFire.cost);
	#endif
	if( minDist>maxDist ) return false; // no weapons
	if( bestFire.gain<=0 ) return false; // no gain possible
	const float goodEnough=0.9f;
	const float notBad=0.4f;

	if (bestFire.weapon>=0)
	{
		const WeaponModeType *info = GetWeaponMode(bestFire.weapon);
		if (info && info->_ammo )
		{
			switch (info->_ammo->_simulation)
			{
				case AmmoShotPipeBomb:
				case AmmoShotTimeBomb:
				{
					// place bomb as near as possible
					pos=_attackTarget->LandAimingPosition();
					// select position opposite to vehicle weapon
					// assume we always know vehicle weapon direction
					// we place bombs very near
					if (_attackTarget->idExact)
					{
						// note: eye direction is much cheaper for soldiers
						// it is also more appropriate (we want to avoid being seen)
						Vector3 weaponDir = _attackTarget->idExact->GetEyeDirection();
						pos -= weaponDir*3;
					}
					bool found = brain->FindNearestEmpty(pos);
					pos[1]=GLandscape->RoadSurfaceYAboveWater(pos.X(),pos.Z());
					pos[1]+=GetCombatHeight();

					#if DIAG_ATTACK
					LogF("Bomb result %.1f,%.1f",pos.X(),pos.Z());
					#endif

					result = bestFire;

					return found;
				}	
			}
		}
	}
	// init estimations
	Vector3Val tgtPos=_attackTarget->LandAimingPosition()+_attackTarget->speed*5;
	if( (_attackEconomicalPos-tgtPos).SquareSizeXZ()>Square(maxDist) )
	{
		// move towards the target
		Vector3 norm = (_attackEconomicalPos-tgtPos);
		Vector3Val off=norm.Normalized()*maxDist*0.7f;
		_attackEconomicalPos=tgtPos+off;
		_attackEconomicalResult.Reset();
	}
	if( (_attackAggresivePos-tgtPos).SquareSizeXZ()>Square(maxDist) )
	{
		Vector3 norm = (_attackAggresivePos-tgtPos);
		Vector3Val off=norm.Normalized()*maxDist*0.5;
		_attackAggresivePos=tgtPos+off;
		_attackAggresiveResult.Reset();
	}
	
	// always check: current position, economical and attack position
	const float myHeight=GetCombatHeight();
	const float maxHeight=GetMaxCombatHeight();
	const float minHeight=GetMinCombatHeight();

	const float refreshDelay=6; // how often we refresh results we have
	int visTests=0;
	if( Glob.time-_attackRefreshTime>refreshDelay )
	{
		_attackEconomicalResult.Reset();
		_attackAggresiveResult.Reset();
		if( EstVisibility&EstimateAttack(_attackAggresivePos,GetCombatHeight()) ) visTests++;
		if( EstVisibility&EstimateAttack(_attackEconomicalPos,GetCombatHeight()) ) visTests++;
		// check position between us and target
		//Vector3Val pos=Position()+(tgtPos-Position())*0.3;
		// check position at optimal distance from target
		Vector3 norm = (tgtPos-Position());
		Vector3Val pos=Position()+norm.Normalized()*bestDist;
		if( EstVisibility&EstimateAttack(pos,GetCombatHeight()) ) visTests++;
		// check current position
		if( EstVisibility&EstimateAttack(Position(),GetCombatHeight()) ) visTests++;
		_attackRefreshTime=Glob.time;
	}
	
	// perform some attack iterations
	// random spread around agresive and economical points
	// if we have some good enough econom. position, do not try to improve it
	if( _attackEconomicalResult.Surplus()>=goodEnough*bestFire.Surplus() )
	{
		pos=_attackEconomicalPos;
		result=_attackEconomicalResult;
		#if DIAG_ATTACK
		LogF("Good EconomicalResult %.1f,%.1f",pos.X(),pos.Z());
		#endif
		return true;
	}

	bool noSolution=true;
	bool noEconomSolution=true;
	if( _attackEconomicalResult.Surplus()>=notBad*bestFire.Surplus() )
	{
		noSolution=false;
		noEconomSolution=false;
	}
	else if( _attackAggresiveResult.CleanSurplus()>=notBad*bestFire.CleanSurplus() )
	{
		noSolution=false;
	}

	// random height for helicopters/plane
	//const float spread=noSolution ? 100 : 50;
	const float spread=noSolution ? 200 : 100;
	int maxTests=noSolution ? 8 : 2;
	int maxN=noSolution ? 8 : 2;
	for( int i=maxN; --i>=0 && visTests<maxTests; )
	{
		// try to improve result
		{
			float xRand=GRandGen.RandomValue()*spread*2-spread;
			float zRand=GRandGen.RandomValue()*spread*2-spread;
			float height=myHeight;
			if( maxHeight>minHeight )
			{
				float yRand=GRandGen.RandomValue()+GRandGen.RandomValue();
				if( yRand>1 ) height=myHeight+(maxHeight-myHeight)*(yRand-1);
				else height=minHeight+(myHeight-minHeight)*yRand;
			}
			Vector3Val pos=_attackEconomicalPos+Vector3(xRand,0,zRand);
			if( EstVisibility&EstimateAttack(pos,height) ) visTests++;
		}
		// if we have only aggresive result, update it
		if( noEconomSolution && !noSolution )
		{
			float xRand=GRandGen.RandomValue()*spread*2-spread;
			float zRand=GRandGen.RandomValue()*spread*2-spread;
			float height=myHeight;
			if( maxHeight>minHeight )
			{
				float yRand=GRandGen.RandomValue()+GRandGen.RandomValue();
				if( yRand>1 ) height=myHeight+(maxHeight-myHeight)*(yRand-1);
				else height=minHeight+(myHeight-minHeight)*yRand;
			}
			Vector3Val pos=_attackAggresivePos+Vector3(xRand,0,zRand);
			if( EstVisibility&EstimateAttack(pos,height) ) visTests++;
		}
	}

	#if 0
		GLOB_ENGINE->ShowMessage
		(
			500,"Est attack ec %.3f ag %.3f",
			_attackEconomicalResult.Surplus()/bestFire.Surplus(),
			_attackAggresiveResult.CleanSurplus()/bestFire.CleanSurplus()
		);
		LogF
		(
			"Est attack ec %.3f ag %.3f",
			_attackEconomicalResult.Surplus()/bestFire.Surplus(),
			_attackAggresiveResult.CleanSurplus()/bestFire.CleanSurplus()
		);
	#endif

	/*
	if( _attackAggresiveResult.CleanSurplus()>=goodEnough*bestFire.CleanSurplus() )
	{
		pos=_attackAggresivePos;
		result=_attackAggresiveResult;
		#if DIAG_ATTACK
		LogF("Good AggresiveResult %.1f,%.1f",pos.X(),pos.Z());
		#endif
		return true;
	}
	*/

	// if not good enough, wait - maybe it will be better
	if( Glob.time-_attackEngageTime<5.0 )
	{
		#if DIAG_ATTACK
		LogF("  time %.1f",Glob.time-_attackEngageTime);
		#endif
		return false;
	}
	#if DIAG_ATTACK
		LogF("  econom best %.1f",bestFire.Surplus());
		LogF("         curr %.1f",_attackEconomicalResult.Surplus());
		LogF("  aggres best %.1f",bestFire.CleanSurplus());
		LogF("         curr %.1f",_attackAggresiveResult.CleanSurplus());
	#endif
	const float bad=0.1f;
	if( _attackEconomicalResult.Surplus()>=bad*bestFire.Surplus() )
	{
		pos=_attackEconomicalPos;
		result=_attackEconomicalResult;
		#if DIAG_ATTACK
		LogF("Bad EconomicalResult %.1f,%.1f",pos.X(),pos.Z());
		#endif
		return true;
	}
	else if( _attackAggresiveResult.CleanSurplus()>=bad*bestFire.CleanSurplus() )
	{
		pos=_attackAggresivePos;
		result=_attackAggresiveResult;
		#if DIAG_ATTACK
		LogF("Bad AggresiveResult %.1f,%.1f",pos.X(),pos.Z());
		#endif
		return true;
	}
	return false;
}

bool EntityAI::AttackReady()
{
	float bestDist,minDist,maxDist;
	FireResult bestFire;
	BestFireResult(bestFire,*_attackTarget,bestDist,minDist,maxDist,30,true);

	#if 1
	// plan attack using cargo soldiers
	if( minDist>maxDist && GetType()->HasCargo() )
	{
		Assert( dyn_cast<const Transport>(this) );
		const Transport *transport=static_cast<const Transport *>(this);
		// consider also cargo soldier weapons (if any)
		// select 'best' soldier in cargo
		// if there is any LAW soldier, use him
		const ManCargo &cargo=transport->GetManCargo();
		//Person *bestSoldier=NULL;
		for( int c=0; c<cargo.Size(); c++ )
		{
			Person *soldier=cargo[c];
			if( !soldier ) continue;
			if( soldier->GetGroup()!=GetGroup() ) continue;
			soldier->BestFireResult(bestFire,*_attackTarget,bestDist,minDist,maxDist,30,true);
			// cargo attack - no weapon
			bestFire.weapon = -1;
			break;
		}
	}
	#endif

	const float bad=0.1f;
	if( _attackEconomicalResult.Surplus()>=bad*bestFire.Surplus() )
	{
		return true;
	}
	else if( _attackAggresiveResult.CleanSurplus()>=bad*bestFire.CleanSurplus() )
	{
		return true;
	}
	return false;
}

#define HIDE_DIAGS 0

bool ObstacleFree( AIUnit *unit, Object *object )
{
	AIGroup *grp = unit->GetGroup();
	for( int u=1; u<MAX_UNITS_PER_GROUP; u++ )
	{
		AIUnit *un = grp->UnitWithID(u);
		if( !un || un==unit || !unit->IsUnit() ) continue;
		EntityAI *veh = un->GetVehicle();
		Vector3Val pos = veh->Position();
		Vector3 predict = pos+veh->Speed()*2;
		if
		(
			pos.DistanceXZ2(object->Position())<Square(5) ||
			predict.DistanceXZ2(object->Position())<Square(5)
		)
		{
			// unit is very close
			return false;
		}
		if ( un->GetVehicle()->GetHideBehind()==object )
		{
			// unit has obstacle allocated
			return false;
		}
	}
	return true;
}

Vector3 EntityAI::HideFrom() const
{
	// what position do we hide from
	// if there is explicit hide target, we hide from it
	if (_hideTarget)
	{
		return _hideTarget->AimingPosition();
	}
	// if not, we hide in formation direction
	AIUnit *unit = CommanderUnit();
	float radius=GetType()->GetMaxSpeedMs()*20;
	saturate(radius,20,500);
	AISubgroup *subgrp = unit->GetSubgroup();
	if (subgrp)
		return Position() + subgrp->GetFormationDirection() * radius;
	else
		return Position() + Direction() * radius;
}

void EntityAI::FindHideBehind( Vector3 pos, float maxDist )
{
	// select hideTarget
	AIUnit *unit = CommanderUnit();
	if( !unit ) return;

	AIGroup *grp = unit->GetGroup();
	AICenter *center = grp->GetCenter();

	// select nearest enemy target
	{
		Target *target = NULL;
		float minFunc = 1e10;
		const TargetList &targetList = grp->GetTargetList();

		for (int i=0; i<targetList.Size(); i++)
		{
			Target *tar = targetList[i];
			if (!tar->idExact) continue;
			if (tar->idExact->IsDammageDestroyed()) continue;
			if (!tar->IsKnownBy(unit) ) continue;
			// in stealth hide also from unknown targets
			if( unit->GetCombatMode()!=CMStealth )
			{
				if (!center->IsEnemy(tar->side)) break;
			}
			else
			{
				if (!center->IsEnemy(tar->side) && tar->side!=TSideUnknown ) break;
			}

			// TODO: beter hide target selection
			float func=tar->AimingPosition().Distance(unit->Position());
			if( func<minFunc )
			{
				minFunc = func;
				target  = tar;
			}
		}

		_hideTarget=target;

	}

	float radius=GetType()->GetMaxSpeedMs()*20;
	saturate(radius,20,500);
	Vector3 tgtPos = HideFrom();

	_hideRefreshTime = Glob.time;


	// TODO: some HideSize?
	float size=Object::CollisionSize();

	int xMin,xMax,zMin,zMax;

	ObjRadiusRectangle(xMin,xMax,zMin,zMax,pos,pos,maxDist);
	int x,z;
	float minFunc=1e10;
	Object *obstacle=NULL;

	float wantsAway=-0.5f+GRandGen.RandomValue()*2;
	float isLazy=1+GRandGen.RandomValue()*2;

	#if HIDE_DIAGS
	LogF
	(
		"%s: Finding cover",(const char *)GetDebugName()
	);
	LogF
	(
		"  isLazy %.1f, wantsAway %.1f",
		isLazy,wantsAway
	);
	#endif


	AIUnit *leader = grp->Leader();

	for( x=xMin; x<=xMax; x++ ) for( z=zMin; z<=zMax; z++ )
	{
		const ObjectList &list=GLandscape->GetObjects(z,x);
		int n=list.Size();
		for( int i=0; i<n; i++ )
		{
			Object *obj=list[i];
			if( obj==this ) continue;
			if( obj->GetType()==Network ) continue; // no collisions with roads
			if( obj->GetType()==Temporary ) continue; // no collisions with roads
			if( obj->GetType()==TypeTempVehicle ) continue; // no collisions with roads
			// TODO: hide behind empty vehicles
			if( !obj->Static() ) continue; // no collisions with roads
			if( obj->CollisionSize()<size ) continue;
			float distC2=obj->Position().Distance2(pos);
			if( distC2>Square(maxDist) ) continue;

			float dist2=obj->Position().Distance2(Position());

			float sizeBonus=(obj->CollisionSize()-size)*5;
			saturateMin(sizeBonus,50);
			float dist=dist2*InvSqrt(dist2);
			float distToEnemy=tgtPos.DistanceXZ(obj->Position());
			if( distToEnemy>radius*2.5f ) continue; // no need to hide
			if( distToEnemy<radius*0.3f ) continue;

			float distToLeader = 0;
			if (leader && !leader->IsPlayer())
			{
				distToLeader  = pos.DistanceXZ(obj->Position());
				dist += distToLeader*2;
			}

			float func = dist*isLazy-distToEnemy*wantsAway+sizeBonus;

			#if HIDE_DIAGS
				{
					float behind=obj->CollisionSize()+CollisionSize()+2;
					Vector3 tgtDir=(tgtPos-obj->Position()).Normalized();
					Vector3 wantedPos=obj->Position()-tgtDir*behind;
					if( unit->CheckEmpty(wantedPos) && ObstacleFree(unit,obj) )
					{
						LogF
						(
							"  candidate %s: func %.1f ",
							(const char *)obj->GetDebugName(),func
						);
						LogF
						(
							"  dist %.1f, distToEnemy %.1f, distToLeader %.1f, sizeBonus %.1f",
							dist,distToEnemy,distToLeader,sizeBonus,wantsAway
						);
					}
				}
			#endif

			if( minFunc>func )
			{
				// check if there is some free space behind the obstacle
				float behind=obj->CollisionSize()+CollisionSize()+2;
				Vector3 norm = (tgtPos-obj->Position());
				Vector3 tgtDir=norm.Normalized();
				Vector3 wantedPos=obj->Position()-tgtDir*behind;

				if( !unit->CheckEmpty(wantedPos) ) continue;
				if( !ObstacleFree(unit,obj) ) continue;

				// empty space - we should be able to hide there
				minFunc=func;
				obstacle=obj;
			}
		}
	} // (for all near objects)
	if( _hideBehind!=obstacle )
	{
		_inFormation=false;
	}
	_hideBehind=obstacle;

	#if HIDE_DIAGS
	if( !_hideBehind ) 
	{
		LogF("  Nowhere to hide");
	}
	else
	{
		LogF
		(
			"  Hide behind: %s",
			(const char *)_hideBehind->GetDebugName()
		);
	}
	#endif
}

void EntityAI::FindHideBehind()
{
	AIUnit *unit = CommanderUnit();
	if( !unit ) return;
	AIGroup *grp = unit->GetGroup();
	if (!grp) return;

	float radius=GetType()->GetMaxSpeedMs()*20;
	saturate(radius,20,500);

	float maxDist = radius;
	AIUnit *leader = grp->Leader();
	// two kinds of hiding
	// in player group hide on given position
	// in AI group hide near leader
	if (leader && !leader->IsAnyPlayer())
	{
		// consider where we should be in formation
		if (!leader->GetSubgroup())
		{
			Fail("Leader, but no subgroup");
			return;
		}
		FindHideBehind(unit->GetFormationAbsolute(leader),maxDist);
	}
	else
	{
		FindHideBehind(Position(),maxDist);
	}
}

void EntityAI::BegHide()
{
	FindHideBehind();
}

void EntityAI::EndHide()
{
	// same as EndAttack
	_hideTarget = NULL;
	_hideBehind = NULL;
	_hideRefreshTime = TIME_MIN;
}

void EntityAI::HideThink()
{
	// true when hide is planned
	if( _hideRefreshTime>Glob.time-5 ) return;

	AIUnit *unit = CommanderUnit();
	if (!unit ) return;

	if( _hideBehind && !_inFormation ) return; // do not change target while running

	// find target we are hinding from
	Vector3 tgtPos = HideFrom();

	if( _hideBehind )
	{
		float radius=GetType()->GetMaxSpeedMs()*20;
		saturate(radius,20,500);
		//LogF("%s: Refresh hide",(const char *)GetDebugName());

		float myDist2ToEnemy = tgtPos.DistanceXZ2(Position());

		// check distance to enemy
		if
		(
			myDist2ToEnemy>Square(radius*0.3f) &&
			myDist2ToEnemy<Square(radius*2.5f)
		)
		{
			//LogF("  Distance to enemy OK");
			// check distance to leader
			AIUnit *unit = CommanderUnit();
			AIUnit *leader = GetGroup()->Leader();
			if (!leader) return;
			float maxDistance = leader->IsAnyPlayer() ? radius : radius*0.2;
			if
			(
				unit->GetFormationAbsolute(leader).DistanceXZ2(Position())<Square(maxDistance)
			)
			{
				// well hidden - leave it as it is
				//LogF("  Distance to formation OK");
				return;
			}
		}
	}

	// refresh required
	FindHideBehind();
}


float EntityAI::FireAngleInRange( int weapon, Vector3Par rel ) const
{
	// most vehicle cannot fire high and low
	float size2=rel.SquareSizeXZ();
	float y2=Square(rel.Y());
	const float maxY=0.5;
	if( y2>size2*Square(maxY) ) return 0;
	// nearly same level
	return 1-fabs(rel.Y())*InvSqrt(size2)*(1/maxY);
}

bool EntityAI::BestFireResult
(
	FireResult &result, const Target &target,
	float &bestDist, float &minDist, float &maxDist,
	float timeToShoot, bool enableAttack
) const
{
	// if enableAttack is set, we must not evaluate weapons with enableAttack=false
	bestDist=600; // default best distance - be far
	if( !IsAbleToFire() ) return false;

	if( !CommanderUnit() ) return false;

	//int x = toIntFloor(Position().X() * InvLandGrid);
	//int z = toIntFloor(Position().Z() * InvLandGrid);
	float timeToLive=CommanderUnit()->GetTimeToLive();

	minDist=+1e10,maxDist=-1e10;
	bool someFire=false;
	for( int w=0; w<NMagazineSlots(); w++ )
	{
		const MagazineSlot &slot = GetMagazineSlot(w);
		if (enableAttack && !slot._muzzle->_enableAttack) continue;
		const WeaponModeType *mode = GetWeaponMode(w);
		if (!mode) continue;
		if (!mode->_ammo) continue;
		const AmmoType &aInfo = *mode->_ammo;
		float aMinDist=aInfo.minRange*0.75f+aInfo.midRange*0.25f;
		float aMaxDist=aInfo.midRange*0.25f+aInfo.maxRange*0.75f;
		saturateMin(minDist,aMinDist);
		saturateMax(maxDist,aMaxDist);
		// check max. possible gain with this weapon
		float bestDistance=BestDistance(aInfo);
		#if DIAG_SHOOT_RESULT
			LogF("  BestFireResult %d",w);
		#endif

		FireResult tResult;
		if
		(
			WhatShootResult
			(
				tResult,target,w,1.0,
				0,timeToLive
				,1.0,bestDistance,30,false // TODO: time argument
			)
		)
		{
			if( tResult.CleanSurplus()>result.CleanSurplus() )
			{
				result=tResult;
				someFire=true;
				bestDist=bestDistance;
			}
		}
	}
	return someFire;
}

bool EntityAI::WhatAttackResult
(
	FireResult &result, const Target &target, float timeToShoot
) const
{
	if( !CommanderUnit() ) return false;

	#if DIAG_RESULT
		LogF
		(
			"WhatAttackResult %s to %s",
			(const char *)GetDebugName(),(const char *)target.type->GetDisplayName()
		);
	#endif

	bool someFire=false;

	if( IsAbleToFire() )
	{

		//int x = toIntFloor(Position().X() * InvLandGrid);
		//int z = toIntFloor(Position().Z() * InvLandGrid);
		float timeToLive=CommanderUnit()->GetTimeToLive();

		// keep timeToLive in reasonable dimensions
		// even if we are safe now, we will not be safe when attacking
		saturate(timeToLive,5,60);

		// include time to attack
		float invSpeed = 2.0f/GetType()->GetMaxSpeedMs();
		float targetDistance = target.AimingPosition().Distance(Position());

		for( int w=0; w<NMagazineSlots(); w++ )
		{
			const MagazineSlot &slot = GetMagazineSlot(w);
			if (!slot._muzzle->_enableAttack) continue;
			const WeaponModeType *mode = GetWeaponMode(w);
			if (!mode) continue;
			if (!mode->_ammo) continue;
			const AmmoType &aInfo = *mode->_ammo;
			// check max. possible gain with this weapon
			float bestDistance=BestDistance(aInfo);

			float travelDistance = targetDistance - bestDistance;
			saturateMax(travelDistance,0);

			#if DIAG_RESULT
				LogF
				(
					"bestDist %.1f, travel %.1f",bestDistance,travelDistance
				);
			#endif

			// estimate travel time
			float travelTime = travelDistance*invSpeed;
			if (travelTime>60)
			{
				#if DIAG_RESULT
				LogF("   traveltime %3.1f",travelTime);
				#endif
				continue;
			}
			const float timeToAim=TimeToAttack + travelTime;
			FireResult tResult;
			if
			(
				WhatShootResult
				(
					tResult,target,w,1.0,timeToAim,timeToLive,
					1.0,bestDistance,timeToShoot,false
				)
			)
			{
				if( tResult.CleanSurplus()>result.CleanSurplus() )
				{
					result=tResult;
					someFire=true;
				}
			}
		}
	}

	#if 1
		// consider soldier attack
		if( !someFire && GetType()->HasCargo() )
		{
			// no weapons - check if we have some soldiers in cargo
			Assert( dyn_cast<const Transport>(this) );
			const Transport *transport=static_cast<const Transport *>(this);
			// select 'best' soldier in cargo
			// if there is any LAW soldier, use him
			const ManCargo &cargo=transport->GetManCargo();
			for( int c=0; c<cargo.Size(); c++ )
			{
				Person *soldier=cargo[c];
				if( !soldier ) continue;
				if( soldier->GetGroup()!=GetGroup() ) continue;
				FireResult tResult;
				float bestDist,minDist,maxDist;
				if( soldier->BestFireResult(tResult,target,bestDist,minDist,maxDist,timeToShoot,true) )
				{
					result=tResult;
					result.weapon = -1;
					someFire=true;
				}
				break;
			}
		}
	#endif
	#if DIAG_RESULT
		LogF
		(
			"  dammage %.2f, gain %.0f, cost %.0f, loan %.0f, weapon %d",
			result.dammage,result.gain,result.cost,result.loan,result.weapon
		);
	#endif
	return someFire;
}

/*!
\patch_internal 1.24 Date 9/20/2001 by Ondra.
- Fixed: destroyed target (idExact==NULL) was sometimes checked for visibility.
*/

bool EntityAI::WhatFireResult
(
	FireResult &result, const Target &target, float timeToShoot
) const
{
	result.weapon=-1;
	#if DIAG_RESULT
		LogF
		(
			"WhatFireResult %s to %s",
			(const char *)GetDebugName(),(const char *)target.type->GetDisplayName()
		);
	#endif
	if( !IsAbleToFire() )
	{
		#if DIAG_RESULT
			LogF(	"  cannot fire.");
		#endif
		return false; // cannot fire
	}

	AIUnit *unit = CommanderUnit();
	if( !unit ) return false;
	if (unit->GetLifeState()==AIUnit::LSDead) return false;

	if (!target.idExact) return false;

	// if visiblity is tracked, check it
	float visibility = _visTracker.KnownValue(this,_currentWeapon,target.idExact,0.9f);
	if( visibility<0 )
	{
		visibility = GLOB_WORLD->Visibility(unit,target.idExact);

		AICenter *center = unit->GetGroup()->GetCenter();
		if (center->IsFriendly(target.side))
		{
			visibility = 1;
		}
	}

	if( visibility<MinVisibleFire )
	{
		#if DIAG_RESULT
			LogF(	"  visibility low (%.3f)",visibility);
		#endif
		return false; // cannot fire
	}

	bool possible=false;
	float maxSurplus=-1e10f;

	// estimate time to live
	//int x = toIntFloor(Position().X() * InvLandGrid);
	//int z = toIntFloor(Position().Z() * InvLandGrid);
	float timeToLive=unit->GetTimeToLive();

	for( int w=0; w<NMagazineSlots(); w++ )
	{
		float timeToAim=0;
		float inRange=FireInRange(w,timeToAim,target);
		#if DIAG_RESULT
			LogF(" inRange %.3f",inRange);
		#endif
		/*
		if( inRange<1 )
		{
			// increase timeToAim if we have to turn vehicle
			timeToAim+=GetCostTurn(4);
			inRange=1;
		}
		*/
		const Magazine *magazine = GetMagazineSlot(w)._magazine;
		if (!magazine) continue;
		//LogF("  magazine %s",(const char *)magazine->_type->GetDisplayName());
		const WeaponModeType *mode = GetWeaponMode(w);
		Assert(mode);
		const AmmoType *ammo = mode->_ammo;
		if (!ammo) continue;

		// target position not known - no fire
		if( target.posError.SquareSize()>Square(ammo->indirectHitRange*2) )
		{
			#if DIAG_RESULT
				LogF("   pos Error %.1f",target.posError.Size());
			#endif
			continue;
		}

		// target not seen recently - no fire
		if( target.lastSeen<Glob.time-10 )
		{
			#if DIAG_RESULT
				LogF("   lastSeen %.1f",Glob.time-10-target.lastSeen);
			#endif
			continue;
		}

		float timeToReload=magazine->_reload+magazine->_reloadMagazine;
		// time to aim is often zero or close to zero
		timeToAim=floatMax(timeToReload,timeToAim);
		//if( inRange<=0.5 ) continue;
		float distance=target.position.Distance(Position());
		FireResult tResult;
		#if DIAG_RESULT
			LogF(" times: live %.3f, aim %.3f",timeToShoot,timeToAim);
		#endif
		if
		(
			inRange>1e-6 &&
			WhatShootResult
			(
				tResult,target,w,inRange,
				timeToAim,timeToLive,
				visibility,distance,timeToShoot,true
			)
		)
		{
			float surplus=tResult.Surplus();
			if( timeToReload>timeToShoot )
			{ // do not have enough time to shoot - make result worse
				surplus *= 0.2;
			}
			if( maxSurplus<surplus )
			{
				possible=true;
				maxSurplus=surplus;
				result=tResult;
			}
		}
	}
	#if DIAG_RESULT
		LogF
		(
			"  dammage %.2f, gain %.0f, cost %.0f, loan %.0f, weapon %d",
			result.dammage,result.gain,result.cost,result.loan,result.weapon
		);
		if( !possible ) LogF("  impossible");
	#endif
	return possible;
}

bool EntityAI::WhatFireResult
(
	FireResult &result, const Target &target, int weapon, float timeToShoot
) const
{
	result.weapon = weapon;
	#if DIAG_RESULT
		LogF
		(
			"WhatFireResult %s to %s (using %d)",
			(const char *)GetDebugName(),(const char *)target.type->GetDisplayName(),weapon
		);
	#endif
	if( !IsAbleToFire() )
	{
		#if DIAG_RESULT
			LogF(	"  cannot fire.");
		#endif
		return false; // cannot fire
	}

	AIUnit *unit = CommanderUnit();
	if (!unit) return false;
	if (unit->GetLifeState()==AIUnit::LSDead) return false;

	if (!target.idExact) return false;

	// if visiblity is tracked, check it
	float visibility = _visTracker.KnownValue(this,_currentWeapon,target.idExact,0.9f);
	if( visibility<0 )
	{
		visibility = GLOB_WORLD->Visibility(unit,target.idExact);
		// special handling of friendly units
		// friendly assumed alsways visible
		AICenter *center = unit->GetGroup()->GetCenter();
		if (center->IsFriendly(target.side))
		{
			visibility = 1;
		}
	}

	if( visibility<MinVisibleFire )
	{
		#if DIAG_RESULT
			LogF(	"  visibility low (%.3f)",visibility);
		#endif
		return false; // cannot fire
	}

	bool possible=false;
	float maxSurplus=-1e10f;

	// estimate time to live
	//int x = toIntFloor(Position().X() * InvLandGrid);
	//int z = toIntFloor(Position().Z() * InvLandGrid);
	float timeToLive=unit->GetTimeToLive();

	float timeToAim=0;
	float inRange=FireInRange(weapon,timeToAim,target);
	#if DIAG_RESULT
		LogF(" inRange %.3f",inRange);
	#endif
	/*
	if( inRange<1 )
	{
		// increase timeToAim if we have to turn vehicle
		timeToAim+=GetCostTurn(4);
		inRange=1;
	}
	*/
	const Magazine *magazine = GetMagazineSlot(weapon)._magazine;
	if (!magazine) return false;
	const WeaponModeType *mode = GetWeaponMode(weapon);
	Assert(mode);
	const AmmoType *ammo = mode->_ammo;
	if (!ammo) return false;

	// target position not known - no fire
	if (target.posError.SquareSize() > Square(ammo->indirectHitRange*2)) return false;

	// target not seen recently - no fire
	if (target.lastSeen < Glob.time - 10) return false;

	float timeToReload=magazine->_reload+magazine->_reloadMagazine;
	// time to aim is often zero or close to zero
	timeToAim=floatMax(timeToReload,timeToAim);
	//if( inRange<=0.5 ) continue;
	float distance=target.position.Distance(Position());
	FireResult tResult;
	#if DIAG_RESULT
		LogF(" times: live %.3f, aim %.3f",timeToShoot,timeToAim);
	#endif
	if
	(
		timeToReload<=timeToShoot && inRange>1e-6 &&
		WhatShootResult
		(
			tResult,target,weapon,inRange,timeToAim,
			timeToLive,visibility,distance,timeToShoot,true
		)
	)
	{
		//tResult.loan=0; // no loan
		float surplus=tResult.Surplus();
		if( maxSurplus<surplus )
		{
			possible=true;
			maxSurplus=surplus;
			result=tResult;
		}
	}
	#if DIAG_RESULT
		LogF
		(
			"  dammage %.2f, gain %.0f, cost %.0f, loan %.0f, weapon %d",
			result.dammage,result.gain,result.cost,result.loan,result.weapon
		);
		if( !possible ) LogF("  impossible");
	#endif
	return possible;
}

Threat EntityAIType::GetStrategicThreat
(
	float distance2, float visibility, float cosAngle
) const
{
	// sum for all weapons
	float maxRange=0;

	for (int i=0; i<NWeaponSystems(); i++)
	{
		const WeaponType *weapon = GetWeaponSystem(i);
		for (int j=0; j<weapon->_muzzles.Size(); j++)
		{
			const MuzzleType *muzzle = weapon->_muzzles[j];
			const MagazineType *magazine = muzzle->_typicalMagazine;
			if (!magazine) continue;
			for (int k=0; k<magazine->_modes.Size(); k++)
			{
				const AmmoType *ammo = magazine->_modes[k]->_ammo;
				if (ammo) saturateMax(maxRange, ammo->maxRange);
			}
		}
	}
	float sum=0;
	if( distance2<maxRange*maxRange )
	{
		float invDist = InvSqrt(distance2);
		float distance = distance2*invDist;
		for (int i=0; i<NWeaponSystems(); i++)
		{
			const WeaponType *weapon = GetWeaponSystem(i);
			for (int j=0; j<weapon->_muzzles.Size(); j++)
			{
				const MuzzleType *muzzle = weapon->_muzzles[j];
				const MagazineType *magazine = muzzle->_typicalMagazine;
				if (!magazine) continue;
				for (int k=0; k<magazine->_modes.Size(); k++)
				{
					const WeaponModeType *mode = magazine->_modes[k];
					const AmmoType *ammo = mode->_ammo;
					if (!ammo) continue;

					// do not try to get under best distance of weapons
					// consider: for very long range weapons this could be very clever
					float probab;
					float bestDist = BestDistance(*ammo, probab);
					if (distance > bestDist)
					{
						probab = HitProbability(distance, *ammo) * visibility;
					}
					float oneHit = probab * ammo->hit * visibility;
					float hitsPerMinute = 60.0f / mode->_reloadTime;
					saturateMin(hitsPerMinute, magazine->_maxAmmo);
					sum += hitsPerMinute * oneHit;
				}
			}
		}
		float backCoef = 1.0;
		float frontCoef = 1.0;
		if (distance>150)
		{
			// no drop down on minimal distance
			// spot danger
			sum *= 1-distance/maxRange;
			// small difference betweem front and back
			frontCoef = 2.5f * _sensitivity;
			// assume it will not look back
			backCoef = 2.5f * (_sensitivity*0.3+_sensitivityEar*0.7);
		}
		else
		{
			frontCoef = 2.5f * _sensitivity;
			// may easily look back
			backCoef = 2.5f * (_sensitivity*0.7+_sensitivityEar*0.3);
		}

		if (cosAngle>0) sum *= frontCoef;
		else sum *=backCoef;

		//LogF
		//(
		//	"  %s: GetStrategicThreat cosA %.2f, sum %.1f, dist %.1f, vis %.1f",
		//	(const char *)GetDisplayName(),cosAngle,sum,distance,visibility
		//);

		// check vehicle sensitivity
	}
	return _threat*sum;
}

Threat EntityAIType::GetDammagePerMinute
(
	float distance2, float visibility, EntityAI *vehicle
) const
{
	// sum for all weapons
	float maxRange=0;
	for (int i=0; i<NWeaponSystems(); i++)
	{
		const WeaponType *weapon = GetWeaponSystem(i);
		for (int j=0; j<weapon->_muzzles.Size(); j++)
		{
			const MuzzleType *muzzle = weapon->_muzzles[j];
			const MagazineType *magazine = muzzle->_typicalMagazine;
			if (!magazine) continue;
			for (int k=0; k<magazine->_modes.Size(); k++)
			{
				const AmmoType *ammo = magazine->_modes[k]->_ammo;
				if (ammo) saturateMax(maxRange, ammo->maxRange);
			}
		}
	}
	float sum=0;
	if( distance2<maxRange*maxRange )
	{
		float distance=sqrt(distance2);
		if (vehicle)
		{
			for (int i=0; i<vehicle->NMagazineSlots(); i++)
			{
				const MagazineSlot &slot = vehicle->GetMagazineSlot(i);
				const Magazine *magazine = slot._magazine;
				if (!magazine) continue;
				const WeaponModeType *mode = vehicle->GetWeaponMode(i);
				Assert(mode);
				const AmmoType *ammo = mode->_ammo;
				if (!ammo) continue;

				float probab = HitProbability(distance, *ammo) * visibility;
				float oneHit = probab * ammo->hit * visibility;
				float hitsPerMinute = 60.0f / mode->_reloadTime;
				saturateMin(hitsPerMinute, magazine->_ammo);
				sum += hitsPerMinute * oneHit;
			}
		}
		else
		{
			for (int i=0; i<NWeaponSystems(); i++)
			{
				const WeaponType *weapon = GetWeaponSystem(i);
				for (int j=0; j<weapon->_muzzles.Size(); j++)
				{
					const MuzzleType *muzzle = weapon->_muzzles[j];
					const MagazineType *magazine = muzzle->_typicalMagazine;
					if (!magazine) continue;
					for (int k=0; k<magazine->_modes.Size(); k++)
					{
						const WeaponModeType *mode = magazine->_modes[k];
						const AmmoType *ammo = mode->_ammo;
						if (!ammo) continue;

						float probab = HitProbability(distance, *ammo) * visibility;
						float oneHit = probab * ammo->hit * visibility;
						float hitsPerMinute = 60.0f / mode->_reloadTime;
						saturateMin(hitsPerMinute, magazine->_maxAmmo);
						sum += hitsPerMinute * oneHit;
					}
				}
			}
		}
	}
	return _threat*sum;
}

bool EntityAI::GetAIFireEnabled(Target *tgt) const
{
	// check if commander have fire enabled
	AIUnit *unit = CommanderUnit();
	if (!unit) return false;
	return unit->IsFireEnabled(tgt);
}

void EntityAI::ReportFireReady() const
{
	// report theat we would like to fire
	// wait until state is steady
	if (_lastWeaponNotReady>Glob.time-5) return;
	// report when changed or did not report for a while
	if (_lastWeaponReady<Glob.time-30 || _lastWeaponNotReady>_lastWeaponReady)
	{
		_lastWeaponReady = Glob.time;
		// send report
		AIUnit *unit = CommanderUnit();
		if (!unit) return;
		AIGroup *grp = unit->GetGroup();
		if (!grp) return;
		grp->ReportFire(unit,true);
	}
	
}

void EntityAI::ReportFireNotReady() const
{
	// wait until state is steady
	if (_lastWeaponReady>Glob.time-5) return;
	// report when changed or did not report for a while
	if (_lastWeaponNotReady<Glob.time-30 || _lastWeaponReady>_lastWeaponNotReady)
	{
		_lastWeaponNotReady = Glob.time;
		// send report
		AIUnit *unit = CommanderUnit();
		if (!unit) return;
		AIGroup *grp = unit->GetGroup();
		if (!grp) return;
		grp->ReportFire(unit,true);
	}
}


FireDecision::FireDecision()
{
	_fireMode = -1; // what weapon should be used
	//LinkTarget _fireTarget; // what target vehicle should fire at
	_firePrepareOnly = true;
	_nextWeaponSwitch = Glob.time; // change weapon
	_nextTargetAquire = Glob.time; // acquire target when no target is acquired
	_nextTargetChange = Glob.time; // change target (when target is acquired)
	_initState = TargetAlive;
}

void FireDecision::SetTarget(AIUnit *sensor, Target *tgt)
{
	if (tgt==_fireTarget) return;
	_fireTarget = tgt;
	if (!tgt) _initState = TargetAlive;
	else _initState = tgt->State(sensor);
}
bool FireDecision::GetTargetFinished(AIUnit *sensor) const
{
	if (_firePrepareOnly) return false; // target never finished
	TargetState state = _fireTarget->State(sensor);
	return state<_initState;
}

void EntityAI::SelectFireWeapon()
{
	SelectFireWeapon(_fire);
}

/*!
\patch 1.08 Date 7/21/2001 by Ondra.
- Fixed: when using weapon only to look, prefer actual weapon.
This helps to avoid unnecessary weapon changes.
- Fixed: AI watched non-combat enemies.
This made weapon selection unstable,
binoculars were also selected with no apparent reason.
Only combat enemies are watched now.
\patch 1.24 Date 9/20/2001 by Ondra.
- Fixed: AI sometimes did not unlock target after destroying it.
\patch 1.27 Date 10/18/2001 by Ondra.
- Fixed: AI: If there are no other targets,
 watch first enemy that was only heard but not seen yet.
\patch 1.42 Date 1/4/2002 by Ondra
- Fixed: AI used binocular too often.
\patch 1.85 Date 9/20/2002 by Ondra
- Fixed: AI units often looked backward.
*/

void EntityAI::SelectFireWeapon(FireDecision &fire)
{
	AIUnit *unit=CommanderUnit();

	if (_userStopped) return; // no targeting
	
	Assert( unit );
	if (!unit) return;
	if (_forceFireWeapon >= 0)
	{
		fire.SetTarget(unit,NULL);
		if (_forceFireWeapon != SelectedWeapon())
		{
			SelectWeapon(_forceFireWeapon);
		}
		return;
	}
	
	AIGroup *grp=unit->GetGroup();
	Assert( grp );
	if (!grp) return;
	AICenter *center=grp->GetCenter();
	if (!center) return;

	#if DIAG_TARGET
	//if (center->GetSide()!=TWest) return;
	if (this!=GWorld->CameraOn()) return;
	#endif

	// do not change weapons too often

	// _nextTargetAquire is used when we are in combat and do not have any target acquired
	// _nextWeaponSwitch is used in all other situations
	// _nextTargetAquire is generaly sooner
	if (fire._fireTarget)
	{
		if (fire.GetTargetFinished(unit))
		{
			// do not fire on vanished or destroyed target
			fire.SetTarget(unit,NULL);
		}
		else if (fire._fireTarget->vanished)
		{
			fire.SetTarget(unit,NULL);
		}
	}
	if (unit->GetCombatMode()<=CMAware || fire._fireTarget)
	{
		if( fire._nextWeaponSwitch>Glob.time )
		{
			return;
		}
	}
	else
	{
		// use _nextTargetAquire
		if( fire._nextTargetAquire>Glob.time )
		{
			return;
		}
	}


	//LogF("%s: SelectFireWeapon",(const char *)GetDebugName());


	// if we are assinged to something very important
	// do not switch for something small
	float targetMinSubjCost = 0;

	Target *tgt=unit->GetTargetAssigned();
	if( tgt )
	{
		if (tgt->State(unit)>TargetDestroyed)
		{
			// if we do not know target precision exactly enough
			// target may be very old
			targetMinSubjCost = tgt->subjectiveCost*0.5;
			if( !tgt->IsKnownBy(unit) || tgt->lastSeen<Glob.time-5 ) tgt=NULL;
		}
		else
		{
			// assigned target already destroyed
			tgt = NULL;
		}
	}

	float timeToLive=unit->GetTimeToLive();
	//saturateMax(timeToLive,5);

	FireResult fResult;
	bool fResultPossible = false;
	// check if we may change target
	bool enableOtherTarget = false;

	if (fire._fireTarget && !fire._firePrepareOnly && fire._nextTargetChange>=Glob.time)
	{
		// target is very recent
		// select most efficient weapon for given target only
		tgt = fire._fireTarget;
		fResultPossible = WhatFireResult(fResult,*tgt,timeToLive);

		#if DIAG_TARGET>=3
		if (fResult.weapon!=fire._fireMode)
		{
			LogF
			(
				"%s: SelectWeapon %.1f, weapon %d",
				(const char *)GetDebugName(),Glob.time-Time(0),fResult.weapon
			);
		}
		#endif
	}
	else
	{
		// select weapon and target
		if( tgt ) fResultPossible = WhatFireResult(fResult,*tgt,timeToLive);
		enableOtherTarget = true;
		#if DIAG_TARGET>2
		LogF
		(
			"%s: SelectTarget %.1f, weapon %d",
			(const char *)GetDebugName(),Glob.time-Time(0),fResult.weapon
		);
		LogF("  time expired (%.2f)",Glob.time-fire._nextTargetChange);
		LogF("  target %s",fire._fireTarget ? (const char *)fire._fireTarget->idExact->GetDebugName() : "<null>");
		#endif
	}

	if (unit->GetAIDisabled()&AIUnit::DAAutoTarget)
	{
		enableOtherTarget = false;
	}

	// FIX
	// when engaging vehicle, crew may be killed
	// vehicle is that considered to be civilian
	// check below will prevent firing at it.
	// FIX END

	// note: target may be friendly
	// in that case do not fire, unless explicitly ordered
	if (tgt && !center->IsEnemy(tgt->side))
	{
		// check if fire has been ordered
		if (unit->GetEnableFireTarget()!=tgt)
		{
			// target not suitable for firing
			fResultPossible = false;
		}
	}
	if (fResultPossible && !enableOtherTarget && !unit->IsHoldingFire())
	{
		#if DIAG_TARGET>=2
		if (fire._firePrepareOnly)
		{
			LogF("  fire possible, active fire enabled");
		}
		#endif
		fire._firePrepareOnly = false;
	}
	if
	(
		!fResultPossible && enableOtherTarget && !unit->IsHoldingFire()
	)
	{
		// preferred target not visible - fire on anything else
		// select only few nearest enemy targets
		// select enemy target with best fire result
		const TargetList &list=grp->GetTargetList();
		int maxEnemies=4;
		for( int i=0; i<list.Size(); i++ )
		{
			FireResult tResult;
			Target *tgtI=list[i];
			if( !tgtI->IsKnownBy(unit) ) continue;
			if (tgtI->destroyed) continue;
			if (tgtI->vanished) continue;
			if( !center->IsEnemy(tgtI->side) ) continue;
			if (tgtI->State(unit)<TargetEnemyCombat) continue;
			if (tgtI->subjectiveCost<targetMinSubjCost) continue;
			if (tgtI->lastSeen<Glob.time-5) continue;
			if( WhatFireResult(tResult,*tgtI,timeToLive) )
			{
				#if DIAG_TARGET
					LogF
					(
						"%s to %s: surplus %g",
						(const char *)GetDebugName(),
						(const char *)tgtI->idExact->GetDebugName(),
						tResult.Surplus()
					);
				#endif
				if( tResult.Surplus()>fResult.Surplus() )
				{
					fResult = tResult;
					tgt = tgtI;
					fResultPossible = true;
				}
			}
			#if DIAG_TARGET>=2
			else
			{
				LogF
				(
					"%s to %s: WhatFireResult => false",
					(const char *)GetDebugName(),
					(const char *)tgtI->idExact->GetDebugName()
				);
			}
			#endif
			if( --maxEnemies<=0 ) break;
		}
	}

	if (!tgt && enableOtherTarget)
	{
		// try to find some unknown target
		// so that we can use scope to look at it
		// this applies only when we have no target assigned
		const TargetList &list=grp->GetTargetList();
		if (!unit->GetTargetAssigned())
		{
			for( int i=0; i<list.Size(); i++ )
			{
				Target *tgtI=list[i];
				if( !tgtI->IsKnownBy(unit) ) continue;
				if (tgtI->vanished) continue;
				if (tgtI->destroyed) continue;
				if (tgtI->side!=TSideUnknown) continue;
				if (tgtI->lastSeen<Glob.time-60) continue;
				// do not watch unknown static targets - no need to watch them
				if (tgtI->idExact && tgtI->idExact->Static()) continue;
				tgt=tgtI;
				break;
			}
		}
		if( !tgt )
		{
			tgt=unit->GetTargetAssigned();
			// check if alive
			if (tgt && tgt->State(unit)<TargetAlive) tgt = NULL;
		}
		if( !tgt )
		{
			// if there is no unknown nor assigned target
			// watch first enemy that is was only heard
			// {{ FIX 1.27
			for( int i=0; i<list.Size(); i++ )
			{
				Target *tgtI=list[i];
				if (tgtI->vanished) continue;
				if (tgtI->destroyed) continue;
				if (!tgtI->IsKnownBy(unit)) continue;
				if (!center->IsEnemy(tgtI->side)) continue;
				if (tgtI->posError.SquareSize()<Square(0.9)) continue;
				if (tgtI->lastSeen<Glob.time-60) continue;
				tgt=tgtI;
				break;
			}
			// }} FIX
		}


		if( tgt )
		{
			// select weapon with best zoom
			float minFov = 1e10;
			// FIX: for looking prefer actual weapon
			int weapon=fire._fireMode;
			if (weapon<0) weapon = _currentWeapon;
      if (weapon >= 0 && weapon < NMagazineSlots())
			{
				const MagazineSlot &slot = GetMagazineSlot(weapon);
				const MuzzleType *muzzle = slot._muzzle;
				// 
				minFov = muzzle->_opticsZoomMax;
			}
			// FIX END

			// check if enemy can be seen by plain eye
			// if not, do not use binoculars or other optics in not current weapon
			if (tgt->posError.SquareSize()<Square(0.9))
			{

				// note: weapons without magazines need to be checked too?
				// TODO: some weapons should not be used for looking (AT missiles)

				for (int i=0; i<NMagazineSlots(); i++)
				{
					const MagazineSlot &slot = GetMagazineSlot(i);
					const MuzzleType *muzzle = slot._muzzle;
					// 
					float fov = muzzle->_opticsZoomMax;
					if (minFov>fov)
					{
						minFov = fov;
						weapon = i;
					}
				}
				if
				(
					_fireState==FireDone && Glob.time>_fireStateDelay ||
					fire._fireMode<0 || fire._fireTarget==NULL
				)
				{
					_fireState=FireInit;
					_fireStateDelay=Glob.time+0.2+GRandGen.RandomValue()*0.5;
				}
			}
			#if DIAG_TARGET>=2
				LogF("Change look weapon %d to %d",fire._fireMode,weapon);
			#endif

			fire._fireMode=weapon;
		}
		else
		{
			// select some weapon
			// preferred: machine gun or gun
			//LogF("  No Target");
			fire._fireMode=-1;
		}

		if (fire._fireTarget!=tgt)
		{
			// some minimal time to watch given 
			fire._nextTargetChange = Glob.time + floatMin(10,GetType()->GetMinFireTime());
			fire.SetTarget(unit,tgt);
			#if DIAG_TARGET
				LogF
				(
					"%s : prepare target %s until %.1f, state %d",
					(const char *)GetDebugName(),
					tgt ? (const char *)tgt->idExact->GetDebugName() : "<null>",
					fire._nextTargetChange-Time(0),
					tgt ? tgt->State(unit) : -1
				);
			#endif
		}
		fire._firePrepareOnly=true;
		fire._initState = TargetEnemyCombat; // if switched to fire, stop immediatelly
		// no need to reconsider targets to often: no enemy
		//LogF("%s: weapon switch %.2f",(const char *)GetDebugName(),Glob.time-Time(0));
		fire._nextWeaponSwitch = Glob.time+0.5;
		fire._nextTargetAquire = Glob.time+0.1;

		return;
	}

	if (!tgt)
	{
		fire._firePrepareOnly=true;
		fire.SetTarget(unit,NULL);
		fire._nextWeaponSwitch = Glob.time+0.5;
		fire._nextTargetAquire = Glob.time+0.1;
		return;
	}

	#if 0
	if( this==GWorld->CameraOn() && tgt && tgt->idExact)
	{
		GlobalShowMessage
		(
			100,"Sec target %s",
			(const char *)tgt->idExact->GetDebugName()
		);
	}
	#endif
	// shoot
	// if target is friendly, shoot only when explicitly assigned
	if( fResult.weapon>=0 )
	{
		if
		(
			_fireState==FireDone && Glob.time>_fireStateDelay ||
			fire._fireMode<0 || fire._fireTarget==NULL
		)
		{
			_fireState=FireInit;
			_fireStateDelay=Glob.time+0.2+GRandGen.RandomValue()*0.5;
		}
		// select best weapons against this target
		int weapon=fResult.weapon;
		if( fire._fireMode!=weapon )
		{
			fire._nextWeaponSwitch=Glob.time+1.5;
			fire._nextTargetAquire = Glob.time+1.5;
			//LogF("%s: weapon switch sw %.2f",(const char *)GetDebugName(),Glob.time-Time(0));
		}

		//SelectWeapon(weapon);
		fire._fireMode=weapon;
	}
	else
	{
		// check if there is some ammo in weapon
		if( fire._fireMode>=0 )
		{
			const Magazine *magazine = GetMagazineSlot(fire._fireMode)._magazine;
			if (!magazine || magazine->_ammo == 0) fire._fireMode = -1;
		}
	}
	if (fire._fireTarget!=tgt)
	{
		fire._nextWeaponSwitch=Glob.time+1.5;
		fire._nextTargetAquire = Glob.time+1.5;
		fire._nextTargetChange = Glob.time + GetType()->GetMinFireTime();
		#if DIAG_TARGET
		LogF
		(
			"%s : select target %s until %.1f, state %d, weapon %d",
			(const char *)GetDebugName(),
			tgt->idExact ? (const char *)tgt->idExact->GetDebugName() : "<null>",
			fire._nextTargetChange-Time(0),
			tgt ? tgt->State(unit) : -1,
			fire._fireMode
		);
		#endif
	}
	//LogF("  Set Fire Target %s",(const char *)tgt->idExact->GetDebugName());
	fire.SetTarget(unit,tgt);

	fire._firePrepareOnly=
	(
		!fResultPossible || fResult.weapon<0 || fire.GetTargetFinished(unit)
	);
	#if DIAG_TARGET
		if (fire._firePrepareOnly)
		{
			LogF("  prepare only");
		}
	#endif
}
