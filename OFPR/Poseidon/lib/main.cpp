/*!
\file
Basic win32 framework, application init and message handling.
*/

#include "wpch.hpp"

#ifdef _WIN32
  #include <io.h>
#endif
#include <sys/stat.h>
#include <fcntl.h>
#ifdef _WIN32
  #include <process.h>
#else
  #include <sys/time.h>
  #include <signal.h>
#endif
#include <time.h>
#ifdef _WIN32
  #include "imexhnd.h"
#endif
#include "resrc1.h"
#include <Es/Strings/bstring.hpp>
#include <Es/Containers/hashMap.hpp>

#include <El/Common/perfLog.hpp>
#include "perfProf.hpp"

#include <El/Common/randomGen.hpp>
#include <Es/Common/filenames.hpp>

#include "global.hpp"
#include "keyInput.hpp"

#include <El/QStream/QBStream.hpp>
#include "world.hpp"
#include "progress.hpp"
#include "engine.hpp"
#include "ai.hpp"
#include "arcadeTemplate.hpp"
#include "crc.hpp"

#include "debugTrap.hpp"


#include "soundDX8.hpp"

#include "chat.hpp"

#include "landscape.hpp"
#include <El/ParamFile/paramFile.hpp>
//#include "loadStream.hpp"
#ifdef _WIN32
  #include "Joystick.hpp"
#endif

//#include "strIncl.hpp"
#include "stringtableExt.hpp"

#include "fileServerMT.hpp"

#include <Es/Types/lLinks.hpp>

#include "engineDll.hpp"

#include "network.hpp"

#include "integrity.hpp"

#include "mbcs.hpp"

#if 0 //!_SUPER_RELEASE
	#include "networkImpl.hpp"
	template Ref<NetworkObject>;
#endif

#ifdef _XBOX
	#include "vkCodes.h"
	#include "dikCodes.h"
#endif
#if PROFILE
	void EnableProfiler();
	void DisableProfiler();

	#define PROFILE_INIT 1
	#define PROFILE_RUN 1
	#define PROFILE_EXIT 1
#endif

#ifdef _XBOX
	#define exit(a) XBOX_Exit(a)
	void XBOX_Exit(int code)
	{
		LogF("Exit");
		for(;;)
		{
			Sleep(10000);
			__asm
			{
				int 3;
			}
		}
	}

#else
#  include "Es/essencepch.hpp"
#  include "Es/Framework/netlog.hpp"
#endif

extern ParamFile Pars;
extern ParamFile Res;

extern ParamFile ExtParsCampaign;
extern ParamFile ExtParsMission;

extern int MaxGroups;

#if USE_WINDOW
	bool UseWindow=true;
#else
	bool UseWindow=false;
#endif
bool HideCursor = false;
static bool MouseEx = true;
bool UseGlide = true;
/*!
\patch 1.32 Date 11/26/2001 by Jirka
- Added: Additional program parameter: -sockets
\patch_internal 1.50 Date 4/9/2002 by Jirka
- Added: Additional program parameter: -dplay, defalt is -sockets
*/

static bool UseSockets = true;
bool IsUseSockets() {return UseSockets;}
void SetUseSockets(bool set) {UseSockets = set;}
int D3DAdapter = -1;

bool NoRedrawWindow=false; // do not redraw window in D3D fullscreen mode

static HINSTANCE  hInstApp;
extern HWND       hwndApp;

static bool fAppActive;
static bool fAppPaused;
static bool appIconic;
static bool ValidateQuit; // tell that user really wants to terminate
bool CloseRequest;

/*!
\patch_internal 1.50 Date 4/12/2002 by Jirka
- Added: support for mods
\patch 1.51 Date 4/16/2002 by Jirka
- Added: Additional program parameter: -mod
*/

#ifndef _MOD_PATH_DEFAULT
#define _MOD_PATH_DEFAULT ""
#endif

RString ModPath = _MOD_PATH_DEFAULT;

//! get readable mod list
RString GetModList()
{
  return ModPath;
}

/*!
\patch_internal 1.26 Date 10/03/2001 by Ondra
- New: different log files for server and client.
*/
#if _ENABLE_REPORT
	static const char *LogFilename = "debug.log";
	static bool ResetLogFile = false;
#endif

/*!
\patch_internal 1.05 Date 7/18/2001 by Jirka
- Added: enable access to flags fAppActive, fAppPaused, appIconic from network.cpp
*/
bool IsAppPaused()
{
	return !fAppActive || fAppPaused || appIconic;
}

extern bool NoGlideDeactivate;
extern bool EnableMemMap;

extern bool NoLandscape;
extern bool EnablePIII;
extern bool GMergeTextures;
bool EnableHWTL = false;

bool NoSound=false;

#if _ENABLE_CHEATS
#include "keyLights.hpp"
KeyLights KeyState;
#endif


#if REM
class MainInfo // no real class - I want to have this file in ClassView
{
};
#endif

extern void ReportMemory();
extern void MemoryFootprint();

/*!
\patch_internal 1.42 Date 1/7/2002 by Jirka
- Changed: _NO_BLOOD option for Korean version was deleted 
*/

bool NoBlood()
{
/*
#if _NO_BLOOD
	return true;
#else
	return GetLangID() == Korean;
#endif
*/
	return false;
}

#if 0
//! Convert version info into float
float VersionToFloat(const char *ptr)
{
	float version = 0;
	while (isdigit(*ptr))
	{
		version *= 10.0f;
		version += *ptr - '0';
		ptr++;
	}
	if (*ptr == '.')
	{
		ptr++;
		float offset = 0.1f;
		while (isdigit(*ptr))
		{
			version += offset * (*ptr - '0');
			offset *= 0.1f;
			ptr++;
		}
	}
	return version;
}
#endif

//! Convert version info into float
int VersionToInt ( const char *ptr )
{
  int version = 0;
  while ( isdigit(*ptr) ) {
    version *= 10;
    version += *ptr - '0';
    ptr++;
    }
  version *= 1000;
  if ( *ptr++ == '.' ) {
    int fr = 100;
    while ( fr && isdigit(*ptr) ) {
      version += fr * (*ptr - '0');
      fr /= 10;
      ptr++;
      }
    }
  return version;
}

//! different directory used in Poseidon and PoseidonEx branches
typedef bool ModDirectoryCallback(RString dir, void *context);

static bool EnumModDirectories(ModDirectoryCallback callback, void *context)
{
#if _XBOX
	return callback("d:",context);
#else
	if (ModPath.GetLength() > 0)
	{
		Temp<char> buffer((const char *)ModPath, ModPath.GetLength() + 1);

		char *ptr;
		while (ptr = strrchr(buffer, ';'))
		{
			*ptr = 0;
			if (callback(ptr + 1, context)) return true;
		}
		if (callback((const char *)buffer, context)) return true;
	}
	return callback("", context);
#endif
}

#if _ENABLE_ADDONS

///////////////////////////////////////////////////////////////////////////
// {{
// note: This sections is intented to provide context sensitive access
// to various addon files.
// It is far from working yet, currently only config context handling is done

/*
class CfgPatches
{
     class zwa_bench
     {
         units[] = {zwa_bench};
         weapons[] = {};
         requiredVersion = 1.40;
				 requiredAddons[] = {"Resistance"};
     };

};
*/

//! contain information necessary to load addon
class AddonInfo
{
	RString _name;
	RString _prefix;

	public:
	AddonInfo(){}
	AddonInfo(RString name, RString prefix)
	:_name(name),_prefix(prefix)
	{
	}
	const char *GetKey() const {return _name;}
	RString GetName() const {return _name;}
	RString GetPrefix() const {return _prefix;}
};

TypeIsMovableZeroed(AddonInfo)

//! map telling which bank corresponds to which addon name
typedef MapStringToClass<AddonInfo,AutoArray<AddonInfo> > AddonInfoMap;


static AddonInfoMap AddonToPrefix;

static void UnlockAddon(AddonInfo &addon, AddonInfoMap *map, void *context)
{
	RString prefix = addon.GetPrefix();
	// find given bank in global bank list
	GFileBanks.Unlock(prefix);
}

static void MarkAddonLockable(AddonInfo &addon, AddonInfoMap *map, void *context)
{
	RString prefix = addon.GetPrefix();
	// find given bank in global bank list
	GFileBanks.SetLockable(prefix,true);
}


//! load addon and make it active
void LoadAddon(const char *addon)
{
	const AddonInfo &info = AddonToPrefix[addon];
	if (AddonToPrefix.IsNull(info))
	{
		LogF("Cannot load addon %s - addon does not exist",(const char *)addon);
		return;
	}
}


//! Prepare addon for unloading
void UnloadAddon(const char *addon)
{
	const AddonInfo &info = AddonToPrefix[addon];
	if (AddonToPrefix.IsNull(info))
	{
		LogF("Cannot unload addon %s - addon does not exist",(const char *)addon);
		return;
	}
	// unload all vehicles and weapons defined by this addon?
	// flush all files used by this bank
	for (int i=0; i<GFileBanks.Size();i++)
	{
		QFBank &bank = GFileBanks[i];
		if (stricmp(bank.GetPrefix(), info.GetPrefix()) == 0)
		{
			GFileServer->FlushBank(&bank);
		}
	}
}


//! list of enabled file banks
class BankContext: public IQFBankContext
{
	// list of enabled banks
	MapStringToClass<RString,AutoArray<RString> > _banks;

	public:
	//! check if file is from some of the banks listed
	bool IsAccessible(QFBank *bank) const;

	//! clear bank list
	void Clear();
	//! add single bank
	void Add(RString addon);
	//! compact memory usage
	void Compact();
};


bool BankContext::IsAccessible(QFBank *bank) const
{
	if (_banks.IsNull(_banks[bank->GetPrefix()])) return false;
	return true;
}
void BankContext::Clear()
{
	_banks.Clear();
}
void BankContext::Add(RString addon)
{
	_banks.Add(addon);
}

void BankContext::Compact()
{
	//_banks.Compact();
}

//! addon interface to file bank list

class AddonContext: public BankContext
{
	public:
	//! activate all default addons and no other
	void Reset();
	//! activate single addon
	void AddAddon(RString addon);
	
};

void AddonContext::Reset()
{
	Clear();
	// active all default addons
	const ParamEntry &def = Pars>>"CfgAddons">>"PreloadBanks";
	for (int c=0; c<def.GetEntryCount(); c++)
	{
		const ParamEntry &cc = def.GetEntry(c);
		if (!cc.IsClass()) continue;
		if (!cc.FindEntry("list")) continue;
		const ParamEntry &cl = cc>>"list";
		for (int i=0; i<cl.GetSize(); i++)
		{
			RString bank = cl[i];
			LogF("Activating bank %s",(const char *)bank);
			Add(bank);
		}
	}
};


void AddonContext::AddAddon(RString addon)
{
	const AddonInfo &info = AddonToPrefix[addon];
	if (AddonToPrefix.IsNull(info)) return;
	Add(addon);
}

//! Checks addon name as given by addon
static RString CheckAddonName(const ParamEntry &addon)
{
	const ParamEntry *patches = addon.FindEntry("CfgPatches");
	if (!patches || patches->GetEntryCount() == 0) return RString();

	// several addons may be present in this bank
	// in that case use first of them
	// it should not matter, as all of them should be always loaded together
	return patches->GetEntry(0).GetName();
}

//! Checks required version for addon
static bool CheckVersion(const RString &prefix, const ParamEntry &addon)
{
	const ParamEntry *patches = addon.FindEntry("CfgPatches");
	if (!patches || patches->GetEntryCount() == 0) return false;

	//const ParamEntry *loadedAddons = Pars.FindEntry("CfgPatches");

	RString GetAppVersion();
	RString strVersion = GetAppVersion();
#if 0
	float version = VersionToFloat(strVersion);
#else
    int version = VersionToInt(strVersion);
#endif

	for (int i=0; i<patches->GetEntryCount(); i++)
	{
		const ParamEntry &patch = patches->GetEntry(i);
		/*
		if (loadedAddons &&	loadedAddons->FindEntry(patch.GetName()))
		{
			RptF
			(
				"Conflicting addon %s ('%s') (1)",
				(const char *)patch.GetName(),(const char *)prefix
			);
		}
		*/


		const ParamEntry *entry = patch.FindEntry("requiredVersion");
		if (entry)
		{
			RString required = *entry;
#if 0
			if (VersionToFloat(required) > version)
#else
			if (VersionToInt(required) > version)
#endif
			{
				WarningMessage
				(
#ifdef _WIN32
					LocalizeString(IDS_MSG_ADDON_VERSION),
#endif
					(const char *)patch.GetName(), (const char *)required
				);
				return false;
			}
		}
		// name of patch class is name of the addon
		#if 0
		LogF
		(
			"Addon %s in bank %s",
			(const char *)patch.GetName(),(const char *)prefix
		);
		#endif
		const AddonInfo &info = AddonToPrefix[patch.GetName()];
		if (AddonToPrefix.NotNull(info))
		{
			RptF
			(
				"Conflicting addon %s in '%s', previous definition in '%s'",
				(const char *)patch.GetName(),(const char *)prefix,
				(const char *)info.GetPrefix()
			);
		}
		else
		{
			AddonToPrefix.Add(AddonInfo(patch.GetName(),prefix));
		}
	}
	return true;
}

// }}
///////////////////////////////////////////////////////////////////////////

static AutoArray< SRef<ParamFile> > AddonConfigs;

//! callback function to parse addon config
//! assumes bank is already fully loaded

static bool ParseAddonConfig(const RString &prefixPath)
{
	// config
	RString filename;
	filename = prefixPath + RString("config.cpp");
	if (QIFStreamB::FileExist(filename))
	{
		SRef<ParamFile> addon = new ParamFile;

		if (QIFStream::FileExists(filename))
		{
			// read from directory
			RString folderPath;
			int n = prefixPath.GetLength();
			DoAssert(prefixPath[n - 1] == '\\');
			folderPath = prefixPath.Substring(0, n - 1);

			char buf[1024];
			GetCurrentDirectory(sizeof(buf),buf);
			#ifndef _WIN32
			unixPath((char*)(const char*)folderPath);
			#endif
			SetCurrentDirectory(folderPath);
			addon->Parse("config.cpp");
			SetCurrentDirectory(buf);
		}
		else
		{
			// read from bank
			addon->Parse(filename);
		}
		
		// check required version for addon
		if (CheckVersion(prefixPath,*addon))
		{
			addon->SetOwner(CheckAddonName(*addon));
			AddonConfigs.Add(addon);
			//Pars.Update(addon);
			//Pars.SetFile(&Pars);
		}
		else
		{
			return false;
		}
	}
	else
	{
		filename = prefixPath + RString("config.bin");
		if (QIFStreamB::FileExist(filename))
		{
			SRef<ParamFile> addon = new ParamFile;
			addon->ParseBin(filename);
			// check required version for addon
			if (CheckVersion(prefixPath,*addon))
			{
				addon->SetOwner(CheckAddonName(*addon));
				// addon config is not parsed yet, it is only stored 
				AddonConfigs.Add(addon);

				
				//Pars.Update(addon);
				//Pars.SetFile(&Pars);
			}
			else
			{
				// bank is not supported
				return false;
			}
		}
	}
	// stringtable
	filename = prefixPath + RString("stringtable.csv");
	if (QIFStreamB::FileExist(filename))
	{
		// GStringTable->Load(filename);
		LoadStringtable("Global", filename, 0, false);
	}

	return true;
}

static bool ParseAddonConfigCallback(QFBank *bank, BankContextBase *context)
{
	//CheckAddonContext *cpc = static_cast<CheckAddonContext *>(context);
	if (!ParseAddonConfig(bank->GetPrefix()))
	{
		return false;
	}
	return true;
}

TypeIsSimple(const ParamFile *)

//! information about what addon is dependent on what addons

struct AddonDependency
{
	bool preloaded;
	const ParamFile *addon;
	AutoArray<const ParamFile *> dependsOn;

	//! dependency resolved - may be removed from addon list
	void Resolved(const AddonDependency &dep);
};

TypeIsMovable(AddonDependency)

void AddonDependency::Resolved(const AddonDependency &dep)
{
	for (int i=0; i<dependsOn.Size(); i++)
	{
		if (dependsOn[i]!=dep.addon) continue;
		dependsOn.Delete(i--);
	}
}

//! extract some addon name from addon config
static RStringB GetAddonName(const ParamFile &config)
{
	const ParamEntry *cfg = config.FindEntry("CfgPatches");
	if (cfg)
	{
		//! enumerate all addons in this config
		for (int i=0; i<cfg->GetEntryCount(); i++)
		{
			const ParamEntry &entry = cfg->GetEntry(i);
			if (!entry.IsClass()) continue;
			return entry.GetName();
		}
	}
	return "";
}

//! find addon config (ParamFile) corresponding to given addon name
static const ParamFile *FindAddonConfig(RStringB name)
{
	for (int i=0; i<AddonConfigs.Size(); i++)
	{
		// scan all entries if CfgPatches
		const ParamFile &config = *AddonConfigs.Get(i);
		const ParamEntry *cfg = config.FindEntry("CfgPatches");
		if (cfg)
		{
			//! enumerate all addons in this config
			for (int i=0; i<cfg->GetEntryCount(); i++)
			{
				const ParamEntry &entry = cfg->GetEntry(i);
				if (!entry.IsClass()) continue;
				if (!strcmpi(entry.GetName(),name)) return &config;
			}
		}
	}
	return NULL;
}

//! check if addon is preloaded (default, official) or not
static bool IsPreloadedAddon(RStringB name)
{
	const ParamEntry &def = Pars>>"CfgAddons">>"PreloadAddons";
	for (int c=0; c<def.GetEntryCount(); c++)
	{
		const ParamEntry &cc = def.GetEntry(c);
		if (!cc.IsClass()) continue;
		if (!cc.FindEntry("list")) continue;
		const ParamEntry &cl = cc>>"list";
		for (int i=0; i<cl.GetSize(); i++)
		{
			RStringB addon = cl[i];
			if (!strcmpi(name,addon)) return true;
		}
	}
	return false;
}

//! parse addon configs in correct order
/*!
\patch 1.82 Date 8/16/2002 by Ondra
- New: Addon dependencies can be declared using requiredAddons[] section of CfgPatches.
*/

void ParseAddonsConfigs()
{
	AutoArray<AddonDependency> dependencies;
	// recommended order to avoid compatibility problems with addons
	// that have no dependency list
	// 1) offical addons with no dependecies
	// 2) unoffical addons with no dependecies
	// 3) offical addons with dependecies
	// 4) unoffical addons with dependecies
	// 5) addons with no CfgPatches (malformed addons)
	// offical addons list can be found in "CfgAddons/PreloadAddons".
	// read dependecy information from ParamFiles
	for (int i=0; i<AddonConfigs.Size(); i++)
	{
		const ParamFile &config = *AddonConfigs.Get(i);
		AddonDependency &dep = dependencies.Append();
		dep.addon = &config;
		dep.preloaded = false;
		const ParamEntry *cfg = config.FindEntry("CfgPatches");
		if (cfg)
		{
			//! enumerate all addons in this config
			for (int i=0; i<cfg->GetEntryCount(); i++)
			{
				const ParamEntry &entry = cfg->GetEntry(i);
				if (!entry.IsClass()) continue;
				// check if we can find this addon in "preloaded" addons list
				dep.preloaded = IsPreloadedAddon(entry.GetName());
				const ParamEntry *required = entry.FindEntry("requiredAddons");
				if (!required) continue;
				// find corresponding addon
				for (int i=0; i<required->GetSize(); i++)
				{
					RStringB requiredName= (*required)[i];
					const ParamFile *file = FindAddonConfig(requiredName);
					if (!file)
					{
						//RString message = LocalizeString(IDS_MSG_ADDON_MISSING) + requiredName;
						RString message = "Addon '%s' requires addon '%s'";
						WarningMessage
						(
							message,
							(const char *)GetAddonName(config),
							(const char *)requiredName
						);
						continue;
					}
					dep.dependsOn.Add(file);
				}
			}
		}
	}
	// build a tree from the dependency

	// sort addons by dependencies
	// we can always load addon that has already resolved all dependencies
	// "loaded" addon is moved to resolved
	AutoArray<AddonDependency> resolved;
	while(dependencies.Size()>0)
	{
		// then find any preloaded addons with resolved dependecies
		// find any other addons with resolved dependecies
		bool someResolved = false;
		for (int preloaded=(int)true; preloaded>=(int)false; preloaded--)
		{
			for (int i=0; i<dependencies.Size(); i++)
			{
				const AddonDependency &dep = dependencies[i];
				if ((int)dep.preloaded!=preloaded) continue;
				if (dep.dependsOn.Size()>0) continue;
				resolved.Add(dep);
				// removed this addon from all dependecies lists
				for (int j=0; j<dependencies.Size(); j++)
				{
					dependencies[j].Resolved(dep);
				}
				dependencies.Delete(i--);
				someResolved = true;
			}
		}
		if (!someResolved)
		{
			// print some addon name
			RStringB addonName = GetAddonName(*dependencies[0].addon);
			ErrorMessage("Circular addon dependency in '%s'",(const char *)addonName);
			break;
		}
	}

	// parse all resolved addon configs
	for (int i=0; i<resolved.Size(); i++)
	{
		Log
		(
			"Parsing addon config '%s' %s",
			(const char *)GetAddonName(*resolved[i].addon),
			resolved[i].preloaded ? "(preloaded)" : ""
		);
		Pars.Update(*resolved[i].addon);
	}

	// ignore all unresolved addon configs - circular dependencies
	/*
	for (int i=0; i<dependencies.Size(); i++)
	{
		Pars.Update(*dependencies[i].addon);
	}
	*/
	Pars.SetFile(&Pars);
}

#endif

#if _VBS1
static bool VBS = true;
#else
static bool VBS = false;
#endif

#if _ENABLE_VBS
bool IsVBS() {return VBS;}
#else
bool IsVBS() {return false;}
#endif

static const char *ProductList[]=
{
	"OFP: Cold War Crisis",
	#if _ENABLE_DATADISC
		"OFP: Resistance",
	#endif
	NULL
};

#if _ENABLE_VBS
static const char *ProductListVBS[]=
{
	"VBS",
	#if _ENABLE_CHEATS
	"OFP: Cold War Crisis",
	"OFP: Resistance",
	#endif
	NULL
};
#endif

static bool StringInList(const char *str, const char **list)
{
	while (*list)
	{
		if (!strcmpi(*list,str)) return true;
		list++;
	}
	return false;
}

struct CheckAddonContext: public BankContextBase
{
	const char **productList;
#if _ENABLE_VBS
	const char **productListVBS;
	bool encryptionRequired;
#endif
};


/*!
\patch_internal 1.97 Date 04/01/2004 by Jirka
- Changed: unencrypted addons enabled for VBS1
*/

static bool CheckProductCallback(QFBank *bank, BankContextBase *context)
{
	CheckAddonContext *cpc = static_cast<CheckAddonContext *>(context);
	RString product = bank->GetProperty("product");
	RString encryption = bank->GetProperty("encryption");
	#if _ENABLE_VBS
		if (!IsVBS())
		{
			if (encryption.GetLength()>0)
			{
				return false;
			}
		}
		#if _VBS1
			if (cpc->encryptionRequired)
			{
				// CHANGED:
				// if (stricmp(encryption, "VBS1") != 0) return false;
				if (encryption.GetLength() > 0 && stricmp(encryption, "VBS1") != 0) return false;
			}
		#endif
	#else
		if (encryption.GetLength()>0)
		{
			return false;
		}
	#endif
	RString formatVersion = bank->GetProperty("pboVersion");
	if (formatVersion.GetLength()>0)
	{
		return false;
	}

	if (product.GetLength()>0)
	{
#if _ENABLE_VBS
		if (IsVBS())
		{
			if(!StringInList(product,cpc->productListVBS))
			{
				return false;
			}
		}
		else
#endif
		{
			if(!StringInList(product,cpc->productList))
			{
				return false;
			}
		}
	}
	return true;
}

//! Find all banks and adds them to global array of banks
/*!
	\patch 1.01 Date 06/12/2001 by Jirka
	- Added: AddOns support
	\patch_internal 1.01 Date 06/12/2001 by Jirka
	- added parameter parseConfig (if true, config.cpp or config.bin is searched on root of bank and merged with global config)
	\patch_internal 1.26 Date 10/03/2001 by Ondra
	- Fixed: banks were opened even when already existed (Data, Data3D in HWTL mode)
*/
static void LoadBanks(const char *path, const char *fullPath, bool emptyPrefix, bool parseConfig = false)
{
	FindArrayRStringCI bankNames;
	FindBank find;
	if (find.First(fullPath))
	{
		do
		{
			char prefix[256];
			strcpy(prefix, find.GetName());
			strlwr(prefix); // note: Win32 - case insensitive
			char *ext = strrchr(prefix, '.');
			Assert(ext);
			*ext = 0;
			bankNames.AddUnique(prefix);
		}
		while (find.Next());
		find.Close();
	}
	RString bankPrefix = RString(path) + RString("\\");
	// open each bank
	for (int i=0; i<bankNames.Size(); i++)
	{
		const RString &bName = bankNames[i];
		RString prefix;
		if (emptyPrefix) prefix = bName;
		else prefix = bankPrefix+bName;
#if _CZECH
		if (stricmp(prefix, "fonts.cz") == 0) prefix = "fonts";
#elif _RUSSIAN
		if (stricmp(prefix, "fonts.russian") == 0) prefix = "fonts";
#endif
		RString prefixPath = prefix+RString("\\");
		if (QIFStreamB::AutoBank(prefixPath))
		{
			// if bank already exists, do not open it
			continue;
		}

		Ref<CheckAddonContext> cpc = new CheckAddonContext;
		cpc->productList = ProductList;
#if _ENABLE_VBS
		cpc->productListVBS = ProductListVBS;
		cpc->encryptionRequired = parseConfig;
#endif
		OpenCallback addonCallback = (OpenCallback)NULL;
#if _ENABLE_ADDONS
		if (parseConfig)
		{
			addonCallback = ParseAddonConfigCallback;
		}
#endif

		RString bankPath = RString(fullPath) + RString("\\");
		GFileBanks.Load
		(
			bankPath,bankPrefix,bName,emptyPrefix,
			CheckProductCallback,addonCallback,cpc
		);
	}
}

/*!
\patch_internal 1.53 Date 4/25/2002 by Jirka
- Added: mods can use also different data banks
*/
struct LoadBanksContext
{
	RString path;
	bool emptyPrefix;
	bool parseConfig;
};

static bool LoadBanksCallback(RString dir, void *context)
{
	LoadBanksContext *ctx = reinterpret_cast<LoadBanksContext *>(context);

	if (dir.GetLength() == 0) dir = ctx->path;
	else dir = dir + RString("\\") + ctx->path;

	LoadBanks(ctx->path, dir, ctx->emptyPrefix, ctx->parseConfig);
	return false;
}

static void LoadBanksEx(const char *path, bool emptyPrefix, bool parseConfig = false)
{
#ifdef _XBOX
	LoadBanks(path, path, emptyPrefix, parseConfig);
#else
	LoadBanksContext ctx;
	ctx.path = path;
	ctx.emptyPrefix = emptyPrefix;
	ctx.parseConfig = parseConfig;
	EnumModDirectories(LoadBanksCallback, &ctx);
#endif
}

/*!
	\patch_internal 1.01 Date 06/12/2001 by Jirka - added AddOns support
	- directory AddOns is searched for banks
*/
void Globals::Init()
{
	//Test();

	if( GUseFileBanks )
	{
		GFileBanks.Clear();
		// first open HW specific data banks
		if (GFileBankPrefix.GetLength()>0)
		{
			LoadBanksEx(RString("dta\\")+GFileBankPrefix,true);
#if _ENABLE_ADDONS
			LoadBanksEx(RString("addons\\") + GFileBankPrefix, true, true);
#endif
		}
		// then generic
		LoadBanksEx("dta",true);
#if _ENABLE_ADDONS
		LoadBanksEx("addons", true, true);
#endif
		LoadBanksEx("Campaigns",false);
#if _ENABLE_ADDONS
		ParseAddonsConfigs();
		// addon config will be no longer required, as they are already parsed in
		AddonConfigs.Clear();
		// maintain default addon list
		AddonToPrefix.ForEach(MarkAddonLockable);
#endif
	}

	remove("clipboard.txt");

	int fileMemory=Glob.config.fileHeapSize*(1024*1024);
	if( fileMemory<1024*1024 ) fileMemory=1024*1024;

	// single thread file server (using Overlapped IO on WinNT platforms)
	GFileServer=new FileServerST(fileMemory);

	GFileServer->Start();

	// start with pesimistic tresholds
	drawTreshold=2.0*2.0;
	shadowTreshold=reflectTreshold=4*4;

	time=Time(0); //-1e8;
	uiTime=UITime(0);

	newGame=false;
	exit=false;
	demo=false;

	strcpy(header.filename, "Game001");
#if _FORCE_DEMO_ISLAND
	RString worldInit = Pars>>"CfgWorlds">>"demoWorld";
#else
	RString worldInit = Pars>>"CfgWorlds">>"initWorld";
#endif
	strcpy(header.worldname, worldInit);

	// get player name (if not passed as argument)
	if (Glob.header.playerName.GetLength() == 0)
	{
		#if defined _WIN32 && !defined _XBOX
			HKEY key;
			BYTE buf[256];
			DWORD bufSize = sizeof(buf);
			if
			(
				::RegOpenKeyEx(HKEY_CURRENT_USER, ConfigApp, 0, KEY_READ, &key) != ERROR_SUCCESS ||
				::RegQueryValueEx(key, "Player Name", NULL, NULL, buf, &bufSize) !=  ERROR_SUCCESS ||
				bufSize <= 1
			)
			{
				if (!::GetUserName((char *)buf, &bufSize) || bufSize <= 0)
					strcpy((char *)buf, "Player");
				header.playerName = (const char *)buf;
			}
			else
			{
				::RegCloseKey(key);
				header.playerName = (const char *)buf;
			}
		#else
			Glob.header.playerName = "Player";
		#endif
	}

	header.playerFace = "Default";
	header.playerGlasses = "None";
	header.playerSpeaker = (Pars >> "CfgVoice" >> "voices")[0];
	header.playerPitch = 1.0;
	RString GetUserParams();
	RString filename = GetUserParams();
	ParamFile cfg;
	cfg.Parse(filename);
	const ParamEntry *identity = cfg.FindEntry("Identity");
	if (identity)
	{
		header.playerFace = (*identity) >> "face";
		if (identity->FindEntry("glasses"))
			header.playerGlasses = (*identity) >> "glasses";
		header.playerSpeaker = (*identity) >> "speaker";
		header.playerPitch = (*identity) >> "pitch";
	}

	header.playerSide = TWest;
	config.easyMode = false;
#if _ENABLE_CHEATS
	config.super = false;
#endif

	if (NoBlood())
	{
		config.blood = false;
	}
	else
	{
		config.blood = GetLangID() != Korean;

		const ParamEntry *entry = cfg.FindEntry("blood");
		if (entry) config.blood = *entry;
	}

	GInput.LoadKeys();
	config.LoadDifficulties();

//	if (!GNetworkManager) GNetworkManager = new CPoseidonNetworkManager();
	ParamFile userCfg;
	userCfg.Parse(GetUserParams());

}

bool Config::IsEnabled(DifficultyType type)
{
	const MissionHeader *header = GetNetworkManager().GetMissionHeader();
	if (header) return header->difficulty[type]; // multiplayer
	else if (easyMode) return cadetDifficulty[type];
	else return veteranDifficulty[type];
}


RString GetUserParams();

void Config::LoadDifficulties()
{
	InitDifficulties();

	ParamFile userCfg;
	userCfg.Parse(GetUserParams());
	for (int i=0; i<DTN; i++)
	{
		RString name = RString("diff") + RString(diffDesc[i].name);
		const ParamEntry *cfg = userCfg.FindEntry(name);
		if (cfg)
		{
			cadetDifficulty[i] = (*cfg)[0];
			if (diffDesc[i].enabledInVeteran)
				veteranDifficulty[i] = (*cfg)[1];
		}
	}

	const ParamEntry *entry = userCfg.FindEntry("showTitles");
	if (entry) showTitles = *entry;

#ifdef _WIN32
	entry = userCfg.FindEntry("showRadio");
	if (entry) GChatList.Enable(*entry);
#endif
}

void Config::SaveDifficulties()
{
	ParamFile userCfg;
	userCfg.Parse(GetUserParams());
	for (int i=0; i<DTN; i++)
	{
		RString name = RString("diff") + RString(diffDesc[i].name);
		ParamEntry *entry = userCfg.AddArray(name);
		entry->Clear();
		entry->AddValue(cadetDifficulty[i]);
		entry->AddValue(veteranDifficulty[i]);
	}
	userCfg.Add("showTitles", showTitles);
#ifdef _WIN32
	userCfg.Add("showRadio", GChatList.Enabled());
#endif
	userCfg.Save(GetUserParams());
}

static int CCALL PrintFileF( HANDLE file, const char* format, ...)
{
    char szBuff[1024];
    int retValue;

#ifdef _WIN32

    DWORD cbWritten;

    va_list argptr;      
    va_start( argptr, format );

    retValue = vsnprintf( szBuff, sizeof(szBuff), format, argptr );
    if ( retValue < 0 )
        retValue = 1023;
    va_end( argptr );

    WriteFile( file, szBuff, retValue * sizeof(char), &cbWritten, 0 );
    
#else
    
    va_list argptr;      
    va_start( argptr, format );

    retValue = vsnprintf( szBuff, sizeof(szBuff), format, argptr );
    if ( retValue < 0 )
        retValue = 1023;

    va_end( argptr );

    write( file, szBuff, retValue );

#endif

    return retValue;
}

bool PrintConfigInfo( HANDLE file )
{
	// print some relevant information about SW/HW configuration
	// ask graphical engine do give information
	if (!GEngine)
	{
		return false;
	}
	PrintFileF(file,"graphics:  %s\r\n",(const char *)GEngine->GetDebugName());
	PrintFileF
	(
		file,"resolution:  %dx%dx%d%s\r\n",
		GEngine->Width(),GEngine->Height(),GEngine->PixelSize(),
		GEngine->IsWBuffer() ? ", w-buffer" : ""
	);

#if _ENABLE_ADDONS
	// print addon list
	// AddonInfoMap AddonToPrefix;
	PrintFileF(file,"Addons:\r\n");
	int lineSize = 0;
	for (AddonInfoMap::Iterator iterator(AddonToPrefix); iterator; ++iterator)
	{
		const AddonInfo &info = *iterator;
		RString oneItem = info.GetName()+RString(" in ")+RString(info.GetPrefix());
		if (lineSize+oneItem.GetLength()>80 && lineSize>0)
		{
			PrintFileF(file,"\r\n");
			lineSize = 0;
		}
		if (lineSize>0)
		{
			PrintFileF(file,", %s",(const char *)oneItem);
		}
		else
		{
			PrintFileF(file,"  %s",(const char *)oneItem);
		}
		lineSize += oneItem.GetLength();
	}
	if (lineSize>0)
	{
		PrintFileF(file,"\r\n");
	}
#endif
	if (ModPath.GetLength()>0)
	{
		PrintFileF(file,"Mods: %s\r\n",(const char *)ModPath);
	}

	return true;
}

void PrintMissionInfo( HANDLE file )
{
	if (strcmp(Glob.header.filenameReal,Glob.header.filename))
	{
		PrintFileF(file,"file:     %s (%s)\r\n",(const char *)Glob.header.filenameReal,Glob.header.filename);
	}
	else
	{
		PrintFileF(file,"file:     %s\r\n",(const char *)Glob.header.filenameReal);
	}
	PrintFileF(file,"world:    %s\r\n",Glob.header.worldname);
	if (CurrentCampaign.GetLength()>0) PrintFileF(file,"campaign: %s\r\n",(const char *)CurrentCampaign);
	if (CurrentBattle.GetLength()>0) PrintFileF(file,"battle:   %s\r\n",(const char *)CurrentBattle);
	if (CurrentMission.GetLength()>0) PrintFileF(file,"mission:  %s\r\n",(const char *)CurrentMission);
}

void DestroyMsgFormats();

void Globals::Clear()
{
	// clear variables that are not part of Global structure
	QIFStreamB::ClearBanks();
	GFileServer.Free();
	ClearShapes();
	Pars.Clear();
	ExtParsCampaign.Clear();
	ExtParsMission.Clear();
	Res.Clear();
	DestroyMsgFormats();
}

Globals Glob;

/*!
\patch 1.36 Date 12/12/2001 by Ondra
- New: -cfg command line argument to enable selecting Flashpoint.cfg.
*/

#if _VBS1
RString FlashpointCfg="VBS1.cfg";
RString FlashpointRpt="VBS1.rpt";
RString FlashpointPar="VBS1.par";
#elif _GALATEA
RString FlashpointCfg="Galatea.cfg";
RString FlashpointRpt="Galatea.rpt";
RString FlashpointPar="Galatea.par";
#elif _COLD_WAR_ASSAULT
RString FlashpointCfg="ColdWarAssault.cfg";
RString FlashpointRpt="ColdWarAssault.rpt";
RString FlashpointPar="ColdWarAssault.par";
#else
RString FlashpointCfg="Flashpoint.cfg";
RString FlashpointRpt="Flashpoint.rpt";
RString FlashpointPar="Flashpoint.par";
#endif

bool Benchmark=false;
bool AutoTest=false;

// memory management import


void InitMemory( int size );
void MemoryFast();
void MemoryGood();
bool MemoryCheck();


/**************************************************************************
	DirectDraw Globals
**************************************************************************/

const char *ErrorString; // MessageBox does not work when in exclusive mode

#ifdef _WIN32

// window only variables
RECT clientRect;

// main globals

static bool CanRender; // everything is initialized
bool ForceRender; // draw frame as soon as possible

extern bool LandEditor;
extern bool NoTextures;


extern bool ObjViewer;

static bool NoSplash=false;
static bool DoPause=false;
static bool DoRestore=false;
static bool DoCreateServer=false;

#else

static bool CanRender; // everything is initialized
extern bool LandEditor;
static bool NoSplash=false;
static bool DoCreateServer=true;

#endif

/*!
\patch_internal 1.45 Date 2/20/2002 by Jirka
- Added: argument vbs in designer version
*/


/*!
\patch 1.11 Date 07/30/2001 by Jirka
- Added: Dedicated server support
*/
/*!
\patch 1.22 Date 09/10/2001 by Ondra
- Added: Separate exe for dedicated server.
*/
//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
	#if _DISABLE_GUI
		const bool DoCreateDedicatedServer=true;
		bool IsDedicatedServer() {return true;}
	#else
		static bool DoCreateDedicatedServer=false;
		bool IsDedicatedServer() {return DoCreateDedicatedServer;}
	#endif
	static RString ServerConfig;
	RString GetServerConfig() {return ServerConfig;}
#endif
//}
RString ClientIP;
/*!
\patch 1.11 Date 07/31/2001 by Jirka
- Added: Additional program parameters
- port=...
- password=...
\patch 1.57 Date 5/16/2002 by Jirka
- Fixed: Default network port changed (2234 -> 2302) due to compatibility with DirectPlay 9
*/

extern const int DefaultNetworkPort = 2302;
static int NetworkPort = DefaultNetworkPort;
int GetNetworkPort() {return NetworkPort;}
static RString NetworkPassword;
RString GetNetworkPassword() {return NetworkPassword;}
static RString RankingLog;
RString GetRankingLog() {return RankingLog;}

#if _ENABLE_DEDICATED_SERVER

/// PidFilename!="" -> exclusive network port usage
static RString PidFileName;
RString GetPidFileName() {return PidFileName;}

/// This OFP instance has created the <pid_file>.
bool MyPidFile = false;

#endif

// filename argument
//char LoadFile[512]="Worlds\\test.wrp";
char LoadFile[512];
//extern const char DefLoadFile[]="Worlds\\test.wrp";

/**************************************************************************
	DirectInput Globals
**************************************************************************/

#if defined _XBOX || _DISABLE_GUI
	#define ENABLE_KEYMOUSE 0
#else
	#define ENABLE_KEYMOUSE 1
#endif

#if _DISABLE_GUI
    #define ENABLE_JOYSTICK 0
#else
    #define ENABLE_JOYSTICK 1
#endif

#if ENABLE_KEYMOUSE
	static IDirectInput8 *dInput;
	static IDirectInputDevice8 *Mouse;        // Use for rotation
	static IDirectInputDevice8 *Keyb;        // Use for rotation
#endif

static bool fMouseAcquired = false; // Acquired for rot'n

/*!
\patch 1.95 Date 1/15/2004 by Ondra
- Improved: Several anti-cheat measures implemented.
*/

#if _VERIFY_KEY
	#include "serial.hpp"
	CDKey GCDKey;
#endif

// this buffer will contain all keyboard changes during one frame
const int KeyboardBufferSize=256;
const int MouseBufferSize=256;

#if ENABLE_KEYMOUSE
#ifndef _XBOX
static DIDEVICEOBJECTDATA KeyboardBuffer[KeyboardBufferSize];
static DIDEVICEOBJECTDATA MouseBuffer[MouseBufferSize];
#endif
#endif

static bool SkipKeys=false;


/**************************************************************************
	CreateMouse
**************************************************************************/

#if ENABLE_KEYMOUSE

bool CreateMouse( HWND hwnd, GUID &guid, IDirectInputDevice8 *& lpdiMouse, DWORD dwAccess)
{
	HRESULT err;
	
	err = dInput->CreateDevice(guid, &lpdiMouse, NULL);
	if(err != DI_OK) goto fail;

	// Tell DirectInput that we want to receive data in mouse format
	err = lpdiMouse->SetDataFormat(&c_dfDIMouse2);
	if(err != DI_OK) goto fail;
	
	// set desired access mode
	err = lpdiMouse->SetCooperativeLevel(hwnd, dwAccess);
	if(err != DI_OK) goto fail;

	// prepare to buffer event data
	DIPROPDWORD prop;
	prop.diph.dwSize=sizeof(prop);
	prop.diph.dwHeaderSize=sizeof(prop.diph);
	prop.diph.dwObj=0;
	prop.diph.dwHow=DIPH_DEVICE;
	prop.dwData=MouseBufferSize;
	lpdiMouse->SetProperty(DIPROP_BUFFERSIZE,&prop.diph);

	return true;

	fail:
	if (lpdiMouse)      lpdiMouse->Release(),       lpdiMouse = 0;
	ErrorMessage("Unable to Create DirectInput Mouse Device");
	
	return false;
}

bool CreateKeyboard( HWND hwnd, GUID &guid, IDirectInputDevice8 *& lpdiKeyb, DWORD dwAccess)
{
	HRESULT err;
	
	err = dInput->CreateDevice(guid, &lpdiKeyb, NULL);
	if(err != DI_OK) goto fail;

	// Tell DirectInput that we want to receive data in mouse format
	err = lpdiKeyb->SetDataFormat(&c_dfDIKeyboard);
	if(err != DI_OK) goto fail;
	
	// set desired access mode
	err = lpdiKeyb->SetCooperativeLevel(hwnd, dwAccess);
	if(err != DI_OK) goto fail;

	// prepare keyboard to buffer event data
	DIPROPDWORD prop;
	prop.diph.dwSize=sizeof(prop);
	prop.diph.dwHeaderSize=sizeof(prop.diph);
	prop.diph.dwObj=0;
	prop.diph.dwHow=DIPH_DEVICE;
	prop.dwData=KeyboardBufferSize;
	lpdiKeyb->SetProperty(DIPROP_BUFFERSIZE,&prop.diph);

	return true;

	fail:
	if (lpdiKeyb) lpdiKeyb->Release(),lpdiKeyb=NULL;
	ErrorMessage("Unable to Create DirectInput Keyboard Device");
	
	return false;
}

#endif // !_DISABLE_GUI

RString GetKeyNameDI(int dik)
{
	#if ENABLE_KEYMOUSE
	if (dik>=INPUT_DEVICE_MOUSE) return RString();
	if (dik>=INPUT_DEVICE_STICK) return RString();
	if (dik>=INPUT_DEVICE_STICK_AXIS) return RString();
	if (dik>=INPUT_DEVICE_STICK_POV) return RString();
	if (!Keyb) return RString();
	DIPROPSTRING string;
	string.diph.dwSize = sizeof(string);
	string.diph.dwHeaderSize = sizeof(string.diph);
	string.diph.dwObj        = dik;
	string.diph.dwHow        = DIPH_BYOFFSET; 
 	HRESULT hr =  Keyb->GetProperty(DIPROP_KEYNAME,&string.diph);
	if (hr!=DI_OK)
	{
		return RString();
	}

	char buffer[1024];
	WideCharToMultiByte(CP_ACP, 0, string.wsz, -1, buffer, sizeof(buffer), NULL, NULL);
	return buffer;
	#else
		return RString();
	#endif
}

#if ENABLE_JOYSTICK
extern SRef<Joystick> JoystickDev;
#endif

/*!
\patch 1.23 Date 09/11/2001 by Ondra
- Fixed: iFeel effects did not work.
*/

void MouseAcquire()
{
	#if ENABLE_KEYMOUSE
	if( !Mouse ) return;

	HRESULT ret=Mouse->Acquire();
	extern void DIError( const char *op, HRESULT err );
	if( ret!=S_FALSE && ret!=DI_OK ) {} //DIError("Mouse Acquire error",ret);
	else
	{
		fMouseAcquired = true;
	}
	#endif
}

#if ENABLE_KEYMOUSE
#pragma comment(lib,"dinput8")
#endif

/**************************************************************************
	InitDInput
**************************************************************************/
bool InitDInput( HWND hwnd )
{
	#if _XBOX
		JoystickDev=new Joystick(true,true,NULL,hwnd);
	#endif
	#if ENABLE_KEYMOUSE

	#if _ENABLE_DEDICATED_SERVER
		if (DoCreateDedicatedServer)
		{
			return true;
		}
	#endif
	HRESULT         err;
	GUID     mGuid = GUID_SysMouse;
	GUID     kGuid = GUID_SysKeyboard;

	//err = DirectInputCreate(hInstApp, DIRECTINPUT_VERSION, &dInput, NULL);
	err = DirectInput8Create
	(
		hInstApp, DIRECTINPUT_VERSION, IID_IDirectInput8 ,(void **)&dInput, NULL
	);

	if(err != DI_OK)
	{
		goto fail;
	}

	// Create a mouse for rotation.  Rotation is done exclusively.
	if( !UseWindow && MouseEx )
	{
		if (!CreateMouse(hwnd,mGuid, Mouse, DISCL_EXCLUSIVE | DISCL_FOREGROUND))
		{
			goto fail;
		}
		MouseAcquire();
	}
	else
	{
		if (!CreateMouse(hwnd,mGuid, Mouse, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND))
		{
			if (!CreateMouse(hwnd,mGuid, Mouse, DISCL_EXCLUSIVE | DISCL_FOREGROUND))
			{
				goto fail;
			}
		}
	}


	if
	(
		!CreateKeyboard
		(
			hwnd,kGuid, Keyb, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND| DISCL_NOWINKEY
		)
	)
	{
		goto fail;
	}

	if( !LandEditor )
	{
		JoystickDev=new Joystick(true,true,NULL,hwnd,dInput);
	}

	// if we get here, all DirectInput objects were created ok
	return true;

	fail:
	ErrorMessage("Cannot initialize DirectInput. (DirectX %d required.)",DIRECTINPUT_VERSION>>8);
	if (Mouse) Mouse ->Release(), Mouse  = NULL;
	if (Keyb) Keyb ->Release(), Keyb  = NULL;
	if (dInput) dInput->Release(), dInput     = NULL;
	return false;
	#else
	return true;
	#endif

}

static void WorldDestroy()
{
	if( GLOB_ENGINE ) GLOB_ENGINE->StopAll();
	if( GWorld ) delete GWorld,GWorld=NULL;
	GPreloadedTextures.Clear();
}

static bool AppWinCreate( HWND hwnd )
{
#ifdef _WIN32
	if( !InitDInput(hwnd) )
	{
		return false;
	}
#endif
	//StartTimers(hwnd);
	if( GWorld ) GWorld->UnloadSounds();
	if( GSoundScene ) delete GSoundScene,GSoundScene=NULL;
	if( GSoundsys ) delete GSoundsys,GSoundsys=NULL;
	//{ NEW For dedicated server or with nosound argument create dummy engine
#ifdef _WIN32
	GSoundsys=new SoundSystem8
	(
		hwndApp,
#if _ENABLE_DEDICATED_SERVER
		DoCreateDedicatedServer ||
#endif
		NoSound
	);
#else
	GSoundsys = new SoundSystem8;
#endif
	//}
	GSoundScene=new SoundScene;
	return true;
}

#if _VERIFY_KEY | _VERIFY_KEY_EXT

// interface to registry key
bool GetKeyFromRegistry(const char *path, unsigned char *cdkey)
{
	memset(cdkey, 0, KEY_BYTES);
	HKEY key;
	if
	(
		!::RegOpenKeyEx
		(
			HKEY_LOCAL_MACHINE, path,
			0, KEY_READ, &key
		) == ERROR_SUCCESS
	) return false;

	DWORD size = KEY_BYTES;
	DWORD type = REG_BINARY;
	::RegQueryValueEx(key, "KEY", 0, &type, cdkey, &size);
	::RegCloseKey(key);
	return true;
}

#endif

#if _VERIFY_KEY
// NEW CD_KEY VERIFY
/*!
\patch_internal 1.01 6/20/2001 Changed inline to __forceinline
to make cracking a little bit harder.
*/
__forceinline bool VerifyKey()
{
	// no Win32 function may be called here
	// as it would make protection impossible

	unsigned char cdkey[KEY_BYTES];
	if (!GetKeyFromRegistry(ConfigApp, cdkey))
	{
		return false;
	}

#if _VBS1
	static const unsigned char publicKey[] =
	{
		0x11, 0x00, 0x00, 0x00,
		0xF9, 0x54, 0x9E, 0x33, 0x73, 0xCD, 0x0F, 0x5A, 0x15, 0x85, 0xE2, 0xAC, 0x32, 0x80, 0xA1
	};
	const char *check = "VBSystem 1";
#else
	static const unsigned char publicKey[] =
	{
		0x13, 0x00, 0x00, 0x00,
		0xF7, 0xB0, 0x64, 0xD7, 0xDB, 0x87, 0x52, 0x27, 0x57, 0x91, 0xB1, 0x9E, 0x0E, 0x81, 0x88
	};
	const char *check = "Flashpoint";
#endif

	GCDKey.Init(cdkey, publicKey);

	return GCDKey.Check(5, check);
}

#endif

#if _VERIFY_KEY_EXT
__forceinline bool VerifyKeyExt()
{
	// no Win32 function may be called here
	// as it would make protection impossible

	unsigned char cdkey[KEY_BYTES];
	if (!GetKeyFromRegistry(ConfigAppExt, cdkey))
	{
		return false;
	}

	static const unsigned char publicKey[] =
	{
		0x13, 0x00, 0x00, 0x00,
		0xF7, 0xB0, 0x64, 0xD7, 0xDB, 0x87, 0x52, 0x27, 0x57, 0x91, 0xB1, 0x9E, 0x0E, 0x81, 0x88
	};
	CDKey keyExt;
	keyExt.Init(cdkey, publicKey);

	_int64 value = keyExt.GetValue(0, 5);
	return keyExt.Check(5, "Resistance") && value >= _VERIFY_KEY_BEGIN && value < _VERIFY_KEY_END;
}
#endif

const int DEF_W=640,DEF_H=480;

extern int MaxMsgSend;
extern int DSMaxMsgSend;
extern int MaxSizeGuaranteed;
extern int MaxSizeNonguaranteed;
extern int MinBandwidth;
extern int DSMinBandwidth;
extern int MaxBandwidth;
extern float MinErrorToSend;
extern float ThrottleGuaranteed;
extern int MaxCustomFileSize;

/*!
\patch_internal 1.24 Date 6/26/2001 by Ondra.
- Optimized: dedicated server memory usage.
*/

static bool ReadRegistry()
{
	// caution - new operator cannot be used in this functions
	// InitMemory not called yet

	#if 1
		Glob.config.tacticalZ=900;
		Glob.config.horizontZ=900;
		Glob.config.objectsZ=600;
		Glob.config.shadowsZ=250;
		Glob.config.radarZ=2500;
	#else
		// check large visibility
		Glob.config.horizontZ=1600;
		Glob.config.objectsZ=1000;
		Glob.config.shadowsZ=300;
		Glob.config.tacticalZ=1600;
		Glob.config.radarZ=6000;
	#endif

	Glob.config.maxObjects=48;
	//Glob.config.maxShadows=32;
	//Glob.config.maxReflections=32;
	
	Glob.config.heapSize=6;
	Glob.config.fileHeapSize=6;
	Glob.config.maxCockText=256;
	Glob.config.maxLandText=128;
	Glob.config.maxObjText=128;
	Glob.config.maxAnimText=32;
	Glob.config.autoDropText=2;
	Glob.config.benchmark = 600;

	Glob.config.lodCoef=10;
	Glob.config.objectLODLimit=0;
	Glob.config.shadowLODLimit=0;
	//Glob.config.reflectLODLimit=0;

	//Glob.config.reflections=0;
	Glob.config.lights=0;
	//Glob.config.background=true;
	//Glob.config.clouds=true;
	//Glob.config.inGameMusic=true;
	//Glob.config.softOnly=false;

	Glob.config.maxLights=4;
	Glob.config.maxSounds=6;

	//Glob.config.threadTextures=false;

	Glob.dropDown=0.0; // default to full quality
	Glob.fullDropDown=0.0; // default to full quality
	Glob.fullDropDownChange=0.0; // default to full quality

	// default is 16b - better memory usage/nearly equal performance

	Glob.config.wantBpp=16;
	Glob.config.wantW=DEF_W;
	Glob.config.wantH=DEF_H;


	ParamFile Cfg;
	Cfg.Parse(FlashpointCfg);
	
	int v;
	float fv;
	fv = Cfg>>"3D_Performance";
	Glob.config.benchmark = fv;
	
	v=Cfg>>"Cockpit_Textures";
	if( v>0 ) Glob.config.maxCockText=v;
	v=Cfg>>"Landscape_Textures";
	if( v>0 ) Glob.config.maxLandText=v;
	v=Cfg>>"Object_Textures";
	if( v>0 ) Glob.config.maxObjText=v;
	v=Cfg>>"Animated_Textures";
	if( v>0 ) Glob.config.maxAnimText=v;
	v=Cfg>>"Textures_Drop_Down";
	if( v>0 )
	{
		int logV=0;
		while( v>1 ) logV++,v>>=1;
		Glob.config.autoDropText=logV;
	}
	fv=Cfg>>"LOD";
	if( fv>0 ) Glob.config.lodCoef=fv;

	fv=Cfg>>"Limit_LOD";
	if( fv>0 ) Glob.config.objectLODLimit=fv;
	fv=Cfg>>"Shadows_LOD";
	if( fv>0 ) Glob.config.shadowLODLimit=fv;
	//fv=Cfg>>"Reflections LOD";
	//if( fv>0 ) Glob.config.reflectLODLimit=fv;

	v=Cfg>>"Texture_Heap";
	if( v>0 ) Glob.config.heapSize=v;
	v=Cfg>>"File_Heap";
	if( v>0 ) Glob.config.fileHeapSize=v;

	//v=Cfg>>"Sky";
	//if( v>=0 ) Glob.config.background=( v!=0 );
	//v=Cfg>>"Reflections";
	//if( v>=0 ) Glob.config.reflections=( v!=0 );
	int lights=0;
	v=Cfg>>"Light_Explo";
	if( v>0 ) lights|=LIGHT_EXPLO;
	v=Cfg>>"Light_Missile";
	if( v>0 ) lights|=LIGHT_MISSILE;
	v=Cfg>>"Light_Static";
	if( v>0 ) lights|=LIGHT_STATIC;
	Glob.config.lights=lights;

	v=Cfg>>"MaxLights";
	if( v>=0 ) Glob.config.maxLights=v;
	//LogF("Max %d global lights",Glob.config.maxLights);
	
	RString engine = Cfg>>"HW_Type";
	if (!strcmpi(engine,"Direct3D"))
	{
		UseGlide=false;
	}
	else if (!strcmpi(engine,"Direct3D HW T&L"))
	{
		UseGlide=false;
		EnableHWTL=true;
	}
	if (Cfg.FindEntry("Adapter"))
	{
		v=Cfg>>"Adapter";
		D3DAdapter = v;
	}

	//v=Cfg>>"MaxShadows";
	//if( v>0 ) Glob.config.maxShadows=v;
	v=Cfg>>"MaxObjects";
	if( v>0 ) Glob.config.maxObjects=v;
	
	v=Cfg>>"Resolution_W";
	if( v>0 ) Glob.config.wantW=v;
	v=Cfg>>"Resolution_H";
	if( v>0 ) Glob.config.wantH=v;
	v=Cfg>>"Resolution_Bpp";
	if( v>0 ) Glob.config.wantBpp=v;

	#if _ENABLE_DEDICATED_SERVER
	if (DoCreateDedicatedServer)
	{
		// use minimal values of visual setting for dedicated server
		// this may conserve some memory
		UseGlide = false;
		EnableHWTL = false;
		Glob.config.objectLODLimit=1.0;
		Glob.config.shadowLODLimit=1.0;
	}
	#endif
	Glob.config.trackTimeToLive=60;
	Glob.config.invTrackTimeToLive=1/Glob.config.trackTimeToLive;

	const ParamEntry *entry = Cfg.FindEntry("MaxMsgSend");
	if (entry)
	{
		MaxMsgSend = *entry;
		DSMaxMsgSend = *entry;
	}
	entry = Cfg.FindEntry("MaxSizeGuaranteed");
	if (entry) MaxSizeGuaranteed = *entry;
	entry = Cfg.FindEntry("MaxSizeNonguaranteed");
	if (entry) MaxSizeNonguaranteed = *entry;
	entry = Cfg.FindEntry("MinBandwidth");
	if (entry)
	{
		MinBandwidth = *entry;
		DSMinBandwidth = *entry;
	}
	entry = Cfg.FindEntry("MaxBandwidth");
	if (entry) MaxBandwidth = *entry;

	entry = Cfg.FindEntry("MinErrorToSend");
	if (entry) MinErrorToSend = *entry;

	entry = Cfg.FindEntry("ThrottleGuaranteed");
	if (entry) ThrottleGuaranteed = *entry;

	entry = Cfg.FindEntry("MaxCustomFileSize");
	if (entry) MaxCustomFileSize = *entry;
	

	#if _ENABLE_DATADISC
		if (!Cfg.FindEntry("Product"))
		{
			if (!UseGlide)
			{
				Cfg.Add("Cockpit_Textures",2048);
				Cfg.Add("Landscape_Textures",2048);
				Cfg.Add("Object_Textures",2048);
				Cfg.Save(FlashpointCfg);
			}

			return false;
		}
	#endif

	return true;
}

#if !_SUPER_RELEASE && !defined _XBOX
#  include <Es/Framework/logflags.hpp>
#endif

/*!
\patch 1.01 Date 6/26/2001 by Ondra.
- Changed: Removed some command line arguments.
- Some other removed from Retail version.
\patch 1.31 Date 11/22/2001 by Jirka
- Added: reporting of MP score in logfile
\patch_internal 1.48 Date 3/13/2002 by Pepca
- Fixed: NetLog facilities are disabled while not using sockets
\patch_internal 1.85 Date 9/23/2002 by Jirka
- Added: Argument -autotest added into designers' version
\patch 1.92 8/8/2003 by Pepca
- Added: Command-line argument -pid=<pid-file> to avoid multiple server-start.
If this parameter is used, only one network port is examined..
\patch 1.93 Date 8/24/2003 by Ondra
- Fixed: New command line option -nomap was ignored sometimes.
*/

static void SingleArgument( const char *beg, const char *end )
{
	if( end==beg ) return;
	if( *beg=='-' || *beg=='/' )
	{
		beg++;
		// option
		static const char xStr[]="x=";
		static const char yStr[]="y=";
		static const char wStr[]="window";
		static const char noSplashStr[]="nosplash";
		static const char nomapStr[]="nomap";
		static const char benchmarkStr[]="benchmark";
		static const char logfiles[]="logfiles";
#ifdef NET_LOG_COMMAND_LINE
    static const char netlog[]="netlog";
#endif
		static const char host[]="host";
		static const char connect[]="connect=";
		static const char name[]="name=";
		static const char ranking[]="ranking=";
//		static const char flashpointConfig[]="cfg=";
//		static const char modPath[]="mod=";
//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
		static const char server[]="server";
		static const char serverConfig[]="config=";
    static const char pidFile[]="pid=";
#endif
//}
		static const char port[]="port=";
		static const char password[]="password=";
		static const char sockets[]="sockets";
		static const char dplay[]="dplay";
		#if !_SUPER_RELEASE
		static const char monStr[]="twomon";
		static const char noland[]="noland";
		static const char dx[]="dx";
		static const char nosound[]="nosound";
		static const char pIII[]="piii";
		static const char tl[]="tl";
		static const char notex[]="notex";
		static const char vbs[]="vbs";
		static const char autotest[]="autotest";
		#endif

		if( !strnicmp(beg,xStr,strlen(xStr)) )
		{
			Glob.config.wantW=atoi(beg+strlen(xStr));
		}
		else if( !strnicmp(beg,yStr,strlen(yStr)) )
		{
			Glob.config.wantH=atoi(beg+strlen(yStr));
		}
		else if( end-beg==(int)strlen(wStr) && !strnicmp(beg,wStr,end-beg) ) UseWindow=true;
		else if( end-beg==(int)strlen(benchmarkStr) && !strnicmp(beg,benchmarkStr,end-beg) ) Benchmark=true;
		// FIX
		#if !_SUPER_RELEASE
		else if( end-beg==(int)strlen(dx) && !strnicmp(beg,dx,end-beg) ) UseGlide=false;
		else if( !strnicmp(beg,pIII,strlen(pIII)) ) EnablePIII=true;
		else if( !strnicmp(beg,tl,strlen(tl)) ) EnableHWTL=true;
		else if( !strnicmp(beg,logfiles,strlen(logfiles)) ) GLogFileOps=true;
		else if( !strnicmp(beg,noland,strlen(noland)) ) NoLandscape=true;
		else if( !strnicmp(beg,nosound,strlen(nosound)) ) NoSound=true;
		else if( !strnicmp(beg,monStr,strlen(monStr)) ) NoGlideDeactivate=true;
#ifdef _WIN32
		else if( end-beg==(int)strlen(notex) && !strnicmp(beg,notex,end-beg) ) NoTextures=true;
#endif
		else if( !strnicmp(beg,vbs,strlen(vbs)) ) VBS=true;
		else if( !strnicmp(beg,autotest,strlen(autotest)) ) AutoTest=true;
		#endif
#ifdef NET_LOG_COMMAND_LINE
		else if( !strnicmp(beg,netlog,strlen(netlog)) ) netLogValid=true;
#endif
		else if( !strnicmp(beg,noSplashStr,strlen(noSplashStr)) ) NoSplash=true;
		else if( !strnicmp(beg,nomapStr,strlen(nomapStr)) ) EnableMemMap=false;
		else if( !strnicmp(beg,host,strlen(host)) )
		{
			DoCreateServer=true;
			#if _ENABLE_REPORT
			ResetLogFile = true;
			LogFilename = "host.log";
			#endif
		}
//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
	#if !_DISABLE_GUI
		else if( !strnicmp(beg,server,strlen(server)) )
		{
			DoCreateDedicatedServer=true;
			#if _ENABLE_REPORT
			ResetLogFile = true;
			LogFilename = "server.log";
			#endif
		}
	#endif
		else if( !strnicmp(beg,serverConfig,strlen(serverConfig)) )
		{
			const char *src = beg + strlen(serverConfig); 
			ServerConfig = RString(src,end-src);;
		}
#endif
//}
		else if( !strnicmp(beg,connect,strlen(connect)) )
		{
			const char *src = beg + strlen(connect); 
			ClientIP = RString(src,end-src);;
		}
		else if( !strnicmp(beg,port,strlen(port)) )
		{
			NetworkPort = atoi(beg + strlen(port));
		}
#if _ENABLE_DEDICATED_SERVER
		else if( !strnicmp(beg,pidFile,strlen(pidFile)) )
		{
			const char *src = beg + strlen(pidFile); 
			PidFileName = RString(src,end-src);;
		}
#endif
		else if( !strnicmp(beg,password,strlen(password)) )
		{
			const char *src = beg + strlen(password); 
			NetworkPassword = RString(src,end-src);;
		}
		else if( !strnicmp(beg,name,strlen(name)) )
		{
			const char *src = beg + strlen(name); 
			Glob.header.playerName = RString(src,end-src);
		}
		else if(!strnicmp(beg,ranking,strlen(ranking)))
		{
			const char *src = beg + strlen(ranking); 
			RankingLog = RString(src,end-src);;
		}
		else if(!strnicmp(beg, sockets, strlen(sockets)) )
    {
        UseSockets = true;
    }
		else if(!strnicmp(beg, dplay, strlen(dplay)) )
    {
        UseSockets = false;
    }
	}
	else
	{
		strncpy(LoadFile,beg,end-beg);
		LoadFile[end-beg]=0;
	}
}

static void SingleArgumentPass1( const char *beg, const char *end )
{
	if( end==beg ) return;
	if( *beg=='-' || *beg=='/' )
	{
		beg++;
		// option
		static const char flashpointConfig[]="cfg=";
		static const char modPath[]="mod=";

		if( !strnicmp(beg,flashpointConfig,strlen(flashpointConfig)) )
		{
			const char *src = beg + strlen(flashpointConfig); 
			FlashpointCfg = RString(src,end-src);
		}
		else if( !strnicmp(beg,modPath,strlen(modPath)) )
		{
			const char *src = beg + strlen(modPath);
			RString add(src, end-src);
			if (ModPath.GetLength() == 0) ModPath = add;
			else ModPath = ModPath + RString(";") + add;
		}
	}
}

extern float Height;

/*!
\patch_internal 1.27 Date 10/12/2001 by Ondra
- Fixed: FPS limitation for all non-graphics modes, not only for ded. server.
\patch 1.27 Date 10/12/2001 by Ondra
- Improved: MP: application behaviour when running on background (alt-tab)
is now more consistent.
\patch_internal 1.31 Date 11/23/2001 by Ondra
- Fixed: FPS limitation for no-graphics cheat
to make MP multiple client testing easier.
\patch_internal 1.33 Date 11/29/2001 by Ondra
- New: Cheat Alt-S to limit fps at 10.
This should help testing some bugs that do not occur with high fps.
*/
void RenderFrame( float deltaT, bool enableDraw )
{
	if( Glob.exit ) CloseRequest=true;
	if (!GWorld) return;

	int startTime = GlobalTickCount();
	// alive should come soon (if nothing extra happens)
	GDebugger.NextAliveExpected(10000);

	/*	
	if( !CanRender || appIconic && !LandEditor )
	{
		ForceRender=true;
		return;
	}
	*/
	GWorld->SetSimulationFocus(enableDraw);
	GWorld->Simulate(deltaT,enableDraw);
	
#ifdef _WIN32	
	ForceRender=false;
#endif
	//Assert( MemoryCheck() );

	#if 1
	// if not drawing, avoid having fps too high
	#if _ENABLE_CHEATS
		static int limitFpsCoef = 0;
		#define limitFps ( limitFpsCoef>0 ? 40/limitFpsCoef : 0 )
		if (GInput.GetCheat2ToDo(DIK_S))
		{
			limitFpsCoef++;
			if (limitFpsCoef>4) limitFpsCoef=0;
			GlobalShowMessage(500,"Limit FPS %d",limitFps);
		}
	#else
		const bool limitFps = false;
	#endif
	if (!enableDraw || GWorld->GetRenderingDisabled() || limitFps)
	{
		#if _ENABLE_CHEATS
			int maxFps = limitFpsCoef ? limitFps : 50;
		#else
			const int maxFps = 50;
		#endif
		const int minMsPerFrame = 1000 / maxFps;

		int durationMs = GlobalTickCount() - startTime;
		int sleepMillis = minMsPerFrame - durationMs;
		if (sleepMillis>0) Sleep(sleepMillis);
	}
	#endif
}

#ifdef _WIN32

static void ProcessMessages( bool noWait );

void ProcessMessagesNoWait() {ProcessMessages(true);}

#else

void ProcessMessagesNoWait()
{}

#endif

// Main Loop

static int MouseAccel( int raw, float timeDelta )
{
	// no acceleration in low framerates
	float aspeed = abs(raw)/timeDelta;
	if( timeDelta>0.2 ) return raw;
	/*
	if( abs(raw)>16 ) raw*=2;
	if( abs(raw)>4 ) raw*=2;
	*/
	if( aspeed>160 ) raw*=4;
	if( aspeed>40 ) raw*=2;
	return raw;
}

void OpenPerfCounters();
void ClosePerfCounters();


void ProcessMouse(DWORD timeDelta)	// used from outside too
{
#ifdef _WIN32
	GInput.mouseLToDo=false;
	GInput.mouseRToDo=false;
	GInput.mouseMToDo=false;

	for (int i=0; i<N_MOUSE_BUTTONS; i++)
	{
		GInput.mouseButtonsToDo[i] = false;
	}

	float moveX = 0;
	float moveY = 0;

	GInput.mouseX=0;
	GInput.mouseY=0;
	GInput.mouseZ=0;

	#if ENABLE_KEYMOUSE
	// mouse input - not used yet
	// every time you read input try to acquire mouse
	// TODO: remove mouseXToDo

	if( !UseWindow )
	{
		MouseAcquire();
	}

	if (!GWorld->IsUserInputEnabled()) return;

	DWORD nMouseData = MouseBufferSize;
	HRESULT	ret=Mouse->GetDeviceData(sizeof(DIDEVICEOBJECTDATA),MouseBuffer,&nMouseData,0);
	if (ret==DI_OK)
	{
		if (SkipKeys)
		{
			nMouseData = 0;
		}
		for (int i=0; i<nMouseData; i++)
		{
			const DIDEVICEOBJECTDATA &data = MouseBuffer[i];
			int btn = -1;
			switch (data.dwOfs)
			{
				case DIMOFS_BUTTON0: btn = 0; break;
				case DIMOFS_BUTTON1: btn = 1; break;
				case DIMOFS_BUTTON2: btn = 2; break;
				case DIMOFS_BUTTON3: btn = 3; break;
				case DIMOFS_X:
					GInput.mouseX += (int)data.dwData*GInput.mouseSensitivityX*1.5;
					moveX = GInput.mouseX*(1.0/200);
					break;
				case DIMOFS_Y:
					GInput.mouseY += (int)data.dwData*GInput.mouseSensitivityY*1.5;
					moveY = GInput.mouseY*(1.0/150);
					break;
				case DIMOFS_Z:
					GInput.mouseZ += (int)data.dwData;
					break;
			}
			if (btn>=0)
			{
				if (GInput.mouseButtonsReversed)
				{
					if (btn<2) btn = 1-btn;
				}
				if (data.dwData&0x80)
				{
					// button pressed
					// TODO: detect double click
					GInput.mouseButtons[btn] = true;
					GInput.mouseButtonsToDo[btn] = true;
				}
				else
				{
					// buttion released
					GInput.mouseButtons[btn] = false;
				}
			}

		}
	}
	// scan buttons (old version)
	GInput.mouseL = GInput.mouseButtons[0]>0;
	GInput.mouseR = GInput.mouseButtons[1]>0;
	GInput.mouseM = GInput.mouseButtons[2]>0;
	GInput.mouseLToDo = GInput.mouseButtonsToDo[0];
	GInput.mouseRToDo = GInput.mouseButtonsToDo[1];
	GInput.mouseMToDo = GInput.mouseButtonsToDo[2];

	// limit movement - helps in low framerates
	const float limitX=0.6;
	const float limitY=0.4;
	saturate(moveX,-limitX,+limitX);
	saturate(moveY,-limitY,+limitY);

#if _ENABLE_CHEATS
	if (GInput.GetCheat1ToDo(DIK_SLASH))
		GInput.revMouse=!GInput.revMouse;
#endif

	#endif

	// record change
	if (GInput.gameFocusLost<=0)
	{
		// update in-game mouse
		// scan moveX and moveY user assigned actions
		float speedX = GInput.GetAction(UAAimRight)-GInput.GetAction(UAAimLeft);
		float speedY = GInput.GetAction(UAAimDown)-GInput.GetAction(UAAimUp);
		moveX += speedX*timeDelta/800; // /200
		moveY += speedY*timeDelta/600; // /150
		GInput.cursorMovedX += moveX;
		if (GInput.revMouse)
		{
			GInput.cursorMovedY -= moveY;
		}
		else
		{
			GInput.cursorMovedY += moveY;
		}
	}
	// update UI mouse
	// used also for map
	GInput.cursorX+=moveX;
	GInput.cursorY+=moveY;

	GInput.cursorMovedZ += 0.01 * GInput.mouseZ;

	// record coordinate
	// saturation for whole screen, not only for 2D region
	AspectSettings as;
	GEngine->GetAspectSettings(as);
	
	float screenTopX = as.uiTopLeftX/(as.uiTopLeftX-as.uiBottomRightX);
	float screenTopY = as.uiTopLeftY/(as.uiTopLeftY-as.uiBottomRightY);

	//float screenBotX * (as.uiBottomRightX-as.uiTopLeftX) + as.uiTopLeftX = 1;
	float screenBotX = (1-as.uiTopLeftX)/(as.uiBottomRightX-as.uiTopLeftX);
	float screenBotY = (1-as.uiTopLeftY)/(as.uiBottomRightY-as.uiTopLeftY);

	// convert from 0..1 to -1 .. +1
	saturate(GInput.cursorX,screenTopX*2-1,screenBotX*2-1);
	saturate(GInput.cursorY,screenTopY*2-1,screenBotY*2-1);
	//saturate(GInput.cursorY,-1,1);
	if
	(
		(GInput.mouseL|GInput.mouseR|GInput.mouseM|GInput.mouseX|GInput.mouseY|GInput.mouseZ) ||
		fabs(GInput.cursorMovedY)+fabs(GInput.cursorMovedX)>0.001 
	)
	{
		GInput.mouseCursorLastActive=Glob.uiTime;
	}
	if
	(
		(GInput.mouseX|GInput.mouseY) ||
		fabs(GInput.cursorMovedY)+fabs(GInput.cursorMovedX)>0.001 
	)
	{
		if (!GInput.lookAroundEnabled) GInput.mouseTurnLastActive=Glob.uiTime;
		else GInput.keyboardTurnLastActive=Glob.uiTime;
	}
#endif
}

/*!
\patch_internal 1.14 Date 8/10/2001 by Ondra
- Added: New cheat to crash the game.
*/

const static EnumName CheatCodeNames[]=
{
	EnumName(CheatNone,"NONE"),
	EnumName(CheatUnlockCampaign,"CAMPAIGN"),
	EnumName(CheatExportMap,"TOPOGRAPHY"),
	EnumName(CheatWinMission,"ENDMISSION"),
	EnumName(CheatSaveGame,"SAVEGAME"),
	EnumName(CheatCrash,"CRASH"),
	EnumName(CheatFreeze,"FREEZE"),
	EnumName()
};

template<>
const EnumName *GetEnumNames(CheatCode dummy) {return CheatCodeNames;}

RString GetKeyName(int dikCode);

//Integrity check - force function aligment on 16B boundary
#pragma optimize("t",on)

void Input::ProcessKeyPressed(int dik)
{
#ifdef _WIN32
	// detect release-mode cheats
	if (!awaitCheat)
	{
		keysToDo[dik]=true;
		return;
	}
	// check if we match any cheat
	RString key = GetKeyName(dik);
	// check if we match some complete or incomplete cheat name
	cheatInProgress = cheatInProgress + key;
	//LogF("Cheat in progress: '%s' ('%s')",(const char *)key,(const char *)CheatInProgress);

	int len = cheatInProgress.GetLength();
	bool matchPossible = false;
	for (int i=0; ; i++)
	{
		const EnumName &chName = CheatCodeNames[i];
		if (!chName.IsValid()) break;
		if (!strcmpi(chName.name,cheatInProgress))
		{
			awaitCheat = false;
			cheatActive = (CheatCode)chName.value;
			GlobalShowMessage(1000,"Activated %s",(const char *)cheatInProgress);
			return;
		}
		if (!strnicmp(chName.name,cheatInProgress,len))
		{
			matchPossible = true;
		#if PROTECTION_ENABLED

			__asm
			{
				jmp Skip
				// Integrity check
				// here emit enough 0 to be on 4B boundary
				align 4
				/*
				#ifndef _KNI
				_emit 0
				_emit 0
				#endif
				*/
				// value here should be replaced with external program
				_emit SUM_ADJUST_MAGIC&0xff
				_emit (SUM_ADJUST_MAGIC>>8)&0xff
				_emit (SUM_ADJUST_MAGIC>>16)&0xff
				_emit (SUM_ADJUST_MAGIC>>24)&0xff
				Skip:
			}
		#endif
		}
	}
	if (!matchPossible)
	{
		//GlobalShowMessage(1000,"Cheat canceled");
		awaitCheat = false;
	}
	else
	{
		//GlobalShowMessage(1000,"Progress %s",(const char *)CheatInProgress);
	}
#endif
}

#pragma optimize("",on)

void ProcessKeyboard(DWORD sysTime, DWORD timeDelta)
{
	#if ENABLE_KEYMOUSE
	if (!Keyb) return;

	if (!UseWindow) Keyb->Acquire();
	bool someKey = false;

	DWORD nData=KeyboardBufferSize;
	HRESULT keyRet = DIERR_INPUTLOST;

	if (!fAppPaused)
	{
		keyRet=Keyb->GetDeviceData(sizeof(DIDEVICEOBJECTDATA),KeyboardBuffer,&nData,0);
	}
	if( SkipKeys )
	{
		// clear any outstanding keys
		GInput.ForgetKeys();
		nData=0,SkipKeys=false;
	}

	if( keyRet!=DI_OK ) nData=0;
	else if( nData>0 ) someKey=true;

	float invTimeDelta=timeDelta>0 ? 1.0/timeDelta : 1000;
	int k;
	for( k=0; k<256; k++ )
	{
		GInput.keys[k]=0;
		GInput.keysToDo[k]=false; // avoid key accumulation
	}

	if (!GWorld->IsUserInputEnabled()) return;

	for( int i=0; i<(int)nData; i++ )
	{
		const DIDEVICEOBJECTDATA &data=KeyboardBuffer[i];
		int k=data.dwOfs;
		if( data.dwData&0x80 )
		{
			// key pressed
			if (GInput.keyPressed[k]>0)
			{
				LogF("Key pressed twice");
			}
			GInput.keyPressed[k]=data.dwTimeStamp;
			if (!GWorld->Chat())
			{
				GInput.ProcessKeyPressed(k);
			}
		}
		else
		{
			if( GInput.keyPressed[k] )
			{
				// key released
				DWORD start=GInput.keyPressed[k];
				GInput.keyPressed[k]=0;
				if (!GWorld->Chat())
				{
					if (start < sysTime - timeDelta) start = sysTime - timeDelta;
					DWORD howLong=data.dwTimeStamp-start;
					GInput.keys[k]+=howLong*invTimeDelta;
				}
			}
			else
			{
				LogF("Key released twice");
			}
		}
	}
	// check for any non-released keys
	if (!GWorld->Chat())
	{
		for( k=0; k<256; k++ ) if( GInput.keyPressed[k] )
		{
			DWORD howLong=sysTime-GInput.keyPressed[k];
			if( howLong>=timeDelta ) GInput.keys[k]=1;
			else GInput.keys[k]+=howLong*invTimeDelta;
		}
		if (GInput.keys[DIK_LSHIFT] && GInput.keys[DIK_NUMPADMINUS])
		{
			//GlobalShowMessage(1000,"Cheat started");
			GInput.awaitCheat = true;
			GInput.cheatInProgress = "";
		}
	}
	#endif
}

void ProcessJoystick()
{
    #if ENABLE_JOYSTICK
	for (int i=0; i<N_JOYSTICK_BUTTONS; i++)
	{
		GInput.stickButtons[i] = 0;
		GInput.stickButtonsToDo[i] = false;
		GInput.stickPov[i] = false;
		GInput.stickPovToDo[i] = false;
	}

	if (!GWorld->IsUserInputEnabled()) return;
	
	const int jTHold=20;
	const int jBigTHold=80;
	int jAxis[JoystickNAxes];
	unsigned char jButtons[Joystick::NButtons];
	bool jButtonEdges[Joystick::NButtons];
	int pov;
	if( JoystickDev && JoystickDev->Get(jAxis,jButtons,jButtonEdges,pov) )
	{
		for( int i=0; i<JoystickNAxes; i++ )
		{
			if( abs(jAxis[i]-GInput.jAxisLast[i])>jTHold )
			{
				GInput.jAxisLast[i]=jAxis[i];
				GInput.jAxisLastActive[i] = Glob.uiTime;
			}
			if( abs(jAxis[i]-GInput.jAxisBigLast[i])>jBigTHold )
			{
				GInput.jAxisBigLast[i]=jAxis[i];
				GInput.jAxisBigLastActive[i] = Glob.uiTime;
			}
			GInput.stickAxis[i] = jAxis[i]*0.01;
		}

		Assert(N_JOYSTICK_BUTTONS == Joystick::NButtons);
		for (int i=0; i<N_JOYSTICK_BUTTONS; i++)
		{
			#ifdef _XBOX
			if (jButtons[i]>XINPUT_GAMEPAD_MAX_CROSSTALK)
			#endif
			{
				GInput.stickButtons[i] = jButtons[i]*(1.0f/255);
			}
			GInput.stickButtonsToDo[i] = jButtonEdges[i];
		}
		int pov8 = -1;
		if( pov!=-1 )
		{
			// pov is n*DI_DEGREES
			pov8=toInt(pov*(1.0/45));
			if( pov8>7 ) pov8-=8;
			else if( pov8<0 ) pov8+=8;
			/*
			static const int povKeys[8]=
			{
				DIK_NUMPAD8,DIK_NUMPAD9,DIK_NUMPAD6,DIK_NUMPAD3,
				DIK_NUMPAD2,DIK_NUMPAD1,DIK_NUMPAD4,DIK_NUMPAD7,
			};
			if( pov8>=0 && pov8<8 )
			{
				int dik=povKeys[pov8];
				GInput.keysToDo[dik]=true;
				GInput.keys[dik]=1;
			}
			*/
		} // pov hat
		// pov conversion: index to array
		for (int i=0; i<N_JOYSTICK_POV; i++)
		{
			GInput.stickPov[i] = ( pov8==i );

			GInput.stickPovToDo[i] = GInput.stickPov[i] && !GInput.stickPovOld[i];
			GInput.stickPovOld[i] = GInput.stickPov[i];
		}

		#ifdef _XBOX
			// convert XBox controls to DoChar events

			if (GInput.stickButtonsToDo[4])
			{
				GWorld->DoKeyDown(VK_UP,1,0);
				GWorld->DoKeyUp(VK_UP,1,0);
			}
			if (GInput.stickButtonsToDo[5])
			{
				GWorld->DoKeyDown(VK_DOWN,1,0);
				GWorld->DoKeyUp(VK_DOWN,1,0);
			}
			if (GInput.stickButtonsToDo[6])
			{
				GWorld->DoKeyDown(VK_LEFT,1,0);
				GWorld->DoKeyUp(VK_LEFT,1,0);
			}
			if (GInput.stickButtonsToDo[7])
			{
				GWorld->DoKeyDown(VK_RIGHT,1,0);
				GWorld->DoKeyUp(VK_RIGHT,1,0);
			}
			if (GInput.stickButtonsToDo[0])
			{
				GWorld->DoKeyDown(VK_RETURN,1,0);
				GWorld->DoKeyUp(VK_RETURN,1,0);
			}
			if (GInput.stickButtonsToDo[1])
			{
				GWorld->DoKeyDown(VK_TAB,1,0);
				GWorld->DoKeyUp(VK_TAB,1,0);
			}
			if (GInput.stickButtonsToDo[9])
			{
				GWorld->DoKeyDown(VK_ESCAPE,1,0);
				GWorld->DoKeyUp(VK_ESCAPE,1,0);
				GInput.keysToDo[DIK_ESCAPE] = true;
			}
			if (GInput.stickButtonsToDo[10])
			{
				GWorld->DoKeyDown(VK_SPACE,1,0);
				GWorld->DoKeyUp(VK_SPACE,1,0);
			}


		#endif

	} // JoystickDev
    #endif
}

#define IsKey(name) GInput.GetKey(GInput.name)>0
#define IsKeyToDo(name) GInput.GetKeyToDo(GInput.name)
#define KeyVal(name) GInput.GetKey(GInput.name)

#if !_DISABLE_GUI
bool AppIdle()
{
	PROFILE_SCOPE(aIdle);
	//static bool timeValid=false;
	//		timeValid=false;
	#ifndef _XBOX
		bool focused = fAppActive && !fAppPaused && !appIconic;
		bool enableDraw = ( focused || ForceRender && ( LandEditor || UseWindow) );
	#else
		bool enableDraw = true;
	#endif

	static DWORD lastTime;
	DWORD actTime=::GlobalTickCount();
	DWORD deltaTMs = actTime-lastTime;

	if (!enableDraw)
	{
		if (deltaTMs<50)
		{
			Sleep(50-deltaTMs);
			actTime=::GlobalTickCount();
			deltaTMs = actTime-lastTime;
		}
	}

	float deltaT=deltaTMs*0.001;
	lastTime=actTime;


	// note: GetDeviceData reports relative to GetTickCount
	static DWORD lastSysTime;
	DWORD sysTime=::GetTickCount();
	DWORD timeDelta=sysTime-lastSysTime;
	lastSysTime=sysTime;
	//if( !timeValid ) timeDelta=0,deltaT=0;
	//timeValid=true;

	saturateMin(deltaT,0.3);

#if _ENABLE_CHEATS
	static bool fixedSimulation=false;
	if (GInput.GetCheat2ToDo(DIK_O))
	{
		fixedSimulation=!fixedSimulation;
		GEngine->ShowMessage(500,"Fix Sim %s",fixedSimulation ? "On":"Off");
	}
	if( fixedSimulation )
	{
		deltaT=1.0/10;
	}
#endif

	#if _ENABLE_PERFLOG && _ENABLE_CHEATS
	static bool EnablePerfLog = false;
	if (GInput.GetCheat2ToDo(DIK_G))
	{
		EnablePerfLog = !EnablePerfLog;
		GlobalShowMessage(500,"PerfLog %s",EnablePerfLog ? "On":"Off");
		if (EnablePerfLog)
		{
			OpenPerfCounters();
		}
		else
		{
			ClosePerfCounters();
		}
	}
	#endif
	//if (!enableSim) deltaT=0;

	//if( ( focused || ForceRender && LandEditor ) && CanRender )


	if( CanRender )
	{
		if( UseWindow && !fMouseAcquired || !enableDraw )
		{
			RenderFrame(deltaT,enableDraw);
		}
		else
		{
			PROFILE_SCOPE(input);
			// use buffered events, use proportional integration of buttons time
			// game time

			// system time - necessary for keyboard events
			
			// read input devices
			ProcessMouse(timeDelta);
			ProcessKeyboard(sysTime, timeDelta);

			if (GInput.CheatActivated() == CheatFreeze)
			{
				//ErrorMessage("Test error");

				GInput.CheatServed();
				for(;;){}
				// freeze - infinite loop
			}

			if (GInput.joystickEnabled) ProcessJoystick();

			// process cheats
#if _ENABLE_CHEATS
			GInput.cheat1 = false;
			GInput.cheat2 = false;
			bool cheat1 = GInput.GetAction(UACheat1, false)>0;
			bool cheat2 = GInput.GetAction(UACheat2, false)>0;
			GInput.cheat1 = cheat1;
			GInput.cheat2 = cheat2;
#endif // #if _ENABLE_CHEATS

			// map actions
			GInput.keyMoveLeft=0;
			GInput.keyMoveRight=0;
			GInput.keyMoveUp=0;
			GInput.keyMoveDown=0;
			GInput.keyMoveForward=0;
			GInput.keyMoveFastForward=0;
			GInput.keyMoveSlowForward=0;
			GInput.keyMoveBack=0;
			
			GInput.keyTurnLeft=0;
			GInput.keyTurnRight=0;

			GInput.fire = false;
			GInput.fireToDo = false;
			//GInput.lookAroundEnabled = GInput.lookAroundToggleEnabled;

			for (int i=0; i<UAN; i++) GInput.actionDone[i] = false;

			{
				bool turbo=false;
#if _ENABLE_CHEATS
				if (!GInput.cheat1 && !GInput.cheat2)
#endif
				{
					if (GInput.GetAction(UATurbo)) turbo=true;

					if (GInput.GetAction(UAFire) > 0) GInput.fire = true;
					if (GInput.GetActionToDo(UAFire , false)) GInput.fireToDo = true;
					if (GInput.GetActionToDo(UALookAroundToggle))
					{
						GInput.lookAroundToggleEnabled = !GInput.lookAroundToggleEnabled;
					}

					GInput.keyMoveSlowForward = GInput.GetAction(UAMoveSlowForward);
					GInput.keyMoveFastForward = GInput.GetAction(UAMoveFastForward);
					GInput.keyMoveBack += GInput.GetAction(UAMoveBack);
					if (turbo)
					{
						GInput.keyMoveFastForward += GInput.GetAction(UAMoveForward);
					}
					else
					{
						GInput.keyMoveForward += GInput.GetAction(UAMoveForward);
					}

					GInput.keyTurnLeft += GInput.GetAction(UATurnLeft);
					GInput.keyTurnRight += GInput.GetAction(UATurnRight);

					bool oldVal = GInput.lookAroundEnabled;
					if (GInput.GetAction(UALookAround))
					{
						// TODO: watch key user defined
						GInput.lookAroundEnabled = true;
					}
					else
					{
						GInput.lookAroundEnabled = GInput.lookAroundToggleEnabled;
					}
					if (oldVal!=GInput.lookAroundEnabled)
					{
						// free-look changed - some reaction possible
						if (GWorld) GWorld->FreelookChange(GInput.lookAroundEnabled);
					}

					GInput.keyMoveUp += GInput.GetAction(UAMoveUp);
					GInput.keyMoveDown += GInput.GetAction(UAMoveDown);

					GInput.keyMoveLeft += GInput.GetAction(UAMoveLeft);
					GInput.keyMoveRight += GInput.GetAction(UAMoveRight);
				}

				
			}
	
			Object *cam = GWorld->CameraOn();
			if (cam)
			{
				cam->DetectControlMode();
			}

			RenderFrame(deltaT,true);
		}
		return false;
	}
	else
	{
		// Don't do anything when not the active app
		return true;
	}
}
#endif

bool AppServerLoop()
{
	PROFILE_SCOPE(sLoop);
	bool enableDraw = false;

	static DWORD lastTime;
	DWORD actTime=::GlobalTickCount();
	DWORD deltaTMs = actTime-lastTime;

	float deltaT=deltaTMs*0.001;
	lastTime=actTime;

	static DWORD lastSysTime;
	DWORD sysTime=::GetTickCount();
	DWORD timeDelta=sysTime-lastSysTime;
	lastSysTime=sysTime;
	saturateMin(deltaT,0.3);

	bool focused = fAppActive && !fAppPaused && !appIconic;

	if (focused)
	{
		ProcessKeyboard(sysTime, timeDelta);
	}


	#if _ENABLE_PERFLOG && _ENABLE_CHEATS
	static bool EnablePerfLog = false;
	if (GInput.GetCheat2ToDo(DIK_G))
	{
		EnablePerfLog = !EnablePerfLog;
		GlobalShowMessage(500,"PerfLog %s",EnablePerfLog ? "On":"Off");
		if (EnablePerfLog)
		{
			OpenPerfCounters();
		}
		else
		{
			ClosePerfCounters();
		}
	}
	#endif

	if( CanRender )
	{
		// map actions
		GInput.keyMoveLeft=0;
		GInput.keyMoveRight=0;
		GInput.keyMoveUp=0;
		GInput.keyMoveDown=0;
		GInput.keyMoveForward=0;
		GInput.keyMoveFastForward=0;
		GInput.keyMoveSlowForward=0;
		GInput.keyMoveBack=0;
		
		GInput.keyTurnLeft=0;
		GInput.keyTurnRight=0;

		GInput.fire = false;
		GInput.fireToDo = false;

		for (int i=0; i<UAN; i++) GInput.actionDone[i] = false;

		RenderFrame(deltaT,enableDraw);
		return false;
	}

	return true;
}

#if PROTECTION_ENABLED

#include "crc.hpp"

/*!
\name Fade
Simple easy to find and hack FADE test.
If the test fails, global variable DoFade is set and program can
degrade in any way.
Please insert list of checkpoints where CheckFade should be performed:
(It may be anywhere in the game or UI, if it is hit once per session, it is enough).
(Timing guideline: 50 ms)
	Landscape::LoadData

Please insert list of degradations:
	Bigger rifle dispersion.
	InGameUI message (with very low probability) Original games do not fade.
	TODO: Slowly decrease experience (score).

For second level protection see CheckGenuine
Please insert list of checkpoints where CheckGenuine should be performed:
(This protection should be called several times during typical mission).
(Timing guideline: 10 ms)
	????

\patch_internal 1.01 Date 6/21/2001 by Ondra
- New: FADE protection
\patch_internal 1.35 Date 12/11/2001 by Ondra added FADE protection
- Improved: 2nd level checksum protection.
*/
//@{

//! integrity check failed - FADE
#if ALWAYS_FAIL_CRC
bool DoFade = true;
#else
bool DoFade = false;
#endif

//! What should be result of CRC test

//! perform integrity check (CRC)
void CheckFade()
{
	CRCCalculator crc;
	unsigned crcAct = crc.CRC((void *)CRC_BASE_MAGIC,CRC_SIZE_MAGIC);
	if (crcAct!=CRC_VALUE_MAGIC)
	{
		// start fading
		DoFade = true;
		#if CHECK_FADE
		FILE *f = fopen("bin\\fade.txt","a");
		if (f)
		{
			fprintf
			(
				f,"FADE CRC failed (%x!=%x)\n",
				crcAct
			);
			fclose(f);
		}
		#endif
	}
}

#if CHECK_FADE

void ExtractFadeArea(void *start, int size)
{
	if (DoFade)
	{
		WarningMessage("Original games do not fade.");
	}
	FILE *f = fopen("bin\\fade.txt","a");
	if (f)
	{
		fprintf
		(
			f,"FADE range from %x to %x\n",
			start,(int)start+size
		);
	}
	FILE *b = fopen("bin\\fade.bin","wb");
	if (b)
	{
		fwrite(start,size,1,b);
		fclose(b);
	}
	FILE *c = fopen("bin\\code.bin","wb");
	if (c)
	{
		// write all code (should be copy of executable)
#define CODE_OFFSET 0x400000
#define CODE_START 0x401000
#define CODE_SIZE  0x257000

    MEMORY_BASIC_INFORMATION mbi;

    if ( !VirtualQuery( ExtractFadeArea, &mbi, sizeof(mbi) ) )
        return;

    DWORD hMod = (DWORD)mbi.AllocationBase;

    // Point to the DOS header in memory
    PIMAGE_DOS_HEADER pDosHdr = (PIMAGE_DOS_HEADER)hMod;

    // From the DOS header, find the NT (PE) header
    PIMAGE_NT_HEADERS pNtHdr = (PIMAGE_NT_HEADERS)(hMod + pDosHdr->e_lfanew);

    PIMAGE_SECTION_HEADER pSection = IMAGE_FIRST_SECTION( pNtHdr );

		int codeStart = pSection->VirtualAddress+CODE_OFFSET;
		int codeSize = pSection->SizeOfRawData;
		
		fwrite((void *)CODE_START,CODE_SIZE,1,c);

		fprintf
		(
			f,"Exe code %x, size %x\n",
			codeStart,codeSize
		);
		
		fclose(c);
	}
	if (f)
	{
		fclose(f);
	}
}

void ReportFade()
{
	ExtractFadeArea((void *)CRC_BASE_MAGIC,CRC_SIZE_MAGIC);
}

#else

#define ReportFade()

#endif

//@}

#endif // PROTECTION_ENABLED

static int ActualResolution( int *resol )
{
	int i;
	for( i=0;;i++ )
	{
		if( resol[i*2+0]==GLOB_ENGINE->Width() && resol[i*2+1]==GLOB_ENGINE->Height() )
		{
			return i;
		}
		if( resol[i*2+0]<=0 || resol[i*2+1]<=0 )
		{
			break;
		}
	}
	return 0;
}


#if 0 //!_RELEASE
int _matherr( struct _exception *except )
{
	Fail("Math error");
	return 0;
}
#endif

void SetFlushToZero();

void InitFPU()
{
#ifdef _WIN32
	// set rounding mode to near
	// disable all exceptions
	// set precision
	// default in Watcom 10.6: RC_NEAR|PC_53|MCW_EM|IC_AFFINE
	//_control87(RC_NEAR|PC_53|MCW_EM,MCW_EM|MCW_RC|MCW_PC);
	if (GDebugger.IsDebugger())
	{
		_control87
		(
			RC_NEAR|PC_24|
			MCW_EM&~
			(
				//_EM_OVERFLOW|_EM_UNDERFLOW|
				//_EM_INVALID|
				// _EM_ZERODIVIDE
				0
			),
			MCW_EM|MCW_RC|MCW_PC
		);
	}
	else
	{
		_control87(RC_NEAR|PC_24|MCW_EM,MCW_EM|MCW_RC|MCW_PC);
	}
	#ifdef _KNI
		_MM_SET_FLUSH_ZERO_MODE(_MM_FLUSH_ZERO_ON);
		if (GDebugger.IsDebugger())
		{
			Assert ( _MM_GET_EXCEPTION_MASK()== _MM_MASK_MASK);
			_MM_SET_EXCEPTION_MASK
			(
				_MM_MASK_MASK&~
				(
					//_MM_MASK_INVALID|
					//_MM_MASK_DENORM|
					//_MM_MASK_DIV_ZERO|
					//_MM_MASK_OVERFLOW|_MM_MASK_UNDERFLOW|
					//_MM_MASK_INEXACT|
					0
				)
			);
		}
		else
		{
			// disable all exceptions - should be default
			_MM_SET_EXCEPTION_MASK(_MM_MASK_MASK);
		}
	#endif
	if (EnablePIII)
	{
		//  set flush mode for PIII
		SetFlushToZero();
	}
	
	#if 0 //_DEBUG
		unsigned int fp_cw = 0;
		unsigned int fp_mask = 0;
		unsigned int bits;
		
		const static char *status[2] = { "disabled", "enabled" };
		
		fp_cw = _control87( fp_cw, fp_mask );
		
		LogF( "Interrupt Exception Masks" );
		bits = fp_cw & MCW_EM;
		LogF( "  Invalid Operation exception %s", status[ (bits & _EM_INVALID) == 0 ] );
		LogF( "  Denormalized exception %s", status[ (bits & _EM_DENORMAL) == 0 ] );
		LogF( "  Divide-By-Zero exception %s", status[ (bits & _EM_ZERODIVIDE) == 0 ] );
		LogF( "  Overflow exception %s", status[ (bits & _EM_OVERFLOW) == 0 ] );
		LogF( "  Underflow exception %s", status[ (bits & _EM_UNDERFLOW) == 0 ] );
		LogF( "  Precision exception %s", status[ (bits & _EM_INEXACT) == 0 ] );
		
		LogF( "Infinity Control" );
		bits = fp_cw & _MCW_IC;
		if( bits == _IC_AFFINE )     LogF( "  affine" );
		if( bits == _IC_PROJECTIVE ) LogF( "  projective" );
		
		LogF( "Rounding Control" );
		bits = fp_cw & _MCW_RC;
		if( bits == _RC_NEAR )       LogF( "  near" );
		if( bits == _RC_DOWN )       LogF( "  down" );
		if( bits == _RC_UP )         LogF( "  up" );
		if( bits == _RC_CHOP )       LogF( "  chop" );
		
		LogF( "Precision Control" );
		bits = fp_cw & _MCW_PC;
		if( bits == _PC_24 )         LogF( "  24 bits\n" );
		if( bits == _PC_53 )         LogF( "  53 bits\n" );
		if( bits == _PC_64 )         LogF( "  64 bits\n" );
	
	#endif
#endif
}

void DestroyEngine()
{
	if( GLOB_ENGINE ) delete GLOB_ENGINE,GLOB_ENGINE=NULL;
}

void InitWorld();

// ....

#include "vtuneProf.hpp"

#ifdef _WIN32
extern ArcadeTemplate GClipboard;
#endif

static void AppWinDestroy( HWND hwnd );
void ManCleanUp();

void DDTerm()
{
	#if defined _XBOX || !defined _WIN32
	AppWinDestroy(hwndApp);
	#endif

	GDebugger.PauseCheckingAlive();
	if( GEngine ) GEngine->ReinitCounters();

	//MainCDThread.Free();
	CanRender=false;
#ifdef _WIN32
	markersMap.Clear();
	CurrentTemplate.Clear();
	GClipboard.Clear();
#endif
	if( GEngine ) GEngine->StopAll();
	if( GWorld ) delete GWorld,GWorld=NULL;
	GPreloadedTextures.Clear();
	if( GSoundsys ) delete GSoundsys,GSoundsys=NULL;
/*
	if( GStringTable ) GStringTable=NULL;
	CampaignStringTable.Clear();
	MissionStringTable.Clear();
*/
	ClearStringtable();
	ManCleanUp();
	Glob.Clear();
	DestroyEngine();
	#if ENABLE_KEYMOUSE
	if (dInput) dInput->Release(), dInput = NULL;
	#endif
	#if PROTECTION_ENABLED && CHECK_FADE
	CheckFade();
	ReportFade();
	#endif
  Pars.Clear();
  Pars.DeleteVariables();
  ExtParsCampaign.Clear();
  ExtParsCampaign.DeleteVariables();
  ExtParsMission.Clear();
  ExtParsMission.DeleteVariables();
  Res.Clear();
  Res.DeleteVariables();
	GetNetworkManager().Done();
#if _ENABLE_DEDICATED_SERVER
    // remove my own pid_file:
  if ( MyPidFile )
  {
    DeleteFile((const char*)GetPidFileName());
  }
#endif
}


void AppPause(bool f)
{
	if (f)
	{
		// turn off rotation while paused
	#if ENABLE_KEYMOUSE
		if (Mouse) Mouse->Unacquire();
		if (Keyb) Keyb ->Unacquire();
	#endif
		fMouseAcquired = false;
		fAppPaused = true;
        if( CanRender ) GLOB_ENGINE->Pause();
	}
	else
	{
		fAppPaused = false;
	}
}

RString GetSaveDirectory();
//void SaveHeader();

// dirty flag - used for save game when Alt-F4 pressed
extern bool ContinueSaved;

static void AppWinDestroy( HWND hwnd )
{
	GDebugger.PauseCheckingAlive();
	if( GWorld ) GWorld->UnloadSounds();

	// save state for continue
	if (!ContinueSaved && GWorld->GetRealPlayer() && !GWorld->GetRealPlayer()->IsDammageDestroyed())
	{
		if (!IsOutOfMemory())
		{
			char buffer[256];
			sprintf(buffer, "%scontinue.fps", (const char *)GetSaveDirectory());
			GWorld->SaveBin(buffer, IDS_AUTOSAVE_GAME);
		}
	}
	
	if( GWorld ) delete GWorld,GWorld=NULL;
	GPreloadedTextures.Clear();
	if( GSoundScene ) delete GSoundScene,GSoundScene=NULL;
	if( GSoundsys ) delete GSoundsys,GSoundsys=NULL;
	//if( Glob.soundsys ) Glob.soundsys->Delete();
	
	//KillTimers(hwnd);
	#if ENABLE_KEYMOUSE
	if (Mouse) Mouse->Unacquire(),Mouse ->Release(),Mouse=NULL;
	if (Keyb)
	{
		if( !UseWindow ) Keyb->Unacquire();
		Keyb->Release();
		Keyb=NULL;
	}
	if (dInput) dInput->Release(),dInput=NULL;
	#endif

	if( ErrorString )
	{
#ifdef _WIN32
		#ifndef _XBOX
		if( HideCursor ) ShowCursor(TRUE);
		MessageBox(hwnd,ErrorString,AppName,MB_OK);
		#endif
#else
    fprintf(stderr,ErrorString);
    fputc('\n',stderr);
#endif
	}

  #if ENABLE_JOYSTICK
	JoystickDev.Free();
  #endif

#ifdef _WIN32
	hwndApp=NULL;
#else
	hwndApp=0;
#endif
}

#ifdef _WIN32

void InitWindow( HINSTANCE hInst, HWND hwnd );

/*!
\patch 1.31 Date 11/13/2001 by Jirka
- Fixed: Korean version - error in WM_IME_CHAR message processing
*/
LONG CALLBACK AppWndProc(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
	#ifndef _XBOX
	if( GLOB_ENGINE )
	{
		if( hwnd==hwndApp ) switch( msg )
		{
			case WM_ACTIVATE:
			{
				// catch activate/deactivate
				WORD fActive = LOWORD(wParam);
				BOOL fMinimized = (BOOL) HIWORD(wParam);
				if ( ( fActive == WA_INACTIVE ) || fMinimized )
				{
					GEngine->Deactivate();
				}
				else
				{
					GEngine->Activate();
				}
				GInput.ForgetKeys();
			}
			break;
		}
	}
	switch (msg)
	{
		case WM_CREATE:
			hwndApp=hwnd;
			InitWindow(hInstApp,hwnd);
			if( !AppWinCreate(hwnd) ) return -1; // some fatal error
		break;
		#if 1
		case WM_ACTIVATEAPP:
			// Keep track of whether or not the app is in the foreground
			fAppActive = ( wParam!=0 );
			// re-acquire mouse
			if( !fAppActive )
			{
				#if _ENABLE_CHEATS
				KeyState.RestoreGlobalState();
				#endif
				// unacquire everything if app is not active
				#if ENABLE_KEYMOUSE
					if (Mouse) Mouse->Unacquire();
					if (Keyb) Keyb->Unacquire();
				#endif
				fMouseAcquired = false;
				SkipKeys=true; // ignore any outstanding key events
				if( GSoundsys ) GSoundsys->Activate(false);
			}
			else
			{
				#if _ENABLE_CHEATS
				KeyState.GetGlobalState();
				#endif
				if( !UseWindow && MouseEx )
				{
					MouseAcquire();
					#if ENABLE_KEYMOUSE
					if (Keyb) Keyb->Acquire();
					#endif
					SkipKeys=true; // ignore any outstanding key events
					if( GSoundsys ) GSoundsys->Activate(true);
				}
			}
		break;
		case WM_SIZE:
			if( wParam==SIZE_MAXIMIZED || wParam==SIZE_RESTORED )
			{
				if( appIconic )
				{
					appIconic=false;
					Log("Window restored");
					if( HideCursor ) ShowCursor(FALSE);
					DoRestore=true;
				}
			}
			else if( wParam==SIZE_MINIMIZED )
			{
				if( !appIconic )
				{
					appIconic=true;
					Log("Window minimized");
					DoPause=true;
					if( HideCursor ) ShowCursor(TRUE);
				}
				break;
			}
		case WM_MOVE:
			if( UseWindow )
			{
				GetClientRect(hwnd, &clientRect);
				ClientToScreen(hwnd, (LPPOINT)&clientRect);
				ClientToScreen(hwnd, (LPPOINT)&clientRect+1);
				if( GLOB_ENGINE && CanRender )
				{
					int w=clientRect.right+1-clientRect.left;
					int h=clientRect.bottom+1-clientRect.top;
					if( w>=10 || h>=10 )
					{
						// ignore nonsense
						Log("Window switch resolution (%dx%d)",w,h);
						GLOB_ENGINE->SwitchRes(w,h,0);
					}
				}
			}
		break;
		#else
		case WM_ACTIVATEAPP:
		case WM_SIZE:
		case WM_MOVE:
			return 0;
		#endif
		
		case WM_SYSCOMMAND:
			//if( !UseWindow )
			//{
			switch( wParam )
			{
				// only a few system menu command are enabled
				case SC_CLOSE: case SC_DEFAULT: case SC_RESTORE:
				case SC_TASKLIST:
					break;
				case SC_MOVE: case SC_SIZE:
					if (UseWindow) break;
					return TRUE;
				// other are disabled and should do nothing
				default:
					//return TRUE;
					break;
				case SC_KEYMENU:
					return TRUE; // ignore
				case SC_MINIMIZE: case SC_MAXIMIZE:
				//case 
					if (NoRedrawWindow) return TRUE;
					break;
			}
		break;

		case WM_MENUSELECT:
			return 0;
			

		case WM_ENTERMENULOOP:
		case WM_EXITMENULOOP:
			return 0;


		/*		
		case WM_ENTERMENULOOP:
			AppPause(true);
		break;
		
		case WM_EXITMENULOOP:
			AppPause(false);
		break;
		*/
		
		case WM_INITMENU:
			// no menu
		return TRUE;
		
		case WM_INITMENUPOPUP:
		break;
		
		case WM_CLOSE:
			CanRender=false;
			CloseRequest=true;
		return TRUE;
		case WM_DESTROY:
			AppWinDestroy(hwnd);
			if( ValidateQuit ) PostQuitMessage(0);
		break;
		case WM_ERASEBKGND:
			if( !appIconic && NoRedrawWindow || UseWindow ) return TRUE;
		break;
		case WM_NCPAINT:
			if( !appIconic && NoRedrawWindow && !UseWindow ) return FALSE;
		break;
		case WM_NCACTIVATE:
			if( !appIconic && NoRedrawWindow && !UseWindow ) return TRUE;
		break;
		case WM_NCCREATE:
			if( !appIconic && NoRedrawWindow && !UseWindow ) return TRUE;
		break;
		case WM_PAINT:
			if (UseWindow)
			{
				ForceRender = true;
				ValidateRect(hwnd,NULL);
				return FALSE;
			}
			if( !appIconic && NoRedrawWindow || UseWindow )
			{
				ValidateRect(hwnd,NULL);
				return FALSE;
			}
		break;
		
		case WM_DISPLAYCHANGE:
		return 0;
		
		case WM_LBUTTONDOWN:
		case WM_RBUTTONDOWN:
		case WM_MBUTTONDOWN:
			if( UseWindow && !fMouseAcquired )
			{
				#if ENABLE_KEYMOUSE
				if(Mouse->Acquire()== DI_OK )
				{
					Keyb->Acquire();
					fMouseAcquired = true;
				}
				#endif
			}
		break;
		case WM_SYSKEYDOWN:
		case WM_KEYDOWN:
			// WM_KEYDOWN
			if( GWorld  )
			{
				unsigned repCnt=(lParam&0xffff);
				unsigned flags=(lParam>>16)&0xffff;
				GWorld->DoKeyDown(wParam,repCnt,flags);
			}
		break;

		case WM_KEYUP:
		case WM_SYSKEYUP:
			// WM_KEYDOWN
			if( GWorld )
			{
				unsigned repCnt=(lParam&0xffff);
				unsigned flags=(lParam>>16)&0xffff;
				GWorld->DoKeyUp(wParam,repCnt,flags);
			}
			break;
		case WM_CHAR:
			if( GWorld )
			{
				unsigned repCnt=(lParam&0xffff);
				unsigned flags=(lParam>>16)&0xffff;
				GWorld->DoChar(wParam,repCnt,flags);
			}
			break;
		case WM_IME_CHAR:
			if( GWorld )
			{
				unsigned repCnt = (lParam & 0xffff);
				unsigned flags = (lParam >> 16) & 0xffff;
				unsigned char ch1 = (wParam >> 8) & 0xff;
				unsigned char ch2 = wParam & 0xff;
				if (ch1 >= 0x80 && ch2 >= 0x40)
					GWorld->DoIMEChar(wParam,repCnt,flags);
			}
			break;
		case WM_IME_COMPOSITION:
			if( GWorld )
			{
				GWorld->DoIMEComposition(wParam,lParam);
			}
			break;
	}
	
	return DefWindowProc(hwnd,msg,wParam,lParam);
	#else
	return FALSE;
	#endif
}

#endif

// global messages

#define USER_RELOAD ( WM_APP+0 )
#define USER_CLOSE ( WM_APP+1 )
#define USER_FILE_HANDLE ( WM_APP+2 )
#define USER_MUTEX_HANDLE ( WM_APP+3 )
#define USER_WINDOW_HANDLE ( WM_APP+4 )
#define USER_ANIM_PHASE ( WM_APP+5 )
#define USER_RUN_SCRIPT ( WM_APP+6 )
#define USER_PREVIEW_DIALOG ( WM_APP+7 )

//#define SHARED_LEN ( 128*1024 ) // objects are usually max 32 KB

//bool CloseFile; // command: close application
char *ObjDataBuf = NULL; // local copy of data
int ObjDataLen;          // size of local data
float DoSetPhase=-1;

// shared memory implementation

static HANDLE FileMutex;
static HWND ObjektivWindow;
static HANDLE FileMap;

static void CloseSharing()
{
#ifdef _WIN32
	if( FileMap ) CloseHandle(FileMap),FileMap=NULL;
	if( FileMutex ) CloseHandle(FileMutex),FileMutex=NULL;
#endif
}

void UnlockSharedData()
{
	if( ObjDataBuf ) delete[] ObjDataBuf,ObjDataBuf=NULL,ObjDataLen=0;
}


void LockSharedData( int size )
{
	#if defined _WIN32 && !defined _XBOX
	UnlockSharedData();
	if( !FileMap || !FileMutex )
	{
		WarningMessage("Handles not inherited.");
	}
	else if( WaitForSingleObject(FileMutex,INFINITE)==WAIT_OBJECT_0 )
	{
		void *memMap=::MapViewOfFile(FileMap,FILE_MAP_READ,0,0,0); // map whole file
		if (memMap)
		{
			ObjDataBuf=new char[size];
			memcpy(ObjDataBuf,memMap,size);
			ObjDataLen=size;
			ForceRender=true;
			::UnmapViewOfFile(memMap);
		}
		ReleaseMutex(FileMutex);
	}
	#endif
}

static void SyncObjektiv()
{
	#if defined _WIN32 && !defined _XBOX
	float time=GWorld->GetViewerPhase();
	if( ObjektivWindow )
	{
		// send message to Objektiv
		int iTime=time*toIntFloor(65536);
		//WarningMessage("Send message %d to %x",iTime,ObjektivWindow);
		PostMessage(ObjektivWindow,USER_ANIM_PHASE,0,iTime);
	}
	#endif
}


/*!
\patch 1.12 Date 08/06/2001 by Ondra
- Added: MP, Anticheat: Exe integrity verification.
*/

unsigned int CalculateExeCRC(int offset, int size)
{
	#if defined _WIN32 && !defined _XBOX
	HMODULE hMod = GetModuleHandle(NULL);

	// get exe header from PE
  PIMAGE_DOS_HEADER pDosHdr = (PIMAGE_DOS_HEADER)hMod;

  // From the DOS header, find the NT (PE) header
  PIMAGE_NT_HEADERS pNtHdr = (PIMAGE_NT_HEADERS)((char *)hMod + pDosHdr->e_lfanew);

  PIMAGE_SECTION_HEADER pSection = IMAGE_FIRST_SECTION( pNtHdr );

	int codeStart = (int)hMod+pSection->PointerToRawData;
	int codeSize = pSection->SizeOfRawData;
	int codeEnd = codeStart + codeSize;

	int checkStart = offset;
	int checkEnd = offset+size;
	saturate(checkStart,codeStart,codeEnd);
	saturate(checkEnd,codeStart,codeEnd);

	//LogF("%x:%x, testing %x:%x",codeStart,codeSize,checkStart,checkEnd);
	CRCCalculator crc;
	crc.Reset();
	crc.Add((void *)checkStart,checkEnd-checkStart);
	return crc.GetResult();
	#else
	return 0;
	#endif
}

unsigned int CalculateDataCRC(const char *path, int offset, int size)
{
	// reject files not inside current directory
	if (!IsRelativePath(path)) return 0;
	QIFStreamB in;
	in.AutoOpen(path);
	if (in.fail()) return 0;
	int endCheck = offset+size;
	saturate(offset,0,in.rest());
	saturate(endCheck,offset,in.rest());

	CRCCalculator crc;
	crc.Reset();
	crc.Add(in.act()+offset,endCheck-offset);
	return crc.GetResult();
}

unsigned int CalculateConfigCRC(const char *path);


// global viewer properties


#if defined _WIN32 && !defined _XBOX
static void ProcessUserMessages( MSG &msg )
{
	if( msg.message==USER_CLOSE ) CloseRequest=true;
	else if( msg.message==USER_RELOAD )
	{
		LockSharedData(msg.lParam);
		Glob.config.lodCoef=msg.wParam*0.01f;
	}
	else if( msg.message==USER_ANIM_PHASE )
	{
		DoSetPhase=msg.lParam*(1.0/65536);
	}
	else if( msg.message==USER_WINDOW_HANDLE )
	{
		ObjektivWindow=(HWND)msg.lParam;
		//WarningMessage("ObjektivWindow handle %x",ObjektivWindow);
	}
	else if( msg.message==USER_FILE_HANDLE )
	{
		// TODO: use CreateFileMapping() to create a named map file
		HANDLE src=(HANDLE)msg.lParam;
		BOOL res=DuplicateHandle
		(
			GetCurrentProcess(),src,
			GetCurrentProcess(),&FileMap,
			0,FALSE,DUPLICATE_SAME_ACCESS
		);
		if( !res ) ErrorMessage("File mapping handle not duplicated");
	}
	else if( msg.message==USER_MUTEX_HANDLE )
	{
		// TODO: create a named mutex
		//CreateMutex(NULL,false,"Suma - ObjView Data Locked");
		HANDLE src=(HANDLE)msg.lParam;
		BOOL res=DuplicateHandle
		(
			GetCurrentProcess(),src,
			GetCurrentProcess(),&FileMutex,
			0,FALSE,DUPLICATE_SAME_ACCESS
		);
		//CloseHandle(src);
		if( !res ) ErrorMessage("Mutex handle not duplicated");
	}
	else if (msg.message == USER_RUN_SCRIPT)
	{
		static const int bufferSize = 256;
		char buffer[bufferSize];
		GlobalGetAtomName((ATOM)msg.lParam, buffer, bufferSize);
		GlobalDeleteAtom((ATOM)msg.lParam);

		Script *script = new Script(buffer, GameValue());
		GWorld->AddScript(script);
	}
	else if (msg.message == USER_PREVIEW_DIALOG)
	{
		static const int bufferSize = 512;
		char buffer[bufferSize];
		GlobalGetAtomName((ATOM)msg.lParam, buffer, bufferSize);
		GlobalDeleteAtom((ATOM)msg.lParam);

		const char *ptr = strchr(buffer, ';');
		if (ptr)
		{
			RString filename(buffer, ptr - buffer);
			RString resource(ptr + 1);
			ptr = strrchr(filename, '\\');
			if (!ptr) ptr = strrchr(filename, '/');
			if (ptr)
			{
				RString dir(filename, ptr - filename);
				filename = ptr + 1;
				::GetCurrentDirectory(bufferSize, buffer);
				::SetCurrentDirectory(dir);
				Res.Clear();
				Res.Parse(filename);
				GWorld->DestroyUserDialog();
				AbstractOptionsUI *CreateUserDialog(RString name);
				GWorld->SetUserDialog(CreateUserDialog(resource));
				::SetCurrentDirectory(buffer);
			}
		}
	}
}
#endif

#ifdef _WIN32
static void ProcessMessages( bool noWait )
{
	#if !defined _XBOX
	MSG msg;
	GDebugger.ProcessAlive();
	if( PeekMessage(&msg, 0, 0, 0, PM_REMOVE) )
	{
		if( msg.message>=WM_APP && msg.message<WM_APP+1024 )
		{
			ProcessUserMessages(msg);
		}
		else if( msg.message==WM_QUIT )
		{
			Assert( ValidateQuit );
			#if PROFILE_EXIT
				EnableProfiler();
			#endif
			DDTerm();
			#if PROFILE_EXIT
				DisableProfiler();
			#endif
			if( HideCursor ) ShowCursor(TRUE);
			LogF("Exit %d",msg.wParam);
			Sleep(500);
			exit(msg.wParam);
		}
		else
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		if (CanRender)
		{
			if( GEngine && DoRestore )
			{
				GLOB_ENGINE->Restore();
				DoRestore=false;
			}
			if( GEngine && DoPause )
			{
				GLOB_ENGINE->Pause();
				DoPause=false;
			}
			if( GLandscape && ObjDataBuf )
			{
				// some data reload required:
				// recreate the viewed object
				GWorld->ReloadViewer(ObjDataBuf,ObjDataLen,"house");
				// destroy temporary data
				UnlockSharedData();
				ForceRender=true;
			}
			if( GWorld && DoSetPhase>=0 )
			{
				GWorld->SetViewerPhase(DoSetPhase);
				DoSetPhase=-1;
				ForceRender=true;
			}
		}
	}
	else if( !noWait )
	{
		#if !_DISABLE_GUI
		if( AppIdle() )
		#endif
		{
			::MsgWaitForMultipleObjects(0,NULL,FALSE,2000,QS_ALLEVENTS);
			//WaitMessage();
		}
	}
	#else
		GDebugger.ProcessAlive();
        #if !_DISABLE_GUI
		if (!noWait) AppIdle();
        #endif
	#endif
}
#endif

/*!
	\patch 1.01 Date 06/19/2001 by Jirka
	- Fixed: keyboard input disabled during mission loading
	\patch_internal 1.01 Date 06/19/2001 by Jirka
	- function removes user input messages from windows queue
	- used in DisplayGetReady constructor to avoid keyboard input during mission loading
*/
void RemoveInputMessages()
{
	#if defined _WIN32 && !defined _XBOX
	MSG msg;
	while (PeekMessage(&msg, 0, WM_KEYFIRST, WM_KEYLAST, PM_REMOVE)) {}
	#endif
}

extern Ref<Script> ProgressScript;

RString GetExtCfgFile();

#define CHECK_STRINGTABLE 0

#if CHECK_STRINGTABLE
void ReportStringtable(QOFStream &out, RString filename)
{
	out << filename << "\r\n";

	StringTableDynamic table;
	table.Load(filename);

	for (int i=0; i<table.NTables(); i++)
	{
		for (int j=0; j<table.GetTable(i).Size(); j++)
		{
			StringTableItem2 item = table.GetTable(i)[j];
			RString name = item.name;
			RString value = item.value;

			bool found = false;
			for (int k=0; k<value.GetLength(); k++)
			{
				unsigned char c = (unsigned char)value[k];
				if (c >= 0x80)
				{
					found = true;
					char buffer[16];
					sprintf(buffer, "0x%x%x ", c / 0x10, c % 0x10);
					out << buffer;
				}
			}
			if (found)
			{
				out << item.name;
				for (int k=0; k<50-item.name.GetLength(); k++) out << " ";
				out << item.value << "\r\n";
			}
		}
	}
	out << "\r\n";
}

void FindStringtables(QOFStream &out, const char *dir)
{
	char buffer[512];
	sprintf(buffer, "%s\\*.*", dir);

	WIN32_FIND_DATA info;
	HANDLE h = FindFirstFile(buffer, &info);
	if (h != INVALID_HANDLE_VALUE)
	{
		do
		{
			if ((info.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0)
			{
				if (info.cFileName[0] != '.')
				{
					sprintf(buffer, "%s\\%s", dir, info.cFileName);
					FindStringtables(out, buffer);
				}
			}
			else if (stricmp(info.cFileName, "stringtable.csv") == 0)
			{
				sprintf(buffer, "%s\\%s", dir, info.cFileName);
				ReportStringtable(out, buffer);
			}
		}
		while (FindNextFile(h, &info));
		FindClose(h);
	}
}

void CheckStringtable(RString language)
{
	GLanguage = language;

	QOFStream out;
	out.open(RString("C:\\check.") + language + RString(".txt"));
	
	ReportStringtable(out, "bin\\stringtable.csv");
	FindStringtables(out, "anims");
	FindStringtables(out, "campaigns");
	FindStringtables(out, "missions");
	FindStringtables(out, "mpmissions");

	out.close();
}
#endif

#if _CZECH
#include "roxxe.hpp"
#pragma comment(lib,"clientlib")
//char CDDrive;
#endif

void InitMan();

static void RunPreferences()
{
#ifdef _WIN32
	STARTUPINFO startupInfo;
	ZeroMemory(&startupInfo, sizeof(startupInfo));
	startupInfo.cb = sizeof(startupInfo);
	PROCESS_INFORMATION processInformation;
	ZeroMemory(&processInformation, sizeof(processInformation));
	::CreateProcess
	(
		NULL, PreferencesExe,
		NULL, NULL,											// security
		FALSE,													// inheritance
		0,															// creation flags 
		NULL,														// env
		NULL,														// pointer to current directory name 
		&startupInfo, &processInformation 
	);
#endif
}

bool InitCommandLine(LPSTR szCmdLine)
{
	#ifndef _XBOX
	// remove this patch after changes in gamespy registration
	#ifdef _WIN32
	if (strstr(szCmdLine, "-host") || strstr(szCmdLine, "-connect"))
	{
		char buffer[512];
		::GetCurrentDirectory(512, buffer);
		if (stricmp(buffer + strlen(buffer) - 4, "\\bin") == 0)
		{
			*(buffer + strlen(buffer) - 4) = 0;
			::SetCurrentDirectory(buffer);
		}
		else if (stricmp(buffer + strlen(buffer) - 5, "\\bin\\") == 0)
		{
			*(buffer + strlen(buffer) - 5) = 0;
			::SetCurrentDirectory(buffer);
		}
	}
	#endif

	if (!QIFStream::FileExists(FlashpointCfg))
	{
		RunPreferences();
		return false;
	}
	#endif
	return true;
}

/*!
\patch_internal 1.20 Czech version Date 09/03/2001 by Jirka
- Added: Enable Polish language at _CZECH configuration
\patch_internal 1.35 Date 12/11/2001 by Jirka
- Added: Check if CD is inserted in Czech version
\patch 1.40 Date 12/20/2001 by Jirka
- Fixed: fix due to bug in resource of Polish Preferences
*/

#if _CZECH
bool InitCzechCD()
{
	GLanguage = "Czech";

	// enable Polish
	RString lang;
	ParamFile cfg;
	cfg.Parse(FlashpointCfg);
	ParamEntry *cfgLang = cfg.FindEntry("language");
	if (cfgLang)
	{
		RString lang = *cfgLang;
		if
		(
			stricmp(lang, "polish") == 0 ||
			stricmp(lang, "polski") == 0
		)
			GLanguage = "Polish";
	}

	HKEY key;
	if
	(
		!::RegOpenKeyEx
		(
			HKEY_LOCAL_MACHINE, ConfigApp,
			0, KEY_READ, &key
		) == ERROR_SUCCESS
	) return false;

	DWORD size = 2;
	DWORD type = REG_SZ;
	char buffer[2];
	::RegQueryValueEx(key, "CDDRIVE", 0, &type, (BYTE *)buffer, &size);
	::RegCloseKey(key);

//	CDDrive = buffer[0];
	
	char path[256];
	sprintf(path, "%s:\\Flashpoint\\OperationFlashpoint.exe", buffer);
	if (!QIFStream::FileExists(path))
	{
		ErrorMessage("Insert original CD");
		return false;
	}
	
	// Czech CD Protection added
	PerformRandomRoxxeTest_000(buffer[0]);
	return true;
}
#endif

#if defined _WIN32 && !defined _XBOX
void InitGlide()
{
	if (UseGlide)
	{
		// safety check - check for Glide3X.dll presence
		HINSTANCE lib = LoadLibrary("Glide3X.dll");
		if (!lib)
		{
			WarningMessage("No Glide3x.dll present - cannot run Glide version");
			UseGlide = false;
		}
	}
	if (UseGlide)
	{
		GFileBankPrefix = "Glide";
	}
}
#else
void InitGlide()
{
	UseGlide = false;
}
#endif

#ifdef _WIN32

void PostDestroy()
{
	#if !defined _XBOX
	PostMessage(hwndApp,WM_DESTROY,0,0);
	#endif
}

#endif

//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER

#ifdef _WIN32
HWND consoleEditHWnd;
#endif

/*!
\patch 1.92 Date 8/8/2003 by Pepca
- Improved: Flushing of stdout (for instant console output) on Linux.
*/

int ConsoleF(const char *format, ...)
{
#ifdef _WIN32

	if (!DoCreateDedicatedServer) return 0;

	BString<512> buf;

	SYSTEMTIME syst;
	GetLocalTime(&syst);
	sprintf
	(
		buf, "%2d:%02d:%02d ",
		syst.wHour, syst.wMinute, syst.wSecond
	);
		
	va_list arglist;
	va_start( arglist, format );
	BString<512> buf2;
	buf2.VPrintF(format,arglist);
	va_end( arglist );
	buf += buf2;

#ifdef NET_LOG
  NetLog((const char*)buf);
#endif

	strcat(buf,"\r\n");

	// output to dialog window
	Temp<char> temp;
	int count = GetWindowTextLength(consoleEditHWnd);

	temp.Realloc(count+strlen(buf)+1);
	temp[0] = 0;
	GetWindowText(consoleEditHWnd,temp,count+1);

	int limit = 30*1024;
	while (strlen(temp)>limit)
	{
		const char *firstLine = strchr(temp,'\n');
		if (!firstLine) {*temp=0;break;}
		memmove(temp,firstLine+1,strlen(firstLine));
	}
	strcat(temp,buf);
	SetWindowText(consoleEditHWnd,temp);
	PostMessage(consoleEditHWnd,EM_SETSEL,strlen(temp),strlen(temp));
	PostMessage(consoleEditHWnd,EM_SCROLLCARET,0,0);

#else

    time_t now;
    time(&now);
    struct tm *now_tm = localtime(&now);
    printf("%2d:%02d:%02d ",now_tm->tm_hour,now_tm->tm_min,now_tm->tm_sec);
    
	va_list arglist;
	va_start( arglist, format );
    vprintf( format, arglist );
    putchar('\n');
	va_end( arglist );
  fflush(stdout);

#ifdef NET_LOG
  char buf[512];
  sprintf(buf,"%2d:%02d:%02d ",now_tm->tm_hour,now_tm->tm_min,now_tm->tm_sec);
  va_start( arglist, format );
    vsprintf( buf+strlen(buf), format, arglist );
  va_end( arglist );
  NetLog(buf);
#endif

#endif

	return 0;
}

int ConsoleTitle(const char *format, ...)
{
	BString<512> buf;
		
	va_list arglist;
	va_start( arglist, format );
	vsprintf(buf,format,arglist);
	va_end( arglist );

#ifdef _WIN32
	SetWindowText(hwndApp,buf);
#else
	puts(buf);
#endif
	return 0;
}

/*
\patch_internal 1.22 Date 9/10/2001 by Ondra
- Fixed: Application Active detection was not working in dedicated server loop.
*/

#ifdef _WIN32
static int CALLBACK ConsoleDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_INITDIALOG:
		{
			hwndApp=hDlg;
			InitWindow(hInstApp,hDlg);
			if( !AppWinCreate(hDlg) ) return FALSE; // some fatal error

			consoleEditHWnd = GetDlgItem(hDlg,IDC_CONSOLE_EDIT);

			return TRUE;
		}

		case WM_COMMAND:
			if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) 
			{
				EndDialog(hDlg, LOWORD(wParam));
				hwndApp = NULL;
				AppWinDestroy(hDlg);
				ValidateQuit=true;
				return TRUE;
			}
			break;

		case WM_SIZE:
			if (consoleEditHWnd)
			{
				RECT rect;
				GetClientRect(hDlg, &rect);
				MoveWindow
				(
					consoleEditHWnd, rect.left, rect.top,
					rect.right - rect.left, rect.bottom - rect.top, TRUE
				);
			}
			break;

		case WM_ACTIVATEAPP:
			// Keep track of whether or not the app is in the foreground
			fAppActive = ( wParam!=0 );
			break;
	}
	return FALSE;
}
#endif        // _WIN32

#endif        // _ENABLE_DEDICATED_SERVER
//}

/*!
\patch_internal 1.15 Date 8/10/2001 by Ondra
- Fixed: Interaction between SafeDisc and Fade protections
caused by dedicated server code.
*/

#if _ENABLE_DEDICATED_SERVER
static void CreateDedicatedServer(HINSTANCE hInst, CreateEnginePars &pars)
{
	Engine *CreateEngineDummy();
	GEngine = CreateEngineDummy();
	pars.HideCursor = false;
	pars.NoRedrawWindow = false;

#ifdef _WIN32
	hwndApp = CreateDialog(hInst, MAKEINTRESOURCE(IDD_CONSOLE), NULL, ConsoleDlgProc);
	ShowWindow(hwndApp,SW_SHOW);
	UpdateWindow(hwndApp);
#else
    AppWinCreate(0);
#endif

	//AllocConsole();
#ifdef _WIN32
	ConsoleF("Dedicated server created");
#else
	ConsoleF("Dedicated server created, memory used: %u KB",MemoryUsed()>>10);
#endif
}

#if _ENABLE_REPORT
#include "MemHeap.hpp"

#ifdef _WIN32

extern RefD<MemHeapLocked> safeHeap;

static void PrintServerStats()
{
	int maxFree = safeHeap->MaxFreeLeft();
	int totalFree = safeHeap->TotalFreeLeft();
	int countFree = safeHeap->CountFreeLeft();
	int totalAlloc = safeHeap->TotalAllocated();
	ConsoleF
	(
		"MT Mem: free %d, max free %d, count free %d, alloc %d",
		totalFree,maxFree,countFree,totalAlloc
	);
	LogF("*** Detailed MT Heap stats");
	safeHeap->LogAllocStats();
}

#else

static void PrintServerStats()
{
}

#endif

#include "statistics.hpp"

StatisticsByName *messagePoolStatistics ();
StatisticsByName *messagePoolStatisticsUsed ();

static void PrintServerDetailedStats()
{
#ifdef _WIN32
	LogF("*** Detailed MT Heap stats");
	safeHeap->LogAllocStats();
#endif

	SRef<StatisticsByName> stats = messagePoolStatistics();
	LogF("*** Message Pool recycled stats");
	stats->Report();

	stats = messagePoolStatisticsUsed();
	LogF("*** Message Pool used stats");
	stats->Report();
}
#else
#define PrintServerStats()
#endif

#ifndef _WIN32

volatile bool interrupted = false;

void handleInt ( int sig )
    // handles SIGINT
{
    interrupted = true;
}

char readKey ()
    // non-blocking keyboard peek
{
    fd_set set;                             // list of receiving fd's
    static struct timeval timeout =
        { 0L, 0L };                         // select() timeout: non-blocking
    FD_ZERO ( &set );
    FD_SET ( STDIN_FILENO, &set );
    int error = select(FD_SETSIZE,&set,NULL,NULL,&timeout);
    if ( error != 1 )
        return (char)0;                     // data are not ready
    return (char)getchar();
}

#endif

static void DedicatedServerLoop()
{
	#if _ENABLE_REPORT
	int stat1Time = GlobalTickCount();
	//int stat2Time = GlobalTickCount();
	#endif
#ifndef _WIN32
  signal(SIGINT,handleInt);
#endif
	while (true)
	{

		#if _ENABLE_REPORT
		int now = GlobalTickCount();
		// print once per hour
		if (now>stat1Time+3600000)
		{
			PrintServerStats();
			stat1Time = now;
		}
		/*
		if (now>stat2Time+60000)
		{
			PrintServerDetailedStats();
			stat2Time = now;
		}
		*/
		#endif
		AppServerLoop();

#ifdef _WIN32

		MSG msg;
		if( PeekMessage(&msg, 0, 0, 0, PM_REMOVE) )
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			if (msg.message==USER_CLOSE)
			{
				PostMessage(hwndApp,WM_CLOSE,0,0);
			}
			if (ValidateQuit)
			{
				#if PROFILE_EXIT
					EnableProfiler();
				#endif
				DDTerm();
				#if PROFILE_EXIT
					DisableProfiler();
				#endif
				if( HideCursor ) ShowCursor(TRUE);
				int ret = 0;
				LogF("Exit %d",ret);
				Sleep(500);
				exit(ret);
			}
		}

#else
    if ( interrupted )
    {
      ConsoleF("Dedicated server finished (SIGINT signal), memory used: %u KB",MemoryUsed()>>10);
      LogF("Exit (SIGINT signal), memory used: %u KB",MemoryUsed()>>10);
      Sleep(500);
      return;
    }
#endif

	}
}
#endif

static bool ParseStringtable(RString dir, void *context)
{
	// find bin directory
	#ifndef _XBOX
	if (dir.GetLength() == 0) dir = "bin";
	else dir = dir + RString("\\bin");
	#endif

	// stringtable filename
	RString file = dir + RString("\\stringtable.csv");
	if (QIFStreamB::FileExist(file))
	{
		LoadStringtable("Global", file, 0, false);
//		GStringTable = new StringTable(file);
		return true;
	}

	return false;
}

/*!
\patch_internal 1.51 Date 4/17/2002 by Jirka
- Changed: config.bin (resource.bin) in mod has higher priority than config.cpp (resource.cpp) in base directory 
*/

static bool ParseConfig(RString dir, void *context)
{
	// find bin directory
	#ifndef _XBOX
	if (dir.GetLength() == 0) dir = "bin";
	else dir = dir + RString("\\bin");
	#endif

	// text config
	RString file = dir + RString("\\config.cpp");
	if (QIFStreamB::FileExist(file))
	{
		#ifndef _XBOX
		char buffer[512];
		::GetCurrentDirectory(512, buffer);
		SetCurrentDirectory(dir);
		#endif
		Pars.Parse("config.cpp");
		#ifndef _XBOX
		SetCurrentDirectory(buffer);
		#endif
		return true;
	}

	// binary config
	file = dir + RString("\\config.bin");
	if (QIFStreamB::FileExist(file))
	{
		Pars.ParseBin(file);
		return true;
	}

	return false;
}

static bool ParseResource(RString dir, void *context)
{
	// find bin directory
	#ifndef _XBOX
	if (dir.GetLength() == 0) dir = "bin";
	else dir = dir + RString("\\bin");
	#endif

	// text resource
	RString file = dir + RString("\\resource.cpp");
	if (QIFStreamB::FileExist(file))
	{
		#ifndef _XBOX
		char buffer[512];
		::GetCurrentDirectory(512, buffer);
		SetCurrentDirectory(dir);
		#endif
		Res.Parse("resource.cpp");
		#ifndef _XBOX
		SetCurrentDirectory(buffer);
		#endif
		return true;
	}

	// binary resource
	file = dir + RString("\\resource.bin");
	if (QIFStreamB::FileExist(file))
	{
		Res.ParseBin(file);
		return true;
	}

	return false;
}

#include "versionNo.h"
#include <El/Modules/modules.hpp>
#include <El/initLibraryElement.hpp>

#if 0

typedef void (*RegisterPluginFunction)(int &retLevel, ModuleFunction *&retInit, ModuleFunction *&retDone);
AutoArray< SRef<RegistrationItem> > PluginCache;
void RegisterPlugins()
{
	RString dir = "Plugins\\";
	WIN32_FIND_DATA info;
	HANDLE h = FindFirstFile(dir + RString("*.dll"), &info);
	if (h != INVALID_HANDLE_VALUE)
	{
		do
		{
			if ((info.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0) continue;
			RString name = dir + info.cFileName;
			HMODULE library = LoadLibrary(name);
			if (library)
			{
				RegisterPluginFunction f = (RegisterPluginFunction)GetProcAddress(library, "RegisterPlugin");
				if (f)
				{
					int level;
					ModuleFunction *initFunction;
					ModuleFunction *doneFunction;
					(f)(level, initFunction, doneFunction);
					RegistrationItem *item = new RegistrationItem(level, initFunction, doneFunction);
					PluginCache.Add(item);
					RegisterModule(item);
				}
			}
		}
		while (FindNextFile(h, &info));
		FindClose(h);
	}
}
#endif

/*!
	\patch_internal 1.01 Date 6/20/2001 by Ondra: Added integrity check.
	(see Integrity check)
	\patch 1.27 Date 10/15/2001 by Ondra:
	- Fixed: Standalone server exe not working.
	\patch 1.36 Date 12/12/2001 by Ondra:
	- Fixed: Application freezing detection turned off for dedicated server,
	it sometimes caused false warnings and premature server termination.
*/

// note: we want WinMain to be protected by Fade
// this is impossible when there are any DLL functions referenced
// all refererences to Win32 calls must be moved outside


#ifdef _WIN32
#ifndef _XBOX
int PASCAL WinMain(HINSTANCE hInst, HINSTANCE hPrev, LPSTR szCmdLine, int sw)
#else
int main()
#endif
#else
int main ( int argc, char **argv )
#endif
{
    AIThreat thr;
    thr.u.soft = 1;
    const char * volatile version = "VersionMapID" APP_VERSION_TEXT;
    InitLibraryElement();
    #ifndef _WIN32
    char cmdLine[1024];
    char *ptr = cmdLine;
    while ( argc-- > 1 ) {
        strcpy(ptr,argv[1]);
        ptr += strlen(argv[1]);
        *ptr++ = ' ';
        argv++;
        }
    *ptr++ = (char)0;
    *ptr   = (char)0;
    char *szCmdLine = cmdLine;
    HINSTANCE hInst = 0;
    HINSTANCE hPrev = 0;
    int sw = 0;
    #endif
	#ifndef _XBOX
	if (!InitCommandLine(szCmdLine))
	{
		return 0;
	}
	#else
		const char *szCmdLine = "\0\0";
		HINSTANCE hInst = NULL;
		HINSTANCE hPrev = NULL;
		int sw = 0;
	#endif
    #ifdef _WIN32
	GDebugExceptionTrap.SetLogFileName(FlashpointRpt);
	GDebugExceptionTrap.SetStackBottom(&hInst);
    #endif

	bool keyOK=false;
	Log("********** WinMain ******** start");

	//HRESULT err=CoInitialize(NULL);
	//if( err!=S_OK && err!=S_FALSE ) ErrorMessage("COM Interface failed.");

	// reserve virtual address space
	//InitMemory(64*1024*1024);

	#if _ENABLE_CHEATS
	KeyState.SetCapsLock(false);
	#endif

	// scan command line for arguments (pass1)
	const char *args=szCmdLine;
	for( const char *s=szCmdLine; ; )
	{
		if( *s=='"' )
		{
			args=++s;
			// string argument
			while( *s && *s!='"' ) s++;
			// argument between args and s
			SingleArgumentPass1(args,s);
		}
		else if( *s==' ' )
		{
			SingleArgumentPass1(args,s);
			args=++s;
		}
		else if( *s==0 )
		{
			SingleArgumentPass1(args,s);
			break;
		}
		else s++;
	}

	if (!ReadRegistry())
	{
		RunPreferences();
		return 0;
	}

#if CHECK_STRINGTABLE
/*
	{
		static RString languages[] = {"English","French","Italian","Spanish","German","Czech"};
		static int n = sizeof(languages) / sizeof(RString);
		for (int i=0; i<n; i++) CheckStringtable(languages[i]);
	}
*/
	CheckStringtable("English");
#endif


	// note: code address is constant unless /fixed:no
	// is specified on linker command line
	// note: code size is constant unless code is changed
	// code mostly grows
	// ADDED Patch 1.01 - Integrity check.
	// BEGIN ADDED
	
	// note: external program must be run to "sign" exe
	#if PROTECTION_ENABLED && CHECK_FADE
	unlink("bin\\fade.txt");
	CheckFade();
	ReportFade();
	CheckGenuine();
	#endif

	// END ADDED


	{
#if _CZECH
		if (!InitCzechCD())
		{
			return 1;
		}
#elif _RUSSIAN
		// force russian
		GLanguage = "Russian";
#elif _COLD_WAR_ASSAULT
		// force english
		GLanguage = "English";
#else
		// load stringtable
		GLanguage = "English";
		ParamFile cfg;
		cfg.Parse(FlashpointCfg);
		ParamEntry *cfgLang = cfg.FindEntry("language");
		if (cfgLang) GLanguage = *cfgLang;
#endif
		SetLangID(GLanguage);

		//CHANGE AFTER RELEASE 1.00 - enable different directories for config files here

		DoVerify(EnumModDirectories(ParseStringtable, NULL));

		// Initialize modules
		// - must be after stringtable is loaded now
		// - must be before config and resource is used now
#if 0
		RegisterPlugins();
#endif
		
		InitModules();

		LogF("Before config parsing: Total allocated: %d",MemoryUsed()/1024);
		
		DoVerify(EnumModDirectories(ParseConfig, NULL));

		MaxGroups =
			(Pars >> "CfgWorlds" >> "GroupNameList" >> "letters").GetSize() *
			(Pars >> "CfgWorlds" >> "GroupColorList" >> "colors").GetSize();

		GStats.Init();

		DoVerify(EnumModDirectories(ParseResource, NULL));
		LogF("After config parsing: Total allocated: %d",MemoryUsed()/1024);
	}

	//LogF("Buldo: loaded config");
	
	//Log("Config files parsed.");

	#if PROFILE_INIT
		EnableProfiler();
	#endif
	
  #ifdef NET_LOG_COMMAND_LINE
  netLogValid = false;
  #endif
	// scan flashpoint.par for arguments
	{
		ParamFile pars;
		pars.Parse(FlashpointPar);
		if( pars.GetEntryCount()>0 )
		{
			const ParamEntry &array=pars>>"Arg";
			for( int i=0; i<array.GetEntryCount(); i++ )
			{
				const ParamEntry &entry=array.GetEntry(i);
				RStringB arg=entry;
				//const char *argc=arg;
				SingleArgument(arg,arg+strlen(arg));
			}
		}
	}

	// scan command line for arguments (pass 2)
	args=szCmdLine;
	for( const char *s=szCmdLine; ; )
	{
		if( *s=='"' )
		{
			args=++s;
			// string argument
			while( *s && *s!='"' ) s++;
			// argument between args and s
			SingleArgument(args,s);
		}
		else if( *s==' ' )
		{
			SingleArgument(args,s);
			args=++s;
		}
		else if( *s==0 )
		{
			SingleArgument(args,s);
			break;
		}
		else s++;
	}

    #if !_SUPER_RELEASE && defined(NET_LOG) && !defined(_XBOX)
        if ( !UseSockets ) netLogValid = false; // force NetLog disable
    #endif
	
	#if _ENABLE_DEDICATED_SERVER
		// no GUI - it must be dedicated server
		if (DoCreateDedicatedServer)
		{
			UseGlide=false;
			EnableHWTL=false;
			GMergeTextures=false;
		}
	#endif

	if (EnablePIII)
	{
		//  set flush mode for PIII
		SetFlushToZero();
	}

	//Glob.diskAccessFree=CreateMutex(NULL,FALSE,NULL);
	if( LandEditor )
	{
		extern bool GReplaceProxies;
		//Glob.config.background=false;
		GUseFileBanks=false;
		GEnableCaching=false;
		GReplaceProxies = false;
	}
	else
	{
		GUseFileBanks = true;
		GMergeTextures = EnableHWTL;
		if (GMergeTextures) GFileBankPrefix = "HWTL"; // HW config dependent banks
	}

#if _ENABLE_DEDICATED_SERVER
	if (!DoCreateDedicatedServer)
#endif
	{
		InitGlide();
	}

	//LogF("Buldo: before Init");

	#if _ENABLE_VBS
		if (IsVBS())
		{
			// register known compression schemes
			IFilebankEncryption *CreateEncryptXOR1024(const void *context);
			RegisterFilebankEncryption("XOR1024",CreateEncryptXOR1024);

			IFilebankEncryption *CreateEncryptVBS1(const void *context);
			RegisterFilebankEncryption("VBS1",CreateEncryptVBS1);
		}
	#endif


	Glob.Init();
	InitMan();
	AI::InitTables();
	GStats.ClearAll();

	#if 0 // !_SUPER_RELEASE
		const char *q = "cfg/CfgAmmo/LAW/";
		int answer = IntegrityCheckAnswer(IQTConfig,IntegrityQuestion(q,0,0));
		LogF("Integrity check %s = %x",q,answer);
	#endif


	#if 0
		//SetPriorityClass(GetCurrentProcess(),HIGH_PRIORITY_CLASS);
		//SetProcessWorkingSetSize(GetCurrentProcess(),8*1024*1024,16*1024*1024);
	#endif

	
	//ShowWSSize();
	
	InitFPU();

  // version logging
#ifdef NET_LOG
  NetLog("OFP version: %s",APP_VERSION_TEXT);
#endif

	// Save instance handle for DialogBoxes
    #ifdef _WIN32
	hInstApp = hInst;
    #endif
    
	CreateEnginePars pars;
		// in
	pars.hInst=hInst;
	pars.hPrev=hPrev;
	pars.sw=sw;
	pars.w=Glob.config.wantW;
	pars.h=Glob.config.wantH;
	pars.bpp = Glob.config.wantBpp;
	pars.UseWindow=UseWindow;

	//LogF("Buldo: before Create Engine");


//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
	if (DoCreateDedicatedServer)
	{
		GDebugger.PauseCheckingAlive();
		CreateDedicatedServer(hInst,pars);
		//HWND AppInit(HINSTANCE hInst,HINSTANCE hPrev,int sw, bool fullscreen, int width, int height );
		//AppInit(hInst, hPrev, SW_HIDE, false, 10, 10);
	}
	else
#endif
//}

#if !_DISABLE_GUI
/*
	#ifndef _XBOX
	if (UseGlide)
	{
		GEngine=CreateEngineGlide(pars);
	}
	else
	#endif
*/
	{
		GEngine=CreateEngineD3D(pars);
		#if _XBOX
		AppWinCreate(hwndApp);
		#endif
	}
#endif

	// out
	HideCursor=pars.HideCursor;
	UseWindow=pars.UseWindow;
	NoRedrawWindow=pars.NoRedrawWindow;

	//LogF("Buldo: before Create World");

	GWorld=new World(GLOB_ENGINE,LandEditor);
	// start init progress
	GPreloadedTextures.Preload(true);

	if( !NoSplash && !LandEditor && !DoCreateServer && ClientIP.GetLength() == 0
//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
		&& !DoCreateDedicatedServer
#endif
		)
//}
	{
		// start loaging script
		GameValue val;
		ProgressScript = new Script("startup.sqs",val);
	}

	ProgressStart( LocalizeString(IDS_LOAD_INIT) );

	GLandscape=new Landscape(GEngine,GWorld,false);

	//LogF("Buldo: before InitWorld");

	I_AM_ALIVE();

	if( !LandEditor )
	{
		GWorld->InitLandscape(GLandscape);
		I_AM_ALIVE();
		VehicleTypes.Preload();
		// flush all banks, so that we can unload them if possible
		GFileServer->FlushBank(NULL);
		// we have already loaded all non-default types
		// unlock all non-default addons 
		//AddonToPrefix.ForEach(UnlockAddon);
	}

	InitWorld();
	I_AM_ALIVE();

	#if 0
	for (AddonInfoMap::Iterator iterator(AddonToPrefix); iterator; ++iterator)
	{
		const AddonInfo &info = *iterator;
		LogF("%s in %s",(const char *)info.GetName(),(const char *)info.GetPrefix());
	}
	#endif

#if _VERIFY_KEY
	keyOK = VerifyKey();
#else
	keyOK = true;
#endif

#if _VERIFY_KEY_EXT
	if (keyOK) keyOK = VerifyKeyExt();
#endif
	
	//ShowPic("Data2D\\TitlScr3.pcc",5000);

	//GEngine->TextBank()->Preload();
	//GEngine->TextBank()->ForceRelight();

	ProgressFinish();

	#if PROFILE_INIT
		DisableProfiler();
	#endif

	if( !keyOK )
	{
		ErrorMessage("Bad serial number given in Setup");
		exit(1);
	}

	#if PROFILE_RUN
		EnableProfiler();
	#endif

	CanRender=true;
	GLOB_ENGINE->SetTimeStartGame(GlobalTickCount());

//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
	if (DoCreateDedicatedServer)
	{
		bool CreateDedicatedServer(RString config);
#ifdef _WIN32
    CreateDedicatedServer(ServerConfig);
#else
		if ( CreateDedicatedServer(ServerConfig) )
#endif
      //TODO: allow some quit command from console
    DedicatedServerLoop();
	}
	else
#endif
//}
	if (DoCreateServer)
	{
		void __cdecl CDPCreateServer();
		CDPCreateServer();
	}
#if !_DISABLE_GUI
	else if (ClientIP.GetLength() > 0)
	{
		void __cdecl CDPCreateClient(RString ip, int port, RString password);
		CDPCreateClient(ClientIP, NetworkPort, NetworkPassword);
	}
	else if ( !LandEditor )
	{
		if (Benchmark)
		{
			strcpy(LoadFile,"Users\\Test\\Missions\\Benchmark.Abel\\mission.sqm");
		}
		GLOB_WORLD->StartIntro();
	}
#endif

	// once init is done, start watching thread
//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
	if (!DoCreateDedicatedServer)
#endif
//}
	{
		GDebugger.ResumeCheckingAlive();
	}
	// finish init progress
	while (ProgressScript)
	{
		I_AM_ALIVE();
		if (ProgressScript->IsTerminated())
		{
			ProgressScript.Free();
		}
	}
	GWorld->SetTitleEffect(NULL);
	GWorld->SetCutEffect(NULL);

	#if 0
		CalculateConfigCRC("");
		CalculateConfigCRC("cfg/CfgVehicles");
		CalculateConfigCRC("rsc/RadioProtocol");
		CalculateExeCRC(0,INT_MAX);
	#endif

	//LogF("Buldo: Running");
	// Polling messages from event queue until quit
	//MemoryFast();
	#ifdef _WIN32
	while( !CloseRequest )
	{
		ProcessMessages(false);
	}
	#endif
	//MemoryGood();
	
	#if PROFILE_RUN
		DisableProfiler();
	#endif

	ValidateQuit=true;
	LogF("Shutdown");
	#if defined _XBOX || !defined _WIN32
		DDTerm();
		#if PROFILE_EXIT
			DisableProfiler();
		#endif
		exit(0);
	#else
	PostDestroy();
	for(;;)
	{
		ProcessMessages(false);
	}
	#endif
	#if _MSC_VER
		#ifndef _INTEL_CC
			LogF("WinMain return 0");
			return 0;
		#endif
	#endif
}

// Public key from private CD key

RString GetPublicKey()
{
#if _VERIFY_KEY
	__int64 val = GCDKey.GetValue(0, 5);
	char buffer[256];
	return _i64toa(val, buffer, 10);
#else
	return "999";
#endif
}


#include "appFrameExt.hpp"

// MessageBox does not work when in exclusive mode

/*!
\patch 1.23 Date 9/11/2001 by Ondra
- New: context.bin create when game terminates with error message box.
\patch 1.28 Date 10/18/2001 by Ondra
- New: Error messages logged to Flashpoint.rpt.
*/

void OFPFrameFunctions::ErrorMessage(const char *format, va_list argptr)
{
	static int avoidRecursion=0;
	if( avoidRecursion++!=0 ) return;

	GDebugger.NextAliveExpected(15*60*1000);

	BString<256> buf;
	vsprintf( buf, format, argptr );

	// kill direct draw
	#if _ENABLE_REPORT
		::RptF("ErrorMessage: %s",(const char *)buf);
	#else
	  #ifdef _WIN32
		GDebugExceptionTrap.LogLine("ErrorMessage: %s",buf.cstr());
		GDebugExceptionTrap.SaveContext();
	  #else
		// !!! not yet !!!
	  #endif
	#endif
	#if defined _WIN32 && !defined _XBOX
	if( hwndApp ) DestroyWindow(hwndApp);
	#endif
	DDTerm();
	// kill main application window
	//MessageBox(hwndApp,buf,AppName,MB_OK);
	//WinDestroy();
	// show message
    #ifdef _WIN32
	#ifndef _XBOX
	if( HideCursor ) ShowCursor(TRUE);
	MessageBox(NULL,buf,AppName,MB_OK|MB_ICONERROR);
	#endif
    #else
    fputs(buf,stderr);
    fputc('\n',stderr);
    #endif
	// terminate
	#if _ENABLE_REPORT
		::LogF("Exit Error");
	#endif
	exit(1);
}

#define ENABLE_WARNINGS 1
#define SINGLE_WARNING 1

static ErrorMessageLevel WarningLevel = EMNote;
static RString WarningText;

ErrorMessageLevel GetMaxError()
{
	return WarningLevel;
}

RString GetMaxErrorMessage()
{
	return WarningText;
}

/*!
\patch 1.50 Date 4/11/2002 by Ondra
- Change: First warning during each mission is shown.
Only first warning after game was launched was shown before.
*/

void ResetErrors()
{
	WarningLevel = EMNote;
	WarningText = RString();
}

/*!
	\patch_internal 1.01 Date 06/21/2001 by Jirka
	- reimplementation of Warning message
	- if GWorld exist, display ingame dialog with warning
	- otherwise, windows Message Box is performed
*/

void WarningMessageLevel( ErrorMessageLevel level, const char *format, va_list argptr)
{
	#if SINGLE_WARNING
		if( WarningLevel>=level ) return;
		WarningLevel = (ErrorMessageLevel)(level+1);
	#endif

	static int avoidRecursion = 0;
	if (avoidRecursion> 0) return;

	avoidRecursion++;
	GDebugger.NextAliveExpected(15*60*1000);

	BString<256> buf;
	vsprintf( buf, format, argptr );

	#if SINGLE_WARNING
		WarningText = buf.cstr();
	#endif

	#if _ENABLE_REPORT
		::ErrF("Warning Message: %s",(const char *)buf);
	#endif

	// CHANGED in Patch 1.01
	if (Benchmark)
	{
		avoidRecursion--;
		return;
	}
	bool result = false;
	// CHANGED in Patch 1.01
	if (GWorld)
	{
		GWorld->CreateWarningMessage(buf.cstr());
		result = true;
	}
	
	// kill main application window
	if (!result)
	{
        #ifdef _WIN32
		#ifndef _XBOX
		if( HideCursor ) ShowCursor(TRUE);
		DWORD icon = level<=EMWarning ? MB_ICONWARNING : MB_ICONERROR;
		MessageBox(NULL,buf,AppName,MB_OK|icon);
		if( HideCursor ) ShowCursor(FALSE);
		#endif
        #else
        puts(buf);
        #endif
	}
	// terminate
	avoidRecursion--;
}

void EnableDesktopCursor(bool enable)
{
#ifdef _WIN32
	if (HideCursor) ShowCursor(enable);
#endif
}

void OFPFrameFunctions::WarningMessage( const char *format, va_list argptr)
{
	WarningMessageLevel(EMWarning,format,argptr);
}

/*!
\patch 1.50 Date 4/11/2002 by Ondra
- Changed: Error "No entry in config" no longer exits game.
This makes mission editing easier in case of misspelled class names.
*/

void OFPFrameFunctions::ErrorMessage(ErrorMessageLevel level, const char *format, va_list argptr)
{
	if (level<=EMError)
	{
		WarningMessageLevel(level,format,argptr);
	}
	else
	{
		ErrorMessage(format,argptr);
	}
}


#if _ENABLE_REPORT

void OFPFrameFunctions::LogF( const char *format, va_list argptr)
{
	BString<512> buf;
	vsprintf(buf,format,argptr);

	strcat(buf,"\n");
	// assume someone will catch it
	//if (GDebugger.IsDebugger())
	#ifdef _WIN32
	OutputDebugString(buf);
	#endif
	#if 1 //_RELEASE
		// always write to debug log
		static bool open = false;
		static LogFile file;
		if (!open || ResetLogFile)
		{
			file.Open(LogFilename);
			open = file.IsOpen();
			ResetLogFile = false;
		}
		if (file.IsOpen()) 
			file.PrintF("%8.3f: %s",GlobalTickCount()*0.001,buf.cstr());
	#endif
}

void OFPFrameFunctions::ErrF( const char *format, va_list argptr)
{
	BString<512> buf;
	vsprintf(buf,format,argptr);

	#if 1
	if (GDebugger.IsDebugger())
	{
/*
		strcat(buf,"\n");
		OutputDebugString(buf);
*/
		::LogF("%s",(const char *)buf);
        #ifdef _WIN32
		__asm int 3;
        #endif
	}
	else
	#endif
	{
		#if _RELEASE
          #ifdef _WIN32
			DebugExceptionTrap::LogFFF(buf);
          #else
            fputs(buf,stderr);
            fputc('\n',stderr);
          #endif
		#endif
	}
}

void OFPFrameFunctions::LstF( const char *format, va_list argptr)
{
	BString<512> buf;
	vsprintf(buf,format,argptr);

	#if _RELEASE
      #ifdef _WIN32
		DebugExceptionTrap::LogFFF(buf,false);
      #else
        fputs(buf,stderr);
        fputc('\n',stderr);
      #endif
	#endif

	::LogF("%s",buf.cstr());
}	

#endif	// of ENABLE_REPORT

#ifdef _WIN32
VOID ServiceStart(DWORD dwArgc, LPTSTR *lpszArgv) {}
VOID ServiceStop() {}
#endif

extern "C" void *_ogg_malloc(size_t size)
{
  return malloc(size);
}

extern "C" void *_ogg_calloc(size_t num, size_t size)
{
  return calloc(num,size);
}

extern "C" void *_ogg_realloc(void *memblock, size_t size)
{
  return realloc(memblock,size);
}

extern "C" void _ogg_free(void *mem)
{
  free(mem);
}
