#ifdef _MSC_VER
#pragma once
#endif

#ifndef _SOLDIER_OLD_HPP
#define _SOLDIER_OLD_HPP

/*!
\file
Interface file for Man and Soldier classes
*/

#include "person.hpp"
#include "animation.hpp"
#include "ai.hpp"
#include "head.hpp"

#include "dynEnum.hpp"

//! fictious enum - only in order to have typechecking
DEFINE_ENUM_BEG(MoveId)
	MoveIdNone=-1
DEFINE_ENUM_END(MoveId)


class MotionType;


#include "rtAnimation.hpp"
#include "envTracker.hpp"

struct MotionPathItem
{
	MoveId id;
	Ref<ActionContextBase> context;

	MotionPathItem(MoveId i, ActionContextBase *c=NULL)
	{id = i; context = c;}
};
TypeIsMovableZeroed(MotionPathItem)

class MotionPath: public AutoArray<MotionPathItem>
{
	public:
	MotionPath();
	~MotionPath();
};


#include "manActs.hpp"

#include <Es/Memory/normalNew.hpp>

struct ActionMapName
{
	const ParamEntry *entry;
	const MotionType *motion;
};

//! Motion capture - normal actions
/*!
define list of actions corresponding to normal moves
*/
class ActionMap: public RefCount
{
	MoveId _actions[ManActN];
	ActionMapName _name;

	// min is most combat
	// max is most safe
	int _upDegree;
	float _turnSpeed;
	float _limitFast;

	public:
	ActionMap( const ActionMapName &name );
	const ActionMapName &GetName() const {return _name;}
	MoveId GetAction( ManAction act ) const {return _actions[act];}
	int GetUpDegree() const {return _upDegree;}
	float GetTurnSpeed() const {return _turnSpeed;}
	float GetLimitFast() const {return _limitFast;}

	USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

//! Motion capture - vehicle related actions
/*!
define list of actions correspond to different vehicle crews 
*/

class ActionVehMap
{
	AutoArray<MoveId> _actionMoves;

	public:
	ActionVehMap();
	void Load(const MotionType *motion, const ParamEntry &map);
	int GetMaxAction() const {return _actionMoves.Size();}
	MoveId GetAction( ManVehAction act ) const {return _actionMoves[act];}
};


template<>
struct BankTraits<ActionMap>
{
	typedef const ActionMapName &NameType;
	static int CompareNames( NameType n1, NameType n2 )
	{
		// compare pointers
		if (n1.entry!=n2.entry) return 1;
		if (n1.motion!=n2.motion) return 1;
		return 0;
	}
	// default container is RefArray
	typedef RefArray<ActionMap> ContainerType;
};

typedef MemAllocSA BlendAnimSelectionsStorage;

class BlendAnimSelections: public StaticArrayAuto<BlendAnimInfo>
{
	public:
	BlendAnimSelections();
	void Load( Skeleton *skelet, const ParamEntry &cfg );
	void AddOther( const BlendAnimSelections &src, float factor );
};

struct BlendAnimTypeName
{
	// MovesType is defined by pair: LODShapeWithShadow and MotionType
	const MotionType *motion;
	const ParamEntry *cfg;
};

class BlendAnimType: public RemoveLinks, public BlendAnimSelections
{
	BlendAnimTypeName _name;

	public:
	BlendAnimType(const BlendAnimTypeName &name);
	const BlendAnimTypeName &GetName() {return _name;}
};

template <>
struct BankTraits<BlendAnimType>
{
	// default name is character
	typedef const BlendAnimTypeName &NameType;
	// default name comparison
	static int CompareNames( BlendAnimTypeName n1, BlendAnimTypeName n2 )
	{
		return n1.motion!=n2.motion || n1.cfg!=n2.cfg;
	}
	// default container is RefArray
	typedef LinkArray<BlendAnimType> ContainerType;
};

typedef BankArray<BlendAnimType> BlendAnimTypes;
extern BlendAnimTypes GBlendAnimTypes;

struct MoveVariant
{
	MoveId _move;
	float _probab;
};

TypeIsSimple(MoveVariant)

class MoveInfo
{
	Ref<AnimationRT> _move;
	Ref<ActionMap> _actions;
	MotionType *_motion;
	bool _disableWeapons;
	bool _enableOptics;
	bool _disableWeaponsLong;
	bool _showWeaponAim;
	bool _onLandBeg,_onLandEnd; // copy surface direction
	bool _enableMissile;
	bool _enableBinocular;
	bool _enableFistStroke;
	bool _enableGunStroke;
	bool _showItemInHand;
	bool _showItemInRightHand;
	bool _showHandGun;
	bool _terminal;
	bool _onLadder;

	float _speed;
	float _duty;
	float _relSpeedMin,_relSpeedMax;
	float _interpolSpeed;
	float _visibleSize;
	float _aimPrecision;

	bool _soundEnabled;
	bool _interpolRestart;

	RStringB _soundOverride; // default - no override
	float _soundEdge1,_soundEdge2; // in what relative positions sound should start

	//BlendAnimSelections _aiming;
	//BlendAnimSelections _legs;
	//BlendAnimSelections _head;
	Ref<BlendAnimType> _aiming;
	Ref<BlendAnimType> _legs;
	Ref<BlendAnimType> _head;
	float _limitGunMovement;

	AutoArray<MoveVariant> _variantsPlayer; // random variants
	AutoArray<MoveVariant> _variantsAI;
	MoveId _equivalentTo;
	float _variantAfterMin; // how long should we wait before playing variant
	float _variantAfterMid;
	float _variantAfterMax;

	public:
	MoveInfo( MotionType *motion, const ParamEntry &entry );

	__forceinline operator AnimationRT *() const {return _move;}
	__forceinline AnimationRT *operator ->() const {return _move;}
	__forceinline ActionMap *GetActionMap() const {return _actions;}
	__forceinline bool WeaponsDisabled() const {return _disableWeapons;}	
	__forceinline bool EnableOptics() const {return _enableOptics;}
	__forceinline bool DisableWeaponsLong() const {return _disableWeaponsLong;}
	__forceinline bool ShowWeaponAim() const {return _showWeaponAim;}
	__forceinline bool MissileEnabled() const {return _enableMissile;}
	__forceinline bool EnableBinocular() const {return _enableBinocular;}
	__forceinline bool EnableGunStroke() const {return _enableGunStroke;}
	__forceinline bool EnableFistStroke() const {return _enableFistStroke;}
	__forceinline bool ShowItemInHand() const {return _showItemInHand;}
	__forceinline bool ShowItemInRightHand() const {return _showItemInRightHand;}
	__forceinline bool ShowHandGun() const {return _showHandGun;}

	__forceinline bool GetTerminal() const {return _terminal;}

	__forceinline bool OnLadder() const {return _onLadder;}
	
	__forceinline bool OnLandBeg() const {return _onLandBeg;}
	__forceinline bool OnLandEnd() const {return _onLandEnd;}
	float OnLand( float time ) const {return _onLandBeg*(1-time)+_onLandEnd*time;}
	__forceinline float GetSpeed() const {return _speed;}
	__forceinline float GetDuty() const {return _duty;}
	__forceinline float GetRelSpeedMin() const {return _relSpeedMin;}
	__forceinline float GetRelSpeedMax() const {return _relSpeedMax;}
	__forceinline bool GetSoundEnabled() const {return _soundEnabled;}

	__forceinline float GetSoundEdge1() const {return _soundEdge1;}
	__forceinline float GetSoundEdge2() const {return _soundEdge2;}
	__forceinline const RStringB &GetSoundOverride() const {return _soundOverride;}

	const BlendAnimSelections &GetAiming() const {return *_aiming;}
	const BlendAnimSelections &GetLegs() const {return *_legs;}
	const BlendAnimSelections &GetHead() const {return *_head;}

	__forceinline float GetInterpolSpeed() const {return _interpolSpeed;}
	__forceinline bool GetInterpolRestart() const {return _interpolRestart;}
	__forceinline float GetLimitGunMovement() const {return _limitGunMovement;}

	__forceinline float GetVisibleSize() const {return _visibleSize;}
	__forceinline float GetAimPrecision() const {return _aimPrecision;}

	const AutoArray<MoveVariant> GetVariantsPlayer() const {return _variantsPlayer;}
	const AutoArray<MoveVariant> GetVariantsAI() const {return _variantsAI;}

	MoveId RandomVariant( const AutoArray<MoveVariant> &vars, float rnd ) const;
	MoveId RandomVariantPlayer() const;
	MoveId RandomVariantAI() const;
	void LoadVariants
	(
		AutoArray<MoveVariant> &vars, AutoArray<MoveVariant> *defaultVars,
		const ParamEntry &cfg
	) const;

	MoveId GetEquivalentTo() const {return _equivalentTo;}

	float GetVariantAfter() const;

};

TypeIsMovableZeroed(MoveInfo);

enum MotionEdgeType
{
	MEdgeNone,
	MEdgeSimple,
	MEdgeInterpol
};


struct MotionEdge
{
	short target;
	char cost; // <0 means infinity
	SizedEnum<MotionEdgeType,char> type;

	MotionEdge(){target=-1,cost=-1,type=MEdgeNone;}
	MotionEdge( int t, int c, MotionEdgeType p){target=t,cost=c,type=p;}
	float GetCost() const {return cost*(1.0f/50);}
};

TypeIsSimple(MotionEdge)

typedef AutoArray<MotionEdge> MotionEdges;

class MotionType
{
	DynEnum _moveIds;
	BankArray<ActionMap> _actionMaps;
	BankArray<BlendAnimType> _blendAnimTypes;

	//SRef<MotionMatrix> _motionMatrix;
	AutoArray<MotionEdges> _vertex;
	Ref<Skeleton> _skeleton;

	ActionVehMap _actionVehMap;


	//
	Ref<ActionMap> _noActions; // always have some actions ready

	const ParamEntry *_entry;

	public:
	MotionType();
	~MotionType();
	const RStringB &GetEntryName() const {return _entry->GetName();}

	void Load( const ParamEntry &entry );
	void Unload();
	const ParamEntry &GetEntry() const {return *_entry;}

	const EnumName *GetMoveIdNames() const {return _moveIds.GetEnumNames();}
	RStringB GetMoveName( MoveId id ) const;
	MoveId GetMoveId( RStringB name ) const;
	int MoveIdN() const {return _moveIds.FirstInvalidValue();}

	MoveId GetDefaultMove( int upDegree ) const;
	MoveId GetMove(int upDegree, ManAction action) const;

	void InitNoActions(const ParamEntry *cfg);

	const ActionVehMap &GetActionVehMap() const {return _actionVehMap;}
	ActionMap *NewActionMap(const ParamEntry *cfg);
	BlendAnimType *NewBlendAnimType(const ParamEntry &cfg);
	Skeleton *GetSkeleton() const {return _skeleton;}
	void AssignSkeleton( RStringB name );

	ActionMap *GetNoActions() const {return _noActions;}


	int EdgeCost( MoveId a, MoveId b ) const;
	const MotionEdge &Edge( MoveId a, MoveId b ) const;
	void AddEdge(MoveId a, MoveId b, MotionEdgeType type, float cost);
	void DeleteEdge(MoveId a, MoveId b);

	bool FindPath(MotionPath &path, MoveId from, MotionPathItem to) const;

};

struct MovesTypeName
{
	// MovesType is defined by pair: LODShapeWithShadow and MotionType
	LODShapeWithShadow *shape;
	MotionType *motionType;
};


class MovesType: public RefCountWithLinks
{
	MovesTypeName _name; // shape and motionType
	
	AutoArray<MoveInfo> _moves;
	Ref<WeightInfo> _weights; // rt animation weighting info	

	public:
	MovesType( const MovesTypeName &name );
	~MovesType();
	const MovesTypeName &GetName() const {return _name;}
	LODShapeWithShadow *GetShape() const {return _name.shape;}

	Skeleton *GetSkeleton() const {return _name.motionType->GetSkeleton();}

	WeightInfo &GetWeights() const {return *_weights;} // rt animation weighting info
	AnimationRT *GetAnimation( MoveId move ) const;
	ActionMap *GetActionMap( MoveId move ) const;
	const MoveInfo *GetMoveInfo( MoveId move ) const;

	const ActionVehMap &GetActionVehMap() const {return _name.motionType->GetActionVehMap();}

	MoveId GetEquivalent( MoveId move ) const;
};

template <>
struct BankTraits<MovesType>
{
	// default name is character
	typedef const MovesTypeName &NameType;
	// default name comparison
	static int CompareNames( NameType n1, NameType n2 )
	{
		return n1.shape!=n2.shape || n1.motionType!=n2.motionType;
	}
	// default container is RefArray
	typedef LinkArray<MovesType> ContainerType;
};

typedef BankArray<MovesType> MovesTypeBank;
extern MovesTypeBank MovesTypes;

#include "wounds.hpp"

class ManType: public EntityAIType, public MotionType
{
	typedef EntityAIType base;
	friend class Man;
	friend class Soldier;

	protected:

	Vector3 _lightPos,_lightDir;
	
	int _gunPosIndex,_gunEndIndex; // index in memory LOD
	//Animation _gunFire;

	bool _isMan; // some properties have no sense for "virtual man" (like horse)
	int _gunMatIndex; // gun matrix index
	int _rpgMatIndex; // gun matrix index
	int _handMatIndex;	// matrix index for item in hand
	int _rightHandMatIndex;	// matrix index for item in hand

	int _stepLIndex,_stepRIndex; // index in memory LOD

	float _minGunElev,_maxGunElev; // gun movement
	float _minGunTurn,_maxGunTurn;
	float _minGunTurnAI,_maxGunTurnAI;

	float _minHeadTurnAI,_maxHeadTurnAI;
	
	float _minTriedrElev,_maxTriedrElev; // gun movement
	float _minTriedrTurn,_maxTriedrTurn;

	RandomSound _hitSound;
	SoundPars _addSound;

	//MotionType _motion;
	bool _canHideBodies;
	bool _canDeactivateMines;

	//! woman model
	bool _woman;
	
	float _sideStepSpeed;
	int _insideView;
	int _gunnerView;
	//int _layDownView;
	//int _crawlView;
	//int _launcherView;

	int _priWeaponIndex; // index of corresponding proxy
	int _secWeaponIndex;
	int _handGunIndex;

	int _pilotPoint;
	int _aimingAxisPoint;
	int _headAxisPoint;
	int _cameraPoint;

	int _nvGogglesProxyIndex[MAX_LOD_LEVELS];
	//AnimationSection _nvGoggles;

	//AnimationWithCenter _gun; // aiming selection
	//AnimationWithCenter _bazooka; // aiming selection
	
	int _aimPoint;
	Vector3 _aimPoint0;
	Ref<MovesType> _moveType;

	AnimationRTWeight _headOnlyWeight;

	AnimationSection _headHide,_neckHide; // hiding in vehicle interior

	WoundTextureSelections _headWound;
	WoundTextureSelections _bodyWound;
	WoundTextureSelections _lArmWound;
	WoundTextureSelections _rArmWound;
	WoundTextureSelections _lLegWound;
	WoundTextureSelections _rLegWound;

	HitPoint _headHit;
	HitPoint _bodyHit,_handsHit;
	HitPoint _legsHit;

	HeadType _head;

	public:
	ManType( const ParamEntry *param );
	virtual void Load(const ParamEntry &par);
	void InitShape();
	void DeinitShape();

	WeightInfo &GetWeights() const {return _moveType->GetWeights();}
	AnimationRT *GetAnimation( MoveId move ) const {return _moveType->GetAnimation(move);}
	ActionMap *GetActionMap( MoveId move ) const;
	const MoveInfo *GetMoveInfo( MoveId move ) const {return _moveType->GetMoveInfo(move);}

};


#include "smokes.hpp"

// transition is considered as animation state, transition factor is 
struct ManAnimState
{
	MoveId anim; // which animation
	float time; // time (0..1) - may be looped or clamped depending on anim
	//float transTime; // transition time (transition speed given by anim)
	//float transSpeed; // positive -> fade in, negative -> fade out
};

enum ManPos
{
	ManPosDead,
	ManPosWeapon, // special weapon - AT
	ManPosBinocLying,
	ManPosLyingNoWeapon,
	ManPosLying,
	ManPosHandGunLying,
	ManPosCrouch,
	ManPosHandGunCrouch,
	ManPosCombat,
	ManPosHandGunStand,
	ManPosStand, // moves with weapon on the back
	ManPosNoWeapon, // civilian moves
	ManPosBinoc, // binocular position
	ManPosBinocStand, // binocular position (weapon on back)

	ManPosNormalMin = ManPosLyingNoWeapon,
	ManPosNormalMax = ManPosNoWeapon
};


typedef bool (MoveInfo::*TestEnable)() const;
typedef float (MoveInfo::*TestValue)() const;
typedef float (MoveInfo::*TestValueTimed)(float time) const;

//typedef bool (Man::CheckActionF)(void *context);

class Building;

//! Man class - simulation of all men
/*!
	Man implements all functionality specific to men.
	Ideally all military related functions should be in Soldier,
	but it is not implemented that way and most such functions
	are also implemented in Man.
	\todo Either merge Man and Soldier or move military functionality to Soldier.
*/

class Man: public Person
{
	typedef Person base;
	protected:

	float _gunYRot,_gunYRotWanted;
	float _gunXRot,_gunXRotWanted;
	float _gunXSpeed,_gunYSpeed;

	float _headYRot,_headYRotWanted;
	float _headXRot,_headXRotWanted;
	//float _headXSpeed,_headYSpeed;

	// periodically change between looking forward and looking at the target
	float _lookForwardTimeLeft;
	float _lookTargetTimeLeft;

	float _correctBankSin; // apply to gun / head to correct slope
	float _correctBankCos; // apply to gun / head to correct slope
	Matrix4 _headTrans; // head movement transformation
	Matrix4 _gunTrans; // gun aiming transformation
	Matrix4 _legTrans; // legs slope adjustment transformation
	bool _headTransIdent,_gunTransIdent; // optimize matrix combination

	Vector3 _aimingPositionWorld; // world space aiming position
	Vector3 _cameraPositionWorld; // world space aiming position
	// this is very often queried, but quite costly to calculate

	float _manScale; // TODO: use _scale of Object

	float _hideBody, _hideBodyWanted; // hide body state

	void RecalcGunTransform(); // calculate _gunTrans from gunXRot ...

	Ref<WeaponType> _mGunFireWeapon;
	int _mGunFireFrames;
	UITime _mGunFireTime;
	int _mGunFirePhase;
	WeaponLightSource _mGunFire;
	WeaponCloudsSource _mGunClouds;
	WeaponCloudsSource _gunClouds;

	Ref<AbstractWave> _soundStep;
	Ref<AbstractWave> _soundBreath;
	Ref<AbstractWave> _soundEnv;

	OLink<EntityAI> _flagCarrier;

	OLinkArray<Vehicle> _pipeBombs;

	bool _doSoundStep;
	bool _canMoveFast;

	bool _nvg;			//!< using Night Vision Goggles
	bool _hasNVG; //!< have Night Vision Goggles
	bool _walkToggle; //!< Manual control - walk wanted
	bool _inBuilding; //!< AI control - last path planned in building
	
	bool _handGun;

	float _tired;

	RStringB _soundStepOverride;
	Time _freeFallUntil;

	float _aimInaccuracyX,_aimInaccuracyY;
	Time _lastInaccuracyTime;
	float _aimInaccuracyDist;
	Time _lastInaccuracyDistTime;

	float _waterDepth;

	Time _whenKilled; // helpers for suspending dead body after a while
	Time _lastMovementTime;
	Time _lastObjectContactTime;
	Time _whenScreamed;
	//float _launchDelay;
	MoveId _stillMoveQueueEnd;
	Time _variantTime; // when we shoudl switch variant

	// current state is given by:
	MotionPathItem _primaryMove;
	MotionPathItem _secondaryMove;

	MotionPathItem _forceMove; // do not do anything else until this move is completed

	MotionPath _externalQueue;
	MotionPathItem _externalMove; // some external move is forced

	bool _externalMoveFinished;
	bool _showPrimaryWeapon;
	bool _showSecondaryWeapon;
	bool _showHead;
	
	// variables used while climing ladder
	//OLink<Object> _ladderBuilding;
	OLink<Building> _ladderBuilding;
	int _ladderIndex;
	float _ladderPosition; // 0..1 - position on ladder, 0 = bottom
	int _ladderAIDir; // 

	int GetActUpDegree() const; // helper function

	Time _upDegreeChangeTime; // time of last change
	// changes are tracked whenever _primaryMove changes
	int _upDegreeStable; // stable upDegree (must last at least 10 sec)

	ManPos _posWanted; // delayed position change
	Time _posWantedTime;

	MotionPath _queueMove; // shortest "path" to desired move
	// desired move is last member of _queueMove
	float _primaryFactor; // interpolation btw. primary and secondary
	float _primaryTime,_secondaryTime;

	void SetPrimaryMove(MotionPathItem item); 
	bool ChangeMoveQueue(MotionPathItem item);
	bool SetMoveQueue(MotionPathItem item, bool enableVariants);
	void RefreshMoveQueue(bool enableVariants); // select variants

	void NextExternalQueue();
	void AdvanceExternalQueue();
	void GetRelSpeedRange( float &speedZ, float &minSpd, float &maxSpd );
	// return true if anything has changed
	bool AdvanceMoveQueue
	(
		float deltaT, float adjustSpeed, float &moveX, float &moveZ,
		SimulationImportance prec
	);

	/*!	
	\patch_internal 1.06 Date 7/17/2001 by Ondra.
	- Removed: unused obsolete speed control members in Man
	*/

	//float _rndSpeed;
	//float _walkSpeed
	float _walkSpeedWanted;
	//float _sideSpeed,_sideSpeedWanted;

	float _turnWanted;
	float _turnToDo; // how much turn is reqired from mouse movement

	mutable SurroundTracker _surround;

	UnitPosition _unitPos;

	Head _head;

	public:
	Man(VehicleType *name, bool fullCreate=true);
	~Man();

	
	const ManType *Type() const
	{
		return static_cast<const ManType *>(GetType());
	}

	float GetCombatHeight() const {return 0;}
	int GetFaceAnimation() const {return _head.GetFaceAnimation();}
	void SetFaceAnimation(int phase) {_head.SetFaceAnimation(phase);}
	void SetFace(RString name, RString player = "");
	void SetGlasses(RString name);
	void SetMimic(RStringB name) {_head.SetForceMimic(name);}

	virtual bool IsWoman() const {return Type()->_woman;}

	void AttachWave(AbstractWave *wave, float freq = 1.0f);
	void SetRandomLip(bool set = true); 
	float GetSpeaking() const;

	void HideBody() {_hideBodyWanted = 1;}
	void ScanNVG(); //!< Check if we have night vission (set _hasNVG flag)

	void AddDefaultWeapons();
	void MinimalWeapons();

	virtual void OnWeaponAdded();
	virtual void OnWeaponRemoved();
	virtual void OnWeaponChanged();
	virtual void OnDanger();

	void Init( Matrix4Par pos );

	void DrawDiags();
	RString DiagText() const;
	int GetAutoUpDegree() const; // based on unit combat mode
	MoveId GetDefaultMove() const; // based on unit combat mode
	MoveId GetDefaultMove(ManAction action) const; // based on unit combat mode
	MoveId GetMove(ManAction action) const; // based on actual move
	MoveId GetVehMove(ManVehAction action) const; // based on actual move

	Texture *GetCursorTexture(Person *person);
	Texture *GetCursorAimTexture(Person *person);
	Texture *GetFlagTexture();
	EntityAI *GetFlagCarrier() {return _flagCarrier;}
	void SetFlagCarrier(EntityAI *veh);
	void SetFlagOwner(Person *veh);

	virtual RString GetActionName(const UIAction &action);
	virtual void PerformAction(const UIAction &action, AIUnit *unit);
	void GetActions(UIActions &actions, AIUnit *unit, bool now);

	bool Supply(EntityAI *vehicle, UIActionType action, int param, int param2, RString param3);

	void ThrowGrenadeAction(int weapon);
	void ProcessUIAction(const UIAction &action);

	void ProcessMoveFunction( ActionContextBase *context );

	bool IsNVEnabled() const;
	bool IsNVWanted() const;
	void SetNVWanted(bool set = true);

	bool IsHandGunSelected() const {return _handGun;}
	void SelectHandGun(bool set = true) {_handGun = set;}

	bool CastProxyShadow(int level, int index) const;
	int GetProxyComplexity
	(
		int level, const FrameBase &pos, float dist2
	) const;
	void DrawProxies
	(
		int level, ClipFlags clipFlags,
		const Matrix4 &transform, const Matrix4 &invTransform,
		float dist2, float z2, const LightList &lights
	);
	Object *GetProxy
	(
		LODShapeWithShadow *&shape,
		int level,
		Matrix4 &transform, Matrix4 &invTransform,
		const FrameBase &parentPos, int i
	) const;


	void DrawNVOptics();
	void DrawCameraCockpit();

	int PassNum( int lod );
	void Draw( int level, ClipFlags clipFlags, const FrameBase &pos );
	#if ALPHA_SPLIT
	void DrawAlpha( int level, ClipFlags clipFlags, const FrameBase &pos );
	#endif

	bool IsAnimated( int level ) const; // appearence changed with Animate
	bool IsAnimatedShadow( int level ) const; // shadow changed with Animate
	void Animate( int level );
	void Deanimate( int level );

	void AnimatedMinMax( int level, Vector3 *minMax );
	void AnimatedBSphere( int level, Vector3 &bCenter, float &bRadius, bool isAnimated );

	void BasicAnimation( int level );
	void BasicDeanimation( int level );

	void WoundsAnimation( int level );
	void WoundsDeanimation( int level );

	float LandSlope(bool &forceStand) const;
	bool IsAbleToStand() const;
	bool IsAbleToFire() const;

	float GetHandsHit() const {return GetHit(Type()->_handsHit);}

	bool GetForceOptics(Person *person) const;
	LODShapeWithShadow *GetOpticsModel(Person *person);

	void ShowHead(int level, bool show = true);
	void ShowWeapons(bool showPrimary = true, bool showSecondary = true);

	protected:
	//!helper function to all Switch... motion related functions
	void SwitchMove(MoveId id, ActionContextBase *context);

	void SelectPrimaryWeapon();
	void SelectHandGunWeapon();

	public:

	// implementation of functions from EntityAI interface
	virtual RString GetCurrentMove() const;
	virtual void PlayMove(RStringB move, ActionContextBase *context=NULL);
	virtual void SwitchMove(RStringB move, ActionContextBase *context=NULL);

	//virtual void SwitchMove( RStringB move, ActionContextBase *context);

	// implementation of functions from Person interface
	virtual bool PlayAction(ManAction action, ActionContextBase *context=NULL);
	virtual bool SwitchAction(ManAction action, ActionContextBase *context=NULL);
	virtual void SwitchVehicleAction(ManVehAction action);

	bool CheckActionProcessing(UIActionType action, AIUnit *unit) const;
	void StartActionProcessing(const UIAction &action, AIUnit *unit);

	// some EntityAI interface implementations
	bool IsActionInProgress(MoveFinishF action) const;
	bool EnableWeaponManipulation() const;
	bool EnableViewThroughOptics() const;

	bool ReloadMagazine(int slotIndex, int iMagazine);

	// more functions
	void ApplyAnimation( int level, RStringB move, float time );
	void ApplyDeanimation( int level );
	float GetAnimSpeed(RStringB move);

	Vector3 GetPilotPosition(CameraType camType) const;

	UnitPosition GetUnitPosition() const;
	void SetUnitPosition(UnitPosition status);

	void Destroy( EntityAI *owner, float overkill, float minExp, float maxExp );
	void HitBy(EntityAI *owner, float howMuch, RString ammo);
	void Scream(EntityAI *killer);
	void ShowDammage(int part);

	void KilledBy( EntityAI *owner );
	void SetDammage(float dammage);
	void ReactToDammage();

	float NeedsAmbulance() const;
	float NeedsRepair() const;
	float NeedsRefuel() const;
	float NeedsInfantryRearm() const;

	void ResetLauncher();
	const WeaponModeType *GetCurrentWeaponMode() const;
	bool LauncherReady() const;
	bool LauncherSelected() const;
	bool LauncherWanted() const;
	bool LaserSelected() const;
	bool BinocularSelected() const;
//	bool HandGunSelected() const;
	bool LauncherFire() const; // AI is firing
	void Simulate( float deltaT, SimulationImportance prec );
	void BasicSimulationCore( float deltaT, SimulationImportance prec ); // also inside vehicle
	void BasicSimulation( float deltaT, SimulationImportance prec, float speedFactor ); // also inside vehicle
	float GetLegPhase() const;

	bool SimulateAnimations
	(
		float &turn, float &moveX, float &moveZ, float deltaT,
		SimulationImportance prec
	);

	void PlaceOnSurface(Matrix4 &trans);

	bool VerifyStructure() const;

	// perform actual catch/drop ladder
	void SimLadderMovement(float deltaT, SimulationImportance prec);

	void CatchLadder(Building *obj, int ladder, bool up);
	void DropLadder(Building *obj, int ladder);
	bool IsOnLadder(Building *obj, int ladder) const;

	void AutoGuide( float deltaT, bool brake=true, bool avoid=true ); // avoid obstacles

	void Sound( bool inside, float deltaT );
	void UnloadSound();

	float Rigid() const;
	bool HasGeometry() const;
	//bool OcclusionFire() const {return true;} // occlusion for fire 
	bool OcclusionView() const {return false;}

	Matrix4Val LegTransform() const {return _legTrans;}
	Matrix4Val GunTransform() const {return _gunTrans;}
	
	void RecalcPositions(const Frame &pos);
  bool MoveHead(float deltaT);
	void MoveWeapons( float deltaT, bool forceRecalcMatrix );
	//void MoveWeaponsFar( float deltaT );
	void OnPositionChanged();

	float FireAngleInRange( int weapon, Vector3Par rel ) const;

  bool AimObserver(Vector3Par direction);
	void AimHead(Vector3Par direction);
	void AimHeadForward();

	void AimHeadAI(Vector3Par direction, float deltaT);

	void AimWeaponAI( int weapon, Vector3Par direction, float deltaT );
	void AimWeaponAI( int weapon, Target *target, float deltaT );
	bool AimWeaponForceFire(int weapon);

	bool CalculateAimWeapon( int weapon, Vector3 &dir, Target *target );	
	bool AimWeapon( int weapon, Vector3Par direction );
	bool AimWeapon( int weapon, Target *target );

	void SimulateHUD(CameraType camType, float deltaT);
	void AimWeaponManDir( int weapon, Vector3Par direction );
	void AimWeaponManSpeed( int weapon, float moveX, float moveY );

	void AdjustWeapon
	(
		int weapon, CameraType camType, float fov, Vector3 &camDir
	);

	Vector3 GetWeaponRelDirection( int weapon ) const;
	Matrix4 GetWeaponRelTransform( int weapon ) const;
	Vector3 GetWeaponDirection( int weapon ) const;
	Vector3 GetWeaponCenter( int weapon ) const;
	Vector3 GetHeadCenter() const;
	Vector3 GetEyeDirection() const;
	Matrix3 GetHeadRelOrientation() const;
	Vector3 GetWeaponPoint( int weapon ) const;
	bool GetWeaponCartridgePos
	(
		int weapon, Matrix4 &pos, Vector3 &vel
	) const;

	float GetAimed( int weapon, Target *target ) const;

	// check current state of motion
	bool IsDead() const;
	bool IsDown() const;
	bool IsLaunchDown() const;

	bool IsBinocularInMove() const;
	bool IsHandGunInMove() const;
	bool IsPrimaryWeaponInMove() const;
	bool IsWeaponInMove() const;
	//bool IsOpticsInMove() const;

	bool DisableWeapons() const;
	bool EnableMissile() const;

	void FireAttemptWhenNotPossible();

	bool EnableTest(TestEnable func) const;
	float ValueTest(TestValue func, float defValue = 0) const;
	float ValueTest(TestValueTimed func, float defValue = 0) const;
	

	#define MOVE_INFO_TEST(name) \
		bool name() const {return EnableTest(&MoveInfo::name);}
	#define MOVE_INFO_VALUE(name) \
		float name() const {return ValueTest(&MoveInfo::name);}
	#define MOVE_INFO_VALUE_DEF(name,def) \
		float name() const {return ValueTest(&MoveInfo::name,def);}

	MOVE_INFO_TEST(ShowItemInHand)
	MOVE_INFO_TEST(ShowItemInRightHand)
	MOVE_INFO_TEST(ShowHandGun)
	MOVE_INFO_TEST(EnableFistStroke)
	MOVE_INFO_TEST(EnableGunStroke)
	MOVE_INFO_TEST(EnableBinocular)
	MOVE_INFO_TEST(WeaponsDisabled)
	MOVE_INFO_TEST(EnableOptics)
	MOVE_INFO_TEST(DisableWeaponsLong)
	MOVE_INFO_TEST(ShowWeaponAim)

	MOVE_INFO_VALUE(GetAimPrecision)
	MOVE_INFO_VALUE(GetLimitGunMovement)
	MOVE_INFO_VALUE_DEF(GetInterpolSpeed,6)
	// more

	typedef const BlendAnimSelections &(MoveInfo::*BlendAnimFunc)() const;

	const BlendAnimSelections &GetBlendAnim( BlendAnimSelections &tgt, BlendAnimFunc func ) const;
	const BlendAnimSelections &GetAiming( BlendAnimSelections &tgt ) const;
	const BlendAnimSelections &GetLegs( BlendAnimSelections &tgt ) const;
	const BlendAnimSelections &GetHead( BlendAnimSelections &tgt ) const;

	//float GetLimitGunMovement() const;
	float VisibleMovement() const;
	float Audible() const;
	float GetHidden() const; // hidden by surroundings

	float CollisionSize() const;
	float VisibleSize() const;
	Vector3 VisiblePosition() const;
	Vector3 AimingPosition() const;
	Vector3 CameraPosition() const;

	Vector3 CalculateAimingPosition(Matrix4Par pos)const;
	Vector3 CalculateCameraPosition(Matrix4Par pos)const;

	float GetArmor() const;
	float GetInvArmor() const;

	void ResetMovement(float speed, int action = -1);	

	const AnimationRTWeight &GetSelWeights( int level, int selection ) const;
	const AnimationRTWeight &GetProxyWeights( int level, const ProxyObject &proxy ) const;
	void AnimateMatrix( Matrix4 &mat, int level, int selection ) const;

	void AnimateMatrix( Matrix4 &mat, const AnimationRTWeight &wgt ) const;
	void BlendMatrix(Matrix4 &mat,const Matrix4 &trans,float factor) const;

	Vector3 COMPosition() const;
	Vector3 AnimatePoint( int level, int selIndex ) const;
	Matrix4 InsideCamera( CameraType camType ) const;
	Vector3 GetCameraDirection(CameraType cam) const;
	Vector3 ExternalCameraPosition( CameraType camType ) const;
	Vector3 GetSpeakerPosition() const;

	bool HasFlares( CameraType camType ) const;

	float TrackingSpeed() const {return 150;}
	//float OutsideCameraDistance( CameraType camType ) const {return 15;}
	float OutsideCameraDistance( CameraType camType ) const {return 10;}

	float RifleInaccuracy() const; // max. rifle accuracy in current state

	bool IsVirtual( CameraType camType ) const;
	bool IsVirtualX( CameraType camType ) const;
	bool IsGunner( CameraType camType ) const;
	CursorMode GetCursorRelMode(CameraType camType) const;
	void LimitCursor( CameraType camType, Vector3 &dir ) const;
	void OverrideCursor( CameraType camType, Vector3 &dir ) const;
	
	bool IsCommander( CameraType camType ) const;
	bool ShowAim( int weapon, CameraType camType ) const;
	bool ShowCursor( int weapon, CameraType camType ) const;

	int InsideLOD( CameraType camType ) const;
	void InitVirtual
	(
		CameraType camType, float &heading, float &dive, float &fov
	) const;
	void LimitVirtual
	(
		CameraType camType, float &heading, float &dive, float &fov
	) const;

	LSError Serialize(ParamArchive &ar);

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx);

	void UnselectLauncher();
	void ProcessGetIn();

	USE_CASTING(base)
};

//! Soldier class - simulation of all soldiers

class Soldier: public Man
{
	typedef Man base;

	public:
	Soldier(VehicleType *name, bool fullCreate=true);
	~Soldier();

	float GetFieldCost( const GeographyInfo &info ) const;
	float GetCost( const GeographyInfo &info ) const;
	float GetCostTurn( int difDir ) const;
	float GetTypeCost(OperItemType type) const;
	float FireInRange( int weapon, float &timeToAim, const Target &target ) const;


	int MissileIndex() const;

#if _ENABLE_AI
	void AIFire( float deltaT );
	void AIPilot(float deltaT, SimulationImportance prec);
	void DisabledPilot(float deltaT, SimulationImportance prec);
#endif

	void JoystickPilot( float deltaT );
	void KeyboardPilot( float deltaT, SimulationImportance prec);
	void SuspendedPilot( float deltaT, SimulationImportance prec);

	void FakePilot( float deltaT );
	bool FireWeapon( int weapon, TargetType *target );
	void FireWeaponEffects(int weapon, const Magazine *magazine,EntityAI *target);

	virtual void OnRecoilAbort();
	virtual float GetRecoilFactor() const;

	void Simulate( float deltaT, SimulationImportance prec );

	USE_CASTING(base)
};

#endif
