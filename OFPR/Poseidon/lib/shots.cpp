// shots and explosions simulation

#include "wpch.hpp"
#include "shots.hpp"
#include "global.hpp"
#include <El/Common/randomGen.hpp>
#include "world.hpp"
//#include "engine.hpp"
#include "lights.hpp"
#include "landscape.hpp"
#include "camera.hpp"
#include "vehicleAI.hpp"
#include "ai.hpp"
//#include "loadStream.hpp"
#include "weapons.hpp"
#include "network.hpp"
#include "txtPreload.hpp"
#include "SpecLods.hpp"
#include "objLine.hpp"

#include <El/Common/enumNames.hpp>

#include <El/QStream/QBStream.hpp>
#include "gameStateExt.hpp"

#if _DEBUG
#define ARROWS 1
#endif

DEFINE_FAST_ALLOCATOR(Shot)
DEFINE_CASTING(Shot)

Shot::Shot( EntityAI *parent, const AmmoType *type )
:base(type->GetShape(),type,-1),_parent(parent)
{
	if( type->GetShape()==NULL )
	{
		Fail("No shape");
		LogF("Type %s",(const char *)type->GetName());
	}
	_timeToLive=10.0;
	SetSimulationPrecision(type->simulationStep);
}

void Shot::SetParent( EntityAI *parent )
{
	_parent=parent;
}

#if _RELEASE
	bool Shot::Invisible() const
	{
		return _speed.Distance2(GLOB_SCENE->GetCamera()->Speed())>Square(400);
	}
#endif

void Shot::Sound( bool inside, float deltaT )
{
	const SoundPars &sound=Type()->_soundFly;
	if( !_sound && sound.name.GetLength() > 0)
	{
		_sound=GSoundScene->OpenAndPlay
		(
			sound.name,Position(),Speed()
		);
	}
	if( _sound )
	{
		const Camera &camera=*GLOB_SCENE->GetCamera();
		Vector3 posToCamera=camera.Position()-Position();
		float speedCoef=1;
		/*
		float speedCoef=0;
		float dp=posToCamera*(Speed()-camera.Speed());
		if( dp<0 || Square(dp)<Square(SpeedOfSound*0.7)*posToCamera.SquareSize() )
		//if( posToCamera.Normalized()*Speed()<SpeedOfSound*0.7 )
		{
			//speedCoef=Speed().SquareSize()*Square(1.0/1200);
			speedCoef=Speed().Size()*(1.0/1200);
		}
		*/
		_sound->SetVolume(sound.vol*speedCoef,sound.freq);
		_sound->SetPosition(Position(),Speed());
	}
}

void Shot::UnloadSound()
{
	_sound.Free();
}

LSError Shot::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	CHECK(ar.SerializeRef("Parent", _parent, 1))
	return LSOK;
	
/* TODO: ?? serialize
	InitPtr<const AmmoType> _type;
	Ref<AbstractWave> _sound;
	float _timeToLive;
	bool _fake;
*/
}

NetworkMessageType Shot::GetNMType(NetworkMessageClass cls) const
{
	switch (cls)
	{
	case NMCCreate:
		return NMTCreateShot;
	case NMCUpdateGeneric:
	case NMCUpdateDammage:
	case NMCUpdatePosition:
		return NMTNone;
	default:
		return base::GetNMType(cls);
	}
}

class IndicesCreateShot : public IndicesCreateVehicle
{
	typedef IndicesCreateVehicle base;

public:
	int parent;
	int timeToLive;
	int createPos;
	int createSpeed;
	int createOrient;

	IndicesCreateShot();
	NetworkMessageIndices *Clone() const {return new IndicesCreateShot;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesCreateShot::IndicesCreateShot()
{
	parent = -1;
	timeToLive = -1;
	createPos = -1;
	createSpeed = -1;
	createOrient = -1;
}

void IndicesCreateShot::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);

	SCAN(parent)
	SCAN(timeToLive)
	SCAN(createPos)
	SCAN(createSpeed)
	SCAN(createOrient)
}

NetworkMessageIndices *GetIndicesCreateShot() {return new IndicesCreateShot();}

class IndicesUpdateShot : public IndicesUpdateVehicle
{
	typedef IndicesUpdateVehicle base;

public:
	IndicesUpdateShot();
	NetworkMessageIndices *Clone() const {return new IndicesUpdateShot;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesUpdateShot::IndicesUpdateShot()
{
}

void IndicesUpdateShot::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);
}

NetworkMessageIndices *GetIndicesUpdateShot() {return new IndicesUpdateShot();}

/*!
\patch 1.82 Date 8/14/2002 by Ondra
- Fixed: MP: Remote grenades and bullet tracers are now visible.
*/

NetworkMessageFormat &Shot::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	Vector3 temp = VZero;
	Matrix3 tempM = M3Identity;
	switch (cls)
	{
	case NMCCreate:
		base::CreateFormat(cls, format);
		format.Add("parent", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Owner of shot"));
		format.Add("timeToLive", NDTFloat, NCTNone, DEFVALUE(float, 10), DOC_MSG("Time to live (in seconds)"));
		format.Add("createPos", NDTVector, NCTNone, DEFVALUE(Vector3, temp), DOC_MSG("Initial position"));
		format.Add("createSpeed", NDTVector, NCTNone, DEFVALUE(Vector3, temp), DOC_MSG("Initial speed"));
		format.Add("createOrient", NDTMatrix, NCTMatrixOrientation, DEFVALUE(Matrix3, tempM), DOC_MSG("Initial orientation"));
		break;
	case NMCUpdateGeneric:
		base::CreateFormat(cls, format);
		break;
	default:
		base::CreateFormat(cls, format);
		break;
	}
	return format;
}

Shot *Shot::CreateObject(NetworkMessageContext &ctx)
{
	base *veh = base::CreateObject(ctx);
	Shot *shot = dyn_cast<Shot>(veh);
	if (!shot) return NULL;

	if (shot->TransferMsg(ctx) != TMOK) return NULL;
	return shot;
}

TMError Shot::TransferMsg(NetworkMessageContext &ctx)
{
	switch (ctx.GetClass())
	{
	case NMCCreate:
		if (ctx.IsSending())
		{
			TMCHECK(base::TransferMsg(ctx))
		}
		{
			Assert(dynamic_cast<const IndicesCreateShot *>(ctx.GetIndices()))
			const IndicesCreateShot *indices = static_cast<const IndicesCreateShot *>(ctx.GetIndices());

			ITRANSF_REF(parent)
			ITRANSF(timeToLive)
			TMCHECK(ctx.IdxTransfer(indices->createSpeed, _speed))
			if (ctx.IsSending())
			{
				Vector3 pos = Position();
				Matrix3 orient = Orientation();
				TMCHECK(ctx.IdxTransfer(indices->createPos, pos))
				TMCHECK(ctx.IdxTransfer(indices->createOrient, orient))
			}
			else
			{
				Vector3 pos;
				Matrix3 orient;
				pos.Init();
				TMCHECK(ctx.IdxTransfer(indices-> createPos, pos))
				TMCHECK(ctx.IdxTransfer(indices-> createOrient, orient))
				SetPosition(pos);
				SetOrientation(orient);
			}
		}
		break;
	case NMCUpdateGeneric:
		TMCHECK(base::TransferMsg(ctx))
		break;
	default:
		TMCHECK(base::TransferMsg(ctx))
		break;
	}
	return TMOK;
}

float Shot::CalculateError(NetworkMessageContext &ctx)
{
	return base::CalculateError(ctx);
}

float Shot::GetMaxPredictionTime(NetworkMessageContext &ctx) const
{
	// shots behave very predictably (mostly balistic trajectory)
	// and therefore they can be predicted reliably for very long time
	// there are also no updates abouts shots, so prediction is neccessary
	return 100;
}

static const EnumName EngineStateNames[]=
{
	EnumName(Missile::Init, "INIT"),
	EnumName(Missile::Thrust, "THRUST"),
	EnumName(Missile::Fly, "FLY"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(Missile::EngineState dummy)
{
	return EngineStateNames;
}

static const EnumName LockStateNames[]=
{
	EnumName(Missile::Locked, "LOCKED"),
	EnumName(Missile::Lost, "LOST"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(Missile::LockState dummy)
{
	return LockStateNames;
}

LSError Missile::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	CHECK(ar.SerializeRef("Target", _target, 1))
	CHECK(ar.Serialize("thrust", _thrust, 1))
	CHECK(ar.SerializeEnum("engine", _engine, 1))
	CHECK(ar.SerializeEnum("lock", _lock, 1))
	return LSOK;

/* TODO: ?? serialize
	Ref<AbstractWave> _soundEngine;

	CloudletSource _cloudlets;

	float _initTime,_thrustTime;

	Color _lightColor;
	Ref<LightPointOnVehicle> _light;
*/
}

/*!
\patch 1.78 Date 7/17/2002 by Ondra
- Fixed: MP: Missile direction was not transfered to other clients.
*/

NetworkMessageType Missile::GetNMType(NetworkMessageClass cls) const
{
	switch (cls)
	{
	case NMCCreate:
		return NMTCreateShot;
	case NMCUpdateGeneric:
	case NMCUpdateDammage:
		return base::GetNMType(cls);
	case NMCUpdatePosition:
		return Entity::GetNMType(cls);
	default:
		return base::GetNMType(cls);
	}
}

float Missile::CalculateError(NetworkMessageContext &ctx)
{
	// if missile is guided, we should send some updates about it
	// base::CalculateError was zero for Shot in past, but now more robust solution
	// using GetNMType was implemented
	return base::CalculateError(ctx);
}

float Missile::GetMaxPredictionTime(NetworkMessageContext &ctx) const
{
	// if we did not receive any updates, continue predicting
	return 100;
}

const float MinExplosion=0.25;
const float MaxExplosion=1;

DEFINE_FAST_ALLOCATOR(PipeBomb)
DEFINE_CASTING(PipeBomb)

PipeBomb::PipeBomb( EntityAI *parent, const AmmoType *type )
:Shot(parent,type)
{
	_explosion = false;
	_timeToLive = FLT_MAX;
}

void PipeBomb::Simulate( float deltaT, SimulationImportance prec )
{
	_timeToLive-=deltaT;
	if (IsLocal() && (_explosion || _timeToLive < 0))
	{
		if( Type()->explosive )
		{
			float size=Type()->hit*0.003;
			saturate(size,MinExplosion,MaxExplosion);
			Explosion *explosion=new Explosion(NULL,_parent,size);
			explosion->SetPosition(Position());
			GLOB_WORLD->AddAnimal(explosion);
			GetNetworkManager().CreateVehicle(explosion, VLTAnimal, "", -1);
		}
		GLandscape->ExplosionDammage(_parent,this,NULL,Position(),VUp,Type());
		_delete=true;
	}
}

DEFINE_FAST_ALLOCATOR(TimeBomb)
DEFINE_CASTING(TimeBomb)

TimeBomb::TimeBomb( EntityAI *parent, const AmmoType *type )
:Shot(parent,type)
{
	_timeToLive = 20;
}

void TimeBomb::Simulate( float deltaT, SimulationImportance prec )
{
	_timeToLive-=deltaT;
	if (IsLocal() && _timeToLive < 0)
	{
		if( Type()->explosive )
		{
			float size=Type()->hit*0.003;
			saturate(size,MinExplosion,MaxExplosion);
			Explosion *explosion=new Explosion(NULL,_parent,size);
			explosion->SetPosition(Position());
			GLOB_WORLD->AddAnimal(explosion);
			GetNetworkManager().CreateVehicle(explosion, VLTAnimal, "", -1);
		}
		GLandscape->ExplosionDammage(_parent,this,NULL,Position(),VUp,Type());
		_delete=true;
	}
}

DEFINE_FAST_ALLOCATOR(Mine)
DEFINE_CASTING(Mine)

Mine::Mine( EntityAI *parent, const AmmoType *type )
: Shot(parent,type)
{
	_timeToLive = GRandGen.PlusMinus(0.5, 0.1);
	_active = true;
}

void Mine::Simulate( float deltaT, SimulationImportance prec )
{
	if (!_active)
	{
		// change orientation to indicate mine is no longer active
		// do not touch Direction
		// change DirectionAside
		float bank = DirectionAside().Y();
		float bankWanted = 0.5;
		if	(bank>=bankWanted)
		{
			return;
		}
		float delta = bankWanted-bank;
		saturate(delta,-deltaT*0.5f,+deltaT*0.5f);
		Matrix3 rotZ(MRotationZ,delta);

		Matrix3 newOrient = Orientation()*rotZ;
		SetOrientation(newOrient);
		/*
		Vector3Val aside = DirectionAside();
		// normal aside is VAside, (1,0,0)
		const static Vector3 asideWanted(H_SQRT2*0.5,H_SQRT2*0.5,0);
		// change it slowly
		Vector3 delta = asideWanted-aside;
		saturate(delta[0],-deltaT*0.5f,+deltaT*0.5f);
		saturate(delta[1],-deltaT*0.5f,+deltaT*0.5f);
		saturate(delta[2],-deltaT*0.5f,+deltaT*0.5f);

		SetDirectionAndAside(Direction(),aside+delta);
		// we want to change it to (1/sqrt(2),1/sqrt(2),0).No
		*/
		return;
	}

	_timeToLive-=deltaT;
	if (IsLocal() && _timeToLive < 0)
	{
		bool found = false;
		for (int i=0; i<GWorld->NVehicles(); i++)
		{
			Vehicle *veh = GWorld->GetVehicle(i);
			if (!veh) continue;
			if (veh->GetMass() < 10000.0) continue;
			if (Position().Distance2(veh->Position()) > Square(6.0)) continue;
			found = true;
			break;
		}
		if (found)
		{
			if( Type()->explosive )
			{
				float size=Type()->hit*0.003;
				saturate(size,MinExplosion,MaxExplosion);
				Explosion *explosion=new Explosion(NULL,_parent,size);
				explosion->SetPosition(Position());
				GLOB_WORLD->AddAnimal(explosion);
				GetNetworkManager().CreateVehicle(explosion, VLTAnimal, "", -1);
			}
			GLandscape->ExplosionDammage(_parent,this,NULL,Position(),VUp,Type());
			_delete=true;
		}
		else
			_timeToLive = GRandGen.PlusMinus(0.5, 0.1);
	}
}

LSError Mine::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	CHECK(ar.Serialize("active", _active, 1, true))
	return LSOK;
}

NetworkMessageType Mine::GetNMType(NetworkMessageClass cls) const
{
	switch (cls)
	{
	case NMCCreate:
		return NMTCreateShot;
	case NMCUpdateGeneric:
		return NMTUpdateMine;
	case NMCUpdateDammage:
	case NMCUpdatePosition:
		return NMTNone;
	default:
		return base::GetNMType(cls);
	}
}

class IndicesUpdateMine : public IndicesUpdateShot
{
	typedef IndicesUpdateShot base;

public:
	//@{
	//! index of field in message format
	int active;
	//@}

	IndicesUpdateMine();
	NetworkMessageIndices *Clone() const {return new IndicesUpdateMine;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesUpdateMine::IndicesUpdateMine()
{
	active = -1;
}

void IndicesUpdateMine::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);
	SCAN(active)
}

NetworkMessageIndices *GetIndicesUpdateMine() {return new IndicesUpdateMine();}


NetworkMessageFormat &Mine::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	switch (cls)
	{
	case NMCUpdateGeneric:
		base::CreateFormat(cls, format);
		format.Add("active", NDTBool, NCTNone, DEFVALUE(bool, true), DOC_MSG("Mine is active (can explode)"), ET_ABS_DIF, ERR_COEF_MODE);
		break;
	default:
		base::CreateFormat(cls, format);
		break;
	}
	return format;
}

TMError Mine::TransferMsg(NetworkMessageContext &ctx)
{
	switch (ctx.GetClass())
	{
	case NMCUpdateGeneric:
		TMCHECK(base::TransferMsg(ctx))
		{
			Assert(dynamic_cast<const IndicesUpdateMine *>(ctx.GetIndices()))
			const IndicesUpdateMine *indices = static_cast<const IndicesUpdateMine *>(ctx.GetIndices());
			ITRANSF(active)
		}
		break;
	default:
		TMCHECK(base::TransferMsg(ctx))
		break;
	}
	return TMOK;
}

float Mine::CalculateError(NetworkMessageContext &ctx)
{
	float error = 0;
	switch (ctx.GetClass())
	{
	case NMCUpdateGeneric:
		error += base::CalculateError(ctx);
		{
			Assert(dynamic_cast<const IndicesUpdateMine *>(ctx.GetIndices()))
			const IndicesUpdateMine *indices = static_cast<const IndicesUpdateMine *>(ctx.GetIndices());

			ICALCERR_NEQ(bool, active, ERR_COEF_MODE)
		}
		break;
	default:
		error += base::CalculateError(ctx);
		break;
	}
	return error;
}

DEFINE_FAST_ALLOCATOR(ShotShell)
DEFINE_CASTING(ShotShell)

/*!
\patch_internal 1.85 Date 9/4/2002 by Jirka
- Added: coefGravity parameter added to ammo ShotShell description (default 1)
*/

ShotShell::ShotShell( EntityAI *parent, const AmmoType *type )
:base(parent,type)
{
	_initDelay = type->initTime;

	_timeToLive=20.0;
	_airFriction = -0.0005;

	_coefGravity = 1;

	const ParamEntry &pars = type->GetParamEntry();
	const ParamEntry *entry = pars.FindEntry("coefGravity");
	if (entry) _coefGravity = *entry;
}

bool ShotShell::Invisible() const
{
	if (_initDelay>0)
	{
		return true;
	}
	return base::Invisible();
}

void ShotShell::Sound( bool inside, float deltaT )
{
	if (_initDelay>0) return;
	base::Sound(inside,deltaT);
}

/*!
\patch 1.26 Date 10/04/2001 by Ondra.
- Fixed: MP: Persisting bullet sound.
\patch 1.50 Date 4/9/2002 by Ondra
- Fixed: Grenades do not fly through glass.
*/

void ShotShell::Simulate( float deltaT, SimulationImportance prec )
{
	// body air friction
	Point3 position=Position();

	// simple air friction
	//Vector3 accel=-Direction()*(_speed.SquareSize()*0.0005);
	Vector3 accel=_speed*(_speed.Size()*_airFriction);
	// add gravity
	accel[1]-= _coefGravity * G_CONST;

	_initDelay-=deltaT;
	if (_initDelay<=0)
	{
		_timeToLive-=deltaT;
		if( _timeToLive<0 )
		{
			// shot lived out
			// avoid delete of remote shot
			_delete=true;
			return;
		}
	}

	if (_initDelay<=0)
	{
		position+=_speed*deltaT;
	}

	if (deltaT > 0)
	{
		// test collision with objects
		CollisionBuffer collision;
		GLandscape->ObjectCollision(collision,this,_parent,Position(),position,0);
		if( collision.Size()>0  )
		{
			float minT=1e10;
			int minI=-1;

			// we should select the nearest collision point (minimal t)
			// t is saved in under
			Texture *glass = GPreloadedTextures.New(TextureBlack);
			// detect bullets
			bool detectGlass = dyn_cast<ShotBullet>(this)!=NULL;

			for( int i=0; i<collision.Size(); i++ )
			{
				// info.pos is relative to object
				CollisionInfo &info=collision[i];
				// we can go through some textures

				if (detectGlass && info.texture == glass) continue;
				if( info.object )
				{
					if( minT>info.under ) minT=info.under,minI=i;
				}
			}
			if (IsLocal())
			{
				// check if we made some glass dammage
				for( int i=0; i<collision.Size(); i++ )
				{
					// info.pos is relative to object
					CollisionInfo &info=collision[i];
					if (detectGlass && info.texture != glass) continue;
					// stop on first solid obstacle
					if (info.under>minT) continue;
					if (!info.object) continue;
					// dammage corresponding component (if any)
					info.object->DirectLocalHit
					(
						info.component,Type()->hit //*info.object->GetInvArmor()
					);
				}
			}

			if( minI>=0 )
			{
				if (IsLocal())
				{
					CollisionInfo &info=collision[minI];
					Point3 pos=info.object->PositionModelToTop(info.pos);

					//LogF("*min %.3f: %s",info.under,(const char *)info.object->GetShape()->Name());

					// explode
					if( Type()->hit>50 )
					{
						//pos-=Direction()*2;
						Vector3 ePos=pos;
						//const SoundPars &pars=_type->_hitGround.SelectSound(GRandGen.RandomValue());
						float size=Type()->hit*0.001;
						saturate(size,MinExplosion,MaxExplosion);
						float minY=GLandscape->RoadSurfaceY(ePos[0],ePos[2])+2*size;
						if( ePos[1]<minY ) ePos[1]=minY;
						Explosion *explosion=new Explosion(NULL,_parent,size);
						explosion->SetPosition(ePos);
						GLOB_WORLD->AddAnimal(explosion);
						GetNetworkManager().CreateVehicle(explosion, VLTAnimal, "", -1);
					}
					GLandscape->ExplosionDammage(_parent,this,info.object,pos,info.dirOut,Type());
				} // if (IsLocal())
				_delete=true;
				return;
			} //if( minI>=0 )
		}


		// calculate intersection with land
		// between Position() and position
		Vector3Val lPos=Position();
		Vector3 lDir = position-lPos;
		if (lDir.SquareSize()>0)
		{
			Vector3 lDirNorm = lDir.Normalized();
			float maxDist = lDir*lDirNorm;

			Vector3 isect;
			float t = GLandscape->IntersectWithGroundOrSea(&isect,lPos,lDirNorm,0,maxDist*1.1);
			if (t<=maxDist)
			{
				position = isect;

				if (IsLocal())
				{
					// same for missiles
					// this would allow to simulate not as fast vehicle, but as normal
					Vector3 exploPos = position;
					if( Type()->explosive )
					{
						float size=Type()->hit*0.003;
						saturate(size,MinExplosion,MaxExplosion);
						exploPos[1]+=0.5*size;
						Explosion *explosion=new Explosion(NULL,_parent,size);
						explosion->SetPosition(exploPos);
						GLOB_WORLD->AddAnimal(explosion);
						GetNetworkManager().CreateVehicle(explosion, VLTAnimal, "", -1);
					}
					// avoid delete of remote shot
					GLandscape->ExplosionDammage(_parent,this,NULL,position,VUp,Type());
				} // if (IsLocal())
				_delete=true;
				return;
			}
		}

	}

	if (_initDelay<=0)
	{
		_speed+=accel*deltaT;
	}

	Move(position);
	//SetOrientation(Orientation());
}

void ShotBullet::Simulate( float deltaT, SimulationImportance prec )
{
	base::Simulate(deltaT,prec);
	SetEnd(Position());
}

void ShotBullet::Sound( bool inside, float deltaT )
{
	base::Sound(inside,deltaT);
}

DEFINE_FAST_ALLOCATOR(Missile)
DEFINE_CASTING(Missile)

/*!
\patch 1.05 Date 7/17/2001 by Ondra.
- Fixed: LAW and RPG are now faster and burn most fuel while in tube.
\patch_internal 1.05 Date 7/17/2001 by Ondra.
- Fixed: thurst is now defined from config. Was calculated from maxSpeed before
*/

Missile::Missile
(
	EntityAI *parent, const AmmoType *type, Object *target
)
:Shot(parent,type),

_lock(Locked),_engine(Init),
_initTime(type->initTime),_thrustTime(type->thrustTime),
_controlDirectionSet(false),
_lightColor(0.7,0.8,1.0),
_target(target)
//_cloudlets(GLOB_SCENE->Preloaded(CloudletMissile),0.01)
{
	_thrust=type->thrust;
//	_cloudlets.SetTimes(0.5,1);
//	_cloudlets.SetFades(0.1,0.05,1);
	// TODO: select corresponding cloudlet in config
	if (type->manualControl)
	{
		_cloudlets.Load(Pars >> "CfgCloudlets" >> "CloudletsMissileManual");
	}
	else
	{
		_cloudlets.Load(Pars >> "CfgCloudlets" >> "CloudletsMissile");
	}
	if (type->maxControlRange<1 || !target && !type->manualControl)
	{	// unguided - Lost
		_lock = Lost;
	}
	if (_thrustTime<=0)
	{
		// free fall bombs should have very high time to live
		_timeToLive = 120;
	}
}

void Missile::SetLight( ColorVal color )
{
	_lightColor=color;
}

void Missile::SetControlDirection( Vector3 dir )
{
	_controlDirection = dir; // manual missile control
	_controlDirectionSet = true; // manual control activated
}

inline bool FaceIsShining(const Poly &f, const Shape *shape)
{
	for (int v=0; v<f.N(); v++)
	{
		ClipFlags clip = shape->Clip(f.GetVertex(v));
		if ((clip&ClipUserMask)!=MSShining*ClipUserStep)
		{
			return false;
		}
	}
	return true;
}

/*!
\patch 1.05 Date 7/17/2001 by Ondra.
- Fixed: Missile fire tail is not drawn when fuel is burn out.
*/

void Missile::Animate( int level )
{
	Shape *shape = _shape->Level(level);
	if (_thrustTime<=0)
	{
		// check if fire should be visible
		// scan faces
		for (Offset o=shape->BeginFaces(); o<shape->EndFaces(); shape->NextFace(o))
		{
			Poly &f = shape->Face(o);
			// check if all face vertices are shining

			if (!FaceIsShining(f,shape)) continue;
			f.OrSpecial(IsHidden);
		}
		// scan sections
		for (int i=0; i<shape->NSections(); i++)
		{
			ShapeSection &sec = shape->GetSection(i);
			if (sec.material==MSShining)
			{
				sec.properties.OrSpecial(IsHidden);
			}
		}
	}

}

void Missile::Deanimate( int level )
{
	Shape *shape = _shape->Level(level);
	if (_thrustTime<=0)
	{
		// check if fire should be visible
		// scan faces
		for (Offset o=shape->BeginFaces(); o<shape->EndFaces(); shape->NextFace(o))
		{
			Poly &f = shape->Face(o);
			// check if all face vertices are shining

			if (!FaceIsShining(f,shape)) continue;
			f.AndSpecial(!IsHidden);
		}
		// scan sections
		for (int i=0; i<shape->NSections(); i++)
		{
			ShapeSection &sec = shape->GetSection(i);
			if (sec.material==MSShining)
			{
				sec.properties.AndSpecial(~IsHidden);
			}
		}
	}
}

/*!
\patch 1.02 Date 7/11/2001 by Ondra.
- Fixed: missiles had too strong lateral air friction.
- As a result gravity had almost no effect on missiles.
\patch 1.33 Date 11/28/2001 by Ondra.
- Improved: Laser guided bomb flight model improved.
*/

void Missile::Simulate( float deltaT, SimulationImportance prec )
{
	//LogF("Missile %x: %.6f",this,deltaT);
	//deltaT*=0.1;

	Vector3 force(VZero),torque(VZero);
	Vector3 friction(VZero),torqueFriction(VZero);
	Vector3 pForce(VZero),pCenter(VZero);
	// body air friction
	Vector3Val position=Position();
	Vector3Val speed=ModelSpeed();
	/*
	pForce[0]=speed[0]*speed[0]*speed[0]*5e-4+speed[0]*fabs(speed[0])*150.0+speed[0]*10000;
	pForce[1]=speed[1]*speed[1]*speed[1]*5e-4+speed[1]*fabs(speed[1])*150.0+speed[1]*10000;
	pForce[2]=speed[2]*speed[2]*speed[2]*1e-5+speed[2]*fabs(speed[2])*0.01+speed[2]*2;
	*/
	float mass = GetMass();
	pForce[0]=speed[0]*speed[0]*speed[0]*5e-4f+speed[0]*fabs(speed[0])*10.0f+speed[0]*10;
	pForce[1]=speed[1]*speed[1]*speed[1]*5e-4f+speed[1]*fabs(speed[1])*10.0f+speed[1]*10;
	pForce[2]=speed[2]*speed[2]*speed[2]*1e-5f+speed[2]*fabs(speed[2])*0.01f+speed[2]*2;
	pForce[0] *= Type()->sideAirFriction;
	pForce[1] *= Type()->sideAirFriction;
	pForce[2] *= Type()->sideAirFriction;
	pForce*=mass*(1.0f/10);

	bool freeFall = Type()->thrustTime<=0;

	#if ARROWS
	Vector3 wCenter(VFastTransform,ModelToWorld(),GetCenterOfMass());
	#endif

	if (freeFall)
	{
		// aerodynamic nonstability makes direction aligned with speed
		pForce[0] *= 0.1f;
		pForce[1] *= 0.1f;
		pCenter=Vector3(0,0,+0.3f);
		torque+=pCenter.CrossProduct(pForce);

		#if ARROWS
			AddForce
			(
				wCenter+DirectionModelToWorld(pCenter),
				DirectionModelToWorld(-pForce*InvMass()),Color(1,0,0)
			);
		#endif
	}
	else
	{
		#if ARROWS
			AddForce
			(
				wCenter,DirectionModelToWorld(-pForce*InvMass()),Color(1,0,0)
			);
		#endif

	}

	/*
	LogF
	(
		"speed %.1f,%.1f,%.1f, ofriction %.1f,%.1f,%.1f",
		speed[0],speed[1],speed[2],
		pForce[0],pForce[1],pForce[2]
	);
	*/

	friction+=pForce;

	if (freeFall)
	{
		// {{ FIX 1.33
		// calculate draconic force
		// note: this should be calculated for all missiles,
		// but it is too late to add it now, as it might cause some changed behaviour.

		pForce[0] = speed[0]*fabs(speed[0])*-0.00033f+speed[0]*-0.005f;
		pForce[1] = speed[1]*fabs(speed[1])*-0.00033f+speed[1]*-0.005f;
		pForce[2] = 0;
		pForce*=mass;
		force += pForce;
		// draconic force makes direction aligned with speed

		#if ARROWS
			AddForce(Position(),pForce*InvMass(),Color(1,1,0));
		#endif
		// }}

	}

	
	switch( _engine )
	{
		case Init:
			_initTime-=deltaT;
			if( _initTime<0 )
			{
				if (_thrustTime>0) _engine=Thrust;
				else _engine = Fly;
			}
		break;
		case Thrust:
		{
			Point3 backPos=PositionModelToWorld(Vector3(0,0,-0.5));
			_thrustTime-=deltaT;
			if( _thrustTime<0 ) _engine=Fly;
			Vector3 cSpeed=Speed()*0.1;
			const float maxCSpeed=30;
			if( cSpeed.SquareSize()>Square(maxCSpeed) )
			{
				cSpeed=cSpeed.Normalized()*maxCSpeed;
			}
			_cloudlets.Simulate(backPos,cSpeed,deltaT);
			// FIX too strong missile engines
			//float fade=floatMax(_thrustTime,1);

			// fade in last part of flight

			float fade = 1;
			const AmmoType *type = Type();
			if (4*_thrustTime<type->thrustTime)
			{
				fade = 4 * _thrustTime/type->thrustTime;
			}

			// END OF FIX
			pForce=Vector3(0,0,_thrust*fade)*mass;
			force+=pForce;

			if( Glob.config.lights&LIGHT_MISSILE )
			{
				if( !_light )
				{
					_light=new LightPointOnVehicle
					(
						GLOB_SCENE->Preloaded(SphereLight),
						_lightColor,Color(HBlack),this,Vector3(0,0,-0.5)
					);
					GLOB_SCENE->AddLight(_light);
				}
				if( _light ) _light->SetDiffuse(_lightColor*fade);
			}
		}
		break;
		case Fly:
			_light.Free();
		break;
	}

	if (_lock==Locked && _target && !_target->LockPossible(Type()))
	{	// unguided - Lost
		_lock = Lost;
	}
	// add torques that controls missile rotation
	bool forceExplosion=false;
	if( _lock==Locked )
	{
		Vector3 cmdDir;
		float estT = 0.3;
		// some missile may be manually controled
		if (_controlDirectionSet && !_target)
		{
			cmdDir = _controlDirection-Position();
		}
		else if (_target)
		{
			Vector3 pos = _target->AimingPosition();
			// calculate relative target position
			float dist = pos.Distance(position);
			float estSpeed = (Type()->maxSpeed+speed.Z())*0.5f;
			float time = dist/floatMax(speed.Z(),estSpeed);
			// lead the target
			pos += time*_target->ObjectSpeed();
			if (freeFall)
			{
				pos[1] += pos.DistanceXZ(position)*0.2f;
			}
			cmdDir = pos-Position();
			estT=floatMin(0.3f,time);
		}

		{
			// estimate orientation change with current angular properties
			Matrix3Val orientation=Orientation();

			// adjust orientation so that rPos and rSpeed is aligned
			float dFactor=Type()->maneuvrability*0.3f;
			Vector3 rDir;
			Matrix3 estOrientation=orientation;
			if (freeFall)
			{
				Matrix3Val derOrientation=_angVelocity.Tilda()*orientation;
				Matrix3 estOrientation=orientation+derOrientation*estT;
				estOrientation.Orthogonalize();
			}
			// convert to estimated model space
			Matrix3Val invEstOrientation=estOrientation.InverseRotation();
			// calculate estimated relative position and speed
			Vector3Val rSpeed=invEstOrientation*(Speed()+estT*Acceleration());
			Vector3Val rPos=invEstOrientation*cmdDir;
			if (!freeFall)
			{
				saturate(dFactor,0.5f,0.95f);
				rDir=rSpeed.Normalized()*dFactor+VForward*(1-dFactor);
			}
			else
			{
				saturate(dFactor,0.1f,0.5f);
				rDir=rSpeed.Normalized()*dFactor+VForward*(1-dFactor);
			}

			Vector3 rdn=rDir.Normalized();
			Vector3 rpn=rPos.Normalized();

			// rSpeed should be near VForward
			//float up=(rpn.Y()-rdn.Y())*800;
			//float left=(rpn.X()-rdn.X())*800;
			float up=(rpn.Y()-rdn.Y())*20;
			float left=(rpn.X()-rdn.X())*20;

			// control up/down flaps
			//float up=pos.Y()*invZ*10;
			//float left=pos.X()*invZ*10;
			if( speed[2]<30 ) up=left=0; // disable controls when flying slow
			if( Type()->manualControl )
			{
				// check if control lost
				if
				(
					!_parent
					|| // controling vehicle is no longer able to control
					!_parent->CanFire()
					|| // controling vehicle too far
					_parent->Position().Distance2(Position())
					>=
					Square(Type()->maxControlRange)
				)
				{
					// note: this is nasty trick
					// stop rotation (once only)
					_angMomentum=VZero;
					up=left=0;
					_lock=Lost;
					//LogF("Lost");
					// loose control
				}
			}
			else
			{
				// check if we can maintain lock (for fire and forget weapon)
				if (_target)
				{
					// calculate relative target position
					Vector3 relPos = PositionWorldToModel(_target->Position());
					if 
					(
						relPos.Z()<0 ||
						fabs(relPos.X())>relPos.Z()*1.5f ||
						fabs(relPos.Y())>relPos.Z()*1.5f
					)
					{
						// target is behind us or out of visible cone
						_angMomentum=VZero;
						up=left=0;
						_lock=Lost;
					}
				}
			}
			//if( fabs(up)>5 || fabs(left)>5 ) _lock=Lost;
			float turn=speed[2]*(1.0/50);
			saturate(turn,0.1,3);
			float invTurn=3/turn;
			// correct for slow speed
			up*=invTurn;
			left*=invTurn;

			float maxMan=Type()->maneuvrability*0.25;
			//if (freeFall) maxMan *= 0.333f;
			saturate(up,-maxMan,+maxMan);
			saturate(left,-maxMan,+maxMan);
			
			Vector3 pCenter=Vector3(0,0,Type()->maneuvrability*0.04f);
			Vector3 pForce=mass*turn*Vector3(left,up,0);
			torque+=pCenter.CrossProduct(pForce);

			if (freeFall)
			{
				// assume that besides of torque, controls cause some force
				force += pForce;
			}

			#if ARROWS
				AddForce
				(
					PositionModelToWorld(pCenter),DirectionModelToWorld(pForce*InvMass()),
					Color(0,0,1)
				);
			#endif
		}
	}
	// convert to world space
	DirectionModelToWorld(friction,friction);
	DirectionModelToWorld(force,force);
	DirectionModelToWorld(torque,torque);

	//torqueFriction=_angMomentum*1.5;
	//float fric=floatMax(_type->maneuvrability,2.0);
	//torqueFriction=_angMomentum*15.0;
	torqueFriction=_angMomentum*5.0;

	// add gravity
	pForce=Vector3(0,-G_CONST,0)*mass;
	force+=pForce;

	// check object collision
	
	// calculate new position
	Matrix4 movePos;
	ApplySpeed(movePos,deltaT);
	// consider using frame with inverse
	Frame moveTrans;
	moveTrans.SetTransform(movePos);

	if (IsLocal() && deltaT > 0)
	{
		CollisionBuffer collision;
		GLandscape->ObjectCollision(collision,this,_parent,Position(),moveTrans.Position(),0);
		//GLandscape->ObjectCollision(collision,this,moveTrans,prec);
		if( collision.Size()>0  )
		{
			float minT=1e10;
			int minI=-1;
			// we should select the nearest collision point (minimal t)
			// t is saved in under
			for( int i=0; i<collision.Size(); i++ )
			{
				// info.pos is relative to object
				const CollisionInfo &info=collision[i];
				// we can go through some textures
				if( info.object )
				{
					if( minT>info.under ) minT=info.under,minI=i;
				}
			}

			if( minI>=0 )
			{
				const CollisionInfo &info=collision[minI];
				Point3 pos=info.object->PositionModelToWorld(info.pos);
				// explode
				//const SoundPars &pars=_type->_hitGround.SelectSound(GRandGen.RandomValue());
				float size=Type()->hit*0.003;
				saturate(size,MinExplosion,MaxExplosion);
				Explosion *explosion=new Explosion(NULL,_parent,size);
				pos-=Direction()*2*size;
				float minY=GLandscape->RoadSurfaceY(position[0],position[2])+3*size;
				if( pos[1]<minY ) pos[1]=minY;
				explosion->SetPosition(pos);
				GLOB_WORLD->AddAnimal(explosion);
				GetNetworkManager().CreateVehicle(explosion, VLTAnimal, "", -1);
				GLandscape->ExplosionDammage(_parent,this,info.object,pos,info.dirOut,Type());
				_delete=true;
				#if _ENABLE_CHEATS
					if (this==GWorld->CameraOn() && _parent)
					{
						GWorld->SwitchCameraTo(_parent,CamInternal);
					}
				#endif
				return;
			}
		}
	}

	_timeToLive-=deltaT;

	if (IsLocal())
	{
		if( _timeToLive<0 )
		{
			forceExplosion=true;
		}

		// calculate intersection with land
		// between Position() and position
		Vector3 lPos = position;
		Vector3 lDir = movePos.Position()-lPos;
		if (lDir.SquareSize()>0)
		{
			Vector3 lDirNorm = lDir.Normalized();
			float maxDist = lDirNorm*lDir;
			Vector3 isect;
			float t = GLandscape->IntersectWithGroundOrSea(&isect,lPos,lDirNorm,0,maxDist*1.1);
			if (t<maxDist)
			{
				lPos = isect;
				forceExplosion = true;
			}
		}

		if (forceExplosion)
		{
			//const SoundPars &pars=_type->_hitGround.SelectSound(GRandGen.RandomValue());
			float size=Type()->hit*0.003;
			saturate(size,MinExplosion,MaxExplosion);
			Explosion *explosion=new Explosion(NULL,_parent,size);
			explosion->SetPosition(lPos);
			GLOB_WORLD->AddAnimal(explosion);
			GetNetworkManager().CreateVehicle(explosion, VLTAnimal, "", -1);
			GLandscape->ExplosionDammage(_parent,this,NULL,lPos,VUp,Type());
			_delete=true;
			#if _ENABLE_CHEATS
				if (this==GWorld->CameraOn() && _parent)
				{
					GWorld->SwitchCameraTo(_parent,CamInternal);
				}
			#endif
			return;
		}
	}

	Move(moveTrans);
	DirectionWorldToModel(_modelSpeed,_speed);

	ApplyForces(deltaT,force,torque,friction,torqueFriction);
	/*
	LogF
	(
		"missile: force %.1f,%.1f,%.1f, friction %.1f,%.1f,%.1f, speed %.1f,%.1f,%.1f",
		force[0],force[1],force[2],
		friction[0],friction[1],friction[2],
		_modelSpeed[0],_modelSpeed[1],_modelSpeed[2]
	);
	*/
}

void Missile::Sound( bool inside, float deltaT )
{
	const SoundPars &sound=Type()->_soundEngine;
	if( _engine==Thrust )
	{
		if( !_soundEngine && sound.name.GetLength()>0 )
		{
			_soundEngine=GSoundScene->OpenAndPlay
			(
				sound.name,Position(),Speed()
			);
		}
		if( _soundEngine )
		{
			float coef=_thrust*(1.0/800);
			_soundEngine->SetVolume(sound.vol*coef,sound.freq);
			_soundEngine->SetPosition(Position(),Speed());
		}
	}
	else
	{
		_soundEngine.Free();
	}
	base::Sound(inside,deltaT);
}

void Missile::UnloadSound()
{
	_soundEngine.Free();
	base::UnloadSound();
}

Missile::~Missile()
{
}

DEFINE_CASTING(IlluminatingShell)
DEFINE_FAST_ALLOCATOR(IlluminatingShell)

#define ILL_TTL			17.0F
#define ILL_EXPL		2.0F

IlluminatingShell::IlluminatingShell( EntityAI *parent, const AmmoType *type )
: base(parent,type),
_lightColor(1.0, 1.0, 1.0)
{
	_timeToLive=ILL_TTL;
	_airFriction = -0.0005;

	const ParamEntry &cls = *type->_par;
	_lightColor = GetColor(cls >> "lightColor");
}

/*!
\patch 1.45 Date 2/18/2002 by Jirka
- Added: When illuminating shell is lit, script onFlare.sqs in mission directory is called.
Received [[R, G, B], gunner], where R, G, B is light color from <0, 1>.
*/

void IlluminatingShell::Simulate( float deltaT, SimulationImportance prec )
{
	base::Simulate(deltaT, prec);
	if (!_light && _timeToLive <= ILL_TTL - ILL_EXPL)
	{
		_light = new LightPointOnVehicle
		(
			GLOB_SCENE->Preloaded(SphereLight),
			_lightColor, Color(HBlack), this, Vector3(0, 0, -0.5)
		);
		_light->SetBrightness(2);
		GLOB_SCENE->AddLight(_light);

		_airFriction = -0.2;
		
		// run script
		RString name = RString("onFlare.sqs");
		RString GetMissionDirectory();
		if (QIFStreamB::FileExist(GetMissionDirectory() + name))
		{
			GameArrayType color;
			color.Add(_lightColor.R());
			color.Add(_lightColor.G());
			color.Add(_lightColor.B());
			GameArrayType arguments;
			arguments.Add(color);
			arguments.Add(GameValueExt(_parent));
			
			Script *script = new Script(name, arguments);
			GWorld->AddScript(script);
			GWorld->SimulateScripts();
		}
	}
	float fade = GRandGen.PlusMinus(0.8, 0.2);
	if( _light ) _light->SetDiffuse(_lightColor*fade);
}

void IlluminatingShell::SetLight( ColorVal color )
{
	_lightColor=color;
}

/*!
\patch 1.78 Date 7/17/2002 by Ondra
- Fixed: MP: Fired flares were not transfered to other clients.
*/


NetworkMessageType IlluminatingShell::GetNMType(NetworkMessageClass cls) const
{
	switch (cls)
	{
	case NMCCreate:
		return NMTCreateShot;
	case NMCUpdateGeneric:
	case NMCUpdateDammage:
		return base::GetNMType(cls);
	case NMCUpdatePosition:
		return Entity::GetNMType(cls);
	default:
		return base::GetNMType(cls);
	}
}

LSError IlluminatingShell::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	CHECK(ar.Serialize("lightColor", _lightColor, 1))
	return LSOK;
}

DEFINE_CASTING(SmokeShell)
DEFINE_FAST_ALLOCATOR(SmokeShell)

#define SMOKE_TTL			60.0F

SmokeShell::SmokeShell( EntityAI *parent, const AmmoType *type )
: base(parent,type)
{
	_timeToLive = SMOKE_TTL;
	_airFriction = -0.0005;

	// load smoke parameters
	const ParamEntry &cls = *type->_par;
	_smoke.Load(cls >> "Smoke");
	_smoke.SetColor(GetColor(cls >> "smokeColor"));
//	_smoke.Load(type->ParClass("Smoke"));
}

/*!
\patch_internal 1.28 Date 10/25/2001 by Ondra.
- Fixed: smoke shell simulation bug fixed (SetPosition used instead of Move).
*/

void SmokeShell::Simulate( float deltaT, SimulationImportance prec )
{
	// body air friction
	Point3 position=Position();

	// simple air friction
	Vector3 accel=_speed*(_speed.Size()*_airFriction);
	// add gravity
	accel[1]-=G_CONST;

	float surfaceY = GLandscape->SurfaceY(position[0],position[2]);

	_initDelay -= deltaT;
	if (_initDelay <= 0)
	{
		_timeToLive -= deltaT;
		if (_timeToLive < 0 ) _delete = true;

		position += _speed * deltaT;
		if (position[1] > surfaceY + 1e-3)
		{
			_speed += accel*deltaT;
		}
	}

	// check ground/object collision
	if (position[1] < surfaceY)
	{
		position[1] = surfaceY;
		_speed = VZero;
	}
	if (deltaT > 0)
	{
		CollisionBuffer collision;
		GLandscape->ObjectCollision(collision, this, _parent, Position(), position, 0);

		int nCol = 0;
		for (int i=0; i<collision.Size(); i++)
		{
			// info.pos is relative to object
			const CollisionInfo &info = collision[i];
			if (info.object) nCol++;
		}
		if (nCol > 0)
		{
			position[1] = surfaceY;
			_speed = VZero;
		}
	}

	Move(position);
	//moveTrans.SetOrientation(Orientation());
	
//	base::Simulate(deltaT, prec);

	_smoke.Simulate(Position(), Speed(), deltaT, prec);
}

LSError SmokeShell::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	return LSOK;
}

DEFINE_FAST_ALLOCATOR(ShotBullet)
DEFINE_CASTING(ShotBullet)

/*!
\patch 1.22 Date 9/10/2001 by Ondra.
- Fixed: Bullet tracers were missing since 1.20.
\patch_internal 1.22 Date 9/10/2001 by Ondra.
- Changed: Bullet shape is now shared between all bullets.
  Bullets are animated in Animate and not in DoDraw.
\patch 1.24 Date 9/26/2001 by Ondra.
- Fixed: Bullet tracers for rifles are now optional.
*/

ShotBullet::ShotBullet( EntityAI *parent, const AmmoType *type )
:ShotShell(parent,type)
{
	_timeToLive=3;
	//_invMass=1/GetMass();
	//_shape=ObjectLine::CreateShape();
	_shape = GScene->Preloaded(BulletLine);

	_beg=VZero;
	_end=VZero;

	float width=type->hit*(1.0/20);
	saturate(width,0.1,2);

	// get tracer color

	//;
	//PackedColor color(Color(1,1,0.3,width));
	PackedColor color = type->_tracerColor;
	if (!Glob.config.IsEnabled(DTTracers))
	{
		color = type->_tracerColorR;
	}
	float a8 = color.A8()*width;
	saturate(a8,0,255);
	color.SetA8(toInt(a8));
	SetConstantColor(color);
}


void ShotBullet::SetBeg( Vector3Val beg )
{
	_beg=beg;
}

void ShotBullet::SetEnd( Vector3Val end )
{
	if( _beg.SquareSize()<=1e-6 ) _beg=end;
	_end=end;
}

void ShotBullet::StartFrame()
{
	// start frame - used for motion blur
	if( _end.SquareSize()<1e-6 ) _end=Position();
	SetBeg(_end);
}

inline void ShotBullet::DoDraw( int level, ClipFlags clipFlags, const FrameBase &pos )
{
	if( _beg.Distance2(_end)<0.1 ) return;

	//ObjectLine::SetPos(_shape,PositionWorldToModel(_beg),PositionWorldToModel(_end));
	DrawLines(level,clipFlags,*this);
}

int ShotBullet::PassNum( int lod )
{
	return 2; // alpha pass
}

bool ShotBullet::IsAnimated( int level ) const
{
	return true;
}

bool ShotBullet::IsAnimatedShadow( int level ) const
{
	return false;
}

void ShotBullet::Animate( int level )
{
	ObjectLine::SetPos(_shape,PositionWorldToModel(_beg),PositionWorldToModel(_end));
}

void ShotBullet::Deanimate( int level )
{
	ObjectLine::SetPos(_shape,VZero,VForward);
}

void ShotBullet::AnimatedMinMax( int level, Vector3 *minMax )
{
	Shape *shape = _shape->Level(level);
	Assert (shape->NVertex()==2);
	Vector3 v0 = shape->Pos(0);
	Vector3 v1 = shape->Pos(1);
	minMax[0] = v0, minMax[1] = v0;
	CheckMinMax(minMax[0],minMax[1],v1);
}
void ShotBullet::AnimatedBSphere( int level, Vector3 &bCenter, float &bRadius, bool isAnimated )
{
	Shape *shape = _shape->Level(level);
	Assert (shape->NVertex()==2);
	Vector3 v0 = shape->Pos(0);
	Vector3 v1 = shape->Pos(1);
	bCenter = (v0+v1)*0.5f;
	bRadius = v0.Distance(v1)*0.5f;
}


void ShotBullet::Draw( int level, ClipFlags clipFlags, const FrameBase &pos )
{
	#if !ALPHA_SPLIT
	DoDraw(level,clipFlags,pos);
	#endif
}

#if ALPHA_SPLIT
void ShotBullet::DrawAlpha( int level, ClipFlags clipFlags, const FrameBase &pos )
{
	DoDraw(level,clipFlags,pos);
}
#endif

#if !_RELEASE
	#define ARROWS 1
#endif


Shot *NewShot( EntityAI *parent, const AmmoType *type, Object *target )
{
	Shot *v=NULL;
	type->VehicleAddRef();
	switch( type->_simulation )
	{
		case AmmoShotShell: v = new ShotShell(parent,type);break;
		case AmmoShotMissile: v = new Missile(parent,type,target);break;
		case AmmoShotRocket: v = new Missile(parent,type,target);break;
		case AmmoShotBullet: v = new ShotBullet(parent,type);break;
		case AmmoShotIlluminating: v = new IlluminatingShell(parent,type);break;
		case AmmoShotSmoke: v = new SmokeShell(parent,type);break;
		case AmmoShotTimeBomb: v = new TimeBomb(parent,type);break;
		case AmmoShotPipeBomb: v = new PipeBomb(parent,type);break;
		case AmmoShotMine: v = new Mine(parent,type);break;
		default:
			ErrF("Unsupported ammo type (type name %s)",(const char *)type->GetName());
		return NULL;
	}
	type->VehicleRelease();
	return v;
}

