// Implementation of main map display
#include "wpch.hpp"
#include "uiMap.hpp"
//#include "win.h"
#include "dikCodes.h"
#include "person.hpp"
#include "diagModes.hpp"

#include "resincl.hpp"
#include "keyInput.hpp"
#include "vkCodes.h"

#include "landscape.hpp"
#include "roads.hpp"

#include "camera.hpp"
#include "detector.hpp"

#include "mapTypes.hpp"
#include <El/Evaluator/express.hpp>
#include "gameStateExt.hpp"

#include <El/QStream/QBStream.hpp>

#include <El/Common/randomGen.hpp>

#include <Es/Algorithms/qsort.hpp>

#include <time.h>

#include <El/Common/enumNames.hpp>

#include "network.hpp"
#include "chat.hpp"

#include "txtPreload.hpp"

#include "stringtableExt.hpp"
#include "mbcs.hpp"

#include <Es/Strings/bstring.hpp>
#include <ctype.h>

#include "aiRadio.hpp"

#include "strFormat.hpp"

#include "uiActions.hpp"

/*!
\file
Implementation file for particular displays (maps, briefing, debriefing, mission editor).
*/

// static const int daysInMonth[]={31,28,31,30,31,30,31,31,30,31,30,31};
int GetDaysInMonth(int year, int month);

extern const float CameraZoom;
extern const float InvCameraZoom;

LSError AbstractOptionsUI::Serialize(ParamArchive &ar) {return LSOK;}

///////////////////////////////////////////////////////////////////////////////
// Map (generic static control)

#define TO_INT(coef, arg) coef > 0 ? toIntFloor(arg) : toIntCeil(arg)

static void AddChar(BString<32> &buffer, char c)
{
	int n = strlen(buffer);
	if (n < 32 - 1)
	{
		buffer[n] = c;
		buffer[n + 1] = 0;
	}
}

static int Modulo(int &i, int base)
{
	int d = i >= 0 ? i / base : (i - base + 1) / base;
	int m = i - d * base;
	i = d;
	return m;
}

void GridFormat(BString<32> &buffer, RString format, int i)
{
	const char *first = NULL;
	for (const char *p=format; *p!=0; p++)
	{
		if (isalnum(*p))
		{
			first = p; break;
		}
	}
	if (!first) return;
	BString<32> revert;
	int cf = 0;
	const char *p;
	for (p=format+format.GetLength()-1; p!=first; p--)
	{
		if (*p >= '0' && *p <= '9')
		{
			int m = Modulo(i, 10) + cf + *p - '0';
			int cf = m;
			m = Modulo(cf, 10);
			AddChar(revert, '0' + m);
		}
		else if (*p >= 'A' && *p <= 'J')
		{
			int m = Modulo(i, 10) + cf + *p - 'A';
			int cf = m;
			m = Modulo(cf, 10);
			AddChar(revert, 'A' + m);
		}
		else if (*p >= 'a' && *p <= 'j')
		{
			int m = Modulo(i, 10) + cf + *p - 'a';
			int cf = m;
			m = Modulo(cf, 10);
			AddChar(revert, 'a' + m);
		}
		else AddChar(revert, *p);
	}
	if (*p >= '0' && *p <= '9')
	{
		int m = Modulo(i, 10) + cf + *p - '0';
		int cf = m;
		m = Modulo(cf, 10);
		AddChar(revert, '0' + m);
	}
	else if (*p >= 'A' && *p <= 'Z')
	{
		int m = Modulo(i, 26) + cf + *p - 'A';
		int cf = m;
		m = Modulo(cf, 26);
		AddChar(revert, 'A' + m);
	}
	else if (*p >= 'a' && *p <= 'z')
	{
		int m = Modulo(i, 26) + cf + *p - 'a';
		int cf = m;
		m = Modulo(cf, 26);
		AddChar(revert, 'a' + m);
	}
	p--;
	for (; p>=(const char *)format; p--) AddChar(revert, *p);

	for (const char *p=revert+strlen(revert)-1; p>=(const char *)revert; p--) AddChar(buffer, *p);
}

static void AddChar(char *buffer, char c)
{
	int n = strlen(buffer);
	buffer[n] = c;
	buffer[n + 1] = 0;
}

void PositionToAA11(Vector3Val pos, char *buffer)
{
	float sizeLand = LandGrid * LandRange;

	const GridInfo *info = GWorld->GetGridInfo(0);
	if (!info) return;

	float offsetX = GWorld->GetGridOffsetX();
	float offsetY = GWorld->GetGridOffsetY();

	*buffer = 0;
	for (const char *p=info->format; *p!=0; p++)
	{
		if (*p == 'X' || *p == 'x')
		{
			float coefX = sizeLand * info->invStepX;
			int x = TO_INT(coefX, (pos.X() - offsetX) * info->invStepX);
			BString<32> buf;
			GridFormat(buf, info->formatX, coefX >= 0 ? x : x - 1);
			strcat(buffer, buf);
		}
		else if (*p == 'Y' || *p == 'y')
		{
			float coefY = sizeLand * info->invStepX;
			int y = TO_INT(coefY, (sizeLand - pos.Z() - offsetY) * info->invStepY);
			BString<32> buf;
			GridFormat(buf, info->formatY, coefY >= 0 ? y : y - 1);
			strcat(buffer, buf);
		}
		else AddChar(buffer, *p);
	}
}

void MapTypeInfo::Load(const ParamEntry &cls)
{
	icon = GlobLoadTexture
	(
		GetPictureName(cls >> "icon")
	);
	color = GetPackedColor(cls >> "color");
	size = cls >> "size";
}

/*!
\patch 1.97 Date 4/2/2004 by Jirka
- Added: User defined maps in missions
*/

CStatic *CreateStaticMap(ControlsContainer *parent, int idc, const ParamEntry &cls)
{
	CStaticMap *map = new CStaticMap(parent, idc, cls, cls >> "scaleMin", cls >> "scaleMax", cls >> "scaleDefault");
	map->SetVisibleRect(map->X(), map->Y(), map->W(), map->H());
	map->Center();
	return map;
}

CStaticMap::CStaticMap
(
	ControlsContainer *parent, int idc, const ParamEntry &cls,
	float scaleMin, float scaleMax, float scaleDefault
)
	: CStatic(parent, idc, cls)
{
	_defaultCenter.Init();
	_defaultCenter[0] = (Pars >> "CfgWorlds" >> Glob.header.worldname >> "centerPosition")[0];
	_defaultCenter[1] = 0;
	_defaultCenter[2] = (Pars >> "CfgWorlds" >> Glob.header.worldname >> "centerPosition")[1];

	float sizeLand = LandGrid * LandRange;
	saturate(_defaultCenter[0], 0, sizeLand);
	saturate(_defaultCenter[2], 0, sizeLand);

	_colorSeaPacked = GetPackedColor(cls >> "colorSea"); 
	_colorSea = Color((ColorVal)_colorSeaPacked);
	_colorForest = GetPackedColor(cls >> "colorForest");

	_colorCountlines = GetPackedColor(cls >> "colorCountlines");
	_colorCountlinesWater = GetPackedColor(cls >> "colorCountlinesWater");
	_colorForestBorder = GetPackedColor(cls >> "colorForestBorder");
	_colorNames = GetPackedColor(cls >> "colorNames");

	_colorInactive = GetPackedColor(cls >> "colorInactive");
	
	_fontLabel = GLOB_ENGINE->LoadFont(GetFontID(cls>>"fontLabel"));
	const ParamEntry *entry = cls.FindEntry("sizeLabel");
	if (entry) _sizeLabel = (float)(*entry) * _fontLabel->Height();
	else _sizeLabel = cls >> "sizeExLabel";
	_fontGrid = GLOB_ENGINE->LoadFont(GetFontID(cls>>"fontGrid"));
	entry = cls.FindEntry("sizeGrid");
	if (entry) _sizeGrid = (float)(*entry) * _fontGrid->Height();
	else _sizeGrid = cls >> "sizeExGrid";
	_fontUnits = GLOB_ENGINE->LoadFont(GetFontID(cls>>"fontUnits"));
	entry = cls.FindEntry("sizeUnits");
	if (entry) _sizeUnits = (float)(*entry) * _fontUnits->Height();
	else _sizeUnits = cls >> "sizeExUnits";
	_fontNames = GLOB_ENGINE->LoadFont(GetFontID(cls>>"fontNames"));
	entry = cls.FindEntry("sizeNames");
	if (entry) _sizeNames = (float)(*entry) * _fontNames->Height();
	else _sizeNames = cls >> "sizeExNames";

//	_moveOnEdges = cls>>"moveOnEdges";

	_infoTree.Load(cls >> "Tree");
	_infoSmallTree.Load(cls >> "SmallTree");
	_infoBush.Load(cls >> "Bush");
	_infoChurch.Load(cls >> "Church");
	_infoChapel.Load(cls >> "Chapel");
	_infoCross.Load(cls >> "Cross");
	_infoRock.Load(cls >> "Rock");
	_infoBunker.Load(cls >> "Bunker");
	_infoFortress.Load(cls >> "Fortress");
	_infoFountain.Load(cls >> "Fountain");
	_infoViewTower.Load(cls >> "ViewTower");
	_infoLighthouse.Load(cls >> "Lighthouse");
	_infoQuay.Load(cls >> "Quay");
	_infoFuelstation.Load(cls >> "Fuelstation");
	_infoHospital.Load(cls >> "Hospital");
	_infoBusStop.Load(cls >> "BusStop");
	
	_infoWaypoint.Load(cls >> "Waypoint");
	_infoWaypointCompleted.Load(cls >> "WaypointCompleted");

	EnableCtrl(true);

	// TODO: place in resource instead of config

	const ParamEntry &cfgMap = Pars >> "CfgInGameUI" >> "IslandMap";
	_iconPlayer = GlobLoadTexture
	(
		GetPictureName(cfgMap >> "iconPlayer")
	);
	_iconSelect = GlobLoadTexture
	(
		GetPictureName(cfgMap >> "iconSelect")
	);
	_iconCamera = GlobLoadTexture
	(
		GetPictureName(cfgMap >> "iconCamera")
	);
	_iconSensor = GlobLoadTexture
	(
		GetPictureName(cfgMap >> "iconSensor")
	);
	_colorFriendly = GetPackedColor(cfgMap >> "colorFriendly");
	_colorEnemy = GetPackedColor(cfgMap >> "colorEnemy");
	_colorCivilian = GetPackedColor(cfgMap >> "colorCivilian");
	_colorNeutral = GetPackedColor(cfgMap >> "colorNeutral");
	_colorUnknown = GetPackedColor(cfgMap >> "colorUnknown");
	_colorMe = GetPackedColor(cfgMap >> "colorMe");
	_colorPlayable = GetPackedColor(cfgMap >> "colorPlayable");
	_colorSelect = GetPackedColor(cfgMap >> "colorSelect");
	_colorSensor = GetPackedColor(cfgMap >> "colorSensor");
	_colorDragging = GetPackedColor(cfgMap >> "colorDragging");
	
	_colorExposureEnemy = GetPackedColor(cfgMap >> "colorExposureEnemy");
	_colorExposureUnknown = GetPackedColor(cfgMap >> "colorExposureUnknown");
	_colorRoads = GetPackedColor(cfgMap >> "colorRoads");
	_colorGrid = GetPackedColor(cfgMap >> "colorGrid");
	_colorGridMap = GetPackedColor(cfgMap >> "colorGridMap");
	_colorCheckpoints = GetPackedColor(cfgMap >> "colorCheckpoints");
	_colorCamera = GetPackedColor(cfgMap >> "colorCamera");
	_colorMissions = GetPackedColor(cfgMap >> "colorMissions");
	_colorActiveMission = GetPackedColor(cfgMap >> "colorActiveMission");
	_colorPath = GetPackedColor(cfgMap >> "colorPath");
	_colorInfoMove = GetPackedColor(cfgMap >> "colorInfoMove");
	_colorGroups = GetPackedColor(cfgMap >> "colorGroups");
	_colorActiveGroup = GetPackedColor(cfgMap >> "colorActiveGroup");
	_colorSync = GetPackedColor(cfgMap >> "colorSync");
	_colorLabelBackground = GetPackedColor(cfgMap >> "colorLabelBackground");

#if _ENABLE_CHEATS
	_show = true;
	_showCost = false;
#endif

	_showIds = false;
	_showScale = true;

	_moveKey = 0;
	_mouseKey = 0;

	_scaleMin = scaleMin;
	_scaleMax = scaleMax;
	_scaleX = _scaleDefault = scaleDefault;

	Reset();
	
	Precalculate();

	SetVisibleRect(_x, _y, _w, _h);
}

void CStaticMap::Reset()
{
	_moving = false;
	ClearAnimation();
	_moveKey = 0;
	_mouseKey = 0;
	_dragging = false;
	_selecting = false;
	_infoClick._type = signNone;
	_infoClickCandidate._type = signNone;
	_infoMove._type = signNone;

//	Center();
}

SignInfo CStaticMap::FindSign(float x, float y)
{
	struct SignInfo info;
	info._type = signNone;
	info._unit = NULL;
	info._id = NULL;
	return info;
}

void CStaticMap::Center()
{
	Center(_defaultCenter);
}

// colors conversion
// TODO: move to PackedColor class

static PackedColor Mult2p00( PackedColor color )
{
	DWORD val=color;
	val&=0x7f7f7f7f;
	val<<=1;
	return PackedColor(val);
}

static PackedColor Mult1p50( PackedColor color )
{
	DWORD val=color;
	val&=0xfefefefe;
	val+=(val>>1);
	return PackedColor(val);
}

static PackedColor Mult0p75( PackedColor color )
{
	DWORD val=color;
	val&=0xfcfcfcfc;
	val+=(val>>1);
	return PackedColor(val>>1);
}

PackedColor Mult(PackedColor col1, PackedColor col2)
{
	int a = ((col1.A8() + 1) * (col2.A8() + 1) - 1) >> 8;
	int r = ((col1.R8() + 1) * (col2.R8() + 1) - 1) >> 8;
	int g = ((col1.G8() + 1) * (col2.G8() + 1) - 1) >> 8;
	int b = ((col1.B8() + 1) * (col2.B8() + 1) - 1) >> 8;
	return PackedColor(r, g, b, a);
}

PackedColor CStaticMap::InactiveColor(PackedColor color)
{
	return Mult(color, _colorInactive);
}

// coordinates conversion

DrawCoord CStaticMap::WorldToScreen(Vector3Val pt)
{
	float invSizeLand = 1.0 / (LandGrid * LandRange);
	float xMap = pt.X() * invSizeLand * _invScaleX;
	float yMap = (1.0 - pt.Z() * invSizeLand) * _invScaleY;
	return DrawCoord(xMap + _mapX + _x, yMap + _mapY + _y);
}

Point3 CStaticMap::ScreenToWorld(DrawCoord ptMap)
{
	float sizeLand = LandGrid * LandRange;
	float x = (ptMap.x - _mapX - _x) * _scaleX * sizeLand;
	float y = (1.0 - (ptMap.y - _mapY - _y) * _scaleY) * sizeLand;
	// !!! WARNING: result.Y() == 0
	return Point3(x, 0, y);
}

#define CX(x) (toInt((x) * _wScreen) + 0.5)
#define CY(y) (toInt((y) * _hScreen) + 0.5)
#define CW(w) toInt((w) * _wScreen)
#define CH(h) toInt((h) * _hScreen)

// drawing
bool CStaticMap::DrawSign(MapTypeInfo &info, Vector3Val pos)
{
	float size = (_invScaleX * 0.05) * info.size;
	return DrawSign(info.icon, info.color, pos, size, size, 0);
}

bool CStaticMap::DrawSign
(
	Texture *texture, PackedColor color,
	Vector3Val pos, float w, float h, float azimut, RString text
)
{
	DrawCoord posMap = WorldToScreen(pos);
	return DrawSign(texture, color, posMap, w, h, azimut, text);
}

bool CStaticMap::DrawSign
(
	Texture *texture, PackedColor color,
	DrawCoord posMap, float w, float h, float azimut, RString text
)
{
	// normalize w, h
	w *= 1.0 / 640;
	h *= 1.0 / 480;

	if (azimut == 0)
	{
		// draw via Draw2D

		// center sign
		posMap.x -= 0.5 * w;
		posMap.y -= 0.5 * h;

		if (posMap.x + w < _x) 
			return false;
		if (posMap.y + h < _y)
			return false;
		if (posMap.x > _x + _w)
			return false;
		if (posMap.y > _y + _h)
			return false;

		MipInfo mip = GEngine->TextBank()->UseMipmap(texture, 0, 0);
		GEngine->Draw2D
		(
			mip, color,
			Rect2DPixel(CX(posMap.x), CY(posMap.y),
			CX(posMap.x+w)-CX(posMap.x), CY(posMap.y+h)-CY(posMap.y)),
			_clipRect
		);

		posMap.x += w;
		posMap.y += 0.5 * h;
	}
	else
	{
		// draw via DrawPoly
		float a = 0.5 * w;
		float b = 0.5 * h;

		// TODO: improve clipping
		float r = sqrt(Square(a) + Square(b));
		if (posMap.x + r < _x) 
			return false;
		if (posMap.y + r < _y)
			return false;
		if (posMap.x - r > _x + _w)
			return false;
		if (posMap.y - r > _y + _h)
			return false;

		float s = sin(azimut);
		float c = cos(azimut);
		float cx = posMap.x * _wScreen;
		float cy = posMap.y * _hScreen;
		a *= _wScreen;
		b *= _hScreen;

		const int n = 4;
		Vertex2DPixel vs[n];
		// 0
		vs[0].x = cx + c * a + s * b;
		vs[0].y = cy + s * a - c * b;
		vs[0].u = 1;
		vs[0].v = 0;
		vs[0].color = color;
		// 1
		vs[1].x = cx + c * a + s * (-b);
		vs[1].y = cy + s * a - c * (-b);
		vs[1].u = 1;
		vs[1].v = 1;
		vs[1].color = color;
		// 2
		vs[2].x = cx + c * (-a) + s * (-b);
		vs[2].y = cy + s * (-a) - c * (-b);
		vs[2].u = 0;
		vs[2].v = 1;
		vs[2].color = color;
		// 3
		vs[3].x = cx + c * (-a) + s * b;
		vs[3].y = cy + s * (-a) - c * b;
		vs[3].u = 0;
		vs[3].v = 0;
		vs[3].color = color;

		MipInfo mip = GEngine->TextBank()->UseMipmap(texture, 0, 0);
		GEngine->DrawPoly(mip, vs, n, _clipRect);

		posMap.x += 0.5 * (fabs(c * w) + fabs(s * h));
	}

	// text
	if (text.GetLength() > 0)
	{
		float size = _sizeNames * (_invScaleX * 0.05);
		saturate(size, 0.5 * _fontNames->Height(), 1.0 * _fontNames->Height());
		float hText = size;
		GEngine->DrawText
		(
			Point2DFloat(posMap.x, posMap.y - 0.5 * hText), size,
			Rect2DFloat(_x, _y, _w, _h),
			_fontNames, PackedColor(color), text
		);
	}

	return true;
}

static const float ptsPerSquareSea = 6;		// seas
static const float ptsPerSquareTxt = 8;		// textures
static const float ptsPerSquareCLn = 8;		// countlines
static const float ptsPerSquareExp = 8;		// exposure
static const float ptsPerSquareCost = 8;	// cost

static const float ptsPerSquareFor = 6;		// forests
static const float ptsPerSquareForBor = 6;// forests' borders
static const float ptsPerSquareRoad = 2;	// roads
static const float ptsPerSquareObj = 10;		// other objects

//! container which makes quick space queries possible
#include <Es/Containers/quadtreeCont.hpp>

struct DetectCircle
{
	int _x;
	int _z;
	int _radius;

	DetectCircle(int x, int z, int radius)
	:_x(x),_z(z),_radius(radius)
	{
	}
	//! check if we overlapp with given rectangle 
	bool operator () (int x, int z, int size) const
	{
		// calculate distance of rectangle center from our center
		int centerX = x+(size>>1);
		int centerZ = z+(size>>1);

		float dist2 = Square(centerX-_x) + Square(centerZ-_z);
		// check if we can quickly reject or quickly accept
		if (dist2<Square(_radius+size))
		{
			// part of the circle is certainly contained in the rectangle
			return true;
		}
		if (dist2>Square(_radius+size)*2)
		{
			// circle is certianly of of the rectangle
			return false;
		}
		// circle may or may not be in the rectangle
		// we can perform stricter check, or we can return true and provide
		// some superfluous items
		return true;
	}
};

struct MountainInfo: public Vector3
{
	int _index;
	bool operator == (const MountainInfo &with) const
	{
		return _index==with._index;
	}
	MountainInfo()
	{
		_index = -1;
	}
	MountainInfo(Vector3Par pos, int index)
	:Vector3(pos),_index(index)
	{
	}

	int GetX() const {return toLargeInt(X());}
	int GetY() const {return toLargeInt(Z());}
};

TypeIsMovable(MountainInfo)

struct SkipNear
{
	MountainInfo _pos;
	//bool _skipped;
	float _minDist;
	SkipNear(MountainInfo pos, float minDist)
	:_pos(pos),
	_minDist(minDist)
	{
	}
	bool operator () (const MountainInfo &arg) const
	{
		// check if the mountain is near enough
		// check if the mountain is higher
		if (arg._index>=_pos._index) return false;
		float dist2 = _pos.DistanceXZ2(arg);
		return dist2<Square(_minDist);
	}

};


//! enumerate all mountains we are interested in
struct MountainRegion
{
	// in
	float _xMin,_xMax;
	float _zMin,_zMax;

	bool operator () (int x, int y, int size) const
	{
		float xMin = x, xMax = x+size;
		float zMin = y, zMax = y+size;
		bool ret = true;
		if (_xMin>xMax) ret = false;
		if (_xMax<xMin) ret = false;
		if (_zMin>zMax) ret = false;
		if (_zMax<zMin) ret = false;
		//LogF("Testing %d,%d:%d, result %d",x,y,size,ret);
		return ret;
	}
};

struct MountainList
{
	// out
	AutoArray<int> _indices;

	bool operator () (const MountainInfo &pos)
	{
		_indices.Add(pos._index);
		return false;
	}
};
/*!
\patch 1.95 Date 10/24/2003 by Ondra
- Optimized: Much faster map drawing for very big islands.
*/

void CStaticMap::DrawBackground()
{
	float invLandRange = 1.0 / LandRange;
	float ptsLand = 800.0 * _invScaleX * invLandRange; // avoid dependece on video resolution
	float invPtsLand = 1.0 / ptsLand;

	float invTerrainRange = 1.0 / TerrainRange;
	float ptsTerrain = 800.0 * _invScaleX * invTerrainRange; // avoid dependece on video resolution
	float invPtsTerrain = 1.0 / ptsTerrain;

//GEngine->ShowMessage(1000, "Scale = %.4f, PtsPerFld = %.2f", _scaleX, ptsLand);
	
	if (_texture)
	{
		// draw paper
		MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(_texture, 0, 0);

		float xStep = (1.0 / 2.0) * _invScaleX;
		float yStep = (1.0 / 2.0) * _invScaleY;
		float xMin = _x - fmod(-_mapX, xStep);
		float yMin = _y - fmod(-_mapY, yStep);
		
		for (float y=yMin; y<_y+_h; y+=yStep)
			for (float x=xMin; x<_x+_w; x+=xStep)
			{
				GEngine->Draw2D
				(
					mip, _bgColor,
					Rect2DPixel(x * _wScreen, y * _hScreen, xStep * _wScreen, yStep * _hScreen),
					_clipRect
				);
			}
	}

	{
		// draw sea
		int iStep = toIntCeil(ptsPerSquareSea * invPtsLand);
		float xStep = iStep * invLandRange * _invScaleX;
		int jStep = iStep;
		float yStep = jStep * invLandRange * _invScaleY;
		int iMin = iStep * toIntFloor(-_mapX / xStep);
		float xMin = _x - fastFmod(-_mapX, xStep);
		int jMin = LandRange - jStep * toIntFloor(-_mapY / yStep);
		float yMin = _y - fastFmod(-_mapY, yStep);

		float y = yMin;
		int j = jMin;
		while (y <= _y + _h)
		{
			float x = xMin;
			int i = iMin;
			while (x <= _x + _w)
			{
				DrawSea(x, y, xStep, yStep, i, j, iStep, -jStep);
				i += iStep;
				x += xStep;
			}
			j -= jStep;
			y += yStep;
		}
	}

//#if _ENABLE_CHEATS
	if (!_showScale)
	{
		// draw fields (textures)
		int iStep = toIntCeil(ptsPerSquareTxt * invPtsLand);
		float xStep = iStep * invLandRange * _invScaleX;
		int jStep = iStep;
		float yStep = jStep * invLandRange * _invScaleY;
		int iMin = iStep * toIntFloor(-_mapX / xStep);
		float xMin = _x - fmod(-_mapX, xStep);
		int jMin = LandRange - jStep * toIntFloor(-_mapY / yStep);
		float yMin = _y - fmod(-_mapY, yStep);

		// calculate mipmap level
		static const float invLog2 = 1.0 / log(2.0);
		_mipmapLevel = 7 - toIntFloor(log(floatMax(xStep * _wScreen, yStep * _hScreen)) * invLog2);
		if (_mipmapLevel < 0 ) _mipmapLevel = 0;
		if (_mipmapLevel > 8 ) _mipmapLevel = 8;

		float y = yMin;
		float j = jMin;
		while (y <= _y + _h)
		{
			float x = xMin;
			int i = iMin;
			while (x <= _x + _w)
			{
				DrawField(x, y, xStep, yStep, i, (int)j, iStep, -jStep);
				i += iStep;
				x += xStep;
			}
			j -= jStep;
			y += yStep;
		}
	}
// #endif

#if _ENABLE_CHEATS
	if (_show)
#endif
	{
		// draw countlines
		{
			int iStep = toIntCeil(ptsPerSquareCLn * invPtsTerrain);
			float xStep = iStep * invTerrainRange * _invScaleX;
			int jStep = iStep;
			float yStep = jStep * invTerrainRange * _invScaleY;
			int iMin = iStep * toIntFloor(-_mapX / xStep);
			float xMin = _x - fmod(-_mapX, xStep);
			int jMin = TerrainRange - jStep * toIntFloor(-_mapY / yStep);
			float yMin = _y - fmod(-_mapY, yStep);

			float step = 50.0 * _scaleX;
			int stepexp = toIntFloor(log10(step));
			float step10 = pow(10.0, stepexp);
			step /= step10;
			if (step <= 2.0) step = 2.0;
			else if (step <= 5.0) step = 5.0;
			else step = 10.0;
			step *= step10;
			float maxLevel = step * toIntCeil(10000.0 / step);

			float y = yMin;
			int j = jMin;
			while (j >= 0 && y <= _y + _h)
			{
				float x = xMin;
				int i = iMin;
				while (i < TerrainRange && x <= _x + _w)
				{
					DrawCountlines(x, y, xStep, yStep, i, j, iStep, -jStep, step, maxLevel);
					i += iStep;
					x += xStep;
				}
				j -= jStep;
				y += yStep;
			}
		}
		// draw objects
		{
			float xStep = invLandRange * _invScaleX;
			float yStep = invLandRange * _invScaleY;

			int iMin = toIntFloor(-_mapX / xStep);
			float xMin = _x - fmod(-_mapX, xStep);
			int jMin = LandRange - toIntFloor(-_mapY / yStep);
			float yMin = _y - fmod(-_mapY, yStep);

			if (ptsLand >= ptsPerSquareFor)
			{
				// forests
				float y = yMin;
				int j = jMin;
				while (j >= 0 && y <= _y + _h)
				{
					float x = xMin;
					int i = iMin;
					while (i < LandRange && x <= _x + _w)
					{
						DrawForests(i, j, x, y, xStep, yStep);
						i++;
						x += xStep;
					}
					j--;
					y += yStep;
				}
			}
			if (ptsLand >= ptsPerSquareForBor)
			{
				// forest borders
				float y = yMin;
				int j = jMin;
				while (j >= 0 && y <= _y + _h)
				{
					float x = xMin;
					int i = iMin;
					while (i < LandRange && x <= _x + _w)
					{
						DrawForestBorders(i, j, x, y, xStep, yStep);
						i++;
						x += xStep;
					}
					j--;
					y += yStep;
				}
			}
			if (ptsLand >= ptsPerSquareRoad)
			{
				// roads
				float y = yMin;
				int j = jMin;
				while (j >= 0 && y <= _y + _h)
				{
					float x = xMin;
					int i = iMin;
					while (i < LandRange && x <= _x + _w)
					{
						DrawRoads(i, j);
						i++;
						x += xStep;
					}
					j--;
					y += yStep;
				}
			}
			if (ptsLand >= ptsPerSquareObj)
			{
				// other objects
				float y = yMin;
				int j = jMin;
				while (j >= 0 && y <= _y + _h)
				{
					float x = xMin;
					int i = iMin;
					while (i < LandRange && x <= _x + _w)
					{
						DrawObjects(i, j);
						i++;
						x += xStep;
					}
					j--;
					y += yStep;
				}
			}
		}
	}

	// names
	const ParamEntry &world = Pars >> "CfgWorlds" >> Glob.header.worldname;
	const ParamEntry *cls = &(world >> "Names");
	if (cls)
	{
		AUTO_STATIC_ARRAY(Vector3, points, 32);
		float minDist2 = Square(1000 * _scaleX);
		int n = cls->GetEntryCount();
		points.Resize(n);
		for (int i=0; i<n; i++)
		{
			const ParamEntry &item = cls->GetEntry(i);
			Vector3 pos;
			pos.Init();
			pos[0] = (item >> "position")[0];
			pos[2] = (item >> "position")[1];
			pos[1] = 0;
			points[i] = pos;
			bool skip = false;
			for (int j=0; j<i; j++)
			{
				if (pos.DistanceXZ2(points[j]) < minDist2)
				{
					skip = true;
					break;
				}
			}
			if (!skip) DrawName(item);
		}
	}
	else
	{
		Fail("Class <Names> expected");
	}

  #if 1
	{
		// mountains
		QuadTreeCont<MountainInfo> mountainPos;

		const AutoArray<Vector3> &mountains = GLandscape->GetMountains();
		for (int i=0; i<mountains.Size(); i++)
		{
			const Vector3& pos = mountains[i];
			// check if given pos is in the visible range
			int x = toLargeInt(pos.X());
			int z = toLargeInt(pos.Z());

			mountainPos.Set(x,z,MountainInfo(pos,i));
		}

		MountainRegion region;
		Vector3 minPos = ScreenToWorld(Point2DFloat(_x,_y));
		Vector3 maxPos = ScreenToWorld(Point2DFloat(_x+_w,_y+_h));
		region._xMin = floatMin(minPos.X(),maxPos.X());
		region._zMin = floatMin(minPos.Z(),maxPos.Z());
		region._xMax = floatMax(minPos.X(),maxPos.X());
		region._zMax = floatMax(minPos.Z(),maxPos.Z());

		MountainList list;


		mountainPos.ForEachInRegion(region,list);

		float minDistF = 1000 * _scaleX;
		int minDist = toLargeInt( 1000 * _scaleX )+2;
		for (int ii=0; ii<list._indices.Size(); ii++)
		{
			int i = list._indices[ii];
			// make space based query
			const Vector3 & pos = mountains[i];

			int x = toLargeInt(pos.X());
			int z = toLargeInt(pos.Z());
			SkipNear ctx(MountainInfo(pos,i),minDistF);

			if (!mountainPos.ForEachInRegion(DetectCircle(x,z,minDist),ctx))
			{
				DrawMount(pos);
			}
		}
	}
  #else
	// mountains
	const AutoArray<Vector3> &mountains = GLandscape->GetMountains();
	float minDist2 = Square(1000 * _scaleX);
	for (int i=0; i<mountains.Size(); i++)
	{
		const Vector3& pos = mountains[i];
		bool skip = false;
		for (int j=0; j<i; j++)
		{
			if (pos.DistanceXZ2(mountains[j]) < minDist2)
			{
				skip = true;
				break;
			}
		}
		if (!skip) DrawMount(pos);
	}
  #endif
}

/*!
\patch_internal 1.45 Date 2/21/2002 by Jirka
- Fixed: Added legend to map
*/
#if _ENABLE_VBS
void CStaticMap::DrawLegend()
{
	float size = _w;
	float x = _x + 0.05 * size;
	float y = _y + _h - 0.15 * size;
	float w = 0.25 * size;
	float h = 0.1 * size;
	float cx = CX(x), cy = CY(y), cw = CW(w), ch = CH(h);

	Draw2DPars pars;
	pars.mip = GEngine->TextBank()->UseMipmap(NULL, 0, 0);
	pars.spec = NoZBuf|IsAlpha|ClampU|ClampV|IsAlphaFog;
	pars.SetColor(PackedWhite);
	pars.SetU(0,1);
	pars.SetV(0,1);
	Rect2DPixel rect(cx, cy, cw, ch);
	GEngine->Draw2D(pars, rect, _clipRect);

	PackedColor color = PackedBlack;
	GEngine->DrawLine(Line2DPixel(cx, cy, cx + cw, cy), color, color, _clipRect);
	GEngine->DrawLine(Line2DPixel(cx + cw, cy, cx + cw, cy + ch), color, color, _clipRect);
	GEngine->DrawLine(Line2DPixel(cx + cw, cy + ch, cx, cy + ch), color, color, _clipRect);
	GEngine->DrawLine(Line2DPixel(cx, cy + ch, cx, cy), color, color, _clipRect);

	float top = y + 0.01 * size;
	float left = x + 0.01 * size;

	float step = 50.0 * _scaleX;
	int stepexp = toIntFloor(log10(step));
	float step10 = pow(10.0, stepexp);
	step /= step10;
	if (step <= 2.0) step = 2.0;
	else if (step <= 5.0) step = 5.0;
	else step = 10.0;
	step *= step10;

	Font *font = _fontGrid;
	GEngine->DrawTextF
	(
		Point2DFloat(left, top), size * 0.02, 
		Rect2DFloat(x, y, w, h),
		font, color, "COUNTOUR INTERVAL %g m", step
	);
	top += size * 0.02;
	GEngine->DrawText
	(
		Point2DFloat(left, top), size * 0.02, 
		Rect2DFloat(x, y, w, h),
		font, color, "Elevations in meters"
	);
	top += size * 0.03;
	
	step *= 10;
	float mapStep = InvLandGrid * InvLandRange * _invScaleX * step;

	GEngine->DrawLine(Line2DPixel(CX(left), CY(top), CX(left), CY(top + size * 0.02)), color, color, _clipRect);
	GEngine->DrawLine(Line2DPixel(CX(left + mapStep), CY(top), CX(left + mapStep), CY(top + size * 0.02)), color, color, _clipRect);
	GEngine->DrawLine(Line2DPixel(CX(left), CY(top + size * 0.01), CX(left + mapStep), CY(top + size * 0.01)), color, color, _clipRect);
	GEngine->DrawTextF
	(
		Point2DFloat(left + mapStep + size * 0.01, top), size * 0.02, 
		Rect2DFloat(x, y, w, h),
		font, color, "%g m", step
	);
}
#endif

void CStaticMap::DrawName(const ParamEntry &cls)
{
	Vector3 pos;
	pos.Init();
	pos[0] = (cls >> "position")[0];
	pos[2] = (cls >> "position")[1];
	pos[1] = 0;
	DrawCoord pt = WorldToScreen(pos);

	float size = _sizeNames * (_invScaleX * 0.05);
	saturate(size, 0.5 * _fontNames->Height(), 2.0 * _fontNames->Height());

	RString text = cls >> "name";
	float h = size;
	float w = GEngine->GetTextWidth(size, _fontNames, text);

	if (pt.x + w < _x) return;
	if (pt.y + 0.5 * h < _y) return;
	if (pt.x > _x + _w) return;
	if (pt.y - 0.5 * h > _y + _h) return;

	GEngine->DrawText
	(
		Point2DFloat(pt.x, pt.y - 0.5 * h),
		size,
		Rect2DFloat(_x, _y, _w, _h),
		_fontNames,
		_colorNames,
		text
	);
}

void CStaticMap::DrawMount(Vector3Par pos)
{
	DrawCoord pt = WorldToScreen(pos);
	float size = 0.02;
	
	char buffer[256];
	sprintf(buffer, "%.0f", pos.Y());

	float h = size;
	float w = GEngine->GetTextWidth(size, _fontNames, buffer);

	if (pt.x + w + 0.005 < _x) return;
	if (pt.y + 0.5 * h < _y) return;
	if (pt.x > _x + _w) return;
	if (pt.y - 0.5 * h > _y + _h) return;

	int x = toIntFloor(pt.x * _wScreen);
	int y = toInt(pt.y * _hScreen);
	GEngine->DrawLine(Line2DPixel(x, y - 1, x + 1, y - 1), _colorCountlines, _colorCountlines, _clipRect);
	GEngine->DrawLine(Line2DPixel(x + 1, y - 1, x + 1, y + 1), _colorCountlines, _colorCountlines, _clipRect);
	GEngine->DrawLine(Line2DPixel(x + 1, y + 1, x, y + 1), _colorCountlines, _colorCountlines, _clipRect);
	GEngine->DrawLine(Line2DPixel(x, y + 1, x, y - 1), _colorCountlines, _colorCountlines, _clipRect);
	GEngine->DrawText
	(
		Point2DFloat(pt.x + 0.005, pt.y - 0.5 * h),
		size,
		Rect2DFloat(_x, _y, _w, _h),
		_fontNames,
		_colorNames,
		buffer
	);
}

#define HEIGHT_LOG (GLandscape->GetTerrainRangeLog()-GLandscape->GetLandRangeLog())

#define GetHeightNT(j,i) GLandscape->GetHeight((j)<<HEIGHT_LOG,(i)<<HEIGHT_LOG)

void CStaticMap::DrawField
(
 float x, float y, float xStep, float yStep,
 int i, int j, int iStep, int jStep
)
{
	if( InRange(i,j) || InRange(i+iStep,j+jStep) )
	{
		// draw only when there is a chance to draw anything
		Draw2DPars pars;
		int maxAlpha8=0;
 
		float alpha = 0.5 + 0.1 * GetHeightNT(j, i);
		int alpha8TL = toIntFloor( alpha * 255 );
		alpha = 0.5 + 0.1 * GetHeightNT(j, i + iStep);
		int alpha8TR = toIntFloor( alpha * 255 );
		alpha = 0.5 + 0.1 * GetHeightNT(j + jStep, i);
		int alpha8BL = toIntFloor( alpha * 255 );
		alpha = 0.5 + 0.1 * GetHeightNT(j + jStep, i + iStep);
		int alpha8BR = toIntFloor( alpha * 255 );
 
		if( maxAlpha8<=alpha8TL ) {if( alpha8TL>255 ) alpha8TL=255;maxAlpha8=alpha8TL;}
		else if (alpha8TL < 0) alpha8TL = 0;
		if( maxAlpha8<=alpha8TR ) {if( alpha8TR>255 ) alpha8TR=255;maxAlpha8=alpha8TR;}
		else if (alpha8TR < 0) alpha8TR = 0;
		if( maxAlpha8<=alpha8BL ) {if( alpha8BL>255 ) alpha8BL=255;maxAlpha8=alpha8BL;}
		else if (alpha8BL < 0) alpha8BL = 0;
		if( maxAlpha8<=alpha8BR ) {if( alpha8BR>255 ) alpha8BR=255;maxAlpha8=alpha8BR;}
		else if (alpha8BR < 0) alpha8BR = 0;
 
		if( maxAlpha8>0 )
		{
			Texture *texture = NULL;
			
			if (iStep == 1 && jStep == -1)
			{
				// TL
				pars.colorTL = Mult1p50( GLOB_LAND->GetRandomColor(i, j, pars.uTL, pars.vTL) );
				pars.colorTL.SetA8(alpha8TL);
				// TR
				pars.colorTR = Mult1p50( GLOB_LAND->GetRandomColor(i + iStep, j, pars.uTR, pars.vTR) );
				pars.colorTR.SetA8(alpha8TR);
				// BL
				pars.colorBL = Mult1p50( GLOB_LAND->GetRandomColor(i, j + jStep, pars.uBL, pars.vBL) );
				pars.colorBL.SetA8(alpha8BL);
				// BR
				pars.colorBR = Mult1p50( GLOB_LAND->GetRandomColor(i + iStep, j + jStep, pars.uBR, pars.vBR ) );
				pars.colorBR.SetA8(alpha8BR);
 
				int id = GLOB_LAND->GetTexture(j + jStep, i);
				texture = GLOB_LAND->GetTexture(id);
				pars.spec = NoZBuf|IsAlpha|IsAlphaFog|DetailTexture|GLOB_LAND->ClampFlags(id);
			}
			else
			{
				// TL
				int id = GLOB_LAND->GetTexture(j, i);
				pars.colorTL = Mult0p75( PackedColor(GLOB_LAND->GetTexture(id)->GetColor()) );
				pars.colorTL.SetA8(alpha8TL);
				// TR
				id = GLOB_LAND->GetTexture(j, i + iStep);
				pars.colorTR = Mult0p75( PackedColor(GLOB_LAND->GetTexture(id)->GetColor()) );
				pars.colorTR.SetA8(alpha8TR);
				// BL
				id = GLOB_LAND->GetTexture(j + jStep, i);
				pars.colorBL = Mult0p75( PackedColor(GLOB_LAND->GetTexture(id)->GetColor()) );
				pars.colorBL.SetA8(alpha8BL);
				// BR
				id = GLOB_LAND->GetTexture(j + jStep, i + iStep);
				pars.colorBR = Mult0p75( PackedColor(GLOB_LAND->GetTexture(id)->GetColor()) );
				pars.colorBR.SetA8(alpha8BR);
 
				pars.spec = NoZBuf|IsAlpha|IsAlphaFog|NoClamp;
			}

			pars.SetU(0, 0);
			pars.SetV(0, 0);
			pars.vTL += 1;
			pars.uTR += 1;
			pars.vTR += 1;
			pars.uBR += 1;
  
			Rect2DPixel rect(x * _wScreen, y * _hScreen, xStep * _wScreen, yStep * _hScreen);

			pars.mip = GLOB_ENGINE->TextBank()->UseMipmap(texture, _mipmapLevel, _mipmapLevel);
			GLOB_ENGINE->Draw2D(pars, rect, _clipRect);
		}
	}
}

void CStaticMap::DrawSea
(
	float x, float y, float xStep, float yStep,
	int i, int j, int iStep, int jStep
)
{
	const float waves = 5.0;
	const float invWaves = 1.0 / (2.0 * waves);

	float heightTL = GetHeightNT(j, i);
	float heightTR = GetHeightNT(j, i + iStep);
	float heightBL = GetHeightNT(j + jStep, i);
	float heightBR = GetHeightNT(j + jStep, i + iStep);
	if
	(
		heightTL >= waves &&
		heightTR >= waves &&
		heightBL >= waves &&
		heightBR >= waves
	) return;

	Draw2DPars pars;

	if
	(
		heightTL <= -waves &&
		heightTR <= -waves &&
		heightBL <= -waves &&
		heightBR <= -waves
	)
	{
		// simple case: sea only
		pars.colorTL = _colorSeaPacked;
		pars.colorTR = _colorSeaPacked;
		pars.colorBL = _colorSeaPacked;
		pars.colorBR = _colorSeaPacked;
	}
	else
	{

		heightTL += waves; heightTL *= invWaves;
		saturate(heightTL, 0, 1);
		heightTR += waves; heightTR *= invWaves;
		saturate(heightTR, 0, 1);
		heightBL += waves; heightBL *= invWaves;
		saturate(heightBL, 0, 1);
		heightBR += waves; heightBR *= invWaves;
		saturate(heightBR, 0, 1);
		
		Color color, colorLand;
		colorLand = _colorSea;
		colorLand.SetA(0);

		// TL
		color = colorLand * heightTL + _colorSea * (1 - heightTL);
		pars.colorTL = PackedColor(color);
		// TR
		color = colorLand * heightTR + _colorSea * (1 - heightTR);
		pars.colorTR = PackedColor(color);
		// BL
		color = colorLand * heightBL + _colorSea * (1 - heightBL);
		pars.colorBL = PackedColor(color);
		// BR
		color = colorLand * heightBR + _colorSea * (1 - heightBR);
		pars.colorBR = PackedColor(color);
	}

	Rect2DPixel rect(x * _wScreen, y * _hScreen, xStep * _wScreen, yStep * _hScreen);
	pars.mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
	pars.spec = NoZBuf|IsAlpha|IsAlphaFog|NoClamp;

	pars.SetU(0, 0);
	pars.SetV(0, 0);
	pars.vTL += 1;
	pars.uTR += 1;
	pars.vTR += 1;
	pars.uBR += 1;

	GLOB_ENGINE->Draw2D(pars, rect, _clipRect);
}

void CStaticMap::DrawScale
(
	float x, float y, float xStep, float yStep,
	int i, int j, int iStep, int jStep
)
{
	const float invMaxHeight = 1.0 / 700.0;

	float heightTL = GetHeightNT(j, i) * invMaxHeight;
	saturate(heightTL, 0, 1);
	float heightTR = GetHeightNT(j, i + iStep) * invMaxHeight;
	saturate(heightTR, 0, 1);
	float heightBL = GetHeightNT(j + jStep, i) * invMaxHeight;
	saturate(heightBL, 0, 1);
	float heightBR = GetHeightNT(j + jStep, i + iStep) * invMaxHeight;
	saturate(heightBR, 0, 1);
	
	Color color, colorL(0, 0, 0, 0), colorH(0, 0, 0, 1);
	Draw2DPars pars;

	// TL
	color = colorH * heightTL + colorL * (1 - heightTL);
	pars.colorTL = PackedColor(color);
	// TR
	color = colorH * heightTR + colorL * (1 - heightTR);
	pars.colorTR = PackedColor(color);
	// BL
	color = colorH * heightBL + colorL * (1 - heightBL);
	pars.colorBL = PackedColor(color);
	// BR
	color = colorH * heightBR + colorL * (1 - heightBR);
	pars.colorBR = PackedColor(color);

	pars.SetU(0, 0);
	pars.SetV(0, 0);
	pars.vTL += 1;
	pars.uTR += 1;
	pars.vTR += 1;
	pars.uBR += 1;

	Rect2DPixel rect(x * _wScreen, y * _hScreen, xStep * _wScreen, yStep * _hScreen);

	pars.mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
	pars.spec = NoZBuf|IsAlpha|IsAlphaFog|NoClamp;
	GLOB_ENGINE->Draw2D(pars, rect, _clipRect);
}

void CStaticMap::DrawForests(int i, int j, float x, float y, float w, float h)
{
	if (!InRange(i, j - 1)) return;

	const ObjectList &list = GLOB_LAND->GetObjects(j - 1, i);
	for (int o=0; o<list.Size(); o++)
	{
		Object *obj = list[o];
		if (obj == NULL) continue;
		if (obj->GetType() == Primary)
		{
			switch (obj->GetShape()->GetMapType())
			{
			case MapForestTriangle:
				{
					const int n = 3;
					Vertex2DPixel vs[n];
					vs[0].u = 0;
					vs[0].v = 0;
					vs[0].color = _colorForest;
					vs[1].u = 0;
					vs[1].v = 0;
					vs[1].color = _colorForest;
					vs[2].u = 0;
					vs[2].v = 0;
					vs[2].color = _colorForest;

					Vector3 dir = obj->Direction();
					float angle = atan2(dir.X(), dir.Z());
					switch (toInt(angle * 2.0 / H_PI))
					{
					case -1:
						vs[0].x = x * _wScreen;
						vs[0].y = (y + h) * _hScreen;
						vs[1].x = x * _wScreen;
						vs[1].y = y * _hScreen;
						vs[2].x = (x + w) * _wScreen;
						vs[2].y = (y + h) * _hScreen;
						break;
					case 0:
						vs[0].x = x * _wScreen;
						vs[0].y = y * _hScreen;
						vs[1].x = (x + w) * _wScreen;
						vs[1].y = y * _hScreen;
						vs[2].x = x * _wScreen;
						vs[2].y = (y + h) * _hScreen;
						break;
					case 1:
						vs[0].x = x * _wScreen;
						vs[0].y = y * _hScreen;
						vs[1].x = (x + w) * _wScreen;
						vs[1].y = y * _hScreen;
						vs[2].x = (x + w) * _wScreen;
						vs[2].y = (y + h) * _hScreen;
						break;
					case -2:
					case 2:
						vs[0].x = x * _wScreen;
						vs[0].y = (y + h) * _hScreen;
						vs[1].x = (x + w) * _wScreen;
						vs[1].y = y * _hScreen;
						vs[2].x = (x + w) * _wScreen;
						vs[2].y = (y + h) * _hScreen;
						break;
					}
					MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
					GLOB_ENGINE->DrawPoly(mip, vs, n, _clipRect);
				}
				return;
			case MapForestSquare:
				{
					Draw2DPars pars;
					pars.mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
					pars.spec = NoZBuf|IsAlpha|ClampU|ClampV|IsAlphaFog;
					pars.SetColor(_colorForest);
					pars.SetU(0,1);
					pars.SetV(0,1);
					Rect2DPixel rect(x  * _wScreen, y * _hScreen, w * _wScreen, h * _hScreen);
					GLOB_ENGINE->Draw2D(pars, rect, _clipRect);
				}
				return;	// only one forest in square is enabled
			}
		}
	}
}

void CStaticMap::DrawObjects(int i, int j)
{
	if (!InRange(i, j - 1)) return;

	const ObjectList &list = GLOB_LAND->GetObjects(j - 1, i);
	for (int o=0; o<list.Size(); o++)
	{
		Object *obj = list[o];
		if (obj == NULL) continue;
		if (obj->GetType() == Primary)
		{
			switch (obj->GetShape()->GetMapType())
			{
			case MapBuilding:
			case MapHouse:
			case MapFence:
			case MapWall:
				{
					PackedColor color = obj->GetShape()->Color();
					const Point3 *minmax = obj->GetShape()->MinMax();
					Point3 ptTL(minmax[0].X(), 0, minmax[0].Z());
					Point3 ptTR(minmax[1].X(), 0, minmax[0].Z());
					Point3 ptBL(minmax[0].X(), 0, minmax[1].Z());
					Point3 ptBR(minmax[1].X(), 0, minmax[1].Z());
					DrawCoord mapTL = WorldToScreen(obj->PositionModelToWorld(ptTL));
					DrawCoord mapTR = WorldToScreen(obj->PositionModelToWorld(ptTR));
					DrawCoord mapBL = WorldToScreen(obj->PositionModelToWorld(ptBL));
					DrawCoord mapBR = WorldToScreen(obj->PositionModelToWorld(ptBR));

					const int n = 4;
					Vertex2DPixel vs[n];
					// 0
					vs[0].x = mapTL.x * _wScreen;
					vs[0].y = mapTL.y * _hScreen;
					vs[0].u = 0;
					vs[0].v = 0;
					vs[0].color = color;
					// 1
					vs[1].x = mapBL.x * _wScreen;
					vs[1].y = mapBL.y * _hScreen;
					vs[1].u = 0;
					vs[1].v = 0;
					vs[1].color = color;
					// 2
					vs[2].x = mapBR.x * _wScreen;
					vs[2].y = mapBR.y * _hScreen;
					vs[2].u = 0;
					vs[2].v = 0;
					vs[2].color = color;
					// 3
					vs[3].x = mapTR.x * _wScreen;
					vs[3].y = mapTR.y * _hScreen;
					vs[3].u = 0;
					vs[3].v = 0;
					vs[3].color = color;

					MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
					GLOB_ENGINE->DrawPoly(mip, vs, n, _clipRect);
				}
				break;
			case MapTree:
				DrawSign(_infoTree, obj->Position());
				break;
			case MapSmallTree:
				DrawSign(_infoSmallTree, obj->Position());
				break;
			case MapBush:
				DrawSign(_infoBush, obj->Position());
				break;
			case MapChurch:
				DrawSign(_infoChurch, obj->Position());
				break;
			case MapChapel:
				DrawSign(_infoChapel, obj->Position());
				break;
			case MapCross:
				DrawSign(_infoCross, obj->Position());
				break;
			case MapRock:
				DrawSign(_infoRock, obj->Position());
				break;
			case MapBunker:
				DrawSign(_infoBunker, obj->Position());
				break;
			case MapFortress:
				DrawSign(_infoFortress, obj->Position());
				break;
			case MapFountain:
				DrawSign(_infoFountain, obj->Position());
				break;
			case MapViewTower:
				DrawSign(_infoViewTower, obj->Position());
				break;
			case MapLighthouse:
				DrawSign(_infoLighthouse, obj->Position());
				break;
			case MapQuay:
				DrawSign(_infoQuay, obj->Position());
				break;
			case MapFuelstation:
				DrawSign(_infoFuelstation, obj->Position());
				break;
			case MapHospital:
				DrawSign(_infoHospital, obj->Position());
				break;
			case MapBusStop:
				DrawSign(_infoBusStop, obj->Position());
				break;
			}

			if (_showIds)
			{
				float invLandRange = 1.0 / LandRange;
				float ptsLand = 800.0 * _invScaleX * invLandRange;	// avoid dependece on video resolution
				float invPtsLand = 1.0 / ptsLand;

				int iStep = toIntCeil(ptsPerSquareCost * invPtsLand);
				float xStep = iStep * invLandRange * _invScaleX;
				if (xStep>0.15)
				{
					// draw object ID
					DrawCoord pos = WorldToScreen(obj->Position());
					const float size = 0.02;
					float width = GEngine->GetTextWidthF(size, _fontGrid, "%d", obj->ID());
					float x = pos.x - 0.5 * width;
					float y = pos.y - 0.5 * size;
					MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
					GEngine->Draw2D
					(
						mip, PackedWhite,
						Rect2DPixel(x * _wScreen, y * _hScreen, width * _wScreen, size * _hScreen),
						Rect2DPixel(_x * _wScreen, _y * _hScreen, _w * _wScreen, _h * _hScreen)
					);
					GEngine->DrawTextF
					(
						Point2DFloat(x, y), size, 
						Rect2DFloat(_x, _y, _w, _h),
						_fontGrid, PackedBlack, "%d", obj->ID()
					);
				}
			}
		}
	}
}

void CStaticMap::DrawForestBorders(int i, int j, float x, float y, float w, float h)
{
	if (!InRange(i, j - 1)) return;

	const ObjectList &list = GLOB_LAND->GetObjects(j - 1, i);
	for (int o=0; o<list.Size(); o++)
	{
		Object *obj = list[o];
		if (obj == NULL) continue;
		if (obj->GetType() == Primary)
		{
			switch (obj->GetShape()->GetMapType())
			{
			case MapForestTriangle:
				{
					Vector3 dir = obj->Direction();
					float angle = atan2(dir.X(), dir.Z());
					switch (toInt(angle * 2.0 / H_PI))
					{
					case -1:
					case 1:
						GLOB_ENGINE->DrawLine
						(
							Line2DPixel(x * _wScreen, y * _hScreen,
							(x + w) * _wScreen, (y + h) * _hScreen),
							_colorForestBorder, _colorForestBorder, _clipRect
						);
						break;
					case -2:
					case 0:
					case 2:
						GLOB_ENGINE->DrawLine
						(
							Line2DPixel(x * _wScreen, (y + h) * _hScreen,
							(x + w) * _wScreen, y * _hScreen),
							_colorForestBorder, _colorForestBorder, _clipRect
						);
						break;
					}
				}
				break;
			case MapForestSquare:
				{
					GeographyInfo geogr = GLOB_LAND->GetGeography(i, j);
					if (!geogr.u.forestInner && !geogr.u.forestOuter)
					{
						GLOB_ENGINE->DrawLine
						(
							Line2DPixel(x * _wScreen, y * _hScreen,
							(x + w) * _wScreen, y * _hScreen),
							_colorForestBorder, _colorForestBorder, _clipRect
						);
					}
					geogr = GLOB_LAND->GetGeography(i, j - 2);
					if (!geogr.u.forestInner && !geogr.u.forestOuter)
					{
						GLOB_ENGINE->DrawLine
						(
							Line2DPixel(x * _wScreen, (y + h) * _hScreen,
							(x + w) * _wScreen, (y + h) * _hScreen),
							_colorForestBorder, _colorForestBorder, _clipRect
						);
					}
					geogr = GLOB_LAND->GetGeography(i - 1, j - 1);
					if (!geogr.u.forestInner && !geogr.u.forestOuter)
					{
						GLOB_ENGINE->DrawLine
						(
							Line2DPixel(x * _wScreen, y * _hScreen,
							x * _wScreen, (y + h) * _hScreen),
							_colorForestBorder, _colorForestBorder, _clipRect
						);
					}
					geogr = GLOB_LAND->GetGeography(i + 1, j - 1);
					if (!geogr.u.forestInner && !geogr.u.forestOuter)
					{
						GLOB_ENGINE->DrawLine
						(
							Line2DPixel((x + w) * _wScreen, y * _hScreen,
							(x + w) * _wScreen, (y + h) * _hScreen),
							_colorForestBorder, _colorForestBorder, _clipRect
						);
					}
				}
			}
		}
	}
}

void CStaticMap::DrawRoads(int i, int j)
{
	if (!InRange(i, j - 1)) return;

	const ObjectList &list = GLOB_LAND->GetObjects(j - 1, i);
	for (int o=0; o<list.Size(); o++)
	{
		Object *obj = list[o];
		if (obj == NULL) continue;
		if (obj->GetType() == Network)
		{
			LODShape *lShape = obj->GetShape();
			Point3 ptTL = lShape->MemoryPoint("LB");
			Point3 ptTR = lShape->MemoryPoint("PB");
			Point3 ptBL = lShape->MemoryPoint("LE");
			Point3 ptBR = lShape->MemoryPoint("PE");

			if (!lShape->MemoryLevel())
			{
				// no memory  - syntetic points required
				Shape *geom = lShape->GeometryLevel();
				if (geom)
				{

					Vector3Val min=geom->Min();
					Vector3Val max=geom->Max();

					ptTL = Vector3(min.X(),max.Y(),min.Z());
					ptTR = Vector3(max.X(),max.Y(),min.Z());
					ptBL = Vector3(min.X(),max.Y(),max.Z());
					ptBR = Vector3(max.X(),max.Y(),max.Z());
				}
			}

			DrawCoord mapTL = WorldToScreen(obj->PositionModelToWorld(ptTL));
			DrawCoord mapTR = WorldToScreen(obj->PositionModelToWorld(ptTR));
			DrawCoord mapBL = WorldToScreen(obj->PositionModelToWorld(ptBL));
			DrawCoord mapBR = WorldToScreen(obj->PositionModelToWorld(ptBR));
			GLOB_ENGINE->DrawLine
			(
				Line2DPixel(mapTL.x * _wScreen, mapTL.y * _hScreen,
				mapBL.x * _wScreen, mapBL.y * _hScreen),
				_colorRoads, _colorRoads, _clipRect
			);
			GLOB_ENGINE->DrawLine
			(
				Line2DPixel(mapTR.x * _wScreen, mapTR.y * _hScreen,
				mapBR.x * _wScreen, mapBR.y * _hScreen),
				_colorRoads, _colorRoads, _clipRect
			);

			if (_showIds)
			{
				float invLandRange = 1.0 / LandRange;
				float ptsLand = 800.0 * _invScaleX * invLandRange;	// avoid dependece on video resolution
				float invPtsLand = 1.0 / ptsLand;

				int iStep = toIntCeil(ptsPerSquareCost * invPtsLand);
				float xStep = iStep * invLandRange * _invScaleX;
				if (xStep>0.15)
				{
					// draw object ID
					const float size = 0.02;
					float x = (mapTL.x+mapBR.x)*0.5;
					float y = (mapTL.y+mapBR.y)*0.5;
					float width = GEngine->GetTextWidthF(size, _fontGrid, "%d", obj->ID());
					MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
					GEngine->Draw2D
					(
						mip, PackedWhite,
						Rect2DPixel(x * _wScreen, y * _hScreen, width * _wScreen, size * _hScreen),
						Rect2DPixel(_x * _wScreen, _y * _hScreen, _w * _wScreen, _h * _hScreen)
					);
					GEngine->DrawTextF
					(
						Point2DFloat(x, y), size, 
						Rect2DFloat(_x, _y, _w, _h),
						_fontGrid, PackedBlack, "%d", obj->ID()
					);
				}
			}
		}
	}
}

/*!
\patch 1.62 Date 5/31/2002 by Ondra
- Fixed: Map contour drawing was not correct in some places
with highly variable gradient.
*/

void CStaticMap::DrawLines
(
	DrawCoord *pt, float *height,
	float step, float minLevel, float maxLevel, PackedColor color
)
{
	float invStep = 1.0 / step;

	int n0, n1, n2, nt;
	float xs, ys, xe, ye;
	// original (wrong):
	// draw triangle <0, 1, 2>
	// draw triangle <1, 2, 3>
	// correct:
	// draw triangle <0, 1, 3>
	// draw triangle <0, 2, 3>
	for (int t=0; t<2; t++)
	{
		// t = 0, 1
		// draw triangel <t, t+	1, t+2>
		n0 = 0; n1 = t + 1; n2 = 3;
		// sort vertices by height
		if (height[n0] > height[n1])
		{ // swap n0, n1
			nt = n0; n0 = n1; n1 = nt;
		}
		if (height[n0] > height[n2])
		{ // swap n0, n2
			nt = n0; n0 = n2; n2 = nt;
		}
		if (height[n1] > height[n2])
		{ // swap n1, n2
			nt = n1; n1 = n2; n2 = nt;
		}

		float level = step * toIntCeil(height[n0] * invStep);
		saturateMax(level, minLevel);
		float toLevel = floatMin(height[n2], maxLevel);
		for (; level<toLevel; level+=step)
		{
			// draw one line (at level <level>)
			float coef = (level - height[n0]) * (1.0 / (height[n2] - height[n0]));
			xe = pt[n0].x + coef * (pt[n2].x - pt[n0].x);
			ye = pt[n0].y + coef * (pt[n2].y - pt[n0].y);
			if (level == height[n1])
			{
				xs = pt[n1].x;
				ys = pt[n1].y;
			}
			else if (level < height[n1])
			{
				float coef = (level - height[n0]) * (1.0 / (height[n1] - height[n0]));
				xs = pt[n0].x + coef * (pt[n1].x - pt[n0].x);
				ys = pt[n0].y + coef * (pt[n1].y - pt[n0].y);
			}
			else
			{
				float coef = (level - height[n1]) * (1.0 / (height[n2] - height[n1]));
				xs = pt[n1].x + coef * (pt[n2].x - pt[n1].x);
				ys = pt[n1].y + coef * (pt[n2].y - pt[n1].y);
			}
			// draw line from <xs, ys> to <xe, ye>
			GLOB_ENGINE->DrawLine
			(
				Line2DPixel(xs * _wScreen, ys * _hScreen,
				xe * _wScreen, ye * _hScreen),
				color, color, _clipRect
			);
		}
	}
}

void CStaticMap::DrawCountlines
(
	float x, float y, float xStep, float yStep,
	int i, int j, int iStep, int jStep,
	float step, float maxLevel
)
{
	// insert bounds into fields
	DrawCoord pt[4];
	float height[4];
	pt[0].x = x; pt[0].y = y;
	pt[1].x = x + xStep; pt[1].y = y;
	pt[2].x = x; pt[2].y = y + yStep;
	pt[3].x = x + xStep; pt[3].y = y + yStep;
	height[0] = GLOB_LAND->GetHeight(j, i);
	height[1] = GLOB_LAND->GetHeight(j, i + iStep);
	height[2] = GLOB_LAND->GetHeight(j + jStep, i);
	height[3] = GLOB_LAND->GetHeight(j + jStep, i + iStep);

	DrawLines(pt, height, step, -maxLevel, 0.5 * step, _colorCountlinesWater);
	DrawLines(pt, height, step, step, maxLevel, _colorCountlines);
}

/*!
\patch_internal 1.45 Date 2/21/2002 by Jirka
- Improved: Map grid format controlled by config
*/

void CStaticMap::DrawGrid()
{
	float sizeLand = LandGrid * LandRange;
	float invSizeLand = 1.0f / sizeLand;

	const GridInfo *info = GWorld->GetGridInfo(_scaleX);
	if (!info) return;

	float offsetX = GWorld->GetGridOffsetX();
	float offsetY = GWorld->GetGridOffsetY();

	float coefX = sizeLand * info->invStepX;
	float invCoefX = invSizeLand * info->stepX;
	
	float coefY = sizeLand * info->invStepY;
	float invCoefY = invSizeLand * info->stepY;

	int xFrom = TO_INT(coefX, (-_mapX * _scaleX * sizeLand - offsetX) * info->invStepX);
	int zFrom = TO_INT(coefY, (-_mapY * _scaleY * sizeLand - offsetY) * info->invStepY);
	int xTo = TO_INT(coefX, ((_w - _mapX) * _scaleX * sizeLand - offsetX) * info->invStepX);
	int zTo = TO_INT(coefY, ((_h - _mapY) * _scaleY * sizeLand - offsetY) * info->invStepY);

	float xBeg = _x + _mapX + (xFrom * info->stepX + offsetX) * _invScaleX * invSizeLand;
	float zBeg = _y + _mapY + (zFrom * info->stepY + offsetY) * _invScaleY * invSizeLand;
	float xFld = fabs(_invScaleX * invCoefX);
	float zFld = fabs(_invScaleY * invCoefY);

	float h = _sizeGrid;

	float zAct = zBeg;
	int zSign = coefY > 0 ? 1 : -1; 
	for (int z = zFrom; zSign * (zTo - z) >= 0; z+=zSign)
	{
		if (zAct >= _y && zAct <= _y + _h)
			GLOB_ENGINE->DrawLine
			(
				Line2DPixel(_clipRect.x, zAct * _hScreen,
				_clipRect.x + _clipRect.w, zAct * _hScreen),
				_colorGridMap, _colorGridMap
			);
		BString<32> buffer;
		GridFormat(buffer, info->formatY, zSign == 1 ? z : z - 1);
		float width = GEngine->GetTextWidth(_sizeGrid, _fontGrid, buffer);
		float left = _x;
		float right = _x + _w - width;
		float top = zAct + 0.5 * (zFld - h);
		GLOB_ENGINE->DrawText
		(
			Point2DFloat(left, top), _sizeGrid,
			Rect2DFloat(_x, _y, _w, _h),
			_fontGrid, _colorGrid, buffer
		);
		GLOB_ENGINE->DrawText
		(
			Point2DFloat(right, top), _sizeGrid,
			Rect2DFloat(_x, _y, _w, _h),
			_fontGrid, _colorGrid, buffer
		);
		zAct += zFld;
	}
	float xAct = xBeg;
	int xSign = coefX > 0 ? 1 : -1; 
	for (int x = xFrom; xSign * (xTo - x) >= 0; x+=xSign)
	{
		if (xAct >= _x && xAct <= _x + _w)
			GLOB_ENGINE->DrawLine
			(
				Line2DPixel(xAct * _wScreen, _clipRect.y,
				xAct * _wScreen, _clipRect.y + _clipRect.h),
				_colorGridMap, _colorGridMap
			);
		BString<32> buffer;
		GridFormat(buffer, info->formatX, xSign == 1 ? x : x - 1);

		float left = xAct + 0.5 * 
		(
			xFld - GEngine->GetTextWidth(_sizeGrid, _fontGrid, buffer)
		);
		float top = _y;
		float bottom = _y + _h - _sizeGrid;
		GLOB_ENGINE->DrawText
		(
			Point2DFloat(left, top), _sizeGrid,
			Rect2DFloat(_x, _y, _w, _h),
			_fontGrid, _colorGrid, buffer
		);
		GLOB_ENGINE->DrawText
		(
			Point2DFloat(left, bottom), _sizeGrid,
			Rect2DFloat(_x, _y, _w, _h),
			_fontGrid, _colorGrid, buffer
		);
		xAct += xFld;
	}
}

void CStaticMap::OnDraw(float alpha)
{
	Precalculate();
	unsigned key = _mouseKey;
	if (key == 0) key = _moveKey;
	if (key != 0)
	{
		float dt = Glob.uiTime - _moveLast;
		_moveLast = Glob.uiTime;
		if (Glob.uiTime - _moveStart < 0.5) dt *= 0.5;

		switch (key)
		{
		case VK_ADD:
			SetScale(exp(-dt) * GetScale());
			break;
		case VK_SUBTRACT:
			SetScale(exp(dt) * GetScale());
			break;
		case VK_NUMPAD1:
			_mapX += dt; SaturateX(_mapX);
			_mapY -= dt; SaturateY(_mapY);
			break;
		case VK_NUMPAD2:
			_mapY -= dt; SaturateY(_mapY);
			break;
		case VK_NUMPAD3:
			_mapX -= dt; SaturateX(_mapX);
			_mapY -= dt; SaturateY(_mapY);
			break;
		case VK_NUMPAD4:
			_mapX += dt; SaturateX(_mapX);
			break;
		case VK_NUMPAD6:
			_mapX -= dt; SaturateX(_mapX);
			break;
		case VK_NUMPAD7:
			_mapX += dt; SaturateX(_mapX);
			_mapY += dt; SaturateY(_mapY);
			break;
		case VK_NUMPAD8:
			_mapY += dt; SaturateY(_mapY);
			break;
		case VK_NUMPAD9:
			_mapX -= dt; SaturateX(_mapX);
			_mapY += dt; SaturateY(_mapY);
			break;
		}
	}
	else if (_interpolator)
	{
		float t = Glob.uiTime - _animationStart;
		Vector3 pos = (*_interpolator)(t).Position();
		_scaleX = exp(pos[1]);
		pos[1] = 0;
		saturate(_scaleX, _scaleMin, _scaleMax);
		Precalculate();
		Center(pos);
		_animationLast = Glob.uiTime;
		if (t > _interpolator->MaxArg())
			ClearAnimation();
	}
	
	DrawBackground();

#if _ENABLE_CHEATS
	if (_show)
#endif
		DrawGrid();

	DrawExt(alpha);

	if (_parent->IsTop() && !GWorld->GetCameraEffect())
	{
		RString cursorName = _parent->GetCursorName();
		if (stricmp(cursorName, "Arrow") != 0)
		{
			int wScreen = GLOB_ENGINE->Width2D();
			int hScreen = GLOB_ENGINE->Height2D();
			const float mouseH = 16.0 / 600;
			const float mouseW = 16.0 / 800;
			float mouseX = 0.5 + GInput.cursorX * 0.5;
			float mouseY = 0.5 + GInput.cursorY * 0.5;
			PackedColor color = _parent->GetCursorColor();
			color.SetA8(color.A8() / 2);
			float pt = mouseX - mouseW;
			if (pt > _x)
				GLOB_ENGINE->DrawLine
				(
					Line2DPixel(wScreen * _x, toInt(hScreen * mouseY),
					wScreen * pt, toInt(hScreen * mouseY)),
					color, color 
				);
			pt = mouseX + mouseW;
			if (pt < _x + _w)
				GLOB_ENGINE->DrawLine
				(
					Line2DPixel(wScreen * pt, toInt(hScreen * mouseY),
					wScreen * (_x + _w), toInt(hScreen * mouseY)),
					color, color 
				);
			pt = mouseY - mouseH;
			if (pt > _y)
				GLOB_ENGINE->DrawLine
				(
					Line2DPixel(toInt(wScreen * mouseX), hScreen * _y,
					toInt(wScreen * mouseX), hScreen * pt),
					color, color 
				);
			pt = mouseY + mouseH;
			if (pt < _y + _h)
				GLOB_ENGINE->DrawLine
				(
					Line2DPixel(toInt(wScreen * mouseX), hScreen * pt,
					toInt(wScreen * mouseX), hScreen * (_y + _h)),
					color, color 
				);
		}
	}
}

// mouse / keyboard events
bool CStaticMap::OnKeyUp(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	switch (nChar)
	{
		case VK_ADD:
		case VK_SUBTRACT:
		case VK_NUMPAD1:
		case VK_NUMPAD2:
		case VK_NUMPAD3:
		case VK_NUMPAD4:
//	case VK_NUMPAD5:
		case VK_NUMPAD6:
		case VK_NUMPAD7:
		case VK_NUMPAD8:
		case VK_NUMPAD9:
			_moveKey = 0;
			return true;
		default:
			return false;
	}
}

bool CStaticMap::OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	switch (nChar)
	{
		case VK_ADD:
		case VK_SUBTRACT:
		case VK_NUMPAD1:
		case VK_NUMPAD2:
		case VK_NUMPAD3:
		case VK_NUMPAD4:
		case VK_NUMPAD6:
		case VK_NUMPAD7:
		case VK_NUMPAD8:
		case VK_NUMPAD9:
			ClearAnimation();
			if (_moveKey != nChar)
			{
				_moveKey = nChar;
				_moveStart = Glob.uiTime;
				_moveLast = Glob.uiTime;
			}
			return true;
/*
		case VK_NUMPAD5:
			Center();
			return true;
*/
		case VK_MULTIPLY:
			_moveKey = 0;
			_mouseKey = 0;
			float time;
			if (_scaleDefault > _scaleX)
				time = log(_scaleDefault / _scaleX);
			else
				time = log(_scaleX / _scaleDefault);
			ClearAnimation();
			AddAnimationPhase(time, _scaleDefault, GetCenter());
			CreateInterpolator();
			return true;
		default:
			return false;
	}
}

void ExportWMF(const char *name, bool grid);

void CStaticMap::ProcessCheats()
{
	if (GInput.CheatActivated() == CheatExportMap)
	{
		char buffer[256];
		sprintf(buffer, "C:\\%s.emf", Glob.header.worldname);
		ExportWMF(buffer, true);
		GInput.CheatServed();
	}
#if _ENABLE_CHEATS
	if (GInput.GetCheat2ToDo(DIK_D))
	{
		char buffer[256];
		sprintf(buffer, "C:\\%s.emf", Glob.header.worldname);
		ExportWMF(buffer, true);
	}
	if (GInput.GetCheat2ToDo(DIK_F))
	{
		char buffer[256];
		sprintf(buffer, "C:\\%s.emf", Glob.header.worldname);
		ExportWMF(buffer, false);
	}
	/*
	if (GInput.GetCheat1ToDo(DIK_V))
	{
		_showCost = !_showCost;
	}
	*/
/*
	if (GInput.GetCheat1ToDo(DIK_B))
	{
		_showScale = !_showScale;
	}
*/
	if (GInput.GetCheat1ToDo(DIK_N))
	{
		_show = !_show;
	}
	if (GInput.GetCheat1ToDo(DIK_W))
	{
		switch (_infoClick._type)
		{
		case signUnit:
			if (_infoClick._unit)
			{
				EntityAI *veh = _infoClick._unit->GetVehicle();
				if (veh)
				{
					GLOB_WORLD->SwitchCameraTo(veh, GLOB_WORLD->GetCameraType());
					GWorld->UI()->ResetHUD();
				}
			}
			break;
		case signVehicle:
		case signStatic:
			if (_infoClick._id!=NULL)
			{
				GLOB_WORLD->SwitchCameraTo(_infoClick._id, GLOB_WORLD->GetCameraType());
				GWorld->UI()->ResetHUD();
			}
			break;
		}
	}
#endif
}

void CStaticMap::OnRButtonDown(float x, float y)
{
	_mouseWorld = ScreenToWorld(DrawCoord(x, y));
	_moving = true;
}

void CStaticMap::OnRButtonUp(float x, float y)
{
	_moving = false;
}

void CStaticMap::OnMouseHold(float x, float y, bool active)
{
/*
	if (_moveOnEdges)
	{
		// automatic map movement on edges
		float dif = _x + 0.02 - x;
		if (dif > 0)
		{
			_mapX += 0.003 * exp(dif * 100.0f);
			SaturateX(_mapX);
		}
		else
		{
			dif = x - (_x + _w - 0.02);
			if (dif > 0)
			{
				_mapX -= 0.003 * exp(dif * 100.0f);
				SaturateX(_mapX);
			}
		}

		dif = _y + 0.02 - y;
		if (dif > 0)
		{
			_mapY += 0.003 * exp(dif * 100.0f);
			SaturateY(_mapY);
		}
		else
		{
			dif = y - (_y + _h - 0.02);
			if (dif > 0)
			{
				_mapY -= 0.003 * exp(dif * 100.0f);
				SaturateY(_mapY);
			}
		}
	}
*/

	if (!_moving && IsInside(x, y))
		_infoMove = FindSign(x, y);
}

void CStaticMap::OnMouseMove(float x, float y, bool active)
{
	if (_moving)
	{
		DrawCoord mouseMap = WorldToScreen(_mouseWorld);
		_mapX += x - mouseMap.x;
		SaturateX(_mapX);
		_mapY += y - mouseMap.y;
		SaturateY(_mapY);
	}
	else
	{
		OnMouseHold(x, y);
		return;
	}
}

void CStaticMap::OnMouseZChanged(float dz)
{
	if (dz == 0) return;
	_moveKey = 0;
	float scale = exp(0.1 * dz) * _scaleX;
	saturate(scale, _scaleMin, _scaleMax);
	SetScale(scale);
/*
	if (dz == 0)
	{
		_mouseKey = 0;
	}
	else if (dz < 0)
	{
		_mouseKey = VK_SUBTRACT;
		_moveStart = Glob.uiTime;
		_moveLast = Glob.uiTime;
	}
	else
	{
		_mouseKey = VK_ADD;
		_moveStart = Glob.uiTime;
		_moveLast = Glob.uiTime;
	}
*/
}

void CStaticMap::ClearAnimation()
{
	_animation.Resize(0);
	_animMatrices.Free();
	_animTimes.Free();
	_interpolator = NULL;
}

void CStaticMap::AddAnimationPhase(float dt, float scale, Vector3Par pos)
{
	int i = _animation.Add();
	MapAnimationPhase &phase = _animation[i];
	if (i == 0)
	{
		_animationStart = Glob.uiTime;
		_animationLast = Glob.uiTime;
		phase.time = Glob.uiTime + dt;
	}
	else
	{
		phase.time = _animation[i - 1].time + dt;
	}
	phase.scale = scale;
	phase.pos = pos;
}

void CStaticMap::CreateInterpolator()
{
	int n = _animation.Size();

	_animTimes.Realloc(n + 1);
	_animMatrices.Realloc(n + 1);
	Vector3 pos;

	pos = GetCenter();
	pos[1] = log(_scaleX);
	_animTimes[0] = 0;
	_animMatrices[0].SetPosition(pos);
	_animMatrices[0].SetOrientation(M3Identity);
	for (int i=0; i<n; i++)
	{
		pos = _animation[i].pos;
		pos[1] = log(_animation[i].scale);
		_animTimes[i + 1] = _animation[i].time - _animationStart;
		_animMatrices[i + 1].SetPosition(pos);
		_animMatrices[i + 1].SetOrientation(M3Identity);
	}
	_interpolator = new InterpolatorLinear(_animMatrices, _animTimes, n + 1);
}

Vector3 CStaticMap::GetCenter()
{
	DrawCoord pt(_center.x + _x, _center.y + _y);
	return ScreenToWorld(pt);
}

// implementation

void CStaticMap::Precalculate()
{
	// precalculates for increasing drawing speed
	_wScreen = GLOB_ENGINE->Width2D();
	_hScreen = GLOB_ENGINE->Height2D();

	float coef = _hScreen / _wScreen;
	_scaleY = coef * _scaleX;
	_invScaleX = 1.0 / _scaleX;
	_invScaleY = 1.0 / _scaleY;

	_clipRect.x = _x * _wScreen;
	_clipRect.y = _y * _hScreen;
	_clipRect.w = _w * _wScreen;
	_clipRect.h = _h * _hScreen;
}

void CStaticMap::SaturateX(float &x)
{
	if (_invScaleX < _w)
		x = 0.5 * (_w - _invScaleX);
	else
		saturate(x, _w - _invScaleX, 0);
}

void CStaticMap::SaturateY(float &y)
{
	if (_invScaleY < _h)
		y = 0.5 * (_h - _invScaleY);
	else
		saturate(y, _h - _invScaleY, 0);
}

void CStaticMap::ScrollX(float dif)
{
	_mapX += dif;
	SaturateX(_mapX);
}

void CStaticMap::ScrollY(float dif)
{
	_mapY += dif;
	SaturateY(_mapY);
}

void CStaticMap::SetVisibleRect(float x, float y, float w, float h)
{
	_center.x = x - _x + 0.5 * w; // offset + 0.5 * width
	_center.y = y - _y + 0.5 * h; // offset + 0.5 * height
}

void CStaticMap::Center(Vector3Val pt)
{
	float invSizeLand = 1.0 / (LandGrid * LandRange);
	float xMap = pt.X() * invSizeLand * _invScaleX;
	float yMap = (1.0 - pt.Z() * invSizeLand) * _invScaleY;
	_mapX = _center.x - xMap;
	_mapY = _center.y - yMap;
	SaturateX(_mapX);
	SaturateY(_mapY);
}

void CStaticMap::SetScale(float scale)
{
	if (scale < 0) scale = _scaleDefault;
	saturate(scale, _scaleMin, _scaleMax);

	float mouseX = 0.5 + GInput.cursorX * 0.5;
	saturate(mouseX, _x, _x + _w);
	float mouseY = 0.5 + GInput.cursorY * 0.5;
	saturate(mouseY, _y, _y + _h);

	DrawCoord ptMap(mouseX, mouseY);
	DrawCoord offset(mouseX - _x, mouseY - _y);
	Vector3 pt = ScreenToWorld(ptMap);

	_scaleX = scale;
	Precalculate();

	float invSizeLand = 1.0 / (LandGrid * LandRange);
	float xMap = pt.X() * invSizeLand * _invScaleX;
	float yMap = (1.0 - pt.Z() * invSizeLand) * _invScaleY;
	_mapX = offset.x - xMap;
	_mapY = offset.y - yMap;
	SaturateX(_mapX);
	SaturateY(_mapY);
}

///////////////////////////////////////////////////////////////////////////////
// Main (ingame) map

CStatic *CreateStaticMapMain(ControlsContainer *parent, int idc, const ParamEntry &cls)
{
	CStaticMap *map = new CStaticMapMain(parent, idc, cls);
	map->SetVisibleRect(map->X(), map->Y(), map->W(), map->H());
	map->Center();
	return map;
}

#define EXPOSURE_COEF			50.0F

AIUnit *GetSelectedUnit(int i);
void SetSelectedUnit(int i, AIUnit *unit);
void ClearSelectedUnits();
bool IsEmptySelectedUnits();
PackedBoolArray ListSelectedUnits();

RString GetUserParams();

MainMapInfo GMainMapInfo;

CStaticMapMain::CStaticMapMain(ControlsContainer *parent, int idc, const ParamEntry &cls)
#if _ENABLE_CHEATS
 : CStaticMap(parent, idc, cls, 0.001, 0.5, 0.16)
#else
 : CStaticMap(parent, idc, cls, 0.05, 0.5, 0.16)
#endif
{
#if _ENABLE_CHEATS
	_showUnits = false;
	_showTargets = false;
	_showSensors = false;
	_showVariables = false;
#endif
	_activeMarker = -1;

	_infoCommand.Load(cls >> "Command");
	_colorActiveMarker = GetPackedColor(cls >> "ActiveMarker" >> "color");
	_sizeActiveMarker = cls >> "ActiveMarker" >> "size";

	_iconPosition = GEngine->TextBank()->Load(GetPictureName
	(
		Pars >> "CfgInGameUI" >> "Cursor" >> "mission"
	));

	GMainMapInfo.Clear();
}

void SetCommandState(int id, CommandState state, AISubgroup *subgrp)
{
	AIGroup *grp = subgrp->GetGroup();
	for (int i=0; i<GMainMapInfo.commands.Size();)
	{
		CommandInfo &cmd = GMainMapInfo.commands[i];
		if (cmd.grp == grp && cmd.id == id)
		{
			cmd.state = state;
			cmd.subgrp = subgrp;
			i++;
		}
		else if (cmd.subgrp == subgrp)
		{
			GMainMapInfo.commands.Delete(i);
		}
		else
		{
			i++;
		}
	}
}

void AddUnitInfo(AIUnit *unit)
{
	if (!unit) return;
	DisplayMap *display = dynamic_cast<DisplayMainMap *>(GWorld->Map());
	if (!display) return;
	CStaticMapMain *map = display->GetMap();
	if (!map) return;
	map->AddUnitInfo(unit);
}

void AddEnemyInfo
(
	TargetType *id, const VehicleType *type, int x, int z
)
{
	if (!id) return;
	DisplayMap *display = dynamic_cast<DisplayMainMap *>(GWorld->Map());
	if (!display) return;
	CStaticMapMain *map = display->GetMap();
	if (!map) return;
	map->AddEnemyInfo(id, type, x, z);
}

void CStaticMapMain::AddUnitInfo(AIUnit *unit)
{
	Assert(unit);
	int index = -1;
	for (int i=0; i<GMainMapInfo.unitInfo.Size(); i++)
		if (GMainMapInfo.unitInfo[i].unit == unit)
		{
			index = i; break;
		}
	if (index < 0) index = GMainMapInfo.unitInfo.Add();
	MapUnitInfo &info = GMainMapInfo.unitInfo[index];
	info.unit = unit;
	info.time = Glob.clock.GetTimeOfDay();
	if (info.time >= 1.0) info.time--;
	info.time *= 24;
	float coef = 100.0 / (LandGrid * LandRange);
	Vector3Val pos = unit->Position();
	info.x = toIntFloor(coef * pos.X());
	info.z = toIntFloor(coef * pos.Z());
}

void CStaticMapMain::AddEnemyInfo
(
	TargetType *id, const VehicleType *type, int x, int z
)
{
	Assert(id);
	int index = -1;
	for (int i=0; i<GMainMapInfo.enemyInfo.Size(); i++)
		if (GMainMapInfo.enemyInfo[i].id == id)
		{
			index = i; break;
		}
	if (index < 0) index = GMainMapInfo.enemyInfo.Add();
	MapEnemyInfo &info = GMainMapInfo.enemyInfo[index];
	info.id = id;
	info.type = type;
	info.time = Glob.clock.GetTimeOfDay();
	if (info.time >= 1.0) info.time--;
	info.time *= 24;
	info.x = x;
	info.z = z;
}

static const EnumName CommandStateNames[]=
{
	EnumName(CSSent, "SENT"),
	EnumName(CSReceived, "RECEIVED"),
	EnumName(CSSucceed, "SUCCEED"),
	EnumName(CSFailed, "FAILED"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(CommandState dummy)
{
	return CommandStateNames;
}

LSError CommandInfo::Serialize(ParamArchive &ar)
{
	CHECK(ar.SerializeRef("Group", grp, 1))
	CHECK(ar.Serialize("id", id, 1))
	CHECK(ar.SerializeEnum("state", state, 1))
	CHECK(ar.SerializeRef("Subgroup", subgrp, 1))
	CHECK(ar.Serialize("position", position, 1))
	return LSOK;
}

LSError MapUnitInfo::Serialize(ParamArchive &ar)
{
	CHECK(ar.SerializeRef("Unit", unit, 1))
	CHECK(ar.Serialize("time", time, 1))
	CHECK(ar.Serialize("x", x, 1))
	CHECK(ar.Serialize("z", z, 1))
	return LSOK;
}

LSError MapEnemyInfo::Serialize(ParamArchive &ar)
{
	CHECK(ar.SerializeRef("Target", id, 1))
	CHECK(::Serialize(ar, "type", type, 1))
	CHECK(ar.Serialize("time", time, 1))
	CHECK(ar.Serialize("x", x, 1))
	CHECK(ar.Serialize("z", z, 1))
	return LSOK;
}

LSError MainMapInfo::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("Commands", commands, 1))
	CHECK(ar.Serialize("UnitInfo", unitInfo, 1))
	CHECK(ar.Serialize("EnemyInfo", enemyInfo, 1))
	return LSOK;
}

void MainMapInfo::Clear()
{
	unitInfo.Clear();
	enemyInfo.Clear();
	commands.Clear();
}

LSError SerializeMapInfo(ParamArchive &ar, RString name, int minVersion)
{
	CHECK(ar.Serialize(name, GMainMapInfo, minVersion))
	return LSOK;
}

LSError CStaticMapMain::Serialize(ParamArchive &ar)
{
	return LSOK;
}

/*!
\patch 1.01 Date 06/07/2001 by Ondra
- Fixed: clipping command info in map (see FIX clipping)
- was return before
- first clipped command caused all others to be clipped)
- repaired with continue
*/

void CStaticMapMain::DrawCommands()
{
	for (int i=0; i<GMainMapInfo.commands.Size();)
	{
		CommandInfo &cmd = GMainMapInfo.commands[i];
		if
		(
			cmd.state == CSFailed ||										// command failed
			(cmd.state != CSSent && cmd.subgrp == NULL)	// subgroup destroyed
		)
		{
			GMainMapInfo.commands.Delete(i);
		}
		else
		{
			DrawCoord pt = WorldToScreen(cmd.position);
			DrawSign
			(
				_infoCommand.icon, _infoCommand.color, pt,
				_infoCommand.size, _infoCommand.size, 0
			);
			if (cmd.subgrp)
			{
				pt.x += 0.015;

				char buffer[256];
				PackedBoolArray list = cmd.subgrp->GetUnitsList();
				CreateUnitsList(list, buffer);
				float h = _size;
				float w = GEngine->GetTextWidth(_size, _font, buffer);

				// FIX clipping
				if
				(
					pt.x + w < _x ||
					pt.y + 0.5 * h < _y ||
					pt.x > _x + _w ||
					pt.y - 0.5 * h > _y + _h
				)
				{
					i++;
					continue;
				}
				GEngine->DrawText
				(
					Point2DFloat(pt.x, pt.y - 0.5 * h),
					_size,
					Rect2DFloat(_x, _y, _w, _h),
					_font,
					_ftColor,
					buffer
				);
			}
			i++;
		}
	}
}

void CStaticMapMain::DrawMarkers()
{
	int n = markersMap.Size();
	for (int i=0; i<n; i++)
	{
		ArcadeMarkerInfo &mInfo = markersMap[i];
		switch (mInfo.markerType)
		{
		case MTIcon:
			{
				if (mInfo.size == 0) break;
				if (!mInfo.icon) break;
				DrawSign
				(
					mInfo.icon, mInfo.color,
					mInfo.position,
					mInfo.size * mInfo.a, mInfo.size * mInfo.b,
					mInfo.angle * (H_PI / 180.0), Localize(mInfo.text)
				);
			}
			break;
		case MTRectangle:
			FillRectangle
			(
				mInfo.position, mInfo.a, mInfo.b, mInfo.angle,
				mInfo.color, mInfo.fill
			);
			break;
		case MTEllipse:
			FillEllipse
			(
				mInfo.position, mInfo.a, mInfo.b, mInfo.angle,
				mInfo.color, mInfo.fill
			);
			break;
		}
	}
}

void CStaticMapMain::DrawSensors()
{
	int n = sensorsMap.Size();
	for (int i=0; i<n; i++)
	{
		Vehicle *veh = sensorsMap[i];
		Detector *det = dyn_cast<Detector>(veh);
		if (!det) continue;
		PackedColor color = _colorSensor;
		if (!det->IsActive())
			color.SetA8(color.A8() / 2);
		
		float angle = (180 / H_PI) * atan2(det->_sinAngle, det->_cosAngle);
		if (det->_rectangular)
			DrawRectangle(det->Position(), det->_a, det->_b, angle, color);
		else
			DrawEllipse(det->Position(), det->_a, det->_b, angle, color);
		DrawSign
		(
			_iconSensor, color,
			det->Position(),
			16, 16, 0
		);
	}
}

DrawCoord CStaticMapMain::GetWaypointPosition(const ArcadeWaypointInfo &wInfo)
{
#if 0
	int type = wInfo.type;
	Point3 pos = wInfo.position;
	if
	(
		type == ACDESTROY ||
		type == ACGETIN ||
		type == ACJOIN ||
		type == ACLEADER
	)
	{
		if (wInfo.id >= 0 && wInfo.id < vehiclesMap.Size())
		{
			Vehicle *veh = vehiclesMap[wInfo.id];
			if (veh) pos = veh->Position();
		}
	}
#else
	// do not update waypoints position
	// waypoints are now only signs in map
	Point3 pos = wInfo.position;
#endif

	return WorldToScreen(pos);
}

void CStaticMapMain::DrawWaypoints(AICenter *center, AIGroup *myGroup)
{
#if _ENABLE_CHEATS
	if (_showUnits)
	{
		EntityAI *veh = dyn_cast<EntityAI>(GWorld->CameraOn());
		if (!veh) return;
		myGroup = veh->GetGroup();
		if (!myGroup) return;
		center = myGroup->GetCenter();
	}
#endif

	int index = INT_MAX;
	if (myGroup->GetCurrent())
		index = myGroup->GetCurrent()->_fsm->Var(0);

	int m = myGroup->NWaypoints();

#if _ENABLE_CHEATS
	if (!_showUnits)
#endif
	{
		int nshow = 0;
		for (int j=1; j<m; j++)
		{
			const ArcadeWaypointInfo &wInfo = myGroup->GetWaypoint(j);
			bool wpShow = wInfo.showWP == ShowAlways ||
				wInfo.showWP == ShowEasy && Glob.config.easyMode;
			if (wpShow) nshow++;
		}
		if (nshow == 0) return;
	}

	// lines
	if (m > 1)
	{
		const ArcadeWaypointInfo &wInfo = myGroup->GetWaypoint(0);
		DrawCoord pt1 = GetWaypointPosition(wInfo);
		for (int j=1; j<m; j++)
		{
			const ArcadeWaypointInfo &wInfo = myGroup->GetWaypoint(j);
			bool wpShow = wInfo.showWP == ShowAlways ||
				wInfo.showWP == ShowEasy && Glob.config.easyMode;
			if
			(
#if _ENABLE_CHEATS
				!_showUnits && 
#endif
				!wpShow
			) continue;
			// draw line
			DrawCoord pt2 = GetWaypointPosition(wInfo);
			GLOB_ENGINE->DrawLine
			(
				Line2DPixel(pt1.x * _wScreen, pt1.y * _hScreen, pt2.x * _wScreen, pt2.y * _hScreen),
				_colorActiveMission, _colorActiveMission, _clipRect
			);
			pt1 = pt2;
		}
	}

	// waypoints
	for (int j=0; j<m; j++)
	{
		const ArcadeWaypointInfo &wInfo = myGroup->GetWaypoint(j);
		bool wpShow = wInfo.showWP == ShowAlways ||
			wInfo.showWP == ShowEasy && Glob.config.easyMode;
		if
		(
			j > 0 &&
#if _ENABLE_CHEATS
			!_showUnits &&
#endif
			!wpShow
		) continue;
		MapTypeInfo &info = j < index ? _infoWaypointCompleted : _infoWaypoint;
		DrawCoord pt = GetWaypointPosition(wInfo);
		DrawSign(info.icon, info.color, pt, info.size, info.size, 0);
		if (wInfo.HasEffect())
		{
			pt.x += 0.015;
			DrawSign(_iconCamera, _colorCamera, pt, 12, 12, 0);
		}
	}
}

void CStaticMapMain::DrawLabel(struct SignInfo &info, PackedColor color)
{
	AICenter *myCenter = GetMyCenter();

	RString text;
	Point3 pos;
	switch (info._type)
	{
	case signNone:
		break;
	case signUnit:
		if (info._unit)
		{
			text = info._unit->GetGroup()->GetName();
			pos = info._unit->Position();
		}
		break;
	case signVehicle:
		if (myCenter && info._id!=NULL)
		{
			const AITargetInfo *enemy = myCenter->FindTargetInfo(info._id);
			if (enemy)
			{
				text = enemy->_type->GetDisplayName();
				pos = enemy->_realPos;
			}
		}
		break;
	case signStatic:
		if (myCenter && info._id!=NULL)
		{
			const AITargetInfo *building = myCenter->FindTargetInfo(info._id);
			if (building)
			{
				text = building->_type->GetDisplayName();
				pos = building->_realPos;
			}
		}
		break;
	case signArcadeWaypoint:
		if (myCenter)
		{
			if (info._indexGroup < 0 || info._indexGroup >= myCenter->NGroups()) break;
			AIGroup *group = myCenter->GetGroup(info._indexGroup);
			if (group)
			{
				Assert(info._index < group->NWaypoints());
				const ArcadeWaypointInfo &wInfo = group->GetWaypoint(info._index);
				text = LocalizeString(IDS_AC_MOVE + wInfo.type - ACMOVE);
				pos = wInfo.position;
			}
		}
		break;
	case signArcadeSensor:
		{
			Vehicle *veh = sensorsMap[info._index];
			Detector *det = dyn_cast<Detector>(veh);
			if (!det) break;
			pos = det->Position();
			if (det->GetText().GetLength() > 0)
			{
				text = det->GetText();
			}
			else
			{
				text = LocalizeString(IDS_SENSOR);
			}
			ArcadeSensorType type = (ArcadeSensorType)det->GetAction();
			if (type != ASTNone)
			{
				text = text + RString(" (") + LocalizeString(IDS_SENSORTYPE_NONE + type) + RString(")");
			}
		}
		break;
	}

	if (text.GetLength()>0)
	{
		DrawCoord posMap = WorldToScreen(pos);

		// place label
		float w = GLOB_ENGINE->GetTextWidth(_sizeLabel, _fontLabel, text);
		float h = _sizeLabel;
		posMap.x -= 0.5 * w;
		posMap.y -= 0.01 + h;

		MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
		GLOB_ENGINE->Draw2D
		(
			mip, _colorLabelBackground,
			Rect2DPixel(posMap.x * _wScreen, posMap.y * _hScreen, w * _wScreen, h * _hScreen),
			_clipRect
		);

		GLOB_ENGINE->DrawText
		(
			posMap, _sizeLabel,
			Rect2DFloat(_x, _y, _w, _h),
			_fontLabel, color, text
		);
	}
}

void CStaticMapMain::DrawExposureEnemy
(
	AICenter *center,
	float x, float y, float xStep, float yStep,
	int i, int j, int iStep, int jStep
)
{
	Draw2DPars pars;
	int maxAlpha8=0;
	const float coef = 0.5 / (256 * EXPOSURE_COEF);

	float alpha = coef * center->GetExposureOptimistic(i, j);
	int alpha8TL = toIntFloor( alpha * 255 );
	alpha = coef * center->GetExposureOptimistic(i + iStep, j);
	int alpha8TR = toIntFloor( alpha * 255 );
	alpha = coef * center->GetExposureOptimistic(i, j + jStep);
	int alpha8BL = toIntFloor( alpha * 255 );
	alpha = coef * center->GetExposureOptimistic(i + iStep, j + jStep);
	int alpha8BR = toIntFloor( alpha * 255 );
 
	if( maxAlpha8<=alpha8TL ) {if( alpha8TL>255 ) alpha8TL=255;maxAlpha8=alpha8TL;}
	else if (alpha8TL < 0) alpha8TL = 0;
	if( maxAlpha8<=alpha8TR ) {if( alpha8TR>255 ) alpha8TR=255;maxAlpha8=alpha8TR;}
	else if (alpha8TR < 0) alpha8TR = 0;
	if( maxAlpha8<=alpha8BL ) {if( alpha8BL>255 ) alpha8BL=255;maxAlpha8=alpha8BL;}
	else if (alpha8BL < 0) alpha8BL = 0;
	if( maxAlpha8<=alpha8BR ) {if( alpha8BR>255 ) alpha8BR=255;maxAlpha8=alpha8BR;}
	else if (alpha8BR < 0) alpha8BR = 0;
 
	if( maxAlpha8>0 )
	{
		pars.SetColor(_colorExposureEnemy);
		// TL
		pars.colorTL.SetA8(alpha8TL);
		// TR
		pars.colorTR.SetA8(alpha8TR);
		// BL
		pars.colorBL.SetA8(alpha8BL);
		// BR
		pars.colorBR.SetA8(alpha8BR);
 
		pars.SetU(0, 0);
		pars.SetV(0, 0);
		pars.vTL += 1;
		pars.uTR += 1;
		pars.vTR += 1;
		pars.uBR += 1;
  
		Rect2DPixel rect(x * _wScreen, y * _hScreen, xStep * _wScreen, yStep * _hScreen);
 
		pars.mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
		pars.spec = NoZBuf|IsAlpha|IsAlphaFog|NoClamp;
		GLOB_ENGINE->Draw2D(pars, rect, _clipRect);

		// draw exposure text

		// draw text information about the field
		if (xStep>0.05f)
		{
			float exposure = center->GetExposureOptimistic(i, j-1);
			if (exposure>0)
			{
				float ty = y;
				float tsize = xStep * 5 * _fontNames->Height();
				//float tsize = 0.2;
				ty += tsize;
				ty += tsize;
				ty += tsize;
				GEngine->DrawTextF
				(
					Point2DFloat(x, ty), tsize,
					Rect2DFloat(x, y, xStep, yStep),
					_fontNames, _colorNames,
					"enemy %.0f", exposure
				);
				ty += tsize;
			}
		}
	}
}

void CStaticMapMain::DrawExposureUnknown
(
	AICenter *center,
	float x, float y, float xStep, float yStep,
	int i, int j, int iStep, int jStep
)
{
	Draw2DPars pars;
	int maxAlpha8=0;
	const float coef = 0.5 / (256 * EXPOSURE_COEF);

	float alpha = coef * center->GetExposureUnknown(i, j);
	int alpha8TL = toIntFloor( alpha * 255 );
	alpha = coef * center->GetExposureUnknown(i + iStep, j);
	int alpha8TR = toIntFloor( alpha * 255 );
	alpha = coef * center->GetExposureUnknown(i, j + jStep);
	int alpha8BL = toIntFloor( alpha * 255 );
	alpha = coef * center->GetExposureUnknown(i + iStep, j + jStep);
	int alpha8BR = toIntFloor( alpha * 255 );
 
	if( maxAlpha8<=alpha8TL ) {if( alpha8TL>255 ) alpha8TL=255;maxAlpha8=alpha8TL;}
	else if (alpha8TL < 0) alpha8TL = 0;
	if( maxAlpha8<=alpha8TR ) {if( alpha8TR>255 ) alpha8TR=255;maxAlpha8=alpha8TR;}
	else if (alpha8TR < 0) alpha8TR = 0;
	if( maxAlpha8<=alpha8BL ) {if( alpha8BL>255 ) alpha8BL=255;maxAlpha8=alpha8BL;}
	else if (alpha8BL < 0) alpha8BL = 0;
	if( maxAlpha8<=alpha8BR ) {if( alpha8BR>255 ) alpha8BR=255;maxAlpha8=alpha8BR;}
	else if (alpha8BR < 0) alpha8BR = 0;
 
	if( maxAlpha8>0 )
	{
		pars.SetColor(_colorExposureUnknown);
		// TL
		pars.colorTL.SetA8(alpha8TL);
		// TR
		pars.colorTR.SetA8(alpha8TR);
		// BL
		pars.colorBL.SetA8(alpha8BL);
		// BR
		pars.colorBR.SetA8(alpha8BR);
 
		pars.vTL += 1;
		pars.uTR += 1;
		pars.vTR += 1;
		pars.uBR += 1;
  
		Rect2DPixel rect(x, y, xStep, yStep);
 
		pars.mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
		pars.spec = NoZBuf|IsAlpha|IsAlphaFog|NoClamp;
		GLOB_ENGINE->Draw2D(pars, rect, _clipRect);
	}
}

inline PackedColor CostToColor(float cost)
{
	static const float int1 = 1.0;
	static const float invint1 = (1.0 / int1);
	static const float int2 = 4.0;
	static const float invint2 = (1.0 / int2);
	static const float int12 = int1 + int2;

	static const float alpha = 0.5;
	static const Color colorR(1, 0, 0, alpha);
	static const Color colorG(0, 1, 0, alpha);
	static const Color colorY(1, 1, 0, alpha);

	saturateMin(cost, int12);
	if (cost <= int1)
	{
		return PackedColor
		(
			colorY * (cost * invint1)  + colorG * (1 - cost * invint1)
		);
	}
	else
	{
		cost -= int1;
		return PackedColor
		(
			colorR * (cost * invint2) + colorY * (1 - cost * invint2)
		);
	}
}

void CStaticMapMain::DrawCost
(
	AISubgroup *subgroup, float invCost,
	float x, float y, float xStep, float yStep,
	int i, int j, int iStep, int jStep
)
{
	GeographyInfo geogr = GLOB_LAND->GetGeography(i, j - 1);

	Draw2DPars pars;
	pars.SetU(0, 0);
	pars.SetV(0, 0);
	pars.vTL += 1;
	pars.uTR += 1;
	pars.vTR += 1;
	pars.uBR += 1;

	float cost;
	if (iStep == 1 && jStep == -1)
	{
		// ALL
		cost = subgroup->GetFieldCost(i, j + jStep) * invCost;
		pars.SetColor(CostToColor(cost));
	}
	else
	{
		// TL
		cost = subgroup->GetFieldCost(i, j) * invCost;
		pars.colorTL = CostToColor(cost);
		// TR
		cost = subgroup->GetFieldCost(i + iStep, j) * invCost;
		pars.colorTR = CostToColor(cost);
		// BL
		cost = subgroup->GetFieldCost(i, j + jStep) * invCost;
		pars.colorBL = CostToColor(cost);
		// BR
		cost = subgroup->GetFieldCost(i + iStep, j + jStep) * invCost;
		pars.colorBR = CostToColor(cost);
	}

	pars.mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
	pars.spec = NoZBuf|IsAlpha|IsAlphaFog|NoClamp;
	Rect2DPixel rect(x * _wScreen, y * _hScreen, xStep * _wScreen, yStep * _hScreen);
	GEngine->Draw2D(pars, rect, _clipRect);

	// draw text information about the field
	if (xStep>0.05f)
	{
		float ty = y;
		float tsize = xStep * 5 * _fontNames->Height();
		//float tsize = 0.2;
		GEngine->DrawTextF
		(
			Point2DFloat(x, ty), tsize,
			Rect2DFloat(x, y, xStep, yStep),
			_fontNames, _colorNames,
			"softObj %d", geogr.u.howManyObjects
		);
		ty += tsize;

		if (geogr.u.howManyHardObjects>0)
		{
			GEngine->DrawTextF
			(
				Point2DFloat(x, ty), tsize,
				Rect2DFloat(x, y, xStep, yStep),
				_fontNames, _colorNames,
				"hardObj %d", geogr.u.howManyHardObjects
			);
			ty += tsize;
		}

		GEngine->DrawTextF
		(
			Point2DFloat(x, ty), tsize,
			Rect2DFloat(x, y, xStep, yStep),
			_fontNames, _colorNames,
			"grad %d", geogr.u.gradient
		);
		ty += tsize;

		if (geogr.u.forestOuter)
		{
			GEngine->DrawText
			(
				Point2DFloat(x, ty), tsize,
				Rect2DFloat(x, y, xStep, yStep),
				_fontNames, _colorNames,
				"Outer forest"
			);
			ty += tsize;
		}

		if (geogr.u.forestInner)
		{
			GEngine->DrawText
			(
				Point2DFloat(x, ty), tsize,
				Rect2DFloat(x, y, xStep, yStep),
				_fontNames, _colorNames,
				"Inner forest"
			);
			ty += tsize;
		}

		if (geogr.u.road)
		{
			GEngine->DrawText
			(
				Point2DFloat(x, ty), tsize,
				Rect2DFloat(x, y, xStep, yStep),
				_fontNames, _colorNames,
				"Road"
			);
			ty += tsize;
		}
		if (geogr.u.track)
		{
			GEngine->DrawText
			(
				Point2DFloat(x, ty), tsize,
				Rect2DFloat(x, y, xStep, yStep),
				_fontNames, _colorNames,
				"Track"
			);
			ty += tsize;
		}

		if (geogr.u.waterDepth>0)
		{
			GEngine->DrawTextF
			(
				Point2DFloat(x, ty), tsize,
				Rect2DFloat(x, y, xStep, yStep),
				_fontNames, _colorNames,
				"Water %d",geogr.u.waterDepth
			);
			ty += tsize;
		}
		if (geogr.u.full)
		{
			GEngine->DrawText
			(
				Point2DFloat(x, ty), tsize,
				Rect2DFloat(x, y, xStep, yStep),
				_fontNames, _colorNames,
				"Square full"
			);
			ty += tsize;
		}
	}

	if (iStep == 1 && jStep == -1)
	{
		RoadList &roadList = GRoadNet->GetRoadList(i, j - 1);
		int i, j, n = roadList.Size();
		if (n > 0)
		{
			bool found = false;
			for (i=0; i<n; i++)
			{
				RoadLink *item = roadList[i];
				for (j=0; j<item->NConnections(); j++)
				{
					if (!item->Connections()[j])
					{
						found = true;
						break;
					}
				}
				if (found) break;
			}
			PackedColor color = PackedColor(found ? Color(1,.2,.2,1) : Color(1,1,1,1));
			GLOB_ENGINE->DrawLine
			(
				Line2DPixel(x * _wScreen, y * _hScreen,
				(x + xStep) * _wScreen, (y + yStep) * _hScreen),
				color, color, _clipRect
			);
			GLOB_ENGINE->DrawLine
			(
				Line2DPixel(x * _wScreen, (y + yStep) * _hScreen,
				(x + xStep) * _wScreen, y * _hScreen),
				color, color, _clipRect
			);
		}
	}
}

void CStaticMapMain::DrawInfo()
{
	if (_scaleX > 0.2) return;

	float coef = 100.0;	// width of field in global coord.
	float invCoef = 0.01; // 1.0 / coef;

	int xFrom = toIntFloor(-_mapX * _scaleX * coef);
	int zFrom = toIntFloor((_invScaleY - (_h - _mapY)) * _scaleY * coef);
	int xTo = toIntFloor((_w - _mapX) * _scaleX * coef);
	int zTo = toIntFloor((_invScaleY - (-_mapY)) * _scaleY * coef);

	float xBeg = _x + _mapX + xFrom * _invScaleX * invCoef;
	float zBeg = _y + _mapY + (_invScaleY - zFrom * _invScaleY * invCoef);
	float xFld = _invScaleX * invCoef;
	float zFld = _invScaleY * invCoef;

	float zAct = zBeg;
	for (int z = zFrom; z <= zTo; z++)
	{
		float xAct = xBeg;
		for (int x = xFrom; x <= xTo; x++)
		{
			DrawInfo(xAct, zAct - zFld, xFld, zFld, x, z);
			xAct += xFld;
		}
		zAct -= zFld;
	}
}

void CStaticMapMain::DrawInfo
(
	float x, float y, float xStep, float yStep, int i, int j
)
{
	static const int maxSubgroups = 3;
	static const int maxEnemies = 3;

	float tsize = xStep * 6 * _fontNames->Height();
	float ty = y + 0.5 * (yStep - (maxSubgroups + 1) * tsize);

	int indices[maxEnemies];
	for (int l=0; l<maxEnemies; l++) indices[l] = -1;
	for (int k=0; k<GMainMapInfo.enemyInfo.Size(); k++)
	{
		MapEnemyInfo &info = GMainMapInfo.enemyInfo[k];
		if (info.x != i || info.z != j) continue;
		for (int l=0; l<maxEnemies; l++)
		{
			if (indices[l] < 0)
			{
				indices[l] = k;
				break;
			}
			else
			{
				float kCost = info.type->GetCost();
				float lCost = GMainMapInfo.enemyInfo[indices[l]].type->GetCost();
				if (kCost >= lCost)
				{
					for (int i=maxEnemies-1; i>l; i--) indices[i] = indices[i - 1];
					indices[l] = k;
					break;
				}
			}
		}
	}
	if (indices[0] >= 0)
	{
		float left = x;
		float maxTime = 0;
		for (int l=0; l<maxEnemies; l++)
		{
			int k = indices[l];
			if (k < 0) break;
			MapEnemyInfo &info = GMainMapInfo.enemyInfo[k];
			saturateMax(maxTime, info.time);
		}
		int hour = toIntFloor(maxTime);
		int minute = toIntFloor(60.0 * (maxTime - hour));
		GEngine->DrawTextF
		(
			Point2DFloat(x, ty), tsize,
			Rect2DFloat(x, y, xStep, yStep),
			_fontNames, _colorEnemy,
			"% 2d:%02d", hour, minute
		);
		left += GEngine->GetTextWidthF(tsize, _fontNames, "% 2d:%02d", hour, minute);
		float height = tsize;
		float width = height * (_hScreen / _wScreen);
		for (int l=0; l<maxEnemies; l++)
		{
			int k = indices[l];
			if (k < 0) break;
			MapEnemyInfo &info = GMainMapInfo.enemyInfo[k];
			Texture *texture = info.type->GetIcon();
			MipInfo mip = GEngine->TextBank()->UseMipmap(texture, 0, 0);
			GEngine->Draw2D
			(
				mip, _colorEnemy,
				Rect2DPixel(left * _wScreen, ty * _hScreen, width * _wScreen, height * _hScreen),
				Rect2DPixel(x * _wScreen, y * _hScreen, xStep * _wScreen, yStep * _hScreen)
			);
			left += width;
		}
		ty += height;
	}

	// newest infos first
	int n = 0;
	for (int k=GMainMapInfo.unitInfo.Size()-1; k>=0; k--)
	{
		MapUnitInfo &info = GMainMapInfo.unitInfo[k];
		AIUnit *unit = info.unit;
		if (!unit)
		{
			GMainMapInfo.unitInfo.Delete(k);
			continue;
		}
		if (info.x != i || info.z != j) continue;
		int hour = toIntFloor(info.time);
		int minute = toIntFloor(60.0 * (info.time - hour));
		GEngine->DrawTextF
		(
			Point2DFloat(x, ty), tsize,
			Rect2DFloat(x, y, xStep, yStep),
			_fontNames, _colorFriendly,
			"% 2d:%02d %d", hour, minute, (const char *)unit->ID()
		);
		ty += tsize;
		n++;
		if (n >= maxSubgroups) break;
	}
}

AIUnit *CStaticMapMain::GetMyUnit()
{
	return GLOB_WORLD->FocusOn();
}

AICenter *CStaticMapMain::GetMyCenter()
{
	AIUnit *me = GetMyUnit();
	AIGroup *myGroup = me ? me->GetGroup() : NULL;
	AICenter *myCenter = myGroup ? myGroup->GetCenter() : NULL;

	if (myCenter) return myCenter;

	switch (Glob.header.playerSide)
	{
	case TEast:
		return GLOB_WORLD->GetEastCenter();
	case TWest:
		return GLOB_WORLD->GetWestCenter();
	case TGuerrila:
		return GLOB_WORLD->GetGuerrilaCenter();
	default:
		Fail("Side");
		return NULL;
	}
}

void CStaticMapMain::DrawExposure()
{
#if _ENABLE_CHEATS
	if (!_show) return;
#endif

	AICenter *myCenter = GetMyCenter();
	if (!myCenter) return;

	float invLandRange = 1.0 / LandRange;
	float ptsLand = 800.0 * _invScaleX * invLandRange;	// avoid dependece on video resolution
	float invPtsLand = 1.0 / ptsLand;

	int iStep = toIntCeil(ptsPerSquareExp * invPtsLand);
	float xStep = iStep * invLandRange * _invScaleX;
	int jStep = iStep;
	float yStep = jStep * invLandRange * _invScaleY;
	int iMin = iStep * toIntFloor(-_mapX / xStep);
	float xMin = _x - fmod(-_mapX, xStep);
	int jMin = LandRange - jStep * toIntFloor(-_mapY / yStep);
	float yMin = _y - fmod(-_mapY, yStep);

	float y = yMin;
	int j = jMin;
	while (j >= 0 && y <= _y + _h)
	{
		float x = xMin;
		int i = iMin;
		while (i < LandRange && x <= _x + _w)
		{
			DrawExposureEnemy(myCenter, x, y, xStep, yStep, i, j, iStep, -jStep);
			DrawExposureUnknown(myCenter, x, y, xStep, yStep, i, j, iStep, -jStep);
			i += iStep;
			x += xStep;
		}
		j -= jStep;
		y += yStep;
	}
}

void CStaticMapMain::DrawCost()
{
	AIUnit *me = GetMyUnit();
	if (!me) return;
	AISubgroup *mySubgroup = me->GetSubgroup();
	if (!mySubgroup) return;

	GeographyInfo geogr;
	geogr.packed = 0;
	float costNormal = -FLT_MAX;
	for (int i=0; i<mySubgroup->NUnits(); i++)
	{
		AIUnit *unit = mySubgroup->GetUnit(i);
		if (!unit || !unit->IsUnit())
			continue;
		float cost = unit->GetVehicle()->GetCost(geogr)
			* unit->GetVehicle()->GetFieldCost(geogr);
		if (cost > costNormal) costNormal = cost;
	}
	float invCost = 1.0 / (costNormal * LandGrid);

	float invLandRange = 1.0 / LandRange;
	float ptsLand = 800.0 * _invScaleX * invLandRange;	// avoid dependece on video resolution
	float invPtsLand = 1.0 / ptsLand;

	int iStep = toIntCeil(ptsPerSquareCost * invPtsLand);
	float xStep = iStep * invLandRange * _invScaleX;
	int jStep = iStep;
	float yStep = jStep * invLandRange * _invScaleY;
	int iMin = iStep * toIntFloor(-_mapX / xStep);
	float xMin = _x - fmod(-_mapX, xStep);
	int jMin = LandRange - jStep * toIntFloor(-_mapY / yStep);
	float yMin = _y - fmod(-_mapY, yStep);

	float y = yMin;
	int j = jMin;
	while (j >= 0 && y <= _y + _h)
	{
		float x = xMin;
		int i = iMin;
		while (i < LandRange && x <= _x + _w)
		{
			// get geography info
			DrawCost(mySubgroup, invCost, x, y, xStep, yStep, i, j, iStep, -jStep);
			i += iStep;
			x += xStep;
		}
		j -= jStep;
		y += yStep;
	}
}

void CStaticMapMain::DrawUnits(AICenter *center)
{
	if (!center) return;
	AICenter *myCenter = GetMyCenter();
	if (!myCenter) return;

	PackedColor colorAlive;
	PackedColor colorDead;
	TargetSide side = center->GetSide();
	if (myCenter->IsFriendly(side))
	{
		colorAlive = _colorFriendly;
		// TODO: move colordead to config
		colorDead = PackedColor(Color(0,1,1));
	}
	else if (myCenter->IsEnemy(side))
	{
		colorAlive = _colorEnemy;
		colorDead = PackedColor(Color(1,1,0));
	}
	else if (myCenter->IsNeutral(side))
	{
		colorAlive = _colorNeutral;
		colorDead = PackedColor(Color(1,0,1));
	}
	else
	{
		colorAlive = _colorUnknown;
		colorDead = PackedColor(Color(1,0,1));
	}

	for (int i=0; i<center->NGroups(); i++)
	{
		AIGroup *grp = center->GetGroup(i);
		if (!grp) continue;
		for (int j=0; j<MAX_UNITS_PER_GROUP; j++)
		{
			AIUnit *unit = grp->UnitWithID(j + 1);
			if (!unit) continue;
			int size = 10;
			if (unit->IsGroupLeader()) size = 16;
			PackedColor color = colorAlive;
			if (unit->GetLifeState()!=AIUnit::LSAlive) color = colorDead;
			Vector3 dir = unit->GetVehicle()->Direction();
			float azimut = atan2(dir.X(), dir.Z());
			DrawSign
			(
				unit->GetVehicle()->GetIcon(), color,
				unit->Position(),
				size, size, azimut
			);
		}
	}
}

void CStaticMapMain::DrawExt(float alpha)
{
	CStaticMap::DrawExt(alpha);

#if _ENABLE_CHEATS
	if (!_show) return;

	if (_showUnits || _showTargets) DrawExposure();
	if (CHECK_DIAG(DECostMap)) DrawCost();
#endif

	AIUnit *me = GetMyUnit();
	AIGroup *myGroup = me ? me->GetGroup() : NULL;
	AICenter *myCenter = GetMyCenter();
	if (!myCenter) return;

	DrawCommands();
	DrawMarkers();
#if _ENABLE_CHEATS
	if (_showSensors) DrawSensors();
#endif
	if( myGroup ) DrawWaypoints(myCenter, myGroup);

#if _ENABLE_CHEATS
	if (_showUnits)
	{
		DrawUnits(GWorld->GetWestCenter());
		DrawUnits(GWorld->GetEastCenter());
		DrawUnits(GWorld->GetGuerrilaCenter());
		DrawUnits(GWorld->GetCivilianCenter());
		DrawUnits(GWorld->GetLogicCenter());
	}
	else if (_showTargets)
	{
		// draw targets
		int n = myCenter->NTargets();
		for (int i=0; i<n; i++)
		{
			const AITargetInfo &info = myCenter->GetTarget(i);
			if (info._vanished) continue;
			Object *objTgt = info._idExact;
			EntityAI *veh = dyn_cast<EntityAI>(objTgt);
			if (!veh) continue;
			//const VehicleType *type = veh->GetType(info.FadingTypeAccuracy());
			//TargetSide side = veh->GetTargetSide(info.FadingSideAccuracy());
			const VehicleType *type = info._type;
			TargetSide side = info._destroyed ? TCivilian : info._side;

			//if (info._disappeared) side = TCivilian;

			int size = 16;
			int alpha8 = 255;
			if (!type->IsKindOf(GLOB_WORLD->Preloaded(VTypeStrategic)) )
			{
				if (!type->IsKindOf(GLOB_WORLD->Preloaded(VTypeAllVehicles))) continue;
				float alpha = info.FadingPositionAccuracy();
				if (alpha < 0.1) continue;
				alpha8 = toIntFloor(alpha * 255);
				if (alpha8 < 0) alpha8 = 0;
				if (alpha8 > 255) alpha8 = 255;
			}

			PackedColor color;
			if (side == TCivilian)
				color = _colorCivilian;
			else if (myCenter->IsFriendly(side))
				color = _colorFriendly;
			else if (myCenter->IsEnemy(side))
				color = _colorEnemy;
			else if (myCenter->IsNeutral(side))
				color = _colorNeutral;
			else
				color = _colorUnknown;
			color.SetA8(alpha8);

			float azimut = atan2(info._dir.X(), info._dir.Z());
			DrawSign(type->GetIcon(), color, info._realPos, size, size, azimut);
		}
	}
	else 
#endif
	if (Glob.config.IsEnabled(DTMap))
	{
		if (myGroup)
		{
			int n = myGroup->GetTargetList().Size();
			for (int i=0; i<n; i++)
			{
				const Target *target = myGroup->GetTargetList()[i];
				if (!target->IsKnownBy(me) || target->vanished || target->destroyed) continue;

				const VehicleType *type = target->type;
				if
				(
					!type->IsKindOf(GWorld->Preloaded(VTypeAllVehicles)) &&
					!type->IsKindOf(GWorld->Preloaded(VTypeStrategic))
				) continue;

				// visibility
				float visible = target->FadingSpotability();
				if (visible <= 0.01) continue;
				saturateMin(visible, 1);

				// color
				TargetSide side = target->side;
				PackedColor color;
				if (side == TCivilian)
					color = _colorCivilian;
				else if (myCenter->IsFriendly(side))
					color = _colorFriendly;
				else if (myCenter->IsEnemy(side))
					color = _colorEnemy;
				else if (myCenter->IsNeutral(side))
					color = _colorNeutral;
				else
					color = _colorUnknown;
				color.SetA8(toIntFloor(color.A8() *		visible));

				int size = 16;
				Vector3 pos = target->AimingPosition();
				Vector3 dir = VZero; // ???
				float azimut = atan2(dir.X(), dir.Z());
				DrawSign(target->type->GetIcon(), color, target->position, size, size, azimut);
			}
		}
	}
	else DrawInfo();

	// draw selected units
#if _ENABLE_CHEATS
	if (_showUnits && myGroup && me == myGroup->Leader() || Glob.config.IsEnabled(DTMap))
	{
		for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
		{
			AIUnit *unit = GetSelectedUnit(i);
			if (!unit) continue;
			Point3 pos = unit->Position();
			DrawCoord posMap = WorldToScreen(pos);
			bool draw = DrawSign
			(
				_iconSelect, _colorSelect,
				posMap,
				16, 16, 0
			);
			if (draw)
			{
				float x = posMap.x + 0.015;
				float y = posMap.y - 0.015;
				GLOB_ENGINE->DrawTextF
				(
					Point2DFloat(x, y), _sizeUnits, _fontUnits, _colorSelect,
					"%d", i + 1
				);
			}
		}
	}
#endif

	// draw myself
	if
	(
#if _ENABLE_CHEATS
		_showUnits ||
#endif
		Glob.config.IsEnabled(DTMap)
	)
	{
		Object *myVehicle = NULL;
		if (me)
		{
			myVehicle = me->GetVehicle();
		}
		else if (GLOB_WORLD->CameraOn())
		{
			myVehicle = GLOB_WORLD->CameraOn();
		}

		if (myVehicle)
		{
			Point3 pos = myVehicle->Position();
			bool draw = DrawSign
			(
				_iconPlayer, _colorMe,
				pos,
				16, 16, 0
			);
			if (draw)
			{
				// draw orientation
				Vector3 dir = myVehicle->Direction();
				dir[1] = 0;
				dir.Normalize();
				DrawCoord ptB = WorldToScreen(pos);
				DrawCoord ptE;
				ptE.x = ptB.x + 0.02 * dir.X();
				ptE.y = ptB.y - 0.02 * dir.Z();
				GLOB_ENGINE->DrawLine
				(
					Line2DPixel(ptB.x * _wScreen, ptB.y * _hScreen,
					ptE.x * _wScreen, ptE.y * _hScreen),
					_colorMe, _colorMe, _clipRect
				);
			}
		}

	}
	else if (me)
	{
		const VehicleType *type = me->GetVehicle()->GetType();
		if
		(
			type->IsKindOf(GWorld->Preloaded(VTypeAir)) ||
			type->IsKindOf(GWorld->Preloaded(VTypeTank))
		)
		{
			PackedColor color = PackedColor(Color(0, 0, 0, 1));
			const float h1 = 4.0 * _hScreen * (1.0 / 480);
			const float w1 = 4.0 * _wScreen * (1.0 / 640);
			const float h2 = 8.0 * _hScreen * (1.0 / 480);
			const float w2 = 8.0 * _wScreen * (1.0 / 640);
			DrawCoord pt = WorldToScreen(me->Position());
			pt.x *= _wScreen;
			pt.y *= _hScreen;
			GEngine->DrawLine
			(
				Line2DPixel(_clipRect.x, pt.y, pt.x - w1, pt.y),
				color, color, _clipRect
			);
			GEngine->DrawLine
			(
				Line2DPixel(pt.x + w1, pt.y, _clipRect.x + _clipRect.w, pt.y),
				color, color, _clipRect
			);
			GEngine->DrawLine
			(
				Line2DPixel(pt.x, _clipRect.y, pt.x, pt.y - h1),
				color, color, _clipRect
			);
			GEngine->DrawLine
			(
				Line2DPixel(pt.x, pt.y + h1, pt.x, _clipRect.y + _clipRect.h),
				color, color, _clipRect
			);
			MipInfo mip = GEngine->TextBank()->UseMipmap(_iconPosition, 0, 0);
			GEngine->Draw2D
			(
				mip, color,
				Rect2DPixel(toInt(pt.x - w2) + 0.5, toInt(pt.y - h2) + 0.5, 2 * w2, 2 * h2)
			);
		}
	}

//	if (_showTargets || _showUnits || Glob.config.easyMode)
	{
		DrawLabel(_infoMove, _colorInfoMove);
	}
#if _ENABLE_CHEATS
	if (_showUnits || _showTargets)
	{
		// draw labels
		// draw my path
		AIUnit *myPilot = me ? me->GetVehicle()->PilotUnit() : NULL;
		if (myPilot)
		{
			int n = myPilot->GetPlanner().GetPlanSize();
			int m = myPilot->GetPlanner().FindBestIndex(me->Position());
			PackedColor color = PackedColor(Color(0,1,1));
			for (int i=0; i<m; i++)
			{
				Point3 pos;
				myPilot->GetPlanner().GetPlanPosition(i, pos);
				DrawSign(NULL, color, pos, 2, 2, 0);
			}
			for (int i=m; i<n; i++)
			{
				Point3 pos;
				myPilot->GetPlanner().GetPlanPosition(i, pos);
				DrawSign(NULL, _colorPath, pos, 2, 2, 0);
			}
		}

		// TODO: another drawing
		int n = sensorsMap.Size();
		for (int i=0; i<n; i++)
		{
			Vehicle *veh = sensorsMap[i];
			if (!veh) continue;
			Detector *sensor = dyn_cast<Detector>(veh);
			Assert(sensor);
			if (!sensor) continue;
			if (!sensor->IsCountdown()) continue;
			GLOB_ENGINE->DrawTextF
			(
				Point2DFloat(_x, _y), 1.5 * _font->Height(), _font, _ftColor,
				"COUNTDOWN: %.0f",
				sensor->GetCountdown()
			);
		}

		// draw camera positions of multiplayer players
		const AutoArray<PlayerIdentity> *identities = GetNetworkManager().GetIdentities();
		if (identities) for (int i=0; i<identities->Size(); i++)
		{
			int dpnid = identities->Get(i).dpnid;
			Vector3 pos = GetNetworkManager().GetCameraPosition(dpnid);
			if (pos.SquareSize() > 0.01)
			{
				DrawSign(NULL, PackedColor(Color(1, 0.8, 0, 1)), pos, 2, 2, 0);
			}
		}

	}
#endif

	if (_activeMarker >= 0)
	{
		const float rectH2 = 0.5 * _sizeActiveMarker * _hScreen * (1.0 / 480);
		const float rectW2 = 0.5 * _sizeActiveMarker * _wScreen * (1.0 / 640);
		ArcadeMarkerInfo &mInfo = markersMap[_activeMarker];
		DrawCoord pt = WorldToScreen(mInfo.position);
		pt.x *= _wScreen;
		pt.y *= _hScreen;
		GEngine->DrawLine
		(
			Line2DPixel(pt.x - rectW2, pt.y - rectH2, pt.x + rectW2, pt.y - rectH2),
			_colorActiveMarker, _colorActiveMarker, _clipRect
		);
		GEngine->DrawLine
		(
			Line2DPixel(pt.x + rectW2, pt.y - rectH2, pt.x + rectW2, pt.y + rectH2),
			_colorActiveMarker, _colorActiveMarker, _clipRect
		);
		GEngine->DrawLine
		(
			Line2DPixel(pt.x + rectW2, pt.y + rectH2, pt.x - rectW2, pt.y + rectH2),
			_colorActiveMarker, _colorActiveMarker, _clipRect
		);
		GEngine->DrawLine
		(
			Line2DPixel(pt.x - rectW2, pt.y + rectH2, pt.x - rectW2, pt.y - rectH2),
			_colorActiveMarker, _colorActiveMarker, _clipRect
		);
		GEngine->DrawLine
		(
			Line2DPixel(_clipRect.x, pt.y, pt.x - rectW2, pt.y),
			_colorActiveMarker, _colorActiveMarker, _clipRect
		);
		GEngine->DrawLine
		(
			Line2DPixel(pt.x + rectW2, pt.y, _clipRect.x + _clipRect.w, pt.y),
			_colorActiveMarker, _colorActiveMarker, _clipRect
		);
		GEngine->DrawLine
		(
			Line2DPixel(pt.x, _clipRect.y, pt.x, pt.y - rectH2),
			_colorActiveMarker, _colorActiveMarker, _clipRect
		);
		GEngine->DrawLine
		(
			Line2DPixel(pt.x, pt.y + rectH2, pt.x, _clipRect.y + _clipRect.h),
			_colorActiveMarker, _colorActiveMarker, _clipRect
		);
	}

#if _ENABLE_CHEATS
	if (_showVariables)
	{
		float top = _y + 0.2;
		float left = _x + 0.02;
		const VarBankType &vars = GWorld->GetGameState()->GetVariables();
		if (vars.NItems() > 0)
		{
			// !!! avoid GetTable when NItems == 0
			for (int i=0; i<vars.NTables(); i++)
			{
				for (int j=0; j<vars.GetTable(i).Size(); j++)
				{
					GameVariable &var = vars.GetTable(i)[j];
					if (GWorld->GetGameState()->IsVisible(var)) continue;
					GLOB_ENGINE->DrawTextF
					(
						Point2DFloat(left, top), _sizeLabel, _fontLabel, PackedColor(Color(0, 0, 0, 1)),
						"%s = %s",
						(const char *)var.GetName(),
						(const char *)var.GetValueText()
					);
					top += _sizeLabel;
				}
			}
		}
	}
#endif

#if _ENABLE_VBS
	bool IsVBS();
	if (IsVBS()) DrawLegend();
#endif
}

// simulation
void CStaticMapMain::Center()
{
	Point3 pt = _defaultCenter;
	if (GLOB_WORLD->FocusOn())
		pt = GLOB_WORLD->FocusOn()->Position();
	else if (GLOB_WORLD->CameraOn())
		pt = GLOB_WORLD->CameraOn()->Position();
	CStaticMap::Center(pt);
}

void CStaticMapMain::FindUnit(AICenter *center, Vector3Par pt, SignInfo &info, float &minDist)
{
	if (!center) return;

	for (int i=0; i<center->NGroups(); i++)
	{
		AIGroup *grp = center->GetGroup(i);
		if (!grp) continue;
		for (int j=0; j<MAX_UNITS_PER_GROUP; j++)
		{
			AIUnit *unit = grp->UnitWithID(j + 1);
			if (!unit) continue;
			float dist = (pt - unit->Position()).SquareSizeXZ();
			if (dist < minDist)
			{
				minDist = dist;
				info._type = signUnit;
				info._unit = unit;
			}
		}
	}
}

void CStaticMapMain::FindWaypoint
(
	AICenter *myCenter, AIGroup *myGroup,
	Vector3Par pt, SignInfo &info, float &minDist
)
{
#if _ENABLE_CHEATS
	if (_showUnits)
	{
		EntityAI *veh = dyn_cast<EntityAI>(GWorld->CameraOn());
		if (!veh) return;
		myGroup = veh->GetGroup();
		if (!myGroup) return;
		myCenter = myGroup->GetCenter();
	}
#endif

	int i = myGroup->ID() - 1;
	int m = myGroup->NWaypoints();

#if _ENABLE_CHEATS
	if (!_showUnits)
#endif
	{
		int nshow = 0;
		for (int j=1; j<m; j++)
		{
			const ArcadeWaypointInfo &wInfo = myGroup->GetWaypoint(j);
			bool wpShow = wInfo.showWP == ShowAlways ||
				wInfo.showWP == ShowEasy && Glob.config.easyMode;
			if (wpShow) nshow++;
		}
		if (nshow == 0) return;
	}
	
	// waypoints
	for (int j=0; j<m; j++)
	{
		const ArcadeWaypointInfo &wInfo = myGroup->GetWaypoint(j);
		bool wpShow = wInfo.showWP == ShowAlways ||
			wInfo.showWP == ShowEasy && Glob.config.easyMode;
		if
		(
			j > 0 &&
#if _ENABLE_CHEATS
			!_showUnits &&
#endif
			!wpShow
		) continue;
		float dist = (pt - wInfo.position).SquareSizeXZ();
		if (dist < minDist)
		{
			minDist = dist;
			info._type = signArcadeWaypoint;
			info._indexGroup = i;
			info._index = j;
		}
	}
}

SignInfo CStaticMapMain::FindSign(float x, float y)
{
	AIUnit *me = GetMyUnit();
	AIGroup *myGroup = me ? me->GetGroup() : NULL;
	AICenter *myCenter = GetMyCenter();

	struct SignInfo info;
	info._type = signNone;
	info._unit = NULL;
	info._id = NULL;
	if (!myCenter) return info;

	Point3 pt = ScreenToWorld(DrawCoord(x, y));
	float sizeLand = LandGrid * LandRange;
	float dist, minDist = 0.02 * sizeLand * _scaleX;
	minDist *= minDist;

#if _ENABLE_CHEATS
	if (_showSensors)
	{
		for (int i=0; i<sensorsMap.Size(); i++)
		{
			Vehicle *veh = sensorsMap[i];
			Detector *det = dyn_cast<Detector>(veh);
			if (!det) continue;

			dist = (pt - det->Position()).SquareSizeXZ();
			if (dist < minDist)
			{
				minDist = dist;
				info._type = signArcadeSensor;
				info._index = i;
			}
		}
	}
	
	if (_showUnits)
	{
		FindUnit(GWorld->GetWestCenter(), pt, info, minDist);
		FindUnit(GWorld->GetEastCenter(), pt, info, minDist);
		FindUnit(GWorld->GetGuerrilaCenter(), pt, info, minDist);
		FindUnit(GWorld->GetCivilianCenter(), pt, info, minDist);
		FindUnit(GWorld->GetLogicCenter(), pt, info, minDist);
	}
	else if (_showTargets)
	{
		// draw targets
		int n = myCenter->NTargets();
		for (int i=0; i<n; i++)
		{
			const AITargetInfo &target = myCenter->GetTarget(i);
			if (target._vanished) continue;
			Object *objTgt = target._idExact;
			EntityAI *veh = dyn_cast<EntityAI>(objTgt);
			if (!veh) continue;
			const VehicleType *type = veh->GetType(target.FadingTypeAccuracy());
//			TargetSide side = veh->GetTargetSide(target.FadingSideAccuracy());

			if (!type->IsKindOf(GLOB_WORLD->Preloaded(VTypeStrategic)) )
			{
				if (!type->IsKindOf(GLOB_WORLD->Preloaded(VTypeAllVehicles))) continue;
				float alpha = target.FadingPositionAccuracy();
				if (alpha < 0.1) continue;
			}

			dist = (pt - target._realPos).SquareSizeXZ();
			if (dist < minDist)
			{
				if (target._type->IsKindOf(GLOB_WORLD->Preloaded(VTypeStatic)) )
				{
					minDist = dist;
					info._type = signStatic;
					info._id = target._idExact;
				}
				else
				{
					minDist = dist;
					info._type = signVehicle;
					info._id = target._idExact;
				}
			}
		}
	}
	else 
#endif
	if (Glob.config.IsEnabled(DTMap))
	{
		if (myGroup)
		{
			int n = myGroup->GetTargetList().Size();
			for (int i=0; i<n; i++)
			{
				const Target *target = myGroup->GetTargetList()[i];
				if (!target->IsKnownBy(me) || target->vanished || target->destroyed) continue;

				// visibility
				float visible = target->FadingSpotability();
				if (visible <= 0.01) continue;

				dist = (pt - target->AimingPosition()).SquareSizeXZ();
				if (dist < minDist)
				{
					if (target->type->IsKindOf(GLOB_WORLD->Preloaded(VTypeStatic)) )
					{
						minDist = dist;
						info._type = signStatic;
						info._id = target->idExact;
					}
					else
					{
						minDist = dist;
						info._type = signVehicle;
						info._id = target->idExact;
					}
				}
			}
		}
	}

/*
	// check my units
	for (int i=0; i<myCenter->NGroups(); i++)
	{
		AIGroup *grp = myCenter->GetGroup(i);
		if (!grp) continue;
		for (int j=0; j<MAX_UNITS_PER_GROUP; j++)
		{
			AIUnit *unit = grp->UnitWithID(j + 1);
			if (!unit) continue;
			dist = (pt - unit->Position()).SquareSizeXZ();
			if (dist < minDist)
			{
				minDist = dist;
				info._type = signUnit;
				info._unit = unit;
			}
		}
	}

	// check targets
	int n = myCenter->NTargets();
	for (int i=0; i<n; i++)
	{
		const AITargetInfo &target = myCenter->GetTarget(i);
		if (!target._type->IsKindOf(GLOB_WORLD->Preloaded(VTypeStrategic)) )
		{
			if (!target._type->IsKindOf(GLOB_WORLD->Preloaded(VTypeAllVehicles)))
				continue;
			if (target._side == myCenter->GetSide())
			{
				Object *objTgt = target._idExact;
				Transport *transport = dyn_cast<Transport>(objTgt);
				if (!transport)
					continue;
				if (transport->QIsDriverIn())
					continue;
			}
		}
		dist = (pt - target._realPos).SquareSizeXZ();
		if (dist < minDist)
		{
			if (target._type->IsKindOf(GLOB_WORLD->Preloaded(VTypeStatic)) )
			{
				minDist = dist;
				info._type = signStatic;
				info._id = target._idExact;
			}
			else
			{
				minDist = dist;
				info._type = signVehicle;
				info._id = target._idExact;
			}
		}
	}
*/

	// check waypoints
	if (myGroup) FindWaypoint(myCenter, myGroup, pt, info, minDist);

/*	
	for (int i=0; i<myCenter->NGroups(); i++)
	{
		AIGroup *grp = myCenter->GetGroup(i);
		if (!grp) continue;
		for (int j=0; j<grp->NWaypoints(); j++)
		{
			const ArcadeWaypointInfo &wInfo = grp->GetWaypoint(j);
			if (wInfo.id >= 0) continue;
			dist = (pt - wInfo.position).SquareSizeXZ();
			if (dist < minDist)
			{
				minDist = dist;
				info._type = signArcadeWaypoint;
				info._indexGroup = i;
				info._index = j;
			}
		}
	}
*/

	// check markers
	int n = markersMap.Size();
	for (int i=0; i<n; i++)
	{
		ArcadeMarkerInfo &mInfo = markersMap[i];
		dist = (pt - mInfo.position).SquareSizeXZ();
		if (dist < minDist)
		{
			minDist = dist;
			info._type = signArcadeMarker;
			info._index = i;
		}
	}

	return info;
}

void CStaticMapMain::IssueMove(float x, float y)
{
	AIUnit *me = GetMyUnit();
	AIGroup *myGroup = me ? me->GetGroup() : NULL;
	if (!myGroup) return;

	PackedBoolArray list = ListSelectedUnits();
	if (me == myGroup->Leader() && !list.IsEmpty())
	{
		// command for units
		Command cmd;
		cmd._message = Command::Move;
		cmd._context = Command::CtxUI;
		cmd._destination = ScreenToWorld(DrawCoord(x, y));
		cmd._destination[1] = GLOB_LAND->RoadSurfaceY
		(
			cmd._destination[0], cmd._destination[2]
		);
/*
		if (list.Get(me->ID() - 1))
		{
			Assert(me == GLOB_WORLD->PlayerOn()->Brain());
			GLOB_WORLD->SetPlayerManual(false);
			Verify(me->SetState(AIUnit::Wait));
		}
*/
		myGroup->SendCommand(cmd, list);

		int index = GMainMapInfo.commands.Add();
		GMainMapInfo.commands[index].grp = myGroup;
		GMainMapInfo.commands[index].id = cmd._id;
		GMainMapInfo.commands[index].state = CSSent;
		GMainMapInfo.commands[index].position = cmd._destination;
		ClearSelectedUnits();
	}
	else
	{
		Transport *veh = me->GetVehicleIn();
		if (veh && veh->CommanderUnit() == me && veh->PilotUnit() != me)
		{
			// command for vehicle
			Vector3 pos = ScreenToWorld(DrawCoord(x, y));
			pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);
			veh->SendMove(pos);
		}
	}
}

/*!
\patch 1.50 Date 4/16/2002 by Jirka
- Added: It is now possible to issue watch command from map interface
using left ALT key and click.
*/

void CStaticMapMain::IssueWatch(float x, float y)
{
	AIUnit *me = GetMyUnit();
	AIGroup *myGroup = me ? me->GetGroup() : NULL;
	if (!myGroup) return;

	PackedBoolArray list = ListSelectedUnits();
	if (me == myGroup->Leader() && !list.IsEmpty())
	{
		// command for units
		Vector3 pos;
		pos = ScreenToWorld(DrawCoord(x, y));
		pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);
		myGroup->SendState(new RadioMessageWatchPos(myGroup, list, pos));
		ClearSelectedUnits();
	}
/*
	else
	{
		Transport *veh = me->GetVehicleIn();
		if (veh && veh->CommanderUnit() == me && veh->PilotUnit() != me)
		{
			// command for vehicle
			Vector3 pos = ScreenToWorld(DrawCoord(x, y));
			pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);
			veh->SendMove(pos);
		}
	}
*/
}

void CStaticMapMain::IssueAttack(TargetType *target)
{
	AIUnit *me = GetMyUnit();
	AIGroup *myGroup = me ? me->GetGroup() : NULL;
	if (myGroup && me == myGroup->Leader())
	{
		PackedBoolArray list = ListSelectedUnits();
		Command cmd;
		cmd._message = Command::AttackAndFire;
		cmd._context = Command::CtxUI;
		cmd._target = target;
/*
		if (list.Get(me->ID() - 1))
		{
			Assert(me == GLOB_WORLD->PlayerOn()->Brain());
			GLOB_WORLD->SetPlayerManual(false);
			Verify(me->SetState(AIUnit::Wait));
		}
*/
		myGroup->SendCommand(cmd, list);
		ClearSelectedUnits();
	}
}

void CStaticMapMain::IssueGetIn(TargetType *target)
{
	AIUnit *me = GetMyUnit();
	AIGroup *myGroup = me ? me->GetGroup() : NULL;
	if (myGroup && me == myGroup->Leader())
	{
		PackedBoolArray list = ListSelectedUnits();
		Command cmd;
		cmd._message = Command::GetIn;
		cmd._context = Command::CtxUI;
		cmd._target = target;
/*
		if (list.Get(me->ID() - 1))
		{
			Assert(me == GLOB_WORLD->PlayerOn()->Brain());
			GLOB_WORLD->SetPlayerManual(false);
			Verify(me->SetState(AIUnit::Wait));
		}
*/
		myGroup->SendCommand(cmd, list);
		ClearSelectedUnits();
	}
}

bool CStaticMapMain::OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	if (nChar == 0) return false;

	if (CStaticMap::OnKeyDown(nChar, nRepCnt, nFlags))
		return true;

	switch (nChar)
	{
		case VK_DELETE:
		{
			if (_infoMove._type == signArcadeMarker)
			{
				const char *userDefined = "_user_defined";
				RString name = markersMap[_infoMove._index].name;
				if (strnicmp(name, userDefined, strlen(userDefined)) == 0)
				{
					GetNetworkManager().MarkerDelete(name);
					markersMap.Delete(_infoMove._index);
					_infoClick._type = signNone;
					_infoClickCandidate._type = signNone;
					_infoMove._type = signNone;
					_dragging = false;
					_selecting = false;
				}
			}
			return true;
		}
	}
	return false;
}

void CStaticMapMain::ProcessCheats()
{
#if _ENABLE_CHEATS
	if (GInput.GetCheat1ToDo(DIK_A))
	{
		_showUnits = !_showUnits;
	}
	if (GInput.GetCheat2ToDo(DIK_A))
	{
		_showSensors = !_showSensors;
	}
	if (GInput.GetCheat1ToDo(DIK_D))
	{
		_showTargets = !_showTargets;
	}
	if (GInput.GetCheat2ToDo(DIK_V))
	{
		_showVariables = !_showVariables;
	}
#endif
	CStaticMap::ProcessCheats();
}

void CStaticMapMain::OnLButtonDown(float x, float y)
{
/*
	PackedBoolArray list = ListSelectedUnits();
	if (!list.IsEmpty())
	{
		IssueMove(x, y);
		return;
	}
*/

	_infoClick = FindSign(x, y);
	switch (_infoClick._type)
	{
	case signArcadeWaypoint:
		{
			AICenter *center = GetMyCenter();
			if (!center) break;
			if (_infoClick._indexGroup < 0) break;
			Assert(_infoClick._indexGroup < center->NGroups());
			AIGroup *group = center->GetGroup(_infoClick._indexGroup);
			if (!group) break;
			const ArcadeWaypointInfo &wInfo = group->GetWaypoint(_infoClick._index);
			bool wpShow = wInfo.showWP == ShowAlways ||
				wInfo.showWP == ShowEasy && Glob.config.easyMode;
			if
			(
#if _ENABLE_CHEATS
				!_showUnits &&
#endif
				!wpShow
			) break;

			_dragging = true;
			_lastPos = ScreenToWorld(DrawCoord(x, y));
/*
			_dragOffset = DrawCoord(x, y);
			DrawCoord pos = WorldToScreen(wInfo.position);
			_dragOffset.x -= pos.x;
			_dragOffset.y -= pos.y;
*/
		}
		return; // avoid IssueMove
	/*
	case signUnit:
		{
			AIUnit *me = GetMyUnit();
			AIGroup *myGroup = me ? me->GetGroup() : NULL;
			if (myGroup && me == myGroup->Leader())
			{
				if (_infoClick._unit && _infoClick._unit->GetGroup() == myGroup)
				{
					int id = _infoClick._unit->ID() - 1;
					if (GetSelectedUnit(id))
					{
						SetSelectedUnit(id, NULL);
					}
					else
					{
						SetSelectedUnit(id, _infoClick._unit);
					}
				}
			}
		}
		return;
	case signVehicle:
		{
			AICenter *center = GetMyCenter();
			if (!center) break;
			EntityAI *veh = dyn_cast<EntityAI, Object>(_infoClick._id);
			if (!veh) break;
			TargetSide side = veh->GetTargetSide();
			if (center->IsEnemy(side))
			{
				IssueAttack(_infoClick._id);
				break;
			}
			Transport *transport = dyn_cast<Transport>(veh);
			if
			(
				transport &&
				(
					transport->QCanIGetIn() ||
					transport->QCanIGetInCargo() && side == center->GetSide()
				)
			)
			{
				IssueGetIn(_infoClick._id);
				break;
			}
		}
		break;
	*/
	}

//	IssueMove(x, y);
}

extern RString GMapOnSingleClick;

void CStaticMapMain::OnLButtonClick(float x, float y)
{
	if (!_dragging)
	{
		bool alt = GInput.keys[DIK_LMENU]>0;
		bool shift = GInput.keys[DIK_LSHIFT]>0;
		bool issueMove = true;
		if (GMapOnSingleClick.GetLength()>0)
		{
			Vector3 pos = ScreenToWorld(DrawCoord(x, y));
			GameState *state = GWorld->GetGameState();
			GameValue value = state->CreateGameValue(GameArray);
			GameArrayType &array = value;
			AIUnit *me = GetMyUnit();
			AIGroup *myGroup = me ? me->GetGroup() : NULL;
			if (myGroup && me == myGroup->Leader())
			{
				for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
				{
					AIUnit *unit = GetSelectedUnit(i);
					if (unit) array.Add(GameValueExt(unit->GetPerson()));
				}
			}

			GameValue posValue = state->CreateGameValue(GameArray);
			GameArrayType &posArray = posValue;
			posArray.Realloc(3);
			posArray.Add(pos[0]);
			posArray.Add(pos[2]);
			posArray.Add(pos[1]-GLandscape->SurfaceYAboveWater(pos[0],pos[2]));

			GameVarSpace vars;
			state->BeginContext(&vars);
			state->VarSetLocal("_units",value,true);
			state->VarSetLocal("_pos",posArray,true);
			state->VarSetLocal("_alt",alt,true);
			state->VarSetLocal("_shift",shift,true);
			issueMove = !state->EvaluateMultipleBool(GMapOnSingleClick);
			state->EndContext();
		}

		if (issueMove)
		{
			if (alt)
			{
				IssueWatch(x, y);
			}
			else
			{
				IssueMove(x, y);
			}
		}
	}
}

void CStaticMapMain::OnLButtonDblClick(float x, float y)
{
	_parent->CreateChild
	(new DisplayInsertMarker(
		_parent, x, y, _x, _y, _w, _h, _parent->SimulationEnabled()
	));
}

void CStaticMapMain::OnLButtonUp(float x, float y)
{
	if (_dragging)
	{
		_dragging = false;

		Assert(_infoClick._type == signArcadeWaypoint);
		AICenter *center = GetMyCenter();
		if (!center) return;
		if (_infoClick._indexGroup < 0) return;
		Assert(_infoClick._indexGroup < center->NGroups());
		AIGroup *group = center->GetGroup(_infoClick._indexGroup);
		if (!group) return;
		if (!group->GetCurrent()) return;

		int &index = group->GetCurrent()->_fsm->Var(0);
		if (index == _infoClick._index)
		{
			AIGroupContext ctx(group);
			ctx._fsm = group->GetCurrent()->_fsm;
			ctx._task = const_cast<Mission *>(group->GetMission());
			ctx._fsm->SetState(1, &ctx);
		}
	}
}

void CStaticMapMain::OnMouseHold(float x, float y, bool active)
{
	if (_dragging)
	{
		Assert(_infoClick._type == signArcadeWaypoint);
		AICenter *center = GetMyCenter();
		if (!center) return;
		if (_infoClick._indexGroup < 0) return;
		Assert(_infoClick._indexGroup < center->NGroups());
		AIGroup *group = center->GetGroup(_infoClick._indexGroup);
		if (!group) return;
		ArcadeWaypointInfo &wInfo = group->GetWaypoint(_infoClick._index);

/*
		DrawCoord ptMap = DrawCoord(x, y);
		ptMap.x -= _dragOffset.x;
		ptMap.y -= _dragOffset.y;
		Point3 pos = ScreenToWorld(ptMap);
		pos[1] = GLOB_LAND->RoadSurfaceY(pos[0], pos[2]);
		wInfo.position = pos;
*/
		Vector3 curPos = ScreenToWorld(DrawCoord(x, y));
		Vector3 offset = curPos - _lastPos;

		Vector3 pos = wInfo.position + offset;
		pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);
		wInfo.position = pos;

		_lastPos = curPos;
	}

	CStaticMap::OnMouseHold(x, y);
}

///////////////////////////////////////////////////////////////////////////////
// Warrant control

CWarrant::CWarrant(ControlsContainer *parent, int idc, const ParamEntry &cls)
	: Control(parent, CT_USER, idc, cls)
{
	_font = GLOB_ENGINE->LoadFont(GetFontID(cls>>"font"));
	const ParamEntry *entry = cls.FindEntry("size");
	if (entry) _size = (float)(*entry) * _font->Height();
	else _size = cls >> "sizeEx";
	_color = GetPackedColor(cls >> "color");
}

inline PackedColor ModAlpha( PackedColor color, float alpha )
{
	int a=toIntFloor(alpha*color.A8());
	saturate(a,0,255);
	return PackedColorRGB(color,a);
}

void CWarrant::OnDraw(float alpha)
{
	int w = GLOB_ENGINE->Width2D();
	int h = GLOB_ENGINE->Height2D();
	float size = _scale * _size;
	PackedColor color = ModAlpha(_color, alpha);

#if 0
	{
		// used for debugging purposes only
		float xx = toInt(_x * w) + 0.5;
		float yy = toInt(_y * h) + 0.5;
		float ww = toInt(_w * w);
		float hh = toInt(_h * h);
		MipInfo mip = GEngine->TextBank()->UseMipmap(NULL, 0, 0);
		PackedColor c = PackedColor(Color(0,0,1,0.3*alpha));
		GEngine->Draw2D(mip, c, xx, yy, ww, hh);
	}
#endif

	Person *soldier = GWorld->GetRealPlayer();
	AIUnit *unit = soldier ? soldier->Brain() : NULL;
	AIStatsCampaign &stats = GStats._campaign;
	//AIUnitHeader &info = stats._playerInfo;

	float height = size;
	float top = _y;
	char buffer[256];
	
	// rank, name
	sprintf
	(
		buffer,
		LocalizeString(IDS_MAIN_RANK),
		(const char *)LocalizeString(IDS_PRIVATE)
	);
	GEngine->DrawText(Point2DFloat(_x, top), size, _font, color, buffer);
	top += height;

	// assignment
	sprintf
	(
		buffer,
		"Assignement: %s",
		"infantry"
	);
	GEngine->DrawText(Point2DFloat(_x, top), size, _font, color, buffer);
	top += height;

	// number in group
	if (unit)
	{
		sprintf
		(
			buffer,
			"Number in group: %d",
			unit->ID()
		);
		GEngine->DrawText(Point2DFloat(_x, top), size, _font, color, buffer);
		top += height;
	}

	top += height;

	// in combat
	sprintf
	(
		buffer,
		"In combat: %d minutes",
		toInt(stats._inCombat / 60.0)
	);
	GEngine->DrawText(Point2DFloat(_x, top), size, _font, color, buffer);
	top += height;

	top += height;

	// kills
	GEngine->DrawText(Point2DFloat(_x, top), size, _font, color, "Kills:");
	top += height;
	// draw table
	float w1 = 0.2 * _w;
	float ht = 0.2 * height;
	float h1 = height + 2.0 * ht;
	float x0 = _x, xx0 = x0 * w;
	float x1 = x0 + w1, xx1 = x1 * w;
	float x2 = x1 + w1, xx2 = x2 * w;
	float x3 = x2 + w1, xx3 = x3 * w;
	float x4 = x3 + w1, xx4 = x4 * w;
	float x5 = x4 + w1, xx5 = x5 * w;
	float y0 = top, yy0 = y0 * h;
	float y1 = y0 + h1, yy1 = y1 * h;
	float y2 = y1 + h1, yy2 = y2 * h;
	float y3 = y2 + h1, yy3 = y3 * h;
	float y4 = y3 + h1, yy4 = y4 * h;
	GEngine->DrawLine(Line2DPixel(xx1, yy0, xx5, yy0), color, color);
	GEngine->DrawLine(Line2DPixel(xx0, yy1, xx5, yy1), color, color);
	GEngine->DrawLine(Line2DPixel(xx0, yy2, xx5, yy2), color, color);
	GEngine->DrawLine(Line2DPixel(xx0, yy3, xx5, yy3), color, color);
	GEngine->DrawLine(Line2DPixel(xx0, yy4, xx5, yy4), color, color);
	GEngine->DrawLine(Line2DPixel(xx0, yy1, xx0, yy4), color, color);
	GEngine->DrawLine(Line2DPixel(xx1, yy0, xx1, yy4), color, color);
	GEngine->DrawLine(Line2DPixel(xx2, yy0, xx2, yy4), color, color);
	GEngine->DrawLine(Line2DPixel(xx3, yy0, xx3, yy4), color, color);
	GEngine->DrawLine(Line2DPixel(xx4, yy0, xx4, yy4), color, color);
	GEngine->DrawLine(Line2DPixel(xx5, yy0, xx5, yy4), color, color);
	float wt = GEngine->GetTextWidth(size, _font, "Enemies");
	GEngine->DrawText(Point2DFloat(x0 + 0.5 * (w1 - wt), y1 + ht), size, _font, color, "Enemies");
	wt = GEngine->GetTextWidth(size, _font, "Friends");
	GEngine->DrawText(Point2DFloat(x0 + 0.5 * (w1 - wt), y2 + ht), size, _font, color, "Friends");
	wt = GEngine->GetTextWidth(size, _font, "Civilians");
	GEngine->DrawText(Point2DFloat(x0 + 0.5 * (w1 - wt), y3 + ht), size, _font, color, "Civilians");
	wt = GEngine->GetTextWidth(size, _font, "Infantry");
	GEngine->DrawText(Point2DFloat(x1 + 0.5 * (w1 - wt), y0 + ht), size, _font, color, "Infantry");
	wt = GEngine->GetTextWidth(size, _font, "Mobile");
	GEngine->DrawText(Point2DFloat(x2 + 0.5 * (w1 - wt), y0 + ht), size, _font, color, "Mobile");
	wt = GEngine->GetTextWidth(size, _font, "Armored");
	GEngine->DrawText(Point2DFloat(x3 + 0.5 * (w1 - wt), y0 + ht), size, _font, color, "Armored");
	wt = GEngine->GetTextWidth(size, _font, "Air");
	GEngine->DrawText(Point2DFloat(x4 + 0.5 * (w1 - wt), y0 + ht), size, _font, color, "Air");
	int n = stats._kills[SKEnemyInfantry];
	if (n > 0)
	{
		wt = GEngine->GetTextWidthF(size, _font, "%d", n);
		GEngine->DrawTextF(Point2DFloat(x1 + 0.5 * (w1 - wt), y1 + ht), size, _font, color, "%d", n);
	}
	n = stats._kills[SKEnemySoft];
	if (n > 0)
	{
		wt = GEngine->GetTextWidthF(size, _font, "%d", n);
		GEngine->DrawTextF(Point2DFloat(x2 + 0.5 * (w1 - wt), y1 + ht), size, _font, color, "%d", n);
	}
	n = stats._kills[SKEnemyArmor];
	if (n > 0)
	{
		wt = GEngine->GetTextWidthF(size, _font, "%d", n);
		GEngine->DrawTextF(Point2DFloat(x3 + 0.5 * (w1 - wt), y1 + ht), size, _font, color, "%d", n);
	}
	n = stats._kills[SKEnemyAir];
	if (n > 0)
	{
		wt = GEngine->GetTextWidthF(size, _font, "%d", n);
		GEngine->DrawTextF(Point2DFloat(x4 + 0.5 * (w1 - wt), y1 + ht), size, _font, color, "%d", n);
	}
	n = stats._kills[SKFriendlyInfantry];
	if (n > 0)
	{
		wt = GEngine->GetTextWidthF(size, _font, "%d", n);
		GEngine->DrawTextF(Point2DFloat(x1 + 0.5 * (w1 - wt), y2 + ht), size, _font, color, "%d", n);
	}
	n = stats._kills[SKFriendlySoft];
	if (n > 0)
	{
		wt = GEngine->GetTextWidthF(size, _font, "%d", n);
		GEngine->DrawTextF(Point2DFloat(x2 + 0.5 * (w1 - wt), y2 + ht), size, _font, color, "%d", n);
	}
	n = stats._kills[SKFriendlyArmor];
	if (n > 0)
	{
		wt = GEngine->GetTextWidthF(size, _font, "%d", n);
		GEngine->DrawTextF(Point2DFloat(x3 + 0.5 * (w1 - wt), y2 + ht), size, _font, color, "%d", n);
	}
	n = stats._kills[SKFriendlyAir];
	if (n > 0)
	{
		wt = GEngine->GetTextWidthF(size, _font, "%d", n);
		GEngine->DrawTextF(Point2DFloat(x4 + 0.5 * (w1 - wt), y2 + ht), size, _font, color, "%d", n);
	}
	n = stats._kills[SKCivilianInfantry];
	if (n > 0)
	{
		wt = GEngine->GetTextWidthF(size, _font, "%d", n);
		GEngine->DrawTextF(Point2DFloat(x1 + 0.5 * (w1 - wt), y3 + ht), size, _font, color, "%d", n);
	}
	n = stats._kills[SKCivilianSoft];
	if (n > 0)
	{
		wt = GEngine->GetTextWidthF(size, _font, "%d", n);
		GEngine->DrawTextF(Point2DFloat(x2 + 0.5 * (w1 - wt), y3 + ht), size, _font, color, "%d", n);
	}
	n = stats._kills[SKCivilianArmor];
	if (n > 0)
	{
		wt = GEngine->GetTextWidthF(size, _font, "%d", n);
		GEngine->DrawTextF(Point2DFloat(x3 + 0.5 * (w1 - wt), y3 + ht), size, _font, color, "%d", n);
	}
	n = stats._kills[SKCivilianAir];
	if (n > 0)
	{
		wt = GEngine->GetTextWidthF(size, _font, "%d", n);
		GEngine->DrawTextF(Point2DFloat(x4 + 0.5 * (w1 - wt), y3 + ht), size, _font, color, "%d", n);
	}
}

///////////////////////////////////////////////////////////////////////////////
// Main Map display

WeatherState GetWeatherState(float overcast)
{
	if (overcast < 0.2) return WSClear;
	else if (overcast < 0.4) return WSCloudly;
	else if (overcast < 0.6) return WSOvercast;
	else if (overcast < 0.8) return WSRainy;
	else return WSStormy;
}

static const EnumName ObjectiveStatusNames[]=
{
	EnumName(OSActive, "ACTIVE"),
	EnumName(OSDone, "DONE"),
	EnumName(OSFailed, "FAILED"),
	EnumName(OSHidden, "HIDDEN"),
	EnumName()
};

template<>
const EnumName *GetEnumNames(ObjectiveStatus dummy)
{
	return ObjectiveStatusNames;
}

#include "saveVersion.hpp"

LSError UnitWeaponsInfo::Serialize(ParamArchive &ar)
{
	CHECK(ar.SerializeRef("Unit", unit, 1))
	CHECK(ar.Serialize("name", name, 1))
	CHECK(ar.Serialize("weaponSlots", weaponSlots, 1))
	for (int i=0; i<WEAPON_SLOTS; i++)
	{
		char buffer[256];
		sprintf(buffer, "Weapon%d", i);
		CHECK(ar.Serialize(buffer, weapons[i], 1))
	}
	for (int i=0; i<MAGAZINE_SLOTS; i++)
	{
		char buffer[256];
		sprintf(buffer, "Magazine%d", i);
		CHECK(ar.Serialize(buffer, magazines[i], 1))
	}
	return LSOK;
}

Ref<Magazine> WeaponsInfo::RemovePoolMagazine(const MagazineType *type)
{
	// add new magazine
	for (int i=0; i<_magazinesPool.Size(); i++)
	{
		if (_magazinesPool[i]->_type == type)
		{
			Ref<Magazine> magazine = _magazinesPool[i];
			_magazinesPool.Delete(i);
			return magazine;
		}
	}
	return NULL;
}

bool WeaponsInfo::Load(RString filename)
{
	ParamArchiveLoad ar;
	if (!ar.LoadBin(filename) && ar.Load(filename) != LSOK) return false;
	ar.FirstPass();
	if (Serialize(ar) != LSOK) return false;
	ar.SecondPass();
	if (Serialize(ar) != LSOK) return false;

	// assign new ID to all magazines
	for (int i=0; i<_weapons.Size(); i++)
	{
		UnitWeaponsInfo &info = _weapons[i];
		for (int j=0; j<MAGAZINE_SLOTS;)
		{
			Magazine *magazine = info.magazines[j++];
			if (!magazine) continue;
			int oldId = magazine->_id;
			magazine->_id = GWorld->GetMagazineID();
			while (j < MAGAZINE_SLOTS)
			{
				Magazine *m = info.magazines[j];
				if (!m || m->_id != oldId) break;
				info.magazines[j++] = magazine;
			}
		}
	}

	for (int j=0; j<_magazinesPool.Size();)
	{
		Magazine *magazine = _magazinesPool[j++];
		if (!magazine) continue;
		int oldId = magazine->_id;
		magazine->_id = GWorld->GetMagazineID();
		while (j < _magazinesPool.Size())
		{
			Magazine *m = _magazinesPool[j];
			if (!m || m->_id != oldId) break;
			_magazinesPool[j++] = magazine;
		}
	}

	return true;
}

bool WeaponsInfo::Save(RString filename)
{
	ParamArchiveSave ar(UserInfoVersion);
	if (Serialize(ar) != LSOK) return false;
#if _ENABLE_CHEATS
	return ar.Save(filename) == LSOK;
#else
	return ar.SaveBin(filename);
#endif
}

LSError WeaponsInfo::Serialize(ParamArchive &ar)
{
	ar.Serialize("Weapons", _weapons, 1);
	ar.Serialize("WeaponsPool", _weaponsPool, 1);
	ar.Serialize("MagazinesPool", _magazinesPool, 1);
	return LSOK;
}

bool WeaponsInfo::Import(const ParamEntry &superclass)
{
	const ParamEntry *cls = superclass.FindEntry("Weapons");
	if (cls)
	{
		int mask = MaskSlotPrimary | MaskSlotSecondary | MaskSlotBinocular | MaskSlotHandGun;
		for (int i=0; i<cls->GetEntryCount(); i++)
		{
			const ParamEntry &entry = cls->GetEntry(i);
			Ref<WeaponType> weapon = WeaponTypes.New(entry.GetName());
			if (!weapon) continue;
			if (weapon->_scope < 2) continue;
			if ((weapon->_weaponType & mask) == 0) continue;
			int count = entry >> "count";
			for (int j=0; j<count; j++) _weaponsPool.Add(weapon);
		}
	}
	cls = superclass.FindEntry("Magazines");
	if (cls)
	{
		int mask = MaskSlotItem | MaskSlotHandGunItem;
		for (int i=0; i<cls->GetEntryCount(); i++)
		{
			const ParamEntry &entry = cls->GetEntry(i);
			Ref<MagazineType> type = MagazineTypes.New(entry.GetName());
			if (!type) continue;
			if (type->_scope < 2) continue;
			if ((type->_magazineType & mask) == 0) continue;
			int count = entry >> "count";
			for (int j=0; j<count; j++)
			{
				Ref<Magazine> magazine = new Magazine(type);
				magazine->_ammo = type->_maxAmmo;
				magazine->_reloadMagazine = 0;
				magazine->_reload = 0;
				if (type->_modes.Size() > 0)
				{
					float	reload = 0.8 + 0.2 * GRandGen.RandomValue();
					magazine->_reload = reload * type->_modes[0]->_reloadTime;
				}
				_magazinesPool.Add(magazine);
			}
		}
	}
	return true;
}

/*!
\patch 1.80 Date 8/5/2002 by Jirka
- Fixed: When replaying mission from campaign, weapon pool was not loaded.
*/

bool EnabledWeaponPool()
{
#if _ENABLE_DATADISC
	// if (!IsCampaign()) return false;
	const ParamEntry *entry = ExtParsCampaign.FindEntry("weaponPool");
	if (!entry) return false;
	return (bool)(*entry);
#else
	return false;
#endif
}

/*!
\patch 1.14 Date 08/10/2001 by Jirka
- Fixed: changes of weapons in briefing are sent over network in MP
*/
void WeaponsInfo::Apply()
{
	// add weapons
	for (int i=0; i<_weapons.Size(); i++)
	{
		UnitWeaponsInfo &info = _weapons[i];
		AIUnit *unit = info.unit;
		if (!unit) continue;
		Person *veh = unit->GetPerson();
		veh->RemoveAllWeapons();
		veh->RemoveAllMagazines();
		for (int j=0; j<MAGAZINE_SLOTS; j++)
		{
			Ref<Magazine> magazine = info.magazines[j];
			if (!magazine) continue;
			info.RemoveMagazine(magazine);
			veh->AddMagazine(magazine);
		}
		for (int j=0; j<WEAPON_SLOTS; j++)
		{
			Ref<WeaponType> weapon = info.weapons[j];
			if (!weapon) continue;
			info.RemoveWeapon(weapon);
			veh->AddWeapon(weapon);
		}
		veh->AddDefaultWeapons();
		if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == veh)
			GWorld->UI()->ResetVehicle(veh);

		veh->AutoReloadAll();

		// FIX
		if (veh->SelectedWeapon() < 0) veh->SelectWeapon(veh->FirstWeapon(), true);
		if (!veh->IsLocal()) GetNetworkManager().UpdateWeapons(veh);
	}

	if (EnabledWeaponPool())
	{
		void CampaignSaveWeaponPool(WeaponsInfo &pool);
		CampaignSaveWeaponPool(*this);
	}

	_weapons.Clear();
}

/*!
\patch 1.07 Date 7/20/2001 by Jirka
- Fixed: after install or new user create marker centering in map does not work properly
*/
DisplayMap::DisplayMap(ControlsContainer *parent, RString resource)
: Display(parent)
{
	_alwaysShow = true;

	_cursor = CursorArrow;
//	_debriefing = false;
	_selectWeapons = false;
	_currentSlot = -1;

	Load(resource);
	// FIX
	AdjustMapVisibleRect();

	Init();
	ResetHUD();

	ShowMap(true);
	ShowCompass(true);
	ShowWatch(true);
	ShowWalkieTalkie(true);
	ShowNotepad(true);
//	ShowWarrant(IsCampaign());
	ShowWarrant(false);
	ShowGPS(false);
}

void DisplayMap::SwitchBriefingSection(RString section)
{
	_briefing->SwitchSection(section);
}

DisplayMap::~DisplayMap()
{
	SaveParams();
}

bool DisplayMap::IsShownMap() const
{
	return _map->IsVisible();	
}

bool DisplayMap::IsShownCompass() const
{
	return _compass->IsVisible();	
}

bool DisplayMap::IsShownWatch() const
{
	return _watch->IsVisible();	
}

bool DisplayMap::IsShownWalkieTalkie() const
{
	return _walkieTalkie->IsVisible();	
}

bool DisplayMap::IsShownNotepad() const
{
	return _notepad->IsVisible();	
}

bool DisplayMap::IsShownWarrant() const
{
	return _warrant->IsVisible();	
}

bool DisplayMap::IsShownGPS() const
{
	return _gps->IsVisible();	
}

void DisplayMap::ShowMap(bool show)
{
	_map->ShowCtrl(show);
}

void DisplayMap::ShowCompass(bool show)
{
	_compass->ShowCtrl(show);
}

void DisplayMap::ShowWatch(bool show)
{
	_watch->ShowCtrl(show);
}

void DisplayMap::ShowWalkieTalkie(bool show)
{
	_walkieTalkie->ShowCtrl(show);
}

void DisplayMap::ShowNotepad(bool show)
{
	_notepad->ShowCtrl(show);
	AdjustMapVisibleRect();
}

void DisplayMap::ShowWarrant(bool show)
{
	_warrant->ShowCtrl(show);
}

void DisplayMap::ShowGPS(bool show)
{
	_gps->ShowCtrl(show);
}

RString GetMissionDirectory();

RString GetEquipmentFile()
{
	RString prefix("dtaExt\\equip\\equipment.");
	RString name = prefix + GLanguage + RString(".html");
	if (QIFStreamB::FileExist(name)) return name;
	name = prefix + RString("html");
	if (QIFStreamB::FileExist(name)) return name;
	return RString();
}

static int FindSectionForUnit(CHTML *html, RString sectionName, AIUnit *unit)
{
	RString prefix = sectionName + RString(".");
	int section = -1;
	if (unit)
	{
		RString name = unit->GetVehicle()->GetVarName();
		if (name.GetLength() > 0)
		{
			section = html->FindSection(prefix + name);
		}
		if (section < 0)
		{
			AIGroup *grp = unit->GetGroup();
			if (grp)
			{
				AIUnit *leader = grp->Leader();
				if (leader && leader != unit)
				{
					name = leader->GetVehicle()->GetVarName();
					if (name.GetLength() > 0)
					{
						section = html->FindSection(prefix + name);
					}
				}
				if (section < 0)
				{
					AICenter *center = grp->GetCenter();
					if (center)
					{
						switch (center->GetSide())
						{
							case TWest:
								name = prefix + RString("West"); goto SideFound;
							case TEast:
								name = prefix + RString("East"); goto SideFound;
							case TGuerrila:
								name = prefix + RString("Guerrila"); goto SideFound;
							case TCivilian:
								name = prefix + RString("Civilian"); goto SideFound;
							SideFound:
								section = html->FindSection(name);
								break;
						}
					}
				}
			}
		}
	}
	if (section < 0) section = html->FindSection(sectionName);
	return section;
}

void DisplayMap::Init()
{
	// setting default values
	_map->SetScale(-1);
	_map->Center();
	_map->Reset();

	// load values
	LoadParams();

	// load briefing
	RString briefing = GetBriefingFile();
	if (briefing.GetLength() > 0)
	_briefing->Load(briefing);
	// assign briefing
	AIUnit *unit = GWorld->FocusOn();
	int section = FindSectionForUnit(_briefing, "Main", unit);
	if (section >= 0)
		_briefing->AddName(section, "__BRIEFING");
	UpdatePlan();

#if _ENABLE_BRIEF_EQ
	// load equipment pages
	_briefing->Load(GetEquipmentFile(), true);
#endif

	// find bookmarks
	_briefing->AddBookmark("__PLAN");
	_briefing->AddBookmark("__BRIEFING");
#if _ENABLE_BRIEF_EQ
	_briefing->AddBookmark("Equipment");
#else
	_bookmark3->SetText("");
#endif
#if _ENABLE_BRIEF_GRP
	_briefing->AddBookmark("Group");
#else
	_bookmark4->SetText("");
#endif
/*
	_briefing->AddBookmark("Debriefing:Notes");
	_briefing->AddBookmark("Debriefing:Log");
*/

	if (_briefing->FindSection("__PLAN") >= 0)
		SwitchBriefingSection("__PLAN");
	else if (_briefing->FindSection("__BRIEFING") >= 0)
		SwitchBriefingSection("__BRIEFING");
#if _ENABLE_BRIEF_EQ
	else
		SwitchBriefingSection("Equipment");
#endif

#if 0
	Vector3 pos = _watch->Position();
	Vector3 posMin = _watch->PositionModelToWorld(_watch->GetShape()->Min());
	Vector3 posMax = _watch->PositionModelToWorld(_watch->GetShape()->Max());
	float dmin = posMin.Z() - pos.Z();
	float dmax = posMax.Z() - pos.Z();
	LogF("Watch %.3f - %.3f", _posWatch.Z() + dmin, _posWatch.Z() + dmax);
	LogF("Watch zoomed %.3f - %.3f", _posWatchZoomed.Z() + dmin, _posWatchZoomed.Z() + dmax);

	pos = _compass->Position();
	posMin = _compass->PositionModelToWorld(_compass->GetShape()->Min());
	posMax = _compass->PositionModelToWorld(_compass->GetShape()->Max());
	dmin = posMin.Z() - pos.Z();
	dmax = posMax.Z() - pos.Z();
	LogF("Compass %.3f - %.3f", _posCompass.Z() + dmin, _posCompass.Z() + dmax);
	LogF("Compass zoomed %.3f - %.3f", _posCompassZoomed.Z() + dmin, _posCompassZoomed.Z() + dmax);

	pos = _walkieTalkie->Position();
	posMin = _walkieTalkie->PositionModelToWorld(_walkieTalkie->GetShape()->Min());
	posMax = _walkieTalkie->PositionModelToWorld(_walkieTalkie->GetShape()->Max());
	dmin = posMin.Z() - pos.Z();
	dmax = posMax.Z() - pos.Z();
	LogF("WalkieTalkie %.3f - %.3f", _posWalkieTalkie.Z() + dmin, _posWalkieTalkie.Z() + dmax);
	LogF("WalkieTalkie zoomed %.3f - %.3f", _posWalkieTalkieZoomed.Z() + dmin, _posWalkieTalkieZoomed.Z() + dmax);

	pos = _notepad->Position();
	posMin = _notepad->PositionModelToWorld(_notepad->GetShape()->Min());
	posMax = _notepad->PositionModelToWorld(_notepad->GetShape()->Max());
	dmin = posMin.Z() - pos.Z();
	dmax = posMax.Z() - pos.Z();
	LogF("Notepad %.3f - %.3f", _posNotepad.Z() + dmin, _posNotepad.Z() + dmax);
	LogF("Notepad zoomed %.3f - %.3f", _posNotepadZoomed.Z() + dmin, _posNotepadZoomed.Z() + dmax);

	pos = _warrant->Position();
	posMin = _warrant->PositionModelToWorld(_warrant->GetShape()->Min());
	posMax = _warrant->PositionModelToWorld(_warrant->GetShape()->Max());
	dmin = posMin.Z() - pos.Z();
	dmax = posMax.Z() - pos.Z();
	LogF("Warrant %.3f - %.3f", _posWarrant.Z() + dmin, _posWarrant.Z() + dmax);
	LogF("Warrant zoomed %.3f - %.3f", _posWarrantZoomed.Z() + dmin, _posWarrantZoomed.Z() + dmax);

	pos = _gps->Position();
	posMin = _gps->PositionModelToWorld(_gps->GetShape()->Min());
	posMax = _gps->PositionModelToWorld(_gps->GetShape()->Max());
	dmin = posMin.Z() - pos.Z();
	dmax = posMax.Z() - pos.Z();
	LogF("GPS %.3f - %.3f", _posGPS.Z() + dmin, _posGPS.Z() + dmax);
	LogF("GPS zoomed %.3f - %.3f", _posGPSZoomed.Z() + dmin, _posGPSZoomed.Z() + dmax);

#endif
}

void DisplayMap::ResetHUD()
{
	if (_map) _map->Reset();
	if (_briefing)
	{
		UpdateWeaponsInBriefing();
		UpdateUnitsInBriefing();
//		UpdateDebriefing();
	}
	GInput.cursorX = 0;
	GInput.cursorY = 0;
}

ControlObject *DisplayMap::OnCreateObject(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
	case IDC_MAP_WATCH:
		_watch = new Watch(this, idc, cls);
		return _watch;
	case IDC_MAP_COMPASS:
		_compass = new Compass(this, idc, cls);
		return _compass;
	case IDC_MAP_WALKIE_TALKIE:
		_walkieTalkie = new ControlObjectContainer(this, idc, cls);
		return _walkieTalkie;
	case IDC_MAP_NOTEPAD:
		_notepad = new Notepad(this, idc, cls);
		_notepad->SetBriefing(_briefing);
		return _notepad;
	case IDC_MAP_WARRANT:
		_warrant = new ControlObjectContainer(this, idc, cls);
		return _warrant;
	case IDC_MAP_GPS:
		_gps = new ControlObjectContainer(this, idc, cls);
		return _gps;
	default:
			return Display::OnCreateObject(type, idc, cls);
	}
}

bool DisplayMap::OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	if (_map && GetCtrl(IDC_MAP)->IsVisible())
	{
		if (_map->OnKeyDown(nChar, nRepCnt, nFlags)) return true;
	}
	return Display::OnKeyDown(nChar, nRepCnt, nFlags);
}

bool DisplayMap::OnKeyUp(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	if (_map && GetCtrl(IDC_MAP)->IsVisible())
	{
		if (_map->OnKeyUp(nChar, nRepCnt, nFlags)) return true;
	}
	return Display::OnKeyUp(nChar, nRepCnt, nFlags);
}

UnitWeaponsInfo::UnitWeaponsInfo(AIUnit *u)
{
	unit = u;
	name = u->GetPerson()->GetInfo()._name;
	Person *veh = u->GetPerson();
	weaponSlots = veh->GetType()->_weaponSlots;
	for (int i=0; i<veh->NWeaponSystems(); i++)
	{
		WeaponType *weapon = const_cast<WeaponType *>(veh->GetWeaponSystem(i));
		if (weapon->_scope < 2) continue;

		int slots = weapon->_weaponType;
		if (slots & MaskSlotPrimary)
			weapons[0] = weapon;
		if (slots & MaskSlotSecondary)
			weapons[1] = weapon;
		if (slots & MaskSlotBinocular)
		{
			if (!weapons[2])
				weapons[2] = weapon;
			else
				weapons[3] = weapon;
		}
		if (slots & MaskSlotHandGun)
		{
			weapons[4] = weapon;
		}
	}
	int item = 0, itemHandGun = 10;
	for (int i=0; i<veh->NMagazines(); i++)
	{
		Magazine *magazine = const_cast<Magazine *>(veh->GetMagazine(i));
		if (!magazine) continue;
		if (magazine->_ammo == 0) continue;
		MagazineType *type = magazine->_type;
		if (type->_scope < 2) continue;
		int slots = type->_magazineType;
		if (slots & MaskSlotItem)
		{
			int nItems = GetItemSlotsCount(slots);
			if (nItems == 0) continue;
			for (int j=0; j<nItems; j++) magazines[item++] = magazine;
		}
		else if (slots & MaskSlotHandGunItem)
		{
			int nItems = GetHandGunItemSlotsCount(slots);
			if (nItems == 0) continue;
			for (int j=0; j<nItems; j++) magazines[itemHandGun++] = magazine;
		}
	}
}

bool UnitWeaponsInfo::IsMagazineUsable(const MagazineType *type)
{
	for (int i=0; i<WEAPON_SLOTS; i++)
	{
		const WeaponType *weapon = weapons[i];
		if (!weapon) continue;
		for (int j=0; j<weapon->_muzzles.Size(); j++)
		{
			const MuzzleType *muzzle = weapon->_muzzles[j];
			for (int k=0; k<muzzle->_magazines.Size(); k++)
				if (muzzle->_magazines[k] == type) return true;
		}
	}
	Ref<const WeaponType> always[2] =
	{
		WeaponTypes.New("Throw"),
		WeaponTypes.New("Put"),
//		WeaponTypes.New("PipeBomb")
	};
	for (int i=0; i<sizeof(always)/sizeof(*always); i++)
	{
		const WeaponType *weapon = always[i];
		if (!weapon) continue;
		for (int j=0; j<weapon->_muzzles.Size(); j++)
		{
			const MuzzleType *muzzle = weapon->_muzzles[j];
			for (int k=0; k<muzzle->_magazines.Size(); k++)
				if (muzzle->_magazines[k] == type) return true;
		}
	}
	return false;
}

void UpdateWeaponsInBriefing()
{
	DisplayMap *map = dynamic_cast<DisplayMap *>(GWorld->Map());
	if (map) map->UpdateWeaponsInBriefing();
}

void DisplayMap::UpdateWeaponsInBriefing()
{
	// create structure
	AIUnit *me = GWorld->FocusOn();
	if (!me) return;
	AIGroup *grp = me->GetGroup();
	if (!grp) return;

	AIUnit *oldUnit = NULL;
	if (_currentUnit >= 0 && _currentUnit < _weaponsInfo._weapons.Size())
		oldUnit = _weaponsInfo._weapons[_currentUnit].unit;
	
	_currentUnit = -1;
	_currentSlot = -1;
	_weaponsInfo._weapons.Resize(0);

	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = grp->UnitWithID(i + 1);
		if (unit) _weaponsInfo._weapons.Add(UnitWeaponsInfo(unit));
	}
	if (oldUnit) for (int i=0; i<_weaponsInfo._weapons.Size(); i++)
	{
		if (_weaponsInfo._weapons[i].unit == oldUnit)
		{
			_currentUnit = i;
			break;
		}
	}
	if (_currentUnit < 0) for (int i=0; i<_weaponsInfo._weapons.Size(); i++)
	{
		if (_weaponsInfo._weapons[i].unit == me)
		{
			_currentUnit = i;
			break;
		}
	}
	Assert(_currentUnit >= 0);

	// create html page
	CreateWeaponsPage();

	if (_selectWeapons)
	{
		// load weapons pool
		if (EnabledWeaponPool())
		{
			void CampaignLoadWeaponPool(WeaponsInfo &pool);
			CampaignLoadWeaponPool(_weaponsInfo);
		}
		else
			_weaponsInfo.Import(ExtParsMission);
	}
}

/*!
\patch_internal 1.06 Date 7/19/2001 by Jirka
- Added: enable briefing pictures for weapons in AddOns
*/

static RString GetWeaponPicture(const WeaponType *weapon)
{
	RString name = weapon->GetPictureName();
	if (name[0] == '\\')
		return name;
	else
	{
		RString prefixW = "equip\\w\\w_";
		return prefixW + name + RString(".paa");
	}
}

static RString GetMagazinePicture(const MagazineType *type)
{
	RString name = type->GetPictureName();
	if (name[0] == '\\')
		return name;
	else
	{
		RString prefixM = "equip\\m\\m_";
		return prefixM + name + RString(".paa");
	}
}

static RString GetEquipmentSection(RString name)
{
	if (name[0] == '\\') return "";
	else return RString("#EQ_") + RString(name);
}

#ifndef min
#ifndef _XBOX
inline int min(int a, int b)
{
	return a<b ? a : b;
}
#endif
#endif

/*!
\patch 1.75 Date 2/11/2002 by Jirka
- Improved: Gear page in briefing 
*/

void DisplayMap::CreateWeaponsPage()
{
#if _ENABLE_BRIEF_EQ
	// create section "Equipment"
	bool actual = false;
	int section = _briefing->FindSection("Equipment");
	if (section < 0)
	{
		section = _briefing->AddSection();
	}
	else
	{
		_briefing->InitSection(section);
		if (_briefing->CurrentSection() == section) actual = true;
		int s;
		while ((s = _briefing->FindSection("Equipment")) >= 0)
		{
			if (_briefing->CurrentSection() == s) actual = true;
			_briefing->RemoveSection(s);
		}
	}
	_briefing->AddName(section, "Equipment");

	// item sizes
	const int weaponsPerLine = 2;
	const int itemsPerLine = 6;
	const int iconsPerItem = 4;
	const int iconsPerWeapon = iconsPerItem * itemsPerLine / weaponsPerLine;

	const float pageWidth = (640.0f - 3.0f) * _briefing->GetPageWidth(); // avoid round probl.
	const float weaponWidth = (1.0 / weaponsPerLine) * pageWidth;
	const float itemWidth = (1.0 / itemsPerLine) * pageWidth;
	float iconWidth = (1.0 / iconsPerItem) * itemWidth;

	float arrowHeight = 480.0f * 1.5 * _briefing->GetPHeight();

	const float weaponHeight = -1;
//	const float emptyWeaponHeight = 48;
	const float itemHeight = 32;
//	const float emptyItemHeight = 32;

	RString emptySlot = LocalizeString(IDS_EMPTY_SLOT);
	RString emptyGun = "equip\\emptygun.paa";
	RString emptySec = "equip\\emptysec.paa";
	RString emptyEq = "equip\\emptyeq.paa";
	RString emptyMag = "equip\\emptymag.paa";
	RString emptyMag2 = "equip\\emptymag2.paa";
	RString emptyHGun = "\\misc\\gun_empty.paa";
	RString emptyHGunMag = "\\misc\\mgun_empty.paa";

	// create page
	UnitWeaponsInfo &info = _weaponsInfo._weapons[_currentUnit];

	bool canDrop = false;
	if (info.unit && IsInGame())
	{
		Person *player = GWorld->GetRealPlayer();
		AIUnit *playerUnit = player ? player->Brain() : NULL;
		if (playerUnit)
		{
			if (info.unit == playerUnit) canDrop = true;
			else
			{
				AIGroup *grp = info.unit->GetGroup();
				canDrop = grp && grp->Leader() == playerUnit;
			}
		}
	}

	// name
#if _ENABLE_DATADISC
	float pw = _briefing->GetPageWidth();
	if (_currentUnit > 0)
		_briefing->AddImage(section, "sipka_left.paa", HACenter, false, -1.0, arrowHeight, "Gear:Prev", RString(), 0.1 * pw);
	else
		_briefing->AddImage(section, "", HACenter, false, -1.0, arrowHeight, "", RString(), 0.1 * pw);
	RString name = Format("%d: %s", info.unit ? info.unit->ID() : 0, (const char *)info.name);
	_briefing->AddText(section, name, HFH2, HACenter, false, false, "", 0.8 * pw);
	if (_currentUnit < _weaponsInfo._weapons.Size() - 1)
		_briefing->AddImage(section, "sipka_right.paa", HACenter, false, -1.0, arrowHeight, "Gear:Next", RString(), 0.1 * pw);
	else
		_briefing->AddImage(section, "", HACenter, false, -1.0, arrowHeight, "", RString(), 0.1 * pw);
#else
	if (_currentUnit > 0)
		_briefing->AddImage(section, "sipka_left.paa", HACenter, false, -1.0, arrowHeight, "Gear:Prev");
	else
		_briefing->AddImage(section, "", HACenter, false, -1.0, arrowHeight, "");
	_briefing->AddText(section, info.name, HFH2, HACenter, false, false, "");
	if (_currentUnit < _weaponsInfo._weapons.Size() - 1)
		_briefing->AddImage(section, "sipka_right.paa", HACenter, false, -1.0, arrowHeight, "Gear:Next");
	else
		_briefing->AddImage(section, "", HACenter, false, -1.0, arrowHeight, "");
#endif
	_briefing->AddBreak(section, false);
//	_briefing->AddBreak(section, false);

	// weapons
	if ((info.weaponSlots & (MaskSlotPrimary | MaskSlotSecondary)) != 0)
	{
/*
		_briefing->AddText(section, LocalizeString(IDS_BRIEF_WEAPONS), HFH3, HALeft, false, false, "");
		_briefing->AddBreak(section, false);
*/
		int slot = -1;
		if ((info.weaponSlots & MaskSlotPrimary) == 0)
			slot = 1; // only secondary weapon
		else if ((info.weaponSlots & MaskSlotSecondary) == 0)
			slot = 0; // only primary weapon
		else if (info.weapons[0] && info.weapons[0] == info.weapons[1])
			slot = 0; // weapon in both slots
		if (slot >= 0)
		{
			const WeaponType *weapon = info.weapons[slot];
			// weapon picture
			RString href = "";
			if (_selectWeapons)
			{
				char buffer[7]; strcpy(buffer, "Slot:A");
				buffer[5] += slot;
				href = buffer;
			}
			if (weapon)
			{
				RString img = GetWeaponPicture(weapon);
				RString text = weapon->GetDisplayName();
				_briefing->AddImage(section, img, HACenter, false, weaponWidth, weaponHeight, href, text);
			}
			else
			{
				_briefing->AddImage(section, emptyGun, HACenter, false, weaponWidth, weaponHeight, href, emptySlot);
			}
			_briefing->AddBreak(section, false);
			// icons
			int rest = iconsPerWeapon;
			if (weapon)
			{
				href = GetEquipmentSection(weapon->GetPictureName());
				if (href.GetLength() > 0)
				{
					_briefing->AddImage(section, "i.paa", HACenter, false, iconWidth, iconWidth, href);
					rest--;
				}
				if (canDrop)
				{
					href = Format("DropWeapon:%s", (const char *)weapon->GetName());
					_briefing->AddImage(section, "\\misc\\poloz.paa", HACenter, false, iconWidth, iconWidth, href);
					rest--;
				}
			}
			_briefing->AddImage(section, "", HACenter, false, rest * iconWidth, iconWidth, "");
			_briefing->AddBreak(section, false);
		}
		else
		{
			// weapon pictures
			const WeaponType *weapon = info.weapons[0];
			RString href = _selectWeapons ? "Slot:A" : "";
			// RString href("Slot:A");
			if (weapon)
			{
				RString img = GetWeaponPicture(weapon);

				RString text = weapon->GetDisplayName();
				_briefing->AddImage(section, img, HACenter, false, weaponWidth, weaponHeight, href, text);
			}
			else
			{
				_briefing->AddImage(section, emptyGun, HACenter, false, weaponWidth, weaponHeight, href, emptySlot);
			}
			weapon = info.weapons[1];
			href = _selectWeapons ? "Slot:B" : "";
			// href = "Slot:B";
			if (weapon)
			{
				RString img = GetWeaponPicture(weapon);
				RString text = weapon->GetDisplayName();
				_briefing->AddImage(section, img, HACenter, false, weaponWidth, weaponHeight, href, text);
			}
			else
			{
				_briefing->AddImage(section, emptySec, HACenter, false, weaponWidth, weaponHeight, href, emptySlot);
			}
			_briefing->AddBreak(section, false);
			// icons
			weapon = info.weapons[0];
			int rest = iconsPerWeapon;
			if (weapon)
			{
				href = GetEquipmentSection(weapon->GetPictureName());
				if (href.GetLength() > 0)
				{
					_briefing->AddImage(section, "i.paa", HACenter, false, iconWidth, iconWidth, href);
					rest--;
				}
				if (canDrop)
				{
					href = Format("DropWeapon:%s", (const char *)weapon->GetName());
					_briefing->AddImage(section, "\\misc\\poloz.paa", HACenter, false, iconWidth, iconWidth, href);
					rest--;
				}
			}
			_briefing->AddImage(section, "", HACenter, false, rest * iconWidth, iconWidth, "");
			weapon = info.weapons[1];
			rest = iconsPerWeapon;
			if (weapon)
			{
				href = GetEquipmentSection(weapon->GetPictureName());
				if (href.GetLength() > 0)
				{
					_briefing->AddImage(section, "i.paa", HACenter, false, iconWidth, iconWidth, href);
					rest--;
				}
				if (canDrop)
				{
					href = Format("DropWeapon:%s", (const char *)weapon->GetName());
					_briefing->AddImage(section, "\\misc\\poloz.paa", HACenter, false, iconWidth, iconWidth, href);
					rest--;
				}
			}
			_briefing->AddImage(section, "", HACenter, false, rest * iconWidth, iconWidth, "");
			_briefing->AddBreak(section, false);
		}
		// _briefing->AddBreak(section, false);
	}

	// items I
	int item = 0;
	int maxItems = GetItemSlotsCount(info.weaponSlots);
	if (item < maxItems)
	{
/*	
		_briefing->AddText(section, LocalizeString(IDS_BRIEF_ITEMS1), HFH3, HALeft, false, false, "");
		_briefing->AddBreak(section, false);
*/
		int oldItem = item;
		// items pictures
		int maxItemsMin = maxItems;
		saturateMin(maxItemsMin,4);
		while (item < maxItemsMin)
		{
			Magazine *magazine = info.magazines[item];
			RString href = "";
			// if (_selectWeapons)
			{
				char buffer[7];
#if _ENABLE_DATADISC
				strcpy(buffer, "Slot:F");
#else
				strcpy(buffer, "Slot:E");
#endif
				buffer[5] += item;
				href = buffer;
			}
			if (magazine)
			{
				int nItems = GetItemSlotsCount(magazine->_type->_magazineType);
				RString img = GetMagazinePicture(magazine->_type);
				RString text;// = magazine->_type->_shortName;
				_briefing->AddImage(section, img, HACenter, false, nItems * itemWidth, itemHeight, href, text);
				item += nItems;
			}
			else
			{
				_briefing->AddImage(section, emptyMag, HACenter, false, itemWidth, itemHeight, href/*, emptySlot*/);
				item++;
			}
		}
		_briefing->AddBreak(section, false);
		item = oldItem;
		// icons
		while (item < maxItemsMin)
		{
			Magazine *magazine = info.magazines[item];
			if (magazine)
			{
				int nItems = GetItemSlotsCount(magazine->_type->_magazineType);
				int rest = nItems * iconsPerItem;
				RString href = GetEquipmentSection(magazine->_type->GetPictureName());
				if (href.GetLength() > 0)
				{
					_briefing->AddImage(section, "i.paa", HACenter, false, iconWidth, iconWidth, href);
					rest--;
				}
				
#if _ENABLE_DATADISC
				if (_selectWeapons)
				{
					href = RString("Fill1:") + magazine->_type->GetName();				
					_briefing->AddImage(section, "\\misc\\hvezdicka.paa", HACenter, false, iconWidth, iconWidth, href);
					rest--;
				}
#endif
				if (canDrop)
				{
					href = Format("DropMagazine:%s", (const char *)magazine->_type->GetName());
					_briefing->AddImage(section, "\\misc\\poloz.paa", HACenter, false, iconWidth, iconWidth, href);
					rest--;
				}

				_briefing->AddImage(section, "", HACenter, false, rest * iconWidth, iconWidth, "");
				item += nItems;
			}
			else
			{
				_briefing->AddImage(section, "", HACenter, false, iconsPerItem * iconWidth, iconWidth, "");
				item++;
			}
		}
		_briefing->AddBreak(section, false);
	}
	// items II
	if (item < maxItems)
	{
/*
		_briefing->AddText(section, LocalizeString(IDS_BRIEF_ITEMS2), HFH3, HALeft, false, false, "");
		_briefing->AddBreak(section, false);
*/
		int oldItem = item;
		// items pictures
		while (item < maxItems)
		{
			Magazine *magazine = info.magazines[item];
			RString href = "";
			// if (_selectWeapons)
			{
				char buffer[7];
#if _ENABLE_DATADISC
				strcpy(buffer, "Slot:F");
#else
				strcpy(buffer, "Slot:E");
#endif
				buffer[5] += item;
				href = buffer;
			}
			if (magazine)
			{
				int nItems = GetItemSlotsCount(magazine->_type->_magazineType);
				RString img = GetMagazinePicture(magazine->_type);
				RString text; // = magazine->_type->_shortName;
				_briefing->AddImage(section, img, HACenter, false, nItems * itemWidth, itemHeight, href, text);
				item += nItems;
			}
			else
			{
				_briefing->AddImage(section, emptyMag2, HACenter, false, itemWidth, itemHeight, href/*, emptySlot*/);
				item++;
			}
		}
		_briefing->AddBreak(section, false);
		item = oldItem;
		// icons
		while (item < maxItems)
		{
			Magazine *magazine = info.magazines[item];
			if (magazine)
			{
				int nItems = GetItemSlotsCount(magazine->_type->_magazineType);
				int rest = nItems * iconsPerItem;
				RString href = GetEquipmentSection(magazine->_type->GetPictureName());
				if (href.GetLength() > 0)
				{
					_briefing->AddImage(section, "i.paa", HACenter, false, iconWidth, iconWidth, href);
					rest--;
				}

#if _ENABLE_DATADISC
				if (_selectWeapons)
				{
					href = RString("Fill2:") + magazine->_type->GetName();				
					_briefing->AddImage(section, "\\misc\\hvezdicka.paa", HACenter, false, iconWidth, iconWidth, href);
					rest--;
				}
#endif
				if (canDrop)
				{
					href = Format("DropMagazine:%s", (const char *)magazine->_type->GetName());
					_briefing->AddImage(section, "\\misc\\poloz.paa", HACenter, false, iconWidth, iconWidth, href);
					rest--;
				}

				_briefing->AddImage(section, "", HACenter, false, rest * iconWidth, iconWidth, "");
				item += nItems;
			}
			else
			{
				_briefing->AddImage(section, "", HACenter, false, iconsPerItem * iconWidth, iconWidth, "");
				item++;
			}
		}
		_briefing->AddBreak(section, false);
	}
	if (maxItems > 0) _briefing->AddBreak(section, false);

#if _ENABLE_DATADISC
	// hand gun with magazines

	if ((info.weaponSlots & MaskSlotHandGun) != 0)
	{
/*
		_briefing->AddText(section, "Hand gun", HFH3, HALeft, false, false, "");
		_briefing->AddBreak(section, false);
*/

		// hand gun picture
		const WeaponType *weapon = info.weapons[4];
		RString href = _selectWeapons ? "Slot:E" : "";
//		RString href = "Slot:E";
		if (weapon)
		{
			RString img = GetWeaponPicture(weapon);
			RString text = weapon->GetDisplayName();
			_briefing->AddImage(section, img, HACenter, false, 2 * itemWidth, itemHeight, href, text);
		}
		else
		{
			_briefing->AddImage(section, emptyHGun, HACenter, false, 2 * itemWidth, itemHeight, href, emptySlot);
		}

		// hand gun magazines pictures
		item = 0;
		maxItems = GetHandGunItemSlotsCount(info.weaponSlots);
		int oldItem = item;
		while (item < maxItems)
		{
			Magazine *magazine = info.magazines[10 + item];
			RString href = "";
//			if (_selectWeapons)
			{
				char buffer[7]; strcpy(buffer, "Slot:F");
				buffer[5] += 10 + item;
				href = buffer;
			}
			if (magazine)
			{
				int nItems = GetHandGunItemSlotsCount(magazine->_type->_magazineType);
				RString img = GetMagazinePicture(magazine->_type);
				RString text; // = magazine->_type->_shortName;
				_briefing->AddImage(section, img, HACenter, false, nItems * itemWidth, itemHeight, href, text);
				item += nItems;
			}
			else
			{
				_briefing->AddImage(section, emptyHGunMag, HACenter, false, itemWidth, itemHeight, href/*, emptySlot*/);
				item++;
			}
		}
		_briefing->AddBreak(section, false);

		// hand gun icons
		int rest = 2 * iconsPerItem;
		if (weapon)
		{
			href = GetEquipmentSection(weapon->GetPictureName());
			if (href.GetLength() > 0)
			{
				_briefing->AddImage(section, "i.paa", HACenter, false, iconWidth, iconWidth, href);
				rest--;
			}
			if (canDrop)
			{
				href = Format("DropWeapon:%s", (const char *)weapon->GetName());
				_briefing->AddImage(section, "\\misc\\poloz.paa", HACenter, false, iconWidth, iconWidth, href);
				rest--;
			}
		}
		_briefing->AddImage(section, "", HACenter, false, rest * iconWidth, iconWidth, "");

		// hand gun magazines icons
		item = oldItem;
		while (item < maxItems)
		{
			Magazine *magazine = info.magazines[10 + item];
			if (magazine)
			{
				int nItems = GetHandGunItemSlotsCount(magazine->_type->_magazineType);
				int rest = nItems * iconsPerItem;
				RString href = GetEquipmentSection(magazine->_type->GetPictureName());
				if (href.GetLength() > 0)
				{
					_briefing->AddImage(section, "i.paa", HACenter, false, iconWidth, iconWidth, href);
					rest--;
				}
				if (_selectWeapons)
				{
					href = RString("Fill3:") + magazine->_type->GetName();				
					_briefing->AddImage(section, "\\misc\\hvezdicka.paa", HACenter, false, iconWidth, iconWidth, href);
					rest--;
				}
				if (canDrop)
				{
					href = Format("DropMagazine:%s", (const char *)magazine->_type->GetName());
					_briefing->AddImage(section, "\\misc\\poloz.paa", HACenter, false, iconWidth, iconWidth, href);
					rest--;
				}
				_briefing->AddImage(section, "", HACenter, false, rest * iconWidth, iconWidth, "");
				item += nItems;
			}
			else
			{
				_briefing->AddImage(section, "", HACenter, false, iconsPerItem * iconWidth, iconWidth, "");
				item++;
			}
		}

		_briefing->AddBreak(section, false);
		_briefing->AddBreak(section, false);
	}
#endif

	// binocular
	if ((info.weaponSlots & MaskSlotBinocular) != 0)
	{
/*
		_briefing->AddText(section, LocalizeString(IDS_BRIEF_SPECIAL), HFH3, HALeft, false, false, "");
		_briefing->AddBreak(section, false);
*/

		// binocular picture
		const WeaponType *weapon = info.weapons[2];
		RString href = _selectWeapons ? "Slot:C" : "";
		// RString href = "Slot:C";
		if (weapon)
		{
			RString img = GetWeaponPicture(weapon);
			RString text = weapon->GetDisplayName();
			_briefing->AddImage(section, img, HACenter, false, weaponWidth, weaponHeight, href, text);
		}
		else
		{
			_briefing->AddImage(section, emptyEq, HACenter, false, weaponWidth, weaponHeight, href, emptySlot);
		}
		weapon = info.weapons[3];
		href = _selectWeapons ? "Slot:D" : "";
		// href = "Slot:D";
		if (weapon)
		{
			RString img = GetWeaponPicture(weapon);
			RString text = weapon->GetDisplayName();
			_briefing->AddImage(section, img, HACenter, false, weaponWidth, weaponHeight, href, text);
		}
		else
		{
			_briefing->AddImage(section, emptyEq, HACenter, false, weaponWidth, weaponHeight, href, emptySlot);
		}
		// _briefing->AddBreak(section, false);
		
		// icons
		weapon = info.weapons[2];
		int rest = iconsPerWeapon;
		if (weapon)
		{
/*
			href = GetEquipmentSection(weapon->GetPictureName());
			_briefing->AddImage(section, "i.paa", HACenter, false, iconWidth, iconWidth, href);
			_briefing->AddImage(section, "", HACenter, false, (iconsPerWeapon - 1) * iconWidth, iconWidth, "");
*/
			if (canDrop)
			{
				href = Format("DropWeapon:%s", (const char *)weapon->GetName());
				_briefing->AddImage(section, "\\misc\\poloz.paa", HACenter, false, iconWidth, iconWidth, href);
				rest--;
			}
		}
		_briefing->AddImage(section, "", HACenter, false, rest * iconWidth, iconWidth, "");
		weapon = info.weapons[3];
		rest = iconsPerWeapon;
		if (weapon)
		{
/*
			href = GetEquipmentSection(weapon->GetPictureName());
			_briefing->AddImage(section, "i.paa", HACenter, false, iconWidth, iconWidth, href);
			_briefing->AddImage(section, "", HACenter, false, (iconsPerWeapon - 1) * iconWidth, iconWidth, "");
*/
			if (canDrop)
			{
				href = Format("DropWeapon:%s", (const char *)weapon->GetName());
				_briefing->AddImage(section, "\\misc\\poloz.paa", HACenter, false, iconWidth, iconWidth, href);
				rest--;
			}
		}
		_briefing->AddImage(section, "", HACenter, false, rest * iconWidth, iconWidth, "");
		_briefing->AddBreak(section, false);
	}

	_briefing->FormatSection(section);
	if (actual) SwitchBriefingSection("Equipment");

#endif // _ENABLE_BRIEF_EQ
}

void DisplayMap::CreateWeaponsPoolPage(int slot)
{
#if _ENABLE_BRIEF_EQ
	// create section "Pool"
	int section = _briefing->FindSection("Pool");
	if (section < 0)
	{
		section = _briefing->AddSection();
	}
	else
	{
		_briefing->InitSection(section);
		int s;
		while ((s = _briefing->FindSection("Pool")) >= 0)
			_briefing->RemoveSection(s);
	}
	_briefing->AddName(section, "Pool");

	RString title;
	RString empty;
	int mask, maskExclude = 0;

	switch (slot)
	{
	case 0:
		title = LocalizeString(IDS_BRIEF_PRIMARY_WEAPONS);
		empty = "Weapon:";
		mask = MaskSlotPrimary;
		break;
	case 1:
		title = LocalizeString(IDS_BRIEF_SECONDARY_WEAPONS);
		empty = "Weapon:";
		mask = MaskSlotSecondary;
		maskExclude = MaskSlotPrimary;
		break;
	case 2:
	case 3:
		title = LocalizeString(IDS_BRIEF_SPECIAL_ITEMS);
		empty = "Weapon:";
		mask = MaskSlotBinocular;
		break;
#if _ENABLE_DATADISC
	case 4:
		title = LocalizeString(IDS_BRIEF_SPECIAL_HANDGUN);
		empty = "Weapon:";
		mask = MaskSlotHandGun;
		break;
	case 15:
	case 16:
	case 17:
	case 18:
		title = LocalizeString(IDS_BRIEF_SPECIAL_HANDGUN_MAGAZINES);
		empty = "Magazine:";
		mask = MaskSlotHandGunItem;
		break;
#endif
	default:
		title = LocalizeString(IDS_BRIEF_SPECIAL_MAGAZINES);
		empty = "Magazine:";
		mask = MaskSlotItem;
		break;
	}
	
	// generate page for weapon selection
	_briefing->AddText(section, title, HFH1, HALeft, false, false, "");
	_briefing->AddBreak(section, false);
	_briefing->AddText(section, LocalizeString(IDS_BRIEF_POOL_CANCEL), HFP, HALeft, false, false, empty + RString("Cancel"));
	_briefing->AddBreak(section, false);
	_briefing->AddText(section, LocalizeString(IDS_BRIEF_POOL_NONE), HFP, HALeft, false, false, empty);
	_briefing->AddBreak(section, false);
	_briefing->AddBreak(section, false);

	if (mask == MaskSlotItem || mask == MaskSlotHandGunItem)
	{
		// magazines
		int n = _weaponsInfo._magazinesPool.Size();
		for (int i=0; i<n; i++)
		{
			Magazine *magazine = _weaponsInfo._magazinesPool[i];
			MagazineType *type = magazine->_type;
			int slots = type->_magazineType;
			if ((slots & mask) == 0) continue;
			if ((slots & maskExclude) != 0) continue;
			int found = 0;
			for (int j=0; j<i; j++)
				if (_weaponsInfo._magazinesPool[j]->_type == type)
				{
					found++; break;
				}
			if (found > 0) continue;
			for (int j=i; j<n; j++)
				if (_weaponsInfo._magazinesPool[j]->_type == type)
				{
					found++;
				}
#if _ENABLE_DATADISC
			char text[512];
			sprintf(text, LocalizeString(IDS_NUM_MAGAZINES), (const char *)type->GetDisplayName(), found);
#else
			RString text = type->GetDisplayName();
#endif
			RString href = empty + type->GetName();
			_briefing->AddText(section, text, HFP, HALeft, false, false, href);
			_briefing->AddBreak(section, false);
		}
	}
	else
	{
		int n = _weaponsInfo._weaponsPool.Size();
		for (int i=0; i<n; i++)
		{
			WeaponType *weapon = _weaponsInfo._weaponsPool[i];
			int slots = weapon->_weaponType;
			if ((slots & mask) == 0) continue;
			if ((slots & maskExclude) != 0) continue;
			int found = 0;
			for (int j=0; j<i; j++)
				if (_weaponsInfo._weaponsPool[j] == weapon)
				{
					found++; break;
				}
			if (found > 0) continue;
			for (int j=i; j<n; j++)
				if (_weaponsInfo._weaponsPool[j] == weapon)
				{
					found++;
				}
#if _ENABLE_DATADISC
			char text[512];
			sprintf(text, LocalizeString(IDS_NUM_MAGAZINES), (const char *)weapon->GetDisplayName(), found);
#else
			RString text = weapon->GetDisplayName();
#endif
			RString href = empty + weapon->GetName();
			_briefing->AddText(section, text, HFP, HALeft, false, false, href);
			_briefing->AddBreak(section, false);
		}
	}
	_briefing->FormatSection(section);
#endif // _ENABLE_BRIEF_EQ
}

static RString GetSkill(float skill)
{
	RString text(" (");
	if (skill <= 0.25)
		text = text + LocalizeString(IDS_SKILL_NOVICE);
	else if (skill <= 0.45)
		text = text + LocalizeString(IDS_SKILL_ROOKIE);
	else if (skill <= 0.65)
		text = text + LocalizeString(IDS_SKILL_RECRUIT);
	else if (skill <= 0.85)
		text = text + LocalizeString(IDS_SKILL_VETERAN);
	else
		text = text + LocalizeString(IDS_SKILL_EXPERT);
	return text + RString(")");
}

/*!
\patch 1.75 Date 2/11/2002 by Jirka
- Improved: Group page in briefing 
*/
void DisplayMap::UpdateUnitsInBriefing()
{
#if _ENABLE_BRIEF_GRP
	int section = _briefing->FindSection("Group");
	if (section < 0)
	{
		section = _briefing->AddSection();
	}
	else
	{
		_briefing->InitSection(section);
	}

	// list of weapons
	_briefing->AddName(section, "Group");
	_briefing->AddText(section, LocalizeString(IDS_BRIEF_GROUP), HFH1, HALeft, false, false, "");
	_briefing->AddBreak(section, false);

	Person *veh = GWorld->PlayerOn();
	if (!veh) return;
	AIUnit *unit = veh->Brain();
	if (!unit) return;
	AIGroup *grp = unit->GetGroup();
	if (!grp) return;

	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = grp->UnitWithID(i + 1);
		if (!unit || !unit->IsUnit()) continue;
		if (unit->IsFreeSoldier())
		{
			char id[8]; sprintf(id, "%d: ", unit->ID());
			RString text = RString(id) +
				LocalizeString(IDS_SHORT_PRIVATE + unit->GetPerson()->GetRank()) +
				RString(". ") +
				unit->GetPerson()->GetInfo()._name;
			if (unit->IsGroupLeader())
				text = text + LocalizeString(IDS_BRIEF_GROUP_LEADER);
			text = text + GetSkill(unit->GetAbility());
#if _ENABLE_DATADISC
			char href[32]; sprintf(href, "gear:%d", unit->ID());
#else
			RString href = "";
#endif
			_briefing->AddText(section, text, HFP, HALeft, false, false, href);
			_briefing->AddBreak(section, false);
		}
		else
		{
			Transport *veh = unit->GetVehicleIn();
			Assert(veh);
			RString text = veh->GetType()->GetDisplayName();
			RString href = "";
			_briefing->AddText(section, text, HFP, HALeft, false, false, href);
			_briefing->AddBreak(section, false);
			AIUnit *u = veh->CommanderBrain();
			if (u)
			{
				RString text = LocalizeString(IDS_BRIEF_COMMANDER);
				text = text + LocalizeString(IDS_PRIVATE + u->GetPerson()->GetRank());
				text = text + RString(" ");
				text = text + u->GetPerson()->GetInfo()._name;
				if (u->IsGroupLeader())
					text = text + LocalizeString(IDS_BRIEF_GROUP_LEADER);
				text = text + GetSkill(u->GetAbility());
#if _ENABLE_DATADISC
				char href[32]; sprintf(href, "gear:%d", u->ID());
#else
				RString href = "";
#endif
				_briefing->AddText(section, text, HFP, HALeft, false, false, href);
				_briefing->AddBreak(section, false);
			}
			u = veh->DriverBrain();
			if (u)
			{
				RString text = LocalizeString(IDS_BRIEF_DRIVER);
				text = text + LocalizeString(IDS_PRIVATE + u->GetPerson()->GetRank());
				text = text + RString(" ");
				text = text + u->GetPerson()->GetInfo()._name;
				if (u->IsGroupLeader())
					text = text + LocalizeString(IDS_BRIEF_GROUP_LEADER);
				text = text + GetSkill(u->GetAbility());
#if _ENABLE_DATADISC
				char href[32]; sprintf(href, "gear:%d", u->ID());
#else
				RString href = "";
#endif
				_briefing->AddText(section, text, HFP, HALeft, false, false, href);
				_briefing->AddBreak(section, false);
			}
			u = veh->GunnerBrain();
			if (u)
			{
				RString text = LocalizeString(IDS_BRIEF_GUNNER);
				text = text + LocalizeString(IDS_PRIVATE + u->GetPerson()->GetRank());
				text = text + RString(" ");
				text = text + u->GetPerson()->GetInfo()._name;
				if (u->IsGroupLeader())
					text = text + LocalizeString(IDS_BRIEF_GROUP_LEADER);
				text = text + GetSkill(u->GetAbility());
#if _ENABLE_DATADISC
				char href[32]; sprintf(href, "gear:%d", u->ID());
#else
				RString href = "";
#endif
				_briefing->AddText(section, text, HFP, HALeft, false, false, href);
				_briefing->AddBreak(section, false);
			}
			for (int j=0; j<veh->GetManCargo().Size(); j++)
			{
				Person *soldier = veh->GetManCargo()[j];
				if (!soldier) continue;
				u = soldier->Brain();
				if (!u) continue;
				RString text = LocalizeString(IDS_BRIEF_CARGO);
				text = text + LocalizeString(IDS_PRIVATE + u->GetPerson()->GetRank());
				text = text + RString(" ");
				text = text + u->GetPerson()->GetInfo()._name;
				if (u->IsGroupLeader())
					text = text + LocalizeString(IDS_BRIEF_GROUP_LEADER);
				text = text + GetSkill(u->GetAbility());
#if _ENABLE_DATADISC
				char href[32]; sprintf(href, "gear:%d", u->ID());
#else
				RString href = "";
#endif
				_briefing->AddText(section, text, HFP, HALeft, false, false, href);
				_briefing->AddBreak(section, false);
			}
		}
	}
	_briefing->FormatSection(section);

#endif // _ENABLE_BRIEF_EQ
}

/*
void DisplayMap::UpdateDebriefing()
{
	int section = _briefing->FindSection("Debriefing:Log");
	while (section >= 0)
	{
		_briefing->RemoveSection(section);
		section = _briefing->FindSection("Debriefing:Log");
	}
	section = _briefing->AddSection();

	// list of weapons
	_briefing->AddName(section, "Debriefing:Log");
	_briefing->AddText(section, "Mission Log", HFH1, HALeft, false, false, "");
	_briefing->AddBreak(section, false);

	AutoArray<AIStatsEvent> &events = GStats._mission._events;
	for (int i=0; i<events.Size(); i++)
	{
		char buffer[256]; sprintf(buffer, "DBLOG_%d", i);
		// Add marker
		bool found = false;
		for (int i=0; i<markersMap.Size(); i++)
		{
			ArcadeMarkerInfo &mInfo = markersMap[i];
			if (stricmp(buffer, mInfo.name) == 0)
			{
				found = true;
				break;
			}
		}
		if (!found)
		{
			int index = markersMap.Add();
			ArcadeMarkerInfo &mInfo = markersMap[index];
			mInfo.position = events[i].position;
			mInfo.name = buffer;
			mInfo.type = "Empty";
			mInfo.angle = 0;
			mInfo.color = PackedColor(Color(0,0,0,0));
			mInfo.icon = NULL;
			mInfo.size = 0;
		}

		RString name = RString("marker:") + RString(buffer);
		_briefing->AddText(section, events[i].message, HFP, HALeft, false, false, name);
		_briefing->AddBreak(section, false);
	}
	_briefing->FormatSection(section);
}
*/

static void AddObjective(CHTMLContainer *html, int src, int dst, int value)
{
	if (value == OSHidden) return;

	RString picture;
	switch (value)
	{
	case OSDone:
		picture = "mission_done.paa";
		break;
	case OSFailed:
		picture = "mission_uncomplete.paa";
		break;
	default:
		Fail("Unknown objective type");
	case OSActive:
		picture = "mission_dot.paa";
		break;
	}
	html->AddBreak(dst, false);
	float imgHeight = 640.0f * 1.5 * html->GetPHeight();
	HTMLField *fld = html->AddImage(dst, picture, HALeft, false, -1, imgHeight, "");
	fld->exclude = true;
	float indent = 1.2 * fld->width;
	html->SetIndent(indent);
	html->CopySection(src, dst);
	html->SetIndent(0);
}

void DisplayMap::UpdatePlan()
{
	// init section
	bool actual = false;
	int section = _briefing->FindSection("__PLAN");
	if (section < 0)
	{
		section = _briefing->AddSection();
	}
	else
	{
		_briefing->InitSection(section);
		if (_briefing->CurrentSection() == section) actual = true;
		int s;
		while ((s = _briefing->FindSection("__PLAN")) >= 0)
		{
			if (_briefing->CurrentSection() == s) actual = true;
			_briefing->RemoveSection(s);
		}
	}
	_briefing->AddName(section, "__PLAN");

	// plan header
	AIUnit *unit = GWorld->FocusOn();
	int source = FindSectionForUnit(_briefing, "Plan", unit);
	if (source >= 0) _briefing->CopySection(source, section);

	// objectives
	TargetSide side = TSideUnknown;
	if (unit)
	{
		AIGroup *grp = unit->GetGroup();
		if (grp)
		{
			AICenter *center = grp->GetCenter();
			if (center) side = center->GetSide();
		}
	}
	for (int s=0; s<_briefing->NSections(); s++)
	{
		const HTMLSection &src = _briefing->GetSection(s);
		for (int n=0; n<src.names.Size(); n++)
		{
			RString name = src.names[n];
			static const char *prefix = "OBJ_";
			static const char *prefixWest = "OBJ_WEST_";
			static const char *prefixEast = "OBJ_EAST_";
			static const char *prefixGuerrila = "OBJ_GUER_";
			static const char *prefixCivilian = "OBJ_CIVIL_";
			if (strnicmp(name, prefix, strlen(prefix)) == 0)
			{
				if (strnicmp(name, prefixWest, strlen(prefixWest)) == 0)
				{
					if (side != TWest) continue;
				}
				else if (strnicmp(name, prefixEast, strlen(prefixEast)) == 0)
				{
					if (side != TEast) continue;
				}
				if (strnicmp(name, prefixGuerrila, strlen(prefixGuerrila)) == 0)
				{
					if (side != TGuerrila) continue;
				}
				if (strnicmp(name, prefixCivilian, strlen(prefixCivilian)) == 0)
				{
					if (side != TCivilian) continue;
				}
				GameState *state = GWorld->GetGameState();
				int value = toInt((float)state->Evaluate(name));
				AddObjective(_briefing, s, section, value);
				break; // next section
			}
		}
	}

	_briefing->FormatSection(section);
	if (actual) SwitchBriefingSection("__PLAN");
}

DrawCoord SceneToScreen(Vector3Par pos)
{
	AspectSettings as;
	GEngine->GetAspectSettings(as);
	float invz = CameraZoom / pos.Z();
	float x3d = 1/as.leftFOV * pos.X() * invz + 0.5;
	float y3d = 0.5 - 1/as.topFOV * pos.Y() * invz;
	DrawCoord pt;
	pt.x = (x3d-as.uiTopLeftX)/(as.uiBottomRightX-as.uiTopLeftX);
	pt.y = (y3d-as.uiTopLeftY)/(as.uiBottomRightY-as.uiTopLeftY);
	return pt;
}

void DisplayMap::SetRadioText()
{
	_radioAlpha->SetText(LocalizeString(IDS_RADIO_UNASSIGNED));
	_radioBravo->SetText(LocalizeString(IDS_RADIO_UNASSIGNED));
	_radioCharlie->SetText(LocalizeString(IDS_RADIO_UNASSIGNED));
	_radioDelta->SetText(LocalizeString(IDS_RADIO_UNASSIGNED));
	_radioEcho->SetText(LocalizeString(IDS_RADIO_UNASSIGNED));
	_radioFoxtrot->SetText(LocalizeString(IDS_RADIO_UNASSIGNED));
	_radioGolf->SetText(LocalizeString(IDS_RADIO_UNASSIGNED));
	_radioHotel->SetText(LocalizeString(IDS_RADIO_UNASSIGNED));
	_radioIndia->SetText(LocalizeString(IDS_RADIO_UNASSIGNED));
	_radioJuliet->SetText(LocalizeString(IDS_RADIO_UNASSIGNED));

	Person *player = GWorld->GetRealPlayer();
	if (!player || player->IsDammageDestroyed()) return;

	for (int i=sensorsMap.Size()-1; i>=0; i--)
	{
		Vehicle *veh = sensorsMap[i];
		if (!veh) continue;
		Detector *det = dyn_cast<Detector>(veh);
		Assert(det);
		if (det->IsActive() && !det->IsRepeating()) continue;
		CActiveText *ctrl;
		int ids;
		switch (det->GetActivationBy())
		{
		case ASAAlpha:
			ctrl = _radioAlpha; ids = IDS_RADIO_ALPHA;
			goto showRadio;
		case ASABravo:
			ctrl = _radioBravo; ids = IDS_RADIO_BRAVO;
			goto showRadio;
		case ASACharlie:
			ctrl = _radioCharlie; ids = IDS_RADIO_CHARLIE;
			goto showRadio;
		case ASADelta:
			ctrl = _radioDelta; ids = IDS_RADIO_DELTA;
			goto showRadio;
		case ASAEcho:
			ctrl = _radioEcho; ids = IDS_RADIO_ECHO;
			goto showRadio;
		case ASAFoxtrot:
			ctrl = _radioFoxtrot; ids = IDS_RADIO_FOXTROT;
			goto showRadio;
		case ASAGolf:
			ctrl = _radioGolf; ids = IDS_RADIO_GOLF;
			goto showRadio;
		case ASAHotel:
			ctrl = _radioHotel; ids = IDS_RADIO_HOTEL;
			goto showRadio;
		case ASAIndia:
			ctrl = _radioIndia; ids = IDS_RADIO_INDIA;
			goto showRadio;
		case ASAJuliet:
			ctrl = _radioJuliet; ids = IDS_RADIO_JULIET;
			goto showRadio;
		showRadio:
			{
				RString text = det->GetText();
				if (stricmp(text, "null") == 0) continue;
				text = Localize(text);
				if (text.GetLength() > 0)
					ctrl->SetText(text);
				else
					ctrl->SetText(LocalizeString(ids));
			}
		}
	}
}

LSError DisplayMap::SerializeParams(ParamArchive &ar)
{
	CHECK(ar.Serialize("Compass", *(SerializeClass *)_compass, 1))	
	CHECK(ar.Serialize("Watch", *(SerializeClass *)_watch, 1))	
	CHECK(ar.Serialize("WalkieTalkie", *(SerializeClass *)_walkieTalkie, 1))	
	CHECK(ar.Serialize("Notepad", *(SerializeClass *)_notepad, 1))	
	CHECK(ar.Serialize("Warrant", *(SerializeClass *)_warrant, 1))	
	CHECK(ar.Serialize("GPS", *(SerializeClass *)_gps, 1))	
	return LSOK;
}

void DisplayMap::LoadParams()
{
	ParamArchiveLoad ar(GetUserParams());
	ParamArchive arSubcls;
	if (!ar.OpenSubclass("MainMap", arSubcls))
	{
		// TODO: load failed
		return;
	}
	if (SerializeParams(arSubcls) != LSOK)
	{
		// TODO: load failed
		return;
	}
}

void DisplayMap::SaveParams()
{
	ParamArchiveSave ar(UserInfoVersion);
	ar.Parse(GetUserParams());

	ParamArchive arSubcls;
	if (!ar.OpenSubclass("MainMap", arSubcls))
	{
		// TODO: save failed
		return;
	}
	if (SerializeParams(arSubcls) != LSOK)
	{
		// TODO: save failed
		return;
	}
	if (ar.Save(GetUserParams()) != LSOK)
	{
		// TODO: save failed
		return;
	}
}

LSError DisplayMap::Serialize(ParamArchive &ar)
{
	if (_map) CHECK(_map->Serialize(ar))
	return LSOK;
}

Control *DisplayMap::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
	case IDC_RADIO_ALPHA:
		_radioAlpha = new CActiveText(this, idc, cls);
		return _radioAlpha;
	case IDC_RADIO_BRAVO:
		_radioBravo = new CActiveText(this, idc, cls);
		return _radioBravo;
	case IDC_RADIO_CHARLIE:
		_radioCharlie = new CActiveText(this, idc, cls);
		return _radioCharlie;
	case IDC_RADIO_DELTA:
		_radioDelta = new CActiveText(this, idc, cls);
		return _radioDelta;
	case IDC_RADIO_ECHO:
		_radioEcho = new CActiveText(this, idc, cls);
		return _radioEcho;
	case IDC_RADIO_FOXTROT:
		_radioFoxtrot = new CActiveText(this, idc, cls);
		return _radioFoxtrot;
	case IDC_RADIO_GOLF:
		_radioGolf = new CActiveText(this, idc, cls);
		return _radioGolf;
	case IDC_RADIO_HOTEL:
		_radioHotel = new CActiveText(this, idc, cls);
		return _radioHotel;
	case IDC_RADIO_INDIA:
		_radioIndia = new CActiveText(this, idc, cls);
		return _radioIndia;
	case IDC_RADIO_JULIET:
		_radioJuliet = new CActiveText(this, idc, cls);
		return _radioJuliet;
	case IDC_MAP_NAME:
		_name = new CStatic(this, idc, cls);
		{
			RString name = Localize(CurrentTemplate.intel.briefingName);
			if (name.GetLength() == 0)
			{
				name = Glob.header.filename;
			}
			_name->SetText(name);
		}
		return _name;
	case IDC_BRIEFING:
		_briefing = new CHTML(this, idc, cls);
		return _briefing;
	case IDC_MAP_NOTES:
		_bookmark1 = new CActiveText(this, idc, cls);
		return _bookmark1;
	case IDC_MAP_PLAN:
		_bookmark2 = new CActiveText(this, idc, cls);
		return _bookmark2;
	case IDC_MAP_GEAR:
		_bookmark3 = new CActiveText(this, idc, cls);
		return _bookmark3;
	case IDC_MAP_GROUP:
		_bookmark4 = new CActiveText(this, idc, cls);
		return _bookmark4;
	case IDC_MAP:
		_map = new CStaticMapMain(this, idc, cls);
		return _map;
	case IDC_WARRANT:
		return new CWarrant(this, idc, cls);
	case IDC_GPS:
		_gpsCtrl = new CStatic(this, idc, cls);
		return _gpsCtrl;
	case IDC_GETREADY_TITLE:
		{
			CStatic *text = new CStatic(this, idc, cls);
			AIUnit *unit = GWorld->FocusOn();
			AIGroup *grp = unit ? unit->GetGroup() : NULL;
			if (grp)
			{
				char buffer[256];
				sprintf
				(
					buffer, LocalizeString(IDS_BRIEFING),
					(const char *)unit->GetPerson()->GetInfo()._name,
					(const char *)grp->GetName(),
					unit->ID()
				);
				text->SetText(buffer);
			}
			else text->SetText("");
			return text;
		}
	default:
		return Display::OnCreateCtrl(type, idc, cls);
	}
}

void ActivateSensor(ArcadeSensorActivation activ)
{
	Person *player = GWorld->GetRealPlayer();
	if (!player || player->IsDammageDestroyed()) return;

	for (int i=0; i<sensorsMap.Size(); i++)
	{
		Vehicle *veh = sensorsMap[i];
		if (!veh) continue;
		Detector *det = dyn_cast<Detector>(veh);
		Assert(det);
		if (det->GetActivationBy() == activ)
		{
			if (stricmp(det->GetText(), "null") == 0) continue;
			if (!det->IsActive() || det->IsRepeating())
			{
				det->DoActivate();
				GetNetworkManager().DetectorActivation(det, true);
			}
		}
	}
}

void DisplayMap::OnButtonClicked(int idc)
{
	switch (idc)
	{
	case IDC_RADIO_ALPHA:
		ActivateSensor(ASAAlpha);
		break;
	case IDC_RADIO_BRAVO:
		ActivateSensor(ASABravo);
		break;
	case IDC_RADIO_CHARLIE:
		ActivateSensor(ASACharlie);
		break;
	case IDC_RADIO_DELTA:
		ActivateSensor(ASADelta);
		break;
	case IDC_RADIO_ECHO:
		ActivateSensor(ASAEcho);
		break;
	case IDC_RADIO_FOXTROT:
		ActivateSensor(ASAFoxtrot);
		break;
	case IDC_RADIO_GOLF:
		ActivateSensor(ASAGolf);
		break;
	case IDC_RADIO_HOTEL:
		ActivateSensor(ASAHotel);
		break;
	case IDC_RADIO_INDIA:
		ActivateSensor(ASAIndia);
		break;
	case IDC_RADIO_JULIET:
		ActivateSensor(ASAJuliet);
		break;
	case IDC_MAP_NOTES:
/*
		if (_debriefing)
			SwitchBriefingSection("Debriefing:Notes");
		else
*/
		SwitchBriefingSection("__BRIEFING");
		break;
	case IDC_MAP_PLAN:
/*
		if (_debriefing)
			SwitchBriefingSection("Debriefing:Log");
		else
*/
		SwitchBriefingSection("__PLAN");
		break;
#if _ENABLE_BRIEF_EQ
	case IDC_MAP_GEAR:
//		if (!_debriefing)
		SwitchBriefingSection("Equipment");
		break;
#endif
#if _ENABLE_BRIEF_GRP
	case IDC_MAP_GROUP:
//		if (!_debriefing)
		SwitchBriefingSection("Group");
		break;
#endif
/*
	case IDC_MAP_BRIEFING:
		if (_debriefing) SwitchDebriefing(false);
		break;
	case IDC_MAP_DEBRIEFING:
		// debriefing disabled
		//		if (!_debriefing) SwitchDebriefing(true);
		break;
*/
	case IDC_CANCEL:
		break;
	}
}

void DisplayMap::RemoveUnusableMagazines(UnitWeaponsInfo &info)
{
	for (int i=0; i<MAGAZINE_SLOTS;)
	{
		Magazine *magazine = info.magazines[i];
		if (!magazine) {i++; continue;}
		MagazineType *type = magazine->_type;
		if ((type->_magazineType & MaskSlotItem) != 0)
		{
			int nItems = GetItemSlotsCount(type->_magazineType);
			if (!info.IsMagazineUsable(type))
			{
				_weaponsInfo._magazinesPool.Add(magazine);
				info.RemoveMagazine(magazine);
			}
			i += nItems;
		}
		else if ((type->_magazineType & MaskSlotHandGunItem) != 0)
		{
			int nItems = GetHandGunItemSlotsCount(type->_magazineType);
			if (!info.IsMagazineUsable(type))
			{
				_weaponsInfo._magazinesPool.Add(magazine);
				info.RemoveMagazine(magazine);
			}
			i += nItems;
		}
	}
}

void DisplayMap::AddUsableMagazines(UnitWeaponsInfo &info, const WeaponType *weapon, int from, int to)
{
	saturateMin(to, GetItemSlotsCount(info.weaponSlots));
	if (from >= to) return;

	if (weapon->_muzzles.Size() == 0) return;
	const MuzzleType *muzzle = weapon->_muzzles[0];

	if (muzzle->_magazines.Size() == 0) return;
	const MagazineType *type = muzzle->_magazines[0];

	int nSlots = GetItemSlotsCount(type->_magazineType);
	for (int i=from; i<=to-nSlots;)
	{
		bool free = true;
		for (int j=0; j<nSlots; j++)
			if (info.magazines[i + j])
			{
				free = false;
				break;
			}
		if (free)
		{
			// find magazine
			Ref<Magazine> magazine;
			for (int k=0; k<_weaponsInfo._magazinesPool.Size(); k++)
			{
				if (_weaponsInfo._magazinesPool[k]->_type == type)
				{
					magazine = _weaponsInfo._magazinesPool[k];
					_weaponsInfo._magazinesPool.Delete(k);
					break;
				}
			}
			if (!magazine) return;
			for (int j=0; j<nSlots; j++)
				info.magazines[i++] = magazine;
		}
		else i++;
	}
}

void DisplayMap::AddUsableHandGunMagazines(UnitWeaponsInfo &info, const WeaponType *weapon, int from, int to)
{
	saturateMin(to, 10 + GetHandGunItemSlotsCount(info.weaponSlots));
	if (from >= to) return;

	if (weapon->_muzzles.Size() == 0) return;
	const MuzzleType *muzzle = weapon->_muzzles[0];

	if (muzzle->_magazines.Size() == 0) return;
	const MagazineType *type = muzzle->_magazines[0];

	int nSlots = GetHandGunItemSlotsCount(type->_magazineType);
	for (int i=from; i<=to-nSlots;)
	{
		bool free = true;
		for (int j=0; j<nSlots; j++)
			if (info.magazines[i + j])
			{
				free = false;
				break;
			}
		if (free)
		{
			// find magazine
			Ref<Magazine> magazine;
			for (int k=0; k<_weaponsInfo._magazinesPool.Size(); k++)
			{
				if (_weaponsInfo._magazinesPool[k]->_type == type)
				{
					magazine = _weaponsInfo._magazinesPool[k];
					_weaponsInfo._magazinesPool.Delete(k);
					break;
				}
			}
			if (!magazine) return;
			for (int j=0; j<nSlots; j++)
				info.magazines[i++] = magazine;
		}
		else i++;
	}
}

/*!
\patch 1.78 Date 7/10/2002 by Jirka
- Fixed: In briefing weapon disappear if trying to assign where doesn't fit
\patch 1.8 Date 9/16/2002 by Ondra
- Fixed: When some units are dead, links in notebook Group section may
point to wrong units.
*/

void DisplayMap::OnHTMLLink(int idc, RString link)
{
	const char *ptr = link;
	const char *name = "marker:";
	int n = strlen(name);
	if (strnicmp(ptr, name, n) == 0)
	{
		ptr += n;
		for (int i=0; i<markersMap.Size(); i++)
		{
			ArcadeMarkerInfo &mInfo = markersMap[i];
			if (stricmp(ptr, mInfo.name) == 0)
			{
				float invSizeLand = 1.0 / (LandGrid * LandRange);
				Vector3 curPos = _map->GetCenter();
				float curScale = _map->GetScale();
				float endScale = curScale;
				float diff = curPos.Distance(mInfo.position);
				float scale = 2.0 * diff * invSizeLand;
				int sizeAnim = _map->_animation.Size();
				if (sizeAnim > 0)
				{
					// animation interrupted, save end scale
					MapAnimationPhase &phase = _map->_animation[sizeAnim - 1];
					endScale = phase.scale;
				}
				_map->ClearAnimation();
				if (scale > curScale)
				{
					float time = log(scale / curScale);
					_map->AddAnimationPhase(time, scale, curPos);
					_map->AddAnimationPhase(1.0, scale, mInfo.position);
				}
				else
				{
					float time = diff * invSizeLand / curScale;
					_map->AddAnimationPhase(time, curScale, mInfo.position);
					scale = curScale;
				}
				if (scale > endScale)
				{
					float time = log(scale / endScale);
					_map->AddAnimationPhase(time, endScale, mInfo.position);
				}
				_map->CreateInterpolator();

				_map->SetActiveMarker(i);
				return;
			}
		}
		return;
	}
	
	if (_selectWeapons)
	{
		name = "slot:";
		n = strlen(name);
		if (strnicmp(ptr, name, n) == 0)
		{
			ptr += n;
			_currentSlot = *ptr - 'A';
			CreateWeaponsPoolPage(_currentSlot);
			SwitchBriefingSection("Pool");
		}
	}

	name = "gear:prev";
	if (stricmp(ptr, name) == 0)
	{
		_currentUnit--;
		CreateWeaponsPage();
		SwitchBriefingSection("Equipment");
		return;
	}

	name = "gear:next";
	if (stricmp(ptr, name) == 0)
	{
		_currentUnit++;
		CreateWeaponsPage();
		SwitchBriefingSection("Equipment");
		return;
	}

	name = "gear:";
	n = strlen(name);
	if (strnicmp(ptr, name, n) == 0)
	{
		ptr += n;
		int newUnitID = atoi(ptr);
		int index = -1;
		for (int i=0; i<_weaponsInfo._weapons.Size(); i++)
		{
			const UnitWeaponsInfo &info = _weaponsInfo._weapons[i];
			if (info.unit && info.unit->ID()==newUnitID) index = i;
		}
		if (index>=0)
		{
			_currentUnit = index;
			CreateWeaponsPage();
			SwitchBriefingSection("Equipment");
		}
		return;
	}

	name = "weapon:";
	n = strlen(name);
	if (strnicmp(ptr, name, n) == 0)
	{
		ptr += n;
		if (stricmp(ptr, "cancel") != 0)
		{
			UnitWeaponsInfo &info = _weaponsInfo._weapons[_currentUnit];
			WeaponType *weapon = info.weapons[_currentSlot];
			if (weapon)
			{
				_weaponsInfo._weaponsPool.Add(weapon);
				info.RemoveWeapon(weapon);
				RemoveUnusableMagazines(info);
			}

			if (*ptr)
			{
				// add new weapon
				Ref<WeaponType> weapon;
				for (int i=0; i<_weaponsInfo._weaponsPool.Size(); i++)
				{
					if (stricmp(ptr, _weaponsInfo._weaponsPool[i]->GetName()) == 0)
					{
						weapon = _weaponsInfo._weaponsPool[i];
						_weaponsInfo._weaponsPool.Delete(i);
						break;
					}
				}
				Assert(weapon);
				if (weapon->_weaponType & MaskSlotBinocular)
				{
					if (info.weaponSlots & MaskSlotBinocular)
					{
/*
						if (info.weapons[_currentSlot])
						{
							_weaponsInfo._weaponsPool.Add(info.weapons[_currentSlot]);
							info.RemoveWeapon(info.weapons[_currentSlot]);
						}
*/
						Assert(!info.weapons[_currentSlot]);
						info.weapons[_currentSlot] = weapon;
					}
					else
					{
						// FIX: Return to pool
						_weaponsInfo._weaponsPool.Add(weapon);
					}
				}
				else if ((weapon->_weaponType & info.weaponSlots) == weapon->_weaponType)
				{
					if (weapon->_weaponType & MaskSlotPrimary)
					{
						if (info.weapons[0])
						{
							_weaponsInfo._weaponsPool.Add(info.weapons[0]);
							info.RemoveWeapon(info.weapons[0]);
							RemoveUnusableMagazines(info);
						}
						info.weapons[0] = weapon;
						AddUsableMagazines(info, weapon, 0, 4);
					}
					if (weapon->_weaponType & MaskSlotSecondary)
					{
						if (info.weapons[1])
						{
							_weaponsInfo._weaponsPool.Add(info.weapons[1]);
							info.RemoveWeapon(info.weapons[1]);
							RemoveUnusableMagazines(info);
						}
						info.weapons[1] = weapon;
						AddUsableMagazines(info, weapon, 4, 10);
					}
					if (weapon->_weaponType & MaskSlotHandGun)
					{
						if (info.weapons[4])
						{
							_weaponsInfo._weaponsPool.Add(info.weapons[4]);
							info.RemoveWeapon(info.weapons[4]);
							RemoveUnusableMagazines(info);
						}
						info.weapons[4] = weapon;
						AddUsableHandGunMagazines(info, weapon, 10, MAGAZINE_SLOTS);
					}
				}
				else
				{
					// does not fit into slots
					// FIX: Return to pool
					_weaponsInfo._weaponsPool.Add(weapon);
				}
			}
			CreateWeaponsPage();
		}
		SwitchBriefingSection("Equipment");
		return;
	}

	name = "magazine:";
	n = strlen(name);
	if (strnicmp(ptr, name, n) == 0)
	{
		ptr += n;
		if (stricmp(ptr, "cancel") != 0)
		{
			UnitWeaponsInfo &info = _weaponsInfo._weapons[_currentUnit];
			int slot = _currentSlot - WEAPON_SLOTS;
			Magazine *magazine = info.magazines[slot];
			if (magazine)
			{
				_weaponsInfo._magazinesPool.Add(magazine);
				info.RemoveMagazine(magazine);
			}

			if (*ptr)
			{
				MagazineType *type = MagazineTypes.New(ptr);
				bool handGun = (type->_magazineType & MaskSlotHandGunItem) != 0;

				int maxItems = handGun ? GetItemSlotsCount(info.weaponSlots) + GetHandGunItemSlotsCount(info.weaponSlots) : GetItemSlotsCount(info.weaponSlots);

				Ref<Magazine> magazine;

				int slots = handGun ? GetHandGunItemSlotsCount(type->_magazineType) : GetItemSlotsCount(type->_magazineType);
				if (type && slots <= maxItems) magazine = _weaponsInfo.RemovePoolMagazine(type);

				if (magazine)
				{
					saturateMin(slot, maxItems - slots);
					for (int i=slot; i<slot+slots; i++)
					{
						if (info.magazines[i])
						{
							_weaponsInfo._magazinesPool.Add(info.magazines[i]);
							info.RemoveMagazine(info.magazines[i]);
						}
						info.magazines[i] = magazine;
					}
				}
			}
			CreateWeaponsPage();
		}
		SwitchBriefingSection("Equipment");
		return;
	}

	name = "fill1:";
	n = strlen(name);
	if (strnicmp(ptr, name, n) == 0)
	{
		ptr += n;
		MagazineType *type = MagazineTypes.New(ptr);
		if (!type) return;

		UnitWeaponsInfo &info = _weaponsInfo._weapons[_currentUnit];
		int maxItems = GetItemSlotsCount(info.weaponSlots);
		int iCnt = maxItems;
		saturateMin(iCnt,4);
		
		int slots = GetItemSlotsCount(type->_magazineType);
		if (iCnt <= slots) return;

		// remove all magazines from items 1
		int iMin = 0, iMax = iMin + iCnt;
		for (int i=iMin; i<iMax; i++)
		{
			Magazine *magazine = info.magazines[i];
			if (magazine)
			{
				_weaponsInfo._magazinesPool.Add(magazine);
				info.RemoveMagazine(magazine);
			}
		}

		// add magazines
		for (int i=iMin; i<iMax-slots+1; i+=slots)
		{
			Ref<Magazine> magazine = _weaponsInfo.RemovePoolMagazine(type);
			if (!magazine) break;
			for (int j=i; j<i+slots; j++) info.magazines[j] = magazine;
		}
		CreateWeaponsPage();
		return;
	}

	name = "fill2:";
	n = strlen(name);
	if (strnicmp(ptr, name, n) == 0)
	{
		ptr += n;
		MagazineType *type = MagazineTypes.New(ptr);
		if (!type) return;

		UnitWeaponsInfo &info = _weaponsInfo._weapons[_currentUnit];
		int maxItems = GetItemSlotsCount(info.weaponSlots);
		int iCnt = maxItems - 4;
		
		int slots = GetItemSlotsCount(type->_magazineType);
		if (iCnt <= slots) return;

		// remove all magazines from items 1
		int iMin = 4, iMax = iMin + iCnt;
		for (int i=iMin; i<iMax; i++)
		{
			Magazine *magazine = info.magazines[i];
			if (magazine)
			{
				_weaponsInfo._magazinesPool.Add(magazine);
				info.RemoveMagazine(magazine);
			}
		}

		// add magazines
		for (int i=iMin; i<iMax-slots+1; i+=slots)
		{
			Ref<Magazine> magazine = _weaponsInfo.RemovePoolMagazine(type);
			if (!magazine) break;
			for (int j=i; j<i+slots; j++) info.magazines[j] = magazine;
		}
		CreateWeaponsPage();
		return;
	}

	name = "fill3:";
	n = strlen(name);
	if (strnicmp(ptr, name, n) == 0)
	{
		ptr += n;
		MagazineType *type = MagazineTypes.New(ptr);
		if (!type) return;

		UnitWeaponsInfo &info = _weaponsInfo._weapons[_currentUnit];
		int maxItems = GetHandGunItemSlotsCount(info.weaponSlots);
		int iCnt = maxItems;
		
		int slots = GetHandGunItemSlotsCount(type->_magazineType);
		if (iCnt <= slots) return;

		// remove all magazines from items 1
		int iMin = 10, iMax = iMin + iCnt;
		for (int i=iMin; i<iMax; i++)
		{
			Magazine *magazine = info.magazines[i];
			if (magazine)
			{
				_weaponsInfo._magazinesPool.Add(magazine);
				info.RemoveMagazine(magazine);
			}
		}

		// add magazines
		for (int i=iMin; i<iMax-slots+1; i+=slots)
		{
			Ref<Magazine> magazine = _weaponsInfo.RemovePoolMagazine(type);
			if (!magazine) break;
			for (int j=i; j<i+slots; j++) info.magazines[j] = magazine;
		}
		CreateWeaponsPage();
		return;
	}

	name = "dropweapon:";
	n = strlen(name);
	if (strnicmp(ptr, name, n) == 0)
	{
		ptr += n;
		UnitWeaponsInfo &info = _weaponsInfo._weapons[_currentUnit];
		AIUnit *unit = info.unit;
		if (unit && unit->GetPerson() == GWorld->GetRealPlayer())
		{
			// create action and perform on unit
			UIAction action;
			action.type = ATDropWeapon;
			action.target = NULL;
			action.param = 0;
			action.param2 = 0;
			action.param3 = ptr;
			action.priority = 0;
			action.showWindow = false;
			action.hideOnUse = false;
			action.Process(unit);
//			UpdateWeaponsInBriefing();
		}
		else if (unit && unit->GetGroup())
		{
			// send command to unit
			Command cmd;
			cmd._message = Command::Action;
			cmd._action = ATDropWeapon;
			cmd._target = NULL;
			cmd._destination = unit->Position();
			cmd._param = 0;
			cmd._param2 = 0;
			cmd._param3 = ptr;
			cmd._time = Glob.time + 480.0;
			unit->GetGroup()->SendAutoCommandToUnit(cmd, unit, true);
		}
		return;
	}

	name = "dropmagazine:";
	n = strlen(name);
	if (strnicmp(ptr, name, n) == 0)
	{
		ptr += n;
		UnitWeaponsInfo &info = _weaponsInfo._weapons[_currentUnit];
		AIUnit *unit = info.unit;
		if (unit && unit->GetPerson() == GWorld->GetRealPlayer())
		{
			// create action and perform on unit
			UIAction action;
			action.type = ATDropMagazine;
			action.target = NULL;
			action.param = 0;
			action.param2 = 0;
			action.param3 = ptr;
			action.priority = 0;
			action.showWindow = false;
			action.hideOnUse = false;
			action.Process(unit);
//			UpdateWeaponsInBriefing();
		}
		else if (unit && unit->GetGroup())
		{
			// send command to unit
			Command cmd;
			cmd._message = Command::Action;
			cmd._action = ATDropMagazine;
			cmd._target = NULL;
			cmd._destination = unit->Position();
			cmd._param = 0;
			cmd._param2 = 0;
			cmd._param3 = ptr;
			cmd._time = Glob.time + 480.0;
			unit->GetGroup()->SendAutoCommandToUnit(cmd, unit, true);
		}
		return;
	}
}

static RString GetTooltip(UnitWeaponsInfo &info, RString href)
{
	const char *name = "fill1:";
	int n = strlen(name);
	if (strnicmp(href, name, n) == 0) return LocalizeString(IDS_TOOLTIP_FILL_ALL_SLOTS);

	name = "fill2:";
	n = strlen(name);
	if (strnicmp(href, name, n) == 0) return LocalizeString(IDS_TOOLTIP_FILL_ALL_SLOTS);

	name = "fill3:";
	n = strlen(name);
	if (strnicmp(href, name, n) == 0) return LocalizeString(IDS_TOOLTIP_FILL_ALL_SLOTS);

	name = "#eq_";
	n = strlen(name);
	if (strnicmp(href, name, n) == 0) return LocalizeString(IDS_TOOLTIP_INFORMATION);

	name = "dropweapon:";
	n = strlen(name);
	if (strnicmp(href, name, n) == 0) return LocalizeString(IDS_TOOLTIP_DROP);

	name = "dropmagazine:";
	n = strlen(name);
	if (strnicmp(href, name, n) == 0) return LocalizeString(IDS_TOOLTIP_DROP);
	
	name = "slot:";
	n = strlen(name);
	if (strnicmp(href, name, n) == 0)
	{
		const char *ptr = href;
		int slot = *(ptr + n) - 'A';

		if (slot < WEAPON_SLOTS)
		{
			return RString();
		}
		else
		{
			const Magazine *magazine = info.magazines[slot - WEAPON_SLOTS];
			const MagazineType *type = magazine ? magazine->_type : NULL;
			if (type) return type->GetDisplayName();
			else return LocalizeString(IDS_EMPTY_SLOT);
		}
	}

	return RString();
}

void DisplayMap::OnDraw(EntityAI *vehicle, float alpha)
{
	// Compass orientation
	Camera *camera = GScene->GetCamera();
	if (camera)
	{
		Vector3Val dir = camera->Direction();
		_compass->SetOrient(Vector3(dir.X(), dir.Z(), dir.Y()), Vector3(0, 0, -1));
	}
	// Radio slots
	SetRadioText();
	// Show / hide mission name
	_name->ShowCtrl(_briefing->ActiveBookmark() >= 0);
	// GPS position
	Object *cameraOn = GWorld->CameraOn();
	if (cameraOn)
	{
		char buffer[16];
		PositionToAA11(cameraOn->Position(), buffer);
		_gpsCtrl->SetText(buffer);
	}
	else
		_gpsCtrl->SetText("----");

	// tooltip
	RString tooltip;
	const HTMLField *field = _briefing->GetActiveField();
	if (_currentUnit >= 0 && field) tooltip = GetTooltip(_weaponsInfo._weapons[_currentUnit], field->href);
	_briefing->SetTooltip(tooltip);


	// FIX: do not show notepad for dead units
	Person *player = GWorld->GetRealPlayer();
	AIUnit *playerUnit = player ? player->Brain() : NULL;
	if (!playerUnit || playerUnit->GetLifeState() == AIUnit::LSDead) ShowNotepad(false);

	Display::OnDraw(vehicle, alpha);
}

/*
void DisplayMap::SwitchDebriefing(bool debriefing)
{
	_debriefing = debriefing;
	if (debriefing)
	{
		if (_briefing->FindSection("Debriefing:Notes") >= 0)
			SwitchBriefingSection("Debriefing:Notes");
		else
			SwitchBriefingSection("Debriefing:Log");
		_bookmark1->SetText(LocalizeString(IDS_MAP_DNOTES));
		_bookmark2->SetText(LocalizeString(IDS_MAP_DLOG));
		_bookmark3->SetText("");
		_bookmark4->SetText("");
	}
	else
	{
		if (_briefing->FindSection("__PLAN") >= 0)
			SwitchBriefingSection("__PLAN");
		else if (_briefing->FindSection("__BRIEFING") >= 0)
			SwitchBriefingSection("__BRIEFING");
		else
			SwitchBriefingSection("Equipment");
		_bookmark1->SetText(LocalizeString(IDS_MAP_NOTES));
		_bookmark2->SetText(LocalizeString(IDS_MAP_PLAN));
		_bookmark3->SetText(LocalizeString(IDS_MAP_GEAR));
		_bookmark4->SetText(LocalizeString(IDS_MAP_GROUP));
	}
}
*/

void DisplayMap::OnSimulate(EntityAI *vehicle)
{
	Display::OnSimulate(vehicle);
	if (_map && _map->IsVisible())
	{
		_map->ProcessCheats();

		float mouseX = 0.5 + GInput.cursorX * 0.5;
		float mouseY = 0.5 + GInput.cursorY * 0.5;

		saturate(mouseX,0,1);
		saturate(mouseY,0,1);

		// automatic map movement on edges
		float dif = 0.02 - mouseX;
		if (dif > 0) _map->ScrollX(0.003 * exp(dif * 100.0f));
		else
		{
			dif = mouseX - 0.98;
			if (dif > 0) _map->ScrollX(-0.003 * exp(dif * 100.0f));
		}

		dif = 0.02 - mouseY;
		if (dif > 0) _map->ScrollY(0.003 * exp(dif * 100.0f));
		else
		{
			dif = mouseY - 0.98;
			if (dif > 0) _map->ScrollY(-0.003 * exp(dif * 100.0f));
		}
		
		IControl *ctrl = GetCtrl(mouseX, mouseY);
		if (ctrl && ctrl == _map)
		{
			if (_map->_dragging || _map->_selecting)
			{
				if (_cursor != CursorMove)
				{
					_cursor = CursorMove;
					SetCursor("Move");
				}
			}
			else if (_map->_moving)
			{
				if (_cursor != CursorScroll)
				{
					_cursor = CursorScroll;
					SetCursor("Scroll");
				}
			}
			else
			{
				if (_cursor != CursorTrack)
				{
					_cursor = CursorTrack;
					SetCursor("Track");
				}
			}
		}
		else
		{
			if (_cursor != CursorArrow)
			{
				_cursor = CursorArrow;
				SetCursor("Arrow");
			}
		}
	}
}

/*!
\patch 1.61 Date 5/29/2002 by Jirka
- Fixed: Map has wrong center when notepad is hidded
*/

void DisplayMap::AdjustMapVisibleRect()
{
	float x = _map->X();
	float y = _map->Y();
	float w = _map->W();
	float h = _map->H();

	if (!IsShownNotepad())
	{
		_map->SetVisibleRect(x, y, w, h);
		return;
	}

	// adjust map visible rectangle
	Vector3 min = _notepad->PositionModelToWorld
	(
		_notepad->GetShape()->Min()
	);
	Vector3 max = _notepad->PositionModelToWorld
	(
		_notepad->GetShape()->Max()
	);
	DrawCoord pt, ptMin, ptMax;
	ptMin.x = 1; ptMin.y = 1;
	ptMax.x = 0; ptMax.y = 0;

	pt = SceneToScreen(min);
	saturateMin(ptMin.x, pt.x);
	saturateMin(ptMin.y, pt.y);
	saturateMax(ptMax.x, pt.x);
	saturateMax(ptMax.y, pt.y);
	pt = SceneToScreen(Vector3(min.X(), min.Y(), max.Z()));
	saturateMin(ptMin.x, pt.x);
	saturateMin(ptMin.y, pt.y);
	saturateMax(ptMax.x, pt.x);
	saturateMax(ptMax.y, pt.y);
	pt = SceneToScreen(Vector3(min.X(), max.Y(), min.Z()));
	saturateMin(ptMin.x, pt.x);
	saturateMin(ptMin.y, pt.y);
	saturateMax(ptMax.x, pt.x);
	saturateMax(ptMax.y, pt.y);
	pt = SceneToScreen(Vector3(min.X(), max.Y(), max.Z()));
	saturateMin(ptMin.x, pt.x);
	saturateMin(ptMin.y, pt.y);
	saturateMax(ptMax.x, pt.x);
	saturateMax(ptMax.y, pt.y);
	pt = SceneToScreen(Vector3(max.X(), min.Y(), min.Z()));
	saturateMin(ptMin.x, pt.x);
	saturateMin(ptMin.y, pt.y);
	saturateMax(ptMax.x, pt.x);
	saturateMax(ptMax.y, pt.y);
	pt = SceneToScreen(Vector3(max.X(), min.Y(), max.Z()));
	saturateMin(ptMin.x, pt.x);
	saturateMin(ptMin.y, pt.y);
	saturateMax(ptMax.x, pt.x);
	saturateMax(ptMax.y, pt.y);
	pt = SceneToScreen(Vector3(max.X(), max.Y(), min.Z()));
	saturateMin(ptMin.x, pt.x);
	saturateMin(ptMin.y, pt.y);
	saturateMax(ptMax.x, pt.x);
	saturateMax(ptMax.y, pt.y);
	pt = SceneToScreen(max);
	saturateMin(ptMin.x, pt.x);
	saturateMin(ptMin.y, pt.y);
	saturateMax(ptMax.x, pt.x);
	saturateMax(ptMax.y, pt.y);

	float aLeft = ptMin.x > x ? (ptMin.x - x) * h : 0;
	float aRight = ptMax.x < x + w ? (x + w - ptMax.x) * h : 0;
	float aTop = ptMin.y > y ? (ptMin.y - y) * w : 0;
	float aBottom = ptMax.y < y + h ? (y + h - ptMax.y) * w : 0;
	
	if (aLeft > aRight)
	{
		if (aLeft > aTop)
		{
			if (aLeft > aBottom) // left
				_map->SetVisibleRect(x, y, ptMin.x - x, h);
			else // bottom
				_map->SetVisibleRect(x, ptMax.y, w, y + h - ptMax.y);
		}
		else if (aTop > aBottom) // top
			_map->SetVisibleRect(x, y, w, ptMin.y - y);
		else // bottom
			_map->SetVisibleRect(x, ptMax.y, w, y + h - ptMax.y);
	}
	else if (aRight > aTop)
	{
		if (aRight > aBottom) // right
			_map->SetVisibleRect(ptMax.x, y, x + w - ptMax.x, h);
		else // bottom
			_map->SetVisibleRect(x, ptMax.y, w, y + h - ptMax.y);
	}
	else if (aTop > aBottom) // top
		_map->SetVisibleRect(x, y, w, ptMin.y - y);
	else if (aBottom > 0) // bottom
		_map->SetVisibleRect(x, ptMax.y, w, y + h - ptMax.y);
	else
		// TODO: move notepad
		_map->SetVisibleRect(x, y, w, h);
}

void DisplayMap::OnChildDestroyed(int idd, int exit)
{
	if (idd == IDD_INSERT_MARKER && exit == IDC_OK)
	{
		DisplayInsertMarker *display = dynamic_cast<DisplayInsertMarker *>((ControlsContainer *)_child);
		Assert(display);

		int index = markersMap.Add();
		ArcadeMarkerInfo &marker = markersMap[index];
		marker.position = _map->ScreenToWorld(DrawCoord(display->_x, display->_y));
		char buffer[256]; sprintf(buffer, "_USER_DEFINED #%d/%d", GetNetworkManager().GetPlayer(), index);
		marker.name = buffer;
		marker.text = display->_text;
		marker.markerType = MTIcon;
		const ParamEntry &markers = Pars >> "CfgMarkers";
		if (display->_picture >= 0 && display->_picture < markers.GetEntryCount())
		{
			const ParamEntry &markerCfg = markers.GetEntry(display->_picture);
			marker.type = markerCfg.GetName();
			marker.OnTypeChanged();
		}

		const ParamEntry &colors = Pars >> "CfgMarkerColors";
		if (display->_color >= 0 && display->_color < colors.GetEntryCount())
		{
			const ParamEntry &colorCfg = colors.GetEntry(display->_color);
			marker.colorName = colorCfg.GetName();
			marker.OnColorChanged();
		}

		marker.fillName = "Solid";
		marker.OnFillChanged();
		marker.a = 1;
		marker.b = 1;
		ChatChannel channel = ActualChatChannel();
		Person *veh = GWorld->GetRealPlayer();
		AIUnit *unit = veh ? veh->Brain() : NULL;
		if (channel == CCGlobal || unit)
			SendMarker(channel, unit, marker);
	}
	Display::OnChildDestroyed(idd, exit);
}

DisplayMainMap::DisplayMainMap(ControlsContainer *parent)
	: DisplayMap(parent, "RscDisplayMainMap")
{
	_enableDisplay = false;

	ShowWarrant(false);
	ShowWalkieTalkie(false);
	ShowGPS(true);
	for (int i=sensorsMap.Size()-1; i>=0; i--)
	{
		Vehicle *veh = sensorsMap[i];
		if (!veh) continue;
		Detector *det = dyn_cast<Detector>(veh);
		Assert(det);
		if (det->IsActive() && !det->IsRepeating()) continue;
		if
		(
			det->GetActivationBy() >= ASAAlpha &&
			det->GetActivationBy() <= ASAJuliet
		)
		{
			ShowWalkieTalkie(true);
			break;
		}
	}
//	SwitchDebriefing(false);
}

DisplayInsertMarker::DisplayInsertMarker(ControlsContainer *parent, float x, float y, float cx, float cy, float cw, float ch, bool enableSimulation)
: Display(parent)
{
	_enableSimulation = enableSimulation;

	GInput.ChangeGameFocus(+1);

	Load("RscDisplayInsertMarker");
	_x = x; _y = y;
	_cx = cx; _cy = cy; _cw = cw; _ch = ch;

	_exitDIK = -1;
	_exitVK = -1;

	_picture = 0;
	_color = 0;
	UpdatePicture();
	
	Control *picture = dynamic_cast<Control *>(GetCtrl(IDC_INSERT_MARKER_PICTURE));
	Control *edit = dynamic_cast<Control *>(GetCtrl(IDC_INSERT_MARKER));
	if (picture && edit)
	{
		float wP = picture->W();
		float wE = edit->W();
		float w = wP + wE;
		float hP = picture->H();
		float hE = edit->H();
		float h = floatMax(hP, hE);
		y -= 0.5 * h;
		x -= 0.5 * wP;
		saturate(x, _cx, _cx + _cw - w);
		saturate(y, _cy, _cy + _ch - h);
		picture->SetPos(x, y + 0.5 * (h - hP), wP, hP);
		edit->SetPos(x + wP, y + 0.5 * (h - hE), wE, hE);
	}
}

DisplayInsertMarker::~DisplayInsertMarker()
{
	GInput.ChangeGameFocus(-1);
}

void DisplayInsertMarker::OnSimulate(EntityAI *vehicle)
{
	// !!! Do not use GetKeysToDo - returns always false in this context

	if (GInput.keysToDo[DIK_ESCAPE])
	{
		GInput.keysToDo[DIK_ESCAPE] = false;
		_exitDIK = IDC_CANCEL;
		if (_exitVK == IDC_CANCEL) Exit(IDC_CANCEL);
		return;
	}
	if (GInput.keysToDo[DIK_RETURN])
	{
		GInput.keysToDo[DIK_RETURN] = false;
		_exitDIK = IDC_OK;
		if (_exitVK == IDC_OK) Exit(IDC_OK);
		return;
	}
	if (GInput.keysToDo[DIK_NUMPADENTER])
	{
		GInput.keysToDo[DIK_NUMPADENTER] = false;
		_exitDIK = IDC_OK;
		if (_exitVK == IDC_OK) Exit(IDC_OK);
		return;
	}
	if (GInput.keysToDo[DIK_DOWN])
	{
		GInput.keysToDo[DIK_DOWN] = false;
		if (GInput.keys[DIK_LSHIFT] || GInput.GetKey(DIK_RSHIFT))
			NextColor();
		else
			NextPicture();
		UpdatePicture();
	}
	if (GInput.keysToDo[DIK_UP])
	{
		GInput.keysToDo[DIK_UP] = false;
		if (GInput.keys[DIK_LSHIFT] || GInput.GetKey(DIK_RSHIFT))
			PrevColor();
		else
			PrevPicture();
		UpdatePicture();
	}
	Display::OnSimulate(vehicle);
}

void DisplayInsertMarker::OnButtonClicked(int idc)
{
	switch (idc)
	{
		case IDC_CANCEL:
		case IDC_OK:
			_exitVK = idc;
			if (_exitDIK == idc) Exit(idc);
			break;
		default:
			Display::OnButtonClicked(idc);
			break;
	}
}

// Control *DisplayInsertMarker::OnCreateCtrl(int type, int idc, const ParamEntry &cls);
void DisplayInsertMarker::Destroy()
{
	Display::Destroy();

	if (_exit != IDC_OK) return;

	CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_INSERT_MARKER));
	Assert(edit);
	_text = edit->GetText();
}

void DisplayInsertMarker::UpdatePicture()
{
	CStatic *ctrl = dynamic_cast<CStatic *>(GetCtrl(IDC_INSERT_MARKER_PICTURE));
	if (ctrl)
	{
		const ParamEntry &markers = Pars >> "CfgMarkers";
		if (_picture < 0 || _picture >= markers.GetEntryCount()) return;
		const ParamEntry &colors = Pars >> "CfgMarkerColors";
		if (_color < 0 || _color >= colors.GetEntryCount()) return;

		const ParamEntry &markerCfg = markers.GetEntry(_picture);
		RString picture = markerCfg >> "icon";
		const ParamEntry &colorCfg = colors.GetEntry(_color);
		PackedColor color;
		if (stricmp(colorCfg.GetName(), "default") == 0)
			color = GetPackedColor(markerCfg >> "color");
		else
			color = GetPackedColor(colorCfg >> "color");
		ctrl->SetText(picture);
		ctrl->SetFtColor(color);
	}
}

void DisplayInsertMarker::PrevPicture()
{
	const ParamEntry &markers = Pars >> "CfgMarkers";
	int n = markers.GetEntryCount();
	_picture--;
	if (_picture < 0) _picture = n - 1;
}

void DisplayInsertMarker::NextPicture()
{
	const ParamEntry &markers = Pars >> "CfgMarkers";
	int n = markers.GetEntryCount();
	_picture++;
	if (_picture >= n) _picture = 0;
}

void DisplayInsertMarker::PrevColor()
{
	const ParamEntry &colors = Pars >> "CfgMarkerColors";
	int n = colors.GetEntryCount();
	_color--;
	if (_color < 0) _color = n - 1;
}

void DisplayInsertMarker::NextColor()
{
	const ParamEntry &colors = Pars >> "CfgMarkerColors";
	int n = colors.GetEntryCount();
	_color++;
	if (_color >= n) _color = 0;
}

ChatChannel ActualChatChannel();
void SetChatChannel(ChatChannel channel);

/*!
\patch 1.50 Date 4/15/2002 by Jirka
- Improved: side chat channel selected when entering briefing screen
*/
DisplayGetReady::DisplayGetReady(ControlsContainer *parent)
	: DisplayMap(parent, "RscDisplayGetReady")
{
	_enableSimulation = false;
	_enableDisplay = false;
	ShowCompass(false);
	ShowWatch(false);
	ShowWalkieTalkie(false);
	ShowGPS(false);
	const ParamEntry *cls = ExtParsMission.FindEntry("showMap");
	ShowMap(!cls ? true : (*cls));
	cls = ExtParsMission.FindEntry("showNotepad");
	ShowNotepad(!cls ? true : (*cls));

//	SwitchDebriefing(false);

	AIUnit *unit = GWorld->FocusOn();
	_selectWeapons = unit && unit->IsGroupLeader();

	RString weapons = GetSaveDirectory() + RString("weapons.cfg");
	if (QIFStream::FileExists(weapons))
	{
		_weaponsInfo.Load(weapons);
		// create html page
		CreateWeaponsPage();
	}
	else
	{
		UpdateWeaponsInBriefing();
	}

	// FIX - cancel user input
	void RemoveInputMessages();
	RemoveInputMessages();

	GChatList.SetRect(0.05, 0.90, 0.9, 0.02);
	GChatList.SetRows(4);

	if
	(
		GetNetworkManager().GetGameState() >= NGSCreate &&
		ActualChatChannel() != CCSide
	)
	{
		SetChatChannel(CCSide);
		if (GWorld->ChatChannel()) GWorld->ChatChannel()->ResetHUD();
		if (GWorld->VoiceChat()) GWorld->VoiceChat()->ResetHUD();
		GWorld->OnChannelChanged();
	}

	_soundPlanPlayed = false;
	_soundNotesPlayed = false;
	_soundGearPlayed = false;
	_soundGroupPlayed = false;
	int index = _briefing->CurrentSection();
	const HTMLSection &section = _briefing->GetSection(index);
	if (section.names.Size() > 0)
		SwitchBriefingSection(section.names[0]);
}

DisplayGetReady::DisplayGetReady(ControlsContainer *parent, RString resource)
	: DisplayMap(parent, resource)
{
	_enableSimulation = false;
	_enableDisplay = false;
	ShowCompass(false);
	ShowWatch(false);
	ShowWalkieTalkie(false);
	ShowGPS(false);
	const ParamEntry *cls = ExtParsMission.FindEntry("showMap");
	ShowMap(!cls ? true : (*cls));
	cls = ExtParsMission.FindEntry("showNotepad");
	ShowNotepad(!cls ? true : (*cls));

//	SwitchDebriefing(false);

	AIUnit *unit = GWorld->FocusOn();
	_selectWeapons = unit && unit->IsGroupLeader();

	RString weapons = GetSaveDirectory() + RString("weapons.cfg");
	if (QIFStream::FileExists(weapons))
	{
		_weaponsInfo.Load(weapons);
		// create html page
		CreateWeaponsPage();
	}
	else
	{
		UpdateWeaponsInBriefing();
	}

	// FIX - cancel user input
	void RemoveInputMessages();
	RemoveInputMessages();

	GChatList.SetRect(0.05, 0.90, 0.9, 0.02);
	GChatList.SetRows(4);

	if
	(
		GetNetworkManager().GetGameState() >= NGSCreate &&
		ActualChatChannel() != CCSide
	)
	{
		SetChatChannel(CCSide);
		if (GWorld->ChatChannel()) GWorld->ChatChannel()->ResetHUD();
		if (GWorld->VoiceChat()) GWorld->VoiceChat()->ResetHUD();
		GWorld->OnChannelChanged();
	}
}

DisplayGetReady::~DisplayGetReady()
{
	GChatList.SetRect(0.05, 0.02, 0.9, 0.02);
	GChatList.SetRows(4);
}

/*!
\patch 1.75 Date 2/7/2002 by Jirka
- Added: Play user defined sound (voice) when section in briefing changes
*/

extern RString GBriefingOnPlan;
extern RString GBriefingOnNotes;
extern RString GBriefingOnGear;
extern RString GBriefingOnGroup;


void DisplayGetReady::SwitchBriefingSection(RString section)
{
	DisplayMap::SwitchBriefingSection(section);
#if _ENABLE_DATADISC
	_sound = NULL;
	if (stricmp(section, "__plan") == 0)
	{
		if (!_soundPlanPlayed)
		{
			PlaySound(GBriefingOnPlan);
			_soundPlanPlayed = true;
		}
	}
	else if (stricmp(section, "__briefing") == 0)
	{
		if (!_soundNotesPlayed)
		{
			PlaySound(GBriefingOnNotes);
			_soundNotesPlayed = true;
		}
	}
	else if (stricmp(section, "equipment") == 0)
	{
		if (!_soundGearPlayed)
		{
			PlaySound(GBriefingOnGear);
			_soundGearPlayed = true;
		}
	}
	else if (stricmp(section, "group") == 0)
	{
		if (!_soundGroupPlayed)
		{
			PlaySound(GBriefingOnGroup);
			_soundGroupPlayed = true;
		}
	}
#endif
}

const ParamEntry *FindSound(RString name, SoundPars &pars);

void DisplayGetReady::PlaySound(RString name)
{
/*
	GameValue value = GWorld->GetGameState()->VarGet(var);
	if (value.GetNil()) return;
	RString name = value;
*/
	if (name.GetLength() == 0) return;

	SoundPars pars;
	if (!FindSound(name, pars)) return;
	_sound = GSoundScene->OpenAndPlayOnce2D
	(
		pars.name, pars.vol, pars.freq, false
	);
	_sound->SetKind(WaveMusic);
	_sound->SetSticky(true);
}

void DisplayGetReady::OnButtonClicked(int idc)
{
	switch (idc)
	{
	case IDC_OK:
	case IDC_CANCEL:
		Display::OnButtonClicked(idc);
		break;
	default:
		DisplayMap::OnButtonClicked(idc);
		break;
	}
}

void DisplayGetReady::Destroy()
{
	if (_selectWeapons)
	{
		RString weapons = GetSaveDirectory() + RString("weapons.cfg");
		_weaponsInfo.Save(weapons);

		_weaponsInfo.Apply();
	}
	DisplayMap::Destroy();
}

RString GetBaseDirectory();
RString GetBaseSubdirectory();
bool ParseMission(bool multiplayer);

/*!
\patch 1.21 Date 08/21/2001 by Jirka
- Improved: Multiplayer debriefing display
*/
DisplayDebriefing::DisplayDebriefing(ControlsContainer *parent, bool animation)
	: Display(parent)
{
//	GWorld->EnableDisplay(false);
	Load("RscDisplayDebriefing");
	//Assert(_debriefing);
	CreateDebriefing();
	_animation = animation;
/*
	if (animation)
	{
		_oldMission = Glob.header.filename;
		_oldDirectory = GetBaseDirectory();
		_oldSubdirectory = GetBaseSubdirectory();

		void StartRandomCutscene(RString world);
		StartRandomCutscene(Glob.header.worldname);
	}
	else
	{
		_enableSimulation = false;
	}
*/
	_enableSimulation = false;

	GetCtrl(IDC_DEBRIEFING_PAD2)->ShowCtrl(false);

	if (GetNetworkManager().IsServer() || GetNetworkManager().IsGameMaster())
	{
		// server
		_server = true;
		_client = false;
		// GetNetworkManager().ClientReady(NGSDebriefingOK);
		CActiveText *ctrl = dynamic_cast<CActiveText *>(GetCtrl(IDC_DEBRIEFING_RESTART));
		if (ctrl)
		{
			ctrl->SetText(LocalizeString(IDS_DISP_CLIENT_READY));
			ctrl->ShowCtrl(false);
		}
	}
	else if (GetNetworkManager().GetGameState() > NGSNone)
	{
		// client
		_server = false;
		_client = true;
		CActiveText *ctrl = dynamic_cast<CActiveText *>(GetCtrl(IDC_DEBRIEFING_RESTART));
		if (ctrl) ctrl->SetText(LocalizeString(IDS_DISP_CLIENT_READY));
		ctrl = dynamic_cast<CActiveText *>(GetCtrl(IDC_CANCEL));
		if (ctrl) ctrl->SetText(LocalizeString(IDS_DISP_DISCONNECT));
	}
	else
	{
		// single player
		_server = false;
		_client = false;
		GetCtrl(IDC_DEBRIEFING_PLAYERS_TITLE_BG)->ShowCtrl(false);
		GetCtrl(IDC_DEBRIEFING_PLAYERS_TITLE)->ShowCtrl(false);
		GetCtrl(IDC_DEBRIEFING_PLAYERS_BG)->ShowCtrl(false);
		GetCtrl(IDC_DEBRIEFING_PLAYERS)->ShowCtrl(false);
	}
}

void DisplayDebriefing::Destroy()
{
	Display::Destroy();
//	GWorld->EnableDisplay(true);
}

Control *DisplayDebriefing::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
	case IDC_DEBRIEFING_LEFT:
		{
			C3DHTML *html = new C3DHTML(this, idc, cls);
			_left = html;
			return html;
		}
		break;
	case IDC_DEBRIEFING_RIGHT:
		{
			C3DHTML *html = new C3DHTML(this, idc, cls);
			_right = html;
			return html;
		}
		break;
	case IDC_DEBRIEFING_STAT:
		{
			C3DHTML *html = new C3DHTML(this, idc, cls);
			_stats = html;
			return html;
		}
		break;
	default:
		return Display::OnCreateCtrl(type, idc, cls);
	}
}

/*!
\patch 1.28 Date 10/22/2001 by Jirka
- Fixed: Debriefing for gamemaster of dedicated server disappear with no user action.
*/

void DisplayDebriefing::OnButtonClicked(int idc)
{
	switch (idc)
	{
	case IDC_DEBRIEFING_RESTART:
		if (_client)
		{
			GetNetworkManager().ClientReady(NGSDebriefingOK);
			GetCtrl(IDC_CANCEL)->ShowCtrl(false);
		}
		else if (!_server)
		{
			if (_oldStats._mission._lives == 0) break;
			GStats = _oldStats;

/*
			if (_animation)
			{
				// reload old mission
				SetMission(Glob.header.worldname, _oldMission, _oldSubdirectory);
				SetBaseDirectory(_oldDirectory);

				ParseMission(false);
			}
*/

			// restart mission
			GWorld->SwitchLandscape(GetWorldName(Glob.header.worldname));
			GStats.ClearMission();
			GWorld->ActivateAddons(CurrentTemplate.addOns);
			GWorld->InitGeneral(CurrentTemplate.intel);
			if (!GWorld->InitVehicles(GModeArcade, CurrentTemplate)) break;

			int lives = GStats._mission._lives;
			if (GStats._mission._lives > 0)
				GStats._mission._lives = lives - 1;
			Exit(IDC_OK);
		}
		break;
	case IDC_CANCEL:
		if (GetNetworkManager().IsServer() || GetNetworkManager().IsGameMaster())
			GetNetworkManager().ClientReady(NGSDebriefingOK);
		Exit(IDC_CANCEL);
		break;
	case IDC_AUTOCANCEL:
		Exit(IDC_AUTOCANCEL);
		break;
	default:
		Display::OnButtonClicked(idc);
		break;
	}
}

void DisplayDebriefing::OnHTMLLink(int idc, RString link)
{
	if (idc == IDC_DEBRIEFING_RIGHT)
	{
		if (stricmp(link, "stat:open") == 0)
		{
			GetCtrl(IDC_DEBRIEFING_PAD2)->ShowCtrl(true);
			return;
		}
	}
	else if (idc == IDC_DEBRIEFING_STAT)
	{
		if (stricmp(link, "stat:close") == 0)
		{
			GetCtrl(IDC_DEBRIEFING_PAD2)->ShowCtrl(false);
			return;
		}
	}

	Display::OnHTMLLink(idc, link);
}

void DisplayDebriefing::OnSimulate(EntityAI *vehicle)
{
	if (_server)
	{
		if (!GetNetworkManager().IsServer() && !GetNetworkManager().IsGameMaster())
		{
			_server = false;
			GetCtrl(IDC_DEBRIEFING_RESTART)->ShowCtrl(true);
			CActiveText *ctrl = dynamic_cast<CActiveText *>(GetCtrl(IDC_CANCEL));
			if (ctrl) ctrl->SetText(LocalizeString(IDS_DISP_DISCONNECT));
		}
	}
	else
	{
		if (GetNetworkManager().IsServer() || GetNetworkManager().IsGameMaster())
		{
			_server = true;
			GetCtrl(IDC_DEBRIEFING_RESTART)->ShowCtrl(false);
			CActiveText *ctrl = dynamic_cast<CActiveText *>(GetCtrl(IDC_CANCEL));
			if (ctrl) ctrl->SetText(LocalizeString(IDS_DISP_CONTINUE));
		}
	}

	// update player list
	CListBox *lbox = dynamic_cast<CListBox *>(GetCtrl(IDC_DEBRIEFING_PLAYERS));
	if (lbox)
	{
		lbox->SetReadOnly();
		lbox->ShowSelected(false);
		lbox->ClearStrings();

		for (int i=0; i<GetNetworkManager().NPlayerRoles(); i++)
		{
			int dpnid = GetNetworkManager().GetPlayerRole(i)->player;
			if (dpnid == NO_PLAYER || dpnid == AI_PLAYER) continue;
			const PlayerIdentity *identity = GetNetworkManager().FindIdentity(dpnid);
			if (!identity) continue;
			int index = lbox->AddString(identity->name);
			switch (identity->state)
			{
			case NGSNone:
			case NGSCreating:
			case NGSCreate:
			case NGSLogin:
			case NGSEdit:
			case NGSMissionVoted:
			case NGSPrepareSide:
			case NGSPrepareRole:
			case NGSPrepareOK:
			case NGSDebriefingOK:
				lbox->SetFtColor(index, PackedColor(Color(0, 1, 0, 1)));
				lbox->SetSelColor(index, PackedColor(Color(0, 1, 0, 1)));
				lbox->SetValue(index, 2);
				break;
			case NGSDebriefing:
				lbox->SetFtColor(index, PackedColor(Color(1, 1, 0, 1)));
				lbox->SetSelColor(index, PackedColor(Color(1, 1, 0, 1)));
				lbox->SetValue(index, 1);
				break;
			case NGSTransferMission:
			case NGSLoadIsland:
			case NGSBriefing:
			case NGSPlay:
				lbox->SetFtColor(index, PackedColor(Color(1, 0, 0, 1)));
				lbox->SetSelColor(index, PackedColor(Color(1, 0, 0, 1)));
				lbox->SetValue(index, 0);
				break;
			default:
				break;
			}
		}
		lbox->SortItemsByValue();
	}

	Display::OnSimulate(vehicle);
}

struct KillsInfo
{
	const VehicleType *type;
	RString playerName;
	int n;
};
TypeIsMovableZeroed(KillsInfo)

struct CasualtiesInfo
{
	bool player;
	RString killedName;
	RString killerName;
	int n;
};
TypeIsMovableZeroed(CasualtiesInfo)

int CmpKillsInfo(const KillsInfo *info1, const KillsInfo *info2)
{
	if (info2->playerName.GetLength() > 0)
		if (info2->playerName.GetLength() > 0) return info2->n - info1->n;
		else return 1;
	else if (info1->playerName.GetLength() > 0) return -1;
	else return (int)(info2->type->_cost - info1->type->_cost);
}

int CmpCasualtiesInfo(const CasualtiesInfo *info1, const CasualtiesInfo *info2)
{
	if (info2->player)
		if (info1->player) return info2->n - info1->n;
		else return 1;
	else if (info1->player) return -1;
	else return strcmp(info1->killedName, info2->killedName);
}

#if _ENABLE_CHEATS
	#define LOG_DEBRIEFING 1
#else
	#define LOG_DEBRIEFING 0
#endif

void DisplayDebriefing::CreateDebriefing()
{
	// prepare statistics	
	_oldStats = GStats;
	GStats.Update();

	AIUnitHeader &oldInfo = _oldStats._campaign._playerInfo;
	AIUnitHeader &newInfo = GStats._campaign._playerInfo;
	RString playerName = GetLocalPlayerName();
	if (newInfo.unit) playerName = newInfo.unit->GetPerson()->GetInfo()._name;
	
	// preload and create section
	RString briefing = GetBriefingFile();
	if (briefing.GetLength() > 0)
	{
		_left->Load(briefing);
		_right->Load(briefing);
	}
	int lSection = _left->AddSection();
	_left->AddName(lSection, "__OBJECTIVES");
	int rSection = _right->AddSection();
	_right->AddName(rSection, "__DEBRIEFING");
	int sSection = _stats->AddSection();
	_stats->AddName(sSection, "__STATISTICS");

#if LOG_DEBRIEFING
	bool doOutput = false;
	const char *end = "";
	switch (GWorld->GetEndMode())
	{
	case EMLoser:
		doOutput = true;
		end = "LOST";
		break;
	case EMEnd1:
		doOutput = true;
		end = "END 1";
		break;
	case EMEnd2:
		doOutput = true;
		end = "END 2";
		break;
	case EMEnd3:
		doOutput = true;
		end = "END 3";
		break;
	case EMEnd4:
		doOutput = true;
		end = "END 4";
		break;
	case EMEnd5:
		doOutput = true;
		end = "END 5";
		break;
	case EMEnd6:
		doOutput = true;
		end = "END 6";
		break;
	}

	FILE *file = NULL;
	if (doOutput) file = fopen("rating.log", "a+");
#endif

	// mission name
	RString text = Localize(CurrentTemplate.intel.briefingName);
	if (text.GetLength() == 0)
		text = RString(Glob.header.filename);
	_right->AddText(rSection, text, HFH1, HACenter, false, false, "");
	_right->AddBreak(rSection, false);

#if LOG_DEBRIEFING
	if (file)
	{
		fprintf(file, "************************************************************\n");
		fprintf(file, "User: %s\n", (const char *)Glob.header.playerName);
		fprintf(file, "Mission: %s (%s)\n", (const char *)Glob.header.filename, end);
		time_t t;
		time(&t);
		fprintf(file, "Time: %s\n", ctime(&t));
	}
#endif

	// rank, name of soldier
	text = LocalizeString(IDS_PRIVATE + newInfo.rank) + RString(" ") + playerName;
	_right->AddText(rSection, text, HFP, HALeft, false, false, "");
	_right->AddBreak(rSection, false);

	// mission duration
	int day = CurrentTemplate.intel.day - 1;
	int year = CurrentTemplate.intel.year;
	for (int m=0; m<CurrentTemplate.intel.month-1; m++) day += GetDaysInMonth(year, m);
	float time = CurrentTemplate.intel.hour * OneHour + CurrentTemplate.intel.minute * OneMinute + day * OneDay + 0.5 * OneSecond;
	float dt = floatMax(Glob.clock.GetTimeInYear() - time, 0);
	dt *= 365 * 24; int hours = toIntFloor(dt); dt -= hours;
	dt *= 60; int minutes = toInt(dt); dt -= minutes;
	char buffer[256];
	if (hours > 0)
		sprintf
		(
			buffer, LocalizeString(IDS_BRIEF_DURATION_LONG),
			hours, minutes
		);
	else
		sprintf
		(
			buffer, LocalizeString(IDS_BRIEF_DURATION_SHORT),
			minutes
		);
	_right->AddText(rSection, buffer, HFP, HALeft, false, false, "");
	_right->AddBreak(rSection, false);

#if LOG_DEBRIEFING
	if (file) fprintf(file, "%s\n", buffer);
#endif

	// score
	float score = newInfo.experience - oldInfo.experience;
	sprintf(buffer, LocalizeString(IDS_BRIEF_SCORE), score);
	_right->AddText(rSection, buffer, HFP, HALeft, false, false, "");
	int points = (int)(GStats._campaign._score - _oldStats._campaign._score);
	if (points >= 0)
	{
		RString image = Glob.config.easyMode ? "debr_pecka.paa" : "debr_star.paa";
#if LOG_DEBRIEFING
		RString sign = Glob.config.easyMode ? "o" : "*";
#endif
		for (int i=0; i<points + 1; i++)
		{
			_right->AddImage(rSection, image, HALeft, false, 16, 16, "");
#if LOG_DEBRIEFING
			strcat(buffer, sign);
#endif
		}
	}
	else
	{
		RString image = "mission_uncomplete.paa";
#if LOG_DEBRIEFING
		RString sign = "X";
#endif
		for (int i=0; i<-points; i++)
		{
			_right->AddImage(rSection, image, HALeft, false, 16, 16, "");
#if LOG_DEBRIEFING
			strcat(buffer, sign);
#endif
		}
	}
	_right->AddBreak(rSection, false);

#if LOG_DEBRIEFING
	if (file)
	{
		fprintf(file, "%s\n", buffer);
		fprintf(file, "Total score in campaign: %d\n", GStats._campaign._score);
	}
#endif

	// mission end
	int src = -1;
	switch (GWorld->GetEndMode())
	{
	case EMLoser:
		src = _right->FindSection("Debriefing:Loser");
		break;
	case EMEnd1:
		src = _right->FindSection("Debriefing:End1");
		break;
	case EMEnd2:
		src = _right->FindSection("Debriefing:End2");
		break;
	case EMEnd3:
		src = _right->FindSection("Debriefing:End3");
		break;
	case EMEnd4:
		src = _right->FindSection("Debriefing:End4");
		break;
	case EMEnd5:
		src = _right->FindSection("Debriefing:End5");
		break;
	case EMEnd6:
		src = _right->FindSection("Debriefing:End6");
		break;
	}
	if (src >= 0) _right->CopySection(src, rSection);

	// objectives
	_left->AddText(lSection, LocalizeString(IDS_BRIEF_OBJECTIVES), HFH1, HACenter, false, false, "");
	_left->AddBreak(lSection, false);
	TargetSide side = TargetSide(Glob.header.playerSide);
	for (int s=0; s<_left->NSections(); s++)
	{
		const HTMLSection &src = _left->GetSection(s);
		for (int n=0; n<src.names.Size(); n++)
		{
			RString name = src.names[n];
			static const char *prefix = "OBJ_";
			static const char *prefixWest = "OBJ_WEST_";
			static const char *prefixEast = "OBJ_EAST_";
			static const char *prefixGuerrila = "OBJ_GUER_";
			static const char *prefixCivilian = "OBJ_CIVIL_";
			if (strnicmp(name, prefix, strlen(prefix)) == 0)
			{
				if (strnicmp(name, prefixWest, strlen(prefixWest)) == 0)
				{
					if (side != TWest) continue;
				}
				else if (strnicmp(name, prefixEast, strlen(prefixEast)) == 0)
				{
					if (side != TEast) continue;
				}
				if (strnicmp(name, prefixGuerrila, strlen(prefixGuerrila)) == 0)
				{
					if (side != TGuerrila) continue;
				}
				if (strnicmp(name, prefixCivilian, strlen(prefixCivilian)) == 0)
				{
					if (side != TCivilian) continue;
				}
				GameState *state = GWorld->GetGameState();
				int value = toInt((float)state->Evaluate(name));
				AddObjective(_left, s, lSection, value);
				break; // next section
			}
		}
	}
	_left->AddBreak(lSection, false);

	// statistics
	_stats->AddText(sSection, LocalizeString(IDS_BRIEF_STATISTICS), HFH1, HACenter, false, false, "");
	_stats->AddBreak(sSection, false);

	AutoArray<AIStatsEvent> &events = _oldStats._mission._events;

	AUTO_STATIC_ARRAY(KillsInfo, kills, 32);
	// kills - enemies
	for (int i=0; i<events.Size(); i++)
	{
		AIStatsEvent &event = events[i];
		switch (event.type)
		{
		case SETKillsEnemyInfantry:
		case SETKillsEnemySoft:
		case SETKillsEnemyArmor:
		case SETKillsEnemyAir:
			{
				RString playerName = event.killedPlayer ? event.killedName : "";
				for (int j=0; j<kills.Size(); j++)
				{
					if (kills[j].type == event.killedType && kills[j].playerName == playerName)
					{
						kills[j].n++;
						goto ExitSwitchEnemies;
					}
				}
				{
					int index = kills.Add();
					kills[index].type = event.killedType;
					kills[index].playerName = playerName;
					kills[index].n = 1;
				}
			}
		ExitSwitchEnemies:
			break;
		}
	}
	if (kills.Size() > 0)
	{
		_stats->AddText(sSection, LocalizeString(IDS_BRIEF_YOURKILLS), HFP, HALeft, false, false, "");
		_stats->AddBreak(sSection, false);
#if LOG_DEBRIEFING
		if (file) fprintf(file, "\nYour kills:\n");
#endif

		QSort(kills.Data(), kills.Size(), CmpKillsInfo);
		for (int j=0; j<kills.Size(); j++)
		{
			KillsInfo &info = kills[j];
			RString killed = info.playerName;
			if (killed.GetLength() == 0) killed = info.type->GetDisplayName();
			if (info.n > 1)
				sprintf(buffer, LocalizeString(IDS_BRIEF_FORMAT_GENERIC_TIMES), info.n, (const char *)killed);
			else
				strcpy(buffer, killed);
			_stats->AddText(sSection, buffer, HFP, HALeft, false, false, "");
			_stats->AddBreak(sSection, false);
#if LOG_DEBRIEFING
			if (file) fprintf(file, "%s\n", buffer);
#endif
		}
		_stats->AddBreak(sSection, false);
	}

	// kills - friends
	kills.Resize(0);
	for (int i=0; i<events.Size(); i++)
	{
		AIStatsEvent &event = events[i];
		switch (event.type)
		{
		case SETKillsFriendlyInfantry:
		case SETKillsFriendlySoft:
		case SETKillsFriendlyArmor:
		case SETKillsFriendlyAir:
//		case SETUnitLost:
			{
				RString playerName = event.killedPlayer ? event.killedName : "";
				for (int j=0; j<kills.Size(); j++)
				{
					if (kills[j].type == event.killedType && kills[j].playerName == playerName)
					{
						kills[j].n++;
						goto ExitSwitchFriends;
					}
				}
				{
					int index = kills.Add();
					kills[index].type = event.killedType;
					kills[index].playerName = playerName;
					kills[index].n = 1;
				}
			}
		ExitSwitchFriends:
			break;
		}
	}
	if (kills.Size() > 0)
	{
		_stats->AddText(sSection, LocalizeString(IDS_BRIEF_YOURKILLS_FRIENDLY), HFP, HALeft, false, false, "");
		_stats->AddBreak(sSection, false);
#if LOG_DEBRIEFING
		if (file) fprintf(file, "\nYour kills - friendly units:\n");
#endif

		QSort(kills.Data(), kills.Size(), CmpKillsInfo);
		for (int j=0; j<kills.Size(); j++)
		{
			KillsInfo &info = kills[j];
			RString killed = info.playerName;
			if (killed.GetLength() == 0) killed = info.type->GetDisplayName();
			if (info.n > 1)
				sprintf(buffer, LocalizeString(IDS_BRIEF_FORMAT_GENERIC_TIMES), info.n, (const char *)killed);
			else
				strcpy(buffer, killed);
			_stats->AddText(sSection, buffer, HFP, HALeft, false, false, "");
			_stats->AddBreak(sSection, false);
#if LOG_DEBRIEFING
			if (file) fprintf(file, "%s\n", buffer);
#endif
		}
		_stats->AddBreak(sSection, false);
	}

	// kills - civilians
	kills.Resize(0);
	for (int i=0; i<events.Size(); i++)
	{
		AIStatsEvent &event = events[i];
		switch (event.type)
		{
		case SETKillsCivilInfantry:
		case SETKillsCivilSoft:
		case SETKillsCivilArmor:
		case SETKillsCivilAir:
			{
				RString playerName = event.killedPlayer ? event.killedName : "";
				for (int j=0; j<kills.Size(); j++)
				{
					if (kills[j].type == event.killedType && kills[j].playerName == playerName)
					{
						kills[j].n++;
						goto ExitSwitchCivil;
					}
				}
				{
					int index = kills.Add();
					kills[index].type = event.killedType;
					kills[index].playerName = playerName;
					kills[index].n = 1;
				}
			}
		ExitSwitchCivil:
			break;
		}
	}
	if (kills.Size() > 0)
	{
		_stats->AddText(sSection, LocalizeString(IDS_BRIEF_YOURKILLS_CIVIL), HFP, HALeft, false, false, "");
		_stats->AddBreak(sSection, false);
#if LOG_DEBRIEFING
		if (file) fprintf(file, "\nYour kills - civilians:\n");
#endif

		QSort(kills.Data(), kills.Size(), CmpKillsInfo);
		for (int j=0; j<kills.Size(); j++)
		{
			KillsInfo &info = kills[j];
			RString killed = info.playerName;
			if (killed.GetLength() == 0) killed = info.type->GetDisplayName();
			if (info.n > 1)
				sprintf(buffer, LocalizeString(IDS_BRIEF_FORMAT_GENERIC_TIMES), info.n, (const char *)killed);
			else
				strcpy(buffer, killed);
			_stats->AddText(sSection, buffer, HFP, HALeft, false, false, "");
			_stats->AddBreak(sSection, false);
#if LOG_DEBRIEFING
			if (file) fprintf(file, "%s\n", buffer);
#endif
		}
		_stats->AddBreak(sSection, false);
	}
		
	AUTO_STATIC_ARRAY(CasualtiesInfo, casual, 32);
	int playerTotal = 0;
	for (int i=0; i<events.Size(); i++)
	{
		AIStatsEvent &event = events[i];
		if (event.type != SETUnitLost) continue;
		
		RString killerName;
		if (event.killedPlayer)
		{
			playerTotal++;
			if (!event.killerPlayer) continue;
			killerName = event.killerName;
		}
		for (int j=0; j<casual.Size(); j++)
		{
			if (casual[j].killedName == event.killedName && casual[j].killerName == killerName)
			{
				casual[j].n++;
				goto ExitSwitch2;
			}
		}
		{
			int index = casual.Add();
			casual[index].player = event.killedPlayer;
			casual[index].killedName = event.killedName;
			casual[index].killerName = killerName;
			casual[index].n = 1;
		}
	ExitSwitch2:
		;
	}
	if (playerTotal > 0 || casual.Size() > 0)
	{
		_stats->AddText(sSection, LocalizeString(IDS_BRIEF_CASUALTIES), HFP, HALeft, false, false, "");
		_stats->AddBreak(sSection, false);

#if LOG_DEBRIEFING
		if (file) fprintf(file, "\nCasualties:\n");
#endif
	
		QSort(casual.Data(), casual.Size(), CmpCasualtiesInfo);
		if (playerTotal > 0)
		{
			if (playerTotal > 1)
				sprintf(buffer, LocalizeString(IDS_BRIEF_FORMAT_YOU_TIMES), playerTotal, (const char *)GetLocalPlayerName());
			else
				sprintf(buffer, LocalizeString(IDS_BRIEF_FORMAT_YOU_ONCE), (const char *)GetLocalPlayerName());
			_stats->AddText(sSection, buffer, HFP, HALeft, false, false, "");
			_stats->AddBreak(sSection, false);
	#if LOG_DEBRIEFING
		if (file) fprintf(file, "%s\n", buffer);
	#endif
		}
		for (int j=0; j<casual.Size(); j++)
		{
			CasualtiesInfo &info = casual[j];
			if (info.player)
			{
				Assert(info.killerName.GetLength() > 0);
				if (info.n > 1)
					sprintf(buffer, LocalizeString(IDS_BRIEF_FORMAT_BY_TIMES), info.n, (const char *)info.killerName);
				else
					sprintf(buffer, LocalizeString(IDS_BRIEF_FORMAT_BY_ONCE), (const char *)info.killerName);
			}
			else
			{
				if (info.n > 1)
					sprintf(buffer, LocalizeString(IDS_BRIEF_FORMAT_GENERIC_TIMES), info.n, (const char *)info.killedName);
				else
					strcpy(buffer, info.killedName);
			}
			_stats->AddText(sSection, buffer, HFP, HALeft, false, false, "");
			_stats->AddBreak(sSection, false);
	#if LOG_DEBRIEFING
		if (file) fprintf(file, "%s\n", buffer);
	#endif
		}
	}

	_right->AddText(rSection, LocalizeString(IDS_BRIEF_STAT_OPEN), HFP, HALeft, false, false, "Stat:open");
	_right->AddBreak(rSection, false);

	_stats->AddText(sSection, LocalizeString(IDS_BRIEF_STAT_CLOSE), HFP, HALeft, false, false, "Stat:close");
	_stats->AddBreak(sSection, false);
	

	// complete and switch to debriefing
	_left->FormatSection(lSection);
	_left->SwitchSection("__OBJECTIVES");
	_right->FormatSection(rSection);
	_right->SwitchSection("__DEBRIEFING");
	_stats->FormatSection(sSection);
	_stats->SwitchSection("__STATISTICS");

#if LOG_DEBRIEFING
	if (file) fclose(file);
#endif
}

Notepad::Notepad(ControlsContainer *parent, int idc, const ParamEntry &cls)
	: ControlObjectContainer(parent, idc, cls)
{
	_briefing = NULL;

	_paper.Init(_shape, "papir", NULL);
	_paper1 = GlobLoadTexture("data\\blok_stranka1.pac");
	_paper2 = GlobLoadTexture("data\\blok_stranka2.pac");
	_paper3 = GlobLoadTexture("data\\blok_stranka3.pac");
	_paper4 = GlobLoadTexture("data\\blok_stranka4.pac");
	_paper5 = GlobLoadTexture("data\\blok_stranka5.pac");
	_paper6 = GlobLoadTexture("data\\blok_stranka6_next.pac");
	_paper7 = GlobLoadTexture("data\\blok_stranka7_next.pac");
}

void Notepad::SetPosition(Vector3Par pos)
{
	ControlObjectContainer::SetPosition(pos);

	DisplayMap *parent = dynamic_cast<DisplayMap *>(_parent);
	if (parent) parent->AdjustMapVisibleRect();
}

void Notepad::Animate(int level)
{
	switch (_briefing->ActiveBookmark())
	{
		case 0:
			_paper.SetTexture(_shape, level, _paper1);
			break;
		case 1:
			_paper.SetTexture(_shape, level, _paper2);
			break;
		case 2:
			_paper.SetTexture(_shape, level, _paper3);
			break;
		case 3:
			_paper.SetTexture(_shape, level, _paper4);
			break;
		case 4:
			_paper.SetTexture(_shape, level, _paper6);
			break;
		case 5:
			_paper.SetTexture(_shape, level, _paper7);
			break;
		default:
			_paper.SetTexture(_shape, level, _paper5);
			break;
	}
}

void Notepad::Deanimate(int level)
{
	_paper.SetTexture(_shape, level, _paper5);
}

Compass::Compass(ControlsContainer *parent, int idc, const ParamEntry &cls)
	: ControlObjectWithZoom(parent, idc, cls)
{
/*!
\patch 1.01 Date 06/07/2001 by Ondra
- Fixed watch and compass animation with T&L
*/
	_shape->AllowAnimation();
	_pointer.Init(_shape, "kompas", NULL, "osa kompasu");
	_cover.Init(_shape, "vicko", NULL, "osa vicka");
}

Vector3 Compass::Center() const
{
	return _pointer.Center();
}

extern Camera OriginalCamera;

void Compass::Animate(int level)
{
	Vector3 dir = OriginalCamera.Direction();
	float angle = atan2(dir.X(), dir.Z());
	_pointer.Rotate(_shape, angle, level);
	_cover.Rotate(_shape, -0.45 * H_PI, level);
/*	
	Object *object = GWorld->CameraOn();
	if( object )
	{
		Vector3 dir = object->Direction();
		float angle = atan2(dir.X(), dir.Z());
		_pointer.Rotate(_shape, angle, level);
		_cover.Rotate(_shape, -0.45 * H_PI, level);
	}
*/
}

void Compass::Deanimate(int level)
{
	_pointer.Restore(_shape, level);
	_cover.Restore(_shape, level);
}

Watch::Watch(ControlsContainer *parent, int idc, const ParamEntry &cls)
	: ControlObjectWithZoom(parent, idc, cls)
{
/*!
\patch_internal 1.01 Date 06/07/2001 by Ondra
- Fixed: watch and compass animation with T&L
*/
	_shape->AllowAnimation();
	_hour.Init(_shape, "hodinova", NULL, "osa");
	_minute.Init(_shape, "minutova", NULL, "osa");
	_second.Init(_shape, "vterinova", NULL, "osa");

	_date1.Init(_shape, "date1");
	_date2.Init(_shape, "date2");
	_day.Init(_shape, "day");
}

void Watch::Animate(int level)
{
	int year = Glob.clock.GetYear();
	int dayOfYear = toIntFloor(Glob.clock.GetTimeInYear() * 365);
	float timeOfDay = Glob.clock.GetTimeOfDay();
	if (timeOfDay >= 1.0)
	{
		timeOfDay--;
		dayOfYear++;
	}
	Assert(timeOfDay >= 0 && timeOfDay < 1.0);
	Assert(dayOfYear >= 0 && dayOfYear < 365);
	struct tm tmDate = { 0, 0, 0, 0, 0, 0 };
	tmDate.tm_year = year - 1900;
	tmDate.tm_yday = dayOfYear;
	int m = 0;
	while (tmDate.tm_yday >= GetDaysInMonth(year, m))
	{
		tmDate.tm_yday -= GetDaysInMonth(year, m);
		m++;
	}
	tmDate.tm_mday = tmDate.tm_yday + 1;
	tmDate.tm_mon = m;
	mktime(&tmDate);
	Assert(tmDate.tm_yday == dayOfYear);
	
	int day = tmDate.tm_wday - 1;
	if (day < 0) day = 6;
	_day.UVOffset(_shape, 0, 0.1 * day, level);

	day = tmDate.tm_mday / 10 - 1;
	if (day < 0) day = 9;
	_date1.UVOffset(_shape, 0, 0.1 * day, level);

	day = tmDate.tm_mday % 10 - 1;
	if (day < 0) day = 9;
	_date2.UVOffset(_shape, 0, 0.1 * day, level);

	float angle = 4.0 * H_PI * timeOfDay;
	_hour.Rotate(_shape, angle, level);

	timeOfDay = fmod(24.0 * timeOfDay, 1.0);
	angle = 2.0 * H_PI * timeOfDay;
	_minute.Rotate(_shape, angle, level);

	timeOfDay = fmod(60.0 * timeOfDay, 1.0);
	int sec = toIntFloor(60.0 * timeOfDay);
	angle = 2.0 * H_PI * (1.0 / 60.0) * sec;
	_second.Rotate(_shape, angle, level);
}

void Watch::Deanimate(int level)
{
	_hour.Restore(_shape, level);
	_minute.Restore(_shape, level);
	_second.Restore(_shape, level);

	_date1.Restore(_shape, level);
	_date2.Restore(_shape, level);
	_day.Restore(_shape, level);
}

PackedBoolArray CUnitsSelector::GetArray() const
{
	return ListSelectedUnits();
}

///////////////////////////////////////////////////////////////////////////////
// creation of main display

AbstractOptionsUI *CreateMainMapUI()
{
	return new DisplayMainMap(NULL);
}

void DisplayMainMap::DestroyHUD(int exit)
{
	GLOB_WORLD->DestroyMap(exit);
}
