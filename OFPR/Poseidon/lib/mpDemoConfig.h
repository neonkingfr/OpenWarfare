// Configuration parameters for internal release / debug version

#define _VERIFY_KEY								0
#define _ENABLE_EDITOR						0
#define _ENABLE_ALL_MISSIONS			0
#define _ENABLE_MP								1
#define _ENABLE_AI								0
#define _ENABLE_CAMPAIGN					0
#define _ENABLE_SINGLE_MISSION		0
#define _ENABLE_BRIEF_EQ					1
#define _ENABLE_BRIEF_GRP					1
#define _ENABLE_PARAMS						0
#define _ENABLE_CHEATS						0
#define _ENABLE_ADDONS						0
#define _ENABLE_DEDICATED_SERVER	0
#define _ENABLE_GAMESPY						0
#define _FORCE_DEMO_ISLAND				1
#define _FORCE_SINGLE_VOICE				1
#define _ENABLE_AIRPLANES					0
#define _ENABLE_HELICOPTERS				0
#define _ENABLE_SHIPS							0
#define _ENABLE_CARS							1
#define _ENABLE_TANKS							1
#define _ENABLE_MOTORCYCLES				0
#define _ENABLE_PARACHUTES				0
#define _ENABLE_DATADISC					0
#define _ENABLE_VBS								0

#define _TIME_ACC_MIN							1.0
#define _TIME_ACC_MAX							1.0

// Registry key for saving player's name, IP address of server etc.
#define ConfigApp "Software\\Codemasters\\Operation Flashpoint MP Demo"

// Application name
#define AppName "Operation Flashpoint MP Demo"

// Preferences program
#define PreferencesExe	"OpFlashPreferencesMPDemo.exe"