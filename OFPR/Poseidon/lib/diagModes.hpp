#ifdef _MSC_VER
#pragma once
#endif

#ifndef _DIAG_MODES_HPP
#define _DIAG_MODES_HPP

#if _ENABLE_CHEATS

#include "El/Common/enumnames.hpp"

#define DIAG_ENABLE_ENUM(type,prefix,XX) \
	XX(type, prefix, CostMap) \
	XX(type, prefix, LockMap) \
	XX(type, prefix, Combat) \
	XX(type, prefix, Force) \
	XX(type, prefix, Animation) \
	XX(type, prefix, Dammage) \
	XX(type, prefix, Collision) \
	XX(type, prefix, Transparent) \
	XX(type, prefix, Sound) \
	XX(type, prefix, Path)

DECLARE_ENUM(DiagEnable,DE,DIAG_ENABLE_ENUM)

extern int DiagMode;

#define CHECK_DIAG(x) ( ::DiagMode&(1<<(x)) )

#endif

#endif

