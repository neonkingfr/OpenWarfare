#include "wpch.hpp"

#include "arcadeTemplate.hpp"
#include "ai.hpp"
#include <El/ParamFile/paramFile.hpp>
#include "paramArchive.hpp"
#include "landscape.hpp"
#include "global.hpp"
#include <El/Common/randomGen.hpp>

#include "resincl.hpp"
#include "uiControls.hpp"
#include "world.hpp"

#include "network.hpp"

#include <El/Common/enumNames.hpp>

#include "stringtableExt.hpp"
#include "chat.hpp"

static const EnumName TitleTypeNames[]=
{
	EnumName(TitleNone, "NONE"),
	EnumName(TitleObject, "OBJECT"),
	EnumName(TitleResource, "RES"),
	EnumName(TitleText, "TEXT"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(TitleType dummy)
{
	return TitleTypeNames;
}

static const EnumName ArcadeWaypointTypeNames[]=
{
	EnumName(ACUNDEFINED, "UNDEF"),
	EnumName(ACMOVE, "MOVE"),
	EnumName(ACDESTROY, "DESTROY"),
	EnumName(ACGETIN, "GETIN"),
	EnumName(ACSEEKANDDESTROY, "SAD"),
	EnumName(ACJOIN, "JOIN"),
	EnumName(ACLEADER, "LEADER"),
	EnumName(ACGETOUT, "GETOUT"),
	EnumName(ACCYCLE, "CYCLE"),
	EnumName(ACLOAD, "LOAD"),
	EnumName(ACUNLOAD, "UNLOAD"),
	EnumName(ACTRANSPORTUNLOAD, "TR UNLOAD"),
	EnumName(ACHOLD, "HOLD"),
	EnumName(ACSENTRY, "SENTRY"),
	EnumName(ACGUARD, "GUARD"),
	EnumName(ACTALK, "TALK"),
	EnumName(ACSCRIPTED, "SCRIPTED"),
	EnumName(ACSUPPORT, "SUPPORT"),
	EnumName(ACAND, "AND"),
	EnumName(ACOR, "OR"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(ArcadeWaypointType dummy)
{
	return ArcadeWaypointTypeNames;
}

static const EnumName SpeedModeNames[]=
{
	EnumName(SpeedUnchanged, "UNCHANGED"),
	EnumName(SpeedLimited, "LIMITED"),
	EnumName(SpeedNormal, "NORMAL"),
	EnumName(SpeedFull, "FULL"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(SpeedMode dummy)
{
	return SpeedModeNames;
}

static const EnumName CombatModeNames[]=
{
	EnumName(CMUnchanged, "UNCHANGED"),
	EnumName(CMCareless, "CARELESS"),
	EnumName(CMSafe, "SAFE"),
	EnumName(CMAware, "AWARE"),
	EnumName(CMCombat, "COMBAT"),
	EnumName(CMStealth, "STEALTH"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(CombatMode dummy)
{
	return CombatModeNames;
}

static const EnumName AWPShowNames[]=
{
	EnumName(ShowNever, "NEVER"),
	EnumName(ShowEasy, "EASY"),
	EnumName(ShowAlways, "ALWAYS"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(AWPShow dummy)
{
	return AWPShowNames;
}

static const EnumName ArcadeUnitSpecialNames[]=
{
	EnumName(ASpNone, "NONE"),
	EnumName(ASpCargo, "CARGO"),
	EnumName(ASpFlying, "FLY"),
	EnumName(ASpForm, "FORM"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(ArcadeUnitSpecial dummy)
{
	return ArcadeUnitSpecialNames;
}

static const EnumName ArcadeUnitAgeNames[]=
{
	EnumName(AAActual, "ACTUAL"),
	EnumName(AA5Min, "5 MIN"),
	EnumName(AA10Min, "10 MIN"),
	EnumName(AA15Min, "15 MIN"),
	EnumName(AA30Min, "30 MIN"),
	EnumName(AA60Min, "60 MIN"),
	EnumName(AA120Min, "120 MIN"),
	EnumName(AAUnknown, "UNKNOWN"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(ArcadeUnitAge dummy)
{
	return ArcadeUnitAgeNames;
}

static const EnumName ArcadeUnitPlayerNames[]=
{
	EnumName(APNonplayable, "NONPLAY"),
	EnumName(APPlayerCommander, "PLAYER COMMANDER"),
	EnumName(APPlayerDriver, "PLAYER DRIVER"),
	EnumName(APPlayerGunner, "PLAYER GUNNER"),
	EnumName(APPlayableC, "PLAY C"),
	EnumName(APPlayableD, "PLAY D"),
	EnumName(APPlayableG, "PLAY G"),
	EnumName(APPlayableCD, "PLAY CD"),
	EnumName(APPlayableCG, "PLAY CG"),
	EnumName(APPlayableDG, "PLAY DG"),
	EnumName(APPlayableCDG, "PLAY CDG"),
	// old versions
	EnumName(APPlayableCDG, "PLAY"),
	EnumName(APPlayerCommander, "P1 COMMANDER"),
	EnumName(APPlayerDriver, "P1 DRIVER"),
	EnumName(APPlayerGunner, "P1 GUNNER"),
	EnumName(APPlayerCommander, "P2 COMMANDER"),
	EnumName(APPlayerDriver, "P2 DRIVER"),
	EnumName(APPlayerGunner,"P2 GUNNER"),
	EnumName(APNonplayable, "-1"),
	EnumName(APPlayableCDG, "0"),
	EnumName(APPlayerCommander, "1"),
	EnumName(APPlayerCommander, "2"),
	EnumName(APNonplayable, "-1.000000"),
	EnumName(APPlayableCDG, "0.000000"),
	EnumName(APPlayerCommander, "1.000000"),
	EnumName(APPlayerCommander, "2.000000"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(ArcadeUnitPlayer dummy)
{
	return ArcadeUnitPlayerNames;
}

static const EnumName LockStateNames[]=
{
	EnumName(LSUnlocked, "UNLOCKED"),
	EnumName(LSDefault, "DEFAULT"),
	EnumName(LSLocked, "LOCKED"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(LockState dummy)
{
	return LockStateNames;
}

static const EnumName ArcadeSensorActivationNames[]=
{
	EnumName(ASANone, "NONE"),
	EnumName(ASAEast, "EAST"),
	EnumName(ASAWest, "WEST"),
	EnumName(ASAGuerrila, "GUER"),
	EnumName(ASACivilian, "CIV"),
	EnumName(ASALogic, "LOGIC"),
	EnumName(ASAAnybody, "ANY"),
	EnumName(ASAAlpha, "ALPHA"),
	EnumName(ASABravo, "BRAVO"),
	EnumName(ASACharlie, "CHARLIE"),
	EnumName(ASADelta, "DELTA"),
	EnumName(ASAEcho, "ECHO"),
	EnumName(ASAFoxtrot, "FOXTROT"),
	EnumName(ASAGolf, "GOLF"),
	EnumName(ASAHotel, "HOTEL"),
	EnumName(ASAIndia, "INDIA"),
	EnumName(ASAJuliet, "JULIET"),
	EnumName(ASAStatic, "STATIC"),
	EnumName(ASAVehicle, "VEHICLE"),
	EnumName(ASAGroup, "GROUP"),
	EnumName(ASALeader, "LEADER"),
	EnumName(ASAMember, "MEMBER"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(ArcadeSensorActivation dummy)
{
	return ArcadeSensorActivationNames;
}

static const EnumName ArcadeSensorActivationTypeNames[]=
{
	EnumName(ASATPresent, "PRESENT"),
	EnumName(ASATNotPresent, "NOT PRESENT"),
	EnumName(ASATWestDetected, "WEST D"),
	EnumName(ASATEastDetected, "EAST D"),
	EnumName(ASATGuerrilaDetected, "GUER D"),
	EnumName(ASATCiviliansDetected, "CIV D"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(ArcadeSensorActivationType dummy)
{
	return ArcadeSensorActivationTypeNames;
}

static const EnumName ArcadeSensorTypeNames[]=
{
	EnumName(ASTNone, "NONE"),
	EnumName(ASTEastGuarded, "EAST G"),
	EnumName(ASTWestGuarded, "WEST G"),
	EnumName(ASTGuerrilaGuarded, "GUER G"),
	EnumName(ASTSwitch, "SWITCH"),
	EnumName(ASTEnd1, "END1"),
	EnumName(ASTEnd2, "END2"),
	EnumName(ASTEnd3, "END3"),
	EnumName(ASTEnd4, "END4"),
	EnumName(ASTEnd5, "END5"),
	EnumName(ASTEnd6, "END6"),
	EnumName(ASTLoose, "LOOSE"),
	EnumName(ASTEnd1, "WIN"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(ArcadeSensorType dummy)
{
	return ArcadeSensorTypeNames;
}

static const EnumName MarkerTypeNames[]=
{
	EnumName(MTIcon, "ICON"),
	EnumName(MTRectangle, "RECTANGLE"),
	EnumName(MTEllipse, "ELLIPSE"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(MarkerType dummy)
{
	return MarkerTypeNames;
}

///////////////////////////////////////////////////////////////////////////////
// struct ArcadeUnitInfo

const float MinAbility=0.2; // private
const float MinRank=RankPrivate;
const float MaxAbility=1;
const float MaxRank=RankColonel;

float RankToSkill(int rank)
{
	float factor = (MaxAbility - MinAbility) / (MaxRank - MinRank);
	return (rank - MinRank) * factor + MinAbility;
}

ArcadeUnitInfo::ArcadeUnitInfo()
{
	Init();
}

ArcadeUnitInfo::ArcadeUnitInfo(const ArcadeUnitInfo &src)
{
	presence = src.presence;
	presenceCondition = src.presenceCondition;
	position = src.position;
	placement = src.placement;
	azimut = src.azimut;
	special = src.special;
	age = src.age;
	id = src.id;
	side = src.side;
	vehicle = src.vehicle;
	icon = src.icon;
	size = src.size;
	player = src.player;
	leader = src.leader;
	lock = src.lock;
	rank = src.rank;
	skill = src.skill;
	health = src.health;
	fuel = src.fuel;
	ammo = src.ammo;
	name = src.name;
	markers = src.markers;
	selected = src.selected;
	init = src.init;
}

/*!
\patch 1.30 Date 11/03/2001 by Jirka
- Changed: Default skill of units inserted in editor is now half skilled,
not minimum skill as before.
*/

void ArcadeUnitInfo::Init()
{
	presence = 1.0;
	presenceCondition = "true";
	position = VZero;
	placement = 0;
	azimut = 0;
	special = ASpForm;
	age = AAUnknown;
	id = 0;
	side = TWest;
	vehicle = "";
	type = NULL;
	icon = NULL;
	size = 0;
	player = APNonplayable;
	leader = 0;
	lock = LSDefault;
	rank = RankPrivate;
	skill = (MaxAbility + MinAbility)*0.5f;
	health = 1.0;
	fuel = 1.0;
	ammo = 1.0;
	name = "";
	markers.Clear();
	selected = false;
	init = "";
}

void ArcadeUnitInfo::AddOffset(Vector3Par offset)
{
	position += offset;
	position[1] = GLOB_LAND->RoadSurfaceYAboveWater
	(
		position[0], position[2]
	);
}

void ArcadeUnitInfo::Rotate(Vector3Par center, float angle, bool sel)
{
	if (sel && !selected) return;

	// rotation
	azimut += (180.0 / H_PI) * angle;

	Vector3 dir = position - center;
	Matrix3 rot(MRotationY, -angle);
	dir = rot * dir;

	position = center + dir;
	position[1] = GLandscape->RoadSurfaceYAboveWater
	(
		position[0], position[2]
	);
}

void ArcadeUnitInfo::CalculateCenter(Vector3 &sum, int &count, bool sel)
{
	if (sel && !selected) return;
	
	sum += position;
	count++;
}

/*!
\patch 1.63 Date 6/2/2002 by Ondra
- Changed: Addon requirements are now determined by addon entry creator,
not only by CfgPatches config section. This makes addon list more reliable
with addons that ommited to list some added types.
*/

// ADDED in patch 1.01 - AddOns check
void ArcadeUnitInfo::RequiredAddons(FindArrayRStringCI &addOns)
{
	const ParamEntry *patches = Pars.FindEntry("CfgPatches");
	if (!patches) return;
	for (int i=0; i<patches->GetEntryCount(); i++)
	{
		const ParamEntry &patch = patches->GetEntry(i);
		for (int j=0; j<(patch >> "units").GetSize(); j++)
		{
			RStringB patchVehicle = (patch >> "units")[j];
			if (stricmp(patchVehicle, vehicle) == 0)
			{
				addOns.AddUnique(patch.GetName());
				goto Break;
			}
		}
	}
	Break:
	// more robust check - check owner of given unit type
	const ParamEntry *entry = (Pars>>"CfgVehicles").FindEntry(vehicle);
	if (entry)
	{
		const RStringB &owner = entry->GetOwner();
		if (owner.GetLength()>0)
		{
			addOns.AddUnique(owner);
		}
	}
}

LSError ArcadeUnitInfo::Serialize(ParamArchive &ar)
{
	if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst) Init();

	CHECK(ar.Serialize("presence", presence, 1, 1.0))	
	CHECK(ar.Serialize("presenceCondition", presenceCondition, 1, "true"))	
	CHECK(ar.Serialize("position", position, 1))	
	CHECK(ar.Serialize("placement", placement, 1, 0))	
	CHECK(ar.Serialize("azimut", azimut, 1, 0))	

	CHECK(ar.SerializeEnum("special", special, 1, ASpForm))	
	CHECK(ar.SerializeEnum("age", age, 1, AAUnknown))	
	CHECK(ar.Serialize("id", id, 1))	
	CHECK(ar.SerializeEnum("side", side, 1))	

	CHECK(ar.Serialize("vehicle", vehicle, 1))	
	CHECK(ar.SerializeEnum("player", player, 1, APNonplayable))	
	CHECK(ar.Serialize("leader", leader, 1, 0))	
	if (ar.IsSaving() || ar.GetArVersion() >= 11)
		CHECK(ar.SerializeEnum("lock", lock, 1, (LockState)LSDefault))	
	else
	{
		bool locked;
		CHECK(ar.Serialize("locked", locked, 7, false))	
			lock = locked ? LSLocked : LSDefault;
	}
	CHECK(ar.SerializeEnum("rank", rank, 1, RankPrivate))	
	CHECK(ar.Serialize("skill", skill, 1, -1.0))	
	CHECK(ar.Serialize("health", health, 1, 1.0))	
	CHECK(ar.Serialize("fuel", fuel, 1, 1.0))	
	CHECK(ar.Serialize("ammo", ammo, 1, 1.0))	
	CHECK(ar.Serialize("text", name, 7, ""))	
	CHECK(ar.SerializeArray("markers", markers, 1))	
	CHECK(ar.Serialize("init", init, 7, ""))	

	if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
	{
		RString iconName = Pars >> "CfgVehicles" >> vehicle >> "icon";
		icon = GlobLoadTexture(GetPictureName(iconName));
		size = Pars >> "CfgVehicles" >> vehicle >> "mapSize";

		ATSParams *params = (ATSParams *)ar.GetParams();
		Assert(params);
		Assert(id >= 0);
//		id += params->baseVeh;
		saturateMax(params->nextVehId, id + 1);
//		if (params->merge) player = APNonplayable;

		if (skill < 0) skill = RankToSkill(rank);
	}

	return LSOK;
}

///////////////////////////////////////////////////////////////////////////////
// struct ArcadeSensorInfo

ArcadeSensorInfo::ArcadeSensorInfo()
{
	Init();
}

ArcadeSensorInfo::ArcadeSensorInfo(const ArcadeSensorInfo &src)
{
	position = src.position;
	a = src.a;
	b = src.b;
	angle = src.angle;
	rectangular = src.rectangular;
	activationBy = src.activationBy;
	activationType = src.activationType;
	repeating = src.repeating;
	timeoutMin = src.timeoutMin;
	timeoutMid = src.timeoutMid;
	timeoutMax = src.timeoutMax;
	interruptable = src.interruptable;
	type = src.type;
	object = src.object;
	age = src.age;
	idStatic = src.idStatic;
	idVehicle = src.idVehicle;
	text = src.text;
	name = src.name;
	expCond = src.expCond;
	expActiv = src.expActiv;
	expDesactiv = src.expDesactiv;
	effects = src.effects;
	synchronizations = src.synchronizations;
	selected = src.selected;
}

void ArcadeSensorInfo::Init()
{
	position = VZero;
	a = 50.0;
	b = 50.0;
	angle = 0;
	rectangular = false;
	activationBy = ASANone;
	activationType = ASATPresent;
	repeating = false;
	timeoutMin = 0;
	timeoutMid = 0;
	timeoutMax = 0;
	interruptable = false;
	type = ASTNone;
	object = "EmptyDetector";
	age = AAUnknown;
	idStatic = -1;
	idVehicle = -1;
	text = "";
	name = "";
	expCond = "this";
	expActiv = "";
	expDesactiv = "";
	effects.Init();
	synchronizations.Clear();
	selected = false;
}

/*
void ArcadeSensorInfo::FromNet(const SENSOR_INFO &msg)
{
	position[0] = msg.position[0];
	position[1] = msg.position[1];
	position[2] = msg.position[2];
	a = msg.a;
	b = msg.b;
	angle = msg.angle;
	rectangular = msg.rectangular;
	activationBy = (ArcadeSensorActivation)msg.activationBy;
	activationType = (ArcadeSensorActivationType)msg.activationType;
	repeating = msg.repeating;
	timeoutMin = msg.timeoutMin;
	timeoutMid = msg.timeoutMid;
	timeoutMax = msg.timeoutMax;
	interruptable = msg.interruptable;
	type = (ArcadeSensorType)msg.type;
	object = msg.object;
	age = (ArcadeUnitAge)msg.age;
	idStatic = msg.idStatic;
	idVehicle = msg.idVehicle;
	text = msg.text;
	name = msg.name;
	expCond = msg.expCond;
	expActiv = msg.expActiv;
	expDesactiv = msg.expDesactiv;
	effects.FromNet(msg.effects);
	// TODO: synchronizations
}

void ArcadeSensorInfo::ToNet(SENSOR_INFO &msg) const
{
	msg.position[0] = position[0];
	msg.position[1] = position[1];
	msg.position[2] = position[2];
	msg.a = a;
	msg.b = b;
	msg.angle = angle;
	msg.rectangular = rectangular;
	msg.activationBy = activationBy;
	msg.activationType = activationType;
	msg.repeating = repeating;
	msg.timeoutMin = timeoutMin;
	msg.timeoutMid = timeoutMid;
	msg.timeoutMax = timeoutMax;
	msg.interruptable = interruptable;
	msg.type = type;
	strncpy
	(
		msg.object, object, sizeof(msg.object)
	);
	msg.age = age;
	msg.idStatic = idStatic;
	msg.idVehicle = idVehicle;
	strncpy
	(
		msg.text, text, sizeof(msg.text)
	);
	strncpy
	(
		msg.name, name, sizeof(msg.name)
	);
	strncpy
	(
		msg.expCond, expCond, sizeof(msg.expCond)
	);
	strncpy
	(
		msg.expActiv, expActiv, sizeof(msg.expActiv)
	);
	strncpy
	(
		msg.expDesactiv, expDesactiv, sizeof(msg.expDesactiv)
	);
	effects.ToNet(msg.effects);
	// TODO: synchronizations
}
*/

void ArcadeSensorInfo::AddOffset(Vector3Par offset)
{
	position += offset;
	position[1] = GLOB_LAND->RoadSurfaceYAboveWater
	(
		position[0], position[2]
	);
}

void ArcadeSensorInfo::Rotate(Vector3Par center, float alpha, bool sel)
{
	if (sel && !selected) return;

	// rotation
	angle += (180.0 / H_PI) * alpha;

	Vector3 dir = position - center;
	Matrix3 rot(MRotationY, -alpha);
	dir = rot * dir;

	position = center + dir;
	position[1] = GLandscape->RoadSurfaceYAboveWater
	(
		position[0], position[2]
	);
}

void ArcadeSensorInfo::CalculateCenter(Vector3 &sum, int &count, bool sel)
{
	if (sel && !selected) return;
	
	sum += position;
	count++;
}

LSError ArcadeSensorInfo::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("position", position, 1))	
	CHECK(ar.Serialize("a", a, 1, 50.0))	
	CHECK(ar.Serialize("b", b, 1, 50.0))	
	CHECK(ar.Serialize("angle", angle, 1, 0))	
	CHECK(ar.Serialize("rectangular", rectangular, 7, false))	

	CHECK(ar.SerializeEnum("activationBy", activationBy, 1, ASANone))	
	CHECK(ar.SerializeEnum("activationType", activationType, 1, ASATPresent))	
	CHECK(ar.Serialize("repeating", repeating, 1, 0))	
	CHECK(ar.Serialize("timeoutMin", timeoutMin, 1, 0))	
	CHECK(ar.Serialize("timeoutMid", timeoutMid, 1, 0))	
	CHECK(ar.Serialize("timeoutMax", timeoutMax, 1, 0))	
	CHECK(ar.Serialize("interruptable", interruptable, 1, 0))	
	CHECK(ar.SerializeEnum("type", type, 1, ASTNone))	
	CHECK(ar.Serialize("object", object, 1, "EmptyDetector"))	
	CHECK(ar.SerializeEnum("age", age, 1))	

	CHECK(ar.Serialize("idStatic", idStatic, 1, -1))	
	CHECK(ar.Serialize("idVehicle", idVehicle, 1, -1))	

	CHECK(ar.Serialize("text", text, 3, ""))	
	CHECK(ar.Serialize("name", name, 7, ""))	

	// if value not present, use default - possible in all versions
	CHECK(ar.Serialize("expCond", expCond, 1, "this"))	
	CHECK(ar.Serialize("expActiv", expActiv, 1, ""))	
	CHECK(ar.Serialize("expDesactiv", expDesactiv, 1, ""))	

	CHECK(ar.Serialize("Effects", effects, 1))	

	CHECK(ar.SerializeArray("synchronizations", synchronizations, 1))	

	if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
	{
		ATSParams *params = (ATSParams *)ar.GetParams();
		Assert(params);
		for (int i=0; i<synchronizations.Size(); i++)
		{
			int &sync = synchronizations[i];
			Assert(sync >= 0);
			saturateMax(params->nextSyncId, sync + 1);
		}
	}

	return LSOK;
}

///////////////////////////////////////////////////////////////////////////////
// struct ArcadeMarkerInfo

ArcadeMarkerInfo::ArcadeMarkerInfo()
{
	Init();
}

ArcadeMarkerInfo::ArcadeMarkerInfo(const ArcadeMarkerInfo &src)
{
	position = src.position;
	name = src.name;
	text = src.text;
	markerType = src.markerType;
	type = src.type;
	colorName = src.colorName;
	color = src.color;
	fillName = src.fillName;
	fill = src.fill;
	icon = src.icon;
	size = src.size;
	a = src.a;
	b = src.b;
	angle = src.angle;
	selected = src.selected;
}

void ArcadeMarkerInfo::Init()
{
	position = VZero;
	name = "";
	text = "";
	markerType = MTIcon;
	type = "";
	colorName = "Default";
	color = PackedBlack;
	fillName = "Solid";
	fill = NULL;
	icon = NULL;
	size = 24;
	a = 1;
	b = 1;
	angle = 0;
	selected = false;
}

void ArcadeMarkerInfo::AddOffset(Vector3Par offset)
{
	position += offset;
	position[1] = GLOB_LAND->RoadSurfaceYAboveWater
	(
		position[0], position[2]
	);
}

void ArcadeMarkerInfo::Rotate(Vector3Par center, float alpha, bool sel)
{
	if (sel && !selected) return;

	// rotation
	angle += (180.0 / H_PI) * alpha;

	Vector3 dir = position - center;
	Matrix3 rot(MRotationY, -alpha);
	dir = rot * dir;

	position = center + dir;
	position[1] = GLandscape->RoadSurfaceYAboveWater
	(
		position[0], position[2]
	);
}

void ArcadeMarkerInfo::CalculateCenter(Vector3 &sum, int &count, bool sel)
{
	if (sel && !selected) return;
	
	sum += position;
	count++;
}

LSError ArcadeMarkerInfo::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("position", position, 1))	
	CHECK(ar.Serialize("name", name, 1))	
	CHECK(ar.Serialize("text", text, 1, ""))	
	CHECK(ar.SerializeEnum("markerType", markerType, 1, MTIcon))
	CHECK(ar.Serialize("type", type, 1))	
	CHECK(ar.Serialize("colorName", colorName, 1, "Default"))	
	CHECK(ar.Serialize("fillName", fillName, 1, "Solid"))	
	CHECK(ar.Serialize("a", a, 1, 1.0f))	
	CHECK(ar.Serialize("b", b, 1, 1.0f))	
	CHECK(ar.Serialize("angle", angle, 1, 0.0f))	
	if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
	{
		OnTypeChanged();
		OnColorChanged();
		OnFillChanged();
	}

	return LSOK;
}

class IndicesMarker
{
public:
	int position;
	int name;
	int text;
	int markerType;
	int type;
	int colorName;
	int fillName;
	int a;
	int b;
	int angle;

	IndicesMarker();
	void Scan(NetworkMessageFormatBase *format);
};

IndicesMarker::IndicesMarker()
{
	position = -1;
	name = -1;
	text = -1;
	markerType = -1;
	type = -1;
	colorName = -1;
	fillName = -1;
	a = -1;
	b = -1;
	angle = -1;
}

void IndicesMarker::Scan(NetworkMessageFormatBase *format)
{
	SCAN(position)
	SCAN(name)
	SCAN(text)
	SCAN(markerType)
	SCAN(type)
	SCAN(colorName)
	SCAN(fillName)
	SCAN(a)
	SCAN(b)
	SCAN(angle)
}

IndicesMarker *GetIndicesMarker() {return new IndicesMarker();}
void DeleteIndicesMarker(IndicesMarker *marker) {delete marker;}

void ScanIndicesMarker(IndicesMarker *marker, NetworkMessageFormatBase *format)
{
	marker->Scan(format);
}

void ArcadeMarkerInfo::CreateFormat
(
	NetworkMessageFormat &format
)
{
	format.Add("position", NDTVector, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Marker position"));
	format.Add("name", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Marker (unique) name"));
	format.Add("text", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Marker title"));
	format.Add("markerType", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, MTIcon), DOC_MSG("Marker type (icon, rectangle, ellipse)"));
	format.Add("type", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Marker icon"));
	format.Add("colorName", NDTString, NCTNone, DEFVALUE(RString, "Default"), DOC_MSG("Marker color name"));
	format.Add("fillName", NDTString, NCTNone, DEFVALUE(RString, "Solid"), DOC_MSG("Marker fill name"));
	format.Add("a", NDTFloat, NCTNone, DEFVALUE(float, 1.0f), DOC_MSG("Width"));
	format.Add("b", NDTFloat, NCTNone, DEFVALUE(float, 1.0f), DOC_MSG("Height"));
	format.Add("angle", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Rotation"));
}

TMError ArcadeMarkerInfo::TransferMsg(NetworkMessageContext &ctx, IndicesMarker *indices)
{
	TMCHECK(ctx.IdxTransfer(indices->position, position))
	TMCHECK(ctx.IdxTransfer(indices->name, name))
	TMCHECK(ctx.IdxTransfer(indices->text, text))
	TMCHECK(ctx.IdxTransfer(indices->markerType, (int &)markerType))
	TMCHECK(ctx.IdxTransfer(indices->type, type))
	TMCHECK(ctx.IdxTransfer(indices->colorName, colorName))
	TMCHECK(ctx.IdxTransfer(indices->fillName, fillName))
	TMCHECK(ctx.IdxTransfer(indices->a, a))
	TMCHECK(ctx.IdxTransfer(indices->b, b))
	TMCHECK(ctx.IdxTransfer(indices->angle, angle))
	if (!ctx.IsSending())
	{
		OnTypeChanged();
		OnColorChanged();
		OnFillChanged();
	}
	return TMOK;
}

void ArcadeMarkerInfo::OnColorChanged()
{
	if (markerType == MTIcon && stricmp(colorName, "Default") == 0)
	{
		const ParamEntry &cls = Pars >> "CfgMarkers" >> type;
		color = GetPackedColor(cls >> "color");
	}
	else
	{
		const ParamEntry &cls = Pars >> "CfgMarkerColors" >> colorName;
		color = GetPackedColor(cls >> "color");
	}
}

void ArcadeMarkerInfo::OnFillChanged()
{
	const ParamEntry &cls = Pars >> "CfgMarkerBrushes" >> fillName;
	RString brush = cls >> "texture";
	if (brush.GetLength() == 0)
		fill = NULL;
	else
		fill = GlobLoadTexture(GetPictureName(brush));
}

void ArcadeMarkerInfo::OnTypeChanged()
{
	if (markerType == MTIcon)
	{
		const ParamEntry &cls = Pars >> "CfgMarkers" >> type;
		icon = GlobLoadTexture
		(
			GetPictureName(cls >> "icon")
		);
		size = cls >> "size";
	}
}

///////////////////////////////////////////////////////////////////////////////
// struct ArcadeEffects

ArcadeEffects::ArcadeEffects()
{
	Init();
}

ArcadeEffects::ArcadeEffects(const ArcadeEffects &src)
{
	condition = src.condition;
	cameraEffect = src.cameraEffect;
	cameraPosition = src.cameraPosition;
	sound = src.sound;
	voice = src.voice;
	soundEnv = src.soundEnv;
	soundDet = src.soundDet;
	track = src.track;
	titleType = src.titleType;
	titleEffect = src.titleEffect;
	title = src.title;
}

void ArcadeEffects::Init()
{
	condition = "true";
	cameraEffect = "";
	cameraPosition = CamEffectBack;
	sound = "$NONE$";
	voice = "";
	soundEnv = "";
	soundDet = "";
	track = "$NONE$";
	titleType = TitleNone;
	titleEffect = TitPlain;
	title = "";
}

LSError ArcadeEffects::Serialize(ParamArchive &ar)
{
	if (ar.IsSaving() || ar.GetArVersion() >= 9)
		CHECK(ar.Serialize("condition", condition, 9, "true"))
	else
	{
		bool playerOnly;
		CHECK(ar.Serialize("playerOnly", playerOnly, 1, false))	
		if (playerOnly)
			condition = "thisList";
		else
			condition = "true";
	}

	CHECK(ar.Serialize("cameraEffect", cameraEffect, 1, ""))	
	CHECK(ar.SerializeEnum("cameraPosition", cameraPosition, 1, (CamEffectPosition)CamEffectBack))	
	
	CHECK(ar.Serialize("sound", sound, 1, "$NONE$"))
	CHECK(ar.Serialize("voice", voice, 5, ""))
	CHECK(ar.Serialize("soundEnv", soundEnv, 5, ""))
	CHECK(ar.Serialize("soundDet", soundDet, 5, ""))
	CHECK(ar.Serialize("track", track, 1, "$NONE$"))

	CHECK(ar.SerializeEnum("titleType", titleType, 1, TitleNone))	
	CHECK(ar.SerializeEnum("titleEffect", titleEffect, 1, (TitEffectName)TitPlain))
	CHECK(ar.Serialize("title", title, 1, ""))	

	return LSOK;
}

LSError ArcadeEffects::WorldSerialize(ParamArchive &ar)
{
	if (ar.IsSaving() || ar.GetArVersion() >= 5)
		CHECK(ar.Serialize("condition", condition, 5, "true"))
	else
	{
		bool playerOnly;
		CHECK(ar.Serialize("playerOnly", playerOnly, 1, false))	
		if (playerOnly)
			condition = "thisList";
		else
			condition = "true";
	}

	CHECK(ar.Serialize("cameraEffect", cameraEffect, 1, ""))	
	CHECK(ar.SerializeEnum("cameraPosition", cameraPosition, 1, (CamEffectPosition)CamEffectBack))	
	
	CHECK(ar.Serialize("sound", sound, 1, "$NONE$"))
	CHECK(ar.Serialize("voice", voice, 1, ""))
	CHECK(ar.Serialize("soundEnv", soundEnv, 1, ""))
	CHECK(ar.Serialize("soundDet", soundDet, 1, ""))
	CHECK(ar.Serialize("track", track, 1, "$NONE$"))

	CHECK(ar.SerializeEnum("titleType", titleType, 1, TitleNone))	
	CHECK(ar.SerializeEnum("titleEffect", titleEffect, 1, (TitEffectName)TitPlain))
	CHECK(ar.Serialize("title", title, 1, ""))	

	return LSOK;
}

class IndicesEffects
{
public:
	int condition;
	int cameraEffect;
	int cameraPosition;
	int sound;
	int voice;
	int soundEnv;
	int soundDet;
	int track;
	int titleType;
	int titleEffect;
	int title;

	IndicesEffects();
	void Scan(NetworkMessageFormatBase *format);
};

IndicesEffects::IndicesEffects()
{
	condition = -1;
	cameraEffect = -1;
	cameraPosition = -1;
	sound = -1;
	voice = -1;
	soundEnv = -1;
	soundDet = -1;
	track = -1;
	titleType = -1;
	titleEffect = -1;
	title = -1;
}

void IndicesEffects::Scan(NetworkMessageFormatBase *format)
{
	SCAN(condition)
	SCAN(cameraEffect)
	SCAN(cameraPosition)
	SCAN(sound)
	SCAN(voice)
	SCAN(soundEnv)
	SCAN(soundDet)
	SCAN(track)
	SCAN(titleType)
	SCAN(titleEffect)
	SCAN(title)
}

IndicesEffects *GetIndicesEffects() {return new IndicesEffects();}
void DeleteIndicesEffects(IndicesEffects *effects) {delete effects;}

void ScanIndicesEffects(IndicesEffects *effects, NetworkMessageFormatBase *format)
{
	effects->Scan(format);
}

void ArcadeEffects::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("condition", NDTString, NCTNone, DEFVALUE(RString, "true"), DOC_MSG("Condition when effect is performed"));
	format.Add("cameraEffect", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Camera effect name"));
	format.Add("cameraPosition", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, CamEffectBack), DOC_MSG("Camera effect position"));
	format.Add("sound", NDTString, NCTNone, DEFVALUE(RString, "$NONE$"), DOC_MSG("Sound effect (2D)"));
	format.Add("voice", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Sound effect (3D)"));
	format.Add("soundEnv", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Enviromental sound effect"));
	format.Add("soundDet", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Detector sound effect"));
	format.Add("track", NDTString, NCTNone, DEFVALUE(RString, "$NONE$"), DOC_MSG("Musical track"));
	format.Add("titleType", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, TitleNone), DOC_MSG("Type of title effect (text, object, resource)"));
	format.Add("titleEffect", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, TitPlain), DOC_MSG("Type (placement) of text title effect"));
	format.Add("title", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Content of title effect"));
}

TMError ArcadeEffects::TransferMsg(NetworkMessageContext &ctx, IndicesEffects *indices)
{
	TMCHECK(ctx.IdxTransfer(indices->condition, condition));
	TMCHECK(ctx.IdxTransfer(indices->cameraEffect, cameraEffect));
	TMCHECK(ctx.IdxTransfer(indices->cameraPosition, (int &)cameraPosition));
	TMCHECK(ctx.IdxTransfer(indices->sound, sound));
	TMCHECK(ctx.IdxTransfer(indices->voice, voice));
	TMCHECK(ctx.IdxTransfer(indices->soundEnv, soundEnv));
	TMCHECK(ctx.IdxTransfer(indices->soundDet, soundDet));
	TMCHECK(ctx.IdxTransfer(indices->track, track));
	TMCHECK(ctx.IdxTransfer(indices->titleType, (int &)titleType));
	TMCHECK(ctx.IdxTransfer(indices->titleEffect, (int &)titleEffect));
	TMCHECK(ctx.IdxTransfer(indices->title, title));
	return TMOK;
}

///////////////////////////////////////////////////////////////////////////////
// struct ArcadeWaypointInfo

ArcadeWaypointInfo::ArcadeWaypointInfo()
{
	Init();
}

ArcadeWaypointInfo::ArcadeWaypointInfo(const ArcadeWaypointInfo &src)
{
	position = src.position;
	placement = src.placement;
	id = src.id;
	idStatic = src.idStatic;
	housePos = src.housePos;
	speed = src.speed;
	combat = src.combat;
	type = src.type;
	timeoutMin = src.timeoutMin;
	timeoutMid = src.timeoutMid;
	timeoutMax = src.timeoutMax;
	combatMode = src.combatMode;
	formation = src.formation;
	description = src.description;
	expCond = src.expCond;
	expActiv = src.expActiv;
	script = src.script;
	showWP = src.showWP;
	synchronizations = src.synchronizations;
	effects = src.effects;
	selected = src.selected;
}

void ArcadeWaypointInfo::Init()
{
	position = VZero;
	placement = 0;
	id = -1;
	idStatic = -1;
	housePos = -1;
	speed = SpeedUnchanged;
	combat = CMUnchanged;
	type = ACUNDEFINED;
	timeoutMin = 0;
	timeoutMid = 0;
	timeoutMax = 0;
	combatMode = (AI::Semaphore)-1; // TODO: AI::SemaphoreNoChange
	formation = (AI::Formation)-1; // TODO: AI::SemaphoreNoChange
	description = "";
	expCond = "true";
	expActiv = "";
	script = "";
	showWP = ShowNever;
	synchronizations.Clear();
	effects.Init();
	selected = false;
}

void ArcadeWaypointInfo::AddOffset(Vector3Par offset)
{
	position += offset;
	position[1] = GLOB_LAND->RoadSurfaceYAboveWater
	(
		position[0], position[2]
	);
}

void ArcadeWaypointInfo::Rotate(Vector3Par center, float angle, bool sel)
{
	if (sel && !selected) return;

	// rotation

	Vector3 dir = position - center;
	Matrix3 rot(MRotationY, -angle);
	dir = rot * dir;

	position = center + dir;
	position[1] = GLandscape->RoadSurfaceYAboveWater
	(
		position[0], position[2]
	);
}

void ArcadeWaypointInfo::CalculateCenter(Vector3 &sum, int &count, bool sel)
{
	if (sel && !selected) return;
	
	sum += position;
	count++;
}

LSError ArcadeWaypointInfo::Serialize(ParamArchive &ar)
{
	if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst) Init();

	CHECK(ar.Serialize("position", position, 1))	
	CHECK(ar.Serialize("placement", placement, 1, 0))	
	CHECK(ar.Serialize("id", id, 1, -1))	
	CHECK(ar.Serialize("idStatic", idStatic, 1, -1))	
	CHECK(ar.Serialize("housePos", housePos, 2, -1))	
	CHECK(ar.SerializeEnum("type", type, 1, ACMOVE))	
	CHECK(ar.SerializeEnum("combatMode", combatMode, 1, (AI::Semaphore)-1))	
	CHECK(ar.SerializeEnum("formation", formation, 1, (AI::Formation)-1))	
	CHECK(ar.SerializeEnum("speed", speed, 1, SpeedUnchanged))	
	CHECK(ar.SerializeEnum("combat", combat, 4, CMUnchanged))	
	CHECK(ar.Serialize("description", description, 1, ""))	
	CHECK(ar.Serialize("expCond", expCond, 7, "true"))	
	CHECK(ar.Serialize("expActiv", expActiv, 7, ""))	
	CHECK(ar.Serialize("script", script, 7, ""))	
	CHECK(ar.SerializeArray("synchronizations", synchronizations, 1))	
	CHECK(ar.Serialize("Effects", effects, 1))	
	CHECK(ar.Serialize("timeoutMin", timeoutMin, 1, 0))	
	CHECK(ar.Serialize("timeoutMid", timeoutMid, 1, 0))	
	CHECK(ar.Serialize("timeoutMax", timeoutMax, 1, 0))	
	if (ar.GetArVersion() >= 10)
	{
		CHECK(ar.SerializeEnum("showWP", showWP, 1, ShowEasy))
	}
	else
	{
		bool show = showWP != ShowNever;
		CHECK(ar.Serialize("show", show, 1, false))
		if (show) showWP = ShowEasy;
		else showWP = ShowNever;
	}

	if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
	{
		ATSParams *params = (ATSParams *)ar.GetParams();
		Assert(params);
		for (int i=0; i<synchronizations.Size(); i++)
		{
			int &sync = synchronizations[i];
			Assert(sync >= 0);
			saturateMax(params->nextSyncId, sync + 1);
		}
	}
	return LSOK;
}

LSError WaypointInfo::Serialize(ParamArchive &ar)
{
	// world serialization
	if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst) Init();

	CHECK(ar.Serialize("position", position, 1))	
	CHECK(ar.Serialize("placement", placement, 1, 0))	
	CHECK(ar.Serialize("id", id, 1, -1))	
	CHECK(ar.Serialize("idStatic", idStatic, 1, -1))	
	CHECK(ar.Serialize("housePos", housePos, 1, -1))	
	CHECK(ar.SerializeEnum("type", type, 1, ACMOVE))	
	CHECK(ar.SerializeEnum("combatMode", combatMode, 1, (AI::Semaphore)-1))	
	CHECK(ar.SerializeEnum("formation", formation, 1, (AI::Formation)-1))	
	CHECK(ar.SerializeEnum("speed", speed, 1, SpeedUnchanged))	
	CHECK(ar.SerializeEnum("combat", combat, 1, CMUnchanged))	
	CHECK(ar.Serialize("description", description, 1, ""))	
	CHECK(ar.Serialize("expCond", expCond, 1, "true"))	
	CHECK(ar.Serialize("expActiv", expActiv, 1, ""))	
	CHECK(ar.Serialize("script", script, 1, ""))	
	CHECK(ar.SerializeArray("synchronizations", synchronizations, 1))	
	ParamArchive arSubcls;
	if (!ar.OpenSubclass("Effects", arSubcls)) return LSStructure;
	effects.WorldSerialize(arSubcls);
	CHECK(ar.Serialize("timeoutMin", timeoutMin, 1, 0))	
	CHECK(ar.Serialize("timeoutMid", timeoutMid, 1, 0))	
	CHECK(ar.Serialize("timeoutMax", timeoutMax, 1, 0))	
	if (ar.GetArVersion() >= 9)
	{
		CHECK(ar.SerializeEnum("showWP", showWP, 1, ShowEasy))
	}
	else
	{
		bool show = showWP != ShowNever;
		CHECK(ar.Serialize("show", show, 1, false))
		if (show) showWP = ShowEasy;
		else showWP = ShowNever;
	}
	return LSOK;
}

class IndicesWaypoint : public NetworkMessageIndices
{
public:
	int position;
	int placement;
	int id;
	int idStatic;
	int housePos;
	int type;
	int combatMode;
	int formation;
	int speed;
	int combat;
	int timeoutMin;
	int timeoutMid;
	int timeoutMax;
	int description;
	int expCond;
	int expActiv;
	int script;
	int showWP;
	int synchronizations;
	IndicesEffects *effects;

	IndicesWaypoint();
	~IndicesWaypoint();
	NetworkMessageIndices *Clone() const {return new IndicesWaypoint;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesWaypoint::IndicesWaypoint()
{
	position = -1;
	placement = -1;
	id = -1;
	idStatic = -1;
	housePos = -1;
	type = -1;
	combatMode = -1;
	formation = -1;
	speed = -1;
	combat = -1;
	timeoutMin = -1;
	timeoutMid = -1;
	timeoutMax = -1;
	description = -1;
	expCond = -1;
	expActiv = -1;
	script = -1;
	showWP = -1;
	synchronizations = -1;

	effects = GetIndicesEffects();
}

IndicesWaypoint::~IndicesWaypoint()
{
	DeleteIndicesEffects(effects);
}

void IndicesWaypoint::Scan(NetworkMessageFormatBase *format)
{
	SCAN(position)
	SCAN(placement)
	SCAN(id)
	SCAN(idStatic)
	SCAN(housePos)
	SCAN(type)
	SCAN(combatMode)
	SCAN(formation)
	SCAN(speed)
	SCAN(combat)
	SCAN(timeoutMin)
	SCAN(timeoutMid)
	SCAN(timeoutMax)
	SCAN(description)
	SCAN(expCond)
	SCAN(expActiv)
	SCAN(script)
	SCAN(showWP)
	SCAN(synchronizations)

	ScanIndicesEffects(effects, format);
}

NetworkMessageIndices *GetIndicesWaypoint() {return new IndicesWaypoint();}

NetworkMessageFormat &ArcadeWaypointInfo::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("position", NDTVector, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Waypoint position"));
	format.Add("placement", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Radius for random placement of waypoint"));
	format.Add("id", NDTInteger, NCTSmallSigned, DEFVALUE(int, -1), DOC_MSG("ID of attached vehicle"));
	format.Add("idStatic", NDTInteger, NCTSmallSigned, DEFVALUE(int, -1), DOC_MSG("ID of attached static object"));
	format.Add("housePos", NDTInteger, NCTSmallSigned, DEFVALUE(int, -1), DOC_MSG("Waypoint position in house"));
	format.Add("type", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, ACMOVE), DOC_MSG("Waypoint type"));
	format.Add("combatMode", NDTInteger, NCTSmallSigned, DEFVALUE(int, -1), DOC_MSG("Group combat mode"));
	format.Add("formation", NDTInteger, NCTSmallSigned, DEFVALUE(int, -1), DOC_MSG("Group formation"));
	format.Add("speed", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, SpeedUnchanged), DOC_MSG("Group speed"));
	format.Add("combat", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, CMUnchanged), DOC_MSG("Group behaviour"));
	format.Add("timeoutMin", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Waypoint timeout"));
	format.Add("timeoutMid", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Waypoint timeout"));
	format.Add("timeoutMax", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Waypoint timeout"));
	format.Add("description", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Waypoint description"));
	format.Add("expCond", NDTString, NCTNone, DEFVALUE(RString, "true"), DOC_MSG("Condition for activation of waypoint"));
	format.Add("expActiv", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Statement processed when waypoint is activated"));
	format.Add("script", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Controling script for scripted waypoints"));
	format.Add("showWP", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, ShowEasy), DOC_MSG("When waypoint is shown in map"));
	format.Add("synchronizations", NDTIntArray, NCTSmallUnsigned, DEFVALUEINTARRAY, DOC_MSG("List of synchronizations"));
	ArcadeEffects::CreateFormat(cls, format);
	return format;
}

TMError ArcadeWaypointInfo::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesWaypoint *>(ctx.GetIndices()))
	const IndicesWaypoint *indices = static_cast<const IndicesWaypoint *>(ctx.GetIndices());
	
	TMCHECK(ctx.IdxTransfer(indices->position, position))
	TMCHECK(ctx.IdxTransfer(indices->placement, placement))
	TMCHECK(ctx.IdxTransfer(indices->id, id))
	TMCHECK(ctx.IdxTransfer(indices->idStatic, idStatic))
	TMCHECK(ctx.IdxTransfer(indices->housePos, housePos))
	TMCHECK(ctx.IdxTransfer(indices->type, (int &)type))
	TMCHECK(ctx.IdxTransfer(indices->combatMode, (int &)combatMode))
	TMCHECK(ctx.IdxTransfer(indices->formation, (int &)formation))
	TMCHECK(ctx.IdxTransfer(indices->speed, (int &)speed))
	TMCHECK(ctx.IdxTransfer(indices->combat, (int &)combat))
	TMCHECK(ctx.IdxTransfer(indices->timeoutMin, timeoutMin))
	TMCHECK(ctx.IdxTransfer(indices->timeoutMid, timeoutMid))
	TMCHECK(ctx.IdxTransfer(indices->timeoutMax, timeoutMax))
	TMCHECK(ctx.IdxTransfer(indices->description, description))
	TMCHECK(ctx.IdxTransfer(indices->expCond, expCond))
	TMCHECK(ctx.IdxTransfer(indices->expActiv, expActiv))
	TMCHECK(ctx.IdxTransfer(indices->script, script))
	TMCHECK(ctx.IdxTransfer(indices->showWP, (int &)showWP))
	TMCHECK(ctx.IdxTransfer(indices->synchronizations, synchronizations))
	TMCHECK(effects.TransferMsg(ctx, indices->effects))
	return TMOK;
}

bool ArcadeWaypointInfo::HasEffect() const
{
	if (effects.cameraEffect.GetLength() > 0) return true;
	if (stricmp(effects.sound, "$NONE$") != 0) return true;
	if (effects.voice.GetLength() > 0) return true;
	if (effects.soundEnv.GetLength() > 0) return true;
	if (effects.soundDet.GetLength() > 0) return true;
	if (stricmp(effects.track, "$NONE$") != 0) return true;
	if (effects.titleType != TitleNone) return true;
	return false;
}

///////////////////////////////////////////////////////////////////////////////
// struct ArcadeGroupInfo

void ArcadeGroupInfo::AddOffset(Vector3Par offset)
{
	for (int i=0; i<units.Size(); i++)
	{
		units[i].AddOffset(offset);
	}
	for (int i=0; i<sensors.Size(); i++)
	{
		sensors[i].AddOffset(offset);
	}
	for (int i=0; i<waypoints.Size(); i++)
	{
		waypoints[i].AddOffset(offset);
	}
}

void ArcadeGroupInfo::Rotate(Vector3Par center, float angle, bool sel)
{
	for (int i=0; i<units.Size(); i++)
	{
		units[i].Rotate(center, angle, sel);
	}
	for (int i=0; i<sensors.Size(); i++)
	{
		sensors[i].Rotate(center, angle, sel);
	}
	for (int i=0; i<waypoints.Size(); i++)
	{
		waypoints[i].Rotate(center, angle, sel);
	}
}

void ArcadeGroupInfo::CalculateCenter(Vector3 &sum, int &count, bool sel)
{
	for (int i=0; i<units.Size(); i++)
	{
		units[i].CalculateCenter(sum, count, sel);
	}
	for (int i=0; i<sensors.Size(); i++)
	{
		sensors[i].CalculateCenter(sum, count, sel);
	}
	for (int i=0; i<waypoints.Size(); i++)
	{
		waypoints[i].CalculateCenter(sum, count, sel);
	}
}

void ArcadeGroupInfo::Select(bool select)
{
	for (int i=0; i<units.Size(); i++)
	{
		units[i].selected = select;
	}
	for (int i=0; i<sensors.Size(); i++)
	{
		sensors[i].selected = select;
	}
	for (int i=0; i<waypoints.Size(); i++)
	{
		waypoints[i].selected = select;
	}
}

// ADDED in patch 1.01 - AddOns check
void ArcadeGroupInfo::RequiredAddons(FindArrayRStringCI &addOns)
{
	for (int i=0; i<units.Size(); i++) units[i].RequiredAddons(addOns);
}

LSError ArcadeGroupInfo::Serialize(ParamArchive &ar)
{
	CHECK(ar.SerializeEnum("side", side, 1))	
	CHECK(ar.Serialize("Vehicles", units, 1))
	CHECK(ar.Serialize("Waypoints", waypoints, 1))
	CHECK(ar.Serialize("Sensors", sensors, 1))
	return LSOK;
}

///////////////////////////////////////////////////////////////////////////////
// struct ArcadeIntel

ArcadeIntel::ArcadeIntel()
{
	Init();
}

void ArcadeIntel::Init()
{
	friends[TEast][TEast] = 1.0;			friends[TEast][TWest] = 0.0;			friends[TEast][TGuerrila] = 0.0;
	friends[TWest][TEast] = 0.0;			friends[TWest][TWest] = 1.0;			friends[TWest][TGuerrila] = 1.0;
	friends[TGuerrila][TEast] = 0.0;	friends[TGuerrila][TWest] = 1.0;	friends[TGuerrila][TGuerrila] = 1.0;
	weather = 0.5;
	fog = 0;
	weatherForecast = 0.5;
	fogForecast = 0;
	year = 1985;
	month = 5;
	day = 10;
	hour = 7;
	minute = 30;

	briefingName = "";
	briefingDescription = "";
}

void SendIntel(ArcadeIntel *intel);

LSError ArcadeIntel::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("briefingName", briefingName, 2, ""))	
	CHECK(ar.Serialize("briefingDescription", briefingDescription, 2, ""))	

	if (ar.GetArVersion() >= 10)
	{
		CHECK(ar.Serialize("resistanceWest", friends[TWest][TGuerrila], 1, 1.0))
		CHECK(ar.Serialize("resistanceEast", friends[TEast][TGuerrila], 1, 0.0))
	}
	else
	{
		CHECK(ar.Serialize("resistance", friends[TWest][TGuerrila], 1, 1.0))
		friends[TEast][TGuerrila] = 1.0f - friends[TWest][TGuerrila];
	}
	if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
	{
		friends[TEast][TEast] = 1.0f; friends[TEast][TWest] = 0.0f;
		friends[TWest][TEast] = 0.0f; friends[TWest][TWest] = 1.0f;
		friends[TGuerrila][TGuerrila] = 1.0f;

		friends[TGuerrila][TEast] = friends[TEast][TGuerrila];
		friends[TGuerrila][TWest] = friends[TWest][TGuerrila];
	}

	CHECK(ar.Serialize("startWeather", weather, 1, 0.5))	
	CHECK(ar.Serialize("startFog", fog, 1, 0))	
	CHECK(ar.Serialize("forecastWeather", weatherForecast, 1, 0.5))	
	CHECK(ar.Serialize("forecastFog", fogForecast, 1, 0))	
	CHECK(ar.Serialize("year", year, 1, 1985))	
	CHECK(ar.Serialize("month", month, 1, 5))	
	CHECK(ar.Serialize("day", day, 1, 10))	
	CHECK(ar.Serialize("hour", hour, 1, 7))	
	CHECK(ar.Serialize("minute", minute, 1, 30))	

	return LSOK;
}

///////////////////////////////////////////////////////////////////////////////
// class ArcadeTemplate

void SelectLeader(ArcadeGroupInfo &gInfo)
{
	int maxRank = RankPrivate - 1;
	int i, iBest = -1;
	for (i=0; i<gInfo.units.Size(); i++)
	{
		ArcadeUnitInfo &uInfo = gInfo.units[i];
		uInfo.leader = false;
		if (uInfo.rank > maxRank)
		{
			maxRank = uInfo.rank;
			iBest = i;
		}
	}
	Assert(iBest >= 0);
	Assert(maxRank >= RankPrivate);
	gInfo.units[iBest].leader = true;
	gInfo.side = gInfo.units[iBest].side;
}

void SendBuildingUpdate(int id, int condition);

ArcadeTemplate::ArcadeTemplate()
{
	showHUD = true;
	showMap = true;
	showWatch = true;
	showCompass = true;
	showNotepad = true;
	showGPS = false;

	nextSyncId = 0;
	nextVehId = 0;

	randomSeed = toLargeInt(GRandGen.RandomValue()*0x1000000)+3;
}

void ArcadeTemplate::CheckSynchro()
{
	AutoArray<int> syncCountsW;
	AutoArray<int> syncCountsS;

	nextSyncId = 0;
	int n = groups.Size();
	int m = sensors.Size();
	
	for (int i=0; i<n; i++)
	{
		ArcadeGroupInfo &gInfo = groups[i];
		int m = gInfo.waypoints.Size();
		for (int j=0; j<m; j++)
		{
			ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
			int p = wInfo.synchronizations.Size();
			for (int l=0; l<p; l++)
				saturateMax(nextSyncId, wInfo.synchronizations[l] + 1);
		}
		m = gInfo.sensors.Size();
		for (int j=0; j<m; j++)
		{
			ArcadeSensorInfo &sInfo = gInfo.sensors[j];
			int p = sInfo.synchronizations.Size();
			for (int l=0; l<p; l++)
				saturateMax(nextSyncId, sInfo.synchronizations[l] + 1);
		}
	}
	for (int j=0; j<m; j++)
	{
		ArcadeSensorInfo &sInfo = sensors[j];
		int p = sInfo.synchronizations.Size();
		for (int l=0; l<p; l++)
			saturateMax(nextSyncId, sInfo.synchronizations[l] + 1);
	}
	
	syncCountsW.Resize(nextSyncId);
	syncCountsS.Resize(nextSyncId);
	for (int i=0; i<nextSyncId; i++)
	{
		syncCountsW[i] = 0;
		syncCountsS[i] = 0;
	}

	for (int i=0; i<n; i++)
	{
		ArcadeGroupInfo &gInfo = groups[i];
		int m = gInfo.waypoints.Size();
		for (int j=0; j<m; j++)
		{
			ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
			int p = wInfo.synchronizations.Size();
			for (int l=0; l<p; l++)
			{
				int sync = wInfo.synchronizations[l];
				Assert(sync >= 0);
				syncCountsW[sync]++;
			}
		}
		m = gInfo.sensors.Size();
		for (int j=0; j<m; j++)
		{
			ArcadeSensorInfo &sInfo = gInfo.sensors[j];
			int p = sInfo.synchronizations.Size();
			for (int l=0; l<p; l++)
			{
				int sync = sInfo.synchronizations[l];
				Assert(sync >= 0);
				syncCountsS[sync]++;
			}
		}
	}
	for (int j=0; j<m; j++)
	{
		ArcadeSensorInfo &sInfo = sensors[j];
		int p = sInfo.synchronizations.Size();
		for (int l=0; l<p; l++)
		{
			int sync = sInfo.synchronizations[l];
			Assert(sync >= 0);
			syncCountsS[sync]++;
		}
	}

	for (int i=0; i<n; i++)
	{
		ArcadeGroupInfo &gInfo = groups[i];
		int m = gInfo.waypoints.Size();
		for (int j=0; j<m; j++)
		{
			ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
			int p = wInfo.synchronizations.Size();
			for (int l=0; l<p;)
			{
				int sync = wInfo.synchronizations[l];
				Assert(sync >= 0);
				Assert(syncCountsW[sync] >= 1);
				if (syncCountsW[sync] + syncCountsS[sync] < 2)
				{
					wInfo.synchronizations.Delete(l);
					p--;
				}
				else
				{
					l++;
				}
			}
		}
		m = gInfo.sensors.Size();
		for (int j=0; j<m; j++)
		{
			ArcadeSensorInfo &sInfo = gInfo.sensors[j];
			int p = sInfo.synchronizations.Size();
			for (int l=0; l<p;)
			{
				int sync = sInfo.synchronizations[l];
				Assert(sync >= 0);
				Assert(syncCountsS[sync] >= 1);
				if (syncCountsW[sync] < 1)
				{
					sInfo.synchronizations.Delete(l);
					p--;
				}
				else
				{
					l++;
				}
			}
		}
	}
	for (int j=0; j<m; j++)
	{
		ArcadeSensorInfo &sInfo = sensors[j];
		int p = sInfo.synchronizations.Size();
		for (int l=0; l<p;)
		{
			int sync = sInfo.synchronizations[l];
			Assert(sync >= 0);
			Assert(syncCountsS[sync] >= 1);
			if (syncCountsW[sync] < 1)
			{
				sInfo.synchronizations.Delete(l);
				p--;
			}
			else
			{
				l++;
			}
		}
	}

	int maxSync = -1;
	for (int i=nextSyncId-1; i>=0; i--)
		if (syncCountsW[i] >= 1 || syncCountsS[i] >= 1)
		{
			Assert(syncCountsW[i] >= 1);
			Assert(syncCountsW[i] + syncCountsS[i] >= 2);
			maxSync = i;
			break;
		}
	nextSyncId = maxSync + 1;
}

bool ArcadeTemplate::IsConsistent(Display *disp, bool multiplayer)
{
	int nPlayers1 = 0;
	int side1;
	int nWestGroups = 0;
	int nEastGroups = 0;
	int nGuerrilaGroups = 0;
	int nCivilianGroups = 0;
	int nLogicGroups = 0;

	int i, n = groups.Size();
	for (i=0; i<n; i++)
	{
		ArcadeGroupInfo &info = groups[i];
		switch (info.side)
		{
		case TWest:
			nWestGroups++;
			break;
		case TEast:
			nEastGroups++;
			break;
		case TGuerrila:
			nGuerrilaGroups++;
			break;
		case TCivilian:
			nCivilianGroups++;
			break;
		case TLogic:
			nLogicGroups++;
			break;
		default:
			Fail("Side !!!");
			RptF("Side %d",info.side);
			break;
		}
		int nUnits = 0;
		for (int j=0; j<info.units.Size(); j++)
		{
			ArcadeUnitInfo &uInfo = info.units[j];
			switch (uInfo.player)
			{
			case APPlayerCommander:
			case APPlayerDriver:
			case APPlayerGunner:
				nPlayers1++;
				side1 = uInfo.side;
				break;
			}
			const VehicleType *type = dynamic_cast<const VehicleType * >(VehicleTypes.New(uInfo.vehicle));
			if( !type )
			{
				Fail("Type is not VehicleType");
				continue;
			}
			if (type->HasDriver()) nUnits++;
			if (type->HasCommander()) nUnits++;
			if (type->HasGunner()) nUnits++;
		}
		Assert(nUnits > 0);
		if (nUnits > MAX_UNITS_PER_GROUP)
		{
			if (disp)
			{
				char buffer[256];
				sprintf(buffer, LocalizeString(IDS_MSG_LOT_UNITS), MAX_UNITS_PER_GROUP);
				disp->CreateMsgBox(MB_BUTTON_OK, buffer);
			}
			return false;
		}
	}
	
	Assert(nPlayers1 <= 1);
#if !_ENABLE_CHEATS
	if (!multiplayer && nPlayers1 == 0)
	{
		if (disp)
			disp->CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_NO_PLAYER));
		return false;
	}
#endif

	if
	(
		nWestGroups > MaxGroups ||
		nEastGroups > MaxGroups ||
		nGuerrilaGroups > MaxGroups ||
		nCivilianGroups > MaxGroups ||
		nLogicGroups > MaxGroups
	)
	{
		if (disp)
		{
			char buffer[256];
			sprintf(buffer, LocalizeString(IDS_MSG_LOT_GROUPS), MaxGroups);
			disp->CreateMsgBox(MB_BUTTON_OK, buffer);
		}
		return false;
	}

	return true;
}

/*!
	\patch_internal 1.01 Date 06/19/2001 by Jirka - added check of required AddOns
*/
void ArcadeTemplate::RequiredAddons(FindArrayRStringCI &addOns)
{
	for (int i=0; i<groups.Size(); i++) groups[i].RequiredAddons(addOns);
	for (int i=0; i<emptyVehicles.Size(); i++) emptyVehicles[i].RequiredAddons(addOns);
}

void CheckPatch(FindArrayRStringCI &addOns, FindArrayRStringCI &missing)
{
	const ParamEntry *patches = Pars.FindEntry("CfgPatches");
	if (!patches)
	{
		missing = addOns;
		return;
	}
	// activate all addons requested by given mission
	GWorld->ActivateAddons(addOns);

	int m = patches->GetEntryCount();

	for (int i=0; i<addOns.Size(); i++)
	{
		RString addOn = addOns[i];
		bool found = false;
		for (int j=0; j<m; j++)
		{
			if (stricmp(addOn, patches->GetEntry(j).GetName()) == 0)
			{
				found = true; break;
			}
		}
		if (!found) missing.Add(addOn);
	}
}

/*!
\patch 1.82 Date 8/15/2002 by Ondra
- Fixed: Addon was not removed from addons[] list
when last unit using that addon was removed in the mission editor.
*/

void ArcadeTemplate::ScanRequiredAddons()
{
	FindArrayRStringCI oldAddOnsAuto = addOnsAuto;
	addOnsAuto.Resize(0);
	RequiredAddons(addOnsAuto);
	// check which items in oldAddOnsAuto are no longer listed in addOnsAuto
	// such items can be removed from addon list
	for (int i=0; i<oldAddOnsAuto.Size(); i++)
	{
		RString old = oldAddOnsAuto[i];
		if (addOnsAuto.Find(old)<0)
		{
			// was listed in old list, but is not in new
			// we can remove it from the required list as 
			// it might be listed several times?
			addOns.Delete(old);
		}
	}
	for (int i=0; i<addOnsAuto.Size(); i++)
	{
		addOns.AddUnique(addOnsAuto[i]);
	}
}

/*!
\patch 1.30 Date 11/03/2001 by Jirka
- Fixed: Check of AddOns in multiplayer game
\patch 1.63 Date 6/2/2002 by Ondra
- Improved: Addons added by mission editor as required are also removed
by the mission editor.
*/


LSError ArcadeTemplate::Serialize(ParamArchive &ar)
{
	ATSParams *params = (ATSParams *)ar.GetParams();
	if (ar.IsSaving())
	{
		// ADDED - AddOns check
		ScanRequiredAddons();
		// addOns.Resize(0);
		// items from this list can be removed if necessary
		CHECK(ar.SerializeArray("addOns", addOns, 1))
		CHECK(ar.SerializeArray("addOnsAuto", addOnsAuto, 1))

		CheckSynchro();	// remove invalid synchronizations
		Compact();
	}
	else if (ar.GetPass() == ParamArchive::PassFirst)
	{
		// ADDED - AddOns check
		addOns.Resize(0);
		missingAddOns.Resize(0);
		CHECK(ar.SerializeArray("addOns", addOns, 1))
		CheckPatch(addOns, missingAddOns);
		if (missingAddOns.Size() > 0) return LSNoAddOn;

		if (!params)
		{
			Fail("Params needed");
			return LSStructure;
		}
		FindArrayRStringCI addOnsBackup = addOns;
		Clear();
		addOns = addOnsBackup;
		params->nextSyncId = 0;
		params->nextVehId = 0;
	}

	CHECK(ar.Serialize("showHUD", showHUD, 8, true))
	CHECK(ar.Serialize("showMap", showMap, 8, true))
	CHECK(ar.Serialize("showWatch", showWatch, 8, true))
	CHECK(ar.Serialize("showCompass", showCompass, 8, true))
	CHECK(ar.Serialize("showNotepad", showNotepad, 8, true))
	CHECK(ar.Serialize("showGPS", showGPS, 8, false))
	CHECK(ar.Serialize("randomSeed", randomSeed, 1, 1))

	CHECK(ar.Serialize("Intel", intel, 7))
	CHECK(ar.Serialize("Groups", groups, 1))
	CHECK(ar.Serialize("Vehicles", emptyVehicles, 1))
	CHECK(ar.Serialize("Markers", markers, 1))
	CHECK(ar.Serialize("Sensors", sensors, 1))

	if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
	{
		Assert(params);
		nextSyncId = params->nextSyncId;
		nextVehId = params->nextVehId;
		CheckSynchro();	// remove invalid synchronizations
		Compact();
	}
	return LSOK;
}

ArcadeUnitInfo *ArcadeTemplate::FindUnit(int id, int &idGroup, int &idUnit)
{
	int i, n = groups.Size();
	for (i=0; i<n; i++)
	{
		ArcadeGroupInfo &gInfo = groups[i];

		int j, m = gInfo.units.Size();
		for (j=0; j<m; j++)
		{
			ArcadeUnitInfo &uInfo = gInfo.units[j];
			if (uInfo.id == id)
			{
				idGroup = i;
				idUnit = j;
				return &uInfo;
			}
		}
	}
	n = emptyVehicles.Size();
	for (i=0; i<n; i++)
	{
		ArcadeUnitInfo &uInfo = emptyVehicles[i];
		if (uInfo.id == id)
		{
			idGroup = -1;
			idUnit = i;
			return &uInfo;
		}
	}
	idGroup = -1;
	idUnit = -1;
	return NULL;
}

ArcadeUnitInfo *ArcadeTemplate::FindPlayer()
{
	int i, n = groups.Size();
	for (i=0; i<n; i++)
	{
		ArcadeGroupInfo &gInfo = groups[i];

		int j, m = gInfo.units.Size();
		for (j=0; j<m; j++)
		{
			ArcadeUnitInfo &uInfo = gInfo.units[j];
			switch (uInfo.player)
			{
			case APPlayerCommander:
			case APPlayerDriver:
			case APPlayerGunner:
				return &uInfo;
			}
		}
	}

	return NULL;
}

ArcadeGroupInfo *ArcadeTemplate::FindPlayerGroup()
{
	int i, n = groups.Size();
	for (i=0; i<n; i++)
	{
		ArcadeGroupInfo &gInfo = groups[i];

		int j, m = gInfo.units.Size();
		for (j=0; j<m; j++)
		{
			ArcadeUnitInfo &uInfo = gInfo.units[j];
			switch (uInfo.player)
			{
			case APPlayerCommander:
			case APPlayerDriver:
			case APPlayerGunner:
				return &gInfo;
			}
		}
	}

	return NULL;
}

ArcadeMarkerInfo *ArcadeTemplate::FindMarker(const char *name)
{
	for (int i=0; i<markers.Size(); i++)
	{
		ArcadeMarkerInfo &mInfo = markers[i];
		if (stricmp(name, mInfo.name) == 0)
		{
			return &mInfo;
		}
	}

	return NULL;
}

ArcadeUnitInfo *ArcadeTemplate::FindVehicle(const char *name)
{
	for (int i=0; i<emptyVehicles.Size(); i++)
	{
		ArcadeUnitInfo &uInfo = emptyVehicles[i];
		if (stricmp(name, uInfo.name) == 0)
		{
			return &uInfo;
		}
	}
	for (int i=0; i<groups.Size(); i++)
	{
		ArcadeGroupInfo &gInfo = groups[i];
		for (int j=0; j<gInfo.units.Size(); j++)
		{
			ArcadeUnitInfo &uInfo = gInfo.units[j];
			if (stricmp(name, uInfo.name) == 0)
			{
				return &uInfo;
			}
		}
	}
	return NULL;
}

ArcadeSensorInfo *ArcadeTemplate::FindSensor(const char *name)
{
	for (int i=0; i<sensors.Size(); i++)
	{
		ArcadeSensorInfo &sInfo = sensors[i];
		if (stricmp(name, sInfo.name) == 0)
		{
			return &sInfo;
		}
	}
	for (int i=0; i<groups.Size(); i++)
	{
		ArcadeGroupInfo &gInfo = groups[i];
		for (int j=0; j<gInfo.sensors.Size(); j++)
		{
			ArcadeSensorInfo &sInfo = gInfo.sensors[j];
			if (stricmp(name, sInfo.name) == 0)
			{
				return &sInfo;
			}
		}
	}
	return NULL;
}

void ArcadeTemplate::Clear()
{
	groups.Clear();
	emptyVehicles.Clear();
	sensors.Clear();
	markers.Clear();
	
	showHUD = true;
	showMap = true;
	showWatch = true;
	showCompass = true;
	showNotepad = true;
	showGPS = false;

	nextSyncId = 0;
	nextVehId = 0;

	addOns.Clear();
	missingAddOns.Clear();
}

void ArcadeTemplate::GroupDelete(int ig)
{
	Assert(ig >= 0);
	if (ig >= 0)
	{
		groups.Delete(ig);
	}
}

void ArcadeTemplate::UnitDelete(int ig, int iu)
{
	if (ig == -1)
	{
		emptyVehicles.Delete(iu);
	}
	else
	{
		ArcadeGroupInfo &gInfo = groups[ig];
		gInfo.units.Delete(iu);
		if (gInfo.units.Size() == 0)
			groups.Delete(ig);
		else
			SelectLeader(gInfo);
	}
}

void ArcadeTemplate::WaypointDelete(int ig, int iw)
{
	ArcadeGroupInfo &gInfo = groups[ig];
	gInfo.waypoints.Delete(iw);
}

bool ArcadeTemplate::UnitChangeGroup(int ig, int iu, int ignew)
{
	if (ignew >= 0)
	{
		if (ignew == ig) return false;
		// remove from old group
		ArcadeGroupInfo &gInfoOld = groups[ig];
		ArcadeUnitInfo uInfo = gInfoOld.units[iu];
		gInfoOld.units.Delete(iu);
		if (gInfoOld.units.Size() == 0)
		{
			groups.Delete(ig);
			if (ignew > ig) ignew--;
		}
		else
		{
			SelectLeader(gInfoOld);
		}
		// insert into new group
		ArcadeGroupInfo &gInfoNew = groups[ignew];
		gInfoNew.units.Add(uInfo);
		// select leader
		SelectLeader(gInfoNew);
		return true;
	}
	else
	{
		// split group
		ArcadeGroupInfo &gInfoOld = groups[ig];
		if (gInfoOld.units.Size() <= 1) return false;
		// remove from old group
		ArcadeUnitInfo uInfo = gInfoOld.units[iu];
		gInfoOld.units.Delete(iu);
		SelectLeader(gInfoOld);
		// insert into new group
		ArcadeGroupInfo gInfoNew;
		gInfoNew.units.Add(uInfo);
		gInfoNew.side = uInfo.side;
		// select leader
		SelectLeader(gInfoNew);

		groups.Add(gInfoNew);
		return true;
	}
}

void ArcadeTemplate::WaypointChangeSynchro(int ig, int iw, int ig1, int iw1)
{
	if (ig1 >= 0)
	{
		if (ig1 != ig)
		{
			// synchronization
			ArcadeGroupInfo &gInfo1 = groups[ig];
			ArcadeWaypointInfo &wInfo1 = gInfo1.waypoints[iw];
			ArcadeGroupInfo &gInfo2 = groups[ig1];
			ArcadeWaypointInfo &wInfo2 = gInfo2.waypoints[iw1];
			
			// check if synchronization does not exist
			int found = 0;
			for (int i=0; i<wInfo1.synchronizations.Size(); i++)
			{
				int sync = wInfo1.synchronizations[i];
				for (int j=0; j<wInfo2.synchronizations.Size(); j++)
				{
					if (wInfo2.synchronizations[j] == sync) found++;
				}
			}

			if (found == 0)
			{
				int sync = nextSyncId++;
				wInfo1.synchronizations.Add(sync);
				wInfo2.synchronizations.Add(sync);
			}
			else
			{
				Assert(found == 1);
				// TODO: MessageBox - both waypoints are synchronized already
			}
		}
		else
		{
			// TODO: MessageBox - cannot synchronize waypoints of the same group
		}
	}
	else
	{
		// split synchronization
		ArcadeGroupInfo &gInfo = groups[ig];
		ArcadeWaypointInfo &wInfo = gInfo.waypoints[iw];
		wInfo.synchronizations.Clear();
		CheckSynchro();
	}
}

bool ArcadeTemplate::UnitChangePosition(int ig, int iu, Vector3Val pos)
{
	ArcadeUnitInfo* uInfo = NULL;
	if (ig == -1)
	{
		Assert(iu >= 0 && iu < emptyVehicles.Size());
		if (iu < 0 || iu >= emptyVehicles.Size()) return false;
		uInfo = &emptyVehicles[iu];
	}
	else
	{
		Assert(ig >= 0 && ig < groups.Size());
		if (ig < 0 || ig >= groups.Size()) return false;
		ArcadeGroupInfo& gInfo = groups[ig];

		Assert(iu >= 0 && iu < gInfo.units.Size());
		if (iu < 0 || iu >= gInfo.units.Size()) return false;
		uInfo = &gInfo.units[iu];
	}
	Assert(uInfo);

	bool changedEnough = (uInfo->position - pos).SquareSizeXZ() >= Square(2);
	uInfo->position = pos;
	return changedEnough;
}

bool ArcadeTemplate::GroupChangePosition(int ig, int iu, Vector3Val pos)
{
	Assert(ig >= 0);
	ArcadeGroupInfo &gInfo = groups[ig];
	ArcadeUnitInfo &uInfo = gInfo.units[iu];

	Vector3 diff = pos - uInfo.position;
	bool changedEnough = diff.SquareSizeXZ() >= Square(2);

	for (int i=0; i<gInfo.units.Size(); i++)
	{
		ArcadeUnitInfo &uInfo = gInfo.units[i];
		Point3 pos = uInfo.position + diff;
		pos[1] = GLOB_LAND->RoadSurfaceY(pos[0], pos[2]);
		uInfo.position = pos;
	}
	
	return changedEnough;
}

bool ArcadeTemplate::WaypointChangePosition(int ig, int iw, Vector3Val pos)
{
	ArcadeGroupInfo &gInfo = groups[ig];
	ArcadeWaypointInfo &wInfo = gInfo.waypoints[iw];

	bool changedEnough = (wInfo.position - pos).SquareSizeXZ() >= Square(2);
	wInfo.position = pos;
	return changedEnough;
}

void ArcadeTemplate::UnitUpdate
(
	int &ig, int &iu,
	ArcadeUnitInfo &uInfo
)
{
	ArcadeUnitInfo *info = NULL;
	if (iu < 0)
	{
		// insert unit
		Assert(ig < 0);
		if (uInfo.side == TEmpty)
		{
			// empty vehicles
			ig = -1;
			uInfo.id = -1;			
			iu = emptyVehicles.Add(uInfo);
			info = &emptyVehicles[iu];
		}
		else
		{
			// Find group to insert into
			float minDist2 = Square(100);

			int i, n = groups.Size();
			for (i=0; i<n; i++)
			{
				ArcadeGroupInfo &gInfo = groups[i];
				if (gInfo.side != uInfo.side) continue;
				int j, m = gInfo.units.Size(), jLeader = -1;
				for (j=0; j<m; j++)
				{
					ArcadeUnitInfo &unit = gInfo.units[j];
					if (unit.leader)
					{
						jLeader = j;
						break;
					}
				}
				if (jLeader >= 0)
				{
					ArcadeUnitInfo &unit = gInfo.units[jLeader];
					float dist2 = (unit.position - uInfo.position).SquareSizeXZ();
					if (dist2 <= minDist2)
					{
						minDist2 = dist2;
						ig = i;
					}
				}
			}
			if (ig < 0)
			{
				ig = groups.Add();
				groups[ig].side = uInfo.side;
			}
			uInfo.id = -1;			
			ArcadeGroupInfo &gInfo = groups[ig];
			iu = gInfo.units.Add(uInfo);
			info = &gInfo.units[iu];
		}
	}
	else
	{
		// edit unit
		if (uInfo.side == TEmpty)
		{
			// empty vehicles
			ig = -1;
			emptyVehicles[iu] = uInfo;
			info = &emptyVehicles[iu];
		}
		else
		{
			Assert(ig >= 0);
			ArcadeGroupInfo &gInfo = groups[ig];
			gInfo.units[iu] = uInfo;
			info = &gInfo.units[iu];
		}
	}

	if (info->player == APPlayerCommander || info->player == APPlayerDriver || info->player == APPlayerGunner)
	{
		Assert(ig >= 0);
		ArcadeUnitPlayer p = info->player;
		info->player = APNonplayable;
		// reset player flag for previous player
		ArcadeUnitInfo *unit = FindPlayer();
		if (unit) unit->player = APNonplayable;
		// set player flag
		info->player = p;

		Assert(info->player != APNonplayable && info->player != APPlayableCDG);
		if (info->player == p)
			Glob.header.playerSide = (TargetSide)info->side;
	}

	RString iconName = Pars >> "CfgVehicles" >> info->vehicle >> "icon";
	info->icon = GlobLoadTexture(GetPictureName(iconName));
	info->size = Pars >> "CfgVehicles" >> info->vehicle >> "mapSize";
	if (info->id < 0) info->id = nextVehId++;
	if (ig >= 0)
	{
		ArcadeGroupInfo &gInfo = groups[ig];
		SelectLeader(gInfo);
	}
}

void ArcadeTemplate::WaypointUpdate
(
	int ig, int iw, int &iwnew,
	ArcadeWaypointInfo &waypoint
)
{
	ArcadeGroupInfo &gInfo = groups[ig];
	if (iw < 0)
	{
		DoAssert(iwnew >= 0);
		gInfo.waypoints.Insert(iwnew, waypoint);
	}
	else if (iwnew == iw || iwnew < 0)
	{
		iwnew = iw;
		gInfo.waypoints[iw] = waypoint;
	}
	else
	{
		gInfo.waypoints.Delete(iw);
		if (iwnew > iw) iwnew--;
		gInfo.waypoints.Insert(iwnew, waypoint);
	}
}

void ArcadeTemplate::SensorUpdate
(
	int ig, int index,
	ArcadeSensorInfo &sInfo
)
{
	if (ig < 0)
	{
		if (index < 0)
		{
			sensors.Add(sInfo);
		}
		else
		{
			sensors[index] = sInfo;
		}
	}
	else
	{
		ArcadeGroupInfo &gInfo = groups[ig];
		if (index < 0)
		{
			index = gInfo.sensors.Add(sInfo);
		}
		else
		{
			gInfo.sensors[index] = sInfo;
		}
	}
}

void ArcadeTemplate::SensorDelete(int ig, int index)
{
	if (ig < 0)
		sensors.Delete(index);
	else
	{
		ArcadeGroupInfo &gInfo = groups[ig];
		gInfo.sensors.Delete(index);
	}
}

bool ArcadeTemplate::SensorChangePosition(int ig, int index, Vector3Val pos)
{
	ArcadeSensorInfo *sInfo = NULL;
	if (ig < 0)
	{
		sInfo = &sensors[index];
	}
	else
	{
		ArcadeGroupInfo& gInfo = groups[ig];
		sInfo = &gInfo.sensors[index];
	}

	bool changedEnough = (sInfo->position - pos).SquareSizeXZ() >= Square(2);
	sInfo->position = pos;
	return changedEnough;
}

bool ArcadeTemplate::SensorChangeGroup(int ig, int index, int ignew)
{
	if (ignew == ig) return false;
	
	ArcadeSensorInfo sInfo;
	if (ig >= 0)
	{
		// remove from old group
		ArcadeGroupInfo &gInfoOld = groups[ig];
		sInfo = gInfoOld.sensors[index];
		gInfoOld.sensors.Delete(index);
	}
	else
	{
		// remove from flags
		sInfo = sensors[index];
		sensors.Delete(index);
	}

	if (ignew >= 0)
	{
		// insert into new group
		ArcadeGroupInfo &gInfoNew = groups[ignew];
		sInfo.activationBy = ASAGroup;
		sInfo.idStatic = -1;
		sInfo.idVehicle = -1;
		gInfoNew.sensors.Add(sInfo);
	}
	else
	{
		if (sInfo.activationBy == ASAGroup)
			sInfo.activationBy = ASANone;
		sensors.Add(sInfo);
	}
	return true;
}

void ArcadeTemplate::SensorChangeVehicle(int ig, int index, int id)
{
	ArcadeSensorInfo *sInfo = NULL;
	if (ig >= 0)
	{
		ArcadeGroupInfo &gInfo = groups[ig];
		sInfo = &gInfo.sensors[index];
	}
	else
	{
		sInfo = &sensors[index];
	}
	sInfo->idStatic = -1;
	sInfo->idVehicle = id;
	sInfo->activationBy = ASAVehicle;
	if (ig >= 0) SensorChangeGroup(ig, index, -1);
}

void ArcadeTemplate::SensorChangeStatic(int ig, int index, int id)
{
	ArcadeSensorInfo *sInfo = NULL;
	if (ig >= 0)
	{
		ArcadeGroupInfo &gInfo = groups[ig];
		sInfo = &gInfo.sensors[index];
	}
	else
	{
		sInfo = &sensors[index];
	}
	sInfo->idStatic = id;
	sInfo->idVehicle = -1;
	sInfo->activationBy = ASAStatic;
	if (ig >= 0) SensorChangeGroup(ig, index, -1);
}

void ArcadeTemplate::SensorChangeSynchro(int ig, int index, int ig1, int iw1)
{
	ArcadeSensorInfo *sInfo = NULL;
	if (ig >= 0)
	{
		ArcadeGroupInfo &gInfo = groups[ig];
		sInfo = &gInfo.sensors[index];
	}
	else
	{
		sInfo = &sensors[index];
	}
	Assert(sInfo);

	if (ig1 >= 0)
	{
		ArcadeGroupInfo &gInfo = groups[ig1];
		ArcadeWaypointInfo &wInfo = gInfo.waypoints[iw1];

		// check if synchronization does not exist
		int found = 0;
		for (int i=0; i<wInfo.synchronizations.Size(); i++)
		{
			int sync = wInfo.synchronizations[i];
			for (int j=0; j<sInfo->synchronizations.Size(); j++)
			{
				if (sInfo->synchronizations[j] == sync) found++;
			}
		}

		if (found == 0)
		{
			int sync = nextSyncId++;
			wInfo.synchronizations.Add(sync);
			sInfo->synchronizations.Add(sync);
		}
		else
		{
			Assert(found == 1);
			// TODO: MessageBox - waypoint and sensor are synchronized already
		}
	}
	else
	{
		// split synchronization
		sInfo->synchronizations.Clear();
		CheckSynchro();
	}
}

void ArcadeTemplate::MarkerUpdate(int index, ArcadeMarkerInfo &mInfo)
{
	if (index < 0)
	{
		index = markers.Add(mInfo);
	}
	else
	{
		if (index >= markers.Size())
			markers.Resize(index + 1);
		markers[index] = mInfo;
	}

	const ParamEntry &cls = Pars >> "CfgMarkers" >> markers[index].type;
	markers[index].icon = GlobLoadTexture
	(
		GetPictureName(cls >> "icon")
	);
	markers[index].size = cls >> "size";
}

void ArcadeTemplate::MarkerDelete(int index)
{
	markers.Delete(index);
}

bool ArcadeTemplate::MarkerChangePosition(int index, Vector3Val pos)
{
	ArcadeMarkerInfo &mInfo = markers[index];

	bool changedEnough = (mInfo.position - pos).SquareSizeXZ() >= Square(2);
	mInfo.position = pos;
	return changedEnough;
}

void ArcadeTemplate::UnitAddMarker(int ig, int index, int indexMarker)
{
	ArcadeUnitInfo *uInfo = NULL;
	if (ig < 0)
	{
		uInfo = &emptyVehicles[index];
	}
	else
	{
		ArcadeGroupInfo &gInfo = groups[ig];
		uInfo = &gInfo.units[index];
	}
	Assert(uInfo);
	RString name = markers[indexMarker].name;
	int n = uInfo->markers.Size();
	for (int i=0; i<n; i++)
	{
		if (stricmp(uInfo->markers[i], name) == 0)
		{
			return; // already in list
		}
	}
	uInfo->markers.Add(name);
}

void ArcadeTemplate::RemoveMarker(int indexMarker)
{
	RString name = markers[indexMarker].name;
	int n = groups.Size();
	for (int i=0; i<n; i++)
	{
		ArcadeGroupInfo &gInfo = groups[i];
		int m = gInfo.units.Size();
		for (int j=0; j<m; j++)
		{
			ArcadeUnitInfo &uInfo = gInfo.units[j];
			int o = uInfo.markers.Size();
			for (int k=0; k<o; k++)
			{
				if (stricmp(uInfo.markers[k], name) == 0)
				{
					uInfo.markers.Delete(k);
					break; // max once in each list
				}
			}
		}
	}
	int m = emptyVehicles.Size();
	for (int j=0; j<m; j++)
	{
		ArcadeUnitInfo &uInfo = emptyVehicles[j];
		int o = uInfo.markers.Size();
		for (int k=0; k<o; k++)
		{
			if (stricmp(uInfo.markers[k], name) == 0)
			{
				uInfo.markers.Delete(k);
				break; // max once in each list
			}
		}
	}
}

void ArcadeTemplate::AddGroup(const ParamEntry &cls, Vector3Par position)
{
	int ig = groups.Add();
	ArcadeGroupInfo &group = groups[ig];
	for (int i=0; i<cls.GetEntryCount(); i++)
	{
		const ParamEntry &entry = cls.GetEntry(i);
		if (!entry.IsClass()) continue;
		int iu = group.units.Add();
		ArcadeUnitInfo &unit = group.units[iu];
		int side = entry >> "side";
		unit.side = (TargetSide)side;
		unit.vehicle = entry >> "vehicle";
		RString iconName = Pars >> "CfgVehicles" >> unit.vehicle >> "icon";
		unit.icon = GlobLoadTexture(GetPictureName(iconName));
		unit.size = Pars >> "CfgVehicles" >> unit.vehicle >> "mapSize";

		RStringB rank = entry >> "rank";
		unit.rank = GetEnumValue<Rank>(rank);
		unit.skill = RankToSkill(unit.rank);
		unit.position = position;
		float offset = (entry >> "position")[0];
		unit.position[0] += offset;
		offset = (entry >> "position")[1];
		unit.position[2] += offset;
		offset = (entry >> "position")[2];
		unit.position[1] += offset;
		unit.id = nextVehId++;
	}
	SelectLeader(group);
}

struct ConversionItem
{
	bool used;
	int newIndex;
};
TypeIsSimple(ConversionItem)

void ArcadeTemplate::Compact()
{
	AutoArray<ConversionItem> table;
	
	// vehicle IDs
	table.Resize(nextVehId);
	for (int i=0; i<nextVehId; i++)
	{
		table[i].used = false;
		table[i].newIndex = -1;
	}
	for (int i=0; i<emptyVehicles.Size(); i++)
	{
		ArcadeUnitInfo &uInfo = emptyVehicles[i];
		Assert(uInfo.id >= 0);
		Assert(uInfo.id < nextVehId);
		table[uInfo.id].used = true;
	}
	for (int i=0; i<groups.Size(); i++)
	{
		ArcadeGroupInfo &gInfo = groups[i];
		for (int j=0; j<gInfo.units.Size(); j++)
		{
			ArcadeUnitInfo &uInfo = gInfo.units[j];
			Assert(uInfo.id >= 0);
			Assert(uInfo.id < nextVehId);
			table[uInfo.id].used = true;
		}
	}
	int index = 0;
	for (int i=0; i<nextVehId; i++)
		if (table[i].used) table[i].newIndex = index++;
	nextVehId = index;
	for (int i=0; i<emptyVehicles.Size(); i++)
	{
		ArcadeUnitInfo &uInfo = emptyVehicles[i];
		uInfo.id = table[uInfo.id].newIndex;
	}
	for (int i=0; i<sensors.Size(); i++)
	{
		ArcadeSensorInfo &sInfo = sensors[i];
		if (sInfo.idVehicle >= 0)
		{
			Assert(sInfo.idVehicle < table.Size());
			sInfo.idVehicle = table[sInfo.idVehicle].newIndex;
		}
	}
	for (int i=0; i<groups.Size(); i++)
	{
		ArcadeGroupInfo &gInfo = groups[i];
		for (int j=0; j<gInfo.units.Size(); j++)
		{
			ArcadeUnitInfo &uInfo = gInfo.units[j];
			uInfo.id = table[uInfo.id].newIndex;
		}
		for (int j=0; j<gInfo.sensors.Size(); j++)
		{
			ArcadeSensorInfo &sInfo = gInfo.sensors[j];
			if (sInfo.idVehicle >= 0)
			{
				Assert(sInfo.idVehicle < table.Size());
				sInfo.idVehicle = table[sInfo.idVehicle].newIndex;
			}
		}
		for (int j=0; j<gInfo.waypoints.Size(); j++)
		{
			ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
			if (wInfo.id >= 0)
			{
				Assert(wInfo.id < table.Size());
				wInfo.id = table[wInfo.id].newIndex;
			}
		}
	}

	// synchronization IDs
	table.Resize(nextSyncId);
	for (int i=0; i<nextSyncId; i++)
	{
		table[i].used = false;
		table[i].newIndex = -1;
	}
	for (int i=0; i<sensors.Size(); i++)
	{
		ArcadeSensorInfo &sInfo = sensors[i];
		for (int s=0; s<sInfo.synchronizations.Size(); s++)
		{
			int sync = sInfo.synchronizations[s];
			Assert(sync >= 0);
			Assert(sync < nextSyncId);
			table[sync].used = true;
		}
	}
	for (int i=0; i<groups.Size(); i++)
	{
		ArcadeGroupInfo &gInfo = groups[i];
		for (int j=0; j<gInfo.sensors.Size(); j++)
		{
			ArcadeSensorInfo &sInfo = gInfo.sensors[j];
			for (int s=0; s<sInfo.synchronizations.Size(); s++)
			{
				int sync = sInfo.synchronizations[s];
				Assert(sync >= 0);
				Assert(sync < nextSyncId);
				table[sync].used = true;
			}
		}
		for (int j=0; j<gInfo.waypoints.Size(); j++)
		{
			ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
			for (int s=0; s<wInfo.synchronizations.Size(); s++)
			{
				int sync = wInfo.synchronizations[s];
				Assert(sync >= 0);
				Assert(sync < nextSyncId);
				table[sync].used = true;
			}
		}
	}
	index = 0;
	for (int i=0; i<nextSyncId; i++)
		if (table[i].used) table[i].newIndex = index++;
	nextSyncId = index;
	for (int i=0; i<sensors.Size(); i++)
	{
		ArcadeSensorInfo &sInfo = sensors[i];
		for (int s=0; s<sInfo.synchronizations.Size(); s++)
		{
			int sync = sInfo.synchronizations[s];
			sInfo.synchronizations[s] = table[sync].newIndex;
		}
	}
	for (int i=0; i<groups.Size(); i++)
	{
		ArcadeGroupInfo &gInfo = groups[i];
		for (int j=0; j<gInfo.sensors.Size(); j++)
		{
			ArcadeSensorInfo &sInfo = gInfo.sensors[j];
			for (int s=0; s<sInfo.synchronizations.Size(); s++)
			{
				int sync = sInfo.synchronizations[s];
				sInfo.synchronizations[s] = table[sync].newIndex;
			}
		}
		for (int j=0; j<gInfo.waypoints.Size(); j++)
		{
			ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
			for (int s=0; s<wInfo.synchronizations.Size(); s++)
			{
				int sync = wInfo.synchronizations[s];
				wInfo.synchronizations[s] = table[sync].newIndex;
			}
		}
	}
}

struct TranslationTableItem
{
	RString from;
	RString to;

	TranslationTableItem(RString f, RString t)
	{
		from = f; to = t;
	}
};
TypeIsMovableZeroed(TranslationTableItem)

class TranslationTable : public AutoArray<TranslationTableItem>
{
	typedef AutoArray<TranslationTableItem> base;
public:
	int Add(RString from, RString to)
	{
		return base::Add(TranslationTableItem(from, to));
	}
	RString Translate(RString from);
};

RString TranslationTable::Translate(RString from)
{
	if (from.GetLength() == 0) return from;
	for (int i=0; i<Size(); i++)
	{
		const TranslationTableItem &item = Get(i);
		if (stricmp(from, item.from) == 0) return item.to;
	}
	return from;
}

void ArcadeTemplate::Merge(ArcadeTemplate &t, Vector3Par offset)
{
	// unique names - ArcadeMarkerInfo::name
	TranslationTable markerNames;
	for (int i=0; i<t.markers.Size(); i++)
	{
		ArcadeMarkerInfo &mInfo = t.markers[i];
		if (mInfo.name.GetLength() == 0) continue;
		if (FindMarker(mInfo.name))
		{
			char name[256];
			for (int l=1; ; l++)
			{
				sprintf(name, "%s_%d", (const char *)mInfo.name, l);
				if (!FindMarker(name)) break;
			}
			markerNames.Add(mInfo.name, name);
		}
	}

	// unique names - ArcadeUnitInfo::text
	TranslationTable unitNames;
	for (int i=0; i<t.emptyVehicles.Size(); i++)
	{
		ArcadeUnitInfo &uInfo = t.emptyVehicles[i];
		if (uInfo.name.GetLength() == 0) continue;
		if (FindVehicle(uInfo.name))
		{
			char name[256];
			for (int l=1; ; l++)
			{
				sprintf(name, "%s_%d", (const char *)uInfo.name, l);
				if (!FindVehicle(name)) break;
			}
			unitNames.Add(uInfo.name, name);
		}
	}
	for (int i=0; i<t.groups.Size(); i++)
	{
		ArcadeGroupInfo &gInfo = t.groups[i];
		for (int i=0; i<gInfo.units.Size(); i++)
		{
			ArcadeUnitInfo &uInfo = gInfo.units[i];
			if (uInfo.name.GetLength() == 0) continue;
			if (FindVehicle(uInfo.name))
			{
				char name[256];
				for (int l=1; ; l++)
				{
					sprintf(name, "%s_%d", (const char *)uInfo.name, l);
					if (!FindVehicle(name)) break;
				}
				unitNames.Add(uInfo.name, name);
			}
		}
	}

	// unique names - ArcadeSensorInfo::name
	TranslationTable sensorNames;
	for (int i=0; i<t.sensors.Size(); i++)
	{
		ArcadeSensorInfo &sInfo = t.sensors[i];
		if (sInfo.name.GetLength() == 0) continue;
		if (FindSensor(sInfo.name))
		{
			char name[256];
			for (int l=1; ; l++)
			{
				sprintf(name, "%s_%d", (const char *)sInfo.name, l);
				if (!FindSensor(name)) break;
			}
			sensorNames.Add(sInfo.name, name);
		}
	}
	for (int i=0; i<t.groups.Size(); i++)
	{
		ArcadeGroupInfo &gInfo = t.groups[i];
		for (int i=0; i<gInfo.sensors.Size(); i++)
		{
			ArcadeSensorInfo &sInfo = gInfo.sensors[i];
			if (sInfo.name.GetLength() == 0) continue;
			if (FindSensor(sInfo.name))
			{
				char name[256];
				for (int l=1; ; l++)
				{
					sprintf(name, "%s_%d", (const char *)sInfo.name, l);
					if (!FindSensor(name)) break;
				}
				sensorNames.Add(sInfo.name, name);
			}
		}
	}

	// merge templates
	for (int i=0; i<t.groups.Size(); i++)
	{
		int index = groups.Add(t.groups[i]);
		ArcadeGroupInfo &gInfo = groups[index];
		// offset positions
		gInfo.AddOffset(offset);
		// offset synchronizations and vehicle IDs
		for (int j=0; j<gInfo.units.Size(); j++)
		{
			ArcadeUnitInfo &uInfo = gInfo.units[j];
			Assert(uInfo.id >= 0);
			Assert(uInfo.id < t.nextVehId);
			uInfo.id += nextVehId;
			uInfo.player = APNonplayable; // avoid 2 players
			uInfo.name = unitNames.Translate(uInfo.name);
			for (int m=0; m<uInfo.markers.Size(); m++)
			{
				uInfo.markers[m] = markerNames.Translate(uInfo.markers[m]);
			}
		}
		for (int j=0; j<gInfo.sensors.Size(); j++)
		{
			ArcadeSensorInfo &sInfo = gInfo.sensors[j];
			if (sInfo.idVehicle >= 0)
			{
				Assert(sInfo.idVehicle < t.nextVehId);
				sInfo.idVehicle += nextVehId;
			}
			for (int s=0; s<sInfo.synchronizations.Size(); s++)
			{
				Assert(sInfo.synchronizations[s] >= 0);
				Assert(sInfo.synchronizations[s] < t.nextSyncId);
				sInfo.synchronizations[s] += nextSyncId;
			}
			sInfo.name = sensorNames.Translate(sInfo.name);
		}
		for (int j=0; j<gInfo.waypoints.Size(); j++)
		{
			ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
			if (wInfo.id >= 0)
			{
				Assert(wInfo.id < t.nextVehId);
				wInfo.id += nextVehId;
			}
			for (int s=0; s<wInfo.synchronizations.Size(); s++)
			{
				Assert(wInfo.synchronizations[s] >= 0);
				Assert(wInfo.synchronizations[s] < t.nextSyncId);
				wInfo.synchronizations[s] += nextSyncId;
			}
		}
	}
	for (int i=0; i<t.emptyVehicles.Size(); i++)
	{
		int index = emptyVehicles.Add(t.emptyVehicles[i]);
		ArcadeUnitInfo &uInfo = emptyVehicles[index];
		// offset positions
		uInfo.AddOffset(offset);
		// offset vehicle IDs
		Assert(uInfo.id >= 0);
		Assert(uInfo.id < t.nextVehId);
		uInfo.id += nextVehId;
		uInfo.name = unitNames.Translate(uInfo.name);
		for (int m=0; m<uInfo.markers.Size(); m++)
		{
			uInfo.markers[m] = markerNames.Translate(uInfo.markers[m]);
		}
	}
	for (int i=0; i<t.sensors.Size(); i++)
	{
		int index = sensors.Add(t.sensors[i]);
		// offset positions
		ArcadeSensorInfo &sInfo = sensors[index];
		sInfo.AddOffset(offset);
		// offset synchronizations and vehicle IDs
		if (sInfo.idVehicle >= 0)
		{
			Assert(sInfo.idVehicle < t.nextVehId);
			sInfo.idVehicle += nextVehId;
		}
		for (int s=0; s<sInfo.synchronizations.Size(); s++)
		{
			Assert(sInfo.synchronizations[s] >= 0);
			Assert(sInfo.synchronizations[s] < t.nextSyncId);
			sInfo.synchronizations[s] += nextSyncId;
		}
		sInfo.name = sensorNames.Translate(sInfo.name);
	}
	for (int i=0; i<t.markers.Size(); i++)
	{
		int index = markers.Add(t.markers[i]);
		// offset positions
		ArcadeMarkerInfo &mInfo = markers[index];
		mInfo.AddOffset(offset);
		mInfo.name = markerNames.Translate(mInfo.name);
	}

	nextVehId += t.nextVehId;
	nextSyncId += t.nextSyncId;
}

void ArcadeTemplate::AddOffset(Vector3Par offset)
{
	for (int i=0; i<emptyVehicles.Size(); i++)
		emptyVehicles[i].AddOffset(offset);
	for (int i=0; i<sensors.Size(); i++)
		sensors[i].AddOffset(offset);
	for (int i=0; i<markers.Size(); i++)
		markers[i].AddOffset(offset);
	for (int i=0; i<groups.Size(); i++)
		groups[i].AddOffset(offset);
}

void ArcadeTemplate::Rotate(Vector3Par center, float angle, bool sel)
{
	for (int i=0; i<emptyVehicles.Size(); i++)
		emptyVehicles[i].Rotate(center, angle, sel);
	for (int i=0; i<sensors.Size(); i++)
		sensors[i].Rotate(center, angle, sel);
	for (int i=0; i<markers.Size(); i++)
		markers[i].Rotate(center, angle, sel);
	for (int i=0; i<groups.Size(); i++)
		groups[i].Rotate(center, angle, sel);
}

void ArcadeTemplate::CalculateCenter(Vector3 &sum, int &count, bool sel)
{
	for (int i=0; i<emptyVehicles.Size(); i++)
		emptyVehicles[i].CalculateCenter(sum, count, sel);
	for (int i=0; i<sensors.Size(); i++)
		sensors[i].CalculateCenter(sum, count, sel);
	for (int i=0; i<markers.Size(); i++)
		markers[i].CalculateCenter(sum, count, sel);
	for (int i=0; i<groups.Size(); i++)
		groups[i].CalculateCenter(sum, count, sel);
}

void ArcadeTemplate::ClearSelection()
{
	for (int i=0; i<emptyVehicles.Size(); i++)
		emptyVehicles[i].selected = false;
	for (int i=0; i<sensors.Size(); i++)
		sensors[i].selected = false;
	for (int i=0; i<markers.Size(); i++)
		markers[i].selected = false;
	for (int i=0; i<groups.Size(); i++)
	{
		ArcadeGroupInfo &gInfo = groups[i];
		for (int j=0; j<gInfo.units.Size(); j++)
			gInfo.units[j].selected = false;
		for (int j=0; j<gInfo.sensors.Size(); j++)
			gInfo.sensors[j].selected = false;
		for (int j=0; j<gInfo.waypoints.Size(); j++)
			gInfo.waypoints[j].selected = false;
	}
}
