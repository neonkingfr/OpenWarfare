#ifdef _MSC_VER
#pragma once
#endif

#ifndef _RECOIL_HPP
#define _RECOIL_HPP

#include <Es/Strings/rString.hpp>

#ifdef PREPROCESS_DOCUMENTATION
#include "networkObject.hpp"
#endif

/*!
\file
Interface file for RecoilFunction clas
*/

struct RecoilItem
{
	float _time;
	Matrix4 _mat;
};

TypeIsSimple(RecoilItem)

struct RecoilFFRamp
{
	float _startTime,_duration;
	float _begAmplitude,_endAmplitude;
};

//! Rifle recoil function representation

class ParamEntry;
/*!
\patch 1.11 Date 7/31/2001 by Ondra.
Added: rifle recoil to force feedback effect conversion
*/

class RecoilFunction: public RefCount
{
	AutoArray<RecoilItem> _queue;
	const ParamEntry *_cfg;

	public:
	//! construct empty
	RecoilFunction();
	//! construct from config (CfgRecoils)
	RecoilFunction( RStringB name );
	~RecoilFunction();

	const RStringB &GetName() const;

	//! direct construction
	void AddItem(const RecoilItem &item);
	//! CfgRecoil-like construction
	void AddItem(float time, float offset, float angle);
	
	float GetRecoilRotX( float time) const;
	void ApplyRecoil(float time, Matrix4 &mat, float factor=1) const;
	bool GetTerminated( float time ) const;

	//! convert recoil item to force representation
	void GetFFEdge(int index, float &time, float &amplitude) const;
	//! convert two recoil items to force feedback ramp effect
	bool GetFFRamp(int index, RecoilFFRamp &ramp) const;
};

#endif
