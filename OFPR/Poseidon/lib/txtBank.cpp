#include "wpch.hpp"
#include "textbank.hpp"
#include "engine.hpp"
#include "paramFileExt.hpp"
#include <El/QStream/QBStream.hpp>
#include <Es/Strings/bstring.hpp>
#include "global.hpp"

int AbstractTextBank::AnimatedNumber( const char *name )
{
	// animated name has structure: name.number.ext (e.g. buch.01.tga)
	const char *n=strrchr(name,'\\');
	if (!n)
	{
		LogF("Bad texture name '%s'",name);
		return -1;
	}
	n++;
	const char *nn=strchr(n,'.');
	// no dot - cannot be animated
	if( !nn ) return -1;
	// check if there is another dot after this one
	nn++;
	const char *ext=strchr(nn,'.');
	if (!ext) return -1;
	// number is placed between nn and ext
	// scan number, detect error
	int num = 0;
	while (isdigit(*nn))
	{
		num = num*10+*nn-'0';
		nn++;
	}
	if (*nn!='.')
	{
		LogF("Texture name %s looked like animated",name);
		return -1;
	}
	return num;
}

int AbstractTextBank::AnimatedName( const char *name, char *prefix, char *postfix )
{
	*postfix=0;
	strcpy(prefix,name);
	char *n=strrchr(prefix,'\\');
	Assert( n );
	n++;
	char *nn=strchr(n,'.');
	if( !nn ) return -1;
	int ret=atoi(nn+1);
	char *ext=strchr(nn+1,'.');
	if( !ext ) return -1;
	strcpy(postfix,ext); // use postfix
	*++nn=0; // terminate prefix
	return ret;
}

const char *AnimatedTexture::Name() const
{
	if( Size()<=0 ) return "";
	Texture *tex = Get(0);
	if (!tex) return RStringB();
	return tex->GetName();
}

RStringB AnimatedTexture::GetName() const
{
	if( Size()<=0 ) return "";
	Texture *tex = Get(0);
	if (!tex) return RStringB();
	return tex->GetName();
}


AnimatedTexture *AbstractTextBank::LoadAnimated( RStringB name )
{
	// scan animated textures for a match
	int i;
	for( i=0; i<_animatedTextures.Size(); i++ )
	{
		AnimatedTexture *tex=_animatedTextures[i];
		if( tex && tex->GetName()==name ) return tex;
	}
	// scan normal textures for a match

	char prefix[256];
	char postfix[256];
	AnimatedName(name,prefix,postfix);
	// try to find texture with the same prefix
	int prefLen=strlen(prefix);
	for( i=0; i<_animatedTextures.Size(); i++ )
	{
		AnimatedTexture *tex=_animatedTextures[i];
		if( !strncmp(tex->Name(),name,prefLen) )
		{
			Log("Double animated texture %s",name);
			return tex;
		}
	}
	// load data
	AnimatedTexture *ret=new AnimatedTexture(this);
	int start=AnimatedNumber(name);
	char loadName[256];
	for( i=start; ; i++ )
	{
		sprintf(loadName,"%s%02d%s",prefix,i,postfix);
		if( !QIFStreamB::FileExist(loadName) ) break;
		Ref<Texture> texture=Load(loadName);
		if (texture)
		{
			ret->Add(texture);
			texture->_inAnimation=ret;
		}
	}
	if( ret->Size()<=0 )
	{
		// create dummy animated texture - placeholder
		Ref<Texture> texture=Load(name);
		ret->Add(texture);
		//WarningMessage("Cannot load animated texture '%s'.",(const char *)name);
	}
	int free = _animatedTextures.Find(NULL);
	if (free>=0)
	{
		_animatedTextures[free] = ret;
	}
	else
	{
		_animatedTextures.Add(ret);
	}
	return ret;
}

void AbstractTextBank::DeleteAnimated( AnimatedTexture *texture )
{
	int index=_animatedTextures.Find(texture);
	Assert( index>=0 );
	if( index>=0 ) _animatedTextures.Delete(index);
}

AnimatedTexture::AnimatedTexture( AbstractTextBank *bank )
:_bank(bank)
{
}

AnimatedTexture::~AnimatedTexture()
{
	int i,n=Size();
	#if !_RELEASE
	if( n>0 )
	{
		Log("Delete animated texture '%s'",Name());
	}
	#endif
	// remove reference from the bank
	//_bank->DeleteAnimated(this);
	for( i=0; i<n; i++ )
	{
		// unlink animation from all its textures
		Texture *texture=Get(i);
		// forget all textures
		if (texture) texture->_inAnimation=NULL;
		Set(i).Free();
	}
	Clear();
}

void AnimatedTexture::Remove( Texture *text )
{
	int i,n=Size();
	for( i=0; i<n; i++ )
	{
		if( (*this)[i]==text )
		{
			Delete(i);
			break;
		}
	}
}


Texture::Texture()
:_inAnimation(NULL)
{
	_refCountLocked = false;
}

/*!
\patch 1.90 Date 11/7/2002 by Ondra
- Fixed: MP: Flying ammo crates or jumping vehicles
caused by inconsistent terrain roughness determination.
*/

void Texture::SetName( RStringB name )
{
	_name=name;
	// use surface information to acquire roughness
	const SurfaceInfo &surface=GLOB_ENGINE->TextBank()->GetSurface(name);
	_roughness=surface._roughness;
	_dustness=surface._dustness;
	_soundEnv=surface._soundEnv;
  #if _ENABLE_CHEATS
  _character=surface._character;
  #endif
	/*
	if (strstr(name,"\\pi.paa"))
	{
		LogF("Tex %s, rough %g",(const char *)name,surface._roughness);
	}
	*/

	_mipmapNeeded = INT_MAX;
	_mipmapWanted = INT_MAX;
}

Texture::~Texture()
{
	// if it is locked, it is too late to unlock it
	// actualy, if it is locked, it should not be destroyed
	// texture bank unlock all textures before destroying itself
	// unless whole bank is 
	DoAssert(!_refCountLocked);
	if( _inAnimation )
	{
		// if the texture is the last in some animated texture, remove whole animation
		// reference to any texture of the animation means reference to whole animation
		// delete any other textures in animation
		delete _inAnimation,_inAnimation=NULL;
	}
}

void Texture::Lock()
{
	if (_refCountLocked) return;
	_refCountLocked = true;
	AddRef();
}

void Texture::Unlock()
{
	if (!_refCountLocked) return;
	_refCountLocked = false;
	Release();
}

int Texture::AnimationLength() const
{
	if( !_inAnimation ) return 1; // no animation
	return _inAnimation->Size();
}

Texture *Texture::GetAnimation( int i ) const
{
	if( !_inAnimation ) return const_cast<Texture *>(this); // no animation
	//int n=_inAnimation->Size();
	//i%=_inAnimation->Size();
	//while( i<0 ) i+=n;
	//while( i>=n ) i-=n;
	Assert( i>=0 && i<_inAnimation->Size() );
	return (*_inAnimation)[i];
}

void Texture::PrepareMipmap( int wanted, int needed )
{
	saturateMin(_mipmapNeeded,needed);
	saturateMin(_mipmapWanted,wanted);
}

void Texture::ResetMipmap()
{
	_mipmapNeeded = INT_MAX;
	_mipmapWanted = INT_MAX;
}

bool NoTextures = false;

Ref<Texture> GlobLoadTexture( RStringB name )
{
	if (NoTextures) return NULL;
	if (name.GetLength()<=0) return NULL;
	if( AbstractTextBank::AnimatedNumber(name)>=0 )
	{
		AnimatedTexture *animated=GlobLoadTextureAnimated(name);
		return animated->Get(0);
	}
	return GLOB_ENGINE->TextBank()->Load(name);
}
Ref<Texture> GlobLoadTextureInterpolated(  RStringB n1, RStringB n2, float factor )
{
	if (NoTextures) return NULL;
	return GLOB_ENGINE->TextBank()->LoadInterpolated(n1,n2,factor);
}
AnimatedTexture *GlobLoadTextureAnimated( RStringB name )
{
	if (NoTextures) return NULL;
	return GLOB_ENGINE->TextBank()->LoadAnimated(name);
}

/*!
\patch_internal 1.43 Date 1/22/2002 by Ondra
- Fixed: Only class members of CfgSurfaces are loaded.
*/

AbstractTextBank::AbstractTextBank()
{
	// load surface information
	// load first from param file
	if( Pars.GetEntryCount()>0 )
	{
		const ParamEntry &par=Pars>>"CfgSurfaces";
		int i;
		for( i=0; i<par.GetEntryCount(); i++ )
		{
			const ParamEntry &entry = par.GetEntry(i);
			if (entry.IsClass())
			{
				SurfaceInfo info;
				info._name=entry>>"files";
				info._roughness=entry>>"rough";
				info._dustness=entry>>"dust";
				info._soundEnv=entry>>"soundEnviron";
        #if _ENABLE_CHEATS
        const ParamEntry *character = entry.FindEntry("character");
        if (character)
        {
          info._character=*character;
        }
        #endif
				_surfaces.Add(info);
			}
		}
	}
}

void AbstractTextBank::LockAllTextures()
{
	int n = NTextures();
	for ( int i=0; i<n; i++)
	{
		Texture *tex = GetTexture(i);
		if (tex) tex->Lock();
	}
}
/*!
\patch 1.35 Date 12/7/2001 by Ondra
- Fixed: Random crash during MP demo mission restart.
*/

void AbstractTextBank::UnlockAllTextures()
{
	int n = NTextures();
	for ( int i=0; i<n; i++)
	{
		Texture *tex = GetTexture(i);
		if (tex) tex->Unlock();
	}
	// check if any animated textures may be released
	for (int i=0; i<_animatedTextures.Size(); i++)
	{
		AnimatedTexture *anim = _animatedTextures[i];
		if (!anim) continue;
		bool isUsed = false;
		for (int a=0; a<anim->Size(); a++)
		{
			// MP demo crash fix:
			Texture *animA = anim->Get(a);
			if (animA && animA->RefCounter()>1)
			{
				isUsed = true;
				break;
			}
		}
		if (!isUsed)
		{
			_animatedTextures.Delete(i--);
		}
	}
}

//! Wildcard pattern matching function.

/*!
'*' may be used only as last character of the pattern
'*' matches any string in name
'?' matches any single character, not zero
*/

static bool PatternMatch( const char *name, const char *pattern )
{
	for(;;)
	{
		if( *name!=*pattern )
		{
			if (*pattern=='*') return true;
			if (*name==0) return false;
			// question marks matches any character, but not zero
			if (*pattern!='?') return false;
		}
		if (*pattern == 0) return true;
		Assert(*name!=0);
		name++,pattern++;
	}
	/*NOTREACHED*/
}

//! Wildcard pattern matching function for surface texture names.

/*!
caution: nasty trick to fix error in older data
final ?????? (six question marks) can match with 6 or 0 characters
*/

static bool PatternMatchQM6( const char *name, const char *pattern )
{
	// try matching full pattern first
	bool match = PatternMatch(name,pattern);
	if (match)
	{
		#if _ENABLE_REPORT
			if (strchr(pattern,'?') || strchr(pattern,'*'))
			{
				LogF("PatternMatch: %s matches %s",name,pattern);
			}
		#endif
		return match;
	}
	// full pattern does not match
	// check if there is trailing ??????
	int patlen = strlen(pattern);
	int namlen = strlen(name);
	// quick rejection test:
	// if ?????? should be matching, length of pattern must be length of name + 6
	if (namlen+6!=patlen) return false;
	int qmCount = 0;
	int patend = patlen;
	while (patend>0 && pattern[patend-1]=='?')
	{
		qmCount++;
		patend--;
	}
	if (qmCount<6) return false;

	// create short pattern
	const int shortPatBufSize = 256;
	int shortPatLen = patlen-6;
	// if pattern would overflow short pattern buffer, terminate
	if (shortPatLen>=shortPatBufSize) return false;
	BString<shortPatBufSize> shortPat;
	strncpy(shortPat,pattern,shortPatLen);
	shortPat[shortPatLen]=0;
	match = PatternMatch(name,shortPat);
	#if _ENABLE_REPORT
		if (match)
		{
			LogF("PatternMatchQM6: %s matches %s (%s)",name,pattern,shortPat.cstr());
		}
	#endif
	return match;
}

int AbstractTextBank::FindSurface( const char *name ) const
{
	// pattern matching ('?')
	// note: wildcards are not in name, but in _surface[i]
	int i;
	for( i=0; i<_surfaces.Size(); i++ )
	{
		if( PatternMatchQM6(name,_surfaces[i]._name) ) return i;
	}
	return -1;
}

const SurfaceInfo &AbstractTextBank::GetSurface( const char *name ) const
{
	// get only pure name (without path or extension)
	const char *fName=strrchr(name,'\\');
	if( fName ) fName++;
	else fName=name;
	char sName[80];
	strcpy(sName,fName);
	char *ext=strrchr(sName,'.');
	if( ext ) *ext=0;
	// search 
	int index=FindSurface(sName);
	if( index<0 )
	{
		index=FindSurface("default");
		if( index<0 )
		{
			static const SurfaceInfo info;
			return info;
		}
		Assert( index>=0 );
	}
	return _surfaces[index];
}

AbstractTextBank::~AbstractTextBank()
{
}

/*!
\patch_internal 1.28 Date 10/25/2001 by Ondra
- Fixed: crash in texture bank destruction during Abort (like in ErrorMessage).
*/

void AbstractTextBank::DeleteAllAnimated()
{
	Assert( _animatedTextures.Size()==0 );
	_animatedTextures.Clear();

}

void AbstractTextBank::StartFrame()
{
}

void AbstractTextBank::FinishFrame()
{
}

LLink<Texture> DefaultTexture;
