#ifdef _MSC_VER
#pragma once
#endif

#ifndef _AIRPLANE_HPP
#define _AIRPLANE_HPP

#include "transport.hpp"
#include "indicator.hpp"
#include "shots.hpp"

class AirplaneType: public TransportType
{
	typedef TransportType base;

	friend class Airplane;
	friend class AirplaneAuto;

	protected:
//	Vector3 _lightPos,_lightDir;
	
	Vector3 _pilotPos; // neutral neck and head positions
	Vector3 _gunnerPos; // current head position

	//Vector3 _missileLPos,_missileRPos;
	Vector3 _rocketLPos,_rocketRPos;

	Vector3 _gunPos,_gunDir; // gun position and direction

	Vector3 _ejectSpeed;

	AnimationAnimatedTexture _animFire;

	AnimationRotation _rFlap,_lFlap;
	AnimationRotation _rAileronT,_lAileronT;
	AnimationRotation _rAileronB,_lAileronB;
	AnimationRotation _rRudder,_lRudder;
	AnimationRotation _rElevator,_lElevator;

	enum {MaxRotors=8};
	AnimationRotation _rotors[MaxRotors];
	AnimationSection _rotorStill, _rotorMove;

	AnimationWithCenter _fWheel,_rWheel,_lWheel;
	//AnimationWithCenter _gun; // no animated gun

	Indicator _altRadarIndicator;
	Indicator _altRadarIndicator2;
	Indicator _altBaroIndicator;
	Indicator _speedIndicator;
	Indicator _vertSpeedIndicator;
	Indicator _vertSpeedIndicator2;
	Indicator _rpmIndicator;
	Indicator _compass, _compass2;
	IndicatorWatch _watch, _watch2;
	AnimationWithCenter _vario;
	Vector3 _varioDirection[MAX_LOD_LEVELS];

	Vector3 _lDust,_rDust;

	float _minGunTurn,_maxGunTurn; // gun movement
	float _minGunElev,_maxGunElev;
	bool _gearRetracting;
	float _landingSpeed;
	float _takeOffSpeed;
	float _stallSpeed;
	float _aileronSensitivity;
	float _elevatorSensitivity;
	float _wheelSteeringSensitivity;
	float _noseDownCoef;
	float _landingAoa;
	float _flapsFrictionCoef;
	
	public:
	AirplaneType( const ParamEntry *param );
	virtual void Load(const ParamEntry &par);
	void InitShape();
};

class Airplane: public Transport
{
	typedef Transport base;

	protected:
	Ref<AbstractWave> _servoSound;
//	Ref<LightReflectorOnVehicle> _light;

	
	bool _pilotGear;

	bool _rocketLRToggle;
	bool _gearDammage;

	int _pilotFlaps;

	float _pilotBrake;

	float _rndFrequency; // volume control - low down hero sound
	float _servoVol;

	float _gunYRot,_gunYRotWanted;
	float _gunXRot,_gunXRotWanted;
	float _gunXSpeed,_gunYSpeed;

	// pilot can directly coordinate these parameters
	float _rpm; // on/off
	float _thrust,_thrustWanted; // turning motor on/off
	float _rotorSpeed;
	float _rotorPosition;
	float _elevator,_elevatorWanted;
	float _rudder,_rudderWanted;
	float _aileron,_aileronWanted;

	float _flaps; // actual flap position
	float _gearsUp; // actual gear position
	float _brake;

	DustSource _leftDust,_rightDust;

	Vector3 _lastAngVelocity; // helper for prediction

	enum SweepState {SweepDisengage,SweepTurn,SweepEngage,SweepFire};
	SweepState _sweepState;
	Time _sweepDelay;
	LinkTarget _sweepTarget;
	Vector3 _sweepDir;

	WeaponLightSource _mGunFire;
	int _mGunFireFrames;
	UITime _mGunFireTime;
	int _mGunFirePhase;
//	WeaponFireSource _mGunFire;
	WeaponCloudsSource _mGunClouds;

	public:
	Airplane( VehicleType *name, Person *pilot );
	~Airplane();
	
	const AirplaneType *Type() const
	{
		return static_cast<const AirplaneType *>(GetType());
	}

	bool IsAnimated( int level ) const; // appearence changed with Animate
	bool IsAnimatedShadow( int level ) const; // shadow changed with Animate
	void Animate( int level );
	void Deanimate( int level );

	bool CastProxyShadow(int level, int index) const;
	int GetProxyComplexity
	(
		int level, const FrameBase &pos, float dist2
	) const;
	void DrawProxies
	(
		int level, ClipFlags clipFlags,
		const Matrix4 &transform, const Matrix4 &invTransform,
		float dist2, float z2, const LightList &lights
	);
	Object *GetProxy
	(
		LODShapeWithShadow *&shape,
		int level,
		Matrix4 &transform, Matrix4 &invTransform,
		const FrameBase &parentPos, int i
	) const;

	void Draw( int level, ClipFlags clipFlags, const FrameBase &pos );

	float DetectStall() const;
	void PerformFF( FFEffects &effects );
	void Simulate( float deltaT, SimulationImportance prec );

	void Sound( bool inside, float deltaT );
	void UnloadSound();

	float GetEngineVol( float &freq ) const;
	float GetEnvironVol( float &freq ) const;

	Matrix4 InsideCamera( CameraType camType ) const;
	int InsideLOD( CameraType camType ) const;
	void InitVirtual( CameraType camType, float &heading, float &dive, float &fov ) const;
	void LimitVirtual( CameraType camType, float &heading, float &dive, float &fov ) const;
	bool IsVirtual( CameraType camType ) const {return true;}
	bool IsContinuous(CameraType camType ) const;
	float TrackingSpeed() const {return 200;}

	bool Airborne() const;

	SimulationImportance WorstImportance() const {return SimulateVisibleFar;}
	SimulationImportance BestImportance() const {return SimulateCamera;}

  Vector3 GetEyeDirection() const;

	bool HasHUD() const {return true;}

	LSError Serialize(ParamArchive &ar);
};

enum AirplaneState
{
	// taxi-in: move to start of runway
	TaxiIn,
	Takeoff,Flight,Marshall,Approach,Final,Landing,Touchdown,WaveOff,
	// taxi-off: move out of runway
	TaxiOff,
};

class AirplaneAuto: public Airplane
{
	typedef Airplane base;

	protected:
	// full autopilot paramters - used for AI
	float _pilotSpeed;
	float _pilotHeading;
	float _pilotHeight;
	float _defPilotHeight;

	// when you start avoiding, continue for some time
	Time _pilotAvoidHigh;
	float _pilotAvoidHighHeight;
	Time _pilotAvoidLow;
	float _pilotAvoidLowHeight;

	// fly by wire - used for manual keyboard control
	float _pilotDive;
	float _pilotBank;

	float _dirCompensate;  // how much we compensate for estimated change

	bool _pilotHelperBankDive; // helpers for keyboard pilot
	bool _pilotHelperHeight; // helpers for keyboard pilot
	bool _pilotHelperDir; // used for mouse control and AI
	bool _pilotHelperThrust; // used for mouse control and AI

	bool _pilotHeadingSet; // keep pilot heading
	bool _pilotDiveSet; // keep pilot dive

	bool _pressedForward,_pressedBack; // recognize fast speed-up, fast brake
	bool _targetOutOfAim;

	float _forceDive; // dive necessary for aiming

	AutopilotState _state;
	AirplaneState _planeState;

	public:
	AirplaneAuto( VehicleType *name, Person *pilot );

	void Simulate( float deltaT, SimulationImportance prec );
	void AvoidGround( float minHeight );

	void DrawDiags();

	bool IsStopped() const;
	void DammageCrew(EntityAI *killer, float howMuch, RString ammo);
	void Eject(AIUnit *unit);
	void Land();
	void CancelLand();
	void SetFlyingHeight(float val);

	bool IsAway( float factor=1 );

	void EngineOn();
	void EngineOff();

	void GetActions(UIActions &actions, AIUnit *unit, bool now);
	RString GetActionName(const UIAction &action);
	void PerformAction(const UIAction &action, AIUnit *unit);

	bool FireWeapon( int weapon, TargetType *target );
	void FireWeaponEffects(int weapon, const Magazine *magazine,EntityAI *target);

	void KeyboardAny(AIUnit *unit, float deltaT);

	void DetectControlMode() const;
	void KeyboardPilot(AIUnit *unit, float deltaT );
	void JoystickDirPilot( float deltaT );
	void JoystickThrustPilot( float deltaT );
	void FakePilot( float deltaT );

	enum PathResult
	{
		PathFinished,
		PathAborted, // obstacle
		PathGoing,
	};

	PathResult PathAutopilot( const Vector3 *path, int nPath );

	bool TaxiOffAutopilot();
	bool TaxiInAutopilot();

	void Autopilot
	(
		Vector3Par target, Vector3Par tgtSpeed, // target
		Vector3Par direction, Vector3Par speed // wanted values
	);
	void ResetAutopilot();

	float MakeAirborne();

	RString DiagText() const;

	// AI interface

	bool CalculateAimWeapon( int weapon, Vector3 &dir, Target *target );
	bool AimWeapon( int weapon, Vector3Par direction );
	bool AimWeapon( int weapon, Target *target );
	Matrix4 GunTransform() const;
	Vector3 GetWeaponDirection( int weapon ) const;
	void LimitCursor( CameraType camType, Vector3 &dir ) const;
	float GetAimed( int weapon, Target *target ) const;

	float GetFieldCost( const GeographyInfo &info ) const;
	float GetCost( const GeographyInfo &info ) const;
	float GetCostTurn( int difDir ) const;

	float FireInRange( int weapon, float &timeToAim, const Target &target ) const;

	void MoveWeapons( float deltaT );

#if _ENABLE_AI
	void AvoidCollision();
	void AIGunner(AIUnit *unit, float deltaT);
	void AIPilot(AIUnit *unit, float deltaT );
#endif

	LSError Serialize(ParamArchive &ar);

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx);
};

#endif

