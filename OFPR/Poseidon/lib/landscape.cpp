/*!
\file
Landscape drawing
*/
#include "wpch.hpp"
#include "lights.hpp"
#include "camera.hpp"
#include "tlVertex.hpp"
#include "world.hpp"
#include "engine.hpp"
#include "landscape.hpp"
#include "paramFileExt.hpp"
#include "global.hpp"
#include <El/Common/randomGen.hpp>
#include <El/Common/perfLog.hpp>
#include "operMap.hpp"
#include "SpecLods.hpp"
#include "perfProf.hpp"
#include "diagModes.hpp"

#define LOG_SEG 0

//#define WATER_SPEED 1.5f // wave animation
#define WATER_TEX_SPEED 0.4f // texture u,v animation

static const PreloadedShape CloudShapes[N_CLOUDS]={Cloud2,Cloud3,Cloud1,Cloud4};

void MemoryCleanUp();

void Landscape::Init()
{
  int i,x,z;
  // empty texture list
  ClipFlags clipSky=ClipAll&~ClipBack|ClipUser0;
  {
    // load sky background shape
    Ref<LODShapeWithShadow> lShape=Shapes.New("data3D\\obloha.p3d",false,false);
    //Ref<LODShapeWithShadow> lShape = GScene->Preloaded(SkySphere);
    if( (lShape->Special()&IsAlphaFog)==0 )
    { // this should be done only once
      Shape *shape=lShape->LevelOpaque(0);
      for( i=0; i<shape->NPos(); i++ )
      {
        shape->SetClip(i,ClipLightSky|clipSky|ClipFogDisable);
      }
      for( Offset f=shape->BeginFaces(); f<shape->EndFaces(); shape->NextFace(f) )
      {
        Poly &face=shape->Face(f);
        face.OrSpecial(BestMipmap);
        //SetMipmapLevel(0,0);
      }
      vecAlign Matrix4 scale(MScale,100);
      lShape->InternalTransform(scale);
      lShape->SetSpecial(NoZBuf|NoZWrite|ClampV|NoShadow|IsAlphaFog|FogDisabled);
      shape->CalculateHints();
      lShape->CalculateHints();
      lShape->AllowAnimation();
    }
    _skyObject=new ObjectPlain(lShape,-1);
  }
  {
    // load star background shape
    Ref<LODShapeWithShadow> lShape=Shapes.New("data3D\\stars.p3d",false,false);
    if( (lShape->Special()&IsAlphaFog)==0 )
    { // this should be done only once
      lShape->SetSpecial(NoZBuf|IsAlphaFog|IsAlpha|NoClamp);
      Shape *shape=lShape->LevelOpaque(0);
      for( i=0; i<shape->NPos(); i++ )
      {

        ClipFlags hints=shape->Clip(i)&ClipUserMask;
        shape->SetClip(i,ClipLightStars|clipSky|ClipFogDisable|hints);
      }
      //lShape->InternalTransform(Matrix4(MScale,100000));
      lShape->InternalTransform(Matrix4(MScale,1000));
      shape->CalculateHints();
      lShape->CalculateHints();
      lShape->SetAutoCenter(false);
      lShape->CalculateBoundingSphere();
      lShape->AllowAnimation();
    }
    _starsObject=new ObjectPlain(lShape,-1);
  }
  {
    // load blended horizont shape
    ClipFlags clipHor=ClipAll&~ClipBack|ClipUser0;
    //Ref<LODShapeWithShadow> lShape = GScene->Preloaded(HorizontObject);
    Ref<LODShapeWithShadow> lShape=Shapes.New("data3D\\horizont.p3d",false,false);
    if( (lShape->Special()&NoZWrite)==0 )
    { // this should be done only once
      lShape->SetSpecial(ClampV|NoShadow|NoZWrite|IsAlpha);
      Shape *shape=lShape->LevelOpaque(0);
      int i;
      for( i=0; i<shape->NPos(); i++ )
      {
        //const ClipFlags ambFlags = ClipLightAmbient|(ClipUserStep*MSInShadow);
        const ClipFlags ambFlags = (ClipUserStep*MSInShadow);
        shape->SetClip(i,ambFlags|clipHor);
      }
      // horizontal range of horizont.p3d is -100..100
      // we need to enlarge it to BACKG_Z
      //shape->SortVertices();
      shape->CalculateHints();
      lShape->CalculateHints();
      //lShape->InternalTransform(Matrix4(MScale,BACKG_Z*(1.0/120)));
      lShape->AllowAnimation();
    }
    _horizontObject=new ObjectPlain(lShape,-1);
  }
  {
    // load sun and moon shape
    const int sunSpec=
    (
      IsAlpha|NoZBuf|NoZWrite|ClampV|ClampU|
      SpecLighting|
      NoShadow|IsAlphaFog|FogDisabled
    );
    Ref<LODShapeWithShadow> haloShape=Shapes.New("data3D\\sunHalo.p3d",false,false);
    Shape *haloShape0=haloShape->LevelOpaque(0);
    int i;
    {
      // load sun shape
      Ref<LODShapeWithShadow> sunShape=Shapes.New("data3D\\sun.p3d",false,false);
      Shape *sunShape0=sunShape->LevelOpaque(0);
      for( i=0; i<sunShape0->NPos(); i++ )
      {
        sunShape0->SetClip(i,ClipLightSun|ClipFogDisable|ClipDecalNormal|clipSky);
      }
      for( i=0; i<haloShape0->NPos(); i++ )
      {
        haloShape0->SetClip(i,ClipLightSunHalo|ClipFogDisable|ClipDecalNormal|clipSky);
      }
      // halo behind the sun
      Ref<LODShapeWithShadow> sunWithHalo=new LODShapeWithShadow(*haloShape);
      sunWithHalo->LevelOpaque(0)->Merge(sunShape0,MIdentity);
      sunWithHalo->LevelOpaque(0)->CalculateHints();
      sunWithHalo->CalculateHints();
      sunWithHalo->SetSpecial(sunSpec);
      sunWithHalo->AllowAnimation();
      _sunObject=new ObjectPlain(sunWithHalo,-1);
    }
    {
      // load moon shape
      Ref<LODShapeWithShadow> lShape=Shapes.New("data3D\\moon.p3d",false,false);
      Shape *moonShape0=lShape->LevelOpaque(0);
      if( (lShape->Special()&IsAlphaFog)==0 )
      {
        lShape->OrSpecial(IsAlphaFog);
        for( i=0; i<moonShape0->NPos(); i++ )
        {
          moonShape0->SetClip(i,ClipLightMoon|ClipFogDisable|clipSky);
        }
        lShape->InternalTransform(Matrix4(MScale,0.25));
      }
      for( i=0; i<haloShape0->NPos(); i++ )
      {
        haloShape0->SetClip(i,ClipLightMoonHalo|ClipFogDisable|clipSky);
      }
      // halo behind the moon
      Ref<LODShapeWithShadow> moonWithHalo=new LODShapeWithShadow(*haloShape);
      moonWithHalo->LevelOpaque(0)->Merge(moonShape0,MIdentity);
      moonWithHalo->LevelOpaque(0)->CalculateHints();
      moonWithHalo->CalculateHints();
      //moonWithHalo->SetSpecial(sunSpec|IsFlare);
      moonWithHalo->SetSpecial(sunSpec|IsLight);
      moonWithHalo->AllowAnimation();
      _moonObject=new ObjectPlain(moonWithHalo,-1);
    }
  }
  _weather.Init(); // reinit weather

  _texture.Clear();
  SetTexture(0,"landtext\\mo.pac");
  // empty landscape
  for( x=0; x<_landRange; x++ ) for( z=0; z<_landRange; z++ )
  {
    SetTex(x,z,0);
    SetData(x,z,0);
  }
  // empty object list
  for( x=0; x<_landRange; x++ ) for( z=0; z<_landRange; z++ )
  {
    _objects(x,z).Clear();
  }

  const ParamEntry &entry = Pars >> "CfgSurfaces" >> "Water";
  _waterSurface._name = entry >> "files";
  _waterSurface._roughness = entry >> "rough";
  _waterSurface._dustness = entry >> "dust";
  _waterSurface._soundEnv = entry >> "soundEnviron";

  _mountains.Clear();

  // good time to clean up allocator
  // now there should quite a lot of free memory
  MemoryCleanUp();
  _objectId=-1;
  // reenter all vehicles
  if( GWorld )
  {
    int v;
    for( v=0; v<GWorld->NVehicles(); v++ )
    {
      AddObject(GWorld->GetVehicle(v));
    }
    for( v=0; v<GWorld->NFastVehicles(); v++ )
    {
      AddObject(GWorld->GetFastVehicle(v));
    }
  }

}

void Weather::SetSky( Landscape *land, RStringB name )
{
  _sky=GlobLoadTexture(name);
  if (_sky)
  {
    _sky->SetMaxSize(1024); // no limit
    _sky->ASetNMipmaps(1); // use only the finest mipmap
  }
  if( land ) land->SetSkyTexture(_sky);
}

void Weather::SetSky( Landscape *land, RStringB n1, RStringB n2, float factor )
{
  _sky=GlobLoadTextureInterpolated(n1,n2,factor);
  if (_sky)
  {
    _sky->SetMaxSize(1024); // no limit
    _sky->ASetNMipmaps(1);
  }
  if( land ) land->SetSkyTexture(_sky);
}

class ThunderBolt: public Vehicle
{
  SoundPars _soundPars;
  float _size;
  bool _soundDone;
  float _phase;
  Ref<LightPoint> _light;

  public:
  ThunderBolt
  (
    LODShapeWithShadow *shape, float size,
    const SoundPars &pars
  );
  void Simulate( float deltaT, SimulationImportance prec );
  bool AnimateTexture( int level, bool shadow );
  bool DeanimateTexture( int level, bool shadow );
};

ThunderBolt::ThunderBolt
(
  LODShapeWithShadow *shape, float size,
  const SoundPars &pars
)
:Vehicle(shape,VehicleTypes.New("ThunderBolt"),-1),
_size(size),_soundPars(pars),_soundDone(false),_phase(0)
{
}

static const Color ThunderBoltColor(1,1,2);
static const Color ThunderBoltAmbient(0.5,0.5,1);

void ThunderBolt::Simulate( float deltaT, SimulationImportance prec )
{
  bool lightVisible=(toIntFloor(_phase*4)&1)!=0;
  if( !_light && lightVisible )
  {
    _light=new LightPoint
    (
      ThunderBoltColor,ThunderBoltAmbient
    );
    _light->SetBrightness(1000);
    _light->SetPosition(Position());
    GLOB_SCENE->AddLight(_light);
  }
  if( _light )
  {
    const int ThunderBoltPhases=3;
    float animation=_phase*ThunderBoltPhases;
    //int phase=toIntFloor(animation);
    float frac=animation-toIntFloor(animation);
    float intensity=(0.5-fabs(frac-0.5))*2.0;
    float useAverage=deltaT*ThunderBoltPhases;
    saturateMin(useAverage,1);
    intensity=0.5*useAverage+intensity*(1-useAverage);
    saturateMax(intensity,0);
    // low down intensity with time
    intensity*=1-_phase;
    _light->SetDiffuse(ThunderBoltColor*intensity);
    _light->SetAmbient(ThunderBoltAmbient*intensity);
  }
  if( !_soundDone )
  {
    _soundDone=true;
    // sound of explosion
    float rndFreq=GRandGen.RandomValue()*0.1+0.95;
    AbstractWave *sound=GSoundScene->OpenAndPlayOnce
    (
      _soundPars.name,Position(),VZero,
      _soundPars.vol,_soundPars.freq*rndFreq
    );
    if( sound )
    {
      GSoundScene->SimulateSpeedOfSound(sound);
      GSoundScene->AddSound(sound);
    }
  }
  _phase+=deltaT*3;
  if( _phase>=1 )
  {
    SetDelete();
  }
}

bool ThunderBolt::AnimateTexture( int level, bool shadow )
{
  Shape *shape=_shape->Level(level);
  if( !shape ) return false;
  shape->Face(shape->BeginFaces()).AnimateTexture(_phase);
  return true;
}

bool ThunderBolt::DeanimateTexture( int level, bool shadow )
{
  return true;
}


void Weather::SetClouds( float alpha, float brightness, float speed, float through )
{
  saturate(alpha,0,1);
  saturate(brightness,0,1);
  saturate(through,0,1);
  _skyThrough=through;
  _cloudsAlpha=alpha;
  _cloudsBrightness=brightness;
  _cloudsSpeed=speed;
}

struct WeatherBasic
{
  float overcast;
  RStringB sky;
  float alpha,bright,speed,through;
  WeatherBasic
  (
    float overcastI, RStringB skyI,
    float alphaI, float brightI, float speedI, float throughI
  )
  {
    overcast=overcastI;
    sky=skyI;
    alpha=alphaI,bright=brightI,speed=speedI,through=throughI;
  }
};

static const WeatherBasic WeatherBasics[]=
{
  //        overcast, sky,            alphaI, brightI, speedI, through
  WeatherBasic(-0.08,"data\\jasno.pac",   0,   1.0, 0.2,  1.0),
  WeatherBasic(0.50, "data\\oblacno.pac", 0.9, 1.0, 0.35, 0.7),
  WeatherBasic(0.66, "data\\zatazeno.pac",0.9, 0.7, 0.6,  0.1),
  WeatherBasic(1.0,  "data\\zatazeno.pac",1.0, 0.5, 1.0,  0.0)
};

const int NWeathers=sizeof(WeatherBasics)/sizeof(*WeatherBasics);

void Weather::Init()
{
  _thunderBoltTime=Glob.time-1;
  _overcastSetSky = -1.0;
  _overcastSetClouds = -1.0;
  _cloudsPos=0;
  _rainDensity=0;
  _rainDensityWanted=0;
  _rainDensitySpeed=1;
}

Weather::Weather()
{
  _windSpeed = VZero;
  Init();
}

/*
float Weather::GetOvercast() const
{
  return _overcastSetClouds;
}
*/

void Weather::SetRain(float density, float time)
{
  saturate(density,0,1);
  saturateMax(time,0.001);
  _rainDensityWanted = density;
  _rainDensitySpeed = fabs(_rainDensityWanted-_rainDensity)/time;
}

void Weather::SetOvercast( Landscape *land, float overcast )
{
  // separate calculations for sky texture / clouds layer

  saturate(overcast,0,1);
  if( fabs(_overcastSetClouds-overcast)>0.001 )
  {
    //LogF("overcastClouds %.3f->%.3f",_overcastSetClouds,overcast);
    // find nearest before and nearest after
    _overcastSetClouds=overcast;
    int index;
    for( index=0; index<NWeathers; index++ )
    {
      const WeatherBasic &basic=WeatherBasics[index];
      if( basic.overcast>overcast ) break;
    }
    if( index<=0 )
    {
      // use basic (clear)
      const WeatherBasic &basic=WeatherBasics[0];
      SetClouds(basic.alpha,basic.bright,basic.speed,basic.through);
      return;
    }
    if( index>=NWeathers )
    {
      const WeatherBasic &basic=WeatherBasics[NWeathers-1];
      SetClouds(basic.alpha,basic.bright,basic.speed,basic.through);
      return;
    }
    // use combination of basic weathers index-1 and index
    const WeatherBasic &basicMin=WeatherBasics[index-1];
    const WeatherBasic &basicMax=WeatherBasics[index];
    float interpol=(overcast-basicMin.overcast)/(basicMax.overcast-basicMin.overcast);
    SetClouds
    (
      (basicMax.alpha-basicMin.alpha)*interpol+basicMin.alpha,
      (basicMax.bright-basicMin.bright)*interpol+basicMin.bright,
      (basicMax.speed-basicMin.speed)*interpol+basicMin.speed,
      (basicMax.through-basicMin.through)*interpol+basicMin.through
    );

    //LogF("bright %.3f",(basicMax.bright-basicMin.bright)*interpol+basicMin.bright);
  }
  if( fabs(_overcastSetSky-overcast)>0.01 )
  {
    //LogF("overcastSky %.3f->%.3f",_overcastSetSky,overcast);
    // find nearest before and nearest after
    _overcastSetSky=overcast;
    int index;
    for( index=0; index<NWeathers; index++ )
    {
      const WeatherBasic &basic=WeatherBasics[index];
      if( basic.overcast>overcast ) break;
    }
    if( index<=0 )
    {
      // use basic (clear)
      const WeatherBasic &basic=WeatherBasics[0];
      SetSky(land,basic.sky);
      return;
    }
    if( index>=NWeathers )
    {
      const WeatherBasic &basic=WeatherBasics[NWeathers-1];
      SetSky(land,basic.sky);
      return;
    }
    // use combination of basic weathers index-1 and index
    const WeatherBasic &basicMin=WeatherBasics[index-1];
    const WeatherBasic &basicMax=WeatherBasics[index];
    float interpol=(overcast-basicMin.overcast)/(basicMax.overcast-basicMin.overcast);
    SetSky(land,basicMin.sky,basicMax.sky,interpol);
  }
}

void Weather::SetFog( Landscape *land, float fog )
{
  saturate(fog,0,1);
  if( fabs(_fogSet-fog)<0.001 ) return;
  _fogSet=fog;
}

/*!
\patch 1.52 Date 4/22/2002 by Ondra
- Changed: Fog density is not dependent on setviewdistance for fog>0.4
*/

void Weather::MoveClouds( float deltaT )
{
  _cloudsPos+=_cloudsSpeed*deltaT;

  // simulate rain
  float maxRain=_overcastSetClouds*1.5-1;
  if( maxRain>0.01 )
  {
    if( fabs(_rainDensityWanted-_rainDensity)<1e-6 )
    {
      saturate(maxRain,0,1);
      _rainDensityWanted=_rainDensity+(GRandGen.RandomValue()-0.5)*0.5*maxRain;
      saturate(_rainDensityWanted,0,maxRain);
      _rainDensitySpeed=(GRandGen.RandomValue()+0.1)*0.01;
    }
    float delta=_rainDensityWanted-_rainDensity;
    Limit(delta,-_rainDensitySpeed*deltaT,+_rainDensitySpeed*deltaT);
    _rainDensity+=delta;
    saturate(_rainDensity,0,1);
    const float thold=0.4;
    if( maxRain>=thold )
    {
      // there is a chance of thunderbolt
      float thunderBoltDensity=(maxRain-thold)*(1/(1-thold));
      if( Glob.time>_thunderBoltTime )
      {
        // do thunderbolt effect
        const ParamEntry *cfg=NULL;
        if( GRandGen.RandomValue()<thunderBoltDensity )
        {
          cfg=&(Pars>>"CfgEffects">>"ThunderboltHeavy");
        }
        else
        {
          cfg=&(Pars>>"CfgEffects">>"ThunderboltNorm");
        }
        Ref<LODShapeWithShadow> shape=Shapes.New(GetShapeName((*cfg)>>"model"),false,false);

        Shape *shape0=shape->LevelOpaque(0);
        for( int v=0; v<shape0->NPos(); v++ )
        {
          shape0->SetClip(v,ClipFogSky|(ClipAll&~ClipBack));
        }
        shape0->CalculateHints();

        // random bolt position
        // select the highest place of some random points
        Vector3Val cPos=GLOB_SCENE->GetCamera()->Position();
        Vector3 best(VZero);
        for( int c=10; --c>=0; )
        {
          Vector3 pos;
          pos.Init();
          pos[0]=(GRandGen.RandomValue()-0.5)*(60*LandGrid)+cPos.X();
          pos[2]=(GRandGen.RandomValue()-0.5)*(60*LandGrid)+cPos.Z();
          pos[1]=GLandscape->SurfaceY(pos[0],pos[2]);
          if( pos[1]>best[1] ) best=pos;
        }
        // use difefent sound for near/far sounds
        float size=5;
        float dist2=best.Distance2(cPos);
        SoundPars pars;
        const float minDist=300;
        if( dist2<Square(minDist) )
        {
          Vector3 norm = (best-cPos);
          best=norm.Normalized()*minDist+cPos;
          best[1]=GLandscape->SurfaceY(best[0],best[2]);
          dist2=best.Distance2(cPos);
        }
        if( dist2>Square(1000) ) GetValue(pars, (*cfg)>>"soundFar");
        else GetValue(pars, (*cfg)>>"soundNear");
        ThunderBolt *bolt=new ThunderBolt(shape,size,pars);
        bolt->SetScale(size);
        bolt->SetPosition(best+shape->BoundingCenter()*size);
        GWorld->AddCloudlet(bolt);
        _thunderBoltTime=Glob.time+GRandGen.RandomValue()*5/(thunderBoltDensity+0.001);

        #if 0
        GLOB_ENGINE->ShowMessage
        (
          5000,"Bolt size %.1f, distance %.1f, next %.1f",
          size,sqrt(dist2),_thunderBoltTime-Glob.time
        );
        #endif
      }
    }
  }
  else
  {
    _rainDensityWanted=0;
    _rainDensity=0;
    _rainDensitySpeed=0.01;
  }

  {
    // night and weather visibility
    float rainVisibility=(1-_rainDensity)*TACTICAL_VISIBILITY+350*_rainDensity;
    // night: very limited visibility
    const LightSun *sun=GLOB_SCENE->MainLight();
    float nightVisibility=sun->GetDiffuse().R()*4;
    float fogVisibility = 1.0f-_fogSet*0.95f;
    saturate(nightVisibility,0.75f,1);

    float noFogVisibility = rainVisibility*nightVisibility;

    float defaultFogDistance = floatMin(900,noFogVisibility)*fogVisibility;
    float currentFogDistance = noFogVisibility*fogVisibility;

    float tacRange = defaultFogDistance;
    const float defFogThold = 0.6f;
    if (fogVisibility>defFogThold)
    {
      float iFactor = (fogVisibility-defFogThold)*(1.0f/(1.0f-defFogThold));
      tacRange = iFactor*currentFogDistance + (1-iFactor)*defaultFogDistance;
    }

    /*
    LogF
    (
      "Tactical %.1f, fog %.1f, tacRange %.1f, def %.1f, cur %.1f",
      TACTICAL_VISIBILITY,fogVisibility,tacRange,
      defaultFogDistance,currentFogDistance
    );
    */
    // when fog is very dense
    // (corresponding to <300 m with 900 m viewdistance, daytime and no rain)
    // it should be related to default visibility (900 m)
    // when there is no fog, it should be related to user-selected visibility

    saturate(tacRange,0.1,1e6);
    GLOB_SCENE->SetTacticalVisibility(tacRange,tacRange);
  }

  #if 0
  if( _thunderBoltTime-Glob.time>5 )
  {
    GLOB_ENGINE->ShowMessage
    (
      100,"Rain %.3f->%.3f (*%.3f), Bolt %.1f",
      _rainDensity,_rainDensityWanted,_rainDensitySpeed,
      _thunderBoltTime-Glob.time
    );
  }
  #endif

  if (Glob.time>_lastWindSpeedChange+5)
  {
    _lastWindSpeedChange = Glob.time;
    _windSpeed[0] += GRandGen.PlusMinus(0,1);
    _windSpeed[2] += GRandGen.PlusMinus(0,1);
    float maxWind = _overcastSetClouds*4 + 1;
    saturate(_windSpeed[0],-maxWind,+maxWind);
    saturate(_windSpeed[2],-maxWind,+maxWind);

    // simulate wind gusts
    float gustTime = GRandGen.PlusMinus(0,8*_overcastSetClouds+2);
    _gustUntil = Glob.time+gustTime;
    _gust = Vector3
    (
      GRandGen.PlusMinus(0,4*_overcastSetClouds),
      GRandGen.PlusMinus(0,1*_overcastSetClouds),
      GRandGen.PlusMinus(0,4*_overcastSetClouds)
    );

  }
}

void Landscape::SetSkyTexture( Texture *texture )
{
  if( _skyObject )
  {
    Shape *shape=_skyObject->GetShape()->LevelOpaque(0);
    // note: face SetTexture is no longer necessary here
    // texture information is used only from sections
    for( Offset f=shape->BeginFaces(); f<shape->EndFaces(); shape->NextFace(f) )
    {
      Poly &face=shape->Face(f);
      face.SetTexture(texture);
    }
    if (shape->NSections()>=0)
    {
      shape->GetSection(0).properties.SetTexture(texture);
    }
  }
  _world->GetScene()->CalculateSkyColor(texture);
}

Texture *Landscape::SkyTexture(){return _weather.SkyTexture();}

const float maxTide=5;
const float maxWave=0.25;

void Landscape::Simulate(float deltaT)
{
  // simulate weather changes
  _weather.MoveClouds(deltaT);
  // simulate tide
  const LightSun *sun=GLOB_SCENE->MainLight();
  // consider sun and moon
  Vector3Val sunDir=sun->SunDirection();
  Vector3Val moonDir=sun->MoonDirection();
  //Vector3 sunTide=(VUp*sunDir)*sunDir;
  //Vector3 moonTide=(VUp*moonDir)*moonDir;
  Vector3 sunTide=sunDir[1]*sunDir;
  Vector3 moonTide=moonDir[1]*moonDir;
  float tide=(sunTide.Y()+moonTide.Y())*0.5;
  // consider moon
  _seaLevel=maxTide*tide;
  // simulate waves
  float wave=sin(2*H_PI*Glob.time.toFloat()*_seaWaveSpeed);
  //float waveSize=(GRandGen.RandomValue()*0.5+0.5)
  _seaLevelWave=_seaLevel+wave*maxWave;
  // simulate wind speed changes

}

Vector3 Landscape::GetWind() const
{
  Vector3 ret = _weather._windSpeed + Vector3(4,0,2)*_weather._overcastSetClouds;
  if (Glob.time<_weather._gustUntil)
  {
    ret += _weather._gust;
  }
  return ret;
}


bool Landscape::VerifyStructure() const
{
  return _segCache.VerifyStructure();
}

void Landscape::MakeShadows( Scene &scene )
{
  // make shadow shapes for all existing shapes
  /*
  int i;
  for( i=0; i<Shapes.Size(); i++ )
  {
    LODShapeWithShadow *shape=Shapes[i];
    if( shape && !(shape->Special()&NoShadow) )
    {
      shape->CreateShadow();
    }
  }
  */
}


Texture *Landscape::ClippedTexture( int z, int x ) const
{
  if( this_InRange(z,x) )
  {
    int index=GetTex(x,z);
    return _texture[index];
  }
  return _texture[0];
}

int Landscape::ClippedTextureIndex( int z, int x ) const
{
  if( this_InRange(z,x) ) return GetTex(x,z);
  return 0;
}


float Landscape::GetHeight( int x, int z ) const
{
  return ClippedData(x,z);
}

int Landscape::GetTexture( int z, int x ) const
{
  return ClippedTextureIndex(z,x);
}


bool Landscape::ClippedIsWater( int z, int x ) const
{
  if( !this_InRange(z,x) || !this_InRange(z-1,x-1) ) return true;
  if( GetTex(x,z)!=0 ) return false;
  if( GetTex(x-1,z)!=0 ) return false;
  if( GetTex(x,z-1)!=0 ) return false;
  if( GetTex(x-1,z-1)!=0 ) return false;
  return true;
}

void Landscape::ReleaseAllVBuffers()
{ 
  CLRefList<LandSegment> &segs = _segCache._segments;
  for( LandSegment *segi=segs.First(); segi; segi=segs.Next(segi) )
  {
    segi->_table.ReleaseVBuffer();
    segi->_wTable.ReleaseVBuffer();
  }

  if( GScene )
  {
    GScene->GetShadowCache().Clear();
    GScene->CleanUp();
  }
}

void Landscape::CreateAllVBuffers()
{
  CLRefList<LandSegment> &segs = _segCache._segments;
  for( LandSegment *segi=segs.First(); segi; segi=segs.Next(segi) )
  {
    if (!segi->_onlyWater)
    {
      //LogF("Land   %x",segi);
      segi->_table.ConvertToVBuffer(VBBigDiscardable);
    }
    if (segi->_someWater)
    {
      //LogF("Water  %x",segi);
      segi->_wTable.ConvertToVBuffer(VBBigDiscardable);
    }
  }
}

void Landscape::FlushCache()
{
//  Assert( GWorld->NVehicles()==0 );
#if _ENABLE_AI
  _operCache = CreateOperCache(this);
  _lockCache = CreateLockCache(this);
#endif
  _segCache.Clear();
  if( GScene ) GScene->GetShadowCache().Clear();
}

void Landscape::FillCache( const Frame &pos )
{
  LogF("Recreate caches %.1f,%.1f",pos.Position().X(),pos.Position().Z());
  _segCache.Fill(pos);
}

void Landscape::HeightChange( int x, int z, float y )
{
  Assert( this_TerrainInRange(x,z) );
  SetData(x,z,y);
  //FlushCache();
}

void Landscape::TextureChange( int x, int z, int id )
{
  Assert( this_InRange(x,z) );
  SetTex(x,z,id);
  //FlushCache();
}

/*!
\patch_internal 1.45 Date 2/20/2002 by Ondra
- New: Variable landscape dimensions.
*/

void Landscape::Dim(int x,int z,int rx, int rz, float landGrid)
{
  int xLog = 0;
  int xVal = x;
  while (xVal>1) xLog++,xVal >>= 1;

  int rxLog = 0;
  int rxVal = rx;
  while (rxVal>1) rxLog++,rxVal >>= 1;

  if (x!=z)
  {
    ErrorMessage("Landscape dimensions %dx%d not rectangular",x,z);
  }
  if (x!=1<<xLog)
  {
    ErrorMessage("Landscape dimensions %dx%d not power of 2",x,z);
  }
  if (rx!=rz)
  {
    ErrorMessage("Terrain dimensions %dx%d not rectangular",rx,rz);
  }
  if (rx!=1<<rxLog)
  {
    ErrorMessage("Terrain dimensions %dx%d not power of 2",rx,rz);
  }

  _landRange = x;
  _invLandRange = 1.0f/x;
  _landRangeMask = x-1;
  _landRangeLog = xLog;

  _terrainRange = rx;
  _terrainRangeMask = rx-1;
  _terrainRangeLog = rxLog;

  _geography.Dim(x,z);
  _soundMap.Dim(x,z);
  _tex.Dim(x,z);
  _objects.Dim(x,z);
  // TODO: _random should be byte only
  // _terrain, not _land
  _random.Dim(x,z);

  _data.Dim(rx,rz);
  Assert(rx==x);
  Assert(rz==z);

  SetLandGrid(landGrid);
}

void Landscape::SetLandGrid(float grid)
{
  #if _DEBUG
    // no object may be present
    for (int z=0; z<_terrainRange; z++)
    for (int x=0; x<_terrainRange; x++)
    {
      const ObjectList &ol = _objects(x,z);
      Assert(ol.Size()==0);
    }
  #endif
  _landGrid = grid;
  _invLandGrid = 1/grid;
  int terrainLog = _terrainRangeLog-_landRangeLog;
  float invTerrainCoef = 1.0/(1<<terrainLog);
  _terrainGrid = grid*invTerrainCoef;
  _invTerrainGrid = 1/_terrainGrid;
}

void Landscape::DoConstruct( Engine *engine, World *world )
{
  _engine=engine;
  _world=world;
  _texture.Clear();
  //for( int i=0; i<MAX_NETWORKS; i++ ) _networks[i]=NULL;
  _operCache = CreateOperCache(this);
  _lockCache = CreateLockCache(this);

  _seaLevel=0; // sea level with tide
  _seaLevelWave=0; // sea level with wave effects
  _seaWaveSpeed=0.08;
  _lastFindObjectX=-1; // no cached query
  _lastFindObjectZ=-1;
}

Landscape *GLandscape;

#pragma warning(disable:4355)

Landscape::Landscape(Engine *engine, World *world, bool nets )
:_nets(nets),
_randGen(8799656,148756)
{
  //Dim(256,256);
  Dim(32,32,32,32,50);

  DoConstruct(engine,world);
  Init();
}

// sin approximation is x+x^3/3!+x^5/5!

inline float MoveWater( float x, float z, const float coef=1 )
{
  // x,z usually from 0 to _landRange
  float sinArg=Glob.time.toFloat()*WATER_TEX_SPEED*coef+x+z;
  x=fastFmod(sinArg,H_PI*2);
  bool negative=false;
  if( x>H_PI ) x-=H_PI,negative=true;
  if( x>H_PI*0.5 ) x=H_PI-x;
  float xPow2=x*x;
  float xPow3=xPow2*x;
  float xPow5=xPow3*xPow2;
  float sinVal=x-xPow3*(1.0/2/3)+xPow5*(1.0/2/3/4/5);
  if( negative ) sinVal=-sinVal;
  AssertDebug( fabs(sinVal-sin(sinArg))<1e-2 );
  return sinVal*0.15;
}

RandomTable SeedTable;


DEFINE_FAST_ALLOCATOR(LandSegment)

LandSegment::LandSegment()
{
  _lastUsed=Glob.time;
  Clear();
}

LandSegment::~LandSegment()
{
}

void LandSegment::Clear()
{
  _table.Clear();
  _wTable.Clear();
  _rect.xBeg=INT_MAX,_rect.xEnd=INT_MIN; // invalid rectangle
  _rect.zBeg=INT_MAX,_rect.zEnd=INT_MIN;
  _valid=false;
  _someWater=false;
  _offset = VZero;
}

bool LandSegment::VerifyStructure() const
{
  return true;
}

bool LandSegment::ValidFor( const LandBegEnd &rect ) const
{
  if( !_valid ) return false;
  if( _rect.xBeg>rect.xBeg ) return false;
  if( _rect.xEnd<rect.xEnd ) return false;
  if( _rect.zBeg>rect.zBeg ) return false;
  if( _rect.zEnd<rect.zEnd ) return false;
  return true;
}

void LandSegment::CalcBSphere()
{
  // scan all vertices
  /*
  const VertexTable &table=_table;
  Vector3 minB(+1e10,+1e10,+1e10);
  Vector3 maxB(-1e10,-1e10,-1e10);
  for( int i=0; i<table.NPos(); i++ )
  {
    const V3 &pos=table.Pos(i);
    CheckMinMax(minB,maxB,pos);
  }
  _bSphere=(minB+maxB)*0.5;
  float maxDist2=0;
  for( int i=0; i<table.NPos(); i++ )
  {
    const V3 &pos=table.Pos(i);
    float dist2=pos.Distance2(_bSphere);
    saturateMax(maxDist2,dist2);
  }
  _bRadius=sqrt(maxDist2);
  */
}

void LandSegment::CalcWBSphere()
{
  /*
  // scan all water vertices
  // scan all vertices
  const VertexTable &table=_wTable;
  Vector3 minB(+1e10,+1e10,+1e10);
  Vector3 maxB(-1e10,-1e10,-1e10);
  for( int i=0; i<table.NPos(); i++ )
  {
    const V3 &pos=table.Pos(i);
    CheckMinMax(minB,maxB,pos);
  }
  _bWSphere=(minB+maxB)*0.5;
  float maxDist2=0;
  for( int i=0; i<table.NPos(); i++ )
  {
    const V3 &pos=table.Pos(i);
    float dist2=pos.Distance2(_bWSphere);
    saturateMax(maxDist2,dist2);
  }
  _bWRadius=sqrt(maxDist2);
  */
}

#define LandSegmentSize 8

const float InvLandSegmentSize=1.0/LandSegmentSize;

/*!
\patch 1.26 Date 10/2/2001 by Ondra
- Fixed: Main menu framerate was sometimes very low, especially with HW T&L.
\patch 1.85 Date 9/17/2002 by Ondra
- Fixed: Performance was very slow with large fovLeft values.
*/

static int CalculateCacheSize(Landscape *l)
{
  float range=Glob.config.horizontZ;
  float cacheSize=Square(range*l->GetInvLandGrid()*InvLandSegmentSize)*10;
  int maxN=toIntCeil(cacheSize)+8;
  //LogF("Wanted for %d segments (visibility %f)",_maxN,sqrt(_maxN)*LandSegmentSize*_landGrid*0.5);
  if( maxN<16 ) maxN=16;
  const int maxReasonable=512*512/(LandSegmentSize*LandSegmentSize);
  Assert( maxN<maxReasonable );
  if( maxN>maxReasonable ) maxN=maxReasonable;
  return maxN;
}

LandCache::LandCache()
{
  // maxN depends on visibility
  // for visibility r there is around (r/(50*8))^2*4 neccessary segments
  // we will allocate cache to hold (r/(50*8))^2*6
  RegisterMemoryFreeOnDemand(this);
}

void LandCache::Clear()
{
  _segments.Clear();
}

//! estimated memory used by of LandSegment

const float LandSegmentMemSize = LandSegmentSize*LandSegmentSize*sizeof(TLVertex);

size_t LandCache::FreeOneItem()
{
  // most recently used segments are first
  LandSegment *seg=_segments.Last();
  if (!seg) return 0;
  _segments.Delete(seg);
  return (size_t)LandSegmentMemSize;
  
}

float LandCache::Priority()
{
  // estimated time to create LandSegment (CPU cycles)
  const float itemTime = 100000;
  // estimated time per byte
  return itemTime/LandSegmentMemSize;
}

bool LandCache::VerifyStructure() const
{
  for( LandSegment *segi=_segments.First(),*segn; segi; segi=segn )
  {
    segn=_segments.Next(segi);
    if (!segi->VerifyStructure()) return false;
  }
  return true;
}

Ref<LandSegment> LandCache::Segment( Landscape *land, const LandBegEnd &rect )
{
  // last in list is LRU
  // search for cached data
  int count=0;
  for( LandSegment *segi=_segments.First(),*segn; segi; segi=segn )
  {
    segn=_segments.Next(segi);
    if( segi->_rect==rect )
    {
      Ref<LandSegment> seg=segi;
      _segments.Delete(seg);
      _segments.Insert(seg);
      seg->_lastUsed=Glob.time;
      return seg;
    }
    if( segi->_lastUsed<Glob.time-60 )
    { // segment too old - delete it
      #if LOG_SEG
        LogF("Dropped unused segment %d,%d",segi->_rect.xBeg,segi->_rect.zBeg);
      #endif
      _segments.Delete(segi);
    }
    else count++;
  }
  // with each segment generation recalculate maxN

  // generate cached data for segment rect
  Ref<LandSegment> seg=land->GenerateSegment(rect);
#if 1
  if( count>=_maxN )
  {
    #if LOG_SEG
      LandSegment *segi=_segments.Last();
      LogF("Dropped segment (count) %d,%d",segi->_rect.xBeg,segi->_rect.zBeg);
    #endif
    _segments.Delete(_segments.Last());
  }
  _segments.Insert(seg);
#endif
  //LogF("LandSegment count %d of %d",_segments.Size(),_maxN);
  seg->_lastUsed=Glob.time;
  return seg;
}

void LandCache::Init(Landscape *land)
{
  _maxN=CalculateCacheSize(land);
}

void LandCache::Fill( const Frame &pos )
{
  // recalculate necessary space  - view distance might change
  float range=Glob.config.horizontZ;
  float cacheSize=Square(range*InvLandGrid*InvLandSegmentSize)*8;
  _maxN=toIntCeil(cacheSize);
  //LogF("Wanted for %d segments (visibility %f)",_maxN,range);
  if( _maxN<16 ) _maxN=16;
  const int maxReasonable=512*512/(LandSegmentSize*LandSegmentSize);
  Assert( _maxN<maxReasonable );
  if( _maxN>maxReasonable ) _maxN=maxReasonable;
  //LogF("Space  for %d segments (visibility %f)",_maxN,sqrt(_maxN)*LandSegmentSize*_landGrid*0.5);

  //LogF("LandCache fill: start %d",_segments.Size());
  // pre-build as many cache segments as possible
  int x=toIntFloor(pos.Position().X()*InvLandGrid);
  int z=toIntFloor(pos.Position().Z()*InvLandGrid);
  int maxRange=64;
  x&=~LandSegmentSize;
  z&=~LandSegmentSize;
  for( int range=0; range<maxRange; range++ )
  {
    for( int xx=-range; xx<=range; xx++ )
    for( int zz=-range; zz<=range; zz++ )
    if( xx==-range || xx==+range || zz==-range || zz==+range )
    {
      // break if cache is full
      if( _segments.Size()>=_maxN ) break;
      int xxx=x+xx*LandSegmentSize;
      int zzz=z+zz*LandSegmentSize;
      LandBegEnd rect;
      rect.xBeg=xxx;
      rect.zBeg=zzz;
      rect.xEnd=xxx+LandSegmentSize;
      rect.zEnd=zzz+LandSegmentSize;
      Segment(GLandscape,rect);
      //LogF("Fill land %d,%d",xxx,zzz);
    }
  }
  LogF("LandCache fill: end %d (of %d)",_segments.Size(),_maxN);
}

static int CmpALight
(
  const ActiveLightPointer *l0, const ActiveLightPointer *l1,
  LightContext *context
)
{
  const Light *light0=*l0;
  const Light *light1=*l1;
  Assert( light0 );
  Assert( light1 );
  Assert( light0!=light1 );
  return light1->Compare(*light0,*context);
}

#include "occlusion.hpp"

// TODO: global
extern SRef<Occlusion> Occlusions;
extern bool EnableObjOcc;

/*!
\patch 1.96 Date 1/24/2004 by Ondra
- Fixed: Long blue triangle artifacts (introduced in 1.95).
*/

void Landscape::DrawMesh
(
  Scene &scene,
  TLVertexTable &table, const Shape &vMesh, Vector3Par offset,
  const LandBegEnd &rect, bool isWater
)
{
  // TODO: quick per-segment full clip/no clip
  // exact per-segment clip rejection
  Vector3Val bCenter=vMesh.BSphereCenter()+offset;
  float bRadius=vMesh.BSphereRadius();

  //Point3 bCenterView(VFastTransform,GScene->ScaledInvTransform(),bCenter);
  if( scene.GetCamera()->IsClipped(bCenter,bRadius,1) )
  {
    return;
  }
  ClipFlags clip=scene.GetCamera()->MayBeClipped(bCenter,bRadius,1);
  ClipFlags andClip=ClipNone;
  ClipFlags orClip=table.CheckClipping(*scene.GetCamera(),clip,andClip);
  if( andClip&ClipAll ) return;

  // select lights on per-segment basis
  LightList work(true);
  const LightList &lights = scene.SelectLights(bCenter,bRadius,work);
  
  table.DoLightingColorized(lights,_colorizePalette,vMesh,0);

  // there will be many vertices clipped
  // we want vertex mesh and table to grow fast to avoid many allocations

  if( isWater )
  {
    // animate all vertices
    Texture *tex = _texture[0];
    if (tex)
    {
      for( int i=0; i<table.NVertex(); i++ )
      {
        Vector3Val pos=vMesh.Pos(i);
        float u=tex->UToLogical(table.U(i));
        float v=tex->VToLogical(table.V(i));
        float x=pos.X()*_invLandGrid;
        float z=pos.Z()*_invLandGrid;
        float mw=MoveWater(x,z,0.3);
        table.SetUV
        (
          i,
          tex->UToPhysical(u+mw*0.5),
          tex->VToPhysical(v+mw)
        );
      }
    }
  }
  
  // prepare surface for drawing
  const FaceArray &sFaces = vMesh.Faces();
  const FaceArray *drawFaces=&sFaces;
  FaceArray clippedFaces;
  // copy all faces and perform per-face clipping

  if (orClip || isWater )
  {
    // this segment is clipped or water
    // avoid copy when not reflected

    Texture *waterTex = _texture[0];
    if (isWater && waterTex)
    {
      float texPhase=fastFmod(Glob.time.toFloat(),2);
      if( texPhase>1.0f ) texPhase=2-texPhase;

      int n = waterTex->AnimationLength();
      if (n<=1) return; // no animation
      int i=toIntFloor(texPhase*n);
      saturate(i,0,n-1);
      waterTex = waterTex->GetAnimation(i);
    }
    // full clipping required
    drawFaces=&clippedFaces;
    clippedFaces.ReserveFaces(sFaces.Size()*2,false);
    for( Offset f=sFaces.Begin(),e=sFaces.End(); f<e; sFaces.Next(f) )
    {
      const Poly &src=sFaces[f];
      // some faces are fully clipped - have invalid vertices
      // trick to avoid branches
      // if any of three is negative, or is negative as well
      ClipFlags clipOr=ClipNone;
      if( orClip )
      {
        ClipFlags clipAnd=ClipAll;
        for (int i=0; i<src.N(); i++)
        {
          int sv = src.GetVertex(i);
          ClipFlags clip = table.Clip(sv);
          clipOr |= clip, clipAnd &= clip;
        }
        if( clipAnd ) continue;
      }

      // TODO: perform quick viewspace cull
      Poly *dst = NULL;
      if( !clipOr )
      {
        dst = clippedFaces.AddNoClip(src,table,scene);
      }
      else
      {
        dst = clippedFaces.AddClipped(src,table,scene,clipOr);
      }
      if (isWater && dst)
      {
        // animate water texture
        dst->SetTexture(waterTex);
      }
    }
  }

  if( drawFaces->Size()<=0 ) return;

  ADD_COUNTER(poly,drawFaces->Size());

  GEngine->PrepareMesh(0);
  _engine->SetBias(0);

  table.DoPerspective(*scene.GetCamera(),orClip);


  if( EnableObjOcc )
  {
    // draw landscape into occlusion buffer
    float invW=+2.0/GEngine->Width();
    float invH=-2.0/GEngine->Height();
    //float zScale=1.0/GScene->GetCamera()->ClipNear();
    OcclusionPoly poly;
    for( Offset f=drawFaces->Begin(); f<drawFaces->End(); drawFaces->Next(f) )
    {
      const Poly &face = drawFaces->Get(f);
      // construct occlusion poly from landscape poly
      float minZ = 1e10;
      poly.Clear();
      for( int i=face.N(); --i>=0; )
      {
        int index = face.GetVertex(i);
        //Vector3PVal pos=table.ScreenPos(index);
        // screen coordinates (0..w,0..h)
        // must be converted to -1..+1 coordinates
        const TLVertex &tl = table.GetVertex(index);
        Vector3Val sPos = table.TransPos(index);
        Vector3 uPos
        (
          tl.pos[0]*invW-1,tl.pos[1]*invH+1,sPos.Z()
        );
        //Vector3 uPos(pos[0]*invW-1,pos[1]*invH+1,pos[2]);
        poly.Add(uPos);
        saturateMin(minZ,sPos.Z());
      }
      if (minZ<250)
      {
        // draw only near landscape polygons
        Occlusions->RenderProjectedPoly(poly);
      }
    }
  }

  // note: BeginMesh may change pos field (may be used for fog etc)
  _engine->BeginMesh(table,0);
  float z=scene.GetCamera()->Position().Distance(bCenter)-bRadius;
  float z2 = Square(floatMax(z,0));
  Texture *lastTexture=NULL;
  int lastSpec=-1;
  // scan all textures used in this segment and prepare them
  vMesh.PrepareTextures(z2,0);
  for( Offset f=drawFaces->Begin(); f<drawFaces->End(); drawFaces->Next(f) )
  {
    const Poly &face=drawFaces->Get(f);
    Texture *texture=face.GetTexture();
    int spec=face.Special();
    if( texture!=lastTexture || spec!=lastSpec )
    {
      lastSpec=spec;
      lastTexture=texture;

      face.Prepare(texture,spec);
    }

    GEngine->DrawPolygon(face.GetVertexList(),face.N());
  }
  _engine->EndMesh(table);
  int idleMs=_engine->HowLongIdle();
  if( idleMs>=0 && GWorld ) GWorld->PrimaryAllowSwitch(idleMs);

}

static int SortByTextureR( const Poly &p0, const Poly &p1 )
{
  return (char *)p0.GetTexture()-(char *)p1.GetTexture();
}

const int MaxVertexCanditates = 6; // vertex may be shared between six faces
class VertexCandidates: public VerySmallArray<int,sizeof(int)*8>
{
  public:
  void AddUnique(int src)
  {
  #if _DEBUG
    for( int i=0; i<Size(); i++ )
    {
      if( (*this)[i]==src )
      {
        Fail("Index not unique");
        return;
      }
    }
  #endif
    Add(src);
  }
  int FindOrAdd(int src);
  int Find(int src) const;
};

int VertexCandidates::FindOrAdd(int src)
{
  for( int i=0; i<Size(); i++ ) if( (*this)[i]==src ) return i;
  return Add(src);
}

int VertexCandidates::Find(int src) const
{
  for( int i=0; i<Size(); i++ ) if( (*this)[i]==src ) return i;
  return -1;
}

extern bool EnableHWTL;
extern bool EnableHWTLState;


#define LOG_SHARING 0

//! bilinear interpolation, yxz, xf and zf = <0,1>

__forceinline float Bilint
(
  float y00, float y01, float y10, float y11,
  float xf, float zf
)
{
  float y0z = y00*(1-zf) + y01*zf;
  float y1z = y10*(1-zf) + y11*zf;
  return y0z*(1-xf) + y1z*xf;
}

static Vector3 WaterNormal(0,-1,0);

//const int WaterFlags = IsWater|SpecularTexture;

const int WaterFlags = SpecularTexture;

#define SEAMS 1

/*!
\patch 1.43 Date 1/30/2002 by Ondra
- Fixed: Great Monolith gradually growing out of the water after mission reload near A1 grid.
(Thanks to WKK Gimbal for discovering when this bug happens.)
\patch 1.45 Date 2/25/2002 by Ondra
- Removed: SW T&L terrain color randomization removed.
This makes terrain memory usage lower.
Note: This feature was never present in HW T&L
\patch_internal 1.45 Date 2/25/2002 by Ondra
- Optimized: Terrain segment creation optimized,
usefull for high resolution terrain.
\patch 1.50 Date 4/2/2002 by Ondra
- Fixed: Water surface normals were reversed.
As a result, water was drawn too dark.
\patch 1.93 Date 9/22/2003 by Ondra
- Fixed: Lighting direction was wrong on terrain (bug was present since version 1.00 and even before)
\patch 1.95 Date 10/24/2003 by Ondra
- Fixed: Rendering artifact: White dots or lines on a terrain when using HW T&L.
*/

Ref<LandSegment> Landscape::GenerateSegment( const LandBegEnd &rect )
{
  #if LOG_SEG
    LogF("GenerateSegment %d,%d",rect.xBeg,rect.zBeg);
  #endif
  LandSegment *seg=new LandSegment;
  // we have to reconstruct cache
  
  // create all vertices
  //const int xCount=rect.xEnd-rect.xBeg+1;
  //int zCount=rect.zEnd-rect.zBeg+1;

  // calculate counts for subdivision of very big landscape squares
  const int subdivisionLevel = _terrainRangeLog-_landRangeLog;
  #define maxSubdivisionLevel 3
  // terrain data are already subdivided

  const int subdivisionCount = 1<<subdivisionLevel;
  //int subdivisionCount2 = subdivisionCount*subdivisionCount;
  const float invSubdivisionCount = 1.0/subdivisionCount;

  LandBegEnd rectSD;
  rectSD.xBeg = rect.xBeg*subdivisionCount;
  rectSD.xEnd = rect.xEnd*subdivisionCount;
  rectSD.zBeg = rect.zBeg*subdivisionCount;
  rectSD.zEnd = rect.zEnd*subdivisionCount;
  
  const int xCountSD=(rect.xEnd-rect.xBeg)*subdivisionCount+1;
  const int zCountSD=(rect.zEnd-rect.zBeg)*subdivisionCount+1;

  Assert( xCountSD==rectSD.xEnd-rectSD.xBeg+1 );
  Assert( zCountSD==rectSD.zEnd-rectSD.zBeg+1 );

  const int nVertices=zCountSD*xCountSD;

  #define maxSubdivisionCount (1<<maxSubdivisionLevel)
  //const int maxSubdivisionCount2 = maxSubdivisionCount*maxSubdivisionCount;
  #define maxNVertices \
  ( \
    (LandSegmentSize*maxSubdivisionCount+1)* \
    (LandSegmentSize*maxSubdivisionCount+1) \
  )

  Assert (nVertices<=maxNVertices);

  seg->_someWater=false;
  seg->_onlyWater=true;
  seg->_seaLevel = 0; //=_seaLevelWave;

  // generate basic pos and normal map
  //AUTO_STATIC_ARRAY(ClipFlags,clip,maxNVertices);
  AUTO_STATIC_ARRAY(Vector3,pos,maxNVertices);
  AUTO_STATIC_ARRAY(Vector3,norm,maxNVertices);
  //AUTO_STATIC_ARRAY(ClipFlags,wClip,maxNVertices);
  AUTO_STATIC_ARRAY(Vector3,wPos,maxNVertices);
  //clip.Resize(nVertices);
  pos.Resize(nVertices);
  norm.Resize(nVertices);

  //wClip.Resize(nVertices);
  wPos.Resize(nVertices);

#if 0 //_ENABLE_CHEATS
  {
    float xMin = rectSD.xBeg*_terrainGrid, xMax = rectSD.xEnd*_terrainGrid;
    float zMin = rectSD.zBeg*_terrainGrid, zMax = rectSD.zEnd*_terrainGrid;
    Vector3Val cpos = GScene->GetCamera()->Position();
    if
    (
      cpos.X()>=xMin && cpos.X()<=xMax &&
      cpos.Z()>=zMin && cpos.Z()<=zMax
    )
    {
      __asm nop;
    }
  }
#endif

  if( this_TerrainInRange(rectSD.zBeg,rectSD.xBeg) )
  {
    // first calculate basic vertices (subdivision)
    float lMin = FLT_MAX;
    float lMax = FLT_MIN;

    for(int z=rectSD.zBeg; z<=rectSD.zEnd; z++)
    for(int x=rectSD.xBeg; x<=rectSD.xEnd; x++)
    {
      //const int xz=(z-rectSD.zBeg)*xCountSD+(x-rectSD.xBeg);
      float l = YOutsideMap;
      if( this_TerrainInRange(x,z) )
      {
        l=GetData(x,z);
      }
      // detect possible water (consider max high tide)
      saturateMax(lMax,l);
      saturateMin(lMin,l);
    }
    
    if( lMin<=maxTide+maxWave ) seg->_someWater=true;
    if( lMax>=-(maxTide+maxWave) ) seg->_onlyWater=false;
  }
  else
  {
    seg->_someWater=seg->_onlyWater=true;
  }

  bool centered = GEngine->GetTL();
  seg->_offset = VZero;
  int offsetX = 0, offsetZ = 0;
  if (centered)
  {
    seg->_offset = Vector3
    (
      _landGrid*0.5f*(rect.xBeg+rect.xEnd),
      0,
      _landGrid*0.5f*(rect.zBeg+rect.zEnd)
    );
    // make sure offset can be represented by integer
    DoAssert(((rectSD.xBeg+rectSD.xEnd)&1)==0);
    DoAssert(((rectSD.zBeg+rectSD.zEnd)&1)==0);
    
    offsetX = (rectSD.xBeg+rectSD.xEnd)>>1;
    offsetZ = (rectSD.zBeg+rectSD.zEnd)>>1;
  }

  if( seg->_someWater )
  {
    seg->_wTable.Reserve(nVertices*4);

    // subdivided water surface
    const int xn = (rectSD.xEnd-rectSD.xBeg)+1;
    const int zn = (rectSD.zEnd-rectSD.zBeg)+1;
    //float baseX = _terrainGrid*(rectSD.xBeg - offsetX);
    //float baseZ = _terrainGrid*(rectSD.zBeg - offsetZ);
    for (int zz=0; zz<zn; zz++) for (int xx=0; xx<xn; xx++)
    {
      const int xz=zz*xCountSD+xx;
      wPos[xz] = Vector3
      (
        _terrainGrid*(xx+rectSD.xBeg - offsetX),
        0,
        _terrainGrid*(zz+rectSD.zBeg - offsetZ)
      );
      //const float stretch = -0.100f; // seam debugging - create artificial hole
      const float stretch = +0.001f;
      if (zz==0) wPos[xz][2] -= stretch;
      if (zz==zn-1) wPos[xz][2] += stretch;
      if (xx==0) wPos[xz][0] -= stretch;
      if (xx==zn-1) wPos[xz][0] += stretch;
    }
  }
  
  // note: every vertex in mesh is initialized

  AUTO_STATIC_ARRAY(VertexCandidates,wCandidates,maxNVertices);
  AUTO_STATIC_ARRAY(VertexCandidates,candidates,maxNVertices);
  
  if( !seg->_onlyWater )
  {

    for (int z=rectSD.zBeg; z<=rectSD.zEnd; z++)
    for (int x=rectSD.xBeg; x<=rectSD.xEnd; x++)
    {
      // calculate normals for all basic vertices
      const int xz=(z-rectSD.zBeg)*xCountSD+(x-rectSD.xBeg);
      
      const Coord xDelta=ClippedData(z,x+1)-ClippedData(z,x-1);
      const Coord zDelta=ClippedData(z+1,x)-ClippedData(z-1,x);
      //Point3 offX(0,xDelta,_terrainGrid);
      //Point3 offZ(_terrainGrid,zDelta,0);
      Point3 offX(_terrainGrid, xDelta, 0);
      Point3 offZ(0, zDelta, _terrainGrid);
      Vector3Val cp = offX.CrossProduct(offZ);
      Vector3Val normal = cp.Normalized();

      Assert( normal.IsFinite() );
      Assert( xz<nVertices );
      norm[xz]=normal;

      // set position data
      float l=ClippedData(z,x);
      pos[xz]=Vector3(_terrainGrid*(x-offsetX),l,_terrainGrid*(z-offsetZ));

      // apply some stretch on border vertices to prevent rounding error tearing
      //const float stretch = -0.100f; // seam debugging - create artificial hole
      const float stretch = +0.0001f;
      if (z==rectSD.zBeg) pos[xz][2] -= stretch;
      if (z==rectSD.zEnd) pos[xz][2] += stretch;
      if (x==rectSD.xBeg) pos[xz][0] -= stretch;
      if (x==rectSD.xEnd) pos[xz][0] += stretch;
    }

    candidates.Resize(nVertices);

    if (seg->_someWater)
    {
      wCandidates.Resize(nVertices);
    }

    // we need to implement clamping
    // clamped faces cannot share (u,v) coordinates
    seg->_table.Reserve(nVertices*4);

    seg->_table.ClearFaces();
    seg->_table.ReserveFaces((rectSD.xEnd-rectSD.xBeg)*(rectSD.zEnd-rectSD.zBeg)*2);
    for( int zs=rect.zBeg; zs<rect.zEnd; zs++ )
    for( int xs=rect.xBeg; xs<rect.xEnd; xs++ )
    {
      int textureIndex=ClippedTextureIndex(zs,xs);
      if( textureIndex==0 ) continue; // no water polygons in this stage

      Texture *texture=_texture[textureIndex];
      int clampFlags=ClampFlags(textureIndex);
      clampFlags|=DetailTexture;

      if (texture)
      {
        seg->_table.RegisterTexture(texture,_landGrid*_landGrid*0.5f);
      }

      //      xz     xz
      float tu00 = 0, tv00 = 0;
      float tu01 = 0, tv01 = 0;
      float tu10 = 0, tv10 = 0;
      float tu11 = 0, tv11 = 0;

      // prepare to offset interpolation
      // get u,v offset in all four vertices
      if( this_InRange(xs,zs) && this_InRange(xs+1,zs+1) )
      {
        const float scale = 1.0/10;
        tu10 = _random(xs+1,zs  ).uOff*scale, tv10 = _random(xs+1,zs  ).vOff*scale;
        tu00 = _random(xs  ,zs  ).uOff*scale, tv00 = _random(xs  ,zs  ).vOff*scale;
        tu01 = _random(xs  ,zs+1).uOff*scale, tv01 = _random(xs  ,zs+1).vOff*scale;
        tu11 = _random(xs+1,zs+1).uOff*scale, tv11 = _random(xs+1,zs+1).vOff*scale;
      }

      float u0,v0;
      if( !(clampFlags&ClampU) ) u0 = xs-rect.xBeg;
      else u0 = 0;
      if( !(clampFlags&ClampV) ) v0 = zs-rect.zBeg;
      else v0 = 0;

      Poly poly;
      poly.Init();
      poly.SetTexture(texture);
      poly.SetSpecial(clampFlags);

      for( int zzs=0; zzs<subdivisionCount; zzs++ )
      for( int xxs=0; xxs<subdivisionCount; xxs++ )
      {
        const int xx = (xs-rect.xBeg)*subdivisionCount+xxs;
        const int zz = (zs-rect.zBeg)*subdivisionCount+zzs;

        // create square face (x,z) (x+1,z), (x+1,z+1), (x,z+1)
        const int xz = zz*xCountSD+xx;
        
        //          xz
        const int vo10 = xz+1+0;
        const int vo00 = xz+0+0;
        const int vo01 = xz+0+xCountSD;
        const int vo11 = xz+1+xCountSD;

        const float xf = xxs*invSubdivisionCount;
        const float zf = zzs*invSubdivisionCount;

        const float u = u0+xf, v=v0+zf;

        // now we have polygon to add
        Assert( vo10<nVertices );
        Assert( vo00<nVertices );
        Assert( vo01<nVertices );
        Assert( vo11<nVertices );
        float uu0 = 0, uu1 = 0, uu2 = 0, uu3 = 0;
        float vv0 = 0, vv1 = 0, vv2 = 0, vv3 = 0;
        if (texture)
        {
          // interpolate u,v offset in all four vertices
          // uuk is interpolated from tuii
          // vvk is interpolated from tvii
          const float xf1 = xf+invSubdivisionCount;
          const float zf1 = zf+invSubdivisionCount;

          const float xf00 = xf,  zf00 = zf;
          const float xf01 = xf,  zf01 = zf1;
          const float xf10 = xf1, zf10 = zf;
          const float xf11 = xf1, zf11 = zf1;

          #define UV(xy) \
            const float tuu##xy = Bilint(tu00,tu01,tu10,tu11,xf##xy,zf##xy); \
            const float tvv##xy = Bilint(tv00,tv01,tv10,tv11,xf##xy,zf##xy)
          UV(00);
          UV(01);
          UV(10);
          UV(11);

          // 00-> 1
          // 01-> 2
          // 10-> 0
          // 11-> 3

          // 10-> 0
          // 00-> 1
          // 01-> 2
          // 11-> 3

          uu0 = texture->UToPhysical(u+invSubdivisionCount+tuu10); // v10
          vv0 = texture->VToPhysical(v                    +tvv10);
          uu1 = texture->UToPhysical(u                    +tuu00); // v00
          vv1 = texture->VToPhysical(v                    +tvv00);
          uu2 = texture->UToPhysical(u                    +tuu01); // v01
          vv2 = texture->VToPhysical(v+invSubdivisionCount+tvv01);
          uu3 = texture->UToPhysical(u+invSubdivisionCount+tuu11); // v11
          vv3 = texture->VToPhysical(v+invSubdivisionCount+tvv11);
        }
        #undef UV

        // store index as candidate for vertices created from same data
        bool reused;
        int v10=seg->_table.AddVertex(
          pos[vo10],norm[vo10],ClipAll,uu0,vv0,
          candidates[vo10].Data(),candidates[vo10].Size(),reused
        );
        if (!reused) candidates[vo10].AddUnique(v10);
        int v00=seg->_table.AddVertex(
          pos[vo00],norm[vo00],ClipAll,uu1,vv1,
          candidates[vo00].Data(),candidates[vo00].Size(),reused
        );
        if (!reused) candidates[vo00].AddUnique(v00);
        int v01=seg->_table.AddVertex(
          pos[vo01],norm[vo01],ClipAll,uu2,vv2,
          candidates[vo01].Data(),candidates[vo01].Size(),reused
        );
        if (!reused) candidates[vo01].AddUnique(v01);
        int v11=seg->_table.AddVertex(
          pos[vo11],norm[vo11],ClipAll,uu3,vv3,
          candidates[vo11].Data(),candidates[vo11].Size(),reused
        );
        if (!reused) candidates[vo11].AddUnique(v11);
        // create a face
        poly.Set(0,v10); // shared vertices
        poly.Set(1,v00);
        poly.Set(2,v01);
        poly.SetN(3);

        seg->_table.AddFace(poly);

        // reuse as much as possible
        poly.Set(1,v01);
        poly.Set(2,v11);

        seg->_table.AddFace(poly);

        #if SEAMS
        const float ySeam = -0.50f;
        if (xxs==0 && xs==rect.xBeg)
        {
          // border: create a seam 
          // use vertices v00, v01

          int v00Seam=seg->_table.AddVertex(
            pos[vo00]+Vector3(0,ySeam,0),norm[vo00],ClipAll,uu1,vv1
          );
          int v01Seam=seg->_table.AddVertex(
            pos[vo01]+Vector3(0,ySeam,0),norm[vo01],ClipAll,uu2,vv2
          );
          
          poly.SetN(4);
          poly.Set(0,v01);
          poly.Set(1,v00);
          poly.Set(2,v00Seam);
          poly.Set(3,v01Seam);
          seg->_table.AddFace(poly);
        }
        if (xxs==subdivisionCount-1 && xs==rect.xEnd-1)
        {
          // border: create a seam 
          // use vertices v10, v11

          int v10Seam=seg->_table.AddVertex(
            pos[vo10]+Vector3(0,ySeam,0),norm[vo10],ClipAll,uu0,vv0
          );
          int v11Seam=seg->_table.AddVertex(
            pos[vo11]+Vector3(0,ySeam,0),norm[vo11],ClipAll,uu3,vv3
          );

          poly.SetN(4);
          poly.Set(0,v10);
          poly.Set(1,v11);
          poly.Set(2,v11Seam);
          poly.Set(3,v10Seam);
          seg->_table.AddFace(poly);
        }

        if (zzs==0 && zs==rect.zBeg)
        {
          // border: create a seam 
          // use vertices v00, v10

          int v00Seam=seg->_table.AddVertex(
            pos[vo00]+Vector3(0,ySeam,0),norm[vo00],ClipAll,uu1,vv1
          );
          int v10Seam=seg->_table.AddVertex(
            pos[vo10]+Vector3(0,ySeam,0),norm[vo10],ClipAll,uu0,vv0
          );
          poly.SetN(4);
          poly.Set(0,v00);
          poly.Set(1,v10);
          poly.Set(2,v10Seam);
          poly.Set(3,v00Seam);
          seg->_table.AddFace(poly);
        }
        if (zzs==subdivisionCount-1 && zs==rect.zEnd-1)
        {
          // border: create a seam 
          // use vertices v11,v01

          int v01Seam=seg->_table.AddVertex(
            pos[vo01]+Vector3(0,ySeam,0),norm[vo01],ClipAll,uu2,vv2
          );
          int v11Seam=seg->_table.AddVertex(
            pos[vo11]+Vector3(0,ySeam,0),norm[vo11],ClipAll,uu3,vv3
          );
          poly.SetN(4);

          poly.Set(0,v11);
          poly.Set(1,v01);
          poly.Set(2,v01Seam);
          poly.Set(3,v11Seam);
          seg->_table.AddFace(poly);
        }
        #endif
      }

    }
    // for( int zs=rect.zBeg; zs<rect.zEnd; zs++ )
    // for( int xs=rect.xBeg; xs<rect.xEnd; xs++ )

    //Log("Alloc %d<%d",seg->_mesh.NVertex(),nVertices*3);
    #if LOG_SHARING
      int maxV = seg->_table.NFaces()*3;
      int actV = seg->_table.NVertex();
      LogF("Ground sharing ratio is %f",actV*1.0f/maxV);
    #endif

    seg->_table.Compact();
    seg->_table.CalculateMinMax();
    
    if( seg->_someWater )
    {
      // calculate how much faces will be in water
      int wFaces = 0;
      int zn = (rect.zEnd-rect.zBeg)*subdivisionCount;
      int xn = (rect.xEnd-rect.xBeg)*subdivisionCount;

      int wVertices = zn+1+xn+1;
      for (int zz=0; zz<zn; zz++) for (int xx=0; xx<xn; xx++)
      {
        // create square face (x,z) (x+1,z), (x+1,z+1), (x,z+1)
        int xz = xx + zz*xCountSD;
        float miny=pos[xz+0+1].Y();
        saturateMin(miny,pos[xz+0+0].Y());
        saturateMin(miny,pos[xz+0+xCountSD].Y());
        saturateMin(miny,pos[xz+1+xCountSD].Y());
        miny += seg->_offset.Y();
        if( miny<=maxTide+maxWave ) wFaces+=2,wVertices+=2;
      }

      seg->_wTable.Clear();
      seg->_wTable.ReserveFaces(wFaces);

      seg->_wTable.Reserve(wVertices*4);
      
      // add water faces
      Poly water;
      water.Init();
      water.SetTexture(_texture[0]);
      water.OrSpecial(WaterFlags|NoClamp);
      water.SetN(3);
      for (int zz=0; zz<zn; zz++) for (int xx=0; xx<xn; xx++)
      {
        // create square face (x,z) (x+1,z), (x+1,z+1), (x,z+1)
        int xz = xx+zz*xCountSD;
        float miny=pos[xz+0+1].Y();
        saturateMin(miny,pos[xz+0+0].Y());
        saturateMin(miny,pos[xz+0+xCountSD].Y());
        saturateMin(miny,pos[xz+1+xCountSD].Y());
        if( miny<=maxTide+maxWave )
        {
          // water polygon
          const float zR=zz*invSubdivisionCount;
          const float xR=xx*invSubdivisionCount;

          const int vo10 = xz+0+0;
          const int vo00 = xz+0+1;
          const int vo01 = xz+xCountSD+0;
          const int vo11 = xz+xCountSD+1;

          Texture *t0 = _texture[0];
          float zu0=0, zu1=0, xv0=0, xv1=0;
          if (t0)
          {
            zu0 = t0->UToPhysical(zR);
            zu1 = t0->UToPhysical(zR+invSubdivisionCount);
            xv0 = t0->VToPhysical(xR);
            xv1 = t0->VToPhysical(xR+invSubdivisionCount);
          }
          bool reused;
          int v10=seg->_wTable.AddVertex
          (
            wPos[vo10],WaterNormal,ClipAll,zu0,xv0,
            wCandidates[vo10].Data(),wCandidates[vo10].Size(),reused
          );
          if (!reused) wCandidates[vo10].AddUnique(v10);
          int v00=seg->_wTable.AddVertex
          (
            wPos[vo00],WaterNormal,ClipAll,zu0,xv1,
            wCandidates[vo00].Data(),wCandidates[vo00].Size(),reused
          );
          if (!reused) wCandidates[vo00].AddUnique(v00);
          int v01=seg->_wTable.AddVertex
          (
            wPos[vo01],WaterNormal,ClipAll,zu1,xv0,
            wCandidates[vo01].Data(),wCandidates[vo01].Size(),reused
          );
          if (!reused) wCandidates[vo01].AddUnique(v01);
          int v11=seg->_wTable.AddVertex
          (
            wPos[vo11],WaterNormal,ClipAll,zu1,xv1,
            wCandidates[vo11].Data(),wCandidates[vo11].Size(),reused
          );
          if (!reused) wCandidates[vo11].AddUnique(v11);
          
          water.Set(0,v00),water.Set(1,v10),water.Set(2,v01);
          seg->_wTable.AddFace(water);

          water.Set(0,v00),water.Set(1,v01),water.Set(2,v11);
          seg->_wTable.AddFace(water);
        }
      }
      // vertices should be shared .. check if it is true
      // verify vertex and face estimation
      Assert( seg->_wTable.NFaces()<=wFaces );
      if (seg->_wTable.NFaces()>0)
      {
        seg->_wTable.RegisterTexture(_texture[0],_landGrid*_landGrid*0.5f);
      }
      seg->_wTable.Compact();
      seg->_wTable.CalculateMinMax();
      //LogF("CLand  %x",seg);
      seg->_wTable.FindSections(true);
      seg->_wTable.ConvertToVBuffer(VBBigDiscardable);

      #if LOG_SHARING
        int maxV = seg->_wTable.NFaces()*3;
        int actV = seg->_wTable.NVertex();
        LogF("Water sharing ratio is %f",actV*1.0f/maxV);
      #endif
    }
    // sort faces by texture - it will help to avoid state changes
    seg->_table.Optimize();
    // TODO: sort vertices by face
    //LogF("CWater %x",seg);
    seg->_table.FindSections(true);
    seg->_table.ConvertToVBuffer(VBBigDiscardable);
  }
  else
  {
    // only water - no landscape
    seg->_table.Clear();

    Assert( seg->_someWater );
    // calculate how much faces will be in water

    int zn = (rect.zEnd-rect.zBeg)*subdivisionCount;
    int xn = (rect.xEnd-rect.xBeg)*subdivisionCount;

    int wFaces=zn*xn*2;

    seg->_wTable.Clear();
    seg->_wTable.ReserveFaces(wFaces);

    // add water faces
    Poly water;
    water.Init();
    water.SetTexture(_texture[0]);
    water.OrSpecial(WaterFlags|NoClamp);
    water.SetN(3);

    wCandidates.Resize(nVertices);

    for( int zz=0; zz<zn; zz++ ) for( int xx=0; xx<xn; xx++ )
    {
      // create square face (x,z) (x+1,z), (x+1,z+1), (x,z+1)
      const int xz = xx + zz*xCountSD;
      // water polygon
      const float zR=zz*invSubdivisionCount;
      const float xR=xx*invSubdivisionCount;

      const int vo10=xz+0+0;
      const int vo00=xz+0+1;
      const int vo01=xz+xCountSD+0;
      const int vo11=xz+xCountSD+1;

      Texture *t0 = _texture[0];
      float zu0=0, zu1=0, xv0=0, xv1=0;
      if (t0)
      {
        zu0 = t0->UToPhysical(zR);
        zu1 = t0->UToPhysical(zR+invSubdivisionCount);
        xv0 = t0->VToPhysical(xR);
        xv1 = t0->VToPhysical(xR+invSubdivisionCount);
      }

      bool reused;
      int v10=seg->_wTable.AddVertex
      (
        wPos[vo10],WaterNormal,ClipAll,zu0,xv0,
        wCandidates[vo10].Data(),wCandidates[vo10].Size(),reused
      );
      if (!reused) wCandidates[vo10].AddUnique(v10);
      int v00=seg->_wTable.AddVertex
      (
        wPos[vo00],WaterNormal,ClipAll,zu0,xv1,
        wCandidates[vo00].Data(),wCandidates[vo00].Size(),reused
      );
      if (!reused) wCandidates[vo00].AddUnique(v00);
      int v01=seg->_wTable.AddVertex
      (
        wPos[vo01],WaterNormal,ClipAll,zu1,xv0,
        wCandidates[vo01].Data(),wCandidates[vo01].Size(),reused
      );
      if (!reused) wCandidates[vo01].AddUnique(v01);
      int v11=seg->_wTable.AddVertex
      (
        wPos[vo11],WaterNormal,ClipAll,zu1,xv1,
        wCandidates[vo11].Data(),wCandidates[vo11].Size(),reused
      );
      if (!reused) wCandidates[vo11].AddUnique(v11);
      
      water.Set(0,v00),water.Set(1,v10),water.Set(2,v01);
      seg->_wTable.AddFace(water);

      water.Set(0,v00),water.Set(1,v01),water.Set(2,v11);
      seg->_wTable.AddFace(water);
    }
    // vertices should be shared .. check if it is true
    //Assert( seg->_wMesh.NVertex()<=wVertices );
    //Assert( seg->_wFaces.Size()<=wFaces );
    //seg->_wFaces.Compact();
    seg->_wTable.Compact();
    Assert( seg->_wTable.NFaces()<=wFaces );
    seg->_wTable.CalculateMinMax();
    seg->_wTable.RegisterTexture(_texture[0],_landGrid*_landGrid*0.5f);
    //LogF("CWater %x",seg);
    seg->_wTable.FindSections(true);
    seg->_wTable.ConvertToVBuffer(VBBigDiscardable);

    #if LOG_SHARING
      int maxV = seg->_wTable.NFaces()*3;
      int actV = seg->_wTable.NVertex();
      LogF("Water (deep) sharing ratio is %f",actV*1.0f/maxV);
    #endif
  }

  if (seg->_someWater)
  {
    // set water material
    // TODO: preload water material
    Ref<TexMaterial> mat = GTexMaterialBank.New("#Water");
    for (int i=0; i<seg->_wTable.NSections(); i++)
    {
      seg->_wTable.GetSection(i).surfMat = mat;
    }
  }

  {
    // set terrain material
    // TODO: preload terrain material
    Ref<TexMaterial> mat = GTexMaterialBank.New("#Terrain");
    for (int i=0; i<seg->_table.NSections(); i++)
    {
      seg->_table.GetSection(i).surfMat = mat;
    }
  }

  // verify seg _table vertices
  seg->_valid=true;
  seg->_rect=rect;
  return seg;
}

#define LANDDRAW 1

class AnimatorDefault: public IAnimator
{
  Color _color;

  public:
  AnimatorDefault(Color color=Color(1,1,1));

  // both Transform and Light should include
  // any animation required on position and normals
  virtual void DoTransform
  (
    TLVertexTable &dst,
    const Shape &src, const Matrix4 &posView,
    int from, int to
  ) const;
  // when Light is called TLVertexTable already contains
  virtual void DoLight
  (
    TLVertexTable &dst,
    const Shape &src, const Matrix4 &worldToModel, const LightList &lights,
    int spec, int material, int from, int to
  ) const;
  // get material with given index
  virtual void GetMaterial(TLMaterial &mat, int index) const;
  virtual bool GetAnimated(const Shape &src) const {return false;}
};

AnimatorDefault::AnimatorDefault(Color color)
:_color(color)
{
}

void AnimatorDefault::DoTransform
(
  TLVertexTable &dst,
  const Shape &src, const Matrix4 &posView,
  int from, int to
) const
{
  dst.DoTransformPoints(src,posView,from,to);
}
// when Light is called TLVertexTable already contains
void AnimatorDefault::DoLight
(
  TLVertexTable &dst,
  const Shape &src, const Matrix4 &worldToModel, const LightList &lights,
  int spec, int material, int from, int to
) const 
{
  TLMaterial mat;
  GetMaterial(mat,material);
  mat.specFlags = spec;
  dst.DoMaterialLightingP
  (
    mat,worldToModel,lights,src,from,to
  );
}

#define TL_LANDSCAPE 1

// get material with given index
void AnimatorDefault::GetMaterial(TLMaterial &mat, int index) const
{
  ColorVal accom=GEngine->GetAccomodateEye();
  // distrubute by predefined materials
  CreateMaterial(mat,accom*_color,index);
}

class AnimatorAlpha: public AnimatorDefault
{
  float _alpha;

  public:
  void SetAlpha(float a){_alpha = a;}
  void GetMaterial(TLMaterial &mat, int index) const;
};

void AnimatorAlpha::GetMaterial(TLMaterial &mat, int index) const
{
  ColorVal accom=GEngine->GetAccomodateEye();
  // distrubute by predefined materials
  CreateMaterial(mat,accom,index);
  mat.ambient.SetA(_alpha);
  mat.diffuse.SetA(_alpha);
}

class AnimatorBrightness: public AnimatorDefault
{
  float _bright;

  public:
  void SetBrigth(float b){_bright = b;}
  void GetMaterial(TLMaterial &mat, int index) const;
};

void AnimatorBrightness::GetMaterial(TLMaterial &mat, int index) const
{
  ColorVal accom=GEngine->GetAccomodateEye() * _bright;
  // distrubute by predefined materials
  CreateMaterial(mat,accom,index);
}

static AnimatorDefault GAnimatorDefault;
static AnimatorDefault GAnimatorWater(Color(0.8,0.8,0.8));

void Landscape::DrawWater(const LandBegEnd &bigRect, Scene &scene)
{
  for (int x=bigRect.xBeg; x<bigRect.xEnd; x+=LandSegmentSize)
  for (int z=bigRect.zBeg; z<bigRect.zEnd; z+=LandSegmentSize)
  {
    LandBegEnd sRect;
    sRect.xBeg=x,sRect.zBeg=z;
    sRect.xEnd=x+LandSegmentSize,sRect.zEnd=z+LandSegmentSize;
    //Ref<LandSegment> seg=GenerateSegment(sRect);
    Ref<LandSegment> seg=_segCache.Segment(this,sRect);
    // draw all water segments
    if( seg->_someWater )
    {
      // after drawing landscape draw water

      Shape *shape = &seg->_wTable;
      #if TL_LANDSCAPE
        if (EnableHWTL && EnableHWTLState)
        {
          Vector3Val bCenter=shape->BSphereCenter()+seg->Offset();
          float bRadius=shape->BSphereRadius();

          if( !scene.GetCamera()->IsClipped(bCenter,bRadius,1) )
          {
            Vector3 offset = seg->Offset()+Vector3(0,_seaLevelWave,0);
            Matrix4 trans(MTranslation,offset);
            Matrix4 iTrans(MTranslation,-offset);
            // calculate z2

            float z=scene.GetCamera()->Position().Distance(bCenter)-bRadius;
            float z2 = Square(floatMax(z,0));

            // animate water texture
            // water shape should have only one section
            Assert(shape->NSections()==1);
            ShapeSection &sec = shape->GetSection(0);
            float texPhase=fastFmod(Glob.time.toFloat(),2);
            if( texPhase>1.0f ) texPhase=2-texPhase;
            sec.properties.AnimateTexture(texPhase);

            shape->PrepareTextures(z2,0);
            LightList work(true);
            const LightList &lights = scene.SelectLights(bCenter,bRadius,work);
            shape->Draw
            (
              &GAnimatorWater,lights,ClipAll,0,trans,iTrans
            );
          }
        }
        else
        {
      #endif

      TLVertexTable tlTable;
      Matrix4Val pointView=scene.ScaledInvTransform();
      tlTable.DoTransformPoints(&GAnimatorWater,seg->_wTable,pointView);
      DrawMesh
      (
        scene,tlTable,*shape,seg->Offset(),seg->_rect,true
      );
      #if TL_LANDSCAPE
      }
      #endif
    }
  }

}

/*!
\patch_internal 1.21 Date 8/16/2001 by Ondra.
- New: Grass rendering.
*/

struct GroundLayerInfo
{
  Vector3 offset;
  float alpha1,alpha2,bright;
  bool isAlpha;

  GroundLayerInfo()
  :offset(VZero),alpha1(0),alpha2(1),isAlpha(false)
  {
  }
  GroundLayerInfo(float y, float a1, float b=1, float a2=1)
  :offset(0,y,0),
  bright(b),alpha1(a1*0.5+0.5),alpha2(a2*0.5),
  isAlpha(true)
  {
  }
};
const static GroundLayerInfo GroundLayersMax[]=
{
  #define MAX_A 1.5f
  #define MAX_S 1.5f
  GroundLayerInfo(0.01f*MAX_S,-0.10f,0.5f,MAX_A),
  GroundLayerInfo(0.02f*MAX_S,-0.20f,0.6f,MAX_A),
  GroundLayerInfo(0.03f*MAX_S,-0.30f,0.6f,MAX_A),
  GroundLayerInfo(0.04f*MAX_S,-0.40f,0.6f,MAX_A),
  GroundLayerInfo(0.05f*MAX_S,-0.50f,0.7f,MAX_A),
  GroundLayerInfo(0.06f*MAX_S,-0.60f,0.8f,MAX_A),
  GroundLayerInfo(0.07f*MAX_S,-0.70f,0.9f,MAX_A),
  GroundLayerInfo(0.08f*MAX_S,-0.80f,0.9f,MAX_A),
  GroundLayerInfo(0.09f*MAX_S,-0.90f,1.0f,MAX_A),
};
const static GroundLayerInfo GroundLayersMid[]=
{
  #define MID_A 2.0f
  GroundLayerInfo(0.01f,-0.10f,0.50f,MID_A),
  GroundLayerInfo(0.03f,-0.30f,0.55f,MID_A),
  GroundLayerInfo(0.05f,-0.50f,0.70f,MID_A),
  GroundLayerInfo(0.07f,-0.70f,0.90f,MID_A),
  GroundLayerInfo(0.09f,-0.90f,1.00f,MID_A),
};
const static GroundLayerInfo GroundLayersMin[]=
{
  #define MID_A 2.0f
  GroundLayerInfo(0.01f,-0.10f,0.60f,MID_A),
  GroundLayerInfo(0.05f,-0.50f,0.75f,MID_A),
  GroundLayerInfo(0.09f,-0.90f,1.00f,MID_A),
};

struct GrassMode
{
  const GroundLayerInfo *layers;
  int nLayers;
  const char *name;
};

GrassMode GrassModes[]=
{
  #define G_MODE(a) {GroundLayers##a,sizeof(GroundLayers##a)/sizeof(*GroundLayers##a),#a}
  G_MODE(Min),
  G_MODE(Mid),
  G_MODE(Max)
  #undef G_MODE
};

const int NGrassModes = sizeof(GrassModes)/sizeof(*GrassModes);


/*!
\patch_internal 1.21 Date 8/16/2001 by Ondra.
- Changed: Landscape drawing structure and object ordering changed
to better suit grass rendering.
\patch_internal 1.43 Date 1/29/2002 by Ondra
- Fixed: Support for high resolution terrain textures.
*/

void Landscape::DrawGround
(
  const LandBegEnd &bigRect, Scene &scene,
  const GroundLayerInfo &layer
)
{
  for( int x=bigRect.xBeg; x<bigRect.xEnd; x+=LandSegmentSize )
  for( int z=bigRect.zBeg; z<bigRect.zEnd; z+=LandSegmentSize )
  {
    LandBegEnd sRect;
    sRect.xBeg=x,sRect.zBeg=z;
    sRect.xEnd=x+LandSegmentSize,sRect.zEnd=z+LandSegmentSize;
    //Ref<LandSegment> seg=GenerateSegment(sRect);
    Ref<LandSegment> seg=_segCache.Segment(this,sRect);

#if 0 //_ENABLE_CHEATS
    float xMin = sRect.xBeg*_landGrid, xMax = sRect.xEnd*_landGrid;
    float zMin = sRect.zBeg*_landGrid, zMax = sRect.zEnd*_landGrid;
    Vector3Val cpos = GScene->GetCamera()->Position();
    if
    (
      cpos.X()>=xMin && cpos.X()<=xMax &&
      cpos.Z()>=zMin && cpos.Z()<=zMax
    )
    {
      __asm nop;
    }
#endif

    if (!EnableHWTL || !EnableHWTLState)
    {
      if( seg->_someWater && seg->_seaLevel!=_seaLevelWave )
      {
        // change level of all water points
        // for TL: adjust matrix
  
        seg->_seaLevel=_seaLevelWave;
        VertexTable &table=seg->_wTable;
        for( int i=0; i<table.NPos(); i++ )
        {
          table.SetPos(i)[1]=_seaLevelWave;
        }
      }
    }

    //if( !_cache || !_cache->ValidFor(rect) )
    
    if( !seg->_onlyWater )
    {
      // reflected==0 means reflected
      //scene.GetCamera()->SetUserClip(1);

      Shape *shape = &seg->_table;

      extern bool LogStatesOnce;
      if (LogStatesOnce)
      {
        LogF("Draw landscape segment %d,%d",bigRect.xBeg,bigRect.zBeg);
      }
      #if TL_LANDSCAPE
        if (EnableHWTL && EnableHWTLState)
        {

          Vector3Val bCenter=shape->BSphereCenter()+seg->Offset();
          float bRadius=shape->BSphereRadius();

          if( !scene.GetCamera()->IsClipped(bCenter,bRadius,1) )
          {
            // select only most important light
            LightList work(true);
            const LightList &lights = scene.SelectLights(bCenter,bRadius,work);
            float z=scene.GetCamera()->Position().Distance(bCenter)-bRadius;
            float z2 = Square(floatMax(z,0));
            shape->PrepareTextures(z2,0);

            #if 0
              Matrix4 trans(MTranslation,seg->Offset());
              Matrix4 iTrans(MTranslation,-seg->Offset());
              shape->Draw
              (
                &GAnimatorDefault,lights,ClipAll,0,trans,iTrans
              );
            #else
              // draw several layers, each with lesser alpha
              //AnimatorAlpha alpha;
              //alpha.SetAlpha(layer.alpha);
              Vector3 offset = seg->Offset()+layer.offset;

              Matrix4 trans(MTranslation,offset);
              Matrix4 iTrans(MTranslation,-offset);
              const int alphaSpec = ::IsAlpha|::NoZWrite|::GrassTexture;
              IAnimator *anim = &GAnimatorDefault;
              AnimatorBrightness animLand;
              if (layer.isAlpha)
              {
                GEngine->SetGrassParams
                (
                  layer.alpha1,layer.alpha2,
                  scene.GetCamera()->GetAdditionalClippingFar()
                );
                shape->OrSpecial(alphaSpec);
                anim = &animLand;
                animLand.SetBrigth(layer.bright);
              }
              #if 1
              shape->Draw
              (
                anim,lights,ClipAll,0,trans,iTrans
              );
              #endif
              if (layer.isAlpha)
              {
                shape->AndSpecial(~alphaSpec);
              }
            #endif
          }
        }
        else
        {
      #endif
        TLVertexTable tlTable;

        Matrix4Val pointView=scene.ScaledInvTransform();
        tlTable.DoTransformPoints(&GAnimatorDefault,seg->_table,pointView);
        // landscape always uses world space coordinates
        
        DrawMesh
        (
          scene,tlTable,*shape,seg->Offset(),sRect,false
        );
      #if TL_LANDSCAPE
        }
      #endif

    }
  }
}

/*!
\patch 1.24 Date 09/27/2001 by Ondra
- Optimized: sky drawing uses less area,
this can help framerate in high resolutions.
\patch 1.30 Date 11/02/2001 by Ondra
- Fixed: sky drawing (horinzont clipping) issues fixed (bug since 1.26).
*/
void Landscape::DrawHorizont(Scene &scene)
{
  GEngine->EnableReorderQueues(false);
  GEngine->FlushQueues();


  // draw landscape background polygon
  LODShape *horShape=_horizontObject->GetShape();
  Shape *horShape0 = horShape->Level(0);
  // 8 points, point layout:
  // 4    5
  // 23  67
  // 0    1
  Assert(horShape0->NVertex()==8);
  // we want to adjust points 0 and 1

  Vector3 direction=scene.GetCamera()->Direction();
  Vector3 horPos=scene.GetCamera()->Position(); //+direction*backZ;

  direction[1]=0;
  _horizontObject->SetOrient(direction,VUp);

  Vector3Val bc = horShape->BoundingCenter();
  horPos+=_horizontObject->DirectionModelToWorld(bc);
  _horizontObject->SetPosition(horPos);

  float extendDown = floatMax(-direction.Y(),0)+0.02;

  const Camera *camera = scene.GetCamera();

  float cameraAbove = horPos.Y()-GetSeaLevel();
  float backZ = scene.GetCamera()->ClipFar()*0.99f;
  float posX = backZ*scene.GetCamera()->Left()*2.0f;
  float negX = -posX;
  float topY = backZ*0.06f;
  float midY = backZ*0.03f;
  float botY = -cameraAbove - extendDown*backZ;

  //float botSize2 = Square(negX)+Square(botY)+Square(backZ);
  //float botCoef = InvSqrt(botSize2)*backZ;

  // calculate (estimate) inverse projection
  // of camera corners to far clipping frame

  // far clipping plane world space equation is
  Plane farPlane
  (
    camera->Direction(),
    camera->Position()+camera->Direction()*backZ
  );

  Matrix4 invTrans = _horizontObject->GetInvTransform();
  farPlane.Transform(*_horizontObject,invTrans);

  // we need to convert this to model space
  // (offset (0,0,0), orientation change)

  horShape0->SetPos(0) = Vector3(negX,botY,backZ); //*botCoef;
  horShape0->SetPos(1) = Vector3(posX,botY,backZ); //*botCoef;

  horShape0->SetPos(2) = Vector3(negX,midY,backZ);
  horShape0->SetPos(3) = Vector3(negX,midY,backZ);

  horShape0->SetPos(4) = Vector3(negX,topY,backZ);
  horShape0->SetPos(5) = Vector3(posX,topY,backZ);

  horShape0->SetPos(6) = Vector3(posX,midY,backZ);
  horShape0->SetPos(7) = Vector3(posX,midY,backZ);

  // note: pos 2..7 must keep y coordinate even after projection to far plane

  // camera (0,0,0) converted to model coordinates is
  Vector3 camZero = invTrans.FastTransform(camera->Position());
  float camZeroDistance = farPlane.Distance(camZero);
  #if 0 //_DEBUG
    Matrix4 toCam = camera->GetInvTransform()*_horizontObject->Transform();
  #endif
  for (int i=0; i<2; i++)
  {
    Vector3 pos = horShape0->Pos(i);
    // we need to calculate intersection of line camZero..pos
    // with far plane
    // we have far plane converted to model coordinates
    float posDistance = farPlane.Distance(pos);
    if (fabs(posDistance-camZeroDistance)<1e-6)
    {
      // no intersection - we have no way to calculate it
      Log("No horizont intersection %d",i);
      continue;
    }
    float t = posDistance/(posDistance-camZeroDistance);
    Vector3 isect = pos*(1-t)+camZero*t;
    // verify - project to camera space
    #if 0 // _DEBUG
      Vector3 isectCam = toCam.FastTransform(isect);

      Log
      (
        "%d: cam %.1f,%.1f,%.1f->%.2f,%.2f",
        i,isectCam[0],isectCam[1],isectCam[2],
        isectCam[0]/isectCam[2],isectCam[1]/isectCam[2]
      );
    #endif
    horShape0->SetPos(i) = isect;

  }

  horShape->SetAutoCenter(false);
  horShape->CalculateMinMax(true);

  //_horizontObject->SetScale(scale);
  // no LOD for horizont

  float horizBorder = backZ*0.025f;
  scene.GetCamera()->SetUserClipPars(VUp,-GetSeaLevel()+horizBorder);
  //_horizontObject->Draw(0,ClipAll&~ClipBack|ClipUser0,*_horizontObject);
  _horizontObject->Draw(0,ClipAll&~ClipBack|ClipUser0,*_horizontObject);
  scene.GetCamera()->CancelUserClip();

  GEngine->FlushQueues();
}

#if _ENABLE_CHEATS
#include "keyInput.hpp"
#include "dikCodes.h"
#endif

/*!
\patch 1.58 Date 5/20/2002 by Ondra
- Fixed: It was possible to see through coast in some places (e.g. Kolgujev Fd72)
*/

void Landscape::DrawRect
(
  Scene &scene, const LandBegEnd &bigRect
)
{
  Camera &camera = *scene.GetCamera();

  //Matrix4 normalView(MIdentity);
  if( bigRect.xEnd<=bigRect.xBeg ) return;
  if( bigRect.zEnd<=bigRect.zBeg ) return;
  // if cache was reconstructed too often, do not try to use it
  // it would degrade performance

  // some loop invariant expression are precalculated here

  // draw all reflected and non-reflected segments
  // split large rectangle into small segments
  #if LANDDRAW
  #if _ENABLE_CHEATS
  if (!CHECK_DIAG(DETransparent) ||! CHECK_DIAG(DEForce))
  #endif
  {
    GEngine->EnableReorderQueues(true);
    PROFILE_SCOPE(lnDrw);

    float nightEye =
    (
      (1-scene.MainLight()->Diffuse().R())*
      scene.MainLight()->NightEffect()
    );

    GEngine->EnableNightEye(nightEye);


    GroundLayerInfo opaqueLayer;
    DrawGround(bigRect,scene,opaqueLayer);
  }
  #endif

  //camera.SetUserClipPars(1,VUp,3.0);
  for( int i=0; i<_arrows.Size(); i++ )
  {
    scene.ObjectForDrawing(_arrows[i]);
  }
  //scene.GetCamera()->SetUserClip(1);

  #if LANDDRAW
  extern bool NoLandscape;
  #if _ENABLE_CHEATS
  if (!CHECK_DIAG(DETransparent) ||! CHECK_DIAG(DEForce))
  #endif
  if( !NoLandscape )
  {
    PROFILE_SCOPE(lnDrw);
    GEngine->EnableReorderQueues(true);
    DrawWater(bigRect,scene);

    DrawHorizont(scene);
  }
  #endif

  GEngine->FlushQueues();
  GEngine->EnableReorderQueues(true);
  // draw non-alpha objects
  scene.DrawObjectsAndShadowsPass1();
  
  #if LANDDRAW
    #if _ENABLE_CHEATS
      static int grassMode = 2;
      if (GEngine->CanGrass() && GInput.GetCheat1ToDo(DIK_MINUS))
      {
        grassMode++;
        if (grassMode>NGrassModes) grassMode = 0;
        if (grassMode>0)
        {
          GlobalShowMessage(500,"Grass mode %s",GrassModes[grassMode-1].name);
        }
        else
        {
          GlobalShowMessage(500,"Grass off",grassMode);
        }
      }
      if (grassMode>0 && GEngine->CanGrass())
      {
        // TODO: grass cliping dependent on FOV
        // calculate pixel size of grass
        #if 2
        // no joy with clipping - there is no effective way to clip on GeForce3
        float grassLength = 0.16f;
        Matrix4Val project = camera.Projection();
        /*

        float pixelSizeY = -project(1,1)*grassLength/z*camera.InvTop()*GEngine->Height();
        // z :: pixelSizeY==1
        project(1,1)*grassLength/z*camera.InvTop()*GEngine->Height() ==1
        project(1,1)*grassLength*camera.InvTop()*GEngine->Height() == z
        */
        float minGrassPixelSize = 1;
        float maxDist = -project(1,1)*grassLength*camera.InvTop()/minGrassPixelSize;
        
        #if 0
          float psVer = -project(1,1)*grassLength/maxDist*camera.InvTop();
          GlobalShowMessage
          (
            100,"Max grass dist %.1f, ver %.2f",maxDist,psVer
          );
        #endif
        camera.SetAdditionalClipping(0,maxDist);
        #endif

        const GrassMode &mode = GrassModes[grassMode-1];
        for (int i=0; i<mode.nLayers; i++)
        {
          DrawGround(bigRect,scene,mode.layers[i]);
        }
        // reset additional clipping to infinity
        camera.SetAdditionalClipping(0,100000);
      }
    #endif
  #endif
  GEngine->FlushQueues();

  // draw alpha objects and shadows
  scene.DrawObjectsAndShadowsPass2();

  GEngine->EnableReorderQueues(false);
  // clear any outstanding arrows (Buldozer ONLY)
  _arrows.Clear();
}

void Landscape::CalculBoundingRect
(
  LandBegEnd &res, const Camera &camera, float dist, float grid
)
{
  Point3 corner[8];
  float cLeftFar=camera.Left()*dist;
  float cTopFar=camera.Top()*dist;
  float cFar=dist;

  Matrix4Val invView=camera.Transform();

  //float invSize=1.0;
  // middle point
  corner[0]=camera.Position();
  corner[1].SetFastTransform(invView,Point3(0,0,cFar));
  /*
  float invSize=cFar*InvSqrt(cLeftFar*cLeftFar+cTopFar*cTopFar+cFar*cFar);
  corner[2].SetFastTransform(invView,Point3(-cLeftFar,-cTopFar,cFar)*invSize);
  corner[3].SetFastTransform(invView,Point3(+cLeftFar,-cTopFar,cFar)*invSize);
  corner[4].SetFastTransform(invView,Point3(-cLeftFar,+cTopFar,cFar)*invSize);
  corner[5].SetFastTransform(invView,Point3(+cLeftFar,+cTopFar,cFar)*invSize);
  */
  corner[2].SetFastTransform(invView,Vector3(-cLeftFar,-cTopFar,cFar));
  corner[3].SetFastTransform(invView,Vector3(+cLeftFar,-cTopFar,cFar));
  corner[4].SetFastTransform(invView,Vector3(-cLeftFar,+cTopFar,cFar));
  corner[5].SetFastTransform(invView,Vector3(+cLeftFar,+cTopFar,cFar));

  Coord xMin,xMax,zMin,zMax;
  // always include camera position
  xMin=xMax=corner[0].X();
  zMin=zMax=corner[0].Z();
  for( int i=1; i<6; i++ )
  { 
    Coord x=corner[i].X(),z=corner[i].Z();
    saturateMin(xMin,x);
    saturateMin(zMin,z);
    saturateMax(xMax,x);
    saturateMax(zMax,z);
  }
  float iGrid=1.0f/grid;
  res.xBeg=toIntFloor(xMin*iGrid)-1; // leave some reserve
  res.zBeg=toIntFloor(zMin*iGrid)-1;
  res.xEnd=toIntCeil(xMax*iGrid)+1;
  res.zEnd=toIntCeil(zMax*iGrid)+1;
}

// draw sky using pre-builded cover
// load cover definition

void Landscape::DrawSky(Scene &scene)
{
  //if( !Glob.config.background ) return;

  Camera &camera=*scene.GetCamera();

  // calculate sun position
  LightSun *sun=scene.MainLight();
  Vector3Val skyPosition=camera.Position();
  _skyObject->SetPosition(skyPosition+_skyObject->GetShape()->BoundingCenter());
  _starsObject->SetPosition(skyPosition+_starsObject->GetShape()->BoundingCenter());
  // rotate stars
  _starsObject->SetOrientation(sun->StarsOrientation());
  const float sunScale = 120.0/12000;
  {
    Vector3 relPos = sun->SunDirection()*12000*sunScale;
    Vector3 sunPosition=camera.Position()-relPos;
    _sunObject->SetScale(sunScale);
    _sunObject->SetPosition(sunPosition);

    //LogF("Sun rel pos %.2f,%.2f,%.2f",relPos[0],relPos[1],relPos[2]);
  }
  {
    Point3 moonPosition=camera.Position()-sun->MoonDirection()*12000*sunScale;
    _moonObject->SetPosition(moonPosition);
    Matrix3 moonOrient;
    moonOrient.SetDirectionAndUp(-sun->MoonDirection(),sun->MoonDirectionUp());
    _moonObject->SetOrientation(moonOrient);
    _moonObject->SetScale(sunScale);
    Shape *shape=_moonObject->GetShape()->LevelOpaque(0);
    if (shape->NFaces()>=2)
    {
      shape->FaceIndexed(1).AnimateTexture(sun->MoonPhase());
    }
    if (shape->NSections()>=2)
    {
      shape->GetSection(1).properties.AnimateTexture(sun->MoonPhase());
    }
  }

  float clipLevel = skyPosition.Y();
  scene.GetCamera()->SetUserClipPars(VUp,-clipLevel);

  _skyObject->Draw(0,ClipAll&~ClipBack|ClipUser0,*_skyObject);
  float starsVisibility=
  (
    // see TLVertexMesh::DoStarLighting
    // overcast limitation
    (1.5*SkyThrough()-0.5)*
    // daytime limitation
    sun->StarsVisibility()
  );
  if( starsVisibility>=0.1 )
  {
    _starsObject->DrawPoints(0,ClipAll&~ClipBack|ClipUser0,*_starsObject);
  }
  scene.GetCamera()->CancelUserClip();

  _sunObject->Draw(0,ClipAll&~ClipBack,*_sunObject);
  _moonObject->Draw(0,ClipAll&~ClipBack,*_moonObject);

}


// there are three separate clouds levels
#define SKY_LEVELS 3

#define SKY_Z ( 20000 )

#define SKY_GRID ( 6000.0f )

struct CloudInfo
{
  float posX,posZ,azimut;
};

TypeIsSimple(CloudInfo)


void Landscape::DrawClouds(Scene &scene)
{
  const Camera &camera=*scene.GetCamera();

  //Matrix4Val invView=camera.Transform();

  //Vector3Val pos=camera.Position();
  LandBegEnd skyBegEnd;
  CalculBoundingRect(skyBegEnd,camera,SKY_Z,SKY_GRID);

  // precalculate variables for fast clipping

  // try to clip bounding sphere
  //Matrix4Val centerView=scene.ScaledInvTransform();
  
  int skyLevel;
  //Ref<Object> cloudObj[N_CLOUDS];
  int i;
  for( i=0; i<N_CLOUDS; i++ ) if( !_cloudObj[i] ) 
  {
    _cloudObj[i]=new ObjectPlain(GScene->Preloaded(CloudShapes[i]),-1);
  }
  //scene.GetCamera()->SetUserClip(1);
  AUTO_STATIC_ARRAY(CloudInfo,clouds0,1024);
  AUTO_STATIC_ARRAY(CloudInfo,clouds1,1024);
  AUTO_STATIC_ARRAY(CloudInfo,clouds2,1024);
  AUTO_STATIC_ARRAY(CloudInfo,clouds3,1024);
  typedef StaticArrayAuto<CloudInfo> CloudArray;
  CloudArray *clouds[N_CLOUDS]={&clouds0,&clouds1,&clouds2,&clouds3};
  for( skyLevel=0; skyLevel<SKY_LEVELS; skyLevel++ )
  {
    clouds0.Resize(0);
    clouds1.Resize(0);
    clouds2.Resize(0);
    clouds3.Resize(0);
    const static float skyY[SKY_LEVELS]={6000,4000,2000};
    #define SP ( 2.0f )
    const static float skyXSpeed[SKY_LEVELS]={+0.020*SP,+0.012*SP,+0.010*SP};
    const static float skyZSpeed[SKY_LEVELS]={+0.004*SP,-0.002*SP,-0.004*SP};
    int xx,zz;
    
    float cPos=CloudsPosition();
    float xMove=cPos*skyXSpeed[skyLevel];
    float zMove=cPos*skyZSpeed[skyLevel];

    float height=CloudsBrightness();
    saturate(height,0.6,1);

    int xMoveInt=toIntFloor(xMove);
    int zMoveInt=toIntFloor(zMove);
    xMove-=xMoveInt;
    zMove-=zMoveInt;
    
    xMove*=SKY_GRID;
    zMove*=SKY_GRID;

    float posY = skyY[skyLevel]*height;

    for( zz=skyBegEnd.zBeg; zz<=skyBegEnd.zEnd; zz++ )
    for( xx=skyBegEnd.xBeg; xx<=skyBegEnd.xEnd; xx++ )
    {
      // preloaded objects _cloud[]
      int seedXZ=_randGen.GetSeed(xx-xMoveInt,zz-zMoveInt,skyLevel);
      float isHereF=_randGen.RandomValue(seedXZ);
      //RandBegin(xx-xMoveInt,zz-zMoveInt,skyLevel);
      int isHere=toIntFloor(isHereF*8);
      if( isHere>=N_CLOUDS ) continue;
      CloudArray &array = *clouds[isHere];
      CloudInfo &info = array.Append();

      float xOffset=xMove+_randGen.RandomValue(seedXZ+1)*(SKY_GRID*0.5);
      float zOffset=zMove+_randGen.RandomValue(seedXZ+2)*(SKY_GRID*0.5);

      info.posX = xx*SKY_GRID+xOffset;
      info.posZ = zz*SKY_GRID+zOffset;
      info.azimut = _randGen.RandomValue(seedXZ+3)*(2*H_PI/2);
      //Point3 pos=Vector3(xx*SKY_GRID+xOffset,skyY[skyLevel]*height,zz*SKY_GRID+zOffset);
  
    }
    for (int isHere=0; isHere<N_CLOUDS; isHere++ )
    {
      Vector3Val camPos = camera.Position();
      Object *object=_cloudObj[isHere];
      if( !object ) continue;

      CloudArray &cloudsType=*clouds[isHere];
      for (int i=0; i<cloudsType.Size(); i++)
      {
        const CloudInfo &info = cloudsType[i];

        Vector3 pos(info.posX,posY,info.posZ);
        Matrix3 orient(MRotationY,info.azimut);
        orient *= CloudScale;

        // pretend clouds are much further
        // by adding part of camera position

        //pos = (pos-camPos)*CloudScale+camPos;
        pos = pos*CloudScale+(1-CloudScale)*camPos;

        object->SetOrient(orient);
        object->SetPosition(pos);

        // note: radius is constant for all clouds of given type
        float radius=object->GetRadius();
        if( (camera.IsClipped(pos,radius,1)&~ClipBack)==ClipNone )
        {
          /*
          LogF
          (
            "cloud pos %.1f,%.1f,%.1f",pos[0],pos[1],pos[2]
          );
          LogF
          (
            "  rel pos %.1f,%.1f,%.1f",
            pos[0]-camPos[0],pos[1]-camPos[1],pos[2]-camPos[2]
          );
          */
          // no LOD for clouds
          object->Draw(0,ClipAll&~ClipBack,*object);

        }
      }
    }
  }
}

/*!
\patch 1.30 Date 11/05/2001 by Ondra.
- Fixed: Sky cloud layer clipping.
*/

void Landscape::Draw( Scene &scene )
{
  {
    PROFILE_SCOPE(skDrw);
    Camera &camera=*scene.GetCamera();

    float oldNear = camera.ClipNear();
    float oldFar = camera.ClipFar();
    //camera.SetClipRange(10,1500);
    camera.SetClipRange(10,4000);

    DrawSky(scene);
    DrawClouds(scene);
    GEngine->FlushQueues();

    camera.SetClipRange(oldNear,oldFar);
  }

  // project points limiting viewpoint
  LandBegEnd begEnd;
  CalculBoundingRect(begEnd,*scene.GetCamera(),scene.GetFogMaxRange(),_landGrid);

  // use only rough rectangles - no generalization
  begEnd.xBeg&=~(LandSegmentSize-1);
  begEnd.zBeg&=~(LandSegmentSize-1);
  begEnd.xEnd=(begEnd.xEnd+(LandSegmentSize-1))&~(LandSegmentSize-1);
  begEnd.zEnd=(begEnd.zEnd+(LandSegmentSize-1))&~(LandSegmentSize-1);
  // detect overflow
  // limits correspond to 13000 km
  // any larger coordinates must mean some kind of error

  const int maxCoord =  0x40000;
  const int minCoord = -0x40000;
  if
  (
    begEnd.xBeg>maxCoord || begEnd.xBeg<minCoord ||
    begEnd.zBeg>maxCoord || begEnd.zBeg<minCoord
  )
  {
    Fail("Ground drawing out of valid range");
    RptF("  Rect %d,%d..%d,%d",begEnd.xBeg,begEnd.zBeg,begEnd.xEnd,begEnd.zEnd);
    return;
  }
  if
  (
    begEnd.xEnd-begEnd.xBeg>0x1000 ||
    begEnd.zEnd-begEnd.zBeg>0x1000
  )
  {
    Fail("Ground drawing segment too big");
    RptF("  Rect %d,%d..%d,%d",begEnd.xBeg,begEnd.zBeg,begEnd.xEnd,begEnd.zEnd);
    RptF("  Fog max range %.1f",scene.GetFogMaxRange());
    return;
  }

  int requiredCacheSize = (begEnd.xEnd-begEnd.xBeg)*(begEnd.zEnd-begEnd.zBeg)/(LandSegmentSize*LandSegmentSize);
  int estimatedCacheSize = CalculateCacheSize(this);
  _segCache._maxN = estimatedCacheSize;
  saturateMax(_segCache._maxN,requiredCacheSize);
  #if 0 //_ENABLE_REPORT
    GlobalShowMessage
    (
      500,"landrect %d,%d..%d,%d (%d,%d), segments %d, cache %d",
      begEnd.xBeg,begEnd.zBeg,
      begEnd.xEnd,begEnd.zEnd,
      begEnd.xEnd-begEnd.xBeg,begEnd.zEnd-begEnd.zBeg,
      requiredCacheSize,estimatedCacheSize
    );

  #endif

  DrawRect(scene,begEnd);
}


Object *Landscape::GetObject( int id ) const
{
  if (_objectIds.Size()<=0)
  {
    ErrF("No Object ID cache - performing slow search");
    return FindObjectNC(id);
  }
  if( id<0 || id>=_objectIds.Size() ) return NULL;
  return _objectIds[id];
}

Texture *Landscape::GetTexture( int id ) const
{
  if( id<0 || id>=_texture.Size() ) return NULL;
  return _texture[id];
}
void Landscape::RegisterTexture( int id, const char *name )
{
  SetTexture(id,name);
}
void Landscape::RegisterObjectType( const char *name )
{
  // add a shape into the bank
  Ref<LODShapeWithShadow> shape=Shapes.New(name,false,true);
}

void Landscape::Quit()
{
}

/*!
\patch 1.50 Date 4/5/2002 by Ondra
- Fixed: Landscape texture size in preferences was ignored,
Object texture settings was used instead.
*/

void Landscape::SetTexture( int i, const char *name )
{
  char aName[64];
  strcpy(aName,name);
  strlwr(aName);
  // some engines use reflections - use alpha instead of water
  //if( !strcmp(aName,"landtext\\mo.pac") )
  if( i==0 )
  {
    strcpy(aName,"data\\more_anim.01.pac");
    AnimatedTexture *aWater=GlobLoadTextureAnimated("data\\more_anim.01.pac");
    _texture.Access(i);
    if (aWater)
    {
      _texture[i].texture=(*aWater)[0];
      _texture[i].offsetUV=false;
      for( int a=0; a<aWater->Size(); a++ )
      {
        Texture *aw=aWater->Get(a);
        if (aw)
        {
          aw->SetMultitexturing(1); // specular highlihts
          aw->SetMaxSize(Glob.config.maxLandText);
        }
      }
    }
    else
    {
      _texture[i].texture=NULL;
      _texture[i].offsetUV=false;
    }
    return;
  }
  _texture.Access(i);
  _texture[i].texture=GlobLoadTexture(aName);
  if (_texture[i].texture)
  {
    _texture[i].texture->SetMultitexturing(1); // detail texture
    _texture[i].texture->SetMaxSize(Glob.config.maxLandText);
    bool simple=false;
    {
      // check if it is simple texture
      const char *name=strchr(aName,'\\');
      const char *ext=strrchr(aName,'.');
      if( ext==name+3 )
      {
        simple=true;
      }
    }
    _texture[i].offsetUV=simple;
  }
  else
  {
    _texture[i].offsetUV=true;
  }
}

Landscape::~Landscape()
{
  /*
  for( int i=0; i<MAX_NETWORKS; i++ )
  {
    if( _networks[i] ) delete _networks[i],_networks[i]=NULL;
  }
  */
  // clear all objects
  int x,z;
  for( z=0; z<_landRange; z++ ) for( x=0; x<_landRange; x++ )
  {
    _objects(x,z).Clear();
  }
}

/*!
\patch 1.05 Date 7/17/2001 by Ondra.
- Fixed: Random seed was used for landscape bump generator.
In MP this caused different simulation results on local and remote computer,
resulting in vehicle shaking.
*/

float Landscape::CalculateBump
(
  float xC, float zC, Texture *texture, float bumpy
) const
{
  // TODO: bilinear interpolation of texture roughness in corners
  //const float bumpScale=1.0/0.3;
  const float bumpScale=1.0;
  float bumpX=xC*bumpScale;
  float bumpZ=zC*bumpScale;
  int iBumpX=toIntFloor(bumpX);
  int iBumpZ=toIntFloor(bumpZ);
  // 
  //RandBegin(iBumpX,iBumpZ);
  float bump00=_randGen.RandomValue(_randGen.GetSeed(iBumpX,iBumpZ));
  float bump01=_randGen.RandomValue(_randGen.GetSeed(iBumpX+1,iBumpZ));
  float bump10=_randGen.RandomValue(_randGen.GetSeed(iBumpX,iBumpZ+1));
  float bump11=_randGen.RandomValue(_randGen.GetSeed(iBumpX+1,iBumpZ+1));

  #if 1
    bumpX-=iBumpX; // relative in-bump coordinates
    bumpZ-=iBumpZ;
    // bilinear interpolation of bumpY
    float bump0=bump00+(bump01-bump00)*bumpX;
    float bump1=bump10+(bump11-bump10)*bumpX;
    float bump=bump0+(bump1-bump0)*bumpZ; // result in range 0 .. 1
    bump-=0.5;
  #else
    const float bump=0;
  #endif
  float tr=0.05;
  // TODO: check where does NULL texture come from
  if( texture ) tr=texture->Roughness();
  return bump*tr*bumpy;
}

float Landscape::BumpySurfaceY
(
  float xC, float zC, float &rdX, float &rdY,
  Texture *&texture, float bumpy, float &bump
) const
{
  // fine rectangles are not used - use rough instead
  // calculate surface level on given coordinates
  float xRel=xC*_invTerrainGrid;
  float zRel=zC*_invTerrainGrid;
  int x=toIntFloor(xRel);
  int z=toIntFloor(zRel);
  float xIn=xRel-x; // relative 0..1 in square
  float zIn=zRel-z;

  bump = 0;
  #if !USE_SWIZZLED_ARRAYS
    if( !this_TerrainInRange(z,x) || !this_TerrainInRange(z+1,x+1) ) return YOutsideMap; // no bump outside landscape
    float y00=GetData(x,z);
    float y01=GetData(x+1,z);
    float y10=GetData(x,z+1);
    float y11=GetData(x+1,z+1);
  #else
    if( !this_TerrainInRange(z,x) || !this_TerrainInRange(z+1,x+1) ) return YOutsideMap; // no bump outside landscape
    RawType y4[2][2];
    _data.GetFour(y4,x,z);
    float y00=RawToHeight(y4[0][0]);
    float y01=RawToHeight(y4[0][1]);
    float y10=RawToHeight(y4[1][0]);
    float y11=RawToHeight(y4[1][1]);
  #endif

  // each face is divided to two triangles
  // determine which triangle contains point
  float y;
  if( xIn<=1-zIn )
  { // triangle 00,01,10
    rdX=(y01-y00)*_invTerrainGrid;
    rdY=(y10-y00)*_invTerrainGrid;
    y=y00+(y10-y00)*zIn+(y01-y00)*xIn;
  }
  else
  {
    // triangle 01,10,11
    rdX=(y11-y10)*_invTerrainGrid;
    rdY=(y11-y01)*_invTerrainGrid;
    y=y11+(y10-y11)*(1-xIn)+(y01-y11)*(1-zIn);
  }
  // depending on surface texture use roughness
  int tLog = _terrainRangeLog-_landRangeLog;
  Texture *rTexture=ClippedTexture(z>>tLog,x>>tLog);
  bump = CalculateBump(xC,zC,rTexture,bumpy);
  texture = rTexture;
  return y;
}

void Landscape::SurfacePlane( Plane &plane, float xC, float zC ) const
{
  // calculate surface level on given coordinates
  float xRel=xC*_invTerrainGrid;
  float zRel=zC*_invTerrainGrid;
  int x=toIntFloor(xRel);
  int z=toIntFloor(zRel);
  float xIn=xRel-x; // relative 0..1 in square
  float zIn=zRel-z;

  #if !USE_SWIZZLED_ARRAYS
    if( !this_TerrainInRange(z,x) || !this_TerrainInRange(z+1,x+1) )
    {
      plane.SetNormal(VUp,-YOutsideMap);
      return;
    }
    float y00=GetData(x,z);
    float y01=GetData(x+1,z);
    float y10=GetData(x,z+1);
    float y11=GetData(x+1,z+1);
  #else
    if( !this_TerrainInRange(z,x) || !this_TerrainInRange(z+1,x+1) ) return YOutsideMap; // no bump outside landscape
    RawType y4[2][2];
    _data.GetFour(y4,x,z);
    float y00=RawToHeight(y4[0][0]);
    float y01=RawToHeight(y4[0][1]);
    float y10=RawToHeight(y4[1][0]);
    float y11=RawToHeight(y4[1][1]);
  #endif
  
  // each face is divided to two triangles
  // determine which triangle contains point
  Vector3 normal=VUp;
  if( xIn<=1-zIn )
  { // triangle 00,01,10

    normal[0]=(y01-y00)*-_invTerrainGrid;
    normal[2]=(y10-y00)*-_invTerrainGrid;
  }
  else
  {
    // triangle 01,10,11
    normal[0]=(y11-y10)*-_invTerrainGrid;
    normal[2]=(y11-y01)*-_invTerrainGrid;
  }
  Vector3 point((x+1)*_terrainGrid,y01,z*_terrainGrid);
  plane=Plane(normal,point);
}

float Landscape::SurfaceY( float x, float z ) const
{
  // code equivalent to
  // return SurfaceY(x,z,NULL,NULL,NULL);
  // as this case is quite common, it is optimized

  // it might be also worth to separate SurfaceY into two parts:
  // preparation and calculation
  // preparation would gather and return yii values
  // fine rectangles are not used - use rough instead
  // calculate surface level on given coordinates
  float xRel=x*_invTerrainGrid;
  float zRel=z*_invTerrainGrid;
  int xi=toIntFloor(xRel);
  int zi=toIntFloor(zRel);
  float xIn=xRel-xi; // relative 0..1 in square
  float zIn=zRel-zi;

  #if !USE_SWIZZLED_ARRAYS
    if( !this_TerrainInRange(zi,xi) || !this_TerrainInRange(zi+1,xi+1) ) return YOutsideMap; // no bump outside landscape
    float y00=GetData(xi,zi);
    float y01=GetData(xi+1,zi);
    float y10=GetData(xi,zi+1);
    float y11=GetData(xi+1,zi+1);
  #else
    if( !this_TerrainInRange(zi,xi) || !this_TerrainInRange(zi+1,xi+1) ) return YOutsideMap; // no bump outside landscape
    RawType y4[2][2];
    _data.GetFour(y4,xi,zi);
    float y00=RawToHeight(y4[0][0]);
    float y01=RawToHeight(y4[0][1]);
    float y10=RawToHeight(y4[1][0]);
    float y11=RawToHeight(y4[1][1]);
  #endif
  
  // each face is divided to two triangles
  // determine which triangle contains point
  if( xIn<=1-zIn )
  { // triangle 00,01,10
    float d1000=y10-y00;
    float d0100=y01-y00;
    return y00+d1000*zIn+d0100*xIn;
  }
  else
  {
    // triangle 01,10,11
    float d1011=y10-y11;
    float d0111=y01-y11;
    return y10+d0111-d1011*xIn-d0111*zIn;
  }
}

float Landscape::SurfaceY
(
  float x, float z, float *rdX, float *rdY,
  Texture **texture
) const
{
  // fine rectangles are not used - use rough instead
  // calculate surface level on given coordinates
  float xRel=x*_invTerrainGrid;
  float zRel=z*_invTerrainGrid;
  int xi=toIntFloor(xRel);
  int zi=toIntFloor(zRel);
  float xIn=xRel-xi; // relative 0..1 in square
  float zIn=zRel-zi;

  if( texture )
  {
    int tLog = _terrainRangeLog-_landRangeLog;
    *texture=ClippedTexture(zi>>tLog,xi>>tLog);
  }
  
  #if !USE_SWIZZLED_ARRAYS
    if( !this_TerrainInRange(zi,xi) || !this_TerrainInRange(zi+1,xi+1) ) return YOutsideMap; // no bump outside landscape
    float y00=GetData(xi,zi);
    float y01=GetData(xi+1,zi);
    float y10=GetData(xi,zi+1);
    float y11=GetData(xi+1,zi+1);
  #else
    if( !this_TerrainInRange(zi,xi) || !this_TerrainInRange(zi+1,xi+1) ) return YOutsideMap; // no bump outside landscape
    RawType y4[2][2];
    _data.GetFour(y4,xi,zi);
    float y00=RawToHeight(y4[0][0]);
    float y01=RawToHeight(y4[0][1]);
    float y10=RawToHeight(y4[1][0]);
    float y11=RawToHeight(y4[1][1]);
  #endif
  
  // each face is divided to two triangles
  // determine which triangle contains point
  if( xIn<=1-zIn )
  { // triangle 00,01,10
    float d1000=y10-y00;
    float d0100=y01-y00;
    if( rdX )
    {
      *rdX=d0100*_invTerrainGrid;
      *rdY=d1000*_invTerrainGrid;
    }
    return y00+d1000*zIn+d0100*xIn;
  }
  else
  {
    // triangle 01,10,11
    float d1011=y10-y11;
    float d0111=y01-y11;
    if( rdX )
    {
      *rdX=d1011*-_invTerrainGrid;
      *rdY=d0111*-_invTerrainGrid;
    }
    return y10+d0111-d1011*xIn-d0111*zIn;
  }
}


float Landscape::RoadSurfaceY
(
  float xC, float zC, float *dX, float *dZ, Texture **texture
) const
{
  Texture *surfTexture=NULL;
  float sdX,sdZ;
  float landY=SurfaceY(xC,zC,&sdX,&sdZ,&surfTexture);

  // if we are on road, return road surface parameters
  // check all near network object clipping boxes

  int xMin,xMax,zMin,zMax;
  Vector3 pos(xC,0,zC);
  ObjRadiusRectangle(xMin,xMax,zMin,zMax,pos,pos,0);
  // prepare return variables
  float maxY=-1e10;
  float maxDX=0, maxDZ=0;
  Texture *maxTexture=NULL;
  Point3 ret;
  // scan for roads
  for( int z=zMin; z<=zMax; z++ ) for( int x=xMin; x<=xMax; x++ )
  {
    Point3 pos(xC,0,zC);
    const ObjectList &list=_objects(x,z);
    int n=list.Size();
    for( int i=0; i<n; i++ )
    {
      Object *obj=list[i];
      if( !obj ) continue; // no collisions with roads
      // check bounding box collision
      LODShape *lShape=obj->GetShape();
      if( !lShape ) continue;
      Shape *shape=lShape->RoadwayLevel();
      if( !shape ) continue;
      float oRad=lShape->BoundingSphere();
      float distXZ2=(obj->Position()-pos).SquareSizeXZ();
      if( distXZ2>oRad*oRad ) continue;
      Matrix4Val invTransform=obj->GetInvTransform();
      obj->Animate(lShape->FindRoadwayLevel());

      // roadway ready - check collision
      // has top and sometimes side polygons
      // many polygons can incide if we look from the top
      Vector3 modelPos=invTransform.FastTransform(Point3(xC,0,zC));
      int fi=0;
      shape->InitPlanes();
      shape->RecalculateNormalsAsNeeded();
      for( Offset f=shape->BeginFaces(),e=shape->EndFaces(); f<e; shape->NextFace(f),fi++ )
      {
        const Poly &face=shape->Face(f);
        const Plane &plane=shape->GetPlane(fi);
        float pdX,pdZ;
        if( face.InsideFromTop(*shape,plane,modelPos,&modelPos[1],&pdX,&pdZ) )
        {
          // the face is not sure to be a triangle
          if( shape->GetOrHints()&ClipLandOn )
          {
            if( maxY<=landY )
            {
              maxTexture=face.GetTexture();
              maxY=landY;
              maxDX=sdX;
              maxDZ=sdZ;
              ret=Vector3(xC,landY,zC);
            }
          }
          else if( maxY<=modelPos[1] )
          {
            maxY=modelPos[1],maxDX=pdX,maxDZ=pdZ;
            maxTexture=face.GetTexture();
            ret=obj->PositionModelToWorld(modelPos);
          }
        }
      }
      obj->Deanimate(lShape->FindRoadwayLevel());
    }
  }

  if( maxY>=-1e3 )
  {
    //_world->GetScene()->DrawCollisionStar(ret);
    // if road is under surface, use normal surface
    if( ret.Y()>=landY )
    {
      if( dX ) *dX=maxDX,*dZ=maxDZ;
      if( texture ) *texture=maxTexture;
      return ret.Y();
    }
  }
  // otherwise return SurfaceY
  if( texture ) *texture=surfTexture;
  if( dX ) *dX=sdX,*dZ=sdZ;
  return landY;
}

float Landscape::RoadSurfaceY
(
  Vector3Par pos, float *dX, float *dZ,
  Texture **texture, Object **obj
) const
{
  Texture *surfTexture=NULL;
  float sdX,sdZ;
  float landY=SurfaceY(pos.X(),pos.Z(),&sdX,&sdZ,&surfTexture);

  // if we are on road, return road surface parameters
  // check all near network object clipping boxes

  int xMin,xMax,zMin,zMax;
  ObjRadiusRectangle(xMin,xMax,zMin,zMax,pos,pos,0);
  // prepare return variables
  float maxY=-1e10;
  float maxDX=0, maxDZ=0;
  Texture *maxTexture=NULL;
  Point3 ret;
  Object *dXdZRelToObj = NULL;
  // scan for roads
  for( int z=zMin; z<=zMax; z++ ) for( int x=xMin; x<=xMax; x++ )
  {
    const ObjectList &list=_objects(x,z);
    int n=list.Size();
    for( int i=0; i<n; i++ )
    {
      Object *o=list[i];
      if( !o ) continue; // no collisions with roads
      // check bounding box collision
      LODShape *lShape=o->GetShape();
      if( !lShape ) continue;
      Shape *shape=lShape->RoadwayLevel();
      if( !shape ) continue;
      float oRad=lShape->BoundingSphere();
      float distXZ2=(o->Position()-pos).SquareSizeXZ();
      if( distXZ2>oRad*oRad ) continue;
      Matrix4Val invTransform=o->GetInvTransform();
      // roadway ready - check collision
      // has top and sometimes side polygons
      // many polygons can incide if we look from the top
      Vector3 modelPos=invTransform.FastTransform(pos);
      int fi=0;
      o->Animate(lShape->FindRoadwayLevel());
      shape->InitPlanes();
      // note: plane may be animated in some cases
      // it may be necessary to recalculate it
      shape->RecalculateNormalsAsNeeded();
      for( Offset f=shape->BeginFaces(),e=shape->EndFaces(); f<e; shape->NextFace(f),fi++ )
      {
        const Poly &face=shape->Face(f);
        const Plane &plane=shape->GetPlane(fi);
        float pdX,pdZ;
        if( face.InsideFromTop(*shape,plane,modelPos,&modelPos[1],&pdX,&pdZ) )
        {
          // the face is not sure to be a triangle
          if( shape->GetOrHints()&ClipLandOn )
          {
            if( maxY<=landY )
            {
              maxTexture=face.GetTexture();
              maxY=landY;
              maxDX=sdX;
              maxDZ=sdZ;
              ret=Vector3(pos.X(),landY,pos.Z());
              if (obj) *obj = o;
            }
          }
          else
          {
            Vector3 wPos = o->PositionModelToWorld(modelPos);
            if (wPos[1]<=pos.Y() && maxY<=wPos[1])
            {
              // create normal, transform it, calculate dX, dZ as atan
              maxY=wPos[1],maxDX=pdX,maxDZ=pdZ;
              maxTexture=face.GetTexture();
              dXdZRelToObj = o;
              ret=wPos;
              if (obj) *obj = o;
            }
          }
        }
      }
      o->Deanimate(lShape->FindRoadwayLevel());
    }
  }

  if( maxY>=-1e3 )
  {
    //_world->GetScene()->DrawCollisionStar(ret);
    // if road is under surface, use normal surface
    if( ret.Y()>=landY )
    {
      if( dX )
      {
        if (dXdZRelToObj)
        {
          // transform maxDX, maxDZ to world space
          // create normal
          Vector3 normal(-maxDX,1,-maxDZ);
          normal.Normalize();
          // transform normal to world space
          dXdZRelToObj->DirectionModelToWorld(normal,normal);
          if (fabs(normal.Y())>1e-2)
          {
            maxDX = -normal.X()/normal.Y();
            maxDZ = -normal.Z()/normal.Y();
          }
          else
          {
            maxDX = 0;
            maxDZ = 0;
          }
        }
        *dX=maxDX,*dZ=maxDZ;
      }
      if( texture ) *texture=maxTexture;
      return ret.Y();
    }
  }
  // otherwise return SurfaceY
  if( texture ) *texture=surfTexture;
  if( dX ) *dX=sdX,*dZ=sdZ;
  if (obj) *obj = NULL;
  return landY;
}

float Landscape::RoadSurfaceYAboveWater
(
  Vector3Par pos, float *dX, float *dZ, Texture **texture
) const
{
  float y=RoadSurfaceY(pos,dX,dZ,texture);
  float minY=GetSeaLevel();
  if( y<minY )
  {
    y=minY;
    if( dX ) *dX=*dZ=0;
    if( texture ) *texture=_texture[0];
  }
  return y;
}

float Landscape::SurfaceYAboveWater( float x, float z ) const
{
  float y=SurfaceY(x,z,NULL,NULL,NULL);
  float minY=GetSeaLevel();
  if( y<minY )
  {
    y=minY;
  }
  return y;
}

float Landscape::SurfaceYAboveWater
(
  float x, float z, float *rdX, float *rdZ,
  Texture **texture
) const
{
  float y=SurfaceY(x,z,rdX,rdZ,texture);
  float minY=GetSeaLevel();
  if( y<minY )
  {
    y=minY;
    if( rdX ) *rdX=*rdZ=0;
    if( texture ) *texture=_texture[0];
  }
  return y;
}

float Landscape::RoadSurfaceYAboveWater
(
  float xC, float zC, float *dX, float *dZ,
  Texture **texture
) const
{
  float y=RoadSurfaceY(xC,zC,dX,dZ,texture);
  float minY=GetSeaLevel();
  if( y<minY )
  {
    y=minY;
    if( dX ) *dX=*dZ=0;
    if( texture ) *texture=_texture[0];
  }
  return y;
}

float Landscape::WaterDepth( float xC, float zC ) const
{
  // TODO: implement water surface simulation
  return 0;
}

Point3 Landscape::PointOnSurface( float x, float y, float z ) const
{
  float surfaceY=SurfaceYAboveWater(x,z);
  return Point3(x,surfaceY+y,z);
}

float Landscape::AboveSurface(Vector3Val pos) const
{
  float surfaceY=SurfaceY(pos.X(),pos.Z());
  return pos.Y()-surfaceY;
}

float Landscape::AboveSurfaceOrWater(Vector3Val pos) const
{
  float surfaceY=SurfaceYAboveWater(pos.X(),pos.Z());
  return pos.Y()-surfaceY;
}

AutoArray<int> Landscape::GetObjectIDList() const
{
  AutoArray<int> ret;
  for( int i=0; i<_objectIds.Size(); i++ )
  {
    Object *obj=_objectIds[i];
    if( obj ) ret.Add(obj->ID());
  }
  return ret;
}

//#define LOG_OBJ_SHAPE "handgrenade"

inline bool IsShape(Object *obj, const char *name)
{
  if (!obj->GetShape()) return false;
  return strstr(obj->GetShape()->Name(),name)!=NULL;
}

void Landscape::AddObject( Object *obj, int *xr, int *zr, bool avoidRecalculation)
{
  int x,z;
  SelectObjectList(x,z,obj->Position().X(),obj->Position().Z());
  if (xr) *xr = x;
  if (zr) *zr = z;
  // add into corresponding list
  ObjectList &list=_objects(x,z);
  Verify( list.Add(obj,x,z,avoidRecalculation)>=0 ); // <0  -> error
  #ifdef LOG_OBJ_SHAPE
    if (IsShape(obj,LOG_OBJ_SHAPE))
    {
      LogF
      (
        "add obj %x:%s at %.2f,%.2f (%d,%d)",obj,obj->GetShape()->Name(),
        obj->Position().X(),obj->Position().Z(),x,z
      );
    }
  #endif

}

void Landscape::Recalculate()
{
  for (int zz=0; zz<_landRange; zz++)
  for (int xx=0; xx<_landRange; xx++)
  {
    ObjectList &list=_objects(xx,zz);
    list.Recalculate();
  }
}

#define ShapeName(s) ( (s) ? (const char *)(s)->Name() : "<null>" )

void Landscape::RemoveObject( Object *obj )
{
  int x,z;
  SelectObjectList(x,z,obj->Position().X(),obj->Position().Z());
  ObjectList &list=_objects(x,z);
  for( int i=0; i<list.Size(); i++ ) if( list[i]==obj )
  {
    #ifdef LOG_OBJ_SHAPE
      if (IsShape(obj,LOG_OBJ_SHAPE))
      {
        LogF
        (
          "del obj %x:%s at %.2f,%.2f (%d,%d)",obj,obj->GetShape()->Name(),
          obj->Position().X(),obj->Position().Z(),x,z
        );
      }
    #endif
    list.Delete(i);
    return;
  }
  #if _ENABLE_REPORT
  #ifdef _MSC_VER
  const type_info &type = typeid(*obj);
  LogF
  (
    "Removed object %x:%s (%s) not in landscape %.2f,%.2f (%d,%d).",
    obj,ShapeName(obj->GetShape()),type.name(),
    obj->Position().X(),obj->Position().Z(),x,z
  );
  #else
  LogF
  (
    "Removed object %x:%s (%s) not in landscape %.2f,%.2f (%d,%d).",
    obj,ShapeName(obj->GetShape()),typeid(*obj).name(),
    obj->Position().X(),obj->Position().Z(),x,z
  );
  #endif
  #endif
  // try to patch it: first search in near slots
  int xMin = x-1, xMax = x+1;
  int zMin = z-1, zMax = z+1;
  saturateMax(xMin,0), saturateMin(xMax,_landRange-1);
  saturateMax(zMin,0), saturateMin(zMax,_landRange-1);
  for (int zz=zMin; zz<=zMax; zz++)
  for (int xx=xMin; xx<=xMax; xx++)
  {
    ObjectList &list=_objects(xx,zz);
    for( int i=0; i<list.Size(); i++ ) if( list[i]==obj )
    {
      list.Delete(i);
      LogF("  found in (%d,%d).",xx,zz);
      return;
    }
  }
  // try to patch it: search for the object in all slots
  for (int zz=0; zz<_landRange; zz++)
  for (int xx=0; xx<_landRange; xx++)
  {
    ObjectList &list=_objects(xx,zz);
    for( int i=0; i<list.Size(); i++ ) if( list[i]==obj )
    {
      list.Delete(i);
      LogF("  found in (%d,%d).",xx,zz);
      return;
    }
  }
}


void Landscape::MoveObject( Object *obj, const Matrix4 &transform )
{
  int xl,zl;
  SelectObjectList(xl,zl,obj->Position().X(),obj->Position().Z());
  ObjectList &list=_objects(xl,zl);

  int xm,zm;
  SelectObjectList(xm,zm,transform.Position().X(),transform.Position().Z());
  ObjectList &move=_objects(xm,zm);

  #ifdef LOG_OBJ_SHAPE
    if (IsShape(obj,LOG_OBJ_SHAPE))
    {
      LogF
      (
        "move trn obj %x:%s %.2f,%.2f (%d,%d)->%.2f,%.2f (%d,%d)",
        obj,obj->GetShape()->Name(),
        obj->Position().X(),obj->Position().Z(),xl,zl,
        transform.Position().X(),transform.Position().Z(),xm,zm
      );
    }
  #endif
  obj->SetTransform(transform);
  if( &list==&move )
  {
    // recalc bsphere
    // this should happen only in buldozer
    if( obj->Static() ) move->StaticChanged();
    return;
  }
  Verify( move.Add(obj,xm,zm)>=0 ); // <0  -> error
  for( int i=0; i<list.Size(); i++ ) if( list[i]==obj )
  {
    list.Delete(i);
    return;
  }
  #if _ENABLE_REPORT
  #ifdef _MSC_VER
  const type_info &type = typeid(*obj);
  ErrF
  (
    "Moved object %x:%s (%s) not in landscape %.2f,%.2f (%d,%d).",
    obj,ShapeName(obj->GetShape()),type.name(),
    obj->Position().X(),obj->Position().Z(),xl,zl
  );
  #else
  ErrF
  (
    "Moved object %x:%s (%s) not in landscape %.2f,%.2f (%d,%d).",
    obj,ShapeName(obj->GetShape()),typeid(*obj).name(),
    obj->Position().X(),obj->Position().Z(),xl,zl
  );
  #endif
  #endif
  // try to patch it: first search in near slots
  int xMin = xl-1, xMax = xl+1;
  int zMin = zl-1, zMax = zl+1;
  saturateMax(xMin,0), saturateMin(xMax,_landRange-1);
  saturateMax(zMin,0), saturateMin(zMax,_landRange-1);
  for (int zz=zMin; zz<=zMax; zz++)
  for (int xx=xMin; xx<=xMax; xx++)
  {
    ObjectList &list=_objects(xx,zz);
    for( int i=0; i<list.Size(); i++ ) if( list[i]==obj )
    {
      list.Delete(i);
      RptF("  found in (%d,%d).",xx,zz);
      return;
    }
  }

  // try to patch it: search for the object in all slots
  for (int zz=0; zz<_landRange; zz++)
  for (int xx=0; xx<_landRange; xx++)
  {
    ObjectList &list=_objects(xx,zz);
    for( int i=0; i<list.Size(); i++ ) if( list[i]==obj )
    {
      list.Delete(i);
      RptF("  found in (%d,%d).",xx,zz);
      return;
    }
  }
}

void Landscape::MoveObject( Object *obj, Vector3Par pos )
{
  int xl,zl;
  SelectObjectList(xl,zl,obj->Position().X(),obj->Position().Z());
  ObjectList &list=_objects(xl,zl);

  int xm,zm;
  SelectObjectList(xm,zm,pos.X(),pos.Z());
  ObjectList &move=_objects(xm,zm);
#if _ENABLE_REPORT
  //const type_info &type = typeid(*obj);
  float oldX = obj->Position().X();
  float oldZ = obj->Position().Z();
#endif

  #ifdef LOG_OBJ_SHAPE
    if (IsShape(obj,LOG_OBJ_SHAPE))
    {
      LogF
      (
        "move pos obj %x:%s %.2f,%.2f (%d,%d)->%.2f,%.2f (%d,%d)",
        obj,obj->GetShape()->Name(),
        obj->Position().X(),obj->Position().Z(),xl,zl,
        pos.X(),pos.Z(),xm,zm
      );
    }
  #endif
  obj->SetPosition(pos);
  if( &list==&move )
  {
    // recalc bsphere
    // this should happen only in buldozer
    if( obj->Static() ) move->StaticChanged();
    /*
    LogF
    (
      "Moved object %x:%s (%s) in same square %.2f,%.2f->%.2f,%.2f (%d,%d),(%d,%d).",
      obj,ShapeName(obj->GetShape()),type.name(),
      oldX,oldZ,pos.X(),pos.Z(),xl,zl,xm,zm
    );
    */
    return;
  }

  for( int i=0; i<list.Size(); i++ ) if( list[i]==obj )
  {
    Verify( move.Add(obj,xm,zm)>=0 ); // <0  -> error
    list.Delete(i);
    /*
    LogF
    (
      "Moved object %x:%s (%s) in diff square %.2f,%.2f->%.2f,%.2f (%d,%d),(%d,%d).",
      obj,ShapeName(obj->GetShape()),type.name(),
      oldX,oldZ,pos.X(),pos.Z(),xl,zl,xm,zm
    );
    */
    return;
  }

  #if _ENABLE_REPORT
  #ifdef _MSC_VER
  const type_info &type = typeid(*obj);
  LogF
  (
    "Moved object %x:%s (%s) not in landscape %.2f,%.2f (%d,%d).",
    obj,ShapeName(obj->GetShape()),type.name(),
    oldX,oldZ,xl,zl
  );
  #else
  LogF
  (
    "Moved object %x:%s (%s) not in landscape %.2f,%.2f (%d,%d).",
    obj,ShapeName(obj->GetShape()),typeid(*obj).name(),
    oldX,oldZ,xl,zl
  );
  #endif
  #endif
  // try to patch it: first search in near slots
  int xMin = xl-1, xMax = xl+1;
  int zMin = zl-1, zMax = zl+1;
  saturateMax(xMin,0), saturateMin(xMax,_landRange-1);
  saturateMax(zMin,0), saturateMin(zMax,_landRange-1);
  for (int zz=zMin; zz<=zMax; zz++)
  for (int xx=xMin; xx<=xMax; xx++)
  {
    ObjectList &list=_objects(xx,zz);
    for( int i=0; i<list.Size(); i++ ) if( list[i]==obj )
    {
      Verify( move.Add(obj,xm,zm)>=0 ); // <0  -> error
      list.Delete(i);
      LogF
      (
        "  found in (%d,%d), moving to %.2f,%.2f (%d,%d)",
        xx,zz,pos.X(),pos.Z(),xm,zm
      );
      return;
    }
  }

  // try to patch it: search for the object in all slots
  for (int zz=0; zz<_landRange; zz++)
  for (int xx=0; xx<_landRange; xx++)
  {
    ObjectList &list=_objects(xx,zz);
    for( int i=0; i<list.Size(); i++ ) if( list[i]==obj )
    {
      Verify( move.Add(obj,xm,zm)>=0 ); // <0  -> error
      list.Delete(i);
      LogF
      (
        "  found in (%d,%d), moving to %.2f,%.2f (%d,%d)",
        xx,zz,pos.X(),pos.Z(),xm,zm
      );
      return;
    }
  }
}

void Landscape::InitObjectVehicles()
{
  RefArray<Object> changed;
  for( int x=0; x<_landRange; x++ ) for( int z=0; z<_landRange; z++ )
  {
    ObjectList &list=_objects(x,z);
    for( int i=0; i<list.Size(); i++ )
    {
      Object *obj=list[i];
      if( !obj ) continue;
      if( obj->GetType()!=Primary ) continue;
      if( !obj->GetShape() ) continue;
      const char *className=obj->GetShape()->GetPropertyClass();
      if( className && *className )
      {
        //Assert( obj->RefCounter()==1 ); // only one reference to any object in this point
        changed.Add(obj);
      }
      #if 0 //!_RELEASE
        // only one reference to any object in this point
        Vehicle *vehicle=dyn_cast<Vehicle>(obj);
        if( vehicle ) Assert( vehicle->RefCounter()<=2 );
      #endif
    }
  }
  /**/
  // TODO: optimize static vehicle handling
  for( int i=0; i<changed.Size(); i++ )
  {
    //add all vehicles to vehicle list
    Object *obj=changed[i];
    Vehicle *vehicle=dyn_cast<Vehicle>(obj);
    if( vehicle )
    {
      //vehicle->SetTransform(obj->Transform());
      RemoveObject(obj);
      //GWorld->AddSlowVehicle(vehicle);
      //GWorld->AddVehicle(vehicle);
      GWorld->AddBuilding(vehicle);
      /*
      LogF
      (
        "Adding vehicle %x:%s,%s",
        vehicle,(const char *)vehicle->GetDebugName(),
        (const char *)vehicle->GetShape()->Name()
      );
      */
    }
  }
  /**/
}


Object *Landscape::AddObject
(
  Vector3Par pos, float head, LODShapeWithShadow *shape, void *user
)
{
  //Fail("Old positioning used.");
  int id=NewObjectID();
  Object *obj=NewObject(shape,id);
  _objectIds.Access(id);
  _objectIds[id]=obj;
  if( head ) obj->SetTransform( Matrix4(MRotationY, head*(H_PI/180) ) );
  Point3 nPos=pos+obj->DirectionModelToWorld(shape->BoundingCenter());
  obj->SetPosition(nPos);
  AddObject(obj);
  return obj;
}

Object *Landscape::ObjectCreate
(
  int id, const char *name, const Matrix4 &transform,
  int *x, int *z, bool avoidRecalculation
)
{
  // create object based on Class hint in LOD 0
  Ref<LODShapeWithShadow> shape=Shapes.New(name,false,true);
  Object *obj=NewObject(shape,id);

  if( obj->GetType()!=Primary && obj->GetType()!=Network )
  {
    obj->SetType(Primary);
  }
  
  // object id list cleared after load is finished
  _objectIds.Access(id);
  _objectIds[id]=obj;
  Assert( obj->ID()==id );
  // check matrix type

  Matrix4 nTransform=transform;

  #if 1
    // "repair" matrix
    // matrix should be scaled rotation
    // (S*R, where S is scale matrix and R is rotation)
    // some matrices must not be repaired
    // this holds especially for forests
    static RStringB forestString("forest");
    if
    (
      shape->GetPropertyClass()!=forestString
    )
    {
      float scale = obj->GetType() == Network ? 1.0f : nTransform.Scale();
      // if object is slope following, up should be VUp
      //if (shape->GetAndHints()&ClipLandKeep)
      if (shape->GetOrHints()&ClipLandKeep)
      {
        nTransform.SetUpAndAside
        (
          VUp,nTransform.DirectionAside()
        );
      }
      else
      {
        nTransform.SetUpAndAside
        (
          nTransform.DirectionUp(),nTransform.DirectionAside()
        );
      }
      nTransform.SetScale(scale);
      // TODO: check if matrix was wrong and report it
      // TODO: repair only in buldozer
    }
  #endif

  obj->SetTransform(nTransform);

  #if 1

  extern bool LandEditor;
  if( !LandEditor )
  {
    // auto-correct values
    Vector3 oPos=obj->Position();
    Vector3 pos = obj->PositionModelToWorld(-shape->BoundingCenter());

    float above=pos[1]-SurfaceY(pos[0],pos[2]);
    if( above>0 )
    {
      oPos[1]-=above;
      obj->SetPosition(oPos);
    }
  }
  #endif

  AddObject(obj,x,z,avoidRecalculation);
  return obj;
}

void Landscape::ObjectTypeChange( int id, const char *name )
{
  Object *obj=GetObject(id);
  Matrix4Val transform=obj->Transform();
  RemoveObject(obj); // remove object from the old position
  // obj no longer valid
  LODShapeWithShadow *shape=Shapes.New(name,false,true);
  obj=new ObjectPlain(shape,id);
  obj->SetTransform(transform);
  AddObject(obj); // insert on new position
}

class TempVehicle: public Vehicle
{
  public:
  TempVehicle( LODShapeWithShadow *shape )
  :Vehicle(shape,VehicleTypes.New("temp"),-1)
  {}
  void Simulate( float deltaT, SimulationImportance prec );
};

class SelArrow: public TempVehicle
{
  public:
  SelArrow();
};


SelArrow::SelArrow()
:TempVehicle(Shapes.New("data3d\\force.p3d",false,true))
{
}

void TempVehicle::Simulate( float deltaT, SimulationImportance prec )
{
  _delete=true; // delete immediatelly
}

void Landscape::ShowArrow( Vector3Par pos )
{
  // create a temporary object for drawing
  // use object min-max box to determine arrow position
  SelArrow *sel=new SelArrow();
  LODShape *arrow=sel->GetShape();
  //float topLevelShape=shape->Max().Y();
  // arrow will be rotated, maxZ will become minY
  float bottomLevelArrow=arrow->Max().Z();
  sel->SetOrientation(Matrix3(MRotationX,H_PI/2));
  sel->SetPosition(pos+Vector3(0,bottomLevelArrow,0));
  _arrows.Add(sel);
}

void Landscape::ShowObject( Object *obj )
{
  // create a temporary object for drawing
  _arrows.Add(obj);
}

void Landscape::SetSelection( const AutoArray<int> &sel )
{
  GameState *gstate = GWorld->GetGameState();
  GameValue list = gstate->CreateGameValue(GameArray);
  GameArrayType &array = list;
  array.Realloc(sel.Size());

  for( int index=0; index<sel.Size(); index++ )
  {
    int id=sel[index];
    Object *obj=GetObject(id);
    // create a temporary object for drawing
    // use object min-max box to determine arrow position
    if( !obj ) continue;
    LODShape *shape=obj->GetShape();
    SelArrow *sel=new SelArrow();
    LODShape *arrow=sel->GetShape();
    float topLevelShape=shape->Max().Y();
    // arrow will be rotated, maxZ will become minY
    float bottomLevelArrow=arrow->Max().Z();
    sel->SetOrientation(Matrix3(MRotationX,H_PI/2));
    sel->SetPosition(obj->Position()+Vector3(0,topLevelShape+bottomLevelArrow,0));
    _arrows.Add(sel);
    //GWorld->AddVehicle(sel);

    GameValue CreateGameObject(Object *obj);
    array.Add(CreateGameObject(obj));
  } 

  gstate->VarSet("bis_buldozer_selection", list, true);
}

void Landscape::SetSelRectangle( Vector3Par min, Vector3Par max )
{
  /*
  // create a temporary object for drawing
  // use object min-max box to determine arrow position
  Ref<LODShapeWithShadow> lShape=new LODShapeWithShadow;
  Ref<Shape> shape=new Shape;
  SelArrow *sel=new SelArrow();
  lShape->AddShape(shape,0);

  Ref<Vehicle> vehicle=new TempVehicle(lShape);

  lShape->SetAutoCenter(false);
  
  lShape->CalculateMinMax();
  lShape->OrSpecial(IsAlpha|NoShadow|OnSurface);
  shape->CalculateHints();
  lShape->CalculateHints();

  vehicle->SetPosition((min+max)*0.5);
  GWorld->AddVehicle(vehicle);
  */
}

bool Landscape::Magnetize( bool points, bool planes, bool lockY, const AutoArray<int> &sel )
{
  return false;
}

void Landscape::ObjectMove( int id, const Matrix4 &transform )
{
  Ref<Object> obj=GetObject(id);
  RemoveObject(obj); // remove object from the old position
  obj->SetTransform(transform);
  AddObject(obj); // insert on new position
}

void Landscape::ObjectDestroy( int id )
{
  Object *obj=GetObject(id);
  Assert( obj );
  if( obj ) RemoveObject(obj); // remove object from the old position
}

DEFINE_FAST_ALLOCATOR(ObjectListFull)

int ObjectListFull::CountNonStatic() const
{
  int ret=0;
  int n=Size();
  for( int i=0; i<n; i++ )
  {
    Object *obj=Get(i);
    if( obj->Static() ) continue;
    ret++;
  }
  return ret;
}

void ObjectListFull::ChangeNonStaticCount( int val )
{
  _nNonStatic+=val;
  if( CountNonStatic()!=_nNonStatic )
  {
    Fail("ChangeNonStaticCount: _nNonStatic not valid.");
  }
}

void ObjectListFull::StaticChanged() // recalculate what is neccessary
{
  // recalculate bounding sphere
  _bRadius=50;
  for( int i=0; i<Size(); i++ )
  {
    Object *obj=Get(i);
    if( !obj->Static() ) continue;
    float dist=obj->Position().Distance(_bCenter)+obj->GetRadius();
    saturateMax(_bRadius,dist);
  }
  // assume center is not changed
}

void ObjectListFull::SetBSphere( Vector3Par center, float radius )
{
  _bCenter=center;
  _bRadius=radius;
}

// default values for landscape grid x,z
void ObjectListFull::SetBSphere( int x, int z )
{
  _bCenter.Init();
  _bCenter[0]=x*LandGrid+LandGrid*0.5;
  _bCenter[2]=z*LandGrid+LandGrid*0.5;
  if (GLandscape)
  {
    _bCenter[1]=GLandscape->SurfaceY(_bCenter[0],_bCenter[2]);
  }
  else
  {
    _bCenter[1]=50; // some estimation
  }
  _bRadius=100; // default value
}

int ObjectListFull::Add(Object *object, bool avoidRecalculation)
{
  int index=base::Add(object);
  if( object->Static() )
  {
    if (!avoidRecalculation) StaticChanged();
  }
  else ChangeNonStaticCount(+1);
  return index;
}

void ObjectListFull::Delete( int index )
{
  bool isStatic=Get(index)->Static();
  base::Delete(index);
  if( isStatic ) StaticChanged();
  else ChangeNonStaticCount(-1);
}

void ObjectListFull::Clear()
{
  base::Clear();
  _nNonStatic=0;
  if( CountNonStatic()!=_nNonStatic )
  {
    Fail("Clear: _nNonStatic not valid.");
  }
}

ObjectListFull::ObjectListFull( int x, int z )
:_bRadius(1e10),
_nNonStatic(0)
{
  SetBSphere(x,z);
}

ObjectListFull::~ObjectListFull()
{
  Clear();
}

Object *Landscape::AddObject
(
  float x, float y, float z, float head, const char *name
)
{
  LODShapeWithShadow *shape=Shapes.New(name,false,true);
  return AddObject(Vector3(x,y,z),head,shape,NULL);
}

Object *Landscape::NearestObject
(
  Vector3Par pos, float limit, ObjectType type, Object *ignore
)
{
  // limit==0 means no limit, search whole landscape
  int x,z;
  int xMin,xMax,zMin,zMax;
  bool defLimit=false;
  if( limit<0 ) defLimit=true,limit=200;
  if( limit==0 )
  {
    xMin=0,xMax=_landRangeMask;
    zMin=0,zMax=_landRangeMask;
    limit=1e10;
  }
  else
  {
    xMin=toIntFloor((pos.X()-limit)*_invLandGrid);
    xMax=toIntCeil((pos.X()+limit)*_invLandGrid);
    zMin=toIntFloor((pos.Z()-limit)*_invLandGrid);
    zMax=toIntCeil((pos.Z()+limit)*_invLandGrid);
    if( xMin<0 ) xMin=0;if( xMin>_landRangeMask ) xMin=_landRangeMask;
    if( xMax<0 ) xMax=0;if( xMax>_landRangeMask ) xMax=_landRangeMask;
    if( zMin<0 ) zMin=0;if( zMin>_landRangeMask ) zMin=_landRangeMask;
    if( zMax<0 ) xMax=0;if( zMax>_landRangeMask ) zMax=_landRangeMask;
  }
  Object *ret=NULL;
  double minDist2=limit*limit;
  for( z=zMin; z<=zMax; z++ ) for( x=xMin; x<=xMax; x++ )
  {
    ObjectList &list=_objects(x,z);
    int n=list.Size();
    for( int i=0; i<n; i++ )
    {
      Object *obj=list[i];
      if( !obj || !(obj->GetType()&type) ) continue;
      double dist2=obj->Position().Distance2(pos);
      // default behaviour: consider only objects
      // that include pos in their bounding sphere
      if( defLimit && dist2>Square(obj->GetRadius()) ) continue;
      if (obj==ignore) continue;
      if( minDist2>dist2 ) minDist2=dist2,ret=obj;
    }
  }
  return ret;
}


static LinkArray<LODShapeWithShadow> VBShapes;

void RegisterVBShape(LODShapeWithShadow *shape)
{
  int free = VBShapes.Find(NULL);
  if (free>=0)
  {
    VBShapes[free]=shape;
    return;
  }
  VBShapes.Add(shape);
}
void UnregisterVBShape(LODShapeWithShadow *shape)
{
  VBShapes.Delete(shape);
}

void ReleaseVBuffers()
{
  Shapes.ReleaseAllVBuffers();
  GLandscape->ReleaseAllVBuffers();
  // note: dynamic shapes (on some vehicles) may have vertex buffers assigned to them
  // such shapes should be registered someplace
  for (int i=0; i<VBShapes.Size(); i++)
  {
    LODShape *lShape = VBShapes[i];
    if (!lShape) continue;
    for (int l=0; l<lShape->NLevels(); l++)
    {
      Shape *shape = lShape->Level(l);
      shape->ReleaseVBuffer();
    }
  }
}

void RestoreVBuffers()
{
  Shapes.OptimizeAll();
  GLandscape->CreateAllVBuffers();
  for (int i=0; i<VBShapes.Size(); i++)
  {
    LODShape *lShape = VBShapes[i];
    if (!lShape) continue;
    for (int l=0; l<lShape->NLevels(); l++)
    {
      Shape *shape = lShape->Level(l);
      shape->ConvertToVBuffer(VBSmallDiscardable);
    }
  }
}


void Landscape::SetSound( int x, int z, int index )
{
  if( this_InRange(x,z) ) _soundMap(x,z)=index+1;
}

int Landscape::GetSound( int x, int z ) const
{
  return this_InRange(x,z) ? int(_soundMap(x,z))-1 : 0;
}

template Link<TexMaterial>;
