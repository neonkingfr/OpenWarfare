#ifdef _MSC_VER
#pragma once
#endif

#ifndef _UI_MAP_HPP
#define _UI_MAP_HPP

#include "uiControls.hpp"
#include "global.hpp"
#include "engine.hpp"
#include "world.hpp"

#include "arcadeTemplate.hpp"
#include "ai.hpp"

#include "interpol.hpp"

/*!
\file
Interface file for particular displays (maps, briefing, debriefing, mission editor).
*/

//! Structure describes coordinates in map control
struct MapCoord
{
	//! x coordinate
	int x;
	//! y coordinate
	int y;
	//! constructor
	MapCoord(int xx, int yy) {x = xx; y = yy;}
	//! constructor without initialization
	MapCoord() {} // no init
};

//! Type of editable object in map
enum SignType
{
	signNone,
	signUnit,
	signVehicle,
	signStatic,
	signCheckpoint,
	signSeekAndDestroy,
	signArcadeUnit,
	signArcadeWaypoint,
	signArcadeSensor,
	signArcadeMarker,
};

//! Description of editable object in map
struct SignInfo
{
	//! type of object
	enum SignType _type;
	//! concrete unit
	OLink<AIUnit> _unit;
	//! concrete entity
	TargetId _id;
	//! position
	Point3 _pos;
	//! index of AI Group
	int _indexGroup;
	//! index of object (inside group or total)
	int _index;
};

//! Briefing notepad control
class Notepad : public ControlObjectContainer
{
protected:
	//! paper (active area) selection
	AnimationSection _paper;
	//@{
	//! paper texture
	Ref<Texture> _paper1;
	Ref<Texture> _paper2;
	Ref<Texture> _paper3;
	Ref<Texture> _paper4;
	Ref<Texture> _paper5;
	Ref<Texture> _paper6;
	Ref<Texture> _paper7;
	//@}

	//! briefing HTML control
	CHTML *_briefing;
public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	Notepad(ControlsContainer *parent, int idc, const ParamEntry &cls);
	void SetPosition(Vector3Par pos);
	void Animate(int level);
	void Deanimate(int level);
	//! attach briefing to notepad
	void SetBriefing(CHTML *briefing) {_briefing = briefing;}
};

//! Compass control
class Compass : public ControlObjectWithZoom
{
protected:
	//! pointer selection
	AnimationRotation _pointer;
	//! cover selection
	AnimationRotation _cover;

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	Compass(ControlsContainer *parent, int idc, const ParamEntry &cls);
	void Animate(int level);
	void Deanimate(int level);
	//! calculates center of rotation
	Vector3 Center() const;
};

//! Watch control
class Watch : public ControlObjectWithZoom
{
protected:
	//@{
	//! date selection
	AnimationUV _date1;
	AnimationUV _date2;
	AnimationUV _day;
	//@}

	//@{
	//! hand selection
	AnimationRotation _hour;
	AnimationRotation _minute;
	AnimationRotation _second;
	//@}

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	Watch(ControlsContainer *parent, int idc, const ParamEntry &cls);

	void Animate(int level);
	void Deanimate(int level);
};

//! Describes map object type
struct MapTypeInfo
{
	//! picture
	Ref<Texture> icon;
	//! color
	PackedColor color;
	//! relative size
	float size;

	//! reads description from resource
	void Load(const ParamEntry &cls);
};

//! Describes single animation state of map
struct MapAnimationPhase
{
	//! time of state
	UITime time;
	//! scale of map
	float scale;
	//! map center position in world coordinates
	Vector3 pos;
};
TypeIsSimple(MapAnimationPhase);

#include "engine.hpp"

//! Generic map control
class CStaticMap : public CStatic
{
public:
	//@{
	//! position (top left point) of "window" on whole map
	float _mapX;
	float _mapY;
	//@}

	//! mouse position in world coordinates
	Vector3 _mouseWorld;
	//! default map center position in world coordinates
	/*!
		Defined in config for every island.
		Used for map center when no player is displayed.
	*/
	Vector3 _defaultCenter;

	//! map object thats may becomes selected
	struct SignInfo _infoClickCandidate;
	//! selected map object
	struct SignInfo _infoClick;
	//! map object map is over
	struct SignInfo _infoMove;

	//@{
	//! current map scale
	float _scaleX;
	float _scaleY;
	float _invScaleX;
	float _invScaleY;
	//@}
	//! minimal map scale
	float _scaleMin;
	//! maximal map scale
	float _scaleMax;
	//! default (initial) map scale
	float _scaleDefault;

//	bool _moveOnEdges;
	
	//! position of map control center
	/*!
		May not be in real center of control, for example if notepad hides some area of map.
	*/
	Point2DFloat _center;

	//@{
	//! precalculates for increasing drawing speed
	float _wScreen;
	float _hScreen;
	Rect2DPixel _clipRect;
	int _mipmapLevel;
	//@}

	//@{
	//! animation implementation
	UITime _animationStart;
	UITime _animationLast;
	AutoArray<MapAnimationPhase> _animation;
	APtr<Matrix4> _animMatrices;
	APtr<float> _animTimes;
	SRef<M4Function> _interpolator;
	//@}
	
	//@{
	//! moving implementation
	UITime _moveStart;
	UITime _moveLast;
	unsigned _moveKey;
	unsigned _mouseKey;
	//@}

	//! map is moving
	bool _moving;
	//! some object is dragging
	bool _dragging;
	//! some area is selecting
	bool _selecting;
	
#if _ENABLE_CHEATS
	//@{
	//! debug show mode
	bool _show;
	bool _showCost;
	//@}
#endif
	//! show IDs of objects
	bool _showIds;
	//! show Grayscale / Textures
	bool _showScale;

	//! player's punctuation picture
	Ref<Texture> _iconPlayer;
	//! selected unit's punctuation picture
	Ref<Texture> _iconSelect;
	//! camera effect picture
	Ref<Texture> _iconCamera;
	//! trigger picture
	Ref<Texture> _iconSensor;
	//! color of friendly units
	PackedColor _colorFriendly;
	//! color of enemy units
	PackedColor _colorEnemy;
	//! color of civilians
	PackedColor _colorCivilian;
	//! color of neutral units
	PackedColor _colorNeutral;
	//! color of unknown units
	PackedColor _colorUnknown;
	//! color of player unit's punctuation
	PackedColor _colorMe;
	//! color of playable unit's punctuation
	PackedColor _colorPlayable;
	//! color of selected unit's punctuation
	PackedColor _colorSelect;
	//! trigger color
	PackedColor _colorSensor;
	//! color of line for dragged object (in group or synchronization mode)
	PackedColor _colorDragging;

	//! color of area with enemy exposure
	PackedColor _colorExposureEnemy;
	//! color of area with unknown exposure
	PackedColor _colorExposureUnknown;
	//! color of roads
	PackedColor _colorRoads;
	//! color of grid texts
	PackedColor _colorGrid;
	//! color of grid lines
	PackedColor _colorGridMap;
	//! not used
	PackedColor _colorCheckpoints;
	//! color of camera effect picture
	PackedColor _colorCamera;
	//! not used
	PackedColor _colorMissions;
	//! color of waypoint lines
	PackedColor _colorActiveMission;
	//! color of strategic path for camera unit
	PackedColor _colorPath;
	//! color of label for object mouse is over
	PackedColor _colorInfoMove;
	//! color of group lines
	PackedColor _colorGroups;
	//! color of group lines for active group
	PackedColor _colorActiveGroup;
	//! color of synchronization lines
	PackedColor _colorSync;
	//! background color of label
	PackedColor _colorLabelBackground;
	
	//@{
	//! mouse position in world coordinates in special cases
	Vector3 _special;
	Vector3 _lastPos;
	//@}
//	DrawCoord _dragOffset;

	////////////////////////////////////
	//                                //
	// NEW PROPERTIES - from resource //
	//                                //
	////////////////////////////////////
	
	//@{
	//! color of sea
	PackedColor _colorSeaPacked;
	Color _colorSea;
	//@}
	//! color of forest
	PackedColor _colorForest;

	//! color of countlines
	PackedColor _colorCountlines;
	//! color of countlines in sea
	PackedColor _colorCountlinesWater;
	//! color of borders of forest
	PackedColor _colorForestBorder;
	//! color of geographic names (towns, villages, mounts etc.)
	PackedColor _colorNames;

	//! color modifier for inactive objects
	PackedColor _colorInactive;

	//! font used for labels
	Ref<Font> _fontLabel;
	//! font size
	float _sizeLabel;
	//! font used for grid texts
	Ref<Font> _fontGrid;
	//! font size
	float _sizeGrid;
	//! font used for id of selected units
	Ref<Font> _fontUnits;
	//! font size
	float _sizeUnits;
	//! font used geographic names
	Ref<Font> _fontNames;
	//! font size
	float _sizeNames;

	//@{
	//! map object type properties
	MapTypeInfo _infoTree;
	MapTypeInfo _infoSmallTree;
	MapTypeInfo _infoBush;
	MapTypeInfo _infoChurch;
	MapTypeInfo _infoChapel;
	MapTypeInfo _infoCross;
	MapTypeInfo _infoRock;
	MapTypeInfo _infoBunker;
	MapTypeInfo _infoFortress;
	MapTypeInfo _infoFountain;
	MapTypeInfo _infoViewTower;
	MapTypeInfo _infoLighthouse;
	MapTypeInfo _infoQuay;
	MapTypeInfo _infoFuelstation;
	MapTypeInfo _infoHospital;
	MapTypeInfo _infoBusStop;

	MapTypeInfo _infoWaypoint;
	MapTypeInfo _infoWaypointCompleted;
	//@}

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
		\param scaleMin minimal map scale
		\param scaleMax maximal map scale
		\param scaleDefault default map scale
	*/
	CStaticMap
	(
		ControlsContainer *parent, int idc, const ParamEntry &cls,
		float scaleMin, float scaleMax, float scaleDefault
	);

	void OnRButtonDown(float x, float y);
	void OnRButtonUp(float x, float y);
	void OnMouseMove(float x, float y, bool active = true);
	void OnMouseHold(float x, float y, bool active = true);
	void OnMouseZChanged(float dz);
	virtual bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	virtual bool OnKeyUp(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	//! processing of cheats (do nothing in SuperRelease)
	virtual void ProcessCheats();

	virtual void OnDraw( float alpha );

	virtual void DrawExt( float alpha ) {}

	//! initialization
	void Reset();

	//! sets actual scale
	void SetScale(float scale);
	//! returns actual scale
	float GetScale() const {return _scaleX;}

	//! sets visible area of map (calculates _center)
	void SetVisibleRect(float x, float y, float w, float h);	// in screen coordinates
	//! center map on given position
	void Center(Vector3Val pt);
	//! center map (by player unit)
	virtual void Center();
	//! returns position of map center in world coordinates
	Vector3 GetCenter();

	//! clear (interrupt) current map animation
	void ClearAnimation();
	//! adds animation phase to animation
	void AddAnimationPhase(float dt, float scale, Vector3Par pos);
	//! finish creation of animation
	void CreateInterpolator();
	//! Check if some animation is running
	bool HasAnimation() const {return _animation.Size() > 0;}

	//! translates world coordinates to screen coordinates
	Point2DFloat WorldToScreen(Vector3Val pt);
	//! translates screen coordinates to world coordinates
	Point3 ScreenToWorld(Point2DFloat ptMap);

	//@{
	//! scroll (move) map window by <dif>
	void ScrollX(float dif);
	void ScrollY(float dif);
	//@}

	//! Is showing of objects' IDs enabled
	bool IsShowingIds() const {return _showIds;}
	//! Show / hide objects' IDs
	void ShowIds(bool show = true) {_showIds = show;}

	//! Is showing greyscale ore textures
	bool IsShowingScale() const {return _showScale;}
	//! Show grayscale / textures
	void ShowScale(bool show = true) {_showScale = show;}

// implementation
protected:
	//! precalculates constans to optimize some calculations
	void Precalculate();
	//! check if x map coordinate is valid (and saturate it)
	void SaturateX(float &x);
	//! check if y map coordinate is valid (and saturate it)
	void SaturateY(float &y);

	//! draw map sign
	/*!
		\param texture texture of picture
		\param color color of picture
		\param pos position in world coordinates
		\param w width of sign (in pixels for resolution 640 x 480)
		\param h height of sign (in pixels for resolution 640 x 480)
		\param azimut rotation of sign
		\param text additional text (drawn on the right side of sign)
	*/
	bool DrawSign(Texture *texture, PackedColor color,
		Vector3Val pos, float w, float h, float azimut, RString text = RString());
	//! draw map sign
	/*!
		\param texture texture of picture
		\param color color of picture
		\param posMap position in map coordinates
		\param w width of sign (in pixels for resolution 640 x 480)
		\param h height of sign (in pixels for resolution 640 x 480)
		\param azimut rotation of sign
		\param text additional text (drawn on the right side of sign)
	*/
	bool DrawSign(Texture *texture, PackedColor color,
		DrawCoord posMap, float w, float h, float azimut, RString text = RString());
	//! draw map sign
	/*!
		\param info description of map object type
		\param pos position in world coordinates
	*/
	bool DrawSign(MapTypeInfo &info, Vector3Val pos);
	//! draw label
	/*!
		\param info editable map object info
		\param color text color
	*/
	virtual void DrawLabel(struct SignInfo &info, PackedColor color) {}
	//! draw map background (all informations from Landscape - sea, countlines, forests, roads, objects etc.)
	void DrawBackground();
	//! draw map legend
#if _ENABLE_VBS
	void DrawLegend();
#endif
	//! draw town or village name
	void DrawName(const ParamEntry &cls);
	//! draw mount dimension
	void DrawMount(Vector3Par pos);
	//! draw real landscape texture in given square
	/*!
		\param x x position on screen
		\param y y position on screen
		\param xStep width of square on screen
		\param yStep height of square on screen
		\param i x position in landscape (in LandGrid coordinates)
		\param j z position in landscape (in LandGrid coordinates)
		\param iStep width in landscape (in LandGrid coordinates)
		\param jStep height in landscape (in LandGrid coordinates)

	*/
	void DrawField
	(
		float x, float y, float xStep, float yStep,
		int i, int j, int iStep, int jStep
	);
	//! draw height scale in given square
	/*!
		\param x x position on screen
		\param y y position on screen
		\param xStep width of square on screen
		\param yStep height of square on screen
		\param i x position in landscape (in LandGrid coordinates)
		\param j z position in landscape (in LandGrid coordinates)
		\param iStep width in landscape (in LandGrid coordinates)
		\param jStep height in landscape (in LandGrid coordinates)

	*/
	void DrawScale
	(
		float x, float y, float xStep, float yStep,
		int i, int j, int iStep, int jStep
	);
	//! draw sea in given square
	/*!
		\param x x position on screen
		\param y y position on screen
		\param xStep width of square on screen
		\param yStep height of square on screen
		\param i x position in landscape (in LandGrid coordinates)
		\param j z position in landscape (in LandGrid coordinates)
		\param iStep width in landscape (in LandGrid coordinates)
		\param jStep height in landscape (in LandGrid coordinates)

	*/
	void DrawSea
	(
		float x, float y, float xStep, float yStep,
		int i, int j, int iStep, int jStep
	);
	//! draw abstract isometric lines
	/*!
		\param pt array of 4 bounding points of square
		\param height array of 4 values in bounding points
		\param step step of value for which lines are drawn
		\param minLevel minimal value for which line is drawn
		\param maxLevel maximal value for which line is drawn
		\param color lines color

	*/
	void DrawLines
	(
		DrawCoord *pt, float *height,
		float step, float minLevel, float maxLevel, PackedColor color
	);
	//! draw countlines in given square
	/*!
		\param x x position on screen
		\param y y position on screen
		\param xStep width of square on screen
		\param yStep height of square on screen
		\param i x position in landscape (in LandGrid coordinates)
		\param j z position in landscape (in LandGrid coordinates)
		\param iStep width in landscape (in LandGrid coordinates)
		\param jStep height in landscape (in LandGrid coordinates)
		\param step step of height for which lines are drawn
		\param maxLevel maximal absolute value for which line is drawn
	*/
	void DrawCountlines
	(
		float x, float y, float xStep, float yStep,
		int i, int j, int iStep, int jStep,
		float step, float maxLevel
	);
	//! draw forests in given square
	/*!
		Drawing is performed by single landscape squares.
		\param i x position in landscape (in LandGrid coordinates)
		\param j z position in landscape (in LandGrid coordinates)
		\param x x position on screen
		\param y y position on screen
		\param w width of square on screen
		\param h height of square on screen
	*/
	void DrawForests(int i, int j, float x, float y, float w, float h);
	//! draw objects in given square
	/*!
		Drawing is performed by single landscape squares.
		\param i x position in landscape (in LandGrid coordinates)
		\param j z position in landscape (in LandGrid coordinates)
	*/
	void DrawObjects(int i, int j);
	//! draw forest borders in given square
	/*!
		Drawing is performed by single landscape squares.
		\param i x position in landscape (in LandGrid coordinates)
		\param j z position in landscape (in LandGrid coordinates)
		\param x x position on screen
		\param y y position on screen
		\param w width of square on screen
		\param h height of square on screen
	*/
	void DrawForestBorders(int i, int j, float x, float y, float w, float h);
	//! draw roads in given square
	/*!
		Drawing is performed by single landscape squares.
		\param i x position in landscape (in LandGrid coordinates)
		\param j z position in landscape (in LandGrid coordinates)
	*/
	void DrawRoads(int i, int j);
	//! draw map grid
	void DrawGrid();

	//! draw ellipse in map
	/*!
		\param position center of ellipse in world coordinates
		\param a axis a of ellipse (in meters)
		\param b axis b of ellipse (in meters)
		\param angle rotation
		\param color border color
	*/
	void DrawEllipse(Vector3Val position, float a, float b, float angle, PackedColor color);
	//! draw rectangle in map
	/*!
		\param position center of ellipse in world coordinates
		\param a side a of rectangle (in meters)
		\param b side b of rectangle (in meters)
		\param angle rotation
		\param color border color
	*/
	void DrawRectangle(Vector3Val position, float a, float b, float angle, PackedColor color);
	//! draw filled ellipse in map
	/*!
		\param position center of ellipse in world coordinates
		\param a axis a of ellipse (in meters)
		\param b axis b of ellipse (in meters)
		\param angle rotation
		\param color fill color
		\param texture fill texture
	*/
	void FillEllipse(Vector3Val position, float a, float b, float angle, PackedColor color, Texture *texture);
	//! draw filled rectangle in map
	/*!
		\param position center of ellipse in world coordinates
		\param a side a of rectangle (in meters)
		\param b side b of rectangle (in meters)
		\param angle rotation
		\param color fill color
		\param texture fill texture
	*/
	void FillRectangle(Vector3Val position, float a, float b, float angle, PackedColor color, Texture *texture);

	//! find editable map object on given screen coordinates (<x>, <y>)
	virtual SignInfo FindSign(float x, float y);

	//! calculate color for inactive item from base color
	PackedColor InactiveColor(PackedColor color);
};

//! weather state (used for selection of icon for weather in mission editor)
enum WeatherState
{
	WSUndefined,
	WSClear,
	WSCloudly,
	WSOvercast,
	WSRainy,
	WSStormy
};

//! translates weather state into icon type
WeatherState GetWeatherState(float overcast);

//! base class for 2D notebook display (currently used only for mission editor)
class DisplayNotebook : public Display
{
protected:
/*
	Ref<Texture> _glassA;
	Ref<Texture> _glassB;
	Ref<Texture> _glassC;
	Ref<Texture> _glassD;
	Ref<Texture> _glassE;
*/

	//! displayed weather
	WeatherState _lastWeather;

public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayNotebook(ControlsContainer *parent);

	//! returns current weather state
	virtual WeatherState GetWeather() = NULL;
	//! returns current game time
	virtual Clock GetTime() = NULL;
	//! returns current player position
	virtual bool GetPosition(Point3 &pos) = NULL;

	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	void OnDraw(EntityAI *vehicle, float alpha);
};

//! types of cursor used in map
enum CursorType
{
	CursorArrow,
	CursorTrack,
	CursorMove,
	CursorScroll
};

#if _ENABLE_EDITOR

//! Abstract mission editor display
class DisplayMapEditor : public DisplayNotebook
{
protected:
	//! current cursor type
	CursorType _cursor;
public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayMapEditor(ControlsContainer *parent);

	//! returns map control
	virtual CStaticMap *GetMap() = NULL;
	void OnSimulate(EntityAI *vehicle);
	virtual bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	virtual bool OnKeyUp(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	void OnDraw(EntityAI *vehicle, float alpha);
};

#endif // #if _ENABLE_EDITOR

// Main map (ingame)

//! Info about command passed from map
struct CommandInfo
{
	//! group
	OLink<AIGroup> grp;
	//! command id (in group)
	int id;
	//! command state
	CommandState state;
	//! subgroup executing command
	OLink<AISubgroup> subgrp;
	//! command target position
	Vector3 position;

	//! serialization
	LSError Serialize(ParamArchive &ar);
};
TypeIsGeneric(CommandInfo);

class DisplayMap;

/*!
\patch 1.01 Date 06/07/2001 by Ondra
- Fixed: friendly units info in map (veteran mode)
- was MapSubgroupInfo before
- information was stored about subgroups, not units
*/

//! Info about reported unit
struct MapUnitInfo
{
	//! reported unit
	OLink<AIUnit> unit;
	//! when report was done
	float time;	// report status time
	//@{
	//! reported position
	int x;
	int z;
	//@}

	//! serialization
	LSError Serialize(ParamArchive &ar);
};
TypeContainsOLink(MapUnitInfo);

//! Info about reported enemy
struct MapEnemyInfo
{
	//! enemy id
	TargetId id;
	//! enemy type
	const VehicleType *type;
	//! when report was done
	float time;	// report status time
	//@{
	//! reported position
	int x;
	int z;
	//@}

	//! serialization
	LSError Serialize(ParamArchive &ar);
};
TypeContainsOLink(MapEnemyInfo);

//! Encapsulates all infos in main map (units, enemies, commands)
struct MainMapInfo : public SerializeClass
{
	//! subgroups
	AutoArray<MapUnitInfo> unitInfo;
	//! enemies
	AutoArray<MapEnemyInfo> enemyInfo;
	//! commands
	AutoArray<CommandInfo> commands;

	LSError Serialize(ParamArchive &ar);
	//! remove all infos
	void Clear();
};

//! Main (ingame) map (and briefing) control
class CStaticMapMain : public CStaticMap
{
protected:
#if _ENABLE_CHEATS
	//@{
	//! debug show mode
	bool _showUnits;
	bool _showTargets;
	bool _showSensors;
	bool _showVariables;
	//@}
#endif

	//! focused marker
	int _activeMarker;

	//! map object type properties for command sign
	MapTypeInfo _infoCommand;
	
	//! lines color for active marker enforcement
	PackedColor _colorActiveMarker;
	//! square size for active marker enforcement
	float _sizeActiveMarker;

	//! picture for current position in veteran mode (in tanks and air units)
	Ref<Texture> _iconPosition;

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	CStaticMapMain(ControlsContainer *parent, int idc, const ParamEntry &cls);

	void OnLButtonDown(float x, float y);
	void OnLButtonUp(float x, float y);
	void OnLButtonClick(float x, float y);
	void OnLButtonDblClick(float x, float y);
	void OnMouseHold(float x, float y, bool active = true);
	virtual bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	virtual void ProcessCheats();
	void DrawExt(float alpha);

	//! returns parent display pointer (casted)
	DisplayMap *GetParent();
	void Center();

	//! sets active (focused) marker
	void SetActiveMarker(int marker) {_activeMarker = marker;}

	//! adds info about reported unit
	void AddUnitInfo(AIUnit *unit);
	//! adds info about reported enemy
	void AddEnemyInfo
	(
		TargetType *id, const VehicleType *type, int x, int z
	);

	LSError Serialize(ParamArchive &ar);

protected:
	//! returns player unit
	AIUnit *GetMyUnit();
	//! returns player's center
	AICenter *GetMyCenter();

	SignInfo FindSign(float x, float y);

	//! draws exposure map (from AICenter database)
	void DrawExposure();
	//! draws field costs (pathfinding) for player unit
	void DrawCost();
	//! draws all units for given center
	void DrawUnits(AICenter *center);

	//! process move command issued from map
	void IssueMove(float x, float y);
	//! process watch command issued from map
	void IssueWatch(float x, float y);
	//! process attack command issued from map
	void IssueAttack(TargetType *target);
	//! process get in command issued from map
	void IssueGetIn(TargetType *target);

	//! draws command infos
	void DrawCommands();
	//! draws info about friendly units and enemies in each square
	void DrawInfo();
	//! draws all markers
	void DrawMarkers();
	//! draws all sensors
	void DrawSensors();
	//! draws waypoints for given group
	void DrawWaypoints(AICenter *center, AIGroup *myGroup);
	//! calculates real position of waypoint
	DrawCoord GetWaypointPosition(const ArcadeWaypointInfo &wInfo);

	void DrawLabel(struct SignInfo &info, PackedColor color);

	//! draws exposure by enemy units in given square
	/*!
		\param center player's center
		\param x x position on screen
		\param y y position on screen
		\param xStep width of square on screen
		\param yStep height of square on screen
		\param i x position in landscape (in LandGrid coordinates)
		\param j z position in landscape (in LandGrid coordinates)
		\param iStep width in landscape (in LandGrid coordinates)
		\param jStep height in landscape (in LandGrid coordinates)

	*/
	void DrawExposureEnemy
	(
		AICenter *center,
		float x, float y, float xStep, float yStep,
		int i, int j, int iStep, int jStep
	);
	//! draws exposure by unknown units in given square
	/*!
		\param center player's center
		\param x x position on screen
		\param y y position on screen
		\param xStep width of square on screen
		\param yStep height of square on screen
		\param i x position in landscape (in LandGrid coordinates)
		\param j z position in landscape (in LandGrid coordinates)
		\param iStep width in landscape (in LandGrid coordinates)
		\param jStep height in landscape (in LandGrid coordinates)

	*/
	void DrawExposureUnknown
	(
		AICenter *center,
		float x, float y, float xStep, float yStep,
		int i, int j, int iStep, int jStep
	);
	//! draws field costs (pathfinding) in given square
	void DrawCost
	/*!
		\param subgroup player's subgroup
		\param x x position on screen
		\param y y position on screen
		\param xStep width of square on screen
		\param yStep height of square on screen
		\param i x position in landscape (in LandGrid coordinates)
		\param j z position in landscape (in LandGrid coordinates)
		\param iStep width in landscape (in LandGrid coordinates)
		\param jStep height in landscape (in LandGrid coordinates)

	*/
	(
		AISubgroup *subgroup, float invCost,
		float x, float y, float xStep, float yStep,
		int i, int j, int iStep, int jStep
	);
	//! draws info about friendly units and enemies in given square
	/*!
		\param x x position on screen
		\param y y position on screen
		\param xStep width of square on screen
		\param yStep height of square on screen
		\param i x position in landscape (in LandGrid coordinates)
		\param j z position in landscape (in LandGrid coordinates)
	*/
	void DrawInfo
	(
		float x, float y, float xStep, float yStep, int i, int j
	);

	//! find nearest unit
	/*!
		\param center center of searched unit
		\param pt position
		\param retrieved info nearest units description retrieved 
		\param minDist limit of distance, retrieved distance of nearest unit
	*/
	void FindUnit(AICenter *center, Vector3Par pt, SignInfo &info, float &minDist);
	//! find nearest waypoint
	/*!
		\param myCenter center of searched unit
		\param myGroup group of searched unit
		\param pt position
		\param retrieved info nearest units description retrieved 
		\param minDist limit of distance, retrieved distance of nearest unit
	*/
	void FindWaypoint
	(
		AICenter *myCenter, AIGroup *myGroup,
		Vector3Par pt, SignInfo &info, float &minDist
	);
};

//! NOT USED CURRENTLY
class CUnitsSelector : public CCheckBoxes
{
public:
	CUnitsSelector(ControlsContainer *parent, int idc, const ParamEntry &cls)
		: CCheckBoxes(parent, idc, cls) {}

	virtual PackedBoolArray GetArray() const;
};

//! NOT USED CURRENTLY
class CWarrant : public Control
{
protected:
	Ref<Font> _font;
	float _size;
	PackedColor _color;

public:
	CWarrant(ControlsContainer *parent, int idc, const ParamEntry &cls);
	void OnDraw(float alpha);
};

//! Display for insertion of user marker in main map (or briefing)
class DisplayInsertMarker : public Display
{
public:
	//! written marker text
	RString _text;
	//! selected marker picture
	int _picture;
	//! selected marker color
	int _color;
	//! position of display
	float _x, _y;
	//! clipping rectangle for display
	float _cx, _cy, _cw, _ch;
protected:
	//@{
	//! partial exit code
	int _exitDIK;
	int _exitVK;
	//@}

public:
	//! constructor
	/*!
		\param parent parent display
		\param x position of display
		\param y position of display
		\param cx clipping rectangle for display
		\param cy clipping rectangle for display
		\param cw clipping rectangle for display
		\param ch clipping rectangle for display
		\param enableSimulation enable game simulation
		- true if called from main map
		- false if called from briefing
	*/
	DisplayInsertMarker(ControlsContainer *parent, float x, float y, float cx, float cy, float cw, float ch, bool enableSimulation);
	//! destructor
	~DisplayInsertMarker();

	// Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	void OnButtonClicked(int idc);
	void OnSimulate(EntityAI *vehicle);
	void Destroy();

protected:
	//! reads parameters of selected picture from config
	void UpdatePicture();
	//! select previous picture
	void PrevPicture();
	//! select next picture
	void NextPicture();
	//! select previous color
	void PrevColor();
	//! select next color
	void NextColor();
};

#if _ENABLE_DATADISC
#define WEAPON_SLOTS			5
#define MAGAZINE_SLOTS		14
#else
//! number of weapon slots (primary weapon, secondary weapon and two special items)
#define WEAPON_SLOTS			4
//! maximal number of magazine slots
#define MAGAZINE_SLOTS		10
#endif
//! total number of slots
#define TOTAL_SLOTS				WEAPON_SLOTS + MAGAZINE_SLOTS

//! Information about weapons owned by unit
struct UnitWeaponsInfo : public SerializeClass
{
	//! which unit
	OLink<AIUnit> unit;
	//! name of unit
	RString name;
	//! description of slots owned by units (bit fields)
	int weaponSlots;
	//! weapons currently in slots
	Ref<WeaponType> weapons[WEAPON_SLOTS];
	//! magazines currently in slots
	Ref<Magazine> magazines[MAGAZINE_SLOTS];

	//! constructor
	UnitWeaponsInfo() {weaponSlots = 0;}
	//! constructor
	/*!
		Reads weapons information from given unit.
	*/
	UnitWeaponsInfo(AIUnit *u);

	//! removes weapon from slots
	void RemoveWeapon(const WeaponType *weapon)
	{
		for (int i=0; i<WEAPON_SLOTS; i++)
			if (weapons[i] == weapon) weapons[i] = NULL;
	}
	//! removes magazine from slots
	void RemoveMagazine(const Magazine *magazine)
	{
		for (int i=0; i<MAGAZINE_SLOTS; i++)
			if (magazines[i] == magazine) magazines[i] = NULL;
	}

	//! returns if magazine can be used for some owned weapon
	bool IsMagazineUsable(const MagazineType *type);

	LSError Serialize(ParamArchive &ar);
};
TypeContainsOLink(UnitWeaponsInfo);

//! Information about weapons owned by group
/*!
	Contains weapons of all units in group and pool.
*/
struct WeaponsInfo : public SerializeClass
{
	//! descriptions of weapons for each unit in group
	AutoArray<UnitWeaponsInfo> _weapons;
	//! weapons in pool (availiable to group)
	RefArray<WeaponType> _weaponsPool;
	//! magazines in pool (availiable to group)
	RefArray<Magazine> _magazinesPool;

	//! loads info from file
	bool Load(RString filename);
	//! saves info into file
	bool Save(RString filename);
	//! loads info from ParamClass
	bool Import(const ParamEntry &superclass);
	//! changes weapons of real units by information in this info
	void Apply();
	//! find magazine in pool and remove them
	Ref<Magazine> RemovePoolMagazine(const MagazineType *type);
	
	LSError Serialize(ParamArchive &ar);
};

//! States of mission objective
enum ObjectiveStatus
{
	OSActive,
	OSDone,
	OSFailed,
	OSHidden
};

//! Base (abstract) map display
class DisplayMap : public Display
{
protected:
	//! briefing control
	CHTML *_briefing;
	//! mission name control
	CStatic *_name;
	//@{
	//! bookmark control
	CActiveText *_bookmark1;
	CActiveText *_bookmark2;
	CActiveText *_bookmark3;
	CActiveText *_bookmark4;
	//@}
	//@{
	//! radio message control
	CActiveText *_radioAlpha;
	CActiveText *_radioBravo;
	CActiveText *_radioCharlie;
	CActiveText *_radioDelta;
	CActiveText *_radioEcho;
	CActiveText *_radioFoxtrot;
	CActiveText *_radioGolf;
	CActiveText *_radioHotel;
	CActiveText *_radioIndia;
	CActiveText *_radioJuliet;
	//@}
	//! gps text control
	CStatic *_gpsCtrl;
	//! map control
	CStaticMapMain *_map;

	//! compass object
	Ref<Compass> _compass;
	//! watch object
	Ref<Watch> _watch;
	//! walkie talkie (radio) object
	Ref<ControlObjectContainer> _walkieTalkie;
	//! briefing notepad object
	Ref<Notepad> _notepad;
	//! warrant object (not used currently)
	Ref<ControlObjectContainer> _warrant;
	//! GPS object
	Ref<ControlObjectContainer> _gps;

//	bool _debriefing;
	//! allow select weapons
	bool _selectWeapons;

	//! currently selected unit on gear page
	int _currentUnit;
	//! currently activated slot on gear page
	int _currentSlot;
	//! information about weapons
	WeaponsInfo _weaponsInfo;

	//! currently displayed mouse cursor
	CursorType _cursor;
public:
	//! constructor
	/*!
		\param parent parent display
		\param resource name of dialog resource template
	*/
	DisplayMap(ControlsContainer *parent, RString resource);
	//! destructor
	~DisplayMap();

	//! returns map control
	CStaticMapMain *GetMap() {return _map;}

	//! returns if map is shown
	bool IsShownMap() const;
	//! returns if compass is shown
	bool IsShownCompass() const;
	//! returns if watch is shown
	bool IsShownWatch() const;
	//! returns if walkie talkie is shown
	bool IsShownWalkieTalkie() const;
	//! returns if briafing notepad is shown
	bool IsShownNotepad() const;
	//! returns if warrant is shown
	bool IsShownWarrant() const;
	//! returns if GPS is shown
	bool IsShownGPS() const;
	//! enables / disables displaying of map
	void ShowMap(bool show = true);
	//! enables / disables displaying of compass
	void ShowCompass(bool show = true);
	//! enables / disables displaying of watch
	void ShowWatch(bool show = true);
	//! enables / disables displaying of walkie talkie
	void ShowWalkieTalkie(bool show = true);
	//! enables / disables displaying of briefing notapad
	void ShowNotepad(bool show = true);
	//! enables / disables displaying of warrant
	void ShowWarrant(bool show = true);
	//! enables / disables displaying of GPS
	void ShowGPS(bool show = true);

	//! ingame (main) map
	virtual bool IsInGame() const {return false;}

	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	ControlObject *OnCreateObject(int type, int idc, const ParamEntry &cls);

	void OnChildDestroyed(int idd, int exit);

	bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	bool OnKeyUp(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	void OnDraw(EntityAI *vehicle, float alpha);
	void OnSimulate(EntityAI *vehicle);

	void OnButtonClicked(int idc);
	void OnHTMLLink(int idc, RString link);

	void ResetHUD();

	//! recalculates rectangle of map not overlapped by notepad
	void AdjustMapVisibleRect();

	//! updates mission plan section of briefing
	void UpdatePlan();

	//! switch section in briefing
	virtual void SwitchBriefingSection(RString section);

	//! loads weapons info from file
	void LoadWeapons(RString filename) {_weaponsInfo.Load(filename);}
	//! saves weapons info into file
	void SaveWeapons(RString filename) {_weaponsInfo.Save(filename);}

	//! updates gear page of briefing by real situation
	void UpdateWeaponsInBriefing();

	LSError Serialize(ParamArchive &ar);

protected:
	//! loads parameters (f.e. placement of items) from user profile
	void LoadParams();
	//! saves parameters (f.e. placement of items) into user profile
	void SaveParams();
	//! serialization of parameters (f.e. placement of items)
	LSError SerializeParams(ParamArchive &ar);

	//! creates gear page of briefing
	void CreateWeaponsPage();
	//! creates page for weapons selection for given slot
	void CreateWeaponsPoolPage(int slot);
	//! updates units page of briefing by real situation
	void UpdateUnitsInBriefing();
//	void UpdateDebriefing();

	//! removes all magazines unusable by owned weapons in given weapon info
	void RemoveUnusableMagazines(UnitWeaponsInfo &info);
	//! adds magazines usable by given weapon into weapon info (and remove them by pool)
	void AddUsableMagazines(UnitWeaponsInfo &info, const WeaponType *weapon, int from, int to);

	//! adds magazines usable by given hand gun into weapon info (and remove them by pool)
	void AddUsableHandGunMagazines(UnitWeaponsInfo &info, const WeaponType *weapon, int from, int to);

	//! updates texts on walkie talkie by real situation
	void SetRadioText();
	//! initialization of display
	void Init();

//	void SwitchDebriefing(bool debriefing = true);

friend void SetCommandState(int id, CommandState state, AISubgroup *subgrp);
};

//! Main (ingame) map display
class DisplayMainMap : public DisplayMap
{
public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayMainMap(ControlsContainer *parent);
	void DestroyHUD(int exit);
	virtual bool IsInGame() const {return true;}
};

//! Briefing display
class DisplayGetReady : public DisplayMap
{
protected:
	bool _soundPlanPlayed;
	bool _soundNotesPlayed;
	bool _soundGearPlayed;
	bool _soundGroupPlayed;

	Ref<AbstractWave> _sound;

public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayGetReady(ControlsContainer *parent);
	//! constructor
	/*!
		\param parent parent display
		\param resource name of dialog resource template
	*/
	DisplayGetReady(ControlsContainer *parent, RString resource);
	~DisplayGetReady();
	void OnButtonClicked(int idc);
	void Destroy();
	virtual void SwitchBriefingSection(RString section);

protected:
	//! Play 2D sound (as reaction to switch sections in briefing)
	/*!
	\param var variable with sound name
	*/
	void PlaySound(RString name);
};

/*
class DisplayDebriefing : public DisplayMap
{
public:
	DisplayDebriefing(ControlsContainer *parent);
	void Destroy();
};
*/

//! Debriefing display
class DisplayDebriefing : public Display
{
protected:
	//! left page control
	CHTMLContainer *_left;
	//! right page control
	CHTMLContainer *_right;
	//! statistics page control
	CHTMLContainer *_stats;
	//! statistics of last mission
	AIStats _oldStats;
	//! NOT USED CURRENTLY
	bool _animation;
/*
	RString _oldMission;
	RString _oldDirectory;
	RString _oldSubdirectory;
*/
	bool _server;
	bool _client;

public:
	//! constructor
	/*!
		\param parent parent display
		\param animation NOT USED CURRENTLY
	*/
	DisplayDebriefing(ControlsContainer *parent, bool animation);
	void Destroy();

	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	void OnButtonClicked(int idc);
	void OnHTMLLink(int idc, RString link);
	void OnSimulate(EntityAI *vehicle);

protected:
	//! create content of debriefing pages
	void CreateDebriefing();		
};

//! Briefing display for client machine in multiplayer
class DisplayClientDebriefing : public DisplayDebriefing
{
	typedef DisplayDebriefing base;

public:
	//! constructor
	/*!
		\param parent parent display
		\param animation NOT USED CURRENTLY
	*/
	DisplayClientDebriefing(ControlsContainer *parent, bool animation);
	void OnSimulate(EntityAI *vehicle);
};

inline DisplayMap *CStaticMapMain::GetParent()
{
	Assert(dynamic_cast<DisplayMap *>(_parent));
	return static_cast<DisplayMap *>(_parent);
}

//! Multiplayer mission wizard map control
#if _ENABLE_EDITOR

class CStaticMapWizard : public CStaticMap
{
protected:
	ArcadeTemplate *_template;

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
		\param scaleMin minimal map scale
		\param scaleMax maximal map scale
		\param scaleDefault default map scale
	*/
	CStaticMapWizard
	(
		ControlsContainer *parent, int idc, const ParamEntry &cls,
		float scaleMin, float scaleMax, float scaleDefault
	);

	void OnLButtonDown(float x, float y);
	void OnLButtonUp(float x, float y);
	void OnLButtonClick(float x, float y);
	void OnMouseHold(float x, float y, bool active = true);

/*
	void OnLButtonClick(float x, float y);
	void OnLButtonDblClick(float x, float y);
	virtual bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	virtual void ProcessCheats();

	//! returns parent display pointer (casted)
	DisplayMap *GetParent();
*/
	void DrawExt(float alpha);
	
	void Center();

	//! sets actual mission (template)
	void SetTemplate(ArcadeTemplate *templ) {_template = templ;}
	//! returns actual mission (template)
	const ArcadeTemplate *GetTemplate() const {return _template;}
protected:
	SignInfo FindSign(float x, float y);

	//! draws markers
	void DrawMarkers();

	void DrawLabel(struct SignInfo &info, PackedColor color);
};

//! Second multiplayer wizard display
/*!
	Enables change position of control points.
*/
class DisplayWizardMap : public Display
{
protected:
	//! currently displayed mouse cursor
	CursorType _cursor;

public:
	//! selected island
	RString _world;
	//! selected template
	RString _t;
	//! template is in bank
	bool _bank;
	//! name of new mission
	RString _name;
	//! map control
	CStaticMapWizard *_map;
	//! mission content (template)
	ArcadeTemplate _mission;
	//! multiplayer / singleplayer mission
	bool _multiplayer;

	//! constructor
	/*!
		\param parent parent display
		\param world selected island
		\param t selected template
		\param name name of new mission
		\param	multiplayer multiplayer / singleplayer mission
	*/
	DisplayWizardMap(ControlsContainer *parent, RString world, RString t, bool bank, RString name, bool multiplayer);
	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	void OnButtonClicked(int idc);
	void OnSimulate(EntityAI *vehicle);
	void OnChildDestroyed(int idd, int exit);

protected:
	//! creates mission by template
	bool CreateMission();
};
#endif

// Arcade map (mission editor)

#if _ENABLE_EDITOR

//! Modes of mission editor
enum InsertMode
{
	IMUnits,
	IMGroups,
	IMSensors,
	IMWaypoints,
	IMSynchronize,
	IMMarkers,
	IMN
};

//! Mission viewer control
class CStaticMapArcadeViewer : public CStaticMap
{
protected:
	//! actual mission content (template)
	ArcadeTemplate *_template;
	//! strategic objects
	OLinkArray<EntityAI> _objects;

	//! size of empty marker (empty marker must be shown in editor)
	float _sizeEmptyMarker;
public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
		\param scaleMin minimal map scale
		\param scaleMax maximal map scale
		\param scaleDefault default map scale
	*/
	CStaticMapArcadeViewer
	(
		ControlsContainer *parent, int idc, const ParamEntry &cls,
		float scaleMin, float scaleMax, float scaleDefault
	);
	
	//! sets actual mission (template)
	void SetTemplate(ArcadeTemplate *templ) {_template = templ;}
	//! returns actual mission (template)
	const ArcadeTemplate *GetTemplate() const {return _template;}

	virtual bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	void OnLButtonDown(float x, float y);

	void Center();
	void DrawExt(float alpha);

	//! Create / update list of strategic objects
	void CreateObjectList();

protected:
	//! returns player unit
	AIUnit *GetMyUnit();
	//! returns player's center
	AICenter *GetMyCenter();

	//! returns current editing mode
	virtual InsertMode GetMode();

	//! draws strategic objects
	void DrawObjects();
	//! find object with given id
	EntityAI *FindObject(int id);

	void DrawLabel(struct SignInfo &info, PackedColor color);
	SignInfo FindSign(float x, float y);

	//! returns if editing is possible
	virtual bool HasFullRights() {return false;}

	//! returns if given editable map object is in selection
	bool IsSelected(const SignInfo &info) const;
	//! toggle selection status of given editable map object
	void InvertSelection(const SignInfo &info);
};

//! access rights for editting of map objects
enum EditRights
{
	ERNone,
	ERGroupWP,
	ERSideWP,
	ERFull
};

//! Mission editor control
class CStaticMapArcade : public CStaticMapArcadeViewer
{
protected:
	//! access rights
	EditRights _editRights;

	//@{
	//! info about last edited group (defaults for next inserted group)
	RString _lastGroupSide;
	RString _lastGroupType;
	RString _lastGroupName;
	//@}

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
		\param rights access rights
		\param scaleMin minimal map scale
		\param scaleMax maximal map scale
		\param scaleDefault default map scale
	*/
	CStaticMapArcade
	(
		ControlsContainer *parent, int idc, const ParamEntry &cls, EditRights rights,
		float scaleMin, float scaleMax, float scaleDefault
	)
	: CStaticMapArcadeViewer(parent, idc, cls, scaleMin, scaleMax, scaleDefault)
	{_editRights = rights;}

	//! sets information about last edited group
	void SetLastGroup(RString side, RString type, RString name)
	{
		_lastGroupSide = side; _lastGroupType = type; _lastGroupName = name;
	}

	virtual bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	void OnLButtonDown(float x, float y);
	void OnLButtonUp(float x, float y);
	void OnLButtonClick(float x, float y);
	void OnLButtonDblClick(float x, float y);
	void OnMouseHold(float x, float y, bool active = true);

	void DrawExt(float alpha);

	virtual void ProcessCheats();

protected:
	//! delete selected editable objects
	void ClipboardDelete();
	//! copy selected editable objects to clipboard
	void ClipboardCopy();
	//! cut selected editable objects to clipboard
	void ClipboardCut();
	//! paste editable objects from clipboard (position depends on mouse cursor position)
	void ClipboardPaste();
	//! paste editable objects from clipboard (position is the same as original position)
	void ClipboardPasteAbsolute();

	InsertMode GetMode();
	//! returns if advanced (or easy) mode of editor is used
	bool IsAdvanced();
	//! returs info about last edited unit (defaults for next inserted unit)
	ArcadeUnitInfo *GetLastUnit();

	virtual bool HasFullRights() {return _editRights == ERFull;}
	//! returns if user can change waypoints of given group
	bool HasRight(int ig);
};

//! Waypoint properties display
class DisplayArcadeWaypoint : public Display
{
public:
	//@{
	//! waypoint identification
	int _indexGroup;
	int _index;
	//@}
	//! waypoint parameters
	ArcadeWaypointInfo _waypoint;

	//! whole mission template
	ArcadeTemplate *_template;

	//! if advanced (or easy) editor mode is selected
	bool _advanced;

public:
	//! constructor
	/*!
		\param parent parent display
		\param templ whole mission template (order of waypoint can changed)
		\param indexGroup waypoint identification
		\param index waypoint identification
		\param waypoint waypoint parameters
		\param advanced	if advanced (or easy) editor mode is selected
	*/
	DisplayArcadeWaypoint
	(
		ControlsContainer *parent, ArcadeTemplate *templ,
		int indexGroup, int index, ArcadeWaypointInfo &waypoint,
		bool advanced
	);
	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);

	void OnButtonClicked(int idc);
	void OnChildDestroyed(int idd, int exit);

	bool CanDestroy();
	void Destroy();
};

//! Mission editor display
class DisplayArcadeMap : public DisplayMapEditor
{
public:
	//! Mission editor control
	CStaticMapArcade *_map;

	//! mission content (template)
	ArcadeTemplate _templateMission;
	//! intro content (template)
	ArcadeTemplate _templateIntro;
	//! outro content (template)
	ArcadeTemplate _templateOutroWin;
	//! outro content (template)
	ArcadeTemplate _templateOutroLoose;

	//! currently selected template
	InitPtr<ArcadeTemplate> _currentTemplate;
	
	//! current editing mode
	InsertMode _mode;
	//! info about last edited unit (defaults for next inserted unit)
	ArcadeUnitInfo _lastUnit;

	//! editor called from server display
	bool _multiplayer;
	//! preview is running
	bool _running;
	//! if advanced (or easy) editor mode is selected
	bool _advanced;

public:
	//! constructor
	/*!
		\param parent parent display
		\param multiplayer editor called from server display
	*/
	DisplayArcadeMap(ControlsContainer *parent, bool multiplayer = false);

	WeatherState GetWeather();
	Clock GetTime();
	bool GetPosition(Point3 &pos);
	CStaticMap *GetMap() {return _map;}

	void OnChildDestroyed(int idd, int exit);

	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	void OnButtonClicked(int idc);
	void OnComboSelChanged(int idc, int curSel);
	void OnToolBoxSelChanged(int idc, int curSel);
	bool OnUnregisteredAddonUsed( RString addon );
	void OnSimulate(EntityAI *vehicle);
	
	bool CanDestroy();
	void Destroy();

	//! update buttons state
	void ShowButtons();

protected:
	//! serialize all templates
	LSError SerializeAll(ParamArchive &ar, bool merge = false);
	//! load all templates from given sqm file
	bool LoadTemplates(const char *filename);
	//! merge all templates with given sqm file
	bool MergeTemplates(const char *filename);
	//! save all templates into given sqm file
	bool SaveTemplates(const char *filename);
	//! sets advanced (or easy) mode of editor
	void SetAdvancedMode(bool advanced);
	//! loads parameters (advanced switch state) from user profile
	void LoadParams();
	//! saves parameters (advanced switch state) from user profile
	void SaveParams();

	//@{
	//! update buttons state
	void UpdateIdsButton();
	void UpdateTexturesButton();
	//@}
};

#endif // #if _ENABLE_EDITOR

// Mission, intro, outro, campaign intro displays

//! "Fake" display used when mission is running
/*!
	No display. Game is running "in background".
	Waits for special user input (ESCAPE) or game over.
*/
class DisplayMission : public Display
{
protected:
	//! if mission is called from missin editor (preview)
	bool _editor;

	//! in game compass object
	Ref<Compass> _compass;
	//! in game watch object
	Ref<Watch> _watch;

public:
	//! constructor
	/*!
		\param parent parent display
		\param editor if mission is called from missin editor (preview)
		\param erase erase save and autosave file
		\param load mission is created by loading of fps file
	*/
	DisplayMission(ControlsContainer *parent, bool editor = false, bool erase = true, bool load = false);
	//! destructor
	~DisplayMission();

	ControlObject *OnCreateObject(int type, int idc, const ParamEntry &cls);
	void OnSimulate(EntityAI *vehicle);
	void OnChildDestroyed(int idd, int exit);
	void OnButtonClicked(int idc) {}

	//! create hint dialog (HintC function)
	/*!
		\param hint displayed string
	*/
	void ShowHint(RString hint);

protected:
	//! sets InGameUI parameters
	void InitUI();
	//! retry mission from autosave or beginning
	bool RetryMission();
//	bool RestartMission();
};

//! "Fake" display used when cutscene is performed
/*!
	No display. Game is running "in background". Waits for user input.
*/
class DisplayCutscene : public Display
{
public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayCutscene(ControlsContainer *parent);
	//! destructor
	~DisplayCutscene();

	void OnSimulate(EntityAI *vehicle);
	void OnButtonClicked(int idc) {}
};

//! "Fake" display used when intro cutscene is performed
/*!
	No display. Game is running "in background". Waits for user input.
*/
class DisplayIntro : public DisplayCutscene
{
public:
	enum NoInit {noInit};
	//! constructor
	/*!
		No cutscene is started.
		\param parent parent display
	*/
	DisplayIntro(ControlsContainer *parent, NoInit);
	//! constructor
	/*!
		Current cutscene is started.
		\param parent parent display
	*/
	DisplayIntro(ControlsContainer *parent);
	//! constructor
	/*!
		Given cutscene is started.
		\param parent parent display
		\param cutscene name of mission which intro is started
	*/
	DisplayIntro(ControlsContainer *parent, RString cutscene);
	//! Initialize game UI (create map etc.)
	void Init();
};

extern RString MPMissionsDir;
extern RString AnimsDir;
extern RString MissionsDir;

RString GetSaveDirectory();
void SetBaseDirectory(RString dir);
void SetMission(RString world, RString mission, RString subdir);
void SetMission(RString world, RString mission);

RString GetMissionsDirectory();
RString GetMissionDirectory();
RString GetBriefingFile();

///////////////////////////////////////////////////////////////////////////////
// Mission editor subdisplays

#if _ENABLE_EDITOR

//! Control for interactive editation of azimuth
class CStaticAzimut : public CStatic
{
protected:
	//! id of attached edit control
	int _idcEdit;

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
		\param idcEdit id of attached edit control
	*/
	CStaticAzimut(ControlsContainer *parent, int idc, const ParamEntry &cls, int idcEdit)
		:	CStatic(parent, idc, cls) {_enabled = true; _idcEdit = idcEdit;}

	void OnDraw(float alpha);
	void OnLButtonClick(float x, float y);
};

//! Unit properties display
class DisplayArcadeUnit : public Display
{
public:
	//@{
	//! unit identification
	int _indexGroup;
	int _index;
	//@}

	//! unit parameters
	ArcadeUnitInfo _unit;
	//! whole mission template
	ArcadeTemplate *_template;

public:
	//! constructor
	/*!
		\param parent parent display
		\param indexGroup unit identification
		\param index unit identification
		\param unit unit parameters
		\param t whole mission template
		\param advanced	if advanced (or easy) editor mode is selected
	*/
	DisplayArcadeUnit
	(
		ControlsContainer *parent,
		int indexGroup, int index,
		ArcadeUnitInfo &unit,
		ArcadeTemplate *t,
		bool advanced
	)
		: Display(parent)
	{
		_enableSimulation = false;
		_enableDisplay = false;
	
		_indexGroup = indexGroup;
		_index = index;
		_unit = unit;
		_template = t;
		if (_unit.side == TEmpty)
		{
			_unit.player = APNonplayable;
		}
		if (advanced)
			Load("RscDisplayArcadeUnit");
		else
			Load("RscDisplayArcadeUnitSimple");
	}
	
	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	void OnComboSelChanged(int idc, int curSel);
	bool CanDestroy();
	void Destroy();

	//! update list of vehicle classes
	/*!
		\param combo updated control
		\param vehicle current vehicle name
	*/
	void UpdateClasses(CCombo *combo, const char *vehicle);
	//! update list of vehicles
	/*!
		\param combo updated control
		\param vehicle current vehicle name
	*/
	void UpdateVehicles(CCombo *combo, const char *vehicle);
	//! update list of playable states
	/*!
		\param combo updated control
		\param player current playable state
	*/
	void UpdatePlayer(CCombo *combo, ArcadeUnitPlayer player);
};

//! Group properties display
class DisplayArcadeGroup : public Display
{
public:
	//! insert position
	Vector3 _position;
	//! initial azimuth
	float _azimut;

public:
	//! constructor
	/*!
		\param parent parent display
		\param position insert position
		\param side initial (default) side
		\param type initial (default) type
		\param name initial (default) name
		\param azimuth initial (default) azimuth
	*/
	DisplayArcadeGroup
	(
		ControlsContainer *parent, Vector3Par position,
		RString side, RString type, RString name, float azimut
	);
	
	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	void OnComboSelChanged(int idc, int curSel);

	//! update list of group types (if side changes)
	void UpdateTypes();	
	//! update list of group names (if type changes)
	void UpdateNames();	
};

//! Marker properties display
class DisplayArcadeMarker : public Display
{
public:
	//! marker identification
	int _index;
	//! marker properties
	ArcadeMarkerInfo _marker;

	//! whole mission template
	ArcadeTemplate *_template;
	
protected:
	//! previous marker type (if type changes, size recalculations can be made)
	int _oldType;

public:
	//! constructor
	/*!
		\param parent parent display
		\param index marker identification
		\param marker marker parameters
		\param templ whole mission template
		\param advanced	if advanced (or easy) editor mode is selected
	*/
	DisplayArcadeMarker
	(
		ControlsContainer *parent,
		int index,
		ArcadeMarkerInfo &marker,
		ArcadeTemplate *templ,
		bool advanced
	)
		: Display(parent)
	{
		_enableSimulation = false;
		_enableDisplay = false;

		_index = index;
		_marker = marker;
		_template = templ;

		_oldType = marker.markerType;

		if (advanced)
			Load("RscDisplayArcadeMarker");
		else
			Load("RscDisplayArcadeMarkerSimple");
		
		ChangeType(marker.markerType);
	}

	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	void OnToolBoxSelChanged(int idc, int curSel);

	bool CanDestroy();
	void Destroy();

protected:
	//! change type of marker (some display updates and size recalculations must be done)
	void ChangeType(int curSel);
};

//! Sensor properties display
class DisplayArcadeSensor : public Display
{
public:
	//@{
	//! sensor identification
	int _ig;
	int _index;
	//@}
	//! sensor properties
	ArcadeSensorInfo _sensor;
	//! whole mission template
	ArcadeTemplate *_t;
	//!	if advanced (or easy) editor mode is selected
	bool _advanced;

public:
	//! constructor
	/*!
		\param parent parent display
		\param ig sensor identification
		\param index sensor identification
		\param sensor sensor parameters
		\param t whole mission template
		\param advanced	if advanced (or easy) editor mode is selected
	*/
	DisplayArcadeSensor
	(
		ControlsContainer *parent,
		int ig, int index,
		ArcadeSensorInfo &sensor,
		ArcadeTemplate *t,
		bool advanced
	)
		: Display(parent)
	{
		_enableSimulation = false;
		_enableDisplay = false;
		_ig = ig;
		_index = index;
		_sensor = sensor;
		_t = t;
		_advanced = advanced;

		if (advanced)
			Load("RscDisplayArcadeSensor");
		else
			Load("RscDisplayArcadeSensorSimple");
	}

	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);

	void OnButtonClicked(int idc);
	void OnChildDestroyed(int idd, int exit);

	bool CanDestroy();
	void Destroy();
};

//! Effects (on sensor or waypoint) properties display
class DisplayArcadeEffects : public Display
{
public:
	//! effects properties
	ArcadeEffects _effects;

public:
	//! constructor
	/*!
		\param parent parent display
		\param effects effects parameters
		\param advanced	if advanced (or easy) editor mode is selected
	*/
	DisplayArcadeEffects
	(
		ControlsContainer *parent,
		ArcadeEffects effects,
		bool advanced
	);
	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	void OnComboSelChanged(int idc, int curSel);

	void Destroy();

protected:
	//! change type of title effects (display update is performed)
	void ChangeTitleType(int type);
};

//! Mission (template) save display
class DisplayTemplateSave : public Display
{
public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayTemplateSave(ControlsContainer *parent)
		: Display(parent)
	{
		_enableSimulation = false;
		_enableDisplay = false;
		Load("RscDisplayTemplateSave");
	}

	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
};

//! Mission (template) load display
class DisplayTemplateLoad : public Display
{
public:
	//! merge operation will be performed
	bool _merge;

public:
	//! constructor
	/*!
		\param parent parent display
		\param merge merge operation will be performed
	*/
	DisplayTemplateLoad(ControlsContainer *parent, bool merge = false)
		: Display(parent)
	{
		_enableSimulation = false;
		_enableDisplay = false;
		_merge = merge;
		Load("RscDisplayTemplateLoad");
		OnIslandChanged();
	}

	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	void OnComboSelChanged(int idc, int curSel);

protected:
	//! Display updates when island changes (list of missions must be updated)
	void OnIslandChanged();
};

//! Mission intel properties
class DisplayIntel : public Display
{
public:
	//! Intel parameters
	ArcadeIntel _intel;

public:
	//! constructor
	/*!
		\param parent parent display
		\param intel intel parameters
		\param advanced	if advanced (or easy) editor mode is selected
	*/
	DisplayIntel(ControlsContainer *parent, ArcadeIntel &intel, bool advanced)
		: Display(parent)
	{
		_enableSimulation = false;
		_enableDisplay = false;

		_intel = intel;
		if (advanced)
			Load("RscDisplayIntel");
		else
			Load("RscDisplayIntelSimple");
	}

	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	void Destroy();

	void OnComboSelChanged(int idc, int curSel);

	//! updates list of days (if month changes)
	/*!
		\param combo updated control
		\param day currently selected day
	*/
	void UpdateDays(CCombo *combo, int day = -1);
};

#endif // #if _ENABLE_EDITOR

#endif
