// Landscape - geography handling, visibility and AI support
// (C) 1996, SUMA
#include "wpch.hpp"
#include "landscape.hpp"
#include "dynSound.hpp"
#include "world.hpp"
#include <El/Common/randomGen.hpp>
#include "roads.hpp"
#include "mapTypes.hpp"
#include "global.hpp"

#include "ai.hpp"
#include "objectClasses.hpp"

#include <Es/Algorithms/qsort.hpp>
#include <Es/Common/filenames.hpp>

static int CalculateGradient( float gradient )
{
	gradient=fabs(gradient);
	if( gradient>=0.95 ) return 7; // nonpenetrable even for man
	else if( gradient>=0.60 ) return 6; // man can go here
	else if( gradient>=0.40 ) return 5; // most vehicles can go here, but very slow
	else if( gradient>=0.25 ) return 4; // most vehicles can go here, but slow
	else if( gradient>=0.15 ) return 3; // most vehicles can go here at 0.33 speed
	else if( gradient>=0.10 ) return 2; // most vehicles can go here at 0.5 speed
	else if( gradient>=0.05 ) return 1; // most vehicles can go here at 0.8 speed
	return 0; // leveled surfaces - top speed for all
}

// calculate max. gradient of plane gradX,gradZ
static float MaxGradient( float gradX, float gradZ )
{
	// note: this is not exact, max. gradient may lay in arbitrary direction
	// TODO: calculate real max. gradient
	float gradXZ=((gradX+gradZ)*0.5)*H_SQRT2;
	return floatMax(gradXZ,floatMax(gradX,gradZ));
}

inline ForestPlain *NewForest( const char *name, int id )
{
	Ref<LODShapeWithShadow> shape = Shapes.New(name,false,true);
	Object *obj = NewObject(shape,id);
	return dyn_cast<ForestPlain>(obj);
	
}

#define ONLY_FOREST 0

void Landscape::ResetGeography()
{
	_mountains.Clear();
}

static const RStringB roadString="road";

/*!
If geography information is already present, detect it and return as fast as possible.
*/

void Landscape::InitGeography()
{
	GRoadNet=new RoadNet;
	GRoadNet->Build(this);

	// check if randomization etc. is valid
	if (_mountains.Size()>0)
	{
		// all data loaded from file
		// no additional processing required
		return;
	}

	RString name = Glob.header.worldname;
	if (name.GetLength()<=0)
	{
		char shortName[80];
		GetFilename(shortName,_name);
		name = shortName;
	}
	// if we are running buldozer, world may be not registered
	extern bool LandEditor;
	if (LandEditor)
	{
		if (!(Pars>>"CfgWorlds").FindEntry(name)) return;
	}

	DoAssert(_landRange==_terrainRange);

/*	
	const ParamEntry &world = Pars >> "CfgWorlds">> name;
	RStringB forestOldTriangle = world >> "forestOldTriangle";
	RStringB forestOldSquare = world >> "forestOldSquare";
	RStringB forestInner = world >> "forestInner";
	RStringB forestOuter = world >> "forestOuter";
	RStringB forestTriangle = world >> "forestTriangle";
	RStringB forestBorder = world >> "forestBorder";

	RStringB forestOuterT1;
	RStringB forestOuterT2;
	if (world.FindEntry("forestOuterT1"))
	{
		forestOuterT1 = world>>"forestOuterT1";
	}
	if (world.FindEntry("forestOuterT2"))
	{
		forestOuterT2 = world>>"forestOuterT2";
	}
*/

	int x,z;
	for( z=0; z<_landRange; z++ )
	{
		I_AM_ALIVE();
		for( x=0; x<_landRange; x++ )
		{
			GeographyInfo &geog=_geography(x,z);
			geog.packed=0;
			if( !this_InRange(x+1,z+1) )
			{
				geog.u.waterDepth=3;
			}
			else
			{
				// check if the current square is in water
				//int tIndex=GetTex(x,z);
				//AbstractTexture *texture=_texture[tIndex];
				// TODO: check texture surface properties
				// check (x,z) gradient
				// gradient is in interval 0..+7 (0 == level)
				float y=GetData(x,z);
				float yNextX=GetData(x+1,z);
				float yNextZ=GetData(x,z+1);
				float yNextXZ=GetData(x+1,z+1);
				float yMin=floatMin(floatMin(y,yNextX),floatMin(yNextZ,yNextXZ));
				float yMax=floatMax(floatMax(y,yNextX),floatMax(yNextZ,yNextXZ));
				if( yMin<0.1 )
				{
					if( yMin<-0.8 )
					{
						if (yMax<-2.5)
						{
							// guranteed deep water
							geog.u.waterDepth=3;
						}
						else
						{
							// mixed shallow/deep water
							geog.u.waterDepth=2;
						}
					}
					else
					{
						// guaranteed shallow water
						geog.u.waterDepth=1;
					}
				}
				float gradX0=fabs((yNextX-y)*_invLandGrid);
				float gradZ0=fabs((yNextZ-y)*_invLandGrid);
				float gradX1=fabs((yNextXZ-yNextZ)*_invLandGrid);
				float gradZ1=fabs((yNextXZ-yNextX)*_invLandGrid);
				float maxGrad0=MaxGradient(gradX0,gradZ0);
				float maxGrad1=MaxGradient(gradX1,gradZ1);
				geog.u.gradient=CalculateGradient(floatMax(maxGrad0,maxGrad1));
				// check how many objects are in the square
				// if there is some forest object, mark square as forest
				float totalArea=0; // sum area of all objects in the sqaure
				int totalCount=0;
				float hardArea=0; // sum area of all objects in the sqaure
				int hardCount=0;
				int howMany=0;
				float xMin=x*_landGrid,zMin=z*_landGrid;
				float xMax=xMin+_landGrid,zMax=zMin+_landGrid;
				for( int xx=x-1; xx<=x+1; xx++ ) for( int zz=z-1; zz<=z+1; zz++ )
				{
					if (!this_InRange(xx,zz)) continue;
					ObjectList &list=_objects(xx,zz);
					for( int oi=0; oi<list.Size(); oi++ )
					{
						Object *obj=list[oi];
						LODShape *shape=obj->GetShape();
						if( !shape ) continue; // ignore empty objects
						const RStringB &className = shape->GetPropertyClass();
						if( xx==x && zz==z )
						{
							if (className==roadString)
							{
								// it is road - use it as road
								geog.u.road=true;
								continue;
							}
							switch (shape->GetMapType())
							{
							case MapForestTriangle:
							case MapForestSquare:
								geog.u.forestOuter = true;
								break;
							case MapForestBorder:
/* delete in generic process driven by class ReplaceObjects
								list.Delete(oi);
								oi--;
*/
								continue;
							}
						}
						if( obj->GetType()!=Primary ) continue; // consider only primary objects
						if( shape->Mass()<10.0 ) continue;
						if( shape->FindGeometryLevel()<0 ) continue;
						if( shape->GetGeomComponents().Size()<=0 ) continue;
						// calculate exact intersection area
						// of bounding rectangle with landscape rectangle
						// TODO: consider rotation
						float objRad=floatMax(shape->Max()[0],shape->Max()[2]);
						Vector3 objCenter=obj->PositionModelToWorld(shape->GeometryCenter());
						float xMaxObj=objCenter.X()+objRad,xMinObj=objCenter.X()-objRad;
						float zMaxObj=objCenter.Z()+objRad,zMinObj=objCenter.Z()-objRad;
						if( xMaxObj<=xMin || xMinObj>=xMax ) continue;
						if( zMaxObj<=zMin || zMinObj>=zMax ) continue;
						saturateMin(xMaxObj,xMax),saturateMax(xMinObj,xMin);
						saturateMin(zMaxObj,zMax),saturateMax(zMinObj,zMin);
						float area=(xMaxObj-xMinObj)*(zMaxObj-zMinObj);
						totalArea+=area;
						totalCount++;
						if (dyn_cast<ForestPlain>(obj)==NULL)
						{
							if( obj->GetDestructType()==DestructNo )
							{
								// undestroyable obstacles calculate much more
								hardArea+=area*4;
								hardCount+=2;
							}
							else if
							(
								obj->GetDestructType()!=DestructTree &&
								obj->GetDestructType()!=DestructTent
							)
							{
								// normal objects are hard to maneuvre
								hardArea+=area;
								hardCount+=1;
							}
						}
					}
				}
				// filled rectangles are assumed forest
				if( totalArea>=(_landGrid*_landGrid)*9/10 ) geog.u.full=1;
				else if( totalArea>=(_landGrid*_landGrid)/2 || totalCount>=20 ) {if( howMany<3 ) howMany=3;}
				else if( totalArea>=(_landGrid*_landGrid)/4 || totalCount>=10 ) {if( howMany<2 ) howMany=2;}
				else if( totalCount>0 ) {if( howMany<1 ) howMany=1;}
				// detect antitank obstacles
				int hard=0;
				if( hardArea>=(_landGrid*_landGrid)/3 || hardCount>=8 ) {if( hard<3 ) hard=3;}
				else if( hardArea>=(_landGrid*_landGrid)/6  || hardCount>=4 ) {if( hard<2 ) hard=2;}
				else if( hardCount>0 ) {if( hard<1 ) hard=1;}
				
				geog.u.howManyObjects=howMany;
				geog.u.howManyHardObjects=hard;

				if( geog.u.road )
				{ // road is always well penetrable
					geog.u.full=false;
					//geog.waterDepth=0;
					if( geog.u.gradient>5 ) geog.u.gradient=5;
					if( geog.u.howManyObjects>2 ) geog.u.howManyObjects=2;
				}
			}
		}
	}

/*
	// check for inner forests
	Ref<LODShapeWithShadow> shapeInner = Shapes.New
	(
		forestInner, false, true
	);
	for (z=0; z<_landRange; z++)
	{
		I_AM_ALIVE();
		for (x=0; x<_landRange; x++)
		{
			GeographyInfo &geog = _geography(x, z);
			if (!geog.forestOuter) continue;
			if (!this_InRange(x, z - 1)) goto OuterForest;
			GeographyInfo g = _geography(x, z - 1);
			if (!g.forestOuter && !g.forestInner) goto OuterForest;
			if (!this_InRange(x, z + 1)) goto OuterForest;
			g = _geography(x, z + 1);
			if (!g.forestOuter && !g.forestInner) goto OuterForest;
			if (!this_InRange(x - 1, z)) goto OuterForest;
			g = _geography(x - 1, z);
			if (!g.forestOuter && !g.forestInner) goto OuterForest;
			if (!this_InRange(x + 1, z)) goto OuterForest;
			g = _geography(x + 1, z);
			if (!g.forestOuter && !g.forestInner) goto OuterForest;
	// InnerForest:
			// geog.forestInner = true;
			// geog.forestOuter = false;
	OuterForest:
			ObjectList &list = _objects(x, z);
			for (int oi=0; oi<list.Size(); oi++)
			{
				Object *obj = list[oi];
				LODShape *shape = obj->GetShape();
				if (!shape) continue; // ignore empty objects
				if (stricmp(shape->Name(), forestOldSquare) == 0)
				{
					if (geog.forestInner)
					{
						obj->SetShape(shapeInner);
					}
					else
					{
						Matrix4 transform = obj->Transform();
						int id = obj->ID();
						list.Delete(oi);
						// obj is not valid now

						if (forestOuterT1.GetLength()>0 && forestOuterT2.GetLength()>0)
						{
							{
								ForestPlain *veh = NewForest(forestOuterT1, id);
								// transform bcenter 

								Vector3 pos;
								pos.Init();
								pos[0] = x * _landGrid + 0.5 * _landGrid;
								pos[2] = z * _landGrid + 0.5 * _landGrid;
								pos[1] = SurfaceY(pos[0], pos[2]);
								pos += veh->GetShape()->BoundingCenter();
								transform.SetTranslation(pos);

								veh->SetTransform(transform);
								veh->InitSkew(this);
								veh->SetType(Primary);
								AddObject(veh);
								_objectIds.Access(id);
								_objectIds[id] = veh;
							}
							{
								// TODO: create a new forest
								int id = NewObjectID();
								ForestPlain *veh = NewForest(forestOuterT2, id);


								Vector3 pos;
								pos.Init();
								pos[0] = x * _landGrid + 0.5 * _landGrid;
								pos[2] = z * _landGrid + 0.5 * _landGrid;
								pos[1] = SurfaceY(pos[0], pos[2]);
								pos += veh->GetShape()->BoundingCenter();

								transform.SetTranslation(pos);

								veh->SetTransform(transform);
								veh->InitSkew(this);
								veh->SetType(Primary);
								AddObject(veh);
								_objectIds.Access(id);
								_objectIds[id] = veh;
							}
						}
						else
						{
							Object *veh = NewForest(forestOuter, id);
							veh->SetTransform(transform);
							veh->SetType(Primary);
							AddObject(veh);
							_objectIds.Access(id);
							_objectIds[id] = veh;
						}
					}
					break;
				}
				else if (stricmp(shape->Name(), forestOldTriangle) == 0)
				{
					#if !ONLY_FOREST
					Assert(geog.forestOuter);
					Matrix4 transform = obj->Transform();
					int id = obj->ID();
					list.Delete(oi);
					Object *veh = NewForest(forestTriangle, id);
					veh->SetTransform(transform);
					veh->SetType(Primary);
					AddObject(veh);
					_objectIds.Access(id);
					_objectIds[id] = veh;
					#endif
					break;
				}
			}
		}
	}

	#if !ONLY_FOREST
	// add forest borders
	Ref<LODShapeWithShadow> shapeBorder = Shapes.New
	(
		forestBorder, false, true
	);
	for (z=0; z<_landRange; z++)
	{
		I_AM_ALIVE();
		for (x=0; x<_landRange; x++)
		{
			GeographyInfo &geog = _geography(x, z);
			if (!geog.forestOuter) continue;
			if (this_InRange(x, z - 1))
			{
				GeographyInfo g = _geography(x, z - 1);
				if (g.forestInner)
				{
					// add border
					int id = NewObjectID();
					Object *forest = NewObject(shapeBorder, id);
					Matrix4 transform(MTranslation, shapeBorder->BoundingCenter());
					transform = Matrix4(MRotationY, H_PI) * transform;
					Vector3 pos;
					pos.Init();
					pos[0] = x * _landGrid + 0.5 * _landGrid;
					pos[2] = z * _landGrid + 0.5 * _landGrid;
					pos[1] = SurfaceY(pos[0], pos[2]);
					transform = Matrix4(MTranslation, pos) * transform;
					forest->SetType(Primary);
					forest->SetTransform(transform);
					AddObject(forest);
					_objectIds.Access(id);
					_objectIds[id] = forest;
				}
			}
			if (this_InRange(x, z + 1))
			{
				GeographyInfo g = _geography(x, z + 1);
				if (g.forestInner)
				{
					// add border
					int id = NewObjectID();
					Object *forest = NewObject(shapeBorder, id);
					Matrix4 transform;
					transform.SetIdentity();
					Vector3 pos;
					pos.Init();
					pos[0] = x * _landGrid + 0.5 * _landGrid;
					pos[2] = z * _landGrid + 0.5 * _landGrid;
					pos[1] = SurfaceY(pos[0], pos[2]);
					pos += shapeBorder->BoundingCenter();
					transform.SetPosition(pos);
					forest->SetType(Primary);
					forest->SetTransform(transform);
					AddObject(forest);
					_objectIds.Access(id);
					_objectIds[id] = forest;
				}
			}
			if (this_InRange(x - 1, z))
			{
				GeographyInfo g = _geography(x - 1, z);
				if (g.forestInner)
				{
					// add border
					int id = NewObjectID();
					Object *forest = NewObject(shapeBorder, id);
					Matrix4 transform(MTranslation, shapeBorder->BoundingCenter());
					transform = Matrix4(MRotationY, 0.5 * H_PI) * transform;
					Vector3 pos;
					pos.Init();
					pos[0] = x * _landGrid + 0.5 * _landGrid;
					pos[2] = z * _landGrid + 0.5 * _landGrid;
					pos[1] = SurfaceY(pos[0], pos[2]);
					transform = Matrix4(MTranslation, pos) * transform;
					forest->SetType(Primary);
					forest->SetTransform(transform);
					AddObject(forest);
					_objectIds.Access(id);
					_objectIds[id] = forest;
				}
			}
			if (this_InRange(x + 1, z))
			{
				GeographyInfo g = _geography(x + 1, z);
				if (g.forestInner)
				{
					// add border
					int id = NewObjectID();
					Object *forest = NewObject(shapeBorder, id);
					Matrix4 transform(MTranslation, shapeBorder->BoundingCenter());
					transform = Matrix4(MRotationY, 1.5 * H_PI) * transform;
					Vector3 pos;
					pos.Init();
					pos[0] = x * _landGrid + 0.5 * _landGrid;
					pos[2] = z * _landGrid + 0.5 * _landGrid;
					pos[1] = SurfaceY(pos[0], pos[2]);
					transform = Matrix4(MTranslation, pos) * transform;
					forest->SetType(Primary);
					forest->SetTransform(transform);
					AddObject(forest);
					_objectIds.Access(id);
					_objectIds[id] = forest;
				}
			}
		}
	}
	#endif
*/
	ReplaceObjects(name);
		
	I_AM_ALIVE();
	InitSoundMap();
	I_AM_ALIVE();
	InitRandomization();
	I_AM_ALIVE();
	InitMountains();
}

struct ReplaceObjectInfo
{
	Ref<LODShapeWithShadow> replace;
	RefArray<LODShapeWithShadow> with;
	bool center;
};
TypeIsMovableZeroed(ReplaceObjectInfo)

/*!
\patch 1.52 Date 4/22/2002 by Jirka
- Added: Support for multiple types of forests on single island
\patch_internal 1.85 Date 9/17/2002 by Ondra
- Fixed: Bug in binarization caused same object ID was used twice.
\patch 1.85 Date 9/17/2002 by Ondra
- Fixed: Some object ID for static objects on all islands were used twice.
*/

void Landscape::ReplaceObjects(RString name)
{
	if (_objectIds.Size()>0)
	{
		RebuildIDCache();
	}
	else
	{
		ResetObjectIDs();
	}
	// decide which objects to replace
	const ParamEntry &world = Pars >> "CfgWorlds">> name;
	
	const ParamEntry *entry = world.FindEntry("ReplaceObjects");
	if (!entry) return;
	int n = entry->GetEntryCount();
	if (n == 0) return;

	AutoArray<ReplaceObjectInfo> replace;
	replace.Realloc(n);
	replace.Resize(n);
	
	for (int i=0; i<n; i++)
	{
		const ParamEntry &cls = entry->GetEntry(i);
		ReplaceObjectInfo &info = replace[i];

		RString value = cls >> "replace";
		info.replace = Shapes.New(value, false, true);
		int m = (cls >> "with").GetSize();
		info.with.Realloc(m);
		info.with.Resize(m);
		for (int j=0; j<m; j++)
		{
			RString value = (cls >> "with")[j];
			info.with[j] = Shapes.New(value, false, true);
		}
		info.center = cls.FindEntry("center") ? cls >> "center" : false;
	}

	// replace objects
	for (int z=0; z<_landRange; z++)
	{
		I_AM_ALIVE();
		for (int x=0; x<_landRange; x++)
		{
			ObjectList &src = _objects(x, z);
			int on = src.Size();
			RefArray<Object> copy;
			copy.Realloc(on);
			copy.Resize(on);
			for (int oi=0; oi<on; oi++) copy[oi] = src[oi];
			for (int oi=0; oi<on; oi++)
			{
				Object *obj = copy[oi];
				LODShapeWithShadow *shape = obj->GetShape();
				if (!shape) continue; // ignore empty objects

				// find in replace list
				for (int i=0; i<replace.Size(); i++)
				{
					if (replace[i].replace == shape)
					{
						Matrix4 transform = obj->Transform();
						int id = obj->ID();
						bool idUsed = false;

						// delete old object
						for (int j=0; j<src.Size(); j++)
						{
							if (src[j] == obj)
							{
								src.Delete(j);
								break;
							}
						}
						
						// add new objects
						for (int j=0; j<replace[i].with.Size(); j++)
						{
							LODShapeWithShadow *newShape = replace[i].with[j];
							if (newShape)
							{
								if (idUsed) id = NewObjectID();
								else idUsed = true;

								Object *newObject = NewObject(newShape, id);

								// transform bcenter 
								if (replace[i].center)
								{
									Vector3 pos;
									pos.Init();
									pos[0] = x * _landGrid + 0.5 * _landGrid;
									pos[2] = z * _landGrid + 0.5 * _landGrid;
									pos[1] = SurfaceY(pos[0], pos[2]);
									pos += newObject->GetShape()->BoundingCenter();
									transform.SetTranslation(pos);
								}

								newObject->SetTransform(transform);
								newObject->InitSkew(this);
								newObject->SetType(Primary);
								AddObject(newObject);
								_objectIds.Access(id);
								_objectIds[id] = newObject;
							}
						}
						break;
					}
				}
			}
		}
	}
}

static int CmpMountains(const Vector3 *v0, const Vector3 *v1)
{
	return sign(v1->Y() - v0->Y());
}

void Landscape::InitMountains()
{
	_mountains.Clear();
	// found local maximas
	for (int z=1; z<_landRangeMask; z++) for (int x=1; x<_landRangeMask; x++)
	{
		float h11 = GetData(x, z);
		if (h11 <= 0) continue;
		float h00 = GetData(x - 1, z - 1);
		float h01 = GetData(x, z - 1);
		float h02 = GetData(x + 1, z - 1);
		float h10 = GetData(x - 1, z);
		float h12 = GetData(x + 1, z);
		float h20 = GetData(x - 1, z + 1);
		float h21 = GetData(x, z + 1);
		float h22 = GetData(x + 1, z + 1);

		if
		(
			h11 > h00 && h11 > h01 && h11 > h02 && h11 > h10 &&
			h11 >= h12 && h11 >= h20 && h11 >= h21 && h11 >= h22
		)
		{
			_mountains.Add(Vector3(x * _landGrid, h11, z * _landGrid));
		}
	}

	QSort(_mountains.Data(), _mountains.Size(), CmpMountains);
}

GeographyInfo Landscape::GetGeography( int x, int z ) const
{
	if( !this_InRange(z,x) )
	{
		GeographyInfo geogr;
		geogr.packed=0;
		geogr.u.waterDepth=3;
		return geogr;
	}
	return _geography(x,z);
}

void Landscape::InitSoundMap()
{
	// some basic environmental sounds
	int x,z;
	// set default sound (birds & wind )
	for( z=0; z<_landRange; z++ ) for( x=0; x<_landRange; x++ )
	{
		SetSound(x,z,2); // default sound - bees
	}
	// spread birds sound
	for( z=0; z<_landRange; z++ ) for( x=0; x<_landRange; x++ )
	{
		// count trees
		int nTrees=0;
		const ObjectList &list=_objects(x,z);
		for( int i=0; i<list.Size(); i++ )
		{
			Object *obj=list[i];
			LODShape *shape=obj->GetShape();
			if( !shape ) continue;
			if( !shape->Name() ) continue;
			if( strstr(shape->Name(),"\\str ") ) nTrees++;
			if( strstr(shape->Name(),"\\les ") ) nTrees+=20;
		}
		if( nTrees>=3 )
		{
			for( int zz=-2; zz<=+2; zz++ ) for( int xx=-2; xx<=+2; xx++ )
			{
				SetSound(x+xx,z+zz,1);
			}
		}
	}
	for( z=0; z<_landRange; z++ ) for( x=0; x<_landRange; x++ )
	{
		// some places are quiet
		float height=GetData(x,z);
		if( height>170 )
		{
			SetSound(x,z,3); // wind on highlands
		}
	}
	// spread sea sound
	for( z=0; z<_landRange; z++ ) for( x=0; x<_landRange; x++ )
	{
		const GeographyInfo &geogr=_geography(x,z);
		if( geogr.u.waterDepth>0 )
		{ // sea is very loud and can be heard very far
			for( int zz=-2; zz<=+2; zz++ ) for( int xx=-2; xx<=+2; xx++ )
			{
				SetSound(x+xx,z+zz,0);
			}
		}
	}
}

#include "global.hpp"

/*
void CountDynSounds( const char *file, int line )
{
	int count=0;
	for( int i=0; i<GWorld->NVehicles(); i++ )
	{
		Vehicle *veh=GWorld->GetVehicle(i);
		DynSoundSource *dyns=dynamic_cast<DynSoundSource *>(veh);
		if( dyns ) count++;
	}
	LogF("%s(%d): %s,%s DynSounds %d",file,line,GLandscape->GetName(),Glob.header.worldname,count);
}

void CountDynSounds( const char *file, int line );
#define COUNT_DYN_SOUNDS() CountDynSounds(__FILE__,__LINE__)
*/

void Landscape::InitDynSounds( const ParamEntry &entry )
{
	// init dynamic sounds
	// load config
	const ParamEntry &sounds=entry>>"sounds";
	for( int i=0; i<sounds.GetSize(); i++ )
	{
		const ParamEntry &source=entry>>RString(sounds[i]);
		RString sim=source>>"simulation";
		const ParamEntry &pos=source>>"position";
		float posX=pos[0];
		float posZ=pos[1];
		float posY=RoadSurfaceY(posX,posZ);
		Vector3 position(posX,posY,posZ);
		DynSoundSource *vehicle=new DynSoundSource(sim);
		vehicle->SetPosition(position);
		//GLOB_WORLD->AddVehicle(vehicle);
		GLOB_WORLD->AddBuilding(vehicle);
	}
}

inline int AvoidSteepChange( int val, int x, bool &someChange )
{
	// never increase abs of val
	if( abs(x)>abs(val) ) return val;
	const int maxDif=2;
	int dif=val-x;
	if( dif>maxDif ) dif=maxDif;
	else if( dif<-maxDif ) dif=-maxDif;
	if( dif ) someChange=true;
	return x+dif;
}

void Landscape::InitRandomization()
{
	I_AM_ALIVE();
	int i,x,z;
	_colorizePalette[0]=PackedColor(0x80,0x80,0x80,0x80);
	for( i=1; i<256; i++ )
	{
		const float colorRand=0.1;
		const float colorMid=1.0;
		const float brightRand=0.5;
		const float brightMid=1.0;
		float randR=GRandGen.RandomValue()*colorRand+(colorMid-colorRand*0.5);
		float randG=GRandGen.RandomValue()*colorRand+(colorMid-colorRand*0.5);
		float randB=GRandGen.RandomValue()*colorRand+(colorMid-colorRand*0.5);
		float randBright=GRandGen.RandomValue()*brightRand+(brightMid-brightRand*0.5);
		Color color(randR,randG,randB);
		_colorizePalette[i]=PackedColor(color*0.5*randBright);
	}
	for( z=0; z<_landRange; z++ ) for( x=0; x<_landRange; x++ )
	{
		RandomInfo &random=_random(x,z);
		int colorIndex=toInt(GRandGen.RandomValue()*255);
		if( colorIndex<0 ) colorIndex=0;
		if( colorIndex>255 ) colorIndex=255;
		random.color=colorIndex;
		random.uOff=random.vOff=0;
	}
	I_AM_ALIVE();
	for( z=1; z<_landRangeMask; z++ ) for( x=1; x<_landRangeMask; x++ )
	{
		RandomInfo &random=_random(x,z);
		// check all four neighbourghs
		bool t00=TextureIsSimple(GetTex(x,z));
		bool tM0=TextureIsSimple(GetTex(x-1,z));
		bool t0M=TextureIsSimple(GetTex(x,z-1));
		bool tMM=TextureIsSimple(GetTex(x-1,z-1));
		if( t00 && tM0 && t0M && tMM )
		{
			// full offset possible in both direction
			//int uOff=toInt(GRandGen.RandomValue()*14-7);
			//int vOff=toInt(GRandGen.RandomValue()*14-7);
			int uOff=toInt(GRandGen.RandomValue()*32-16);
			int vOff=toInt(GRandGen.RandomValue()*32-16);
			if( uOff<-7 ) uOff=-7;else if( uOff>+7 ) uOff=+7;
			if( vOff<-7 ) vOff=-7;else if( vOff>+7 ) vOff=+7;
			random.uOff=uOff;
			random.vOff=vOff;
		}
	}
	// avoid too steep changes in u-v offset between neighbourghs
	// TODO: better randomization algorithm
	for( i=0; i<10; i++ )
	{
		I_AM_ALIVE();
		bool someChange=false;
		for( z=1; z<_landRangeMask; z++ ) for( x=1; x<_landRangeMask; x++ )
		{
			bool t00=TextureIsSimple(GetTex(x,z));
			bool tM0=TextureIsSimple(GetTex(x-1,z));
			bool t0M=TextureIsSimple(GetTex(x,z-1));
			bool tMM=TextureIsSimple(GetTex(x-1,z-1));
			if( t00 && tM0 && t0M && tMM )
			{
				RandomInfo &random00=_random(x,z);
				const RandomInfo &randomP0=_random(x+1,z);
				const RandomInfo &randomN0=_random(x-1,z);
				const RandomInfo &random0P=_random(x,z+1);
				const RandomInfo &random0N=_random(x,z-1);
				random00.uOff=AvoidSteepChange(random00.uOff,randomP0.uOff,someChange);
				random00.uOff=AvoidSteepChange(random00.uOff,randomN0.uOff,someChange);
				random00.uOff=AvoidSteepChange(random00.uOff,random0P.uOff,someChange);
				random00.uOff=AvoidSteepChange(random00.uOff,random0N.uOff,someChange);
				random00.vOff=AvoidSteepChange(random00.vOff,randomP0.vOff,someChange);
				random00.vOff=AvoidSteepChange(random00.vOff,randomN0.vOff,someChange);
				random00.vOff=AvoidSteepChange(random00.vOff,random0P.vOff,someChange);
				random00.vOff=AvoidSteepChange(random00.vOff,random0N.vOff,someChange);
			}
		}
		if( !someChange ) break;
	}
	//Log("Landscape offset map blended: %d",i);
	I_AM_ALIVE();
}

