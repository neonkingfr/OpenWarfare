#include "wpch.hpp"

#if !_DISABLE_GUI
#include "winpch.hpp"
#include "poly.hpp"
#include "global.hpp"

#include <El/ParamFile/paramFile.hpp>

#include "engdd.hpp"
#include "lights.hpp"

#ifndef _XBOX
	#include <dxerr8.h>
#endif

#include "debugTrap.hpp"

HWND AppInit
(
	HINSTANCE hInst,HINSTANCE hPrev,int sw, bool fullscreen,
	int x, int y, int width, int height
);

void DDError( const char *text, int err )
{
	#ifndef _XBOX
	LogF("%s:%s",text,DXGetErrorString8(err));
	#else
	LogF("%s:%x",text,err);
	#endif
	if (GDebugger.IsDebugger())
	{
		//BREAK();
	}
}

#define FONT_SIZE 16

// constructor - attach  a engine to given backBuffer

void EngineDD::InitSurfaces()
{
	LoadConfig();
	Init3D();
	DoSetGamma();

	// direct-color mode required
	D3DConstruct();
}

// destroy engine
void EngineDD::DestroySurfaces()
{
	_vBufferLast.Free(); // last SetStreamSource
	_iBufferLast.Free(); // last SetIndices

	//_zBuffer.Free();
	_backBuffer.Free();
	_frontBuffer.Free();
	_d3DDevice.Free();

}

EngineDD::~EngineDD()
{
	SaveConfig();

	D3DDestruct();
	DestroySurfaces();

	_direct3D.Free();
}

void EngineDD::DoSetGamma()
{
	D3DGAMMARAMP ramp;
	// prepare gamma ramp
	int table[256];

	table[0]=0;
	float eGamma=1/_gamma;
	for( int i=1; i<256; i++ )
	{
		float x=i*(1/255.0);
		float fx=pow(x,eGamma);
		#ifdef _XBOX
			int ifx=toInt(fx*255.0);
			saturate(ifx,0,255);
		#else
			int ifx=toInt(fx*65535.0);
			saturate(ifx,0,65535);
		#endif
		table[i]=ifx;
	}

	for( int i=0; i<256; i++ )
	{
		ramp.red[i]=ramp.green[i]=ramp.blue[i]=table[i];
	}
	_d3DDevice->SetGammaRamp(0,&ramp);
	LogF("set gamma %.3f",_gamma);
}

void EngineDD::SetGamma( float gamma )
{
	saturate(gamma,1e-3,1e3);
	_gamma=gamma;
	if (_d3DDevice)
	{
		DoSetGamma();
	}

}

RString GetUserParams();

template <class Type>
static Type LoadCfgDef(const ParamEntry &cfg, const char *name, Type def)
{
	const ParamEntry *entry = cfg.FindEntry(name);
	if (!entry) return def;
	return *entry;
}

extern RString FlashpointCfg;

void EngineDD::LoadConfig()
{
	{
		// global config
		ParamFile cfg;
		cfg.Parse(FlashpointCfg);

		if (!_windowed)
		{
			_w = LoadCfgDef(cfg,"Resolution_W",_w);
			_h = LoadCfgDef(cfg,"Resolution_H",_h);
			_pixelSize = LoadCfgDef(cfg,"Resolution_Bpp",_pixelSize);
			_refreshRate = LoadCfgDef(cfg,"refresh",_refreshRate);
		}
	}
	{
		// user config
		RString name = GetUserParams();

		ParamFile cfg;
		cfg.Parse(name);

		SetGamma(LoadCfgDef(cfg,"gamma",1.6));

		_userEnabledWBuffer = LoadCfgDef(cfg, "useWBuffer", true);
/*
		if (!_windowed)
		{
			_w = LoadCfgDef(cfg,"width",_w);
			_h = LoadCfgDef(cfg,"height",_h);
			_pixelSize = LoadCfgDef(cfg,"bpp",_pixelSize);
			_refreshRate = LoadCfgDef(cfg,"refresh",_refreshRate);
		}
*/
	}

	base::LoadConfig();
}
void EngineDD::SaveConfig()
{
	if (!IsOutOfMemory())
	{
		{
			// gloabal config
			ParamFile cfg;
			cfg.Parse(FlashpointCfg);

			if (!_windowed)
			{
				cfg.Add("Resolution_W",_w);
				cfg.Add("Resolution_H",_h);
				cfg.Add("Resolution_Bpp",_pixelSize);
				cfg.Add("refresh",_refreshRate);
			}

			cfg.Save(FlashpointCfg);
		}
		{
			// user config
			RString name = GetUserParams();

			ParamFile cfg;
			cfg.Parse(name);
			cfg.Add("gamma",_gamma);
			cfg.Save(name);
		}
		base::SaveConfig();
	}
}

void EngineDD::Clear( bool clearZ, bool clear, PackedColor color )
{
	D3DRECT rect;
	rect.x1=0,rect.y1=0;
	rect.x2=_w,rect.y2=_h;
	int flags=0;
	if (clear) flags|=D3DCLEAR_TARGET;
	if (clearZ)
	{
		flags|=D3DCLEAR_ZBUFFER;
		if (_hasStencilBuffer) flags|=D3DCLEAR_STENCIL;
	}

	_d3DDevice->Clear(0,NULL,flags,color,1,0);
}

void EngineDD::WorkToBack()
{
}

//#include "txtD3D.hpp"
//#include "perfProf.hpp"

void EngineDD::BackToFront()
{
	_d3DDevice->Present(NULL,NULL,NULL,NULL);
}


int EngineDD::FrameTime() const
{
	return GlobalTickCount()-_frameTime;
}


void EngineDD::ReportGRAM(const char *name)
{
	#ifndef _XBOX
	IDirect3DDevice8 *device = GetDirect3DDevice();

	if (name) LogF("VRAM report: %s",name);
	int dwFree = device->GetAvailableTextureMem();
	if (name) LogF("  VRAM free %d",dwFree);
	#endif
}


void EngineDD::CreateSurfaces(bool windowed )
{
	_windowed = windowed;

	InitSurfaces();

	// clear all backbuffers
	Clear(true,true);
	BackToFront();
	Clear(true,true);
	BackToFront();
	Clear(true,true);
	BackToFront();

	ReportGRAM("After creating Frame-Buffer");
}

void EngineDD::Pause()
{
}

/*!
\patch_internal 1.36 Date 12/12/2001 by Ondra
- Fixed: Improved multimonitor support in windowed mode.
*/

EngineDD::EngineDD
(
	HINSTANCE hInst, HINSTANCE hPrev, int sw,
	int w, int h, bool windowed, int bpp
)
:_texLoc(TexLocalVidMem),
_flippable(FALSE),
_pixelSize(bpp),
_refreshRate(0),
_bias(0),
_gamma(1),
//_sMesh(NULL),
_mesh(NULL),
_textBank(NULL),
_useDXTL(false),
_useHWTL(false),
_userEnabledWBuffer(true),
_maxLights(8),
_can565(false),_can88(false),_can8888(false),
_lastQueueSource(NULL),
_lastClampU(false),_lastClampV(false),_hasStencilBuffer(false),
_resetNeeded(false)
{
	_grassParam[0] = 0;
	_grassParam[1] = 0;
	_grassParam[2] = 0;
	_grassParam[3] = 0;
	_formatSet=SingleTex;
	_renderMode = RMTris;
	_enableReorder = false;
	_tlActive = false;

	CreateD3D();

	// raster parameters
	#ifndef _XBOX
	if (windowed)
	{
		// search for a location that is handled by selected apapted
		int x=0,y=0;
		SearchMonitor(x,y,w,h);
		_hwndApp=AppInit(hInst,hPrev,sw,FALSE,x,y,w,h);

		RECT client;
		// adjust width, height to match client area
		GetClientRect(_hwndApp,&client);
		_w = client.right-client.left+1;
		_h = client.bottom-client.top+1;
		CreateSurfaces(windowed);
		//_textBank=new TextBank(_engine);
	}
	else
	#endif
	{

		_hwndApp=AppInit(hInst,hPrev,sw,TRUE,0,0,160,160);
		#ifndef _XBOX
		Log("Window created (%x)",_hwndApp);
		if( !_hwndApp ) return;
		#endif
		
		_w = w;
		_h = h;
		CreateSurfaces(windowed);
		//_textBank=new TextBank(_engine);
	}
}

bool EngineDD::SwitchRes(int w, int h, int bpp)
{
	if (w==_w && h==_h && _pixelSize==bpp) return true;
	// react to window resizing by changing surface dimensions
	_w = w;
	_h = h;
	_pixelSize = bpp;
	D3DDEVICE_CREATION_PARAMETERS pars;
	_d3DDevice->GetCreationParameters(&pars);
	int adapter = pars.AdapterOrdinal;
	FindMode(adapter);
	if (_windowed)
	{
		Reset();
	}
	else
	{
		ResetHard();
	}
	return true;
}

bool EngineDD::SwitchWindowed(bool windowed)
{
	if (windowed == _windowed) return true;
	_windowed = windowed;
	_pp.Windowed = windowed;
	Reset();
	return true;
}

bool EngineDD::SwitchRefreshRate(int refresh)
{
	if (_pp.Windowed) return false;
	if (refresh==0) return false;
	if (_refreshRate==refresh) return true;
	_refreshRate = refresh;
	ResetHard();
	return true;
}

static int FormatToBpp(D3DFORMAT format)
{
	switch (format)
	{
		case D3DFMT_A8R8G8B8:
		case D3DFMT_X8R8G8B8:
			return 32;
		#ifndef _XBOX
		case D3DFMT_R8G8B8:
			return 24;
		#endif
		#ifndef _XBOX
		case D3DFMT_X4R4G4B4:
		#endif
		case D3DFMT_R5G6B5:
		case D3DFMT_X1R5G5B5:
		case D3DFMT_A1R5G5B5:
		case D3DFMT_A4R4G4B4:
			return 16;
		#ifndef _XBOX
		case D3DFMT_R3G3B2:
		case D3DFMT_A8R3G3B2:
		#endif
		case D3DFMT_A8:
			return 8;
		default:
			// unknown format
			return 0;
	}

}

void EngineDD::ListResolutions(FindArray<ResolutionInfo> &ret)
{
	ret.Clear();
	if (_pp.Windowed)
	{
		// no resolution change possible
		return;
	}
	// enumerate resolutions available on current device
	D3DDEVICE_CREATION_PARAMETERS pars;
	_d3DDevice->GetCreationParameters(&pars);
	int adapter = pars.AdapterOrdinal;
	for (int i=0; i<_direct3D->GetAdapterModeCount(adapter); i++)
	{
		D3DDISPLAYMODE mode;
		_direct3D->EnumAdapterModes(adapter,i,&mode);
		// mode contains resolution and refresh rate
		ResolutionInfo info;
		info.w = mode.Width;
		info.h = mode.Height;
		info.bpp = FormatToBpp(mode.Format);
		if (info.bpp) ret.AddUnique(info);

	}
}

void EngineDD::ListRefreshRates(FindArray<int> &ret)
{
	if (_pp.Windowed)
	{
		ret.Clear();
		ret.Add(0); // return default
		// no refresh change possible
		return;
	}
	// list refresh rates for current w,h,bpp
	D3DDEVICE_CREATION_PARAMETERS pars;
	_d3DDevice->GetCreationParameters(&pars);
	int adapter = pars.AdapterOrdinal;
	for (int i=0; i<_direct3D->GetAdapterModeCount(adapter); i++)
	{
		D3DDISPLAYMODE mode;
		_direct3D->EnumAdapterModes(adapter,i,&mode);
		// mode contains resolution and refresh rate
		if
		(
			mode.Width==Width() && mode.Height==Height() &&
			FormatToBpp(mode.Format)==PixelSize()
		)
		{
			ret.AddUnique(mode.RefreshRate);
		}

	}

}


template Ref<VertexStaticData>;
template Link<Light>;
#endif