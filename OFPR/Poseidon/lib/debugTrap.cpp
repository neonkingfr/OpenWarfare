#include "wpch.hpp"
#include <Es/Common/win.h>

#include "debugTrap.hpp"

#ifdef _WIN32

#include "multisync.hpp"
#include "imexhnd.h"

class DebugThreadWatch
{
	// check if given thread is working
	mutable CriticalSection _lock;
	int _timeOut;
	int _enable; // <0 disabled, >=0 enabled

	HANDLE _mainThreadHandle;
	HANDLE _myThreadHandle;
	DWORD _myThreadId;
	Event _terminate;
	Event _keepAlive;

	public:
	DebugThreadWatch();
	~DebugThreadWatch();

	
	DWORD Execute();

	static DWORD WINAPI ExecuteHelper( void *watch )
	{
		return ((DebugThreadWatch*)watch)->Execute();
	}

	// external access functions
	void Terminate() {_terminate.Set();}
	void KeepAlive() {_keepAlive.Set();}
	void AliveEnable (int count)
	{
		ScopeLockSection lock(_lock);
		_enable+=count;
	}
	int GetAliveEnabled() const
	{
		ScopeLockSection lock(_lock);
		return _enable;
	}

	void SetAliveTimeout(int timeMs)
	{
		ScopeLockSection lock(_lock);
		_timeOut=timeMs;
	}

};

#ifdef _XBOX
//	#define NtCurrentProcess() NULL
#endif

/*!
\patch 1.37 Date 12/18/2001 by Ondra
- Fixed: Freeze detection caused application shutdown.
In certain situation application can be considered frozen while is is actually only slow.
Now only context.bin is saved, but application continues running
even when it is supposed to be frozen.
*/

DebugThreadWatch::DebugThreadWatch()
{
	ScopeLockSection lock(_lock);
	_timeOut=-1;
	_enable=0;

	::DuplicateHandle
	(
		GetCurrentProcess(),GetCurrentThread(),
		GetCurrentProcess(),&_mainThreadHandle,
		0,false,DUPLICATE_SAME_ACCESS
	);

	DWORD threadId;
	_myThreadId;
	_myThreadHandle = ::CreateThread
	(
		NULL,64*1024,ExecuteHelper,this,CREATE_SUSPENDED,&threadId
	);

	if (_myThreadHandle)
	{
		::ResumeThread(_myThreadHandle);
	}
}

/*!
\patch 1.28 Date 10/18/2001 by Ondra.
- New: When application is frozen, it creates context.bin and Flashpoint.rpt log
and after 30 sec it attempts to terminate.
*/

DWORD DebugThreadWatch::Execute()
{
	DWORD lastAlive=::GetTickCount();
	for(;;)
	{
		SignaledObject *wait[]={&_terminate,&_keepAlive};
		int waitTime,waitTime0;
		{
			ScopeLockSection lock(_lock);
			waitTime0 = waitTime = _timeOut;
			if (waitTime<0 || waitTime>10000) waitTime = 10000;
		}
		DWORD start = ::GetTickCount();
		int which = SignaledObject::WaitForMultiple
		(
			wait,sizeof(wait)/sizeof(*wait),waitTime
		);
		if (which==0)
		{
			return 0; // _terminate event
		}
		else if (which==1)	 // alive event
		{
			lastAlive=::GetTickCount();
			DWORD delay = lastAlive-start;
			if (delay>waitTime-2000 || delay>2500)
			{
				LogF("No alive in %d of %d ms",delay,waitTime);
				LogF("  check another threads -- add ProgressRefresh()");
			}
			// no more event expected until told by NextAlive


		}
		else
		{
			if (waitTime0>=0 && waitTime0<20000 && _enable>0)
			{
				RptF("No alive in %d",waitTime);
				CONTEXT context;
				context.ContextFlags=CONTEXT_FULL;
				::SuspendThread(_mainThreadHandle);
				#ifndef _XBOX
					if (GetThreadContext(_mainThreadHandle,&context))
					{
						GDebugExceptionTrap.ReportContext("FROZEN",&context);
					}
				#else
					FailHook("Frozen");
				#endif
				#if _ENABLE_REPORT
				if (::GetTickCount()-lastAlive>=30000)
				{
					// fatal situation - terminate
					extern void DDTerm();
					DDTerm();
					exit(1);
					/*NOT REACHED*/
				}
				#endif
				::ResumeThread(_mainThreadHandle);
			}
		}
	}
	return 0;
}

DebugThreadWatch::~DebugThreadWatch()
{
}

Debugger::Debugger()
{
	// dynamic function import
	_isDebugger=false;

	#ifndef _XBOX
	HMODULE kernel32 = LoadLibrary("kernel32.dll");
	if (kernel32)
	{
		typedef BOOL IsDebuggerPresentProc(VOID);
		IsDebuggerPresentProc *debPresent = (IsDebuggerPresentProc *)
		(
			GetProcAddress(kernel32,"IsDebuggerPresent")
		);
		if (debPresent)
		{
			_isDebugger = debPresent()!=FALSE;
			if (_isDebugger)
			{
				LogF("Starting debugger session");
			}
		}
	}
	#else
	_isDebugger = true;
	#endif
	#if _RELEASE
	if (!_isDebugger)
	{
		// start light debugger thread
		_watch = new DebugThreadWatch();
	}
	#endif
}

void Debugger::ForceLogging()
{
	_isDebugger = false;
}

void Debugger::NextAliveExpected( int timeout )
{
	/*
	GlobalShowMessage
	(
		timeout,
		"Next alive in %d ms, enabled %d",
		timeout, CheckingAlivePaused()
	);
	*/
	if (_watch)
	{
		_watch->SetAliveTimeout(timeout);
		_watch->KeepAlive();
	}
}

bool Debugger::CheckingAlivePaused()
{
	if (_watch)
	{
		return _watch->GetAliveEnabled()>=0;
	}
	return false;
}

void Debugger::PauseCheckingAlive()
{ // decrement counter - cheking enabled
	if (_watch)
	{
		_watch->AliveEnable(-1);
	}
}

void Debugger::ResumeCheckingAlive()
{ // increment counter - cheking enabled
	if (_watch)
	{
		_watch->AliveEnable(+1);
	}
	I_AM_ALIVE();
}


void Debugger::ProcessAlive()
{
	static int lastAlive=GetTickCount();

	int time = GetTickCount();
	int delay = time-lastAlive; 
	lastAlive = time;

	if (delay>1000)
	{
		__asm nop;
	}
	if (_watch) _watch->KeepAlive();
}

Debugger::~Debugger()
{
	if (_watch) _watch->Terminate();

	if (_isDebugger)
	{
		LogF("Closing debugger session");
	}
}

#else              // ifdef _WIN32

class DebugThreadWatch
{
};

Debugger::Debugger()
{
	_isDebugger = false;
}

void Debugger::ForceLogging()
{
	_isDebugger = false;
}

void Debugger::NextAliveExpected( int timeout )
{
}

bool Debugger::CheckingAlivePaused()
{
	return false;
}

void Debugger::PauseCheckingAlive()
{
}

void Debugger::ResumeCheckingAlive()
{
}

void Debugger::ProcessAlive()
{
}

Debugger::~Debugger()
{
}

#endif

// initialization
Debugger GDebugger;
