// Poseidon - shadow casting
// (C) 1997, SUMA
#include "wpch.hpp"

#include "object.hpp"
#include "scene.hpp"
#include "poly.hpp"
#include "tlVertex.hpp"
#include "global.hpp"
#include "engine.hpp"
#include "landscape.hpp"
#include "world.hpp"
#include "lights.hpp"
#include "camera.hpp"
#include <El/Common/perfLog.hpp>

//#define MAX_CACHE_SHADOWS 512

DEFINE_FAST_ALLOCATOR(RemmemberShadow)

RemmemberShadow::RemmemberShadow()
:_object(NULL),
_lightDir(NoInit),_splitOnly(false)
{
}

void RemmemberShadow::Init
(
	Object *object, Vector3Par lightDir,
	int level, Matrix4Par pos, bool splitOnly
)
{
	// splitOnly is used for object that should be split
	// but not shadow projected
	// such shapes also preserve original y offset
	_object=object;
	//_coordinates=*object;
	_lightDir=lightDir;
	_objectPos = pos;
	_level=level;
	_lastUsed=Glob.time;
	_splitOnly = splitOnly;
	if (splitOnly)
	{
		// copy source data
		_shadow = new Shape(*object->GetShape()->Level(level),false);
	}
	else
	{
		// retainStructure = false -> force duplicating source data
		_shadow = object->RecalcShadow(level,*object,false);
	}
	if (_shadow)
	{
		_shadow->SurfaceSplit
		(
			GScene->GetLandscape(),object->Transform(),
			GLOB_ENGINE->ZShadowEpsilon(),splitOnly ? object->Scale() : 0
		);
		// it is already fitted - prevent future surface fitting
		_shadow->CalculateMinMax();
		_shadow->AndSpecial(~OnSurface);
		_shadow->OrSpecial(IsOnSurface);
		// make sure buffer is regenerated
		_shadow->ReleaseVBuffer();
		// optimize it for HW rendering
		_shadow->FindSections();
		if (_shadow->NVertex()>1024)
		{
			_shadow->ConvertToVBuffer(VBBigDiscardable);
		}
		else
		{
			_shadow->ConvertToVBuffer(VBSmallDiscardable);
		}

		// table should be exact - for best memory usage
		_shadow->Compact();
	}
}

bool RemmemberShadow::IsShadow( Object *object, int level ) const
{
	if( object!=_object ) return false;
	if( level<0 ) return true;
	if( level!=_level ) return false;
	return true;
}

DEFINE_FAST_ALLOCATOR(ShadowIndex)

void Object::RemoveAllShadows()
{
	_shadow.Free();
}

RemmemberShadow *Object::GetShadow(int level) const
{
	if (!_shadow) return NULL;
	return _shadow->_lods[level];
}

void Object::RemoveShadow(int level)
{
	#if !_RELEASE
	if (!_shadow->_lods[level])
	{
		Fail("Removed non-existing shadow");
	}
	#endif
	_shadow->_lods[level] = NULL;
	if (--_shadow->_nShadows<=0)
	{
		// last shadow removed from the index
		_shadow.Free();
	}
}

void Object::SetShadow(int level, RemmemberShadow *shadow)
{
	if (!_shadow)
	{
		_shadow = new ShadowIndex;
		_shadow->_nShadows = 0;
	}
	if (!_shadow->_lods[level])
	{
		_shadow->_nShadows++;
	}
	_shadow->_lods[level] = shadow;
}


ShadowCache::ShadowCache()
{
	RegisterMemoryFreeOnDemand(this);
}

ShadowCache::~ShadowCache()
{}


Ref<Shape> ShadowCache::Shadow
(
	Object *object, Vector3Par lightDir,
	int level, Matrix4Par pos, bool splitOnly
)
{
	int count=0;

	RemmemberShadow *si = object->GetShadow(level);

	if (si)
	{
		const float maxLDDiff=0.01f;
		const float maxPosDiff=0.02f;
		// check 
		DoAssert (si->_splitOnly==splitOnly);
		// check if the light or object position has not changed
		if
		(
			!splitOnly && lightDir.Distance2(si->LightDir())>Square(maxLDDiff)
			|| pos.Distance2(si->ObjectPos())>Square(maxPosDiff)
		)
		{
			// both static and dynamic object shadows are cached
			si->Init(object,lightDir,level,pos,splitOnly);
			//LogF("UPDATE shadow %s %d",(const char *)object->GetDebugName(),level);
		}
		// reorder shadow cache
		Ref<RemmemberShadow> temp=si;
		_data.Delete(si);
		_data.Insert(si);
		si->_lastUsed = Glob.time;
	}
	else
	{
		// a new shadow
		// _data is sorted by time when it was used
		Ref<RemmemberShadow> entry;
		int maxShadows = toLargeInt(Square(Glob.config.horizontZ)*(0.5/900));
		saturate(maxShadows,512,4096);
		if( count>=maxShadows )
		{
			// remove last entry, mark as removed
			entry=_data.Last();
			entry->GetObject()->RemoveShadow(entry->_level);
			_data.Delete(entry);
		}
		else
		{
			entry=new RemmemberShadow;
		}
		entry->Init(object,lightDir,level,pos,splitOnly);
		// create a new entry
		_data.Insert(entry);
		si = entry;
	}
	object->SetShadow(level,si);

	return si->_shadow;
}

void ShadowCache::CleanUp()
{
	// old entries are listed at the end
	for( RemmemberShadow *entry=_data.Last(); entry;  )
	{
		RemmemberShadow *next=_data.Prev(entry);
		if( !entry->GetObject() )
		{
			_data.Delete(entry);
		}
		else if( entry->_lastUsed<Glob.time-60 )
		{
			entry->GetObject()->RemoveShadow(entry->_level);
			_data.Delete(entry);
		}
		else break;
		entry = next;
	}
}

const float ShadowMemSize = 10*1024;

size_t ShadowCache::FreeOneItem()
{
	RemmemberShadow *entry=_data.Last();
	if (!entry) return 0;

	Object *obj = entry->GetObject();
	if (obj)
	{
		obj->RemoveShadow(entry->_level);
	}
	_data.Delete(entry);

	return (size_t)ShadowMemSize;
}

float ShadowCache::Priority()
{
	// estimated time to free LandSegment (CPU cycles)
	const float itemTime = 10000;
	// estimated time per byte
	return itemTime/ShadowMemSize;
}


void ShadowCache::ShadowChanged(Object *obj)
{
	// remove all shadows cached for this object
	ShadowIndex *index = obj->GetShadowIndex();
	if (!index) return;
	for (int i=0; i<MAX_LOD_LEVELS; i++)
	{
		RemmemberShadow *entry = index->_lods[i];
		if (!entry) continue;
		Assert(entry->_object==obj);
		_data.Delete(entry);
	}
	obj->RemoveAllShadows();

}

void ShadowCache::Clear()
{
	for( RemmemberShadow *sc=_data.First(); sc; sc=_data.Next(sc) )
	{
		Object *obj = sc->GetObject();
		Shape *sh = sc->_shadow;
		if (obj)
		{
			obj->RemoveShadow(sc->_level);
		}
		if (sh) sh->ReleaseVBuffer();
	}

	_data.Clear();
	// clear shadow indices for all objects in landscape
	#if !_RELEASE
	for( int x=0; x<LandRange; x++ ) for( int z=0; z<LandRange; z++ )
	{
		const ObjectList &list=GLandscape->GetObjects(z,x);
		for( int o=0; o<list.Size(); o++ )
		{
			Object *obj=list[o];
			if(obj->GetShadowIndex())
			{
				LogF("Late ShadowIndex release - %s",(const char *)obj->GetDebugName());
				obj->RemoveAllShadows();
			}
		}
	}
	#endif
}

Ref<Shape> Object::RecalcShadow
(
	int level, const FrameBase &frame,
	bool retainStructure,
	LODShapeWithShadow *forceShape
)
{
	// all polygons cast shadows
	// project all points to ground level
	int i;
	Matrix4Val iTrans=frame.GetInvTransform();
	float y=GEngine->ZShadowEpsilon();

	Ref<LODShape> shadow = NULL;
	if (!forceShape) forceShape= _shape;
	if (retainStructure)
	{
		// note: _shape may be used only when retainStructure is true
		forceShape->CreateShadow();
		shadow = forceShape->Shadow();
	}
	else
	{
		shadow = forceShape->MakeShadow();
	}

	if (!shadow) return NULL;
	Ref<Shape> shape = shadow->Level(level);
	Shape *orig = forceShape->Level(level);
	if( shape && shape->NFaces()>=0 )
	{

		Vector3Val lightDir=GScene->MainLight()->ShadowDirection();
		Vector3 modelLightDir=iTrans.Rotate(lightDir);
		if
		(
			(orig->GetAndHints()&ClipDecalMask)==ClipDecalVertical &&
			(orig->GetOrHints()&ClipDecalMask)==ClipDecalVertical
		)
		{
			// special case - shadow casted by vertical decal
			LogF("Vertical decal shadow obsolete");
			return NULL;
		}
		else
		{
			FaceArray dest;
			if (!retainStructure)
			{
				dest.ReserveFaces(1024,false);
				// reserve same size as source
				dest.ReserveRaw(shape->Faces().RawSize());
			}
			// make sure offsets in both shapes match
			#if 0
			if (shape->NFaces()!=orig->NFaces())
			{
				RptF("Bad shadow (face count): %s",(const char *)GetDebugName());
				return false;
			}
			if (shape->Faces().RawSize()!=orig->Faces().RawSize())
			{
				RptF("Bad shadow (face size): %s",(const char *)GetDebugName());
				return false;
			}
			#endif

			for( Offset f=shape->BeginFaces(),e=shape->EndFaces(); f<e; shape->NextFace(f) )
			{
				const Poly &sFace=orig->Face(f);
				Poly &face=shape->Face(f);
				// make sure both sFace and face are valid face pointers
				if( face.N()<3 ) continue;
				if( (sFace.Special()&(NoShadow|IsHidden|IsHiddenProxy)) )
				{
					face.OrSpecial(ShadowDisabled);
				}
				else
				{
					// world corrdinates of face vertices
					Assert( face.GetVertex(0)<orig->NVertex() );
					Assert( face.GetVertex(1)<orig->NVertex() );
					Assert( face.GetVertex(2)<orig->NVertex() );
					Vector3Val v0=orig->Pos(face.GetVertex(0));
					Vector3Val v1=orig->Pos(face.GetVertex(1));
					Vector3Val v2=orig->Pos(face.GetVertex(2));
					Vector3Val v10 = v1-v0;
					Vector3 normal=v10.CrossProduct(v2-v0);
					float check=modelLightDir*normal;
					if( check>0 )
					{
						// drop face
						face.OrSpecial(ShadowDisabled);
					}
					else
					{
						face.AndSpecial(~ShadowDisabled);
						if( face.Special()&(IsAlpha|IsTransparent) ) face.SetTexture(orig->Face(f).GetTexture());
						else face.SetTexture(NULL);
						if (!retainStructure)
						{
							// face should be included in shadow - add it to destination storage
							dest.Add(face);
						}
					}
				}
			}
			bool ok = true;
			for( i=0; i<shape->NPos(); i++ )
			{
				// calculate world space position of shadow
				Vector3Val origPos=orig->Pos(i);
				Vector3 pPos(VFastTransform,frame.Transform(),origPos);
				Vector3 shadowPos;
				if (!GScene->ShadowPos(pPos,shadowPos,GScene->MainLight()))
				{
					ok = false;
				}
				shadowPos[1]+=y;
				V3 &sPos=shape->SetPos(i);
				sPos.SetFastTransform(iTrans,shadowPos);
				shape->SetClip(i,ClipLandOn|ClipAll);
			}
			if (!ok) return NULL;
			if (!retainStructure)
			{
				// move destination storage to shadow faces
				shape->SetFaces(dest);
			}
		}
		shadow->SetAutoCenter(false);
		shape->CalculateMinMax();
		shadow->CalculateMinMax(false);
		if( shape->Max().Distance2(shape->Min())>=(100*100) ) shape = NULL;
	}
	return shape;
}

LODShape *LODShape::MakeShadow()
{
	// note shadow shape structure is constant
	// changing are positions and ShadowDisabled flag of any face
	//
	if( Special()&NoShadow ) return NULL; // no shadow
	// do not copy animation for shadow shapes
	LODShape *shadow=new LODShape(*this,false);
	// all polygons cast shadows
	for( int level=0; level<shadow->NLevels(); level++ )
	{
		Shape *shape=shadow->Level(level);
		if( !shape || shape->FindProperty("lodnoshadow")>=0 || Resolution(level)>900 )
		{
			shadow->_lods[level]=NULL;
			continue; // this LOD is never used for shadows
		}
		if( shape->NFaces()==0 )
		{
			shadow->_lods[level]=NULL;
			continue;
		}

		if (shape->NFaces()>512 || shape->NVertex()>4096)
		{
			RptF
			(
				"%s: very complex shadow %d,%d (LOD %d:%.3f)",
				(const char *)Name(),
				shape->NFaces(),shape->NVertex(),
				level,Resolution(level)
			);
			shadow->_lods[level]=NULL;
			continue;
		}
		for( Offset dst=shape->BeginFaces(); dst<shape->EndFaces(); shape->NextFace(dst) )
		{
			Poly &poly=shape->Face(dst);
			if( (poly.Special()&(NoShadow|IsHidden|IsHiddenProxy))==0 )
			{
				if (!(poly.Special()&(IsAlpha|IsTransparent)))
				{
					poly.SetTexture(NULL);
					poly.SetSpecial
					(
						OnSurface|IsShadow|NoZWrite|IsAlphaFog|
						ClampU|ClampV
					);
				}
				else
				{
					#if _ENABLE_PERFLOG
					extern bool LogStatesOnce;
					if (LogStatesOnce)
					{
						LogF("-- Shadow texture %s",(const char *)poly.GetDebugText());
					}
					#endif
					poly.SetSpecial
					(
						OnSurface|IsShadow|NoZWrite|IsAlphaFog|
						poly.Special()&(NoClamp|ClampU|ClampV|IsAnimated|IsAlpha|IsTransparent)
					);
				}
			} // if( face has shadow )
		} // for(  all faces )
		//shape->_face.Resize(dst);
		shape->SetHints(0,0);
		//shape->_face._sections.Clear();
	}
	shadow->OrSpecial(OnSurface|IsShadow|IsAlphaFog);
	shadow->_boundingCenter = VZero;
	return shadow;
}

void LODShapeWithShadow::CreateShadow()
{
	if( !_shadow )
	{
		_shadow=MakeShadow();
	}
	//_shadow=MakeShadow(scene);
}

// import from Object.cpp, TODO: move to header object.hpp
bool NoFogNeeded( float dist2, const Shape *shape, float bRadius );

Ref<Shape> Object::PrepareShadow
(
	int level, Vector3Par shadowPos, const FrameBase &frame
)
{
	Ref<Shape> ret;
	// shadows can be less detailed than objects
	Assert( level>=0 );

	// prepare shadow for drawing
	bool someAnim=IsAnimatedShadow(level);
	if( someAnim || !_static )
	{
		// do not cache shadows of dynamic objects
		// if there is some animated texture
		// it must be animated even in the shadow object
		Animate(level);
		// note: object may be destroyed
		// in that case we need destroyed shadow?
		ret = RecalcShadow(level,frame,true);
		Deanimate(level);
	}
	else
	{
		// if there exists pre-calculated shadow, use it
		Matrix4 pos;
		pos.SetOrientation(M3Identity);
		pos.SetPosition(shadowPos);
		ret = GScene->GetShadowCache().Shadow
		(
			this,GScene->MainLight()->ShadowDirection(),
			level,pos
		);
	}
	return ret;
}

extern bool EnableHWTLState;

void Object::DrawShadow
(
	Shape *shadow,
	Vector3Par shadowPos, ClipFlags clipFlags, const FrameBase &frame
)
{
	int spec=IsOnSurface|IsShadow|IsAlphaFog;

	if (GEngine->GetTL() && EnableHWTLState && shadow->_buffer && !(spec&OnSurface))
	{
		#if _ENABLE_PERFLOG
		extern bool LogStatesOnce;
		if (LogStatesOnce)
		{
			LogF("Draw TL shadow %s",_shape->Name());
		}
		#endif
		// T&L shadow drawing
		// turn off all lights
		GEngine->EnableSunLight(false);
		// prepare matrices
		const LightList noLights;
		GEngine->PrepareMeshTL(noLights,frame.Transform(),spec);
		// set z-bias
		GEngine->SetBias(0x10);

		if (shadow->NFaces()>0 && shadow->NSections()>0)
		{
			ADD_COUNTER(poly,shadow->NFaces());
			ADD_COUNTER(TLd3d,shadow->NFaces());

			// set shadow material
			TLMaterial shadowMat;

			if (spec&OnSurface)
			{
				// we may need to split the shape

			}
			float shadowFactor = GEngine->GetShadowFactor()*(1.0/256);

			shadowMat.diffuse = Color(0,0,0,shadowFactor);
			shadowMat.ambient = Color(0,0,0,shadowFactor);
			shadowMat.emmisive = HBlack;
			shadowMat.forcedDiffuse = HBlack;
			shadowMat.specFlags = 0;
			LightList empty;
			GEngine->SetMaterial(shadowMat,empty,0);

			GEngine->BeginMeshTL(*shadow,spec);
			// check first face properties
			// note: we need alpha set-up properly
			/*
			for (int i=0; i<shadow->NSections(); i++)
			{
				const ShapeSection &sec = shadow->GetSection(i);
				if (sec.properties.Special()&(IsHidden|IsHiddenProxy|ShadowDisabled)) continue;

				Texture *texture=sec.properties.GetTexture();
				int spec=sec.properties.Special();
				sec.properties.PrepareTL(texture,spec);
				// draw whole index buffer
				GEngine->DrawSectionTL(*shadow,i);
			}
			*/

			int secBeg = -1;
			int secEnd = -1;
			Texture *secTexture = (Texture *)-1;
			int secSpecial = -1;

			for (int i=0; i<shadow->NSections(); i++)
			{
				const ShapeSection &sec = shadow->GetSection(i);
				if (sec.properties.Special()&(IsHidden|IsHiddenProxy)) continue;

				if (secBeg<0)
				{
					secBeg = i;
					secEnd = i+1;
					secTexture = sec.properties.GetTexture();
					secSpecial = sec.properties.Special();
				}
				else if
				(
					sec.properties.GetTexture()==secTexture &&
					sec.properties.Special()==secSpecial &&
					i==secEnd
				)
				{
					// extend section
					secEnd=i+1;
				}
				else
				{
					// flush section
					shadow->GetSection(secBeg).properties.PrepareTL();
					GEngine->DrawSectionTL(*shadow,secBeg,secEnd);
					// open another section
					secBeg = i;
					secEnd = i+1;
					secTexture = sec.properties.GetTexture();
					secSpecial = sec.properties.Special();
				}


			}
			if (secEnd>secBeg)
			{
				// flush section
				shadow->GetSection(secBeg).properties.PrepareTL();
				GEngine->DrawSectionTL(*shadow,secBeg,secEnd);
			}

			GEngine->EndMeshTL(*shadow);
		}

		return;
	}

	
	// calculate point transformation
	//Matrix4Val pointView=GScene->ScaledInvTransform()*frame.Transform();
	Matrix4Val pointView=GScene->ScaledInvTransform()*frame.Transform();

	// note: some engines need not shadow counting
	int nDrawn=0;
	// single-object shadows must not overlay
	Shape *shape=shadow;
	if( shape ) for( Offset i=shape->BeginFaces(); i<shape->EndFaces(); shape->NextFace(i) )
	{
		const Poly &face=shape->Face(i);
		if( face.N()<3 ) continue;
		if( face.Special()&(NoShadow|ShadowDisabled|IsHidden|IsHiddenProxy) ) continue;
		nDrawn++;
	}

	float bias=0x100;
	float biasStep=bias/nDrawn;
	const int maxBiasStep=4;
	if( biasStep>maxBiasStep ) biasStep=maxBiasStep,bias=biasStep*nDrawn;

	if( shape && shape->NFaces()>0 )
	{

		ADD_COUNTER(obj,1);

		#if _ENABLE_PERFLOG
		extern bool LogStatesOnce;
		if (LogStatesOnce)
		{
			LogF
			(
				"Draw shadow %s",_shape->Name()
			);
		}
		#endif
		TLVertexTable tlTable(this,*shape,pointView);

		ClipFlags clipAnd=clipFlags;
		ClipFlags orClip=clipFlags;
		const Camera &cam = *GScene->GetCamera();
		orClip&=tlTable.CheckClipping(cam,clipFlags,clipAnd);

		if( clipAnd ) goto Break;
		
		LightList noLights;
		int special=IsShadow|IsAlphaFog;
		// many object are not fogged at all
		// disable fog calculation for them
		float rDist2 = GScene->GetCamera()->Position().Distance2(Position());
		//if( NoFogNeeded(rDist2,shape,_shape->Shadow()->BoundingSphere()) ) special|=FogDisabled;
		// estimate bounding sphere
		float bRadius = shape->Min().Distance(shape->Max())*0.5; 
		//if( NoFogNeeded(rDist2,shape,_shape->Shadow()->BoundingSphere()) ) special|=FogDisabled;
		if (NoFogNeeded(rDist2,shape,bRadius)) special|=FogDisabled;
		tlTable.DoLighting(this,frame.GetInvTransform(),noLights,*shape,special);

		FaceArray clippedFaces(0,false);
		//if( spec&(OnSurface|AboveSurface|UnderSurface) )

		if( spec&OnSurface )
		{
			//float y=( (spec&IsShadow) ? engine->ZShadowEpsilon() : engine->ZRoadEpsilon() );
			float y=GEngine->ZShadowEpsilon();
			// surface split may need to clip faces that were unclipped before
			clippedFaces.SurfaceSplit(shape->_face,tlTable,*GScene,orClip,y);
			// shadow aproximate clipping is often invalid
			orClip=ClipAll;
		}
		else
		{
			clippedFaces.Clip(shape->_face,tlTable,*GScene->GetCamera(),orClip);
		}
		if( clippedFaces.Begin()<clippedFaces.End() )
		{
			GEngine->PrepareMesh(spec);
			GEngine->SetBias(0x20); // set default bias for stencil buffer implementation
			//GEngine->SetBias(0x12); // set default bias for stencil buffer implementation
			tlTable.DoPerspective(*GScene->GetCamera(),orClip);
			GEngine->BeginMesh(tlTable,spec);
			Texture *lastTexture=NULL;
			int lastSpec=-1;


			bool biasExclusion = GEngine->ZBiasExclusion();

			for( Offset si=clippedFaces.Begin(); si<clippedFaces.End(); clippedFaces.Next(si) )
			{
				const Poly &face=clippedFaces[si];
				if( face.N()<3 ) continue;
				int spec=face.Special();
				if( spec&(NoShadow|ShadowDisabled|IsHidden|IsHiddenProxy) ) continue;
				if (biasExclusion)
				{
					int iBias=toIntCeil(bias);
					if( iBias<=0 ) iBias=1;
					GEngine->SetBias(iBias);
					bias-=biasStep;
				}

				// TODO: optimize
				Texture *texture=face.GetTexture();
				if( texture!=lastTexture || spec!=lastSpec )
				{
					lastSpec=spec;
					lastTexture=texture;
					// shadows are only near - probably need good textures
					face.Prepare(texture,spec);
				}

				GEngine->DrawPolygon(face.GetVertexList(),face.N());
				//face.DrawNoClip(tlTable);
			}
			GEngine->EndMesh(tlTable);

		}
	}

	Break:

	int idleMs=GEngine->HowLongIdle();
	if( idleMs>=0 && GLOB_WORLD ) GLOB_WORLD->PrimaryAllowSwitch(idleMs);
}

template Ref<Light>;
