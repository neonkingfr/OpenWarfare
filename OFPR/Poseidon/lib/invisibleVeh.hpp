#ifdef _MSC_VER
#pragma once
#endif

#ifndef _INVISIBLE_VEH_HPP
#define _INVISIBLE_VEH_HPP

#include "vehicleAI.hpp"

class InvisibleVehicleType: public VehicleType
{
	typedef VehicleType base;

	friend class InvisibleVehicle;

	public:
	InvisibleVehicleType( const ParamEntry *param );
	virtual void Load(const ParamEntry &par);
	void InitShape();
};

class InvisibleVehicle: public Person
{
	typedef Person base;

	public:
	const InvisibleVehicleType *Type() const
	{
		return static_cast<const InvisibleVehicleType *>(GetType());
	}

	InvisibleVehicle( VehicleType *name );
	~InvisibleVehicle();

	bool Invisible() const {return true;}
	
	void DrawDiags();

	void ResetMovement( float speed, int action) {}

	void Simulate(float deltaT, SimulationImportance prec);
};

#endif

