// global redefiniton of new, delete operators
#include "wpch.hpp"

#pragma optimize("t",on)

//#include "global.hpp"
#include <Es/Common/win.h>

#include "MemGrow.hpp"
#include "MemHeap.hpp"

//#include "engineDll.hpp"

#include <Es/Memory/normalNew.hpp>

#if PROFILE
	#define CHECK 0
	#define DO_STATS 0
#elif _DEBUG
	#define CHECK 1
	#define DO_STATS 0
#elif _RELEASE
	#define CHECK 0
	#define DO_STATS 0
#else
	// testing
	#define CHECK 1
	#define DO_STATS 1
#endif


#if _MSC_VER && !defined INIT_SEG_COMPILER
	// we want Memory Heap to deallocate last
	#pragma warning(disable:4074)
	#pragma init_seg(compiler)
	#define INIT_SEG_COMPILER
#endif

//{ useAppFrameExt.cpp is embedded here:
//  - LogF is used in MemHeap construction, so it must be defined in compiler init_seg

#include "appFrameExt.hpp"
static OFPFrameFunctions GOFPFrameFunctions INIT_PRIORITY_URGENT;
AppFrameFunctions *CurrentAppFrameFunctions = &GOFPFrameFunctions;

//} End of embedded useAppFrameExt.cpp

#ifndef MFC_NEW

#if CHECK || DO_STATS
#include "memCheck.hpp"
#endif

#if DO_STATS
	// check new call point histogram
	#include "statistics.hpp"
	#include "keyInput.hpp"
	#include "dikCodes.h"

	static StatisticsByName MemCntStats INIT_PRIORITY_URGENT;
	static StatisticsByName MemTotStats INIT_PRIORITY_URGENT;
	static int MemStatsDisabled=0; // avoid recursion

	void MemoryFootprint()
	{
		//MemTotStats.Clear();
		// scan all allocated blocks
		for
		(
			MemoryInfo *info=Allocated.First();
			info;
			info=Allocated.Next(info)
		)
		{
			char buf[128];
			_snprintf(buf,sizeof(buf),"%s(%d): S",info->File(),info->Line());
			buf[sizeof(buf)-1]=0;
			MemTotStats.Count(buf,info->Size());
		}
		MemTotStats.Sample();
	}

	void CountNew( const char *file, int line, int size )
	{
		if( MemStatsDisabled>0 ) return;
		MemStatsDisabled++;
		char buf[128];
		_snprintf(buf,sizeof(buf),"%s(%d): C",file,line);
		buf[sizeof(buf)-1]=0;
		MemCntStats.Count(buf);
		// scan all alocated blocks and add them to memory footprint
		MemStatsDisabled--;
	}
	void ReportMemory()
	{
		LogF("Allocation footprint report (%d samples)",MemTotStats.NSamples());
		LogF("-----------------------");
		MemTotStats.Report();
		LogF("Allocation count report");
		LogF("-----------------------");
		MemCntStats.Report();
	}
#else
	void MemoryFootprint(){}
	#define ReportMemory() GMemFunctions->ReportMemory(){}

	#define CountNew(file,line,size)
#endif


static bool MemoryErrorState = false;

void MemoryErrorReported()
{
	MemoryErrorState = true;
}

void MemoryError( int size );


// manage memory pool
// memory pool is reserved when application is started
// and commited as necessary

// maybe we could maintain order of free blocks
// by address (this will speed-up Free) (est. 2x)
// by size (this will speed-up Alloc) (est. 2x)

int MemoryFreeBlocks();

class MemHeap;

//extern MemHeap *SMemPtr;
extern MemHeap *BMemPtr;


#if INDIRECT

typedef FreeBlock *HeaderType;

#else

typedef FreeBlock HeaderType;

#endif

//////////////////////////////////////////////////////////

InfoAlloc::InfoAlloc(size_t n)
:esize( n<sizeof(Link*) ? sizeof(Link*) : n ),
head(0),
chunks(NULL)
{
	//#ifdef _KNI
	//const int alignMem=16;
	//#else
	const int alignMem=esize>=16 ? 16 : 8;
	//#endif
	esize=(esize+alignMem-1)&~(alignMem-1);
}

InfoAlloc::~InfoAlloc()
{
	// check if all chunks may be freed now
}

#if !_RELEASE
	#define IA_DIAGS 1
#endif

#define NO_ALLOCATION_AFTER_ERROR 0

InfoAlloc::Chunk *InfoAlloc::NewChunk()
{
	//LogF("Allocated Info blocks: %d",allocated);
	#if NO_ALLOCATION_AFTER_ERROR
	if (MemoryErrorState)
	{
		return NULL;
	}
	#endif
#ifdef _WIN32
	return (Chunk *)GlobalAlloc(GMEM_FIXED,sizeof(Chunk));
#else
	return (Chunk *)malloc(sizeof(Chunk));
#endif
}

void InfoAlloc::DeleteChunk( InfoAlloc::Chunk *chunk )
{
#ifdef _WIN32
	GlobalFree((HGLOBAL)chunk);
#else
	free(chunk);
#endif
}

void* InfoAlloc::Alloc( size_t n )
{

	Assert( n<=esize );
	if( head==0 ) Grow();
	Link *p=head;

	head=p->next;

	// mark chunk to link
	return p;
}

void InfoAlloc::Free( void *pAlloc )
{
	Link *p=static_cast<Link*>(pAlloc);
	//p--; // link is skipped
	p->next=head;
	head=p;

	// check if all chunks may be freed now
}


void InfoAlloc::Grow()
{
	//Chunk *n=new Chunk; // use global new
	//Chunk *n=(Chunk *)ChunkHeap.Alloc(sizeof(Chunk)); // use chunk new
	Chunk *n=NewChunk(); // use chunk new

	n->next=chunks;
	chunks=n;

	char *start=n->mem;

	int chSize = Chunk::size;
	const int nelem=chSize/esize;

	char *last=&start[(nelem-1)*esize];
	for( char*p = start; p<last; p+=esize )
	{
		Link *link = reinterpret_cast<Link*>(p);

		link->next=reinterpret_cast<Link*>(p+esize);
		link->chunk=n;
	}
	Link *lastLink = reinterpret_cast<Link*>(last);
	lastLink->next=0;
	lastLink->chunk=n;
	head=reinterpret_cast<Link*>(start);
}


void InfoAlloc::FreeChunks()
{

	#if IA_DIAGS
		int size=0;
	#endif
	// remove all chunks
	Chunk *n=chunks;
	while( n )
	{
		Chunk *p=n;
		n=n->next;
		// TODO: perform smart DeleteChunk when chunk is empty
		DeleteChunk(p); // use chunk delete
		#if IA_DIAGS
			size+=sizeof(*p);
		#endif
	}
	chunks=NULL;
	head=NULL;

	#if IA_DIAGS
		LogF("InfoAlloc chunks - %d KB (%d)",(size+1023)/1024,esize);
	#endif

}


#define VERIFY_BLOCKS 0


#if !VERIFY_BLOCKS
#define VerifyStructure()
#endif

#if DO_STATS || !defined _MSC_VER

// memory allocated by smallAlloc would appear in statistics twice
// because Alloc calls Chunk::new to alloc Chunks
#define SMALL_ALLOC 0

#else

#define SMALL_ALLOC 1

#endif

// find corresponding free list


void MemHeap::DoConstruct(MemHeap **lateDestructPointer)
{
	InitFastAllocs();
	_name = "No";
	_lateDestruct = lateDestructPointer;
}

void MemHeap::DoConstruct
(
	const char *name, MemSize size, MemSize align,
	MemHeap **lateDestructPointer
)
{
	InitFastAllocs();

	LogF("MemHeap %s constructed (%d)",name,size);
	_name = name;

	_destruct=false;

	_align=align;
	size=AlignSize(size);
	_minAlloc=AlignSize(_minAlloc);
	// pool must be aligned
	_memBlocks.Reserve(size);

	char *startFree=(char *)_memBlocks.Data();
	int sizeFree=_memBlocks.Size();

	LogF
	(
		"Size of FreeBlock is %d, BusyBlock %d, align is %d",
		sizeof(FreeBlock),sizeof(BusyBlock),_align
	);
	LogF
	(
		"Minimal allocation size %d",_minAlloc
	);
	int skip=_align-sizeof(HeaderType);
	if( skip>0 && skip!=_align )
	{
		// there will be MemItem at the beginning of each allocated block
		// this could misalign data
		startFree+=skip;
		sizeFree-=skip;
		LogF("Start aligned to %x (skip %d)",startFree,skip);
	}

	#if INDIRECT
	FreeBlock *allFree = new(_infoAlloc) FreeBlock;
	allFree->_memory = startFree;
	#else
	_memBlocks.Commit(sizeof(FreeBlock));
	FreeBlock *allFree=(FreeBlock *)startFree;
	#endif
	
	_freeList.Insert(allFree);
	_blockList.Insert(allFree);

	_nItemsFree=1;
	_lateDestruct = lateDestructPointer;
}

void MemHeap::DoDestruct()
{
	DestructFastAllocs();

	LogF("Destruct memory heap %s",_name);
	// all memory shoule be dallocated by now

	_blockList.Clear();
	// clear all free lists
	_freeList.Clear();
	//_freeList256.Clear();
	_memBlocks.Clear();
	_infoAlloc.Destruct();
	//_memTable.Clear();
	_nItemsFree=0;
	//SMemPtr=NULL; // nothing to diagnose - memory is gone
	if (_lateDestruct)
	{
		Assert (_lateDestruct==&BMemPtr);
		*_lateDestruct = NULL; // nothing to diagnose - memory is gone
		Log("**** All memory released - memory checker deinitialized *****");
	}
}

#include "statistics.hpp"

void MemHeap::LogAllocStats() const
{
	#if INDIRECT
	StatisticsByName stats;
	for( BusyBlock *block=_blockList.First(); block; block=_blockList.Next(block) )
	{
		if (block->IsFree()) continue;
		int size= BlockSize(block);

		char name[256];
		sprintf(name,"%08d",size);
		stats.Count(name,1);
	}
	stats.Report();
	#endif

}

#if VERIFY_BLOCKS

void MemHeap::VerifyStructure()
{
	#if 1
		for( BusyBlock *block=_blockList.First(); block; block=_blockList.Next(block) )
		{
			Assert( block->CLTLink<1>::next->prev==block );
			Assert( block->CLTLink<1>::prev->next==block );
			BusyBlock *next=_blockList.Next(block);
			DoAssert( block->_size>=sizeof(BusyBlock) || block->_size<=-sizeof(BusyBlock) );
			if( next )
			{
				void *end=block->EndAny();
				DoAssert( next==end );
			}
		}
	#endif
	#if 1
		_freeListXXX.VerifyStructure();
		//_freeList256.VerifyStructure();
	#endif
}


void MemHeap::VerifyBlock( BusyBlock *busy )
{
	for( BusyBlock *block=_blockList.First(); block; block=_blockList.Next(block) )
	{
		if( block==busy ) return;
	}
	Fail("Bad block");
}

bool FreeList::VerifyFreeBlock( FreeBlock *block )
{
	for( FreeBlock *free=First(); free; free=Next(free) )
	{
		if( block==free ) return true;
	}
	return false;
}


void FreeList::VerifyStructure()
{
	for( FreeBlock *free=First(); free; free=Next(free) )
	{
		// verify blockList chaing
		DoAssert( free->CLTLink<1>::next->prev==free );
		DoAssert( free->CLTLink<1>::prev->next==free );
		//DoAssert( free->TLink<2>::next->prev==free );
		//DoAssert( free->TLink<2>::prev->next==free );
		FreeBlock *next=Next(free);
		Assert( free->IsFree() );
		DoAssert( free->_size>=sizeof(FreeBlock) );
	}
}

void MemHeap::VerifyFreeBlock( FreeBlock *block )
{
	#if VERIFY_BLOCKS
	if( _freeListXXX.VerifyFreeBlock(block) ) return;
	//if( _freeList256.VerifyFreeBlock(block) ) return;
	Fail("Bad free block");
	#endif
}

#endif


//#pragma optimize ("a",off)

// Alloc and Free are used only within operator new/delete
// inlining avoids using 1:1 call

#if _RELEASE && _ENABLE_PERFLOG
	#define MEM_PERF_LOG 1
#endif

#if MEM_PERF_LOG
	#include <El/Common/perfLog.hpp>
	#include "perfProf.hpp"
#endif

MemHeap::~MemHeap()
{
	DoDestruct();
}

size_t MemHeap::FreeOnDemand(size_t size)
{
	return _freeOnDemand.Free(size);
}

size_t MemHeap::FreeOnDemandAll()
{
	return _freeOnDemand.FreeAll();
}

void MemHeap::RegisterFreeOnDemand(IMemoryFreeOnDemand *object)
{
	_freeOnDemand.Register(object);
}

FreeBlock *MemHeap::FindBest( MemSize size )
{
	FreeBlock *best=NULL;
	// note: a nice trick to save one conditional jump
	//if( diff>=0 && diff<bestDiff )
	//if( diff+INT_MIN>=INT_MIN && diff+INT_MIN<bestDiff+INT_MIN )
	// same as above, unroll loop by 4
	size+=(MemSize)INT_MIN;
	int bestDiff=INT_MAX+INT_MIN;
	//for( FreeBlock *free=_freeList.First(); free; free=_freeList.Next(free) )
	for
	(
		FreeBlock *free=_freeList.Start();
		//freeList.NotEnd(free);
		NotEndOf(FreeLink,free,_freeList);
		free=_freeList.Advance(free)
	)
	{
		int freeSize=BlockSize(free);
		int diff=freeSize-size; // GetFreeSize
		if( diff<bestDiff ) bestDiff=diff,best=free; //,bestPrev=prev;
	}
	return best;
}

#if SMALL_ALLOC


const int maxSmallAllocSize=256;


class MemoAlloc: public FastAlloc
{
	MemHeap *_heap;

	public:
	MemoAlloc( MemHeap *heap, size_t n ):FastAlloc(n,"mem",12),_heap(heap){}
	~MemoAlloc(){}

	void *operator new( size_t size ) {return malloc(size);}
	void operator delete( void *mem ) {free(mem);}

	virtual Chunk *NewChunk();
	virtual void DeleteChunk( Chunk *chunk );
};

MemoAlloc::Chunk *MemoAlloc::NewChunk()
{
	return (Chunk *)_heap->Alloc(sizeof(Chunk));
}

void MemoAlloc::DeleteChunk( MemoAlloc::Chunk *chunk )
{
	_heap->Free(chunk);
}

void MemHeap::InitFastAllocs()
{
	for( int i=0; i<nAllocSlots; i++ )
	{
		_smallAllocs[i]=new MemoAlloc(this,allocSizes[i]);
		_allocStats[i]=0;
	}
}

void MemHeap::DestructFastAllocs()
{
	for( int i=0; i<nAllocSlots; i++ )
	{
		_smallAllocs[i]->~MemoAlloc();
	}
}

#else

void MemHeap::InitFastAllocs() {}

void MemHeap::DestructFastAllocs() {}

#endif

#if SMALL_ALLOC
const int DefaultMinAlloc=maxSmallAllocSize;
#else
const int DefaultMinAlloc=32;
#endif

#if defined USE_MALLOC && USE_MALLOC
#error malloc allocation is used - using MemTable makes no sense.
#endif

MemHeap::MemHeap
(
	const char *name, MemSize size, MemSize align,
	MemHeap **lateDestructPointer
)
:_infoAlloc(sizeof(BusyBlock))
{
	_minAlloc=DefaultMinAlloc;
	DoConstruct(name,size,align,lateDestructPointer);
}
MemHeap::MemHeap(MemHeap **lateDestructPointer)
:_infoAlloc(sizeof(BusyBlock))
{
	_align=DefaultAlign;
	_minAlloc=DefaultMinAlloc;
	DoConstruct(lateDestructPointer);
}

void *MemHeapLocked::Alloc( MemSize size, int aligned )
{
	SCOPE_LOCK();
	return base::Alloc(size,aligned);
}

void MemHeapLocked::Free( void *mem )
{
	SCOPE_LOCK();
	base::Free(mem);
}

void MemHeapLocked::CleanUp()
{
	SCOPE_LOCK();
	base::CleanUp();
}

void *MemHeap::Alloc( MemSize size, int aligned )
{
	VerifyStructure();

	#if MEM_PERF_LOG
	PROFILE_SCOPE(memAl);
	#endif

	if( size==0 ) return NULL;
	//if( size>=32*1024*1024 )
	if( size>=256*1024*1024 )
	{
		ErrF("Bad memory allocation, %d B",size);
		return NULL;
		//for(;;){}
	}
	#if SMALL_ALLOC
	int needSize=size;
	int allocIndex=nAllocSlots;
	for( int i=0; i<nAllocSlots; i++ )
	{
		if( allocSizes[i]>=needSize ) {allocIndex=i;break;}
	}
	_allocStats[allocIndex]++;

	if( allocIndex<nAllocSlots )
	{
		MemoAlloc *alloc=_smallAllocs[allocIndex];
		int *mem=(int *)alloc->AllocCounted(needSize);
		if (!mem) return NULL;
		++mem;
		// verify alignment
		//LogF("Small Memory %x",mem);
		/*
		if (((int)mem&15)!=0 && ((allocSizes[allocIndex]&15)==0) )
		{
			LogF("Small Memory %x, %d<=%d<=%d",mem,allocSizes[allocIndex],needSize,size);
			LogF("  Misaligned Alloc small");
		}
		*/
		return mem; // skip header
	}
	DoAssert( needSize>maxSmallAllocSize );
	#endif
	
	// item must be able to hold busy item information

	size+=sizeof(HeaderType);
	// align size
	if( aligned<(int)_align ) aligned=(int)_align;
	size=AlignSize(size,aligned);
	#if MEM_PERF_LOG
		ADD_COUNTER(mCnt,1);
		ADD_COUNTER(mSize,size);
	#endif
	// find best match
	FreeBlock *best=FindBest(size);
	if (!best)
	{
		// if we cannot find any match, try cleaning up small allocators
		LogF("%s: cleaning-up",_name);
		CleanUp();
		best=FindBest(size);
		if (!best) return NULL;
	}
	int bestSize=BlockSize(best);
	int bestDiff=bestSize-size;
	//LogF("Best block start %x",best->_memory);
	//MemItem *best=memTable+bestI;
	//FreeBlock *busy=best;

	//VerifyBlock(busy);

	#if INDIRECT
	// block _memory should be valid
	char *ret=(char *)((HeaderType *)best->_memory+1); // use shorter data type
	#else
	char *ret=(char *)((HeaderType *)best+1); // use shorter data type
	#endif
	//LogF("  ret %x",ret);
	bool ok = _memBlocks.Commit(ret-(char *)_memBlocks.Data()+size);
	if (!ok) return NULL;

	// there must be at least busy block + _align left
	const int minLeft=_minAlloc;
	//const int minLeft=32; // less efficient, less fragmentation
	if( bestDiff<=minLeft )
	{
		// exact or nearly exact match, do not split - use whole item
		// delete best from the free list
		//best->SetBusySize(best->GetFreeSize());
		_freeList.Delete(best);
		//freeList.DeleteNext(bestPrev,best);
		//Log("Allocated %d B (%x)",size,(char *)busy);
		_nItemsFree--;

		VerifyStructure();
	}
	else
	{
		// split item into two parts
		#if INDIRECT
			FreeBlock *free=new(_infoAlloc) FreeBlock;
			if (!free) return NULL;
			free->_memory=best->_memory+size;
		#else
			FreeBlock *free=(FreeBlock *)((char *)best+size);
			bool ok = _memBlocks.Commit((char *)free-(char *)_memBlocks.Data()+sizeof(FreeBlock));
			if (!ok) return NULL;
		#endif

		_freeList.Delete(best);

		_blockList.InsertAfter(best,free);
		_freeList.Insert(free); // replace best block with free block
		//_nItemsFree count not changed

		VerifyStructure();
	}

	AddRef(); // increase RefCount - disable deallocation
	//Log("Allocated %x: %06d",ret,size);

	// create back reference
	#if INDIRECT
	*(HeaderType *)best->_memory=best;
	#endif

	if( best->FreeLink::next || best->FreeLink::prev )
	{
		Fail("Alloc: Memory block corrupt.");
	}

	/*
	if (((int)ret&15)!=0)
	{
		LogF("Memory %x",ret);
		LogF("  Misaligned Alloc large");
	}
	*/

	return ret;
}

bool MemHeap::IsFromHeap( void *mem )
{
	//BusyBlock *busy=(HeaderType *)mem-1;
	//if( busy<_memBlocks.Data() || busy->EndBusy()>(char *)_memBlocks.Data()+_memBlocks.Size() ) return false;
	return true;
}

void MemHeap::Free( void *mem )
{
	if( !mem ) return;

	#if MEM_PERF_LOG
	PROFILE_SCOPE(memFr);
	#endif

	#if SMALL_ALLOC
	int *smallMem=(int *)mem-1;
	FastAlloc *alloc=FastAlloc::WhichAllocator(smallMem);
	if( alloc )
	{
		// busyBlock first item is never null
		
		alloc->FreeCounted(smallMem);
		return;
	}
	#endif

	//BusyBlock *busy=(BusyBlock *)mem-1;
	#if INDIRECT
	HeaderType *header=(HeaderType *)mem-1;
	FreeBlock *busy=*header;
	if( busy->_memory!=(void *)header )
	{
		Fail("Free: Memory block corrupt.");
	}
	#else
	FreeBlock *busy=(FreeBlock *)((HeaderType *)mem-1);
	#endif

	Ref<MemHeap> keepAllocated=this; // keep allocated until Return

	VerifyStructure();

	//Log("Free %x: %06d",mem,busy->size);
	// check if this block was really allocated in this heap

	//if( busy<_memBlocks.Data() || busy->EndBusy()>(char *)_memBlocks.Data()+_memBlocks.Size() )
	//{
	//	Fail("Free: Memory block not from this heap");
	//	return;
	//}
	if( busy->FreeLink::next || busy->FreeLink::prev )
	{
		Fail("Free: Memory block corrupt.");
		return;
	}
	/*
	if( busy->_size>0 )
	{
		Fail("Free: Memory block invalid size.");
		return; // bad memory block
	}
	if( busy->_size>_memBlocks.Size() )
	{
		Fail("Free: Memory block too large.");
		return; // bad memory block
	}
	*/

	// check if block is valid block

	//VerifyBlock(busy);

	#if MEM_PERF_LOG
		int busySize=BlockSize(busy);
		ADD_COUNTER(mCnt,1);
		ADD_COUNTER(mSize,busySize);
	#endif
	// insert busy into the memTable
	// find place where to insert it
	
	//BusyBlock 	

	bool doInsert=true;

	// change busy block into free block

	// check if we can concatenate with next block
	BusyBlock *next=_blockList.Next(busy);
	if( next && next->IsFree() )
	{
		FreeBlock *nextCat=(FreeBlock *)next;

		_freeList.Insert(busy);
		_freeList.Delete(nextCat);

		_blockList.Delete(nextCat);

		//delete(_infoAlloc) nextCat;
		_infoAlloc.Free(nextCat); // replacement of delete(_infoAlloc) nextCat

		doInsert=false; // block is already inserted in all lists
		
	}

	// check if we can concatenate with previous block
	BusyBlock *prev=_blockList.Prev(busy);
	if( prev && prev->IsFree() )
	{
		FreeBlock *prevCat=(FreeBlock *)prev;
		_blockList.Delete(busy);
		// if the block should not be inserted into the free list
		// it is already inserted and must be deleted
		if( !doInsert )
		{
			// this is the only place where free list is searched when freeing
			_freeList.Delete(busy);
			_nItemsFree--;
		}
		doInsert=false; // block is already inserted in free list

		//delete(_infoAlloc) busy;
		_infoAlloc.Free(busy); // replacement of delete(_infoAlloc) busy

		busy=prevCat;
	}

	if( doInsert )
	{
		// there is no need to keep freeList sorted
		_freeList.Insert(busy);
		_nItemsFree++;
	}

	Release(); // decrease RefCount - enable deallocation

	VerifyStructure();
}

void MemHeap::CleanUp()
{
#if SMALL_ALLOC
	// clean-up all small allocators
	for (int i=0; i<nAllocSlots; i++)
	{
		MemoAlloc *ma = _smallAllocs[i];
		Assert(ma);
		if (!ma) continue;
		// check how much we cleaned up
		ma->CleanUp();
	}
#endif
}

MemSize FreeList::MaxFreeLeft( const MemHeap *heap ) const
{
	int max=0;
	for( FreeBlock *free=Start(); NotEnd(free); free=Advance(free) )
	{
		int size=heap->BlockSize(free);
		if( max<size ) max=size;		
	}
	return max;
}

MemSize FreeList::TotalFreeLeft( const MemHeap *heap ) const
{
	MemSize total=0;
	for( FreeBlock *free=Start(); NotEnd(free); free=Advance(free) )
	{
		int size=heap->BlockSize(free);
		total+=size;
	}
	return total;
}

MemSize MemHeap::MaxFreeLeft() const
{
	int max=0;
	int val;
	val=_freeList.MaxFreeLeft(this);
	if( max<val ) max=val;
	//val=_freeList256.MaxFreeLeft();
	//if( max<val ) max=val;
	return max;
}
MemSize MemHeap::TotalFreeLeft() const
{
	int sum=0;
	sum+=_freeList.TotalFreeLeft(this);
	//sum+=_freeList256.TotalFreeLeft();
	return sum;
}
MemSize MemHeap::TotalAllocated() const
{
	MemSize free=TotalFreeLeft();
	return _memBlocks.Size()-free;
}
MemSize MemHeap::TotalCommited() const
{
	return _memBlocks.GetCommited();
}

int MemHeap::CountFreeLeft() const
{
	return _nItemsFree;
}

#if DO_STATS
class ReportN
{
	public:
	~ReportN()
	{
		ReportMemory();
	}
};

static ReportN ReportNOnExit INIT_PRIORITY_URGENT;

#endif

#define MemHeapType MemHeap
//#define MemHeapType MemHeapLocked

class RefHeap
{
	Ref<MemHeapType> _memory;

	public:
	operator MemHeapType *() {return _memory;}
	MemHeapType *operator ->() {return _memory;}

	RefHeap( int sizeMB=256 );
	~RefHeap();
};

/*!
\patch_internal 1.50 Date 4/11/2002 by Ondra
- Canceled: (1.63) 1 GB RAM can be used for memory allocations
when it is available (limit 512 MB was set before).
*/

static DWORD LimitSizeKB = 512*1024;

void LimitHeapSizeMB(int sizeMB)
{
	LimitSizeKB = sizeMB*1024;
}

int DetectHeapSizeMB()
{
#ifdef _WIN32
	// note: memory allocation is not available yet, command line arguments are not parsed yet
	MEMORYSTATUS memstat;
	memstat.dwLength = sizeof(memstat);
	GlobalMemoryStatus(&memstat);
	DWORD maxSizeKB = memstat.dwTotalVirtual/1024;
	// we will never try to use more than 1 GB
	// some space must be left for file memory mapping etc.
	//const DWORD limitSizeKB = 768*1024;
	const DWORD memLimitKB = memstat.dwTotalPageFile/1024 + memstat.dwTotalPhys/1024;
	// we know 512 MB reserved worked well in OFP 1.00-1.49
	// and there seems to be no reason to reserve less
	const DWORD minReservedKB = 512*1024;

	if (maxSizeKB>memLimitKB) maxSizeKB = memLimitKB;
	if (maxSizeKB>LimitSizeKB) maxSizeKB = LimitSizeKB;

	if (maxSizeKB<minReservedKB) maxSizeKB  = minReservedKB;

	LogF
	(
		"Memory: virtual %d, swap %d, physical %d, result %d", 
		memstat.dwTotalVirtual/(1024*1024),
		memstat.dwTotalPageFile/(1024*1024),
		memstat.dwTotalPhys/(1024*1024),
		maxSizeKB/1024
	);
	return maxSizeKB/1024;
#else
	return 512;
#endif
}

RefHeap BMemory(DetectHeapSizeMB()) INIT_PRIORITY_URGENT; // memory low condition testing

//MemHeapType *SMemPtr=SMemory;
MemHeapType *BMemPtr=BMemory;

RefHeap::RefHeap( int sizeMB )
{
	#if SMALL_ALLOC
	const int align=maxSmallAllocSize;
	#else
	const int align=DefaultAlign;
	#endif
#ifdef _WIN32
	_memory=new MemHeapType("Default",sizeMB*1024*1024,align,&BMemPtr);
#endif
}

RefHeap::~RefHeap()
{
	#if !_RELEASE
	LogF("Memory static ref deallocated.");
	if( _memory && _memory->TotalAllocated()>0 )
	{
		LogF("  %d KB not deallocated",_memory->TotalAllocated()/1024);
	}
	#endif
}


void MemoryInit()
{
}

void MemoryCleanUp()
{
#ifdef _WIN32
	if( BMemPtr )
	{
		// force all allocators to clean up
		FastCAlloc::CleanUpAll();
		BMemPtr->CleanUp();
	}
#endif
}

void MemoryDone()
{
	// all FastAlloc should be cleaned up now
	// SmallAlloc probably are not

	if ( BMemPtr )
	{
		BMemPtr->CleanUp();
	}

	if ( BMemPtr )
	{
		RptF("Memory should be free now.");
		RptF("  %d B not deallocated",BMemPtr->TotalAllocated());

		#if CHECK
			ReportAllocated();
		#endif
	}
	else
	{
		LogF("Memory is free now.");
	}
	#if 0 //SMALL_ALLOC
	LogF("Allocation size report");
	for( int i=0; i<=nAllocSlots; i++ )
	{
		int size=i<nAllocSlots ? allocSizes[i] : 1*1024*1024;
		LogF("slot %2d (%7d): %9d",i,size,AllocStats[i]);
	}
	#endif
}

class MemTableFunctions: public MemFunctions
{
	public:
	virtual void *New(size_t size);
	virtual void *New(size_t size, const char *file, int line);
	virtual void Delete(void *mem);
	virtual void Delete(void *mem, const char *file, int line);


	//! base of memory allocation space, NULL is unknown/undefined
	virtual void *HeapBase()
	{
		if( !BMemory ) return NULL;
		return BMemory->Memory();
	}

	//! approximate total ammount of commited memory
	virtual size_t HeapUsed()
	{
		if( !BMemory ) return 0;
		return BMemory->TotalAllocated();
	}

	//! approximate total count of free memory blocks (used to determine fragmentation)
	virtual int FreeBlocks()
	{
		if( !BMemory ) return 0;
		return BMemory->CountFreeLeft();
	}

	//! approximate total count of allocated memory blocks (used to determine fragmentation)
	virtual int MemoryAllocatedBlocks()
	{
		if( !BMemory ) return 0;
		return BMemory->RefCounter();
	}

	//! list all allocated blocks to debug log
	virtual void Report()
	{
		if( !BMemory ) return;
		int alloc=BMemory->TotalAllocated();
		int commit=BMemory->TotalCommited();
		LogF("Total allocated %d, commited %d",alloc,commit);
	}

	//! check heap integrity
	virtual bool CheckIntegrity() {return BMemory->Check();}
	//! check if out of memory is signalized
	//! if true, application should limit memory allocation as much as possible
	virtual bool IsOutOfMemory() {return MemoryErrorState;}

	virtual void CleanUp() {BMemory->CleanUp();}
};

MemTableFunctions OMemTableFunctions INIT_PRIORITY_URGENT;

MemFunctions *GMemFunctions = &OMemTableFunctions;

void RegisterMemoryFreeOnDemand(IMemoryFreeOnDemand *object)
{
#ifdef _WIN32
	DoAssert( BMemory );
#endif
	if( BMemory )
	{
		BMemory->RegisterFreeOnDemand(object);
	}
}

size_t MemoryFreeOnDemand(size_t size)
{
	return BMemory->FreeOnDemand(size);
}

/*!
	\patch_internal 1.05 Date 07/10/2001 by Ondra.
	- Fix: Bytes were reported instead of KB.
*/


void MemoryError( int size )
{
	MemoryErrorReported();
	ErrorMessage
	(
		"Out of reserved memory (%d KB).\n"
		"Code change required (current limit %d KB).\n"
		"Total free %d KB\n"
		"Free blocks %d, Max free size %d KB",
		size/1024,
		BMemory->Size()/1024,
		BMemory->TotalFreeLeft()/1024,
		BMemory->CountFreeLeft(),
		BMemory->MaxFreeLeft()/1024
	);
}

void MemoryErrorMalloc( int size )
{
	ErrorMessage("Out of Win32 memory (%d KB).\n",size);
}


#if _MSC_VER
#define MEM_CONV __cdecl
#else
#define MEM_CONV
#endif

/*!
\patch 1.45 Date 2/15/2002 by Ondra
- Fixed: When out of memory, application was crashing.
Message should now be shown instead.
*/

#if 0 //_ENABLE_REPORT

static DWORD UnsafeHeapThread=GetCurrentThreadId();

#define AssertThread() DoAssert(GetCurrentThreadId()==UnsafeHeapThread)

#else

#define AssertThread()

#endif

inline void *ActualAlloc( size_t size )
{
#ifdef _WIN32
	AssertThread();
	#if NO_ALLOCATION_AFTER_ERROR
	if (MemoryErrorState)
	{
		// we did not have memory before - we do not have it again
		return NULL;
	}
	#endif
	//if( size<256 ) return SMemory->Alloc(size);
	//else
	void *mem = BMemory->Alloc(size);
	if( !mem && size )
	{
		// attempt  to free some memory
		for(;;)
		{
			size_t freed = BMemory->FreeOnDemand(size);
			if (freed==0)
			{
				// failure: nothing to free - we are unable to satisfy the request
				MemoryError(size);
				return NULL;
			}
			void *mem = BMemory->Alloc(size);
			if (mem)
			{
				// success: we have memory - flush was successfull
				return mem;
			}
			// some memory was freed, but request was not successfull
			// this is partial success
			// continue freeing until final success or failure
		}
	}
	return mem;
#else
        return malloc(size);
#endif
}

inline void ActualFree( void *mem )
{
#ifdef _WIN32
	AssertThread();
	//if( SMemory->IsFromHeap(mem) ) SMemory->Free(mem);
	//else
	BMemory->Free(mem);
#else
        free(mem);
#endif
}



void *MemTableFunctions::New(size_t size)
{
	CountNew("nofile",0,size);
	#if CHECK
		size=PrepareAlloc(size);
	#endif
	void *ret=ActualAlloc(size);
	#if CHECK
		ret=FinishAlloc(ret,size);
	#endif
	return ret;
}

void *MemTableFunctions::New(size_t size, const char *file, int line)
{
	#if DO_STATS
	if (*file==0)
	{
		LogF("Warning");
	}
	#endif
	CountNew(file,line,size);
	#if CHECK
		size=PrepareAlloc(size);
	#endif
	void *ret=ActualAlloc(size);
	#if CHECK
		ret=FinishAlloc(ret,size,file,line);
	#endif
	return ret;
}

void MemTableFunctions::Delete(void *mem)
{
	if( !mem ) return;
	#if CHECK
		mem=PrepareFree(mem);
	#endif
	// determine which heap it is in
	ActualFree(mem);
}
void MemTableFunctions::Delete(void *mem, const char *file, int line)
{
	if( !mem ) return;
	#if CHECK
		mem=PrepareFree(mem);
	#endif
	// determine which heap it is in
	ActualFree(mem);
}

#ifdef _WIN32

//---------------------------------------------------------------------------
//  Net-heap support:

#ifdef _WIN32

/// Static instance of mt-safe heap (to be destructed as late as possible).
RefD<MemHeapLocked> safeHeap INIT_PRIORITY_URGENT;

#ifdef NET_LOG_SAFE_HEAP
unsigned safeHeapCounter = 0;
#endif

#define NET_HEAP_SIZE   128                 // 128MB of memory


class MemFunctionsSafe: public MemFunctions
{
	private:
	bool Create();
	void Destroy();

	public:
	void *New(size_t size);
	void *New(size_t size, const char *file, int line);
	void Delete(void *mem);
	void Delete(void *mem, const char *file, int line);

	void CleanUp() {BMemory->CleanUp();}
};

MemFunctionsSafe SafeHeapFunctions INIT_PRIORITY_URGENT;

MemFunctions *GSafeMemFunctions = &SafeHeapFunctions;

bool MemFunctionsSafe::Create()
{
    if ( safeHeap ) return true;
    safeHeap = new MemHeapLocked;
    safeHeap->Init("SafeHeap",NET_HEAP_SIZE<<20); // X MB of memory
#ifdef NET_LOG_SAFE_HEAP
    NetLog("Initializing SafeHeap: %d MB of memory",NET_HEAP_SIZE);
#endif
    return true;
}

void MemFunctionsSafe::Destroy ()
{
    if ( !safeHeap ) return;
    safeHeap = NULL;
}

void *MemFunctionsSafe::New(size_t size)
{
    if ( !safeHeap ) {
        Verify( Create() );
        }
    void *mem;
    mem = safeHeap->Alloc(size);
    if (!mem)
		{
				safeHeap->FreeOnDemand(size);
        mem = safeHeap->Alloc(size);
        }
#ifdef NET_LOG_SAFE_HEAP
    //if ( !(++safeHeapCounter & 0xff) )
        NetLog("safeNew (%s): %6u bytes, %u bytes remains (in %u blocks)",
               mem?"success":"fail",(unsigned)size,(unsigned)safeHeap->TotalFreeLeft(),(unsigned)safeHeap->CountFreeLeft());
#endif
    DoAssert( mem );
    return mem;
}
void *MemFunctionsSafe::New(size_t size, const char *file, int line)
{
	return New(size);
}
void MemFunctionsSafe::Delete(void *mem)
{
    Assert( safeHeap );
    safeHeap->Free(mem);
#ifdef NET_LOG_SAFE_HEAP
    //if ( !(++safeHeapCounter & 0xff) )
        NetLog("safeDelete:                      %u bytes remains (in %u blocks)",
               (unsigned)safeHeap->TotalFreeLeft(),(unsigned)safeHeap->CountFreeLeft());
#endif
}
void MemFunctionsSafe::Delete(void *mem, const char *file, int line)
{
	Delete(mem);
}

#else

void *safeNew ( size_t size )
{
    return malloc(size);
}

void safeDelete ( void *mem )
{
    free(mem);
}

#endif



#endif

#if defined(_WIN32) && !defined(_XBOX)
  // netLogger instance moved to netlog.cpp for Linux..
  
#  include "Es/essencepch.hpp"
#  include "Es/Framework/netlog.hpp"

#  if defined(NET_LOG) && defined(EXTERN_NET_LOG)
NetLogger netLogger;
#  endif

#endif

#else

void RegisterMemoryFreeOnDemand(IMemoryFreeOnDemand *object)
{
}

size_t MemoryFreeOnDemand(size_t size)
{
	return 0;
}

void MemoryInit(){}
void MemoryDone(){}
void MemoryFootprint(){}

#endif