#ifdef _MSC_VER
#pragma once
#endif

#ifndef _HOUSE_HPP
#define _HOUSE_HPP

#include "transport.hpp"
#include "dynSound.hpp"

struct HousePathArrayItem
{
	Vector3 pos;
	int index;
};
TypeIsSimple(HousePathArrayItem)

typedef StaticArrayAuto<int> HousePathArrayIndexed;
typedef StaticArrayAuto<HousePathArrayItem> HousePathArray;

#define HOUSE_PATH_ARRAY(name,size) AUTO_STATIC_ARRAY(HousePathArrayItem,name,size)
#define HOUSE_PATH_ARRAY_INDEXED(name,size) AUTO_STATIC_ARRAY(int,name,size)

struct Ladder
{
	int _top,_bottom;
};

TypeIsSimple(Ladder)

struct WeaponProxy
{
	LLink<Object> obj;
	int selection; // copied over from proxyObject

	WeaponProxy() {selection = -1;}
	bool IsPresent() const {return selection >= 0;}
};


// temporary solution of problems with Transport derived buildings
class BuildingType: public TransportType
{
	friend class Building;
	friend class IPaths; // give access to path information

	//typedef VehicleType base;
	typedef TransportType base;

	AutoArray<int> _exits;
	AutoArray<int> _positions;
	AutoArray< FindArray<int> > _connections;
	AutoArray<Ladder> _ladders;

	float _coefInside;	// we must go slowly inside houses 
	float _coefInsideHeur;	// estimation of cost based on in/out points

	WeaponProxy _proxies[MAX_LOD_LEVELS];

	public:
	BuildingType( const ParamEntry *param );
	virtual void Load(const ParamEntry &par);
	void InitShape(); // after shape is loaded

	const V3 &GetPosition(int index) const;
	float GetCoefInside() const {return _coefInside;}
	float GetCoefInsideHeur() const {return _coefInsideHeur;}
	int FindNearestExit(Vector3Par pos, Vector3 &ret) const;
	int FindNearestPosition(Vector3Par pos, Vector3 &ret) const;
	//int FindNearestPoint(Vector3Par pos, Vector3 &ret, float maxDist2=FLT_MAX) const;
	bool SearchPath(int from, int to, HousePathArrayIndexed &path) const;


	const Ladder &GetLadder(int i) const {return _ladders[i];}
	int GetLadderCount() const {return _ladders.Size();}
};

class FountainType: public BuildingType
{
	friend class Fountain;

	typedef BuildingType base;

	Animation _water; // east/west flag
	RString _sound;
	float _animSpeed;

	public:
	FountainType( const ParamEntry *param );
	virtual void Load(const ParamEntry &par);
	void InitShape(); // after shape is loaded
};

class ChurchType : public BuildingType
{
	friend class Church;
	typedef BuildingType base;

	AnimationRotation _hour1;
	AnimationRotation _minute1;
	AnimationRotation _hour2;
	AnimationRotation _minute2;
	AnimationRotation _hour3;
	AnimationRotation _minute3;
	AnimationRotation _hour4;
	AnimationRotation _minute4;

	public:
	ChurchType( const ParamEntry *param );
	virtual void Load(const ParamEntry &par);
	void InitShape(); // after shape is loaded
};

class IPaths
{
protected:
	AutoArray<int> _locks; 

public:
	virtual const BuildingType *GetBType() const = NULL;
	virtual const Object *GetObject() const = NULL;
	
	Vector3 GetPosition(int index) const;
	bool SearchPath( int from, int to, HousePathArray &path ) const;
	int FindNearestExit(Vector3Par pos, Vector3 &ret) const;
	int FindNearestPosition(Vector3Par pos, Vector3 &ret) const;
	int FindNearestPoint(Vector3Par pos, Vector3 &ret,float maxDist2=FLT_MAX) const;

	int NExits() const {return GetBType()->_exits.Size();}
	int NPos() const {return GetBType()->_positions.Size();}
	int NPoints() const {return GetBType()->_connections.Size();}
	int GetExit(int index) const {return GetBType()->_exits[index];}
	int GetPos(int index) const {return GetBType()->_positions[index];}

	bool IsLocked(int index) const
	{
		if (index >= _locks.Size()) return false;
		return _locks[index] > 0;
	}
	void Lock(int index)
	{
		int n = _locks.Size();
		if (index >= n)
		{
			_locks.Resize(index + 1);
			for (int i=n; i<=index; i++) _locks[i] = 0;
		}
		_locks[index]++;
	}
	void Unlock(int index)
	{
		DoAssert(index < _locks.Size());
		_locks[index]--;
	}
};

class Building: public VehicleSupply, public IPaths
{
	typedef VehicleSupply base;

public:
	Building( VehicleType *name, int id, LODShapeWithShadow *shape );
	
	// get interface
	virtual const IPaths *GetIPaths() const {return this;}
	virtual const Object *GetObject() const {return this;}
	
	const BuildingType *Type() const
	{
		return static_cast<const BuildingType *>(GetType());
	}

	const BuildingType *GetBType() const
	{
		return static_cast<const BuildingType *>(GetType());
	}

	void Simulate( float deltaT, SimulationImportance prec );

	// building are usually empty
	void Sound( bool inside, float deltaT ) {}
	void UnloadSound() {}

	virtual bool CastShadow() const;

	void DrawDiags();
	bool QIsManual() const {return false;}
	bool IsMoveTarget() const;

	Matrix4 InsideCamera( CameraType camType ) const;
	int InsideLOD( CameraType camType ) const;
	
	// no get-in to buildings
	bool QCanIGetIn( Person *who = NULL ) const {return false;}

	bool IsAnimated( int level ) const; // appearence changed with Animate
	bool IsAnimatedShadow( int level ) const; // shadow changed with Animate
	void Animate( int level );
	void Deanimate( int level );

	Vector3 GetLadderPos( int ladder, bool up );

	virtual RString GetActionName(const UIAction &action);
	virtual void PerformAction(const UIAction &action, AIUnit *unit);

	virtual void GetActions(UIActions &actions, AIUnit *unit, bool now);

	void DrawProxies
	(
		int level, ClipFlags clipFlags,
		const Matrix4 &transform, const Matrix4 &invTransform,
		float dist2, float z2, const LightList &lights
	);
	int GetProxyComplexity
	(
		int level, const FrameBase &pos, float dist2
	) const;

	//TargetSide GetTargetSide() const {return TCivilian;}
	USE_CASTING(base)
};

class CameraBuilding: public Transport
{
	typedef Transport base;

	public:
	CameraBuilding( VehicleType *name, int id, LODShapeWithShadow *shape );
	
	const BuildingType *Type() const
	{
		return static_cast<const BuildingType *>(GetType());
	}

	void Simulate( float deltaT, SimulationImportance prec ){}

	// building are usually empty
	void Sound( bool inside, float deltaT ) {}
	void UnloadSound() {}

	void DrawDiags(){}
	bool QIsManual() const {return false;}

	Matrix4 InsideCamera( CameraType camType ) const {return MIdentity;}
	int InsideLOD( CameraType camType ) const {return 0;}
	
	bool QCanIGetIn( Person *who = NULL ) const {return false;}

	bool IsAnimated( int level ) const {return false;}
	bool IsAnimatedShadow( int level ) const {return false;}
	void Animate( int level ) {}
	void Deanimate( int level ) {}
};


class Church: public Building
{
	typedef Building base;

	int _ringSmall,_ringBig;
	float _nextRing;
	float _badTime; // no clock is exact

	public:
	Church( VehicleType *name, int id, LODShapeWithShadow *shape );
	
	void Simulate( float deltaT, SimulationImportance prec );

	bool IsAnimated( int level ) const; // appearence changed with Animate
	bool IsAnimatedShadow( int level ) const; // shadow changed with Animate
	void Animate( int level );
	void Deanimate( int level );

	void Sound( bool inside, float deltaT );
	void UnloadSound();

	void ResetStatus();

	const ChurchType *Type() const
	{
		return static_cast<const ChurchType *>(GetType());
	}
};

class Fountain: public Building
{
	float _anim;
	Time _lastAnimation;
	Ref<SoundObject> _sound;

	typedef Building base;

	public:
	Fountain( VehicleType *name, int id, LODShapeWithShadow *shape );

	const FountainType *Type() const
	{
		return static_cast<const FountainType *>(GetType());
	}
	
	void Simulate( float deltaT, SimulationImportance prec );

	void Sound( bool inside, float deltaT );
	void UnloadSound();

	bool IsAnimated( int level ) const; // appearence changed with Animate
	bool IsAnimatedShadow( int level ) const; // shadow changed with Animate
	void Animate( int level );
	void Deanimate( int level );	
};

#endif
