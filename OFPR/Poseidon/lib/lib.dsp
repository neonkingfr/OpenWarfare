# Microsoft Developer Studio Project File - Name="lib" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=lib - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "lib.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "lib.mak" CFG="lib - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "lib - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "lib - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE "lib - Win32 Testing" (based on "Win32 (x86) Application")
!MESSAGE "lib - Win32 Demo Version" (based on "Win32 (x86) Application")
!MESSAGE "lib - Win32 Super Release" (based on "Win32 (x86) Application")
!MESSAGE "lib - Win32 Czech Super Release" (based on "Win32 (x86) Application")
!MESSAGE "lib - Win32 MP Demo" (based on "Win32 (x86) Application")
!MESSAGE "lib - Win32 VBS1" (based on "Win32 (x86) Application")
!MESSAGE "lib - Win32 Super Designer" (based on "Win32 (x86) Application")
!MESSAGE "lib - Win32 Super Server" (based on "Win32 (x86) Application")
!MESSAGE "lib - Win32 Korean Super Release" (based on "Win32 (x86) Application")
!MESSAGE "lib - Win32 Russian Super Release" (based on "Win32 (x86) Application")
!MESSAGE "lib - Win32 Resistance" (based on "Win32 (x86) Application")
!MESSAGE "lib - Win32 Resistance Server" (based on "Win32 (x86) Application")
!MESSAGE "lib - Win32 Resistance Czech" (based on "Win32 (x86) Application")
!MESSAGE "lib - Win32 Resistance Russian" (based on "Win32 (x86) Application")
!MESSAGE "lib - Win32 Resistance Demo" (based on "Win32 (x86) Application")
!MESSAGE "lib - Win32 VBS1 Server" (based on "Win32 (x86) Application")
!MESSAGE "lib - Win32 VBS1 Demo" (based on "Win32 (x86) Application")
!MESSAGE "lib - Win32 Galatea" (based on "Win32 (x86) Application")
!MESSAGE "lib - Win32 ColdWarAssault" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/Poseidon/lib", IRCAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "lib - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "../Release"
# PROP Intermediate_Dir "../Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\archive\ofpr" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "NET_LOG_COMMAND_LINE" /YX"wpch.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 Delayimp.lib ddraw.lib vorbisfile60.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib largeint.lib /nologo /subsystem:windows /map:"o:\StatusQuo\bin\generic.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\StatusQuo\bin\generic.bin" /fixed:no /OPT:REF /DELAYLOAD:glide3x.dll /DELAYLOAD:mpiwin32.dll
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "../Debug"
# PROP Intermediate_Dir "../Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /GZ /c
# ADD CPP /nologo /G6 /W3 /GR /Zi /Od /I "w:\c\archive\ofpr" /D __MSC__=1100 /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "NET_LOG_COMMAND_LINE" /YX"wpch.hpp" /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 Delayimp.lib ddraw.lib vorbisfile60d.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib largeint.lib /nologo /subsystem:windows /incremental:no /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\PosDebug\pos_3dfx.exe" /DELAYLOAD:glide3x.dll /DELAYLOAD:mpiwin32.dll
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "lib___Win32_Testing"
# PROP BASE Intermediate_Dir "lib___Win32_Testing"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Testing"
# PROP Intermediate_Dir "Testing"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gy /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /Fp"Release/pos_3dfx.pch" /YX"wpch.hpp" /FD /GF /c
# ADD CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gy /I "w:\c\archive\ofpr" /D __MSC__=1100 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /YX"wpch.hpp" /FD /GF /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 Delayimp.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib ddraw.lib dxguid.lib dinput.lib /nologo /subsystem:windows /map:"o:\StatusQuo\bin\generic.map" /debug /machine:I386 /out:"o:\StatusQuo\bin\generic.bin" /fixed:no /OPT:REF /DELAYLOAD:glide3x.dll
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 Delayimp.lib ddraw.lib vorbisfile.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib largeint.lib /nologo /subsystem:windows /map:"o:\StatusQuo\bin\testing.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\StatusQuo\bin\testing.bin" /fixed:no /OPT:REF /DELAYLOAD:glide3x.dll /DELAYLOAD:mpiwin32.dll
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "lib___Win32_Demo_Version"
# PROP BASE Intermediate_Dir "lib___Win32_Demo_Version"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "../Demo"
# PROP Intermediate_Dir "../Demo"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /YX"wpch.hpp" /FD /c
# ADD CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\archive\ofpr" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D _DEMO=1 /YX"wpch.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 Delayimp.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib ddraw.lib dxguid.lib dinput.lib version.lib /nologo /subsystem:windows /map:"o:\StatusQuo\bin\generic.map" /debug /machine:I386 /out:"o:\StatusQuo\bin\generic.bin" /fixed:no /OPT:REF /DELAYLOAD:glide3x.dll
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 Delayimp.lib ddraw.lib vorbisfile.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib /nologo /subsystem:windows /map:"o:\FPDemo\bin\demo.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\FPDemo\bin\demo.bin" /fixed:no /OPT:REF /DELAYLOAD:glide3x.dll /DELAYLOAD:mpiwin32.dll
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "lib___Win32_Super_Release"
# PROP BASE Intermediate_Dir "lib___Win32_Super_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\SuperRelease"
# PROP Intermediate_Dir "..\SuperRelease"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _DEMO=1 /YX"wpch.hpp" /FD /c
# ADD CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\archive\ofpr" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /YX"wpch.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 Delayimp.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib ddraw.lib dxguid.lib dinput.lib version.lib vorbisfile.lib /nologo /subsystem:windows /map:"o:\FPDemo\bin\demo.map" /debug /machine:I386 /out:"o:\FPDemo\bin\demo.bin" /fixed:no /OPT:REF /DELAYLOAD:glide3x.dll
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 Delayimp.lib ddraw.lib vorbisfile.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib largeint.lib /nologo /subsystem:windows /map:"O:\FPSuperRelease\OperationFlashpoint.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\FPSuperRelease\OperationFlashpoint.exe" /OPT:REF /DELAYLOAD:glide3x.dll /DELAYLOAD:mpiwin32.dll /order:@O:\FPSuperRelease\OperationFlashpoint.prf
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "lib___Win32_Czech_Super_Release"
# PROP BASE Intermediate_Dir "lib___Win32_Czech_Super_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\CzechSuperRelease"
# PROP Intermediate_Dir "..\CzechSuperRelease"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /YX"wpch.hpp" /FD /c
# ADD CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\archive\ofpr" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D _CZECH=1 /YX"wpch.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 Delayimp.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib ddraw.lib dxguid.lib dinput.lib version.lib vorbisfile.lib /nologo /subsystem:windows /map:"o:\FPDemo\bin\demo.map" /debug /machine:I386 /out:"o:\FPSuperRelease\OperationFlashpoint.exe" /fixed:no /OPT:REF /DELAYLOAD:glide3x.dll
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 Delayimp.lib ddraw.lib vorbisfile.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib w:\c\Hierarchy\common\SuperRelease\common.lib w:\c\Hierarchy\network\SuperRelease\network.lib /nologo /subsystem:windows /map:"o:\FPCzechSuperRelease\OperationFlashpoint.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\FPCzechSuperRelease\OperationFlashpoint.exe" /fixed:no /OPT:REF /DELAYLOAD:glide3x.dll /DELAYLOAD:mpiwin32.dll /order:@O:\FPCzechSuperRelease\OperationFlashpoint.prf
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "lib___Win32_MP_Demo"
# PROP BASE Intermediate_Dir "lib___Win32_MP_Demo"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "../MPDemo"
# PROP Intermediate_Dir "../MPDemo"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _DEMO=1 /YX"wpch.hpp" /FD /c
# ADD CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\archive\ofpr" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D _MP_DEMO=1 /YX"wpch.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 Delayimp.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib ddraw.lib dxguid.lib dinput.lib version.lib vorbisfile.lib /nologo /subsystem:windows /map:"o:\FPDemo\bin\demo.map" /debug /machine:I386 /out:"o:\FPDemo\bin\demo.bin" /fixed:no /OPT:REF /DELAYLOAD:glide3x.dll
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 Delayimp.lib ddraw.lib vorbisfile.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib /nologo /subsystem:windows /map:"o:\MPDemo\OperationFlashpointMPDemo.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\MPDemo\OperationFlashpointMPDemo.exe" /fixed:no /OPT:REF /DELAYLOAD:glide3x.dll /DELAYLOAD:mpiwin32.dll
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "lib___Win32_VBS1"
# PROP BASE Intermediate_Dir "lib___Win32_VBS1"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\VBS1"
# PROP Intermediate_Dir "..\VBS1"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /YX"wpch.hpp" /FD /c
# ADD CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\archive\ofpr" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D _VBS1=1 /YX"wpch.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG" /d _VBS1=1
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 Delayimp.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib ddraw.lib dxguid.lib dinput.lib version.lib vorbisfile.lib /nologo /subsystem:windows /map:"O:\FPSuperRelease\OperationFlashpoint.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\FPSuperRelease\OperationFlashpoint.exe" /OPT:REF /DELAYLOAD:glide3x.dll /order:@O:\FPSuperRelease\OperationFlashpoint.prf
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 Delayimp.lib ddraw.lib vorbisfile60.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib largeint.lib /nologo /subsystem:windows /map:"O:\VBS1\VBS1.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\VBS1\VBS1.exe" /OPT:REF /DELAYLOAD:glide3x.dll /DELAYLOAD:mpiwin32.dll /order:@O:\VBS1\VBS1.prf
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "lib___Win32_Super_Designer"
# PROP BASE Intermediate_Dir "lib___Win32_Super_Designer"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\SuperDesigner"
# PROP Intermediate_Dir "..\SuperDesigner"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /YX"wpch.hpp" /FD /c
# ADD CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\archive\ofpr" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D _DESIGN_SRELEASE=1 /YX"wpch.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 Delayimp.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib ddraw.lib dxguid.lib dinput.lib version.lib vorbisfile.lib /nologo /subsystem:windows /map:"O:\FPSuperRelease\OperationFlashpoint.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\FPSuperRelease\OperationFlashpoint.exe" /OPT:REF /DELAYLOAD:glide3x.dll /order:@O:\FPSuperRelease\OperationFlashpoint.prf
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 Delayimp.lib ddraw.lib vorbisfile.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib /nologo /subsystem:windows /map:"O:\FPSuperRelease\OFP_Designer.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\FPSuperRelease\OFP_Designer.exe" /OPT:REF /DELAYLOAD:glide3x.dll /DELAYLOAD:mpiwin32.dll /order:@O:\FPSuperRelease\OFP_Designer.prf
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "lib___Win32_Super_Server"
# PROP BASE Intermediate_Dir "lib___Win32_Super_Server"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\SuperServer"
# PROP Intermediate_Dir "..\SuperServer"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /YX"wpch.hpp" /FD /c
# ADD CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\archive\ofpr" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D _DED_SERVER=1 /YX"wpch.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 Delayimp.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib ddraw.lib dxguid.lib dinput.lib version.lib vorbisfile.lib /nologo /subsystem:windows /map:"O:\FPSuperRelease\OperationFlashpoint.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\FPSuperRelease\OperationFlashpoint.exe" /OPT:REF /DELAYLOAD:glide3x.dll /order:@O:\FPSuperRelease\OperationFlashpoint.prf
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 vorbisfile.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib w:\c\Hierarchy\common\SuperRelease\common.lib w:\c\Hierarchy\network\SuperRelease\network.lib /nologo /subsystem:windows /map:"O:\FPSuperRelease\OFP_Server.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\FPSuperRelease\OFP_Server.exe" /OPT:REF /order:@O:\FPSuperRelease\OFP_Server.prf /DELAYLOAD:mpiwin32.dll
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "lib___Win32_Korean_Super_Release"
# PROP BASE Intermediate_Dir "lib___Win32_Korean_Super_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\KoreanSuperRelease"
# PROP Intermediate_Dir "..\KoreanSuperRelease"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /YX"wpch.hpp" /FD /c
# ADD CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\archive\ofpr" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D _NO_BLOOD=1 /YX"wpch.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 Delayimp.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib ddraw.lib dxguid.lib dinput.lib version.lib vorbisfile.lib /nologo /subsystem:windows /map:"O:\FPSuperRelease\OperationFlashpoint.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\FPSuperRelease\OperationFlashpoint.exe" /OPT:REF /DELAYLOAD:glide3x.dll /order:@O:\FPSuperRelease\OperationFlashpoint.prf
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 Delayimp.lib ddraw.lib vorbisfile.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib /nologo /subsystem:windows /map:"O:\FPKoreanSuperRelease\OperationFlashpoint.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\FPKoreanSuperRelease\OperationFlashpoint.exe" /OPT:REF /DELAYLOAD:glide3x.dll /DELAYLOAD:mpiwin32.dll /order:@O:\FPSuperRelease\OperationFlashpoint.prf
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "lib___Win32_Russian_Super_Release"
# PROP BASE Intermediate_Dir "lib___Win32_Russian_Super_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\RussianSuperRelease"
# PROP Intermediate_Dir "..\RussianSuperRelease"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D _CZECH=1 /YX"wpch.hpp" /FD /c
# ADD CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\archive\ofpr" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D _RUSSIAN=1 /YX"wpch.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 Delayimp.lib ddraw.lib vorbisfile.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib /nologo /subsystem:windows /map:"o:\FPCzechSuperRelease\OperationFlashpoint.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\FPCzechSuperRelease\OperationFlashpoint.exe" /fixed:no /OPT:REF /DELAYLOAD:glide3x.dll /order:@O:\FPCzechSuperRelease\OperationFlashpoint.prf
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 Delayimp.lib ddraw.lib vorbisfile.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib w:\c\Hierarchy\common\SuperRelease\common.lib w:\c\Hierarchy\network\SuperRelease\network.lib /nologo /subsystem:windows /map:"o:\FPRussianSuperRelease\OperationFlashpoint.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\FPRussianSuperRelease\OperationFlashpoint.exe" /fixed:no /OPT:REF /DELAYLOAD:glide3x.dll /DELAYLOAD:mpiwin32.dll /order:@O:\FPRussianSuperRelease\OperationFlashpoint.prf
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "lib___Win32_Resistance"
# PROP BASE Intermediate_Dir "lib___Win32_Resistance"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\Resistance"
# PROP Intermediate_Dir "..\Resistance"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\element" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D "NET_LOG_COMMAND_LINE" /YX"wpch.hpp" /FD /c
# ADD CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\archive\ofpr" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D _RESISTANCE=1 /D "NET_LOG_COMMAND_LINE" /YX"wpch.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 Delayimp.lib ddraw.lib vorbisfile.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib largeint.lib /nologo /subsystem:windows /map:"O:\FPSuperRelease\OperationFlashpoint.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\FPSuperRelease\OperationFlashpoint.exe" /OPT:REF /DELAYLOAD:glide3x.dll /order:@O:\FPSuperRelease\OperationFlashpoint.prf
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 Delayimp.lib ddraw.lib vorbisfile.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib largeint.lib /nologo /subsystem:windows /map:"O:\FPSuperRelease\FlashpointResistance.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\FPSuperRelease\FlashpointResistance.exe" /OPT:REF /DELAYLOAD:glide3x.dll /DELAYLOAD:mpiwin32.dll /order:@O:\FPSuperRelease\FlashpointResistance.prf
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "lib___Win32_Resistance_Server"
# PROP BASE Intermediate_Dir "lib___Win32_Resistance_Server"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "../ResistanceServer"
# PROP Intermediate_Dir "../ResistanceServer"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\element" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D _RESISTANCE=1 /D "NET_LOG_COMMAND_LINE" /YX"wpch.hpp" /FD /c
# ADD CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\archive\ofpr" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D _RESISTANCE_SERVER=1 /D "NET_LOG_COMMAND_LINE" /FD /c
# SUBTRACT CPP /YX
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 Delayimp.lib ddraw.lib vorbisfile.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib largeint.lib w:\c\Hierarchy\common\SuperRelease\common.lib w:\c\Hierarchy\network\SuperRelease\network.lib /nologo /subsystem:windows /map:"O:\FPSuperRelease\FlashpointResistance.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\FPSuperRelease\FlashpointResistance.exe" /OPT:REF /DELAYLOAD:glide3x.dll /order:@O:\FPSuperRelease\FlashpointResistance.prf
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 Delayimp.lib ddraw.lib vorbisfile60.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib largeint.lib /nologo /subsystem:windows /map:"O:\FPSuperRelease\OFPR_Server.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\FPSuperRelease\OFPR_Server.exe" /OPT:REF /DELAYLOAD:glide3x.dll /DELAYLOAD:mpiwin32.dll
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "lib___Win32_Resistance_Czech"
# PROP BASE Intermediate_Dir "lib___Win32_Resistance_Czech"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\ResistanceCzech"
# PROP Intermediate_Dir "..\ResistanceCzech"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\element" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D _RESISTANCE=1 /YX"wpch.hpp" /FD /c
# ADD CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\archive\ofpr" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D _RESISTANCE=1 /D _CZECH=1 /YX"wpch.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 Delayimp.lib ddraw.lib vorbisfile.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib largeint.lib w:\c\Hierarchy\common\SuperRelease\common.lib w:\c\Hierarchy\network\SuperRelease\network.lib /nologo /subsystem:windows /map:"O:\FPSuperRelease\FlashpointResistance.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\FPSuperRelease\FlashpointResistance.exe" /OPT:REF /DELAYLOAD:glide3x.dll /order:@O:\FPSuperRelease\FlashpointResistance.prf
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 Delayimp.lib ddraw.lib vorbisfile.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib largeint.lib /nologo /subsystem:windows /map:"O:\FPCzechSuperRelease\FlashpointResistance.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\FPCzechSuperRelease\FlashpointResistance.exe" /OPT:REF /DELAYLOAD:glide3x.dll /DELAYLOAD:mpiwin32.dll /order:@O:\FPCzechSuperRelease\FlashpointResistance.prf
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "lib___Win32_Resistance_Russian"
# PROP BASE Intermediate_Dir "lib___Win32_Resistance_Russian"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\ResistanceRussian"
# PROP Intermediate_Dir "..\ResistanceRussian"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\element" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D _RESISTANCE=1 /YX"wpch.hpp" /FD /c
# ADD CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\archive\ofpr" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D _RESISTANCE=1 /D _RUSSIAN=1 /YX"wpch.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 Delayimp.lib ddraw.lib vorbisfile.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib largeint.lib w:\c\Hierarchy\common\SuperRelease\common.lib w:\c\Hierarchy\network\SuperRelease\network.lib /nologo /subsystem:windows /map:"O:\FPSuperRelease\FlashpointResistance.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\FPSuperRelease\FlashpointResistance.exe" /OPT:REF /DELAYLOAD:glide3x.dll /order:@O:\FPSuperRelease\FlashpointResistance.prf
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 Delayimp.lib ddraw.lib vorbisfile.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib largeint.lib /nologo /subsystem:windows /map:"O:\FPRussianSuperRelease\FlashpointResistance.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\FPRussianSuperRelease\FlashpointResistance.exe" /OPT:REF /DELAYLOAD:glide3x.dll /DELAYLOAD:mpiwin32.dll /order:@O:\FPRussianSuperRelease\FlashpointResistance.prf
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "lib___Win32_Resistance_Demo"
# PROP BASE Intermediate_Dir "lib___Win32_Resistance_Demo"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "../Demo"
# PROP Intermediate_Dir "../Demo"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\element" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D _DEMO=1 /YX"wpch.hpp" /FD /c
# ADD CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\archive\ofpr" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D _RESISTANCE_DEMO=1 /YX"wpch.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 Delayimp.lib ddraw.lib vorbisfile.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib /nologo /subsystem:windows /map:"o:\FPDemo\bin\demo.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\FPDemo\bin\demo.bin" /fixed:no /OPT:REF /DELAYLOAD:glide3x.dll
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 Delayimp.lib ddraw.lib vorbisfile.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib largeint.lib /nologo /subsystem:windows /map:"o:\FPDemo\FlashpointResistanceDemo.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\FPDemo\FlashpointResistanceDemo.exe" /fixed:no /OPT:REF /DELAYLOAD:glide3x.dll /DELAYLOAD:mpiwin32.dll
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "lib___Win32_VBS1_Server"
# PROP BASE Intermediate_Dir "lib___Win32_VBS1_Server"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\VBS1Server"
# PROP Intermediate_Dir "..\VBS1Server"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\archive\ofpr" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D _VBS1=1 /YX"wpch.hpp" /FD /c
# ADD CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\archive\ofpr" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D _VBS1=1 /D _VBS1_SERVER=1 /YX"wpch.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d _VBS1=1
# ADD RSC /l 0x409 /d "NDEBUG" /d _VBS1=1
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 Delayimp.lib ddraw.lib vorbisfile60.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib largeint.lib /nologo /subsystem:windows /map:"O:\VBS1\VBS1.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\VBS1\VBS1.exe" /OPT:REF /DELAYLOAD:glide3x.dll /DELAYLOAD:mpiwin32.dll /order:@O:\VBS1\VBS1.prf
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 Delayimp.lib ddraw.lib vorbisfile60.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib largeint.lib /nologo /subsystem:windows /map:"O:\VBS1\VBS1Server.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\VBS1\VBS1Server.exe" /OPT:REF /DELAYLOAD:glide3x.dll /DELAYLOAD:mpiwin32.dll
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "lib___Win32_VBS1_Demo"
# PROP BASE Intermediate_Dir "lib___Win32_VBS1_Demo"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\VBS1Demo"
# PROP Intermediate_Dir "..\VBS1Demo"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\archive\ofpr" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D _VBS1=1 /YX"wpch.hpp" /FD /c
# ADD CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\archive\ofpr" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D _VBS1_DEMO=1 /YX"wpch.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d _VBS1=1
# ADD RSC /l 0x409 /d "NDEBUG" /d _VBS1=1
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 Delayimp.lib ddraw.lib vorbisfile60.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib largeint.lib /nologo /subsystem:windows /map:"O:\VBS1\VBS1.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\VBS1\VBS1.exe" /OPT:REF /DELAYLOAD:glide3x.dll /DELAYLOAD:mpiwin32.dll /order:@O:\VBS1\VBS1.prf
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 Delayimp.lib ddraw.lib vorbisfile60.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib largeint.lib /nologo /subsystem:windows /map:"O:\VBS1Demo\VBS1Demo.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\VBS1Demo\VBS1Demo.exe" /OPT:REF /DELAYLOAD:glide3x.dll /DELAYLOAD:mpiwin32.dll /order:@O:\VBS1Demo\VBS1Demo.prf
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "lib___Win32_Galatea"
# PROP BASE Intermediate_Dir "lib___Win32_Galatea"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\Galatea"
# PROP Intermediate_Dir "..\Galatea"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\archive\ofpr" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D _RESISTANCE=1 /D "NET_LOG_COMMAND_LINE" /YX"wpch.hpp" /FD /c
# ADD CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\archive\ofpr" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D _GALATEA=1 /YX"wpch.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG" /d _GALATEA=1
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 Delayimp.lib ddraw.lib vorbisfile.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib largeint.lib /nologo /subsystem:windows /map:"O:\FPSuperRelease\FlashpointResistance.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\FPSuperRelease\FlashpointResistance.exe" /OPT:REF /DELAYLOAD:glide3x.dll /DELAYLOAD:mpiwin32.dll /order:@O:\FPSuperRelease\FlashpointResistance.prf
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 Delayimp.lib ddraw.lib vorbisfile60.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib largeint.lib /nologo /subsystem:windows /map:"O:\Galatea\Galatea.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\Galatea\Galatea.exe" /OPT:REF /DELAYLOAD:glide3x.dll /DELAYLOAD:mpiwin32.dll /order:@O:\Galatea\Galatea.prf
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "lib___Win32_ColdWarAssault"
# PROP BASE Intermediate_Dir "lib___Win32_ColdWarAssault"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "lib___Win32_ColdWarAssault"
# PROP Intermediate_Dir "lib___Win32_ColdWarAssault"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\archive\ofpr" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D _GALATEA=1 /YX"wpch.hpp" /FD /c
# ADD CPP /nologo /G6 /W3 /GR /Zi /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "w:\c\archive\ofpr" /D __MSC__=1100 /D _RELEASE=1 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D _SUPER_RELEASE=1 /D _COLD_WAR_ASSAULT=1 /YX"wpch.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d _GALATEA=1
# ADD RSC /l 0x409 /d "NDEBUG" /d _COLD_WAR_ASSAULT=1
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 Delayimp.lib ddraw.lib vorbisfile60.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib largeint.lib /nologo /subsystem:windows /map:"O:\Galatea\Galatea.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\Galatea\Galatea.exe" /OPT:REF /DELAYLOAD:glide3x.dll /DELAYLOAD:mpiwin32.dll /order:@O:\Galatea\Galatea.prf
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 Delayimp.lib ddraw.lib vorbisfile60.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib largeint.lib /nologo /subsystem:windows /map:"O:\ColdWarAssault\ColdWarAssault.map" /debug /machine:I386 /nodefaultlib:"libcmt.lib" /out:"o:\ColdWarAssault\ColdWarAssault.exe" /OPT:REF /DELAYLOAD:glide3x.dll /DELAYLOAD:mpiwin32.dll /order:@O:\ColdWarAssault\ColdWarAssault.prf
# SUBTRACT LINK32 /pdb:none

!ENDIF 

# Begin Target

# Name "lib - Win32 Release"
# Name "lib - Win32 Debug"
# Name "lib - Win32 Testing"
# Name "lib - Win32 Demo Version"
# Name "lib - Win32 Super Release"
# Name "lib - Win32 Czech Super Release"
# Name "lib - Win32 MP Demo"
# Name "lib - Win32 VBS1"
# Name "lib - Win32 Super Designer"
# Name "lib - Win32 Super Server"
# Name "lib - Win32 Korean Super Release"
# Name "lib - Win32 Russian Super Release"
# Name "lib - Win32 Resistance"
# Name "lib - Win32 Resistance Server"
# Name "lib - Win32 Resistance Czech"
# Name "lib - Win32 Resistance Russian"
# Name "lib - Win32 Resistance Demo"
# Name "lib - Win32 VBS1 Server"
# Name "lib - Win32 VBS1 Demo"
# Name "lib - Win32 Galatea"
# Name "lib - Win32 ColdWarAssault"
# Begin Group "Resource"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\resource.h
# End Source File
# Begin Source File

SOURCE=.\resource.rc
# End Source File
# Begin Source File

SOURCE=.\resrc1.h
# End Source File
# Begin Source File

SOURCE=.\vbs1.ico
# End Source File
# Begin Source File

SOURCE=.\version.rc2
# End Source File
# End Group
# Begin Group "Library"

# PROP Default_Filter ""
# Begin Group "Headers"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\animation.hpp
# End Source File
# Begin Source File

SOURCE=.\aStar.hpp
# End Source File
# Begin Source File

SOURCE=.\camEffects.hpp
# End Source File
# Begin Source File

SOURCE=.\camera.hpp
# End Source File
# Begin Source File

SOURCE=.\CdaPfn.h
# End Source File
# Begin Source File

SOURCE=.\cdaPfnCond.h
# End Source File
# Begin Source File

SOURCE=.\clip2d.hpp
# End Source File
# Begin Source File

SOURCE=.\clipShape.hpp
# End Source File
# Begin Source File

SOURCE=.\clipVert.hpp
# End Source File
# Begin Source File

SOURCE=.\data3d.h
# End Source File
# Begin Source File

SOURCE=.\debugWin.hpp
# End Source File
# Begin Source File

SOURCE=.\detector.hpp
# End Source File
# Begin Source File

SOURCE=.\dikCodes.h
# End Source File
# Begin Source File

SOURCE=.\dynSound.hpp
# End Source File
# Begin Source File

SOURCE=.\Eax.h
# End Source File
# Begin Source File

SOURCE=.\edges.hpp
# End Source File
# Begin Source File

SOURCE=.\engine.hpp
# End Source File
# Begin Source File

SOURCE=.\engineDll.hpp
# End Source File
# Begin Source File

SOURCE=.\fileServer.hpp
# End Source File
# Begin Source File

SOURCE=.\fileServerMT.hpp
# End Source File
# Begin Source File

SOURCE=.\font.hpp
# End Source File
# Begin Source File

SOURCE=.\fsm.hpp
# End Source File
# Begin Source File

SOURCE=.\global.hpp
# End Source File
# Begin Source File

SOURCE=.\handledList.hpp
# End Source File
# Begin Source File

SOURCE=.\heap.hpp
# End Source File
# Begin Source File

SOURCE=.\imexhnd.h
# End Source File
# Begin Source File

SOURCE=.\interpol.hpp
# End Source File
# Begin Source File

SOURCE=.\Joystick.hpp
# End Source File
# Begin Source File

SOURCE=.\keyInput.hpp
# End Source File
# Begin Source File

SOURCE=.\keyLights.hpp
# End Source File
# Begin Source File

SOURCE=.\landFile.hpp
# End Source File
# Begin Source File

SOURCE=.\landscape.hpp
# End Source File
# Begin Source File

SOURCE=.\languages.hpp
# End Source File
# Begin Source File

SOURCE=.\lights.hpp
# End Source File
# Begin Source File

SOURCE=.\lockCache.hpp
# End Source File
# Begin Source File

SOURCE=.\mapFile.hpp
# End Source File
# Begin Source File

SOURCE=.\mapTypes.hpp
# End Source File
# Begin Source File

SOURCE=.\memcheck.hpp
# End Source File
# Begin Source File

SOURCE=.\MemGrow.hpp
# End Source File
# Begin Source File

SOURCE=..\cfg\minimal.hpp
# End Source File
# Begin Source File

SOURCE=.\multisync.hpp
# End Source File
# Begin Source File

SOURCE=.\object.hpp
# End Source File
# Begin Source File

SOURCE=.\objectClasses.hpp
# End Source File
# Begin Source File

SOURCE=.\occlusion.hpp
# End Source File
# Begin Source File

SOURCE=.\operMap.hpp
# End Source File
# Begin Source File

SOURCE=.\optionsUI.hpp
# End Source File
# Begin Source File

SOURCE=.\pactext.hpp
# End Source File
# Begin Source File

SOURCE=.\paramArchive.hpp
# End Source File
# Begin Source File

SOURCE=.\pathPlanner.hpp
# End Source File
# Begin Source File

SOURCE=.\pathSteer.hpp
# End Source File
# Begin Source File

SOURCE=.\perfProf.hpp
# End Source File
# Begin Source File

SOURCE=.\plane.hpp
# End Source File
# Begin Source File

SOURCE=.\poly.hpp
# End Source File
# Begin Source File

SOURCE=.\polyClip.hpp
# End Source File
# Begin Source File

SOURCE=.\quatrix.hpp
# End Source File
# Begin Source File

SOURCE=.\roads.hpp
# End Source File
# Begin Source File

SOURCE=.\rtAnimation.hpp
# End Source File
# Begin Source File

SOURCE=.\samples.hpp
# End Source File
# Begin Source File

SOURCE=.\saveVersion.hpp
# End Source File
# Begin Source File

SOURCE=.\scene.hpp
# End Source File
# Begin Source File

SOURCE=.\scripts.hpp
# End Source File
# Begin Source File

SOURCE=.\sentences.hpp
# End Source File
# Begin Source File

SOURCE=.\serial.hpp
# End Source File
# Begin Source File

SOURCE=.\Shape.hpp
# End Source File
# Begin Source File

SOURCE=.\speaker.hpp
# End Source File
# Begin Source File

SOURCE=.\statistics.hpp
# End Source File
# Begin Source File

SOURCE=.\textbank.hpp
# End Source File
# Begin Source File

SOURCE=.\threadSync.hpp
# End Source File
# Begin Source File

SOURCE=.\time.hpp
# End Source File
# Begin Source File

SOURCE=.\titEffects.hpp
# End Source File
# Begin Source File

SOURCE=.\tlVertex.hpp
# End Source File
# Begin Source File

SOURCE=.\types.hpp
# End Source File
# Begin Source File

SOURCE=.\vertex.hpp
# End Source File
# Begin Source File

SOURCE=.\visibility.hpp
# End Source File
# Begin Source File

SOURCE=.\visual.hpp
# End Source File
# Begin Source File

SOURCE=.\weapons.hpp
# End Source File
# Begin Source File

SOURCE=.\winpch.hpp
# End Source File
# Begin Source File

SOURCE=.\world.hpp
# End Source File
# End Group
# Begin Group "Network"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\chat.cpp
# End Source File
# Begin Source File

SOURCE=.\chat.hpp
# End Source File
# Begin Source File

SOURCE=.\idString.cpp
# End Source File
# Begin Source File

SOURCE=.\idString.hpp
# End Source File
# Begin Source File

SOURCE=.\messageFactory.hpp
# End Source File
# Begin Source File

SOURCE=.\netTransport.hpp
# End Source File
# Begin Source File

SOURCE=.\netTransportDPlay.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\netTransportNet.cpp
# End Source File
# Begin Source File

SOURCE=.\network.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\network.hpp
# End Source File
# Begin Source File

SOURCE=.\networkClient.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\networkImpl.hpp
# End Source File
# Begin Source File

SOURCE=.\networkMsg.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\networkObject.hpp
# End Source File
# Begin Source File

SOURCE=.\networkServer.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

!ENDIF 

# End Source File
# End Group
# Begin Group "AI"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\ai.hpp
# End Source File
# Begin Source File

SOURCE=.\aiArcade.cpp
# End Source File
# Begin Source File

SOURCE=.\aiCenter.cpp
# End Source File
# Begin Source File

SOURCE=.\aiDefs.hpp
# End Source File
# Begin Source File

SOURCE=.\aiGroup.cpp
# End Source File
# Begin Source File

SOURCE=.\aiPathPlanner.hpp
# End Source File
# Begin Source File

SOURCE=.\aiRadio.cpp
# End Source File
# Begin Source File

SOURCE=.\aiRadio.hpp
# End Source File
# Begin Source File

SOURCE=.\aiSubgroup.cpp
# End Source File
# Begin Source File

SOURCE=.\aiSubgroupFSM.cpp
# End Source File
# Begin Source File

SOURCE=.\aiTypes.hpp
# End Source File
# Begin Source File

SOURCE=.\aiUnit.cpp
# End Source File
# Begin Source File

SOURCE=.\arcadeTemplate.cpp
# End Source File
# Begin Source File

SOURCE=.\arcadeTemplate.hpp
# End Source File
# Begin Source File

SOURCE=.\ArcadeWaypoint.hpp
# End Source File
# End Group
# Begin Group "Sound"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\riffFile.hpp
# End Source File
# Begin Source File

SOURCE=.\soundDummy.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\soundDX8.cpp
# End Source File
# Begin Source File

SOURCE=.\soundDX8.hpp
# End Source File
# Begin Source File

SOURCE=.\soundScene.cpp
# End Source File
# Begin Source File

SOURCE=.\soundScene.hpp
# End Source File
# Begin Source File

SOURCE=.\soundSys.cpp
# End Source File
# Begin Source File

SOURCE=.\soundsys.hpp
# End Source File
# End Group
# Begin Group "UI"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\autoComplete.cpp
# End Source File
# Begin Source File

SOURCE=.\autoComplete.hpp
# End Source File
# Begin Source File

SOURCE=.\displayUI.cpp
# End Source File
# Begin Source File

SOURCE=.\displayUI.hpp
# End Source File
# Begin Source File

SOURCE=.\optionsUI.cpp
# End Source File
# Begin Source File

SOURCE=.\uiActions.hpp
# End Source File
# Begin Source File

SOURCE=.\uiArcade.cpp
# End Source File
# Begin Source File

SOURCE=.\uiContainers.cpp
# End Source File
# Begin Source File

SOURCE=.\uiControls.cpp
# End Source File
# Begin Source File

SOURCE=.\uiControls.hpp
# End Source File
# Begin Source File

SOURCE=.\uiControlsExt.cpp
# End Source File
# Begin Source File

SOURCE=.\uiMap.cpp
# End Source File
# Begin Source File

SOURCE=.\uiMap.hpp
# End Source File
# Begin Source File

SOURCE=.\uiMapExport.cpp
# End Source File
# Begin Source File

SOURCE=.\uiMapExt.cpp
# End Source File
# End Group
# Begin Group "Cloth"

# PROP Default_Filter ""
# Begin Group "Cloth Sources"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Cloth\ClothObject.cpp
# End Source File
# End Group
# Begin Group "Cloth Headers"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Cloth\ClothObject.h
# End Source File
# End Group
# End Group
# Begin Group "Vehicles"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\airplane.cpp
# End Source File
# Begin Source File

SOURCE=.\airplane.hpp
# End Source File
# Begin Source File

SOURCE=.\allAIVehicles.hpp
# End Source File
# Begin Source File

SOURCE=.\allVehicles.hpp
# End Source File
# Begin Source File

SOURCE=.\camera.cpp
# End Source File
# Begin Source File

SOURCE=.\cameraHold.cpp
# End Source File
# Begin Source File

SOURCE=.\cameraHold.hpp
# End Source File
# Begin Source File

SOURCE=.\car.cpp
# End Source File
# Begin Source File

SOURCE=.\car.hpp
# End Source File
# Begin Source File

SOURCE=.\detector.cpp
# End Source File
# Begin Source File

SOURCE=.\dynSound.cpp
# End Source File
# Begin Source File

SOURCE=.\envTracker.cpp
# End Source File
# Begin Source File

SOURCE=.\envTracker.hpp
# End Source File
# Begin Source File

SOURCE=.\fireplace.hpp
# End Source File
# Begin Source File

SOURCE=.\gearBox.cpp
# End Source File
# Begin Source File

SOURCE=.\gearBox.hpp
# End Source File
# Begin Source File

SOURCE=.\head.cpp
# End Source File
# Begin Source File

SOURCE=.\head.hpp
# End Source File
# Begin Source File

SOURCE=.\helicopter.cpp
# End Source File
# Begin Source File

SOURCE=.\helicopter.hpp
# End Source File
# Begin Source File

SOURCE=.\house.cpp
# End Source File
# Begin Source File

SOURCE=.\house.hpp
# End Source File
# Begin Source File

SOURCE=.\indicator.cpp
# End Source File
# Begin Source File

SOURCE=.\indicator.hpp
# End Source File
# Begin Source File

SOURCE=.\laserTarget.cpp
# End Source File
# Begin Source File

SOURCE=.\laserTarget.hpp
# End Source File
# Begin Source File

SOURCE=.\motorcycle.cpp
# End Source File
# Begin Source File

SOURCE=.\motorcycle.hpp
# End Source File
# Begin Source File

SOURCE=.\parachute.cpp
# End Source File
# Begin Source File

SOURCE=.\parachute.hpp
# End Source File
# Begin Source File

SOURCE=.\person.cpp
# End Source File
# Begin Source File

SOURCE=.\person.hpp
# End Source File
# Begin Source File

SOURCE=.\pilotHead.hpp
# End Source File
# Begin Source File

SOURCE=.\proxyWeapon.cpp
# End Source File
# Begin Source File

SOURCE=.\proxyWeapon.hpp
# End Source File
# Begin Source File

SOURCE=.\randomShape.cpp
# End Source File
# Begin Source File

SOURCE=.\randomShape.hpp
# End Source File
# Begin Source File

SOURCE=.\recoil.cpp
# End Source File
# Begin Source File

SOURCE=.\recoil.hpp
# End Source File
# Begin Source File

SOURCE=.\seaGull.cpp
# End Source File
# Begin Source File

SOURCE=.\seaGull.hpp
# End Source File
# Begin Source File

SOURCE=.\ship.cpp
# End Source File
# Begin Source File

SOURCE=.\ship.hpp
# End Source File
# Begin Source File

SOURCE=.\shots.cpp
# End Source File
# Begin Source File

SOURCE=.\shots.hpp
# End Source File
# Begin Source File

SOURCE=.\simul.cpp
# End Source File
# Begin Source File

SOURCE=.\smokes.cpp
# End Source File
# Begin Source File

SOURCE=.\smokes.hpp
# End Source File
# Begin Source File

SOURCE=.\soldierOld.cpp
# End Source File
# Begin Source File

SOURCE=.\soldierOld.hpp
# End Source File
# Begin Source File

SOURCE=.\tank.cpp
# End Source File
# Begin Source File

SOURCE=.\tank.hpp
# End Source File
# Begin Source File

SOURCE=.\tankOrCar.cpp
# End Source File
# Begin Source File

SOURCE=.\tankOrCar.hpp
# End Source File
# Begin Source File

SOURCE=.\target.cpp
# End Source File
# Begin Source File

SOURCE=.\thing.cpp
# End Source File
# Begin Source File

SOURCE=.\thing.hpp
# End Source File
# Begin Source File

SOURCE=.\tracks.cpp
# End Source File
# Begin Source File

SOURCE=.\tracks.hpp
# End Source File
# Begin Source File

SOURCE=.\transport.cpp
# End Source File
# Begin Source File

SOURCE=.\transport.hpp
# End Source File
# Begin Source File

SOURCE=.\vehicle.hpp
# End Source File
# Begin Source File

SOURCE=.\vehicleAI.cpp
# End Source File
# Begin Source File

SOURCE=.\vehicleAI.hpp
# End Source File
# Begin Source File

SOURCE=.\vehicleTypes.cpp
# End Source File
# Begin Source File

SOURCE=.\wounds.cpp
# End Source File
# Begin Source File

SOURCE=.\wounds.hpp
# End Source File
# End Group
# Begin Group "Sources"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\camEffects.cpp
# End Source File
# Begin Source File

SOURCE=.\dammage.cpp
# End Source File
# Begin Source File

SOURCE=.\debugTrap.cpp
# End Source File
# Begin Source File

SOURCE=.\debugTrap.hpp
# End Source File
# Begin Source File

SOURCE=.\debugWin.cpp
# End Source File
# Begin Source File

SOURCE=.\debugWinImpl.hpp
# End Source File
# Begin Source File

SOURCE=.\dynEnum.cpp
# End Source File
# Begin Source File

SOURCE=.\dynEnum.hpp
# End Source File
# Begin Source File

SOURCE=.\edges.cpp
# End Source File
# Begin Source File

SOURCE=.\engine.cpp
# End Source File
# Begin Source File

SOURCE=.\engineDraw.cpp
# End Source File
# Begin Source File

SOURCE=.\fileServer.cpp
# End Source File
# Begin Source File

SOURCE=.\fireplace.cpp
# End Source File
# Begin Source File

SOURCE=.\fixed.hpp
# End Source File
# Begin Source File

SOURCE=.\font.cpp
# End Source File
# Begin Source File

SOURCE=.\fontDraw.cpp
# End Source File
# Begin Source File

SOURCE=.\frame.cpp
# End Source File
# Begin Source File

SOURCE=.\frameInv.cpp
# End Source File
# Begin Source File

SOURCE=.\frameInv.hpp
# End Source File
# Begin Source File

SOURCE=.\fsm.cpp
# End Source File
# Begin Source File

SOURCE=.\geography.cpp
# End Source File
# Begin Source File

SOURCE=.\imexhnd.cpp
# End Source File
# Begin Source File

SOURCE=.\interpol.cpp
# End Source File
# Begin Source File

SOURCE=.\invisibleVeh.cpp
# End Source File
# Begin Source File

SOURCE=.\invisibleVeh.hpp
# End Source File
# Begin Source File

SOURCE=.\Joystick.cpp
# End Source File
# Begin Source File

SOURCE=.\keyLights.cpp
# End Source File
# Begin Source File

SOURCE=.\landSave.cpp
# End Source File
# Begin Source File

SOURCE=.\lights.cpp
# End Source File
# Begin Source File

SOURCE=.\mainCommon.cpp
# End Source File
# Begin Source File

SOURCE=.\mainHelper.cpp
# End Source File
# Begin Source File

SOURCE=.\manActs.hpp
# End Source File
# Begin Source File

SOURCE=.\mapFile.cpp
# End Source File
# Begin Source File

SOURCE=.\MemGrow.cpp
# End Source File
# Begin Source File

SOURCE=.\memWin32.cpp
# End Source File
# Begin Source File

SOURCE=.\move_actions.hpp
# End Source File
# Begin Source File

SOURCE=.\networks.cpp
# End Source File
# Begin Source File

SOURCE=.\objectClasses.cpp
# End Source File
# Begin Source File

SOURCE=.\objLine.cpp
# End Source File
# Begin Source File

SOURCE=.\objLine.hpp
# End Source File
# Begin Source File

SOURCE=.\objTrash.cpp
# End Source File
# Begin Source File

SOURCE=.\onExit.cpp
# End Source File
# Begin Source File

SOURCE=.\packFiles.cpp
# End Source File
# Begin Source File

SOURCE=.\packFiles.hpp
# End Source File
# Begin Source File

SOURCE=.\pactext.cpp
# End Source File
# Begin Source File

SOURCE=.\paramArchive.cpp
# End Source File
# Begin Source File

SOURCE=.\pathPlanner.cpp
# End Source File
# Begin Source File

SOURCE=.\pathSteer.cpp
# End Source File
# Begin Source File

SOURCE=.\perfLog.cpp
# End Source File
# Begin Source File

SOURCE=.\plane.cpp
# End Source File
# Begin Source File

SOURCE=.\poly.cpp
# End Source File
# Begin Source File

SOURCE=.\profiler.cpp
# End Source File
# Begin Source File

SOURCE=.\progress.cpp
# End Source File
# Begin Source File

SOURCE=.\progress.hpp
# End Source File
# Begin Source File

SOURCE=.\resIncl.hpp
# End Source File
# Begin Source File

SOURCE=.\roads.cpp
# End Source File
# Begin Source File

SOURCE=.\scripts.cpp
# End Source File
# Begin Source File

SOURCE=.\serial.cpp
# End Source File
# Begin Source File

SOURCE=.\Shape.cpp
# End Source File
# Begin Source File

SOURCE=.\shapeFile.cpp
# End Source File
# Begin Source File

SOURCE=.\shapeLand.cpp
# End Source File
# Begin Source File

SOURCE=.\speaker.cpp
# End Source File
# Begin Source File

SOURCE=.\SpecLods.hpp
# End Source File
# Begin Source File

SOURCE=.\statistics.cpp
# End Source File
# Begin Source File

SOURCE=.\strFormat.cpp
# End Source File
# Begin Source File

SOURCE=.\strFormat.hpp
# End Source File
# Begin Source File

SOURCE=.\threadSync.cpp
# End Source File
# Begin Source File

SOURCE=.\time.cpp
# End Source File
# Begin Source File

SOURCE=.\titEffects.cpp
# End Source File
# Begin Source File

SOURCE=.\txtBank.cpp
# End Source File
# Begin Source File

SOURCE=.\txtPreload.cpp
# End Source File
# Begin Source File

SOURCE=.\txtPreload.hpp
# End Source File
# Begin Source File

SOURCE=.\version.ver
# End Source File
# Begin Source File

SOURCE=.\vertex.cpp
# End Source File
# Begin Source File

SOURCE=.\viewer.cpp
# End Source File
# Begin Source File

SOURCE=.\visibility.cpp
# End Source File
# Begin Source File

SOURCE=.\vtuneProf.cpp
# End Source File
# Begin Source File

SOURCE=.\vtuneProf.hpp
# End Source File
# Begin Source File

SOURCE=.\wave.cpp
# End Source File
# Begin Source File

SOURCE=.\weapons.cpp
# End Source File
# Begin Source File

SOURCE=.\world.cpp
# End Source File
# Begin Source File

SOURCE=.\wpch.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /Yc"wpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD BASE CPP /Yc"wpch.hpp"
# ADD CPP /Yc"wpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD BASE CPP /Yc"wpch.hpp"
# ADD CPP /Yc"wpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD BASE CPP /Yc"wpch.hpp"
# ADD CPP /Yc"wpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD BASE CPP /Yc"wpch.hpp"
# ADD CPP /Yc"wpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD BASE CPP /Yc"wpch.hpp"
# ADD CPP /Yc"wpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD BASE CPP /Yc"wpch.hpp"
# ADD CPP /Yc"wpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD BASE CPP /Yc"wpch.hpp"
# ADD CPP /Yc"wpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD BASE CPP /Yc"wpch.hpp"
# ADD CPP /Yc"wpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD BASE CPP /Yc"wpch.hpp"
# ADD CPP /Yc"wpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD BASE CPP /Yc"wpch.hpp"
# ADD CPP /Yc"wpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD BASE CPP /Yc"wpch.hpp"
# ADD CPP /Yc"wpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD BASE CPP /Yc"wpch.hpp"
# ADD CPP /Yc"wpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD BASE CPP /Yc"wpch.hpp"
# ADD CPP /Yc"wpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD BASE CPP /Yc"wpch.hpp"
# ADD CPP /Yc"wpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /Yc"wpch.hpp"
# ADD CPP /Yc"wpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /Yc"wpch.hpp"
# ADD CPP /Yc"wpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /Yc"wpch.hpp"
# ADD CPP /Yc"wpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /Yc"wpch.hpp"
# ADD CPP /Yc"wpch.hpp"

!ENDIF 

# End Source File
# End Group
# Begin Group "VerConfig"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\afxConfig.h
# End Source File
# Begin Source File

SOURCE=.\coldWarAssaultConfig.h
# End Source File
# Begin Source File

SOURCE=.\dedServerConfig.h
# End Source File
# Begin Source File

SOURCE=.\demoConfig.h
# End Source File
# Begin Source File

SOURCE=.\desReleaseConfig.h
# End Source File
# Begin Source File

SOURCE=.\galateaConfig.h
# End Source File
# Begin Source File

SOURCE=.\mpDemoConfig.h
# End Source File
# Begin Source File

SOURCE=.\mpTestConfig.h
# End Source File
# Begin Source File

SOURCE=.\mpTestServerConfig.h
# End Source File
# Begin Source File

SOURCE=.\normalConfig.h
# End Source File
# Begin Source File

SOURCE=.\resistanceConfig.h
# End Source File
# Begin Source File

SOURCE=.\resistanceServerConfig.h
# End Source File
# Begin Source File

SOURCE=.\sReleaseConfig.h
# End Source File
# Begin Source File

SOURCE=.\VBS1Config.h
# End Source File
# Begin Source File

SOURCE=.\VBS1DemoConfig.h
# End Source File
# Begin Source File

SOURCE=.\VBS1ServerConfig.h
# End Source File
# Begin Source File

SOURCE=.\wpch.hpp
# End Source File
# Begin Source File

SOURCE=.\xboxConfig.h
# End Source File
# End Group
# Begin Group "InGameUI"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\inGameUI.cpp
# End Source File
# Begin Source File

SOURCE=.\inGameUI.hpp
# End Source File
# Begin Source File

SOURCE=.\inGameUIDraw.cpp
# End Source File
# Begin Source File

SOURCE=.\InGameUIImpl.hpp
# End Source File
# End Group
# Begin Group "Global Headers"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\vkCodes.h
# End Source File
# End Group
# Begin Group "Checksum"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\crc.cpp
# End Source File
# Begin Source File

SOURCE=.\crc.hpp
# End Source File
# Begin Source File

SOURCE=.\integrity.hpp
# End Source File
# End Group
# Begin Group "JPG"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\jpgImport.cpp
# End Source File
# Begin Source File

SOURCE=.\jpgImport.hpp
# End Source File
# End Group
# Begin Group "GameSpy"

# PROP Default_Filter ""
# Begin Group "queryreporting"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\GameSpy\queryreporting\gqueryreporting.c
# End Source File
# Begin Source File

SOURCE=.\GameSpy\queryreporting\gqueryreporting.h
# End Source File
# End Group
# Begin Group "cengine"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\GameSpy\cengine\goaceng.h
# End Source File
# Begin Source File

SOURCE=.\GameSpy\cengine\gserver.c
# End Source File
# Begin Source File

SOURCE=.\GameSpy\cengine\gserver.h
# End Source File
# Begin Source File

SOURCE=.\GameSpy\cengine\gserverlist.c
# End Source File
# Begin Source File

SOURCE=.\GameSpy\cengine\gutil.c
# End Source File
# Begin Source File

SOURCE=.\GameSpy\cengine\gutil.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\GameSpy\darray.c
# End Source File
# Begin Source File

SOURCE=.\GameSpy\darray.h
# End Source File
# Begin Source File

SOURCE=.\GameSpy\hashtable.c
# End Source File
# Begin Source File

SOURCE=.\GameSpy\hashtable.h
# End Source File
# Begin Source File

SOURCE=.\GameSpy\md5.h
# End Source File
# Begin Source File

SOURCE=.\GameSpy\md5c.c
# End Source File
# Begin Source File

SOURCE=.\GameSpy\nonport.c
# End Source File
# Begin Source File

SOURCE=.\GameSpy\nonport.h
# End Source File
# End Group
# Begin Group "MBCS"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\mbcs.cpp
# End Source File
# Begin Source File

SOURCE=.\mbcs.hpp
# End Source File
# End Group
# Begin Group "ParamFileExt"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\paramFileExt.cpp
# End Source File
# Begin Source File

SOURCE=.\paramFileExt.hpp
# End Source File
# End Group
# Begin Group "QStreamExt"

# PROP Default_Filter ""
# Begin Group "Encryption"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Encryption\VBS1.cpp
# End Source File
# Begin Source File

SOURCE=.\Encryption\XOR1024.cpp
# End Source File
# End Group
# Begin Source File

SOURCE=.\qstreamExt.cpp
# End Source File
# End Group
# Begin Group "EvaluatorExt"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\expressExt.cpp
# End Source File
# Begin Source File

SOURCE=.\gameStateExt.cpp
# End Source File
# Begin Source File

SOURCE=.\gameStateExt.hpp
# End Source File
# End Group
# Begin Group "StringtableExt"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\stringids.hpp
# End Source File
# Begin Source File

SOURCE=.\stringtableExt.cpp
# End Source File
# Begin Source File

SOURCE=.\stringtableExt.hpp
# End Source File
# End Group
# Begin Group "Optimize"

# PROP Default_Filter ""
# Begin Group "Speed"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\animation.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /O2
# ADD CPP /O2

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\clipdraw.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /O2
# ADD CPP /O2

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\clipShape.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /O2
# ADD CPP /O2

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\collisions.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /O2
# ADD CPP /O2

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\drawpoly.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /O2
# ADD CPP /O2

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\landscape.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /O2
# ADD CPP /O2

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\lockCache.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\memTable.cpp
# End Source File
# Begin Source File

SOURCE=.\object.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /O2
# ADD CPP /O2

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\occlusion.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /O2
# ADD CPP /O2

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\operMap.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /O2
# ADD CPP /O2

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\rtAnimation.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /O2
# ADD CPP /O2

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\scene.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /O2
# ADD CPP /O2

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\shadow.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /O2
# ADD CPP /O2

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\ShapeDraw.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\StdLib.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /O2
# ADD CPP /O2

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\transLight.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /O2
# ADD CPP /O2

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /O2
# ADD CPP /O2

!ENDIF 

# End Source File
# End Group
# Begin Group "Header"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\MemHeap.hpp
# End Source File
# Begin Source File

SOURCE=.\v3quads.hpp
# End Source File
# End Group
# Begin Group "Intel"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\v3QuadsP3.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

# ADD CPP /D "_USE_INTEL_COMPILER"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# ADD BASE CPP /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT BASE CPP /Ox /Os
# ADD CPP /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT CPP /Ox /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD BASE CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT BASE CPP /Os
# ADD CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD BASE CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT BASE CPP /Os
# ADD CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD BASE CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT BASE CPP /Os
# ADD CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD BASE CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT BASE CPP /Os
# ADD CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD BASE CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT BASE CPP /Os
# ADD CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD BASE CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT BASE CPP /Os
# ADD CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD BASE CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT BASE CPP /Os
# ADD CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD BASE CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT BASE CPP /Os
# ADD CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD BASE CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT BASE CPP /Os
# ADD CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD BASE CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT BASE CPP /Os
# ADD CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD BASE CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT BASE CPP /Os
# ADD CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD BASE CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT BASE CPP /Os
# ADD CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD BASE CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT BASE CPP /Os
# ADD CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD BASE CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT BASE CPP /Os
# ADD CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT BASE CPP /Os
# ADD CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT BASE CPP /Os
# ADD CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT BASE CPP /Os
# ADD CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT BASE CPP /Os
# ADD CPP /Ox /Ot /Ob2 /D "_USE_INTEL_COMPILER"
# SUBTRACT CPP /Os

!ENDIF 

# End Source File
# End Group
# Begin Group "Core"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\colorsK.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\colorsP.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\v3quads.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ENDIF 

# End Source File
# End Group
# Begin Group "CoreHeader"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\colors.hpp
# End Source File
# Begin Source File

SOURCE=.\colorsFloat.hpp
# End Source File
# Begin Source File

SOURCE=.\colorsK.hpp
# End Source File
# Begin Source File

SOURCE=.\vecTempl.hpp
# End Source File
# End Group
# End Group
# Begin Group "ASE"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\ASE\ASEMasterSDK.c
# End Source File
# Begin Source File

SOURCE=.\ASE\ASEMasterSDK.h
# End Source File
# Begin Source File

SOURCE=.\ASE\readme.txt
# End Source File
# End Group
# Begin Source File

SOURCE=.\appFrameExt.hpp
# End Source File
# Begin Source File

SOURCE=.\keyInput.cpp
# End Source File
# Begin Source File

SOURCE=.\material.cpp
# End Source File
# Begin Source File

SOURCE=.\material.hpp
# End Source File
# Begin Source File

SOURCE=.\serializeBinExt.cpp
# End Source File
# Begin Source File

SOURCE=.\serializeBinExt.hpp
# End Source File
# End Group
# Begin Group "Docs"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Docs\cheats.txt
# End Source File
# Begin Source File

SOURCE=..\Docs\devstudio.txt
# End Source File
# Begin Source File

SOURCE=..\Docs\konvence.doc
# End Source File
# Begin Source File

SOURCE=..\Docs\notes.txt
# End Source File
# Begin Source File

SOURCE=..\Docs\ofp_modules.doc
# End Source File
# Begin Source File

SOURCE=..\Docs\OFP_ProgDoc.chm
# End Source File
# Begin Source File

SOURCE=..\Docs\OFPDoc.cfg
# End Source File
# Begin Source File

SOURCE=..\Docs\Poznamky.txt
# End Source File
# Begin Source File

SOURCE=..\Docs\sensors.html
# End Source File
# Begin Source File

SOURCE=..\Docs\vassistTempl.txt
# End Source File
# End Group
# Begin Group "D3D"

# PROP Default_Filter ""
# Begin Group "Shaders"

# PROP Default_Filter "psa;vsa"
# Begin Group "Output"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Shaders\night.h

!IF  "$(CFG)" == "lib - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Shaders\nightDetail.h

!IF  "$(CFG)" == "lib - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Shaders\psDetail.h

!IF  "$(CFG)" == "lib - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Shaders\psDetailSpecular.h
# End Source File
# Begin Source File

SOURCE=.\Shaders\psGrass.h

!IF  "$(CFG)" == "lib - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Shaders\psNightDetailSpecular.h
# End Source File
# Begin Source File

SOURCE=.\Shaders\psNightSpecular.h
# End Source File
# Begin Source File

SOURCE=.\Shaders\psNormal.h

!IF  "$(CFG)" == "lib - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Shaders\psSpecular.h
# End Source File
# Begin Source File

SOURCE=.\Shaders\psWaterMap.h
# End Source File
# Begin Source File

SOURCE=.\Shaders\vsDayGrass.h

!IF  "$(CFG)" == "lib - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

!ENDIF 

# End Source File
# End Group
# Begin Source File

SOURCE=.\Shaders\night.psa

!IF  "$(CFG)" == "lib - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# Begin Custom Build - NVAsm $(InputPath)
InputPath=.\Shaders\night.psa
InputName=night

"shaders/$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	c:\bin\nvasm\nvasm.exe -h $(InputPath) shaders/$(InputName).h

# End Custom Build

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# Begin Custom Build - NVAsm $(InputPath)
InputPath=.\Shaders\night.psa
InputName=night

"shaders/$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	c:\bin\nvasm\nvasm.exe -h $(InputPath) shaders/$(InputName).h

# End Custom Build

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# Begin Custom Build - NVAsm $(InputPath)
InputPath=.\Shaders\night.psa
InputName=night

"shaders/$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	c:\bin\nvasm\nvasm.exe -h $(InputPath) shaders/$(InputName).h

# End Custom Build

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# Begin Custom Build - NVAsm $(InputPath)
InputPath=.\Shaders\night.psa
InputName=night

"shaders/$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	c:\bin\nvasm\nvasm.exe -h $(InputPath) shaders/$(InputName).h

# End Custom Build

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Shaders\nightDetail.psa

!IF  "$(CFG)" == "lib - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# Begin Custom Build - NVAsm $(InputPath)
InputPath=.\Shaders\nightDetail.psa
InputName=nightDetail

"shaders/$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	c:\bin\nvasm\nvasm.exe -h $(InputPath) shaders/$(InputName).h

# End Custom Build

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# Begin Custom Build - NVAsm $(InputPath)
InputPath=.\Shaders\nightDetail.psa
InputName=nightDetail

"shaders/$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	c:\bin\nvasm\nvasm.exe -h $(InputPath) shaders/$(InputName).h

# End Custom Build

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# Begin Custom Build - NVAsm $(InputPath)
InputPath=.\Shaders\nightDetail.psa
InputName=nightDetail

"shaders/$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	c:\bin\nvasm\nvasm.exe -h $(InputPath) shaders/$(InputName).h

# End Custom Build

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# Begin Custom Build - NVAsm $(InputPath)
InputPath=.\Shaders\nightDetail.psa
InputName=nightDetail

"shaders/$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	c:\bin\nvasm\nvasm.exe -h $(InputPath) shaders/$(InputName).h

# End Custom Build

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Shaders\psDecl.h

!IF  "$(CFG)" == "lib - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Shaders\psDetail.psa

!IF  "$(CFG)" == "lib - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# Begin Custom Build - NVAsm $(InputPath)
InputPath=.\Shaders\psDetail.psa
InputName=psDetail

"shaders/$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	c:\bin\nvasm\nvasm.exe -h $(InputPath) shaders/$(InputName).h

# End Custom Build

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# Begin Custom Build - NVAsm $(InputPath)
InputPath=.\Shaders\psDetail.psa
InputName=psDetail

"shaders/$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	c:\bin\nvasm\nvasm.exe -h $(InputPath) shaders/$(InputName).h

# End Custom Build

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# Begin Custom Build - NVAsm $(InputPath)
InputPath=.\Shaders\psDetail.psa
InputName=psDetail

"shaders/$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	c:\bin\nvasm\nvasm.exe -h $(InputPath) shaders/$(InputName).h

# End Custom Build

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# Begin Custom Build - NVAsm $(InputPath)
InputPath=.\Shaders\psDetail.psa
InputName=psDetail

"shaders/$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	c:\bin\nvasm\nvasm.exe -h $(InputPath) shaders/$(InputName).h

# End Custom Build

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Shaders\psDetailSpecular.psa
# End Source File
# Begin Source File

SOURCE=.\Shaders\psGrass.psa

!IF  "$(CFG)" == "lib - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# Begin Custom Build - NVAsm $(InputPath)
InputPath=.\Shaders\psGrass.psa
InputName=psGrass

"shaders/$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	c:\bin\nvasm\nvasm.exe -h $(InputPath) shaders/$(InputName).h

# End Custom Build

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# Begin Custom Build - NVAsm $(InputPath)
InputPath=.\Shaders\psGrass.psa
InputName=psGrass

"shaders/$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	c:\bin\nvasm\nvasm.exe -h $(InputPath) shaders/$(InputName).h

# End Custom Build

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# Begin Custom Build - NVAsm $(InputPath)
InputPath=.\Shaders\psGrass.psa
InputName=psGrass

"shaders/$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	c:\bin\nvasm\nvasm.exe -h $(InputPath) shaders/$(InputName).h

# End Custom Build

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# Begin Custom Build - NVAsm $(InputPath)
InputPath=.\Shaders\psGrass.psa
InputName=psGrass

"shaders/$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	c:\bin\nvasm\nvasm.exe -h $(InputPath) shaders/$(InputName).h

# End Custom Build

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Shaders\psNightDetailSpecular.psa
# End Source File
# Begin Source File

SOURCE=.\Shaders\psNightSpecular.psa
# End Source File
# Begin Source File

SOURCE=.\Shaders\psNormal.psa

!IF  "$(CFG)" == "lib - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# Begin Custom Build - NVAsm $(InputPath)
InputPath=.\Shaders\psNormal.psa
InputName=psNormal

"shaders/$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	c:\bin\nvasm\nvasm.exe -h $(InputPath) shaders/$(InputName).h

# End Custom Build

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# Begin Custom Build - NVAsm $(InputPath)
InputPath=.\Shaders\psNormal.psa
InputName=psNormal

"shaders/$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	c:\bin\nvasm\nvasm.exe -h $(InputPath) shaders/$(InputName).h

# End Custom Build

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# Begin Custom Build - NVAsm $(InputPath)
InputPath=.\Shaders\psNormal.psa
InputName=psNormal

"shaders/$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	c:\bin\nvasm\nvasm.exe -h $(InputPath) shaders/$(InputName).h

# End Custom Build

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# Begin Custom Build - NVAsm $(InputPath)
InputPath=.\Shaders\psNormal.psa
InputName=psNormal

"shaders/$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	c:\bin\nvasm\nvasm.exe -h $(InputPath) shaders/$(InputName).h

# End Custom Build

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Shaders\psSpecular.psa
# End Source File
# Begin Source File

SOURCE=.\Shaders\psWaterMapNight.h
# End Source File
# Begin Source File

SOURCE=.\Shaders\Shaders.txt

!IF  "$(CFG)" == "lib - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Shaders\vsDayGrass.vsa

!IF  "$(CFG)" == "lib - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

USERDEP__VSDAY="shaders/vsDecl.h"	
# Begin Custom Build - NVAsm $(InputPath)
InputPath=.\Shaders\vsDayGrass.vsa
InputName=vsDayGrass

"shaders/$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	c:\bin\nvasm\nvasm.exe -h $(InputPath) shaders/$(InputName).h

# End Custom Build

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

USERDEP__VSDAY="shaders/vsDecl.h"	
# Begin Custom Build - NVAsm $(InputPath)
InputPath=.\Shaders\vsDayGrass.vsa
InputName=vsDayGrass

"shaders/$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	c:\bin\nvasm\nvasm.exe -h $(InputPath) shaders/$(InputName).h

# End Custom Build

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

USERDEP__VSDAY="shaders/vsDecl.h"	
# Begin Custom Build - NVAsm $(InputPath)
InputPath=.\Shaders\vsDayGrass.vsa
InputName=vsDayGrass

"shaders/$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	c:\bin\nvasm\nvasm.exe -h $(InputPath) shaders/$(InputName).h

# End Custom Build

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

USERDEP__VSDAY="shaders/vsDecl.h"	
# Begin Custom Build - NVAsm $(InputPath)
InputPath=.\Shaders\vsDayGrass.vsa
InputName=vsDayGrass

"shaders/$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	c:\bin\nvasm\nvasm.exe -h $(InputPath) shaders/$(InputName).h

# End Custom Build

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Shaders\vsDecl.h

!IF  "$(CFG)" == "lib - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

!ENDIF 

# End Source File
# End Group
# Begin Source File

SOURCE=.\d3dDefs.hpp
# End Source File
# Begin Source File

SOURCE=.\engd3d.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\engdd.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\engdd.hpp
# End Source File
# Begin Source File

SOURCE=.\txtd3d.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\txtd3d.hpp
# End Source File
# End Group
# Begin Group "Element"

# PROP Default_Filter ""
# Begin Group "PCH"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\PCH\afxConfig.h
# End Source File
# Begin Source File

SOURCE=..\..\El\PCH\ext_options.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\PCH\libIncludes.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\PCH\normalConfig.h
# End Source File
# Begin Source File

SOURCE=..\..\El\PCH\sReleaseConfig.h
# End Source File
# Begin Source File

SOURCE=..\..\El\PCH\stdIncludes.h
# End Source File
# End Group
# Begin Group "ParamFile"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\ParamFile\paramFile.cpp
# ADD CPP /YX"../elementpch.hpp"
# End Source File
# Begin Source File

SOURCE=..\..\El\ParamFile\paramFile.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\ParamFile\paramFileCtx.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\ParamFile\paramFileCtx.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\ParamFile\paramFileEval.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\ParamFile\paramFileStringtable.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\ParamFile\paramFileUsePreprocC.cpp
# End Source File
# End Group
# Begin Group "Evaluator"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\Evaluator\express.cpp
# ADD CPP /YX"../elementpch.hpp"
# End Source File
# Begin Source File

SOURCE=..\..\El\Evaluator\express.hpp
# End Source File
# End Group
# Begin Group "Common"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\Common\enumNames.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\common\globalAlive.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /YX"elementpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\El\Common\isaac.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Common\perfLog.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Common\randomGen.cpp
# ADD CPP /YX"../elementpch.hpp"
# End Source File
# Begin Source File

SOURCE=..\..\El\Common\randomGen.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Common\randomJames.cpp
# ADD CPP /I "w:\c\Hierarchy"
# End Source File
# Begin Source File

SOURCE=..\..\El\Common\randomJames.h
# End Source File
# End Group
# Begin Group "QStream"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\QStream\fileCompress.cpp
# ADD CPP /YX"../elementpch.hpp"
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\fileCompress.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\fileinfo.h
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\fileMapping.cpp
# ADD CPP /YX"../elementpch.hpp"
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\fileMapping.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\qstream\fileOverlapped.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\qstream\fileOverlapped.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\qbstream.cpp
# ADD CPP /YX"../elementpch.hpp"
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\QBStream.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\qstream.cpp
# ADD CPP /YX"../elementpch.hpp"
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\QStream.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\serializeBin.cpp
# ADD CPP /YX"../elementpch.hpp"
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\serializeBin.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\ssCompress.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /O2 /YX"../elementpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

# ADD CPP /YX"../elementpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# ADD CPP /YX"../elementpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD CPP /YX"../elementpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD CPP /YX"../elementpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD CPP /YX"../elementpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD CPP /YX"../elementpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD CPP /YX"../elementpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD CPP /YX"../elementpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD CPP /YX"../elementpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD CPP /YX"../elementpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD CPP /YX"../elementpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD BASE CPP /YX"../elementpch.hpp"
# ADD CPP /YX"../elementpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD BASE CPP /YX"../elementpch.hpp"
# ADD CPP /YX"../elementpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD BASE CPP /YX"../elementpch.hpp"
# ADD CPP /YX"../elementpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD BASE CPP /YX"../elementpch.hpp"
# ADD CPP /YX"../elementpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD BASE CPP /YX"../elementpch.hpp"
# ADD CPP /YX"../elementpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /YX"../elementpch.hpp"
# ADD CPP /YX"../elementpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /YX"../elementpch.hpp"
# ADD CPP /YX"../elementpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /YX"../elementpch.hpp"
# ADD CPP /YX"../elementpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /YX"../elementpch.hpp"
# ADD CPP /YX"../elementpch.hpp"

!ENDIF 

# End Source File
# End Group
# Begin Group "PreprocC"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\PreprocC\Preproc.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\PreprocC\Preproc.h
# End Source File
# Begin Source File

SOURCE=..\..\El\PreprocC\preprocC.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\PreprocC\preprocC.hpp
# End Source File
# End Group
# Begin Group "Stringtable"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\Stringtable\stringtable.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Stringtable\stringtable.hpp
# End Source File
# End Group
# Begin Group "XML"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\XML\xml.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /YX"elementpch.hpp"

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\El\XML\xml.hpp
# End Source File
# End Group
# Begin Group "ElementNetwork"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\Network\netapi.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Network\netapi.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Network\netchannel.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Network\netchannel.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Network\netglobal.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Network\netmessage.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Network\netmessage.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Network\netpch.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Network\netpeer.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Network\netpeer.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Network\netpool.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Network\peerfactory.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Network\peerfactory.hpp
# End Source File
# End Group
# Begin Group "Calendar"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\Calendar\pocalendar.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Calendar\pocalendar.hpp
# End Source File
# End Group
# Begin Group "Modules"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\Modules\modules.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Modules\modules.hpp
# End Source File
# End Group
# Begin Group "Math"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\Math\math3d.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Math\math3dK.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\El\Math\math3dK.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Math\math3dP.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\El\Math\math3dP.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Math\math3DPK.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\El\Math\math3dT.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\El\Math\math3dT.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Math\mathDefs.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Math\mathOpt.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\El\Math\mathOpt.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Math\matrix.asm
# End Source File
# Begin Source File

SOURCE=..\..\El\Math\matrixP3.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

!ENDIF 

# End Source File
# End Group
# Begin Group "FreeOnDemand"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\FreeOnDemand\memFreeReq.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\FreeOnDemand\memFreeReq.hpp
# End Source File
# End Group
# Begin Group "Interfaces"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\Interfaces\iPreproc.hpp
# End Source File
# End Group
# Begin Source File

SOURCE=..\..\El\elementpch.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\initLibraryElement.hpp
# End Source File
# End Group
# Begin Group "Essence"

# PROP Default_Filter ""
# Begin Group "Algorithms"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Es\Algorithms\bSearch.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Algorithms\crc32.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Algorithms\crc32.h
# End Source File
# Begin Source File

SOURCE=..\..\Es\Algorithms\qsort.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Algorithms\splineEqD.hpp
# End Source File
# End Group
# Begin Group "EssenceCommon"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Es\Common\filenames.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Common\fltopts.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Common\global.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Common\mathND.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Common\win.h
# End Source File
# End Group
# Begin Group "Containers"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Es\Containers\array.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\array2D.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\bankArray.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\bankInitArray.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\bigArray.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\bitmask.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\bitmask.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\boolArray.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\cachelist.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\compactBuf.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\hashMap.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\list.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\maps.cpp
# ADD CPP /I "w:\c\Hierarchy"
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\maps.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\quadtree.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\quadtreeCont.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\rStringArray.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\smallArray.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\sortedArray.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\staticArray.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\streamArray.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\typeDefines.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\typeGenTemplates.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\typeOpts.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\typeTemplates.hpp
# End Source File
# End Group
# Begin Group "Framework"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Es\Framework\appFrame.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Framework\appFrame.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Framework\debugLog.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Framework\logflags.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Framework\netlog.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Framework\netlog.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Framework\optDefault.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Framework\optEnable.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Framework\potime.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Framework\potime.hpp
# End Source File
# End Group
# Begin Group "Memory"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Es\Memory\checkMem.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\debugNew.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\defNew.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\defNormNew.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\fastAlloc.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\fastAlloc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\memAlloc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\normalNew.hpp
# End Source File
# End Group
# Begin Group "Strings"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Es\Strings\bstring.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Strings\rString.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\Es\Strings\rString.hpp
# End Source File
# End Group
# Begin Group "Types"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Es\Types\enum_decl.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\lLinks.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# ADD BASE CPP /Ot
# SUBTRACT BASE CPP /Os
# ADD CPP /Ot
# SUBTRACT CPP /Os

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\lLinks.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\memtype.h
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\pointers.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\removeLinks.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\scopeLock.hpp
# End Source File
# End Group
# Begin Group "Threads"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Es\Threads\pocritical.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Threads\pocritical.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Threads\posemaphore.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Threads\posemaphore.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Threads\pothread.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Threads\pothread.hpp
# End Source File
# End Group
# Begin Source File

SOURCE=..\..\Es\essencepch.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\platform.hpp
# End Source File
# End Group
# Begin Source File

SOURCE=.\diagModes.hpp
# End Source File
# Begin Source File

SOURCE=.\engDummy.cpp
# End Source File
# Begin Source File

SOURCE=".\howto-1.96.txt"
# End Source File
# Begin Source File

SOURCE=.\main.cpp
# End Source File
# Begin Source File

SOURCE=.\main3dfx.cpp

!IF  "$(CFG)" == "lib - Win32 Release"

# SUBTRACT CPP /WX

!ELSEIF  "$(CFG)" == "lib - Win32 Debug"

!ELSEIF  "$(CFG)" == "lib - Win32 Testing"

# SUBTRACT BASE CPP /WX
# SUBTRACT CPP /WX

!ELSEIF  "$(CFG)" == "lib - Win32 Demo Version"

# SUBTRACT BASE CPP /WX
# SUBTRACT CPP /WX

!ELSEIF  "$(CFG)" == "lib - Win32 Super Release"

# SUBTRACT BASE CPP /WX
# SUBTRACT CPP /WX

!ELSEIF  "$(CFG)" == "lib - Win32 Czech Super Release"

# SUBTRACT BASE CPP /WX
# SUBTRACT CPP /WX

!ELSEIF  "$(CFG)" == "lib - Win32 MP Demo"

# SUBTRACT BASE CPP /WX
# SUBTRACT CPP /WX

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1"

# SUBTRACT BASE CPP /WX
# SUBTRACT CPP /WX

!ELSEIF  "$(CFG)" == "lib - Win32 Super Designer"

# SUBTRACT BASE CPP /WX
# SUBTRACT CPP /WX

!ELSEIF  "$(CFG)" == "lib - Win32 Super Server"

# SUBTRACT BASE CPP /WX
# SUBTRACT CPP /WX

!ELSEIF  "$(CFG)" == "lib - Win32 Korean Super Release"

# SUBTRACT BASE CPP /WX
# SUBTRACT CPP /WX

!ELSEIF  "$(CFG)" == "lib - Win32 Russian Super Release"

# SUBTRACT BASE CPP /WX
# SUBTRACT CPP /WX

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance"

# SUBTRACT BASE CPP /WX
# SUBTRACT CPP /WX

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Server"

# SUBTRACT BASE CPP /WX
# SUBTRACT CPP /WX

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Czech"

# SUBTRACT BASE CPP /WX
# SUBTRACT CPP /WX

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Russian"

# SUBTRACT BASE CPP /WX
# SUBTRACT CPP /WX

!ELSEIF  "$(CFG)" == "lib - Win32 Resistance Demo"

# SUBTRACT BASE CPP /WX
# SUBTRACT CPP /WX

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Server"

# SUBTRACT BASE CPP /WX
# SUBTRACT CPP /WX

!ELSEIF  "$(CFG)" == "lib - Win32 VBS1 Demo"

# SUBTRACT BASE CPP /WX
# SUBTRACT CPP /WX

!ELSEIF  "$(CFG)" == "lib - Win32 Galatea"

# SUBTRACT BASE CPP /WX
# SUBTRACT CPP /WX

!ELSEIF  "$(CFG)" == "lib - Win32 ColdWarAssault"

# SUBTRACT BASE CPP /WX
# SUBTRACT CPP /WX

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\roxxe.hpp
# End Source File
# Begin Source File

SOURCE=.\versionNo.h
# End Source File
# Begin Source File

SOURCE=.\winapp.cpp
# End Source File
# End Target
# End Project
