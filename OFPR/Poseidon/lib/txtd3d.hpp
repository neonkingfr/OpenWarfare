#ifdef _MSC_VER
#pragma once
#endif

#ifndef _TXTD3D_HPP
#define _TXTD3D_HPP

#if !_DISABLE_GUI

#include "types.hpp"
#include <El/Math/math3d.hpp>

#include "d3ddefs.hpp"
#include <Es/Memory/fastAlloc.hpp>

#include <Es/Containers/cachelist.hpp>
#include "pactext.hpp"
#include "textbank.hpp"
#include "colors.hpp"

#include <Es/Memory/normalNew.hpp>

class HMipCacheD3D: public CLDLink
{
	public:
	// only textures are stored in VRAM memory
	TextureD3D *texture;

	USE_FAST_ALLOCATOR;
};

typedef HMipCacheD3D HSysCacheD3D;

#include <Es/Memory/debugNew.hpp>

typedef CLList<HMipCacheD3D> D3DMipCacheRoot;
typedef D3DMipCacheRoot D3DSysCacheRoot;

struct TEXTUREDESC8
{
	int w,h;
	int nMipmaps;
	D3DFORMAT format;
	D3DPOOL pool;
};

class SurfaceInfoD3D
{
	private:
	ComRef<IDirect3DTexture8> _surface;

	static int _nextId;
	int _id;

	public:
	int _totalSize; // expected size for allocation
	int _usedSize; // actually allocated size
	int _w,_h;
	int _nMipmaps;
	PacFormat _format;

	int SizeExpected() const {return _totalSize;}
	int SizeUsed() const {return _usedSize;}

	int GetCreationID() const {return _id;}

	public:

	static int CalculateSize
	(
		IDirect3DDevice8 *dDraw, const TEXTUREDESC8 &desc, PacFormat format,
		int totalSize=-1
	);
	HRESULT CreateSurface
	(
		IDirect3DDevice8 *dDraw, const TEXTUREDESC8 &desc, PacFormat format,
		int totalSize=-1
	);
	void Free(bool lastRef, int refValue=0 );
	// TODO: use ComRef more consistently
	// add AddRef to ComRef contructor?
	// or use Ref for COM types
	ComRef<IDirect3DTexture8> &GetSurface() {return _surface;}
	const ComRef<IDirect3DTexture8> &GetSurface() const {return _surface;}
};

TypeIsMovableZeroed(SurfaceInfoD3D);

struct D3DSurfaceToDestroy
{
	int _id;
	ComRef<IDirect3DTexture8> _surface;
};

TypeIsMovableZeroed(D3DSurfaceToDestroy);

class D3DSurfaceDestroyer
{
	// manage order of batch of destroyed surfaces
	// DX memory manager releases surfaces much faster
	// when released in reverse order of creation
	AutoArray<D3DSurfaceToDestroy> _surfaces;
	public:
	D3DSurfaceDestroyer();
	~D3DSurfaceDestroyer();
	void Add( const SurfaceInfoD3D &surf );
	void DestroyAll();
};


//class MipmapLevelD3D: public PacLevelMem {};

#include <Es/Memory/normalNew.hpp>

#define ASSERT_INIT() Assert(_initialized);

class TextureD3D: public Texture
{
	typedef Texture base;

	friend class TextBankD3D;
	friend class EngineDD;

	private:
	//PacPalette _pal;

	SRef<ITextureSource> _src; //!< texture source provider

	Ref<TextureD3D> _interpolate;
	float _iFactor; // current interpolation factor

	// store all neccessary LOD information for multibase addressing
	bool _isDetail;
	bool _useDetail;
	bool _initialized;

	//bool _isPaa;
	int _maxSize; // landscape textures have different limit than object...

	// this information is same for all mipmaps, but is also copied in mipmap header
	int _nMipmaps;
	PacLevelMem _mipmaps[MAX_MIPMAPS];

	SurfaceInfoD3D _surface; // video memory copy
	SurfaceInfoD3D _smallSurface; // small video memory copy
	SurfaceInfoD3D _sysSurface; // system memory copy

	signed char _largestUsed;
	signed char _smallLoaded; // which mipmap level loaded to _smallSurface
	signed char _levelLoaded; // which mipmap level loaded to _surface
	signed char _systemLoaded;  // which mipmap level loaded to _sysSurface
	signed char _levelNeededThisFrame;
	signed char _levelNeededLastFrame;
	signed char _inUse; // is currently used for background operation - do not release

	HMipCacheD3D *_cache; // is allocated in heap?
	HSysCacheD3D *_sysCache; // is allocated in system heap?

	TextureD3DHandle GetSmallSurface() const {return _smallSurface.GetSurface();}
	TextureD3DHandle GetBigSurface() const {return _surface.GetSurface();}

	int LevelNeeded() const
	{
		return
		(
			_levelNeededThisFrame<_levelNeededLastFrame 
			? _levelNeededThisFrame : _levelNeededLastFrame
		);
	}
	
	public:
	TextureD3D();
	~TextureD3D();
	
	//int LoadPalette( QIStream &in );
	//static int SkipPalette( QIStream &in ) {return PacPalette::Skip(in);} // load palette
	
	// handle single mip-map levels
	void InitDDSD( TEXTUREDESC8 &ddsd, int levelMin, bool enableDXT );
	bool CopyToVRAM( SurfaceInfoD3D &surface, int levelMin );

	int LoadLevels( int levelMin ); // load to VRAM
	int LoadLevelsSys( int levelMin ); // load to sysmem

	TextureD3DHandle GetHandle() const
	{
		TextureD3DHandle handle = GetBigSurface();
		return handle ? handle : GetSmallSurface();
	}
	const SurfaceInfoD3D &GetSurface() const
	{
		return _surface.GetSurface() ? _surface : _smallSurface;
	}

	private:
	
	void MemoryReleased(); // mark as released

	int TotalSize( int levelMin ) const;
	void ReleaseSmall( bool store=false, D3DSurfaceDestroyer *batch=NULL );
	int LoadSmall(); // load small texture version

	void SysCacheUse( D3DSysCacheRoot &list );

	void SystemReleased(); // mark as released
	void ReuseSystem( SurfaceInfoD3D &surf ); // reuse immediatelly

	public:	
	

	void ReleaseSystem( bool store=false, D3DSurfaceDestroyer *batch=NULL ); // move to reusable
	void ReleaseMemory( bool store=false, D3DSurfaceDestroyer *batch=NULL ); // move to reusable
	void ReuseMemory( SurfaceInfoD3D &surf ); // reuse immediatelly

	bool IsAlpha() const {ASSERT_INIT();return _src && _src->IsAlpha();}
	int AMaxSize() const {return _maxSize;}
	void SetMaxSize( int size );
	void SetMultitexturing( int type );

	bool VerifyChecksum( const MipInfo &mip ) const;
	
	//int Load( int level );
	
	void SetMipmapRange( int min, int max );

	//! do as little preparation as possible
	int Init( const char *name );

	//! do real preparation - is called before any real data is used
	void DoLoadHeaders();
	//! ask file server to preload headers from file
	void PreloadHeaders();
	
	//! load header if necessary (non-virtual)
	void LoadHeadersNV() const
	{
		if (_initialized) return;
		const_cast<TextureD3D *>(this)->DoLoadHeaders();
	}

	void LoadHeaders();

	const PacLevelMem *Mipmap( int level ) const
	{
		ASSERT_INIT();
		return &_mipmaps[level];
	}

	const AbstractMipmapLevel &AMipmap( int level ) const
	{
		ASSERT_INIT();
		return _mipmaps[level];
	}
	AbstractMipmapLevel &AMipmap( int level )
	{
		ASSERT_INIT();
		return _mipmaps[level];
	}
	
	int NMipmaps() const
	{
		ASSERT_INIT();
		return _nMipmaps;
	}
	//const PacPalette &GetPalette() const {return _pal;}

	int ANMipmaps() const {ASSERT_INIT();return _nMipmaps;}
	void ASetNMipmaps( int n ); // {_nMipmaps=n;}
	int AWidth( int level=0 ) const {LoadHeadersNV();return _mipmaps[level]._w;}
	int AHeight( int level=0 ) const {LoadHeadersNV();return _mipmaps[level]._h;}
	bool IsTransparent() const {ASSERT_INIT();return _src && _src->IsTransparent();}

	Color GetPixel( int level, float u, float v ) const;
	Color GetColor() {LoadHeaders();return _src ? _src->GetAverageColor() : HBlack;}

	int Width( int level ) const {ASSERT_INIT();return _mipmaps[level]._w;}
	int Height( int level ) const {ASSERT_INIT();return _mipmaps[level]._h;}

	void CacheUse( D3DMipCacheRoot &list );

	NoCopy(TextureD3D);
	USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

class TextBankD3D: public AbstractTextBank
{
	typedef AbstractTextBank base;

	friend class TextureD3D;
	
	int _maxTextureMemory; // inital memory free for textures
	int _limitAlocatedTextures; // sum of currently allocated textures + free space - reserved space
	int _limitSystemTextures;
	int _reserveTextureMemory; // we may know from experience some VRAM must be left
	int _freeTextureMemory; // updated every time CheckTextureMemory is called

	int _maxSmallTexturePixels; // how many pixels of each texture should always be loaded

	LLinkArray<TextureD3D> _texture;
	Ref<TextureD3D> _detail;
	Ref<TextureD3D> _specular;
	Ref<TextureD3D> _grass;
	Ref<TextureD3D> _white; // required for pixel shader - NULL texture is black
	Ref<TextureD3D> _waterBump; // water bump map
	
	// system texture bank
	AutoArray<SurfaceInfoD3D> _freeSysTextures; // free system surfaces
	AutoArray<SurfaceInfoD3D> _freeTextures; // free VRAM surfaces

	// no heaps - use DirectDraw surfaces and only count allocated memory
	//int _vramSize; // total ammount of allocated system memory textures

	int _totalAllocated; // allocated in vidmem heap (_freeTextures and _texture)
	int _systemAllocated; // allocated in system heap (_freeSysTextures and _texture)
	
	EngineDD *_engine;

	D3DMipCacheRoot _thisFrameWholeUsed;
	D3DMipCacheRoot _lastFrameWholeUsed;
	D3DMipCacheRoot _thisFramePartialUsed;
	D3DMipCacheRoot _lastFramePartialUsed;
	D3DMipCacheRoot _previousUsed;
	
	int _thisFrameCopied; // blit/copied textures (in pixels)
	int _thisFrameAlloc; // VRAM allocations/deallocations (count)
	
	D3DSysCacheRoot _systemUsed; // keep this copy

	public:
	TextBankD3D( EngineDD *engine );
	~TextBankD3D();

	int FreeTextureMemory();

	private:

	int FreeAGPMemory();
	void CheckTextureMemory();
	void InitDetailTextures();

		
	void PreCreateVRAM( int reserve, bool randomOrder ); // pre-create surfaces
	void PreCreateSys(); // pre-create surfaces

	void PreCreateSurfaces();

	public:
	virtual int NTextures() const {return _texture.Size();}
	virtual Texture *GetTexture( int i ) const {return _texture[i];}

	void Compact();

	void ReloadAll(); // texture cache was destroyed - reload

	void Preload(); // initalize VRAM
	void FlushTextures(); // switch data - flush and preload
	void FlushBank(QFBank *bank);

	protected:
	int FindFree();
	
	TextureD3D *Copy( int from );
	int Find( RStringB name1, TextureD3D *interpolate=NULL );
	
	int FindSurface
	(
		int w, int h, int nMipmaps, PacFormat format,
		const AutoArray<SurfaceInfoD3D> &array
	) const;

	// find, create if not found
	int FindSystem
	(
		int w, int h, int nMipmaps, PacFormat format
	) const;
	void UseSystem( SurfaceInfoD3D &surf, const TEXTUREDESC8 &ddsd, PacFormat format );
	void AddSystem( SurfaceInfoD3D &surf );
	int DeleteLastSystem( int need, D3DSurfaceDestroyer *batch=NULL );

	int FindReleased
	(
		int w, int h, int nMipmaps, PacFormat format
	) const
	{
		return FindSurface(w,h,nMipmaps,format,_freeTextures);
	}
	void AddReleased( SurfaceInfoD3D &surf );
	void UseReleased
	(
		SurfaceInfoD3D &surf,
		const TEXTUREDESC8 &ddsd, PacFormat format
	);
	int DeleteLastReleased( int need, D3DSurfaceDestroyer *batch=NULL );

	void Reuse
	(
		SurfaceInfoD3D &surf, const TEXTUREDESC8 &ddsd, PacFormat format
	);
	void ReuseSys
	(
		SurfaceInfoD3D &surf, const TEXTUREDESC8 &ddsd, PacFormat format
	);

	public:
	Ref<Texture> Load( RStringB name );
	Ref<Texture> LoadInterpolated( RStringB n1, RStringB n2, float factor );

	void StopAll(); // stop all background activity (before shutdown)

	void StartFrame();
	void FinishFrame();

	MipInfo UseMipmap
	(
		Texture *texture, int level, int levelTop
	); // we need some mipmape
	TextureD3D *GetDetailTexture() const {return _detail;}
	TextureD3D *GetGrassTexture() const {return _grass;}
	TextureD3D *GetSpecularTexture() const {return _specular;}
	TextureD3D *GetWaterBumpMap() const {return _waterBump;}
	//void ReleaseMipmap();

	bool VerifyChecksums();
	bool ReserveMemory( D3DMipCacheRoot &root, int limit );
	bool ReserveMemory( int size );
	bool ForcedReserveMemory( int size );
	void ReleaseAllTextures(bool releaseSysMem=false);

	int CreateVRAMSurface
	(
		SurfaceInfoD3D &surface,
		const TEXTUREDESC8 &desc, PacFormat format, int totalSize
	);
	
	bool ReserveSystem( D3DSysCacheRoot &root, int limit );
	bool ReserveSystem( int size );
	bool ForcedReserveSystem( int size );
	
	void ReportTextures( const char *name );

	NoCopy(TextBankD3D);
};

#endif // !_DISABLE_GUI

#endif

