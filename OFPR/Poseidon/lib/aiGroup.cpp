// AI - implementation of Groups

#include "wpch.hpp"
#include "ai.hpp"
#include "person.hpp"
#include "aiRadio.hpp"
#include "global.hpp"

#include "world.hpp"
#include "landscape.hpp"
//#include "loadStream.hpp"
#include "paramArchive.hpp"
#include <El/Common/randomGen.hpp>

#include "camEffects.hpp"

//#include "strIncl.hpp"
#include "stringtableExt.hpp"

#include <Es/Algorithms/qsort.hpp>

#include "chat.hpp"
#include "network.hpp"
#include "perfProf.hpp"

#include <El/Common/enumNames.hpp>
#include "uiActions.hpp"

#include "move_actions.hpp"

///////////////////////////////////////////////////////////////////////////////
// Parameters

#include "aiDefs.hpp"

#pragma warning( disable: 4355 )

///////////////////////////////////////////////////////////////////////////////
// class AIGroup

// Construction and destruction
AIGroup::AIGroup()
	: _radio(CCGroup, this, RNRadio)
{
	_id = 0;

	_lastUpdateTime = Glob.time - 1.0F;
	_semaphore = SemaphoreYellow;
	_combatModeMinor = CMSafe; // default auto behaviour

	_nextCmdId = 0;
	_locksWP = 0;

	_enemiesDetected = 0;
	_unknownsDetected = 0;
	_lastEnemyDetected = Glob.time;
	// avoid assigning all groups at once

	_expensiveThinkFrac = toIntFloor(GRandGen.RandomValue()*1000);
	// perform first expensive think as soon as possible
	_expensiveThinkTime = Glob.time;
	_checkCenterDBase = Glob.time-60;
	
	_lastSendTargetTime = Glob.time-60;

	_disclosed = TIME_MIN;

	_maxStrength = 0;
	_forceCourage = -1;
	_courage = 1.0;
	_flee = false;

	_threshold = 0;
	_thresholdValid = Glob.time - 1.0;

	_nearestEnemyDist2 = 100; // unless we know better, assume 100 m

	_guardPosition = VZero;
}

inline void AIGroup::ExpensiveThinkDone()
{
	// calculate time of next expensive think
	// find start of this second
	// advance to start of next second, advance to our frac
	_expensiveThinkTime = Glob.time.Floor().AddMs(1000+_expensiveThinkFrac);
	#if 0
	LogF
	(
		"%.3f: Think %s (%d)",
		Glob.time-Time(0),(const char *)GetDebugName(),_expensiveThinkFrac
	);
	#endif
}

AIGroup::~AIGroup()
{
	_targetList.Clear();
	if (IsLocal())
	{
             NetworkId ni = GetNetworkId();
	     GetNetworkManager().DeleteObject(ni);
        }
}

AICenter* AIGroup::GetCenterS() const
{
	// not-inlined implementation
	return _center;
}



AIGroup *AIGroup::LoadRef(ParamArchive &ar)
{
	TargetSide side = TSideUnknown;
	int id;
	if (ar.SerializeEnum("side", side, 1) != LSOK) return NULL;
	if (ar.Serialize("id", id, 1) != LSOK) return NULL;
	AICenter *center = NULL;
	switch (side)
	{
		case TWest:
			center = GWorld->GetWestCenter();
			break;
		case TEast:
			center = GWorld->GetEastCenter();
			break;
		case TGuerrila:
			center = GWorld->GetGuerrilaCenter();
			break;
		case TCivilian:
			center = GWorld->GetCivilianCenter();
			break;
		case TLogic:
			center = GWorld->GetLogicCenter();
			break;
	}
	if (!center) return NULL;
	for (int i=0; i<center->NGroups(); i++)
	{
		AIGroup *grp = center->GetGroup(i);
		if (grp && grp->ID() == id) return grp; 
	}
	return NULL;
}

LSError AIGroup::SaveRef(ParamArchive &ar) const
{
	TargetSide side = GetCenter() ? GetCenter()->GetSide() : TSideUnknown;
	int id = ID();
	CHECK(ar.SerializeEnum("side", side, 1))
	CHECK(ar.Serialize("id", id, 1))
	return LSOK;
}

LSError AIGroup::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))

	// structure
	CHECK(ar.Serialize("Subgroups", _subgroups, 1))
	CHECK(ar.SerializeRef("MainSubgroup", _mainSubgroup, 1))
	CHECK(ar.SerializeRef("Leader", _leader, 1))
	CHECK(ar.SerializeRef("Center", _center, 1))
	CHECK(ar.Serialize("Radio", _radio, 1))

	if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
	{
		// build this array in first pass - will be used for refs
		for (int i=0; i<MAX_UNITS_PER_GROUP; i++) _units[i] = NULL;
		for (int i=0; i<_subgroups.Size(); i++)
		{
			AISubgroup *subgrp = _subgroups[i];
			Assert(subgrp);
			if (!subgrp) continue;
			for (int j=0; j<subgrp->NUnits(); j++)
			{
				AIUnit *unit = subgrp->GetUnit(j);
				Assert(unit);
				if (!unit) continue;
				int id = unit->ID();
				Assert(id > 0 && id <=MAX_UNITS_PER_GROUP);
				_units[id - 1] = unit;
			}
		}
	}

	for (int i=0; i<MAX_UNITS_PER_GROUP; i++) if (_units[i])
	{
		char buf[256];
		sprintf(buf,"AssignTarget%d",i);
		CHECK(ar.SerializeRef(buf, _assignTarget[i], 1))

		sprintf(buf,"AssignTargetState%d",i);
		CHECK(ar.SerializeEnum(buf, _assignTargetState[i], 1, TargetEnemyCombat))

		sprintf(buf,"assignValidUntil%d",i);
		CHECK(ar.Serialize(buf, _assignValidUntil[i], 1, Time(0)))
		sprintf(buf,"ammo%d",i);
		CHECK(ar.SerializeEnum(buf,_ammoState[i],1, AIUnit::RSNormal));
		sprintf(buf,"health%d",i);
		CHECK(ar.SerializeEnum(buf,_healthState[i],1, AIUnit::RSNormal));
		sprintf(buf,"dammage%d",i);
		CHECK(ar.SerializeEnum(buf,_dammageState[i],1, AIUnit::RSNormal));
		sprintf(buf,"fuel%d",i);
		CHECK(ar.SerializeEnum(buf,_fuelState[i],1, AIUnit::RSNormal));
		sprintf(buf,"down%d",i);
		CHECK(ar.Serialize(buf,_reportedDown[i],1, false));
		sprintf(buf,"reporttime%d",i);
		CHECK(ar.Serialize(buf,_reportBeforeTime[i],1, TIME_MAX));
	}

	CHECK(ar.Serialize("expensiveThinkFrac",_expensiveThinkFrac,1,0));
	CHECK(ar.Serialize("expensiveThinkTime",_expensiveThinkTime,1,Time(0)));
	CHECK(ar.Serialize("checkCenterDBase",_checkCenterDBase,1,Time(0)));
	// state
	CHECK(ar.Serialize("lastUpdateTime", _lastUpdateTime, 1))
	CHECK(ar.Serialize("checkTime", _checkTime, 1))	// used in seek and destroy waypoint
	CHECK(ar.SerializeEnum("semaphore", _semaphore, 1, SemaphoreYellow))
	CHECK(ar.SerializeEnum("combatModeMinor", _combatModeMinor, 1, CMSafe))
	CHECK(ar.Serialize("lastEnemyDetected", _lastEnemyDetected, 1))
	CHECK(ar.Serialize("nextCmdId", _nextCmdId, 2, 0))
	CHECK(ar.Serialize("locksWP", _locksWP, 2, 0))

	CHECK(ar.Serialize("nearestEnemyDist2", _nearestEnemyDist2, 1, 0));
	CHECK(ar.Serialize("enemiesDetected", _enemiesDetected, 1, 0))
	CHECK(ar.Serialize("unknownsDetected", _unknownsDetected, 1, 0))

	CHECK(ar.Serialize("disclosed", _disclosed, 1))

	// info
	CHECK(ar.Serialize("id", _id, 1))

/*
	if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
		Init();
*/

	CHECK(ar.Serialize("name", _letterName, 1, ""))
	CHECK(ar.Serialize("color", _colorName, 1, ""))
	CHECK(ar.Serialize("picture", _pictureName, 1, ""))
	
	if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
	{
		if (_letterName.GetLength() == 0 && _colorName.GetLength() == 0)
			Init();
		else
			SetIdentity(_letterName, _colorName, _pictureName);
	}

	// targets
	CHECK(ar.Serialize("targetList", _targetList, 1))

	// assigned vehicles
	CHECK(ar.SerializeRefs("Vehicles", _vehicles, 1))

	CHECK(ar.SerializeRef("OverlookTarget", _overlookTarget, 1))
	CHECK(ar.Serialize("guardPosition", _guardPosition, 1))

	// waypoints
	CHECK(ar.Serialize("Waypoints", _wp, 1))

	// flee mode
	CHECK(ar.Serialize("maxStrength", _maxStrength, 1))
	CHECK(ar.Serialize("forceCourage", _forceCourage, 1))
	CHECK(ar.Serialize("courage", _courage, 1))
	CHECK(ar.Serialize("flee", _flee, 1, false))

	// threshold for randomization
	CHECK(ar.Serialize("threshold", _threshold, 1))
	CHECK(ar.Serialize("thresholdValid", _thresholdValid, 1))

	return LSOK;
}

NetworkMessageType AIGroup::GetNMType(NetworkMessageClass cls) const
{
	switch (cls)
	{
	case NMCCreate:
		return NMTCreateAIGroup;
	case NMCUpdateGeneric:
		return NMTUpdateAIGroup;
	default:
		return NMTNone;
	}
}

//! network message indices for AIGroup class
class IndicesCreateAIGroup : public IndicesNetworkObject
{
	typedef IndicesNetworkObject base;

public:
	//@{
	//! index of field in message format
	int center;
	int id;
	int waypoints;
	//@}

	//! Constructor
	IndicesCreateAIGroup();
	NetworkMessageIndices *Clone() const {return new IndicesCreateAIGroup;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesCreateAIGroup::IndicesCreateAIGroup()
{
	center = -1;
	id = -1;
	waypoints = -1;
}

void IndicesCreateAIGroup::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);

	SCAN(center)
	SCAN(id)
	SCAN(waypoints)
}

//! Create network message indices for AIGroup class
NetworkMessageIndices *GetIndicesCreateAIGroup() {return new IndicesCreateAIGroup();}

IndicesUpdateAIGroup::IndicesUpdateAIGroup()
{
	mainSubgroup = -1;
	leader = -1;
	semaphore = -1;
	combatModeMinor = -1;
	// ??	_lastEnemyDetected
	// ?? _nextCmdId
	// ?? _locksWP
	enemiesDetected = -1;
	unknownsDetected = -1;
	// ?? disclosed = -1;
	// TODO: _targetList
	// ?? _vehicles
	// ?? _overlookTarget
	// ?? _guardPosition
	// TODO: waypoints
	// ?? _maxStrength
	forceCourage = -1;
	courage = -1;
	flee = -1;
	// ?? _threshold
	// ?? _thresholdValid

	waypointIndex = -1;
}

void IndicesUpdateAIGroup::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);

	SCAN(mainSubgroup)
	SCAN(leader)
	SCAN(semaphore)
	SCAN(combatModeMinor)
	// ??	_lastEnemyDetected
	// ?? _nextCmdId
	// ?? _locksWP
	SCAN(enemiesDetected)
	SCAN(unknownsDetected)
	// ?? SCAN(disclosed)
	// TODO: _targetList
	// ?? _vehicles
	// ?? _overlookTarget
	// ?? _guardPosition
	// TODO: waypoints
	// ?? _maxStrength
	SCAN(forceCourage)
	SCAN(courage)
	SCAN(flee)
	// ?? _threshold
	// ?? _thresholdValid

	SCAN(waypointIndex)
}

//! Create network message indices for AIGroup class
NetworkMessageIndices *GetIndicesUpdateAIGroup() {return new IndicesUpdateAIGroup();}

NetworkMessageFormat &AIGroup::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	switch (cls)
	{
	case NMCCreate:
		NetworkObject::CreateFormat(cls, format);
		format.Add("center", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Superior center"));
		format.Add("id", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Unique identifier in center"));
		format.Add("waypoints", NDTObjectArray, NCTNone, DEFVALUE_MSG(NMTWaypoint), DOC_MSG("List of waypoints"));
		break;
	case NMCUpdateGeneric:
		NetworkObject::CreateFormat(cls, format);
		format.Add("mainSubgroup", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Main subgroup"), ET_NOT_EQUAL, ERR_COEF_STRUCTURE);
		format.Add("leader", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Leader unit"), ET_NOT_EQUAL, ERR_COEF_STRUCTURE);
		format.Add("semaphore", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, SemaphoreYellow), DOC_MSG("Default combat mode"), ET_NOT_EQUAL, ERR_COEF_MODE);
		format.Add("combatModeMinor", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, CMSafe), DOC_MSG("Default behaviour"), ET_NOT_EQUAL, ERR_COEF_MODE);
		// ??	_lastEnemyDetected
		// ?? _nextCmdId
		// ?? _locksWP
		format.Add("enemiesDetected", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Number of detected enemies"), ET_ABS_DIF, ERR_COEF_VALUE_MINOR);
		format.Add("unknownsDetected", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Number of detected possible enemies"), ET_ABS_DIF, ERR_COEF_VALUE_MINOR);
		// ?? format.Add("disclosed", NDTTime, NCTNone, DEFVALUE(Time, Time(0)));
		// TODO: _targetList
		// ?? _vehicles
		// ?? _overlookTarget
		// ?? _guardPosition
		// TODO: waypoints
		// ?? _maxStrength
		format.Add("forceCourage", NDTFloat, NCTFloatM1ToP1, DEFVALUE(float, -1), DOC_MSG("Enforced (by designer) courage"), ET_ABS_DIF, ERR_COEF_MODE);
		format.Add("courage", NDTFloat, NCTFloat0To1, DEFVALUE(float, 1), DOC_MSG("Calculated courage"), ET_ABS_DIF, ERR_COEF_MODE);
		format.Add("flee", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Units are fleeing"), ET_NOT_EQUAL, ERR_COEF_MODE);
		// ?? _threshold
		// ?? _thresholdValid

		format.Add("waypointIndex", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Index of currently processing waypoint"), ET_NOT_EQUAL, ERR_COEF_MODE);
		break;
	default:
		NetworkObject::CreateFormat(cls, format);
		break;
	}
	return format;
}

AIGroup *AIGroup::CreateObject(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesCreateAIGroup *>(ctx.GetIndices()))
	const IndicesCreateAIGroup *indices = static_cast<const IndicesCreateAIGroup *>(ctx.GetIndices());

	AICenter *center;
	if (ctx.IdxTransferRef(indices->center, center) != TMOK) return NULL;
	if (!center) return NULL;
	int id;
	if (ctx.IdxTransfer(indices->id, id) != TMOK) return NULL;
	AIGroup *grp = new AIGroup();
	center->AddGroup(grp, id);

	NetworkId objectId;
	if (ctx.IdxTransfer(indices->objectCreator, objectId.creator) != TMOK) return NULL;
	if (ctx.IdxTransfer(indices->objectId, objectId.id) != TMOK) return NULL;
	grp->SetNetworkId(objectId);
	grp->SetLocal(false);

	// create waypoints etc.
	grp->TransferMsg(ctx);
	// create mission
	Mission mis;
	mis._action = Mission::Arcade;
	grp->ReceiveMission(mis);
	return grp;
}

void AIGroup::DestroyObject()
{
	if (NUnits() > 0) return;

	AICenter *center = _center;
	if (center)
	{
		_center = NULL;
		center->GroupRemoved(this);
	}
}

void ApplyEffects(AIGroup *group, int index);

TMError AIGroup::TransferMsg(NetworkMessageContext &ctx)
{
	switch (ctx.GetClass())
	{
	case NMCCreate:
		TMCHECK(NetworkObject::TransferMsg(ctx))
		{
			Assert(dynamic_cast<const IndicesCreateAIGroup *>(ctx.GetIndices()))
			const IndicesCreateAIGroup *indices = static_cast<const IndicesCreateAIGroup *>(ctx.GetIndices());

			if (ctx.IsSending())
			{

				ITRANSF_REF(center)
				ITRANSF(id)
			}
			TMCHECK(ctx.IdxTransferArray(indices->waypoints, _wp))
			if (!ctx.IsSending())
			{
				for (int i=0; i<_wp.Size(); i++)
				{
					ArcadeWaypointInfo &wInfo = _wp[i];
					for (int j=0; j<wInfo.synchronizations.Size(); j++)
					{
						int sync = wInfo.synchronizations[j];
						Assert(sync >= 0);
						if (sync >= synchronized.Size())
							synchronized.Resize(sync + 1);
						synchronized[sync].Add(this);
					}
				}
			}
		}
		break;
	case NMCUpdateGeneric:
		TMCHECK(NetworkObject::TransferMsg(ctx))
		{
			Assert(dynamic_cast<const IndicesUpdateAIGroup *>(ctx.GetIndices()))
			const IndicesUpdateAIGroup *indices = static_cast<const IndicesUpdateAIGroup *>(ctx.GetIndices());

			if (ctx.IsSending())
			{
				ITRANSF_REF(mainSubgroup)
				ITRANSF_REF(leader)
			}
			else
			{
				AISubgroup *subgrp;
				TMCHECK(ctx.IdxTransferRef(indices->mainSubgroup, subgrp))
				if (subgrp != _mainSubgroup)
				{
					if (_mainSubgroup)
					{
						LogF("Warning: main subgroup changed");
					}
					if (!subgrp)
						LogF("Warning: no main subgroup");
					else if (subgrp->GetGroup() != this)
						LogF("Warning: main subgroup not from this group");
					else
						_mainSubgroup = subgrp;
				}
				AIUnit *leader;
				TMCHECK(ctx.IdxTransferRef(indices->leader, leader))
				if (leader != _leader)
				{
					if (!leader)
						LogF("Warning: no group leader");
					else if (leader->GetGroup() != this)
						LogF("Warning: leader not from this group");
					else
						_leader = leader;
	/*
					if (leader && !leader->GetSubgroup()) AddUnit(leader);
					_leader = leader;
	*/
				}
			}
			ITRANSF_ENUM(semaphore)
			ITRANSF_ENUM(combatModeMinor)
			// ??	_lastEnemyDetected
			// ?? _nextCmdId
			// ?? _locksWP
			ITRANSF(enemiesDetected)
			ITRANSF(unknownsDetected)
			// ?? format.Add(indices->disclosed, NDTTime, NCTNone, DEFVALUE(Time, Time(0)));
			// TODO: _targetList
			// ?? _vehicles
			// ?? _overlookTarget
			// ?? _guardPosition
			// ?? _maxStrength
			ITRANSF(forceCourage)
			ITRANSF(courage)
			ITRANSF(flee)
			// ?? _threshold
			// ?? _thresholdValid
			DoAssert(GetCurrent());

			if (ctx.IsSending())
				TMCHECK(ctx.IdxTransfer(indices->waypointIndex, GetCurrent()->_fsm->Var(0)))
			else
			{
				int &oldIndex = GetCurrent()->_fsm->Var(0);
				int newIndex;
				TMCHECK(ctx.IdxTransfer(indices->waypointIndex, newIndex))
				if (newIndex != oldIndex)
				{
					ApplyEffects(this, oldIndex);
					oldIndex = newIndex;
				}
			}
		}
		break;
	default:
		TMCHECK(NetworkObject::TransferMsg(ctx))
		break;
	}
	return TMOK;
}

float AIGroup::CalculateError(NetworkMessageContext &ctx)
{
	float error = NetworkObject::CalculateError(ctx);
	{
		Assert(dynamic_cast<const IndicesUpdateAIGroup *>(ctx.GetIndices()))
		const IndicesUpdateAIGroup *indices = static_cast<const IndicesUpdateAIGroup *>(ctx.GetIndices());

		ICALCERR_NEQREF(AISubgroup, mainSubgroup, ERR_COEF_STRUCTURE)
		ICALCERR_NEQREF(AIUnit, leader, ERR_COEF_STRUCTURE)
		ICALCERR_NEQ(int, semaphore, ERR_COEF_MODE)
		ICALCERR_NEQ(int, combatModeMinor, ERR_COEF_MODE)
		ICALCERR_ABSDIF(int, enemiesDetected, ERR_COEF_VALUE_MINOR)
		ICALCERR_ABSDIF(int, unknownsDetected, ERR_COEF_VALUE_MINOR)
		ICALCERR_ABSDIF(float, forceCourage, ERR_COEF_MODE)
		ICALCERR_ABSDIF(float, courage, ERR_COEF_MODE)
		ICALCERR_NEQ(bool, flee, ERR_COEF_MODE)
		ICALCERRE_NEQ(int, waypointIndex, GetCurrent()->_fsm->Var(0), ERR_COEF_MODE)
		// TODO: Not implemented
	}
	return error;
}

int AIGroup::NUnits() const
{
	int n = 0;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = _units[i];
		if (unit)
			n++;
	}
	return n;
}

void AIGroup::SetUnit(int index, AIUnit *unit)
{
// low level function used for network transfer
// !!! avoid another using
	_units[index] = unit;
	InitUnitSlot(index);
}

int AIGroup::NSoldiers() const
{
	int n = 0;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = _units[i];
		if
		(
			unit &&
			unit->IsFreeSoldier() &&
			!unit->_vehicleAssigned
		)
		n++;
	}
	return n;
}

int AIGroup::NFreeVehicles() const
{
	int n = 0;
	for (int i=0; i<_vehicles.Size(); i++)
	{
		Transport *veh = _vehicles[i];
		if (veh && !veh->GetDriverAssigned())
			n++;
	}
	return n;
}

int AIGroup::NFreeManCargo() const
{
	int n = 0;
	for (int i=0; i<_vehicles.Size(); i++)
	{
		Transport *veh = _vehicles[i];
		if (veh)
			n += veh->GetMaxManCargo() - veh->NCargoAssigned();
	}
	return n;
}

void AIGroup::SetCombatModeMajor(CombatMode mode)
{
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = _units[i];
		if (unit) unit->SetCombatModeMajor(mode);
	}
//	if (_combatModeMinor < CMCombat) _combatModeMinor = mode;
}

void AIGroup::LockWP(bool lock)
{
	if (lock) _locksWP++; else _locksWP--;
}

void AIGroup::Disclose(AIUnit *sender)
{
	if( IsPlayerGroup() ) return;
	if (!sender) sender = Leader();

	if (!sender->IsLocal()) return;
	if (sender->GetLifeState()!=AIUnit::LSAlive) return;

	//LogF("%s: disclosed %.2f",(const char *)GetDebugName(),Glob.time-Time(0));
	_disclosed = Glob.time;
	
	if( !_flee )
	{
		// note: combatModeMinor is never CMCareless (it is autodetected)
		// combatModeMajor may be - but this is member of leader
		if (_combatModeMinor < CMCombat)
		{
			_combatModeMinor = CMCombat;
			AIUnit *unit = Leader();
			if
			(
				unit && unit->GetLifeState()==AIUnit::LSAlive &&
				unit->GetCombatModeMajor()!=CMCareless
			)
			{
				SendUnderFire(sender);

				// unload all units in car cargo position
				for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
				{
					AIUnit *unit = _units[i];
					if (!unit) continue;
					// going to combat - get out of some vehicles
					if (unit->IsInCargo())
					{
						Transport *trans = unit->GetVehicleIn();
						if (trans && trans->Type()->GetUnloadInCombat())
						{
							unit->OrderGetIn(false);
						}
					}
				}
			}
		}
		if (_semaphore!=SemaphoreBlue)
		{
			_semaphore = ApplyOpenFire(_semaphore, OFSOpenFire);
			PackedBoolArray list;
			for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
			{
				AIUnit *unit = _units[i];
				if (unit) list.Set(i, true);
			}
			SendOpenFire(OFSOpenFire, list);
		}
	}
}

// Changes of structure
void AIGroup::SubgroupRemoved(AISubgroup *subgrp)
{
	DoAssert(subgrp->NUnits() == 0);
	DoAssert(subgrp != MainSubgroup()); 

	int index = _subgroups.Find(subgrp);
	Assert(index >= 0);
	if (index < 0)
		return;

	_subgroups.Delete(index);
}

void AIGroup::UnitRemoved(AIUnit *unit)
{
	Assert(unit->GetGroup() == this);
	bool bLeader = (unit == Leader());

	int id = unit->ID() - 1;
	Assert(_units[id] == unit);
	_units[id] = NULL;
	unit->GetSubgroup()->UnitRemoved(unit);

	if (NUnits() == 0)
	{
//		GetCenter()->GroupRemoved(this);
		_leader = NULL;
		return;
	}
	if (bLeader)
		GetCenter()->SelectLeader(this);
}

void AIGroup::AddSubgroup(AISubgroup *subgrp)
{
	Assert(!subgrp->GetGroup());

	// add to group
	int index = _subgroups.Find(NULL);
	if (index >= 0) _subgroups[index] = subgrp;
	else _subgroups.Add(subgrp);
	subgrp->_group = this;
}

void AIGroup::CopyUnitSlot(int from, int to)
{
	_assignTarget[to] = _assignTarget[from];
	_healthState[to] = _healthState[from];
	_ammoState[to] = _ammoState[from];
	_fuelState[to] = _fuelState[from];
	_dammageState[to] = _dammageState[from];
	_reportedDown[to] = _reportedDown[from];
	_reportBeforeTime[to] = _reportBeforeTime[from];
	_assignTargetState[to] = _assignTargetState[from];
}

void AIGroup::InitUnitSlot(int i)
{
	_assignTarget[i] = NULL;
	_healthState[i] = AIUnit::RSNormal;
	_ammoState[i] = AIUnit::RSNormal;
	_fuelState[i] = AIUnit::RSNormal;
	_dammageState[i] = AIUnit::RSNormal;
	_reportedDown[i] = false;
	_reportBeforeTime[i] = TIME_MAX;
	_assignTargetState[i] = TargetEnemyCombat;
}

void AIGroup::RessurectUnit(AIUnit *unit)
{
	int i = unit->ID() - 1;
	_healthState[i] = AIUnit::RSNormal;
	_dammageState[i] = AIUnit::RSNormal;
	_reportedDown[i] = false;
	_reportBeforeTime[i] = TIME_MAX;
}

void AIGroup::AddUnit(AIUnit *unit, int id)
{
	if (!_mainSubgroup)
	{
		_mainSubgroup = new AISubgroup();
		AddSubgroup(_mainSubgroup);
	}
	if (unit->ID() != 0)
	{
		int i = unit->ID() - 1;
		if (_units[i] && _units[i] != unit)
		{
			Fail("Unit ID mismatch");
		}
		else
		{
			_units[i] = unit;
			InitUnitSlot(i);
			_mainSubgroup->AddUnit(unit);
		}
		return;
	}

	if (id > 0)
	{
		Ref<AIUnit> removed = (AIUnit*)_units[id - 1];
		_units[id - 1] = unit;

		unit->_id = id;
		_mainSubgroup->AddUnit(unit);
		if (removed)
		{
			for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
			{
				if (!_units[i])
				{
					_units[i] = removed.GetRef();
					removed->_id = i + 1;
					// move old information together with the unit
					CopyUnitSlot(id-1,i);
					// init information for the new unit
					InitUnitSlot(id-1);

					return;
				}
			}
			Fail("Cannot find id for unit");
		}
		else
		{
			InitUnitSlot(id-1);
		}
	}
	else
	{
		for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
		{
			if (!_units[i])
			{
				_units[i] = unit;
				InitUnitSlot(i);
				unit->_id = i + 1;
				_mainSubgroup->AddUnit(unit);
				return;
			}
		}
		Fail("Cannot find id for unit");
		_mainSubgroup->AddUnit(unit);
	}
}

void AIGroup::AddVehicle(Transport *veh)
{
	// delete free positions
	_vehicles.Compact();
	// try to find
	if (_vehicles.Find(veh) >= 0)
		return;

	// assign
	veh->AssignGroup(this);

	// add to array sorted by cost	
	int i;
	for (i=0; i<_vehicles.Size(); i++)
	{
		Transport *trans = _vehicles[i];
		if (veh->GetType()->GetCost() >= trans->GetType()->GetCost())
		{
			_vehicles.Insert(i, veh);
			return;
		}
	}
	_vehicles.Add(veh);
}

AIUnit *AIGroup::LeaderCandidate(AIUnit *dead)const
{
	// implementation of leader selection for group
	AIUnit *unit = NULL;
	Rank bestRank = RankPrivate;
	float bestExp = -FLT_MAX;
	// find unit with the highest rank
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *u = UnitWithID(i + 1);
//		group leader can be in cargo now
//		if (u && !u->GetInCargo())
		if (u && u!=dead)
		{
			Rank rank = u->GetPerson()->GetRank();
			if (rank > bestRank)
			{
				bestRank = rank;
				bestExp = u->GetPerson()->GetExperience();
				unit = u;
			}
			else if (rank == bestRank)
			{
				float exp = u->GetPerson()->GetExperience();
				if (exp > bestExp)
				{
					bestExp = exp;
					unit = u;
				}
			}
		}
	}

	return unit;
}

void AIGroup::SelectLeader(AIUnit *unit)
{
	Assert(unit);
	Assert(unit->GetSubgroup())
	Assert(unit->GetSubgroup()->GetGroup() == this);

	if (_leader != unit)
	{
		_leader = unit;
		CalculateCourage();

		AISubgroup *subgrp = unit->GetSubgroup();
		subgrp->SelectLeader();

		if (unit == GWorld->FocusOn())
		{
			subgrp->ClearAllCommands();
			if (subgrp != MainSubgroup() && MainSubgroup())
			{
				MainSubgroup()->ClearAllCommands();
			}
			// TODO: clear radio protocol
		}
	}
}

static int CompUnits(const OLink<AIUnit> *unit1, const OLink<AIUnit> *unit2)
{
	// check existence
	AIUnit *u1 = unit1->GetLink();
	if (!u1) return 1;
	AIUnit *u2 = unit2->GetLink();
	if (!u2) return -1;

	// check rank
	int rank1 = u1->GetPerson()->GetRank();
	int rank2 = u2->GetPerson()->GetRank();
	int diff = rank2 - rank1;
	if (diff != 0) return diff;
	
	// check function
	int pos1 = 1;
	int pos2 = 1;
	Transport *v1 = u1->GetVehicleIn(); 
	Transport *v2 = u2->GetVehicleIn();
	if (v1)
	{
		if (v1->CommanderBrain() == u1) pos1 = 2;
		else if (v1->GunnerBrain() == u1) pos1 = 0;
	}
	if (v2)
	{
		if (v2->CommanderBrain() == u2) pos2 = 2;
		else if (v2->GunnerBrain() == u2) pos2 = 0;
	}
	diff = pos2 - pos1;
	if (diff != 0) return diff;

	diff = u2->IsGroupLeader()-u1->IsGroupLeader();
	if (diff) return diff;
	// other cases
	return u1->ID() - u2->ID();
}

void AIGroup::SortUnits()
{
	QSort(_units, MAX_UNITS_PER_GROUP, CompUnits);

	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = _units[i];
		if (unit) unit->_id = i + 1;
	}
}

bool AIGroup::IsPlayerGroup() const
{
	return Leader() && Leader()->IsPlayer();
}

bool AIGroup::IsAnyPlayerGroup() const
{
	// note: currently group is always local where leader is local
	// therefore IsAnyPlayerGroup called on local group
	// should always be identical to IsPlayerGroup
	return Leader() && Leader()->IsAnyPlayer();
}

bool AIGroup::IsCameraGroup() const
{
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		if( !_units[i] ) continue;
		if( _units[i]->GetVehicle()==GWorld->CameraOn() )
		{
			return true;
		}
	}
	return false;
}

// Mind
bool AIGroup::Think()
{
	PROFILE_SCOPE(aiGrp);
#if LOG_THINK
	Log("Group %s think.", (const char *)GetDebugName());
#endif

	if (!Leader())
	{
		DoAssert(NUnits() == 0);
		return false;
	}

	if (GetCenter()->GetSide() == TLogic)
	{
		if (IsLocal()) DoUpdate();
		return false;
	}

	if (!IsLocal())
	{
		// only update target list
		if (Glob.time > _expensiveThinkTime)
		{
			ExpensiveThinkDone();
			CreateTargetList();
		}
		return false;
	}

	bool leaderAlive = Leader()->GetLifeState()==AIUnit::LSAlive;

	if (IsAnyPlayerGroup())
	{
		if (_flee) Unflee();
	}
	else
	{
		// flee only when leader is alive
		if (leaderAlive)
		{
			float strength = ActualStrength();
			if (strength < (1.0 - _courage) * _maxStrength)
			{
				if (!_flee)
				{
					Flee();
				}
			}
			else
			{
				if (_flee)
				{
					Unflee();
				}
			}
		}
	}

// check if state in state graph not changed
	{
		//PROFILE_SCOPE(aiGUp);
		Ref<AIGroup> safePtr = this;
		DoUpdate();
		Assert(safePtr->RefCounter() > 0);
		if (safePtr->RefCounter() == 1) return false; // group destroyed

		if (!Leader())
		{
			DoAssert(NUnits() == 0);
			return false;
		}
	}

// TODO: find better placement
	AISubgroup *leaderSubgroup = Leader()->GetSubgroup();
	if (leaderSubgroup != MainSubgroup() && !leaderSubgroup->HasCommand())
	{
		leaderSubgroup->JoinToSubgroup(MainSubgroup());
	}

	if( !IsAnyPlayerGroup() )
	{
		// do not join while scripted waypoint
		if (!GetScript())
		{
			// if there are subgroups in wait state, join them
			for( int i=0; i<_subgroups.Size(); i++ )
			{
				AISubgroup *sub = _subgroups[i];
				if( !sub ) continue;
				if( sub==MainSubgroup() ) continue;
				if( sub->HasCommand() ) continue;
				sub->JoinToSubgroup(MainSubgroup());
			}
		}
	}


	// if some enemy is near, we have to track near vincity continuously
	if (_nearestEnemyDist2<Square(100))
	{
		// force all units to track if necessary
		for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
		{
			AIUnit *unit = _units[i];
			if (!unit) continue;
			if (!unit->IsLocal()) continue;
			if (!unit->IsUnit()) continue;
			if (unit->GetLifeState()!=AIUnit::LSAlive) continue;
			if (unit->GetNearestEnemyDist2()<Square(100))
			{
				unit->GetVehicle()->TrackNearTargets(_targetList);
			}
		}
	}


	// visible enemy targets
	// if we have fresh assignement, do not assign anything
	if (Glob.time>_expensiveThinkTime)
	{
		// many functions need not be performed in every thing
		// once per second is enough for them
		//PROFILE_SCOPE(aiGTg);

		ExpensiveThinkDone();

		int oldED = _enemiesDetected;
		CreateTargetList();

		if (oldED == 0 && _enemiesDetected > 0 && !IsAnyPlayerGroup())
		{
			ReactToEnemyDetected();
		}

		if (leaderAlive)
		{
			//if (enemies)
			// min delay betweeen targets
			float minDelay = Leader()->GetInvAbility()*5;
			if (Glob.time>_lastSendTargetTime+minDelay)
			{
				//PROFILE_SCOPE(aiGAT);
				AssignTargets();
			}
		}

		if (_enemiesDetected > 0)
		{
			_lastEnemyDetected = Glob.time + GRandGen.PlusMinus(30.0f, 5.0f);
		}

	// try to assign unassigned vehicles
		if (!IsAnyPlayerGroup() && leaderAlive)
		{
			//PROFILE_SCOPE(aiGMs);
			AssignVehicles();
			GetInVehicles();


			// check resources
			bool ok = true;
			if (!CheckFuel()) ok = false;
			if (!CheckArmor()) ok = false;
			if (!CheckHealth()) ok = false;
			if (!CheckAmmo()) ok = false;

			if (ok)
			{
				Assert(_center);
				if (_center->GetRadio().Done() && _center->IsSupported(this, ATNone))
				{
					_center->GetRadio().Transmit
					(
						new RadioMessageSupportDone(this),
						_center->GetLanguage()
					);
				}
			}
		}

		// check if support needed
		CheckSupport();

		CheckAlive(); // check if unseen units are alive

	}

	if (!IsAnyPlayerGroup() && leaderAlive)
	{
		// autodetection only for non-player groups
		if (_combatModeMinor >= CMAware)
		{
			float timeFromCombat = Glob.time-_lastEnemyDetected;
			if (_disclosed>Glob.time-10*60)
			{
				saturateMin(timeFromCombat,Glob.time -(_disclosed + 15.0f));
			}
			if (timeFromCombat>0)
			{
				if (timeFromCombat>5*60)
				{
					_combatModeMinor = CMSafe;
				}
				else
				{
					if (_combatModeMinor>=CMCombat)
					{
						_combatModeMinor = CMAware;
						SendClear(Leader());
					}
				}
			}
		}
	}


	bool someOperPath=false;
	for (int i=0; i<NSubgroups(); i++)
	{
		// do not destroy subgroup during Think
		Ref <AISubgroup> subgrp = GetSubgroup(i);
		ThinkImportance prec = subgrp->CalculateImportance();
		if (subgrp->Think(prec))
			someOperPath = true;
	}


#if LOG_THINK
	Log("End of group %s think (%d).", (const char *)GetDebugName(), someOperPath);
#endif
	return someOperPath;
}

// Communication with subgroups
void CreateUnitsList(PackedBoolArray list, char *buffer);

void AIGroup::SendCommand(Command &cmd, bool channelCenter)
{
	cmd._id = _nextCmdId++;

	Assert(Leader());
#if LOG_COMM
	Log("Send command: Main subgroup of %s: Command %d (context %d)",
	(const char *)GetDebugName(), cmd._message, cmd._context);
#endif
	
	AIUnit *leader = Leader();
	if (!leader) return;
	
	// if leader is not alive, he should not be able to send commands
	if (leader && leader->GetLifeState()!=AIUnit::LSAlive)
	{
		LogF("No command, leader %s is not alive",(const char *)leader->GetDebugName());
		return;
	}
	bool transmit = true;
	if (leader->GetSubgroup() == MainSubgroup())
	{
		MainSubgroup()->ReceiveCommand(cmd);
		transmit = false;
	}

	// do not say if its near
	if (!transmit)
	{
		AIUnit *subLeader = MainSubgroup()->Leader();
		if (subLeader && subLeader->Position().Distance2(cmd._destination) < Square(6)) return;
	}
	
	Assert(GetCenter());
	RadioChannel &radio = channelCenter ? GetCenter()->GetRadio() : GetRadio();
	// TODO: check: list is empty - what is it good for
	PackedBoolArray list;
	radio.Transmit
	(
		new RadioMessageCommand(this, list, cmd, true, transmit),
		GetCenter()->GetLanguage()
	);
}

void AIGroup::SendCommand(Command &cmd, PackedBoolArray list, bool channelCenter)
{
	cmd._id = _nextCmdId++;

	Assert(Leader());

#if LOG_COMM
	char buffer[256];
	CreateUnitsList(list, buffer);
	Log("Send command: units %s of %s: Command %d (context %d)",
		buffer, (const char *)GetDebugName(), cmd._message, cmd._context);
#endif

	AIUnit *leader = Leader();
	// if leader is not alive, he should not be able to send commands
	if (leader && leader->GetLifeState()!=AIUnit::LSAlive)
	{
		LogF("SendCommand: No command, leader %s is not alive",(const char *)leader->GetDebugName());
		return;
	}

	Assert(GetCenter());
	RadioChannel &radio = channelCenter ? GetCenter()->GetRadio() : GetRadio();

	int i;
	for (i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		if (!list.Get(i))
			continue;
		AIUnit *unit = UnitWithID(i + 1);
		if (!unit)
		{
			list.Set(i, false);
			continue;
		}

		if (cmd._message == Command::GetIn || cmd._message == Command::GetOut)
		{
			// check radio channel
			int index = INT_MAX;
			while (true)
			{
				RadioMessage *msg = radio.GetPrevMessage(index);
				if (!msg)
					break;
				if (msg->GetType() == RMTCommand)
				{
					Assert(dynamic_cast<RadioMessageCommand *>(msg));
					RadioMessageCommand *msgCmd = static_cast<RadioMessageCommand *>(msg);
					Assert(msgCmd->GetFrom() == this);
					if (msgCmd->GetMessage() == Command::GetIn && msgCmd->IsTo(unit))
					{
						msgCmd->DeleteTo(unit);
						goto SendCommandForContinue;
					}
					if (msgCmd->GetMessage() == Command::GetOut && msgCmd->IsTo(unit))
					{
						msgCmd->DeleteTo(unit);
						goto SendCommandForContinue;
					}
				}
			}
		}

SendCommandForContinue:
		continue;
	}

	if (list[leader->ID() - 1])
	{
		// try to process command directly
		if (cmd._message == Command::GetIn || cmd._message == Command::GetOut)
		{
			list.Set(leader->ID() - 1, false);
			PackedBoolArray leaderList;
			leaderList.Set(leader->ID() - 1, true);
			IssueCommand(cmd, leaderList);
		}
		else
		{
			IssueCommand(cmd, list);
			return;
		}
	}
	
	if (!list.IsEmpty())
		radio.Transmit
		(
			new RadioMessageCommand(this, list, cmd),
			GetCenter()->GetLanguage()
		);
}

void AIGroup::IssueCommand(Command &cmd, PackedBoolArray list)
{
	if (list.IsEmpty()) return;

	AIUnit *leader = Leader();
	// if leader is not alive, he should not be able to send commands
	if (leader && leader->GetLifeState()!=AIUnit::LSAlive)
	{
		// auto commands may be issued even without leader alive
		if (cmd._context != Command::CtxAuto && cmd._context != Command::CtxAutoSilent)
		{
			LogF("IssueCommand: No command, leader %s is not alive",(const char *)leader->GetDebugName());
			return;
		}
	}

	// TODO: direct Command::Join processing

	if (cmd._message == Command::GetOut)
	{
		// Get Out processed by units
		int i;
		for (i=0; i<MAX_UNITS_PER_GROUP; i++)
		{
			if (!list[i]) continue;
			AIUnit *unit = UnitWithID(i + 1);
			if (!unit) continue;
			
			if (unit->IsFreeSoldier())
				continue;

			AISubgroup *subgrp = unit->GetSubgroup();
			Assert(subgrp);
			if
			(
				subgrp != MainSubgroup() &&
				subgrp->NUnits() == 1
			)
			{
				cmd._joinToSubgroup = NULL;
				subgrp->ReceiveCommand(cmd);
			}
			else
			{
				if (cmd._context == Command::CtxAuto || cmd._context == Command::CtxAutoSilent)
					cmd._joinToSubgroup = subgrp;
				subgrp = new AISubgroup();
				AddSubgroup(subgrp);
				subgrp->AddUnit(unit);
				subgrp->SelectLeader(unit);
				if (GWorld->GetMode() == GModeNetware)
					GetNetworkManager().CreateObject(subgrp);
				subgrp->ReceiveCommand(cmd);
			}
		}
	}
	else if
	(
		cmd._message == Command::GetIn ||
		cmd._message == Command::Heal ||
		cmd._message == Command::Repair ||
		cmd._message == Command::Refuel ||
		cmd._message == Command::Rearm ||
		cmd._message == Command::Support ||
		cmd._message == Command::Stop ||
		cmd._message == Command::Expect ||
		cmd._message == Command::Join
	)
	{
		// Get In and supply commands processed by vehicles
		int i;
		for (i=0; i<MAX_UNITS_PER_GROUP; i++)
		{
			if (!list[i]) continue;
			AIUnit *unit = UnitWithID(i + 1);
			if (!unit) continue;
			if (cmd._message == Command::GetIn && unit->GetVehicle() == cmd._target)
			{
				// unit already in vehicle
				continue;
			}

			Transport *veh = unit->GetVehicleIn();
			AIUnit *commander = NULL;
			if (veh) commander = veh->CommanderUnit();

			if (unit->IsInCargo())
			{
				if (!commander || commander->GetGroup() != this || !list[commander->ID() - 1])
					unit->IssueGetOut();
			}
			else if (!unit->IsUnit())
			{
				if (commander && commander->GetGroup() == this)
				{
					if (list[commander->ID() - 1]) continue;
					else
					{
						list.Set(commander->ID() - 1, true);
						if (commander->ID() - 1 > i) continue;
						unit = commander;
					}
				}
			}

			AISubgroup *subgrp = unit->GetSubgroup();
			Assert(subgrp);
			if (subgrp != MainSubgroup())
			{
				bool allInside = true;
				if (veh)
				{
					for (int i=0; i<subgrp->NUnits(); i++)
					{
						AIUnit *u = subgrp->GetUnit(i);
						if (u && u->GetVehicleIn() != veh)
						{
							allInside = false;
							break;
						}
					}
				}
				else
				{
					allInside = subgrp->NUnits() == 1;
				}
				if (allInside)
				{
					subgrp->ReceiveCommand(cmd);
					continue;
				}
			}
			if (cmd._context == Command::CtxAuto || cmd._context == Command::CtxAutoSilent)
				cmd._joinToSubgroup = subgrp;
			subgrp = new AISubgroup();
			AddSubgroup(subgrp);
			if (veh)
			{
				if (veh->CommanderBrain() && veh->CommanderBrain()->GetGroup() == this)
				{
					subgrp->AddUnit(veh->CommanderBrain());
					unit = veh->CommanderBrain();
				}
				if (veh->DriverBrain() && veh->DriverBrain()->GetGroup() == this)
				{
					subgrp->AddUnit(veh->DriverBrain());
					unit = veh->DriverBrain();
				}
				if (veh->GunnerBrain() && veh->GunnerBrain()->GetGroup() == this)
					subgrp->AddUnit(veh->GunnerBrain());
				subgrp->SelectLeader(unit);
			}
			else
			{
				subgrp->AddUnit(unit);
				subgrp->SelectLeader(unit);
			}
			if (GWorld->GetMode() == GModeNetware)
				GetNetworkManager().CreateObject(subgrp);
			subgrp->ReceiveCommand(cmd);
		}
	}
	else 
	{
		// create subgroup
		AISubgroup *subgrp = NULL;
		{
			for (int i=0; i<NSubgroups(); i++)
			{
				AISubgroup *s = GetSubgroup(i);
				if (!s || s == MainSubgroup()) continue;
				if (list.Contain(s->GetUnitsListNoCargo()))
				{
					subgrp = s;
					break;
				}
			}
			if (!subgrp)
			{
				subgrp = new AISubgroup();
				AddSubgroup(subgrp);
				if (GWorld->GetMode() == GModeNetware)
					GetNetworkManager().CreateObject(subgrp);
			}
		}
		Assert(subgrp);
		Assert(subgrp->GetGroup());
		DoAssert(subgrp->GetGroup() == this);
		subgrp->AvoidRefresh(); // avoid refresh during subgrp creation
		// add units from other subgroups
		for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
		{
			if (!list[i]) continue;
			AIUnit *unit = UnitWithID(i + 1);
			if (!unit) continue;
			if (unit->GetSubgroup() == subgrp) continue;
			DoAssert(unit->GetGroup() == this);
			if (!unit->IsUnit())
			{
				AIUnit *u = unit->GetVehicleIn()->CommanderUnit();
				if (!u || u->GetGroup() != this || u->IsPlayer())
				{
					// unit in cargo with other group or no driver
					unit->IssueGetOut();
				}
				else if (list[u->ID() - 1])
					continue;
				else
					unit = u;
			}
			#if _ENABLE_REPORT
			DoAssert(unit->GetGroup() == this);
			AISubgroup *oldSubgrp = unit->GetSubgroup();
			DoAssert(oldSubgrp != subgrp);
			#endif
			if (!unit->IsUnit())
			{
				subgrp->AddUnit(unit);
				subgrp->SelectLeader();
				DoAssert(subgrp->GetGroup() == this);
			}
			else
			{
				DoAssert(subgrp->GetGroup() == this);
				subgrp->AddUnitWithCargo(unit);
				DoAssert(subgrp->GetGroup() == this);
			}
/*	// TODO: BUG
			if (oldSubgrp && oldSubgrp->NUnits() <= 0 && oldSubgrp != MainSubgroup())
			{
				oldSubgrp->RemoveFromGroup();
			}
*/
		}
		subgrp->AvoidRefresh(false);

		// send command to subgroup	
		subgrp->ReceiveCommand(cmd);
	}
}

void AIGroup::SendFormation(Formation f, AISubgroup *to)
{
	Assert(to);
	if (!to)
		return;

	Assert(Leader());
	if (!Leader()) return;

	// check radio channel
	int index = INT_MAX;
	while (true)
	{
		RadioMessage *msg = GetRadio().FindPrevMessage(RMTFormation, index);
		if (!msg)
			break;
		Assert( dynamic_cast<RadioMessageFormation *>(msg) );
		RadioMessageFormation *msgForm = static_cast<RadioMessageFormation *>(msg);
		Assert(msgForm);
		Assert(msgForm->GetFrom() == this);
		if (msgForm->GetTo() == to)
		{
			if (msgForm->GetFormation() == f)
			{
				return;
			}
			else
			{
				msgForm->SetFormation(f);
				return;
			}
		}
	}

	{
		RadioMessage *msg = GetRadio().GetActualMessage();
		if (msg && msg->GetType() == RMTFormation)
		{
			Assert( dynamic_cast<RadioMessageFormation *>(msg) );
			RadioMessageFormation *msgForm = static_cast<RadioMessageFormation *>(msg);
			Assert(msgForm);
			Assert(msgForm->GetFrom() == this);
			if (msgForm->GetTo() == to)
			{
				if (msgForm->GetFormation() == f)
					return;
				else
					goto TransmitSendFormation;
			}
		}
	}

	if (to->GetFormation() == f)
		return;

	if (NUnits() == 1)
	{
		to->SetFormation(f);
		return;
	}

TransmitSendFormation:
	GetRadio().Transmit
	(
		new RadioMessageFormation(this, to, f),
		GetCenter()->GetLanguage()
	);
}

void AIGroup::SendSemaphore(Semaphore sem, PackedBoolArray list)
{
	switch (sem)
	{
	case AI::SemaphoreBlue:
		SendLooseFormation(false, list);
		SendOpenFire(OFSNeverFire, list);
		break;
	case AI::SemaphoreGreen:
		SendLooseFormation(false, list);
		SendOpenFire(OFSHoldFire, list);
		break;
	case AI::SemaphoreWhite:
		SendLooseFormation(true, list);
		SendOpenFire(OFSHoldFire, list);
		break;
	case AI::SemaphoreYellow:
		SendLooseFormation(false, list);
		SendOpenFire(OFSOpenFire, list);
		break;
	case AI::SemaphoreRed:
		SendLooseFormation(true, list);
		SendOpenFire(OFSOpenFire, list);
		break;
	}
}

void AIGroup::SendBehaviour(CombatMode mode, PackedBoolArray list)
{
	AIUnit *leader = Leader();
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		if (!list.Get(i))
			continue;
		AIUnit *unit = UnitWithID(i + 1);
		if (!unit)
		{
			list.Set(i, false);
			continue;
		}

		if (!leader || unit == leader)
		{
			unit->SetCombatModeMajor(mode);
			list.Set(i, false);
			continue;
		}

		// check radio channel
		int index = INT_MAX;
		while (true)
		{
			RadioMessage *msg = GetRadio().FindPrevMessage(RMTBehaviour, index);
			if (!msg)
				break;
			Assert( dynamic_cast<RadioMessageBehaviour *>(msg) );
			RadioMessageBehaviour *msgSem = static_cast<RadioMessageBehaviour *>(msg);
			Assert(msgSem);
			Assert(msgSem->GetFrom() == this);
			if (msgSem->IsTo(unit))
			{
				if (msgSem->GetBehaviour() == mode)
				{
					list.Set(i, false);
					goto SendBehaviourForContinue;
				}
				else
				{
					msgSem->DeleteTo(unit);
					break;
				}
			}
			else if (msgSem->GetBehaviour() == mode)
			{
				msgSem->AddTo(unit);
				list.Set(i, false);
				goto SendBehaviourForContinue;
			}
		}

		{
			RadioMessage *msg = GetRadio().GetActualMessage();
			if (msg && msg->GetType() == RMTBehaviour)
			{
				Assert( dynamic_cast<RadioMessageBehaviour *>(msg) );
				RadioMessageBehaviour *msgSem = static_cast<RadioMessageBehaviour *>(msg);
				Assert(msgSem);
				Assert(msgSem->GetFrom() == this);
				if (msgSem->IsTo(unit))
				{
					if (msgSem->GetBehaviour() == mode)
					{
						list.Set(i, false);
						continue;
					}
					else
						goto SendBehaviourForContinue;
				}
			}
		}

		if (unit->GetCombatModeMajor() == mode)
		{
			list.Set(i, false);
			continue;
		}

SendBehaviourForContinue:
		continue;
	}

	if (leader && !list.IsEmpty())
	{
		GetRadio().Transmit
		(
			new RadioMessageBehaviour(this, list, mode),
			GetCenter()->GetLanguage()
		);
	}
}

void AIGroup::SendLooseFormation(bool loose, PackedBoolArray list)
{
	AIUnit *leader = Leader();
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		if (!list.Get(i))
			continue;
		AIUnit *unit = UnitWithID(i + 1);
		if (!unit)
		{
			list.Set(i, false);
			continue;
		}

		if (!leader || unit == leader)
		{
			AI::Semaphore s = unit->GetSemaphore();
			s = ApplyLooseFormation(s, loose);
			unit->SetSemaphore(s);
			list.Set(i, false);
			continue;
		}

		// check radio channel
		int index = INT_MAX;
		while (true)
		{
			RadioMessage *msg = GetRadio().FindPrevMessage(RMTLooseFormation, index);
			if (!msg)
				break;
			Assert( dynamic_cast<RadioMessageLooseFormation *>(msg) );
			RadioMessageLooseFormation *msgSem = static_cast<RadioMessageLooseFormation *>(msg);
			Assert(msgSem);
			Assert(msgSem->GetFrom() == this);
			if (msgSem->IsTo(unit))
			{
				if (msgSem->IsLooseFormation() == loose)
				{
					list.Set(i, false);
					goto SendLooseFormationForContinue;
				}
				else
				{
					msgSem->DeleteTo(unit);
					break;
				}
			}
			else if (msgSem->IsLooseFormation() == loose)
			{
				msgSem->AddTo(unit);
				list.Set(i, false);
				goto SendLooseFormationForContinue;
			}
		}

		{
			RadioMessage *msg = GetRadio().GetActualMessage();
			if (msg && msg->GetType() == RMTLooseFormation)
			{
				Assert( dynamic_cast<RadioMessageLooseFormation *>(msg) );
				RadioMessageLooseFormation *msgSem = static_cast<RadioMessageLooseFormation *>(msg);
				Assert(msgSem);
				Assert(msgSem->GetFrom() == this);
				if (msgSem->IsTo(unit))
				{
					if (msgSem->IsLooseFormation() == loose)
					{
						list.Set(i, false);
						continue;
					}
					else
						goto SendLooseFormationForContinue;
				}
			}
		}

		{
			AI::Semaphore s = unit->GetSemaphore();
			if (ApplyLooseFormation(s, loose) == s)
			{
				list.Set(i, false);
				continue;
			}
		}

SendLooseFormationForContinue:
		continue;
	}

	if (leader && !list.IsEmpty())
	{
		GetRadio().Transmit
		(
			new RadioMessageLooseFormation(this, list, loose),
			GetCenter()->GetLanguage()
		);
	}
}

void AIGroup::SendOpenFire(OpenFireState open, PackedBoolArray list)
{
	AIUnit *leader = Leader();
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		if (!list.Get(i))
			continue;
		AIUnit *unit = UnitWithID(i + 1);
		if (!unit)
		{
			list.Set(i, false);
			continue;
		}

		if (!leader || unit == leader)
		{
			AI::Semaphore s = unit->GetSemaphore();
			s = ApplyOpenFire(s, open);
			unit->SetSemaphore(s);
			list.Set(i, false);
			continue;
		}

		// check radio channel
		int index = INT_MAX;
		while (true)
		{
			RadioMessage *msg = GetRadio().FindPrevMessage(RMTOpenFire, index);
			if (!msg)
				break;
			Assert( dynamic_cast<RadioMessageOpenFire *>(msg) );
			RadioMessageOpenFire *msgSem = static_cast<RadioMessageOpenFire *>(msg);
			Assert(msgSem);
			Assert(msgSem->GetFrom() == this);
			if (msgSem->IsTo(unit))
			{
				if (msgSem->GetOpenFireState() == open)
				{
					list.Set(i, false);
					goto SendOpenFireForContinue;
				}
				else
				{
					msgSem->DeleteTo(unit);
					break;
				}
			}
			else if (msgSem->GetOpenFireState() == open)
			{
				msgSem->AddTo(unit);
				list.Set(i, false);
				goto SendOpenFireForContinue;
			}
		}

		{
			RadioMessage *msg = GetRadio().GetActualMessage();
			if (msg && msg->GetType() == RMTOpenFire)
			{
				Assert( dynamic_cast<RadioMessageOpenFire *>(msg) );
				RadioMessageOpenFire *msgSem = static_cast<RadioMessageOpenFire *>(msg);
				Assert(msgSem);
				Assert(msgSem->GetFrom() == this);
				if (msgSem->IsTo(unit))
				{
					if (msgSem->GetOpenFireState() == open)
					{
						list.Set(i, false);
						continue;
					}
					else
						goto SendOpenFireForContinue;
				}
			}
		}

		{
			AI::Semaphore s = unit->GetSemaphore();
			if (ApplyOpenFire(s, open) == s)
			{
				list.Set(i, false);
				continue;
			}
		}

SendOpenFireForContinue:
		continue;
	}

	if (leader && !list.IsEmpty())
	{
		GetRadio().Transmit
		(
			new RadioMessageOpenFire(this, list, open),
			GetCenter()->GetLanguage()
		);
	}
}

Target *AIGroup::EngageSent(AIUnit *unit) const
{
	Target *tgt = unit->GetEngageTarget();
	Target *defTgt = unit->GetTargetAssigned();

	int index = INT_MAX;
	while (true)
	{
		RadioMessage *msg = GetRadio().FindPrevMessage(RMTTarget, index);
		if (!msg)
			break;
		Assert( dynamic_cast<RadioMessageTarget *>(msg) );
		RadioMessageTarget *msgTgt = static_cast<RadioMessageTarget *>(msg);
		if (!msgTgt->IsTo(unit)) continue;
		// we have some message to this unit
		if (msgTgt->GetTarget()) defTgt = msgTgt->GetTarget();
		if (msgTgt->GetEngage()) tgt = defTgt;
	}

	// check actual message
	RadioMessage *msg = GetRadio().GetActualMessage();
	if (msg && msg->GetType()==RMTTarget)
	{
		Assert( dynamic_cast<RadioMessageTarget *>(msg) );
		RadioMessageTarget *msgTgt = static_cast<RadioMessageTarget *>(msg);
		if (msgTgt->IsTo(unit))
		{
			if (msgTgt->GetTarget()) defTgt = msgTgt->GetTarget();
			if (msgTgt->GetEngage()) tgt = defTgt;
		}
	}

	return tgt;
}

Target *AIGroup::FireSent(AIUnit *unit) const
{
	Target *tgt = unit->GetEnableFireTarget();
	Target *defTgt = unit->GetTargetAssigned();

	int index = INT_MAX;
	while (true)
	{
		RadioMessage *msg = GetRadio().FindPrevMessage(RMTTarget, index);
		if (!msg)
			break;
		Assert( dynamic_cast<RadioMessageTarget *>(msg) );
		RadioMessageTarget *msgTgt = static_cast<RadioMessageTarget *>(msg);
		if (!msgTgt->IsTo(unit)) continue;
		// we have some message to this unit
		if (msgTgt->GetTarget()) defTgt = msgTgt->GetTarget();
		if (msgTgt->GetFire()) tgt = defTgt;
	}

	// check actual message
	RadioMessage *msg = GetRadio().GetActualMessage();
	if (msg && msg->GetType()==RMTTarget)
	{
		Assert( dynamic_cast<RadioMessageTarget *>(msg) );
		RadioMessageTarget *msgTgt = static_cast<RadioMessageTarget *>(msg);
		if (msgTgt->IsTo(unit))
		{
			if (msgTgt->GetTarget()) defTgt = msgTgt->GetTarget();
			if (msgTgt->GetFire()) tgt = defTgt;
		}
	}

	return tgt;
}

Target *AIGroup::TargetSent(AIUnit *unit) const
{
	Target *tgt = unit->GetTargetAssigned();

	int index = INT_MAX;
	while (true)
	{
		RadioMessage *msg = GetRadio().FindPrevMessage(RMTTarget, index);
		if (!msg)
			break;
		Assert( dynamic_cast<RadioMessageTarget *>(msg) );
		RadioMessageTarget *msgTgt = static_cast<RadioMessageTarget *>(msg);
		if (!msgTgt->IsTo(unit)) continue;
		// we have some message to this unit
		if (msgTgt->GetTarget()) tgt = msgTgt->GetTarget();	
	}

	// check actual message
	RadioMessage *msg = GetRadio().GetActualMessage();
	if (msg &&msg->GetType()==RMTTarget)
	{
		Assert( dynamic_cast<RadioMessageTarget *>(msg) );
		RadioMessageTarget *msgTgt = static_cast<RadioMessageTarget *>(msg);
		if (msgTgt->IsTo(unit))
		{
			if (msgTgt->GetTarget()) tgt = msgTgt->GetTarget();
		}
	}

	return tgt;

}

void AIGroup::SendTarget
(
	Target *target, bool engage, bool fire,
	PackedBoolArray list, bool silent
)
{ 
	AIUnit *leader = Leader();
	if (!leader) return;

	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		if (!list.Get(i))
			continue;
		AIUnit *unit = UnitWithID(i + 1);
		if (!unit)
		{
			list.Set(i, false);
			continue;
		}

		if (target)
		{
			EntityAI *tgtAI = target->idExact;
			if (tgtAI == unit->GetVehicleIn() || tgtAI==unit->GetPerson())
			{
				// unit cannot target itself
				list.Set(i, false);
				continue;
			}
		}

		// check if unit is already doing what we tell it
		if
		(
			TargetSent(unit)==target &&
			( !fire || target==FireSent(unit) ) &&
			( !engage || target==EngageSent(unit) )
		)
		{
			list.Set(i, false);
			continue;
		}
		if (unit == leader || silent)
		{
			// leader - direct processing
			if (!target) target = unit->GetTargetAssigned();
			else unit->AssignTarget(target);
			if (target)
			{
				if (fire) unit->EnableFireTarget(target);
				if (engage) unit->EngageTarget(target);
			}

			list.Set(i, false);
			continue;
		}

		// check radio channel
		int index = INT_MAX;
		while (true)
		{
			RadioMessage *msg = GetRadio().FindPrevMessage(RMTTarget, index);
			if (!msg)
				break;
			Assert( dynamic_cast<RadioMessageTarget *>(msg) );
			RadioMessageTarget *msgTgt = static_cast<RadioMessageTarget *>(msg);
			Assert(msgTgt);
			Assert(msgTgt->GetFrom() == this);
			if (msgTgt->IsTo(unit))
			{
				// we have found some target command to same unit
				// if parameters wanted are subset of actual parameters
				// we may consider target transmitted
				if (target && msgTgt->GetTarget()!=target)
				{
					// we may remove the old message - it is being superseded
					msgTgt->DeleteTo(unit);
					break;
				}
				// we might want to change target parameters
				// we transmit either no target command (adding par to last target)
				// or same target command
				// this means: !target || msgTgt->GetTarget()==target
				// see if above -
				if (msgTgt->IsOnlyTo(unit))
				{
					// message only to given unit - we may add new parameters
					if (fire) msgTgt->SetFire(true);
					if (engage) msgTgt->SetEngage(true);
					list.Set(i, false);
					goto SendTargetForContinue;
				}
				// check if some target (even to multiple units)
				// that whould be superset of current target is on the way
				if
				(
					msgTgt->GetEngage()>=engage &&
					msgTgt->GetFire()>=fire
				)
				{
					list.Set(i, false);
					goto SendTargetForContinue;
				}
			}
			else if
			(
				msgTgt->GetTarget()==target &&
				msgTgt->GetEngage()==engage &&
				msgTgt->GetFire()==fire
			)
			{
				// some message with same arguments to other unit is on the way
				// we add unit to address list
				msgTgt->AddTo(unit);
				list.Set(i, false);
				goto SendTargetForContinue;
			}
		}

SendTargetForContinue:
		continue;
		// TODO: check if target is not pending
		/*
		bool pending = false;
		if (tgt)
		{
			if (engage) 
			if (TargetSent()==tgt) pending = true;
		if (!engage
		*/
	}

	if (!list.IsEmpty())
	{
		GetRadio().Transmit
		(
			new RadioMessageTarget(this, list, target, engage, fire),
			GetCenter()->GetLanguage()
		);
	}
}

void AIGroup::SendState(RadioMessageState *msg, bool silent)
{
	// usually called with new
	// if message is not queued we have the only reference to the message
	Ref<RadioMessageState> ref = msg;
	AIUnit *leader = Leader();
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = UnitWithID(i + 1);
		if (!unit) continue;
		if (!msg->IsTo(unit)) continue;

		if (!leader || unit == leader || silent)
		{
			// leader command is executed directly
			Ref<RadioMessageState> temp = msg->Clone();
			temp->ClearTo();
			temp->AddTo(unit);
			temp->Transmitted();
			msg->DeleteTo(unit);
			continue;
		}

		// TODO: check radio channel & optimize
	}
	if (leader && msg->IsToSomeone())
	{
		GetRadio().Transmit
		(
			msg,GetCenter()->GetLanguage()
		);
	}
}

void AIGroup::SendReportStatus(PackedBoolArray list)
{
	Assert(Leader());
	if (!Leader()) return;

	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		if (!list.Get(i))
			continue;
		AIUnit *unit = UnitWithID(i + 1);
		if (!unit)
		{
			list.Set(i, false);
			continue;
		}

		if (unit == Leader())
		{
			list.Set(i, false);
			continue;
		}

		int index = INT_MAX;
		while (true)
		{
			RadioMessage *msg = GetRadio().FindPrevMessage(RMTReportStatus, index);
			if (!msg)
				break;
			Assert( dynamic_cast<RadioMessageReportStatus *>(msg) );
			RadioMessageReportStatus *msgRep = static_cast<RadioMessageReportStatus *>(msg);
			Assert(msgRep);
			Assert(msgRep->GetFrom() == this);
			if (msgRep->IsTo(unit))
			{
				list.Set(i, false);
				goto SendReportStatusForContinue;
			}
		}

		{
			RadioMessage *msg = GetRadio().GetActualMessage();
			if (msg && msg->GetType() == RMTReportStatus)
			{
				Assert( dynamic_cast<RadioMessageReportStatus *>(msg) );
				RadioMessageReportStatus *msgRep = static_cast<RadioMessageReportStatus *>(msg);
				Assert(msgRep);
				Assert(msgRep->GetFrom() == this);
				if (msgRep->IsTo(unit))
				{
					list.Set(i, false);
					continue;
				}
			}
		}

SendReportStatusForContinue:
		continue;
	}

	if (!list.IsEmpty())
		GetRadio().Transmit
		(
			new RadioMessageReportStatus(this, list),
			GetCenter()->GetLanguage()
		);
}

void AIGroup::SendObjectDestroyed(AIUnit *sender, const VehicleType *type)
{
	if (!sender) return;

	GetRadio().Transmit
	(
		new RadioMessageObjectDestroyed(sender, this, type),
		GetCenter()->GetLanguage()
	);
}

void AIGroup::SendContact(AIUnit *sender)
{
	if (!sender) return;

	GetRadio().Transmit
	(
		new RadioMessageContact(sender, this),
		GetCenter()->GetLanguage()
	);
}

void AIGroup::SendUnitDown(AIUnit *sender, AIUnit *down)
{
	// check if report already exists
	// check actual message
	RadioMessage *msg = GetRadio().GetActualMessage();
	if 
	(
		msg && msg->GetSender() &&
		msg->GetSender()->GetLifeState()==AIUnit::LSAlive &&
		msg->GetType()==RMTUnitKilled
	)
	{
		
		RadioMessageUnitKilled *msgT = static_cast<RadioMessageUnitKilled *>(msg);
		if (msgT && msgT->GetWhoKilled()==down) return;
	}
	// check message queue
	int index = INT_MAX;
	while (true)
	{
		RadioMessage *msg = GetRadio().FindPrevMessage(RMTUnitKilled, index);
		if (!msg)
			break;

		if (!msg->GetSender()) continue;
		if (msg->GetSender()->GetLifeState()!=AIUnit::LSAlive) continue;
		RadioMessageUnitKilled *msgT = static_cast<RadioMessageUnitKilled *>(msg);
		if (msgT->GetWhoKilled()==down) return;
	}

	if (!GetReportedDown(down))
	{
		GetRadio().Transmit
		(
			new RadioMessageUnitKilled(sender,down),
			GetCenter()->GetLanguage()
		);
	}
	else
	{
		// it is already suppossed to be dead
		// react immediatelly
		if (down->GetLifeState()==AIUnit::LSDead)
		{
			AISubgroup *subgrp = down->GetSubgroup();
			if (subgrp) subgrp->ReceiveAnswer(down,AI::UnitDestroyed);
		}	
	}
}

void AIGroup::SendUnderFire(AIUnit *sender)
{
	if (!sender) return;

	GetRadio().Transmit
	(
		new RadioMessageUnderFire(sender, this),
		GetCenter()->GetLanguage()
	);
}

void AIGroup::ReportFire(AIUnit *who, bool status)
{
	// check radio for messages of the same type
	int index = INT_MAX;
	while (true)
	{
		RadioMessage *msg = GetRadio().FindPrevMessage(RMTFireStatus, index);
		if (!msg)
			break;
		GetRadio().Cancel(msg);
	}

	GetRadio().Transmit
	(
		new RadioMessageFireStatus(who,status),
		GetCenter()->GetLanguage()
	);
}

void AIGroup::SendClear(AIUnit *sender)
{
	if (!sender) return;

	GetRadio().Transmit
	(
		new RadioMessageClear(sender, this),
		GetCenter()->GetLanguage()
	);
}

void AIGroup::SendGetOut(PackedBoolArray list)
{
	Command cmd;
	cmd._message = Command::GetOut;
	cmd._context = Command::CtxAuto;
	SendCommand(cmd, list);
}

void AIGroup::SendAutoCommandToUnit(Command &cmd, AIUnit *unit, bool join, bool channelCenter)
{
	Assert(unit);
	if (!unit || !unit->IsUnit())
		return;
	AISubgroup *subgrp = unit->GetSubgroup();
	Assert(subgrp);
	if (!subgrp)
		return;

	PackedBoolArray list;
	list.Set(unit->ID() - 1, true);

	if (join)
	{
/*
		if (cmd._message == Command::NoCommand)
		{
			cmd._message = Command::Join;
			cmd._joinToSubgroup = subgrp;
			cmd._context = Command::CtxAutoJoin;
		}
		else
		{
			Command *cmdCurrent = subgrp->GetCommand();
			if (cmdCurrent && cmdCurrent->_message == Command::Join)
			subgrp = cmdCurrent->_joinToSubgroup;

			cmd._joinToSubgroup = subgrp;
			cmd._context = Command::CtxAuto;
		}
*/
		// TODO: check stack and radio for some other command with _joinToSubgroup
		cmd._joinToSubgroup = subgrp;
		cmd._context = Command::CtxAuto;
	}
	else
		cmd._context = Command::CtxAuto;

	Assert(cmd._message != Command::NoCommand);
	SendCommand(cmd, list, channelCenter);
}

void AIGroup::IssueAutoCommand(Command &cmd, AIUnit *unit)
{
	Assert(unit);
	if (!unit || !unit->IsUnit())
		return;
	AISubgroup *subgrp = unit->GetSubgroup();
	Assert(subgrp);
	if (!subgrp)
		return;

	PackedBoolArray list;
	list.Set(unit->ID() - 1, true);

	cmd._joinToSubgroup = subgrp;
	cmd._context = Command::CtxAuto;
	cmd._id = _nextCmdId++;

	Assert(cmd._message != Command::NoCommand);

	IssueCommand(cmd, list);
}
void AIGroup::NotifyAutoCommand(Command &cmd, AIUnit *unit)
{
	// unit is not able to issue any commands when it is not alive
	if (unit->GetLifeState()!=AIUnit::LSAlive) return;
	IssueAutoCommand(cmd,unit);
	GetRadio().Transmit
	(
		new RadioMessageNotifyCommand(unit, this, cmd),
		GetCenter()->GetLanguage()
	);
}

void AIGroup::ReceiveUnitStatus(AIUnit *unit, Answer answer)
{
	Assert (unit->GetGroup()==this);
	if (unit->GetGroup()!=this) return;
	int id = unit->ID();
	switch (answer)
	{
		case HealthCritical:
			_healthState[id-1] = AIUnit::RSCritical;
			break;
		case DammageCritical:
			_dammageState[id-1] = AIUnit::RSCritical;
			break;
		case FuelCritical:
			_fuelState[id-1] = AIUnit::RSCritical;
			break;
		case FuelLow:
			_fuelState[id-1] = AIUnit::RSLow;
			break;
		case AmmoCritical:
			_ammoState[id-1] = AIUnit::RSCritical;
			break;
		case AmmoLow:			
			_ammoState[id-1] = AIUnit::RSLow;
			break;
	}
}

void AIGroup::ReceiveAnswer(AISubgroup* from, Answer answer)
{
	if (!from)
		return;

#if LOG_COMM
	Log("Receive answer: Group %s: From subgroup %s: Answer %d)",
		(const char *)GetDebugName(), (const char *)from->GetDebugName(), answer);
#endif
	
	switch (answer)
	{
		case AI::CommandCompleted:
		{
		}
		break;
		case AI::CommandFailed:
		case AI::SubgroupDestinationUnreacheable:
		{
		}
		break;
	}
}

// Communication with center
void AIGroup::SendAnswer(Answer answer)
{
#if LOG_COMM
	Log("Send answer: Group %s: Answer %d",
	(const char *)GetDebugName(), answer);
#endif

	if (_center)
	{
		_center->ReceiveAnswer(this, answer);

		/*
		switch (answer)
		{
			case AI::MissionCompleted:
			case AI::MissionFailed:
			case AI::DestinationUnreacheable:
			case AI::GroupDestroyed:
				if (GLOB_WORLD->FocusOn() && GLOB_WORLD->FocusOn()->GetGroup() == this)
					GLOB_WORLD->CancelTimeJump();
				break;
		}
		*/
	}
}

void AIGroup::SendRadioReport(ReportSubject subject, Target &target)
{
	if (NUnits() <= 1) return;

	// select any unit as reporting
	AIUnit *from = target.idSensor ? target.idSensor->CommanderUnit() : NULL;
	// find first unit that is alive
	if (!from)
	{
		for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
		{
			AIUnit *unit = _units[i];
			if (!unit || unit->GetLifeState()!=AIUnit::LSAlive)
			{
				from = unit;
			}
		}
	}

	if (!from || from->GetLifeState()!=AIUnit::LSAlive) return;

	Assert(subject == ReportNew);
	if (ReportSent(subject, target.type)) return;

	GetRadio().Transmit
	(
		new RadioMessageReportTarget(from, this, subject, target),
		GetCenter()->GetLanguage()
	);
}

/*!
\patch_internal 1.45 Date 2/18/2002 by Ondra
- Fixed: send reports about seen units only when leader
is alive and knows about the target.
*/

void AIGroup::SendReport(ReportSubject subject, Target &target)
{
	if (_center)
	{
		// send report about units only when leader is alive
		AIUnit *leader = Leader();
		if
		(
			leader && leader->GetLifeState()==AIUnit::LSAlive &&
			target.IsKnownBy(leader)
		)
		{
			_center->ReceiveReport(this, subject, target);
		}
	}
}

TypeIsSimple(AIUnit *);

void AIGroup::UnassignVehicle(Transport *veh)
{
	if (!veh) return;
	// remove from list
	for (int i=0; i<_vehicles.Size(); i++)
	{
		if (_vehicles[i] == veh)
		{
			_vehicles.Delete(i);
			break;
		}
	}
	veh->AssignGroup(NULL);
	
	AIUnit *unit;
	unit = veh->GetDriverAssigned();
	if (unit)
	{
		// faster than unit->UnassignVehicle
		unit->_vehicleAssigned = NULL;
		veh->AssignDriver(NULL);
	}
	unit = veh->GetCommanderAssigned();
	if (unit)
	{
		// faster than unit->UnassignVehicle
		unit->_vehicleAssigned = NULL;
		veh->AssignCommander(NULL);
	}
	unit = veh->GetGunnerAssigned();
	if (unit)
	{
		// faster than unit->UnassignVehicle
		unit->_vehicleAssigned = NULL;
		veh->AssignGunner(NULL);
	}
	for (int i=0; i<veh->NCargoAssigned(); i++)
	{
		// faster than unit->UnassignVehicle
		unit = veh->GetCargoAssigned(i);
		if (unit) unit->_vehicleAssigned = NULL;
	}
	veh->EmptyCargo();
}

// Implementation
/*!
\patch 1.17 Date 08/14/2001 by Ondra
- Fixed: Discrepancy between conditions for getting in and automatical eject.
\patch_internal 1.17 Date 08/14/2001 by Ondra
- New: New function IsPossibleToGetIn, has most functionality from IsAbleToMove,
but has no test for driver.
*/

void AIGroup::AssignVehicles()
{
	// if there is nothing to assign, do nothing
	if (_vehicles.Size() <=0) return;
	// TODO: check assignement structure for consistence

	AUTO_STATIC_ARRAY(AIUnit *,soldiers,MAX_UNITS_PER_GROUP)
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = _units[i];
		if (!unit) continue;
		//if (!unit->IsFreeSoldier()) continue;
		Transport *veh = unit->VehicleAssigned();
		if (veh)
		{
			if (veh->IsAbleToMove() && veh->GetDriverAssigned()) continue;
			// we need to remove this vehicle
		}

		float exp = unit->GetPerson()->GetExperience();
		for (int j=0; j<soldiers.Size(); j++)
		{
			AIUnit *with = soldiers[j];
			if (exp >= with->GetPerson()->GetExperience())
			{
				soldiers.Insert(j, unit);
				goto NextUnit;
			}
		}
		soldiers.Add(unit);
NextUnit:
		continue;
	}

	if (soldiers.Size() <= 0) return;

	for (int i=0; i<_vehicles.Size(); i++)
	{
		Transport *veh = _vehicles[i];
		if (!veh  || !veh->IsPossibleToGetIn()) continue;

		AIUnit *commander = NULL;
		AIUnit *driver = NULL;
		AIUnit *gunner = NULL;
		if (veh->GetType()->HasCommander() && !veh->GetCommanderAssigned())
		{
			for (int j=0; j<soldiers.Size(); j++)
			{
				AIUnit *unit = soldiers[j];
				if (veh->QCanIGetInCommander(unit->GetPerson()))
				{
					commander = unit;
					soldiers.Delete(j);
					break;
				}
			}
			//LogF("Commander %s",commander ? (const char *)commander->GetDebugName() : "no");
		}
		if (veh->GetType()->HasGunner() && !veh->GetGunnerAssigned())
		{
			for (int j=0; j<soldiers.Size(); j++)
			{
				AIUnit *unit = soldiers[j];
				if (veh->QCanIGetInGunner(unit->GetPerson()))
				{
					gunner = unit;
					soldiers.Delete(j);
					break;
				}
			}
			//LogF("Gunner %s",gunner ? (const char *)gunner->GetDebugName() : "no");
			if (!gunner)
			{
				// no suitable gunner found - use commander if possible
				gunner = commander, commander = NULL;
				//LogF("   2nd %s",gunner ? (const char *)gunner->GetDebugName() : "no");
			}
		}

		if (veh->GetType()->HasDriver() && !veh->GetDriverAssigned())
		{
			for (int j=0; j<soldiers.Size(); j++)
			{
				AIUnit *unit = soldiers[j];
				if (veh->QCanIGetIn(unit->GetPerson()))
				{
					driver = unit;
					soldiers.Delete(j);
					break;
				}
			}

			//LogF("Driver %s",driver ? (const char *)driver->GetDebugName() : "no");
			if (!driver)
			{
				// no suitable driver found - use gunner or commander if possible
				driver = commander, commander = NULL;
				if (!driver) driver = gunner, gunner = NULL;
				//LogF("   2nd %s",driver ? (const char *)driver->GetDebugName() : "no");
			}
		}

		// assign most needed positions first

		if (driver) driver->AssignAsDriver(veh);
		if (gunner) gunner->AssignAsGunner(veh);
		if (commander) commander->AssignAsCommander(veh);

		if (soldiers.Size() <= 0) return;
	}

	// remove vehicles that can be no longer used
	/*
	for (int i=0; i<_vehicles.Size(); i++)
	{
		Transport *veh = _vehicles[i];
		if (!veh  || !veh->IsAbleToMove()) continue;
	}
	*/

	for (int i=0; i<_vehicles.Size(); i++)
	{
		Transport *veh = _vehicles[i];
		if (!veh  || !veh->IsAbleToMove()) continue;

		int nFree = veh->GetMaxManCargo() - veh->NCargoAssigned();
		if (nFree > 0)
		{
			for (int j=0; j<soldiers.Size();)
			{
				AIUnit *unit = soldiers[j];
				if (veh->QCanIGetInCargo(unit->GetPerson()))
				{
					soldiers.Delete(j);
					unit->AssignAsCargo(veh);
					if (--nFree <= 0) break;
				}
				else
				{
					j++;
				}
			}

			if (soldiers.Size() <= 0) return;
		}
	}
}

bool AIGroup::CommandSent(bool channelCenter)
{
	// channelCenter == true ... check radio of center
	// channelCenter == false ... check radio of group and FSM stack

	if (!channelCenter)
	{
		// check stack
		if (MainSubgroup()->_stack.Size() > 0)
			return true;
	}

	Assert(GetCenter());
	RadioChannel &radio = channelCenter ? GetCenter()->GetRadio() : GetRadio();

	// check radio channel
	int index = INT_MAX;
	while (true)
	{
		RadioMessage *msg = radio.FindPrevMessage(RMTCommand, index);
		if (!msg)
			break;
		Assert( dynamic_cast<RadioMessageCommand *>(msg) );
		RadioMessageCommand *msgCmd = static_cast<RadioMessageCommand *>(msg);
		Assert(msgCmd);
		Assert(msgCmd->GetFrom() == this);
		if (msgCmd->IsToMainSubgroup())
		{
			return true;
		}
	}

	// check actual message
	RadioMessage *msg = radio.GetActualMessage();
	if (msg && msg->GetType() == RMTCommand)
	{
		Assert( dynamic_cast<RadioMessageCommand *>(msg) );
		RadioMessageCommand *msgCmd = static_cast<RadioMessageCommand *>(msg);
		Assert(msgCmd);
		Assert(msgCmd->GetFrom() == this);
		if (msgCmd->IsToMainSubgroup())
		{
			return true;
		}
	}

	return false;
}

bool AIGroup::CommandSent(Command::Message message, bool channelCenter)
{
	// channelCenter == true ... check radio of center
	// channelCenter == false ... check radio of group and FSM stack

	if (!channelCenter)
	{
		int j, m = NSubgroups();
		for (j=0; j<m; j++)
		{
			AISubgroup *subgrp = GetSubgroup(j);
			if (!subgrp) continue;

			int i, n = subgrp->_stack.Size();
			for (i=0; i<n; i++)
			{
				const Command *cmd = subgrp->_stack[i]._task;
				if (cmd->_message == message)
					return true;
			}
		}
	}

	Assert(GetCenter());
	RadioChannel &radio = channelCenter ? GetCenter()->GetRadio() : GetRadio();

	// check radio channel
	int index = INT_MAX;
	while (true)
	{
		RadioMessage *msg = radio.FindPrevMessage(RMTCommand, index);
		if (!msg)
			break;
		Assert( dynamic_cast<RadioMessageCommand *>(msg) );
		RadioMessageCommand *msgCmd = static_cast<RadioMessageCommand *>(msg);
		Assert(msgCmd);
		Assert(msgCmd->GetFrom() == this);
		if (msgCmd->GetMessage() == message)
		{
			return true;
		}
	}

	RadioMessage *msg = radio.GetActualMessage();
	if (msg && msg->GetType() == RMTCommand)
	{
		Assert( dynamic_cast<RadioMessageCommand *>(msg) );
		RadioMessageCommand *msgCmd = static_cast<RadioMessageCommand *>(msg);
		Assert(msgCmd);
		Assert(msgCmd->GetFrom() == this);
		if (msgCmd->GetMessage() == message)
		{
			return true;
		}
	}

	return false;
}

bool AIGroup::CommandSent(AIUnit *to, Command::Message message, bool channelCenter)
{
	// channelCenter == true ... check radio of center
	// channelCenter == false ... check radio of group and FSM stack

	if (!channelCenter)
	{
		AISubgroup *subgrp = to->GetSubgroup();
		if (!subgrp) return false;	// BUG in MP
		int i, n = subgrp->_stack.Size();
		for (i=0; i<n; i++)
		{
			const Command *cmd = subgrp->_stack[i]._task;
			if (cmd->_message == message)
				return true;
		}
	}

	Assert(GetCenter());
	RadioChannel &radio = channelCenter ? GetCenter()->GetRadio() : GetRadio();

	// check radio channel
	int index = INT_MAX;
	while (true)
	{
		RadioMessage *msg = radio.FindPrevMessage(RMTCommand, index);
		if (!msg)
			break;
		Assert( dynamic_cast<RadioMessageCommand *>(msg) );
		RadioMessageCommand *msgCmd = static_cast<RadioMessageCommand *>(msg);
		Assert(msgCmd);
		Assert(msgCmd->GetFrom() == this);
		if (msgCmd->IsTo(to) && msgCmd->GetMessage() == message)
		{
			return true;
		}
	}

	RadioMessage *msg = radio.GetActualMessage();
	if (msg && msg->GetType() == RMTCommand)
	{
		Assert( dynamic_cast<RadioMessageCommand *>(msg) );
		RadioMessageCommand *msgCmd = static_cast<RadioMessageCommand *>(msg);
		Assert(msgCmd);
		Assert(msgCmd->GetFrom() == this);
		if (msgCmd->IsTo(to) && msgCmd->GetMessage() == message)
		{
			return true;
		}
	}

	return false;
}

void AIGroup::ClearGetInCommands(AIUnit *to)
{
	// cancel in radio
	int index = INT_MAX;
	while (true)
	{
		RadioMessage *msg = _radio.FindPrevMessage(RMTCommand, index);
		if (!msg) break;
		Assert( dynamic_cast<RadioMessageCommand *>(msg) );
		RadioMessageCommand *msgCmd = static_cast<RadioMessageCommand *>(msg);
		Assert(msgCmd);
		Assert(msgCmd->GetFrom() == this);
		if
		(
			msgCmd->IsTo(to) && msgCmd->GetMessage() == Command::GetIn &&
			(msgCmd->GetContext() == Command::CtxAuto || msgCmd->GetContext() == Command::CtxAutoSilent)
		)
		{
			_radio.Cancel(msg);
			index = INT_MAX;
		}
	}

	RadioMessage *msg = _radio.GetActualMessage();
	if (msg && msg->GetType() == RMTCommand)
	{
		Assert( dynamic_cast<RadioMessageCommand *>(msg) );
		RadioMessageCommand *msgCmd = static_cast<RadioMessageCommand *>(msg);
		Assert(msgCmd);
		Assert(msgCmd->GetFrom() == this);
		Assert(msgCmd->GetFrom() == this);
		if
		(
			msgCmd->IsTo(to) && msgCmd->GetMessage() == Command::GetIn &&
			(msgCmd->GetContext() == Command::CtxAuto || msgCmd->GetContext() == Command::CtxAutoSilent)
		)
		{
			_radio.Cancel(msg);
		}
	}
}

bool AIGroup::ReportSent(ReportSubject subject, const VehicleType *type)
{
	// check radio channel
	int index = INT_MAX;
	while (true)
	{
		RadioMessage *msg = GetRadio().FindPrevMessage(RMTReportTarget, index);
		if (!msg)
			break;
		Assert(dynamic_cast<RadioMessageReportTarget *>(msg));
		RadioMessageReportTarget *msgReport = static_cast<RadioMessageReportTarget *>(msg);
		Assert(msgReport);
		if (msgReport->GetSubject() == subject && msgReport->HasType(type))
		{
			return true;
		}
	}

	RadioMessage *msg = GetRadio().GetActualMessage();
	if (msg && msg->GetType() == RMTReportTarget)
	{
		Assert( dynamic_cast<RadioMessageReportTarget *>(msg) );
		RadioMessageReportTarget *msgReport = static_cast<RadioMessageReportTarget *>(msg);
		Assert(msgReport);
		if (msgReport->GetSubject() == subject && msgReport->HasType(type))
		{
			return true;
		}
	}

	return false;
}

struct GetInPair
{
	TargetId vehicle;
	PackedBoolArray list;
};
TypeContainsOLink(GetInPair)

class GetInPairs : public AutoArray<GetInPair>
{
	typedef AutoArray<GetInPair> base;

public:
	void Add(EntityAI *veh, AIUnit *unit);
};

void GetInPairs::Add(EntityAI *veh, AIUnit *unit)
{
	int i, n = Size();
	for (i=0; i<n; i++)
	{
		GetInPair &pair = Set(i);
		if (pair.vehicle == veh)
		{
			pair.list.Set(unit->ID() - 1, true);
			return;
		}
	}
	i = base::Add();
	GetInPair &pair = Set(i);
	pair.vehicle = TargetId(veh);
	pair.list.Set(unit->ID() - 1, true);
}

void AIGroup::GetInVehicles()
{
	if (IsAnyPlayerGroup())
		return;

	// get in / get out
	PackedBoolArray getout;
	GetInPairs getin;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = _units[i];
		if (!unit) continue;

		if (CommandSent(unit, Command::GetIn))
			continue;
		if (CommandSent(unit, Command::GetOut))
			continue;

		Transport *assigned = unit->VehicleAssigned();
		if (assigned && !assigned->IsAbleToMove())
			unit->UnassignVehicle();
		if
		(
			unit->IsGetInAllowed() &&
			unit->IsGetInOrdered() &&
			assigned
		)
		{
			// must be inside
			if (unit->IsFreeSoldier())
			{
				if (!unit->GetPerson()->IsActionInProgress(MFGetIn))
					getin.Add(assigned, unit);
			}
			// wrong vehicle or wrong position
			else if
			(
				assigned != unit->GetVehicle() ||
				(assigned->GetDriverAssigned() == unit) != (assigned->DriverBrain() == unit) ||
				(assigned->GetCommanderAssigned() == unit) != (assigned->CommanderBrain() == unit) ||
				(assigned->GetGunnerAssigned() == unit) != (assigned->GunnerBrain() == unit)
			)
			{
				getout.Set(unit->ID() - 1, true);
			}
		}
		else
		{
			// must be outside
			if (!unit->IsFreeSoldier())
			{
				getout.Set(unit->ID() - 1, true);
			}
		}
	}

	Command cmd;
	cmd._joinToSubgroup = MainSubgroup();
	cmd._context = Command::CtxAuto;
	if (!getout.IsEmpty())
	{
		cmd._message = Command::GetOut;
		SendCommand(cmd, getout);
	}

	cmd._message = Command::GetIn;
	for (int i=0; i<getin.Size(); i++)
	{
		GetInPair &pair = getin[i];
		//Target *tgt=FindTarget(pair.vehicle);
		//if( tgt )
		//{
		//  cmd._target = tgt;
		//	SendCommand(cmd, pair.list);
		//}
		//else
		//{
		//	Fail("Getin target unknown");
		//}
		cmd._target = pair.vehicle;
		SendCommand(cmd, pair.list);
	}
}

static int BetterTarget(const Ref<Target> *v0, const Ref<Target> *v1)
{
	Target *t0 = *v0;
	Target *t1 = *v1;
	return sign(t1->subjectiveCost - t0->subjectiveCost);
}

/*!
\patch 1.28 Date 10/19/2001 by Ondra
- Fixed: Bug in reaction to enemy detection caused passangers not attacking enemy.
*/
void AIGroup::ReactToEnemyDetected()
{
	AIUnit *leader = Leader();
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = _units[i];
		if (!unit) continue;
		unit->SetDanger();
		if (unit->GetCombatModeMajor() == CMCareless) continue;
		if (_combatModeMinor >= CMCombat) continue;

		// going to combat - get out of some vehicles
		if (unit->IsInCargo())
		{
			Transport *trans = unit->GetVehicleIn();
			if (trans && trans->Type()->GetUnloadInCombat())
			{
				unit->OrderGetIn(false);
			}
		}
	}
	if (_combatModeMinor < CMCombat)
	{
		if (leader && leader->GetLifeState()==AIUnit::LSAlive && NUnits()>1)
		{
			SendContact(leader);
		}
		_combatModeMinor = CMCombat;
	}
}

/*!
\patch 1.28 Date 10/30/2001 by Jirka
- Fixed: MP - gunner does not see any unit on the radar
*/
bool AIGroup::CreateTargetList(bool initialize, bool report)
{
	PROFILE_SCOPE(aiGCT);

	float nearestEnemyDist2 = 1e10;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = _units[i];
		if (!unit) continue;
//		if (!unit->IsLocal()) continue;
		if (!unit->IsUnit()) continue;
		if (unit->GetLifeState()!=AIUnit::LSAlive) continue;
		if (!unit->GetVehicle()) continue;
		unit->GetVehicle()->WhatIsVisible(_targetList, initialize);
		// check unit for nearestEnemyDist2
		saturateMin(nearestEnemyDist2,unit->GetNearestEnemyDist2());
	}

	_nearestEnemyDist2 = nearestEnemyDist2;
	//LogF("%s: nearestEnemyDist %g",(const char *)GetDebugName(),sqrt(_nearestEnemyDist2));

	_targetList.Manage(this);

	_enemiesDetected = 0;
	_unknownsDetected = 0;
	
	AICenter *center = GetCenter();

	bool someTarget=false;
	int n = _targetList.Size(); 
	for (int i=0; i<n; i++)
	{
		Target *vtar = _targetList[i];
		// any target that is vanished or destroyed should be updated
		// and no more processing is required
		if (vtar->vanished || vtar->destroyed)
		{
			SendReport(ReportDestroy, *vtar);
			// we might also send radio report

			if
			(
				vtar->destroyed && vtar->isKnown &&
				vtar->idKiller && vtar->timeReported<=TIME_MIN &&
				vtar->idExact && vtar->idExact->IsInLandscape()
			)
			{
				AIUnit *killer = vtar->idKiller->CommanderUnit();
				if (killer && killer->IsLocal() && !killer->IsAnyPlayer())
				{
					if (killer->GetLifeState()==AIUnit::LSAlive)
					{
						SendObjectDestroyed(killer,vtar->type);
					}
					vtar->timeReported = Glob.time;
					vtar->posReported = vtar->position;
				}
			}
				
			continue;	
		}
		bool doReport = report ? vtar->IsKnown() : vtar->isKnown;
		// report only recently seen targets
		if( report && doReport )
		{
			// check if we can report it

			if
			(
				vtar->lastSeen>=Glob.time-10 &&
				vtar->idSensor && vtar->idSensor->IsLocal() &&
				(!vtar->idSensor->IsNetworkPlayer() || vtar->timeReported<=TIME_MIN)
			)
			{
				// check how large position change is required to report
				float time = 240; // time from reporting 
				if (vtar->timeReported>Glob.time-time)
				{
					time = Glob.time-vtar->timeReported;
				}
				float minDist = 1000 - time*4;
				float minTime = 60;
				saturateMax(minDist,0);

				// calculate how big distance is required to report the change
				AIUnit *unit = vtar->idSensor->Brain();
				if (unit && unit->GetCombatMode()==CMStealth)
				{
					// in stealth mode report targets more often
					minDist *= 0.125;
					minTime = 10;
				}
				
				if
				(
					vtar->timeReported<Glob.time-minTime &&
					vtar->posReported.Distance2(vtar->position)>Square(minDist)
				)
				{
					// timeReported - when was target reported
					if (initialize)
					{
						vtar->timeReported = TIME_MIN;
						vtar->posReported = VZero;
					}
					else
					{
						if
						(
							vtar->type->IsKindOf(GWorld->Preloaded(VTypeStatic)) ||
							center->IsFriendly(vtar->side) ||
							center->IsNeutral(vtar->side)
						)
						{
							// never report friendly or static
							vtar->timeReported = Glob.time;
							vtar->posReported = vtar->position;
							//LogF("  ignore report");
						}
						else if( vtar->side == TSideUnknown || center->IsEnemy(vtar->side) )
						{
							// report enemy or unknown - only once
							vtar->timeReported = Glob.time;
							vtar->posReported = vtar->position;
							SendRadioReport(ReportNew, *vtar);
							//LogF("  send report");
						}
					}
				} // if (some change to report)
			} // if (can report)
		} // if (report)
		if( doReport )
		{
			SendReport(ReportNew, *vtar);

			if (center->IsEnemy(vtar->side) && IsLocal() && !IsAnyPlayerGroup())
			{
				for (int j=0; j<NSubgroups(); j++)
				{
					AISubgroup *subgrp = GetSubgroup(j);
					if (subgrp) subgrp->OnEnemyDetected(vtar->type, vtar->position);
				}
			}
		}

		if (vtar->IsKnownBySome())
		{
			if (vtar->side == TSideUnknown)
			{
				someTarget=true;
				_unknownsDetected++;
			}
			else if (center->IsEnemy(vtar->side))
			{
				Threat threat=vtar->type->GetDammagePerMinute(Square(200),1);
				if( (threat[VSoft]+threat[VArmor]+threat[VAir])>0 )
				{ // calculate only dangerous enemies
					_enemiesDetected++;
				}
				someTarget=true;
			}
		}
	}


	// check center database sometimes
	if (Glob.time>_checkCenterDBase+10 && !IsAnyPlayerGroup())
	{
		// use AICenter database for target recognition
		_checkCenterDBase = Glob.time;

		for (int i=0; i<_targetList.Size(); i++)
		{
			Target *tar = _targetList[i];

			float sideAccuracy=tar->FadingSideAccuracy();
			float typeAccuracy=tar->FadingSideAccuracy();
			
			if (sideAccuracy < 1.5 || typeAccuracy < 1.5 || tar->side==TSideUnknown )
			{	// inaccurate info, consult AICenter
				const AITargetInfo *tgt = center->FindTargetInfo(tar->idExact);
				if (tgt)
				{
					if (tgt->FadingSideAccuracy() > sideAccuracy)
					{
						tar->side = tgt->_side;
						tar->sideAccuracy = tgt->_accuracySide;
						tar->sideAccuracyTime = tgt->_timeSide;
						if (tar->sideAccuracy>1.5f) tar->sideChecked = true;
					}
					if (tgt->FadingTypeAccuracy() > typeAccuracy)
					{
						tar->type = tgt->_type;
						tar->accuracy = tgt->_accuracyType;
						tar->accuracyTime = tgt->_timeType;
					}
				}
			}
		}
	}
	// prepare subjective costs
	for (int i=0; i<_targetList.Size(); i++)
	{
		Target *tar = _targetList[i];

		if
		(
			!tar->idExact || !tar->IsKnown() ||
			tar->destroyed || tar->vanished ||
			tar->State(Leader())<TargetEnemyCombat
		)
		{
			// reset subjective cost
			tar->dammagePerMinute=0;
			tar->subjectiveCost=-1e10;
			continue;
		}

		
		if (center->IsEnemy(tar->side))
		{
			// calculate dammagePerMinute and subjectiveCost
			tar->dammagePerMinute=GetDammagePerMinute(tar);
			tar->subjectiveCost=GetSubjectiveCost(tar);
		}
		else if( tar->side==TSideUnknown )
		{
			tar->dammagePerMinute=0;
			tar->subjectiveCost=0;
		}
		else
		{
			// friendly or unknown - no need to test it
			tar->dammagePerMinute=0;
			tar->subjectiveCost=-1e5;
		}
	}


	// sort
	QSort(_targetList.Data(), _targetList.Size(), BetterTarget);

	// realloc target list if allocated space is too big
	int allocSize=_targetList.MaxSize();
	if( allocSize>32 && allocSize>_targetList.Size()*4 )
	{
		_targetList.Realloc(_targetList.Size()*2);
	}


#if LOG_THINK
		Log("Group %s, %d enemies, %d visible",
		(const char *)GetDebugName(), _targetList.Size(), visibleList.Size() );
#endif

	/*
	int time = TIME_IN_SCOPE();
	if (time>2500000)
	{
		LogF("%s: time in CreateTargetList %d",(const char *)GetDebugName(),time);
	}
	*/

	return someTarget;
}

/*!
\patch 1.45 Date 2/18/2002 by Ondra
- Fixed: When enemy was killed, killer was always detected,
even when there was no one able to detect him.
(He was detected by the dead unit).
*/

Target *AIGroup::AddTarget
(
	EntityAI *object, float accuracy, float sideAccuracy, float delay,
	const Vector3 *pos, AIUnit *sensor, float sensorDelay
)
{
	// check if target is already in list
	Target *target=NULL;
	for (int i=0; i<_targetList.Size(); i++)
	{
		Target *t = _targetList[i];
		if (t->idExact == object)
		{
			// set new accuracy
			target=t;
			break;
		}
	}

	if( !target )
	{
		target=new Target(this);
		_targetList.Add(target);
		target->idExact=object;
		//target->dammage=0;
		target->side=TSideUnknown;
		target->sideChecked = false;
		target->type=GWorld->Preloaded(VTypeAllVehicles);
		target->timeReported = TIME_MIN;
		target->isKnown=false;
	}
	if( !target->isKnown )
	{
		target->isKnown=true;
		if( !pos )
		{
			Vector3 apos=object->AimingPosition();
			// introduce some random error
			if (accuracy<1)
			{
				float randomX=GRandGen.RandomValue()*200-100;
				float randomZ=GRandGen.RandomValue()*200-100;
				// y above ground should be correct
				float aboveGround = GLandscape->SurfaceYAboveWater(apos.X(),apos.Z())-apos.Y();
				apos[0] += randomX;
				apos[2] += randomZ;
				apos[1] = GLandscape->SurfaceYAboveWater(apos[0],apos[2])+aboveGround;
			}
			target->position=apos;
		}
		else
		{
			target->position=*pos;
		}
		target->speed=VZero; // speed not known
		if (sensor)
		{
			target->lastSeen=Glob.time-40+sensorDelay;
			target->delaySensor = Glob.time+sensorDelay;
			target->delay = Glob.time+delay;
			target->idSensor = sensor->GetPerson();
		}
		else
		{
			target->lastSeen=Glob.time-40+delay;
			target->delay=Glob.time+delay;
		}
		// we assume we have seen it, but not very recently
		// we set lastSeen so that target is known only for 5 sec
		// that should be enough to activate defensive reaction
		// usually some Attack is issued
	}
	else
	{
		Time delayUntil=Glob.time+delay;
		if( target->delay>delayUntil ) target->delay=delayUntil;
	}
	const VehicleType *type=object->GetTypeAtLeast(accuracy);
	accuracy=type->GetAccuracy();
	if( target->FadingAccuracy()<accuracy )
	{
		// find first type description so that accuracy >=0.01
		target->accuracy=accuracy;
		target->accuracyTime=Glob.time;
		target->type=type;
	}
	if( target->FadingSideAccuracy()<sideAccuracy )
	{
		target->sideAccuracy=sideAccuracy;
		target->sideAccuracyTime=Glob.time;
		target->side=object->GetTargetSide(sideAccuracy);
		Assert( target->sideAccuracy<3 || target->side!=TSideUnknown );
		target->sideChecked = sideAccuracy>=1.5f;
	}
	float spotability=floatMin(accuracy*2.5,1);
	if( target->FadingSpotability()<spotability )
	{
		target->spotability=spotability;
		target->spotabilityTime=Glob.time;
	}
	return target;
}

float AIGroup::GetDammagePerMinute(Target *tar) const
{
	// calculate how much are assigned unit able to destroy this target
	const VehicleType *type=tar->type;
	float dpm = 0;
	for (int j=0; j<MAX_UNITS_PER_GROUP; j++)
	{
		AIUnit *unit = _units[j];
		if (!unit || !unit->IsUnit()) continue;
		if (_assignTarget[j]==tar)
		{
			VehicleKind kind = type->GetKind();
			float dist2 = tar->position.Distance2(unit->Position());
			EntityAI *veh = unit->GetVehicle();
			if (!veh) continue;
			Threat threat = veh->GetType()->GetDammagePerMinute(dist2, 1.0, veh);
			dpm += threat[kind];
		}
	}
	return dpm;
}

float AIGroup::GetSubjectiveCost(Target *tar) const
{
	const VehicleType *type=tar->type;
	float baseCost = type->GetCost();

	Object *obj = tar->idExact;
	EntityAI *enemy = dyn_cast<EntityAI>(obj);
	if (!enemy || !enemy->IsAbleToFire()) return baseCost;

	float maxCost = 0;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = _units[i];
		if (!unit || !unit->IsUnit()) continue;

		EntityAI *veh = unit->GetVehicle();
		if (!veh) continue;

		VehicleKind vehKind = veh->GetType()->GetKind();
		float dist2 = veh->Position().Distance2(tar->position);

		Threat threat = type->GetDammagePerMinute(dist2, 1.0);
		float dammagePerMinute = threat[vehKind];
		if (dammagePerMinute > 0)
		{
			float lTimeToLive = veh->GetArmor() * 60 / dammagePerMinute;
			float danger = unit->GetTimeToLive() / lTimeToLive;
			// note: lTimeToLive may be higher that timeToLive if tgt is relatively new
			saturateMin(danger, 10);
			danger *= veh->GetType()->GetCost();
			saturateMax(maxCost, danger);
		}
	}

	return baseCost + maxCost;
}

#define COEF_TTL			0.6
#define COEF_TIMEOUT	5.0
// TODO: timeout property of vehicletype
#define MIN_TIMEOUT	 2.0

//void CreateUnitsList(PackedBoolArray list, char *buffer)

#define DIAGS 0


void AIGroup::UnitAssignCanceled( AIUnit *unit )
{
	Assert( unit->GetGroup()==this );
	_assignTarget[unit->ID()-1] = NULL;
}

void AIGroup::AssignTargets()
{

	AICenter *center = GetCenter();
	Assert(center);
	if (!center) return;

	AIUnit *leader = Leader();
	if (!leader) return;
	if (leader->GetPerson()->IsUserStopped()) return;

#if DIAGS
	LogF("Group %s - targets assignement", (const char *)GetDebugName());
#endif
	// calculate TimeToLive for all units and whole group
	float ttl[MAX_UNITS_PER_GROUP];
	float groupTTL = FLT_MAX;
	int nUnits = 0;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = _units[i];
		if (!unit || !unit->IsUnit())
		{
			ttl[i] = FLT_MAX;
			continue;
		}

		Target *at = _assignTarget[i];
		if
		(
			!at ||
			_assignValidUntil[i]<Glob.time ||
			at->destroyed || at->vanished ||
			at->State(unit)<_assignTargetState[i]
		)
		{
			_assignTarget[i] = NULL;
			nUnits++;
		}
	#if DIAGS>=2
		else
		{
	LogF
	(
		"  Unit %d - TAS = %.3f",
		unit->ID(), _assignValidUntil[i]-Glob.time
	);
		}
#endif
		ttl[i] = unit->GetTimeToLive();
		saturateMin(groupTTL, ttl[i]);
#if DIAGS>=2
	LogF("  Unit %d - TTL = %.3f", unit->ID(), ttl[i]);
#endif
	}
	groupTTL *= COEF_TTL;
#if DIAGS>=2
	LogF("  Group's TTL = %.3f\n", groupTTL);
#endif

	if (nUnits <= 0)
	{
#if DIAGS>=2
	LogF("  No unassigned units, exitting");
#endif
		return;	// no unassigned units
	}

	// sort group target list 

	if (IsAnyPlayerGroup())
	{
		// player has to assign targets manualy
		return;
	}

	// try to assign units to 
	const float MinTimeToLive = 1e-10;
	const float MaxTimeToLive = 2419200.0;
	// first targets in the list are the most dangerous

	for (int i=0; i<_targetList.Size(); i++)
	{
		Target *tar = _targetList[i];
		if (!tar->idExact) continue;
		if (tar->idExact->IsDammageDestroyed()) continue;
		if (!tar->IsKnown() ) continue;
		if (!center->IsEnemy(tar->side)) break;

#if DIAGS>=2
		PackedBoolArray array;
		for (int j=0; j<MAX_UNITS_PER_GROUP; j++)
		{
			AIUnit *unit = _units[j];
			if (!unit || !unit->IsUnit()) continue;
			if (_assignTarget[j] == tar)
			{
				array.Set(j, true);
			}
		}
		char buffer[256];
		CreateUnitsList(array, buffer);

		LogF
		(
			"  Target %s (side %d): cost %.0f, dpm %.0f, targeted by %s",
			(const char *)tar->idExact->GetDebugName(),tar->side,
			tar->subjectiveCost, tar->dammagePerMinute, buffer
		);
		if (tar->dammagePerMinute>0 && array.IsEmpty())
		{
			float dammage = GetDammagePerMinute(tar);
			LogF("Dammage with no dammage source: %g",dammage);
		}
		const AITargetInfo *tgt = center->FindTargetInfo(tar->idExact);
		if( tgt )
		{
			LogF
			(
				"    - center info: side %d (%.3f), type %s (%.3f)",
				tgt->_side,tgt->FadingSideAccuracy(),
				(const char *)tgt->_type->GetDisplayName(),tgt->FadingTypeAccuracy()
			);
		}
#endif
		
		float enemyTTL = MaxTimeToLive;
		if (tar->dammagePerMinute > 0) 
		{
			enemyTTL = 60 * tar->type->GetArmor() / tar->dammagePerMinute;
			saturate(enemyTTL, MinTimeToLive, MaxTimeToLive);
		}
#if DIAGS
	if (enemyTTL > groupTTL)
		LogF
		(
			"  Try to cover target %s (side %d)",
			(const char *)tar->idExact->GetDebugName(),tar->side
		);
#endif
		bool someAssigned = false;
		while (enemyTTL > groupTTL)
		{
			// select best unit
			float maxSurplus = -FLT_MAX;
			AIUnit *bestUnit = NULL;
			FireResult bestResult;
			bool attack = false;

			for (int j=0; j<MAX_UNITS_PER_GROUP; j++)
			{
				AIUnit *unit = _units[j];
				if (!unit || !unit->IsUnit()) continue;
				if (_assignTarget[j]) continue;
				if (unit->IsHoldingFire()) continue; // ???
				if (unit->GetAIDisabled()&AIUnit::DATarget) continue;
				// note: group leader may assign target to himself
				//if (unit->IsGroupLeader()) continue;
				if (!unit->GetVehicle()->IsAbleToFire()) continue;

//				bool canFire = !unit->IsPlayer();
				bool canFire = true;
				float attackCoef = unit->IsKeepingFormation() ? 1.0/2 : 1.0/16;
				float attackCost = unit->IsKeepingFormation() ? 1000 : 10000;

				bool canAttack =
				(
					!CommandSent(unit, Command::Attack) &&
					!CommandSent(unit, Command::AttackAndFire) &&
					!CommandSent(unit, Command::GetOut) &&
					!CommandSent(unit, Command::GetIn)
				);
	
				// TODO: delay before attacking

				// before issuing Attack there is delay
				// unit is assigned to target but attack is not issued
				// if unit is not able to fire during this time
				// attack is issued (if there is still some surplus from using it)
				if (unit==leader && unit->IsKeepingFormation())
				{
					canAttack=false;
				}

				FireResult result;
				if (canFire)
				{
					if (unit->GetVehicle()->WhatFireResult(result, *tar, ttl[j]))
					{
						float surplus = result.Surplus();
						if (surplus > maxSurplus)
						{
							maxSurplus = surplus;
							bestUnit = unit;
							bestResult = result;
							attack = false;
						}
					}
					else
					{
#if DIAGS
	LogF
	(
		"  - Unit %d - fire is not possible", unit->ID()
	);
#endif
					}
				}
				else
				{
#if DIAGS
	LogF
	(
		"  - Unit %d - cannot fire", unit->ID()
	);
#endif
				}
				if (canAttack)
				{
					if (unit->GetVehicle()->WhatAttackResult(result, *tar, ttl[j]))
					{
						float surplus = result.Surplus()*attackCoef-attackCost;
						if (surplus > maxSurplus)
						{
							maxSurplus = surplus;
							bestUnit = unit;
							bestResult = result;
							attack = true;
						}
					}
					else
					{
#if DIAGS
	LogF
	(
		"  - Unit %d - attack is not possible", unit->ID()
	);
#endif
					}
				} // if( canAttack )
				else
				{
#if DIAGS
	LogF
	(
		"  - Unit %d - attack is forbidden%s%s%s%s", unit->ID(),
		unit->IsKeepingFormation() ? " Semaphore" : "",
		CommandSent(unit, Command::Attack) ? " Attack" : "",
		CommandSent(unit, Command::AttackAndFire) ? " Attack" : "",
		CommandSent(unit, Command::GetIn) ? " GetIn" : "",
		CommandSent(unit, Command::GetOut) ? " GetOut" : ""
	);
#endif
				}
			}
			if (!bestUnit) break;

			// update ttl
			VehicleKind kind = tar->type->GetKind();
			float dist2 = tar->position.Distance2(bestUnit->Position());
			Threat threat = bestUnit->GetVehicle()->GetType()->GetDammagePerMinute(dist2, 1.0, bestUnit->GetVehicle());
			tar->dammagePerMinute += threat[kind];
			if(tar->dammagePerMinute > 0)
			{
				enemyTTL = 60 * tar->type->GetArmor() / tar->dammagePerMinute;
			}

#if DIAGS
	LogF("    Unit %d assigned", bestUnit->ID());
#endif
			{
				float dt = COEF_TIMEOUT * enemyTTL;
				float minTimeout=bestUnit->GetVehicle()->GetMinFireTime();
				saturate(dt, minTimeout, 120);

				// send AssignTarget to radio
				PackedBoolArray list;
				int bestI = bestUnit->ID() - 1;
				list.Set(bestI, true);
				_assignTarget[bestI] = tar;
				_assignTargetState[bestI] = tar->State(bestUnit);
				_assignValidUntil[bestI] = Glob.time+dt;

				SendTarget(tar, false, false, list);
				_lastSendTargetTime = Glob.time;
				someAssigned = true;
				if (attack)
				{
					SendTarget(tar, true, false, list);
				}

				// no hide nor attack - "IGNORE" behaviour
				bestUnit->GetSubgroup()->ClearAttackCommands();
#if DIAGS
	LogF("    - fire for %.1f sec",dt);
#endif
			}

			if (--nUnits <= 0) return;
		} // while (ttl)
		if (someAssigned) break; // only one target per function call
	} // for (target)

	// all units left should hide
	// select hide command target

	bool groupEnableHideEnemy = !GetFlee();
	bool groupEnableHideUnknown = groupEnableHideEnemy;

	if (groupEnableHideEnemy)
	{
		// check for enemy / unknown targets
		groupEnableHideEnemy = false;
		groupEnableHideUnknown = false;
		for (int i=0; i<_targetList.Size(); i++)
		{
			Target *tar = _targetList[i];
			if (!tar->idExact) continue;
			if (tar->idExact->IsDammageDestroyed()) continue;
			if (!tar->IsKnown() ) continue;
			if (center->IsEnemy(tar->side))
			{
				groupEnableHideEnemy = true;
				groupEnableHideUnknown = true;
				break;
			}
			else if (tar->side==TSideUnknown)
			{
				groupEnableHideUnknown = true;
			}
		}
	}

	for (int j=0; j<MAX_UNITS_PER_GROUP; j++)
	{
		AIUnit *unit = _units[j];
		if (!unit || !unit->IsUnit()) continue;
		if (IsAnyPlayerGroup() && unit->IsGroupLeader()) continue;
		
		// check if there is some hide command issued
		// if there is valid hide, leave it

		AISubgroup *subgrp = unit->GetSubgroup();
		bool oldHideTgt = subgrp->CheckHide();
		bool enableHide=false;

		if (_assignTarget[j]==NULL && unit->GetCombatMode() != CMCareless)
		{
			if
			(
				groupEnableHideEnemy ||
				// in stealth hide also from unknown targets
				groupEnableHideUnknown && unit->GetCombatMode() == CMStealth
			)
			{
				if (!unit->IsKeepingFormation())
				{
					// TODO: enable hiding of other kinds of vehicles
					if( unit->IsFreeSoldier() ) enableHide=true;
				}
			}
		}

		// select hide target

		if( oldHideTgt==enableHide ) continue;

		// hide target changed - issue command
		subgrp->ClearAttackCommands();

		if( enableHide )
		{
			Command cmd;
			cmd._message = Command::Hide;
			cmd._targetE = NULL;
			cmd._destination = unit->Position();
			cmd._id = GetNextCmdId();

			IssueAutoCommand(cmd, unit);
		}
	}
}

#define	ATTACK_DISTANCE		250.0
Threat AIGroup::GetAttackInfluence()
{
	Threat sum;
	int i;
	for (i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = UnitWithID(i + 1);
		if (!unit)
			continue;
		const VehicleType *type = unit->GetPerson()->GetType();
		sum += type->GetDammagePerMinute(ATTACK_DISTANCE, 1.0);
		if (!unit->IsSoldier())
		{	// driver
			type = unit->GetVehicle()->GetType();
			sum += type->GetDammagePerMinute(ATTACK_DISTANCE, 1.0);
		}
	}
	for (int v=0; v<_vehicles.Size(); v++)
	{
		Transport *veh = _vehicles[v];
		if (!veh || veh->IsDammageDestroyed())
			continue;
		sum += veh->GetType()->GetDammagePerMinute(ATTACK_DISTANCE, 1.0);
	}
	return sum;	// return total dammage per minute
}

Threat AIGroup::GetDefendInfluence() 
{
	Threat sum;
	int i;
	for (i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = UnitWithID(i + 1);
		if (!unit)
			continue;
		const VehicleType *type = unit->GetPerson()->GetType();
		sum[type->GetKind()] += type->GetArmor();
		if (!unit->IsSoldier())
		{	// driver
			type = unit->GetVehicle()->GetType();
			sum[type->GetKind()] += type->GetArmor();
		}
	}
	for (int v=0; v<_vehicles.Size(); v++)
	{
		Transport *veh = _vehicles[v];
		if (!veh || veh->IsDammageDestroyed())
			continue;
		const VehicleType *type = veh->GetType();
		sum[type->GetKind()] += type->GetArmor();
	}
	return sum;	// return armor
}

void AIGroup::AddFirstWaypoint(Vector3Par pos)
{
	_wp.Insert(0);
	ArcadeWaypointInfo &wInfo = _wp[0];
	wInfo.Init();
	wInfo.position = pos;
	wInfo.type = ACMOVE;
}

void AIGroup::Move(AISubgroup *who, Vector3Val destination, Command::Discretion discretion)
{
	if (!who) return;
	if (who->Leader())
	{
		Vector3Val pos = who->Leader()->Position();
		float dist = 200;
		if ((pos - destination).SquareSizeXZ() > Square(dist))
		{
			for (int i=0; i<who->NUnits(); i++)
			{
				AIUnit *unit = who->GetUnit(i);
				if (unit) unit->OrderGetIn(true);
			}
		}
		AssignVehicles();
		GetInVehicles();
	}

	Command cmd;
	cmd._message = Command::Move;
	cmd._destination = destination;
	cmd._discretion = discretion;
	cmd._context = Command::CtxMission;
	if (who == MainSubgroup())
	{
		SendCommand(cmd);
	}
	else
		SendCommand(cmd, who->GetUnitsList());
}

void AIGroup::Wait(AISubgroup *who, Time until, Command::Discretion discretion)
{
	Command cmd;
	cmd._message = Command::Wait;
	cmd._time = until;
	cmd._discretion = discretion;
	cmd._context = Command::CtxMission;
	if (who == MainSubgroup())
	{
		SendCommand(cmd);
	}
	else
		SendCommand(cmd, who->GetUnitsList());
}

void AIGroup::Attack(AISubgroup *who, TargetType *target, Command::Discretion discretion)
{
	Command cmd;
	cmd._message = Command::Attack;
	cmd._target = target;
	//bool found = MainSubgroup()->FindAttackPosition(cmd._target, cmd._destination);
	//Assert(found);
	// TODO: some correct attack position
	cmd._discretion = discretion;
	cmd._context = Command::CtxMission;
	/*
	for( int u=0; u<who->NUnits(); u++ )
	{
		AIUnit *unit=who->GetUnit(u);
		if( !unit || unit->GetInCargo() ) continue;
		if( 
		who->
	}
	*/
	if (who == MainSubgroup())
	{
		cmd._destination = who->GetGroup()->Leader()->GetVehicle()->Position();
		SendCommand(cmd);
	}
	else
	{
		cmd._destination = who->Leader()->GetVehicle()->Position();
		SendCommand(cmd, who->GetUnitsList());
	}
}

void AIGroup::Fire(AISubgroup *who, TargetType *target, int weapon, Command::Discretion discretion)
{
	Command cmd;
	cmd._message = Command::Fire;
	cmd._target = target;
	cmd._param = weapon;
	cmd._discretion = discretion;
	cmd._context = Command::CtxMission;
	if (who == MainSubgroup())
	{
		SendCommand(cmd);
	}
	else
		SendCommand(cmd, who->GetUnitsList());
}

void AIGroup::GetIn(AISubgroup *who, TargetType *target, Command::Discretion discretion)
{
	Command cmd;
	cmd._message = Command::GetIn;
	cmd._target = target;
	cmd._discretion = discretion;
	cmd._context = Command::CtxMission;
	if (who == MainSubgroup())
	{
		SendCommand(cmd);
	}
	else
		SendCommand(cmd, who->GetUnitsList());
}

void AIGroup::MoveUnit(AIUnit *who, Vector3Val destination, Command::Discretion discretion)
{
	Command cmd;
	cmd._message = Command::Move;
	cmd._destination = destination;
	cmd._discretion = discretion;
	SendAutoCommandToUnit(cmd, who, false);
}

float AIGroup::UpdateAndGetThreshold()
{
	if (Glob.time >= _thresholdValid)
	{
		_threshold = GRandGen.RandomValue();
		_thresholdValid = Glob.time + 60.0f;
	}
	return _threshold;
}

bool AIGroup::GetAllDone() const
{
	if (IsPlayerGroup()) return false;

	// query group status
	if( !_radio.Done() )
	{
		return false;
	}
	// check if main subgroup completed
	if( MainSubgroup()->HasCommand() ) return false;
	AIUnit *leader = Leader();
	// group may not be done if leader is not ready
	if (leader && leader->GetLifeState()!=AIUnit::LSAlive) return false;
	return true;
}

void AIGroup::AllGetOut()
{
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = _units[i];
		if (unit) unit->AllowGetIn(false);
	}
}

void AIGroup::CargoGetOut()
{
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = _units[i];
		if (unit && unit->IsInCargo()) unit->AllowGetIn(false);
	}
}

Target *AIGroup::FindTargetAll(TargetType * id) const
{
	if (!id) return NULL;
	for (int i=0; i<_targetList.Size(); i++)
	{
		const Target *t = _targetList[i];
		if (t->idExact == id)
		{
			return const_cast<Target *>(t);
		}
	}
	return NULL;
}

Target *AIGroup::FindTarget(TargetType *id) const
{
	if (!id) return NULL;
	for (int i=0; i<_targetList.Size(); i++)
	{
		const Target *t = _targetList[i];
		if (!t->IsKnown()) continue;
		if (t->idExact == id)
		{
			//if (id->GetTotalDammage() >= 1.0)
			//	return NULL;
			return const_cast<Target *>(t);
		}
	}
	return NULL;
}

bool AIGroup::FindTarget(TargetType *id, TargetSide &side, const VehicleType *&type) const
{
	// TODO: use FindTarget(id) instead
	bool result = false;
	Target *tar = FindTarget(id);
	if (tar)
	{
		result = true;
		side = tar->side;
		type = tar->type;
	}

	return result;
}

#define COMMAND_TIMEOUT			480.0		// 8 min

#define FUEL_NEAR			100.0f
#define FUEL_FAR			500.0f

const AITargetInfo *AIGroup::FindRefuelPosition(AIUnit::ResourceState state) const
{
	if (state == AIUnit::RSNormal) return NULL;
	Assert(Leader());
	
	// find fuel in close range
	const AITargetInfo *depot = NULL;
	float dist2Depot = FLT_MAX;
	const AITargetInfo *truck = NULL;
	float dist2Truck = FLT_MAX;

	for (int i=0; i<GetCenter()->NTargets(); i++)
	{
		const AITargetInfo &info = GetCenter()->GetTarget(i);
		if (info._destroyed)
			continue;
		if (!(info._side == TCivilian) && !(GetCenter()->IsFriendly(info._side)))
			continue;
		if (info._type->GetMaxFuelCargo() <= 0)
			continue;
		VehicleSupply *veh = dyn_cast<VehicleSupply, Object>(info._idExact);
		if (!veh) continue;
		if (veh->GetFuelCargo() <= 0) continue;
		if( GetCenter()->GetExposurePessimistic(info._realPos)>0 ) continue;
		float dist2 = (Leader()->Position() - info._realPos).SquareSizeXZ();
		
		if (info._type->IsKindOf(GLOB_WORLD->Preloaded(VTypeStatic)))
		{
			if (dist2 < dist2Depot)
			{
				dist2Depot = dist2;
				depot = &info;
			}
		}
		else
		{
			if (dist2 < dist2Truck)
			{
				dist2Truck = dist2;
				truck = &info;
			}
		}
	}

	if (depot && dist2Depot < Square(FUEL_NEAR))
	{
		// refuel in near depot
		return depot;
	}
	if (truck && dist2Truck < Square(FUEL_NEAR))
	{
		return truck;
	}

	if (state == AIUnit::RSLow) return NULL;

	// find fuel in wider range
	if (depot && dist2Depot < Square(FUEL_FAR))
	{
		// refuel in far depot
		return depot;
	}
	if (truck && dist2Truck < Square(FUEL_FAR))
	{
		// refuel in far truck
		return truck;
	}

	// fuel not found
	return NULL;
}

bool AIGroup::FindRefuelPosition(Command &cmd) const
{
	const AITargetInfo *target = FindRefuelPosition(AIUnit::RSCritical);
	if (!target) return false;

	cmd._destination = target->_realPos;
	cmd._target = target->_idExact;
	cmd._time = Glob.time + COMMAND_TIMEOUT;
	return true;
}

/*!
\patch 1.16 Date 08/14/2001 by Jirka
- Fixed: Unit repeats ask for support
*/
bool AIGroup::CheckFuel()
{
	Assert(Leader());
	if (IsAnyPlayerGroup())
		return true;

	bool ok = true;
	AIUnit::ResourceState stateMax = AIUnit::RSNormal;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = _units[i];
		if (!unit) continue;
		AIUnit::ResourceState state = GetFuelStateReported(unit);
		if (state == AIUnit::RSNormal) continue;

		ok = false;
		if (unit->GetCombatMode() == CMCareless) continue;
		// check both group and center radio channel
		if (CommandSent(unit, Command::Refuel, false)) continue;
		if (CommandSent(unit, Command::Refuel, true)) continue;
		if (CommandSent(unit, Command::Support)) continue;
		if (state > stateMax) stateMax = state;
	}

	const AITargetInfo *target = FindRefuelPosition(stateMax);

	if (target)
	{
		bool channelCenter = false;
		EntityAI *veh = target->_idExact;
		if (veh)
		{
			AIUnit *unit = veh->CommanderUnit();
			if (unit && unit->GetLifeState() == AIUnit::LSAlive)
				channelCenter = unit->GetGroup() != this;
		}

		Command cmd;
		cmd._message = Command::Refuel;
		cmd._destination = target->_realPos;
		cmd._target = target->_idExact;
		cmd._time = Glob.time + COMMAND_TIMEOUT;

		for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
		{
			AIUnit *unit = _units[i];
			if (!unit) continue;
			if (unit->GetCombatMode() == CMCareless) continue;
			// check both group and center radio channel
			if (CommandSent(unit, Command::Refuel, false)) continue;
			if (CommandSent(unit, Command::Refuel, true)) continue;
			if (CommandSent(unit, Command::Support)) continue;

			if (unit->GetFuelState() >= stateMax)
			{
				SendAutoCommandToUnit(cmd, unit, true, channelCenter);
			}
		}
	}
	else
	{
		if (stateMax == AIUnit::RSCritical && !IsAnyPlayerGroup())
		{
			Assert(_center);
			// FIX
			if (_center->GetRadio().Done() && _center->CanSupport(ATRefuel) && !_center->WaitingForSupport(this, ATRefuel))
			{
				_center->GetRadio().Transmit
				(
					new RadioMessageSupportAsk(this, ATRefuel),
					_center->GetLanguage()
				);
			}
		}
	}

	return ok;
}

#define REPAIR_NEAR			100.0f
#define REPAIR_FAR			500.0f

const AITargetInfo *AIGroup::FindRepairPosition(AIUnit::ResourceState state) const
{
	if (state == AIUnit::RSNormal) return NULL;
	Assert(Leader());

	// find repair in close range
	const AITargetInfo *depot = NULL;
	float dist2Depot = FLT_MAX;
	const AITargetInfo *truck = NULL;
	float dist2Truck = FLT_MAX;

	for (int i=0; i<GetCenter()->NTargets(); i++)
	{
		const AITargetInfo &info = GetCenter()->GetTarget(i);
		if (!info._type) continue;
		if (!(info._side == TCivilian) && !(GetCenter()->IsFriendly(info._side)))
			continue;
		if (info._type->GetMaxRepairCargo() <= 0)
			continue;
		VehicleSupply *veh = dyn_cast<VehicleSupply, Object>(info._idExact);
		if (!veh) continue;
		if (veh->GetRepairCargo() <= 0) continue;
		if( GetCenter()->GetExposurePessimistic(info._realPos)>0 ) continue;
		float dist2 = (Leader()->Position() - info._realPos).SquareSizeXZ();

		if (info._type->IsKindOf(GLOB_WORLD->Preloaded(VTypeStatic)))
		{
			if (dist2 < dist2Depot)
			{
				dist2Depot = dist2;
				depot = &info;
			}
		}
		else
		{
			if (dist2 < dist2Truck)
			{
				dist2Truck = dist2;
				truck = &info;
			}
		}
	}

	if (depot && dist2Depot < Square(REPAIR_NEAR))
	{
		// repair in near depot
		return depot;
	}
	if (truck && dist2Truck < Square(REPAIR_NEAR))
	{
		// repair in near truck
		return truck;
	}

	if (state == AIUnit::RSLow) return NULL;

	// find repair in wider range
	if (depot && dist2Depot < Square(REPAIR_FAR))
	{
		// repair in far depot
		return depot;
	}
	if (truck && dist2Truck < Square(REPAIR_FAR))
	{
		// repair in far truck
		return truck;
	}

	// repair not found
	return NULL;
}

bool AIGroup::FindRepairPosition(Command &cmd) const
{
	const AITargetInfo *target = FindRepairPosition(AIUnit::RSCritical);
	if (!target) return false;

	cmd._destination = target->_realPos;
	cmd._target = target->_idExact;
	cmd._time = Glob.time + COMMAND_TIMEOUT;
	return true;
}

bool AIGroup::CheckArmor()
{
	Assert(Leader());
	if (IsAnyPlayerGroup())
		return true;

	bool ok = true;
	AIUnit::ResourceState stateMax = AIUnit::RSNormal;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = _units[i];
		if (!unit) continue;
		AIUnit::ResourceState state = GetDammageStateReported(unit);
		if (state == AIUnit::RSNormal) continue;

		ok = false;
		if (unit->GetCombatMode() == CMCareless) continue;
		// check both group and center radio channel
		if (CommandSent(unit, Command::Repair, false)) continue;
		if (CommandSent(unit, Command::Repair, true)) continue;
		if (CommandSent(unit, Command::Support)) continue;

		if (state > stateMax) stateMax = state;
	}

	const AITargetInfo *target = FindRepairPosition(stateMax);

	if (target)
	{
		bool channelCenter = false;
		EntityAI *veh = target->_idExact;
		if (veh)
		{
			AIUnit *unit = veh->CommanderUnit();
			if (unit && unit->GetLifeState() == AIUnit::LSAlive)
				channelCenter = unit->GetGroup() != this;
		}

		Command cmd;
		cmd._message = Command::Repair;
		cmd._destination = target->_realPos;
		cmd._target = target->_idExact;
		cmd._time = Glob.time + COMMAND_TIMEOUT;

		for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
		{
			AIUnit *unit = _units[i];
			if (!unit) continue;
			if (unit->GetCombatMode() == CMCareless) continue;
			// check both group and center radio channel
			if (CommandSent(unit, Command::Repair, false)) continue;
			if (CommandSent(unit, Command::Repair, true)) continue;
			if (CommandSent(unit, Command::Support)) continue;

			if (unit->GetArmorState() >= stateMax)
			{
				SendAutoCommandToUnit(cmd, unit, true, channelCenter);
			}
		}
	}
	else
	{
		if (stateMax == AIUnit::RSCritical && !IsAnyPlayerGroup())
		{
			Assert(_center);
			if (_center->GetRadio().Done() && _center->CanSupport(ATRepair) && !_center->WaitingForSupport(this, ATRepair))
			{
				_center->GetRadio().Transmit
				(
					new RadioMessageSupportAsk(this, ATRepair),
					_center->GetLanguage()
				);
			}
		}
	}

	return ok;
}

#define HOSPITAL_NEAR			100.0f
#define HOSPITAL_FAR			500.0f

const AITargetInfo *AIGroup::FindHealPosition(AIUnit::ResourceState state, AIUnit *unit) const
{
	if (state == AIUnit::RSNormal) return NULL;
	Assert(Leader());

	// find hospital / ambulance in close range
	const AITargetInfo *depot = NULL;
	float dist2Depot = FLT_MAX;
	const AITargetInfo *truck = NULL;
	float dist2Truck = FLT_MAX;

	Vector3 pos = unit ? unit->Position() : Leader()->Position();

	for (int i=0; i<GetCenter()->NTargets(); i++)
	{
		const AITargetInfo &info = GetCenter()->GetTarget(i);
		if (info._destroyed)
			continue;
		const VehicleType *type = info._idExact ? info._idExact->GetType() : info._type;
		if (!type) continue;
		if (!type->IsAttendant())
			continue;
		if (!(info._side == TCivilian) && !(GetCenter()->IsFriendly(info._side)))
			continue;
		if (unit && info._idExact == unit->GetPerson()) continue;
		if (unit && info._idExact == unit->GetVehicleIn()) continue;
		if( GetCenter()->GetExposureOptimistic(info._realPos)>50.0f ) continue;
		float dist2 = (pos - info._realPos).SquareSizeXZ();

		if (type->IsKindOf(GLOB_WORLD->Preloaded(VTypeStatic)))
		{
			if (dist2 < dist2Depot)
			{
				dist2Depot = dist2;
				depot = &info;
			}
		}
		else
		{
			if (dist2 < dist2Truck)
			{
				dist2Truck = dist2;
				truck = &info;
			}
		}
	}

	if (depot && dist2Depot < Square(HOSPITAL_NEAR))
	{
		// heal in near hospital
		return depot;
	}
	if (truck && dist2Truck < Square(HOSPITAL_NEAR))
	{
		// heal in near ambulance
		return truck;
	}

	if (state == AIUnit::RSLow) return NULL;

	// find hospital / ambulance in wider range
	if (depot && dist2Depot < Square(HOSPITAL_FAR))
	{
		// heal in far hospital
		return depot;
	}
	if (truck && dist2Truck < Square(HOSPITAL_FAR))
	{
		// heal in far ambulance
		return truck;
	}

	// hospital / ambulance not found
	return NULL;
}

bool AIGroup::FindHealPosition(Command &cmd) const
{
	const AITargetInfo *target = FindHealPosition(AIUnit::RSCritical);
	if (!target) return false;

	cmd._destination = target->_realPos;
	cmd._target = target->_idExact;
	cmd._time = Glob.time + COMMAND_TIMEOUT;
	return true;
}


void AIGroup::SendIsDown(Transport *vehicle, AIUnit *down)
{
	// any unit from the same vehicle should report if possible
	// check which units are alive
	// consider: better check here, with Report Status verification
	AIUnit *reportTransport = NULL;
	AIUnit *report = NULL;
	AIUnit *reportPlayer = NULL;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = _units[i];
		if (!unit) continue;
		// check when was the unit last seen (or heard)
		// ignore units that are alive
		if (unit->GetLifeState()==AIUnit::LSAlive)
		{
			if (!reportTransport && unit->GetVehicle()==vehicle)
			{
				reportTransport = unit;
			}
			if (report) continue;
			if (unit->IsPlayer() || unit->GetPerson()->IsRemotePlayer())
			{
				if (!reportPlayer) reportPlayer = unit;
				continue;
			}
			report = unit;
			continue;
		}
	}
	// lowest ID AI soldier should report casualties
	if (reportTransport) report = reportTransport;
	if (!report) report = reportPlayer;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = _units[i];
		if (!unit) continue;
		if (unit->GetLifeState()!=AIUnit::LSAlive)
		{
			if (report)
			{
				if (report->IsLocal())
				{
					SendUnitDown(report,unit);
				}
			}
			else
			{
				if (unit->GetLifeState()==AIUnit::LSDead)
				{
					AISubgroup *subgrp = unit->GetSubgroup();
					if (subgrp) subgrp->ReceiveAnswer(unit,AI::UnitDestroyed);
				}
			}
		}
	}
}

void AIGroup::SendIsDown(AIUnit *down)
{
	// lowest ID AI soldier should report casualties
	// autoselect sender
	// check which units are alive
	// consider: better check here, with Report Status verification
	AIUnit *report = NULL;
	AIUnit *reportPlayer = NULL;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = _units[i];
		if (!unit) continue;
		// check when was the unit last seen (or heard)
		// ignore units that are alive
		if (unit->GetLifeState()==AIUnit::LSAlive)
		{
			if (report) continue;
			if (unit->IsPlayer() || unit->GetPerson()->IsRemotePlayer())
			{
				if (!reportPlayer) reportPlayer = unit;
				continue;
			}
			report = unit;
			continue;
		}
	}
	// lowest ID AI soldier should report casualties
	if (!report) report = reportPlayer;
	// for sure: verify unit is down
	if (down->GetLifeState()!=AIUnit::LSAlive)
	{
		if (report)
		{
			if (report->IsLocal())
			{
				SendUnitDown(report,down);
			}
		}
		else
		{
			if (down->GetLifeState()==AIUnit::LSDead)
			{
				AISubgroup *subgrp = down->GetSubgroup();
				if (subgrp) subgrp->ReceiveAnswer(down,AI::UnitDestroyed);
			}
		}
	}
}

void AIGroup::CheckAlive()
{
	// check which units are alive
	// consider: better check here, with Report Status verification
	AIUnit *leader = Leader();
	bool leaderAlive = leader && leader->GetLifeState()==AIUnit::LSAlive;
	if (leader && !leaderAlive)
	{
		// set max. time when to known leader is dead
		SetReportBeforeTime(leader,Glob.time+30);
	}
	if ((!IsAnyPlayerGroup() || Glob.config.IsEnabled(DTAutoSpot)) && leaderAlive)
	{
		// only AI groups or autospot players will ask for status
		for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
		{
			AIUnit *unit = _units[i];
			if (!unit) continue;
			// check when was the unit last seen (or heard)
			// ignore units that are alive
			if (unit->GetLifeState()==AIUnit::LSAlive) continue;
			// find corresponding target
			Target *tgt = FindTarget(unit->GetVehicle());
			if (!tgt || tgt->lastSeen<Glob.time-50)
			{
				// ask unit to report status
				// check if report was not asked yet?
				if (GetReportBeforeTime(unit)>Glob.time+30)
				{
					// nobody asked about it - it is probably dead?
					PackedBoolArray list;
					list.Set(i,true);
					SendReportStatus(list);
				}

			}
		}
	}
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = _units[i];
		if (!unit) continue;
		// only local unit can report
		if (GetReportBeforeTime(unit)<Glob.time)
		{
			SetReportBeforeTime(unit,TIME_MAX);
			SendIsDown(unit);
		}
	}
}

void AIGroup::CheckSupport()
{
	bool playerGroup = IsPlayerGroup();

	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = _units[i];
		if (!unit || !unit->IsUnit()) continue;
		if (unit->IsPlayer() && playerGroup) continue;
		if (unit->GetCombatMode() == CMCareless) continue;
		if (playerGroup && unit->IsKeepingFormation()) continue;

		if (CommandSent(unit, Command::Support)) continue;
		if (CommandSent(unit, Command::Heal, false)) continue;
		if (CommandSent(unit, Command::Heal, true)) continue;
		if (CommandSent(unit, Command::Rearm, false)) continue;
		if (CommandSent(unit, Command::Rearm, true)) continue;
		if (CommandSent(unit, Command::Repair, false)) continue;
		if (CommandSent(unit, Command::Repair, true)) continue;
		if (CommandSent(unit, Command::Refuel, false)) continue;
		if (CommandSent(unit, Command::Refuel, true)) continue;

		VehicleSupply *veh = dyn_cast<VehicleSupply>(unit->GetVehicle());
		if (!veh) continue;

		EntityAI *target = veh->GetAllocSupply();
		if (!target)
		{
			float minDist2 = FLT_MAX;
			const OLinkArray<AIUnit> &supplyUnits = veh->GetSupplyUnits();
			for (int j=0; j<supplyUnits.Size(); j++)
			{
				if (!supplyUnits[j]) continue;
				EntityAI *t = supplyUnits[j]->GetVehicle();
				float dist2 = t->Position().Distance2(veh->Position());
				if (dist2 < minDist2)
				{
					target = t;
					minDist2 = dist2;
				}
			}
		}
		if (!target) continue;

		if (target->Position().Distance2(veh->Position())>Square(veh->GetPrecision()*4))
		{
			Command cmd;
			cmd._message = Command::Support;
			cmd._destination = target->Position();
			cmd._target = target;
			cmd._time = Glob.time + COMMAND_TIMEOUT;
			if (unit->IsPlayer() || unit->IsKeepingFormation())
				SendAutoCommandToUnit(cmd, unit, true);
			else
				NotifyAutoCommand(cmd, unit);
		}
	}
}

bool AIGroup::CheckHealth()
{
	bool ok = true;
	
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = _units[i];
		if (!unit) continue;
		if (unit->GetCombatMode() == CMCareless) continue;

		EntityAI *vehicle = unit->GetVehicle();
		if (vehicle->GetType()->IsAttendant())
		{
			// heal himself
			if (unit->GetHealthState() != AIUnit::RSNormal && !vehicle->CheckActionProcessing(ATHeal, unit))
			{
				UIAction action;
				action.type = ATHeal;
				action.target = vehicle;
				action.param = 0;
				action.param2 = 0;
				action.priority = 0;
				action.showWindow = false;
				action.hideOnUse = false;
				vehicle->StartActionProcessing(action, unit);
			}
			continue;
		}

		AIUnit::ResourceState state = GetHealthStateReported(unit);
		if (state == AIUnit::RSNormal) continue;

		ok = false;

		// check both group and center radio channel
		if (CommandSent(unit, Command::Heal, false)) continue;
		if (CommandSent(unit, Command::Heal, true)) continue;
		if (CommandSent(unit, Command::Support)) continue;

		const AITargetInfo *target = FindHealPosition(state, unit);
		if (!target)
		{
			if (state == AIUnit::RSCritical && !IsAnyPlayerGroup())
			{
				Assert(_center);
				if (_center->GetRadio().Done() && _center->CanSupport(ATHeal) && !_center->WaitingForSupport(this, ATHeal))
				{
					_center->GetRadio().Transmit
					(
						new RadioMessageSupportAsk(this, ATHeal),
						_center->GetLanguage()
					);
				}
			}
			continue;
		}

		bool channelCenter = false;
		EntityAI *veh = target->_idExact;
		if (veh)
		{
			AIUnit *unit = veh->CommanderUnit();
			if (unit && unit->GetLifeState() == AIUnit::LSAlive)
				channelCenter = unit->GetGroup() != this;
		}

		Command cmd;
		cmd._message = Command::Heal;
		cmd._destination = target->_realPos;
		cmd._target = veh;
		cmd._time = Glob.time + COMMAND_TIMEOUT;
		SendAutoCommandToUnit(cmd, unit, true, channelCenter);
	}

	return ok;
}

#define AMMO_NEAR								100.0f
#define AMMO_FAR								500.0f

const AITargetInfo *AIGroup::FindRearmPosition(AIUnit::ResourceState state) const
{
	if (state == AIUnit::RSNormal) return NULL;
	Assert(Leader());
	
	// find ammo in close range
	const AITargetInfo *depot = NULL;
	float dist2Depot = FLT_MAX;
	const AITargetInfo *truck = NULL;
	float dist2Truck = FLT_MAX;

	for (int i=0; i<GetCenter()->NTargets(); i++)
	{
		const AITargetInfo &info = GetCenter()->GetTarget(i);
		if (info._destroyed)
			continue;
		if (!(info._side == TCivilian) && !(GetCenter()->IsFriendly(info._side)))
			continue;
		if (info._type->GetMaxAmmoCargo() <= 0)
			continue;
		VehicleSupply *veh = dyn_cast<VehicleSupply, Object>(info._idExact);
		if (!veh) continue;
		if (veh->GetAmmoCargo() <= 0) continue;
		if( GetCenter()->GetExposurePessimistic(info._realPos)>0 ) continue;
		float dist2 = (Leader()->Position() - info._realPos).SquareSizeXZ();

		if (info._type->IsKindOf(GLOB_WORLD->Preloaded(VTypeStatic)))
		{
			if (dist2 < dist2Depot)
			{
				dist2Depot = dist2;
				depot = &info;
			}
		}
		else
		{
			if (dist2 < dist2Truck)
			{
				dist2Truck = dist2;
				truck = &info;
			}
		}
	}
	if (depot && dist2Depot < Square(AMMO_NEAR))
	{
		// rearm in near depot
		return depot;
	}
	if (truck && dist2Truck < Square(AMMO_NEAR))
	{
		// rearm in near truck
		return truck;
	}

	if (state == AIUnit::RSLow) return NULL;

	// find ammo in wider range
	if (depot && dist2Depot < Square(AMMO_FAR))
	{
		// rearm in far depot
		return depot;
	}
	if (truck && dist2Truck < Square(AMMO_FAR))
	{
		// rearm in far truck
		return truck;
	}

	// ammo not found
	return NULL;
}

bool AIGroup::FindRearmPosition(Command &cmd) const
{
	const AITargetInfo *target = FindRearmPosition(AIUnit::RSCritical);
	if (!target) return false;

	cmd._destination = target->_realPos;
	cmd._target = target->_idExact;
	cmd._time = Glob.time + COMMAND_TIMEOUT;
	return true;
}

bool AIGroup::CheckAmmo()
{
	Assert(Leader());
	if (IsAnyPlayerGroup())
		return true;

	bool ok = true;
	AIUnit::ResourceState stateMax = AIUnit::RSNormal;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = _units[i];
		if (!unit) continue;
		AIUnit::ResourceState state = GetAmmoStateReported(unit);
		if (state == AIUnit::RSNormal) continue;
		ok = false;

		if (unit->GetCombatMode() == CMCareless) continue;
		// check both group and center radio channel
		if (CommandSent(unit, Command::Rearm, false)) continue;
		if (CommandSent(unit, Command::Rearm, true)) continue;
		if (CommandSent(unit, Command::Support)) continue;
		unit->CheckAmmo();
		if (state > stateMax) stateMax = state;
	}


	const AITargetInfo *target = FindRearmPosition(stateMax);
	if (target)
	{
		bool channelCenter = false;
		EntityAI *veh = target->_idExact;
		if (veh)
		{
			AIUnit *unit = veh->CommanderUnit();
			if (unit && unit->GetLifeState() == AIUnit::LSAlive)
				channelCenter = unit->GetGroup() != this;
		}

		Command cmd;
		cmd._message = Command::Rearm;
		cmd._destination = target->_realPos;
		cmd._target = veh;
		cmd._time = Glob.time + COMMAND_TIMEOUT;

		for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
		{
			AIUnit *unit = _units[i];
			if (!unit) continue;
			if (unit->GetCombatMode() == CMCareless) continue;
			// check both group and center radio channel
			if (CommandSent(unit, Command::Rearm, false)) continue;
			if (CommandSent(unit, Command::Rearm, true)) continue;
			if (CommandSent(unit, Command::Support)) continue;

			if (GetAmmoStateReported(unit) >= stateMax)
			{
				SendAutoCommandToUnit(cmd, unit, true, channelCenter);
			}
		}
	}
	else
	{
		if (stateMax == AIUnit::RSCritical && !IsAnyPlayerGroup())
		{
			Assert(_center);
			if (_center->GetRadio().Done() && _center->CanSupport(ATRearm) && !_center->WaitingForSupport(this, ATRearm))
			{
				_center->GetRadio().Transmit
				(
					new RadioMessageSupportAsk(this, ATRearm),
					_center->GetLanguage()
				);
			}
		}
	}

	return ok;
}

float AIGroup::GetCost() const
{
	// note: this function is obsolete and needs some polishing
	// if it should be used again
	float cost = 0;
	for (int u=0; u<MAX_UNITS_PER_GROUP; u++)
	{
		AIUnit *unit = UnitWithID(u + 1);
		if (!unit)
			continue;
		cost += unit->GetPerson()->GetType()->GetCost();
		if (!unit->IsSoldier())
		{	// driver
			cost += unit->GetVehicle()->GetType()->GetCost();
		}
	}
	for (int v=0; v<_vehicles.Size(); v++)
	{
		Transport *veh = _vehicles[v];
		if (!veh || veh->IsDammageDestroyed())
			continue;
		cost += veh->GetType()->GetCost();
	}
	return cost;
}

float AIGroup::EstimateTime(Vector3Val pos) const
{
	EntityAI *vehicle = NULL;
	for (int v=0; v<_vehicles.Size(); v++)
	{
		Transport *veh = _vehicles[v];
		if (!veh || veh->IsDammageDestroyed())
			continue;
		vehicle = veh;
		break;
	}
	if (!vehicle)
	{
		for (int u=0; u<MAX_UNITS_PER_GROUP; u++)
		{
			AIUnit *unit = UnitWithID(u + 1);
			if (!unit || !unit->IsUnit())
				continue;
			vehicle = unit->GetVehicle();
			break;
		}
	}
	Assert(vehicle);
	Assert(Leader());
	float dist = (pos - Leader()->Position()).SizeXZ();
	float time = dist / (0.5 * vehicle->GetType()->GetTypSpeed() * (1000.0 / 60.0)); // speed km/h -> m/min
	
	return time;
}

RString AIGroup::GetDebugName() const
{
	char buffer[256];
	if( GetCenter() )
	{
		sprintf(buffer, "%s %s", (const char *)GetCenter()->GetDebugName(), GetName());
	}
	else
	{
		sprintf(buffer, "%s %s", "new", GetName());
	}
	return buffer;
}

bool AIGroup::AssertValid() const
{
	bool result = true;
	if (NUnits() <= 0)
	{
		return true;	// its valid now
	}
	else if (!Leader())
	{
		Fail("no leader");
		result = false;
	}
	else if (Leader()->GetGroup() == NULL)
	{
		Fail("leader has no group");
		result = false;
	}
	else if (Leader()->GetGroup() != this)
	{
		Fail("leader from other group");
		result = false;
	}
	if (!MainSubgroup())
	{
		Fail("no main subgroup");
		result = false;
	}
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = _units[i];
		if (!unit) continue;
		if (unit->ID() != i + 1)
		{
			RptF
			(
				"Group %s, id %d, should be %d",
				(const char *)GetDebugName(),unit->ID(),i+1
			);
			Fail("unit with bad ID");
			result = false;
			// error recovery
			AIGroup *grp = const_cast<AIGroup *>(this);
			grp->_units[i] = NULL;
			continue;
		}
		if (unit->GetGroup() == NULL)
		{
			Fail("unit with no group");
			result = false;
		}
		else if (unit->GetGroup() != this)
		{
			Fail("unit with other group");
			result = false;
		}
	}
	for (int i=0; i<_subgroups.Size(); i++)
	{
		AISubgroup *subgrp = _subgroups[i];
		if (!subgrp) continue;
		if (subgrp->GetGroup() == NULL)
		{
			Fail("subgroup with no group");
			result = false;
		}
		else if (subgrp->GetGroup() != this)
		{
			Fail("subgroup with other group");
			result = false;
		}
		// check subgroup
		if (!subgrp->AssertValid()) result = false;
	}
	return result;
}

void AIGroup::Dump(int indent) const
{
}

///////////////////////////////////////////////////////////////////////////////
// class Mission

static const EnumName MissionActionNames[]=
{
	EnumName(Mission::NoMission, "NO MISSION"),
	EnumName(Mission::Arcade, "ARCADE"),
	EnumName(Mission::Arcade, "TRAINING"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(Mission::Action dummy)
{
	return MissionActionNames;
}

LSError Mission::Serialize(ParamArchive &ar)
{
	CHECK(ar.SerializeEnum("action", _action, 1, Mission::Arcade))
	CHECK(ar.SerializeRef("Target", _target, 1))
	CHECK(ar.Serialize("destination", _destination, 1))
	return LSOK;
}

void AIGroup::Init()
{
	Assert(_id > 0 && _id <= MaxGroups);
	if (_id <= 0 || _id > MaxGroups) return;

	int i = _id - 1;
	int names = (Pars >> "CfgWorlds" >> "GroupNameList" >> "letters").GetSize();
	int color = i / names;
	int name = i % names;

	_letterName = (Pars >> "CfgWorlds" >> "GroupNameList" >> "letters")[name];
	_colorName = (Pars >> "CfgWorlds" >> "GroupColorList" >> "colors")[color];
	const ParamEntry &clsName = Pars >> "CfgWorlds" >> "GroupNames" >> _letterName;
	const ParamEntry &clsColor = Pars >> "CfgWorlds" >> "GroupColors" >> _colorName;
	_name = (clsName >> "name").GetValue() + RString(" ") + (clsColor >> "name").GetValue();
}

void AIGroup::SetIdentity(RString name, RString color, RString picture)
{
	const ParamEntry *clsName = (Pars >> "CfgWorlds" >> "GroupNames").FindEntry(name);
	if (!clsName) return;
	const ParamEntry *clsColor = (Pars >> "CfgWorlds" >> "GroupColors").FindEntry(color);
	if (!clsColor) return;

	_letterName = name;
	_colorName = color;

	RString dnLetter = (*clsName) >> "name";
	RString dnColor =  (*clsColor) >> "name";
	RString fullname;
	if (dnColor.GetLength() > 0)
		fullname = dnLetter + RString(" ") + dnColor;
	else
		fullname = dnLetter;
	AICenter *center = GetCenter();
	if (center)
	{
		for (int i=0; i<center->NGroups(); i++)
		{
			AIGroup *grp = center->GetGroup(i);
			if (!grp) continue;
			if (grp->_name == fullname)
			{
				grp->_name = _name;
//				grp->_nameSign = _nameSign;
//				grp->_colorSign = _colorSign;
				break;
			}
		}
	}
/*
	_nameSign = GlobLoadTexture
	(
		GetPictureName((*clsName) >> "image")
	);
	_colorSign = GlobLoadTexture
	(
		GetPictureName((*clsColor) >> "image")
	);
*/
	_name = fullname;

	_pictureName = picture;
	_picture = NULL;
	if (_pictureName.GetLength() > 0) _picture = GlobLoadTexture(picture);
}

// structure
void AIGroup::RemoveFromCenter()
{
	if (!IsLocal()) return;

	AICenter *center = _center;
	if (center)
	{
		_center = NULL;
		center->GroupRemoved(this);
	}
}

void AIGroup::ForceRemoveFromCenter()
{
	AICenter *center = _center;
	if (center)
	{
		_center = NULL;
		center->GroupRemoved(this);
	}
}

// FSM virtuals
void AIGroup::DoUpdate()
{
	AIGroupContext context(this);
	Update(&context);
}

void AIGroup::DoRefresh()
{
	AIGroupContext context(this);
	UpdateAndRefresh(&context);
}

// communication with center
void AIGroup::ReceiveMission(Mission &mis)
{
#if LOG_COMM
	Log("Receive mission: Group %s: Mission %d at %.0f,%.0f", (const char *)GetDebugName(), mis._action, mis._destination.X(), mis._destination.Z());
#endif
	// repeat mission for members of my group
	/*
	Assert(Leader());
	GetRadio().Transmit
	(
		new RadioMessageMission(NULL, this, mis),
		GetCenter()->GetLanguage(),
		Leader()->GetSpeaker()
	);
	*/

	AIGroupContext context(this);
	base::Clear();
	PushTask(mis, &context);
//	Update(&context);
}

float AIGroup::ActualStrength() const
{
	float strength = 0;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = _units[i];
		if (!unit) continue;
		Person *person = unit->GetPerson();
		if (!person) continue;
		float armor = person->GetType()->GetArmor();
		float dammage = person->GetTotalDammage();
		armor *= (1.0 - dammage);
		strength += armor * InvSqrt(armor);
		Transport *veh = unit->GetVehicleIn();
		if (!veh) continue;
		if (veh->GetGroupAssigned() != this) // transporting vehicle
			continue;
		if (unit->IsUnit())
		{
			armor = veh->GetType()->GetArmor();
			dammage = veh->GetTotalDammage();
			armor *= (1.0 - dammage);
			strength += armor * InvSqrt(armor);
		}
	}
/*
	LogF
	(
		"%s: Actual strength %.3f",
		(const char *)GetDebugName(),
		strength
	);
*/
	return strength;
}

void AIGroup::CalculateMaximalStrength()
{
	_maxStrength = 0;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = _units[i];
		if (!unit) continue;
		float armor = unit->GetPerson()->GetType()->GetArmor();
		_maxStrength += armor * InvSqrt(armor);
		Transport *veh = unit->GetVehicleIn();
		if (!veh) continue;
		if (veh->GetGroupAssigned() != this) // transporting vehicle
			continue;
		if (unit->IsUnit())
		{
			armor = veh->GetType()->GetArmor();
			_maxStrength += armor * InvSqrt(armor);
		}
	}
/*
	LogF
	(
		"%s: New maximal strength %.3f",
		(const char *)GetDebugName(),
		_maxStrength
	);
*/
}

void AIGroup::CalculateCourage()
{
	if (_forceCourage >= 0)
	{
		_courage = _forceCourage;
		return;
	}
	if (!Leader()) return;
/*
	int rank = Leader()->GetPerson()->GetRank();
	if (rank<0 || rank>=NRanks)
	{
		ErrF("Patch: rank out of range (%d)",rank);
		return;
	}

	_courage = (Pars >> "CfgExperience" >> "courage")[rank];
	float coef = 0.9 + 0.2 * GRandGen.RandomValue();
	_courage *= coef;
*/
	_courage = Leader()->GetAbility();
}

extern void Flee(AIGroupContext *context);
extern void Unflee(AIGroupContext *context);

Vector3 FindFleePoint( AIGroup *group, bool &forced );

void AIGroup::Flee()
{
	bool forced;
	Vector3 pos=FindFleePoint(this,forced);
	if( !forced && pos.Distance2(Leader()->Position())<50 )
	{
		// if we've got nowhere to flee to, do not flee
		return;
	}
	_flee = true;
	if (!GetCurrent()) return;
	AIGroupContext context(this);
	context._task = GetCurrent()->_task;
	context._fsm = GetCurrent()->_fsm;
	::Flee(&context);
}

void AIGroup::Unflee()
{
	_flee = false;
	if (!GetCurrent()) return;
	AIGroupContext context(this);
	context._task = GetCurrent()->_task;
	context._fsm = GetCurrent()->_fsm;
	::Unflee(&context);
}


AIUnit::ResourceState AIGroup::GetHealthStateReported(AIUnit *unit)
{
	Assert(unit->GetGroup()==this);
	return _healthState[unit->ID()-1];
}

AIUnit::ResourceState AIGroup::GetAmmoStateReported(AIUnit *unit)
{
	Assert(unit->GetGroup()==this);
	return _ammoState[unit->ID()-1];
}

AIUnit::ResourceState AIGroup::GetFuelStateReported(AIUnit *unit)
{
	Assert(unit->GetGroup()==this);
	return _fuelState[unit->ID()-1];
}
AIUnit::ResourceState AIGroup::GetDammageStateReported(AIUnit *unit)
{
	Assert(unit->GetGroup()==this);
	return _dammageState[unit->ID()-1];
}
AIUnit::ResourceState AIGroup::GetWorstStateReported(AIUnit *unit)
{
	Assert(unit->GetGroup()==this);
	int i = unit->ID()-1;
	AIUnit::ResourceState state = _ammoState[i];
	if (state<_healthState[i]) state = _healthState[i];
	if (state<_ammoState[i]) state = _ammoState[i];
	if (state<_fuelState[i]) state = _fuelState[i];
	if (state<_dammageState[i]) state = _dammageState[i];
	return state;
}

void AIGroup::SetHealthStateReported(AIUnit *unit, AIUnit::ResourceState state)
{
	Assert(unit->GetGroup()==this);
	_healthState[unit->ID()-1] = state;
}
void AIGroup::SetAmmoStateReported(AIUnit *unit, AIUnit::ResourceState state)
{
	Assert(unit->GetGroup()==this);
	_ammoState[unit->ID()-1] = state;
}
void AIGroup::SetFuelStateReported(AIUnit *unit, AIUnit::ResourceState state)
{
	Assert(unit->GetGroup()==this);
	_fuelState[unit->ID()-1] = state;
}
void AIGroup::SetDammageStateReported(AIUnit *unit, AIUnit::ResourceState state)
{
	Assert(unit->GetGroup()==this);
	_dammageState[unit->ID()-1] = state;
}

bool AIGroup::GetReportedDown(AIUnit *unit)
{
	Assert(unit->GetGroup()==this);
	return _reportedDown[unit->ID()-1];
}

void AIGroup::SetReportedDown(AIUnit *unit, bool state)
{
	Assert(unit->GetGroup()==this);
	_reportedDown[unit->ID()-1] = state;
}

Time AIGroup::GetReportBeforeTime(AIUnit *unit)
{
	Assert(unit->GetGroup()==this);
	return _reportBeforeTime[unit->ID()-1];
}

void AIGroup::SetReportBeforeTime(AIUnit *unit, Time time)
{
	Assert(unit->GetGroup()==this);
	if (_reportedDown[unit->ID()-1]) return;
	Time &tgt = _reportBeforeTime[unit->ID()-1];
	if (tgt>time) tgt = time;
}
