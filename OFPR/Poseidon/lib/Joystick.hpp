/******************************************************************************
FILE: DXInput.h
******************************************************************************/

//#define DIRECTINPUT_VERSION		0x0800
#include <Es/Common/win.h>
#if defined _WIN32 && !defined _XBOX
#include <dinput.h>
#endif
#include <Es/Types/pointers.hpp>

class CImmMouse;
class CImmPeriodic;
//! Joystick support encapsulation


class Joystick
{
	public:
	enum
	{
		NAxes=8, // 6 regular and two sliders
		#ifdef _XBOX
		// all controls mapped to buttons, we need a lot of them
		NButtons=32
		#else
		NButtons=8
		#endif
	};

	private:
	SRef<CImmMouse> _iFeelMouse;
	SRef<CImmPeriodic> _iFeelEngine;
	SRef<CImmPeriodic> _iFeelWeapon;
	bool _iFeelEngineStarted;

	#ifndef _XBOX
	ComRef<IDirectInput8> _dInput; //  allow external DirectInput object
	ComRef<IDirectInputDevice8> _devJoystick;

	ComRef<IDirectInputEffect> _effCenter;
	ComRef<IDirectInputEffect> _effWeapon;
	ComRef<IDirectInputEffect> _effEngine;
	#else
	HANDLE _inputHandle;
	#endif

	int _nCenterAxes;

	bool _effEngineStarted;
	bool _effWeaponStarted;

	bool _weaponByConstantForce;
	bool _buttonState[NButtons];
	bool _ffEnabled;
	bool _ffOn;

	float _stiffnessX,_stiffnessY;
	float _engineMag,_engineFreq;

	void InitForceFeedback();
	void InitEffects();
	void InitIFeel(HINSTANCE  instance, HWND hwnd);
	
	public:
	#ifndef _XBOX
	Joystick
	(
		bool joystick, bool iFeelMouse,
		HINSTANCE  instance, HWND hwnd, IDirectInput8 *dInput=NULL
	);
	#else
	Joystick
	(
		bool joystick, bool iFeelMouse, HINSTANCE  instance, HWND hwnd
	);
	#endif
	~Joystick();

	// hat -- angle in degrees * 1000 (90000 , 180000 ...)
	bool Get
	(
		int axis[NAxes], unsigned char button[NButtons], bool buttonEdge[NButtons],
		int &hatAngle
	);

	// environment
	void DoSetStiffness(float valueX, float valueY); // how much autocentering is applied
	void DoSetEngine( float mag, float freq ); // set constant sine

	void SetStiffness(float valueX, float valueY); // how much autocentering is applied
	void SetEngine( float mag, float freq ); // set constant sine

	// used to simulate weapons
	void PlayRamp(float begMag, float endMag, float duration); // play ramp

	void PlayRampIFeel(float begMag, float endMag, float duration); // play ramp
	void DoSetEngineIFeel( float mag, float freq ); // set constant sine
	void DoStopEngineIFeel(); // set constant sine

	void FFStart();
	void FFStop();

	void FFOn();
	void FFOff();
		
	// DI callback 
	#ifndef _XBOX
	BOOL EnumJoy(LPCDIDEVICEINSTANCE lpddi, bool bEnumForce, HWND hwnd );
	#endif
};

//! global joystick instance
extern SRef<Joystick> JoystickDev;
