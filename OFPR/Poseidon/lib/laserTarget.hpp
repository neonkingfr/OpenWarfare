#ifdef _MSC_VER
#pragma once
#endif

#ifndef _LASER_TARGET_HPP
#define _LASER_TARGET_HPP

#include "vehicleAI.hpp"

//! virtual laser designated target type

class LaserTargetType: public EntityAIType
{
	friend class LaserTarget;

	typedef EntityAIType base;

	public:
	LaserTargetType( const ParamEntry *param );
	virtual void Load(const ParamEntry &par);
	virtual void InitShape(); // after shape is loaded
};

//! virtual laser designated target

class LaserTarget: public EntityAI
{
	typedef EntityAI base;
	Time _lastActivation; // time of last activation
	OLink<Object> _designatedTarget;

	void CheckDesignatedTarget(Matrix4Par pos);

	public:
	LaserTarget(EntityAIType *name, bool fullCreate=true);

	virtual bool IgnoreObstacle(Object *obstacle, ObjIntersect type) const;

	virtual void Init(Matrix4Par pos);
	virtual void Move(Matrix4Par transform);
	virtual void Move(Vector3Par position);

	void Simulate( float deltaT, SimulationImportance prec );

	void Draw( int level, ClipFlags clipFlags, const FrameBase &pos );
	void DrawDiags();

	float VisibleSize() const;
	float VisibleSizeRequired() const;
	Vector3 AimingPosition() const;
	virtual LSError Serialize(ParamArchive &ar);

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx);

	virtual bool QIsManual() const {return false;}

	USE_CASTING(base)
};

#endif

