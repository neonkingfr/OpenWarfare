#ifdef _MSC_VER
#pragma once
#endif

#ifndef _HELICOPTER_HPP
#define _HELICOPTER_HPP

#include "transport.hpp"
#include "indicator.hpp"
#include "shots.hpp"

#include "wounds.hpp"

class HelicopterType: public TransportType
{
	typedef TransportType base;
	friend class Helicopter;
	friend class HelicopterAuto;

	protected:
	Vector3 _pilotPos; // neutral neck and head positions

	Vector3 _missileLPos,_missileRPos;
	Vector3 _rocketLPos,_rocketRPos;

	Vector3 _gunPos,_gunDir; // to do: gun position and direction

	AnimationAnimatedTexture _animFire;
	TurretType _turret;

	AnimationRotation _rotorV; // animate geometry
	AnimationRotation _rotorH;

	AnimationSection _hRotorStill,_hRotorMove;
	AnimationSection _vRotorStill,_vRotorMove;

	Vector3 _mainRotorDiveAxis;
	Vector3 _backRotorDiveAxis;

	Indicator _altRadarIndicator, _altRadarIndicator2;
	Indicator _altBaroIndicator, _altBaroIndicator2;
	Indicator _speedIndicator, _speedIndicator2;
	Indicator _vertSpeedIndicator, _vertSpeedIndicator2;
	Indicator _rpmIndicator, _rpmIndicator2;
	Indicator _compass, _compass2;
	IndicatorWatch _watch, _watch2;
	AnimationWithCenter _vario, _vario2;
	Vector3 _varioDirection[MAX_LOD_LEVELS], _vario2Direction[MAX_LOD_LEVELS];
	Vector3 _hudPosition, _hudRight, _hudDown; 

	bool _enableSweep;

	HitPoint _hullHit;
	HitPoint _engineHit;
	HitPoint _rotorHHit,_rotorVHit;
	HitPoint _avionicsHit;
	HitPoint _missiles;
	HitPoint _glassRHit;
	HitPoint _glassLHit;

	WoundTextureSelections _glassDammageHalf;
	WoundTextureSelections _glassDammageFull;
 
	float _mainRotorSpeed;
	float _backRotorSpeed;

	// note: dive is used for Chinook.
	// Back rotor is actualy second main rotot in this case
	float _minMainRotorDive;
	float _minBackRotorDive;
	float _maxMainRotorDive;
	float _maxBackRotorDive;
	float _neutralMainRotorDive;
	float _neutralBackRotorDive;

	public:
	HelicopterType( const ParamEntry *param );
	virtual void Load(const ParamEntry &par);
	void InitShape();
};

class Helicopter: public Transport
{
	typedef Transport base;

	protected:
//	Ref<LightReflectorOnVehicle> _light;

	Ref<Shape> _cargoCockpit;

	bool _missileLRToggle:1;
	bool _rocketLRToggle:1;
		
	float _rndFrequency; // volume control - low down hero sound
	float _rotorSpeed,_rotorSpeedWanted; // turning motor on/off
	float _rotorPosition;

	// pilot can directly coordinate these parameters
	float _backRotor,_backRotorWanted;
	float _mainRotor,_mainRotorWanted; // main rotor thrust
	float _cyclicForward,_cyclicForwardWanted;
	float _cyclicAside,_cyclicAsideWanted; // main rotor thrust direction

	float _rotorDive,_rotorDiveWanted;

	Turret _turret;

	Vector3 _lastAngVelocity; // helper for prediction
	Vector3 _turbulence;
	Time _lastTurbulenceTime;

	WeaponLightSource _mGunFire;
	int _mGunFireFrames;
	UITime _mGunFireTime;
	int _mGunFirePhase;
//	WeaponFireSource _mGunFire;
	WeaponCloudsSource _mGunClouds;

	public:
	Helicopter( VehicleType *name, Person *pilot );
	~Helicopter();
	
	const HelicopterType *Type() const
	{
		return static_cast<const HelicopterType *>(GetType());
	}

	Matrix4 GunTransform() const;

	protected:
	float GetGlassBroken() const;

	void DammageAnimation( int level );
	void DammageDeanimation( int level );

	public:
	bool IsAnimated( int level ) const; // appearence changed with Animate
	bool IsAnimatedShadow( int level ) const; // shadow changed with Animate
	void Animate( int level );
	void Deanimate( int level );

	Vector3 AnimatePoint( int level, int index ) const;
	void AnimateMatrix(Matrix4 &mat, int level, int selection) const;

	void Draw( int level, ClipFlags clipFlags, const FrameBase &pos );

	//bool SelectTexture( int level, float rotorSpeed );
	//bool AnimateTexture( int level, bool shadow );
	//bool DeanimateTexture( int level, bool shadow );
	bool IsAbleToMove() const;
	bool IsPossibleToGetIn() const;

	void Simulate( float deltaT, SimulationImportance prec );
	void Sound( bool inside, float deltaT );
	void UnloadSound();

	float GetEngineVol( float &freq ) const;
	float GetEnvironVol( float &freq ) const;

	Texture *GetCursorTexture(Person *person);
	Texture *GetCursorAimTexture(Person *person);

	float Rigid() const {return 0.3;} // how much energy is transfered in collision

	bool IsVirtual( CameraType camType ) const {return true;}
	//bool IsContinuous( CameraType camType ) const {return true;}
	bool HasFlares( CameraType camType ) const;

	Matrix4 InsideCamera( CameraType camType ) const;
	int InsideLOD( CameraType camType ) const;
	void InitVirtual( CameraType camType, float &heading, float &dive, float &fov ) const;
	void LimitVirtual( CameraType camType, float &heading, float &dive, float &fov ) const;
	float TrackingSpeed() const {return 100;}

	float GetCombatHeight() const {return 30;}
	float GetMinCombatHeight() const {return 30;}
	float GetMaxCombatHeight() const {return 100;}

	virtual void DoTransform
	(
		TLVertexTable &dst,
		const Shape &src, const Matrix4 &posView,
		int from, int to
	) const;

	SimulationImportance WorstImportance() const {return SimulateVisibleFar;}
	SimulationImportance BestImportance() const {return SimulateCamera;}

	bool Airborne() const;

	bool HasHUD() const {return true;}

	LSError Serialize(ParamArchive &ar);
};

class HelicopterAuto: public Helicopter
{
	typedef Helicopter base;

	enum StopMode
	{
		SMNone,
		SMLand,
		SMGetIn,
		SMGetOut
	};

	protected:
	Vector3 _pilotSpeed;
	float _pilotHeading; // headind set by pilot
	float _pilotDive; // dive set by pilot

	float _forceDive; // dive necessary for aiming
	float _forceBank;
	//float _apDive,_apBank;
	float _pilotHeight;
	float _defPilotHeight; // set by scripting
	float _dirCompensate;  // how much we compensate for estimated change
	bool _avoidBankJitter;  // this should be off when autopilot is flying precisely
	// esp. when landing

	// helpers for keyboard pilot
	bool _pilotHeightHelper; // keyboard helper activated
	bool _pilotSpeedHelper; // keyboard helper activated
	bool _pilotDirHelper; // keyboard helper activated

	bool _hoveringAutopilot;

	bool _pressedForward,_pressedBack; // recognize fast speed-up, fast brake
	bool _pressedUp,_pressedDown;
	bool _pilotHeadingSet; // keep pilot heading
	bool _pilotDiveSet; // keep pilot dive

	Vector3 _stopPosition;
	StopMode _stopMode;

	// non-helper interface (joystrick)
	float _bankWanted,_diveWanted;

	AutopilotState _state;

	// basic helicopter tactics - perform sweep over target

	enum SweepState {SweepDisengage,SweepTurn,SweepEngage,SweepFire};
	SweepState _sweepState;
	Time _sweepDelay;
	LinkTarget _sweepTarget;
	Vector3 _sweepDir;
	
	public:
	HelicopterAuto( VehicleType *name, Person *pilot );

	void Simulate( float deltaT, SimulationImportance prec );
	void AvoidGround( float minHeight );

	void GetActions(UIActions &actions, AIUnit *unit, bool now);
	RString GetActionName(const UIAction &action);
	void PerformAction(const UIAction &action, AIUnit *unit);

	bool IsStopped() const;

	void FindStopPosition();

	void DammageCrew( EntityAI *killer, float howMuch,RString ammo );
	void Eject(AIUnit *unit);

	float MakeAirborne();
	void SetFlyingHeight(float val);

	void EngineOn();
	void EngineOff();
	void EngineOffAction();
	bool EngineIsOn() const;

	void StopRotor();

	bool FireWeapon( int weapon, TargetType *target );
	void FireWeaponEffects(int weapon, const Magazine *magazine,EntityAI *target);

	float FireValidTime() const {return 45;}

	void SuspendedPilot(AIUnit *unit, float deltaT );
	void KeyboardPilot(AIUnit *unit, float deltaT );
	void DetectControlMode() const;

	void JoystickDirPilot( float deltaT );
	void JoystickHeightPilot( float deltaT );
	void FakePilot( float deltaT );

	Vector3 GetStopPosition() const {return _stopPosition;}

	void Autopilot
	(
		float deltaT,
		Vector3Par target, Vector3Par tgtSpeed, // target
		Vector3Par direction, Vector3Par speed // wanted values
	);
	void ResetAutopilot();
	void BrakingManeuver();

	// AI interface
	bool AimWeapon(int weapon, Vector3Par direction );
	bool AimWeapon(int weapon, Target *target );
	bool AimObserver(Vector3Par direction);

	Vector3 GetWeaponPoint( int weapon ) const;
	Vector3 GetWeaponCenter( int weapon ) const;
	Vector3 GetWeaponDirection( int weapon ) const;
	Vector3 GetWeaponDirectionWanted( int weapon ) const;
	float GetAimed( int weapon, Target *target ) const;

	float GetFieldCost( const GeographyInfo &info ) const;
	float GetCost( const GeographyInfo &info ) const;
	float GetCostTurn( int difDir ) const;

	float FireInRange( int weapon, float &timeToAim, const Target &target ) const;
	float FireAngleInRange( int weapon, Vector3Par rel ) const;

	void MoveWeapons( float deltaT );

#if _ENABLE_AI
	void AIGunner(AIUnit *unit, float deltaT );
	void AIPilot(AIUnit *unit, float deltaT );
#endif

	void DrawDiags();
	RString DiagText() const;

	LSError Serialize(ParamArchive &ar);

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	static HelicopterAuto *CreateObject(NetworkMessageContext &ctx);
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx);
protected:
	void UpdateStopMode(AIUnit *unit);
};

#endif

