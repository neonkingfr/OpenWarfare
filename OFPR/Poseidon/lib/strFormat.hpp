#ifdef _MSC_VER
#pragma once
#endif

/*!
\file
Functions to help with RString formating
*/

#ifndef _STR_FORMAT_HPP
#define _STR_FORMAT_HPP

#include <Es/Strings/rString.hpp>

//! convert integer to string
RString FormatNumber(int number);

//! printf variant with RString output
RString Format(const char *format, ...);

#endif
