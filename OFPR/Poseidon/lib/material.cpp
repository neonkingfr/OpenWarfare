#include "wpch.hpp"
#include "material.hpp"
#include "paramFileExt.hpp"
#include "engine.hpp"

TexMaterial::TexMaterial()
:_emmisive(HBlack),
_ambient(HWhite),
_diffuse(HWhite),
_forcedDiffuse(HBlack),
_specular(HBlack),
_bumpmapFactor(0),
_detailmapFactor(0),
_specularPower(0)
{
}

template <class Type>
void LoadValue
(
	Type &dst, const ParamEntry &cfg, const char *name, const Type &defVal
)
{
	const ParamEntry *entry = cfg.FindEntry(name);
	if (!entry)
	{
		dst = defVal;
	}
	else
	{
		GetValue(dst,*entry);
	}
}

template <class Type>
Type LoadValue
(
	const ParamEntry &cfg, const char *name, const Type &defVal
)
{
	const ParamEntry *entry = cfg.FindEntry(name);
	if (!entry)
	{
		return defVal;
	}
	else
	{
		return *entry;
	}
}

TexMaterial::TexMaterial(const char *name)
{
	ParamFile cfg;
	const ParamEntry *entry = NULL;
	// load material from given file
	if (*name=='#')
	{
		// if first character is #, load from global material table
		entry = (Pars>>"CfgMaterials").FindEntry(name+1);
	}
	else if (cfg.ParseBinOrTxt(name))
	{
		entry = &cfg;
	}
	if (!entry)
	{
		_emmisive = HBlack;
		_ambient = HWhite;
		_diffuse = HWhite;
		_forcedDiffuse = HBlack;
		_specular = HBlack;
		_bumpmapFactor = 0;
		_detailmapFactor = 0;
		_specularPower=0;
		LogF("Material %s not load failed",name);
		return;
	}
	// load properties from file
	LoadValue(_emmisive,*entry,"emmisive",HBlack);
	LoadValue(_ambient,*entry,"ambient",HWhite);
	LoadValue(_diffuse,*entry,"diffuse",HWhite);
	LoadValue(_forcedDiffuse,*entry,"forcedDiffuse",HBlack);
	LoadValue(_specular,*entry,"specular",HBlack);

	_specularPower = LoadValue(*entry,"specularPower",0);

	_bumpmapFactor = LoadValue(*entry,"bumpmapFactor",1);
	_detailmapFactor = LoadValue(*entry,"detailmapFactor",1);

}


void TexMaterial::Combine(TLMaterial &dst, const TLMaterial &src)
{
	dst.ambient = src.ambient*_ambient;
	dst.diffuse = src.diffuse*_diffuse;
	dst.forcedDiffuse = src.forcedDiffuse*_forcedDiffuse;
	// note: combination of some properties is addition
	// those properties are zero for default material
	// and therefore it makes no sense multiplying them
	// note: src material has already acoomodation calculated in
	// this does not
	ColorVal accom = GEngine->GetAccomodateEye();
	dst.specular = src.specular+_specular*accom;
	dst.specularPower = (int)(src.specularPower+_specularPower);
	dst.emmisive = src.emmisive+_emmisive*accom;
	dst.specFlags = src.specFlags;
}

TexMaterialBank::TexMaterialBank()
{
}

TexMaterialBank::~TexMaterialBank()
{
}

TexMaterial *TexMaterialBank::TextureToMaterial(Texture *tex)
{
	if (!tex) return NULL;
	// check if given texture already exists
	int index = Find(tex->GetName());
	if (index>=0)
	{
		return Get(index);
	}
	// if it does not exist, try to find definition and load it
	if (!Pars.FindEntry("CfgMaterials"))
	{
		return NULL;
	}
	const ParamEntry &texmat = Pars>>"CfgTextureToMaterial";
	for (int i=0; i<texmat.GetEntryCount(); i++)
	{
		const ParamEntry &entry = texmat.GetEntry(i);
		if (!entry.IsClass()) continue;
		// check texture name
		//const RStringB &texname = entry>>"texture";
		const ParamEntry &texnames = entry>>"textures";
		const RStringB &matname = entry>>"material";

		for (int t=0; t<texnames.GetSize(); t++)
		{
			const RStringB &texname = texnames[t];
			if (!strcmpi(texname,tex->GetName()))
			{
				return New(matname);
			}
		}
	}

	return NULL;
}

//template Link<TexMaterial>;

TexMaterialBank GTexMaterialBank;

