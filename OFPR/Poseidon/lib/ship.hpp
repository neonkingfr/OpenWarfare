#ifdef _MSC_VER
#pragma once
#endif

#ifndef _SHIP_HPP
#define _SHIP_HPP

#include "tracks.hpp"
#include "gearBox.hpp"

#include "transport.hpp"
//#include "ai.hpp"
#include "shots.hpp"

#include <El/ParamFile/paramFile.hpp>

// every tank mainains a list of tracksteps

class ShipType: public TransportType
{
	typedef TransportType base;
	friend class Ship;
	friend class ShipWithAI;

	protected:
	Point3 _pilotPos; // camera position	
	Point3 _gunnerPilotPos; // camera position - gunner view
	Point3 _commanderPilotPos;

	Point3 _gunPos,_gunDir;
/*
	float _neutralGunXRot;

	float _minGunElev,_maxGunElev; // gun movement
	float _minTurretTurn,_maxTurretTurn;

	Point3 _turretYAxis; // rotate around this point
	Point3 _gunXAxis;
	Animation _turret,_gun;
*/

	AnimationAnimatedTexture _animFire;
	TurretType _turret;

	Matrix4 _toWheelAxis; // transformation
	AnimationWithCenter _drivingWheel;

	Matrix4 _toIndicatorSpeedAxis; // transformation
	AnimationWithCenter _indicatorSpeed;

	AnimationRotation _radar;

	HitPoint _turretHit,_gunHit;

	public:
	ShipType( const ParamEntry *param );
	virtual void Load(const ParamEntry &par);
	void InitShape();
};

//#include "fsm.hpp"

//! all ships (big and small)

class Ship: public Transport
{
	typedef Transport base;
	// basic catterpillar vehicle (tank/APC) simulation
	protected:
	Vector3 _lastAngVelocity;
	float _randFrequency;

	bool _pilotBrake:1;
	bool _targetOutOfAim:1;
/*
	bool _gunStabilized:1;

	float _servoVol;

	float _turretYRot,_turretYRotWanted;
	float _gunXRot,_gunXRotWanted;
	float _gunXSpeed,_turretYSpeed;
*/
	Turret _turret;

	WeaponLightSource _mGunFire;
	int _mGunFireFrames;
	UITime _mGunFireTime;
	int _mGunFirePhase;
//	WeaponFireSource _mGunFire;
	WeaponCloudsSource _mGunClouds;

	WaterSource _leftEngine,_rightEngine;
	WaterSource _leftWater,_rightWater;
		
	float _thrustLWanted,_thrustRWanted; // accelerate (left), accelerate (right)
	float _thrustL,_thrustR; // accelerate (left), accelerate (right)

	float _sink; // current sink status

	public:
	Ship( VehicleType *name, Person *driver );

	const ShipType *Type() const
	{
		return static_cast<const ShipType *>(GetType());
	}
	
	Vector3 DragFriction( Vector3Par speed );
	Vector3 DragForce( Vector3Par speed );

	void MoveWeapons(float deltaT);
	float Rigid() const {return 0.1;}

	void Simulate( float deltaT, SimulationImportance prec );

	float GetEngineVol( float &freq ) const;
	float GetEnvironVol( float &freq ) const;

	Matrix4 InsideCamera( CameraType camType ) const;
	int InsideLOD( CameraType camType ) const;
	bool IsVirtual( CameraType camType ) const;
	void InitVirtual
	(
		CameraType camType, float &heading, float &dive, float &fov
	) const;
	void LimitVirtual
	(
		CameraType camType, float &heading, float &dive, float &fov
	) const;

	float OutsideCameraDistance( CameraType camType ) const
	{
		return floatMax(20,GetShape()->GeometrySphere()*2);
	}
	Matrix4 TurretTransform() const;
	Matrix4 GunTurretTransform() const;

	void Draw( int level, ClipFlags clipFlags, const FrameBase &pos );

	bool AnimateTexture
	(
		int level,
		float phaseL, float phaseR, float speedL, float speedR
	);
	//bool AnimateTexture( int level, bool shadow );
	//bool DeanimateTexture( int level, bool shadow );
	bool IsAnimated( int level ) const; // appearence changed with Animate
	bool IsAnimatedShadow( int level ) const; // shadow changed with Animate
	void Animate( int level );
	void Deanimate( int level );

	Vector3 AnimatePoint( int level, int index ) const;
	void AnimateMatrix(Matrix4 &mat, int level, int selection) const;

	void Sound( bool inside, float deltaT );
	void UnloadSound();

	float TrackingSpeed() const {return 80;}

	// all tank weapons are linked
	// special case is guided missile, which is linked, but have weapon lock
	bool AimWeapon( int weapon, Vector3Par direction );
	bool AimWeapon( int weapon, Target *target );
	float GetAimed( int weapon, Target *target ) const;
	Vector3 GetWeaponDirection( int weapon ) const;
	Vector3 GetWeaponCenter( int weapon ) const;
	
	bool FireWeapon( int weapon, TargetType *target );
	void FireWeaponEffects(int weapon, const Magazine *magazine,EntityAI *target);

	void Eject(AIUnit *unit);

	Vector3 GetDriverGetInPos(Person *person, Vector3Par pos) const;
	Vector3 GetCommanderGetInPos(Person *person, Vector3Par pos) const;
	Vector3 GetGunnerGetInPos(Person *person, Vector3Par pos) const;
	Vector3 GetCargoGetInPos(Person *person, Vector3Par pos) const;
	Vector3 GetCargoGetOutPos(Person *person) const;

	void SuspendedPilot(AIUnit *unit, float deltaT );
	void KeyboardPilot(AIUnit *unit, float deltaT );
	void JoystickPilot( float deltaT );
	void FakePilot( float deltaT );
#if _ENABLE_AI
	void AIPilot(AIUnit *unit, float deltaT ){}
#endif

	bool HasHUD() const {return true;}

	virtual void ResetStatus();

	LSError Serialize(ParamArchive &ar);
};

class ShipWithAI: public Ship
{
	typedef Ship base;

public:
	enum StopState
	{
		SSNone,
		SSFindPath,
		SSMove,
		SSStop
	};

protected:
	Vector3 _stopPosition;
	StopState _stopState;

public:
	ShipWithAI( VehicleType *name, Person *driver );
	~ShipWithAI();

	float GetFieldCost( const GeographyInfo &info ) const;
	float GetCost( const GeographyInfo &info ) const;
	float GetCostTurn( int difDir ) const;
	float FireInRange( int weapon, float &timeToAim, const Target &target ) const;

	void Simulate( float deltaT, SimulationImportance prec );
#if _ENABLE_AI
	void AIGunner(AIUnit *unit, float deltaT );
	void AIPilot(AIUnit *unit, float deltaT );
#endif

	Vector3 GetStopPosition() const {return _stopPosition;}

	LSError Serialize(ParamArchive &ar);

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx);

protected:
	void FindStopPosition();
	void Autopilot(AIUnit *unit, float &speedWanted, float &headChange, float &turnPredict);
};

#endif
