#ifdef _MSC_VER
#pragma once
#endif

#ifndef _DISPLAY_UI_HPP
#define _DISPLAY_UI_HPP

#include "head.hpp"

#ifdef PREPROCESS_DOCUMENTATION
#include "networkObject.hpp"
#endif

#if _ENABLE_GAMESPY
#include "GameSpy/cengine/goaceng.h"
#endif

/*!
\file
Interface file for particular displays.
*/

//! Control for user controls configuration
class CKeys : public C3DListBox
{
protected:

	//! action to keys assignment
	AutoArray<int> _keys[UAN];
	//! collisions in definitions (if one key is assigned to several action)
	AutoArray<bool> _collisions[UAN];

	//! selected column
	int _mode;
	//! ignored user input (do not process key used for enter into column)
	int _ignoredKey;

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	CKeys(ControlsContainer *parent, int idc, const ParamEntry &cls);

	//! returns actual column
	int GetMode() const {return _mode;}
	//! avoid usage of ignored key
	void CheckIgnoredKey();
	
	//! returns whole key assignment
	const AutoArray<int> &GetKeys(int action) const {return _keys[action];}
	//! sets whole key assignment
	void SetKeys(int action, const AutoArray<int> &keys);
	//! removes the last assigned key from action
	void RemoveKey(int action);
	//! adds new key to action
	void AddKey(int action, int key);

	bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	bool OnKeyUp(unsigned nChar, unsigned nRepCnt, unsigned nFlags) {return true;}
	bool OnChar(unsigned nChar, unsigned nRepCnt, unsigned nFlags) {return true;}
	void OnLButtonDown(float x, float y);

	void DrawItem
	(
		Vector3Par position, Vector3Par down, int i, float alpha
	);
protected:
	//! updates collision array for given key
	void CheckCollisions(int key);
};

//! User control configuration display
class DisplayConfigure : public Display
{
protected:
	//! configuration listbox
	CKeys *_keys;
	//@{
	//! original value (restored when Cancel performed)
	bool _oldJoystickEnabled;
	bool _oldRevMouse;
	bool _oldMouseButtonsReversed;
	float _oldMouseSensitivityX;
	float _oldMouseSensitivityY;
	//@}

public:
	//! constructor
	/*!
		\param parent parent display
		\param enableSimulation enable game simulation
		- true if called form main menu
		- false in case of in game invocation
	*/
	DisplayConfigure(ControlsContainer *parent, bool enableSimulation);

	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	void OnSimulate(EntityAI *vehicle);
	void OnButtonClicked(int idc);
	void OnSliderPosChanged(int idc, float pos);

	void Destroy();
/*
	void InitParams();	// set defaults
	void LoadParams();
	void SaveParams();
*/
};

//! Select island display (notebook)
class DisplaySelectIsland : public Display
{
protected:
	//! wanted exit code (exit is performed after close animation is finished)
	int _exitWhenClose;

public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplaySelectIsland(ControlsContainer *parent)
		: Display(parent)
	{
		Load("RscDisplaySelectIsland");
		_exitWhenClose = -1;
#if !_ENABLE_EDITOR
		GetCtrl(IDC_SELECT_ISLAND_WIZARD)->ShowCtrl(false);
#endif
	}

	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	void OnLBDblClick(int idc, int curSel);
	void OnButtonClicked(int idc);
	void OnChildDestroyed(int idd, int exit);
	void OnCtrlClosed(int idc);
};

//! All missions (for designers) display
class DisplayCustomArcade : public Display
{
public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayCustomArcade(ControlsContainer *parent)
		: Display(parent)
	{
		Load("RscDisplayCustomArcade");
		InsertGames();
		ShowButtons();
	}

	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	void OnButtonClicked(int idc);
	void OnTreeSelChanged(int idc);
	void OnChildDestroyed(int idd, int exit);

	bool CanDestroy();

// implementation
protected:
	//! creates missions tree
	void InsertGames();
	//! updates buttons when selected item in tree changes
	void ShowButtons();
};

enum SortColumn
{
	SCServer,
	SCMission,
	SCState,
	SCPlayers,
	SCPing
};

//! Control for displaying info about sessions
class CSessions : public C3DListBox
{
friend class DisplayMultiplayer;
protected:
	//! session descriptions
	AutoArray<SessionInfo> _sessions;
	//! lock picture - shown when session is password protected
	Ref<Texture> _password;
	//! picture shown when session has incompatible version
	Ref<Texture> _version;
	//! no picture
	Ref<Texture> _none;

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	CSessions(ControlsContainer *parent, int idc, const ParamEntry &cls);

	int GetSize() {return _sessions.Size();}
	RString GetData(int i) {return _sessions[i].guid;}
	void DrawItem
	(
		Vector3Par position, Vector3Par down, int i, float alpha
	);

	void Sort(SortColumn column, bool ascending);
};

#if _ENABLE_GAMESPY
#define BROWSING_SOURCE_ENUM(type, prefix, XX) \
	XX(type, prefix, LAN) \
	XX(type, prefix, Internet) \
	XX(type, prefix, Remote)
#else
#define BROWSING_SOURCE_ENUM(type, prefix, XX) \
	XX(type, prefix, LAN) \
	XX(type, prefix, Remote)
#endif

DECLARE_ENUM(BrowsingSource, BS, BROWSING_SOURCE_ENUM)

struct SessionFilter : SerializeClass
{
	RString serverName;
	RString missionName;
	int maxPing;
	int minPlayers;
	int maxPlayers;
	bool fullServers;
	bool passwordedServers;

	SessionFilter()
	{
		maxPing = 0;
		minPlayers = 0;
		maxPlayers = 0;
		fullServers = true;
		passwordedServers = true;
	}
	LSError Serialize(ParamArchive &ar);
};

//! Base multiplayer display
/*!
	Shows available sessions on LAN or given remote computer.
	Enables join into running session or create new session.
*/
class DisplayMultiplayer : public Display
{
protected:
	//! IP address of computer for which sessions are displayed (empty for LAN)
	RString _ipAddress;
	//! port of computer for which sessions are displayed (on local network)
	int _portLocal;
	//! port of computer for which sessions are displayed (on remote server)
	int _portRemote;
	//! password for session
	/*!
		When Join is performed, password is used for access to selected session.
		When New is performed, password is used for protecting new session.
	*/
	RString _password;
	//! wanted exit code (exit is performed after close animation is finished)
	int _exitWhenClose;
	//! sessions info listbox
	CSessions *_sessions;

#if _ENABLE_GAMESPY
	//! GameSpy server list
	GServerList _serverList;
#endif

	//! which sessions are listed
	BrowsingSource _source;

	//! sort by
	SortColumn _sort;
	//! sort order
	bool _ascending;

	//! ascending sort picture name
	RString _sortUp; 
	//! descending sort picture name
	RString _sortDown;

	//! filter for sessions
	SessionFilter _filter;

	//! force update of sessions
	bool _refresh;

	//! update of sessions is running
	bool _refreshing;

public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayMultiplayer(ControlsContainer *parent);
	//! destructor
	~DisplayMultiplayer();

	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	void OnSimulate(EntityAI *vehicle);
	void OnButtonClicked(int idc);
	void OnLBDblClick(int idc, int curSel);
	void OnCtrlClosed(int idc);
	void OnChildDestroyed(int idd, int exit);

	//! update server list update progress
	void SetProgress(float progress);

protected:
	//! updates display (buttons, session list) when IP address changed
	void UpdateAddress(RString ipAddress, int port);
	//! updates display (buttons) when password changed
	void UpdatePassword(RString password);
	//! updates list of session (called in each simulation step)
	void UpdateSessions();
#if _ENABLE_GAMESPY
	//! updates list of session on internet (using GameSpy browser)
	void UpdateServerList();
#endif
	//! updates display (buttons) when MP implementation changed
	void UpdateSockets(bool sockets);
	//! returns current port
	int GetPort();
	//! sets current port
	void SetPort(int port);
	//! set session source
	void SetSource(BrowsingSource source);
	//! update sort icons
	void UpdateIcons();
	//! update filter titles
	void UpdateFilter();

	//! load options for display from user profile
	void LoadParams();
	//! save options for display to user profile
	void SaveParams();
};

//! Password display (enter password for multiplayer session)
class DisplayPassword : public Display
{
protected:
	//! actual password
	RString _password;

public:
	//! constructor
	/*!
		\param parent parent display
		\password current password
	*/
	DisplayPassword(ControlsContainer *parent, RString password)
		: Display(parent)
	{
		_enableSimulation = false;
		_password = password;
		Load("RscDisplayPassword");
	}
	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
};

//! Port display (enter port for multiplayer session)
class DisplayPort : public Display
{
protected:
	//! actual password
	int _port;

public:
	//! constructor
	/*!
		\param parent parent display
		\password current password
	*/
	DisplayPort(ControlsContainer *parent, int port)
		: Display(parent)
	{
		_enableSimulation = false;
		_port = port;
		Load("RscDisplayPort");
	}
	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
};

//! First server display
/*!
	Enables select (island and) multiplayer mission and sets difficulty.
*/
class DisplayServer : public Display
{
public:
	//! difficulty
	bool _cadetMode;

	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayServer(ControlsContainer *parent)
		: Display(parent)
	{
		_enableSimulation = false;
		Load("RscDisplayServer");
		UpdateMissions();

		_cadetMode = true;
		LoadParams();
		OnChangeDifficulty();
#if !_ENABLE_EDITOR
		GetCtrl(IDC_SERVER_EDITOR)->ShowCtrl(false);
#endif
	}

	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	void OnButtonClicked(int idc);
	void OnLBSelChanged(int idc, int curSel);
	void OnLBDblClick(int idc, int curSel);
	void OnChildDestroyed(int idd, int exit);

// implementation
protected:
	//! updates list of missions (for example if selected island changes)
	void UpdateMissions(RString filename = "");
	//! updates buttons and tooltip when mission changes
	void OnMissionChanged(int sel);
	//! sets selected mission as current mission (in framework)
	/*!
		\param editor editor will be performed
	*/
	bool SetMission(bool editor);

	//! updates display (buttons) when difficulty changed
	void OnChangeDifficulty();

	//! load options for display from user profile
	void LoadParams();
	//! save options for display to user profile
	void SaveParams();
};

//! Gamemaster select mission display
/*!
	Enables select (island and) multiplayer mission and sets difficulty on remote dedicated server.
*/
class DisplayRemoteMissions : public Display
{
public:
	//! difficulty
	bool _cadetMode;

	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayRemoteMissions(ControlsContainer *parent)
		: Display(parent)
	{
		_enableSimulation = false;
		Load("RscDisplayRemoteMissions");
		UpdateMissions();

		_cadetMode = false;
		OnChangeDifficulty();
		GetCtrl(IDC_SERVER_EDITOR)->ShowCtrl(false);
	}

	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	void OnButtonClicked(int idc);
	void OnLBSelChanged(int idc, int curSel);
	void OnLBDblClick(int idc, int curSel);
	void OnSimulate(EntityAI *vehicle);

// implementation
protected:
	//! updates list of missions (for example if selected island changes)
	void UpdateMissions(RString filename = "");
	//! updates buttons and tooltip when mission changes
	void OnMissionChanged(int sel);

	//! updates display (buttons) when difficulty changed
	void OnChangeDifficulty();
};

//! IP Address display (enter ip address for session list)
class DisplayIPAddress : public Display
{
public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayIPAddress(ControlsContainer *parent)
		: Display(parent)
	{
		_enableSimulation = false;
		Load("RscDisplayIPAddress");
	}
	bool CanDestroy();
	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
};

//! First multiplayer wizard display
/*!
	Enables select template and sets name of new mission.
	\patch 1.14 Date 08/09/2001 by Jirka
	- Added: multiplayer mission wizard
*/
#if _ENABLE_EDITOR

class DisplayWizardTemplate : public Display
{
public:
	//! selected island
	RString _world;
	//! multiplayer / singleplayer mission
	bool _multiplayer;

	//! constructor
	/*!
		\param parent parent display
		\param world selected island
		\param	multiplayer multiplayer / singleplayer mission
	*/
	DisplayWizardTemplate(ControlsContainer *parent, RString world, bool multiplayer);

	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	void OnLBSelChanged(int idc, int curSel);
	void OnLBDblClick(int idc, int curSel);
	void OnButtonClicked(int idc);
	void OnChildDestroyed(int idd, int exit);

// implementation
protected:
	//! updates overview when template changes
	void OnTemplateChanged();
};
#endif

//! First client display
/*!
	Displays text "Wait for server". Checks if server is ready (mission is selected).
*/
class DisplayClient : public Display
{
public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayClient(ControlsContainer *parent)
		: Display(parent)
	{
		_enableSimulation = false;
		Load("RscDisplayClient");
		SetCursor(NULL);
	}

	void OnButtonClicked(int idc);
	void OnChildDestroyed(int idd, int exit);
	void OnSimulate(EntityAI *vehicle);
};

/*!
\patch 1.50 Date 4/8/2002 by Jirka
- Changed: Multiplayer assignment screens reworked 
*/

//! Multiplayer setup display (common functionality for both server and client)
class DisplayMultiplayerSetup : public Display
{
protected:
	//! currently selected player
	int _player;
	//! displayed message (shown instead of display)
	RString _message;
	//! message font
	Ref<Font> _messageFont;
	//! message font size
	float _messageSize;
	
	//! shown side
	TargetSide _side;

	//@{
	//! initialization state flag
	bool _init;
	bool _transferMission;
	bool _loadIsland;
	bool _play;
	//@}

	//! further connecting of clients is disabled
	bool _sessionLocked;
	
	//! all AI units are disabled
	bool _allDisabled;
	
	//! if some player is dragging
	bool _dragging;
	//! directplay id of dragged player
	int _dragPlayer;
	//! name of dragged player
	RString _dragName;
	//! font used for drag cursor
	Ref<Font> _dragFont;
	//! font size
	float _dragSize;
	//! font color
	PackedColor _dragColor;

	Ref<Texture> _none;
	Ref<Texture> _westUnlocked;
	Ref<Texture> _westLocked;
	Ref<Texture> _eastUnlocked;
	Ref<Texture> _eastLocked;
	Ref<Texture> _guerUnlocked;
	Ref<Texture> _guerLocked;
	Ref<Texture> _civlUnlocked;
	Ref<Texture> _civlLocked;
	
public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayMultiplayerSetup(ControlsContainer *parent);
	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);

	void OnButtonClicked(int idc);
	void OnLBSelChanged(int idc, int curSel);
	void OnChildDestroyed(int idd, int exit);
	void OnSimulate(EntityAI *vehicle);
	void OnDraw(EntityAI *vehicle, float alpha);

	void OnLBDrag(int idc, int curSel);
	void OnLBDragging(float x, float y);
	void OnLBDrop(float x, float y);

protected:
	//! initialize display with no values (called in constructor) 
	void Preinit();
	//! initialize display
	void Init();
	//! updates display (listboxes) - called in each simulation step
	void Update();
	//! checks if given player can be dragged
	bool CanDrag(int player);
};

// Server and client briefing

//! Mission briefing for server
/*!
	Shows list of players with ready / not ready state.
*/
class DisplayServerGetReady : public DisplayGetReady
{
	typedef DisplayGetReady base;
public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayServerGetReady(ControlsContainer *parent);
	void OnButtonClicked(int idc);
	void OnChildDestroyed(int idd, int exit);
	void Destroy();
	void OnSimulate(EntityAI *vehicle);
};

//! Mission briefing for server
/*!
	Enables send ready state.
*/
class DisplayClientGetReady : public DisplayGetReady
{
	typedef DisplayGetReady base;
public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayClientGetReady(ControlsContainer *parent);
	void Destroy();
	void OnButtonClicked(int idc);
	void OnSimulate(EntityAI *vehicle);
};

//! Multiplayer players display
/*!
	Shows list of all players.
	For selected player shows detailed info (from squad description site).
*/
class DisplayMPPlayers : public Display
{
protected:
	//@{
	//! colors of players in list
	PackedColor _color;
	PackedColor _colorWest;
	PackedColor _colorEast;
	PackedColor _colorCiv;
	PackedColor _colorRes;
	//@}

public:
	//! constructor
	/*!
		\param parent parent display
		\param init enable initialization (controls creation from template)
	*/
	DisplayMPPlayers(ControlsContainer *parent, bool init = true)
		: Display(parent)
	{
		if (init)
		{
			_enableSimulation = true;

			Load("RscDisplayMPPlayers");
			UpdatePlayers();
			UpdatePlayerInfo();
		}
		
		const ParamEntry &cls = Pars >> "CfgInGameUI" >> "MPTable";
		_color = GetPackedColor(cls >> "color");
		_colorWest = GetPackedColor(cls >> "colorWest");
		_colorEast = GetPackedColor(cls >> "colorEast");
		_colorCiv = GetPackedColor(cls >> "colorCiv");
		_colorRes = GetPackedColor(cls >> "colorRes");

		GInput.ChangeGameFocus(+1);
	}
	//! destructor
	~DisplayMPPlayers()
	{
		GInput.ChangeGameFocus(-1);
	}

	void OnButtonClicked(int idc);
	void OnLBSelChanged(int idc, int curSel);
	void OnSimulate(EntityAI *vehicle);

protected:
	//! update list of players (called in each simulation step)
	void UpdatePlayers();
	//! update detailed player info (called if selected player changes)
	void UpdatePlayerInfo();
};

//! Client wait display
/*!
	Shown if player cannot join into current session.
	Shows list of all players.
	For selected player shows detailed info (from squad description site).
*/
class DisplayClientWait : public DisplayMPPlayers
{
public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayClientWait(ControlsContainer *parent)
		: DisplayMPPlayers(parent, false)
	{
		_enableSimulation = false;

		Load("RscDisplayClientWait");
		UpdatePlayers();
		UpdatePlayerInfo();
	}

	void OnButtonClicked(int idc);
	void OnSimulate(EntityAI *vehicle);
};

//! Login display (used for selection of current user)
class DisplayLogin : public Display
{
protected:
	//! wanted exit code (exit is performed after close animation is finished)
	int _exitWhenClose;

public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayLogin(ControlsContainer *parent)
		: Display(parent)
	{
		Load("RscDisplayLogin");
	}

	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	void OnButtonClicked(int idc);
	void OnCtrlClosed(int idc);
	void OnChildDestroyed(int idd, int exit);

	bool CanDestroy();
};

//! Control for preview of selected face
class CHead : public ControlObject
{
protected:
	Ref<LODShapeWithShadow> _manShape;
	Ref<LODShapeWithShadow> _womanShape;

	//! head types (shape, animations etc.)
	SRef<HeadType> _manHeadType;
	SRef<HeadType> _womanHeadType;

	//! head objects (concrete face, mimics etc.)
	SRef<Head> _manHead;
	SRef<Head> _womanHead;

	//! time of last simulation step
	UITime _lastSimulation;

	//! use woman / man head
	bool _woman;

public:
	//! constructor
	/*!
		Used when control is created by resource template.
		\param parent control container by which this control is owned
		\param idc id of control
		\param cls resource template
	*/
	CHead(ControlsContainer *parent, int idc, const ParamEntry &cls);
	void Animate(int level);
	void Deanimate(int level);
	void OnDraw(float alpha);
	//! single simulation step
	void Simulate();
	//! changes face
	void SetFace(RString name);
	//! changes glasses
	void SetGlasses(RString name);
	//! attaches lip info to head object
	void AttachWave(AbstractWave *wave, float freq = 1);
	//! Return head object
	Head *GetHead() {return _woman ? _womanHead : _manHead;}
	//! Return head type
	HeadType *GetHeadType() {return _woman ? _womanHeadType : _manHeadType;}
	//! Return head shape
	LODShapeWithShadow *GetHeadShape() {return _woman ? _womanShape.GetRef() : _manShape.GetRef();}
};

//! User properties dialog
/*!
	Enables change basic parameters of player (name, face, voice etc.).
*/
class DisplayNewUser : public Display
{
public:
	//! initial player's name
	RString _name;
	//! if edit existing user or create new
	bool _edit;

protected:
	//! if voice preview will be performed
	bool _doPreview;

	//! wanted exit code (exit is performed after close animation is finished)
	int _exitWhenClose;

	//! face name
	RString _face;
	//! glasses name
	RString _glasses;
	//! speaker name
	RString _speaker;
	//! squad id
	RString _squad;
	//! pitch of voice
	float _pitch;

	//! time when preview will start (if no additional user changes will be performed)
	DWORD _previewTime;
	//! speaker name to preview
	RString _previewSpeaker;
	//! voice pitch to preview
	float _previewPitch;
	//! preview sound
	Ref<AbstractWave> _previewSpeech;

	//! face preview control
	Ref<CHead> _head;

public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayNewUser(ControlsContainer *parent, RString name, bool edit);

	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	ControlObject *OnCreateObject(int type, int idc, const ParamEntry &cls);
	void OnButtonClicked(int idc);
	void OnSliderPosChanged(int idc, float pos);
	void OnLBSelChanged(int idc, int curSel);
	void OnObjectMoved(int idc, Vector3Par offset);
	void OnCtrlClosed(int idc);
	void OnSimulate(EntityAI *vehicle);
	bool CanDestroy();

protected:
	//! starts voice preview
	void Preview(RString speaker, float pitch);
};

//! Display performed when player was killed
class DisplayMissionEnd : public Display
{
public:
	//! quotation index
	int _quotation;

public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayMissionEnd(ControlsContainer *parent/*, bool editor = false*/);
	//! destructor
	~DisplayMissionEnd();
	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);

	void OnButtonClicked(int idc);
};


// Mission, intro, outro, campaign intro displays

//! "Fake" display used when outro cutscene is performed
/*!
	No display. Game is running "in background". Waits for user input.
*/
class DisplayOutro : public DisplayCutscene
{
public:
	//! end of game index (used for outro selection)
	EndMode _mode;

public:
	//! constructor
	/*!
		\param parent parent display
		\param mode end of game index (used for outro selection)
	*/
	DisplayOutro(ControlsContainer *parent, EndMode mode);
};

//! "Fake" display used when award / penalty cutscene is performed
/*!
	No display. Game is running "in background". Waits for user input.
*/
class DisplayAward : public DisplayCutscene
{
public:
	//! constructor
	/*!
		\param parent parent display
		\param cutscene name of cutscene
	*/
	DisplayAward(ControlsContainer *parent, RString cutscene);
};

//! "Fake" display used when battle intro cutscene is performed
/*!
	No display. Game is running "in background". Waits for user input.
*/
class DisplayCampaignIntro : public DisplayCutscene
{
public:
	//! constructor
	/*!
		\param parent parent display
		\param cutscene name of cutscene
	*/
	DisplayCampaignIntro(ControlsContainer *parent, RString cutscene);
};

//! Interrupt display (performed if mission is interruptet by user (ESCAPE key))
class DisplayInterrupt : public Display
{
public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayInterrupt(ControlsContainer *parent);
	//! destructor
	~DisplayInterrupt();

	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	void OnButtonClicked(int idc);
	void OnChildDestroyed(int idd, int exit);
};

//! Main menu display
class DisplayMain : public Display
{
protected:
	//! end of current mission index
	EndMode _end;
	//! application version (read from windows resource)
	RString _version;

public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayMain(ControlsContainer *parent);

	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	void OnButtonClicked(int idc);
	void OnChildDestroyed(int idd, int exit);
	void OnDraw(EntityAI *vehicle, float alpha);

	void DestroyHUD(int exit);

protected:
//	void LoadHeader();
};

#if _ENABLE_CHEATS
//! Debug console display
class DisplayDebug : public Display
{
protected:
	//! index of expression in expressions history list
	int _current;
	//! gamestate this console is working with
	GameState *_gs;

public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayDebug(ControlsContainer *parent, GameState *gs, const char *cls = "RscDisplayDebug" );
	//! destructor
	~DisplayDebug();
	Control *OnCreateCtrl(int type, int idc, const ParamEntry &cls);
	void OnButtonClicked(int idc);
	bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	void OnDraw(EntityAI *vehicle, float alpha);

	void DestroyHUD(int exit);

protected:
	//! updates log list
	void UpdateLog(CListBox *lbox);
};
#endif

RString FindPicture(RString name);
RString GetCampaignDirectory(RString campaign);
RString GetUserDirectory();
RString GetMissionDirectory();
RString GetSaveDirectory();

RString GetBaseDirectory();
RString GetBaseSubdirectory();
void SetBaseDirectory(RString dir);
void SetMission(RString world, RString mission);

bool ProcessTemplateName(RString name);
bool ProcessTemplateName(RString name, RString dir);
bool ParseMission(bool multiplayer);
bool ParseIntro();
bool ParseCutscene(RString name, bool multiplayer);

void RunInitScript();

extern bool ContinueSaved;

#endif

