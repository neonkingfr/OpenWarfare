#ifdef _MSC_VER
#pragma once
#endif

#ifndef _GEAR_BOX_HPP
#define _GEAR_BOX_HPP

#include <Es/Containers/array.hpp>
#include "time.hpp"

class GearBox
{
	AutoArray<float> _gears;
	int _gear; // current gear (0..3)
	int _gearWanted; // current gear (0..3)
	Time _gearChangeTime; // when should be the change applied

	protected:
	void ChangeGearUp( int gear, float time=1.0 );
	void ChangeGearDown( int gear, float time=1.0 );

	public:
	GearBox();
	void SetGears( const AutoArray<float> &gears );
	bool Change( float speed );
	bool Neutral();
	bool Forward();
	bool Reverse();

	int Gear() const {return _gear;}
	float Ratio() const {return _gears[_gear];}
};


#endif
