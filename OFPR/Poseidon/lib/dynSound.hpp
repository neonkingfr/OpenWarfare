#ifdef _MSC_VER
#pragma once
#endif

#ifndef _DYN_SOUND_HPP
#define _DYN_SOUND_HPP

#include "paramFileExt.hpp"
#include "vehicle.hpp"

class SoundEntry: public SoundPars
{
	// inherited: sfxfile, vol, frq
	friend class DynSound;
	friend class DynSoundSource;
	friend class DynSoundObject;
	protected:
	float _probab;
	float _min_delay,_mid_delay,_max_delay;
};


TypeIsMovableZeroed(SoundEntry);

struct DynSoundName
{
	// MovesType is defined by pair: LODShapeWithShadow and MotionType
	RString name;
	RString location;
};

class DynSound: public RefCountWithLinks
{
	friend class DynSoundBank;

	RString _name;
	AutoArray<SoundEntry> _sounds;
	SoundEntry _emptySound;

	public:
	DynSound();
	DynSound( const char *name );
	void Load( const char *name );

	static SoundEntry LoadEntry( const ParamEntry &entry );

	const SoundEntry &SelectSound( float probab ) const;
	bool IsOneLoopingSound() const;
	const char *GetName() const {return _name;}

};

TypeIsMovable(DynSound);

#include <Es/Containers/bankArray.hpp>


template <>
struct BankTraits<DynSound>
{
	// default name is character
	typedef const char *NameType;
	// default name comparison
	static int CompareNames(const char *n1, const char *n2)
	{
		return strcmpi(n1,n2);
	}
	// store only links - this guarantees releasing when mission changes etc.
	typedef LinkArray<DynSound> ContainerType;
};

class DynSoundBank: public BankArray<DynSound>
{
};

extern DynSoundBank DynSounds;

struct TitlesItem
{
	Time time;
	RString text;
};
TypeIsMovableZeroed(TitlesItem);

class SoundObject: public RemoveOLinks
{
	protected:
	Ref<AbstractWave> _sound;
	OLink<Object> _object;
	OLink<AIUnit> _sender;
	RString _soundName;
	bool _hasObject;
	bool _looped;
	bool _paused;
	bool _waiting;
	bool _forceTitles;

	AutoArray <TitlesItem> _titles;
	int _index;

	Ref<Font> _titlesFont;
	float _titlesSize;

	float _maxTitlesDistance;
	float _speed;

	void LoadSound();
	void StartSound();
	
	public:
	SoundObject(RString name, Object *source, bool looped = false, float maxTitlesDistance = 100.0f, float speed = 1.0f);
	bool Simulate(float deltaT, SimulationImportance prec);

	void SimulateTitles();

	AbstractWave *GetWave() {return _sound;}
	void Pause(bool pause = true) {_paused = pause;}

	bool IsWaiting() const {return _waiting;}
	const AIUnit *GetSender() const {return _sender;}
};

class SoundOnVehicle : public Vehicle
{
protected:
	Ref<SoundObject> _sound;

public:
	SoundOnVehicle(const char *name, Object *source, float maxTitlesDistance = 100.0f, float speed = 1.0f);
	void Simulate( float deltaT, SimulationImportance prec );
	bool MustBeSaved() const {return false;}
	bool Invisible() const {return true;}
};

class DynSoundObject: public RefCount
{
	protected:
	Ref<DynSound> _dynSound;
	Link<AbstractWave> _sound;

	float _timeToLive; // how long current sound will be played

	public:
	DynSoundObject( const char *name );
	~DynSoundObject();

	void Simulate( Object *source, float deltaT, SimulationImportance prec );
	void StopSound();
};

class DynSoundSource: public Vehicle
{
	Ref<DynSoundObject> _dynSound;

	public:
	DynSoundSource( const char *name );
	~DynSoundSource();

	void Simulate( float deltaT, SimulationImportance prec );
	void StopSound();

	// no load/save
	bool MustBeSaved() const {return false;}
};

#endif
