// font implementation common to all engines
// (C) 1997, Suma
#include "wpch.hpp"

#include "engine.hpp"
#include "scene.hpp"
#include "font.hpp"
#include "textbank.hpp"
#include <El/QStream/QBStream.hpp>
#include "global.hpp"
#include "scene.hpp"
#include "camera.hpp"
#include <stdarg.h>

#include "mbcs.hpp"

Font::Font()
{
/*
	int i;
	for( i=0; i<NChar; i++ )
	{
		_width[i]=0;
		_height[i]=0;
		_wTex[i]=0;
		_hTex[i]=0;
	}
	_name[0]=0;
*/
	_nChars = 0;
	_langID = English;
}

Font::~Font()
{
	GLOB_ENGINE->FontDestroyed(this);
	DoDestruct();
}

void Font::DoDestruct()
{
	//int i;
	//for( i=0; i<NChar; i++ ) _charTextures[i]=NULL;
}

static int getiw( QIStream &f )
{
	int low=(unsigned char)f.get();
	int high=(unsigned char)f.get();
	return low|(high<<8);
}

/*!
\patch_internal 1.31 Date 11/21/2001 by Jirka
- Changed: number of characters in font is now variable
*/
void Font::Load( const char *name )
{
	//LogF("Load font request %s",(const char *)name);
	strcpy(_name,name);
	strlwr(_name);
	// load all char textures
	char fontName[256];
	// first check new font
	sprintf(fontName,"%s.fxy",_name);
	QIFStreamB f;
	f.AutoOpen(fontName);
	if (f.rest()>0)
	{
		_nChars = f.rest() / (6 * sizeof(short));
		_infos.Realloc(_nChars);
		_infos.Resize(_nChars);

		//LogF("Load font %s",(const char *)fontName);
		// new font - large font textures
		// read font information
		/*
		memset(_width,0,sizeof(_width));
		memset(_height,0,sizeof(_height));
		memset(_xTex,0,sizeof(_xTex));
		memset(_yTex,0,sizeof(_yTex));
		*/

		_maxHeight = 0;
		int maxW = 0;
		while (f.rest()>0)
		{
			int c = getiw(f); // get character code

			int setNum = getiw(f); // get set number
			int x = getiw(f);
			int y = getiw(f);
			int w = getiw(f);
			int h = getiw(f);

			// create a texture name
			char texName[256];
			sprintf(texName,"%s-%02d.paa",_name,setNum);

			CharInfo &info = _infos[c];

			info.nameTex = texName;
			info.xTex = x;
			info.yTex = y;
			info.width = w-1;
			info.height = h-1;
			//  big font textures
			int w2 = 1, h2 = 1;
			while( w2<w ) w2<<=1;
			while( h2<h ) h2<<=1;

			info.wTex = w2;
			info.hTex = h2;

			saturateMax(maxW,w-1);
			saturateMax(_maxHeight,h-1);

		}
		_infos[0].width=maxW*3/4;
	}
	else
	{
		_nChars = 256 - 32;
		_infos.Realloc(_nChars);
		_infos.Resize(_nChars);
		for (int i=0; i<_nChars; i++)
		{
			CharInfo &info = _infos[i];
			info.nameTex = "";
			info.xTex = 0;
			info.yTex = 0;
			info.width = 0;
			info.height = 0;
		}
	}
/*
	else
	{
		sprintf(fontName,"%s.fhw",_name);
		f.AutoOpen(fontName);

		if( f.rest()<sizeof(_width)+sizeof(_height) )
		{
			// special case - old font, short hw tables
			LogF("Very old font %s",fontName);
			int count=f.rest()/(sizeof(int)*2);
			memset(_width,0,sizeof(_width));
			memset(_height,0,sizeof(_height));
			f.read((char *)_width,sizeof(int)*count);
			f.read((char *)_height,sizeof(int)*count);
		}
		else
		{
			if (f.rest()<=0)
			{
				LogF("Missing font %s",fontName);
			}
			else
			{
				LogF("Old font %s",fontName);
				f.read((char *)_width,sizeof(_width));
				f.read((char *)_height,sizeof(_height));
			}
		}
		Assert( f.rest()==0 );
		int maxW=0;
		_maxHeight=0;
		for( int i=0; i<NChar; i++ )
		{
			int w2=1,h2=1;
			while( w2<_width[i] ) w2<<=1;
			while( h2<_height[i] ) h2<<=1;
			if( maxW<_width[i] ) maxW=_width[i];
			if( _maxHeight<_height[i] ) _maxHeight=_height[i];
			_wTex[i]=w2;
			_hTex[i]=h2;
			// TODO: big font textures
			_xTex[i]=0;
			_yTex[i]=0;

			// prepare texture name
			char charName[256];
			sprintf(charName,"%s_%02x.paa",Name(),i+StartChar);
			_nameTex[i]=charName;
		}
		_width[0]=maxW*3/4;
	}
*/
	//LogF("Font %s: h=%d",name,_maxHeight);
}

float Font::Height() const
{
	// normalized height (fraction of screen height)
	return _maxHeight*(1.0/600);
}




Font *FontCache::Load(FontID id)
{
	char lowName[256];
	strcpy(lowName,id.name),strlwr(lowName);
	int i;
	for( i=0; i<_fonts.Size(); i++ )
	{
		Font *font=_fonts[i];
		if( font && !strcmp(font->Name(),lowName) )
		{
			DoAssert(font->GetLangID() == id.langID);
			return font;
		}
	}
	// add new font
	Font *font=new Font;
	Assert( font );
	font->Load(lowName);
	font->SetLangID(id.langID);
	_fonts.Add(font);
	return font;
}


Font *Engine::LoadFont(FontID id)
{
	// search for loaded fonts
	return _fonts.Load(id);
}

Texture *FontCache::Load( Font *font, RStringB texName )
{
	// check font info
	// search cache
	for( int i=0; i<_lastChars.Size(); i++ )
	{
		CachedChar &item=_lastChars[i];
		Assert( item._font );
		if( item._font!=font ) continue;
		if( item._c!=texName ) continue;
		Texture *texture=item._texture;
		// move item forward
		CachedChar copy=item;
		_lastChars.Delete(i);
		_lastChars.Add(copy);
		return texture;
	}

	// check if letter exists
	Ref<Texture> texture;
	if( QIFStreamB::FileExist(texName) )
	{
		//Log("Load texture %c %s",c,charName);
		texture=GLOB_ENGINE->TextBank()->Load(texName);
		if (texture) texture->SetMipmapRange(0,0);
	}
	else
	{
		LogF("Missing file %s",(const char *)texName);
		texture=DefaultTexture;
	}
	//font->_charTextures[c]=texture;
	// not in cache - add
	CachedChar item;
	item._font=font;
	item._texture=texture;
	item._c=texName;
	const int maxChars=256;
	if( _lastChars.Size()>maxChars ) _lastChars.Delete(0);
	_lastChars.Add(item);
	return texture;
}

void FontCache::RemoveFont( Font *font )
{
	// TODO: dynamic font loading/unloading
}

void FontCache::Clear()
{
	_fonts.Clear();
	Assert( _fonts.Size()==0 );
	_lastChars.Clear();
}
