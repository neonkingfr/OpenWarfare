// AI - implementation of Units

#include "wpch.hpp"
#include "ai.hpp"
#include "aiRadio.hpp"
#include "global.hpp"
#include "scene.hpp"
#include "person.hpp"

#include "world.hpp"
#include "landscape.hpp"
//#include "loadStream.hpp"
#include "paramArchive.hpp"

#include <El/Common/randomGen.hpp>
#include <El/Common/perfLog.hpp>
#include "perfProf.hpp"

//#include "strIncl.hpp"
#include "stringtableExt.hpp"

#include "roads.hpp"
#include "operMap.hpp"

#include "house.hpp"
#include "network.hpp"
#include "move_actions.hpp"

#include "uiActions.hpp"

#include <El/Common/enumNames.hpp>

#include "cdapfncond.h"

/*
#if _CZECH
#include "roxxe.hpp"
#endif
*/

///////////////////////////////////////////////////////////////////////////////
// Parameters

#include "aiDefs.hpp"

// speed in nodes
#define SPEED_COEF	6.0F


// A* algoritm for searching the best path
int directions8[8][2] = {{0,-1},{-1,-1},{-1,0},{-1,1},{0,1},{1,1},{1,0},{1,-1}};
int directions20[20][3] =
{
	{0,-1,0},{-1,-2,1},{-1,-1,2},{-2,-1,3},
	{-1,0,4},{-2,1,5},{-1,1,6},{-1,2,7},
	{0,1,8},{1,2,9},{1,1,10},{2,1,11},
	{1,0,12},{2,-1,13},{1,-1,14},{1,-2,15},
	{0,-2,0},{-2,0,4},{0,2,8},{2,0,12}
};

// exposures
#define VALID_EXPOSURE_CHANGE			10.0F
#define	COEF_EXPOSURE							4e-6F		// coeficient for including exposure into cost

#define	DIAG_PLANNING							0

///////////////////////////////////////////////////////////////////////////////
// class AILocker

AILocker::AILocker() {} // avoid inlined construct/destruct
AILocker::~AILocker() {}

void AILocker::LockItem(int x, int z, bool soldier)
{
	if (x < 0 || x >= OperItemRange * LandRange || z < 0 || z >= OperItemRange * LandRange)
		return;

	int x0 = x / OperItemRange;
	int z0 = z / OperItemRange;
	int x1 = x - x0 * OperItemRange;
	int z1 = z - z0 * OperItemRange;
	LockField *fld = NULL;
	for (int i=0; i<_fields.Size(); i++)
		if (_fields[i]->_x == x0 && _fields[i]->_z == z0)
		{
			fld = _fields[i];
			break;
		}
	if (fld == NULL)
	// note: field may be locked twice with the same locker
	{
		fld = GLOB_LAND->LockingCache()->GetLockField(x0, z0);
		_fields.Add(fld);
	}
	fld->Lock(x1, z1, soldier, true);
}

void AILocker::UnlockItem(int x, int z, bool soldier)
{
	if (x < 0 || x >= OperItemRange * LandRange || z < 0 || z >= OperItemRange * LandRange)
		return;

	int x0 = x / OperItemRange;
	int z0 = z / OperItemRange;
	int x1 = x - x0 * OperItemRange;
	int z1 = z - z0 * OperItemRange;
	LockField *fld = NULL;
	for (int i=0; i<_fields.Size(); i++)
		if (_fields[i]->_x == x0 && _fields[i]->_z == z0)
		{
			fld = _fields[i];
			break;
		}
	if (fld == NULL)
	{
		Fail("Cannot unlock item that was not locked.");
		return;
	}
	fld->Lock(x1, z1, soldier, false);
}

void AILocker::LockPosition( Vector3Val pos, float radius, bool soldier, float size)
{
	int LockRange = toIntCeil(radius * InvOperItemGrid);
	if (LockRange < 0) LockRange = 0;

	int xx, x = toIntFloor(pos.X() * InvOperItemGrid);
	int zz, z = toIntFloor(pos.Z() * InvOperItemGrid);
	for( xx=-LockRange; xx<=LockRange; xx++ )
	for( zz=-LockRange; zz<=LockRange; zz++ )
	{
		float xr=(x+xx+0.5f)*OperItemGrid-pos.X();
		float zr=(z+zz+0.5f)*OperItemGrid-pos.Z();
		if ( xr*xr+zr*zr<radius*radius || xx==0 && zz==0 )
		{
			LockItem(x + xx, z + zz, true);
			//LogF("DoLock  %d %d,%d",soldier,x + xx, z + zz);
		}
	}

	Assert( GRoadNet );
	x = toIntFloor(pos.X() * InvLandGrid);
	z = toIntFloor(pos.Z() * InvLandGrid);
	// lock road item if vehicle is inside "boundingSphere" + size
	for (zz=z-1; zz<=z+1; zz++)
		for (xx=x-1; xx<=x+1; xx++)
		{
			if (!InRange(xx, zz)) continue;
			RoadList &roadList = GRoadNet->GetRoadList(xx, zz);
			int i;
			for (i=0; i<roadList.Size(); i++)
			{
				RoadLink *item = roadList[i];
				if (item->IsInside(pos, size))
				{
					item->Lock();
					_roads.Add(item);
				}
			}
		}

	if (soldier)
	{
		// this is not clear - pos is AimingPosition, we need position on surface
		Vector3 realPos = pos - Vector3(0, 1, 0);

		// lock position in house
		int xMin, xMax, zMin, zMax;
		ObjRadiusRectangle(xMin, xMax, zMin, zMax, pos, pos, 50);
		for (int x=xMin; x<=xMax; x++) for(int z=zMin; z<=zMax; z++)
		{
			const ObjectList &list = GLandscape->GetObjects(z, x);
			int n = list.Size();
			for (int i=0; i<n; i++)
			{
				Object *obj = list[i];

				IPaths *building = const_cast<IPaths *>(obj->GetIPaths());
				if (!building) continue;
				for (int j=0; j<building->NPos(); j++)
				{
					Vector3 dest = building->GetPosition(building->GetPos(j));
					float dist2 = dest.Distance2(realPos);
					if (dist2 <= Square(size))
					{
						building->Lock(j);
						int index = _buildings.Add();
						_buildings[index].house = const_cast<Object *>(building->GetObject());
						_buildings[index].index = j;
					}
				}
			}
		}
	}
}

void AILocker::UnlockPosition( Vector3Val pos, float radius, bool soldier)
{
	int LockRange = toIntCeil(radius * InvOperItemGrid);
	if (LockRange < 0) LockRange = 0;

	int x = toIntFloor(pos.X() * InvOperItemGrid);
	int z = toIntFloor(pos.Z() * InvOperItemGrid);
//Log("%s:Unlocking %d, %d", (const char *)GetDebugName(), x, z);
	for( int xx=-LockRange; xx<=LockRange; xx++ )
	for( int zz=-LockRange; zz<=LockRange; zz++ )
	{
		float xr=(x+xx+0.5f)*OperItemGrid-pos.X();
		float zr=(z+zz+0.5f)*OperItemGrid-pos.Z();
		if( xr*xr+zr*zr<radius*radius || xx==0 && zz==0 )
		{
			UnlockItem(x + xx, z + zz, true);
			//LogF("UnLock  %d %d,%d",soldier,x + xx, z + zz);
		}
	}
 	int i, n = _fields.Size();
	for (i=0; i<n; i++)
	{
		GLOB_LAND->LockingCache()->ReleaseLockField(_fields[i]->_x, _fields[i]->_z);
	}

	n = _roads.Size();
	for (i=0; i<n; i++)
		if (_roads[i]) _roads[i]->Unlock();

	if (soldier)
	{
		// unlock position in house
		for (int i=0; i<_buildings.Size(); i++)
		{
			IPaths *building = _buildings[i].house ? const_cast<IPaths *>(_buildings[i].house->GetIPaths()) : NULL;
			if (_buildings[i].house) building->Unlock(_buildings[i].index);
		}
	}

	// TODO: some smart deallocation scheme
	// avoid dealocation
	//_fields.Clear();
	//_roads.Clear();
	_fields.Resize(0);
	_roads.Resize(0);
	_buildings.Resize(0);
}


void AILocker::LockPositionMan( Vector3Val pos, float radius)
{
	int LockRange = toIntCeil(radius * InvOperItemGrid);
	if (LockRange < 0) LockRange = 0;

	int xx, x = toIntFloor(pos.X() * InvOperItemGrid);
	int zz, z = toIntFloor(pos.Z() * InvOperItemGrid);
	for( xx=-LockRange; xx<=LockRange; xx++ )
	for( zz=-LockRange; zz<=LockRange; zz++ )
	{
		float xr=(x+xx+0.5f)*OperItemGrid-pos.X();
		float zr=(z+zz+0.5f)*OperItemGrid-pos.Z();
		if ( xr*xr+zr*zr<radius*radius || xx==0 && zz==0 )
		{
			//LogF("DoLockM %d %d,%d",false,x + xx, z + zz);
			LockItem(x + xx, z + zz, false);
		}
	}
}


void AILocker::UnlockPositionMan( Vector3Val pos, float radius)
{
	int LockRange = toIntCeil(radius * InvOperItemGrid);
	if (LockRange < 0) LockRange = 0;

	int xx, x = toIntFloor(pos.X() * InvOperItemGrid);
	int zz, z = toIntFloor(pos.Z() * InvOperItemGrid);
	for( xx=-LockRange; xx<=LockRange; xx++ )
	for( zz=-LockRange; zz<=LockRange; zz++ )
	{
		float xr=(x+xx+0.5f)*OperItemGrid-pos.X();
		float zr=(z+zz+0.5f)*OperItemGrid-pos.Z();
		if ( xr*xr+zr*zr<radius*radius || xx==0 && zz==0 )
		{
			//LogF("UnLockM %d %d,%d",false,x + xx, z + zz);
			UnlockItem(x + xx, z + zz, false);
		}
	}
}



///////////////////////////////////////////////////////////////////////////////
// class AIUnit

const Vector3 VUndefined(0, 1e9, 0);

static float AIUnitGetFieldCost(int x, int z, void *param)
{
	if (!param) return SET_UNACCESSIBLE;
	AIUnit *unit = (AIUnit *)param;
	switch (unit->GetPlanningMode())
	{
	case AIUnit::DoNotPlan:
	case AIUnit::LeaderDirect:
	default:
		{
			Fail("planning mode");
			return SET_UNACCESSIBLE;
		}
	case AIUnit::LeaderPlanned:
		{
			AISubgroup *subgrp = unit->GetSubgroup();
			if (!subgrp) return SET_UNACCESSIBLE;
			return subgrp->GetFieldCost(x, z);
		}
	case AIUnit::FormationPlanned:
	case AIUnit::VehiclePlanned:
		{
			if (!InRange(x,z)) return SET_UNACCESSIBLE;
			
			// base cost
			EntityAI *veh = unit->GetVehicle();
			GeographyInfo geogr = GLandscape->GetGeography(x,z);
			float cost = veh->GetCost(geogr) * veh->GetFieldCost(geogr);
			cost *= LandGrid; // cost = time for distance == LandGrid
			
			// exposure
			float exposure = veh->CalculateExposure(x, z); // dammage (in $) per time cost
			return cost * (1.0 + COEF_EXPOSURE * exposure);
		}
	}
}

#pragma warning(disable:4355)

AIUnit::AIUnit( Person *vehicle )
#if _ENABLE_AI
	: _planner(CreateAIPathPlanner(AIUnitGetFieldCost,this))
#endif
{
	_person = vehicle;
	_inVehicle = NULL;
	_subgroup = NULL;
	
	_id = 0;
	SetSpeaker((Pars >> "CfgVoice" >> "voices")[0], 1.0);

	_captive = false;

	_playable = false;
	_lifeState = LSAlive;
	_disabledAI = 0;

	_path.SetOperIndex(1);
	_path.SetMaxIndex(1);
	_path.SetOnRoad(false);

	_expensiveThinkFrac = toIntFloor(GRandGen.RandomValue()*1000);
	// perform first expensive think as soon as possible
	_expensiveThinkTime = Glob.time;

	_delay = Time(0);
	_iter = 0;
	_isAway = false;

	_state = Wait;
	_mode = Normal;

	_lastFuelState = RSNormal;
	_lastHealthState = RSNormal;
	_lastArmorState = RSNormal;
	_lastAmmoState = RSNormal;

	_formPos = AI::PosInFormation;
	_formPosCoef = 0;

	_semaphore = SemaphoreYellow;
	_combatModeMajor = CMAware;
	_dangerUntil = Glob.time - 60.0f;

	_expPosition=VZero;

	_formationPos = VZero;
	_formationAngle = 0;

	_watchDir = VZero;
	_watchDirHead = VZero;
	_watchPos = VZero;
	_watchMode = WMNo;

	_getInAllowed = true;
	_getInOrdered = false;

	_nearestEnemyDist2 = 100;

	_housePos = -1;

	_wantedPosition = VUndefined;
	_plannedPosition = VUndefined;
	_planningMode = DoNotPlan;

	_completedReceived = false;

	_ability = _invAbility = 1.0;

	ClearStrategicPlan();
}

inline void AIUnit::ExpensiveThinkDone()
{
	// calculate time of next expensive think
	// find start of this second
	// advance to start of next second, advance to our frac
	_expensiveThinkTime = Glob.time.Floor().AddMs(1000+_expensiveThinkFrac);
	#if 0
	LogF
	(
		"%.3f: Think %s (%d)",
		Glob.time-Time(0),(const char *)GetDebugName(),_expensiveThinkFrac
	);
	#endif
}


AIUnit::~AIUnit()
{
	//DoAssert(GetLifeState() != LSDeadInRespawn);
	if (IsLocal())
	{
	    NetworkId ni = GetNetworkId();
	    GetNetworkManager().DeleteObject(ni);
	}
}

void AIUnit::Load(const ParamEntry &cls)
{
	Person *soldier = GetPerson();
	if (soldier)
	{
		AIUnitInfo &info = soldier->GetInfo();
		info._identityContext = cls.GetContext();
		info._name = cls >> "name";
		info._face = cls >> "face";
		info._glasses = cls >> "glasses";
		soldier->SetFace(info._face);
		soldier->SetGlasses(info._glasses);
		info._speaker = cls >> "speaker";
		info._pitch = cls >> "pitch";
		SetSpeaker(info._speaker, info._pitch);
	}
}

static const EnumName ResourceStateNames[]=
{
	EnumName(AIUnit::RSNormal, "NORMAL"),
	EnumName(AIUnit::RSLow, "LOW"),
	EnumName(AIUnit::RSCritical, "CRITICAL"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(AIUnit::ResourceState dummy)
{
	return ResourceStateNames;
}

static const EnumName UnitModeNames[]=
{
	EnumName(AIUnit::DirectNormal, "DIRECT NORMAL"),
	EnumName(AIUnit::DirectExact, "DIRECT EXACT"),
	EnumName(AIUnit::Normal, "NORMAL"),
	EnumName(AIUnit::Exact, "EXACT"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(AIUnit::Mode dummy)
{
	return UnitModeNames;
}

static const EnumName UnitPlanningModeNames[]=
{
	EnumName(AIUnit::DoNotPlan, "DoNotPlan"),
	EnumName(AIUnit::LeaderPlanned, "LEADER PLANNED"),
	EnumName(AIUnit::LeaderDirect, "LEADER DIRECT"),
	EnumName(AIUnit::FormationPlanned, "FORMATION PLANNED"),
	EnumName(AIUnit::VehiclePlanned, "VEHICLE PLANNED"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(AIUnit::PlanningMode dummy)
{
	return UnitPlanningModeNames;
}

static const EnumName UnitStateNames[]=
{
	EnumName(AIUnit::Wait, "WAIT"),
	EnumName(AIUnit::Init, "INIT"),
	EnumName(AIUnit::Busy, "BUSY"),
	EnumName(AIUnit::Completed, "OK"),
	EnumName(AIUnit::Delay, "DELAY"),
	EnumName(AIUnit::InCargo, "CARGO"),
	EnumName(AIUnit::Stopping, "STOPPING"),
	EnumName(AIUnit::Replan, "REPLAN"),
	EnumName(AIUnit::Stopped, "STOPPED"),
	EnumName(AIUnit::Sending, "SENDING"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(AIUnit::State dummy)
{
	return UnitStateNames;
}

static const EnumName UnitWatchModeNames[]=
{
	EnumName(AIUnit::WMNo, "NO"),
	EnumName(AIUnit::WMDir, "DIR"),
	EnumName(AIUnit::WMPos, "POS"),
	EnumName(AIUnit::WMTgt, "TGT"),
	EnumName(AIUnit::WMAround, "AROUND"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(AIUnit::WatchMode dummy)
{
	return UnitWatchModeNames;
}

static const EnumName LifeStateNames[]=
{
	EnumName(AIUnit::LSAlive, "ALIVE"),
	EnumName(AIUnit::LSDead, "DEAD"),
	EnumName(AIUnit::LSDeadInRespawn, "DEAD-RESPAWN"),
	EnumName(AIUnit::LSAsleep, "ASLEEP"),
	EnumName(AIUnit::LSUnconscious, "UNCONSCIOUS"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(AIUnit::LifeState dummy)
{
	return LifeStateNames;
}

static const EnumName DisabledAINames[]=
{
	EnumName(AIUnit::DATarget, "TARGET"),
	EnumName(AIUnit::DAAutoTarget, "AUTOTARGET"),
	EnumName(AIUnit::DAMove, "MOVE"),
	EnumName(AIUnit::DAAnim, "ANIM"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(AIUnit::DisabledAI dummy)
{
	return DisabledAINames;
}

LSError AIUnitInfo::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("identityContext", _identityContext, 1, RString()))
	CHECK(ar.Serialize("name", _name, 1))
	CHECK(ar.Serialize("experience", _experience, 1, 0))
	CHECK(ar.SerializeEnum("rank", _rank, 1, RankPrivate))
	CHECK(ar.Serialize("face", _face, 1, ""))
	CHECK(ar.Serialize("glasses", _glasses, 1, "None"))
	CHECK(ar.Serialize("speaker", _speaker, 1, ""))
	CHECK(ar.Serialize("pitch", _pitch, 1, 1.0))
	return LSOK;
}

LSError AIUnit::Serialize(ParamArchive &ar)
{
	// structure
	CHECK(ar.SerializeRef("Person", _person, 1))
	CHECK(ar.SerializeRef("InVehicle", _inVehicle, 1))
	CHECK(ar.SerializeRef("Subgroup", _subgroup, 1))

	// info
	CHECK(ar.Serialize("id", _id, 1))
//	CHECK(ar.Serialize("speaker", _speaker, 1, -1))
	if (ar.IsSaving())
	{
		if (_person)
			CHECK(ar.Serialize("Info", _person->GetInfo(), 1))
	}
	else if (ar.GetPass() == ParamArchive::PassSecond)
	{
		if (_person)
		{
			AIUnitInfo &info = _person->GetInfo();
			ar.FirstPass();
			CHECK(ar.Serialize("Info", info, 1))
			ar.SecondPass();
			CHECK(ar.Serialize("Info", info, 1))
			
			if (info._face.GetLength() > 0)
				_person->SetFace(info._face);
			
			if (info._glasses.GetLength() > 0)
				_person->SetGlasses(info._glasses);
			
			if (info._speaker.GetLength() > 0)
				SetSpeaker(info._speaker, info._pitch);
		}
	}
	CHECK(ar.Serialize("ability", _ability, 1, 0.2))
	if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassSecond)
	{
		_invAbility = 1.0 / _ability;
	}

	CHECK(ar.SerializeRef("VehicleAssigned", _vehicleAssigned, 1))
	CHECK(ar.SerializeEnum("semaphore", _semaphore, 1, SemaphoreYellow))
	CHECK(ar.SerializeEnum("combatModeMajor", _combatModeMajor, 1, CMAware))
	CHECK(ar.Serialize("dangerUntil", _dangerUntil, 1, Time(0)))
	CHECK(ar.Serialize("captive", _captive, 1, false))
	CHECK(ar.Serialize("isAway", _isAway, 1, false))

	CHECK(ar.SerializeEnum("lifeState", _lifeState, 1, LSAlive))
	CHECK(ar.SerializeEnum("lifeState", _lifeState, 1, LSAlive))
	_disabledAI = 0;

	CHECK(ar.SerializeRef("TargetAssigned", _targetAssigned, 1))
	CHECK(ar.SerializeRef("TargetEngage", _targetEngage, 1))
	CHECK(ar.SerializeRef("TargetEnableFire", _targetEnableFire, 1))

	CHECK(ar.Serialize("expensiveThinkFrac",_expensiveThinkFrac,1,0));
	CHECK(ar.Serialize("expensiveThinkTime",_expensiveThinkTime,1,Time(0)));

	CHECK(ar.Serialize("nearestEnemyDist2", _nearestEnemyDist2, 1))
	
	// actual instructions
	CHECK(ar.Serialize("Path", _path, 1))
	CHECK(ar.SerializeRef("House", _house, 1))
	CHECK(ar.Serialize("housePos", _housePos, 1, -1))

	CHECK(ar.Serialize("wantedPosition", _wantedPosition, 1, VUndefined))
	CHECK(ar.Serialize("plannedPosition", _plannedPosition, 1, VUndefined))
	CHECK(ar.SerializeEnum("planningMode", _planningMode, 1, DoNotPlan))
	CHECK(ar.Serialize("completedReceived", _completedReceived, 1, false))

	CHECK(ar.Serialize("formationAngle", _formationAngle, 1))
	CHECK(ar.Serialize("formationPos", _formationPos, 1))
	CHECK(ar.Serialize("watchDirection", _watchDir, 1, VZero))
	CHECK(ar.Serialize("watchDirectionHead", _watchDirHead, 1, VZero))
	CHECK(ar.Serialize("watchPosition", _watchPos, 1))
	CHECK(ar.SerializeRef("watchTarget", _watchTgt, 1))
	CHECK(ar.SerializeEnum("watchMode", _watchMode, 1))

	CHECK(ar.Serialize("expPosition", _expPosition, 1))

	CHECK(ar.SerializeEnum("state", _state, 1, Busy))
	CHECK(ar.SerializeEnum("mode", _mode, 1, Normal))

	// counter for trying of finding path
	CHECK(ar.Serialize("delay", _delay, 1))
	CHECK(ar.Serialize("iter", _iter, 1, 0))

	CHECK(ar.Serialize("getInAllowed", _getInAllowed, 1, true))
	CHECK(ar.Serialize("getInOrdered", _getInOrdered, 1, false))

	// messages
	CHECK(ar.SerializeEnum("lastFuelState", _lastFuelState, 1, RSNormal))
	CHECK(ar.SerializeEnum("lastHealthState", _lastHealthState, 1, RSNormal))
	CHECK(ar.SerializeEnum("lastArmorState", _lastArmorState, 1, RSNormal))
	CHECK(ar.SerializeEnum("lastAmmoState", _lastAmmoState, 1, RSNormal))

	CHECK(ar.Serialize("_fuelCriticalTime", _fuelCriticalTime, 1))
	CHECK(ar.Serialize("_healthCriticalTime", _healthCriticalTime, 1))
	CHECK(ar.Serialize("_dammageCriticalTime", _dammageCriticalTime, 1))
	CHECK(ar.Serialize("_ammoCriticalTime", _ammoCriticalTime, 1))

	// strategic plan
	if (ar.GetArVersion() >= 8)
	{
#if _ENABLE_AI
		CHECK(ar.Serialize("Planner", *_planner, 1))
		CHECK(ar.Serialize("completedTime", _completedTime, 1))	// not used - why ??
		CHECK(ar.Serialize("waitWithPlan", _waitWithPlan, 1))
		CHECK(ar.Serialize("attemptPlan", _attemptPlan, 1, 0))
#endif

		CHECK(ar.Serialize("lastPlan", _lastPlan, 1, false))
		CHECK(ar.Serialize("noPath", _noPath, 1, false))
		CHECK(ar.Serialize("updatePath", _updatePath, 1, false))

		CHECK(ar.Serialize("exposureChange", _exposureChange, 1, 0))
	}
	return LSOK;
}

AIUnit *AIUnit::LoadRef(ParamArchive &ar)
{
	TargetSide side = TSideUnknown;
	int idGroup;
	int id;
	if (ar.SerializeEnum("side", side, 1) != LSOK) return NULL;
	if (ar.Serialize("idGroup", idGroup, 1) != LSOK) return NULL;
	if (ar.Serialize("id", id, 1) != LSOK) return NULL;
	AICenter *center = NULL;
	switch (side)
	{
		case TWest:
			center = GWorld->GetWestCenter();
			break;
		case TEast:
			center = GWorld->GetEastCenter();
			break;
		case TGuerrila:
			center = GWorld->GetGuerrilaCenter();
			break;
		case TCivilian:
			center = GWorld->GetCivilianCenter();
			break;
		case TLogic:
			center = GWorld->GetLogicCenter();
			break;
	}
	if (!center) return NULL;
	AIGroup *group = NULL;
	for (int i=0; i<center->NGroups(); i++)
	{
		AIGroup *grp = center->GetGroup(i);
		if (grp && grp->ID() == idGroup)
		{
			group = grp;
			break; 
		}
	}
	if (!group) return NULL;
	return group->UnitWithID(id);
}

LSError AIUnit::SaveRef(ParamArchive &ar) const
{
	AIGroup *grp = GetGroup();
	AICenter *center = grp ? grp->GetCenter() : NULL;
	TargetSide side = center ? center->GetSide() : TSideUnknown;
	int idGroup = grp ? grp->ID() : -1;
	int id = ID();
	CHECK(ar.SerializeEnum("side", side, 1))
	CHECK(ar.Serialize("idGroup", idGroup, 1))
	CHECK(ar.Serialize("id", id, 1))
	return LSOK;
}

NetworkMessageType AIUnit::GetNMType(NetworkMessageClass cls) const
{
	switch (cls)
	{
	case NMCCreate:
		return NMTCreateAIUnit;
	case NMCUpdateGeneric:
		return NMTUpdateAIUnit;
	default:
		return NMTNone;
	}
}

IndicesCreateAIUnit::IndicesCreateAIUnit()
{
	person = -1;
	subgroup = -1;
	id = -1;
	name = -1;
	face = -1;
	glasses = -1;
	speaker = -1;
	pitch = -1;
	rank = -1;
	experience = -1;
	initExperience = -1;
	playable = -1;
	squadPicture = -1;
	squadTitle = -1;
}

void IndicesCreateAIUnit::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);

	SCAN(person)
	SCAN(subgroup)
	SCAN(id)
	SCAN(name)
	SCAN(face)
	SCAN(glasses)
	SCAN(speaker)
	SCAN(pitch)
	SCAN(rank)
	SCAN(experience)
	SCAN(initExperience)
	SCAN(playable)
	SCAN(squadPicture)
	SCAN(squadTitle)
}

//! Create network message indices for AIUnit class
NetworkMessageIndices *GetIndicesCreateAIUnit() {return new IndicesCreateAIUnit();}

//! network message indices for AIUnit class
/*!
\patch 1.11 Date 07/27/2001 by Jirka
- Improved: MP code
\patch_internal 1.10 Date 07/27/2001 by Jirka
- some information from AIUnit are not sent through network now (operative path etc.)
*/
class IndicesUpdateAIUnit : public IndicesNetworkObject
{
	typedef IndicesNetworkObject base;

public:
	//@{
	//! index of field in message format
	int person;
	int experience;
	int ability;
	int semaphore;
	int combatModeMajor;
	int dangerUntil;
	int captive;
	// TODO: int targetAssigned;
	// TODO: int targetAssignedValid;
//	IndicesPath *path;
	int house;
	int housePos;
	int wantedPosition;
//	int plannedPosition;
	int planningMode;
//	int completedReceived;
	int formationAngle;
	int formationPos;
//	int expPosition;
	int state;
	int mode;
	int lifeState;
	// ?? _delay
	int getInAllowed;
	int getInOrdered;
	// TODO: strategic plan
//	int lastPlan;
//	int noPath;
	//@}

	//! Constructor
	IndicesUpdateAIUnit();
	~IndicesUpdateAIUnit();
	NetworkMessageIndices *Clone() const {return new IndicesUpdateAIUnit;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesUpdateAIUnit::IndicesUpdateAIUnit()
{
	person = -1;
	experience = -1;
	ability = -1;
	semaphore = -1;
	combatModeMajor = -1;
	dangerUntil = -1;
	captive = -1;
	// TODO: targetAssigned = -1;
	// TODO: targetAssignedValid = -1;
//	IndicesPath *GetIndicesPath();
//	path = GetIndicesPath();
	house = -1;
	housePos = -1;
	wantedPosition = -1;
//	plannedPosition = -1;
	planningMode = -1;
//	completedReceived = -1;
	formationAngle = -1;
	formationPos = -1;
//	expPosition = -1;
	state = -1;
	mode = -1;
	lifeState = -1;
	// ?? _delay
	getInAllowed = -1;
	getInOrdered = -1;
	// TODO: strategic plan
//	lastPlan = -1;
//	noPath = -1;
}

IndicesUpdateAIUnit::~IndicesUpdateAIUnit()
{
/*
	void DeleteIndicesPath(IndicesPath *path);
	DeleteIndicesPath(path);
*/
}

void IndicesUpdateAIUnit::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);

	SCAN(person)
	SCAN(experience)
	SCAN(ability)
	SCAN(semaphore)
	SCAN(combatModeMajor)
	SCAN(dangerUntil)
	SCAN(captive)
	// TODO: SCAN(targetAssigned)
	// TODO: SCAN(targetAssignedValid)
//	void ScanIndicesPath(IndicesPath *path, NetworkMessageFormatBase *format);
//	ScanIndicesPath(path, format);
	SCAN(house)
	SCAN(housePos)
	SCAN(wantedPosition)
//	SCAN(plannedPosition)
	SCAN(planningMode)
//	SCAN(completedReceived)
	SCAN(formationAngle)
	SCAN(formationPos)
//	SCAN(expPosition)
	SCAN(state)
	SCAN(mode)
	SCAN(lifeState)
	// ?? _delay
	SCAN(getInAllowed)
	SCAN(getInOrdered)
	// TODO: strategic plan
//	SCAN(lastPlan)
//	SCAN(noPath)
}

//! Create network message indices for AIUnit class
NetworkMessageIndices *GetIndicesUpdateAIUnit() {return new IndicesUpdateAIUnit();}

NetworkMessageFormat &AIUnit::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	switch (cls)
	{
	case NMCCreate:
		NetworkObject::CreateFormat(cls, format);
		format.Add("person", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Attached body"));
		format.Add("subgroup", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Superior subgroup"));
		format.Add("id", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("ID in group"));
		format.Add("name", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Full name"));
		format.Add("face", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Face"));
		format.Add("glasses", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Glasses type"));
		format.Add("speaker", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Speaker"));
		format.Add("pitch", NDTFloat, NCTNone, DEFVALUE(float, 1.0f), DOC_MSG("Voice pitch"));
		format.Add("rank", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, RankPrivate), DOC_MSG("Current rank"));
		format.Add("experience", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Amount of experience"));
		format.Add("initExperience", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Initial amount of experience"));
		format.Add("playable", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Is unit playable"));
		format.Add("squadPicture", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Squad picture (shown on vehicles)"));
		format.Add("squadTitle", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Squad title (shown on vehicles)"));
		break;
	case NMCUpdateGeneric:
		NetworkObject::CreateFormat(cls, format);
		format.Add("person", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Attached body"));
		format.Add("experience", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Amount of experience"), ET_ABS_DIF, 0.01 * ERR_COEF_VALUE_MINOR);
		format.Add("ability", NDTFloat, NCTFloat0To1, DEFVALUE(float, 0.2), DOC_MSG("Ability of (AI) unit"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);
		format.Add("semaphore", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, SemaphoreYellow), DOC_MSG("Combat mode"), ET_NOT_EQUAL, ERR_COEF_MODE);
		format.Add("combatModeMajor", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, CMAware), DOC_MSG("Behaviour"), ET_NOT_EQUAL, ERR_COEF_MODE);
		format.Add("dangerUntil", NDTTime, NCTNone, DEFVALUE(Time, Time(0)), DOC_MSG("In danger mode until..."), ET_NOT_EQUAL, ERR_COEF_MODE);
		format.Add("captive", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Captive unit"));
		// TODO: format.Add("targetAssigned", NDTRef, NCTNone, DEFVALUENULL);
		// TODO: format.Add("targetAssignedValid", NDTTime, NCTNone, DEFVALUE(Time, Time(0)));
//		Path::CreateFormat(format);
		format.Add("house", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("House, where destination is"), ET_NOT_EQUAL, ERR_COEF_VALUE_MAJOR);
		format.Add("housePos", NDTInteger, NCTSmallSigned, DEFVALUE(int, -1), DOC_MSG("Destination position in house"), ET_NOT_EQUAL, ERR_COEF_VALUE_MAJOR);
		format.Add("wantedPosition", NDTVector, NCTNone, DEFVALUE(Vector3, VUndefined), DOC_MSG("Destination position"), ET_NOT_EQUAL, ERR_COEF_VALUE_MINOR);
//		format.Add("plannedPosition", NDTVector, NCTNone, DEFVALUE(Vector3, VUndefined), ET_NOT_EQUAL, ERR_COEF_MODE);
		format.Add("planningMode", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, DoNotPlan), DOC_MSG("How to plan path"), ET_NOT_EQUAL, ERR_COEF_VALUE_MINOR);
//		format.Add("completedReceived", NDTBool, NCTNone, DEFVALUE(bool, false));
		format.Add("formationAngle", NDTFloat, NCTFloatAngle, DEFVALUE(float, 0), DOC_MSG("Relative orientation in formation"));
		format.Add("formationPos", NDTVector, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Relative position in formation"));
//		format.Add("expPosition", NDTVector, NCTNone, DEFVALUE(Vector3, VZero), ET_SQUARE_DIF, 0.1 * ERR_COEF_VALUE_MINOR);
		format.Add("state", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, Wait), DOC_MSG("Unit (FSM) state"), ET_NOT_EQUAL, ERR_COEF_MODE);
		format.Add("mode", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, Normal), DOC_MSG("Precision of path planning"), ET_NOT_EQUAL, ERR_COEF_MODE);
		format.Add("lifeState", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, LSAlive), DOC_MSG("Life state of unit (Alive, Death, Asleep etc.)"), ET_NOT_EQUAL, ERR_COEF_MODE);
		// ?? _delay
		format.Add("getInAllowed", NDTBool, NCTNone, DEFVALUE(bool, true), DOC_MSG("Can get in vehicles"), ET_NOT_EQUAL, ERR_COEF_MODE);
		format.Add("getInOrdered", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Must get in vehicles"), ET_NOT_EQUAL, ERR_COEF_MODE);
		// TODO: strategic plan
//		format.Add("lastPlan", NDTBool, NCTNone, DEFVALUE(bool, false));
//		format.Add("noPath", NDTBool, NCTNone, DEFVALUE(bool, false), ET_NOT_EQUAL, ERR_COEF_MODE);
		break;
	default:
		NetworkObject::CreateFormat(cls, format);
		break;
	}
	return format;
}

AIUnit *AIUnit::CreateObject(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesCreateAIUnit *>(ctx.GetIndices()))
	const IndicesCreateAIUnit *indices = static_cast<const IndicesCreateAIUnit *>(ctx.GetIndices());

	Person *person;
	if (ctx.IdxTransferRef(indices->person, person) != TMOK) return NULL;
	if (!person) return NULL;
	AISubgroup *subgrp;
	if (ctx.IdxTransferRef(indices->subgroup, subgrp) != TMOK) return NULL;
	if (!subgrp) return NULL;
	AIGroup *grp = subgrp->GetGroup();
	if (!grp) return NULL;
	int id;
	if (ctx.IdxTransfer(indices->id, id) != TMOK) return NULL;
	AIUnit *unit = person->Brain();
	unit->_id = id;
	grp->SetUnit(id - 1, unit);
	subgrp->AddUnit(unit);
	
	// temporary select leader (avoid diag output)
	if (!grp->Leader()) grp->GetCenter()->SelectLeader(grp);
	if (!subgrp->Leader()) subgrp->SelectLeader();

	NetworkId objectId;
	if (ctx.IdxTransfer(indices->objectCreator, objectId.creator) != TMOK) return NULL;
	if (ctx.IdxTransfer(indices->objectId, objectId.id) != TMOK) return NULL;
	unit->SetNetworkId(objectId);
	unit->SetLocal(false);

	unit->TransferMsg(ctx);

	return unit;
}

void AIUnit::DestroyObject()
{
	AddRef();
	
	// remove from AI structure
	Ref<AISubgroup> oldSubgroup = (AISubgroup*)_subgroup;
	Ref<AIGroup> oldGroup = GetGroup();
	bool bSubgroupLeader = oldSubgroup && oldSubgroup->Leader() == this;
	bool bGroupLeader = oldGroup && oldGroup->Leader() == this;
	if (oldGroup)
	{
		oldGroup->UnitRemoved(this);
		_subgroup = NULL;
		_id = 0;
	}
	if (bGroupLeader) oldGroup->GetCenter()->SelectLeader(oldGroup);
	if (bSubgroupLeader) oldSubgroup->SelectLeader();

	// remove from vehicle
	Transport *veh = VehicleAssigned();
	if (veh) veh->UpdateStop();

	Person *person = _person;
	if (person)
	{
		_person = NULL;
		GWorld->RemoveSensor(person);
		person->SetBrain(NULL);
	}
	Release();
}

/*!
\patch 1.04 Date 07/13/2001 by Jirka
	- Fixed: squad pictures and titles missing on client machines
\patch_internal 1.04 Date 07/13/2001 by Jirka
	- added transfer of AIUnitInfo::_squadTitle and AIUnitInfo::_squadPicture
\patch_internal 1.12 Date 08/06/2001 by Jirka
	- Fixed: improved reaction when bad order of incoming messages
*/
TMError AIUnit::TransferMsg(NetworkMessageContext &ctx)
{
	switch (ctx.GetClass())
	{
	case NMCCreate:
		{
			TMCHECK(NetworkObject::TransferMsg(ctx))
			{
				Assert(dynamic_cast<const IndicesCreateAIUnit *>(ctx.GetIndices()))
				const IndicesCreateAIUnit *indices = static_cast<const IndicesCreateAIUnit *>(ctx.GetIndices());

				if (ctx.IsSending())
				{
					ITRANSF_REF(person)
					ITRANSF_REF(subgroup)
					ITRANSF(id)
				}
				AIUnitInfo &info = GetPerson()->GetInfo();
				TMCHECK(ctx.IdxTransfer(indices->name, info._name))
				TMCHECK(ctx.IdxTransfer(indices->face, info._face))
				TMCHECK(ctx.IdxTransfer(indices->glasses, info._glasses))
				TMCHECK(ctx.IdxTransfer(indices->speaker, info._speaker))
				TMCHECK(ctx.IdxTransfer(indices->pitch, info._pitch))
				TMCHECK(ctx.IdxTransfer(indices->rank, (int &)info._rank))
				TMCHECK(ctx.IdxTransfer(indices->experience, info._experience))
				TMCHECK(ctx.IdxTransfer(indices->initExperience, info._initExperience))
				TMCHECK(ctx.IdxTransfer(indices->playable, _playable))
				// ADDED in 1.04
				TMCHECK(ctx.IdxTransfer(indices->squadTitle, info._squadTitle))
				if (ctx.IsSending())
				{
					// ADDED in 1.04
					RString picture;
					if (info._squadPicture) picture = info._squadPicture->GetName();
					TMCHECK(ctx.IdxTransfer(indices->squadPicture, picture))
				}
				else
				{
					// ADDED in 1.04
					RString picture;
					TMCHECK(ctx.IdxTransfer(indices->squadPicture, picture))
					if (picture.GetLength() > 0)
						info._squadPicture = GlobLoadTexture(picture);

					GetPerson()->SetFace(info._face, info._name);
					GetPerson()->SetGlasses(info._glasses);
					SetSpeaker(info._speaker, info._pitch);
				}
			}
		}
		break;
	case NMCUpdateGeneric:
		TMCHECK(NetworkObject::TransferMsg(ctx))
		{
			Assert(dynamic_cast<const IndicesUpdateAIUnit *>(ctx.GetIndices()))
			const IndicesUpdateAIUnit *indices = static_cast<const IndicesUpdateAIUnit *>(ctx.GetIndices());

			if (ctx.IsSending())
			{
				ITRANSF_REF(person)
			}
			else
			{
				VehicleWithBrain *person = NULL;
				TMCHECK(ctx.IdxTransferRef(indices->person, person))
				// FIX
				if (person && person != _person)
				{
					AddRef();
					if (_person)
					{
						if (person)
						{
							AIUnitInfo &info = _person->GetInfo();
							person->SetInfo(info);
							person->SetFace(info._face, info._name);
							person->SetGlasses(info._glasses);
						}
						GWorld->RemoveSensor(_person);
						_person->SetBrain(NULL);
						_person = NULL;
					}
					if (person)
					{
						person->SetBrain(this);
						_person = person;
					}
					Release();
					_inVehicle = NULL;
				}
			}
			if (_person)
			{
				TMCHECK(ctx.IdxTransfer(indices->experience, _person->GetInfo()._experience))
			}
			ITRANSF(ability)
			if (!ctx.IsSending()) _invAbility = 1.0 / _ability;
			ITRANSF_ENUM(semaphore)
			ITRANSF_ENUM(combatModeMajor)
			ITRANSF(dangerUntil)
			ITRANSF(captive)
			// TODO: TMCHECK(ctx.IdxTransferRef(indices->targetAssigned, _targetAssigned))
			// TODO: TMCHECK(ctx.IdxTransfer(indices->targetAssignedValid, _targetAssignedValid))
//			TMCHECK(_path.TransferMsg(ctx, indices->path))
			ITRANSF_REF(house)
			ITRANSF(housePos)
			ITRANSF(wantedPosition)
//			ITRANSF(plannedPosition)
			ITRANSF_ENUM(planningMode)
//			ITRANSF(completedReceived)
			ITRANSF(formationAngle)
			ITRANSF(formationPos)
//			ITRANSF(expPosition)
			ITRANSF_ENUM(state)
			ITRANSF_ENUM(mode)
			ITRANSF_ENUM(lifeState)
			// ?? _delay
			ITRANSF(getInAllowed)
			ITRANSF(getInOrdered)
			// TODO: strategic plan
//			ITRANSF(lastPlan)
//			ITRANSF(noPath)
			if (!ctx.IsSending())
			{
				_path.Clear();
				_plannedPosition = _wantedPosition;
				_completedReceived = false;
				_expPosition = _wantedPosition;
				_lastPlan = false;
				_noPath = true;
			}
		}
		break;
	default:
		TMCHECK(NetworkObject::TransferMsg(ctx))
		break;
	}
	return TMOK;
}

float AIUnit::CalculateError(NetworkMessageContext &ctx)
{
	float error = NetworkObject::CalculateError(ctx);

	Assert(dynamic_cast<const IndicesUpdateAIUnit *>(ctx.GetIndices()))
	const IndicesUpdateAIUnit *indices = static_cast<const IndicesUpdateAIUnit *>(ctx.GetIndices());

	if (_person)
	{
		ICALCERRE_ABSDIF(float, experience, _person->GetInfo()._experience, 0.01 * ERR_COEF_VALUE_MINOR)
	}
	ICALCERRE_ABSDIF(float, ability, _ability, ERR_COEF_VALUE_MAJOR)
	ICALCERR_NEQ(int, semaphore, ERR_COEF_MODE)
	ICALCERR_NEQ(int, combatModeMajor, ERR_COEF_MODE)
	ICALCERR_NEQ(Time, dangerUntil, ERR_COEF_MODE)
	ICALCERR_NEQ(bool, captive, ERR_COEF_MODE)
	ICALCERR_NEQREF(Object, house, ERR_COEF_VALUE_MAJOR)
	ICALCERR_NEQ(int, housePos, ERR_COEF_VALUE_MAJOR)
	ICALCERR_NEQ(Vector3, wantedPosition, ERR_COEF_VALUE_MINOR)
//	ICALCERR_NEQ(Vector3, plannedPosition, ERR_COEF_MODE)
	ICALCERR_NEQ(int, planningMode, ERR_COEF_VALUE_MINOR)
//	ICALCERR_DIST2(expPosition, 0.1 * ERR_COEF_VALUE_MINOR)
	ICALCERR_NEQ(int, state, ERR_COEF_MODE)
	ICALCERR_NEQ(int, mode, ERR_COEF_MODE)
	ICALCERR_NEQ(bool, getInAllowed, ERR_COEF_MODE)
	ICALCERR_NEQ(bool, getInOrdered, ERR_COEF_MODE)
//	error += _path.CalculateError(ctx, indices->path);
//	ICALCERR_NEQ(bool, noPath, ERR_COEF_MODE)
	return error;
}

EntityAI* AIUnit::GetVehicle() const
{
	EntityAI* inVeh=_inVehicle.GetLink();
	if (inVeh) return inVeh;
	else return _person.GetLink();
}

/*!
\patch 1.33 Date 11/28/2001 by Jirka
- Fixed: ErrorMessage when MP server has VoiceRH addon instaled and client hasn't
*/
void AIUnit::SetSpeaker(RString speaker, float pitch)
{
	const ParamEntry &cfg = Pars >> "CfgVoice";
	if (speaker.GetLength() <= 0)
	{
		// TODO: remove hack (to patch NULL speaker in MP)
		Fail("Speaker");
		speaker = (cfg >> "voices")[0];
	}
	const ParamEntry *entry = cfg.FindEntry(speaker);
	if (!entry)
	{
		speaker = (cfg >> "voices")[0];
		entry = cfg.FindEntry(speaker);
	}
	Assert(entry);
	Ref<BasicSpeaker> basic = new BasicSpeaker(*entry);
	_speaker = new Speaker(basic, pitch); 
}

CombatMode AIUnit::GetCombatMode() const
{
	CombatMode mode = _combatModeMajor;
	if (mode!=CMCareless)
	{
		AIGroup *grp = GetGroup();
		if (grp)
		{
			CombatMode gMode = grp->GetCombatModeMinor();
			if (mode<gMode) mode = gMode;
		}
		EntityAI *veh = GetVehicle();
		if (veh->GetFireTarget() && !veh->IsFirePrepare())
		{
			CombatMode gMode = CMCombat;
			if (mode<gMode) mode = gMode;
		}
	}
	return mode;
}

bool AIUnit::HasAI() const
{
	if (_inVehicle)
	{
		if (_inVehicle->QIsManual(this)) return false;
	}
	else
		if (_person->QIsManual()) return false;

	return !_person->IsRemotePlayer();
}

bool AIUnit::IsUnit() const
{
	if (!_inVehicle) return true;
	return _inVehicle->CommanderUnit() == this;
}

bool AIUnit::IsCommander() const
{
	Transport *vehIn = GetVehicleIn();
	return vehIn && vehIn->CommanderBrain() == this;
}

bool AIUnit::IsDriver() const
{
	Transport *vehIn = GetVehicleIn();
	return vehIn && vehIn->DriverBrain() == this;
}

bool AIUnit::IsGunner() const
{
	Transport *vehIn = GetVehicleIn();
	return vehIn && vehIn->GunnerBrain() == this;
}

bool AIUnit::IsInCargo() const
{
	// TODO: inline release version
	return _state==InCargo;
}

/*
const float MinAbility=0.2; // private
const float MinRank=RankPrivate;
const float MaxAbility=1;
const float MaxRank=RankColonel;
*/

void AIUnit::SetAbility(float ability)
{
	Assert(ability > 0);
	_ability = ability;
	_invAbility = 1.0 / ability;
}
/*!
\patch 1.33 Date 12/03/2001 by Ondra
- New: Diffuculty option "Super AI". When this option is enabled,
all AI units have maximal skill.
*/
float AIUnit::GetAbility() const
{
	// returns from 1 (able) to 0.2 (unable)
	if( IsAnyPlayer() || Glob.config.IsEnabled(DTUltraAI) ) return 1; // always maximal
	return _ability;
}

float AIUnit::GetInvAbility() const
{
	// returns from 1 (able) to 5 (unable)
	if( IsAnyPlayer() || Glob.config.IsEnabled(DTUltraAI)) return 1; // always maximal
	return _invAbility;
}

void AIUnit::AddExp(float exp)
{
	float limit = floatMin(GetPerson()->GetInfo()._experience, 0);
	GetPerson()->GetInfo()._experience += exp;
	if (GetPerson() != GLOB_WORLD->GetRealPlayer())
		saturateMax(GetPerson()->GetInfo()._experience, limit);
}

void AIUnit::ClearOperativePlan()
{
	_path.Clear();
	_path.SetOperIndex(1);
	_path.SetMaxIndex(1);
	_path.SetOnRoad(false);
}

void AIUnit::ClearStrategicPlan()
{
	_noPath = true;
	_updatePath = false;
	_lastPlan = false;

#if _ENABLE_AI
	_attemptPlan = 0;
	_planner->Init();
	_waitWithPlan = Time(0);
	_completedTime = Glob.clock.GetTimeInYear();
#endif

	if 
	(
		GetState() != InCargo &&
		GetState() != Stopping && GetState() != Stopped
	)
		Verify(SetState(Wait));

	_exposureChange = 0;
}

void AIUnit::RefreshStrategicPlan()
{
#if _ENABLE_AI
	if (_noPath)
	{
		ClearStrategicPlan();
	}
	else
	{
		Assert(_planner->GetPlanSize() > 0);
		_planner->StopSearching();
		_updatePath = true;
	}
	_exposureChange = 0;
#endif
}

void AIUnit::ForceReplan(bool clear)
{
	_plannedPosition = _wantedPosition;
	if (clear)
	{
		ClearStrategicPlan();
		ClearOperativePlan();
	}
	else
		RefreshStrategicPlan();
}

void AIUnit::ExposureChanged(int x, int z, float optimistic, float pessimistic)
{
#if _ENABLE_AI
	float exposure = IsHoldingFire() ? fabs(pessimistic) : fabs(optimistic);
	if (exposure < VALID_EXPOSURE_CHANGE) return;

	if
	(
		_planner->IsOnPath
		(
			x, z,
			_planner->FindBestIndex(Position()),
			_planner->GetPlanSize() - 1
		)
	) _exposureChange += exposure;
#endif
}

bool AIUnit::SetState(State state)
{
	if (state == Completed)
	{
		if (_planningMode == DoNotPlan) return true;
		
		if (_completedReceived) return true;
		_completedReceived = true;
//LogF("Unit %s: Completed received", (const char *)GetDebugName());

/*
		if (IsSubgroupLeaderVehicle() && GetSubgroup()->HasCommand())
		{
			Command *cmd = GetSubgroup()->GetCommand();
			LogF("%s: Completed (%d)", (const char *)GetDebugName(), _planningMode);
			LogF("  position: %.0f, %.0f", Position().X(), Position().Z());
			LogF("  planned: %.0f, %.0f", _plannedPosition.X(), _plannedPosition.Z());
			LogF("  wanted: %.0f, %.0f", _wantedPosition.X(), _wantedPosition.Z());
			LogF("  command: %.0f, %.0f", cmd->_destination.X(), cmd->_destination.Z());
			if (cmd->_destination.Distance2(_wantedPosition) > Square(1))
			{
				Fail("  Error !!!");
			}
			if (cmd->_destination.Distance2(Position()) > Square(10))
			{
				Fail("  Error !!!");
			}
		}
*/
	}


	if (state == InCargo)
	{
		_state = InCargo;
		return true;
	}

	if (_inVehicle)
	{
		AIUnit *unit;
		unit = _inVehicle->PilotUnit();
		if (unit)
		{
			if (state == Wait) ClearOperativePlan();
			unit->_state = state;
		}
		unit = _inVehicle->ObserverUnit();
		if (unit)
		{
			if (state == Wait) ClearOperativePlan();
			unit->_state = state;
		}
		unit = _inVehicle->GunnerUnit();
		if (unit)
		{
			if (state == Wait) ClearOperativePlan();
			unit->_state = state;
		}
	}
	else
	{
		Assert(_state != InCargo);
		if (_state == InCargo) return false;
		if (state == Wait) ClearOperativePlan();
		_state = state;
	}
	return true;
}

// check size of formation

static float FormSize( AISubgroup *subgrp )
{
	return 0.0f;
}

Vector3 AIUnit::GetFormationRelative() const
{
	// most common case
	// no need to calculate formation size
	if (_formPos == AI::PosInFormation) return _formationPos;
	// relative to leader
	// consider formation position (advance etc...)
	Vector3 offset = VZero;
	// calculate formation size
	float sizeX=0;
	float sizeZ=0;
	AISubgroup *subgrp = GetSubgroup();
	for (int i=0; i<subgrp->NUnits(); i++)
	{
		AIUnit *unit = subgrp->GetUnit(i);
		if (!unit) continue;
		EntityAI *veh = unit->GetVehicle();
		sizeX += veh->GetFormationX();
		sizeZ += veh->GetFormationZ();
	}
	if (subgrp->NUnits()<8)
	{
		float nCoef = 8.0/subgrp->NUnits();
		sizeX *= nCoef;
		sizeZ *= nCoef;
	}
	float coef = _formPosCoef;
	switch (_formPos)
	{
		case AI::PosAdvance: offset[2] += sizeZ*coef; break;
		case AI::PosStayBack: offset[2] -= sizeZ*coef; break;
		case AI::PosFlankLeft: offset[0] -= sizeX*coef; break;
		case AI::PosFlankRight: offset[0] += sizeX*coef; break;
	}
	return _formationPos+offset;
}

Vector3 AIUnit::GetFormationAbsolute( AIUnit *leader) const
{
	// absolute formation position
	// suppose leader is formation leader
	EntityAI *lVehicle = leader->GetVehicle();

	AISubgroup *subgrp = leader->GetSubgroup();
	Matrix4 transform;
	Vector3Val formDir=subgrp->GetFormationDirection();
	transform.SetDirectionAndUp(formDir,VUp);
	transform.SetPosition(lVehicle->Position());

	Vector3 pos = transform.FastTransform
	(
		GetFormationRelative()-leader->GetFormationRelative()
	);
	if( GetVehicle()->GetType()->GetKind()!=VAir )
	{
	// absolute formation position
		pos[1]=GLandscape->SurfaceYAboveWater(pos[0],pos[2]);
	}
	else
	{
		float landY=GLandscape->SurfaceYAboveWater(pos[0],pos[2]);
		saturateMax(pos[1],landY+25);
	}
	return pos;
}

Vector3 AIUnit::GetFormationAbsolute() const
{
	if( !_subgroup ) {Fail("No subgroup");return GetVehicle()->Position();}
	AIUnit *leader = _subgroup->Leader();
	if( !leader )  {Fail("No leader");return GetVehicle()->Position();}
	return GetFormationAbsolute(leader);
}

bool AIUnit::IsSimplePath(Vector3Val from, Vector3Val pos)
{
	const float maxDist=300;
	float dist2=(from-pos).SquareSizeXZ();
	if( dist2>Square(maxDist) ) return false;

	GetVehicle()->PerformUnlock();

	int xs = toIntFloor(from.X() * InvOperItemGrid);
	int zs = toIntFloor(from.Z() * InvOperItemGrid);
	int xe = toIntFloor(pos.X() * InvOperItemGrid);
	int ze = toIntFloor(pos.Z() * InvOperItemGrid);

	int xMin = xs/OperItemRange;
	int zMin = zs/OperItemRange;
	int xMax = xe/OperItemRange;
	int zMax = ze/OperItemRange;
	if (xMax < xMin) swap(xMax,xMin);
	if (zMax < zMin) swap(zMax,zMin);

	OperMap map;
	if ((xMax-xMin)>50 || (zMax-zMin)>50)
	{
		LogF("xMax %d, xMin %d",xMax,xMin);
		LogF("zMax %d, zMin %d",xMax,xMin);
		LogF("pos %.2f,%.2f,%.2f",pos[0],pos[1],pos[2]);
		LogF("from %.2f,%.2f,%.2f",from[0],from[1],from[2]);
	}
	map.CreateMap(GetVehicle(), xMin, zMin, xMax, zMax);
	bool retVal = map.IsSimplePath(this, xs, zs, xe, ze);

	GetVehicle()->PerformLock();
	return retVal;
}

#if _ENABLE_AI

bool AIUnit::VerifyPath()
{
	// do not verify fresh path 
	if( _path.GetSearchTime()>Glob.time-0.5 )
	{
		// assume fresh path is always valid
		return true;
	}

	EntityAI *veh=GetVehicle();
	if (_path.GetOnRoad())
	{
		veh->PerformUnlock();
		int index = _path.GetOperIndex();
		for (int i=index; i<_path.GetMaxIndex(); i++)
		{
			if (GRoadNet->IsLocked(_path[i]._pos, 0.1)) return false;
		}
		veh->PerformLock();
	}
	else
	{
		return true;
		// TODO: repair

		#if 0

		Vector3 lastPos = Position();
		{
			OperMap map;
			int x = toIntFloor(lastPos.X() * InvOperItemGrid);
			int z = toIntFloor(lastPos.Z() * InvOperItemGrid);
			int xx = x / OperItemRange;
			int zz = z / OperItemRange;
			veh->PerformUnlock();
			map.CreateMap(this, xx, zz, xx, zz);
			bool result = map.GetFieldCost(x, z, true, veh, IsSoldier()) >= GET_UNACCESSIBLE;
			veh->PerformLock();
			if (result)
				return true;	// cannot find valid path - don't try it
		}

		int index = _path.GetOperIndex();
		for (int i=index; i<_path.GetMaxIndex(); i++)
		{
			if (!IsSimplePath(lastPos, _path[i]._pos))
			{
/*
LogF
(
	"Unit %s: path is not valid - index %d/%d",
	(const char *)GetDebugName(), i, _path.GetMaxIndex() - 1
);
*/
				return false;
			}
			lastPos = _path[i]._pos;
		}
		#endif
	}
	return true;
}

void AIUnit::CopyPath(const IAIPathPlanner &planner)
{
	EntityAI *veh = GetVehicle();
	float combatHeight = veh->GetCombatHeight();

	int planIndex = planner.FindBestIndex(Position());
	int i, n = planner.GetPlanSize() - planIndex;
	if (planner.GetPlanSize() > 0 && n > 0)
	{
		float sumCost = 0;
		Point3 lastPos = Position();

		_path.Resize(n + 1);
		_path[0]._pos = lastPos;
		_path[0]._cost = sumCost;
		for (i=0; i<n; i++)
		{
			Point3 pos;
			planner.GetPlanPosition(planIndex + i, pos);
			GeographyInfo geogr = planner.GetGeography(planIndex + i);
			float cost = veh->GetCost(geogr) * veh->GetFieldCost(geogr);
			if (cost >= GET_UNACCESSIBLE)
				cost = 1.0;
			cost *= (pos - lastPos).SizeXZ();
			sumCost += cost;
			lastPos = GLandscape->PointOnSurface(pos[0],combatHeight,pos[2]);
			_path[i + 1]._pos = lastPos;
			_path[i + 1]._cost = sumCost;
		}
	}
	else
	{
		int x = toIntFloor(Position().X() * InvLandGrid);
		int z = toIntFloor(Position().Z() * InvLandGrid);
		GeographyInfo geogr = GLOB_LAND->GetGeography(x, z);
		float cost = veh->GetCost(geogr) * veh->GetFieldCost(geogr);
		if (cost >= GET_UNACCESSIBLE)
			cost = 1.0;
		cost *= (_expPosition - Position()).SizeXZ();
		_path.Resize(2);
		_path[0]._pos = Position();
		_path[0]._cost = 0;
		_path[1]._pos = GLandscape->PointOnSurface(_expPosition[0],combatHeight,_expPosition[2]);
		_path[1]._cost = cost;
	}
	_path.SetMaxIndex(2);
	_path.SetOperIndex(1);
	_path.SetOnRoad(false);
	_path.SetSearchTime(Glob.time);
}
#endif

static const IPaths *InsideBuilding(Vector3Val pos)
{
	int xMin,xMax,zMin,zMax;
	ObjRadiusRectangle(xMin,xMax,zMin,zMax,pos,pos,50);
	int x,z;
	for( x=xMin; x<=xMax; x++ ) for( z=zMin; z<=zMax; z++ )
	{
		const ObjectList &list=GLandscape->GetObjects(z,x);
		int n=list.Size();
		for( int i=0; i<n; i++ )
		{
			Object *obj=list[i];

			if (!obj->GetShape()) continue;
			if (obj->GetShape()->FindPaths() < 0) continue;
			// vehicle with paths - must be building
			float dist2 = obj->Position().Distance2(pos);
			if (dist2 > Square(obj->GetShape()->BoundingSphere())) continue;
			// pos is inside vehicle's bounding

			const IPaths *building = obj->GetIPaths();
			if (!building) continue;

			// check if point is near of some path in building
			Vector3 nearest;
			int nearestIndex = building->FindNearestPoint(pos,nearest,Square(2.5));
			if( nearestIndex>=0 )
			{
				return building;
			}
			// test if pos is inside building
			// test1
			/*
			for (int j=0; j<building->NPoints(); j++)
			{
				Vector3 bpos = building->GetPosition(j);
				if (bpos.Distance2(pos) < Square(2.5f))
					return building;
			}
			*/
			// test2
			/*
			CollisionBuffer result;
			obj->Intersect(result, pos, pos + Vector3(0, 0.5, 0), 0);
			if (result.Size() > 0)
			{
				LogF
				(
					"Point %.1f,%.1f,%.1f is inside %s",
					pos.X(), pos.Y(), pos.Z(),
					(const char *)obj->GetDebugName()
				);
				return building;
			}
			*/
		}
	}
	//LogF("Point %.1f,%.1f,%.1f is outside", pos.X(), pos.Y(), pos.Z());
	return NULL;
}

#define ROAD_DIAGS 0
#define HOUSE_DIAGS 0


#define DIST_NOT_SEARCH			5.0F

#if _ENABLE_AI

bool AIUnit::CreatePath(Vector3Val from, Vector3Val to)
{
	// this function always sets path.time to time of last search
	_path.SetSearchTime(Glob.time);
//LogF("Time %.1f: Searching path (house = %x)", Glob.time.toFloat(), _house);
	const IPaths *fromBuilding = InsideBuilding(from);
	const IPaths *toBuilding = _house ? _house->GetIPaths() : InsideBuilding(to);
	EntityAI *veh = GetVehicle();
	float combatHeight = veh->GetCombatHeight();
	if (IsFreeSoldier() && (fromBuilding || toBuilding))
	{
		// how much slower should units move in buildings
		if (fromBuilding == toBuilding)
		{
			#if HOUSE_DIAGS
				LogF
				(
					"%s: fromTo %s",
					(const char *)GetDebugName(),
					(const char *)toBuilding->GetObject()->GetDebugName()
				);
			#endif
			Vector3 dummy;
			int posFrom = fromBuilding->FindNearestPoint(from - Vector3(0, combatHeight, 0), dummy);
			if (posFrom < 0) return false;
			int posTo = _house ?
				toBuilding->GetPos(_housePos)  :
				toBuilding->FindNearestPoint(to, dummy);
			if (posTo < 0)
			{
				posTo = toBuilding->GetPos(0);
				if (posTo < 0) return false;
			}
			HOUSE_PATH_ARRAY(path,64);

			if (!fromBuilding->SearchPath(posFrom, posTo, path))
				return false;	// cannot get outside - failed
			int n = path.Size();
			_path.Resize(n);
			float cost = 0;
			Vector3 lastPos = path[0].pos;
			_path[0]._pos = lastPos;
			_path[0]._pos[1] += combatHeight;
			_path[0]._cost = cost;
			_path[0]._house = const_cast<Object *>(fromBuilding->GetObject());
			_path[0]._index = path[0].index;
			for (int i=1; i<n; i++)
			{
				cost += fromBuilding->GetBType()->GetCoefInside() *
					path[i].pos.Distance(lastPos) *
					GetVehicle()->GetType()->GetMinCost();
				lastPos = path[i].pos;
				_path[i]._pos = lastPos;
				_path[i]._pos[1] += combatHeight;
				_path[i]._cost = cost;
				_path[i]._house = const_cast<Object *>(fromBuilding->GetObject());
				_path[i]._index = path[i].index;
			}
			_path.SetOperIndex(1);
			_path.SetMaxIndex(n);
			_path.SetOnRoad(false);
			return true;
		}
		else
			return CreateNoRoadPath(from, to, fromBuilding, toBuilding);
	}
	if (!GetVehicle()->IsCautious() && CreateRoadPath(from, to)) return true;
	return CreateNoRoadPath(from, to);
}

#endif //_ENABLE_AI

bool AIUnit::CheckEmpty(Vector3Par pos )
{
	EntityAI *veh = GetVehicle();
	veh->PerformUnlock();

	float xEnd = pos.X();
	float zEnd = pos.Z();
	int xe = toIntFloor(xEnd * InvOperItemGrid);
	int ze = toIntFloor(zEnd * InvOperItemGrid);

	int xei=xe/OperItemRange;
	int zei=ze/OperItemRange;

	const int border = 0;

	OperMap map;
	map.CreateMap(veh, xei - border, zei - border, xei + border, zei + border);

	bool ret = map.GetFieldCost(xe, ze, true, GetVehicle(), IsSoldier()) < GET_UNACCESSIBLE;
	veh->PerformLock();
	return ret;
}

bool AIUnit::FindNearestEmpty(Vector3 &pos, bool soldier, EntityAI *veh)
{
	veh->PerformUnlock();

	float xEnd = pos.X();
	float zEnd = pos.Z();
	float xef = xEnd * InvOperItemGrid;
	float zef = zEnd * InvOperItemGrid;
	int xe = toIntFloor(xef);
	int ze = toIntFloor(zef);

	int xei=xe/OperItemRange;
	int zei=ze/OperItemRange;

	const int border = 1;

	OperMap map;
	map.CreateMap(veh, xei - border, zei - border, xei + border, zei + border);

	if (map.GetFieldCost(xe, ze, true, veh, soldier) >= GET_UNACCESSIBLE)
	{
		int oxe=xe,oze=ze;
		if
		(
			map.FindNearestEmpty
			(
					xe, ze, xef, zef,
				(xei - border) * OperItemRange, (zei - border) * OperItemRange,
				(1 + 2 * border) * OperItemRange,
				(1 + 2 * border) * OperItemRange, true, veh, soldier,
				DefFindFreePositionCallback, NULL
			)
		)
		{
			// xEnd, zEnd must be changed (is forced into last path point)
			if( xe!=oxe || ze!=oze )
			{
				pos[0]= xe * OperItemGrid + 0.5 * OperItemGrid;
				pos[2] = ze * OperItemGrid + 0.5 * OperItemGrid;
			}
		}
		else
		{
#if LOG_PROBL
			Log("Unit %s: End point not found", (const char *)veh->GetDebugName());
			map.LogMap(xei - border, xei + border, zei - border, zei + border, xe, xe, ze, ze, false);
#endif
			veh->PerformLock();
			return false;
		}
	}
	veh->PerformLock();
	return true;
}

bool AIUnit::FindNearestEmpty(Vector3 &pos )
{
	return FindNearestEmpty(pos, IsSoldier(), GetVehicle());
}

#if _ENABLE_AI

bool AIUnit::CreateNoRoadPath
(
	Vector3Val from, Vector3Val pos,
	const IPaths *fromBuilding, const IPaths *toBuilding
)
{
	Assert(!fromBuilding || !toBuilding || fromBuilding != toBuilding);
	
	GetPath().SetSearchTime(Glob.time);

	const float maxDist=300;
	float dist2=(from-pos).SquareSizeXZ();

	if (dist2 > Square(maxDist))
	{
		LogF("Error: Unit %s is searching path per distance %.0f", (const char *)GetDebugName(), sqrt(dist2));
		ClearOperativePlan();
		return false;
	}
	
	EntityAI *veh = GetVehicle();
	float combatHeight = veh->GetCombatHeight();

	veh->PerformUnlock();

	Vector3 fromOutside = from;
	int fromBuildingFromPos = -1;
	int fromBuildingToPos = -1;
	if (fromBuilding)
	{
		Vector3 dummy;
		fromBuildingFromPos = fromBuilding->FindNearestPoint(from - Vector3(0, combatHeight, 0), dummy);
		if (fromBuildingFromPos < 0)
		{
			LogF("Unit %s: From building from point not found", (const char *)GetDebugName());
			veh->PerformLock();
			return false;	// no point
		}
		fromBuildingToPos = fromBuilding->FindNearestExit(pos, fromOutside);
		if (fromBuildingToPos < 0)
		{
			LogF("Unit %s: From building exit not found", (const char *)GetDebugName());
			veh->PerformLock();
			return false;		// no exit
		}
	}
	Vector3 toOutside = pos;
	int toBuildingFromPos = -1;
	int toBuildingToPos = -1;
	if (toBuilding)
	{
		Vector3 dummy;
		toBuildingToPos = _house ?
			toBuilding->GetPos(_housePos) :
			toBuilding->FindNearestPoint(pos, dummy);
		if (toBuildingToPos < 0)
		{
			LogF("Unit %s: To building to position not found", (const char *)GetDebugName());
			veh->PerformLock();
			return false;			// no point
		}
		toBuildingFromPos = toBuilding->FindNearestExit(from, toOutside);
		if (toBuildingFromPos < 0)
		{
			LogF("Unit %s: To building exit not found", (const char *)GetDebugName());
			veh->PerformLock();
			return false;		// no exit
		}
	}

	float xStart = fromOutside.X();
	float zStart = fromOutside.Z();
	float xEnd = toOutside.X();
	float zEnd = toOutside.Z();

	float xsf = xStart * InvOperItemGrid;
	float zsf = zStart * InvOperItemGrid;
	int xs = toIntFloor(xsf);
	int zs = toIntFloor(zsf);

	float xef = xEnd * InvOperItemGrid;
	float zef = zEnd * InvOperItemGrid;
	int xe = toIntFloor(xef);
	int ze = toIntFloor(zef);

	int xMin = xs/OperItemRange;
	int zMin = zs/OperItemRange;
	int xMax = xe/OperItemRange;
	int zMax = ze/OperItemRange;
	if (xMax < xMin) swap(xMax,xMin);
	if (zMax < zMin) swap(zMax,zMin);

#if FIELD_ON_DEMAND
	const int border = 0;
#else
	const int border = 1;
#endif

	OperMap map;
	map.CreateMap(veh, xMin - border, zMin - border, xMax + border, zMax + border);

#if 0
LogF("FindPath statistics:");
LogF
(
	"\t%s is searching path from %d, %d to %d,%d - distance %.0f",
	(const char *)veh()->GetType()->GetDisplayName(), xs, zs, xe, ze, sqrt(dist2)
);
LogF
(
	"\tusing %d fields, memory %d",
	map._fields.Size(), map._fields.Size() * sizeof(OperField)
);
LogF
(
	"\ttotal in cache %d(soldiers) + %d(others)",
	GLandscape->OperationalCacheSoldiers()->NFields() * sizeof(OperField),
	GLandscape->OperationalCache()->NFields() * sizeof(OperField)
);
#endif

	if (map.GetFieldCost(xs, zs, true, veh, IsSoldier()) >= GET_UNACCESSIBLE)
		if
		(
			map.FindNearestEmpty
			(
				xs, zs, xsf, zsf,
				(xMin - border) * OperItemRange, (zMin - border) * OperItemRange,
				(xMax - xMin + 1 + 2 * border) * OperItemRange,
				(zMax - zMin + 1 + 2 * border) * OperItemRange, true, veh, IsSoldier(),
				DefFindFreePositionCallback, NULL
			)
		)
		{
/*	
			// from must be changed (is forced into first path point)
			from[0] = xs * OperItemGrid + 0.5 * OperItemGrid;
			from[2] = zs * OperItemGrid + 0.5 * OperItemGrid;
*/
		}
		else
		{
			LogF("Unit %s: Start point not found", (const char *)GetDebugName());
#if LOG_PROBL
			map.LogMap(xMin - border, xMax + border, zMin - border, zMax + border, xs, xe, zs, ze, false);
#endif
			veh->PerformLock();
			return false;
		}

	if (map.GetFieldCost(xe, ze, true, veh, IsSoldier()) >= GET_UNACCESSIBLE)
	{
		if
		(
			map.FindNearestEmpty
			(
				xe, ze, xef, zef,
				(xMin - border) * OperItemRange, (zMin - border) * OperItemRange,
				(xMax - xMin + 1 + 2 * border) * OperItemRange,
				(zMax - zMin + 1 + 2 * border) * OperItemRange, true, veh, IsSoldier(),
				DefFindFreePositionCallback, NULL
			)
		)
		{
/*
			// pos must be changed (is forced into last path point)
			pos[0] = xe * OperItemGrid + 0.5 * OperItemGrid;
			pos[2] = ze * OperItemGrid + 0.5 * OperItemGrid;
*/
		}
		else
		{
			LogF("Unit %s: End point not found", (const char *)GetDebugName());
#if LOG_PROBL
			map.LogMap(xMin - border, xMax + border, zMin - border, zMax + border, xs, xe, zs, ze, false);
#endif
			veh->PerformLock();
			return false;
		}
	}

	if (xe == xs && ze == zs)
	{
		// trivial path
/*
		_path.Resize(2);
		_path[0]._pos = GLandscape->PointOnSurface(from[0],combatHeight,from[2]);
		_path[0]._cost = 0;
		_path[1]._pos = GLandscape->PointOnSurface(pos[0],combatHeight,pos[2]);
		int x = toIntFloor(from.X() * InvLandGrid);
		int z = toIntFloor(from.Z() * InvLandGrid);
		GeographyInfo info = GLOB_LAND->GetGeography(x, z);
		_path[1]._cost = veh->GetCost(info) * (pos - from).SizeXZ();
*/
		map._path.Resize(2);
		map._path[0]._x = xs;
		map._path[0]._z = zs;
		map._path[0]._cost = 0;
		map._path[0].house = NULL;
		map._path[0].from = -1;
		map._path[0].to = -1;
		int x = toIntFloor(fromOutside.X() * InvLandGrid);
		int z = toIntFloor(fromOutside.Z() * InvLandGrid);
		GeographyInfo info = GLOB_LAND->GetGeography(x, z);
		map._path[1]._x = xe;
		map._path[1]._z = ze;
		map._path[1]._cost = veh->GetCost(info) * (toOutside - fromOutside).SizeXZ();
		map._path[1].house = NULL;
		map._path[1].from = -1;
		map._path[1].to = -1;

		goto PrefixPostfix;
/*
		_path.SetOperIndex(1);
		_path.SetMaxIndex(2);
		_path.SetOnRoad(false);

		return true;
*/
	}

	int dir, origSize;
	{
		if (GetVehicleIn() && GetVehicleIn()->GetMoveMode() == VMMBackward)
			dir = CalcDirection(-Direction());
		else
			dir = CalcDirection(Direction());

//		__int64 start = ReadTsc();
		if (xs == 3092 && zs == 1309 && xe == 3099 && ze == 1290 && dir == 0)
		{
			LogF("Here.");
		}
		bool result = map.FindPath(this, dir, xs, zs, xe, ze, true);
//		int total = int(ReadTsc() - start);
//		LogF("Path from %d,%d to %d,%d: %d", xs, zs, xe, ze, total);

		origSize = map._path.Size();
		if (!result)
		{
			LogF("Unit %s: Operative path not found", (const char *)GetDebugName());
#if LOG_PROBL
			map.LogMap(xMin - border, xMax + border, zMin - border, zMax + border, xs, xe, zs, ze, false);
#endif
			veh->PerformLock();
			return false;
		}
	}

PrefixPostfix:
	if (fromBuilding)
	{
		#if HOUSE_DIAGS
			LogF
			(
				"%s: from %s",
				(const char *)GetDebugName(),
				(const char *)fromBuilding->GetObject()->GetDebugName()
			);
		#endif
		map._path.Insert(0);
		Assert(map._path.Size() > 1);
		map._path[0]._x = toIntFloor(from.X() * InvOperItemGrid);
		map._path[0]._z = toIntFloor(from.Z() * InvOperItemGrid);
		map._path[0]._cost = 0;
		map._path[0].house = NULL;
		map._path[0].from = -1;
		map._path[0].to = -1;
		map._path[1].house = const_cast<Object *>(fromBuilding->GetObject());
		map._path[1].from = fromBuildingFromPos;
		map._path[1].to = fromBuildingToPos;
	}
	if (toBuilding && !map.IsAlternateGoal())
	{
		#if HOUSE_DIAGS
			LogF
			(
				"%s: to %s",
				(const char *)GetDebugName(),
				(const char *)toBuilding->GetObject()->GetDebugName()
			);
		#endif
		int index = map._path.Add();
		Assert(map._path.Size() > 1);
		map._path[index]._x = toIntFloor(pos.X() * InvOperItemGrid);
		map._path[index]._z = toIntFloor(pos.Z() * InvOperItemGrid);
		map._path[index]._cost = map._path[index - 1]._cost;
		map._path[index].house = const_cast<Object *>(toBuilding->GetObject());
		map._path[index].from = toBuildingFromPos;
		map._path[index].to = toBuildingToPos;
	}

	int maxIndex = map.ResultPath(this);
	if (_lastPlan) maxIndex = _path.Size();

	_path.SetOperIndex(1);
	_path.SetMaxIndex(maxIndex);
	_path.SetOnRoad(false);

	if (_path.Size() < 2)
	{
		RptF
		(
			"Error %s: Invalid path from [%.2f, %.2f, %.2f] (building %x) to [%.2f, %.2f, %.2f] (building %x).",
			(const char *)GetDebugName(),
			from.X(), from.Y(), from.Z(), fromBuilding,
			pos.X(), pos.Y(), pos.Z(), toBuilding
		);
		RptF
		(
			" - from [%d, %d] dir %d to [%d, %d] - original size %d",
			xs, zs, dir, xe, ze, origSize
		);
	}

	veh->PerformLock();

	if (!VerifyPath())
	{
		Fail("Path not verified.");
	}
	
	/**/
	_path[0]._pos = GLandscape->PointOnSurface(from[0],combatHeight,from[2]);

	if (!map.IsAlternateGoal())
	{
		int last = _path.Size() - 1;
		if (!_path[last]._house)
			_path[last]._pos = GLandscape->PointOnSurface(pos[0],combatHeight,pos[2]);
	}
	/**/

	return true;
}

bool AIUnit::CreateRoadPath(Vector3Val from, Vector3Val to)
{
	GetPath().SetSearchTime(Glob.time);

	EntityAI *veh = GetVehicle();

	veh->PerformUnlock();
	float precision = veh->GetPrecision();
	float combatHeight = veh->GetCombatHeight();
#if ROAD_DIAGS
	LogF("%s: Searching road path with precision %.1f", (const char *)GetDebugName(), precision);
#endif

	RoadPathArray roadPath; // StaticArray
	static StaticStorage<Vector3> vectorStorage;
	roadPath.SetStorage(vectorStorage.Init(128));

	if (!GRoadNet->SearchPath(from, to, roadPath, 1.25 * precision))
	{	
#if ROAD_DIAGS
		LogF("  Road path not found");
#endif
		veh->PerformLock();
		return false;
	}
#if ROAD_DIAGS
LogF("  Road path found");
#endif

	veh->PerformLock();

	int n = roadPath.Size();
	if (n == 1)
	{
		// trivial path
		_path.Resize(2);
		_path[0]._pos = GLandscape->PointOnSurface(from[0],combatHeight,from[2]);
		//_path[0]._dir = 0xff;
		_path[0]._cost = 0;

		_path[1]._pos = GLandscape->PointOnSurface(to[0],combatHeight,to[2]);
		//_path[1]._dir = 0xff;
		int x = toIntFloor(from.X() * InvLandGrid);
		int z = toIntFloor(from.Z() * InvLandGrid);
		GeographyInfo info = GLOB_LAND->GetGeography(x, z);
		_path[1]._cost = veh->GetCost(info) * (to - from).SizeXZ();

		_path.SetOperIndex(1);
		_path.SetMaxIndex(2);
		_path.SetOnRoad(false);
		return true;
	}
	Assert(n >= 2);

	_path.Resize(n);
	int maxIndex=-1;
	for (int i=0; i<n; i++)
	{
		// update cost info if necessary
		Vector3 lastPos = GLandscape->PointOnSurface(roadPath[i][0],combatHeight,roadPath[i][2]);
		_path[i]._pos = lastPos;
		_path[i]._cost = 0;
		float actDist2 = (lastPos - from).SquareSizeXZ();
		if (maxIndex < 0 && i > 1 && actDist2 >= Square(DIST_MAX_OPER))
		{
			maxIndex = i;
		}
	}
	veh->FillPathCost(_path);

	_path.SetOperIndex(1);
	if (maxIndex < 0) maxIndex = _path.Size();
	_path.SetMaxIndex(maxIndex);
	if (_lastPlan) _path.SetMaxIndex(_path.Size());

	_path.SetOnRoad(true);

	return true;
}

#endif // _ENABLE_AI

#define HEALTH_LOW			0.6F
#define HEALTH_CRITICAL	0.5F

#define ARMOR_LOW				0.6F
#define ARMOR_CRITICAL	0.5F

#define FUEL_LOW				0.3F
#define FUEL_CRITICAL		0.1F

#define AMMO_LOW				0.4F
#define AMMO_CRITICAL		0.2F

AIUnit::ResourceState AIUnit::GetFuelState() const
{
	if (IsSoldier()) return RSNormal;

	float curFuel =	GetVehicle()->GetFuel();
	float maxFuel = GetVehicle()->GetType()->GetFuelCapacity();

	if (curFuel < FUEL_CRITICAL * maxFuel) return RSCritical;
	if (curFuel < FUEL_LOW * maxFuel) return RSLow;
	return RSNormal;
}

AIUnit::ResourceState AIUnit::GetHealthState() const
{
	float health = 1.0 - GetPerson()->NeedsAmbulance();

	if (health < HEALTH_CRITICAL) return RSCritical;
	if (health < HEALTH_LOW) return RSLow;
	return RSNormal;
}

AIUnit::ResourceState AIUnit::GetArmorState() const
{
	if (IsSoldier()) return RSNormal;

	float armor = 1.0 - GetVehicle()->NeedsRepair();

	if (armor < ARMOR_CRITICAL) return RSCritical;
	if (armor < ARMOR_LOW) return RSLow;
	return RSNormal;
}

AIUnit::ResourceState AIUnit::GetAmmoState() const
{
	if (IsFreeSoldier())
	{
		float needs = GetPerson()->NeedsInfantryRearm();
		if (needs > 1 - AMMO_CRITICAL) return RSCritical;
		if (needs > 1 - AMMO_LOW) return RSLow;
		return RSNormal;
	}

	// used for soldiers in vehicle - ammo state of vehicle	
	if (!IsUnit()) return RSNormal;

	float curAmmo =	0;
	float maxAmmo = 0;
	EntityAI *veh = GetVehicle();
	for (int i=0; i<veh->NMagazines(); i++)
	{
		Magazine *magazine = veh->GetMagazine(i);
		if (!magazine) continue;
		curAmmo += magazine->_ammo;
		maxAmmo += magazine->_type->_maxAmmo;
	}

	if (curAmmo < AMMO_CRITICAL * maxAmmo) return RSCritical;
	if (curAmmo < AMMO_LOW * maxAmmo) return RSLow;
	return RSNormal;
}

void AIUnit::CheckAmmo
(
	const MuzzleType * &muzzle1, const MuzzleType * &muzzle2,
	int &slots1, int &slots2, int &slots3
)
{
	GetPerson()->CheckAmmo(muzzle1,muzzle2,slots1,slots2,slots3);
}

#define INFANTRY_AMMO_NEAR			50.0f
#define INFANTRY_AMMO_FAR				100.0f
#define COMMAND_TIMEOUT					480.0f		// 8 min

void AIUnit::CheckAmmo()
{
	ResourceState state = GetAmmoState();
	if (state == RSNormal) return;

	const AITargetInfo *target = CheckAmmo(state);

	if (target)
	{
		bool channelCenter = false;
		EntityAI *veh = target->_idExact;
		if (veh)
		{
			AIUnit *unit = veh->CommanderUnit();
			if (unit && unit->GetLifeState() == AIUnit::LSAlive)
				channelCenter = unit->GetGroup() != GetGroup();
		}

		Command cmd;
		cmd._message = Command::Rearm;
		cmd._destination = target->_realPos;
		cmd._target = target->_idExact;
		cmd._time = Glob.time + COMMAND_TIMEOUT;
		GetGroup()->SendAutoCommandToUnit(cmd, this, true, channelCenter);
	}
}

const AITargetInfo *AIUnit::CheckAmmo(ResourceState state)
{
	float maxDist;
	if (state == RSCritical) maxDist = INFANTRY_AMMO_FAR;
	else maxDist = INFANTRY_AMMO_NEAR;
	float maxDist2 = Square(maxDist);

	// which magazines we needed
	const MuzzleType *muzzle1 = NULL;
	const MuzzleType *muzzle2 = NULL;
	int slots1 = 0, slots2 = 0, slots3 = 0;
	CheckAmmo(muzzle1, muzzle2, slots1, slots2, slots3);
	if (slots1 == 0 && slots2 == 0 && slots3 == 0) return NULL; // no empty slots

	// check candidates
	float maxCoef = 0;
	const AITargetInfo *target = NULL;

	for (int i=0; i<GetGroup()->GetCenter()->NTargets(); i++)
	{
		const AITargetInfo &info = GetGroup()->GetCenter()->GetTarget(i);
//		if (info._destroyed) continue;
		VehicleSupply *veh = dyn_cast<VehicleSupply, Object>(info._idExact);
		if (!veh) continue;
		float dist2 = Position().DistanceXZ2(info._realPos);
		if (dist2 > maxDist2) continue;
//		if (GetCenter()->GetExposurePessimistic(info._realPos)>0 ) continue;
		saturateMax(dist2, 1);
		float coef = 0;
		if (info._type->IsKindOf(GWorld->Preloaded(VTypeMan)))
		{
			if (!veh->IsDammageDestroyed()) continue;
			// check magazines
			int slots1Wanted = slots1, slots2Wanted = slots2, slots3Wanted = slots3;
			for (int j=0; j<veh->NMagazines(); j++)
			{
				const Magazine *magazine = veh->GetMagazine(j);
				if (!magazine) continue;
				if (magazine->_ammo == 0) continue;
				const MagazineType *type = magazine->_type;
				int slots = GetItemSlotsCount(type->_magazineType);
				if (muzzle1)
					for (int k=0; k<muzzle1->_magazines.Size(); k++)
					{
						if (muzzle1->_magazines[k] == type)
						{
							if (slots <= slots1Wanted)
							{
								slots1Wanted -= slots;
								coef += 4.0 * slots;
							}
							goto NextMagazine;
						}
					}
				if (muzzle2)
					for (int k=0; k<muzzle2->_magazines.Size(); k++)
					{
						if (muzzle2->_magazines[k] == type)
						{
							if (slots <= slots2Wanted)
							{
								slots2Wanted -= slots;
								coef += 2.0 * slots;
							}
							goto NextMagazine;
						}
					}
				if (GetPerson()->IsMagazineUsable(type))
				{
					if (slots <= slots3Wanted)
					{
						slots3Wanted -= slots;
						coef += 1.0 * slots;
					}
				}
NextMagazine:
				;
			}
		}
		else
		{
			if (veh->IsDammageDestroyed()) continue;
			// check magazine cargo
			// TODO: join with check magazines
			int slots1Wanted = slots1, slots2Wanted = slots2, slots3Wanted = slots3;
			for (int j=0; j<veh->GetMagazineCargoSize(); j++)
			{
				const Magazine *magazine = veh->GetMagazineCargo(j);
				if (!magazine) continue;
				if (magazine->_ammo == 0) continue;
				const MagazineType *type = magazine->_type;
				int slots = GetItemSlotsCount(type->_magazineType);
				if (muzzle1)
					for (int k=0; k<muzzle1->_magazines.Size(); k++)
					{
						if (muzzle1->_magazines[k] == type)
						{
							if (slots <= slots1Wanted)
							{
								slots1Wanted -= slots;
								coef += 4.0 * slots;
							}
							goto NextMagazineCargo;
						}
					}
				if (muzzle2)
					for (int k=0; k<muzzle2->_magazines.Size(); k++)
					{
						if (muzzle2->_magazines[k] == type)
						{
							if (slots <= slots2Wanted)
							{
								slots2Wanted -= slots;
								coef += 2.0 * slots;
							}
							goto NextMagazineCargo;
						}
					}
				if (GetPerson()->IsMagazineUsable(type))
				{
					if (slots <= slots3Wanted)
					{
						slots3Wanted -= slots;
						coef += 1.0 * slots;
					}
				}
NextMagazineCargo:
				;
			}
		}
		coef *= InvSqrt(dist2);
		if (coef > maxCoef)
		{
			maxCoef = coef;
			target = &info;
		}
	}
	return target;
}

float AIUnit::GetFeeling() const
{
	int x = toIntFloor(Position().X() * InvLandGrid);
	int z = toIntFloor(Position().Z() * InvLandGrid);
	if( !GetGroup() || !GetGroup()->GetCenter() )
	{
		Fail("AI structure corrputed.");
		return 1;
	}
	float feeling = 0.0001 * GetGroup()->GetCenter()->GetExposureOptimistic(x, z);
	saturateMin(feeling, 1);
	return feeling;
}

void AIUnit::Disclose( bool discloseGroup )
{
	// no effect on player group
	if (IsPlayer()) return;
	if (!IsLocal()) return;

	AIGroup *grp = GetGroup();
	if (!grp) return;

	if (!grp->GetFlee())
	{
		// unit was disclosed - do something
		SetDanger();
		if (!grp->IsAnyPlayerGroup())
		{
			if( discloseGroup ) grp->Disclose(this);
			if (_semaphore!=SemaphoreBlue)
			{
				_semaphore = ApplyOpenFire(_semaphore, OFSOpenFire);
			}
		}
	}
	else
		SetDanger(GRandGen.PlusMinus(1.0f, 0.2f));
}

AIGroup* AIUnit::GetGroup() const
{
	if (!_subgroup) return NULL;
	return _subgroup->GetGroup();
}

void AIUnit::RemoveFromSubgroup()
{
	AISubgroup *subgroup = _subgroup;
	_subgroup = NULL;
	if (subgroup) subgroup->UnitRemoved(this);
}

/*!
\patch 1.43 Date 1/25/2002 by Ondra
- Fixed: MP: Player only partially removed from group after respawning to sea-gull.
This caused corrupted group list drawing and repeating "xxx is down" message
after "Report status" was issued.
*/

void AIUnit::RemoveFromGroup()
{
	Ref<AIUnit> unit = this;
	AIGroup *group = GetGroup();
	if (group)
	{
		if (!group->IsLocal()) return;
		#if _ENABLE_REPORT
		if (!IsLocal())
		{
			LogF
			(
				"Non-local unit %s removed from group",
				(const char *)GetDebugName()
			);
		}
		#endif
		group->UnitRemoved(this);
		_subgroup = NULL;
		_id = 0;
	}
}

void AIUnit::ForceRemoveFromGroup()
{
	Ref<AIUnit> unit = this;
	AIGroup *group = GetGroup();
	if (group)
	{
		group->UnitRemoved(this);
		_subgroup = NULL;
		_id = 0;
	}
}

void AIUnit::IssueGetOut()
{
	if (IsFreeSoldier())
		return;

	Assert(_inVehicle); // !IsFreeSoldier
	Person *driverMan = _inVehicle->Driver();
	AIUnit *driver = driverMan ? driverMan->Brain() : NULL;
	if( driver ) _inVehicle->WaitForGetOut(this);
	else
	{
		_inVehicle->GetOutStarted(this);
	}
}

CameraType ValidateCamera(CameraType cam)
{
	if (cam==CamGunner) return CamInternal;
	return cam;
}

void MoveToGetInPos(AIUnit *unit, Transport &veh)
{
	Vector3 pos = veh.GetUnitGetInPos(unit);
	pos[1] = GLandscape->RoadSurfaceYAboveWater(pos[0], pos[2]);
	Vector3 dir = veh.Position() - pos;
	dir[1] = 0;
	Matrix4 trans;
	trans.SetUpAndDirection(VUp, dir);
	trans.SetPosition(pos);
	unit->GetPerson()->Move(trans);
}

/*!
\patch 1.85 Date 9/18/2002 by Jirka
- Fixed: Allow captive be in vehicle with enemies
*/

static bool AvoidBeInWith(AICenter *center, Person *person)
{
	if (!person) return false;
	if (!center->IsEnemy(person->GetTargetSide())) return false;
	if (person->Brain() && person->Brain()->GetCaptive()) return false;

	return true;
}

bool AIUnit::ProcessGetIn(Transport &veh)
{
	if (!IsLocal()) return false;
	if (!IsFreeSoldier()) return false;
	if (IsPlayer() && veh.GetLock() == LSLocked) return false;
	if (GetGroup()->IsPlayerGroup() && veh.GetLock() == LSLocked) return false;

	AIGroup *grp = GetGroup();
	Assert(grp);
	AICenter *center = grp->GetCenter();
	Assert(center);
	
	// avoid get in enemy vehicle
	if (AvoidBeInWith(center, veh.Commander()))
	{
		if (_vehicleAssigned == &veh)
		{
			UnassignVehicle();
			grp->UnassignVehicle(&veh);
		}
		return false;
	}
	if (AvoidBeInWith(center, veh.Driver()))
	{
		if (_vehicleAssigned == &veh)
		{
			UnassignVehicle();
			grp->UnassignVehicle(&veh);
		}
		return false;
	}
	if (AvoidBeInWith(center, veh.Gunner()))
	{
		if (_vehicleAssigned == &veh)
		{
			UnassignVehicle();
			grp->UnassignVehicle(&veh);
		}
		return false;
	}
	for (int i=0; i<veh.GetManCargo().Size(); i++)
	{
		Person *man = veh.GetManCargo()[i];
		if (AvoidBeInWith(center, man))
		{
			if (_vehicleAssigned == &veh)
			{
				UnassignVehicle();
				grp->UnassignVehicle(&veh);
			}
			return false;
		}
	}

	if
	(
		IsPlayer() && (veh.GetLock() == LSUnlocked || IsGroupLeader() || GWorld->GetMode() == GModeNetware)
	)
	{
		// can get into any vehicle - try to assign position
		if (!veh.QIsDriverIn() && veh.GetType()->HasDriver())
		{
			if (veh.QCanIGetIn(GetPerson()))
			{
				GetGroup()->AddVehicle(&veh);
				AssignAsDriver(&veh);
				AllowGetIn(true);
				OrderGetIn(true);
			}
			else
			{
				// cannot get in as driver
				return false;
			}
		}
		else
		{
			Assert(veh.GetGroupAssigned()); // driver is in
			bool assigned = GetGroup() == veh.GetGroupAssigned();
			// if !assigned - can get in cargo only
			if (assigned && veh.GetType()->HasCommander() && !veh.QIsCommanderIn())
			{
				if (veh.QCanIGetInCommander(GetPerson()))
				{
					AssignAsCommander(&veh);
					AllowGetIn(true);
					OrderGetIn(true);
				}
				else
				{
					// cannot get in as driver
					return false;
				}
			}
			else if (assigned && veh.GetType()->HasGunner() && !veh.QIsGunnerIn())
			{
				if (veh.QCanIGetInGunner(GetPerson()))
				{
					AssignAsGunner(&veh);
					AllowGetIn(true);
					OrderGetIn(true);
				}
				else
				{
					// cannot get in as driver
					return false;
				}
			}
			else if (veh.GetFreeManCargo() > 0)
			{
				if (veh.QCanIGetInCargo(GetPerson()))
				{
					AssignAsCargo(&veh);
					AllowGetIn(true);
					OrderGetIn(true);
				}
				else
				{
					// cannot get in as cargo
					return false;
				}
			}
			else
			{
				// vehicle is full
				return false;
			}
		}
	}

	if (VehicleAssigned() == &veh)
	{
		// move soldier to get in position
		MoveToGetInPos(this, veh);

		UIActionType pos = ATGetInCargo;
		if (veh.GetDriverAssigned() == this) pos = ATGetInDriver;
		else if (veh.GetCommanderAssigned() == this) pos = ATGetInCommander;
		else if (veh.GetGunnerAssigned() == this) pos = ATGetInGunner;

		// play animation
		ActionContextBase *CreateGetInActionContext(Transport *veh, UIActionType pos);
		ActionContextBase *context = CreateGetInActionContext(&veh, pos);
		context->function = MFGetIn;
		GetPerson()->PlayAction(veh.Type()->GetGetInAction(), context);
		return true;
	}
//		return ProcessGetIn2();
	else
		return false;
}

static void JoinSubgroups(AISubgroup *subgrp1, AISubgroup *subgrp2)
{
	if (!subgrp2 || subgrp2 == subgrp1) return;
	AIGroup *grp = subgrp1->GetGroup();
	if (subgrp2->GetGroup() != grp) return;

	if (subgrp2 == grp->MainSubgroup())
		subgrp1->JoinToSubgroup(subgrp2);
	else
		subgrp2->JoinToSubgroup(subgrp1);
}

/*!
\patch 1.34 Date 12/03/2001 by Ondra
- Fixed: Occasional invalid memory access during getting/in out of vehicle.
*/

static void JoinSubgroups(Transport *veh)
{
	AIUnit *commander = veh->CommanderUnit();
	if (!commander) return;

	AISubgroup *subgrp = commander->GetSubgroup();
	if (!subgrp) return;

	// note: JoinSubgroups may join former commanders subgroup
	AIUnit *unit = veh->DriverBrain();
	if (unit)
	{
		JoinSubgroups(subgrp, unit->GetSubgroup());
		subgrp = commander->GetSubgroup();
	}
	unit = veh->CommanderBrain();
	if (unit)
	{
		JoinSubgroups(subgrp, unit->GetSubgroup());
		subgrp = commander->GetSubgroup();
	}
	unit = veh->GunnerBrain();
	if (unit)
	{
		JoinSubgroups(subgrp, unit->GetSubgroup());
		subgrp = commander->GetSubgroup();
	}
	const ManCargo &cargo = veh->GetManCargo();
	for (int i=0; i<cargo.Size(); i++)
	{
		Person *man = cargo[i];
		if (!man) continue;
		AIUnit *unit = man->Brain();
		if (unit)
		{
			JoinSubgroups(subgrp, unit->GetSubgroup());
			subgrp = commander->GetSubgroup();
		}
	}
}

// Macrovision CD Protection
void __cdecl CDPGunnerGetIn(Transport *veh, AIUnit *unit);
CDAPFN_DECLARE_GLOBAL(CDPGunnerGetIn, CDAPFN_OVERHEAD_L2, CDAPFN_CONSTRAINT_NONE);

/*!
	\patch_internal 1.03 Date 7/12/2001 by Jirka - changed placement of Czech CD Protection
	- removed: Get in gunner position (CDPGunnerGetIn)
	- removed: Heal (CDPHeal)
	- added: Beginning of mission (DisplayMission::DisplayMission)
	- added: Saving (World::SaveBin)
*/
void __cdecl CDPGunnerGetIn(Transport *veh, AIUnit *unit)
{
	Person *person = unit->GetPerson();

	if (veh->IsLocal())
	{
		veh->GetInFinished(unit);
		veh->GetInGunner(person);
	}
	else
		GetNetworkManager().AskForGetIn(person, veh, GIPGunner);
	if (!veh->CanCancelStop())
	{
		veh->UpdateStopTimeout();
		Verify(unit->SetState(AIUnit::Stopped));
	}

	// join subgroups
	JoinSubgroups(veh);

	// big warning: subgrp may change now

	// avoid leader in cargo
	AISubgroup *subgrp = unit->GetSubgroup();

	if (!subgrp->Leader()->IsUnit())
		subgrp->SelectLeader();

	// fixed - remote getin can fail - do not switch camera yet (will be switched later)
	if (GLOB_WORLD->FocusOn() == unit && veh->IsLocal())
		GLOB_WORLD->SwitchCameraTo(veh, ValidateCamera(GLOB_WORLD->GetCameraType()));
	subgrp->RefreshPlan();

	CDAPFN_ENDMARK(CDPGunnerGetIn);

	// Czech CD Protection removed
/*
#if _CZECH
	PerformRandomRoxxeTest_000(CDDrive);
#endif
*/
}

/*!
	\patch 1.01 Date 06/11/2001 by Jirka
	- Fixed: respawn in MP - problems if player killed during get in animation
	- do not continue with get in if unit is dead
	- camera can switch to dead body
	\patch 1.02 Date 07/12/2001 by Jirka
	- Fixed: getin in MP - two players gets in concurrently
	- camera has switched to vehicle on both players (but only one gets in)
*/

bool AIUnit::ProcessGetIn2(Transport *veh, UIActionType pos)
{
	// Transport *veh = VehicleAssigned();
	if (!veh) return false;

	// fixed - do not continue with get in if unit is dead
	if (GetPerson()->IsDammageDestroyed()) return false;

	Assert(veh->IsLocal());	// warning: BUG
/*
Person *person = GetPerson();
RptF
(
	"Processing get in: %s (%s, id %d:%d) into %s (%s, id %d:%d)",
	(const char *)person->GetDebugName(), person->IsLocal() ? "LOCAL" : "REMOTE",
	person->GetNetworkId().creator, person->GetNetworkId().id,
	(const char *)veh->GetDebugName(), veh->IsLocal() ? "LOCAL" : "REMOTE",
	veh->GetNetworkId().creator, veh->GetNetworkId().id
);
*/
	AIGroup *grp = GetGroup();
	Assert(grp);
	AICenter *center = grp->GetCenter();
	Assert(center);
	
	// avoid get in enemy vehicle
	if (AvoidBeInWith(center, veh->Commander()))
	{
		if (_vehicleAssigned == veh)
		{
			UnassignVehicle();
			grp->UnassignVehicle(veh);
		}
		return false;
	}
	if (AvoidBeInWith(center, veh->Driver()))
	{
		if (_vehicleAssigned == veh)
		{
			UnassignVehicle();
			grp->UnassignVehicle(veh);
		}
		return false;
	}
	if (AvoidBeInWith(center, veh->Gunner()))
	{
		if (_vehicleAssigned == veh)
		{
			UnassignVehicle();
			grp->UnassignVehicle(veh);
		}
		return false;
	}
	for (int i=0; i<veh->GetManCargo().Size(); i++)
	{
		Person *man = veh->GetManCargo()[i];
		if (AvoidBeInWith(center, man))
		{
			if (_vehicleAssigned == veh)
			{
				UnassignVehicle();
				grp->UnassignVehicle(veh);
			}
			return false;
		}
	}

	switch (pos)
	{
	// if (veh->GetDriverAssigned() == this)
	case ATGetInDriver:
		{
			if (veh->QCanIGetIn(GetPerson()))
			{
				if (veh->IsLocal())
				{
					veh->GetInFinished(this);
					veh->GetInDriver(GetPerson());
				}
				else
					GetNetworkManager().AskForGetIn(GetPerson(), veh, GIPDriver);
				GetGroup()->CalculateMaximalStrength();
				if (!veh->CanCancelStop())
	/*
				(
					veh->WhoIsGettingIn().Count() > 0 ||
					veh->WhoIsGettingOut().Count() > 0
				)
	*/
				{
					veh->UpdateStopTimeout();
					Verify(SetState(Stopped));
				}

				// join subgroups
				JoinSubgroups(veh);
	/*
				const ManCargo &cargo = veh->GetManCargo();
				for (int i=0; i<cargo.Size(); i++)
				{
					Person *man = cargo[i];
					if (!man) continue;
					AIUnit *unit = man->Brain();
					if (!unit) continue;
					if (unit->GetGroup() == GetGroup())
					{
						if (unit->GetSubgroup() != GetSubgroup())
						{
							if (unit->GetSubgroup() == GetGroup()->MainSubgroup())
							{
								GetSubgroup()->JoinToSubgroup(unit->GetSubgroup());
							}
							else
							{
								unit->GetSubgroup()->JoinToSubgroup(GetSubgroup());
							}
						}
					}
				}
	*/
				// avoid leader in cargo
				if (!GetSubgroup()->Leader()->IsUnit())
					GetSubgroup()->SelectLeader();
				
				// fixed - remote getin can fail - do not switch camera yet (will be switched later)
				if (GLOB_WORLD->FocusOn() == this && veh->IsLocal())
					GLOB_WORLD->SwitchCameraTo(veh, ValidateCamera(GLOB_WORLD->GetCameraType()));
				GetSubgroup()->RefreshPlan();
				return true;
			}
			else
			{
				// cannot get in as driver
				return false;
			}
		}
//		else if (veh->GetGunnerAssigned() == this)
	case ATGetInGunner:
		{
			if (veh->QCanIGetInGunner(GetPerson()))
			{
				CDPGunnerGetIn(veh, this);
				return true;
			}
			else
			{
				// cannot get in as gunner
				return false;
			}
		}
//		else if (veh->GetCommanderAssigned() == this)
	case ATGetInCommander:
		{
			if (veh->QCanIGetInCommander(GetPerson()))
			{
				if (veh->IsLocal())
				{
					veh->GetInFinished(this);
					veh->GetInCommander(GetPerson());
				}
				else
					GetNetworkManager().AskForGetIn(GetPerson(), veh, GIPCommander);
				if (!veh->CanCancelStop())
	/*
				if
				(
					veh->WhoIsGettingIn().Count() > 0 ||
					veh->WhoIsGettingOut().Count() > 0
				)
	*/
				{
					veh->UpdateStopTimeout();
					Verify(SetState(Stopped));
				}

				// join subgroups
				JoinSubgroups(veh);

				// avoid leader in cargo
				if (!GetSubgroup()->Leader()->IsUnit())
					GetSubgroup()->SelectLeader();

				// fixed - remote getin can fail - do not switch camera yet (will be switched later)
				if (GLOB_WORLD->FocusOn() == this && veh->IsLocal())
					GLOB_WORLD->SwitchCameraTo(veh, ValidateCamera(GLOB_WORLD->GetCameraType()));
				GetSubgroup()->RefreshPlan();
				return true;
			}
			else
			{
				// cannot get in as commander
				return false;
			}
		}
//		else
	default:
		Assert(pos == ATGetInCargo);
		{
			if (veh->QCanIGetInCargo(GetPerson()))
			{
				if (veh->IsLocal())
				{
					veh->GetInFinished(this);
					veh->GetInCargo(GetPerson());
				}
				else
					GetNetworkManager().AskForGetIn(GetPerson(), veh, GIPCargo);
				SetState(InCargo);

				// join subgroups
				JoinSubgroups(veh);

				// avoid leader in cargo
				if (!GetSubgroup()->Leader()->IsUnit())
					GetSubgroup()->SelectLeader();

				// fixed - remote getin can fail - do not switch camera yet (will be switched later)
				if (GLOB_WORLD->FocusOn() == this && veh->IsLocal())
					GLOB_WORLD->SwitchCameraTo(veh, ValidateCamera(GLOB_WORLD->GetCameraType()));
				GetSubgroup()->RefreshPlan();
				return true;
			}
			else
			{
				// cannot get in as cargo
				return false;
			}
		}
	}
}

/*!
\patch 1.24 Date 9/20/2001 by Ondra
- New: Ejection from plane is real ejection, not only jumping out.
\patch 1.27 Date 10/09/2001 by Ondra
- Fixed: MP: Ejection on client problems fixed. This could also cause player duplication.
*/

void AIUnit::DoGetOut(Transport *veh, bool parachute)
{
	Assert(veh->IsLocal()); // warning: BUG

	bool isFocused = (GLOB_WORLD->FocusOn() == this);

	Person *person = GetPerson();
	Vector3 ejectSpeed = person->Speed();
/*
RptF
(
	"Processing get out: %s (%s, id %d:%d) from %s (%s, id %d:%d)",
	(const char *)person->GetDebugName(), person->IsLocal() ? "LOCAL" : "REMOTE",
	person->GetNetworkId().creator, person->GetNetworkId().id,
	(const char *)veh->GetDebugName(), veh->IsLocal() ? "LOCAL" : "REMOTE",
	veh->GetNetworkId().creator, veh->GetNetworkId().id
);
*/

	// calculate get out position
	Vector3 pos = veh->GetUnitGetOutPos(this);
	Matrix4 transform;
	transform.SetPosition(pos);
	transform.SetUpAndDirection(VUp,pos-veh->Position());

	veh->GetOutFinished(this);

	if (veh->DriverBrain() == this)	// driver
	{
		veh->GetOutDriver(transform);
		if (IsLocal() && GetGroup())
			GetGroup()->CalculateMaximalStrength();
	}
	else if (veh->GunnerBrain() == this) // gunner
	{
		veh->GetOutGunner(transform);
	}
	else if (veh->CommanderBrain() == this) // commander
	{
		veh->GetOutCommander(transform);
	}
	else // cargo
	{
		veh->GetOutCargo(person, transform);

		// the only place where reset InCargo flag
		if (IsLocal())
		{
			Assert(_state == InCargo);
			_state = Wait;
			ClearOperativePlan();
		}
	}

	if (parachute)
	{
		const char *paraName = "ParachuteWest";
		TargetSide side = GetGroup()->GetCenter()->GetSide();
		if (side==TEast) paraName = "ParachuteEast";
		else if (side==TGuerrila) paraName = "ParachuteG";
		else if (side==TCivilian) paraName = "ParachuteC";

		Ref<EntityAI> ent = NewVehicle(paraName,"");
		Ref<Transport> getOutTo = dyn_cast<Transport,EntityAI>(ent);
		if (getOutTo)
		{
			getOutTo->SetAllowDammage(person->GetAllowDammage());
			if (getOutTo->Driver())
			{
				ErrF("%s already has a driver",(const char *)getOutTo->GetDebugName());
			}
			else
			{
				//Log("Getting out to %s",(const char *)getOutTo->GetDebugName());
				Vector3 aimCenter = getOutTo->GetShape()->AimingCenter();
				// aimCenter should be on pos position

				//getOutTo->AimingPosition();

				Matrix4 trans = person->Transform();
				trans.SetPosition(trans.Position()+getOutTo->Speed()*0.3f-aimCenter);

				getOutTo->SetSpeed(veh->Speed()+ejectSpeed);
				getOutTo->SetTransform(trans);

				GWorld->AddVehicle(getOutTo);
				GetNetworkManager().CreateVehicle(getOutTo, VLTVehicle, "", -1);

				DoAssert(getOutTo->IsLocal());

				getOutTo->GetInFinished(this);
				getOutTo->GetInDriver(person,false);
			}
		}

	}
	if (isFocused)
	{
		GLOB_WORLD->SwitchCameraTo(GetVehicle(), ValidateCamera(GLOB_WORLD->GetCameraType()));
	}
}

/*!
\patch 1.43 Date 1/28/2002 by Ondra
- Fixed: Dead AI unit blocked getting out for other AI units
in the same vehicle.
\patch_internal 1.85 Date 9/11/2002 by Jirka
- Fixed: usage of function AIUnit::CheckIfAliveInTransport sometimes leads to crash, unit can be destroyed inside
*/

void AIUnit::CheckIfAliveInTransport()
{
	if (GetLifeState()==AIUnit::LSAlive) return;
	Transport *veh = GetVehicleIn(); 
	if (!veh) return;
	AIGroup *grp = GetGroup();
	if (!grp) return;
	grp->SendIsDown(veh,this);
}

bool AIUnit::ProcessGetOut(bool parachute)
{
	if (!IsLocal()) return false;
	if (IsFreeSoldier()) return false;
	if (GetLifeState()!=AIUnit::LSAlive) return false;

	Transport *veh = GetVehicleIn(); 
	if (!veh) return false;

	if (veh->IsLocal())
		DoGetOut(veh,parachute);
	else
	{
		GetNetworkManager().AskForGetOut(GetPerson(), veh, parachute);

		// actions on unit's side
		if (veh->DriverBrain() == this)	// driver
		{
			if (GetGroup()) GetGroup()->CalculateMaximalStrength();
		}
		if (_state == InCargo)
		{
			_state = Wait;
			ClearOperativePlan();
		}
	}

	// rearm if possible
	if (GetSubgroup())
	{
		if (IsGroupLeader() && !IsSubgroupLeader())
			GetSubgroup()->SelectLeader(this);

		GetSubgroup()->RefreshPlan();
	}

	return true;
}

/*!
\patch 1.34 Date 12/6/2001 by Ondra
- Fixed: MP: Player messages "Injured", "Ammo low", "Fuel low" did not work
when player was not leader.
*/

void AIUnit::SendAnswer(Answer answer)
{
	if (!IsLocal()) return;
	AISubgroup *subgrp = GetSubgroup();
	
	if (!subgrp) return;
	AIGroup *grp = subgrp->GetGroup();
	if (!grp) return;
	if (!grp->IsLocal())
	{
		// note: this should be player that wants to transmit some information
		//  all AI units should be local where group is local
		DoAssert(IsAnyPlayer());
		if (IsAnyPlayer())
		{
			// we need to transmit the message to the remote group
			// do two things
			// 1) transmit it on the radio
			// 2) send answer accross network (done in Transmitted())
			grp->GetRadio().Transmit
			(
				new RadioMessageUnitAnswer(this, subgrp, answer),
				grp->GetCenter()->GetLanguage()
			);
		}
		return; // no simulation for remote groups
	}

	if (answer == AI::UnitDestroyed)
	{
		// subgroup does not know it yet (until someone will report it)
		// check: dead or deadinrespawn
		if (GetLifeState()!=LSDead)
		{
			SetLifeState(LSDead);
			GetNetworkManager().UpdateObject(GetPerson());
			GetNetworkManager().UpdateObject(this);
			if (grp->Leader()==this)
			{
				// if we are leader, units expect us to communicate
				grp->SetReportBeforeTime(this,Glob.time+30);
			}

			RString identity = GetPerson()->GetInfo()._identityContext;
			if (identity.GetLength() > 0)
			{
				// check if unit is from player group
				Person *player = GWorld->GetRealPlayer();
				AIUnit *playerUnit = player ? player->Brain() : NULL;
				AIGroup *playerGroup = playerUnit ? playerUnit->GetGroup() : NULL;
				void AddDeadIdentity(RString identity);
				if (playerGroup == grp) AddDeadIdentity(identity);
			}
		}

		//subgrp->ReceiveAnswer(this, answer);
		return;
	}


	if (answer == AI::StepTimeOut)
	{
		if (_state == Stopping)
		{
			// Get out cannot be processed
			Transport *veh = dyn_cast<Transport>(GetVehicle());
			Assert(veh);
			if (veh)
			{
				veh->WhoIsGettingOut().Clear();
			}
			else
				Verify(SetState(Wait)); // TODO: BUG
		}
		else if (_state == AIUnit::Busy)
		{
			Verify(SetState(Replan));
		}
		return;
	}
	else if (answer == AI::StepCompleted)
	{
		if (_state == Stopping)
		{
			SetState(Stopped);
			Transport *veh = dyn_cast<Transport>(GetVehicle());
			if (veh)
			{
				veh->LandFinished();

				veh->WhoIsGettingOut().Compact();
				int n = veh->WhoIsGettingOut().Size();
				AIUnit *unit = NULL;
				if (n > 0)
				{
					for (int i=0; i<n; i++)
					{
						AIUnit *u = veh->WhoIsGettingOut()[i];
						if (u->IsInCargo())
						{
							unit = u;
							break;
						}
					}
					if (!unit) for (int i=0; i<n; i++)
					{
						AIUnit *u = veh->WhoIsGettingOut()[i];
						if (u->IsGunner())
						{
							unit = u;
							break;
						}
					}
					if (!unit) for (int i=0; i<n; i++)
					{
						AIUnit *u = veh->WhoIsGettingOut()[i];
						if (u->IsCommander())
						{
							unit = u;
							break;
						}
					}
					if (!unit)
						unit = veh->WhoIsGettingOut()[0];
					Assert(unit);
					if (unit)
					{
						veh->SetGetOutTime(Glob.time + 2.0);
						// FIX
						unit->AddRef();
						unit->CheckIfAliveInTransport(); // unit can be destroyed inside
						unit->ProcessGetOut(false);
						unit->Release();
					}
				}
			}
		}
		else if (_state == AIUnit::Busy)
		{
			if (_lastPlan)
			{
				EntityAI *veh = GetVehicle();
				float precision = veh->GetPrecision();
				if (_wantedPosition.Distance(_plannedPosition) > Square(precision))
				{
					ForceReplan();
				}
				else
				{
					Vector3 pos = _path[_path.Size() - 1]._pos;
					if (pos.DistanceXZ2(_expPosition) > Square(1))
					{
						// try to find better path
						_iter = 0;
						Verify(SetState(Wait));
					}
					else
					{
						Verify(SetState(Completed));
					}
				}
			}
			else
			{
				Verify(SetState(Replan));
			}
		}
		return;
	}

#if LOG_COMM
	Log("Send answer: Unit %s: Answer %d",
	(const char *)GetDebugName(), answer);
#endif

	if (grp->NUnits()>1)
	{
		// transmit to radio only where there is somebody to listen
		grp->GetRadio().Transmit
		(
			new RadioMessageUnitAnswer(this, subgrp, answer),
			grp->GetCenter()->GetLanguage()
		);
	}
	else
	{
		subgrp->ReceiveAnswer(this,answer);
	}
}

void AIUnit::SetWatchDirection( Vector3Val dir )
{
	_watchDirHead = _watchDir = dir;
	_watchMode = WMDir;
	_watchDirSet = Glob.time;
	_targetAssigned = NULL;
}

void AIUnit::SetWatchAround()
{
	_watchDirHead = _watchDir = GetVehicle()->Direction();
	_watchMode = WMAround;
	_watchDirSet = Glob.time;
	_targetAssigned = NULL;
}

void AIUnit::SetNoWatch()
{
	_watchTgt = NULL, _watchMode = WMNo;
	_targetAssigned = NULL;
}
void AIUnit::SetWatchPosition( Vector3Val pos )
{
	_watchPos = pos,_watchMode=WMPos;
	_targetAssigned = NULL;
}
void AIUnit::SetWatchTarget( Target *tgt )
{
	_watchTgt = tgt,_watchMode=WMTgt;
}

float HowMuchInteresting(AIUnit *unit, const Target *tgt)
{
	EntityAI *veh = unit->GetVehicle();
	Vector3Val unitDir = veh->Direction();
	Vector3Val unitPos = veh->Position();
	AIGroup *grp = unit->GetGroup();
	AICenter *cnt = grp->GetCenter();
	float interesting = 1;
	// we get some points when unknown
	if (tgt->side==TSideUnknown) interesting += 10;
	// we get many point for being enemy
	if (cnt->IsEnemy(tgt->side)) interesting += 50;
	// we get some points for being human
	if (tgt->type->IsKindOf(GWorld->Preloaded(VTypeMan))) interesting += 10;
	// we get some points for moving
	EntityAI *entity = tgt->idExact;
	if (entity)
	{
		// we get many points for firing, screaming and moving
		// this is all calculated in audibility - making noise
		// if target is visible, we should also check VisibleMovement()
		// we check it always - hopefully it will not matter too much
		interesting += entity->Audible()*5;
		interesting += entity->VisibleMovement();
		// and also for speaking
		interesting += entity->GetSpeaking()*30;
	}
	// we loose a lot for being out of natural focus area
	Vector3 tgtDir = tgt->position-unitPos;
	float cosAlpha = tgtDir*unitDir*tgtDir.InvSize();
	float focus = floatMax(cosAlpha,0.2);
	return interesting * focus;
}

Target *SelectInterestingTarget(AIUnit *unit, Target *thn)
{
	Vector3Val uPos = unit->Position();
	AIGroup *grp = unit->GetGroup();
	if (!grp) return NULL;
	if (!grp->GetCenter()) return NULL;
	// find something known that is near
	const TargetList &list = grp->GetTargetList();
	// never select target less interesting than certain limit
	float thnInteresting = 20;
	float thnDist = 50;
	if (thn)
	{
		// increase old target interest to maintain some stability
		thnInteresting = HowMuchInteresting(unit,thn)*3.0f;
		thnDist = uPos.Distance(thn->position);
	}
	for (int i=0; i<list.Size(); i++)
	{
		Target *tgt = list[i];
		if (tgt->idExact==unit->GetPerson()) continue;
		if (tgt->idExact==unit->GetVehicleIn()) continue;
		if (!tgt->IsKnownBy(unit)) continue;
		if (tgt->type->IsKindOf(GWorld->Preloaded(VTypeStatic))) continue;
		if (tgt->idExact && tgt->idExact->Static()) continue;
		
		float tgtDist = tgt->position.Distance(uPos);
		float tgtInteresting = HowMuchInteresting(unit,tgt);
		if (tgtInteresting*thnDist>thnInteresting*tgtDist)
		{
			thn = tgt;
			thnDist = tgtDist;
			thnInteresting = tgtInteresting;
		}
	}
	return thn;
}

/*!
\patch 1.52 Date 4/20/2002 by Ondra
- New: All men are now interested in what is happening around them.
*/

void AIUnit::SetWatch()
{
	// cosider engaging target
	AIGroup *grp = GetGroup();
	if 
	(
		!IsPlayer() &&
		_targetAssigned && 
		!grp->CommandSent(this, Command::Attack) &&
		!grp->CommandSent(this, Command::AttackAndFire) &&
		!grp->CommandSent(this, Command::GetOut) &&
		!grp->CommandSent(this, Command::GetIn) &&
		(
			_targetAssigned==_targetEngage || !IsKeepingFormation()
		)
	)
	{
		EntityAI *veh = GetVehicle();
		FireResult result;
		Vector3 pos;
		if (veh->AttackThink(result,pos))
		{
			// check if we are ready to fire
			// if not, we probably will have to attack
			float ttl = GetTimeToLive();
			FireResult fResult;
			veh->WhatFireResult(fResult,*_targetAssigned,ttl);
			if
			(
				result.CleanSurplus()>fResult.CleanSurplus()*1.2+200
			)
			{
				// TODO: wait for a while
				// some good attack position found
				// issue auto attack command

				Command cmd;
				cmd._message = Command::Attack;
				cmd._targetE = _targetAssigned;
				cmd._destination = pos;

				// automatic attack command
				grp->NotifyAutoCommand(cmd, this);
			}
		}
	}

	// consider formation and watch mode
	switch (_watchMode)
	{
		case WMAround:
		{
			float time = Glob.time - _watchDirSet;
			Matrix3 rotY(MRotationY, time *((H_PI*2)/30));
			_watchDir = rotY * _watchDir;
			_watchDir[1] = 0;
			_watchDir.Normalize();
			_watchDirSet = Glob.time;
			_watchDirHead = _watchDir;
		}
		break;
		case WMPos:
		{
			_watchDir = _watchPos - GetVehicle()->CameraPosition();
			_watchDir.Normalize();
			_watchDirHead = _watchDir;
		}
		break;
		case WMTgt:
			if (_watchTgt)
			{
				_watchPos = _watchTgt->AimingPosition();
				_watchDir = _watchPos - GetVehicle()->CameraPosition();
				_watchDir.Normalize();
				_watchDirHead = _watchDir;
				break;
			}
		// fall trough from case WMTgt to case WMNo
		case WMNo:
		{
			// watch direction given by formation
			AISubgroup *subgrp = GetSubgroup();
			Vector3Val dir = subgrp->GetFormationDirection();
			float relAngle = _formationAngle;
			Matrix3 rot(MRotationY,-relAngle);
			_watchDirHead = _watchDir = rot * dir;
			// if auto targeting is disabled, we must not be curious
			// mission designer has full control over us
			if (IsSoldier() && (_disabledAI&DAAutoTarget)==0)
			{
				// there is no need to simulate this for units
				// that are far
				//EntityAI *body = GetVehicle();

				//if (GetPrec)
				// To considre: maybe combat units may be affected by this?
				// when soldier is idle, automatically select "interesting" targets
				Target *tgt = NULL;
				tgt = SelectInterestingTarget(this,_watchTgt);

				if (tgt)
				{
					// we see something interesting - watch it
					_watchTgt = tgt;
					_watchPos = tgt->AimingPosition();
					if (tgt->idExact)
					{
						// make correction for different aiming/camera position
						if (_watchPos.Distance2(tgt->idExact->AimingPosition())<0.5f)
						{
							_watchPos = tgt->idExact->CameraPosition();
						}
					}
					_watchDirHead = _watchPos - GetVehicle()->CameraPosition();
					_watchDirHead.Normalize();
				}
			}


		}
		break;
	}
}

void AIUnit::CheckResources()
{
	ResourceState state = GetHealthState();
	if (state == RSCritical)
	{
		if (_lastHealthState < RSCritical)
		{
			_healthCriticalTime = Glob.time + FIRST_REPORT_TIME;
		}
		else if (Glob.time > _healthCriticalTime)
		{
			_healthCriticalTime = Glob.time + REPEAT_REPORT_TIME;
			SendAnswer(HealthCritical);
		}
	}
	_lastHealthState = state;

	if (IsUnit())
	{
		state = GetFuelState();
		if (state == RSCritical)
		{
			if (_lastFuelState < RSCritical)
			{
				_fuelCriticalTime = Glob.time + FIRST_REPORT_TIME;
			}
			else if (Glob.time > _fuelCriticalTime)
			{
				_fuelCriticalTime = Glob.time + REPEAT_REPORT_TIME;
				SendAnswer(FuelCritical);
			}
		}
		else if (state == RSLow)
		{
			if (_lastFuelState < RSLow)
				SendAnswer(FuelLow);
		}
		_lastFuelState = state;

		state = GetArmorState();
		if (state == RSCritical)
		{
			if (_lastArmorState < RSCritical || Glob.time > _dammageCriticalTime)
			{
				_dammageCriticalTime = Glob.time + REPEAT_REPORT_TIME;
				SendAnswer(DammageCritical);
			}
		}
		_lastArmorState = state;

		state = GetAmmoState();
		if (state == RSCritical)
		{
			if (_lastAmmoState < RSCritical || Glob.time > _ammoCriticalTime)
			{
				_ammoCriticalTime = Glob.time + REPEAT_REPORT_TIME;
				SendAnswer(AmmoCritical);
			}
		}
		else if (state == RSLow)
		{
			if (_lastAmmoState < RSLow)
				SendAnswer(AmmoLow);
		}
		_lastAmmoState = state;
	}
}

void AIUnit::CheckGetOut()
{
	Transport *veh = _inVehicle;
	if
	(
		veh && (!veh->Driver() || !veh->Driver()->Brain())
		&& veh->GetGetOutTime() <= Glob.time
	)
	{
		veh->WhoIsGettingOut().Compact();
		int n = veh->WhoIsGettingOut().Size();
		for (int i=0; i<n; i++)
		{
			if (veh->WhoIsGettingOut()[i].GetLink() == this)
			{
				veh->SetGetOutTime(Glob.time + 2.0);
				// FIX
				AddRef();
				CheckIfAliveInTransport(); // unit can be destroyed inside
				ProcessGetOut(false);
				Release();
			}
		}
	}
}

void AIUnit::CheckGetInGetOut()
{
	Transport *veh = _inVehicle;
	if (veh)
	{
#if 0
		if (veh->GetGetInTimeout() <= Glob.time)
		{
LogF
(
"%.2f: Vehicle %s, get in timed out (%.2f)",
Glob.time.toFloat(),
(const char *)veh->GetDebugName(),
veh->GetGetInTimeout().toFloat()
);
		}
#endif
#if 0
		if (veh->WhoIsGettingIn().Count() <= 0)
		{
LogF
(
"%.2f: Vehicle %s, all units got in",
Glob.time.toFloat(),
(const char *)veh->GetDebugName()
);
		}
#endif
		if (veh->GetGetInTimeout() <= Glob.time || veh->WhoIsGettingIn().Count() <= 0)
		{
#if 0
LogF
(
"%.2f: Vehicle %s, wait for get out ended",
Glob.time.toFloat(),
(const char *)veh->GetDebugName()
);
#endif
			veh->SetGetInTimeout(Time(0));
			veh->WhoIsGettingIn().Clear();
		}
		veh->WhoIsGettingOut().Compact();
		int n = veh->WhoIsGettingOut().Size();
		if (n > 0 && Glob.time >= veh->GetGetOutTime())
		{
			AIUnit *unit = NULL;
			for (int i=0; i<n; i++)
			{
				AIUnit *u = veh->WhoIsGettingOut()[i];
				if (u->IsInCargo())
				{
					unit = u;
					break;
				}
			}
			if (!unit) for (int i=0; i<n; i++)
			{
				AIUnit *u = veh->WhoIsGettingOut()[i];
				if (u->IsGunner())
				{
					unit = u;
					break;
				}
			}
			if (!unit) for (int i=0; i<n; i++)
			{
				AIUnit *u = veh->WhoIsGettingOut()[i];
				if (u->IsCommander())
				{
					unit = u;
					break;
				}
			}
			if (!unit)
				unit = veh->WhoIsGettingOut()[0];
			Assert(unit);
			if (unit)
			{
				veh->SetGetOutTime( Glob.time + 2.0 );
				// FIX
				unit->AddRef();
				unit->CheckIfAliveInTransport(); // unit can be destroyed inside
				unit->ProcessGetOut(false);
				unit->Release();
			}
		}
		if( veh->CanCancelStop() )
		{
			SetState(Wait);	// valid pass Stopped -> Wait
		}
	}
	else
	{
		if (HasAI())
		{
			if (GetPerson()->CanCancelStop())
			{
				Verify(SetState(Wait)); // TODO: BUG
				// ?? Fail("Unit stopped");
			}
		}
	}
}

#if _ENABLE_AI

void AIUnit::OnStrategicPathFound()
{
	_noPath = false;
	_updatePath = false;
	_lastPlan = false;
	State state = GetState();
	if
	(
		state != Delay && state != InCargo &&
		state != Stopping && state != Stopped
	)
	Verify(SetState(Wait));
	_iter = 0;

	float totalCost = _planner->GetTotalCost();
	_completedTime = Glob.clock.GetTimeInYear() + totalCost * OneSecond;
}

void AIUnit::OnStrategicPathNotFound(bool update)
{
	_waitWithPlan = Glob.time + 10.0F;

	if (update)
	{
		#if LOG_POSITION_PROBL
			LogF("Did not found path in update.");
		#endif
		_completedTime = Glob.clock.GetTimeInYear() + OneDay; // set conditions for timeout
	}
	else
	{
		switch (_planningMode)
		{
		case LeaderPlanned:
			if (++_attemptPlan >= 3)
				GetSubgroup()->FailCommand();
			else
				GetSubgroup()->SendAnswer(AI::SubgroupDestinationUnreacheable);
			break;
		case FormationPlanned:
		case VehiclePlanned:
			// nothing to do
			break;
		default:
			Fail("Unexpected mode");
			break;
		}
	}
}
#endif //_ENABLE_AI

void AIUnit::SetWantedPosition(Vector3Par pos, PlanningMode mode, bool forceReplan)
{
#if DIAG_PLANNING
LogF
(
	"%.3f, %s: SetWantedPosition to %.0f, %.0f, mode %s %s",
	Glob.time.toFloat(), (const char *)GetDebugName(),
	pos.X(), pos.Z(), (const char *)FindEnumName(mode),
	forceReplan ? ", force replan" : ""
);
#endif

	_planningMode = mode;
	_wantedPosition = pos;
	_completedReceived = false;
// LogF("Unit %s: SetWantedPosition [%.0f, %.0f]", (const char *)GetDebugName(), pos.X(), pos.Z());
	if (mode != DoNotPlan) GetVehicle()->GoToStrategic(pos);

	switch (mode)
	{
	case DoNotPlan:
		// continue with plan
		if (forceReplan)
		{
			ClearStrategicPlan();
			ClearOperativePlan();
		}
		break;
	case LeaderPlanned:
	case VehiclePlanned:
//		if (_state == Completed) SetState(Wait);
	case FormationPlanned:
		if (!forceReplan)
		{
			float error2 = _plannedPosition.Distance2(_wantedPosition);
			float limit2 = Square(0.1) * Position().Distance2(_wantedPosition);
			forceReplan = error2 > limit2;
		}
	
		if (forceReplan)
		{
			_plannedPosition = _wantedPosition;
			if (mode != FormationPlanned) _noPath = true;
			RefreshStrategicPlan();
		}
		// else replan only when _wantedPosition differs from _plannedPosition enough
		break;
	case LeaderDirect:
		// force replan
		_plannedPosition = pos;
		_expPosition = pos;
		ClearStrategicPlan();
		ClearOperativePlan();
		break;
	}
}

#if _ENABLE_AI

void AIUnit::CreateStrategicPath(ThinkImportance prec)
{
	PROFILE_SCOPE(pthSt);

	Vector3Val dest = _plannedPosition;
	if (_noPath && _waitWithPlan <= Glob.time)
	{
#if LOG_THINK
		Log("  - Searching plan (strategic) to %.1f, %.1f (%d,%d).",
			dest.X(), dest.Z(), xe, ze);
#endif
		if (!_planner->IsSearching())
		{
			// begin new search
			if (!_planner->StartSearching(prec, GetVehicle(), Position(), dest))
				OnStrategicPathNotFound(false);
			else if (!_planner->IsSearching())
				OnStrategicPathFound();
		}
		if (_planner->IsSearching())
		{
			// continue with searching 
			if (!_planner->ProcessSearching())
				OnStrategicPathNotFound(false);
			else if (!_planner->IsSearching())
				OnStrategicPathFound();
		}
	}
	else if (_updatePath && _waitWithPlan <= Glob.time)
	{
#if LOG_THINK
		Log("  - Update plan (strategic) to %.1f, %.1f (%d,%d).",
			dest.X(), dest.Z(), xe, ze);
#endif
		if (!_planner->IsSearching())
		{
			// begin new search
			if (!_planner->StartSearching(prec, GetVehicle(), Position(), dest))
				OnStrategicPathNotFound(true);
			else if (!_planner->IsSearching())
				OnStrategicPathFound();
		}
		if (_planner->IsSearching())
		{
			// continue with searching 
			if (!_planner->ProcessSearching())
				OnStrategicPathNotFound(true);
			else if (!_planner->IsSearching())
				OnStrategicPathFound();
		}
	}
}

void AIUnit::OperPath(ThinkImportance prec)
{
	EntityAI *veh = GetVehicle();

	SetHouse(NULL, -1);
	Vector3Val pos = Position();
	float dist2;

	if
	(
		_state != Stopped &&
		_state != Stopping &&
		_state != Delay &&
		_state != InCargo
	)
	{
		if (!_noPath)
		{	// construct path by strategic plan
			int planIndex = _planner->FindBestIndex(pos);
			int minPlanIndex = planIndex;
			float precision = veh->GetPrecision();
			bool onRoad = GRoadNet->IsOnRoad
			(
				pos, precision * 1.2
			) != NULL;
			do
			{
				_lastPlan = _planner->GetPlanPosition(planIndex, _expPosition);

				dist2 = (_expPosition - pos).SquareSizeXZ();
				if (dist2 >= Square(1.1 * precision))
				// do not force target under vehicle's precision
				{
					FieldPassing::Mode planMode = _planner->GetPlanMode(planIndex);
					if (!onRoad)
					{
						if (planMode == FieldPassing::MoveOnRoad)
							break;	// move onto road
					}
					else
					{
						if (planMode != FieldPassing::MoveOnRoad)
							break;	// move out of road
					}
				}
			}
			while
			(
				dist2 < Square(DIST_MIN_OPER) && ++planIndex < _planner->GetPlanSize()
			);

			if (_iter == 1)
			{
				if (planIndex > minPlanIndex)
				{
					planIndex--;
					_lastPlan = _planner->GetPlanPosition(planIndex, _expPosition);
				}
			}
			else if (_iter == 2)
			{
				if (planIndex < _planner->GetPlanSize() - 1)
				{
					planIndex++;
					_lastPlan = _planner->GetPlanPosition(planIndex, _expPosition);
				}
			}

			saturateMin(planIndex, _planner->GetPlanSize() - 1);

			if (_lastPlan)
			{
				if (prec <= LevelFastOperative)
				{
					SetMode(AIUnit::Exact);
				}
				else
				{
					SetMode(AIUnit::DirectExact);
				}
				Command *cmd = GetSubgroup()->GetCommand();
				if (cmd && cmd->_message == Command::Move && cmd->_target)
				{
					const IPaths *house = cmd->_target->GetIPaths();
					if( house && cmd->_target->GetShape()->FindPaths()>=0 )
					{
						SetHouse(cmd->_target, cmd->_param);
					}
				}
			}
			else
			{
				if (prec <= LevelFastOperative)
				{
					SetMode(AIUnit::Normal);
				}
				else
				{
					SetMode(AIUnit::DirectNormal);
				}
			}

			Verify(SetState(AIUnit::Init));
		}
	}
	else
	{
		_expPosition = pos;
		_lastPlan = false;
		// do not change state (Stopping, Stopped, InCargo, Delay)
	}
}

bool AIUnit::CreateOperativePath()
{
	EntityAI *veh = GetVehicle();

	// new attempt
	if (_state == Delay && _delay <= Glob.time)
	{
		Verify(SetState(Wait));
		return false;
	}

	if (_state != Init)
	{
		// check if current path is valid
		if (!VerifyPath())
		{
			if (IsSubgroupLeader())
			{
				Verify(SetState(Replan));
			}
			else
			{
				Verify(SetState(Wait));	// clear plan
			}
		}
		// do not plan
		return false;
	}

	PROFILE_SCOPE(pthOp);
	float combatHeight = veh->GetCombatHeight();
	if
	(
		veh->GetType()->IsKindOf(GLOB_WORLD->Preloaded(VTypeAir))
		|| veh->GetType()->IsKindOf(GLOB_WORLD->Preloaded(VTypeShip))
	)
	{
		if (_mode == Exact)
			_mode = DirectExact;
		else if (_mode == Normal)
			_mode = DirectNormal;
	}
	if (_mode == DirectExact || _mode == DirectNormal)
	{
		// creates path from Position() to _expPosition if none in _planner
		CopyPath(*_planner);
		Verify(SetState(Busy));
		return false;
	}
	else
	{
		float dist2=(Position() - _expPosition).SquareSizeXZ();
		if (dist2 < Square(DIST_NOT_SEARCH))
		{
			if (_lastPlan)
			{
				float precision = veh->GetPrecision();
				if (_wantedPosition.Distance(_plannedPosition) > Square(precision))
					ForceReplan();
				else
				{
					Verify(SetState(Completed));
				}
			}
			else
			{
				Verify(SetState(Replan));
			}
			return false;
		}
		if (!CreatePath(Position(), _expPosition))
		{
LogF("%s: path not found - iteration %d", (const char *)GetDebugName(), _iter);
			if (++_iter > 2)
			{
				LogF("  fail command");
				GetSubgroup()->FailCommand();
				_iter = 0;
			}
			else
			{
				_delay = Glob.time + 5.0;
				Verify(SetState(Delay));
			}
			return true;
		}
		_iter = 0;
	
		// add strategic plan to path
		int index = -1;
		int n = _planner->GetPlanSize();
		for (int i=0; i<n; i++)
		{
			Point3 pos;
			_planner->GetPlanPosition(i, pos);
			if ((pos - _expPosition).SquareSizeXZ() < 1.0f)
			{
				index = i;
				break;
			}
		}
		if (index >= 0)
		{
			int j = _path.Size() - 1;
			float lastCost = _path[j]._cost;
			Point3 lastPos = GLandscape->PointOnSurface(_expPosition[0],combatHeight,_expPosition[2]);
			for (int i=index+1; i<n; i++)
			{
				Point3 pos;
				_planner->GetPlanPosition(i, pos);
				GeographyInfo geogr = _planner->GetGeography(i);
				float cost = veh->GetCost(geogr) * veh->GetFieldCost(geogr);
				if (cost >= GET_UNACCESSIBLE)
					cost = 1.0;
				cost *= (pos - lastPos).SizeXZ();
				lastCost += cost;
				lastPos = GLandscape->PointOnSurface(pos[0],combatHeight,pos[2]);
				j = _path.Add();
				_path[j]._pos = lastPos;
				_path[j]._cost = lastCost;
				//_path[j]._dir = 0xff;
			}
		}
		Verify(SetState(Busy));
		return true;
	}
}
#endif // _ENABLE_AI

/*!
\patch 1.01 Date 6/26/2001 by Ondra.
- Improved: AI takes NV goggles on/off.
\patch 1.50 Date 4/12/2002 by Ondra
- Fixed: Multiple AI units asked "Where are you" same time.
*/

bool AIUnit::Think(ThinkImportance prec)
{
	if (!IsLocal()) return false;

	if (GetLifeState()!=LSAlive) return false;
	PROFILE_SCOPE(aiUnt);
#if LOG_THINK
	Log("    Unit %s think.", (const char *)GetDebugName());
#endif

	EntityAI *veh = GetVehicle();
	// unassign target if not valid

	if (!_subgroup) return false;

	// TODO: move to separate function
	// find attack position if neccessary

	

	SetWatch();
	// some parts of AIUnit::Think may be executed less often

	if (Glob.time > _expensiveThinkTime)
	{
		ExpensiveThinkDone();
		if (!IsAnyPlayer() || Glob.config.IsEnabled(DTAutoSpot)) CheckResources();
		if (!IsSubgroupLeader() && IsUnit())
		{
			bool away = veh->IsAway();
			if (away)
			{
				if (!_isAway)
				{
					_awayTime = Glob.time + FIRST_AWAY_TIME;
				}
				else if (Glob.time > _awayTime)
				{
					if (!_subgroup->IsPlayerSubgroup() && veh->IsAway(1.5))
					{
						_awayTime = Glob.time + REPEAT_AWAY_TIME;
						AIGroup *grp = GetGroup();
						grp->GetRadio().Transmit
						(
							new RadioMessageReturnToFormation(_subgroup, this),
							grp->GetCenter()->GetLanguage()
						);
					}
					else if (!IsPlayer())
					{
						AIGroup *grp = GetGroup();
						grp->GetRadio().Transmit
						(
							new RadioMessageWhereAreYou(this, grp),
							grp->GetCenter()->GetLanguage()
						);
						// all units of the group should be marked not to ask this question
						// to avoid asking it multiple times
						for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
						{
							AIUnit *u = grp->UnitWithID(i+1);
							if (!u) continue;
							u->SetAwayTime
							(
								Glob.time + REPEAT_AWAY_TIME + GRandGen.RandomValue()*20
							);
						}
					}
				}
			}
			_isAway = away;
		}

		if( _targetAssigned )
		{
			if
			( 
				_targetAssigned->destroyed || _targetAssigned->vanished ||
				!veh->IsAbleToFire()
			)
			{
				AssignTarget(NULL);
			}
		}

		// FIX
		// consider if we should use nvg
		if (!IsPlayer())
		{
			Person *person = GetPerson();
			if (person->IsNVEnabled())
			{
				const LightSun *sun = GScene->MainLight();
				float night=sun->NightEffect();
				if (night>0)
				{
					// check light intensity
					float dark = 1.02f-sun->Ambient().Brightness()-sun->Diffuse().Brightness();
					saturateMin(night,dark);

					float thold = 0.5f + ID()*0.02f - GetAbility()*0.2f;

					person->SetNVWanted(night>thold);
				}
			}
			#if 0 //_ENABLE_REPORT
			int reloaded = veh->AutoReloadAll();
			if (reloaded>=0)
			{
				RptF
				(
					"Unit %s autoreloaded weapon %d",
					(const char *)GetDebugName(),reloaded
				);
			}
			#endif
		}

	}

	if (veh->PilotUnit() != this)
	{
		// Warning: unit can be destroyed inside, return immediatelly
		CheckGetOut();
		// create path only for pilot units
		return false;
	}

	// free soldier or driver
	if (!HasAI())
	{
		if
		(
			veh->Speed().SquareSize() < Square(0.5) &&
			veh->Position().Y() - GLandscape->RoadSurfaceYAboveWater
			(
				veh->Position().X(),
				veh->Position().Z()
			) < 4.0
		) CheckGetInGetOut();
		return false;
	}

#if _ENABLE_AI

	if (_state == Stopped)
	{
		// waiting for get in / get out
		CheckGetInGetOut();
		return false;
	}

	switch (_planningMode)
	{
	case DoNotPlan:
		// do not create new plan (if exist, can continue with the old one)
		if (_noPath) return false;
		else
		{
			DoAssert(IsSubgroupLeader());
//			DoAssert(IsUnit());

			// operative planning target
			if (_state == Wait || _state == Replan) OperPath(prec);

			// operative planning
			return CreateOperativePath();
		}
	case LeaderDirect:
		{
			DoAssert(IsSubgroupLeader());
			DoAssert(IsUnit());

			SetMode(AIUnit::DirectExact);
			Verify(SetState(AIUnit::Init));
			_iter = 0;
			_lastPlan = true;

			// operative planning
			return CreateOperativePath();
		}
	case LeaderPlanned:
	case FormationPlanned:
	case VehiclePlanned:
		{
			// check strategic target
			float error2 = _plannedPosition.Distance2(_wantedPosition);
			float limit2 = Square(0.1) * Position().Distance2(_wantedPosition);
			// replan if target position changed by 10% of total distance
			if (error2 > limit2)
			{
				_plannedPosition = _wantedPosition;
				_noPath = true;
				RefreshStrategicPlan();
			}

			// strategic planning
			CreateStrategicPath(prec);

			// operative planning target
			if (_state == Wait || _state == Replan) OperPath(prec);

			// operative planning
			return CreateOperativePath();
		}
	default:
		Fail("Planning mode");
		return false;
	}

#else
	return false;
#endif //_ENABLE_AI
}

void AIUnit::SetSemaphore(Semaphore status)
{
	_semaphore = status;
}

bool AIUnit::IsHoldingFire() const
{
	return _semaphore == AI::SemaphoreBlue ||
		_semaphore == AI::SemaphoreGreen ||
		_semaphore == AI::SemaphoreWhite;
}

bool AIUnit::IsKeepingFormation() const
{
	return _semaphore == AI::SemaphoreBlue ||
		_semaphore == AI::SemaphoreGreen ||
		_semaphore == AI::SemaphoreYellow;
}

bool AIUnit::IsFireEnabled(Target *tgt) const
{
	// check if fire enabled at given target
	if (!IsHoldingFire()) return true;
	if (tgt && tgt==GetEnableFireTarget()) return true;
	return false;
}
bool AIUnit::IsEngageEnabled(Target *tgt) const
{
	// check if fire enabled at given target
	if (!IsKeepingFormation()) return true;
	if (tgt && tgt==GetEngageTarget()) return true;
	return false;
}


void AIUnit::SetFormationPos(FormationPos status)
{
	_formPos = status;
	_formPosCoef = 1;
}

void AIUnit::AddFormationPos(FormationPos status)
{
	if (_formPos==status)
	{
		_formPosCoef += 1;
	}
	else
	{
		_formPos = status;
		_formPosCoef = 1;
	}
}

void AIUnit::SetUnitPosition(UnitPosition status)
{
	Person *person = GetPerson();
	if (person) person->SetUnitPosition(status);
}

UnitPosition AIUnit::GetUnitPosition() const
{
	Person *person = GetPerson();
	if (person) return person->GetUnitPosition();
	return UPAuto;
}

bool AIUnit::IsDanger() const
{
	return Glob.time <= _dangerUntil;
}

void AIUnit::SetDanger(float until)
{
	if (IsPlayer()) return;
	switch (GetCombatMode())
	{
		case CMSafe:
		case CMAware:
			if (until < 0) until = GRandGen.PlusMinus(5.0f, 1.0f);
			_dangerUntil = Glob.time + until;
			//LogF("%s: danger until %.2f",(const char *)GetDebugName(),until);
			EntityAI *veh = GetVehicle();
			if (veh) veh->OnDanger();
			break;
		// don't set danger in Combat mode 
	}
}

void AIUnit::ReportStatus()
{
	if (!_subgroup)
		return;

	ResourceState state;
	
	state = GetFuelState();
	if (state == RSCritical)
	{
		_fuelCriticalTime = Glob.time + REPEAT_REPORT_TIME;
		SendAnswer(FuelCritical);
	}
	else if (state == RSLow)
	{
		SendAnswer(FuelLow);
	}
	_lastFuelState = state;

	state = GetHealthState();
	if (state == RSCritical)
	{
		_healthCriticalTime = Glob.time + REPEAT_REPORT_TIME;
		SendAnswer(HealthCritical);
	}
	_lastHealthState = state;

	state = GetArmorState();
	if (state == RSCritical)
	{
		_dammageCriticalTime = Glob.time + REPEAT_REPORT_TIME;
		SendAnswer(DammageCritical);
	}
	_lastArmorState = state;

	state = GetAmmoState();
	if (state == RSCritical)
	{
		_ammoCriticalTime = Glob.time + REPEAT_REPORT_TIME;
		SendAnswer(AmmoCritical);
	}
	else if (state == RSLow)
	{
		SendAnswer(AmmoLow);
	}
	_lastAmmoState = state;

	if (GetCombatMode()==CMStealth)
	{
		AIGroup *grp = GetGroup();
		if (grp)
		{
			// stealth: mark all targets watch by this unit as not reported
			const TargetList &list = grp->GetTargetList();
			for ( int i=0; i<list.Size(); i++)
			{
				Target *tgt = list[i];
				if (tgt->idSensor==GetPerson())
				{
					// set as never reported - force reporting targets
					tgt->timeReported = TIME_MIN;
					tgt->posReported = VZero;
				}
			}
		}
	}

	SendAnswer(AI::ReportPosition);
}

bool AIUnit::IsAnyPlayer() const
{
	if (IsPlayer()) return true;
	Person *person = GetPerson();
	return person && person->IsNetworkPlayer();
}

bool AIUnit::IsPlayer() const
{
	return
		_person == GLOB_WORLD->PlayerOn();
}

bool AIUnit::IsPlayerDriven() const
{
	return
		_person == GLOB_WORLD->PlayerOn() ||
		_inVehicle &&
		_inVehicle->CommanderUnit() == this &&
		_inVehicle->Driver() == GLOB_WORLD->PlayerOn();
}

float AIUnit::GetRandomizedExperience() const
{
	float exp = GetPerson()->GetExperience();
	const float coef = 0.2;
	// result is from <(1 - coef) * exp ; (1 + coef) * exp>
	return exp * ((1.0 - coef) + (2.0 * coef) * GRandGen.RandomValue());
}

float AIUnit::GetInvAverageSpeed() const
{
	GeographyInfo geogr;
	geogr.packed = 0;
	geogr.u.gradient = 1;

	return GetVehicle()->GetCost(geogr);
}

float AIUnit::GetAverageSpeed() const
{
	return 1.0 / GetInvAverageSpeed();
}

void AIUnit::IncreaseExperience(const VehicleType& type, TargetSide side)
{
	float coef;

	AIGroup *grp = GetGroup();
	Assert(grp);
	if (!grp) return;

	AICenter *center = grp->GetCenter();
	if (side == TCivilian)
		coef = ExperienceDestroyCivilian;
	else if (center->IsEnemy(side))
		coef = ExperienceDestroyEnemy;
	else
		coef = ExperienceDestroyFriendly;

	if (!IsPlayer() && coef <= 0) return;

	int i, n = ExperienceDestroyTable.Size();
	float base = 0;
	float cost = type.GetCost();
	if (cost > ExperienceDestroyTable[n - 2].maxCost)
	{
		base = ExperienceDestroyTable[n - 1].exp;
	}
	else
		for (i=0; i<n-1; i++)
		{
			if (cost <= ExperienceDestroyTable[i].maxCost)
			{
				base = ExperienceDestroyTable[i].exp;
				break;
			}
		}

	AddExp(coef * base);
}

bool DefFindFreePositionCallback(Vector3Par pos, void *context)
{
	return true;
}

bool AIUnit::FindFreePosition
(
	Vector3 &pos, Vector3 &normal, bool soldier, EntityAI *veh,
	FindFreePositionCallback *isFree, void *context
)
{
	OperMap map;
	int mask = MASK_AVOID_OBJECTS | MASK_AVOID_VEHICLES;
	float xf = pos.X() * InvLandGrid;
	float zf = pos.Z() * InvLandGrid;
	int x = toIntFloor(xf);
	int z = toIntFloor(zf);
	int border = 1;
	int xMin = x - border;
	int xMax = x + border + 1;
	int zMin = z - border;
	int zMax = z + border + 1;
	float dx,dz;
	if
	(
		xMax <= 0 || xMin >= LandRange ||
		zMax <= 0 || zMin >= LandRange
	)
	{
		pos[1] = GLOB_LAND->GetSeaLevel();
		normal=VUp;
//		Log("Out of map");
		return false;
	}
	if (xMin < 0) xMin = 0;
	if (xMax > LandRange) xMax = LandRange;
	if (zMin < 0) zMin = 0;
	if (zMax > LandRange) zMax = LandRange;
	for (z=zMin; z<zMax; z++)
		for (x=xMin;x<xMax; x++)
			map.CreateField(x, z, mask, veh);

	bool ok = true;
	x = toIntFloor(pos.X() * InvOperItemGrid);
	z = toIntFloor(pos.Z() * InvOperItemGrid);
	if
	(
		map.GetFieldCost(x, z, true, veh, soldier) >= GET_UNACCESSIBLE ||
		!isFree(pos,context)
	)
	{
		ok = map.FindNearestEmpty
		(
			x, z, xf,zf,
			xMin * OperItemRange, zMin * OperItemRange,
			(xMax - xMin) * OperItemRange, (zMax - zMin) * OperItemRange,
			true, veh, soldier, isFree, context
		);

		if (ok)
		{
			Assert(map.GetFieldCost(x, z, true, veh, soldier) < GET_UNACCESSIBLE);
		}


		pos[0] = x * OperItemGrid + 0.5 * OperItemGrid;
		pos[2] = z * OperItemGrid + 0.5 * OperItemGrid;
		pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2], &dx, &dz);
	}
	else 
	{
		// given field is accessible. Check neighbouring fields.
		// If all are acessible, do not channge pos, at it is certainly valid
		// ignore locks
		if
		(
			map.GetFieldCost(x-1, z-1, false, veh, soldier) >= GET_UNACCESSIBLE ||
			map.GetFieldCost(x,   z-1, false, veh, soldier) >= GET_UNACCESSIBLE ||
			map.GetFieldCost(x+1, z-1, false, veh, soldier) >= GET_UNACCESSIBLE ||
			map.GetFieldCost(x-1, z,   false, veh, soldier) >= GET_UNACCESSIBLE ||
			map.GetFieldCost(x+1, z,   false, veh, soldier) >= GET_UNACCESSIBLE ||
			map.GetFieldCost(x-1, z+1, false, veh, soldier) >= GET_UNACCESSIBLE ||
			map.GetFieldCost(x,   z+1, false, veh, soldier) >= GET_UNACCESSIBLE ||
			map.GetFieldCost(x+1, z+1, false, veh, soldier) >= GET_UNACCESSIBLE
		)
		{
			pos[0] = x * OperItemGrid + 0.5 * OperItemGrid;
			pos[2] = z * OperItemGrid + 0.5 * OperItemGrid;
			pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2], &dx, &dz);
		}
		else
		{
			pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos, &dx, &dz);
		}
	}

	map.ClearMap();
	if (soldier)
	{
		normal=VUp;
	}
	else
	{
		// use different
		pos[1] -= veh->GetShape()->Min().Y();
		normal=Vector3(-dx,1,-dz);
	}
//	Log("Field %d, %d - OK = %d", x, z, ok);
	return ok;
}

bool AIUnit::FindFreePosition()
{
	Point3 pos = Position();
	Vector3 normal;
	EntityAI *vehicle=GetVehicle();
	if (FindFreePosition(pos, normal, IsSoldier(), vehicle))
	{
		Matrix4 transform=vehicle->Transform();
		transform.SetPosition(pos);
		transform.SetUpAndDirection(normal,vehicle->Direction());
		vehicle->MoveNetAware(transform);
		return true;
	}

	return false;
}

bool AIUnit::FindFreePosition( Vector3 &pos, Vector3 &normal )
{
	EntityAI *vehicle=GetVehicle();
	return FindFreePosition(pos, normal, IsSoldier(), vehicle);
}

void AIUnit::RefreshMission()
{
	if
	(
		_state != Stopped &&
		_state != Stopping &&
		_state != Delay &&
		_state != InCargo
	)
		Verify(SetState(AIUnit::Wait));

	AIGroup *grp = GetGroup();
	if (!grp)
		return;

	if (grp->Leader() == this && !GetSubgroup()->HasCommand())
	{
		grp->DoRefresh();
	}
}

float AIUnit::GetTimeToLive() const
{
	const float MinTimeToLive = 5.0;
	// assume we can live max. 24 hours
	float maxTimeToLive = 3600*24.0;
	// when in combat, assume we can live max. 1 hour
	if (GetCombatMode()>=CMCombat) maxTimeToLive = 3600;

	if( !GetGroup() )
	{
		Fail("No group in GetTimeToLive");
		return MinTimeToLive;
	}

	// exposure is updated quite slowly
	// when we calculate timeToLive from current target,
	// we will almost certainly not underestimate

	/*
	Target *tgt = GetTargetAssigned();
	if (tgt)
	{
		EntityAI *veh = GetVehicle();
		float dist2 = veh->Position().Distance2(tgt->AimingPosition());
		float visibility = 1;
		Threat threat = tgt->type->GetDammagePerMinute(dist2,visibility);
		VehicleKind kind = veh->GetType()->GetKind();
		float dpm = threat[kind];

		if (dpm>0)
		{
			float ttl = 60 * GetVehicle()->GetArmor() / dpm;
			saturateMin(maxTimeToLive,ttl);
		}
	}
	*/

	int x = toIntFloor(GetVehicle()->Position().X() * InvLandGrid);
	int z = toIntFloor(GetVehicle()->Position().Z() * InvLandGrid);
	float exposure;
	if (IsHoldingFire())
	{
		exposure = GetGroup()->GetCenter()->GetExposurePessimistic(x, z);
	}
	else
	{
		exposure = GetGroup()->GetCenter()->GetExposureOptimistic(x, z);
	}
	if (exposure <= 0) return maxTimeToLive;

	float timeToLive = 60 * GetVehicle()->GetArmor() / exposure;
	saturate(timeToLive, MinTimeToLive, maxTimeToLive);
	return timeToLive;
}

void AIUnit::AllowGetIn(bool flag)
{
	_getInAllowed = flag;
	if (!flag)
	{
		AISubgroup *subgrp = GetSubgroup();
		if (subgrp)
		{
			subgrp->ClearGetInCommands();
			AIGroup *grp = subgrp->GetGroup();
			if (grp) grp->ClearGetInCommands(this);
		}
	}
}

void AIUnit::UnassignVehicle()
{
	if (_vehicleAssigned)
	{
		_vehicleAssigned->RemoveAssignement(this);
		_vehicleAssigned = NULL;
	}
}

bool AIUnit::AssignAsDriver(Transport *veh)
{
	if (!veh->GetType()->HasDriver()) return false;

	if (veh && veh == _vehicleAssigned && veh->GetDriverAssigned() == this)
		return true;	// already assigned as driver

	if (_vehicleAssigned)	// unassign
	{
		_vehicleAssigned->RemoveAssignement(this);
		_vehicleAssigned = NULL;
	}

	if (veh)
	{
		AIUnit *oldDriver = veh->GetDriverAssigned();
		if (oldDriver)	// unassign
		{
			if (veh->Driver() && !veh->Driver()->IsDammageDestroyed())
			{	// cannot assign - driver is inside
				Assert(veh->DriverBrain() == oldDriver);
				return false;
			}
			veh->RemoveAssignement(oldDriver);
			oldDriver->_vehicleAssigned = NULL;
		}
		veh->AssignDriver(this);
		_vehicleAssigned = veh;
	}
	return true;
}

bool AIUnit::AssignAsGunner(Transport *veh)
{
	if (!veh->GetType()->HasGunner()) return false;

	if (veh == _vehicleAssigned && veh->GetGunnerAssigned() == this)
		return true;	// already assigned as gunner

	if (_vehicleAssigned)	// unassign
	{
		_vehicleAssigned->RemoveAssignement(this);
		_vehicleAssigned = NULL;
	}

	if (veh)
	{
		AIUnit *oldGunner = veh->GetGunnerAssigned();
		if (oldGunner)	// unassign
		{
			if (veh->Gunner() && !veh->Gunner()->IsDammageDestroyed())
			{	// cannot assign - driver is inside
				Assert(veh->GunnerBrain() == oldGunner);
				return false;
			}
			veh->RemoveAssignement(oldGunner);
			oldGunner->_vehicleAssigned = NULL;
		}
		veh->AssignGunner(this);
		_vehicleAssigned = veh;
	}
	return true;
}

bool AIUnit::AssignAsCommander(Transport *veh)
{
	if (veh && !veh->GetType()->HasCommander()) return false;

	if (veh && veh == _vehicleAssigned && veh->GetCommanderAssigned() == this)
		return true;	// already assigned as commander

	if (_vehicleAssigned)	// unassign
	{
		_vehicleAssigned->RemoveAssignement(this);
		_vehicleAssigned = NULL;
	}

	if (veh)
	{
		AIUnit *oldCommander = veh->GetCommanderAssigned();
		if (oldCommander)	// unassign
		{
			if (veh->Commander() && !veh->Commander()->IsDammageDestroyed())
			{	// cannot assign - driver is inside
				Assert(veh->CommanderBrain() == oldCommander);
				return false;
			}
			veh->RemoveAssignement(oldCommander);
			oldCommander->_vehicleAssigned = NULL;
		}
		veh->AssignCommander(this);
		_vehicleAssigned = veh;
	}
	return true;
}

bool AIUnit::AssignAsCargo(Transport *veh)
{
	if
	(
		veh && veh == _vehicleAssigned &&
		veh->GetCommanderAssigned() != this &&
		veh->GetDriverAssigned() != this &&
		veh->GetGunnerAssigned() != this
	)
	{
		#if !_RELEASE
			bool found = false;
			for (int i=0; i<veh->NCargoAssigned(); i++)
				if (veh->GetCargoAssigned(i) == this)
				{
					found = true;
					break;
				}
			Assert(found);
		#endif
		return true;	// already assigned as cargo
	}

	if (_vehicleAssigned)	// unassign
	{
		_vehicleAssigned->RemoveAssignement(this);
		_vehicleAssigned = NULL;
	}

	if (veh)
	{
		int n = veh->NCargoAssigned();
		if (n >= veh->Type()->GetMaxManCargo())
		{
			for (int i=0; i<n; i++)
			{
				AIUnit *oldCargo = veh->GetCargoAssigned(i);
				if (oldCargo->GetVehicle() == veh && !oldCargo->GetPerson()->IsDammageDestroyed())
				{
					Assert(oldCargo->IsInCargo());
					continue;
				}
				Assert(oldCargo->IsFreeSoldier() || oldCargo->GetPerson()->IsDammageDestroyed());
				// unassign
				veh->RemoveAssignement(oldCargo);
				oldCargo->_vehicleAssigned = NULL;
				break;
			}
		}
		n = veh->NCargoAssigned();
		if (n >= veh->Type()->GetMaxManCargo()) return false;	// cannot assign - cargo is full
		veh->AssignCargo(this);
		_vehicleAssigned = veh;
	}
	return true;
}


RString AIUnit::GetDebugName() const
{
	if( !GetGroup() ) return RString("no group");
	char buffer[256];
	sprintf(buffer, "%s:%d", (const char *)GetGroup()->GetDebugName(), ID());
	Person *person = GetPerson();
	if (person && person->IsNetworkPlayer())
		sprintf(buffer + strlen(buffer), " (%s)", (const char *)person->GetInfo()._name);
	if (person && !person->IsLocal()) strcat(buffer, " REMOTE");
	return buffer;
}

bool AIUnit::AssertValid() const
{
	bool ok = true;
	if (!GetPerson())
	{
		// all units must be represented by some soldier
		Fail("no person");
		ok = false;
	}

	AISubgroup *subgrp = GetSubgroup();
	if (subgrp)
	{
		AIGroup *grp = subgrp->GetGroup();
		if (grp)
		{
			AICenter *center = grp->GetCenter();
			if (!center)
			{
				Fail("no center");
				ok = false;
			}
		}
		else
		{
			Fail("no group");
			ok = false;
		}
	}
	else
	{
		Fail("no subgroup");
		ok = false;
	}

	// TODO: test position in driver / commander / gunner / cargo
	return ok;
}

void AIUnit::Dump(int indent) const
{
}
