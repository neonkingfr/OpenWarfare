/**************************************************************************

		Fixed.h - 16.16 Fixed point class

**************************************************************************/

#ifndef _FAKE_FIXED_H
#define _FAKE_FIXED_H

typedef float Fixed;

//#define FIXED_MAX Fixed(Fixed::Fx,0x7fffffff)
//#define FIXED_MIN Fixed(Fixed::Fx,-0x80000000)

// standard way to call explicit conversions:
#define fixed(x) (x)

#define fxToInt(x) toLargeInt(x)
#define fxToIntFloor(x) toLargeInt((x)-0.5f)
#define fxToIntCeil(x) toLargeInt((x)+0.5f)
#define fxToFloat(x) (x)

#define Fixed0 (0.0f)

//#define abs(x) fabs(x)

#include "math.h"

#endif

