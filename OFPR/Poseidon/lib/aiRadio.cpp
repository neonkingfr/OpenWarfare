// AI - implementation of AI radio messages

#include "wpch.hpp"
#include "ai.hpp"
#include "person.hpp"
#include "aiDefs.hpp"

#include "aiRadio.hpp"

#include "world.hpp"
#include "landscape.hpp"
//#include "loadStream.hpp"
#include <El/Common/randomGen.hpp>
#include "global.hpp"

//#include "strIncl.hpp"
#include "stringtableExt.hpp"

#include <El/ParamFile/paramFile.hpp>
extern ParamFile Res;

#include "languages.hpp"
#include "sentences.hpp"
#include "uiActions.hpp"

#include <ctype.h>

#include "chat.hpp"

#include <El/Common/enumNames.hpp>

#include "network.hpp"

//#define PAUSE_AFTER_WORD				0.05F
//#define PAUSE_IN_NUMBER					0.01F
//#define PAUSE_IN_UNIT_LIST			0.02F
#define PAUSE_AFTER_WORD				0.00F
#define PAUSE_IN_NUMBER					0.00F

#define PAUSE_AFTER_NUMBER			PAUSE_AFTER_WORD
#define PAUSE_IN_UNIT_LIST			0.00F
#define PAUSE_AFTER_UNIT_LIST		0.05F

///////////////////////////////////////////////////////////////////////////////
// helper functions

static PackedBoolArray GetUnitsList(AIGroup *grp,OLinkArray<AIUnit> &units)
{
	PackedBoolArray result;
	int i;
	for (i=0; i<units.Size(); i++)
	{
		AIUnit *unit = units[i];
		if (!unit)
			continue;
		if (grp && unit->GetGroup()!=grp) continue;
		result.Set(unit->ID() - 1, true);
	}
	return result;
}

static void Spell(RadioSentence &sentence, const char *buffer, float pauseAfter)
{
	int i, n = strlen(buffer);
	for (i=0; i<n; i++)
	{
		float pause = (i == n - 1) ? pauseAfter : PAUSE_IN_NUMBER;
		char c = tolower(buffer[i]);
		switch (c)
		{
		case '0':
			sentence.Add("zero", pause);
			break;
		case '1':
			sentence.Add("one", pause);
			break;
		case '2':
			sentence.Add("two", pause);
			break;
		case '3':
			sentence.Add("three", pause);
			break;
		case '4':
			sentence.Add("four", pause);
			break;
		case '5':
			sentence.Add("five", pause);
			break;
		case '6':
			sentence.Add("six", pause);
			break;
		case '7':
			sentence.Add("seven", pause);
			break;
		case '8':
			sentence.Add("eight", pause);
			break;
		case '9':
			sentence.Add("nine", pause);
			break;
		case 'a':
			sentence.Add("alpha", pause);
			break;
		case 'b':
			sentence.Add("bravo", pause);
			break;
		case 'c':
			sentence.Add("charlie", pause);
			break;
		case 'd':
			sentence.Add("delta", pause);
			break;
		case 'e':
			sentence.Add("echo", pause);
			break;
		case 'f':
			sentence.Add("foxtrot", pause);
			break;
		case 'g':
			sentence.Add("golf", pause);
			break;
		case 'h':
			sentence.Add("hotel", pause);
			break;
		case 'i':
			sentence.Add("india", pause);
			break;
		case 'j':
			sentence.Add("juliet", pause);
			break;
		case '.':
		case ',':
		case ':':
		case ';':
		case '?':
		case '!':
		case '-':
		case '(':
		case ')':
			break;
		default:
			Fail("Character");
			break;
		}
	}
}

static void SayUnitNumber(RadioSentence &sentence, int number, float pause)
{
	switch (number)
	{
	case 1:
		sentence.Add("one", pause);
		break;
	case 2:
		sentence.Add("two", pause);
		break;
	case 3:
		sentence.Add("three", pause);
		break;
	case 4:
		sentence.Add("four", pause);
		break;
	case 5:
		sentence.Add("five", pause);
		break;
	case 6:
		sentence.Add("six", pause);
		break;
	case 7:
		sentence.Add("seven", pause);
		break;
	case 8:
		sentence.Add("eight", pause);
		break;
	case 9:
		sentence.Add("nine", pause);
		break;
	case 10:
		sentence.Add("ten", pause);
		break;
	case 11:
		sentence.Add("eleven", pause);
		break;
	case 12:
		sentence.Add("twelve", pause);
		break;
	default:
		Fail("Unit ID");
		break;
	}
}

static void SayNumber
(
	RadioSentence &sentence, int number, float pauseAfter,
	const char *format = "%d", int mode=1
)
{
	switch (mode)
	{
		default:
			char buffer[32];
			sprintf(buffer, format, number);
			Spell(sentence, buffer, pauseAfter);
		break;
		case 2:
			SayUnitNumber(sentence,number,pauseAfter);
		break;
	}
}

static void SayUnit(RadioSentence &sentence, AIUnit *unit, int mode, float pause)
{
	Assert(unit);
	Assert(unit->ID() > 0 && unit->ID() <= MAX_UNITS_PER_GROUP);
	if (mode == 1)
	{
		SayUnitNumber(sentence,unit->ID(),pause);
	}
	else if (mode == 2)
	{
		switch (unit->ID())
		{
		case 1:
			sentence.Add("1ToLeader", pause);
			break;
		case 2:
			sentence.Add("2ToLeader", pause);
			break;
		case 3:
			sentence.Add("3ToLeader", pause);
			break;
		case 4:
			sentence.Add("4ToLeader", pause);
			break;
		case 5:
			sentence.Add("5ToLeader", pause);
			break;
		case 6:
			sentence.Add("6ToLeader", pause);
			break;
		case 7:
			sentence.Add("7ToLeader", pause);
			break;
		case 8:
			sentence.Add("8ToLeader", pause);
			break;
		case 9:
			sentence.Add("9ToLeader", pause);
			break;
		case 10:
			sentence.Add("10ToLeader", pause);
			break;
		case 11:
			sentence.Add("11ToLeader", pause);
			break;
		case 12:
			sentence.Add("12ToLeader", pause);
			break;
		default:
			Fail("Unit ID");
			break;
		}
	}
	else
		Fail("Mode");
}

static PackedBoolArray PrepareList(AIGroup *grp, PackedBoolArray list, bool wholeCrew)
{
	if (!wholeCrew) return list;
	if (!grp) return list;
	
	PackedBoolArray result = list;
	int i;
	for (i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		if (!list.Get(i)) continue;
		AIUnit *unit = grp->UnitWithID(i + 1);
		if (!unit) continue;
		Transport *veh = unit->GetVehicleIn();
		if (!veh) continue;
		// one of the vehicle is always CommanderUnit
		AIUnit *commander = veh->CommanderUnit();
		if (commander && unit!=commander)
		{
			// pass all commands to vehicle commander
			result.Set(i, false);
			if (commander->GetGroup() == grp && commander != grp->Leader())
				result.Set(commander->ID() - 1, true);
		}
	}
	return result;
}

static bool AllUnits(AIGroup *grp, PackedBoolArray list, bool wholeCrew)
{
	if (!grp) return false;
	if (list.GetCount() <= 1) return false;
	
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = grp->UnitWithID(i + 1);
		if (!unit) continue;
		if (wholeCrew)
		{
			Transport *veh = unit->GetVehicleIn();
			if (veh)
			{
				AIUnit *commander = veh->CommanderUnit();
				if (commander)
				{
					unit = commander;
				}
			}
		}
		if (unit == grp->Leader()) continue;
		if (!list.Get(unit->ID() - 1))
		{
			return false;
		}
	}

	return true;
}

static const EnumName TeamNames[]=
{
	EnumName(TeamMain, "MAIN"),
	EnumName(TeamRed, "RED"),
	EnumName(TeamGreen, "GREEN"),
	EnumName(TeamBlue, "BLUE"),
	EnumName(TeamYellow, "YELLOW"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(Team dummy)
{
	return TeamNames;
}

Team GetTeam(int i);
void SetTeam(int i, Team team);

static int WholeTeam(AIGroup *grp, PackedBoolArray list, bool wholeCrew)
{
	if (!grp) return -1;
	if (list.GetCount() <= 1) return -1;
	
	int all = 0;
	int main = 0;
	int red = 0;
	int green = 0;
	int blue = 0;
	int yellow = 0;
	int mainOut = 0;
	int redOut = 0;
	int greenOut = 0;
	int blueOut = 0;
	int yellowOut = 0;

	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = grp->UnitWithID(i + 1);
		if (!unit) continue;
		if (wholeCrew)
		{
			Transport *veh = unit->GetVehicleIn();
			if (veh && veh->GetType()->HasCommander())
			{
				AIUnit *commander = veh->CommanderBrain();
				AIUnit *driver = veh->DriverBrain();
				AIUnit *gunner = veh->GunnerBrain();
				if (commander)
				{
					if (driver == unit || gunner == unit)
					{
						unit = commander;
					}
				}
				else if (driver)
				{
					if (gunner == unit)
					{
						unit = driver;
					}
				}
			}
		}
		if (unit == grp->Leader()) continue;

		Team team = GetTeam(unit->ID() - 1);
		if (list.Get(unit->ID() - 1))
		{
			all++;
			switch (team)
			{
			case TeamMain:
				main++;
				break;
			case TeamRed:
				red++;
				break;
			case TeamGreen:
				green++;
				break;
			case TeamBlue:
				blue++;
				break;
			case TeamYellow:
				yellow++;
				break;
			}
		}
		else
		{
			switch (team)
			{
			case TeamMain:
				mainOut++;
				break;
			case TeamRed:
				redOut++;
				break;
			case TeamGreen:
				greenOut++;
				break;
			case TeamBlue:
				blueOut++;
				break;
			case TeamYellow:
				yellowOut++;
				break;
			}
		}
	}

	if (all > 0)
	{
		if (main == all && mainOut == 0) return TeamMain;
		if (red == all && redOut == 0) return TeamRed;
		if (green == all && greenOut == 0) return TeamGreen;
		if (blue == all && blueOut == 0) return TeamBlue;
		if (yellow == all && yellowOut == 0) return TeamYellow;
	}
	return -1;
}

static void SayUnits(RadioSentence &sentence, int n, AIGroup *grp, PackedBoolArray list, int mode, float pauseAfter)
{
	if (!grp) return;

	int i;
	for (i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		if (!list.Get(i))
			continue;
		float pause;
		if (--n == 0)
			pause = pauseAfter;
		else
			pause = PAUSE_IN_UNIT_LIST;
		if (mode == 1)
		{
			switch (i + 1)
			{
			case 1:
				sentence.Add("one", pause);
				break;
			case 2:
				sentence.Add("two", pause);
				break;
			case 3:
				sentence.Add("three", pause);
				break;
			case 4:
				sentence.Add("four", pause);
				break;
			case 5:
				sentence.Add("five", pause);
				break;
			case 6:
				sentence.Add("six", pause);
				break;
			case 7:
				sentence.Add("seven", pause);
				break;
			case 8:
				sentence.Add("eight", pause);
				break;
			case 9:
				sentence.Add("nine", pause);
				break;
			case 10:
				sentence.Add("ten", pause);
				break;
			case 11:
				sentence.Add("eleven", pause);
				break;
			case 12:
				sentence.Add("twelve", pause);
				break;
			default:
				Fail("Unit ID");
				break;
			}
		}
	}
}

void CreateUnitsList(PackedBoolArray list, char *buffer)
{
	buffer[0] = 0;
	bool empty = true;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		if (list.Get(i))
		{
			if (!empty) strcat(buffer, ", ");
			sprintf(buffer+strlen(buffer), "%d", i + 1);
			empty = false;
		}
	}
}

int SentenceParamSimpleList::Add(RString text, TargetSide side)
{
	for (int i=0; i<Size(); i++)
	{
		SentenceParamSimple &param = Set(i);
		if
		(
			param.type == SPTText &&
			param.side == side &&
			strcmp(param.text, text) == 0 &&
			param.number < 9
		)
		{
			param.number++;
			return i;
		}
	}
	int index = base::Add();
	SentenceParamSimple &param = Set(index);
	param.type = SPTText;
	param.number = 1;
	param.side = side;
	param.text = text;
	return index;
}

int SentenceParams::Add(AISubgroup *subgrp, bool wholeCrew)
{
	int index = base::Add();
	Set(index).type = SPTUnitList;
	Set(index).grp = subgrp->GetGroup();
	Set(index).list = subgrp->GetUnitsList();
	Set(index).wholeCrew = wholeCrew;
	return index;
}

int SentenceParams::Add(OLinkArray<AIUnit> &units, bool wholeCrew)
{
	int index = base::Add();
	Set(index).type = SPTUnitList;
	Set(index).grp = units[0]->GetGroup();
	Set(index).list = GetUnitsList(NULL,units);
	Set(index).wholeCrew = wholeCrew;
	return index;
}

int SentenceParams::Add(Ref<SentenceParamSimpleList> targets)
{
	int index = base::Add();
	Set(index).type = SPTTargetList;
	Set(index).targets = targets;
	return index;
}

void SentenceParams::AddAzimutRelDir(Vector3Par dir)
{
	static const RStringB azimSay[13]=
	{
		"at12","at1",
		"at2","at3",
		"at4","at5",
		"at6","at7",
		"at8","at9",
		"at10","at11",
		"at12",
	};
	static const RStringB azimWrite[13]=
	{
		LocalizeString(IDS_WORD_AT12),LocalizeString(IDS_WORD_AT1),
		LocalizeString(IDS_WORD_AT2),LocalizeString(IDS_WORD_AT3),
		LocalizeString(IDS_WORD_AT4),LocalizeString(IDS_WORD_AT5),
		LocalizeString(IDS_WORD_AT6),LocalizeString(IDS_WORD_AT7),
		LocalizeString(IDS_WORD_AT8),LocalizeString(IDS_WORD_AT9),
		LocalizeString(IDS_WORD_AT10),LocalizeString(IDS_WORD_AT11),
		LocalizeString(IDS_WORD_AT12),
	};

	int azimut = toInt(atan2(dir.X(), dir.Z()) * (6 / H_PI));
	if (azimut < 0) azimut += 12;

	//int ret = -1;
	if (azimut>=0 && azimut<=12)
	{
		AddWord(azimSay[azimut],azimWrite[azimut]);
	}
}

void SentenceParams::AddDistance(float dist)
{
	static const RStringB distSay[]=
	{
		"dist50", // 0..50
		"dist100", // 50..150
		"dist200","dist200",
		"dist500",
		"dist500", // 500
		"dist500",
		"dist1000","dist1000","dist1000",
		"dist1000", // 1000
		"dist1000","dist1000","dist1000","dist1000","dist1000",
		"dist2000","dist2000","dist2000","dist2000",
		"dist2000", // 2000
		"dist2000","dist2000","dist2000","dist2000","dist2000",
		"far", // 2500 (and more)
	};
	static const RStringB distWrite[]=
	{
		LocalizeString(IDS_WORD_DIST50),
		LocalizeString(IDS_WORD_DIST100), // 50..150
		LocalizeString(IDS_WORD_DIST200),LocalizeString(IDS_WORD_DIST200),
		LocalizeString(IDS_WORD_DIST500),
		LocalizeString(IDS_WORD_DIST500), // 500
		LocalizeString(IDS_WORD_DIST500),
		LocalizeString(IDS_WORD_DIST1000),LocalizeString(IDS_WORD_DIST1000),
		LocalizeString(IDS_WORD_DIST1000),
		LocalizeString(IDS_WORD_DIST1000), // 1000
		LocalizeString(IDS_WORD_DIST1000),LocalizeString(IDS_WORD_DIST1000),
		LocalizeString(IDS_WORD_DIST1000),LocalizeString(IDS_WORD_DIST1000),
		LocalizeString(IDS_WORD_DIST1000),
		LocalizeString(IDS_WORD_DIST2000),LocalizeString(IDS_WORD_DIST2000),
		LocalizeString(IDS_WORD_DIST2000),LocalizeString(IDS_WORD_DIST2000),
		LocalizeString(IDS_WORD_DIST2000), // 2000
		LocalizeString(IDS_WORD_DIST2000),LocalizeString(IDS_WORD_DIST2000),
		LocalizeString(IDS_WORD_DIST2000),LocalizeString(IDS_WORD_DIST2000),
		LocalizeString(IDS_WORD_DIST2000),
		LocalizeString(IDS_WORD_DISTFAR), // 2500 (and more)
	};

	int dist100 = toInt(dist*0.01);
	const int maxDist = sizeof(distWrite)/sizeof(*distWrite)-1;
	saturate(dist100,0,maxDist);

	AddWord(distSay[dist100],distWrite[dist100]);
}

void SentenceParams::AddRelativePosition(Vector3Par dir)
{
	// convert position to relative position
	int azimut = toInt(0.1 * atan2(dir.X(), dir.Z()) * (180 / H_PI));
	if (azimut < 0) azimut += 36;
	Add("%02d", azimut);
	//int dist = toInt(0.1 * dir.SizeXZ());
	//Add(dist);
}

void SentenceParams::AddAzimut(Vector3Par pos)
{
	AIUnit *unit = GWorld->FocusOn();
	// FIXED for dedicated server
	if (!unit)
	{
		AddAzimutRelDir(VForward);
		return;
	}

	AIGroup *grp = unit ? unit->GetGroup() : NULL;
	AIUnit *leader = grp ? grp->Leader() : NULL;
	if (leader)
	{
		Matrix4 transf;
		transf.SetOriented(grp->MainSubgroup()->GetFormationDirection(), VUp);
		transf.SetPosition(leader->Position());
		AddAzimutRelDir(transf.InverseRotation().FastTransform(pos));
	}
	else
		AddAzimutRelDir(GWorld->CameraOn()->GetInvTransform().FastTransform(pos));
//	GWorld->UI()->ShowGroupDir();
}

void SentenceParams::AddAzimutDir(Vector3Par dir)
{
	AIUnit *unit = GWorld->FocusOn();
	// FIXED for dedicated server
	if (!unit)
	{
		AddAzimutRelDir(VForward);
		return;
	}

	AIGroup *grp = unit ? unit->GetGroup() : NULL;
	AIUnit *leader = grp ? grp->Leader() : NULL;
	if (leader)
	{
		Matrix3 orient(MDirection, grp->MainSubgroup()->GetFormationDirection(), VUp);
		AddAzimutRelDir(orient.InverseRotation() * dir);
	}
	else
		AddAzimutRelDir(GWorld->CameraOn()->GetInvTransform().Rotate(dir));
//	GWorld->UI()->ShowGroupDir();
}

/*!
\patch 1.29 Date 10/31/2001 by Jirka
- Fixed: Group direction shown in MP game
*/
static void ShowGroupDir(AIGroup *to)
{
	AIUnit *unit = GWorld->FocusOn();
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *u = to->UnitWithID(i + 1);
		if (!u) continue;
		if (u == unit)
			GWorld->UI()->ShowGroupDir();
		else if (u->GetPerson()->IsRemotePlayer())
			GetNetworkManager().ShowGroupDir(u->GetPerson(), u->GetGroup()->MainSubgroup()->GetFormationDirection());
	}
}

static void ShowGroupDir(OLinkArray<AIUnit> &to)
{
	AIUnit *unit = GWorld->FocusOn();
	for (int i=0; i<to.Size(); i++)
	{
		AIUnit *u = to[i];
		if (!u) continue;
		if (u == unit)
			GWorld->UI()->ShowGroupDir();
		else if (u->GetPerson()->IsRemotePlayer())
			GetNetworkManager().ShowGroupDir(u->GetPerson(), u->GetGroup()->MainSubgroup()->GetFormationDirection());
	}
}

static RString SelectVariant(const ParamEntry &cls, float rnd)
{
	int n = (cls.GetSize() + 1) / 2;
	
	int i, ii = n - 1;
	for (i=0; i<ii; i++)
	{
		float r = cls[2 * i + 1];
		rnd -= r;
		if (rnd <= 0)
		{
			ii = i;
			break;
		}
	}
	
	return cls[2 * ii];
}

RString SPEECH(int x)
{
	char buffer[256];
	sprintf(buffer, "Speech%d", x);
	return buffer;
}

static void SaySentence
(
	RadioSentence &sentence, int language,
	const ParamEntry &clsVariant, SentenceParams &params,
	float pauseAfterMessage
)
{
	const ParamEntry &cls = clsVariant >> SPEECH(language);

	int n = cls.GetSize();
	int nParams = params.Size();
	for (int i=0; i<n; i++)
	{
		RString word = cls[i];
		if (!word || word.GetLength() == 0) continue;
		if (word[0] == '\'') continue;
		if (word[0] == '%')
		{
			if (word[1] == 'q')
			{
				// do not say MicOut
				// do not wait
				return;
			}
			else
			{
				const char *format = word;
				format++;
				int index = 0;
				int mode = 1;
				while (isdigit(*format))
				{
					index *= 10;
					index += *format - '0';
					format++;
				}
				index--;
				if (*format == '.')
				{
					format++;
					mode = 0;
					while (isdigit(*format))
					{
						mode *= 10;
						mode += *format - '0';
						format++;
					}
				}
/*
				int index = atoi(format + 1) - 1;
*/
				if (index < 0 || index >= nParams) continue;
				SentenceParam param = params[index];
				switch (param.type)
				{
				case SPTWordText:
					sentence.Add(param.text, PAUSE_AFTER_WORD);
					break;
				case SPTText:
					Spell(sentence, param.text, PAUSE_AFTER_WORD);
					break;
				case SPTNumber:
					SayNumber(sentence, param.number, PAUSE_AFTER_WORD, "%d", mode);
					break;
				case SPTFormat:
					SayNumber(sentence, param.number, PAUSE_AFTER_WORD, param.text, mode);
					break;
				case SPTUnit:
					SayUnit(sentence, param.unit, mode, PAUSE_AFTER_UNIT_LIST);
					break;
				case SPTUnitList:
					{
						PackedBoolArray list = PrepareList(param.grp, param.list, param.wholeCrew);
						if (AllUnits(param.grp, list, param.wholeCrew))
						{
							sentence.Add("all", PAUSE_AFTER_UNIT_LIST);
						}
						else
						{
							int team = WholeTeam(param.grp, list, param.wholeCrew);
							if (team >= 0)
							{
								switch (team)
								{
								case TeamMain:
									sentence.Add("whiteTeam", PAUSE_AFTER_UNIT_LIST);
									break;
								case TeamRed:
									sentence.Add("redTeam", PAUSE_AFTER_UNIT_LIST);
									break;
								case TeamGreen:
									sentence.Add("greenTeam", PAUSE_AFTER_UNIT_LIST);
									break;
								case TeamBlue:
									sentence.Add("blueTeam", PAUSE_AFTER_UNIT_LIST);
									break;
								case TeamYellow:
									sentence.Add("yellowTeam", PAUSE_AFTER_UNIT_LIST);
									break;
								}
							}
							else
							{
								int i, n = 0;
								for (i=0; i<MAX_UNITS_PER_GROUP; i++) if (list.Get(i)) n++;
								SayUnits(sentence, n, param.grp, list, mode, PAUSE_AFTER_UNIT_LIST);
							}
						}
					}
					break;
				case SPTTargetList:
					for (int j=0; j<param.targets->Size(); j++)
					{
						SentenceParamSimple &target = (*param.targets)[j];
						if (j > 0 && j == param.targets->Size() - 1)
						{
							sentence.Add("and", PAUSE_AFTER_WORD);
						}
						if (target.number > 1)
							SayNumber(sentence, target.number, PAUSE_AFTER_WORD);
						Assert(target.type == SPTText);
						Spell(sentence, target.text, PAUSE_AFTER_WORD);
					}
					break;
				}
			}
		}
		else
			sentence.Add(word, PAUSE_AFTER_WORD);
	}

	sentence.Add("xmit", 0);
	//sentence.Add(RandomMicOut(), pauseAfterMessage);
}

static RString WriteSentence(RString format, SentenceParams &params)
{
	int nParams = params.Size();
	char result[2048], c;
	const char *src = format;
	char *dst = result;

	while ((c = *src)!=0)
	{
		if (c == '%')
		{
			src++;
			int index = 0;
			int mode = 1;
			while (isdigit(*src))
			{
				index *= 10;
				index += *src - '0';
				src++;
			}
			index--;
			if (*src == '.')
			{
				src++;
				mode = 0;
				while (isdigit(*src))
				{
					mode *= 10;
					mode += *src - '0';
					src++;
				}
			}
			if (index < 0 || index >= nParams) continue;
			SentenceParam param = params[index];
			switch (param.type)
			{
			case SPTWordText:
				{
					strcpy(dst, param.text2);
					dst += param.text2.GetLength();
				}
				break;
			case SPTText:
				strcpy(dst, param.text);
				dst += param.text.GetLength();
				break;
			case SPTNumber:
				{
					char buffer[32];
					sprintf(buffer, "%d", param.number);
					strcpy(dst, buffer);
					dst += strlen(buffer);
				}
				break;
			case SPTFormat:
				{
					char buffer[32];
					sprintf(buffer, param.text, param.number);
					strcpy(dst, buffer);
					dst += strlen(buffer);
				}
				break;
			case SPTUnit:
				if (param.unit)
				{
					if (mode == 1)
					{
						char buffer[32];
						sprintf(buffer, "%d", param.unit->ID());
						strcpy(dst, buffer);
						dst += strlen(buffer);
					}
					else if (mode == 2)
					{
						if (param.unit->ID() > 0 && param.unit->ID() <= MAX_UNITS_PER_GROUP)
						{
							RString word = LocalizeString(IDS_1_TO_LEADER + param.unit->ID() - 1);
							strcpy(dst, word);
							dst += word.GetLength();
						}
					}
					else
						Fail("Mode");
				}
				break;
			case SPTUnitList:
				{
					PackedBoolArray list = PrepareList(param.grp, param.list, param.wholeCrew);
					if (AllUnits(param.grp, list, param.wholeCrew))
					{
						strcpy(dst, LocalizeString(IDS_WORD_ALL));
						dst += strlen(dst);
					}
					else
					{
						int team = WholeTeam(param.grp, list, param.wholeCrew);
						if (team >= 0)
						{
							switch (team)
							{
							case TeamMain:
								strcpy(dst, LocalizeString(IDS_WORD_TEAM_MAIN));
								break;
							case TeamRed:
								strcpy(dst, LocalizeString(IDS_WORD_TEAM_RED));
								break;
							case TeamGreen:
								strcpy(dst, LocalizeString(IDS_WORD_TEAM_GREEN));
								break;
							case TeamBlue:
								strcpy(dst, LocalizeString(IDS_WORD_TEAM_BLUE));
								break;
							case TeamYellow:
								strcpy(dst, LocalizeString(IDS_WORD_TEAM_YELLOW));
								break;
							}
							dst += strlen(dst);
						}
						else if (mode == 1)
						{
							char buffer[256];
							CreateUnitsList(list, buffer);
							strcpy(dst, buffer);
							dst += strlen(buffer);
						}
					}
				}
				break;
			}
		}
		else
		{
			*dst = c;
			src++;
			dst++;
		}
	}
	*dst = 0;

	return result;
}

const float coefAa11 = 100.0 / (256*50);
const float invCoefAa11 = 1.0 / coefAa11;

void PositionToAA11(Vector3Val pos, char *buffer);
/*
void PositionToAA11(int x, int z, char *buffer)
{
	if (x < 0)
		while (x < 0) x += 260;
	else
		while (x > 259) x -= 260;
	int y = 99 - z;
	sprintf(buffer, "%c%c%02d", 'A' + x / 10, 'a' + x % 10, y);
}
*/

///////////////////////////////////////////////////////////////////////////////
// class RadioMessage

static const EnumName RadioMessageTypeNames[]=
{
	EnumName(RMTCommand, "COMMAND"),
	EnumName(RMTFormation, "FORM"),
	EnumName(RMTSemaphore, "SEM"),
	EnumName(RMTBehaviour, "BEHAVIOUR"),
	EnumName(RMTOpenFire, "OPEN FIRE"),
	EnumName(RMTCeaseFire, "CEASE FIRE"),
	EnumName(RMTLooseFormation, "LOOSE FORM"),
	EnumName(RMTReportStatus, "STATUS"),
	EnumName(RMTTarget, "TARGET"),
	EnumName(RMTGroupAnswer, "GRP ANS"),
	EnumName(RMTSubgroupAnswer, "SUBGRP ANS"),
	EnumName(RMTReturnToFormation, "RETURN TO FORM"),
	EnumName(RMTUnitAnswer, "UNIT ANS"),
	EnumName(RMTFireStatus,"FIRE STATUS"),
	EnumName(RMTUnitKilled, "UNIT KILLED"),
	EnumName(RMTText, "TEXT"),
	EnumName(RMTReportTarget, "REPORT TARGET"),
	EnumName(RMTObjectDestroyed, "OBJ DESTROYED"),
	EnumName(RMTContact, "CONTACT"),
	EnumName(RMTUnderFire, "UNDER FIRE"),
	EnumName(RMTClear, "CLEAR"),
	EnumName(RMTRepeatCommand, "REPEAT COMMAND"),
	EnumName(RMTWhereAreYou, "WHERE ARE YOU"),
	EnumName(RMTNotifyCommand, "NOTIFY"),
	EnumName(RMTCommandConfirm, "CONFIRM"),
	EnumName(RMTPosition, "POSITION"),
	EnumName(RMTFormationPos, "FORM POS"),
	EnumName(RMTTeam, "TEAM"),
	EnumName(RMTAskSupply, "ASK SUPPLY"),
	EnumName(RMTSupportAsk, "SUPPORT ASK"),
	EnumName(RMTSupportConfirm, "SUPPORT CONFIRM"),
	EnumName(RMTSupportReady, "SUPPORT READY"),
	EnumName(RMTSupportDone, "SUPPORT DONE"),
	EnumName(RMTJoin, "JOIN"),
	EnumName(RMTJoinDone, "JOIN DONE"),
	EnumName(RMTWatchAround, "WATCH AROUND"),
	EnumName(RMTWatchDir, "WATCH DIR"),
	EnumName(RMTWatchPos, "WATCH POS"),
	EnumName(RMTWatchTgt, "WATCH TGT"),
	EnumName(RMTWatchAuto, "WATCH AUTO"),
	EnumName(RMTVehicleMove, "V MOVE"),
	EnumName(RMTVehicleFire, "V FIRE"),
	EnumName(RMTVehicleFormation, "V FORM"),
	EnumName(RMTVehicleCeaseFire, "V CEASE FIRE"),
	EnumName(RMTVehicleSimpleCommand, "V SIMPLE COMMAND"),
	EnumName(RMTVehicleTarget, "V TARGET"),
	EnumName(RMTVehicleLoad, "V LOAD"),
	EnumName(RMTVehicleAzimut, "V AZIMUT"),
	EnumName(RMTVehicleStopTurning, "V STOP TURNING"),
	EnumName(RMTVehicleFireFailed, "V FIRE FAILED"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(RadioMessageType dummy)
{
	return RadioMessageTypeNames;
}

static const EnumName AnswerNames[]=
{
	EnumName(AI::NoAnswer, "NO ANSWER"),
	EnumName(AI::StepCompleted, "STEP OK"),
	EnumName(AI::StepTimeOut, "STEP TIMEOUT"),
	EnumName(AI::UnitDestroyed, "DESTROYED"),
	EnumName(AI::HealthCritical, "HEALTH CRIT"),
	EnumName(AI::DammageCritical, "DAMM CRIT"),
	EnumName(AI::FuelCritical, "FUEL CRIT"),
	EnumName(AI::ReportPosition, "REPORT POS"),
	EnumName(AI::ReportSemaphore, "REPORT SEM"),
	EnumName(AI::AmmoCritical, "AMMO CRIT"),
	EnumName(AI::FuelLow, "FUEL LOW"),
	EnumName(AI::AmmoLow, "AMMO LOW"),
	EnumName(AI::IsLeader, "LEADER"),
	EnumName(AI::CommandCompleted, "COMM OK"),
	EnumName(AI::CommandFailed, "COMM FAIL"),
	EnumName(AI::SubgroupDestinationUnreacheable, "SUBGRP UNREACH"),
	EnumName(AI::MissionCompleted, "MISS OK"),
	EnumName(AI::MissionFailed, "MISS FAIL"),
	EnumName(AI::DestinationUnreacheable, "UNREACH"),
	EnumName(AI::GroupDestroyed, "GRP DESTROYED"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(AI::Answer dummy)
{
	return AnswerNames;
}

static const EnumName ReportSubjectNames[]=
{
	EnumName(ReportNew, "NEW"),
	EnumName(ReportDestroy, "DESTROY"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(ReportSubject dummy)
{
	return ReportSubjectNames;
}

LSError ReportTargetInfo::Serialize(ParamArchive &ar)
{
	// old version - load Target
	if (ar.IsLoading())
	{
		TargetId oldTarget;
		CHECK(ar.SerializeRef("Target", oldTarget, 1))
		if (oldTarget)
		{
		}
	}
	//CHECK(ar.SerializeEnum("side", side, 1))
	//CHECK(::Serialize(ar, "type", type, 1))
	return LSOK;
}

RadioMessage::RadioMessage()
{
	_transmitted = false;
	_playerMsg = false;
}

float RadioMessage::GetDuration() const
{
	return 1.0;
}

Speaker *RadioMessage::GetSpeaker() const
{
	AIUnit *sender = GetSender();
	return sender ? sender->GetSpeaker() : NULL;
}

static RadioMessage *CreateRadioMessage(RadioMessageType type)
{
	if (type >= (RadioMessageType)RMTFirstVehicle)
		return CreateVehicleMessage(type);
	else switch (type)
	{
	case RMTCommand:
		return new RadioMessageCommand();
	case RMTFormation:
		return new RadioMessageFormation();
	case RMTSemaphore:
		return new RadioMessageSemaphore();
	case RMTBehaviour:
		return new RadioMessageBehaviour();
	case RMTOpenFire:
		return new RadioMessageOpenFire();
	case RMTCeaseFire:
		return new RadioMessageCeaseFire();
	case RMTLooseFormation:
		return new RadioMessageLooseFormation();
	case RMTReportStatus:
		return new RadioMessageReportStatus();
	case RMTTarget:
		return new RadioMessageTarget();
	case RMTGroupAnswer:
		return new RadioMessageGroupAnswer();
	case RMTSubgroupAnswer:
		return new RadioMessageSubgroupAnswer();
	case RMTReturnToFormation:
		return new RadioMessageReturnToFormation();
	case RMTUnitAnswer:
		return new RadioMessageUnitAnswer();
	case RMTFireStatus:
		return new RadioMessageFireStatus();
	case RMTUnitKilled:
		return new RadioMessageUnitKilled();
	case RMTText:
		return new RadioMessageText();
	case RMTReportTarget:
		return new RadioMessageReportTarget();
	case RMTObjectDestroyed:
		return new RadioMessageObjectDestroyed();
	case RMTContact:
		return new RadioMessageContact();
	case RMTUnderFire:
		return new RadioMessageUnderFire();
	case RMTClear:
		return new RadioMessageClear();
	case RMTRepeatCommand:
		return new RadioMessageRepeatCommand();
	case RMTWhereAreYou:
		return new RadioMessageWhereAreYou();
	case RMTNotifyCommand:
		return new RadioMessageNotifyCommand();
	case RMTCommandConfirm:
		return new RadioMessageCommandConfirm();
	case RMTWatchAround:
		return new RadioMessageWatchAround();
	case RMTWatchDir:
		return new RadioMessageWatchDir();
	case RMTWatchPos:
		return new RadioMessageWatchPos();
	case RMTWatchTgt:
		return new RadioMessageWatchTgt();
	case RMTWatchAuto:
		return new RadioMessageWatchAuto();
	case RMTPosition:
		return new RadioMessageUnitPos();
	case RMTFormationPos:
		return new RadioMessageFormationPos();
	case RMTTeam:
		return new RadioMessageTeam();
	case RMTAskSupply:
		return new RadioMessageAskSupply();
	case RMTSupportAsk:
		return new RadioMessageSupportAsk();
	case RMTSupportConfirm:
		return new RadioMessageSupportConfirm();
	case RMTSupportReady:
		return new RadioMessageSupportReady();
	case RMTSupportDone:
		return new RadioMessageSupportDone();
	case RMTJoin:
		return new RadioMessageJoin();
	case RMTJoinDone:
		return new RadioMessageJoinDone();
	}
	Fail("Message Type");
	return NULL;
}

RadioMessage *RadioMessage::CreateObject(ParamArchive &ar)
{
	RadioMessageType type;
	if (ar.SerializeEnum("type", type, 1) != LSOK) return NULL;
	return CreateRadioMessage(type);
}

LSError RadioMessage::Serialize(ParamArchive &ar)
{
	if (ar.IsSaving())
	{
		RadioMessageType type = (RadioMessageType)GetType();
		CHECK(ar.SerializeEnum("type", type, 1))
	}
	CHECK(ar.Serialize("language", _language, 1, English))
	CHECK(ar.Serialize("priority", _priority, 1, 0))
	CHECK(ar.Serialize("timeout", _timeOut, 1))
	CHECK(ar.Serialize("playerMsg", _playerMsg, 1, false))
	return LSOK;
}

///////////////////////////////////////////////////////////////////////////////
// class RadioChannel

RadioMessage *RadioChannel::CreateMessage(int type)
{
	return CreateRadioMessage((RadioMessageType)type);
}

void RadioSentence::Say(RadioChannel *channel, Speaker *speaker)
{
	bool transmit = false;
	for (int i=0; i<Size(); i++)
	{
		RString id = Get(i).id;
		if (id.GetLength()<=0)
		{
			// log all words
			char buf[1024];
			*buf=0;
			for (int s=0; s<Size(); s++)
			{
				strcat(buf,"'");
				strcat(buf,Get(s).id);
				strcat(buf,"' ");
			}
			RptF("Empty word in sentence %s",buf);
			continue;
		}
		else if (stricmp(id, "xmit") == 0)
		{
			transmit = true;
		}
		else
		{
			channel->Say(speaker, Get(i).id, Get(i).pauseAfter, transmit);
			transmit = false;
		}
	}
}

const ParamEntry *FindRadio(RString name, SoundPars &pars);

void RadioChannel::Say(RString waveName, AIUnit *sender, RString senderName, RString player, float duration)
{
	if (!sender && senderName.GetLength() == 0) return;

	SoundPars pars;
	if (waveName[0] == '#')
	{
		char buffer[256];
		strncpy(buffer, waveName + 1, 256); buffer[255] = 0;
		char *ext = strrchr(buffer, '.');
		if (ext) *ext = 0;
		GChatList.Add(_chatChannel, sender, buffer, false, false);

		RString dir;
		if (player.GetLength() > 0)
		{
			dir = RString("tmp\\players\\") + player + RString("\\sound\\");
		}
		else
		{
			RString GetUserDirectory();
			dir = GetUserDirectory() + RString("sound\\");
		}
		pars.name = dir + RString(waveName + 1);
		pars.freq = 1;
		pars.freqRnd = 1;
		pars.vol = 1;
		pars.volRnd = 1;
	}
	else
	{
		const ParamEntry *cls = FindRadio(waveName, pars);

		if (cls)
		{
			if (sender)
				GChatList.Add(_chatChannel, sender, *cls >> "title", false, false);
			else
				GChatList.Add(_chatChannel, Pars >> "CfgHQIdentities" >> senderName >> "name", *cls >> "title", false, false);
		}
	}

	if (pars.name.GetLength() > 0  && GWorld->GetAcceleratedTime() <= 1)
	{
		//float volume=GSoundsys->GetSpeechVolCoef();
		AbstractWave *wave = GSoundScene->OpenAndPlayOnce2D
		(
			pars.name, 1, pars.freq, false
		);
		if (wave) wave->SetKind(WaveSpeech);
		if (!_saying)
		{
			// start queue
			_saying = wave;
			if (_saying)
			{
				_saying->Play(); // start playback (once)
			}
		}
		else
		{
			if (wave)
			{
				wave->Queue(_saying);
			}
		}
		_pauseAfterMessage = 0;
	}
	else
	{
		_pauseAfterMessage = duration;
	}

	if (sender)
	{
		_speaker = *sender->GetSpeaker();
	}
	else // if (senderName.GetLength() > 0)
	{
		const ParamEntry &cls = Pars >> "CfgHQIdentities" >> senderName;
		RString name = cls >> "speaker";
		float pitch = cls >> "pitch";
		Ref<BasicSpeaker> basicSpeaker = new BasicSpeaker(Pars >> "CfgVoice" >> name);
		_speaker = Speaker(basicSpeaker, pitch);
	}
	// start playing noise
	if (_saying)
	{
		if (_speaker.IsSpeakerValid() && _noiseType==RNRadio)
		{
			_noise = _speaker.SayNoPitch("loop",true);
		}
	}
}

bool IsSpeakingDirect(AIUnit *sender);

bool IsSpeakingRadio(AIUnit *sender)
{
	if (!sender) return false;

	// gloabal channel
	const RadioMessage *msg = GWorld->GetRadio().GetActualMessage();
	if (msg && msg->GetSender() == sender) return true;

	// vehicle channel
	Transport *veh = sender->GetVehicleIn();
	if (veh)
	{
		msg = veh->GetRadio().GetActualMessage();
		if (msg && msg->GetSender() == sender) return true;
	}

	// group channel
	AIGroup *grp = sender->GetGroup();
	if (grp)
	{
		msg = grp->GetRadio().GetActualMessage();
		if (msg && msg->GetSender() == sender) return true;

		// side channel
		AICenter *center = grp->GetCenter();
		if (center)
		{
			msg = center->GetRadio().GetActualMessage();
			if (msg && msg->GetSender() == sender) return true;
		}
	}

	return false;
}

/*!
\patch 1.55 Date 5/7/2002 by Ondra
- Changed: All radio trafic that is done in mission init is now processed before the mission starts,
expect for messages transmitted via xxxRadio and xxxChat scripting commands.
\patch 1.78 Date 7/16/2002 by Jirka
- Fixed: Silent processing of radio messages sometimes freeze
*/

void RadioChannel::SilentProcess()
{
	if (_actualMsg)
	{
		if (!_actualMsg->IsTransmitted())
			_actualMsg->Transmitted(); // confirm xmit done
			// SetTransmitted is needless
		_actualMsg=NULL;
	}
/*
	for (int i=0; i<_messageQueue.Size(); i++)
	{
		RadioMessage *msg = _messageQueue[i];
		if (msg->GetType()!=RMTText)
		{
			msg->Transmitted();
		}
	}
	_messageQueue.Clear();
*/
	// FIX
	while (_messageQueue.Size() > 0)
	{
		Ref<RadioMessage> msg = _messageQueue[0];
		_messageQueue.Delete(0);
		if (msg->GetType()!=RMTText)
			msg->Transmitted();
	}
}

void RadioChannel::NextMessage()
{
	if (_actualMsg)
	{
		if (!_actualMsg->IsTransmitted())
			_actualMsg->Transmitted(); // confirm xmit done
			// SetTransmitted is needless
		_actualMsg=NULL;
	}

	while
	(
		_messageQueue.Size() > 0 &&
		((!_messageQueue[0]) || Glob.time > _messageQueue[0]->GetTimeOut())
	)
	{
		//if (_messageQueue[0])
			_messageQueue[0]->Canceled();
		_messageQueue.Delete(0);
	}

	if (_messageQueue.Size() <= 0 ) return;

	AIUnit *sender = _messageQueue[0]->GetSender();
	if (IsSpeakingRadio(sender) || IsSpeakingDirect(sender))
	{
		_pauseAfterMessage = 0; // check in next simulation step
		return;
	}

	_actualMsg = _messageQueue[0];
	_messageQueue.Delete(0);

	float pauseAfterMessage = 0;
	if ((!sender || sender != GWorld->FocusOn()) && _messageQueue.Size() <= 5)
		pauseAfterMessage = GRandGen.Gauss(0.1, 1.0, 5.0); 

	if (_audible)
	{
		SentenceParams params;
		const char *sentence = _actualMsg->PrepareSentence(params);
		if (sentence)
		{
			const ParamEntry &clsSent = Res >> "RadioProtocol" >> sentence;
			float rnd = GRandGen.RandomValue();
			RString variant = SelectVariant(clsSent >> "versions", rnd);
			const ParamEntry &clsVariant = clsSent >> variant;

			RadioSentence sent;
			SaySentence(sent, _actualMsg->GetLanguage(), clsVariant, params, pauseAfterMessage);
			if (GWorld->GetAcceleratedTime() <= 1)
			{
				sent.Say(this, _actualMsg->GetSpeaker());
				_pauseAfterMessage=0;
			}
			else
			{
				_pauseAfterMessage = _actualMsg->GetDuration();
			}

			RString text = WriteSentence(clsVariant >> "text", params);
			GChatList.Add(_chatChannel, _actualMsg->GetSender(), text, _actualMsg->IsPlayerMsg(), false);
			SendRadioChat
			(
				_chatChannel, _actualMsg->GetSender(),
				_object, text, sent
			);
		}
		else
		{
			RString wave = _actualMsg->GetWave();
			if (wave.GetLength() > 0)
			{
				Say(wave, _actualMsg->GetSender(), _actualMsg->GetSenderName(), "", _actualMsg->GetDuration());
			}
			else
			{
				_pauseAfterMessage = _actualMsg->GetDuration();
			}
// Do not send over network - used as title effects etc.
// - activated on each client
/*
			SendRadioChatWave
			(
				_chatChannel, _object, wave, _actualMsg->GetSender(), _actualMsg->GetSenderName()
			);
*/
		}
	}
	else
	{
		_pauseAfterMessage=_actualMsg->GetDuration();
		if (GWorld->GetMode() == GModeNetware)
		{
			SentenceParams params;
			const char *sentence = _actualMsg->PrepareSentence(params);
			if (sentence)
			{
				const ParamEntry &clsSent = Res >> "RadioProtocol" >> sentence;
				float rnd = GRandGen.RandomValue();
				RString variant = SelectVariant(clsSent >> "versions", rnd);
				const ParamEntry &clsVariant = clsSent >> variant;

				RadioSentence sent;
				SaySentence(sent, _actualMsg->GetLanguage(), clsVariant, params, pauseAfterMessage);
				RString text = WriteSentence(clsVariant >> "text", params);

				SendRadioChat
				(
					_chatChannel, _actualMsg->GetSender(),
					_object, text, sent
				);
			}
			else
			{
// Do not send over network - used as title effects etc.
// - activated on each client
/*
				RString wave = _actualMsg->GetWave();
				SendRadioChatWave
				(
					_chatChannel, _object, wave, _actualMsg->GetSender(), _actualMsg->GetSenderName()
				);
*/
			}
		}
	}
}

static bool UnitAlive(AIUnit *unit)
{
	// check if unit is alive
	// if it is, confirm it to leader
	if (!unit) return false;
	AIGroup *grp = unit->GetGroup();
	if (!grp) return false;
	if (unit->GetLifeState()!=AIUnit::LSAlive) return false;
	grp->SetReportedDown(unit,false);
	grp->SetReportBeforeTime(unit,TIME_MAX);
	return true;
}


///////////////////////////////////////////////////////////////////////////////
// class RadioMessageUnitKilled

LSError RadioMessageUnitKilled::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.SerializeRef("from", _from, 1))
	CHECK(ar.SerializeRef("who", _who, 1))
	return LSOK;
}

const char *RadioMessageUnitKilled::GetPriorityClass()
{
	return "PriorityReport";
}

const char *RadioMessageUnitKilled::PrepareSentence(SentenceParams &params)
{
	if (!_from) return NULL;
	if (!_who) return NULL;
	if (_from->GetLifeState()!=AIUnit::LSAlive) return NULL;

	SetPlayerMsg(_from == GWorld->FocusOn());

	const char *sentence;
	
	sentence = "SentUnitKilled";
	params.Add(_who);
	params.Add(_from);

	return sentence;
}

/*!
\patch 1.42 Date 1/4/2002 by Ondra
- Fixed: Repeated ".. is down" message when player was killed.
*/

void RadioMessageUnitKilled::Transmitted()
{
	// nothing to do
	if (!UnitAlive(_from)) return;
	if (!_who) return;
	// remove unit from the subgroup and group
	if (_who->GetLifeState()==AIUnit::LSDead)
	{
		AISubgroup *subgrp = _who->GetSubgroup();
		subgrp->ReceiveAnswer(_who,AI::UnitDestroyed);
	}
	else
	{
		// force unit to report status
		// temporarily hide in group list (MIA status)
		AIGroup *grp = _who->GetGroup();
		if
		(
			grp && !_who->IsAnyPlayer() ||
			_who->GetLifeState()!=AIUnit::LSAlive
		)
		{
			grp->SetReportedDown(_who,true);
		}
		if (_who->GetLifeState()==AIUnit::LSAlive)
		{
			_who->ReportStatus();
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageAskSupply

LSError RadioMessageAskSupply::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.SerializeRef("from", _from, 1))
	CHECK(ar.SerializeEnum("message", _message, 1, Command::Heal))
	return LSOK;
}

const char *RadioMessageAskSupply::GetPriorityClass()
{
	return "NormalCommand";
}

void RadioMessageAskSupply::Transmitted()
{
}

const char *RadioMessageAskSupply::PrepareSentence(SentenceParams &params)
{
	return NULL;
/*
	if (!_from) return NULL;
	if (_from->GetLifeState() != AIUnit::LSAlive) return NULL;

	SetPlayerMsg(_from == GWorld->FocusOn());

	switch (_message)
	{
	case Command::Heal:
		return "SentAskHeal";
	case Command::Repair:
		return "SentAskRepair";
	case Command::Refuel:
		return "SentAskRefuel";
	case Command::Rearm:
		return "SentAskRearm";
	default:
		return "SentAskSupply";
	}
*/
}


///////////////////////////////////////////////////////////////////////////////
// class RadioMessageSupportAsk

LSError RadioMessageSupportAsk::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.SerializeRef("from", _from, 1))
	CHECK(ar.SerializeEnum("type", _type, 1, (UIActionType)ATNone))
	return LSOK;
}

const char *RadioMessageSupportAsk::GetPriorityClass()
{
	return "NormalCommand";
}

void RadioMessageSupportAsk::Transmitted()
{
	AIUnit *sender = GetSender();
	if (!_from || !sender) return;

	AICenter *center = _from->GetCenter();
	Assert(center);
	center->AskSupport(_from, _type);
}

const char *RadioMessageSupportAsk::PrepareSentence(SentenceParams &params)
{
	AIUnit *sender = GetSender();
	if (!_from || !sender) return NULL;
	
	Vector3Val pos = sender->Position();
	char buffer[6];
	PositionToAA11(pos, buffer);
	params.Add(RString(buffer));
	
	switch (_type)
	{
	case ATHeal:
		return "SentSupportAskHeal";
	case ATRepair:
		return "SentSupportAskRepair";
	case ATRearm:
		return "SentSupportAskRearm";
	case ATRefuel:
		return "SentSupportAskRefuel";
	default:
		Fail("Action");
		return NULL;
	}
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageSupportConfirm

LSError RadioMessageSupportConfirm::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.SerializeRef("from", _from, 1))
	return LSOK;
}

const char *RadioMessageSupportConfirm::GetPriorityClass()
{
	return "NormalCommand";
}

void RadioMessageSupportConfirm::Transmitted()
{
}

const char *RadioMessageSupportConfirm::PrepareSentence(SentenceParams &params)
{
	AIUnit *sender = GetSender();
	if (!_from || !sender) return NULL;

	const AIGroup *group = _from->GetSupportedGroup();
	if (!group) return NULL;
	params.AddWord("", group->GetName());

	Vector3Val pos = _from->GetSupportPos();
	char buffer[6];
	PositionToAA11(pos, buffer);
	params.Add(RString(buffer));

	return "SentSupportConfirm";
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageSupportReady

LSError RadioMessageSupportReady::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.SerializeRef("from", _from, 1))
	return LSOK;
}

const char *RadioMessageSupportReady::GetPriorityClass()
{
	return "NormalCommand";
}

void RadioMessageSupportReady::Transmitted()
{
}

const char *RadioMessageSupportReady::PrepareSentence(SentenceParams &params)
{
	AIUnit *sender = GetSender();
	if (!_from || !sender) return NULL;

	const AIGroup *group = _from->GetSupportedGroup();
	if (!group) return NULL;
	params.AddWord("", group->GetName());

	Vector3Val pos = _from->GetSupportPos();
	char buffer[6];
	PositionToAA11(pos, buffer);
	params.Add(RString(buffer));

	return "SentSupportReady";
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageSupportDone

LSError RadioMessageSupportDone::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.SerializeRef("from", _from, 1))
	return LSOK;
}

const char *RadioMessageSupportDone::GetPriorityClass()
{
	return "NormalCommand";
}

void RadioMessageSupportDone::Transmitted()
{
	AIUnit *sender = GetSender();
	if (!_from || !sender) return;

	AICenter *center = _from->GetCenter();
	Assert(center);
	center->SupportDone(_from);
}

const char *RadioMessageSupportDone::PrepareSentence(SentenceParams &params)
{
	AIUnit *sender = GetSender();
	if (!_from || !sender) return NULL;

	return "SentSupportDone";
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageJoin

RadioMessageJoin::RadioMessageJoin(AIGroup *from, PackedBoolArray list)
{
	_from = from;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		if (list.Get(i))
		{
			AIUnit *unit = from->UnitWithID(i + 1);
			if (unit) _to.Add(unit);
		}
	}
}

LSError RadioMessageJoin::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.SerializeRef("From", _from, 1))
	CHECK(ar.SerializeRefs("To", _to, 1))
	return LSOK;
}

const char *RadioMessageJoin::GetPriorityClass()
{
	return "NormalCommand";
}

bool RadioMessageJoin::IsTo(AIUnit *unit) const
{
	for (int i=0; i<_to.Size(); i++)
	{
		AIUnit *u = _to[i];
		if (u == unit)
			return true;
	}
	return false;
}

const char *RadioMessageJoin::PrepareSentence(SentenceParams &params)
{
	AIUnit *sender = GetSender();
	if (!_from || !sender) return NULL;

	_to.Compact();
	if (_to.Size() <= 0) return NULL;

	SetPlayerMsg(IsTo(GWorld->FocusOn()));

	params.Add(_to, true);
	params.Add(_from->Leader());

	return "SentCmdFollow";
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageJoinDone

LSError RadioMessageJoinDone::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.SerializeRef("From", _from, 1))
	CHECK(ar.SerializeRef("To", _to, 1))
	return LSOK;
}

const char *RadioMessageJoinDone::GetPriorityClass()
{
	return "JoinCompleted";
}

const char *RadioMessageJoinDone::PrepareSentence(SentenceParams &params)
{
	if (!_from) return NULL;

	SetPlayerMsg(_from == GWorld->FocusOn());

	params.Add(_from);
	return "SentJoinCompleted";
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageUnitAnswer

LSError RadioMessageUnitAnswer::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.SerializeRef("From", _from, 1))
	CHECK(ar.SerializeRef("To", _to, 1))
	CHECK(ar.SerializeEnum("answer", _answer, 1))
	return LSOK;
}

const char *RadioMessageUnitAnswer::GetPriorityClass()
{
	switch (_answer)
	{
		case AI::HealthCritical:
		case AI::DammageCritical:
			return "CriticalReport";
		case AI::FuelCritical:
		case AI::AmmoCritical:
		case AI::IsLeader:
			return "PriorityReport";
		case AI::FuelLow:
		case AI::AmmoLow:
		case AI::ReportPosition:
		case AI::ReportSemaphore:
			return "Report";
		case AI::SubgroupDestinationUnreacheable:
			return "Failure";
		default:
			Fail("Answer");
			return "Default";
	}
}

const char *RadioMessageUnitAnswer::PrepareSentence(SentenceParams &params)
{
	if (!_from)
		return NULL;
	if (!_to)
		return NULL;

	if (_from->GetLifeState()!=AIUnit::LSAlive) return NULL;

	SetPlayerMsg(_from == GWorld->FocusOn());

	const char *sentence;

	params.Add(_from);
	switch (_answer)
	{
		case AI::HealthCritical:
			sentence = "SentHealthCritical";
			break;
		case AI::DammageCritical:
			sentence = "SentDammageCritical";
			break;
		case AI::FuelCritical:
			sentence = "SentFuelCritical";
			break;
		case AI::FuelLow:
			sentence = "SentFuelLow";
			break;
		case AI::AmmoCritical:
			sentence = "SentAmmoCritical";
			break;
		case AI::AmmoLow:
			sentence = "SentAmmoLow";
			break;
		case AI::ReportPosition:
			{
				Vector3Val pos = _from->Position();
				char buffer[6];
				PositionToAA11(pos, buffer);
				sentence = "SentReportPosition";
				params.Add(RString(buffer));
			}
			break;
		case AI::ReportSemaphore:
			return NULL;
		case AI::IsLeader:
			sentence = "SentIsLeader";
			break;
		case AI::SubgroupDestinationUnreacheable:
			sentence = "SentDestinationUnreacheable";
			break;
		default:
			Fail("Answer");
			return NULL;
	}

	return sentence;
}

void RadioMessageUnitAnswer::Transmitted()
{
	if (_to && _from)
	{
		if (UnitAlive(_from))
		{
			AIGroup *grp = _to->GetGroup();
			if (grp)
			{
				grp->SetReportedDown(_from,false);
				grp->SetReportBeforeTime(_from,TIME_MAX);
			}
			if (!_to->IsLocal())
			{
				DoAssert(_from->IsAnyPlayer());
				if (_from->IsAnyPlayer())
				{
					GetNetworkManager().AskForReceiveUnitAnswer(_from,_to,_answer);
				}
				return;
			}
			_to->ReceiveAnswer(_from, _answer);
			if (_answer == AI::ReportPosition)
			{
				if (grp && grp->IsPlayerGroup())
				{
					void AddUnitInfo(AIUnit *subgroup);
					AddUnitInfo(_from);
				}
			}

			if
			(
				_answer == AI::ReportPosition ||
				_answer == AI::IsLeader
			)
			{
				// show leader when report position or taking command takes place
				AIUnit *player = GWorld->FocusOn();
				AIGroup *pgrp = player ? player->GetGroup() : NULL;
				if (pgrp && pgrp->Leader() == _from && GWorld->UI())
					GWorld->UI()->ShowFormPosition();
				// unit reported - clear flag we are waiting for report
			}
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageFireStatus

LSError RadioMessageFireStatus::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.SerializeRef("From", _from, 1))
	CHECK(ar.Serialize("answer", _answer, 1, false))
	return LSOK;
}

const char *RadioMessageFireStatus::GetPriorityClass()
{
	return "Report";
}

const char *RadioMessageFireStatus::PrepareSentence(SentenceParams &params)
{
	if (!_from)
		return NULL;

	SetPlayerMsg(_from == GWorld->FocusOn());

	if (_answer)
	{
		return "SentFireReady";
	}
	else
	{
		return "SentFireNegative";
	}
}

void RadioMessageFireStatus::Transmitted()
{
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageSubgroupAnswer

LSError RadioMessageSubgroupAnswer::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.SerializeRef("From", _from, 1))
	CHECK(ar.SerializeRef("Leader", _leader, 1))
	CHECK(ar.SerializeRef("To", _to, 1))
	CHECK(ar.SerializeEnum("answer", _answer, 1))
	CHECK(ar.Serialize("cmd", _cmd, 2))
	CHECK(ar.Serialize("display", _display, 1, false))
	CHECK(ar.Serialize("forceLeader", _forceLeader, 1, false))
	return LSOK;
}

RadioMessageSubgroupAnswer::RadioMessageSubgroupAnswer() {}

RadioMessageSubgroupAnswer::RadioMessageSubgroupAnswer
(
	AISubgroup *from, AIGroup *to, AI::Answer answer, Command *cmd, bool display,
	AIUnit *leader
)
{
	_from = from;
	_leader = leader ? leader : from->Leader();
	_forceLeader = leader!=NULL;
	_to = to;
	_answer = answer;
	if (cmd) _cmd = *cmd;
	_display = display;
}

const char *RadioMessageSubgroupAnswer::GetPriorityClass()
{
	switch (_answer)
	{
		case AI::CommandCompleted:
			return "Completition";
		case AI::CommandFailed:
		case AI::SubgroupDestinationUnreacheable:
			return "Failure";
		default:
			Fail("Answer");
			return "Default";
	}
}

const char *RadioMessageSubgroupAnswer::PrepareSentence(SentenceParams &params)
{
	if (!_display)
		return NULL;

	if (!_forceLeader || !_leader)
	{
		if (_from && _from->Leader())
			_leader = _from->Leader();
		if (!_leader)
			return NULL;
	}

	if (_leader->GetLifeState()!=AIUnit::LSAlive) return NULL;

	const char *sentence;
	switch (_answer)
	{
		case AI::CommandCompleted:
			if
			(
				_cmd._message == Command::Attack ||
				_cmd._message == Command::AttackAndFire
			)
				return NULL; // no message
			else
				sentence = "SentCommandCompleted";
			break;
		case AI::CommandFailed:
			sentence = "SentCommandFailed";
			break;
		case AI::SubgroupDestinationUnreacheable:
			sentence = "SentDestinationUnreacheable";
			break;
		default:
			Fail("Answer");
			return NULL;
	}

	params.Add(_leader);
	SetPlayerMsg(_leader == GWorld->FocusOn());

	return sentence;
}

AIUnit *RadioMessageSubgroupAnswer::GetSender() const
{
	if (_forceLeader && _leader) return _leader;
	return _from && _from->Leader() ? _from->Leader() : (AIUnit*)_leader;
}

void RadioMessageSubgroupAnswer::Transmitted()
{
	AIGroup *grp = _to;
	if (!grp) return;
	AIUnit *fromLeader = _leader;
	if (_from && _from->Leader()) fromLeader = _from->Leader();
	if (!fromLeader) return;

	if (!UnitAlive(fromLeader)) return;
	// if command was some kind of supply, reset supply status
	const Command &cmd = GetCommand();
	Command::Message msg = cmd._message;
	if (msg==Command::Action)
	{
		// get action ID
		switch (cmd._action)
		{
			case ATHeal: msg = Command::Heal; break;
			case ATRepair: msg = Command::Repair; break;
			case ATRefuel: msg = Command::Refuel; break;
			case ATRearm: msg = Command::Rearm; break;
			case ATTakeWeapon: msg = Command::Rearm; break;
			case ATTakeMagazine: msg = Command::Rearm; break;
		}
	}
	switch (msg)
	{
		case Command::Heal:
			grp->SetHealthStateReported(fromLeader,AIUnit::RSNormal);
			break;
		case Command::Rearm:
			grp->SetAmmoStateReported(fromLeader,AIUnit::RSNormal);
			break;
		case Command::Repair:
			grp->SetDammageStateReported(fromLeader,AIUnit::RSNormal);
			break;
		case Command::Refuel:
			grp->SetFuelStateReported(fromLeader,AIUnit::RSNormal);
			break;
		case Command::Action:
			// if it was action, it might have changed unit state
			break;

	}
	if (_from)
	{
		grp->ReceiveAnswer(_from, _answer);
	}
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageGroupAnswer

#define REPEAT_SENDER_AFTER			20.0

LSError RadioMessageGroupAnswer::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.SerializeRef("From", _from, 1))
	CHECK(ar.SerializeRef("To", _to, 1))
	CHECK(ar.SerializeEnum("answer", _answer, 1))
	return LSOK;
}

const char *RadioMessageGroupAnswer::GetPriorityClass()
{
	return "Default";
}

const char *RadioMessageGroupAnswer::PrepareSentence(SentenceParams &params)
{
	return NULL;
}

void RadioMessageGroupAnswer::Transmitted()
{
	if (_to)
		_to->ReceiveAnswer(_from, _answer);
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageReturnToFormation

const char *RadioMessageReturnToFormation::GetPriorityClass()
{
	return "NormalCommand";
}

LSError RadioMessageReturnToFormation::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.SerializeRef("From", _from, 1))
	CHECK(ar.SerializeRef("Leader", _leader, 1))
	CHECK(ar.SerializeRef("To", _to, 1))
	return LSOK;
}

void RadioMessageReturnToFormation::Transmitted()
{
	AIUnit *sender = GetSender();
	if (!sender || !_to) return;

	if (_to == GWorld->FocusOn() && GWorld->UI())
		GWorld->UI()->ShowFormPosition();
}

const char *RadioMessageReturnToFormation::PrepareSentence(SentenceParams &params)
{
	AIUnit *sender = GetSender();
	if (!sender || !_to) return NULL;

	SetPlayerMsg(_to == GWorld->FocusOn());

	params.Add(_to);
	params.Add(sender);

	return "SentReturnToFormation";
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageReply
// abstract class for all reply information

RadioMessageReply::RadioMessageReply(AIUnit *from, AIGroup *to)
{
	_from = from;
	_to = to;
}

LSError RadioMessageReply::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.SerializeRef("From", _from, 1))
	CHECK(ar.SerializeRef("To", _to, 1))
	return LSOK;
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageReportTarget

RadioMessageReportTarget::RadioMessageReportTarget(AIUnit *from, AIGroup *to, ReportSubject subject, Target &target)
: base(from, to)
{
	_subject = subject;

	_x = toIntFloor(coefAa11 * target.position.X());
	_z = toIntFloor(coefAa11 * target.position.Z());
	_targets.Resize(1);
	_targets[0].tgt = &target; //.idExact;
	//_targets[0].side = target.side;
	//_targets[0].type = target.type;
}

/*
RadioMessageReportTarget::RadioMessageReportTarget(AIUnit *from, AIGroup *to, ReportSubject subject, AITargetInfo &target)
: base(from, to)
{
	_subject = subject;

	_x = toIntFloor(coefAa11 * target._realPos.X());
	_z = toIntFloor(coefAa11 * target._realPos.Z());
	_targets.Resize(1);
	_targets[0].id = target._idExact;
	_targets[0].side = target._side;
	_targets[0].type = target._type;
}
*/

LSError RadioMessageReportTarget::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	CHECK(ar.SerializeRef("From", _from, 1))
	CHECK(ar.SerializeRef("To", _to, 1))
	CHECK(ar.SerializeEnum("subject", _subject, 1, ReportNew))
	CHECK(ar.Serialize("x", _x, 1))
	CHECK(ar.Serialize("z", _z, 1))
	CHECK(ar.Serialize("Targets", _targets, 1))
	return LSOK;
}

const char *RadioMessageReportTarget::GetPriorityClass()
{
	return "Detected";
}

/*!
\patch_internal 1.51 Date 4/19/2002 by Ondra
- Fixed: More consistend target handling in radio message.
Radio message was used from in-group reports only,
but was prepared also for center, which made group reports ineffective.
\patch 1.51 Date 4/19/2002 by Ondra
- Fixed: Target reporting often did not work, especially when using binoculars.
*/

const char *RadioMessageReportTarget::PrepareSentence(SentenceParams &params)
{
	if (!_from || !_to || !_to->Leader()) return NULL;
	AICenter *center = _to->GetCenter();
	if (!center) return NULL;
	if (!GWorld->CameraOn()) return NULL;

	Assert(_subject == ReportNew);
	Assert(_targets.Size() == 1);
	
	ReportTargetInfo &target = _targets[0];
	Target *tgt = target.tgt;
	if (!tgt) return NULL;
	if (tgt->side==center->GetSide()) return NULL;
	/*
	{
		// if center knows about it, check if it is friendly
		AITargetInfo *info = center->FindTargetInfo(target.id);
		if (info && info->_side == center->GetSide()) return NULL;
	}
	*/

	SetPlayerMsg(_from == GWorld->FocusOn());

	// sentence
	const char *sentence = NULL;

	// who reported
	params.Add(_from);

	// target
	params.AddWord(tgt->type->GetNameSound(), tgt->type->GetDisplayName());
	//params.AddWord(info->_type->GetNameSound(), info->_type->GetDisplayName());

	float tgtLeaderDist2 = _to->Leader()->Position().Distance2(tgt->position);
	float untLeaderDist2 = _to->Leader()->Position().Distance2(_from->Position());


	if
	(
		//tgtLeaderDist2 > Square(400) ||
		untLeaderDist2 > Square(100) && untLeaderDist2 > tgtLeaderDist2*0.75

	) // far
	{
		// square in map
		char buffer[6];
		PositionToAA11(tgt->position, buffer);
		params.Add(RString(buffer));

		if (tgt->side == TCivilian) sentence = "SentEnemyDetectedSimpleFar";
		else sentence = "SentEnemyDetectedFar"; // add side
	}
	else // near
	{
		// direction
		params.AddAzimut(tgt->position);
		::ShowGroupDir(_to);

		if (tgt->side == TCivilian) sentence = "SentEnemyDetectedSimple";
		else sentence = "SentEnemyDetected"; // add side
	}

	if (tgt->side == TSideUnknown)
		params.AddWord("unknown", LocalizeString(IDS_WORD_UNKNOWN));
	else if (_to->GetCenter()->IsEnemy(tgt->side))
		params.AddWord("enemy", LocalizeString(IDS_WORD_ENEMY));
	else if (_to->GetCenter()->IsFriendly(tgt->side))
		params.AddWord("friendly", LocalizeString(IDS_WORD_FRIENDLY));
	else
		params.AddWord("neutral", LocalizeString(IDS_WORD_NEUTRAL));

	float distance = _from->Position().Distance(tgt->position);
	params.AddDistance(distance);
	return sentence;
}

void RadioMessageReportTarget::Transmitted()
{
	if (_to && UnitAlive(_from))
	{
		if (_to->IsPlayerGroup())
		{
			void AddEnemyInfo(TargetType *id, const VehicleType *type, int x, int z);
			for (int i=0; i<_targets.Size(); i++)
			{
				Target *tgt = _targets[i].tgt;
				if (tgt && _to->GetCenter()->IsEnemy(tgt->side))
				{
					AddEnemyInfo(tgt->idExact, tgt->type, _x, _z);
				}
			}
		}
	}
	// nothing to do
}

bool RadioMessageReportTarget::IsTarget(TargetType *id) const
{
	for (int i=0; i<_targets.Size(); i++)
	{
		const ReportTargetInfo &target = _targets[i];
		if (target.tgt && target.tgt->idExact == id) return true;
	}
	return false;
}

bool RadioMessageReportTarget::HasType(const VehicleType *type) const
{
	for (int i=0; i<_targets.Size(); i++)
	{
		const ReportTargetInfo &target = _targets[i];
		if (target.tgt && target.tgt->type == type) return true;
	}
	return false;
}

bool RadioMessageReportTarget::IsInside(Vector3Val pos) const
{
	int x = toIntFloor(coefAa11 * pos.X());
	if (x != _x) return false;
	int z = toIntFloor(coefAa11 * pos.Z());
	return z == _z;
}

void RadioMessageReportTarget::DeleteTarget(TargetType *id)
{
	int i=0;
	while (i<_targets.Size())
	{
		ReportTargetInfo &target = _targets[i];
		if (target.tgt && target.tgt->idExact == id)
		{
			_targets.Delete(i);
			continue;
		}
		i++;
	}
}

void RadioMessageReportTarget::AddTarget(Target &target)
{
	int index = _targets.Add();
	_targets[index].tgt = &target;
}

/*
void RadioMessageReportTarget::AddTarget(AITargetInfo &target)
{
	int index = _targets.Add();
	_targets[index].id = target._idExact;
	_targets[index].side = target._side;
	_targets[index].type = target._type;
}
*/

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageNotifyCommand

RadioMessageNotifyCommand::RadioMessageNotifyCommand(AIUnit *from, AIGroup *to, Command &command)
: base(from, to)
{
	_command = command;
}

LSError RadioMessageNotifyCommand::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	CHECK(ar.Serialize("Command", _command, 1))
	return LSOK;
}

const char *RadioMessageNotifyCommand::GetPriorityClass()
{
	return "Notify";
}

const char *RadioMessageNotifyCommand::PrepareSentence(SentenceParams &params)
{
	if (!UnitAlive(_from) || !_to) return NULL;
	
	SetPlayerMsg(_from == GWorld->FocusOn());

	const char *sentence = NULL;
	params.Add(_from);

	switch (_command._message)
	{
	case Command::Attack:
	#if ENABLE_HOLDFIRE_FIX
	case Command::AttackAndFire:
	#endif
		{
			sentence = "SentNotifyAttack";

			Target *tgt=_command._targetE;
			if( !tgt ) tgt=_from->GetGroup()->FindTarget(_command._target);
			if (tgt)
			{
				params.AddWord(tgt->type->GetNameSound(), tgt->type->GetDisplayName());
			}
			else
			{
				params.AddWord("unknown", LocalizeString(IDS_WORD_UNKNOWN));
			}
		}
		break;
	case Command::Support:
		{
			VehicleWithAI *target = _command._target;
			if (!target) return NULL;
			
			sentence = "SentNotifySupport";
			params.AddWord(target->GetType()->GetNameSound(), target->GetType()->GetDisplayName());
		}
		break;
	}

	return sentence;
}

void RadioMessageNotifyCommand::Transmitted()
{
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageObjectDestroyed

RadioMessageObjectDestroyed::RadioMessageObjectDestroyed(AIUnit *from, AIGroup *to, const VehicleType *type)
: base(from, to)
{
	_vehicleType = type;
}

LSError RadioMessageObjectDestroyed::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	CHECK(::Serialize(ar, "vehicleType", _vehicleType, 1))
	return LSOK;
}

const char *RadioMessageObjectDestroyed::GetPriorityClass()
{
	return "Completition";
}

void RadioMessageObjectDestroyed::Transmitted()
{
}

const char *RadioMessageObjectDestroyed::PrepareSentence(SentenceParams &params)
{
	if (!_to || !UnitAlive(_from)) return NULL;

	SetPlayerMsg(_from == GWorld->FocusOn());

	// who reported
	params.Add(_from);

	// target
	if (_vehicleType)
	{
		params.AddWord(_vehicleType->GetNameSound(), _vehicleType->GetDisplayName());
		return "SentObjectDestroyed";
	}
	else
	{
		return "SentObjectDestroyedUnknown";
	}
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageContact

RadioMessageContact::RadioMessageContact(AIUnit *from, AIGroup *to)
: base(from, to)
{
}

LSError RadioMessageContact::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	return LSOK;
}

const char *RadioMessageContact::GetPriorityClass()
{
	return "UrgentCommand";
}

void RadioMessageContact::Transmitted()
{
	if (!_to) return;

}

const char *RadioMessageContact::PrepareSentence(SentenceParams &params)
{
	if (!_from || !_to) return NULL;

	// who reported
	params.Add(_from);

	return "SentContact";
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageUnderFire

RadioMessageUnderFire::RadioMessageUnderFire(AIUnit *from, AIGroup *to)
: base(from, to)
{
}

LSError RadioMessageUnderFire::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	return LSOK;
}

const char *RadioMessageUnderFire::GetPriorityClass()
{
	return "UrgentCommand";
}

void RadioMessageUnderFire::Transmitted()
{
	if (!_to) return;

}

const char *RadioMessageUnderFire::PrepareSentence(SentenceParams &params)
{
	if (!_from || !_to) return NULL;

	SetPlayerMsg(_from == GWorld->FocusOn());

	// who reported
	params.Add(_from);

	return "SentUnderFire";
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageClear

RadioMessageClear::RadioMessageClear(AIUnit *from, AIGroup *to)
: base(from, to)
{
}

LSError RadioMessageClear::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	return LSOK;
}

const char *RadioMessageClear::GetPriorityClass()
{
	return "NormalCommand";
}

void RadioMessageClear::Transmitted()
{
}

const char *RadioMessageClear::PrepareSentence(SentenceParams &params)
{
	if (!_from || !_to) return NULL;

	// who reported
	params.Add(_from);

	return "SentClear";
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageRepeatCommand

RadioMessageRepeatCommand::RadioMessageRepeatCommand(AIUnit *from, AIGroup *to)
: base(from, to)
{
}

LSError RadioMessageRepeatCommand::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	return LSOK;
}

const char *RadioMessageRepeatCommand::GetPriorityClass()
{
	return "NormalCommand";
}

void RadioMessageRepeatCommand::Transmitted()
{
	if (!_from || !_to || !_to->Leader()) return;
	AISubgroup *subgrp = _from->GetSubgroup();
	if (!subgrp) return;
	Command *cmd = subgrp->GetCommand();
	if (!cmd) return;
	_to->GetRadio().Transmit
	(
		new RadioMessageCommand(_to, subgrp->GetUnitsList(), *cmd, false, false),
		_to->GetCenter()->GetLanguage()
	);
}

const char *RadioMessageRepeatCommand::PrepareSentence(SentenceParams &params)
{
	if (!_from || !_to) return NULL;

	// who reported
	params.Add(_from);

	return "SentRepeatCommand";
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageWhereAreYou

RadioMessageWhereAreYou::RadioMessageWhereAreYou(AIUnit *from, AIGroup *to)
: base(from, to)
{
}

LSError RadioMessageWhereAreYou::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	return LSOK;
}

const char *RadioMessageWhereAreYou::GetPriorityClass()
{
	return "NormalCommand";
}

void RadioMessageWhereAreYou::Transmitted()
{
	if (!_to) return;
	AIUnit *leader = _to->Leader();
	if (!leader || leader->IsAnyPlayer()) return;
	if (leader->GetLifeState()!=AIUnit::LSAlive)
	{
		_to->SetReportBeforeTime(leader,Glob.time+10);
	}
	else
	{
		leader->SendAnswer(AI::ReportPosition);
	}
}

const char *RadioMessageWhereAreYou::PrepareSentence(SentenceParams &params)
{
	if (!_from || !_to) return NULL;

	// who reported
	params.Add(_from);

	return "SentWhereAreYou";
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageCommandConfirm

RadioMessageCommandConfirm::RadioMessageCommandConfirm(AIUnit *from, AIGroup *to, Command &command)
: base(from, to)
{
	_command = command;
}

LSError RadioMessageCommandConfirm::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	CHECK(ar.Serialize("Command", _command, 1))
	return LSOK;
}

const char *RadioMessageCommandConfirm::GetPriorityClass()
{
	return "Confirmation";
}

const char *RadioMessageCommandConfirm::PrepareSentence(SentenceParams &params)
{
	if (!_from || _from->GetLifeState()!=AIUnit::LSAlive) return NULL;

	SetPlayerMsg(_from == GWorld->FocusOn());

	params.Add(_from);
	if (_from->GetPerson()->GetRank() == RankPrivate)
		return "SentConfirmPrivate";
	else if
	(
		_command._message == Command::Attack ||
		_command._message == Command::AttackAndFire
	)
	{
		return "SentConfirmAttack";
	}
	else if (_command._message == Command::Move)
		return "SentConfirmMove";
	else
		return "SentConfirmOther";
}

void RadioMessageCommandConfirm::Transmitted()
{
	if (!_from) return;
	if (UnitAlive(_from)) return;
	// unit should report, but does not
	AIGroup *grp = _from->GetGroup();
	if (grp)
	{
		grp->SetReportBeforeTime(_from,Glob.time+5);
	}
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageFormation

LSError RadioMessageFormation::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.SerializeRef("From", _from, 1))
	CHECK(ar.SerializeRef("To", _to, 1))
	CHECK(ar.SerializeEnum("formation", _formation, 1, AI::FormLine))
	return LSOK;
}

const char *RadioMessageFormation::GetPriorityClass()
{
	return "NormalCommand";
}

const char *RadioMessageFormation::PrepareSentence(SentenceParams &params)
{
	if (!_from || !_from->Leader())
		return NULL;

	const char *sentence = NULL;
	
	params.Add(_from->Leader());
	switch (_formation)
	{
		case AI::FormColumn:
			sentence = "SentFormColumn";
			break;
		case AI::FormStaggeredColumn:
			sentence = "SentFormStaggeredColumn";
			break;
		case AI::FormWedge:
			sentence = "SentFormWedge";
			break;
		case AI::FormEcholonLeft:
			sentence = "SentFormEcholonLeft";
			break;
		case AI::FormEcholonRight:
			sentence = "SentFormEcholonRight";
			break;
		case AI::FormVee:
			sentence = "SentFormVee";
			break;
		default:
		case AI::FormLine:
			sentence = "SentFormLine";
			break;
	}

	return sentence;
}

void RadioMessageFormation::Transmitted()
{
	if (_to)
		_to->SetFormation(_formation);
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageSemaphoreState

RadioMessageState::RadioMessageState(AIGroup *from, PackedBoolArray list)
{
	_from = from;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		if (list.Get(i))
		{
			AIUnit *unit = from->UnitWithID(i + 1);
			if (unit) _to.Add(unit);
		}
	}
}

RadioMessageState::RadioMessageState()
{
}

bool RadioMessageState::IsTo(AIUnit *unit) const
{
	for (int i=0; i<_to.Size(); i++)
	{
		AIUnit *u = _to[i];
		if (u == unit)
			return true;
	}
	return false;
}

bool RadioMessageState::IsOnlyTo(AIUnit *unit) const
{
	bool ret = false;
	for (int i=0; i<_to.Size(); i++)
	{
		AIUnit *u = _to[i];
		if (!u) continue;
		if (u != unit) return false;
		else ret = true;
	}
	return ret;
}

void RadioMessageState::DeleteTo(AIUnit *unit)
{
	for (int i=0; i<_to.Size(); i++)
	{
		AIUnit *u = _to[i];
		if (u == unit)
		{
			_to.Delete(i);
			return;
		}
	}
}

void RadioMessageState::ClearTo()
{
	_to.Clear();
}

void RadioMessageState::AddTo(AIUnit *unit)
{
	Assert(!IsTo(unit));
	_to.Add(unit);
}

bool RadioMessageState::IsToSomeone() const
{
	for (int i=0; i<_to.Size(); i++)
	{
		if (_to[i]) return true;
	}
	return false;
}

const char *RadioMessageState::GetPriorityClass()
{
	return "NormalCommand";
}

LSError RadioMessageState::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	CHECK(ar.SerializeRef("From", _from, 1))
	CHECK(ar.SerializeRefs("To", _to, 1))
	return LSOK;
}

///////////////////////////////////////////////////////////////////////////////
// TODO: remove class RadioMessageSemaphore

RadioMessageSemaphore::RadioMessageSemaphore(AIGroup *from, PackedBoolArray list, AI::Semaphore semaphore)
:base(from,list)
{
	_semaphore = semaphore;
}

LSError RadioMessageSemaphore::Serialize(ParamArchive &ar)
{
	return LSOK;
}

const char *RadioMessageSemaphore::GetPriorityClass()
{
	return "Default";
}


const char *RadioMessageSemaphore::PrepareSentence(SentenceParams &params)
{
	return NULL;
}

void RadioMessageSemaphore::Transmitted()
{
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageBehaviour

RadioMessageBehaviour::RadioMessageBehaviour(AIGroup *from, PackedBoolArray list, CombatMode behaviour)
:base(from,list)
{
	_behaviour = behaviour;
}

LSError RadioMessageBehaviour::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	CHECK(ar.SerializeEnum("behaviour", _behaviour, 1, CMAware))
	return LSOK;
}

const char *RadioMessageBehaviour::GetPriorityClass()
{
	return "NormalCommand";
}


const char *RadioMessageBehaviour::PrepareSentence(SentenceParams &params)
{
	if (!_from || !_from->Leader()) return NULL;
	_to.Compact();
	if (_to.Size() <= 0)
		return NULL;

	SetPlayerMsg(IsTo(GWorld->FocusOn()));

	params.Add(_to, true);
	params.Add(_from->Leader());
	switch (_behaviour)
	{
	case CMCareless:
		return "SentBehaviourCareless";
	case CMSafe:
		return "SentBehaviourSafe";
	case CMAware:
		return "SentBehaviourAware";
	case CMCombat:
		return "SentBehaviourCombat";
	case CMStealth:
		return "SentBehaviourStealth";
	default:
		Fail("Behaviour");
		return NULL;
	}
}

void RadioMessageBehaviour::Transmitted()
{
	int i;
	for (i=0; i<_to.Size(); i++)
	{
		AIUnit *unit = _to[i];
		if (unit)
			unit->SetCombatModeMajor(_behaviour);
	}
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageOpenFire

static const EnumName OpenFireNames[]=
{
	EnumName(OFSNeverFire, "NEVER FIRE"),
	EnumName(OFSHoldFire, "HOLD FIRE"),
	EnumName(OFSOpenFire, "OPEN FIRE"),
	EnumName(OFSHoldFire, "0"),
	EnumName(OFSOpenFire, "1"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(OpenFireState dummy)
{
	return OpenFireNames;
}

RadioMessageOpenFire::RadioMessageOpenFire(AIGroup *from, PackedBoolArray list, OpenFireState open)
:base(from,list)
{
	_open = open;
}

LSError RadioMessageOpenFire::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	CHECK(ar.SerializeEnum("open", _open, 1, OFSOpenFire))
	return LSOK;
}

const char *RadioMessageOpenFire::GetPriorityClass()
{
	switch (_open)
	{
	case OFSNeverFire:
	case OFSHoldFire:
		return "UrgentCommand";
	default:
		Fail("Open fire status");
	case OFSOpenFire:
		return "NormalCommand";
	}
}


const char *RadioMessageOpenFire::PrepareSentence(SentenceParams &params)
{
	if (!_from || !_from->Leader()) return NULL;
	_to.Compact();
	if (_to.Size() <= 0)
		return NULL;

	SetPlayerMsg(IsTo(GWorld->FocusOn()));

	params.Add(_to, true);
	params.Add(_from->Leader());
	switch (_open)
	{
	case OFSNeverFire:
	case OFSHoldFire:
		if (_from->GetCombatModeMinor() >= CMCombat)
			return "SentHoldFireInCombat";
		else
			return "SentHoldFire";
	default:
		Fail("Open Fire State");
	case OFSOpenFire:
		if (_from->GetCombatModeMinor() >= CMCombat)
			return "SentOpenFireInCombat";
		else
			return "SentOpenFire";
	}
}

AI::Semaphore ApplyOpenFire(AI::Semaphore s, OpenFireState open)
{
	switch (s)
	{
	case AI::SemaphoreBlue:
		switch (open)
		{
		case OFSHoldFire:
			s = AI::SemaphoreGreen;
			break;
		case OFSOpenFire:
			s = AI::SemaphoreYellow;
			break;
		}
		break;
	case AI::SemaphoreGreen:
		switch (open)
		{
		case OFSNeverFire:
			s = AI::SemaphoreBlue;
			break;
		case OFSOpenFire:
			s = AI::SemaphoreYellow;
			break;
		}
		break;
	case AI::SemaphoreWhite:
		switch (open)
		{
		case OFSNeverFire:
			s = AI::SemaphoreBlue;
			break;
		case OFSOpenFire:
			s = AI::SemaphoreRed;
			break;
		}
		break;
	case AI::SemaphoreYellow:
		switch (open)
		{
		case OFSNeverFire:
			s = AI::SemaphoreBlue;
			break;
		case OFSHoldFire:
			s = AI::SemaphoreGreen;
			break;
		}
		break;
	case AI::SemaphoreRed:
		switch (open)
		{
		case OFSNeverFire:
			s = AI::SemaphoreBlue;
			break;
		case OFSHoldFire:
			s = AI::SemaphoreWhite;
			break;
		}
		break;
	}
	return s;
}

void RadioMessageOpenFire::Transmitted()
{
	int i;
	for (i=0; i<_to.Size(); i++)
	{
		AIUnit *unit = _to[i];
		if (unit)
		{
			AI::Semaphore s = unit->GetSemaphore();
			s = ApplyOpenFire(s, _open);
			unit->SetSemaphore(s);
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageCeaseFire

RadioMessageCeaseFire::RadioMessageCeaseFire(AIUnit *from, AIUnit *to, bool insideGroup)
{
	_from = from;
	_to = to;
	_insideGroup = insideGroup;
}

LSError RadioMessageCeaseFire::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	CHECK(ar.SerializeRef("from", _from, 1))
	CHECK(ar.SerializeRef("to", _to, 1))
	CHECK(ar.Serialize("insideGroup", _insideGroup, 1, true))
	return LSOK;
}

const char *RadioMessageCeaseFire::GetPriorityClass()
{
	return "UrgentCommand";
}


const char *RadioMessageCeaseFire::PrepareSentence(SentenceParams &params)
{
	if (!_from || !_to || !_to->GetGroup()) return NULL;
	if (_from->GetLifeState() != AIUnit::LSAlive) return NULL;
	SetPlayerMsg(_to == GWorld->FocusOn());

	if (_insideGroup)
	{
		params.Add(_to);
		return "SentCeaseFireInsideGroup";
	}
	else
	{
		params.AddWord("", _to->GetGroup()->GetName());
		params.Add(_to);
		return "SentCeaseFire";
	}
}

void RadioMessageCeaseFire::Transmitted()
{
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageLooseFormation

RadioMessageLooseFormation::RadioMessageLooseFormation(AIGroup *from, PackedBoolArray list, bool loose)
:base(from,list)
{
	_loose = loose;
}

LSError RadioMessageLooseFormation::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	CHECK(ar.Serialize("loose", _loose, 1, false))
	return LSOK;
}

const char *RadioMessageLooseFormation::GetPriorityClass()
{
	return "NormalCommand";
}


const char *RadioMessageLooseFormation::PrepareSentence(SentenceParams &params)
{
	if (!_from || !_from->Leader()) return NULL;
	_to.Compact();
	if (_to.Size() <= 0)
		return NULL;

	SetPlayerMsg(IsTo(GWorld->FocusOn()));

	params.Add(_to, true);
	params.Add(_from->Leader());
	if (_loose)
		return "SentLooseFormation";
	else
		return "SentKeepFormation";
}

AI::Semaphore ApplyLooseFormation(AI::Semaphore s, bool loose)
{
	switch (s)
	{
	case AI::SemaphoreBlue:
		if (loose) s = AI::SemaphoreWhite;
		break;
	case AI::SemaphoreGreen:
		if (loose) s = AI::SemaphoreWhite;
		break;
	case AI::SemaphoreWhite:
		if (!loose) s = AI::SemaphoreGreen;
		break;
	case AI::SemaphoreYellow:
		if (loose) s = AI::SemaphoreRed;
		break;
	case AI::SemaphoreRed:
		if (!loose) s = AI::SemaphoreYellow;
		break;
	}
	return s;
}

void RadioMessageLooseFormation::Transmitted()
{
	int i;
	for (i=0; i<_to.Size(); i++)
	{
		AIUnit *unit = _to[i];
		if (unit)
		{
			AI::Semaphore s = unit->GetSemaphore();
			s = ApplyLooseFormation(s, _loose);
			unit->SetSemaphore(s);
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageFormationPos

static const EnumName FormationPosNames[]=
{
	EnumName(UPUp, "Up"),
	EnumName(UPDown, "Down"),
	EnumName(UPAuto, "Auto"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(AI::FormationPos dummy)
{
	return FormationPosNames;
}

RadioMessageFormationPos::RadioMessageFormationPos(AIGroup *from, PackedBoolArray list, AI::FormationPos state)
:base(from,list)
{
	_state = state;
}

LSError RadioMessageFormationPos::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	CHECK(ar.SerializeEnum("state", _state, 1, AI::PosInFormation))
	return LSOK;
}

const char *RadioMessageFormationPos::PrepareSentence(SentenceParams &params)
{
	if (!_from || !_from->Leader()) return NULL;
	_to.Compact();
	if (_to.Size() <= 0)
		return NULL;

	SetPlayerMsg(IsTo(GWorld->FocusOn()));

	const char *sentence;
	
	params.Add(_to, true);
	params.Add(_from->Leader());
	switch (_state)
	{
		case AI::PosAdvance: sentence = "SentFormPosAdvance"; break;
		case AI::PosStayBack: sentence = "SentFormPosStayBack"; break;
		case AI::PosFlankLeft: sentence = "SentFormPosFlankLeft"; break;
		case AI::PosFlankRight: sentence = "SentFormPosFlankRight"; break;
		default: Fail("FormationPos"); return NULL;
	}

	return sentence;
}

void RadioMessageFormationPos::Transmitted()
{
	int i;
	for (i=0; i<_to.Size(); i++)
	{
		AIUnit *unit = _to[i];
		if (unit)
			unit->AddFormationPos(_state);
	}
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageUnitPos

static const EnumName UnitPosNames[]=
{
	EnumName(UPUp, "Up"),
	EnumName(UPDown, "Down"),
	EnumName(UPAuto, "Auto"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(UnitPosition dummy)
{
	return UnitPosNames;
}

RadioMessageUnitPos::RadioMessageUnitPos(AIGroup *from, PackedBoolArray list, UnitPosition state)
:base(from,list)
{
	_state = state;
}

LSError RadioMessageUnitPos::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	CHECK(ar.SerializeEnum("state", _state, 1, (UnitPosition)UPAuto))
	return LSOK;
}

const char *RadioMessageUnitPos::PrepareSentence(SentenceParams &params)
{
	if (!_from || !_from->Leader()) return NULL;
	_to.Compact();
	if (_to.Size() <= 0)
		return NULL;

	SetPlayerMsg(IsTo(GWorld->FocusOn()));

	const char *sentence;
	
	params.Add(_to, true);
	params.Add(_from->Leader());
	switch (_state)
	{
		case UPDown: sentence = "SentUnitPosDown"; break;
		case UPUp: sentence = "SentUnitPosUp"; break;
		case UPAuto: sentence = "SentUnitPosAuto"; break;
		default: Fail("UnitPos"); return NULL;
	}

	return sentence;
}

void RadioMessageUnitPos::Transmitted()
{
	int i;
	for (i=0; i<_to.Size(); i++)
	{
		AIUnit *unit = _to[i];
		if (unit)
			unit->SetUnitPosition(_state);
	}
}

/////////////////////////////////

RadioMessageWatchAround::RadioMessageWatchAround(AIGroup *from, PackedBoolArray list)
:base(from,list)
{
}

LSError RadioMessageWatchAround::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	return LSOK;
}

void RadioMessageWatchAround::Transmitted()
{
	int i;
	for (i=0; i<_to.Size(); i++)
	{
		AIUnit *unit = _to[i];
		if (!unit) continue;
		unit->SetWatchAround();
	}
}

const char *RadioMessageWatchAround::PrepareSentence(SentenceParams &params)
{
	if (!_from || !_from->Leader()) return NULL;
	_to.Compact();
	if (_to.Size() <= 0) return NULL;

	SetPlayerMsg(IsTo(GWorld->FocusOn()));

	const char *sentence = "SentWatchAround";
	params.Add(_to, false);
	params.Add(_from->Leader());
	return sentence;	
}

/////////////////////////////////

RadioMessageWatchAuto::RadioMessageWatchAuto(AIGroup *from, PackedBoolArray list)
:base(from,list)
{
}

LSError RadioMessageWatchAuto::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	return LSOK;
}

void RadioMessageWatchAuto::Transmitted()
{
	int i;
	for (i=0; i<_to.Size(); i++)
	{
		AIUnit *unit = _to[i];
		if (!unit) continue;
		unit->SetNoWatch();
	}
}

const char *RadioMessageWatchAuto::PrepareSentence(SentenceParams &params)
{
	if (!_from || !_from->Leader()) return NULL;
	_to.Compact();
	if (_to.Size() <= 0) return NULL;

	SetPlayerMsg(IsTo(GWorld->FocusOn()));

	const char *sentence = "SentNoTarget";
	params.Add(_to, false);
	params.Add(_from->Leader());
	return sentence;	
}

/////////////////////////////////

RadioMessageWatchDir::RadioMessageWatchDir(AIGroup *from, PackedBoolArray list, Vector3Val dir)
:base(from,list)
{
	_dir = dir;
}

LSError RadioMessageWatchDir::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	CHECK(ar.Serialize("dir", _dir, 1))
	return LSOK;
}

void RadioMessageWatchDir::Transmitted()
{
	int i;
	for (i=0; i<_to.Size(); i++)
	{
		AIUnit *unit = _to[i];
		if (!unit) continue;
		unit->SetWatchDirection(_dir);
	}
}

const char *RadioMessageWatchDir::PrepareSentence(SentenceParams &params)
{
	if (!_from || !_from->Leader()) return NULL;
	_to.Compact();
	if (_to.Size() <= 0) return NULL;

	SetPlayerMsg(IsTo(GWorld->FocusOn()));

	const char *sentence = "SentWatchDir";
	params.Add(_to, false);
	params.AddAzimutDir(_dir);
	::ShowGroupDir(_to);
	params.Add(_from->Leader());
	return sentence;	
}

/////////////////////////////////

RadioMessageWatchPos::RadioMessageWatchPos(AIGroup *from, PackedBoolArray list, Vector3Val pos)
:base(from,list)
{
	_pos = pos;
}

LSError RadioMessageWatchPos::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	CHECK(ar.Serialize("pos", _pos, 1))
	return LSOK;
}

void RadioMessageWatchPos::Transmitted()
{
	int i;
	for (i=0; i<_to.Size(); i++)
	{
		AIUnit *unit = _to[i];
		if (!unit) continue;
		unit->SetWatchPosition(_pos);
	}
}

const char *RadioMessageWatchPos::PrepareSentence(SentenceParams &params)
{
	if (!_from || !_from->Leader()) return NULL;
	_to.Compact();
	if (_to.Size() <= 0) return NULL;

	SetPlayerMsg(IsTo(GWorld->FocusOn()));

	const char *sentence = "SentWatchPos";
	params.Add(_to, false);
	params.AddAzimut(_pos);
	::ShowGroupDir(_to);
	params.Add(_from->Leader());
	return sentence;	
}

/////////////////////////////////

RadioMessageWatchTgt::RadioMessageWatchTgt(AIGroup *from, PackedBoolArray list, Target *tgt)
:base(from,list)
{
	_tgt = tgt;
}

LSError RadioMessageWatchTgt::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	CHECK(ar.SerializeRef("tgt", _tgt, 1))
	return LSOK;
}

void RadioMessageWatchTgt::Transmitted()
{
	int i;
	for (i=0; i<_to.Size(); i++)
	{
		AIUnit *unit = _to[i];
		if (!unit) continue;
		unit->SetWatchTarget(_tgt);
	}
}

const char *RadioMessageWatchTgt::PrepareSentence(SentenceParams &params)
{
	if (!_from || !_from->Leader()) return NULL;
	_to.Compact();
	if (_to.Size() <= 0) return NULL;

	SetPlayerMsg(IsTo(GWorld->FocusOn()));

	const char *sentence = "SentWatchTgt";
	params.Add(_to, false);

	Target *tgt=_tgt;
	if (tgt)
	{
		params.AddWord(tgt->type->GetNameSound(), tgt->type->GetDisplayName());
	}
	else
	{
		params.AddWord("unknown", LocalizeString(IDS_WORD_UNKNOWN));
	}
	params.Add(_from->Leader());
	return sentence;	
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageReportStatus

RadioMessageReportStatus::RadioMessageReportStatus(AIGroup *from, PackedBoolArray list)
	: base(from,list)
{
}

LSError RadioMessageReportStatus::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	return LSOK;
}

const char *RadioMessageReportStatus::GetPriorityClass()
{
	return "NormalCommand";
}

const char *RadioMessageReportStatus::PrepareSentence(SentenceParams &params)
{
	if (!_from || !_from->Leader()) return NULL;
	_to.Compact();
	if (_to.Size() <= 0)
		return NULL;

	SetPlayerMsg(IsTo(GWorld->FocusOn()));

	const char *sentence;
	
	sentence = "SentReportStatus";
	params.Add(_to, true);
	params.Add(_from->Leader());

	return sentence;
}

void RadioMessageReportStatus::Transmitted()
{
	int i;
	for (i=0; i<_to.Size(); i++)
	{
		AIUnit *unit = _to[i];
		if (!unit) continue;
		if (unit->GetLifeState()==AIUnit::LSAlive)
		{
			unit->ReportStatus();
			AIGroup *grp = unit->GetGroup();
			if (grp)
			{
				// reset status indication here
				// if it should be set, unit will report it soon
				grp->SetHealthStateReported(unit,AIUnit::RSNormal);
				grp->SetDammageStateReported(unit,AIUnit::RSNormal);
				grp->SetFuelStateReported(unit,AIUnit::RSNormal);
				grp->SetAmmoStateReported(unit,AIUnit::RSNormal);
			}
		}
		else
		{
			AIGroup *grp = unit->GetGroup();
			if (grp)
			{
				grp->SetReportBeforeTime(unit,Glob.time+5);
			}			
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageTarget

RadioMessageTarget::RadioMessageTarget()
{
	_engage = false;
	_fire = false;
}

RadioMessageTarget::RadioMessageTarget
(
	AIGroup *from, PackedBoolArray list, Target *target,
	bool engage, bool fire
)
: base(from,list)
{
	_target = target;
	_engage = engage;
	_fire = fire;
}

LSError RadioMessageTarget::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	CHECK(ar.SerializeRef("target", _target, 1))
	CHECK(ar.Serialize("engage", _engage, 1, false))
	CHECK(ar.Serialize("fire", _fire, 1, false))
	return LSOK;
}

/*!
\patch 1.29 Date 10/31/2001 by Jirka
- Fixed: Target cursor shown in MP game
*/
void RadioMessageTarget::Transmitted()
{
	if (_target && (_target->destroyed || _target->vanished))
	{
		return;
	}
	if (_target)
	{
		if (IsTo(GWorld->FocusOn()) && GWorld->UI())
			GWorld->UI()->ShowTarget();
	}
	for (int i=0; i<_to.Size(); i++)
	{
		AIUnit *to = _to[i];
		if (!to) continue;
		if (_target)
		{
			VehicleWithAI *tgtAI = _target->idExact;
			if (to->GetPerson()->IsRemotePlayer())
			{
				GetNetworkManager().ShowTarget(to->GetPerson(), tgtAI);
			}
			if (tgtAI == to->GetVehicleIn() || tgtAI==to->GetPerson())
			{
				// unit cannot target itself
				continue;
			}
		}
		if (_engage)
		{	
			Target *tgt = _target ? (Target*)_target : to->GetTargetAssigned();
			to->EngageTarget(tgt);
		}
		if (_fire)
		{	
			Target *tgt = _target ? (Target*)_target : to->GetTargetAssigned();
			to->EnableFireTarget(tgt);
		}
		if (_target)
		{
			to->AssignTarget(_target);
			VehicleWithAI *veh = to->GetVehicle();
			veh->BegAttack(_target);
		}
	}
}

void RadioMessageTarget::Canceled()
{
	for (int i=0; i<_to.Size(); i++)
	{
		AIUnit *to = _to[i];
		if (!to) continue;
		to->GetGroup()->UnitAssignCanceled(to);
	}
}

const char *RadioMessageTarget::GetPriorityClass()
{
	return "UrgentCommand";
}

const char *RadioMessageTarget::PrepareSentence(SentenceParams &params)
{
	if (!_from || !_from->Leader()) return NULL;
	_to.Compact();
	if (_to.Size() <= 0) return NULL;

	SetPlayerMsg(IsTo(GWorld->FocusOn()));

	Target *target = _target;
	const char *sentence = "SentNoTarget";
	if (_engage && _fire) sentence = "SentAttackNoTarget";
	else if (_engage) sentence = "SentEngageNoTarget";
	else if (_fire) sentence = "SentFireNoTarget";
	params.Add(_to, false);

	if (target)
	{
		sentence = "SentTarget";
		if (_engage && _fire) sentence = "SentCmdAttack";
		else if (_engage) sentence = "SentEngage";
		else if (_fire) sentence = "SentFire";
	}

	if (target)
	{
		if (target->destroyed || target->vanished)
		{
			return NULL;
		}
		params.AddWord(target->type->GetNameSound(), target->type->GetDisplayName());
	}
	else
	{
		params.AddWord("unknown", LocalizeString(IDS_WORD_UNKNOWN));
	}
	params.Add(_from->Leader());
	return sentence;	
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageTeam

RadioMessageTeam::RadioMessageTeam(AIGroup *from, PackedBoolArray list, Team team)
{
	_from = from;
	int i;
	for (i=0; i<MAX_UNITS_PER_GROUP; i++)
		if (list.Get(i))
		{
			AIUnit *unit = from->UnitWithID(i + 1);
			if (unit)
				_to.Add(unit);
		}
	_team = team;
}

LSError RadioMessageTeam::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.SerializeRef("From", _from, 1))
	CHECK(ar.SerializeRefs("To", _to, 1))
	CHECK(ar.SerializeEnum("team", _team, 1, TeamMain))
	return LSOK;
}

const char *RadioMessageTeam::GetPriorityClass()
{
	return "NormalCommand";
}

void RadioMessageTeam::Transmitted()
{
	for (int i=0; i<_to.Size(); i++)
	{
		AIUnit *u = _to[i];
		if (!u) continue;
		if (u->GetGroup()!=_from) continue;
		SetTeam(u->ID() - 1, _team);
	}
}

bool RadioMessageTeam::IsTo(AIUnit *unit) const
{
	Assert(unit);

	int i;
	for (i=0; i<_to.Size(); i++)
	{
		AIUnit *u = _to[i];
		if (u == unit)
			return true;
	}
	return false;
}

const char *RadioMessageTeam::PrepareSentence(SentenceParams &params)
{
	if (!_from || !_from->Leader()) return NULL;
	_to.Compact();
	if (_to.Size() <= 0) return NULL;
	
	SetPlayerMsg(IsTo(GWorld->FocusOn()));
	
	params.Add(_to, false);
	switch (_team)
	{
	case TeamMain:
		params.AddWord("whiteTeam", LocalizeString(IDS_WORD_TEAM_MAIN));
		break;
	case TeamRed:
		params.AddWord("redTeam", LocalizeString(IDS_WORD_TEAM_RED));
		break;
	case TeamGreen:
		params.AddWord("greenTeam", LocalizeString(IDS_WORD_TEAM_GREEN));
		break;
	case TeamBlue:
		params.AddWord("blueTeam", LocalizeString(IDS_WORD_TEAM_BLUE));
		break;
	case TeamYellow:
		params.AddWord("yellowTeam", LocalizeString(IDS_WORD_TEAM_YELLOW));
		break;
	}
	params.Add(_from->Leader());
	return "SentTeam";
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageCommand

RadioMessageCommand::RadioMessageCommand(AIGroup *from, PackedBoolArray list, Command &command, bool toMainSubgroup, bool transmit)
{
	_from = from;
	int i;
	for (i=0; i<MAX_UNITS_PER_GROUP; i++)
		if (list.Get(i))
		{
			AIUnit *unit = from->UnitWithID(i + 1);
			if (unit)
				_to.Add(unit);
		}
	_toMainSubgroup = toMainSubgroup;
	_transmit = transmit;
	_command = command;
}

LSError RadioMessageCommand::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.SerializeRef("From", _from, 1))
	CHECK(ar.SerializeRefs("To", _to, 1))
	CHECK(ar.Serialize("Command", _command, 1))
	CHECK(ar.Serialize("toMainSubgrp", _toMainSubgroup, 1, false))
	CHECK(ar.Serialize("transmit", _transmit, 1, true))
	return LSOK;
}

const char *RadioMessageCommand::GetPriorityClass()
{
	if
	(
		_command._context == Command::CtxUI ||
		_command._context == Command::CtxUIWithJoin
	)
		return "UICommand";
	else switch (_command._message)
	{
	case Command::Move:
	case Command::Heal:
	case Command::Repair:
	case Command::Refuel:
	case Command::Rearm:
	case Command::Support:
	case Command::GetIn:
	case Command::GetOut:
	case Command::Join:
		return "NormalCommand";
	case Command::Attack:
	#if ENABLE_HOLDFIRE_FIX
	case Command::AttackAndFire:
	#endif
	case Command::Fire:
		return "UrgentCommand";
	default:
		return "Default";
	}
}

bool RadioMessageCommand::IsTo(AIUnit *unit)
{
	// unit may be NULL in MP
	//Assert(unit);

	if (_toMainSubgroup)
		return _from && unit->GetSubgroup() == _from->MainSubgroup();

	int i;
	for (i=0; i<_to.Size(); i++)
	{
		AIUnit *u = _to[i];
		if (u == unit)
			return true;
	}
	return false;
}

void RadioMessageCommand::DeleteTo(AIUnit *unit)
{
	Assert(unit);

	if (_toMainSubgroup)
		return;

	int i;
	for (i=0; i<_to.Size(); i++)
	{
		AIUnit *u = _to[i];
		if (u == unit)
		{
			_to.Delete(i);
			return;
		}
	}
}

Object *FindNearestMoveObject(Vector3Par pos)
{
	if (!GWorld->CameraOn()) return NULL;
	float dist2 = pos.Distance2(GWorld->CameraOn()->Position());
	if (dist2 > Square(400)) return NULL;
	float diff = 0.25 * sqrt(dist2);
	saturate(diff, 5.0, 100.0);
	int xMin,xMax,zMin,zMax;
	ObjRadiusRectangle(xMin,xMax,zMin,zMax,pos,pos,diff);

	Object *best = NULL;
	const float minDist2 = diff * diff;
	float minFunc = FLT_MAX;
	for (int x=xMin; x<=xMax; x++)
		for (int z=zMin; z<=zMax; z++)
		{
			const ObjectList &list = GLandscape->GetObjects(z, x);
			int n = list.Size();
			for (int i=0; i<n; i++)
			{
				Object *obj = list[i];
				if (obj->GetType() == Network) continue;
				if (obj->GetType() == Temporary) continue;
				if (obj->GetType() == TypeTempVehicle) continue;
				if (!obj->IsMoveTarget()) continue;
				float dist2 = pos.Distance2(obj->Position());
				if (dist2 >= minDist2) continue;
				float func = dist2 * obj->GetInvMass();
				if (func >= minFunc) continue;
				minFunc = func ;
				best = obj;
			}
		} // (for all near objects)
	return best;
}

/*!
\patch 1.01 Date 6/25/2001 by Ondra.
- Fixed: Get In UNKNOWN.
*/

const char *RadioMessageCommand::PrepareSentence(SentenceParams &params)
{
	if 
	(
		!_from ||
		!_from->Leader() ||
		_from->Leader()->GetLifeState()!=AIUnit::LSAlive ||
		_command._message == Command::NoCommand ||
		_command._message == Command::Wait ||
		_command._context == Command::CtxUndefined ||
		_command._context == Command::CtxJoin
	)
		return NULL;

	if
	( 
		!_command._target && !_command._targetE &&
		(
			_command._message == Command::Fire ||
			_command._message == Command::Attack ||
			_command._message == Command::AttackAndFire
		)
	)
	{
		return NULL;
	}
	if
	(
		!_command._target &&
		(
			_command._message == Command::GetIn ||
			_command._message == Command::Heal ||
			_command._message == Command::Repair ||
			_command._message == Command::Refuel ||
			_command._message == Command::Rearm ||
			_command._message == Command::Support
		)
	)
		return NULL;

	const char *sentence;
	AIUnit *first = NULL;
	if (_toMainSubgroup)
	{
		params.AddWord("allGroup", LocalizeString(IDS_WORD_ALLGROUP));
		first = _from->MainSubgroup()->Leader();
		if (!first) first = _from->Leader();
	}
	else
	{
		_to.Compact();
		if (_to.Size() <= 0) return NULL;
		bool wholeCrew =
			_command._message != Command::GetIn &&
			_command._message != Command::GetOut;
		params.Add(_to, wholeCrew);
		first = _to[0];

		SetPlayerMsg(IsTo(GWorld->FocusOn()));
	}
	Assert(first);

	// say command
	Target *tgt = NULL;
	switch (_command._message)
	{
	case Command::Join:
		{
			AISubgroup *subgroup = _command._joinToSubgroup;
			if (!subgroup) subgroup = _from->MainSubgroup();
			Assert(subgroup);
			if (!subgroup->Leader())
			{
				sentence = "SentCmdFollow";
				params.AddWord("allGroup", LocalizeString(IDS_WORD_ALLGROUP));
			}
			else if (subgroup->Leader() == _from->Leader())
			{
				sentence = "SentCmdFollowMe";
			}
			else
			{
				sentence = "SentCmdFollow";
				params.Add(subgroup->Leader());
			}
		}
		break;
	case Command::GetOut:
		sentence = "SentCmdGetOut";
		break;
	case Command::Move:
		tgt = _command._targetE;
		if (!tgt) tgt = _from->FindTarget(_command._target);
		if (tgt && tgt->type->GetDisplayName().GetLength() > 0)
		{
			sentence = "SentCmdMoveTo";
			goto CmdSupplyTarget;
		}
		else if (_command._destination.Distance2(_from->Leader()->Position()) > Square(400))
		{
			sentence = "SentCmdMoveFar";
			goto CmdPositionFar;
		}
		else
		{
			Object *obj = FindNearestMoveObject(_command._destination);
			if (obj)
			{
				sentence = "SentCmdMoveNear";
				params.AddWord(obj->GetNameSound(), obj->GetDisplayName());
				params.AddAzimut(obj->Position());
				::ShowGroupDir(_to);
				break;
			}
			else
			{
				sentence = "SentCmdMove";
				goto CmdPosition;
			}
		}
	case Command::Stop:
		sentence = "SentCmdStop";
		break;
	case Command::Expect:
		sentence = "SentCmdExpect";
		break;
	case Command::Hide:
		sentence = "SentCmdHide";
		break;
	case Command::Heal:
Heal:
		tgt = _command._targetE;
		if (!tgt) tgt = _from->FindTarget(_command._target);
		if (tgt && tgt->type->GetDisplayName().GetLength() > 0)
		{
			sentence = "SentCmdHealAt";
			goto CmdSupplyTarget;
		}
		else if (_command._destination.Distance2(_from->Leader()->Position()) > Square(400))
		{
			sentence = "SentCmdHealFar";
			goto CmdPositionFar;
		}
		else
		{
			sentence = "SentCmdHeal";
			goto CmdPosition;
		}
	case Command::Repair:
Repair:
		tgt = _command._targetE;
		if (!tgt) tgt = _from->FindTarget(_command._target);
		if (tgt && tgt->type->GetDisplayName().GetLength() > 0)
		{
			sentence = "SentCmdRepairAt";
			goto CmdSupplyTarget;
		}
		else if (_command._destination.Distance2(_from->Leader()->Position()) > Square(400))
		{
			sentence = "SentCmdRepairFar";
			goto CmdPositionFar;
		}
		else
		{
			sentence = "SentCmdRepair";
			goto CmdPosition;
		}
	case Command::Refuel:
Refuel:
		tgt = _command._targetE;
		if (!tgt) tgt = _from->FindTarget(_command._target);
		if (tgt && tgt->type->GetDisplayName().GetLength() > 0)
		{
			sentence = "SentCmdRefuelAt";
			goto CmdSupplyTarget;
		}
		else if (_command._destination.Distance2(_from->Leader()->Position()) > Square(400))
		{
			sentence = "SentCmdRefuelFar";
			goto CmdPositionFar;
		}
		else
		{
			sentence = "SentCmdRefuel";
			goto CmdPosition;
		}
	case Command::Rearm:
Rearm:
		tgt = _command._targetE;
		if (!tgt) tgt = _from->FindTarget(_command._target);
		if (tgt && tgt->type->GetDisplayName().GetLength() > 0)
		{
			sentence = "SentCmdRearmAt";
			goto CmdSupplyTarget;
		}
		else if (_command._destination.Distance2(_from->Leader()->Position()) > Square(400))
		{
			sentence = "SentCmdRearmFar";
			goto CmdPositionFar;
		}
		else
		{
			sentence = "SentCmdRearm";
			goto CmdPosition;
		}
	case Command::Support:
		tgt = _command._targetE;
		if (!tgt) tgt = _from->FindTarget(_command._target);
		if (tgt && tgt->type->GetDisplayName().GetLength() > 0)
		{
			sentence = "SentCmdSupportAt";
			goto CmdSupplyTarget;
		}
		else if (_command._destination.Distance2(_from->Leader()->Position()) > Square(400))
		{
			sentence = "SentCmdSupportFar";
			goto CmdPositionFar;
		}
		else
		{
			sentence = "SentCmdSupport";
			goto CmdPosition;
		}
	case Command::Attack:
	#if ENABLE_HOLDFIRE_FIX
	case Command::AttackAndFire:
	#endif
		sentence = "SentCmdAttack";
		goto CmdTarget;
	case Command::Fire:
		sentence = "SentCmdFire";
		goto CmdTarget;
	case Command::GetIn:
		sentence = "SentCmdGetIn";
		if (!_toMainSubgroup)
		{
			if (_to.Size() == 1)
			{
				Transport *veh = first->VehicleAssigned();
				if (veh == _command._target)
				{
					if (first == veh->GetCommanderAssigned())
						sentence = "SentCmdGetInCommander";
					else if (first == veh->GetDriverAssigned())
					{
						if (veh->GetType()->IsKindOf(GWorld->Preloaded(VTypeAir)))
							sentence = "SentCmdGetInPilot";
						else
							sentence = "SentCmdGetInDriver";
					}
					else if (first == veh->GetGunnerAssigned())
						sentence = "SentCmdGetInGunner";
					else
						for (int i=0; i<veh->NCargoAssigned(); i++)
							if (first == veh->GetCargoAssigned(i))
							{
								sentence = "SentCmdGetInCargo";
								break;
							}
				}
			}
			else
			{
				// check if all units gets in cargo
				bool ok = true;
				Transport *veh = first->VehicleAssigned();
				if (veh)
				{
					for (int j=0; j<_to.Size(); j++)
					{
						AIUnit *unit = _to[j];
						if (unit->VehicleAssigned() != veh)
						{
							ok = false; break;
						}
						bool found = false;
						for (int i=0; i<veh->NCargoAssigned(); i++)
							if (unit == veh->GetCargoAssigned(i))
							{
								found = true; break;
							}
						if (!found)
						{
							ok = false; break;
						}
					}
				}
				else ok = false;
				if (ok) sentence = "SentCmdGetInCargo";
			}
		}
		goto CmdTarget;
	case Command::Action:
		{
			switch (_command._action)
			{
			case ATHeal:
				goto Heal;
			case ATRepair:
				goto Repair;
			case ATRefuel:
				goto Refuel;
			case ATRearm:
				goto Rearm;
			case ATTakeWeapon:
				tgt = _command._targetE;
				if (!tgt) tgt = _from->FindTarget(_command._target);
				if (tgt && tgt->type->GetDisplayName().GetLength() > 0)
				{
					sentence = "SentCmdTakeWeaponAt";
					goto CmdSupplyTarget;
				}
				else if (_command._destination.Distance2(_from->Leader()->Position()) > Square(400))
				{
					sentence = "SentCmdTakeWeaponFar";
					goto CmdPositionFar;
				}
				else
				{
					sentence = "SentCmdTakeWeapon";
					goto CmdPosition;
				}
			case ATTakeMagazine:
				tgt = _command._targetE;
				if (!tgt) tgt = _from->FindTarget(_command._target);
				if (tgt && tgt->type->GetDisplayName().GetLength() > 0)
				{
					sentence = "SentCmdTakeMagazineAt";
					goto CmdSupplyTarget;
				}
				else if (_command._destination.Distance2(_from->Leader()->Position()) > Square(400))
				{
					sentence = "SentCmdTakeMagazineFar";
					goto CmdPositionFar;
				}
				else
				{
					sentence = "SentCmdTakeMagazine";
					goto CmdPosition;
				}
			default:
				{
					UIAction action;
					action.type = _command._action;
					action.target = _command._target;
					action.param = _command._param;
					action.param2 = _command._param2;
					action.param3 = _command._param3;
					action.priority = 0;
					action.showWindow = false;
					action.hideOnUse = false;
					RString name = action.GetDisplayName(NULL);
					params.AddWord("", name);

					tgt = _command._targetE;
					if (!tgt) tgt = _from->FindTarget(_command._target);
					if (tgt && tgt->type->GetDisplayName().GetLength() > 0)
					{
						sentence = "SentCmdActionAt";
						goto CmdSupplyTarget;
					}
					else if (_command._destination.Distance2(_from->Leader()->Position()) > Square(400))
					{
						sentence = "SentCmdActionFar";
						goto CmdPositionFar;
					}
					else
					{
						sentence = "SentCmdAction";
						goto CmdPosition;
					}
				}
			}
		}
	default:
		Fail("Unknown command.");
		return NULL;
CmdPosition:
		{
			Vector3 dir = _command._destination - first->Position();
			int azimut = toInt(0.1 * atan2(dir.X(), dir.Z()) * (180 / H_PI));
			if (azimut < 0) azimut += 36;
			int dist = toInt(0.1 * dir.SizeXZ());
			params.Add("%02d", azimut);
			params.Add(dist);
		}
		break;
CmdPositionFar:
		{
			char buffer[6];
			PositionToAA11(_command._destination, buffer);
			params.Add(RString(buffer));
		}
		break;
CmdSupplyTarget:
		{
			params.AddWord(tgt->type->GetNameSound(), tgt->type->GetDisplayName());
			params.AddAzimut(tgt->position);
			::ShowGroupDir(_to);
		}
		break;
CmdTarget:
		{
			tgt=_command._targetE;
			if( !tgt )tgt=_from->FindTarget(_command._target);

			// check if we should use center 
			if (tgt)
			{
				params.AddWord(tgt->type->GetNameSound(), tgt->type->GetDisplayName());
			}
			else
			{
				// FIX tgt not found - use center database
				AICenter *center = _from->GetCenter();
				AITargetInfo *aiTgt = center->FindTargetInfo(_command._target);
				if (aiTgt)
				{
					params.AddWord(aiTgt->_type->GetNameSound(), aiTgt->_type->GetDisplayName());
				}
				else
				{
					params.AddWord("unknown", LocalizeString(IDS_WORD_UNKNOWN));
				}
			}
		}
		break;
	}
	params.Add(_from->Leader());

	return sentence;
}

/*!
\patch 1.34 Date 12/06/2001 by Ondra
- Fixed: MP: Player character autoresponded "Ready" when given command.
*/

void RadioMessageCommand::Transmitted()
{
	if (!_transmit) return;

	if (!_from)	return;
	if (!_from->Leader())	return;
	if (_from->Leader()->GetLifeState()!=AIUnit::LSAlive) return;

	if
	( 
		!_command._target && !_command._targetE &&
		(
			_command._message == Command::Attack ||
			_command._message == Command::AttackAndFire ||
			_command._message == Command::Fire
		)
	)
	{
		return;
	}
	if
	(
		!_command._target &&
		(
			_command._message == Command::GetIn ||
			_command._message == Command::Heal ||
			_command._message == Command::Repair ||
			_command._message == Command::Refuel ||
			_command._message == Command::Rearm ||
			_command._message == Command::Support
		)
	)
		return;

	bool confirm = 
		_command._message != Command::NoCommand &&
		_command._message != Command::Wait &&
		_command._context != Command::CtxUndefined &&
		_command._context != Command::CtxJoin;

	AIUnit *unit = NULL;
	if (_toMainSubgroup)
	{
		_from->MainSubgroup()->ReceiveCommand(_command);
		if (confirm)
		{
			unit = _from->MainSubgroup()->Leader();
			if (!unit) unit = _from->Leader();
		}
	}
	else
	{
		if (_to.Count() > 0)
		{
			bool wholeCrew =
				_command._message != Command::GetIn &&
				_command._message != Command::GetOut;
			PackedBoolArray list = PrepareList
			(
				_from, GetUnitsList(_from,_to), wholeCrew
			);
			if (!list.IsEmpty())
			{
				_from->IssueCommand(_command, list);
				if (confirm)
				{
					unit = NULL;
					for (int i=0; i<_to.Size(); i++)
					{
						unit = _to[i];
						if (unit) break;
					}
					Assert(unit);
				}
			}
		}
	}

	if (unit)
	{
		//if (unit->GetLifeState()==AIUnit::LSAlive && !unit->IsPlayer())
		if (!unit->IsAnyPlayer())
		{
			_from->GetRadio().Transmit
			(
				new RadioMessageCommandConfirm(unit, _from, _command),
				_from->GetCenter()->GetLanguage()
			);
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageText

LSError RadioMessageText::Serialize(ParamArchive &ar)
{
	CHECK(RadioMessage::Serialize(ar))
	CHECK(ar.Serialize("wave", _wave, 1))
	CHECK(ar.Serialize("ttl", _timeToLive, 1))
	CHECK(ar.SerializeRef("sender", _sender, 1))
	CHECK(ar.Serialize("senderName", _senderName, 1, RString()))

	return LSOK;
}

const char *RadioMessageText::GetPriorityClass()
{
	return "Design";
}

const char *RadioMessageText::PrepareSentence(SentenceParams &params)
{
	return NULL;
}

void RadioMessageText::Transmitted()
{
}

RString RadioMessageText::GetWave()
{
	return _wave;
}

float RadioMessageText::GetDuration() const
{
	return _timeToLive;
}
