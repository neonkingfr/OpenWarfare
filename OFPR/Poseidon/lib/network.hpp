#ifdef _MSC_VER
#pragma once
#endif

#ifndef _NETWORK_HPP
#define _NETWORK_HPP

#include "networkObject.hpp"
#include "netTransport.hpp"
#include <Es/Strings/rString.hpp>
#include <Es/Types/pointers.hpp>
#include <Es/Containers/staticArray.hpp>
#include "time.hpp"
#include "vehicleAI.hpp"

/*!
\file
Basic interface file for multiplayer game
*/

#define NO_PLAYER								0
#define AI_PLAYER								1

#define STATIC_OBJECT						1

#if _ENABLE_CHEATS
#define DOCUMENT_MSG_FORMATS		0
#else
#define DOCUMENT_MSG_FORMATS		0
#endif

#if DOCUMENT_MSG_FORMATS
#define DOC_MSG(text) text
#else
#define DOC_MSG(text) ""
#endif

DECL_ENUM(NetworkMessageErrorType)

struct MissionHeader;
struct PlayerRole;
struct PlayerIdentity;

DECL_ENUM(VehicleListType)
class VehicleSupply;
class Transport;
class Detector;
class EntityAI;
typedef EntityAI TargetType;
class Mine;
class Fireplace;

class RadioSentence;

class Command;

struct VehicleInitCmd;

#define CHECK_CAST(dst, src, dsttype) \
	dsttype *dst = static_cast<dsttype *>(src); \
	Assert(dynamic_cast<dsttype *>(src) != NULL);

#define CHECK_ASSIGN(dst, src, dsttype) \
	dsttype &dst = static_cast<dsttype &>(src);

//! Game name registered at GameSpy
char *GetGameName(bool datadisc);

//! Return secret key (password) for GameSpy
char *GetSecretKey(bool datadisc);

///////////////////////////////////////////////////////////////////////////////
// Interface for network manager object

//! Basic info about multiplayer player
struct NetPlayerInfo
{
	//! Unique DirectPlay ID of player
	int dpid;
	//! name of player
	RString name;
};
TypeIsMovableZeroed(NetPlayerInfo)

//! States of multiplayer game
enum NetworkGameState
{
	//! no game or exiting game
	NGSNone,
	//! game is creating (message formats are not registered yet)
	NGSCreating,
	//! game is created, messages can be sent
	NGSCreate,
	//! player is logged into game (his identity is created)
	NGSLogin,
	//! server is editting mission
	NGSEdit,
	//! player voted mission
	NGSMissionVoted,
	//! players are assigned to sides
	NGSPrepareSide,
	//! players are assigned to roles
	NGSPrepareRole,
	//! player is already assigned
	NGSPrepareOK,
	//! mission debriefing
	NGSDebriefing,
	//! mission debriefing is already read
	NGSDebriefingOK,
	//! transfering mission file
	NGSTransferMission,
	//! loading island
	NGSLoadIsland,
	//! briefing screen
	NGSBriefing,
	//! game is already running
	NGSPlay,
};

//! Positions in vehicle to get in
enum GetInPosition
{
	//! anywhere
	GIPAny,
	//! to commander position
	GIPCommander,
	//! to driver / pilot position
	GIPDriver,
	//! to gunner position
	GIPGunner,
	//! back
	GIPCargo
};

//! Respawn mode
enum RespawnMode
{
	RespawnNone,
	RespawnSeaGull,
	RespawnAtPlace,
	RespawnInBase,
	RespawnToGroup, // respawn to group member, leader to leader, other to other
	RespawnToFriendly, // respawn to any playable friendly unit
};

//! State of sound
enum SoundStateType
{
	SSRestart,
	SSStop,
	SSRepeat,
	SSLastLoop,
};

//! Quality of connection
enum ConnectionQuality
{
	CQGood,
	CQPoor,
	CQBad
};

class Magazine;

//enum UIActionType;

enum KickOffReason
{
	KORKick,
	KORBan,
	KORFade,
	KORAddon,
	KOROther
};

//! Interface to basic network functions
class INetworkManager
{
public:
	//! Initialize multiplayer, listen on given ip address and port
	virtual bool Init(RString ip, int port, bool startEnum = true) = NULL;
	//! Finish multiplayer
	virtual void Done() = NULL;
//	virtual void GetSessions(AutoArray<SessionInfo, MemAllocSA> &sessions) = NULL;
	//! Retrieve list of sessions
	virtual void GetSessions(AutoArray<SessionInfo> &sessions) = NULL;
	//! Transfer IP address and port into DirectPlay URL address
	virtual RString IPToGUID(RString ip, int port) = NULL;
	//! Create multiplayer game (server + client)
	/*!
	\param port UDP port for server
	\param password password of session
	\return true if game created
	*/
	virtual bool CreateSession(int port, RString password) = NULL;
	//! Join to multiplayer game (client only)
	/*!
	\param guid unique identification of session
	\param password password of session
	\return result of connecting client to server
	*/
	virtual ConnectResult JoinSession(RString guid, RString password) = NULL;
	//! Wait until session is created on given ip and port
	virtual bool WaitForSession() = NULL;
	//! Retrieve list of players
	virtual void GetPlayers(AutoArray<NetPlayerInfo, MemAllocSA> &players) = NULL;
	//! Creates pbo file for selected mission (on server)
	virtual void CreateMission(RString mission, RString world) = NULL;
	//! Initialize mission
	/*!
	- create and send mission header structure (including difficulty settings)
	- create and send slots for sides and roles
	*/
	virtual void InitMission(bool cadetMode) = NULL;
	//! Close multiplayer session, listen to sessions again
	virtual void Close() = NULL;
	//! Network simulation - called onced during every frame
	virtual void OnSimulate() = NULL;
	//! Return if server is created
	virtual bool IsServer() const = NULL;
	//! Kick off given player from game
	virtual void KickOff(int dpnid, KickOffReason reason) = NULL;
	//! Ban given player (kick off + add to dynamic ban list)
	virtual void Ban(int dpnid) = NULL;
	//! disable/enable connecting of further clients 
	virtual void LockSession(bool lock = true) = NULL;

	//! perform regular memory clean-up
	virtual unsigned CleanUpMemory() {return 0;}

	//! Return current state of game
	virtual NetworkGameState GetGameState() const = NULL;
	//! Return current state on server
	virtual NetworkGameState GetServerState() const = NULL;
	//! Set current state of game (on server + send to clients)
	virtual void SetGameState(NetworkGameState state) = NULL;
	//! Check if client is logged into dedicated server as game master
	virtual bool IsGameMaster() const = NULL;
	//! Return if gamemaster is only voted admin (cannot do shutdown etc.)
	virtual bool IsAdmin() const = NULL;
	//! Return connection quality for client
	virtual ConnectionQuality GetConnectionQuality() const = NULL;
	//! Retrieves parameters for current mission
	virtual void GetParams(float &param1, float &param2) const = NULL;
	//! Set parameters for current mission
	virtual void SetParams(float param1, float param2) = NULL;
	//! Return mission header
	virtual const MissionHeader *GetMissionHeader() const = NULL;
	//! Return id of player on client
	virtual int GetPlayer() const = NULL;
	//! Return number of role slots for current mission
	virtual int NPlayerRoles() const = NULL;
	//! Return given role slot 
	virtual const PlayerRole *GetPlayerRole(int role) const = NULL;
	//! Return role slot for local client
	virtual const PlayerRole *GetMyPlayerRole() const = NULL;
	//! Search for identity with given player id
	virtual const PlayerIdentity *FindIdentity(int dpnid) const = NULL;
	//! Return array of identities
	virtual const AutoArray<PlayerIdentity> *GetIdentities() const = NULL;

	//! Assign player to role slot
	virtual void AssignPlayer(int role, int player) = NULL;
	//! Unassign role slot
	virtual void UnassignPlayer(int role) = NULL;
	//! Select person as player
	/*!
	\param player DirectX player id
	\param person player's person
	\param respawn play "ressurect" cutscene
	*/
	virtual void SelectPlayer(int player, Person *person, bool respawn = false) = NULL;
	//! Play sound on all clients (except itself)
	/*!
	\param name name of sound file
	\param position source position
	\param speed source speed
	\param volume sound volume
	\param freq sound frequency
	\param wave local sound object (for mapping local sounds to sent sounds)
	*/
	virtual void PlaySound
	(
		RString name, Vector3Par position, Vector3Par speed, 
		float volume, float freq, AbstractWave *wave
	) = NULL;
	//! Change state of played sound on all clients (except itself)
	/*!
	\param wave local sound object
	\param state state to set
	*/
	virtual void SoundState(AbstractWave *wave, SoundStateType state) = NULL;
	//! Return game state for given player
	virtual NetworkGameState GetPlayerState(int dpid) = NULL;
	//! Return name for given player
	virtual RString GetPlayerName(int dpid) = NULL;
	//! Return camera position for given player
	virtual Vector3 GetCameraPosition(int dpid) = NULL;
	//! Register vehicle on client and send message to server (and other clients)
	/*!
	\param veh registered vehicle
	\param type id of vehicle list to add into
	\param name name of variable vehicle is stored in
	\param idVeh id of vehicle (assigned in mission editor)
	*/
	virtual bool CreateVehicle
	(
		Vehicle *veh, VehicleListType type, RString name, int idVeh
	) = NULL;
	//! Register whole AI structure for given AICenter (center, groups, subgroups, units)
	virtual bool CreateCenter(AICenter *center) = NULL;	// whole AI structure
	//! Register generic network object and send message to server (and other clients)
	virtual bool CreateObject(NetworkObject *object) = NULL;
	//! Send first update of all created objects over network (as guaranteed message)
	virtual void CreateAllObjects() = NULL;
	//! Unregister given network object, send message to others
	virtual void DeleteObject(NetworkId &id) = NULL;
	//! Unregister all objects, both local and remote
	virtual void DestroyAllObjects() = NULL;
	//! Register command as network object, send message to others
	virtual bool CreateCommand(AISubgroup *subgrp, int index, Command *cmd) = NULL;
	//! Unregister command, send message to others
	virtual void DeleteCommand(AISubgroup *subgrp, int index, Command *cmd) = NULL;
	//! Send message to server that player changed his state
	virtual void ClientReady(NetworkGameState state) = NULL;
	//! Ask object owner for damage of object
	/*!
	\param who damaged object
	\param owner who is responsible for damage
	\param modelPos position of damage
	\param val amount of damage
	\param valRange range of damage
	\param ammo ammunition type
	*/
	virtual void AskForDammage
	(
		Object *who, EntityAI *owner,
		Vector3Par modelPos, float val, float valRange, RString ammo
	) = NULL;
	//! Ask object owner for set of total damage of object
	/*!
	\param who damaged object
	\param damage new value of total damage
	*/
	virtual void AskForSetDammage
	(
		Object *who, float dammage
	) = NULL;
	//! Ask vehicle owner for get in person
	/*!
	\param soldier who is getting in
	\param vehicle vehicle to get in
	\param position position in vehicle to get in
	*/
	virtual void AskForGetIn
	(
		Person *soldier, Transport *vehicle,
		GetInPosition position
	) = NULL;
	//! Ask vehicle owner for get out person
	/*!
	\param soldier who is getting out
	\param vehicle vehicle to get out
	\param getOutTo vehicle to get in
	*/
	virtual void AskForGetOut
	(
		Person *soldier, Transport *vehicle, bool parachute
	) = NULL;
	//! Ask vehicle owner for change person position
	/*!
	\param soldier who is changing position
	\param vehicle vehicle where position is changed
	\param type performed action
	*/
	virtual void AskForChangePosition
	(
		Person *soldier, Transport *vehicle, UIActionType type
	) = NULL;
	//! Ask vehicle owner for aim weapon
	/*!
	\param vehicle vehicle which weapon is aiming
	\param weapon aiming weapon index
	\param dir direction to aim
	*/
	virtual void AskForAimWeapon
	(
		EntityAI *vehicle, int weapon, Vector3Par dir
	) = NULL;
	//! Ask vehicle owner for aim observer turret
	/*!
	\param vehicle vehicle which turret is aiming
	\param dir direction to aim
	*/
	virtual void AskForAimObserver
	(
		EntityAI *vehicle, Vector3Par dir
	) = NULL;
	//! Ask vehicle owner for select weapon
	/*!
	\param vehicle vehicle which weapon is selecting
	\param weapon selected weapon index
	*/
	virtual void AskForSelectWeapon
	(
		EntityAI *vehicle, int weapon
	) = NULL;
	//! Ask vehicle owner for change ammo state
	/*!
	\param vehicle vehicle which ammo is changing
	\param weapon weapon index
	\param burst amount of ammo to decrease
	*/
	virtual void AskForAmmo
	(
		EntityAI *vehicle, int weapon, int burst
	) = NULL;
	//! Ask vehicle owner for add impulse
	/*!
	\param vehicle vehicle impulse is applied to
	\param force applied force
	\param torque applied torque
	*/
	virtual void AskForAddImpulse
	(
		Vehicle *vehicle, Vector3Par force, Vector3Par torque
	) = NULL;
	//! Ask object owner for move object
	/*!
	\param vehicle moving object
	\param pos new position
	*/
	virtual void AskForMove
	(
		Object *vehicle, Vector3Par pos
	) = NULL;
	//! Ask object owner for move object
	/*!
	\param vehicle moving object
	\param trans new transformation matrix
	*/
	virtual void AskForMove
	(
		Object *vehicle, Matrix4Par trans
	) = NULL;
	//! Ask group owner for join groups
	/*!
	\param join joined group
	\param group joining group
	*/
	virtual void AskForJoin
	(
		AIGroup *join, AIGroup *group
	) = NULL;
	//! Ask group owner for join groups
	/*!
	\param join joined group
	\param units joining units
	*/
	virtual void AskForJoin
	(
		AIGroup *join, OLinkArray<AIUnit> &units
	) = NULL;
	//! Ask person owner for hide body
	/*!
	\param vehicle body to hide
	*/
	virtual void AskForHideBody(Person *vehicle) = NULL;
	//! Transfer explosion effects (explosion, smoke, etc.) to other clients
	/*!
	\param owner shot owner (who is responsible for explosion)
	\param shot shot
	\param directHit hitted object
	\param pos explosion position
	\param dir explosion direction
	\param type ammunition
	\param enemyDammage some enemy was damaged
	*/
	virtual void ExplosionDammageEffects
	(
		EntityAI *owner, Shot *shot,
		Object *directHit, Vector3Par pos, Vector3Par dir, const AmmoType *type,
		bool enemyDammage
	) = NULL;
	//! Transfer fire effects (sound, fire, smoke, recoil effect, etc.) to other clients
	/*!
	\param vehicle firing vehicle
	\param weapon firing weapon index
	\param magazine fired magazine
	\param target aimed target
	*/
	virtual void FireWeapon
	(
		EntityAI *vehicle, int weapon, const Magazine *magazine, EntityAI *target
	) = NULL;
	//! Send update of weapons to vehicle owner
	/*!
	\param vehicle vehicle to update
	*/
	virtual void UpdateWeapons(EntityAI *vehicle) = NULL;
	//! Ask vehicle to add weapon into cargo
	/*!
	\param vehicle asked vehicle
	\param weapon name of weapon type to add
	*/
	virtual void AddWeaponCargo(VehicleSupply *vehicle, RString weapon) = NULL;
	//! Ask vehicle to remove weapon from cargo
	/*!
	\param vehicle asked vehicle
	\param weapon name of weapon type to remove
	*/
	virtual void RemoveWeaponCargo(VehicleSupply *vehicle, RString weapon) = NULL;
	//! Ask vehicle to add magazine into cargo
	/*!
	\param vehicle asked vehicle
	\param magazine magazine to add
	*/
	virtual void AddMagazineCargo(VehicleSupply *vehicle, const Magazine *magazine) = NULL;
	//! Ask vehicle to remove magazine from cargo
	/*!
	\param vehicle asked vehicle
	\param creator id of magazine to remove
	\param id id of magazine to remove
	*/
	virtual void RemoveMagazineCargo(VehicleSupply *vehicle, int creator, int id) = NULL;
	//! Transfer init expression to other clients and execute it
	/*!
	\param init vehicle init expression description
	*/
	virtual void VehicleInit(VehicleInitCmd &init) = NULL;
	//! Transfer message who is responsible to destroy vehicle to other clients
	/*!
	\param killed destroyed vehicle
	\param killer who is responsible for destroying
	*/
	virtual void OnVehicleDestroyed(EntityAI *killed, EntityAI *killer) = NULL;
	//! Transfer message about damage of vehicle to other clients
	/*!
	\param damaged damaged vehicle
	\param killer who is responsible for destroying
	\param damage amount of damage hit
	\param ammo ammunition type
	*/
	virtual void OnVehicleDamaged(EntityAI *damaged, EntityAI *killer, float damage, RString ammo) = NULL;
	//! Transfer message about fired missile to other clients
	/*!
	\param target missile target
	\param ammo ammunition type
	\param owner missile owner
	*/
	virtual void OnIncomingMissile(EntityAI *target, RString ammo, EntityAI *owner) = NULL;
	//! Transfer info about (user made) marker creation to other clients
	/*!
	\param channel chat channel (who will see the marker)
	\param sender sender unit
	\param units receiving units
	\param info marker itself
	*/
	virtual void MarkerCreate(int channel, AIUnit *sender, RefArray<NetworkObject> &units, ArcadeMarkerInfo &info) = NULL;
	//! Transfer info about (user made) marker was deleted to other clients
	/*!
	\param name name of marker
	*/
	virtual void MarkerDelete(RString name) = NULL;
	//! Ask flag (carrier) owner for assign new owner
	/*!
	\param owner new owner
	\param carrier flag carrier
	*/
	virtual void SetFlagOwner
	(
		Person *owner, EntityAI *carrier
	) = NULL;
	//! Ask client owns flag owner for change of flag ownership
	/*!
	\param owner new or old flag owner
	\param carrier flag carrier
	*/
	virtual void SetFlagCarrier
	(
		Person *owner, EntityAI *carrier
	) = NULL;
	//! Send radio message to other clients
	/*!
	Sending whole radio message is now implemented only for vehicle messages.
	\param msg radio message
	*/
	virtual void SendRadioMessage(NetworkSimpleObject *msg) = NULL;
	//! Public variable to other clients
	/*!
	\param name variable name
	*/
	virtual void PublicVariable(RString name) = NULL;
	//! Send chat message
	/*!
	\param channel chat channel
	\param text chat text
	*/
	virtual void Chat(int channel, RString text) = NULL;
	//! Send chat message
	/*!
	\param channel chat channel
	\param sender sender unit
	\param units receiving units
	\param text chat text
	*/
	virtual void Chat(int channel, AIUnit *sender, RefArray<NetworkObject> &units, RString text) = NULL;
	//! Send chat message
	/*!
	\param channel chat channel
	\param sender sender name
	\param units receiving units
	\param text chat text
	*/
	virtual void Chat(int channel, RString sender, RefArray<NetworkObject> &units, RString text) = NULL;
	//! Send radio message as chat (text and sentence)
	/*!
	\param channel chat channel
	\param sender sender name
	\param units receiving units
	\param text chat text
	\param sentence - list of words to be spoken
	*/
	virtual void RadioChat(int channel, AIUnit *sender, RefArray<NetworkObject> &units, RString text, RadioSentence &sentence) = NULL;
	//! Send text radio message as chat (sound and title)
	/*!
	\param channel chat channel
	\param units receiving units
	\param wave name of class from CfgRadio containing sound and title
	\param sender sender unit
	\param senderName sender name
	*/
	virtual void RadioChatWave(int channel, RefArray<NetworkObject> &units, RString wave, AIUnit *sender, RString senderName) = NULL;
	//! Set channel for Voice Over Net
	/*!
	\param channel chat channel
	*/
	virtual void SetVoiceChannel(int channel) = NULL;
	//! Set channel for Voice Over Net
	/*!
	\param channel chat channel
	\param units receiving units
	*/
	virtual void SetVoiceChannel(int channel, RefArray<NetworkObject> &units) = NULL;
	//! Transfer file to other clients
	/*!
	\param dest destination filename
	\param source source filename
	*/
	virtual void TransferFile(RString dest, RString source) = NULL;
	//! Server send mission pbo file to clients
	virtual void SendMissionFile() = NULL;
	//! Retrieve state of file transfer
	/*!
	\param curBytes amount of transfered bytes
	\param totBytes total amount bytes to transfer
	*/
	virtual void GetTransferStats(int &curBytes, int &totBytes) = NULL;
	//! Returns respawn mode
	virtual RespawnMode GetRespawnMode() const = NULL;
	//! Returns respawn delay
	virtual float GetRespawnDelay() const = NULL;
	//! Add person to respawn queue (respawn after delay)
	/*!
	\param soldier person to respawn
	\param pos position where respawn
	*/
	virtual void Respawn(Person *soldier, Vector3Par pos) = NULL;
	//! Process chat command for remote control of dedicated server
	/*!
	\param command chat command
	*/
	virtual bool ProcessCommand(RString command) = NULL;
	//! Ask server to kick off player
	/*!
	\param player DirectPlay ID of player to kick off
	*/
	virtual void SendKick(int player) = NULL;
	//! Ask server to disable/enable connection of further clients
	/*!
	\param lock true to disable connection
	*/
	virtual void SendLockSession(bool lock = true) = NULL;

	//! Checks if gamemaster is to select mission on dedicated server
	virtual bool CanSelectMission() const = NULL;
	//! Checks if client is to vote mission on dedicated server
	virtual bool CanVoteMission() const = NULL;
	//! Returns array of missions available on dedicated server
	virtual const AutoArray<RString> &GetServerMissions() const = NULL;
	//! Select mission on dedicated server
	/*!
	\param mission name of selected mission
	\param cadetMode cadet / veteran mode
	*/
	virtual void SelectMission(RString mission, bool cadetMode) = NULL;
	//! Vote mission on dedicated server
	/*!
	\param mission name of selected mission
	\param cadetMode cadet / veteran mode
	*/
	virtual void VoteMission(RString mission, bool cadetMode) = NULL;
	//! Force update of network object
	/*!
	\param object object to update
	*/
	virtual void UpdateObject(NetworkObject *object) = NULL;
	//! Ask player to show target
	/*!
	\param vehicle player person
	\param target target to show
	*/
	virtual void ShowTarget(Person *vehicle, TargetType *target) = NULL;
	//! Ask player to show group direction
	/*!
	\param vehicle player person
	\param dir rirection to show
	*/
	virtual void ShowGroupDir(Person *vehicle, Vector3Par dir) = NULL;
	//! Transfer activation of group synchronization
	/*
	\param grp synchronized group
	\param active state of synchronization
	*/
	virtual void GroupSynchronization(AIGroup *grp, int synchronization, bool active) = NULL;
	//! Transfer activation of detector (through radio)
	/*
	\param grp synchronized group
	\param active state of synchronization
	*/
	virtual void DetectorActivation(Detector *det, bool active) = NULL;
	//! Ask group owner to create new unit
	/*!
	\param group group unit will be added to
	\param type name of vehicle type
	\param position position create at
	\param skill initial skill
	\param rank initial rank
	*/
	virtual void AskForCreateUnit(AIGroup *group, RString type, Vector3Par position, RString init, float skill, Rank rank) = NULL;
	//! Ask vehicle owner to destroy vehicle
	/*!
	\param vehicle vehicle to destroy
	*/
	virtual void AskForDeleteVehicle(Entity *veh) = NULL;
	//! Ask subgroup to receive answer from unit
	virtual void AskForReceiveUnitAnswer
	(
		AIUnit *from, AISubgroup *to, int answer
	) = NULL;
	//! Ask group owner to respawn player
	/*!
	\param person killed player
	\param killer killer entity
	*/
	virtual void AskForGroupRespawn(Person *person, EntityAI *killer) = NULL;
	//! Ask mine owner to activate it
	/*!
	\param mine mine
	\param activate activate / desactivate
	*/
	virtual void AskForActivateMine(Mine *mine, bool activate) = NULL;
	//! Ask fireplace to inflame / put down
	/*!
	\param fireplace fireplace
	\param fire inflame / put down
	*/
	virtual void AskForInflameFire(Fireplace *fireplace, bool fire) = NULL;
	//! Ask vehicle for user defined animation
	/*!
	\param vehicle animated vehicle
	\param animation animation name
	\param phase wanted animation phase
	*/
	virtual void AskForAnimationPhase(Entity *vehicle, RString animation, float phase) = NULL;
	//! Copy unit info from one person to other
	/*!
	\param from copy source
	\param to copy destination
	*/
	virtual void CopyUnitInfo(Person *from, Person *to) = NULL;
	//! Return estimated end of mission time
	virtual Time GetEstimatedEndTime() const = NULL;
	//! Set estimated end of mission time
	/*!
	\param time estimated time
	*/
	virtual void SetEstimatedEndTime(Time time) = NULL;
	
	//! Body can be hidden (for better performance)
	virtual void DisposeBody(Person *body) = NULL;

	//! Game is paused due to disconnection state of game
	virtual bool IsControlsPaused() = NULL;

	//! Last received message's age in seconds (used to eliminate "disconnect cheat")
	virtual float GetLastMsgAgeReliable() = NULL;
};

//! Return networn manager class
INetworkManager &GetNetworkManager();

//! Interface to basic network component (client or server)
class INetworkComponent
{
public:
	//! Virtual destructor
	virtual ~INetworkComponent() {}
	//! Return message format of given message type
	/*!
	\param type message type
	*/
	virtual NetworkMessageFormatBase *GetFormat(/*DWORD client, */int type) = NULL;
	//! Return network object with given id
	/*!
	\param id id of object to return
	*/
	virtual NetworkObject *GetObject(NetworkId &id) = NULL;
};

///////////////////////////////////////////////////////////////////////////////
// Messages

#define TMCHECK(command) \
{TMError err = command; if (err != TMOK && err != TMNotFound) return err;}

#define MSG_RECEIVE		false
#define MSG_SEND			true

//! Transfer message errors
DEFINE_ENUM_BEG(TMError)			// Transfer message errors
	//! transfer OK
	TMOK,					// OK
	//! transferred item not found in message
	TMNotFound,		// OK - different versions
	//! generic error
	TMGeneric,
	TMBadType,
	//! message format not found
	TMBadFormat,
DEFINE_ENUM_END(TMError)

//! Message types
#define NETWORK_MESSAGE_TYPES(XX) \
	XX(FORMAT_CREATE, NetworkMessageFormatItem, MsgFormatItem, "<notused/> <embedded/> Define single item of message format.", Generic) \
	XX(FORMAT_CREATE, NetworkMessageFormatBase, MsgFormat, "<notused/> Used internally by OFP for dynamic definition of message formats.", Generic) \
	XX(FORMAT_CREATE, PlayerMessage, Player, "This message is send by server to confirm that client was connected successfully.", Control) \
	XX(FORMAT_CREATE, NetworkMessageQueue, Messages, "<notused/> Used internally by OFP for aggregation of messages.", Generic) \
	XX(FORMAT_CREATE, ChangeGameState, GameState, "Message is send by server if its state is changed or by (player) client, when some operation is completed.", Control) \
	XX(FORMAT_CREATE, PlayerIdentity, Login, "Detail description of player.", Control) \
	XX(FORMAT_CREATE, LogoutMessage, Logout, "Server broadcast this message if some player disconnect.", Control) \
	XX(FORMAT_CREATE, SquadIdentity, Squad, "Detail description of player's squad.", Control) \
	XX(FORMAT_CREATE, PublicVariableMessage, PublicVariable, "Broadcast game variable to other clients.", Broadcast) \
	XX(FORMAT_CREATE, ChatMessage, Chat, "Chat message.", Chat) \
	XX(FORMAT_CREATE, RadioChatMessage, RadioChat, "Radio message (game radio protocol).", Chat) \
	XX(FORMAT_CREATE, RadioChatWaveMessage, RadioChatWave, "Radio message (message defined by mission designer).", Chat) \
	XX(FORMAT_CREATE, SetVoiceChannelMessage, SetVoiceChannel, "Set Voice Over Net channel (targets).", Chat) \
	XX(FORMAT_CREATE, SetSpeakerMessage, SetSpeaker, "Set which unit is speaking using Voice Over Net.", Chat) \
	XX(FORMAT_CREATE, MissionHeader, MissionHeader, "Description of selected mission.", Control) \
	XX(FORMAT_CREATE, PlayerRole, PlayerSide, "<notused/> Obsolete.", Control) \
	XX(FORMAT_CREATE, PlayerRole, PlayerRole, "Define attachment between players and his role in game.", Control) \
	XX(FORMAT_CREATE, SelectPlayerMessage, SelectPlayer, "Assign player to unit.", Control) \
	XX(FORMAT_CREATE, AttachPersonMessage, AttachPerson, "Attach body (instance of class Person) and brain (instance of class AIUnit).", Control) \
	XX(FORMAT_CREATE, TransferFileMessage, TransferFile, "Used to transfer generic file over network.", Generic) \
	XX(FORMAT_CREATE, AskMissionFileMessage, AskMissionFile, "Used by client to tell server if its mission file is actual.", Control) \
	XX(FORMAT_CREATE, TransferMissionFileMessage, TransferMissionFile, "Used to transfer mission file over network.", Generic) \
	XX(FORMAT_CREATE, TransferFileToServerMessage, TransferFileToServer, "Used to transfer generic file over network.", Generic) \
	XX(FORMAT_CREATE, AskForDammageMessage, AskForDammage, "Message for ask object owner for damage of object.", Ask) \
	XX(FORMAT_CREATE, AskForSetDammageMessage, AskForSetDammage, "Message for ask object owner for set of total damage of object.", Ask) \
	XX(FORMAT_CREATE, AskForGetInMessage, AskForGetIn, "Message for ask vehicle owner for get in person.", Ask) \
	XX(FORMAT_CREATE, AskForGetOutMessage, AskForGetOut, "Message for ask vehicle owner for get out person.", Ask) \
	XX(FORMAT_CREATE, AskForChangePositionMessage, AskForChangePosition, "Message for ask vehicle owner for change person position.", Ask) \
	XX(FORMAT_CREATE, AskForAimWeaponMessage, AskForAimWeapon, "Message for ask vehicle owner for aim weapon.", Ask) \
	XX(FORMAT_CREATE, AskForAimObserverMessage, AskForAimObserver, "Message for ask vehicle owner for aim observer turret.", Ask) \
	XX(FORMAT_CREATE, AskForSelectWeaponMessage, AskForSelectWeapon, "Message for ask vehicle owner for select weapon.", Ask) \
	XX(FORMAT_CREATE, AskForAmmoMessage, AskForAmmo, "Message for ask vehicle owner for change ammo state.", Ask) \
	XX(FORMAT_CREATE, AskForAddImpulseMessage, AskForAddImpulse, "Message for ask vehicle owner for add impulse.", Ask) \
	XX(FORMAT_CREATE, AskForMoveVectorMessage, AskForMoveVector, "Message for ask object owner for move object.", Ask) \
	XX(FORMAT_CREATE, AskForMoveMatrixMessage, AskForMoveMatrix, "Message for ask object owner for move object.", Ask) \
	XX(FORMAT_CREATE, AskForJoinGroupMessage, AskForJoinGroup, "Message for ask group owner for join other group.", Ask) \
	XX(FORMAT_CREATE, AskForJoinUnitsMessage, AskForJoinUnits, "Message for ask group owner for join other units.", Ask) \
	XX(FORMAT_CREATE, ExplosionDammageEffectsMessage, ExplosionDammageEffects, "Message for transfer explosion effects (explosion, smoke, etc.) to other clients.", Broadcast) \
	XX(FORMAT_CREATE, FireWeaponMessage, FireWeapon, "Message for transfer fire effects (sound, fire, smoke, recoil effect, etc.) to other clients.", Broadcast) \
	XX(FORMAT_CREATE, UpdateWeaponsMessage, UpdateWeapons, "Message is sent to update of weapons to vehicle owner.", Ask) \
	XX(FORMAT_CREATE, AddWeaponCargoMessage, AddWeaponCargo, "Message for ask vehicle to add weapon into cargo.", Ask) \
	XX(FORMAT_CREATE, RemoveWeaponCargoMessage, RemoveWeaponCargo, "Message for ask vehicle to remove weapon from cargo.", Ask) \
	XX(FORMAT_CREATE, AddMagazineCargoMessage, AddMagazineCargo, "Message for ask vehicle to add magazine into cargo.", Ask) \
	XX(FORMAT_CREATE, RemoveMagazineCargoMessage, RemoveMagazineCargo, "Message for ask vehicle to remove magazine from cargo.", Ask) \
	XX(FORMAT_CREATE, VehicleInitCmd, VehicleInit, "Broadcast initialization expression for vehicle.", Broadcast) \
	XX(FORMAT_CREATE, VehicleDestroyedMessage, VehicleDestroyed, "Message for transfer message who is responsible to destroy vehicle to other clients.", Statistics) \
	XX(FORMAT_CREATE, MarkerCreateMessage, MarkerCreate, "Message for transfer info about (user made) marker creation to other clients.", Chat) \
	XX(FORMAT_CREATE, MarkerDeleteMessage, MarkerDelete, "Message for transfer info about (user made) marker was deleted to other clients", Chat) \
	XX(FORMAT_CREATE, SetFlagOwnerMessage, SetFlagOwner, "Message for ask flag (carrier) owner for assign new owner.", Ask) \
	XX(FORMAT_CREATE, SetFlagCarrierMessage, SetFlagCarrier, "Message for ask client owns flag owner for change of flag ownership.", Ask) \
	XX(FORMAT_CREATE, RadioMessageVTarget, MsgVTarget, "Set target.", Command) \
	XX(FORMAT_CREATE, RadioMessageVFire, MsgVFire, "Fire on target.", Command) \
	XX(FORMAT_CREATE, RadioMessageVMove, MsgVMove, "Move to destination.", Command) \
	XX(FORMAT_CREATE, RadioMessageVFormation, MsgVFormation, "Return to formation.", Command) \
	XX(FORMAT_CREATE, RadioMessageVSimpleCommand, MsgVSimpleCommand, "Simple command.", Command) \
	XX(FORMAT_CREATE, RadioMessageVLoad, MsgVLoad, "Switch to other weapon.", Command) \
	XX(FORMAT_CREATE, RadioMessageVAzimut, MsgVAzimut, "Move in direction.", Command) \
	XX(FORMAT_CREATE, RadioMessageVStopTurning, MsgVStopTurning, "Stop turning and continue with movement.", Command) \
	XX(FORMAT_CREATE, RadioMessageVFireFailed, MsgVFireFailed, "Gunner is unable to fire on given target.", Command) \
	XX(FORMAT_CREATE, ChangeOwnerMessage, ChangeOwner, "Message is sent when owner of some object changes.", Control) \
	XX(FORMAT_CREATE, PlaySoundMessage, PlaySound, "Message sent to clients to play sound.", Broadcast) \
	XX(FORMAT_CREATE, SoundStateMessage, SoundState, "Message sent to clients to change state of played sound.", Broadcast) \
	XX(FORMAT_CREATE, DeleteObjectMessage, DeleteObject, "Message announcing destroying of network object.", Destroy) \
	XX(FORMAT_CREATE, DeleteCommandMessage, DeleteCommand, "Message announcing destroying of command.", Destroy) \
	XX(FORMAT_CREATE, Object, CreateObject, "Create Object.", Create) \
	XX(FORMAT_UPDATE, Object, UpdateObject, "Generic update of Object.", Update) \
	XX(FORMAT_CREATE, Vehicle, CreateVehicle, "Create Entity (: Object).", Create) \
	XX(FORMAT_UPDATE, Vehicle, UpdateVehicle, "Generic update of Entity (: Object).", Update) \
	XX(FORMAT_UPDATE_POSITION, Vehicle, UpdatePositionVehicle, "Update position of Entity (: Object)", UpdPos) \
	XX(FORMAT_CREATE, Detector, CreateDetector, "Create Trigger (: Entity).", Create) \
	XX(FORMAT_UPDATE, Detector, UpdateDetector, "Generic update of Trigger (: Entity)", Update) \
	XX(FORMAT_UPDATE, FlagCarrier, UpdateFlag, "Generic update of Flag (: EntityWithSupply)", Update) \
	XX(FORMAT_CREATE, Shot, CreateShot, "Create Shot (: Entity).", Create) \
	XX(FORMAT_UPDATE, Shot, UpdateShot, "Generic update of Shot (: Entity)", Update) \
	XX(FORMAT_CREATE, Explosion, CreateExplosion, "Create Explosion (: Entity).", Create) \
	XX(FORMAT_CREATE, Crater, CreateCrater, "Create Crater (: Entity).", Create) \
	XX(FORMAT_CREATE, CraterOnVehicle, CreateCraterOnVehicle, "Create CraterOnVehicle (: Crater).", Create) \
	XX(FORMAT_CREATE, ObjectDestructed, CreateObjectDestructed, "Create ObjectDestruction (: Entity).", Create) \
	XX(FORMAT_CREATE, AICenter, CreateAICenter, "Create AICenter.", Create) \
	XX(FORMAT_UPDATE, AICenter, UpdateAICenter, "Generic update of AICenter.", Update) \
	XX(FORMAT_CREATE, AIGroup, CreateAIGroup, "Create AIGroup.", Create) \
	XX(FORMAT_UPDATE, AIGroup, UpdateAIGroup, "Generic update of AIGroup.", Update) \
	XX(FORMAT_CREATE, ArcadeWaypointInfo, Waypoint, "<embedded/> Waypoint.", Create) \
	XX(FORMAT_CREATE, AISubgroup, CreateAISubgroup, "Create AISubgroup (formation).", Create) \
	XX(FORMAT_UPDATE, AISubgroup, UpdateAISubgroup, "Generic update of AISubgroup (formation).", Update) \
	XX(FORMAT_CREATE, AIUnit, CreateAIUnit, "Create AIUnit (brain).", Create) \
	XX(FORMAT_UPDATE, AIUnit, UpdateAIUnit, "Generic update of AIUnit (brain).", Update) \
	XX(FORMAT_CREATE, Command, CreateCommand, "Create Command.", Create) \
	XX(FORMAT_UPDATE, Command, UpdateCommand, "Generic update of Command.", Update) \
	XX(FORMAT_UPDATE, EntityAI, UpdateVehicleAI, "Generic update of EntityWithAI (: Entity).", Update) \
	XX(FORMAT_UPDATE, Person, UpdateVehicleBrain, "Generic update of Person (: EntityWithSupply).", Update) \
	XX(FORMAT_UPDATE, VehicleSupply, UpdateVehicleSupply, "Generic update of EntityWithSupply (: EntityWithAI).", Update) \
	XX(FORMAT_UPDATE, Transport, UpdateTransport, "Generic update of Vehicle (: EntityWithSupply).", Update) \
	XX(FORMAT_UPDATE, Man, UpdateMan, "Generic update of Man (: Person).", Update) \
	XX(FORMAT_UPDATE_POSITION, Man, UpdatePositionMan, "Update position of Man (: Person).", UpdPos) \
	XX(FORMAT_UPDATE, TankOrCar, UpdateTankOrCar, "Generic update of TankOrCar (: Vehicle).", Update) \
	XX(FORMAT_UPDATE, TankWithAI, UpdateTank, "Generic update of Tank (: TankOrCar).", Update) \
	XX(FORMAT_UPDATE_POSITION, TankWithAI, UpdatePositionTank, "Update position of Tank (: TankOrCar).", UpdPos) \
	XX(FORMAT_CREATE, Turret, UpdateTurret, "<embedded/> Update of Turret.", Update) \
	XX(FORMAT_UPDATE, Car, UpdateCar, "Generic update of Car (: TankOrCar).", Update) \
	XX(FORMAT_UPDATE_POSITION, Car, UpdatePositionCar, "Update position of Car (: TankOrCar).", UpdPos) \
	XX(FORMAT_UPDATE, AirplaneAuto, UpdateAirplane, "Generic update of Airplane (: Vehicle).", Update) \
	XX(FORMAT_UPDATE_POSITION, AirplaneAuto, UpdatePositionAirplane, "Update position of Airplane (: Vehicle).", UpdPos) \
	XX(FORMAT_UPDATE, HelicopterAuto, UpdateHelicopter, "Generic update of Helicopter (: Vehicle).", Update) \
	XX(FORMAT_UPDATE_POSITION, HelicopterAuto, UpdatePositionHelicopter, "Update position of Helicopter (: Vehicle).", UpdPos) \
	XX(FORMAT_UPDATE, ParachuteAuto, UpdateParachute, "Generic update of Parachute (: Vehicle).", Update) \
	XX(FORMAT_UPDATE, ShipWithAI, UpdateShip, "Generic update of Ship (: Vehicle).", Update) \
	XX(FORMAT_UPDATE_POSITION, ShipWithAI, UpdatePositionShip, "Update position of Ship (: Vehicle).", UpdPos) \
	XX(FORMAT_CREATE, Magazine, Magazine, "<embedded/> Magazine.", Create) \
	XX(FORMAT_CREATE, OperInfoResult, PathPoint, "<embedded/> PathPoint.", Create) \
	XX(FORMAT_UPDATE, Motorcycle, UpdateMotorcycle, "Generic update of Motorcycle (: TankOrCar).", Update) \
	XX(FORMAT_UPDATE_POSITION, Motorcycle, UpdatePositionMotorcycle, "Update position of Motorcycle (: TankOrCar).", UpdPos) \
	XX(FORMAT_CREATE, AskForHideBodyMessage, AskForHideBody, "Message for ask person owner for hide body.", Ask) \
	XX(FORMAT_CREATE, NetworkCommandMessage, NetworkCommand, "Message for transfer of Network Command", Control) \
	XX(FORMAT_CREATE, IntegrityQuestionMessage, IntegrityQuestion, "This message is sent by server to clients to check consistency of their data (to avoid cheaters).", Control) \
	XX(FORMAT_CREATE, IntegrityAnswerMessage, IntegrityAnswer, "Answer to integrity question message.", Control) \
	XX(FORMAT_CREATE, PlayerStateMessage, PlayerState, "Message used for distribution of players' states to clients.", Control) \
	XX(FORMAT_UPDATE, SeaGullAuto, UpdateSeagull, "Generic update of SeaGull (: Entity).", Update) \
	XX(FORMAT_UPDATE_POSITION, SeaGullAuto, UpdatePositionSeagull, "Update position of SeaGull (: Entity).", UpdPos) \
	XX(FORMAT_UPDATE_POSITION, PlayerIdentity, PlayerUpdate, "Update network statistics of clients.", Statistics) \
	XX(FORMAT_UPDATE_DAMMAGE, EntityAI, UpdateDammageVehicleAI, "Update damage state of EntityWithAI (: Entity).", UpdDmg) \
	XX(FORMAT_UPDATE_DAMMAGE, Object, UpdateDammageObject, "Update damage state of Object.", UpdDmg) \
	XX(FORMAT_CREATE, HelicopterAuto, CreateHelicopter, "Create Helicopter (: Vehicle).", Create) \
	XX(FORMAT_UPDATE, ClientInfoObject, UpdateClientInfo, "Update position of players.", Control) \
	XX(FORMAT_CREATE, ShowTargetMessage, ShowTarget, "Message for ask person owner for show target.", Ask) \
	XX(FORMAT_CREATE, ShowGroupDirMessage, ShowGroupDir, "Message for ask person owner for show group direction.", Ask) \
	XX(FORMAT_SIMPLE, Dummy, GroupSynchronization, "Transfer activation of group synchronization.", Broadcast) \
	XX(FORMAT_SIMPLE, Dummy, DetectorActivation, "Transfer activation of detector (through radio).", Broadcast) \
	XX(FORMAT_SIMPLE, Dummy, AskForCreateUnit, "Ask group owner to create new unit.", Ask) \
	XX(FORMAT_SIMPLE, Dummy, AskForDeleteVehicle, "Ask vehicle owner to destroy vehicle.", Ask) \
	XX(FORMAT_SIMPLE, Dummy, AskForReceiveUnitAnswer, "Ask subgroup to receive answer from unit.", Ask) \
	XX(FORMAT_SIMPLE, Dummy, AskForGroupRespawn, "Ask group owner to respawn player.", Ask) \
	XX(FORMAT_SIMPLE, Dummy, CopyUnitInfo, "Copy unit info from one person to other.", Broadcast) \
	XX(FORMAT_SIMPLE, Dummy, GroupRespawnDone, "Answer if respawn in group succeed.", Ask) \
	XX(FORMAT_SIMPLE, Dummy, MissionParams, "Broadcast parameters of mission.", Control) \
	XX(FORMAT_UPDATE, Mine, UpdateMine, "Generic update of Mine (: Shot).", Update) \
	XX(FORMAT_SIMPLE, Dummy, AskForActivateMine, "Ask mine owner to activate it.", Ask) \
	XX(FORMAT_SIMPLE, Dummy, VehicleDamaged, "Transfer message about damage of vehicle to other clients.", Statistics) \
	XX(FORMAT_UPDATE, Fireplace, UpdateFireplace, "Generic update of Fireplace (: EntityWithAI).", Update) \
	XX(FORMAT_SIMPLE, Dummy, AskForInflameFire, "Ask fireplace to inflame / put down.", Ask) \
	XX(FORMAT_SIMPLE, Dummy, AskForAnimationPhase, "Ask vehicle for user defined animation.", Ask) \
	XX(FORMAT_SIMPLE, Dummy, IncomingMissile, "Transfer message about fired missile to other clients.", Broadcast)

#define NMT_DEFINE_ENUM(macro, class, name, description, group) NMT##name,

DEFINE_ENUM_BEG(NetworkMessageType)
	NMTNone = -1,	// used as return value of GetNMType - no message
	NETWORK_MESSAGE_TYPES(NMT_DEFINE_ENUM)
	NMTN,
	NMTFirstVariant = NMTGameState
DEFINE_ENUM_END(NetworkMessageType)

//! get identifier or corresponding network data types
template <class Type>
struct GetNDType
{
	//enum {value=-1};
};

//! register particular datatype
#define REGISTER_NDT(type,name) template <> struct GetNDType< type > {enum {value=name};};

template <int id>
struct NoType {};

#define NETWORK_DATA_TYPES(XX) \
	XX(bool,Bool,"Boolean value. Transferred as single byte.") \
	XX(int,Integer,"4 bytes long integer value.") \
	XX(float,Float,"4 bytes long real value.") \
	XX(RString,String,"Null terminated string of bytes, including the trailing zero.") \
	XX(AutoArray<char>,RawData,"Array of bytes. Integer for size, then size bytes of content.") \
	XX(Time,Time,"4 bytes long integer value (time in ms).") \
	XX(Vector3,Vector,"Vector of 3 floats.") \
	XX(Matrix3,Matrix,"Matrix of 3 x 3 floats") \
	XX(AutoArray<int>,IntArray,"Array of integers. Integer for size, then size integers of content.") \
	XX(AutoArray<float>,FloatArray,"Array of floats. Integer for size, then size floats of content.") \
	XX(AutoArray<RString>,StringArray,"Array of strings. Integer for size, then size strings of content.") \
	XX(RadioSentence,Sentence,"Radio message. Integer for number of words, then pairs: string id of word, float pause after word.") \
	XX(NetworkMessage,Object,"Nested message (encoded as whole new message)") \
	XX(AutoArray<NetworkMessage>,ObjectArray,"Array of nested messages. Integer for size, then size of messages.") \
	XX(NetworkId,Ref,"Reference to object. Pair of integer values: id of client where object was created, id of object unique on this client.") \
	XX(AutoArray<NetworkId>,RefArray,"Array of references. Integer for size, then size of references.") \
	XX(NoType<2>,Data,"<notused/> Generic data used for dynamic definition of formats.")

#define NDT_DEFINE_ENUM(type,name,description) NDT##name,
#define NDT_REGISTER_TYPE(type,name,description) REGISTER_NDT(type,NDT##name)

//! Types of message items
enum NetworkDataType
{
	NETWORK_DATA_TYPES(NDT_DEFINE_ENUM)
};

NETWORK_DATA_TYPES(NDT_REGISTER_TYPE)

// register various types that have identical handling as some basic type
REGISTER_NDT(AutoArray<RStringI>,NDTStringArray)
REGISTER_NDT(AutoArray<RStringS>,NDTStringArray)
REGISTER_NDT(AutoArray<RStringB>,NDTStringArray)
REGISTER_NDT(AutoArray<RStringIB>,NDTStringArray)
REGISTER_NDT(StaticArrayAuto<float>,NDTFloatArray)


//! Types of items compression
enum NetworkCompressionType
{
	NCTNone,
	NCTSmallUnsigned, //!< special compression for unsigned int
	NCTSmallSigned, //!< special compression for signed int
	NCTDefault=NCTSmallSigned, //!< default compression for each data type
	NCTStringGeneric=NCTDefault, //!< generic string table
	NCTStringMove, //!< string table for moves
	NCTFloat0To1, //!< float 0 to 1 (8b)
	NCTFloat0To2, //!< float 0 to 2 (8b)
	NCTFloatM1ToP1, //!< float -1 to 1 (8b)

	NCTFloatMostly0To1, //!< float, mostly 0 to 1 (8b-40b)
	NCTFloatAngle, //!< float, mostly -pi to pi (8b-40b)
	//! camera position - used to control error calculation (1m..10m precision)
	//! x,z assumed mostly in range 0..10000, y mostly in range 0..1000
	NCTVectorPositionCamera,
	//! generic position - 1 cm precision required
	//! x,z assumed mostly in range 0..10000, y mostly in range 0..1000
	NCTVectorPosition,
	//! matrix orienttaion - matrix is assumed orthogonal
	NCTMatrixOrientation,
};

struct NetworkMessageFormatItem;

//! Interface for class encapsulates value of basic message items types
struct NetworkData : public RefCount
{
	// all fucntionality moved to RefNetworkData
	//! Calculate difference from other item with the same type
	/*!
	\param type error algorithm type
	\param value2 item to compare with
	\param item item format description
	*/
	//virtual float CalculateError(NetworkMessageErrorType type, NetworkData *value2, NetworkMessageFormatItem &item, float dt) = NULL;
	//! Calculate size of serialized and compressed value
	/*!
	\param compression compression type
	*/
	//virtual int CalculateSize(NetworkCompressionType compression) = NULL;
};

#include <Es/Memory/normalNew.hpp>

//! Class encapsulates value of basic message items types
template <class Type>
struct NetworkDataTyped : public NetworkData
{
	//! raw value
	Type value;
	//! constructor
	NetworkDataTyped() {}
	//! constructor
	/*!
	\param val own value
	*/
	NetworkDataTyped(const Type &val) {value = val;}

	//! direct data constructor (implemented only for raw data type messages)
	NetworkDataTyped(void *val, int size);

	/*
	float CalculateError(NetworkMessageErrorType type, NetworkData *value2, NetworkMessageFormatItem &item, float dt);
	int CalculateSize(NetworkCompressionType compression);
	*/

	USE_FAST_ALLOCATOR
};

//! Algorithms for error calculation
DEFINE_ENUM_BEG(NetworkMessageErrorType)
	//! no error
	ET_NONE,
	//! 1 if not equal, otherwise 0
	ET_NOT_EQUAL,
	//! absolute value of difference
	ET_ABS_DIF,
	//! square of difference
	ET_SQUARE_DIF,
	//! number of different items in array
	ET_NOT_EQUAL_COUNT,
	//! number of items in second array, not contained in first array
	ET_NOT_CONTAIN_COUNT,
	//! specialized entity position packed
	ET_UPD_ENTITY_POS,
	//! specialized man position packed
	ET_UPD_MAN_POS,
	//! specialized magazines difference
	ET_MAGAZINES,
	//! difference of derivations (role of time is incorporated)
	ET_DER_DIF,
DEFINE_ENUM_END(NetworkMessageErrorType)

#include <Es/Memory/debugNew.hpp>

//! Class intended to optimization Ref<NetworkData> pointer overhead
/*!
note: No optimizaton implemented yet
Rules: classes derived from RefNetworkData
must have same size as RefNetworkData. No data members and no virtual functions
can be added, because RefNetworkData is used for direct assignement
using operator =.
*/

class RefNetworkData
{
	protected:
	Ref<NetworkData> _data;

	public:

	//! Calculate difference from other item with the same type
	/*!
	\param type error algorithm type
	\param value2 item to compare with
	\param item item format description
	*/
	float CalculateError(NetworkMessageErrorType type, const RefNetworkData &value2, NetworkMessageFormatItem &item, float dt) const;
	//! Calculate size of serialized and compressed value
	/*!
	\param compression compression type
	*/
	int CalculateSize(NetworkCompressionType compression, const NetworkMessageFormatItem &item) const;
};

class RefNetworkDataNull: public RefNetworkData
{
	public:
	__forceinline RefNetworkDataNull(){}
};

#define ND_NULL RefNetworkDataNull()

TypeIsMovable(RefNetworkData)

//! Class intended to optimization NetworkDataTyped pointer overhead
/*! note: No optimizaton implemented yet */

template <class Type>
class RefNetworkDataTyped : public RefNetworkData
{
	public:
	__forceinline RefNetworkDataTyped(const Type &src)
	{
		_data = new NetworkDataTyped<Type>(src);
	}
	__forceinline RefNetworkDataTyped()
	{
		_data = new NetworkDataTyped<Type>();
	}

	//! direct data constructor (implemented only for raw data type messages)
	__forceinline RefNetworkDataTyped(void *val, int size)
	{
		_data = new NetworkDataTyped<Type>(val,size);
	}
	Type &GetVal() const
	{
		NetworkDataTyped<Type> *typedRef = static_cast<NetworkDataTyped<Type> *>(_data.GetRef());
		Assert(dynamic_cast<NetworkDataTyped<Type> *>(_data.GetRef()) != NULL);
		return typedRef->value;
	}

	//! Calculate difference from other item with the same type
	/*!
	\param type error algorithm type
	\param value2 item to compare with
	\param item item format description
	*/
	float CalculateError
	(
		NetworkMessageErrorType type,
		const RefNetworkDataTyped &value2,
		NetworkMessageFormatItem &item,
		float dt
	) const;
	//! Calculate size of serialized and compressed value
	/*!
	\param compression compression type
	*/
	int CalculateSize(NetworkCompressionType compression, const NetworkMessageFormatItem &item) const;

	ClassIsMovableZeroed(RefNetworkDataTyped)
};



#define DEFVALUE(type, val) RefNetworkDataTyped<type>(val)
#define DEFVALUE_MSG(val) RefNetworkDataTyped<int>(val)
#define DEFVALUENULL RefNetworkDataTyped<NetworkId>(NetworkId::Null())
#define DEFVALUEREFARRAY RefNetworkDataTyped< AutoArray<NetworkId> >()
#define DEFVALUEINTARRAY RefNetworkDataTyped< AutoArray<int> >()
#define DEFVALUEFLOATARRAY RefNetworkDataTyped< AutoArray<float> >()
#define DEFVALUESTRINGARRAY RefNetworkDataTyped< AutoArray<RString> >()
#define DEFVALUERAWDATA RefNetworkDataTyped< AutoArray<char> >()


//! Description of format of single message item
struct NetworkMessageFormatItem
{
	//! name of item
	RString name;
	//! data type
	NetworkDataType type;
	//! compression type
	NetworkCompressionType compression;
	//! default value
	RefNetworkData defValue;

	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	//! Message update from / to this object
	/*!
		\param ctx message context
	*/
	TMError TransferMsg(NetworkMessageContext &ctx);
};
TypeIsMovable(NetworkMessageFormatItem)

//! Basic item for mapping item name to index in array of items
struct NameToIndex
{
	//! item name (key)
	RString name;
	//! index in array of items (value)
	int index;

	//! Constructor
	NameToIndex() {index = -1;}
	//! Constructor
	/*!
	\param n item name
	\param i index in array of items
	*/
	NameToIndex(RString n, int i) {name = n; index = i;}
	//! Return key of item
	const char *GetKey() const {return name;}
};
TypeIsMovableZeroed(NameToIndex)

//! Map items names to indices in items array
typedef MapStringToClass< NameToIndex, AutoArray<NameToIndex> > MapNameToIndex;

//! Basic network message format class (dynamic - sent from server to clients)
class NetworkMessageFormatBase : public NetworkSimpleObject
{
protected:
	//! array of items
	AutoArray<NetworkMessageFormatItem> _items;
	//! map item names to item indices
	MapNameToIndex _map;
	//! preloaded indices (to increase transfer speed)
	SRef<NetworkMessageIndices> _indices;

public:
	//! Constructor
	NetworkMessageFormatBase();

	//! Initialize format after received
	/*!
	- initialize map from items
	- initialize preloaded indices
	\param indices class for preloaded indices
	*/
	void Init(NetworkMessageIndices *indices);

	//! Returns number of format items
	int NItems() const {return _items.Size();}
	//! Return format item with given index
	const NetworkMessageFormatItem &GetItem(int i) const {return _items[i];}
	//! Return format item with given index
	NetworkMessageFormatItem &GetItem(int i) {return _items[i];}

	//! Return array of items
	const NetworkMessageIndices *GetIndices() const {return _indices;}

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTMsgFormat;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);

	//! Search for index of item with given name (return -1 if not found)
	int FindIndex(const char *name) const;
	//! Destroy array of items and map
	void Clear();
};
TypeIsMovable(NetworkMessageFormatBase)

//! orthonormal matrix can be encoded in four float numbers
// plus sign information about two float numbers

struct EncodedMatrix3
{
	// single values encoded as -1 to +1 in 16b representation
	// encoded col 1 (direction up)
	short _01c,_11c;
	// encoded col 2 (direction)
	short _02c,_12c;
	char _21sign;
	char _22sign;

	//! encode 3x3 matrix
	void Encode(const Matrix3 &m);
	//! decode 3x3 matrix
	void Decode(Matrix3 &m) const;
	//! decode 3x3 part of 3x4 matrix
	void Decode(Matrix4 &m) const;
	//@{ decode columns of 3x3 matrix
	Vector3 DirectionUp() const;
	Vector3 Direction() const;
	//@}
};

//! Entity update position submessage (behave as single format item)
struct NetworkUpdEntityPos
{
	EncodedMatrix3 orientation;
	Vector3 position;
	Vector3 speed;
	Vector3 angMomentum;
};

//! Man update position submessage (behave as single format item)
struct NetworkUpdManPos
{
	//@{ 8b encoded, assumed in range -PI,+PI
	char gunXRotWantedC;
	char gunYRotWantedC;
	char headXRotWantedC;
	char headYRotWantedC;
};

//! encode angle in range -PI to +PI to 8b represetation
inline char EncodeRot8b(float x)
{
	int c = toInt(x*(127/H_PI));
	saturate(c,-127,+127);
	return c;
}

//! decode 8b angle to range -PI to +PI
inline float DecodeRot8b(char x)
{
	return x*(H_PI/127);
}

//! compare two 8b angles
inline float CompareRot8b(char x1, char x2)
{
	return (x1-x2)*(H_PI/127);
}

//! Error coeficient for nonimportant values
#define ERR_COEF_VALUE_MINOR	0.1
//! Error coeficient for normal values
#define ERR_COEF_VALUE_NORMAL	1
//! Error coeficient for important values
#define ERR_COEF_VALUE_MAJOR	10
//! Error coeficient for mode - like values
#define ERR_COEF_MODE					100
//! Error coeficient for structural values
#define ERR_COEF_STRUCTURE		10000

//! Item error description
struct NetworkMessageErrorInfo
{
	//! algorithm type
	NetworkMessageErrorType type;
	//! multiplier
	float coef;
};
TypeIsMovableZeroed(NetworkMessageErrorInfo)

TypeIsSimple(const char *)

//! Extended network message format class (static - used also for error calculations)
class NetworkMessageFormat : public NetworkMessageFormatBase
{
	typedef NetworkMessageFormatBase base;

protected:
	//! Error descriptions
	AutoArray<NetworkMessageErrorInfo> _errors;

#if DOCUMENT_MSG_FORMATS
public:
	//! Items documentation
	AutoArray<const char *> _descriptions;
#endif

public:
	//! Return error description for given item
	const NetworkMessageErrorInfo &GetErrorInfo(int i) const {return _errors[i];}
	//! Return error description for given item
	NetworkMessageErrorInfo &GetErrorInfo(int i) {return _errors[i];}
	//! Add new item
	/*!
	\param name name of item
	\param type data type
	\param compression compression type
	\param defValue default value
	\param description item documentation
	\param errType error algorithm
	\param errCoef error multiplier
	*/
	int Add
	(
		const char *name,
		NetworkDataType type,
		NetworkCompressionType compression,
		const RefNetworkData &defValue,
		const char *description,
		NetworkMessageErrorType errType = ET_NONE,
		float errCoef = 0
	);
	//! Add new item - NULL passed as default value
	/*!
	This function is created in order to make porting to RefNetworkData easier.
	DefValue NULL was often passed in this argument, but there is no conversion
	from NULL to RefNetworkData.
	\param name name of item
	\param type data type
	\param compression compression type
	\param null default value (must be zero)
	\param description item documentation
	\param errType error algorithm
	\param errCoef error multiplier
	*/
	int Add
	(
		const char *name,
		NetworkDataType type,
		NetworkCompressionType compression,
		int null,
		const char *description,
		NetworkMessageErrorType errType = ET_NONE,
		float errCoef = 0
	);
	NetworkMessageFormat *Init(NetworkMessageIndices *indices);
	void Clear();
};

struct NetworkUpdateInfo;
struct NetworkObjectInfo;
struct NetworkPlayerObjectInfo;

//! Network message (internal representation)
struct NetworkMessage : public RefCount
{
	// network message header
	//! time when message was sent
	Time time;
	//! size of raw (serialized, compressed) message - not transferred, used for statistics
	int size;	

	// network message body
	//! transferred values (message body)
	AutoArray<RefNetworkData> values;

	//! Temporary pointer to update info - used for update object info whenever message is sent
	NetworkUpdateInfo *objectUpdateInfo;

	//! Temporary pointer (OnSendComplete) - used on server only
	NetworkObjectInfo *objectServerInfo;

	//! Temporary pointer (OnSendComplete) - used on server only
	NetworkPlayerObjectInfo *objectPlayerInfo;

	//! Constructor
	NetworkMessage()
	{
		size = 0;
		objectUpdateInfo = NULL;
		objectServerInfo= NULL;
		objectPlayerInfo= NULL;
	}
	//! Destructor
	~NetworkMessage() {}
};
TypeIsMovable(NetworkMessage)

#define TRANSFER_FORMAT \
	if (index < 0) return TMNotFound; \
	NetworkMessageFormatItem &item = _format->GetItem(index); \
	(void)item; // may be never used
// index < 0 is valid - old message format

#if _RELEASE
// memory access optimized version, no array bound checks,
// direct access via AutoArray::Data

#define TRANSFER(type) \
	RefNetworkData *idata = _msg->values.Data(); \
	if (_sending) \
	{ \
		idata[index] = RefNetworkDataTyped< type >(value); \
	} \
	else \
	{ \
		const RefNetworkData &val = idata[index]; \
		CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped< type >) \
		value = valTyped.GetVal(); \
	} \
	return TMOK;
#else
#define TRANSFER(type) \
	if (_sending) \
	{ \
		_msg->values[index] = RefNetworkDataTyped< type >(value); \
	} \
	else \
	{ \
		const RefNetworkData &val = _msg->values[index]; \
		CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped< type >) \
		value = valTyped.GetVal(); \
	} \
	return TMOK;
#endif

#define TRANSFER_REF \
	Assert(item.type == NDTRef); \
	if (_sending) \
	{ \
		if (!value) \
		{ \
			_msg->values[index] = \
				RefNetworkDataTyped<NetworkId>(NetworkId::Null()); \
		} \
		else \
		{ \
			NetworkId id = value->GetNetworkId(); \
			if (id.IsNull()) \
			{ \
				ErrF("Ref to nonnetwork object %s",(const char *)value->GetDebugName()); \
				return TMGeneric; \
			} \
			_msg->values[index] = RefNetworkDataTyped<NetworkId>(id); \
		} \
	} \
	else \
	{ \
		const RefNetworkData &val = _msg->values[index]; \
		CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped<NetworkId>) \
		NetworkId id = valTyped.GetVal(); \
		if (id.IsNull()) \
			value = NULL; \
		else \
			value = dynamic_cast<Type *>(_component->GetObject(id)); \
	} \
	return TMOK;

#define TRANSFER_REFS \
	Assert(item.type == NDTRefArray); \
	if (_sending) \
	{ \
		RefNetworkDataTyped< AutoArray<NetworkId> > valTyped; \
		_msg->values[index] = valTyped; \
		AutoArray<NetworkId> &array = valTyped.GetVal(); \
		int n = value.Size(); \
		array.Resize(n); \
		for (int i=0; i<n; i++) \
		{ \
			Type *item = value[i]; \
			if (item) \
			{ \
				NetworkId id = item->GetNetworkId(); \
				if (id.IsNull()) \
				{ \
					ErrF("Ref to nonnetwork object %s",(const char *)item->GetDebugName()); \
					return TMGeneric; \
				} \
				array[i] = id; \
			} \
			else \
				array[i] = NetworkId::Null(); \
		} \
	} \
	else \
	{ \
		const RefNetworkData &val = _msg->values[index]; \
		CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped< AutoArray<NetworkId> >) \
		AutoArray<NetworkId> &array = valTyped.GetVal(); \
		int n = array.Size(); \
		value.Resize(n); \
		for (int i=0; i<n; i++) \
		{ \
			NetworkId id = array[i]; \
			if (id.IsNull()) \
				value[i] = NULL; \
			else \
				value[i] = dynamic_cast<Type *>(_component->GetObject(id)); \
		} \
	} \
	return TMOK;

//! Network message context
/*!
Passed into TransferMsg functions of network objects.
Contains all info needed for network transfer (and message itself).
*/
class NetworkMessageContext
{
protected:
	//! message itself
	NetworkMessage *_msg;
	//! message format
	NetworkMessageFormatBase *_format;
	//! server / client class which called TransferMsg
	INetworkComponent *_component;	
	//! message update class
	NetworkMessageClass _cls;
	// DWORD _client;
	//! sending / receiving message
	bool _sending;
	//! initial (guaranteed) update of network object
	bool _initialUpdate;

public:
	//! Constructor
	/*!
	\param msg message itself
	\param format message format
	\param component server / client class which called TransferMsg
	\param client not used
	\param sending sending / receiving message
	*/
	NetworkMessageContext
	(
		NetworkMessage *msg,
		NetworkMessageFormatBase *format,
		INetworkComponent *component,
		DWORD client,
		bool sending
	)
	{
		_msg = msg; _format = format;
		_component = component; //_client = client;
		_sending = sending; _cls = NMCUpdateGeneric;
		_initialUpdate = false;
		if (_sending) PrepareMessage();
	}
	//! Constructor
	/*!
	\param msg message itself
	\param format message format
	\param ctx parent message context
	*/
	NetworkMessageContext
	(
		NetworkMessage *msg,
		NetworkMessageFormatBase *format,
		NetworkMessageContext &ctx
	)
	{
		_msg = msg; _format = format;
		_component = ctx._component; //_client = ctx._client;
		_sending = ctx._sending; _cls = ctx._cls;
		_initialUpdate = ctx._initialUpdate;
		if (_sending) PrepareMessage();
	}
	//! Return preloaded indices
	const NetworkMessageIndices *GetIndices() const {return _format->GetIndices();}

	//! Return if message is to send
	bool IsSending() const {return _sending;}
	//! Return message update class
	NetworkMessageClass GetClass() const {return _cls;}
	//! Set message update class
	void SetClass(NetworkMessageClass cls) {_cls = cls;}
	//! Return if initial (guaranteed) update is performed
	bool GetInitialUpdate() const {return _initialUpdate;}
	//! Set if initial (guaranteed) update is performed
	void SetInitialUpdate() {_initialUpdate = true;}
	//! Return message format
	const NetworkMessageFormatBase *GetFormat() const {return _format;}
	//! Return message format
	NetworkMessageFormatBase *GetFormat() {return _format;}
	//! Return server / client class which called TransferMsg
	INetworkComponent *GetComponent() const {return _component;}
	//! Return message itself
	NetworkMessage *GetMessage() const {return _msg;}

	//! Return time when message was sent
	Time GetMsgTime() const {return _msg->time;}
	// DWORD GetClient() const {return _client;}

	//@{
	//! Transfer single item of message (raw data)
	TMError IdxGetRaw(int index, void *&value, int &size);
	TMError IdxSendRaw(int index, void *value, int size);
	//@}
	//@{
	//! Transfer single item of message
	/*!
	\param index index of item in message
	\param value transferred value
	\name Transfer functions (index based)
	*/
	TMError IdxTransfer(int index, bool &value);
	TMError IdxTransfer(int index, int &value);
	TMError IdxTransfer(int index, float &value);
	TMError IdxTransfer(int index, RString &value);
	TMError IdxTransfer(int index, Time &value);
	TMError IdxTransfer(int index, Vector3 &value);
	TMError IdxTransfer(int index, Matrix3 &value);

	template <class Type>
	TMError IdxTransfer(int index, AutoArray<Type> &value)
	{
		TRANSFER_FORMAT
		Assert
		(
			item.type == GetNDType< AutoArray<Type> >::value
		);
		TRANSFER(AutoArray<Type>)
	}
	template <class Type>
	TMError IdxTransfer(int index, StaticArrayAuto<Type> &value)
	{
		TRANSFER_FORMAT
		Assert
		(
			item.type == GetNDType< StaticArrayAuto<Type> >::value
		);
		if (_sending)
		{
			RefNetworkDataTyped< AutoArray<Type> > valTyped;
			AutoArray<Type> &array = valTyped.GetVal();
			array.Copy(value.Data(),value.Size());
			_msg->values[index] = valTyped;
		}
		else
		{
			const RefNetworkData &val = _msg->values[index];
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped< AutoArray<Type> >)
			AutoArray<Type> &array = valTyped.GetVal();
			value.Copy(array.Data(),array.Size());
		}
		return TMOK;
	}
	/*
	TMError IdxTransfer(int index, AutoArray<char> &value);
	TMError IdxTransfer(int index, AutoArray<int> &value);
	TMError IdxTransfer(int index, AutoArray<float> &value);
	TMError IdxTransfer(int index, AutoArray<RString> &value);
	*/

	TMError IdxTransfer(int index, RadioSentence &value);
	template <class Type>
	TMError IdxTransferRef(int index, Type *&value)
	{
		TRANSFER_FORMAT
		TRANSFER_REF
	}
	template <class Type>
	TMError IdxTransferRef(int index, Ref<Type> &value)
	{
		TRANSFER_FORMAT
		TRANSFER_REF
	}
	template <class Type>
	TMError IdxTransferRef(int index, OLink<Type> &value)
	{
		TRANSFER_FORMAT
		TRANSFER_REF
	}
	template <class Type>
	TMError IdxTransferRefs(int index, RefArray<Type> &value)
	{
		TRANSFER_FORMAT
		TRANSFER_REFS
	}
	template <class Type>
	TMError IdxTransferRefs(int index, OLinkArray<Type> &value)
	{
		TRANSFER_FORMAT
		TRANSFER_REFS
	}
	template <class Type>
	TMError IdxTransferObject(int index, Type &value)
	{
		TRANSFER_FORMAT
		Assert
		(
			item.type == NDTObject
		);

		// message format
		const RefNetworkData &defVal = item.defValue;
		CHECK_ASSIGN(defValTyped, defVal, const RefNetworkDataTyped<int>)
		NetworkMessageFormatBase *formatMsg = _component->GetFormat((NetworkMessageType)defValTyped.GetVal());
		if (!formatMsg) return TMBadFormat;

		if (_sending)
		{
			RefNetworkDataTyped<NetworkMessage> valTyped;
				//RefNetworkDataTyped<NetworkMessage>();
			_msg->values[index] = valTyped;
			NetworkMessage &msg = valTyped.GetVal();
			NetworkMessageContext ctx(&msg, formatMsg, *this);
			TMCHECK(value.TransferMsg(ctx))
		}
		else
		{
			const RefNetworkData &val = _msg->values[index];
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped<NetworkMessage>)
			NetworkMessage &msg = valTyped.GetVal();
			NetworkMessageContext ctx(&msg, formatMsg, *this);
			TMCHECK(value.TransferMsg(ctx))
		}

		return TMOK;
	}
	template <class Type>
	TMError IdxTransferContent(int index, Ref<Type> &value)
	{
		TRANSFER_FORMAT
		Assert
		(
			item.type == NDTObject
		);

		// messages format
		const RefNetworkData &defVal = item.defValue;
		CHECK_ASSIGN(defValTyped, defVal, const RefNetworkDataTyped<int>)
		NetworkMessageFormatBase *formatMsg = _component->GetFormat((NetworkMessageType)defValTyped.GetVal());
		if (!formatMsg) return TMBadFormat;

		if (_sending)
		{
			// access to message
			RefNetworkDataTyped<NetworkMessage> valTyped;
			_msg->values[index] = valTyped;

			// transfer
			NetworkMessage &msg = valTyped.GetVal();
			NetworkMessageContext ctx(&msg, formatMsg, *this);
			TMCHECK(value->TransferMsg(ctx))
		}
		else
		{
			// access to message
			const RefNetworkData &val = _msg->values[index];
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped<NetworkMessage>)

			// transfer
			NetworkMessage &msg = valTyped.GetVal();
			NetworkMessageContext ctx(&msg, formatMsg, *this);
			value = Type::CreateObject(ctx);
			TMCHECK(value->TransferMsg(ctx))
		}

		return TMOK;
	}
	template <class Type>
	TMError IdxTransferArray(int index, AutoArray<Type> &value)
	{
		TRANSFER_FORMAT
		Assert
		(
			item.type == NDTObjectArray
		);

		// messages format
		const RefNetworkData &defVal = item.defValue;
		CHECK_ASSIGN(defValTyped, defVal, const RefNetworkDataTyped<int>)
		NetworkMessageFormatBase *formatItem = _component->GetFormat((NetworkMessageType)defValTyped.GetVal());
		if (!formatItem) return TMBadFormat;

		if (_sending)
		{
			// access to message array
			RefNetworkDataTyped< AutoArray<NetworkMessage> > valTyped;
			//	RefNetworkDataTyped< AutoArray<NetworkMessage> >();
			_msg->values[index] = valTyped;
			AutoArray<NetworkMessage> &array = valTyped.GetVal();

			// transfer
			int n = value.Size();
			array.Resize(n);
			for (int i=0; i<n; i++)
			{
				NetworkMessageContext ctx(&array[i], formatItem, *this);
				TMCHECK(value[i].TransferMsg(ctx))
			}
		}
		else
		{
			// access to message array
			const RefNetworkData &val = _msg->values[index];
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped< AutoArray<NetworkMessage> >)
			AutoArray<NetworkMessage> &array = valTyped.GetVal();

			// transfer
			int n = array.Size();
			value.Resize(n);
			for (int i=0; i<n; i++)
			{
				NetworkMessageContext ctx(&array[i], formatItem, *this);
				TMCHECK(value[i].TransferMsg(ctx))
			}
		}

		return TMOK;
	}
	template <class Type>
	TMError IdxTransferArray(int index, StaticArrayAuto<Type> &value)
	{
		TRANSFER_FORMAT
		Assert
		(
			item.type == NDTObjectArray
		);

		// messages format
		const RefNetworkData &defVal = item.defValue;
		CHECK_ASSIGN(defValTyped, defVal, const RefNetworkDataTyped<int>)
		NetworkMessageFormatBase *formatItem = _component->GetFormat((NetworkMessageType)defValTyped.GetVal());
		if (!formatItem) return TMBadFormat;

		if (_sending)
		{
			// access to message array
			RefNetworkDataTyped< AutoArray<NetworkMessage> > valTyped;
			//	RefNetworkDataTyped< AutoArray<NetworkMessage> >();
			_msg->values[index] = valTyped;
			AutoArray<NetworkMessage> &array = valTyped.GetVal();

			// transfer
			int n = value.Size();
			array.Resize(n);
			for (int i=0; i<n; i++)
			{
				NetworkMessageContext ctx(&array[i], formatItem, *this);
				TMCHECK(value[i].TransferMsg(ctx))
			}
		}
		else
		{
			// access to message array
			const RefNetworkData &val = _msg->values[index];
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped< AutoArray<NetworkMessage> >)
			AutoArray<NetworkMessage> &array = valTyped.GetVal();

			// transfer
			int n = array.Size();
			value.Resize(n);
			for (int i=0; i<n; i++)
			{
				NetworkMessageContext ctx(&array[i], formatItem, *this);
				TMCHECK(value[i].TransferMsg(ctx))
			}
		}

		return TMOK;
	}
	template <class Type>
	TMError IdxTransferArraySimple(int index, StaticArrayAuto<Type> &value)
	{
		TRANSFER_FORMAT
		Assert
		(
			item.type == NDTObjectArray
		);

		// messages format
		const RefNetworkData &defVal = item.defValue;
		CHECK_ASSIGN(defValTyped, defVal, const RefNetworkDataTyped<int>)
		NetworkMessageFormatBase *formatItem = _component->GetFormat((NetworkMessageType)defValTyped.GetVal());
		if (!formatItem) return TMBadFormat;

		if (_sending)
		{
			// access to message array
			RefNetworkDataTyped< AutoArray<NetworkMessage> > valTyped;
			//	RefNetworkDataTyped< AutoArray<NetworkMessage> >();
			_msg->values[index] = valTyped;
			AutoArray<NetworkMessage> &array = valTyped.GetVal();

			// transfer
			int n = value.Size();
			array.Resize(n);
			for (int i=0; i<n; i++)
			{
				NetworkMessageContext ctx(&array[i], formatItem, *this);
				TMCHECK(value[i].TransferMsgSimple(ctx))
			}
		}
		else
		{
			// access to message array
			const RefNetworkData &val = _msg->values[index];
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped< AutoArray<NetworkMessage> >)
			AutoArray<NetworkMessage> &array = valTyped.GetVal();

			// transfer
			int n = array.Size();
			value.Resize(n);
			for (int i=0; i<n; i++)
			{
				NetworkMessageContext ctx(&array[i], formatItem, *this);
				TMCHECK(value[i].TransferMsgSimple(ctx))
			}
		}

		return TMOK;
	}
	template <class Type>
	TMError IdxTransferArray(int index, RefArray<Type> &value)
	{
		TRANSFER_FORMAT
		Assert
		(
			item.type == NDTObjectArray
		);

		// messages format
		const RefNetworkData &defVal = item.defValue;
		CHECK_ASSIGN(defValTyped, defVal, const RefNetworkDataTyped<int>)
		NetworkMessageFormatBase *formatItem = _component->GetFormat((NetworkMessageType)defValTyped.GetVal());
		if (!formatItem) return TMBadFormat;

		if (_sending)
		{
			// access to message array
			RefNetworkDataTyped< AutoArray<NetworkMessage> > valTyped;
			//	RefNetworkDataTyped< AutoArray<NetworkMessage> >();
			_msg->values[index] = valTyped;
			AutoArray<NetworkMessage> &array = valTyped.GetVal();

			// transfer
			int n = value.Size();
			array.Resize(n);
			for (int i=0; i<n; i++)
			{
				NetworkMessageContext ctx(&array[i], formatItem, *this);
				TMCHECK(value[i]->TransferMsg(ctx))
			}
		}
		else
		{
			// access to message array
			const RefNetworkData &val = _msg->values[index];
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped< AutoArray<NetworkMessage> >)
			AutoArray<NetworkMessage> &array = valTyped.GetVal();

			// transfer
			int n = array.Size();
			value.Resize(n);
			for (int i=0; i<n; i++)
			{
				NetworkMessageContext ctx(&array[i], formatItem, *this);
				value[i] = new Type();
				TMCHECK(value[i]->TransferMsg(ctx))
			}
		}

		return TMOK;
	}
	template <class Type>
	TMError IdxTransferObjArray(int index, RefArray<Type> &value)
	{
		TRANSFER_FORMAT
		Assert
		(
			item.type == NDTObjectArray
		);

		// messages format
		const RefNetworkData &defVal = item.defValue;
		CHECK_ASSIGN(defValTyped, defVal, const RefNetworkDataTyped<int>)
		NetworkMessageFormatBase *formatItem = _component->GetFormat((NetworkMessageType)defValTyped.GetVal());
		if (!formatItem) return TMBadFormat;

		if (_sending)
		{
			// access to message array
			RefNetworkDataTyped< AutoArray<NetworkMessage> > valTyped;
			//	RefNetworkDataTyped< AutoArray<NetworkMessage> >();
			_msg->values[index] = valTyped;
			AutoArray<NetworkMessage> &array = valTyped.GetVal();

			// transfer
			int n = value.Size();
			array.Resize(n);
			for (int i=0; i<n; i++)
			{
				NetworkMessageContext ctx(&array[i], formatItem, *this);
				TMCHECK(value[i]->TransferMsg(ctx))
			}
		}
		else
		{
			// access to message array
			const RefNetworkData &val = _msg->values[index];
			CHECK_ASSIGN(valTyped, val, const RefNetworkDataTyped< AutoArray<NetworkMessage> >)
			AutoArray<NetworkMessage> &array = valTyped.GetVal();

			// transfer
			int n = array.Size();
			value.Resize(n);
			for (int i=0; i<n; i++)
			{
				NetworkMessageContext ctx(&array[i], formatItem, *this);
				value[i] = Type::CreateObject(ctx);
				TMCHECK(value[i]->TransferMsg(ctx))
			}
		}

		return TMOK;
	}
	//@}

	//! Transfer single item of message (type NDTData - generic data with format)
	/*!
	\param index index of item in message
	\param type data type
	\param compression compression type
	\param defVal transferred data itself
	*/
	TMError IdxTransfer
	(
		int index,
		NetworkDataType &type, NetworkCompressionType &compression,
		RefNetworkData &defVal
	);

	//! Retrieves network id of object from given item of message
	/*!
	\param index index of item in message
	\param id output id
	*/
	TMError IdxGetId(int index, NetworkId &id);
	//! Retrieves array of network ids of objects from given item of message
	/*!
	\param index index of item in message
	\param array output array of ids
	*/
	TMError IdxGetIds(int index, AutoArray<NetworkId> &array);

	//! Log message into debug output
	/*!
	\param level debug level
	\param indent indentation string
	*/
	void LogMessage(int level, RString indent) const;

protected:
	//! Prepare message for receiving (create values and fill by default values)
	void PrepareMessage();
};

// macros
#define SCAN(name) name = format->FindIndex(#name);

#define ITRANSF(name) \
	TMCHECK(ctx.IdxTransfer(indices-> name, _##name))
#define ITRANSF_REF(name) \
	TMCHECK(ctx.IdxTransferRef(indices-> name, _##name))
#define ITRANSF_REFS(name) \
	TMCHECK(ctx.IdxTransferRefs(indices-> name, _##name))
#define ITRANSF_ENUM(name) \
	TMCHECK(ctx.IdxTransfer(indices-> name, (int &)_##name))
#define ITransferBitField(type, ctx, index, value) \
	{ \
		type t = value; \
		TMCHECK(ctx.IdxTransfer(index, t)); \
		value = t; \
	}
#define ITransferBitBool(ctx, index, value) \
	ITransferBitField(bool, ctx, name, value)
#define ITRANSF_BITFIELD(type, name) \
	ITransferBitField(type, ctx, indices-> name, _##name)
#define ITRANSF_BITBOOL(name) \
	ITransferBitField(bool, ctx, indices-> name, _##name)

/*
#define TRANSF(name) \
	TMCHECK(ctx.Transfer(#name, _##name))
#define TRANSF_REF(name) \
	TMCHECK(ctx.TransferRef(#name, _##name))
#define TRANSF_REFS(name) \
	TMCHECK(ctx.TransferRefs(#name, _##name))
#define TRANSF_ENUM(name) \
	TMCHECK(ctx.Transfer(#name, (int &)_##name))
#define TransferBitField(type, ctx, name, value) \
	{ \
		type t = value; \
		TMCHECK(ctx.Transfer(name, t)); \
		value = t; \
	}
#define TransferBitBool(ctx, name, value) \
	TransferBitField(bool, ctx, name, value)
#define TRANSF_BITFIELD(type, name) \
	TransferBitField(type, ctx, #name, _##name)
#define TRANSF_BITBOOL(name) \
	TransferBitField(bool, ctx, #name, _##name)
*/

#define ICALCERR_NEQ(type, name, value) \
	{ \
		type name; \
		if (ctx.IdxTransfer(indices-> name, name) == TMOK) \
			if (name != _##name) error += value; \
	}
#define ICALCERRE_NEQ(type, name, member, value) \
	{ \
		type name; \
		if (ctx.IdxTransfer(indices-> name, name) == TMOK) \
			if (name != member) error += value; \
	}
#define ICALCERR_NEQREF(type, name, value) \
	{ \
		type *name; \
		if (ctx.IdxTransferRef(indices-> name, name) == TMOK) \
			if (name != _##name) error += value; \
	}
#define ICALCERRE_NEQREF(type, name, member, value) \
	{ \
		type *name; \
		if (ctx.IdxTransferRef(indices-> name, name) == TMOK) \
			if (name != member) error += value; \
	}
#define ICALCERR_NEQSTR(name, value) \
	{ \
		RString name; \
		if (ctx.IdxTransfer(indices-> name, name) == TMOK) \
			if (stricmp(name, _##name) != 0) error += value; \
	}
#define ICALCERRE_NEQSTR(name, member, value) \
	{ \
		RString name; \
		if (ctx.IdxTransfer(indices-> name, name) == TMOK) \
			if (stricmp(name, member) != 0) error += value; \
	}
#define ICALCERR_ABSDIF(type, name, value) \
	{ \
		type name; \
		if (ctx.IdxTransfer(indices-> name, name) == TMOK) \
			error += value * fabs(name - _##name); \
	}
#define ICALCERRE_ABSDIF(type, name, member, value) \
	{ \
		type name; \
		if (ctx.IdxTransfer(indices-> name, name) == TMOK) \
			error += value * fabs(name - member); \
	}

#define ICALCERR_DIST2(name, value) \
	{ \
		Vector3 name; \
		if (ctx.IdxTransfer(indices-> name, name) == TMOK) \
			error += value * name.Distance2(_##name); \
	}
#define ICALCERRE_DIST2(name, member, value) \
	{ \
		Vector3 name; \
		if (ctx.IdxTransfer(indices-> name, name) == TMOK) \
			error += value * name.Distance2(member); \
	}
#define ICALCERR_MATRIX(name, value) \
	{ \
		Matrix3 name; \
		if (ctx.IdxTransfer(indices-> name, name) == TMOK) \
		{ \
			float errOrient = 0; \
			for (int i=0; i<3; i++) for (int j=0; j<3; j++) \
				errOrient += Square(name(i, j) - _##name(i, j)); \
			error += value * errOrient; \
		} \
	}
#define ICALCERRE_MATRIX(name, member, value) \
	{ \
		Matrix3 name; \
		if (ctx.IdxTransfer(indices-> name, name) == TMOK) \
		{ \
			float errOrient = 0; \
			for (int i=0; i<3; i++) for (int j=0; j<3; j++) \
				errOrient += Square(name(i, j) - member(i, j)); \
			error += value * errOrient; \
		} \
	}

#define ICALCERR_DIST(name, value) \
	{ \
		Vector3 name; \
		if (ctx.IdxTransfer(indices-> name, name) == TMOK) \
			error += value * name.Distance(_##name); \
	}
#define ICALCERRE_DIST(name, member, value) \
	{ \
		Vector3 name; \
		if (ctx.IdxTransfer(indices-> name, name) == TMOK) \
			error += value * name.Distance(member); \
	}

#include <Es/Containers/rStringArray.hpp>

//! Description of currently played mission
struct MissionHeader : public NetworkSimpleObject
{
	//! name of island
	RString island;
	//! name of mission
	RString name;
	//! description of mission
	RString description;

	//! filename of mission directory
	RString fileName;
	//! placement (path) where mission directory is
	RString fileDir;
	//@{
	//! size of mission pbo file
	int fileSizeL;
	int fileSizeH;
	//@}
/*
	int fileTimeL;
	int fileTimeH;
*/
	//! CRC of mission pbo file
	int fileCRC;

	//! play in cadet / veteran mode
	bool cadetMode;
#if _ENABLE_AI
	//! AI playable units are disabled / enabled
	bool disabledAI;
#endif
	// ADDED
	//! show AI playable units in MP statistics
	bool aiKills;

	// only update _missionHeader without other actions
	bool updateOnly;

	//! detailed difficulty description
	AutoArray<bool> difficulty;

	//! respawn mode
	RespawnMode respawn;
	//! respawn delay
	float respawnDelay;

	//! Names of needed AddOns
	FindArrayRStringCI addOns;

	//! Starting time of mission
	int start;

	//! Estimated time left to end of mission
	Time estimatedEndTime;

	//@{
	//! mission parameters
	RString titleParam1;
	AutoArray<float> valuesParam1;
	AutoArray<RString> textsParam1;
	float defValueParam1;

	RString titleParam2;
	AutoArray<float> valuesParam2;
	AutoArray<RString> textsParam2;
	float defValueParam2;
	//@}

	//! Constructor
	MissionHeader();
	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTMissionHeader;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

enum TargetSide;

//! Position in vehicle
enum PlayerRolePosition
{
	PRPNone,
	PRPCommander,
	PRPDriver,
	PRPGunner
};

//! Player role slot
/*!
Used for assignment players to role slots.
*/
struct PlayerRole : public NetworkSimpleObject
{
	//! side
	TargetSide side;
	//! group index (inside side)
	int group;
	//! unit index (inside group)
	int unit;
	//! display name of vehicle type
	RString vehicle;
	//! position in vehicle
	PlayerRolePosition position;
	//! if it's group leader
	bool leader;
	//! slot is locked
	bool roleLocked;
	//! DirectPlay ID of assigned player (AI_PLAYER or NO_PLAYER if unassigned)
	int player;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTPlayerRole;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};
TypeIsMovable(PlayerRole)

//! Squad description
struct SquadIdentity : public RefCount, public NetworkSimpleObject
{
	//! unique id of squad (URL of XML page)
	RString id;
	//! nick (short) name of squad
	RString nick;
	//! full name of squad
	RString name;
	//! e-mail of squad administrator
	RString email;
	//! web page of squad
	RString web;
	//! picture of squad (shown on vehicles)
	RString picture;
	//! title of squad (shown on vehicles)
	RString title;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTSquad;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};
TypeIsMovable(SquadIdentity)

//! State of delayed kick-off
enum KickOffState
{
  /// waiting for kick off because of modified exe
	KOExe,
  /// waiting for kicking
	KOWait,
  /// waiting for warning
	KOWarning,
	KODone
};

//! what king of priviledges has given player
enum PlayerRights
{
	PRNone=0,
	PRSideCommander=1,
	PRVotedAdmin=2,
	PRAdmin=4,
	PRServer=8
};

//! Player description
struct PlayerIdentity : public NetworkSimpleObject
{
	//! DirectPlay ID of player
	int dpnid;
	//! ID unique in session (shorter than dpnid)
	int playerid;
	//! unique id of player (derivated from CD key)
	RString id;
	//! nick (short) name of player
	RString name;
	//! selected face
	RString face;
	//! selected glasses
	RString glasses;
	//! selected speaker
	RString speaker;
	//! selected voice pitch
	float pitch;
	//! squad (if confirmed)
	Ref<SquadIdentity> squad;
	//! unique id of squad (if confirmed)
	RString squadId;
	//! full name of player
	RString fullname;
	//! e-mail of player
	RString email;
	//! ICQ of player
	RString icq;
	//! remark about player
	RString remark;
	UITime kickOffTime; //!< id is "0", kickoff will follow
	KickOffState kickOffState; //!< chanded during kickoff process
	//! user was kicked
	bool destroy;
	//! timeout for player destroy after kick
	UITime destroyTime;
	//! state of player's network client
	NetworkGameState state;
	//! version player is using
	//! if zero, it may be anything up to 1.42
	//! it was introduced in 1.43
	int version;
  /// number of failed admin logins in row
  int failedLogin;
	
	/*!
	\name Client-Server network conditions
	This information is updated more often (by UpdatePosition messages)
	*/
	//@{
	//! ping range estimation
	int _minPing,_avgPing,_maxPing;
	//! bandwidth estimation (in kbps)
	int _minBandwidth, _avgBandwidth, _maxBandwidth;
	//! current desync level (max. error of unsent messages)
	int _desync;
	//! special rights of given player
	int _rights;
	//@}

	//! returns name displayed in game - nick + squad nick
	RString GetName() const;

	PlayerIdentity();
	~PlayerIdentity();

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};
TypeIsMovable(PlayerIdentity)

//! Type of integrity check
enum IntegrityQuestionType
{
	IQTConfig,
	IQTExe,
	IQTData,
	IntegrityQuestionTypeCount
};

//! return name of player on local client
RString GetLocalPlayerName();

//! network message indices for Magazine and MagazineNetworkInfo classes
class IndicesMagazine : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int type;
	int ammo;
	int burstLeft;
	int reload;
	int reloadMagazine;
	int creator;
	int id;
	//@}

	//! Constructor
	IndicesMagazine();
	NetworkMessageIndices *Clone() const {return new IndicesMagazine;}
	void Scan(NetworkMessageFormatBase *format);
};

//! helper class for simple transfer of Magazine over network
struct MagazineNetworkInfo : public NetworkSimpleObject
{
	//! see Magazine::_type
	RString _type;	
	//! see Magazine::_ammo
	int _ammo;
	//! see Magazine::_burstLeft
	int _burstLeft;
	//! see Magazine::_reload
	float _reload;
	//! see Magazine::_reloadMagazine
	float _reloadMagazine;

	//! see Magazine::_creator
	int _creator;
	//! see Magazine::_id
	int _id;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const {return NMTMagazine;}
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
	TMError TransferMsgSimple(NetworkMessageContext &ctx);
};
TypeIsMovable(MagazineNetworkInfo)

#endif
