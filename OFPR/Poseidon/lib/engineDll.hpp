#ifdef _MSC_VER
#pragma once
#endif

#ifndef _ENGINE_DLL_HPP
#define _ENGINE_DLL_HPP

#include <Es/Common/win.h>

struct CreateEnginePars
{
	// in
	HINSTANCE hInst;
	HINSTANCE hPrev;
	int sw;
	int w,h,bpp;
	// in/out
	bool UseWindow;
	// out
	bool HideCursor;
	bool NoRedrawWindow;
};

// import configuration dependent functions
Engine *CreateEngineGlide( CreateEnginePars &pars );
Engine *CreateEngineD3D( CreateEnginePars &pars );

//extern EngineImport *ImportedF; // used in DLL
//extern EngineImport EngineGlobal; // used in EXE

#endif
