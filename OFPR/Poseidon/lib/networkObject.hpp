#ifdef _MSC_VER
#pragma once
#endif

#ifndef _NETWORK_OBJECT_HPP
#define _NETWORK_OBJECT_HPP

#include <Es/Types/lLinks.hpp>

#ifdef PREPROCESS_DOCUMENTATION
#include <Es/Containers/typeDefines.hpp>
#include "normalConfig.h"
#include "math3d.hpp"
#endif

/*!
\file
Interface file for base objects transferred over network in multiplayer game
*/

#define OLink LLink
#define OLinkArray LLinkArray
#define RemoveOLinks RemoveLLinks
#define TypeContainsOLink TypeIsMovable
//#define OLink Link
//#define OLinkArray LinkArray
//#define RemoveOLinks RefCountWithLinks
//#define TypeContainsOLink TypeIsGeneric

class NetworkMessageContext;
struct NetworkMessage;
class NetworkMessageFormatBase;
class NetworkMessageFormat;
DECL_ENUM(TMError)
DECL_ENUM(NetworkMessageType)

//! message update class for objects
enum NetworkMessageClass
{
	NMCCreate = -1,
	NMCUpdateFirst = 0,	// update values are used as index into array
	NMCUpdateGeneric = NMCUpdateFirst,
	NMCUpdatePosition,
	NMCUpdateDammage, // dammage and hit update
	NMCUpdateN
};

#include <Es/Strings/rString.hpp>

//! simple network object
/*!
- used as base class for guaranteed message structures
*/
class NetworkSimpleObject
{
public:
	//! Destructor
	virtual ~NetworkSimpleObject() {}
	//! Returns message type for this object and message class
	/*!
		\param cls messsage update class
	*/
	virtual NetworkMessageType GetNMType(NetworkMessageClass cls = NMCCreate) const = NULL;
	//! Create message format for this object and given class
	/*!
		\param cls message class of message
		\param format creating format
		\return created format
	*/
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	) {return format;}
	//! Message update from / to this object
	/*!
		\param ctx message context
	*/
	virtual TMError TransferMsg(NetworkMessageContext &ctx) = NULL;
};

//! unique identifier of network objects
struct NetworkId
{
	//! Player id of client where network object was created
	int creator;
	//! Unique id of network object on given client
	int id;

	//! Constructor
	NetworkId() {creator = 0; id = 0;}
	//! Constructor
	/*!
	\param c player id of client where network object was created
	\param i unique id of network object on given client
	*/
	NetworkId(int c, int i) {creator = c; id = i;}
	//! Assignement operator
	void operator =(const NetworkId &src)
	{
		creator = src.creator;
		id = src.id;
	}
	//! Equality operator
	bool operator ==(const NetworkId &with) const
	{
		return with.creator == creator && with.id == id;
	}
	//! Nonequality operator
	bool operator !=(const NetworkId &with) const
	{
		return with.creator != creator || with.id != id;
	}
	
	//! Null (unassigned) network id
	static NetworkId Null() {return NetworkId();}
	//! Check if id value is null (unassigned)
	bool IsNull() const {return creator == 0;}
};
TypeIsMovable(NetworkId);

//! helper base class for faster TransferMsg
/*!
- used as base class for Indices... classes
- Indices clases contains indices of items in array of items
*/
class NetworkMessageIndices
{
public:
	//! Constructor
	NetworkMessageIndices() {};
	//! Destructor
	virtual ~NetworkMessageIndices() {};

	//! Create copy (clone) of this object
	virtual NetworkMessageIndices *Clone() const = 0;
	//! Initialize members from message format
	/*!
	\param format message format
	*/
	virtual void Scan(NetworkMessageFormatBase *format) = 0;
};

//! network message indices for NetworkObject
class IndicesNetworkObject : public NetworkMessageIndices
{
public:
	int objectCreator;
	int objectId;
	int objectPosition;
	int guaranteed;
	
	IndicesNetworkObject();
	NetworkMessageIndices *Clone() const {return new IndicesNetworkObject;}
	void Scan(NetworkMessageFormatBase *format);
};

#include "time.hpp"

//! updateable network object
/*!
	Base class for all objects with automatic update over network in multiplayer game
*/
class NetworkObject : public RemoveOLinks, public NetworkSimpleObject
{
protected:
	//! time when last update from server arrived
	Time _lastUpdateTime;

	//! time when prediction should stop (calculated by GetMaxPredictionTime() function)
	Time _maxPredictionTime;

public:
	//! Constructor
	NetworkObject();

	//! Set unique id of network object
	virtual void SetNetworkId(NetworkId &id) = NULL;
	//! Return unique id of network object
	virtual NetworkId GetNetworkId() const = NULL;
	//! Returns current position of this object
	virtual Vector3 GetCurrentPosition() const = NULL;
	//! Returns speaker position for this object
	virtual Vector3 GetSpeakerPosition() const {return GetCurrentPosition();}
	//! Enable / disable random lip movement (enabled during direct speaking chat)
	virtual void SetRandomLip(bool set = true) {}
	//! Return if object is local on this client
	virtual bool IsLocal() const = NULL;
	//! Set if object is local on this client
	virtual void SetLocal(bool local = true) = NULL;
	//! Destroy (remote) object
	virtual void DestroyObject() = NULL;

	//! Get debugging name (debugging only)
	virtual RString GetDebugName() const = NULL;

	//! How long is client side prediction allowed to predict this object
	virtual float GetMaxPredictionTime(NetworkMessageContext &ctx) const;

	//! Check if prediction should be frozen
	virtual bool CheckPredictionFrozen() const;

	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	virtual TMError TransferMsg(NetworkMessageContext &ctx) = NULL;
	//! Calculate difference between given message and current state
	/*!
		\param ctx context for given network message
		\return calculated error (difference)
	*/
	virtual float CalculateError(NetworkMessageContext &ctx);
	//! Calculate coeficient error must be multiplied by
	/*!
		- coeficient is 1 for near objects, for distant objects decrease to zero
		\param position current position of object
		\param cameraPosition camera position on receiving client
	*/
	static float CalculateErrorCoef(Vector3Par position, Vector3Par cameraPosition);
};

#endif
