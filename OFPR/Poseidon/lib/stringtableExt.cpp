#include "wpch.hpp"
#include "stringtableExt.hpp"

#define STRING(x) int IDS_##x;
#include "stringids.hpp"
#undef STRING

#include <El/Modules/modules.hpp>

INIT_MODULE(StringtableExt, 1)
{
#define STRING(x) IDS_##x = RegisterString("STR_"#x);
#include "stringids.hpp"
#undef STRING
}
