// Poseidon - vehicle class
// (C) 1997, Suma
#include "wpch.hpp"

#include "ai.hpp"
#include "tankOrCar.hpp"
#include "scene.hpp"
#include "global.hpp"
#include "network.hpp"
#include <El/Common/randomGen.hpp>

#include "SpecLods.hpp"

TankOrCarType::TankOrCarType( const ParamEntry *param )
:base(param),
_pilotPos(VZero),
_outPilotPos(VZero),
_outPilotOnTurret(false)
//_toIndicatorSpeedAxis(MIdentity),
//_toIndicatorRpmAxis(MIdentity)
{
}

void TankOrCarType::Load(const ParamEntry &par)
{
	base::Load(par);
	_canFloat = par>>"canFloat";
}

void TankOrCarType::InitShape()
{
	base::InitShape();

	_brakeLights.Init(_shape,"brzdove svetlo",NULL);

	const ParamEntry &par=*_par;

	_speedIndicator.Init(_shape, par >> "IndicatorSpeed");
	_speedIndicator2.Init(_shape, par >> "IndicatorSpeed2");
	_rpmIndicator.Init(_shape, par >> "IndicatorRPM");

	int level=_shape->FindLevel(VIEW_CARGO);
	if( level>=0 )
	{
		Shape *oShape=_shape->LevelOpaque(level);
		oShape->MakeCockpit();
	}
	level=_shape->FindLevel(VIEW_PILOT);
	if( level>=0 )
	{
		Shape *oShape=_shape->LevelOpaque(level);
		oShape->MakeCockpit();
	}
	level=_shape->FindLevel(0);
	if( level>=0 )
	{
		Shape *oShape=_shape->LevelOpaque(level);
		_outPilotPos=oShape->NamedPosition("pilot");
		// check if outPilot is in turet selection
		int index=oShape->FindNamedSel("otocVez");
		int pIndex=oShape->PointIndex("pilot");
		if( index>=0 && pIndex>=0 )
		{
			const Selection &turret=oShape->NamedSel(index);
			if( turret.IsSelected(pIndex) ) _outPilotOnTurret=true;
		}
	}

	GetValue(_gearSound, par>>"soundGear");
	_hasExhaust=_shape->MemoryPointExists("vyfuk start");
	Vector3 beg=_shape->MemoryPoint("vyfuk start");
	Vector3 end=_shape->MemoryPoint("vyfuk konec");
	_exhaustPos=end;
	_exhaustDir=(end-beg);
	_exhaustDir.Normalize();

	DEF_HIT_CFG(_shape,_engineHit,par>>"HitEngine",GetArmor());
}

TankOrCar::TankOrCar( VehicleType *name, Person *driver )
:base(name,driver),_pilotBrake(false),
_lastPilotBrake(Glob.time-60),
_freeFallUntil(Glob.time-60)
{
	float exhaustSize=GetMass()*(1.0/16000);
	saturate(exhaustSize,0.3,3);
	_exhaust.SetSize(exhaustSize);
}

void TankOrCar::AnimateSpeedIndicator(Matrix4 &trans, int level)
{
	trans = MIdentity;
	int selection = Type()->_speedIndicator.GetSelection(level);
	if (selection>=0)
	{
		AnimateMatrix(trans,level,selection);
	}
}

void TankOrCar::Animate( int level )
{
	if( !_shape->Level(level) ) return;
	float value = fabs(ModelSpeed()[2]);
	Matrix4 speedAnim;
	AnimateSpeedIndicator(speedAnim,level);

	Type()->_speedIndicator.SetValue(_shape, level, value, speedAnim);
	Type()->_speedIndicator2.SetValue(_shape, level, value);
	value = _rpm;
	Type()->_rpmIndicator.SetValue(_shape, level, value);

	// avoid flashing brake lights
	if( _pilotBrake && EngineIsOn() )
	{
		_lastPilotBrake=Glob.time;
	}
	if( _lastPilotBrake>Glob.time-0.4 )
	{
		Type()->_brakeLights.Unhide(_shape,level);
	}
	else
	{
		Type()->_brakeLights.Hide(_shape,level);
	}


	base::Animate(level);
}

void TankOrCar::Deanimate( int level )
{
	if( !_shape->Level(level) ) return;
	base::Deanimate(level);
}


void TankOrCar::Simulate( float deltaT, SimulationImportance prec )
{
	if( _isDead )
	{
		SmokeSourceVehicle *smoke=dyn_cast<SmokeSourceVehicle>(GetSmoke());
		if( smoke )
		{
			float explosionDelay = GRandGen.Gauss(0.2f,0.5f,1.5f);
			Time explosionTime = Glob.time+explosionDelay;
			smoke->Explode(explosionTime);
		}
		NeverDestroy();
	}

	
	base::Simulate(deltaT,prec);
}

float TankOrCar::GetSteerAheadSimul() const
{
	float val=base::GetSteerAheadSimul();
	if( Type()->_canFloat && _waterContact && !_landContact) val*=8;
	return val;
}

float TankOrCar::GetSteerAheadPlan() const
{
	float val=base::GetSteerAheadPlan();
	if( Type()->_canFloat && _waterContact && !_landContact) val*=8;
	return val;
}

float TankOrCar::GetPrecision() const
{
	float val=base::GetPrecision();
	if( Type()->_canFloat && _waterContact ) val*=3;
	return val;
}

/*!
\patch 1.90 Date 10/29/2002 by Ondra
- Fixed: Tank and car exhausts did not work (introduced in 1.85)
*/

void TankOrCar::SimulateExhaust( float deltaT, SimulationImportance prec )
{
	if( !EnableVisualEffects(prec) ) return;
	// simulate exhaust smoke
	if( Type()->_hasExhaust && EngineIsOn() )
	{
		Vector3 exhaustPos=PositionModelToWorld(Type()->_exhaustPos);
		float moreThrust=ThrustWanted()-Thrust();
		saturate(moreThrust,0,0.2);
		float dammage=GetTotalDammage();
		float intensity=Thrust()*0.3+moreThrust*3+0.1;
		if( _gearSound )
		{ // gear change smoke
			intensity=1;
		}
		intensity+=dammage*0.2;
		intensity*=1+dammage;
		float alpha=intensity*0.3;
		float density=intensity*0.5+0.05;
		_exhaust.SetAlpha(alpha);
		Vector3 cSpeed=DirectionModelToWorld(Type()->_exhaustDir);
		cSpeed*=9*floatMin(1,_exhaust.GetSize());

		static Color normColor(0.1,0.2,0.35);
		_exhaust.SetColor(normColor*(1-dammage));
		_exhaust.Simulate
		(
			exhaustPos+_speed*0.1,cSpeed+_speed*0.7,density,deltaT
		);
	}
}

NetworkMessageType TankOrCar::GetNMType(NetworkMessageClass cls) const
{
	switch (cls)
	{
	case NMCUpdateGeneric:
		return NMTUpdateTankOrCar;
	default:
		return base::GetNMType(cls);
	}
}

IndicesUpdateTankOrCar::IndicesUpdateTankOrCar()
{
	pilotBrake = -1;
}

void IndicesUpdateTankOrCar::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);

	SCAN(pilotBrake)
}

NetworkMessageIndices *GetIndicesUpdateTankOrCar() {return new IndicesUpdateTankOrCar();}

NetworkMessageFormat &TankOrCar::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	switch (cls)
	{
	case NMCUpdateGeneric:
		base::CreateFormat(cls, format);
		format.Add("pilotBrake", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("State of pilot brake (on / off)"), ET_NOT_EQUAL, ERR_COEF_MODE);
/*
		format.Add("gear", NDTInteger, NCTNone, DEFVALUE(int, 0));
		format.Add("gearWanted", NDTInteger, NCTNone, DEFVALUE(int, 0));
		format.Add("gearChangeTime", NDTTime, NCTNone, DEFVALUE(Time, Time(0)));
*/
		break;
	default:
		base::CreateFormat(cls, format);
		break;
	}
	return format;
}

TMError TankOrCar::TransferMsg(NetworkMessageContext &ctx)
{
	switch (ctx.GetClass())
	{
	case NMCUpdateGeneric:
		TMCHECK(base::TransferMsg(ctx))
		{
			Assert(dynamic_cast<const IndicesUpdateTankOrCar *>(ctx.GetIndices()))
			const IndicesUpdateTankOrCar *indices = static_cast<const IndicesUpdateTankOrCar *>(ctx.GetIndices());

			ITRANSF(pilotBrake)
		}
/*
		TMCHECK(ctx.Transfer("gear", _gearBox._gear))
		TMCHECK(ctx.Transfer("gearWanted", _gearBox._gearWanted))
		TMCHECK(ctx.Transfer("gearChangeTime", _gearBox._gearChangeTime))
*/
		break;
	default:
		return base::TransferMsg(ctx);
	}
	return TMOK;
}

float TankOrCar::CalculateError(NetworkMessageContext &ctx)
{
	float error = 0;
	switch (ctx.GetClass())
	{
	case NMCUpdateGeneric:
		error += base::CalculateError(ctx);
		{
			Assert(dynamic_cast<const IndicesUpdateTankOrCar *>(ctx.GetIndices()))
			const IndicesUpdateTankOrCar *indices = static_cast<const IndicesUpdateTankOrCar *>(ctx.GetIndices());
		
			ICALCERR_NEQ(bool, pilotBrake, ERR_COEF_MODE)
		}
		break;
	default:
		error += base::CalculateError(ctx);
		break;
	}
	return error;
}

