#ifdef _MSC_VER
#pragma once
#endif

#ifndef _KEY_INPUT_HPP
#define _KEY_INPUT_HPP

#include "global.hpp"
#include "object.hpp"

#ifdef PREPROCESS_DOCUMENTATION
#include "networkObject.hpp"
#endif

/*!
\patch 1.12 Date 08/06/2001 by Jirka
- Added: configuration for zoom on right mouse button
*/
DEFINE_ENUM_BEG(UserAction)
	// user defined keys
	UAMoveForward,
	UAMoveBack,
	UATurnLeft,
	UATurnRight,
	UAMoveUp,
	UAMoveDown,
	UAMoveFastForward,
	UAMoveSlowForward,
	UAMoveLeft,
	UAMoveRight,
	UAToggleWeapons,
	UAFire,
	UAReloadMagazine,
	UALockTargets,	// next / prev target - DIK_TAB
	UALockTarget,		// lock target - RIGHT MOUSE BUTTON
	UARevealTarget,
	UAPrevAction,
	UANextAction,
	UAAction,
	UAHeadlights,
	UANightVision,
	UABinocular,
	UAHandgun,
	UACompass,
	UAWatch,
	UAMap,
	UAHelp,
	UATimeInc,
	UATimeDec,
	UAOptics,
	UAPersonView,
	UATacticalView,
	UAZoomIn,
	UAZoomOut,
	UALookAround,
	UALookAroundToggle,
	UALookLeftDown,
	UALookDown,
	UALookRightDown,
	UALookLeft,
	UALookCenter,
	UALookRight,
	UALookLeftUp,
	UALookUp,
	UALookRightUp,
	UAPrevChannel,
	UANextChannel,
	UAChat,
	UAVoiceOverNet,
	UANetworkStats,
	UANetworkPlayers,
	UASelectAll,
	UATurbo,
	UASlow,

	/*!
	\patch 1.11 Date 7/31/2001 by Ondra
	- New: Joystick axis configuration.
	*/
	UAAxisTurn,
	UAAxisDive,
	UAAxisRudder,
	UAAxisThrust,
	/*!
	\patch 1.47 Date 3/5/2002 by Ondra
	- New: Mouseless control scheme possible.
	Aiming may be assigned to joystick buttons or keyboard.
	*/
	UAAimUp,
	UAAimDown,
	UAAimLeft,
	UAAimRight,
	/*!
	\patch_internal 1.79 Date 7/25/2002 by Jirka
	- Added: cheat keys are configurable
	*/
#if _ENABLE_CHEATS
	UACheat1,
	UACheat2,
#endif
	UAN	// terminator
DEFINE_ENUM_END(UserAction)

typedef AutoArray<int> KeyList;

struct UserActionDesc
{
	const char *name;
	int &desc;
	bool axis;
	// default values
	KeyList keys;

	UserActionDesc(const char *n, int &d, int first, ...);
	UserActionDesc(bool a, const char *n, int &d, int first, ...);
};

#define N_MOUSE_BUTTONS			8
#ifdef _XBOX
#define N_JOYSTICK_BUTTONS	32
#else
#define N_JOYSTICK_BUTTONS	8
#endif
#define N_JOYSTICK_AXES	    8 // 3 normal, 3 rotate, 2 sliders
#define N_JOYSTICK_POV	    8 // pov - 8 state

#define INPUT_DEVICE_MASK				0x00ff0000
#define INPUT_DEVICE_KEYBOARD		0x00000000
#define INPUT_DEVICE_MOUSE			0x00010000 //! mouse button
#define INPUT_DEVICE_STICK			0x00020000 //! stick button
#define INPUT_DEVICE_STICK_AXIS 0x00030000 //! stick axis
#define INPUT_DEVICE_STICK_POV  0x00040000 //! stick pov switch

enum CheatCode
{
	CheatNone,
	CheatUnlockCampaign,
	CheatExportMap,
	CheatWinMission,
	CheatSaveGame,
	CheatCrash,
	CheatFreeze,
};

const int JoystickNAxes = N_JOYSTICK_AXES;
const int JoystickNButtons = N_JOYSTICK_BUTTONS;

struct Input
{
	// map actions to input events
	static UserActionDesc userActionDesc[UAN];
	KeyList userKeys[UAN];
	bool actionDone[UAN];

	// keyboard	
#if _ENABLE_CHEATS
	bool cheat1;
	bool cheat2;
#endif

	DWORD keyPressed[256]; // time stamp of last key pressed event
	float keys[256]; // integrated key time (as part of deltaT)
	bool keysToDo[256]; // edges to process

	bool awaitCheat;
	CheatCode cheatActive;
	RString cheatInProgress;

	void ProcessKeyPressed(int dik); // cheat processing

	CheatCode CheatActivated() const {return cheatActive;}
	void CheatServed() {cheatActive=CheatNone;}

	// mouse
	float mouseButtons[N_MOUSE_BUTTONS];
	bool mouseButtonsToDo[N_MOUSE_BUTTONS];
	int mouseX,mouseY,mouseZ;
	bool mouseL,mouseR,mouseM;
	bool mouseLToDo,mouseRToDo,mouseMToDo; // click event
	bool mouseDClickToDo; // double click event

	float mouseSensitivityX;
	float mouseSensitivityY;

	float cursorMovedX,cursorMovedY,cursorMovedZ;
	float cursorX,cursorY; // accumulate cursor position (-1..+1)

	// stick
	bool stickPov[N_JOYSTICK_POV];
	bool stickPovOld[N_JOYSTICK_POV]; // edge detection
	bool stickPovToDo[N_JOYSTICK_POV];

	float stickButtons[N_JOYSTICK_BUTTONS];
	bool stickButtonsToDo[N_JOYSTICK_BUTTONS];

	float stickAxis[N_JOYSTICK_AXES];
	/*!\name Stick axis access
	Check userActionDesc, return corresponding axis
	*/
	//@{
	float GetStickForward() const;
	float GetStickLeft() const;
	float GetStickThrust() const;
	float GetStickRudder() const;
	//@}

	// actions
	bool lookAroundToggleEnabled; // toggle
	bool lookAroundEnabled; // result of both toggle and temporary
	
	bool fire,fireToDo;

	float keyTurnLeft,keyTurnRight;
	
	float keyMoveFastForward,keyMoveSlowForward;
	float keyMoveForward,keyMoveBack;
	float keyMoveUp,keyMoveDown;
	float keyMoveLeft,keyMoveRight;

	int gameFocusLost; // how many controls prevent game to have input focus
		
	float GetKey(int dik, bool checkFocus = true) const;
	bool GetKeyToDo(int dik, bool reset=true, bool checkFocus = true);

#if _ENABLE_CHEATS
	float GetCheat1(int dik) const;
	bool GetCheat1ToDo(int dik, bool reset=true);
	float GetCheat2(int dik) const;
	bool GetCheat2ToDo(int dik, bool reset=true);
#endif

	float GetMouseButton(int index, bool checkFocus = true) const;
	bool GetMouseButtonToDo(int index, bool reset = true, bool checkFocus = true);

	float GetJoystickButton(int index, bool checkFocus = true) const;
	bool GetJoystickButtonToDo(int index, bool reset = true, bool checkFocus = true);

	float GetAction(UserAction action, bool checkFocus = true) const;
	bool GetActionToDo(UserAction action, bool reset=true, bool checkFocus = true);

	bool GetFire(bool checkFocus = true) const;
	bool GetFireToDo(bool reset=true, bool checkFocus = true);

	bool GetMouseL(bool checkFocus = true);
	bool GetMouseR(bool checkFocus = true);
	bool GetMouseM(bool checkFocus = true);
	bool GetMouseLToDo(bool reset=true, bool checkFocus = true);
	bool GetMouseRToDo(bool reset=true, bool checkFocus = true);
	bool GetMouseMToDo(bool reset=true, bool checkFocus = true); // click event
	bool GetMouseDClickToDo(bool reset=true, bool checkFocus = true); // double click event

	void ChangeGameFocus(int add ) {gameFocusLost+=add;}
	void ResetGameFocus() {gameFocusLost=0;}

	Input();
	void ForgetKeys();
	void InitKeys();
	void LoadKeys();
	void SaveKeys();

	bool revMouse;
	bool mouseButtonsReversed;

	bool joystickEnabled;
	UITime joystickMoveLastActive,keyboardMoveLastActive;

	UITime mouseCursorLastActive;
	UITime mouseTurnLastActive;
	UITime keyboardCursorLastActive;
	UITime keyboardTurnLastActive;

	UITime joystickThrustLastActive;
	UITime keyboardThrustLastActive;

	/*!
	\name Stick movement detection
	*/
	//@{
	//! last axis state
	int jAxisLast[JoystickNAxes];
	//! time of last axis movement
	UITime jAxisLastActive[JoystickNAxes];

	//! last axis state
	int jAxisBigLast[JoystickNAxes];
	//! time of last axis movement
	UITime jAxisBigLastActive[JoystickNAxes];
	//@}

	bool JoystickThurstActive() const; // joystick vs. keyboard movement
	bool JoystickActive() const; // joystick vs. keyboard movement
	bool MouseCursorActive() const;  // mouse vs. keyboard cursor
	bool MouseTurnActive() const;  // mouse vs. keyboard cursor

	// KeyboardPilot should report when appropriate input is detected

	//! Mouse control touched - switch to mouse mode
	void CursorTouchedByMouse();
	//! Keyboard used to control direction - unbound mouse
	void CursorTouchedByKeyboard();
	//! Joystick used to control direction - unbound mouse
	void CursorTouchedByJoystick();

	//! Thrust controlled by keyboard
	void ThrustControledByKeyboard();
	//! Thrust controlled by stick
	void ThrustControledByJoystick();
};

extern Input GInput;

#endif
