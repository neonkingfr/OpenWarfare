#ifndef _MEMGROW_HPP
#define _MEMGROW_HPP

class MemGrow
{
	private:
	void *_data;
	int _reserved,_commited;
	int _pageSize;
	bool _error;

	protected:
	void DoConstruct();
	void DoConstruct( int size );
	void DoDestruct();

	private: // no copy
	MemGrow( const MemGrow &src );
	void operator = ( const MemGrow &src );

	public:
	MemGrow(){DoConstruct();}
	MemGrow( int size ){DoConstruct(size);}
	~MemGrow(){DoDestruct();}
	void Reserve( int size );
	bool Commit( int size );
	int GetCommited() const {return _commited;}
	int GetReserved() const {return _reserved;}
	void Clear(){DoDestruct();}
	void *Data() const {return _data;}
	int Size() const {return _reserved;}
};

#endif
