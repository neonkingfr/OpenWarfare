// Configuration parameters for super release

#define _VERIFY_KEY								1
#define _ENABLE_EDITOR						1
#define _ENABLE_ALL_MISSIONS			0
#define _ENABLE_MP								0
#define _ENABLE_AI								1
#define _ENABLE_CAMPAIGN					1
#define _ENABLE_SINGLE_MISSION		1
#define _ENABLE_BRIEF_EQ					1
#define _ENABLE_BRIEF_GRP					1
#define _ENABLE_PARAMS						1
#define _ENABLE_CHEATS						1
#define _ENABLE_ADDONS						1
#define _ENABLE_DEDICATED_SERVER	0
#define _ENABLE_GAMESPY						0
#define _FORCE_DEMO_ISLAND				0
#define _FORCE_SINGLE_VOICE				0
#define _ENABLE_AIRPLANES					1
#define _ENABLE_HELICOPTERS				1
#define _ENABLE_SHIPS							1
#define _ENABLE_CARS							1
#define _ENABLE_TANKS							1
#define _ENABLE_MOTORCYCLES				1
#define _ENABLE_PARACHUTES				1
#define _ENABLE_DATADISC					0
#define _ENABLE_VBS								0

#define _DISABLE_GUI							0 // no GUI (mouse, output, keyboard)

#define _TIME_ACC_MIN							1.0
#define _TIME_ACC_MAX							4.0
																
#if defined(_CZECH) || defined (_RUSSIAN)
#define _MACRO_CDP							  0
#else														
#define _MACRO_CDP							  1 // macrovision protection only for World release
#endif


// Registry key for saving player's name, IP address of server etc.
#define ConfigApp "Software\\Codemasters\\Operation Flashpoint"

// Application name
#if _RUSSIAN
	#define AppName "\x0CE\x0EF\x0E5\x0F0\x0E0\x0F6\x0E8\x0FF Flashpoint"
#else
	#define AppName "Operation Flashpoint"
#endif

// Preferences program
#define PreferencesExe	"OpFlashPreferences.exe"