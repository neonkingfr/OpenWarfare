// Implementation of control hierarchy (HTML, Tree)

#include "wpch.hpp"
#include "uiControls.hpp"
#include "keyInput.hpp"
#include "engine.hpp"
#include "world.hpp"
#include "vkCodes.h"
#include <ctype.h>
#include <El/Evaluator/express.hpp>

#include "resincl.hpp"
#include "txtPreload.hpp"

#include <El/QStream/QBStream.hpp>

#include <Es/Algorithms/qsort.hpp>

#include "mbcs.hpp"

/*!
\file
Implementation file for extended controls (HTML control and Tree control).
*/

RString FindPicture(RString name);

// for tree
#define SCROLL_SPEED		100.0
#define SCROLL_MIN			2.0
#define SCROLL_MAX			10.0

#define ISSPACE(c) ((c) >= 0 && (c) <= 32)

#define CX(x) (toInt((x) * w) + 0.5)
#define CY(y) (toInt((y) * h) + 0.5)

#define DrawBottom(i, color) GLOB_ENGINE->DrawLine(Line2DPixel(xx + i, yy + hh - 1 - i, xx + ww - i, yy + hh - 1 - i),	color, color);
#define DrawRight(i, color)	GLOB_ENGINE->DrawLine(Line2DPixel(xx + ww - 1 - i, yy + hh - 1 - i, xx + ww - 1 - i, yy + 0 + i), color, color);
#define DrawLeft(i, color) GLOB_ENGINE->DrawLine(Line2DPixel(xx + i, yy + hh - 1 - i, xx + i, yy + i), color, color);
#define DrawTop(i, color) GLOB_ENGINE->DrawLine(Line2DPixel(xx + i, yy + i, xx + ww - 1 - i, yy + i), color, color);

inline PackedColor ModAlpha( PackedColor color, float alpha )
{
	int a=toIntFloor(alpha*color.A8());
	saturate(a,0,255);
	return PackedColorRGB(color,a);
}

const float textBorder = 0.005;

///////////////////////////////////////////////////////////////////////////////
// class CHTMLContainer

Texture *HTMLField::GetTexture()
{
	if (condition.GetLength() > 0)
	{
		GameState *gstate = GWorld->GetGameState();
		bool result = gstate->EvaluateBool(condition);
		return result ? texture1 : texture2;
	}
	else return texture1;
}

CHTMLContainer::CHTMLContainer(const ParamEntry &cls)
{
	_bgColor = GetPackedColor(cls >> "colorBackground");
	_textColor = GetPackedColor(cls >> "colorText");
	_boldColor = GetPackedColor(cls >> "colorBold");
	_linkColor = GetPackedColor(cls >> "colorLink");
	_activeLinkColor = GetPackedColor(cls >> "colorLinkActive");

	_fontH1 = GLOB_ENGINE->LoadFont(GetFontID(cls >> "H1" >> "font"));
	_fontH1Bold = GLOB_ENGINE->LoadFont(GetFontID(cls >> "H1" >> "fontBold"));
	const ParamEntry *entry = (cls >> "H1").FindEntry("size");
	if (entry) _sizeH1 = (float)(*entry) * _fontH1->Height();
	else _sizeH1 = cls >> "H1" >> "sizeEx";
	_fontH2 = GLOB_ENGINE->LoadFont(GetFontID(cls >> "H2" >> "font"));
	_fontH2Bold = GLOB_ENGINE->LoadFont(GetFontID(cls >> "H2" >> "fontBold"));
	entry = (cls >> "H2").FindEntry("size");
	if (entry) _sizeH2 = (float)(*entry) * _fontH2->Height();
	else _sizeH2 = cls >> "H2" >> "sizeEx";
	_fontH3 = GLOB_ENGINE->LoadFont(GetFontID(cls >> "H3" >> "font"));
	_fontH3Bold = GLOB_ENGINE->LoadFont(GetFontID(cls >> "H3" >> "fontBold"));
	entry = (cls >> "H3").FindEntry("size");
	if (entry) _sizeH3 = (float)(*entry) * _fontH3->Height();
	else _sizeH3 = cls >> "H3" >> "sizeEx";
	_fontH4 = GLOB_ENGINE->LoadFont(GetFontID(cls >> "H4" >> "font"));
	_fontH4Bold = GLOB_ENGINE->LoadFont(GetFontID(cls >> "H4" >> "fontBold"));
	entry = (cls >> "H4").FindEntry("size");
	if (entry) _sizeH4 = (float)(*entry) * _fontH4->Height();
	else _sizeH4 = cls >> "H4" >> "sizeEx";
	_fontH5 = GLOB_ENGINE->LoadFont(GetFontID(cls >> "H5" >> "font"));
	_fontH5Bold = GLOB_ENGINE->LoadFont(GetFontID(cls >> "H5" >> "fontBold"));
	entry = (cls >> "H5").FindEntry("size");
	if (entry) _sizeH5 = (float)(*entry) * _fontH5->Height();
	else _sizeH5 = cls >> "H5" >> "sizeEx";
	_fontH6 = GLOB_ENGINE->LoadFont(GetFontID(cls >> "H6" >> "font"));
	_fontH6Bold = GLOB_ENGINE->LoadFont(GetFontID(cls >> "H6" >> "fontBold"));
	entry = (cls >> "H6").FindEntry("size");
	if (entry) _sizeH6 = (float)(*entry) * _fontH6->Height();
	else _sizeH6 = cls >> "H6" >> "sizeEx";
	_fontP = GLOB_ENGINE->LoadFont(GetFontID(cls >> "P" >> "font"));
	_fontPBold = GLOB_ENGINE->LoadFont(GetFontID(cls >> "P" >> "fontBold"));
	entry = (cls >> "P").FindEntry("size");
	if (entry) _sizeP = (float)(*entry) * _fontP->Height();
	else _sizeP = cls >> "P" >> "sizeEx";

	Init();
	_filename = cls >> "filename";

	_indent = 0;
/*
	RString filename = cls>>"filename";
	if (filename.GetLength() > 0)
		Load(filename);
*/
}

int CHTMLContainer::FindSection(const char *name)
{
	for (int s=0; s<_sections.Size(); s++)
	{
		HTMLSection &to = _sections[s];
		for (int n=0; n<to.names.Size(); n++)
		{
			if (stricmp(name, to.names[n]) == 0)
				return s;
		}
	}
	return -1;
}

void CHTMLContainer::SwitchSection(const char *name)
{
	int s = FindSection(name);
	if (s >= 0)
	{
		_currentSection = s;
		float mouseX = 0.5 + GInput.cursorX * 0.5;
		float mouseY = 0.5 + GInput.cursorY * 0.5;
		_activeField = FindField(mouseX, mouseY);
	}
}

int CHTMLContainer::ActiveBookmark()
{
	if (_sections.Size() <= 0) return -1;
	HTMLSection &section = _sections[_currentSection];
	int n = section.names.Size();
	int m = _bookmarks.Size();
	for (int i=0; i<n; i++)
	{
		RString name = section.names[i];
		for (int j=0; j<m; j++)
		{
			if (stricmp(name, _bookmarks[j]) == 0)
				return j;
		}
	}
	return -1;
}

const HTMLField *CHTMLContainer::GetActiveField() const
{
	if (_activeField < 0) return NULL;
	if (_sections.Size() <= 0) return NULL;
	const HTMLSection &section = _sections[_currentSection];
	return &section.fields[_activeField];
}
	
void CHTMLContainer::Init()
{
	_sections.Clear();
	_currentSection = 0;
	_activeField = -1;
}

void CHTMLContainer::InitSection(int section)
{
	_sections[section].fields.Clear();
	_sections[section].rows.Clear();
	_sections[section].names.Clear();
}

int CHTMLContainer::AddSection()
{
	return _sections.Add();
}

void CHTMLContainer::AddBreak(int section, bool bottom)
{
	if (section < 0 || section >= _sections.Size()) return;
	
	HTMLSection &sec = _sections[section];
	int i = sec.fields.Add();
	HTMLField &fld = sec.fields[i];

//	fld.first = 0;
	fld.nextline = true;
	fld.exclude = false;
	fld.text = "";
	fld.format = HFP;
	fld.href = "";
	fld.bottom = bottom;
	fld.indent = _indent;
	fld.tableWidth = 0;
}

void CHTMLContainer::AddText(int section, RString text, HTMLFormat format, HTMLAlign align, bool bottom, bool bold, RString href, float tableWidth)
{
	if (section < 0 || section >= _sections.Size()) return;
	
	HTMLSection &sec = _sections[section];
	int i = sec.fields.Add();
	HTMLField &fld = sec.fields[i];

//	fld.first = 0;
	fld.nextline = false;
	fld.exclude = false;
	fld.text = text;
	fld.format = format;
	fld.align = align;
	fld.bold = bold;
	fld.href = href;
	fld.bottom = bottom;
	fld.indent = _indent;
	fld.tableWidth = tableWidth;
}

HTMLField *CHTMLContainer::AddImage(int section, RString image, HTMLAlign align, bool bottom, float w, float h, RString href, RString text, float tableWidth)
{
	if (section < 0 || section >= _sections.Size()) return NULL;
	
	HTMLSection &sec = _sections[section];
	int i = sec.fields.Add();
	HTMLField &fld = sec.fields[i];

	fld.format = HFImg;
	fld.align = align;
	fld.nextline = false;
	fld.exclude = false;
	fld.text = text;
	fld.href = href;
	fld.bottom = bottom;
	fld.indent = _indent;
	fld.tableWidth = tableWidth;
	
	char buffer[256];
	const char *q = strchr(image, '?');
	if (q)
	{
		fld.condition = image.Substring(0, q - image);
		RString image1 = q + 1;
		const char *p = strchr(image1, ':');
		if (p)
		{
			RString image2 = p + 1;
			image1 = image1.Substring(0, p - image1);

			if (image2[0] == '\\')
			{
				image2 = image2.Substring(1, INT_MAX);
			}
			else
			{
				// find in current directory
				strcpy(buffer, _filename);
				char *ext = strrchr(buffer, '\\');
				if (ext) *(++ext) = 0;
				strcat(buffer, image2);
				if (QIFStreamB::FileExist(buffer))
					image2 = buffer;
				else
					image2 = FindPicture(image2);
			}
			image2.Lower();
			I_AM_ALIVE();
			fld.texture2 = GlobLoadTexture(image2);
			I_AM_ALIVE();
		}
		if (image1[0] == '\\')
		{
			image1 = image1.Substring(1, INT_MAX);
		}
		else
		{
			// find in current directory
			strcpy(buffer, _filename);
			char *ext = strrchr(buffer, '\\');
			if (ext) *(++ext) = 0;
			strcat(buffer, image1);
			if (QIFStreamB::FileExist(buffer))
				image1 = buffer;
			else
				image1 = FindPicture(image1);
		}
		image1.Lower();
		I_AM_ALIVE();
		fld.texture1 = GlobLoadTexture(image1);
		I_AM_ALIVE();
		if (!fld.texture1) fld.texture1 = fld.texture2;
	}
	else
	{	
		if (image[0] == '\\')
		{
			image = image.Substring(1, INT_MAX);
		}
		else
		{
			// find in current directory
			strcpy(buffer, _filename);
			char *ext = strrchr(buffer, '\\');
			if (ext) *(++ext) = 0;
			strcat(buffer, image);
			if (QIFStreamB::FileExist(buffer))
				image = buffer;
			else
				image = FindPicture(image);
		}
		image.Lower();
		I_AM_ALIVE();
		if (image.GetLength() > 0)
			fld.texture1 = GlobLoadTexture(image);
		else
			fld.texture1 = NULL;
		fld.texture2 = fld.texture1;
		I_AM_ALIVE();
	}
	if (fld.texture1)
	{
		fld.texture1->SetMaxSize(1024); // no limits
		if (w < 0)
		{
			MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(fld.texture1, 0, 0);
			if (h < 0)
			{
				w = fld.texture1->AWidth();
				h = fld.texture1->AHeight();
			}
			else
			{
				w = fld.texture1->AWidth() * h / fld.texture1->AHeight();
			}
		}
		else if (h < 0)
		{
			h = fld.texture1->AHeight() * w / fld.texture1->AWidth();
		}

		fld.width = w * (1.0 / 640.0);
		fld.height = h * (1.0 / 480.0);
	}
	else
	{
		fld.width = (w > 0 ? w : h) * (1.0 / 640.0);
		fld.height = (h > 0 ? h : w) * (1.0 / 480.0);
		saturateMax(fld.width, 0);
		saturateMax(fld.height, 0);
	}
	return &fld;
}

void CHTMLContainer::AddName(int section, RString name)
{
	if (section < 0 || section >= _sections.Size()) return;
	
	HTMLSection &sec = _sections[section];
	sec.names.Add(name);
}

void CHTMLContainer::AddBookmark(RString link)
{
	_bookmarks.Add(link);
}

static RString ReadTag(QIStream &in)
{
	char buf[256];
	int len = 0;
	int c = in.get();
	while (!in.eof() && !in.fail() && (isalnum(c) || c == '/'))
	{
		if (len < sizeof(buf)-1) buf[len++] = c;
		c = in.get();
	}
	in.unget();
	buf[len] = 0;
	return buf;
}

static void SkipTag(QIStream &in)
{
	int c = in.get();
	while (!in.eof() && !in.fail() && c != '>')
	{
		c = in.get();
	}
}

static void SkipSpaces(QIStream &in)
{
	int c = in.get();
	while (!in.eof() && !in.fail() && ISSPACE(c))
	{
		c = in.get();
	}
	in.unget();
}

static RString ReadPropertyName(QIStream &in)
{
	char buf[256];
	int len = 0;
	int c = in.get();
	while (!in.eof() && !in.fail() && !ISSPACE(c) && c != '=')
	{
		if (len < sizeof(buf)-1) buf[len++] = c;
		c = in.get();
	}
	in.unget();
	Assert(len<sizeof(buf));
	buf[len] = 0;
	return buf;
}

/*!
\patch 1.45 Date 2/18/2002 by Ondra
- Fixed: Briefing HTML error could cause application crash during mission startup
when malformed HTML was loaded in some custom missions.
*/

static RString ReadPropertyValue(QIStream &in)
{
	SkipSpaces(in);
	int c = in.get();
	if (c != '=')
	{
		in.unget();
		return "";
	}
	
	SkipSpaces(in);
	c = in.get();
	if (c != '"')
	{
		in.unget();
		return "";
	}

	char buf[256];
	int len = 0;
	c = in.get();
	while (!in.eof() && !in.fail() && c != '"')
	{
		if (len < sizeof(buf)-1) buf[len++] = c;
		c = in.get();
	}
	Assert(len<sizeof(buf));
	buf[len] = 0;
	return buf;
}

static char ReadChar(QIStream &in)
{
	char buf[2048];
	int len = 0;
	int c = in.get();
	while (!in.eof() && !in.fail() && c != ';')
	{
		if (len < sizeof(buf)-1) buf[len++] = c;
		c = in.get();
	}
	buf[len] = 0;
	if (buf[0] == '#') return atoi(buf + 1);
	else if (strcmp(buf, "amp") == 0) return '&';
	else if (strcmp(buf, "quot") == 0) return '\"';
	else if (strcmp(buf, "lt") == 0) return '<';
	else if (strcmp(buf, "gt") == 0) return '>';
//	else if (stricmp(buf, "nbsp") == 0) return ' ';
	else if (strcmp(buf,"nbsp") == 0) return (char)160;
	else if (strcmp(buf,"iexcl") == 0) return (char)161;
	else if (strcmp(buf,"cent") == 0) return (char)162;
	else if (strcmp(buf,"pound") == 0) return (char)163;
	else if (strcmp(buf,"curren") == 0) return (char)164;
	else if (strcmp(buf,"yen") == 0) return (char)165;
	else if (strcmp(buf,"brvbar") == 0) return (char)166;
	else if (strcmp(buf,"sect") == 0) return (char)167;
	else if (strcmp(buf,"uml") == 0) return (char)168;
	else if (strcmp(buf,"copy") == 0) return (char)169;
	else if (strcmp(buf,"ordf") == 0) return (char)170;
	else if (strcmp(buf,"laquo") == 0) return (char)171;
	else if (strcmp(buf,"not") == 0) return (char)172;
	else if (strcmp(buf,"shy") == 0) return (char)173;
	else if (strcmp(buf,"reg") == 0) return (char)174;
	else if (strcmp(buf,"macr") == 0) return (char)175;
	else if (strcmp(buf,"deg") == 0) return (char)176;
	else if (strcmp(buf,"plusmn") == 0) return (char)177;
	else if (strcmp(buf,"sup2") == 0) return (char)178;
	else if (strcmp(buf,"sup3") == 0) return (char)179;
	else if (strcmp(buf,"acute") == 0) return (char)180;
	else if (strcmp(buf,"micro") == 0) return (char)181;
	else if (strcmp(buf,"para") == 0) return (char)182;
	else if (strcmp(buf,"middot") == 0) return (char)183;
	else if (strcmp(buf,"cedil") == 0) return (char)184;
	else if (strcmp(buf,"sup1") == 0) return (char)185;
	else if (strcmp(buf,"ordm") == 0) return (char)186;
	else if (strcmp(buf,"raquo") == 0) return (char)187;
	else if (strcmp(buf,"frac14") == 0) return (char)188;
	else if (strcmp(buf,"frac12") == 0) return (char)189;
	else if (strcmp(buf,"frac34") == 0) return (char)190;
	else if (strcmp(buf,"iquest") == 0) return (char)191;
	else if (strcmp(buf,"Agrave") == 0) return (char)192;
	else if (strcmp(buf,"Aacute") == 0) return (char)193;
	else if (strcmp(buf,"Acirc") == 0) return (char)194;
	else if (strcmp(buf,"Atilde") == 0) return (char)195;
	else if (strcmp(buf,"Auml") == 0) return (char)196;
	else if (strcmp(buf,"Aring") == 0) return (char)197;
	else if (strcmp(buf,"AElig") == 0) return (char)198;
	else if (strcmp(buf,"Ccedil") == 0) return (char)199;
	else if (strcmp(buf,"Egrave") == 0) return (char)200;
	else if (strcmp(buf,"Eacute") == 0) return (char)201;
	else if (strcmp(buf,"Ecirc") == 0) return (char)202;
	else if (strcmp(buf,"Euml") == 0) return (char)203;
	else if (strcmp(buf,"Igrave") == 0) return (char)204;
	else if (strcmp(buf,"Iacute") == 0) return (char)205;
	else if (strcmp(buf,"Icirc") == 0) return (char)206;
	else if (strcmp(buf,"Iuml") == 0) return (char)207;
	else if (strcmp(buf,"ETH") == 0) return (char)208;
	else if (strcmp(buf,"Ntilde") == 0) return (char)209;
	else if (strcmp(buf,"Ograve") == 0) return (char)210;
	else if (strcmp(buf,"Oacute") == 0) return (char)211;
	else if (strcmp(buf,"Ocirc") == 0) return (char)212;
	else if (strcmp(buf,"Otilde") == 0) return (char)213;
	else if (strcmp(buf,"Ouml") == 0) return (char)214;
	else if (strcmp(buf,"times") == 0) return (char)215;
	else if (strcmp(buf,"Oslash") == 0) return (char)216;
	else if (strcmp(buf,"Ugrave") == 0) return (char)217;
	else if (strcmp(buf,"Uacute") == 0) return (char)218;
	else if (strcmp(buf,"Ucirc") == 0) return (char)219;
	else if (strcmp(buf,"Uuml") == 0) return (char)220;
	else if (strcmp(buf,"Yacute") == 0) return (char)221;
	else if (strcmp(buf,"THORN") == 0) return (char)222;
	else if (strcmp(buf,"szlig") == 0) return (char)223;
	else if (strcmp(buf,"agrave") == 0) return (char)224;
	else if (strcmp(buf,"aacute") == 0) return (char)225;
	else if (strcmp(buf,"acirc") == 0) return (char)226;
	else if (strcmp(buf,"atilde") == 0) return (char)227;
	else if (strcmp(buf,"auml") == 0) return (char)228;
	else if (strcmp(buf,"aring") == 0) return (char)229;
	else if (strcmp(buf,"aelig") == 0) return (char)230;
	else if (strcmp(buf,"ccedil") == 0) return (char)231;
	else if (strcmp(buf,"egrave") == 0) return (char)232;
	else if (strcmp(buf,"eacute") == 0) return (char)233;
	else if (strcmp(buf,"ecirc") == 0) return (char)234;
	else if (strcmp(buf,"euml") == 0) return (char)235;
	else if (strcmp(buf,"igrave") == 0) return (char)236;
	else if (strcmp(buf,"iacute") == 0) return (char)237;
	else if (strcmp(buf,"icirc") == 0) return (char)238;
	else if (strcmp(buf,"iuml") == 0) return (char)239;
	else if (strcmp(buf,"eth") == 0) return (char)240;
	else if (strcmp(buf,"ntilde") == 0) return (char)241;
	else if (strcmp(buf,"ograve") == 0) return (char)242;
	else if (strcmp(buf,"oacute") == 0) return (char)243;
	else if (strcmp(buf,"ocirc") == 0) return (char)244;
	else if (strcmp(buf,"otilde") == 0) return (char)245;
	else if (strcmp(buf,"ouml") == 0) return (char)246;
	else if (strcmp(buf,"divide") == 0) return (char)247;
	else if (strcmp(buf,"oslash") == 0) return (char)248;
	else if (strcmp(buf,"ugrave") == 0) return (char)249;
	else if (strcmp(buf,"uacute") == 0) return (char)250;
	else if (strcmp(buf,"ucirc") == 0) return (char)251;
	else if (strcmp(buf,"uuml") == 0) return (char)252;
	else if (strcmp(buf,"yacute") == 0) return (char)253;
	else if (strcmp(buf,"thorn") == 0) return (char)254;
	else if (strcmp(buf,"yuml") == 0) return (char)255;
	else return '?';
}

/*!
\patch 1.27 Date 10/17/2001 by Ondra
- Fixed: buffer overflow in HTML reading, could cause crash when starting mission.
*/

static RString ReadText(QIStream &in, int langID)
{
	char buf[4*1024];
	int len = 0;
	int c = in.get();
	while (!in.eof() && !in.fail() && c != '<')
	{
		if (c == 0x0a)
		{
			// avoid spaces at line end
			while (len > 0 && buf[len - 1] == ' ') len--;
			// avoid CR LF at begin of the text
			if (len == 0) goto ReadTextContinue;
		}
		if (c != 0x0d) // avoid CR LF -> 2 spaces (CRLF or LF are handled well)
		{
			if (ISSPACE(c)) c = ' ';
			else if (c == '&') c = ReadChar(in);

			if (c >= 0x80 && langID == Korean)
			{
				int c2 = in.get();
				if (len < sizeof(buf) - 2)
				{
					buf[len++] = c;
					buf[len++] = c2;
				}
			}
			else
			{
				if (len < sizeof(buf)-1) buf[len++] = c;
			}
		}
ReadTextContinue:
		c = in.get();
	}
	in.unget();
	buf[len] = 0;
	return buf;
}

static RString ConvertURL(RString src)
{
	char buf[2048];
	const char *ptr = src;

	int j = 0;
	while (*ptr)
	{
		if (strnicmp(ptr, "%20", 3) == 0)
		{
			buf[j++] = ' ';
			ptr += 3;
		}
		else
		{
			buf[j++] = *(ptr++);
		}
	}
	buf[j] = 0;
	return buf;
}

enum HTMLContext
{
	HTMLNone,
	HTMLHTML,
	HTMLHead,
	HTMLBody,
	HTMLP,
	HTMLH1,
	HTMLH2,
	HTMLH3,
	HTMLH4,
	HTMLH5,
	HTMLH6,
	HTMLA,
	HTMLB,
	HTMLAddr,
	HTMLTable,
	HTMLTR,
	HTMLTD,
};

struct HTMLStackItem
{
	HTMLContext context;
	HTMLAlign align;
	HTMLFormat format;
	bool bold;
	float width;

	HTMLStackItem(HTMLContext c, HTMLAlign a, HTMLFormat f, bool b = false, float w = 0)
	{
		context = c; align = a; format = f; bold = b; width = w;
	}
};
TypeIsSimple(HTMLStackItem);

class HTMLStack : public AutoArray<HTMLStackItem>
{
public:
	void Push(HTMLStackItem item) {Add(item);}
	void Push(HTMLContext c, HTMLAlign a, HTMLFormat f) {Add(HTMLStackItem(c, a, f));}
	HTMLStackItem Pop()
	{
		int n = Size() - 1;
		HTMLStackItem item = Get(n);
		Delete(n);
		return item;
	}
//	HTMLFormat GetFormat();
};

/*
HTMLFormat GetFormat(HTMLContext ctx)
{
	switch (ctx)
	{
		case HTMLP:
		case HTMLAddr:
			return HFP;
		case HTMLH1:
			return HFH1;
		case HTMLH2:
			return HFH2;
		case HTMLH3:
			return HFH3;
		case HTMLH4:
			return HFH4;
		case HTMLH5:
			return HFH5;
		case HTMLH6:
			return HFH6;
		default:
			return HFError;
	}
}

HTMLFormat HTMLStack::GetFormat()
{
	int n = Size() - 1;
	for (int i=n; i>=0; i--)
	{
		HTMLFormat f = ::GetFormat(Get(i).context);
		if (f != HFError) return f;
	}
	return HFError;
}
*/

void ReadParagraphProperties(QIStream &in, HTMLStackItem &item)
{
	SkipSpaces(in);
	char c = in.get();
	in.unget();
	while (c != '>')
	{
		RString name = ReadPropertyName(in);
		if (name.GetLength() == 0)
		{
			LogF("SYNTAX ERROR");
			break;
		}
		RString value = ReadPropertyValue(in);
		if (stricmp(name, "align") == 0)
		{
			if (stricmp(value, "left") == 0)
				item.align = HALeft;
			else if (stricmp(value, "center") == 0)
				item.align = HACenter;
			else if (stricmp(value, "right") == 0)
				item.align = HARight;
		}
		SkipSpaces(in);
		c = in.get();
		in.unget();
	}
}

/*!
\patch 1.44 Date 2/8/2002 by Jirka
- Added: Support for simple tables in html files 
*/

void ReadTableProperties(QIStream &in, HTMLStackItem &item, float pageWidth)
{
	SkipSpaces(in);
	char c = in.get();
	in.unget();
	while (c != '>')
	{
		RString name = ReadPropertyName(in);
		if (name.GetLength() == 0)
		{
			LogF("SYNTAX ERROR");
			break;
		}
		RString value = ReadPropertyValue(in);
		if (stricmp(name, "width") == 0)
		{
			float w = atoi(value);
			int len = value.GetLength();
			if (len > 0 && value[len - 1] == '%')
				item.width = 0.01 * w * pageWidth;
			else
				item.width = w * (1.0 / 640.0);
		}
		else if (stricmp(name, "align") == 0)
		{
			if (stricmp(value, "left") == 0)
				item.align = HALeft;
			else if (stricmp(value, "center") == 0)
				item.align = HACenter;
			else if (stricmp(value, "right") == 0)
				item.align = HARight;
		}
		else if (stricmp(name, "size") == 0)
		{
			switch (atoi(value))
			{
			case 0:
				item.format = HFP; break;
			case 1:
				item.format = HFH1; break;
			case 2:
				item.format = HFH2; break;
			case 3:
				item.format = HFH3; break;
			case 4:
				item.format = HFH4; break;
			case 5:
				item.format = HFH5; break;
			case 6:
				item.format = HFH6; break;
			default:
				Fail("Size");
				break;
			}
		}
		SkipSpaces(in);
		c = in.get();
		in.unget();
	}
}

static RString GetAlignment(HTMLAlign align)
{
	switch (align)
	{
		case HALeft:
			return "left";
		case HACenter:
			return "center";
		case HARight:
			return "right";
		default:
			return "unknown";
	}
}

void CHTMLContainer::Load(const char *filename, bool add)
{
	if (!add) Init();	// clear current content
	_filename = filename;

	if (!QIFStreamB::FileExist(filename))
	{
		// try to find alternate location
		RString GetMissionDirectory();
		RString fullname = GetMissionDirectory() + _filename;
		if (QIFStreamB::FileExist(fullname))
		{
			filename = _filename = fullname;
		}
		else
		{
			RString GetBaseDirectory();
			fullname = GetBaseDirectory() + _filename;
			if (QIFStreamB::FileExist(fullname))
			{
				filename = _filename = fullname;
			}
			else return;
		}
	}

	int section = -1;

	QIFStreamB in;
	in.AutoOpen(filename);
	
	HTMLStackItem item(HTMLNone, HALeft, HFP);
	HTMLStack stack;
	RString href = "";
	bool bottom = false;
	int c;
	while (true)
	{
		I_AM_ALIVE();

		c = in.get();
		if (in.eof() || in.fail()) break;
		
		if (c == '<')
		{
			// tag
			RString tag = ReadTag(in);
			switch (item.context)
			{
			case HTMLNone:
				if (stricmp(tag, "html") == 0)
				{
					stack.Push(item);
					item.context = HTMLHTML;
				}
				break;
			case HTMLHTML:
				if (stricmp(tag, "/html") == 0)
				{
					item = stack.Pop();
				}
				else if (stricmp(tag, "head") == 0)
				{
					stack.Push(item);
					item.context = HTMLHead;
				}
				else if (stricmp(tag, "body") == 0)
				{
					stack.Push(item);
					item.context = HTMLBody;
					if (section >= 0) FormatSection(section);
					section = AddSection();
					bottom = false;
				}
				break;
			case HTMLHead:
				if (stricmp(tag, "/head") == 0)
				{
					item = stack.Pop();
				}
				break;
			case HTMLBody:
				if (stricmp(tag, "/body") == 0)
				{
					item = stack.Pop();
				}
				else if (stricmp(tag, "p") == 0)
				{
					stack.Push(item);
					item.context = HTMLP;
					item.format = HFP;
					ReadParagraphProperties(in, item);
				} 
				else if (stricmp(tag, "address") == 0)
				{
					stack.Push(item);
					item.context = HTMLAddr;
					bottom = true;
					ReadParagraphProperties(in, item);
				} 
				else if (stricmp(tag, "h1") == 0)
				{
					stack.Push(item);
					item.context = HTMLH1;
					item.format = HFH1;
					ReadParagraphProperties(in, item);
				}
				else if (stricmp(tag, "h2") == 0)
				{
					stack.Push(item);
					item.context = HTMLH2;
					item.format = HFH2;
					ReadParagraphProperties(in, item);
				}
				else if (stricmp(tag, "h3") == 0)
				{
					stack.Push(item);
					item.context = HTMLH3;
					item.format = HFH3;
					ReadParagraphProperties(in, item);
				}
				else if (stricmp(tag, "h4") == 0)
				{
					stack.Push(item);
					item.context = HTMLH4;
					item.format = HFH4;
					ReadParagraphProperties(in, item);
				}
				else if (stricmp(tag, "h5") == 0)
				{
					stack.Push(item);
					item.context = HTMLH5;
					item.format = HFH5;
					ReadParagraphProperties(in, item);
				}
				else if (stricmp(tag, "h6") == 0)
				{
					stack.Push(item);
					item.context = HTMLH6;
					item.format = HFH6;
					ReadParagraphProperties(in, item);
				}
				else if (stricmp(tag, "table") == 0)
				{
					stack.Push(item);
					item.context = HTMLTable;
				}
				else if (stricmp(tag, "hr") == 0)
				{
					if (section >= 0) FormatSection(section);
					section = AddSection();
					bottom = false;
				}
				else goto General;
				break;
			case HTMLP:
				if (stricmp(tag, "/p") == 0)
				{
					item = stack.Pop();
					AddBreak(section, bottom);
				} 
				else goto Paragraph;
				break;
			case HTMLAddr:
				if (stricmp(tag, "/address") == 0)
				{
					item = stack.Pop();
					AddBreak(section, bottom);
				} 
				else goto Paragraph;
				break;
			case HTMLH1:
				if (stricmp(tag, "/h1") == 0)
				{
					item = stack.Pop();
					AddBreak(section, bottom);
				}
				else goto Paragraph;
				break;
			case HTMLH2:
				if (stricmp(tag, "/h2") == 0)
				{
					item = stack.Pop();
					AddBreak(section, bottom);
				}
				else goto Paragraph;
				break;
			case HTMLH3:
				if (stricmp(tag, "/h3") == 0)
				{
					item = stack.Pop();
					AddBreak(section, bottom);
				}
				else goto Paragraph;
				break;
			case HTMLH4:
				if (stricmp(tag, "/h4") == 0)
				{
					item = stack.Pop();
					AddBreak(section, bottom);
				}
				else goto Paragraph;
				break;
			case HTMLH5:
				if (stricmp(tag, "/h5") == 0)
				{
					item = stack.Pop();
					AddBreak(section, bottom);
				}
				else goto Paragraph;
				break;
			case HTMLH6:
				if (stricmp(tag, "/h6") == 0)
				{
					item = stack.Pop();
					AddBreak(section, bottom);
				}
				else goto Paragraph;
				break;
			case HTMLTable:
				if (stricmp(tag, "/table") == 0)
				{
					item = stack.Pop();
				}
				else if (stricmp(tag, "tr") == 0)
				{
					stack.Push(item);
					item.context = HTMLTR;
				}
				break;
			case HTMLTR:
				if (stricmp(tag, "/tr") == 0)
				{
					item = stack.Pop();
					AddBreak(section, bottom);
				}
				else if (stricmp(tag, "td") == 0)
				{
					stack.Push(item);
					item.context = HTMLTD;
					ReadTableProperties(in, item, GetPageWidth());
				}
				break;
			case HTMLTD:
				if (stricmp(tag, "/td") == 0)
				{
					item = stack.Pop();
				}
				else goto Paragraph;
				break;
			case HTMLA:
				if (stricmp(tag, "/a") == 0)
				{
					item = stack.Pop();
					href = "";
				}
				else goto General;
			case HTMLB:
				if (stricmp(tag, "/b") == 0)
				{
					item = stack.Pop();
				}
				else goto General;
				break;
			Paragraph:
				if (stricmp(tag, "a") == 0)
				{
					stack.Push(item);
					item.context = HTMLA;
					SkipSpaces(in);
					c = in.get();
					in.unget();
					while (c != '>')
					{
						RString name = ReadPropertyName(in);
						if (name.GetLength() == 0)
						{
							LogF("SYNTAX ERROR");
							break;
						}
						RString value = ReadPropertyValue(in);
						if (stricmp(name, "href") == 0)
						{
							href = ConvertURL(value);
						}
						else if (stricmp(name, "name") == 0)
						{
							AddName(section, value);
						}
						SkipSpaces(in);
						c = in.get();
						in.unget();
					}
				}
				else if (stricmp(tag, "b") == 0)
				{
					stack.Push(item);
					item.bold = true;
					item.context = HTMLB;
				}
				else goto General;
				break;
			General:
				if (stricmp(tag, "br") == 0)
				{
					AddBreak(section, bottom);
				}
				else if (stricmp(tag, "img") == 0)
				{
					RString image = "";
					float w = -1.0;
					float h = -1.0;

					SkipSpaces(in);
					c = in.get();
					in.unget();
					while (c != '>')
					{
						RString name = ReadPropertyName(in);
						if (name.GetLength() == 0)
						{
							LogF("SYNTAX ERROR");
							break;
						}
						RString value = ReadPropertyValue(in);
						if (stricmp(name, "src") == 0)
						{
							image = value;
						}
						else if (stricmp(name, "width") == 0)
						{
							w = atoi(value);
						}
						else if (stricmp(name, "height") == 0)
						{
							h = atoi(value);
						}
						SkipSpaces(in);
						c = in.get();
						in.unget();
					}
					AddImage(section, image, item.align, bottom, w, h, href, RString(),  item.width);
				}
				break;
			}
			SkipTag(in);
		}
		else
		{
			// text
			in.unget();
			RString text = ReadText(in, _fontP->GetLangID());
			switch (item.context)
			{
			case HTMLNone:
			case HTMLHTML:
			case HTMLHead:
			case HTMLBody:
				// ignore text
				break;
			case HTMLP:
			case HTMLAddr:
			case HTMLH1:
			case HTMLH2:
			case HTMLH3:
			case HTMLH4:
			case HTMLH5:
			case HTMLH6:
			case HTMLTD:
			case HTMLA:
			case HTMLB:
				AddText(section, text, item.format, item.align, bottom, item.bold, href, item.width);
				break;
			}
		}
	}
	if (section >= 0) FormatSection(section);

	if (item.context != HTMLNone)
	{
		RptF("Error in HTML file %s", filename);
		RptF("Context %d",item.context);
	}
	DoAssert(item.context == HTMLNone);
	DoAssert(stack.Size() == 0);

	for (int i=0; i<_sections.Size(); i++)
	{
		_sections[i].fields.Compact();
		_sections[i].names.Compact();
		_sections[i].rows.Compact();
	}
	_sections.Compact();
	_bookmarks.Compact();

}

void CHTMLContainer::RemoveSection(int s)
{
	_sections.Delete(s);
}

void CHTMLContainer::CopySection(int from, int to)
{
	if (from < 0 || from >= _sections.Size()) return;
	if (to < 0 || to >= _sections.Size()) return;

	HTMLSection &src = _sections[from];
	HTMLSection &dest = _sections[to];

	for (int i=0; i<src.fields.Size(); i++)
	{
		HTMLField &fld = src.fields[i]; 
		int index = dest.fields.Add(fld);
		dest.fields[index].indent += _indent;
	}
}

void CHTMLContainer::FormatSection(int s)
{
	float maxLineWidth = GetPageWidth();
	float minHeight = _sizeP;

	HTMLSection &section = _sections[s];
	section.rows.Clear();

	int r = section.rows.Add();
	HTMLRow *row = &section.rows[r];
	row->firstField = 0;
	row->firstPos = 0;
	row->width = 0;
	row->align = HALeft;
	row->height = minHeight;
	
	HTMLFormat format = HFP;
	bool bold = false;
	Font *font = _fontP;
	float size = _sizeP;

	bool bottom = false;

	int n = section.fields.Size();
	for (int f=0; f<n; f++)
	{
		HTMLField &field = section.fields[f];
		float lineWidth = maxLineWidth - field.indent;
		if (field.bottom) bottom = true;
		if (field.nextline)
		{
			row->lastField = f + 1;
			row->lastPos = 0;
			row->bottom = bottom;
			r = section.rows.Add();
			row = &section.rows[r];
			row->firstField = f + 1;
			row->firstPos = 0;
			row->width = 0;
			row->align = HALeft;
			row->height = minHeight;
		}
		else if (field.format == HFImg)
		{
			if (row->width > 0 && row->width + field.width > lineWidth)
			{
				row->lastField = f;
				row->lastPos = 0;
				row->bottom = bottom;
				r = section.rows.Add();
				row = &section.rows[r];
				row->firstField = f;
				row->firstPos = 0;
				row->width = 0;
				row->align = HALeft;
				row->height = minHeight;
			}
			float height = field.height;
			if (field.text.GetLength() > 0)
			{
				height += _sizeP;
			}
			saturateMax(row->height, height);
			row->width += field.width;
			row->align = field.align;
			if (field.exclude)
			{
				row->lastField = f + 1;
				row->lastPos = 0;
				row->bottom = bottom;
				row->height = 0;
				r = section.rows.Add();
				row = &section.rows[r];
				row->firstField = f + 1;
				row->firstPos = 0;
				row->width = 0;
				row->align = HALeft;
				row->height = minHeight;
			}
		}
		else
		{
			if (field.format != format || field.bold != bold)
			{
				format = field.format;
				bold = field.bold;
				switch (field.format)
				{
				case HFP:
					if (bold)
						font = _fontPBold;
					else
						font = _fontP;
					size = _sizeP;
					break;
				case HFH1:
					if (bold)
						font = _fontH1Bold;
					else
						font = _fontH1;
					size = _sizeH1;
					break;
				case HFH2:
					if (bold)
						font = _fontH2Bold;
					else
						font = _fontH2;
					size = _sizeH2;
					break;
				case HFH3:
					if (bold)
						font = _fontH3Bold;
					else
						font = _fontH3;
					size = _sizeH3;
					break;
				case HFH4:
					if (bold)
						font = _fontH4Bold;
					else
						font = _fontH4;
					size = _sizeH4;
					break;
				case HFH5:
					if (bold)
						font = _fontH5Bold;
					else
						font = _fontH5;
					size = _sizeH5;
					break;
				case HFH6:
					if (bold)
						font = _fontH6Bold;
					else
						font = _fontH6;
					size = _sizeH6;
					break;
				default:
					Fail("Format");
					break;
				}
			}
			saturateMax(row->height, size);

			if (field.tableWidth > 0)
			{
				row->width += field.tableWidth;
			}
			else
			{
				float curW = row->width;
				float wordW = curW;

				int wordI = -1;
				int n = field.text.GetLength();
				for (int i=0; i<n; i++)
				{
					int j = i;
					char c = field.text[i];
					Assert(c != 0);
					if (ISSPACE(c))
					{
						wordI = i;
						wordW = curW;
					}
					float cW;
					if ((c & 0x80) && font->GetLangID() == Korean && i + 1 < n)
					{
						// TODO: simplify
						char c2 = field.text[++i];
						char temp[3]; temp[0] = c; temp[1] = c2; temp[2] = 0;
						cW = GetTextWidth(size, font, temp);
					}
					else
					{
						// TODO: simplify
						char temp[2]; temp[0] = c; temp[1] = 0;
						cW = GetTextWidth(size, font, temp);
					}

					if (curW + cW > lineWidth)
					{
						if (wordW > 0)
						{
							row->width = wordW;
							i = wordI;
						}
						else
						{
							row->width = curW;
							i = j - 1;
						}
						row->align = field.align;
						row->lastField = f;
						row->lastPos = i + 1;
						row->bottom = bottom;
						r = section.rows.Add();
						row = &section.rows[r];
						row->firstField = f;
						row->firstPos = i + 1;
						row->width = 0;
						row->height = floatMax(minHeight, size);
						row->align = HALeft;

						curW = 0;
						wordW = 0;
						wordI = i + 1;
					}
					else
						curW += cW;
				}
				row->width = curW;
				row->align = field.align;
			}
		}
	}
	row->lastField = section.fields.Size();
	row->lastPos = 0;
	row->bottom = bottom;

	// delete obsolete rows
	for (r=section.rows.Size()-1; r>=0; r--)
	{
		if (section.rows[r].firstField >= n)
			section.rows.Delete(r);
	}

	SplitSection(s);
#if 0
	// display formated HTML code

	LogF("\nSection %d", s);
	for (int i=0; i<section.fields.Size(); i++)
	{
		HTMLField &field = section.fields[i];
		const char *format;
		switch (field.format)
		{
		case HFError: format = "ERROR"; break;
		case HFP: format = "P"; break;
		case HFH1: format = "H1"; break;
		case HFH2: format = "H2"; break;
		case HFH3: format = "H3"; break;
		case HFH4: format = "H4"; break;
		case HFH5: format = "H5"; break;
		case HFH6: format = "H6"; break;
		case HFImg: format = "Img"; break;
		default: format = "Unknown"; break;
		}
		if (field.nextline) format = "Next Line";
		LogF
		(
			"Field %d (%s): text=%s, width=%.2f, height=%.2f, %s %s, tableWidth = %.2f",
			i, format, (const char *)field.text, field.width, field.height,
			field.bottom ? "bottom" : "top", GetAlignment(field.align), field.tableWidth
		);
	}

	for (int i=0; i<section.rows.Size(); i++)
	{
		HTMLRow &row = section.rows[i];
		LogF
		(
			"Row %d: from [%d, %d] to [%d, %d], width=%.2f, height=%.2f, %s",
			i, row.firstField, row.firstPos,
			row.lastField, row.lastPos,
			row.width, row.height, row.bottom ? "bottom" : "top"
		);
	}
#endif
}

void CHTMLContainer::SplitSection(int s)
{
	HTMLSection source = _sections[s];
	if (source.names.Size() < 1) return;

	// check if section doesn't fit on one page
	float pageHeight = GetPageHeight();
	int n = source.rows.Size();
	float totalHeight = 0;
	for (int i=0; i<n; i++)
	{
		HTMLRow &row = source.rows[i];
		totalHeight += row.height;
	}
	if (totalHeight <= pageHeight) return;


	RString name = source.names[0];
	char buffer[256];

	RemoveSection(s);

	float rowHeight = _sizeP;
	float imgRowHeight = 1.5f * rowHeight;
	float imgHeight = 480.0f * imgRowHeight;
	pageHeight -= 2.0f * rowHeight + imgRowHeight;
	
	float curHeight = 0;
	int page = 0;

	s = _sections.Add();
	HTMLSection *dest = &_sections[s];
	dest->names.Add(name);
	sprintf(buffer, "%s/%d", (const char *)name, page);
	dest->names.Add(buffer);

	int firstField = 0;
	for (int i=0; i<n; i++)
	{
		HTMLRow &row = source.rows[i];
		curHeight += row.height;
		// keep line with height == 0 with the next page
		float checkHeight = curHeight;
		if (row.height == 0 && i + 1 < n)
		{
			checkHeight += source.rows[i + 1].height;
		}
		// next page?
		if (checkHeight > pageHeight)
		{
			// complete old page
			int lastField = source.rows[i - 1].lastField;
			for (int j=firstField; j<=lastField; j++)
			{
				if (j >= source.fields.Size()) break;
				dest->fields.Add(source.fields[j]);
			}
			// add link to prev / next
			int f1 = dest->fields.Size();
			{
				// empty line
				AddBreak(s, true);
				int r = dest->rows.Add();
				HTMLRow &rowDest = dest->rows[r];
				rowDest.firstField = f1;
				rowDest.firstPos = 0;
				f1 = dest->fields.Size();
				rowDest.lastField = f1;
				rowDest.lastPos = 0;
				rowDest.height = rowHeight;
				rowDest.width = 0;
				rowDest.bottom = true;
			}
			{
				// empty line
				AddBreak(s, true);
				int r = dest->rows.Add();
				HTMLRow &rowDest = dest->rows[r];
				rowDest.firstField = f1;
				rowDest.firstPos = 0;
				f1 = dest->fields.Size();
				rowDest.lastField = f1;
				rowDest.lastPos = 0;
				rowDest.height = rowHeight;
				rowDest.width = 0;
				rowDest.bottom = true;
			}
			if (page > 0)
			{
				// ref to previous page
				sprintf(buffer, "#%s/%d", (const char *)name, page - 1);
				AddImage(s, "sipka_left.paa", HARight, true, -1.0, imgHeight, buffer);
			}
			sprintf(buffer, "#%s/%d", (const char *)name, page + 1);
			AddImage(s, "sipka_right.paa", HARight, true, -1.0, imgHeight, buffer);
			AddBreak(s, true);
			int r = dest->rows.Add();
			HTMLRow &rowDest = dest->rows[r];
			rowDest.firstField = f1;
			rowDest.firstPos = 0;
			f1 = dest->fields.Size();
			rowDest.lastField = f1;
			rowDest.lastPos = 0;
			rowDest.height = imgRowHeight;
			rowDest.width = 0;
			rowDest.bottom = true;
			for (int f=rowDest.firstField; f<rowDest.lastField; f++)
/*
				rowDest.width += GEngine->GetTextWidth
				(
					_sizeP, _fontP, dest->fields[f].text
				);
*/
				rowDest.width += dest->fields[f].width;
			rowDest.align = HARight;
			// new section
			curHeight = row.height;
			page++;
			s = _sections.Add();
			dest = &_sections[s];
			dest->names.Add(name);
			sprintf(buffer, "%s/%d", (const char *)name, page);
			dest->names.Add(buffer);
		}
		int r = dest->rows.Add(row);
		dest->rows[r].firstField -= firstField;
		dest->rows[r].lastField -= firstField;
	}
	// complete last page
	int lastField = source.rows[n - 1].lastField;
	for (int j=firstField; j<=lastField; j++)
	{
		if (j >= source.fields.Size()) break;
		dest->fields.Add(source.fields[j]);
	}
	// add link to prev
	if (page > 0)
	{
		int f1 = dest->fields.Size();
		{
			// empty line
			AddBreak(s, true);
			int r = dest->rows.Add();
			HTMLRow &rowDest = dest->rows[r];
			rowDest.firstField = f1;
			rowDest.firstPos = 0;
			f1 = dest->fields.Size();
			rowDest.lastField = f1;
			rowDest.lastPos = 0;
			rowDest.height = rowHeight;
			rowDest.width = 0;
			rowDest.bottom = true;
		}
		{
			// empty line
			AddBreak(s, true);
			int r = dest->rows.Add();
			HTMLRow &rowDest = dest->rows[r];
			rowDest.firstField = f1;
			rowDest.firstPos = 0;
			f1 = dest->fields.Size();
			rowDest.lastField = f1;
			rowDest.lastPos = 0;
			rowDest.height = rowHeight;
			rowDest.width = 0;
			rowDest.bottom = true;
		}
		sprintf(buffer, "#%s/%d", (const char *)name, page - 1);
		AddImage(s, "sipka_left.paa", HARight, true, -1.0, imgHeight, buffer);
		AddBreak(s, true);
		int r = dest->rows.Add();
		HTMLRow &rowDest = dest->rows[r];
		rowDest.firstField = f1;
		rowDest.firstPos = 0;
		f1 = dest->fields.Size();
		rowDest.lastField = f1;
		rowDest.lastPos = 0;
		rowDest.height = imgRowHeight;
		rowDest.width = 0;
		rowDest.bottom = true;
		for (int f=rowDest.firstField; f<rowDest.lastField; f++)
/*
			rowDest.width += GEngine->GetTextWidth
			(
				_sizeP, _fontP, dest->fields[f].text
			);
*/
			rowDest.width += dest->fields[f].width;
		rowDest.align = HARight;
	}
}

int CHTMLContainer::FindField(float x, float y)
{
	if (_sections.Size() <= 0) return -1;
	HTMLSection &section = _sections[_currentSection];

	int r = -1;
	float top = 0;
	bool bottom = false;
	for (int i=0; i<section.rows.Size(); i++)
	{
		if (!bottom && section.rows[i].bottom)
		{
			bottom = true;
			top = GetPageHeight();
			for (int rr=i; rr<section.rows.Size(); rr++)
				top -= section.rows[rr].height;
			if (top > y) return -1;
		}
		top += section.rows[i].height;
		if (top > y)
		{
			r = i;
			break;
		}
	}
	if (r < 0) return -1;

	HTMLRow &row = section.rows[r];
	float left;
	switch (row.align)
	{
	case HARight:
		left = GetPageWidth() - row.width;
		break;
	case HACenter:
		left = 0.5 * (GetPageWidth() - row.width);
		break;
	default:
		Assert(row.align == HALeft);
		left = 0;
		break;
	}
	if (left > x) return -1;
	for (int f=row.firstField; f<=row.lastField; f++)
	{
		if (f >= section.fields.Size()) continue;
		HTMLField &field = section.fields[f];
		if (field.nextline) continue;
		int from = 0;
		if (f == row.firstField) from = row.firstPos;
		int to = INT_MAX;
		if (f == row.lastField) to = row.lastPos;
		if (from >= to) continue;

		if (field.tableWidth > 0)
			left += field.tableWidth;
		else if (field.format == HFImg)
			left += field.width;
		else
		{
			Font *font = _fontP;
			float size = _sizeP;
			switch (field.format)
			{
			case HFP:
				if (field.bold)
					font = _fontPBold;
				else
					font = _fontP;
				size = _sizeP;
				break;
			case HFH1:
				if (field.bold)
					font = _fontH1Bold;
				else
					font = _fontH1;
				size = _sizeH1;
				break;
			case HFH2:
				if (field.bold)
					font = _fontH2Bold;
				else
					font = _fontH2;
				size = _sizeH2;
				break;
			case HFH3:
				if (field.bold)
					font = _fontH3Bold;
				else
					font = _fontH3;
				size = _sizeH3;
				break;
			case HFH4:
				if (field.bold)
					font = _fontH4Bold;
				else
					font = _fontH4;
				size = _sizeH4;
				break;
			case HFH5:
				if (field.bold)
					font = _fontH5Bold;
				else
					font = _fontH5;
				size = _sizeH5;
				break;
			case HFH6:
				if (field.bold)
					font = _fontH6Bold;
				else
					font = _fontH6;
				size = _sizeH6;
				break;
			default:
				Fail("Format");
				break;
			}
			RString text = field.text.Substring(from, to);
			left += GetTextWidth(size, font, text);
		}
		if (left + field.indent > x) return f;
	}

	return -1;
}

///////////////////////////////////////////////////////////////////////////////
// class CHTML

CHTML::CHTML(ControlsContainer *parent, int idc, const ParamEntry &cls)
	: Control(parent, CT_HTML, idc, cls),
		CHTMLContainer(cls)
{
	if (_filename.GetLength() > 0)
		Load(_filename);
}

void CHTML::OnLButtonDown(float x, float y)
{
	if (!IsInside(x, y)) return;

	if (_activeField < 0) return;
	if (_sections.Size() <= 0) return;

	HTMLSection &section = _sections[_currentSection];
	HTMLField &field = section.fields[_activeField];

	const char *text = field.href;
	if (*text == 0) return;

	if (*text == '#')
	{
		SwitchSection(text + 1);
		return;
	}
	else
	{
		if (_parent) _parent->OnHTMLLink(IDC(), field.href);
	}
}

void CHTML::OnMouseMove(float x, float y, bool active)
{
	if (active)
		_activeField = FindField(x, y);
	else
		_activeField = -1;
}

#define DRAW_TABLE_BORDERS	0

void CHTML::OnDraw(float alpha)
{
	const int w = GLOB_ENGINE->Width2D();
	const int h = GLOB_ENGINE->Height2D();

	float xx = toInt(_x * w) + 0.5;
	float yy = toInt(_y * h) + 0.5;
	float ww = toInt(_w * w);
	float hh = toInt(_h * h);

	PackedColor pictureColor = PackedColor(Color(1, 1, 1, 0.6 * alpha));
	PackedColor pictureColorSelected = PackedColor(Color(1, 1, 1, alpha));
	PackedColor bgColor = ModAlpha(_bgColor, alpha);

	MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
	GLOB_ENGINE->Draw2D(mip, bgColor, Rect2DPixel(xx, yy, ww, hh));

	if (_sections.Size() <= 0) return;
	HTMLSection &section = _sections[_currentSection];

	float top = _y;
	bool bottom = false;

	for (int r=0; r<section.rows.Size(); r++)
	{
		HTMLRow &row = section.rows[r];
		if (!bottom && row.bottom)
		{
			bottom = true;
			top = _y + _h;
			for (int rr=r; rr<section.rows.Size(); rr++)
				top -= _scale * section.rows[rr].height;
		}
		float left;
		switch (row.align)
		{
		case HARight:
			left = _x + _w - _scale * (row.width + textBorder);
			break;
		case HACenter:
			left = _x + 0.5 * (_w - _scale * row.width);
			break;
		default:
			Assert(row.align == HALeft);
			left = _x + _scale * textBorder;
			break;
		}
		for (int f=row.firstField; f<=row.lastField; f++)
		{
			if (f >= section.fields.Size()) continue;
			HTMLField &field = section.fields[f];
			if (field.nextline) continue;
			int from = 0;
			if (f == row.firstField) from = row.firstPos;
			int to = INT_MAX;
			if (f == row.lastField) to = row.lastPos;
			if (from >= to) continue;

			if (field.format == HFImg)
			{
				float l = left;
				if (field.tableWidth > 0)
				{
					switch (field.align)
					{
					case HARight:
						l += _scale * (field.tableWidth - field.width);
						break;
					case HACenter:
						l += 0.5 * _scale * (field.tableWidth - field.width);
						break;
					}
				}
				if (field.text.GetLength() > 0)
				{
					float t;
					if (row.height > 0)
						t = top + _scale * (row.height - field.height - _sizeP);
					else
						t = top - _scale * _sizeP;
					float lText = l + 0.5 * _scale *
						(field.width + field.indent - GetTextWidth(_sizeP, _fontP, field.text));
					PackedColor color;
					if (field.href.GetLength() == 0)
						color = _textColor;
					else if (f == _activeField)
						color = _activeLinkColor;
					else
						color = _linkColor;
					GEngine->DrawText
					(
						Point2DFloat(lText, t), _scale * _sizeP,
						Rect2DFloat(_x, _y, _w, _h),
						_fontP, color, field.text
					);
				}
				Texture *texture = field.GetTexture();
				if (texture)
				{
					float t;
					if (row.height > 0)
						t = top + _scale * (row.height - field.height);
					else
						t = top;
					Draw2DPars pars;
					pars.mip = GEngine->TextBank()->UseMipmap(texture, 0, 0);
					pars.spec = NoZBuf|IsAlpha|ClampU|ClampV|IsAlphaFog;
					PackedColor color;
					if (field.href.GetLength() == 0)
						color = pictureColorSelected;
					else if (f == _activeField)
						color = pictureColorSelected;
					else
						color = pictureColor;
					pars.SetColor(color);
					pars.SetU(0,1);
					pars.SetV(0,1);
					Rect2DPixel rect((l + _scale * field.indent) * w, t * h, _scale * field.width * w, _scale * field.height * h);
					Rect2DPixel clip(_x * w, _y * h, _w * w, _h * h);
					GEngine->Draw2D(pars, rect, clip);
				}
				if (field.tableWidth > 0)
				{
#if DRAW_TABLE_BORDERS
					GEngine->DrawLine
					(
						CX(left), CY(top),
						CX(left + _scale * field.tableWidth), CY(top),
						PackedBlack, PackedBlack
					);
					GEngine->DrawLine
					(
						CX(left + _scale * field.tableWidth), CY(top),
						CX(left + _scale * field.tableWidth), CY(top + _scale * row.height),
						PackedBlack, PackedBlack
					);
					GEngine->DrawLine
					(
						CX(left + _scale * field.tableWidth), CY(top + _scale * row.height),
						CX(left), CY(top + _scale * row.height),
						PackedBlack, PackedBlack
					);
					GEngine->DrawLine
					(
						CX(left), CY(top + _scale * row.height),
						CX(left), CY(top),
						PackedBlack, PackedBlack
					);
#endif
					left += _scale * field.tableWidth;
				}
				else
					left += _scale * field.width;
			}
			else
			{
				Font *font = _fontP;
				float size = _scale * _sizeP;
				switch (field.format)
				{
				case HFP:
					if (field.bold)
						font = _fontPBold;
					else
						font = _fontP;
					size = _scale * _sizeP;
					break;
				case HFH1:
					if (field.bold)
						font = _fontH1Bold;
					else
						font = _fontH1;
					size = _scale * _sizeH1;
					break;
				case HFH2:
					if (field.bold)
						font = _fontH2Bold;
					else
						font = _fontH2;
					size = _scale * _sizeH2;
					break;
				case HFH3:
					if (field.bold)
						font = _fontH3Bold;
					else
						font = _fontH3;
					size = _scale * _sizeH3;
					break;
				case HFH4:
					if (field.bold)
						font = _fontH4Bold;
					else
						font = _fontH4;
					size = _scale * _sizeH4;
					break;
				case HFH5:
					if (field.bold)
						font = _fontH5Bold;
					else
						font = _fontH5;
					size = _scale * _sizeH5;
					break;
				case HFH6:
					if (field.bold)
						font = _fontH6Bold;
					else
						font = _fontH6;
					size = _scale * _sizeH6;
					break;
				default:
					Fail("Format");
					break;
				}
				PackedColor color;
				if (field.href.GetLength() == 0)
				{
					if (field.bold)
						color = _boldColor;
					else
						color = _textColor;
				}
				else if (f == _activeField)
					color = _activeLinkColor;
				else
					color = _linkColor;

				RString text = field.text.Substring(from, to);
				float l = left;
				if (field.tableWidth > 0)
				{
					switch (field.align)
					{
					case HARight:
						l += (_scale * field.tableWidth - GetTextWidth(size, font, text));
						break;
					case HACenter:
						l += 0.5 * (_scale * field.tableWidth - GetTextWidth(size, font, text));
						break;
					}
				}
				float t = top + _scale * row.height - size;
				GLOB_ENGINE->DrawText
				(
					Point2DFloat(l + _scale * field.indent, t), size,
					Rect2DFloat(_x, _y, _w, _h),
					font, color, text
				);
				if (field.tableWidth > 0)
				{
#if DRAW_TABLE_BORDERS
					GEngine->DrawLine
					(
						CX(left), CY(top),
						CX(left + _scale * field.tableWidth), CY(top),
						PackedBlack, PackedBlack
					);
					GEngine->DrawLine
					(
						CX(left + _scale * field.tableWidth), CY(top),
						CX(left + _scale * field.tableWidth), CY(top + _scale * row.height),
						PackedBlack, PackedBlack
					);
					GEngine->DrawLine
					(
						CX(left + _scale * field.tableWidth), CY(top + _scale * row.height),
						CX(left), CY(top + _scale * row.height),
						PackedBlack, PackedBlack
					);
					GEngine->DrawLine
					(
						CX(left), CY(top + _scale * row.height),
						CX(left), CY(top),
						PackedBlack, PackedBlack
					);
#endif
					left += _scale * field.tableWidth;
				}
				else
					left += GetTextWidth(size, font, text);
			}
		}
		top += _scale * row.height;
	}
}

int CHTML::FindField(float x, float y)
{
	if (!IsInside(x, y)) return -1;

	return CHTMLContainer::FindField
	(
		(x - _x) / _scale - textBorder,
		(y - _y) / _scale
	);
}

float CHTML::GetPageWidth() const
{
	return (_w - 2 * textBorder) / _scale;
}

float CHTML::GetPageHeight() const
{
	return _h / _scale;
}

float CHTML::GetTextWidth(float size, Font *font, const char *text) const
{
	return GEngine->GetTextWidth(size, font, text);
}


///////////////////////////////////////////////////////////////////////////////
// class C3DHTML

C3DHTML::C3DHTML(ControlsContainer *parent, int idc, const ParamEntry &cls)
	: Control3D(parent, CT_3DHTML, idc, cls),
		CHTMLContainer(cls)
{
	_width = cls >> "size";
	_invWidth = _width != 0 ? 1.0 / _width : 0;
	_height = -1;
	_invHeight = -1;
}

void C3DHTML::UpdateInfo(ControlObject *object, ControlInObject &info)
{
	Control3D::UpdateInfo(object, info);
	if (_height < 0)
	{
		// first UpdateInfo
		_height = _width * _down.Size() / _right.Size();
		_invHeight = _height != 0 ? 1.0 / _height : 0;
		// _width, _height must be initialized
		if (_filename.GetLength() > 0)
			Load(_filename);
	}
}

void C3DHTML::OnLButtonDown(float x, float y)
{
	if (!IsInside(x, y)) return;

	if (_activeField < 0) return;
	if (_sections.Size() <= 0) return;

	HTMLSection &section = _sections[_currentSection];
	HTMLField &field = section.fields[_activeField];

	const char *text = field.href;
	if (*text == 0) return;

	if (*text == '#')
	{
		SwitchSection(text + 1);
		return;
	}
	else
	{
		if (_parent) _parent->OnHTMLLink(IDC(), field.href);
	}
}

void C3DHTML::OnMouseMove(float x, float y, bool active)
{
	if (active)
		_activeField = FindField(x, y);
	else
		_activeField = -1;
}

void C3DHTML::OnDraw(float alpha)
{
	PackedColor bgColor = ModAlpha(_bgColor, alpha);
	GEngine->Draw3D(_position, _down, _right, ClipAll, bgColor, DisableSun, NULL);

	if (_sections.Size() <= 0) return;
	HTMLSection &section = _sections[_currentSection];

	Vector3 posTop = _position;
	Vector3 right = _invWidth * _right;
	Vector3 down = _invHeight * _down;

	PackedColor pictureColor = PackedColor(Color(1, 1, 1, 0.6 * alpha));
	PackedColor pictureColorSelected = PackedColor(Color(1, 1, 1, alpha));
	
	bool bottom = false;

	for (int r=0; r<section.rows.Size(); r++)
	{
		HTMLRow &row = section.rows[r];
		if (!bottom && row.bottom)
		{
			bottom = true;
			posTop = _position + _down;
			for (int rr=r; rr<section.rows.Size(); rr++)
				posTop -= section.rows[rr].height * down;
		}
		Vector3 posLeft;
		switch (row.align)
		{
		case HARight:
			posLeft = posTop + (_width - row.width) * right;
			break;
		case HACenter:
			posLeft = posTop + 0.5 * (_width - row.width) * right;
			break;
		default:
			Assert(row.align == HALeft);
			posLeft = posTop;
			break;
		}
		for (int f=row.firstField; f<=row.lastField; f++)
		{
			if (f >= section.fields.Size()) continue;
			HTMLField &field = section.fields[f];
			if (field.nextline) continue;
			int from = 0;
			if (f == row.firstField) from = row.firstPos;
			int to = INT_MAX;
			if (f == row.lastField) to = row.lastPos;
			if (from >= to) continue;

			if (field.format == HFImg)
			{
				Vector3 l = posLeft;
				if (field.tableWidth > 0)
				{
					switch (field.align)
					{
					case HARight:
						l += (field.tableWidth - field.width) * right;
						break;
					case HACenter:
						l += 0.5 * (field.tableWidth - field.width) * right;
						break;
					}
				}
				Texture *texture = field.GetTexture();
				if (texture)
				{
					PackedColor color;
					if (field.href.GetLength() == 0)
						color = pictureColorSelected;
					else if (f == _activeField)
						color = pictureColorSelected;
					else
						color = pictureColor;
					Vector3 p;
					if (row.height > 0)
						p = l + (row.height - field.height) * down + field.indent * right;
					else
						p = l;
					Vector3 r = (field.width) * right;
					Vector3 d = field.height * down;
					GEngine->Draw3D(p, d, r, ClipAll, color, DisableSun, texture);
				}
				if (field.tableWidth > 0)
				{
#if DRAW_TABLE_BORDERS
					Vector3 &tl = posLeft;
					Vector3 tr = posLeft + field.tableWidth * right;
					Vector3 bl = posLeft + row.height * down;
					Vector3 br = posLeft + row.height * down + field.tableWidth * right;
					GEngine->DrawLine3D (tl, tr, PackedBlack, DisableSun);
					GEngine->DrawLine3D (tr, br, PackedBlack, DisableSun);
					GEngine->DrawLine3D (br, bl, PackedBlack, DisableSun);
					GEngine->DrawLine3D (bl, tl, PackedBlack, DisableSun);
#endif
					posLeft += field.tableWidth * right;
				}
				else
					posLeft += field.width * right;
			}
			else
			{
				Font *font = _fontP;
				float size = _sizeP;
				switch (field.format)
				{
				case HFP:
					if (field.bold)
						font = _fontPBold;
					else
						font = _fontP;
					size = _sizeP;
					break;
				case HFH1:
					if (field.bold)
						font = _fontH1Bold;
					else
						font = _fontH1;
					size = _sizeH1;
					break;
				case HFH2:
					if (field.bold)
						font = _fontH2Bold;
					else
						font = _fontH2;
					size = _sizeH2;
					break;
				case HFH3:
					if (field.bold)
						font = _fontH3Bold;
					else
						font = _fontH3;
					size = _sizeH3;
					break;
				case HFH4:
					if (field.bold)
						font = _fontH4Bold;
					else
						font = _fontH4;
					size = _sizeH4;
					break;
				case HFH5:
					if (field.bold)
						font = _fontH5Bold;
					else
						font = _fontH5;
					size = _sizeH5;
					break;
				case HFH6:
					if (field.bold)
						font = _fontH6Bold;
					else
						font = _fontH6;
					size = _sizeH6;
					break;
				default:
					Fail("Format");
					break;
				}
				PackedColor color;
				if (field.href.GetLength() == 0)
				{
					if (field.bold)
						color = _boldColor;
					else
						color = _textColor;
				}
				else if (f == _activeField)
					color = _activeLinkColor;
				else
					color = _linkColor;

				RString text = field.text.Substring(from, to);
				Vector3 l = posLeft;
				if (field.tableWidth > 0)
				{
					switch (field.align)
					{
					case HARight:
						l += (field.tableWidth - GetTextWidth(size, font, text)) * right;
						break;
					case HACenter:
						l += 0.5 * (field.tableWidth - GetTextWidth(size, font, text)) * right;
						break;
					}
				}
				Vector3 p = l + (row.height - size) * down + field.indent * right;
				Vector3 u = -size * down;
				Vector3 r = 0.75 * u.Size() * right.Normalized();
				GEngine->DrawText3D(p, u, r, ClipAll, font, color, DisableSun, text);
				if (field.tableWidth > 0)
				{
#if DRAW_TABLE_BORDERS
					Vector3 &tl = posLeft;
					Vector3 tr = posLeft + field.tableWidth * right;
					Vector3 bl = posLeft + row.height * down;
					Vector3 br = posLeft + row.height * down + field.tableWidth * right;
					GEngine->DrawLine3D (tl, tr, PackedBlack, DisableSun);
					GEngine->DrawLine3D (tr, br, PackedBlack, DisableSun);
					GEngine->DrawLine3D (br, bl, PackedBlack, DisableSun);
					GEngine->DrawLine3D (bl, tl, PackedBlack, DisableSun);
#endif
					posLeft += field.tableWidth * right;
				}
				else
					posLeft += GetTextWidth(size, font, text) * right;
			}
		}
		posTop += row.height * down;
	}
}

int C3DHTML::FindField(float x, float y)
{
	if (!IsInside(x, y)) return -1;

	return CHTMLContainer::FindField
	(
		_u * _width, _v * _height
	);
}

float C3DHTML::GetTextWidth(float size, Font *font, const char *text) const
{
	Vector3 dir = 0.75 * size * _right.Normalized();
	return GEngine->GetText3DWidth(dir, font, text).Size();
}

///////////////////////////////////////////////////////////////////////////////
// class CTree

CTreeItem::CTreeItem(int l, CTreeItem *p)
{
	level = l;
	parent = p;

	text = "";
	data = "";
	value = 0;

	expanded = false;
}

CTreeItem *CTreeItem::AddChild()
{
	CTreeItem *item = new CTreeItem(level + 1, this);
	children.Add(item);
	return item;
}

CTreeItem *CTreeItem::InsertChild(int pos)
{
	saturate(pos, 0, children.Size());
	CTreeItem *item = new CTreeItem(level + 1, this);
	children.Insert(pos, item);
	return item;
}

int CompChildren(const Ref<CTreeItem> *pitem1, const Ref<CTreeItem> *pitem2)
{
	CTreeItem *item1 = *pitem1;
	CTreeItem *item2 = *pitem2;
	return stricmp(item1->text, item2->text);
}

int CompChildrenRev(const Ref<CTreeItem> *pitem1, const Ref<CTreeItem> *pitem2)
{
	CTreeItem *item1 = *pitem1;
	CTreeItem *item2 = *pitem2;
	return stricmp(item2->text, item1->text);
}

void CTreeItem::SortChildren(bool reversed)
{
	if (reversed)
		QSort(children.Data(), children.Size(), CompChildrenRev);
	else
		QSort(children.Data(), children.Size(), CompChildren);
}

void CTreeItem::ExpandTree(bool expand)
{
	expanded = expand;

	int n = children.Size();
	for (int i=0; i<n; i++)
		children[i]->ExpandTree(expand);
}

CTreeItem *CTreeItem::GetOBrother()
{
	if (!parent) return NULL;
	int n = parent->children.Size();
	for (int i=0; i<n-1; i++)
	{
		CTreeItem *item = parent->children[i];
		if (item == this)
			return parent->children[i + 1];
	}
	return NULL;
}

CTreeItem *CTreeItem::GetYBrother()
{
	if (!parent) return NULL;
	int n = parent->children.Size();
	CTreeItem *item = parent->children[0];
	if (item == this) return NULL;
	for (int i=1; i<n; i++)
	{
		item = parent->children[i];
		if (item == this)
			return parent->children[i - 1];
	}
	return NULL;
}

CTreeItem *CTreeItem::GetNextVisible()
{
	if (children.Size() > 0 && expanded)
		return children[0];

	CTreeItem *item = this;
	while (item->parent)
	{
		CTreeItem *brother = item->GetOBrother();
		if (brother) return brother;
		item = item->parent;
	}
	return NULL;
}

CTreeItem *CTreeItem::GetLastVisible()
{
	if (expanded)
	{
		int n = children.Size();
		if (n > 0) return children[n - 1]->GetLastVisible();
	}
	return this;
}

CTreeItem *CTreeItem::GetPrevVisible()
{
	if (!parent) return NULL;
	CTreeItem *brother = GetYBrother();
	if (brother) return brother->GetLastVisible();
	return parent;
}

int CTreeItem::NVisibleItems()
{
	int n = 0;
	CTreeItem *item = this;
	while (item)
	{
		n++;
		item = item->GetNextVisible();
	}
	return n;
}

CTree::CTree(ControlsContainer *parent, int idc, const ParamEntry &cls)
	: Control(parent, CT_TREE, idc, cls)
{
	_bgColor = GetPackedColor(cls >> "colorBackground");
	_ftColor = GetPackedColor(cls >> "colorText");
	_selColor = GetPackedColor(cls >> "colorSelect");
	_font = GLOB_ENGINE->LoadFont(GetFontID(cls>>"font"));
	const ParamEntry *entry = cls.FindEntry("size");
	if (entry) _size = (float)(*entry) * _font->Height();
	else _size = cls >> "sizeEx";

	CTreeItem *item = new CTreeItem(0, NULL);
	_root = item;
	if ((_style & TR_SHOWROOT) == 0) _root->expanded = true;
	_firstVisible = FindFirstVisible();
	_selected = _firstVisible;

	_scrollUp = false;
	_scrollDown = false;
	_scrollTime = Glob.uiTime;
	_offset = 0;
}

void CTree::SetSelected(CTreeItem *item)
{
	if ((_style & TR_SHOWROOT) == 0 && item == _root) return;

	_selected = item;
	_parent->OnTreeSelChanged(IDC());
	
	EnsureVisible(_selected);
}

void CTree::Expand(CTreeItem *item, bool expand)
{
	item->expanded = expand;

	if (expand && item == _selected && (_style & TR_AUTOCOLLAPSE))
	{
		CTreeItem *except = _selected;
		CTreeItem *parent = _selected->parent;
		while (parent)
		{
			int n = parent->children.Size();
			for (int i=0; i<n; i++)
			{
				if (parent->children[i] != except)
					parent->children[i]->ExpandTree(false);
			}

			except = parent;
			parent = except->parent;
		}
		EnsureVisible(_selected);
	}
}

void CTree::EnsureVisible(CTreeItem *item)
{
	while
	(
		_firstVisible && _firstVisible->parent &&
		!_firstVisible->parent->expanded
	)
		_firstVisible = _firstVisible->parent;
	
	CTreeItem *firstVisible = FindFirstVisible();
	if (!_firstVisible) _firstVisible = firstVisible;
	if (!_firstVisible) return;

	// special case
	if (item == _firstVisible)
	{
		_offset = 0;
		return;
	}

	int lineItem = -1;
	int lineFirst = -1;

	CTreeItem *p = firstVisible;
	int line = 0;
	while (p)
	{
		if (p == item)
		{
			lineItem = line;
			if (lineFirst >= 0) goto EVOK;
		}
		if (p == _firstVisible)
		{
			lineFirst = line;
			if (lineItem >= 0) goto EVOK;
		}
		p = p->GetNextVisible();
		line++;
	}
	return;

EVOK:
	if (lineItem < lineFirst)
	{
		_offset = 0;
		int n = lineFirst - lineItem;
		for (int i=0; i<n; i++)
		{
			CTreeItem *it = _firstVisible->GetPrevVisible();
			if (!it) break;
			_firstVisible = it;
		}
	}
	else
	{
		int n = lineItem - lineFirst;

		const float border = 0.005;
		float textHeight = _size;
		float h = _h + _offset * textHeight - 2 * border;
		if (_offset > 0 || _firstVisible != firstVisible)
			h -= textHeight;
		int nItems = _firstVisible->NVisibleItems();
		if (nItems * textHeight > h)
			h -= textHeight;
		if ((n + 1) * textHeight <= h) return;

		_offset = 0;		
		h = _h - 2 * border - textHeight;
		if (item->GetNextVisible()) h -= textHeight;
		int nn = toIntCeil(((n + 1) * textHeight - h) / textHeight);
		for (int i=0; i<nn; i++)
		{
			CTreeItem *it = _firstVisible->GetNextVisible();
			if (!it) break;
			_firstVisible = it;
		}
	}
}

void CTree::RemoveAll()
{
	_root->children.Clear();

	_root->text = "";
	_root->data = "";
	_root->value = 0;

	_root->expanded = (_style & TR_SHOWROOT) == 0;

	//CTreeItem *item = _root;
	_firstVisible = FindFirstVisible();
	_selected = _firstVisible;
}

void CTree::OnDraw(float alpha)
{
	const int w = GLOB_ENGINE->Width2D();
	const int h = GLOB_ENGINE->Height2D();

	float xx = CX(_x);
	float yy = CY(_y);
	float ww = toInt((_w+_x) * w)-xx;
	float hh = toInt((_h+_y) * h)-yy;

	PackedColor bgColor = ModAlpha(_bgColor, alpha);
	PackedColor ftColor = ModAlpha(_ftColor, alpha);
	PackedColor selColor = ModAlpha(_selColor, alpha);

	// draw list
	Texture *texture = GLOB_SCENE->Preloaded(TextureWhite);
	MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(texture, 0, 0);
	GLOB_ENGINE->Draw2D
	(
		mip, bgColor, Rect2DPixel(xx, yy, ww, hh)
	);
	DrawLeft(0, ftColor);
	DrawTop(0, ftColor);
	DrawBottom(0, ftColor);
	DrawRight(0, ftColor);

	CTreeItem *firstVisible = FindFirstVisible();
	if (!_firstVisible) _firstVisible = firstVisible;
	if (!_firstVisible) return;
	CTreeItem *item = _firstVisible;

	const float border = 0.005;
	
	float top = _y;
	float textHeight = _size;
	if (_offset > 0 || item != firstVisible)
	{
		float y0 = CY(_y + textHeight);
		GEngine->DrawLine(Line2DPixel(xx, y0, xx + ww, y0), ftColor, ftColor);

		float x0 = CX(_x + 0.5 * _w);
		float x1 = CX(_x + 0.5 * _w - 0.3 * textHeight);
		float x2 = CX(_x + 0.5 * _w + 0.3 * textHeight);
		float y1 = CY(_y + border);
		float y2 = CY(_y + textHeight - border);
		GEngine->DrawLine(Line2DPixel(x0, y1, x1, y2), ftColor, ftColor);
		GEngine->DrawLine(Line2DPixel(x1, y2, x2, y2), ftColor, ftColor);
		GEngine->DrawLine(Line2DPixel(x2, y2, x0, y1), ftColor, ftColor);
		
		top += textHeight;
	}
	top += border;
	float topClip = top;
	top -= _offset * textHeight;
	
	float bottomClip = _y + _h - border;
	int nItems = item->NVisibleItems();
	if (top + nItems * textHeight > bottomClip)
	{
		float y0 = CY(_y + _h - textHeight);
		GEngine->DrawLine(Line2DPixel(xx, y0, xx + ww, y0), ftColor, ftColor);

		float x0 = CX(_x + 0.5 * _w);
		float x1 = CX(_x + 0.5 * _w - 0.3 * textHeight);
		float x2 = CX(_x + 0.5 * _w + 0.3 * textHeight);
		float y1 = CY(_y + _h - border);
		float y2 = CY(_y + _h - textHeight + border);
		GEngine->DrawLine(Line2DPixel(x0, y1, x1, y2), ftColor, ftColor);
		GEngine->DrawLine(Line2DPixel(x1, y2, x2, y2), ftColor, ftColor);
		GEngine->DrawLine(Line2DPixel(x2, y2, x0, y1), ftColor, ftColor);

		bottomClip -= textHeight;
	}
	
	while (item && top < bottomClip)
	{
		DrawItem(alpha, item, top, topClip, bottomClip, item == firstVisible);
		top += textHeight;
		item = item->GetNextVisible();
	}
}

void CTree::DrawItem
(
	float alpha, CTreeItem *item,
	float top, float topClip, float bottomClip, bool first
)
{
	const int w = GLOB_ENGINE->Width2D();
	const int h = GLOB_ENGINE->Height2D();
	const float border = 0.005;

	Rect2DPixel clip(_x * w, topClip * h, _w * w, (bottomClip - topClip) * h);

	float textHeight = _size;
	float textWidth = 0.75 * textHeight;
	float left = _x + border + textWidth * item->level;

	PackedColor color = ModAlpha(_ftColor, alpha);
	float lineleft = left - textWidth;
	CTreeItem *cur = item;
	if (cur->parent)
	{
		if (cur->children.Size() == 0)
		{
			float x1 = CX(lineleft + 0.5 * textWidth);
			float x2 = CX(lineleft + textWidth + 0.5 * border);
			float y2 = CY(top + 0.5 * textHeight);
			if (!first)
			{
				float y1 = CY(top);
				GLOB_ENGINE->DrawLine
				(
					Line2DPixel(x1, y1, x1, y2), color, color, clip
				);
			}
			GLOB_ENGINE->DrawLine
			(
				Line2DPixel(x1, y2, x2, y2), color, color, clip
			);
			if (cur->GetOBrother())
			{
				float y3 = CY(top + textHeight);
				GLOB_ENGINE->DrawLine
				(
					Line2DPixel(x1, y2, x1, y3), color, color, clip
				);
			}
		}
		else
		{
			float x1 = CX(lineleft + 0.15 * textWidth);
			float x2 = CX(lineleft + 0.30 * textWidth);
			float x3 = CX(lineleft + 0.50 * textWidth);
			float x4 = CX(lineleft + 0.70 * textWidth);
			float x5 = CX(lineleft + 0.85 * textWidth);
			float x6 = CX(lineleft + 1.00 * textWidth + 0.5 * border);
			float y1 = CY(top + 0.15 * textHeight);
			float y3 = CY(top + 0.50 * textHeight);
			float y5 = CY(top + 0.85 * textHeight);
			GEngine->DrawLine(Line2DPixel(x1, y1, x1, y5), color, color, clip);
			GEngine->DrawLine(Line2DPixel(x1, y5, x5 + 1, y5), color, color, clip);
			GEngine->DrawLine(Line2DPixel(x5, y5, x5, y1), color, color, clip);
			GEngine->DrawLine(Line2DPixel(x5, y1, x1, y1), color, color, clip);
			GEngine->DrawLine(Line2DPixel(x2, y3, x4 + 1, y3), color, color, clip);
			if (!cur->expanded)
			{
				float y2 = CY(top + 0.3 * textHeight);
				float y4 = CY(top + 0.7 * textHeight);
				GEngine->DrawLine(Line2DPixel(x3, y2, x3, y4 + 1), color, color, clip);
			}
			if (!first)
			{
				float y0 = CY(top);
				GEngine->DrawLine(Line2DPixel(x3, y0, x3, y1), color, color, clip);
			}
			GEngine->DrawLine(Line2DPixel(x5, y3, x6, y3), color, color, clip);
			if (cur->GetOBrother())
			{
				float y6 = CY(top + textHeight);
				GEngine->DrawLine(Line2DPixel(x3, y5, x3, y6), color, color, clip);
			}
		}
		cur = cur->parent;
		lineleft -= textWidth;
		while (cur->parent)
		{
			if (cur->GetOBrother())
			{
				float x1 = CX(lineleft + 0.5 * textWidth);
				float y1 = CY(top);
				float y2 = CY(top + textHeight);
				GLOB_ENGINE->DrawLine
				(
					Line2DPixel(x1, y1, x1, y2), color, color, clip
				);
			}
			cur = cur->parent;
			lineleft -= textWidth;
		}
	}

	if (item == _selected)
	{
		float width = GEngine->GetTextWidth(_size, _font, item->text) + 2 * border;
		float height = floatMin(textHeight, bottomClip - topClip);

		float xx = toInt(left * w) + 0.5;
		float yy = toInt(top * h) + 0.5;
		float ww = toInt(width * w);
		float hh = toInt(height * h);

		color = ModAlpha(_selColor, alpha);
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel(xx, yy, xx + ww, yy), color, color, clip
		);
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel(xx + ww, yy, xx + ww, yy + hh + 1), color, color, clip
		);
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel(xx + ww, yy + hh, xx, yy + hh), color, color, clip
		);
		GLOB_ENGINE->DrawLine
		(
			Line2DPixel(xx, yy + hh, xx, yy), color, color, clip
		);
	}
	GLOB_ENGINE->DrawText
	(
		Point2DFloat(left + border , top), _size,
		Rect2DFloat(_x, topClip, _w, bottomClip - topClip),
		_font, color, item->text
	);
}

bool CTree::OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	CTreeItem *firstVisible = FindFirstVisible();
	if (!_firstVisible) _firstVisible = firstVisible;
	if (!_firstVisible) return false;
	if (!_selected) _selected = _firstVisible;

	switch (nChar)
	{
	case VK_UP:
		if (_selected != firstVisible)
		{
			CTreeItem *item = _selected->GetPrevVisible();
			Assert(item);
			SetSelected(item);
		}
		return true;
	case VK_DOWN:
		{
			CTreeItem *item = _selected->GetNextVisible();
			if (item) SetSelected(item);
		}
		return true;
	case VK_PRIOR:
		{
			CTreeItem *item = _selected;
			for (int i=0; i<10; i++)
			{
				if (item == firstVisible) break;
				CTreeItem *it = item->GetPrevVisible();
				Assert(it);
				item = it;
			}
			SetSelected(item);
		}
		return true;
	case VK_NEXT:
		{
			CTreeItem *item = _selected;
			for (int i=0; i<10; i++)
			{
				CTreeItem *it = item->GetNextVisible();
				if (!it) break;
				item = it;
			}
			SetSelected(item);
		}
		return true;
	case VK_LEFT:
		if (_selected->expanded)
			Expand(_selected, false);
		else
		{
			CTreeItem *item = _selected->parent;
			if (_style & TR_SHOWROOT)
			{
				if (item) SetSelected(item);
			}
			else
			{
				if (item && item != _root) SetSelected(item);
			}
		}
		return true;
	case VK_RIGHT:
		if (!_selected->expanded)
			Expand(_selected, true);
		else
		{
			if (_selected->children.Size() > 0)
				SetSelected(_selected->children[0]);
		}
		return true;
	default:
		return false;
	}
}

bool CTree::OnChar(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	CTreeItem *firstVisible = FindFirstVisible();
	if (!_firstVisible) _firstVisible = firstVisible;
	if (!_firstVisible) return false;
	if (!_selected) _selected = _firstVisible;

	switch (nChar)
	{
	case '+':
		Expand(_selected, true);
		return true;
	case '-':
		Expand(_selected, false);
		return true;
	default:
		return false;
	}
}

void CTree::OnLButtonDown(float x, float y)
{
	CTreeItem *item = FindItem(x, y);
	if (!item) return;

	const float border = 0.005;
	float textHeight = _size;
	float textWidth = 0.75 * textHeight;
	float left = _x + border + textWidth * item->level;
	
	if (x < left - textWidth) return;
	SetSelected(item);
	if (x < left) Expand(item, !item->expanded);
}

void CTree::OnLButtonDblClick(float x, float y)
{
/*	
	CTreeItem *item = FindItem(x, y);
	if (!item) return;
	
	const float border = 0.005;
	float textHeight = _size;
	float textWidth = 0.75 * textHeight;
	float left = _x + border + textWidth * item->level;

	if (x < left) return;
	Expand(item, !item->expanded);
*/
	_parent->OnTreeDblClick(IDC(), GetSelected());
}

void CTree::OnMouseMove(float x, float y, bool active)
{
	if (!GInput.mouseL) return;

	OnMouseHold(x, y, active);
}

void CTree::OnMouseHold(float x, float y, bool active)
{
	if (!GInput.mouseL) return;

	CTreeItem *firstVisible = FindFirstVisible();
	if (!_firstVisible) _firstVisible = firstVisible;
	if (!_firstVisible) return;

	const float border = 0.005;

	bool canScrollUp = false;
	bool canScrollDown = false;

	float top = _y;
	float textHeight = _size;
	if (_offset > 0 || _firstVisible != firstVisible)
	{
		canScrollUp = true;
		top += textHeight;
	}
	top += border - _offset * textHeight;
	float bottomClip = _y + _h - border;
	int nItems = _firstVisible->NVisibleItems();
	if (top + nItems * textHeight > bottomClip)
	{
		canScrollDown = true;
	}

	float dy;
	if 
	(
		canScrollUp && 
		(dy = _y + textHeight - y) > 0
	)
	{
		_scrollDown = false;
		// scroll up
		if (_scrollUp)
		{
			float scroll = SCROLL_SPEED * dy;
			saturate(scroll, SCROLL_MIN, SCROLL_MAX);
			_offset -= scroll * (Glob.uiTime - _scrollTime);
			CTreeItem *item = _firstVisible;
			while (_offset < 0)
			{
				if (item == firstVisible)
				{
					_offset = 0;
					break;
				}
				item = item->GetPrevVisible();
				_offset++;
			}
			if (item == firstVisible) _offset = 0;
			_firstVisible = item;
			_scrollTime = Glob.uiTime;
		}
		else
		{
			_scrollUp = true;
			_scrollTime = Glob.uiTime;
		}
	}
	else if
	(
		canScrollDown &&
		(dy = y - _y - _h + textHeight) > 0
	)
	{
		_scrollUp = false;
		// scroll down
		if (_scrollDown)
		{
			float scroll = SCROLL_SPEED * dy;
			saturate(scroll, SCROLL_MIN, SCROLL_MAX);
			_offset += scroll * (Glob.uiTime - _scrollTime);
			CTreeItem *item = _firstVisible;
			while (_offset > 1.0)
			{
				if (item->GetNextVisible() == NULL)
				{
					_offset = 0;
					break;
				}
				item = item->GetNextVisible();
				_offset--;
			}
			if (item == firstVisible && _offset > 0)
			{
				item = item->GetNextVisible();
			}
			_firstVisible = item;
			_scrollTime = Glob.uiTime;
		}
		else
		{
			_scrollDown = true;
			_scrollTime = Glob.uiTime;
		}
	}
	else
	{
		_scrollUp = _scrollDown = false;
	}
}

void CTree::OnMouseZChanged(float dz)
{
	if (dz == 0) return;
	
	CTreeItem *firstVisible = FindFirstVisible();
	if (!_firstVisible) _firstVisible = firstVisible;
	if (!_firstVisible) return;

	const float border = 0.005;

	bool canScrollUp = false;
	bool canScrollDown = false;

	float top = _y;
	float textHeight = _size;
	if (_offset > 0 || _firstVisible != firstVisible)
	{
		canScrollUp = true;
		top += textHeight;
	}
	top += border - _offset * textHeight;
	float bottomClip = _y + _h - border;
	int nItems = _firstVisible->NVisibleItems();
	if (top + nItems * textHeight > bottomClip)
	{
		canScrollDown = true;
	}

	if (canScrollUp && dz > 0)
	{
		// scroll up
		float scroll = -SCROLL_SPEED * dz;
		saturate(scroll, -SCROLL_MAX, -SCROLL_MIN);
		_offset += 0.1 * scroll;
		CTreeItem *item = _firstVisible;
		while (_offset < 0)
		{
			if (item == firstVisible)
			{
				_offset = 0;
				break;
			}
			item = item->GetPrevVisible();
			_offset++;
		}
		if (item == firstVisible) _offset = 0;
		_firstVisible = item;
	}
	else if (canScrollDown && dz < 0)
	{
		// scroll down
		float scroll = -SCROLL_SPEED * dz;
		saturate(scroll, SCROLL_MIN, SCROLL_MAX);
		_offset += 0.1 * scroll;
		CTreeItem *item = _firstVisible;
		while (_offset > 1.0)
		{
			if (item->GetNextVisible() == NULL)
			{
				_offset = 0;
				break;
			}
			item = item->GetNextVisible();
			_offset--;
		}
		if (item == firstVisible && _offset > 0)
		{
			item = item->GetNextVisible();
		}
		_firstVisible = item;
	}
}

CTreeItem *CTree::FindItem(float x, float y)
{
	Assert(IsInside(x, y));
	CTreeItem *firstVisible = FindFirstVisible();
	if (!_firstVisible) _firstVisible = firstVisible;
	if (!_firstVisible) return NULL;
	CTreeItem *item = _firstVisible;

	const float border = 0.005;
	
	float top = _y;
	float textHeight = _size;
	if (_offset > 0 || item != firstVisible)
	{
		top += textHeight;
		if (y < top) return NULL;
	}
	top += border - _offset * textHeight;
	
	float bottomClip = _y + _h - border;
	int nItems = item->NVisibleItems();
	if (top + nItems * textHeight > bottomClip)
	{
		bottomClip -= textHeight;
		if (y > bottomClip) return NULL;
	}
	
	while (item && top < bottomClip)
	{
		top += textHeight;
		if (y < top) return item;
		item = item->GetNextVisible();
	}
	return NULL;
}

CTreeItem *CTree::FindFirstVisible()
{
	if (_style & TR_SHOWROOT)
	{
		return _root;
	}
	else
	{
		return _root->GetNextVisible();
	}
}

