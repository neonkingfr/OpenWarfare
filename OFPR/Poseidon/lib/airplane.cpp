// Poseidon - vehicle class
// (C) 1997, Suma
#include "wpch.hpp"


#include "airplane.hpp"
#include "shots.hpp"
#include "scene.hpp"
#include "world.hpp"
#include "landscape.hpp"
#include "keyInput.hpp"
#include "global.hpp"
#include "lights.hpp"
#include "ai.hpp"
#include "dikCodes.h"
#include <El/Common/randomGen.hpp>
#include "camera.hpp"
#include "frameInv.hpp"
#include "diagModes.hpp"

#include "network.hpp"
#include "uiActions.hpp"
//#include "strIncl.hpp"
#include "stringtableExt.hpp"

#if _RELEASE
	#define ARROWS 0
#else
	#define ARROWS 1
#endif

#if _ENABLE_CHEATS
	#define LOG_SWEEP 1
#endif

Airplane::Airplane( VehicleType *name, Person *pilot )
:Transport(name,pilot),

_rpm(0), // on/off
_rotorSpeed(0),
_rotorPosition(0),

_lastAngVelocity(VZero),

_thrust(0),_thrustWanted(0), // turning motor on/off
_elevator(0),_elevatorWanted(0),
_rudder(0),_rudderWanted(0),
_aileron(0),_aileronWanted(0),
_gearsUp(0),_flaps(0),_brake(0),
_rndFrequency(1-GRandGen.RandomValue()*0.05), // do not use same sound frequency

_pilotGear(true),
_rocketLRToggle(false),
_pilotFlaps(0),
_servoVol(0),

_gunYRot(0),_gunYRotWanted(0),
_gunXRot(0),_gunXRotWanted(0),
_gunXSpeed(0),_gunYSpeed(0),

_gearDammage(false),

_pilotBrake(1)
//_leftDust(GLOB_SCENE->Preloaded(CloudletBasic),0.05),
//_rightDust(GLOB_SCENE->Preloaded(CloudletBasic),0.05),
{
	_head.SetPars("Air");
	_head.Init(Type()->_pilotPos-Vector3(0,0.2,0),Type()->_pilotPos,this);
	SetSimulationPrecision(1.0/15);

	_mGunClouds.Load((*Type()->_par) >> "MGunClouds");

	_mGunFireFrames = 0;
	_mGunFireTime = UITIME_MIN;
	_mGunFirePhase = 0;
}

Airplane::~Airplane()
{
}


AirplaneType::AirplaneType( const ParamEntry *param )
:base(param)
{
	_scopeLevel=1;

	_gunPos = VZero;

	//_gunDir = Vector3(0,-0.2f,1).Normalized();
	_gunDir = VForward;
}

void AirplaneType::Load(const ParamEntry &par)
{
	base::Load(par);

	_minGunElev=(float)(par>>"minGunElev")*(H_PI/180);
	_maxGunElev=(float)(par>>"maxGunElev")*(H_PI/180);
	_minGunTurn=(float)(par>>"minGunTurn")*(H_PI/180);
	_maxGunTurn=(float)(par>>"maxGunTurn")*(H_PI/180);

	_gearRetracting = par>>"gearRetracting";

	_aileronSensitivity = par>>"aileronSensitivity";
	_elevatorSensitivity = par>>"elevatorSensitivity";
	_wheelSteeringSensitivity = par>>"wheelSteeringSensitivity";
	_landingSpeed = par>>"landingSpeed";
	if (_landingSpeed<1)
	{
		_landingSpeed = GetMaxSpeedMs()*0.33f;
		saturateMax(_landingSpeed,120/3.6f);
	}
	else
	{
		_landingSpeed *= 1.0f/3.6f;
	}
	if (_landingSpeed>21)
	{
		_stallSpeed = _landingSpeed*0.65f;
	}
	else
	{
		_stallSpeed = _landingSpeed*0.87f;
	}
	_takeOffSpeed = floatMin(_landingSpeed,65);

	_flapsFrictionCoef = par>>"flapsFrictionCoef";
	_noseDownCoef = par>>"noseDownCoef";
	_landingAoa = par>>"landingAoa";

	_ejectSpeed = Vector3
	(
		(par>>"ejectSpeed")[0],
		(par>>"ejectSpeed")[1],
		(par>>"ejectSpeed")[2]
	);
}

/*!
\patch_internal 1.06 Date 7/18/2001 by Jirka
- Added: alternate name for airplane flap axis
*/

/*!
\patch 1.43 Date 1/24/2002 by Ondra
- New: Support for plane models with multiple propellers.
*/

void AirplaneType::InitShape()
{
	const ParamEntry &par=*_par;

	_scopeLevel=2;
	base::InitShape();

	_lRudder.Init(_shape,"leva smerovka",NULL,"osa leve smerovky");
	_rRudder.Init(_shape,"prava smerovka",NULL,"osa prave smerovky");
	_lElevator.Init(_shape,"leva vejskovka",NULL,"osa leve vejskovky");
	_rElevator.Init(_shape,"prava vejskovka",NULL,"osa prave vejskovky");
	// CHANGED
	_lAileronT.Init(_shape,"lkh klapka",NULL,"osa lk klapky","osa lkh klapky");
	_rAileronT.Init(_shape,"pkh klapka",NULL,"osa pk klapky","osa pkh klapky");
	_lAileronB.Init(_shape,"lkd klapka",NULL,"osa lk klapky","osa lkd klapky");
	_rAileronB.Init(_shape,"pkd klapka",NULL,"osa pk klapky","osa pkd klapky");
	_lFlap.Init(_shape,"ls klapka",NULL,"osa ls klapky");
	_rFlap.Init(_shape,"ps klapka",NULL,"osa ps klapky");

	_rotors[0].Init(_shape,"vrtule","vrtule 0","osa vrtule","osa vrtule 0");
	for (int i=1; i<MaxRotors; i++)
	{
		char selName[64];
		char axisName[64];
		sprintf(selName,"vrtule %d",i);
		sprintf(axisName,"osa vrtule %d",i);
		_rotors[i].Init(_shape,selName,NULL,axisName);
	}

	_rotorStill.Init(_shape,"vrtule staticka",NULL);
	_rotorMove.Init(_shape,"vrtule blur",NULL);

	_lWheel.Init(_shape,"levy kolo",NULL,"osa leveho kola");
	_rWheel.Init(_shape,"pravy kolo",NULL,"osa praveho kola");
	_fWheel.Init(_shape,"predni kolo",NULL,"osa predniho kola");

	_altRadarIndicator.Init(_shape, par >> "IndicatorAltRadar");
	_altRadarIndicator2.Init(_shape, par >> "IndicatorAltRadar2");
	_altBaroIndicator.Init(_shape, par >> "IndicatorAltBaro");
	_speedIndicator.Init(_shape, par >> "IndicatorSpeed");
	_vertSpeedIndicator.Init(_shape, par >> "IndicatorVertSpeed");
	_vertSpeedIndicator2.Init(_shape, par >> "IndicatorVertSpeed2");
	_rpmIndicator.Init(_shape, par >> "IndicatorRPM");
	_compass.Init(_shape, par >> "IndicatorCompass");
	_compass2.Init(_shape, par >> "IndicatorCompass2");
	_watch.Init(_shape, par >> "IndicatorWatch");
	_watch2.Init(_shape, par >> "IndicatorWatch2");

	_vario.Init(_shape, "horizont", NULL, "osa_horizont", NULL);
	for (int i=0; i<_shape->NLevels(); i++)
	{
		int sel = _vario.GetSelection(i);
		int selCenter = _vario.GetCenterSelection(i);
		if (sel >= 0 && selCenter >= 0)
		{
			Shape *shape = _shape->Level(i);
			_varioDirection[i] = _vario.Center(i) - shape->CalculateCenter(shape->NamedSel(sel));
		}
		else
			_varioDirection[i] = VForward;
	}

	if
	(
		_shape->MemoryLevel()->FindNamedSel("kulomet")
		//&& _shape->MemoryLevel()->FindNamedSel("kulas usti")
	)
	{
		//_gun.Init(_shape,"kulas","kulas","kulas osa");
		_gunPos=_shape->MemoryPoint("kulomet");
		//_gunAxis=_shape->MemoryPoint("kulas osa");;
		//_gunDir=(_shape->MemoryPoint("kulas usti")-_gunPos).Normalized();
		//_gunDir=VForward; //(_shape->MemoryPoint("kulas usti")-_gunPos).Normalized();

		//_gunDir=VForward; //(_shape->MemoryPoint("kulas usti")-_gunPos).Normalized();

		//_neutralGunXRot=atan2(_gunDir.Y(),_gunDir.Z());
	}

	//_missileLPos=_shape->MemoryPoint("L strela");
	//_missileRPos=_shape->MemoryPoint("P strela");
	_rocketLPos=_shape->MemoryPoint("L raketa");
	_rocketRPos=_shape->MemoryPoint("P raketa");
/*
	_lightPos=_shape->MemoryPoint("svetlo");
	_lightDir=_shape->MemoryPoint("konec svetla")-_lightPos;
	_lightDir.Normalize();
*/

	_lDust=_shape->MemoryPoint("levy prach");
	_rDust=_shape->MemoryPoint("pravy prach");

	int level;
	level=_shape->FindLevel(1100);
	if( level>=0 )
	{
		_shape->LevelOpaque(level)->MakeCockpit();
		_pilotPos=_shape->LevelOpaque(level)->NamedPosition("pilot");
	}
	level=_shape->FindLevel(1000);
	if( level>=0 )
	{
		_shape->LevelOpaque(level)->MakeCockpit();
		_gunnerPos=_shape->LevelOpaque(level)->NamedPosition("pilot");
	}

	if( _pilotPos.SquareSize()<0.1 ) _pilotPos=_shape->MemoryPoint("pilot");
	if( _gunnerPos.SquareSize()<0.1 ) _gunnerPos=_shape->MemoryPoint("gunner");

	_animFire.Init(_shape, "zasleh", NULL);
}

static float UpForce(float speed, float aoa, float maxSpeed)
{
	// ignore fly envelope - simulate low flight only
	Vector3 force(VZero);
	// speed -> up force diagram
	// speed in knots
	// force in G
	//                   land   normal     A10 max speed   
	// max pos. G 000 050 100 150 200 250 300 350 400 450
	//float maxG[]={0.0,0.1,0.4,1.5,2.5,3.3,3.8,3.2,1.0,0.0};
	const static float maxG[]={0.0,0.1,0.4,2.2,3.3,3.5,3.2,2.5,1.0,0.0};
	const int maxI=sizeof(maxG)/sizeof(*maxG)-1;
	const float maxAoa=16*H_PI/180;
	const float minAoa=-3*H_PI/180;
	const float neutralAoa=5*H_PI/180;
	aoa+=neutralAoa;

	if( aoa>maxAoa )
	{
		aoa=2*maxAoa-aoa;	
		if( aoa<0 ) aoa=0;
	}
	if( aoa<minAoa )
	{
		aoa=minAoa;
	}
	aoa *=	1/maxAoa;
	//aoa *= fabs(aoa);

	// base on airplane maxspeed 
	float fSpeed=maxI*(speed*0.7/maxSpeed);

	// 
	int iLow=toIntFloor(fSpeed);
	float frac=fSpeed-iLow;
	if( iLow>=maxI ) return 0; // no up force
	if( iLow<0 ) return 0;
	float fLow=maxG[iLow];
	float fHigh=maxG[iLow+1];
	float speedCoef = fLow+(fHigh-fLow)*frac;
	float ret = speedCoef*aoa;
	/*
	GlobalShowMessage
	(
		100,"fSpeed %.3f, speedCoef %.1f, Up coef %.1f, aoa %.1f",
		fSpeed,speedCoef,ret,aoa
	);
	*/
	#if 0
	LogF
	(
		"fSpeed %.3f, speedCoef %.1f, Up coef %.1f, aoa %.1f",
		fSpeed,speedCoef,ret,aoa
	);
	#endif
	return ret;
}


#define FV(x) (x)[0],(x)[1],(x)[2]
#define FFV "[%.0f,%.0f,%.0f]"

static const Color HeliLightColor(0.8,0.8,1.0);
static const Color HeliLightAmbient(0.07,0.07,0.1);

bool Airplane::CastProxyShadow(int level, int index) const
{
	return base::CastProxyShadow(level,index);
}

// TODO: move to EntityAI class

//! Count guided missiles
static int CountMissiles(const EntityAI *ai)
{
	int total = 0;
	for (int i=0; i<ai->NMagazines(); i++)
	{
		const Magazine *mag = ai->GetMagazine(i);
		if (mag->_type->_modes.Size()<=0) continue;
		const WeaponModeType *mode = mag->_type->_modes[0];
		const AmmoType *ammo = mode->_ammo;
		if (!ammo) continue;
		if (ammo->_simulation!=AmmoShotMissile) continue;
		if (ammo->maxControlRange<10) continue;
		total += mag->_ammo;
	}
	return total;		
}

//! Count ammo of given name
static int CountAmmo(const EntityAI *ai, const char *weapon)
{
	int total = 0;
	for (int i=0; i<ai->NMagazines(); i++)
	{
		const Magazine *mag = ai->GetMagazine(i);
		if (strcmpi(mag->_type->GetName(), weapon) != 0) continue;
		total += mag->_ammo;
	}
	return total;
}


/*!
\patch_internal 1.07 Date 7/20/2001 by Ondra.
- Fixed: Missiles recognized by properties instead of name.
*/
int Airplane::GetProxyComplexity
(
	int level, const FrameBase &pos, float dist2
) const
{
	return base::GetProxyComplexity(level,pos,dist2);
}

/*!
\patch 1.02 Date 7/11/2001 by Ondra.
- Fixed: Mavericks disappers from pylons after release.
*/

void Airplane::DrawProxies
(
	int level, ClipFlags clipFlags,
	const Matrix4 &transform, const Matrix4 &invTransform,
	float dist2, float z2, const LightList &lights
)
{
	// draw flag
	base::DrawProxies
	(
		level,clipFlags,transform,invTransform,dist2,z2,lights
	);
}

Object *Airplane::GetProxy
(
	LODShapeWithShadow *&shape,
	int level,
	Matrix4 &transform, Matrix4 &invTransform,
	const FrameBase &parentPos, int i
) const
{
	// check proxy type:

	return base::GetProxy(shape,level,transform,invTransform,parentPos,i);
}


void Airplane::Draw( int level, ClipFlags clipFlags, const FrameBase &pos )
{
	base::Draw(level,clipFlags,pos);
}

void AirplaneAuto::DrawDiags()
{
	// draw ILS position and direction
	
	LODShapeWithShadow *forceArrow=GScene->ForceArrow();

	Vector3 ilsPos,ilsDir;
	GWorld->GetILS(ilsPos,ilsDir,_targetSide);
	{
		Ref<Object> arrow=new ObjectColored(forceArrow,-1);

		float size=3;
		arrow->SetPosition(ilsPos);
		arrow->SetOrient(VUp,VForward);
		arrow->SetPosition
		(
			arrow->PositionModelToWorld(forceArrow->BoundingCenter()*size)
		);
		arrow->SetScale(size);
		arrow->SetConstantColor(PackedColor(Color(0,0,0)));
		GScene->ObjectForDrawing(arrow);

	}

	//if (!QIsManual())
	{
		Vector3 dir = Matrix3(MRotationY,-_pilotHeading).Direction();

		Ref<Object> arrow=new ObjectColored(forceArrow,-1);

		float size=0.3f;
		arrow->SetPosition(Position()+VUp*2);
		arrow->SetOrient(dir,VUp);
		arrow->SetPosition
		(
			arrow->PositionModelToWorld(forceArrow->BoundingCenter()*size)
		);
		arrow->SetScale(size);
		arrow->SetConstantColor(PackedColor(Color(0,1,0)));
		GScene->ObjectForDrawing(arrow);
	}

	{
		Ref<Object> arrow=new ObjectColored(forceArrow,-1);

		float size=1;
		arrow->SetPosition(ilsPos+ilsDir*10);
		arrow->SetOrient(VUp,VForward);
		arrow->SetPosition
		(
			arrow->PositionModelToWorld(forceArrow->BoundingCenter()*size)
		);
		arrow->SetScale(size);
		arrow->SetConstantColor(PackedColor(Color(0,0,0)));
		GScene->ObjectForDrawing(arrow);

	}

	if( _sweepTarget )
	{
		{
			Ref<Object> arrow=new ObjectColored(forceArrow,-1);

			float size=5;
			arrow->SetPosition(_sweepTarget->AimingPosition());
			arrow->SetOrient(VUp,VForward);
			arrow->SetPosition
			(
				arrow->PositionModelToWorld(forceArrow->BoundingCenter()*size)
			);
			arrow->SetScale(size);
			arrow->SetConstantColor(PackedColor(Color(1,0,0)));
			GScene->ObjectForDrawing(arrow);

		}
		{
			Ref<Object> arrow=new ObjectColored(forceArrow,-1);

			float size=5;
			Vector3 pos=Position()+Direction()*10;
			arrow->SetPosition(pos);
			arrow->SetOrient(_sweepTarget->AimingPosition()-pos,VUp);
			arrow->SetPosition
			(
				arrow->PositionModelToWorld(forceArrow->BoundingCenter()*size)
			);
			arrow->SetScale(size);
			arrow->SetConstantColor(PackedColor(Color(1,0,0)));
			GScene->ObjectForDrawing(arrow);

		}
	}

	base::DrawDiags();
}

RString AirplaneAuto::DiagText() const
{
	RString text=base::DiagText();
	char buf[256];
	sprintf(buf," h=%.1f, d=%.2f",_pilotHeight,_forceDive);
	if( _sweepTarget )
	{
		sprintf(buf+strlen(buf)," sweep %d",_sweepState);
	}
	return text+RString(buf);
}

inline const float DecreaseAbs(float x, float y)
{
	if (fabs(x)<y) return 0;
	if (x>0) return x-y;
	return x+y;
}


inline float CalculateAOA(Vector3Par speed)
{
  float speedSize2 = speed.SquareSize();
  return speedSize2>1e-6 ? -speed.Y()*InvSqrt(speedSize2) : 0;
}


float Airplane::DetectStall() const
{
	if (_landContact) return 0;
  float aoa = CalculateAOA(ModelSpeed());

	//float landingSpeed = Type()->_landingSpeed;
	float stallSpeed = Type()->_stallSpeed;

	float stall = 1.5-ModelSpeed().Z()/stallSpeed;
	saturateMax(stall,0);
	stall += 0.3f;
	saturateMin(stall,1);

	const float maxAoa = 10*H_PI/180;
	const float okAoa = 5*H_PI/180;

	float aoaStall = (aoa-okAoa)/maxAoa;
	saturate(aoaStall,0,1);

	//GlobalShowMessage(100,"Stall %.3f, %.3f",stall,aoaStall);
	return stall*aoaStall;
}

void Airplane::PerformFF(FFEffects &eff)
{
	base::PerformFF(eff);
	// stall warning
	eff.engineFreq = 7;
	// stall warning
	eff.engineMag = DetectStall()*0.25f;
	eff.stiffnessX = 0.6f;
	eff.stiffnessY = 0.6f;
}

/*!
\patch 1.02 Date 7/10/2001 by Ondra.
- Fixed: Plane - less speed loss while turning.
\patch 1.22 Date 8/30/2001 by Ondra.
- Fixed: Planes bouncing when falling in water.
\patch 1.24 Date 9/20/2001 by Ondra.
- Fixed: Airplane ground target engaging improved.
\patch 1.30 Date 10/20/2001 by Ondra.
- Fixed: MP: Airplane gear could be dammaged on remote computer.
\patch 1.43 Date 1/25/2002 by Ondra
- Fixed: Airplane climbing in high altitudes limited.
Maximum possible altitude is now about 12000 m.
\patch 1.58 Date 5/19/2002 by Ondra
- Fixed: Airplane jumping after crash fixed.
\patch 1.89 Date 10/22/2002 by Ondra
- New: Adjustable plane wheel steering (wheelSteeringSensitivity).
Should make big plane taxiing possible.
\patch 1.90 Date 10/30/2002 by Ondra
- Fixed: Airplane engine produces less thrust when speed is near plane top speed.
\patch 1.90 Date 10/30/2002 by Ondra
- Improved: Better airplane AI dogfighting.
\patch 1.90 Date 11/1/2002 by Ondra
- Fixed: Keyboard rudder control enabled while taxiing.
\patch 1.90 Date 11/1/2002 by Ondra
- Fixed: Airplanes were turning south when climbing while flying east or west.
\patch 1.95 Date 10/26/2003 by Ondra
- Fixed: Airplane rudder working during flight.
*/

void Airplane::Simulate( float deltaT, SimulationImportance prec )
{
	_isDead = IsDammageDestroyed();

	if( _thrust>0 )
	{
		ConsumeFuel(deltaT*0.03f);
	}
	
	float delta;

	if( _isDead ) _engineOff=true,_pilotBrake=1;
	if( _fuel<=0 ) _engineOff=true;
	if( _engineOff )
	{
		_thrustWanted=0;
		if( _landContact ) _pilotBrake=1;
	}

	// main engine
	delta=!_engineOff-_rpm;
	Limit(delta,-0.1f*deltaT,+0.1f*deltaT);
	_rpm+=delta;
	Limit(_rpm,0,1);

	delta=_thrustWanted-_thrust;
	Limit(delta,-0.7f*deltaT,+0.4f*deltaT);
	_thrust+=delta;
	Limit(_thrust,0,1);

	_rotorSpeed = 10.0f * _rpm * (0.4f + _thrust);
	_rotorPosition += _rotorSpeed * deltaT * 20.0f;

	Vector3Val speed=ModelSpeed();
	//float landingSpeed = Type()->_landingSpeed;
	//float stallSpeed = Type()->_stallSpeed;

	float altCoef = 1;
	float alt = Position().Y();

	const float altFullForce = 5000;
	const float altNoForce = 13000;
	if (alt>altFullForce)
	{
		altCoef = 1-(alt-altFullForce)*(1/(altNoForce-altFullForce));
	}

	float flapEff = fabs(speed[2])/(Type()->_landingSpeed*0.65f)-0.5f;
	saturate(flapEff,0,1);
	flapEff *= altCoef;
	//GlobalShowMessage
	//(
	//	100,"Eff %.2f, speed %.2f, stall %.2f",
	//	flapEff,speed[2],stallSpeed
	//);

	Matrix3 undive;
	/*
	undive.SetDirectionAndUp(Direction(),VUp);
	Vector3 bankSide=undive*DirectionAside();
	*/
	undive.SetUpAndDirection(VUp,Direction());
	Vector3 bankSide=undive.InverseRotation()*DirectionAside();
	//float bank=DirectionAside().Y();
	//GlobalShowMessage
	//(
	//	100,"bankSide %.2f,%.2f,%.2f",bankSide[0],bankSide[1],bankSide[2]
	//);
	float bank=bankSide.Y();
	// coordinate rudder with bank
	// control rudder only when moving slow
  float rudderWanted = bank*flapEff+_rudderWanted; //*(1-flapEff);
	const float fullElevSpeed = floatMin(25,Type()->_landingSpeed*0.8f);
	const float noElevSpeed = fullElevSpeed*0.5;
	if( speed[2]<fullElevSpeed && _landContact )
	{
		float maxElev = (speed[2]-13.5f)/(fullElevSpeed-noElevSpeed);
		saturate(maxElev,0,1);
		// do not use ailerons and elevators when ground steering
		saturate(_aileronWanted,-maxElev,+maxElev);
		saturate(_elevatorWanted,-maxElev,+maxElev);
		//_aileronWanted=0;
		//_elevatorWanted=0;
	}

	//GlobalShowMessage
	//(
	//	100,"Rudder %.2f->%.2f, eff %.3f, bank %.2f",rudderWanted,_rudder,flapEff,bank
	//);
	// change rudder
	delta=rudderWanted-_rudder;
	Limit(delta,-4*deltaT,4*deltaT);
	_rudder+=delta;
	Limit(_rudder,-1,+1);

	// change aileron
	delta=_aileronWanted-_aileron;
	Limit(delta,-4*deltaT,4*deltaT);
	_aileron+=delta;
	Limit(_aileron,-1,+1);
	
	// change elevator
	delta=_elevatorWanted-_elevator;
	Limit(delta,-4*deltaT,4*deltaT);
	_elevator+=delta;
	Limit(_elevator,-1,+1);

	bool doServo=false;

	// change gears
	float gearWanted=1-_pilotGear;
	if( _gearDammage ) gearWanted=-0.66f;
	if (!Type()->_gearRetracting)
	{
		gearWanted = 0;
	}
	delta=gearWanted-_gearsUp;
	if( fabs(delta)>0.01 ) doServo=true;
	Limit(delta,-0.5f*deltaT,+0.3f*deltaT);
	float oldGears = _gearsUp;
	_gearsUp+=delta;
	const float tholdUp = 0.95f;
	const float tholdDown = 0.05f;
	if (_gearsUp>=tholdUp && oldGears<tholdUp) OnEvent(EEGear,false);
	if (_gearsUp<=tholdDown && oldGears>tholdDown) OnEvent(EEGear,true);
	Limit(_gearsUp,-1,1);

	// change flaps
	delta=_pilotFlaps*0.5-_flaps;
	if( fabs(delta)>0.01 ) doServo=true;
	Limit(delta,-0.33*deltaT,0.33*deltaT);
	_flaps+=delta;
	Limit(_flaps,0,1);

	delta=doServo-_servoVol;
	Limit(delta,-2*deltaT,2*deltaT);
	_servoVol+=delta;

	// change brakes
	float brakeWanted=_pilotBrake;
	if( _landContact && fabs(speed[2])<10 ) brakeWanted=0;
	if( _isDead ) brakeWanted=0;
	delta=brakeWanted-_brake;
	Limit(delta,-1*deltaT,1*deltaT);
	_brake+=delta;
	Limit(_brake,0,1);

	// do not predict too much (MP)
	if (!CheckPredictionFrozen())
	{
		// calculate all forces, frictions and torques
		Vector3 force(VZero),friction(VZero);
		Vector3 torque(VZero),torqueFriction(VZero);
		
		// world space center of mass
		Vector3 wCenter(VFastTransform,ModelToWorld(),GetCenterOfMass());

		// we consider following forces
		// gravity
		// main rotor (in direction of rotor axis, changed by cyclic, controled by thrust)
		// back rotor (in direction of rotor axis - controled together with cyclic)
		// body aerodynamics (air friction)
		// main rotor aerodynamics (air friction and "up" force)
		// body and rotor torque friction (air friction)

		// partial force and application point
		//Vector3 pForce(NoInit),pCenter(NoInit);
		Vector3 pForce(VZero),pCenter(VZero);


		// apply main engine force in COM
		float maxSpeed = GetType()->GetMaxSpeedMs();
		// once top speed is reached, engine becomes ineffective
		float spdCoef = Interpolativ(speed.Z(),maxSpeed*0.66f,maxSpeed*1.15f,1,0);
		force[2]+=_thrust*GetMass()*sqrt(maxSpeed)*(0.4*G_CONST/13)*altCoef*spdCoef;
		// real aoa - angle between plane axis and plane speed direction
		// assume x==sin(x) for small x
    float aoa = CalculateAOA(ModelSpeed()) + _flaps*(3*H_PI/180);

		float upForce=UpForce(fabs(speed[2]),aoa,maxSpeed)*altCoef;
		// apply wing up force
		force[1]+=upForce*G_CONST*GetMass();
		#if 0
			LogF
			(
				"Force size %.2f, aoa %.2f, vert %.2f",
				force.Size(),aoa,DirectionModelToWorld(force).Y()
			);
		#endif
		#if ARROWS
			AddForce
			(
				wCenter,
				DirectionModelToWorld(force)*InvMass(),
				Color(0,1,0)
			);
		#endif
		Assert( force.Size()<G_CONST*GetMass()*10 );
		// apply rudder/ailerons
		if( _landContact && speed[2]<15 )
		{
			// steering engaged
			pCenter=Vector3(0,0,0.75);
			float steerEff=fabs(speed[2]*6);
			saturateMin(steerEff,40);
			steerEff *= Type()->_wheelSteeringSensitivity;
			pForce=Vector3(-_rudder*GetMass()*steerEff,0,0);
			torque+=pCenter.CrossProduct(pForce);
			#if ARROWS
				AddForce
				(
					DirectionModelToWorld(pCenter)+wCenter,
					DirectionModelToWorld(pForce)*InvMass()
				);
			#endif
		}
		else
		{
			// aileron efficiency depend on size?
			float sizeCoef = GetRadius()*(1.0/6)*Type()->_aileronSensitivity;
			pCenter = Vector3(5,0,0);
			pForce = Vector3(0,_aileron*GetMass()*flapEff*G_CONST*0.8*sizeCoef,0);
			torque += pCenter.CrossProduct(pForce);
			#if ARROWS
				AddForce
				(
					DirectionModelToWorld(pCenter)+wCenter,
					DirectionModelToWorld(pForce)*InvMass()
				);
			#endif
		}
		// apply elevator
		pForce=Vector3(0,_elevator*GetMass()*flapEff*G_CONST*0.6*Type()->_elevatorSensitivity,0);
		pCenter=Vector3(0,0,-5);
		torque+=pCenter.CrossProduct(pForce);
		#if ARROWS
			AddForce
			(
				DirectionModelToWorld(pCenter)+wCenter,
				DirectionModelToWorld(pForce)*InvMass(),
				Color(0,0,1)
			);
		#endif

		// rotate plane when in free fall mode
		if (_isDead)
		{
			torque+=Vector3(1.5,3.2,-2.5).CrossProduct(Vector3(0,GetMass(),0));
		}
		// detect stall
		else if(fabs(ModelSpeed().Z())<Type()->_landingSpeed*0.65f)
		{
			float stall = DetectStall();
			torque+=Vector3(1.5,3.2,-2.5).CrossProduct(Vector3(0,GetMass()*stall,0));
		}
		
		// when moving fast, bank causes torque
		//LogF("bank %.2f, rudder %.2f",bank,_rudder);
    float rudderZ = GetRadius()*0.38f;
    //GlobalShowMessage(100,"rz %.2f",rudderZ);
    pForce=Vector3(_rudder*GetMass()*flapEff,0,0);
    // rudder force dependent on size
    pCenter=Vector3(0,-0.25,-rudderZ); // rudder causes some change in bank as well
    torque+=pCenter.CrossProduct(pForce);
    Assert(torque.IsFinite());
    #if ARROWS
      AddForce
      (
        DirectionModelToWorld(pCenter)+wCenter,
        DirectionModelToWorld(pForce)*InvMass()
      );
    #endif

    pForce=Vector3(bank*0.3*GetMass()*flapEff,0,0);
		pCenter=Vector3(0,0,-3);
		torque+=pCenter.CrossProduct(pForce);
		#if ARROWS
			AddForce
			(
				DirectionModelToWorld(pCenter)+wCenter,
				DirectionModelToWorld(pForce)*InvMass()
			);
		#endif

		// there are forces keeping plane aligned with its speed direction
		// simulate wing draconic force
		// strong in y-direction (wing)
		// calculate area of wing in direction of movement

		// when low speed, descrease draconic force and apply torque instead
		float highSpeed = speed[2]/(Type()->_landingSpeed*0.65f*0.7f)-0.3f;
		saturate(highSpeed,0,1);
		//pForce[1]=(speed[1]*-0.5+speed[1]*fabs(speed[1])*-0.10)*GetMass();
		// FIX: bigger draconic force
		// drag size depend on
		pForce[1]=(speed[1]*-0.05f+speed[1]*fabs(speed[1])*-0.10f)*GetMass();
		// weak in x-direction (body)
		pForce[0]=(speed[0]*-0.1f+speed[0]*fabs(speed[0])*-0.04f)*GetMass();
		pForce[2]=0;
		//pCenter=Vector3(0,0,-0.1);
		//pCenter=Vector3(0,0,-0.005/dracFactor);
		pCenter=VZero;
		//torque+=pCenter.CrossProduct(pForce);
		pForce *= highSpeed*altCoef;
		force += pForce;
		#if 0
			LogF
			(
				"Draconic %.1f,%.1f,%.1f, up %.1f, speed %.1f,%.1f,%.1f",
				pForce.X(),pForce.Y(),pForce.Z(),
				DirectionModelToWorld(pForce).Y(),
				speed.X(),speed.Y(),speed.Z()
			);
		#endif
		#if ARROWS 
			AddForce
			(
				DirectionModelToWorld(pCenter)+wCenter,
				DirectionModelToWorld(pForce)*InvMass(),
				Color(1,1,0)
			);
		#endif
		if (highSpeed<1)
		{
			// apply torque that will alling plane so that it flies forward
			pForce = -speed*0.25f*(1-highSpeed)*GetMass();
			pCenter = Vector3(0,0,-4);
			torque+=pCenter.CrossProduct(pForce);
			#if ARROWS 
				AddForce
				(
					DirectionModelToWorld(pCenter)+wCenter,
					DirectionModelToWorld(pForce)*InvMass(),
					Color(0,0,1)
				);
			#endif
		}

		// aerodynamic body friction	
		friction[0]=speed[0]*fabs(speed[0])*0.00028+speed[0]*0.028;
		friction[1]=speed[1]*fabs(speed[1])*0.00050+speed[1]*0.050;
		friction[2]=speed[2]*fabs(speed[2])*0.00006+speed[2]*0.006;
		// apply all elements
		friction[2]*=(1.0+_flaps*Type()->_flapsFrictionCoef+fabs(1-_gearsUp)*0.5+3*_brake);
		friction*=GetMass()*altCoef;

		#if ARROWS
			AddForce
			(
				wCenter,
				DirectionModelToWorld(friction)*InvMass(),
				Color(1,0,0)
			);
		#endif

		// convert forces to world coordinates
		DirectionModelToWorld(torque,torque);
		DirectionModelToWorld(force,force);
		DirectionModelToWorld(friction,friction);

		// when the plane is banking, nose goes down
		// I have no proper explanation for this, but it happens
		// this force is key for turning
		// bank causes plane front tip to go down (in world coordinate system)
		float noseCoef = fabs(bank);

		saturateMin(noseCoef,0.6f);
		pForce=Vector3(0,noseCoef*GetMass()*7*noseCoef*Type()->_noseDownCoef,0);
		pCenter=-3*Direction();
		torque+=pCenter.CrossProduct(pForce);

		#if ARROWS
			AddForce
			(
				pCenter+wCenter,
				pForce*InvMass(),
				Color(0,1,1)
			);
		#endif
		
		// angular velocity causes also some angular friction
		// this should be simulated as torque
		//torqueFriction=_angMomentum*0.8;
		torqueFriction=_angMomentum*2.0;
		
		// calculate new position
		Matrix4 movePos;
		ApplySpeed(movePos,deltaT);
		// consider using frame with inverse
		Frame moveTrans;
		moveTrans.SetTransform(movePos);

		// body air friction
		#if ARROWS
			AddForce(wCenter,-friction*InvMass());
		#endif
		
		// gravity - no torque
		pForce=Vector3(0,-1,0)*(GetMass()*G_CONST);
		force+=pForce;
		#if ARROWS
			AddForce(wCenter,pForce*InvMass());
		#endif


		float totalPressure=0;
		

		// recalculate COM to reflect change of position
		wCenter.SetFastTransform(movePos,GetCenterOfMass());

		float soft=0,dust=0;
		if( deltaT>0 )
		{
			bool wasLandContact = _landContact;
			_objectContact=false;
			_landContact=false;
			_waterContact=false;

			Vector3 totForce(VZero);

			// check collision on new position

			float crash = 0;
			if (IsLocal())
			{
				CollisionBuffer collision;
				GLandscape->ObjectCollision(collision,this,moveTrans);
				if( collision.Size()>0  )
				{
					#define MAX_IN 0.4
					#define MAX_IN_FORCE 0.2
					#define MAX_IN_FRICTION 0.4

					_objectContact=true;
					for( int i=0; i<collision.Size(); i++ )
					{
						// info.pos is relative to object
						CollisionInfo &info=collision[i];
						if (info.object)
						{
							if( info.object->GetMass()>=1000 )
							{
								Point3 pos=info.object->PositionModelToWorld(info.pos);
								Vector3 dirOut=info.object->DirectionModelToWorld(info.dirOut);
								// create a force pushing "out" of the collision
								float forceIn=floatMin(info.under,MAX_IN_FORCE);
								Vector3 pForce=dirOut*GetMass()*30*forceIn;
								// apply proportional part of force in place of impact
								pCenter=pos-wCenter;
								totForce+=pForce;
								torque+=pCenter.CrossProduct(pForce);
								
								Vector3Val objSpeed=info.object->ObjectSpeed();
								Vector3 colSpeed=_speed-objSpeed;
								// if info.under is bigger than MAX_IN, move out
								if( info.under>MAX_IN )
								{
									Point3 newPos=moveTrans.Position();
									float moveOut=info.under-MAX_IN;
									newPos+=dirOut*moveOut*0.1;
									moveTrans.SetPosition(newPos);
								}

								saturateMax(crash, (colSpeed.SquareSize()-25)*(1.0/25));

								const float maxRelSpeed=5;
								if( colSpeed.SquareSize()>Square(maxRelSpeed) )
								{
									// adapt _speed to match criterion
									colSpeed.Normalize();
									colSpeed*=maxRelSpeed;
									// only slow down
									float oldSize = _speed.Size();
									_speed = colSpeed+objSpeed;
									if (_speed.SquareSize()>Square(oldSize))
									{
										_speed = _speed.Normalized()*oldSize;
									}
								}

								// second is "land friction" - causing no momentum

								float frictionIn=floatMin(info.under,MAX_IN_FRICTION);
								pForce[0]=fSign(speed[0])*20000;
								pForce[1]=speed[1]*fabs(speed[1])*1000+speed[1]*8000+fSign(speed[1])*10000;
								pForce[2]=speed[2]*fabs(speed[2])*150+speed[2]*250+fSign(speed[2])*2000;

								pForce=DirectionModelToWorld(pForce)*GetMass()*(1.0/1000)*frictionIn;
								//saturateMin(pForce[1],0);
								//torque-=pCenter.CrossProduct(pForce);
								#if ARROWS
									AddForce(wCenter+pCenter,-pForce*InvMass());
								#endif
								friction+=pForce;
								torqueFriction+=_angMomentum*0.15;
							}
						}
					}
				}
			}
			
			GroundCollisionBuffer gCollision;
			bool enableLandcontact = true;
			if (wasLandContact)
			{
				float dx,dz;
				GLandscape->RoadSurfaceY(Position(),&dx,&dz);
				Vector3 up(-dx,1,-dz);
				up.Normalize();
				// check aside and front-back separetelly
				Vector3 relUp = DirectionWorldToModel(up);


				if(fabs(relUp.X())>0.17f || fabs(relUp.Z())>0.34f) // sin 10 deg, sin 20 deg
				{
					enableLandcontact = false;
				}
			}
			GLOB_LAND->GroundCollision(gCollision,this,moveTrans,0.05,1,enableLandcontact);

			if( gCollision.Size()>0 )
			{
				Vector3 gFriction(VZero);
				float maxUnder=0;
				#define MAX_UNDER 0.1
				#define MAX_UNDER_FORCE 0.1
				Shape *landcontact = GetShape()->LandContactLevel();
				int nContactPoint = landcontact ? landcontact->NPos() : 3;
				saturateMax(nContactPoint,gCollision.Size());
				const float nPointCoef = 3.0f/nContactPoint;
				for( int i=0; i<gCollision.Size(); i++ )
				{
					// info.pos is world space
					UndergroundInfo &info=gCollision[i];
					if( info.under<0 ) continue;
					float under;
					if( info.type==GroundWater )
					{
						under=floatMin(info.under,2)*0.00025*nPointCoef;
						// TODO: calculate water friction
						_waterContact=true;
						if( _speed.SquareSize()>Square(8) ) crash=2;
					}
					else
					{
						_landContact=true;
						// we consider two forces
						//ReportFloat("land",info.under);
						if( maxUnder<info.under ) maxUnder=info.under;
						under=floatMin(info.under,MAX_UNDER_FORCE);
					}

					// one is ground "pushing" everything out - causing some momentum
					Vector3 dirOut=Vector3(0,info.dZ,1).CrossProduct(Vector3(1,info.dX,0)).Normalized();
					pForce=dirOut*(GetMass()*120.0*under*nPointCoef);
					pCenter=info.pos-wCenter;
					torque+=pCenter.CrossProduct(pForce);
					//torque+=pCenter.CrossProduct(pForce*0.5);
					// to do: analyze ground reaction force
					totForce+=pForce;

					if (pForce.SquareSize()>Square(GetMass()*G_CONST*2))
					{
						// if there is more than certain limit, gear becomes dammaged
						if (IsLocal())
						{
							if( _gearsUp<0.5 ) _gearDammage=true;
						}
					}

					#if ARROWS
						AddForce(wCenter+pCenter,pForce*InvMass());
					#endif
					
					// surface normal is dirOut
					// ignore vertical speed size
					Vector3 sSpeed=speed-dirOut*(dirOut*speed);
					// second is "wheel and land friction" - causing momentum
					// apply wheel friction
					float pressure=under*10*nPointCoef;
					pForce[0]=fSign(sSpeed[0])*24000*pressure;
					pForce[1]=fSign(sSpeed[1])*24000*pressure;
					pForce[2]=sSpeed[2]*300*pressure+fSign(sSpeed[2])*500*pressure;

					if( info.texture )
					{
						soft=info.texture->Roughness()*2.5;
						dust=info.texture->Dustness()*2.5;
					}

					// when we are on soft surface, there is more friction
					pForce[0]+=pressure*soft*sSpeed[0]*fabs(sSpeed[0])*100;
					pForce[1]+=pressure*soft*sSpeed[1]*fabs(sSpeed[1])*100;
					pForce[2]+=pressure*soft*sSpeed[2]*fabs(sSpeed[2])*25;
					
					if (IsDammageDestroyed() || !enableLandcontact)
					{
						// very strong friction for moving dammaged plane
						if (fabs(sSpeed[2])>2)
						{
							pForce[2]+=fSign(sSpeed[2])*160000*pressure;
						}
						else
						{
							pForce[2]+=fSign(sSpeed[2])*40000*pressure;
						}
					}
					else if( fabs(sSpeed[2])<25)
					{
						// apply wheel brake only when moving slow
						pForce[2]+=fSign(sSpeed[2])*20000*pressure*_pilotBrake;
					}

					totalPressure+=pressure;
					
					// torque applied if speed is big enough
					if( fabs(sSpeed[0])<0.1 ) pForce[0]=0;
					if( fabs(sSpeed[1])<0.1 ) pForce[1]=0;
					if( fabs(sSpeed[2])<0.1 ) pForce[2]=0;


					pForce=DirectionModelToWorld(pForce)*nPointCoef*(GetMass()*(1.0/10000));

					if( pForce.SquareSize()>Square(GetMass()*G_CONST) )
					{
						// if there is more than certain limit, gear becomes dammaged
						if (IsLocal())
						{
							if( _gearsUp<0.5 ) _gearDammage=true;
						}
					}

					friction+=pForce;

					#if ARROWS
						AddForce(wCenter+pCenter+Vector3(0,0.2,0),-pForce*InvMass());
					#endif
					torque-=pCenter.CrossProduct(pForce); // sub: it is friction

					torqueFriction+=_angMomentum*0.5;
				}
				if (_waterContact)
				{
					const SurfaceInfo &info = GLandscape->GetWaterSurface();
					soft = info._roughness * 2.5;
					dust = info._dustness * 2.5;
				}
				// check gears
				// with gears we can handle much higher Z speed
				if (_gearsUp>0.5)
				{
					// gears up
					saturateMax(crash, (_speed.SquareSize()-25)*(1.0/20));
				}
				else if (_gearsUp>-0.5)
				{
					// gears down
					Vector3 crashSpeed = ModelSpeed();
					crashSpeed[0] = DecreaseAbs(crashSpeed[0],2);
					crashSpeed[1] = DecreaseAbs(crashSpeed[1],3);
					crashSpeed[2] = DecreaseAbs(crashSpeed[2],maxSpeed*0.40);
					saturateMax(crash, (crashSpeed.SquareSize()-15)*(1.0/20));
				}
				else
				{
					// gears dammages
					Vector3 crashSpeed = ModelSpeed();
					crashSpeed[0] = DecreaseAbs(crashSpeed[0],2);
					crashSpeed[1] = DecreaseAbs(crashSpeed[1],2);
					crashSpeed[2] = DecreaseAbs(crashSpeed[2],maxSpeed*0.25);
					saturateMax(crash, (crashSpeed.SquareSize()-10)*(1.0/20));
				}
				if( maxUnder>MAX_UNDER )
				{
					// it is neccessary to move object immediatelly
					Point3 newPos=moveTrans.Position();
					float moveUp=maxUnder-MAX_UNDER;
					newPos[1]+=moveUp;
					moveTrans.SetPosition(newPos);

					if (_speed.SquareSize()>Square(70))
					{
						_speed.Normalize();
						_speed *= 70;

					}
					saturateMax(_speed[1],-0.1);
				}
				if (_waterContact)
				{
					friction += 8*_speed;
				}


			}
			force+=totForce;

			if (crash>0)
			{
				if( Glob.time>_disableDammageUntil )
				{
					_disableDammageUntil = Glob.time+0.5;
					// crash boom bang state - impact speed too high
					//LogF("Heli Crash %g",crash);
					_doCrash=CrashLand;
					if( _objectContact ) _doCrash=CrashObject;
					if( _waterContact ) _doCrash=CrashWater;
					_crashVolume=crash;
					saturateMax(_crashVolume,crash);

					CrashDammage(crash);
					if (IsLocal() && crash>5)
					{
						// set some explosion
						float maxTime = 5-crash*0.2;
						saturate(maxTime,0.5,3);
						if (_explosionTime>Glob.time+maxTime)
						{
							_explosionTime=Glob.time+GRandGen.Gauss(0,maxTime*0.3,maxTime);
						}
					}
				}
			}
		}

		if( _pilotBrake>0.5f && _landContact )
		{
			// apply static friction
			if( _speed.SquareSize()<0.5f )
			{
				_speed=VZero;
				//Stop();
			}
		}

		// apply all forces
		_lastAngVelocity=_angVelocity; // helper for prediction
		ApplyForces(deltaT,force,torque,friction,torqueFriction);

		// simulate head position
		// calculate how pilot's head is moved is world space between the frames
		// new vehicle position is in moveTrans
		// old is in Transform()
		if( prec<=SimulateCamera ) _head.Move(deltaT,moveTrans,*this);

		if
		(
			prec<=SimulateVisibleFar
			&& _landContact && DirectionUp().Y()>=0.3
			&& fabs(ModelSpeed()[2])>1
		)
		{
			//_track.Update(*this,deltaT,!_landContact);
			float dSoft=floatMax(dust,0.001)*totalPressure;
			// density depends on mass
			float density=fabs(ModelSpeed()[2])*(1.0/10)*dSoft*(2+_pilotBrake*8);
			density *= GetMass()*(1.0/40000);
			if( density>=0.002 )
			{
				Vector3 lPos=PositionModelToWorld(Type()->_lDust);
				Vector3 rPos=PositionModelToWorld(Type()->_rDust);
				saturate(density,0,1);
				float dustColor=dSoft*8;
				saturate(dustColor,0,1);
				Color color=Color(0.51,0.46,0.33)*dustColor+Color(0.5,0.5,0.5)*(1-dustColor);
				_leftDust.SetColor(color);
				_rightDust.SetColor(color);
				_leftDust.Simulate(lPos+_speed*0.1,_speed*0.66,density,deltaT);
				_rightDust.Simulate(rPos+_speed*0.1,_speed*0.66,density,deltaT);
			}
		}

		if( _isDead && (_landContact || _objectContact) )
		{
			NeverDestroy();
			SmokeSourceVehicle *smoke=dyn_cast<SmokeSourceVehicle>(GetSmoke());
			if( smoke ) smoke->Explode();
		}


		Move(moveTrans); // finally apply move
		DirectionWorldToModel(_modelSpeed,_speed);
	} // if (!CheckPredictionFrozen())

	// machine gun light and clouds
	if (EnableVisualEffects(prec))
	{
		if (_mGunClouds.Active() || _mGunFire.Active())
		{
			Matrix4Val toWorld = Transform();
			Vector3Val dir = toWorld.Direction();
			Vector3 gunPos(VFastTransform,toWorld,Type()->_gunPos);
			_mGunClouds.Simulate(gunPos,Speed()*0.7+dir*5.0,0.35,deltaT);
			_mGunFire.Simulate(gunPos,deltaT);
		}
	}

	base::Simulate(deltaT,prec);
}

Vector3 Airplane::GetEyeDirection() const
{
  // watch nearest known enemy
  AIUnit *unit = CommanderUnit();
  if (!unit) return base::GetEyeDirection();
  if (unit!=PilotUnit()) return base::GetEyeDirection();
  if (QIsManual(unit)) return base::GetEyeDirection();
  Person *person = unit->GetPerson();
  // if there is any target assigned, track it
  Target *tgt = _fire._fireTarget;
  if (!tgt) tgt = unit->GetTargetAssigned();
  if (!tgt) return base::GetEyeDirection();
  Vector3 tgtPos = tgt->LandAimingPosition();
  Vector3 relDir = PositionWorldToModel(tgtPos);
  // if target is forward, look forward
  // if (fabs(relDir.X())<relDir.Z()*0.5f) return base::GetEyeDirection();
  float heading = atan2(relDir.X(),relDir.Z());
  float dive = 0;
  float fov = 1;
  // apply head constraints
	Type()->_viewPilot.LimitVirtual(CamInternal,heading,dive,fov);
  Matrix3 rotHeading(MRotationY,-heading);
  Vector3 dir = rotHeading.Direction();
	Vector3 wDir = DirectionModelToWorld(dir);
  if (person)
  {
    //Matrix4P invTrans = person->WorldTransform().InverseRotation();
    // our model space is worldspace of the person
    //Vector3 personDir = invTrans.Rotate(wDir);
    person->AimObserver(dir);  
  }
  return wDir;
}

bool Airplane::IsContinuous(CameraType camType ) const
{
  return false; // GInput.lookAroundEnabled;
}

bool Airplane::Airborne() const {return !_landContact;}

bool AirplaneAuto::IsAway( float factor )
{
	// airplane is never away
	return false;
}

void AirplaneAuto::EngineOn()
{
	base::EngineOn();
}
void AirplaneAuto::EngineOff()
{
	_thrustWanted=0;
	base::EngineOff();
}

float Airplane::GetEngineVol( float &freq ) const
{
	freq=_rndFrequency*(_thrust+0.5)*_rpm;
	return (_thrust+0.5);
}

float Airplane::GetEnvironVol( float &freq ) const
{
	freq=1;
	return _speed.Size()/Type()->GetMaxSpeedMs();
}

void Airplane::Sound( bool inside, float deltaT )
{
	if( _servoVol>0.001 )
	{
		const SoundPars &pars=Type()->GetServoSound();
		if( !_servoSound )
		{
			_servoSound=GSoundScene->OpenAndPlay
			(
				pars.name,Position(),Speed()
			);
		}
		if( _servoSound )
		{
			float vol=pars.vol*_servoVol;
			float freq=pars.freq;
			_servoSound->SetVolume(vol,freq); // volume, frequency
			_servoSound->Set3D(!inside);
			_servoSound->SetPosition(Position(),Speed());
		}
	}
	else
	{
		_servoSound.Free();
	}

	base::Sound(inside,deltaT);
}

void Airplane::UnloadSound()
{
	base::UnloadSound();
}

Matrix4 Airplane::InsideCamera( CameraType camType ) const
{
	return base::InsideCamera(camType);
}

int Airplane::InsideLOD( CameraType camType ) const
{
	return base::InsideLOD(camType);
}

void Airplane::InitVirtual( CameraType camType, float &heading, float &dive, float &fov ) const
{
	base::InitVirtual(camType,heading,dive,fov);
}

void Airplane::LimitVirtual( CameraType camType, float &heading, float &dive, float &fov ) const
{
	base::LimitVirtual(camType,heading,dive,fov);
}

bool Airplane::IsAnimated( int level ) const {return true;}
bool Airplane::IsAnimatedShadow( int level ) const {return true;}

void Airplane::Animate( int level )
{
	Type()->_lFlap.Rotate(_shape,-_flaps*0.5,level);
	Type()->_rFlap.Rotate(_shape,+_flaps*0.5,level);
	Type()->_lElevator.Rotate(_shape,-_elevator*0.5,level);
	Type()->_rElevator.Rotate(_shape,-_elevator*0.5,level);
	float abOff=_brake*1.5;
	float tAileron=floatMin(_aileron*0.5+abOff,+1.4);
	float bAileron=floatMax(_aileron*0.5-abOff,-1.4);
	Type()->_lAileronT.Rotate(_shape,tAileron,level);
	Type()->_lAileronB.Rotate(_shape,bAileron,level);
	Type()->_rAileronT.Rotate(_shape,bAileron,level);
	Type()->_rAileronB.Rotate(_shape,tAileron,level);
	Type()->_lRudder.Rotate(_shape,-_rudder*0.4,level);
	Type()->_rRudder.Rotate(_shape,-_rudder*0.4,level);
	for (int i=0; i<Type()->MaxRotors; i++)
	{
		Type()->_rotors[i].Rotate(_shape,_rotorPosition,level);
	}
	if (_rotorSpeed < 1.0)
	{
		Type()->_rotorMove.Hide(_shape, level);
	}
	else
	{
		Type()->_rotorStill.Hide(_shape, level);
	}

	Matrix4 fGear(MRotationX,-_gearsUp*(H_PI*0.58));
	Type()->_fWheel.Apply(_shape,fGear,level);
	Matrix4 bGear(MRotationX,-_gearsUp*(H_PI*0.45));
	Type()->_rWheel.Apply(_shape,bGear,level);
	Type()->_lWheel.Apply(_shape,bGear,level);

	if (_mGunFireFrames > 0 || Glob.uiTime < _mGunFireTime + 0.05)
	{
		Type()->_animFire.Unhide(_shape, level);
		Type()->_animFire.SetPhase(_shape, level, _mGunFirePhase);
		_mGunFireFrames--;
	}
	else
	{
		Type()->_animFire.Hide(_shape, level);
	}

	// indicators
	float value = fastFmod(Position().Y(), 304);
	Type()->_altRadarIndicator.SetValue(_shape, level, value);
	Type()->_altRadarIndicator2.SetValue(_shape, level, value);
	value = Position().Y() - GLandscape->SurfaceYAboveWater(Position().X(), Position().Z());
	Type()->_altBaroIndicator.SetValue(_shape, level, value);
	value = fabs(ModelSpeed()[2]);
	Type()->_speedIndicator.SetValue(_shape, level, value);
	value = _speed.Y();
	Type()->_vertSpeedIndicator.SetValue(_shape, level, value);
	Type()->_vertSpeedIndicator2.SetValue(_shape, level, value);
	value = _rpm * _thrust;
	Type()->_rpmIndicator.SetValue(_shape, level, value);
	value = atan2(Direction().X(), Direction().Z());
	Type()->_compass.SetValue(_shape, level, value);
	Type()->_compass2.SetValue(_shape, level, value);
	Type()->_watch.SetTime(_shape, level, Glob.clock);
	Type()->_watch2.SetTime(_shape, level, Glob.clock);
	
	// vario
	{
		Vector3Val dir = Type()->_varioDirection[level];
		// transformations from / into vertical plane
		Matrix3 rot(MDirection, dir, VUp);
		Matrix3 invRot(MInverseRotation, rot);
		
		Vector3 up = InvTransform().DirectionUp();
		up[2] = -up[2];

		// transform into vertical plane
		up = invRot * up;
		Matrix3 orient(MUpAndDirection, up, VForward);
		// back into vario plane
		orient = rot * orient;

		Matrix4 trans;
		trans.SetPosition(VZero);
		trans.SetOrientation(orient);
		Type()->_vario.Apply(_shape, trans, level);
	}

	base::Animate(level);
}

void Airplane::Deanimate( int level )
{
	Type()->_lFlap.Restore(_shape,level);
	Type()->_rFlap.Restore(_shape,level);
	Type()->_lElevator.Restore(_shape,level);
	Type()->_rElevator.Restore(_shape,level);
	Type()->_lAileronT.Restore(_shape,level);
	Type()->_rAileronT.Restore(_shape,level);
	Type()->_lAileronB.Restore(_shape,level);
	Type()->_rAileronB.Restore(_shape,level);
	Type()->_lRudder.Restore(_shape,level);
	Type()->_rRudder.Restore(_shape,level);
	for (int i=0; i<Type()->MaxRotors; i++)
	{
		Type()->_rotors[i].Restore(_shape,level);
	}

	Type()->_rotorStill.Unhide(_shape, level);
	Type()->_rotorMove.Unhide(_shape, level);

	Type()->_fWheel.Restore(_shape,level);
	Type()->_lWheel.Restore(_shape,level);
	Type()->_rWheel.Restore(_shape,level);
	base::Deanimate(level);
}


// basic autopilot

AirplaneAuto::AirplaneAuto( VehicleType *name, Person *pilot )
:Airplane(name,pilot),_pilotHeading(0),_pilotSpeed(0),_pilotHeight(0),_defPilotHeight(100),

_pilotAvoidHigh(TIME_MIN),
_pilotAvoidHighHeight(0),
_pilotAvoidLow(TIME_MIN),
_pilotAvoidLowHeight(100000),

_dirCompensate(0.5),
_state(AutopilotNear),
_pressedForward(false),_pressedBack(false),
_pilotBank(0),_pilotDive(0),
_pilotHelperHeight(true),_pilotHelperDir(true),
_pilotHelperThrust(true),_pilotHelperBankDive(true),
_forceDive(1),
_pilotHeadingSet(false),_pilotDiveSet(false),

_targetOutOfAim(false),
_planeState(TaxiIn) // get ready for take-off
{
}


/*!
\patch 1.16 Date 8/13/2001 by Ondra.
- Fixed: Landing autopilot now work for all planes both on Everon and Malden.
\patch 1.33 Date 11/29/2001 by Ondra.
- Fixed: Dead AI pilot was sometimes able to continue flying.
\patch 1.42 Date 1/10/2002 by Ondra
- Fixed: Cessna AI pilot swinging up and down when flying slow.
\patch 1.43 Date 1/25/2002 by Ondra
- Fixed: Cessna flight model and AI pilot improved.
(Thanks to WKK Gimbal for a suggestion how to fix it.)
\patch 1.44 Date 2/12/2002 by Ondra
- Fixed: Airplane turned engine off on bumpy surface during takeoff,
making takeoff with joystick impossible outside the airport.
*/



void AirplaneAuto::Simulate( float deltaT, SimulationImportance prec )
{
	// use current pilot
	SimulateUnits(deltaT);

	// autopilot: convert simple _pilot control
	// to advanced simulation model
	if (!_isDead)
	{
		if (PilotUnit() && PilotUnit()->GetLifeState()==AIUnit::LSAlive)
		{
			// force gear when landed
			if( _planeState==Flight && _landContact && DirectionUp().Y()>0.5 )
			{
				if (!QIsManual())
				{
					_planeState=TaxiOff;
				}
        else
        {
          _planeState=Takeoff;
        }
			}
		
			float surfaceY=GLOB_LAND->SurfaceYAboveWater(Position().X(),Position().Z());

			//if( _pilotHelper )
			{
				bool setControls = _pilotHelperBankDive;

        float aoa = CalculateAOA(ModelSpeed());
				float height=Position().Y()-surfaceY;
				// TODO: find nearest airport?

				Vector3 ilsPos,ilsDir;
				GWorld->GetILS(ilsPos,ilsDir,_targetSide);
				// use acceleration to estimate change of position
				if (_pilotHelperThrust)
				{
					#define EST_DELTA 2.0f
					// estimate vertical acceleration
					float relSpeed=ModelSpeed().Z();
					float speedWanted=_pilotSpeed;
					Vector3 accel=DirectionWorldToModel(_acceleration);
					float changeAccel=(speedWanted-relSpeed)*(1/EST_DELTA)-accel[2];

					if (fabs(speedWanted)<2)
					{
						_thrustWanted=0;
						_pilotBrake=1;
					}
					else if (relSpeed-speedWanted>5 )
					{
						_thrustWanted=0;
						_pilotBrake=(relSpeed-speedWanted)*0.1f;
						saturate(_pilotBrake,0,1);
					}
					else
					{
						_thrustWanted=changeAccel*0.1f+_thrust;
						_pilotBrake=0;
					}
				}

				// get simple aproximations of bank and dive
				// we must consider current angular velocity
				Matrix3Val orientation=Orientation();

				const float dirEstT = _dirCompensate;
				Matrix3Val derOrientation=_angVelocity.Tilda()*orientation;
				Matrix3Val estOrientation=orientation+derOrientation*dirEstT;
				//float lastBank=orientation.DirectionAside().Y();
				Vector3Val estDirection=estOrientation.Direction().Normalized();

				float curHeading=atan2(estDirection[0],estDirection[2]);
				float changeHeading=AngleDifference(_pilotHeading,curHeading);
				#if 0
					float curRHeading=atan2(Direction()[0],Direction()[2]);
					LogF("  est chh %.2f",changeHeading);
					LogF("  raw chh %.2f",AngleDifference(_pilotHeading,curRHeading));
				#endif

				// scan surface before you
				const float estT=2.0f;
				const int nSamples = 4;
				Vector3 step = _speed*0.5f;

				Vector3 estPos=Position()+_speed*estT+_acceleration*estT*estT*0.5;

				float estSurfY = GLOB_LAND->SurfaceYAboveWater(estPos.X(),estPos.Z());
				float maxSurfaceY = estSurfY;

				Vector3 tPos = Position();
				for (int s=nSamples; --s>=0;)
				{
					const float surfY = GLOB_LAND->SurfaceYAboveWater(tPos.X(),tPos.Z());
					saturateMax(maxSurfaceY,surfY);	
					tPos += step;
				}

				//GlobalShowMessage(100,"max %.0f, y %.0f",maxSurfaceY,estSurfY);


				float estHeight=estPos.Y()-maxSurfaceY;

				//float minHeight=ModelSpeed()[2];
				float minHeight=ModelSpeed()[2]*0.5f+10;
				saturateMin(minHeight,20);
				saturateMax(_pilotHeight,minHeight);
				float diveWanted=0;
				bool diveWantedSet=false;
				// turn using bank

		
				// landing speed: 200 km/h for 600 km/h max (A10)
				// landing speed: 100 km/h for 300 km/h max and less (Cessna)

				float landingSpeed = Type()->_landingSpeed;
				float stallSpeed = Type()->_stallSpeed;

				bool elevatorSet=false;
				if( _planeState==Marshall )
				{
					
					// fly to marshal point
					Vector3 approachPos=ilsPos+ilsDir*3000+Vector3(650,0,0);
					Vector3 relPos=approachPos-Position();
					_pilotHeading=atan2(relPos.X(),relPos.Z());
					changeHeading=AngleDifference(_pilotHeading,curHeading);
					Limit(changeHeading,-0.7f,+0.7f);
					_pilotHeight=100;
					_pilotSpeed=landingSpeed*2;
					if( relPos.SquareSize()<Square(500) ) _planeState=Approach;
					setControls = true;
					_rudderWanted = 0;
				}
				else if( _planeState==Approach )
				{
					// fly to marshal point
					Vector3 approachPos=ilsPos+ilsDir*2000+Vector3(90,0,0);
					Vector3 relPos=approachPos-Position();
					_pilotHeading=atan2(relPos.X(),relPos.Z());
					changeHeading=AngleDifference(_pilotHeading,curHeading);
					Limit(changeHeading,-0.7f,+0.7f);
					_pilotHeight=100;
					_pilotSpeed=landingSpeed*1.5f;
					if( relPos.SquareSize()<Square(500) )
					{
						_pilotSpeed=landingSpeed*1.2f;
					}
					if( relPos.SquareSize()<Square(200) )
					{
						_planeState=Final;
					}
					setControls = true;
					_rudderWanted = 0;
				}
				else if( _planeState==Final )
				{
					// fly to final approach
					Vector3 finalPos=ilsPos+ilsDir*1000;
					Vector3 relPos=finalPos-Position();
					_pilotHeading=atan2(relPos.X(),relPos.Z());
					changeHeading=AngleDifference(_pilotHeading,curHeading);
					Limit(changeHeading,-0.5f,+0.5f);
					_pilotHeight=92;

					_pilotSpeed=landingSpeed*1.0f;
					if( relPos.SquareSize()<Square(200) )
					{
						if (ModelSpeed()[2]<landingSpeed*1.5f && fabs(Direction().X())<H_PI/4)
						{
							_planeState=Landing;
						}
						else _planeState=Marshall;
					}
					setControls = true;
					_rudderWanted = 0;
				}
				else if (_planeState==Landing || _planeState==Touchdown)
				{
					_pilotHelperHeight = false;
					_pilotHelperThrust = true;
					_pilotHelperBankDive = true;
					// desired aoa is reached
					_pilotFlaps=2;
					_pilotGear=true;
					_pilotSpeed=landingSpeed;
					if( !_landContact )
					{
						// check if we are in ils corridor
						float distance = (Position()-ilsPos)*ilsDir; //.Distance(ilsPos);
						Vector3 ilsCPos=ilsPos+Vector3(0,1.5f,0)+ilsDir*distance;
						Vector3 relPos=ilsPos-Position();
						if( _planeState==Touchdown )
						{
							// follow runway
							float y=ilsCPos[1];
							ilsCPos=ilsPos-ilsDir*500;
							ilsCPos[1]=y;
							relPos=ilsCPos-Position();
						}
						_pilotHeading=atan2(relPos.X(),relPos.Z());
						changeHeading=AngleDifference(_pilotHeading,curHeading);
						Limit(changeHeading,-0.2f,+0.2f);
						// check position in ils corridor
						float touchdownY=ilsPos[1];
						float ilsY=ilsCPos.Y()-touchdownY;
						saturateMin(ilsY,100);
						ilsY+=touchdownY;
						float above=Position().Y()-ilsY-2.0f;
						if (distance<200)
						{
							if (Position().Y()-touchdownY>40)
							{
								_planeState=WaveOff;
								LogF("WaveOff far: too high %.1f",Position().Y()-touchdownY);
							}
							else if (fabs(relPos.X()>25) || fabs(Direction().X())>0.15f)
							{
								_planeState=WaveOff;
								LogF("WaveOff far: pos.x %.1f, dir.x %.4f",relPos.X(),Direction().X());
							}
							else if( _planeState!=Touchdown && Position().Y()-touchdownY<15)
							{
								// check if we are aligned
								if( fabs(relPos.X()<13) && fabs(Direction().X())<0.06 ) _planeState=Touchdown;
								else
								{
									_planeState=WaveOff;
									LogF("WaveOff: pos.x %.1f, dir.x %.4f",relPos.X(),Direction().X());
								}
							}
						}
						// special handling of landing condition
						// calculate aoa
						// keep AoA at 13
						// estimate descent rate
						//const float estT=2.0;
						const float estT = 3.0f;
						float estDescent=-_speed[1]-_acceleration[1]*estT;
						// control descent to keep in ils
						// keep descent rate at 3 m/s
						// descent rate should be set so that at current velocity
						// we would descent in direction given by -ilsDir
						const float landDescent = ilsDir[1]*_speed.SizeXZ();
						//const float landDescent = 2.7;
						float descent=landDescent+above*0.5f;
						// keep AoA constant
						// use thrust to control AoA
						float aoaWanted = Type()->_landingAoa;
						_thrustWanted =_thrust+(aoa-aoaWanted)*6.0f;
						// use dive to control descent rate


						// keep constant dive
						diveWanted=6.5f*(H_PI/180);
						diveWantedSet=true;
						float maxDescent = landDescent*1.25f;
						if( _planeState==Touchdown )
						{
							const float normDescent = ilsDir[1]*landingSpeed;
							Limit(descent,normDescent*0.25f,normDescent*0.5f);
							Limit(changeHeading,-0.05f,+0.05f);
						}
						else
						{
							Limit(descent,0,maxDescent);
						}
						float dive= Direction().Y();
						diveWanted = dive - (descent-estDescent)*0.05f;
						saturate(diveWanted,-0.2f,+0.15f);
						#if _DEBUG
						GlobalShowMessage
						(
							100,
							"dive %.2f->%.2f, descent %.2f, est %.2f",
							dive,diveWanted,descent,estDescent
						);
						#endif
						// final phase of touch-down
						if (_planeState==Touchdown && Position().Y()-touchdownY<4)
						{
							// apply brake just when touching ground
							_thrustWanted = -1;
						}
						// apply air brake when necessary
						if (_thrustWanted<0) _pilotBrake = -_thrustWanted;
						else _pilotBrake = 0;

						// LANDING DIAG
						#if 0
						LogF
						(
							"Descent %.2f (cur %.2f, est %.2f, land %.2f), above %.2f, t %.2f->%.2f, dive %.2f"
							"Speed %.2f (land %.2f,%.2f)",
							descent,-_speed[1],estDescent,landDescent,above,_thrust,_thrustWanted,diveWanted,
							ModelSpeed().Z(),landingSpeed,stallSpeed
						);
						#endif

						#if 0
						const char *state="???";
						switch( _planeState )
						{
							case Flight: state="Flight";break;
							case Takeoff: state="Takeoff";break;
							case TaxiIn: state="TaxiIn";break;
							case Marshall: state="Marshall";break;
							case Approach: state="Approach";break;
							case Final: state="Final";break;
							case Landing: state="Landing";break;
							case Touchdown: state="Touchdown";break;
							case WaveOff: state="WaveOff";break;
							case TaxiOff: state="TaxiOff";break;
						}
						GlobalShowMessage
						(
							100,
							"%s: Dive %.2f->%.2f, surfY %.1f"
							"descent %.1f->%.1f (norm %.1f, above %.1f), thrust %.2f->%.2f",
							state,Direction().Y()*(180/H_PI),diveWanted*(180/H_PI),
							Position().Y()-touchdownY,
							estDescent,descent,landDescent,above,
							_thrust,_thrustWanted
						);
						#endif
					}
					else
					{
						// on the ground
						Vector3 ilsCPos=ilsPos-ilsDir*700;
						Vector3 relPos=ilsCPos-Position();
						_pilotHeading=atan2(relPos.X(),relPos.Z());
						changeHeading=AngleDifference(_pilotHeading,curHeading);
						Limit(changeHeading,-0.2,+0.2);
						_thrustWanted=0;
						_pilotSpeed=0;
						_pilotBrake=1;
						diveWanted=0;
						diveWantedSet=true;
						if( fabs(ModelSpeed()[2]<20) )
						{
							_pilotFlaps=0;
							// wait until you come to the right point of runway to taxi-off
							_planeState=TaxiOff;

						}
					}
					setControls = true;
					_rudderWanted = 0;
				}
				else if( _planeState==WaveOff )
				{
					_pilotHeight=fabs(ModelSpeed()[2])*1.5-30;
					saturateMax(_pilotHeight,10);
					_pilotSpeed=50;
					Vector3 ilsCPos=ilsPos-ilsDir*700;
					Vector3 relPos=ilsCPos-Position();
					_pilotHeading=atan2(relPos.X(),relPos.Z());
					changeHeading=AngleDifference(_pilotHeading,curHeading);
					Limit(changeHeading,-0.5,+0.5);
					if( relPos*ilsDir>0 ) _planeState=Marshall;
					setControls = true;
					_rudderWanted = 0;
				}
				else if (_planeState==TaxiOff)
				{
					// move out of runway
					// handle random touch down
					if (!_landContact && ModelSpeed()[2]>60)
					{
						_planeState = Flight;
					}
					else
					{
						// use path autopilot
						if (TaxiOffAutopilot())
						{
							EngineOff();
							_planeState = TaxiIn;
						}

						changeHeading=AngleDifference(_pilotHeading,curHeading);
						Limit(changeHeading,-0.4,+0.4);
						_elevatorWanted = 0;
						elevatorSet=true;
					}
					setControls = true;
					_rudderWanted = 0;
				}
				else if (_planeState==TaxiIn)
				{
					if (!_landContact && ModelSpeed()[2]>60)
					{
						_planeState = Flight;
					}
					else
					{
						// use path autopilot
						if (TaxiInAutopilot())
						{
							_planeState = Takeoff;
						}

						changeHeading=AngleDifference(_pilotHeading,curHeading);
						Limit(changeHeading,-0.4,+0.4);
						elevatorSet=true;
					}
					setControls = true;
					_rudderWanted = 0;
				}

				saturateMin(_pilotSpeed,GetType()->GetMaxSpeedMs()*1.5f);

				float bankWanted = 0;
				if (_pilotHelperDir)
				{
					float bankFactor = 1;
					if (ModelSpeed().Z()<landingSpeed*2)
					{
						float slow = (landingSpeed*2-ModelSpeed().Z())/landingSpeed;
						// slow is 0 at zSpeed == landingSpeed*2
						// slow is 1 at zSpeed == landingSpeed
						saturate(slow,0,1);
						bankFactor = 1+slow;
					}
					bankWanted = -changeHeading*4*bankFactor;
					// to change heading some minimal bank is needed
					if (!QIsManual() && _sweepTarget)
					{
						const float bankTurningWell = 0.5f;
						if (fabs(bankWanted)<bankTurningWell)
						{
							float changeFactor = floatMin(10,fabs(changeHeading)*300+1);
							/*
							LogF
							(
								"bankWanted %.3f, changeHeading %.3f, changeFactor %.3f",
								bankWanted,changeHeading,changeFactor
							);
							*/
							bankWanted *= changeFactor;
							saturate(bankWanted,-bankTurningWell,+bankTurningWell);
						}
					}
					
				}
				else
				{
					bankWanted = _pilotBank;
				}
				if (_pilotHelperHeight)
				{
					if (ModelSpeed().Z()<stallSpeed*2)
					{
						float fast = (ModelSpeed().Z()-stallSpeed)/stallSpeed;
						saturate(fast,0,1);
						float maxBank = fast*0.5+0.4;
						saturate(bankWanted,-maxBank,+maxBank);
					}
				}
				saturate(bankWanted,-0.9,0.9);

				float bank=estOrientation.DirectionAside().Y();
				float dive=estOrientation.Direction().Y();

				// no ailerons until full takeoff
				float maxAilerons = 1;

				if( _planeState==Takeoff )
				{
					// special handling of take off condition
					// leave flaps setting as it is
          if (!_pilotHelperDir)
          {
            if (!_landContact)
            {
              if (!QIsManual() || height>=2)
              {
                _planeState = Flight;
              }
            }
          }
					if (!QIsManual())
					{
						const float takeOffSpeed = Type()->_takeOffSpeed;
						float actDive = Direction().Y();
						if( ModelSpeed()[2]>=takeOffSpeed )
						{
							float diveWanted=9*H_PI/180;
							elevatorSet=true;
							_elevatorWanted=(actDive-diveWanted)*10;
							if (!_landContact && height>=10)
							{
								maxAilerons = 0.3f;
							}
							else
							{
								maxAilerons = 0;
							}
						}
						else if (ModelSpeed()[2]>=takeOffSpeed*0.66f)
						{
							float diveWanted=0*H_PI/180;
							if (ModelSpeed()[2]>=takeOffSpeed*0.8f)
							{
								diveWanted=7*H_PI/180;
							}
							elevatorSet=true;
							_elevatorWanted=(actDive-diveWanted)*10;
							maxAilerons = 0;
						}
						else
						{
							elevatorSet=true;
							_elevatorWanted=0;
						}
					}

					if (!_landContact && height>=10)
					{
						_pilotGear=false;
						if (!_landContact && height>=30)
						{
							_planeState=Flight;
							if (!QIsManual())
							{
								_pilotHeight=height+5;
								_pilotFlaps=0;
							}
						}
					}
				}

				// calculate flap efficiency
				float flapEff=ModelSpeed()[2]*(1.0/40);
				saturate(flapEff,0.1,1);
				float invFlapEff=1/flapEff;
				
				if (setControls)
				{
					if (_pilotHelperDir)
					{
						_rudderWanted =- changeHeading*invFlapEff;
					}
					if (fabs(changeHeading)>H_PI/4 && ModelSpeed().Z()<_pilotSpeed+20)
					{
						// do not brake in turns
						// we will loose enough energy without braking
						_pilotBrake = 0;
					}
				}

				// trim when flying slow
				if (setControls && !elevatorSet)
				{

					if( !diveWantedSet )
					{

						if( fabs(_forceDive)<0.99 )
						{
							// forced dive (attacking etc)
							diveWanted=_forceDive;
						}
						else
						{
							if (_pilotHelperHeight)
							{
								float diveChange = _pilotHeight-estHeight;

								float diveFactor = 0.5f;
								if (ModelSpeed().Z()>1)
								{
									diveFactor /= ModelSpeed().Z();
								}


								// when 
								diveWanted = dive+diveChange*diveFactor;

								// positive dive is up

								float limitDive = floatMin(fabs(bank)*1.4-1,0);
								saturateMax(diveWanted,limitDive);

								float zSpeed = ModelSpeed().Z();
								if (zSpeed<landingSpeed*1.15)
								{
									_pilotFlaps = 2;
								}
								else if (zSpeed>landingSpeed*1.2 && zSpeed<landingSpeed*1.4)
								{
									_pilotFlaps = 1;
								}
								else if (zSpeed>landingSpeed*1.5)
								{
									_pilotFlaps = 0;
									_pilotGear = false;
								}

								//max dive / bank depending on AoA (stall recovery)

								if (!_landContact && !_waterContact && !_objectContact)
								{
									// detect actual stall
									float stall = 0;
									const float maxNormalAoa = 5*H_PI/180;
									const float maxStallAoa = 30*H_PI/180;
									if(aoa>maxNormalAoa)
									{
										saturateMax(stall, (aoa-maxNormalAoa)*(1.0/(maxStallAoa-maxNormalAoa)));
									}
									if (ModelSpeed().Z()<stallSpeed*2)
									{
										// detect stall possibility due to low speed
										saturateMax(stall,(stallSpeed*2-ModelSpeed().Z())/stallSpeed);
									}
									saturate(stall,0,1);
									/*
									if (stall>0 && GLOB_WORLD->CameraOn()==this)
									{
										GlobalShowMessage(100,"Stall %.2f",stall);
									}
									*/
									if (_planeState!=Final)
									{
										saturateMax(_thrustWanted,stall);
									}
									// patch: limit turning in slow planes like cessna
									// if in combat, more aggressive maneuvers are enabled
									float maxBank = 0.7f*(1-stall)+0.2f;
									if (GetType()->GetMaxSpeed()<450 && !_sweepTarget)
									{
										saturateMin(maxBank,0.7f);
									}
									float maxDive = 0.7f*(1-stall);
									Limit(bankWanted,-maxBank,+maxBank);
									Limit(diveWanted,-0.7f,maxDive);
									//Limit(diveWanted,-0.7f,maxDive);

									if (stall>0.3f)
									{
										_pilotBrake=0;
									}
								}
							}
							else
							{
								diveWanted = _pilotDive;
							}
						}
						Limit(diveWanted,-0.7,+0.7);

					}
					float bankElevator = floatMax(0,fabs(bank)-0.4);
					// when making small adjustments
					// we need to make elevator movements stronger
					float elevChange = (diveWanted-dive)*invFlapEff*4;
					if (!QIsManual() && _sweepTarget)
					{
						const float diveEnhancerMax = 0.1f;
						if (fabs(elevChange)<diveEnhancerMax)
						{
							float factor = floatMin(10,fabs(elevChange)*300+1);
							/*
							LogF
							(
								"diveWanted %.3f, dive %.3f, elevChange %.3f, factor %.3f",
								diveWanted,dive,elevChange,factor
							);
							*/
							elevChange *= factor;
							saturate(elevChange,-diveEnhancerMax,+diveEnhancerMax);
						}
					}

					_elevatorWanted = -elevChange - bankElevator;
				} // pilot helper full
				#if 0
				LogF
				(
					" bank %.2f->%.2f, dive %.2f->%.2f, rudderW %.2f, elevW %.2f",
					bank,bankWanted,dive,diveWanted,_rudderWanted,_elevatorWanted
				);
				#endif
				// if plane is lower that wanted, limit dive to enable climbing
				if (setControls)
				{
					if (_pilotHelperHeight)
					{
						const float loSafeHeight = _defPilotHeight;
						const float hiSafeHeight = _defPilotHeight+100;
						const float loSafeFactor = 10;
						const float hiSafeFactor = 40;
						float heightScale = loSafeFactor;
						if (height>hiSafeHeight) heightScale = hiSafeFactor;
						else if (height<loSafeHeight) heightScale = loSafeFactor;
						else
						{
							float factor = (height-loSafeFactor)*1/(hiSafeFactor-loSafeFactor);
							heightScale = factor*hiSafeFactor + loSafeFactor - factor*loSafeFactor;
						}
						float lowWarning=(_pilotHeight-height)/heightScale;
						float heightSafe=1-lowWarning*0.5f;
						saturate(heightSafe,0.1f,1);
						Limit(bankWanted,-0.9f*heightSafe,+0.9f*heightSafe);
					}
					_aileronWanted = +(bankWanted-bank)*2;

					if( _angVelocity.SquareSize()>=10*10 )
					{ // if we are spinning fast, leave all controls in neutral position
						_rudderWanted=0;
						_aileronWanted=0;
						_elevatorWanted=0;
						_thrustWanted=0.5;
					}
				}
				Limit(_aileronWanted,-maxAilerons,+maxAilerons);

				

				#if _ENABLE_CHEATS
				if (CHECK_DIAG(DECombat) && GLOB_WORLD->CameraOn()==this )
				{
					const char *state="???";
					switch( _planeState )
					{
						case Flight: state="Flight";break;
						case Takeoff: state="Takeoff";break;
						case TaxiIn: state="TaxiIn";break;
						case Marshall: state="Marshall";break;
						case Approach: state="Approach";break;
						case Final: state="Final";break;
						case Landing: state="Landing";break;
						case Touchdown: state="Touchdown";break;
						case WaveOff: state="WaveOff";break;
						case TaxiOff: state="TaxiOff";break;
					}
					/*
					GlobalShowMessage
					(
						100,
						"%s: AoA %.1f, v=%.0f, dive=%.2f->%.2f, thr %.2f->%.2f, bank %.2f->%.2f",
						state,aoa*(180/H_PI),ModelSpeed()[2]*3.6,
						dive,diveWanted,
						_thrust,_thrustWanted,bank,bankWanted
					);
					*/
					GlobalShowMessage
					(
						100,
						"AoA %.1f, dive=%.2f->%.2f, elev %.2f->%.2f, thr %.2f, height %.2f",
						aoa*(180/H_PI),dive,diveWanted,_elevator,_elevatorWanted,
						_thrust,_pilotHeight
					);
				}
				#endif
			}

		}
		else 
		{
			_rudderWanted=0.1;
			_aileronWanted=0.1;
			_elevatorWanted=0;
			_thrustWanted=0.3;
		}
	}
	
	saturate(_thrustWanted,0,1);

	MoveWeapons(deltaT);

	// perform advanced simulation
	base::Simulate(deltaT,prec);
}


bool AirplaneAuto::AimWeapon( int weapon, Vector3Par direction )
{
	if (weapon < 0)
	{
		if (NMagazineSlots() <= 0) return false;
		weapon = 0;
	}
	SelectWeapon(weapon);

	// always aim gun - to be ready if needed
	// move turret/gun accordingly to direction
	Vector3 relDir(VMultiply,DirWorldToModel(),direction);
	// calculate current gun direction
	// compensate for neutral gun position
	_gunYRotWanted=-atan2(relDir.X(),relDir.Z());
	//float neutralXRot=atan2(_missileDir.Y(),_missileDir.Z());
	float sizeXZ=sqrt(Square(relDir.X())+Square(relDir.Z()));
	//const WeaponInfo &info=GetWeapon(weapon);
	_gunXRotWanted=atan2(relDir.Y(),sizeXZ); //-Type()->_neutralGunXRot;

	float gunXRotWanted=_gunXRotWanted;
	float gunYRotWanted=_gunYRotWanted;

	Limit(_gunXRotWanted,Type()->_minGunElev,Type()->_maxGunElev);
	Limit(_gunYRotWanted,Type()->_minGunTurn,Type()->_maxGunTurn);
	float xOffRange=fabs(gunXRotWanted-_gunXRotWanted);
	float yOffRange=fabs(gunYRotWanted-_gunYRotWanted);
	float xToAim=fabs(_gunXRotWanted-_gunXRot);
	float yToAim=fabs(_gunYRotWanted-_gunYRot);
	if( xToAim+yToAim>1e-6 ) CancelStop(); // enable simulation

	const WeaponModeType *mode = GetWeaponMode(weapon);
	if (!mode) return false;
	if (!mode->_ammo) return false;
	if (mode->_ammo->_simulation == AmmoShotMissile ) return true;
	_targetOutOfAim=false;
	if( xOffRange+yOffRange<0.001 ) return true;
	_targetOutOfAim=true;
	return false;
}

bool AirplaneAuto::CalculateAimWeapon( int weapon, Vector3 &dir, Target *target )
{
	if (weapon < 0)
	{
		if (NMagazineSlots() <= 0) return false;
		weapon = 0;
	}
	_fire.SetTarget(CommanderUnit(),target);
	Vector3 tgtPos=target->AimingPosition();
	Vector3 weaponPos=Type()->_gunPos;

	const WeaponModeType *mode = GetWeaponMode(weapon);
	const Magazine *magazine = GetMagazineSlot(weapon)._magazine;
	const MagazineType *aInfo = magazine ? magazine->_type : NULL;
	if (mode && mode->_ammo && mode->_ammo->_simulation != AmmoShotMissile)
	{
		// calculate gun balistics
		float dist2=tgtPos.Distance2(Position());
		float time2 = 0;
		if (aInfo)
    {
      // depending on the relative speed the travel time may be shorter
      Vector3 relSpeed = DirectionWorldToModel(Speed()-target->speed);
      time2 = dist2 / Square(aInfo->_initSpeed+relSpeed.Z());
    }
		float time=sqrt(time2);
		// calculate balistics
		float fall=0.5*G_CONST*time2;
		tgtPos[1]+=fall; // consider balistics
		tgtPos+=(target->speed-Speed())*(time+0.25);
	}

	const float predTime=0.1;
	Vector3 myPos=PositionModelToWorld(weaponPos);
	tgtPos+=target->speed*predTime;
	myPos+=Speed()*predTime;

	dir = tgtPos-myPos;
	return true;
}

bool AirplaneAuto::AimWeapon( int weapon, Target *target )
{
	Vector3 dir;
	if (!CalculateAimWeapon(weapon,dir,target)) return false;
	return AimWeapon(weapon,dir);
}

Matrix4 AirplaneAuto::GunTransform() const
{
	return MIdentity;
}

void AirplaneAuto::LimitCursor( CameraType camType, Vector3 &dir ) const
{
	/*
	if (camType==CamInternal || camType==CamExternal)
	{
		// use coordinate system given by dir / up
		Matrix3 orient;
		orient.SetDirectionAndUp(dir,VUp);

		dir = orient * Type()->_gunDir;
	}
	*/
	//base::LimitCursor(camType,dir);
}

Vector3 AirplaneAuto::GetWeaponDirection( int weapon ) const
{
	Matrix4Val shootTrans=GunTransform();
	Vector3 wepDir = shootTrans.Rotate(Type()->_gunDir);

	return DirectionModelToWorld(wepDir);
}

/*!
\patch 1.33 Date 11/28/2001 by Ondra.
- Fixed: AI: Airplane laser guided bomb aiming improved.
\patch 1.42 Date 1/9/2002 by Ondra
- Fixed: Debugging message shown when AI preparing to release LG bomb.
*/

float AirplaneAuto::GetAimed( int weapon, Target *target ) const
{
/*
	// check if weapon is aimed
	return 1.0;
*/

	if( !target ) return 0;
	if( !target->idExact ) return 0;
	// check if weapon is aimed
	if( weapon<0 ) return 0;
	float visible=_visTracker.Value(this,_currentWeapon,target->idExact,0.9);
	const WeaponModeType *mode = GetWeaponMode(weapon);
	if (!mode || !mode->_ammo) return 0;

	const Magazine *magazine = GetMagazineSlot(weapon)._magazine;
	const MagazineType *aInfo = magazine ? magazine->_type : NULL;

	// 0.6 visibility means 0.8 unaimed
	visible=1-(1-visible)*0.5f;

	Vector3 ap=target->AimingPosition();

	const AmmoType *ammo = mode->_ammo;
	if (ammo->_simulation == AmmoShotMissile)
	{
		if (ammo->thrustTime>0)
		{
			// guided/non-guided missile	
			Vector3 relPos=PositionWorldToModel(ap);
			// check if target is in front of us
			if( relPos.Z()<=100 ) return 0; // missile fire impossible
			// check if target position is withing missile lock cone
			float rYPos=relPos.Y();
			if( fabs(relPos.X())>relPos.Z() ) return 0;
			if( fabs(rYPos)>relPos.Z() ) return 0;
			// the nearer we are, the more precise lock required
			float invRZ=0.5/relPos.Z();
			float lockX=1-fabs(relPos.X())*invRZ;
			float lockY=1-fabs(rYPos)*invRZ;
			float lock=floatMin(lockX,lockY);
			saturate(lock,0,1);
			lock*=visible;
			if( lock<0.5 ) lock=0;
			return lock;
		}
		else
		{
			// free fall bomb
			// predict where the bomb will fall
			// assume it is guided - larger error is acceptable

			Vector3 wPos = PositionModelToWorld(GetWeaponCenter(weapon));
			Vector3 wDir=GetWeaponDirection(weapon);

			// CCIP calculation
			// assume air friction decreased acceleration
			// weapon direction is not important here
			float a = G_CONST*0.9f;

			float dist = ap.Distance(wPos);

			// calculate time to impact to surface under us
			// assume surface is flat
			float surfaceY = GLandscape->SurfaceYAboveWater(Position().X(),Position().Z());
			float aboveSurface = Position().Y()-surfaceY;

			// d = v*t+0.5*a*t*t
			// special case: v=0: d = 0.5*a*t*t
			// d = 1/2*a*t*t
			// sqrt(d*2/a) = t

			float t = sqrt(2*aboveSurface/a);

			// assume no horizontal deceleration
			Vector3 hit=wPos + _speed*t - Vector3(0,a*0.5f,0)*t*t;
			Vector3 estPos = ap+target->speed*t;

			Vector3 hError=hit-estPos;
			#if _ENABLE_CHEATS
			if (CHECK_DIAG(DECombat) /*&& this==GWorld->CameraOn()*/)
			{
				GlobalShowMessage
				(
					100,"Time to impact %.2f, error %.1f (%.1f,%.1f,%.1f)",
					t,hError.Size(),hError[0],hError[1],hError[2]
				);
			}
			#endif
			/**/

			hError[1] *= 0.3f;

			float error = hError.Size();
			float aimPrecision=(GetInvAbility()-1)*1.5f+1;

			float tgtSize=target->idExact->GetShape()->GeometrySphere()*aimPrecision*0.25f;
			tgtSize += dist*aimPrecision*0.02f+ammo->indirectHitRange;

			// assume Laser guiding can correct a lot
			tgtSize += t*10;

			if( error<=tgtSize ) return visible;
			if( error>=tgtSize*2 ) return 0;
			return ( 2*tgtSize-error )/tgtSize*visible;

		}
	}
	else
	{
		// predict shot result
		Vector3 wPos = PositionModelToWorld(GetWeaponCenter(weapon));

		float dist=ap.Distance(wPos);

		float time=dist*aInfo->_invInitSpeed;
    Vector3 leadSpeed = target->speed-Speed();
		Vector3 estPos = ap+leadSpeed*time;

		Vector3 wDir = GetWeaponDirection(weapon);

		float eDist=estPos.Distance(wPos);

		Vector3 hit = wPos+wDir*eDist;
		hit[1] -= 0.5f*G_CONST*time*time;
		Vector3 hError=hit-estPos;
#if _ENABLE_CHEATS
		float distHit = hit.Distance(wPos);
#endif

		hError[1] *= 3;

		float error = hError.Size();
		float aimPrecision=(GetInvAbility()-1)*1.5f+1;

		float tgtSize =
		(
			target->idExact->GetShape()->GeometrySphere()*aimPrecision*0.25f
			+ ammo->indirectHitRange*3
		);
		const MuzzleType *muzzle = GetMagazineSlot(weapon)._muzzle;
		float dCoef=floatMax(muzzle->_aiDispersionCoefX,muzzle->_aiDispersionCoefY);
		tgtSize+=dist*mode->_dispersion*dCoef*aimPrecision*0.5f;

    if (leadSpeed.SquareSize()>Square(10)) tgtSize *= 2;

#if _ENABLE_CHEATS
		if ((Object *)this==GWorld->CameraOn() && CHECK_DIAG(DECombat))
		{
			GlobalShowMessage
			(
				2000,
				"Error %.1f, tgtSize %.1f, time %.2f, "
				"ePos %.1f,%.1f,%.1f, distErr %.1f",
				error,tgtSize,time,
				hError[0],hError[1],hError[2],
				distHit-eDist
			);
		}
#endif

		if( error<=tgtSize ) return visible;
		if( error>=tgtSize*2 ) return 0;
		return ( 2*tgtSize-error )/tgtSize*visible;

	}
}


// manual control

void AirplaneAuto::GetActions(UIActions &actions, AIUnit *unit, bool now)
{
	// if we are stopeed, we may leave it to base class handling
	// it will add get-out, eject and any more default actions
	base::GetActions(actions, unit, now);

	// if we are not stopped, eject and land
	// only for manual plane - gear and flaps
	if (unit && unit->GetVehicleIn() == this && DriverBrain()==unit)
	{

		switch (_planeState)
		{
			case Flight:
				actions.Add(ATLand, this, 0.9);
				break;
			case Marshall: case Approach: case Final: case Landing:
				actions.Add(ATCancelLand, this, 0.9);
				break;
		}
		// only for manual plane - gear and flaps
		if (QIsManual())
		{
			if (!_gearDammage && Type()->_gearRetracting)
			{
				actions.Add(ATLandGear,this,0.8,0,false,false);
			}
			if (_flaps>0.25) actions.Add(ATFlapsUp,this,0.7,0,false,false);
			if (_flaps<0.75) actions.Add(ATFlapsDown,this,0.7,0,false,false);
		}
	}

}

RString AirplaneAuto::GetActionName(const UIAction &action)
{
	switch (action.type)
	{
		case ATLand:
			return LocalizeString(IDS_ACTION_LAND);
		case ATCancelLand:
			return LocalizeString(IDS_ACTION_CANCEL_LAND);
		case ATLandGear:
			if (_gearsUp>0.5) return LocalizeString(IDS_ACTION_GEAR_DOWN);
			else return LocalizeString(IDS_ACTION_GEAR_UP);
		case ATFlapsDown:
			return LocalizeString(IDS_ACTION_FLAPS_DOWN);
		case ATFlapsUp:
			return LocalizeString(IDS_ACTION_FLAPS_UP);
		default:
			return base::GetActionName(action);
	}
}

void AirplaneAuto::PerformAction(const UIAction &action, AIUnit *unit)
{
	switch (action.type)
	{
		case ATLandGear:
			_pilotGear = _gearsUp>0.5;
			break;
		case ATFlapsDown:
			if (_pilotFlaps<2) _pilotFlaps++;
			break;
		case ATFlapsUp:
			if (_pilotFlaps>0) _pilotFlaps--;
			break;
		default:
			base::PerformAction(action,unit);
			break;
	}
}


bool AirplaneAuto::IsStopped() const
{
	if (!_landContact && !_objectContact) return false;
	if (_speed.SquareSize()>Square(2) ) return false;
	if (QIsManual() && EngineIsOn()) return false;
	return base::IsStopped();
}

void AirplaneAuto::DammageCrew( EntityAI *killer, float howMuch, RString ammo )
{
	AIUnit *commander = CommanderUnit();
	if (commander)
	{
		if (GetRawTotalDammage()>=0.45f && commander->GetCombatMode()>=CMCombat)
		{
			Time goTime = Glob.time + GRandGen.PlusMinus(1.0f,0.5f);
			if (goTime<_getOutAfterDammage)  _getOutAfterDammage = goTime;
		}
	}
	
	base::DammageCrew(killer,howMuch,ammo);
}

/*!
\patch 1.22 Date 8/30/2001 by Ondra
- Fixed: parachute ejected from plane or helicopter was always considered western.
\patch_internal 1.24 Date 9/20/2001 by Ondra
- Fixed: added Civilian and Resistance (in addition to previous West and East)
*/

void AirplaneAuto::Eject(AIUnit *unit)
{
	// check height
	float surfaceY=GLOB_LAND->SurfaceYAboveWater(Position()[0],Position()[2]);
	float height = Position().Y() - surfaceY;
	bool parachute = (height>30 || Type()->_ejectSpeed.SquareSize()>Square(2));
	/*
	{
		// create a parachute
		//
		const char *paraName = "ParachuteWest";
		TargetSide side = unit->GetGroup()->GetCenter()->GetSide();
		if (side==TEast) paraName = "ParachuteEast";
		else if (side==TGuerrila) paraName = "ParachuteG";
		else if (side==TCivilian) paraName = "ParachuteC";

		Ref<EntityAI> ent = NewVehicle(paraName,"");
		parachute = dyn_cast<Transport,EntityAI>(ent);
		if (parachute)
		{
			parachute->SetTransform(Transform());
			parachute->SetAllowDammage(GetAllowDammage());
		}
	}
	*/
	if (parachute)
	{
		unit->GetPerson()->SetSpeed(DirectionModelToWorld(Type()->_ejectSpeed));
	}
	unit->ProcessGetOut(parachute);
}

void AirplaneAuto::Land()
{
	if( _planeState==Flight ) _planeState=Marshall;
}

void AirplaneAuto::CancelLand()
{
	switch (_planeState)
	{
		case Marshall: case Approach: case Final:
			_planeState = Flight;
			break;
		case Landing:
			_planeState = WaveOff;
			break;
	}
}

/*!
\patch 1.80 Date 8/2/2002 by Ondra
- New: Scripting: flyInHeight now affects not only helicopters, but also planes.
*/

void AirplaneAuto::SetFlyingHeight(float val)
{
	_defPilotHeight = val;
}

void AirplaneAuto::FakePilot( float deltaT )
{
}

inline float JAdj(float x)
{
	return x*fabs(x);
}

void AirplaneAuto::JoystickDirPilot(float deltaT)
{
	_forceDive=1;

	//_pilotHelper = true;
	_pilotHelperDir = false;
	_pilotHelperHeight = false;
	_pilotHelperBankDive = false;

	_rudderWanted=-GInput.GetStickRudder();
	_aileronWanted=JAdj(GInput.GetStickLeft());
	_elevatorWanted=JAdj(GInput.GetStickForward());

	_pilotDive = 0;
	_pilotBank = 0;

	if( _pilotGear )
	{
		// auto-gear up
		Vector3Val position=Position();
		float surfaceY=GLOB_LAND->SurfaceYAboveWater(position[0],position[2]);
		float above=position[1]-surfaceY;
		if( above>80 && ModelSpeed().Z()>GetType()->GetMaxSpeedMs()*0.5f)
		{
			_pilotGear=false;
		}
	}

}

/*!
\patch 1.43 Date 1/25/2002 by Ondra
- Fixed: Airplane joystick throttle was reversed.
*/

void AirplaneAuto::JoystickThrustPilot( float deltaT )
{
	_pilotHelperThrust = false;
	_thrustWanted = -GInput.GetStickThrust();
	_pilotBrake=floatMax(-_thrustWanted,0);
	saturate(_thrustWanted,0,1);
}

void AirplaneAuto::KeyboardAny(AIUnit *unit, float deltaT)
{
}

void AirplaneAuto::DetectControlMode() const
{
	static const UserAction moveActions[]=
	{
		UAMoveUp,UAMoveDown,
		UAMoveLeft,UAMoveRight,
		UATurnLeft,UATurnRight
	};
	static const UserAction turnActions[]=
	{
		UAMoveForward,UAMoveBack,
		UAMoveFastForward,
		UAMoveLeft,UAMoveRight,
		UATurnLeft,UATurnRight
	};
	static const UserAction cursorActions[]=
	{
		UALookLeftDown,UALookDown,UALookRightDown,
		UALookLeft,UALookCenter,UALookRight,
		UALookLeftUp,UALookUp,UALookRightUp
	};
	static const UserAction thrustActions[]=
	{
		UAMoveForward,UAMoveBack,
		UAMoveFastForward,
		//UAMoveUp,UAMoveDown,
	};

	const int nMoveActions = sizeof(moveActions)/sizeof(*moveActions);
	const int nTurnActions = sizeof(turnActions)/sizeof(*turnActions);
	const int nCursorActions = sizeof(cursorActions)/sizeof(*cursorActions);
	const int nThrustActions = sizeof(thrustActions)/sizeof(*thrustActions);
	DetectControlModeActions
	(
		moveActions,nMoveActions,
		turnActions,nTurnActions,
		cursorActions,nCursorActions,
		thrustActions,nThrustActions
	);
}

/*!
\patch 1.44 Date 2/12/2002 by Ondra
- Fixed: Airplane: Autodetection of joystick / keyboard thrust was wrong.
After using joystick thrust it was impossible to switch back to keyboard thrust.
\patch 1.89 Date 10/23/2002 by Ondra
- Improved: Slow plane takeoff improved, added more adjustable config parameters.
*/

void AirplaneAuto::KeyboardPilot(AIUnit *unit, float deltaT )
{
	switch (_planeState)
	{
		case Flight:
		case Takeoff:
		case TaxiIn: case TaxiOff:
			// no keyboard controls when autopilot is on
			break;
		default:
			// same parameters as AIPilot
			_dirCompensate=0.9;

			//_pilotHelper = true;
			_pilotHelperHeight = true;
			_pilotHelperDir = true;
			_pilotHelperBankDive = true;
			_rudderWanted = 0;

			_forceDive=1;
			
			return;
	}
	_dirCompensate=0.5; // low heading compensation
	
	_forceDive=1;

	CancelStop();


	if( GInput.JoystickActive() )
	{
		JoystickDirPilot(deltaT);
	}
	else
	{

		//_pilotHelper=true;
		_pilotHelperHeight = false;

		_forceDive = 1;

		// TODO: check if driving by mouse
		// keyboard driving



		float bank=DirectionAside().Y();
		float dive=Direction().Y();

		if (_landContact)
		{
			const float takeOffSpeed = Type()->_takeOffSpeed;
			if (ModelSpeed()[2]>=takeOffSpeed)
			{
				// help auto takeoff to avoid gear dammage
				dive = 9*H_PI/180;
			}
			else
			{
				dive = 0;
			}
			_pilotDiveSet = false;
		}

		bool internalCamera = IsGunner(GWorld->GetCameraType());
		if (internalCamera && GInput.MouseTurnActive() && !GInput.lookAroundEnabled)
		{
			// last input from mouse - use mouse controls
			_pilotHelperDir = true;
			_pilotHelperBankDive = true;
			_pilotHeading = atan2(_mouseDirWanted[0],_mouseDirWanted[2]);
			_rudderWanted = 0;
			// dive controlled directly
			_pilotDive = _mouseDirWanted[1];
		}
		else
		{
			_pilotHelperDir = false;
			_pilotHelperBankDive = true;
			_rudderWanted = GInput.keyMoveLeft-GInput.keyMoveRight;
			float turnBank = GInput.keyTurnRight-GInput.keyTurnLeft;
			if (fabs(turnBank)<0.2 && fabs(bank)<0.1)
			{
				// auto-level plane
				_pilotBank = 0;
			}
			else
			{
				_pilotBank = bank - turnBank*0.3;
			}

			float turnDive = GInput.keyMoveUp-GInput.keyMoveDown;
			if (fabs(turnDive)>0.2)
			{
				_pilotDive = dive + turnDive*0.3;
				_pilotDiveSet = false;
			}
			else if (!_pilotDiveSet)
			{
				_pilotDive = dive;
				_pilotDiveSet = true;
			}
		}
	}

	if( GInput.JoystickThurstActive() )
	{
		JoystickThrustPilot(deltaT);
	}
	else
	{
		_pilotHelperThrust = true;

		Vector3Val relSpeed=ModelSpeed();

		float forward=
		(
			GInput.keyMoveForward*0.5
			+GInput.keyMoveFastForward
			-GInput.keyMoveBack*0.5
		);

		if( forward<0.1 )
		{
			// automatically stop when moving very slow
			if( _pilotSpeed<5 ) _pilotSpeed=0;
		}
		if( forward>0 )
		{
			EngineOn();
			if( !_pressedForward ) _pilotSpeed=relSpeed[2],_pressedForward=true;
			_pilotSpeed+=deltaT*20*forward;
		}
		else
		{
			if( _pressedForward ) _pilotSpeed=relSpeed[2],_pressedForward=false;
		}
		if( forward<0 )
		{
			if( !_pressedBack ) _pilotSpeed=relSpeed[2],_pressedBack=true;
			_pilotSpeed+=deltaT*20*forward;
		}
		else
		{
			if( _pressedBack ) _pilotSpeed=relSpeed[2],_pressedBack=false;
		}
		Limit(_pilotSpeed,0,GetType()->GetMaxSpeedMs()*1.5f);
	}
	
	Limit(_pilotHeight,0,250);
	
	//Log("Plane v=%f m/s, h=%fm",_pilotSpeed[2],_pilotHeight);

	/**/
	// manual plane does not have taxiing autopilot
	if( _planeState==TaxiIn || _planeState==TaxiOff )
	{
		_planeState=Takeoff;
	}
	/**/
}

// AI autopilot
class Vector3Path
{
	const Vector3 *_path;
	int _nPath;

	public:
	Vector3Path( const Vector3 *path, int nPath )
	:_path(path),_nPath(nPath)
	{
	}
	// cost is distance from the beginnig of the path
	float GetCost( Vector3Val pos ) const;
	Vector3 GetPos( float cost, bool &end ) const;
};


float Vector3Path::GetCost( Vector3Val pos ) const
{
	if (_nPath<2) return 0;

	float minDist2 = 1e10;
	Vector3 nearestPos = pos;
	int nearestI = 0;
	for (int i=1; i<_nPath; i++)
	{
		Vector3Val b = _path[i-1];
		Vector3Val e = _path[i]-b;
		// calculate distance from line path[i-1] path[i]

		Vector3 p = pos - b;
		float t=(e*p)/e.SquareSize();
		saturate(t,0,1);
		Vector3 nearest=b+t*e;
		float dist2 = nearest.Distance2(pos);
		if (minDist2>dist2)
		{
			minDist2 = dist2;
			nearestPos = nearest;
			nearestI = i;
		}
	}
	float cost = 0;
	// calculate cost to point nearestI-1
	for (int i=0; i<nearestI-1; i++)
	{
		Vector3Val b = _path[i];
		Vector3Val e = _path[i+1];
		float dist = b.Distance(e);
		cost += dist;
	}
	Vector3Val prevPoint = _path[nearestI-1];
	cost += nearestPos.Distance(prevPoint);
	return cost;
}

Vector3 Vector3Path::GetPos( float cost, bool &end ) const
{
	for (int i=1; i<_nPath; i++)
	{
		Vector3Val b = _path[i-1];
		Vector3Val e = _path[i];
		float dist = e.Distance(b);
		if (cost<dist)
		{
			end = false;
			return b+(e-b)*(cost/dist);
		}
		cost-=dist;
	}
	end = true;
	return _path[_nPath-1];
}

AirplaneAuto::PathResult AirplaneAuto::PathAutopilot
(
	const Vector3 *path, int nPath
)
{
	_pilotGear=true;

	if (nPath<2)
	{
		return PathFinished;
	}

	Vector3Path vpath(path,nPath);

#if _ENABLE_CHEATS
	if (CHECK_DIAG(DEPath))
	{
		for (int i=0; i<nPath; i++)
		{
			Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),-1);
			obj->SetPosition(path[i]);
			obj->SetScale(0.2);
			obj->SetConstantColor(PackedColor(Color(1,1,0)));
			GLandscape->ShowObject(obj);			
		}
	}
#endif

	float cost = vpath.GetCost(Position());

	bool endOfPath = false;

	Vector3 pos = vpath.GetPos(cost+22,endOfPath);

	Vector3 predTurnPos = vpath.GetPos(cost+220,endOfPath);

	// advance in direction of 

#if _ENABLE_CHEATS
	if (CHECK_DIAG(DECombat))
	{
		{
			Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),-1);
			obj->SetPosition(pos);
			obj->SetScale(0.5);
			obj->SetConstantColor(PackedColor(Color(0,1,1)));
			GLandscape->ShowObject(obj);			
		}

		{
			Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),-1);
			obj->SetPosition(predTurnPos);
			obj->SetScale(0.5);
			obj->SetConstantColor(PackedColor(Color(0,1,0)));
			GLandscape->ShowObject(obj);			
		}
	}
#endif

	// 
	Vector3 relPos=pos-Position();

	float distance = predTurnPos.Distance(Position());
	// adjust thrust to maintain some speed
	// speed is dependent on relPos size and angle

	PathResult ret = PathGoing;
	if (endOfPath && distance<20)
	{
		_pilotSpeed=0;
		_thrustWanted=0;
		_pilotBrake=1;
		ret = PathFinished;
	}
	else
	{
		_pilotHeading=atan2(relPos.X(),relPos.Z());

		float distFactor =distance>210 ? 1 : distance*(1.0/210);
		_pilotSpeed = 28*distFactor;

		Vector3 relPredPos = PositionWorldToModel(predTurnPos);

		float turn = 0.9;
		if (relPredPos.Z()>1)
		{
			turn = fabs(relPredPos.X()*0.1);


			// predicted turn never requires really low speed
			saturateMin(turn,0.9);
		}

		// actual turn may required very low speed
		float curHeading = atan2(Direction().X(),Direction().Z());
		saturateMax(turn,fabs(_pilotHeading-curHeading)*4);

		saturateMin(turn,1);

		saturateMin(_pilotSpeed,turn * 3 + (1-turn)*30);
	}

	_elevatorWanted=0;
	_pilotFlaps=0;

	// check for collision / locked field
	

	float mySize=CollisionSize(); // assume vehicle is not round
	float mySpeedSize=fabs(ModelSpeed()[2]);
	// check if we are on collision course
	VehicleCollisionBuffer col;
	float gap = mySize*3;
	float maxDist = Square(mySpeedSize)*0.5+gap;
	float maxTime = 3.5+mySpeedSize*0.2;

	//LogF("%s: Predict gap %.1f, maxDist %.1f",(const char *)GetDebugName(),gap,maxDist);

	GLOB_LAND->PredictCollision(col,this,maxTime,gap,maxDist);
	if( col.Size()>0 )
	{
		// stop and wait until road is clear
		// if vehicle's engine is off stop and terminate
		bool stop = false;

		for (int i=0; i<col.Size(); i++)
		{
			const VehicleCollision &info=col[i];
			const VehicleWithAI *who=info.who;
			if (!who) continue;
			#if COL_DIAG
			LogF
			(
				"%s: col %s",
				(const char *)GetDebugName(),
				(const char *)who->GetDebugName()
			);
			#endif
			// check if vehicle is in front of us
			float relDist = PositionWorldToModel(who->Position()).Z();
			if (relDist<=0)
			{
			#if COL_DIAG
				LogF("  not in front");
			#endif
				continue;
			}
			if (!who->EngineIsOn())
			{
			#if COL_DIAG
				LogF("  engine off");
				LogF("  my speed %.1f",ModelSpeed().Z());
				LogF("  tw %.2f, t %.2f",_thrustWanted,_thrust);
			#endif
				stop=true;
				break;
			}
			// we want to keep gap between us
			float relSpeed = DirectionWorldToModel(who->Speed()).Z()-2;

			float slower = floatMax((relDist-gap)*0.5,0);
			if (relSpeed>0) relSpeed=0;

			#if COL_DIAG
			LogF("  relSpeed %.1f, slower %.1f",relSpeed,slower);
			#endif
			relSpeed -=slower;
			saturateMax(relSpeed,0);
			if (relSpeed<5) relSpeed=0;

			if (relSpeed<_avoidSpeed || Glob.time>_avoidSpeedTime)
			{
				// if we decide to brake, we will brake for some time
				_avoidSpeed = relSpeed;
				_avoidSpeedTime = Glob.time+1;
			#if COL_DIAG
				LogF("  avoid speed %.2f",_avoidSpeed);
			#endif
			}
		}
		if (stop)
		{
			ret = PathAborted;
			_pilotSpeed=0;
			_thrustWanted=0;
			_pilotBrake=1;

			_avoidSpeed = 0;
			_avoidSpeedTime = Glob.time+3;
		}
	}

	if( Glob.time<_avoidSpeedTime )
	{
		saturateMin(_pilotSpeed,_avoidSpeed);
	}

	/*
				int operX=toInt(Position().X()*InvOperItemGrid);
				int operZ=toInt(Position().Z()*InvOperItemGrid);
				LockCache *locks=GLandscape->LockingCache();
				const int range=50;
					bool lockV=locks->IsLocked(x,z,false);
					bool lockS=locks->IsLocked(x,z,true);
	*/

	return ret;
}

bool AirplaneAuto::TaxiOffAutopilot()
{
	// contruct path relative to ils position
	const AutoArray<Vector3> &path = GWorld->GetTaxiOffPath(_targetSide);

	return PathAutopilot(path.Data(),path.Size())<=PathAborted;
}

bool AirplaneAuto::TaxiInAutopilot()
{
	const AutoArray<Vector3> &path = GWorld->GetTaxiInPath(_targetSide);

	return PathAutopilot(path.Data(),path.Size())<=PathFinished;
}

void AirplaneAuto::Autopilot
(
	Vector3Par target, Vector3Par tgtSpeed, // target
	Vector3Par direction, Vector3Par speed // wanted values
)
{
}

void AirplaneAuto::ResetAutopilot()
{
}

float AirplaneAuto::MakeAirborne()
{
	_pilotSpeed=GetType()->GetMaxSpeedMs()*0.66;
	_pilotHeight=_defPilotHeight;
	_rpm=1;
	_thrust=_thrustWanted=0.5;

	_speed=Direction()*_pilotSpeed;
	_flaps=0;
	_gearsUp=1;
	_planeState=Flight;
	_pilotGear=false;
	_pilotFlaps=0;
	_landContact = false;

	EngineOn();

	return _pilotHeight;
}

/*!
\patch 1.02 Date 7/11/2001 by Ondra.
- Fixed: Mavericks released from correct position.
\patch 1.07 Date 7/20/2001 by Ondra.
- New: Rocket support for airplanes.
*/

bool AirplaneAuto::FireWeapon( int weapon, TargetType *target )
{
	if (GetNetworkManager().IsControlsPaused()) return false;
	if (weapon >= NMagazineSlots()) return false;
	if( !GetWeaponLoaded(weapon) ) return false;
	if( !IsFireEnabled() ) return false;

	const Magazine *magazine = GetMagazineSlot(weapon)._magazine;
	if (!magazine) return false;
	const MagazineType *aInfo = magazine ? magazine->_type : NULL;

	const WeaponModeType *mode = GetWeaponMode(weapon);
	if (!mode || !mode->_ammo) return false;
	bool fired=false;
	switch (mode->_ammo->_simulation)
	{
		case AmmoShotRocket:
		case AmmoShotMissile:
			if (mode->_ammo->maxControlRange<10)
			{
				_rocketLRToggle=!_rocketLRToggle;
				Vector3Val pos=( _rocketLRToggle ? Type()->_rocketLPos : Type()->_rocketRPos );
				fired=FireMissile
				(
					weapon,
					pos,VForward,Vector3(0,0,aInfo->_initSpeed),
					target
				);
			}
			else
			{
				// find corresponding proxy position
				int count = GetMagazineSlot(weapon)._magazine->_ammo;
				bool found;
				Vector3Val pos = FindMissilePos(count,found);
				fired=FireMissile
				(
					weapon,
					pos,VForward,VZero,
					target
				);
			}
		break;
		case AmmoShotBullet:
		{
			Matrix4Val shootTrans=GunTransform();
			fired=FireMGun
			(
				weapon,
				shootTrans.FastTransform(Type()->_gunPos),
				shootTrans.Rotate(Type()->_gunDir),
				target
			);
		}
		break;
		case AmmoNone:
		break;
		default:
			Fail("Unknown ammo used.");
		break;
	}

	if( fired )
	{
		base::FireWeapon(weapon, target);
		return true;
	}
	return false;
}

void AirplaneAuto::FireWeaponEffects
(
	int weapon, const Magazine *magazine,EntityAI *target
)
{
	const MagazineSlot &slot = GetMagazineSlot(weapon);
	if (!magazine || slot._magazine!=magazine) return;

	const WeaponModeType *mode = GetWeaponMode(weapon);
	if (!mode) return;
	if (!mode->_ammo) return;
	
	if (EnableVisualEffects(SimulateVisibleNear)) switch (mode->_ammo->_simulation)
	{
		case AmmoShotRocket:
		case AmmoShotMissile:
		case AmmoNone:
			break;
		case AmmoShotBullet:
			_mGunClouds.Start(0.1);
			_mGunFire.Start(0.1,0.4,true);
			_mGunFireFrames = 1;
			_mGunFireTime = Glob.uiTime;
			int newPhase;
			while ((newPhase = toIntFloor(GRandGen.RandomValue() * 3)) == _mGunFirePhase);
			_mGunFirePhase = newPhase;
			break;
	}

	base::FireWeaponEffects(weapon, magazine,target);
}

// AI interface

float AirplaneAuto::GetFieldCost( const GeographyInfo &info ) const
{
	float cost=1;

	// slightly prefer roads
	if( info.u.road ) cost*=1/1.05f;
	else if( info.u.track ) cost*=1/1.02f;
	return cost;
}

float AirplaneAuto::GetCost( const GeographyInfo &geogr ) const
{
	float cost=Type()->GetMinCost(); // basic speed
	// water is low and therefore following water is usually safer and quicker
	// avoid any water
	if (geogr.u.waterDepth>=2) cost*=0.8f;
	else if (geogr.u.waterDepth>=1) cost*=0.9f;
	// penalty for hills
	int grad = geogr.u.gradient;
	if( grad>5 ) grad=5;
	static const float gradPenalty[6]={1.0,1.1,1.2,1.3,1.6,2.5};
	cost *= gradPenalty[grad];
	return cost;
}

float AirplaneAuto::GetCostTurn( int difDir ) const
{ // in sec
	if( difDir==0 ) return 0;
	float aDir=fabs(difDir);
	float cost=aDir*0.15+aDir*aDir*0.02;
	if( difDir<0 ) return cost*0.8;
	return cost;
}

float AirplaneAuto::FireInRange( int weapon, float &timeToAim, const Target &target ) const
{
	timeToAim=0;
	Vector3 relPos = target.AimingPosition()-Position();
	// plane should prefer targets that are near the center
	float cosAngle = relPos.CosAngle(Direction());
	// it should prefer targets not very near, as aiming them is difficult
	float distance = relPos.Size();
	// assume targetting target in distance  0 will take 5 sec
	// assume targetting target in distance 200 will take 0 sec

	// ideal target distance is 1000
	// add 10 sec for each 1000 m for distance above 1000
	// add 20 sec for each 1000 m for distance below 1000 (30 sec for dist 0)
	float timeToAimDistance = distance>1000 ? (distance-1000)*(10.0f/1000) : (1000-distance)*(30.0f/1000);
	saturate(timeToAimDistance,0,30);
	float timeToAimAngle = (1-floatMax(cosAngle,0))*15;
	timeToAim = timeToAimAngle+timeToAimDistance;

	return 1;
}

#if _ENABLE_AI

void AirplaneAuto::AIGunner(AIUnit *unit, float deltaT )
{
	Assert(unit);

	if( !GetFireTarget() ) return;

	AimWeapon(_currentWeapon,GetFireTarget());
	
	if( _currentWeapon<0 ) return;
	if( _fire._firePrepareOnly ) return;
	
	// check if weapon is aimed
	if
	(
		GetWeaponLoaded(_currentWeapon) && GetAimed(_currentWeapon,GetFireTarget())>=0.7
		&& GetWeaponReady(_currentWeapon,GetFireTarget())
	)
	{
		if (!GetAIFireEnabled(GetFireTarget())) ReportFireReady();
		else
		{
			FireWeapon(_currentWeapon,GetFireTarget()->idExact);
	//		_firePrepareOnly = true;
			_fireState=FireDone;
			_fireStateDelay=Glob.time+5; // leave some time to recover
		}
	}
}

#endif //_ENABLE_AI

void AirplaneAuto::MoveWeapons( float deltaT )
{
	float delta;
	float speed;
	speed=(_gunXRotWanted-_gunXRot)*8;
	const float maxA=10;
	const float maxV=5;
	delta=speed-_gunXSpeed;
	Limit(delta,-maxA*deltaT,+maxA*deltaT);
	_gunXSpeed+=delta;
	Limit(_gunXSpeed,-maxV,+maxV);
	_gunXRot+=_gunXSpeed*deltaT;
	Limit(_gunXRot,Type()->_minGunElev,Type()->_maxGunElev);

	speed=AngleDifference(_gunYRotWanted,_gunYRot)*6;
	delta=speed-_gunYSpeed;
	Limit(delta,-maxA*deltaT,+maxA*deltaT);
	_gunYSpeed+=delta;
	Limit(_gunYSpeed,-maxV,+maxV);
	_gunYRot+=_gunYSpeed*deltaT;
	_gunYRot=AngleDifference(_gunYRot,0);
	Limit(_gunYRot,Type()->_minGunTurn,Type()->_maxGunTurn);
}

#if _ENABLE_AI

/*!
Avoid collision. Current plane route determined by _pilotHeading and _pilotSpeed
*/

#define DIAG_COL 0

/*!
\patch 1.90 Date 10/30/2002 by Ondra
- Fixed: Airplanes try to avoid mid-air collisions.
*/

void AirplaneAuto::AvoidCollision()
{
	if (!Airborne()) return;
	AIUnit *unit = PilotUnit();
	if( !unit ) return;

	AISubgroup *mySubgrp = unit->GetSubgroup();

	if( mySubgrp && mySubgrp->GetMode()==AISubgroup::DirectGo ) return;
	// if we are stopped, do not try to avoid
	// TODO: some vehicles (esp. men) should go out of way of tanks
	const float maxSpeedStopped=0.05;
	if( fabs(_pilotSpeed)<=maxSpeedStopped ) return;

	// avoid collisions
	float mySize=floatMax(15,CollisionSize()*2); // assume vehicle is not round
	//float mySpeedSize=fabs(ModelSpeed()[2]);
	float gap = mySize*4;
	//float frontGap = mySize*8;
	// check if we are on collision course
	VehicleCollisionBuffer ret;

	const float maxSpeed = GetType()->GetMaxSpeedMs();
	const float mySpeed = ModelSpeed().Z();
	const float maxTime = 4;
	const float maxDist = floatMax(mySize*4,floatMax(mySpeed,maxSpeed*0.2)*maxTime);
	GLOB_LAND->PredictCollision(ret,this,maxTime,gap,maxDist);
	if( ret.Size()<=0 ) return;

	// some collision predicted

	//AIGroup *myGroup = unit->GetGroup();

	// precalculate
	//float invMaxSpeed=1/maxSpeed;

	//Vector3 mySpeed=Speed();

	for( int i=0; i<ret.Size(); i++ )
	{
		const VehicleCollision &info=ret[i];
		const EntityAI *who=info.who;

		if (!who->Airborne()) continue;
		// something is near
		// some vehicle
		// determine who should slow down
		// if who is in front of us, slow down to his speed
		// if we are heave and he is enemy soldier, ignore him

#if _ENABLE_CHEATS
		if( CHECK_DIAG(DECombat) )
		{
			Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),-1);
			obj->SetPosition(info.pos);
			Color color(1,1,0,0.3);
			//color=Color(1,-1,0)*danger+Color(0,1,0);
			obj->SetScale(info.distance<gap ? 1 : 0.3);
			obj->SetConstantColor(PackedColor(color));
			GLandscape->ShowObject(obj);
		}
#endif


		#if DIAG_COL
		//if( this==GWorld->CameraOn() )
		{
			LogF("%s vs %s",(const char *)GetDebugName(),(const char *)who->GetDebugName());
		}
		#endif

		Vector3 relPos = PositionWorldToModel(who->Position());
		bool iAmInFrontOfHim=who->PositionWorldToModel(Position()).Z()>0;
		bool heIsInFrontOfMe=relPos.Z()>0;

		if (heIsInFrontOfMe && iAmInFrontOfHim)
		{
			// who is higher should climb
			float heIsHigher = who->Position().Y()-Position().Y();
			if (heIsHigher<0)
			{
				float surfaceY = GLandscape->SurfaceYAboveWater(Position().X(),Position().Z());
				saturateMax(_pilotAvoidHighHeight,who->Position().Y()-surfaceY+mySize);
				Time until = Glob.time+10;
				if (_pilotAvoidHigh<until) _pilotAvoidHigh=until;

				#if DIAG_COL
				//if( this==GWorld->CameraOn() )
				{
					LogF("  avoid hi %.2f",_pilotAvoidHighHeight);
				}
				#endif
			}
			else if (heIsHigher>0)
			{
				float surfaceY = GLandscape->SurfaceYAboveWater(Position().X(),Position().Z());
				saturateMin(_pilotAvoidLowHeight,who->Position().Y()-surfaceY-mySize);
				Time until = Glob.time+10;
				if (_pilotAvoidLow<until) _pilotAvoidLow=until;

				#if DIAG_COL
				//if( this==GWorld->CameraOn() )
				{
					LogF("  avoid lo %.2f",_pilotAvoidLowHeight);
				}
				#endif
			}
		}

		if( info.distance>gap ) continue;

		if (info.distance<mySize)
		{
			// collission imminent - who is back needs to brake
			// check if he is in front of me
			if (heIsInFrontOfMe)
			{
				// slow down as much as possible
				_pilotSpeed = Type()->_landingSpeed;
			}
		}
	} // for(i)
}

static int RandomID(AIUnit *unit)
{
	// part of seed is based on unit pointer or ID
	int idSeed = unit->ID()*25689;
  AIGroup *grp = unit->GetGroup();
  if (!grp) return idSeed;
  int grpSeed = grp->ID()*13;
  AICenter *cnt = grp->GetCenter();
  if (!cnt) return idSeed+grpSeed;
  int cntSeed = cnt->GetSide()*5;
  return idSeed+grpSeed+cntSeed;
}

static int RandomManeuver(AIUnit *unit, int nManeuvers, float duration, float &manPhase)
{
  int seed = RandomID(unit);
  const float loopDuration = duration*nManeuvers;
  float phase = GRandGen.RandomValue(seed)*loopDuration;
  float t = (Glob.time.toFloat()+phase)*(1/duration);
  Assert(t>=0);
  int tFloor = toIntFloor(t);
  manPhase = t-tFloor;
  return toInt(GRandGen.RandomValue(tFloor)*100000+tFloor)%nManeuvers;
}

/*!
\patch 1.08 Date 7/20/2001 by Ondra.
- Fixed: AI Airplane target engaging improved.
\patch 1.33 Date 11/29/2001 by Ondra.
- Fixed: AI Airplane mgun and LGB target engaging improved.
\patch 1.58 Date 5/17/2002 by Ondra
- Fixed: AI Airplane maneuvring when aiming singificantly improved. 
\patch 1.78 Date 7/16/2002 by Ondra
- Improved: AI Airplane mgun engaging improved.
\patch 1.95 Date 10/27/2003 by Ondra
- Improved: AI Airplane dogfight evasive tactics.
*/

void AirplaneAuto::AIPilot(AIUnit *unit, float deltaT )
{
	_dirCompensate=0.9;

	//_pilotHelper = true;
	_pilotHelperHeight = true;
	_pilotHelperBankDive = true;
	_rudderWanted = 0;
	_pilotHelperDir = true;
	_pilotHelperThrust = true;

	_forceDive=1;

	if( unit->GetState()==AIUnit::Stopping )
	{
		if( _landContact && _speed.SquareSize()<0.1 && !EngineIsOn())
		{
			UpdateStopTimeout();
			unit->SendAnswer(AI::StepCompleted);
			// note: Brain may be NULL now
			return;
		}
		if( _planeState==Flight ) _planeState=Marshall;
	}
	else if( unit->GetState()==AIUnit::Stopped )
	{
		EngineOff();
	}
	else if( _planeState==Takeoff )
	{
		Vector3 ilsPos,ilsDir;
		GWorld->GetILS(ilsPos,ilsDir,_targetSide);

		_pilotSpeed=100;

		Vector3 ilsCPos=ilsPos-ilsDir*800;
		Vector3 relPos=ilsCPos-Position();
		_pilotHeading=atan2(relPos.X(),relPos.Z());
	}
	else if (_planeState==TaxiIn || _planeState==TaxiOff)
	{
		// Airplane::Simulate autopilot handles this condition
		EngineOn();
		_pilotGear=true;
	}
	else
	{
		_pilotGear=false;

		Target *assigned=unit->GetTargetAssigned();
		if( assigned && _sweepTarget!=assigned && assigned->State(unit)>=TargetAlive)
		{
			#if LOG_SWEEP
			if( assigned->idExact )
			{
				LogF
				(
					"%s: %.1f Switch sweep (disengage) from %s to %s",
					(const char *)GetDebugName(),
					Glob.time-Time(0),
					_sweepTarget.IdExact() ? (const char *)_sweepTarget.IdExact()->GetDebugName() : "<null>",
					(const char *)assigned->idExact->GetDebugName()
				);
			}
			#endif
			_sweepTarget=assigned;
			_sweepState=SweepDisengage;
			_sweepDelay=Glob.time+10;
		}

		if( _sweepTarget )
		{
			EntityAI *sweepTargetAI =  _sweepTarget->idExact;
			bool laserTarget =
			(
				sweepTargetAI && sweepTargetAI->GetType()->GetLaserTarget()
			);

			float surfaceY=GLOB_LAND->SurfaceYAboveWater(Position().X(),Position().Z());
			float height=Position().Y()-surfaceY;

			_pilotHeight=height;
			saturate(_pilotHeight,50,200);


			_pilotSpeed=Type()->GetMaxSpeedMs()*0.6f;

			Vector3 relPos;
			// use exact weapon aiming calculation
			if (!CalculateAimWeapon(_currentWeapon,relPos,_sweepTarget))
			{
				// if exact calculation failed, used approxiate calculation
				float tgtDist = _sweepTarget->AimingPosition().Distance(Position());
				float tgtTime = tgtDist*(1.0f/100);
				Vector3 tgtPos = _sweepTarget->AimingPosition()+_sweepTarget->speed*tgtTime;
				relPos = tgtPos-Position();
			}
			float distXZ=relPos.SizeXZ();

      #if _ENABLE_CHEATS
        if (GWorld->CameraOn()==this)
        {
          __asm nop;
        }
      #endif

			// detect situation: he is targetting me
			// if his speed can be compared with mine, dogfighting is required
			bool evade = false;
			// if engaging fast moving targets, different tactics is required
			bool dogfight = sweepTargetAI && sweepTargetAI->Speed().SquareSize()>=Speed().SquareSize()*Square(0.5);
		  float minHeight = dogfight ? 30 : 50;
			if (dogfight)
			{
				// he is on my six
				Vector3 myPosToHim = Position()-sweepTargetAI->Position();
				float hisCosAngle = sweepTargetAI->Direction().CosAngle(myPosToHim);
				if (hisCosAngle>0.75f && myPosToHim.SquareSizeXZ()<Square(300))
				{
					evade = true;
				}
			}

      bool specManeuver = false;
			float headChange=0;
      if (evade && distXZ<600)
      {
        // when dogfighting, we can do various evasive maneuvers to get some tactical advantage
        // we do it randomly based on time intervals
        float manPhase;
        int maneuver = RandomManeuver(unit,10,10,manPhase);
        #if _ENABLE_CHEATS
          if (GWorld->CameraOn()==this)
          {
            static const char *name[]=
            {
              "Break climb","Break climb",
              "Break dive","Break dive",
              "Turn dive","Turn dive",
              "Climb stall","Climb stall",
              "Run away",""
            };
            GlobalShowMessage(
              500,"Man %d (%s): %.3f",
              maneuver,name[maneuver],manPhase
            );
          }
        #endif

        // when evading, always move as fast as possible
        // turning will probably slow you down anyway
        _pilotSpeed = Type()->GetMaxSpeedMs()*1.5f;
        switch(maneuver)
        {
          case 0: case 1:// climb and break
            _pilotHeight = floatMin(height+10,500);
            headChange = (maneuver&1) ? +1.3f : -1.3f;
            specManeuver = true;
            break;
          case 2: case 3:// dive and break
            _pilotHeight = floatMax(minHeight,height-10);
            headChange = (maneuver&1) ? +1.5f : -1.5f;
            specManeuver = true;
            break;
          case 4: case 5: // dive and turn
            _pilotHeight = floatMax(minHeight,height-10);
            headChange = (maneuver&1) ? +0.5f : -0.5f;
            specManeuver = true;
            break;
          case 6: case 7:// climb, when near stall, turn
            _forceDive = 0.95f;
            _pilotHeight = floatMin(height+10,500);
            if (ModelSpeed().Z()>Type()->_stallSpeed && manPhase<0.6f)
            {
              headChange = (maneuver&1) ? +0.7f : -0.7f;
            }
            else
            {
              headChange = (maneuver&1) ? +2.0f : -2.0f;
            }
            specManeuver = true;
            break;
          case 8: // dive and change direction (run away)
            _pilotHeight = floatMax(minHeight,height-30);
            headChange = sin(manPhase*H_PI*6)*0.3f;
            specManeuver = true;
            break;
        }
      }

      if (!specManeuver)
      {
			  const float safeDistance=dogfight ? 300 : 900;
			  if( _sweepState==SweepDisengage )
			  {
          // slow plane: while disengaging be faster
  			  _pilotSpeed = floatMin(180/3.6,Type()->GetMaxSpeedMs());
				  // this is 
				  Vector3 tgtDir=PositionWorldToModel(_sweepTarget->AimingPosition());
				  float headChangeToTarget=atan2(tgtDir.X(),tgtDir.Z());
				  if (laserTarget)
				  {
					  // in case of laser target disenagage in the direction of source
					  // laser target faces away from laser source
					  Vector3 aimDir=DirectionWorldToModel(-sweepTargetAI->Direction());
					  headChange=atan2(aimDir.X(),aimDir.Z());
				  }
				  else
				  {
					  // you can disengage in any directon
					  // TODO: select suitable disengage direction
					  if (dogfight)
					  {
						  // disengage to his left
						  Vector3 aimDir = DirectionWorldToModel(-sweepTargetAI->DirectionAside());
						  headChange=atan2(aimDir.X(),aimDir.Z());
					  }
					  else
					  {
						  headChange = 0;
					  }
				  }


			    const float predictTime = 2;
			    Vector3 predictedPos = _sweepTarget->position+_sweepTarget->speed*predictTime;
			    float predictedDistXZ2 = predictedPos.DistanceXZ2(Position());

          // while disengaging gain altitude slowly
				  _pilotHeight = floatMin( minHeight+distXZ*(1.0f/500)*(130-minHeight),500);
				  if( predictedDistXZ2>Square(safeDistance) || fabs(headChangeToTarget)<0.5f || evade)
				  {
					  if (!dogfight || evade || distXZ>=200 || fabs(headChangeToTarget)<0.5f)
					  {
						  _sweepState=SweepEngage;
						  _sweepDelay=Glob.time+25;
						  #if _ENABLE_REPORT
						  if (evade)
						  {
							  LogF("%s: evading %s",(const char *)GetDebugName(),(const char *)sweepTargetAI->GetDebugName());
						  }
						  #endif
						  #if LOG_SWEEP
							  LogF
							  (
								  "%s: %.1f sweep engage (%s), dist %.0f, dogfight %d, evade %d",
								  (const char *)GetDebugName(),Glob.time-Time(0),
								  sweepTargetAI ? (const char *)sweepTargetAI->GetDebugName() : "<null>",
                  distXZ,dogfight,evade
							  );
						  #endif
					  }
				  }
			  }
			  else if( _sweepState==SweepEngage )
			  {
				  // target speed required
				  // in dogfight avoid overshopting (getting in front of him)
				  // if we are far, apply more than target speed
				  float speedAdd = Interpolativ(distXZ,0,2000,-1,Type()->GetMaxSpeedMs()*0.5f);
				  _pilotSpeed = _sweepTarget->speed.Size()+speedAdd;
				  saturateMax(_pilotSpeed,Type()->GetMaxSpeedMs()*0.5f);

				  _sweepDir = relPos;
				  Vector3 wantedDir = _sweepDir;
				  Vector3 diveDir = _sweepDir;
				  if (dogfight)
				  {
					  _pilotHeight=_sweepTarget->position.Y()-surfaceY;
					  saturateMax(_pilotHeight,minHeight);
				  }

				  Matrix3 orientUp;
				  orientUp.SetUpAndDirection(VUp,Direction());
				  Vector3Val aimDir = orientUp.InverseRotation()*wantedDir;

				  headChange=atan2(aimDir.X(),aimDir.Z());

          // check if target is in front  of us
          const float offTreshold = H_PI*0.1f;
          if (dogfight && fabs(headChange)>offTreshold)
          {
            // target not in front of us - try some offensive maneuver
            float manPhase;
            int maneuver = RandomManeuver(unit,4,6,manPhase);
            #if _ENABLE_CHEATS
              if (GWorld->CameraOn()==this)
              {
                static const char *name[]=
                {
                  "High yoyo",
                  "Low yoyo",
                  "Turn",
                  ""
                };
                GlobalShowMessage(
                  500,"Off %d (%s): %.3f",
                  maneuver,name[maneuver],manPhase
                );
              }
            #endif
            switch (maneuver)
            {
              case 0: // high yoyo
                headChange += fSign(headChange)*floatMin(1,(fabs(headChange)-offTreshold)*4);
                _pilotHeight = height+15;
              break;
              case 1: // low yoyo
                headChange += fSign(headChange)*floatMin(1,(fabs(headChange)-offTreshold)*4);
                _pilotHeight = floatMax(minHeight,height-15);
                break;
              case 2: // level overturn
                headChange += fSign(headChange)*floatMin(1,(fabs(headChange)-offTreshold)*4);
                break;
            }
          }
				  // when plane turn, it is banking
				  // this would change relative position in x-direction
				  // to counteract this, we create orientation from direction and vertical
				  //LogF("Sweep headc %.2f, aimDir %.2f,%.2f",headChange,aimDir.X(),aimDir.Z());
				  
				  // aggresive maneuver
				  // if we are far, maneuver a little bit mor aggesive
				  saturate(headChange,-H_PI*0.8f,+H_PI*0.8f);
          
				  // move - to be hard target
				  if( distXZ<200 && !dogfight )
				  {
					  _sweepState=SweepFire;
					  _sweepDelay=Glob.time+5; // start immediatelly

					  #if LOG_SWEEP
						  LogF
						  (
							  "%s: %.1f sweep fire (%s)",
							  (const char *)GetDebugName(),Glob.time-Time(0),
							  sweepTargetAI ? (const char *)sweepTargetAI->GetDebugName() : "<null>"
						  );
					  #endif
				  }

				  float minZDist = dogfight ? 30 : 100;
				  if (laserTarget)
				  {
					  // force laser sweep target as fire target
					  _fire.SetTarget(CommanderUnit(),_sweepTarget);
				  }
				  else if
				  (
					  aimDir.Z()>minZDist &&
					  ( fabs(aimDir.X())<25 || fabs(aimDir.X())<aimDir.Z()*0.07f )
				  )
				  {
					  // if the target is horizontally aimed, make vertical adjust
					  // check if evasion is necessary - avoid equal opportunity situations
					  if (dogfight && evade && distXZ<200)
					  {
						  // need to evade - otherwise crash or being hit is possible
						  _sweepState=SweepDisengage;
						  _sweepDelay=Glob.time+10; // start immediatelly
					  }
					  // if we are high enough, aim vertically
            // check if we are in weapons and visibility range
            float minFireDist = floatMin(1200,Glob.config.horizontZ);
            if (_currentWeapon>=0)
            {
          	  const WeaponModeType *mode = GetWeaponMode(_currentWeapon);
              if (mode && mode->_ammo)
              {
                saturateMin(minFireDist,mode->_ammo->maxRange);
              }
            }
          
            if (aimDir.Z()>minFireDist)
            {
    				  _pilotHeight = floatMin( minHeight+20+distXZ*(1.0f/500)*(130-minHeight),500);
            }
					  else if (height>minHeight && sweepTargetAI)
					  { // we are flying high enough
						  // actual aiming
						  float visible = _visTracker.KnownValue
						  (
							  this,_currentWeapon,sweepTargetAI
						  );
						  if (visible>0.7f)
						  {
							  // adapt forceDive to aim target

							  float rawDive = diveDir.Y()*diveDir.InvSizeXZ();
							  _forceDive = rawDive - Type()->_gunDir.Y();

							  // empirical fix: plane almost always fired too low
                // might be caused by some speed estimation bug
							  _forceDive += 0.001f;

							  //LogF("Dive raw %.3f, act %.3f",rawDive,_forceDive);
							  
							  float maxDive = dogfight ? 0.3f : 0.1f;	
							  // positive dive is up
							  saturate(_forceDive,-0.7f,maxDive);

							  //LogF("  clip %.3f",_forceDive);

							  //LogF("  force dive %.2f, %.2f",_forceDive,Direction().Y());
							  // sweep target aligned: force it as fire target
							  _fire.SetTarget(CommanderUnit(),_sweepTarget);
						  }
						  // override stall avoidance
						  _pilotHeight=height;
					  }
				  }
				  else
				  {
					  if (distXZ>1200)
					  {
						  // climb while you can
						  _pilotHeight = 200;
					  }
					  // FIX
					  else if (height>minHeight)
					  {
						  // avoid diving  - we need elevator for smooth turn
						  if (!dogfight)
						  {
							  _forceDive = 0;
						  }
						  //LogF("  force no dive %.2f",_forceDive);
					  }
				  }
			  }
			  else if( _sweepState==SweepFire )
			  {
				  Matrix3 orientUp;
				  orientUp.SetUpAndDirection(VUp,Direction());
				  Vector3Val aimDir = orientUp.InverseRotation()*_sweepDir;

				  headChange=atan2(aimDir.X(),aimDir.Z());

				  if( !evade && fabs(headChange)>0.6f && ( distXZ<100 || distXZ>300 ) )
				  {
					  _sweepState=SweepDisengage;
					  _sweepDelay=Glob.time+10; // start immediatelly

					  #if LOG_SWEEP
						  LogF
						  (
							  "%s: %.1f sweep disengage (%s)",
							  (const char *)GetDebugName(),Glob.time-Time(0),
							  sweepTargetAI ? (const char *)sweepTargetAI->GetDebugName() : "<null>"
						  );
					  #endif
				  }	

			  }
      }

			float actHeading = atan2(Direction().X(),Direction().Z());
			_pilotHeading=actHeading+headChange;
			/*
			LogF
			(
				"PH %.2f, AH %.2f, PH-AH %.2f",
				_pilotHeading,actHeading,AngleDifference(_pilotHeading,actHeading)
			);
			*/

			// no limits during sweep
			_limitSpeed=GetType()->GetMaxSpeedMs()*1.5f;
			// check if target is alive
			if (!sweepTargetAI || _sweepTarget->State(unit)<TargetAlive)
			{
				_sweepTarget=NULL;
			}
			else if (laserTarget)
			{
				saturateMax(_pilotHeight,200);
			}
			if( Glob.time>_sweepDelay ) _sweepTarget=NULL;
		} // sweep 
		else
		{
			float speedWanted=0;
			// normal flight
			Vector3 destination=VZero;
			if( !unit->IsSubgroupLeader() )
			{
				EngineOn();

				// formation - wingman
				// trivial solution always works
				unit->ForceReplan();
				_limitSpeed=GetType()->GetMaxSpeedMs()*1.5f;
				const float estTTurn = 5.0;
				const float estTSpeed = 5.0;
				AIUnit *leader = unit->GetSubgroup()->Leader();

				Vector3Val relFormWanted = unit->GetFormationRelative()-leader->GetFormationRelative();

				VehicleWithAI *leaderVeh = leader->GetVehicle();

				Matrix4 formTransform;
				// predict orientation
				float orientTime = 0.5;
				Matrix3Val leaderOrient = leaderVeh->Orientation();
				Matrix3Val leaderDerOrientation=leaderVeh->AngVelocity().Tilda()*leaderOrient;
				Matrix3Val leaderEstOrientation=leaderOrient+leaderDerOrientation*orientTime;
				Vector3 leaderEstDir = leaderEstOrientation.Direction().Normalized();
				formTransform.SetUpAndDirection(VUp,leaderEstDir);
				formTransform.SetPosition(leaderVeh->Position());

				Vector3 formPos = formTransform.FastTransform(relFormWanted);

				destination = formPos +leaderVeh->Speed()*estTTurn;

				Vector3 relForm = PositionWorldToModel(formPos);
				float destinationZ = relForm.Z();
				float leaderSpeedZ = leaderVeh->Speed().Size();
				// adjust speed based on formation position

				Vector3 relDest=destination-Position();
				_pilotHeading=atan2(relDest.X(),relDest.Z());

				float leaderHeading = atan2(leaderEstDir.X(),leaderEstDir.Z());
				float curHeading = atan2(Direction().X(),Direction().Z());

				float distanceToForm = relForm.Size();
				float inFormFactor = 1-distanceToForm*(1.0/1000);
				if (inFormFactor>0)
				{	
					/*
					LogF
					(
						"  inFormFactor %.2f, ph %.2f, lh %.2f",
						inFormFactor,_pilotHeading,leaderHeading
					);
					*/
					_pilotHeading = inFormFactor*AngleDifference(leaderHeading,_pilotHeading) + _pilotHeading;

				}

				
				// if leader starts turning, never slow down
				// keep at least leader speed - we will need it
				float leaderInTurn = fabs(leaderHeading-curHeading)*(1.0/0.2);
				//LogF("leaderInTurn %.2f",leaderInTurn);

				float maxSlowDown = floatMax(1-leaderInTurn,-1)*10;

				float reachSpeed = destinationZ*(1/estTSpeed);
				saturate(reachSpeed,-maxSlowDown,200);
				speedWanted=leaderSpeedZ+reachSpeed;

				Vector3Val leaderPos=leaderVeh->Position();
				float leaderSurfY=GLandscape->SurfaceYAboveWater(leaderPos.X(),leaderPos.Z());
				_pilotHeight=leaderPos.Y()-leaderSurfY;
				saturateMax(_pilotHeight,_defPilotHeight);

				saturate(speedWanted,Type()->_landingSpeed*1.3f,Type()->GetMaxSpeedMs()*1.5f);
				_pilotSpeed=speedWanted;
			}
			else
			{
				// formation - leader

				speedWanted=_limitSpeed; // go faster
				
				#if DIAG_SPEED
				if( this==GWorld->CameraOn() )
				{
					LogF("Basic speed %.1f",speedWanted*3.6);
				}
				#endif

				// take path destination
				destination=SteerPoint(4.0,6.0);
				//AvoidCollision(steerPos);
				
				// check path position
				const Path &path=unit->GetPath();
				if( path.Size()>=2 )
				{
					EngineOn();

					float cost=path.CostAtPos(Position());
					Vector3 pos=path.PosAtCost(cost,Position());

					float dist2 = Position().DistanceXZ2(pos);
					float distEnd2 = Position().DistanceXZ2(path.End());
					float precision = GetPrecision();
					// check if we have first point of Plan complete
					if (distEnd2<Square(precision) || cost>path.EndCost())
					{
						unit->SendAnswer(AI::StepCompleted);
//						unit->ForceReplan();
					}
					if (dist2>Square(600))
					{
						unit->SendAnswer(AI::StepTimeOut);
					}
				}
				saturate(speedWanted,Type()->_landingSpeed*1.3f,Type()->GetMaxSpeedMs()*1.5f);
				_pilotSpeed=speedWanted;
				Vector3 relDest=destination-Position();
				_pilotHeading=atan2(relDest.X(),relDest.Z());
				_pilotHeight=_defPilotHeight;
			}
		} // normal flight
		// basic collision avoidance
		AvoidCollision();
		if (Glob.time<_pilotAvoidHigh)
		{
			saturateMax(_pilotHeight,_pilotAvoidHighHeight);
		}
		else
		{
			_pilotAvoidHighHeight = 0;
		}
		if (Glob.time<_pilotAvoidLow)
		{
			saturateMin(_pilotHeight,_pilotAvoidLowHeight);
			saturateMax(_pilotHeight,30);
		}
		else
		{
			_pilotAvoidLowHeight = 100000;
		}


	}
}
#endif // _ENABLE_AI

LSError Airplane::Serialize(ParamArchive &ar)
{
	SERIAL_BASE

	if (!IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
	{
		SERIAL_DEF(rpm,1); // on/off
		SERIAL_DEF(thrust, 0);
		SERIAL_DEF(thrustWanted, 0); // turning motor on/off
		SERIAL_DEF(rotorSpeed, 0);
		SERIAL_DEF(rotorPosition, 0);
		SERIAL_DEF(elevator, 0);SERIAL_DEF(elevatorWanted, 0);
		SERIAL_DEF(rudder, 0);SERIAL_DEF(rudderWanted, 0);
		SERIAL_DEF(aileron, 0);SERIAL_DEF(aileronWanted, 0);

		SERIAL_DEF(flaps, 0); // actual flap position
		SERIAL_DEF(gearsUp, 0); // actual gear position
		SERIAL_DEF(brake, 0);

		SERIAL_DEF(pilotBrake, false);
		SERIAL_DEF(pilotGear, true);

	}

	return LSOK;
}

LSError AirplaneAuto::Serialize(ParamArchive &ar)
{
	SERIAL_BASE
	// TODO: serialize _planeState using EnumName
	if (!IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
	{
		CHECK(ar.Serialize("planeState", *(int *)&_planeState, 1, 1 ))
		SERIAL_DEF(pilotAvoidHigh, TIME_MIN);
		SERIAL_DEF(pilotAvoidHighHeight, 0);
		SERIAL_DEF(pilotAvoidLow, TIME_MIN);
		SERIAL_DEF(pilotAvoidLowHeight, 100000);
	}

	return LSOK;
}

NetworkMessageType AirplaneAuto::GetNMType(NetworkMessageClass cls) const
{
	switch (cls)
	{
	case NMCUpdateGeneric:
		return NMTUpdateAirplane;
	case NMCUpdatePosition:
		return NMTUpdatePositionAirplane;
	default:
		return base::GetNMType(cls);
	}
}

//! network message indices for AirplaneAuto class
/*!
\patch 1.01 Date 6/26/2001 by Ondra.
- Fixed: Added more transfered properties in MP for Airplanes.
*/
class IndicesUpdateAirplane : public IndicesUpdateTransport
{
	typedef IndicesUpdateTransport base;

public:
	//@{
	//! index of field in message format

	// ADDED
	int pilotFlaps;
	
	int gearDammage;
	
	int pilotGear;
	// END ADDED
	//@}

	//! Constructor
	IndicesUpdateAirplane();
	NetworkMessageIndices *Clone() const {return new IndicesUpdateAirplane;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesUpdateAirplane::IndicesUpdateAirplane()
{
	pilotFlaps = -1;
	
	gearDammage = -1;
	pilotGear = -1;
}

void IndicesUpdateAirplane::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);

	SCAN(pilotFlaps);
	
	SCAN(gearDammage);
	SCAN(pilotGear);
}

//! Create network message indices for AirplaneAuto class
NetworkMessageIndices *GetIndicesUpdateAirplane() {return new IndicesUpdateAirplane();}

//! network message indices for AirplaneAuto class
class IndicesUpdatePositionAirplane : public IndicesUpdatePositionVehicle
{
	typedef IndicesUpdatePositionVehicle base;


public:
//	int turret;
	int thrustWanted;
	int elevatorWanted;
	int rudderWanted;
	int aileronWanted;
	int pilotBrake;

	//! Constructor
	IndicesUpdatePositionAirplane();
	NetworkMessageIndices *Clone() const {return new IndicesUpdatePositionAirplane;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesUpdatePositionAirplane::IndicesUpdatePositionAirplane()
{
//	turret = -1;
	thrustWanted = -1;
	elevatorWanted = -1;
	rudderWanted = -1;
	aileronWanted = -1;
	pilotBrake = -1;
}

void IndicesUpdatePositionAirplane::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);
//	SCAN(turret)
	SCAN(thrustWanted);
	SCAN(elevatorWanted);
	SCAN(rudderWanted);
	SCAN(aileronWanted);
	SCAN(pilotBrake);
}

//! Create network message indices for AirplaneAuto class
NetworkMessageIndices *GetIndicesUpdatePositionAirplane() {return new IndicesUpdatePositionAirplane();}

NetworkMessageFormat &AirplaneAuto::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	switch (cls)
	{
	case NMCUpdateGeneric:
		base::CreateFormat(cls, format);

		format.Add("pilotFlaps", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Position of flaps, wanted by pilot"), ET_ABS_DIF, ERR_COEF_MODE);
		format.Add("gearDammage", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Gear is damaged"), ET_ABS_DIF, ERR_COEF_MODE);
		format.Add("pilotGear", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Position of gear, wanted by pilot"), ET_ABS_DIF, ERR_COEF_MODE);

		// TODO: implementation
		break;
	case NMCUpdatePosition:
		base::CreateFormat(cls, format);
		// format.Add("turret", NDTObject, NCTNone, DEFVALUE_MSG(NMTUpdateTurret), ET_ABS_DIF, 1);
		format.Add("thrustWanted", NDTFloat, NCTFloat0To1, DEFVALUE(float, 0), DOC_MSG("Wanted engine thrust"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);
		format.Add("elevatorWanted", NDTFloat, NCTFloatM1ToP1, DEFVALUE(float, 0), DOC_MSG("Wanted elevator position"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);
		format.Add("rudderWanted", NDTFloat, NCTFloatM1ToP1, DEFVALUE(float, 0), DOC_MSG("Wanted rudder position"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);
		format.Add("aileronWanted", NDTFloat, NCTFloatM1ToP1, DEFVALUE(float, 0), DOC_MSG("Wanted aileron position"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);
		format.Add("pilotBrake", NDTFloat, NCTFloat0To1, DEFVALUE(float, 0), DOC_MSG("State of brake, wanted by pilot"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);

		break;
	default:
		base::CreateFormat(cls, format);
		break;
	}
	return format;
}


TMError AirplaneAuto::TransferMsg(NetworkMessageContext &ctx)
{
	switch (ctx.GetClass())
	{
	case NMCUpdateGeneric:
		TMCHECK(base::TransferMsg(ctx))
		{
			Assert(dynamic_cast<const IndicesUpdateAirplane *>(ctx.GetIndices()))
			const IndicesUpdateAirplane *indices = static_cast<const IndicesUpdateAirplane *>(ctx.GetIndices());

			ITRANSF(pilotFlaps);
			
			ITRANSF(gearDammage);
			ITRANSF(pilotGear);
			// TODO: implementation
		}
		break;
	case NMCUpdatePosition:
		{
			Assert(dynamic_cast<const IndicesUpdatePositionAirplane *>(ctx.GetIndices()))
			const IndicesUpdatePositionAirplane *indices = static_cast<const IndicesUpdatePositionAirplane *>(ctx.GetIndices());
			TMCHECK(base::TransferMsg(ctx))
			ITRANSF(thrustWanted);
			ITRANSF(elevatorWanted);
			ITRANSF(rudderWanted);
			ITRANSF(aileronWanted);
			ITRANSF(pilotBrake);
/*
			if (ctx.IsSending() || !(GunnerUnit() && GunnerUnit()->GetPerson()->IsLocal()))
				TMCHECK(ctx.IdxTransferObject(indices->turret, _turret))
			_turret.Stabilize
			(
				this, Type()->_turret,
				oldTrans, Orientation()
			);
*/
		}
		break;
	default:
		return base::TransferMsg(ctx);
	}
	return TMOK;
}

float AirplaneAuto::CalculateError(NetworkMessageContext &ctx)
{
	float error = 0;
	switch (ctx.GetClass())
	{
	case NMCUpdateGeneric:
		error += base::CalculateError(ctx);
		{
			Assert(dynamic_cast<const IndicesUpdateAirplane *>(ctx.GetIndices()))
			const IndicesUpdateAirplane *indices = static_cast<const IndicesUpdateAirplane *>(ctx.GetIndices());

			ICALCERR_ABSDIF(int, pilotFlaps, ERR_COEF_MODE)
			ICALCERR_NEQ(bool, gearDammage, ERR_COEF_MODE)
			ICALCERR_NEQ(bool, pilotGear, ERR_COEF_MODE)

			// TODO: implementation
		}
		break;
	case NMCUpdatePosition:
		{
			error += 	base::CalculateError(ctx);
			Assert(dynamic_cast<const IndicesUpdatePositionAirplane *>(ctx.GetIndices()))
			const IndicesUpdatePositionAirplane *indices = static_cast<const IndicesUpdatePositionAirplane *>(ctx.GetIndices());

			ICALCERR_ABSDIF(float, thrustWanted, ERR_COEF_VALUE_MAJOR)

			ICALCERR_ABSDIF(float, elevatorWanted, ERR_COEF_VALUE_MAJOR)
			ICALCERR_ABSDIF(float, rudderWanted, ERR_COEF_VALUE_MAJOR)
			ICALCERR_ABSDIF(float, aileronWanted, ERR_COEF_VALUE_MAJOR)
			ICALCERR_ABSDIF(float, pilotBrake, ERR_COEF_VALUE_MAJOR)
/*
			int index = indices->turret;
			if (index >= 0)
			{
				NetworkMessageFormatBase *format = const_cast<NetworkMessageFormatBase *>(ctx.GetFormat());
				NetworkMessageFormatItem &item = format->GetItem(index);
				int type = static_cast< NetworkDataTyped<int> *>(item.defValue.GetRef())->value;
				NetworkMessageFormatBase *subformat = ctx.GetComponent()->GetFormat((NetworkMessageType)type);
				if (subformat)
				{
					NetworkData *val = ctx.GetMessage()->values[index];
					NetworkMessage &submsg = static_cast< NetworkDataTyped<NetworkMessage> *>(val)->value;
					NetworkMessageContext subctx(&submsg, subformat, ctx);
					error += _turret.CalculateError(subctx);
				}
			}
*/
		}
		break;
	default:
		error += base::CalculateError(ctx);
		break;
	}
	return error;
}

