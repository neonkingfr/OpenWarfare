// parameter file parser

#include "wpch.hpp"
#include "paramFileExt.hpp"

#include <El/QStream/QBStream.hpp>

//#include "strIncl.hpp"
#include "stringtableExt.hpp"
#include "mbcs.hpp"

#ifndef ACCESS_ONLY
#include "paramArchive.hpp"
#endif

FontID GetFontID( RString baseName )
{
	int langID = English;
	const ParamEntry *cls = (Pars >> "CfgFonts").FindEntry(GLanguage);
	if (cls)
	{
		const ParamEntry *entry = cls->FindEntry(baseName);
		if (entry)
		{
			baseName = *entry;
			langID = GetLangID();
		}
	}
	return FontID(GetDefaultName(baseName,"fonts\\",""), langID);
}

PackedColor GetPackedColor(const ParamEntry &entry)
{
	if (entry.GetSize() != 4)
	{
		// ErrorMessage("Config: '%s' not an ARGB color",(const char *)GetName());
		return PackedWhite;
	}
	int r8 = toInt(entry[0].GetFloat() * 255);
	int g8 = toInt(entry[1].GetFloat() * 255);
	int b8 = toInt(entry[2].GetFloat() * 255);
	int a8 = toInt(entry[3].GetFloat() * 255);
	saturate(r8, 0, 255);
	saturate(g8, 0, 255);
	saturate(b8, 0, 255);
	saturate(a8, 0, 255);
	return PackedColor(r8, g8, b8, a8);
}

Color GetColor(const ParamEntry &entry)
{
	if (entry.GetSize() != 4)
	{
		//ErrorMessage("Config: '%s' not an ARGB color",(const char *)GetName());
		return HWhite;
	}
	return Color(entry[0].GetFloat(), entry[1].GetFloat(), entry[2].GetFloat(), entry[3].GetFloat());
}

bool GetValue(SoundPars &pars, const ParamEntry &entry)
{
	if (entry.GetSize() < 3)
	{
		const static SoundPars nil;
		//ErrorMessage("Config: '%s' not sound parameters.",(const char *)GetName());
		pars = nil;
		return false;
	}
	pars.name = GetSoundName(entry[0]);
	pars.vol = entry[1].GetFloat();
	pars.freq = entry[2].GetFloat();
	pars.freqRnd = 0;
	pars.volRnd = 0.05;
	return true;
}

bool GetValue(SoundPars &pars, const IParamArrayValue &entry)
{
	if (entry.GetItemCount() < 3)
	{
		const static SoundPars nil;
		//ErrorMessage("Config: '%s' not sound parameters.",(const char *)GetName());
		pars = nil;
		return false;
	}
	pars.name = GetSoundName(entry[0]);
	pars.vol = entry[1].GetFloat();
	pars.freq = entry[2].GetFloat();
	pars.freqRnd = 0;
	pars.volRnd = 0.05;
	return true;
}

bool GetValue(PackedColor &val, const ParamEntry &entry)
{
	if (entry.GetSize() != 4)
	{
		val = PackedWhite;
		return false;
	}
	val = PackedColor(Color(entry[0].GetFloat(), entry[1].GetFloat(), entry[2].GetFloat(), entry[3].GetFloat()));
	return true;
}

bool GetValue(Color &val, const ParamEntry &entry)
{
	if (entry.GetSize() != 4)
	{
		val = HWhite;
		return false;
	}
	val = Color(entry[0].GetFloat(), entry[1].GetFloat(), entry[2].GetFloat(), entry[3].GetFloat());
	return true;
}

RString GetDefaultName( RString baseName, const char *dDir, const char *dExt )
{
	if( !baseName || !baseName[0] ) return "";
	char buf[256];
	const char *ext=strchr(baseName,'.');
	*buf=0;
	if( baseName[0]!='\\' )
	{
		strcat(buf,dDir);
		strcat(buf,baseName);
	}
	else
	{
		strcat(buf,baseName+1);
	}
	if( !ext ) strcat(buf,dExt);
	strlwr(buf);
	return buf;
	
}

RString GetShapeName( RString baseName )
{
	return GetDefaultName(baseName,"data3d\\",".p3d");
}
RString GetAnimationName( RString baseName )
{
	return GetDefaultName(baseName,"anim\\",".rtm");
}
RString GetPictureName( RString baseName )
{
	return GetDefaultName(baseName,"data\\",".paa");
}
RString GetSoundName( RString baseName )
{
	return GetDefaultName(baseName,"sound\\",".wss");
}

#ifndef ACCESS_ONLY
LSError SoundPars::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("name", name, 1))
	CHECK(ar.Serialize("vol", vol, 1))
	CHECK(ar.Serialize("freq", freq, 1))
	CHECK(ar.Serialize("volRnd", volRnd, 1))
	CHECK(ar.Serialize("freqRnd", freqRnd, 1))
	return LSOK;
}
#endif

/*!
\patch 1.21 Date 08/21/2001 by Jirka
- Added: allow new worlds in AddOns
*/
RString GetWorldName( RString baseName )
{
#if _FORCE_DEMO_ISLAND
	RString demo = Pars >> "CfgWorlds" >> "demoWorld";
	if (stricmp(baseName, demo) != 0)
		WarningMessage(LocalizeString(IDS_MSG_NO_WORLD), (const char *)baseName);
	const ParamEntry *entry = (Pars >> "CfgWorlds").FindEntry(demo);
#else
	const ParamEntry *entry = (Pars >> "CfgWorlds").FindEntry(baseName);
	if (!entry)
	{
		baseName = Pars >> "CfgWorlds" >> "initWorld";
		entry = (Pars >> "CfgWorlds").FindEntry(baseName);
	}
#endif
	RString world = (*entry) >> "worldName";
	return GetDefaultName(world,"worlds\\",".wrp");
}

static RString PathFirstFolder(const char *path)
{
	const char *next = strchr(path,'/');
	if (!next) return "";
	return RString(path,next-path);
}

#if 0

//! calculate checksum

/*!
simple checksum calculation.
Interface similiar to CRCCalculator (incremental interface).
*/

class ChecksumCalculator
{
	unsigned long _sum;

	public:
	//! Constructor
	ChecksumCalculator() {Reset();}
	//! initialize
	void Reset() {_sum=0;}
	//! add memory block
	void Add(const void *data, int len);
	//! add signle character
	void Add(char c){_sum += c;}
	//! get result of all Add operations since Reset()
	unsigned long GetResult() const {return _sum;}
};

void ChecksumCalculator::Add(const void *data, int len)
{
	int sum = _sum;
	char *cdata = (char *)data;
	while (--len>=0)
	{
		sum += *cdata++;
	}
	_sum = sum;
}

class PASumCalculator: public ChecksumCalculator
{
};

#else

#include "crc.hpp"

class PASumCalculator: public CRCCalculator
{
};

#endif

const ParamEntry *FindConfigParamEntry(const char *path)
{
	if (*path==0)
	{
		return NULL;
	}
	// check path base
	const ParamEntry *entry = NULL;
	RString base = PathFirstFolder(path);
	if (!strcmpi(base,"cfg"))
	{
		entry = &Pars;
	}
	else if (!strcmpi(base,"rsc"))
	{
		entry = &Res;
	}
	else
	{
		LogF("Invalid base in %s",path);
		return NULL;
	}
	path += strlen(base)+1;

	while (strlen(path)>0)
	{
		RString base = PathFirstFolder(path);
		if (base.GetLength()<=0)
		{
			ParamEntry *nEntry = entry->FindEntry(path);
			if (!nEntry) break;
			if (!nEntry->IsClass()) break;
			entry = nEntry;
			break;
		}
		else
		{
			ParamEntry *nEntry = entry->FindEntry(base);
			if (!nEntry) break;
			if (!nEntry->IsClass()) break;
			entry = nEntry;
			path += strlen(base)+1;
		}
	}
	return entry;
}
/*!
\patch 1.12 Date 08/06/2001 by Ondra
- Added: MP, Anticheat: Config integrity verification.
*/
unsigned int CalculateConfigCRC(const char *path)
{
	// check path base
	const ParamEntry *entry = FindConfigParamEntry(path);
	if (!entry)
	{
		PASumCalculator sum;
		sum.Reset();
		Pars.CalculateCheckValue(sum);
		Res.CalculateCheckValue(sum);
		return sum.GetResult();
	}

	PASumCalculator sum;
	sum.Reset();
	entry->CalculateCheckValue(sum);
	return sum.GetResult();
}

//! class of callback functions
class PASumCalculatorFunctions : public CRCFunctions
{
public:
	//! callback function to add buffer to CRC sum
	virtual void Add(PASumCalculator &sum, const void *buffer, int size);

	PASumCalculatorFunctions() {ParamFile::SetDefaultCRCFunctions(this);}
} GPASumCalculatorFunctions;

void PASumCalculatorFunctions::Add(PASumCalculator &sum, const void *buffer, int size)
{
	sum.Add(buffer, size);
}
