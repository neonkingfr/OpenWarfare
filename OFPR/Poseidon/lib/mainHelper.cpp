#include "wpch.hpp"
#include "keyInput.hpp"
#include "world.hpp"
#include <El/ParamFile/paramFile.hpp>
#include <Es/Common/win.h>
#if defined _WIN32 && !defined _XBOX
  #include <mmsystem.h>
#endif
#ifdef _WIN32
  #include "Joystick.hpp"
#endif
#include "resincl.hpp"
#include "dikCodes.h"
#include "chat.hpp"
#include "appFrameExt.hpp"

#include "stringtableExt.hpp"

Input GInput;

#ifdef _WIN32
SRef<Joystick> JoystickDev;
#endif

UserActionDesc::UserActionDesc(const char *n, int &d, int first, ...)
: desc(d)
{
	name = n;
	axis = false;

	keys.Resize(0);
	va_list arglist;
	va_start(arglist, first);
	int key = first;
	while (key != -1)
	{
		keys.Add(key);
		key = va_arg(arglist, int);
	}
	va_end(arglist);
	keys.Compact();
}

UserActionDesc::UserActionDesc(bool a, const char *n, int &d, int first, ...)
: desc(d)
{
	name = n;
	axis = a;

	keys.Resize(0);
	va_list arglist;
	va_start(arglist, first);
	int key = first;
	while (key != -1)
	{
		keys.Add(key);
		key = va_arg(arglist, int);
	}
	va_end(arglist);
	keys.Compact();
}

#ifndef _XBOX
UserActionDesc Input::userActionDesc[] =
{
	UserActionDesc("MoveForward", IDS_USRACT_MOVE_FORWARD, DIK_W, DIK_UP, -1),
	UserActionDesc("MoveBack", IDS_USRACT_MOVE_BACK, DIK_S, DIK_DOWN, -1),
	UserActionDesc("TurnLeft", IDS_USRACT_TURN_LEFT, DIK_A, DIK_LEFT, -1),
	UserActionDesc("TurnRight", IDS_USRACT_TURN_RIGHT, DIK_D, DIK_RIGHT, -1),
	UserActionDesc("MoveUp", IDS_USRACT_MOVE_UP, DIK_Q, DIK_PRIOR, -1),
	UserActionDesc("MoveDown", IDS_USRACT_MOVE_DOWN, DIK_Z, DIK_NEXT, -1),
	UserActionDesc("MoveFastForward", IDS_USRACT_FAST_FORWARD, DIK_E, -1),
	UserActionDesc("MoveSlowForward", IDS_USRACT_SLOW_FORWARD, DIK_Q, -1),
	UserActionDesc("MoveLeft", IDS_USRACT_MOVE_LEFT, DIK_X, DIK_DELETE, -1),
	UserActionDesc("MoveRight", IDS_USRACT_MOVE_RIGHT, DIK_C, DIK_END, -1),
	UserActionDesc("ToggleWeapons", IDS_USRACT_TOGGLE_WEAPONS, DIK_SPACE, DIK_RCONTROL, INPUT_DEVICE_STICK + 2, -1),
	UserActionDesc("Fire", IDS_USRACT_FIRE, DIK_LCONTROL, INPUT_DEVICE_STICK, -1),
	UserActionDesc("ReloadMagazine", IDS_USRACT_RELOAD_MAGAZINE, DIK_R, DIK_HOME, -1),
	UserActionDesc("LockTargets", IDS_USRACT_LOCK_TARGETS, DIK_TAB, INPUT_DEVICE_STICK + 1, -1),
	UserActionDesc("LockTarget", IDS_USRACT_LOCK_OR_ZOOM, INPUT_DEVICE_MOUSE + 1, -1),
	UserActionDesc("RevealTarget", IDS_USRACT_REVEAL_TARGET, INPUT_DEVICE_MOUSE + 1, -1),
	UserActionDesc("PrevAction", IDS_USRACT_PREV_ACTION, DIK_LBRACKET, INPUT_DEVICE_STICK+6, -1),
	UserActionDesc("NextAction", IDS_USRACT_NEXT_ACTION, DIK_RBRACKET, INPUT_DEVICE_STICK+4, -1),
	UserActionDesc("Action", IDS_USRACT_ACTION, DIK_RETURN, INPUT_DEVICE_MOUSE + 2, INPUT_DEVICE_STICK+5, -1),
	UserActionDesc("Headlights", IDS_USRACT_HEADLIGHTS, DIK_L, -1),
	UserActionDesc("NightVision", IDS_USRACT_NIGHT_VISION, DIK_N, -1),
	UserActionDesc("Binocular", IDS_USRACT_BINOCULAR, DIK_B, -1),
	UserActionDesc("Handgun", IDS_USRACT_HANDGUN, DIK_Y, -1),
	UserActionDesc("Compass", IDS_USRACT_COMPASS, DIK_G, -1),
	UserActionDesc("Watch", IDS_USRACT_WATCH, DIK_T, -1),
	UserActionDesc("Map", IDS_USRACT_MAP, DIK_M, -1),
	UserActionDesc("Help", IDS_USRACT_HELP, DIK_H, -1),
	UserActionDesc("TimeInc", IDS_USRACT_TIME_INC, DIK_EQUALS, -1),
	UserActionDesc("TimeDec", IDS_USRACT_TIME_DEC, DIK_MINUS, -1),
	UserActionDesc("Optics", IDS_USRACT_OPTICS, DIK_V, DIK_NUMPAD0, -1),
	UserActionDesc("PersonView", IDS_USRACT_PERSON_VIEW, DIK_NUMPADENTER, -1),
	UserActionDesc("TacticalView", IDS_USRACT_TACTICAL_VIEW, DIK_DECIMAL, -1),
	UserActionDesc("ZoomIn", IDS_USRACT_ZOOM_IN, DIK_ADD, INPUT_DEVICE_STICK+3, -1),
	UserActionDesc("ZoomOut", IDS_USRACT_ZOOM_OUT, DIK_SUBTRACT, -1),
	UserActionDesc("LookAround", IDS_USRACT_LOOK_ARROUND, DIK_LMENU, -1),
	UserActionDesc("LookAroundToggle", IDS_USRACT_LOOK_ARROUND_TOGGLE, DIK_MULTIPLY, -1),
	UserActionDesc("LookLeftDown", IDS_USRACT_LOOK_LEFT_DOWN, DIK_NUMPAD1, INPUT_DEVICE_STICK_POV + 5, -1),
	UserActionDesc("LookDown", IDS_USRACT_LOOK_DOWN, DIK_NUMPAD2, INPUT_DEVICE_STICK_POV + 4, -1),
	UserActionDesc("LookRightDown", IDS_USRACT_LOOK_RIGHT_DOWN, DIK_NUMPAD3, INPUT_DEVICE_STICK_POV + 3, -1),
	UserActionDesc("LookLeft", IDS_USRACT_LOOK_LEFT, DIK_NUMPAD4, INPUT_DEVICE_STICK_POV + 6, -1),
	UserActionDesc("LookCenter", IDS_USRACT_LOOK_CENTER, DIK_NUMPAD5, -1),
	UserActionDesc("LookRight", IDS_USRACT_LOOK_RIGHT, DIK_NUMPAD6, INPUT_DEVICE_STICK_POV + 2, -1),
	UserActionDesc("LookLeftUp", IDS_USRACT_LOOK_LEFT_UP, DIK_NUMPAD7, INPUT_DEVICE_STICK_POV + 7, -1),
	UserActionDesc("LookUp", IDS_USRACT_LOOK_UP, DIK_NUMPAD8, INPUT_DEVICE_STICK_POV + 0, -1),
	UserActionDesc("LookRightUp", IDS_USRACT_LOOK_RIGHT_UP, DIK_NUMPAD9, INPUT_DEVICE_STICK_POV + 1, -1),
	UserActionDesc("PrevChannel", IDS_USRACT_PREV_CHANNEL, DIK_COMMA, -1),
	UserActionDesc("NextChannel", IDS_USRACT_NEXT_CHANNEL, DIK_PERIOD, -1),
	UserActionDesc("Chat", IDS_USRACT_CHAT, DIK_SLASH, -1),
	UserActionDesc("VoiceOverNet", IDS_USRACT_VOICE_OVER_NET, DIK_CAPITAL, -1),
	UserActionDesc("NetworkStats", IDS_USRACT_NETWORK_STATS, DIK_I, -1),
	UserActionDesc("NetworkPlayers", IDS_USRACT_NETWORK_PLAYERS, DIK_P, -1),
	UserActionDesc("SelectAll", IDS_USRACT_SELECT_ALL, DIK_GRAVE, -1),
	UserActionDesc("Turbo", IDS_USRACT_TURBO, DIK_LSHIFT, DIK_RSHIFT, -1),
	UserActionDesc("Walk", IDS_USRACT_WALK, DIK_F, -1),
	UserActionDesc(true, "AxisTurn", IDS_USRACT_TURN, INPUT_DEVICE_STICK_AXIS+0, -1),
	UserActionDesc(true, "AxisDive", IDS_USRACT_ACCELERATE, INPUT_DEVICE_STICK_AXIS+1, -1),
	UserActionDesc(true, "AxisRudder", IDS_USRACT_RUDDER, INPUT_DEVICE_STICK_AXIS+5, -1),
	UserActionDesc(true, "AxisThrust", IDS_USRACT_THRUST, INPUT_DEVICE_STICK_AXIS+2, INPUT_DEVICE_STICK_AXIS+6, -1),

	UserActionDesc("AimUp", IDS_USRACT_AIM_UP, -1),
	UserActionDesc("AimDown", IDS_USRACT_AIM_DOWN, -1),
	UserActionDesc("AimLeft", IDS_USRACT_AIM_LEFT, -1),
	UserActionDesc("AimRight", IDS_USRACT_AIM_RIGHT, -1),
#if _ENABLE_CHEATS
	UserActionDesc("Cheat1", IDS_USRACT_CHEAT_1, DIK_RWIN, -1),
	UserActionDesc("Cheat2", IDS_USRACT_CHEAT_2, DIK_RMENU, -1),
#endif
};
#else

// easy-to-read names of XBox controller buttons and axes

#define INPUT_XBOX_A INPUT_DEVICE_STICK
#define INPUT_XBOX_B INPUT_DEVICE_STICK + 1
#define INPUT_XBOX_X INPUT_DEVICE_STICK + 2
#define INPUT_XBOX_Y INPUT_DEVICE_STICK + 3
#define INPUT_XBOX_Up INPUT_DEVICE_STICK + 4
#define INPUT_XBOX_Down INPUT_DEVICE_STICK + 5
#define INPUT_XBOX_Left INPUT_DEVICE_STICK + 6
#define INPUT_XBOX_Right INPUT_DEVICE_STICK + 7
#define INPUT_XBOX_Start INPUT_DEVICE_STICK + 8
#define INPUT_XBOX_Back INPUT_DEVICE_STICK + 9
#define INPUT_XBOX_Black INPUT_DEVICE_STICK + 10
#define INPUT_XBOX_White INPUT_DEVICE_STICK + 11
#define INPUT_XBOX_LeftTrigger INPUT_DEVICE_STICK + 12
#define INPUT_XBOX_RightTrigger INPUT_DEVICE_STICK + 13
#define INPUT_XBOX_LeftThumb INPUT_DEVICE_STICK + 14
#define INPUT_XBOX_RightThumb INPUT_DEVICE_STICK + 15
#define INPUT_XBOX_LeftThumbXRight INPUT_DEVICE_STICK + 16
#define INPUT_XBOX_LeftThumbYUp INPUT_DEVICE_STICK + 17
#define INPUT_XBOX_RightThumbXRight INPUT_DEVICE_STICK + 18
#define INPUT_XBOX_RightThumbYUp INPUT_DEVICE_STICK + 19
#define INPUT_XBOX_LeftThumbXLeft INPUT_DEVICE_STICK + 20
#define INPUT_XBOX_LeftThumbYDown INPUT_DEVICE_STICK + 21
#define INPUT_XBOX_RightThumbXLeft INPUT_DEVICE_STICK + 22
#define INPUT_XBOX_RightThumbYDown INPUT_DEVICE_STICK + 23

UserActionDesc Input::userActionDesc[] =
{
	UserActionDesc("MoveForward", IDS_USRACT_MOVE_FORWARD, -1),

	UserActionDesc("MoveBack", IDS_USRACT_MOVE_BACK, INPUT_XBOX_LeftThumbYDown, -1),
	UserActionDesc("TurnLeft", IDS_USRACT_TURN_LEFT, INPUT_XBOX_LeftThumbXLeft, -1),
	UserActionDesc("TurnRight", IDS_USRACT_TURN_RIGHT, INPUT_XBOX_LeftThumbXRight, -1),
	UserActionDesc("MoveUp", IDS_USRACT_MOVE_UP, INPUT_XBOX_Up, INPUT_XBOX_LeftThumb, -1),
	UserActionDesc("MoveDown", IDS_USRACT_MOVE_DOWN, INPUT_XBOX_Down, INPUT_XBOX_Y, -1),
	UserActionDesc("MoveFastForward", IDS_USRACT_FAST_FORWARD, INPUT_XBOX_LeftThumbYUp, -1),

	UserActionDesc("MoveSlowForward", IDS_USRACT_SLOW_FORWARD, -1),
	UserActionDesc("MoveLeft", IDS_USRACT_MOVE_LEFT, -1),
	UserActionDesc("MoveRight", IDS_USRACT_MOVE_RIGHT, -1),
	UserActionDesc("ToggleWeapons", IDS_USRACT_TOGGLE_WEAPONS, INPUT_XBOX_B, -1),
	UserActionDesc("Fire", IDS_USRACT_FIRE, DIK_LCONTROL, INPUT_XBOX_RightTrigger, -1),
	UserActionDesc("ReloadMagazine", IDS_USRACT_RELOAD_MAGAZINE, INPUT_XBOX_X, -1),
	UserActionDesc("LockTargets", IDS_USRACT_LOCK_TARGETS, -1),
	UserActionDesc("LockTarget", IDS_USRACT_LOCK_OR_ZOOM, INPUT_XBOX_LeftTrigger, -1),
	UserActionDesc("RevealTarget", IDS_USRACT_REVEAL_TARGET, INPUT_XBOX_LeftTrigger, -1),
	UserActionDesc("PrevAction", IDS_USRACT_PREV_ACTION, INPUT_XBOX_Left, -1),
	UserActionDesc("NextAction", IDS_USRACT_NEXT_ACTION, INPUT_XBOX_Right, -1),
	UserActionDesc("Action", IDS_USRACT_ACTION, DIK_RETURN, INPUT_XBOX_A, -1),
	UserActionDesc("Headlights", IDS_USRACT_HEADLIGHTS, -1),
	UserActionDesc("NightVision", IDS_USRACT_NIGHT_VISION, -1),
	UserActionDesc("Binocular", IDS_USRACT_BINOCULAR, -1),
	UserActionDesc("Compass", IDS_USRACT_COMPASS, -1),
	UserActionDesc("Watch", IDS_USRACT_WATCH, -1),
	UserActionDesc("Map", IDS_USRACT_MAP, INPUT_XBOX_White, -1),
	UserActionDesc("Help", IDS_USRACT_HELP, -1),
	UserActionDesc("TimeInc", IDS_USRACT_TIME_INC, -1),
	UserActionDesc("TimeDec", IDS_USRACT_TIME_DEC, -1),
	UserActionDesc("Optics", IDS_USRACT_OPTICS, INPUT_XBOX_RightThumb, -1),
	UserActionDesc("PersonView", IDS_USRACT_PERSON_VIEW, -1),
	UserActionDesc("TacticalView", IDS_USRACT_TACTICAL_VIEW, DIK_DECIMAL, -1),
	UserActionDesc("ZoomIn", IDS_USRACT_ZOOM_IN, -1),
	UserActionDesc("ZoomOut", IDS_USRACT_ZOOM_OUT, -1),
	UserActionDesc("LookAround", IDS_USRACT_LOOK_ARROUND, DIK_LMENU, -1),
	UserActionDesc("LookAroundToggle", IDS_USRACT_LOOK_ARROUND_TOGGLE, DIK_MULTIPLY, -1),
	UserActionDesc("LookLeftDown", IDS_USRACT_LOOK_LEFT_DOWN, INPUT_DEVICE_STICK_POV + 5, -1),
	UserActionDesc("LookDown", IDS_USRACT_LOOK_DOWN, INPUT_DEVICE_STICK_POV + 4, -1),
	UserActionDesc("LookRightDown", IDS_USRACT_LOOK_RIGHT_DOWN, INPUT_DEVICE_STICK_POV + 3, -1),
	UserActionDesc("LookLeft", IDS_USRACT_LOOK_LEFT, INPUT_DEVICE_STICK_POV + 6, -1),
	UserActionDesc("LookCenter", IDS_USRACT_LOOK_CENTER, DIK_NUMPAD5, -1),
	UserActionDesc("LookRight", IDS_USRACT_LOOK_RIGHT, INPUT_DEVICE_STICK_POV + 2, -1),
	UserActionDesc("LookLeftUp", IDS_USRACT_LOOK_LEFT_UP, INPUT_DEVICE_STICK_POV + 7, -1),
	UserActionDesc("LookUp", IDS_USRACT_LOOK_UP, INPUT_DEVICE_STICK_POV + 0, -1),
	UserActionDesc("LookRightUp", IDS_USRACT_LOOK_RIGHT_UP, INPUT_DEVICE_STICK_POV + 1, -1),
	UserActionDesc("PrevChannel", IDS_USRACT_PREV_CHANNEL, DIK_COMMA, -1),
	UserActionDesc("NextChannel", IDS_USRACT_NEXT_CHANNEL, DIK_PERIOD, -1),
	UserActionDesc("Chat", IDS_USRACT_CHAT, DIK_SLASH, -1),
	UserActionDesc("VoiceOverNet", IDS_USRACT_VOICE_OVER_NET, DIK_CAPITAL, -1),
	UserActionDesc("NetworkStats", IDS_USRACT_NETWORK_STATS, DIK_I, -1),
	UserActionDesc("NetworkPlayers", IDS_USRACT_NETWORK_PLAYERS, DIK_P, -1),
	UserActionDesc("SelectAll", IDS_USRACT_SELECT_ALL, DIK_GRAVE, -1),
	UserActionDesc("Turbo", IDS_USRACT_TURBO, DIK_LSHIFT, DIK_RSHIFT, -1),
	UserActionDesc("Walk", IDS_USRACT_WALK, DIK_F, -1),
	UserActionDesc(true, "AxisTurn", IDS_USRACT_TURN, INPUT_DEVICE_STICK_AXIS+0, -1),
	UserActionDesc(true, "AxisDive", IDS_USRACT_ACCELERATE, INPUT_DEVICE_STICK_AXIS+1, -1),
	UserActionDesc(true, "AxisRudder", IDS_USRACT_RUDDER, INPUT_DEVICE_STICK_AXIS+5, -1),
	UserActionDesc(true, "AxisThrust", IDS_USRACT_THRUST, INPUT_DEVICE_STICK_AXIS+2, INPUT_DEVICE_STICK_AXIS+6, -1),

	UserActionDesc("AimUp", IDS_USRACT_AIM_UP, INPUT_XBOX_RightThumbYUp, -1),
	UserActionDesc("AimDown", IDS_USRACT_AIM_DOWN, INPUT_XBOX_RightThumbYDown, -1),
	UserActionDesc("AimLeft", IDS_USRACT_AIM_LEFT, INPUT_XBOX_RightThumbXLeft, -1),
	UserActionDesc("AimRight", IDS_USRACT_AIM_RIGHT, INPUT_XBOX_RightThumbXRight, -1),

#if _ENABLE_CHEATS
	UserActionDesc("Cheat1", IDS_USRACT_CHEAT_1, DIK_RWIN, -1),
	UserActionDesc("Cheat2", IDS_USRACT_CHEAT_2, DIK_RMENU, -1),
#endif
};
#endif

Input::Input()
{
	gameFocusLost = 0;

	for( int k=0; k<256; k++ )
	{
		keyPressed[k]=0;
		keys[k]=0;
		keysToDo[k]=false;
	}
	cursorX=0,cursorY=0; // cursor position

	mouseLToDo=false;
	mouseRToDo=false;
	mouseMToDo=false;

	// TODO: remove lastKeyTime and lastMouseTime members
	revMouse = false;
	joystickEnabled=true;
	mouseButtonsReversed = false;
	mouseSensitivityX = 1.0;
	mouseSensitivityY = 1.0;

	ForgetKeys();
	InitKeys();
}

void Input::ForgetKeys()
{
	for( int k=0; k<256; k++ )
	{
		keysToDo[k]=false;
		keyPressed[k]=0; // time stamp of last key pressed event
		keys[k]=0; // integrated key time (as part of deltaT)
	}
	for (int i=0; i<JoystickNAxes; i++)
	{
		jAxisLastActive[i] = Glob.uiTime-120;
		jAxisBigLastActive[i] = Glob.uiTime-120;
	}

	joystickMoveLastActive=Glob.uiTime-1;
	keyboardMoveLastActive=Glob.uiTime+5;
	mouseCursorLastActive=Glob.uiTime-1;
	mouseTurnLastActive=Glob.uiTime-1;
	keyboardCursorLastActive=Glob.uiTime;
	keyboardTurnLastActive=Glob.uiTime;
	cursorMovedX=cursorMovedY=cursorMovedZ=0;
	cheatActive = CheatNone;
	awaitCheat = false;
	cheatInProgress = RString();
}

void Input::InitKeys()
{
	for (int i=0; i<UAN; i++)
	{
		userKeys[i] = userActionDesc[i].keys;
	}
}

RString GetUserParams();

void Input::LoadKeys()
{
	InitKeys();

	ParamFile userCfg;
	userCfg.Parse(GetUserParams());
	for (int i=0; i<UAN; i++)
	{
		RString name = RString("key") + RString(userActionDesc[i].name);
		const ParamEntry *cfgKey = userCfg.FindEntry(name);
		if (cfgKey)
		{
			int m = (*cfgKey).GetSize();
			userKeys[i].Resize(0);
			for (int j=0; j<m; j++)
			{
				int key = (*cfgKey)[j];
				if (key != -1) userKeys[i].Add(key);
			}
			userKeys[i].Compact();
		}
	}
	
	const ParamEntry *entry = userCfg.FindEntry("revMouse");
	if (entry) revMouse = *entry;
	entry = userCfg.FindEntry("mouseButtonsReversed");
	if (entry) mouseButtonsReversed = *entry;
	entry = userCfg.FindEntry("joystickEnabled");
	if (entry) joystickEnabled = *entry;
	entry = userCfg.FindEntry("mouseSensitivityX");
	if (entry) mouseSensitivityX = *entry;
	entry = userCfg.FindEntry("mouseSensitivityY");
	if (entry) mouseSensitivityY = *entry;
}

void Input::SaveKeys()
{
	ParamFile userCfg;
	userCfg.Parse(GetUserParams());
	for (int i=0; i<UAN; i++)
	{
		RString name = RString("key") + RString(userActionDesc[i].name);
		ParamEntry *entry = userCfg.AddArray(name);
		entry->Clear();
		for (int j=0; j<userKeys[i].Size(); j++)
			entry->AddValue(userKeys[i][j]);
	}
	userCfg.Add("revMouse", revMouse);
	userCfg.Add("joystickEnabled", joystickEnabled);
	userCfg.Add("mouseButtonsReversed", mouseButtonsReversed);
	userCfg.Add("mouseSensitivityX", mouseSensitivityX);
	userCfg.Add("mouseSensitivityY", mouseSensitivityY);
	userCfg.Save(GetUserParams());
}

float Input::GetKey(int dik, bool checkFocus) const
{
	if (checkFocus && gameFocusLost>0) return 0;
#if _ENABLE_CHEATS
	if (cheat1 || cheat2) return 0;
#endif
	if (awaitCheat) return 0;
	if (dik<0) return 0;

	return keys[dik];
}

bool Input::GetKeyToDo(int dik, bool reset, bool checkFocus)
{
	if (checkFocus && gameFocusLost>0) return false;
#if _ENABLE_CHEATS
	if (cheat1 || cheat2) return 0;
#endif
	if (awaitCheat) return 0;
	if (dik<0) return 0;
	bool ret = keysToDo[dik];
	if (reset) keysToDo[dik] = false;
	return ret;
}

#if _ENABLE_CHEATS

float Input::GetCheat1(int dik) const
{
	if (!cheat1 || cheat2 || dik < 0) return 0;
	return keys[dik];
}

bool Input::GetCheat1ToDo(int dik, bool reset)
{
	if (!cheat1 || cheat2 || dik<0) return false;
	bool ret = keysToDo[dik];
	if (reset) keysToDo[dik] = false;
	return ret;
}

float Input::GetCheat2(int dik) const
{
	if (cheat1 || !cheat2 || dik < 0) return 0;
	return keys[dik];
}

bool Input::GetCheat2ToDo(int dik, bool reset)
{
	if (cheat1 || !cheat2 || dik<0) return false;
	bool ret = keysToDo[dik];
	if (reset) keysToDo[dik] = false;
	return ret;
}

#endif

float Input::GetMouseButton(int index, bool checkFocus) const
{
	if (checkFocus && gameFocusLost > 0) return 0;
	if (index < 0) return 0;

	return mouseButtons[index];
}

bool Input::GetMouseButtonToDo(int index, bool reset, bool checkFocus)
{
	if (checkFocus && gameFocusLost > 0) return 0;
	if (index < 0) return 0;

	bool ret = mouseButtonsToDo[index];
	if (reset) mouseButtonsToDo[index] = false;
	return ret;
}

float Input::GetJoystickButton(int index, bool checkFocus) const
{
	if (checkFocus && gameFocusLost > 0) return 0;
	if (index < 0) return 0;

	return stickButtons[index];
}

bool Input::GetJoystickButtonToDo(int index, bool reset, bool checkFocus)
{
	if (checkFocus && gameFocusLost > 0) return 0;
	if (index < 0) return 0;

	bool ret = stickButtonsToDo[index];
	if (reset) stickButtonsToDo[index] = false;
	return ret;
}

float Input::GetStickForward() const
{
	return -GetAction(UAAxisDive,true);
}
float Input::GetStickLeft() const
{
	return -GetAction(UAAxisTurn,true);
}
float Input::GetStickThrust() const
{
	return GetAction(UAAxisThrust,true);
}
float Input::GetStickRudder() const
{
	return GetAction(UAAxisRudder,true);
}

float Input::GetAction(UserAction action, bool checkFocus) const
{
	float sum = 0;
	for (int i=0; i<userKeys[action].Size(); i++)
	{
		int key = userKeys[action][i];
		switch (key & INPUT_DEVICE_MASK)
		{
		case INPUT_DEVICE_KEYBOARD:
			sum += GetKey(key - INPUT_DEVICE_KEYBOARD, checkFocus);
			break;
		case INPUT_DEVICE_MOUSE:
			sum += GetMouseButton(key - INPUT_DEVICE_MOUSE, checkFocus);
			break;
		case INPUT_DEVICE_STICK:
			sum += GetJoystickButton(key - INPUT_DEVICE_STICK, checkFocus);
			break;
		case INPUT_DEVICE_STICK_AXIS:
			{
				int offset = key-INPUT_DEVICE_STICK_AXIS;
				Assert (offset>=0);
				Assert (offset<N_JOYSTICK_AXES);
				sum += stickAxis[offset];
			}
			break;
		case INPUT_DEVICE_STICK_POV:
			{
				int offset = key-INPUT_DEVICE_STICK_POV;
				Assert (offset>=0);
				Assert (offset<N_JOYSTICK_POV);
				sum += stickPov[offset];
			}
			break;
		}
	}
	return sum;
}

bool Input::GetActionToDo(UserAction action, bool reset, bool checkFocus)
{
	// reset action, no event
	if (actionDone[action]) return false;
	if (reset) actionDone[action] = true;

	bool found = false;
	for (int i=0; i<userKeys[action].Size(); i++)
	{
		int key = userKeys[action][i];
		switch (key & INPUT_DEVICE_MASK)
		{
		case INPUT_DEVICE_KEYBOARD:
			if (GetKeyToDo(key - INPUT_DEVICE_KEYBOARD, false, checkFocus)) found = true;
			break;
		case INPUT_DEVICE_MOUSE:
			if (GetMouseButtonToDo(key - INPUT_DEVICE_MOUSE, false, checkFocus)) found = true;
			break;
		case INPUT_DEVICE_STICK:
			if (GetJoystickButtonToDo(key - INPUT_DEVICE_STICK, false, checkFocus)) found = true;
			break;
		case INPUT_DEVICE_STICK_POV:
			if (!checkFocus || gameFocusLost<=0)
			{
				int offset = key-INPUT_DEVICE_STICK_POV;
				Assert (offset>=0);
				Assert (offset<N_JOYSTICK_POV);
				if (stickPovToDo[offset]) found = true;
			}
			break;
		}
	}
	return found;
}

bool Input::GetFire(bool checkFocus) const
{
	if (checkFocus && gameFocusLost>0) return false;
	return fire;
}

bool Input::GetFireToDo(bool reset, bool checkFocus)
{
	if (checkFocus && gameFocusLost>0) return false;
	bool ret = fireToDo;
	if (reset) fireToDo = false;
	return ret;
}

bool Input::GetMouseL(bool checkFocus)
{
	if (checkFocus && gameFocusLost>0) return false;
	return mouseL;
}
bool Input::GetMouseR(bool checkFocus)
{
	if (checkFocus && gameFocusLost>0) return false;
	return mouseR;
}
bool Input::GetMouseM(bool checkFocus)
{
	if (checkFocus && gameFocusLost>0) return false;
	return mouseM;
}
bool Input::GetMouseLToDo(bool reset, bool checkFocus)
{
	if (checkFocus && gameFocusLost>0) return false;
	bool ret = mouseLToDo;
	if (reset)
	{
/*
		if (mouseLToDo)
			LogF("Processed");
*/
		mouseLToDo = false;
	}
	return ret;
}
bool Input::GetMouseRToDo(bool reset, bool checkFocus)
{
	if (checkFocus && gameFocusLost>0) return false;
	bool ret = mouseRToDo;
	if (reset) mouseRToDo = false;
	return ret;
}
bool Input::GetMouseMToDo(bool reset, bool checkFocus)
{
	if (checkFocus && gameFocusLost>0) return false;
	bool ret = mouseMToDo;
	if (reset) mouseMToDo = false;
	return ret;
}
bool Input::GetMouseDClickToDo(bool reset, bool checkFocus)
{
	if (checkFocus && gameFocusLost>0) return false;
	bool ret = mouseDClickToDo;
	if (reset) mouseDClickToDo = false;
	return ret;
}

bool Input::JoystickThurstActive() const
{
	if (!joystickEnabled) return false;
	return joystickThrustLastActive>keyboardThrustLastActive;
}

bool Input::JoystickActive() const
{
	//return joystickMoveLastActive>=keyboardMoveLastActive;
	if (!joystickEnabled) return false;
	if (keyboardMoveLastActive>=joystickMoveLastActive) return false;
	if (!lookAroundEnabled && mouseCursorLastActive > joystickMoveLastActive) return false;
	return true;
}

bool Input::MouseCursorActive() const
{
	if( keyboardCursorLastActive>mouseCursorLastActive) return false;
	return true;
}

bool Input::MouseTurnActive() const
{
	if (lookAroundEnabled) return false;
	if (keyboardCursorLastActive>mouseCursorLastActive) return false;
	if (keyboardTurnLastActive>mouseTurnLastActive) return false;
	if (joystickMoveLastActive>mouseTurnLastActive) return false;
	return true;
}

DifficultyDesc Config::diffDesc[DTN] = 
{
	DifficultyDesc("Armor", IDS_DIFF_ARMOR, true, false, false),
	DifficultyDesc("FriendlyTag", IDS_DIFF_FRIENDLY_TAG, true, false, false),
	DifficultyDesc("EnemyTag", IDS_DIFF_ENEMY_TAG, false, false, false),
	DifficultyDesc("HUD", IDS_DIFF_HUD, true, false, false),
	DifficultyDesc("AutoSpot", IDS_DIFF_AUTO_SPOT, true, false, false),
	DifficultyDesc("Map", IDS_DIFF_MAP, true, false, false),
	DifficultyDesc("WeaponCursor", IDS_DIFF_WEAPON_CURSOR, true, true, true),
	DifficultyDesc("AutoGuideAT", IDS_DIFF_AUTO_GUIDE_AT, true, false, false),
	DifficultyDesc("ClockIndicator", IDS_DIFF_CLOCK_INDICATOR, true, true, true),
	DifficultyDesc("3rdPersonView", IDS_DIFF_3RD_PERSON_VIEW, true, true, true),
	DifficultyDesc("Tracers", IDS_DIFF_TRACERS, true, true, true),
	DifficultyDesc("UltraAI", IDS_ULTRA_AI, false, false, true),
};


void Config::InitDifficulties()
{
	for (int i=0; i<DTN; i++)
	{
		cadetDifficulty[i] = diffDesc[i].defaultCadet;
		veteranDifficulty[i] = diffDesc[i].defaultVeteran;
	}

	Glob.config.showTitles = true;
	GChatList.Enable(true);
}

#ifdef _WIN32

class HiResTicks
{
	bool _counterPresent;
	LARGE_INTEGER _frequency;
	float _invFrequency;

	public:
	HiResTicks();
	__int64 GetTicks() const; // get ticks in ms
	__int64 GetFreq() const
	{
		return _frequency.QuadPart;
	}
	float TimeFrom(__int64 from) const
	{
		return (GetTicks()-from)*_invFrequency;
	}
};

static HiResTicks HiResCounter;

HiResTicks::HiResTicks()
{
	_counterPresent=::QueryPerformanceFrequency(&_frequency)!=FALSE;
	if( !_counterPresent ) return;
	_invFrequency=1.0f/_frequency.QuadPart;
}

__int64 HiResTicks::GetTicks() const
{
	if( !_counterPresent ) return GlobalTickCount()*1000;
	// get ticks in ms
	LARGE_INTEGER count;
	::QueryPerformanceCounter(&count);
	return count.QuadPart;
}

#endif

typedef __int64 SectionTimeHandle;
#ifndef _WIN32
typedef unsigned long long unsigned64;
extern unsigned64 getSystemTime ();
#endif

SectionTimeHandle StartSectionTime()
{
#ifdef _WIN32
	return HiResCounter.GetTicks();
#else
	return getSystemTime();
#endif
}

__int64 GetSectionResolution()
{
#ifdef _WIN32
	return HiResCounter.GetFreq();
#else
	return 1000000;
#endif
};

float GetSectionTime(SectionTimeHandle section)
{
#ifdef _WIN32
	return HiResCounter.TimeFrom(section);
#else
	return( 1.e-6f * (getSystemTime() - section) );
#endif
}

/*!
\patch 1.93 Date 10/7/2003 by Ondra
- Fixed: AI waypoint delays and other AI degradation possible on very fast (~3GHz) CPU
due to bad handling of high resolution timers.
*/

bool CompareSectionTimeGE(SectionTimeHandle section, float time)
{
#ifdef _WIN32
	return HiResCounter.GetTicks()>=section+to64bInt(time*HiResCounter.GetFreq());
#else
	return( 1.e-6f * (getSystemTime() - section) >= time );
#endif
}

DWORD OFPFrameFunctions::TickCount()
{
#ifdef _WIN32
	#if 1
		static DWORD offset=timeGetTime();
		return timeGetTime()-offset; // 1 ms resolution
	#else
		static DWORD offset=GetTickCount();
		return GetTickCount()-offset; // 55 ms resolution?
	#endif
#else
	static unsigned64 offset = getSystemTime();
	unsigned64 now = getSystemTime();
	return( (DWORD)((now - offset + 500) / 1000) ); // 1 ms resolution
#endif
}
