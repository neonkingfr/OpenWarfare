#ifdef _MSC_VER
#pragma once
#endif

#ifndef _TANKORCAR_HPP
#define _TANKORCAR_HPP

#include "tracks.hpp"
#include "gearBox.hpp"

#include "transport.hpp"
#include "shots.hpp"

#include "indicator.hpp"

class TankOrCarType: public TransportType
{
	typedef TransportType base;
	friend class TankOrCar;
	friend class Car;
	friend class Tank;
	friend class TankWithAI;

	protected:
//	Matrix4 _toIndicatorSpeedAxis,_toIndicatorRpmAxis; // transformation
//	AnimationWithCenter _indicatorSpeed,_indicatorRpm;
	Indicator _speedIndicator, _speedIndicator2, _rpmIndicator;

	Vector3 _pilotPos; // camera position	
	Vector3 _outPilotPos; // camera position
	Vector3 _exhaustPos,_exhaustDir;

	SoundPars _gearSound;

//	Vector3 _leftLightPos,_rightLightPos;
//	Vector3 _leftLightDir,_rightLightDir;

	AnimationSection _brakeLights;

//	HitPoint _lightLHit,_lightRHit;
	HitPoint _engineHit;

	bool _outPilotOnTurret;
	bool _hasExhaust;
	bool _canFloat; // is able to move in water

	public:

	TankOrCarType( const ParamEntry *param );
	virtual void Load(const ParamEntry &par);
	void InitShape();
};

class IndicesUpdateTankOrCar : public IndicesUpdateTransport
{
	typedef IndicesUpdateTransport base;

public:
	int pilotBrake;

	IndicesUpdateTankOrCar();
	NetworkMessageIndices *Clone() const {return new IndicesUpdateTankOrCar;}
	void Scan(NetworkMessageFormatBase *format);
};

class TankOrCar: public Transport
{
	typedef Transport base;
	protected:

	float _rpm,_rpmWanted; // smooth change of rpm
	GearBox _gearBox; // current gear (0..3)
	DustSource _leftDust,_rightDust;
	Link<AbstractWave> _gearSound;

	ExhaustSource _exhaust;

//	Ref<LightReflectorOnVehicle> _leftLight;
//	Ref<LightReflectorOnVehicle> _rightLight;	

	bool _pilotBrake;
	Time _lastPilotBrake; // avoid flashing lights

	// sometimes vehicles goes "flying" - e.g. after hit (impulse)
	Time _freeFallUntil;

	public:
	TankOrCar( VehicleType *name, Person *driver );

	const TankOrCarType *Type() const
	{
		return static_cast<const TankOrCarType *>(GetType());
	}

	void Simulate( float deltaT, SimulationImportance prec );

	//! additonal animation for speedometer
	//! some vehicles have speedo attached on something
	virtual void AnimateSpeedIndicator(Matrix4 &trans, int level);

	void Animate( int level );
	void Deanimate( int level );

	void SimulateExhaust( float deltaT, SimulationImportance prec );

	virtual float GetSteerAheadSimul() const;
	virtual float GetSteerAheadPlan() const;

	virtual float GetPrecision() const;


	virtual float Thrust() const = NULL;
	virtual float ThrustWanted() const = NULL;

	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx);
};

#endif

