#ifdef _MSC_VER
#pragma once
#endif

#ifndef _MOTORCYCLE_HPP
#define _MOTORCYCLE_HPP

#include "tankOrCar.hpp"

#include "transport.hpp"
#include "shots.hpp"

#include "rtAnimation.hpp"

enum MotorcycleWheel
{
	MCFWheel,
	MCBWheel,
	MaxMCWheels,
	MCNoWheel=MaxMCWheels
};

#include "wounds.hpp"

class MotorcycleType: public TankOrCarType
{
	typedef TankOrCarType base;
	friend class Motorcycle;

	protected:
	
	Matrix4 _toWheelAxis; // transformation
	AnimationWithCenter _drivingWheel;

	Vector3 _steeringPoint;
	float _wheelCircumference;
	float _turnCoef;
	float _terrainCoef;

	AnimationAnimatedTexture _animFire;
	TurretType _turret;
	bool _hasTurret;
	bool _isBicycle;

	AnimationWithCenter _frontWheel;
	AnimationWithCenter _backWheel;
	AnimationRotation _pedals,_pedalR,_pedalL;
	AnimationWithCenter _frontWheelDamper;
	Animation _backWheelDamper;

	Buffer<MotorcycleWheel> _whichWheelContact; // conversion from landcontact
	AnimationWithCenter _support; //!< Used when cycle is standing

	WoundTextureSelections _glassDammageHalf;
	WoundTextureSelections _glassDammageFull;

	AnimationWithCenter *_wheels[MaxMCWheels];

	PlateInfos _plateInfos;

	HitPoint _glassRHit;
	HitPoint _glassLHit;
	HitPoint _bodyHit;
	HitPoint _fuelHit;

	HitPoint _wheelFHit;
	HitPoint _wheelBHit;

	Ref<WeightInfo> _weights; // rt animation weighting info	
	Ref<Skeleton> _skeleton;

	public:
	MotorcycleType( const ParamEntry *param );
	virtual void Load(const ParamEntry &par);
	void InitShape();
};

class Motorcycle: public TankOrCar
{
	typedef TankOrCar base;

	// basic catterpillar vehicle (car/APC) simulation
	protected:
	Link<AbstractWave> _hornSound;

	Turret _turret;

	WeaponLightSource _mGunFire;
	int _mGunFireFrames;
	UITime _mGunFireTime;
	int _mGunFirePhase;
	WeaponCloudsSource _mGunClouds;

	RString _plateNumber;
	
	float _wheelPhase;
	float _dampers[MaxMCWheels];
	
	float _reverseTimeLeft; // force reverse state
	float _forwardTimeLeft; // force forward state

	TrackOptimized _track;
	
	// simulate gear box
	
	// thrust is relative, in range (-1.0,1.0)
	// actually it is engine RPM?
	float _thrustWanted,_thrust; // accelerate 

	// assume steering point the middle of front whee axe (see MotorcycleType)
	// _turn specifies side offset of steering point
	// when car moves 1 m forward - and therefore it can be viewed
	// as tangens of angle of front wheels
	// each time angular velocity change is calculated,
	// actual radial velocity of

	float _turnWanted,_turn; // turn

	float _turnIncreaseSpeed,_turnDecreaseSpeed; // keyboard different from stick/auto
	
	float _support;

	public:
	Motorcycle( VehicleType *name, Person *driver );

	const MotorcycleType *Type() const
	{
		return static_cast<const MotorcycleType *>(GetType());
	}

	Vector3 Friction( Vector3Par speed );

	float Rigid() const {return 0.3;} // how much energy is transfered in collision
	bool HasTurret() const {return Type()->_hasTurret;}
	bool IsBlocked() const;
	void PlaceOnSurface(Matrix4 &trans);

	bool IsStopped() const;
	void MoveWeapons(float deltaT);
	void SimulateOneIter( float deltaT, SimulationImportance prec );
	void Simulate( float deltaT, SimulationImportance prec );
	Matrix4 InsideCamera( CameraType camType ) const;
	Vector3 ExternalCameraPosition( CameraType camType ) const;
	int InsideLOD( CameraType camType ) const;
	bool IsGunner( CameraType camType ) const;
	bool IsContinuous( CameraType camType ) const;
	bool HasFlares( CameraType camType ) const;
	void SimulateHUD(CameraType camType,float deltaT);
	void Draw( int level, ClipFlags clipFlags, const FrameBase &pos );
	void DrawProxies
	(
		int level, ClipFlags clipFlags,
		const Matrix4 &transform, const Matrix4 &invTransform,
		float dist2, float z2, const LightList &lights
	);

	void AnimateSpeedIndicator(Matrix4 &trans, int level);
	void AnimateMatrix(Matrix4 &mat, int level, int selection) const;

	RString GetActionName(const UIAction &action);
	void PerformAction(const UIAction &action, AIUnit *unit);
	void GetActions(UIActions &actions, AIUnit *unit, bool now);

	Matrix4 TurretTransform() const;
	Matrix4 GunTurretTransform() const;

	//float OutsideCameraDistance( CameraType camType ) const {return 8.3;}
	float OutsideCameraDistance( CameraType camType ) const {return 20;}

	float GetGlassBroken() const;

	void DammageAnimation( int level );
	void DammageDeanimation( int level );

	bool IsAnimated( int level ) const; // appearence changed with Animate
	bool IsAnimatedShadow( int level ) const; // shadow changed with Animate
	void Animate( int level );
	void Deanimate( int level );

	Vector3 AnimatePoint( int level, int index ) const;

	RString GetPlateNumber() const {return _plateNumber;}
	void SetPlateNumber( RString plate ) {_plateNumber=plate;}

	void Sound( bool inside, float deltaT );
	void UnloadSound();

	bool IsPossibleToGetIn() const;
	bool IsAbleToMove() const;
	bool IsCautious() const;

	float GetEngineVol( float &freq ) const;
	float GetEnvironVol( float &freq ) const;

	float DriverAnimSpeed() const;
	float CommanderAnimSpeed() const;
	float GunnerAnimSpeed() const;
	float CargoAnimSpeed(int position) const;

	float Thrust() const {return fabs(_thrust);}
	float ThrustWanted() const {return fabs(_thrustWanted);}

	float TrackingSpeed() const {return 80;}

	void Eject(AIUnit *unit);
	void JoystickPilot( float deltaT );
	void SuspendedPilot(AIUnit *unit, float deltaT );
	void KeyboardPilot(AIUnit *unit, float deltaT );
	void FakePilot( float deltaT );
#if _ENABLE_AI
	void AIPilot(AIUnit *unit, float deltaT );
#endif

	RString DiagText() const;

	float GetPathCost( const GeographyInfo &info, float dist ) const;
	void FillPathCost( Path &path ) const;

	float GetFieldCost( const GeographyInfo &info ) const;
	float GetCost( const GeographyInfo &info ) const;
	float GetCostTurn( int difDir ) const;

	bool FireWeapon( int weapon, TargetType *target );
	void FireWeaponEffects(int weapon, const Magazine *magazine,EntityAI *target);
	Vector3 GetCameraDirection( CameraType camType ) const;
	void LimitCursor( CameraType camType, Vector3 &dir ) const;

	// horn is always aimed
	bool AimWeapon( int weapon, Vector3Par direction );
	bool AimWeapon( int weapon, Target *target );
	Vector3 GetWeaponDirection( int weapon ) const;
	Vector3 GetWeaponCenter( int weapon ) const;

	virtual LSError Serialize(ParamArchive &ar);
	
	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx);
};

#endif

