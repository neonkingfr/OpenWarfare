/*!
\file
HUD UI - In-game UIl layer
\patch_internal 1.01 Date 6/29/2001 by Ondra.
Win32 call replaced by corresponding C counterparts.
Win32 are DLL linked and make protection impossible,
as they are changed by macromedia protection.
*/

#include "wpch.hpp"
#include "world.hpp"
#include "ai.hpp"
#include "person.hpp"
#include "keyInput.hpp"
#include "InGameUIImpl.hpp"
#include "engine.hpp"
#include "camera.hpp"
#include "landscape.hpp"
#include "detector.hpp"
#include "house.hpp"
#include <El/QStream/QBStream.hpp>
#include "network.hpp"
#include <El/Common/randomGen.hpp>
#include "cameraHold.hpp"
#include "diagModes.hpp"

#include "move_actions.hpp"
#include "manActs.hpp"

extern class ParamFile Res;
#include <El/ParamFile/paramFile.hpp>

#include "resincl.hpp"
#include <ctype.h>
#ifdef _WIN32
  #include <io.h>
#endif
#include "dikCodes.h"

#include <Es/Algorithms/qsort.hpp>

#include "chat.hpp"

#include "stringtableExt.hpp"
#include "mbcs.hpp"

//#include "win.h"

#define FontS "tahomaB24"
#define FontM "tahomaB36"

#ifdef PREPROCESS_DOCUMENTATION
#include "../cfg/strIncl.hpp"
#endif

/*

RString GLanguage;
SRef<StringTable> GStringTable;
StringTableDynamic CampaignStringTable;
StringTableDynamic MissionStringTable;

void NextLine(QIStream &f)
{
	while (!f.eof() && f.get() != 0x0D);
	if (!f.eof() && f.get() != 0x0A) f.unget();
}

RString ReadColumn(QIStream &f)
{
	if (f.eof()) return "";
	int c = f.get();
	while (c != 0x0D && isspace(c))
	{
		if (f.eof()) return "";
		c = f.get();
	}
	char buffer[4096];
	int i = 0;
	if (c == '\"')
	{
		if (f.eof()) return "";
		c = f.get();
		while (true)
		{
			if (c == '\"')
			{
				if (f.eof()) return buffer;
				c = f.get();
				if (c != '"')
				{
					while (c != 0x0D && c != ',')		
					{
						if (f.eof()) return buffer;
						c = f.get();
					}
					if (c == 0x0D) f.unget();
					if (i == 0) return "";
					buffer[i] = 0;
					return buffer;
				}
			}
			buffer[i++] = c;
			if (f.eof())
			{
				Assert(i > 0);
				buffer[i] = 0;
				return buffer;
			}
			c = f.get();
		}
		// Unaccessible
	}
	else
	{
		while (c != 0x0D && c != ',')		
		{
			buffer[i++] = c;
			if (f.eof()) return buffer;
			c = f.get();
		}
		if (c == 0x0D) f.unget();
		if (i == 0) return "";
		buffer[i] = 0;
		return buffer;
	}
}

void StringTableDynamic::Add(RString name, RString value)
{
	const StringTableItem2 &check = Get(name);
	// avoid duplicity in names
	if (!IsNull(check))
	{
		RptF("Item %s listed twice", (const char *)name);
		return;
	}
	base::Add(StringTableItem2(name, value));
}

void StringTableDynamic::Load(const char *filename)
{
	QIFStreamB f;
	f.AutoOpen(filename);
	int column = -1;
	while (true)
	{
		if (f.eof()) return;
		RString name = ReadColumn(f);
		if (name && stricmp(name, "LANGUAGE") == 0)
		{
			RString value = ReadColumn(f);
			int c = 0;
			while (value.GetLength() > 0)
			{
				if (stricmp(value, GLanguage) == 0)
				{
					column = c;
					break;
				}
				value = ReadColumn(f);
				c++;
			}
			NextLine(f);
			break;
		}
		else
			NextLine(f);
	}
	if (column < 0)
	{
		RptF("Unsupported language %s in %s",(const char *)GLanguage,filename);
		column = 0;
	}

	while (!f.eof())
	{
		RString name = ReadColumn(f);
		if (name.GetLength()<=0 || stricmp(name, "COMMENT") == 0)
		{
			NextLine(f);
			continue;
		}
		// check collision with
		if (GStringTable)
		{
			// registered strings
			int index = GStringTable->FindStringIndex(name);
			if (index >= 0)
			{
				WarningMessage
				(
					"String %s defined in %s is reserved (registered global string).",
					(const char *)name, filename
				);
				NextLine(f);
				continue;
			}

			// other strings
			StringTableItem2 &item = GStringTable->_table2[name];
			if (!GStringTable->_table2.IsNull(item))
			{
				WarningMessage
				(
					"String %s defined in %s is reserved (nonregistered global string).",
					(const char *)name, filename
				);
				NextLine(f);
				continue;
			}
		}
		for (int c=0; c<column; c++) ReadColumn(f);
		RString value = ReadColumn(f);
		if (value) Add(name, value);
		NextLine(f);
	}
}

StringTable::StringTable(const char *filename)
{
	Init();
	Load(filename);
}

void StringTable::Load(const char *filename)
{
	QIFStreamB f;
	f.AutoOpen(filename);
	int column = -1;
	while (true)
	{
		if (f.eof()) return;
		RString name = ReadColumn(f);
		if (name.GetLength()>0 && stricmp(name, "LANGUAGE") == 0)
		{
			RString value = ReadColumn(f);
			int c = 0;
			while (value.GetLength() > 0)
			{
				if (stricmp(value, GLanguage) == 0)
				{
					column = c;
					break;
				}
				value = ReadColumn(f);
				c++;
			}
			NextLine(f);
			break;
		}
		else
			NextLine(f);
	}
	if (column < 0)
	{
		RptF("Unsupported language %s in %s",(const char *)GLanguage,filename);
		column = 0;
	}

	while (!f.eof())
	{
		RString name = ReadColumn(f);
		if (name.GetLength()<=0 || stricmp(name, "COMMENT") == 0)
		{
			NextLine(f);
			continue;
		}
		for (int c=0; c<column; c++) ReadColumn(f);
		RString value = ReadColumn(f);
		int index = FindStringIndex(name);
		if (index >= 0)
			_table[index].value = value;
		else
			_table2.Add(name, value);
		NextLine(f);
	}
}

int StringTable::FindStringIndex(const char *str)
{
	for (int i=0; i<IDSN; i++)
		if (stricmp(_table[i].name, str) == 0) return i;

	return -1;
}

#if !_ENABLE_DATADISC
static const EnumName PatchStringtable[]=
{
	EnumName(IDS_MPROLE_START,"Start"),
	EnumName(IDS_MP_TRANSFER_FILE,"Receiving mission file (%d KB / %d KB)"),
	EnumName(IDS_SINGLE_OPEN,"Open"),
	EnumName(IDS_NUM_MAGAZINES,"%s"),
	EnumName(IDS_MP_NO_MESSAGE,"No message received for %.0f seconds"),
	EnumName(IDS_MP_SESSION_LOST,"Session lost"),
	EnumName()
};
#endif

RString LocalizeString(IDS ids)
{
	#if !_ENABLE_DATADISC
	if (ids>=IDS_SINGLE_OPEN)
	{
		// check patch table
		RString value = GetEnumName(PatchStringtable,ids);
		if (value.GetLength()>0) return value;
#if _ENABLE_CHEATS
		return "!!! NO STRING IDS";
#else
		return "";
#endif
	}
	#endif
	if (ids < 0 || ids >= IDSN)
	{
		// check patch table
		Fail("Unknown string");
#if _ENABLE_CHEATS
		return "!!! NO STRING IDS";
#else
		return "";
#endif
	}
	if (!GStringTable) return "";
	return GStringTable->_table[ids].value;
}

RString LocalizeString(int ids)
{
	return LocalizeString((IDS)ids);
}

RString LocalizeString(const char *str)
{
	// mission string table
	{
		StringTableItem2 &item = MissionStringTable[str];
		if (!MissionStringTable.IsNull(item)) return item.value;
	}
	// campaign string table
	{
		StringTableItem2 &item = CampaignStringTable[str];
		if (!CampaignStringTable.IsNull(item)) return item.value;
	}
	// gloabal string table
	if (GStringTable)
	{
		// registered strings
		int index = GStringTable->FindStringIndex(str);
		if (index >= 0) return LocalizeString(index);

		// other strings
		StringTableItem2 &item = GStringTable->_table2[str];
		if (!GStringTable->_table2.IsNull(item)) return item.value;
	}

	RptF("%s - string not found in stringtable",str);
#if _ENABLE_CHEATS
	static RString notFound("!!! Undefined string !!!");
#else
	static RString notFound("");
#endif
	return notFound;
}

RString Localize(RString str)
{
	if (str[0] == '@') return LocalizeString((const char *)str + 1);
	return str;
}
*/

///////////////////////////////////////////////////////////////////////////////
// Global selected units - valid for IngameUI and map

OLink<AIUnit> GSelectedUnits[MAX_UNITS_PER_GROUP];
Team GTeams[MAX_UNITS_PER_GROUP];

Team GetTeam(int i)
{
	return GTeams[i];
}

void SetTeam(int i, Team team)
{
	GTeams[i] = team;
}

void ClearTeams()
{
	#ifdef __ICL
	#pragma novector
	#endif
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
		GTeams[i] = TeamMain;
}

PackedBoolArray ListTeam(Team team)
{
	PackedBoolArray list;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		if (GTeams[i] == team) list.Set(i, true);
	}
	return list;
}

AIUnit *GetSelectedUnit(int i)
{
	AIUnit *unit = GSelectedUnits[i];
	if (unit == GWorld->FocusOn())
	{
		GSelectedUnits[i] = NULL;
		return NULL;
	}
	else
		return unit;
}

void SetSelectedUnit(int i, AIUnit *unit)
{
	if (unit == GWorld->FocusOn()) unit = NULL;
	GSelectedUnits[i] = unit;
}

void ClearSelectedUnits()
{
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		GSelectedUnits[i] = NULL;
	}
}

bool IsEmptySelectedUnits()
{
	if (GWorld->FocusOn())
		GSelectedUnits[GWorld->FocusOn()->ID() - 1] = NULL;

	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		if (GSelectedUnits[i]) return false;
	}
	return true;
}

PackedBoolArray ListSelectedUnits()
{
	if (GWorld->FocusOn())
		GSelectedUnits[GWorld->FocusOn()->ID() - 1] = NULL;

	PackedBoolArray list;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		if (GSelectedUnits[i]) list.Set(i, true);
	}
	return list;
}

///////////////////////////////////////////////////////////////////////////////
// class UIActions - action list

static const float dimTime = 10.0f;
static const float fadeTime = dimTime + 2.0f;
static const float protectionTime = 0.5f;
static const float fadeCoef = 1.0f / (fadeTime - dimTime);

UIActions::UIActions()
{
	_selected.type = ATNone;

	_right = 0.98;
	_bottom = 0.88;
	_w = 0.3;
	_h = 0.02;

	_rows = 6;

	_bgColor = PackedColor(Color(0, 0, 0, 0.8));
	_textColor = PackedColor(Color(0.8, 0.8, 0.8, 1));
	_selColor = PackedColor(Color(0.9, 0.8, 0, 1));

#if _CZECH
	static FontID tahomaB24Font = GetFontID("CZERTB24");
#elif _RUSSIAN
	static FontID tahomaB24Font = GetFontID("RUSB24");
#else
	static FontID tahomaB24Font = GetFontID("tahomaB24");
#endif
	_font = GEngine->LoadFont(tahomaB24Font);
	// TODO: store _font somwhere to avoid recreation (memalloc optimization)
//	_size = _font->Height();
	_size = 0.02;

	Hide();
}

void UIActions::Refresh(bool user)
{
	if (user || Glob.uiTime < _dimStart + dimTime)
		_dimStart = Glob.uiTime - protectionTime; // avoid protection
	else
		_dimStart = Glob.uiTime;										// set protection
}

void UIActions::Hide()
{
	_dimStart = Glob.uiTime - fadeTime;
}

float UIActions::GetAge() const
{
	return Glob.uiTime - _dimStart;
}


float UIActions::GetAlpha() const
{
	float age = GetAge();
	if (age <= dimTime) return 1.0f;
	if (age >= fadeTime) return 0.0f;
	return fadeCoef * (fadeTime - age);
}

int UIActions::Add
(
	UIActionType type, TargetType *target, float priority,
	int param, bool showWindow, bool hideOnUse,
	int param2, RString param3
)
{
	int index = base::Add();
	UIAction &action = Set(index);
	action.type = type;
	action.target = target;
	action.param = param;
	action.param2 = param2;
	action.param3 = param3;
	action.priority = priority;
	action.showWindow = showWindow;
	action.hideOnUse = hideOnUse;
	return index;
}

int UIActions::FindSelected()
{
	int n = Size();
	if (n == 0) return -1;
	if (_selected.type == ATNone) return 0; // default
	for (int i=0; i<n; i++)
	{
		const UIAction &action = Get(i);
		if
		(
			action.type == _selected.type &&
			action.target == _selected.target &&
			action.param == _selected.param &&
			action.param2 == _selected.param2 &&
			action.param3 == _selected.param3
		) return i;
	}
	_selected.type = ATNone;
	return 0;	// default
}

void UIActions::SelectPrev(bool cycle)
{
	if (Size() == 0) return;
	int i = FindSelected() - 1;
	if (i < 0)
	{
		if (cycle) i = Size() - 1;
		else i = 0;
	}
	_selected = Get(i);
}

void UIActions::SelectNext(bool cycle)
{
	if (Size() == 0) return;
	int i = FindSelected() + 1;
	if (i >= Size())
	{
		if (cycle) i = 0;
		else i = Size() - 1;
	}
	_selected = Get(i);
}

bool CheckSupply(EntityAI *vehicle, EntityAI *parent, SupportCheckF check, float limit, bool now);

/*!
\patch 1.21 Date 08/20/2001 by Jirka
- Fixed: better processing of conflict in "Take Flag" and "Return Flag" actions
\patch 1.78 Date 7/22/2002 by Jirka
- Fixed: Behaviour of AI in Capture the flag or Flag Fight missions
	(sometimes flag was placed on wrong unit)
*/

void UIAction::Process(AIUnit *unit) const
{
	if (!unit || unit->GetLifeState() != AIUnit::LSAlive) return;
	
	EntityAI *veh = target;
	switch (type)
	{
	case ATGetInCommander:
	case ATGetInDriver:
	case ATGetInGunner:
	case ATGetInCargo:
		{
			if (!unit->IsFreeSoldier()) return;
			Transport *trans = dyn_cast<Transport>(veh);
			if (!trans) return;
			if (unit->IsPlayer() && trans->GetLock() == LSLocked) return;
			AIGroup *grp = unit->GetGroup();
			Assert(grp);
			AICenter *center = grp->GetCenter();
			Assert(center);
			// avoid get in enemy vehicle
			if (trans->Commander() && center->IsEnemy(trans->Commander()->GetTargetSide())) return;
			if (trans->Driver() && center->IsEnemy(trans->Driver()->GetTargetSide())) return;
			if (trans->Gunner() && center->IsEnemy(trans->Gunner()->GetTargetSide())) return;
			for (int i=0; i<trans->GetManCargo().Size(); i++)
			{
				Person *man = trans->GetManCargo()[i];
				if (!man) continue;
				if (center->IsEnemy(man->GetTargetSide())) return;
			}
			switch (type)
			{
			case ATGetInCommander:
				if (!trans->GetGroupAssigned())
					unit->GetGroup()->AddVehicle(trans);
				unit->AssignAsCommander(trans);
				break;
			case ATGetInDriver:
				if (!trans->GetGroupAssigned())
					unit->GetGroup()->AddVehicle(trans);
				unit->AssignAsDriver(trans);
				break;
			case ATGetInGunner:
				if (!trans->GetGroupAssigned())
					unit->GetGroup()->AddVehicle(trans);
				unit->AssignAsGunner(trans);
				break;
			case ATGetInCargo:
				unit->AssignAsCargo(trans);
				break;
			}
			unit->AllowGetIn(true);
			unit->OrderGetIn(true);
			DoAssert(unit->VehicleAssigned() == trans);
			void MoveToGetInPos(AIUnit *unit, Transport &veh);
			MoveToGetInPos(unit, *trans);
			ActionContextBase *CreateGetInActionContext(Transport *veh, UIActionType pos);
			Ref<ActionContextBase> context = CreateGetInActionContext(trans, type);
			context->function = MFGetIn;
			unit->GetPerson()->PlayAction(trans->Type()->GetGetInAction(), context);
			// unit->ProcessGetIn2();
		}
		return;
	case ATTakeFlag:
		if (veh && unit->IsFreeSoldier())
		{
			// FIX
			AIGroup *grp = unit->GetGroup();
			AICenter *center = grp ? grp->GetCenter() : NULL;

			Person *person = dyn_cast<Person>(veh);
			EntityAI *flag = person ? person->GetFlagCarrier() : veh;
			// test conditions once more (action can be launched through procedure)
			if
			(
				flag &&
				(!person && !flag->GetFlagOwner() || flag->GetFlagOwner() == person && !person->IsAbleToMove()) &&
				center && center->IsEnemy(flag->GetFlagSide()) &&
				CheckSupply(unit->GetPerson(), veh, NULL, 0, true)
			)
			{
				if (flag->IsLocal())
					flag->SetFlagOwner(unit->GetPerson());
				else
					GetNetworkManager().SetFlagOwner(unit->GetPerson(), flag);
			}
		}
		return;
		
	case ATReturnFlag:
		if (veh && unit->IsFreeSoldier())
		{
			// FIX
			AIGroup *grp = unit->GetGroup();
			AICenter *center = grp ? grp->GetCenter() : NULL;

			Person *person = dyn_cast<Person>(veh);
			EntityAI *flag = person ? person->GetFlagCarrier() : veh;
			// test conditions once more (action can be launched through procedure)
			if
			(
				flag && person &&
				(flag->GetFlagOwner() == person && !person->IsAbleToMove()) &&
				center && center->IsFriendly(flag->GetFlagSide()) &&
				CheckSupply(unit->GetPerson(), veh, NULL, 0, true)
			)
			{
				if (flag->IsLocal())
					flag->SetFlagOwner(NULL);
				else
					GetNetworkManager().SetFlagOwner(NULL, flag);
			}
		}
		return;
	case ATLoadMagazine:
		if (veh && unit->GetVehicle() == veh && !veh->IsActionInProgress(MFReload))
		{
			int iMagazine = -1;
			for (int i=0; i<veh->NMagazines(); i++)
			{
				const Magazine *mag = veh->GetMagazine(i);
				if (!mag) continue;
				if
				(
					mag->_creator == param &&
					mag->_id == param2
				)
				{
					iMagazine = i;
					break;
				}
			}
			if (iMagazine < 0) return;

			const char *separator = strchr(param3, '|');
			if (!separator) return;
			int separatorPos = separator - param3;
			RString weapon = param3.Substring(0, separatorPos);
			RString muzzle = param3.Substring(separatorPos + 1, INT_MAX);
			int iSlot = -1;
			for (int i=0; i<veh->NMagazineSlots(); i++)
			{
				const MagazineSlot &slot = veh->GetMagazineSlot(i);
				if (slot._weapon->GetName() == weapon && slot._muzzle->GetName() == muzzle)
				{
					iSlot = i;
					break;
				}
			}
			if (iSlot < 0) return;

			veh->ReloadMagazine(iSlot,iMagazine);

			// FIX
			if (!veh->IsLocal()) GetNetworkManager().UpdateWeapons(veh);

			void UpdateWeaponsInBriefing();
			UpdateWeaponsInBriefing();
		}
		return;
	case ATDropWeapon:
		if (target && target != unit->GetPerson())
			target->PerformAction(*this, unit);
		else if (unit->IsFreeSoldier())
		{
			// find weapon		
			Ref<WeaponType> weapon = WeaponTypes.New(param3);
			if (!weapon || !weapon->_canDrop) return;

			Person *person = unit->GetPerson();
			if (person->FindWeapon(weapon))
			{
				// create container
				Ref<VehicleSupply> container;
				int slots = weapon->_weaponType;
				if ((slots & MaskSlotSecondary) != 0 && (slots & MaskSlotPrimary) == 0)
					container = dyn_cast<VehicleSupply>(NewVehicle("SecondaryWeaponHolder"));
				else
					container = dyn_cast<VehicleSupply>(NewVehicle("WeaponHolder"));
				if (!container) return;

				Vector3 pos = person->Position() + 0.5f * person->Direction() + VUp*0.5f;
				Matrix3 dir;
/*
				Vector3 normal;
				AIUnit::FindFreePosition(pos, normal, false, container);
				dir.SetUpAndDirection(normal, person->Direction());
*/
				dir.SetUpAndDirection(VUp, person->Direction());
				Matrix4 transform;
				transform.SetPosition(pos);
				transform.SetOrientation(dir);

				container->PlaceOnSurface(transform);
				container->SetTransform(transform);
				container->Init(transform);

				// add container to world
				GWorld->AddBuilding(container);
				if (GWorld->GetMode() == GModeNetware)
					GetNetworkManager().CreateVehicle(container, VLTBuilding, "", -1);
				if (unit->GetGroup()) unit->GetGroup()->AddTarget(container, 4.0f, 4.0f, 0);
				
				// remove weapon
				person->RemoveWeapon(weapon);
				if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == person)
					GWorld->UI()->ResetVehicle(person);

				container->AddWeaponCargo(weapon, 1);
				if (GWorld->GetMode() == GModeNetware)
					GetNetworkManager().AddWeaponCargo(container, weapon->GetName());
				
				// remove unusable magazines
				for (int i=0; i<person->NMagazines();)
				{
					Ref<Magazine> magazine = person->GetMagazine(i);
					if (!magazine || person->IsMagazineUsable(magazine->_type))
					{
						i++;
						continue;
					}
					person->RemoveMagazine(magazine);
					if (magazine->_ammo > 0)
					{
						container->AddMagazineCargo(magazine);
						if (GWorld->GetMode() == GModeNetware)
							GetNetworkManager().AddMagazineCargo(container, magazine);
					}
				}
			}
			void UpdateWeaponsInBriefing();
			UpdateWeaponsInBriefing();
		}
		return;
	case ATDropMagazine:
		if (target && target != unit->GetPerson())
			target->PerformAction(*this, unit);
		else if (unit->IsFreeSoldier())
		{
			Person *person = unit->GetPerson();

			// find magazine
			Ref<MagazineType> type = MagazineTypes.New(param3);
			if (!type) return;

			Ref<const Magazine> magazine;
			int minCount = INT_MAX;
			// find in nonused magazines
			for (int i=0; i<person->NMagazines(); i++)
			{
				const Magazine *m = person->GetMagazine(i);
				if (!m) continue;
				if (m->_type != type) continue;
				if (person->IsMagazineUsed(m)) continue;
				if (m->_ammo < minCount)
				{
					magazine = m;
					minCount = m->_ammo; 
				}
			}
			// find in all magazines
			if (!magazine) for (int i=0; i<person->NMagazines(); i++)
			{
				const Magazine *m = person->GetMagazine(i);
				if (!m) continue;
				if (m->_type != type) continue;
				if (m->_ammo < minCount)
				{
					magazine = m;
					minCount = m->_ammo; 
				}
			}

			if (magazine)
			{
				// remove magazine
				person->RemoveMagazine(magazine);
				if (minCount > 0)
				{
					// create container
					Ref<VehicleSupply> container = dyn_cast<VehicleSupply>(NewVehicle("WeaponHolder"));
					if (!container) return;

					Vector3 pos = person->Position() + 0.5f * person->Direction() + VUp*0.5f;
					Matrix3 dir;
/*
					Vector3 normal;
					AIUnit::FindFreePosition(pos, normal, false, container);
					dir.SetUpAndDirection(normal, person->Direction());
*/
					dir.SetUpAndDirection(VUp, person->Direction());
					Matrix4 transform;
					transform.SetPosition(pos);
					transform.SetOrientation(dir);

					container->PlaceOnSurface(transform);
					container->SetTransform(transform);
					container->Init(transform);

					// add container to world
					GWorld->AddBuilding(container);
					if (GWorld->GetMode() == GModeNetware)
						GetNetworkManager().CreateVehicle(container, VLTBuilding, "", -1);
					if (unit->GetGroup()) unit->GetGroup()->AddTarget(container, 4.0f, 4.0f, 0);

					// add magazine to container
					container->AddMagazineCargo(const_cast<Magazine *>(magazine.GetRef()));
					if (GWorld->GetMode() == GModeNetware)
						GetNetworkManager().AddMagazineCargo(container, magazine);
				}
			}
			void UpdateWeaponsInBriefing();
			UpdateWeaponsInBriefing();
		}
		return;
	case ATNVGoggles:
		{
			Person *person = unit->GetPerson();
			person->SetNVWanted(!person->IsNVWanted());
		}
		break;
	default:
		if (target)
		{
			target->PerformAction(*this, unit);
			return;
		}
		Fail("Bad action type - no target");
		return;
	}
}

void UIActions::ProcessAction(AIUnit *unit)
{
	Assert(unit);

	int i = FindSelected();
	if (i < 0) return;
	const UIAction &action = Get(i);
	if (action.hideOnUse)
	{
		Hide();
		_selected.type = ATNone;
	}
	unit->GetVehicle()->StartActionProcessing(action,unit);
}

int CmpActions(const UIAction *action1, const UIAction *action2)
{
	int s = sign(action2->priority - action1->priority);
	if (s != 0) return s;
	s = (int)action1->target.GetLink() - (int)action2->target.GetLink();
	if (s != 0) return s;
	s = action1->param - action2->param;
	if (s != 0) return s;
	s = action1->param2 - action2->param2;
	if (s != 0) return s;
	return strcmp(action1->param3, action2->param3);
}

void UIActions::Sort()
{
	QSort(Data(), Size(), CmpActions);
}

static float ActionSourceCost(EntityAI *veh, Target *tgt)
{
	// check target focus
	Assert(tgt->idExact);
	Vector3 relPos = veh->PositionWorldToModel(tgt->idExact->AimingPosition());
	float dist2 = relPos.SquareSize();
	if (relPos.Z()<0)
	{
		dist2 *= 8;
	}
	if (fabs(relPos.X())>relPos.Z())
	{
		dist2 *= 4;
	}
	return dist2;
}

#if _ENABLE_DATADISC
static void AddDropActions(AIUnit *unit, UIActions &actions)
{
	if (unit->IsFreeSoldier())
	{
		int n = actions.Size();
		EntityAI *veh = unit->GetVehicle();

		bool findWeapons = false;
		bool findMagazines = false;
		for (int i=0; i<n; i++)
		{
			if (actions[i].type == ATDropWeapon) findWeapons = true;
			else if (actions[i].type == ATDropMagazine) findMagazines = true;
		}

		if (!findWeapons)
		{
			int index = veh->SelectedWeapon();
			if (index >= 0)
			{
				const MagazineSlot &slot = veh->GetMagazineSlot(index);
				if (slot._weapon && slot._weapon->_canDrop)
					actions.Add(ATDropWeapon, NULL, -0.01, 0, false, false, 0, slot._weapon->GetName());
			}
		}
		if (!findMagazines)
		{
			int index = veh->SelectedWeapon();
			if (index >= 0)
			{
				const MagazineSlot &slot = veh->GetMagazineSlot(index);
				if (slot._magazine)
				{
					const MagazineType *type = slot._magazine->_type;
					for (int i=0; i<veh->NMagazines(); i++)
					{
						const Magazine *magazine = veh->GetMagazine(i);
						if (!magazine) continue;
						if (magazine->_ammo == 0) continue;
						if (magazine->_type != type) continue;
						// if (veh->IsMagazineUsed(magazine)) continue;
						actions.Add(ATDropMagazine, NULL, -0.02, 0, false, false, 0, type->GetName());
						break;
					}
				}
			}
		}
	}
}

TypeIsSimple(const MagazineType *)

static void AddDropAllActions(AIUnit *unit, UIActions &actions)
{
	if (unit->IsFreeSoldier())
	{
		EntityAI *veh = unit->GetVehicle();

		for (int i=0; i<veh->NWeaponSystems(); i++)
		{
			const WeaponType *weapon = veh->GetWeaponSystem(i);
			if (weapon && weapon->_canDrop)
				actions.Add(ATDropWeapon, NULL, -0.01, 0, false, false, 0, weapon->GetName());
		}
		AUTO_STATIC_ARRAY(const MagazineType *, types, 32);
		for (int i=0; i<veh->NMagazines(); i++)
		{
			const Magazine *magazine = veh->GetMagazine(i);
			if (!magazine) continue;
			if (magazine->_ammo == 0) continue;
			// if (veh->IsMagazineUsed(magazine)) continue;
			const MagazineType *type = magazine->_type;
			bool found = false;
			for (int j=0; j<types.Size(); j++)
				if (types[j] == type)
				{
					found = true; break;
				}
			if (!found)
			{
				actions.Add(ATDropMagazine, NULL, -0.02, 0, false, false, 0, type->GetName());
				types.Add(type);
			}
		}
	}
}
#endif

/*!
\patch_internal 1.27 Date 10/17/2001 by Jirka
- Fixed: ProcessActions assumed unit->GetGroup() != NULL - crash occured when this condition where not satisfied
\patch 1.79 Date 7/26/2002 by Ondra
- New: MP: Player who is disconnected from server cannot activate actions.
*/

void InGameUI::ProcessActions(AIUnit *unit)
{
	if (unit->GetLifeState() != AIUnit::LSAlive || !unit->GetGroup())
	{
		_actions.Clear();
		return;
	}

	// first process user input, than collect new actions (actions may changed)
	// user input
	float age = _actions.GetAge();
	bool visible = age >= protectionTime && age <= dimTime;

	if (GInput.GetActionToDo(UAPrevAction))
	{
		_actions.SelectPrev(true);
		_actions.Refresh(true);
	}
	if (GInput.cursorMovedZ > 0 && visible)
	{
		_actions.SelectPrev(false);
		_actions.Refresh(true);
	}
	if (GInput.GetActionToDo(UANextAction))
	{
		_actions.SelectNext(true);
		_actions.Refresh(true);
	}
	if (GInput.cursorMovedZ < 0 && visible)
	{
		_actions.SelectNext(false);
		_actions.Refresh(true);
	}
	if (GInput.GetActionToDo(UAAction))
	{
		if (visible)
		{
			if (_actions.Size() > 0)
			{
				if (!GetNetworkManager().IsControlsPaused())
				{
					_actions.ProcessAction(unit);
				}
			}
			else
				_actions.Hide();
		}
		else
			_actions.Refresh(true);
	}

	
	UIActions actions;
	static StaticStorage<UIAction> actionStorage;
	actions.SetStorage(actionStorage.Init(64));
	
	// collect all actions
	EntityAI *veh = unit->GetVehicle();
	veh->GetActions(actions, unit, true);
	
	const TargetList &visibleList=*VisibleList();
/*
	int minI = -1;
	float minCost = 1e10;
	for (int i=0; i<visibleList.Size(); i++)
	{
		EntityAI *target = visibleList[i]->idExact;
		if (!target) continue;
		if (target == veh) continue;
		float cost = ActionSourceCost(veh,visibleList[i]);
		if (cost<minCost)
		{
			minCost = cost;
			minI = i;
		}
	}

	if (minI>=0)
	{
		EntityAI *target = visibleList[minI]->idExact;
		target->GetActions(actions, unit, true);
	}
*/
	for (int i=0; i<visibleList.Size(); i++)
	{
		EntityAI *target = visibleList[i]->idExact;
		if (!target) continue;
		if (target == veh) continue;
		target->GetActions(actions, unit, true);
	}

#if _ENABLE_DATADISC
	AddDropActions(unit, actions);
#endif
	
	Person *person = unit->GetPerson();
	if (person->QIsManual())
	{
		// night vision
		if (person->IsNVEnabled()) actions.Add(ATNVGoggles, person, 0.511, 0);
	}

	int n = actions.Size();

	// check for new action
	bool newAction = false;
	if (n == 0)
	{
		if (_actions.Size() > 0) _actions.Hide();
	}
	else
	{
		for (int i=0; i<n; i++)
		{
			UIAction &action = actions[i];
			if (!action.showWindow) continue;
			bool found = false;
			for (int j=0; j<_actions.Size(); j++)
			{
				UIAction &compare = _actions[j];
				if
				(
					compare.target == action.target &&
					compare.type == action.type &&
					compare.param == action.param
				)
				{
					found = compare.showWindow;
					break;
				}
			}
			if (!found)
			{
				newAction = true;
				break;
			}
		}
	}
	_actions.Resize(n);
	for (int i=0; i<n; i++) _actions[i] = actions[i];
	if (newAction) _actions.Refresh(false);

	// sort actions
	_actions.Sort();
}

///////////////////////////////////////////////////////////////////////////////
// Global selected units - valid for IngameUI and map

DisplayUnitInfo::DisplayUnitInfo(ControlsContainer *parent)
	: Display(parent)
{
	SetCursor(NULL);
	InitControls();
}

void DisplayUnitInfo::InitControls()
{
	time = NULL;
	date = NULL;
	name = NULL;
	unit = NULL;
	valueExp = NULL;
	formation = NULL;
	combatMode = NULL;
	valueHealth = NULL;
	weapon = NULL;
	ammo = NULL;
	vehicle = NULL;
	speed = NULL;
	alt = NULL;
	valueArmor = NULL;
	valueFuel = NULL;
	cargoMan = NULL;
	cargoFuel = NULL;
	cargoRepair = NULL;
	cargoAmmo = NULL;
}

void DisplayUnitInfo::Reload(const ParamEntry &clsEntry)
{
	Init();
	InitControls();
	Load(clsEntry);
}

Control *DisplayUnitInfo::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
		case IDC_IGUI_BG:
			background = new CStatic(this, idc, cls);
			return background;
		case IDC_IGUI_TIME:
			time = new CStaticTime(this, idc, cls, true);
			return time;
		case IDC_IGUI_DATE:
			date = new CStatic(this, idc, cls);
			return date;
		case IDC_IGUI_NAME:
			name = new CStatic(this, idc, cls);
			return name;
		case IDC_IGUI_UNIT:
			unit = new CStatic(this, idc, cls);
			return unit;
		case IDC_IGUI_VALUE_EXP:
			valueExp = new CProgressBar(this, idc, cls);
			return valueExp;
		case IDC_IGUI_FORMATION:
			formation = new CStatic(this, idc, cls);
			return formation;
		case IDC_IGUI_COMBAT_MODE:
			combatMode = new CStatic(this, idc, cls);
			return combatMode;
		case IDC_IGUI_VALUE_HEALTH:
			valueHealth = new CProgressBar(this, idc, cls);
			return valueHealth;
		case IDC_IGUI_WEAPON:
			weapon = new CStatic(this, idc, cls);
			return weapon;
		case IDC_IGUI_AMMO:
			ammo = new CStatic(this, idc, cls);
			return ammo;
		case IDC_IGUI_VEHICLE:
			vehicle = new CStatic(this, idc, cls);
			return vehicle;
		case IDC_IGUI_SPEED:
			speed = new CStatic(this, idc, cls);
			return speed;
		case IDC_IGUI_ALT:
			alt = new CStatic(this, idc, cls);
			return alt;
		case IDC_IGUI_VALUE_ARMOR:
			valueArmor = new CProgressBar(this, idc, cls);
			return valueArmor;
		case IDC_IGUI_VALUE_FUEL:
			valueFuel = new CProgressBar(this, idc, cls);
			return valueFuel;
		case IDC_IGUI_CARGO_MAN:
			cargoMan = new CStatic(this, idc, cls);
			return cargoMan;
		case IDC_IGUI_CARGO_FUEL:
			cargoFuel = new CStatic(this, idc, cls);
			return cargoFuel;
		case IDC_IGUI_CARGO_REPAIR:
			cargoRepair = new CStatic(this, idc, cls);
			return cargoRepair;
		case IDC_IGUI_CARGO_AMMO:
			cargoAmmo = new CStatic(this, idc, cls);
			return cargoAmmo;
	}
	return Display::OnCreateCtrl(type, idc, cls);
}

// Hints

DisplayHint::DisplayHint(ControlsContainer *parent)
: Display(parent)
{
	SetCursor(NULL);
	Load(Res >> "RscInGameUI" >> "RscHint");
}

Control *DisplayHint::OnCreateCtrl(int type, int idc, const ParamEntry &cls)
{
	switch (idc)
	{
	case IDC_IGHINT_BG:
		_background = new CStatic(this, idc, cls);
		return _background;
	case IDC_IGHINT_HINT:
		_hint = new CStatic(this, idc, cls);
		_hint->EnableCtrl(false);
		return _hint;
	default:
		return Display::OnCreateCtrl(type, idc, cls);
	}
}

void DisplayHint::SetHint(RString hint)
{
	_hint->SetText(hint);
	float h = _hint->GetTextHeight();
	float dh = _background->H() - _hint->H();
	_hint->SetPos
	(
		_hint->X(), _hint->Y(), _hint->W(), h
	);
	_background->SetPos
	(
		_background->X(), _background->Y(), _background->W(), h + dh
	);
}

void DisplayHint::SetPosition(float top)
{
	float dy = _hint->Y() - _background->Y();
	_background->SetPos
	(
		_background->X(), top, _background->W(), _background->H()
	);
	_hint->SetPos
	(
		_hint->X(), top + dy, _hint->W(), _hint->H()
	);
}

MenuItem::MenuItem
(
	RString text, int key, RString ch, int cmd,
	UIActionType action, EntityAI *target,
	int param, int param2, RString param3
)
:_text(text),_baseText(text),_key(key),_char(ch),_cmd(cmd),
	_action(action), _target(target),
	_param(param), _param2(param2), _param3(param3),
	_enable(true),_visible(true),_check(false)
{
}

MenuItem::MenuItem(RString text, int key, RString ch, Menu *submenu, int cmd)
:_text(text),_baseText(text),_key(key),_char(ch),_cmd(cmd),
	_action(ATNone), _target(NULL),
	_param(0),_param2(0),_param3(""),
	_submenu(submenu),_enable(true),_visible(true),_check(false)
{
}

Menu::Menu()
{
	_parent = NULL;
	_enable = true;
	_visible = true;
	_minCmd=INT_MAX,_maxCmd=0; // for faster rejection of EnableCommand/ShowCommand
	_atomic=false; // submenu may be enabled/disabled only as whole
}
Menu::Menu(const char *text, Menu *parent)
{
	_text=text;
	_parent = parent;
	_enable = true;
	_visible = true;
	_minCmd=INT_MAX,_maxCmd=0; // for faster rejection of EnableCommand/ShowCommand
	_atomic=false; // submenu may be enabled/disabled only as whole
}

void Menu::Load(const ParamEntry *cls)
{
	_text = RString((*cls)>>"title");
	_atomic = (*cls)>>"atomic";
	for (int i=0; i<((*cls)>>"items").GetSize(); i++)
	{
		const ParamEntry *clsItem = &((*cls)>>RString(((*cls)>>"items")[i]));
		RString itemTitle = (*clsItem)>>"title";
		int itemKey = ((*clsItem)>>"key").GetInt();
		RString itemChar = (*clsItem)>>"character";
		int itemCmd = ((*clsItem)>>"command").GetInt();
		if (clsItem->FindEntry("menu"))
		{
			Menu *submenu = new Menu();
			submenu->_parent = this;
			submenu->Load(&(Res>>RString((*clsItem)>>"menu")));

			if (!submenu->_atomic)
			{
				saturateMax(_maxCmd,submenu->_maxCmd);
				saturateMin(_minCmd,submenu->_minCmd);
			}


			_items.Add(new MenuItem(itemTitle, itemKey, itemChar, submenu, itemCmd));
		}
		else
			_items.Add(new MenuItem(itemTitle, itemKey, itemChar, itemCmd));
		if (itemCmd>=0)
		{
			saturateMax(_maxCmd,itemCmd);
			saturateMin(_minCmd,itemCmd);
		}
	}
}

bool Menu::CanBeInMenu(int cmd) const
{
	return cmd>=_minCmd && cmd<=_maxCmd;
}

void Menu::NotifySubmenuCommandAdded(int cmd)
{
	saturateMax(_maxCmd,cmd);
	saturateMin(_minCmd,cmd);
	// note: change needs to be propagated to all upper level menus
	if (!_atomic && _parent) _parent->NotifySubmenuCommandAdded(cmd);
}

void Menu::NotifySubmenuCommandRemoved(int cmd)
{
	if (!_atomic && _parent) _parent->NotifySubmenuCommandRemoved(cmd);
	if (cmd==_maxCmd || cmd==_minCmd)
	{
		// range may be descreased - something happening on the edge
		RescanMinMax();
	}
}

void Menu::RescanParents()
{
	Menu *parent = _parent;
	while (parent)
	{
		parent->RescanMinMax();
		if (parent->_atomic) break;
		parent = parent->_parent;
	}
}

void Menu::RescanChildren()
{
	for (int i=0; i<_items.Size(); i++)
	{
		MenuItem *item = _items[i];
		if (item->_submenu)
		{
			item->_submenu->RescanChildren();
			item->_submenu->RescanMinMax();
		}
	}
}

void Menu::RescanMinMax()
{
	// scan menu (and optionally all submenus)
	int oldMin = _minCmd, oldMax = _maxCmd;
	_minCmd = INT_MAX,_maxCmd=INT_MIN;
	for (int i=0; i<_items.Size(); i++)
	{
		MenuItem *item = _items[i];
		if (item->_cmd>=0)
		{
			saturateMax(_maxCmd,item->_cmd);
			saturateMin(_minCmd,item->_cmd);
		}
		if (!_atomic && item->_submenu)
		{
			saturateMax(_maxCmd,item->_submenu->_maxCmd);
			saturateMin(_minCmd,item->_submenu->_minCmd);
		}
	}
	if (_minCmd!=oldMin || _maxCmd!=oldMax)
	{
		// change - force all parents to update
		RescanParents();
	}
}

void Menu::AddItem(MenuItem *item)
{
	if (item->_cmd>=0)
	{
		NotifySubmenuCommandAdded(item->_cmd);
	}
	_items.Add(item);
}


bool Menu::EnableCommand(int cmd, bool enable)
{
	if (!CanBeInMenu(cmd)) return false;
	if (_atomic) return false;
	bool retValue = false;
	for (int i=0; i<_items.Size(); i++)
	{
		MenuItem *item = _items[i];
		if (item->_cmd == cmd)
		{
			item->_enable = enable;
			retValue = true;
		}
		else if (item->_submenu)
		{
			if (item->_submenu->EnableCommand(cmd, enable))
			{
				bool ok = false;
				for (int j=0; j<item->_submenu->_items.Size(); j++)
				{
					MenuItem *subItem = item->_submenu->_items[j];
					int cmd = subItem->_cmd;
					if (cmd==CMD_BACK || cmd== CMD_SEPARATOR || cmd==CMD_HIDE_MENU) continue;
					if (subItem->_enable)
					{
						ok = true;
						break;
					}
				}
				item->_enable = ok;
				item->_submenu->_enable = ok;
				retValue = true;
			}
		}
	}
	return retValue;
}


bool Menu::ShowCommand(int cmd, bool show)
{
	if (!CanBeInMenu(cmd)) return false;
	if (_atomic) return false;
	bool ret=false;
	for (int i=0; i<_items.Size(); i++)
	{
		MenuItem *item = _items[i];
		if (item->_cmd == cmd)
		{
			item->_visible = show;
			ret = true;
			break;
		}
		else if (item->_submenu)
		{
			if (item->_submenu->ShowCommand(cmd, show))
			{
				bool ok = false;
				for (int j=0; j<item->_submenu->_items.Size(); j++)
				{
					MenuItem *subItem = item->_submenu->_items[j];
					if (subItem->_visible)
					{
						int cmd = subItem->_cmd;
						if (cmd!=CMD_SEPARATOR && cmd!=CMD_BACK && cmd!=CMD_HIDE_MENU)
						{
							ok = true;
							break;
						}
					}
				}
				item->_visible = ok;
				item->_submenu->_visible = ok;
				ret = true;
				break;
			}
		}
	}
	
	return ret;
}

bool Menu::ShowAndEnableCommand(int cmd, bool show, bool enable)
{
	if (!CanBeInMenu(cmd)) return false;
	if (_atomic) return false;
	bool ret=false;
	for (int i=0; i<_items.Size(); i++)
	{
		MenuItem *item = _items[i];
		if (item->_cmd == cmd)
		{
			item->_visible = show;
			item->_enable = enable;
			ret = true;
			break;
		}
		else if (item->_submenu)
		{
			if (item->_submenu->ShowAndEnableCommand(cmd, show,enable))
			{
				bool okShow = false;
				bool okEnable = false;
				for (int j=0; j<item->_submenu->_items.Size(); j++)
				{
					MenuItem *subItem = item->_submenu->_items[j];
					int cmd = subItem->_cmd;
					if (cmd!=CMD_SEPARATOR && cmd!=CMD_BACK && cmd!=CMD_HIDE_MENU)
					{
						if(subItem->_visible) okShow = true;
						if(subItem->_enable) okEnable = true;
					}
				}
				item->_visible = okShow;
				item->_enable = okEnable;
				item->_submenu->_visible = okShow;
				item->_submenu->_enable = okEnable;
				ret = true;
				break;
			}
		}
	}
	return ret;
}

Menu *Menu::FindMenu(int cmd, bool alsoInAtomic)
{
	if (!alsoInAtomic)
	{
		if (!CanBeInMenu(cmd)) return NULL;
		if (_atomic) return NULL;
	}
	for (int i=0; i<_items.Size(); i++)
	{
		MenuItem *item = _items[i];
		if (item->_submenu)
		{
			Menu *found = item->_submenu->FindMenu(cmd,alsoInAtomic);
			if (found != NULL) return found;
		}
		else
		{
			if (item->_cmd == cmd) return this;
		}
	}
	return NULL;
}

MenuItem *Menu::Find(int cmd, bool alsoInAtomic)
{
	if (!alsoInAtomic)
	{
		if (!CanBeInMenu(cmd)) return NULL;
		if (_atomic) return NULL;
	}
	for (int i=0; i<_items.Size(); i++)
	{
		MenuItem *item = _items[i];
		if (item->_cmd == cmd) return item;
		if (item->_submenu)
		{
			MenuItem *found = item->_submenu->Find(cmd,alsoInAtomic);
			if (found != NULL) return found;
		}
	}
	return NULL;
}

bool Menu::SetText(int cmd, RString text)
{
	MenuItem *item = Find(cmd);
	if (item == NULL) return false;
	item->_text = text;
	return true;
}

bool Menu::ResetText(int cmd)
{
	MenuItem *item = Find(cmd);
	if (item == NULL) return false;
	item->_text = item->_baseText;
	return true;
}

bool Menu::CheckCommand(int cmd, bool check)
{
	MenuItem *item = Find(cmd);
	if (item == NULL) return false;
	item->_check = check;
	return true;
}

InGameUI::InGameUI()
:_mode(UIFire),_modeAuto(UIFire),
_groundPointValid(false)
{
#if _ENABLE_CHEATS
	_showAll=false;
#endif
	_cursorWorld=false;
	_worldCursor=VForward; // world cursor direction
	_modelCursor=VForward;
	_worldCursorTime=Glob.uiTime-120;

	_lastMeTime = Glob.uiTime;
	_lastCmdId = -1;
	_lastCmdTime = Glob.uiTime - 120;
	_lastTargetTime = Glob.uiTime - 120;
	_lastGroupDirTime = Glob.uiTime - 120;
	_lastFormTime = Glob.uiTime - 120;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
		_lastSelTime[i] = Glob.uiTime - 120;
	_lastUnitInfoTime = Glob.uiTime;
	_lastMenuTime = Glob.uiTime;
	#if PROTECTION_ENABLED && !CHECK_FADE && !ALWAYS_FAIL_CRC
		// anything between 0.5 .. 100 hours of playing
		// before message is shown
		_timeToPlay = Glob.uiTime+1800+GRandGen.RandomValue()*(3600*100);
	#else
		_timeToPlay = Glob.uiTime+GRandGen.PlusMinus(60,30);
	#endif

	_target = NULL;
	_lockTarget = NULL;
	_wantLock = false; // users wants to lock enemy target

	_lockAimValidUntil = Glob.uiTime-60;
//	_curWeapon = 0;
	
	_blinkState = false;
	_blinkStateChange = UITime(0);

	_dragging = false;
	_mouseDown = false;

//	_timeSendLoad = UITIME_MAX;

	Init();

	_lastUnitInfoType = NUnitInfoType;

	_unitInfo = new DisplayUnitInfo(NULL);

	_hint = new DisplayHint(NULL);
	_hint->SetHint("");
	_hintTime = Glob.uiTime;

	_tmPos = 1;
	_tmTime = UITime(0);
	_tmIn = false;
	_tmOut = false;

	_tankPos = 1;
	_tankTime = UITime(0);
	_tankIn = false;
	_tankOut = false;

	_font24=GLOB_ENGINE->LoadFont(GetFontID(FontS));
	_font36=GLOB_ENGINE->LoadFont(GetFontID(FontM));

	_leftPressed = false;
	_rightPressed = false;

	InitMenu();

	// TODO: remove
	tdName = "";
	giName = "";
	piName = "";
	uiName = "";

	const ParamEntry* mainCfg = &(Pars>>"CfgInGameUI");
	bgColor = GetPackedColor((*mainCfg)>>"colorBackground");
	bgColorCmd = GetPackedColor((*mainCfg)>>"colorBackgroundCommand");
	bgColorHelp = GetPackedColor((*mainCfg)>>"colorBackgroundHelp");
	ftColor = GetPackedColor((*mainCfg)>>"colorText");
	_imageBar = GLOB_ENGINE->TextBank()->Load
	(
		GetPictureName((*mainCfg) >> "Bar" >> "imageBar")
	);

	const ParamEntry* cfg = &((*mainCfg)>>"Picture");
	pictureColor = GetPackedColor((*cfg)>>"color");
	pictureProblemsColor = GetPackedColor((*cfg)>>"colorProblems");

	cfg = &((*mainCfg)>>"Capture");
	capBgColor = GetPackedColor((*cfg)>>"colorBackground");
	capFtColor = GetPackedColor((*cfg)>>"colorText");
	capLnColor = GetPackedColor((*cfg)>>"colorLine");

	cfg = &((*mainCfg)>>"Menu");
	tmX = (*cfg)>>"left";
	tmY = (*cfg)>>"top";
	tmW = (*cfg)>>"width";
	tmH = (*cfg)>>"height";
	menuCheckedColor = GetPackedColor((*cfg)>>"colorChecked");
	menuEnabledColor = GetPackedColor((*cfg)>>"colorEnabled");
	menuDisabledColor = GetPackedColor((*cfg)>>"colorDisabled");
	menuHideTime = (*cfg)>>"hideTime";

	cfg = &((*mainCfg)>>"Messages");
	msg1Color = GetPackedColor((*cfg)>>"color1");
	msg2Color = GetPackedColor((*cfg)>>"color2");
	msg3Color = GetPackedColor((*cfg)>>"color3");

	cfg = &((*mainCfg)>>"TacticalDisplay");
	tdX = (*cfg)>>"left";
	tdY = (*cfg)>>"top";
	tdW = (*cfg)>>"width";
	tdH = (*cfg)>>"height";
	friendlyColor = GetPackedColor((*cfg)>>"colorFriendly");
	enemyColor = GetPackedColor((*cfg)>>"colorEnemy");
	neutralColor = GetPackedColor((*cfg)>>"colorNeutral");
	civilianColor = GetPackedColor((*cfg)>>"colorCivilian");
	unknownColor = GetPackedColor((*cfg)>>"colorUnknown");
	cameraColor = GetPackedColor((*cfg)>>"colorCamera");
	cfg = &((*cfg)>>"Cursor");
	tdCurW = (*cfg)>>"width";
	tdCurH = (*cfg)>>"height";
	tdCursorColor = GetPackedColor((*cfg)>>"color");

	cfg = &((*mainCfg)>>"TankDirection");

	tankX = (*cfg)>>"left";
	tankY = (*cfg)>>"top";
	tankW = (*cfg)>>"width";
	tankH = (*cfg)>>"height";

	tankColor = GetPackedColor((*cfg)>>"color");
	tankColorFullDammage = GetPackedColor((*cfg)>>"colorFullDammage");
	tankColorHalfDammage = GetPackedColor((*cfg)>>"colorHalfDammage");

	_imageTurret = GlobLoadTexture(GetPictureName((*cfg) >> "imageTurret"));
	_imageGun = GlobLoadTexture(GetPictureName((*cfg) >> "imageGun"));
	_imageObsTurret = GlobLoadTexture(GetPictureName((*cfg) >> "imageObsTurret"));

	_imageHull = GlobLoadTexture(GetPictureName((*cfg) >> "imageHull"));
	_imageEngine = GlobLoadTexture(GetPictureName((*cfg) >> "imageEngine"));

	_imageLTrack = GlobLoadTexture(GetPictureName((*cfg) >> "imageLTrack"));
	_imageRTrack = GlobLoadTexture(GetPictureName((*cfg) >> "imageRTrack"));

	cfg = &((*mainCfg)>>"GroupDir");
	gdX = (*cfg)>>"left";
	gdY = (*cfg)>>"top";
	gdW = (*cfg)>>"width";
	gdH = (*cfg)>>"height";
	groupDirDimStartTime = (*cfg) >> "dimmStartTime";
	groupDirDimEndTime = (*cfg) >> "dimmEndTime";
	_imageGroupDir = GlobLoadTexture(GetPictureName((*cfg) >> "image"));

	cfg = &((*mainCfg)>>"Compass");
	coX = (*cfg)>>"left";
	coY = (*cfg)>>"top";
	coW = (*cfg)>>"width";
	coH = (*cfg)>>"height";
	compassColor = GetPackedColor((*cfg)>>"color");
	
	compassDirColor = GetPackedColor((*cfg)>>"dirColor");
	compassTurretDirColor = GetPackedColor((*cfg)>>"turretDirColor");
	
	cfg = &((*mainCfg)>>"GameInfo");
	giX = (*cfg)>>"left";
	giY = (*cfg)>>"top";
	giW = (*cfg)>>"width";
	giH = (*cfg)>>"height";

	cfg = &((*mainCfg)>>"PlayerInfo");
	timeColor = GetPackedColor((*cfg)>>"colorTime");
	piX = (*cfg)>>"left";
	piY = (*cfg)>>"top";
	piW = (*cfg)>>"width";
	piH = (*cfg)>>"height";
	piDimStartTime = (*cfg) >> "dimmStartTime";
	piDimEndTime = (*cfg) >> "dimmEndTime";
	abarW = (*cfg)>>"ArmorBar">>"width";
	fbarW = (*cfg)>>"FuelBar">>"width";
	hbarW = (*cfg)>>"HealthBar">>"width";
	ebarW = (*cfg)>>"ExperienceBar">>"width";
	ebarColor = GetPackedColor((*cfg)>>"ExperienceBar">>"color");
	ppicW = (*cfg)>>"UnitPicture">>"width";
	ppicH = (*cfg)>>"UnitPicture">>"height";
	piSideH = (*cfg)>>"Side">>"height";
	piSideW = (*cfg)>>"Side">>"width";
	cfg = &((*cfg)>>"Sign");
	piSignH = (*cfg)>>"height";
	piSignSW = (*cfg)>>"widthSector";
	piSignGW = (*cfg)>>"widthGroup";
	piSignUW = (*cfg)>>"widthUnit";

	cfg = &((*mainCfg)>>"GroupInfo");
	uiX = (*cfg)>>"left";
	uiY = (*cfg)>>"top";
	uiW = (*cfg)>>"width";
	uiH = (*cfg)>>"height";
	groupInfoDim = (*cfg)>>"dimm";
	uiColorNone = GetPackedColor((*cfg)>>"colorIDNone");
	uiColorNormal = GetPackedColor((*cfg)>>"colorIDNormal");
	uiColorSelected = GetPackedColor((*cfg)>>"colorIDSelected");
	uiColorPlayer = GetPackedColor((*cfg)>>"colorIDPlayer");
	_imageDefaultWeapons = GLOB_ENGINE->TextBank()->Load
	(
		GetPictureName((*cfg) >> "imageDefaultWeapons")
	);
	_imageNoWeapons = GLOB_ENGINE->TextBank()->Load
	(
		GetPictureName((*cfg) >> "imageNoWeapons")
	);
	cfg = &((*cfg)>>"Semaphore");
	semW = (*cfg)>>"width";
	semH = (*cfg)>>"height";
	holdFireColor = GetPackedColor((*cfg)>>"colorHoldFire");
	_imageSemaphore = GLOB_ENGINE->TextBank()->Load
	(
		GetPictureName((*cfg) >> "imageSemaphore")
	);

	cfg = &((*mainCfg)>>"Cursor");
	actW = (*cfg)>>"activeWidth";
	actH = (*cfg)>>"activeHeight";
	actMin = (*cfg)>>"activeMinimum";
	actMax = (*cfg)>>"activeMaximum";
	cursorColor = GetPackedColor((*cfg)>>"color");
	cursorBgColor = GetPackedColor((*cfg)>>"colorBackground");
	cursorDim = (*cfg)>>"dimm";
	cursorLockColor = GetPackedColor((*cfg)>>"colorLocked");
	enemyActColor = GetPackedColor((*cfg)>>"enemyActiveColor");
	_iconMe = GLOB_ENGINE->TextBank()->Load
	(
		GetPictureName((*cfg) >> "me")
	);
	_iconSelect = GLOB_ENGINE->TextBank()->Load
	(
		GetPictureName((*cfg) >> "select")
	);
	_iconLeader = GLOB_ENGINE->TextBank()->Load
	(
		GetPictureName((*cfg) >> "leader")
	);
	_iconMission = GLOB_ENGINE->TextBank()->Load
	(
		GetPictureName((*cfg) >> "mission")
	);

	meColor = GetPackedColor((*cfg)>>"meColor");
	selectColor = GetPackedColor((*cfg)>>"selectColor");
	leaderColor = GetPackedColor((*cfg)>>"leaderColor");
	missionColor = GetPackedColor((*cfg)>>"missionColor");

	meDim = (*cfg) >> "dimmMe";
	meDimStartTime = (*cfg) >> "dimmMeStartTime";
	meDimEndTime = (*cfg) >> "dimmMeEndTime";
	cmdDimStartTime = (*cfg) >> "dimmCmdStartTime";
	cmdDimEndTime = (*cfg) >> "dimmCmdEndTime";

	// TODO: read from the right place
	formDimStartTime = (*cfg) >> "dimmCmdStartTime";
	formDimEndTime = (*cfg) >> "dimmCmdEndTime";
	targetDimStartTime = (*cfg) >> "dimmCmdStartTime";
	targetDimEndTime = (*cfg) >> "dimmCmdEndTime";

	cfg = &((*cfg)>>"Sign");
	curSignH = (*cfg)>>"height";
	curSignSW = (*cfg)>>"widthSector";
	curSignGW = (*cfg)>>"widthGroup";
	curSignUW = (*cfg)>>"widthUnit";

	cfg = &((*mainCfg)>>"Bar");
	barBgColor = GetPackedColor((*cfg)>>"colorBackground");
	barGreenColor = GetPackedColor((*cfg)>>"colorGreen");
	barYellowColor = GetPackedColor((*cfg)>>"colorYellow");
	barRedColor = GetPackedColor((*cfg)>>"colorRed");
	barBlinkOnColor = GetPackedColor((*cfg)>>"colorBlinkOn");
	barBlinkOffColor = GetPackedColor((*cfg)>>"colorBlinkOff");
	barH = (*cfg)>>"height";

	// TODO: into config
	teamColors[TeamMain] = PackedColor(Color(1,1,1,1));
	teamColors[TeamRed] = PackedColor(Color(1,0,0,1));
	teamColors[TeamGreen] = PackedColor(Color(0,1,0,1));
	teamColors[TeamBlue] = PackedColor(Color(0,0,1,1));
	teamColors[TeamYellow] = PackedColor(Color(0.8,0.8,0,1));

	// TODO: into config
	dragColor = PackedColor(Color(0,1,0,1));

	cfg = &((*mainCfg)>>"Hint");
	hintDimStartTime = (*cfg) >> "dimmStartTime";
	hintDimEndTime = (*cfg) >> "dimmEndTime";
	GetValue(_hintSound, (*cfg) >> "sound");

	cfg = &((*mainCfg)>>"ConnectionLost");

	_clX = (*cfg) >> "left";
	_clY = (*cfg) >> "top";
	_clW = (*cfg) >> "width";
	_clH = (*cfg) >> "height";
	_clFont = GEngine->LoadFont(GetFontID((*cfg) >> "font"));
	_clSize = (*cfg) >> "size";
	_clColor = GetPackedColor((*cfg) >> "color");

}

InGameUI::~InGameUI()
{
	_visibleListTemp.Clear();
//	_visibleListS.Clear();
}

AbstractUI *CreateInGameUI() {return new InGameUI;}

/*
const char *InGameUI::GetMissionEnd()
{
	if (_missionEnd && _missionEnd->_title)
	{
		RString text = _missionEnd->_title->GetText();
		if ( text.GetLength() > 0)
			return text;
	}
	return NULL;
}

void InGameUI::SetMissionEnd(const char *text, int colorIndex)
{
	if (_missionEnd && _missionEnd->_title)
	{
		_missionEnd->_title->SetText(text);
		switch (colorIndex)
		{
		case 0:
			_missionEnd->_title->SetFtColor(unknownColor);
			break;
		case 1:
			_missionEnd->_title->SetFtColor(friendlyColor);
			break;
		case 2:
			_missionEnd->_title->SetFtColor(enemyColor);
			break;
		}
	}
}
*/

void AbstractUI::ShowAll(bool show)
{
	_showUnitInfo = show;
	_showTacticalDisplay = show;
	_showCompass = show;
	_showMenu = show;
	_showTankDirection = show;
	_showGroupInfo = show;
}

void InGameUI::Init()
{
	ShowAll();
	ShowCursors();
}


int ValidateWeapon(EntityAI *vehicle, int weapon)
{
	int n = vehicle->NMagazineSlots();
	if (weapon < 0 || weapon >= n) return -1;

	return weapon;
}

void InGameUI::ResetVehicle( EntityAI *vehicle )
{
	int weapon = vehicle->FirstWeapon();
	weapon = ValidateWeapon(vehicle, weapon);
	// if the weapon is not weapon, but rather special item, do not select it
	if (weapon>=0)
	{
		const MagazineSlot &slot = vehicle->GetMagazineSlot(weapon);
		const WeaponType *type = slot._weapon;
		if
		(
			!(type->_weaponType&MaskSlotPrimary) &&
			(type->_weaponType&(MaskSlotBinocular|MaskSlotSecondary))
		)
		{
			// do not autoselect binocular or secondary weapon
			weapon = -1;
		}
	}
	if (vehicle->QIsManual())
	{
		vehicle->SelectWeapon(weapon, true);
	}

//LogF("Select weapon %d (ResetVehicle)", weapon);

	if( vehicle==_target.IdExact() ) _target=NULL;
	if( vehicle==_lockTarget.IdExact() ) _lockTarget = NULL, _lockAimValidUntil = Glob.uiTime-60;

	_cursorWorld=false;
	_modelCursor=VForward;
	// commander should always use eye direction

	AIUnit *unit = GWorld->FocusOn();
	bool isObserver = unit && unit == vehicle->ObserverUnit();
	bool isGunner = unit && unit == vehicle->GunnerUnit();

	if (isObserver)
	{
		_worldCursor=vehicle->GetEyeDirection();
	}
	else if (weapon>=0 && isGunner)
	{
		_worldCursor=vehicle->GetWeaponDirection(0);
	}
	else
	{
		_worldCursor=vehicle->Direction();
	}
	SetCursorDirection(_worldCursor);

//	_timeSendLoad = UITIME_MAX;

	_lastCmdId = -1;
}

void InGameUI::OnWeaponRemoved(int slot)
{
	// TODO: better implementation
	AIUnit *unit = GWorld->FocusOn();
	if (!unit) return;
	SelectWeapon(unit, unit->GetVehicle()->FirstWeapon());
}

void InGameUI::ResetHUD()
{
	_target = NULL;
	_lockTarget = NULL;

	_lockAimValidUntil = Glob.uiTime-60;

//	_curWeapon = -1;

	_modeAuto = _mode = UIFire;
	_groundPointValid = false;

	ClearSelectedUnits();
	ClearTeams();
	
	_menuType = MTNone;
	_menuCurrent = _menuMain;

	_dragging = false;
	_mouseDown = false;

	_visibleListTemp.Clear();

//	_timeSendLoad = UITIME_MAX;

	_hint->SetHint("");

	_lastCmdId = -1;
}

DEFINE_ENUM_BEG(VCommand)
	VCFire,
	VCMove,
	VCCancelFire,
	VCCancelMove
DEFINE_ENUM_END(VCommand)

void InGameUI::IssueVCommand(EntityAI *vehicle, VCommand cmd)
{
	AIUnit *unit = GWorld->FocusOn();
	Transport *transport=dyn_cast<Transport>(vehicle);
	if( !transport ) return ;
	if (transport->CommanderUnit() != unit) return;
	// unit is vehicle commander
	// issue command to vehicle driver or gunner

	switch (cmd)
	{
		case VCMove:
			if ( _groundPointValid)
			{
				if (transport->IsLocal())
					transport->SendMove(_groundPoint);
				else
				{
					RadioMessageVMove msg(transport, _groundPoint);
					GetNetworkManager().SendRadioMessage(&msg);
				}
			}
			break;
		case VCFire:
			if (transport->IsLocal())
				transport->SendFire(_target);
			else
			{
				RadioMessageVFire msg(transport, _target);
				GetNetworkManager().SendRadioMessage(&msg);
			}
			break;
		case VCCancelMove:
			if (transport->IsLocal())
				transport->SendJoin();
			else
			{
				RadioMessageVFormation msg(transport);
				GetNetworkManager().SendRadioMessage(&msg);
			}
			break;
		case VCCancelFire:
			if (transport->IsLocal())
				transport->SendSimpleCommand(SCCeaseFire);
			else
			{
				RadioMessageVSimpleCommand msg(transport, SCCeaseFire);
				GetNetworkManager().SendRadioMessage(&msg);
			}
			break;
	}
}

void InGameUI::IssueCommand(EntityAI *vehicle, Command::Message cmd, bool follow)
{
	AIUnit *unit = GWorld->FocusOn();

	//AIUnit *unit = vehicle->Brain();
	if (!unit) return;
	AIGroup *grp = unit->GetGroup();
	if (!grp) return;

	if (cmd == Command::NoCommand)
	{	// auto command - command type is selected by context
		switch (_modeAuto)
		{
			case UIStrategySelect:
				{
					if (!GInput.keys[DIK_LSHIFT] && !GInput.keys[DIK_RSHIFT]) ClearSelectedUnits();

					EntityAI *vTarget=_target.IdExact();
					Assert( vTarget );
					if (!vTarget )
						return;
					AIUnit *u = vTarget->CommanderUnit();
					if (u)
					{
						if (u->GetVehicle() != vehicle)
						{
							int index = u->ID() - 1;
							SetSelectedUnit(index, u);
							_lastSelTime[index] = Glob.uiTime;
						}
					}
					return;
				}
			case UIStrategyMove:
				cmd = Command::Move;
				break;
			case UIStrategyAttack:
				cmd = Command::AttackAndFire;
				break;
			case UIStrategyGetIn:
				cmd = Command::GetIn;
				break;
			case UIStrategyWatch:
			{
				if (_target.IdExact())
				{
					grp->SendState(new RadioMessageWatchTgt(grp,ListSelectedUnits(),_target));
					ClearSelectedUnits();
				}
				else
				{
					grp->SendState(new RadioMessageWatchPos(grp,ListSelectedUnits(),_groundPoint));
					ClearSelectedUnits();
				}
				return;
			}
				
			default:
				return;
		}
	}

	if (cmd <= 0)
		return;


	vehicle = unit->GetVehicle();
	// count selected units
	if (IsEmptySelectedUnits())
	{
		// try to issue command to my vehicle
		Transport *transport=dyn_cast<Transport>(vehicle);
		if( !transport ) return ;
		if (transport->CommanderUnit() != unit) return;
		// unit is vehicle commander
		// issue command to vehicle driver or gunner

		switch (cmd)
		{
			case Command::Move:
				IssueVCommand(transport,VCMove);
				//if ( _groundPointValid) transport->SendMove(_groundPoint);
			break;
			case Command::Attack:
	#if ENABLE_HOLDFIRE_FIX
			case Command::AttackAndFire:
	#endif
				//IssueVCommand(transport,VCFire);
				//transport->SendFire(_target.idExact);
			break;
			case Command::Join:
				IssueVCommand(transport,VCCancelMove);
				//transport->SendJoin();
			break;
			/*
			case Command::Join:
				IssueVCommand(transport,VCCancelMove);
				//transport->SendJoin();
			break;
			*/
		}
		return;
	}

	// create command
	Command command;
	command._message = cmd;
	command._context = Command::CtxUI;
	switch (cmd)
	{	// add command type specific parameters
	case Command::Stop:
		// no destination
		break;
	case Command::Move:
		if (_target.IdExact())
		{
			// Move into house
			command._target = _target.IdExact();
			command._param = _housePos;
			const IPaths *house = command._target->GetIPaths();
			if (house && _housePos >= 0 && _housePos < house->NPos())
				command._destination = house->GetPosition(house->GetPos(_housePos));
			else
				command._destination = command._target->Position();
		}
		else
		{
			if (!_groundPointValid)
				return;
			command._destination = _groundPoint;
		}
		break;
	case Command::Heal:
		if (!grp->FindHealPosition(command)) return;
		goto JoinAfterCommand;
	case Command::Repair:
		if (!grp->FindRepairPosition(command)) return;
		goto JoinAfterCommand;
	case Command::Refuel:
		if (!grp->FindRefuelPosition(command)) return;
		goto JoinAfterCommand;
	case Command::Rearm:
	{
		for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
		{
			AIUnit *unit = GetSelectedUnit(i);
			if (!unit) continue;
			if (unit->IsSoldier())
			{
				const AITargetInfo *target = unit->CheckAmmo(AIUnit::RSCritical);
				if (target)
				{
					Command cmd;
					cmd._message = Command::Rearm;
					cmd._destination = target->_realPos;
					cmd._target = target->_idExact;
					if (unit->GetSubgroup() == grp->MainSubgroup())
					{
						cmd._context = Command::CtxUIWithJoin;
						cmd._joinToSubgroup = grp->MainSubgroup();
					}
					else
					{
						cmd._context = Command::CtxUI;
					}
					PackedBoolArray list;
					list.Set(unit->ID() - 1, true);
					grp->SendCommand(cmd, list);
				}
				SetSelectedUnit(i, NULL);
			}
		}
		if (IsEmptySelectedUnits()) return;
		if (!grp->FindRearmPosition(command)) return;
		goto JoinAfterCommand;
/*


		bool onlySoldiers=true;
		for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
		{
			AIUnit *unit = GetSelectedUnit(i);
			if (!unit) continue;
			if (!unit->IsSoldier() ) onlySoldiers=false;
		}
		if( onlySoldiers )
		{
			if (!grp->FindInfantryRearmPosition(command)) return;
		}
		else
		{
			if (!grp->FindRearmPosition(command)) return;
		}
*/
		goto JoinAfterCommand;
	}
	case Command::Join:
		command._joinToSubgroup = grp->MainSubgroup();
		break;
	case Command::Attack:
	#if ENABLE_HOLDFIRE_FIX
	case Command::AttackAndFire:
	#endif
	{
		// command destination is not used
		// we give commander's destination so that is contains something
		command._destination = vehicle->Position();
		command._targetE = _target;
		if (!command._targetE) command._targetE = _lockTarget;
		if (!command._targetE) return;
		goto JoinAfterCommand;
	}
	case Command::GetOut:
		for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
		{
			AIUnit *unit = GetSelectedUnit(i);
			if (!unit) continue;
			unit->AllowGetIn(false);
		}
JoinAfterCommand:
		if (CheckJoin(grp))
		{
			command._context = Command::CtxUIWithJoin;
			command._joinToSubgroup = grp->MainSubgroup();
		}
		break;
	case Command::GetIn:
		if (!_target.IdExact())
		{
			Transport *transport = dyn_cast<Transport>(unit->GetVehicle());
			if (transport && transport->QCanIGetInCargo())
			{
				command._target = transport;
			}
			else
				return;
		}
		else
		{
			Transport *transport = dyn_cast<Transport, Object>(_target.IdExact());
			if (transport)
			{
				command._target = transport;
			}
			else
				return;
		}

		{	// assign vehicle
			Transport *veh = dyn_cast<Transport, EntityAI>(command._target);
			Assert(veh);
			bool canAsDriver = veh->GetType()->HasDriver();
			bool canAsCommander = veh->GetType()->HasCommander();
			bool canAsGunner = veh->GetType()->HasGunner();
			AIUnit *driver = veh->GetDriverAssigned();
			if (driver)
			{
				if (veh->QIsDriverIn())
					canAsDriver = false;
				else if
				(
					driver->GetGroup() &&
					driver->GetGroup()->CommandSent(driver, Command::GetIn)
				)
					canAsDriver = false;
			}
			if (canAsCommander)
			{
				AIUnit *commander = veh->GetCommanderAssigned();
				if (commander)
				{
					if (veh->QIsCommanderIn())
						canAsCommander = false;
					else if
					(
						commander->GetGroup() &&
						commander->GetGroup()->CommandSent(commander, Command::GetIn)
					)
						canAsCommander = false;
				}
			}
			if (canAsGunner)
			{
				AIUnit *gunner = veh->GetGunnerAssigned();
				if (gunner)
				{
					if (veh->QIsGunnerIn())
						canAsGunner = false;
					else if
					(
						gunner->GetGroup() &&
						gunner->GetGroup()->CommandSent(gunner, Command::GetIn)
					)
						canAsGunner = false;
				}
			}
			for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
			{
				AIUnit *unit = GetSelectedUnit(i);
				if (!unit) continue;
				if (canAsDriver)
				{
					if (veh->GetGroupAssigned() != grp)
						grp->AddVehicle(veh);
					unit->AssignAsDriver(veh);
					canAsDriver = false;
				}
				else if (canAsGunner)
				{
					unit->AssignAsGunner(veh);
					canAsGunner = false;
				}
				else if (canAsCommander)
				{
					unit->AssignAsCommander(veh);
					canAsCommander = false;
				}
				else if (unit->VehicleAssigned() != veh)
					unit->AssignAsCargo(veh);
				unit->AllowGetIn(true);
			}
		}

		break;
	}

	grp->SendCommand(command, ListSelectedUnits());
	ClearSelectedUnits();
}

TargetSide InGameUI::RadarTargetSide( AIUnit *unit, Target &tar )
{
	TargetSide side = tar.idExact->GetType()->GetTypicalSide();
	if (!tar.idExact->EngineIsOn())
	{
		AIGroup *grp = tar.idExact->GetGroup();
		if (grp!=unit->GetGroup())
		{
			side = TCivilian; // empty marked as civilian
		}
	}

	if
	(
		Glob.config.IsEnabled(DTEnemyTag)
#if _ENABLE_CHEATS
		|| _showAll
#endif
	)
	{
		side = tar.side;
		if (tar.destroyed) side = TCivilian;
	}
	return side;
}

/*!
\patch 1.28 Date 10/24/2001 by Ondra.
- Fixed: Locking missile was possible even on some targets that cannot be locked.
*/

void InGameUI::FindTarget( EntityAI *me, bool prev )
{
	int weapon = me->SelectedWeapon();
	weapon = ValidateWeapon(me, weapon);
	if( weapon<0 ) return;
	Vector3 curDir=me->GetWeaponDirection(weapon);
	AIUnit *unit=me->CommanderUnit();
	if( !unit ) return; // no commander - no TAB switching
	if( unit->IsFreeSoldier() ) return; // soldier - no TAB switching
	if( unit->IsGunner() ) return; // soldier - no TAB switching
	if( _lockTarget ) curDir=_lockTarget->AimingPosition()-me->Position();
	else if ( _lockAimValidUntil>=Glob.uiTime ) curDir=_lockAim;
	AICenter *myCenter=unit->GetGroup()->GetCenter();
	float curAzimut=atan2(curDir.X(),curDir.Z());
	int i;
	// check radar visible targets
	// lock only enemy or unknown
	const TargetList &visibleList = *VisibleList();

	float minDiffE=1e10;
	float minDiffU=H_PI/4;
	int minIE=-1;
	int minIU=-1;

	for( i=0; i<visibleList.Size(); i++ )
	{
		Target &tar = *visibleList[i];
		if (tar.vanished) continue;
		if (tar.destroyed) continue;
		/*
		if( myCenter->IsEnemy(tar.side) ) nEnemy++;
		*/
		EntityAI *ai = tar.idExact;
		if (!ai) continue;
		if (ai==_lockTarget.IdExact()) continue; // skip current target
		if (ai==me) continue; // skip current target
		if
		(
			!ai->GetType()->GetIRTarget()
			&& !ai->GetType()->GetLaserTarget()
		) continue; // skip non-IR targets
		if (!me->CanLock(ai)) continue;
		Vector3Val pos = ai->AimingPosition();
		float dist2 = me->Position().Distance2(pos);
		float visible = me->CalcVisibility(ai,dist2);
		if (visible<0.01) continue;
		TargetSide side = RadarTargetSide(unit,tar);

		Vector3 relPos=pos-me->Position();
		float azimut=atan2(relPos.X(),relPos.Z());
		float diff=AngleDifference(azimut,curAzimut);
		if( prev ) diff=-diff;
		if( diff<=0 ) diff+=(H_PI*2);
		if( myCenter->IsEnemy(side) )
		{
			if( minDiffE>diff ) minDiffE=diff,minIE=i;
		}
		else if (side==TSideUnknown)
		{
			if( minDiffU>diff ) minDiffU=diff,minIU=i;
		}
	}
	if (minIE<0) minIE = minIU;
	if( minIE<0 )
	{
		Matrix3 rotY(MRotationY,prev ? +H_PI/4 : -H_PI/4);
		_lockTarget=NULL;
		_lockAim=rotY*curDir;
		_lockAimValidUntil=Glob.uiTime+3.0;
		return; // no target
	}
	Target *tgt=visibleList[minIE];
	_lockTarget=tgt;
	// we have to disclose and report target
	RevealTarget(tgt,0.3);

	_timeSendTarget = Glob.uiTime + 1.0;
	_lockAimValidUntil=Glob.uiTime-60;
}

#define CameraFrame() GScene->GetCamera()
//#define CameraFrame() GWorld->CameraOn()

void InGameUI::SetCursorMode( bool world )
{
	if( _cursorWorld==world ) return;
	_cursorWorld=world;
	const FrameBase *cam=CameraFrame();
	Assert( cam );
	if( cam )
	{
		if( !_cursorWorld )
		{ // convert from world cursor
			Matrix4Val invTransform=cam->GetInvTransform();
			_modelCursor=invTransform.Rotate(_worldCursor);
		}
		else
		{ // convert to world cursor
			_worldCursor=cam->DirectionModelToWorld(_modelCursor);
		}
	}
}

bool InGameUI::GetCursorMode() const {return _cursorWorld;}

Vector3 InGameUI::GetWorldCursor() const {return _worldCursor;}
void InGameUI::SetWorldCursor( Vector3Par dir ) {_worldCursor=dir;}
Vector3 InGameUI::GetModelCursor() const {return _modelCursor;}
void InGameUI::SetModelCursor( Vector3Par dir )
{
	_modelCursor=dir;
}

Vector3 InGameUI::GetCursorDirection() const
{
	if( !_cursorWorld )
	{
		const FrameBase *cam=CameraFrame();
		if( cam )
		{
			return cam->DirectionModelToWorld(_modelCursor);
		}
	}
	return _worldCursor;
}

void InGameUI::SetCursorDirection( Vector3Par dir )
{
	if( !_cursorWorld )
	{
		const FrameBase *cam=CameraFrame();
		if( cam )
		{
			Matrix4Val invTransform=cam->GetInvTransform();
			//_modelCursor=invTransform.Rotate(_worldCursor);
			_modelCursor=invTransform.Rotate(dir);
			return;
		}
		return;
	}
	_worldCursor=dir;
}

bool InGameUI::CheckJoin(AIGroup *grp)
{
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *u = GetSelectedUnit(i);
		if (!u) continue;
		if (u->GetSubgroup() == grp->MainSubgroup()) return true;
//		if (grp->CommandSent(u, Command::Join)) return true;
		// TODO: check stack and radio
	}
	return false;
}

void InGameUI::BackupTargets()
{
	_visibleListTemp.Clear();
	const TargetList &visibleList=*VisibleList();
	for (int i=0; i<visibleList.Size(); i++)
	{
		Target *target = visibleList[i];
		if (target) _visibleListTemp.Add(target);
	}
}

#define COMMAND_TIMEOUT			480.0		// 8 min

void InGameUI::IssueAction(AIGroup *grp, MenuItem &item)
{
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = GetSelectedUnit(i);
		if (unit)
		{
			if (item._target && item._target != unit->GetVehicle())
			{
				// send autocommand
				Command cmd;
				cmd._message = Command::Action;
				cmd._action = item._action;
				cmd._target = item._target;
				cmd._destination = item._target->Position();
				cmd._param = item._param;
				cmd._param2 = item._param2;
				cmd._param3 = item._param3;
				cmd._time = Glob.time + COMMAND_TIMEOUT;
				unit->GetGroup()->SendAutoCommandToUnit(cmd, unit, true);
			}
			else
			{
				// process immediatelly
				UIAction action;
				action.type = item._action;
				action.target = unit->GetVehicle();
				action.param = item._param;
				action.param2 = item._param2;
				action.param3 = item._param3;
				action.priority = 0;
				action.showWindow = false;
				action.hideOnUse = false;
				unit->GetVehicle()->StartActionProcessing(action,unit);
			}
		}
	}
	
	ClearSelectedUnits();

/*
	if (IsEmptySelectedUnits()) return;

	AIUnit *unit = NULL;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *u = GetSelectedUnit(i);
		if (u)
		{
			unit = u;
			break;
		}
	}
	if (!unit) return;

	int tgt = index / N_ACTIONS;
	index -= tgt * N_ACTIONS;
	int action = index;

	Target *target = _visibleListTemp[tgt];
	if (!target) return;

	Command cmd;
	switch (action)
	{
	case ATHeal:
		cmd._message = Command::Heal;
		break;
	case ATRepair:
		cmd._message = Command::Repair;
		break;
	case ATRefuel:
		cmd._message = Command::Refuel;
		break;
	case ATRearm:
		cmd._message = Command::Rearm;
		break;
	case ATTakeWeapon:
		cmd._message = Command::TakeWeapon;
		break;
	case ATTakeMagazine:
		cmd._message = Command::TakeMagazine;
		break;
	default:
		Fail("Unexpected action type");
		return;
	}
	cmd._param = param;
	cmd._param2 = param2;
	cmd._param3 = param3;
	cmd._destination = target->position;
	cmd._target = target->idExact;
	cmd._time = Glob.time + COMMAND_TIMEOUT;
	if (CheckJoin(grp))
	{
		cmd._context = Command::CtxUIWithJoin;
		cmd._joinToSubgroup = grp->MainSubgroup();
	}
	else
	{
		cmd._context = Command::CtxUI;
	}

	grp->SendCommand(cmd, ListSelectedUnits());
	ClearSelectedUnits();
*/
}

void InGameUI::IssueMove(AIGroup *grp, int where)
{
	if (IsEmptySelectedUnits()) return;

	AIUnit *unit = NULL;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *u = GetSelectedUnit(i);
		if (u)
		{
			unit = u;
			break;
		}
	}
	if (!unit) return;

	int dir = where / N_MOVE_DIST;
	int dist = where - dir * N_MOVE_DIST;
	float angle = (2 * H_PI / N_MOVE_DIR) * dir;
	float distance = 0;
	switch (dist)
	{
	case 0: distance = 50; break;
	case 1: distance = 100; break;
	case 2: distance = 200; break;
	case 3: distance = 500; break;
	case 4: distance = 1000; break;
	case 5: distance = 2000; break;
	}
	Matrix3 rotY(MRotationY, angle);
	Vector3 move = Vector3(0, 0, distance) * rotY;

	Command cmd;
	cmd._message = Command::Move;
	cmd._context = Command::CtxUI;
	cmd._destination = unit->Position() + move;
	grp->SendCommand(cmd, ListSelectedUnits());
	ClearSelectedUnits();
}

void InGameUI::IssueVMove(Transport *vehicle, int where)
{
	if (!vehicle) return;

	int dir = where / N_MOVE_DIST;
	int dist = where - dir * N_MOVE_DIST;
	float angle = (2 * H_PI / N_MOVE_DIR) * dir;
	float distance = 0;
	switch (dist)
	{
	case 0: distance = 50; break;
	case 1: distance = 100; break;
	case 2: distance = 200; break;
	case 3: distance = 500; break;
	case 4: distance = 1000; break;
	case 5: distance = 2000; break;
	}
	Matrix3 rotY(MRotationY, angle);
	Vector3 move = Vector3(0, 0, distance) * rotY;
	Vector3 pos = vehicle->Position() + move;

	if (vehicle->IsLocal())
		vehicle->SendMove(pos);
	else
	{
		RadioMessageVMove msg(vehicle, pos);
		GetNetworkManager().SendRadioMessage(&msg);
	}
}

void InGameUI::IssueWatchTarget(AIGroup *grp, int tgt)
{
	if (IsEmptySelectedUnits()) return;

	Target *target = _visibleListTemp[tgt];
	if (!target) return;

	grp->SendTarget(target,false,false,ListSelectedUnits());

	//grp->SendState(new RadioMessageWatchTgt(grp,ListSelectedUnits(),target));
	ClearSelectedUnits();
}

void InGameUI::IssueWatchAround(AIGroup *grp)
{
	if (IsEmptySelectedUnits()) return;
	grp->SendState(new RadioMessageWatchAround(grp,ListSelectedUnits()));
	ClearSelectedUnits();
}

void InGameUI::IssueEngage(AIGroup *grp)
{
	if (IsEmptySelectedUnits()) return;
	grp->SendState(new RadioMessageTarget(grp,ListSelectedUnits(),NULL,true,false));
	ClearSelectedUnits();
}

void InGameUI::IssueFire(AIGroup *grp)
{
	if (IsEmptySelectedUnits()) return;
	grp->SendState(new RadioMessageTarget(grp,ListSelectedUnits(),NULL,false,true));
	ClearSelectedUnits();
}

void InGameUI::IssueWatchAuto(AIGroup *grp)
{
	if (IsEmptySelectedUnits()) return;
	grp->SendState(new RadioMessageWatchAuto(grp,ListSelectedUnits()));
	ClearSelectedUnits();
}

void InGameUI::SendFireReady(AIUnit *unit, bool ready)
{
	if (!unit) return;
	AIGroup *grp = unit->GetGroup();
	if (!grp) return;
	grp->ReportFire(unit,ready);	
}

/*!
\patch 1.82 Date 8/23/2002 by Ondra
- Fixed: Several fixes in radio submenu 0 - Reply.
- Custom radio key was the same as key for Repeat.
- Mission radio key was the same as key for Copy.
- Done could be issued only when some command was active.
- Negative could be issued only when some command was active.
*/

void InGameUI::SendAnswer(AIUnit *unit, AI::Answer answer)
{
	AIGroup *grp = unit->GetGroup();
	if (!grp) return;
	if (unit->IsSubgroupLeader())
	{
		AISubgroup *subgrp = unit->GetSubgroup();
		subgrp->SendAnswer(answer);
		subgrp->FailCommand();
		if (unit == grp->Leader())
		{
			Command temp;
			temp._message = Command::Wait;
			grp->GetRadio().Transmit
			(
				new RadioMessageSubgroupAnswer(unit->GetSubgroup(), NULL, answer, &temp, true, unit),
				grp->GetCenter()->GetLanguage()
			);
		}
	}
	else
	{
		Command temp;
		temp._message = Command::Wait;
		grp->GetRadio().Transmit
		(
			new RadioMessageSubgroupAnswer(unit->GetSubgroup(), NULL, answer, &temp, true, unit),
			grp->GetCenter()->GetLanguage()
		);

	}
}

void InGameUI::SendConfirm(AIUnit *unit)
{
	// get command
	AISubgroup *subgrp = unit->GetSubgroup();
	Command *cmd = subgrp->GetCommand();
	Command temp;
	if (!cmd)
	{
		temp._message = Command::Wait;
		cmd = &temp;
	}
	AIGroup *group = subgrp->GetGroup();
	group->GetRadio().Transmit
	(
		new RadioMessageCommandConfirm(unit, group, *cmd),
		group->GetCenter()->GetLanguage()
	);
}

void InGameUI::SendResourceState(AIUnit *unit,AI::Answer answer)
{
	unit->SendAnswer(answer);
	// report some state
}

void InGameUI::SendObjectDestroyed(AIUnit *unit,AIGroup *grp)
{
	// find if there is some taget to report
	if (!grp) return;
	const TargetList &list = grp->GetTargetList();
	Target *tgt = NULL;
	for (int i=0; i<list.Size(); i++)
	{
		Target *tar = list[i];
		EntityAI *killer = tar->idKiller;
		if (!killer) continue;
		if
		(
			(killer==unit->GetVehicle() || killer==unit->GetPerson()) &&
			tar->destroyed &&
			(tar->timeReported<=TIME_MIN || tar->timeReported<Glob.time-30)
		)
		{
			tgt = tar;
			// mark as reported
			tar->timeReported = Glob.time;
			tar->posReported = tar->position;
			break;
		}
	}

	AICenter *center = grp->GetCenter();
	grp->GetRadio().Transmit
	(
		new RadioMessageObjectDestroyed(unit, grp, tgt ? tgt->type : NULL),
		center->GetLanguage()
	);
}

void InGameUI::SendKilled(AIUnit *unit,PackedBoolArray list)
{
	AIGroup *grp = unit->GetGroup();
	if (!grp) return;
	// TODO: multiple units is killed list
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		if (list.Get(i)) grp->SendUnitDown(unit,grp->UnitWithID(i+1));
	}
}

void InGameUI::IssueWatch(AIGroup *grp,int what)
{
	if (IsEmptySelectedUnits()) return;

	int dir = what;
	float angle = (-2 * H_PI / N_WATCH_DIR) * dir;
	Matrix3 rotY(MRotationY, angle);
	Vector3 tgtDir = rotY.Direction();

	grp->SendState(new RadioMessageWatchDir(grp,ListSelectedUnits(),tgtDir));

	ClearSelectedUnits();
}

void InGameUI::IssueAttack(AIGroup *grp, int tgt)
{
	if (IsEmptySelectedUnits()) return;

	Target *target = _visibleListTemp[tgt];
	if (!target) return;

	AIUnit *unit = NULL;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *u = GetSelectedUnit(i);
		if (u)
		{
			unit = u;
			break;
		}
	}
	if (!unit) return;

	Command cmd;
	cmd._message = Command::AttackAndFire;
	cmd._targetE = target;
	cmd._destination = unit->Position();

	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *u = GetSelectedUnit(i);
		if (u) u->AssignTarget(target);
	}

	if (CheckJoin(grp))
	{
		cmd._context = Command::CtxUIWithJoin;
		cmd._joinToSubgroup = grp->MainSubgroup();
	}
	else
	{
		cmd._context = Command::CtxUI;
	}


	grp->SendCommand(cmd, ListSelectedUnits());
	ClearSelectedUnits();
}

void InGameUI::IssueGetIn(AIGroup *grp, int index)
{
	if (IsEmptySelectedUnits()) return;

	int tgt = index / N_GETIN_POS;
	int pos = index - tgt * N_GETIN_POS;

	Target *target = _visibleListTemp[tgt];
	if (!target) return;
	TargetType *obj = target->idExact;
	Transport *veh = dyn_cast<Transport>(obj);
	if (!veh) return;

	// assign vehicle
	bool canAsDriver = veh->GetType()->HasDriver();
	bool canAsCommander = veh->GetType()->HasCommander();
	bool canAsGunner = veh->GetType()->HasGunner();
	AIUnit *driver = veh->GetDriverAssigned();
	if (driver)
	{
		if (veh->QIsDriverIn())
			canAsDriver = false;
		else if
		(
			driver->GetGroup() &&
			driver->GetGroup()->CommandSent(driver, Command::GetIn)
		)
			canAsDriver = false;
	}
	if (canAsCommander)
	{
		AIUnit *commander = veh->GetCommanderAssigned();
		if (commander)
		{
			if (veh->QIsCommanderIn())
				canAsCommander = false;
			else if
			(
				commander->GetGroup() &&
				commander->GetGroup()->CommandSent(commander, Command::GetIn)
			)
				canAsCommander = false;
		}
	}
	if (canAsGunner)
	{
		AIUnit *gunner = veh->GetGunnerAssigned();
		if (gunner)
		{
			if (veh->QIsGunnerIn())
				canAsGunner = false;
			else if
			(
				gunner->GetGroup() &&
				gunner->GetGroup()->CommandSent(gunner, Command::GetIn)
			)
				canAsGunner = false;
		}
	}

	switch (pos)
	{
	case 1:
		if (!canAsDriver) return;
		break;
	case 2:
		if (!canAsCommander) return;
		break;
	case 3:
		if (!canAsGunner) return;
		break;
	}

	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = GetSelectedUnit(i);
		if (!unit) continue;
		unit->AllowGetIn(true);

		switch (pos)
		{
		case 0: // anywhere
			if (canAsDriver)
			{
				if (veh->GetGroupAssigned() != grp)
					grp->AddVehicle(veh);
				unit->AssignAsDriver(veh);
				canAsDriver = false;
			}
			else if (canAsCommander)
			{
				unit->AssignAsCommander(veh);
				canAsCommander = false;
			}
			else if (canAsGunner)
			{
				unit->AssignAsGunner(veh);
				canAsGunner = false;
			}
			else if (unit->VehicleAssigned() != veh)
				unit->AssignAsCargo(veh);
			break;
		case 4: // cargo
//			if (unit->VehicleAssigned() != veh)
			unit->AssignAsCargo(veh);
			break;
		case 1: // driver
			// canAsDriver == true
			if (veh->GetGroupAssigned() != grp)
				grp->AddVehicle(veh);
			unit->AssignAsDriver(veh);
			ClearSelectedUnits();
			SetSelectedUnit(i, unit);
			goto SendGetIn;
		case 2: // commander
			// canAsCommander == true
			unit->AssignAsCommander(veh);
			ClearSelectedUnits();
			SetSelectedUnit(i, unit);
			goto SendGetIn;
		case 3: // gunner
			// canAsGunner == true
			unit->AssignAsGunner(veh);
			ClearSelectedUnits();
			SetSelectedUnit(i, unit);
			goto SendGetIn;
		}
	}

SendGetIn:

	Command cmd;
	cmd._message = Command::GetIn;
	cmd._target = obj;
	/*
	if (CheckJoin(grp))
	{
		cmd._context = Command::CtxUIWithJoin;
		cmd._joinToSubgroup = grp->MainSubgroup();
	}
	else
	*/
	{
		cmd._context = Command::CtxUI;
	}


	grp->SendCommand(cmd, ListSelectedUnits());
	ClearSelectedUnits();
}

void InGameUI::CreateAttackList(AIGroup *group, Menu *submenu, int cmdBase)
{
	int nItem = 1;

	submenu->AddItem
	(
		new MenuItem
		(
			LocalizeString(IDS_WATCH_AUTO),
			DIK_1,
			"1",
			CMD_WATCH_AUTO
		)
	);

	bool friendly = false;
	RString menuName = submenu->_text;
	
	int n = _visibleListTemp.Size();
	for (int i=0; i<n; i++)
	{
		Target *target = _visibleListTemp[i];
		if (!target) continue;
		if (!target->IsKnown()) continue;
		if (target->vanished) continue;
		if (target->destroyed) continue;
		if (!target->idExact) continue;
		bool forceSplit = !friendly &&
			target->side != TSideUnknown &&
			!group->GetCenter()->IsEnemy(target->side);
		if (forceSplit) friendly = true;
//		if (nItem == 0) forceSplit = false;
		if (nItem == 9 || forceSplit)
		{
			Menu *newmenu = new Menu();
			newmenu->_text = menuName;
			newmenu->_parent = submenu;
			submenu->AddItem
			(
				new MenuItem
				(
					LocalizeString(IDS_MORE_MENU),
					DIK_0, "0", newmenu, CMD_NOTHING
				)
			);
			submenu->AddItem
			(
				new MenuItem
				(
					LocalizeString(IDS_CANCEL_MENU),
					DIK_BACK, LocalizeString(IDS_MENU_BACKSPACE), CMD_BACK
				)
			);
			submenu = newmenu;
			nItem = 0;
		}
		char key[2] = "1"; key[0] += nItem;
		RString name = target->type->GetDisplayName();

		char text[256];

		EntityAI *veh = target->idExact;
		AIUnit *u = veh ? veh->CommanderUnit() : NULL;
		AIGroup *g = u ? u->GetGroup() : NULL;
		if (g == group)
		{
			sprintf
			(
				text, LocalizeString(IDS_TARGET_MENU_GROUP),
				(const char *)u->GetPerson()->GetInfo()._name,
				(const char *)name, u->ID()
			);
		}
		else
		{
			Vector3 pos = GWorld->CameraOn()->GetInvTransform() * target->position;
			int azimut = toInt(atan2(pos.X(), pos.Z()) * (6 / H_PI));
			if (azimut <= 0) azimut += 12;

			bool showSensor = false;
			EntityAI *sensorVeh = target->idSensor;
			AIUnit *sensor = sensorVeh ? sensorVeh->CommanderUnit() : NULL;
			if (sensor)
			{
				AISubgroup *subSensor = sensor->GetSubgroup();
				if (subSensor)
				{
					AIGroup *grpSensor = subSensor->GetGroup();
					if (grpSensor == group)
					{
						showSensor = subSensor != group->MainSubgroup();
					}
				}
			}
			
			if (showSensor)
			{
				sprintf
				(
					text, LocalizeString(IDS_TARGET_MENU_SENSOR),
					(const char *)name, sensor->ID(), azimut
				);
			}
			else
			{
				sprintf
				(
					text, LocalizeString(IDS_TARGET_MENU),
					(const char *)name, azimut
				);
			}
		}
		submenu->AddItem
		(
			new MenuItem
			(
				text,
				DIK_1 + nItem,
				key,
				cmdBase + i
			)
		);
		nItem++;
	}
	submenu->AddItem
	(
		new MenuItem
		(
			LocalizeString(IDS_CANCEL_MENU),
			DIK_BACK, LocalizeString(IDS_MENU_BACKSPACE), CMD_BACK
		)
	);
}

void ActivateSensor(ArcadeSensorActivation activ);

void InGameUI::CollectActions(UIActions &actions)
{
	AIUnit *unit = NULL;
	int nUnits = 0;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		if (GSelectedUnits[i])
		{
			unit = GSelectedUnits[i];
			nUnits++;
		}
	}

	if (nUnits == 0) return;
	if (nUnits == 1)
	{
		// one unit selected
		int n = _visibleListTemp.Size();
		for (int i=0; i<n; i++)
		{
			Target *target = _visibleListTemp[i];
			if (!target) continue;
			if (!target->IsKnown()) continue;
			if (target->vanished) continue;
			EntityAI *veh = target->idExact;
			if (!veh) continue;
/*
			if 
			(
				!group->GetCenter()->IsFriendly(target->side) &&
				!veh->IsDammageDestroyed()
			) continue;
*/
			veh->GetActions(actions, unit, false);
		}
#if _ENABLE_DATADISC
    AddDropAllActions(unit, actions);
#endif
	}
	else
	{
		// more units selected
		for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
		{
			AIUnit *unit = GSelectedUnits[i];
			if (unit)
				unit->GetVehicle()->GetActions(actions, unit, true);
		}
		for (int i=0; i<actions.Size(); i++) actions[i].target = NULL;
	}

	for (int i=0; i<actions.Size();)
	{
		UIActionType type = actions[i].type;
		if
		(
			type == ATGetInCommander ||
			type == ATGetInDriver ||
			type == ATGetInGunner ||
			type == ATGetInCargo ||
			type == ATGetOut
		)	actions.Delete(i);
		else i++;
	}

	// agregation
	for (int i=0; i<actions.Size(); i++)
	{
		UIAction &action1 = actions[i];
		for (int j=i+1; j<actions.Size();)
		{
			UIAction &action2 = actions[j];
			if
			(
				action1.type == action2.type &&
				action1.param == action2.param &&
				action1.param2 == action2.param2 &&
				action1.param3 == action2.param3
			)
			{
				// select action with better target
				if (action1.target)
				{
					if (action2.target)
					{
						// select better target
						Vector3Val pos = unit->Position();
						if (pos.Distance2(action1.target->Position()) > pos.Distance2(action2.target->Position()))
							action1.target = action2.target;
					}
					// else action2 has no target - delete it
				}
				else
				{
					// action1 has no target - delete it
					action1.target = action2.target;
				}
				actions.Delete(j);
			}
			else j++;
		}
	}
	
	// sorting
	actions.Sort();
}

void InGameUI::RefreshActionsMenu()
{
	MenuItem *item = NULL;
	for (int i=0; i<_menuMain->_items.Size(); i++)
	{
		MenuItem *it = _menuMain->_items[i];
		if (it->_cmd == CMD_ACTION)
		{
			item = it;
			break;
		}
	}
	if (!item) return;

	UIActions actions;
	BackupTargets();
	CollectActions(actions);
	
	// level 1 submenu becomes current
	Menu *submenu = new Menu();
	submenu->_text = LocalizeString(IDS_ACTION);
	submenu->_parent = _menuMain;
	item->_submenu = submenu;
	_menuMain->RescanMinMax();

	_menuCurrent = submenu;
	if (_menuType == MTNone) _menuType = MTMain;
	ShowMenu();

	// add actions to menu
	int nItem = 0;
	for (int i=0; i<actions.Size(); i++)
	{
		if (nItem == 9)
		{
			// more actions - new submenu
			Menu *newmenu = new Menu();
			newmenu->_text = LocalizeString(IDS_ACTION);
			newmenu->_parent = submenu;
			submenu->AddItem
			(
				new MenuItem
				(
					LocalizeString(IDS_MORE_MENU),
					DIK_0, "0", newmenu, CMD_NOTHING
				)
			);
			submenu->AddItem
			(
				new MenuItem
				(
					LocalizeString(IDS_CANCEL_MENU),
					DIK_BACK, LocalizeString(IDS_MENU_BACKSPACE), CMD_BACK
				)
			);
			submenu = newmenu;
			nItem = 0;
		}
		// add action
		UIAction &action = actions[i];
		RString displayName = action.GetDisplayName(NULL);
		if (displayName.GetLength() == 0) continue;
		char key[2] = "1"; key[0] += nItem;
		submenu->AddItem
		(
			new MenuItem
			(
				displayName,
				DIK_1 + nItem, key, CMD_ACTION_TARGET,
				action.type, action.target,
				action.param, action.param2, action.param3
			)
		);
		nItem++;
	}
	submenu->AddItem
	(
		new MenuItem
		(
			LocalizeString(IDS_CANCEL_MENU),
			DIK_BACK, LocalizeString(IDS_MENU_BACKSPACE), CMD_BACK
		)
	);
}

/*!
\patch 1.01 Date 06/19/2001 by Jirka
- Added: "NEXT WAYPOINT" command implemented for selected units
- was only for commanded vehicle in 1.00
\patch 1.22 Date 08/27/2001 by Jirka
- Added: hide in-game menu after mouse command or after some time
\patch 1.78 Date 7/10/2002 by Jirka
- Added: Radio commands India & Juliet
*/


void InGameUI::ProcessMenu(const Camera &camera, EntityAI *vehicle)
{
	AIUnit *unit = GWorld->FocusOn();
	if( !unit ) return;
	AISubgroup *subgroup = unit->GetSubgroup();
	Assert(subgroup);
	AIGroup *group = subgroup->GetGroup();
	Assert(group);
	AICenter *center = group->GetCenter();
	Assert(center);

	// blink state
	if (Glob.uiTime > _blinkStateChange)
	{
		_blinkStateChange = Glob.uiTime + 0.5;
		_blinkState = !_blinkState;
	}

//	if (!GInput.keys[DIK_RMENU] && !GInput.keys[DIK_RWIN])
	{
		static const struct UnitKey
		{
			int key, unit;
		} unitKeys[MAX_UNITS_PER_GROUP] =
		{
			{DIK_F1,0},{DIK_F2,1},{DIK_F3,2},{DIK_F4,3},
			{DIK_F5,4},{DIK_F6,5},{DIK_F7,6},{DIK_F8,7},
			{DIK_F9,8},{DIK_F10,9},{DIK_F11,10},{DIK_F12,11}
		};
		if (unit->IsGroupLeader() && group->NUnits() > 1)
		{
			// TODO: clear units that are dead/not present at all

			// Units selection
			bool selectionChanged = false;
			for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
			{
				if (GInput.GetKeyToDo(unitKeys[i].key))
				{
					ToggleSelection(group, unitKeys[i].unit + 1);
					selectionChanged = true;
				}
			}
			if (GInput.GetActionToDo(UASelectAll))
			{
				bool allSelected=true;
				bool mainSubgrpSelected=true;
				for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
				{
					AIUnit *u = group->UnitWithID(i + 1);
					if (!u || u == unit) continue;
					if (!GetSelectedUnit(i))
					{
						allSelected = false;
						if (u->GetSubgroup() == group->MainSubgroup())
							mainSubgrpSelected=false;
					}
				}
				if (allSelected)
					ClearSelectedUnits();
				else if (mainSubgrpSelected)
				{
					for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
					{
						AIUnit *u = group->UnitWithID(i + 1);
						if (!u || u == unit) continue;
						SetSelectedUnit(i, u);
					}
				}
				else
				{
					for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
					{
						AIUnit *u = group->UnitWithID(i + 1);
						if (!u || u == unit) continue;
						if (u->GetSubgroup() == group->MainSubgroup())
							SetSelectedUnit(i, u);
						else
							SetSelectedUnit(i, NULL);
					}
				}
				selectionChanged = true;
			}
			if (selectionChanged)
			{
				if (_menuType == MTNone && !IsEmptySelectedUnits())
				{
					_menuType = MTMain;
					_menuCurrent = _menuMain;
				}
				ShowMenu();

				Menu *actionMenu = NULL;
				for (int i=0; i<_menuMain->_items.Size(); i++)
				{
					MenuItem *item = _menuMain->_items[i];
					if (item->_cmd == CMD_ACTION)
					{
						actionMenu = item->_submenu;
						break;
					}
				}
				bool found = false;
				Menu *menu = _menuCurrent;
				while (menu)
				{
					if (menu == actionMenu)
					{
						found = true;
						break;
					}
					menu = menu->_parent;
				}
				if (found) RefreshActionsMenu();
			}
		}
		else
		{
			for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
			{
				if (GInput.GetKeyToDo(unitKeys[i].key))
				{
					_menuType = MTMain;
					ShowMenu();
				}
			}
			ClearSelectedUnits();
		}
	}

	bool enableCommands =
	(
		unit->IsGroupLeader() && unit->GetLifeState()==AIUnit::LSAlive && group->NUnits() > 1
	);

	// check list of selected units
	bool notEmpty = false;
	bool notEmptyMySubgroup = false;
	bool notEmptySubgroups = false;
	bool notEmptyMainTeam = false;
	bool notEmptyRedTeam = false;
	bool notEmptyGreenTeam = false;
	bool notEmptyBlueTeam = false;
	bool notEmptyYellowTeam = false;

	if (enableCommands)
	{
		for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
		{
			AIUnit *u = GetSelectedUnit(i);
			if (u)
			{
				notEmpty = true;
				if (u->GetSubgroup() == subgroup)
					notEmptyMySubgroup = true;
				if (u->GetSubgroup() != group->MainSubgroup())
					notEmptySubgroups = true;
			}
		}

		for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
		{
			if (!group->UnitWithID(i + 1)) continue;
			switch (GetTeam(i))
			{
			case TeamMain:
				notEmptyMainTeam = true; break;
			case TeamRed:
				notEmptyRedTeam = true; break;
			case TeamGreen:
				notEmptyGreenTeam = true; break;
			case TeamBlue:
				notEmptyBlueTeam = true; break;
			case TeamYellow:
				notEmptyYellowTeam = true; break;
			}
		}
	}

	// show / hide radio menu items
	//bool canHaveRadioMenu = false;
	_menuMain->ShowCommand(CMD_RADIO_ALPHA, false);
	_menuMain->ShowCommand(CMD_RADIO_BRAVO, false);
	_menuMain->ShowCommand(CMD_RADIO_CHARLIE, false);
	_menuMain->ShowCommand(CMD_RADIO_DELTA, false);
	_menuMain->ShowCommand(CMD_RADIO_ECHO, false);
	_menuMain->ShowCommand(CMD_RADIO_FOXTROT, false);
	_menuMain->ShowCommand(CMD_RADIO_GOLF, false);
	_menuMain->ShowCommand(CMD_RADIO_HOTEL, false);
	_menuMain->ShowCommand(CMD_RADIO_INDIA, false);
	_menuMain->ShowCommand(CMD_RADIO_JULIET, false);
	if (unit->IsGroupLeader())
	{
		for (int i=sensorsMap.Size()-1; i>=0; i--)
		{
			Vehicle *veh = sensorsMap[i];
			if (!veh) continue;
			Detector *det = dyn_cast<Detector>(veh);
			Assert(det);
			if (det->IsActive() && !det->IsRepeating()) continue;
			int cmd;
			switch (det->GetActivationBy())
			{
			case ASAAlpha:
				cmd = CMD_RADIO_ALPHA; goto showRadioCmd;
			case ASABravo:
				cmd = CMD_RADIO_BRAVO; goto showRadioCmd;
			case ASACharlie:
				cmd = CMD_RADIO_CHARLIE; goto showRadioCmd;
			case ASADelta:
				cmd = CMD_RADIO_DELTA; goto showRadioCmd;
			case ASAEcho:
				cmd = CMD_RADIO_ECHO; goto showRadioCmd;
			case ASAFoxtrot:
				cmd = CMD_RADIO_FOXTROT; goto showRadioCmd;
			case ASAGolf:
				cmd = CMD_RADIO_GOLF; goto showRadioCmd;
			case ASAHotel:
				cmd = CMD_RADIO_HOTEL; goto showRadioCmd;
			case ASAIndia:
				cmd = CMD_RADIO_INDIA; goto showRadioCmd;
			case ASAJuliet:
				cmd = CMD_RADIO_JULIET; goto showRadioCmd;
			showRadioCmd:
				{
					RString text = det->GetText();
					if (stricmp(text, "null") == 0) continue;
					_menuMain->ShowCommand(cmd, true);
					text = Localize(text);
					if (text.GetLength() > 0)
						_menuMain->SetText(cmd, text);
					else
						_menuMain->ResetText(cmd);
					//canHaveRadioMenu = true;
				}
			}
		}
	}

	Transport *transport = dyn_cast<Transport>(vehicle);
	bool vehicleCommander = transport && transport->CommanderUnit() == unit;
	bool commandsToGunner =
	(
		vehicleCommander && transport->GunnerUnit() && transport->GunnerUnit()!=unit
	);
	bool commandsToPilot =
	(
		vehicleCommander && transport->PilotUnit() && transport->PilotUnit()!=unit
	);

	// show / hide menu
	if (_menuType == MTMain && Glob.uiTime >= _lastMenuTime + menuHideTime)
		_menuType = MTNone;
	if( _menuType == MTMain )
	{ // no menu if not group leader
		if (_tmPos > 0)
		{
			if (_tmIn)
			{
				_tmPos -= 3.0 * (Glob.uiTime - _tmTime);
				if (_tmPos < 0) _tmPos = 0;
			}
			else
			{
				_tmIn = true;
				_tmOut = false;
			}
			_tmTime = Glob.uiTime;
		}
	}
	else
	{
		if (_tmPos < 1)
		{
			if (_tmOut)
			{
				_tmPos += 3.0 * (Glob.uiTime - _tmTime);
				if (_tmPos > 1) _tmPos = 1;
			}
			else
			{
				_tmIn = false;
				_tmOut = true;
			}
			_tmTime = Glob.uiTime;
		}
	}

	// show / hide tank direction
	CameraType type = GWorld->GetCameraType();
	const VehicleType *vehType = vehicle->GetType();
	if (vehType->HasCommander() && vehicle->IsTurret(type))
	{
		_tankPos = 0;
	}
	else
	{
		_tankPos = 1;
	}

	bool isCommander = unit == vehicle->CommanderUnit();

	bool enableRepeat = subgroup->HasCommand() || GWorld->GetMode()==GModeNetware;
	_menuMain->EnableCommand(CMD_REPLY_DONE, enableRepeat);
	_menuMain->EnableCommand(CMD_REPLY_FAIL, enableRepeat);
	_menuMain->EnableCommand(CMD_REPLY_COPY, enableRepeat);
	_menuMain->EnableCommand(CMD_REPLY_REPEAT, enableRepeat);
	_menuMain->EnableCommand(CMD_REPLY_FIREREADY, isCommander);
	_menuMain->EnableCommand(CMD_REPLY_FIRENOTREADY, isCommander);
	

	bool isLeader = unit->IsGroupLeader() && group->NUnits() > 1;
	if (!isLeader)
	{
		ClearSelectedUnits();
	}
	while ((!_menuCurrent->_visible || !_menuCurrent->_enable) && _menuCurrent->_parent)
		_menuCurrent = _menuCurrent->_parent;

	// check formation
	for (int i=0; i<=AI::NForms; i++)
		_menuMain->CheckCommand(CMD_FORM_COLUMN + i, false);
	_menuMain->CheckCommand(CMD_FORM_COLUMN + subgroup->GetFormation(), true);

	// some items are context dependent
/*		
	if (isLeader)
	{
		MenuItem *item = _menuMain->Find(CMD_REPLY_WHERE_ARE_YOU);
		if (item)
		{
			item->_text = LocalizeString(IDS_REPORT);
			item->_cmd = CMD_REPORT;
		}	
	}
	else
	{
		MenuItem *item = _menuMain->Find(CMD_REPORT);
		if (item)
		{
			item->_text = LocalizeString(IDS_WHERE_ARE_YOU);
			item->_cmd = CMD_REPLY_WHERE_ARE_YOU;
		}	
	}
*/		
	_menuMain->ShowCommand(CMD_REPORT, isLeader);
	_menuMain->ShowCommand(CMD_REPLY_WHERE_ARE_YOU, !isLeader);

	// enable / disable menu items, select mode, ground point
	static const int commandLeaderOnly[]=
	{
		CMD_FORM_COLUMN,CMD_FORM_STAGCOL,CMD_FORM_WEDGE,CMD_FORM_ECHLEFT,
		CMD_FORM_ECHRIGHT,CMD_FORM_VEE,CMD_FORM_LINE,

		CMD_TEAM_RED,CMD_TEAM_GREEN,CMD_TEAM_BLUE,CMD_TEAM_YELLOW,CMD_TEAM_MAIN,
		CMD_ASSIGN_RED,CMD_ASSIGN_GREEN,CMD_ASSIGN_BLUE,CMD_ASSIGN_YELLOW,CMD_ASSIGN_MAIN,

		CMD_SUPPORT_MEDIC, CMD_SUPPORT_REPAIR, CMD_SUPPORT_REARM, CMD_SUPPORT_REFUEL,
		CMD_SUPPORT_DONE,
	};
	static const int commandNotEmpty[]=
	{	
		// commands to unit
		CMD_MOVE_SUBMENU,CMD_WATCH_SUBMENU,

		CMD_GETIN,
		CMD_ADVANCE,CMD_STAY_BACK,CMD_FLANK_LEFT,CMD_FLANK_RIGHT,

		CMD_ACTION,CMD_HIDE,CMD_STOP,CMD_EXPECT,
		CMD_ENGAGE,CMD_FIRE,

		CMD_GETOUT,
		CMD_KEEP_FORM, CMD_LOOSE_FORM,
		CMD_STEALTH, CMD_COMBAT, CMD_AWARE, CMD_SAFE,
		CMD_POS_UP,CMD_POS_DOWN,CMD_POS_AUTO,CMD_REPORT,
		//CMD_WATCH_N,CMD_WATCH_NE,CMD_WATCH_E,CMD_WATCH_SE,
		//CMD_WATCH_S,CMD_WATCH_SW,CMD_WATCH_W,CMD_WATCH_NW,
		CMD_WATCH_AROUND,CMD_WATCH_AUTO,
		CMD_WATCH_TARGET,
		CMD_REPLY_KILLED
	};
	static const int commandGunner[]=
	{
		// commands to unit or vehicle
		CMD_HOLD_FIRE, CMD_OPEN_FIRE,
	};
	static const int commandDriver[]=
	{
		// commands to unit or vehicle
		CMD_NEXT_WAYPOINT,CMD_JOIN,
	};
	for (int i=0; i<sizeof(commandLeaderOnly)/sizeof(*commandLeaderOnly); i++)
	{
		_menuMain->ShowAndEnableCommand(commandLeaderOnly[i], isLeader, isLeader);
	}
	for (int i=0; i<sizeof(commandNotEmpty)/sizeof(*commandNotEmpty); i++)
	{
		_menuMain->ShowAndEnableCommand(commandNotEmpty[i], isLeader, notEmpty);
	}
	for (int i=0; i<sizeof(commandGunner)/sizeof(*commandGunner); i++)
	{
		_menuMain->ShowAndEnableCommand
		(
			commandGunner[i], isLeader || vehicleCommander,
			notEmpty || commandsToGunner			
		);
	}
	for (int i=0; i<sizeof(commandDriver)/sizeof(*commandDriver); i++)
	{
		_menuMain->ShowAndEnableCommand
		(
			commandDriver[i], isLeader || vehicleCommander,
			notEmpty || commandsToPilot			
		);
	}


	_menuMain->ShowAndEnableCommand(CMD_TEAM_MAIN, isLeader, notEmptyMainTeam);
	_menuMain->ShowAndEnableCommand(CMD_TEAM_RED, isLeader, notEmptyRedTeam);
	_menuMain->ShowAndEnableCommand(CMD_TEAM_GREEN, isLeader, notEmptyGreenTeam);
	_menuMain->ShowAndEnableCommand(CMD_TEAM_BLUE, isLeader, notEmptyBlueTeam);
	_menuMain->ShowAndEnableCommand(CMD_TEAM_YELLOW, isLeader, notEmptyYellowTeam);
	_menuMain->ShowCommand(CMD_ASSIGN_MAIN, isLeader);
	_menuMain->ShowCommand(CMD_ASSIGN_RED, isLeader);
	_menuMain->ShowCommand(CMD_ASSIGN_GREEN, isLeader);
	_menuMain->ShowCommand(CMD_ASSIGN_BLUE, isLeader);
	_menuMain->ShowCommand(CMD_ASSIGN_YELLOW, isLeader);

	// Menu
	while ((!_menuCurrent->_visible || !_menuCurrent->_enable) && _menuCurrent->_parent)
		_menuCurrent = _menuCurrent->_parent;
	if (_menuCurrent)
	{
		for (int i=0; i<_menuCurrent->_items.Size(); i++)
		{
			MenuItem* item = _menuCurrent->_items[i];
			if (item->_cmd == CMD_SEPARATOR) continue;
			if (!item->_visible || !item->_enable) continue;
			if (!GInput.GetKeyToDo(item->_key)) continue;

			if (item->_cmd == CMD_ACTION_TARGET)
			{
				// action
				IssueAction(group, *item);
				_visibleListTemp.Clear();
				MenuItem *actions = _menuMain->Find(CMD_ACTION);
				if (actions)
				{
					actions->_submenu = NULL;
					_menuMain->RescanMinMax();
				}
			}
			else if (item->_cmd >= CMD_WATCH_TARGET)
			{
				if (notEmpty)
				{
					IssueWatchTarget(group, item->_cmd - CMD_WATCH_TARGET);
					_visibleListTemp.Clear();
				}
				else
				{
					if (vehicleCommander)
					{
						Target *target = _visibleListTemp[item->_cmd - CMD_WATCH_TARGET];
						if (target)
						{
							if (transport->IsLocal())
								transport->SendTarget(target);
							else
							{
								RadioMessageVTarget msg(transport, target);
								GetNetworkManager().SendRadioMessage(&msg);
							}
							_lockTarget = target;
							_timeSendTarget = UITIME_MAX;
							_lockAimValidUntil = Glob.uiTime - 60;
						}
					}
				}
				_visibleListTemp.Clear();
				/*
				IssueWatchTarget(group, item->_cmd - CMD_WATCH_TARGET);
				_visibleListTemp.Clear();
				*/
			}
			else if (item->_cmd >= CMD_GETIN_TARGET)
			{
				// get in
				IssueGetIn(group, item->_cmd - CMD_GETIN_TARGET);
				_visibleListTemp.Clear();
			}
			else if (item->_cmd >= CMD_MOVE_FIRST)
			{
				// move
				if (!IsEmptySelectedUnits())
					IssueMove(group, item->_cmd - CMD_MOVE_FIRST);
			}
			else switch (item->_cmd)
			{
			case CMD_HIDE_MENU:
				if (_menuType == MTNone)
				{
					_menuType = MTMain;
					ShowMenu();
				}
				else
					_menuType = MTNone;
				_menuCurrent = _menuMain;
				goto MenuOK;
			case CMD_BACK:
				if (_menuType == MTNone)
				{
					_menuType = MTMain;
					_menuCurrent = _menuMain;
				}
				else
				{
					if (_menuCurrent->_parent)
						_menuCurrent = _menuCurrent->_parent;
				}
				ShowMenu();
				goto MenuOK;
			case CMD_GETIN:
				BackupTargets();
				if (transport)
				{
					for (int i=0; i<_visibleListTemp.Size(); i++)
					{
						Target *target = _visibleListTemp[i];
						Assert(target);
						if (target->idExact == transport)
						{
							_visibleListTemp.Delete(i);
							_visibleListTemp.Insert(0, target);
							break;
						}
					}
				}
				{
					int n = _visibleListTemp.Size();
					if (n > 0)
					{
						Menu *submenu = new Menu();
						submenu->_text = LocalizeString(IDS_GETIN);
						submenu->_parent = _menuCurrent;
						submenu->RescanMinMax();
						item->_submenu = submenu;
						_menuCurrent = submenu;
						if (_menuType == MTNone) _menuType = MTMain;
						ShowMenu();
						int nItem = 1;


						submenu->AddItem
						(
							new MenuItem
							(
								LocalizeString(IDS_GETOUT),
								DIK_1, "1", CMD_GETOUT
							)
						);

						for (int i=0; i<n; i++)
						{
							Target *target = _visibleListTemp[i];
							if (!target) continue;
							if (!target->IsKnown()) continue;
							if (target->vanished) continue;
							if (target->destroyed) continue;
							Object *obj = target->idExact;
							Transport *veh = dyn_cast<Transport>(obj);
							if (!veh) continue;
							if (!center->IsFriendly(target->side)) continue;
							bool asDriver = veh->QCanIGetIn();
							bool asCommander = veh->QCanIGetInCommander();
							bool asGunner = veh->QCanIGetInGunner();
							bool asCargo = veh->QCanIGetInCargo();
							if (!asDriver && !asCommander && !asGunner && !asCargo) continue;
							if (nItem == 9)
							{
								Menu *newmenu = new Menu();
								newmenu->_text = LocalizeString(IDS_GETIN);
								newmenu->_parent = submenu;
								submenu->AddItem
								(
									new MenuItem
									(
										LocalizeString(IDS_MORE_MENU),
										DIK_0, "0", newmenu, CMD_NOTHING
									)
								);
								submenu->AddItem
								(
									new MenuItem
									(
										LocalizeString(IDS_CANCEL_MENU),
										DIK_BACK, LocalizeString(IDS_MENU_BACKSPACE), CMD_BACK
									)
								);
								submenu = newmenu;
								nItem = 0;
							}
							char key[2] = "1"; key[0] += nItem;
							char text[256];
							RString name = target->type->GetDisplayName();
					
							AIUnit *u = veh ? veh->CommanderUnit() : NULL;
							AIGroup *g = u ? u->GetGroup() : NULL;
							if (g == group)
							{
								sprintf
								(
									text, LocalizeString(IDS_TARGET_MENU_GROUP),
									(const char *)u->GetPerson()->GetInfo()._name,
									(const char *)name, u->ID()
								);
							}
							else
							{
								Vector3 pos = GWorld->CameraOn()->GetInvTransform() * target->position;
								int azimut = toInt(atan2(pos.X(), pos.Z()) * (6 / H_PI));
								if (azimut <= 0) azimut += 12;
								sprintf
								(
									text, LocalizeString(IDS_TARGET_MENU),
									(const char *)name, azimut
								);
							}
							Menu *newmenu = new Menu();
							submenu->AddItem
							(
								new MenuItem
								(
									text,
									DIK_1 + nItem,
									key,
									newmenu, CMD_NOTHING
								)
							);
							newmenu->_text = LocalizeString(IDS_GETIN_POS);
							newmenu->_parent = submenu;
							int cmd = CMD_GETIN_TARGET + N_GETIN_POS * i;
							newmenu->AddItem
							(
								new MenuItem
								(
									LocalizeString(IDS_GETIN_POS_ANY),
									DIK_1, "1", cmd
								)
							);
							if (asDriver)
							{
								if (target->type->IsKindOf(GWorld->Preloaded(VTypeAir)))
									newmenu->AddItem
									(
										new MenuItem
										(
											LocalizeString(IDS_GETIN_POS_PILOT),
											DIK_2, "2", cmd + 1
										)
									);
								else
									newmenu->AddItem
									(
										new MenuItem
										(
											LocalizeString(IDS_GETIN_POS_DRIVER),
											DIK_2, "2", cmd + 1
										)
									);
							}
							if (asCommander)
								newmenu->AddItem
								(
									new MenuItem
									(
										LocalizeString(IDS_GETIN_POS_COMM),
										DIK_3, "3", cmd + 2
									)
								);
							if (asGunner)
								newmenu->AddItem
								(
									new MenuItem
									(
										LocalizeString(IDS_GETIN_POS_GUNN),
										DIK_4, "4", cmd + 3
									)
								);
							if (asCargo)
								newmenu->AddItem
								(
									new MenuItem
									(
										LocalizeString(IDS_GETIN_POS_CARGO),
										DIK_5, "5", cmd + 4
									)
								);
							newmenu->AddItem
							(
								new MenuItem
								(
									LocalizeString(IDS_CANCEL_MENU),
									DIK_BACK, LocalizeString(IDS_MENU_BACKSPACE), CMD_BACK
								)
							);
							nItem++;
						}
						submenu->AddItem
						(
							new MenuItem
							(
								LocalizeString(IDS_CANCEL_MENU),
								DIK_BACK, LocalizeString(IDS_MENU_BACKSPACE), CMD_BACK
							)
						);
					}
				}
				goto MenuOK;
				break;
			case CMD_GETOUT:
				IssueCommand(vehicle, Command::GetOut);
				break;
			case CMD_HIDE:
				IssueCommand(vehicle, Command::Hide);
				break;
			case CMD_STOP:
				IssueCommand(vehicle, Command::Stop);
				break;
			case CMD_EXPECT:
				IssueCommand(vehicle, Command::Expect);
				break;
			case CMD_WATCH_N: case CMD_WATCH_NE:
			case CMD_WATCH_E: case CMD_WATCH_SE:
			case CMD_WATCH_S: case CMD_WATCH_SW:
			case CMD_WATCH_W: case CMD_WATCH_NW:
				IssueWatch(group, item->_cmd-CMD_WATCH_N);
				break;
			case CMD_WATCH_AROUND:
				IssueWatchAround(group);
				break;
			case CMD_WATCH_AUTO:
				IssueWatchAuto(group);
				break;
			case CMD_ENGAGE:
				IssueEngage(group);
				break;
			case CMD_FIRE:
				IssueFire(group);
				break;
			case CMD_JOIN:
				IssueCommand(vehicle, Command::Join);
				break;
			case CMD_WATCH:
				BackupTargets();
				{
					int n = _visibleListTemp.Size();
					if (n > 0)
					{
						Menu *submenu = new Menu();
						submenu->_text = LocalizeString(IDS_WATCH);
						submenu->_parent = _menuCurrent;
						item->_submenu = submenu;
						submenu->RescanMinMax();
						_menuCurrent = submenu;
						if (_menuType == MTNone) _menuType = MTMain;
						ShowMenu();
						CreateAttackList(group, submenu, CMD_WATCH_TARGET);
					}
				}
				goto MenuOK;
			/*
			case CMD_ATTACK:
				BackupTargets();
				{
					int n = _visibleListTemp.Size();
					if (n > 0)
					{
						Menu *submenu = new Menu();
						submenu->_text = LocalizeString(IDS_ATTACK);
						submenu->_parent = _menuCurrent;
						item->_submenu = submenu;
						submenu->RescanMinMax();
						_menuCurrent = submenu;
						if (_menuType == MTNone) _menuType = MTMain;
						CreateAttackList(group, submenu, CMD_ATTACK_TARGET);
					}
				}
				goto MenuOK;
			*/
			case CMD_ACTION:
				RefreshActionsMenu();
				goto MenuOK;
			case CMD_FORM_COLUMN:
			case CMD_FORM_STAGCOL:
			case CMD_FORM_WEDGE:
			case CMD_FORM_ECHLEFT:
			case CMD_FORM_ECHRIGHT:
			case CMD_FORM_VEE:
			case CMD_FORM_LINE:
				group->SendFormation
				(
					(AI::Formation)(AI::FormColumn + item->_cmd - CMD_FORM_COLUMN),
					subgroup
				);							
				ClearSelectedUnits();
				break;
			case CMD_STEALTH:
				group->SendBehaviour(CMStealth, ListSelectedUnits());
				ClearSelectedUnits();
				break;
			case CMD_COMBAT:
				group->SendBehaviour(CMCombat, ListSelectedUnits());
				ClearSelectedUnits();
				break;
			case CMD_AWARE:
				group->SendBehaviour(CMAware, ListSelectedUnits());
				ClearSelectedUnits();
				break;
			case CMD_SAFE:
				group->SendBehaviour(CMSafe, ListSelectedUnits());
				ClearSelectedUnits();
				break;
			case CMD_KEEP_FORM:
				group->SendLooseFormation(false, ListSelectedUnits());
				ClearSelectedUnits();
				break;
			case CMD_LOOSE_FORM:
				group->SendLooseFormation(true, ListSelectedUnits());
				ClearSelectedUnits();
				break;
			case CMD_HOLD_FIRE:
				group->SendOpenFire(OFSHoldFire, ListSelectedUnits());
				ClearSelectedUnits();
				break;
			case CMD_OPEN_FIRE:
				group->SendOpenFire(OFSOpenFire, ListSelectedUnits());
				ClearSelectedUnits();
				break;
			case CMD_ADVANCE:
				SetFormationPos(unit, AI::PosAdvance);
				ClearSelectedUnits();
				break;
			case CMD_STAY_BACK:
				SetFormationPos(unit, AI::PosStayBack);
				ClearSelectedUnits();
				break;
			case CMD_FLANK_LEFT:
				SetFormationPos(unit, AI::PosFlankLeft);
				ClearSelectedUnits();
				break;
			case CMD_FLANK_RIGHT:
				SetFormationPos(unit, AI::PosFlankRight);
				ClearSelectedUnits();
				break;
			case CMD_POS_UP:   SetUnitPosition(unit, UPUp  ); break;
			case CMD_POS_DOWN: SetUnitPosition(unit, UPDown); break;
			case CMD_POS_AUTO: SetUnitPosition(unit, UPAuto); break;
			case CMD_REPORT:
				group->SendReportStatus(ListSelectedUnits());
				ClearSelectedUnits();
				break;
			case CMD_TEAM_MAIN:
				ClearSelectedUnits();
				for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
				{
					if (GetTeam(i) != TeamMain) continue;
					AIUnit *u = group->UnitWithID(i + 1);
					if (!u || u == unit) continue;
					SetSelectedUnit(i, u);
				}
				goto MainMenu;
			case CMD_TEAM_RED:
				ClearSelectedUnits();
				for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
				{
					if (GetTeam(i) != TeamRed) continue;
					AIUnit *u = group->UnitWithID(i + 1);
					if (!u || u == unit) continue;
					SetSelectedUnit(i, u);
				}
				goto MainMenu;
			case CMD_TEAM_GREEN:
				ClearSelectedUnits();
				for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
				{
					if (GetTeam(i) != TeamGreen) continue;
					AIUnit *u = group->UnitWithID(i + 1);
					if (!u || u == unit) continue;
					SetSelectedUnit(i, u);
				}
				goto MainMenu;
			case CMD_TEAM_BLUE:
				ClearSelectedUnits();
				for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
				{
					if (GetTeam(i) != TeamBlue) continue;
					AIUnit *u = group->UnitWithID(i + 1);
					if (!u || u == unit) continue;
					SetSelectedUnit(i, u);
				}
				goto MainMenu;
			case CMD_TEAM_YELLOW:
				ClearSelectedUnits();
				for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
				{
					if (GetTeam(i) != TeamYellow) continue;
					AIUnit *u = group->UnitWithID(i + 1);
					if (!u || u == unit) continue;
					SetSelectedUnit(i, u);
				}
				goto MainMenu;
			case CMD_ASSIGN_MAIN:
				group->GetRadio().Transmit
				(
					new RadioMessageTeam(group, ListSelectedUnits(), TeamMain),
					center->GetLanguage()
				);
				ClearSelectedUnits();
				break;
			case CMD_ASSIGN_RED:
				group->GetRadio().Transmit
				(
					new RadioMessageTeam(group, ListSelectedUnits(), TeamRed),
					center->GetLanguage()
				);
				ClearSelectedUnits();
				break;
			case CMD_ASSIGN_GREEN:
				group->GetRadio().Transmit
				(
					new RadioMessageTeam(group, ListSelectedUnits(), TeamGreen),
					center->GetLanguage()
				);
				ClearSelectedUnits();
				break;
			case CMD_ASSIGN_BLUE:
				group->GetRadio().Transmit
				(
					new RadioMessageTeam(group, ListSelectedUnits(), TeamBlue),
					center->GetLanguage()
				);
				ClearSelectedUnits();
				break;
			case CMD_ASSIGN_YELLOW:
				group->GetRadio().Transmit
				(
					new RadioMessageTeam(group, ListSelectedUnits(), TeamYellow),
					center->GetLanguage()
				);
				ClearSelectedUnits();
				break;
			case CMD_RADIO_ALPHA:
				ActivateSensor(ASAAlpha);
				break;
			case CMD_RADIO_BRAVO:
				ActivateSensor(ASABravo);
				break;
			case CMD_RADIO_CHARLIE:
				ActivateSensor(ASACharlie);
				break;
			case CMD_RADIO_DELTA:
				ActivateSensor(ASADelta);
				break;
			case CMD_RADIO_ECHO:
				ActivateSensor(ASAEcho);
				break;
			case CMD_RADIO_FOXTROT:
				ActivateSensor(ASAFoxtrot);
				break;
			case CMD_RADIO_GOLF:
				ActivateSensor(ASAGolf);
				break;
			case CMD_RADIO_HOTEL:
				ActivateSensor(ASAHotel);
				break;
			case CMD_RADIO_INDIA:
				ActivateSensor(ASAIndia);
				break;
			case CMD_RADIO_JULIET:
				ActivateSensor(ASAJuliet);
				break;
			case CMD_NEXT_WAYPOINT:
				if (notEmpty)
				{
					// FIX - implementation of command "NEXT WAYPOINT" for selected units
					int &index = group->GetCurrent()->_fsm->Var(0);
					if (index < group->NWaypoints())
					{
						const ArcadeWaypointInfo &wInfo = group->GetWaypoint(index);
						Command cmd;
						cmd._message = Command::Move;
						cmd._context = Command::CtxUI;
						cmd._destination = wInfo.position;
						group->SendCommand(cmd, ListSelectedUnits());
						ClearSelectedUnits();
					}
				}
				else if (vehicleCommander)
				{
					int &index = group->GetCurrent()->_fsm->Var(0);
					if (index < group->NWaypoints())
					{
						const ArcadeWaypointInfo &wInfo = group->GetWaypoint(index);
						if (transport->IsLocal())
							transport->SendMove(wInfo.position);
						else
						{
							RadioMessageVMove msg(transport, wInfo.position);
							GetNetworkManager().SendRadioMessage(&msg);
						}
					}
				}
				break;
			case CMD_REPLY_FIREREADY:
				SendFireReady(unit,true);
				break;
			case CMD_REPLY_FIRENOTREADY:
				SendFireReady(unit,false);
				break;
			case CMD_REPLY_DONE:
				SendAnswer(unit,AI::CommandCompleted);
				break;
			case CMD_REPLY_FAIL:
				SendAnswer(unit,AI::CommandFailed);
				break;
			case CMD_REPLY_COPY:
				SendConfirm(unit);
				break;
			case CMD_REPLY_KILLED:
				SendKilled(unit,ListSelectedUnits());
				ClearSelectedUnits();
				break;
			case CMD_REPLY_HIT:
				if (unit->GetVehicleIn())
				{
					SendResourceState(unit,AI::DammageCritical);
				}
				else
				{
					SendResourceState(unit,AI::HealthCritical);
				}
				break;
			case CMD_REPLY_INJURED:
				SendResourceState(unit,AI::HealthCritical);
				if (unit->IsGroupLeader())
				{
					const AITargetInfo *info = group->FindHealPosition(AIUnit::RSCritical, unit);
					if (info)
					{
						VehicleSupply *veh = dyn_cast<VehicleSupply, EntityAI>(info->_idExact);
						if (veh)
						{
							Command cmd;
							cmd._message = Command::Heal;
							cmd._destination = veh->Position();
							cmd._target = veh;
							cmd._time = Glob.time + COMMAND_TIMEOUT;
							group->SendAutoCommandToUnit(cmd, unit, true);
						}
					}
				}
				break;
			case CMD_REPLY_AMMO_LOW:
				SendResourceState(unit,AI::AmmoLow);
				break;
			case CMD_REPLY_FUEL_LOW:
				SendResourceState(unit,AI::FuelLow);
				break;
			case CMD_REPLY_REPEAT:
				group->GetRadio().Transmit
				(
					new RadioMessageRepeatCommand(unit, group),
					center->GetLanguage()
				);
				break;
			case CMD_REPLY_WHERE_ARE_YOU:
				group->GetRadio().Transmit
				(
					new RadioMessageWhereAreYou(unit, group),
					center->GetLanguage()
				);
				break;
			case CMD_REPLY_ENGAGING:
				{
					Command cmd;
					cmd._message = Command::Attack;
					group->GetRadio().Transmit
					(
						new RadioMessageNotifyCommand(unit, group, cmd),
						center->GetLanguage()
					);
				}
				break;
			case CMD_REPLY_UNDER_FIRE:
				group->GetRadio().Transmit
				(
					new RadioMessageUnderFire(unit, group),
					center->GetLanguage()
				);
				break;
			case CMD_REPLY_ONE_LESS:
				SendObjectDestroyed(unit,group);
				break;
			case CMD_SUPPORT_MEDIC:
				center->GetRadio().Transmit
				(
					new RadioMessageSupportAsk(group, ATHeal),
					center->GetLanguage()
				);
				break;
			case CMD_SUPPORT_REPAIR:
				center->GetRadio().Transmit
				(
					new RadioMessageSupportAsk(group, ATRepair),
					center->GetLanguage()
				);
				break;
			case CMD_SUPPORT_REARM:
				center->GetRadio().Transmit
				(
					new RadioMessageSupportAsk(group, ATRearm),
					center->GetLanguage()
				);
				break;
			case CMD_SUPPORT_REFUEL:
				center->GetRadio().Transmit
				(
					new RadioMessageSupportAsk(group, ATRefuel),
					center->GetLanguage()
				);
				break;
			case CMD_SUPPORT_DONE:
				center->GetRadio().Transmit
				(
					new RadioMessageSupportDone(group),
					center->GetLanguage()
				);
				break;
			case CMD_RADIO_CUSTOM_1:
			case CMD_RADIO_CUSTOM_2:
			case CMD_RADIO_CUSTOM_3:
			case CMD_RADIO_CUSTOM_4:
			case CMD_RADIO_CUSTOM_5:
			case CMD_RADIO_CUSTOM_6:
			case CMD_RADIO_CUSTOM_7:
			case CMD_RADIO_CUSTOM_8:
			case CMD_RADIO_CUSTOM_9:
			case CMD_RADIO_CUSTOM_0:
				{
					RString name = RString("#") + _customRadio[item->_cmd - CMD_RADIO_CUSTOM_1];
					ChatChannel channel = CCGroup;
					if (GWorld->GetMode() == GModeNetware) channel = ActualChatChannel();
					
					RadioChannel *FindChannel(AIUnit *unit, int channel);
					RadioChannel *radio = FindChannel(unit, channel);

					radio->Say(name, unit, "", "", 2.0);
					SendRadioChatWave
					(
						channel, name, unit, ""
					);
				}
				break;
			default:
				if (item->_submenu)
				{
					if (_menuType == MTNone)
					{
						Menu *top = _menuCurrent;
						while (top->_parent) top = top->_parent;
						if (top == _menuMain)
						{
							_menuType = MTMain;
						}
						else
						{
							Fail("Unknown root menu");
						}
					}
					_menuCurrent = item->_submenu;
					ShowMenu();
					goto MenuOK;
				}
				else
				{
					LogF("Bad command id %d",item->_cmd);
					ClearSelectedUnits();
				}
				break;
			} // switch( command )
			_menuType = MTNone;
MainMenu:
			_menuCurrent = _menuMain;
			
MenuOK:
			break;
		} // for( menu items )
	} // if( _menuCurrent )
}

static bool IsManual( EntityAI *veh, AIUnit *unit )
{
	if (!unit->IsPlayer()) return false;

	if( !GWorld->PlayerManual() ) return false;
	if( GWorld->GetPlayerSuspended() ) return false;
	return true;
}

void InGameUI::RevealTarget( Target *tgt, float spot )
{
	AIUnit *unit = GWorld->FocusOn();
	if (!unit) return;
	EntityAI *vehicle = unit->GetVehicle();
	if (!vehicle) return;
	AIGroup *grp = unit->GetGroup();
	if (!grp) return;

	if
	(
		!tgt->isKnown ||
		tgt->delay>Glob.time+1 ||
		tgt->FadingSpotability()<spot
	)
	{
		// report only targets that are not known

		tgt->isKnown=true;
		if (tgt->delay>Glob.time+1) tgt->delay=Glob.time+1;
		tgt->delaySensor=Glob.time-5;
		tgt->idSensor = unit->GetPerson();
		tgt->lastSeen = Glob.time;
		// use current properties
		EntityAI *exact=tgt->idExact;
		tgt->position=exact->AimingPosition();
		tgt->speed=exact->Speed();
		tgt->posError=VZero;
		if( tgt->FadingSpotability()<spot )
		{
			tgt->spotability=spot;
			tgt->spotabilityTime=Glob.time;
		}
	}
	// if it is dead body of my group unit, report it
	EntityAI *obj = tgt->idExact;
	if (obj)
	{
		// TODO: report all our units that are in the vehicle
		AIUnit *tgtUnit = obj->CommanderUnit();
		if
		(
			tgtUnit && tgtUnit->GetLifeState()!=AIUnit::LSAlive
			&& tgtUnit->GetGroup()==grp
		)
		{
			grp->SendUnitDown(unit,tgtUnit);
		}
		else if (obj->IsDammageDestroyed())
		{
			// check taregt side
			if (grp->GetCenter()->IsEnemy(obj->Vehicle::GetTargetSide()))
			{
				// if it is dead enemy, you may report it
				// but only when I killed him
				if
				(
					(tgt->idKiller == vehicle || tgt->idKiller == unit->GetPerson()) &&
					tgt->timeReported<=TIME_MIN
				)
				{
					if (grp->NUnits()>1)
					{
						// send radio message
						grp->SendObjectDestroyed(unit, tgt->type);
					}
					// mark as reported
					tgt->timeReported = Glob.time;
					tgt->posReported = tgt->position;
				}
			}
		}
	}
}

Target *InGameUI::CheckCursorTarget
(
	Vector3 &itPos, Vector3Par cursorDir,
	const Camera &camera, CameraType cam, bool knownOnly
)
{
	AIUnit *unit = GWorld->FocusOn();
	AISubgroup *subgroup = unit->GetSubgroup();
	AIGroup *group = subgroup->GetGroup();
	EntityAI *vehicle = unit->GetVehicle();

	float maxVisDist=floatMax(vehicle->GetType()->GetIRScanRange(),TACTICAL_VISIBILITY);

	CollisionBuffer retVal;
	GLandscape->ObjectCollision
	(
		retVal,vehicle,NULL,
		camera.Position(),camera.Position()+cursorDir*maxVisDist,0,ObjIntersectView
	);
	// select first intersected object
	int minI = -1;
	float minT = 1e10;
	for (int i=0; i<retVal.Size(); i++)
	{
		const CollisionInfo &info = retVal[i];
		if (!info.object) continue;
		if (info.under<minT)
		{
			minT = info.under;
			minI = i;
		}
	}
	Target *iTarget = NULL;

	itPos = camera.Position();

	#if 1
	if (minI>=0)
	{	
		// check intersection with land
		const CollisionInfo &info = retVal[minI];
		EntityAI *ai = dyn_cast<EntityAI,Object>(info.object);
		if (ai)
		{
			Vector3 iPos = info.object->WorldTransform().FastTransform(info.pos);
			Vector3 iDir = iPos-camera.Position();
			Vector3 iDirN = iDir.Normalized();
			float iDirSize = iDir*iDirN;
			Vector3 lPos;
			float isect = GLandscape->IntersectWithGroundOrSea
			(
				&lPos,camera.Position(),iDirN,0,iDirSize*1.1
			);
			// we know what is 

			if (isect>iDirSize)
			{
				// no ground obstacle
				// check if we know the target
				if (knownOnly)
				{
					iTarget = group->FindTarget(ai);
				}
				else
				{
					iTarget = group->FindTargetAll(ai);
				}
				if (iTarget) itPos = iPos;
				//GScene->DrawCollisionStar(iPos,1,PackedColor(Color(0,1,0)));
			}
			else
			{
				// ground occluding
				//GScene->DrawCollisionStar(lPos,0.5,PackedColor(Color(1,0,0)));
			}
		}
	}

	// if we are aiming at something directly, return it
	if (iTarget) return iTarget;
	#endif

	// if not, we should use cursor range look-up

	float distNearest = 1e10;
	const TargetList &visibleList=*VisibleList();
	int	i,n = visibleList.Size(); 
	for (i=0; i<n; i++)
	{
		Target* tar = visibleList[i];

		TargetType *veh = tar->idExact;
		if( !veh ) continue; // invisible

		if
		(
			tar->vanished
#if _ENABLE_CHEATS
			&& !_showAll
#endif
		) continue; // invisible
	
		if ( vehicle==veh )
		{
			if (_mode != UIStrategy)
				continue;
			if (GWorld->GetCameraType() < CamExternal)
				continue;
		}

		// static (mostly large) objects do not use range look-up
		if (veh->GetType()->IsKindOf(GWorld->Preloaded(VTypeStatic)))
		{
			continue;
		}

		if (knownOnly)
		{
			if
			(
				!tar->IsKnownBy(unit)
	#if _ENABLE_CHEATS
				&& !_showAll
	#endif
			) continue;
		}
		else
		{
			// visible only
			// check distance to target
			float irRange = vehicle->GetType()->GetIRScanRange();
			if (!veh || !veh->GetType()->GetIRTarget()) irRange = 0;
			float maxDistance=floatMax(irRange,TACTICAL_VISIBILITY);

			float dist2 = veh->Position().Distance2(vehicle->Position());
			if (dist2>Square(maxDistance)) continue;
			// check LOS to target
			float vis = GLandscape->Visible(vehicle,veh);
			if (vis<0.25) continue;

		}
		
		Vector3Val pos =
		(
#if _ENABLE_CHEATS
			(_showAll || !knownOnly)
#else
			!knownOnly
#endif
			?
			tar->idExact->AimingPosition() :
			tar->AimingPosition()
		);
		Vector3Val dir = pos-camera.Position();

		float bounding = veh->GetShape()->GeometrySphere()*0.5;

		// check nearest point on line camPos,cursorDir to target aiming position
		Vector3 cDir = cursorDir.Normalized();
		float nearestT = cDir*dir;
		saturateMax(nearestT,0);
		Vector3 nearP = camera.Position()+cDir*nearestT;


		float distToObj = nearP.Distance(pos);
		// check if object is within cursor range
		float maxDist = bounding + 0.03 * camera.Left()*nearestT;
		if (distToObj<maxDist)
		{
			//GScene->DrawCollisionStar(nearP);
			if (distToObj<distNearest)
			{
				distNearest = distToObj;
				iTarget = tar;
				itPos = nearP;
			}
		}

		/*
		GScene->DrawDiagModel
		(
			pos,GScene->Preloaded(SphereModel),
			bounding,PackedColor(Color(1,1,1,0.5))
		);
		*/

	}
	return iTarget;
}

void InGameUI::SimulateHUDNonAI
(
	const Camera &camera, Entity *vehicle, CameraType cam, float deltaT
)
{
	if( GInput.MouseCursorActive() ) _worldCursorTime=Glob.uiTime;

	_modeAuto = UIFire;
	_mode = UIFire;

	CameraHolder *camHolder=dyn_cast<CameraHolder,Object>(vehicle);
	if( camHolder && camHolder->GetManual())
	{
			// driver's vehicle is always local
		Vector3 weaponDir = GetCursorDirection();
		camHolder->AimDriver(weaponDir);
	}
}

/*!
\patch_internal 1.24 Date 09/24/2001 by Jirka
- Improved: Do not send AskForAimWeapon too frequently
\patch 1.32 Date 11/26/2001 by Jirka
- Fixed: Disable action menu in cutscenes
*/
void InGameUI::SimulateHUD
(
	const Camera &camera, EntityAI *vehicle, CameraType cam, float deltaT
)
{
	if (GInput.GetActionToDo(UAHelp))
	{
		ShowLastHint();
	}

	AIUnit *unit = GWorld->FocusOn();
	if (!unit) return;
	AISubgroup *subgroup = unit->GetSubgroup();
	if (!subgroup) return;
	AIGroup *group = subgroup->GetGroup();
	if (!group) return;
	vehicle = unit->GetVehicle();
	Assert(vehicle);
	if (!vehicle)	return;

	ProcessMenu(camera, vehicle);

	if (GWorld->HasMap()) return;

	if (!GWorld->Chat() && !GWorld->GetPlayerSuspended())
		ProcessActions(unit);

	// focus may changed !!!
	unit = GWorld->FocusOn();
	if (!unit) return;
	subgroup = unit->GetSubgroup();
	if (!subgroup) return;
	group = subgroup->GetGroup();
	if (!group) return;
	vehicle = unit->GetVehicle();
	Assert(vehicle);
	if (!vehicle)	return;

	bool isCommander = unit == vehicle->CommanderUnit();
	bool isPilot = unit == vehicle->PilotUnit();
	bool isGunner = unit == vehicle->GunnerUnit() && vehicle->IsGunner(cam);
	bool isObserver = unit == vehicle->ObserverUnit() && vehicle->IsGunner(cam);

	bool isGunnerFire = unit == vehicle->GunnerUnit();

	Transport *transport = unit->GetVehicleIn();
	bool vehicleCommander =(transport && transport->Commander() == unit->GetPerson());
	
	//bool commandsToGunner = transport->GunnerUnit()!=unit;
	//bool commandsToPilot = transport->PilotUnit()!=unit;

	if( GInput.MouseCursorActive() ) _worldCursorTime=Glob.uiTime;

	int nUnits = group->NUnits();
#if _ENABLE_CHEATS
	if (CHECK_DIAG(DECombat))
		SwitchToStrategy(vehicle);
	else
#endif
	if (vehicleCommander)
		SwitchToStrategy(vehicle);
	else if (!(unit->IsGroupLeader() && nUnits > 1))
		SwitchToFire(vehicle);
	else if (!vehicle->IsCommander(cam))
		SwitchToFire(vehicle);
	else if (!vehicle->IsGunner(cam))
		SwitchToStrategy(vehicle);
	else if (IsEmptySelectedUnits())
		SwitchToFire(vehicle);
	else
		SwitchToStrategy(vehicle);

#if _ENABLE_CHEATS
	if( GInput.GetCheat1ToDo(DIK_A))
	{
		_showAll=!_showAll;
		GlobalShowMessage(500,"ShowAll %s",_showAll ? "On":"Off");
	}

	// show / hide status displays
	if (GInput.GetCheat1ToDo(DIK_SEMICOLON))
	{
		bool show = !_showUnitInfo;
		ShowAll(show);
		GChatList.Enable(show);
	}

	if (GInput.GetCheat1ToDo(DIK_APOSTROPHE))
	{
		ShowCursors(!_showCursors);
	}
#endif

	if (IsManual(vehicle,unit) && (isCommander || isGunnerFire))
	{
		// only commander can toggle weapons
		int weapon = vehicle->SelectedWeapon();
		bool weaponChanged = 0;
		if (GInput.GetActionToDo(UAToggleWeapons))
		{
			weapon = vehicle->NextWeapon(weapon);
			weaponChanged = true;
		}
		else if (weapon >= 0 && weapon < vehicle->NMagazineSlots())
		{
			const MagazineSlot &slot = vehicle->GetMagazineSlot(weapon);
			if
			(
				!slot._muzzle->_showEmpty && vehicle->EmptySlot(slot)
			)
			{
				weapon = vehicle->NextWeapon(weapon);
				weaponChanged = true;
			}
		}

		if (weaponChanged)
		{
			weapon = ValidateWeapon(vehicle, weapon);
			SelectWeapon(unit, weapon);
		}
	}
	/*
	{
		int weapon = vehicle->SelectedWeapon();
		if (weapon < 0)
		{
			weapon = vehicle->FirstWeapon();
			// select weapon only if it is primary weapon
			SelectWeapon(unit, weapon);
		}
	}
	*/
	
	// list of visible objects
	const TargetList &visibleList=*VisibleList();
	int	i,n = visibleList.Size(); 

	// find visible target with cursor on
	_target=NULL;
	_housePos = -1;
	Vector3 housePosition = VZero;

	Vector3 cursorDir = GetCursorDirection();

	vehicle->LimitCursor(cam,cursorDir);

	if (GInput.GetActionToDo(UARevealTarget) && !GWorld->HasMap())
	{ // no reset - some processing needs to be done yet
		Vector3 itPos;
		Target *manualTarget= CheckCursorTarget
		(
			itPos,cursorDir,camera,cam,false
		);

		if( manualTarget )
		{
			RevealTarget(manualTarget,0.3);

			_target = manualTarget;

			// if some units are selected, xmit Target

			if (!IsEmptySelectedUnits())
			{
				if (group->GetCenter()->IsEnemy(manualTarget->side))
				{
					group->SendTarget(manualTarget,false,false,ListSelectedUnits());
					ClearSelectedUnits();
				}
			}

		}
	}

	if (!_target)
	{
		// check which object is showed by cursor
		Vector3 itPos;
		Target *iTarget = CheckCursorTarget
		(
			itPos,cursorDir,camera,cam,true
		);
		if (iTarget)
		{
			_target = iTarget;
			// check if we should look-up in-house positions

			_housePos = -1;

			// find nearest in-house position to the point we are aiming at
			EntityAI *veh = iTarget->idExact;
			const IPaths *house = veh->GetIPaths();
			float minDist2 = 1e10;
			if (house && house->NPos() > 0)
			{
				for (int j=0; j<house->NPos(); j++)
				{
					Vector3Val pos = house->GetPosition(house->GetPos(j));
					float dist2 = pos.Distance2(itPos);
					if (minDist2>dist2)
					{
						minDist2 = dist2;
						_housePos = j;
						housePosition = pos;
					}
				}
			}
		}
	}
	
	// check list of selected units
	bool notEmpty = false;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *u = GetSelectedUnit(i);
		if (u)
		{
			notEmpty = true;
			break;
		}
	}

	_modeAuto = _mode;
	if (_mode == UIStrategy && GInput.keys[DIK_LMENU])
	{
		_modeAuto = UIStrategyWatch;
		if (_target)
		{
			if (_housePos >= 0) _groundPoint = housePosition;
#if _ENABLE_CHEATS
			else if (_showAll && _target->idExact) _groundPoint = _target->idExact->AimingPosition();
#endif
			else _groundPoint = _target->AimingPosition();
		}
		else
		{
			_groundPoint = GLOB_LAND->IntersectWithGroundOrSea(camera.Position(), cursorDir);
		}
	}
	else if (_target)
	{
		if (_housePos >= 0) _groundPoint = housePosition;
#if _ENABLE_CHEATS
		else if (_showAll && _target->idExact) _groundPoint = _target->idExact->AimingPosition();
#endif
		else _groundPoint = _target->AimingPosition();
		if (_mode == UIStrategy)
		{
			EntityAI *targetAI=_target.IdExact();
			if( targetAI )
			{
				if (!notEmpty)
				{
					AIUnit *u = targetAI->CommanderUnit();
					if
					(
						unit->IsGroupLeader() &&
						u &&
						u->GetGroup() == unit->GetGroup()
					)
					{	// inside my group / subgroup
						_modeAuto = UIStrategySelect;
					}
					if (vehicleCommander)
					{
						if (!group->GetCenter()->IsFriendly(_target->side))
						{
							// Enemy target - attack
							_modeAuto = UIStrategyFire;
						}
						else
						{
							// no target - move
							_modeAuto=UIStrategyMove;
						}
					}
				}
				else
				{
					// TODO: preload
					const VehicleType *target=GWorld->Preloaded(VTypeTarget);
					if( _target->type->IsKindOf(target) )
					{
						_modeAuto = UIStrategyAttack;
					}
					else if (group->GetCenter()->IsFriendly(_target->side))
					{	// Friendly target
						Transport *targetTransport = dyn_cast<Transport>(targetAI);
						if
						(
							targetTransport &&
							(
/*
								targetTransport->QCanIGetIn() ||
								_target->side == vehicle->GetTargetSide() &&
								(
									targetTransport->QCanIGetInCommander() || 
									targetTransport->QCanIGetInGunner() ||
									targetTransport->QCanIGetInCargo()
								)
*/
								targetTransport->QCanIGetIn() ||
								targetTransport->QCanIGetInCommander() || 
								targetTransport->QCanIGetInGunner() ||
								targetTransport->QCanIGetInCargo()
							)
						)
						{	// empty vehicle
							_modeAuto = UIStrategyGetIn;
						}
						else
						{
							AIUnit *u = targetAI->CommanderUnit();
							if
							(
								unit->IsGroupLeader() &&
								u &&
								u->GetGroup() == unit->GetGroup()
							)
							{	// inside my group / subgroup
								_modeAuto = UIStrategySelect;
							}
							else
							{
								// outside my group
								if (notEmpty)
									_modeAuto = UIStrategyMove;
							}
						}
					}
					else
					{	// enemy / neutral / unknown target
						_modeAuto = UIStrategyAttack;
					}
				}
			}
		}
		else if (_mode == UIFire)
		{
			_modeAuto = UIFirePosLock;
		}
	}
	else if (_lockTarget)
	{
		if (_mode == UIStrategy && !notEmpty) _modeAuto = UIStrategyFire;
	}
	else
	{
		_groundPoint = GLOB_LAND->IntersectWithGroundOrSea(camera.Position(), cursorDir);
	}
	_groundPointValid=camera.Position().Distance2(_groundPoint)<Square(2000);
	if( _groundPoint.Y()<-5 ) _groundPointValid = false;
	if (_groundPointValid)
	{
		_groundPointDistance = _groundPoint.Distance(vehicle->Position());
		if (_modeAuto == UIStrategy && ( notEmpty || vehicleCommander ) )
		{
			_modeAuto = UIStrategyMove;
		}
	}

#if _ENABLE_CHEATS
	if(GInput.GetCheat1ToDo(DIK_W))
	{
		if( _target.IdExact() )
		{
			GWorld->SwitchCameraTo(_target.IdExact(),GWorld->GetCameraType());
			GWorld->UI()->ResetHUD();
		}
	}
#endif

	//////////////
	// Commander

	if (isCommander && IsManual(vehicle,unit))
	{
		if (!isPilot)
		{
			Assert(transport);
			bool left = false, right = false;
			bool leftUp = false, rightUp = false;
			bool back = false;
			bool slow = false, forw = false, fast = false;
			if (GInput.GetActionToDo(UATurnLeft))
			{
				left = true;
			}
			if
			(
				_leftPressed && !GInput.GetAction(UATurnLeft)
			)
			{
				_leftPressed = false; leftUp = true;
			}
			if (GInput.GetActionToDo(UATurnRight))
			{
				right = true;
			}
			if
			(
				_rightPressed && !GInput.GetAction(UATurnRight)
			)
			{
				_rightPressed = false; rightUp = true;
			}
			if (GInput.GetActionToDo(UAMoveSlowForward))
			{
				slow = true;
			}
			if (GInput.GetActionToDo(UAMoveFastForward))
			{
				fast = true;
			}
			if (GInput.GetActionToDo(UAMoveForward))
			{
				if (GInput.GetAction(UATurbo)) fast = true;
				else forw = true;
			}
			if (GInput.GetActionToDo(UAMoveBack))
			{
				back = true;
			}

			if (left)
			{
				if (transport->IsLocal())
					transport->SendSimpleCommand(SCLeft);
				else
				{
					RadioMessageVSimpleCommand msg(transport, SCLeft);
					GetNetworkManager().SendRadioMessage(&msg);
				}
				_leftPressed = true;
			}
			if (right)
			{
				if (transport->IsLocal())
					transport->SendSimpleCommand(SCRight);
				else
				{
					RadioMessageVSimpleCommand msg(transport, SCRight);
					GetNetworkManager().SendRadioMessage(&msg);
				}
				_rightPressed = true;
			}
			if (leftUp || rightUp)
			{
				//Vector3Val dir = transport->Direction();

				// predict orientation in estT
				const float estT = 0.35f;
				const Matrix3 &orientation=transport->Orientation();
				Matrix3Val derOrientation=transport->AngVelocity().Tilda()*orientation;
				Matrix3Val estOrientation=orientation+derOrientation*estT;

				Vector3Val estDirection=estOrientation.Direction().Normalized();
				float azimut = atan2(estDirection.X(), estDirection.Z());

				if (transport->IsLocal())
					transport->SendStopTurning(azimut);
				else
				{
					RadioMessageVStopTurning msg(transport, azimut);
					GetNetworkManager().SendRadioMessage(&msg);
				}
			}
			if (back)
			{
				if (transport->IsLocal())
					transport->SendSimpleCommand(SCKeyDown);
				else
				{
					RadioMessageVSimpleCommand msg(transport, SCKeyDown);
					GetNetworkManager().SendRadioMessage(&msg);
				}
			}
			if (slow)
			{
				if (transport->IsLocal())
					transport->SendSimpleCommand(SCKeySlow);
				else
				{
					RadioMessageVSimpleCommand msg(transport, SCKeySlow);
					GetNetworkManager().SendRadioMessage(&msg);
				}
			}
			if (forw)
			{
				if (transport->IsLocal())
					transport->SendSimpleCommand(SCKeyUp);
				else
				{
					RadioMessageVSimpleCommand msg(transport, SCKeyUp);
					GetNetworkManager().SendRadioMessage(&msg);
				}
			}
			if (fast)
			{
				if (transport->IsLocal())
					transport->SendSimpleCommand(SCKeyFast);
				else
				{
					RadioMessageVSimpleCommand msg(transport, SCKeyFast);
					GetNetworkManager().SendRadioMessage(&msg);
				}
			}
		}

		if (!isGunner && transport)
		{
			if (transport->IsManualFire())
			{
				int weapon = vehicle->SelectedWeapon();
				weapon = ValidateWeapon(vehicle, weapon);
				if (weapon >= 0)
				{
					const WeaponModeType *mode = vehicle->GetWeaponMode(weapon);
					bool fire = false;
					const Magazine *mag = vehicle->GetMagazineSlot(weapon)._magazine;

					if (mode && mode->_autoFire && mag->_ammo>0)
					{
						if
						(
							GInput.GetAction(UAFire) > 0 || GInput.GetFire()
						) fire = true;
						if
						(
							// in not driver, click can be command to driver
							// rule is to fire only when target is locked
							(_lockTarget || isPilot) &&
							!GWorld->HasMap() &&
							(!ModeIsStrategy(_mode) || _modeAuto == UIStrategyFire) &&
							GInput.GetMouseLToDo()
						) fire = true;
					}
					else
					{
						fire = GInput.GetActionToDo(UAFire) || GInput.GetFireToDo();
						if
						(
							// in not driver, click can be command to driver
							// rule is to fire only when target is locked
							(_lockTarget || isPilot)
							&& !GWorld->HasMap() && GInput.GetMouseL()
						) fire = true;
					}
					if (fire)
					{
						vehicle->FireWeapon(weapon,_lockTarget.IdExact());
						if (transport)
						{
							const WeaponModeType *mode = transport->GetWeaponMode(weapon);
							if (!mode || !mode->_autoFire)
								transport->SetFirePrepare(true);
						}
					}
				}
			}
			else
			{
				bool fire = false;
				if (GInput.GetActionToDo(UAFire) || GInput.GetFireToDo())
				{
					fire = true;
				}
				if
				(
					// in not driver, click can be command to driver
					// rule is to fire only when target is locked
					(_lockTarget || isPilot) &&
					!GWorld->HasMap() &&
					(!ModeIsStrategy(_mode) || _modeAuto == UIStrategyFire) &&
					GInput.GetMouseLToDo()
				)
				{
					fire = true;
				}
				if (fire)
				{
					if (transport->IsLocal())
						transport->SendSimpleCommand(SCKeyFire);
					else
					{
						RadioMessageVSimpleCommand msg(transport, SCKeyFire);
						GetNetworkManager().SendRadioMessage(&msg);
					}
				}
			}

			if (GInput.GetActionToDo(UALockTarget) && !GWorld->HasMap())
			{
				if (_target.IdExact() && _target.IdExact() != vehicle && !_target->vanished)
				{
					if (transport->IsLocal())
						transport->SendTarget(_target);
					else
					{
						RadioMessageVTarget msg(transport, _target);
						GetNetworkManager().SendRadioMessage(&msg);
					}
					_lockTarget = _target;
					_timeSendTarget = UITIME_MAX;
					_lockAimValidUntil = Glob.uiTime - 60;
				}
				else
				{
					if (transport->IsLocal())
						transport->SendTarget(NULL);
					else
					{
						RadioMessageVTarget msg(transport, NULL);
						GetNetworkManager().SendRadioMessage(&msg);
					}
					_lockTarget = NULL;
					_timeSendTarget = UITIME_MAX;
					_lockAimValidUntil = Glob.uiTime - 60;
				}
			}

			if (_lockTarget && Glob.uiTime >= _timeSendTarget)
			{
				if (transport->IsLocal())
					transport->SendTarget(_lockTarget);
				else
				{
					RadioMessageVTarget msg(transport, _lockTarget);
					GetNetworkManager().SendRadioMessage(&msg);
				}
				_timeSendTarget = UITIME_MAX;
			}
/*
			int weapon = ValidateWeapon(vehicle, _curWeapon);
			if (weapon >= 0 && Glob.uiTime >= _timeSendLoad)
			{
				if (transport->IsLocal())
					transport->SendLoad(weapon);
				else
				{
					RadioMessageVLoad msg(transport, weapon);
					GetNetworkManager().SendRadioMessage(&msg);
				}
				_timeSendLoad = UITIME_MAX;
			}
*/
		}
	}

	//////////////
	// Gunner

	if (isGunnerFire && IsManual(vehicle,unit))
	{
		/*
		if (!isCommander)
		{
			Target *tgt = vehicle->GetFireTarget();
			if (tgt && tgt->IsKnownBy(unit))
			{
				_lockTarget = tgt;
			}
		}
		*/


		if (vehicle->CanFire())
		{
			int weapon = vehicle->SelectedWeapon();
			weapon = ValidateWeapon(vehicle, weapon);
			if (weapon >= 0)
			{
				const WeaponModeType *mode = vehicle->GetWeaponMode(weapon);
				bool fire = false;
				const Magazine *mag = vehicle->GetMagazineSlot(weapon)._magazine;

				if (mode && mode->_autoFire && mag->_ammo > 0)
				{
					fire = GInput.GetAction(UAFire)>0 || GInput.GetFire();
				}
				else
				{
					fire = GInput.GetActionToDo(UAFire) || GInput.GetFireToDo();
				}
				if (fire)
				{
					vehicle->FireWeapon(weapon,_lockTarget.IdExact());
					if (transport)
					{
						const WeaponModeType *mode = transport->GetWeaponMode(weapon);
						if (!mode || !mode->_autoFire)
							transport->SetFirePrepare(true);
					}
				}
			}
		}

	}

	if( IsManual(vehicle,unit) )
	{
		if (GInput.GetActionToDo(UALockTargets))
		{
			// switch target - use target list (tactical display)
			if (GInput.keys[DIK_LSHIFT] || GInput.GetKey(DIK_RSHIFT)) PrevTarget(vehicle);
			else NextTarget(vehicle);
		}
	}
	else
	{
		Target *tgt=vehicle->GetFireTarget();
		if( !tgt ) tgt=unit->GetTargetAssigned();
		if( tgt )
		{
			for( int i=0; i<visibleList.Size(); i++ )
			{
				Target *tar = visibleList[i];
				if( tar==tgt )
				{
					_lockTarget=tar;
					_timeSendTarget = UITIME_MAX;
					break;
				}
			}
		}
		else _lockTarget=NULL;
	}
	// react to mouse buttons
	if( ModeIsStrategy(_mode) )
	{
		if (GInput.GetMouseLToDo())
		{
			_mouseDown = true;
			_mouseDownTime = Glob.uiTime + 0.2;
			_startSelection = GetCursorDirection();
			_fireEnabled = false;
		}
		else
		{
			if (_mouseDown)
			{
				Vector3 dir = GetCursorDirection();
				if
				(
					dir.Distance2(_startSelection) > Square(0.01) &&
					Glob.uiTime >= _mouseDownTime
				)
				{
					_mouseDown = false;
					_dragging = true;
				}
				else if (!GInput.GetMouseL())
				{
					_mouseDown = false;
					IssueCommand(vehicle);
					_menuType = MTNone;
				}
			}
			if (_dragging)
			{
				_endSelection = GetCursorDirection();
				if (!GInput.GetMouseL())
				{
					AUTO_STATIC_ARRAY(OLink<AIUnit>,units,64);
					AUTO_STATIC_ARRAY(LinkTarget,enemies,64);

					Matrix4Val camInvTransform = camera.GetInvTransform();
					Vector3 posStart = camInvTransform.Rotate(_startSelection);
					Vector3 posEnd = camInvTransform.Rotate(_endSelection);
					if (posStart.Z() > 0 && posEnd.Z() > 0)
					{
						float invZ = 1.0 / posStart.Z();
						float xs = 0.5 * (1.0 + posStart.X() * invZ * camera.InvLeft());
						float ys = 0.5 * (1.0 - posStart.Y() * invZ * camera.InvTop());
						invZ = 1.0 / posEnd.Z();
						float xe = 0.5 * (1.0 + posEnd.X() * invZ * camera.InvLeft());
						float ye = 0.5 * (1.0 - posEnd.Y() * invZ * camera.InvTop());
						if (xs > xe) swap(xs, xe);
						if (ys > ye) swap(ys, ye);
						int n = visibleList.Size();
						for (int i=0; i<n; i++)
						{
							Target &tar = *visibleList[i];
							if (!tar.IsKnownBy(unit) || tar.vanished) continue;
							EntityAI *veh = tar.idExact;
							if (!veh) continue;
							// check position
							Vector3 dir = tar.AimingPosition() - camera.Position();
							Vector3 pos = camInvTransform.Rotate(dir);
							if (pos.Z() <= 0) continue;
							invZ = 1.0 / pos.Z();
							float x = 0.5 * (1.0 + pos.X() * invZ * camera.InvLeft());
							float y = 0.5 * (1.0 - pos.Y() * invZ * camera.InvTop());
							if (x < xs) continue;
							if (x > xe) continue;
							if (y < ys) continue;
							if (y > ye) continue;
							AIUnit *u = veh->CommanderUnit();
							if (u && u->GetGroup() == group)
							{
								if (u != unit)
									units.Add(u);
							}
							else if (group->GetCenter()->IsEnemy(tar.side))
							{
								enemies.Add(&tar);
							}
						}
					}

					if (GInput.keys[DIK_LSHIFT] || GInput.GetKey(DIK_RSHIFT))
					{
						for (int i=0; i<units.Size(); i++)
						{
							AIUnit *u = units[i];
							int id = u->ID();
							if (GetSelectedUnit(id - 1))
								SetSelectedUnit(id - 1, NULL);
							else
							{
								SetSelectedUnit(id - 1, u);
								_lastSelTime[id - 1] = Glob.uiTime;
							}
						}
					}
					else if (IsEmptySelectedUnits() || enemies.Size() == 0)
					{
						ClearSelectedUnits();
						for (int i=0; i<units.Size(); i++)
						{
							AIUnit *u = units[i];
							int id = u->ID();
							SetSelectedUnit(id - 1, u);
							_lastSelTime[id - 1] = Glob.uiTime;
						}
					}
					else
					{
						#if 1
						Command cmd;
						cmd._message = Command::AttackAndFire;
						cmd._targetE = enemies[0];
						cmd._destination = vehicle->Position();

						for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
						{
							AIUnit *u = GetSelectedUnit(i);
							if (u) u->AssignTarget(enemies[0]);
						}

						if (CheckJoin(group))
						{
							cmd._context = Command::CtxUIWithJoin;
							cmd._joinToSubgroup = group->MainSubgroup();
						}
						else
						{
							cmd._context = Command::CtxUI;
						}

						group->SendCommand(cmd, ListSelectedUnits());
						#else
						group->SendTarget(enemies[0],false,false,ListSelectedUnits());
						group->SendTarget(enemies[0],false,true,ListSelectedUnits());
						#endif
						ClearSelectedUnits();
					}	
					_dragging = false;
				}
			}
		}
	}
	else
	{
		bool fire = false;
		if (GInput.GetMouseLToDo())
		{
			_fireEnabled = true;
			fire = true;
		}
		if (IsManual(vehicle,unit) && _fireEnabled && vehicle->CanFire() && isGunnerFire)
		{
			int weapon = vehicle->SelectedWeapon();
			weapon = ValidateWeapon(vehicle, weapon);
			if (weapon >= 0)
			{
				const WeaponModeType *mode = vehicle->GetWeaponMode(weapon);
				const Magazine *mag = vehicle->GetMagazineSlot(weapon)._magazine;
				if (mode && mode->_autoFire && mag->_ammo > 0)
				{
					fire = GInput.GetMouseL() && !GWorld->HasMap();
				}
				if (fire)
				{
					vehicle->FireWeapon(weapon, _lockTarget.IdExact());
					if (transport)
					{
						const WeaponModeType *mode = transport->GetWeaponMode(weapon);
						if (!mode || !mode->_autoFire)
							transport->SetFirePrepare(true);
					}
				}
			}
		}
	}

	if( IsManual(vehicle,unit) && isGunnerFire && GInput.GetActionToDo(UALockTarget))
	{
		int weapon = vehicle->SelectedWeapon();
		weapon = ValidateWeapon(vehicle, weapon);
		if (weapon >= 0 && weapon < vehicle->NMagazineSlots())
		{
			if (_target.IdExact() && _target.IdExact() != vehicle && !_target->vanished)
			{
				const MagazineSlot &slot = vehicle->GetMagazineSlot(weapon);
			
				bool canLock =
					slot._muzzle->_canBeLocked == 2 ||
					slot._muzzle->_canBeLocked == 1 && Glob.config.IsEnabled(DTAutoGuideAT);

				if (canLock && vehicle->CanLock(_target.IdExact()))
				{
					_lockTarget = _target;
					_timeSendTarget = UITIME_MAX;
				}
			}
			else
			{
				_lockTarget = NULL;
				_timeSendTarget = UITIME_MAX;
			}
		}
	}

	// follow selected target
	if( _lockTarget )
	{
		bool lockFound=false; //,targetFound=false;
		Target *oldTarget = _lockTarget;
		int weapon = vehicle->SelectedWeapon();
		weapon = ValidateWeapon(vehicle, weapon);
		if (weapon>=0 && weapon<vehicle->NMagazineSlots())
		{
			const MagazineSlot &slot = vehicle->GetMagazineSlot(weapon);
			const Magazine *magazine = slot._magazine;
      const AmmoType *ammo = NULL;
      if (magazine)
      {
        const MagazineType *type = magazine->_type;
        if (type && slot._mode >= 0 && slot._mode < type->_modes.Size())
          ammo = type->_modes[slot._mode]->_ammo;
      }

			bool canLock =
				slot._muzzle->_canBeLocked == 2 ||
				slot._muzzle->_canBeLocked == 1 && Glob.config.IsEnabled(DTAutoGuideAT);
			
			if (ammo && (canLock || !IsManual(vehicle,unit)))
			{
				for (i=0; i<n; i++)
				{
					Target *tar = visibleList[i];
					Assert( tar->type );
					if( !tar->type ) continue;
					if (!tar->idExact) continue;
					if( !tar->idExact->LockPossible(ammo) ) continue;
					if (tar->idExact == _lockTarget.IdExact() && tar->IsKnownBy(unit) )
					{
						_lockTarget=tar;
						lockFound=true;
						//_wantLock = true;
					}
				}
			}
		}
		if( !lockFound || _lockTarget.IdExact()==vehicle )
		{
			if( oldTarget && oldTarget->idExact!=vehicle )
			{
				if( !ModeIsStrategy(_mode) )
				{
					Vector3 norm = (oldTarget->AimingPosition()-vehicle->Position());
					SetCursorDirection(norm.Normalized());
				}
			}
			_lockTarget=NULL;
		}
	}

	// in aiming mode adjust weapons accordingly to aiming
	if (IsManual(vehicle,unit))
	{
		const float threshold2 = Square(0.0002);

		if(isGunner)
		{
			int weapon = vehicle->SelectedWeapon();
			weapon = ValidateWeapon(vehicle, weapon);

			Vector3 weaponDir = GetCursorDirection();
			if (weaponDir.Distance2(vehicle->GetWeaponDirectionWanted(weapon)) > threshold2)
			{
				if (vehicle->IsLocal())
					vehicle->AimWeaponManDir(weapon, weaponDir);
				else
				{
					vehicle->AskForAimWeapon(weapon, weaponDir);
					vehicle->AimWeaponManDir(weapon, weaponDir); // simulate directly
					// vehicle->WeaponTouched();
				}
			}
		} // if (isGunner)
		if (isObserver)
		{
			Vector3 weaponDir = GetCursorDirection();
			// apply weapon adjustment to cursor direction
			// this depends on weapon type and current fov
			// if we look through optics, get current fov
			// soldier aims always
			if (weaponDir.Distance2(vehicle->GetEyeDirectionWanted()) > threshold2)
			{
				if (vehicle->IsLocal())
					vehicle->AimObserver(weaponDir);
				else
				{
					/*
					LogF
					(
						"dir %.6f,%.6f,%.6f -> %.6f,%.6f,%.6f",
						weaponDir[0],weaponDir[1],weaponDir[2],
						vehicle->GetEyeDirectionWanted()[0],
						vehicle->GetEyeDirectionWanted()[1],
						vehicle->GetEyeDirectionWanted()[2]
					);
					*/
					GetNetworkManager().AskForAimObserver(vehicle, weaponDir);
					vehicle->AimObserver(weaponDir); // simulate directly
				}
			}
		} // if (isObserver)
		if (isPilot)
		{
			// driver's vehicle is always local
			Vector3 weaponDir = GetCursorDirection();
			// apply weapon adjustment to cursor direction
			// this depends on weapon type and current fov
			// if we look through optics, get current fov
			// soldier aims always
			vehicle->AimDriver(weaponDir);
		}
	}
	//_wantLock=( _lockTarget.idExact );

	if (GInput.GetActionToDo(UANightVision))
	{
		Person *person = unit->GetPerson();
		if (person->IsNVEnabled())
			person->SetNVWanted(!person->IsNVWanted());
	}
}

void InGameUI::SwitchToStrategy( EntityAI *vehicle )
{
	if( !ModeIsStrategy(_mode) )
	{
		_modeAuto = _mode = UIStrategy;
		_worldCursorTime=Glob.uiTime;

		_dragging = false;
		_mouseDown = false;
	}
}

void InGameUI::SwitchToFire( EntityAI *vehicle )
{
	if( ModeIsStrategy(_mode) )
	{
		_modeAuto = _mode = UIFire;
		// set cursor to current weapon direction
		int weapon = vehicle->SelectedWeapon();
		weapon = ValidateWeapon(vehicle, weapon);

		if( vehicle)
		{

			AIUnit *unit = GWorld->FocusOn();
			bool isObserver = unit && unit == vehicle->ObserverUnit();
			bool isGunner = unit && unit == vehicle->GunnerUnit();

			if (isObserver)
			{
				SetCursorDirection(vehicle->GetEyeDirection());
			}
			else if (isGunner && weapon>=0)
			{
				SetCursorDirection(vehicle->GetWeaponDirection(weapon));
			}
			else
			{
				SetCursorDirection(vehicle->Direction());
			}
			_worldCursorTime=Glob.uiTime;
		}

		_dragging = false;
		_mouseDown = false;
	}
}

void InGameUI::SelectWeapon(AIUnit *unit, int weapon)
{
	EntityAI *vehicle = unit->GetVehicle();
	vehicle->SelectWeaponCommander(unit,weapon);
}

int CmpStringI(const RString *str1, const RString *str2)
{
	return stricmp(*str1, *str2);
}

extern int MaxCustomSoundSize;

void InGameUI::InitMenu()
{
	_menuMain = new Menu();
	_menuMain->Load(&(Res>>"RscMainMenu"));
	_menuType = MTNone;
	_menuCurrent = _menuMain;

	Menu *menuDist = _menuMain->FindMenu(CMD_MOVE_FIRST,true);
	Assert(menuDist);
	Menu *menuDir = menuDist->_parent;
	Assert(menuDir);
	int offset = CMD_MOVE_FIRST;
	for (int i=0; i<N_MOVE_DIR; i++)
	{
		Menu *menuDist = menuDir->_items[i]->_submenu;
		for (int j=0; j<N_MOVE_DIST; j++)
		{
			MenuItem *item = menuDist->_items[j];
			item->_cmd = offset++;
			menuDist->NotifySubmenuCommandAdded(item->_cmd);
		}
	}

	AUTO_STATIC_ARRAY(RString, sounds, 32);
	#ifdef _WIN32
	_finddata_t info;
	RString GetUserDirectory();
	long h = _findfirst(GetUserDirectory() + RString("Sound\\*.*"), &info);
	if (h != -1)
	{
		do
		{
			if 
			(
				(info.attrib & _A_SUBDIR) == 0
			)
			{
				// ignore sounds larger than 40 KB
				if (info.size<=MaxCustomSoundSize)
				{
					sounds.Add(info.name);
				}
			}
		}
		while (0==_findnext(h, &info));
		_findclose(h);
	}
	#endif
	int n = sounds.Size();
	if (n > 0)
	{
		QSort(sounds.Data(), n, CmpStringI);
		saturateMin(n, 10);
		MenuItem *item = _menuMain->Find(CMD_RADIO_CUSTOM, false);
		Assert(item);
		Menu *menuRadio = item->_submenu;
		Assert(menuRadio);
		_customRadio.Realloc(n);
		_customRadio.Resize(n);
		for (int i=0; i<n; i++)
		{	
			_customRadio[i] = sounds[i];
			char buffer[256]; strncpy(buffer, sounds[i], 256); buffer[255] = 0;
			char *ext = strrchr(buffer, '.');
			if (ext) *ext = 0;
			int key = DIK_1 + i;
			int cmd = CMD_RADIO_CUSTOM_1 + i;
			char ch[2];
			ch[0] = i == 9 ? '0' : '1' + i;
			ch[1] = 0;
			menuRadio->_items.Insert(i, new MenuItem(buffer, key, ch, (Menu*)NULL, cmd));
			menuRadio->NotifySubmenuCommandAdded(cmd);
		}
	}
	else
	{
		_menuMain->EnableCommand(CMD_RADIO_CUSTOM, false);
	}
}

void InGameUI::ToggleSelection(AIGroup *grp, int id)
{
	// check if there is unit id in grp
	AIUnit *u = grp->UnitWithID(id);

	if (!u || u==grp->Leader())
	{
		// make menu visible
		_menuType = MTMain;
		ShowMenu();
	}
	
	if (GetSelectedUnit(id - 1))
		SetSelectedUnit(id - 1, NULL);
	else
		SetSelectedUnit(id - 1, u);
}

void InGameUI::SetSemaphore(AIUnit *unit, AI::Semaphore status)
{
	if (!unit) return;
	AIGroup *group = unit->GetGroup();
	if (!group) return;
	if (unit->IsGroupLeader())
	{
		group->SendSemaphore(status, ListSelectedUnits());
	}
}

void InGameUI::SetBehaviour(CombatMode mode)
{
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *u = GSelectedUnits[i];
		if (u) u->SetCombatModeMajor(mode);
	}
}

void InGameUI::SetUnitPosition(AIUnit *unit, UnitPosition status)
{
	if (!unit) return;
	AIGroup *group = unit->GetGroup();
	if (!group) return;
	if (unit->IsGroupLeader())
	{
		group->SendState(new RadioMessageUnitPos(group,ListSelectedUnits(),status));
	}
	ClearSelectedUnits();
}

void InGameUI::SetFormationPos(AIUnit *unit, AI::FormationPos status)
{
	if (!unit) return;
	AIGroup *group = unit->GetGroup();
	if (!group) return;
	if (unit->IsGroupLeader())
	{
		group->SendState(new RadioMessageFormationPos(group,ListSelectedUnits(),status));
	}
	ClearSelectedUnits();
}

void InGameUI::ShowHint(RString hint)
{
	_hint->SetHint(hint);
	_hintTime = Glob.uiTime;
	if (hint.GetLength() > 0 && _hintSound.name.GetLength() > 0)
	{
		AbstractWave *wave = GSoundScene->OpenAndPlayOnce2D
		(
			_hintSound.name, _hintSound.vol, _hintSound.freq, false
		);
		if (wave)
		{
			wave->SetKind(WaveMusic); // UI sounds considered music???
			GSoundScene->AddSound(wave);
		}
	}
}

/*
void StringTable::Init()
{
	#define STRING(x) _table[IDS_##x].name = "STR_" #x;
	#include "../cfg/stringids.hpp"
	#undef STRING
	for (int i=0; i<IDSN; i++)
#if _ENABLE_CHEATS
		_table[i].value = "!!! NO STRING IN CSV";
#else
		_table[i].value = "";
#endif
}
*/
