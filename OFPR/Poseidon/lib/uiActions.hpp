#ifdef _MSC_VER
#pragma once
#endif

#ifndef _UI_ACTIONS_HPP
#define _UI_ACTIONS_HPP

#ifdef PREPROCESS_DOCUMENTATION
#include "networkObject.hpp"
#endif

//! action types
/*!
\patch_internal 1.01 Date 06/15/2001 by Jirka
- added ATUser action type
\patch 1.75 Date 3/8/2002 by Jirka
- Added: user actions assigned to vehicle type (ATUserType)
*/

DEFINE_ENUM_BEG(UIActionType)
	ATNone,
	ATGetInCommander,
	ATGetInDriver,
	ATGetInGunner,
	ATGetInCargo,
	ATHeal,
	ATRepair,
	ATRefuel,
	ATRearm,
	ATGetOut,
	ATLightOn,
	ATLightOff,
	ATEngineOn,
	ATEngineOff,
	ATSwitchWeapon,
	ATUseWeapon,
	ATLoadMagazine,
	ATTakeWeapon,
	ATTakeMagazine,
	ATTakeFlag,
	ATReturnFlag,
	ATTurnIn,
	ATTurnOut,
	ATWeaponInHand, // take weapon in hands
	ATWeaponOnBack, // take weapon in hands
	ATSitDown, // some special actions (can be used in safe mode)
	ATLand,
	ATCancelLand,
	ATEject,
	ATMoveToDriver,
	ATMoveToGunner,
	ATMoveToCommander,
	ATMoveToCargo,
	ATHideBody,
	ATTouchOff,
	ATSetTimer,
	ATDeactivate,
	ATNVGoggles,
	ATManualFire,
	ATAutoHover,
	ATStrokeFist,
	ATStrokeGun,
	ATLadderUp,
	ATLadderDown,
	ATLadderOnDown,
	ATLadderOnUp,
	ATLadderOff,
	ATFireInflame,
	ATFirePutDown,
	ATLandGear,
	ATFlapsDown,
	ATFlapsUp,
	ATSalute,
	ATScudLaunch,
	ATScudStart,
	ATUser,
	ATDropWeapon,
	ATDropMagazine,
	ATUserType,
	ATHandGunOn,
	ATHandGunOff,
	ATTakeMine,
	ATDeactivateMine,
	ATUseMagazine,
DEFINE_ENUM_END(UIActionType)

class InGameUI;

class UIAction
{
	public:
	UIActionType type;
	TargetId target;
	int param;
	int param2;
	RString param3;
	float priority;
	bool showWindow;
	bool hideOnUse;

	RString GetDisplayName(AIUnit *unit) const;
	void Process(AIUnit *unit) const;
};
TypeContainsOLink(UIAction)

class UIActions : public StaticArray<UIAction>
{
	typedef StaticArray<UIAction> base;
protected:
	UIAction _selected;

	float _right, _bottom, _w, _h;	// _h is height of one row
	int _rows;										// max rows

	PackedColor _bgColor, _textColor, _selColor;
	Ref<Font> _font;
	float _size;

	UITime _dimStart;

public:
	UIActions();

	int Add
	(
		UIActionType type, TargetType *target, float priority,
		int param = 0, bool showWindow = false, bool hideOnUse=true,
		int param2 = 0, RString param3 = ""
	);
	int FindSelected();
	void SelectPrev(bool cycle);
	void SelectNext(bool cycle);
	void ProcessAction(AIUnit *unit);

	void Sort();

	void OnDraw();

	void Refresh(bool user);
	void Hide();
	float GetAge() const;
	float GetAlpha() const;
};

#endif
