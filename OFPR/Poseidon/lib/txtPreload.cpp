// Scene does all transformation, clipping and ligthing
#include "wpch.hpp"
#include "textbank.hpp"
#include "scene.hpp"
#include "txtPreload.hpp"
#include "engine.hpp"
#include "paramFileExt.hpp"

PreloadedTextures::PreloadedTextures()
{
}

PreloadedTextures::~PreloadedTextures()
{
	_data.Clear();
}

void PreloadedTextures::Preload( bool all )
{
	if (_data.Size()>=MaxPreloadedTexture) return;
	_data.Clear();
	_data.Resize(MaxPreloadedTexture);


	//#define LOAD bank->Load
	#define LOAD GlobLoadTexture
	// actual texture preloading
	//AbstractTextBank *bank=GEngine->TextBank();
	_data[TextureDefault]=LOAD("data\\default.pac");
	DefaultTexture=_data[TextureDefault].GetRef();
	_data[TextureWhite]=NULL;
	_data[TextureBlack]=LOAD("data\\black.pac");
	_data[TextureLine] = LOAD("data\\tracer.paa"); // 8x8
	// we want 4x4 mipmap level only
	if (_data[TextureLine])
	{
		_data[TextureLine]->SetMipmapRange(1,1);
	}

	if( !all ) return;

	_data[TrackTexture]=LOAD("data\\stopa_pas_1.paa");
	_data[TrackTextureFour]=LOAD("data\\jeep_otisk_kol.paa");

	_data[Corner]=LOAD( GetPictureName(Pars>>"CfgInGameUI">>"imageCornerElement") );

	const ParamEntry &wrapper=Pars>>"CfgWrapperUI";
	_data[DialogBackground] = LOAD(GetPictureName(wrapper>>"Background">>"texture"));
	_data[DialogTitle] = LOAD(GetPictureName(wrapper>>"TitleBar">>"texture"));
	_data[DialogGroup] = LOAD(GetPictureName(wrapper>>"GroupBox2">>"texture"));
	
	const ParamEntry &cursor=Pars>>"CfgInGameUI">>"Cursor";
	_data[CursorLocked]=LOAD( GetPictureName(cursor>>"lock_target"));
	_data[CursorTarget]=LOAD( GetPictureName(cursor>>"select_target"));
	_data[CursorAim]=LOAD( GetPictureName(cursor>>"aim"));
	_data[CursorWeapon]=LOAD( GetPictureName(cursor>>"weapon"));

	_data[CursorStrategy]=LOAD( GetPictureName(cursor>>"tactical") );
	_data[CursorStrategyMove]=LOAD( GetPictureName(cursor>>"move") );
	_data[CursorStrategyAttack]=LOAD( GetPictureName(cursor>>"attack") );
	_data[CursorStrategyGetIn]=LOAD( GetPictureName(cursor>>"getin") );
	_data[CursorStrategySelect]=LOAD( GetPictureName(cursor>>"select") );
	_data[CursorStrategyWatch]=LOAD( GetPictureName(cursor>>"watch") );

	_data[CursorOutArrow]=LOAD( GetPictureName(cursor>>"outArrow") );

	_data[Compass000]=LOAD("data\\komp0.paa");
	_data[Compass090]=LOAD("data\\komp90.paa");
	_data[Compass180]=LOAD("data\\komp180.paa");
	_data[Compass270]=LOAD("data\\komp270.paa");
	#define MAX_S(id) {if (_data[id]) _data[id]->SetMaxSize(256);}
	MAX_S(Compass000);
	MAX_S(Compass090);
	MAX_S(Compass180);
	MAX_S(Compass270);

	_data[SkyBright]=LOAD("data\\jasno.pac");
	_data[SkyCloudy]=LOAD("data\\oblacno.pac");
	_data[SkyRainy]=LOAD("data\\zatazeno.pac");

	// TODO: name from config
	_data[TextureRain]=LOAD("data\\desta.01.paa");

	// load AI signs
	int s;
/*
	for( s=0; s<10; s++ )
	{
		char buf[80];
		// unit
		sprintf(buf,"data\\unit_%d.paa",s);
		_data[SignUnit0+s]=LOAD(buf);
	}
*/

	const ParamEntry &entry=Pars>>"CfgWorlds";
	_data[SignSideE]=LOAD(GetPictureName(entry>>"eastSign"));
	_data[SignSideW]=LOAD(GetPictureName(entry>>"westSign"));
	_data[SignSideG]=LOAD(GetPictureName(entry>>"guerrilaSign"));
	_data[FlagSideE]=LOAD(GetPictureName(entry>>"eastFlag"));
	_data[FlagSideW]=LOAD(GetPictureName(entry>>"westFlag"));
	_data[FlagSideG]=LOAD(GetPictureName(entry>>"guerrilaFlag"));
	for( s=0; s<16; s++ )
	{
		char buf[80];
		// unit
		sprintf(buf,"data\\flare%02d.pac",s);
		_data[Flare0+s]=LOAD(buf);
	}
}

void PreloadedTextures::Clear()
{
	_data.Clear();
}

Texture *PreloadedTextures::New( RStringB name )
{
	// make texture permanent
	// assign new id
	// search data for the same texture
	for (int i=0; i<_data.Size(); i++)
	{
		Texture *dataI = _data[i];
		if ( dataI && dataI->GetName()==name)
		{
			return dataI;
		}
	}
	Ref<Texture> txt = GlobLoadTexture(name);
	_data.Add(txt);
	return txt;
}

Texture *PreloadedTextures::New( PreloadedTexture id )
{
	// predefined texture ids
	return _data[id];
}

PreloadedTextures GPreloadedTextures;

Texture *GlobPreloadTexture( RStringB name )
{
	return GPreloadedTextures.New(name);
}
