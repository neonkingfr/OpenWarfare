/******************************************************************************
FILE: DXInput.cpp
This module contains all of the Direct Input specific code.  This includes
initialization and uninitializatoin functions as well as functions to get
joystick and keyboard information, and a function to cause force feedback
events.
*/

#include "wpch.hpp"

#ifdef _XBOX
#include "common/win.h"
#endif

#include "Joystick.hpp"
#include "keyInput.hpp"
#include "global.hpp"

#if _DISABLE_GUI || defined _XBOX
	#define ENABLE_JOYSTICK 0
#else
	#define ENABLE_JOYSTICK 1
#endif

#if ENABLE_JOYSTICK
	#define IFEEL 1
#else
	#define IFEEL 0
#endif

#if IFEEL
	#include <ifc22/ifc.h>
	#pragma comment(lib,"ifc22")
#else
	class CImmMouse {};
	class CImmPeriodic {};

#endif

#ifdef _XBOX
static XDEVICE_PREALLOC_TYPE PreallocTypes[]=
{
	XDEVICE_TYPE_GAMEPAD,1
};
#endif


//SRef<CImmMouse> GFeelMouse;

struct EnumContext
{
	Joystick *system;
	bool ffOnly;
	HWND hwnd;
};

#ifndef _XBOX
BOOL CALLBACK EnumJoyCallback(LPCDIDEVICEINSTANCE lpddi, LPVOID pvRef);
#endif

/******************************************************************************
FUNCTION: InitDirectInput
This is where the actual COM initialization occurs.  Also, the devices are
created here.

PARAMETERS:
None

RETURNS:
Success or Failure
******************************************************************************/

/*!
\patch_internal 1.01 Date 6/25/2001 by Ondra.
- Joystick support disabled.
*/

#ifndef _XBOX
Joystick::Joystick
(
	bool joystick, bool iFeelMouse,
	HINSTANCE instance, HWND hwnd, IDirectInput8 *dInput
)
#else
Joystick::Joystick
(
	bool joystick, bool iFeelMouse,
	HINSTANCE instance, HWND hwnd
)
#endif
{
	#ifdef _XBOX
		XInitDevices(1, PreallocTypes);

		for(;;)
		{
			DWORD done = XGetDevices(XDEVICE_TYPE_GAMEPAD);
			if (done&XDEVICE_PORT0_MASK) break;
			Sleep(20);
		}
		_inputHandle = XInputOpen
		(
			XDEVICE_TYPE_GAMEPAD,
			XDEVICE_PORT0,XDEVICE_NO_SLOT,NULL
		);
		LogF("Input %x",_inputHandle);
		if (!_inputHandle)
		{
			HRESULT hr = GetLastError();
			LogF("Error %x",hr);
			//DXTRACE_ERR_NOMSGBOX("No input",hr);
		}

	#endif

	if (iFeelMouse)
	{
		InitIFeel(instance,hwnd);
	}
	if (!joystick) return;
	#if !ENABLE_JOYSTICK
		return;
	#else

	if( !dInput )
	{
		HRESULT err=DirectInput8Create
		(
			instance, DIRECTINPUT_VERSION, IID_IDirectInput8 ,(void **)&dInput, NULL
		);
//		HRESULT err = DirectInputCreateEx(instance, DIRECTINPUT_VERSION,IID_IDirectInput7,_dInput.Init(),NULL);
		if( err!=DI_OK ) return;
	}
	else
	{
		_dInput=dInput; // this does not increment RefCount
		_dInput->AddRef();
	}

	// We need to enumerate the joysticks to search for the desired GUID.
	// This requires a callback function (EnumJoy) which is where we do the
	// actual CreateDevice call for the joystick.
	// On our first attempt we try for a force-feedback capable device,
	// otherwise we give in and enumerate regular joysticks.

	#if 1 //_ENABLE_CHEATS
	// joystick disabled
	EnumContext context;
	context.system=this;
	context.ffOnly=true;
	context.hwnd=hwnd;
	_dInput->EnumDevices
	(
		DI8DEVCLASS_GAMECTRL, EnumJoyCallback,&context,
		DIEDFL_FORCEFEEDBACK|DIEDFL_ATTACHEDONLY
	);
	if( !_devJoystick )
	{
		context.ffOnly=false;
		_dInput->EnumDevices
		(
			DI8DEVCLASS_GAMECTRL, EnumJoyCallback,&context,DIEDFL_ATTACHEDONLY
		);
	}

	for (int i=0; i<NButtons; i++ ) _buttonState[i] = false; 
	

	#endif
	// Was the force with us?
	#endif
	InitForceFeedback();

	DoSetStiffness(0.2,0.2);
	DoSetEngine(0,0);

}

/******************************************************************************
FUNCTION: EnumJoy
This function is called by the EnumDevices function in the IDirectInput2
interface.  It is used to enumerate existing joysticks on the system.  This
function looks for a force feedback capable joystick, and then will settle for
any installed joystick.

PARAMETERS:
lpddi -- Device instance information
pvRef -- LPVOID passed into the call to EnumDevices... used here to indicate
         whether we are trying to find force feedback joysticks.

RETURNS:
DIENUM_STOP -- If we find a joystick
DIENUM_CONTINUE -- If we haven't yet
******************************************************************************/
#if ENABLE_JOYSTICK
BOOL CALLBACK EnumJoyCallback(LPCDIDEVICEINSTANCE lpddi, LPVOID pvRef)
{
	EnumContext *context=static_cast<EnumContext *>(pvRef);
	return context->system->EnumJoy(lpddi,context->ffOnly,context->hwnd);
}
#endif

/*!
\patch_internal 1.01 Date 6/25/2001 by Ondra.
- Fixed: joystick support.
\patch 1.11 Date 7/31/2001 by Ondra.
- New: joystick support.
\patch 1.16 Date 8/9/2001 by Ondra.
- New: Steering wheel Force Feedback.
\patch 1.16 Date 8/10/2001 by Ondra.
- New: iFeel mouse force feedback support.
*/

#if ENABLE_JOYSTICK
BOOL Joystick::EnumJoy(LPCDIDEVICEINSTANCE lpddi, bool bEnumForce, HWND hwnd )
{
	DIPROPRANGE          dipRange;
	DIPROPDWORD          dipDWORD;

	// Are we enuming force feedback joysticks?
	// Can we create the device that wased passed in the lpddi pointer
	if
	(
	FAILED
	(
		_dInput->CreateDevice(lpddi->guidInstance,_devJoystick.Init(), NULL)
	)
	)
	{
		// keep trying
		_devJoystick = NULL;
		return DIENUM_CONTINUE;
	}

	// We are setting the data format for the device.  c_dfDIJoystick is a
	// predefined structure that should be used for joysticks.  It is defined
	// in dinput.lib
	_devJoystick->SetDataFormat(&c_dfDIJoystick);

	// We are setting the cooperative level to exclusive/foreground for this
	// object.  This means that as long as our app's window is in the
	// foreground, we have exclusive control of the device.  Consider using
	// exclusive background for debugging purposes.
	//_devJoystick->SetCooperativeLevel(hwnd,DISCL_EXCLUSIVE|DISCL_BACKGROUND);
	_devJoystick->SetCooperativeLevel(hwnd,DISCL_EXCLUSIVE|DISCL_FOREGROUND);

	// The SetProperty function of the IDirectInputDevice2 interface allows you
	// to set different settings for the device.  Here we are setting the min
	// and max ranges for the stick to -100 through 100 for the X and Y axes
	dipRange.diph.dwSize       = sizeof(dipRange);
	dipRange.diph.dwHeaderSize = sizeof(dipRange.diph);
	dipRange.lMin              = -100;
	dipRange.lMax              = +100;
	dipRange.diph.dwHow        = DIPH_BYOFFSET;

	dipRange.diph.dwObj        = DIJOFS_X;
	_devJoystick->SetProperty( DIPROP_RANGE, &dipRange.diph);

	dipRange.diph.dwObj        = DIJOFS_Y;
	_devJoystick->SetProperty( DIPROP_RANGE, &dipRange.diph);

	dipRange.diph.dwObj        = DIJOFS_Z;
	_devJoystick->SetProperty( DIPROP_RANGE, &dipRange.diph);

	dipRange.diph.dwObj        = DIJOFS_RX;
	_devJoystick->SetProperty( DIPROP_RANGE, &dipRange.diph);

	dipRange.diph.dwObj        = DIJOFS_RY;
	_devJoystick->SetProperty( DIPROP_RANGE, &dipRange.diph);

	dipRange.diph.dwObj        = DIJOFS_RZ;
	_devJoystick->SetProperty( DIPROP_RANGE, &dipRange.diph);

	dipRange.diph.dwObj        = DIJOFS_SLIDER(0);
	_devJoystick->SetProperty( DIPROP_RANGE, &dipRange.diph);

	dipRange.diph.dwObj        = DIJOFS_SLIDER(1);
	_devJoystick->SetProperty( DIPROP_RANGE, &dipRange.diph);

	// Here we are using SetProperty to set the deadzone to 1/5 of the joystick
	// range
	dipDWORD.diph.dwSize       = sizeof(dipDWORD);
	dipDWORD.diph.dwHeaderSize = sizeof(dipDWORD.diph);
	dipDWORD.diph.dwHow        = DIPH_BYOFFSET;
	dipDWORD.dwData            = 700;

	dipDWORD.diph.dwObj        = DIJOFS_X;
	_devJoystick->SetProperty( DIPROP_DEADZONE, &dipDWORD.diph);

	dipDWORD.diph.dwObj        = DIJOFS_Y;
	_devJoystick->SetProperty( DIPROP_DEADZONE, &dipDWORD.diph);

	dipDWORD.diph.dwObj        = DIJOFS_Z;
	_devJoystick->SetProperty( DIPROP_DEADZONE, &dipDWORD.diph);

	dipDWORD.diph.dwObj        = DIJOFS_RX;
	_devJoystick->SetProperty( DIPROP_DEADZONE, &dipDWORD.diph);

	dipDWORD.diph.dwObj        = DIJOFS_RY;
	_devJoystick->SetProperty( DIPROP_DEADZONE, &dipDWORD.diph);

	dipDWORD.diph.dwObj        = DIJOFS_RZ;
	_devJoystick->SetProperty( DIPROP_DEADZONE, &dipDWORD.diph);

   // If we were enumerating a force object then we made it... set the global
   // variable as such.
	_ffEnabled = bEnumForce;

	LogF
	(
		"Joystick detected: %s%s",
		lpddi->tszProductName,
		_ffEnabled ? " (Force feedback)" : ""
	);

	return DIENUM_STOP;
}
#endif
#include <Es/Strings/rstring.hpp>

static RString GetParamNames(DWORD pars)
{
	#if ENABLE_JOYSTICK
	RString ret = "";
	if (pars&DIEP_AXES) ret = ret + (RString)" Axes";
	if (pars&DIEP_DIRECTION) ret = ret + (RString)" Direction";
	if (pars&DIEP_DURATION) ret= ret + (RString)" Duration";
	if (pars&DIEP_ENVELOPE) ret= ret + (RString)" Envelope";
	if (pars&DIEP_GAIN) ret= ret + (RString)" Gain";
	if (pars&DIEP_SAMPLEPERIOD) ret= ret + (RString)" Sample period";
	if (pars&DIEP_STARTDELAY) ret= ret + (RString)" Startdelay"; 
	if (pars&DIEP_TRIGGERBUTTON) ret= ret + (RString)" Trigger button";
	if (pars&DIEP_TRIGGERREPEATINTERVAL) ret= ret + (RString)" Trigger repeat interval";
	if (pars&DIEP_TYPESPECIFICPARAMS) ret= ret + (RString)" Type specific params";
	return ret;
	#else
	return "";
	#endif
}

#if ENABLE_JOYSTICK
static BOOL CALLBACK DIEnumEffectsCallback(LPCDIEFFECTINFO ei, LPVOID pvRef)
{
    //HRESULT              hr;
    //LPDIRECTINPUTDEVICE8 lpdid = (LPDIRECTINPUTDEVICE8)pvRef;    
		Assert (ei->dwSize>=sizeof(DIEFFECTINFO));
 
    if (DIEFT_GETTYPE(ei->dwEffType) == DIEFT_CONSTANTFORCE)
    {
			LogF("Constant force %s",ei->tszName);
			LogF("  Static  %s",(const char *)GetParamNames(ei->dwStaticParams));
			LogF("  Dynamic %s",(const char *)GetParamNames(ei->dwDynamicParams));
    }
    if (DIEFT_GETTYPE(ei->dwEffType) == DIEFT_RAMPFORCE)
    {
			LogF("Ramp force %s",ei->tszName);
			LogF("  Static  %s",(const char *)GetParamNames(ei->dwStaticParams));
			LogF("  Dynamic %s",(const char *)GetParamNames(ei->dwDynamicParams));
    }
    if (DIEFT_GETTYPE(ei->dwEffType) == DIEFT_PERIODIC)
		{
			LogF("Periodic %s",ei->tszName);
			LogF("  Static  %s",(const char *)GetParamNames(ei->dwStaticParams));
			LogF("  Dynamic %s",(const char *)GetParamNames(ei->dwDynamicParams));
		}
    if (DIEFT_GETTYPE(ei->dwEffType) == DIEFT_CONDITION)
		{
			LogF("Condition %s",ei->tszName);
			LogF("  Static  %s",(const char *)GetParamNames(ei->dwStaticParams));
			LogF("  Dynamic %s",(const char *)GetParamNames(ei->dwDynamicParams));
		}
		 
 
    return DIENUM_CONTINUE;
}  // End of callback
#endif

/******************************************************************************
FUNCTION: InitForceFeedback
This function is called by InitDirectInput if a force feedback joystick has
been detected.  This function turns off auto center, and then calls the
individual functions to create effects.

PARAMETERS:
None

RETURNS:
Success or Failure
******************************************************************************/
void Joystick::InitForceFeedback()
{
	#if ENABLE_JOYSTICK
	if( !_devJoystick )
	{
		_ffEnabled=false;
		return;
	}

	_effWeaponStarted = false;
	_effEngineStarted = false;

	if( !_ffEnabled ) return;

	// enumerate effects
	//HRESULT hr =
	#if 0 //_DEBUG
		_devJoystick->EnumEffects(&DIEnumEffectsCallback, _devJoystick, DIEFT_ALL);
	#endif


	// SetProperty can be used to set various options for a device.  One of
	// them being DIPROP_AUTOCENTER, which we are disabling.
	#if 1
	DIPROPDWORD diPropAutoCenter;
	diPropAutoCenter.diph.dwSize = sizeof(diPropAutoCenter);
	diPropAutoCenter.diph.dwHeaderSize = sizeof(DIPROPHEADER);
	diPropAutoCenter.diph.dwObj = 0;
	diPropAutoCenter.diph.dwHow = DIPH_DEVICE;
	diPropAutoCenter.dwData = DIPROPAUTOCENTER_OFF;
	_devJoystick->SetProperty(DIPROP_AUTOCENTER,&diPropAutoCenter.diph);
	#endif

	if( FAILED(_devJoystick->Acquire()) ) _ffEnabled=false;
	InitEffects();

	FFOn(); // TODO: FFOn/Off from game UI

#endif
}

void Joystick::InitIFeel(HINSTANCE instance, HWND hwnd)
{
	#if IFEEL
	#if _ENABLE_REPORT
	//IFC_SET_ERROR_HANDLING( IFC_OUTPUT_ERR_TO_DEBUG | IFC_OUTPUT_ERR_TO_DIALOG ); 
	IFC_SET_ERROR_HANDLING(IFC_OUTPUT_ERR_TO_DEBUG); 

	#endif
	_iFeelMouse = new CImmMouse();

	if (_iFeelMouse)
	{
		BOOL ok = _iFeelMouse->Initialize(instance,hwnd);
		if (!ok)
		{
			_iFeelMouse.Free();
			return;
		}
		_iFeelEngine = new CImmPeriodic(GUID_Imm_Sine);
		_iFeelWeapon = new CImmPeriodic(GUID_Imm_Square);

		ok = _iFeelEngine->Initialize(_iFeelMouse,0,100,INFINITE);
		if (!ok) _iFeelEngine.Free();
		_iFeelEngineStarted = false;

		ok = _iFeelWeapon->Initialize(_iFeelMouse,0,30,INFINITE);
		if (!ok) _iFeelWeapon.Free();

	}
	#endif
}

static bool CheckStickActive()
{
	for (int i=0; i<JoystickNAxes; i++)
	{
		if (GInput.jAxisLastActive[i]>Glob.uiTime-15)
		{
			#if 0
			UITime maxTime = UITIME_MIN;
			for (int i=0; i<JoystickNAxes; i++)
			{
				if (maxTime<GInput.jAxisLastActive[i]) maxTime = GInput.jAxisLastActive[i];
			}
			//JoystickDev->GetForceFeedbackState(
			GlobalShowMessage(100,"%d: Active %.3f",i,Glob.uiTime-maxTime);
			LogF
			(
				"%.3f - %.3f, %d: Active %.3f",
				Glob.uiTime.toFloat(),maxTime.toFloat(),
				i,Glob.uiTime-maxTime
			);
			#endif
			return true;
		}
	}
	return false;
}

/******************************************************************************
FUNCTION: InitEffects
This function is called by InitForceFeedback to create all effects

******************************************************************************/
/*!
*/
void Joystick::InitEffects()
{
	#if ENABLE_JOYSTICK
	// What axes are we mapping for this effect to
	static DWORD dwAxes[1] = {DIJOFS_Y };
	static LONG lDirection[1] = {1};

	// parameters common to all effects
	DIEFFECT diEffect;
	memset(&diEffect,0,sizeof(diEffect));

	diEffect.dwSize = sizeof(DIEFFECT);
	diEffect.dwFlags = DIEFF_CARTESIAN | DIEFF_OBJECTOFFSETS;
	diEffect.dwSamplePeriod = 0;
	diEffect.dwGain = DI_FFNOMINALMAX;

	// both axes
	diEffect.cAxes = 1;
	diEffect.rgdwAxes = dwAxes;
	diEffect.rglDirection = lDirection;
	diEffect.lpEnvelope = NULL;

	diEffect.dwTriggerButton = DIEB_NOTRIGGER;
	diEffect.dwTriggerRepeatInterval = 0;

	// default infinite duration
	diEffect.dwDuration = INFINITE;

	// No envelope
	diEffect.lpEnvelope = NULL;

	DIPERIODIC  diPeriodic;

	// Set up the "Wavy" effect.
	// This effect will be periodic, so we will use the DIPERIODIC

	// Set the magnitude to half of max.
	diPeriodic.dwMagnitude = 0;

	// No offset or phase defined... we want it cycling around center
	// and we don't much care where it starts in the wave
	diPeriodic.lOffset = 0;
	diPeriodic.dwPhase = 0;
	diPeriodic.dwPeriod = DI_SECONDS;


	diEffect.cbTypeSpecificParams = sizeof(diPeriodic);
	diEffect.lpvTypeSpecificParams = &diPeriodic;

	// Create the effect and get an interface back in our pointer
	_devJoystick->CreateEffect(GUID_Sine, &diEffect,_effEngine.Init(), NULL);
	if (!_effEngine)
	{
		// if effect creation failed, chance is device has no Y axis
		dwAxes[0] = DIJOFS_X;
		_devJoystick->CreateEffect(GUID_Sine, &diEffect,_effEngine.Init(), NULL);
	}

	DICONSTANTFORCE  cf;
	cf.lMagnitude = 0;

	DIENVELOPE envelope;
	envelope.dwSize = sizeof(envelope);
	envelope.dwAttackTime = 0;
	envelope.dwFadeTime = INFINITE;
	envelope.dwFadeLevel = 0;
	envelope.dwAttackLevel = 0;

	diEffect.lpEnvelope = &envelope;
	diEffect.cbTypeSpecificParams = sizeof(cf);
	diEffect.lpvTypeSpecificParams = &cf;
	diEffect.dwDuration = INFINITE;

	#if 1
		_devJoystick->CreateEffect
		(
			GUID_ConstantForce, &diEffect,_effWeapon.Init(), NULL
		);
	#endif
	if (_effWeapon)
	{
		_weaponByConstantForce = true;
	}
	else
	{
		_weaponByConstantForce = false;

		DIPERIODIC per;
		per.dwMagnitude = 0;
		per.dwPhase = 0;
		per.dwPeriod = 20;
		per.lOffset = 0;

		diEffect.cbTypeSpecificParams = sizeof(per);
		diEffect.lpvTypeSpecificParams = &per;
		_devJoystick->CreateEffect
		(
			GUID_Square, &diEffect,_effWeapon.Init(), NULL
		);
	}

	DICONDITION cond[2];
	cond[0].dwNegativeSaturation = DI_FFNOMINALMAX;
	cond[0].dwPositiveSaturation = DI_FFNOMINALMAX;
	cond[0].lDeadBand = 0;
	cond[0].lOffset = 0;
	cond[0].lNegativeCoefficient = 0;
	cond[0].lPositiveCoefficient = 0;

	cond[1].dwNegativeSaturation = DI_FFNOMINALMAX;
	cond[1].dwPositiveSaturation = DI_FFNOMINALMAX;
	cond[1].lDeadBand = 0;
	cond[1].lOffset = 0;
	cond[1].lNegativeCoefficient = 0;
	cond[1].lPositiveCoefficient = 0;

	static DWORD condAxes[2] = { DIJOFS_X, DIJOFS_Y };
	static LONG condDirection[2] = {0,1};

	diEffect.cAxes = 2;
	diEffect.rgdwAxes = condAxes;
	diEffect.rglDirection = condDirection;
	diEffect.dwDuration = INFINITE;
	diEffect.dwGain = DI_FFNOMINALMAX;

	diEffect.cbTypeSpecificParams = sizeof(cond);
	diEffect.lpvTypeSpecificParams = cond;

	_devJoystick->CreateEffect(GUID_Spring, &diEffect,_effCenter.Init(), NULL);
	if (!_effCenter)
	{
		// failed: try one axis only
		diEffect.cAxes = 1;
		diEffect.cbTypeSpecificParams = sizeof(cond[0]);
		_devJoystick->CreateEffect(GUID_Spring, &diEffect,_effCenter.Init(), NULL);
	}
	_nCenterAxes = diEffect.cAxes;
	#endif
}

/*
XBox controller mapping:

button[0] = A
button[1] = B
button[2] = X
button[3] = Y
button[4] = DPAD_UP
button[5] = DPAD_DOWN
button[6] = DPAD_LEFT
button[7] = DPAD_RIGHT

button[8] = START
button[9] = BACK

button[10] = BLACK
button[11] = WHITE

button[12] = LEFT_TRIGGER;
button[13] = RIGHT_TRIGGER;

button[14] = LEFT_THUMB;
button[15] = RIGHT_THUMB;

button[16] = ThumbLX>0 (Right)
button[17] = ThumbLY>0 (Up)
button[18] = ThumbRX>0 (Right)
button[19] = ThumbRY>0 (Up)
button[20] = ThumbLX<0 (Left)
button[21] = ThumbLY<0 (Down)
button[22] = ThumbRX<0 (Left)
button[23] = ThumbRY<0 (Down)

*/

/*!
\param axis -1000..+1000 analog value
\param button 8b analog value, 0xff = fully pressed, 0x80 = half pressed
\param buttonEdge boolean value if button is pressed since last poll
*/

bool Joystick::Get
(
	int axis[NAxes], unsigned char button[NButtons], bool buttonEdge[NButtons],
	int &hat
)
{
	#if _XBOX
		memset(axis,0,NAxes*sizeof(*axis));
		// note: button must be permanent 
		memset(button,0,NButtons*sizeof(*button));
		memset(buttonEdge,0,NButtons*sizeof(*buttonEdge));
		hat = -1;

		if (_inputHandle)
		{
			XINPUT_STATE state;
			XInputGetState(_inputHandle,&state);
			// convert XBox controls to standard joystick controls
			// left hand and right hand is converted to four axes
			const XINPUT_GAMEPAD &pad = state.Gamepad;

			int thumbLX = pad.sThumbLX>>7;
			int thumbLY = pad.sThumbLY>>7;
			int thumbRX = pad.sThumbRX>>7;
			int thumbRY = pad.sThumbRY>>7;
			saturate(thumbLX,-255,255);
			saturate(thumbLY,-255,255);
			saturate(thumbRX,-255,255);
			saturate(thumbRY,-255,255);

			button[0] = pad.bAnalogButtons[XINPUT_GAMEPAD_A];
			button[1] = pad.bAnalogButtons[XINPUT_GAMEPAD_B];
			button[2] = pad.bAnalogButtons[XINPUT_GAMEPAD_X];
			button[3] = pad.bAnalogButtons[XINPUT_GAMEPAD_Y];

			button[4] = (pad.wButtons&XINPUT_GAMEPAD_DPAD_UP)!=0 ? 255 : 0;
			button[5] = (pad.wButtons&XINPUT_GAMEPAD_DPAD_DOWN)!=0 ? 255 : 0;
			button[6] = (pad.wButtons&XINPUT_GAMEPAD_DPAD_LEFT)!=0 ? 255 : 0;
			button[7] = (pad.wButtons&XINPUT_GAMEPAD_DPAD_RIGHT)!=0 ? 255 : 0;

			button[8] = (pad.wButtons&XINPUT_GAMEPAD_START)!=0 ? 255 : 0;
			button[9] = (pad.wButtons&XINPUT_GAMEPAD_BACK)!=0 ? 255 : 0;

			button[10] = pad.bAnalogButtons[XINPUT_GAMEPAD_BLACK];
			button[11] = pad.bAnalogButtons[XINPUT_GAMEPAD_WHITE];

			button[12] = pad.bAnalogButtons[XINPUT_GAMEPAD_LEFT_TRIGGER];
			button[13] = pad.bAnalogButtons[XINPUT_GAMEPAD_RIGHT_TRIGGER];

			button[14] = (pad.wButtons&XINPUT_GAMEPAD_LEFT_THUMB)!=0 ? 255 : 0;
			button[15] = (pad.wButtons&XINPUT_GAMEPAD_RIGHT_THUMB)!=0 ? 255 : 0;

			button[16] = thumbLX>0 ? thumbLX : 0;
			button[17] = thumbLY>0 ? thumbLY : 0;
			button[18] = thumbRX>0 ? thumbRX : 0;
			button[19] = thumbRY>0 ? thumbRY : 0;
			button[20] = thumbLX<0 ? -thumbLX : 0;
			button[21] = thumbLY<0 ? -thumbLY : 0;
			button[22] = thumbRX<0 ? -thumbRX : 0;
			button[23] = thumbRY<0 ? -thumbRY : 0;

			hat = -1;


			static const int tholds[24][2]=
			{
				// different button have different tresholds
				// when they are considered to be pressed
				// normal button tresholds
				{64,96},{64,96},{64,96},{64,96},
				{64,96},{64,96},{64,96},{64,96},
				{64,96},{64,96},{64,96},{64,96},
				{64,96},{64,96},{64,96},{64,96},
				// stick tresholds
				{96,144},{96,144},{96,144},{96,144},
				{96,144},{96,144},{96,144},{96,144},
			};

			for( int i=0; i<NButtons; i++ )
			{
				// if button is pressed, request lower treshold to consider it released again
				int thold = tholds[i][_buttonState[i]];
				bool nbi = button[i]>thold;
				
				buttonEdge[i]=( nbi && !_buttonState[i] );
				_buttonState[i] = nbi; 
			}
		}
	return true;
		// 
	#endif
	#if ENABLE_JOYSTICK
	HRESULT     hr;
	DIJOYSTATE  diJoyState;

	// No device object?  Give up.
	if(!_devJoystick) return false;

	CheckStickActive();

	// Starting from scratch
	memset(axis,0,NAxes*sizeof(*axis));
	// note: button must be permanent 
	memset(button,0,NButtons*sizeof(*button));

	// Poll, or retrieve information from the device
	hr = _devJoystick->Poll();
	if(hr== DIERR_INPUTLOST || hr== DIERR_NOTACQUIRED )
	{
		// If the poll failed, maybe we should acquire
		_devJoystick->Acquire();
		// When we lose control of the device we may need to re-download
		// the effects that are mapped to buttons, the other effects will
		if( _ffOn ) FFStart();
		// autodownload
		//if( _effect[Wave] ) _effect[Wave]->Download();
		//if( _effect[Ramp] ) _effect[Ramp]->Download();

		hr = _devJoystick->Poll();
	}
	if( FAILED(hr) ) return false;

	// Get the device state, and populate the DIJOYSTATE structure
	hr=_devJoystick->GetDeviceState(sizeof(DIJOYSTATE),&diJoyState);
	if( FAILED(hr) ) return false;

	// Map information here to our return variables.  It would be a trivial
	// addition to retrieve information such as rudder, hat, and throttle from
	// this structure.
	axis[0] = diJoyState.lX;
	axis[1] = diJoyState.lY;
	axis[2] = diJoyState.lZ;
	axis[3] = diJoyState.lRx;
	axis[4] = diJoyState.lRy;
	axis[5] = diJoyState.lRz;
	axis[6] = diJoyState.rglSlider[0];
	axis[7] = diJoyState.rglSlider[1];

	/*
	GlobalShowMessage
	(
		100,"%4d %4d %4d, %4d %4d %4d",
		diJoyState.lX,diJoyState.lY,diJoyState.lZ,
		diJoyState.lRx,diJoyState.lRy,diJoyState.lRz
	);
	*/

	hat = diJoyState.rgdwPOV[0];
	if( (hat&0xfffff)==0xffff ) hat=-1;
	if (hat!=-1)
	{
		hat /= DI_DEGREES;
	}
	for( int i=0; i<NButtons; i++ )
	{
		bool nbi = diJoyState.rgbButtons[i]!=0;
		
		buttonEdge[i]=( nbi && !_buttonState[i] );
		_buttonState[i] = nbi; 
		button[i] = nbi; 
	}
	return true;
	#else
	return false;
	#endif
}

void Joystick::FFStart()
{
	#if ENABLE_JOYSTICK
	//if( _effForce ) _effForce->Start(1,0);
	if( _effEngine && _effEngineStarted) _effEngine->Start(1,0);
	if( _effWeapon && _effWeaponStarted) _effWeapon->Start(1,0);
	if( _effCenter ) _effCenter->Start(1,0);
	#if IFEEL
	if (_iFeelEngine && _iFeelEngineStarted) _iFeelEngine->Start();
	#endif
	#endif
	// start / stop iFeel affects

}

void Joystick::FFStop()
{
	#if ENABLE_JOYSTICK
	if (_effEngine) _effEngine->Stop();
	if (_effWeapon) _effWeapon->Stop();
	//if( _effForce ) _effForce->Stop();
	#if IFEEL
	if (_iFeelWeapon) _iFeelWeapon->Stop();
	if (_iFeelEngine) _iFeelEngine->Stop();
	#endif
	#endif
}

void Joystick::FFOn()
{
	if (_ffOn) return;
	_ffOn=true;
	FFStart();
}

void Joystick::FFOff()
{
	if( _ffOn ) FFStop();
	_ffOn=false;
}

void DIError( const char *op, HRESULT err )
{
	#if ENABLE_JOYSTICK
	const char *name="???";
	char buf[256];
	switch( err )
	{
		case DI_OK: return;
		case S_FALSE: return;
		#define CODE(s) case s: name=#s; break;
		CODE(DI_EFFECTRESTARTED);
		CODE(DI_DOWNLOADSKIPPED);
		CODE(DI_TRUNCATED);
		CODE(DI_TRUNCATEDANDRESTARTED);
		CODE(DIERR_NOTINITIALIZED);
		CODE(DIERR_INCOMPLETEEFFECT);
		CODE(DIERR_INPUTLOST);
		CODE(DIERR_INVALIDPARAM);
		CODE(DIERR_EFFECTPLAYING);
		default:
		{
			FormatMessage
			(
				FORMAT_MESSAGE_FROM_SYSTEM,
				NULL, // source
				err, // requested message identifier 
				0, // languague
				buf,sizeof(buf),
				NULL
			);
			char *eol=strrchr(buf,'\n');
			if( eol==buf+strlen(buf)-1 ) *eol=0;
			name=buf;
		}
		break;
	}
	if( name ) LogF("FF %s: %x - %s",op,err,name);
	#endif
}

#include "global.hpp"

// used to simulate weapons
void Joystick::PlayRampIFeel(float begMag, float endMag, float duration)
{
	#if IFEEL
	if (!_iFeelWeapon)
	{
		return;
	}
	/*
	GlobalShowMessage(100,"Mag %.3f..%.3f",begMag,begMag);
	LogF("%.2f: Mag %.3f..%.3f",Glob.time.toFloat(),begMag,begMag);
	*/

	// Constant force, full strength
	if( fabs(begMag)+fabs(endMag)<1e-3 )
	{
		_iFeelWeapon->Stop();
		return;
	}

	int iBegMag=toIntFloor(begMag*20000);
	int iEndMag=toIntFloor(endMag*20000);
	int iDuration = toLargeInt(1000*duration);

	saturate(iBegMag,1,10000);
	saturate(iEndMag,1,10000);

	IMM_ENVELOPE envelope;

	envelope.dwSize = sizeof(envelope);
	envelope.dwAttackTime = iDuration;
	envelope.dwFadeTime = INFINITE;
	envelope.dwFadeLevel = iEndMag;
	envelope.dwAttackLevel = iBegMag;


	_iFeelWeapon->ChangeParameters
	(
		iEndMag,
		IMM_EFFECT_DONT_CHANGE,
		IMM_EFFECT_DONT_CHANGE,
		IMM_EFFECT_DONT_CHANGE,
		IMM_EFFECT_DONT_CHANGE,
		IMM_EFFECT_DONT_CHANGE,
		IMM_EFFECT_DONT_CHANGE,
		&envelope
	);
	_iFeelWeapon->Start();
	#endif
}

const int maxIFeelMag = 10000;

void Joystick::DoStopEngineIFeel()
{
	if (!_iFeelEngine) return;
	if (_iFeelEngineStarted)
	{
#if ENABLE_JOYSTICK
		_iFeelEngine->Stop();
#endif
		_iFeelEngineStarted = false;
	}
}

void Joystick::DoSetEngineIFeel( float mag, float freq )
{
	#if IFEEL
	if (!_iFeelEngine) return;
	int iMag = toInt(mag*maxIFeelMag);
	saturate(freq,1,500);
	int iPer = toInt(1000/freq);
	//GlobalShowMessage(100,"Mag %d, per %d",iMag,iPer);
	if (iMag<10 || iPer>500)
	{
		DoStopEngineIFeel();
		return;

	}
	_iFeelEngine->ChangeMagnitude(iMag);
	_iFeelEngine->ChangePeriod(iPer);
	if (!_iFeelEngineStarted)
	{
		_iFeelEngine->Start();
		_iFeelEngineStarted = true;
	}
	#endif
}



void Joystick::PlayRamp(float begMag, float endMag, float duration)
{
	#if ENABLE_JOYSTICK
	if (_iFeelMouse)
	{
		// check if user is holding mouse
		if 
		(
			GInput.mouseCursorLastActive<Glob.uiTime-5
			// && GInput.mouseCursorLastActive<GInput.keyboardCursorLastActive
		)
		{
			// if mouse did not move for a long time, user is probably not using it
			// we will switch ff effects off
			PlayRampIFeel(0,0,0);
		}
		else
		{
			PlayRampIFeel(begMag,endMag,duration);
		}
	}
	// play ramp
	if (!_effWeapon) return;

	// check if some axis has been active recently
	bool active = CheckStickActive();

	// Constant force, full strength
	float mag2=Square(begMag)+Square(endMag);
	if( mag2<1e-6 || !active)
	{
		#if 0
		DWORD status;
		_effWeapon->GetEffectStatus(&status);
		if(status&DIEGES_PLAYING)
		{
			LogF("%.2f: Cancel constant force",Glob.time.toFloat());
		}
		#endif
		_effWeapon->Stop();
		_effWeaponStarted = false;
		return;
	}

	int iBegMag=toIntFloor(begMag*DI_FFNOMINALMAX);
	int iEndMag=toIntFloor(endMag*DI_FFNOMINALMAX);
	int iDuration = toLargeInt(DI_SECONDS*duration);

	saturate(iBegMag,1,DI_FFNOMINALMAX);
	saturate(iEndMag,1,DI_FFNOMINALMAX);

	//DICONSTANTFORCE  diPars = { iBegMag, iEndMag };
	static LONG  lDirection[1] = {1};
	static DWORD dwAxes[1] = { DIJOFS_Y };

	DICONSTANTFORCE  cf;
	DIPERIODIC per;
	DIEFFECT diEffect;
	memset(&diEffect,0,sizeof(diEffect));
	diEffect.dwSize = sizeof(diEffect);

	if (_weaponByConstantForce)
	{

		// The beginning and the end values for our ramp force
		cf.lMagnitude = endMag;
		diEffect.cbTypeSpecificParams = sizeof(cf);
		diEffect.lpvTypeSpecificParams = &cf;
	}
	else
	{
		per.dwMagnitude = endMag;
		per.lOffset = 0;
		per.dwPhase = 0;
		per.dwPeriod = 20;
		diEffect.cbTypeSpecificParams = sizeof(per);
		diEffect.lpvTypeSpecificParams = &per;
	}


	DIENVELOPE envelope;
	envelope.dwSize = sizeof(envelope);
	envelope.dwAttackTime = iDuration;
	envelope.dwFadeTime = INFINITE;
	envelope.dwFadeLevel = iEndMag;
	envelope.dwAttackLevel = iBegMag;



	diEffect.cAxes = 1;
	diEffect.dwFlags = DIEFF_CARTESIAN | DIEFF_OBJECTOFFSETS;
	diEffect.dwTriggerButton = -1;
	diEffect.dwGain = DI_FFNOMINALMAX;
	diEffect.lpEnvelope = &envelope;

	diEffect.rgdwAxes = dwAxes;
	diEffect.rglDirection = lDirection;
	diEffect.dwDuration = INFINITE; // after attack stay on ending level

	#if 0
	LogF
	(
		"%.2f: Set constant envelope: %d..%d, time %d",
		Glob.time.toFloat(),iBegMag,iEndMag,iDuration
	);
	#endif
	// Get an interface to our new effect
	HRESULT err=_effWeapon->SetParameters
	(
		&diEffect,
		//DIEP_DIRECTION|
		DIEP_DURATION|DIEP_ENVELOPE|
		DIEP_TYPESPECIFICPARAMS|DIEP_START
	);
	if (err<DI_OK)
	{
		DIError("Force SetParameters",err);
	}
	_effWeaponStarted = true;
	#endif
}

/*!
\patch 1.93 Date 9/15/2003 by Ondra
- Fixed: Autocentering force did not work properly on some joysticks including MS Sidewinder FF 2.
*/


// weapon effects

void Joystick::SetStiffness(float valueX, float valueY)
{
	float thold = 0.05f;
	if (fabs(valueX-_stiffnessX)>thold || fabs(valueY-_stiffnessY)>=thold)
	{
		DoSetStiffness(valueX,valueY);
	}
}

void Joystick::DoSetStiffness(float valueX, float valueY)
{
	#if ENABLE_JOYSTICK
	_stiffnessX = valueX;
	_stiffnessY = valueY;
	if (!_effCenter) return;

	DIEFFECT diEffect;
	memset(&diEffect,0,sizeof(diEffect));
	diEffect.dwSize = sizeof(DIEFFECT);
	diEffect.dwFlags = DIEFF_CARTESIAN | DIEFF_OBJECTOFFSETS;

	// how much autocentering is applied
	int iValX = toInt(DI_FFNOMINALMAX*valueX);
	int iValY = toInt(DI_FFNOMINALMAX*valueY);

	saturate(iValX,0,DI_FFNOMINALMAX);
	saturate(iValY,0,DI_FFNOMINALMAX);

	DICONDITION cond[2];
	cond[0].dwNegativeSaturation = DI_FFNOMINALMAX;
	cond[0].dwPositiveSaturation = DI_FFNOMINALMAX;
	cond[0].lDeadBand = 0;
	cond[0].lOffset = 0;
	cond[0].lNegativeCoefficient = iValX;
	cond[0].lPositiveCoefficient = iValX;

	cond[1].dwNegativeSaturation = DI_FFNOMINALMAX;
	cond[1].dwPositiveSaturation = DI_FFNOMINALMAX;
	cond[1].lDeadBand = 0;
	cond[1].lOffset = 0;
	cond[1].lNegativeCoefficient = iValY;
	cond[1].lPositiveCoefficient = iValY;

	static DWORD condAxes[2] = { DIJOFS_X, DIJOFS_Y };
	static LONG condDirection[2] = {0,1};

	diEffect.cAxes = _nCenterAxes;
	diEffect.rgdwAxes = condAxes;
	diEffect.rglDirection = condDirection;

	diEffect.cbTypeSpecificParams = sizeof(*cond)*_nCenterAxes;
	diEffect.lpvTypeSpecificParams = cond;

	HRESULT err = _effCenter->SetParameters(&diEffect,DIEP_TYPESPECIFICPARAMS|DIEP_START);
	if (err<DI_OK)
	{
		DIError("_effCenter SetParameters",err);
	}
	#endif
}

void Joystick::SetEngine( float mag, float freq )
{
	if (_iFeelMouse)
	{
		if (GInput.mouseCursorLastActive<Glob.uiTime-5)
		{
			DoStopEngineIFeel();
		}
	}

	if (fabs(mag-_engineMag)>0.02 || fabs(freq-_engineFreq)>5)
	{
		DoSetEngine(mag,freq);
	}
}

void Joystick::DoSetEngine( float mag, float freq )
{
	#if ENABLE_JOYSTICK
	_engineMag = mag;
	_engineFreq = freq;
	// set constant sine
	if (_iFeelMouse)
	{
		if (GInput.mouseCursorLastActive<Glob.uiTime-5)
		{
			DoStopEngineIFeel();
		}
		else
		{
			DoSetEngineIFeel(mag,freq);
		}
	}

	if( !_effEngine ) return;

	// Constant force, full strength

	bool active = CheckStickActive();
	if( mag<1e-3 || !active)
	{
		_effEngine->Stop();
		_effEngineStarted = false;
		return;
	}

	int iMag=toIntFloor(mag*DI_FFNOMINALMAX);
	saturateMin(iMag,DI_FFNOMINALMAX);

	DIPERIODIC  diSpec;
	diSpec.dwMagnitude = iMag;
	diSpec.lOffset = 0;
	diSpec.dwPhase = 0;
	diSpec.dwPeriod = toLargeInt(DI_SECONDS/freq);

	//LogF("Engine %5d,%5d : %5d",iDirX,iDirY,iMag);

	LONG             lDirection[1] = { 1 };

	DIEFFECT         diEffect;
	memset(&diEffect,0,sizeof(diEffect));

	diEffect.dwSize = sizeof(DIEFFECT);
	diEffect.dwFlags = DIEFF_CARTESIAN | DIEFF_OBJECTOFFSETS;
	//diEffect.dwSamplePeriod = 0;
	//diEffect.dwGain = DI_FFNOMINALMAX;

	diEffect.cAxes = 1;
	diEffect.rglDirection = lDirection;

	diEffect.cbTypeSpecificParams = sizeof(diSpec);
	diEffect.lpvTypeSpecificParams = &diSpec;

	// Get an interface to our new effect
	HRESULT err=_effEngine->SetParameters
	(
		&diEffect,
		//DIEP_DIRECTION|
		DIEP_TYPESPECIFICPARAMS
	);
	DIError("Engine SetParameters",err);
	DWORD status;
	_effEngine->GetEffectStatus(&status);
	if( (status&DIEGES_PLAYING)==0 )
	{
		err=_effEngine->Start(1,0);

		DIError("Engine Start",err);
	}
	_effEngineStarted = true;
	#endif
}

Joystick::~Joystick()
{
	_iFeelEngine.Free();
	_iFeelWeapon.Free();
	_iFeelMouse.Free();
	//Basic rule of thumb:  If it exists... release it
	#if _XBOX
		if (_inputHandle) XInputClose(_inputHandle), _inputHandle = NULL;
	#endif
	#if ENABLE_JOYSTICK
	if( _devJoystick )
	{
		_devJoystick->Acquire();

		FFOff();

		if( _effEngine ) _effEngine->Unload(),_effEngine.Free();
		//if( _effGun ) _effGun->Unload(),_effGun.Free();
		//if( _effForce ) _effForce->Unload(),_effForce.Free();
		if( _effWeapon ) _effWeapon->Unload(),_effWeapon.Free();
		if( _effCenter ) _effCenter->Unload(),_effCenter.Free();

		_devJoystick->Unacquire();

		DIPROPDWORD diPropAutoCenter;
		diPropAutoCenter.diph.dwSize = sizeof(diPropAutoCenter);
		diPropAutoCenter.diph.dwHeaderSize = sizeof(DIPROPHEADER);
		diPropAutoCenter.diph.dwObj = 0;
		diPropAutoCenter.diph.dwHow = DIPH_DEVICE;
		diPropAutoCenter.dwData = DIPROPAUTOCENTER_ON;
		_devJoystick->SetProperty(DIPROP_AUTOCENTER,&diPropAutoCenter.diph);

		_devJoystick.Free();
	}

	_dInput.Free();
	#endif
}
