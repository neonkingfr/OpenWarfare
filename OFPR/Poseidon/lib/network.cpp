#include "wpch.hpp"
#include "networkImpl.hpp"
#include "global.hpp"
//#include "strIncl.hpp"
#include "stringtableExt.hpp"

#include "arcadeTemplate.hpp"
#include "ai.hpp"
#include "world.hpp"
#include "landscape.hpp"
#include "chat.hpp"
#include <El/XML/xml.hpp>
#include "strFormat.hpp"

#if defined _WIN32 && !defined _XBOX
  #include <dxerr8.h>
  #pragma comment(lib,"dxerr8")
#endif

#include <Es/Algorithms/qsort.hpp>

#include "allAIVehicles.hpp"
#include "seaGull.hpp"
#include "detector.hpp"
#include "shots.hpp"

#include "debugTrap.hpp"

#include <El/QStream/QBStream.hpp>
#include "packFiles.hpp"
#include "fileServer.hpp"

#include <El/Common/randomGen.hpp>
#include "gameStateExt.hpp"

#include "progress.hpp"
#include <El/Common/perfLog.hpp>

#include "keyInput.hpp"
#include "dikCodes.h"
#include "uiActions.hpp"

#include "crc.hpp"

#ifdef _WIN32
  #include <io.h>
#endif

#include "camera.hpp"

#include "mbcs.hpp"

/*!
\file
Basic implementation file for multiplayer game
	\patch 1.01 Date 07/06/2001 by Ondra
	- Fixed: verions number changed from 1 to 101.
	- MP server version should reflect the exe version. As it is integer,
	it is neccessary to use different numbering format.
*/

//! Use private memory heap for system messages
#define USE_PRIVATE_HEAP 1

template Ref<NetworkObject>;

#if _ENABLE_CHEATS

//! current diagnostic type
/*!
- 0: no diagnostics
- 1: basic statistics
- 2: outgoing messages
- 3: incomming messages
*/
int outputDiags = 0;
//! enable general diagnostic logs
bool outputLogs = false;
#endif

/*!
\patch 1.27 Date 10/16/2001 by Jirka
- Added: some network adjustments can be made in Flashpoint.cfg
 MaxMsgSend - maximal number of messages sent in single simulation step (default 26)
 MaxSizeGuaranteed - preferred maximal size of guaranteed packet without header (default 512)
 MaxSizeNonguaranteed - preferred maximal size of nonguaranteed packet without header (default 256)
 MinBandwidth - limit (minimum) for bandwidth estimation (default 28800)
 MaxBandwidth - limit (maximum) for bandwidth estimation (default no limit)
\patch 1.33 Date 12/03/2001 by Jirka
- Added: different default values for MaxMsgSend (128) and MinBandwidth (131072) on dedicated server
*/

/*!
\patch 1.53 Date 4/23/2002 by Ondra
- Fixed: Min and MaxBadwidth were undestood as if given in Bytes per second (Bps).
Accordingly to documentation and general undestanting it should be bits per second (bps).
*/


int MaxMsgSend = 26;
int DSMaxMsgSend = 128;
int MaxSizeGuaranteed = 512;
int MaxSizeNonguaranteed = 256;
int MinBandwidth = 28800;
int DSMinBandwidth = 131072;
int MaxBandwidth = INT_MAX;
/*!
\patch 1.82 Date 8/16/2002 by Ondra
- Improved: MP: Minimal error to send updates across network can be now
adjusted by MinErrorToSend value in Flashpoint.cfg. Default value is 0.01.
Using smaller value can make units observed by binoculars or sniper rifle
to move smoother.
*/
float MinErrorToSend = 0.01f;
/*!
\patch 1.87 Date 10/17/2002 by Ondra
- Fixed: An attempt to fix latency problems:
ThrottleGuaranteed is now 1, as it was in 1.75, not 10, as in 1.85.
Moreover, it can be configured in Flashpoint.cfg by adding ThrottleGuaranteed=value.
\patch 1.88 Date 10/21/2002 by Ondra
- Fixed: Another attempt to fix latency problems.
Support for ThrottleGuaranteed is removed completely and corrsponding code
is returned as it was in 1.75.
*/

float ThrottleGuaranteed = 1.0f;

//! Check consistency of local objects
#define CHECK_MSG	0

//! Temporary directory for Network server (temporary directory must be different for different dedicated servers)
RString ServerTmpDir;
//! Return temporary directory for Network server
RString GetServerTmpDir()
{
	return ServerTmpDir;
}

unsigned int CalculateConfigCRC(const char *path);
unsigned int CalculateExeCRC(int offset, int size);
unsigned int CalculateDataCRC(const char *path, int offset, int size);

#define CLIENT_CRC_KNOWN 0
#if CLIENT_CRC_KNOWN
  static const ExeCRCBlock ToCheck[] =
  {
    {0x0042a000,0x0400,0xf39fef09}, // ID changer
    {0x00427c00,0x0400,0x4aca1424},
    {0x00428000,0x0400,0xed59f759},
    {0x00428400,0x0400,0x3fe4da96},
    {0x00435800,0x0400,0x212315ef}, // ammo cheat
    {0,0,0},
  };
  /// check known valid client crc
  static int GetReferenceCRC(int offset, int size)
  {
    for (const ExeCRCBlock *crc=ToCheck; crc->size>0; crc++)
    {
      if (crc->offset==offset && crc->size==size)
      {
        return crc->crc;
      }
    }
    return 0;
  }
  const ExeCRCBlock *GetCodeCheck() {return ToCheck;}

#else
  static const ExeCRCBlock ToCheck[] =
  {
    {0,0,0}
  };
  const ExeCRCBlock *GetCodeCheck() {return ToCheck;}
  /// check known valid client crc
  static int GetReferenceCRC(int offset, int size)
  {
    return CalculateExeCRC(offset,size);
  }
#endif


//! Calculate answer to any integrity question
/**
@param server do not check real exe, check stored CRC instead
*/
unsigned int IntegrityCheckAnswer
(
	IntegrityQuestionType type, const IntegrityQuestion &q, bool server
)
{
	switch (type)
	{
		case IQTConfig:
			return CalculateConfigCRC(q.name);
		case IQTExe:
      if (server)
      {
  			return GetReferenceCRC(q.offset,q.size);
      }
			return CalculateExeCRC(q.offset,q.size);
		case IQTData:
			return CalculateDataCRC(q.name,q.offset,q.size);
	}
	return 0;
}

//! General diagnostics level
/*!
- level == 0 - nothing
- level == 1 - errors
- level == 2 - basic statictics
- level == 3 - extended statistics
- level == 4 - warnings
*/
const int DiagLevel = 0;

//! Return diagnostics level for given message type
int GetDiagLevel(NetworkMessageType type, bool remote)
{
// level == 0 - nothing
// level == 1 - errors
// level == 2 - message headers
// level == 3 - message bodies
// level == 4 - all
	switch (type)
	{
/*
	case NMTMsgFormat:
	case NMTMsgFormatItem:
	case NMTPlayer:
	case NMTMessages:
	case NMTLogin:
	case NMTLogout:
	case NMTSquad:
	case NMTMissionHeader:
	case NMTPlayerSide:
	case NMTPlayerRole:
	case NMTSelectPlayer:
	case NMTAttachPerson:
	case NMTTransferFile:
	case NMTAskMissionFile:
	case NMTTransferMissionFile:
	case NMTTransferFileToServer:
	case NMTPublicVariable:
	case NMTRadioChatWave:
	case NMTSetVoiceChannel:
	case NMTSetSpeaker:
	case NMTAskForGetIn:
	case NMTAskForGetOut:
	case NMTAskForChangePosition:
	case NMTAskForDammage:
	case NMTAskForSetDammage:
	case NMTAskForAmmo:
	case NMTAskForAddImpulse:
	case NMTAskForMoveVector:
	case NMTAskForMoveMatrix:
	case NMTAskForJoinGroup:
	case NMTAskForJoinUnits:
	case NMTExplosionDammageEffects:
	case NMTFireWeapon:
	case NMTUpdateWeapons:
	case NMTAddWeaponCargo:
	case NMTRemoveWeaponCargo:
	case NMTAddMagazineCargo:
	case NMTRemoveMagazineCargo:
	case NMTVehicleInit:
	case NMTVehicleDestroyed:
	case NMTMarkerCreate:
	case NMTMarkerDelete:
//	case NMTRespawn:
	case NMTSetFlagOwner:
	case NMTSetFlagCarrier:
	case NMTMsgVTarget:
	case NMTMsgVFire:
	case NMTMsgVMove:
	case NMTMsgVFormation:
	case NMTMsgVSimpleCommand:
	case NMTMsgVLoad:
	case NMTMsgVAzimut:
	case NMTMsgVStopTurning:
	case NMTMsgVFireFailed:
	case NMTUpdateObject:
	case NMTCreateVehicle:
	case NMTUpdateVehicle:
	case NMTUpdatePositionVehicle:
	case NMTCreateAICenter:
	case NMTUpdateAICenter:
	case NMTCreateAIGroup:
	case NMTUpdateAIGroup:
	case NMTWaypoint:
	case NMTCreateAISubgroup:
	case NMTUpdateAISubgroup:
	case NMTCreateAIUnit:
	case NMTUpdateAIUnit:
	case NMTCreateCommand:
	case NMTUpdateCommand:
	case NMTUpdateVehicleAI:
	case NMTUpdateVehicleBrain:
	case NMTUpdateVehicleSupply:
	case NMTUpdateTransport:
	case NMTUpdateMan:
	case NMTUpdatePositionMan:
	case NMTUpdateTankOrCar:
	case NMTUpdateCar:
	case NMTUpdatePositionCar:
	case NMTUpdateAirplane:
	case NMTUpdatePositionAirplane:
	case NMTUpdateHelicopter:
	case NMTUpdatePositionHelicopter:
	case NMTUpdateShip:
	case NMTUpdatePositionShip:
	case NMTUpdateSeagull:
	case NMTUpdatePositionSeagull:
	case NMTUpdateShot:
	case NMTCreateDetector:
	case NMTUpdateDetector:
	case NMTUpdateFlag:
	case NMTGameState:
	case NMTChangeOwner:
	case NMTCreateObject:
	case NMTDeleteObject:
	case NMTDeleteCommand:
	case NMTAskForAimWeapon:
	case NMTAskForAimObserver:
	case NMTAskForSelectWeapon:
	case NMTUpdateTank:
	case NMTUpdatePositionTank:
	case NMTUpdateTurret:
	case NMTCreateShot:
	case NMTCreateExplosion:
	case NMTCreateCrater:
	case NMTCreateCraterOnVehicle:
	case NMTCreateObjectDestructed:
	case NMTMagazine:
	case NMTPathPoint:
	case NMTAskForHideBody:
	case NMTNetworkCommand:
	case NMTIntegrityQuestion:
	case NMTIntegrityAnswer:
	case NMTPlayerState:
	case NMTPlayerUpdate:
	case NMTCreateHelicopter:
	case NMTUpdateClientInfo:
	case NMTShowTarget:
	case NMTShowGroupDir:
	case NMTGroupSynchronization:
	case NMTDetectorActivation:
	case NMTAskForCreateUnit:
	case NMTAskForDeleteVehicle:
	case NMTAskForGroupRespawn:
	case NMTCopyUnitInfo:
	case NMTGroupRespawnDone:
	case NMTMissionParams:
	case NMTUpdateMine:
	case NMTAskForActivateMine:
	case NMTVehicleDamaged:
	case NMTUpdateFireplace:
	case NMTAskForInflameFire:
	case NMTAskForAnimationPhase:
	case NMTIncomingMissile:

*/
	default:
	case NMTUpdateSeagull:
		return 0;
	/*
	case NMTUpdateDammageObject:
	case NMTUpdateDammageVehicleAI:
	case NMTChat:
	case NMTRadioChat:
	*/
	/*
	case NMTCreateObject:
	case NMTCreateDetector:
	case NMTCreateExplosion:
	case NMTCreateCrater:
	case NMTCreateCraterOnVehicle:
	case NMTCreateObjectDestructed:
	case NMTCreateAICenter:
	case NMTCreateAIGroup:
	case NMTCreateAISubgroup:
	case NMTCreateAIUnit:
	case NMTCreateCommand:
	case NMTCreateHelicopter:
	*/
	/*
	case NMTCreateVehicle:
	case NMTCreateShot:
	case NMTUpdateDammageObject:
	case NMTUpdateShot:
	case NMTDeleteObject:
	*/
// copy here to log remote messages (transferred via DPlay)
//	case NMTGameState:
		return remote ? 3 : 0;
	}
}

//! Names of message types (used for diagnostics output)
#define NMT_DEFINE_ENUM_NAMES(macro, class, name, description, group) #name,

const char *NetworkMessageTypeNames[NMTN] =
{
	NETWORK_MESSAGE_TYPES(NMT_DEFINE_ENUM_NAMES)
};

//! Diagnostics logs in single simulation step 
AutoArray<RString> DiagOutput;

//! Write diagnostics logs to output
void WriteDiagOutput(bool server)
{
	if (DiagOutput.Size() == 0) return;

	if (server)
		LogF("Server simulation, time = %.3f, %d rows", Glob.time.toFloat(), DiagOutput.Size());
	else
		LogF("Client simulation, time = %.3f, %d rows", Glob.time.toFloat(), DiagOutput.Size());
	for (int i=0; i<DiagOutput.Size(); i++)
		LogF(DiagOutput[i]);
	LogF("");
	DiagOutput.Clear();
}

//! Write single line of text into file
static void WriteToFile(HANDLE file, RString string)
{
	DWORD written;
	WriteFile(file, string.Data(), string.GetLength(), &written, 0);
	WriteFile(file, "\r\n", 2, &written, 0);
}

//! Write diagnostics logs into file
void PrintNetworkInfo(HANDLE file)
{
	if (DiagOutput.Size() == 0) return;
	
	WriteToFile(file, "Pending network output:");
	for (int i=0; i<DiagOutput.Size(); i++)
		WriteToFile(file, DiagOutput[i]);
}

//! Write message into diagnostics log
void DiagLogF( const char *format, ... )
{
	char buf[512];
		
	va_list arglist;
	va_start( arglist, format );
	vsprintf(buf,format,arglist);
	va_end( arglist );

	DiagOutput.Add(buf);
}

//! Destroy file bank with given prefix
void RemoveBank(const char *prefix)
{
	int prefixLen = strlen(prefix);
	for (int i=0; i<GFileBanks.Size();)
	{
		QFBank &bank = GFileBanks[i];
		if (strnicmp(bank.GetPrefix(), prefix, prefixLen) == 0)
		{
			GFileServer->FlushBank(&bank);
			GFileBanks.Delete(i);
		}
		else
			i++;
	}
}

//! Create file bank for multiplayer mission
/*!
\param filename bank name without extension (.pbo)
\param island island where mission acts
*/
RString CreateMPMissionBank(RString filename, RString island)
{
	// remove bank
	const char *prefix = "mpmissions\\__cur_mp.";
	RemoveBank(prefix);

	// create bank
	int index = GFileBanks.Add();
	QFBank &bank = GFileBanks[index];
	bank.open(filename);
	RString str = 
		RString(prefix) + island + RString("\\");
	str.Lower();
	bank.SetPrefix(str);
	return str;
}

NetworkMessageType NetworkSimpleObject::GetNMType(NetworkMessageClass cls) const
{
	return NMTNone;
}

IndicesNetworkObject::IndicesNetworkObject()
{
	objectCreator = -1;
	objectId = -1;
	objectPosition = -1;
	guaranteed = -1;
}

void IndicesNetworkObject::Scan(NetworkMessageFormatBase *format)
{
	SCAN(objectCreator)
	SCAN(objectId)
	SCAN(objectPosition)
	SCAN(guaranteed)
}

//! Create network message indices for NetworkObject class
NetworkMessageIndices *GetIndicesNetworkObject() {return new IndicesNetworkObject();}

NetworkObject::NetworkObject()
{
	_lastUpdateTime = TIME_MIN;
	_maxPredictionTime = TIME_MIN;
}

/*!
\patch 1.26 Date 10/05/2001 by Ondra
- Fixed: MP: client side prediction is now limited to short time only.
*/

float NetworkObject::GetMaxPredictionTime(NetworkMessageContext &ctx) const
{
	// default: if we do not receive any update for 10 seconds
	// assume object in not doing anything (otherwise we would get an update)
	return 10;
}

/*
\patch 1.79 Date 7/26/2002 by Ondra
- New: MP: Units disconnected from server are now frozen.
*/

bool NetworkObject::CheckPredictionFrozen() const
{
	if (!IsLocal()) return Glob.time>_maxPredictionTime;
	return GetNetworkManager().IsControlsPaused();
}

NetworkMessageFormat &NetworkObject::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("objectCreator", NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("ID of client, which created this object"));
	format.Add("objectId", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Unique (for client) ID of object"));
	format.Add("objectPosition", NDTVector, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Current position of object"));
	format.Add("guaranteed", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Message is guaranteed (must be delivered)"));
	return format;
}

TMError NetworkObject::TransferMsg(NetworkMessageContext &ctx)
{
	if (ctx.IsSending())
	{
		// TESTING - Replace by Assert
		DoAssert(dynamic_cast<const IndicesNetworkObject *>(ctx.GetIndices()))
		const IndicesNetworkObject *indices = static_cast<const IndicesNetworkObject *>(ctx.GetIndices());

		NetworkId id = GetNetworkId();
		TMCHECK(ctx.IdxTransfer(indices->objectCreator, id.creator))
		TMCHECK(ctx.IdxTransfer(indices->objectId, id.id))
		Vector3 position = GetCurrentPosition();
		TMCHECK(ctx.IdxTransfer(indices->objectPosition, position))
	}
	else
	{
		if (ctx.GetClass()==NMCUpdatePosition)
		{
			_lastUpdateTime = Glob.time;
			_maxPredictionTime = _lastUpdateTime + GetMaxPredictionTime(ctx);
		}
	}
	return TMOK;
}

float NetworkObject::CalculateError(NetworkMessageContext &ctx)
{
	switch (ctx.GetClass())
	{
	case NMCUpdatePosition:
		return ERR_COEF_TIME_POSITION * (Glob.time - ctx.GetMsgTime());
	default:
		return ERR_COEF_TIME_GENERIC * (Glob.time - ctx.GetMsgTime());
	}
}

float NetworkObject::CalculateErrorCoef(Vector3Par position, Vector3Par cameraPosition)
{
	// undefined camera position
	if (cameraPosition[0] == -FLT_MAX) return 1.0;
	
	// coef = limit / distance (maximum is 1)
	const float limit = 20.0f;
	float dist2 = position.Distance2(cameraPosition);
	if (dist2 <= Square(limit)) return 1.0;
	else
	{
		return Square(limit) / dist2;
	}
}

NetworkMessageType ClientInfoObject::GetNMType(NetworkMessageClass cls) const
{
	switch (cls)
	{
	case NMCUpdateGeneric:
		return NMTUpdateClientInfo;
	default:
		return NMTNone;
	}
}

//! network message indices for ClientInfoObject class
class IndicesUpdateClientInfo : public IndicesNetworkObject
{
	typedef IndicesNetworkObject base;

public:
	//@{
	//! index of field in message format
	int cameraPosition;
	//@}

	//! Constructor
	IndicesUpdateClientInfo();
	NetworkMessageIndices *Clone() const {return new IndicesUpdateClientInfo;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesUpdateClientInfo::IndicesUpdateClientInfo()
{
	cameraPosition = -1;
}

void IndicesUpdateClientInfo::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);

	SCAN(cameraPosition)
}

//! Create network message indices for ClientInfoObject class
NetworkMessageIndices *GetIndicesUpdateClientInfo() {return new IndicesUpdateClientInfo();}

NetworkMessageFormat &ClientInfoObject::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	switch (cls)
	{
	case NMCUpdateGeneric:
		NetworkObject::CreateFormat(cls, format);
		format.Add("cameraPosition", NDTVector, NCTNone, DEFVALUE(Vector3, InvalidCamPos), DOC_MSG("Position of camera on client"));
		break;
	default:
		NetworkObject::CreateFormat(cls, format);
		break;
	}
	return format;
}

TMError ClientInfoObject::TransferMsg(NetworkMessageContext &ctx)
{
	switch (ctx.GetClass())
	{
	case NMCUpdateGeneric:
		TMCHECK(NetworkObject::TransferMsg(ctx))
		{
			Assert(dynamic_cast<const IndicesUpdateClientInfo *>(ctx.GetIndices()))
			const IndicesUpdateClientInfo *indices = static_cast<const IndicesUpdateClientInfo *>(ctx.GetIndices());
			
			if (ctx.IsSending())
			{
				const Camera *camera = GScene->GetCamera();
				if (camera)
					_cameraPosition = camera->Position();
				else
					_cameraPosition = InvalidCamPos;
			}
			ITRANSF(cameraPosition)
		}
		break;
	default:
		TMCHECK(NetworkObject::TransferMsg(ctx))
		break;
	}
	return TMOK;
}

float ClientInfoObject::CalculateError(NetworkMessageContext &ctx)
{
	float error = NetworkObject::CalculateError(ctx);

	switch (ctx.GetClass())
	{
	case NMCUpdateGeneric:
		{
			Assert(dynamic_cast<const IndicesUpdateClientInfo *>(ctx.GetIndices()))
			const IndicesUpdateClientInfo *indices = static_cast<const IndicesUpdateClientInfo *>(ctx.GetIndices());
		
			Vector3 pos;
			if (ctx.IdxTransfer(indices->cameraPosition, pos) == TMOK)
			{
				const Camera *camera = GScene->GetCamera();
				if (camera)
					error += 1.0 * pos.Distance2(camera->Position());
			}
		}
	default:
		break;
	}
	return error;
}

RString ClientInfoObject::GetDebugName() const
{
	return RString("ClientInfoObject");
}


///////////////////////////////////////////////////////////////////////////////
// static (nonregistered) messages

//! network message indices for PlayerMessage class
class IndicesPlayer : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int player;
	int name;
//{ DEDICATED SERVER SUPPORT
	int server;
//}
	//@}

	//! Constructor
	IndicesPlayer();
	NetworkMessageIndices *Clone() const {return new IndicesPlayer;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesPlayer::IndicesPlayer()
{
	player = -1;
	name = -1;
//{ DEDICATED SERVER SUPPORT
	server = -1;
//}
}

void IndicesPlayer::Scan(NetworkMessageFormatBase *format)
{
	SCAN(player)
	SCAN(name)
//{ DEDICATED SERVER SUPPORT
	SCAN(server)
//}
}

//! Create network message indices for PlayerMessage class
NetworkMessageIndices *GetIndicesPlayer() {return new IndicesPlayer();}

NetworkMessageFormat &PlayerMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("player", NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("Unique ID of client (player)"));
	format.Add("name", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Player's name"));
//{ DEDICATED SERVER SUPPORT
	format.Add("server", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Bot client (running in the same process as server)"));
//}
	return format;
}

TMError PlayerMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesPlayer *>(ctx.GetIndices()))
	const IndicesPlayer *indices = static_cast<const IndicesPlayer *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransfer(indices->player, player))
	TMCHECK(ctx.IdxTransfer(indices->name, name))
//{ DEDICATED SERVER SUPPORT
	TMCHECK(ctx.IdxTransfer(indices->server, server))
//}
	return TMOK;
}

//! network message indices for NetworkMessageQueue class
class IndicesMessages : public NetworkMessageIndices
{
public:
	IndicesMessages();
	NetworkMessageIndices *Clone() const {return new IndicesMessages;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesMessages::IndicesMessages()
{
}

void IndicesMessages::Scan(NetworkMessageFormatBase *format)
{
}

//! Create network message indices for NetworkMessageQueue class
NetworkMessageIndices *GetIndicesMessages() {return new IndicesMessages();}

NetworkMessageFormat &NetworkMessageQueue::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	return format;
}

TMError NetworkMessageQueue::TransferMsg(NetworkMessageContext &ctx)
{
	return TMOK;
}


///////////////////////////////////////////////////////////////////////////////
// variant (registered) messages

//! network message indices for ChangeGameState class
class IndicesGameState : public NetworkMessageIndices
{
public:
	//! index of field in message format
	int gameState;

	//! Constructor
	IndicesGameState();
	NetworkMessageIndices *Clone() const {return new IndicesGameState;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesGameState::IndicesGameState()
{
	gameState = -1;
}

void IndicesGameState::Scan(NetworkMessageFormatBase *format)
{
	SCAN(gameState)
}

//! Create network message indices for ChangeGameState class
NetworkMessageIndices *GetIndicesGameState() {return new IndicesGameState();}

NetworkMessageFormat &ChangeGameState::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("gameState", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Current multiplayer game state"));
	return format;
}

TMError ChangeGameState::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesGameState *>(ctx.GetIndices()))
	const IndicesGameState *indices = static_cast<const IndicesGameState *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransfer(indices->gameState, (int &)gameState))
	return TMOK;
}

//! network message indices for LogoutMessage class
class IndicesLogout : public NetworkMessageIndices
{
public:
	//! index of field in message format
	int dpnid;

	//! Constructor
	IndicesLogout();
	NetworkMessageIndices *Clone() const {return new IndicesLogout;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesLogout::IndicesLogout()
{
	dpnid = -1;
}

void IndicesLogout::Scan(NetworkMessageFormatBase *format)
{
	SCAN(dpnid)
}

//! Create network message indices for LogoutMessage class
NetworkMessageIndices *GetIndicesLogout() {return new IndicesLogout();}

NetworkMessageFormat &LogoutMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("dpnid", NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("ID of client (player)"));
	return format;
}

TMError LogoutMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesLogout *>(ctx.GetIndices()))
	const IndicesLogout *indices = static_cast<const IndicesLogout *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransfer(indices->dpnid, (int &)dpnid))
	return TMOK;
}

//! network message indices for MissionHeader class
/*!
\patch_internal 1.05 Date 7/18/2001 by Jirka
- Changed: check CRC instead of time in mission file validation
*/
class IndicesMissionHeader : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int island;
	int name;
	int description;
	int fileName;
	int fileDir;
	int fileSizeL;
	int fileSizeH;
	int fileCRC;
/*
	int fileTimeL;
	int fileTimeH;
*/
	int respawn;
	int respawnDelay;
	int cadetMode;
#if _ENABLE_AI
	int disabledAI;
#endif
	int aiKills;
	int updateOnly;
	int difficulty[DTN];
	int addOns;
	int estimatedEndTime;

	int titleParam1;
	int valuesParam1;
	int textsParam1;
	int defValueParam1;
	int titleParam2;
	int valuesParam2;
	int textsParam2;
	int defValueParam2;
	//@}

	//! Constructor
	IndicesMissionHeader();
	NetworkMessageIndices *Clone() const {return new IndicesMissionHeader;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesMissionHeader::IndicesMissionHeader()
{
	island = -1;
	name = -1;
	description = -1;
	fileName = -1;
	fileDir = -1;
	fileSizeL = -1;
	fileSizeH = -1;
/*
	fileTimeL = -1;
	fileTimeH = -1;
*/
	fileCRC = -1;
	respawn = -1;
	respawnDelay = -1;
	cadetMode = -1;
#if _ENABLE_AI
	disabledAI = -1;
#endif
	aiKills = -1;
	updateOnly = -1;
	for (int i=0; i<DTN; i++) difficulty[i] = -1;
	addOns = -1;
	estimatedEndTime = -1;

	titleParam1 = -1;
	valuesParam1 = -1;
	textsParam1 = -1;
	defValueParam1 = -1;
	titleParam2 = -1;
	valuesParam2 = -1;
	textsParam2 = -1;
	defValueParam2 = -1;
}

void IndicesMissionHeader::Scan(NetworkMessageFormatBase *format)
{
	SCAN(island)
	SCAN(name)
	SCAN(description)
	SCAN(fileName)
	SCAN(fileDir)
	SCAN(fileSizeL)
	SCAN(fileSizeH)
/*
	SCAN(fileTimeL)
	SCAN(fileTimeH)
*/
	SCAN(fileCRC)
	SCAN(respawn)
	SCAN(respawnDelay)
	SCAN(cadetMode)
#if _ENABLE_AI
	SCAN(disabledAI)
#endif
	SCAN(aiKills)
	SCAN(updateOnly)
	for (int i=0; i<DTN; i++)
	{
		RString name = RString("diff") + RString(Glob.config.diffDesc[i].name);
		difficulty[i] = format->FindIndex(name);
	}
	SCAN(addOns)

	SCAN(estimatedEndTime)

	SCAN(titleParam1)
	SCAN(valuesParam1)
	SCAN(textsParam1)
	SCAN(defValueParam1)
	SCAN(titleParam2)
	SCAN(valuesParam2)
	SCAN(textsParam2)
	SCAN(defValueParam2)
}

//! Create network message indices for MissionHeader class
NetworkMessageIndices *GetIndicesMissionHeader() {return new IndicesMissionHeader();}

MissionHeader::MissionHeader()
{
	difficulty.Resize(DTN);
	difficulty.Compact();
	
	estimatedEndTime = TIME_MIN;

	updateOnly = false;
	
	defValueParam1 = 0;
	defValueParam2 = 0;
}

NetworkMessageFormat &MissionHeader::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("island", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Island (map), where mission is placed"));
	format.Add("name", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Name of mission"));
	format.Add("description", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Description of mission"));

	format.Add("fileName", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Name of mission file"));
	format.Add("fileDir", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Directory, where mission file is placed"));
	format.Add("fileSizeL", NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("Size of mission file (low DWORD)"));
	format.Add("fileSizeH", NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("Size of mission file (high DWORD)"));
/*
	format.Add("fileTimeL", NDTInteger, NCTNone, DEFVALUE(int, 0));
	format.Add("fileTimeH", NDTInteger, NCTNone, DEFVALUE(int, 0));
*/
	format.Add("fileCRC", NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("CRC of mission file"));

	format.Add("respawn", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, RespawnSeaGull), DOC_MSG("Respawn type"));
	format.Add("respawnDelay", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Respawn delay (in seconds)"));

	format.Add("cadetMode", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Cadet / Veteran mode"));
#if _ENABLE_AI
	format.Add("disabledAI", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("AI is disabled"));
#endif
	format.Add("aiKills", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Write AI kills into statistics"));

	format.Add("updateOnly", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("This message is only update of (recently sent) mission info"));

	for (int i=0; i<DTN; i++)
	{
		RString name = RString("diff") + RString(Glob.config.diffDesc[i].name);
		format.Add(name, NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Difficulty settings"));
	}

	format.Add("addOns", NDTStringArray, NCTNone, DEFVALUESTRINGARRAY, DOC_MSG("List of used addons"));

	format.Add("estimatedEndTime", NDTTime, NCTNone, DEFVALUE(Time, TIME_MIN), DOC_MSG("Time of estimated end of mission"));

	format.Add("titleParam1", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Mission parameter - title"));
	format.Add("valuesParam1", NDTFloatArray, NCTNone, DEFVALUEFLOATARRAY, DOC_MSG("Mission parameter - list of values"));
	format.Add("textsParam1", NDTStringArray, NCTNone, DEFVALUESTRINGARRAY, DOC_MSG("Mission parameter - list of value names"));
	format.Add("defValueParam1", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Mission parameter - default value"));
	format.Add("titleParam2", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Mission parameter - title"));
	format.Add("valuesParam2", NDTFloatArray, NCTNone, DEFVALUEFLOATARRAY, DOC_MSG("Mission parameter - list of values"));
	format.Add("textsParam2", NDTStringArray, NCTNone, DEFVALUESTRINGARRAY, DOC_MSG("Mission parameter - list of value names"));
	format.Add("defValueParam2", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Mission parameter - default value"));

	return format;
}

TMError MissionHeader::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesMissionHeader *>(ctx.GetIndices()))
	const IndicesMissionHeader *indices = static_cast<const IndicesMissionHeader *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransfer(indices->island, island))
	TMCHECK(ctx.IdxTransfer(indices->name, name))
	TMCHECK(ctx.IdxTransfer(indices->description, description))

	TMCHECK(ctx.IdxTransfer(indices->fileName, fileName))
	TMCHECK(ctx.IdxTransfer(indices->fileDir, fileDir))
	TMCHECK(ctx.IdxTransfer(indices->fileSizeL, fileSizeL))
	TMCHECK(ctx.IdxTransfer(indices->fileSizeH, fileSizeH))
/*
	TMCHECK(ctx.IdxTransfer(indices->fileTimeL, fileTimeL))
	TMCHECK(ctx.IdxTransfer(indices->fileTimeH, fileTimeH))
*/
	TMCHECK(ctx.IdxTransfer(indices->fileCRC, fileCRC))

	TMCHECK(ctx.IdxTransfer(indices->respawn, (int &)respawn))
	TMCHECK(ctx.IdxTransfer(indices->respawnDelay, respawnDelay))

	TMCHECK(ctx.IdxTransfer(indices->cadetMode, cadetMode))
#if _ENABLE_AI
	TMCHECK(ctx.IdxTransfer(indices->disabledAI, disabledAI))
#endif
	TMCHECK(ctx.IdxTransfer(indices->aiKills, aiKills))
	TMCHECK(ctx.IdxTransfer(indices->updateOnly, updateOnly))
	for (int i=0; i<DTN; i++)
	{
		TMCHECK(ctx.IdxTransfer(indices->difficulty[i], difficulty[i]))
	}

	TMCHECK(ctx.IdxTransfer(indices->addOns, addOns))

	TMCHECK(ctx.IdxTransfer(indices->estimatedEndTime, estimatedEndTime))

	TMCHECK(ctx.IdxTransfer(indices->titleParam1, titleParam1))
	TMCHECK(ctx.IdxTransfer(indices->valuesParam1, valuesParam1))
	TMCHECK(ctx.IdxTransfer(indices->textsParam1, textsParam1))
	TMCHECK(ctx.IdxTransfer(indices->defValueParam1, defValueParam1))
	TMCHECK(ctx.IdxTransfer(indices->titleParam2, titleParam2))
	TMCHECK(ctx.IdxTransfer(indices->valuesParam2, valuesParam2))
	TMCHECK(ctx.IdxTransfer(indices->textsParam2, textsParam2))
	TMCHECK(ctx.IdxTransfer(indices->defValueParam2, defValueParam2))

	return TMOK;
}

IndicesPlayerRole::IndicesPlayerRole()
{
	index = -1;
	side = -1;
	group = -1;
	unit = -1;
	vehicle = -1;
	position = -1;
	leader = -1;
	roleLocked = -1;
	player = -1;
}

void IndicesPlayerRole::Scan(NetworkMessageFormatBase *format)
{
	SCAN(index)
	SCAN(side)
	SCAN(group)
	SCAN(unit)
	SCAN(vehicle)
	SCAN(position)
	SCAN(leader)
	SCAN(roleLocked)
	SCAN(player)
}

//! Create network message indices for PlayerRole class
NetworkMessageIndices *GetIndicesPlayerRole() {return new IndicesPlayerRole();}
NetworkMessageIndices *GetIndicesPlayerSide() {return new IndicesPlayerRole();}

NetworkMessageFormat &PlayerRole::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("index", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Index of role"));
	format.Add("side", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Side of role"));
	format.Add("group", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Group ID of role"));
	format.Add("unit", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Unit ID of role"));
	format.Add("vehicle", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Vehicle used by this role"));
	format.Add("position", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Position in vehicle (driver, commander, ...)"));
	format.Add("leader", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Is this unit group leader"));
	format.Add("roleLocked", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Role is locked (only admin can assign this role)"));
	format.Add("player", NDTInteger, NCTNone, DEFVALUE(int, AI_PLAYER), DOC_MSG("Currently attached player"));
	return format;
}

TMError PlayerRole::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesPlayerRole *>(ctx.GetIndices()))
	const IndicesPlayerRole *indices = static_cast<const IndicesPlayerRole *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransfer(indices->side, (int &)side))
	TMCHECK(ctx.IdxTransfer(indices->group, group))
	TMCHECK(ctx.IdxTransfer(indices->unit, unit))
	TMCHECK(ctx.IdxTransfer(indices->vehicle, vehicle))
	TMCHECK(ctx.IdxTransfer(indices->position, (int &)position))
	TMCHECK(ctx.IdxTransfer(indices->leader, leader))
	TMCHECK(ctx.IdxTransfer(indices->roleLocked, roleLocked))
	TMCHECK(ctx.IdxTransfer(indices->player, player))
	return TMOK;
}

DEFINE_NET_MESSAGE(PublicVariable,PUB_VAR_MSG)

DEFINE_NET_MESSAGE(GroupSynchronization,GROUP_SYNC_MSG)

DEFINE_NET_MESSAGE(DetectorActivation,DET_ACT_MSG)

DEFINE_NET_MESSAGE(AskForCreateUnit,CREATE_UNIT_MSG)

DEFINE_NET_MESSAGE(AskForDeleteVehicle,DELETE_VEHICLE_MSG)

DEFINE_NET_MESSAGE(AskForReceiveUnitAnswer,UNIT_ANSWER_MSG)

DEFINE_NET_MESSAGE(AskForGroupRespawn,GROUP_RESPAWN_MSG)

DEFINE_NET_MESSAGE(CopyUnitInfo,COPY_UNIT_INFO_MSG)

DEFINE_NET_MESSAGE(GroupRespawnDone,GROUP_RESPAWN_DONE_MSG)

DEFINE_NET_MESSAGE(MissionParams, MISSION_PARAMS_MSG)

DEFINE_NET_MESSAGE(AskForActivateMine, ACTIVATE_MINE_MSG)

DEFINE_NET_MESSAGE(VehicleDamaged, VEHICLE_DAMAGED_MSG)

DEFINE_NET_MESSAGE(AskForInflameFire, INFLAME_FIRE_MSG)

DEFINE_NET_MESSAGE(AskForAnimationPhase, ANIMATION_PHASE_MSG)

DEFINE_NET_MESSAGE(IncomingMissile, INCOMING_MISSILE_MSG)

IndicesChat::IndicesChat()
{
	channel = -1;
	sender = -1;
	units = -1;
	name = -1;
	text = -1;
}

void IndicesChat::Scan(NetworkMessageFormatBase *format)
{
	SCAN(channel)
	SCAN(sender)
	SCAN(units)
	SCAN(name)
	SCAN(text)
}

//! Create network message indices for ChatMessage class
NetworkMessageIndices *GetIndicesChat() {return new IndicesChat();}

NetworkMessageFormat &ChatMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("channel", NDTInteger, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Radio channel"));
	format.Add("sender", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Sender unit"));
	format.Add("units", NDTRefArray, NCTNone, DEFVALUEREFARRAY, DOC_MSG("List of receiving units"));
	format.Add("name", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Sender name"));
	format.Add("text", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Message content"));
	return format;
}

TMError ChatMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesChat *>(ctx.GetIndices()))
	const IndicesChat *indices = static_cast<const IndicesChat *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransfer(indices->channel, channel))
	TMCHECK(ctx.IdxTransferRef(indices->sender, sender))
	TMCHECK(ctx.IdxTransferRefs(indices->units, units))
	TMCHECK(ctx.IdxTransfer(indices->name, name))
	TMCHECK(ctx.IdxTransfer(indices->text, text))
	return TMOK;
}

IndicesRadioChat::IndicesRadioChat()
{
	channel = -1;
	sender = -1;
	units = -1;
	text = -1;
	sentence = -1;
}

void IndicesRadioChat::Scan(NetworkMessageFormatBase *format)
{
	SCAN(channel)
	SCAN(sender)
	SCAN(units)
	SCAN(text)
	SCAN(sentence)
}

//! Create network message indices for RadioChatMessage class
NetworkMessageIndices *GetIndicesRadioChat() {return new IndicesRadioChat();}

NetworkMessageFormat &RadioChatMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("channel", NDTInteger, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Radio channel"));
	format.Add("sender", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Sender unit"));
	format.Add("units", NDTRefArray, NCTNone, DEFVALUEREFARRAY, DOC_MSG("List of receiving units"));
	format.Add("text", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Content of message (text)"));
	format.Add("sentence", NDTSentence, NCTNone, DEFVALUE(RadioSentence, RadioSentence()), DOC_MSG("Content of message (list of words to say)"));
	return format;
}

TMError RadioChatMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesRadioChat *>(ctx.GetIndices()))
	const IndicesRadioChat *indices = static_cast<const IndicesRadioChat *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransfer(indices->channel, channel))
	TMCHECK(ctx.IdxTransferRef(indices->sender, sender))
	TMCHECK(ctx.IdxTransferRefs(indices->units, units))
	TMCHECK(ctx.IdxTransfer(indices->text, text))
	TMCHECK(ctx.IdxTransfer(indices->sentence, sentence))
	return TMOK;
}

IndicesRadioChatWave::IndicesRadioChatWave()
{
	channel = -1;
	units = -1;
	wave = -1;
	sender = -1;
	senderName = -1;
}

void IndicesRadioChatWave::Scan(NetworkMessageFormatBase *format)
{
	SCAN(channel)
	SCAN(units)
	SCAN(wave)
	SCAN(sender)
	SCAN(senderName)
}

//! Create network message indices for RadioChatWaveMessage class
NetworkMessageIndices *GetIndicesRadioChatWave() {return new IndicesRadioChatWave();}

NetworkMessageFormat &RadioChatWaveMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("channel", NDTInteger, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Radio channel"));
	format.Add("units", NDTRefArray, NCTNone, DEFVALUEREFARRAY, DOC_MSG("List of receiving units"));
	format.Add("wave", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Sound identifier"));
	format.Add("sender", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Sender unit"));
	format.Add("senderName", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Sender name"));
	return format;
}

TMError RadioChatWaveMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesRadioChatWave *>(ctx.GetIndices()))
	const IndicesRadioChatWave *indices = static_cast<const IndicesRadioChatWave *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransfer(indices->channel, channel))
	TMCHECK(ctx.IdxTransferRefs(indices->units, units))
	TMCHECK(ctx.IdxTransfer(indices->wave, wave))
	TMCHECK(ctx.IdxTransferRef(indices->sender, sender))
	TMCHECK(ctx.IdxTransfer(indices->senderName, senderName))
	return TMOK;
}

IndicesSetVoiceChannel::IndicesSetVoiceChannel()
{
	channel = -1;
	units = -1;
}

void IndicesSetVoiceChannel::Scan(NetworkMessageFormatBase *format)
{
	SCAN(channel)
	SCAN(units)
}

//! Create network message indices for SetVoiceChannelMessage class
NetworkMessageIndices *GetIndicesSetVoiceChannel() {return new IndicesSetVoiceChannel();}

NetworkMessageFormat &SetVoiceChannelMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("channel", NDTInteger, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Radio channel"));
	format.Add("units", NDTRefArray, NCTNone, DEFVALUEREFARRAY, DOC_MSG("List of receiving units"));
	return format;
}

TMError SetVoiceChannelMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesSetVoiceChannel *>(ctx.GetIndices()))
	const IndicesSetVoiceChannel *indices = static_cast<const IndicesSetVoiceChannel *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransfer(indices->channel, channel))
	TMCHECK(ctx.IdxTransferRefs(indices->units, units))
	return TMOK;
}

//! network message indices for SetSpeakerMessage class
class IndicesSetSpeaker : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int player;
	int on;
	int creator;
	int id;
	//@}

	//! Constructor
	IndicesSetSpeaker();
	NetworkMessageIndices *Clone() const {return new IndicesSetSpeaker;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesSetSpeaker::IndicesSetSpeaker()
{
	player = -1;
	on = -1;
	creator = -1;
	id = -1;
}

void IndicesSetSpeaker::Scan(NetworkMessageFormatBase *format)
{
	SCAN(player)
	SCAN(on)
	SCAN(creator)
	SCAN(id)
}

//! Create network message indices for SetSpeakerMessage class
NetworkMessageIndices *GetIndicesSetSpeaker() {return new IndicesSetSpeaker();}

NetworkMessageFormat &SetSpeakerMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("player", NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("Client (player) ID of speaking player"));
	format.Add("on", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Turn on / off direct speaking"));
	format.Add("creator", NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("ID of speaking unit"));
	format.Add("id", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("ID of speaking unit"));
	return format;
}

TMError SetSpeakerMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesSetSpeaker *>(ctx.GetIndices()))
	const IndicesSetSpeaker *indices = static_cast<const IndicesSetSpeaker *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransfer(indices->player, player))
	TMCHECK(ctx.IdxTransfer(indices->on, on))
	TMCHECK(ctx.IdxTransfer(indices->creator, object.creator))
	TMCHECK(ctx.IdxTransfer(indices->id, object.id))
	return TMOK;
}

//! network message indices for SelectPlayerMessage class
class IndicesSelectPlayer : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int player;
	int creator;
	int id;
	int position;
	int respawn;
	//@}

	//! Constructor
	IndicesSelectPlayer();
	NetworkMessageIndices *Clone() const {return new IndicesSelectPlayer;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesSelectPlayer::IndicesSelectPlayer()
{
	player = -1;
	creator = -1;
	id = -1;
	position = -1;
	respawn = -1;
}

void IndicesSelectPlayer::Scan(NetworkMessageFormatBase *format)
{
	SCAN(player)
	SCAN(creator)
	SCAN(id)
	SCAN(position)
	SCAN(respawn)
}

//! Create network message indices for SelectPlayerMessage class
NetworkMessageIndices *GetIndicesSelectPlayer() {return new IndicesSelectPlayer();}

NetworkMessageFormat &SelectPlayerMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("player", NDTInteger, NCTNone, DEFVALUE(int, AI_PLAYER), DOC_MSG("Client (player) ID of player"));
	format.Add("creator", NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("ID of player's unit"));
	format.Add("id", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("ID of player's unit"));
	format.Add("position", NDTVector, NCTNone, DEFVALUE(Vector3, Vector3(-FLT_MAX, -FLT_MAX, -FLT_MAX)), DOC_MSG("Player's unit position"));
	format.Add("respawn", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Selection of player's unit after respawn"));
	return format;
}

TMError SelectPlayerMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesSelectPlayer *>(ctx.GetIndices()))
	const IndicesSelectPlayer *indices = static_cast<const IndicesSelectPlayer *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransfer(indices->player, player))
	TMCHECK(ctx.IdxTransfer(indices->creator, person.creator))
	TMCHECK(ctx.IdxTransfer(indices->id, person.id))
	TMCHECK(ctx.IdxTransfer(indices->position, position))
	TMCHECK(ctx.IdxTransfer(indices->respawn, respawn))
	return TMOK;
}

//! network message indices for ChangeOwnerMessage class
class IndicesChangeOwner : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int creator;
	int id;
	int owner;
	//@}

	//! Constructor
	IndicesChangeOwner();
	NetworkMessageIndices *Clone() const {return new IndicesChangeOwner;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesChangeOwner::IndicesChangeOwner()
{
	creator = -1;
	id = -1;
	owner = -1;
}

void IndicesChangeOwner::Scan(NetworkMessageFormatBase *format)
{
	SCAN(creator)
	SCAN(id)
	SCAN(owner)
}

//! Create network message indices for ChangeOwnerMessage class
NetworkMessageIndices *GetIndicesChangeOwner() {return new IndicesChangeOwner();}

NetworkMessageFormat &ChangeOwnerMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("creator", NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("ID of object, which owner is changing"));
	format.Add("id", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("ID of object, whic player is changing"));
	format.Add("owner", NDTInteger, NCTNone, DEFVALUE(int, AI_PLAYER), DOC_MSG("Client ID of new owner"));
	return format;
}

TMError ChangeOwnerMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesChangeOwner *>(ctx.GetIndices()))
	const IndicesChangeOwner *indices = static_cast<const IndicesChangeOwner *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransfer(indices->creator, object.creator))
	TMCHECK(ctx.IdxTransfer(indices->id, object.id))
	TMCHECK(ctx.IdxTransfer(indices->owner, owner))
	return TMOK;
}

//! network message indices for PlaySoundMessage class
class IndicesPlaySound : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int name;
	int position;
	int speed;
	int volume;
	int frequency;
	int creator;
	int soundId;
	//@}

	//! Constructor
	IndicesPlaySound();
	NetworkMessageIndices *Clone() const {return new IndicesPlaySound;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesPlaySound::IndicesPlaySound()
{
	name = -1;
	position = -1;
	speed = -1;
	volume = -1;
	frequency = -1;
	creator = -1;
	soundId = -1;
}

void IndicesPlaySound::Scan(NetworkMessageFormatBase *format)
{
	SCAN(name)
	SCAN(position)
	SCAN(speed)
	SCAN(volume)
	SCAN(frequency)
	SCAN(creator)
	SCAN(soundId)
}

//! Create network message indices for PlaySoundMessage class
NetworkMessageIndices *GetIndicesPlaySound() {return new IndicesPlaySound();}

NetworkMessageFormat &PlaySoundMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	Vector3 temp = VZero;
	format.Add("name", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Sound identifier"));
	format.Add("position", NDTVector, NCTNone, DEFVALUE(Vector3, temp), DOC_MSG("Sound source position"));
	format.Add("speed", NDTVector, NCTNone, DEFVALUE(Vector3, temp), DOC_MSG("Sound source speed"));
	format.Add("volume", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Sound volume"));
	format.Add("frequency", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Sound pitch"));
	format.Add("creator", NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("Unique network ID of sound"));
	format.Add("soundId", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Unique network ID of sound"));
	return format;
}

TMError PlaySoundMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesPlaySound *>(ctx.GetIndices()))
	const IndicesPlaySound *indices = static_cast<const IndicesPlaySound *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransfer(indices->name, name))
	TMCHECK(ctx.IdxTransfer(indices->position, position))
	TMCHECK(ctx.IdxTransfer(indices->speed, speed))
	TMCHECK(ctx.IdxTransfer(indices->volume, volume))
	TMCHECK(ctx.IdxTransfer(indices->frequency, freq))
	TMCHECK(ctx.IdxTransfer(indices->creator, creator))
	TMCHECK(ctx.IdxTransfer(indices->soundId, soundId))
	return TMOK;
}

//! network message indices for SoundStateMessage class
class IndicesSoundState : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int state;
	int creator;
	int soundId;
	//@}

	//! Constructor
	IndicesSoundState();
	NetworkMessageIndices *Clone() const {return new IndicesSoundState;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesSoundState::IndicesSoundState()
{
	state = -1;
	creator = -1;
	soundId = -1;
}

void IndicesSoundState::Scan(NetworkMessageFormatBase *format)
{
	SCAN(state)
	SCAN(creator)
	SCAN(soundId)
}

//! Create network message indices for SoundStateMessage class
NetworkMessageIndices *GetIndicesSoundState() {return new IndicesSoundState();}

NetworkMessageFormat &SoundStateMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("state", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("New state of sound"));
	format.Add("creator", NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("Network ID of sound"));
	format.Add("soundId", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Network ID of sound"));
	return format;
}

TMError SoundStateMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesSoundState *>(ctx.GetIndices()))
	const IndicesSoundState *indices = static_cast<const IndicesSoundState *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransfer(indices->state, (int &)state))
	TMCHECK(ctx.IdxTransfer(indices->creator, creator))
	TMCHECK(ctx.IdxTransfer(indices->soundId, soundId))
	return TMOK;
}

//! network message indices for TransferFileMessage, TransferMissionFileMessage and TransferFileToServerMessage classes
class IndicesTransferFile : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int path;
	int data;
	int totSize;
	int offset;
	int totSegments;
	int curSegment;
	// int timeL;
	// int timeH;
	//@}

	//! Constructor
	IndicesTransferFile();
	NetworkMessageIndices *Clone() const {return new IndicesTransferFile;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesTransferFile::IndicesTransferFile()
{
	path = -1;
	data = -1;
	totSize = -1;
	offset = -1;
	totSegments = -1;
	curSegment = -1;
	// timeL = -1;
	// timeH = -1;
}

void IndicesTransferFile::Scan(NetworkMessageFormatBase *format)
{
	SCAN(path)
	SCAN(data)
	SCAN(totSize)
	SCAN(offset)
	SCAN(totSegments)
	SCAN(curSegment)
	// SCAN(timeL)
	// SCAN(timeH)
}

//! Create network message indices for TransferFileMessage class
NetworkMessageIndices *GetIndicesTransferFile() {return new IndicesTransferFile();}
//! Create network message indices for TransferMissionFileMessage class
NetworkMessageIndices *GetIndicesTransferMissionFile() {return new IndicesTransferFile();}
//! Create network message indices for TransferFileToServerMessage class
NetworkMessageIndices *GetIndicesTransferFileToServer() {return new IndicesTransferFile();}

NetworkMessageFormat &TransferFileMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("path", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Path of transferred file"));
	format.Add("data", NDTRawData, NCTNone, DEFVALUERAWDATA, DOC_MSG("Content of file (single segment)"));
	format.Add("totSize", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Total size of file"));
	format.Add("offset", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Offset of this segment"));
	format.Add("totSegments", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 1), DOC_MSG("Total number of segments"));
	format.Add("curSegment", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Index of this segment"));
	// format.Add("timeL", NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("File time - last write (low DWORD)"));
	// format.Add("timeH", NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("File time - last write (high DWORD)"));
	return format;
}

TMError TransferFileMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesTransferFile *>(ctx.GetIndices()))
	const IndicesTransferFile *indices = static_cast<const IndicesTransferFile *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransfer(indices->path, path))
	TMCHECK(ctx.IdxTransfer(indices->data, data))
	TMCHECK(ctx.IdxTransfer(indices->totSize, totSize))
	TMCHECK(ctx.IdxTransfer(indices->offset, offset))
	TMCHECK(ctx.IdxTransfer(indices->totSegments, totSegments))
	TMCHECK(ctx.IdxTransfer(indices->curSegment, curSegment))
	// TMCHECK(ctx.IdxTransfer(indices->timeL, timeL))
	// TMCHECK(ctx.IdxTransfer(indices->timeH, timeH))
	return TMOK;
}

//! network message indices for AskMissionFileMessage class
class IndicesAskMissionFile : public NetworkMessageIndices
{
public:
	//! index of field in message format
	int valid;

	//! Constructor
	IndicesAskMissionFile();
	NetworkMessageIndices *Clone() const {return new IndicesAskMissionFile;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesAskMissionFile::IndicesAskMissionFile()
{
	valid = -1;
}

void IndicesAskMissionFile::Scan(NetworkMessageFormatBase *format)
{
	SCAN(valid)
}

//! Create network message indices for AskMissionFileMessage class
NetworkMessageIndices *GetIndicesAskMissionFile() {return new IndicesAskMissionFile();}

NetworkMessageFormat &AskMissionFileMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("valid", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Mission file is valid (present on client computer)"));
	return format;
}

TMError AskMissionFileMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesAskMissionFile *>(ctx.GetIndices()))
	const IndicesAskMissionFile *indices = static_cast<const IndicesAskMissionFile *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransfer(indices->valid, valid))
	return TMOK;
}

IndicesAskForDammage::IndicesAskForDammage()
{
	who = -1;
	owner = -1;
	modelPos = -1;
	val = -1;
	valRange = -1;
	ammo = -1;
}

void IndicesAskForDammage::Scan(NetworkMessageFormatBase *format)
{
	SCAN(who)
	SCAN(owner)
	SCAN(modelPos)
	SCAN(val)
	SCAN(valRange)
	SCAN(ammo)
}

//! Create network message indices for AskForDammageMessage class
NetworkMessageIndices *GetIndicesAskForDammage() {return new IndicesAskForDammage();}

NetworkMessageFormat &AskForDammageMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	Vector3 temp = VZero;
	format.Add("who", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Damaged object"));
	format.Add("owner", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Who is responsible for damage"));
	format.Add("modelPos", NDTVector, NCTNone, DEFVALUE(Vector3, temp), DOC_MSG("Position of damage"));
	format.Add("val", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Amount of damage"));
	format.Add("valRange", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Range of damage"));
	format.Add("ammo", NDTString, NCTNone, DEFVALUE(RString, RString()), DOC_MSG("Ammunition type"));
	return format;
}

TMError AskForDammageMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesAskForDammage *>(ctx.GetIndices()))
	const IndicesAskForDammage *indices = static_cast<const IndicesAskForDammage *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->who, who))
	TMCHECK(ctx.IdxTransferRef(indices->owner, owner))
	TMCHECK(ctx.IdxTransfer(indices->modelPos, modelPos))
	TMCHECK(ctx.IdxTransfer(indices->val, val))
	TMCHECK(ctx.IdxTransfer(indices->valRange, valRange))
	TMCHECK(ctx.IdxTransfer(indices->ammo, ammo))
	return TMOK;
}

IndicesAskForSetDammage::IndicesAskForSetDammage()
{
	who = -1;
	dammage = -1;
}

void IndicesAskForSetDammage::Scan(NetworkMessageFormatBase *format)
{
	SCAN(who)
	SCAN(dammage)
}

//! Create network message indices for AskForSetDammageMessage class
NetworkMessageIndices *GetIndicesAskForSetDammage() {return new IndicesAskForSetDammage();}

NetworkMessageFormat &AskForSetDammageMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	Vector3 temp = VZero;
	format.Add("who", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Damaged object"));
	format.Add("dammage", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("New value of total damage"));
	return format;
}

TMError AskForSetDammageMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesAskForSetDammage *>(ctx.GetIndices()))
	const IndicesAskForSetDammage *indices = static_cast<const IndicesAskForSetDammage *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->who, who))
	TMCHECK(ctx.IdxTransfer(indices->dammage, dammage))
	return TMOK;
}

IndicesAskForGetIn::IndicesAskForGetIn()
{
	soldier = -1;
	vehicle = -1;
	position = -1;
}

void IndicesAskForGetIn::Scan(NetworkMessageFormatBase *format)
{
	SCAN(soldier)
	SCAN(vehicle)
	SCAN(position)
}

//! Create network message indices for AskForGetInMessage class
NetworkMessageIndices *GetIndicesAskForGetIn() {return new IndicesAskForGetIn();}

NetworkMessageFormat &AskForGetInMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("soldier", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Who is getting in"));
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Vehicle to get in"));
	format.Add("position", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Position in vehicle to get in"));
	return format;
}

TMError AskForGetInMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesAskForGetIn *>(ctx.GetIndices()))
	const IndicesAskForGetIn *indices = static_cast<const IndicesAskForGetIn *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->soldier, soldier))
	TMCHECK(ctx.IdxTransferRef(indices->vehicle, vehicle))
	TMCHECK(ctx.IdxTransfer(indices->position, (int &)position))
	return TMOK;
}

IndicesAskForGetOut::IndicesAskForGetOut()
{
	soldier = -1;
	vehicle = -1;
	parachute = -1;
}

void IndicesAskForGetOut::Scan(NetworkMessageFormatBase *format)
{
	SCAN(soldier)
	SCAN(vehicle)
	SCAN(parachute)
}

//! Create network message indices for AskForGetOutMessage class
NetworkMessageIndices *GetIndicesAskForGetOut() {return new IndicesAskForGetOut();}

NetworkMessageFormat &AskForGetOutMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("soldier", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Who is getting out"));
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Vehicle to get out"));
	format.Add("parachute", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Parachute or plain ejection"));
	return format;
}

TMError AskForGetOutMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesAskForGetOut *>(ctx.GetIndices()))
	const IndicesAskForGetOut *indices = static_cast<const IndicesAskForGetOut *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->soldier, soldier))
	TMCHECK(ctx.IdxTransferRef(indices->vehicle, vehicle))
	TMCHECK(ctx.IdxTransfer(indices->parachute, parachute))
	return TMOK;
}

IndicesAskForChangePosition::IndicesAskForChangePosition()
{
	soldier = -1;
	vehicle = -1;
	type = -1;
}

void IndicesAskForChangePosition::Scan(NetworkMessageFormatBase *format)
{
	SCAN(soldier)
	SCAN(vehicle)
	SCAN(type)
}

//! Create network message indices for AskForChangePositionMessage class
NetworkMessageIndices *GetIndicesAskForChangePosition() {return new IndicesAskForChangePosition();}

NetworkMessageFormat &AskForChangePositionMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("soldier", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Who is changing position"));
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Vehicle where position is changed"));
	format.Add("type", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, ATMoveToDriver), DOC_MSG("Performed action"));
	return format;
}

TMError AskForChangePositionMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesAskForChangePosition *>(ctx.GetIndices()))
	const IndicesAskForChangePosition *indices = static_cast<const IndicesAskForChangePosition *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->soldier, soldier))
	TMCHECK(ctx.IdxTransferRef(indices->vehicle, vehicle))
	TMCHECK(ctx.IdxTransfer(indices->type, (int &)type))
	return TMOK;
}

IndicesAskForAimWeapon::IndicesAskForAimWeapon()
{
	vehicle = -1;
	weapon = -1;
	dir = -1;
}

void IndicesAskForAimWeapon::Scan(NetworkMessageFormatBase *format)
{
	SCAN(vehicle)
	SCAN(weapon)
	SCAN(dir)
}

//! Create network message indices for AskForAimWeaponMessage class
NetworkMessageIndices *GetIndicesAskForAimWeapon() {return new IndicesAskForAimWeapon();}

NetworkMessageFormat &AskForAimWeaponMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Vehicle which weapon is aiming"));
	format.Add("weapon", NDTInteger, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Aiming weapon index"));
	format.Add("dir", NDTVector, NCTNone, DEFVALUE(Vector3, VForward), DOC_MSG("Direction to aim"));
	return format;
}

TMError AskForAimWeaponMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesAskForAimWeapon *>(ctx.GetIndices()))
	const IndicesAskForAimWeapon *indices = static_cast<const IndicesAskForAimWeapon *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->vehicle, vehicle))
	TMCHECK(ctx.IdxTransfer(indices->weapon, weapon))
	TMCHECK(ctx.IdxTransfer(indices->dir, dir))
	return TMOK;
}

IndicesAskForAimObserver::IndicesAskForAimObserver()
{
	vehicle = -1;
	dir = -1;
}

void IndicesAskForAimObserver::Scan(NetworkMessageFormatBase *format)
{
	SCAN(vehicle)
	SCAN(dir)
}

//! Create network message indices for AskForAimObserverMessage class
NetworkMessageIndices *GetIndicesAskForAimObserver() {return new IndicesAskForAimObserver();}

NetworkMessageFormat &AskForAimObserverMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Vehicle which turret is aiming"));
	format.Add("dir", NDTVector, NCTNone, DEFVALUE(Vector3, VForward), DOC_MSG("Direction to aim"));
	return format;
}

TMError AskForAimObserverMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesAskForAimObserver *>(ctx.GetIndices()))
	const IndicesAskForAimObserver *indices = static_cast<const IndicesAskForAimObserver *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->vehicle, vehicle))
	TMCHECK(ctx.IdxTransfer(indices->dir, dir))
	return TMOK;
}

IndicesAskForSelectWeapon::IndicesAskForSelectWeapon()
{
	vehicle = -1;
	weapon = -1;
}

void IndicesAskForSelectWeapon::Scan(NetworkMessageFormatBase *format)
{
	SCAN(vehicle)
	SCAN(weapon)
}

//! Create network message indices for AskForSelectWeaponMessage class
NetworkMessageIndices *GetIndicesAskForSelectWeapon() {return new IndicesAskForSelectWeapon();}

NetworkMessageFormat &AskForSelectWeaponMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Vehicle which weapon is selecting"));
	format.Add("weapon", NDTInteger, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Selected weapon index"));
	return format;
}

TMError AskForSelectWeaponMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesAskForSelectWeapon *>(ctx.GetIndices()))
	const IndicesAskForSelectWeapon *indices = static_cast<const IndicesAskForSelectWeapon *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->vehicle, vehicle))
	TMCHECK(ctx.IdxTransfer(indices->weapon, weapon))
	return TMOK;
}

IndicesAskForAddImpulse::IndicesAskForAddImpulse()
{
	vehicle = -1;
	force = -1;
	torque = -1;
}

void IndicesAskForAddImpulse::Scan(NetworkMessageFormatBase *format)
{
	SCAN(vehicle)
	SCAN(force)
	SCAN(torque)
}

//! Create network message indices for AskForAddImpulseMessage class
NetworkMessageIndices *GetIndicesAskForAddImpulse() {return new IndicesAskForAddImpulse();}

NetworkMessageFormat &AskForAddImpulseMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Vehicle impulse is applied to"));
	format.Add("force", NDTVector, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Applied force"));
	format.Add("torque", NDTVector, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Applied torque"));
	return format;
}

TMError AskForAddImpulseMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesAskForAddImpulse *>(ctx.GetIndices()))
	const IndicesAskForAddImpulse *indices = static_cast<const IndicesAskForAddImpulse *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->vehicle, vehicle))
	TMCHECK(ctx.IdxTransfer(indices->force, force))
	TMCHECK(ctx.IdxTransfer(indices->torque, torque))
	return TMOK;
}

IndicesAskForMoveVector::IndicesAskForMoveVector()
{
	vehicle = -1;
	pos = -1;
}

void IndicesAskForMoveVector::Scan(NetworkMessageFormatBase *format)
{
	SCAN(vehicle)
	SCAN(pos)
}

//! Create network message indices for AskForMoveVectorMessage class
NetworkMessageIndices *GetIndicesAskForMoveVector() {return new IndicesAskForMoveVector();}

NetworkMessageFormat &AskForMoveVectorMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Moving object"));
	format.Add("pos", NDTVector, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("New position"));
	return format;
}

TMError AskForMoveVectorMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesAskForMoveVector *>(ctx.GetIndices()))
	const IndicesAskForMoveVector *indices = static_cast<const IndicesAskForMoveVector *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->vehicle, vehicle))
	TMCHECK(ctx.IdxTransfer(indices->pos, pos))
	return TMOK;
}

IndicesAskForMoveMatrix::IndicesAskForMoveMatrix()
{
	vehicle = -1;
	pos = -1;
	orient = -1;
}

void IndicesAskForMoveMatrix::Scan(NetworkMessageFormatBase *format)
{
	SCAN(vehicle)
	SCAN(pos)
	SCAN(orient)
}

//! Create network message indices for AskForMoveMatrixMessage class
NetworkMessageIndices *GetIndicesAskForMoveMatrix() {return new IndicesAskForMoveMatrix();}

NetworkMessageFormat &AskForMoveMatrixMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Moving object"));
	format.Add("pos", NDTVector, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("New position"));
	format.Add("orient", NDTMatrix, NCTNone, DEFVALUE(Matrix3, M3Identity), DOC_MSG("New orientation"));
	return format;
}

TMError AskForMoveMatrixMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesAskForMoveMatrix *>(ctx.GetIndices()))
	const IndicesAskForMoveMatrix *indices = static_cast<const IndicesAskForMoveMatrix *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->vehicle, vehicle))
	TMCHECK(ctx.IdxTransfer(indices->pos, pos))
	TMCHECK(ctx.IdxTransfer(indices->orient, orient))
	return TMOK;
}


IndicesAskForJoinGroup::IndicesAskForJoinGroup()
{
	join = -1;
	group = -1;
}

void IndicesAskForJoinGroup::Scan(NetworkMessageFormatBase *format)
{
	SCAN(join)
	SCAN(group)
}

//! Create network message indices for AskForJoinGroupMessage class
NetworkMessageIndices *GetIndicesAskForJoinGroup() {return new IndicesAskForJoinGroup();}

NetworkMessageFormat &AskForJoinGroupMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("join", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Joined group"));
	format.Add("group", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Joining group"));
	return format;
}

TMError AskForJoinGroupMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesAskForJoinGroup *>(ctx.GetIndices()))
	const IndicesAskForJoinGroup *indices = static_cast<const IndicesAskForJoinGroup *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->join, join))
	TMCHECK(ctx.IdxTransferRef(indices->group, group))
	return TMOK;
}

IndicesAskForJoinUnits::IndicesAskForJoinUnits()
{
	join = -1;
	units = -1;
}

void IndicesAskForJoinUnits::Scan(NetworkMessageFormatBase *format)
{
	SCAN(join)
	SCAN(units)
}

//! Create network message indices for AskForMoveMatrixMessage class
NetworkMessageIndices *GetIndicesAskForJoinUnits() {return new IndicesAskForJoinUnits();}

NetworkMessageFormat &AskForJoinUnitsMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("join", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Joined group"));
	format.Add("units", NDTRefArray, NCTNone, DEFVALUEREFARRAY, DOC_MSG("Joining units"));
	return format;
}

TMError AskForJoinUnitsMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesAskForJoinUnits *>(ctx.GetIndices()))
	const IndicesAskForJoinUnits *indices = static_cast<const IndicesAskForJoinUnits *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->join, join))
	TMCHECK(ctx.IdxTransferRefs(indices->units, units))
	return TMOK;
}

IndicesAskForHideBody::IndicesAskForHideBody()
{
	vehicle = -1;
}

void IndicesAskForHideBody::Scan(NetworkMessageFormatBase *format)
{
	SCAN(vehicle)
}

//! Create network message indices for AskForHideBodyMessage class
NetworkMessageIndices *GetIndicesAskForHideBody() {return new IndicesAskForHideBody();}

NetworkMessageFormat &AskForHideBodyMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Body to hide"));
	return format;
}

TMError AskForHideBodyMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesAskForHideBody *>(ctx.GetIndices()))
	const IndicesAskForHideBody *indices = static_cast<const IndicesAskForHideBody *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->vehicle, vehicle))
	return TMOK;
}

//! network message indices for ExplosionDammageEffectsMessage class
class IndicesExplosionDammageEffects : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int owner;
	int shot;
	int directHit;
	int pos;
	int dir;
	int type;
	int enemyDammage;
	//@}

	//! Constructor
	IndicesExplosionDammageEffects();
	NetworkMessageIndices *Clone() const {return new IndicesExplosionDammageEffects;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesExplosionDammageEffects::IndicesExplosionDammageEffects()
{
	owner = -1;
	shot = -1;
	directHit = -1;
	pos = -1;
	dir = -1;
	type = -1;
	enemyDammage = -1;
}

void IndicesExplosionDammageEffects::Scan(NetworkMessageFormatBase *format)
{
	SCAN(owner)
	SCAN(shot)
	SCAN(directHit)
	SCAN(pos)
	SCAN(dir)
	SCAN(type)
	SCAN(enemyDammage)
}

//! Create network message indices for ExplosionDammageEffectsMessage class
NetworkMessageIndices *GetIndicesExplosionDammageEffects() {return new IndicesExplosionDammageEffects();}

NetworkMessageFormat &ExplosionDammageEffectsMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("owner", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Shot owner (who is responsible for explosion)"));
	format.Add("shot", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Shot"));
	format.Add("directHit", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Hitted object"));
	format.Add("pos", NDTVector, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Explosion position"));
	format.Add("dir", NDTVector, NCTNone, DEFVALUE(Vector3, VForward), DOC_MSG("Explosion direction"));
	format.Add("type", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Ammunition type"));
	format.Add("enemyDammage", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Some enemy was damaged"));
	return format;
}

TMError ExplosionDammageEffectsMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesExplosionDammageEffects *>(ctx.GetIndices()))
	const IndicesExplosionDammageEffects *indices = static_cast<const IndicesExplosionDammageEffects *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->owner, owner))
	TMCHECK(ctx.IdxTransferRef(indices->shot, shot))
	TMCHECK(ctx.IdxTransferRef(indices->directHit, directHit))
	TMCHECK(ctx.IdxTransfer(indices->pos, pos))
	TMCHECK(ctx.IdxTransfer(indices->dir, dir))
	TMCHECK(ctx.IdxTransfer(indices->type, type))
	TMCHECK(ctx.IdxTransfer(indices->enemyDammage, enemyDammage))
	return TMOK;
}

IndicesDeleteObject::IndicesDeleteObject()
{
	creator = -1;
	id = -1;
}

void IndicesDeleteObject::Scan(NetworkMessageFormatBase *format)
{
	SCAN(creator)
	SCAN(id)
}

//! Create network message indices for DeleteObjectMessage class
NetworkMessageIndices *GetIndicesDeleteObject() {return new IndicesDeleteObject();}

NetworkMessageFormat &DeleteObjectMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("creator", NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("Id of object to destroy"));
	format.Add("id", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Id of object to destroy"));
	return format;
}

TMError DeleteObjectMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesDeleteObject *>(ctx.GetIndices()))
	const IndicesDeleteObject *indices = static_cast<const IndicesDeleteObject *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransfer(indices->creator, object.creator))
	TMCHECK(ctx.IdxTransfer(indices->id, object.id))
	return TMOK;
}

IndicesDeleteCommand::IndicesDeleteCommand()
{
	creator = -1;
	id = -1;
	subgrp = -1;
	index = -1;
}

void IndicesDeleteCommand::Scan(NetworkMessageFormatBase *format)
{
	SCAN(creator)
	SCAN(id)
	SCAN(subgrp)
	SCAN(index)
}

//! Create network message indices for DeleteCommandMessage class
NetworkMessageIndices *GetIndicesDeleteCommand() {return new IndicesDeleteCommand();}

NetworkMessageFormat &DeleteCommandMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("creator", NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("Id of command to destroy"));
	format.Add("id", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("ID of command to destroy"));
	format.Add("subgrp", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Subgroup that owns command"));
	format.Add("index", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Index of command in subgroup"));
	return format;
}

TMError DeleteCommandMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesDeleteCommand *>(ctx.GetIndices()))
	const IndicesDeleteCommand *indices = static_cast<const IndicesDeleteCommand *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransfer(indices->creator, object.creator))
	TMCHECK(ctx.IdxTransfer(indices->id, object.id))
	TMCHECK(ctx.IdxTransferRef(indices->subgrp, subgrp))
	TMCHECK(ctx.IdxTransfer(indices->index, index))
	return TMOK;
}

IndicesAskForAmmo::IndicesAskForAmmo()
{
	vehicle = -1;
	weapon = -1;
	burst = -1;
}

void IndicesAskForAmmo::Scan(NetworkMessageFormatBase *format)
{
	SCAN(vehicle)
	SCAN(weapon)
	SCAN(burst)
}

//! Create network message indices for AskForAmmoMessage class
NetworkMessageIndices *GetIndicesAskForAmmo() {return new IndicesAskForAmmo();}

NetworkMessageFormat &AskForAmmoMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Vehicle which ammo is changing"));
	format.Add("weapon", NDTInteger, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Weapon index"));
	format.Add("burst", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 1), DOC_MSG("Amount of ammo to decrease"));
	return format;
}

TMError AskForAmmoMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesAskForAmmo *>(ctx.GetIndices()))
	const IndicesAskForAmmo *indices = static_cast<const IndicesAskForAmmo *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->vehicle, vehicle))
	TMCHECK(ctx.IdxTransfer(indices->weapon, weapon))
	TMCHECK(ctx.IdxTransfer(indices->burst, burst))
	return TMOK;
}

//! network message indices for FireWeaponMessage class
class IndicesFireWeapon : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int vehicle;
	int target;
	int weapon;
	int magazineCreator;
	int magazineId;
	//@}

	//! Constructor
	IndicesFireWeapon();
	NetworkMessageIndices *Clone() const {return new IndicesFireWeapon;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesFireWeapon::IndicesFireWeapon()
{
	vehicle = -1;
	target = -1;
	weapon = -1;
	magazineCreator = -1;
	magazineId = -1;
}

void IndicesFireWeapon::Scan(NetworkMessageFormatBase *format)
{
	SCAN(vehicle)
	SCAN(target)
	SCAN(weapon)
	SCAN(magazineCreator)
	SCAN(magazineId)
}

//! Create network message indices for FireWeaponMessage class
NetworkMessageIndices *GetIndicesFireWeapon() {return new IndicesFireWeapon();}

NetworkMessageFormat &FireWeaponMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Firing vehicle"));
	format.Add("target", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Aimed target"));
	format.Add("weapon", NDTInteger, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Firing weapon index"));
	format.Add("magazineCreator", NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("Fired magazine id"));
	format.Add("magazineId", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Fired magazine id"));
	return format;
}

TMError FireWeaponMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesFireWeapon *>(ctx.GetIndices()))
	const IndicesFireWeapon *indices = static_cast<const IndicesFireWeapon *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->vehicle, vehicle))
	TMCHECK(ctx.IdxTransferRef(indices->target, target))
	TMCHECK(ctx.IdxTransfer(indices->weapon, weapon))
	TMCHECK(ctx.IdxTransfer(indices->magazineCreator, magazineCreator))
	TMCHECK(ctx.IdxTransfer(indices->magazineId, magazineId))
	return TMOK;
}

IndicesUpdateWeapons::IndicesUpdateWeapons()
{
	vehicle = -1;
	IndicesUpdateEntityAIWeapons *GetIndicesUpdateEntityAIWeapons();
	weapons = GetIndicesUpdateEntityAIWeapons();
}

IndicesUpdateWeapons::~IndicesUpdateWeapons()
{
	void DeleteIndicesUpdateEntityAIWeapons(IndicesUpdateEntityAIWeapons *weapons);
	DeleteIndicesUpdateEntityAIWeapons(weapons);
}

void IndicesUpdateWeapons::Scan(NetworkMessageFormatBase *format)
{
	SCAN(vehicle)
	void ScanIndicesUpdateEntityAIWeapons(IndicesUpdateEntityAIWeapons *weapons, NetworkMessageFormatBase *format);
	ScanIndicesUpdateEntityAIWeapons(weapons, format);
}

//! Create network message indices for UpdateWeaponsMessage class
NetworkMessageIndices *GetIndicesUpdateWeapons() {return new IndicesUpdateWeapons();}

NetworkMessageFormat &UpdateWeaponsMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Vehicle to update"));
	EntityAI::CreateFormatWeapons(format);
	return format;
}

TMError UpdateWeaponsMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesUpdateWeapons *>(ctx.GetIndices()))
	const IndicesUpdateWeapons *indices = static_cast<const IndicesUpdateWeapons *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->vehicle, vehicle))
	if (vehicle) TMCHECK(vehicle->TransferMsgWeapons(ctx, indices->weapons))
	return TMOK;
}

//! network message indices for AddWeaponCargoMessage class
class IndicesAddWeaponCargo : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int vehicle;
	int weapon;
	//@}

	//! Constructor
	IndicesAddWeaponCargo();
	NetworkMessageIndices *Clone() const {return new IndicesAddWeaponCargo;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesAddWeaponCargo::IndicesAddWeaponCargo()
{
	vehicle = -1;
	weapon = -1;
}

void IndicesAddWeaponCargo::Scan(NetworkMessageFormatBase *format)
{
	SCAN(vehicle)
	SCAN(weapon)
}

//! Create network message indices for AddWeaponCargoMessage class
NetworkMessageIndices *GetIndicesAddWeaponCargo() {return new IndicesAddWeaponCargo();}

NetworkMessageFormat &AddWeaponCargoMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Asked vehicle"));
	format.Add("weapon", NDTString, NCTDefault, DEFVALUE(RString, ""), DOC_MSG("Name of weapon type to add"));
	return format;
}

TMError AddWeaponCargoMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesAddWeaponCargo *>(ctx.GetIndices()))
	const IndicesAddWeaponCargo *indices = static_cast<const IndicesAddWeaponCargo *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->vehicle, vehicle))
	TMCHECK(ctx.IdxTransfer(indices->weapon, weapon))
	return TMOK;
}

//! network message indices for RemoveWeaponCargoMessage class
class IndicesRemoveWeaponCargo : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int vehicle;
	int weapon;
	//@}

	//! Constructor
	IndicesRemoveWeaponCargo();
	NetworkMessageIndices *Clone() const {return new IndicesRemoveWeaponCargo;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesRemoveWeaponCargo::IndicesRemoveWeaponCargo()
{
	vehicle = -1;
	weapon = -1;
}

void IndicesRemoveWeaponCargo::Scan(NetworkMessageFormatBase *format)
{
	SCAN(vehicle)
	SCAN(weapon)
}

//! Create network message indices for RemoveWeaponCargoMessage class
NetworkMessageIndices *GetIndicesRemoveWeaponCargo() {return new IndicesRemoveWeaponCargo();}

NetworkMessageFormat &RemoveWeaponCargoMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Asked vehicle"));
	format.Add("weapon", NDTString, NCTDefault, DEFVALUE(RString, ""), DOC_MSG("Name of weapon type to remove"));
	return format;
}

TMError RemoveWeaponCargoMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesRemoveWeaponCargo *>(ctx.GetIndices()))
	const IndicesRemoveWeaponCargo *indices = static_cast<const IndicesRemoveWeaponCargo *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->vehicle, vehicle))
	TMCHECK(ctx.IdxTransfer(indices->weapon, weapon))
	return TMOK;
}

//! network message indices for AddMagazineCargoMessage class
class IndicesAddMagazineCargo : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int vehicle;
	int magazine;
	//@}

	//! Constructor
	IndicesAddMagazineCargo();
	NetworkMessageIndices *Clone() const {return new IndicesAddMagazineCargo;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesAddMagazineCargo::IndicesAddMagazineCargo()
{
	vehicle = -1;
	magazine = -1;
}

void IndicesAddMagazineCargo::Scan(NetworkMessageFormatBase *format)
{
	SCAN(vehicle)
	SCAN(magazine)
}

//! Create network message indices for AddMagazineCargoMessage class
NetworkMessageIndices *GetIndicesAddMagazineCargo() {return new IndicesAddMagazineCargo();}

NetworkMessageFormat &AddMagazineCargoMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Asked vehicle"));
	format.Add("magazine", NDTObject, NCTNone, DEFVALUE_MSG(NMTMagazine), DOC_MSG("Magazine to add"));
	return format;
}

TMError AddMagazineCargoMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesAddMagazineCargo *>(ctx.GetIndices()))
	const IndicesAddMagazineCargo *indices = static_cast<const IndicesAddMagazineCargo *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->vehicle, vehicle))
	TMCHECK(ctx.IdxTransferContent(indices->magazine, magazine))

	// IdxTransferObject not implemented for Ref<Type>
	/*
	if (ctx.IsSending())
	{
		DoAssert(magazine);
		TMCHECK(ctx.IdxTransferObject(indices->magazine, *magazine))
	}
	else
	{
		magazine = Magazine::CreateObject(ctx);
		TMCHECK(ctx.IdxTransferObject(indices->magazine, *magazine))
	}
	*/
	return TMOK;
}

//! network message indices for RemoveMagazineCargoMessage class
class IndicesRemoveMagazineCargo : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int vehicle;
	int creator;
	int id;
	//@}

	//! Constructor
	IndicesRemoveMagazineCargo();
	NetworkMessageIndices *Clone() const {return new IndicesRemoveMagazineCargo;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesRemoveMagazineCargo::IndicesRemoveMagazineCargo()
{
	vehicle = -1;
	creator = -1;
	id = -1;
}

void IndicesRemoveMagazineCargo::Scan(NetworkMessageFormatBase *format)
{
	SCAN(vehicle)
	SCAN(creator)
	SCAN(id)
}

//! Create network message indices for RemoveMagazineCargoMessage class
NetworkMessageIndices *GetIndicesRemoveMagazineCargo() {return new IndicesRemoveMagazineCargo();}

NetworkMessageFormat &RemoveMagazineCargoMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Asked vehicle"));
	format.Add("creator", NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("ID of magazine to remove"));
	format.Add("id", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("ID of magazine to remove"));
	return format;
}

TMError RemoveMagazineCargoMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesRemoveMagazineCargo *>(ctx.GetIndices()))
	const IndicesRemoveMagazineCargo *indices = static_cast<const IndicesRemoveMagazineCargo *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->vehicle, vehicle))
	TMCHECK(ctx.IdxTransfer(indices->creator, creator))
	TMCHECK(ctx.IdxTransfer(indices->id, id))
	return TMOK;
}

NetworkMessageType VehicleInitCmd::GetNMType(NetworkMessageClass cls) const
{
	return NMTVehicleInit;
}

//! network message indices for VehicleInitCmd class
class IndicesVehicleInit : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int vehicle;
	int init;
	//@}

	//! Constructor
	IndicesVehicleInit();
	NetworkMessageIndices *Clone() const {return new IndicesVehicleInit;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesVehicleInit::IndicesVehicleInit()
{
	vehicle = -1;
	init = -1;
}

void IndicesVehicleInit::Scan(NetworkMessageFormatBase *format)
{
	SCAN(vehicle)
	SCAN(init)
}

//! Create network message indices for VehicleInitCmd class
NetworkMessageIndices *GetIndicesVehicleInit() {return new IndicesVehicleInit();}

NetworkMessageFormat &VehicleInitCmd::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Vehicle which is initialized"));
	format.Add("init", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Initialization statement"));
	return format;
}

TMError VehicleInitCmd::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesVehicleInit *>(ctx.GetIndices()))
	const IndicesVehicleInit *indices = static_cast<const IndicesVehicleInit *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->vehicle, vehicle))
	TMCHECK(ctx.IdxTransfer(indices->init, init))
	return TMOK;
}

//! network message indices for VehicleDestroyedMessage class
class IndicesVehicleDestroyed : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int killed;
	int killer;
	//@}

	//! Constructor
	IndicesVehicleDestroyed();
	NetworkMessageIndices *Clone() const {return new IndicesVehicleDestroyed;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesVehicleDestroyed::IndicesVehicleDestroyed()
{
	killed = -1;
	killer = -1;
}

void IndicesVehicleDestroyed::Scan(NetworkMessageFormatBase *format)
{
	SCAN(killed)
	SCAN(killer)
}

//! Create network message indices for VehicleDestroyedMessage class
NetworkMessageIndices *GetIndicesVehicleDestroyed() {return new IndicesVehicleDestroyed();}

NetworkMessageFormat &VehicleDestroyedMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("killed", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Destroyed vehicle"));
	format.Add("killer", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Who is responsible for destroying"));
	return format;
}

TMError VehicleDestroyedMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesVehicleDestroyed *>(ctx.GetIndices()))
	const IndicesVehicleDestroyed *indices = static_cast<const IndicesVehicleDestroyed *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->killed, killed))
	TMCHECK(ctx.IdxTransferRef(indices->killer, killer))
	return TMOK;
}

//! network message indices for MarkerDeleteMessage class
class IndicesMarkerDelete : public NetworkMessageIndices
{
public:
	//! index of field in message format
	int name;

	//! Constructor
	IndicesMarkerDelete();
	NetworkMessageIndices *Clone() const {return new IndicesMarkerDelete;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesMarkerDelete::IndicesMarkerDelete()
{
	name = -1;
}

void IndicesMarkerDelete::Scan(NetworkMessageFormatBase *format)
{
	SCAN(name)
}

//! Create network message indices for MarkerDeleteMessage class
NetworkMessageIndices *GetIndicesMarkerDelete() {return new IndicesMarkerDelete();}

NetworkMessageFormat &MarkerDeleteMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("name", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Name of marker"));
	return format;
}

TMError MarkerDeleteMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesMarkerDelete *>(ctx.GetIndices()))
	const IndicesMarkerDelete *indices = static_cast<const IndicesMarkerDelete *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransfer(indices->name, name))
	return TMOK;
}

IndicesMarkerCreate::IndicesMarkerCreate()
{
	channel = -1;
	sender = -1;
	units = -1;

	IndicesMarker *GetIndicesMarker();
	marker = GetIndicesMarker();
}

IndicesMarkerCreate::~IndicesMarkerCreate()
{
	void DeleteIndicesMarker(IndicesMarker *marker);
	DeleteIndicesMarker(marker);
}

void IndicesMarkerCreate::Scan(NetworkMessageFormatBase *format)
{
	SCAN(channel)
	SCAN(sender)
	SCAN(units)

	void ScanIndicesMarker(IndicesMarker *marker, NetworkMessageFormatBase *format);
	ScanIndicesMarker(marker, format);
}

//! Create network message indices for MarkerCreateMessage class
NetworkMessageIndices *GetIndicesMarkerCreate() {return new IndicesMarkerCreate();}

NetworkMessageFormat &MarkerCreateMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("channel", NDTInteger, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Chat channel (who will see the marker)"));
	format.Add("sender", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Sender unit"));
	format.Add("units", NDTRefArray, NCTNone, DEFVALUEREFARRAY, DOC_MSG("List of receiving units"));
	ArcadeMarkerInfo::CreateFormat(format);
	return format;
}

TMError MarkerCreateMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesMarkerCreate *>(ctx.GetIndices()))
	const IndicesMarkerCreate *indices = static_cast<const IndicesMarkerCreate *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransfer(indices->channel, channel))
	TMCHECK(ctx.IdxTransferRef(indices->sender, sender))
	TMCHECK(ctx.IdxTransferRefs(indices->units, units))
	return marker.TransferMsg(ctx, indices->marker);
}

//! network message indices for NetworkCommandMessage class
class IndicesNetworkCommand : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int type;
	int content;
	//@}

	//! Constructor
	IndicesNetworkCommand();
	NetworkMessageIndices *Clone() const {return new IndicesNetworkCommand;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesNetworkCommand::IndicesNetworkCommand()
{
	type = -1;
	content = -1;
}

void IndicesNetworkCommand::Scan(NetworkMessageFormatBase *format)
{
	SCAN(type)
	SCAN(content)
}

//! Create network message indices for NetworkCommandMessage class
NetworkMessageIndices *GetIndicesNetworkCommand() {return new IndicesNetworkCommand();}

NetworkMessageFormat &NetworkCommandMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("type", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Type of command"));
	format.Add("content", NDTRawData, NCTNone, DEFVALUERAWDATA, DOC_MSG("Parameters of command"));
	return format;
}

TMError NetworkCommandMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesNetworkCommand *>(ctx.GetIndices()))
	const IndicesNetworkCommand *indices = static_cast<const IndicesNetworkCommand *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransfer(indices->type, (int &)type))
	TMCHECK(ctx.IdxTransfer(indices->content, content))
	return TMOK;
}

//! network message indices for IntegrityQuestionMessage class
class IndicesIntegrityQuestion : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int id;
	int type;
	int name;
	int offset;
	int size;
	//@}

	//! Constructor
	IndicesIntegrityQuestion();
	NetworkMessageIndices *Clone() const {return new IndicesIntegrityQuestion;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesIntegrityQuestion::IndicesIntegrityQuestion()
{
	id = -1;
	type = -1;
	name = -1;
	offset = -1;
	size = -1;
}

void IndicesIntegrityQuestion::Scan(NetworkMessageFormatBase *format)
{
	SCAN(id)
	SCAN(type)
	SCAN(name)
	SCAN(offset)
	SCAN(size)
}

//! Create network message indices for IntegrityQuestionMessage class
NetworkMessageIndices *GetIndicesIntegrityQuestion() {return new IndicesIntegrityQuestion();}

NetworkMessageFormat &IntegrityQuestionMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("id", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Unique id of question"));
	format.Add("type", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Type of question"));
	format.Add("name", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Question name"));
	format.Add("offset", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Region in question"));
	format.Add("size", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Region in question"));
	return format;
}

TMError IntegrityQuestionMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesIntegrityQuestion *>(ctx.GetIndices()))
	const IndicesIntegrityQuestion *indices = static_cast<const IndicesIntegrityQuestion *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransfer(indices->id, id))
	TMCHECK(ctx.IdxTransfer(indices->type, (int &)type))
	TMCHECK(ctx.IdxTransfer(indices->name, q.name))
	TMCHECK(ctx.IdxTransfer(indices->offset, q.offset))
	TMCHECK(ctx.IdxTransfer(indices->size, q.size))
	return TMOK;
}

//! network message indices for IntegrityAnswerMessage class
class IndicesIntegrityAnswer : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int answer;
	int id;
	int type;
	//@}

	//! Constructor
	IndicesIntegrityAnswer();
	NetworkMessageIndices *Clone() const {return new IndicesIntegrityAnswer;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesIntegrityAnswer::IndicesIntegrityAnswer()
{
	answer = -1;
	id = -1;
	type = -1;
}

void IndicesIntegrityAnswer::Scan(NetworkMessageFormatBase *format)
{
	SCAN(answer)
	SCAN(id)
	SCAN(type)
}

//! Create network message indices for IntegrityAnswerMessage class
NetworkMessageIndices *GetIndicesIntegrityAnswer() {return new IndicesIntegrityAnswer();}

NetworkMessageFormat &IntegrityAnswerMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("id", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Unique id of question"));
	format.Add("type", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Type of question"));
	format.Add("answer", NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("Answer value (CRC of selected data)"));
	return format;
}

TMError IntegrityAnswerMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesIntegrityAnswer *>(ctx.GetIndices()))
	const IndicesIntegrityAnswer *indices = static_cast<const IndicesIntegrityAnswer *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransfer(indices->id, id))
	TMCHECK(ctx.IdxTransfer(indices->type, (int &)type))
	TMCHECK(ctx.IdxTransfer(indices->answer, answer))
	return TMOK;
}

//! network message indices for PlayerStateMessage class
class IndicesPlayerState : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int player;
	int state;
	//@}

	//! Constructor
	IndicesPlayerState();
	NetworkMessageIndices *Clone() const {return new IndicesPlayerState;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesPlayerState::IndicesPlayerState()
{
	player = -1;
	state = -1;
}

void IndicesPlayerState::Scan(NetworkMessageFormatBase *format)
{
	SCAN(player)
	SCAN(state)
}

//! Create network message indices for PlayerStateMessage class
NetworkMessageIndices *GetIndicesPlayerState() {return new IndicesPlayerState();}

NetworkMessageFormat &PlayerStateMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("player", NDTInteger, NCTNone, DEFVALUE(int, AI_PLAYER), DOC_MSG("Client (player) ID"));
	format.Add("state", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, NGSNone), DOC_MSG("New state of player"));
	return format;
}

TMError PlayerStateMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesPlayerState *>(ctx.GetIndices()))
	const IndicesPlayerState *indices = static_cast<const IndicesPlayerState *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransfer(indices->player, (int &)player))
	TMCHECK(ctx.IdxTransfer(indices->state, (int &)state))
	return TMOK;
}

AttachPersonMessage::AttachPersonMessage(Person *p)
{
	Assert(p);
	person = p;
	unit = person->Brain();
}

IndicesAttachPerson::IndicesAttachPerson()
{
	person = -1;
	unit = -1;
}

void IndicesAttachPerson::Scan(NetworkMessageFormatBase *format)
{
	SCAN(person)
	SCAN(unit)
}

//! Create network message indices for AttachPersonMessage class
NetworkMessageIndices *GetIndicesAttachPerson() {return new IndicesAttachPerson();}

NetworkMessageFormat &AttachPersonMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("person", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Person to attach"));
	format.Add("unit", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Unit to attach"));
	return format;
}

TMError AttachPersonMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesAttachPerson *>(ctx.GetIndices()))
	const IndicesAttachPerson *indices = static_cast<const IndicesAttachPerson *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->person, person))
	TMCHECK(ctx.IdxTransferRef(indices->unit, unit))
	return TMOK;
}
/*
NetworkMessageFormat &RespawnQueueItem::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("type", NDTString, NCTNone, DEFVALUE(RString, ""));
	format.Add("side", NDTInteger, NCTNone, DEFVALUE(int, TSideUnknown));
	format.Add("id", NDTInteger, NCTNone, DEFVALUE(int, -1));
	format.Add("varname", NDTString, NCTNone, DEFVALUE(RString, ""));
	// info
	format.Add("firstname", NDTString, NCTNone, DEFVALUE(RString, ""));
	format.Add("name", NDTString, NCTNone, DEFVALUE(RString, ""));
	format.Add("rank", NDTInteger, NCTNone, DEFVALUE(int, RankPrivate));
	format.Add("experience", NDTFloat, NCTNone, DEFVALUE(float, 0));

	format.Add("group", NDTRef, NCTNone, DEFVALUENULL);
	format.Add("position", NDTVector, NCTNone, DEFVALUE(Vector3, VZero));
	format.Add("time", NDTTime, NCTNone, DEFVALUE(Time, Time(0)));
	format.Add("player", NDTInteger, NCTNone, DEFVALUE(int, 0));
	return format;
}

TMError RespawnQueueItem::TransferMsg(NetworkMessageContext &ctx)
{
	if (ctx.IsSending())
	{
		RString typeName = type ? type->GetName() : "";
		TMCHECK(ctx.Transfer("type", typeName))
	}
	else
	{
		RString typeName;
		TMCHECK(ctx.Transfer("type", typeName))
		if (typeName.GetLength() > 0)
			type = static_cast<const VehicleType *>(VehicleTypes.New(typeName));
		else
			type = NULL;
	}
	TMCHECK(ctx.Transfer("side", (int &)side))
	TMCHECK(ctx.Transfer("id", id))
	TMCHECK(ctx.Transfer("varname", varname))

	TMCHECK(ctx.Transfer("firstname", info._firstname))
	TMCHECK(ctx.Transfer("name", info._name))
	TMCHECK(ctx.Transfer("rank", (int &)info._rank))
	TMCHECK(ctx.Transfer("experience", info._experience))

	TMCHECK(ctx.TransferRef("group", group))
	TMCHECK(ctx.Transfer("position", position))
	TMCHECK(ctx.Transfer("time", time))
	TMCHECK(ctx.Transfer("player", player))
	return TMOK;
}
*/

IndicesSetFlagOwner::IndicesSetFlagOwner()
{
	owner = -1;
	carrier = -1;
}

void IndicesSetFlagOwner::Scan(NetworkMessageFormatBase *format)
{
	SCAN(owner)
	SCAN(carrier)
}

//! Create network message indices for SetFlagOwnerMessage class
NetworkMessageIndices *GetIndicesSetFlagOwner() {return new IndicesSetFlagOwner();}

//! Create network message indices for SetFlagCarrierMessage class
NetworkMessageIndices *GetIndicesSetFlagCarrier() {return new IndicesSetFlagOwner();}

NetworkMessageFormat &SetFlagOwnerMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("owner", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Flag owner"));
	format.Add("carrier", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Flag carrier"));
	return format;
}

TMError SetFlagOwnerMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesSetFlagOwner *>(ctx.GetIndices()))
	const IndicesSetFlagOwner *indices = static_cast<const IndicesSetFlagOwner *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->owner, owner))
	TMCHECK(ctx.IdxTransferRef(indices->carrier, carrier))
	return TMOK;
}

//! network message indices for PlayerIdentity class
class IndicesLogin : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int dpnid;
	int playerid;
	int id;
	int name;
	int face;
	int glasses;
	int speaker;
	int pitch;
	int squad;
	int fullname;
	int email;
	int icq;
	int remark;
	int state;
	int version;
	//@}

	//! Constructor
	IndicesLogin();
	NetworkMessageIndices *Clone() const {return new IndicesLogin;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesLogin::IndicesLogin()
{
	dpnid = -1;
	playerid = -1;
	id = -1;
	name = -1;
	face = -1;
	glasses = -1;
	speaker = -1;
	pitch = -1;
	squad = -1;
	fullname = -1;
	email = -1;
	icq = -1;
	remark = -1;
	state = -1;
	version = -1;

}

void IndicesLogin::Scan(NetworkMessageFormatBase *format)
{
	SCAN(dpnid)
	SCAN(playerid)
	SCAN(id)
	SCAN(name)
	SCAN(face)
	SCAN(glasses)
	SCAN(speaker)
	SCAN(pitch)
	SCAN(squad)
	SCAN(fullname)
	SCAN(email)
	SCAN(icq)
	SCAN(remark)
	SCAN(state)
	SCAN(version)
}

IndicesPlayerUpdate::IndicesPlayerUpdate()
{
	dpnid = -1;
	minPing = -1;
	avgPing = -1;
	maxPing = -1;
	minBandwidth = -1;
	avgBandwidth = -1;
	maxBandwidth = -1;
	desync = -1;
	rights = -1;
}

void IndicesPlayerUpdate::Scan(NetworkMessageFormatBase *format)
{
	SCAN(dpnid);
	SCAN(minPing)
	SCAN(avgPing)
	SCAN(maxPing)
	SCAN(minBandwidth)
	SCAN(avgBandwidth)
	SCAN(maxBandwidth)
	SCAN(desync)
	SCAN(rights)
}

//! Create network message indices for IndicesPlayerUpdate class
NetworkMessageIndices *GetIndicesLogin()
{
	return new IndicesLogin();
}

//! Create network message indices for IndicesPlayerUpdate class position update
NetworkMessageIndices *GetIndicesPlayerUpdate()
{
	return new IndicesPlayerUpdate();
}

PlayerIdentity::PlayerIdentity()
{
	_minPing = 0,_avgPing = 0 ,_maxPing = 0;
	_minBandwidth = 0, _avgBandwidth = 0 , _maxBandwidth =0;
	_desync = 0;

	_rights = PRNone;
	
	destroy = false;
  failedLogin = 0;

  kickOffTime = UITIME_MAX;
	kickOffState = KOWait;
}

PlayerIdentity::~PlayerIdentity()
{
}

NetworkMessageType PlayerIdentity::GetNMType(NetworkMessageClass cls) const
{
	if (cls==NMCUpdatePosition) return NMTPlayerUpdate;
	return NMTLogin;
}

NetworkMessageFormat &PlayerIdentity::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	switch (cls)
	{
		case NMCUpdatePosition:
			format.Add("dpnid", NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("Client (player) ID"));
			format.Add("minPing",NDTInteger,NCTSmallUnsigned, DEFVALUE(int, 10), DOC_MSG("Ping range estimation"), ET_ABS_DIF, ERR_COEF_VALUE_MINOR);
			format.Add("avgPing",NDTInteger,NCTSmallUnsigned, DEFVALUE(int, 100), DOC_MSG("Ping range estimation"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);
			format.Add("maxPing",NDTInteger,NCTSmallUnsigned, DEFVALUE(int, 1000), DOC_MSG("Ping range estimation"), ET_ABS_DIF, ERR_COEF_VALUE_MINOR);
			format.Add("minBandwidth",NDTInteger, NCTSmallUnsigned, DEFVALUE(int,  2), DOC_MSG("Bandwidth estimation (in kbps)"), ET_ABS_DIF, ERR_COEF_VALUE_MINOR);
			format.Add("avgBandwidth",NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 14), DOC_MSG("Bandwidth estimation (in kbps)"), ET_ABS_DIF, ERR_COEF_VALUE_MINOR);
			format.Add("maxBandwidth",NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 28), DOC_MSG("Bandwidth estimation (in kbps)"), ET_ABS_DIF, ERR_COEF_VALUE_MINOR);
			format.Add("desync",NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Current desync level (max. error of unsent messages)"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);
			format.Add("rights",NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Special rights of given player"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);
			return format;
		default:
			format.Add("dpnid", NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("Client (player) ID"));
			format.Add("playerid", NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("ID unique in session (shorter than dpnid)"));
			format.Add("id", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Unique id of player (derivated from CD key)"));
			format.Add("name", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Nick (short) name of player"));
			format.Add("face", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Selected face"));
			format.Add("glasses", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Selected glasses"));
			format.Add("speaker", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Selected speaker"));
			format.Add("pitch", NDTFloat, NCTNone, DEFVALUE(float, 1.0f), DOC_MSG("Selected voice pitch"));
			format.Add("squad", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("unique id (URL) of squad"));
			format.Add("fullname", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Full name of player"));
			format.Add("email", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("E-mail of player"));
			format.Add("icq", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("ICQ of player"));
			format.Add("remark", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Remark about player"));
			format.Add("state", NDTInteger, NCTNone, DEFVALUE(int, NGSNone), DOC_MSG("State of player's network client"));
			format.Add("version", NDTInteger, NCTNone, DEFVALUE(int, 0), DOC_MSG("Version player is using"));
			return format;
	}
}

TMError PlayerIdentity::TransferMsg(NetworkMessageContext &ctx)
{
	switch (ctx.GetClass())
	{
		case NMCUpdatePosition:
		{
			Assert(dynamic_cast<const IndicesPlayerUpdate *>(ctx.GetIndices()))
			const IndicesPlayerUpdate *indices = static_cast<const IndicesPlayerUpdate *>(ctx.GetIndices());

			ITRANSF(minPing);
			ITRANSF(avgPing);
			ITRANSF(maxPing);
			ITRANSF(minBandwidth);
			ITRANSF(avgBandwidth);
			ITRANSF(maxBandwidth);
			ITRANSF(desync);
			ITRANSF(rights);

			return TMOK;
		}
		default:
		{
			Assert(dynamic_cast<const IndicesLogin *>(ctx.GetIndices()))
			const IndicesLogin *indices = static_cast<const IndicesLogin *>(ctx.GetIndices());

			TMCHECK(ctx.IdxTransfer(indices->dpnid, (int &)dpnid))
			TMCHECK(ctx.IdxTransfer(indices->playerid, (int &)playerid))
			TMCHECK(ctx.IdxTransfer(indices->id, id))
			TMCHECK(ctx.IdxTransfer(indices->name, name))
			TMCHECK(ctx.IdxTransfer(indices->face, face))
			TMCHECK(ctx.IdxTransfer(indices->glasses, glasses))
			TMCHECK(ctx.IdxTransfer(indices->speaker, speaker))
			TMCHECK(ctx.IdxTransfer(indices->pitch, pitch))
			TMCHECK(ctx.IdxTransfer(indices->squad, squadId))
			TMCHECK(ctx.IdxTransfer(indices->fullname, fullname))
			TMCHECK(ctx.IdxTransfer(indices->email, email))
			TMCHECK(ctx.IdxTransfer(indices->icq, icq))
			TMCHECK(ctx.IdxTransfer(indices->remark, remark))
			TMCHECK(ctx.IdxTransfer(indices->state, (int &)state))
			TMCHECK(ctx.IdxTransfer(indices->version, version))

			if (!ctx.IsSending() && kickOffTime==UITIME_MAX)
			{
				__int64 idNum = _atoi64(id);
				if (idNum==0)
				{
					float timeToPlay = GRandGen.Gauss(10,20,30);
					kickOffTime = Glob.uiTime+timeToPlay;
          kickOffState = KOWait;
				}
				else if (idNum>110000000 || idNum>10000000 && idNum<100000000) // 10 - 100,110 milions
				{
					float timeToPlay = GRandGen.Gauss(30,60,90);
					kickOffTime = Glob.uiTime+timeToPlay;
          kickOffState = KOWait;
				}
			}
			return TMOK;
		}
	}
}

RString PlayerIdentity::GetName() const
{
	if (squad) return name + RString(" [") + squad->nick + RString("]");
	else return name;
}

//! network message indices for SquadIdentity class
class IndicesSquad : public NetworkMessageIndices
{
public:
	//@{
	//! index of field in message format
	int id;
	int nick;
	int name;
	int email;
	int web;
	int picture;
	int title;
	//@}

	//! Constructor
	IndicesSquad();
	NetworkMessageIndices *Clone() const {return new IndicesSquad;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesSquad::IndicesSquad()
{
	id = -1;
	nick = -1;
	name = -1;
	email = -1;
	web = -1;
	picture = -1;
	title = -1;
}

void IndicesSquad::Scan(NetworkMessageFormatBase *format)
{
	SCAN(id)
	SCAN(nick)
	SCAN(name)
	SCAN(email)
	SCAN(web)
	SCAN(picture)
	SCAN(title)
}

//! Create network message indices for SquadIdentity class
NetworkMessageIndices *GetIndicesSquad() {return new IndicesSquad();}

NetworkMessageFormat &SquadIdentity::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("id", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Unique id of squad (URL of XML page)"));
	format.Add("nick", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Nick (short) name of squad"));
	format.Add("name", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Full name of squad"));
	format.Add("email", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("E-mail of squad administrator"));
	format.Add("web", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Web page of squad"));
	format.Add("picture", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Picture of squad (shown on vehicles)"));
	format.Add("title", NDTString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Title of squad (shown on vehicles)"));
	return format;
}

TMError SquadIdentity::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesSquad *>(ctx.GetIndices()))
	const IndicesSquad *indices = static_cast<const IndicesSquad *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransfer(indices->id, id))
	TMCHECK(ctx.IdxTransfer(indices->nick, nick))
	TMCHECK(ctx.IdxTransfer(indices->name, name))
	TMCHECK(ctx.IdxTransfer(indices->email, email))
	TMCHECK(ctx.IdxTransfer(indices->web, web))
	TMCHECK(ctx.IdxTransfer(indices->picture, picture))
	TMCHECK(ctx.IdxTransfer(indices->title, title))
	return TMOK;
}

IndicesShowTarget::IndicesShowTarget()
{
	vehicle = -1;
	target = -1;
}

void IndicesShowTarget::Scan(NetworkMessageFormatBase *format)
{
	SCAN(vehicle)
	SCAN(target)
}

//! Create network message indices for ShowTargetMessage class
NetworkMessageIndices *GetIndicesShowTarget() {return new IndicesShowTarget();}

NetworkMessageFormat &ShowTargetMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Player person"));
	format.Add("target", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Target to show"));
	return format;
}

TMError ShowTargetMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesShowTarget *>(ctx.GetIndices()))
	const IndicesShowTarget *indices = static_cast<const IndicesShowTarget *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->vehicle, vehicle))
	TMCHECK(ctx.IdxTransferRef(indices->target, target))
	return TMOK;
}

IndicesShowGroupDir::IndicesShowGroupDir()
{
	vehicle = -1;
	dir = -1;
}

void IndicesShowGroupDir::Scan(NetworkMessageFormatBase *format)
{
	SCAN(vehicle)
	SCAN(dir)
}

//! Create network message indices for ShowGroupDirMessage class
NetworkMessageIndices *GetIndicesShowGroupDir() {return new IndicesShowGroupDir();}

NetworkMessageFormat &ShowGroupDirMessage::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("vehicle", NDTRef, NCTNone, DEFVALUENULL, DOC_MSG("Player person"));
	format.Add("dir", NDTVector, NCTNone, DEFVALUE(Vector3, VForward), DOC_MSG("Direction to show"));
	return format;
}

TMError ShowGroupDirMessage::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesShowGroupDir *>(ctx.GetIndices()))
	const IndicesShowGroupDir *indices = static_cast<const IndicesShowGroupDir *>(ctx.GetIndices());

	TMCHECK(ctx.IdxTransferRef(indices->vehicle, vehicle))
	TMCHECK(ctx.IdxTransfer(indices->dir, dir))
	return TMOK;
}


//! Declare static variable for message format
#define DECLARE_FORMAT(macro, class, name, description, group) \
	static NetworkMessageFormat items##name; \
	NetworkMessageIndices *GetIndices##name();

NETWORK_MESSAGE_TYPES(DECLARE_FORMAT)

//! Add (create) format to static array of formats
#define FORMAT_SIMPLE(dummy, name) \
	GMsgFormats[curMsgFormat++] = name##Message::CreateFormat(NMCCreate, items##name).Init(GetIndices##name())
//! Add (create) format to static array of formats
#define FORMAT_CREATE(type, format) \
	GMsgFormats[curMsgFormat++] = type::CreateFormat(NMCCreate, items##format).Init(GetIndices##format())
//! Add (update) format to static array of formats
#define FORMAT_UPDATE(type, format) \
	GMsgFormats[curMsgFormat++] = type::CreateFormat(NMCUpdateGeneric, items##format).Init(GetIndices##format())
//! Add (update position) format to static array of formats
#define FORMAT_UPDATE_POSITION(type, format) \
	GMsgFormats[curMsgFormat++] = type::CreateFormat(NMCUpdatePosition, items##format).Init(GetIndices##format())

//! Add (update dammage) format to static array of formats
#define FORMAT_UPDATE_DAMMAGE(type, format) \
	GMsgFormats[curMsgFormat++] = type::CreateFormat(NMCUpdateDammage, items##format).Init(GetIndices##format())

//! number of registered local (static) message formats
static int curMsgFormat = 0;

//! local (static) message formats
NetworkMessageFormat *GMsgFormats[NMTN];

#if DOCUMENT_MSG_FORMATS

#define NDT_DEFINE_ENUM_NAME(type,name,description) #name,

static const char *ItemTypeNames[] = {
	NETWORK_DATA_TYPES(NDT_DEFINE_ENUM_NAME)
};

#define NDT_ENUM_DESCRIPTION(type,name,description) description,

static const char *ItemTypeDescriptions[] = {
	NETWORK_DATA_TYPES(NDT_ENUM_DESCRIPTION)
};

#define NMT_ENUM_DESCRIPTION(macro, class, name, description, group) description,

static const char *MessageTypeDescriptions[] = {
	NETWORK_MESSAGE_TYPES(NMT_ENUM_DESCRIPTION)
};

#define NMT_ENUM_GROUP(macro, class, name, description, group) #group,

static const char *MessageTypeGroups[] = {
	NETWORK_MESSAGE_TYPES(NMT_ENUM_GROUP)
};

#include <Strings/bstring.hpp>

void WriteF(QOStream &out, int indent, const char *format, ...)
{
	va_list arglist;
	va_start(arglist, format);

	for (int i=0; i<indent; i++) out.put('\t');

	BString<512> buffer;
	vsprintf(buffer, format, arglist);
	strcat(buffer, "\r\n");
	out.write(buffer, strlen(buffer));

	va_end(arglist);
}

void DocumentFormat(QOStream &out, int index)
{
	WriteF(out, 2, "<message name=\"%s\" id=\"%d\" group=\"%s\">", NetworkMessageTypeNames[index], index, MessageTypeGroups[index]);
	WriteF(out, 2, MessageTypeDescriptions[index]);
	const NetworkMessageFormat &format = *GMsgFormats[index];
	WriteF(out, 3, "<items>");
	for (int i=0; i<format.NItems(); i++)
	{
		const NetworkMessageFormatItem &item = format.GetItem(i);
		WriteF(out, 4, "<item name=\"%s\" type=\"%s\">", (const char *)item.name, ItemTypeNames[item.type]);
		WriteF(out, 4, format._descriptions[i]);
		WriteF(out, 4, "</item>");
	}
	WriteF(out, 3, "</items>");
	WriteF(out, 2, "</message>");
}
#endif

//! Initialization of local (static) message formats

#define NMT_DEFINE_FORMAT(macro, class, name, description, group) macro(class, name);

void InitMsgFormats()
{
	if (curMsgFormat > 0) return;
	NETWORK_MESSAGE_TYPES(NMT_DEFINE_FORMAT)
	DoAssert(curMsgFormat == NMTN);
	
#if DOCUMENT_MSG_FORMATS
	QOFStream out("messages.xml");
	WriteF(out, 0, "<?xml version=\"1.0\"?>");
	WriteF(out, 0, "<?xml-stylesheet href=\"messages.xsl\" type=\"text/xsl\"?>");

	WriteF(out, 0, "");
	WriteF(out, 0, "<root>");

	WriteF(out, 1, "<types>");
	for (int i=0; i<sizeof(ItemTypeNames)/sizeof(*ItemTypeNames); i++)
	{
		WriteF(out, 2, "<type name=\"%s\" id=\"%d\">", ItemTypeNames[i], i);
		WriteF(out, 2, ItemTypeDescriptions[i]);
		WriteF(out, 2, "</type>");
	}
	WriteF(out, 1, "</types>");
	
	WriteF(out, 1, "<messages>");
	for (int i=0; i<NMTN; i++) DocumentFormat(out, i);
	WriteF(out, 1, "</messages>");
	
	WriteF(out, 0, "</root>");
	out.close();
#endif
}

//! Destroy all local (static) message formats
void DestroyMsgFormats()
{
	for (int i=0; i<NMTN; i++)
	{
		if (GMsgFormats[i])
		{
			GMsgFormats[i]->Clear();
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
// Squad checking

//! XML parser for squad xml document
class SquadParser : public SAXParser
{
protected:
	//! identity of checked player
	PlayerIdentity *_identity;
	//! squad of checked player
	SquadIdentity *_squad;

	//! currently read element
	RString _element;
	//! inside <squad> ... </squad>
	bool _ctxSquad;
	//! inside <member> ... </member>
	bool _ctxMember;
	//! found <member> record for checked player
	bool _found;
	//! player nick match with nick given in <member> record
	bool _validated;

public:
	//! Constructor
	/*!
	\param identity identity of checked player
	\param squad squad of checked player
	*/
	SquadParser(PlayerIdentity *identity, SquadIdentity *squad)
	{
		_identity = identity; _squad = squad;
		_ctxSquad = false; _ctxMember = false; _found = false;
		_validated = false;
	}
	//! Return if player record was found and validated
	bool Found() const {return _found && !_ctxMember && _validated;}

	void OnStartElement(RString name, XMLAttributes &attributes);
	void OnEndElement(RString name);
	void OnCharacters(RString chars);
};

void SquadParser::OnStartElement(RString name, XMLAttributes &attributes)
{
	if (strcmp(name, "squad") == 0)
	{
		Assert(!_ctxSquad);
		Assert(!_ctxMember);
		_ctxSquad = true;
		const XMLAttribute *attr = attributes.Find("nick");
		if (attr) _squad->nick = attr->value;
	}
	else if (strcmp(name, "member") == 0)
	{
		Assert(_ctxSquad);
		Assert(!_ctxMember);
		_ctxMember = true;
		const XMLAttribute *attr = attributes.Find("id");
		if (attr && attr->value == _identity->id)
		{
			_found = true;
			const XMLAttribute *attr = attributes.Find("nick");
			// verify nick matches - if not, refuse validation
			_validated = attr && strcmpi(_identity->name, attr->value) == 0;
		}
	}
	_element = name;
}

void SquadParser::OnEndElement(RString name)
{
	if (strcmp(name, "squad") == 0)
	{
		Assert(_ctxSquad);
		Assert(!_ctxMember);
		_ctxSquad = false;
	}
	else if (strcmp(name, "member") == 0)
	{
		Assert(_ctxMember);
		_ctxMember = false;
		if (_found) Abort();
	}
	_element = "";
}

void SquadParser::OnCharacters(RString chars)
{
	if (!_ctxSquad) return;
	if (_ctxMember)
	{
		if (!_found) return;
		if (strcmp(_element, "name") == 0)
			_identity->fullname = chars;
		else if (strcmp(_element, "email") == 0)
			_identity->email = chars;
		else if (strcmp(_element, "icq") == 0)
			_identity->icq = chars;
		else if (strcmp(_element, "remark") == 0)
			_identity->remark = chars;
	}
	else
	{
		if (strcmp(_element, "name") == 0)
			_squad->name = chars;
		else if (strcmp(_element, "email") == 0)
			_squad->email = chars;
		else if (strcmp(_element, "web") == 0)
			_squad->web = chars;
		else if (strcmp(_element, "picture") == 0)
			_squad->picture = chars;
		else if (strcmp(_element, "title") == 0)
			_squad->title = chars;
	}
}

/*!
\patch 1.14 Date 08/09/2001 by Ondra
- Improved: Squad verification is processes on background
and no longer causes server to wait.
*/
CheckSquadObject::CheckSquadObject(PlayerIdentity &identity, Ref<SquadIdentity> squad, bool newSquad, RString proxy)
:_stateDone(true)
{
	_identity = identity;
	_squad = squad;
	_newSquad = newSquad;
	_proxy = proxy;

	StartDownloadingXMLSource();
}

CheckSquadObject::~CheckSquadObject()
{
	// we cannot destruct background object while thread is working
	_stateDone.Wait();
}

//! Download to memory
static DWORD WINAPI DownloadToMemThread(void *context)
{
	DownloadToMemContext *c = (DownloadToMemContext *)context;
	c->result = DownloadFile(c->url,*c->size,c->proxy);
	c->event->Set();
	return 0;
}

//! Download to file
static DWORD WINAPI DownloadToFileThread(void *context)
{
	DownloadToFileContext *c = (DownloadToFileContext *)context;
	DownloadFile(c->url,c->file,c->proxy);
	c->event->Set();
	return 0;
}

#ifdef _WIN32
//! Use independent thread for file download
#define MT_DOWNLOAD 1
#endif

#if MT_DOWNLOAD

//! Download to file
static void DownloadToFileOverlapped(DownloadToFileContext *context)
{
	DWORD threadId;
	HANDLE handle = CreateThread
	(
		NULL,64*1024,DownloadToFileThread,context,0,&threadId
	);
	if (!handle)
	{
		// create thread failed - fallback to direct processing
		DownloadToFileThread(context);
	}
	else
	{
		// handle no longer required
		CloseHandle(handle);
	}
}

//! Download to memory
static void DownloadToMemOverlapped(DownloadToMemContext *context)
{
	DWORD threadId;
	HANDLE handle = CreateThread
	(
		NULL,64*1024,DownloadToMemThread,context,0,&threadId
	);
	if (!handle)
	{
		// create thread failed - fallback to direct processing
		DownloadToMemThread(context);
	}
	else
	{
		// handle no longer required
		CloseHandle(handle);
	}
}

#else

//! Download to file
static void DownloadToFileOverlapped(DownloadToFileContext *context)
{
	DownloadToFileThread(context);
}

//! Download to memory
static void DownloadToMemOverlapped(DownloadToMemContext *context)
{
	DownloadToMemThread(context);
}

#endif

void CheckSquadObject::StartDownloadingXMLSource()
{
	_state = DownloadingSquad;
	_stateDone.Reset();

	_squadContext.url = _identity.squadId;
	_squadContext.size = &_squadXMLSize;
	_squadContext.event = &_stateDone;
	_squadContext.proxy = _proxy.GetLength() > 0 ? (const char *)_proxy : NULL;
	_squadContext.result.Free();
	DownloadToMemOverlapped(&_squadContext);
}

void CheckSquadObject::EndDownloadingXMLSource()
{
	_squadXMLData = _squadContext.result;
}

void CheckSquadObject::StartDownloadingLogo()
{

	// process XML results
	ProcessXML();


	if (_logoUrl.GetLength()>0 && _logoFile.GetLength()>0)
	{
		_state = DownloadingLogo;
		_stateDone.Reset();
		// start downloading logo - if necessary
		// note: _logoUrl is RString, but is passed as const char *
		// which is MT safe
		_logoContext.file = _logoFile;
		_logoContext.url = _logoUrl;
		_logoContext.event = &_stateDone;
		_logoContext.proxy = _proxy.GetLength() > 0 ? (const char *)_proxy : NULL;
		DownloadToFileOverlapped(&_logoContext);
		// download result ignored?

	}
	else
	{
		_state = AllDone;
		// _stateDone is still Set
	}
}

bool CheckSquadObject::IsDone()
{
	if (!_stateDone.TryWait()) return false;
	switch (_state)
	{
		case DownloadingSquad:
			EndDownloadingXMLSource();
			StartDownloadingLogo();
			return false;
		case DownloadingLogo:
			_state = AllDone;
			return true;
		case AllDone:
			return true;
	}
	return false;
}

void CheckSquadObject::ProcessXML()
{
	_logoUrl = RString();
	_logoFile = RString();
	if (DoProcessXML())
	{
		_identity.squad = _squad;
	}
	else
	{
		_squad = NULL;
		_identity.squadId = "";
	}

}


bool CheckSquadObject::DoProcessXML()
{
	Assert(_identity.squadId.GetLength() > 0);


	if (!_squadXMLData || _squadXMLSize<=0) return false;

	SquadParser parser(&_identity, _squad);
	QIStream in(_squadXMLData,_squadXMLSize);

	if (!parser.Parse(in)) return false;
	_squadXMLData.Free();

	if (!parser.Found()) return false;

	if (_newSquad && _squad->picture.GetLength() > 0)
	{
		// download picture
		RString src = GetServerTmpDir() + RString("\\squads\\") + _squad->nick + RString("\\") + _squad->picture;
		CreatePath(src);
		char url[256];
		strcpy(url, _squad->id);
		char *ptr = strrchr(url, '/');
		// FIX allow also \ in url
		if (!ptr) ptr = strrchr(url, '\\');
		if (ptr) {ptr++; *ptr = 0;}
		else ptr = url;
		strcpy(ptr, _squad->picture);

		_logoUrl = url;
		_logoFile = src;
	}
	else
	{
		_logoUrl = RString();
		_logoFile = RString();
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////
// Voting system

bool Vote::HasValue(const char *val, int valueSize) const
{
	if (valueSize != value.Size()) return false;
	return memcmp(val, value.Data(), valueSize) == 0;
}

int Voting::Add(int player, char *value, int valueSize)
{
	int index = -1;
	for (int i=0; i<Size(); i++)
	{
		if (Get(i).player == player)
		{
			index = i;
			break;
		}
	}
	if (index < 0)
	{
		index = base::Add();
		Set(index).player = player;
	}
	if (value)
	{
		Set(index).value.Realloc(valueSize);
		Set(index).value.Resize(valueSize);
		memcpy(Set(index).value.Data(), value, valueSize);
	}
	else
	{
		Set(index).value.Realloc(0);
		Set(index).value.Resize(0);
	}
	return index;
}

/*!
\patch 1.34 Date 12/04/2001 by Jirka
- Improved: Mission voting is valid if half of players vote the same mission
\patch 1.79 Date 7/30/2002 by Ondra
- Improved: Mission voting is finished once result is certain.
*/

bool Voting::Check(const AutoArray<PlayerIdentity> &identities) const
{
	int sum = 0;
	int n = identities.Size();

	for (int j=0; j<Size(); j++)
	{
		int player = Get(j).player;
		for (int i=0; i<n; i++)
		{
			if (identities[i].dpnid == player)
			{
				sum++;
				break;
			}
		}
	}
	if (sum > _threshold * n) return true;
	if (!_selection) return false;

	// let
	// rest = number of players that did not vote yet
	// first = number of players that voted for the most popular choice
	// second = number of players that voted for the second most popular choice
	// voting is done when 
	// first>=second+rest

	int first = 0, second = 0;
	int rest = identities.Size()-sum;
	GetValue(&first,&second);
	LogF("First %d, second %d, rest %d",first,second,rest);
	return first>=second+rest;
	//return first >= (n + 1) / 2;
	// first >= (n+1)/2 -> (rest+second)<=(n+1)/2

}

TypeIsSimple(const Vote *)

const AutoArray<char> *Voting::GetValue(int *n, int *n2) const
{
	if (Size() == 0) return NULL;

	AUTO_STATIC_ARRAY(const Vote *, values, 32);
	AUTO_STATIC_ARRAY(int, votes, 32);
	for (int i=0; i<Size(); i++)
	{
		int found = -1;
		const char *value = Get(i).value.Data();
		int valueSize = Get(i).value.Size();
		for (int j=0; j<values.Size(); j++)
		{
			if (values[j]->HasValue(value, valueSize))
			{
				found = j;
				break;
			}
		}
		if (found >= 0)
		{
			votes[found]++;
		}
		else
		{
			values.Add(&Get(i));
			votes.Add(1);
		}
	}
	const Vote *best = NULL;
	int maxVotes = 0;
	for (int i=0; i<votes.Size(); i++)
	{
		if (votes[i] > maxVotes)
		{
			maxVotes = votes[i];
			best = values[i];
		}
	}
	if (n) *n = maxVotes;
	if (n2)
	{
		int max2Votes = 0;
		for (int i=0; i<votes.Size(); i++)
		{
			if (votes[i] > max2Votes && values[i]!=best)
			{
				max2Votes = votes[i];
			}
		}
		*n2 = max2Votes;
	}
	if (best) return &best->value;
	return NULL;
}

bool Voting::HasID(char *id, int idSize) const
{
	if (idSize != _id.Size()) return false;
	return memcmp(id, _id.Data(), idSize) == 0;
}

void Votings::Add(NetworkServer *server, char *id, int idSize, float threshold, int player, char *value, int valueSize, bool selection)
{
	int index = -1;
	for (int i=0; i<Size(); i++)
	{
		if (Get(i).HasID(id, idSize))
		{
			index = i;
			break;
		}
	}
	
	if (index < 0)
	{
		index = base::Add();
		Set(index)._threshold = threshold;
		Set(index)._selection = selection;
		Set(index)._id.Realloc(idSize);
		Set(index)._id.Resize(idSize);
		memcpy(Set(index)._id.Data(), id, idSize);
	}
	
	Voting &voting = Set(index);
	voting.Add(player, value, valueSize);
	if (voting.Check(*server->GetIdentities()))
	{
		// FIX: voting can change in ApplyVoting
		Voting copy = voting;
		Delete(index);
		server->ApplyVoting(copy.GetID(), copy.GetValue());
	}
}

/*!
\patch 1.30 Date 11/01/2001 by Jirka
- Improved: Voting system is now more consistent
*/
void Votings::Check(NetworkServer *server)
{
	for (int i=0; i<Size();)
	{
		Voting &voting = Set(i);
		if (voting.Check(*server->GetIdentities()))
		{
			server->ApplyVoting(voting.GetID(), voting.GetValue());
			Delete(i);
		}
		else i++;
	}
}

///////////////////////////////////////////////////////////////////////////////
// Network Components

#include <Es/Memory/normalNew.hpp>

NetworkComponent::NetworkComponent(NetworkManager *parent)
{
	_parent = parent;
	_state = NGSNone;
	_nextId = 0;
}

NetworkComponent::~NetworkComponent()
{
}

#include <Es/Memory/debugNew.hpp>

#if 0
//! Process out of memory error on local heap
/*!
	\patch 1.01 Date 07/10/2001 by Ondra.
	- Improved: Better handling of out of memory condition.
*/
void NetworkMemoryError( MemHeapLocked *heap, int size )
{
#ifdef _WIN32
	ErrorMessage
	(
		"Network: Out of reserved memory (%d KB).\n"
		"Code change required (current limit %d KB).\n"
		"Total free %d KB\n"
		"Free blocks %d, Max free size %d KB",
		size/1024,
		heap->Size()/1024,
		heap->TotalFreeLeft()/1024,
		heap->CountFreeLeft(),
		heap->MaxFreeLeft()/1024
	);
#else
	ErrorMessage
	(
		"Network: Out of reserved memory (%d KB).\n",
		size/1024
	);
#endif
}
#endif

// !!! internal compiler error - moved into deader
/*
NetworkComponent::~NetworkComponent
{
  DeleteCriticalSection(&_receivedSystemMessagesLock);
  DeleteCriticalSection(&_receivedUserMessagesLock);
}
*/


// high level transfer functions

DWORD NetworkComponent::SendMsg(int to, NetworkSimpleObject *object, NetMsgFlags dwFlags)
{
	if (!object) return 0xFFFFFFFF;

	// prepare format
	NetworkMessageType type = object->GetNMType();
	NetworkMessageFormatBase *format = GetFormat(/*to, */type);
	if (!format)
	{
		if (DiagLevel >= 1)
		{
			DiagLogF("Warning: Format not found");
		}
		return 0xFFFFFFFF;		
	}

	// create message
	Ref<NetworkMessage> msg = new NetworkMessage();
	msg->time = Glob.time;
	NetworkMessageContext ctx(msg, format, this, to, MSG_SEND);
	TMError err = object->TransferMsg(ctx);
	if (err != TMOK)
		return 0xFFFFFFFF;

	// send message
	return SendMsg(to, msg, type, dwFlags);
}

void NetworkComponent::DecodeMessage(int from, NetworkMessageRaw &src)
{
#if _ENABLE_DEDICATED_SERVER || _ENABLE_CHEATS
	StatRawMsgReceived(from, src.GetSize());
#endif

	//{ DEDICATED SERVER SUPPORT
	#if _ENABLE_DEDICATED_SERVER
	OnMonitorIn(src.GetSize());
	#endif
	//}

	// message header
	NetworkMessageType type;
	src.Get((int &)type, NCTSmallUnsigned);
	Time time;
	src.Get(time, NCTNone);				// TODO: compression

	if (type == NMTMessages)
	{
		int n;
		src.Get(n, NCTSmallUnsigned);
		for (int i=0; i<n; i++)
		{
			NetworkMessageType type;
#if _ENABLE_CHEATS
			int oldPos = src.GetPos();
#endif
			src.Get((int &)type, NCTSmallUnsigned);
			NetworkMessageFormatBase *format = GetFormat(type);
			if (!format)
			{
				if (DiagLevel >= 1)
				{
					DiagLogF("Warning: Format not found");
				}
				return;		
			}
			Ref<NetworkMessage> msg = new NetworkMessage();
			msg->time = time;
			// Message body
			DecodeMsg(msg, src, format);
			OnMessage(from, msg, type);
#if _ENABLE_CHEATS
	StatMsgReceived(type, src.GetPos() - oldPos);
#endif
		}
	}
	else
	{
		NetworkMessageFormatBase *format = GetFormat(type);
		if (!format)
		{
			if (DiagLevel >= 1)
			{
				DiagLogF("Warning: Format not found");
			}
			return;		
		}
		Ref<NetworkMessage> msg = new NetworkMessage();
		msg->time = time;
		// Message body
		DecodeMsg(msg, src, format);
		OnMessage(from, msg, type);
#if _ENABLE_CHEATS
	StatMsgReceived(type, src.GetSize());
#endif
	}
}

void NetworkComponent::ReceiveLocalMessages()
{
	for (int i=0; i<_receivedLocalMessages.Size(); i++)
	{
		NetworkLocalMessageInfo &info = _receivedLocalMessages[i];
		OnMessage(info.from, info.msg, info.type);
	}
	_receivedLocalMessages.Resize(0);
}

const PlayerRole *NetworkComponent::FindPlayerRole(int player) const
{
	for (int i=0; i<_playerRoles.Size(); i++)
	{
		const PlayerRole &role = _playerRoles[i];
		if (role.player == player) return &role;
	}
	return NULL;
}

const PlayerIdentity *NetworkComponent::FindIdentity(int dpnid) const
{
	for (int i=0; i<_identities.Size(); i++)
	{
		const PlayerIdentity &identity = _identities[i];
		if (identity.dpnid == dpnid) return &identity;
	}
	return NULL;
}

PlayerIdentity *NetworkComponent::FindIdentity(int dpnid)
{
	for (int i=0; i<_identities.Size(); i++)
	{
		PlayerIdentity &identity = _identities[i];
		if (identity.dpnid == dpnid) return &identity;
	}
	return NULL;
}

/*!
\patch 1.90 Date 10/30/2002 by Jirka
- Fixed: Bad date and time of modification occurs in files transferred over network
*/

int NetworkComponent::ReceiveFileSegment(TransferFileMessage &msg)
{
	// find file in list
	int index = -1;
	for (int i=0; i<_files.Size(); i++)
	{
		if (_files[i].fileName == msg.path)
		{
			index = i;
			break;
		}
	}
	if (index < 0)
	{
		// new file
		index = _files.Add();
		ReceivingFile &file = _files[index];
		file.fileName = msg.path;
		file.fileSegments.Resize(msg.totSegments);
		for (int i=0; i<file.fileSegments.Size(); i++) file.fileSegments[i] = false;
		file.fileData.Resize(msg.totSize);
		file.received = 0;
	}
	ReceivingFile &file = _files[index];
	//LogF("Received %s:%d of %d",(const char *)file.fileName,msg.curSegment,msg.totSegments);

	// receive segment
	file.fileSegments[msg.curSegment] = true;
	memcpy(file.fileData.Data() + msg.offset, msg.data.Data(), msg.data.Size());
	file.received += msg.data.Size();
	for (int i=0; i<file.fileSegments.Size(); i++)
  {
		if (!file.fileSegments[i]) return 0;
  }
	//LogF("*Finished %s:%d",(const char *)file.fileName,msg.totSegments);
	// whole file received
	CreatePath(file.fileName);
	QOFStream f;
	f.open(file.fileName);
	f.write(file.fileData.Data(), file.fileData.Size());
	f.close();
	// delete buffer
	_files.Delete(index);
  return f.fail() ? -1 : +1;
}

void NetworkComponent::TransferFile(int to, RString dest, RString source)
{
	int maxSegmentSize;
	if (IsUseSockets())
		maxSegmentSize = 512 - 50;
	else
		maxSegmentSize = 0x10000;

	QIFStreamB f;
	f.AutoOpen(source);

	TransferFileMessage msg;
	msg.path = dest;
	msg.totSize = f.GetBuffer()->GetSize();
	msg.totSegments = (msg.totSize + maxSegmentSize - 1) / maxSegmentSize;
	msg.offset = 0;
	for (int i=0; i<msg.totSegments; i++)
	{
		msg.curSegment = i;
		int size = min(maxSegmentSize, msg.totSize - msg.offset);
		msg.data.Resize(size);
		memcpy(msg.data.Data(), f.GetBuffer()->GetData() + msg.offset, size);
		SendMsg(to, &msg, NMFGuaranteed);
		msg.offset += size;
	}
}

void NetworkComponent::TransferFace(int to, RString player)
{
	// user only for transfer from server to clients
	NetworkClient *client = _parent->GetClient();
	bool notBotClient = !client || client->GetPlayer() != to;

	RString srcDir = GetServerTmpDir() + RString("\\players\\") + player + RString("\\");
	RString dstDir = RString("tmp\\players\\") + player + RString("\\");

	RString src = srcDir + RString("face.paa");
	RString dst = dstDir + RString("face.paa");
	if (QIFStream::FileExists(src))
	{
		if (notBotClient)
			TransferFile(to, dst, src);
		else
		{
			CreatePath(dstDir);
			::CopyFile(src, dst, FALSE);
		}
	}
	else
	{
		src = srcDir + RString("face.jpg");
		dst = dstDir + RString("face.jpg");
		if (QIFStream::FileExists(src))
		{
			if (notBotClient)
				TransferFile(to, dst, src);
			else
				CreatePath(dstDir);
				::CopyFile(src, dst, FALSE);
		}
	}
}

void NetworkComponent::TransferCustomRadio(int to, RString player)
{
	// user only for transfer from server to clients
	NetworkClient *client = _parent->GetClient();
	bool notBotClient = !client || client->GetPlayer() != to;

	RString srcDir = GetServerTmpDir() + RString("\\players\\") + player + RString("\\sound\\");
	RString dstDir = RString("tmp\\players\\") + player + RString("\\sound\\");

#ifdef _WIN32
	WIN32_FIND_DATA info;
	HANDLE h = FindFirstFile(srcDir + RString("*.*"), &info);
	if (h != INVALID_HANDLE_VALUE)
	{
		do
		{
			if 
			(
				(info.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0
			)
			{
				RString src = srcDir + RString(info.cFileName);
				RString dst = dstDir + RString(info.cFileName);
				if (notBotClient)
					TransferFile(to, dst, src);
				else
				{
					CreatePath(dstDir);
					::CopyFile(src, dst, FALSE);
				}
			}
		}
		while (FindNextFile(h, &info));
		FindClose(h);
	}
#else
  LocalPath(sdir,(const char*)srcDir);
  int len = strlen(sdir);
  if ( sdir[len-1] == '/' )
    sdir[--len] = (char)0;
  DIR *dir = opendir(sdir);
  if ( !dir ) return;
  struct dirent *entry;
  sdir[len++] = '/';
  while ( (entry = readdir(dir)) ) {  // process one directory entry
      // stat the entry: copy only regular files...
    strcpy(sdir+len,entry->d_name);
    struct stat st;
    if ( !stat(sdir,&st) &&
         S_ISREG(st.st_mode) ) {    // regular file
      RString src = srcDir + RString(entry->d_name);
      RString dst = dstDir + RString(entry->d_name);
      if ( notBotClient )
        TransferFile(to, dst, src);
      else {
        CreatePath(dstDir);
        ::CopyFile(src, dst, FALSE);
        }
      }
    }
  closedir(dir);
#endif
}


///////////////////////////////////////////////////////////////////////////////
// Network Manager

extern const int DefaultNetworkPort;

NetworkManager::NetworkManager()
{
	_sessionEnum = NULL;
}

NetworkManager::~NetworkManager()
{
	Done();
}

void NetworkManager::StopEnumHosts()
{
	if (_sessionEnum) _sessionEnum->StopEnumHosts();
}

/*!
\patch 1.24 Date 09/19/2001 by Jirka
- Changed: all sessions (on all ports) are enumerated at once
\patch 1.27 Date 10/16/2001 by Jirka
- Added: enable multiple enumerations of hosts.
Now two enumerations are provided - on default DirectPlay port and on port given by player.
This fixes problems with enumeration with most firewalls.
\patch 1.33 Date 11/30/2001 by Jirka
- Added: external hosts.txt file with list of favourite hosts
*/
bool NetworkManager::StartEnumHosts()
{
	if (_sessionEnum)
	{
		_lastEnumHosts = Glob.uiTime;
		AutoArray<RemoteHostAddress> *hosts = NULL;
		if (_ip.GetLength() == 0) hosts = &_hosts;
		return _sessionEnum->StartEnumHosts(_ip, _port, hosts);
	}
	return true;
}

extern RString FlashpointCfg;
	
bool NetworkManager::Init(RString ip, int port, bool startEnum)
{
	InitMsgFormats();

	_ip = ip;
	_port = port;

    	// load some settings from Flashpoint.cfg
	ParamFile cfg;
	cfg.Parse(FlashpointCfg);

#ifdef _WIN32
	if (IsUseSockets())
		_sessionEnum = CreateNetSessionEnum(cfg);
	else
		_sessionEnum = CreateDPlaySessionEnum();
#else
	_sessionEnum = CreateNetSessionEnum(cfg);
#endif

	Assert(_sessionEnum);
	if (!_sessionEnum->Init(MAGIC_APP))
	{
		_sessionEnum = NULL;
		return false;
	}

	// read external hosts file
	ParamFile file;
	file.ParsePlainText("hosts.txt");
	int n = file.GetEntryCount();
	_hosts.Realloc(n);
	_hosts.Resize(n);
	for (int i=0; i<n; i++)
	{
		const ParamEntry &entry = file.GetEntry(i);
		RemoteHostAddress &address = _hosts[i];

		address.name = entry.GetName();
		RString value = entry;

		const char *ptr = strrchr(value, ':');
		if (ptr)
		{
			address.ip = value.Substring(0, ptr - value);
			address.port = atoi(ptr + 1);
		}
		else
		{
			address.ip = value;
			address.port = DefaultNetworkPort;
		}
	}
	
	if (startEnum) return StartEnumHosts();
	return true;
}

void NetworkManager::Done()
{
	_client = NULL;
	_server = NULL;
	_sessionEnum = NULL;
}

//void NetworkManager::GetSessions(AutoArray<SessionInfo, MemAllocSA> &sessions)
void NetworkManager::GetSessions(AutoArray<SessionInfo> &sessions)
{
	if (_sessionEnum) _sessionEnum->GetSessions(sessions);

	for (int i=0; i<sessions.Size(); i++)
	{
		SessionInfo &info = sessions[i];

		info.badActualVersion = info.actualVersion < MP_VERSION_REQUIRED;
		info.badRequiredVersion = MP_VERSION_ACTUAL < info.requiredVersion;

    RString GetModList();
    info.badMod = info.equalModRequired && stricmp(info.mod, GetModList()) != 0;
	}
}

void CheckMPVersion(SessionInfo &info)
{
	info.badActualVersion = info.actualVersion < MP_VERSION_REQUIRED;
	info.badRequiredVersion = MP_VERSION_ACTUAL < info.requiredVersion;
}

RString NetworkManager::IPToGUID(RString ip, int port)
{
	if (_sessionEnum) return _sessionEnum->IPToGUID(ip, port);
	return NULL;
}

bool NetworkManager::CreateSession(int port, RString password)
{
#if _ENABLE_MP
	StopEnumHosts();

	// create server
	GDebugger.PauseCheckingAlive();
	ProgressStart(LocalizeString(IDS_CREATE_SERVER));
	_server = new NetworkServer(this, port, password);
	if (!_server->IsValid())
	{
		_server = NULL;
		StartEnumHosts();
		ProgressFinish();
		GDebugger.ResumeCheckingAlive();
		return false;
	}
	ProgressFinish();
	GDebugger.ResumeCheckingAlive();

	// retreive local address
	char address[512]; address[0] = 0;
	if (!_server->GetURL(address, sizeof(address)))
	{
		_server = NULL;
		StartEnumHosts();
		ProgressFinish();
		GDebugger.ResumeCheckingAlive();
		return false;
	}

	// create bot client
	GDebugger.PauseCheckingAlive();
	ProgressStart(LocalizeString(IDS_CREATE_CLIENT));
	_client = new NetworkClient(this, address, password, true);
	if (!_client->IsValid())
	{
		_server = NULL;
		_client = NULL;
		StartEnumHosts();
		ProgressFinish();
		GDebugger.ResumeCheckingAlive();
		return false;
	}
	ProgressFinish();
	GDebugger.ResumeCheckingAlive();

	return true;
#else
	return false;
#endif // if _ENABLE_MP
}

/*!
\patch 1.05 Date 7/18/2001 by Jirka
- Improved: allow user to interrupt waiting for server when connecting through GameSpy Arcade
- also show title "Wait for server"
*/
bool NetworkManager::WaitForSession()
{
	ProgressStart(LocalizeString(IDS_DISP_CLIENT_TEXT));

	StartEnumHosts();
	DWORD lastEnumTime = ::GlobalTickCount();
	while (true)
	{
		// ADDED
		#if defined _WIN32 && !defined _XBOX
		bool IsAppPaused();
		if (!IsAppPaused())
		{
			int esc = GetAsyncKeyState(VK_ESCAPE);
			if (esc & 1)
			{
				ProgressFinish();
				return false;
			}
		}
		#endif

		if (_sessionEnum->NSessions() > 0) break;

		Sleep(100);
		I_AM_ALIVE();
		if (::GlobalTickCount() - lastEnumTime > 5000 ||
            IsUseSockets() && ::GlobalTickCount() - lastEnumTime > 2500)
		{
			StartEnumHosts();
			lastEnumTime = ::GlobalTickCount();
		}
	}
	StopEnumHosts();
	ProgressFinish();
	return true;
}

ConnectResult NetworkManager::JoinSession(RString guid, RString password)
{
#if _ENABLE_MP
	StopEnumHosts();

	// create client
	GDebugger.PauseCheckingAlive();
	ProgressStart(LocalizeString(IDS_CREATE_CLIENT));
	_client = new NetworkClient(this, guid, password, false);
	if (!_client->IsValid())
	{
		ConnectResult result = _client->GetConnectResult();
		_client = NULL;
		ProgressFinish();
		GDebugger.ResumeCheckingAlive();
		StartEnumHosts();
		return result;
	}
	ProgressFinish();
	GDebugger.ResumeCheckingAlive();

	return CROK;
#else // #if _ENABLE_MP
	return CRError;
#endif
}

void NetworkManager::Close()
{
	GDebugger.PauseCheckingAlive();

	// remove server and client
	_client = NULL;
	_server = NULL;

	StartEnumHosts();

	GDebugger.ResumeCheckingAlive();
}

void NetworkManager::GetPlayers(AutoArray<NetPlayerInfo, MemAllocSA> &players)
{
	if (_client) _client->GetPlayers(players);
	else players.Resize(0);
}

unsigned NetworkManager::CleanUpMemory()
{
	unsigned ret = 0;
	if (_server) ret += _server->CleanUpMemory();
	if (_client) ret += _client->CleanUpMemory();
	return ret;
}

void NetworkManager::CreateMission(RString mission, RString world)
{
	if (_server) _server->CreateMission(mission, world);
}

void NetworkManager::KickOff(int dpnid, KickOffReason reason)
{
	if (_server) _server->KickOff(dpnid,reason);
}

void NetworkManager::Ban(int dpnid)
{
	if (_server) _server->Ban(dpnid);
}

void NetworkManager::LockSession(bool lock)
{
	if (_server) _server->LockSession(lock);
}

void NetworkManager::InitMission(bool cadetMode)
{
	if (_server) _server->InitMission(cadetMode);
}

void NetworkManager::OnSimulate()
{
	#if _ENABLE_MP
	if (_sessionEnum && _sessionEnum->RunningEnumHosts())
	{
		// refresh
		float age = Glob.uiTime - _lastEnumHosts;
		if (age > 5 || IsUseSockets() && age > 3) StartEnumHosts();
	}

	if (_client) _client->OnSimulate();
	if (_server) _server->OnSimulate();
	#endif
}

NetworkGameState NetworkManager::GetGameState() const
{
	if (_client) return _client->GetGameState();
	else return NGSNone;
}

NetworkGameState NetworkManager::GetServerState() const
{
	if (_client) return _client->GetServerState();
	else return NGSNone;
}

void NetworkManager::SetGameState(NetworkGameState state)
{
	if (_server) _server->SetGameState(state);
}

bool NetworkManager::IsGameMaster() const
{
	if (_client) return _client->IsGameMaster();
	else return false;
}

bool NetworkManager::IsAdmin() const
{
	if (_client) return _client->IsAdmin();
	else return false;
}

ConnectionQuality NetworkManager::GetConnectionQuality() const
{
	if (_client) return _client->GetConnectionQuality();
	else return CQGood;
}

void NetworkManager::GetParams(float &param1, float &param2) const
{
	if (_client) _client->GetParams(param1, param2);
}

void NetworkManager::SetParams(float param1, float param2)
{
	if (_client) _client->SetParams(param1, param2);
}

const MissionHeader *NetworkManager::GetMissionHeader() const
{
	if (_server) return _server->GetMissionHeader();
	else if (_client) return _client->GetMissionHeader();
	else return NULL;
}

int NetworkManager::GetPlayer() const
{
	if (_client) return _client->GetPlayer();
	else return 0;
}

int NetworkManager::NPlayerRoles() const
{
	if (_client) return _client->NPlayerRoles();
	else return 0;
}

const PlayerRole *NetworkManager::GetPlayerRole(int role) const
{
	if (_client) return _client->GetPlayerRole(role);
	else return NULL;
}

const PlayerRole *NetworkManager::GetMyPlayerRole() const
{
	if (_client) return _client->GetMyPlayerRole();
	else return NULL;
}

const PlayerIdentity *NetworkManager::FindIdentity(int dpnid) const
{
	if (_client) return _client->FindIdentity(dpnid);
	else return NULL;
}

const AutoArray<PlayerIdentity> *NetworkManager::GetIdentities() const
{
	if (_client) return _client->GetIdentities();
	else return NULL;
}

void NetworkManager::GetTransferStats(int &curBytes, int &totBytes)
{
	if (_client) _client->GetTransferStats(curBytes, totBytes);
}

void NetworkManager::ClientReady(NetworkGameState state)
{
	if (_client) _client->ClientReady(state);
}

void NetworkManager::AssignPlayer(int role, int player)
{
	if (_client) _client->AssignPlayer(role, player);
}

void NetworkManager::UnassignPlayer(int role)
{
	if (_client) _client->AssignPlayer(role, AI_PLAYER);
}

void NetworkManager::SelectPlayer(int player, Person *person, bool respawn)
{
	if (_client) _client->SelectPlayer(player, person, respawn);
}

void NetworkManager::PlaySound
(
	RString name, Vector3Par position, Vector3Par speed,
	float volume, float freq, AbstractWave *wave
)
{
	if (_client) _client->PlaySound(name, position, speed, volume, freq, wave);
}

void NetworkManager::SoundState
(
	AbstractWave *wave, SoundStateType state
)
{
	if (_client) _client->SoundState(wave, state);
}

NetworkGameState NetworkManager::GetPlayerState(int dpid)
{
	if (_server) return _server->GetPlayerState(dpid);
	else return NGSNone;
}

RString NetworkManager::GetPlayerName(int dpid)
{
	if (_server) return _server->GetPlayerName(dpid);
	else return "Unknown player";
}

Vector3 NetworkManager::GetCameraPosition(int dpid)
{
	if (_server) return _server->GetCameraPosition(dpid);
	else return VZero;
}

void NetworkManager::CreateAllObjects()
{
	if (_client) _client->CreateAllObjects();
}

void NetworkManager::DestroyAllObjects()
{
	if (_server) _server->DestroyAllObjects();
	if (_client) _client->DestroyAllObjects();
}

void NetworkManager::DeleteObject(NetworkId &id)
{
	if (_client) _client->DeleteObject(id);
}

bool NetworkManager::CreateVehicle(Vehicle *veh, VehicleListType type, RString name, int idVeh)
{
	if (_client) return _client->CreateVehicle(veh, type, name, idVeh);
	return false;
}

bool NetworkManager::CreateCenter(AICenter *center)
{
	if (_client) return _client->CreateCenter(center);
	return false;
}

bool NetworkManager::CreateObject(NetworkObject *object)
{
	if (_client) return _client->CreateObject(object);
	return false;
}

bool NetworkManager::CreateCommand(AISubgroup *subgrp, int index, Command *cmd)
{
	if (_client) return _client->CreateCommand(subgrp, index, cmd);
	return false;
}

void NetworkManager::DeleteCommand(AISubgroup *subgrp, int index, Command *cmd)
{
	if (_client) _client->DeleteCommand(subgrp, index, cmd);
}

void NetworkManager::AskForDammage
(
	Object *who, EntityAI *owner,
	Vector3Par modelPos, float val, float valRange, RString ammo
)
{
	if (_client) _client->AskForDammage(who, owner, modelPos, val, valRange, ammo);
}

void NetworkManager::AskForSetDammage
(
	Object *who, float dammage
)
{
	if (_client) _client->AskForSetDammage(who, dammage);
}

void NetworkManager::AskForAddImpulse
(
	Vehicle *vehicle, Vector3Par force, Vector3Par torque
)
{
	if (_client) _client->AskForAddImpulse(vehicle, force, torque);
}

void NetworkManager::AskForMove
(
	Object *vehicle, Vector3Par pos
)
{
	if (_client) _client->AskForMove(vehicle, pos);
}

void NetworkManager::AskForMove
(
	Object *vehicle, Matrix4Par trans
)
{
	if (_client) _client->AskForMove(vehicle, trans);
}

void NetworkManager::AskForJoin
(
	AIGroup *join, AIGroup *group
)
{
	if (_client) _client->AskForJoin(join, group);
}

void NetworkManager::AskForJoin
(
	AIGroup *join, OLinkArray<AIUnit> &units
)
{
	if (_client) _client->AskForJoin(join, units);
}

void NetworkManager::ExplosionDammageEffects
(
	EntityAI *owner, Shot *shot,
	Object *directHit, Vector3Par pos, Vector3Par dir, const AmmoType *type,
	bool enemyDammage
)
{
	if (_client) _client->ExplosionDammageEffects
	(
		owner, shot, directHit, pos, dir, type, enemyDammage
	);
}

void NetworkManager::AskForGetIn
(
	Person *soldier, Transport *vehicle,
	GetInPosition position
)
{
	if (_client) _client->AskForGetIn(soldier, vehicle, position);
}

void NetworkManager::AskForGetOut
(
	Person *soldier, Transport *vehicle, bool parachute
)
{
	if (_client) _client->AskForGetOut(soldier, vehicle, parachute);
}

void NetworkManager::AskForChangePosition
(
	Person *soldier, Transport *vehicle, UIActionType type
)
{
	if (_client) _client->AskForChangePosition(soldier, vehicle, type);
}

void NetworkManager::AskForAimWeapon
(
	EntityAI *vehicle, int weapon, Vector3Par dir
)
{
	if (_client) _client->AskForAimWeapon(vehicle, weapon, dir);
}

void NetworkManager::AskForAimObserver
(
	EntityAI *vehicle, Vector3Par dir
)
{
	if (_client) _client->AskForAimObserver(vehicle, dir);
}

void NetworkManager::AskForSelectWeapon
(
	EntityAI *vehicle, int weapon
)
{
	if (_client) _client->AskForSelectWeapon(vehicle, weapon);
}

void NetworkManager::AskForAmmo
(
	EntityAI *vehicle, int weapon, int burst
)
{
	if (_client) _client->AskForAmmo(vehicle, weapon, burst);
}

void NetworkManager::AskForHideBody(Person *vehicle)
{
	if (_client) _client->AskForHideBody(vehicle);
}

void NetworkManager::FireWeapon
(
	EntityAI *vehicle, int weapon, const Magazine *magazine, EntityAI *target
)
{
	if (_client) _client->FireWeapon(vehicle, weapon, magazine, target);
}

void NetworkManager::UpdateWeapons(EntityAI *vehicle)
{
	if (_client) _client->UpdateWeapons(vehicle);
}

void NetworkManager::AddWeaponCargo(VehicleSupply *vehicle, RString weapon)
{
	if (_client) _client->AddWeaponCargo(vehicle, weapon);
}

void NetworkManager::RemoveWeaponCargo(VehicleSupply *vehicle, RString weapon)
{
	if (_client) _client->RemoveWeaponCargo(vehicle, weapon);
}

void NetworkManager::AddMagazineCargo(VehicleSupply *vehicle, const Magazine *magazine)
{
	if (_client) _client->AddMagazineCargo(vehicle, magazine);
}

void NetworkManager::RemoveMagazineCargo(VehicleSupply *vehicle, int creator, int id)
{
	if (_client) _client->RemoveMagazineCargo(vehicle, creator, id);
}

void NetworkManager::UpdateObject(NetworkObject *object)
{
	if (_client) _client->UpdateObject(object, NMFGuaranteed);
}

void NetworkManager::VehicleInit(VehicleInitCmd &init)
{
	if (_client) _client->VehicleInit(init);
}

void NetworkManager::OnVehicleDestroyed(EntityAI *killed, EntityAI *killer)
{
	if (_client) _client->OnVehicleDestroyed(killed, killer);
}

void NetworkManager::OnVehicleDamaged(EntityAI *damaged, EntityAI *killer, float damage, RString ammo)
{
	if (_client) _client->OnVehicleDamaged(damaged, killer, damage, ammo);
}

void NetworkManager::OnIncomingMissile(EntityAI *target, RString ammo, EntityAI *owner)
{
	if (_client) _client->OnIncomingMissile(target, ammo, owner);
}

void NetworkManager::MarkerCreate(int channel, AIUnit *sender, RefArray<NetworkObject> &units, ArcadeMarkerInfo &info)
{
	if (_client) _client->MarkerCreate(channel, sender, units, info);
}

void NetworkManager::MarkerDelete(RString name)
{
	if (_client) _client->MarkerDelete(name);
}

void NetworkManager::SetFlagOwner
(
	Person *owner, EntityAI *carrier
)
{
	if (_client) _client->SetFlagOwner(owner, carrier);
}

void NetworkManager::SetFlagCarrier
(
	Person *owner, EntityAI *carrier
)
{
	if (_client) _client->SetFlagCarrier(owner, carrier);
}

void NetworkManager::SendRadioMessage(NetworkSimpleObject *msg)
{
	if (_client) _client->SendMsg(msg, NMFGuaranteed);
}

bool NetworkManager::ProcessCommand(RString command)
{
	if (_client) return _client->ProcessCommand(command);
	return false;
}

void NetworkManager::SendKick(int player)
{
	if (_client) _client->SendKick(player);
}

void NetworkManager::SendLockSession(bool lock)
{
	if (_client) _client->SendLockSession(lock);
}

bool NetworkManager::CanSelectMission() const
{
	if (_client) return _client->CanSelectMission();
	return false;
}

bool NetworkManager::CanVoteMission() const
{
	if (_client) return _client->CanVoteMission();
	return false;
}

const AutoArray<RString> &NetworkManager::GetServerMissions() const
{
	DoAssert(_client);
	return _client->GetServerMissions();
}

void NetworkManager::SelectMission(RString mission, bool cadetMode)
{
	if (_client) _client->SelectMission(mission, cadetMode);
}

void NetworkManager::VoteMission(RString mission, bool cadetMode)
{
	if (_client) _client->VoteMission(mission, cadetMode);
}

void NetworkManager::PublicVariable(RString name)
{
	if (_client) _client->PublicVariable(name);
}

void NetworkManager::Chat(int channel, RString text)
{
	if (_client) _client->Chat(channel, text);
}

void NetworkManager::Chat(int channel, AIUnit *sender, RefArray<NetworkObject> &units, RString text)
{
	if (_client) _client->Chat(channel, sender, units, text);
}

void NetworkManager::Chat(int channel, RString sender, RefArray<NetworkObject> &units, RString text)
{
	if (_client) _client->Chat(channel, sender, units, text);
}

void NetworkManager::RadioChat(int channel, AIUnit *sender, RefArray<NetworkObject> &units, RString text, RadioSentence &sentence)
{
	if (_client) _client->RadioChat(channel, sender, units, text, sentence);
}

void NetworkManager::RadioChatWave(int channel, RefArray<NetworkObject> &units, RString wave, AIUnit *sender, RString senderName)
{
	if (_client) _client->RadioChatWave(channel, units, wave, sender, senderName);
}

void NetworkManager::SetVoiceChannel(int channel)
{
	if (_client) _client->SetVoiceChannel(channel);
}

void NetworkManager::SetVoiceChannel(int channel, RefArray<NetworkObject> &units)
{
	if (_client) _client->SetVoiceChannel(channel, units);
}

void NetworkManager::TransferFile(RString dest, RString source)
{
	if (_client) _client->TransferFile(TO_SERVER, dest, source);
}

void NetworkManager::SendMissionFile()
{
	if (_server) _server->SendMissionFile();
}

RespawnMode NetworkManager::GetRespawnMode() const
{
	if (_client) return _client->GetRespawnMode();
	return RespawnNone;
}

float NetworkManager::GetRespawnDelay() const
{
	if (_client) return _client->GetRespawnDelay();
	return 0;
}

void NetworkManager::Respawn(Person *soldier, Vector3Par pos)
{
	if (_client)
		_client->Respawn(soldier, pos);
}

RString NetworkManager::GetLocalPlayerName() const
{
	if (_client)
		return _client->GetLocalPlayerName();
	return Glob.header.playerName;
}

void NetworkManager::ShowTarget(Person *vehicle, TargetType *target)
{
	if (_client) _client->ShowTarget(vehicle, target);
}

void NetworkManager::ShowGroupDir(Person *vehicle, Vector3Par dir)
{
	if (_client) _client->ShowGroupDir(vehicle, dir);
}

void NetworkManager::GroupSynchronization(AIGroup *grp, int synchronization, bool active)
{
	if (_client) _client->GroupSynchronization(grp, synchronization, active);
}

void NetworkManager::DetectorActivation(Detector *det, bool active)
{
	if (_client) _client->DetectorActivation(det, active);
}

void NetworkManager::AskForCreateUnit(AIGroup *group, RString type, Vector3Par position, RString init, float skill, Rank rank)
{
	if (_client) _client->AskForCreateUnit(group, type, position, init, skill, rank);
}

void NetworkManager::AskForDeleteVehicle(Entity *veh)
{
	if (_client) _client->AskForDeleteVehicle(veh);
}

void NetworkManager::AskForReceiveUnitAnswer
(
	AIUnit *from, AISubgroup *to, int answer
)
{
	if (_client) _client->AskForReceiveUnitAnswer(from,to,answer);
}

void NetworkManager::AskForGroupRespawn(Person *person, EntityAI *killer)
{
	if (_client) _client->AskForGroupRespawn(person, killer);
}

void NetworkManager::AskForActivateMine(Mine *mine, bool activate)
{
	if (_client) _client->AskForActivateMine(mine, activate);
}

void NetworkManager::AskForInflameFire(Fireplace *fireplace, bool fire)
{
	if (_client) _client->AskForInflameFire(fireplace, fire);
}

void NetworkManager::AskForAnimationPhase(Entity *vehicle, RString animation, float phase)
{
	if (_client) _client->AskForAnimationPhase(vehicle, animation, phase);
}

void NetworkManager::CopyUnitInfo(Person *from, Person *to)
{
	if (_client) _client->CopyUnitInfo(from, to);
}

Time NetworkManager::GetEstimatedEndTime() const
{
	if (_client) return _client->GetEstimatedEndTime();
	else return TIME_MIN;
}

void NetworkManager::SetEstimatedEndTime(Time time)
{
	if (_server) _server->SetEstimatedEndTime(time);
}

void NetworkManager::DisposeBody(Person *body)
{
	if (_client) _client->DisposeBody(body);
}

#if _ENABLE_CHEATS
extern bool forceControlsPaused;
#endif

bool NetworkManager::IsControlsPaused()
{
#if _ENABLE_CHEATS
	if (forceControlsPaused) return true;
#endif
	if (_client) return _client->IsControlsPaused();
	return false;
}

float NetworkManager::GetLastMsgAgeReliable()
{
	if (_client) return _client->GetLastMsgAgeReliable();
	return 0;
}

//! Global instance of Network Manager class
NetworkManager GNetworkManager;

INetworkManager &GetNetworkManager() {return GNetworkManager;}

RString GetLocalPlayerName()
{
	return GNetworkManager.GetLocalPlayerName();
}

#if _ENABLE_CHEATS
void NetworkComponent::StatMsgSent(int msg, int size)
{
	for (int i=0; i<_statistics.Size(); i++)
	{
		if (_statistics[i].message == msg)
		{
			_statistics[i].msgSent++;
			_statistics[i].sizeSent+=size;
			return;
		}
	}
	int index = _statistics.Add();
	_statistics[index].message = msg;
	_statistics[index].msgSent++;
	_statistics[index].sizeSent+=size;
}

void NetworkComponent::StatMsgReceived(int msg, int size)
{
	for (int i=0; i<_statistics.Size(); i++)
	{
		if (_statistics[i].message == msg)
		{
			_statistics[i].msgReceived++;
			_statistics[i].sizeReceived+=size;
			return;
		}
	}
	int index = _statistics.Add();
	_statistics[index].message = msg;
	_statistics[index].msgReceived++;
	_statistics[index].sizeReceived+=size;
}

//! Round screen position to pixel
#define CX(x) (toInt((x) * w) + 0.5)
//! Round screen position to pixel
#define CY(y) (toInt((y) * h) + 0.5)

//! Compare statistics items for sorted display
int CmpSentMessages
(
	const NetworkStatisticsItem *info1,
	const NetworkStatisticsItem *info2
)
{
	float diff = info2->msgSent - info1->msgSent;
	if (diff != 0) return sign(diff);
	diff = info2->sizeSent - info1->sizeSent;
	if (diff != 0) return sign(diff);
	return info2->message - info1->message;
}

//! Compare statistics items for sorted display
int CmpReceivedMessages
(
	const NetworkStatisticsItem *info1,
	const NetworkStatisticsItem *info2
)
{
	float diff = info2->msgReceived - info1->msgReceived;
	if (diff != 0) return sign(diff);
	diff = info2->sizeReceived - info1->sizeReceived;
	if (diff != 0) return sign(diff);
	return info2->message - info1->message;
}

//! Draw network statistics by types of messages
void DrawNetworkStatistics()
{
	NetworkComponent *component = GNetworkManager.GetServer();
	if (!component) component = GNetworkManager.GetClient();
	if (!component) return;
	AutoArray<NetworkStatisticsItem> &stat = component->GetStatistics();
	
	RString title;
	switch (outputDiags)
	{
		case 2:
			QSort(stat.Data(), stat.Size(), CmpSentMessages);
			title = "Outgoing messages";
			break;
		case 3:
			QSort(stat.Data(), stat.Size(), CmpReceivedMessages);
			title = "Incomming messages";
			break;
		default:
			return;
	}

	PackedColor color(Color(1, 1, 1, 1));
	static Ref<Font> font;
	static float size;
	if (!font)
	{
		font = GEngine->LoadFont(GetFontID("tahomaB48"));
		size = 0.024;
	}
	float fontHeight = size;

	const float xb = 0.05;
	const float xe = 0.95;
	const float yb = 0.05;
	const float ye = 0.95;
	const float rowHeight = 0.03;
	const int nRows = toInt((ye - yb) / rowHeight) - 1;
	const int columns = 4;
	const float colWidths[columns] =
	{
		0.4,
		0.16,
		0.16,
		0.16
		//0.3
		//0.2,
		//0.3
	};

	const float w = GEngine->Width2D();
	const float h = GEngine->Height2D();

	const float cxb = CX(xb);
	const float cxe = CX(xe);
	const float cyb = CY(yb);
	const float cye = CY(ye);

	// background
	PackedColor bgColor(Color(0, 0, 0, 0.5));
	MipInfo mip = GEngine->TextBank()->UseMipmap(NULL, 0, 0);
	GEngine->Draw2D(mip, bgColor, Rect2DPixel(cxb, cyb, cxe-cxb, cye-cyb));

	// lines
	float bottom = yb;
	float cbottom = CY(bottom);
	GEngine->DrawLine(Line2DPixel(cxb, cbottom, cxe, cbottom), color, color);
	for (int i=0; i<nRows+1; i++)
	{
		bottom += rowHeight;
		cbottom = CY(bottom);
		GEngine->DrawLine(Line2DPixel(cxb, cbottom, cxe, cbottom), color, color);
	}
	float left = xb;
	float cleft = cxb;
	GEngine->DrawLine(Line2DPixel(cleft, cyb, cleft, cbottom), color, color);
	for (int i=0; i<columns; i++)
	{
		left += colWidths[i];
		cleft = CX(left);
		GEngine->DrawLine(Line2DPixel(cleft, cyb, cleft, cbottom), color, color);
	}

	// titles
	float top = yb + 0.5 * (rowHeight - fontHeight);
	left = xb;
	RString str = title;
	float x = left + 0.5 * (colWidths[0] - GEngine->GetTextWidth(size, font, str));
	GEngine->DrawText(Point2DFloat(x, top), size, font, color, str);

	left += colWidths[0];
	str = "# of messages";
	x = left + 0.5 * (colWidths[1] - GEngine->GetTextWidth(size, font, str));
	GEngine->DrawText(Point2DFloat(x, top), size, font, color, str);

	left += colWidths[1];
	str = "Tot. B";
	x = left + 0.5 * (colWidths[2] - GEngine->GetTextWidth(size, font, str));
	GEngine->DrawText(Point2DFloat(x, top), size, font, color, str);

	left += colWidths[2];
	str = "Avg. B";
	x = left + 0.5 * (colWidths[3] - GEngine->GetTextWidth(size, font, str));
	GEngine->DrawText(Point2DFloat(x, top), size, font, color, str);

	// values
	for (int i=0; i<nRows && i<stat.Size(); i++)
	{
		top += rowHeight;

		left = xb;
		str = NetworkMessageTypeNames[stat[i].message];
		x = left + 0.5 * (colWidths[0] - GEngine->GetTextWidth(size, font, str));
		GEngine->DrawText(Point2DFloat(x, top), size, font, color, str);
		left += colWidths[0];

		int val1 = outputDiags == 2 ? stat[i].msgSent : stat[i].msgReceived;
		int val2 = outputDiags == 2 ? stat[i].sizeSent : stat[i].sizeReceived;

		if (val1 > 0)
		{
			x = left + 0.5 * (colWidths[1] - GEngine->GetTextWidthF(size, font, "%d", val1));
			GEngine->DrawTextF(Point2DFloat(x, top), size, font, color, "%d", val1);
		}
		left += colWidths[1];

		if (val2 > 0)
		{
			x = left + 0.5 * (colWidths[2] - GEngine->GetTextWidthF(size, font, "%d", val2));
			GEngine->DrawTextF(Point2DFloat(x, top), size, font, color, "%d", val2);
		}
		left += colWidths[2];
		if (val2 > 0 && val1>0)
		{
			float avg = float(val2)/val1;
			x = left + 0.5 * (colWidths[2] - GEngine->GetTextWidthF(size, font, "%.1f", avg));
			GEngine->DrawTextF(Point2DFloat(x, top), size, font, color, "%.1f", avg);
		}
	}
}

#endif

#if _ENABLE_DEDICATED_SERVER || _ENABLE_CHEATS
void NetworkComponent::StatRawMsgSent(int to, int size)
{
	for (int i=0; i<_rawStatistics.Size(); i++)
	{
		if (_rawStatistics[i].player == to)
		{
			_rawStatistics[i].msgSent++;
			_rawStatistics[i].sizeSent+=size;
			return;
		}
	}
	int index = _rawStatistics.Add();
	_rawStatistics[index].player = to;
	_rawStatistics[index].msgSent++;
	_rawStatistics[index].sizeSent+=size;
}

void NetworkComponent::StatRawMsgReceived(int from, int size)
{
	for (int i=0; i<_rawStatistics.Size(); i++)
	{
		if (_rawStatistics[i].player == from)
		{
			_rawStatistics[i].msgReceived++;
			_rawStatistics[i].sizeReceived+=size;
			return;
		}
	}
	int index = _rawStatistics.Add();
	_rawStatistics[index].player = from;
	_rawStatistics[index].msgReceived++;
	_rawStatistics[index].sizeReceived+=size;
}

#endif

