#ifndef _QUATRIX_HPP
#define _QUATRIX_HPP

typedef __m128 Vec4M128[4];

struct Quatrix4
{
	Vec4M128 qa[3];

	operator Vec4M128 * () {return qa;}
	operator const Vec4M128 * () const {return qa;}
};

typedef __m128 Vec3M128[3];

struct Quatrix3
{
	Vec3M128 qa[3];

	operator Vec3M128 * () {return qa;}
	operator const Vec3M128 * () const {return qa;}
};

#endif

