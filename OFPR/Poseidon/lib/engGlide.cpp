// Glide engine implementation
// (C) 1997, Suma
#include "wpch.hpp"

#if !_DISABLE_GUI && !defined _XBOX

#include "lights.hpp"
//#include "engineDll.hpp"
#include "tlVertex.hpp"
#include "engine.hpp"
#include "clip2D.hpp"
#include <El/ParamFile/paramFile.hpp>
#include "global.hpp"
#include <El/common/perfLog.hpp>
#include "keyInput.hpp"
#include "dikCodes.h"

#include "scene.hpp" // TODO: avoid
#include "camera.hpp"

#include <stddef.h>

#include "engGlide.hpp"

#pragma comment(lib,"glide3x")

#define FONT_SIZE 16

#if _ENABLE_PERFLOG
static bool LogTexSwitches;
static bool LogStateSwitches; // TODO: implement

static LogFile TexStateLog;
#endif

extern bool UseWindow;


LONG CALLBACK AppWndProc(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);
extern bool NoGlideDeactivate;

// list of all available resolutions

struct GlideRes {int w,h,num;};

const static GlideRes Resolutions[]=
{
	//{512,384,GR_RESOLUTION_512x384},
	//{640,400,GR_RESOLUTION_640x400},
	{640,480,GR_RESOLUTION_640x480},
	{800,600,GR_RESOLUTION_800x600},
	//{856,480,GR_RESOLUTION_856x480},
	{960,720,GR_RESOLUTION_960x720},
	{1024,768,GR_RESOLUTION_1024x768},
	{1280,1024,GR_RESOLUTION_1280x1024},
	{1600,1200,GR_RESOLUTION_1600x1200},
	{0,0,0},
};


HWND AppInit
(
	HINSTANCE hInst,HINSTANCE hPrev,int sw, bool fullscreen,
	int x, int y, int width, int height
);

#ifndef NDEBUG
static void GrErrorCallbackFnc( const char *string, FxBool fatal )
{
	if( fatal ) ErrorMessage("Glide fatal error: %s",string);
	else
	{
		MessageBox(NULL,string,"Glide error",MB_OK);
	}
}
#endif

bool EngineGlide::_initDone=false; // static member init.

EngineGlide::EngineGlide
(
	HINSTANCE hInst, HINSTANCE hPrev, int sw,
	int width, int height, bool fullscreen
)
:_currentBias(0),
_context(0),
_alphaTestValue(1), // nothing set
_gamma(1),
_fogCoordExtension(false),
_textBank(NULL),
_textColor(0x80ffffc0)
{
	(void)hInst,(void)hPrev,(void)sw;

	//_font=NULL;
	_lastSpec=0;
	_refresh = 75;

	int i;
	for( i=0; i<MaxTMUs; i++ ) _lastClampU[i]=_lastClampV[i]=false;
	
	//_lastPart=NULL;
	_lastMipmap=NULL;
	_lastTexture=NULL;
	_lastLevel=NULL;

	// _fogColor was uninitialized, detected by BoundsChecker
	// no effect - if would be initialized later
	_fogColor=HWhite;

	_nTMUs=1;

	#ifndef NDEBUG
		grErrorSetCallback(GrErrorCallbackFnc);
	#endif
		
		// create main application window
	if( fullscreen ) _hwndApp=AppInit(hInst,hPrev,sw,FALSE,0,0,160,160);
	else _hwndApp=AppInit(hInst,hPrev,sw,FALSE,0,0,width,height);
	if( !_hwndApp )
	{
		ErrorMessage("Cannot create application window.");
	}

	LoadConfig();

	_w = width;
	_h = height;
	InitGlide(fullscreen);

}

FxI32 GetValue( FxU32 name )
{
	FxI32 getVal;
	Verify( grGet(name,sizeof(getVal),&getVal)==sizeof(getVal) );
	return getVal;
}

static bool IsExtension( const char *name )
{
	const char *extensions=grGetString(GR_EXTENSION);
	const char *where=strstr(extensions,name);
	if( !where ) return false;
	if( where>name && where[-1]!=' ' ) return false;
	const char *endWhere=where+strlen(name);
	if( *endWhere!=0 && *endWhere!=' ' ) return false;
	return true;
}

static GrColor_t PackColor( ColorVal c )
{
	Color s = c;
	s.Saturate();
	//int a = c.A8();
	int r = c.R8();
	int g = c.G8();
	int b = c.B8();
	//saturate(a,0,255);
	DoAssert((r&~0xff)==0);
	DoAssert((g&~0xff)==0);
	DoAssert((b&~0xff)==0);
	//DoAssert((a&~0xff)==0);
	saturate(r,0,255);
	saturate(g,0,255);
	saturate(b,0,255);
	return 0xff000000|(r<<16)|(g<<8)|b;
}

void EngineGlide::FogColorChanged( ColorVal fogColor )
{
	grFogColorValue(PackColor(_fogColor));
	//grFogColorValue(0);
}

void EngineGlide::SetMultiTexturing( VFormatSet format )
{
	if( _formatSet==format ) return;
	switch( format )
	{
		default:
			//grVertexLayout(GR_PARAM_ST1, offsetof(GlideVertex,tmuvtx), GR_PARAM_ENABLE);
			grVertexLayout(GR_PARAM_ST1, offsetof(TLVertex,t0), GR_PARAM_ENABLE);
		break;
		case DetailTex:
			ActivateDetailTexturing();
			grVertexLayout(GR_PARAM_ST1, offsetof(TLVertex,t1), GR_PARAM_ENABLE);
		break;
		case SpecularTex:
			ActivateSpecularTexturing();
			grVertexLayout(GR_PARAM_ST1, offsetof(TLVertex,t1), GR_PARAM_ENABLE);
		break;
	}
	_formatSet=format;
}

// destroy engine
EngineGlide::~EngineGlide()
{
	SaveConfig();
	Log("Destruct Glide engine");
	_fonts.Clear();
	if( _textBank ) delete _textBank,_textBank=NULL;
	if( _context )
	{
		grSstWinClose(_context);
		grGlideShutdown(),_context=0;
	}
}

bool EngineGlide::SwitchRes(int w, int h, int bpp)
{
	// switching resolution is meaningfull only in fullscreen mode
	if( w!=_w || h!=_h )
	{
		// reinit Glide with different parameters, take care of GRAM changes
		DeinitGlide();
		_w = w;
		_h = h;
		InitGlide(true);
	}
	return TRUE;
}

int EngineGlide::RefreshRate() const
{
	return _refresh;
}


bool EngineGlide::SwitchRefreshRate(int refresh)
{
	if (_refresh==refresh) return true;
	DeinitGlide();
	_refresh = refresh;
	InitGlide(!UseWindow);
	return true;
}

static const GlideRes *FindResolution(int resol)
{
	for (int i=0; i<sizeof(Resolutions)/sizeof(*Resolutions); i++)
	{
		if (resol==Resolutions[i].num) return &Resolutions[i];
	}
	return NULL;
}

static bool ResolutionAvailable( const GrResolution *list, int listSize, int resol )
{
	for( int i=0; i<listSize; i++ )
	{
		if( list[i].resolution==resol ) return true;
	}
	return false;
}

TypeIsSimple(GrResolution);

void EngineGlide::ListResolutions(FindArray<ResolutionInfo> &ret)
{

	// find all possible modes that include a z-buffer
	GrResolution	query;
	query.resolution	= GR_QUERY_ANY;
	query.refresh	= GR_QUERY_ANY;
	query.numColorBuffers	= GR_QUERY_ANY;
	query.numAuxBuffers	= 1;

	int listSize = grQueryResolutions(&query,NULL); 
	Temp<GrResolution> list(listSize);
	grQueryResolutions(&query,list);

	int i,dst;
	// convert Glide list to our own list

	for( i=0,dst=0; ; i++ )
	{
		int w=Resolutions[i].w;
		int h=Resolutions[i].h;
		if( w<=0 || h<=0 ) break;
		int num=Resolutions[i].num;
		if( ResolutionAvailable(list,listSize,num) )
		{
			ResolutionInfo info;
			info.w=w;
			info.h=h;
			info.bpp=16;
			ret.AddUnique(info);
		}
	}
}

// convert refresh rate


static int RefreshFromGlide(GrScreenRefresh_t ref)
{
	switch (ref)
	{
		case GR_REFRESH_60Hz: return 60;
		case GR_REFRESH_70Hz: return 70;
		case GR_REFRESH_72Hz: return 72;
		case GR_REFRESH_75Hz: return 75;
		case GR_REFRESH_80Hz: return 80;
		case GR_REFRESH_90Hz: return 90;
		case GR_REFRESH_100Hz: return 100;
		case GR_REFRESH_85Hz: return 85;
		case GR_REFRESH_120Hz: return 120;
	}
	return 0;
}

static GrScreenRefresh_t RefreshToGlide(int ref)
{
	switch (ref)
	{
		case 60: return GR_REFRESH_60Hz;
		case 70: return GR_REFRESH_70Hz;
		case 72: return GR_REFRESH_72Hz;
		case 75: return GR_REFRESH_75Hz;
		case 80: return GR_REFRESH_80Hz;
		case 90: return GR_REFRESH_90Hz;
		case 100: return GR_REFRESH_100Hz;
		case 85: return GR_REFRESH_85Hz;
		case 120: return GR_REFRESH_120Hz;
	}
	return -1;
}

void EngineGlide::ListRefreshRates(FindArray<int> &ret)
{
	// list refresh rates connected with current resolution
	// find all possible modes that include a z-buffer
	GrResolution	query;
	query.resolution	= GR_QUERY_ANY;
	query.refresh	= GR_QUERY_ANY;
	query.numColorBuffers	= GR_QUERY_ANY;
	query.numAuxBuffers	= 1;

	int listSize = grQueryResolutions(&query,NULL); 
	Temp<GrResolution> list(listSize);
	grQueryResolutions(&query,list);

	for (int i=0; i<listSize; i++)
	{
		const GrResolution &res = list[i];
		const GlideRes *gr = FindResolution(res.resolution);
		if (!gr) continue;
		if (gr->h!=_h || gr->w!=_w) continue;
		int ref = RefreshFromGlide(res.refresh);
		if (ref!=0) ret.AddUnique(ref);
	}
}

bool EngineGlide::Detect()
{
	if( !_initDone ) grGlideInit(),_initDone=true;
	return GetValue(GR_NUM_BOARDS)>0;
}


GRAMOffset EngineGlide::TexMinAddress( int tmu ) const
{
	return grTexMinAddress(tmu);
}
GRAMOffset EngineGlide::TexMaxAddress( int tmu ) const
{
	return grTexMaxAddress(tmu);
}

void EngineGlide::Clear( bool clearZ, bool clear, PackedColor color )
{
	grColorMask(clear,false);
	SetDepthMask(clearZ);
	FxI32 wFarthest=0;
	if (clearZ)
	{
		FxI32 wMinMax[2];
		grGet(GR_WDEPTH_MIN_MAX,sizeof(wMinMax),wMinMax);
		wFarthest=wMinMax[1];
	}
	grBufferClear(color,0,wFarthest);
	// return to default mask
	grColorMask(true,false);
}

void EngineGlide::Recreate()
{
	// close the context
	LogF("Release all gram");
	_textBank->RecreateGRAM();

	if (_context)
	{
		LogF("Close window");
		grSstWinClose(_context);
		_context = NULL;
	}

	// create it again
	LogF("InitGlide");
	InitGlide(!UseWindow);
}

// clear


void EngineGlide::InitDraw( bool clear, PackedColor color )
{
	base::InitDraw();
	if( !grSelectContext(_context) )
	{
		// TODO: all lost (including textures) - reestablish status
		// we need to refresh all textures
		// we should do grSstWinClose
		// and then grSstWinOpen again
		//Recreate();
	}
	
	grColorMask(clear,false);
	SetDepthMask(true);
	FxI32 wMinMax[2];
	grGet(GR_WDEPTH_MIN_MAX,sizeof(wMinMax),wMinMax);
	// note: we are not sure if farthest if maximum
	FxI32 wFarthest=wMinMax[1];
	//PackedColor packed=PackedColor(_fogColor);
	//if( clear ) packed=color;
	grBufferClear(color,0,wFarthest);
	grColorMask(true,false);


	#if _ENABLE_PERFLOG
	if (GInput.GetCheat1ToDo(DIK_Q))
	{
		LogTexSwitches=true;
		LogStateSwitches=true;
		TexStateLog.Open("textState.log");
		TexStateLog << "Starting Texture & State Log ";
	}
	#endif

	_textBank->StartFrame();
	_lastSpec=-1; // assume unknown render state

	// restore clamping state - it can be changed
	// (star rendering?)
	for( int tmu=0; tmu<_nTMUs; tmu++ )
	{
		grTexClampMode(tmu,_lastClampU[tmu],_lastClampV[tmu]);
	}
}

// flip
void EngineGlide::FinishDraw()
{
	base::FinishDraw();

	#if _ENABLE_PERFLOG
	LogTexSwitches=false;
	LogStateSwitches=false;
	#endif

	_textBank->FinishFrame();	

	base::DrawFinishTexts();

	#if _ENABLE_PERFLOG
	if (TexStateLog.IsOpen())
	{
		TexStateLog << "\nClosing Texture & State Log\n";
		TexStateLog.Close();
	}
	LogStateSwitches=false;
	LogTexSwitches=false;
	#endif

	if (_context)
	{
		// minimize latency and avoid PCI stalls
		const int maxPendingBuffers=1;
		//DWORD start=::GlobalTickCount();
		while( GetValue(GR_PENDING_BUFFERSWAPS)>maxPendingBuffers )
		{
			#if 1
				Sleep(0); // Sleep will show up as WMM in VTune
			#else
				 // we want to see waiting for pending buffers in profiler
				for( volatile int wait=200; --wait>=0; ){}
			#endif
		}
		//DWORD end=::GlobalTickCount();
		//if( end>start )
		//{
		//	LogF("Pending buffers for %d ms",end-start);
		//}

		grBufferSwap(1); // flip on first VSync
	}
	

}

int EngineGlide::FrameTime() const
{
	// time elapsed from the beginnig of the frame (in ms)
	if( _frameTime ) return GlobalTickCount()-_frameTime;
	else return 0; // first frame can be rendered slow
}


void EngineGlide::Activate()
{
	if (_hasPassthrough)
	{
		grEnable(GR_PASSTHRU);
	}
	else
	{
		// open/close context on task switch
		if (!_context)
		{
			LogF("Recreate - activate");
			Recreate();
		}
	}
}
void EngineGlide::Deactivate()
{
	if (_hasPassthrough)
	{
		if( !NoGlideDeactivate ) grDisable(GR_PASSTHRU);
	}
	else
	{
		// close context
		if (_context)
		{
			LogF("Close window - deactivate");
			grSstWinClose(_context);
			_context = NULL;
			// send minimize message to the window
			//MSG msg;
			//memset(msg,0,sizeof(msg));
			//msg.hwnd = _hwndApp;
			//msg.message = WM_SIZE;
			//msg.wParam = SIZE_MINIMIZED;
			PostMessage(_hwndApp,WM_SYSCOMMAND,SC_MINIMIZE,0);
		}
	}
}
void EngineGlide::Resize( int x, int y, int w, int h )
{
}

/*
bool EngineGlide::CaptureMessage( HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	// only full screen in Glide3
	if( hwnd==_hwndApp ) switch( message )
	{
		case WM_ACTIVATE:
		{
			// TODO: open/close context on task switch
			WORD fActive = LOWORD(wParam);
			BOOL fMinimized = (BOOL) HIWORD(wParam);
			if ( ( fActive == WA_INACTIVE ) || fMinimized )
			{
				if( !NoGlideDeactivate ) grDisable(GR_PASSTHRU);
			}
			else
			{
				grEnable(GR_PASSTHRU);
			}
		}
		break;
	}
	return true;
}
*/

void EngineGlide::Pause()
{
	ShowCursor(TRUE);
}


void EngineGlide::Restore()
{
	//ErrorMessage("Not implemented: Cannot restore");
	// window restored (from minimized)
	// all texture cache is lost and must be reloaded
	ShowCursor(FALSE);
	/*
	if( _font ) _font->ReleaseGRAM();
	*/
	//_textBank->ReloadAll();
}

void EngineGlide::StopAll()
{
	if( _textBank ) _textBank->StopAll();
}

void EngineGlide::TextureDestroyed( Texture *tex )
{
	if( _lastTexture==static_cast<TextureGlide *>(tex) )
	{
		_lastTexture=NULL;
		_lastMipmap=NULL;
	}
}


/*
#if 0
	void ValidateCoord( float &c ){}
#else
	#define LOG_INVALID 0
	void ValidateCoord( float &c )
	{
		const int minC=-200,maxC=+1200;
		//	const int minC=-2047,maxC=+2047;
		// coordinate must be within range -2048...2047
		// int comparison is much faster
		int iC=toIntFloor(c);
		if( iC<minC )
		{
			//c=minC;
			LogF("Invalid coord (min) %.f",c);
		}
		else if( iC>maxC )
		{
			//c=maxC;
			LogF("Invalid coord (max) %.f",c);
		}
	}
#endif
*/

void EngineGlide::PrepareMesh( int spec )
{
	// prepare internal variables
}

void EngineGlide::BeginMesh( TLVertexTable &mesh, int spec )
{
	if( mesh.NVertex()<=0 ) return;
	// u,v cannot be converted if we do not know texture aspect ratio
	// mesh is already clipped - allocate exactly for mesh
	//_meshCache.Realloc(mesh.NVertex());
	//_meshCache.Resize(mesh.NVertex());

	_mesh=&mesh;
	TLVertex *v=mesh.VertexData();
	int n=mesh.NVertex();
	//LogF("BeginMesh %d",n);
	float coef = GScene->GetCamera()->ClipNear();

	for( int i=0; i<n; i++,v++ )
	{
		// vertices are D3D ready
		// perform conversion
		// correct texture mapping coordinates and fog

		// note: rhw is positive now
		float oow = coef*v->rhw;
		
		saturate(oow,1.0/65536,0.99999);

		v->rhw = oow;

		v->t0.u*=oow;
		v->t0.v*=oow;

		// coefs. for multitexturing
		v->t1.u*=oow;
		v->t1.v*=oow;

		v->pos[2]=(0xff-v->specular.A8())*(65535.0/255);
	}
}

void EngineGlide::EndMesh( TLVertexTable &mesh )
{
	Assert( _mesh==&mesh );
	_mesh=NULL;
	//_meshCache.Clear();
}


void EngineGlide::GetZCoefs(float &zAdd, float &zMult)
{
	int zBias = _currentBias;
	zMult = 1.0f-zBias*1e-7f;
	zAdd = zBias*-2e-7f;
}

void EngineGlide::SetBias( int value )
{
	if( _currentBias!=value )
	{
		_currentBias=value;
		grDepthBiasLevel(-_currentBias);
	}
}

void EngineGlide::ActivateTMU( int aTmu )
{
	if( _tmuActive==aTmu ) return;
	if( aTmu>=0 )
	{
		for( int tmu=0; tmu<=aTmu; tmu++ )
		{
			if( tmu<aTmu )
			{
				grTexCombine
				(
					GR_TMU0+tmu,
					GR_COMBINE_FUNCTION_SCALE_OTHER,GR_COMBINE_FACTOR_ONE,
 					GR_COMBINE_FUNCTION_SCALE_OTHER,GR_COMBINE_FACTOR_ONE,
 					FXFALSE,FXFALSE
				);
			}
			else
			{
				grTexCombine
				(
					GR_TMU0+tmu,
					GR_COMBINE_FUNCTION_LOCAL,GR_COMBINE_FACTOR_ONE,
 					GR_COMBINE_FUNCTION_LOCAL,GR_COMBINE_FACTOR_ONE,
 					FXFALSE,FXFALSE
				);
			}
		}
	}
	else
	{
		// produce pure white
		grTexCombine
		(
			GR_TMU0,
			GR_COMBINE_FUNCTION_SCALE_OTHER,GR_COMBINE_FACTOR_ZERO,
 			GR_COMBINE_FUNCTION_SCALE_OTHER,GR_COMBINE_FACTOR_ZERO,
 			FXTRUE,FXTRUE
		);
	}
	_tmuActive=aTmu;
}

enum
{
	tmuDetailTexturing=0x100,
	tmuSpecularTexturing
};

void EngineGlide::ActivateDetailTexturing()
{
	if( _nTMUs<=1 ) return;
	if( _tmuActive==tmuDetailTexturing ) return;
	// set detail texture source in tmu 1
	TextureGlide *detail=_textBank->DetailTexture();
	MipInfo mipI=_textBank->UseMipmap(detail,0,0);

	MipmapLevelGlideInfo *useMip=&detail->_mipmapsInfo[mipI._level];

	GRAMOffset baseAddr=detail->_isGRAM->Memory();
	Assert( detail->_tmu==1 );
	#if _ENABLE_PERFLOG
	if (LogStateSwitches)
	{
		TexStateLog << "\nDetail " << detail->Name()<< " ";
	}
	#endif
	grTexSource
	(
		1,baseAddr,GR_MIPMAPLEVELMASK_BOTH,
		const_cast<GrTexInfo *>(&useMip->_info)
	);
	// set tmu combine units
	grTexCombine
	(
		GR_TMU0,
		GR_COMBINE_FUNCTION_SCALE_OTHER_MINUS_LOCAL_ADD_LOCAL,GR_COMBINE_FACTOR_DETAIL_FACTOR,
		//GR_COMBINE_FUNCTION_SCALE_OTHER_ADD_LOCAL,GR_COMBINE_FACTOR_DETAIL_FACTOR,
		GR_COMBINE_FUNCTION_LOCAL,GR_COMBINE_FACTOR_ONE, // set alpha to local only
		FXFALSE, FXFALSE
	);
	grTexCombine
	(
		GR_TMU1,
		GR_COMBINE_FUNCTION_LOCAL,GR_COMBINE_FACTOR_ONE,
		GR_COMBINE_FUNCTION_LOCAL,GR_COMBINE_FACTOR_ONE,
		FXFALSE,FXFALSE
	);
	//grTexDetailControl(GR_TMU0,+4,1,0.3);
	grTexDetailControl(GR_TMU0,12,3,0.7);
	grTexClampMode(GR_TMU1,FXFALSE,FXFALSE);
	_lastClampU[1]=false,_lastClampV[1]=false;
	_tmuActive=tmuDetailTexturing;
}

void EngineGlide::ActivateSpecularTexturing()
{
	if( _nTMUs<=1 ) return;
	if( _tmuActive==tmuSpecularTexturing ) return;
	// set detail texture source in tmu 1
	TextureGlide *specular=_textBank->SpecularTexture();
	MipInfo mipI=_textBank->UseMipmap(specular,0,0);
	MipmapLevelGlideInfo *useMip=&specular->_mipmapsInfo[mipI._level];
	GRAMOffset baseAddr=specular->_isGRAM->Memory();
	Assert( specular->_tmu==1 );
	#if _ENABLE_PERFLOG
	if (LogStateSwitches)
	{
		TexStateLog << "\nSpecular " << specular->Name() << " ";
	}
	#endif
	grTexSource
	(
		1,baseAddr,GR_MIPMAPLEVELMASK_BOTH,
		const_cast<GrTexInfo *>(&useMip->_info)
	);
	// set tmu combine units
	grTexCombine
	(
		GR_TMU0,
		GR_COMBINE_FUNCTION_SCALE_OTHER_ADD_LOCAL,GR_COMBINE_FACTOR_DETAIL_FACTOR,
		GR_COMBINE_FUNCTION_LOCAL,GR_COMBINE_FACTOR_ONE, // set alpha to local only
		FXFALSE, FXFALSE
	);
	grTexCombine
	(
		GR_TMU1,
		GR_COMBINE_FUNCTION_LOCAL,GR_COMBINE_FACTOR_ONE,
		GR_COMBINE_FUNCTION_LOCAL,GR_COMBINE_FACTOR_ONE,
		FXFALSE,FXFALSE
	);
	grTexDetailControl(GR_TMU0,+10,3,0.25);
	grTexClampMode(GR_TMU1,FXFALSE,FXFALSE);
	_lastClampU[1]=false,_lastClampV[1]=false;
	_tmuActive=tmuSpecularTexturing;
}

#if OPT_STATS

static int ChangeRequests=0;
static int ChangeDone=0;

void StateChangeRequest()
{
	ChangeRequests++;
}

void StateChangeDone()
{
	ChangeDone++;
	if( ChangeDone>=1000 )
	{
		LogF("Optimized %d to %d",ChangeRequests,ChangeDone);
		ChangeRequests=0;
		ChangeDone=0;
	}
}

#endif

void EngineGlide::DoSetAlphaTestValue( int value )
{
	StateChangeDone();
	_alphaTestValue=value;
	grAlphaTestReferenceValue(value);
}

void EngineGlide::DoSetFogMode( int mode )
{
	StateChangeDone();
	_fogMode=mode;
	grFogMode(mode);
}

enum AlphaCombineMode
{
	AlphaGouraudTexture,
	AlphaTransparentTexture,
	AlphaTexture,
	AlphaGouraud,
};


void EngineGlide::DoSetAlphaCombine( AlphaCombineMode mode )
{
	StateChangeDone();
	_alphaCombineMode=mode;
	switch( mode )
	{
		case AlphaGouraudTexture:
			grAlphaCombine
			(
				GR_COMBINE_FUNCTION_SCALE_OTHER,GR_COMBINE_FACTOR_LOCAL,
				GR_COMBINE_LOCAL_ITERATED,GR_COMBINE_OTHER_TEXTURE, 
				FXFALSE
			);
		return;
		case AlphaTransparentTexture:
			grAlphaCombine
			(
				GR_COMBINE_FUNCTION_LOCAL,
				GR_COMBINE_FACTOR_NONE,
				GR_COMBINE_LOCAL_CONSTANT,GR_COMBINE_OTHER_CONSTANT,
				FXFALSE
			);
		return;
		case AlphaTexture:
			grAlphaCombine
			(
				GR_COMBINE_FUNCTION_SCALE_OTHER,GR_COMBINE_FACTOR_ONE,
				GR_COMBINE_LOCAL_NONE,GR_COMBINE_OTHER_TEXTURE, 
				FXFALSE
			);
		return;
		case AlphaGouraud:
			grAlphaCombine
			(
				GR_COMBINE_FUNCTION_LOCAL,GR_COMBINE_FACTOR_NONE,
				GR_COMBINE_LOCAL_ITERATED,GR_COMBINE_OTHER_NONE, 
				FXFALSE
			);
		return;
	}
}

enum AlphaBlendMode
{
	AlphaBlendBlack,
	AlphaBlendNormal,
	AlphaBlendAdd,
	AlphaBlendNo,
	AlphaBlendNoBlack,
};

void EngineGlide::DoSetAlphaBlend( AlphaBlendMode mode )
{
	StateChangeDone();
	_alphaBlendMode=mode;

	switch( mode )
	{
		case AlphaBlendBlack:
			grAlphaBlendFunction
			(
				GR_BLEND_ZERO,GR_BLEND_ONE_MINUS_SRC_ALPHA,
				GR_BLEND_ONE,GR_BLEND_ONE
			);
		break;
		case AlphaBlendNormal:
			grAlphaBlendFunction
			(
				GR_BLEND_SRC_ALPHA,GR_BLEND_ONE_MINUS_SRC_ALPHA,
				GR_BLEND_ONE,GR_BLEND_ONE
			);
		break;
		case AlphaBlendAdd:
			grAlphaBlendFunction
			(
				GR_BLEND_SRC_ALPHA,GR_BLEND_ONE,
				GR_BLEND_ONE,GR_BLEND_ONE
			);
		break;
		case AlphaBlendNo:
			grAlphaBlendFunction
			(
				GR_BLEND_ONE,GR_BLEND_ZERO,
				GR_BLEND_ONE,GR_BLEND_ONE
			);
		break;
		case AlphaBlendNoBlack:
			grAlphaBlendFunction
			(
				GR_BLEND_ZERO,GR_BLEND_ZERO,
				GR_BLEND_ONE,GR_BLEND_ONE
			);
		break;

	}
}

enum ColorCombineMode
{
	ColorGouraudTexture,
	ColorGouraud,
};

void EngineGlide::DoSetColorCombine( ColorCombineMode mode )
{
	StateChangeDone();
	_colorCombineMode=mode;
	switch( mode )
	{
		case ColorGouraudTexture:
			grColorCombine
			(
				GR_COMBINE_FUNCTION_SCALE_OTHER,GR_COMBINE_FACTOR_LOCAL,
				GR_COMBINE_LOCAL_ITERATED,GR_COMBINE_OTHER_TEXTURE,
				FXFALSE
			);
		return;
		case ColorGouraud:
			grColorCombine
			(
				GR_COMBINE_FUNCTION_LOCAL,GR_COMBINE_FACTOR_NONE,
				GR_COMBINE_LOCAL_ITERATED,GR_COMBINE_OTHER_NONE,
				FXFALSE
			);
		return;
	}
}


void EngineGlide::DoSetDepthFunction( int value )
{
	StateChangeDone();
	_depthFunction=value;
	grDepthBufferFunction(value);
}
void EngineGlide::DoSetDepthMask( bool value )
{
	StateChangeDone();
	_depthMask=value;
	grDepthMask(value);
}

void EngineGlide::DoEnableAA( bool value )
{
	StateChangeDone();
	_aaEnabled=value;
	if( value ) grEnable(GR_AA_ORDERED);
	else grDisable(GR_AA_ORDERED);
}

void EngineGlide::SetGamma( float g )
{
	_gamma=g;
	if (!_context) return;
	guGammaCorrectionRGB(_gamma,_gamma,_gamma);
}

RString GetUserParams();

extern RString FlashpointCfg;

void EngineGlide::SaveConfig()
{
	{
		// gloabal config
		ParamFile cfg;
		cfg.Parse(FlashpointCfg);

		cfg.Add("Resolution_W",_w);
		cfg.Add("Resolution_H",_h);
		cfg.Add("refresh",_refresh);

		cfg.Save(FlashpointCfg);
	}
	{
		// user config
		RString name = GetUserParams();
		ParamFile cfg;
		cfg.Parse(name);
		cfg.Add("gamma", _gamma);
/*
		cfg.Add("width",_w);
		cfg.Add("height",_h);
		cfg.Add("refresh",_refresh);
*/
		cfg.Save(name);
	}
	base::SaveConfig();
}

static float LoadCfgDef(const ParamEntry &cfg, const char *name, float def)
{
	const ParamEntry *entry = cfg.FindEntry(name);
	if (!entry) return def;
	return *entry;
}
void EngineGlide::LoadConfig()
{
	{
		// global config
		ParamFile cfg;
		cfg.Parse(FlashpointCfg);

		_w = LoadCfgDef(cfg,"Resolution_W",_w);
		_h = LoadCfgDef(cfg,"Resolution_H",_h);
		_refresh = LoadCfgDef(cfg,"refresh",_refresh);
	}
	{
		// user config
		RString name = GetUserParams();
		ParamFile cfg;
		cfg.Parse(name);
		SetGamma(LoadCfgDef(cfg,"gamma",1.6));
/*
		_w = LoadCfgDef(cfg,"width",_w);
		_h = LoadCfgDef(cfg,"height",_h);
		_refresh = LoadCfgDef(cfg,"refresh",_refresh);
*/
	}
	base::LoadConfig();
}

inline int EngineGlide::FogEnable() const
{
	if( _fogCoordExtension ) return GR_FOG_WITH_TABLE_ON_FOGCOORD_EXT;
	return GR_FOG_WITH_ITERATED_Z;
}
inline int EngineGlide::FogDisable() const
{
	return GR_FOG_DISABLE;
}

RString EngineGlide::GetDebugName() const
{
	RString ret = "3Dfx Glide";

	ret = ret + RString(grGetString(GR_VENDOR)) + RString(";");
	ret = ret + RString(grGetString(GR_HARDWARE)) + RString(";");
	ret = ret + RString(grGetString(GR_RENDERER)) + RString(";");
	ret = ret + RString(grGetString(GR_VERSION));
	return ret;
}

void EngineGlide::InitGlide(bool fullscreen)
{
	
	// find supported mode nearest to width, height
	// compare area
	// release all surfaces

	
	if( !_context )
	{
		if( !_initDone ) grGlideInit(),_initDone=true;
		//fullscreen=true;
		// select Voodoo2 over Banshee?
		#if !_RELEASE
			int maxB=GetValue(GR_NUM_BOARDS);
			Log("%d 3Dfx boards",maxB);
			const char *vendName=grGetString(GR_VENDOR);
			const char *hwName=grGetString(GR_HARDWARE);
			const char *swName=grGetString(GR_RENDERER);
			const char *verName=grGetString(GR_VERSION);
			Log("HW Name: %s %s",vendName,hwName);
			Log("SW Name: %s %s",swName,verName);
		#endif
		grSstSelect(0);
	}

	if( !fullscreen )
	{

		RECT client;
		// adjust width, height to match client area
		GetClientRect(_hwndApp,&client);
		_w=client.right-client.left+1;
		_h=client.bottom-client.top+1;

		_context=grSstWinOpen
		(
			(DWORD)_hwndApp, // window handle
			//NULL, // window handle
			GR_RESOLUTION_NONE,GR_REFRESH_NONE,
			GR_COLORFORMAT_ARGB,
			GR_ORIGIN_UPPER_LEFT,
			2,1
		);
		// if we fail, it is probably not VoodooRush board - switch to full screen
		if( !_context ) goto FullScreen;
		/*
		if( !_context )
		{
			_w=_h=0;
			ErrorMessage("Cannot initialize Glide window (%dx%d)",width,height);
			return; // Error
		}
		*/
	}
	else
	{
		FullScreen:

		// select resolution nearest to given
		FindArray<ResolutionInfo> resol;
		ListResolutions(resol);
		int bestI=0,bestDif=INT_MAX;
		for( int i=0; i<resol.Size();i++ )
		{
			int w=resol[i].w;
			int h=resol[i].h;
			if( w<=0 || h<=0 ) break;
			int dif=abs(w-_w)+abs(h-_h);
			if( bestDif>=dif ) bestDif=dif,bestI=i;
		}
		_w=resol[bestI].w;
		_h=resol[bestI].h;

		GrScreenRefresh_t rf = RefreshToGlide(_refresh);
		LogF("Refresh glide %d <-%d",rf);
		if (rf<0) rf = GR_REFRESH_60Hz;

		// use triple buffering if possible
		#if 1
		// TODO: on UMA: calculate how much memory will be left for textures
		if( _w*_h<1200*1024 )
		{
			_context=grSstWinOpen
			(
				(DWORD)_hwndApp, // window handle
				//NULL, // window handle
				Resolutions[bestI].num,rf,
				GR_COLORFORMAT_ARGB,
				GR_ORIGIN_UPPER_LEFT,
				3,1
			);
			if( _context ) _tripleBuffering=true;
		}
		else
		#endif
		{
			_tripleBuffering=false;
			_context=grSstWinOpen
			(
				(DWORD)_hwndApp, // window handle
				//NULL, // window handle
				Resolutions[bestI].num,rf,
				GR_COLORFORMAT_ARGB,
				GR_ORIGIN_UPPER_LEFT,
				2,1
			);
		}
		if( !_context )
		{
			ErrorMessage("Cannot initialize Glide (%dx%d)",_w,_h);
			_w=_h=0;
			return; // Error
		}
	}

	Log("Glide resolution %d,%d",_w,_h);
	_nTMUs=GetValue(GR_NUM_TMU);
	Log("Glide: %d TMUs",_nTMUs);
	

	// default assume Voodoo or Voodoo2
	_hasPassthrough = true;

	// if it is UMA, assume no separate memory
	int memUma=GetValue(GR_MEMORY_UMA);
	if (memUma>0)
	{
		LogF("Uma %d",memUma);
		_hasPassthrough = false; // no UMA has passthrough
	}


	// create texture bank or change GRAM settings of existing bank
	if( !_textBank ) _textBank=new TextBankGlide(this);
	else
	{
		//_textBank->RecreateGRAM();
		_textBank->RecreateGRAM();
		_textBank->InitGRAM();
	}

	#if 0
	if( IsExtension("FOGCOORD") )
	{
		LogF("Fog coord extension supported.");
		_fogCoordExtension=true;
	}
	#endif

	grCoordinateSpace(GR_WINDOW_COORDS);
	grVertexLayout(GR_PARAM_XY, offsetof(TLVertex,pos[0]), GR_PARAM_ENABLE);
	grVertexLayout(GR_PARAM_PARGB, offsetof(TLVertex,color), GR_PARAM_ENABLE);
	grVertexLayout(GR_PARAM_Q, offsetof(TLVertex,rhw), GR_PARAM_ENABLE);
	if( _fogCoordExtension )
	{
		grVertexLayout(GR_PARAM_FOG_EXT, offsetof(TLVertex,pos[2]), GR_PARAM_ENABLE);
	}
	else
	{
		grVertexLayout(GR_PARAM_Z, offsetof(TLVertex,pos[2]), GR_PARAM_ENABLE);
	}
	//grVertexLayout(GR_PARAM_Z, offsetof(TLVertex,pos[2]), GR_PARAM_ENABLE);
	grVertexLayout(GR_PARAM_ST0, offsetof(TLVertex,t0), GR_PARAM_ENABLE);
	grVertexLayout(GR_PARAM_ST1, offsetof(TLVertex,t0), GR_PARAM_ENABLE);
	_formatSet=SingleTex;
	
	//grClipWindow(0,0,width,height);
	grDepthBufferMode(GR_DEPTHBUFFER_WBUFFER);
	DoSetDepthFunction(GR_CMP_LEQUAL);
	DoSetDepthMask(true);
	DoEnableAA(false);

	grCullMode(GR_CULL_POSITIVE);

	// note: grGammaCorrectionValue seem not to do anything
	//	grGammaCorrectionValue(1.7);
	
 	// set up simple texture mapping (one TMU)
	DoSetColorCombine(ColorGouraudTexture);
	grTexCombine
	(
		GR_TMU0,GR_COMBINE_FUNCTION_LOCAL,GR_COMBINE_FACTOR_NONE,
 		GR_COMBINE_FUNCTION_LOCAL,GR_COMBINE_FACTOR_NONE,
 		FXFALSE,FXFALSE
	);
	DoSetAlphaCombine(AlphaTexture);
	DoSetAlphaBlend(AlphaBlendNormal);

	grAlphaTestFunction(GR_CMP_GEQUAL);

	DoSetAlphaTestValue(_alphaTestValue);
	DoSetFogMode(FogEnable());


	//grConstantColorValue(0x40000000); // set constant color

	//grDitherMode(GR_DITHER_DISABLE);
	grDitherMode(GR_DITHER_2x2);
	//grDitherMode(GR_DITHER_4x4);
	
	grFogColorValue(PackColor(_fogColor));

	_tmuActive=-2; // default - no TMU active

	//grHints
	for( int tmu=0; tmu<_nTMUs; tmu++ )
	{
		grTexFilterMode(GR_TMU0+tmu,GR_TEXTUREFILTER_BILINEAR,GR_TEXTUREFILTER_BILINEAR);
		_lastBilin[tmu]=true;
		grTexMultibase(GR_TMU0+tmu,FXFALSE);
		grTexMipMapMode(GR_TMU0+tmu,GR_MIPMAP_NEAREST,FXFALSE);
		//grTexLodBiasValue(GR_TMU0+tmu,0.5);
		//grTexLodBiasValue(GR_TMU0+tmu,-1);
		grTexLodBiasValue(GR_TMU0+tmu,0);

	}

	guGammaCorrectionRGB(_gamma,_gamma,_gamma);

	grDepthBiasLevel(0);
	_currentBias=0;

	_frameTime=0;
	
	//Log("Glide engine constructed");
}

void EngineGlide::DeinitGlide()
{
	if (TextBank())
	{
		// release all GRAM textures
		for( int tmu=0; tmu<_textBank->NTMUs(); tmu++ )
		{
			_textBank->ReserveGRAM(tmu,INT_MAX,true);
		}
	}
	if (_context)
	{
		LogF("Close window - deactivate");
		grSstWinClose(_context);
		_context = NULL;
	}
}


enum {PointSpec=-2,LineSpec=-3,Line3DSpec=-4};

void EngineGlide::PrepareState( int specFlags )
{
	EnableAA(false);
	_lastSpec=specFlags;

	if( specFlags&NoZBuf )
	{
		SetDepthFunction(GR_CMP_ALWAYS);
		SetDepthMask(false);
	}
	else if( specFlags&NoZWrite )
	{
		if( specFlags&IsShadow )
		{
			// shadow
			SetDepthFunction(GR_CMP_LESS);
			SetDepthMask(true);
		}
		else
		{
			// road
			SetDepthFunction(GR_CMP_LEQUAL);
			SetDepthMask(false);
		}
	}
	else
	{
		SetDepthFunction(GR_CMP_LEQUAL);
		SetDepthMask(true);
	}
	#define MIN_ALPHA_DRAW 16
	#define MIN_ALPHA_DRAW_LIGHT 1
	#define MIN_ALPHA_DRAW_CLOUDS 1
	if( specFlags&IsShadow )
	{
		// shadows - do not use fog, use alpha modulation instead
		SetAlphaCombine(AlphaGouraudTexture);

		SetFogMode(FogDisable());
		SetColorCombine(ColorGouraud);

		SetAlphaBlend(AlphaBlendNormal);
		//if( specFlags&IsTransparent )
		//{
		//	SetAlphaTestValue(192/4);
		/*
		}
		else
		{
		}
		*/
		SetAlphaTestValue(1);
		/*
		}
		else
		{
			SetAlphaTestValue(192/4);
			SetAlphaBlend(AlphaBlendNoBlack);
		}
		*/
	}
	else if( specFlags&IsWater )
	{
		//grConstantColorValue(0xdc000000); // quarter-transparent water
		//SetAlphaCombine(AlphaTransparentTexture);
		SetAlphaCombine(AlphaGouraudTexture);
		SetColorCombine(ColorGouraudTexture);
		SetAlphaBlend(AlphaBlendNo);
		//SetAlphaBlend(AlphaBlendNormal);
		SetFogMode(FogEnable());
		SetAlphaTestValue(MIN_ALPHA_DRAW);
	}
	else if( specFlags&IsLight )
	{
		SetAlphaCombine(AlphaGouraudTexture);
		SetFogMode(FogDisable());
		SetAlphaTestValue(MIN_ALPHA_DRAW_LIGHT);
		SetColorCombine(ColorGouraudTexture);
		SetAlphaBlend(AlphaBlendAdd);
	}
	else if( specFlags&IsAlphaFog )
	{ // clouds or other transparent and distant objects
		SetFogMode(FogDisable());
		SetAlphaCombine(AlphaGouraudTexture);
		SetColorCombine(ColorGouraudTexture);
		SetAlphaTestValue(MIN_ALPHA_DRAW_CLOUDS);
		SetAlphaBlend(AlphaBlendNormal);
	}
	else
	{
		// normal fog
		SetColorCombine(ColorGouraudTexture);
		if( specFlags&NoZWrite ) SetAlphaTestValue(MIN_ALPHA_DRAW_CLOUDS);
		else if( specFlags&IsAlpha ) SetAlphaTestValue(MIN_ALPHA_DRAW);
		else SetAlphaTestValue(192);
		SetAlphaCombine(AlphaTexture);
		SetFogMode(FogEnable());
		if( specFlags&IsAlpha )
		{
			SetAlphaBlend(AlphaBlendNormal);
		}
		else
		{
			SetAlphaBlend(AlphaBlendNo);
		}
	}
}

void EngineGlide::DownloadMipmap( TextureGlide *texture, int level )
{
	// different texture or only partial texture loaded in last access
	// find best mipmap available
	if( texture )
	{
		int useLevel=texture->_gramLevel;
		Assert( useLevel<texture->NMipmaps() );
		const MipmapLevelGlideInfo *useMip=&texture->_mipmapsInfo[useLevel];
		if( _lastMipmap!=useMip )
		{
			Assert( texture->_isGRAM );
			GRAMOffset baseAddr=texture->_isGRAM->Memory();
			ADD_COUNTER(tChng,1);
			#if _ENABLE_PERFLOG
			if (LogStateSwitches)
			{
				TexStateLog << "\nTexture " << texture->Name() << " - " << useLevel << " ";
			}
			#endif
			grTexSource
			(
				texture->_tmu,baseAddr,GR_MIPMAPLEVELMASK_BOTH,
				const_cast<GrTexInfo *>(&useMip->_info)
			);
			_lastMipmap=useMip;
			Assert( !texture->_isDetail );
			if( !texture->_useDetail )
			{
				ActivateTMU(texture->_tmu);
			}
			else
			{
				Assert( texture->_tmu==0 );
				//ActivateTMU(texture->_tmu);
				//ActivateDetailTexturing();
			}
		}
	}
	else
	{
		#if _ENABLE_PERFLOG
		if (LogStateSwitches)
		{
			TexStateLog << "\nNo texture ";
		}
		#endif
		_lastMipmap=NULL;
		ActivateTMU(-1);
	}
	
	_lastTexture=texture;
	_lastLevel=level;
}

#define USE_AA 1

void EngineGlide::PrepareTriangle
(
	const MipInfo &absMip, int specFlags
)
{
	TextureGlide *texture=static_cast<TextureGlide *>(absMip._texture);
	int level=absMip._level;
	// set render state

	VFormatSet format=SingleTex;
	if( texture )
	{
		// set clamping flags
		Assert( (specFlags&(NoClamp|ClampU|ClampV))!=0 );
		// all triangles are marked for clamping
		bool clampU=(specFlags&ClampU)!=0;
		bool clampV=(specFlags&ClampV)!=0;
		PrepareClamping(texture,level,clampU,clampV);

		{
			int tmu=texture->_tmu;
			bool bilin=(specFlags&IsShadow)==0;
			if( bilin!=_lastBilin[tmu] )
			{
				int mode=bilin ? GR_TEXTUREFILTER_BILINEAR : GR_TEXTUREFILTER_POINT_SAMPLED;
				grTexFilterMode(GR_TMU0+tmu,mode,mode);
				_lastBilin[tmu]=bilin;
			}
		}
		// only some spec modes are relevant for triangle drawing
		if( specFlags&(DetailTexture|SpecularTexture) )
		{
			if( specFlags&DetailTexture) format=DetailTex;
			else if( specFlags&SpecularTexture) format=SpecularTex;
		}
	}
	SetMultiTexturing(format);

	specFlags&=
	(
		NoZBuf|NoZWrite|
		IsShadow|IsAlpha|IsWater|IsLight|IsOnSurface|OnSurface|
		IsAlphaFog|PointSampling
	);
	
	// prepare status accordingly to specFlags
	if( _lastSpec!=specFlags ) PrepareState(specFlags);
	if( _lastTexture==texture && _lastLevel<=level ) return;
	DownloadMipmap(texture,level);
}


static void CheckedDrawVertexArray
(
	int type, int n, const TLVertex *vp[],
	int w, int h
)
{
	for (int i=0; i<n; i++)
	{
		const TLVertex *v=vp[i];
		if ( v->pos.X()<-128 || v->pos.X()>w+128 )
		{
			LogF("Glide: grDrawVertexArray bad v->pos.X() %g",v->pos.X());
			return;
		}
		if ( v->pos.Y()<-128 || v->pos.Y()>w+128 )
		{
			LogF("Glide: grDrawVertexArray bad v->pos.Y() %g",v->pos.Y());
			return;
		}
	}
	grDrawVertexArray(type,n,vp);
}

static void CheckedDrawLine
(
	const TLVertex *a, const TLVertex *b, int w, int h
)
{
	if ( a->pos.X()<-128 || a->pos.X()>w+128 )
	{
		LogF("Glide: grDrawLine bad a->pos.X() %g",a->pos.X());
		return;
	}
	if ( b->pos.Y()<-128 && b->pos.Y()>w+128 )
	{
		LogF("Glide: grDrawLine bad b->pos.Y() %g",b->pos.Y());
		return;
	}
	grDrawLine(a,b);
}

#define CHECK_GLIDE 1

#if CHECK_GLIDE
	#define grDrawVertexArray(a,b,c) CheckedDrawVertexArray(a,b,c,_w,_h)
	#define grDrawLine(a,b) CheckedDrawLine(a,b,_w,_h)	
#endif

void EngineGlide::PrepareClamping
(
	TextureGlide *texture, int level, bool clampU, bool clampV
)
{
	int tmu=texture->_tmu;
	if( _lastClampU[tmu]!=clampU || _lastClampV[tmu]!=clampV )
	{
		grTexClampMode(tmu,clampU,clampV);
		_lastClampU[tmu]=clampU,_lastClampV[tmu]=clampV;
	}
}	

void EngineGlide::DrawSection
(
	const FaceArray &face, Offset beg, Offset end
)
{
	for( Offset i=beg; i<end; face.Next(i) )
	{
		const Poly &f=face[i];
		EngineGlide::DrawPolygon(f.GetVertexList(),f.N());
	}
}

void EngineGlide::DrawPolygon( const VertexIndex *ii, int n )
{
	const TLVertex *vp[MaxPoly];

	int i;
	//LogF("Poly");
	for( i=0; i<n; i++ )
	{
		int index=ii[i];
		const TLVertex *v=&_mesh->GetVertex(index);
		vp[n-1-i]=v;
	}
	
	grDrawVertexArray(GR_TRIANGLE_FAN,n,vp);
	ADD_COUNTER(tris,n-2);
	#if _ENABLE_PERFLOG
	if (LogTexSwitches) for (int c=n-2; --c>=0; ) TexStateLog << ".";
	#endif
}

int EngineGlide::HowLongIdle()
{
	#if 0
		FxI32 getVal[2];
		Verify( grGet(GR_FIFO_FULLNESS,sizeof(getVal),getVal)==sizeof(getVal) );
		// The value is returned as 1.24 fixed point
		int fifoFull1p24=getVal[0];
		//LogF("FIFO: %d %.3f",fifoFull1p24,(float)fifoFull1p24/(1<<24));
		// note: it seems there is some bug in Glide3
	#endif
	return 1;
}

void EngineGlide::DrawDecal
(
	Vector3Par screen, float rhw, float sizeX, float sizeY, PackedColor color,
	const MipInfo &mip, int specFlags

)
{
	//int aRatio=_lastMipmap ? _lastMipmap->_info.aspectRatioLog2 : NULL;

	float vx,vy;
	vx=screen.X();
	vy=screen.Y();

	float coef = GScene->GetCamera()->ClipNear();

	float oow = rhw*coef;
	saturate(oow,1.0f/65535,1.0f);
	float oow256=oow*256;

	// perform simple clipping
	float xBeg=vx-sizeX;
	float xEnd=vx+sizeX;
	float yBeg=vy-sizeY;
	float yEnd=vy+sizeY;

	float uBeg=0;
	float vBeg=0;
	float uEnd=oow256;
	float vEnd=oow256;

	if( xBeg<0 )
	{
		// -xBeg is out, side length is 2*sizeX
		uBeg=oow256*-xBeg/(2*sizeX);
		xBeg=0;
	}
	if( xEnd>_w )
	{
		// xEnd-_w is out, side length is 2*sizeX
		uEnd=oow256-oow256*(xEnd-_w)/(2*sizeX);
		xEnd=_w;
	}
	if( yBeg<0 )
	{
		// -yBeg is out, side length is 2*sizeY
		vBeg=oow256*-yBeg/(2*sizeY);
		yBeg=0;
	}
	if( yEnd>_h )
	{
		// yEnd-_h is out, side length is 2*sizeY
		vEnd=oow256-oow256*(yEnd-_h)/(2*sizeY);
		yEnd=_h;
	}
	
	if( xBeg>=xEnd || yBeg>=yEnd ) return;

	TLVertex v[4];
	// set vertex 0
	v[0].pos[0]=xBeg;
	v[0].pos[1]=yBeg;
	v[0].t0.u=uBeg;
	v[0].t0.v=vBeg;
	// set vertex 1
	v[1].pos[0]=xEnd;
	v[1].pos[1]=yBeg;
	v[1].t0.u=uEnd;
	v[1].t0.v=vBeg;
	// set vertex 2
	v[2].pos[0]=xEnd;
	v[2].pos[1]=yEnd;
	v[2].t0.u=uEnd;
	v[2].t0.v=vEnd;
	// set vertex 3
	v[3].pos[0]=xBeg;
	v[3].pos[1]=yEnd;
	v[3].t0.u=uBeg;
	v[3].t0.v=vEnd;
	if( specFlags&IsAlphaFog )
	{
		v[0].color=color;
		v[0].pos[2]=0;
	}
	else
	{
		// use fog with z
		v[0].pos[2]=color.A8()*(65535.0/255);
		v[0].color=PackedColor(color|0xff000000);
	}
	int i;
	for( i=0; i<4; i++ )
	{
		v[i].rhw=oow;
		v[i].color=v[0].color;
		v[i].pos[2]=v[0].pos[2];
	}

	// use lighting result
	PrepareTriangle(mip,specFlags);

	const TLVertex *vp[4];
	vp[0]=&v[0];
	vp[1]=&v[3];
	vp[2]=&v[1];
	vp[3]=&v[2];
	grDrawVertexArray(GR_TRIANGLE_STRIP,4,vp);
	ADD_COUNTER(tris,2);
	#if _ENABLE_PERFLOG
	if (LogTexSwitches) TexStateLog << "DD";
	#endif
}

void EngineGlide::DrawPoints( int beg, int end )
{
	// this function is used for star rendering
	// we assume no z-buffering is used
	if( _lastSpec!=PointSpec )
	{
		_lastSpec=PointSpec;

		// disable z-buffer
		SetDepthFunction(GR_CMP_LEQUAL);
		SetDepthMask(false);
		// prepare alpha unit for AA points
		SetFogMode(FogDisable());
		grConstantColorValue(0xffffffff); // set constant color - why??
		SetColorCombine(ColorGouraud);
		SetAlphaCombine(AlphaGouraud);
		SetAlphaTestValue(1);
		// back to front antialiasing
		SetAlphaBlend(AlphaBlendNormal);
		EnableAA(true);
	}

	for( int i=0; i<_mesh->NVertex(); i++ )
	{
		if( _mesh->Clip(i)&ClipAll ) continue;
		PackedColor color=_mesh->GetColor(i);
		if( color.A8()<8 ) continue; // do not draw stars that are not visible

		const TLVertex *v=&_mesh->GetVertex(i);
		grDrawPoint(v);
		#if _ENABLE_PERFLOG
		if (LogTexSwitches) TexStateLog << "P";
		#endif
	}
}

void EngineGlide::DrawLine( int beg, int end )
{

	// this function is used for star rendering
	// we assume no z-buffering is used
	if( _lastSpec!=Line3DSpec )
	{
		_lastSpec=Line3DSpec;

		// disable z-buffer
		SetDepthFunction(GR_CMP_LEQUAL);
		SetDepthMask(false);
		// prepare alpha unit for AA points
		SetFogMode(FogDisable());
		// disable texturing
		SetColorCombine(ColorGouraud);
		SetAlphaCombine(AlphaGouraud);
		SetAlphaTestValue(1);
		// back to front antialiasing
		SetAlphaBlend(AlphaBlendNormal);
		EnableAA(true);
	}

	grDrawLine(&_mesh->GetVertex(beg),&_mesh->GetVertex(end));
	#if _ENABLE_PERFLOG
	if (LogTexSwitches) TexStateLog << "L";
	#endif
}

void EngineGlide::Draw2D
(
	const Draw2DPars &pars, const Rect2DAbs &rect, const Rect2DAbs &clip
)
{
	Assert( pars.mip.IsOK() );
	if( !pars.mip.IsOK() ) return;

	TextureGlide *texture=static_cast<TextureGlide *>(pars.mip._texture);
	int level=pars.mip._level;

	// perform simple clipping
	float xBeg=rect.x;
	float xEnd=rect.x+rect.w;
	float yBeg=rect.y;
	float yEnd=rect.y+rect.h;

	float uBeg=0;
	float vBeg=0;
	float uEnd=1;
	float vEnd=1;

	float xec=floatMin(clip.x+clip.w,_w);
	float yec=floatMin(clip.y+clip.h,_h);

	float xc=floatMax(clip.x,0);
	float yc=floatMax(clip.y,0);

	if( xBeg<xc )
	{
		// -xBeg is out, side length is 2*sizeX
		uBeg=(xc-xBeg)/rect.w;
		xBeg=xc;
	}
	if( xEnd>xec )
	{
		// xEnd-_w is out, side length is 2*sizeX
		uEnd=1-(xEnd-xec)/rect.w;
		xEnd=xec;
	}
	if( yBeg<yc )
	{
		// -yBeg is out, side length is 2*sizeY
		vBeg=(yc-yBeg)/rect.h;
		yBeg=yc;
	}
	if( yEnd>yec )
	{
		// yEnd-_h is out, side length is 2*sizeY
		vEnd=1-(yEnd-yec)/rect.h;
		yEnd=yec;
	}
	
	if( xBeg>=xEnd || yBeg>=yEnd ) return;

	PrepareTriangle(pars.mip,pars.spec);

	TLVertex pos[4];
	pos[0].color=pars.colorTL;
	pos[1].color=pars.colorTR;
	pos[2].color=pars.colorBR;
	pos[3].color=pars.colorBL;
	pos[0].rhw=pos[1].rhw=pos[2].rhw=pos[3].rhw=1;
	Assert( xBeg>=0 );
	Assert( yBeg>=0 );
	Assert( xEnd<=_w );
	Assert( yEnd<=_h );
	pos[0].pos[0]=xBeg,pos[0].pos[1]=yBeg;
	pos[1].pos[0]=xEnd,pos[1].pos[1]=yBeg;
	pos[2].pos[0]=xEnd,pos[2].pos[1]=yEnd;
	pos[3].pos[0]=xBeg,pos[3].pos[1]=yEnd;

	if( texture )
	{

		float uTL=pars.uTL+uBeg*(pars.uTR-pars.uTL)+vBeg*(pars.uBL-pars.uTL);
		float uTR=pars.uTL+uEnd*(pars.uTR-pars.uTL)+vBeg*(pars.uBL-pars.uTL);
		float uBL=pars.uTL+uBeg*(pars.uTR-pars.uTL)+vEnd*(pars.uBL-pars.uTL);
		float uBR=pars.uTL+uEnd*(pars.uTR-pars.uTL)+vEnd*(pars.uBL-pars.uTL);

		float vTL=pars.vTL+uBeg*(pars.vTR-pars.vTL)+vBeg*(pars.vBL-pars.vTL);
		float vTR=pars.vTL+uEnd*(pars.vTR-pars.vTL)+vBeg*(pars.vBL-pars.vTL);
		float vBL=pars.vTL+uBeg*(pars.vTR-pars.vTL)+vEnd*(pars.vBL-pars.vTL);
		float vBR=pars.vTL+uEnd*(pars.vTR-pars.vTL)+vEnd*(pars.vBL-pars.vTL);
		
		float u,v;
		const MipmapLevelGlideInfo *useMip=&texture->_mipmapsInfo[level];
		int aRatio=useMip->_info.aspectRatioLog2;
		if( aRatio>0 ) u=256,v=256>>aRatio;
		else v=256,u=256>>-aRatio;
		uTL*=u,uTR*=u,uBL*=u,uBR*=u;
		vTL*=v,vTR*=v,vBL*=v,vBR*=v;

		pos[0].t0.u=uTL,pos[0].t0.v=vTL;
		pos[1].t0.u=uTR,pos[1].t0.v=vTR;
		pos[2].t0.u=uBR,pos[2].t0.v=vBR;
		pos[3].t0.u=uBL,pos[3].t0.v=vBL;

		pos[0].t1.u=uTL*64,pos[0].t1.v=vTL*64;
		pos[1].t1.u=uTR*64,pos[1].t1.v=vTR*64;
		pos[2].t1.u=uBR*64,pos[2].t1.v=vBR*64;
		pos[3].t1.u=uBL*64,pos[3].t1.v=vBL*64;
		
	}
	else
	{
		pos[0].t0.u=0,pos[0].t0.v=0;
		pos[1].t0.u=0,pos[1].t0.v=0;
		pos[2].t0.u=0,pos[2].t0.v=0;
		pos[3].t0.u=0,pos[3].t0.v=0;

		pos[0].t1.u=0,pos[0].t1.v=0;
		pos[1].t1.u=0,pos[1].t1.v=0;
		pos[2].t1.u=0,pos[2].t1.v=0;
		pos[3].t1.u=0,pos[3].t1.v=0;
	}

	const TLVertex *vp[4];
	vp[0]=&pos[0];
	vp[1]=&pos[3];
	vp[2]=&pos[1];
	vp[3]=&pos[2];
	grDrawVertexArray(GR_TRIANGLE_STRIP,4,vp);
	ADD_COUNTER(tris,2);
	#if _ENABLE_PERFLOG
	if (LogTexSwitches) TexStateLog << "##";
	#endif
}

void EngineGlide::DrawLine
(
	const Line2DAbs &line,
	PackedColor c0, PackedColor c1,
	const Rect2DAbs &clip
)
{
	float x0 = line.beg.x;
	float y0 = line.beg.y;
	float x1 = line.end.x;
	float y1 = line.end.y;
	// we assume no z-buffering is used
	if( _lastSpec!=LineSpec )
	{
		_lastSpec=LineSpec;

		// disable z-buffer
		SetDepthFunction(GR_CMP_ALWAYS);
		SetDepthMask(false);
		// prepare alpha unit for AA points
		SetFogMode(FogDisable());
		// disable texturing
		SetColorCombine(ColorGouraud);
		SetAlphaCombine(AlphaGouraud);
		SetAlphaTestValue(1);
		// back to front antialiasing
		SetAlphaBlend(AlphaBlendNormal);
		EnableAA(true);
	}
	// perform clipping
	// clip x axis
	bool swapped=false;
	if( x0>x1 )
	{
		swap(x1,x0),swap(y1,y0),swap(c1,c0);
		swapped=true;
	}
	if( x1<clip.x ) return;
	if( x0>clip.x+clip.w ) return;
	float dy=( fabs(x1-x0)>0 ? (y1-y0)/(x1-x0) : 0 );
	if( x0<clip.x )
	{
		y0+=(clip.x-x0)*dy;
		x0=clip.x;
	}
	if( x1>clip.x+clip.w )
	{
		y1-=(x1-(clip.x+clip.w))*dy;
		x1=clip.x+clip.w;
	}
	// clip y axis
	if( y0>y1 )
	{
		swap(x1,x0),swap(y1,y0),swap(c1,c0);
		swapped=!swapped;
	}
	if( y1<clip.y ) return;
	if( y0>clip.y+clip.h ) return;
	float dx=( fabs(y1-y0)>0 ? (x1-x0)/(y1-y0) : 0 );
	if( y0<clip.y )
	{
		x0+=(clip.y-y0)*dx;
		y0=clip.y;
	}
	if( y1>clip.y+clip.h )
	{
		x1-=(y1-(clip.y+clip.h))*dx;
		y1=clip.y+clip.h;
	}
	
	if( swapped )
	{
		swap(x1,x0),swap(y1,y0),swap(c1,c0);
	}
	TLVertex beg,end;
	beg.pos[0]=x0,beg.pos[1]=y0;
	end.pos[0]=x1,end.pos[1]=y1;
	beg.rhw=end.rhw=1;
	beg.color=c0,end.color=c1;
	beg.pos[2]=end.pos[2]=0;
	beg.t0.u=beg.t0.v=
	end.t0.u=end.t0.v=0;
	grDrawLine(&beg,&end);
	#if _ENABLE_PERFLOG
	if (LogTexSwitches) TexStateLog << "l";
	#endif
}


void EngineGlide::DrawPoly
(
	const MipInfo &mip, const Vertex2DPixel *vertices, int n,
	const Rect2DPixel &clipRect, int specFlags
)
{
	const int maxN=32;

	// reject poly if fully outside or invalid
	ClipFlags orClip=0;
	ClipFlags andClip=ClipAll;
	ClipFlags clipV[maxN];
	for( int i=0; i<n; i++ )
	{
		const Vertex2DPixel &vs=vertices[i];
		float x=vs.x;
		float y=vs.y;
		ClipFlags clip=0;
		if( x<clipRect.x ) clip|=ClipLeft;
		else if( x>clipRect.x+clipRect.w ) clip|=ClipRight;
		if( y<clipRect.y ) clip|=ClipTop;
		else if( y>clipRect.y+clipRect.h ) clip|=ClipBottom;
		clipV[i]=clip;
		orClip|=clip;
		andClip&=clip;
	}
	if( andClip ) return;
	Vertex2DPixel clippedVertices1[maxN]; // temporay buffer to keep clipped result
	Vertex2DPixel clippedVertices2[maxN]; // temporay buffer to keep clipped result
	if( orClip )
	{
		Vertex2DPixel *free=clippedVertices1;
		Vertex2DPixel *used=clippedVertices2;
		// perform clipping
		for( int i=0; i<n; i++ ) used[i]=vertices[i];
		if( orClip&ClipTop ) n=Clip2D(clipRect,free,used,n,InsideTopPixel),swap(free,used);
		if( orClip&ClipBottom ) n=Clip2D(clipRect,free,used,n,InsideBottomPixel),swap(free,used);
		if( orClip&ClipLeft ) n=Clip2D(clipRect,free,used,n,InsideLeftPixel),swap(free,used);
		if( orClip&ClipRight ) n=Clip2D(clipRect,free,used,n,InsideRightPixel),swap(free,used);
		// use result
		vertices=used;
	}

	// we assume no z-buffering is used
	PrepareTriangle(mip,specFlags);

	// TODO: 2D clipping (with clip)

	//int aRatio=_lastMipmap ? _lastMipmap->_info.aspectRatioLog2 : 0;

	if( n>maxN )
	{
		n=maxN;
		Fail("Poly: Too much vertices");
	}
	TLVertex gv[maxN];
	const TLVertex *vp[maxN];

	//LogF("Poly");
	float x2d = Left2D();
	float y2d = Top2D();

	for( int i=0; i<n; i++ )
	{
		TLVertex *v=&gv[i];
		const Vertex2DPixel &vs=vertices[i];
		float x=vs.x+x2d;
		float y=vs.y+y2d;

		vp[n-1-i]=v;
		//vp[i]=v;
		v->pos[0]=x;
		v->pos[1]=y;
		v->rhw=1;
		v->color=vs.color;
		v->pos[2]=0;
		v->t0.u=vs.u*256;
		v->t0.v=vs.v*256;
		// tmu1vtx?
	}


	grDrawVertexArray(GR_TRIANGLE_FAN,n,vp);
	ADD_COUNTER(tris,n-2);
	#if _ENABLE_PERFLOG
	if (LogTexSwitches) for (int c=n-2; --c>=0; ) TexStateLog << "#";
	#endif
}

void EngineGlide::DrawPoly
(
	const MipInfo &mip, const Vertex2DAbs *vertices, int n,
	const Rect2DAbs &clipRect, int specFlags
)
{
	const int maxN=32;

	// reject poly if fully outside or invalid
	ClipFlags orClip=0;
	ClipFlags andClip=ClipAll;
	ClipFlags clipV[maxN];
	for( int i=0; i<n; i++ )
	{
		const Vertex2DAbs &vs=vertices[i];
		float x=vs.x;
		float y=vs.y;
		ClipFlags clip=0;
		if( x<clipRect.x ) clip|=ClipLeft;
		else if( x>clipRect.x+clipRect.w ) clip|=ClipRight;
		if( y<clipRect.y ) clip|=ClipTop;
		else if( y>clipRect.y+clipRect.h ) clip|=ClipBottom;
		clipV[i]=clip;
		orClip|=clip;
		andClip&=clip;
	}
	if( andClip ) return;
	Vertex2DAbs clippedVertices1[maxN]; // temporay buffer to keep clipped result
	Vertex2DAbs clippedVertices2[maxN]; // temporay buffer to keep clipped result
	if( orClip )
	{
		Vertex2DAbs *free=clippedVertices1;
		Vertex2DAbs *used=clippedVertices2;
		// perform clipping
		for( int i=0; i<n; i++ ) used[i]=vertices[i];
		if( orClip&ClipTop ) n=Clip2D(clipRect,free,used,n,InsideTopAbs),swap(free,used);
		if( orClip&ClipBottom ) n=Clip2D(clipRect,free,used,n,InsideBottomAbs),swap(free,used);
		if( orClip&ClipLeft ) n=Clip2D(clipRect,free,used,n,InsideLeftAbs),swap(free,used);
		if( orClip&ClipRight ) n=Clip2D(clipRect,free,used,n,InsideRightAbs),swap(free,used);
		// use result
		vertices=used;
	}

	// we assume no z-buffering is used
	PrepareTriangle(mip,specFlags);

	// TODO: 2D clipping (with clip)

	//int aRatio=_lastMipmap ? _lastMipmap->_info.aspectRatioLog2 : 0;

	if( n>maxN )
	{
		n=maxN;
		Fail("Poly: Too much vertices");
	}
	TLVertex gv[maxN];
	const TLVertex *vp[maxN];

	//LogF("Poly");

	for( int i=0; i<n; i++ )
	{
		TLVertex *v=&gv[i];
		const Vertex2DAbs &vs=vertices[i];
		float x=vs.x;
		float y=vs.y;

		vp[n-1-i]=v;
		//vp[i]=v;
		v->pos[0]=x;
		v->pos[1]=y;
		v->rhw=1;
		v->color=vs.color;
		v->pos[2]=0;
		v->t0.u=vs.u*256;
		v->t0.v=vs.v*256;
		// tmu1vtx?
	}


	grDrawVertexArray(GR_TRIANGLE_FAN,n,vp);
	ADD_COUNTER(tris,n-2);
	#if _ENABLE_PERFLOG
	if (LogTexSwitches) for (int c=n-2; --c>=0; ) TexStateLog << "#";
	#endif
}

// functions usefull for TextBankGlide

struct DXTBlock64
{
	// see "Compressed Texture Formats" in DX Docs
	word c0,c1; // color data
	word tex0,tex1; // texel data
};

// note: int. division very slow - precalc. table instead

//#define DIV_3(x) ( ((x)+1)/3 )
#define DIV_3(x) ( (x)/3 )

static char Div3[128];

static struct Div3Init
{
	Div3Init()
	{
		for (int i=64; i<128; i++)
		{
			Div3[i]=(i-64)/3;
		}
		for (int i=0; i<64; i++)
		{
			Div3[i]=-(-(i-64)/3);
		}
	}
} dummy;

inline int Convert565To1555( int x )
{
	return 0x8000|(x&0x1f)|((x>>1)&0x7fe0);
}

void EngineGlide::CopyMipmap( TextureGlide *texture, int tgtLevel, int srcLevel )
{
	PacLevelMem *src=&texture->_mipmaps[srcLevel];
	MipmapLevelGlideInfo *dst=&texture->_mipmapsInfo[tgtLevel];
	// copy mipmap level into 3DHW memory
	//Assert( src->_level<=dst->_level );
	Assert( texture->_memory ); // mipmap level source
	Assert( texture->_isGRAM ); // mipmap level destination
		
	GRAMOffset baseAddr=texture->_isGRAM->Memory();
	// calculate dst offset in src
	GrTexInfo info=dst->_info;
	//GrTexInfo info=texture->_texInfo;
	Assert( texture->_memory );
	Assert( texture->_memory->Memory() );
	info.data=texture->_memory->Memory();
	int w = src->_w, h = src->_h;
	for( int level=srcLevel; level<tgtLevel; level++ )
	{
		info.data=(char *)info.data+texture->Mipmap(level)->Size();
		w>>=1, h>>=1;
	}
	// src level may be compressed
	// need decompression - now
	// compressed source at info.data

	if (src->DstFormat()==PacDXT1)
	{
		//Assert(dst->_format==PacARGB1555);
		Assert(info.format==GR_TEXFMT_ARGB_1555);

		int levelW = w;
		int levelH = h;
		int pixelSize = 0;
		while (levelW>=4 && levelH>=4)
		{
			pixelSize += levelW*levelH;
			levelW>>=1;
			levelH>>=1;
		}

		// make space for all mipmaps
		AUTO_STATIC_ARRAY(word,pixels,256*(256+128));
		pixels.Resize(pixelSize);

		//memset(pixels.Data(),-1,pixelSize*2);

		// scan 64b packets
		DXTBlock64 *s = (DXTBlock64 *)info.data;

		word *line = pixels.Data();
		// convert all levels
		for (int level=tgtLevel; level<texture->_nMipmaps; level++)
		{
			Assert(w>=4);
			Assert(h>=4);

			PacLevelMem::DecompressDXT1(line,s,w,h);
			// skip source data
			s += (w*h)>>4;
			// skip destination data
			line += w*h;

			// process smaller mipmaps
			w >>= 1, h >>= 1;
		}

		info.data = pixels.Data();
		grTexDownloadMipMap
		(
			texture->_tmu,baseAddr,GR_MIPMAPLEVELMASK_BOTH,&info
		);

	}
	else
	{
		grTexDownloadMipMap
		(
			texture->_tmu,baseAddr,GR_MIPMAPLEVELMASK_BOTH,&info
		);
	}
}

// 2D bitmap drawing

extern bool HideCursor;

#include "engineDll.hpp"

Engine *CreateEngineGlide(CreateEnginePars &pars )
{
	if( EngineGlide::Detect() )
	{
		pars.NoRedrawWindow = false;
		pars.HideCursor = false;
		//pars.HideCursor=true;
		//ShowCursor(FALSE);
		return new EngineGlide
		(
			pars.hInst,pars.hPrev,pars.sw,pars.w,pars.h,
			!pars.UseWindow
		);
	}
	else
	{
		ErrorMessage("3Dfx not detected");
		return NULL;
	}
}


#endif //!_DISABLE_GUI
