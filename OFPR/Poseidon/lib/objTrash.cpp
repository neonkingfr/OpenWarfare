// Poseidon - object destruction
// (C) 1998, SUMA

#include "wpch.hpp"

#include "object.hpp"
//#include "scene.hpp"
#include <El/Common/randomGen.hpp>
#include "landscape.hpp"
//#include "tlVertex.hpp"

template Ref<ConvexComponent>;

/*!
\patch_internal 1.53 Date 4/27/2002 by Ondra
- Fixed: Deformation alghoritm was inconsitent when LOD levels had different min. Y.
*/

void Shape::MakeDestroyed( float yOffset, Vector3Par bCenter, int seed, float coef )
{
	// start: triangulate
	// all points should be held on ground level
	//float yOffset=-Min().Y();
	int i;
	for( i=0; i<NPos(); i++ )
	{
		V3 &pos=SetPos(i);
		float yFactor=pos[1]+yOffset; // old Y coordinate
		saturateMax(yFactor,0.1);
		int iSeed=toIntFloor(pos[0]+pos[1]+pos[2])&0x3ff+seed;
		// randomize height above ground level
		pos[1]=
		(
			(1-GRandGen.RandomValue(iSeed)*0.2*coef)*yFactor
			+(GRandGen.RandomValue(iSeed+1)-0.5)*0.1*coef
			-yOffset
		);
		// randomize position
		pos[0]+=(GRandGen.RandomValue(iSeed+2)-0.5)*yFactor*0.25*coef;
		pos[2]+=(GRandGen.RandomValue(iSeed+3)-0.5)*yFactor*0.55*coef;
		ClipFlags clip=Clip(i);
		clip&=~ClipLandMask;
		clip|=ClipLandKeep;
		SetClip(i,clip);
	}
	CalculateHints();
	ClearOriginalPos();
	// normal recalculation necessary
	RecalculateNormals(true);
}

void Shape::MakeTreeDestroyed( float yOffset, Vector3Par bCenter, int seed, float coef )
{
	// start: triangulate
	// all points should be held on ground level
	//float yOffset=bCenter.Y();
	//float yOffset=-Min().Y();
	int i;
	for( i=0; i<NPos(); i++ )
	{
		V3 &pos=SetPos(i);
		float yFactor=pos[1]+yOffset; // old Y coordinate
		saturateMax(yFactor,0.1);
		int iSeed=toIntFloor(pos[0]+pos[1]+pos[2])&0x3ff+seed;
		// randomize height above ground level
		pos[1]=
		(
			(GRandGen.RandomValue(iSeed)*0.1+0.05)*yFactor
			+(GRandGen.RandomValue(iSeed+1)-0.5)*0.05*coef
			-yOffset
		);
		// randomize position
		pos[0]+=(GRandGen.RandomValue(iSeed+2)-0.5)*yFactor*0.25*coef;
		pos[2]+=(GRandGen.RandomValue(iSeed+3)-0.5)*yFactor*0.55*coef;
		pos[0]+=(1-GRandGen.RandomValue(iSeed+4)*0.2*coef)*yFactor;
		ClipFlags clip=Clip(i);
		clip&=~ClipLandMask;
		clip|=ClipLandKeep;
		SetClip(i,clip);
	}
	CalculateHints();
	ClearOriginalPos();
	// normal recalculation necessary
	RecalculateNormals(true);
}

LODShape *LODShapeWithShadow::MakeDestroyed( float coef )
{
	if( !_destroyed )
	{
		_destroyed=new LODShape(*this);
		if (NLevels()>0)
		{
			int seed=toIntFloor(GRandGen.RandomValue()*2048);
			float yOffset=-Level(0)->Min().Y();
			for( int level=0; level<NLevels(); level++ )
			{
				Shape *shape=_destroyed->Level(level);
				if( shape )
				{
					shape->MakeDestroyed(yOffset,_destroyed->BoundingCenter(),seed,coef);
					#if USE_QUADS
					if (shape->PosQuad().Size()>=0)
					{
						// recalculate Quad array
						shape->ConvertToQArray();
					}
					#endif
					//shape->FindSections();
					//shape->ConvertToVBuffer(false);
				}
			}
		}
		//_destroyed->ShadowChanged();
		_destroyed->OrSpecial(NoShadow);
		_destroyed->LockAutoCenter(true);
		_destroyed->CalculateMinMax(true);
		// _destroyed convex components not used
		_destroyed->_geomComponents->Clear();
	}
	return _destroyed;
}

LODShape *LODShapeWithShadow::MakeTreeDestroyed( float coef )
{
	if( !_destroyed )
	{
		_destroyed=new LODShape(*this);
		if (NLevels()>0)
		{
			int seed=toIntFloor(GRandGen.RandomValue()*2048);
			float yOffset=-Level(0)->Min().Y();
			for( int level=0; level<NLevels(); level++ )
			{
				Shape *shape=_destroyed->Level(level);
				if( shape )
				{
					shape->MakeTreeDestroyed(yOffset,_destroyed->BoundingCenter(),seed,coef);
					#if USE_QUADS
					if (shape->PosQuad().Size()>=0)
					{
						// recalculate Quad array
						shape->ConvertToQArray();
					}
					#endif
				}
			}
		}
		//_destroyed->ShadowChanged();
		// leave bounding center as it is

		_destroyed->OrSpecial(NoShadow);
		_destroyed->LockAutoCenter(true);
		_destroyed->CalculateMinMax(true);
		// object disappeared for collisions
		_destroyed->_geomComponents->Clear();
	}
	return _destroyed;
}
