#ifdef _MSC_VER
#pragma once
#endif

#ifndef __ENGDD_HPP
#define __ENGDD_HPP

#if !_DISABLE_GUI

// implementation of DirectDraw engine
// without actual triangle drawing
// so that different implementations (including Direct3D) are possible

#include "d3ddefs.hpp"

#include <Es/Containers/array.hpp>
#include <Es/Containers/staticArray.hpp>
#include "types.hpp"
#include "engine.hpp"
#include "tlVertex.hpp"

//typedef DWORD Pixel32;
//typedef DWORD PixelGen;

//typedef unsigned short ZFyzType; // used for storing z-buffer information
//#define zToFyz(z) toInt(z*0xc000) // nearly 16b precision on 1.0
//#define MAX_FYZ_Z 0xbfff
typedef int ZFyzType; // used for storing z-buffer information
#define zToFyz(z) toInt(z*0x10000000) // 28b precision on 1.0
#define MAX_FYZ_Z 0x7fffffff

void DDError( const char *text, int err );

enum TexLoc {TexLocalVidMem,TexNonlocalVidMem,TexSysMem};

enum
{
	MeshBufferLength=32*1024,
	IndexBufferLength=4*1024
};

/*
#if _SUPER_RELEASE
#define PIXEL_SHADERS 0
#else
#define PIXEL_SHADERS 1
#endif
*/
#ifndef _XBOX
	#define PIXEL_SHADERS 1
#else
	#define PIXEL_SHADERS 0
#endif

#if _SUPER_RELEASE

#define VERTEX_SHADERS 0

#elif _RELEASE

#define VERTEX_SHADERS 0

#endif

#if PIXEL_SHADERS

enum PixelShaderMode
{
	PSMDay,
	PSMNight,
	NPixelShaderModes
};

enum PixelShaderID
{
	PSNormal, // diffuse color modulate, alpha replicate
	PSDetail, // detail texturing
	PSGrass,  // grass texturing
	PSWater,  // water bumpmapping
	NPixelShaders,
	PSNone=NPixelShaders
};

enum PixelShaderSpecular
{
	PSSSpecular,
	PSSNormal,
	NPixelShaderSpecular
};

#endif

#if VERTEX_SHADERS

enum VertexShader
{
	VSNormal,
	VSDayGrass, // grass with one directional light
	NVertexShaders
};

#endif

// several queues for different textures
// other states must be same

struct TriQueue
{
	//int _queueBase,_queueSize; // _triangleQueue position in VB

	StaticArray<WORD> _triangleQueue; // see BeginMesh,EndMesh

	TextureD3D *_texture; // which texture
	int _level; // which level
	int _special; // drawing flags
	int _lastUsed; // used counter
};


enum
{
	MaxTriQueues=32, // 16 - 32 seems to be reasonable
	TriQueueSize=2048
};

struct Queue
{
	ComRef<IDirect3DVertexBuffer8> _meshBuffer;
	ComRef<IDirect3DIndexBuffer8> _indexBuffer;
	int _vertexBufferUsed; // how many vertices are in vertex buffer
	int _indexBufferUsed; // how many indices are is index buffer

	int _meshBase,_meshSize; // _mesh position in VB

	TriQueue _tri[MaxTriQueues];
	bool _triUsed[MaxTriQueues];
	// queue 0 is used only for alpha rendering
	int _actTri; // queue assigned by PrepareTriangle call

	int _usedCounter; // used for LRU tracking
	bool _firstVertex; // first call in scene should always use DISCARD
	bool _firstIndex;

	Queue();
	int Allocate
	(
		TextureD3D *tex, int level, int spec,
		int minI, int maxI, int tip
	);
	void Free( int i );
};

struct SVertex
{
	Vector3P pos;
	Vector3P norm;
	UVPair t0;
	//UVPair t1;
};


class VertexDynamicData
{
	friend class EngineDD;

	ComRef<IDirect3DVertexBuffer8> _vBuffer;
	int _vBufferSize; // reserved sizes of resources
	int _vBufferUsed; // used sizes

	public:
	VertexDynamicData();

	void Init(int size, IDirect3DDevice8 *dev);
	void Dealloc();

	int AddVertices(const Shape &src);
};

class VertexStaticData: public RefCountWithLinks
{
	friend class EngineDD;

	private:

	ComRef<IDirect3DVertexBuffer8> _vBuffer;
	ComRef<IDirect3DIndexBuffer8> _iBuffer;

	int _vBufferSize; // reserved sizes of resources
	int _iBufferSize;

	int _vBufferUsed; // used sizes
	int _iBufferUsed;

	int _nShapes;

	public:
	VertexStaticData();
	~VertexStaticData();

	void Init(int vertices, int indices, IDirect3DDevice8 *dev);
	void Dealloc();

	bool AddShape(const Shape &src, int &vIndex, int &iIndex, bool dynamic);
	void RemoveShape(const Shape &src);
};

class VertexBufferD3D;

//! D3D engine implementation
/*!
both HW and SW T&L version are implemented
*/

class EngineDD: public Engine
{
	typedef Engine base;

	enum TexGenMode
	{
		TGFixed,
		TGNone,
		TGDetail,
		TGGrass,
		TGWater
	};
	
	protected:
	// data members
	//int _rLog,_gLog,_bLog; // shift ammount
	//int _rMax,_gMax,_bMax; // maximal value (bit mask before shifting)

	int _w,_h; // back buffer dimensions

	int _pixelSize; // 16 or 32 bit mode?
	int _depthBpp; // 16 or 32 bit mode?
	int _refreshRate;

	D3DPRESENT_PARAMETERS _pp;
	
	HWND _hwndApp;
	
	int _bias; // z-buffer bias
	float _matC, _matD; // w-buffer limits set
	float _grassParam[4];
	bool _clipANearEnabled,_clipAFarEnabled;
	Plane _clipANear, _clipAFar; // additonal clipping set

	int _minGuardX; // guard band clipping properties
	int _maxGuardX;
	int _minGuardY;
	int _maxGuardY;

	bool _useDXTL;
	bool _useHWTL;
	bool _useWBuffer;
	bool _userEnabledWBuffer;
	bool _windowed;
	bool _resetNeeded;

	protected:
	TextBankD3D *_textBank;
	TLVertexTable *_mesh; // mesh data used during rendering
	//const VertexTable *_sMesh;

	// source mesh for T&L
	//VertexBuffer *_sBuffer;


	// this class provides all functionality common for window/full screen versions
	protected:
	// members used for Direct3D only
	ComRef<IDirect3D8> _direct3D;
	ComRef<IDirect3DDevice8> _d3DDevice;
	
	int _lastSpec;
	int _prepSpec;
	TextureD3DHandle _lastHandle;
	bool _stencilExclusionEnabled;
	TexGenMode _texGenMode;
	bool _tlActive;
	bool _sunEnabled;

	LinkArray<Light> _lights; // currently active lights
	TLMaterial _materialSet;
	int _materialSetSpec;

	ComRef<IDirect3DVertexBuffer8> _vBufferLast; // last SetStreamSource
	ComRef<IDirect3DIndexBuffer8> _iBufferLast; // last SetIndices
	int _vOffsetLast;
	int _vBufferSize;

	int _iOffset; // something must be added to index source in DIP

	Queue *_lastQueueSource;

	// dynamic data for TL
	VertexDynamicData _dynVBuffer;
	// shared static data for TL
	RefArray<VertexStaticData> _statVBuffer;

	bool _lastClampU,_lastClampV;
	bool _enableReorder; // external hint - reorder enabled

	TexLoc _texLoc;

	bool _can565,_can88,_can8888;
	int _dxtFormats; // bitfield of dxt formats (1..5)

	bool _hasStencilBuffer:1;
	bool _canDetailTex:1;
	bool _canZBias:1;
	bool _canWBuffer:1;
	bool _canWBuffer32:1;
	int _maxLights;


	#if PIXEL_SHADERS
	int _vertexShaders; // pixel shader version
	int _pixelShaders; // pixel shader version
	bool _usePixelShaders;
	DWORD _pixelShader[NPixelShaderSpecular][NPixelShaderModes][NPixelShaders];
	PixelShaderID _pixelShaderSel;
	PixelShaderMode _pixelShaderModeSel;
	PixelShaderSpecular _pixelShaderSpecularSel;
	#endif

	#if VERTEX_SHADERS
	VertexShader _vertexShaderSel;
	DWORD _vertexShader[NVertexShaders];
	#endif

	float _nightEye;

	// members used for both DirectDraw/Direct3D

	ComRef<IDirect3DSurface8> _backBuffer;
	ComRef<IDirect3DSurface8> _frontBuffer;

	//ComRef<IDirect3DSurface8> _zBuffer;

	Queue _queueNo;
	//Queue _queueTL;

	enum RenderMode {RMLines,RMTris,RM2DLines,RM2DTris};
	RenderMode _renderMode;

	float _gamma;

	bool _flippable; // front/back buffer can be flipped

	bool _d3dFrameOpen; // FinishFrame is neccessary

	Color _textColor;

	enum VFormatSet {SingleTex,DetailTex,SpecularTex,GrassTex};
	VFormatSet _formatSet;

	private:
	void EnablePointSampling(bool enable, bool optimize);
	void SetMultiTexturing( VFormatSet format );
	void SetTexture( const TextureD3D *tex, int specFlags );

	public:
	// properties
	RString GetDebugName() const;
	int Width() const {return _w;}
	int Height() const {return _h;}
	int PixelSize() const {return _pixelSize;} // 16 or 32 bit mode?
	int RefreshRate() const {return _pp.FullScreen_RefreshRateInHz;}
	bool CanBeWindowed() const {return true;}
	bool IsWindowed() const {return _windowed;}

	int Width2D() const {return _w;}
	int Height2D() const {return _h;}
	
	int MinGuardX() const {return _minGuardX;}
	int MaxGuardX() const {return _maxGuardX;}
	int MinGuardY() const {return _minGuardY;}
	int MaxGuardY() const {return _maxGuardY;}

	int MinSatX() const {return _minGuardX;}
	int MaxSatX() const {return _maxGuardX;}
	int MinSatY() const {return _minGuardY;}
	int MaxSatY() const {return _maxGuardY;}

	void InitVRAMPixelFormat( D3DFORMAT &pf, PacFormat format, bool enableDXT );
	
	protected:
	void WorkToBack();
	void BackToFront();
	
	void CreateD3D();
	void DestroyD3D();
	void SearchMonitor(int &x, int &y, int &w, int &h);

	public:
	// constructor - init all pointers to NULL

	void CreateSurfaces(bool windowed);
	
	public:
	EngineDD
	(
		HINSTANCE hInst, HINSTANCE hPrev, int sw,
		int width, int height, bool windowed, int bpp
	);
	~EngineDD();

	bool InitDrawDone();
	
	bool IsAbleToDraw();
	bool IsAbleToDrawCheckOnly();
	void InitDraw( bool clear=false, PackedColor color=PackedColor(0) ); // Begin scene
	void FinishDraw();
	void NextFrame(); // swap frames - get ready for next frame

	void Pause();
	void Restore();

	void PreReset(bool hard);
	void PostReset();

	bool Reset(); // use Reset to change setting / reset device
	bool ResetHard(); // destroy and create the device again

	bool SwitchRes(int w, int h, int bpp);
	bool SwitchRefreshRate(int refresh);
	bool SwitchWindowed(bool windowed);

	void ListResolutions(FindArray<ResolutionInfo> &ret); // result is zero terminated w,h pairs of valid resolutions
	void ListRefreshRates(FindArray<int> &ret); // result is zero terminated w,h pairs of valid resolutions

	// functions specific to Direct3D

	// functions for both DirectDraw/Direct3D
	void InitSurfaces();
	void DestroySurfaces();
	
	//IDirectDraw7 *GetDirectDraw() const {return _directDraw;}
	IDirect3D8 *GetDirect3D() const {return _direct3D;}
	IDirect3DDevice8 *GetDirect3DDevice() const {return _d3DDevice;}

	bool Can565() const {return _can565;}
	bool Can88() const {return _can88;}
	bool Can8888() const {return _can8888;}

	TexLoc GetTexLoc() const {return _texLoc;}
	
	int DXTSupport() const {return _dxtFormats;}
	bool CanDXT( int i ) const {return (_dxtFormats&(1<<i))!=0;}
	
	public:
	
	bool GetHWTL() const {return _useHWTL;}

	void Clear( bool clearZ=true, bool clear=true, PackedColor color=PackedColor(0) );
	
	VertexBuffer *CreateVertexBuffer(const Shape &src, VBType type);
	int CompareBuffers(const Shape &s1, const Shape &s2);

	// access to _statVBuffer
	VertexStaticData *AddShape
	(
		const Shape &src, int &vIndex, int &iIndex, bool dynamic
	);

	int FrameTime() const;
	int AFrameTime() const {return FrameTime();}

	void DoSetGamma();
	void SetGamma( float gamma );
	float GetGamma() const {return _gamma;}

	void LoadConfig();
	void SaveConfig();
	
	//HWND WindowHandle() const {return _hwndApp;}
	//HWND GetWindowHandle() const {return _hwndApp;}

	void EnableReorderQueues( bool enableReorded );
	void FlushQueues();

	virtual void GetZCoefs(float &zAdd, float &zMult);
	void SetBias( int bias );
	int GetBias(){return _bias;}

	void SetGrassParams(float a1, float a2, float a3=0, float a4=0);

	bool CanZBias() const;
	bool ZBiasExclusion() const {return !_hasStencilBuffer;}

	AbstractTextBank *TextBank();
	TextBankD3D *TextBankDD() const {return _textBank;}

	void CreateTextBank();
	void ReportGRAM(const char *name);

	void Screenshot(RString filename);
	
	protected: // D3D helpers
	void DoSetGrassParamsPS();
	void DoSetGrassParamsVS();

	enum {MaxStages=8};
	struct StateInfo
	{
		DWORD value;
		StateInfo( DWORD v=-1 ):value(v){}
		ClassIsMovable(StateInfo);
	};
	typedef StateInfo TextureStageStateInfo;
	typedef StateInfo RenderStateInfo;
	AutoArray<TextureStageStateInfo> _textureStageState[MaxStages];
	AutoArray<RenderStateInfo> _renderState;

	void StencilExclusion( bool enable )
	{
		if (enable==_stencilExclusionEnabled) return;
		DoStencilExclusion(enable,true);
	}
	void DoStencilExclusion( bool enable, bool optimize );

	void ChangeClipPlanes();
	void ChangeWDepth(float matC, float matD);
	void PrepareDetailTex(bool water, bool grass);
	void PrepareSingleTexModulateA();
	void PrepareSingleTexDiffuseA();

	void EnableDetailTexGen(TexGenMode mode, bool optimize)
	{
		if (_texGenMode!=mode) DoEnableDetailTexGen(mode,optimize);
	}
	void DoEnableDetailTexGen(TexGenMode mode, bool optimize);

	#if _DEBUG
		#define RS_DIAGS 1
	#else
		#define RS_DIAGS 0
	#endif
	#if RS_DIAGS
	HRESULT D3DSetTextureStageStateName
	(
		DWORD stage, D3DTEXTURESTAGESTATETYPE state, DWORD value, const char *text,
		bool optimize=true
	);
	HRESULT D3DSetRenderStateName
	(
		D3DRENDERSTATETYPE state, DWORD value, const char *text,
		bool optimize=true
	);
	#define D3DSetRenderState(state,value) \
		D3DSetRenderStateName(state,value,#state,true)
	#define D3DSetTextureStageState(stage,state,value) \
		D3DSetTextureStageStateName(stage,state,value,#state,true)
	#define D3DSetRenderStateOpt(state,value,optimize) \
		D3DSetRenderStateName(state,value,#state,optimize)
	#define D3DSetTextureStageStateOpt(stage,state,value,optimize) \
		D3DSetTextureStageStateName(stage,state,value,#state,optimize)
	#else
	HRESULT D3DSetTextureStageState
	(
		DWORD stage, D3DTEXTURESTAGESTATETYPE state, DWORD value, bool optimize=true
	);
	HRESULT D3DSetRenderState
	(
		D3DRENDERSTATETYPE state, DWORD value, bool optimize=true
	);		
	#define D3DSetRenderStateOpt D3DSetRenderState
	#define D3DSetTextureStageStateOpt D3DSetTextureStageState
	#endif

	WORD *QueueAdd( Queue &queue, int n );
	void QueueFan( const VertexIndex *ii, int n );

	void Queue2DPoly( const TLVertex *v, int n );

	void FlushQueue(Queue &queue, int index);
	void FlushAndFreeQueue(Queue &queue, int index);

	int AllocateQueue(Queue &queue, TextureD3D *tex, int level, int spec);
	void FreeQueue(Queue &queue, int index);
	void FreeAllQueues(Queue &queue);
	void FlushAndFreeAllQueues(Queue &queue, bool nonEmptyOnly=false);
	void FlushAllQueues(Queue &queue, int skip=-1);

	void CloseAllQueues(Queue &queue);

	//void FlushQueue(){FlushQueue(_queue,index;
	//void Flush2DQueue();


	void SwitchRenderMode( RenderMode mode )
	{
		if (_renderMode==mode) return;
		DoSwitchRenderMode(mode);
	}
	void DoSwitchRenderMode( RenderMode mode );

	void D3DPreparePoint();
	void D3DPrepare3DLine();
	void DrawPoints(const TLVertex *vs, int nVertex);
	void DrawPoints( int beg, int end );
	void DrawLine( int beg, int end );
	void DrawPoly
	(
		const MipInfo &mip, const Vertex2DPixel *vertices, int nVertices,
		const Rect2DPixel &clip=Rect2DClipPixel, int specFlags=DefSpecFlags2D
	);
	void DrawPoly
	(
		const MipInfo &mip, const Vertex2DAbs *vertices, int nVertices,
		const Rect2DAbs &clip=Rect2DClipAbs, int specFlags=DefSpecFlags2D
	);

	void DoPrepareTriangle( const MipInfo &absMip, int specFlags );
	void QueuePrepareTriangle( const MipInfo &absMip, int specFlags );
	void DoPrepareTriangle( TextureD3D *tex, int level, int spec );

	void PrepareTriangle( const MipInfo &absMip, int specFlags );
	void PrepareTriangleTL(const MipInfo &mip, int specFlags);

	void DrawPolygon(const VertexIndex *ii, int n);
	void DrawSection(const FaceArray &face, Offset beg, Offset end);


	// integrated transform&lighting
	bool GetTL() const {return _useDXTL;}
	bool HasWBuffer() const {return _useWBuffer;}
	//bool HasWBuffer() const {return false;}

	// engine w-buffer interface
	virtual bool IsWBuffer() const;
	virtual bool CanWBuffer() const;
	virtual void SetWBuffer(bool val);

	void SetupLights(const TLMaterial &mat, const LightList &lights, int spec);
	void DoSwitchTL( bool active );

	//void AddVertices( Queue &queue, const void *v, int n, int vSize );

	void DiscardVB();
	void AddVertices( const TLVertex *v, int n );
	//void AddTLVertices( const SVertex *v, int n );

	public:
	/// helper: used from VertexBufferD3D::Update
	void SetVSourceStatic( const VertexBufferD3D &buf );
	void SetVSourceDynamic
	(
		const Shape &src, const VertexBufferD3D &buf
	);

	public:
	void SwitchTL( bool active )
	{
		if (active==_tlActive) return;
		DoSwitchTL(active);
	}
	void DoSetMaterial(const TLMaterial &mat, const LightList &lights, int spec);
	void SetMaterial(const TLMaterial &mat, const LightList &lights, int spec);
	void EnableSunLight(bool enable);

	int AddLight(Light *light);
	void ClearLights();

	void PrepareMeshTL( const LightList &lights, const Matrix4 &modelToWorld, int spec ); // prepare internal variables
	void BeginMeshTL( const Shape &sMesh, int spec, bool dynamic=false ); // convert all mesh vertices
	void EndMeshTL( const Shape &sMesh ); // forget mesh
	void DrawSectionTL(const Shape &sMesh, int beg, int end);

	void DrawDecal
	(
		Vector3Par screen, float rhw, float sizeX, float sizeY, PackedColor color,
		const MipInfo &mip, int specFlags
	);
	void Draw2D
	(
		const Draw2DPars &pars, const Rect2DAbs &rect, const Rect2DAbs &clip
	);
	void DrawLine
	(
		const Line2DAbs &line,
		PackedColor c0, PackedColor c1,
		const Rect2DAbs &clip=Rect2DClipAbs
	);
	//void D3DTextState();

	void D3DDrawTexts();

	void D3DConstruct();
	void D3DDestruct();
	void TextureDestroyed( Texture *tex );

	void D3DCloseDraw();
	void D3DBeginScene();
	void D3DEndScene();

	public:
	void CreateVB();
	void DestroyVB();

	void CreateVBTL();
	void DestroyVBTL();

	void RestoreVB();
	
	bool CanRestore();
	void FogColorChanged( ColorVal fogColor )
	{
		SetD3DFog(fogColor);
	}
	virtual void EnableNightEye(float night);

	void PrepareMesh( int spec ); // prepare internal variables
	void BeginMesh( TLVertexTable &mesh, int spec ); // convert all mesh vertices
	void EndMesh( TLVertexTable &mesh ); // forget mesh

	bool CanGrass() const;

	#if VERTEX_SHADERS
	void InitVertexShaders();
	void DeinitVertexShaders();

	void SelectVertexShader(VertexShader mode)
	{
		if (_vertexShaderSel==mode) return;
		DoSelectVertexShader(mode);
	}

	void DoSelectVertexShader(VertexShader mode);
	#endif

	#if PIXEL_SHADERS

	void InitPixelShaders();
	void DeinitPixelShaders();
	bool UsingPixelShaders() const {return _usePixelShaders;}

	void SelectPixelShaderMode(PixelShaderMode mode)
	{
		if (_pixelShaderModeSel==mode) return;
		DoSelectPixelShader(_pixelShaderSel,mode,_pixelShaderSpecularSel);
	}
	void SelectPixelShaderSpecular(PixelShaderSpecular spec)
	{
		if (_pixelShaderSpecularSel==spec) return;
		DoSelectPixelShader(_pixelShaderSel,_pixelShaderModeSel,spec);
	}

	void SelectPixelShader(PixelShaderID ps)
	{
		if (_pixelShaderSel==ps) return;
		DoSelectPixelShader(ps,_pixelShaderModeSel,_pixelShaderSpecularSel);
	}
	void DoSelectPixelShader(PixelShaderID ps, PixelShaderMode mode, PixelShaderSpecular spec);
	#endif

	void Init3DState();
	void FindMode(int adapter); // prepare presentation parameters based on _w, _h, _pixelSize
	void Init3D();
	void SetD3DFog( ColorVal fog );
	//void SetD3DBias( int bias );

	float ZShadowEpsilon() const {return 0.01;}
	float ZRoadEpsilon() const {return 0.005;}

	float ObjMipmapCoef() const {return 1.5;}
	float LandMipmapCoef() const {return 1.0;}

	bool ShadowsFirst() const {return false;}
	bool SortByShape() const {return true;}

};

#define GEngineDD static_cast<EngineDD *>(GEngine)

// XBOX / Win32 specific texture format definition

#ifdef _XBOX

/*
#define tex_A1R5G5B5 D3DFMT_LIN_A1R5G5B5
#define tex_A4R4G4B4 D3DFMT_LIN_A4R4G4B4
#define tex_A8R8G8B8 D3DFMT_LIN_A8R8G8B8
#define tex_R5G6B5 D3DFMT_LIN_R5G6B5
#define tex_A8L8 D3DFMT_LIN_A8L8
*/
#define tex_A1R5G5B5 D3DFMT_A1R5G5B5
#define tex_A4R4G4B4 D3DFMT_A4R4G4B4
#define tex_A8R8G8B8 D3DFMT_A8R8G8B8
#define tex_R5G6B5 D3DFMT_R5G6B5
#define tex_A8L8 D3DFMT_A8L8

#else

#define tex_A1R5G5B5 D3DFMT_A1R5G5B5
#define tex_A4R4G4B4 D3DFMT_A4R4G4B4
#define tex_A8R8G8B8 D3DFMT_A8R8G8B8
#define tex_R5G6B5 D3DFMT_R5G6B5
#define tex_A8L8 D3DFMT_A8L8

#endif

#endif // !_DISABLE_GUI

#endif

