#ifdef _MSC_VER
#pragma once
#endif

/*!
\file
Interface of network transport implementation classes
*/

#ifndef _NET_TRANSPORT_HPP
#define _NET_TRANSPORT_HPP

#include <Es/Strings/rString.hpp>
#include <Es/Containers/array.hpp>

//! Description of multiplayer session
struct SessionInfo
{
	//! URL of host
	RString guid;
	//! Name of session
	RString name;
	//! Last time session was accessed
	DWORD lastTime;
	//! Host's current version
	int actualVersion;
	//! Host's required version
	int requiredVersion;
	//! Session is pasword protected
	bool password;
	//! Host's version is too old
	bool badActualVersion;
	//! Host's version is too new
	bool badRequiredVersion;
  //! Mod lists differs
  bool badMod;
	//! Mission name
	RString mission;
	//! State of game on host
	int gameState;
	//! Ping time (in ms)
	int ping;
	//! Current number of players
	int numPlayers;
	//! Maximal number of players
	int maxPlayers;
	//! Estimated time to mission end (in minutes)
	int timeleft;
  //! mod list on host
  RString mod;
  //! equal mod list is required by host
  bool equalModRequired;
};
TypeIsMovableZeroed(SessionInfo)

/*!
\patch 1.92 Date 6/23/2003 by Jirka
- Added: Testing of -mod compatibility in MP
*/

#define MOD_LENGTH  80

//! Basic information sent in session description as user data
struct MPVersionInfoOld
{
	//! actual application version
	int versionActual;
	//! required application version
	int versionRequired;
	//! name of running mission
	char mission[40];
	//! state of multiplayer game
	int gameState;
};

struct MPVersionInfo : public MPVersionInfoOld
{
  //! -mod list
  char mod[MOD_LENGTH];
};

//! Result of connecting client to server
enum ConnectResult
{
	//! connect in progress
	CRNone = -1,
	//! connected
	CROK,
	//! bad password
	CRPassword,
	//! incompatible program version
	CRVersion,
	//! other error
	CRError,
	//! session full (maximum number of players allotted for the session has been reached)
	CRSessionFull,
};

// flags for message
enum NetMsgFlags
{
	NMFNone=0,
	NMFGuaranteed=1,
	NMFHighPriority=2,
	NMFStatsAlreadyDone=4
};

enum NetTerminationReason
{
	NTRTimeout,
	NTRDisconnected,
	NTRKicked,
	NTRBanned,
	NTRMissingAddon,
	NTROther, // must be last
};

__forceinline NetMsgFlags operator | (NetMsgFlags a, NetMsgFlags b)
{
	return NetMsgFlags((int)a|(int)b);
}

//! Info about message completion
/*!
Used for transfer information from receiving thread to main application thread.
*/
struct SendCompleteInfo
{
	DWORD msgID;
	bool ok;
};
TypeIsSimple(SendCompleteInfo)

//! Info about new player
/*!
Used for transfer information from receiving thread to main application thread.
*/
struct CreatePlayerInfo
{
	int player;
	bool botClient;
	char name[56];
  char mod[MOD_LENGTH];
};
TypeIsSimple(CreatePlayerInfo)

//! Info about removed player
/*!
Used for transfer information from receiving thread to main application thread.
*/
struct DeletePlayerInfo
{
	int player;
};
TypeIsSimple(DeletePlayerInfo)

//! additional remote host info
struct RemoteHostAddress
{
	RString name;
	RString ip;
	int port;
};
TypeIsMovableZeroed(RemoteHostAddress)

typedef void UserMessageClientCallback(char *buffer, int bufferSize, void *context);
typedef void UserMessageServerCallback(int from, char *buffer, int bufferSize, void *context);
typedef void SendCompleteCallback(DWORD msgID, bool ok, void *context);
typedef void CreatePlayerCallback(int player, bool botClient, const char *name, const char *mod, void *context);
typedef void DeletePlayerCallback(int player, void *context);
typedef bool CreateVoicePlayerCallback(int player, void *context);

//! Interface for class supporting session enumeration
class NetTranspSessionEnum
{
public:
	//! Constructor
	NetTranspSessionEnum() {}
	//! Destructor
	virtual ~NetTranspSessionEnum() {}

	virtual bool Init( int magic =0 ) = NULL;

	virtual bool RunningEnumHosts() = NULL;
	virtual bool StartEnumHosts(RString ip, int port, AutoArray<RemoteHostAddress> *hosts) = NULL;
	virtual void StopEnumHosts() = NULL;

	virtual int NSessions() = NULL;
	virtual void GetSessions(AutoArray<SessionInfo> &sessions) = NULL;

	//! Transfer IP address and port into URL address
	virtual RString IPToGUID(RString ip, int port) = NULL;
};

//! Interface for 3D sound buffer
class NetTranspSound3DBuffer
{
public:
	//! Constructor
	NetTranspSound3DBuffer() {}
	//! Destructor
	virtual ~NetTranspSound3DBuffer() {}
	
	virtual void SetPosition(float x, float y, float z) = NULL;
};

class ParamEntry;

//! Interface for network transport client class
class NetTranspClient
{
public:
	//! Constructor
	NetTranspClient() {}
	//! Destructor
	virtual ~NetTranspClient() {}

	//! Initialization of client
	/*!
	\param address URL address of host
	\param password session password
	\param botClient client is bot client (must be sent to server in connection packet)
	\param playerName name of player (must be sent to server in connection packet)
	\param versionInfo info about actual and required application version
				(must be sent to server in connection packet)
	\param magic application identifier
	\param port
				- on input: expected host IP port (obtained form session enumeration or user input)
				- on output: real session port client was connected to
	\return result of connect
	*/
	virtual ConnectResult Init
	(
		RString address, RString password, bool botClient, int &port,
		RString player, MPVersionInfo &versionInfo, int magic =0
	) = NULL;
	//! Create and initialize DirectPlay voice client
	virtual bool InitVoice() = NULL;

	virtual bool SendMsg(BYTE *buffer, int bufferSize, DWORD &msgID, NetMsgFlags flags) = NULL;
	virtual void GetSendQueueInfo(int &nMsg, int &nBytes, int &nMsgG, int &nBytesG) = NULL;
	virtual bool GetConnectionInfo(int &latencyMS, int &throughputBPS) = NULL;
	virtual bool GetConnectionInfoRaw(int &latencyMS, int &throughputBPS)
	{
		return GetConnectionInfo(latencyMS, throughputBPS);
	}

    virtual void SetNetworkParams ( const ParamEntry &cfg )
    {}

	virtual float GetLastMsgAge() = NULL;
	virtual float GetLastMsgAgeReliable() = NULL;
	virtual float GetLastMsgAgeReported() = NULL;
	virtual void LastMsgAgeReported() = NULL;

	virtual bool IsSessionTerminated() = NULL;
	virtual NetTerminationReason GetWhySessionTerminated() = NULL;

	virtual bool IsVoicePlaying(int player) = NULL;
	virtual bool IsVoiceRecording() = NULL;
	virtual NetTranspSound3DBuffer *Create3DSoundBuffer(int player) = NULL;

	virtual void ProcessUserMessages(UserMessageClientCallback *callback, void *context) = NULL;
	virtual void RemoveUserMessages() = NULL;
	virtual void ProcessSendComplete(SendCompleteCallback *callback, void *context) = NULL;
	virtual void RemoveSendComplete() = NULL;

    virtual RString GetStatistics ()
    { return RString(); }

    virtual unsigned FreeMemory ()
    { return 0; }

};

//! Interface for network transport server class
class NetTranspServer
{
public:
	//! Constructor
	NetTranspServer() {}
	//! Destructor
	virtual ~NetTranspServer() {}

	//! Initialization of client
	/*!
	\param port recommended IP port
	\param password session password (information if password is required must be included in enumeration response)
	\param hostname host name - must be included in session name sent in enumeration response, if empty, use IP address:port instead
	\param maxPlayers maximal number of connected players (must be incuded in enumeration response)
	\param sessionNameInit session name used if host address cannot be obtained
	\param sessionNameFormat format of session name (printf syntax) - playerName and hostName are sent as parameters
	\param playerName name of hosting player (used in session name)
	\param versionInfo info about actual and required application version
				(used to decide if client can connect)
	\param magic application identifier
	*/
	virtual bool Init
	(
		int port, RString password, char *hostname, int maxPlayers,
		RString sessionNameInit, RString sessionNameFormat, RString playerName,
    MPVersionInfo &versionInfo, bool equalModRequired, int magic =0
	) = NULL;
	//! Create and initialize DirectPlay voice server
	virtual bool InitVoice() = NULL;

	virtual RString GetSessionName() = NULL;
	//! Return real local port of session
	virtual int GetSessionPort() = NULL;

	virtual bool SendMsg(int to, BYTE *buffer, int bufferSize, DWORD &msgID, NetMsgFlags flags) = NULL;
	virtual void CancelAllMessages() = NULL;
	virtual void GetSendQueueInfo(int to, int &nMsg, int &nBytes, int &nMsgG, int &nBytesG) = NULL;
	virtual bool GetConnectionInfo(int to, int &latencyMS, int &throughputBPS) = NULL;
	virtual bool GetConnectionInfoRaw(int to, int &latencyMS, int &throughputBPS)
	{
		return GetConnectionInfo(to, latencyMS, throughputBPS);
	}
    virtual void SetNetworkParams ( const ParamEntry &cfg )
    {}
	virtual float GetLastMsgAgeReliable(int player) = NULL;

	virtual void UpdateSessionDescription(int state, RString mission) = NULL;
	virtual void KickOff(int player, NetTerminationReason reason) = NULL;
	//! Retrieves URL of server
	virtual bool GetURL(char *address, DWORD addressLen) = NULL;

	virtual void GetTransmitTargets(int from, AutoArray<int, MemAllocSA> &to) = NULL;
	virtual void SetTransmitTargets(int from, AutoArray<int, MemAllocSA> &to) = NULL;

	virtual void ProcessUserMessages(UserMessageServerCallback *callback, void *context) = NULL;
	virtual void RemoveUserMessages() = NULL;
	virtual void ProcessSendComplete(SendCompleteCallback *callback, void *context) = NULL;
	virtual void RemoveSendComplete() = NULL;
	virtual void ProcessPlayers(CreatePlayerCallback *callbackCreate, DeletePlayerCallback *callbackDelete, void *context) = NULL;
	virtual void RemovePlayers() = NULL;
	virtual void ProcessVoicePlayers(CreateVoicePlayerCallback *callback, void *context) = NULL;
	virtual void RemoveVoicePlayers() = NULL;

    virtual RString GetStatistics (int player)
    { return RString(); }

    virtual unsigned FreeMemory ()
    { return 0; }

};

//! Create DirectPlay implementation of NetTranspSessionEnum class
NetTranspSessionEnum *CreateDPlaySessionEnum();
//! Create DirectPlay implementation of NetTranspClient class
NetTranspClient *CreateDPlayClient();
//! Create DirectPlay implementation of NetTranspServer class
NetTranspServer *CreateDPlayServer();

//! Create Net implementation of NetTranspSessionEnum class
NetTranspSessionEnum *CreateNetSessionEnum( const ParamEntry &cfg );
//! Create Net implementation of NetTranspClient class
NetTranspClient *CreateNetClient( const ParamEntry &cfg );
//! Create Net implementation of NetTranspServer class
NetTranspServer *CreateNetServer( const ParamEntry &cfg );

#endif
