#ifdef _MSC_VER
#pragma once
#endif

#ifndef _ANIMATION_HPP
#define _ANIMATION_HPP

#include "Shape.hpp"
#include "time.hpp"

class Animation
{
	protected:
	int _selection[MAX_LOD_LEVELS]; // named selection index
	
	void DoConstruct();

	public:
	Animation();
	//Animation( LODShape *shape, const char *nameSel, const char *altName );
	void Init( LODShape *shape, const char *nameSel, const char *altName );
	void Deinit();

	void Transform
	(
		LODShape *shape, const Matrix4 &trans, int level
	) const; // apply transformation to all points - use original position
	void TransformOver
	(
		LODShape *shape, const Matrix4 &trans, int level
	) const; // apply transformation to all points - use current position
	void TransformWithWeight
	(
		LODShape *shape, const Matrix4 &trans, int level
	) const; // apply transformation to all points

	Vector3 Transform
	(
		LODShape *shape, const Matrix4 &trans, int level, int index
	) const; // apply transformation to one point - use original position
	Vector3 TransformWithWeight
	(
		LODShape *shape, const Matrix4 &trans, int level, int index
	) const; // apply transformation to one point

	void Restore( LODShape *shape, int level ) const; // restore original

	int GetSelection( int level ) const {return _selection[level];}
	bool IsEmpty() const; // check if empty in all levels

};


TypeIsMovable(Animation)

// AnimationSection is used for animation based on faces
// hiding or changing texture are typical operations

class AnimationSection: public Animation
{
	protected:
	void DoConstruct();

	public:
	AnimationSection();
	//AnimationSection( LODShape *shape, const char *nameSel, const char *altName );
	void Register( LODShape *shape, const char *nameSel, const char *altName );
	void Init( LODShape *shape, const char *nameSel, const char *altName );
	void Deinit();

	void SetTexture( LODShape *shape, int level, Texture *texture ) const; // apply texture to all faces

	Texture *GetTexture(LODShape *shape) const; // get first texture used on selection

	void Hide( LODShape *shape, int level ) const; // apply texture to all faces
	void Unhide( LODShape *shape, int level ) const; // apply texture to all faces
};


TypeIsMovable(AnimationSection)

class AnimationUV
{
	protected:
	int _selection[MAX_LOD_LEVELS]; // selected mesh indices
	Temp<float> _origU[MAX_LOD_LEVELS]; // original U coordinates
	Temp<float> _origV[MAX_LOD_LEVELS]; // original V coordinates
	
	void DoConstruct();

	public:
	AnimationUV(){DoConstruct();}
	//AnimationUV( LODShape *shape, const char *nameSel ){DoConstruct();Init(shape,nameSel);}
	void Init( LODShape *shape, const char *nameSel );
	void UVOffset( LODShape *shape, float offsetU, float offsetV, int level ) const; // apply transformation to all points
	void Restore( LODShape *shape, int level ) const; // restore original
	//void FlipU( LODShape *shape, int level ) const;  // apply transformation to all points
	//void FlipV( LODShape *shape, int level ) const;  // apply transformation to all points
	int GetSelection( int level ) const {return _selection[level];}
};

TypeIsMovable(AnimationUV)

class AnimationAnimatedTexture: public AnimationSection
{
	// original textures coordinates
	// by face
	Temp< Ref<Texture> > _animTexF[MAX_LOD_LEVELS];
	// by section
	Temp< Ref<Texture> > _animTexS[MAX_LOD_LEVELS];

	void DoConstruct();

	public:
	AnimationAnimatedTexture(){DoConstruct();}
	//AnimationAnimatedTexture( LODShape *shape, const char *nameSel ){DoConstruct();Init(shape,nameSel,NULL);}
	void Init( LODShape *shape, const char *nameSel, const char *altNameSel );
	void Deinit();

	void SetPhase(LODShape *shape, int level, int phase) const;
	void AnimateTexture(LODShape *shape, int level, float anim) const;
};

TypeIsMovable(AnimationAnimatedTexture)

class AnimationWithCenter: public Animation
{
	private:
	int _centerSelection[MAX_LOD_LEVELS]; // named selection index
	Vector3 _center[MAX_LOD_LEVELS];
	
	public:
	AnimationWithCenter();
	//AnimationWithCenter
	//(
	//	LODShape *shape, const char *name, const char *altName,
	//	const char *center, const char *altCenter=NULL
	//);
	void Init
	(
		LODShape *shape, const char *name, const char *altName,
		const char *center, const char *altCenter=NULL
	);
	void Deinit();

	void Apply( LODShape *shape, const Matrix4 &trans, int level ) const;
	void ApplyWithWeight( LODShape *shape, const Matrix4 &trans, int level ) const;
	Vector3 Center() const
	{
		for( int i=0; i<MAX_LOD_LEVELS; i++ ) if( _selection[i]>=0 ) return _center[i];
		return VZero;
	}
	Vector3 Center( int level ) const
	{
		if( _selection[level]>=0 ) return _center[level];
		return VZero;
	}
	int GetCenterSelection( int level ) const {return _centerSelection[level];}
	void SetCenter( Vector3Par center )
	{
		for( int i=0; i<MAX_LOD_LEVELS; i++ ) _center[i]=center;
	}
	void CalculateCenter( LODShape *shape );
};

TypeIsMovable(AnimationWithCenter)


class AnimationRotation: public Animation
{
	Vector3 _center[MAX_LOD_LEVELS];
	Vector3 _direction[MAX_LOD_LEVELS];

	public:
	AnimationRotation();
	//AnimationRotation
	//(
	//	LODShape *shape, const char *name, const char *altName,
	//	const char *axis, const char *altAxis = NULL, bool inMemory = true
	//);
	void Init
	(
		LODShape *shape, const char *name, const char *altName,
		const char *axis, const char *altAxis = NULL, bool inMemory = true
	);
	void Init2
	(
		LODShape *shape, const char *name, const char *begin, const char *end, bool inMemory = true
	);
	void Deinit();

	void GetRotation( Matrix4 &mat, float angle, int level ) const;
	void Rotate( LODShape *shape, float angle, int level ) const;
	void RotateWithWeight( LODShape *shape, float angle, int level ) const;
	Vector3 Center() const
	{
		for( int i=0; i<MAX_LOD_LEVELS; i++ ) if( _selection[i]>=0 ) return _center[i];
		return Vector3(0,0,0);
	}
	Vector3 Direction() const
	{
		for( int i=0; i<MAX_LOD_LEVELS; i++ ) if( _selection[i]>=0 ) return _direction[i];
		return Vector3(0,0,0);
	}
	void SetCenter( Vector3Par center, Vector3Par direction )
	{
		for( int i=0; i<MAX_LOD_LEVELS; i++ ) _center[i]=center,_direction[i]=direction;
	}
	void CalculateCenter( LODShape *shape );
};

TypeIsMovable(AnimationRotation)

class ParamEntry;

/*!
\patch_internal 1.75 Date 3/6/2002 by Jirka
- Added: new support for animations defined in config and launched by procedure animate
*/

class AnimationType : public RefCount
{
protected:
	RString _name;
	float _animSpeed;

public:
	static AnimationType *CreateObject(const ParamEntry &cls, LODShape *shape);
	virtual void Init(const ParamEntry &cls, LODShape *shape);

	RString GetName() const {return _name;}
	float GetAnimSpeed() const {return _animSpeed;}

	virtual int GetSelection(int level) const = NULL;
	virtual void Animate
	(
		LODShape *shape, int level, float phase, Matrix4Par baseAnim
	) = NULL;
	virtual void Deanimate(LODShape *shape, int level) = NULL;
};

class AnimationRotationType : public AnimationType
{
protected:
	typedef AnimationType base;
    
	AnimationRotation _animation;
	float _angle0;
	float _angle1;

public:
	void Init(const ParamEntry &cls, LODShape *shape);

	int GetSelection(int level) const;
	void Animate(LODShape *shape, int level, float phase, Matrix4Par baseAnim);
	void Deanimate(LODShape *shape, int level);
};

class AnimationInstance
{
protected:
	Ref<AnimationType> _type;
	float _phase;
	float _phaseWanted;
	Time _lastAnimation;
    
public:
	AnimationInstance();
	void Init();
	
	RString GetName() const {return _type ? _type->GetName() : "";}
	float GetPhase() const {return _phase;}
	float GetPhaseWanted() const {return _phaseWanted;}

	void SetType(AnimationType *type) {_type = type;}
	void SetPhaseWanted(float phase) {_phaseWanted = phase;}

	int GetSelection(int level) const;
	void Animate(LODShape *shape, int level, Matrix4Par baseAnim);
	void Deanimate(LODShape *shape, int level);

protected:
	void AdvanceTime();
};

TypeIsMovable(AnimationInstance)

#endif
