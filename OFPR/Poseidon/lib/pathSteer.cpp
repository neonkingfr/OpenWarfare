// implementation of Path steering

#include "wpch.hpp"

#include "pathSteer.hpp"
#include "landscape.hpp"
#include "ai.hpp"
#include "operMap.hpp"
#include "global.hpp"
#include "network.hpp"

inline float Dist2( const OperInfoResult &info, Vector3Par pos )
{
	return info._pos.Distance2(pos);
}

int Path::FindNearest( Vector3Par pos ) const
{
	// find nearest point on path
	int minI=-1;
	float minDist2=1e10;
	for( int i=0; i<Size(); i++ )
	{
		const OperInfoResult &info=Get(i);
		float dist2=Dist2(info,pos);
		if( minDist2>dist2 )
		{
			minDist2=dist2;
			minI=i;
		}
	}
	return minI;
}

float Path::Distance( int index, Vector3Par pos ) const
{
	const OperInfoResult &curr=Get(index);
	const OperInfoResult &next=Get(index+1);
	Vector3 b = curr._pos;
	Vector3 e = next._pos - curr._pos;
	Vector3 p = pos - curr._pos;
	float t=(e*p)/e.SquareSize();
	saturate(t,0,1);
	Vector3 nearest=b+t*e;
	float dist2Next=nearest.Distance2(pos);
	return dist2Next;
}

int Path::FindNext( Vector3Par pos ) const
{
	// find index so that distance of line index-1,index to pos 
	// is minimal
	int index=0;
	float minDist=1e10;
	for( int i=0; i<Size()-1; i++ )
	{
		float dist=Distance(i,pos);
		if( minDist>=dist ) minDist=dist,index=i;
	}
	return index+1;
}

inline Vector3Val OperPos( const OperInfoResult &info )
{
	return info._pos;
}


Path::Path()
{
	_onRoad=false;
}

//#include "scene.hpp"

inline float NearestPointT( Vector3Par beg, Vector3Par end, Vector3Par pos )
{
	// point on line beg .. end nearest to pos
	Vector3Val eb = end - beg;
	Vector3Val pb = pos - beg;
	return (eb*pb) / eb.SquareSize();
}

inline Vector3 NearestPoint( Vector3Par beg, Vector3Par end, Vector3Par pos )
{
	// point on line beg .. end nearest to pos
	Vector3Val eb = end - beg;
	Vector3Val pb = pos - beg;
	float t = (eb*pb) / eb.SquareSize();
	saturate(t,0,1);
	return beg+eb*t;
}

inline Vector3 NearestPointInfinite( Vector3Par beg, Vector3Par end, Vector3Par pos )
{
	// point on line beg .. end nearest to pos
	Vector3Val eb = end - beg;
	Vector3Val pb = pos - beg;
	float t = (eb*pb) / eb.SquareSize();
	return beg+eb*t;
}

float Path::CostAtPos( Vector3Par pos ) const
{
	if( Size()<2 ) return Get(Size()-1)._cost;
	int index=FindNext(pos);
	if( index<1 )
	{
		Fail("Bad next.");
		return Get(0)._cost;
	}
	const OperInfoResult &curr=Get(index);
	const OperInfoResult &prev=Get(index-1);
	float tPrev = NearestPointT(curr._pos,prev._pos,pos);
	saturate(tPrev,0,1);
	return curr._cost+(prev._cost-curr._cost)*tPrev;
}

Vector3 Path::NearestPos( Vector3Par pos ) const
{
	if( Size()<2 ) return Get(Size()-1)._pos;
	int index=FindNext(pos);
	if( index<0 )
	{
		Fail("Bad next.");
		return Get(Size()-1)._pos;
	}
	const OperInfoResult &curr=Get(index);
	const OperInfoResult &prev=Get(index-1);

	return NearestPoint(curr._pos,prev._pos,pos);
}

Vector3 Path::PosAtCost( float cost ) const
{
	int next;
	int size=Size();
	for( next=1; next<size; next++ )
	{
		if( Get(next)._cost>=cost ) break;
	}
	if( next>=size )
	{
		// return end of path
		return OperPos(Get(size-1));
	}
	// interpolate
	const OperInfoResult &pInfo=Get(next-1);
	const OperInfoResult &nInfo=Get(next);
	float denom=nInfo._cost-pInfo._cost;
	if( denom<=0 ) return OperPos(nInfo);
	float factor=(cost-pInfo._cost)/denom;
	Assert( factor>=-0.001 );
	Assert( factor<=+1.001 );

	Vector3Val beg = pInfo._pos;
	Vector3Val end = nInfo._pos;

	return beg * (1-factor) + end * factor;
}

#include "scene.hpp"

bool Path::InHouseAtCost( float cost, Vector3Par pos ) const
{
	int next;
	int size=Size();
	if (size<=0) return false;
	for( next=1; next<size; next++ )
	{
		if( Get(next)._cost>=cost ) break;
	}
	if( next>=size )
	{
		// return end of path
		const OperInfoResult &last=Get(size-1);
		return last._house!=NULL;
	}
	// interpolate
	const OperInfoResult &pInfo=Get(next-1);
	const OperInfoResult &nInfo=Get(next);

	return pInfo._house!=NULL && nInfo._house!=NULL;
}

Vector3 Path::PosAtCost( float cost, Vector3Par point ) const
{
	int next;
	int size=Size();
	for( next=1; next<size; next++ )
	{
		if( Get(next)._cost>=cost ) break;
	}
	if( next>=size )
	{
		// return end of path
		return OperPos(Get(size-1));
	}
	// interpolate
	const OperInfoResult &pInfo=Get(next-1);
	const OperInfoResult &nInfo=Get(next);
	float denom=nInfo._cost-pInfo._cost;
	if( denom<=0 ) return OperPos(nInfo);
	float factor=(cost-pInfo._cost)/denom;
	Assert( factor>=-0.001 );
	Assert( factor<=+1.001 );


	Vector3Val beg = pInfo._pos;
	Vector3Val end = nInfo._pos;

	// ret is point on line corresponding to cost
	Vector3 ret = beg * (1-factor) + end * factor;
	
	// the point must be somewhere above or below the path

	// containing point
	// i.e. everything should happen in plane containing beg,end and end+VUp
	// adjust direction is aside to beg..end

	
	Vector3Val eb = end - beg;

	if (eb.SquareSize()<1e-6)
	{
		// singular case - between two identical points
		LogF("Singular path");
		return ret;
	}
	// calculate normal of vertical plane containing eb
	Vector3 planeNormal = VUp.CrossProduct(eb);
	// calculate most vertical perpendicular to eb
	Vector3 perpFromEB = planeNormal.CrossProduct(eb);

	// search for point ret..ret+aside nearest to point
	Vector3 retPoint = NearestPointInfinite(ret,ret+perpFromEB,point);

	#if 0
	if (CHECK_DIAG(DECostMap)
	{

	// white ball - point
	{
		Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),-1);
		obj->SetPosition(point);
		obj->SetScale(0.1);
		obj->SetConstantColor(PackedColor(Color(1,1,1)));
		GLandscape->ShowObject(obj);
	}

	// red ball - end
	{
		Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),-1);
		obj->SetPosition(end);
		obj->SetScale(0.1);
		obj->SetConstantColor(PackedColor(Color(1,0,0)));
		GLandscape->ShowObject(obj);
	}

	// green ball - beg
	{
		Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),-1);
		obj->SetPosition(beg);
		obj->SetScale(0.1);
		obj->SetConstantColor(PackedColor(Color(0,1,0)));
		GLandscape->ShowObject(obj);
	}

	// blue ball - ret
	{
		Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),-1);
		obj->SetPosition(ret);
		obj->SetScale(0.1);
		obj->SetConstantColor(PackedColor(Color(0,0,1)));
		GLandscape->ShowObject(obj);
	}

	// yellow ball - ret+normal
	{
		Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),-1);
		obj->SetPosition(ret+planeNormal);
		obj->SetScale(0.1);
		obj->SetConstantColor(PackedColor(Color(1,1,0)));
		GLandscape->ShowObject(obj);
	}

	// cyan ball - ret+perpFromEB
	{
		Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),-1);
		obj->SetPosition(ret+perpFromEB);
		obj->SetScale(0.1);
		obj->SetConstantColor(PackedColor(Color(0,1,1)));
		GLandscape->ShowObject(obj);
	}

	// black ball - retPoint
	{
		Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),-1);
		obj->SetPosition(retPoint);
		obj->SetScale(0.1);
		obj->SetConstantColor(PackedColor(Color(0,0,0)));
		GLandscape->ShowObject(obj);
	}

	}

	#endif

	return retPoint;
}

float Path::SpeedAtCost( float cost ) const
{
	int next;
	int size=Size();
	for( next=1; next<size; next++ )
	{
		if( Get(next)._cost>=cost ) break;
	}
	if( next>=size )
	{
		// end of path - no speed
		next=size-1;
		if( next<=0 ) return 0; // no path - no speed
	}
	// interpolate
	const OperInfoResult &pInfo=Get(next-1);
	const OperInfoResult &nInfo=Get(next);
	float nom=OperPos(nInfo).Distance(OperPos(pInfo));
	float denom=nInfo._cost-pInfo._cost;
	if( nInfo._cost>GET_UNACCESSIBLE )
	{
		// TODO: BUG repair
		LogF("GET_UNACCESSIBLE cost in path N");
		return 1;
	}
	if( pInfo._cost>GET_UNACCESSIBLE )
	{
		// TODO: BUG repair
		LogF("GET_UNACCESSIBLE cost in path P");
		return 1;
	}
	if( denom<=0 )
	{
		if( nom>1 )
		{
			// TODO: BUG repair
			LogF("Singular cost in path");
			LogF("Path size %d, %.1f/%.1f",size,nom,denom);
		}
		return nom*100;
	}
	return nom/denom;
}

Vector3 Path::Begin() const
{
	return OperPos(Get(0));
}

Vector3 Path::End() const
{
	return OperPos(Get(Size()-1));
}

float Path::EndCost() const
{
	return Get(Size()-1)._cost;
}


LSError OperInfoResult::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("pos", _pos, 1))
	CHECK(ar.Serialize("cost", _cost, 1))
	return LSOK;
}

LSError Path::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("operIndex", _operIndex, 1, 1))
	CHECK(ar.Serialize("maxIndex", _maxIndex, 1))
	CHECK(ar.Serialize("searchTime", _searchTime, 1))
	CHECK(ar.Serialize("onRoad", _onRoad, 1, false))
	CHECK(ar.Serialize("Path", *((base *)this), 1))
	return LSOK;
}

class IndicesPathPoint : public NetworkMessageIndices
{
public:
	int pos;
	int cost;

	IndicesPathPoint();
	NetworkMessageIndices *Clone() const {return new IndicesPathPoint;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesPathPoint::IndicesPathPoint()
{
	pos = -1;
	cost = -1;
}

void IndicesPathPoint::Scan(NetworkMessageFormatBase *format)
{
	SCAN(pos)
	SCAN(cost)
}

NetworkMessageIndices *GetIndicesPathPoint() {return new IndicesPathPoint();}

NetworkMessageFormat &OperInfoResult::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("pos", NDTVector, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Path node position"));
	format.Add("cost", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Path node cost"));
	return format;
}

TMError OperInfoResult::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesPathPoint *>(ctx.GetIndices()))
	const IndicesPathPoint *indices = static_cast<const IndicesPathPoint *>(ctx.GetIndices());

	ITRANSF(pos)
	ITRANSF(cost)
	return TMOK;
}

class IndicesPath
{
public:
	int operIndex;
	int maxIndex;
	int searchTime;
	int onRoad;
	int path;

	IndicesPath();
	void Scan(NetworkMessageFormatBase *format);
};

IndicesPath::IndicesPath()
{
	operIndex = -1;
	maxIndex = -1;
	searchTime = -1;
	onRoad = -1;
	path = -1;
}

void IndicesPath::Scan(NetworkMessageFormatBase *format)
{
	SCAN(operIndex)
	SCAN(maxIndex)
	SCAN(searchTime)
	SCAN(onRoad)
	SCAN(path)
}

IndicesPath *GetIndicesPath() {return new IndicesPath();}
void DeleteIndicesPath(IndicesPath *path) {delete path;}

void ScanIndicesPath(IndicesPath *path, NetworkMessageFormatBase *format)
{
	path->Scan(format);
}

void Path::CreateFormat
(
	NetworkMessageFormat &format
)
{
	format.Add("operIndex", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 1), DOC_MSG("Currently executed path node index"));
	format.Add("maxIndex", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 1), DOC_MSG("Last valid path node index"));
	format.Add("searchTime", NDTTime, NCTNone, DEFVALUE(Time, Time(0)), DOC_MSG("Time, when path was planned"), ET_NOT_EQUAL, ERR_COEF_MODE);
	format.Add("onRoad", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Path on road"));
	format.Add("path", NDTObjectArray, NCTNone, DEFVALUE_MSG(NMTPathPoint), DOC_MSG("List of nodes"));
}

TMError Path::TransferMsg(NetworkMessageContext &ctx, IndicesPath *indices)
{
	ITRANSF(operIndex)
	ITRANSF(maxIndex)
	ITRANSF(searchTime)
	ITRANSF(onRoad)
	TMCHECK(ctx.IdxTransferArray(indices->path, *((base *)this)))
	return TMOK;
}

float Path::CalculateError(NetworkMessageContext &ctx, IndicesPath *indices)
{
	float error = 0;

	ICALCERR_NEQ(Time, searchTime, ERR_COEF_MODE)
	// ?? _operIndex

	return error;
}

#define DIAG 0

void Path::Optimize( VehicleWithAI *vehicle )
{
	Fail("Not used, optimization included in OperMap::ResultPath");
	return;
	
}

void Path::AvoidCollision( VehicleWithAI *vehicle )
{
	// change path to avoid collisions
	// TODO: implement
}

