// River Raid - sound module
// (C) 1996 - 2002, SUMA
#include "wpch.hpp"
#include "keyInput.hpp"

#include "global.hpp"
#include "soundDX8.hpp"
#include "paramFileExt.hpp"

#include "dikCodes.h"
#include <El/QStream/QBStream.hpp>
#include <El/Common/perfLog.hpp>

#ifdef _WIN32
#if !defined _XBOX
#include <dxerr8.h>
#include <mmsystem.h>
#else
#include <dsstdfx.h>
#endif
#endif

#define DIAG_VOL 0
#define DIAG_STREAM 0

#if defined _WIN32 && !_DISABLE_GUI && !defined _XBOX
#pragma comment(lib,"dxerr8")
#pragma comment(lib,"eaxguid")
#pragma comment(lib,"dsound")
#endif

#if defined _WIN32 && !defined _XBOX
#define GetPos2Flags DSBCAPS_GETCURRENTPOSITION2
#define LocHWFlags DSBCAPS_LOCHARDWARE
#define LocSWFlags DSBCAPS_LOCSOFTWARE
#else
#define GetPos2Flags 0
#define LocHWFlags 0
#define LocSWFlags 0
#endif

/*
// implement IStream interface
class BStream: public RefCount, public IStream, private QIFStreamB
{
	public:
	// IUnknown interface
	ULONG STDMETHODCALLTYPE AddRef() {return RefCount::AddRef();}
	ULONG STDMETHODCALLTYPE Release() {return RefCount::Release();}
	HRESULT STDMETHODCALLTYPE QueryInterface( REFIID iid, void ** ppvObject );

	// ISequentialStream 
	HRESULT STDMETHODCALLTYPE Read
	(
		void * pv, ULONG cb, ULONG * pcbRead
	);
	HRESULT STDMETHODCALLTYPE Write
	(
		void const* pv, ULONG cb, ULONG * pcbWritten
	);
  
	// IStream 
	HRESULT STDMETHODCALLTYPE Seek
	(
		LARGE_INTEGER dlibMove, DWORD dwOrigin, ULARGE_INTEGER * plibNewPosition
	);
	HRESULT STDMETHODCALLTYPE SetSize
	(
		ULARGE_INTEGER libNewSize                           
	);
	HRESULT STDMETHODCALLTYPE CopyTo
	(
		IStream * pstm, ULARGE_INTEGER cb,
		ULARGE_INTEGER * pcbRead, ULARGE_INTEGER * pcbWritten
	);
	HRESULT STDMETHODCALLTYPE Commit
	(
		DWORD grfCommitFlags 
	);
	HRESULT STDMETHODCALLTYPE Revert();
	HRESULT STDMETHODCALLTYPE LockRegion
	(
		ULARGE_INTEGER libOffset, ULARGE_INTEGER cb, DWORD dwLockType  
	);
	HRESULT STDMETHODCALLTYPE UnlockRegion
	(
		ULARGE_INTEGER libOffset, ULARGE_INTEGER cb, DWORD dwLockType    
	);
	HRESULT STDMETHODCALLTYPE Stat
	(
		STATSTG * pstatstg, DWORD grfStatFlag   
	);
	HRESULT STDMETHODCALLTYPE Clone( IStream ** ppstm );

	// construction / destruction
	BStream();
	BStream(const char *name);
	BStream(const BStream &src);
	~BStream();
	HRESULT Initialize( const char *name );
};

HRESULT BStream::QueryInterface( REFIID iid, void ** ppvObject )
{
	if (iid==IID_IUnknown)
	{
		AddRef();
		*ppvObject = (IUnknown *)this;
		return S_OK;
	}
	if (iid==IID_IStream)
	{
		AddRef();
		*ppvObject = (IStream *)this;
		return S_OK;
	}
	*ppvObject = NULL;
	return E_NOINTERFACE;
}

HRESULT BStream::Read
(
	void * pv, ULONG cb, ULONG * pcbRead
)
{
	//LogF("BStream %x Read %d from %d",this,cb,_readFrom);
	int toRead = rest();
	if (cb<=toRead)
	{
		read(pv,cb);
		if (pcbRead) *pcbRead = cb;
		return S_OK;
	}
	read(pv,toRead);
	if (pcbRead) *pcbRead = toRead;
	return S_FALSE;
}

HRESULT BStream::Write
(
	void const* pv, ULONG cb, ULONG * pcbWritten
)
{
	if (pcbWritten) *pcbWritten = 0;
	return STG_E_ACCESSDENIED;
}

HRESULT BStream::Seek
(
	LARGE_INTEGER dlibMove, DWORD dwOrigin, ULARGE_INTEGER * plibNewPosition
)
{
	//int oldPos = tellg();
	// calculate new position
	int newPos = 0;
	switch (dwOrigin)
	{
		case STREAM_SEEK_SET: newPos = dlibMove.QuadPart; break;
		case STREAM_SEEK_CUR: newPos = _readFrom+dlibMove.QuadPart; break;
		case STREAM_SEEK_END: newPos = _len+dlibMove.QuadPart; break;
		default: return STG_E_INVALIDFUNCTION;
	}
	HRESULT ret = S_OK;
	if (newPos<0)
	{
		ret = STG_E_INVALIDPOINTER;
		newPos = 0;
	}
	else if( newPos>_len)
	{
		ret = STG_E_INVALIDPOINTER;
		newPos = _len;
	}
	seekg(newPos,QIOS::beg);
	if (plibNewPosition) plibNewPosition->QuadPart = tellg();
	//LogF
	//(
	//	"BStream %x Seek %d:%d -> %d",
	//	this,dwOrigin,(int)dlibMove.QuadPart,tellg()
	//);
	return ret;
}

HRESULT BStream::SetSize
(
	ULARGE_INTEGER libNewSize                           
)
{
	return STG_E_ACCESSDENIED;
}

HRESULT BStream::CopyTo
(
	IStream * pstm, ULARGE_INTEGER cb,
	ULARGE_INTEGER * pcbRead, ULARGE_INTEGER * pcbWritten
)
{
	// TODO: implement if necessary
	return STG_E_ACCESSDENIED;
}

HRESULT BStream::Commit
(
	DWORD grfCommitFlags 
)
{
	return S_OK;
}

HRESULT BStream::Revert()
{
	return STG_E_ACCESSDENIED;
}


HRESULT BStream::LockRegion
(
	ULARGE_INTEGER libOffset, ULARGE_INTEGER cb, DWORD dwLockType  
)
{
	return STG_E_INVALIDFUNCTION;
}

HRESULT BStream::UnlockRegion
(
	ULARGE_INTEGER libOffset, ULARGE_INTEGER cb, DWORD dwLockType    
)
{
	return STG_E_INVALIDFUNCTION;
}
HRESULT BStream::Stat
(
	STATSTG * pstatstg, DWORD grfStatFlag   
)
{
	Fail("BStream::Stat");
	return STG_E_ACCESSDENIED;
}

HRESULT BStream::Clone( IStream ** ppstm )
{
	BStream *clone = new BStream(*this);
	clone->AddRef();
	*ppstm = clone;
	return S_OK;
}


BStream::BStream()
{
	//LogF(">>> Construct BStream at %x",this);
}

BStream::BStream(const char *name)
{
	//LogF(">>> Construct BStream at %x - %s",this,name);
	AutoOpen(name);
}

BStream::BStream(const BStream &src)
:QIFStreamB(src)
{
	//LogF(">>> Cloned    BStream at %x from %x",this,&src);
}

BStream::~BStream()
{
	//LogF("<<< Destruct BStream at %x",this);
}

HRESULT BStream::Initialize( const char *name )
{
	AutoOpen(name);
	if (fail()) return S_FALSE;
	return S_OK;
}

*/

// mixer - volume control

#if defined _WIN32 && !defined _XBOX
class AudioMixer
{
	bool _open;
	HMIXER _mixer;

	UINT _waveId;
	DWORD _oldVolume;

	public:
	AudioMixer();
	~AudioMixer();

	void SetWaveVolume( float vol );
	float GetWaveVolume() const;
};

AudioMixer::AudioMixer()
{
	_open = false;
	MMRESULT mr = mixerOpen
	(
		&_mixer,0,NULL,NULL,MIXER_OBJECTF_MIXER
	);
	if (mr!=MMSYSERR_NOERROR)
	{
		return;
	}
	_open = true;
	waveOutGetVolume((HWAVEOUT)WAVE_MAPPER,&_oldVolume);

}

AudioMixer::~AudioMixer()
{
	if (_open)
	{
		waveOutSetVolume((HWAVEOUT)WAVE_MAPPER,_oldVolume);
		mixerClose(_mixer);
	}
}

float AudioMixer::GetWaveVolume() const
{
	// note: 
	DWORD dVol;
	waveOutGetVolume((HWAVEOUT)WAVE_MAPPER,&dVol);
	DWORD lVol = (dVol&0xffff);
	DWORD rVol = (dVol>>16)&0xffff;
	int avgVol = (rVol+lVol)/2;
	return avgVol*(1.0/0xffff);
}
void AudioMixer::SetWaveVolume( float vol )
{
	int iVol = toInt(0xffff*vol);
	saturate(iVol,0,0xffff);
	DWORD dVol=(DWORD)iVol|(((DWORD)iVol)<<16);
	waveOutSetVolume((HWAVEOUT)WAVE_MAPPER,dVol);
	//LogF("Set volume %x",dVol);
}
#endif

#define lenof(a) ( sizeof(a)/sizeof(*(a)) )


#define DO_PERF 0
#if DO_PERF
#include "perfLog.hpp"
#endif

//#include "engine.hpp"


void Wave8::DoConstruct()
{
	_type=Free;
	_soundSys=NULL;
	_size=0;         // Size of data.
	_frequency=0;      // sampling rate
	_nChannel=0;        // number of channels
	_sSize=0;           // number of bytes per sample
	_playing=false;       // Is this one playing?
	_wantPlaying = false;
	_loaded=false;
	_loadError=false;
	_only2DEnabled=false;
	_looping=true;
	_terminated=false;

	_volume=1.0;
	_accomodation=1.0;
	_volumeAdjust = 1;
	_enableAccomodation=true;

	_stopTreshold = FLT_MAX; // never stop

	_volumeSet=0;
	_frequencySet=0;
	_curPosition=0;
	_position=VZero;
	_velocity=VZero;
}

/*
WaveGlobal8::WaveGlobal8()
{
	_queued=false;
}
*/

WaveGlobal8::~WaveGlobal8()
{
}

#define DIAG_LOAD 0

WaveGlobal8::WaveGlobal8( SoundSystem8 *sSys, float delay )
:Wave8(sSys,delay)
{
	_queued=false;
}

WaveGlobal8::WaveGlobal8( SoundSystem8 *sSys, RString name, bool is3D )
:Wave8(sSys,name,is3D)
{
	_queued=false;
}

DEFINE_FAST_ALLOCATOR(WaveGlobal8)

/*
WaveGlobal8::WaveGlobal8( const Wave8 &wave )
:Wave8(wave)
{
	_queued=false;
}
*/

void WaveGlobal8::AdvanceQueue()
{
	if( !_queue ) return;
	if( _terminated )
	{
		if( _queue )
		{
			_queue->_queued=false;
			_queue->DoPlay();
			_queued=true;
			//LogF("Sound %s activated",(const char *)_queue->Name());
		}
		OnTerminateOnce();
		return;
	}
}

void WaveGlobal8::Play()
{
	if( _queued ) return;

	// check termination
	IsTerminated();

	AdvanceQueue();
	
	base::Play();
}

void WaveGlobal8::Skip( float deltaT )
{
	if( _queued ) return;

	//if( _loaded )
	//{
		// check termination
		IsTerminated();

		AdvanceQueue();
	//}

	base::Skip(deltaT);
}

void WaveGlobal8::Advance( float deltaT )
{
	if( _playing || _wantPlaying && _curPosition>=0) return;
	Skip(deltaT);
}

void WaveGlobal8::Queue( AbstractWave *wave,int repeat )
{
	// TODO: use DMusic primary segment queing
	WaveGlobal8 *after=static_cast<WaveGlobal8 *>(wave);
	//Assert( after->_repeat>0 );
	Assert( repeat>0 );
	while( after->_queue ) after=after->_queue;
	after->_queue=this;
	/*
	LogF
	(
		"Queue %s after %s",
		(const char *)Name(),(const char *)after->Name()
	);
	*/
	_queued=true;
	Repeat(repeat);
}

bool WaveGlobal8::IsTerminated()
{
	if( !base::IsTerminated() ) return false;
	for( WaveGlobal8 *next = this; next; next=next->_queue )
	{
		if( !next->_terminated ) return false;
	}
	OnTerminateOnce();
	return true;
}

#define DEFERRED 1 // TODO: use deferred mode

static bool DSVerifyF( const char *file, int line, HRESULT err )
{
	if( err==DS_OK ) return true;
	const char *message="";
	switch( err )
	{
		#ifndef _XBOX
		case DSERR_BUFFERLOST:
			message="Buffer lost";
		break;
		case DSERR_PRIOLEVELNEEDED:
			message="Prior level needed";
		break;
		case DSERR_INVALIDPARAM:
			message="Invalid Param";
		break;
		#endif
		case DSERR_INVALIDCALL:
			message="Invalid Call";
		break;
		case DSERR_CONTROLUNAVAIL:
			message="Control Unavailable";
		break;
		case DSERR_GENERIC:
			message="Generic";
		break;
		default:
			message="Unknown";
		break;
	}
	//LogF("%s(%d) : DirectSound error %s",file,line,message);
	//Fail("Direct sound error");
	return false;
}

#define DoDSVerify(x) DSVerifyF(__FILE__,__LINE__,x)
#define DoDSAssert(x) DSVerifyF(__FILE__,__LINE__,x)

#if !_RELEASE
	#define DSVerify(x) DoDSVerify(x)
	#define DSAssert(x) DoDSAssert(x)
#else
	#define DSVerify(x) (x)
	#define DSAssert(x) (x)
#endif

WaveBuffers8::WaveBuffers8()
:_position(VZero),_velocity(VZero)
{
	_3DEnabled = true;
}

WaveBuffers8::~WaveBuffers8()
{
}

void WaveBuffers8::Reset( SoundSystem8 *sys )
{
	// TODO: force setting all parameters
	_volumeSet = 0;

	_position = VZero;
	_velocity = VZero;

	_volume = 1; // volume used for 3D sound
	_accomodation = 1; // current ear accomodation setting
	_volumeAdjust = sys->_volumeAdjustEffect;

	_kind = WaveEffect;
	_3DEnabled = true;
}


void WaveBuffers8::SetVolumeDb( LONG volumeDb )
{
	if( !_playing )
	{
		_volumeSet=volumeDb;
		return;
	}
	if( volumeDb!=_volumeSet )
	{
		if( _buffer )
		{
			DSVerify( _buffer->SetVolume(volumeDb) );
			#if DIAG_VOL
			//LogF("PP: SetVolume %s %d",(const char *)Name(),volumeDb);
			LogF("PP: SetVolume %x %d",this,volumeDb);
			#endif
		}
		else
		{
			#if DIAG_VOL
			//LogF("PresetVolume %s %d",(const char *)Name(),volumeDb);
			LogF("PresetVolume %x %d",this,volumeDb);
			#endif
		}
		_volumeSet=volumeDb;
	}
}

/*
#pragma warning(disable:4035)

static __int64 Rdtsc()
{
	__asm rdtsc;
}
*/



void Wave8::Unload()
{
	if( !_loaded ) return;

	Assert( GSoundsys==_soundSys );
	Assert( _soundSys->_soundReady );

	#if 1
		if (_buffer)
		{
			// TODO: we may store also streaming buffer
			// so that thay can be reused
			if (!_stream)
			{
				_soundSys->StoreToCache(this);
			}
		}
	#endif

	#if DO_PERF
	if (_buffer)
	{
		ADD_COUNTER(wavUL,1);
	}
	#endif

	_buffer3D.Free();
	_buffer.Free();
	_loaded=false;
	_stream.Free();

	_counters[_type]--;
	_type=Free;
	ReportStats();

	//LogF("%s unloaded",(const char *)Name());
}


void Wave8::DoDestruct()
{
	Stop();
	Unload();
}


int Wave8::_counters[NTypes];

void Wave8::AddStats( const char *name )
{
	if (!_buffer) return;
	#ifndef _XBOX
	DSBCAPS caps;
	caps.dwSize=sizeof(caps);
	_buffer->GetCaps(&caps);
	if( caps.dwFlags&DSBCAPS_LOCHARDWARE )
	{
		_type= _only2DEnabled ? Hw2D : Hw3D;
		//LogF("%s %s: Hw",name,(const char *)Name());
	}
	else
	{
		_type=Sw;
		//LogF("%s %s: Sw",name,(const char *)Name());
	}
	#else
		_type= _only2DEnabled ? Hw2D : Hw3D;
	#endif

	_counters[_type]++;
	ReportStats();
}

void Wave8::ReportStats()
{
	/*
	char buf[256];
	strcpy(buf,"Snd");
	for( int i=1; i<NTypes; i++ )
	{
		sprintf(buf+strlen(buf)," %d",_counters[i]);
	}
	GEngine->ShowMessage(1000,buf);
	*/
}

int Wave8::TotalAllocated()
{
	int total=0;
	for( int i=1; i<NTypes; i++ ) total+=_counters[i];
	return total;
}

void Wave8::LoadHeader()
{
	if( _sSize>0 ) return;

	// only header is needed - whole files are cached
	#if DIAG_LOAD>=2
		LogF("Wave8 %s: Load file header",(const char *)Name());
	#endif
	Ref<WaveStream> stream = SoundLoadFile(Name());

	if( !stream )
	{
		LogF("Cannot load sound %s", (const char *)Name());
		return;
	}
	
	WAVEFORMATEX format;
	stream->GetFormat(format);
	_frequency = format.nSamplesPerSec;      // sampling rate
	_nChannel = format.nChannels;        // number of channels
	_sSize = format.nBlockAlign;           // number of bytes per sample
	_size = stream->GetUncompressedSize();
	#if DIAG_STREAM
		if (_size>StreamingBufferSize)
		{
			LogF("%s: Open stream, size %d)",(const char *)Name(),_size);
		}
	#endif
}

#define DISABLE_HW_ACCEL 0
#define ENABLE_VOICEMAN 1

void Wave8::Load()
{

	if( _loadError ) return;
	if( _loaded ) return;
	if (Name().GetLength()<=0)
	{
		_frequency = 1000; // time given in ms
	}

	// scan all channels
	// if possible make some channels free (release cache)
	_soundSys->ReserveCache(1-_only2DEnabled,_only2DEnabled);

	// search all playing waves
	_soundSys->_waves.Compact();
	// first try to reuse any cached data
	if( _soundSys->LoadFromCache(this) )
	{
		#if DIAG_LOAD>=2
		LogF("Cache reused %s",(const char *)Name());
		#endif
		AddStats("Cache");

		_loaded=true;
		_terminated=false;
		_playing=false;
		_wantPlaying=false;
		_streamPos=0;
		_lastLoopSet = false;
		_lastLoopVerified = false;
		return;
	}

	// try to duplicate some existing sound buffer
	for( int i=0; i<_soundSys->_waves.Size(); i++ )
	{
		Wave8 *wave=_soundSys->_waves[i];
		if( !wave->Loaded() ) continue;
		if( wave==this ) continue;
		if( !strcmpi(wave->Name(),Name()) )
		{
			if( Duplicate(*wave) ) return;
		}
	}


	//LogF("Wave8 %s loaded",(const char *)Name());
	DoDestruct();

	_loadError=true;
	_loaded=true;
	_playing=false;
	_wantPlaying=false;
	
	Ref<WaveStream> stream = SoundLoadFile(Name());

	if( !stream )
	{
		RptF("Cannot load sound %s", (const char *)Name());
		_loadError = true;
		return;
	}
	
	WAVEFORMATEX format;
	stream->GetFormat(format);
	_frequency = format.nSamplesPerSec;      // sampling rate
	_nChannel = format.nChannels;        // number of channels
	_sSize = format.nBlockAlign;           // number of bytes per sample
	_size = stream->GetUncompressedSize();

	// Set up the direct sound buffer. 
	DSBUFFERDESC dsbd;
	memset(&dsbd, 0, sizeof(DSBUFFERDESC));
	dsbd.dwSize = sizeof(DSBUFFERDESC);
	dsbd.dwFlags = DSBCAPS_CTRLFREQUENCY|DSBCAPS_CTRLVOLUME;
	dsbd.dwFlags |= GetPos2Flags;
	if (_soundSys->_hwEnabled)
	{
		#if !ENABLE_VOICEMAN
		dsbd.dwFlags |= LocHWFlags;
		#else
		dsbd.dwFlags |= DSBCAPS_LOCDEFER;
		#endif
	}
	else
	{
		dsbd.dwFlags |= LocSWFlags;
	}
	if( !_only2DEnabled )
	{
		#ifndef _XBOX
		dsbd.dwFlags|=DSBCAPS_CTRL3D|DSBCAPS_MUTE3DATMAXDISTANCE;
		dsbd.guid3DAlgorithm=DS3DALG_DEFAULT; // surround/quadro if available
		#endif
	}


	// stream if file is too large for static implementation
	bool streaming = _size>StreamingBufferSize;

	if (streaming)
	{
		dsbd.dwBufferBytes = StreamingBufferSize;
	}
	else
	{
		dsbd.dwBufferBytes = _size;
	}

	dsbd.lpwfxFormat = &format;
	HRESULT hr;
	ADD_COUNTER(dsCSB,1);
	IDirectSound *ds = _soundSys->DirectSound();
	if (ds)
	{
		hr = ds->CreateSoundBuffer(&dsbd,_buffer.Init(),NULL);
		if (hr!=DS_OK)
		{
			RptF("Cannot create sound buffer %s: %x", (const char *)Name(),hr);
			#ifndef _XBOX
			DXTRACE_ERR_NOMSGBOX("CreateSoundBuffer",hr);
			#endif
			_loadError = true;
			return;
		}
	}

	#if DIAG_LOAD
		LogF("Wave8 %s: CreateSoundBuffer",(const char *)Name());
	#endif

	#ifndef _XBOX
	if( ds &&!_only2DEnabled )
	{
		if( _buffer->QueryInterface(IID_IDirectSound3DBuffer,(void **)_buffer3D.Init()) )
		{
			RptF("Cannot create 3D sound buffer %s", (const char *)Name());
			_loadError = true;
			_buffer.Free();
			return;
		}
	}
	#endif
	
	{
		
		// note: at this point streaming / static implementation
		// is different
		// static decompresses whole buffer once, while streaming keeps
		// memory mapped file buffer and reads from it sequentially
		if (streaming)
		{
			// fill buffer with start of streaming data
			_stream = stream;
			_streamPos = 0;
			_lastLoopSet = false;
			_lastLoopVerified = false;
			//LogF("Reset stream %s",(const char *)Name());
		}
		else
		{
			if (ds)
			{
				BYTE *pbData;
				// Ok, lock the sucker down, and copy the memory to it.
				DWORD dwLength=0;
				// caution: following line caused VisualC++ debugger to hang
				// do not step through it - use breakpoint instead
				if( _buffer->Lock(0,_size,(void **)&pbData,&dwLength,NULL,0,0L)!=DS_OK )
				{
					RptF("Cannot lock sound buffer");
				}
				else
				{
					int read = stream->GetDataClipped(pbData,0,dwLength);
					if (read<dwLength)
					{
						LogF("GetData returned %d instead of %d",read,dwLength);
						memset(pbData+read,0,dwLength);
					}
					// Ok, now unlock the buffer, we don't need it anymore.
					if( _buffer->Unlock(pbData,dwLength,NULL,0)!=DS_OK )
					{
						RptF("Cannot unlock sound buffer");
					}
				}
			}
		}
	}
	
	//_soundSys->LogChannels("Load");
	AddStats("Load");

	// silent sound

	_loadError=false;
}

inline int StreamBufferRoundDistance( int a, int b )
{
	int d = a-b;
	if (d<0) d += StreamingBufferSize;
	return d;
}

/*!
\patch 1.43 Date 1/31/2002 by Ondra
- Fixed: Support for looping streaming sounds.
\patch 1.85 Date 9/12/2002 by Ondra
- Fixed: Streaming sounds might loop infinitelly on some hardware.
*/

void Wave8::FillStream()
{
	// get write and play cursors
	if (!_buffer) return;
	DWORD play,write;
	_buffer->GetCurrentPosition(&play,&write);

	//write = play;

	// we may write anywhere between write and play, not between play and write

	// check what has been actually played

	int toWrite = StreamBufferRoundDistance(play,_writePos);
	// wait until we may write enough data
	if (toWrite<StreamingPartSize) return;

	int writeToPlayDist = StreamBufferRoundDistance(write,play);
	int maxWrite = StreamingBufferSize - writeToPlayDist;

	/*
	LogF
	(
		"Sound write: write %d, play %d, toWrite %d, writeToPlayDist %d (max %d,%d)",
		write,play,toWrite,writeToPlayDist, maxWrite, StreamingBufferSize
	);
	*/
	
	// I think this should not be possible
	if (toWrite>maxWrite)
	{
		LogF
		(
			"Sound overflow?: write %d, play %d, toWrite %d, writeToPlayDist %d (max %d,%d)",
			write,play,toWrite,writeToPlayDist, maxWrite, StreamingBufferSize
		);
		toWrite = maxWrite;
	}

	// StreamingPartSize must be power of two
	// round to nearest smaller multiply of StreamingPartSize
	toWrite &= ~(StreamingPartSize-1);
	if (toWrite<StreamingPartSize) return;

	void *data,*data2;
	DWORD size=0,size2=0;

	#if DIAG_STREAM
		LogF
		(
			"Filling stream %d..%d, play pos %d, status %d",
			_writePos,_writePos+toWrite,play,status&DSBSTATUS_PLAYING
		);
	#endif
	HRESULT hr = _buffer->Lock(_writePos,toWrite,&data,&size,&data2,&size2,0);
	if (SUCCEEDED(hr))
	{
		//LogF("Streaming data %d:%d to %d",_streamPos,size,_writePos);

		int actualWrite = _writePos;
		#if DIAG_STREAM
		LogF("%s: Load stream %d:%d (of %d)",(const char *)Name(),_streamPos,size,_size);
		#endif
		if (_looping)
		{
			actualWrite += _stream->GetDataLooped(data,_streamPos,size);
		}
		else
		{
			actualWrite += _stream->GetDataClipped(data,_streamPos,size);
		}
		_streamPos += size;
		_writePos += size;
		if (size2>0)
		{
			#if DIAG_STREAM
			LogF("%s: Load stream %d:%d",(const char *)Name(),_streamPos,size2);
			#endif
			if (_looping)
			{
				actualWrite += _stream->GetDataLooped(data2,_streamPos,size2);
			}
			else
			{
				actualWrite += _stream->GetDataClipped(data2,_streamPos,size2);
			}
			_streamPos += size2;
			_writePos += size2;

			//LogF("Streaming data2 %d",size);
		}

		if (_writePos>=StreamingBufferSize) _writePos -= StreamingBufferSize;
		if (actualWrite>=StreamingBufferSize) actualWrite -= StreamingBufferSize;


		_buffer->Unlock(data,size,data2,size2);


		if (_streamPos>=_size)
		{
			if (!_looping)
			{
				if (!_lastLoopSet)
				{
					_lastLoopSet = true;
					// record position where last actual data were written
					_endPos = actualWrite;
				}
				else if (!_lastLoopVerified)
				{
					// wait until playback gets far enough from _endPos
					// so that we are able to check when _endPos is actually reached
					// to be sure we can 
					if (StreamBufferRoundDistance(play,_endPos)>StreamingBufferSize/2)
					{
						_lastLoopVerified = true;
					}
				}
				else
				{
					// if we are close after the _endPos, it means playback
					// has finished last round and may be safely terminated
					if (StreamBufferRoundDistance(play,_endPos)<StreamingBufferSize/4)
					{
						// end of buffer reached - stop
						_buffer->Stop();
					}
				}
			}
			else
			{
				_streamPos -= _size;
			}
		}
	}
}

#if 1

#define STAT_PLAY(x)
#define STAT_TERM(x)
#define STAT_STOP(x)
#define STAT_GETCHANNELS() 0

#else

static int NPlaying = 0;

#define STAT_PLAY(x) \
	NPlaying++; \
	GlobalShowMessage(100,"Playing %d", NPlaying); \
	LogF("%2d: Play %x:%s",NPlaying,x,(const char *)(x)->Name())

#define STAT_TERM(x) \
	LogF("   Term %x:%s",x,(const char *)(x)->Name())

#define STAT_STOP(x) \
	NPlaying--; \
	GlobalShowMessage(100,"Playing %d", NPlaying); \
	LogF("%2d: Stop %x:%s",NPlaying,x,(const char *)(x)->Name())

#define STAT_GETCHANNELS() NPlaying

#endif

void Wave8::Play()
{
	// set flag we want to play
	// commit will play it after listener position is set
	if( _terminated ) return;
	_wantPlaying = true;
}

void Wave8::DoPlay()
{
	if( _terminated )
	{
		return;
	}

	if (!_soundSys->DirectSound())
	{
		return;
	}

	if( _playing )
	{
		// if it is streaming, fill with data as necessary
		if (_stream) FillStream();
		return;
	}

	if (!_posValid && _buffer3D)
	{
		// position not set yet - cannot start playing
		return;
	}
	if( _curPosition>=0 )
	{
		//LogF("Wave %s, pos %d",(const char *)Name(),_curPosition);
		Load();

		if( _curPosition>=_size )
		{
			if( _looping && _size>0 )
			{	
				while( _curPosition>=_size ) _curPosition-=_size;
			}
			else
			{
				_terminated=true;
				_curPosition=_size;
			}
		}
		else
		{
			#if DIAG_VOL
			LogF("SP: SetVolume %s %d",(const char *)Name(),_volumeSet);
			#endif
			//LogF("Start play %s, vol %d",(const char *)Name(),_volumeSet);
			if (_buffer)
			{
				/**/
				DSVerify( _buffer->SetVolume(_volumeSet) );
				DSVerify( _buffer->SetFrequency(_frequencySet) );
				/**/
			}
			
			//LogF("SetPos %s",(const char *)Name());
			DoSetPosition(true,_soundSys->_hwEnabled);

			#if 0
			if (_buffer3D)
			{
				DS3DBUFFER pars;
				pars.dwSize=sizeof(pars);
				_buffer3D->GetAllParameters(&pars);

				LONG vol;
				_buffer->GetVolume(&vol);
				LogF
				(
					"Starting\n"
					"    mode %d\n"
					"    pos (%.1f,%.1f,%.1f) (%.1f,%.1f,%.1f)\n"
					"    vel (%.1f,%.1f,%.1f) (%.1f,%.1f,%.1f)\n"
					"    vol %d - %d (%.3f)\n"
					"    minDist %g",
					pars.dwMode,
					_position[0],_position[1],_position[2],
					pars.vPosition.x,pars.vPosition.y,pars.vPosition.z,
					_velocity[0],_velocity[1],_velocity[2],
					pars.vVelocity.x,pars.vVelocity.y,pars.vVelocity.z,
					_volumeSet/100,vol/100,_volume,
					pars.flMinDistance
				);
			}
			#endif
			
			if (_buffer)
			{
				if (!_stream)
				{
					#if 0
					if (_curPosition>0)
					{
						LogF("%s: set curpos to %d",(const char *)Name(),_curPosition);
					}
					#endif
					DSVerify( _buffer->SetCurrentPosition(_curPosition) );

					if (_looping) DSVerify( _buffer->Play(0,0,DSBPLAY_LOOPING) );
					else DSVerify( _buffer->Play(0,0,0) );
				}
				else
				{
					// start play-back
					DSVerify( _buffer->SetCurrentPosition(0) );
					// perform initial fill on the stream

					_streamPos = _curPosition;
					//LogF("Set stream %s start to %d",(const char *)Name(),_streamPos);
					FillStreamInit();

					DSVerify( _buffer->Play(0,0,DSBPLAY_LOOPING) );
				}

			}
		}
		_playing=true;
		STAT_PLAY(this);
		OnPlayOnce();
	}
}

void Wave8::Repeat(int repeat)
{
	if (repeat<2)
	{
		_looping=false;
	}
	else
	{
		_looping = true;
		if( _playing )
		{
			if (_buffer)
			{
				DSVerify( _buffer->Play(0,0,DSBPLAY_LOOPING) );
			}
		}
	}
}
void Wave8::Restart()
{
	Stop();
	_terminated=false;
	if (_playing)
	{
		LogF("%x: %s: Still playing",this,(const char *)Name());
	}
	//_playing=false;
	//STAT_STOP(this); // restarting - reset stats
	_curPosition=0;
	if (_stream)
	{
		_streamPos = 0;
	}
}

/*!
\patch 1.01 Date 15/6/2001 by Ondra.
 - Fixed: playMusic [name,t] with t>96
*/

void Wave8::Skip( float deltaT )
{
	// note: pre-start delay is used to implement speed of sound
	// this gives bad results if listener speed is high

	if (_playing)
	{
		StoreCurrentPosition();
	}

	// FIX
	int skip=toLargeInt(deltaT*_frequency)*_sSize;
	AdvanceCurrentPosition(skip);
	while( _curPosition>=(int)_size )
	{
		if (!_looping || _size<=0)
		{
			Stop();
			_terminated=true;
			_curPosition=_size;
			break;
		}
		_curPosition-=_size;
	}
	if( _playing && !_terminated )
	{
		DSVerify( _buffer->SetCurrentPosition(_curPosition) );
		if (_stream)
		{
			LogF("Cannot advance playing stream %s",(const char *)Name());
		}
	}
}

void Wave8::Advance( float deltaT )
{
	if( !_playing ) Skip(deltaT);
}

bool Wave8::IsStopped() {return !_playing && !_wantPlaying;}
bool Wave8::IsWaiting() {return _curPosition<0;}

RString Wave8::GetDiagText()
{
	RString ret;
	if (_curPosition<0)
	{
		float delay = float(-_curPosition)/(_sSize*_frequency);
		char buf[256];
		sprintf(buf,"Delay %.3f ",delay);
		ret = buf;
	}
	if( IsStopped() ) return ret+RString("Stopped");
	else if( IsTerminated() ) return ret+RString("Terminated");
	else if( IsWaiting() ) return ret+RString("Waiting");
	else 
	{
		if( !_loaded ) return ret+RString("Not loaded");
		if( !_buffer ) return ret+RString("No buffer");
		// TODO: more diags
		if( !_buffer3D )
		{
			char buf[512];
			sprintf
			(
				buf,
				"Playing 2D\n"
				"    vol %d (%.3f, adj %.3f), kind %d",
				_volumeSet/100,_volume,_volumeAdjust,_kind
			);
			return ret+RString(buf);
		}
		else
		{

			char buf[512];
			sprintf
			(
				buf,
				"Playing\n"
				"    pos (%.1f,%.1f,%.1f)\n"
				"    vel (%.1f,%.1f,%.1f)\n"
				"    vol %d - (%.3f)",
				_position[0],_position[1],_position[2],
				_velocity[0],_velocity[1],_velocity[2],
				_volumeSet/100,_volume
			);
			return ret+RString(buf);
		}
	}
}

float Wave8::GetLength() const
{
	// get wave length in sec
	if (_sSize<=0)
	{
		RptF("%s: sSize %d",(const char *)Name(),_sSize);
		return 1;
	}
	if (_frequency<=0)
	{
		RptF("%s: frequency %d",(const char *)Name(),_frequency);
		return 1;
	}
	return float(_size/_sSize)/_frequency;
}

bool Wave8::IsTerminated()
{
	if (!_terminated)
	{
		if (_playing)
		{
			if (_buffer)
			{
				DWORD status;
				DSVerify( _buffer->GetStatus(&status) );
				if( status&DSBSTATUS_PLAYING ) return false;
				DWORD temp;
				DSVerify( _buffer->GetCurrentPosition(&temp,NULL) );
				_curPosition=temp;
			}
			STAT_TERM(this);
			_terminated=true;
		}
		else if (_loadError)
		{
			// sound that cannot be loaded is considered terminated
			_terminated = true;
		}
	}
	return _terminated;
}

void Wave8::FillStreamInit()
{
	// fill buffer with start of streaming data
	DWORD size=0;
	void *data;
	int initSize = StreamingBufferSize-2*StreamingPartSize;
	if( _buffer->Lock(0,initSize,&data,&size,NULL,0,0L)!=DS_OK )
	{
		RptF("Cannot lock sound buffer");
	}
	else
	{
		//LogF("Init fill from %d (%d)",_streamPos,size);

		#if DIAG_STREAM
		LogF
		(
			"%s: Init stream %d:%d of %d",
			(const char *)Name(),_streamPos,size,StreamingBufferSize
		);
		#endif
		_streamPos += _stream->GetDataClipped(data,_streamPos,size);
		_writePos = size;
		if (_writePos>=StreamingBufferSize) _writePos -= StreamingBufferSize;

		if( _buffer->Unlock(data,size,NULL,0)!=DS_OK )
		{
			RptF("Cannot unlock sound buffer");
		}

	}
}

void Wave8::StoreCurrentPosition()
{
	// store current position so that is is available when playing the buffer
	if (!_buffer) return;

	DWORD temp;
	DSVerify( _buffer->GetCurrentPosition(&temp,NULL) );
	_curPosition=temp;
	// for streaming audio store all neccessary information
	if (_stream)
	{
		// check where in the stream is current position
		int leftInBuffer = StreamBufferRoundDistance(_curPosition,_writePos);
		_streamPos -= leftInBuffer;
		_curPosition = _streamPos;
		_streamPos = 0;

		//LogF("Stored stream position %d (%d ahead, %d)",_curPosition,leftInBuffer,leftInBuffer2);
	}
}

/*!
\patch 1.27 Date 10/11/2001 by Ondra
- Fixed: speed of sound delay caused some sounds to be decayed (since 1.26).
*/
void Wave8::AdvanceCurrentPosition(int skip)
{
	if (!_stream)
	{
		int oldPos = _curPosition;
		_curPosition += skip;
		if( _curPosition<0 )
		{
			Stop();
		}
		else
		{
			
			if (oldPos<0) _curPosition = 0;
		}
	}
	else
	{
		//LogF("Advance stream %s",(const char *)Name());
		if (_playing)
		{
			LogF("Cannot advance in playing stream %s",(const char *)Name());
		}
		// note: only one of _streamPos and _curPosition should be non-zero
		Assert (_streamPos==0 || _curPosition==0);
		int newPos = _streamPos + _curPosition + skip;
		if (newPos<0)
		{
			Stop();
			_curPosition = newPos;
			//LogF("Hard rewind %s to %d",(const char *)Name(),newPos);
		}
		else
		{
			_curPosition = newPos;
			//LogF("Soft rewind %s to %d",(const char *)Name(),newPos);
		}
	}
}


void Wave8::DoStop()
{
	_wantPlaying = false;
	if( !_loaded )
	{
		if (_playing)
		{
			LogF("%x,%s: Playing, not loaded",this,(const char *)Name());
		}
		return;
	}
	if( !_playing ) return;
	if (_buffer)
	{
		DSVerify( _buffer->Stop() );
		StoreCurrentPosition(); // 
		_writePos = 0;
		//if (_stream) LogF("Stopped stream %s",(const char *)Name());
	}
	_playing=false;
	STAT_STOP(this);
}

void Wave8::Stop()
{
	DoStop();
	Unload();
}
void Wave8::LastLoop()
{
	if( !_loaded ) return;
	if( !_looping ) return;
	if( _playing )
	{
		if (!_stream)
		{
			if (_buffer)
			{
				DSVerify( _buffer->Play(0,0,0) );
			}
		}
	}
	_looping=false;
}

void Wave8::PlayUntilStopValue( float time )
{
	// play until some time
	_stopTreshold = time;
	_looping = true;
	if( !_loaded ) return;
	if( !_looping ) return;
	if( _playing )
	{
		if (_buffer)
		{
			DSVerify( _buffer->Play(0,0,DSBPLAY_LOOPING) );
		}
	}
}
void Wave8::SetStopValue( float time )
{
	//LogF("%s: SetStopValue %.2f, %.2f",(const char *)Name(),time,_stopTreshold);
	if (time>_stopTreshold)
	{
		_terminated = true;
		Stop();
	}
}

// Volume is 0 to 1
// frequency is multiplied by original frequency

const int DBOffset=0;

static LONG float2db( float val )
{
	// 20 dB is 10x
	// val = 10^(dbi/20)
	// log10(val)=dbi/20
	// log10(val)*20=dbi;
	if( val<=0 ) return DSBVOLUME_MIN;
	int ret=toIntFloor(log10(val)*2000)+DBOffset;
	saturate(ret,DSBVOLUME_MIN,DSBVOLUME_MAX);
	//Log("float2db %f %d",val,ret);
	return ret;
}

/*
\patch 1.47 Date 3/11/2002 by Ondra
- Workaround: Distance falloff limits do not work properly on SB Live and Audigy cards
due to driver or hardware bug. As a result, footsteps and other quiet sounds were
heard from large distances. Alternate method of distance limits handling is used
on HW accelerated cards.
*/

void WaveBuffers8::DoSetPosition( bool immediate, bool maxDistanceFix )
{
	if( _only2DEnabled ) return;	
	if( !_buffer3D ) return;

	Vector3Val pos=_position;
	Vector3Val vel=_velocity;
	
	DS3DBUFFER pars;
	pars.dwSize=sizeof(pars);
	pars.vPosition.x=pos[0],pars.vPosition.y=pos[1],pars.vPosition.z=pos[2];
	pars.vVelocity.x=vel[0],pars.vVelocity.y=vel[1],pars.vVelocity.z=vel[2];
	//pars.vVelocity.x=0,pars.vVelocity.y=0,pars.vVelocity.z=0;
	pars.dwInsideConeAngle=360;
	pars.dwOutsideConeAngle=360;
	pars.vConeOrientation.x=pars.vConeOrientation.y=0,pars.vConeOrientation.z=1;
	pars.lConeOutsideVolume=0;

	pars.flMinDistance=floatMax
	(
		sqrt(_volume*_accomodation)*30,1e-6
	);
	if (maxDistanceFix)
	{
		pars.flMaxDistance=DS3D_DEFAULTMAXDISTANCE;
		//pars.flMaxDistance=pars.flMinDistance*10000;
	}
	else
	{
		pars.flMaxDistance=pars.flMinDistance*100;
	}
	pars.dwMode=_3DEnabled ? DS3DMODE_NORMAL : DS3DMODE_DISABLE;

	//LogF("  %x: set mode %d (%s)",this,pars.dwMode,immediate ? "Imm" : "Defer");
	//LogF("  min %.4f,max %.3f",pars.flMinDistance,pars.flMaxDistance);

	#if DEFERRED
	if( !immediate )
	{
		DSVerify( _buffer3D->SetAllParameters(&pars,DS3D_DEFERRED) );
	}
	else
	#endif
	{
		// dx8 bug: deferred setting override immediate
		// at least for dwMode
		_buffer3D->SetMode(pars.dwMode,DS3D_DEFERRED);
		DSVerify( _buffer3D->SetAllParameters(&pars,DS3D_IMMEDIATE) );
	}
}

bool Wave8::Get3D() const
{
	return WaveBuffers8::Get3D();
}

void Wave8::Set3D(bool is3D)
{
	if (is3D!=_3DEnabled)
	{
		//LogF("Change mode of %s to %d",(const char *)Name(),is3D);
		// stop buffer to avoid clicks during switching
		DoStop();
		WaveBuffers8::Set3D(is3D);
	}
}

void Wave8::SetPosition( Vector3Par pos, Vector3Par vel, bool immediate )
{
	_position=pos;
	_velocity=vel;
	// IDirectSound3DBuffer implementation
	_posValid = true;

	if( !_playing ) return;

	//LogF("SetPos %s",(const char *)Name());
	DoSetPosition(immediate,_soundSys->_hwEnabled);
}



bool WaveBuffers8::GetMuted() const
{
	return _volumeSet<=-10000;
}

void WaveBuffers8::UpdateVolume()
{
	if (_only2DEnabled || !_3DEnabled)
	{
		// some problem with setting parameters
		// convert _volume do Db
		float vol = _volume*_accomodation*_volumeAdjust;
		if (!_only2DEnabled) vol *= 100;
		SetVolumeDb(float2db(vol));
	}
	else
	{
		// instead of volume use 3D MinDistance factor
		SetVolumeDb(float2db(_volumeAdjust));
	}
}

void WaveBuffers8::SetAccomodation( float acc )
{
	if( !_enableAccomodation ) return;
	_accomodation=acc;

	UpdateVolume();
}

void WaveBuffers8::Set3D( bool is3D )
{
	// disable 3D processing
	_3DEnabled = is3D;
}

bool WaveBuffers8::Get3D() const
{
	return _3DEnabled;
}

void WaveBuffers8::SetKind( SoundSystem8 * sys, WaveKind kind )
{
	_kind = kind;
	float vol = sys->_volumeAdjustEffect;
	if (_kind==WaveMusic) vol = sys->_volumeAdjustMusic;
	else if (_kind==WaveSpeech) vol = sys->_volumeAdjustSpeech;
	_volumeAdjust = vol;
	UpdateVolume();
}

void WaveBuffers8::SetVolumeAdjust
(
	float volEffect, float volSpeech, float volMusic
)
{
	float vol = volEffect;
	if (_kind==WaveMusic) vol = volMusic;
	else if (_kind==WaveSpeech) vol = volSpeech;
	_volumeAdjust = vol;
	UpdateVolume();
}

void WaveBuffers8::SetVolume( float vol, bool imm )
{
	_volume=vol;;
	UpdateVolume();
}

void Wave8::SetFrequency( float freq, bool immediate )
{
	DWORD iFreq=toIntFloor(freq*_frequency);
	if( iFreq<_soundSys->_minFreq ) iFreq=_soundSys->_minFreq;
	else if( iFreq>_soundSys->_maxFreq ) iFreq=_soundSys->_maxFreq;
	//float ratio = _frequencySet==0 ? 1000 : iFreq*1.0f/_frequencySet;
	//if (fabs(ratio-1)>0.1f)
	if( iFreq!=_frequencySet )
	{
		/**/
		//LogF("%s: ratio %g",(const char *)Name(),ratio);
		if( _playing && _buffer )
		{
			DSVerify( _buffer->SetFrequency(iFreq) );
		}
		/**/
		_frequencySet=iFreq;
	}
}

void Wave8::SetVolume( float volume, float freq, bool immediate )
{
	// recalculate volume using master volume
	Wave8::SetFrequency(freq,immediate);
	WaveBuffers8::SetVolume(volume,immediate);
}

// Sound system

SoundSystem8::SoundSystem8()
:_soundReady(false)
{
	#ifndef _XBOX
  CoInitialize(NULL);
	#endif
}
SoundSystem8::SoundSystem8(HWND hwnd, bool dummy)
:_soundReady(false)
{
	#ifndef _XBOX
  CoInitialize(NULL);
	#endif
	New(hwnd,dummy);
}
SoundSystem8::~SoundSystem8()
{
	SaveConfig();
	Delete();
	#ifndef _XBOX
  CoUninitialize();
	#endif
}

RString GetUserParams();

void SoundSystem8::LoadConfig()
{
	RString name = GetUserParams();

	ParamFile config;
	config.Parse(name);
	const float volFull=10;
	float cdVolume=volFull*0.5;
	float fxVolume=volFull*0.5;
	float speechVolume=volFull*0.5;
	if
	(
		config.FindEntry("volumeCD") && config.FindEntry("volumeFX")
		&& config.FindEntry("volumeSpeech")
	)
	{
		cdVolume=config>>"volumeCD";
		fxVolume=config>>"volumeFX";
		speechVolume=config>>"volumeSpeech";
	}
	SetCDVolume(cdVolume);
	SetWaveVolume(fxVolume);
	SetSpeechVolume(speechVolume);

	_hwEnabled = false;
	_eaxEnabled = false;
	if (config.FindEntry("enableEAX"))
	{
		_eaxEnabled = config>>"enableEAX";
	}
	if (config.FindEntry("enableHW"))
	{
		_hwEnabled = config>>"enableHW";
	}

#if _FORCE_SINGLE_VOICE
	Glob.config.singleVoice = true;
#else
	Glob.config.singleVoice = false;
	const ParamEntry *entry = config.FindEntry("singleVoice");
	if (entry) Glob.config.singleVoice = *entry;
#endif
}

void SoundSystem8::SaveConfig()
{
	if (!IsOutOfMemory())
	{
		RString name = GetUserParams();

		ParamFile config;
		config.Parse(name);
		config.Add("volumeCD",GetCDVolume());
		config.Add("volumeFX",GetWaveVolume());
		config.Add("volumeSpeech",GetSpeechVolume());
		config.Add("singleVoice", Glob.config.singleVoice);
		config.Add("enableEAX",_eaxEnabled);
		config.Add("enableHW",_hwEnabled);
		config.Save(name);
	}
}

bool SoundSystem8::EnableHWAccel(bool val)
{
	if (!_canHW) return false;
	if (_hwEnabled==val) return true;
	_hwEnabled = val;
	if (_hwEnabled)
	{
		_canEAX = true;
	}

	// we must restart all playing buffers
	_waves.Compact();
	for (int i=0; i<_waves.Size(); i++)
	{
		Wave8 *wave = _waves[i];
		if (!wave->_playing) continue;
		wave->Stop();
		wave->DoPlay();
	}
	// clear cache - release all stored sounds
	_cache.Clear();
	_cacheUniqueBuffers = 0;

	return _hwEnabled;
}

bool SoundSystem8::EnableEAX(bool val)
{
	if (!_canEAX || !_hwEnabled) return false;
	if (_eaxEnabled==val) return true;
	_eaxEnabled = val;
	if (_eaxEnabled)
	{
		if (!InitEAX())
		{
			_eaxEnabled = false;
		}
	}
	else
	{
		DeinitEAX();
	}
	return _eaxEnabled;
}

bool SoundSystem8::GetEAX() const
{
	return _eaxEnabled;
}

bool SoundSystem8::GetHWAccel() const
{
	return _hwEnabled;
}

/*!
\patch_internal 1.11 Date 8/1/2001 by Ondra
- New: possibility of dummy sound system - no DirectSound objects are ever created.
*/

void SoundSystem8::New(HWND winHandle, bool dummy)
{
	extern bool NoSound;
	#if !_DISABLE_GUI
	if (NoSound || dummy)
	#endif
	{
		LogF("No sound - command line switch");

		_soundReady=true;	
		_commited=false;
		_canHW = false;
		_canEAX = false;
		_maxChannels=31;
		_maxFreq=100000;
		_minFreq=100;
		_enablePreview = true;
		_doPreview = false;
		_hwEnabled = false;
		_eaxEnabled = false;

		SetSpeechVolume(5);
		SetWaveVolume(5);
		SetCDVolume(5);

		LoadConfig();
		return;	
	}

	#if !_DISABLE_GUI
	_enablePreview = false; // preview disabled until sound initialized
	_doPreview = false;

	_cacheUniqueBuffers = 0;

	_listenerPos=VZero;
	_listenerVel=VZero;
	_listenerDir=VForward;
	_listenerUp=VUp;

	_hwDuplicateWorks=true;

	#ifndef _XBOX
	_mixer = new AudioMixer();
	#endif
	_volumeAdjustEffect=1;
	_volumeAdjustSpeech=1;
	_volumeAdjustMusic=1;


	SetSpeechVolume(5);
	SetWaveVolume(5);
	SetCDVolume(5);

	LoadConfig();

	Assert( !_soundReady );

	_eaxEnv.type = SEPlain;
	_eaxEnv.density = 0.2;
	_eaxEnv.size = 100;

	#ifndef _XBOX

	HRESULT hr = DS_OK;
	hr = CoCreateInstance
	(
		CLSID_DirectSound8, NULL, CLSCTX_INPROC, 
		IID_IDirectSound8, (void**)_ds.Init()
	);
	if (!_ds)
	{
		hr = CoCreateInstance
		(
			CLSID_DirectSound, NULL, CLSCTX_INPROC, 
			IID_IDirectSound, (void**)_ds.Init()
		);
	}
	if( FAILED(hr) || !_ds )
	{
		LogF("No DirectSound");
		return;
	}
	_ds->Initialize(NULL);

	if( _ds->SetCooperativeLevel(winHandle,DSSCL_EXCLUSIVE)!=DS_OK )
	{
		LogF("Cannot set DirectSound cooperative level.");
		_ds.Free();
		return;
	}

	DSBUFFERDESC dsbd;
	// Set up the primary direct sound buffer.
	memset(&dsbd,0,sizeof(dsbd));
	dsbd.dwSize = sizeof(dsbd);
	dsbd.dwFlags = DSBCAPS_PRIMARYBUFFER|DSBCAPS_CTRL3D;
	

	if( _ds->CreateSoundBuffer(&dsbd,_primary.Init(),NULL) )
	{
		ErrorMessage("Cannot create primary sound buffer.");
	}

	WAVEFORMATEX wfx;
	wfx.wFormatTag=WAVE_FORMAT_PCM;
	wfx.nChannels=2;
	wfx.nSamplesPerSec=22050;
	//wfx.nSamplesPerSec=44100;
	//wfx.nSamplesPerSec=11025;
	wfx.wBitsPerSample=16;
	wfx.nBlockAlign=(short)(wfx.wBitsPerSample/8*wfx.nChannels);
	wfx.nAvgBytesPerSec=wfx.nBlockAlign*wfx.nSamplesPerSec;
	wfx.cbSize=0;
	
	if( _primary->SetFormat(&wfx)!=DS_OK )
	{
		ErrorMessage("Cannot set primary buffer format.");
	}

	if( _primary->QueryInterface(IID_IDirectSound3DListener, (void **)_listener.Init())!=DS_OK )
	{
		ErrorMessage("Primary buffer is not 3D.");
	}
	

	if( _primary->Play(0,0,DSBPLAY_LOOPING)!=DS_OK )
	{
		ErrorMessage("Cannot start primary buffer.");
	}

	#else
	DirectSoundCreate(NULL,_ds.Init(),NULL);
	QIFStream in;
	in.open("dsstdfx.bin");
	DSEFFECTIMAGELOC dsploc;
	DSEFFECTIMAGEDESC *fxdesc;
	dsploc.dwI3DL2ReverbIndex = I3DL2_CHAIN_I3DL2_REVERB;
	dsploc.dwCrosstalkIndex = I3DL2_CHAIN_XTALK;

	_ds->DownloadEffectsImage(in.act(),in.rest(),&dsploc,&fxdesc);
	//_ds->SetAzimuthOnly3DHRTF();

	DirectSoundUseFullHRTF();

	#endif


	_soundReady=true;	
	_commited=false;
	 // check valid freq. range

	#ifndef _XBOX
		// check HW acceleration level
		DSCAPS caps;
		caps.dwSize=sizeof(caps);
		_ds->GetCaps(&caps);
		_canHW = false;
		_canEAX = false;
		#if !DISABLE_HW_ACCEL
		// if card supports too few channels, do not use it's HW feutures
		if( caps.dwMaxHwMixingAllBuffers>=16 && caps.dwMaxHw3DAllBuffers>=16 )
		{
			_canHW = true;
			_canEAX = true;
			LogF("Using DirectSound3D HW");
		}
		else
		#endif
		{
			_hwEnabled = false;
			_eaxEnabled = false;
			_canEAX = false;
		}
	#else
		_canHW = true;
		_hwEnabled = true;
		_eaxEnabled = false;
		_canEAX = false;
	#endif

	#ifndef _XBOX	
	if (_hwEnabled)
	{	
		_minFreq=caps.dwMinSecondarySampleRate;
		_maxFreq=caps.dwMaxSecondarySampleRate;
	}
	else
	#endif
	{
		// no need to check hw caps if we do not use HW
		_minFreq=20;
		_maxFreq=100000;
	}
	// TODO: _maxChannels autodetection
	_maxChannels=31;
	if( _maxFreq==0 ) _maxFreq=100000;
	if( _minFreq==0 ) _minFreq=100;

	_enablePreview = true; // preview disabled until sound initialized
	_doPreview = false;

	if (_hwEnabled && _eaxEnabled)
	{
		_eaxEnabled = InitEAX();
	}
	#endif
}

static const RString PauseName="";

Wave8::Wave8( SoundSystem8 *sSys, float delay ) // empty wave
:AbstractWave(PauseName)
{
	WaveBuffers8::Reset(sSys);
	DoConstruct();
	_soundSys=sSys;
	_only2DEnabled=true;

	// note: only wav files may be streaming
	_volumeSet=-10000;
	_frequencySet=0;
	_frequency = 1000;
	_sSize = 1;
	_curPosition=toLargeInt(delay*-1000); // delay time in ms
	_size = 0;
	_volumeAdjust = _soundSys->_volumeAdjustEffect;
	_looping = false;	
	_posValid = false;

}

Wave8::Wave8( SoundSystem8 *sSys, RString name, bool is3D )
:AbstractWave(name)
{
	WaveBuffers8::Reset(sSys);
	DoConstruct();
	_soundSys=sSys;
	_only2DEnabled=!is3D;

	_posValid=!is3D; // 3d position explicitly set

	// note: only wav files may be streaming
	_volumeSet=-10000;
	_frequencySet=0;
	_curPosition=0;
	_volumeAdjust = _soundSys->_volumeAdjustEffect;

	LoadHeader();
}



Wave8::~Wave8()
{
	DoDestruct();
}

void Wave8::CacheStore( WaveDSCache8 &store )
{
	#if DO_PERF
		ADD_COUNTER(wavCS,1);
	#endif
	store._name=Name();
	store._buffer=_buffer;
	store._buffer3D=_buffer3D;
}

WaveDSCache8::~WaveDSCache8()
{
	if (_buffer)
	{
		#if DO_PERF
			ADD_COUNTER(wavCD,1);
		#endif

		_buffer3D.Free();
		_buffer.Free();

		#if DIAG_LOAD
			if (refC==0)
			{
				LogF("Destroy cache %s",(const char *)_name);
			}
			else
			{
				#if DIAG_LOAD>=2
				LogF("Release cache %s",(const char *)_name);
				#endif
			}
		#endif
	}
}

void Wave8::CacheLoad( WaveDSCache8 &store )
{
	#if DO_PERF
	if (_buffer)
	{
		ADD_COUNTER(wavD1,1);
	}
	#endif
	_buffer=store._buffer;
	_buffer3D=store._buffer3D;
	// all properties will be reset
	store._buffer=NULL;
	store._buffer3D=NULL;
}

bool Wave8::Duplicate( const Wave8 &src )
{
	if (src._stream) return false; // no duplicate possible on streaming buffers

	#if DO_PERF
	ADD_COUNTER(wavDp,1);
	#endif
	DoAssert( src.Loaded() );
	if( src._type!=Sw && !_soundSys->_hwDuplicateWorks ) return false;
	// try to duplicate hw buffer once
	
	//__int64 start = Rdtsc();

	#ifndef _XBOX
	if( _soundSys->_ds->DuplicateSoundBuffer(src._buffer,_buffer.Init()) )
	#endif
	{
		if( src._type!=Sw )
		{
			_soundSys->_hwDuplicateWorks=false;
		}
		// note: DuplicateSoundBuffer fails on most HW accelerators
		Log("Cannot duplicate sound buffer");
		// create wave from a scratch
		//Load(); //,Name());
		return false;
	}

	#ifndef _XBOX

	// we have real duplicate - shares sound buffer memory
	if( src._buffer3D )
	{ // obtain 3D buffer interface if neccessary
		if( _buffer->QueryInterface(IID_IDirectSound3DBuffer,(void **)_buffer3D.Init()) )
		{
			ErrorMessage("Cannot create 3D sound buffer");
		}
	}


	_size=src._size;
	_frequency=src._frequency; // same format as source
	_nChannel=src._nChannel;
	_sSize=src._sSize;

	_maxVolume=src._maxVolume;
	_avgVolume=src._avgVolume;

	_loaded=true;

	AddStats("Dup");
	//_soundSys->LogChannels("Duplicate");
	return true;
	#endif
}

bool SoundSystem8::LoadFromCache( Wave8 *wave )
{
	for( int i=0; i<_cache.Size(); i++ )
	{
		WaveDSCache8 &cache=_cache[i];
		if( !strcmp(cache._name,wave->Name()) )
		{
			wave->CacheLoad(cache);
			_cache.Delete(i);
			return true;
		}
	}
	return false;
}

void SoundSystem8::LogChannels( const char *name )
{
	#ifndef _XBOX
	DSCAPS caps;
	caps.dwSize=sizeof(caps);
	_ds->GetCaps(&caps);
	LogF
	(
		"%10s: Free tot %3d 3D %3d   Total tot %3d 3D %3d",
		name,
		caps.dwFreeHwMixingAllBuffers,caps.dwFreeHw3DAllBuffers,
		caps.dwMaxHwMixingAllBuffers,caps.dwMaxHw3DAllBuffers
	);
	#endif
}

void SoundSystem8::ReserveCache( int number3D, int number2D )
{
	#if !ENABLE_VOICEMAN
	// with voice manager there is no need to evict sounds from cache
	if (!_hwEnabled) return;
	// with HW acceleration: free HW channels
	// otherwise: keep some reasonable number of SW channels
	int size=_cache.Size();
	//int waves=_waves.Size();
	while( --size>=0 )
	{
		DSCAPS caps;
		caps.dwSize=sizeof(caps);
		_ds->GetCaps(&caps);
		// note: some HW reports negative free hw buffers
		WaveDSCache8 &cache=_cache[size];
		if( (int)caps.dwFreeHwMixingAllBuffers<number2D )
		{
			// this should be very rare
			#if DIAG_LOAD
			LogF
			(
				"Cache deleted %s (all %d, 3D %d)",
				(const char *)cache._name,
				caps.dwFreeHwMixingAllBuffers,
				caps.dwFreeHw3DAllBuffers
			);
			#endif
			_cache.Delete(size);
			#if DIAG_LOAD
			LogChannels("CacheDel2D");
			#endif
		}
		else if( (int)caps.dwFreeHw3DAllBuffers<number3D )
		{
			if( cache._buffer3D )
			{
				#if DIAG_LOAD
				LogF
				(
					"Cache 3D deleted %s (all %d, 3D %d)",
					(const char *)cache._name,caps.dwFreeHw3DAllBuffers,
					caps.dwFreeHwMixingAllBuffers,
					caps.dwFreeHw3DAllBuffers
				);
				#endif
				_cache.Delete(size);
				#if DIAG_LOAD
				LogChannels("CacheDel3D");
				#endif
			}
		}
	}
	#endif
}

void SoundSystem8::StoreToCache( Wave8 *wave )
{
	if (wave->_stream)
	{
		Fail("Storing streaming buffer");
		return;
	}
	#if DO_PERF
		ADD_COUNTER(sndSC,1);
	#endif
	// note: it may make sense to have the same sound stored twice
	// if DuplicateBuffer does not work well, it may be much faster
	// this happend only when debugging in DX8 beta
	/**/
	for( int i=0; i<_cache.Size();  )
	{
		WaveDSCache8 &cache=_cache[i];
		if( !strcmp(cache._name,wave->Name()) )
		{
			#if DIAG_LOAD>=2
				LogF("Wave8 %s cache refresh",(const char *)_cache[i]._name);
			#endif
			_cache.Delete(i);
			// move to cache beginning
			_cache.Insert(0);
			wave->CacheStore(_cache[0]);
			#if DO_PERF
				ADD_COUNTER(sndRC,1); // refresh cache
			#endif
			return;
		}
		else i++;
	}
	/**/


	int size=_cache.Size();
	//int waves=_waves.Size();
	// free HW channels

	const int maxSize=128;
	while( size>=maxSize )
	{
		_cache.Delete(--size);
		//LogChannels("CacheSize");
		#if DO_PERF
			ADD_COUNTER(sndCD,1);
		#endif
	}

	int index=0;
	_cache.Insert(index);
	WaveDSCache8 &cache=_cache[index];
	wave->CacheStore(cache);

	#if DIAG_LOAD>=2
	LogF("Wave8 %s inserted into the cache",(const char *)wave->Name());
	#endif
}

float SoundSystem8::GetWaveDuration( const char *filename )
{
	// load file header

	// only header is needed - whole files are cached
	Ref<WaveStream> stream = SoundLoadFile(filename);
	if (!stream) return 1;

	WAVEFORMATEX format;
	stream->GetFormat(format);
	int frequency = format.nSamplesPerSec;      // sampling rate
	int sSize = format.nBlockAlign;           // number of bytes per sample
	int size = stream->GetUncompressedSize();

	return float(size/sSize)/frequency;

}

bool SoundSystem8::InitEAX()
{
	#if !_DISABLE_GUI && !defined _XBOX
	WAVEFORMATEX wfx;

	wfx.wFormatTag=WAVE_FORMAT_PCM;
	wfx.nChannels=1;
	wfx.nSamplesPerSec=22050;
	wfx.wBitsPerSample=16;
	wfx.nBlockAlign=(short)(wfx.wBitsPerSample/8*wfx.nChannels);
	wfx.nAvgBytesPerSec=wfx.nBlockAlign*wfx.nSamplesPerSec;
	wfx.cbSize=0;

	// Set up the direct sound buffer. 
	DSBUFFERDESC dsbd;
	memset(&dsbd, 0, sizeof(DSBUFFERDESC));
	dsbd.dwSize = sizeof(DSBUFFERDESC);
	dsbd.dwFlags = DSBCAPS_CTRLFREQUENCY|DSBCAPS_CTRLVOLUME;
	dsbd.dwFlags |= LocHWFlags;
	dsbd.dwFlags |= DSBCAPS_CTRL3D;
	dsbd.dwFlags |= GetPos2Flags;
	dsbd.dwFlags |= DSBCAPS_MUTE3DATMAXDISTANCE;
	dsbd.guid3DAlgorithm=DS3DALG_DEFAULT; // surround/quadro if available

	dsbd.dwBufferBytes = 22050*2; // 1 sec buffer
	dsbd.lpwfxFormat = &wfx;

	HRESULT hr;
	hr =_ds->CreateSoundBuffer(&dsbd,_eaxFakeListener.Init(),NULL);
	if (hr!=DS_OK)
	{
		DXTRACE_ERR_NOMSGBOX("CreateSoundBuffer (EAX)",hr);
		return false;
	}


	if( _eaxFakeListener->QueryInterface(IID_IKsPropertySet,(void **)_eaxListener.Init()) )
	{
		RptF("Cannot get IID_IKsPropertySet (EAX)");
		_eaxFakeListener.Free();
		return false;
	}
	// check if EAX is supported
	DWORD support;
	const DWORD supportNeed = KSPROPERTY_SUPPORT_GET|KSPROPERTY_SUPPORT_SET;
	hr = _eaxListener->QuerySupport
	(
		DSPROPSETID_EAX_ListenerProperties,DSPROPERTY_EAXLISTENER_ALLPARAMETERS,&support
	);
	if (FAILED(hr) || (support&supportNeed)!=supportNeed)
	{
		_eaxFakeListener.Free();
		_eaxListener.Free();
		return false;
	}
	// 
	DoSetEAXEnvironment();
	
	return true;
	#else
	return false;
	#endif
}

void SoundSystem8::DeinitEAX()
{
	#ifndef _XBOX
	if (_eaxListener)
	{
		#if !_DISABLE_GUI
		// set no EAX
		DWORD val = -10000;
		_eaxListener->Set
		(
			DSPROPSETID_EAX_ListenerProperties,DSPROPERTY_EAXLISTENER_ROOM,
			NULL,0,&val,sizeof(val)
		);
		#endif
	}
	_eaxFakeListener.Free();
	_eaxListener.Free();
	#endif
}

void SoundSystem8::DoSetEAXEnvironment()
{
	#ifndef _XBOX
	#if !_DISABLE_GUI
	// TODO: convert SoundEnvironment to EAX2 properties
	//EAXLISTENERPROPERTIES eaxProp;
	DWORD type = EAX_ENVIRONMENT_GENERIC;
	switch (_eaxEnv.type)
	{
		case SEPlain: type = EAX_ENVIRONMENT_PLAIN; break;
		case SEMountains: type = EAX_ENVIRONMENT_MOUNTAINS; break;
		case SEForest: type = EAX_ENVIRONMENT_CITY; break;
		case SECity: type = EAX_ENVIRONMENT_CITY; break;
		case SERoom: type = EAX_ENVIRONMENT_ROOM; break;
	} 
	_eaxListener->Set
	(
		DSPROPSETID_EAX_ListenerProperties,DSPROPERTY_EAXLISTENER_ENVIRONMENT,
		NULL,0,&type,sizeof(type)
	);
	DWORD size = _eaxEnv.size;
	if (size<2) size = 2;
	if (size>100) size = 100;
	_eaxListener->Set
	(
		DSPROPSETID_EAX_ListenerProperties,DSPROPERTY_EAXLISTENER_ENVIRONMENTSIZE,
		NULL,0,&size,sizeof(size)
	);
	#endif
	#endif
}

void SoundSystem8::SetEnvironment( const SoundEnvironment &env )
{
	if
	(
		env.type==_eaxEnv.type &&
		fabs(env.size-_eaxEnv.size)<1 &&
		fabs(env.density-_eaxEnv.density)<0.05
	)
	{
		// no change
		return;
	}
	_eaxEnv = env;
	#ifndef _XBOX
	if (!_eaxListener) return;

	DoSetEAXEnvironment();
	#endif
}

/*!
\patch 1.34 Date 12/10/2001 by Ondra
- Fixed: MP: Dedicated server memory leak.
(Caused by bug in sound management with sounds disabled).
*/

void SoundSystem8::AddWaveToList(WaveGlobal8 *wave)
{
	for (int i=0; i<_waves.Size(); i++)
	{
		if (!_waves[i])
		{
			_waves[i] = wave;
			return;
		}
	}
	_waves.Add(wave);
}

AbstractWave *SoundSystem8::CreateWave( const char *filename, bool is3D, bool prealloc )
{

	// TODO: use prealloc
	if( !_soundReady ) return NULL;
	// if the wave is preloaded, simply duplicate it
	char lowName[256];
	strcpy(lowName,filename);
	strlwr(lowName);
	if (!*lowName) return NULL; // empty filename

	WaveGlobal8 *wave=new WaveGlobal8(this,lowName,is3D);
	//LogF("Created %s: %x",lowName,wave);
	AddWaveToList(wave);
	return wave;
}

AbstractWave *SoundSystem8::CreateEmptyWave( float duration )
{
	// create a Wave8 with no buffers, only delay
	if( !_soundReady ) return NULL;
	WaveGlobal8 *wave=new WaveGlobal8(this,duration);
	//LogF("Created empty %x",wave);
	AddWaveToList(wave);
	return wave;
}

void SoundSystem8::Delete()
{
	if( !_soundReady ) return;

	#ifndef _XBOX
	_eaxFakeListener.Free();
	_eaxListener.Free();
	#endif

	_previewEffect.Free();
	_previewMusic.Free();
	_previewSpeech.Free();

	#if DIAG_LOAD
	while( _cache.Size()>0 )
	{
		const WaveDSCache8 &wc = _cache[0];
		LogF("Wave8 %s deleted from cache",(const char *)wc._name);
		_cache.Delete(0);
		//LogChannels("CacheSize");
	}
	#endif

	_cache.Clear();	
	_cacheUniqueBuffers=0;
	_waves.Clear();

	if (_primary) _primary->Stop();
	_primary.Free();
	_listener.Free();

	_ds.Free();
	_soundReady=false;

	#ifndef _XBOX
	_mixer.Free();
	#endif
}



void SoundSystem8::SetListener
(
	Vector3Par pos, Vector3Par vel, Vector3Par dir, Vector3Par up
)
{

	_listenerPos=pos;
	_listenerVel=vel;
	_listenerDir=dir.Normalized();
	_listenerUp=up.Normalized();
	if (_listenerDir.SquareSize()<0.1) _listenerDir = VForward;
	if (_listenerUp.SquareSize()<0.1) _listenerUp = VUp;
	if (!_soundReady) return;
	if (!_listener) return;

	#if DEFERRED
		const int flags=DS3D_DEFERRED;
	#else
		const int flags=DS3D_IMMEDIATE;
	#endif

	DS3DLISTENER pars;
	pars.dwSize=sizeof(pars);
	pars.vPosition.x=pos[0],pars.vPosition.y=pos[1],pars.vPosition.z=pos[2];
	pars.vVelocity.x=vel[0],pars.vVelocity.y=vel[1],pars.vVelocity.z=vel[2];
	pars.vOrientFront.x=dir[0],pars.vOrientFront.y=dir[1],pars.vOrientFront.z=dir[2];
	pars.vOrientTop.x=up[0],pars.vOrientTop.y=up[1],pars.vOrientTop.z=up[2];
	pars.flDistanceFactor=1.0;
	pars.flRolloffFactor=1.0;
	pars.flDopplerFactor=1.0;
	_listener->SetAllParameters(&pars,flags);
}


static bool PreviewAdvance(AbstractWave *wave, float deltaT)
{
	if (!wave) return false;
	if (!wave->IsMuted())
	{
		wave->Play();
		//LogF("Preview %s play",(const char *)wave->Name());
	}
	else
	{
		wave->Stop();
		wave->Advance(deltaT);
		//LogF("Preview %s muted",(const char *)wave->Name());
	}
	return wave->IsTerminated();
}

/*!
\patch 1.01 Date 15/6/2001 by Ondra.
- Fixed double music volume in Audio options.
*/

void SoundSystem8::Commit()
{
	if( !_soundReady ) return;

	#if DEFERRED
		if (_listener)
		{
			_listener->CommitDeferredSettings();
		}
	#endif
	_commited=TRUE;
	for( int i=0; i<_waves.Size(); i++ )
	{
		Wave8 *wave = _waves[i];
		if (!wave) continue;
		if (wave->_wantPlaying)
		{
			wave->DoPlay();
			wave->_wantPlaying=false;
		}
	}

#if _ENABLE_CHEATS
	if( GInput.GetCheat2ToDo(DIK_W) )
	{
		// scan all playing waves
		LogF("Actual sound buffers:");
		int nPlay = 0;
		int nStop = 0;
		int nTerm = 0;
		for( int i=0; i<_waves.Size(); i++ )
		{
			Wave8 *wave=_waves[i];
			if( !wave ) continue;
			if (wave->_playing)
			{
				nPlay++;
				if (wave->_terminated) nTerm++;
			}
			else nStop++;
			const char *name = wave->Name();
			if (wave->Name().GetLength()<=0) name = "Pause";
			LogF("  %x:%s %s",wave,name,(const char *)wave->GetDiagText());
		}
		LogF("  channels stat %d",STAT_GETCHANNELS());
		LogF("  playing %d, terminated %d",nPlay,nTerm);
		LogF("  stopped %d",nStop);
		LogF("  listener");

		if (_listener)
		{
			DS3DLISTENER pars;
			pars.dwSize=sizeof(pars);
			//pars.vOrientTop.x=up[0],pars.vOrientTop.y=up[1],pars.vOrientTop.z=up[2];
			//pars.flDistanceFactor=1.0;
			//pars.flRolloffFactor=1.0;
			//pars.flDopplerFactor=1.0;
			_listener->GetAllParameters(&pars);

			LogF
			(
				"    pos (%.1f,%.1f,%.1f) (%.1f,%.1f,%.1f)\n"
				"    dir (%.1f,%.1f,%.1f) (%.1f,%.1f,%.1f)\n"
				"    vel (%.1f,%.1f,%.1f) (%.1f,%.1f,%.1f)",
				_listenerPos[0],_listenerPos[1],_listenerPos[2],
				pars.vPosition.x,pars.vPosition.y,pars.vPosition.z,
				_listenerDir[0],_listenerDir[1],_listenerDir[2],
				pars.vOrientFront.x,pars.vOrientFront.y,pars.vOrientFront.z,
				_listenerVel[0],_listenerVel[1],_listenerVel[2],
				pars.vVelocity.x,pars.vVelocity.y,pars.vVelocity.z
			);
		}

	}
#endif

	// there may be some preview active
	if (_doPreview && GlobalTickCount()>=_previewTime)
	{
		float deltaT = 0.1;
		if (!_previewSpeech )
		{
			//float volume=GetSpeechVolCoef();
			// start first sound
			const ParamEntry &cfg = Pars >> "CfgSFX" >> "Preview" >> "speech"; 
			RStringB name = cfg[0];
			AbstractWave *wave = CreateWave(name,false,true);
			_previewSpeech = wave;
			if( wave )
			{
				wave->SetKind(WaveSpeech);
				wave->EnableAccomodation(false);
				wave->SetSticky(true);
				wave->SetVolume(cfg[1],cfg[2],true);
				wave->Repeat(1);
				wave->Play();
			}
			else
			{
				_doPreview = false; // error - no preview possible
			}
		}
		else if( PreviewAdvance(_previewSpeech,deltaT) && !_previewEffect )
		{
			// start second sound
			const ParamEntry &cfg = Pars >> "CfgSFX" >> "Preview" >> "effect"; 
			RStringB name = cfg[0];
			AbstractWave *wave = CreateWave(name,true,false);
			_previewEffect = wave;
			if( _previewEffect )
			{
				wave->EnableAccomodation(false);
				wave->SetSticky(true);
				wave->SetVolume(cfg[1],cfg[2],true);
				wave->SetPosition
				(
					_listenerPos+_listenerDir*50,_listenerVel,true
				);
				wave->Repeat(1);
				wave->Play();
			}
		}
		else if (PreviewAdvance(_previewEffect,deltaT) && !_previewMusic)
		{
			const ParamEntry &cfg = Pars >> "CfgSFX" >> "Preview" >> "music"; 
			RStringB name = cfg[0];
			AbstractWave *wave = CreateWave(name,false,true);
			_previewMusic=wave;
			if( wave )
			{
				wave->SetKind(WaveMusic);
				wave->EnableAccomodation(false);
				wave->SetSticky(true);
				// FIX
				// default volume for music is 0.5
				float vol = cfg[1];
				wave->SetVolume(vol*0.5,cfg[2],true);
				wave->Repeat(1);
				wave->Play();
			}
		}
		else if (PreviewAdvance(_previewMusic,deltaT))
		{
			_doPreview = false;
			_previewSpeech.Free();
			_previewEffect.Free();
			_previewMusic.Free();
		}
	}
}


inline float expVol( float vol )
{
	if (vol<=0) return 1e-10; // muted -> -200 db
	// vol in range 0..10
	//return exp(vol*0.5-5);
	//return exp((vol*2-20)*0.5);
	// max. volume is 0
	return exp((vol-10)*0.7);
}

void SoundSystem8::PreviewMixer()
{
	if (!_enablePreview) return;
	_doPreview=true;
	_previewTime = GlobalTickCount()+200;
}

void SoundSystem8::StartPreview()
{
	if (_previewMusic)
	{
		TerminatePreview();
	}
	PreviewMixer();
}

void SoundSystem8::TerminatePreview()
{
	_previewEffect.Free();
	_previewMusic.Free();
	_previewSpeech.Free();
	_doPreview = false;
}

void SoundSystem8::FlushBank(QFBank *bank)
{
	// release any cached waves from this bank
	if (bank)
	{
		for (int i=0; i<_cache.Size(); i++)
		{
			if (!bank->FileExists(_cache[i]._name)) continue;
			_cache.Delete(i);
			i--;
		}
	}
	_waves.Compact();
}

float SoundSystem8::MaxVolume() const
{
	return floatMax(_musicVol,floatMax(_waveVol,_speechVol));
}

float SoundSystem8::MaxExpVolume() const
{
	return floatMax(_musicVolExp,floatMax(_waveVolExp,_speechVolExp));
}

void SoundSystem8::UpdateMixer()
{
	#ifndef _XBOX
		float maxVolExp = MaxExpVolume();
	#else
		float maxVolExp = 1;
	#endif
	_volumeAdjustEffect = _waveVolExp/maxVolExp;
	_volumeAdjustSpeech = _speechVolExp/maxVolExp;
	_volumeAdjustMusic = _musicVolExp/maxVolExp;

	//LogF("Update _volumeAdjust %g",_volumeAdjust);

	#ifndef _XBOX
	if (!_mixer) return;
	#endif
	
	for (int i=0; i<_waves.Size(); i++)
	{
		Wave8 *wave = _waves[i];
		if (wave) wave->SetVolumeAdjust
		(
			_volumeAdjustEffect,_volumeAdjustSpeech,_volumeAdjustMusic
		);
	}

	//LogF("Update %g,%g,%g, maxVol %g",_waveVolExp,_speechVolExp,_musicVolExp,maxVol);
	// TODO: scale accordingly to have both in same scale
	#ifndef _XBOX
	float maxVol = MaxVolume();
	_mixer->SetWaveVolume(maxVol*0.1);
	#endif
}

float SoundSystem8::GetSpeechVolume()
{
	return _speechVol;
}
void SoundSystem8::SetSpeechVolume(float val)
{
	_speechVol=val;
	_speechVolExp=expVol(_speechVol);
	UpdateMixer();
}
float SoundSystem8::GetWaveVolume()
{
	return _waveVol;
}
void SoundSystem8::SetWaveVolume(float val)
{
	_waveVol=val;
	_waveVolExp=expVol(_waveVol);
	UpdateMixer();
}

float SoundSystem8::GetCDVolume() const
{
	return _musicVol;
}
void SoundSystem8::SetCDVolume(float val)
{
	_musicVol=val;
	_musicVolExp=expVol(_musicVol);
	UpdateMixer();
}


void SoundSystem8::Activate(bool){}

