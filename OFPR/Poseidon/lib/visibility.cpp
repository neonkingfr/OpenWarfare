// visibility matrix
// row index is brain (sensor) handle
// column index is target handle

#include "wpch.hpp"
#include "vehicleAI.hpp"
#include "visibility.hpp"
#include "ai.hpp"
#include "global.hpp"
#include "world.hpp"
#include <El/Common/perfLog.hpp>
#include "perfProf.hpp"
#include "landscape.hpp"

#define DIAGS 0

SensorRow::SensorRow()
{
}

void SensorRow::Init( Person *vehicle, int size )
{
	_info.Realloc(size);
	_info.Resize(size);
	for( int i=0; i<size; i++ )
	{
		_info[i].Init();
	}
	_vehicle=vehicle;
}

void SensorRow::Free()
{
	_info.Clear();
	_vehicle=NULL;
}

template <class VehicleType>
LSError SensorInfo<VehicleType>::Serialize(ParamArchive &ar)
{
	CHECK(ar.SerializeRef("Vehicle", _vehicle, 1))
	return LSOK;
}

void SensorUpdate::SetVisibility(float vis)
{
	int iVis = (int)(vis*255);
	saturate(iVis,0,255);
	vis8 = iVis;
}

void SensorUpdate::Init()
{
	// full dirty
	rowPos = VZero;
	colPos = VZero;
	time = TIME_MIN;
	lastVisible = TIMESEC_MIN;
	vis8 = 0;
}

RString SensorUpdate::DiagText() const
{
	char buf[256];
	sprintf
	(
		buf,"%.2f (%.1fs, lv %.1fs, %.1fs)",
		GetVisibility(),Glob.time.Diff(time),
		Glob.time.Diff(lastVisible),Glob.time.Diff(GetLastVisibilityTime())
	);
	return buf;
}

#define DEF_STRB(x) static RStringB String_##x(#x);
#define USE_STRB(x) String_##x

DEF_STRB(lastVisible)
DEF_STRB(vis8)
DEF_STRB(time)
DEF_STRB(rowPos)
DEF_STRB(colPos)

bool SensorUpdate::IsDefaultValue(ParamArchive &ar) const
{
	if (Time(lastVisible)<Glob.time-120)
	{
		// it was not visible for very long time and can be considered never visible
		// - use default values
		return true;
	}
	return false;
}

void SensorUpdate::LoadDefaultValues(ParamArchive &ar)
{
	// if there are no values, use default
	lastVisible = ar.GetArVersion()>=12 ? TIMESEC_MIN : TIMESEC_MAX;
	vis8 = 0;
	time = Glob.time;
	rowPos = VZero;
	colPos = VZero;
}


/*!
\patch 1.50 Date 4/11/2002 by Ondra
- Optimized: Memory usage during save-game is now lower.
*/

LSError SensorUpdate::Serialize(ParamArchive &ar)
{
	// visibility matrix can be huge
	// many items in visibility matrix are "never visible"
	#if 0
		class Item141
		{
			lastVisible=-32805.683594;
			time=-33.307999;
			rowPos[]={3540.997559,2.224397,3244.218994};
			colPos[]={5488.179199,73.651917,3496.116943};
		};
	#endif
	// we do not save such values
	// when loading them, we use some reasonable default
	if (ar.IsSaving())
	{
		if (Time(lastVisible)<Glob.time-120)
		{
			// it was not visible for very long time and can be considered never visible
			// - use default values
			return LSOK;
		}
	}
	TimeSec defLastVisible = ar.GetArVersion()>=12 ? TIMESEC_MIN : TIMESEC_MAX;
	CHECK(ar.Serialize(USE_STRB(lastVisible), lastVisible, 1, defLastVisible))
	// if there are no values, use default
	CHECK(ar.Serialize(USE_STRB(vis8), vis8, 1, 0))
	// if not saved, load as MAX (most recent possible)
	CHECK(ar.Serialize(USE_STRB(time), time, 1, Glob.time))
	CHECK(ar.Serialize(USE_STRB(rowPos), rowPos, 1, VZero))
	CHECK(ar.Serialize(USE_STRB(colPos), colPos, 1, VZero))

	return LSOK;
}

LSError SensorRow::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	CHECK(ar.SerializeDef("Vis", _info, 1))
	return LSOK;
}

float SensorList::Unimportance
(
	EntityAI *from, EntityAI *to, float &defValue
)
{
	defValue = 1;
	if( !from ) return 1e10;
	AIUnit *fromBrain = from->CommanderUnit();
	Assert( fromBrain );
	if( !fromBrain ) return 1e10;
	AIGroup *fromGroup = fromBrain->GetGroup();
	if( !fromGroup ) return 1e10;
	AICenter *fromCenter = fromGroup->GetCenter();
	// same center units are ignored - thay are always seen and known
	AIUnit *toUnit = to->CommanderUnit();
	if (toUnit)
	{
		AIGroup *toGroup = toUnit->GetGroup();
		if (toGroup)
		{
			AICenter *toCenter = toGroup->GetCenter();
			if (fromCenter==toCenter)
			{
				// it may be renegade, running captive or joined enemy soldier
				if (!fromCenter->IsEnemy(to->GetTargetSide()))
				{
					if (fromGroup!=toGroup)
					{
						// unimportant friendly - different group
						return 1e10;
					}
					// unimportant: same group
					// check visibility only when not alive
					if (toUnit && toUnit->GetLifeState()==AIUnit::LSAlive)
					{
						defValue = 0;
						return 1e10;
					}
				}
			}
		}
	}
	// calculate max. allowed dirty
	float unimportance=to->VisibleSize();
	saturateMax(unimportance,2);
	float dist2=from->Position().Distance2(to->Position());
	if( dist2>Square(200) )
	{
		unimportance*=2;
		if( dist2>Square(600) ) unimportance*=2; // cannot be targeted
	}
	return unimportance;
}

SensorList::SensorList()
:_lastUpdateRow(0),_lastUpdateCol(0)
{
}

SensorList::~SensorList()
{
}

SensorRowID SensorList::FindRowID( Person *veh )
{
	for( int i=0; i<_rows.Size(); i++ )
	{
		if( _rows[i]._vehicle==veh ) return SensorRowID(i);
	}
	return SensorRowID(-1);
}
SensorColID SensorList::FindColID( EntityAI *veh )
{
	for( int i=0; i<_cols.Size(); i++ )
	{
		if( _cols[i]._vehicle==veh ) return SensorColID(i);
	}
	return SensorColID(-1);
}

SensorRowID SensorList::AddRow( Person *veh )
{
	if (!veh->Brain() || veh->Brain()->GetLifeState()==AIUnit::LSDead)
	{
		LogF("Add dead sensor %s",(const char *)veh->GetDebugName());
		// error:
		return (SensorRowID)-1;
	}
	if (veh->IsDammageDestroyed())
	{
		LogF("Add destroyed sensor %s",(const char *)veh->GetDebugName());
		return (SensorRowID)-1;
	}
	// seach for some free slot
	SensorRowID rowID=FindRowID(NULL);
	if( rowID<0 ) rowID=_rows.Add();
	_rows[rowID].Init(veh,_cols.Size());
	//_rows[rowID]._dirty=MaxSensorDirty;
	veh->SetSensorRowID(rowID);
	return rowID;
}

SensorColID SensorList::AddCol( EntityAI *veh )
{
	if (!veh->IsInLandscape())
	{
		Fail("BUG: Adding in-vehicle target.");
	}
	SensorColID colID=FindColID(NULL);
	if( colID<0 )
	{
		colID=_cols.Add();
		// add this ID to all rows
		for( int row=0; row<_rows.Size(); row++ ) if( _rows[row]._vehicle!=NULL ) 
		{
			_rows[row]._info.Access(colID);
		}
	}
	for( int row=0; row<_rows.Size(); row++ ) if( _rows[row]._vehicle!=NULL ) 
	{
		_rows[row]._info[colID].Init();
	}
	_cols[colID]._vehicle=veh;
	veh->SetSensorColID(colID);
	return colID;
}
void SensorList::DeleteRow( SensorRowID i )
{
	Person *veh = _rows[i]._vehicle;
	veh->SetSensorRowID(SensorRowID(-1)); // no longer used as sensor
	_rows[i].Free(); // delete
}


void SensorList::DeleteRow( Person *veh )
{
	SensorRowID index=veh->GetSensorRowID();
	if( index<0 ) return;
	#if !_RELEASE
		Log
		(
			"Removed sensor row %s",
			(const char *)veh->GetDebugName()
		);
	#endif
	Assert( index==FindRowID(veh) );
	_rows[index].Free(); // delete
	veh->SetSensorRowID(SensorRowID(-1)); // no longer used as sensor
}

void SensorList::DeleteCol( SensorColID i )
{
	EntityAI *veh = _cols[i]._vehicle;
	veh->SetSensorColID(SensorColID(-1)); // no longer used as sensor
	_cols[i]._vehicle=NULL; // delete
}

void SensorList::DeleteCol( EntityAI *veh )
{
	SensorColID index=veh->GetSensorColID();
	if( index<0 ) return;
	#if !_RELEASE
		Log
		(
			"Removed sensor column %s",
			(const char *)veh->GetDebugName()
		);
	#endif
	Assert( index==FindColID(veh) );
	DeleteCol(index);
}


void SensorList::Compact()
{
	Log("SensorList::Compact not impemented.");
}


int SensorList::UpdateCell( SensorRowID r, SensorColID c )
{
	SensorRow &row=_rows[r];
	Person *fromDriver=row._vehicle;
	if( !fromDriver ) return 0;
	SensorCol &col=_cols[c];
	EntityAI *to=col._vehicle;
	if( !to ) return 0;
	// 
	AIUnit *fromUnit = fromDriver->CommanderUnit();
	if (!fromUnit) return 0;

	EntityAI *from = fromUnit->GetVehicle();

	// update single
	SensorUpdate &info=row._info[c];
	if( from==to )
	{
		info.SetVisibility1();
		info.SetLastVisible(Glob.time);
		info.rowPos=from->Position();
		info.colPos=to->Position();
		info.time=Glob.time;
		return 0;
	}
	float dist2 = from->Position().Distance2(to->Position());
	float irRange = from->GetType()->GetIRScanRange();
	if (!to->GetType()->GetIRTarget()) irRange = 0;
	float sensorLimit=floatMax(irRange,TACTICAL_VISIBILITY);
	if( dist2<Square(sensorLimit) )
	{
		float defValue=0;
		float unimportance=Unimportance(from,to,defValue);

		// unimportant: assume default value
		if( unimportance>1e5 )
		{
			info.SetVisibility(defValue);
			info.rowPos=from->Position();
			info.colPos=to->Position();
			info.time=Glob.time-30;
			if (defValue>=0.02f)
			{
				info.SetLastVisible(Glob.time);
			}
			// set time in past 
			// to guarantee quick evaluation whenunimportance would drop down
			// (like in case of death)
			return 0;
		}
		// calculate dirty value
		float toMoved=to->Position().Distance(info.colPos);
		float fromMoved=from->Position().Distance(info.rowPos);
		float timeMoved=timeMoved=Glob.time.Diff(info.time);
		
		// sophisticated check (4D: 3D + time)
		float dirty=toMoved+fromMoved+timeMoved*0.1;
		if( dirty<unimportance ) return 0;

		/*
		if (dist2>Square(Glob.config.horizontZ*4))
		{
			LogF
			(
				"%s: scanrange %.1f (%s)",
				(const char *)from->GetDebugName(),from->GetType()->GetIRScanRange(),
				(const char *)from->GetType()->GetName()
			);
			LogF("%s: IR target %d",(const char *)to->GetDebugName(),to->GetType()->GetIRTarget());
		}
		float distCA2 = from->CameraPosition().Distance2(to->AimingPosition());
		if (distCA2>Square(Glob.config.horizontZ*4))
		{
			LogF("CA distance %.1f, distance %.1f",sqrt(distCA2),sqrt(dist2));
			LogF
			(
				"%s: scanrange %.1f (%s)",
				(const char *)from->GetDebugName(),from->GetType()->GetIRScanRange(),
				(const char *)from->GetType()->GetName()
			);
			LogF("%s: IR target %d",(const char *)to->GetDebugName(),to->GetType()->GetIRTarget());
		}
		*/
		// check from/to camera/aiming positions
		float visibility=GLOB_LAND->Visible(from,to);

		/*
		float aboveFrom = GLandscape->AboveSurface(from->CameraPosition());
		float aboveTo = GLandscape->AboveSurface(to->AimingPosition());
		LogF("  from %s: above %.1f",(const char *)from->GetDebugName(),aboveFrom);
		LogF("  to   %s: above %.1f",(const char *)to->GetDebugName(),aboveTo);
		*/

		info.SetVisibility(visibility);
		info.rowPos=from->Position();
		info.colPos=to->Position();
		info.time=Glob.time;
		if (visibility>0.02f)
		{
			info.SetLastVisible(Glob.time);
		}
		GLOB_WORLD->SecondaryAllowSwitch();
		#if DIAGS
			LogF
			(
				"Cell update %s to %s (%d,%d) %.2f, dirty %.1f<%.1f",
				(const char *)from->GetDebugName(),(const char *)to->GetDebugName(),
				r,c,visibility,dirty,unimportance
			);
		#endif
		return 1;
	}
	else
	{
		info.SetVisibility0();
		info.rowPos=from->Position();
		info.colPos=to->Position();
		info.time=Glob.time;
		return 0;
	}
}

int SensorList::UpdateRow( SensorRowID id )
{
	int countUpdates=0;
	SensorRow &row=_rows[id];
	Person *fromDriver=row._vehicle;
	Assert( fromDriver );
	AIUnit *fromUnit = fromDriver->CommanderUnit();
	//EntityAI *from = fromUnit->GetVehicle();
	if (!fromUnit)
	{
		ErrF("No unit sensor row %s",(const char *)fromDriver->GetDebugName());
		row.Free(); // delete - it will not be needed for a long time
		fromDriver->SetSensorRowID(SensorRowID(-1));
		return 0;
	}
	if ( fromUnit->IsInCargo())
	{
		// TODO: BUG: assert failed
		//Fail("Cargo unit sensor present");
		// unit loaded in cargo
		#if DIAGS
			LogF("Removed cargo sensor row %s",(const char *)fromDriver->GetDebugName());
		#endif
		row.Free(); // delete - it will not be needed for a long time
		fromDriver->SetSensorRowID(SensorRowID(-1));
		return 0;
	}
	for( int i=0; i<_cols.Size(); i++ )
	{
		countUpdates+=UpdateCell(id,SensorColID(i));
	}
	#if DIAGS
		if( countUpdates>0 )
		{
			LogF
			(
				"Update sensor row %d,%s %d updates",
				id,(const char *)from->GetDebugName(),countUpdates
			);
		}
	#endif
	return countUpdates;
}

int SensorList::UpdateCol( SensorColID id )
{
	//LogF("UpdateCol %d",id);
	// last column pos. change is col._posUpdatedTime
	int countUpdates=0;
	//SensorCol &col=_cols[id];
	//EntityAI *to=;
	Assert( _cols[id]._vehicle );
	//bool someDirty=false;

	for( int i=0; i<_rows.Size(); i++ )
	{
		countUpdates+=UpdateCell(SensorRowID(i),id);
	}

	#if DIAGS
		if( countUpdates>0 )
		{
			LogF
			(
				"Update target col %d,%s  %d updates",
				id,(const char *)to->GetDebugName(),
				countUpdates
			);
		}
	#endif
	return countUpdates;
}

LSError SensorList::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("Cols", _cols, 1))
	CHECK(ar.Serialize("Rows", _rows, 1))
	CHECK(ar.Serialize("lastUpdateRow", _lastUpdateRow, 1))
	CHECK(ar.Serialize("lastUpdateCol", _lastUpdateCol, 1))
	return LSOK;
}

void SensorList::CheckPos()
{
	// verify no targets are inside the vehcile
	// if they are, their position is invalid
	for (int i=0; i<_cols.Size(); i++)
	{
		SensorCol &col = _cols[i];
		EntityAI *veh = col._vehicle;
		if (!veh) continue;
		// vehicle may be person that is inside of another vehicle
		// TODO: make function IsInLandscape better
		if (veh->IsInLandscape()) continue;
		// TODO: report this situation
		LogF("Removing in-vehicle target %s",(const char *)veh->GetDebugName());
		DeleteCol(SensorColID(i));
		// target should be removed when getting in the vehicle
	}
	// remove dead sensors
	for (int i=0; i<_rows.Size(); i++)
	{
		SensorRow &row = _rows[i];
		Person *veh = row._vehicle;
		if (!veh) continue;
		if (veh->IsDammageDestroyed() && !veh->CommanderUnit())
		{
			DeleteRow(SensorRowID(i));
			LogF("Removing dead sensor %s",(const char *)veh->GetDebugName());
		}
		
	}
}

/*!
\patch 1.30 Date 11/06/2001 by Ondra.
- Fixed: Random crash in visibility matrix update.
*/

int SensorList::SmartUpdateAll()
{
	PROFILE_SCOPE(senUA);
	CheckPos();
	int sum=0;
	const int maxTests=4;
	const int maxCalcs=4;
	for( int maxUpdates=maxCalcs,maxRowCols=maxTests,maxChecks=_rows.Size(); --maxChecks>=0; )
	{
		if( ++_lastUpdateRow>=_rows.Size() ) _lastUpdateRow=0;
		if (_rows.Size()<=_lastUpdateRow) break;
		SensorRow &row=_rows[_lastUpdateRow];
		if( row._vehicle!=NULL )
		{
			int updates=UpdateRow(SensorRowID(_lastUpdateRow));
			GLOB_WORLD->SecondaryAllowSwitch();
			if( (maxUpdates-=updates)<=0 ) break;
			sum+=updates;
			if( --maxRowCols<0 ) break;
		}
	}
	for( int maxUpdates=maxCalcs,maxRowCols=maxTests,maxChecks=_cols.Size(); --maxChecks>=0; )
	{
		if( ++_lastUpdateCol>=_cols.Size() ) _lastUpdateCol=0;
		if (_cols.Size()<=_lastUpdateCol) break;
		SensorCol &col=_cols[_lastUpdateCol];
		if( col._vehicle!=NULL )
		{
			int updates=UpdateCol(SensorColID(_lastUpdateCol));
			GLOB_WORLD->SecondaryAllowSwitch();
			if( (maxUpdates-=updates)<=0 ) break;
			sum+=updates;
			if( --maxRowCols<0 ) break;
		}
	}
	#if DIAGS
	if( sum>0 )
	{
		LogF("%5f: Visibility: %d updates",Glob.time-Time(0),sum);
	}
	#endif
	//ADD_COUNTER(sCell,sum);
	return sum;
}

int SensorList::UpdateAll()
{	
	CheckPos();
	int sum=0;
	int index;
	for( index=0; index<_rows.Size(); index++ ) if( _rows[index]._vehicle!=NULL )
	{
		sum+=UpdateRow(SensorRowID(index));
	}
	for( index=0; index<_cols.Size(); index++ ) if( _cols[index]._vehicle!=NULL )
	{
		sum+=UpdateCol(SensorColID(index));
	}
	#if DIAGS
		LogF("Visibility: %d updates",sum);
	#endif
	//ADD_COUNTER(sCell,sum);
	return sum;
}


float SensorList::GetVisibility( Person *from, EntityAI *to ) const
{
	if( from->GetSensorRowID()<0 )
	{
		#if DIAGS
			LogF("Added sensor row %s",(const char *)from->GetDebugName());
		#endif
		SensorRowID id = const_cast<SensorList *>(this)->AddRow(from);
		if (id<0) return 0;
	}
	if( to->GetSensorColID()<0 )
	{
		#if DIAGS
			LogF("Added sensor col %s",(const char *)to->GetDebugName());
		#endif
		SensorColID id = const_cast<SensorList *>(this)->AddCol(to);
		if (id<0) return 0;
	}
	if (from->GetSensorRowID()>=_rows.Size())
	{
		ErrF
		(
			"%s: bad sensor row %d (>=%d)",
			(const char *)from->GetDebugName(),from->GetSensorRowID(),_rows.Size()
		);
		return 0;
	}
	const SensorRow &row=_rows[from->GetSensorRowID()];	
	if (to->GetSensorColID()>=row._info.Size())
	{
		ErrF
		(
			"%s: bad sensor col %d (>=%d)",
			(const char *)to->GetDebugName(),to->GetSensorColID(),row._info.Size()
		);
		return 0;
	}
	return row._info[to->GetSensorColID()].GetVisibility();
}


Time SensorUpdate::GetLastVisibilityTime() const
{
	if(lastVisible<=TIMESEC_MIN)
	{
		return TIME_MIN;
	}
	if (time<TIME_MIN+1 || Time(lastVisible)>=time-1)
	{
		return Glob.time;
	}
	return lastVisible;
}

Time SensorList::GetVisibilityTime( Person *from, EntityAI *to ) const
{
	// this function is used to perform safety check on AI targets
	// it is therefore optimisitic and if real value is not know, it assumes
	// target is visible now
	if( from->GetSensorRowID()<0 ) return Glob.time;
	if( to->GetSensorColID()<0 ) return Glob.time;
	if (from->GetSensorRowID()>=_rows.Size()) return Glob.time;
	const SensorRow &row=_rows[from->GetSensorRowID()];	
	if (to->GetSensorColID()>=row._info.Size()) return Glob.time;
	const SensorUpdate &upd = row._info[to->GetSensorColID()];
	// if time of last visibility is same as time of last check
	// return it is visible now
	return upd.GetLastVisibilityTime();
}

RString SensorList::DiagText( Person *from, EntityAI *to ) const
{
	char buf[256];
	if( from->GetSensorRowID()<0 )
	{
		return "No row";
	}
	if( to->GetSensorColID()<0 )
	{
		return "No col";
	}
	const SensorRow &row=_rows[from->GetSensorRowID()];	
	//return row._vis[to->GetSensorColID()]*(1.0/255);
	const SensorUpdate &info=row._info[to->GetSensorColID()];
	sprintf(buf,"Cell %d,%d: ",from->GetSensorRowID(),to->GetSensorColID());
	strcat(buf,info.DiagText());
	return buf;
}

bool SensorList::CheckStructure() const
{
	return true;
}


