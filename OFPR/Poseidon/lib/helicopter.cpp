// Poseidon - vehicle class
// (C) 1997, Suma
#include "wpch.hpp"

#include "helicopter.hpp"
#include "ai.hpp"
#include "shots.hpp"
#include "scene.hpp"
#include "world.hpp"
#include "landscape.hpp"
#include "keyInput.hpp"
#include "lights.hpp"
//#include "strIncl.hpp"
#include "stringtableExt.hpp"
#include "txtPreload.hpp"
#include "diagModes.hpp"

#include "tlVertex.hpp"

#include "uiActions.hpp"

#include <El/Common/randomGen.hpp>

#include "network.hpp"

#include "engine.hpp"
#include "SpecLods.hpp"

#include <El/Common/enumNames.hpp>

#include "mbcs.hpp"

#if _RELEASE
	#define ARROWS 0
#else
	#define ARROWS 1
#endif

Helicopter::Helicopter( VehicleType *name, Person *pilot )
:base(name,pilot),

_rotorSpeed(0),_rotorSpeedWanted(0), // turning motor on/off
_rotorPosition(0),
_mainRotor(0),_mainRotorWanted(0),
_backRotor(0),_backRotorWanted(0),

_cyclicAside(0),_cyclicAsideWanted(0),
_cyclicForward(0),_cyclicForwardWanted(0),
_rotorDive(0),_rotorDiveWanted(0),

_lastAngVelocity(VZero),
_turbulence(VZero),
_lastTurbulenceTime(Glob.time),

_rndFrequency(1-GRandGen.RandomValue()*0.05), // do not use same sound frequency
_missileLRToggle(false),_rocketLRToggle(false)

{
	_head.SetPars("Air");
	_head.Init(Type()->_pilotPos-Vector3(0,0.2,0),Type()->_pilotPos,this);
	if (pilot && QIsManual(pilot->Brain()) ) EngineOn();
	SetSimulationPrecision(1.0/15);

	_mGunClouds.Load((*Type()->_par) >> "MGunClouds");

	_mGunFireFrames = 0;
	_mGunFireTime = UITIME_MIN;
	_mGunFirePhase = 0;
}

Helicopter::~Helicopter()
{
}

HelicopterType::HelicopterType( const ParamEntry *param )
:base(param)
{
	_scopeLevel=1;

	_pilotPos=VZero;

/*
	_gunPos=VZero;
	_gunDir=Vector3(0,0,1);
	_gunAxis=VZero;
*/

	_hudPosition = VZero;
	_hudRight = VZero;
	_hudDown = VZero; 
}

void HelicopterType::Load(const ParamEntry &par)
{
	base::Load(par);

	_turret.Load(par>>"Turret");

	_enableSweep = par>>"enableSweep";

	_mainRotorSpeed = par>>"mainRotorSpeed";
	_backRotorSpeed = par>>"backRotorSpeed";

	_minMainRotorDive = float(par>>"minMainRotorDive")*(H_PI/180);
	_maxMainRotorDive = float(par>>"maxMainRotorDive")*(H_PI/180);
	_neutralMainRotorDive = float(par>>"neutralMainRotorDive")*(H_PI/180);


	_minBackRotorDive = float(par>>"minBackRotorDive")*(H_PI/180);
	_maxBackRotorDive = float(par>>"maxBackRotorDive")*(H_PI/180);
	_neutralBackRotorDive = float(par>>"neutralBackRotorDive")*(H_PI/180);
}

void HelicopterType::InitShape()
{
	const ParamEntry &par=*_par;

	_scopeLevel=2;
	base::InitShape();

//	_rotorHText.Init(_shape,"velka vrtule textura",NULL);
	_rotorH.Init(_shape,"velka vrtule",NULL,"velka osa");
	_rotorV.Init(_shape,"mala vrtule",NULL,"mala osa");
	_hRotorStill.Init(_shape,"velka vrtule staticka",NULL);
	_hRotorMove.Init(_shape,"velka vrtule blur",NULL);
	_vRotorStill.Init(_shape,"mala vrtule staticka",NULL);
	_vRotorMove.Init(_shape,"mala vrtule blur",NULL);
	_missileLPos=_shape->MemoryPoint("L strela");
	_missileRPos=_shape->MemoryPoint("P strela");
	_rocketLPos=_shape->MemoryPoint("L raketa");
	_rocketRPos=_shape->MemoryPoint("P raketa");
/*
	_lightPos=_shape->MemoryPoint("svetlo");
	_lightDir=_shape->MemoryPoint("konec svetla")-_lightPos;
	_lightDir.Normalize();
*/

	_mainRotorDiveAxis = _shape->MemoryPoint("predni osa naklonu");
	_backRotorDiveAxis = _shape->MemoryPoint("zadni osa naklonu");

	_altRadarIndicator.Init(_shape, par >> "IndicatorAltRadar");
	_altBaroIndicator.Init(_shape, par >> "IndicatorAltBaro");
	_speedIndicator.Init(_shape, par >> "IndicatorSpeed");
	_vertSpeedIndicator.Init(_shape, par >> "IndicatorVertSpeed");
	_rpmIndicator.Init(_shape, par >> "IndicatorRPM");
	_compass.Init(_shape, par >> "IndicatorCompass");
	_watch.Init(_shape, par >> "IndicatorWatch");

	_vario.Init(_shape, "horizont", NULL, "osa_horizont", NULL);
	for (int i=0; i<_shape->NLevels(); i++)
	{
		int sel = _vario.GetSelection(i);
		int selCenter = _vario.GetCenterSelection(i);
		if (sel >= 0 && selCenter >= 0)
		{
			Shape *shape = _shape->Level(i);
			_varioDirection[i] = _vario.Center(i) - shape->CalculateCenter(shape->NamedSel(sel));
		}
		else
			_varioDirection[i] = VForward;
	}
	
	_altRadarIndicator2.Init(_shape, par >> "IndicatorAltRadar2");
	_altBaroIndicator2.Init(_shape, par >> "IndicatorAltBaro2");
	_speedIndicator2.Init(_shape, par >> "IndicatorSpeed2");
	_vertSpeedIndicator2.Init(_shape, par >> "IndicatorVertSpeed2");
	_rpmIndicator2.Init(_shape, par >> "IndicatorRPM2");
	_compass2.Init(_shape, par >> "IndicatorCompass2");
	_watch2.Init(_shape, par >> "IndicatorWatch2");

	_vario2.Init(_shape, "horizont2", NULL, "osa_horizont2", NULL);
	for (int i=0; i<_shape->NLevels(); i++)
	{
		int sel = _vario2.GetSelection(i);
		int selCenter = _vario2.GetCenterSelection(i);
		if (sel >= 0 && selCenter >= 0)
		{
			Shape *shape = _shape->Level(i);
			_vario2Direction[i] = _vario2.Center(i) - shape->CalculateCenter(shape->NamedSel(sel));
		}
		else
			_vario2Direction[i] = VForward;
	}
	
	_hudPosition = _shape->MemoryPoint("HUD LH");
	_hudRight = _shape->MemoryPoint("HUD PH") - _hudPosition;
	_hudDown = _shape->MemoryPoint("HUD LD") - _hudPosition;

	// turret animations
	_turret.InitShape(par>>"Turret",_shape);

	_gunDir = _turret._dir;
	_gunPos = _turret._pos;

	
	int level=_shape->FindLevel(VIEW_GUNNER);
	if (level >= 0)
	{
		_shape->LevelOpaque(level)->MakeCockpit();
	}

	level=_shape->FindLevel(VIEW_PILOT);
	if( level>=0 )
	{
		_shape->LevelOpaque(level)->MakeCockpit();
		_pilotPos=_shape->LevelOpaque(level)->NamedPosition("pilot");
	}
	
	level=_shape->FindLevel(VIEW_CARGO);
	if( level<0 ) level=_shape->FindLevel(VIEW_PILOT);
	if( level>=0 )
	{
		_shape->LevelOpaque(level)->MakeCockpit();
	}

	if( _pilotPos.SquareSize()<0.1 ) _pilotPos=_shape->MemoryPoint("pilot");

	_animFire.Init(_shape, "zasleh", NULL);

	DEF_HIT(_shape,_hullHit,"trup",NULL,GetArmor()*(float)(par>>"armorHull"));
	DEF_HIT(_shape,_engineHit,"motor",NULL,GetArmor()*(float)(par>>"armorEngine"));
	DEF_HIT(_shape,_avionicsHit,"elektronika",NULL,GetArmor()*(float)(par>>"armorAvionics"));
	DEF_HIT(_shape,_rotorVHit,"mala vrtule",NULL,GetArmor()*(float)(par>>"armorVRotor"));
	DEF_HIT(_shape,_rotorHHit,"velka vrtule",NULL,GetArmor()*(float)(par>>"armorHRotor"));
	DEF_HIT(_shape,_missiles,"munice",NULL,GetArmor()*(float)(par>>"armorMissiles"));

	DEF_HIT(_shape,_glassRHit,"sklo predni P",NULL,GetArmor()*(float)(par>>"armorGlass"));
	DEF_HIT(_shape,_glassLHit,"sklo predni L",NULL,GetArmor()*(float)(par>>"armorGlass"));
	// attach hitpoint to convex component corresponding to selection "sklo"
	FindArray<int> hitsL,hitsR;
	_shape->FindHitComponents(hitsR,"sklo predni P");
	_shape->FindHitComponents(hitsL,"sklo predni L");
	_glassRHit.SetIndexCC(hitsR);
	_glassLHit.SetIndexCC(hitsL);

	{
		WoundInfo dammageInfo;
		dammageInfo.LoadAndRegister(_shape,par>>"dammageHalf");
		_glassDammageHalf.Init(_shape,dammageInfo,NULL,NULL);
	}
	{
		WoundInfo dammageInfo;
		dammageInfo.LoadAndRegister(_shape,par>>"dammageFull");
		_glassDammageFull.Init(_shape,dammageInfo,NULL,NULL);
	}
}

/*!
\patch 1.30 Date 8/23/2001 by Ondra.
- Fixed: Chinook too fast (bug in adjustable rotor angle simulation).
*/
static Vector3 BodyFriction( Vector3Val speed, float rotorEff, bool large)
{
	Vector3 friction;
	friction.Init();
	float stabXY=rotorEff*0.9+0.1;
	friction[0] = speed[0]*fabs(speed[0])*25+speed[0]*1500+fSign(speed[0])*40;
	if (!large)
	{
		friction[1] = speed[1]*fabs(speed[1])*3+speed[1]*500+fSign(speed[1])*15;
		friction[2] = speed[2]*fabs(speed[2])*1+speed[2]*100+fSign(speed[2])*6;
	}
	else
	{
		friction[1] = speed[1]*fabs(speed[1])*4+speed[1]*500+fSign(speed[1])*15;
		friction[2] =
		(
			speed[2]*fabs(speed[2])*1+
			speed[2]*100+fSign(speed[2])*6 +
			speed[2]*speed[2]*speed[2]*0.05f
		);
	}
	friction[0] *= stabXY;
	friction[1] *= stabXY;
	return friction;
}

/*!
\patch 1.43 Date 1/25/2002 by Ondra
- Fixed: Limited helicopter climbing in high altitudes.
Maximum possible altitude is now about 2500 m.
*/

static Vector3 RotorUpForce( Vector3Val speed, float coef, float alt )
{
	Vector3 force(VZero);
	float spd1=speed[1]+6-36*coef;
	//float spd1=speed[1]+3-18*coef;
	saturateMax(spd1,-5);
	force[1]+=speed[0]*speed[0]*-4+fabs(speed[0])*660;
	force[1]+=spd1*fabs(spd1)*-400+spd1*-14000;
	force[1]+=speed[2]*speed[2]*-4+fabs(speed[2])*660;
	saturateMax(force[1],-7000);

	/**/
	float fwdCoef=speed[2]*(1.0/20);
	saturate(fwdCoef,-1,1);
	force[2]+=force[1]*0.1*fwdCoef;
	/**/
	// limit rotor forces in high altitude
	const float altFullForce = 1000;
	const float altNoForce = 3000;
	if (alt>altFullForce)
	{
		float altCoef = 1-(alt-altFullForce)*(1/(altNoForce-altFullForce));
		force *= altCoef;
	}
	return force;
}

#define FAST_COEF (1.0/25) // use fast/slow simulation mode

/*!
\patch 1.14 Date 8/9/2001 by Ondra.
- Fixed: Apache Hellfires disappears from pylons after release.
\patch 1.21 Date 8/23/2001 by Ondra.
- Fixed: Bug in helicopter autorotating sometimes caused main rotor never stopped.
\patch 1.30 Date 8/23/2001 by Ondra.
- Fixed: Helicopter back rotor turning was too fast (since 1.29).
\patch 1.31 Date 11/08/2001 by Ondra.
- Fixed: MP: Remote prediction could cause helicopter or plane explosion.
This casued helicopters and planes crashing midair with no reason.
\patch 1.31 Date 11/20/2001 by Ondra.
- Fixed: Helicopter mouse turning was inconvenient, esp. for light choppers (since 1.29).
\patch 1.61 Date 5/30/2002 by Ondra
- Fixed: Helicopters can no longer stay under water.
*/

void Helicopter::Simulate( float deltaT, SimulationImportance prec )
{
	_isDead = IsDammageDestroyed();

	if( _rotorSpeed>0 )
	{
		ConsumeFuel(deltaT*0.03);
	}

	ConsumeFuel(deltaT*GetHit(Type()->_hullHit)*0.2);
	
	//Vector3Val position=Position();

	// calculate all forces, frictions and torques
	Vector3Val speed=ModelSpeed();
	Vector3 force(VZero),friction(VZero);
	Vector3 torque(VZero),torqueFriction(VZero);
	
	// world space center of mass
	Vector3 wCenter(VFastTransform,ModelToWorld(),GetCenterOfMass());

	// we consider following forces
	// gravity
	// main rotor (in direction of rotor axis, changed by cyclic, controled by thrust)
	// back rotor (in direction of rotor axis - controled together with cyclic)
	// body aerodynamics (air friction)
	// main rotor aerodynamics (air friction and "up" force)
	// body and rotor torque friction (air friction)

	Vector3 pForce(VZero); // partial force
	Vector3 pCenter(VZero); // partial force application point
	
	float delta;

	if( _isDead )
	{
		if( _landContact || _waterContact || _objectContact ) EngineOffAction();
	}
	if( _fuel<=0 ) EngineOffAction();

	{
		if( _rotorSpeedWanted>0.2 || _rotorSpeed>0.1 ) IsMoved();

		// handle impulse
		float impulse2=_impulseForce.SquareSize();
		if( impulse2>Square(GetMass()*0.01f) )
		{
			IsMoved();
		}
	}

	if (GetStopped())
	{
		// reset impulse - avoid cummulation
		_impulseForce = VZero;
		_impulseTorque = VZero;
	}

	_rotorPosition+=_rotorSpeed*deltaT*20;
	// keep number small, is equivalent mod 2 (see usage of _rotorPosition)
	_rotorPosition=fastFmod(_rotorPosition,4*3*5*H_PI);
	
	if( !_isStopped && !CheckPredictionFrozen())
	{

		saturateMin(_rotorSpeedWanted,1-GetHit(Type()->_engineHit));
		// main engine
		delta=_rotorSpeedWanted-_rotorSpeed;
		float brakeRotor = _mainRotor;
		saturate(brakeRotor,-0.2f,1); // if we apply rotor up, rotor will stop
		// auto-rotate - when going down, rotor will move faster
		float goingDown = (-ModelSpeed().Y()-2)*0.25f;
		saturateMax(goingDown,0);
		//GlobalShowMessage(200,"brake %.2f, down %.2f",brakeRotor,goingDown);
		saturateMax(brakeRotor,-goingDown);

		float maxDown = -0.025f - brakeRotor * 0.37f;
		Limit(delta,maxDown*deltaT,+0.05f*deltaT);
		_rotorSpeed+=delta;
		Limit(_rotorSpeed,0,1);

		//GlobalShowMessage
		//(
		//	100,"Rotor %.3f, mr %.3f, auto %.3f (%.3f)",
		//	_rotorSpeed,_mainRotor,brakeRotor,maxDown
		//);


		// main rotor thrust
		// change main rotor force
		float mrw = floatMin(_mainRotorWanted,_rotorSpeed);
		delta = mrw-_mainRotor;
		Limit(delta,-0.25*deltaT,+0.25*deltaT);
		_mainRotor+=delta;
		Limit(_mainRotor,-0.2,_rotorSpeed);

		// change back rotor force
		delta=_backRotorWanted-_backRotor;
		Limit(delta,-10*deltaT,+10*deltaT);
		_backRotor+=delta;
		Limit(_backRotor,-1,+1);
		
		// change cyclic
		Limit(_cyclicAsideWanted,-2,+2);
		delta=_cyclicAsideWanted-_cyclicAside;
		Limit(delta,-10*deltaT,10*deltaT);
		_cyclicAside+=delta,Limit(_cyclicAside,-1,1);

		delta=_cyclicForwardWanted-_cyclicForward;
		Limit(delta,-10*deltaT,10*deltaT);
		_cyclicForward+=delta,Limit(_cyclicForward,-1,1);

		Limit(_rotorDiveWanted,-1,+1);
		delta=_rotorDiveWanted-_rotorDive;
		Limit(delta,-0.15*deltaT,0.15*deltaT);
		_rotorDive+=delta,Limit(_rotorDive,-1,1);

		// force applied to the center of the rotor
		// it is forced to be aligned with center of mass
		//Vector3 sideOffset(VZero);
		pCenter.Init();
		pCenter[0]=_cyclicAside*0.8;
		pCenter[2]=_cyclicForward*-0.8;
		pCenter[1]=0;
		// apply aerodynamics of the main rotor
		float rotorSpeed=_rotorSpeed;
		if( _isDead ) rotorSpeed=0;
		if( rotorSpeed>0.2 )
		{
			float massCoef=GetMass()*(1.0/3000);
			saturate(massCoef,1,3);
			pForce = 
			(
				RotorUpForce(speed,_mainRotor*rotorSpeed,Position().Y())*
				(_rotorSpeed*_rotorSpeed*massCoef)
			);
				// rotate force by main rotor orientation
			if (fabs(_rotorDive)>1e-3)
			{
				Matrix3 rotDive(MRotationX,_rotorDive);
				pForce = rotDive * pForce;
			}


		}
		else pForce=VZero;
		#if ARROWS
			AddForce
			(
				DirectionModelToWorld(pCenter)+wCenter,
				DirectionModelToWorld(pForce)*InvMass()
			);
		#endif
		force+=pForce;
		torque+=pCenter.CrossProduct(pForce);

		// rotate helicopter when in free fall mode
		if( !_landContact && _rotorSpeed<0.1 )
		{
			torque+=Vector3(0.5,3.2,1.3).CrossProduct(Vector3(0,GetMass(),0));
		}
		
		float backRadius=_shape->BoundingSphere()*0.95;
		// when moving fast, side bank causes torque
		float bank=DirectionAside().Y();
		float fastTurn=fabs(speed[2]-3)*FAST_COEF;
		Limit(fastTurn,0,1); // back rotor has always some significance
		float sizeFactor=backRadius*(1.0/12);
		pForce=Vector3
		(
			(bank*10*fastTurn+_backRotor*4*floatMax(1-fastTurn,0.1))
			*(_rotorSpeed*_rotorSpeed*GetMass()*sizeFactor),
			0,0
		);
		pCenter=Vector3(0,+backRadius*0.08,-backRadius);
		torque+=pCenter.CrossProduct(pForce);

		#if ARROWS
			AddForce
			(
				DirectionModelToWorld(pCenter)+wCenter,
				DirectionModelToWorld(pForce)*InvMass()
			);
		#endif
		
		// convert forces to world coordinates

		DirectionModelToWorld(torque,torque);
		DirectionModelToWorld(force,force);
		
		// angular velocity causes also some angular friction
		// this should be simulated as torque
		torqueFriction=_angMomentum*(0.2+_rotorSpeed*_rotorSpeed*2);
		
		// calculate new position
		Matrix4 movePos;
		ApplySpeed(movePos,deltaT);
		Frame moveTrans;
		moveTrans.SetTransform(movePos);

		// model space turbulence calculation

		if( Glob.time>_lastTurbulenceTime+2 )
		{
			_lastTurbulenceTime=Glob.time;
			const float maxXT=1;
			const float maxYT=1;
			const float maxZT=1;
			float tx=(GRandGen.RandomValue()-0.5)*(maxXT*2);
			float ty=(GRandGen.RandomValue()-0.5)*(maxYT*2);
			float tz=(GRandGen.RandomValue()-0.5)*(maxZT*2);
			_turbulence=Vector3(tx,ty,tz);
		}

		// body air friction
		Vector3 wind = GLandscape->GetWind()+_turbulence;
		Vector3 airSpeed = speed + DirectionWorldToModel(wind);
		friction=BodyFriction
		(
			airSpeed,_rotorSpeed*_rotorSpeed,
			Type()->_maxMainRotorDive>0.01
		);

		DirectionModelToWorld(friction,friction);

		#if ARROWS
			AddForce
			(
				wCenter,-friction*InvMass(),PackedColor(Color(0.5,0,0))
			);
		#endif
		
		// gravity - no torque
		pForce=Vector3(0,-1,0)*(GetMass()*G_CONST);
		force+=pForce;
		#if ARROWS
			AddForce(wCenter,pForce*InvMass());
		#endif

		
		// recalculate COM to reflect change of position
		wCenter.SetFastTransform(moveTrans.ModelToWorld(),GetCenterOfMass());
		if( deltaT>0 )
		{
			bool wasLandContact = _landContact;
			_objectContact=false;
			_landContact=false;
			_waterContact=false;

			Vector3 totForce(VZero);

			float crash=0;

			if( prec<=SimulateVisibleFar && IsLocal())
			{
				CollisionBuffer collision;
				GLOB_LAND->ObjectCollision(collision,this,moveTrans);
				#define MAX_IN 0.4
				#define MAX_IN_FORCE 0.1
				#define MAX_IN_FRICTION 0.4

				for( int i=0; i<collision.Size(); i++ )
				{
					// info.pos is relative to object
					CollisionInfo &info=collision[i];
					if (info.object && !info.object->IsPassable())
					{
						_objectContact=true;
						float cFactor=1;
						if( info.object->GetMass()<50 ) continue;
						if( info.object->GetType()==Primary )
						{
							cFactor=1;
						}
						else
						{
							cFactor=info.object->GetMass()*GetInvMass();
							saturate(cFactor,0,5);
						}
						if( cFactor>0.05 )
						{
							Vector3Val pos=info.object->PositionModelToWorld(info.pos);
							Vector3Val dirOut=info.object->DirectionModelToWorld(info.dirOut);
							// create a force pushing "out" of the collision
							float forceIn=floatMin(info.under,MAX_IN_FORCE);
							Vector3 pForce=dirOut*GetMass()*40*forceIn;
							// apply proportional part of force in place of impact
							pCenter=pos-wCenter;
							totForce+=pForce;
							torque+=pCenter.CrossProduct(pForce);
							
							Vector3Val objSpeed=info.object->ObjectSpeed();
							Vector3 colSpeed=_speed-objSpeed;

							// if info.under is bigger than MAX_IN, move out
							if( info.under>MAX_IN )
							{
								Vector3 newPos=moveTrans.Position();
								float moveOut=info.under-MAX_IN;
								if( moveOut>0.1 ) crash=0.5;
								newPos+=dirOut*moveOut*0.1;
								moveTrans.SetPosition(newPos);
							}
							saturateMax(crash, (colSpeed.SquareSize()-25)*(1.0/25));

							const float maxRelSpeed=5;
							if( colSpeed.SquareSize()>Square(maxRelSpeed) )
							{
								// adapt _speed to match criterion
								colSpeed.Normalize();
								colSpeed*=maxRelSpeed;
								// only slow down
								float oldSize = _speed.Size();
								_speed = colSpeed+objSpeed;
								if (_speed.SquareSize()>Square(oldSize))
								{
									_speed = _speed.Normalized()*oldSize;
								}
							}


							// second is "land friction" - causing no momentum

							float frictionIn=floatMin(info.under,MAX_IN_FRICTION);
							pForce[0]=fSign(speed[0])*20000;
							pForce[1]=speed[1]*fabs(speed[1])*1000+speed[1]*8000+fSign(speed[1])*10000;
							pForce[2]=speed[2]*fabs(speed[2])*150+speed[2]*250+fSign(speed[2])*2000;

							pForce=DirectionModelToWorld(pForce)*GetMass()*(4.0/10000)*frictionIn;
							//saturateMin(pForce[1],0);
							//torque-=pCenter.CrossProduct(pForce);
							#if ARROWS
								AddForce(wCenter+pCenter,-pForce*InvMass());
							#endif
							friction+=pForce;
							torqueFriction+=_angMomentum*0.15;
						}
					}
				}
			} // if( object collisions enabled )
			
			GroundCollisionBuffer gCollision;
			bool enableLandcontact = true;
			if (wasLandContact && DirectionUp().Y()<0.985f) // cos 10 deg
			{
				enableLandcontact = false;
			}
			GLOB_LAND->GroundCollision(gCollision,this,moveTrans,0.05f,0,enableLandcontact);

			if( gCollision.Size()>0 )
			{

				Vector3 gFriction(VZero);
				float maxUnder=0;
				float maxUnderWater = 0;
				#define MAX_UNDER 0.1f
				#define MAX_UNDER_FORCE 0.1f
				
				Shape *landcontact = GetShape()->LandContactLevel();
				int nContactPoint = landcontact ? landcontact->NPos() : 3;
				saturateMax(nContactPoint,gCollision.Size());
				const float nPointCoef = 3.0f/nContactPoint;
				for( int i=0; i<gCollision.Size(); i++ )
				{
					// info.pos is world space
					UndergroundInfo &info=gCollision[i];
					if( info.under<0 ) continue;
					// we consider two forces
					//ReportFloat("land",info.under);
					float under;
					if( info.type==GroundWater )
					{
						under=floatMin(info.under,3)*0.001f;
						if( maxUnderWater<info.under ) maxUnderWater=info.under;
						_waterContact=true;
						if( _speed.SquareSize()>Square(8) ) crash=2;
					}
					else
					{
						_landContact=true;
						// we consider two forces
						//ReportFloat("land",info.under);
						if( maxUnder<info.under ) maxUnder=info.under;
						under=floatMin(info.under,MAX_UNDER_FORCE);
					}
					// one is ground "pushing" everything out - causing some momentum
					Vector3 dirOut=Vector3(0,info.dZ,1).CrossProduct(Vector3(1,info.dX,0)).Normalized();
					pForce=dirOut*GetMass()*40.0f*nPointCoef*under;
					pCenter=info.pos-wCenter;
					torque+=pCenter.CrossProduct(pForce);
					// to do: analyze ground reaction force
					totForce+=pForce;

					#if ARROWS
						AddForce(wCenter+pCenter,pForce*under*InvMass());
					#endif
					
					// second is "land friction" - causing momentum
					pForce[0]=speed[0]*5000+fSign(speed[0])*10000;
					pForce[1]=speed[1]*fabs(speed[1])*1000+speed[1]*8000+fSign(speed[1])*10000;
					pForce[2]=speed[2]*fabs(speed[2])*150+speed[2]*250+fSign(speed[2])*5000;
					
					pForce=DirectionModelToWorld(pForce)*GetMass()*nPointCoef*(1.0f/10000);
					#if ARROWS
						AddForce(wCenter+pCenter,-pForce*InvMass());
					#endif
					friction+=pForce;

					// torque applied if speed is big enough
					if( fabs(speed[0])<1 ) pForce[0]=0;
					if( fabs(speed[1])<1 ) pForce[1]=0;
					if( fabs(speed[2])<1 ) pForce[2]=0;
					torque-=pCenter.CrossProduct(pForce); // sub: it is friction

					torqueFriction+=_angMomentum*info.under*3;
				}
				//torqueFriction=_angMomentum*1.0;
				saturateMax(crash, (_speed.SquareSize()-25)*(1.0f/30));
				if( maxUnder>MAX_UNDER )
				{
					// it is neccessary to move object immediatelly
					Vector3 newPos=moveTrans.Position();
					float moveUp=maxUnder-MAX_UNDER;
					newPos[1]+=moveUp;
					moveTrans.SetPosition(newPos);

					if (_speed.SquareSize()>Square(10))
					{
						_speed.Normalize();
						_speed *= 10;

					}
					saturateMax(_speed[1],-0.1f);
				}
				//GlobalShowMessage(100,"maxUnder %.2f",maxUnder);
				const float maxFord=1.1f;
				if( maxUnderWater>maxFord )
				{
					float dammage=(maxUnderWater-maxFord)*0.5f;
					saturateMin(dammage,0.2f);
					LocalDammage(NULL,this,VZero,dammage*deltaT,GetRadius());
				}
			}
			force+=totForce;
			if (crash>0)
			{
				if( Glob.time>_disableDammageUntil )
				{
					_disableDammageUntil = Glob.time+0.5f;
					// crash boom bang state - impact speed too high
					//LogF("Heli Crash %g",crash);
					_doCrash=CrashLand;
					if( _objectContact ) _doCrash=CrashObject;
					if( _waterContact ) _doCrash=CrashWater;
					_crashVolume=crash;
					saturateMax(_crashVolume,crash);

					CrashDammage(crash);
					if (IsLocal() && crash>5)
					{
						// set some explosion
						float maxTime = 5-crash*0.2f;
						saturate(maxTime,0.5f,3);
						if (_explosionTime>Glob.time+maxTime)
						{
							_explosionTime=Glob.time+GRandGen.Gauss(0,maxTime*0.3f,maxTime);
						}
					}
				}
			}
		}

		bool stopCondition=false;
		if( _landContact && !_waterContact && !_objectContact )
		{
			// apply static friction
			float maxSpeed=Square(0.7f);
			if( !Driver() ) maxSpeed=Square(1.2f);
			if( _speed.SquareSize()<maxSpeed && _angVelocity.SquareSize()<maxSpeed*0.3f )
			{
				stopCondition=true;
			}
		}
		if( stopCondition) StopDetected();
		else IsMoved();

		// apply all forces

		_lastAngVelocity=_angVelocity; // helper for prediction
		ApplyForces(deltaT,force,torque,friction,torqueFriction);

		//LogF("ang vel %.3f,%.3f,%.3f",_angVelocity[0],_angVelocity[1],_angVelocity[2]);
		//LogF("ang mom %.3f,%.3f,%.3f",_angMomentum[0],_angMomentum[1],_angMomentum[2]);

		_turret.Stabilize
		(
			this, Type()->_turret,
			Transform().Orientation(), moveTrans.Orientation()
		);

		// simulate head position
		// calculate how pilot's head is moved is world space between the frames
		// new vehicle position is in moveTrans
		// old is in Transform()
		if( prec<=SimulateCamera ) _head.Move(deltaT,moveTrans,*this);
		Move(moveTrans);
		DirectionWorldToModel(_modelSpeed,_speed);
	}

	if( _isDead && ( _landContact || _objectContact ) )
	{
		NeverDestroy();
		SmokeSourceVehicle *smoke=dyn_cast<SmokeSourceVehicle>(GetSmoke());
		if( smoke )
		{
			smoke->SetExplosion(100,100);
			smoke->Explode();
		}
		// note: high speed during contact causes explosion
	}

	// machine gun light and clouds
	if (EnableVisualEffects(prec))
	{
		if (_mGunClouds.Active() || _mGunFire.Active())
		{
			Matrix4Val toWorld = Transform() * GunTransform();
			Vector3Val dir = toWorld.Direction();
			Vector3 gunPos(VFastTransform,toWorld,Type()->_gunPos);
			_mGunClouds.Simulate(gunPos,Speed()*0.7+dir*5.0,0.35,deltaT);
			_mGunFire.Simulate(gunPos,deltaT);
		}
	}

	base::Simulate(deltaT,prec);
}

/*!
\patch 1.89 Date 10/24/2002 by Ondra
- Fixed: Helicopers appeared white on radar after Load/Retry.
*/

bool HelicopterAuto::EngineIsOn() const
{
	return _rotorSpeedWanted>0.5f;
}

void HelicopterAuto::EngineOn()
{
	_rotorSpeedWanted=1;
	base::EngineOn();
	// keep landed
	//_pilotHeight=0;
}
void HelicopterAuto::EngineOff()
{
	//base::EngineOff();
	//_rotorSpeedWanted=0;
}
void HelicopterAuto::EngineOffAction()
{
	StopRotor();
}

float HelicopterAuto::MakeAirborne()
{
	EngineOn();
	_rotorSpeed=_rotorSpeedWanted=1.0;
	_mainRotor=_mainRotorWanted=0.7;
	_pilotHeight=_defPilotHeight;
	_landContact=false;
	_objectContact=false;
	return _defPilotHeight;
}

void HelicopterAuto::SetFlyingHeight(float val)
{
	_defPilotHeight = val;
}

bool Helicopter::Airborne() const
{
	return !_landContact;
}

void HelicopterAuto::StopRotor()
{
	_rotorSpeedWanted=0;
	base::EngineOff();
}

float Helicopter::GetEngineVol( float &freq ) const
{
	freq=_rndFrequency*_rotorSpeed*(1-_mainRotor*0.1);
	return 1;
}

float Helicopter::GetEnvironVol( float &freq ) const
{
	freq=1;
	return _speed.Size()/Type()->GetMaxSpeedMs();
}

Texture *Helicopter::GetCursorTexture(Person *person)
{
	return base::GetCursorTexture(person);
}

Texture *Helicopter::GetCursorAimTexture(Person *person)
{
	// check if target designator is active
	if (_laserTargetOn)
	{
		return GPreloadedTextures.New(CursorLocked);
	}
	return base::GetCursorAimTexture(person);
}

void Helicopter::Sound( bool inside, float deltaT )
{
	_turret.Sound(Type()->_turret,inside,deltaT,*this,Speed());
	base::Sound(inside,deltaT);
}

void Helicopter::UnloadSound()
{
	base::UnloadSound();
	_turret.UnloadSound();
}

bool Helicopter::HasFlares( CameraType camType ) const
{
	return camType!=CamInternal && camType!=CamGunner;
}

Matrix4 Helicopter::InsideCamera( CameraType camType ) const
{
	return base::InsideCamera(camType);
}

int Helicopter::InsideLOD( CameraType camType ) const
{
	return base::InsideLOD(camType);
}

void Helicopter::InitVirtual( CameraType camType, float &heading, float &dive, float &fov ) const
{
	base::InitVirtual(camType,heading,dive,fov);
/*
	switch( camType )
	{
		case CamGunner:
			fov=0.50;
		break;
	}
*/
}

void Helicopter::LimitVirtual( CameraType camType, float &heading, float &dive, float &fov ) const
{
	base::LimitVirtual(camType,heading,dive,fov);
/*
	switch( camType )
	{
		case CamGunner:
			saturate(fov,0.3,1.2);
			saturate(heading,-1.8,+1.8);
			saturate(dive,-0.7,+0.3);
		break;
	}
*/
}

Matrix4 Helicopter::GunTransform() const
{
/*
	return
	(
		Matrix4(MTranslation,Type()->_gunAxis)*
		Matrix4(MRotationY,_gunYRot)*
		Matrix4(MRotationX,-_gunXRot)*
		Matrix4(MTranslation,-Type()->_gunAxis)
	);
*/
	// animate matrix connected with selection Type()->_mainTurret._gun
	int memory = GetShape()->FindMemoryLevel();
	int sel = Type()->_turret._gun.GetSelection(memory);
	if (sel>=0)
	{
		Matrix4 mat=MIdentity;
		AnimateMatrix(mat,memory,sel);
		return mat;
	}
	return MIdentity;
}

bool Helicopter::IsAnimated( int level ) const {return true;}
bool Helicopter::IsAnimatedShadow( int level ) const {return true;}

void Helicopter::AnimateMatrix(Matrix4 &mat, int level, int selection) const
{
	// default proxy transform calculation
	// check which selection is the proxy in:

	_turret.AnimateMatrix(Type()->_turret,mat,this,level,selection);
}

Vector3 Helicopter::AnimatePoint( int level, int index ) const
{
	// note: only turret/gun animation is done here
	// check which animations is this point in

	Shape *shape = _shape->Level(level);
	if( !shape ) return VZero;
	shape->SaveOriginalPos();

	Vector3 pos = shape->OrigPos(index);

	_turret.AnimatePoint(Type()->_turret,pos,this,level,index);

	return pos;
}

// TODO: move broken glass to Transport class
inline float Helicopter::GetGlassBroken() const
{
	const HelicopterType *type = Type();
	float glassDammage = GetHitCont(type->_hullHit);
	saturateMax(glassDammage,GetTotalDammage());
	saturateMax(glassDammage,GetHitCont(type->_glassLHit));
	saturateMax(glassDammage,GetHitCont(type->_glassRHit));
	return glassDammage;
}

void Helicopter::DammageAnimation( int level )
{
	const HelicopterType *type = Type();
	// scan corresponding wound

	float glassDammage = GetGlassBroken();
	if (glassDammage>=0.6)
	{
		type->_glassDammageFull.Apply(_shape,level);
	}
	else if (glassDammage>=0.3)
	{
		type->_glassDammageHalf.Apply(_shape,level);
	}
}

void Helicopter::DammageDeanimation( int level )
{
	const HelicopterType *type = Type();
	// scan corresponding wound
	float glassDammage = GetGlassBroken();
	if (glassDammage>=0.6)
	{
		type->_glassDammageFull.Restore(_shape,level);
	}
	else if (glassDammage>=0.3)
	{
		type->_glassDammageHalf.Restore(_shape,level);
	}
}

void Helicopter::Animate( int level )
{
	if (_rotorSpeed < 0.6)
	{
		Type()->_hRotorMove.Hide(_shape, level);
		Type()->_vRotorMove.Hide(_shape, level);
	}
	else
	{
		Type()->_hRotorStill.Hide(_shape, level);
		Type()->_vRotorStill.Hide(_shape, level);
	}
	// if drawing shadows, old style animation is required?

	if (Type()->_rotorH.GetSelection(level)>=0)
	{
		Matrix4 rot;
		Type()->_rotorH.GetRotation(rot,_rotorPosition*Type()->_mainRotorSpeed,level);

		float diveAngle = _rotorDive +Type()->_neutralMainRotorDive;
		if (fabs(diveAngle)>1e-3)
		{
			saturate(diveAngle,Type()->_minMainRotorDive,Type()->_maxMainRotorDive);
			Matrix4 dive(MRotationX,diveAngle);

			Matrix4 diveAxis(MTranslation,Type()->_mainRotorDiveAxis);
			Matrix4 diveAxisInv(MTranslation,-Type()->_mainRotorDiveAxis);

			Matrix4 comb = diveAxis*dive*diveAxisInv*rot;

			Type()->_rotorH.Transform(_shape,comb,level);
		}
		else
		{
			Type()->_rotorH.Transform(_shape,rot,level);
		}
	}

	if (Type()->_rotorV.GetSelection(level)>=0)
	{
		Matrix4 rot;
		Type()->_rotorV.GetRotation(rot,_rotorPosition*Type()->_backRotorSpeed,level);

		float diveAngle = _rotorDive +Type()->_neutralBackRotorDive;

		if (fabs(diveAngle)>1e-3)
		{
			saturate(diveAngle,Type()->_minBackRotorDive,Type()->_maxBackRotorDive);
			Matrix4 dive(MRotationX,diveAngle);

			Matrix4 diveAxis(MTranslation,Type()->_backRotorDiveAxis);
			Matrix4 diveAxisInv(MTranslation,-Type()->_backRotorDiveAxis);

			Matrix4 comb = diveAxis*dive*diveAxisInv*rot;

			Type()->_rotorV.Transform(_shape,comb,level);
		}
		else
		{
			Type()->_rotorV.Transform(_shape,rot,level);
		}
	}

	// indicators
	float value = fastFmod(Position().Y(), 304);
	Type()->_altRadarIndicator.SetValue(_shape, level, value);
	Type()->_altRadarIndicator2.SetValue(_shape, level, value);
	value = Position().Y() - GLandscape->SurfaceYAboveWater(Position().X(), Position().Z());
	Type()->_altBaroIndicator.SetValue(_shape, level, value);
	Type()->_altBaroIndicator2.SetValue(_shape, level, value);
	value = fabs(ModelSpeed()[2]);
	Type()->_speedIndicator.SetValue(_shape, level, value);
	Type()->_speedIndicator2.SetValue(_shape, level, value);
	value = _speed.Y();
	Type()->_vertSpeedIndicator.SetValue(_shape, level, value);
	Type()->_vertSpeedIndicator2.SetValue(_shape, level, value);
	float rpm = _rotorSpeed*(1-_mainRotor*0.1);
	value = 10.0 * rpm;
	Type()->_rpmIndicator.SetValue(_shape, level, value);
	Type()->_rpmIndicator2.SetValue(_shape, level, value);
	value = atan2(Direction().X(), Direction().Z());
	Type()->_compass.SetValue(_shape, level, value);
	Type()->_compass2.SetValue(_shape, level, value);
	Type()->_watch.SetTime(_shape, level, Glob.clock);
	Type()->_watch2.SetTime(_shape, level, Glob.clock);
	
	// vario
	{
		Vector3Val dir = Type()->_varioDirection[level];
		// transformations from / into vertical plane
		Matrix3 rot(MDirection, dir, VUp);
		Matrix3 invRot(MInverseRotation, rot);
		
		Vector3 up = InvTransform().DirectionUp();
		up[2] = -up[2];

		// transform into vertical plane
		up = invRot * up;
		Matrix3 orient(MUpAndDirection, up, VForward);
		// back into vario plane
		orient = rot * orient;

		Matrix4 trans;
		trans.SetPosition(VZero);
		trans.SetOrientation(orient);
		Type()->_vario.Apply(_shape, trans, level);
	}

	// vario2
	{
		Vector3Val dir = Type()->_vario2Direction[level];
		// transformations from / into vertical plane
		Matrix3 rot(MDirection, dir, VUp);
		Matrix3 invRot(MInverseRotation, rot);
		
		Vector3 up = InvTransform().DirectionUp();
		up[2] = -up[2];

		// transform into vertical plane
		up = invRot * up;
		Matrix3 orient(MUpAndDirection, up, VForward);
		// back into vario plane
		orient = rot * orient;

		Matrix4 trans;
		trans.SetPosition(VZero);
		trans.SetOrientation(orient);
		Type()->_vario2.Apply(_shape, trans, level);
	}

	if (_mGunFireFrames > 0 || Glob.uiTime < _mGunFireTime + 0.05)
	{
		Type()->_animFire.Unhide(_shape, level);
		Type()->_animFire.SetPhase(_shape, level, _mGunFirePhase);
		_mGunFireFrames--;
	}
	else
	{
		Type()->_animFire.Hide(_shape, level);
	}

	// set gun and turret to correct position
	// calculate animation transformation
	// turret transformation
	_turret.Animate(Type()->_turret,this,level);

	DammageAnimation(level);

	base::Animate(level);
//	SelectTexture(level,_rotorSpeed);
	// TODO: animate min-max box
	// set minmax box and sphere
	Shape *shape = GetShape()->Level(level);
	float factor = 1.1;
	Vector3 min = shape->MinOrig();
	Vector3 max = shape->MaxOrig();
	Vector3 bCenter = shape->BSphereCenterOrig();
	float bRadius = shape->BSphereRadiusOrig();
	// enlarge twice to be sure soldier will fit it
	min = bCenter+(min-bCenter)*factor;
	max = bCenter+(max-bCenter)*factor;
	bRadius *=factor;
	shape->SetMinMax(min,max,bCenter,bRadius);
}

void Helicopter::Deanimate( int level )
{
	DammageDeanimation(level);

	base::Deanimate(level);
	Type()->_rotorH.Restore(_shape,level);
	Type()->_rotorV.Restore(_shape,level);
//	SelectTexture(level,0);
	Type()->_hRotorStill.Unhide(_shape, level);
	Type()->_hRotorMove.Unhide(_shape, level);
	Type()->_vRotorStill.Unhide(_shape, level);
	Type()->_vRotorMove.Unhide(_shape, level);

	_turret.Deanimate(Type()->_turret,_shape,level);
}

void Helicopter::DoTransform
(
	TLVertexTable &dst,
	const Shape &src, const Matrix4 &posView,
	int from, int to
) const
{
	// perform default transform
	base::DoTransform(dst,src,posView,from,to);
}

bool Helicopter::IsPossibleToGetIn() const
{
	if (GetHit(Type()->_engineHit)>=0.5) return false;
	return base::IsPossibleToGetIn();
}

bool Helicopter::IsAbleToMove() const
{
	if (GetHit(Type()->_engineHit)>=0.5) return false;
	return base::IsAbleToMove();
}

void Helicopter::Draw(int level, ClipFlags clipFlags, const FrameBase &pos)
{
	base::Draw(level, clipFlags, pos);
	
	Vector3 origin = Type()->_hudPosition;
	Vector3 right = Type()->_hudRight;
	Vector3 down = Type()->_hudDown;

	Vector3 normal = down.CrossProduct(right).Normalized(); 
	origin = origin - 0.002 * normal;

	if (right.SquareSize() < Square(0.001) || down.SquareSize() < Square(0.001)) return;

	pos.PositionModelToWorld(origin, origin);
	pos.DirectionModelToWorld(right, right);
	pos.DirectionModelToWorld(down, down);

	PackedColor color(Color(0, 1, 0, 0.03));
/*
	GEngine->DrawLine3D(origin, origin + right, color, 0);
	GEngine->DrawLine3D(origin + right, origin + right + down, color, 0);
	GEngine->DrawLine3D(origin + right + down, origin + down, color, 0);
	GEngine->DrawLine3D(origin + down, origin, color, 0);
*/
	Vector3 tl = origin + 0.25 * right;
	Vector3 tr = origin + 0.75 * right;
	Vector3 br = origin + 0.75 * right + down;
	Vector3 bl = origin + 0.25 * right + down;
	GEngine->DrawLine3D(tl, tr, color, 0);
	GEngine->DrawLine3D(tr, br, color, 0);
	GEngine->DrawLine3D(br, bl, color, 0);
	GEngine->DrawLine3D(bl, tl, color, 0);

	tl = origin + 0.25 * down;
	tr = origin + 0.25 * down + right;
	br = origin + 0.75 * down + right;
	bl = origin + 0.75 * down;
	GEngine->DrawLine3D(tl, tr, color, 0);
	GEngine->DrawLine3D(tr, br, color, 0);
	GEngine->DrawLine3D(br, bl, color, 0);
	GEngine->DrawLine3D(bl, tl, color, 0);

	// texts
	static Ref<Font> font;
	if (!font) font = GEngine->LoadFont(GetFontID("tahomaB48"));

	char text[256];
	PackedColor textColor(Color(0, 1, 0, 0.5));
	
	Vector3 textUp = -0.4 * 0.25 * down;
	Vector3 textRight = 0.75 * textUp.Size() * right.Normalized();
	float invRSize = 1.0 / textRight.Size();
	
	// fuel
	Vector3 textPos = origin + 0.6 * 0.25 * down;
	float value = 100.0f * GetFuel() / Type()->GetFuelCapacity();
	sprintf(text, "%.0f%%", value);
	/*
	Vector3 width = GEngine->GetText3DWidth
	(
		textRight, font, text
	);
	*/
	float x2c = 0.25 * right.Size() * invRSize;
	GEngine->DrawText3D
	(
		textPos, textUp, textRight, ClipAll | (MSShining * ClipUserStep),
		font, textColor, 0, text,
		0, 0, x2c, 1
	);

	Target *target = GetFireTarget();
	if (target)
	{
		// target distance
		textPos = origin + 0.75 * right + 0.6 * 0.25 * down;
		value = target->AimingPosition().Distance(Position());
		sprintf(text, "%.0f", value);
		x2c = 0.25 * right.Size() * invRSize;
		GEngine->DrawText3D
		(
			textPos, textUp, textRight, ClipAll | (MSShining * ClipUserStep),
			font, textColor, 0, text,
			0, 0, x2c, 1
		);
	}

	// speed
	textPos = origin + 0.75 * down;
	value = 3.6 * fabs(ModelSpeed()[2]);
	sprintf(text, "%.0f", value);
	x2c = 0.25 * right.Size() * invRSize;
	GEngine->DrawText3D
	(
		textPos, textUp, textRight, ClipAll | (MSShining * ClipUserStep),
		font, textColor, 0, text,
		0, 0, x2c, 1
	);

	// height
	textPos = origin + 0.75 * right + 0.75 * down;
	value = Position().Y() + GetShape()->Min().Y() - GLandscape->SurfaceYAboveWater(Position().X(), Position().Z());
	sprintf(text, "%d", toInt(value));
	x2c = 0.25 * right.Size() * invRSize;
	GEngine->DrawText3D
	(
		textPos, textUp, textRight, ClipAll | (MSShining * ClipUserStep),
		font, textColor, 0, text,
		0, 0, x2c, 1
	);
}

// basic autopilot

HelicopterAuto::HelicopterAuto( VehicleType *name, Person *pilot )
:Helicopter(name,pilot),_pilotHeading(0),_pilotSpeed(VZero),_pilotHeight(2.5),
_pilotHeadingSet(false),
_pilotDiveSet(false),
_hoveringAutopilot(false),
_dirCompensate(0.5),
_defPilotHeight(50),
_forceDive(1),
_pilotDive(0), // dive set by pilot
_pilotHeightHelper(true), // keyboard helper activated
_pilotSpeedHelper(true), // keyboard helper activated
_pilotDirHelper(true), // keyboard helper activated
_avoidBankJitter(false),
_state(AutopilotNear),
_pressedForward(false),_pressedBack(false),
_pressedUp(false),_pressedDown(false),
_sweepDelay(Glob.time),
_sweepState(SweepDisengage),
_stopMode(SMNone),
_stopPosition(VZero)
{
}

void HelicopterAuto::Simulate( float deltaT, SimulationImportance prec )
{
	if( !_driver && !_isDead ) StopRotor();

	SimulateUnits(deltaT);

	// get simple aproximations of bank and dive
	// we must consider current angular velocity
	float massCoef=GetMass()*(1.0/3000);
	saturate(massCoef,1,3);

	//float dirEstT = massCoef*0.7;
	float dirEstT = massCoef;
	const Matrix3 &orientation=Orientation();

	//Vector3Val angAcceleration=(_angVelocity-_lastAngVelocity)/deltaT;
	//Vector3Val avgAngVelocity = _angVelocity+angAcceleration*0.5*dirEstT;
	Vector3Val avgAngVelocity = _angVelocity;
	Matrix3Val derOrientation=avgAngVelocity.Tilda()*orientation;

	Matrix3Val estOrientation=orientation+derOrientation*dirEstT;
	Vector3Val estDirection=estOrientation.Direction().Normalized();

	float bank=estOrientation.DirectionAside().Normalized().Y();
	float dive=estDirection.Y();

	#if 0
	if (this==GWorld->CameraOn())
	{
		const char *state = FindEnumName(_state);
		GlobalShowMessage
		(
			500,"h:%d, s:%d, d:%d, "
			"PHeight %.1f, PHeading %.1f, PDive %.1f, PSpeed %.1f, "
			"BankW %.1f, DiveW %.1f, APState %s",
			_pilotHeightHelper,_pilotSpeedHelper,_pilotDirHelper,
			_pilotHeight,_pilotHeading,_pilotDive,_pilotSpeed[2],
			_bankWanted,_diveWanted,state
		);
	}
	#endif
	float avoidGroundMRW=_mainRotorWanted;
	bool driverAlive =
	(
		_driver && _driver->Brain() && _driver
		&& _driver->Brain()->GetLifeState()==AIUnit::LSAlive
	);
	if( _pilotHeightHelper && driverAlive)
	{
		// autopilot: convert simple _pilot control
		// to advanced simulation model
		
		// use acceleration to estimate change of position
		// estimate vertical acceleration
		//float estY=Position.Y()+_pilotSpeed*EST_DELTA+_acceleration*EST_DELTA*EST_DELTA*0.5;
		// estimate position after 
		const float estT=2;
		const float accPredict=0.2; // estimate acceleration only partially
		Vector3 estPos=Position()+_speed*estT+0.5*accPredict*estT*estT*_acceleration;
		float estSurfaceY=GLOB_LAND->SurfaceYAboveWater(estPos.X(),estPos.Z());
		float estHeight=estPos.Y()-estSurfaceY;
		float minEstHeight=estHeight;
		if( _speed.SquareSizeXZ()>Square(5) )
		{
			// predict terrain
			for( int t=1; t<3; t++ )
			{
				float estT=t*0.8;
				Vector3 estPos=Position()+_speed*estT+0.5*estT*estT*_acceleration;
				float estSurfaceY=GLOB_LAND->SurfaceYAboveWater(estPos.X(),estPos.Z());
				float estHeight=estPos.Y()-estSurfaceY;
				saturateMin(minEstHeight,estHeight);
			}
		}

		float changeAY=(_pilotHeight-minEstHeight)*0.1;
		_mainRotorWanted=changeAY+_mainRotor;
		saturate(_mainRotorWanted,-1,1);


		const float minHeight=25;
		if( _pilotHeight>=minHeight )
		{
			float avoidGroundCAY=(_pilotHeight-minEstHeight)*0.1;
			avoidGroundMRW=_mainRotor+avoidGroundCAY;
		}
	}

	const Vector3 relSpeed=ModelSpeed();
	float zPilotSpeed=_pilotSpeed[2];

	// changeAccel is required for speedhelper and dirhelper
	float estAccT=3.0;
	if (_avoidBankJitter) estAccT=1.0;
	Vector3 absSpeedWanted(VMultiply,DirModelToWorld(),_pilotSpeed);
	Vector3 changeAccel=(absSpeedWanted-_speed)*(1/estAccT)-_acceleration;
	float bankLimit=0.5;

	if( _pilotSpeedHelper && driverAlive)
	{
		
		// if we need to accelerate forward, move cyclic forward
		DirectionWorldToModel(changeAccel,changeAccel);
		
		float minDive=-1,maxDive=+1;
		// if we need to apply more rotor power that we have
		// we have to limit cyclic movement
		if( avoidGroundMRW>=1.0 )
		{ // limit dive - we need to climb to avoid ground collision
			// if we are moving forward, we may want to climb up using dive
			float avoidW=floatMin(avoidGroundMRW-1,1);
			float speedDL=fabs(ModelSpeed()[2])*(1.0/20);
			// negative dive is accelerating forward
			if( ModelSpeed()[2]>5 )
			{
				minDive=floatMin(+speedDL*avoidW,+0.5);
			}
			else if( ModelSpeed()[2]<-5 )
			{
				maxDive=floatMax(-speedDL*avoidW,-0.5);
			}
			else
			{
				// low speed climb - disable dive
				minDive = -0.2;
				maxDive = +0.2;
			}
			bankLimit=1.0-0.8*avoidW;
		}

		// when moving slow, dive corresponds to acceleration
		// when moving fast, dive corresponds to speed
		if( fabs(_forceDive)<0.9 ) _diveWanted=_forceDive;
		else
		{
			float fastDive=fabs(zPilotSpeed)*(1.0/70);
			Limit(fastDive,0.2,0.7);
			float normalDive = (dive+changeAccel[2])*(-0.2)*(1-fastDive);
			_diveWanted = normalDive+zPilotSpeed*(-0.3/82)*fastDive;
		}
		saturate(_diveWanted,minDive,maxDive);
	}

	if (_pilotDirHelper && driverAlive)
	{
		//float dirC = _dirCompensate;
		float dirC = 1.0f;
		//float dirC = 0.9f;
		//float dirC = 0.0f;
		//0.5f;
		Vector3 direction=Direction()*(1-dirC)+estDirection*dirC;
		float curHeading=atan2(direction[0],direction[2]);
		float changeHeading=AngleDifference(_pilotHeading,curHeading)*8;
		Limit(changeHeading,-1,1);
		// when slow, use back rotor
		float fastTurn=floatMax(fabs(relSpeed[2])-6,0)*FAST_COEF;
		Limit(fastTurn,0,1);
		_backRotorWanted=-changeHeading*2;
		saturate(_backRotorWanted,-1,+1);

		// when moving slow, do side slip
		if( fabs(_forceBank)<0.9 ) _bankWanted=_forceBank;
		else
		{
			float bankInTurn=fabs(zPilotSpeed)*FAST_COEF; // probably in turn
			saturateMin(bankInTurn,1);

			float turnByBank=(fabs(relSpeed[2])+fabs(_pilotSpeed[2])-4)*0.05;
			saturate(turnByBank,0,1);
			saturateMax(turnByBank,bankInTurn);

			// when moving fast, turn by banking
			//float bankWantedFast=-changeHeading*2*turnByBank;
			//float bankWantedFast=-changeHeading*0.5f*turnByBank;
			float bankWantedFast=-changeHeading*turnByBank;

			// turn enhancer - was not working - multiplication was always by one
			//float enhance = fabs(changeHeading*3.0f)+1.0f;
			//bankWantedFast *= floatMin(enhance,1);



			if( _avoidBankJitter )
			{
				float thold=0.1;
				if( fabs(bankWantedFast)<thold ) bankWantedFast=0;
			}

			_bankWanted = bankWantedFast;
			if (_pilotSpeedHelper)
			{
				float bankWantedSlow=bank+changeAccel[0]*(-1.0/20);
				if (fabs(changeAccel[0])<1 && fabs(_pilotSpeed[0])<1)
				{
					saturate(bankWantedSlow,-0.1,+0.1);
				}
				bankWantedSlow *= 1-turnByBank;

				_bankWanted += bankWantedSlow;
			}

			saturate(_bankWanted,-0.5,0.5);


			#if 0 //_ENABLE_CHEATS
			if (this==GWorld->CameraOn())
			{
				GlobalShowMessage
				(
					100,
					"bank %.2f,%.2f->%.2f (f %.2f), ch %.2f, enh %.2f",
					DirectionAside().Y(),bank,_bankWanted,
					bankWantedFast,
					changeHeading,enhance
				);
			}
			#endif
		}

	}

	if (_pilotSpeedHelper && driverAlive)
	{
		// bank is limited 
		//if( fabs(_diveWanted)>0.3 ) 
		// if diving, limit bank
		saturate(_diveWanted,-0.7,0.6);
		float diveLimitBank=0.5-floatMax(fabs(_diveWanted),fabs(dive));
		saturate(diveLimitBank,0.1,0.8);
		saturateMin(bankLimit,diveLimitBank);

		saturate(_bankWanted,-bankLimit,+bankLimit);

	}

	if (!_driver || _driver->IsDammageDestroyed())
	{
		if (!_landContact)
		{
			_cyclicAsideWanted=-0.1;
			_cyclicForwardWanted=0.1;
			_backRotorWanted=-0.1;
			_mainRotorWanted=0.1;
		}
		else
		{
			_cyclicAsideWanted=0;
			_cyclicForwardWanted=0;
			_backRotorWanted=0;
			_mainRotorWanted=0;
		}
	}
	else
	{
		if( _rotorSpeed>=0.3 )
		{
			if( !_landContact )
			{
				_cyclicAsideWanted=+(_bankWanted-bank)*1.0f;

				float minRotorDive = floatMin(Type()->_minMainRotorDive,Type()->_minBackRotorDive);
				float maxRotorDive = floatMax(Type()->_maxMainRotorDive,Type()->_maxBackRotorDive);

				float diveByRotor = _diveWanted;
				saturate(diveByRotor,minRotorDive,maxRotorDive);
				_rotorDiveWanted = -diveByRotor;

				float adjustedDive = _diveWanted - diveByRotor;
				_cyclicForwardWanted =- (adjustedDive-dive)*16.0f;
				Limit(_cyclicForwardWanted,-1,+1);
			}
			else
			{
				_cyclicAsideWanted=0;
				_cyclicForwardWanted=0;
				_backRotorWanted=0;
				_rotorDiveWanted = 0;
			}
		}
		else
		{
			// no controls available - no engine power
			_cyclicAsideWanted=0;
			_cyclicForwardWanted=0;
			_backRotorWanted=0;
			_mainRotorWanted=0.3;
			_rotorDiveWanted = 0;
		}


		if( _angVelocity.SquareSize()>=10*10 )
		{ // if we are rotating fast, leave all controls in neutral position
			_cyclicAsideWanted=0;
			_cyclicForwardWanted=0;
			_backRotorWanted=0;
			_mainRotorWanted=0.3;
			_rotorDiveWanted = 0;
		}
	}

	// perform advanced simulation
	MoveWeapons(deltaT);
	base::Simulate(deltaT,prec);
}

bool HelicopterAuto::AimObserver(Vector3Par direction)
{
	return true;
}

bool HelicopterAuto::AimWeapon(int weapon, Vector3Par direction )
{
	if (weapon < 0)
	{
		if (NMagazineSlots() <= 0) return false;
		weapon = 0;
	}
	SelectWeapon(weapon);
	// move turret/gun accordingly to direction
	Vector3 relDir(VMultiply,DirWorldToModel(),direction);
	// calculate current gun direction
	// compensate for neutral gun position

	if (_turret.Aim(Type()->_turret,relDir))
	{
		CancelStop();
	}
	return true;
}

/*!
\patch 1.22 Date 9/10/2001 by Ondra.
- Fixed: Helicopter AI gunner aimed under target with mgun.
*/

bool HelicopterAuto::AimWeapon(int weapon, Target *target )
{
	if (weapon < 0)
	{
		if (NMagazineSlots() <= 0) return false;
		weapon = 0;
	}
	_fire.SetTarget(CommanderUnit(),target);
	Vector3 tgtPos=target->AimingPosition();
	Vector3 weaponPos=Type()->_gunPos;

	const Magazine *magazine = GetMagazineSlot(weapon)._magazine;
	const MagazineType *aInfo = magazine ? magazine->_type : NULL;
	const WeaponModeType *mode = GetWeaponMode(weapon);
	if (mode && mode->_ammo)
	{
		if
		(
			mode->_ammo->_simulation != AmmoShotMissile &&
			mode->_ammo->_simulation != AmmoShotLaser
		)
		{
			// calculate gun balistics
			float dist2=tgtPos.Distance2(Position());
			float time2 = 0;
			if (aInfo) time2 = dist2 * Square(aInfo->_invInitSpeed);
			float time=sqrt(time2);
			// calculate balistics
			float fall=0.5*G_CONST*time2;
			tgtPos[1]+=fall; // consider balistics
			tgtPos+=target->speed*(time+0.25);
		}
	}

	const float predTime=0.2;
	Vector3 myPos=PositionModelToWorld(weaponPos);
	tgtPos+=target->speed*predTime;
	myPos+=Speed()*predTime;

	return AimWeapon(weapon,tgtPos-myPos);
}

Vector3 HelicopterAuto::GetWeaponDirection( int weapon ) const
{
	if (weapon<0 || weapon>=NMagazineSlots()) return Direction();
	const WeaponModeType *mode = GetWeaponMode(weapon);
	if (!mode || !mode->_ammo ) return Direction();
	if (mode->_ammo->_simulation == AmmoShotMissile)
	{
		return Direction();
	}
	else if (mode->_ammo->_simulation == AmmoShotRocket)
	{
		return Direction();
	}
	else
	{
		return Transform().Rotate(GunTransform().Rotate(Type()->_gunDir));
	}
}

Vector3 HelicopterAuto::GetWeaponDirectionWanted( int weapon ) const
{
	if (weapon<0 || weapon>=NMagazineSlots()) return Direction();
	const WeaponModeType *mode = GetWeaponMode(weapon);
	if (!mode || !mode->_ammo ) return Direction();
	if (mode->_ammo->_simulation == AmmoShotMissile)
	{
		return Direction();
	}
	else if (mode->_ammo->_simulation == AmmoShotRocket)
	{
		return Direction();
	}
	else
	{
		Vector3 dir = Type()->_turret._dir;
		Matrix3Val aim = _turret.GetAimWanted();
		return Transform().Rotate(aim*dir);
	}
}

Vector3 HelicopterAuto::GetWeaponPoint( int weapon ) const
{
	if (weapon<0 || weapon >= NMagazineSlots()) return VZero;
	//const Magazine *magazine = GetMagazineSlot(weapon)._magazine;
	//if (!magazine) return VZero;
	const WeaponModeType *mode = GetWeaponMode(weapon);
	if (!mode) return VZero;
	if (!mode->_ammo) return VZero;
	switch (mode->_ammo->_simulation )
	{
		case AmmoShotRocket:
			return !_rocketLRToggle ? Type()->_rocketLPos : Type()->_rocketRPos;
		case AmmoShotMissile:
		{
			int count = GetMagazineSlot(weapon)._magazine->_ammo;
			bool found;
			Vector3 pos = FindMissilePos(count,found);
			if (!found)
			{
				pos = !_missileLRToggle ? Type()->_missileLPos : Type()->_missileRPos;
			}
			return pos;
		}
		case AmmoShotBullet:
		case AmmoShotLaser:
		{
			Matrix4Val shootTrans=GunTransform();
			return shootTrans.FastTransform(Type()->_gunPos);
		}
		default:
			return VZero;
	}
	return VZero;

}

Vector3 HelicopterAuto::GetWeaponCenter( int weapon ) const
{
	const WeaponModeType *mode = GetWeaponMode(weapon);
	if (!mode || !mode->_ammo ) return Direction();
	if (mode->_ammo->_simulation == AmmoShotMissile)
	{
		return base::GetWeaponCenter(weapon);
	}
	else if (mode->_ammo->_simulation == AmmoShotRocket)
	{
		return base::GetWeaponCenter(weapon);
	}
	else
	{
		return _turret.GetCenter(Type()->_turret);
	}
}

float HelicopterAuto::GetAimed( int weapon, Target *target ) const
{
	if( !target ) return 0;
	if( !target->idExact ) return 0;
	// check if weapon is aimed
	if( weapon<0 ) return 0;
	float visible=_visTracker.Value(this,_currentWeapon,target->idExact);
	const WeaponModeType *mode = GetWeaponMode(weapon);
	if (!mode || !mode->_ammo ) return 0;

	// 0.6 visibility means 0.8 unaimed
	visible=1-(1-visible)*0.5f;

	if (mode->_ammo->_simulation == AmmoShotMissile)
	{
		if (mode->_ammo->maxControlRange>10)
		{
			// guided missile	
			Vector3 relPos=PositionWorldToModel(target->AimingPosition());
			// check if target is in front of us
			if( relPos.Z()<=50 ) return 0; // missile fire impossible
			// check if target position is withing missile lock cone
			if( fabs(relPos.X())>relPos.Z() ) return 0;
			if( fabs(relPos.Y())>relPos.Z() ) return 0;
			// the nearer we are, the more precise lock required
			float invRZ=1.0/relPos.Z();
			float lockX=1-fabs(relPos.X())*invRZ;
			float lockY=1-fabs(relPos.Y())*invRZ;
			float lock=floatMin(lockX,lockY);
			saturate(lock,0,1);
			// we can fire
			#if 0
			if( (Object *)this==GWorld->CameraOn() )
			{
				LogF("Lock precision %.3f, vis %.3f",lock,visible);
				GEngine->ShowMessage(100,"Lock precision %.3f, vis %.3f",lock,visible);
			}
			#endif
			lock*=visible;
			if( lock<0.5 ) lock=0;
			return lock;
		}
		else
		{
			// unguided rocket
			Vector3 relPos=PositionWorldToModel(target->AimingPosition());
			// check if target is in front of us
			if( relPos.Z()<=30 ) return 0; // missile fire impossible
			// check if target position is withing missile lock cone
			// calculate error
			float xError = fabs(relPos.X());
			float yError = fabs(relPos.Y());
			float tgtSize = target->idExact->GetShape()->GeometrySphere();
			float error = floatMax(xError,yError);
			float maxError = mode->_ammo->indirectHitRange*4.5;
			if( error>tgtSize+maxError ) return 0;
			float lock = 1-(error-tgtSize)/(tgtSize+maxError);
			saturate(lock,0,1);
			#if 0
			if( (Object *)this==GWorld->CameraOn() )
			{
				GEngine->ShowMessage(100,"Lock precision %.3f, vis %.3f",lock,visible);
			}
			#endif
			return lock*visible;
		}
	}
	else
	{
/*
		float deltaAimX=fabs(_gunXRot-_gunXRotWanted);
		float deltaAimY=fabs(_gunYRot-_gunYRotWanted);
		float distance=target->AimingPosition().Distance(Position());
		float xError=distance*deltaAimY; // rot around y axis - x dir error
		float yError=distance*deltaAimX;
		float tgtSize=target->idExact->GetShape()->GeometrySphere();
		return( xError<tgtSize*2 && yError<tgtSize )*visible;
*/
		// predict shot result
		const Magazine *magazine = GetMagazineSlot(weapon)._magazine;
		const MagazineType *aInfo = magazine ? magazine->_type : NULL;
		Vector3 ap=target->AimingPosition();
		float dist=ap.Distance(Position());
		float time=dist*aInfo->_invInitSpeed;
		Vector3 estPos=ap+target->speed*time;
		Vector3 wDir=GetWeaponDirection(weapon);
		Vector3 wPos=PositionModelToWorld(GetWeaponCenter(weapon));
		float eDist=wPos.Distance(estPos);
		Vector3 hit=wPos+wDir*eDist;
		hit[1]-=G_CONST*time*time*0.5;
		Vector3 hError=hit-estPos;
		hError[1]*=2;
		float error=hError.Size()*0.5;

		float tgtSize=target->idExact->GetShape()->GeometrySphere();
		float maxError=tgtSize*0.7+mode->_ammo->indirectHitRange*0.3;
		maxError+=dist*mode->_dispersion;

		if (mode->_ammo->_simulation != AmmoShotBullet)
		{
			maxError *=2;
		}
		return ( error<maxError )*visible;
	}
}


// manual control

void HelicopterAuto::DammageCrew( EntityAI *killer, float howMuch, RString ammo)
{
	AIUnit *commander = CommanderUnit();
	if (commander)
	{
		if (GetRawTotalDammage()>=0.7f && commander->GetCombatMode()>=CMCombat)
		{
			Time goTime = Glob.time + GRandGen.PlusMinus(2.0f,1.0f);
			if (goTime<_getOutAfterDammage)  _getOutAfterDammage = goTime;
		}
	}
	
	base::DammageCrew(killer,howMuch,ammo);
}

void HelicopterAuto::Eject(AIUnit *unit)
{
	// check height
	float surfaceY=GLOB_LAND->SurfaceYAboveWater(Position()[0],Position()[2]);
	float height = Position().Y() - surfaceY;
	bool parachute = (height>30);
	unit->ProcessGetOut(parachute);
}


void HelicopterAuto::FakePilot( float deltaT )
{
	_forceDive=1;
}

void HelicopterAuto::JoystickDirPilot( float deltaT )
{
	_pilotSpeedHelper=false; // keyboard helper deactivated
	_pilotDirHelper=false; // keyboard helper deactivated
	_avoidBankJitter = false;

	_bankWanted=GInput.GetStickLeft();
	_diveWanted=-GInput.GetStickForward();
	_backRotorWanted=-GInput.GetStickRudder();

	Limit(_bankWanted,-0.4,0.4);
	Limit(_diveWanted,-0.7,0.45);
	saturate(_backRotorWanted,-1,+1);

}

/*!
\patch 1.43 Date 1/25/2002 by Ondra
- Fixed: Helicopter collective was too sensitive near joystick center position.
*/

void HelicopterAuto::JoystickHeightPilot( float deltaT )
{
	_pilotHeightHelper=false; // keyboard helper deactivated
	// make thrust less sensitive in central area
	float collective0 = GInput.GetStickThrust();
	//float collective = collective0*fabs(collective0);
	float collective = collective0;
	// collecive is in range -1..1
	// we need result in range -0.5..1
	// scale by 1.5/2
	_mainRotorWanted=collective*(1.5f/2)+0.5f; // result from 0.5 to 1.5
	saturate(_mainRotorWanted,-0.5f,1);

	// maintain _pilotHeight
	// we need it in case we would like to use keyboard or switch to AI
	float surfaceY=GLOB_LAND->SurfaceYAboveWater(Position()[0],Position()[2]);
	_pilotHeight=Position().Y()-surfaceY;
}

void HelicopterAuto::SuspendedPilot(AIUnit *unit, float deltaT )
{
	float surfaceY=GLOB_LAND->SurfaceYAboveWater(Position()[0],Position()[2]);
	_pilotHeight=Position().Y()-surfaceY;
	_pilotSpeed=VZero;
}

void HelicopterAuto::DetectControlMode() const
{
	static const UserAction moveActions[]=
	{
		UAMoveForward,UAMoveBack,
		UAMoveFastForward
	};
	static const UserAction turnActions[]=
	{
		UAMoveForward,UAMoveBack,
		UAMoveFastForward,
		UAMoveLeft,UAMoveRight,
		UATurnLeft,UATurnRight
	};
	static const UserAction cursorActions[]=
	{
		UALookLeftDown,UALookDown,UALookRightDown,
		UALookLeft,UALookCenter,UALookRight,
		UALookLeftUp,UALookUp,UALookRightUp
	};
	static const UserAction thrustActions[]=
	{
		UAMoveUp,UAMoveDown,
	};

	const int nMoveActions = sizeof(moveActions)/sizeof(*moveActions);
	const int nTurnActions = sizeof(turnActions)/sizeof(*turnActions);
	const int nCursorActions = sizeof(cursorActions)/sizeof(*cursorActions);
	const int nThrustActions = sizeof(thrustActions)/sizeof(*thrustActions);
	DetectControlModeActions
	(
		moveActions,nMoveActions,
		turnActions,nTurnActions,
		cursorActions,nCursorActions,
		thrustActions,nThrustActions
	);
}

/*!
\patch 1.43 Date 1/25/2002 by Ondra
- Fixed: Helicopter height limit when flying with keyboard
removed in multiplayer or veteran mode. It is still used in SP cadet mode.
\patch 1.46 Date 3/1/2002 by Ondra
- Fixed: Helicopter landing with keyboard impossible.
When landing, helicopter stops hovering above the ground.
*/

void HelicopterAuto::KeyboardPilot(AIUnit *unit, float deltaT )
{	

	_dirCompensate=0; // low heading compensation
	_forceDive=1;
	_forceBank=1;

	if( GInput.JoystickThurstActive() )
	{
		JoystickHeightPilot(deltaT);
	}
	else
	{
		_pilotHeightHelper = true; // keyboard helper activated

		Vector3Val position=Position();
		float surfaceY=GLOB_LAND->SurfaceYAboveWater(position[0],position[2]);
		float curHeight=position.Y()-surfaceY;
		const float predictTime=2.0;
		if( GInput.keyMoveUp )
		{
			if( !EngineIsOn() ) EngineOn();

			if( !_pressedUp ) _pilotHeight=curHeight,_pressedUp=true;
			_pilotHeight+=deltaT*20*GInput.keyMoveUp;
		}
		else
		{
			if( _pressedUp ) _pilotHeight=curHeight+_speed[1]*predictTime,_pressedUp=false;
		}
		if( GInput.keyMoveDown )
		{
			if( !_pressedDown ) _pilotHeight=curHeight,_pressedDown=true;
			_pilotHeight-=deltaT*20*GInput.keyMoveDown;
		}
		else
		{
			if( _pressedDown ) _pilotHeight=curHeight+_speed[1]*predictTime,_pressedDown=false;
		}
		float canLand=1;
		saturateMin(canLand,2-_speed.SizeXZ()*0.1); // no landing in high speed
		//saturateMin(canLand,1.5-_rotorSpeed); // no landing with rotor on
		saturateMax(canLand,0);
		//Limit(_pilotHeight,4-canLand*3,150);
		//Limit(_pilotHeight,4-canLand*3,150);
		saturateMax(_pilotHeight,4-canLand*3);
		if (Glob.config.easyMode && GWorld->GetMode()!= GModeNetware)
		{
			saturateMin(_pilotHeight,150);
		}
		else
		{
			saturateMin(_pilotHeight,10000);
		}
	}

	_pilotSpeed[1]=0; // maintain height
	_pilotSpeed[0]=0; // no side slip

	/*
	float forward=
	(
		GInput.keyMoveForward*0.5
		+GInput.keyMoveFastForward
		-GInput.keyMoveBack*0.25
	);

	if( fabs(forward)<0.1 )
	{
		if( fabs(_pilotSpeed[2])<12 )
		{
			// automatically stop when moving very slow
			_pilotSpeed[2]=0;
		}
	}
	if( forward>0 )
	{
		if( !_pressedForward )
		{
			_pilotSpeed[2]=relSpeed[2],_pressedForward=true;
			_enableDirectionChange = relSpeed[2]>-2; 
		}
		_pilotSpeed[2]+=deltaT*30*forward;
		if (!_enableDirectionChange) saturateMin(_pilotSpeed[2],0);
	}
	else
	{
		if( _pressedForward ) _pilotSpeed[2]=relSpeed[2],_pressedForward=false;
	}
	if( forward<0 )
	{
		if( !_pressedBack )
		{
			_pilotSpeed[2]=relSpeed[2],_pressedBack=true;
			_enableDirectionChange = relSpeed[2]<+2; 
		}
		_pilotSpeed[2]+=deltaT*30*forward;
		if (!_enableDirectionChange) saturateMax(_pilotSpeed[2],0);
	}
	else
	{
		if( _pressedBack ) _pilotSpeed[2]=relSpeed[2],_pressedBack=false;
	}
	
	bool internalCamera = IsGunner(GWorld->GetCameraType());
	if( internalCamera && GInput.MouseTurnActive() && !GInput.lookAroundEnabled)
	{
		// last input from mouse - use mouse controls
		// 
		Vector3Val direction = Direction();
		_pilotHeading = atan2(direction[0],direction[2]);
		_pilotHeading += _mouseTurnWanted;		
	}
	else
	{
		float turnSpeed = 0.5*(GInput.keyTurnRight-GInput.keyTurnLeft);
		if (fabs(turnSpeed)>0.1 || !_pilotHeadingSet)
		{
			Vector3Val direction = Direction();
			_pilotHeading = atan2(direction[0],direction[2]);
			_pilotHeading += turnSpeed;
			_pilotHeadingSet = false;
			// TODO: remove _pilotHeadingSet - it is never set to true
		}
	}
	_pilotSpeed[0]=10*(GInput.keyMoveRight-GInput.keyMoveLeft);
	*/

	if( GInput.JoystickActive() )
	{
		JoystickDirPilot(deltaT);
	}
	else
	{
		// TODO: when speed is low and controls was not touched for a long time
		// activate hovering autopilot
		
		bool internalCamera = IsGunner(GWorld->GetCameraType());
		if (internalCamera && GInput.MouseTurnActive() && !GInput.lookAroundEnabled)
		{
			// last input from mouse - use mouse controls
			// 
			_pilotHeading = atan2(_mouseDirWanted[0],_mouseDirWanted[2]);


			_pilotDirHelper = true; // keyboard helper activated
			_avoidBankJitter = false;

			if (!_hoveringAutopilot)
			{
				_pilotDive = _mouseDirWanted[1];;
				saturate(_pilotDive,-0.7,+0.7);
				_diveWanted = _pilotDive;
				_pilotSpeedHelper = false; // keyboard helper activated
			}
			else
			{
				_pilotSpeedHelper = true; // keyboard helper activated
				// control hovering with mouse
				_pilotDive = 0;
				_pilotSpeed[0] = (GInput.keyTurnRight-GInput.keyTurnLeft)*3;
				_pilotSpeed[2] = -_mouseDirWanted[1]*20;
				saturate(_pilotSpeed[2],-5,+7);
			}

		}
		else
		{
			_avoidBankJitter = true;

			// turn keys control bank
			_backRotorWanted = (GInput.keyMoveLeft-GInput.keyMoveRight);
			saturate(_backRotorWanted,-1,+1);

			if (!_hoveringAutopilot)
			{
				_pilotDirHelper = false;
				_pilotSpeedHelper = false; // keyboard helper activated
				// turn keys control bank
				_bankWanted = -0.5*(GInput.keyTurnRight-GInput.keyTurnLeft);

				float forward=
				(
					GInput.keyMoveForward
					+GInput.keyMoveFastForward*2
					-GInput.keyMoveBack
				);

				float dive = Direction().Y() - _rotorDive;

				if (fabs(forward)>0.1)
				{
					_pilotDive = dive-forward;
					_pilotDiveSet = false;
				}
				else if (!_pilotDiveSet)
				{
					_pilotDiveSet = true;
					_pilotDive = dive;
				}
				saturate(_pilotDive,-0.7,+0.7);

				_diveWanted = _pilotDive;
			}
			else
			{
				_pilotDirHelper = fabs(_backRotorWanted)<0.1; // keyboard helper activated

				if (!_pilotDirHelper)
				{
					_pilotHeading = atan2(Direction()[0],Direction()[2]);
				}
				// control hovering with keyboard
				_pilotSpeedHelper = true; // keyboard helper activated

				float forward=
				(
					GInput.keyMoveForward*0.5
					+GInput.keyMoveFastForward
					-GInput.keyMoveBack*0.5
				);

				_pilotSpeed[0] = (GInput.keyTurnRight-GInput.keyTurnLeft)*3;
				_pilotSpeed[2] = forward*7;
				_pilotDive = 0;
				_bankWanted = 0;

			}
		}

	
		Limit(_pilotSpeed[2],-10,+Type()->GetMaxSpeedMs());
	}

}

void HelicopterAuto::AvoidGround( float minHeight )
{
	Point3 estimate=Position();
	float maxUnder=0;
	for( int i=0; i<2; i++ )
	{
		float estY=GLOB_LAND->SurfaceYAboveWater(estimate.X(),estimate.Z());
		estY+=minHeight;
		float estUnder=estY-estimate.Y();
		saturateMax(maxUnder,estUnder);
		estimate+=_speed*3.0;
	}
	if( maxUnder>0 )
	{
		float maxSpeed=Interpolativ(maxUnder,0,10,Type()->GetMaxSpeedMs(),0);
		Limit(_pilotSpeed[2],-maxSpeed,maxSpeed);
	}

	#if 0
		if( GLOB_WORLD->CameraOn()==this )
		{
			GLOB_ENGINE->ShowMessage
			(
				100,"pilotHeight %.1f minHeight %.1f maxUnder %.1f",
				_pilotHeight,minHeight,maxUnder
			);
		}
	#endif
}

// AI autopilot

void HelicopterAuto::BrakingManeuver()
{
	_pilotHeading = atan2(-Speed().X(),-Speed().Z());
	float curHeading = atan2(Direction().X(),Direction().Z());
	float headChange = AngleDifference(_pilotHeading,curHeading);

	// once chopper is turned is desired direction, it may start braking
	float brakeDive = floatMax(0,1-headChange*(0.5f/H_PI));
	_forceDive = brakeDive;
	saturate(_forceDive,-0.6f,-0.1f);
}

void HelicopterAuto::Autopilot
(
	float deltaT,
	Vector3Par target, Vector3Par tgtSpeed, // target
	Vector3Par direction, Vector3Par speed // wanted values
)
{
	// point we would like to reach
	float avoidGround=0;
	Vector3Val position=Position();
	Vector3 absDistance=target-position;
	//Vector3 absDirection=absDistance+tgtSpeed*4; // "lead target" - est. target position
	Vector3 distance=DirectionWorldToModel(absDistance);

	float bank=DirectionAside().Y();
	float dive=Direction().Y();

	float sizeXZ2=distance.SquareSizeXZ();
	switch( _state )
	{
		default: //case AutopilotFar:
		{
			Vector3 absDirection=absDistance+direction*(tgtSpeed.Size()*20); // "lead target" - est. target position
			// use special maneuvre for fast brakeing when travelling at high speed
			_pilotHeading=atan2(absDirection.X(),absDirection.Z());
			avoidGround=30;
			_pilotHeight=avoidGround;
			// use special trick for fast breaking
			// start turning before we reach destination
			Vector3 relSpeed=DirectionWorldToModel(_speed-tgtSpeed);
			#define BRAKE_SEC 3.0
			if
			(
				tgtSpeed.SquareSize()<20*30 // target is slow
				&& (_speed.SquareSize()-speed.SquareSize())>40*40 // we will need to brake
			)
			{
				if( distance[2]-relSpeed[2]*BRAKE_SEC<0 && sizeXZ2<Square(450))
				{
					// we are aproaching the target, start braking
					_state=AutopilotBrake;
				}
			}
			else if( sizeXZ2<200*200 )
			{
				_state=AutopilotNear;
			}
			_pilotSpeed[0]=0; // no side slips
			_pilotSpeed[1]=0; // vertical speed is ignored anyway
			_pilotSpeed[2]=Type()->GetMaxSpeedMs()*0.8;
			// target height
		}
		break;
		case AutopilotBrake:
		{
			Vector3 relSpeed=DirectionWorldToModel(_speed-tgtSpeed);
			// wait until loosing speed
			_pilotSpeed[0]=0; // no side slips
			_pilotSpeed[1]=0; // vertical speed is ignored anyway
			avoidGround=30;
			_pilotHeight=avoidGround;
			BrakingManeuver();
			// maintain forward dive
			if (sizeXZ2>Square(550))
			{
				_state = AutopilotFar;
			}
			else if (tgtSpeed.SquareSize()>40*40) // target is too fast
			{
				_state=AutopilotNear;
			}
			else if (Speed().SquareSize()<Square(Type()->GetMaxSpeedMs()*0.25f))
			{
				// if we are moving slow, we may stop braking
				_state = AutopilotNear;
			}
		}
		break;
		case AutopilotNear:
		{

			Vector3 absDirection=absDistance+direction*(tgtSpeed.Size()*10); // "lead target" - est. target position
			// slow down near the target
			_pilotHeading=atan2(absDirection.X(),absDirection.Z());

			float targetAbove=target.Y()-GLOB_LAND->SurfaceYAboveWater(position[0],position[2]);
			avoidGround=floatMax(30,targetAbove);
			_pilotHeight=avoidGround;
			_pilotSpeed[0]=0; // no side slips
			_pilotSpeed[1]=0; // vertical speed is ignored anyway
			// select speed so that you will reach target in 10 sec
			if (tgtSpeed.SquareSize()<Square(5))
			{
				// target is static
				// if we are moving very fast, we need to brake
				float fast=sqrt(sizeXZ2)*(1.0/500);
				Limit(fast,0,1);
				float wantedSpeed = Type()->GetMaxSpeedMs()*fast*0.5+1;
				float currentSpeed = ModelSpeed()[2];
				_pilotSpeed[2] = wantedSpeed;
				if
				(
					sizeXZ2<Square(50) // near enough
					&& speed.Distance2(tgtSpeed)<Square(5) // there is a chance to align
					// never assume aligned when target is moving fast
				)
				{
					_state=AutopilotAlign;
				}
				else if (currentSpeed>wantedSpeed*1.5 && currentSpeed>45)
				{
					// moving too fast - initiate braking maneuver
					_state=AutopilotBrake;
				}
			}
			else
			{
				// moving target
				// target is the place we should be right now
				Vector3 relTgtPos = PositionWorldToModel(target);
				float timeToReach = 10;
				// adjust speed so that you reach target at given time
				_pilotSpeed[2] = relTgtPos.Z()*(1.0/timeToReach);
				if
				(
					distance[2]>fabs(distance[0])*2 && sizeXZ2>300*300
				)
				{
					// far away, heading to target, target is moving slow
					_state=AutopilotFar;
				}
			}
		}
		break;
		case AutopilotAlign: case AutopilotReached:
		{
			_pilotHeading=atan2(direction.X(),direction.Z());
			float sizeXZ=sqrt(sizeXZ2);
			float highX=Interpolativ(sizeXZ,5,50,2,30);
			float highZ=highX;
			// control to be there in estT sec
			const float estT=4.0;
			Vector3Val estSpeed=DirectionWorldToModel(_speed+2.0*_acceleration);
			float estSpeedZ = estSpeed.Z();
			float estSpeedX = estSpeed.X();
			Vector3Val estPosA=Position()+_speed*estT+0.5*estT*estT*_acceleration;
			Vector3Val estTgt=target+tgtSpeed*estT;
			
			//Vector3 tgtPos=DirectionWorldToModel(estTgt-estPos);
			Vector3 tgtPos=DirectionWorldToModel(estTgt-estPosA);

#if _ENABLE_CHEATS
			if( CHECK_DIAG(DEPath) )
			{
				Vector3Val estPos=Position()+_speed*estT; //+0.5*estT*estT*_acceleration;
				{
					Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),-1);
					obj->SetPosition(estPos);
					obj->SetScale(1);
					obj->SetConstantColor(PackedColor(Color(1,0,0)));
					GLandscape->ShowObject(obj);
				}
				{
					Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),-1);
					obj->SetPosition(estPosA);
					obj->SetScale(0.75);
					obj->SetConstantColor(PackedColor(Color(1,1,0)));
					GLandscape->ShowObject(obj);
				}
			}
#endif

			// if we are nearly aligned and stable, tolerate some inaccuracy
			if( _speed.SquareSizeXZ()<2 && fabs(dive)<0.1 && fabs(bank)<0.1 )
			{
				if( fabs(tgtPos[2])<2 ) tgtPos[2]=0,highZ=0;
				if( fabs(tgtPos[0])<2 ) tgtPos[0]=0,highX=0;
			}
			

			float high=floatMax(highX,highZ);
			if( high<4.0f ) _state=AutopilotReached;


			if( tgtSpeed.SquareSize()<Square(8) )
			{
				float high=floatMax(highX,highZ);
				float highSpeed=Interpolativ(_speed.Distance(speed),0.2,20,2,30);
				saturateMax(high,highSpeed);
			}
			else
			{
				_state = AutopilotNear;
			}

			// once we are reached, stay reached, stay low

			float targetAbove=target.Y()-GLOB_LAND->SurfaceYAboveWater(position[0],position[2]);

			if( _state==AutopilotReached )
			{
				avoidGround=-0.5;
				_pilotHeight = targetAbove;
			}
			else
			{
				avoidGround=high;
				_pilotHeight=floatMax(high,targetAbove);
			}
			
			float apDive=dive-tgtPos.Z()*0.05;
			float apBank=bank-tgtPos.X()*0.05;

			// if speed is high enough, do not accelerate
			const float maxZSpd=1;
			const float maxXSpd=1;
			if( ModelSpeed().Z()>+maxZSpd ) saturateMax(apDive,0); // no negative
			if( ModelSpeed().Z()<-maxZSpd ) saturateMin(apDive,0); // no positive
			if( ModelSpeed().X()>+maxXSpd ) saturateMax(apBank,0);
			if( ModelSpeed().X()<-maxXSpd ) saturateMin(apBank,0);

			if( estSpeedZ>0 ) saturateMax(apDive,-0.1); // little negative
			if( estSpeedZ<0 ) saturateMin(apDive,+0.1); // little positive
			if( estSpeedX>0 ) saturateMax(apBank,-0.1);
			if( estSpeedX<0 ) saturateMin(apBank,+0.1);

			float posLimit=floatMax(tgtPos.SizeXZ()*(1.0f/4)-0.2f,0);
			float spdLimit=floatMax(speed.SizeXZ()*(1.0f/4)-0.2f,0);
			float maxDive=spdLimit*0.3f+posLimit*0.2f;
			saturateMin(maxDive,0.5f);

			saturate(apDive,-maxDive,+maxDive);
			saturate(apBank,-maxDive,+maxDive);

			_forceDive=apDive;
			_forceBank=apBank;

			// pilotSpeed is not used (forceDive and forceBank is used instead)
			// but it may be used in MP prediction
			// it should be low: setting it to zero should give good results
			_pilotSpeed = VZero;

#if _ENABLE_CHEATS
			if( CHECK_DIAG(DEPath) && GLOB_WORLD->CameraOn()==this )
			{
				GlobalShowMessage
				(
					500,
					"spd %.1f,%.1f, mspd %.1f,%.1f espd %.1f,%.1f, tPos %.1f,%.1f "
					"d %.2f->ad %.2f, b %.2f->ab %.2f",
					_pilotSpeed[0],_pilotSpeed[2],
					ModelSpeed()[0],ModelSpeed()[2],
					estSpeed[0],estSpeed[2],
					tgtPos.X(),tgtPos.Z(),
					dive,apDive,bank,apBank
				);
			}
#endif
			
			if( sizeXZ2>Square(90) || tgtSpeed.Distance2(speed)>=10*10 )
			{
				_state=AutopilotNear;
			}
		}
		break;
	}
	_pilotSpeed+=DirectionWorldToModel(speed);
	Limit(_pilotSpeed[2],-10,Type()->GetMaxSpeedMs());
	if( avoidGround>0 ) AvoidGround(avoidGround);
}

void HelicopterAuto::ResetAutopilot()
{
	// We set state to near. It will go to far automatically (if necessary).
	_state=AutopilotNear;
	//_apDive=0,_apBank=0;
}

bool HelicopterAuto::FireWeapon( int weapon, TargetType *target )
{
	if (GetNetworkManager().IsControlsPaused()) return false;
	if (weapon >= NMagazineSlots()) return false;
	if( !GetWeaponLoaded(weapon) ) return false;
	if( !IsFireEnabled() ) return false;

	const Magazine *magazine = GetMagazineSlot(weapon)._magazine;
	if (!magazine) return false;
	const MagazineType *aInfo = magazine ? magazine->_type : NULL;
	const WeaponModeType *mode = GetWeaponMode(weapon);
	Assert(mode);
	if (!mode->_ammo) return false;
	bool fired=false;
	switch (mode->_ammo->_simulation )
	{
		case AmmoShotRocket:
		{
			_rocketLRToggle=!_rocketLRToggle;
			Vector3Val pos=( _rocketLRToggle ? Type()->_rocketLPos : Type()->_rocketRPos );
			fired=FireMissile
			(
				weapon,
				pos,VForward,Vector3(0,0,aInfo->_initSpeed),
				target
			);
		}
		break;
		case AmmoShotMissile:
		{
			_missileLRToggle=!_missileLRToggle;

			int count = GetMagazineSlot(weapon)._magazine->_ammo;
			bool found;
			Vector3 pos = FindMissilePos(count,found);
			if (!found)
			{
				pos=( _missileLRToggle ? Type()->_missileLPos : Type()->_missileRPos );
			}
			fired=FireMissile
			(
				weapon,
				pos,VForward,Vector3(0,0,aInfo->_initSpeed),
				target
			);
		}
		break;
		case AmmoShotBullet:
		{
			Matrix4Val shootTrans=GunTransform();
			fired=FireMGun
			(
				weapon,
				shootTrans.FastTransform(Type()->_gunPos),
				shootTrans.Rotate(Type()->_gunDir),
				target
			);
		}
		break;
		case AmmoNone:
		break;
		case AmmoShotLaser:
			FireLaser(weapon, target);
		break;
		default:
			Fail("Unknown ammo used.");
		break;
	}

	if( fired )
	{
		VehicleWithAI::FireWeapon(weapon, target);
		return true;
	}
	return false;
}

void HelicopterAuto::FireWeaponEffects
(
	int weapon, const Magazine *magazine,EntityAI *target
)
{
	const MagazineSlot &slot = GetMagazineSlot(weapon);
	if (!magazine || slot._magazine!=magazine) return;

	const WeaponModeType *mode = GetWeaponMode(weapon);
	if (!mode) return;
	if (!mode->_ammo) return;
	
	if (EnableVisualEffects(SimulateVisibleNear)) switch (mode->_ammo->_simulation)
	{
		case AmmoShotRocket:
		case AmmoShotMissile:
		case AmmoNone:
			break;
		case AmmoShotBullet:
			_mGunClouds.Start(0.1);
			_mGunFire.Start(0.1,0.4,true);
			_mGunFireFrames = 1;
			_mGunFireTime = Glob.uiTime;
			int newPhase;
			while ((newPhase = toIntFloor(GRandGen.RandomValue() * 3)) == _mGunFirePhase);
			_mGunFirePhase = newPhase;
			break;
	}

	base::FireWeaponEffects(weapon, magazine,target);
}

// AI interface

float HelicopterAuto::GetFieldCost( const GeographyInfo &info ) const
{
	return 1;
}

float HelicopterAuto::GetCost( const GeographyInfo &geogr ) const
{
	float cost=Type()->GetMinCost(); // basic speed
	// water is low and therefore following water is usually safer and quicker
	if (geogr.u.waterDepth>=2) cost*=0.8;
	else if (geogr.u.waterDepth>=1) cost*=0.9;
	// penalty for objects
	//cost *= 1 + geogr.howManyObjects*0.1;
	// avoid steep hills
	// penalty for hills
	int grad = geogr.u.gradient;
	Assert( grad<=7 );
	static const float gradPenalty[8]={1.0,1.02,1.05,1.2,1.3,1.5,1.7,2.0};
	cost *= gradPenalty[grad];
	return cost;
}

float HelicopterAuto::GetCostTurn( int difDir ) const
{ // in sec
	if( difDir==0 ) return 0;
	float aDir=fabs(difDir);
	float cost=aDir*0.15+aDir*aDir*0.02;
	if( difDir<0 ) return cost*0.8;
	return cost;
}

float HelicopterAuto::FireInRange( int weapon, float &timeToAim, const Target &target ) const
{
	timeToAim=0;
	//return GetAimed(weapon,target.idExact);
	return 1;
}

float HelicopterAuto::FireAngleInRange( int weapon, Vector3Par rel ) const
{
	// helicopter cannot fire high, can fire slight low
	if( rel.Y()>0 ) return 0;
	float size2=rel.SquareSizeXZ();
	float y2=Square(rel.Y());
	const float maxY=0.25;
	if( y2>size2*Square(maxY) ) return 0;
	// nearly same level
	float invSize=InvSqrt(size2);
	return 1-rel.Y()*invSize*(1/maxY);
}

#if _ENABLE_AI

void HelicopterAuto::AIGunner(AIUnit *unit, float deltaT )
{
	Assert(unit);

	if( !GetFireTarget() ) return;

	if( !_fire._fireTarget || _fire.GetTargetFinished(unit) )
	{
		_fire._fireMode=-1;
		_fire._fireTarget=NULL;
		return;
	}
	

	AimWeapon(_currentWeapon,GetFireTarget());
	
	if( _currentWeapon<0 ) return;
	if( _fire._firePrepareOnly ) return;
	
	// check if weapon is aimed
	if
	(
		GetWeaponLoaded(_currentWeapon) && GetAimed(_currentWeapon,GetFireTarget())>=0.7
		&& GetWeaponReady(_currentWeapon,GetFireTarget())
	)
	{
		if (!GetAIFireEnabled(GetFireTarget())) ReportFireReady();
		else
		{
			FireWeapon(_currentWeapon,GetFireTarget()->idExact);
	//		_firePrepareOnly = true;
			_fireState=FireDone;
			_fireStateDelay=Glob.time+5; // leave some time to recover
		}
	}

}

#endif //_ENABLE_AI

void HelicopterAuto::MoveWeapons( float deltaT )
{
/*
	float delta;
	float speed;
	speed=(_gunXRotWanted-_gunXRot)*8;
	const float maxA=10;
	const float maxV=5;
	delta=speed-_gunXSpeed;
	Limit(delta,-maxA*deltaT,+maxA*deltaT);
	_gunXSpeed+=delta;
	Limit(_gunXSpeed,-maxV,+maxV);
	_gunXRot+=_gunXSpeed*deltaT;
	Limit(_gunXRot,Type()->_minGunElev,Type()->_maxGunElev);

	speed=AngleDifference(_gunYRotWanted,_gunYRot)*6;
	delta=speed-_gunYSpeed;
	Limit(delta,-maxA*deltaT,+maxA*deltaT);
	_gunYSpeed+=delta;
	Limit(_gunYSpeed,-maxV,+maxV);
	_gunYRot+=_gunYSpeed*deltaT;
	_gunYRot=AngleDifference(_gunYRot,0);
	Limit(_gunYRot,Type()->_minGunTurn,Type()->_maxGunTurn);
*/
	// move turret
	AIUnit *unit = GunnerUnit();
	if( !unit ) 
	{
		_turret.Stop(Type()->_turret);
	}
	else
	{
/*
		if( GetHit(Type()->_gunHit)>0.9 )
		{
			_turret.GunBroken(Type()->_turret);
		}
		else if( GetHit(Type()->_turretHit)>0.9 )
		{
			_turret.TurretBroken(Type()->_turret);
		}
		else
*/
		{
			_turret._gunStabilized = true;
		}
		_turret.MoveWeapons(Type()->_turret,unit,deltaT);
	}
}

void HelicopterAuto::UpdateStopMode(AIUnit *unit)
{
	if (unit->GetState() == AIUnit::Stopping || unit->GetState() == AIUnit::Stopped)
	{
		_getinUnits.Compact();
		_getoutUnits.Compact();
		bool getin = _getinUnits.Size() > 0;
		bool getout = _getoutUnits.Size() > 0;
		bool supply = _supplyUnits.Size() > 0;
		bool pilot = false;
		for (int i=0; i<_getoutUnits.Size(); i++)
			if (_getoutUnits[i] == unit)
			{
				pilot = true;
				break;
			}
		StopMode mode = SMNone;
		if (pilot || _landing == LMLand) mode = SMLand;
		else if (getin || supply || _landing == LMGetIn) mode = SMGetIn;
		else if (getout || _landing == LMGetOut) mode = SMGetOut;

		if
		(
			mode != _stopMode ||
			unit->GetState() == AIUnit::Stopping && !unit->CheckEmpty(_stopPosition)
		)
		{
			_stopMode = mode;
			FindStopPosition();
		}
	}
	else
	{
		_stopMode = SMNone;
	}
}

void HelicopterAuto::FindStopPosition()
{
	AIUnit *unit = DriverBrain();
	if (!unit)
	{
		Fail("No pilot");
		return;
	}

	bool found = false;

	Vector3 bestPos=Position();

	// preferred position lies in front of us
	Vector3 preferredPos = Position()+Speed()*6;
	{
		VehicleNonAIType *heliHType = VehicleTypes.New("HeliH");
		float minDist2 = Square(500);
		
		for (int i=0; i<GWorld->NBuildings(); i++)
		{
			Vehicle *veh = GWorld->GetBuilding(i);
			if (!veh) continue;
			const VehicleNonAIType *type = veh->GetNonAIType();
			if (!type) continue;
			if (!type->IsKindOf(heliHType)) continue;
			float dist2 = Position().Distance2(veh->Position());
			if (dist2 >= minDist2) continue;
			// check if H is free
			if (!unit->CheckEmpty(veh->Position())) continue;
			// ok
			minDist2 = dist2;
			bestPos = veh->Position();
			found = true;
		}
	}

	if (!found)
	{
		// unlock while searching
		PerformUnlock();

		float bestCost=1e10;
		int range=2;
		do
		{
			// check neighbourghodd
			// increase range if necessary
			int x=toIntFloor(preferredPos.X()*InvLandGrid);
			int z=toIntFloor(preferredPos.Z()*InvLandGrid);
			for( int xx=x-range; xx<=x+range; xx++ )
			for( int zz=z-range; zz<=z+range; zz++ )
			{
				GeographyInfo info=GLOB_LAND->GetGeography(xx,zz);
				float cost=0;
				if( info.u.waterDepth>0 ) continue;
				if( info.u.full ) continue;
				if( info.u.howManyObjects ) continue;
				int grad=info.u.gradient;
				if (_stopMode == SMLand)
				{
					if( grad>=4 ) continue;
					const static int gradPenalty[4]={0,5,10,30};
					cost+=gradPenalty[grad];
					if (info.u.road) cost+=10;
				}
				else
				{
					if( grad>=5 ) continue;
					const static int gradPenalty[5]={0,0,2,5,10};
					cost+=gradPenalty[grad];
					if (info.u.road) cost+=2;
				}
				cost+=sqrt((xx-x)*(xx-x)+(zz-z)*(zz-z))*0.5; // small penalty for distance
				Vector3 pos,normal;
				pos.Init();
				normal.Init();
				pos[0]=xx*LandGrid+LandGrid*0.5,pos[2]=zz*LandGrid+LandGrid*0.5;
				pos[1]=GLOB_LAND->RoadSurfaceYAboveWater(pos[0],pos[2]);
				// check for object collision at given place
				if( bestCost>cost )
				{
					//AIUnit *unit=DriverBrain();
					if( !AIUnit::FindFreePosition(pos,normal,false,this) ) continue;
					bestPos=pos;
					bestCost=cost;
				}
			}
		} while( bestCost>10 && (range+=2)<8 );

		PerformLock();
	}

	if ((_stopPosition-bestPos).SquareSize()>Square(4))
	{
		// reset only when there is significant change
		ResetAutopilot();
	}
	_stopPosition=bestPos;
}

#if _ENABLE_AI
/*!
\patch 1.16 Date 8/10/2001 by Ondra.
- Fixed: Random crash caused occasinally by helicopter flying in formation.
\patch 1.33 Date 11/28/2001 by Ondra.
- Improved: AI Helicopters engaging laser targets.
\patch 1.42 Date 01/04/2002 by Ondra.
- Fixed: Helicopter took off after landing even when no waypoint was active
(bug since 1.40).
\patch 1.78 Date 7/16/2002 by Ondra
- Fixed: AI pilot was sometimes unable to land AH 64 helicopter.
\patch 1.78 Date 7/16/2002 by Ondra
- Fixed: AI Helicopter pilot no longer climbs high when preparing to stop or land.
*/

void HelicopterAuto::AIPilot(AIUnit *unit, float deltaT )
{
	Assert( unit );
	Assert( unit->GetSubgroup() );
	if (!unit->GetSubgroup()) return;
	bool isLeader=unit->IsSubgroupLeader();

	// AI: activate all helpers
	_pilotSpeedHelper=true;
	_pilotDirHelper=true;
	_pilotHeightHelper=true;

	_dirCompensate=1;
	// if aiming for fire, we need quick reactions
	/*
	if( _fireMode>=0 )
	{
		if( !_firePrepareOnly ) _dirCompensate=0.1;
		else _dirCompensate=0.3;
	}
	*/
	_avoidBankJitter=false;

	Vector3 steerPos=SteerPoint(2.0,4.0);

	Vector3 steerWant=PositionWorldToModel(steerPos);
	
	float headChange=atan2(steerWant.X(),steerWant.Z());
	float speedWanted=0;
	
	float inCombat=2-_nearestEnemy*(1.0/400);
	saturate(inCombat,0,1);
	speedWanted=Type()->GetMaxSpeedMs()*inCombat*0.5;

	if (unit->GetCombatMode()<=CMSafe)
	{
		inCombat = 0;
	}

	
	Target *assigned = unit->GetTargetAssigned();
	if (assigned && _sweepTarget!=assigned && Type()->_enableSweep)
	{
		_sweepTarget=assigned;
		_sweepState=SweepDisengage;
		_sweepDelay=Glob.time+10;
	}

	if (_sweepTarget)
	{
		EntityAI *swAI = _sweepTarget->idExact;
		if (unit->GetCombatMode()<=CMSafe || !unit->IsFireEnabled(_sweepTarget))
		{
			_sweepTarget = NULL;
		}
		// check if target is alive
		else if
		(
			// destroyed
			!swAI || swAI->IsDammageDestroyed()
			// or not enemy and not ordered to fire
			|| _sweepTarget->State(unit)<TargetEnemy && unit->GetEnableFireTarget()!=_sweepTarget
		)
		{
			_sweepTarget=NULL;
		}
	}

	_forceDive=1;
	_forceBank=1;

	UpdateStopMode(unit);

	bool autopilot=false;
	if( unit->GetState()==AIUnit::Stopping || unit->GetState()==AIUnit::Stopped )
	{
		// special handling of stop state
		// landing position is in _stopPositon

		Vector3 sPos = _stopPosition;
		if (_stopMode==SMGetIn) sPos[1] += 1.0;
		else if (_stopMode==SMGetOut) sPos[1] += 2.0;

		// direction - opposite to wind
		Vector3Val windDir = GLandscape->GetWind();

		float windSize = windDir.Size();

		Vector3 landDir = Direction();
		if (windSize>0.5)
		{
			float windFactor = windSize*0.3;
			saturate(windFactor,0,0.5);

			landDir = windDir*windFactor+Direction()*(1-windFactor);
			landDir[1] = 0;
			landDir.Normalize();
		}

		Autopilot(deltaT,_stopPosition,VZero,landDir,VZero);
		speedWanted=0;

		// check if heli is already landed
		if (_landContact)
		{
			if (_stopMode==SMLand)
			{
				StopRotor();
				if( _rotorSpeed<0.7 )
				{
					UpdateStopTimeout();
					// note: Pilot may get out - Brain may be NULL
					unit->SendAnswer(AI::StepCompleted);
					if( unit->IsFreeSoldier() ) return;
				}
			}
			if (unit->GetState()==AIUnit::Stopping)
			{
				UpdateStopTimeout();
				// note: Pilot may get out - Brain may be NULL
				unit->SendAnswer(AI::StepCompleted);
				if( unit->IsFreeSoldier() ) return;
			}
		}

		float bottomY = Position().Y()+_shape->GeometryLevel()->Min().Y();
		switch (_stopMode)
		{
			case SMLand:
				autopilot=true;
				if( _state==AutopilotReached )
				{
					float curSurfaceY=GLOB_LAND->RoadSurfaceYAboveWater(Position().X(),Position().Z());
					if( Position().Y()<=curSurfaceY+2.5f || bottomY<curSurfaceY+0.5f)
					{
						StopRotor();
					}
				}
				break;
			case SMGetIn:
			case SMGetOut:
				if (_state == AutopilotReached)
				{
					if( Position().Y()<=_stopPosition.Y()+2.5f || bottomY<_stopPosition.Y()+0.5f)
					{
						if (unit->GetState() == AIUnit::Stopping)
						{
							UpdateStopTimeout();
							unit->SendAnswer(AI::StepCompleted);
							Assert(!unit->IsFreeSoldier());
						}
					}
				}
				autopilot=true;
				break;
		}
	}
	else if( _sweepTarget && Type()->_enableSweep)
	{
		bool laserTarget = _sweepTarget->idExact->GetType()->GetLaserTarget();
		speedWanted=laserTarget ? 0 : Type()->GetMaxSpeedMs()*0.5f;
		Vector3 relPos=_sweepTarget->AimingPosition()-Position();
		float distXZ=relPos.SizeXZ();
		const float safeDistance = laserTarget ? 400 : 250;
		if( _sweepState==SweepDisengage )
		{
			Vector3 aimDir=PositionWorldToModel(_sweepTarget->AimingPosition());
			headChange=atan2(aimDir.X(),aimDir.Z());

			// move - to be hard target
			if( distXZ>safeDistance || fabs(headChange)<0.5f )
			{
				_sweepState=SweepEngage;
				_sweepDelay=Glob.time+25;
			}
			headChange=0;
		}
		else if( _sweepState==SweepEngage )
		{
			Vector3 aimDir=PositionWorldToModel(_sweepTarget->AimingPosition());
			headChange=atan2(aimDir.X(),aimDir.Z());
			// move - to be hard target
			if( distXZ<100 )
			{
				_sweepState=SweepFire;
				_sweepDelay=Glob.time+5; // start immediatelly
				_sweepDir=_sweepTarget->AimingPosition()-Position();
			}

			if( aimDir.Z()>20 && fabs(headChange)<0.2f )
			{ // if the target is horizontally aimed, make vertical adjust
				float minSpeed=laserTarget ? Type()->GetMaxSpeedMs()*0.3f : 0;
				float speed=ModelSpeed()[2];
				if( speed>minSpeed )
				{ // we are flying fast enough
					// actual aiming (using speed)
					Vector3 relPos=_sweepTarget->AimingPosition()-Position();
					_forceDive=(relPos.Y()-3)*relPos.InvSizeXZ();
					saturate(_forceDive,-0.7f,0);
				}
			}
		}
		else if( _sweepState==SweepFire )
		{
			Vector3 relSweepDir=DirectionWorldToModel(_sweepDir);
			headChange=atan2(relSweepDir.X(),relSweepDir.Z());
			if( fabs(headChange)>0.6 && ( distXZ<50 || distXZ>150 ) )
			{
				_sweepState=SweepDisengage;
				_sweepDelay=Glob.time+10; // start immediatelly
			}	

		}

		// sweep target should be slightly below
		float wantAbove=distXZ*0.1;
		saturate(wantAbove,30,50);
		float wantY=_sweepTarget->AimingPosition().Y()+wantAbove;

		float curSurfaceY=GLOB_LAND->SurfaceYAboveWater(Position().X(),Position().Z());
		_pilotHeight=wantY-curSurfaceY;
		
		// when firing at laser target, do not fly too high
		saturate(_pilotHeight,30,laserTarget ? 60 : 200);

		if( Glob.time>_sweepDelay ) _sweepTarget=NULL;
	}
	else if( !isLeader )
	{
		AIUnit *leader = unit->GetSubgroup()->Leader();
		if (!EngineIsOn() && leader)
		{
			// check if we should take off
			// check if leader is airborne
			VehicleWithAI *veh = leader->GetVehicle();
			if (veh->Airborne() || veh->Position().Distance2(Position())>Square(200))
			{
				EngineOn();
			}
		}
		// trivial solution always works
		unit->ForceReplan();
		_limitSpeed=GetType()->GetMaxSpeedMs()*1.5;

		if (leader)
		{
			Vector3Val relFormWanted = unit->GetFormationRelative()-leader->GetFormationRelative();

			VehicleWithAI *leaderVeh = leader->GetVehicle();

			Matrix4 formTransform;
			formTransform.SetDirectionAndUp(leaderVeh->Direction(),VUp);
			formTransform.SetPosition(leaderVeh->Position());

			Vector3 formPos = formTransform.FastTransform(relFormWanted);

			// use autopilot to stay in position
			Autopilot(deltaT,formPos,leaderVeh->Speed(),leaderVeh->Direction(),leaderVeh->Speed());
			autopilot=true;

			// predict leader's position
			Vector3Val leaderPos=leaderVeh->Position();
			float leaderSurfY=GLandscape->SurfaceYAboveWater(leaderPos.X(),leaderPos.Z());
			_pilotHeight=leaderPos.Y()-leaderSurfY;
			saturateMax(_pilotHeight,_defPilotHeight);
		}
		else
		{
			Autopilot(deltaT,Position(),VZero,Direction(),VZero);
		}

	}
	else
	{
		// 

		speedWanted=_limitSpeed; // go faster
		
		#if DIAG_SPEED
		if( this==GWorld->CameraOn() )
		{
			LogF("Basic speed %.1f",speedWanted*3.6);
		}
		#endif

		// check path position
		const Path &path=unit->GetPath();
		if( path.Size()>=2 )
		{
			EngineOn();

			float precision=GetPrecision();
			//_moveMode=gotoNormal;
			float cost=path.CostAtPos(Position());
			Vector3 pos=path.PosAtCost(cost,Position());

			float distEnd2=Position().Distance2(path.End());
			float dist2=(Position()-pos).SquareSizeXZ();
			// check if we have first point of Plan complete
			if( distEnd2<Square(precision) || cost>path.EndCost() )
			{
				unit->SendAnswer(AI::StepCompleted);
//				unit->ForceReplan();
			}
			if( dist2>Square(precision*3) )
			{
				unit->SendAnswer(AI::StepTimeOut);
			}
			_pilotHeight=_defPilotHeight;
		}
		else
		{
			//_moveMode=gotoWait;
			speedWanted=0;
			saturateMin(_pilotHeight,_defPilotHeight);

			/*
			Autopilot(deltaT,_stopPosition,VZero,Direction(),VZero);
			speedWanted=0;
			*/
			// do not turn engine on
		}

		if( _stratGoToPos.SquareSize()>0.1f )
		{
			// strategic target known
			float finalDist2=(_stratGoToPos-Position()).SquareSizeXZ();
			if( finalDist2<Square(1000) )
			{
				float maxSpd=60;
				if( finalDist2<Square(500) )
				{
					maxSpd=40;
					if( finalDist2<Square(300) )
					{
						maxSpd=20;
						if( finalDist2<Square(150) )
						{
							maxSpd=10;
						}
					}
				}
				saturateMin(speedWanted,maxSpd);
			}
		}


		saturate(speedWanted,-_limitSpeed,+_limitSpeed); // move max. by given speed
		if( inCombat>=0.3f )
		{
			float combatSpeed=Type()->GetMaxSpeedMs()*inCombat*0.5f;
			saturateMax(speedWanted,combatSpeed);
		}
	}

	Assert(unit);

	AvoidCollision(deltaT,speedWanted,headChange);
	/*
	if( unit->GetState()!=AIUnit::Stopping && unit->GetState()!=AIUnit::Stopped )
	{
		EngineOn();
	}
	*/
	
	if( !autopilot )
	{
		float avoidGround=0.5f;
		float speedSize=fabs(ModelSpeed().Z());
		saturateMax(avoidGround,speedSize*0.35f);

		_pilotSpeed=Vector3(0,0,speedWanted);

		if( avoidGround>0 ) AvoidGround(avoidGround);

		// if we need to decrease speed significantly, we may want to use braking maneuver

		if (ModelSpeed().Z()-speedWanted>Type()->GetMaxSpeedMs()*0.5f && speedWanted>=0)
		{
			BrakingManeuver();
		}
		else
		{
			float maxTurn=H_PI;
			if( inCombat>0.1 )
			{
				const float maxTurnInCombat=0.3f;
				const float maxTurnNoCombat=0.9f;
				maxTurn=(maxTurnInCombat-maxTurnNoCombat)*inCombat+maxTurnNoCombat;
			}
			saturate(headChange,-maxTurn,+maxTurn);
			float curHeading=atan2(Direction()[0],Direction()[2]);
			_pilotHeading=curHeading+headChange;
		}

	}

	#if 0
	if( this==GWorld->CameraOn() )
	{
		LogF
		(
			"InCombat %.2f wSpd %.1f pSpd %.1f spd %.1f",
			inCombat,speedWanted*3.6,_pilotSpeed[2]*3.6,ModelSpeed()[2]*3.6
		);
	}
	#endif

	#if 0
		if( this==GWorld->CameraOn() && _fireMode>=0 )
		{
			GEngine->ShowMessage
			(
				100,"Heli mode %d state %d, delay %.1f spd %.1f",
				_fireMode,_fireState,_fireStateDelay-Glob.time,speedWanted
			);
		}
	#endif

	// aim to current target
	// if( !_isDead ) AIFire(unit, deltaT);
}
#endif // _ENABLE_AI

/*!
\patch 1.31 Date 11/23/2001 by Ondra.
- Improved: MP: Helicopter remote prediction more accurate.
*/

void HelicopterAuto::DrawDiags()
{
	if (CommanderUnit())
	{
		LODShapeWithShadow *forceArrow=GScene->ForceArrow();

		if( _stopPosition.SquareSize()>0.5 )
		{
			Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),-1);
			obj->SetPosition(_stopPosition);
			obj->SetScale(2);
			obj->SetConstantColor(PackedColor(Color(1,0,1)));
			GLandscape->ShowObject(obj);
		}

		#if 1
			// draw pilot diags
			{
				Matrix3 rotY(MRotationY,-_pilotHeading);
				Vector3 pilotDir=rotY.Direction();
				Ref<Object> arrow=new Object(forceArrow,-1);
				Point3 pos=Position()+Vector3(0,5,0);

				float size=0.6;
				arrow->SetPosition(pos);
				arrow->SetOrient(pilotDir,VUp);
				arrow->SetPosition
				(
					arrow->PositionModelToWorld(forceArrow->BoundingCenter()*size)
				);
				arrow->SetScale(size);
				arrow->SetConstantColor(PackedColor(Color(1,1,0)));

				GScene->ObjectForDrawing(arrow);
			}
		#endif
	}

	base::DrawDiags();
}

RString HelicopterAuto::DiagText() const
{
	RString text=base::DiagText();
	char buf[256];
	sprintf
	(
		buf," pSpd=%.1f, MRW=%.2f, ",
		_pilotSpeed[2]*3.6,_mainRotorWanted
	);
	if (_state!=AutopilotNear)
	{
		text = text + FindEnumName(_state);
	}
	return text+RString(buf);
}

LSError Helicopter::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	// TODO: serialize other members

	#define SERIAL(name) CHECK(ar.Serialize(#name, _##name, 1))
	#define SERIAL_DEF(name,value) CHECK(ar.Serialize(#name, _##name, 1, value))

	if (!IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
	{
		SERIAL_DEF(rotorPosition,0);
		SERIAL_DEF(rotorSpeed,1);SERIAL_DEF(rotorSpeedWanted,1);

		SERIAL_DEF(backRotor,0);SERIAL_DEF(backRotorWanted,0);
		SERIAL_DEF(mainRotor,0);SERIAL_DEF(mainRotorWanted,0);
		SERIAL_DEF(cyclicForward,0);SERIAL_DEF(cyclicForwardWanted,0);
		SERIAL_DEF(cyclicAside,0);SERIAL_DEF(cyclicAsideWanted,0);
		SERIAL_DEF(rotorDive,0);SERIAL_DEF(rotorDiveWanted,0);

		CHECK(_turret.Serialize(ar))
	}
/*
	SERIAL_DEF(gunYRot,0);SERIAL_DEF(gunYRotWanted,0);
	SERIAL_DEF(gunXRot,0);SERIAL_DEF(gunXRotWanted,0);
	SERIAL_DEF(gunXSpeed,0);SERIAL_DEF(gunYSpeed,0);
*/

	return LSOK;
}

static const EnumName AutopilotStateNames[]=
{
	EnumName(AutopilotFar, "FAR"),
	EnumName(AutopilotBrake, "BRAKE"),
	EnumName(AutopilotNear, "NEAR"),
	EnumName(AutopilotAlign, "ALIGN"),
	EnumName(AutopilotReached, "REACHED"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(AutopilotState dummy)
{
	return AutopilotStateNames;
}

RString HelicopterAuto::GetActionName(const UIAction &action)
{
	switch (action.type)
	{
		case ATAutoHover:
			if (_hoveringAutopilot) return LocalizeString(IDS_ACTION_HOVER_CANCEL);
			else return LocalizeString(IDS_ACTION_HOVER);
	}
	return base::GetActionName(action);
}

void HelicopterAuto::PerformAction(const UIAction &action, AIUnit *unit)
{
	switch (action.type)
	{
		case ATAutoHover:
			_hoveringAutopilot = !_hoveringAutopilot;
			return;
	}
	base::PerformAction(action,unit);
}

void HelicopterAuto::GetActions(UIActions &actions, AIUnit *unit, bool now)
{
	if (unit && unit==DriverBrain() && QIsManual())
	{
		actions.Add(ATAutoHover, this, 0.9);
	}

	base::GetActions(actions, unit, now);
}

bool HelicopterAuto::IsStopped() const
{
	// check manual heli stopped
	// check height above surface
	if (_landContact || _objectContact)
	{
		return base::IsStopped();
	}

	// high flying heli cannot be stopped
	float surfaceY=GLOB_LAND->SurfaceYAboveWater(Position()[0],Position()[2]);
	float height = Position().Y()-surfaceY;
	if (height>5) return false;

	return base::IsStopped();
}

LSError HelicopterAuto::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	if (!IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
	{
		CHECK(ar.Serialize("defPilotHeight", _defPilotHeight, 1, 50))
		CHECK(ar.Serialize("_pilotHeight", _pilotHeight, 1, 2.5))
		CHECK(ar.Serialize("_pilotSpeed", _pilotSpeed, 1, VZero))
		CHECK(ar.Serialize("_stopPosition", _stopPosition, 1, VZero))
		CHECK(ar.SerializeEnum("state", _state, 1, AutopilotNear))
	}
	// TODO: serialize other members

	return LSOK;
}

/*!
\patch 1.21 Date 8/24/2001 by Jirka
- Fixed: Added more transfered properties in MP for Helicopters.
*/

NetworkMessageType HelicopterAuto::GetNMType(NetworkMessageClass cls) const
{
	switch (cls)
	{
	case NMCCreate:
		return NMTCreateHelicopter;
	case NMCUpdateGeneric:
		return NMTUpdateHelicopter;
	case NMCUpdatePosition:
		return NMTUpdatePositionHelicopter;
	default:
		return base::GetNMType(cls);
	}
}

class IndicesCreateHelicopter : public IndicesCreateVehicle
{
	typedef IndicesCreateVehicle base;

public:
	int rotorSpeed;

	IndicesCreateHelicopter();
	NetworkMessageIndices *Clone() const {return new IndicesCreateHelicopter;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesCreateHelicopter::IndicesCreateHelicopter()
{
	rotorSpeed = -1;
}

void IndicesCreateHelicopter::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);

	SCAN(rotorSpeed)
}

NetworkMessageIndices *GetIndicesCreateHelicopter() {return new IndicesCreateHelicopter();}

class IndicesUpdateHelicopter : public IndicesUpdateTransport
{
	typedef IndicesUpdateTransport base;

public:
	int rotorSpeedWanted;
	int state;
	int pilotHeight;
	int pilotSpeed;
	int stopMode;
	int stopPosition;
	int pilotSpeedHelper;
	int pilotHeightHelper;
	int pilotDirHelper;

	IndicesUpdateHelicopter();
	NetworkMessageIndices *Clone() const {return new IndicesUpdateHelicopter;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesUpdateHelicopter::IndicesUpdateHelicopter()
{
	rotorSpeedWanted = -1;
	state = -1;
	pilotHeight = -1;
	pilotSpeed = -1;
	stopMode = -1;
	stopPosition = -1;
	pilotSpeedHelper = -1;
	pilotHeightHelper = -1;
	pilotDirHelper = -1;
}

void IndicesUpdateHelicopter::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);

	SCAN(rotorSpeedWanted)
	SCAN(state)
	SCAN(pilotHeight)
	SCAN(pilotSpeed)
	SCAN(stopMode)
	SCAN(stopPosition)

	SCAN(pilotSpeedHelper)
	SCAN(pilotHeightHelper)
	SCAN(pilotDirHelper)
}

NetworkMessageIndices *GetIndicesUpdateHelicopter() {return new IndicesUpdateHelicopter();}

class IndicesUpdatePositionHelicopter : public IndicesUpdatePositionVehicle
{
	typedef IndicesUpdatePositionVehicle base;

public:
	int turret;
	int backRotorWanted;
	int mainRotorWanted;
	int cyclicForwardWanted;
	int cyclicAsideWanted;
	int rotorDiveWanted;
	int bankWanted;
	int diveWanted;
	int pilotHeading;
	int pilotDive;

	IndicesUpdatePositionHelicopter();
	NetworkMessageIndices *Clone() const {return new IndicesUpdatePositionHelicopter;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesUpdatePositionHelicopter::IndicesUpdatePositionHelicopter()
{
	turret = -1;
	backRotorWanted = -1;
	mainRotorWanted = -1;
	cyclicForwardWanted = -1;
	cyclicAsideWanted = -1;
	rotorDiveWanted = -1;
	bankWanted = -1;
	diveWanted = -1;
	pilotHeading = -1;
	pilotDive = -1;
}

void IndicesUpdatePositionHelicopter::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);
	SCAN(turret)
	SCAN(backRotorWanted)
	SCAN(mainRotorWanted)
	SCAN(cyclicForwardWanted)
	SCAN(cyclicAsideWanted)
	SCAN(rotorDiveWanted)
	SCAN(bankWanted)
	SCAN(diveWanted)
	SCAN(pilotHeading)
	SCAN(pilotDive)
}

NetworkMessageIndices *GetIndicesUpdatePositionHelicopter() {return new IndicesUpdatePositionHelicopter();}

NetworkMessageFormat &HelicopterAuto::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	switch (cls)
	{
	case NMCCreate:
		base::CreateFormat(cls, format);

		format.Add("rotorSpeed", NDTFloat, NCTFloat0To1, DEFVALUE(float, 0), DOC_MSG("Initial rotor speed"));
		break;
	case NMCUpdateGeneric:
		base::CreateFormat(cls, format);

		format.Add("rotorSpeedWanted", NDTFloat, NCTFloat0To1, DEFVALUE(float, 0), DOC_MSG("Wanted rotor speed"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);
		format.Add("state", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Autopilot state"), ET_NOT_EQUAL, ERR_COEF_MODE);
		format.Add("pilotHeight", NDTFloat, NCTNone, DEFVALUE(float, 0), DOC_MSG("Height, wanted by pilot"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);
		format.Add("pilotSpeed", NDTVector, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Speed, wanted by pilot"), ET_ABS_DIF, 1);
		format.Add("stopMode", NDTInteger, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Landing type"), ET_NOT_EQUAL, ERR_COEF_MODE);
		format.Add("stopPosition", NDTVector, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Landing position"), ET_ABS_DIF, 0.1);

		format.Add("pilotSpeedHelper", NDTBool, NCTNone, DEFVALUE(bool, false), DOC_MSG("pilotSpeed is valid"), ET_NOT_EQUAL, ERR_COEF_VALUE_MAJOR);
		format.Add("pilotHeightHelper", NDTBool, NCTNone, DEFVALUE(bool, true), DOC_MSG("pilotHeight is valid"), ET_NOT_EQUAL, ERR_COEF_VALUE_MAJOR);
		format.Add("pilotDirHelper", NDTBool, NCTNone, DEFVALUE(bool, true), DOC_MSG("pilotHeading is valid"), ET_NOT_EQUAL, ERR_COEF_VALUE_MAJOR);

		break;
	case NMCUpdatePosition:
		base::CreateFormat(cls, format);
		format.Add("turret", NDTObject, NCTNone, DEFVALUE_MSG(NMTUpdateTurret), DOC_MSG("Turret object"), ET_ABS_DIF, 1);
		format.Add("backRotorWanted", NDTFloat, NCTFloatM1ToP1, DEFVALUE(float, 0), DOC_MSG("Wanted back rotor control"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);
		format.Add("mainRotorWanted", NDTFloat, NCTFloatM1ToP1, DEFVALUE(float, 0), DOC_MSG("Wanted main rotor control"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);
		format.Add("cyclicForwardWanted", NDTFloat, NCTFloatM1ToP1, DEFVALUE(float, 0), DOC_MSG("Wanted forward cyclic position"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);
		format.Add("cyclicAsideWanted", NDTFloat, NCTFloatM1ToP1, DEFVALUE(float, 0), DOC_MSG("Wanted aside cyclic position"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);
		format.Add("rotorDiveWanted", NDTFloat, NCTFloatM1ToP1, DEFVALUE(float, 0), DOC_MSG("Wanted rotor dive"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);
		format.Add("bankWanted", NDTFloat, NCTFloatM1ToP1, DEFVALUE(float, 0), DOC_MSG("Wanted bank position"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);
		format.Add("diveWanted", NDTFloat, NCTFloatM1ToP1, DEFVALUE(float, 0), DOC_MSG("Wanted dive"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);

		format.Add("pilotHeading", NDTFloat, NCTFloatAngle, DEFVALUE(float, 0), DOC_MSG("Heading, wanted by pilot"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);
		format.Add("pilotDive", NDTFloat, NCTFloatM1ToP1, DEFVALUE(float, 0), DOC_MSG("Dive, wanted by pilot"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);

		break;
	default:
		base::CreateFormat(cls, format);
		break;
	}
	return format;
}

HelicopterAuto *HelicopterAuto::CreateObject(NetworkMessageContext &ctx)
{
	Entity *veh = Entity::CreateObject(ctx);
	HelicopterAuto *heli = dyn_cast<HelicopterAuto>(veh);
	if (!heli) return NULL;
	heli->TransferMsg(ctx);
	return heli;
}

TMError HelicopterAuto::TransferMsg(NetworkMessageContext &ctx)
{
	switch (ctx.GetClass())
	{
	case NMCCreate:
		if (ctx.IsSending())
		{
			TMCHECK(base::TransferMsg(ctx))
		}
		{
			Assert(dynamic_cast<const IndicesCreateHelicopter *>(ctx.GetIndices()))
			const IndicesCreateHelicopter *indices = static_cast<const IndicesCreateHelicopter *>(ctx.GetIndices());

			ITRANSF(rotorSpeed)
		}
		break;
	case NMCUpdateGeneric:
		TMCHECK(base::TransferMsg(ctx))
		{
			Assert(dynamic_cast<const IndicesUpdateHelicopter *>(ctx.GetIndices()))
			const IndicesUpdateHelicopter *indices = static_cast<const IndicesUpdateHelicopter *>(ctx.GetIndices());

			ITRANSF(rotorSpeedWanted)

			ITRANSF_ENUM(state)
			ITRANSF(pilotHeight)
			ITRANSF(pilotSpeed)
			ITRANSF_ENUM(stopMode)
			ITRANSF(stopPosition)
			ITRANSF(pilotSpeedHelper)
			ITRANSF(pilotHeightHelper)
			ITRANSF(pilotDirHelper)

			if (!ctx.IsSending() && ctx.GetInitialUpdate())
			{
				_rotorSpeed = _rotorSpeedWanted;
			}
		}
		break;
	case NMCUpdatePosition:
		{
			Assert(dynamic_cast<const IndicesUpdatePositionHelicopter *>(ctx.GetIndices()))
			const IndicesUpdatePositionHelicopter *indices = static_cast<const IndicesUpdatePositionHelicopter *>(ctx.GetIndices());

			Matrix3 oldTrans = Orientation();
			TMCHECK(base::TransferMsg(ctx))
			if (ctx.IsSending() || !(GunnerUnit() && GunnerUnit()->GetPerson()->IsLocal()))
				TMCHECK(ctx.IdxTransferObject(indices->turret, _turret))
			_turret.Stabilize
			(
				this, Type()->_turret,
				oldTrans, Orientation()
			);
			ITRANSF(backRotorWanted)
			ITRANSF(mainRotorWanted)
			ITRANSF(cyclicForwardWanted)
			ITRANSF(cyclicAsideWanted)
			ITRANSF(rotorDiveWanted)
			ITRANSF(bankWanted)
			ITRANSF(diveWanted)
			ITRANSF(pilotHeading)
			ITRANSF(pilotDive)
			if (!ctx.IsSending() && ctx.GetInitialUpdate())
			{
				_mainRotor = _mainRotorWanted;
			}
		}
		break;
	default:
		return base::TransferMsg(ctx);
	}
	return TMOK;
}

#define MAKE_FINITE(x) \
	if (!_finite(x)) \
	{ \
		RptF("%s(%d) : Infinite value %s==%g",__FILE__,__LINE__,#x,x); \
		x = 1000; \
	}


float HelicopterAuto::CalculateError(NetworkMessageContext &ctx)
{
	float error = 0;
	switch (ctx.GetClass())
	{
	case NMCUpdateGeneric:
		error += base::CalculateError(ctx);
		{
			Assert(dynamic_cast<const IndicesUpdateHelicopter *>(ctx.GetIndices()))
			const IndicesUpdateHelicopter *indices = static_cast<const IndicesUpdateHelicopter *>(ctx.GetIndices());

			ICALCERR_ABSDIF(float, rotorSpeedWanted, ERR_COEF_VALUE_MAJOR)
			ICALCERR_NEQ(int, state, ERR_COEF_MODE)
			ICALCERR_ABSDIF(float, pilotHeight, ERR_COEF_VALUE_MAJOR)
			ICALCERR_DIST(pilotSpeed, 1)
			ICALCERR_NEQ(int, stopMode, ERR_COEF_MODE)
			ICALCERR_DIST(stopPosition, 0.1)

			ICALCERR_NEQ(bool, pilotSpeedHelper, ERR_COEF_VALUE_MAJOR)
			ICALCERR_NEQ(bool, pilotHeightHelper, ERR_COEF_VALUE_MAJOR)
			ICALCERR_NEQ(bool, pilotDirHelper, ERR_COEF_VALUE_MAJOR)
		}
		break;
	case NMCUpdatePosition:
		{
			error += 	base::CalculateError(ctx);
			Assert(dynamic_cast<const IndicesUpdatePositionHelicopter *>(ctx.GetIndices()))
			const IndicesUpdatePositionHelicopter *indices = static_cast<const IndicesUpdatePositionHelicopter *>(ctx.GetIndices());

			int index = indices->turret;
			if (index >= 0)
			{
				NetworkMessageFormatBase *format = const_cast<NetworkMessageFormatBase *>(ctx.GetFormat());
				NetworkMessageFormatItem &item = format->GetItem(index);
				CHECK_ASSIGN(typeVal,item.defValue,const RefNetworkDataTyped<int>);
				int type = typeVal.GetVal();
				NetworkMessageFormatBase *subformat = ctx.GetComponent()->GetFormat((NetworkMessageType)type);
				if (subformat)
				{
					const RefNetworkData &val = ctx.GetMessage()->values[index];
					CHECK_ASSIGN(msgVal,val,const RefNetworkDataTyped<NetworkMessage>);
					NetworkMessage &submsg =msgVal.GetVal();
					NetworkMessageContext subctx(&submsg, subformat, ctx);
					error += _turret.CalculateError(subctx);
				}
			}

			MAKE_FINITE(_backRotorWanted)
			MAKE_FINITE(_mainRotorWanted)
			MAKE_FINITE(_cyclicForwardWanted)
			MAKE_FINITE(_cyclicAsideWanted)
			MAKE_FINITE(_bankWanted)
			MAKE_FINITE(_diveWanted)
			MAKE_FINITE(_pilotHeading)
			MAKE_FINITE(_pilotDive)

			ICALCERR_ABSDIF(float, backRotorWanted, ERR_COEF_VALUE_MAJOR)
			ICALCERR_ABSDIF(float, mainRotorWanted, ERR_COEF_VALUE_MAJOR)
			ICALCERR_ABSDIF(float, cyclicForwardWanted, ERR_COEF_VALUE_MAJOR)
			ICALCERR_ABSDIF(float, cyclicAsideWanted, ERR_COEF_VALUE_MAJOR)
			ICALCERR_ABSDIF(float, rotorDiveWanted, ERR_COEF_VALUE_MAJOR)
			ICALCERR_ABSDIF(float, bankWanted, ERR_COEF_VALUE_MAJOR)
			ICALCERR_ABSDIF(float, diveWanted, ERR_COEF_VALUE_MAJOR)

			ICALCERR_ABSDIF(float, pilotHeading, ERR_COEF_VALUE_MAJOR)
			ICALCERR_ABSDIF(float, pilotDive, ERR_COEF_VALUE_MAJOR)
		}
		break;
	default:
		error += base::CalculateError(ctx);
		break;
	}
	DoAssert(_finite(error));
	return error;
}

