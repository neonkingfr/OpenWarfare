// Poseidon - vehicle class
// (C) 1997, Suma
#include "wpch.hpp"
#include "frameInv.hpp"

FrameWithInverse::FrameWithInverse( Matrix4Par trans, Matrix4Par invTrans )
{
	Frame::SetTransform(trans);
	_invTransform = invTrans;
}

