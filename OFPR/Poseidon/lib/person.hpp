#ifdef _MSC_VER
#pragma once
#endif

#ifndef _PERSON_HPP
#define _PERSON_HPP

/*!
\file
Interface for Person class
*/

#include "vehicleAI.hpp"

class Building;

//!fictions enum - used just to have type checking
enum ManVehAction
{
	ManVehActNone=-1,
};

TypeIsSimple(ManVehAction)

//!enum definind all possible motion actions
//enum ManAction;

#include "dynEnum.hpp"

DECL_ENUM(MoveId)
TypeIsSimple(MoveId);

extern DynEnum GActionVehNames;

//! Abstract class Person - interface for any person related activites
/*!
	This class defines interface used to control any person activity.
	It also provides some minimal default implementation for some funcions.
*/
class Person: public VehicleSupply
{
	typedef VehicleSupply base;

	protected:
	Ref<AIUnit> _brain;
	AIUnitInfo _info;
	SensorRowID _sensorRowID;
	/*!
	\patch_internal 1.28 Date 10/29/2001 by Jirka
	- Changed: _remotePlayer contain multiplayer ID of player
	_remotePlayer == 1 - AI Player
	*/
	int _remotePlayer;

	public:
	Person(VehicleType *name, bool fullCreate=true);
	~Person();

	AIUnit *Brain() const {return _brain;}
#ifdef _MSC_VER
	void SetBrain( AIUnit *brain ) {_brain=brain;}
#else
	void SetBrain( AIUnit *brain );
#endif
	virtual AIUnit *ObserverUnit() const {return _brain;}
	virtual AIUnit *CommanderUnit() const {return _brain;}
	virtual AIUnit *PilotUnit() const {return _brain;}
	virtual AIUnit *GunnerUnit() const {return _brain;}

	AIUnitInfo& GetInfo() {return _info;}
	void SetInfo(AIUnitInfo& info) {_info = info;}

	float GetExperience() const {return _info._experience;}
	Rank GetRank() const {return _info._rank;}
	void SetRank(Rank rank) {_info._rank = rank;}

	TargetSide GetTargetSide() const;

	virtual void KilledBy( EntityAI *owner );

	bool QIsManual() const;
	void SetRemotePlayer(int player) {_remotePlayer = player;}
	int GetRemotePlayer() const {return _remotePlayer;}
	bool IsRemotePlayer() const {return !IsLocal() && _remotePlayer != 1;}
	bool IsNetworkPlayer() const;

	void SetSensorRowID( SensorRowID sensorRowID ) {_sensorRowID=sensorRowID;}
	SensorRowID GetSensorRowID() const {return _sensorRowID;}

	virtual LODShapeWithShadow *GetOpticsModel(Person *person);
	virtual PackedColor GetOpticsColor(Person *person);
	virtual bool GetForceOptics(Person *person) const; 

	virtual bool IsNVEnabled() const;
	virtual bool IsNVWanted() const;
	virtual void SetNVWanted(bool set = true);
	virtual void DrawNVOptics();

	void CheckAmmo
	(
		const MuzzleType * &muzzle1, const MuzzleType * &muzzle2,
		int &slots1, int &slots2, int &slots3
	);

	void Simulate( float deltaT, SimulationImportance prec );

	virtual void ResetMovement(float speed, int action = -1) = NULL;
	//! Action based interface to control person - smooth transition
	virtual bool PlayAction(ManAction action, ActionContextBase *context=NULL);
	//! Action based interface to control person - immediate transition
	virtual bool SwitchAction(ManAction action, ActionContextBase *context=NULL);

	//!used to switch move for vehicle crews (action id given by ActionVehMap)
	virtual void SwitchVehicleAction(ManVehAction action);

	virtual void ResetStatus();

	virtual UnitPosition GetUnitPosition() const;
	virtual void SetUnitPosition(UnitPosition status);

	virtual void SetFace(RString name, RString player = "");
	virtual void SetGlasses(RString name);

	virtual void ApplyAnimation( int level, RStringB move, float time ) {}
	virtual void ApplyDeanimation( int level ) {}
	virtual void BasicSimulation( float deltaT, SimulationImportance prec, float speedFactor ) {} // also inside vehicle
	virtual float GetLegPhase() const;

	virtual float GetAnimSpeed(RStringB move) {return 1;}
	virtual Vector3 GetPilotPosition(CameraType camType) const {return VZero;}

	virtual void ShowHead(int level, bool show = true) {}
	virtual void ShowWeapons(bool showPrimary = true, bool showSecondary = true) {}

	virtual EntityAI *GetFlagCarrier();
	virtual void SetFlagCarrier(EntityAI *veh);

	virtual void HideBody() {}

	virtual LSError Serialize(ParamArchive &ar);
	virtual LSError SerializeIdentity(ParamArchive &ar);

	virtual void CatchLadder(Building *obj, int ladder, bool up);
	virtual void DropLadder(Building *obj, int ladder);
	virtual bool IsOnLadder(Building *obj, int ladder) const;

	virtual bool IsWoman() const {return false;}
	
	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx);

	private: // no default copy
	Person( const Person &src );
	void operator =( const Person &src );

	USE_CASTING(base)
};

typedef Person VehicleWithBrain;
//template Ref<Person>;

#endif
