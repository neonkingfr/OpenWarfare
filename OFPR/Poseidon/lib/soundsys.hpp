#ifdef _MSC_VER
#pragma once
#endif

#ifndef _SOUNDSYS_HPP
#define _SOUNDSYS_HPP

#include "types.hpp"

#include <Es/Strings/rString.hpp>

enum WaveKind {WaveEffect,WaveSpeech,WaveMusic};

typedef void WaveCallback( AbstractWave *wave, RefCount *context );

class AbstractWave: public RefCountWithLinks
{
	RString _name;
	bool _sticky;
	WaveCallback *_onTerminate;
	Ref<RefCount> _onTerminateContext;

	WaveCallback *_onPlay;
	Ref<RefCount> _onPlayContext;

	protected:
	float _stopTreshold; // sound should be stopped if Glob.time>_stopTime

	// playing one sample
	public:

	RString Name() const {return _name;}
	explicit AbstractWave( RString name );
	void SetName( RString name ) {_name=name;}
	
	virtual ~AbstractWave();
	
	virtual void Queue( AbstractWave *wave, int repeat=1 ) = NULL; // queue playing
	virtual void Repeat( int repeat=1 ) = NULL; // set repeat count (1 or infinity)

	virtual void Play() = NULL; // start playing
	virtual void Stop() = NULL; // stop playing immediatelly
	virtual void LastLoop() = NULL; // stop playing at the end of the loop

	virtual void PlayUntilStopValue( float time ) = NULL; // play until some time
	virtual void SetStopValue( float time ) = NULL; // play until some time

	void OnTerminateOnce();
	void OnPlayOnce();

	void SetOnTerminate( WaveCallback *function, RefCount *context )
	{
		_onTerminate = function;
		_onTerminateContext = context;
	}
	void SetOnPlay( WaveCallback *function, RefCount *context )
	{
		_onPlay = function;
		_onPlayContext = context;
	}
	
	virtual bool IsWaiting() = NULL;
	virtual void Skip( float deltaT ) = NULL; // advance time
	virtual void Advance( float deltaT ) = NULL; // skip if stopped
	virtual float GetLength() const = NULL; // get wave length in sec

	virtual bool IsStopped() = NULL; // query for Stop status
	virtual bool IsTerminated() = NULL; // query for termination status
	virtual void Restart() = NULL; // start playing again
	
	// set sound parameters
	virtual bool IsMuted() const = NULL; // check if wave is muted
	virtual float GetVolume() const = NULL;
	virtual void SetVolume( float volume, float freq=1.0, bool immediate=false ) = NULL;

	// get volume scanned in file
	virtual float GetFileMaxVolume() const = NULL;
	virtual float GetFileAvgVolume() const = NULL;

	virtual void SetAccomodation( float accom=1.0 ) = NULL;
	virtual float GetAccomodation() const = NULL;
	virtual void EnableAccomodation( bool enable ) = NULL;
	virtual bool AccomodationEnabled() const = NULL;

	virtual void SetKind( WaveKind kind ) = NULL;
	virtual WaveKind GetKind() const = NULL;
	
	void SetSticky( bool sticky ){_sticky=sticky;}
	bool GetSticky() const {return _sticky;}

	virtual void SetPosition
	(
		Vector3Par pos, Vector3Par vel, bool immediate=false
	) = NULL;
	virtual Vector3 GetPosition() const = NULL;
	// set if sound should be 3D processed
	// if not, volume is computed based on current source position
	virtual void Set3D( bool is3D ) = NULL;
	virtual bool Get3D() const = NULL;
	//virtual void DisablePosition( float volume, bool immediate=false ) = NULL;
	//virtual void EnablePosition( float volume, bool immediate=false ) = NULL;

	//virtual bool PositionEnabled() const = NULL;
	// equvalent distance of 2D sounds for position disabled sounds
	// this is necessary for loudness calculation
	virtual float Distance2D() const = NULL;
};

enum SoundEnvironmentType
{
	SEPlain, // normal
	SECity, // houses around
	SEForest, // in forest
	SEMountains, // mountains
	SERoom // inside building
};

struct SoundEnvironment
{
	SoundEnvironmentType type;
	float size; // how large (controls mostly echo time)
	float density; // how dense (echo intensity)
};

class QFBank;

class AbstractSoundSystem // whole playing system
{
	public:

	virtual float GetWaveDuration( const char *filename ) = NULL;

	virtual AbstractWave *CreateEmptyWave( float duration ) = NULL; // duration in ms
	
	virtual AbstractWave *CreateWave
	(
		const char *filename, bool is3D=true, bool prealloc=false
	) = NULL;
	//virtual AbstractStream *CreateStream( const char *filename, int lStart, int lEnd ) = NULL;	
	virtual void SetListener
	(
		Vector3Par pos, Vector3Par vel,
		Vector3Par dir, Vector3Par up
	) = NULL;
	virtual Vector3 GetListenerPosition() const = NULL;
	virtual void Commit() = NULL;
	virtual void Activate( bool active ) = NULL;
	virtual void SetEnvironment( const SoundEnvironment &env ) {}
	virtual ~AbstractSoundSystem(){}

	virtual float GetCDVolume() const = NULL;
	virtual void SetCDVolume( float vol ) = NULL;

	virtual void SetWaveVolume( float vol ) = NULL;
	virtual float GetWaveVolume() = NULL;

	virtual void SetSpeechVolume( float vol ) = NULL;
	virtual float GetSpeechVolume() = NULL;

	virtual void StartPreview() = NULL;
	virtual void TerminatePreview() = NULL;

	virtual void FlushBank(QFBank *bank) {}

	// return state after enable
	virtual bool EnableHWAccel(bool val) = NULL;
	virtual bool EnableEAX(bool val) = NULL;
	// get if feature is enabled
	virtual bool GetEAX() const = NULL;
	virtual bool GetHWAccel() const = NULL;

	virtual void LoadConfig() = NULL;
	virtual void SaveConfig() = NULL;
};

extern AbstractSoundSystem *GSoundsys;

const float SpeedOfSound=330;
const float InvSpeedOfSound=1/SpeedOfSound;

#endif


