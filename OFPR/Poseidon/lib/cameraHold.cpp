// Poseidon - camera holders
// (C) 2000, SUMA
#include "wpch.hpp"

#include "cameraHold.hpp"
#include <El/ParamFile/paramFile.hpp>
#include "paramArchive.hpp"
#include "global.hpp"
#include "keyInput.hpp"
#include "camera.hpp"
#include "landscape.hpp"
#include "engine.hpp"
#include "world.hpp"
#include "scene.hpp"
#include "dikCodes.h"
#include "txtPreload.hpp"

DEFINE_CASTING(CameraHolder)

CameraHolder::CameraHolder
(
	LODShapeWithShadow *shape, const VehicleNonAIType *type, int id
)
:base(shape,type,id),
_manual(false),
_camPos(VZero),
_camFov(0.7),_camFovMin(1.5),_camFovMax(0.07),
_camDive(0),_camBank(0),_camHeading(0)
{
}
CameraHolder::~CameraHolder()
{
}

void CameraHolder::Command( RString mode )
{
	// camera dependend special command
	if( !strcmpi(mode,"manual on") )
	{
		SetManual(true);
	}
	else if( !strcmpi(mode,"manual off") )
	{
		SetManual(false);
	}
}

Vector3 CameraTarget::GetPos() const
{
	if( _target ) return _target->CameraPosition();
	else if( _pos.SquareSize()>0.5 ) return _pos;
	else return VZero;
}

Vector3 CameraTarget::PositionAbsToRel( Vector3Val pos ) const
{
	if (!_target)
	{
		return pos - _pos;
	}
	Matrix4 toAbs = _target->ModelToWorld();
	toAbs.SetDirectionAndUp(toAbs.Direction(),VUp);
	// make direction up pointing up
	return toAbs.InverseScaled().FastTransform(pos);
}

Vector3 CameraTarget::PositionRelToAbs( Vector3Val pos ) const
{
	if (!_target)
	{
		return pos + _pos;
	}

	Matrix4 toAbs = _target->ModelToWorld();
	toAbs.SetDirectionAndUp(toAbs.Direction(),VUp);
	// make direction up pointing up
	return toAbs.FastTransform(pos);
}

LSError CameraTarget::Serialize(ParamArchive &ar)
{
	CHECK(ar.SerializeRef("target", _target, 1 ))
	CHECK(ar.Serialize("pos", _pos, 1, VZero ))
	return LSOK;
}	

LSError CameraHolder::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	CHECK(_camTgt.Serialize(ar))
	CHECK(ar.Serialize("camPos", _camPos, 1, VZero ))
	CHECK(ar.Serialize("camFov", _camFov, 1, 0 ))
	CHECK(ar.Serialize("camFovMin", _camFovMin, 1, 0 ))
	CHECK(ar.Serialize("camFovMax", _camFovMax, 1, 0 ))
	CHECK(ar.Serialize("camDive", _camDive, 1, 0 ))
	CHECK(ar.Serialize("camBank", _camBank, 1, 0 ))
	CHECK(ar.Serialize("camHeading", _camHeading, 1, 0 ))
	CHECK(ar.Serialize("manual", _manual, 1, false))
	return LSOK;
}

CameraVehicle::CameraVehicle()
:base(NULL,VehicleTypes.New("Camera"),-1),
_inertia(false), // manual camera simulation 
_crossHairs(true)
{
	 // set all target properties to invalid values
	_movePos = VZero;
	_minFovT = _maxFovT = -1;
	// set all times to infinite future
	_movePosTime=TIME_MAX;
	_minMaxFovTime=TIME_MAX;
	_targetTime = TIME_MAX;
	_oldTargetTime = TIME_MAX;

	// all commited
	_timeCommitted = TIME_MIN;
	 // set all actual properties to default values
	_minFov=0.7,_maxFov=0.7;
	_lastFov=0.7;
	_lastTgtPos=VZero;
}

CameraVehicle::~CameraVehicle()
{
}

	// camera effect parameters
void CameraVehicle::DrawCameraCockpit()
{
	if( !_manual ) return; // nothing to draw
	if( !_crossHairs ) return;

	// check if we are drawing from inside
	// assume yes - we are manual


	{
		// draw circle around locked target
		Color lockColor(1,1,0,1);
		Vector3 lockPos = _target._pos;
		if( _target._target )
		{
			lockColor = Color(1,0,0,1);
			lockPos = _target._target->CameraPosition();
		}
		float ls2 = lockPos.SquareSize();
		if( ls2>0.5f && ls2<1e9f )
		{
			PackedColor color=PackedColor(lockColor);
			float w = GEngine->Width();
			float h = GEngine->Height();

			const Camera &camera = *GScene->GetCamera();
			Matrix4Val camInvTransform=camera.GetInvTransform();

			Point3 pos = camInvTransform.FastTransform(lockPos);
			if (pos.Z() >= 0)
			{
				float invZ = 1.0f / pos.Z();
				float cx = pos.X() * invZ * camera.InvLeft();
				float cy = - pos.Y() * invZ * camera.InvTop();

				float wScreen = toInt(w*0.05f);
				float hScreen = toInt(h*(0.05f*4/3));
				float xScreen = w*cx*0.5f+w*0.5f-wScreen*0.5f;
				float yScreen = h*cy*0.5f+h*0.5f-hScreen*0.5f;
				Texture *texture = GScene->Preloaded(CursorTarget);
				MipInfo mip=GLOB_ENGINE->TextBank()->UseMipmap(texture,0,0);
				GLOB_ENGINE->Draw2D
				(
					mip, PackedBlack,
					Rect2DAbs(xScreen+1, yScreen+1, wScreen, hScreen)
				);
				GLOB_ENGINE->Draw2D
				(
					mip, color,
					Rect2DAbs(xScreen, yScreen, wScreen, hScreen)
				);
			}
		}
	}
	{
		// draw 2D cursor in screen center
		float w = GEngine->Width();
		float h = GEngine->Height();

		float wScreen = toInt(w*0.05f);
		float hScreen = toInt(h*(0.05f*4/3));
		float xScreen = toInt(w*0.5f)-wScreen*0.5f;
		float yScreen = toInt(h*0.5f)-hScreen*0.5f;
		Texture *texture = GScene->Preloaded(CursorAim);
		MipInfo mip=GLOB_ENGINE->TextBank()->UseMipmap(texture,0,0);
		PackedColor color=PackedWhite;
		GLOB_ENGINE->Draw2D
		(
			mip, PackedBlack,
			Rect2DAbs(xScreen+1, yScreen+1, wScreen, hScreen)
		);
		GLOB_ENGINE->Draw2D
		(
			mip, color,
			Rect2DAbs(xScreen, yScreen, wScreen, hScreen)
		);
	}

}

void CameraVehicle::StartFrame()
{
}

Vector3 CameraVehicle::GetTarget() const
{
	if (GetManual())
	{
		return _target.GetPos();
	}
	Vector3 nTgt = _target.GetPos();
	Vector3 oTgt = _oldTarget.GetPos();
	if( oTgt.SquareSize()<0.5 )
	{
		return nTgt;
	}
	if( nTgt.SquareSize()<0.5 )
	{
		return oTgt;
	}
	// both targets - interpolate
	if( Glob.time>=_targetTime )
	{
		return nTgt;
	}
	Vector3 oRel = oTgt-Position();
	Vector3 nRel = nTgt-Position();
	Vector3 oDir = oRel.Normalized();
	Vector3 nDir = nRel.Normalized();
	float oDist = oDir*oRel;
	float nDist = nDir*nRel;

	float ipol = 1;
	if( _targetTime>_oldTargetTime )
	{
		ipol = (Glob.time-_oldTargetTime)/(_targetTime-_oldTargetTime);
	}
	saturate(ipol,0,1);
	// total time 
	// interpolate distance and direction separately
	// TODO: interpolate direction using quaternions
	Vector3 iDir = nDir*ipol + oDir*(1-ipol);
	float iDist = nDist*ipol + oDist*(1-ipol);
	return  iDir*iDist + Position();
}

float CameraVehicle::CamEffectFOV() const
{
	// transition between old and new target

	Vector3 tPos = GetTarget();

	if( tPos.SquareSize()>=0.5 )
	{
		_lastTgtPos=tPos;
	}

	// calculate FOV based on target
	if( _lastTgtPos.SquareSize()>0.5 )
	{
		Vector3 norm = _lastTgtPos-Position();
		Vector3 dir = norm.Normalized();

		const_cast<CameraVehicle *>(this)->SetDirectionAndUp(dir,VUp);

		float invDist=(_lastTgtPos-Position()).InvSize();
		float fov=invDist*10;
		saturate(fov,_minFov,_maxFov);
		_lastFov=fov;
	}
	return _lastFov;
}

void ClipboardSaveText( QOStream &stream );

void CameraVehicle::ResetTargets()
{
	Vector3 tPos = Position()+Direction()*1e5; // virtual infinity
	_target = tPos;
	_oldTarget = NULL;
	_lastTgtPos = _target._pos;
}

#include <sys/types.h>
#include <sys/timeb.h>
#include <time.h>

void CameraVehicle::Simulate( float deltaT, SimulationImportance prec )
{
	if( !GetManual() )
	{
		//_moveTrans=*this;
		if( _movePos.SquareSize()>0.5 )
		{
			if( Glob.time>=_movePosTime )
			{
				Move(_movePos);
				_speed = VZero;
			}
			else
			{
				// smooth interpolation
				// control speed so that you are in position in time
				Vector3 offset = _movePos - Position();
				_speed = offset * (1/ (_movePosTime-Glob.time));

				Move(Position()+_speed*deltaT);

			}
		}
		if( _minFovT>=0 )
		{
			// interpolate minFov
			if( Glob.time<_minMaxFovTime )
			{
				float invTime = deltaT/(_minMaxFovTime-Glob.time);
				_minFov += (_minFovT-_minFov)*invTime;
				_maxFov += (_maxFovT-_maxFov)*invTime;
			}
			else
			{
				_minFov = _minFovT;
				_maxFov = _maxFovT;
			}
		}

		//_integrationDone=true;
	}
	else
	{
		if (_target.GetPos().SquareSize()<0.5)
		{
			ResetTargets();			
		}

		deltaT /= GWorld->GetAcceleratedTime();
		// manual controls
		// use basic controls to control movement

		float forward=
		(
			GInput.keyMoveForward*4+
			GInput.keyMoveFastForward*16+
			GInput.keyMoveBack*-4
		);

		float aside =
		(
			(GInput.keyMoveRight-GInput.keyMoveLeft)*16+
			(GInput.keyTurnRight-GInput.keyTurnLeft)*4
		);
		float up = (GInput.keyMoveUp-GInput.keyMoveDown)*4;

		const float ZoomSpeed=4;

		float expChange=GInput.keys[DIK_NUMPADMINUS]-GInput.keys[DIK_NUMPADPLUS];
		float change=pow(ZoomSpeed,expChange*deltaT);
		_lastFov*=change;
		saturate(_lastFov,0.01,2.0);
		_minFov = _maxFov = _lastFov; // force manual FOV

		// turn to follow camera direction
		// (i.e follow weapon)

		Matrix3 speedOrient;
		speedOrient.SetUpAndDirection( VUp, Direction() );

		Vector3 speedWanted = speedOrient * Vector3(aside,up,forward);
		if( _inertia )
		{
			// smooth changes
			speedWanted *=4;

			Vector3 accel = speedWanted-_speed;
			saturate(accel[0],-5,+5);
			saturate(accel[1],-5,+5);
			saturate(accel[2],-10,+10);
			_speed += accel*deltaT;
		}
		else
		{
			_speed = speedWanted;
		}

		Vector3 position = Position()+_speed*deltaT;
		float surfY = GLandscape->SurfaceYAboveWater(position[0],position[2]);
		saturateMax(position[1],surfY+0.05);
		Move(position);

		// get mouse movement
		// or numeric keyboard

		float headSpeed=GInput.keys[DIK_NUMPAD4]-GInput.keys[DIK_NUMPAD6];
		float diveSpeed=GInput.keys[DIK_NUMPAD2]-GInput.keys[DIK_NUMPAD8];
		float headChange=headSpeed*2*deltaT*_lastFov;
		float diveChange=diveSpeed*2*deltaT*_lastFov;
		Matrix3 orient=Orientation();
		if( headChange )
		{
			Matrix3 rotY(MRotationY,headChange);
			orient=rotY*orient;
		}
		if( diveChange )
		{
			Matrix3 rotX(MRotationX,diveChange);
			// neutralize heading
			Matrix3 head,invHead;
			head.SetDirectionAndUp(Direction(),VUp);
			invHead=head.InverseRotation();
			orient=head*rotX*invHead*orient;
			ResetTargets();
		}
		SetOrientation(orient);

		if( headChange || diveChange )
		{
			ResetTargets();
		}

		if( GInput.keys[DIK_NUMPAD5] )
		{
			// unlock camera
			ResetTargets();
		} // fire
		if
		(
			GInput.GetActionToDo(UAToggleWeapons) ||
			GInput.keysToDo[DIK_DIVIDE]
		)
		{
			// lock camera
			// shoot probe ray
			// if nothing is hit, fix at ground point
			// try to find some lock
			CollisionBuffer retVal;
			GLandscape->ObjectCollision
			(
				retVal,this,NULL,Position(),Position()+Direction()*500,2,ObjIntersectGeom
			);
			// find nearest intersection
			Object *minObj = NULL;
			float minDist2 = 1e10;
			for( int i=0; i<retVal.Size(); i++ )
			{
				Object *obj = retVal[0].object;
				if( !dyn_cast<Vehicle>(obj) ) continue;
				float dist2 = obj->Position().Distance2(Position());
				if (dist2<minDist2)
				{
					minDist2 = dist2;
					minObj = obj;
				}
			}
			if( minObj )
			{
				_target = minObj;
			}
			else
			{
				Vector3 land = GLandscape->IntersectWithGroundOrSea(Position(),Direction());
				_target = land;
			}
		} // selectweapon
		if (GInput.keysToDo[DIK_DELETE])
		{
			_inertia = !_inertia;
		}
		if (GInput.GetActionToDo(UAHeadlights))
		{
			_crossHairs = !_crossHairs;
		}
		if( GInput.keysToDo[DIK_V] )
		{
			SetDelete(); // vehicle should be removed
			GInput.keysToDo[DIK_V]=false;
		}
		if (GInput.GetActionToDo(UAFire))
		{
			// save text (preferably to clipboard)
			char buf[1024];
			QOFStream f;
			const char *myName = "_camera"; // TODO: check symbolic name
			
			{
				time_t tb;
				time(&tb);
				struct tm *lt = localtime(&tb);
				
				sprintf(buf,";=== %d:%02d:%02d\r\n",lt->tm_hour,lt->tm_min,lt->tm_sec);
				f.write(buf,strlen(buf));
			}
			
			if( _target._target )
			{
				// TODO: get symbolic name (expression)
				RString aiName = _target._target->GetVarName();
				if (aiName.GetLength()>0)
				{
					sprintf(buf,"%s camSetTarget %s\r\n",myName,(const char *)aiName);
				}
				else
				{
					RString symName = _target._target->GetDebugName();
					sprintf(buf,"%s camSetTarget '%s'\r\n",myName,(const char *)symName);
				}
				f.write(buf,strlen(buf));
			}
			else
			{
				if( _target._pos.SquareSize()>0.5 )
				{
					float surfY = GLandscape->SurfaceYAboveWater(_target._pos[0],_target._pos[2]);
					sprintf
					(
						buf,"%s camSetTarget [%.2f,%.2f,%.2f]\r\n",myName,
						_target._pos[0],_target._pos[2],_target._pos[1]-surfY
					);
					f.write(buf,strlen(buf));
				}
			}
			if (_target._target && GInput.keys[DIK_LSHIFT] )
			{
				Vector3 relPos = _target.PositionAbsToRel(Position());
				sprintf
				(
					buf,"%s camSetRelPos [%.2f,%.2f,%.2f]\r\n",myName,
					relPos[0],relPos[2],relPos[1]
				);
				f.write(buf,strlen(buf));
			}
			else
			{
				float surfY = GLandscape->SurfaceYAboveWater(Position()[0],Position()[2]);
				sprintf
				(
					buf,"%s camSetPos [%.2f,%.2f,%.2f]\r\n",myName,
					Position()[0],Position()[2],Position()[1]-surfY
				);
				f.write(buf,strlen(buf));
			}
			{
				sprintf
				(
					buf,"%s camSetFOV %.3f\r\n",myName,_lastFov
				);
				f.write(buf,strlen(buf));
			}

			{
				sprintf
				(
					buf,"%s camCommit 0\r\n",myName
				);
				f.write(buf,strlen(buf));
			}

			{
				sprintf
				(
					buf,"@camCommitted _camera\r\n"
				);
				f.write(buf,strlen(buf));
			}
			

			f.export_clip("clipboard.txt");
		} // fire - save parameters
	}
}

void CameraVehicle::Command( RString mode )
{
	// camera dependend special command
	if (!strcmpi(mode,"inertia on") )
	{
		_inertia=true;
	}
	else if (!strcmpi(mode,"inertia off") )
	{
		_inertia=false;
	}
	else
	{
		base::Command(mode);
	}
}

void CameraVehicle::Commit( float time )
{
	Time cTime = Glob.time+time;
	// commit all settings
	if( _set._camPos )
	{
		_movePos = _camPos;
		_movePosTime = cTime;
		_set._camPos = false;
	}
	if( _set._camTgt )
	{
		_oldTarget = _target;
		_target = _camTgt;
		_targetTime = cTime;
		_oldTargetTime = Glob.time;
		_set._camTgt = false;
	}
	if( _set._camFovMinMax )
	{
		_minFovT = _camFovMin;
		_maxFovT = _camFovMax;
		_minMaxFovTime = cTime;
	}

	if( cTime>_timeCommitted ) _timeCommitted = cTime;
	if (time<=0)
	{
		// commit immediatelly
		Simulate(0,SimulateVisibleNear);
	}
}

bool CameraVehicle::GetCommited() const
{
	// check if all times have been reached
	return Glob.time>=_timeCommitted;
}

