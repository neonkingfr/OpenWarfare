// Poseidon - vehicle class
// (C) 1997, Suma
#include "wpch.hpp"

#include "tank.hpp"
#include "ai.hpp"

#include "shots.hpp"
#include "scene.hpp"
#include "camera.hpp"
#include "engine.hpp"
#include "world.hpp"
#include "landscape.hpp"
#include "keyInput.hpp"
#include "diagModes.hpp"

#include <El/Common/randomGen.hpp>

#include <El/ParamFile/paramFile.hpp>

#include <El/Common/perfLog.hpp>
#include "perfProf.hpp"

#include "network.hpp"
#include "operMap.hpp"

#include "SpecLods.hpp"

#define ARROWS 0

TankType::TankType( const ParamEntry *param )
:base(param)
{
	_scopeLevel=1;

	// no dropdown for a cockpit
	_gunnerPilotPos=VZero;

}

void TankType::Load(const ParamEntry &par)
{
	base::Load(par);

	_mainTurret.Load(par>>"Turret");
	_comTurret.Load(par>>"ComTurret");
}

void TankType::InitShape()
{
	_scopeLevel=2;
	base::InitShape();

	const ParamEntry &par=*_par;

	// turret animations
	_mainTurret.InitShape(par>>"Turret",_shape);
	_comTurret.InitShape(par>>"ComTurret",_shape);

	_comTurretOnMainTurret = true;
	{
		// check if commander turret is part of main turret
		// check in memory
		int level = _shape->FindMemoryLevel();
		Shape *shape = _shape->MemoryLevel();

		int mainBody = _mainTurret.GetBodySelection(level);
		int comBody = _comTurret.GetBodySelection(level);
		if (mainBody>=0 && comBody>=0)
		{
			const NamedSelection &mBody = shape->NamedSel(mainBody);
			const NamedSelection &cBody = shape->NamedSel(comBody);
			if (!mBody.IsSubset(cBody))
			{
				//LogF("%s Observer turret separate",(const char *)GetName());
				_comTurretOnMainTurret = false;
			}
		}
	}

	_radarIndicator.Init(_shape, par >> "IndicatorRadar");
	_turretIndicator.Init(_shape, par >> "IndicatorTurret");
	_watch.Init(_shape, par >> "IndicatorWatch");

	// track animations
	_leftOffset.Init(_shape,"PasOffsetP");
	_rightOffset.Init(_shape,"PasOffsetL");

	_hatchDriver.Init(_shape, par >> "HatchDriver");
	_hatchCommander.Init(_shape, par >> "HatchCommander");
	_hatchGunner.Init(_shape, par >> "HatchGunner");

	_animFire.Init(_shape, "zasleh", NULL);

	// weapon directions and positions
	Point3 beg,end;
	beg=_shape->MemoryPoint("spice rakety","usti hlavne");
	end=_shape->MemoryPoint("konec rakety","konec hlavne");
	_missilePos=(beg+end)*0.5f;
	_missileDir=(beg-end);
	_missileDir.Normalize();

	_gunDir = _mainTurret._dir;
	_gunPos = _shape->MemoryPoint("kulas");
	if( _gunPos.SquareSize()<0.1f ) _gunPos = _mainTurret._pos;

	int level;
	level=_shape->FindLevel(VIEW_GUNNER);
	if( level>=0 )
	{
		Shape *cockpit=_shape->LevelOpaque(level);
		cockpit->MakeCockpit();
		if( _gunnerPilotPos.SquareSize()<=1e-6 )
		{
			_gunnerPilotPos=cockpit->NamedPosition("pilot");
		}
	}

	_cargoLightPos = VZero;
	Shape *memory = _shape->MemoryLevel();
	if (memory)
	{
		_cargoLightPos = memory->NamedPosition("cargo light");
	}

	DEF_HIT_CFG(_shape,_hullHit,par>>"HitHull",GetArmor());
	DEF_HIT_CFG(_shape,_turretHit,par>>"HitTurret",GetArmor());
	DEF_HIT_CFG(_shape,_gunHit,par>>"HitGun",GetArmor());

	DEF_HIT_CFG(_shape,_trackLHit,par>>"HitLTrack",GetArmor());
	DEF_HIT_CFG(_shape,_trackRHit,par>>"HitRTrack",GetArmor());

	//DEF_HIT(_shape,_hullHit,"hull",NULL,GetArmor()*(float)(par>>"armorHull"));
	//DEF_HIT(_shape,_turretHit,"turet",NULL,GetArmor()*(float)(par>>"armorTurret"));
	//DEF_HIT(_shape,_gunHit,"gun",NULL,GetArmor()*(float)(par>>"armorGun"));
	//DEF_HIT(_shape,_trackLHit,"pasL",NULL,GetArmor()*(float)(par>>"armorTracks"));
	//DEF_HIT(_shape,_trackRHit,"pasP",NULL,GetArmor()*(float)(par>>"armorTracks"));

	// scan wheels - load from cfg
	const ParamEntry &wheels = par>>"Wheels";
	const ParamEntry &rotLWheels = wheels>>"rotL";
	const ParamEntry &rotRWheels = wheels>>"rotR";
	const ParamEntry &upDownRWheels = wheels>>"upDownR";
	const ParamEntry &upDownLWheels = wheels>>"upDownL";

	_wheelsRotL.Clear();
	_wheelsRotR.Clear();
	_wheelsUpDownL.Clear();
	_wheelsUpDownR.Clear();
	_tracksUpDownL.Clear();
	_tracksUpDownR.Clear();

	_wheelsRotL.Realloc(rotLWheels.GetSize());
	for (int i=0; i<rotLWheels.GetSize(); i++)
	{
		RStringB wheel = rotLWheels[i];
		AnimationWithCenter &anim = _wheelsRotL.Append();
		anim.Init(_shape,wheel,NULL,NULL);
		// check if Animation is non-empty in some LOD
		if (anim.IsEmpty()) _wheelsRotL.Delete(_wheelsRotL.Size()-1);
	}
	_wheelsRotR.Compact();

	_wheelsRotR.Realloc(rotRWheels.GetSize());
	for (int i=0; i<rotRWheels.GetSize(); i++)
	{
		RStringB wheel = rotRWheels[i];
		AnimationWithCenter &anim = _wheelsRotR.Append();
		anim.Init(_shape,wheel,NULL,NULL);
		// check if Animation is non-empty in some LOD
		if (anim.IsEmpty()) _wheelsRotR.Delete(_wheelsRotR.Size()-1);
	}
	_wheelsRotL.Compact();

	_wheelsUpDownL.Realloc(upDownLWheels.GetSize()/2);
	_tracksUpDownL.Realloc(upDownLWheels.GetSize()/2);
	for (int i=0; i<upDownLWheels.GetSize()-1; i+=2)
	{
		RStringB wheel = upDownLWheels[i];
		RStringB track = upDownLWheels[i+1];
		AnimationWithCenter &animW = _wheelsUpDownL.Append();
		AnimationWithCenter &animT = _tracksUpDownL.Append();
		animW.Init(_shape,wheel,NULL,NULL);
		animT.Init(_shape,track,NULL,NULL);
		// check if Animation is non-empty in some LOD
		if (animW.IsEmpty()!=animT.IsEmpty())
		{
			RptF
			(
				"Model %s: selections not corresponding: %s<->%s",
				_shape->Name(),(const char *)wheel,(const char *)track
			);
		}
		if (animW.IsEmpty()) _wheelsUpDownL.Delete(_wheelsUpDownL.Size()-1);
		if (animT.IsEmpty()) _tracksUpDownL.Delete(_tracksUpDownL.Size()-1);
	}
	_wheelsUpDownL.Compact();
	_tracksUpDownL.Compact();

	_wheelsUpDownR.Realloc(upDownRWheels.GetSize()/2);
	_tracksUpDownR.Realloc(upDownRWheels.GetSize()/2);
	for (int i=0; i<upDownRWheels.GetSize()-1; i+=2)
	{
		RStringB wheel = upDownRWheels[i];
		RStringB track = upDownRWheels[i+1];
		AnimationWithCenter &animW = _wheelsUpDownR.Append();
		AnimationWithCenter &animT = _tracksUpDownR.Append();
		animW.Init(_shape,wheel,NULL,NULL);
		animT.Init(_shape,track,NULL,NULL);
		// check if Animation is non-empty in some LOD
		if (animW.IsEmpty()!=animT.IsEmpty())
		{
			RptF
			(
				"Model %s: selections not corresponding: %s<->%s",
				_shape->Name(),(const char *)wheel,(const char *)track
			);
		}
		if (animW.IsEmpty()) _wheelsUpDownR.Delete(_wheelsUpDownR.Size()-1);
		if (animT.IsEmpty()) _tracksUpDownR.Delete(_tracksUpDownR.Size()-1);

	}
	_wheelsUpDownR.Compact();
	_tracksUpDownR.Compact();
}

TurretType::TurretType()
{
}

void TurretType::Load(const ParamEntry &cfg)
{
	_minElev=(float)(cfg>>"minElev")*(H_PI/180);
	_maxElev=(float)(cfg>>"maxElev")*(H_PI/180);
	_minTurn=(float)(cfg>>"minTurn")*(H_PI/180);
	_maxTurn=(float)(cfg>>"maxTurn")*(H_PI/180);
	//LogF("%s: elev %g..%g",(const char *)cfg.GetContext(),_minElev,_maxElev);
	GetValue(_servoSound, cfg >> "soundServo");
}

void TurretType::InitShape(const ParamEntry &cfg, LODShape *shape)
{
	// get selection names
	RStringB gunAxis = cfg>>"gunAxis"; //"OsaHlavne";
	RStringB turretAxis = cfg>>"turretAxis"; //"OsaVeze";

	RStringB gunBeg = cfg>>"gunBeg"; // "usti hlavne"
	RStringB gunEnd = cfg>>"gunEnd"; // "konec hlavne"

	Shape *memory = shape->MemoryLevel();
	if (memory)
	{
		_yAxisIndex = memory->PointIndex(turretAxis);
		_xAxisIndex = memory->PointIndex(gunAxis);
		/*
		if (_yAxisIndex>=0)
		{
			LogF("_yAxisIndex %s: %d",(const char *)turretAxis,_yAxisIndex);
			for (int i=0; i<memory->NNamedSel(); i++)
			{
				if (memory->NamedSel(i).IsSelected(_yAxisIndex))
				{
					LogF("  in sel %s",memory->NamedSel(i).Name());
				}
			}
		}
		*/
		if (_xAxisIndex<0) _xAxisIndex = _yAxisIndex;
		/*
		else
		{
			LogF("_xAxisIndex %s: %d",(const char *)gunAxis,_xAxisIndex);
			for (int i=0; i<memory->NNamedSel(); i++)
			{
				if (memory->NamedSel(i).IsSelected(_xAxisIndex))
				{
					LogF("  in sel %s",memory->NamedSel(i).Name());
				}
			}
		}
		*/
	}
	else
	{
		_yAxisIndex = -1;
		_xAxisIndex = -1;
	}

	_yAxis=shape->MemoryPoint(turretAxis);
	if (shape->MemoryPointExists(gunAxis))
	{
		_xAxis=shape->MemoryPoint(gunAxis);
	}
	else
	{
		_xAxis = _yAxis;
	}


	if (shape->MemoryPointExists(gunBeg))
	{
		Vector3Val beg = shape->MemoryPoint(gunBeg);
		Vector3Val end = shape->MemoryPoint(gunEnd);
		_pos = (beg+end)*0.5f;
		_dir = (beg-end);
		_dir.Normalize();
	}
	else
	{
		_pos = VZero;
		_dir = VForward;
	}

	_neutralXRot = atan2(_dir.Y(),_dir.SizeXZ());
	//_neutralYRot = -atan2(_dir.X(),_dir.Z());
	_neutralYRot = 0;

	RString bodyName = cfg>>"body";
	RString gunName = cfg>>"gun";

	_body.Init(shape,bodyName,NULL);
	_gun.Init(shape,gunName,NULL);
}

Turret::Turret()
:_yRot(0),_yRotWanted(0),
_xRot(0),_xRotWanted(0),
_xSpeed(0),_ySpeed(0),
_servoVol(0),
_gunStabilized(true)
{
}

LSError Turret::Serialize(ParamArchive &ar)
{
	SerializeBitBool(ar, "gunStabilized", _gunStabilized, 1, false)
	CHECK(ar.Serialize("yRot", _yRot, 1, 0))
	CHECK(ar.Serialize("xRot", _xRot, 1, 0))
	CHECK(ar.Serialize("yRotWanted", _yRotWanted, 1, 0))
	CHECK(ar.Serialize("xRotWanted", _xRotWanted, 1, 0))
	CHECK(ar.Serialize("xSpeed", _xSpeed, 1, 0))
	CHECK(ar.Serialize("ySpeed", _ySpeed, 1, 0))

	return LSOK;
}

void Turret::Sound
(
	const TurretType &type, bool inside, float deltaT,
	FrameBase &pos, Vector3Val speed // parent position
)
{
	if( _servoVol>0.001f )
	{
		const SoundPars &pars=type._servoSound;
		if( !_servoSound && pars.name.GetLength()>0 )
		{
			_servoSound=GSoundScene->OpenAndPlay
			(
				pars.name,pos.Position(),speed
			);
		}
		if( _servoSound )
		{
			float vol=pars.vol*_servoVol;
			float freq=pars.freq;
			_servoSound->SetVolume(vol,freq); // volume, frequency
			_servoSound->Set3D(!inside);
			_servoSound->SetPosition(pos.Position(),speed);
		}
	}
	else
	{
		_servoSound.Free();
	}
}

void Turret::UnloadSound()
{
	_servoSound.Free();
}

void Turret::Animate
(
	const TurretType &type, const Object *obj, int level
)
{
	int gunSel = type._gun.GetSelection(level);
	int bodySel = type._body.GetSelection(level);
	// check if there is something to animate
	if (bodySel<0 && gunSel<0) return;

	LODShape *lShape = obj->GetShape();
	Shape *shape = lShape->Level(level);

	if (bodySel>=0 && shape->NamedSel(bodySel).Size()>0)
	{
		Matrix4 mat = MIdentity;
		obj->AnimateMatrix(mat,level,bodySel);
		type._body.Transform(lShape,mat,level);
	}
	if (gunSel>=0 && shape->NamedSel(gunSel).Size()>0)
	{
		Matrix4 mat = MIdentity;
		obj->AnimateMatrix(mat,level,gunSel);
		//AnimateMatrix(type,mat,obj,level,gunSel);
		type._gun.Transform(lShape,mat,level);
	}
}

void Turret::Deanimate(const TurretType &type, LODShape *shape, int level)
{
	if (type._body.GetSelection(level)>=0)
	{
		type._body.Restore(shape,level);
	}
	if (type._gun.GetSelection(level)>=0)
	{
		type._gun.Restore(shape,level);
	}
}

Matrix3 Turret::GetAimWanted() const
{
	return
	(
		Matrix3(MRotationY,_yRotWanted)*Matrix3(MRotationX,-_xRotWanted)
	);
}

bool Turret::Aim(const TurretType &type, Vector3Val relDir)
{
	
	_yRotWanted=AngleDifference(-atan2(relDir.X(),relDir.Z()),type._neutralYRot);
	//float neutralXRot=atan2(_missileDir.Y(),_missileDir.Z());
	float sizeXZ=relDir.SizeXZ();
	_xRotWanted=atan2(relDir.Y(),sizeXZ)-type._neutralXRot;
	
	if (type._maxTurn-type._minTurn<H_PI*2)
	{
		// if turning is limited, saturate around turning midpoint
		float midTurn = (type._maxTurn+type._minTurn)*0.5f;
		_yRotWanted = AngleDifference(_yRotWanted,midTurn)+midTurn;
	}
	else
	{
		_yRotWanted=AngleDifference(_yRotWanted,0);
	}

	#if 0
	GlobalShowMessage
	(
		100,"y %.2f->%.2f, %.2f..%.2f",
		_yRot,_yRotWanted,
		type._minTurn,type._maxTurn
	);
	#endif
	Limit(_xRotWanted,type._minElev,type._maxElev);
	Limit(_yRotWanted,type._minTurn,type._maxTurn);
	float xToAim=fabs(_xRotWanted-_xRot);
	float yToAim=fabs(_yRotWanted-_yRot);
	if( xToAim+yToAim>1e-6 ) return true; // enable simulation
	return false;
}

/*!
\patch 1.12 Date 8/7/2001 by Ondra.
- Changed: Turret response to aiming quicker.
\patch 1.31 Date 11/20/2001 by Ondra.
- Changed: Turret overaiming fixed.
*/

void Turret::MoveWeapons(const TurretType &type, AIUnit *unit, float deltaT )
{
	float maxSpeed=0;
	float ability = unit->GetAbility();
	float delta;
	float speed;

	speed=(_xRotWanted-_xRot)*4;
	saturateMax(maxSpeed,fabs(speed));
	delta=speed-_xSpeed;
	Limit(delta,-1*deltaT,+1*deltaT);
	_xSpeed+=delta;
	Limit(_xSpeed,-0.3f*ability,0.3f*ability);
	_xRot+=_xSpeed*deltaT;

	speed=AngleDifference(_yRotWanted,_yRot)*4;
	saturateMax(maxSpeed,fabs(speed));
	delta=speed-_ySpeed;
	Limit(delta,-3*deltaT,+3*deltaT);
	_ySpeed+=delta;
	Limit(_ySpeed,-1.2f*ability,+1.2f*ability);
	_yRot+=_ySpeed*deltaT;
	if (type._maxTurn-type._minTurn<H_PI*2)
	{
		// if turning is limited, saturate around turning midpoint
		float midTurn = (type._maxTurn+type._minTurn)*0.5f;
		_yRot = AngleDifference(_yRot,midTurn)+midTurn;
	}
	else
	{
		_yRot=AngleDifference(_yRot,0);
	}

	Limit(_xRot,type._minElev,type._maxElev);
	Limit(_yRot,type._minTurn,type._maxTurn);


	//_servoVol=floatMin(1,maxSpeed*4);
	float servoVolWanted=0;
	if( maxSpeed>0.01f ) servoVolWanted=1;
	delta=servoVolWanted-_servoVol;
	Limit(delta,-2*deltaT,+2*deltaT);
	_servoVol+=delta;

}

Matrix4 Turret::TurretTransform(const TurretType &type) const
{
	//Vector3 yAxis = type.GetYAxis(obj);
	Vector3 yAxis = type._yAxis;
	return
	(
		Matrix4(MTranslation,yAxis)*
		Matrix4(MRotationY,_yRot)*
		Matrix4(MTranslation,-yAxis)
	);
}

Matrix4 Turret::GunTransform(const TurretType &type) const
{
	//Vector3 xAxis = type.GetXAxis(obj);
	Vector3 xAxis = type._xAxis;
	return
	(
		Matrix4(MTranslation,xAxis)*
		Matrix4(MRotationX,-_xRot)*
		Matrix4(MTranslation,-xAxis)
	);
}

Vector3 Turret::GetCenter(const TurretType &type) const
{
	const Vector3 &yAxis=type._yAxis; // rotate around this point
	const Vector3 &xAxis=type._xAxis; // rotate around this point
	return Vector3(yAxis[0],xAxis[1],yAxis[2]);
}

void Turret::AnimatePoint
(
	const TurretType &type, Vector3 &pos,
	const Object *obj, int level, int index
) const
{
	Shape *shape = obj->GetShape()->Level(level);
	int selI = type._gun.GetSelection(level);
	if (selI>=0)
	{
		const NamedSelection &sel = shape->NamedSel(selI);
		if (sel.IsSelected(index))
		{
			// apply gun transformation
			Matrix4Val gunTransform = GunTransform(type);
			pos = gunTransform.FastTransform(pos);
		}
	}

	selI = type._body.GetSelection(level);
	if (selI>=0)
	{
		const NamedSelection &sel = shape->NamedSel(selI);
		if (sel.IsSelected(index))
		{
			// apply turret transformation
			Matrix4Val turTransform = TurretTransform(type);
			pos = turTransform.FastTransform(pos);
		}
	}
}

void Turret::AnimateMatrix
(
	const TurretType &type, Matrix4 &mat,
	const Object *obj, int level, int selection
) const
{
	if (selection<0) return;
	Shape *shape = obj->GetShape()->Level(level);
	const NamedSelection &sel = shape->NamedSel(selection);

	int selI = type._gun.GetSelection(level);
	if (selI>=0)
	{
		// check if sel is whole contained in particular selection
		const NamedSelection &tSel = shape->NamedSel(selI);
		if (tSel.IsSubset(sel))
		{
			// apply turret animation to this proxy
			Matrix4Val transform = GunTransform(type);
			mat = transform * mat;
		}
	}
	selI = type._body.GetSelection(level);
	if (selI>=0)
	{
		// check if sel is whole contained in particular selection
		const NamedSelection &tSel = shape->NamedSel(selI);
		if (tSel.IsSubset(sel))
		{
			// apply turret animation to this proxy
			Matrix4Val transform = TurretTransform(type);
			mat = transform * mat;
			
		}
	}
}

/*!
\patch 1.24 Date 9/24/2001 by Ondra.
- Fixed: Improved turret stabilization, especially in Multiplayer.
\patch_internal 1.24 Date 9/24/2001 by Ondra.
- Fixed: turret stabilization code is correct for even for bigger angles.
*/

void Turret::Stabilize
(
	const Object *obj,
	const TurretType &type, Matrix3Val oldTrans, Matrix3Val newTrans
)
{
	if( _gunStabilized && type._yAxisIndex>=0)
	{
		Vector3Val gunDir=TurretTransform(type).Rotate(GunTransform(type).Direction());

		// move with _xRot, _yRot so that newDir is equal to oldDir

		// adjust newDir to be equal to oldDir

		Matrix3 invTrans = newTrans.InverseRotation();


		// assume oldDirection and newDirection are very near

		Vector3 gunDirNew = invTrans * (oldTrans * gunDir);

		float yRotNew = -atan2(gunDirNew.X(),gunDirNew.Z());
		float xRotNew = atan2(gunDirNew.Y(),gunDirNew.SizeXZ());
		_yRot += AngleDifference(yRotNew,_yRot);
		_xRot += AngleDifference(xRotNew,_xRot);
		//_yRot += AngleDifference(gunDirNew.X(),gunDir.X());
		
		//_xRot -= newDirY-oldDirY;
		//_yRot -= ( invTrans * oldTrans.Direction()).X();

		Limit(_xRot,type._minElev,type._maxElev);
		Limit(_yRot,type._minTurn,type._maxTurn);

	}
}

void Turret::Stop( const TurretType &type)
{
	_gunStabilized=false;
	_servoVol=0;
	_xSpeed=0;
	_ySpeed=0;
}

void Turret::GunBroken(const TurretType &type)
{
	_xRotWanted=type._minElev;
	_gunStabilized=false;
}

void Turret::TurretBroken(const TurretType &type)
{
	_yRotWanted=_yRot; // no rotation
	_gunStabilized=false;
}

class IndicesUpdateTurret : public NetworkMessageIndices
{
public:
	int gunStabilized;
	int yRotWanted;
	int xRotWanted;

	IndicesUpdateTurret();
	NetworkMessageIndices *Clone() const {return new IndicesUpdateTurret;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesUpdateTurret::IndicesUpdateTurret()
{
	gunStabilized = -1;
	yRotWanted = -1;
	xRotWanted = -1;
}

void IndicesUpdateTurret::Scan(NetworkMessageFormatBase *format)
{
	SCAN(gunStabilized)
	SCAN(yRotWanted)
	SCAN(xRotWanted)
}

NetworkMessageIndices *GetIndicesUpdateTurret() {return new IndicesUpdateTurret();}

NetworkMessageFormat &Turret::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	format.Add("gunStabilized", NDTBool, NCTNone, DEFVALUE(bool, true), DOC_MSG("Gun is stabilized"), ET_NOT_EQUAL, ERR_COEF_MODE);
	format.Add("yRotWanted", NDTFloat, NCTFloatAngle, DEFVALUE(float, 0), DOC_MSG("Wanted rotation in y axis"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);
	format.Add("xRotWanted", NDTFloat, NCTFloatAngle, DEFVALUE(float, 0), DOC_MSG("Wanted rotation in x axis"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);
	return format;
}

TMError Turret::TransferMsg(NetworkMessageContext &ctx)
{
	Assert(dynamic_cast<const IndicesUpdateTurret *>(ctx.GetIndices()))
	const IndicesUpdateTurret *indices = static_cast<const IndicesUpdateTurret *>(ctx.GetIndices());

	ITRANSF(gunStabilized)
	ITRANSF(yRotWanted)
	ITRANSF(xRotWanted)
	return TMOK;
}

float Turret::CalculateError(NetworkMessageContext &ctx)
{
	float error = 0;

	Assert(dynamic_cast<const IndicesUpdateTurret *>(ctx.GetIndices()))
	const IndicesUpdateTurret *indices = static_cast<const IndicesUpdateTurret *>(ctx.GetIndices());

	ICALCERR_NEQ(bool, gunStabilized, ERR_COEF_MODE)
	ICALCERR_ABSDIF(float, yRotWanted, ERR_COEF_VALUE_MAJOR)
	ICALCERR_ABSDIF(float, xRotWanted, ERR_COEF_VALUE_MAJOR)
	return error;
}

Hatch::Hatch()
{
	_openAngle = 0;
}

void Hatch::Init(LODShape *shape, const ParamEntry &par)
{
	RString selection = par >> "selection";
	RString axis = par >> "axis";
	_animation.Init(shape, selection, NULL, axis, NULL);
	_openAngle = HDegree(par >> "angle");
}

void Hatch::Open(Matrix4Par parent, LODShape *shape, int level, float value) const
{
	if (_animation.GetSelection(level)<0) return;
	float angle = value * _openAngle;
	Matrix4 rot;
	_animation.GetRotation(rot,angle,level);

	rot = parent * rot;
	_animation.Transform(shape, rot, level);

	//_animation.TransformOver(shape, rot, level);
}

void Hatch::Restore(LODShape *shape, int level) const
{
	_animation.Restore(shape, level);
}

Tank::Tank( VehicleType *name, Person *driver )
:base(name,driver),

// pilot controls
_thrustL(0),_thrustR(0),
_thrustLWanted(0),_thrustRWanted(0),

_randFrequency(1-GRandGen.RandomValue()*0.05f), // do not use same sound frequency

// turret controls

_track(_shape),
// different variables
_phaseL(0),_phaseR(0),

_fireDustTimeLeft(0),
_invFireDustTimeTotal(1),

_backwardUsedAsBrake(false),
_forwardUsedAsBrake(false),
_parkingBrake(false)
{
	_rpm=0.1f,_rpmWanted=0.1f;
	// init gear box
	AutoArray<float> gears;
	gears.Add(0);
	gears.Add(1.0f/7);
	gears.Add(1.0f/10);
	gears.Add(1.0f/14);
	gears.Add(1.0f/19);
	_gearBox.SetGears(gears);

	_mGunClouds.Load((*Type()->_par) >> "MGunClouds");

	_mGunFireFrames = 0;
	_mGunFireTime = UITIME_MIN;
	_mGunFirePhase = 0;

	_gunClouds.Load((*Type()->_par) >> "GunClouds");
	_gunFire.Load((*Type()->_par) >> "GunFire");

	_fireDust.SetSize(1,2);

	_head.SetPars("Land");
	_head.Init(Type()->_pilotPos-Vector3(0,0.2f,0),Type()->_pilotPos,this);
}

float Tank::GetEngineVol( float &freq ) const
{
	freq=_randFrequency*_rpm*1.2f;
	return (fabs(_thrustL)+fabs(_thrustR))*0.25f+0.5f;
}

float Tank::GetEnvironVol( float &freq ) const
{
	freq=1;
	return _speed.SquareSize()/Square(Type()->GetMaxSpeedMs());
}

void Tank::PerformFF( FFEffects &effects )
{
	base::PerformFF(effects);
}

float Tank::GetHitForDisplay(int kind) const
{
	// see InGameUI::DrawTankDirection
	switch (kind)
	{
		case 0: return GetHitCont(Type()->_hullHit);
		case 1: return GetHitCont(Type()->_engineHit);
		case 2: return GetHitCont(Type()->_trackLHit);
		case 3: return GetHitCont(Type()->_trackRHit);
		case 4: return GetHitCont(Type()->_turretHit);
		case 5: return GetHitCont(Type()->_gunHit);
		default: return 0;
	}
}

void Tank::Sound( bool inside, float deltaT )
{
	if( _doGearSound && !_gearSound )
	{
		_doGearSound=false;
		AbstractWave *sound=GSoundScene->OpenAndPlayOnce
		(
			Type()->_gearSound.name,Position(),Speed()
		);
		_gearSound=sound;
		if (sound)
		{
			GSoundScene->SimulateSpeedOfSound(sound);
			GSoundScene->AddSound(sound);
		}
	}
	if( _gearSound )
	{
		float gearVol=Type()->_gearSound.vol;
		if( inside ) gearVol*=0.2f;
		_gearSound->SetVolume(gearVol); // volume, frequency
		_gearSound->SetPosition(Position(),Speed());
	}

	_mainTurret.Sound(Type()->_mainTurret,inside,deltaT,*this,Speed());
	_comTurret.Sound(Type()->_comTurret,inside,deltaT,*this,Speed());
	base::Sound(inside,deltaT);
}

bool Tank::IsTurret( CameraType camType ) const
{
	AIUnit *unit = GWorld->FocusOn();
	if (unit)
	{
		if (unit==CommanderUnit())
		{
			// commander needs picture to be able to give commands
			return true;
		}
		if (unit==PilotUnit())
		{
			if (_driverHidden<0.5f) return false;
		}
		else if (unit==GunnerUnit())
		{
			if (_gunnerHidden<0.5f) return false;
		}
		if (unit==ObserverUnit())
		{
			//if (_commanderHidden<0.5) return false;
		}
	}
	return true;
}

bool Tank::HasFlares( CameraType camType ) const
{
	if( camType==CamGunner )
	{
		return Type()->_outPilotOnTurret;
	}
	return base::HasFlares(camType);
}

inline float tanSat(float x)
{
	// tan(pi*0.49) == 31.82
	if (x>+H_PI*0.49f) return +32;
	if (x<-H_PI*0.49f) return -32;
	return atan(x);
}
/*!
\patch 1.52 Date 4/19/2002 by Ondra
- Fixed: Mouse cursor was sometimes lost in tank commander optics view.
*/

void Tank::LimitCursorHard(CameraType camType, Vector3 &dir) const
{
	if (camType==CamGunner)
	{
		// when controlling weapons,
		// limit cursor to maximum angle given by current weapon systems
		// plus screen size angle
		AIUnit *unit = GWorld->FocusOn();
		Person *person = unit ? unit->GetPerson() : NULL;
		if (person)
		{
			if (person==_commander)
			{
				Vector3 relDir = DirectionWorldToModel(dir);
				float scrAngle = atan(GScene->GetCamera()->Top())*0.99f;
				//float scrAngle = 0;
				// commander turret does not elevate together with main turret
				const TurretType &ttype = Type()->_comTurret;
				float minElev = ttype._minElev;
				float maxElev = ttype._maxElev;
				// normalize direction to plane with distance 1
				float sizeXZ = relDir.SizeXZ();
				float minAlpha = minElev - scrAngle + ttype._neutralXRot;
				float maxAlpha = maxElev + scrAngle + ttype._neutralXRot;

				// convert angular elevation to offset in distance sizeXZ
				// avoid overflow when alpha is out of range -pi/2,+pi/2
				float minY = tanSat(minAlpha)*sizeXZ;
				float maxY = tanSat(maxAlpha)*sizeXZ;
				//float minY = tanSat(minAlpha);
				//float maxY = tanSat(maxAlpha);
				//float minY = minAlpha*sizeXZ;
				//float maxY = maxAlpha*sizeXZ;
				
				saturate(relDir[1],minY,maxY);
				dir = DirectionModelToWorld(relDir);
			}
			else if (person==_gunner)
			{
				const TurretType &ttype = Type()->_mainTurret;
				Vector3 relDir = DirectionWorldToModel(dir);
				float scrAngle = atan(GScene->GetCamera()->Top())*0.99f;
				//float scrAngle = 0;
				// commander turret does not elevate together with main turret
				float minElev = ttype._minElev;
				float maxElev = ttype._maxElev;
				// normalize direction to plane with distance 1
				float sizeXZ = relDir.SizeXZ();
				float minAlpha = minElev - scrAngle + ttype._neutralXRot;
				float maxAlpha = maxElev + scrAngle + ttype._neutralXRot;

				// convert angular elevation to offset in distance sizeXZ
				// avoid overflow when alpha is out of range -pi/2,+pi/2
				float minY = tanSat(minAlpha)*sizeXZ;
				float maxY = tanSat(maxAlpha)*sizeXZ;
				//float minY = tanSat(minAlpha);
				//float maxY = tanSat(maxAlpha);
				//float minY = minAlpha*sizeXZ;
				//float maxY = maxAlpha*sizeXZ;
				
				saturate(relDir[1],minY,maxY);
				dir = DirectionModelToWorld(relDir);
			}
			else if (person==_driver)
			{
			}
			else
			{
			}
		}
	}
}

void Tank::LimitVirtual
(
	CameraType camType, float &heading, float &dive, float &fov
) const
{
/*
	switch( camType )
	{
		case CamGunner:
			saturate(fov,0.07f,0.35f);
			base::LimitVirtual(camType,heading,dive,fov);
		break;
	}
*/
	base::LimitVirtual(camType,heading,dive,fov);
}

void Tank::InitVirtual
(
	CameraType camType, float &heading, float &dive, float &fov
) const
{
	base::InitVirtual(camType,heading,dive,fov);
/*
	switch( camType )
	{
		case CamGunner:
			fov=0.3;
		break;
	}
*/
}

void Tank::UnloadSound()
{
	base::UnloadSound();
	_engineSound.Free();
	_mainTurret.UnloadSound();
	_comTurret.UnloadSound();
}

Vector3 Tank::Friction( Vector3Par speed )
{
	Vector3 friction;
	friction.Init();
	friction[0]=speed[0]*fabs(speed[0])*50+speed[0]*1500+fSign(speed[0])*300;
	friction[1]=speed[1]*fabs(speed[1])*50+speed[1]*1100+fSign(speed[1])*60;
	friction[2]=speed[2]*fabs(speed[2])*15+speed[2]*100+fSign(speed[2])*10;
	return friction*GetMass()*(1.0f/60000);
}

void Tank::MoveWeapons(float deltaT )
{
	// move all turrets
	{
		AIUnit *unit = GunnerUnit();
		if( !unit ) 
		{
			_mainTurret.Stop(Type()->_mainTurret);
		}
		else
		{
		// check if driver hatch is closed
		// if not, main turret must be locked
		// note: this probably does not apply to some tanks (M113, BMP)
			if( GetHit(Type()->_gunHit)>0.9f )
			{
				_mainTurret.GunBroken(Type()->_mainTurret);
			}
			if (Type()->HasDriver() && _driverHidden<0.99f)
			{
				_mainTurret._gunStabilized = false;
				_mainTurret.Aim(Type()->_mainTurret,VForward);
			}
			else if( GetHit(Type()->_turretHit)>0.9f )
			{
				_mainTurret.TurretBroken(Type()->_mainTurret);
			}
			else
			{
				_mainTurret._gunStabilized=true;
			}
			_mainTurret.MoveWeapons(Type()->_mainTurret,unit,deltaT);
		}
	}
	{
		AIUnit *unit = ObserverUnit();
		if( !unit ) 
		{
			_comTurret.Stop(Type()->_comTurret);
		}
		else
		{
			if( GetHit(Type()->_turretHit)>0.9f )
			{
				_comTurret.TurretBroken(Type()->_comTurret);
			}
			else
			{
				_comTurret._gunStabilized=true;
			}
			_comTurret.MoveWeapons(Type()->_comTurret,unit,deltaT);
			/*
			LogF
			(
				"elev %.2f->%.2f, %.2f .. %.2f",
				_comTurret._xRot,_comTurret._xRotWanted,
				Type()->_comTurret._minElev,Type()->_comTurret._maxElev
			);
			*/
		}
	}
}

LSError TankWithAI::Serialize(ParamArchive &ar)
{
	CHECK( base::Serialize(ar) );
	//CHECK(ar.Serialize("randFrequency", _randFrequency, 1))
	
	if (!IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
	{
		//bool _doGearSound:1;

		CHECK(_mainTurret.Serialize(ar) )
		CHECK(_comTurret.Serialize(ar) )

		//float _servoVol;

		//WaterSource _leftWater,_rightWater;
		//DustSource _fireDust;

		CHECK(ar.Serialize("invFireDustTimeTotal", _invFireDustTimeTotal, 1))
		CHECK(ar.Serialize("fireDustTimeLeft", _fireDustTimeLeft, 1))
			
		//float _phaseL,_phaseR; // texture animation
		
		//TrackOptimized _track;

		CHECK(ar.Serialize("thrustLWanted", _thrustLWanted, 1, 0))
		CHECK(ar.Serialize("thrustRWanted", _thrustRWanted, 1, 0))
		CHECK(ar.Serialize("thrustL", _thrustL, 1, 0))
		CHECK(ar.Serialize("thrustR", _thrustR, 1, 0))
	}
	return LSOK;
}

bool Tank::IsPossibleToGetIn() const
{
	if( GetHit(Type()->_trackRHit)>=0.9f ) return false;
	if( GetHit(Type()->_trackLHit)>=0.9f ) return false;
	if( GetHit(Type()->_engineHit)>=0.9f ) return false;
	if( GetHit(Type()->_hullHit)>=0.9f ) return false;
	return base::IsPossibleToGetIn();
}

bool Tank::IsAbleToMove() const
{
	if( GetHit(Type()->_trackRHit)>=0.9f ) return false;
	if( GetHit(Type()->_trackLHit)>=0.9f ) return false;
	if( GetHit(Type()->_engineHit)>=0.9f ) return false;
	if( GetHit(Type()->_hullHit)>=0.9f ) return false;
	return base::IsAbleToMove();
}

bool Tank::IsAbleToFire() const
{
	if( GetHit(Type()->_turretHit)>=0.9f ) return false;
	if( GetHit(Type()->_gunHit)>=0.9f ) return false;
	return base::IsAbleToFire();
}

void Tank::LandFriction
(
	Vector3 &friction, Vector3 &torqueFriction,
	Vector3 &torque,
	bool brakeFriction, Vector3Par fSpeed, Vector3Par speed,
	Vector3Par pCenter,
	float coefNPoints, Texture *texture
)
{
	Vector3 pForce;
	pForce.Init();
	// second is "land friction" - causing no momentum
	pForce[0]=fSpeed[0]*2000+fSign(fSpeed[0])*30000;
	pForce[1]=fSpeed[1]*8000+fSign(fSpeed[1])*5000;
	if( brakeFriction )
	{
		pForce[2]=fSpeed[2]*200+fSign(fSpeed[2])*40000;
	}
	else
	{
		pForce[2]=fSpeed[2]*100+fSign(fSpeed[2])*2000;
	}

	#if ARROWS
	Vector3 wCenter(VFastTransform,ModelToWorld(),GetCenterOfMass());
	#endif

	pForce=DirectionModelToWorld(pForce)*GetMass()*(1.0f/40000);
	#if ARROWS
		AddForce(wCenter+pCenter,-pForce*InvMass(),Color(1,0,0));
	#endif
	// apply some torque
	friction+=pForce*coefNPoints;
	torqueFriction+=_angMomentum*0.12f*coefNPoints;

	// some friction is caused by moving the land aside
	// this applies only to soft surfaces
	float soft=0;
	if( texture )
	{
		soft=texture->Roughness()*0.7f;
		//soft*=softFactor;
		saturateMin(soft,1);
	}
	float landMoved=0.01f*soft;
	if( brakeFriction ) landMoved*=2;
	pForce[0]=speed[0]*35.0f*landMoved;
	pForce[1]=0;
	pForce[2]=speed[2]*6.0f*landMoved;
	pForce=DirectionModelToWorld(pForce)*GetMass();
	#if ARROWS
		AddForce(wCenter+pCenter,-pForce*InvMass(),Color(1,1,0,0.5f));
	#endif
	friction+=pForce*coefNPoints;
}

/*!
\patch 1.30 Date 7/30/2001 by Ondra.
- Fixed: tank reaction to collision with other tanks.
\patch 1.36 Date 12/13/2001 by Ondra
- Fixed: APC did very little harm to soldiers when in contact.
*/


void Tank::ObjectContact
(
	Frame &moveTrans,
	Vector3Par wCenter,
	float deltaT,
	Vector3 &torque, Vector3 &friction, Vector3 &torqueFriction,
	Vector3 &totForce, float &crash
)
{
	Vector3Val speed=ModelSpeed();

	CollisionBuffer collision;
	GLOB_LAND->ObjectCollision(collision,this,moveTrans);
	#define MAX_IN 0.2f
	#define MAX_IN_FORCE 0.1f
	#define MAX_IN_FRICTION 0.2f

	for( int i=0; i<collision.Size(); i++ )
	{
		_objectContact=true;
		// info.pos is relative to object
		CollisionInfo &info=collision[i];
		Object *obj=info.object;
		if( !obj ) continue;
		if (info.hierLevel>0) continue;
		if( !obj->GetShape() ) continue;
		//float maxImpulse=0; // impulse factor
		float cFactor = obj->GetMass()*InvMass();
		//bool isStatic=false;
		if( obj->Static() )
		{
			// fixed object - apply fixed collision routines
			// calculate his dammage
			// depending on vehicle speed and mass
			float dFactor=GetMass()*obj->InvMass();
			float dSpeed = _speed.SquareSize()+_angVelocity.SquareSize();
			float dammage = dSpeed*obj->GetInvArmor()*dFactor*0.2f;
			if( dammage>0.01f )
			{
				obj->LocalDammage(NULL,this,VZero,dammage,obj->GetShape()->GeometrySphere());
			}
			if
			(
				obj->GetDestructType()==DestructTree ||
				obj->GetDestructType()==DestructTent ||
				obj->GetDestructType()==DestructMan
			)
			{
				saturate(cFactor,0.001f,0.03f);
			}
			else
			{
				saturate(cFactor,0.001f,1);
			}
		}
		else
		{
			saturate(cFactor,0,10);
		}
		Point3 pos=info.object->PositionModelToWorld(info.pos);
		Vector3 dirOut=info.object->DirectionModelToWorld(info.dirOut);
		// create a force pushing "out" of the collision
		float forceIn=floatMin(info.under,MAX_IN_FORCE);
		Vector3 pForce=dirOut*GetMass()*40*forceIn*cFactor;
		// apply proportional part of force in place of impact
		Vector3 pCenter=pos-wCenter;
		if( cFactor>0.01f )
		{
			totForce+=pForce;
			// apply same force to second object
			torque+=pCenter.CrossProduct(pForce*0.5f);
		}

		Vehicle *veh=dyn_cast<Vehicle,Object>(obj);
		if( veh )
		{
			// transfer all my intertia to him?
			Vector3 relDistance = veh->Position()-Position();
			Vector3 relSpeed = veh->Speed()-Speed();
			if
			(
				_speed.SquareSize()>Square(3.5f) &&
				relSpeed.SquareSize()>Square(3.5f)
			)
			{
				if (dyn_cast<Person>(veh))
				{
					// soldier - different dammage calculation
					float speedTransfer = relSpeed*relDistance*-relDistance.InvSize();
					saturate(speedTransfer,0,0.5f);
					float limitMass = floatMin(GetMass(),20000);
					Vector3 impulse = _speed*limitMass*deltaT*speedTransfer*0.08f;
					
					veh->AddImpulseNetAware(impulse,info.pos.CrossProduct(impulse));
				}
				else
				{
					float speedTransfer = relSpeed*relDistance*-relDistance.InvSize();
					saturate(speedTransfer,0,0.5f);
					Vector3 impulse = _speed*GetMass()*deltaT*speedTransfer*0.02f;

					veh->AddImpulseNetAware(impulse,info.pos.CrossProduct(impulse));
				}
			}
		}
		
		if (cFactor<0.05f)
		{
			// TODO: apply some friction - based on cFactor
			continue;
		}
		
		// if info.under is bigger than MAX_IN, move out
		if( info.under>MAX_IN )
		{

			Matrix4 transform=moveTrans.Transform();
			Vector3 newPos=transform.Position();
			float moveOut=info.under-MAX_IN;
			Vector3 move=dirOut*moveOut*0.1f;
			newPos+=move;
			transform.SetPosition(newPos);
			moveTrans.SetTransform(transform);
			const float crashLimit=0.3f;
			if( moveOut>crashLimit ) crash+=moveOut-crashLimit;

			Vector3Val objSpeed=info.object->ObjectSpeed();
			Vector3 colSpeed=_speed-objSpeed;

			float potentialGain=move[1]*GetMass();
			float oldKinetic=GetMass()*colSpeed.SquareSize()*0.5f; // E=0.5*m*v^2
			// kinetic to potential conversion is not 100% effective
			float crashFactor=(moveOut-crashLimit)*4+1.5f;
			saturateMax(crashFactor,2.5f);
			float newKinetic=oldKinetic-potentialGain*crashFactor;
			float newSpeedSize2=newKinetic*InvMass()*2;
			if( newSpeedSize2<=0 || oldKinetic<=0 ) colSpeed=VZero;
			else colSpeed*=sqrt(newSpeedSize2*colSpeed.InvSquareSize());
			// limit relative speed to object we crashed into
			const float maxRelSpeed=2;
			if( colSpeed.SquareSize()>Square(maxRelSpeed) )
			{
				// adapt _speed to match criterion
				crash+=(colSpeed.Size()-maxRelSpeed)*0.3f;
				colSpeed.Normalize();
				colSpeed*=maxRelSpeed;
			}
			_speed=objSpeed+colSpeed;
		}

		// second is "land friction" - causing little momentum
		float frictionIn=floatMin(info.under,MAX_IN_FRICTION);
		pForce[0]=fSign(speed[0])*10000;
		pForce[1]=speed[1]*fabs(speed[1])*1000+speed[1]*8000+fSign(speed[1])*10000;
		pForce[2]=speed[2]*fabs(speed[2])*150+speed[2]*250+fSign(speed[2])*2000;

		pForce=DirectionModelToWorld(pForce)*GetMass()*(4.0f/10000)*frictionIn;
		#if ARROWS
			AddForce(wCenter+pCenter,-pForce*InvMass());
		#endif
		friction+=pForce;
		torqueFriction+=_angMomentum*0.15f;
	}
}

#if _ENABLE_CHEATS
extern bool disableSimpleSim;
#endif

void Tank::StabilizeTurrets
(
	Matrix3Val oldTrans, Matrix3Val newTrans,
	Matrix3Val oldTurretTrans
)
{
	_mainTurret.Stabilize
	(
		this,Type()->_mainTurret,
		oldTrans,newTrans
	);
	// stabilize with relation to mainTurret
	if (Type()->_comTurretOnMainTurret)
	{
		Matrix3 newTurretTrans = newTrans*TurretTransform().Orientation();
		_comTurret.Stabilize
		(
			this,Type()->_comTurret,oldTurretTrans,newTurretTrans
		);
	}
	else
	{
		_comTurret.Stabilize
		(
			this,Type()->_comTurret,oldTrans,newTrans
		);
	}
}

bool Tank::UseSimpleSimulation(SimulationImportance prec) const
{
	if (_isUpsideDown) return false;
	if (!_landContact || _waterContact) return false;
#if _ENABLE_CHEATS
	if (disableSimpleSim) return false;
#endif
	if (prec>=SimulateInvisibleNear)
	{
		return true;
	}
	return prec>=SimulateVisibleNear && Glob.time>_freeFallUntil;
}

//#include "statistics.hpp"

void Tank::SimulateOptimized( float deltaT, SimulationImportance prec )
{
	float step = SimulationPrecision();
	//static StatEventRatio ratio("Smple tank sim");
	//ratio.Count(UseSimpleSimulation(prec));

	if(UseSimpleSimulation(prec))
	{
		// when using simple simulation, we can use very big step
		step = 0.2f;
	}

	// variable simulation step, based on distance from camera
	_simulationSkipped+=deltaT;
	if( _simulationSkipped>=step )
	{
		Simulate(step,prec);
		_simulationSkipped-=step;
		ADD_COUNTER(simO,1);
	}
}

void Tank::PlaceOnSurface(Matrix4 &trans)
{
	base::PlaceOnSurface(trans);

	Vector3 pos = trans.Position();

	Texture *txt = NULL;
	float dX,dZ;
	GLandscape->RoadSurfaceYAboveWater(pos,&dX,&dZ,&txt);
	if (txt && GLandscape->GetTexture(0)==txt)
	{
		// estimate water sub-merging
		pos[1] -= 1;
	}
	else
	{
		Vector3 normal(-dX,1,-dZ);
		normal.Normalize();

		Shape *lcLevel = GetShape()->LandContactLevel();
		if (!lcLevel) lcLevel = GetShape()->GeometryLevel();
		Vector3 comDown = GetCenterOfMass();
		comDown[1] = lcLevel->Min().Y();
		Vector3 nPos= trans.FastTransform(comDown);

		// check COM position on the ground
		Vector3 oPos = nPos;
		nPos[1]=GLandscape->RoadSurfaceYAboveWater(nPos,&dX,&dZ);

		float offset = 0.075f;
		float normalizedOffset = offset/normal.Y();
		nPos[1] += normalizedOffset;

		pos[1] += nPos[1]-oPos[1];
	}
	trans.SetPosition(pos);
}

static Vector3 AddFriction( Vector3Val force, Vector3Val friction )
{
	Vector3 res=force+friction;
	if( res[0]*force[0]<=0 ) res[0]=0;
	if( res[1]*force[1]<=0 ) res[1]=0;
	if( res[2]*force[2]<=0 ) res[2]=0;
	return res;
}

/*!
\patch 1.11 Date 7/30/2001 by Ondra.
Improved: tank climbing on very steep hills (45-60%)
\patch 1.31 Date 11/8/2001 by Ondra.
- Fixed: vehicle explosion could be set on both local and remote instance.
This could cause less or more dammage to vehicle crew than it should.
\patch 1.43 Date 1/28/2002 by Ondra
- Fixed: AI tanks were able to climb hills too fast.
\patch 1.43 Date 2/1/2002 by Ondra
- Fixed: Tanks were jumping after being hit from small weapons.
\patch_internal 1.59 Date 5/22/2002 by Ondra
- Fixed: Tanks "sliding like on ice" bug (introduced in 1.58)
\patch 1.61 Date 5/27/2002 by Ondra
- Fixed: Tanks now move main gun into neutral position after any collision.
This should make tank movement in urban areas more robust.
\patch 1.82 Date 8/15/2002 by Ondra
- Fixed: Tanks jumping after getting in, especially on the hill.
*/

void Tank::Simulate( float deltaT, SimulationImportance prec )
{
	if (!_cargoLight && Type()->_cargoLightPos.Size() > 0)
	{
		LODShapeWithShadow *shape = GLOB_SCENE->Preloaded(Marker);
		_cargoLight = new LightPointOnVehicle
		(
			shape, Color(0, 0, 0, 0), Color(0, 0, 0, 0), this, Type()->_cargoLightPos
		);
		_cargoLight->LightPoint::Load((*Type()->_par) >> "CargoLight");
		GScene->AddLight(_cargoLight);
	}

	Vector3Val speed=ModelSpeed();
	float speedSize=fabs(speed.Z());
	if( !_engineOff )
	{
		ConsumeFuel(deltaT*(0.01f+_rpm*0.02f));

		if( _fuel<=0 ) _engineOff=true, _fuel=0, _rpmWanted=0;
		else
		{
			// calculate engine rpm
			_rpmWanted=speedSize*_gearBox.Ratio();
			if( _rpmWanted<0.3f )
			{
				float avgThrust=(fabs(_thrustR)+fabs(_thrustL))*0.5f;
				_rpmWanted=avgThrust*0.7f+0.3f;
			}
			saturate(_rpmWanted,0,2);
		}
	}
	else
	{
		// engine off
		_rpmWanted=0;
	}

	if
	(
		// tank is going to explode
		GetHit(Type()->_engineHit)>=0.9f || GetHit(Type()->_hullHit)>=0.9f
	)
	{
		if (IsLocal() && _explosionTime>Glob.time+60)
		{
			// set some explosion
			_explosionTime=Glob.time+GRandGen.Gauss(2,5,20);
		}
	}

	float delta;
	delta=_rpmWanted-_rpm;
	Limit(delta,-0.5f*deltaT,+0.3f*deltaT);
	_rpm+=delta;
	
	// TODO: commander view need not be attached to main turret
	// check actual selection

	Matrix3 oldTurretTrans = Orientation()*TurretTransform().Orientation();
	MoveWeapons(deltaT);

	if( _isDead || _isUpsideDown ) _engineOff=true,_pilotBrake=true;
	if( _engineOff ) _thrustLWanted=_thrustRWanted=0,_pilotBrake = true;

	if( fabs(_thrustLWanted)>0.1f || fabs(_thrustRWanted)>0.1f )
	{
		IsMoved();
	}

	//if( _impulseForce.SquareSize()>Square(GetMass()*1e-6f) )
	if( _impulseForce.SquareSize()>Square(GetMass()*0.005f) )
	{
		IsMoved();
		// make this time longer that stop detection
		// so that if vehicle is stable long enough, it goes directly
		// from complete simulation to full stop
		_freeFallUntil=Glob.time+5.5f;
	}
	if( !_landContact )
	{
		IsMoved();
		_freeFallUntil=Glob.time+2;
	}
	if (GetStopped())
	{
		// reset impulse - avoid acummulation
		_impulseForce = VZero;
		_impulseTorque = VZero;
	}

	if( EnableVisualEffects(prec) )
	{
		// TODO: move to Turret
		if( _gunFire.Active() || _gunClouds.Active() || _mGunClouds.Active() || _mGunFire.Active() )
		{

			const TurretType &tur = Type()->_mainTurret;
			Matrix4Val gunTransform = GunTurretTransform();
			Matrix4Val toWorld = Transform()*gunTransform;
			Vector3Val dir = toWorld.Direction();
			Vector3 firePos(VFastTransform,toWorld,tur._pos + 0.6f * tur._dir);
			Vector3 smokePos(VFastTransform,toWorld,tur._pos + 0.6f * tur._dir);
			Vector3 gunPos(VFastTransform,toWorld,Type()->_gunPos);
			_gunFire.Simulate(firePos,Speed()*0.85f+dir*20,0.8f,deltaT);
			_gunClouds.Simulate(smokePos,Speed()*0.7f+dir*5,0.7f,deltaT);
			_mGunClouds.Simulate(gunPos,Speed()*0.7f+dir*0.1f,0.35f,deltaT);
			_mGunFire.Simulate(gunPos,deltaT);
		}
	}
	
	bool isStatic = Type()->GetFuelCapacity()<=0;
	if (!GetStopped() && !isStatic && !CheckPredictionFrozen())
	{
	
		// simulate left/right engine
		delta=_thrustLWanted-_thrustL;
		if( _thrustLWanted*_thrustL<=0 ) Limit(delta,-2.0f*deltaT,+2.0f*deltaT);
		else Limit(delta,-deltaT,+deltaT);
		_thrustL+=delta;
		Limit(_thrustL,-1.0f,1.0f);

		delta=_thrustRWanted-_thrustR;
		if( _thrustRWanted*_thrustR<=0 ) Limit(delta,-2.0f*deltaT,+2.0f*deltaT);
		else Limit(delta,-deltaT,+deltaT);
		_thrustR+=delta;
		Limit(_thrustR,-1.0f,1.0f);

		// calculate all forces, frictions and torques
		Vector3 force(VZero),friction(VZero);
		Vector3 torque(VZero),torqueFriction(VZero);

		Vector3 pForce(NoInit); // partial force
		Vector3 pCenter(NoInit); // partial force application point

		Vector3 wCenter(VFastTransform,ModelToWorld(),GetCenterOfMass());

		// apply left/right thrust

		bool gearChanged=false;
		if( _engineOff )
		{
			// upside down
			gearChanged=_gearBox.Neutral();
		}
		else
		{
			gearChanged=_gearBox.Change(speedSize);
			if( _landContact || _objectContact || _waterContact )
			{

				// the more we turn, the more power we loose
				float eff=1-fabs(_thrustR-_thrustL)*0.4f;
				saturateMax(eff,0);
				float invSpeedSize;
				const float coefInvSpeed=3;
				const float defSpeed=80.0f; // model tuned at this speed (plain level grass)
				float power=Type()->GetMaxSpeed()*(1/defSpeed);
				// FIX: tank climbing (minInvSpeed was 2.5 in 1.00)
				const float minInvSpeed=2.0f;
				// FIX END
				const float invSpeedSizeMax=coefInvSpeed/(minInvSpeed*power);
				if( speedSize<minInvSpeed*power ) invSpeedSize=invSpeedSizeMax;
				else invSpeedSize=coefInvSpeed/speedSize;
				float rpmEff=0.7f;
				const float effC=3.0f*rpmEff*eff;
				float invSpeedSizeL=speed.Z()*_thrustL<0.1f ? invSpeedSizeMax : invSpeedSize;
				float invSpeedSizeR=speed.Z()*_thrustR<0.1f ? invSpeedSizeMax : invSpeedSize;
				invSpeedSizeL*=effC;
				invSpeedSizeR*=effC;
				//saturateMax(invSpeedSize,1/power);
				//float rpmEff=rpm*0.5+0.5;
				// water movement is much less efficient
				if( !_landContact && !_objectContact )
				{
					invSpeedSizeL*=0.1f;
					invSpeedSizeR*=0.1f;
				}
				float turnEnhancer=speedSize*0.5f*(1/power);
				turnEnhancer*=CollisionSize()*(1.0f/3);
				saturate(turnEnhancer,2,10);
				float lAccel=_thrustL*invSpeedSizeL*power;
				float rAccel=_thrustR*invSpeedSizeR*power;

				rAccel *= 1-GetHit(Type()->_engineHit);
				lAccel *= 1-GetHit(Type()->_engineHit);

				rAccel *= 1-GetHit(Type()->_trackRHit);
				lAccel *= 1-GetHit(Type()->_trackLHit);

				pForce=Vector3(0,0,lAccel*GetMass());
				force+=pForce;
				pCenter=Vector3(+7*turnEnhancer,0,0); // relative to the center of mass
				torque+=pCenter.CrossProduct(pForce);
				#if ARROWS
					AddForce
					(
						DirectionModelToWorld(pCenter)+wCenter,
						DirectionModelToWorld(pForce*InvMass())
					);
				#endif

				pForce=Vector3(0,0,rAccel*GetMass());
				force+=pForce;
				pCenter=Vector3(-7*turnEnhancer,0,0); // relative to the center of mass
				torque+=pCenter.CrossProduct(pForce);
				#if ARROWS
					AddForce
					(
						DirectionModelToWorld(pCenter)+wCenter,
						DirectionModelToWorld(pForce*InvMass())
					);
				#endif
			}
		}

		if( gearChanged )
		{
			_doGearSound=true;
		}

		// animate track
		// add movement forward (partially)
		float forwardChangePhase = ModelSpeed()[2]*0.5f;
		const float thrustCoef = 0.5f;
		_phaseL+=(_thrustL*thrustCoef+forwardChangePhase)*deltaT;
		if( _phaseL>=1 ) _phaseL-=1;
		if( _phaseL<0 ) _phaseL+=1;

		_phaseR+=(_thrustR*thrustCoef+forwardChangePhase)*deltaT;
		if( _phaseR>=1 ) _phaseR-=1;
		if( _phaseR<0 ) _phaseR+=1;

		// convert forces to world coordinates
		DirectionModelToWorld(torque,torque);
		DirectionModelToWorld(force,force);

		// apply gravity
		pForce=Vector3(0,-G_CONST*GetMass(),0);
		force+=pForce;
		
		#if ARROWS
			AddForce(wCenter,pForce*InvMass());
		#endif

		// angular velocity causes also some angular friction
		// this should be simulated as torque
		//torqueFriction=_angMomentum*2.5;
		torqueFriction=_angMomentum*1.2f;

		// calculate new position
		Matrix4 movePos;
		ApplySpeed(movePos,deltaT);
		// consider using frame with inverse
		Frame moveTrans;
		moveTrans.SetTransform(movePos);


		// body air friction
		DirectionModelToWorld(friction,Friction(speed));
		//friction=Vector3(0,0,0);
		#if ARROWS
			AddForce(wCenter,friction*InvMass());
		#endif
		
		//wCenter=moveTrans.PositionModelToWorld();
		// recalculate COM to reflect change of position
		wCenter.SetFastTransform(moveTrans.ModelToWorld(),GetCenterOfMass());

		Texture *texture=NULL;

		if( deltaT>0 )
		{
			// check collision on new position
			Vector3 totForce(VZero);

			float crash=0;
			_objectContact=false;
			if( prec<=SimulateVisibleFar && IsLocal())
			{
				ObjectContact
				(
					moveTrans,
					wCenter,deltaT,
					torque,friction,torqueFriction,
					totForce,crash
				);
			} // if( object collisions enabled )

			float nSpeed=Type()->GetMaxSpeed()*0.16f; // km/h
			bool brakeFriction=false;
			Vector3 fSpeed;
			if( _landContact && ( DirectionUp().Y()<=0.3f || _pilotBrake ) )
			{
				// brake friction speed
				brakeFriction=true;
				fSpeed=speed;
			}
			else
			{
				fSpeed=speed-Vector3(0,0,(_thrustL+_thrustR)*nSpeed);
			}
			float maxUnderWater=0;
			float softFactor=floatMin(5000*GetInvMass(),1.0f);
			#define MAX_UNDER_FORCE 0.1f
			#define UNDER_OFFSET MAX_UNDER_FORCE

			if(UseSimpleSimulation(prec))
			{
				// TODO: handle also water (floating)
				// very simple simulation
				// - single point terrain following
				Shape *lcLevel = GetShape()->LandContactLevel();
				if (!lcLevel) lcLevel = GetShape()->GeometryLevel();

				float dX,dZ;
				//Vector3 comOffset=moveTrans.DirectionModelToWorld(GetCenterOfMass());
				//Vector3 pos=moveTrans.PositionModelToWorld(GetCenterOfMass());

				// check COM-corresponding position on the landcontact level
				Vector3 comDown = GetCenterOfMass();
				comDown[1] = lcLevel->Min().Y();
				Vector3 nPos= moveTrans.FastTransform(comDown);

				// check COM position on the ground
				Vector3 oPos = nPos;

				// calculate surface Y in COM position
				//Vector3 pos=moveTrans.Position();
				//Vector3 nPos = oPos;
				nPos[1]=GLandscape->RoadSurfaceYAboveWater(nPos,&dX,&dZ,&texture);
#if _ENABLE_CHEATS
				if (CHECK_DIAG(DECollision))
				{
					GScene->DrawCollisionStar(oPos,0.06f,PackedColor(Color(0.25,0.25,0)));
					GScene->DrawCollisionStar(nPos,0.1f,PackedColor(Color(0.5,0.5,0)));
				}
#endif

				Vector3 normal(-dX,1,-dZ);
				normal.Normalize();

				// convert to object position
				// check if there is some water

				if (texture && texture==GLandscape->GetTexture(0))
				{
					// simulate water movement - cannot use simple simulation here?
					_waterContact = true;
				}
				// offset tuned empirically
				// so that simple and complete simulation do not differ
				float offset = 0.07f;

				// Min().Y() offset should be considered in direction of surface normal
				float normalizedOffset = offset/normal.Y();
				nPos[1] += normalizedOffset;

				//GlobalShowMessage
				//(
				//	100,"offset %.3f, normalizedOffset %.3f",offset,normalizedOffset
				//);

				// one contact point is always used in this version
				//const float coefNPoints = 9.0f/lcPoints;
				const float coefNPoints = 9.0f;

				Vector3 stabilize=normal-moveTrans.DirectionUp();
				torque+=Vector3(0,coefNPoints*8*GetMass(),0).CrossProduct(stabilize);

				if (!_waterContact)
				{
					Vector3 mPos = moveTrans.Position();
					mPos[1] += nPos[1]-oPos[1];
					moveTrans.SetPosition(mPos);
				}

				// there should be force pushing tank out-of-ground
				// vertical component of this force should be 1G
				// first approximation: force size is 1G
				pForce=normal*GetMass()*G_CONST;
				totForce+=pForce;

				// calculate friction
				LandFriction
				(
					friction,torqueFriction,torque,
					brakeFriction,fSpeed,speed,
					Vector3(0,-1,0), // simpilfied simulation - apply below center of mass
					coefNPoints,texture
				);

				// calculate acceleration
				// align speed to copy terrain
				_speed[1]=_speed[0]*dX+_speed[2]*dZ;
				ADD_COUNTER(tGndS,1);
			}
			else
			{
				GroundCollisionBuffer gCollision;
				if( prec>=SimulateVisibleNear )
				{
					GLandscape->GroundCollisionPlane(gCollision,this,moveTrans,UNDER_OFFSET,softFactor);
					ADD_COUNTER(tGndP,gCollision.Size());
				}
				else
				{
					GLandscape->GroundCollision(gCollision,this,moveTrans,UNDER_OFFSET,softFactor);
					ADD_COUNTER(tGndC,gCollision.Size());
				}
				_landContact=false;
				_waterContact=false;
				#define MAX_UNDER 0.4f

				Shape *lcLevel = GetShape()->LandContactLevel();
				if (!lcLevel) lcLevel = GetShape()->GeometryLevel();
				int lcPoints = lcLevel ? lcLevel->NPoints() : 6;
				saturateMax(lcPoints,gCollision.Size());

				const float coefNPoints = 9.0f/lcPoints;

				float maxUnder=0;
				for( int i=0; i<gCollision.Size(); i++ )
				{
					// info.pos is world space
					UndergroundInfo &info=gCollision[i];
					// we consider two forces
					if( info.under<0 ) continue;
					if( info.type==GroundWater )
					{
						_waterContact=true;
						// simulate swimming force
						//const float coefNPoints=3;
						// first is water is "pushing" everything up - causing some momentum
						saturateMax(maxUnderWater,info.under);

						pForce=Vector3(0,15000*info.under*coefNPoints,0);
						if( !Type()->_canFloat ) pForce*=0.3f;
						pCenter=info.pos-wCenter;
						torque+=pCenter.CrossProduct(pForce);
						totForce+=pForce;

						// add stabilizing torque
						// stabilized means DirectionUp() is (0,1,0)
						Vector3 stabilize=VUp-moveTrans.DirectionUp();
						torque+=Vector3(0,coefNPoints*1.8f*GetMass(),0).CrossProduct(stabilize);

						#if ARROWS
							AddForce(wCenter+pCenter,pForce*InvMass());
						#endif
						
						// second is "water friction" - causing no momentum
						pForce[0]=speed[0]*fabs(speed[0])*15;
						pForce[1]=speed[1]*fabs(speed[1])*15+speed[1]*40;
						pForce[2]=speed[2]*fabs(speed[2])*6;

						pForce=DirectionModelToWorld(pForce*info.under)*GetMass()*(coefNPoints/350);
						#if ARROWS
							AddForce(wCenter+pCenter,-pForce*InvMass());
						#endif
						friction+=pForce;
						torqueFriction+=_angMomentum*0.15f;

						if( _speed.SquareSize()>Square(8) ) crash=2;
					}
					else
					{
						_landContact=true;
						if( maxUnder<info.under ) maxUnder=info.under;
						float under=floatMin(info.under,MAX_UNDER_FORCE);

						//const float coefNPoints=1.5f;
						// one is ground "pushing" everything out - causing some momentum
						Vector3 dirOut=Vector3(0,info.dZ,1).CrossProduct(Vector3(1,info.dX,0)).Normalized();
						pForce=dirOut*GetMass()*40.0f*under*coefNPoints;
						pCenter=info.pos-wCenter;
						torque+=pCenter.CrossProduct(pForce);
						// to do: analyze ground reaction force
						totForce+=pForce;

						#if ARROWS
							AddForce(wCenter+pCenter,pForce*under*InvMass());
						#endif
						
						// friction
						LandFriction
						(
							friction,torqueFriction,torque,
							brakeFriction,fSpeed,speed,pCenter,
							coefNPoints,info.texture
						);
						// select roughest texture
						if( !texture || info.texture && info.texture->Roughness()>texture->Roughness() )
						{	
							texture=info.texture;
						}
					}
				}
				//GlobalShowMessage(100,"MaxUnder %.3f",maxUnder);
				if( maxUnder>MAX_UNDER )
				{
					// it is neccessary to move object immediatelly
					Matrix4 transform=moveTrans.Transform();
					Point3 newPos=transform.Position();
					float moveUp=maxUnder-MAX_UNDER;
					newPos[1]+=moveUp;
					transform.SetPosition(newPos);
					moveTrans.SetTransform(transform);
					// we move up - we have to maintain total energy
					// what potential energy will gain, kinetic must loose
					const float crashLimit=0.3f;
					if( moveUp>crashLimit ) crash+=moveUp-crashLimit;
					float potentialGain=moveUp*GetMass();
					float oldKinetic=GetMass()*_speed.SquareSize()*0.5f; // E=0.5*m*v^2
					// kinetic to potential conversion is not 100% effective
					float crashFactor=(moveUp-crashLimit)*4+1.5f;
					saturateMax(crashFactor,2.5f);
					float newKinetic=oldKinetic-potentialGain*crashFactor;
					//float newSpeedSize=sqrt(newKinetic*InvMass()*2);
					float newSpeedSize2=newKinetic*InvMass()*2;
					// _speed=_speed*_speed.InvSize()*newSpeedSize
					if( newSpeedSize2<=0 || oldKinetic<=0 ) _speed=VZero;
					else _speed*=sqrt(newSpeedSize2*_speed.InvSquareSize());
				}
			} // simple simulation not aplicable

			force+=totForce;
			float crashTreshold=30*GetMass(); // 3G
			float forceCrash=0;
			if( totForce.SquareSize()>Square(crashTreshold) )
			{
				forceCrash=(totForce.Size()-crashTreshold)*InvMass()*(1.0f/100);
			}
			crash+=forceCrash;
			if( crash>0.1f )
			{
				if( Glob.time>_disableDammageUntil )
				{
					// crash boom bang state - impact speed too high
					_doCrash=CrashLand;
					if( _objectContact ) _doCrash=CrashObject;
					if( _waterContact ) _doCrash=CrashWater;
					_crashVolume=crash*0.5f;
					if( _doCrash==CrashWater ) crash*=0.1f;
				}
			}
			if( !Type()->_canFloat )
			{
				const float maxFord=1.8f;
				if( maxUnderWater>maxFord )
				{
					float dammage=(maxUnderWater-maxFord)*0.5f;
					saturateMin(dammage,0.2f);
					LocalDammage(NULL,this,VZero,dammage*deltaT,GetRadius());
				}
			}
		}

		if (_objectContact)
		{
			_turretFrontUntil = Glob.time+30;
		}
		// apply all forces
		// handle special case: brake is on, speed is very low and total forces are low
		Vector3 frictionedForce = AddFriction(force,-friction);
		Vector3 frictionedTorque = AddFriction(torque,-torqueFriction);
		float staticFriction = 0;
		if
		(
			_pilotBrake && _landContact
		)
		{
			staticFriction = GetMass()*G_CONST*0.5f; // assume brakes can hold 0.5 G
		}
		/*
		LogF
		(
			"force %.2f, fric %.2f, torque %.2f, fForce %.2f, fTorque %.2f, speed %.2f, angv %.2f",
			force.Size()*GetInvMass(),friction.Size()*GetInvMass(),torque.Size()*GetInvMass(),
			frictionedForce.Size()*GetInvMass(),frictionedTorque.Size()*GetInvMass(),
			Speed().Size(),_angVelocity.Size()
		);
		*/
		ApplyForces(deltaT,force,torque,friction,torqueFriction,staticFriction);

		bool stopCondition=false;
		if( _pilotBrake && _landContact && !_waterContact && !_objectContact )
		{
			// apply static friction
			float maxSpeed=Square(0.7f);
			if( !Driver() ) maxSpeed=Square(1.2f);
			if( _speed.SquareSize()<maxSpeed && _angVelocity.SquareSize()<maxSpeed*0.3f )
			{
				stopCondition=true;
			}
		}
		if( stopCondition) StopDetected();
		else IsMoved();


		// simulate track drawing		
		if( EnableVisualEffects(prec) )
		{

			if( DirectionUp().Y()>=0.3f )
			{
				_track.Update(*this,deltaT,!_landContact);
			}
			if( _landContact )
			{
				float dust=texture ? texture->Dustness()*0.7f : 0.1f;
				// check texture under 
				Vector3 lPos=PositionModelToWorld(_track.LeftPos()+Vector3(+0.5f,0.1f,0));
				Vector3 rPos=PositionModelToWorld(_track.RightPos()+Vector3(-0.5f,0.1f,0));
				float dSoft=floatMax(dust,0.003f);

				float lDensity = (speedSize*0.5f+fabs(_thrustL))*dSoft;
				float rDensity = (speedSize*0.5f+fabs(_thrustL))*dSoft;

				saturateMin(lDensity,1);
				saturateMin(rDensity,1);
				float dustColor=dSoft*8;
				saturate(dustColor,0,1);
				Color color=Color(0.51f,0.46f,0.33f)*dustColor+Color(0.5f,0.5f,0.5f)*(1-dustColor);
				_leftDust.SetColor(color);
				_rightDust.SetColor(color);
				if (lDensity>0.2f)
				{
					_leftDust.Simulate(lPos+_speed*0.2f,Speed()*0.25f,lDensity,deltaT);
				}
				if (rDensity>0.2f)
				{
					_rightDust.Simulate(rPos+_speed*0.2f,Speed()*0.25f,rDensity,deltaT);
				}
				if( _fireDustTimeLeft>0 )
				{
					float phase=1-_fireDustTimeLeft*_invFireDustTimeTotal;
					const float thold=0.02f;
					if( phase>thold ) phase=(1-phase)*(1/(1-thold));
					else phase=phase*(1/thold);
					float density=phase*dSoft*1.5f;
					saturateMin(density,1);
					Vector3 dustPos=(lPos+rPos)*0.5f+GetWeaponDirection(0)*6;
					Vector3 dustSpeed
					(
						GRandGen.RandomValue()-0.5f,
						GRandGen.RandomValue()*0.8f+0.2f,
						GRandGen.RandomValue()-0.5f
					);
					_fireDust.SetColor(color);
					_fireDust.Simulate(dustPos,dustSpeed,density,deltaT);
					_fireDustTimeLeft-=deltaT;
				}
			}
			if( _waterContact )
			{
				if( Type()->_canFloat )
				{
					Vector3 lPos=PositionModelToWorld(Vector3(+0.3f,0,-3.2f));
					Vector3 rPos=PositionModelToWorld(Vector3(-0.3f,0,-3.2f));
					lPos[1]=GLOB_LAND->GetSeaLevel(); // sea level
					rPos[1]=GLOB_LAND->GetSeaLevel();
					float dens=floatMin(speedSize*0.3f,0.7f);
					float densL=fabs(_thrustL)+dens;
					float densR=fabs(_thrustR)+dens;
					saturateMin(densL,1);
					saturateMin(densR,1);
					float coefL=_thrustL*0.3f;
					float coefR=_thrustR*0.3f;
					saturateMax(coefL,0.05f);
					saturateMax(coefR,0.05f);
					Vector3 spdL=Speed()*0.1f-DirectionModelToWorld(Vector3(0,0,4)*coefL);
					Vector3 spdR=Speed()*0.1f-DirectionModelToWorld(Vector3(0,0,4)*coefR);
					//_leftWater.Simulate(lPos+_speed*0.5,spdL,densL,deltaT);
					//_rightWater.Simulate(rPos+_speed*0.5,spdR,densR,deltaT);
					_leftWater.Simulate(lPos,spdL,densL,deltaT);
					_rightWater.Simulate(rPos,spdR,densR,deltaT);
				}
			}
		}

		SimulateExhaust(deltaT,prec);
	
		// simulate pilot's head movement
		if( prec<=SimulateCamera ) _head.Move(deltaT,moveTrans,*this);

		#if 0 //_ENABLE_CHEATS
			// shake the tank
			float randRot = GRandGen.PlusMinus(0,0.2f);
			Matrix3 rot(MRotationY,randRot);
			moveTrans.SetOrientation(moveTrans.Orientation()*rot);
		#endif

		StabilizeTurrets
		(
			Transform().Orientation(),moveTrans.Orientation(),oldTurretTrans
		);


		Move(moveTrans);
		DirectionWorldToModel(_modelSpeed,_speed);
	} // if (!GetStopped() && !isStatic)



	base::Simulate(deltaT,prec);
}

inline Matrix4 Tank::TurretTransform() const
{
	//return MIdentity;
	int memory = GetShape()->FindMemoryLevel();
	int sel = Type()->_mainTurret._body.GetSelection(memory);
	if (sel>=0)
	{
		Matrix4 mat=MIdentity;
		AnimateMatrix(mat,memory,sel);
		return mat;
	}
	return MIdentity;
}

Matrix4 Tank::GunTurretTransform() const
{
	// animate matrix connected with selection Type()->_mainTurret._gun
	int memory = GetShape()->FindMemoryLevel();
	int sel = Type()->_mainTurret._gun.GetSelection(memory);
	if (sel>=0)
	{
		Matrix4 mat=MIdentity;
		AnimateMatrix(mat,memory,sel);
		return mat;
	}
	return MIdentity;
}

inline Matrix4 Tank::ObsTransform() const
{
	int memory = GetShape()->FindMemoryLevel();
	int sel = Type()->_comTurret._body.GetSelection(memory);
	if (sel>=0)
	{
		Matrix4 mat=MIdentity;
		AnimateMatrix(mat,memory,sel);
		return mat;
	}
	return MIdentity;
}

inline Matrix4 Tank::ObsGunTurretTransform() const
{
	int memory = GetShape()->FindMemoryLevel();
	int sel = Type()->_comTurret._gun.GetSelection(memory);
	if (sel>=0)
	{
		Matrix4 mat=MIdentity;
		AnimateMatrix(mat,memory,sel);
		return mat;
	}
	return MIdentity;
}

bool Tank::AnimateTexture
(
	int level,
	float phaseL, float phaseR, float speedL, float speedR
)
{
	// animate tracks
	Shape *shape=_shape->Level(level);
	if( !shape ) return false;

	// use _phaseL to modify u,v mapping of the faces
	// we need some textyre to be able to perform konversion to physical
	Type()->_leftOffset.UVOffset(_shape,0,-_phaseL,level);
	Type()->_rightOffset.UVOffset(_shape,0,-_phaseR,level);

	return false; // no alpha change
}

bool Tank::IsAnimated( int level ) const {return true;}
bool Tank::IsAnimatedShadow( int level ) const {return true;}

void Tank::AnimateMatrix(Matrix4 &mat, int level, int selection) const
{
	// default proxy transform calculation
	// check which selection is the proxy in:
	//LogF("AnimateMatrix %s",.
	const TankType *type = Type();
	_comTurret.AnimateMatrix(type->_comTurret,mat,this,level,selection);
	_mainTurret.AnimateMatrix(type->_mainTurret,mat,this,level,selection);
}

Vector3 Tank::AnimatePoint( int level, int index ) const
{
	// note: only turret/gun animation is done here
	// check which animations is this point in

	Shape *shape = _shape->Level(level);
	if( !shape ) return VZero;
	shape->SaveOriginalPos();

	Vector3 pos = shape->OrigPos(index);

	_comTurret.AnimatePoint(Type()->_comTurret,pos,this,level,index);
	_mainTurret.AnimatePoint(Type()->_mainTurret,pos,this,level,index);

	return pos;
}


void Tank::Animate( int level )
{
	Shape *shape = _shape->Level(level);
	if( !shape ) return;
	// set gun and turret to correct position
	// calculate animation transformation
	// turret transformation
	const TankType *type = Type();

	_mainTurret.Animate(type->_mainTurret,this,level);
	_comTurret.Animate(type->_comTurret,this,level);

	float value = 0.5f * Glob.time.toFloat();
	value = value - toIntFloor(value);
	Type()->_radarIndicator.SetValue(_shape, level, value);
	Vector3 dir = GetWeaponDirection(0);
	value = atan2(dir.X(), dir.Z());
	Type()->_watch.SetTime(_shape, level, Glob.clock);

	// note: turret indicator is always mounted on turret
	if (Type()->_turretIndicator.GetSelection(level)>=0)
	{
		Matrix4 rot;
		Type()->_turretIndicator.GetRotationForValue(rot,level,value);
		// combine indicator with turret animation
		Matrix4 turRot = _mainTurret.TurretTransform(type->_mainTurret);
		Type()->_turretIndicator.Transform(_shape,rot,level);
	}

	// assume driver is not bound to any turret
	type->_hatchDriver.Open(MIdentity,_shape, level, 1.0f - _driverHidden);
	int sel = type->_hatchCommander.GetSelection(level);
	if (sel>=0)
	{
		Matrix4 mat = MIdentity;
		AnimateMatrix(mat,level,sel);
		type->_hatchCommander.Open(mat,_shape, level, 1.0f - _commanderHidden);
	}
	sel = type->_hatchGunner.GetSelection(level);
	if (sel>=0)
	{
		Matrix4 mat = MIdentity;
		AnimateMatrix(mat,level,sel);
		type->_hatchGunner.Open(mat, _shape, level, 1.0f - _gunnerHidden);
	}

	if (_mGunFireFrames > 0 || Glob.uiTime < _mGunFireTime + 0.05f)
	{
		type->_animFire.Unhide(_shape, level);
		type->_animFire.SetPhase(_shape, level, _mGunFirePhase);
		_mGunFireFrames--;
	}
	else
	{
		type->_animFire.Hide(_shape, level);
	}

	base::Animate(level);

	// TODO: animate min-max box
	// set minmax box and sphere
	Vector3 min = shape->MinOrig();
	Vector3 max = shape->MaxOrig();
	Vector3 bCenter = shape->BSphereCenterOrig();
	float bRadius = shape->BSphereRadiusOrig();
	// enlarge sure vehicle will fit it
	Vector3 factor(3,1.2f,1.2f);
	float sFactor = 2.5f;
	min = bCenter+factor.Modulate(min-bCenter);
	max = bCenter+factor.Modulate(max-bCenter);
	bRadius *=sFactor;
	shape->SetMinMax(min,max,bCenter,bRadius);

	AnimateTexture(level,_phaseL,_phaseR,_thrustL,_thrustR);

	// rotate all wheels - based on phaseL / phaseR
	// TODO: different multiplier for phase offset and wheel rotation
	Matrix4 rotL(MRotationX,_phaseL*2*H_PI);
	Matrix4 rotR(MRotationX,_phaseR*2*H_PI);
	for (int w=0; w<type->_wheelsRotL.Size(); w++)
	{
		const AnimationWithCenter &anim = type->_wheelsRotL[w];
		// rotate 
		anim.Apply(_shape,rotL,level);
	}
	for (int w=0; w<type->_wheelsRotR.Size(); w++)
	{
		const AnimationWithCenter &anim = type->_wheelsRotR[w];
		// rotate 
		anim.Apply(_shape,rotR,level);
	}
	// calculate offsets for all wheels
	typedef AutoArray<AnimationWithCenter> WheelArray;
	const WheelArray *wheelsLR[2]={&type->_wheelsUpDownL,&type->_wheelsUpDownR};
	const WheelArray *tracksLR[2]={&type->_tracksUpDownL,&type->_tracksUpDownR};

	for (int lr=0; lr<2; lr++)
	{
		const Matrix4 &rot = lr ? rotL : rotR;
		const WheelArray *wheels = wheelsLR[lr];
		const WheelArray *tracks = tracksLR[lr];
		for (int w=0; w<wheels->Size(); w++)
		{
			if (w>=tracks->Size()) continue;
			const AnimationWithCenter *animW = &wheels->Get(w);
			const AnimationWithCenter *animT = &tracks->Get(w);
			if (!animT || !animW) continue;
			Vector3 centerT = animT->Center(level);
			Vector3 centerW = animW->Center(level);
			Vector3 wCenterT = PositionModelToWorld(centerT);
			// add surface randomization
			Vector3Val surfPos = wCenterT;
			float dX,dZ,bump; // dummy return values
			Texture *tex;
			float lY = GLandscape->BumpySurfaceY
			(
				surfPos.X(),surfPos.Z(),dX,dZ,tex,1,bump
			); // TODO: control bumpines
			float radius = 0.1f;
			float dist = lY - surfPos.Y() + radius;
			saturate(dist,-0.05f,+0.3f);
			Matrix4 offset(MTranslation,Vector3(0,dist,0));
			// 

			Matrix4 trans =
			(
				offset * Matrix4(MTranslation,centerW)*
				rot* Matrix4(MTranslation,-centerW)
			);
			
			animW->Transform(_shape,trans,level);
			animT->Transform(_shape,offset,level);
		}
	}

}

void Tank::Deanimate( int level )
{
	if( !_shape->Level(level) ) return;
	base::Deanimate(level);
	const TankType *type = Type();

	_mainTurret.Deanimate(type->_mainTurret,_shape,level);
	_comTurret.Deanimate(type->_comTurret,_shape,level);

	type->_hatchDriver.Restore(_shape, level);
	type->_hatchCommander.Restore(_shape, level);
	type->_hatchGunner.Restore(_shape, level);
}

RString Tank::DiagText() const
{
	char buf[512];
	float dx,dz;
	Vector3 pos = Position();

	pos[1] = GLandscape->SurfaceYAboveWater(pos[0], pos[2], &dx, &dz);

	float slope = Direction().Z()*dz + Direction().X()*dx;

	sprintf
	(
		buf,"  Slope %.0f %%, %s %.2f,%.2f",
		slope*100,
		_pilotBrake ? "B " : "E ",
		_thrustLWanted,_thrustRWanted
	);

	return base::DiagText()+RString(buf);
}

void Tank::Draw( int level, ClipFlags clipFlags, const FrameBase &frame )
{
	//return;

	if( level==LOD_INVISIBLE ) return; // invisible LOD
	base::Draw(level,clipFlags,frame);
}

#if ALPHA_SPLIT
void Tank::DrawAlpha( int level, ClipFlags clipFlags, const FrameBase &frame )
{
	base::Draw(level,clipFlags);
}
#endif

void Tank::Eject(AIUnit *unit)
{
	base::Eject(unit);
}

void Tank::FakePilot( float deltaT )
{
}

void Tank::JoystickPilot( float deltaT )
{
	_thrustRWanted = _thrustLWanted = GInput.GetStickForward();

	float stickLeft = GInput.GetStickLeft();
	_thrustRWanted -= stickLeft;
	_thrustLWanted += stickLeft;

	Limit(_thrustRWanted,-1,1);
	Limit(_thrustLWanted,-1,1);

	if
	(
		fabs(_thrustLWanted)+fabs(_thrustRWanted)<0.1f && fabs(_modelSpeed.Z())<5.0f
	)
	{
		_pilotBrake=true;
	}
	else
	{
		if
		(
			ModelSpeed()[2]*(_thrustLWanted+_thrustRWanted)<-4
		)
		{
			_pilotBrake = true;
		}
		else
		{
			_pilotBrake=false;
		}
		if (fabs(_thrustLWanted)+fabs(_thrustRWanted)>0.1f)
		{
			CancelStop();
			EngineOn();
		}
	}
}

void Tank::SuspendedPilot(AIUnit *unit, float deltaT )
{
}

/*!
\patch 1.24 Date 9/21/2001 by Ondra.
- Fixed: Tank wobbling when driving South, esp. with mouse.
\patch 1.82 Date 8/15/2002 by Ondra
- Fixed: Tank braking singnificantly faster.
\patch 1.82 Date 8/15/2002 by Ondra
- Fixed: Tanks no longer slide aside when standing on the hills.
*/

void Tank::KeyboardPilot(AIUnit *unit, float deltaT )
{
	if( GInput.JoystickActive() )
	{
		JoystickPilot(deltaT);
		return;
	}

	float forward=(GInput.keyMoveForward-GInput.keyMoveBack)*0.75f;
	forward+=GInput.keyMoveFastForward;
	forward+=GInput.keyMoveSlowForward*0.33f;

	if (ModelSpeed()[2]>2 && forward<-0.5) _backwardUsedAsBrake = true;
	else if (forward>=0) _backwardUsedAsBrake = false;

	if (ModelSpeed()[2]<-2 && forward>+0.5) _forwardUsedAsBrake = true;
	else if (forward<=0) _forwardUsedAsBrake = false;

	bool brake = _backwardUsedAsBrake || _forwardUsedAsBrake;
	if (brake)
	{
		forward = -ModelSpeed()[2]*10;
		saturate(forward,-1,1);
		if (_speed.SquareSize()<0.2f)
		{
			_parkingBrake = true;
			_backwardUsedAsBrake = _forwardUsedAsBrake = false;
		}
	}
	else
	{
		if (fabs(forward)>0.25f)
		{
			_parkingBrake = false;
		}
	}

	_thrustRWanted=_thrustLWanted=forward;

	bool internalCamera = IsGunner(GWorld->GetCameraType());
	bool mouseControl = internalCamera && GInput.MouseTurnActive() && !GInput.lookAroundEnabled;
	bool fullTurn = fabs(_thrustLWanted+_thrustRWanted)<0.01f;

	float estT = mouseControl ? 0.75f : 0.25f;

	if (!fullTurn) estT *= 2;

	// estimate heading
	Matrix3Val orientation=Orientation();
	Matrix3Val derOrientation=_angVelocity.Tilda()*orientation;
	Matrix3Val estOrientation=orientation+derOrientation*estT;
	Vector3Val estDirection=estOrientation.Direction();

	float curHeading=atan2(Direction()[0],Direction()[2]);
	float estHeading=atan2(estDirection[0],estDirection[2]);

	float turnWanted = 0;

	if(mouseControl)
	{
		// last input from mouse - use mouse controls
		// _mouseTurnWanted is difference from current heading

		Vector3 relDir(VMultiply,DirWorldToModel(),_mouseDirWanted);
		float mTurnWanted = atan2(relDir.X(),relDir.Z());

		turnWanted = AngleDifference(curHeading+mTurnWanted,estHeading);
	}
	else
	{
		// note: keys give wanted turning speed, not turning acceleration
		float turnKey = GInput.keyTurnRight-GInput.keyTurnLeft;
		// when moving fast, we want to turn slowly

		float slow = floatMax(0,1-fabs(ModelSpeed().Z())*(1.0f/20));

		float factor = 1 - slow*0.5f;
		turnWanted = AngleDifference(curHeading+ turnKey*factor,estHeading);
		//_thrustLWanted+=GInput.keyTurnLeft-GInput.keyTurnRight;
		//_thrustRWanted-=GInput.keyTurnLeft-GInput.keyTurnRight;
	}


	// special case - tank moving fast and players wants to brake
	// in such situation tank is usually out of control
	// we need to brake first and turn later
	float bFactor = ModelSpeed()[2]*forward;
	//GlobalShowMessage(100,"bFactor %.2f",bFactor);
	if (bFactor<-7) saturate(turnWanted,-0.05f,0.05f);

	_thrustLWanted -= turnWanted*8;
	_thrustRWanted += turnWanted*8;

	// if we are goind forward, no track can go backward
	if( _thrustLWanted+_thrustRWanted>0.01f )
	{
		saturateMax(_thrustLWanted,0);
		saturateMax(_thrustRWanted,0);
	}
	else if ( _thrustLWanted+_thrustRWanted<-0.01f )
	{
		saturateMin(_thrustLWanted,0);
		saturateMin(_thrustRWanted,0);
	}
	Limit(_thrustLWanted,-1,1);
	Limit(_thrustRWanted,-1,1);

	if
	(
		(
			fabs(_thrustLWanted)+fabs(_thrustRWanted)<0.1f ||
			brake || _parkingBrake
		)
		&& fabs(_modelSpeed.Z())<5.0f
	)
	{
		_pilotBrake=true;
	}
	else
	{
		if
		(
			ModelSpeed()[2]*(_thrustLWanted+_thrustRWanted)<-4
		)
		{
			_pilotBrake = true;
		}
		else
		{
			_pilotBrake=false;
		}
		if (fabs(_thrustLWanted)+fabs(_thrustRWanted)>0.1f)
		{
			CancelStop();
			EngineOn();
		}
	}

}

#define DIAG_SPEED 0

#if _ENABLE_AI
void TankWithAI::AIPilot(AIUnit *unit, float deltaT )
{
	// TODO: limit AIPilot simulation rate (10 ps)
	Assert( unit );
	if (!unit) return;
	Assert( unit->GetSubgroup() );
	if (!unit->GetSubgroup()) return;
	AIUnit *leader = unit->GetSubgroup()->Leader();
	bool isLeaderVehicle = leader && leader->GetVehicleIn() == this;

	Vector3Val speed=ModelSpeed();
	
	float headChange=0;
	float speedWanted=0;
	float turnPredict=0;

	if( unit->GetState()==AIUnit::Stopping )
	{
		// special handling of stop state
		if( fabs(speed[2])<1 )
		{
			UpdateStopTimeout();
			unit->SendAnswer(AI::StepCompleted);
		}
		speedWanted=0;
		headChange=0;
	}
	else if( unit->GetState()==AIUnit::Stopped )
	{
		// special handling of stop state
		speedWanted=0;
		headChange=0;
	}
	else if( !isLeaderVehicle )
	{
		FormationPilot(speedWanted,headChange,turnPredict);
	}
	else
	{ // subgroup leader -
		// if we are near the target we have to operate more precisely
		LeaderPilot(speedWanted,headChange,turnPredict);
	}

	#if DIAG_SPEED
	if( this==GWorld->CameraOn() )
	{
		LogF("Pilot %.1f",speedWanted*3.6);
	}
	#endif

	AvoidCollision(deltaT,speedWanted,headChange);


	#if DIAG_SPEED
	if( this==GWorld->CameraOn() )
	{
		LogF("AvoidCollision %.1f",speedWanted*3.6);
	}
	#endif

	float curHeading=atan2(Direction()[0],Direction()[2]);
	float wantedHeading=curHeading+headChange;

	// estimate inertial orientation change
	Matrix3Val orientation=Orientation();
	Matrix3Val derOrientation=_angVelocity.Tilda()*orientation;
	Matrix3Val estOrientation=orientation+derOrientation;
	Vector3Val estDirection=estOrientation.Direction();
	float estHeading=atan2(estDirection[0],estDirection[2]);

	headChange=AngleDifference(wantedHeading,estHeading);

	{
		float maxSpeed=GetType()->GetMaxSpeedMs();
		float limitSpeed=Interpolativ(fabs(turnPredict),H_PI/8,H_PI/4,maxSpeed,3);
		float limitSpeedC=Interpolativ(fabs(headChange),H_PI/8,H_PI/2,maxSpeed,0);
		#if DIAG_SPEED
		if( this==GWorld->CameraOn() )
		{
			LogF("Turn limit %.1f (%.3f, turn %.3f)",limitSpeed,headChange,turnPredict);
		}
		#endif

		saturate(speedWanted,-limitSpeed,+limitSpeed);
		saturate(speedWanted,-limitSpeedC,+limitSpeedC);
	}

	if (fabs(speedWanted)>0.2f) EngineOn();
	if (fabs(headChange)>0.2f) EngineOn();

	Vector3 relAccel=DirectionWorldToModel(_acceleration);
	float changeAccel=(speedWanted-speed.Z())*(1/0.5f)-relAccel.Z();
	// some thrust is needed to keep speed
	float isSlow=1-fabs(speed.Z())*(1.0f/17);
	saturate(isSlow,0.2f,1);

	float isLevel=1-fabs(Direction()[1]*(1.0f/0.6f));
	saturate(isLevel,0.2f,1);
	saturateMax(isSlow,isLevel); // change thrust slowly on steep surfaces
	changeAccel*=isSlow;
	float thrustOld=(_thrustL+_thrustR)*0.5f;
	float thrust=thrustOld+changeAccel*0.33f;
	Limit(thrust,-1,1);

	const float rotCoef=5;
	_thrustLWanted=thrust-headChange*rotCoef;
	_thrustRWanted=thrust+headChange*rotCoef;
	Limit(_thrustLWanted,-1,1);
	Limit(_thrustRWanted,-1,1);

	#if DIAG_SPEED
	if( this==GWorld->CameraOn() )
	{
		LogF("Thrust %.1f L %.1f R %.1f",thrust,_thrustLWanted,_thrustRWanted);
	}
	#endif

	if( fabs(headChange)<0.05f && fabs(speedWanted)<0.5f )
	{
		if( fabs(speed[2])<0.5f ) _thrustLWanted=_thrustRWanted=0;
		_pilotBrake=true;
	}
	else if( fabs(speed[2])<5 && fabs(speedWanted)<0.5f && fabs(headChange)<0.5f )
	{
		_pilotBrake=true;
	}
	else
	{
		_pilotBrake=false;
	}
}

void TankWithAI::AIGunner(AIUnit *unit, float deltaT )
{
	if( _isDead || _isUpsideDown ) return;
	base::AIGunner(unit,deltaT);
}

#endif //_ENABLE_AI

const float MissileUpAngle=0;

float Tank::GetAimed( int weapon, Target *target ) const
{
	return base::GetAimed(weapon,target);
}

float TankWithAI::FireInRange( int weapon, float &timeToAim, const Target &target ) const
{
	timeToAim=0;
	Vector3 relDir=PositionWorldToModel(target.position);
	return FireAngleInRange(weapon,relDir);
}

float TankWithAI::FireAngleInRange( int weapon, Vector3Par rel ) const
{
	// all tanks have turret that can be fully turned around (in Y axis)
	float dist2=rel.SquareSizeXZ();
	float y2=Square(rel.Y());
	// x>0: atan(x)<x
	// atan(y/sqrt(dist2)) < type->_maxGunElev
	// y/sqrt(dist2) < type->_maxGunElev
	// y < type->_maxGunElev * sqrt(dist2)
	// y2 < type->_maxGunElev^2 * dist2
	// we need to have correct signs
	const TurretType &type = Type()->_mainTurret;
	Assert( type._maxElev>=0 );
	Assert( type._minElev<=0 );
	float ret=0;
	if( rel.Y()>=0 )
	{
		// fire up
		if( y2>dist2*Square(type._maxElev) ) return 0;
		ret=rel.Y()*InvSqrt(dist2)*(1/type._maxElev);
	}
	else
	{
		// fire down
		if( y2>dist2*Square(type._minElev) ) return 0;
		ret=rel.Y()*InvSqrt(dist2)*(1/type._minElev);
	}
	return floatMin(1,6-ret*6);
}

void TankWithAI::Simulate( float deltaT, SimulationImportance prec )
{
	PROFILE_SCOPE(simTn);
	// if dammaged or upside down, tank is dead
	_isUpsideDown=DirectionUp().Y()<0.3f;
	_isDead=IsDammageDestroyed();

	if (!SimulateUnits(deltaT))
	{
		EngineOff();
		_pilotBrake = true;
	}
	base::Simulate(deltaT,prec);
}

#pragma warning(disable:4355)

TankWithAI::TankWithAI( VehicleType *name, Person *driver )
:Tank(name,driver)
{
}

TankWithAI::~TankWithAI()
{
}

bool Tank::FireWeapon( int weapon, TargetType *target )
{
	if (GetNetworkManager().IsControlsPaused()) return false;
	if (weapon >= NMagazineSlots() ) return false;
	if( !GetWeaponLoaded(weapon) ) return false;
	if( !IsFireEnabled() ) return false;

	const WeaponModeType *mode = GetWeaponMode(weapon);
	if (!mode || !mode->_ammo) return false;
	bool fired=false;
	switch (mode->_ammo->_simulation )
	{
		case AmmoShotMissile:
		{
			Matrix4Val shootTrans = GunTurretTransform();
			fired=FireMissile
			(
				weapon,
				shootTrans.FastTransform(Type()->_missilePos),
				shootTrans.Rotate(Type()->_missileDir),
				shootTrans.Rotate(Vector3(0,1,20)),
				target
			);
		}
		break;
		case AmmoShotShell:
		{
			Matrix4Val shootTrans = GunTurretTransform();
			Vector3Val pos = shootTrans.FastTransform(Type()->_mainTurret._pos);
			Vector3Val dir = shootTrans.Rotate(Type()->_mainTurret._dir);
			fired=FireShell(weapon,pos,dir,target);

			// add impulse
			Vector3 force=DirectionModelToWorld(-dir*GetMass()*2);
			Vector3 forcePos=DirectionModelToWorld(pos-_shape->CenterOfMass());
			AddImpulseNetAware(force,forcePos.CrossProduct(force));
		}
		break;
		case AmmoShotBullet:
		{
			Matrix4Val shootTrans = GunTurretTransform();
			Vector3 pos;
			Vector3 dir;
			if (GetMagazineSlot(weapon)._weapon->_shotFromTurret)
			{
				pos = shootTrans.FastTransform(Type()->_mainTurret._pos);
				dir = shootTrans.Rotate(Type()->_mainTurret._dir);
			}
			else
			{
				pos = shootTrans.FastTransform(Type()->_gunPos);
				dir = shootTrans.Rotate(Type()->_gunDir);
			}

			fired=FireMGun
			(
				weapon, pos, dir, target
			);
		}
		break;
		case AmmoNone:
		break;
		default:
			Fail("Unknown ammo used.");
		break;
	}
	if( fired )
	{
		VehicleWithAI::FireWeapon(weapon, target);
		return true;
	}
	return false;
}

void Tank::FireWeaponEffects(int weapon, const Magazine *magazine,EntityAI *target)
{
	const MagazineSlot &slot = GetMagazineSlot(weapon);
	if (!magazine || slot._magazine!=magazine) return;

	const WeaponModeType *mode = GetWeaponMode(weapon);
	if (!mode) return;
	if (!mode->_ammo) return;
	
	if (EnableVisualEffects(SimulateVisibleNear)) switch (mode->_ammo->_simulation)
	{
		case AmmoShotMissile:
		case AmmoNone:
		break;
		case AmmoShotShell:
		{
			_gunClouds.Start(1.5f);
			_gunFire.Start(0.1f,1,false);
			const float time=1;
			_fireDustTimeLeft=time;
			_invFireDustTimeTotal=1/time;
		}
		break;
		case AmmoShotBullet:
		{
			if (GetMagazineSlot(weapon)._weapon->_shotFromTurret)
			{
				_gunClouds.Start(0.1f);
			}
			else
			{
				_mGunClouds.Start(0.1f);
				_mGunFire.Start(0.1f,0.4f,true);
				_mGunFireFrames = 1;
				_mGunFireTime = Glob.uiTime;
				int newPhase;
				while ((newPhase = toIntFloor(GRandGen.RandomValue() * 3)) == _mGunFirePhase);
				_mGunFirePhase = newPhase;
			}
		}
		break;
	}

	base::FireWeaponEffects(weapon, magazine,target);
}

/*!
\patch 1.12 Date 8/7/2001 by Ondra.
- New: AT3 (BMP missile) is guided manually (when not locked).
*/

bool Tank::AimWeapon(int weapon, Vector3Par direction)
{
	if (weapon < 0)
	{
		if (NMagazineSlots() <= 0) return false;
		weapon = 0;
	}
	SelectWeapon(weapon);
	// move turret/gun accordingly to direction
	Vector3 relDir(VMultiply,DirWorldToModel(),direction);
	// calculate current gun direction
	// compensate for neutral gun position

	_mainTurret.Aim(Type()->_mainTurret,relDir);

	if (Gunner() && Gunner()->QIsManual())
	{
		// advanced missile control
		// check if last shot is missile
		// check if we have still selected missile
		//const MagazineSlot &slot = GetMagazineSlot(weapon);
		//if (slot._magazine && slot._m
		Missile *missile = dyn_cast<Missile,Vehicle>(_lastShot);
		if (missile)
		{
			// check cursor position
			Vector3 dir = Position()+GetWeaponDirection(weapon)*1500;
			missile->SetControlDirection(dir);
		}
	}

	return true;
}

bool Tank::CalculateAimObserver(Vector3 &dir, Target *target)
{
	Vector3 weaponPos = Type()->_comTurret._pos;
	Vector3 tgtPos=target->LandAimingPosition();
	// predict his and my movement

	weaponPos = ObsGunTurretTransform().FastTransform(Type()->_mainTurret._pos);

	const float minPredTime=0.25f;
	Vector3 myPos=PositionModelToWorld(weaponPos);
	myPos+=Speed()*minPredTime;
	dir = tgtPos-myPos;
	return true;
}

bool Tank::CalculateAimWeapon( int weapon, Vector3 &dir, Target *target )
{
	if (weapon < 0)
	{
		if (NMagazineSlots() <= 0) return false;
		weapon = 0;
	}
	_fire.SetTarget(CommanderUnit(),target);
	const Magazine *magazine = GetMagazineSlot(weapon)._magazine;
	const MagazineType *aInfo = magazine ? magazine->_type : NULL;
	const WeaponModeType *mode = GetWeaponMode(weapon);
	const AmmoType *ammo = mode ? mode->_ammo : NULL;
	Vector3 weaponPos = Type()->_mainTurret._pos;
	Vector3 tgtPos=target->LandAimingPosition();
	// predict his and my movement
	float dist2=tgtPos.Distance2(Position());
	float time2 = 0;
	if (aInfo) time2 = dist2 * Square(aInfo->_invInitSpeed * 1.2f);
	if (ammo) switch (ammo->_simulation)
	{
		case AmmoShotBullet:
			weaponPos = GunTurretTransform().FastTransform(Type()->_gunPos);
		break;
		case AmmoShotMissile:
			weaponPos = GunTurretTransform().FastTransform(Type()->_missilePos);
			time2=0;
		break;
		default:
			if (target->idExact && target->idExact->GetShape())
			{
				if( target->idExact->GetShape()->GeometrySphere()<1.5f )
				{
					// we do not expect direct hit on small target: hit ground instead
					tgtPos[1]=GLOB_LAND->SurfaceY(tgtPos[0],tgtPos[2]);
				}
			}
			weaponPos = GunTurretTransform().FastTransform(Type()->_mainTurret._pos);
		break;
	}
	float time=sqrt(time2);
	const float minPredTime=0.25f;
	float predTime=floatMax(time+0.1f,minPredTime);
	Vector3 myPos=PositionModelToWorld(weaponPos);
	//tgtPos+=target->ObjectSpeed()*predTime;
	myPos+=Speed()*minPredTime;
	float fall=0.5f*G_CONST*time2;
	// calculate balistics
	tgtPos[1]+=fall; // consider balistics
	if( aInfo )
	{
		Vector3 speedEst=target->speed;
		const float maxSpeedEst=aInfo->_maxLeadSpeed;
		if( speedEst.SquareSize()>Square(maxSpeedEst) ) speedEst=speedEst.Normalized()*maxSpeedEst;
		tgtPos+=speedEst*predTime;
	}
	dir = tgtPos-myPos;
	return true;
}

bool Tank::AimWeapon(int weapon, Target *target)
{
	Vector3 dir;
	if (!CalculateAimWeapon(weapon,dir,target)) return false;
	return AimWeapon(weapon,dir);
}

/*!
\patch 1.24 Date 9/26/2001 by Ondra.
- Fixed: Observer commander turret aiming when observer turret
is not mounted on main turret (like BMP).
*/

bool Tank::AimObserver(Vector3Val dir)
{
	// move turret/gun accordingly to direction
	if (Type()->_comTurretOnMainTurret)
	{
		Matrix3 invTurretOrient = TurretTransform().Orientation().InverseRotation();
		Vector3 relDir = invTurretOrient*(DirWorldToModel()*dir);
		// calculate current gun direction
		// compensate for neutral gun position

		_comTurret.Aim(Type()->_comTurret,relDir);
		return true;
	}
	else
	{
		Vector3 relDir = DirWorldToModel()*dir;
		// calculate current gun direction
		// compensate for neutral gun position
		_comTurret.Aim(Type()->_comTurret,relDir);
		return true;
	}
}

/*!
\patch 1.31 Date 11/20/2001 by Ondra.
- Fixed: Tank observer turret direction calculated wrong (since 1.28).
*/

Vector3 Tank::GetEyeDirection() const
{
	if (!Type()->HasCommander())
	{
		return GetWeaponDirection(0);
	}
	return Transform().Rotate(ObsGunTurretTransform().Direction());
}

Vector3 Tank::GetWeaponDirection( int weapon ) const
{
	Vector3 dir = Type()->_mainTurret._dir;
	if (weapon < 0 || weapon >= NMagazineSlots()) return Direction();
	const WeaponModeType *mode = GetWeaponMode(weapon);
	const AmmoType *ammo = mode ? mode->_ammo : NULL;
	if (ammo) switch (ammo->_simulation )
	{
		case AmmoShotMissile:
			dir=Type()->_missileDir;
		break;
		case AmmoShotBullet:
			dir=Type()->_gunDir;
		break;
	}
	return Transform().Rotate(GunTurretTransform().Rotate(dir));
}

Vector3 Tank::GetWeaponDirectionWanted( int weapon ) const
{
	Vector3 dir = Type()->_mainTurret._dir;
	if (weapon < 0 || weapon >= NMagazineSlots()) return Direction();
	const WeaponModeType *mode = GetWeaponMode(weapon);
	const AmmoType *ammo = mode ? mode->_ammo : NULL;
	if (ammo) switch (ammo->_simulation )
	{
		case AmmoShotMissile:
			dir=Type()->_missileDir;
		break;
		case AmmoShotBullet:
			dir=Type()->_gunDir;
		break;
	}
	Matrix3Val aim = _mainTurret.GetAimWanted();
	return Transform().Rotate(aim*dir);
}

Vector3 Tank::GetEyeDirectionWanted() const
{
	Vector3 dir = Direction();
	if (!Type()->HasCommander())
	{
		return GetWeaponDirection(0);
	}
	if (Type()->_comTurretOnMainTurret)
	{
		return Transform().Rotate(_mainTurret.GetAimWanted()*_comTurret.GetAimWanted().Direction());
	}
	return Transform().Rotate(_comTurret.GetAimWanted().Direction());
}

Vector3 Tank::GetWeaponCenter( int weapon ) const
{
	return _mainTurret.GetCenter(Type()->_mainTurret);
}

static const float ObjPenalty1[]={1.0,1.05,1.1,1.4};

static const float ObjPenalty2[]={1.0,1.10,1.25,1.5};

static const float ObjRoadPenalty2[]={1.0,1.05,1.1,1.2};
static const float ObjRoadHardPenalty[]={1.0,1.1,1.2,1.6};

static const float ObjHardPenalty[]={1.0,1.5,2.0,4.0};

const float RoadFaster=1.2;
float TankWithAI::GetFieldCost( const GeographyInfo &info ) const
{
	// road fields are expected to be faster
	// fields with objects will be passed through slower
	int nObj=info.u.howManyObjects;
	int hObj=info.u.howManyHardObjects;
	Assert( nObj<=3 );
	if( info.u.road || info.u.track )
	{
		return (1.0f/RoadFaster)*ObjRoadPenalty2[nObj]*ObjRoadHardPenalty[hObj];
	}
	else
	{
		return ObjPenalty2[nObj]*ObjHardPenalty[hObj];
	}
}

float TankWithAI::GetCost( const GeographyInfo &geogr ) const
{
	float cost=Type()->GetMinCost()*RoadFaster;
	// avoid any water
	//if( geogr.road ) cost=1.0/20; // road speed is 12 m/s 
	if (geogr.u.waterDepth>=2 && !(geogr.u.road || geogr.u.track))
	{
		return ( Type()->_canFloat ? 1.0f/2 : 1e30f ); // in water - speed 2 m/s
	}
	// avoid forests
	if( geogr.u.full ) return 1e30;
	// penalty for objects
	int nObj=geogr.u.howManyObjects;
	Assert( nObj<=3 );
	cost *= ObjPenalty1[nObj];
	// avoid steep hills
	// penalty for hills
	int grad = geogr.u.gradient;
	if( grad>=6 ) return 1e30;
	static const float gradPenalty[6]={1.0,1.0,1.05,1.1,2.0,3.0};
	cost *= gradPenalty[grad];
	// penalty for water
	if( geogr.u.waterDepth==1 && !(geogr.u.road || geogr.u.track)) cost+= ( 1.0f/6 ); // ford speed expected 6 m/s
	return cost;
}

float TankWithAI::GetCostTurn( int difDir ) const
{ // in sec
	if( difDir==0 ) return 0;
	float aDir=fabs(difDir);
	float cost=aDir*0.25f+aDir*aDir*0.04f;
	if( difDir<0 ) return cost*0.8f;
	return cost;
}

float TankWithAI::GetTypeCost(OperItemType type) const
{
	#define COST_BIG 50
	static const float costsSafe[] =
	{
		1.0,               // OITNormal,
		1.0,               // OITAvoidBush,
		3.0,               // OITAvoidTree,
		3.0,               // OITAvoid,
		SET_UNACCESSIBLE,	 // OITWater,
		SET_UNACCESSIBLE,	 // OITSpaceRoad,
		0.5,               // OITRoad
		1.0,               // OITSpaceBush,
    COST_BIG,          // OITSpaceTree,
		SET_UNACCESSIBLE,	 // OITSpace,
		1.0             	 // OITRoadForced,
	};
	static const float costsAware[] =
	{
		1.0,               // OITNormal,
		1.0,               // OITAvoidBush,
		3.0,               // OITAvoidTree,
		3.0,               // OITAvoid,
		SET_UNACCESSIBLE,	 // OITWater,
		SET_UNACCESSIBLE,	 // OITSpaceRoad,
		0.75,              // OITRoad
		1.0,               // OITSpaceBush,
    COST_BIG,          // OITSpaceTree,
		SET_UNACCESSIBLE,	 // OITSpace,
		1.0             	 // OITRoadForced,
	};
	static const float costsCombat[] =
	{
		1.0,               // OITNormal,
		1.0,               // OITAvoidBush,
		1.2,               // OITAvoidTree,
		3.0,               // OITAvoid,
		SET_UNACCESSIBLE,	 // OITWater,
		SET_UNACCESSIBLE,	 // OITSpaceRoad,
		1.0,               // OITRoad
		1.0,               // OITSpaceBush,
    1.5,               // OITSpaceTree,
		SET_UNACCESSIBLE,	 // OITSpace,
		1.0             	 // OITRoadForced,
	};
	static const float costsStealth[] =
	{
		1.0,               // OITNormal,
		1.0,               // OITAvoidBush,
		3.0,               // OITAvoidTree,
		3.0,               // OITAvoid,
		SET_UNACCESSIBLE,	 // OITWater,
		SET_UNACCESSIBLE,	 // OITSpaceRoad,
		1.2,               // OITRoad
		1.0,               // OITSpaceBush,
    SET_UNACCESSIBLE,  // OITSpaceTree,
		SET_UNACCESSIBLE,	 // OITSpace,
		1.0             	 // OITRoadForced,
	};
	Assert(sizeof(costsSafe)/sizeof(*costsSafe)==NOperItemType);
	Assert(sizeof(costsAware)/sizeof(*costsAware)==NOperItemType);
	Assert(sizeof(costsCombat)/sizeof(*costsCombat)==NOperItemType);
	Assert(sizeof(costsStealth)/sizeof(*costsStealth)==NOperItemType);
	static const float *costs[] =
	{
		costsSafe, costsSafe, costsAware, costsCombat, costsStealth
	};

	if (type==OITWater && Type()->_canFloat)
	{
		return 1;
	}
	CombatMode cmode = CMCombat;
	AIUnit *unit = PilotUnit();
	if (unit)
	{
		cmode = unit->GetCombatMode();
		Assert(cmode >= CMCareless && cmode <= CMStealth);
	}
	return costs[cmode - CMCareless][type];
}

NetworkMessageType TankWithAI::GetNMType(NetworkMessageClass cls) const
{
	switch (cls)
	{
	case NMCUpdateGeneric:
		return NMTUpdateTank;
	case NMCUpdatePosition:
		return NMTUpdatePositionTank;
	default:
		return base::GetNMType(cls);
	}
}

class IndicesUpdateTank : public IndicesUpdateTankOrCar
{
	typedef IndicesUpdateTankOrCar base;

public:
	IndicesUpdateTank();
	NetworkMessageIndices *Clone() const {return new IndicesUpdateTank;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesUpdateTank::IndicesUpdateTank()
{
}

void IndicesUpdateTank::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);
}

NetworkMessageIndices *GetIndicesUpdateTank() {return new IndicesUpdateTank();}

class IndicesUpdatePositionTank : public IndicesUpdatePositionVehicle
{
	typedef IndicesUpdatePositionVehicle base;

public:
	int mainTurret;
	int comTurret;
	int rpmWanted;
	int thrustLWanted;
	int thrustRWanted;

	IndicesUpdatePositionTank();
	NetworkMessageIndices *Clone() const {return new IndicesUpdatePositionTank;}
	void Scan(NetworkMessageFormatBase *format);
};

IndicesUpdatePositionTank::IndicesUpdatePositionTank()
{
	mainTurret = -1;
	comTurret = -1;
	rpmWanted = -1;
	thrustLWanted = -1;
	thrustRWanted = -1;
}

void IndicesUpdatePositionTank::Scan(NetworkMessageFormatBase *format)
{
	base::Scan(format);

	SCAN(mainTurret)
	SCAN(comTurret)
	SCAN(rpmWanted)
	SCAN(thrustLWanted)
	SCAN(thrustRWanted)
}

NetworkMessageIndices *GetIndicesUpdatePositionTank() {return new IndicesUpdatePositionTank();}

NetworkMessageFormat &TankWithAI::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	switch (cls)
	{
	case NMCUpdateGeneric:
		base::CreateFormat(cls, format);
//		float _randFrequency;
//		bool _doGearSound:1;
//		float _servoVol;
//		float _phaseL,_phaseR; // texture animation
		break;
	case NMCUpdatePosition:
		base::CreateFormat(cls, format);
		format.Add("mainTurret", NDTObject, NCTNone, DEFVALUE_MSG(NMTUpdateTurret), DOC_MSG("Main turret object"), ET_ABS_DIF, 1);
		format.Add("comTurret", NDTObject, NCTNone, DEFVALUE_MSG(NMTUpdateTurret), DOC_MSG("Commander turret object"), ET_ABS_DIF, 1);
		format.Add("rpmWanted", NDTFloat, NCTFloat0To2, DEFVALUE(float, 0), DOC_MSG("Wanted value of RPM"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);
		format.Add("thrustLWanted", NDTFloat, NCTFloatM1ToP1, DEFVALUE(float, 0), DOC_MSG("Wanted left thrust"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);
		format.Add("thrustRWanted", NDTFloat, NCTFloatM1ToP1, DEFVALUE(float, 0), DOC_MSG("Wanted right thrust"), ET_ABS_DIF, ERR_COEF_VALUE_MAJOR);
		break;
	default:
		base::CreateFormat(cls, format);
		break;
	}
	return format;
}

TMError TankWithAI::TransferMsg(NetworkMessageContext &ctx)
{
	switch (ctx.GetClass())
	{
	case NMCUpdateGeneric:
		TMCHECK(base::TransferMsg(ctx))
		{
			//Assert(dynamic_cast<const IndicesUpdateTank *>(ctx.GetIndices()))
			//const IndicesUpdateTank *indices = static_cast<const IndicesUpdateTank *>(ctx.GetIndices());
		}
		break;
	case NMCUpdatePosition:
		{
			Assert(dynamic_cast<const IndicesUpdatePositionTank *>(ctx.GetIndices()))
			const IndicesUpdatePositionTank *indices = static_cast<const IndicesUpdatePositionTank *>(ctx.GetIndices());

			Matrix3 oldTrans = Orientation();
			Matrix3 oldTurretTrans = Orientation()*TurretTransform().Orientation();
			TMCHECK(base::TransferMsg(ctx))
			if (ctx.IsSending() || !(GunnerUnit() && GunnerUnit()->GetPerson()->IsLocal()))
				TMCHECK(ctx.IdxTransferObject(indices->mainTurret, _mainTurret))
			if (ctx.IsSending() || !(ObserverUnit() && ObserverUnit()->GetPerson()->IsLocal()))
				TMCHECK(ctx.IdxTransferObject(indices->comTurret, _comTurret))
			StabilizeTurrets(oldTrans,Orientation(),oldTurretTrans);

			ITRANSF(rpmWanted)
			ITRANSF(thrustLWanted)
			ITRANSF(thrustRWanted)
		}
		break;
	default:
		TMCHECK(base::TransferMsg(ctx))
		break;
	}
	return TMOK;
}

float TankWithAI::CalculateError(NetworkMessageContext &ctx)
{
	float error = 0;
	switch (ctx.GetClass())
	{
	case NMCUpdateGeneric:
		error += 	base::CalculateError(ctx);
		{
			//Assert(dynamic_cast<const IndicesUpdateTank *>(ctx.GetIndices()))
			//const IndicesUpdateTank *indices = static_cast<const IndicesUpdateTank *>(ctx.GetIndices());
		
		}
		break;
	case NMCUpdatePosition:
		{
			error += 	base::CalculateError(ctx);

			Assert(dynamic_cast<const IndicesUpdatePositionTank *>(ctx.GetIndices()))
			const IndicesUpdatePositionTank *indices = static_cast<const IndicesUpdatePositionTank *>(ctx.GetIndices());

			int index = indices->mainTurret;
			if (index >= 0)
			{
				NetworkMessageFormatBase *format = const_cast<NetworkMessageFormatBase *>(ctx.GetFormat());
				NetworkMessageFormatItem &item = format->GetItem(index);
				CHECK_ASSIGN(typeVal,item.defValue,const RefNetworkDataTyped<int>);
				int type = typeVal.GetVal();
				//int type = static_cast< NetworkDataTyped<int> *>(item.defValue.GetRef())->value;
				NetworkMessageFormatBase *subformat = ctx.GetComponent()->GetFormat((NetworkMessageType)type);
				if (subformat)
				{
					const RefNetworkData &val = ctx.GetMessage()->values[index];
					CHECK_ASSIGN(msgVal,val,const RefNetworkDataTyped<NetworkMessage>);
					NetworkMessage &submsg =msgVal.GetVal();
					//NetworkMessage &submsg = static_cast< NetworkDataTyped<NetworkMessage> *>(val)->value;
					NetworkMessageContext subctx(&submsg, subformat, ctx);
					error += _mainTurret.CalculateError(subctx);
				}
			}
			index = indices->comTurret;
			if (index >= 0)
			{
				NetworkMessageFormatBase *format = const_cast<NetworkMessageFormatBase *>(ctx.GetFormat());
				NetworkMessageFormatItem &item = format->GetItem(index);
				CHECK_ASSIGN(typeVal,item.defValue,const RefNetworkDataTyped<int>);
				int type = typeVal.GetVal();
				//int type = static_cast< NetworkDataTyped<int> *>(item.defValue.GetRef())->value;
				NetworkMessageFormatBase *subformat = ctx.GetComponent()->GetFormat((NetworkMessageType)type);
				if (subformat)
				{
					const RefNetworkData &val = ctx.GetMessage()->values[index];
					CHECK_ASSIGN(msgVal,val,const RefNetworkDataTyped<NetworkMessage>);
					NetworkMessage &submsg =msgVal.GetVal();
					//NetworkData *val = ctx.GetMessage()->values[index];
					//NetworkMessage &submsg = static_cast< NetworkDataTyped<NetworkMessage> *>(val)->value;
					NetworkMessageContext subctx(&submsg, subformat, ctx);
					error += _comTurret.CalculateError(subctx);
				}
			}
			ICALCERR_ABSDIF(float, rpmWanted, ERR_COEF_VALUE_MAJOR)
			ICALCERR_ABSDIF(float, thrustLWanted, ERR_COEF_VALUE_MAJOR)
			ICALCERR_ABSDIF(float, thrustRWanted, ERR_COEF_VALUE_MAJOR)
		}
		break;
	default:
		error += 	base::CalculateError(ctx);
		break;
	}
	return error;
}

