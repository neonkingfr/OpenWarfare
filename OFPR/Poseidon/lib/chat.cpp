#include "wpch.hpp"
#include "chat.hpp"
#include "uiControls.hpp"
#include "world.hpp"
#include "network.hpp"
#include "resincl.hpp"
#include "ai.hpp"
#include "transport.hpp"
#include "person.hpp"
#include "landscape.hpp"

#include "keyInput.hpp"
#include "keyLights.hpp"
#include "dikCodes.h"

#include "stringtableExt.hpp"
#include "mbcs.hpp"

/*!
\file
Implementation file for chat processing - chat edit, chat (radio message) list
*/

//! maximal total number of chat messages in history
#define CHAT_ITEMS					100
//! default number of displayed messages (rows)
#define CHAT_ITEMS_VISIBLE	4

RString GetFullPlayerName()
{
	const PlayerIdentity *identity = GetNetworkManager().FindIdentity(GetNetworkManager().GetPlayer());
	if (identity)
		return identity->GetName();
	else
		return Glob.header.playerName;
}

RString GetFullPlayerName(Person *person)
{
	int player = person->GetRemotePlayer();
	if (player == 1) return person->GetInfo()._name;
	
	const PlayerIdentity *identity = GetNetworkManager().FindIdentity(player);
	if (identity)
		return identity->GetName();
	else
		return person->GetInfo()._name;
}

ChatList::ChatList()
{
	SetRows(CHAT_ITEMS_VISIBLE);
	SetRect(0.05, 0.02, 0.9, 0.02);

	_font = NULL;
	_size = 0;
	_bgColor = PackedColor(Color(0, 0, 0, 0.8));
	_colors[CCGlobal] = PackedColor(Color(0.8, 0.8, 0.8, 1));
	_colors[CCSide] = PackedColor(Color(0, 0.9, 0.9, 1));
	_colors[CCGroup] = PackedColor(Color(0.1, 0.9, 0.2, 1));
	_colors[CCVehicle] = PackedColor(Color(0.9, 0.8, 0, 1));
	_colors[CCDirect] = PackedColor(Color(0.9, 0, 0.8, 1));
	_enable = true;

	_offset = -1;
}

void ChatList::BrowseUp()
{
	if (_offset < Size() - 1) _offset++;
}

void ChatList::BrowseDown()
{
	if (_offset > 0) _offset--;
}

void ChatList::BrowseReset()
{
	_offset = -1;
}

void ChatList::SetRect(float x, float y, float w, float h)
{
	_x = x; _y = y; _w = w; _h = h;
}
	
void ChatList::Add(ChatChannel channel, RString from, RString text, bool playerMsg, bool forceDisplay)
{
	if (channel == CCDirect)
	{
		GWorld->SetTitleEffect(CreateTitleEffect(TitPlainDown ,text));
		return;
	}

	while (Size() >= CHAT_ITEMS)
	{
		Delete(CHAT_ITEMS - 1);
	}
	Insert(0);
	Set(0).channel = channel;
	Set(0).from = from;
	Set(0).time = Glob.clock;
	Set(0).text = text;
	Set(0).uiTime = Glob.uiTime;
	Set(0).playerMsg = playerMsg;
	Set(0).forceDisplay = forceDisplay;

	if (_offset > 0) _offset++;
}

void ChatList::Add(ChatChannel channel, AIUnit *sender, RString text, bool playerMsg, bool forceDisplay)
{
	char from[256];
	from[0] = 0;
	if (sender)
	{
		Person *person = sender->GetPerson();
		Assert(person);

		switch (channel)
		{
		case CCGlobal:
			{
				AIGroup *grp = sender->GetGroup();
				if (!grp) break;
				AICenter *center = grp->GetCenter();
				if (!center) break;
				switch (center->GetSide())
				{
				case TWest:
					strcpy(from, LocalizeString(IDS_WEST)); break;
				case TEast:
					strcpy(from, LocalizeString(IDS_EAST)); break;
				case TGuerrila:
					strcpy(from, LocalizeString(IDS_GUERRILA)); break;
				case TCivilian:
					strcpy(from, LocalizeString(IDS_CIVILIAN)); break;
				}
			}
			break;
		case CCSide:
			{
				AIGroup *grp = sender->GetGroup();
				if (!grp) break;
				sprintf(from, "%s %d", (const char *)grp->GetName(), sender->ID());
			}
			break;
		case CCGroup:
			sprintf(from, "%d", sender->ID());
			break;
		case CCVehicle:
			{
				Transport *veh = sender->GetVehicleIn();
				if (!veh) break;
				if (person == veh->Commander())
					strcpy(from, LocalizeString(IDS_COMMANDER));
				else if (person == veh->Driver())
				{
					if (veh->GetType()->IsKindOf(GWorld->Preloaded(VTypeAir)))
						strcpy(from, LocalizeString(IDS_PILOT));
					else
						strcpy(from, LocalizeString(IDS_DRIVER));
				}
				else if (person == veh->Gunner())
					strcpy(from, LocalizeString(IDS_GUNNER));
				else if (sender == veh->CommanderUnit())
					strcpy(from, LocalizeString(IDS_COMMANDER));
				else
					LogF("Warning: unknown position");
			}
			break;
		case CCDirect:
			GWorld->SetTitleEffect(CreateTitleEffect(TitPlainDown ,text));
			return;
		}
		
		if (person && person->IsNetworkPlayer())
		{
			if (from[0])
			{
/*
				strcat(from, " (");
				strcat(from, person->GetInfo()._name);
				strcat(from, ")");
*/
				strcat(from, " (");
				strcat(from, GetFullPlayerName(person));
				strcat(from, ")");
			}
			else
			{
/*
				strcpy(from, person->GetInfo()._name);
*/
				strcpy(from, GetFullPlayerName(person));
			}
		}
	}
	Add(channel, from, text, playerMsg, forceDisplay);
}

void ChatList::OnDraw()
{
	if (_rows <= 0) return;

	const float startDim = 25, endDim = 30;
	int w = GEngine->Width2D();
	int h = GEngine->Height2D();

	if (!_font)
	{
#if _CZECH
		_font = GEngine->LoadFont(GetFontID("CZERTB24"));
#elif _RUSSIAN
		_font = GEngine->LoadFont(GetFontID("RUSB24"));
#else
		_font = GEngine->LoadFont(GetFontID("tahomaB24"));
#endif
		Assert(_font);
//		_size = _font->Height();
		_size = 0.02;
	}

	float top = _y + (_rows - 1) * _h;
	int n = Size();
	if (n == 0) return;
	int row = _rows - 1;
	int begin = _offset;
	saturate(begin, 0, n - 1);
	for (int i=begin; i<n; i++)
	{
		const ChatItem &item = Get(i);
		if (item.text.GetLength() == 0) continue;
		if (!_enable && !item.forceDisplay) continue; 
		float age = Glob.uiTime - item.uiTime;
		float alpha = 1;
		if (_offset == -1 && age > startDim)
		{
			// interpolate dim
			if (age > endDim) continue;
			alpha = (endDim - age) * (1.0 / (endDim - startDim));
		}
/*
		char buffer[1024];
		if (item.from.GetLength() == 0)
			strcpy(buffer, item.text);
		else
			sprintf
			(
				buffer, "%s: \"%s\"",
				(const char *)item.from,
				(const char *)item.text
			);
*/
		RString buffer;
		if (item.from.GetLength() == 0)
			buffer = item.text;
		else
			buffer = item.from + RString(": \"") + item.text + RString("\"");
		PackedColor color,colorB;
		color = _colors[item.channel];
		color = PackedColorRGB(color, toIntFloor(color.A8() * alpha));
		colorB = PackedColorRGB(_bgColor, toIntFloor(_bgColor.A8() * alpha));

		if (item.playerMsg)
		{
			// make background less intensive
			colorB = PackedColorRGB(color, color.A8()/2);
			// make foreground more intensive
			color = PackedColor(Color(0, 0, 0, alpha));
		}

		AUTO_STATIC_ARRAY(int, lines, 32);
		lines.Add(0);

		if (GEngine->GetTextWidth(_size, _font, buffer) > _w)
		{
			const char *p = buffer;
			const char *word = NULL;
			float width = 0;
			while (*p != 0)
			{
				const char *q = p;
				char c = *p++;
				if ((unsigned char)c <= 32)
				{
					word = p;
				}
				if ((c & 0x80) && _font->GetLangID() == Korean && *p != 0)
				{
					// TODO: simplify
					char temp[3]; temp[0] = c; temp[1] = *p++; temp[2] = 0;
					width += GEngine->GetTextWidth(_size, _font, temp);
				}
				else
				{
					// TODO: simplify
					char temp[2]; temp[0] = c; temp[1] = 0;
					width += GEngine->GetTextWidth(_size, _font, temp);
				}
				if (width > _w)
				{
					if (word) p = word;
					else p = q;
					lines.Add(p - buffer);
					word = NULL;
					width = 0;
				}
			}
		}

		for (int i=lines.Size()-1; i>=0; i--)
		{
			char *line = buffer.MutableData() + lines[i];

			float wline = GEngine->GetTextWidth(_size, _font, line);
			MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
			GEngine->Draw2D(mip, colorB, Rect2DPixel(_x * w, top * h, wline * w, _h * h));
			GEngine->DrawText
			(
				Point2DFloat(_x, top), _size,
				Rect2DFloat(_x, top, _w, _h),
				_font, color, line
			);
			*line = 0;

			row--;
			if (row < 0) return;
			top -= _h;
		}
	}
}

static void WhatUnitsVehicle(RefArray<NetworkObject> &units, Transport *veh)
{
	Person *person = veh->Driver();
	if (person && person->IsRemotePlayer()) units.Add(person);
	person = veh->Commander();
	if (person && person->IsRemotePlayer()) units.Add(person);
	person = veh->Gunner();
	if (person && person->IsRemotePlayer()) units.Add(person);
}

static void WhatUnitsGroup(RefArray<NetworkObject> &units, AIGroup *grp)
{
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = grp->UnitWithID(i + 1);
		if (!unit) continue;
		Person *person = unit->GetPerson();
		if (person && person->IsRemotePlayer()) units.Add(person);
	}
}

static void WhatUnitsSide(RefArray<NetworkObject> &units, AICenter *center)
{
	for (int j=0; j<center->NGroups(); j++)
	{
		AIGroup *grp = center->GetGroup(j);
		if (!grp) continue;
		for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
		{
			AIUnit *unit = grp->UnitWithID(i + 1);
			if (!unit) continue;
			Person *person = unit->GetPerson();
			if (person && person->IsRemotePlayer()) units.Add(person);
		}
	}
}

static void WhatUnitsDirect(RefArray<NetworkObject> &units, Person *person)
{
	const float maxDist = 20.0;

	Vector3Val pos = person->Position();
	int xMin, xMax, zMin, zMax;
	ObjRadiusRectangle(xMin, xMax, zMin, zMax, pos, pos, maxDist);

	for (int x=xMin; x<=xMax; x++)
		for (int z=zMin; z<=zMax; z++)
		{
			if (!InRange(x, z)) continue;

			const ObjectList &list = GLandscape->GetObjects(z, x);
			int n = list.Size();
			for (int i=0; i<n; i++)
			{
				Object *obj = list[i];
				if (!obj) continue;
				if (obj->GetType() != TypeVehicle) continue;

				EntityAI *veh = dyn_cast<EntityAI>(obj);
				if (!veh) continue;
				if (veh->IsDammageDestroyed()) continue;

				AIUnit *unit = veh->CommanderUnit();
				if (!unit) continue;
				if (!unit->GetPerson()->IsRemotePlayer()) continue;
				if (!unit->IsFreeSoldier()) continue;
				Assert(unit->GetPerson() == veh);

				float dist2 = pos.Distance2(obj->Position());
				if (dist2 <= Square(maxDist)) units.Add(veh);
			}
		} // (for all near objects)

}

/*
static void WhatUnitsAll(RefArray<NetworkObject> &units)
{
	AICenter *center = GWorld->GetEastCenter();
	if (center) WhatUnitsSide(units, center);
	center = GWorld->GetWestCenter();
	if (center) WhatUnitsSide(units, center);
	center = GWorld->GetGuerrilaCenter();
	if (center) WhatUnitsSide(units, center);
	center = GWorld->GetCivilianCenter();
	if (center) WhatUnitsSide(units, center);
}
*/

//! Retrieves list of players, which can receive given message
/*!
	\param units retrieved list of units
	\param channel channel on which communication runs
	\param object channel's owner 
*/
void WhatUnits
(
	RefArray<NetworkObject> &units,
	ChatChannel channel, 
	NetworkObject *object
)
{
	switch (channel)
	{
		case CCVehicle:
			{
				Transport *veh = dynamic_cast<Transport *>(object);
				if (veh) WhatUnitsVehicle(units, veh);
			}
			break;
		case CCGroup:
			{
				AIGroup *grp = dynamic_cast<AIGroup *>(object);
				if (grp) WhatUnitsGroup(units, grp);
			}
			break;
		case CCSide:
			{
				AICenter *center = dynamic_cast<AICenter *>(object);
				if (center) WhatUnitsSide(units, center);
			}
			break;
		case CCGlobal:
/*
			WhatUnitsAll(units);
*/
			break;
		case CCDirect:
		default:
			Fail("Bad radio channel");
			break;
	}
}

//! Retrieves list of players, which can receive given message
/*!
	\param unit sender unit
	\param units retrieved list of units
	\param channel channel on which communication runs
*/
static void WhatUnits
(
	AIUnit *unit,
	RefArray<NetworkObject> &units,
	ChatChannel channel
)
{
	switch (channel)
	{
		case CCVehicle:
			{
				Transport *veh = unit->GetVehicleIn();
				if (veh) WhatUnitsVehicle(units, veh);
			}
			break;
		case CCGroup:
			{
				AIGroup *grp = unit->GetGroup();
				if (grp) WhatUnitsGroup(units, grp);
			}
			break;
		case CCSide:
			{
				AIGroup *grp = unit->GetGroup();
				if (!grp) break;
				AICenter *center = grp->GetCenter();
				if (center) WhatUnitsSide(units, center);
			}
			break;
		case CCGlobal:
			break;
		case CCDirect:
			{
				if (unit->IsFreeSoldier()) WhatUnitsDirect(units, unit->GetPerson());
			}
			break;
		case CCNone:
			break;
		default:
			Fail("Bad radio channel");
			break;
	}
}

//! Retrieves list of players, which can receive given message
/*!
	Assume player is a message sender.
	\param units retrieved list of units
	\param channel channel on which communication runs
*/
static void WhatUnits
(
	RefArray<NetworkObject> &units,
	ChatChannel channel
)
{
	Person *person = GWorld->GetRealPlayer();
	if (!person) return;
	AIUnit *unit = person->Brain();
	if (!unit) return;

	WhatUnits(unit, units, channel);
}

//! Sends chat message by player
/*!
	\param channel channel on which communication runs
	\text message text
*/
void SendChat(ChatChannel channel, RString text)
{
	if (text.GetLength() > 0 && text[0] == '#')
	{
		GChatList.Add(channel, "", text, false, true);
		GetNetworkManager().ProcessCommand(text);
		return;
	}

	AIUnit *sender = GWorld->GetRealPlayer() ? GWorld->GetRealPlayer()->Brain() : NULL; 

	if (GetNetworkManager().GetGameState() < NGSPlay || !sender)
	{
		// use playerRoles info
		GetNetworkManager().Chat(channel, text);
		GChatList.Add(channel, GetFullPlayerName(), text, false, true);
	}
	else
	{
		// send actual state
		RefArray<NetworkObject> units;
		WhatUnits(units, channel);
		if (channel == CCGlobal || units.Size() > 0)
			GetNetworkManager().Chat(channel, sender, units, text);

		GChatList.Add(channel, sender, text, false, true);
	}
}

//! Sends mission radio message
/*!
	\param channel channel on which communication runs
	\param object channel's owner 
	\param wave name of class with message description
	\param sender sender unit
	\param senderName sender name
*/
void SendRadioChatWave
(
	ChatChannel channel,
	NetworkObject *object,
	RString wave,
	AIUnit *sender, RString senderName
)
{
	if (GetNetworkManager().GetGameState() < NGSPlay) return;
	RefArray<NetworkObject> units;
	WhatUnits(units, channel, object);
	if (channel == CCGlobal || units.Size() > 0)
		GetNetworkManager().RadioChatWave(channel, units, wave, sender, senderName);
}

//! Sends mission radio message
/*!
	\param channel channel on which communication runs
	\param wave name of class with message description
	\param sender sender unit
	\param senderName sender name
*/
void SendRadioChatWave
(
	ChatChannel channel,
	RString wave,
	AIUnit *sender, RString senderName
)
{
	if (GetNetworkManager().GetGameState() < NGSPlay) return;
	RefArray<NetworkObject> units;
	WhatUnits(sender, units, channel);
	if (channel == CCGlobal || units.Size() > 0)
		GetNetworkManager().RadioChatWave(channel, units, wave, sender, senderName);
}

//! Sends radio message
/*!
	\param channel channel on which communication runs
	\param sender sender unit
	\param object channel's owner 
	\param text text of message
	\param sentence radio sentence of message
*/
void SendRadioChat
(
	ChatChannel channel, AIUnit *sender,
	NetworkObject *object,
	RString text, RadioSentence &sentence
)
{
	if (GetNetworkManager().GetGameState() < NGSPlay) return;
	RefArray<NetworkObject> units;
	WhatUnits(units, channel, object);
	if (channel == CCGlobal || units.Size() > 0)
		GetNetworkManager().RadioChat(channel, sender, units, text, sentence);
}

//! Sends marker
/*!
	\param channel channel on which communication runs
	\param sender sender unit
	\param marker marker description
*/
void SendMarker
(
	ChatChannel channel, AIUnit *sender, ArcadeMarkerInfo &marker
)
{
	if (GetNetworkManager().GetGameState() < NGSBriefing) return;
	RefArray<NetworkObject> units;
	WhatUnits(units, channel);
	if (channel == CCGlobal || units.Size() > 0)
		GetNetworkManager().MarkerCreate(channel, sender, units, marker);
}

//! Channel selection display
class DisplayChannel : public Display
{
public:
	//! actual selected channel
	static int _channel;

public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayChannel(ControlsContainer *parent);
	void OnButtonClicked(int idc) {}
	void ResetHUD();
};

int DisplayChannel::_channel = CCGlobal;

//! Creates channel selection display
AbstractOptionsUI *CreateChannelUI()
{
	return new DisplayChannel(NULL);
}

//! returns currently selected chat channel
ChatChannel ActualChatChannel()
{
	return (ChatChannel)DisplayChannel::_channel;
}

//! Select chat channel
void SetChatChannel(ChatChannel channel)
{
	DisplayChannel::_channel = channel;
}

//! Select next chat channel
void NextChatChannel()
{
	DisplayChannel::_channel++;
	if (DisplayChannel::_channel >= CCN) DisplayChannel::_channel = 0;
}

//! Select previous chat channel
void PrevChatChannel()
{
	DisplayChannel::_channel--;
	if (DisplayChannel::_channel < 0) DisplayChannel::_channel = CCN - 1;
}

DisplayChannel::DisplayChannel(ControlsContainer *parent)
:Display(parent)
{
	Load("RscDisplayChannel");
	SetCursor(NULL);
	ResetHUD();
}

void DisplayChannel::ResetHUD()
{
	CStatic *text = dynamic_cast<CStatic *>(GetCtrl(IDC_CHANNEL));
	if (text)
	{
		text->SetText(LocalizeString(IDS_CHANNEL_GLOBAL + _channel));
		text->SetFtColor(GChatList.GetColor(_channel));
	}
}

//! Chat line display
/*!
	Enables write chat messages
*/
class DisplayChatLine : public Display
{
public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayChatLine(ControlsContainer *parent);
	void OnButtonClicked(int idc);
	bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	bool OnKeyUp(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	void Destroy();
	void DestroyHUD(int exit);
};

DisplayChatLine::DisplayChatLine(ControlsContainer *parent)
:Display(parent)
{
	Load("RscDisplayChat");
	SetCursor(NULL);
}

void DisplayChatLine::OnButtonClicked(int idc)
{
	if (idc == IDC_OK)
	{
		CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_CHAT));
		if (edit)
		{
			SendChat(ActualChatChannel(), edit->GetText());
		}
	}
	Display::OnButtonClicked(idc);
	if (_exit >= 0 && !_destroyed)
	{
		if (CanDestroy())
		{
			Destroy();
		}
		else
			_exit = -1;
	}
}

//! Global chat history
ChatList GChatList;

/*!
\patch 1.42 Date 1/10/2002 by Jirka
- Added: browsing in chat history using Page Up and Page Down in chat line 
*/

bool DisplayChatLine::OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	// catch RETURN, ESC, UP, DOWN, PAGE UP, PAGE DOWN (avoid transmit into game)
	if
	(
		nChar == 0x0D || nChar == 0x1B || nChar == 0x26 || nChar == 0x28 ||
		nChar == 0x21 || nChar == 0x22
	)
		return true;

	return Display::OnKeyDown(nChar, nRepCnt, nFlags);
}

bool IsPlayerDead();

bool DisplayChatLine::OnKeyUp(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	if (nChar == 0x0D)					// VK_RETURN
	{
		OnButtonClicked(IDC_OK);
		return true;
	}
	else if (nChar == 0x1B)			// VK_ESCAPE
	{
		OnButtonClicked(IDC_CANCEL);
		return true;
	}
	else if (nChar == 0x28)			// VK_DOWN
	{
		if (!IsPlayerDead())
		{
			PrevChatChannel();
			if (GWorld->ChatChannel()) GWorld->ChatChannel()->ResetHUD();
			if (GWorld->VoiceChat()) GWorld->VoiceChat()->ResetHUD();
			GWorld->OnChannelChanged();
		}
		return true;
	}
	else if (nChar == 0x26)			// VK_UP
	{
		if (!IsPlayerDead())
		{
			NextChatChannel();
			if (GWorld->ChatChannel()) GWorld->ChatChannel()->ResetHUD();
			if (GWorld->VoiceChat()) GWorld->VoiceChat()->ResetHUD();
			GWorld->OnChannelChanged();
		}
		return true;
	}
	else if (nChar == 0x21)			// VK_PAGE_UP
	{
		GChatList.BrowseUp();
		return true;
	}
	else if (nChar == 0x22)			// VK_PAGE_DOWN
	{
		GChatList.BrowseDown();
		return true;
	}

	return Display::OnKeyUp(nChar, nRepCnt, nFlags);
}

void DisplayChatLine::Destroy()
{
	Display::Destroy();
	DestroyHUD(_exit);
}

void DisplayChatLine::DestroyHUD(int exit)
{
	GWorld->DestroyChat(exit);
	GChatList.BrowseReset();
}

//! Creates chat line display
AbstractOptionsUI *CreateChatUI()
{
	return new DisplayChatLine(NULL);
}

//! Voice chat indicator display
/*!
	Display picture when voice recording is enabled.
	Color of picture indicates channel on which transfer runs.
*/
class DisplayVoiceChat : public Display
{
public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayVoiceChat(ControlsContainer *parent);
	bool OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	bool OnKeyUp(unsigned nChar, unsigned nRepCnt, unsigned nFlags);
	void OnSimulate(EntityAI *vehicle);
	void Destroy();
	void DestroyHUD(int exit);

	void ResetHUD();
};

DisplayVoiceChat::DisplayVoiceChat(ControlsContainer *parent)
:Display(parent)
{
	Load("RscDisplayVoiceChat");
	SetCursor(NULL);

	ResetHUD();
}

bool DisplayVoiceChat::OnKeyDown(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	// catch UP, DOWN (avoid transmit into game)
	/*
	if (nChar == 0x1B || nChar == 0x26 || nChar == 0x28)
		return true;
	else
		return Display::OnKeyDown(nChar, nRepCnt, nFlags);
	*/
	return false;
}

bool DisplayVoiceChat::OnKeyUp(unsigned nChar, unsigned nRepCnt, unsigned nFlags)
{
	/*
	if (nChar == 0x1B)			// VK_ESCAPE
	{
		return true;
	}
	if (nChar == 0x26)			// VK_UP
	{
		SetChannel(_channel - 1);
		return true;
	}
	else if (nChar == 0x28)			// VK_DOWN
	{
		SetChannel(_channel + 1);
		return true;
	}
	else
		return Display::OnKeyUp(nChar, nRepCnt, nFlags);
	*/
	return false;
}

void DisplayVoiceChat::OnSimulate(EntityAI *vehicle)
{
	if (GInput.GetActionToDo(UAVoiceOverNet, true, false) && !_destroyed)
	{
		Destroy();
	}
}

void DisplayVoiceChat::Destroy()
{
	Display::Destroy();
//	KeyLights::SetCapsLock(false);
	GetNetworkManager().SetVoiceChannel(CCNone);
	DestroyHUD(_exit);
}

void DisplayVoiceChat::ResetHUD()
{
	ChatChannel channel = ActualChatChannel();

	if (GetNetworkManager().GetGameState() < NGSPlay)
	{
		// use playerRoles info
		GetNetworkManager().SetVoiceChannel(channel);
	}
	else
	{
		// send actual state
		RefArray<NetworkObject> units;
		WhatUnits(units, (ChatChannel)channel);
		if (channel == CCGlobal || units.Size() > 0)
			GetNetworkManager().SetVoiceChannel(channel, units);
	}

	CStatic *text = dynamic_cast<CStatic *>(GetCtrl(IDC_VOICE_CHAT));
	if (text)
	{
//		text->SetText(LocalizeString(IDS_CHANNEL_GLOBAL + channel));
		text->SetFtColor(GChatList.GetColor(channel));
	}
}

void DisplayVoiceChat::DestroyHUD(int exit)
{
	GWorld->DestroyVoiceChat(exit);
}

AbstractOptionsUI *CreateVoiceChatUI()
{
	return new DisplayVoiceChat(NULL);
}
