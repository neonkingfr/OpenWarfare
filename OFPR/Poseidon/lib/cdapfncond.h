/*!
\file
Conditional include of Macrovision CD protection macros
Dummy macros are selected when _MACRO_CDP is not defined
or if _MACRO_CDP is defined to zero.
*/

#if _MACRO_CDP

#include "cdaPfn.h"

#else

#define CDAPFN_DECLARE_GLOBAL(FnName, MaxOverhead, Constraints)
#define CDAPFN_ENDMARK(FnName)

#endif

