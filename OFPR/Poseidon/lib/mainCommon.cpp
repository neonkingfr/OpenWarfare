#include "wpch.hpp"
#include <Es/Common/win.h>

#include "global.hpp"
#include <El/ParamFile/paramFile.hpp>
#include "paramArchive.hpp"
//#include "loadStream.hpp"

//#include "strIncl.hpp"
#include "stringtableExt.hpp"

#include <time.h>

/*
#include "keyInput.hpp"
#include "world.hpp"
#include "paramFile.hpp"
#include <mmsystem.h>
#include "joystick.hpp"
*/


bool NoGlideDeactivate=false;
bool NoLandscape=false;
bool EnablePIII = false;
bool ObjViewer=false;

int MaxGroups;

HWND       hwndApp;

ParamFile Pars;
ParamFile ExtParsCampaign;
ParamFile ExtParsMission;
ParamFile Res;

LSError Clock::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("timeInYear",_timeInYear,1));
	CHECK(ar.Serialize("changeTime",_changeTime,1));
	CHECK(ar.Serialize("timeOfDay",_timeOfDay,1));
	return LSOK;
}

bool Clock::AdvanceTime( float deltaT )
{
	// timeD is in days
	_changeTime+=deltaT;
	bool ret = fabs(_changeTime) > 20 * OneSecond;
	if (ret)
	{
		float oldTime=_timeInYear;
		_timeInYear+=_changeTime;
		_changeTime-=(_timeInYear-oldTime);
	}
	//_timeOfDay = fastFmod(_timeInYear + _changeTime, OneDay) * (1.0 / OneDay);
	const float InvOneDay=1.0 / OneDay;
	_timeOfDay = fastFmod(_timeInYear , OneDay) * InvOneDay +_changeTime * InvOneDay;
	// TODO: end of year
	return ret;
}

/*!
\patch 1.44 Date 2/6/2002 by Jirka
- Fixed: Program now properly works with year in date
*/
static const int daysInMonth[]={31,28,31,30,31,30,31,31,30,31,30,31};
int GetDaysInMonth(int year, int month)
{
	if (month == 1 && year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)) return 29;
	else return daysInMonth[month];
}

void Clock::FormatDate(const char *format, char *buffer)
{
	int dayOfYear = toIntFloor(_timeInYear * 365);
	if (_timeOfDay > 1.0) dayOfYear++;
	Assert(dayOfYear >= 0 && dayOfYear < 365);
	struct tm tmDate = { 0, 0, 0, 0, 0, 0 };
	tmDate.tm_year = _year - 1900;
	tmDate.tm_yday = dayOfYear;
	int m = 0;
	while (tmDate.tm_yday >= GetDaysInMonth(_year, m))
	{
		tmDate.tm_yday -= GetDaysInMonth(_year, m);
		m++;
	}
	tmDate.tm_mday = tmDate.tm_yday + 1;
	tmDate.tm_mon = m;
	tmDate.tm_yday = 0;
	mktime(&tmDate);
	Assert(tmDate.tm_yday == dayOfYear);
	const char *ptrsrc = format;
	char *ptrdst = buffer;
	while (*ptrsrc)
	{
		if (*ptrsrc == '%')
		{
			ptrsrc++;
			switch (*ptrsrc)
			{
			case 0:
				break;
			case 'a':
				strcpy(ptrdst, LocalizeString(IDS_SUNDAY + tmDate.tm_wday));
				while (*ptrdst) ptrdst++;
				ptrsrc++;
				break;
			case 'b':
				strcpy(ptrdst, LocalizeString(IDS_JANUARY + tmDate.tm_mon));
				while (*ptrdst) ptrdst++;
				ptrsrc++;
				break;
			case 'd':
                #ifdef _WIN32
				_itoa(tmDate.tm_mday, ptrdst, 10);
				while (*ptrdst) ptrdst++;
                #else
                ptrdst += sprintf(ptrdst,"%d",tmDate.tm_mday);
                #endif
				ptrsrc++;
				break;
			case 'y':
                #ifdef _WIN32
				_itoa(tmDate.tm_year, ptrdst, 10);
				while (*ptrdst) ptrdst++;
                #else
                ptrdst += sprintf(ptrdst,"%d",tmDate.tm_year);
                #endif
				ptrsrc++;
				break;
			default:
				ptrsrc++;
				break;
			}
		}
		else
			*(ptrdst++) = *(ptrsrc++);
	}
	*(ptrdst++) = 0;
}

float Clock::ScanDateTime(const char *date, const char *time, int &year)
{
	int hour=0,min=0;
	int day=0,month=0;
	sscanf(time,"%d:%d",&hour,&min);
	sscanf(date,"%d/%d/%d",&day,&month,&year);
	day--;
	while( --month>0 )
	{
		day += GetDaysInMonth(year, month - 1);
	}
	return hour*OneHour+min*OneMinute+day*OneDay+0.5*OneSecond;
}


