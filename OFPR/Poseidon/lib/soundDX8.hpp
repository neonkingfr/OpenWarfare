// DirectX sound system
// (C) 1996, SUMA
#ifndef _SOUND_DX8_HPP
#define _SOUND_DX8_HPP

#include <Es/Common/win.h>

//#include <dmusici.h>

#if defined _XBOX
	#define IMySoundBuffer IDirectSoundBuffer8
	#define IMySound3DBuffer IDirectSoundBuffer8
	#define IMySound IDirectSound
#elif defined _WIN32
  #ifndef DWORD_PTR
    #define DWORD_PTR DWORD
  #endif
	#include <mmsystem.h>
	#include <dsound.h>
	#include "eax.h"

	#define IMySoundBuffer IDirectSoundBuffer
	#define IMySound3DBuffer IDirectSound3DBuffer
	#define IMySound IDirectSound
#endif

#include "soundsys.hpp"
#include "samples.hpp"

#ifdef PREPROCESS_DOCUMENTATION
#include "networkObject.hpp"
#endif

struct WaveDSCache8
{
	RString _name;

    #ifdef _WIN32

	// Pointer to direct sound buffer.
	ComRef<IMySoundBuffer> _buffer;

	// optional pointer to DS3D interface
	ComRef<IMySound3DBuffer> _buffer3D;

    #endif

	~WaveDSCache8();
};

TypeIsMovableZeroed(WaveDSCache8)

#include <Es/Memory/normalNew.hpp>

class SoundSystem8;

class WaveBuffers8
{
	
	protected:
	Vector3 _position;
	Vector3 _velocity;

	float _volume; // volume used for 3D sound
	float _accomodation; // current ear accomodation setting

	float _volumeAdjust;
	WaveKind _kind;

    #ifdef _WIN32

	// Pointer to direct sound buffer.
	ComRef<IMySoundBuffer> _buffer;
	// optional pointer to DS3D interface
	ComRef<IMySound3DBuffer> _buffer3D;

    #endif

	bool _playing;       // Is this one playing?
	bool _wantPlaying;       // Is this one playing?
	bool _only2DEnabled; // 2D sound
	bool _3DEnabled; // 3D may be temporarily disabled
	bool _enableAccomodation;

	int _volumeSet; // 2D volume set
	DWORD _frequencySet;

	void SetVolumeDb( LONG volumeDb );
	void DoSetPosition( bool immediate, bool maxDistanceFix );

	WaveBuffers8();
	~WaveBuffers8();

	void Reset( SoundSystem8 *sys );

	void UpdateVolume();

	void SetVolume( float vol, bool imm );
	//void SetFrequency( float freq, bool imm );

	bool GetMuted() const;
	float GetVolume() const {return _volume;}
	Vector3 GetPosition() const {return _position;}

	void SetAccomodation( float accom );
	float GetAccomodation() const {return _accomodation;}

	void SetVolumeAdjust( float volEffect, float volSpeech, float volMusic );

	void Set3D( bool is3D );
	bool Get3D() const;

	void EnableAccomodation( bool enable )
	{
		_enableAccomodation=enable;
		if( !enable ) _accomodation=1.0;
	}
	bool AccomodationEnabled() const {return _enableAccomodation;}

	void SetKind( SoundSystem8 * sys, WaveKind kind );
};

// buffer cca 3 seconds of 22khz ahead
//const int StreamingBufferSize = 32*1024*4; // 128 KB buffer
const int StreamingBufferSize = 512*1024; // 512 KB buffer
// should be enough for 2 sec of 44KHz/16b/stereo

const int StreamingBufferParts = 16; // number of subsections
const int StreamingPartSize = StreamingBufferSize/StreamingBufferParts;
// StreamingPartSize must be power of two

class Wave8: public AbstractWave, public WaveBuffers8
{
	friend class SoundSystem8;
	protected:

	SoundSystem8 *_soundSys;

	// basic file information
	
	UINT _size;         // size of data in bytes.
	LONG _frequency;      // sampling rate
	int _nChannel;        // number of channels
	int _sSize;           // number of bytes per sample
	
	float _maxVolume,_avgVolume; // file properties

	bool _terminated;       // Is this one playing?
	bool _loaded;        // Is this one loaded?
	bool _loadError;        // Is this one loaded?
	bool _looping;
	bool _enableAccomodation;

	int _curPosition; // current playing position
	bool _posValid; // 3d position explicitly set

	// streaming audio variables
	Ref<WaveStream> _stream;
	int _streamPos; // current position in stream

	DWORD _writePos; // where in the buffer we have already written data
	DWORD _endPos; // last usefull data went to this position
	bool _lastLoopSet;
	bool _lastLoopVerified;

	public:
	enum Type {Free,Sw,Hw2D,Hw3D,NTypes};

	protected:
	Type _type;	
	static int _counters[NTypes];

	void AddStats( const char *name );
	static void ReportStats();

	public:
	static int TotalAllocated();
	static int Allocated( Type i ) {return _counters[i];}
		
	protected:

	void DoConstruct();
	
	public:

	void LoadHeader();
	
	void Load();
	void Unload();

	void DoDestruct();

	//Wave8();
	Wave8( SoundSystem8 *sSys, float delay ); // empty wave
	Wave8( SoundSystem8 *sSys, RString name, bool is3D ); // real wave
	~Wave8();

	bool Duplicate( const Wave8 &wave ); // duplicate buffer

	void CacheStore( WaveDSCache8 &store );
	void CacheLoad( WaveDSCache8 &store );

	private:
	Wave8( const Wave8 &wave ); // duplicate buffer
	void operator = ( const Wave8 &wave ); // duplicate buffer

	public:

	//void Queue(AbstractWave *wave,int repeat=1 );
	void Repeat(int repeat=1 );
	void Restart();
	
	//void AdvanceQueue();
	void Play(); // start playing
	void DoPlay(); // start playing
	void Skip( float deltaT );
	void Advance( float deltaT ); // skip if stopped
	float GetLength() const; // get wave length in sec
	void DoStop(); // stop playing immediatelly
	void Stop(); // stop playing immediatelly
	void LastLoop(); // stop playing on the end of the loop
	void PlayUntilStopValue( float time ); // play until some time
	void SetStopValue( float time ); // play until some time

	WaveKind GetKind() const {return _kind;}
	void SetKind( WaveKind kind ) {WaveBuffers8::SetKind(_soundSys,kind);}

	float GetFileMaxVolume() const {return _maxVolume;}
	float GetFileAvgVolume() const {return _avgVolume;}

	void SetFrequency( float freq, bool imm );

	// set sound parameters
	void SetVolume( float volume, float freq, bool immediate=false );

	void SetPosition( Vector3Par pos, Vector3Par vel, bool immediate=false );

	void SetAccomodation( float accom=1.0 ){WaveBuffers8::SetAccomodation(accom);}
	float GetAccomodation() const {return WaveBuffers8::GetAccomodation();}
	void EnableAccomodation( bool enable ) {WaveBuffers8::EnableAccomodation(enable);}
	bool AccomodationEnabled() const {return WaveBuffers8::AccomodationEnabled();}

	bool Loaded() const {return _loaded;}
	bool IsTerminated();
	bool IsStopped();
	bool IsWaiting();

	// equvalent distance of 2D sounds for position disabled sounds
	// this is necessary for loudness calculation
	float Distance2D() const {return 1;}

	void Set3D( bool is3D );
	bool Get3D() const;

	RString GetDiagText();

	bool IsMuted() const {return WaveBuffers8::GetMuted();}
	float GetVolume() const {return WaveBuffers8::GetVolume();}
	Vector3 GetPosition() const {return WaveBuffers8::GetPosition();}

	private:

	void FillStream();
	void FillStreamInit();


	// universal functions for both streaming / static sounds
	void StoreCurrentPosition();
	void AdvanceCurrentPosition(int skip);
};

class WaveGlobal8: public Wave8
{
	typedef Wave8 base;

	Ref<WaveGlobal8> _queue; // next wave in queue
	bool _queued; // will be started by parent channel

	public:
	//WaveGlobal8();
	~WaveGlobal8();
	WaveGlobal8( SoundSystem8 *sSys, RString name, bool is3D );
	WaveGlobal8( SoundSystem8 *sSys, float delay );

	private:
	WaveGlobal8( const Wave8 &wave ); // duplicate buffer
	void operator = ( const Wave8 &wave ); // duplicate buffer

	public:

	void AdvanceQueue();

	void Play();
	void Skip( float deltaT );
	void Advance( float deltaT );

	bool IsTerminated();

	void Queue(AbstractWave *wave,int repeat=1 );

	USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>


#include <Es/Containers/array.hpp>

#if defined _WIN32 && !defined _XBOX
class AudioMixer;
#endif

class SoundSystem8: public RefCount, public AbstractSoundSystem
{
	friend class Wave8;
	friend class WaveBuffers8;
	
	private:
	#if defined _WIN32 && !defined _XBOX
	SRef<AudioMixer> _mixer;
	#endif

	LinkArray<Wave8> _waves; // stored preloaded waves
	
	AutoArray<WaveDSCache8> _cache;
	int _cacheUniqueBuffers; // count of non-duplicated buffers in cache

	float _waveVol;
	float _musicVol;
	float _speechVol;

	float _speechVolExp;
	float _waveVolExp;
	float _musicVolExp;

	float _volumeAdjustEffect;
	float _volumeAdjustSpeech;
	float _volumeAdjustMusic;

    #ifdef _WIN32

	ComRef<IMySoundBuffer> _primary;

	ComRef<IDirectSound3DListener> _listener;  // Pointer to direct sound buffer.

    #endif

	#if defined _WIN32 && !defined _XBOX
	ComRef<IMySoundBuffer> _eaxFakeListener;
	ComRef<IKsPropertySet> _eaxListener;
	#endif
	SoundEnvironment _eaxEnv;

	//ComRef<IDirectMusicLoader8> _loader;
	//ComRef<IDirectMusicPerformance8> _performance;

    #ifdef _WIN32
	ComRef<IDirectSound> _ds;
	//ComRef<IDirectMusic8> _dm;
    #endif

	Vector3 _listenerPos;
	Vector3 _listenerVel;
	Vector3 _listenerDir;
	Vector3 _listenerUp;
	//Matrix4 _listenerMatrix;
	//Vector3 _listenerSpeed;
	DWORD _minFreq,_maxFreq; // valid freq. range
	int _maxChannels;

	bool _commited; // do not play anything until all is initialized (2nd frame)
	bool _soundReady;
	bool _hwDuplicateWorks;
	bool _hwEnabled,_eaxEnabled;
	bool _canHW,_canEAX;
	
	// preview state variables
	bool _doPreview;
	DWORD _previewTime;

	Ref<AbstractWave> _previewSpeech;
	Ref<AbstractWave> _previewEffect;
	Ref<AbstractWave> _previewMusic;
	bool _enablePreview; // preview disabled until sound initialized

	public:
	void New(HWND win,bool dummy);
	void Delete();
	SoundSystem8();
	SoundSystem8(HWND hwnd, bool dummy);
	~SoundSystem8();

	// return state after enable
	virtual bool EnableHWAccel(bool val);
	virtual bool EnableEAX(bool val);
	// get if feature is enabled
	virtual bool GetEAX() const;
	virtual bool GetHWAccel() const;

	void LoadConfig();
	void SaveConfig();

	bool LoadFromCache( Wave8 *wave );
	void StoreToCache( Wave8 *wave );
	void ReserveCache( int number3D, int number2D );

	void LogChannels( const char *name );

	float GetWaveDuration( const char *filename );
	// create normal or streaming wave
	void AddWaveToList(WaveGlobal8 *wave);
	AbstractWave *CreateWave
	(
		const char *name ,bool is3D=true ,bool prealloc=false
	);
	// create empty wave
	AbstractWave *CreateEmptyWave( float duration ); // duration in ms
	void Activate( bool active );

	virtual void SetEnvironment( const SoundEnvironment &env );

	void DoSetEAXEnvironment();

	bool InitEAX();
	void DeinitEAX();

	float GetCDVolume() const;
	void SetCDVolume(float val);

	void SetWaveVolume(float val );
	float GetWaveVolume();

	void SetSpeechVolume(float val );
	float GetSpeechVolume();

	void UpdateMixer();
	void PreviewMixer();

	void StartPreview();
	void TerminatePreview();

	void FlushBank(QFBank *bank);

	void SetListener
	(
		Vector3Par pos, Vector3Par vel,
		Vector3Par dir, Vector3Par up
	);
	Vector3 GetListenerPosition() const {return _listenerPos;}
	void Commit();

	private:
	float MaxVolume() const;
	float MaxExpVolume() const;

    #ifdef _WIN32
	IDirectSound *DirectSound(){return _ds;} // Wave8 class uses this
    #endif
};

#define SoundSystem SoundSystem8

#endif
