/*********************************************************************/
/*																	 */
/*	Poseidon Messages 												 */
/*									Copyright � Mentar, 1998    	 */
/*																	 */
/*********************************************************************/

// komunika�n� knihovna pro editory Poseidon

#define MSG_MEM_COUNT	500		// max. po�et bufferovan�ch ud�lost�
								// ostatn� jsou pouze odlo�eny !
#define LEN_FILENAME	128
#define LEN_PATHNAME	64
#define LEN_NAME		32

//////////////////////////////////////////////////////////////
// definice ud�lost�

#define SYSTEM_QUIT						1
#define SYSTEM_INIT						2
#define FILE_EXPORT						3
#define FILE_IMPORT						4
#define FILE_TRANSFER					5
#define CURSOR_POSITION_SET				6
#define SELECTION_CLEAR					7
#define SELECTION_OBJECT_ADD			8
#define REGISTER_LANDSCAPE_TEXTURE		9
#define REGISTER_OBJECT_TYPE			10
#define LAND_HEIGHT_CHANGE				11
#define LAND_TEXTURE_CHANGE				12
#define OBJECT_CREATE					13
#define OBJECT_DESTROY					14
#define OBJECT_MOVE						15
#define OBJECT_TYPE_CHANGE				16
#define FILE_IMPORT_BEGIN				17
#define FILE_IMPORT_END				18
#define BLOCK_MOVE_BEGIN				19
#define BLOCK_MOVE_END				20

typedef struct
{
	int		nMsgID;		// identifikace ud�losti

	union
	{
		struct	// Load, Save
		{
			char szFileName[LEN_FILENAME];
		};
		struct	// Transfer
		{
			char szPathFrom[LEN_PATHNAME];
			char szPathTo[LEN_PATHNAME];
		};
		struct	// Working with objects
		{
			int nID;
			char szName[LEN_NAME];
			bool bState;
			float Position[4][4];
		};
		struct	// Working with landscape
		{
			int nX;
			int nZ;
			float Y;
			int nTextureID;
		};
		struct // Init landscape parameters
		{
			int landRangeX,landRangeY;
			float landGridX,landGridY;
		};
	};
}	SPosMessage;

//////////////////////////////////////////////////////////////
// fronta ud�lost�

// stav aplikac�
#define APP_STATE_NOT_READY	0
#define APP_STATE_ACTIVE	1


#ifdef _WIN32
#ifndef _MSG_DLL
#undef	MNT_EXT_CLASS
#define MNT_EXT_CLASS __declspec(dllimport)
#else
#undef	MNT_EXT_CLASS
#define MNT_EXT_CLASS __declspec(dllexport)
#endif
#else
#undef  MNT_EXT_CLASS
#define MNT_EXT_CLASS
#endif

typedef struct SFrontItemTag
{
	SPosMessage		sMsg;
	void*			pNext;
}	SFrontItem;

class MNT_EXT_CLASS CMessageFront
{
public:
				  CMessageFront();
	virtual		  ~CMessageFront();

	// funkce pro server - Poseidon Editor

	// vrac� po�et ud�lost� ve front�
	int  GetServerMsgCount();
	// p�e�te ud�lost Serveru
	BOOL GetServerMessage(SPosMessage&);
	// po�le ud�lost na Clienta
	void PostMessageToClient(SPosMessage&);
	// reset fronty ud�lost� ori client
	void ResetClientMessageFront();

	// vrac� stav aplikace serveru
	int	 GetServerAppState();
	// nastav� stav aplikace serveru
	void SetServerAppState(int nState);

	// funkce pro client - Poseidon View

	// vrac� po�et ud�lost� ve front�
	int	 GetClientMsgCount();
	// p�e�te ud�lost Clienta
	BOOL GetClientMessage(SPosMessage&);
	// po�le ud�lost na Server
	void PostMessageToServer(SPosMessage&);
	// reset fronty ud�lost� pro server
	void ResetServerMessageFront();


	// vrac� stav aplikace clienta
	int	 GetClientAppState();
	// nastav� stav aplikace clienta
	void SetClientAppState(int nState);

	// funkce pro komunikaci mezi aplikacemi
	// funkce by mela b�t vol�na periodicky !!!
	BOOL TryToSendMessages();


public:
	void	AddItem(SPosMessage&,SFrontItem*& pFirst,SFrontItem*& pLast);
	void	RemItem(SFrontItem*& pFirst,SFrontItem*& pLast);

// fronty ud�lost�
	SFrontItem*	m_pFirstToServer;
	SFrontItem*	m_pLastToServer;

	SFrontItem*	m_pFirstToClient;
	SFrontItem*	m_pLastToClient;
};


