#ifdef _MSC_VER
#pragma once
#endif

#ifndef _SEAGULL_HPP
#define _SEAGULL_HPP

#include "vehicle.hpp"
#include "animation.hpp"
#include "lights.hpp"
#include "paramFileExt.hpp"

class SeaGullAuto;

#include "cameraHold.hpp"

//! used for respawning and special effects

class SeaGull: public CameraHolder
{
	typedef CameraHolder base;

	protected:
	Ref<AbstractWave> _sound;
	SoundPars _soundPars;

	float _wingPhase;
	float _wingSpeed;
	float _wingBase; // which kind of animation - landed/winging
	//bool _objectContact;
	//bool _landContact;
	Time _nextCreek;

	// rotor force is wing speed
	float _rpm,_rpmWanted; // landing control
	float _mainRotor,_mainRotorWanted; // main rotor thrust
	float _cyclicForwardWanted;
	float _cyclicAsideWanted; // main rotor thrust direction
	float _wingDive,_wingDiveWanted; // use wings to control forward movement
	float _thrust,_thrustWanted;

	bool _landContact;
		
	public:
	SeaGull();
	~SeaGull();
	
	bool IsAnimated( int level ) const; // appearence changed with Animate
	bool IsAnimatedShadow( int level ) const; // shadow changed with Animate
	void Animate( int level );
	void Deanimate( int level );

	void Simulate( float deltaT, SimulationImportance prec );
	void Sound( bool inside, float deltaT );
	void UnloadSound();

	bool OcclusionFire() const {return false;}
	bool OcclusionView() const {return false;}

	float TrackingSpeed() const {return 100;}
	float OutsideCameraDistance( CameraType camType ) const {return 3;}
	//float OutsideCameraDistance( CameraType camType ) const {return 10;}
	Vector3 ExternalCameraPosition( CameraType camType ) const;

	void QuickStart();

	// no load/save
	bool MustBeSaved() const {return false;}

	USE_CASTING(base)
};

#include <Es/Memory/normalNew.hpp>

//! high level controled - used for respawning and special effects

class SeaGullAuto: public SeaGull
{
	typedef SeaGull base;

	protected:
	Vector3 _mouseDirWanted; // mouse control
	Vector3 _pilotSpeed;
	float _pilotHeading;
	float _pilotHeight;
	float _dirCompensate;  // how much we compensate for estimated change
	bool _pilotHeadingSet;
	// esp. when landing

	// helpers for keyboard control
	bool _pressedForward,_pressedBack; // recognize fast speed-up, fast brake
	bool _pressedUp,_pressedDown;

	//Ref<SeaGullPilot> _pilot;
	Time _lastPilotTime; // avoid calculating pilot too often

	AutopilotState _state;
	
	public:
	SeaGullAuto( void *pilot );

	void Simulate( float deltaT, SimulationImportance prec );
	void KeyboardPilot(AIUnit *unit, float deltaT );
	void JoystickPilot( float deltaT );

	void AvoidGround( float minHeight );
	void Autopilot
	(
		Vector3Par target, Vector3Par tgtSpeed, // target
		Vector3Par direction, Vector3Par speed // wanted values
	);
	void ResetAutopilot();

	//void SetPilot( void *pilot ) {}
	//void *Pilot() const {return _pilot;}

	void Draw( int forceLOD, ClipFlags clipFlags, const FrameBase &pos );
	void DrawDiags();

	void MakeLanded();
	void MakeAirborne( float height );

	bool IsVirtualX( CameraType camType ) const;
	virtual void AimDriver(Vector3Val val);
	
	//! moved from obsolete SeaGullPilotCamera
	virtual void CamControl( float deltaT );

	// CameraHolder implementation
	virtual void Command( RString mode );
	virtual void Commit( float time );

	// Multiplayer support
	NetworkMessageType GetNMType(NetworkMessageClass cls) const;
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContext &ctx);

	USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

#endif

