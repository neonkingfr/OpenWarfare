#include "wpch.hpp"
#include "networkImpl.hpp"
#include "global.hpp"
//#include "strIncl.hpp"
#include "stringtableExt.hpp"

#include "arcadeTemplate.hpp"
#include "ai.hpp"
#include "world.hpp"
#include "landscape.hpp"
#include "chat.hpp"
#include <El/XML/xml.hpp>
#include "strFormat.hpp"

#if defined _WIN32 && !defined _XBOX
  #include <dxerr8.h>
  #pragma comment(lib,"dxerr8")
#endif

#include <Es/Algorithms/qsort.hpp>
#include <Es/Common/filenames.hpp>

#include "allAIVehicles.hpp"
#include "seaGull.hpp"
#include "detector.hpp"
#include "shots.hpp"

#include "debugTrap.hpp"

#include <El/QStream/QBStream.hpp>
#include "packFiles.hpp"
#include "fileServer.hpp"

#include <El/Common/randomGen.hpp>
#include "gameStateExt.hpp"

#include "progress.hpp"
#include <El/Common/perfLog.hpp>

#include "keyInput.hpp"
#include "dikCodes.h"
#include "uiActions.hpp"

#include "crc.hpp"
#include <Es/Strings/bstring.hpp>

#ifdef _WIN32
  #include <io.h>
  #if _ENABLE_MP
    #include <winsock2.h>
  #endif
  #if _ENABLE_DEDICATED_SERVER
    #include <process.h>
  #endif
#endif

/*!
\file
Basic implementation file for network server
*/

template Ref<NetworkObject>;

// DISABLED: random integrity check
#define _ENABLE_RANDOM_INTEGRITY_CHECK 0

#define LOG_SEND_PROCESS 0

#define LOG_PLAYERS	1

extern const char *GameStateNames[];
extern int MaxCustomFileSize;


//! enable diagnostic logs of message errors
#define LOG_ERRORS				0
//! limit for diagnostic logs of message errors
const float LogErrorLimit = 1.0f;

RString GetModList();

DEFINE_FAST_ALLOCATOR(NetworkPlayerObjectInfo)

NetworkPlayerObjectInfo *NetworkObjectInfo::GetPlayerObjectInfo(int player)
{
	for (int i=0; i<playerObjects.Size(); i++)
	{
		NetworkPlayerObjectInfo *info = playerObjects[i];
		if (info->player == player) return info;
	}
	return NULL;
}

NetworkPlayerObjectInfo *NetworkObjectInfo::CreatePlayerObjectInfo(int player)
{
	NetworkPlayerObjectInfo *info = GetPlayerObjectInfo(player);
	if (info) return info;

	int index = playerObjects.Add();
	playerObjects[index] = new NetworkPlayerObjectInfo;
	NetworkPlayerObjectInfo &poInfo = *playerObjects[index];
	poInfo.player = player;
	for (int j=NMCUpdateFirst; j<NMCUpdateN; j++)
	{
		NetworkUpdateInfo &info = poInfo.updates[j];
		info.lastCreatedMsgId = 0xFFFFFFFF;
		info.lastCreatedMsgTime = 0;
	}
	return &poInfo;
}

void NetworkObjectInfo::DeletePlayerObjectInfo(int player)
{
	for (int i=0; i<playerObjects.Size(); i++)
	{
		NetworkPlayerObjectInfo &info = *playerObjects[i];
		if (info.player == player)
		{
			playerObjects.Delete(i);
			return;
		}
	}
}

#if _ENABLE_DEDICATED_SERVER
bool IsDedicatedServer();
int ConsoleF(const char *format, ...);
int ConsoleTitle(const char *format, ...);
#endif
RString GetUserDirectory();

//! Load ban list from file
/*!
\param filename name (path) of file
\param list output ban list
*/
static void LoadBanList(RString filename, FindArray<__int64> &list)
{
	list.Clear();
	QIFStream f;
	f.open(filename);
	while (!f.eof() && !f.fail())
	{
		int c = f.get();
		if (f.eof() || f.fail()) return;
		while (!isdigit(c))
		{
			c = f.get();
			if (f.eof() || f.fail()) return;
		}
		__int64 value = 0;
		while (isdigit(c))
		{
			value *= 10;
			value += c - '0';
			c = f.get();
		}
		list.AddUnique(value);
	}
}

//! Return if given message type is kind of transport update
/*!
When driver of transport changes, ownership of whole transport is changed.
*/
bool IsUpdateTransport(NetworkMessageType type)
{
	switch (type)
	{
	case NMTUpdateTransport:
	case NMTUpdateTankOrCar:
	case NMTUpdateTank:
	case NMTUpdateCar:
	case NMTUpdateMotorcycle:
	case NMTUpdateAirplane:
	case NMTUpdateHelicopter:
	case NMTUpdateParachute:
	case NMTUpdateShip:
		return true;
	default:
		return false;
	}
}

//! Find file bank with given prefix
QFBank *FindBank(const char *prefix)
{
	int prefixLen = strlen(prefix);
	for (int i=0; i<GFileBanks.Size(); i++)
	{
		QFBank &bank = GFileBanks[i];
		if (strnicmp(bank.GetPrefix(), prefix, prefixLen) == 0)
			return &bank;
	}
	return NULL;
}

NetworkServer::NetworkServer(NetworkManager *parent, int port, RString password)
: NetworkComponent(parent)
{
	_botClient = 0xFFFFFFFF;
	_state = NGSCreate;
	_password = password.GetLength() > 0;

	_nextPlayerId = 1;

	Verify(Init(port, password));

	_param1 = 0;
	_param2 = 0;


//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
	_kickDuplicate = false;
	_dedicated = false;
	_missionIndex = -1;

	_gameMaster = AI_PLAYER;
	_admin = false;
	_restart = false;
	_reassign = false;
	
	Monitor(0);
	_debugNext = INT_MAX;
	_debugInterval = 0;

#endif
//}
	_pingUpdateNext = GlobalTickCount() + 5000;
	
	_sessionLocked = false;

  _equalModRequired = false;

	// ensure no content remain
	if (_server) DeleteDirectoryStructure(GetServerTmpDir(), false);

#if _ENABLE_DEDICATED_SERVER
	if (!IsDedicatedServer())
#endif
		LoadBanList(GetUserDirectory() + RString("ban.txt"), _banListLocal);
}

#include "cdapfncond.h"

/*
#if _CZECH
#include "roxxe.hpp"
#endif
*/

extern const int DefaultNetworkPort;

// Macrovision CD Protection
#if _ENABLE_MP
NetTranspServer * __cdecl CDPInitServer(int port, RString password, const ParamEntry &cfg);
CDAPFN_DECLARE_GLOBAL(CDPInitServer, CDAPFN_OVERHEAD_L2, CDAPFN_CONSTRAINT_NONE);

//! SafeDisk protected version of NetworkServer::Init
/*!
\param port IP port where server comunicate
\param password session password
\patch 1.11 Date 07/31/2001 by Jirka
- Improved: Multiplayer session description
\patch 1.11 Date 08/02/2001 by Jirka
- Changed: Temporary directory on server in now Tmp<port> instead of Tmp
\patch 1.24 Date 09/19/2001 by Jirka
- Added: parameter "hostname" added to dedicated server config
- Added: port displayed in session description if not default
- Added: parameter "maxPlayers" added to dedicated server config
\patch 1.30 Date 11/01/2001 by Jirka
- Added: parameter "voteMissionPlayers" added to dedicated server config
	when no Missions section in server config and at least "voteMissionPlayers" (default = 1)
	players are connected, mission voting is started automaticly
\patch 1.42 Date 1/9/2002 by Jirka
- Added: parameter "voiceOverNet" added to dedicated server config (default = true)
*/

NetTranspServer * __cdecl CDPInitServer(int port, RString password, const ParamEntry &cfg)
{
/*
	char buffer[256];
	sprintf(buffer, "Tmp%d", port);
	ServerTmpDir = buffer;
*/
	NetTranspServer *server = NULL;
#ifdef _WIN32
	if (IsUseSockets())
		server = CreateNetServer(cfg);
	else
		server = CreateDPlayServer();
#else
	server = CreateNetServer(cfg);
#endif
	if (!server) return NULL;
    	// load some settings from Flashpoint.cfg
	server->SetNetworkParams(cfg);

	RString sessionNameInit = LocalizeString(IDS_SESSION_NAME_INIT);
	RString sessionNameFormat = LocalizeString(IDS_SESSION_NAME_FORMAT);
	RString playerName;

	MPVersionInfo versionInfo;
	versionInfo.versionActual = MP_VERSION_ACTUAL;
	versionInfo.versionRequired = MP_VERSION_REQUIRED;
	versionInfo.mission[0] = 0;
	versionInfo.gameState = NGSCreate;
  strncpy(versionInfo.mod, GetModList(), MOD_LENGTH);
  versionInfo.mod[MOD_LENGTH - 1] = 0;

  bool equalModRequired = false;

	char hostname[512]; hostname[0] = 0;
	int maxPlayers = 0;
	bool voiceOverNet = true;

#if _ENABLE_DEDICATED_SERVER
	if (IsDedicatedServer())
	{
		ParamFile cfg;
		RString GetServerConfig();
		cfg.Parse(GetServerConfig());
		
		const ParamEntry *entry = cfg.FindEntry("hostname");
		if (entry)
		{
			RString hn = *entry;
			strncpy(hostname, hn, 512);
			hostname[511] = 0;
		}
		entry = cfg.FindEntry("maxPlayers");
		if (entry)
		{
			maxPlayers = *entry;
			maxPlayers+=2;
		}

		entry = cfg.FindEntry("voiceOverNet");
		if (entry)
		{
			voiceOverNet = *entry;
		}

    entry = cfg.FindEntry("equalModRequired");
    if (entry) equalModRequired = *entry;

		// on dedicated server, use only server name as session name
		// playerName = LocalizeString(IDS_DEDICATED_SERVER);
	}
	else
#endif
	{
		playerName = Glob.header.playerName;
	}

	bool result = server->Init
	(
		port, password, hostname, maxPlayers,
		sessionNameInit, sessionNameFormat, playerName,
		versionInfo, equalModRequired, MAGIC_APP
	);
	if (!result)
	{
		delete server;
		return NULL;
	}

	if (voiceOverNet)
		server->InitVoice();

	char buffer[256];
	sprintf(buffer, "Tmp%d", server->GetSessionPort());
	ServerTmpDir = buffer;

/*
#if _CZECH
	PerformRandomRoxxeTest_001(CDDrive);
#endif
*/

	CDAPFN_ENDMARK(CDPInitServer);
	
	return server;
}
#endif

extern RString FlashpointCfg;

/*!
\patch 1.89 Date 10/25/2002 by Jirka
- Added: Item "proxy" added to Flashpoint.cfg to enforce proxy server used to download xml squad page etc.
*/

bool NetworkServer::Init(int port, RString password)
{
	#if _ENABLE_MP
 	// load some settings from Flashpoint.cfg
	ParamFile cfg;
	cfg.Parse(FlashpointCfg);

	const ParamEntry *entry = cfg.FindEntry("proxy");
	if (entry) _proxy = *entry;

	// Macrovision CD Protection
	_server = CDPInitServer(port, password, cfg);
	return _server != NULL;
	#else
	return false;
	#endif
}

NetworkServer::~NetworkServer()
{
	RemoveSystemMessages();

//{ QUERY & REPORTING SDK
#if _ENABLE_DEDICATED_SERVER && _ENABLE_GAMESPY
	if (IsDedicatedServer()) DoneQR();
#endif
//}

	Done();
}

void NetworkServer::Done()
{
	// _dp->CancelAsyncOperation(NULL, DPNCANCEL_ALL_OPERATIONS);
	RemoveUserMessages();
  if (_server) DeleteDirectoryStructure(GetServerTmpDir(), true);
	_server = NULL;
}

int NetworkServer::GetMaxPlayers()
{
	int maxPlayers = 64;

#if _ENABLE_DEDICATED_SERVER
	if (IsDedicatedServer())
	{
		ParamFile cfg;
		RString GetServerConfig();
		cfg.Parse(GetServerConfig());
		
		const ParamEntry *entry = cfg.FindEntry("maxPlayers");
		if (entry) maxPlayers = *entry;
	}
#endif

	if (_state >= NGSPrepareSide)
	{
		saturateMin(maxPlayers, _playerRoles.Size());
	}
	maxPlayers += 2;
	return maxPlayers; 
}

bool NetworkServer::GetURL(char *address, DWORD addressLen)
{
	if (!_server) return false;
	return _server->GetURL(address, addressLen);
}

/*!
\patch 1.21 Date 08/23/2001 by Jirka
- Improved: session description
*/
void NetworkServer::UpdateSessionDescription()
{
	RString mission;
	if (_state >= NGSPrepareSide) mission = _missionHeader.name;
	_server->UpdateSessionDescription(_state, mission);
}

/*!
\patch 1.21 Date 08/22/2001 by Jirka
- Added: GameSpy Query & Reporting SDK support for dedicated server
\patch_internal 1.86 Date 10/16/2002 by Jirka
- Added: ASE SDK support for dedicated server
\patch_internal 1.90 Date 11/12/2002 by Jirka
- Fixed: dedicated server was reporting to ASE even for reportingIP="" 
*/


// 1.96 compilation compatibility hack
//{ QUERY & REPORTING SDK
#if _ENABLE_DEDICATED_SERVER && _ENABLE_GAMESPY

#include "GameSpy/queryreporting/gqueryreporting.h"
extern "C"
{
#include "ASE/ASEMasterSDK.h"
}

char *GetGameName(bool datadisc)
{
	if (datadisc)
		return "opflashr";
	else
		return "opflash";
}

char *GetSecretKey(bool datadisc)
{
	static char secret_key[9];

	//set the secret key, in a semi-obfuscated manner
	if (datadisc)
	{
		secret_key[0] = 'Y';
		secret_key[1] = '3';
		secret_key[2] = 'k';
		secret_key[3] = '7';
		secret_key[4] = 'x';
		secret_key[5] = '1';
		secret_key[6] = '\0';
	}
	else
	{
		secret_key[0] = 'T';
		secret_key[1] = 'a';
		secret_key[2] = 'j';
		secret_key[3] = 'n';
		secret_key[4] = 'e';
		secret_key[5] = '0';
		secret_key[6] = '0';
		secret_key[7] = '7';
		secret_key[8] = '\0';
	}

	return secret_key;
}

//! Handle called from GameSpy Query & Reporting SDK
static void basic_callback(char *outbuf, int maxlen, void *userdata)
{
	((NetworkServer *)userdata)->OnQueryBasic(outbuf, maxlen);
}
//! Handle called from GameSpy Query & Reporting SDK
static void info_callback(char *outbuf, int maxlen, void *userdata)
{
	((NetworkServer *)userdata)->OnQueryInfo(outbuf, maxlen);
}
//! Handle called from GameSpy Query & Reporting SDK
static void rules_callback(char *outbuf, int maxlen, void *userdata)
{
	((NetworkServer *)userdata)->OnQueryRules(outbuf, maxlen);
}
//! Handle called from GameSpy Query & Reporting SDK
static void players_callback(char *outbuf, int maxlen, void *userdata)
{
	((NetworkServer *)userdata)->OnQueryPlayers(outbuf, maxlen);
}

/*!
\patch 1.34 Date 12/04/2001 by Jirka
- Changed: GameSpy base local port
*/
void NetworkServer::InitQR()
{
	if (_reportingIP.GetLength() <= 0) return;

	int len = sizeof(qr_hostname) / sizeof(char);
	strncpy(qr_hostname, _reportingIP, len);
	qr_hostname[len - 1] = 0;

	//set the secret key, in a semi-obfuscated manner
	bool datadisc = false;
#if _ENABLE_DATADISC
	datadisc = true;
#endif
	qr_init
	(
		NULL, NULL, ::GetNetworkPort() + 1, GetGameName(datadisc), GetSecretKey(datadisc),
		basic_callback, info_callback, rules_callback, players_callback,
		this
	);

	ASEMaster_init(1);
}

void NetworkServer::DoneQR()
{
	if (_reportingIP.GetLength() <= 0) return;

	_state = NGSNone;
	qr_send_statechanged(NULL);
	qr_shutdown(NULL);
}

void NetworkServer::SimulateQR()
{
	if (_reportingIP.GetLength() <= 0) return;

	qr_process_queries(NULL);

	bool datadisc = false;
#if _ENABLE_DATADISC
	datadisc = true;
#endif
	ASEMaster_heartbeat(0, ::GetNetworkPort() + 1, GetGameName(datadisc));
}

void NetworkServer::ChangeStateQR()
{
	if (_reportingIP.GetLength() <= 0) return;

	qr_send_statechanged(NULL);
}

/*!
\patch 1.41 Date 1/2/2002 by Jirka
- Fixed: limit for size of GameSpy Q&R SDK answer is respected now
*/

inline bool SafePrint(char * &ptr, int &maxlen, char *format, ...)
{
	char buf[2048];
	
	va_list arglist;
	va_start(arglist, format);
	int size = vsprintf(buf, format, arglist);
	va_end(arglist);

	if (size >= maxlen) return false;

	strcpy(ptr, buf);
	ptr += size;
	maxlen -= size;

	return true;
}

void NetworkServer::OnQueryBasic(char *outbuf, int maxlen)
{
	if (_reportingIP.GetLength() <= 0) return;

	bool datadisc = false;
#if _ENABLE_DATADISC
	datadisc = true;
#endif

	SafePrint
	(
		outbuf, maxlen, "\\gamename\\%s\\gamever\\%s",
		GetGameName(datadisc), APP_VERSION_TEXT
	);
}

void NetworkServer::OnQueryInfo(char *outbuf, int maxlen)
{
	if (_reportingIP.GetLength() <= 0) return;

	char *ptr = outbuf;
	
	RString mission;
	RString island;
	if (_state >= NGSPrepareSide)
	{
		mission = _missionHeader.name;
		island = _missionHeader.island;
	}

	if (!SafePrint(ptr, maxlen, "\\groupid\\261")) return;
	if (!SafePrint(ptr, maxlen, "\\hostname\\%s", (const char *)_server->GetSessionName())) return;
	if (!SafePrint(ptr, maxlen, "\\hostport\\%d", _server->GetSessionPort())) return;
	if (!SafePrint(ptr, maxlen, "\\mapname\\%s", (const char *)island)) return;
	if (!SafePrint(ptr, maxlen, "\\gametype\\%s", (const char *)mission)) return;
	if (!SafePrint(ptr, maxlen, "\\numplayers\\%d", _identities.Size())) return;
	if (!SafePrint(ptr, maxlen, "\\maxplayers\\%d", GetMaxPlayers())) return;
	const char *mode = "";
/*
	switch (_state)
	{
		case NGSNone:
			mode = "exiting"; 
			break;
		case NGSCreating:
		case NGSCreate:
		case NGSLogin:
		case NGSPrepareSide:
		case NGSEdit:
			mode = "wait"; 
			break;
		case NGSPrepareRole:
		case NGSPrepareOK:
		case NGSTransferMission:
		case NGSLoadIsland:
			mode = "settings"; 
			break;
		case NGSBriefing:
		case NGSPlay:
			mode = "openplaying"; 
			break;
		case NGSDebriefing:
		case NGSDebriefingOK:
			mode = "debriefing"; 
			break;
		default:
			Fail("Game state");
			break;
	}
*/
	// test - 1.28
	mode = "openplaying";

	if (!SafePrint(ptr, maxlen, "\\gamemode\\%s", mode)) return;
}

void NetworkServer::OnQueryRules(char *outbuf, int maxlen)
{
	if (_reportingIP.GetLength() <= 0) return;

	char *ptr = outbuf;

	int timeleft;
	if (_state == NGSPlay)
	{
		timeleft = 15;

		float et = _missionHeader.estimatedEndTime.toFloat();
		if (et > 0)
		{
			et -= Glob.time.toFloat();
			timeleft = toInt(et / 60.0);
			saturateMax(timeleft, 1);
		}
	}
	else timeleft = 0;

	if (!SafePrint(ptr, maxlen, "\\timeleft\\%d", timeleft)) return;
	if (!SafePrint(ptr, maxlen, "\\param1\\%.0f", _param1)) return;
	if (!SafePrint(ptr, maxlen, "\\param2\\%.0f", _param2)) return;

	if (!SafePrint(ptr, maxlen, "\\actver\\%d", MP_VERSION_ACTUAL)) return;
	if (!SafePrint(ptr, maxlen, "\\reqver\\%d", MP_VERSION_REQUIRED)) return;
  if (!SafePrint(ptr, maxlen, "\\mod\\%s", (const char *)GetModList())) return;
  if (!SafePrint(ptr, maxlen, "\\equalModRequired\\%d", _equalModRequired)) return;
	if (!SafePrint(ptr, maxlen, "\\password\\%d", _password ? 1 : 0)) return;
	if (!SafePrint(ptr, maxlen, "\\gstate\\%d", _state)) return;
	const char *implementation = IsUseSockets() ? "sockets" : "dplay";
	if (!SafePrint(ptr, maxlen, "\\impl\\%s", implementation)) return;
#ifdef _WIN32
	if (!SafePrint(ptr, maxlen, "\\platform\\win")) return;
#else
	if (!SafePrint(ptr, maxlen, "\\platform\\linux")) return;
#endif
}

void NetworkServer::OnQueryPlayers(char *outbuf, int maxlen)
{
	if (_reportingIP.GetLength() <= 0) return;

	char *ptr = outbuf;

	AutoArray<AIStatsMPRow> &table = GStats._mission._tableMP;
	for (int i=0; i<_identities.Size(); i++)
	{
		const PlayerIdentity &identity = _identities[i];

		int score = 0;
		int deaths = 0;
		for (int j=0; j<table.Size(); j++)
		{
			if (strcmp(table[j].player, identity.name) == 0)
			{
				score = table[j].killsTotal;
				deaths = table[j].killed;
				break;
			}
		}
		RString squad;
		if (identity.squad) squad = identity.squad->nick;
		
		if (!SafePrint
		(
			ptr, maxlen, "\\player_%d\\%s\\team_%d\\%s\\score_%d\\%d\\deaths_%d\\%d",
			i, (const char *)identity.name,
			i, (const char *)squad,
			i, score,
			i, deaths
		)) return;
	}
}

#endif
//}

void SetBaseDirectory(RString dir);
void SetMission(RString world, RString mission, RString subdir);
extern RString MPMissionsDir;

//! Add list of missions to Network Command Message
static void AddMissionList(NetworkCommandMessage &answer)
{
#ifdef _WIN32
	// add mission banks
	_finddata_t info;
	long h = _findfirst("MPMissions\\*.pbo", &info);
	if (h != -1)
	{
		do
		{
			if ((info.attrib & _A_SUBDIR) == 0)
			{
				char name[256];
				strcpy(name, info.name);
				char *ext = strrchr(name, '.');	// remove extension .pbo
				*ext = 0;
				answer.content.WriteString(name);
			}
		}
		while (_findnext(h, &info) == 0);
		_findclose(h);
	}
	// add mission directories
	h = _findfirst("MPMissions\\*.*", &info);
	if (h != -1)
	{
		do
		{
			if 
			(
				(info.attrib & _A_SUBDIR) != 0 &&
				info.name[0] != '.'
			)
			{
				answer.content.WriteString(info.name);
			}
		}
		while (_findnext(h, &info) == 0);
		_findclose(h);
	}
	answer.content.WriteString("");
#else
    // add both mission banks & directories
  DIR *dir = opendir("mpmissions");
  if ( dir ) {
    struct dirent *entry;
    while ( (entry = readdir(dir)) ) {  // process one directory entry
      char fn[1024] = "mpmissions/";
      strcat(fn,entry->d_name);
      struct stat st;
      if ( !stat(fn,&st) )
        if ( S_ISDIR(st.st_mode) ) {    // directory
          if ( entry->d_name[0] != '.' )
            answer.content.WriteString(entry->d_name);
          }
        else {                          // regular file
          int len = strlen(entry->d_name);
          if ( len > 4 &&
               !strcmp(entry->d_name+len-4,".pbo") ) {
            memcpy(fn,entry->d_name,len-4); // remove ".pbo" extension
            fn[len-4] = (char)0;
            answer.content.WriteString(fn);
            }
          }
      }
    }
	answer.content.WriteString("");
#endif
}

void NetworkServer::ApplyVoting(const AutoArray<char> &id, const AutoArray<char> *value)
{
#if _ENABLE_DEDICATED_SERVER
	int *ptr = (int *)id.Data();
	int type = *(ptr++);
	switch (type)
	{
		case NCMTMission:
			if (value)
			{
				const char *p = value->Data();
				_mission = p;
				p += strlen(p) + 1;
				_cadetMode = *(bool *)p;
				if (_mission.GetLength() > 0) _restart = true;
				if (_state != NGSPlay)
				{
					GNetworkManager.DestroyAllObjects();
					SetGameState(NGSCreate);
				}
			}
			break;
		case NCMTMissions:
			{
				_mission = "?";
				_restart = true;
				if (_state != NGSPlay)
				{
					GNetworkManager.DestroyAllObjects();
					SetGameState(NGSCreate);
				}
				NetworkCommandMessage answer;
				answer.type = NCMTVoteMission;
				AddMissionList(answer);
				for (int i=0; i<_identities.Size(); i++)
					SendMsg(_identities[i].dpnid, &answer, NMFGuaranteed);
			}
			break;
		case NCMTRestart:
			if (_state == NGSPlay)
			{
				_restart = true;
				_reassign = false;
			}
			break;
		case NCMTReassign:
			_restart = true;
			_reassign = true;
			if (_state != NGSPlay)
			{
				GNetworkManager.DestroyAllObjects();

				// unassign all players
				#if _ENABLE_AI
					int defaultPlayer = _missionHeader.disabledAI ? NO_PLAYER : AI_PLAYER;
				#else
					int defaultPlayer = NO_PLAYER;
				#endif
				for (int i=0; i<_playerRoles.Size(); i++)
				{
					_playerRoles[i].player = defaultPlayer;
					_playerRoles[i].roleLocked = false;
				}
				// send to clients
				for (int i=0; i<_players.Size(); i++)
				{
					NetworkPlayerInfo &info = _players[i];
					if (info.state < NGSCreate) continue;
					SendMissionInfo(info.dpid, true);
					info.state = NGSPrepareSide;
					SetPlayerState(info.dpid, NGSPrepareSide);
				}

				SetGameState(NGSPrepareSide);

				_restart = false;
				_reassign = false;
			}
			break;
		case NCMTKick:
			{
				int player = *(ptr++);
				if (player != _botClient) KickOff(player,KORKick);
			}
			break;
		case NCMTAdmin:
			if (value)
			{
				const char *p = value->Data();
				int player = *((int *)p);
				if (player != AI_PLAYER && _dedicated && _gameMaster == AI_PLAYER)
				{
					_gameMaster = player;
					_admin = true;
					RString name;
					for (int i=0; i<_players.Size(); i++)
						if (_players[i].dpid == player)
						{
							name = _players[i].name;
							break;
						}
					ConsoleF("Admin %s logged in.", (const char *)name);

					_votings.Clear();

					NetworkCommandMessage answer;
					answer.type = NCMTLogged;
					answer.content.Write(&_admin, sizeof(_admin));
					// AddMissionList(answer);
					SendMsg(player, &answer, NMFGuaranteed);

					if (_mission[0] == '?' && _mission[1] == 0)
					{
						NetworkCommandMessage answer;
						answer.type = NCMTVoteMission;
						AddMissionList(answer);
						SendMsg(player, &answer, NMFGuaranteed);
					}

					UpdateAdminState();
				}
			}
			break;
	}
#endif
}

void OnServerUserMessage(int from, char *buffer, int bufferSize, void *context)
{
	NetworkServer *server = (NetworkServer *)context;

	bufferSize -= sizeof(int);
	int crc = *(int *)(buffer + bufferSize);
	static CRCCalculator calculator;
	if (calculator.CRC(buffer, bufferSize) == crc)
	{
		NetworkMessageRaw rawMsg(buffer, bufferSize);
		server->DecodeMessage(from, rawMsg);
	}
	else
	{
		Fail("Bad CRC for incoming message");
	}
}

void OnServerSendComplete(DWORD msgID, bool ok, void *context)
{
	NetworkServer *server = (NetworkServer *)context;
	server->OnSendComplete(msgID, ok);
}

/*!
\patch 1.93 Date 8/28/2003 by Jirka
- Fixed: Crash when too long mod list entered  
*/

void OnServerPlayerCreate(int player, bool botClient, const char *name, const char *mod, void *context)
{
  if (!botClient && stricmp(mod, GetModList()) != 0)
  {
    char message[512];
    RString format = LocalizeString(IDS_MP_VALIDERROR_2);

    sprintf(message, format, name);
    if (mod && *mod)
    {
      sprintf(message+strlen(message)," - %s", mod);
    }

    RefArray<NetworkObject> dummy;
    GNetworkManager.Chat(CCGlobal, "", dummy, message);
    GChatList.Add(CCGlobal, NULL, message, false, true);
#if _ENABLE_DEDICATED_SERVER
    ConsoleF(message);
#endif
  }

	NetworkServer *server = (NetworkServer *)context;
	server->OnCreatePlayer(player, botClient, name);
}

void OnServerPlayerDelete(int player, void *context)
{
	NetworkServer *server = (NetworkServer *)context;
	server->OnPlayerDestroy(player);
}

bool OnServerVoicePlayerCreate(int player, void *context)
{
	NetworkServer *server = (NetworkServer *)context;
	return server->OnCreateVoicePlayer(player);
}

unsigned NetworkServer::CleanUpMemory()
{
	if (_server) return _server->FreeMemory();
	return 0;
}

void NetworkServer::ReceiveSystemMessages()
{
	if (_server)
	{
		_server->ProcessSendComplete(OnServerSendComplete, this);
		_server->ProcessPlayers(OnServerPlayerCreate, OnServerPlayerDelete, this);
		_server->ProcessVoicePlayers(OnServerVoicePlayerCreate, this);
	}
}

void NetworkServer::ReceiveUserMessages()
{
	if (_server) _server->ProcessUserMessages(OnServerUserMessage, this);
}

void NetworkServer::RemoveSystemMessages()
{
	if (_server)
	{
		_server->RemoveSendComplete();
		_server->RemovePlayers();
		_server->RemoveVoicePlayers();
	}
}

void NetworkServer::RemoveUserMessages()
{
	if (_server) _server->RemoveUserMessages();
}

void CalculateMinMaxAvg::Sample(float value, float weight)
{
	saturateMin(_min,value);
	saturateMax(_max,value);
	_sum += value*weight;
	_weight += weight;
}

void CalculateMinMaxAvg::Reset()
{
	_min = FLT_MAX;
	_max = FLT_MIN;
	_sum = 0;
	_weight = 0;
}

CalculateMinMaxAvg::CalculateMinMaxAvg()
{
	Reset();
}


inline int toLargeIntSat(float x)
{
	if (x>=INT_MAX) return INT_MAX;
	if (x<INT_MIN) return INT_MIN;
	return toLargeInt(x);
}

void NetworkServer::ServerMessage(const char *text)
{
//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
	ConsoleF(text);
#endif
//}
	GChatList.Add(CCGlobal, NULL, text, false, true);
	RefArray<NetworkObject> dummy;
	GNetworkManager.Chat(CCGlobal, "", dummy, text);
}


//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER

#define DEBUG_TYPE_ENUM(type,prefix,XX) \
	XX(type, prefix, TotalSent) \
	XX(type, prefix, UserSent) \
	XX(type, prefix, Console) \
	XX(type, prefix, UserInfo) \
	XX(type, prefix, UserQueue)

DECLARE_DEFINE_ENUM(DebugType,DT,DEBUG_TYPE_ENUM)

/*!
\patch 1.88 Date 10/21/2002 by Ondra
- Added: Dedicated server command #debug (userInfo)
(Requires 1.87 client)
\patch 1.90 Date 10/31/2002 by Ondra
- New: MP: Added #debug checkfile command.
Server admins can use this command to check if any file used by clients matches server version.
\patch 1.90 Date 10/31/2002 by Ondra
- New: MP: Added server.cfg array checkfiles[] -
list of files that should be checked for each player connecting.
Example: checkfiles[]={"HWTL\dta\data3d.pbo","dta\data3d.pbo"}
\patch 1.90 Date 10/31/2002 by Ondra
- New: Servers checks if world file (.wrp) used by client matches server version.
*/

static RString ExtractWord(RString &line)
{
	const char *endWord = strchr(line,' ');
	if (!endWord)
	{
		RString word = line;
		line = RString();
		return word;
	}
	RString word = line.Substring(0,endWord-line.Data());
	line = endWord+1;
	return word;
}

/*
\patch  1.96b Date 07/05/2005 by Ondra
- Added: Improved cheat detection.
*/

void NetworkServer::DebugAsk(RString str, int from, bool fullAccess)
{
	// check for some special debug commands
	RString line = str;
	RString word = ExtractWord(line);
  if (!strcmpi(word,"version"))
  {
    RString versionText = APP_VERSION_TEXT;

		DebugAnswer(RString("Debug: version ") + versionText);

		ChatMessage chat;
		chat.channel = CCGlobal;
		chat.text = RString("Server Version: ") + versionText;
		chat.sender = NULL;
		chat.name = "";
		SendMsg(from, &chat, NMFGuaranteed);
    return;
  }
  if (!fullAccess)
  {
    return;
  }
	if (!strcmpi(word,"checkfile"))
	{
		// get file name
		RString filename = ExtractWord(line);
		if (filename.GetLength()>0)
		{
			// check file CRC
			if (!IsRelativePath(filename))
			{
				RString message = RString("Only relative path check allowed: ") + filename;	
				GChatList.Add(CCGlobal, NULL, message, false, true);
			}
			else
			{
				// TODO: get player ID (optional)
				// if there is no ID, check all players
				PerformFileIntegrityCheck(filename);
			}
		}
	}
  #if _ENABLE_REPORT
	else if (!strcmpi(word,"checkexe"))
  {
    // format: offset:size offset:size, offset:size
    for(;;)
    {
      RString location = ExtractWord(line);
      if (location.GetLength()<=0) break;
			PerformExeIntegrityCheck(location);
    }
  }
  #endif

	// check if string is only numbers
	char *endNum;
	float interval = strtod(str,&endNum);
	if (*endNum==0)
	{
		_debugInterval = interval;
		saturate(_debugInterval,0.1f,3600);
		BString<256> msg;
		sprintf(msg,"Debug: Interval %g",_debugInterval);
		DebugAnswer(RString(msg));
		return;
	}
	if (!stricmp(str,"off"))
	{
		// clear all debug info
		_debugOn.Clear();
		DebugAnswer(RString("Debug: All off"));
		//_debugInterval = 0;
		_debugNext = INT_MAX;
		return;
	}

	const char *beg = str;
	const char *end = strchr(beg, ' ');
	int len = end ? end - beg : strlen(beg);

	RString strCommand(beg, len);
	DebugType command = GetEnumValue<DebugType>((const char *)strCommand);
	if (command < 0) return;

	beg += len;
	while (*beg == ' ') beg++;

	if (stricmp(beg, "off") == 0)
	{
		_debugOn.DeleteKey(command);
		DebugAnswer(RString("Debug: ") + strCommand + RString(" off"));
	}
	else
	{
		_debugOn.AddUnique(command);
		DebugAnswer(RString("Debug: ") + strCommand);

		if (_debugInterval<=0)
		{
			_debugInterval = 10;
			_debugNext = _debugNext + toInt(_debugInterval*1000);
		}
		int next = GlobalTickCount()+toInt(_debugInterval*1000);
		if (_debugNext>next) _debugNext = next;
	}
}

void NetworkServer::DebugAnswer(RString str)
{
	if (_gameMaster != AI_PLAYER)
	{
		NetworkCommandMessage msg;
		msg.type = NCMTDebugAnswer;
		msg.content.WriteString(str);
		SendMsg(_gameMaster, &msg, NMFGuaranteed);
	}
}

int NetworkServer::ConsoleF(const char *format, ...)
{
	va_list arglist;
	va_start(arglist, format);

	BString<512> buf;
	vsprintf(buf, format, arglist);

	if (_debugOn.FindKey(DTConsole) >= 0)
	{
		DebugAnswer((const char *)buf);
	}
	int result = ::ConsoleF(buf);
	
	va_end( arglist );
	return result;
}

#endif
//}

/*!
\patch 1.52 Date 4/19/2002 by Jirka
- Fixed: When gamemaster disconnect from server during mission selection, server was in bad state.
\patch 1.79 Date 7/31/2002 by Jirka
- Improved: Mission on dedicated server launched if all roles are assigned and confirmed even if some players remain
\patch 1.82 Date 8/23/2002 by Ondra
- Fixed: MP: If server had predefined mission cycle,
mission sometimes started while first user was connecting.
*/

void NetworkServer::SimulateDS()
{
//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
	if (!_dedicated) return;

	DWORD tickCount = GlobalTickCount();
	if (_players.Size() <= 1)
	{
		if (_state != NGSCreate)
		{
			ConsoleF("All users disconnected, waiting for users");
			SetGameState(NGSCreate);
			CleanUpMemory();
		}
		_missionIndex = -1;
		_mission = "";
		_restart = false;
		_reassign = false;
	}
	else
	{
		static bool loadIsland = false;
//			static UITime debriefing;
		static RString lastSideMessage;
		static UITime lastSideMessageTime;
		switch (_state)
		{
		case NGSCreate:
			{
				RString t;
				if (_mission.GetLength() == 1 && _mission[0] == '?') break;
				else if (_mission.GetLength() > 0)
				{
					t = _mission;
					Glob.config.easyMode = _cadetMode;
					_param1 = FLT_MAX;
					_param2 = FLT_MAX;

					_mission = "";
					_restart = false;
					_reassign = false;
				}
				else
				{
					const ParamEntry *entry = _serverCfg.FindEntry("Missions");
					int nEntries = entry ? entry->GetEntryCount() : 0;
					if (nEntries == 0)
					{
						int voteMissionPlayers = 1;
						entry = _serverCfg.FindEntry("voteMissionPlayers");
						if (entry) voteMissionPlayers = *entry;
						if (_identities.Size() >= voteMissionPlayers)
						{
							_mission = "?";
							NetworkCommandMessage answer;
							answer.type = NCMTVoteMission;
							AddMissionList(answer);
							for (int i=0; i<_identities.Size(); i++)
								SendMsg(_identities[i].dpnid, &answer, NMFGuaranteed);
						}
						break;
					}

					_missionIndex++;
					if (_missionIndex >= nEntries) _missionIndex = 0;
					const ParamEntry& item = entry->GetEntry(_missionIndex);

					t = item >> "template";
					Glob.config.easyMode = item >> "cadetMode";

					_param1 = 0;
					_param2 = 0;
					if (item.FindEntry("param1")) _param1 = item >> "param1";
					if (item.FindEntry("param2")) _param2 = item >> "param2";
				}

				CurrentCampaign = "";
				CurrentBattle = "";
				CurrentMission = "";

				SetBaseDirectory("");

				char mission[256]; strcpy(mission, t);
				char *world = strrchr(mission, '.');
				if (!world) return;
				*world = 0; world++;

				if (QIFStream::FileExists(MPMissionsDir + t + RString(".pbo")))
				{
					// mission in public bank
					CreateMission(mission, world);
					SetMission(world, "__cur_mp", MPMissionsDir);
					Glob.header.filenameReal = mission;

					ConsoleF("Mission %s read from bank", (const char *)t);
				}
				else if (QIFStream::FileExists(MPMissionsDir + t + RString("\\mission.sqm")))
				{
					// mission in public directory
					CreateMission("", "");
					SetMission(world, mission, MPMissionsDir);

					ConsoleF("Mission %s read from directory", (const char *)t);
				}
				else break;

				// load template
				CurrentTemplate.Clear();
				if (!ParseMission(true)) break;

				if (_param1 == FLT_MAX)
				{
					_param1 = 0;
					const ParamEntry *cfg = ExtParsMission.FindEntry("defValueParam1");
					if (cfg) _param1 = *cfg;
				}
				if (_param2 == FLT_MAX)
				{
					_param2 = 0;
					const ParamEntry *cfg = ExtParsMission.FindEntry("defValueParam2");
					if (cfg) _param2 = *cfg;
				}

				InitMission(Glob.config.easyMode);
				SetGameState(NGSPrepareSide);
			}
			break;
		case NGSPrepareSide:
		case NGSPrepareRole:
			{
				// check if all commanders are ready
				if (!_restart)
				{
					if (_gameMaster != AI_PLAYER)
					{
						if (GetPlayerState(_gameMaster) < NGSPrepareOK) break;
					}
					else
					{
						int total = 0, ready = 0;
						for (int i=0; i<_players.Size(); i++)
						{
							if (_players[i].dpid == _botClient) continue;
							
							// CHECK FOR PLAYER WITHOUT IDENTITY
							if (!FindIdentity(_players[i].dpid))
							{
								RptF
								(
									"Server error: Player without identity %s (id %d)",
									(const char *)_players[i].name, _players[i].dpid
								);
								continue;
							}

							total++;
							if (_players[i].state >= NGSPrepareOK) ready++;
						}
						if (ready < total && ready < NPlayerRoles()) break;
						if (ready<=0) break;
					}
				}
				_restart = false;

				// create game
				SetGameState(NGSTransferMission);
				SendMissionFile();

				ConsoleF("Roles assigned");

				SetGameState(NGSLoadIsland);
				loadIsland = true;
			}
			break;
		case NGSLoadIsland:
			if (loadIsland && GNetworkManager.GetClient()->GetGameState() == NGSLoadIsland)
			{
				loadIsland = false;

				ConsoleF("Reading mission ...");

				GWorld->SwitchLandscape(GetWorldName(Glob.header.worldname));

				//set parameters - after SwitchLandscape
				// const ParamEntry &cls = (_serverCfg >> "Missions").GetEntry(_missionIndex);
				float param1 = 0, param2 = 0;
				GetNetworkManager().GetParams(param1, param2);
				/*
				float param1 = 0, param2 = 0;
				if (cls.FindEntry("param1")) param1 = cls >> "param1";
				if (cls.FindEntry("param2")) param2 = cls >> "param2";
				*/
				GameState *gstate = GWorld->GetGameState();
				gstate->VarSet("param1", GameValue(param1), false);
				gstate->VarSet("param2", GameValue(param2), false);
				GNetworkManager.PublicVariable("param1");
				GNetworkManager.PublicVariable("param2");
				
				GStats.ClearMission();
				GWorld->ActivateAddons(CurrentTemplate.addOns);
				GWorld->InitGeneral(CurrentTemplate.intel);
				bool error = false;
				if (!GWorld->InitVehicles(GModeNetware, CurrentTemplate))
				{
					error = true;
				}
				RString message = GetMaxErrorMessage();
				if (message.GetLength()>0)
				{
					ServerMessage(message);
					SetGameState(NGSCreate);
					break;
				}
				if (error)
				{
					ServerMessage("Error loading mission");
					SetGameState(NGSCreate);
					break;
				}
				
				GNetworkManager.CreateAllObjects();

				void RunInitScript();
				RunInitScript();

				message = GetMaxErrorMessage();
				if (message.GetLength()>0)
				{
					ServerMessage(message);
					SetGameState(NGSCreate);
					break;
				}
				else
				{
					ConsoleF("Mission read");
				}
			}
			break;
		case NGSBriefing:
			{
				bool all = true;
				for (int i=0; i<_playerRoles.Size(); i++)
				{
					int player = _playerRoles[i].player;
					if (player == AI_PLAYER || player == NO_PLAYER) continue;

					if (GetPlayerState(player) != NGSPlay)
					{
						all = false;
						break;
					}
				}
				if (all)
				{
					// start game
					SetGameState(NGSPlay);
					NetworkPlayerInfo *info = GetPlayerInfo(_botClient);
					Assert(info);
					info->state = NGSPlay;
					SetPlayerState(_botClient, NGSPlay);

          // source hack to enable 1.96 compiled with the actual version of this cpp file
          #if APP_VERSION_NUM>196
					  GStats.OnMissionStart();
          #endif
					ConsoleF("Game started");
				}
			}
			break;
		case NGSPlay:
			if (_restart)
			{
				GNetworkManager.DestroyAllObjects();
				ConsoleF("Game restarted");
				SetGameState(NGSDebriefing);
				//debriefing = Glob.uiTime;
			}
			else if 
			(
				GWorld->GetEndMode() != EMContinue &&
				(
					(!GWorld->GetCameraEffect() && !GWorld->GetTitleEffect()) ||
					GWorld->IsEndForced()
				)
			)
			{
				GNetworkManager.DestroyAllObjects();
				ConsoleF("Game finished");
				SetGameState(NGSDebriefing);
				//debriefing = Glob.uiTime;
			}
			else
			{
				bool all = true;
				for (int i=0; i<_playerRoles.Size(); i++)
				{
					int player = _playerRoles[i].player;
					if (player == AI_PLAYER || player == NO_PLAYER) continue;

					if (GetPlayerState(player) == NGSPlay)
					{
						all = false;
						break;
					}
				}
				if (all)
				{
					GNetworkManager.DestroyAllObjects();
					ConsoleF("Game finished");
					SetGameState(NGSDebriefing);
					//debriefing = Glob.uiTime;
				}
			}
			break;
		case NGSDebriefing:
			{
				float all = true;
				if (_gameMaster != AI_PLAYER)
				{
					NetworkGameState state = GetPlayerState(_gameMaster);
					all = state != NGSDebriefing && state <= NGSDebriefingOK;
				}
				else for (int i=0; i<_playerRoles.Size(); i++)
				{
					int player = _playerRoles[i].player;
					if (player == AI_PLAYER || player == NO_PLAYER) continue;

					NetworkGameState state = GetPlayerState(player);
					if (state == NGSDebriefing || state > NGSDebriefingOK)
					{
						all = false;
						break;
					}
				}
//				if (Glob.uiTime - debriefing >= 5.0)
				if (all)
				{
					if (_restart && _reassign)
					{
						// unassign all players
						#if _ENABLE_AI
							int defaultPlayer = _missionHeader.disabledAI ? NO_PLAYER : AI_PLAYER;
						#else
							int defaultPlayer = NO_PLAYER;
						#endif
						for (int i=0; i<_playerRoles.Size(); i++)
						{
							_playerRoles[i].player = defaultPlayer;
							_playerRoles[i].roleLocked = false;
						}
						// send to clients
						for (int i=0; i<_players.Size(); i++)
						{
							NetworkPlayerInfo &info = _players[i];
							if (info.state < NGSCreate) continue;
							SendMissionInfo(info.dpid, true);
							info.state = NGSPrepareSide;
							SetPlayerState(info.dpid, NGSPrepareSide);
						}

						SetGameState(NGSPrepareSide);
						_restart = false;
						_reassign = false;
					}
					else if (_restart && _mission.GetLength() == 0)
					{
						SetGameState(NGSPrepareRole);
						loadIsland = true;
					}
					else
					{
						SetGameState(NGSCreate);
						ConsoleF("Waiting for next game");
					}
				}
			}
			break;
		}
	}

	_monitorFrames++;
	if (tickCount >= _monitorNext)
	{
		if (_gameMaster == AI_PLAYER)
		{
			_monitorInterval = 0;
		}
		else //if (_state == NGSPlay)
		{
			NetworkCommandMessage msg;
			msg.type = NCMTMonitorAnswer;
			float invInterval = 1.0 / (_monitorInterval + 0.001f * (tickCount - _monitorNext));
			float fps = _monitorFrames * invInterval;
			int memory = MemoryUsed();
			float in = _monitorIn * invInterval;
			float out = _monitorOut * invInterval;
			msg.content.Write(&fps, sizeof(fps));
			msg.content.Write(&memory, sizeof(memory));
			msg.content.Write(&in, sizeof(in));
			msg.content.Write(&out, sizeof(out));

			int sizeG = 0, sizeNG = 0;
			for (int i=0; i<_players.Size(); i++)
			{
				NetworkPlayerInfo &player = _players[i];
				for (int j=0; j<player._messageQueue.Size(); j++)
					sizeG += player._messageQueue[j].msg->size;
				for (int j=0; j<player._messageQueueNonGuaranteed.Size(); j++)
					sizeNG += player._messageQueueNonGuaranteed[j].msg->size;
			}
			msg.content.Write(&sizeG, sizeof(sizeG));
			msg.content.Write(&sizeNG, sizeof(sizeNG));

			SendMsg(_gameMaster, &msg, NMFGuaranteed);
		}
		Monitor(_monitorInterval);
	}


	for (int i=0; i<_players.Size(); i++)
	{
		NetworkPlayerInfo &info = _players[i];
		if (info.state < NGSCreate) continue;
		if (info.motdIndex >= 0 && Glob.uiTime >= info.motdTime)
		{
			ChatMessage chat;
			chat.channel = CCGlobal;
			chat.text = _motd[info.motdIndex];
			chat.sender = NULL;
			chat.name = "";
			SendMsg(info.dpid, &chat, NMFGuaranteed);

			info.motdIndex++;
			if (info.motdIndex >= _motd.Size()) info.motdIndex = -1;
			else info.motdTime = Glob.uiTime + _motdInterval;
		}
	}
#endif
//}
}

/*!
\patch 1.79 Date 7/26/2002 by Jirka
- Added: Elimination of "disconnect cheat"
\patch 1.90 Date 10/30/2002 by Jirka
- Fixed: Message "Player ... is loosing connection" isn't write for server now
*/


void NetworkServer::OnSimulate()
{
	// raw statistics
#if 0
	// remove statistics info
	static AutoArray<int> texts;
	for (int i=0; i<texts.Size(); i++)
	{
		if (texts[i] >= 0) GEngine->RemoveText(texts[i]);
	}
	texts.Resize(0);

	if (outputDiags == 4)
	{
		int y = 40;
		for (int i=0; i<_rawStatistics.Size(); i++)
		{
			char output[1024];
			NetworkPlayerInfo *info = GetPlayerInfo(_rawStatistics[i].player);
			if (!info) continue;
			sprintf
			(
				output, "%s: sent %d (%d), received %d (%d)",
				(const char *)info->name,
				_rawStatistics[i].sizeSent, _rawStatistics[i].msgSent,
				_rawStatistics[i].sizeReceived, _rawStatistics[i].msgReceived
			);
			texts.Add(GEngine->ShowTextF(1000, 10, y, output));
			y += 25;
		}
	}
#endif

#if _ENABLE_DEDICATED_SERVER
	if (_dedicated)
	{
		//static DWORD lastDebugMessageTime = GlobalTickCount();
		DWORD now = GlobalTickCount();
		if (now >= _debugNext)
		{
			if (_gameMaster == AI_PLAYER || _debugInterval<0.001)
			{
				_debugNext = INT_MAX;
			}
			else
			{
				_debugNext = _debugNext + toInt(_debugInterval*1000);
			}

			float invAge = 1/_debugInterval;

			if (_debugOn.FindKey(DTUserSent) >= 0)
			{
				for (int i=0; i<_rawStatistics.Size(); i++)
				{
					BString<512> output;
					NetworkPlayerInfo *info = GetPlayerInfo(_rawStatistics[i].player);
					if (!info) continue;
					sprintf
					(
						output, "%s: sent %.0f bps (%.2f Msgps), received %.0f bps (%.2f Msgps)",
						(const char *)info->name,
						invAge * _rawStatistics[i].sizeSent *8, invAge * _rawStatistics[i].msgSent,
						invAge * _rawStatistics[i].sizeReceived *8, invAge * _rawStatistics[i].msgReceived
					);
					DebugAnswer((const char *)output);
				}
			}

			if (_debugOn.FindKey(DTTotalSent) >= 0)
			{
				int totalMsgSent = 0;
				int totalMsgReceived = 0;
				int totalSizeSent = 0;
				int totalSizeReceived = 0;
				for (int i=0; i<_rawStatistics.Size(); i++)
				{
					totalMsgSent += _rawStatistics[i].msgSent;
					totalMsgReceived += _rawStatistics[i].msgReceived;
					totalSizeSent += _rawStatistics[i].sizeSent;
					totalSizeReceived += _rawStatistics[i].sizeReceived;
				}
				BString<512> output;
				sprintf
				(
					output, "** Total: sent %.0f bps (%.2f Msgps), received %.0f bps (%.2f Msgps)",
					invAge * totalSizeSent * 8, invAge * totalMsgSent,
					invAge * totalSizeReceived * 8, invAge * totalMsgReceived
				);
				DebugAnswer((const char *)output);
			}

			_rawStatistics.Clear();

			if (_debugOn.FindKey(DTUserQueue) >= 0)
			{
				// send info about all players
				for (int i=0; i<_players.Size(); i++)
				{
					const NetworkPlayerInfo &player = _players[i];
					int sizeG = 0, sizeNG = 0;
					for (int j=0; j<player._messageQueue.Size(); j++)
						sizeG += player._messageQueue[j].msg->size;
					for (int j=0; j<player._messageQueueNonGuaranteed.Size(); j++)
						sizeNG += player._messageQueueNonGuaranteed[j].msg->size;
					// print actual desync as well

					BString<512> output;
					sprintf
					(
						output, "%s: Queue %d B (%d) %d B Guaranteed (%d), Desync %.0f",
						(const char *)player.name,
						sizeNG, player._messageQueueNonGuaranteed.Size(), sizeG, player._messageQueue.Size(),
						player._desync.GetAvg()*100
					);
					DebugAnswer((const char *)output);
				}
			}
			if (_debugOn.FindKey(DTUserInfo) >= 0)
			{
				// send info about all players
				for (int i=0; i<_players.Size(); i++)
				{
					const NetworkPlayerInfo &player = _players[i];

					RString stats = _server->GetStatistics(player.dpid);
					
					BString<512> output;
					sprintf
					(
						output, "%s: Info %s",
						(const char *)player.name, (const char *)stats
					);
					DebugAnswer((const char *)output);
				}
			}
		} // if (time for #debug)

	}
#endif
	
	// receive all system and user messages
	ReceiveSystemMessages();
	ReceiveLocalMessages();
	ReceiveUserMessages();
	PerformIntegrityInvestigations();

	#if _ENABLE_MP
	// cancel all update messages
	for (int i=0; i<_players.Size(); i++)
	{
		NetworkPlayerInfo &pInfo = _players[i];
		// statistics
		if (DiagLevel >= 3)
		{
			int nMsg, nBytes, nMsgG, nBytesG;
			_server->GetSendQueueInfo(pInfo.dpid, nMsg, nBytes, nMsgG, nBytesG);
			if (nMsg > 0 || nMsgG > 0)
            {
                if ( nMsgG < 0 )        // old style
				    DiagLogF("Server to %s: pending in SendQueue: %d messages, %d bytes", (const char *)pInfo.name, nMsg, nBytes);
                else                    // new style
				    DiagLogF("Server to %s: pending in SendQueue: common - %d messages, %d bytes, guaranteed - %d messages, %d bytes",
                             (const char *)pInfo.name, nMsg, nBytes, nMsgG, nBytesG);
            }
		}
		// cancel messages
/*
		int canceled = 0;
		for (int j=0; j<pInfo.objects.Size(); j++)
		{
			NetworkPlayerObjectInfo &poInfo = pInfo.objects[j];
			if (poInfo.lastCreatedMsgId != 0xFFFFFFFF && poInfo.canCancel)
			{
				HRESULT hr = _dp->CancelAsyncOperation(poInfo.lastCreatedMsgId, 0);
				if (DiagLevel > 0 && FAILED(hr))
					LogF("Server: cannot cancel message %x, error %x", poInfo.lastCreatedMsgId, hr);
				canceled++;
			}
		}
		// statistics
		if (DiagLevel >= 2)
		{
			if (canceled > 0) DiagLogF("Server to %s: canceled %d messages", (const char *)pInfo.name, canceled);
		}
*/
	}
	#endif

	SimulateDS();

	// kick off players faded out
	CheckFadeOut();

	// destroy players kicked out
	for (int i=0; i<_identities.Size(); i++)
	{
		PlayerIdentity &identity = _identities[i];
		if (identity.destroy && Glob.uiTime >= identity.destroyTime)
		{
			OnPlayerDestroy(identity.dpnid);
			i--;
		}
	}

	// send update messages
	SendMessages();

	DWORD tickCount = GlobalTickCount();
	if (tickCount >= _pingUpdateNext)
	{
		_pingUpdateNext = tickCount + 30000;
		
		// update all players information
		for (int i=0; i<_players.Size(); i++)
		{
			NetworkPlayerInfo &pnet = _players[i];
			//if (pnet.dpid==_botClient) continue;
			PlayerIdentity *id = FindIdentity(pnet.dpid);
			if (!id) continue;
			id->_minBandwidth = toLargeIntSat(pnet._bandwidth.GetMin()/(1024/8));
			id->_maxBandwidth = toLargeIntSat(pnet._bandwidth.GetMax()/(1024/8));
			id->_avgBandwidth = toLargeIntSat(pnet._bandwidth.GetAvg()/(1024/8));
			id->_minPing = toInt(pnet._ping.GetMin());
			id->_maxPing = toInt(pnet._ping.GetMax());
			id->_avgPing = toInt(pnet._ping.GetAvg());
			id->_desync = toInt(pnet._desync.GetAvg()*100);
			saturate(id->_desync,0,100000);
			pnet._ping.Reset();
			pnet._bandwidth.Reset();
			pnet._desync.Reset();
		}

		// broadcast messages about all players to all players
		for (int i=0; i<_players.Size(); i++)
		{
			NetworkPlayerInfo &info = _players[i];
			if (info.state < NGSCreate) continue;
			for (int i=0; i<_identities.Size(); i++)
			{
				PlayerIdentity &pi = _identities[i];

				NetworkMessageType type = pi.GetNMType(NMCUpdatePosition);
				NetworkMessageFormatBase *format = GetFormat(type);

				Ref<NetworkMessage> msg = new NetworkMessage();
				msg->time = Glob.time;
				NetworkMessageContext ctx(msg, format, this, info.dpid, MSG_SEND);
				ctx.SetClass(NMCUpdatePosition);

				Assert(dynamic_cast<const IndicesPlayerUpdate *>(ctx.GetIndices()))
				const IndicesPlayerUpdate *indices = static_cast<const IndicesPlayerUpdate *>(ctx.GetIndices());

				TMError err;
				int dpnid = pi.dpnid;
				err = ctx.IdxTransfer(indices->dpnid, dpnid);
				if (err != TMOK) continue;
				err = pi.TransferMsg(ctx);
				if (err != TMOK) continue;

				// send message
				//NetworkComponent::
				NetworkComponent::SendMsg(info.dpid,msg,type,NMFNone);
			}
		}
	}

	// integrity checks
	DWORD time = GlobalTickCount();
	for (int i=0; i<_players.Size(); i++)
	{
		NetworkPlayerInfo &info = _players[i];
		for (int t=0; t<info.integrityQuestions.Size(); t++)
		{
			if (time > info.integrityQuestions[t].timeout)
			{
				OnIntegrityCheckFailed(info.dpid,(IntegrityQuestionType)t,NULL,true);
				info.integrityQuestions.Delete(t);
				t--;
			}
		}
	}

	// squad checks
	for (int i=0; i<_squadChecks.Size();)
	{
		CheckSquadObject *obj = _squadChecks[i];
		if (obj->IsDone())
		{
			PlayerIdentity &identity = obj->_identity;
			Ref<SquadIdentity> squad = obj->_squad;
			if (squad)
			{
				// only new squad is passed into CreateIdentity and added into _squads
				for (int i=0; i<_squads.Size(); i++)
					if (_squads[i]->id == identity.squadId)
					{
						squad = NULL;
						break;
					}
			}
			CreateIdentity(identity, squad);

			_squadChecks.Delete(i);
		}
		else i++;
	}

//{ QUERY & REPORTING SDK
#if _ENABLE_DEDICATED_SERVER && _ENABLE_GAMESPY
	if (IsDedicatedServer()) SimulateQR();
#endif
//}

	DoAssert(CheckIntegrityOfPendingMessages());

	//DWORD tickCount = ::GlobalTickCount();
	// check pending messages for timeout
	for (int p=0; p<_pendingMessages.Size(); p++)
	{
		NetPendingMessage &pend = _pendingMessages[p];
		NetworkObjectInfo *oInfo = pend.info;
		NetworkPlayerObjectInfo *poInfo = pend.player;
		NetworkUpdateInfo *uInfo = pend.update;
		//NetworkUpdateInfo &info = poInfo->updates[j];
		#if _ENABLE_REPORT
			// verify update is in player
			DoAssert (uInfo>=poInfo->updates && uInfo<poInfo->updates+NMCUpdateN);
			// verify player exists in given object
			bool found = false;
			for (int i=0; i<oInfo->playerObjects.Size(); i++)
			{
				if (oInfo->playerObjects[i]==poInfo) found = true;
			}
			DoAssert(found);
		#endif
		DoAssert(uInfo->lastCreatedMsgId==pend.msgID);
		if (uInfo->lastCreatedMsgId!=pend.msgID) continue;
		if (uInfo->lastCreatedMsg && tickCount > uInfo->lastCreatedMsgTime + 5000 )
		{
			RptF("Server: Network message %x is pending", uInfo->lastCreatedMsgId);
			// try to find it in pending message list

			uInfo->lastCreatedMsg = NULL;
			uInfo->lastCreatedMsgId = 0xFFFFFFFF;
			uInfo->lastCreatedMsgTime = 0;

			_pendingMessages.Delete(p);
			p--;
			//DoAssert(CheckIntegrityOfPendingMessages());
		}
	}
	// check if sent confirmed for all outgoing messages
	#if _ENABLE_REPORT
	for (int o=0; o<_objects.Size(); o++)
	{
		NetworkObjectInfo *oInfo = _objects[o];
		for (int i=0; i<oInfo->playerObjects.Size(); i++)
		{
			NetworkPlayerObjectInfo &poInfo = *oInfo->playerObjects[i];
			for (int j=NMCUpdateFirst; j<NMCUpdateN; j++)
			{
				NetworkUpdateInfo &info = poInfo.updates[j];
				if (info.lastCreatedMsg && tickCount > info.lastCreatedMsgTime + 5000)
				{
					RptF("Server: Late pending discovery - message %x", info.lastCreatedMsgId);
					// try to find it in pending message list
					int pend = FindPendingMessage(info.lastCreatedMsgId);
					if (pend>=0)
					{
						_pendingMessages.Delete(pend);
						RptF("  Deleted from pending message list");
					}
					else
					{
						Fail("  Message missing in pending message list");
					}
					Fail("Late pending discovery");

					info.lastCreatedMsg = NULL;
					info.lastCreatedMsgId = 0xFFFFFFFF;
					info.lastCreatedMsgTime = 0;
				}
			}
		}
	}
	#endif

	// log all diagnostics
	WriteDiagOutput(true);

	// check for disconnected players
	for (int i=0; i<_players.Size(); i++)
	{
		NetworkPlayerInfo &info = _players[i];
		if (info.state < NGSPlay) continue;
		if (info.dpid == _botClient) continue;
		float age = _server->GetLastMsgAgeReliable(info.dpid);
		if (info.connectionProblemsReported)
		{
			info.connectionProblemsReported = age > 5.0f;
		}
		else if (age >= 10.0f)
		{
			BString<256> message;
			sprintf(message, LocalizeString(IDS_MP_CONNECTION_LOOSING), (const char *)info.name);
			ServerMessage(message);
			info.connectionProblemsReported = true;
		}
	}
}

DEFINE_FAST_ALLOCATOR(NetworkObjectInfo)


int NetworkServer::FindPendingMessage(NetworkUpdateInfo *update) const
{
	// try to find this message in pending message list
	for (int i=0; i<_pendingMessages.Size(); i++)
	{
		if (_pendingMessages[i].update==update) return i;
	}
	return -1;
}

int NetworkServer::FindPendingMessage(DWORD msgID) const
{
	// try to find this message in pending message list
	for (int i=0; i<_pendingMessages.Size(); i++)
	{
		if (_pendingMessages[i].msgID==msgID) return i;
	}
	return -1;
}

void NetworkServer::AddPendingMessage
(
	DWORD msgID, NetworkObjectInfo *info,
	NetworkUpdateInfo *update, NetworkPlayerObjectInfo *player
)
{
	DoAssert (info);
	DoAssert (msgID!=MSGID_REPLACE);
	/*
	for (int i=0; i<_pendingMessages.Size(); i++)
	{
		const NetPendingMessage &pend = _pendingMessages[i];
		if (pend.msgID==msgID && pend.info==info) return;
	}
	*/
	NetPendingMessage &pend = _pendingMessages.Append();
	pend.msgID = msgID;
	pend.info = info;
	pend.player = player;
	pend.update = update;
	// note: similiar message (same msgId and info) may be already pending
	// two updates for one object may travel in one message

	//pend.update = update;
	//LogF("Add pending message %x: object (%d:%d)",msgID,info->id.creator,info->id.id);
	#if 0 //_ENABLE_REPORT
		if (_pendingMessages.Size()%100==0)
		{
			LogF("%d pending messages",_pendingMessages.Size());
		}
	#endif
}

bool NetworkUpdateInfo::OnSendComplete(bool ok)
{
	if (!lastCreatedMsg)
	{
		Fail("Message ID without message");
		lastCreatedMsgId = 0xFFFFFFFF;
		lastCreatedMsgTime = 0;
		return false;
	}
	if (ok)
	{
		ADD_COUNTER(netCN, 1);
		ADD_COUNTER(netCS, lastCreatedMsg->size);
		// message sent
		lastSentMsg = lastCreatedMsg;
	}
	else
	{
		ADD_COUNTER(netFN, 1);
		ADD_COUNTER(netFS, lastCreatedMsg->size);
		if (DiagLevel >= 4)
		{
			DiagLogF("Server: sent of message %x failed", lastCreatedMsgId);
		}
	}
	lastCreatedMsg = NULL;
	lastCreatedMsgId = 0xFFFFFFFF;
	lastCreatedMsgTime = 0;
#if LOG_SEND_PROCESS
	LogF("  - update info %x updated", &info);
#endif
	return true;
}

/*!
\patch 1.76 Date 6/13/2002 by Ondra
- Optimized: Dedicated server CPU load improved, especially when many players are connected.
*/

void NetworkServer::OnSendComplete(DWORD msgID, bool ok)
{
#if LOG_SEND_PROCESS
	LogF("Server: Send %x completed: %s", msgID, ok ? "ok" : "failed");
#endif
	// try to find this message in pending message list
	//DoAssert(CheckIntegrityOfPendingMessages());
	int foundCount = 0;
	for (int i=0; i<_pendingMessages.Size(); i++)
	{
		if (_pendingMessages[i].msgID==msgID)
		{
			//LogF("Pending message match %d of %d",i,_pendingMessages.Size());
			// we have object candidate - search in it
			NetworkObjectInfo *oInfo = _pendingMessages[i].info;
			NetworkPlayerObjectInfo *poInfo = _pendingMessages[i].player;
			NetworkUpdateInfo *uInfo = _pendingMessages[i].update;
			#if _ENABLE_REPORT
				// verify update is in player
				DoAssert (uInfo>=poInfo->updates && uInfo<poInfo->updates+NMCUpdateN);
				// verify player exists in given object
				bool found = false;
				for (int j=0; j<oInfo->playerObjects.Size(); j++)
				{
					if (oInfo->playerObjects[j]==poInfo) found = true;
				}
				DoAssert(found);
			#endif
			DoAssert(uInfo->lastCreatedMsgId==msgID);
			if (uInfo->lastCreatedMsgId == msgID)
			{
				#if LOG_SEND_PROCESS
				LogF
				(
					"Processed pending message %x (%d:%d)",msgID,
					oInfo->id.creator,oInfo->id.id
				);
				#endif
				uInfo->OnSendComplete(ok);
				foundCount++;
			}
			// pending message processed - delete it
			_pendingMessages.Delete(i);
			i--;
			/*
			if(!CheckIntegrityOfPendingMessages())
			{
				RptF("Message %x processed",msgID);
				Fail("!CheckIntegrityOfPendingMessages()");
			}
			*/
		}
	}
	if (foundCount>0)
	{
		return;
	}
	#if _ENABLE_REPORT
	//RptF("message %x not found in pending messages",msgID);
	// if it failed, it is probably some bug
	// to be robust, we will perform complete dumb search

	for (int o=0; o<_objects.Size(); o++)
	{
		NetworkObjectInfo &oInfo = *_objects[o];
		for (int i=0; i<oInfo.playerObjects.Size(); i++)
		{
			NetworkPlayerObjectInfo &poInfo = *oInfo.playerObjects[i];
			for (int j=NMCUpdateFirst; j<NMCUpdateN; j++)
			{
				NetworkUpdateInfo &info = poInfo.updates[j];
				if (info.lastCreatedMsgId == msgID)
				{
					ErrF("dumb search found pending message %x",msgID);
					info.OnSendComplete(ok);
					// note: multiple logical messages may be connected with one message id
				}
			}
		}
	}
	#endif
}

/*!
\patch 1.47 Date 3/7/2002 by Ondra
- New: Player ID now displayed in server console when player is connected.
\patch 1.56 Date 5/13/2002 by Ondra
- New: When player is kicked off or banned, specific message is shown.
*/

void NetworkServer::OnCreatePlayer(int player, bool botClient, const char *name)
{
	if (_sessionLocked)
	{
		// session is locked
		_server->KickOff(player,NTRKicked);
		return;
	}

	if (botClient) _botClient = player;

//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
	bool server = _dedicated && player == _botClient;
	if (server) name = "__SERVER__";
#else
	bool server = false;
#endif
//}

	NetworkPlayerInfo *pInfo = OnPlayerCreate(player, name);

	// send player int and name to himself
//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
	if (_dedicated)
	{
		if (server)
		{
			ConsoleF("Server identity created");
		}
		else
		{
			ConsoleF("Player %s connecting", (const char *)pInfo->name);
		}
	}
#endif
//}

	PlayerMessage msg(player, pInfo->name, server);
	SendMsg(player, &msg, NMFGuaranteed);

}

bool NetworkServer::OnCreateVoicePlayer(int player)
{
	NetworkPlayerInfo *info = GetPlayerInfo(player);
	if (!info) return false;
	info->dvid = player;
	return true;
}

void NetworkServer::GetPlayers(AutoArray<NetPlayerInfo, MemAllocSA> &players)
{
	players.Resize(0);
	for (int i=0; i<_players.Size(); i++)
	{
		int index = players.Add();
		players[index].dpid = _players[i].dpid;
		players[index].name = _players[i].name;
	}
}

void NetworkServer::GetPlayersOnChannel(AutoArray<int, MemAllocSA> &players, AutoArray<NetworkId> units, DWORD from, bool voice)
{
	players.Resize(0);
	for (int i=0; i<_players.Size(); i++)
	{
		NetworkPlayerInfo &info = _players[i];
		if (info.dpid == from) continue;
		if (info.state < NGSCreate) continue;
		for (int j=0; j<units.Size(); j++)
		{
			if (units[j] == info.person)
			{
				if (voice)
				{
					if (info.dvid != 0) players.Add(info.dvid);
				}
				else
					players.Add(info.dpid);
				break;
			}
		}
	}
}

void NetworkServer::GetPlayersOnChannel(AutoArray<int, MemAllocSA> &players, int channel, DWORD from, bool voice)
{
	players.Resize(0);
	switch (channel)
	{
	case CCNone:
	case CCDirect:
		return;
	case CCGlobal:
		for (int i=0; i<_players.Size(); i++)
		{
			NetworkPlayerInfo &info = _players[i];
			if (info.dpid == from) continue;
			if (info.state < NGSCreate) continue;
			if (voice)
			{
				if (info.dvid != 0) players.Add(info.dvid);
			}
			else
				players.Add(info.dpid);
		}
		break;
	case CCSide:
		{
			TargetSide side = TSideUnknown;
			// find side
			// TODO: use _playerSides instead
			for (int i=0; i<_playerRoles.Size(); i++)
			{
				PlayerRole &role = _playerRoles[i];
				if (role.player == from)
				{
					side = role.side;
					break;
				}
			}
			if (side != TSideUnknown)
			{
				for (int i=0; i<_playerRoles.Size(); i++)
				{
					PlayerRole &role = _playerRoles[i];
					if (role.player == from) continue;
					if (role.side != side) continue;
					NetworkPlayerInfo *info = GetPlayerInfo(role.player);
					if (!info) continue;
					if (info->state < NGSCreate) continue;
					if (voice)
					{
						if (info->dvid != 0) players.Add(info->dvid);
					}
					else
						players.Add(info->dpid);
				}
			}
		}
		break;
	case CCGroup:
		{
			TargetSide side = TSideUnknown;
			int group = -1;
			// find group
			for (int i=0; i<_playerRoles.Size(); i++)
			{
				PlayerRole &role = _playerRoles[i];
				if (role.player == from)
				{
					side = role.side;
					group = role.group;
					break;
				}
			}
			// send message to all players with this group
			if (group >= 0)
			{
				for (int i=0; i<_playerRoles.Size(); i++)
				{
					PlayerRole &role = _playerRoles[i];
					if (role.player == from) continue;
					if (role.side != side || role.group != group) continue;
					NetworkPlayerInfo *info = GetPlayerInfo(role.player);
					if (!info) continue;
					if (info->state < NGSCreate) continue;
					if (voice)
					{
						if (info->dvid != 0) players.Add(info->dvid);
					}
					else
						players.Add(info->dpid);
				}
			}
		}
		break;
	case CCVehicle:
		{
			TargetSide side = TSideUnknown;
			int group = -1;
			int unit = -1;
			// find unit
			for (int i=0; i<_playerRoles.Size(); i++)
			{
				PlayerRole &role = _playerRoles[i];
				if (role.player == from)
				{
					side = role.side;
					group = role.group;
					unit = role.unit;
					break;
				}
			}
			// send message to all players with this group
			if (group >= 0)
			{
				for (int i=0; i<_playerRoles.Size(); i++)
				{
					PlayerRole &role = _playerRoles[i];
					if (role.player == from) continue;
					if
					(
						role.side != side || role.group != group ||
						role.unit != unit
					) continue;
					NetworkPlayerInfo *info = GetPlayerInfo(role.player);
					if (!info) continue;
					if (info->state < NGSCreate) continue;
					if (voice)
					{
						if (info->dvid != 0) players.Add(info->dvid);
					}
					else
						players.Add(info->dpid);
				}
			}
		}
		break;
	}
}

/*!
\patch 1.12 Date 08/08/2001 by Jirka
- Changed: squad validation on server is now processed asynchronously
\patch 1.22 Date 09/04/2001 by Jirka
- Changed: Messages "Player connected", "Player disconnected" are broadcasts over network
\patch 1.27 Date 10/17/2001 by Jirka
- Fixed: Player identity is not registered if player disconnect meanwhile.
 This caused unkickable ghost playes appering in player list.
\patch 1.34 Date 12/6/2001 by Jirka
- Fixed: squad logos on dedicated server game
\patch_internal 1.46 Date 3/4/2002 by Jirka
- Changed: random integrity checks disabled (does not work fine) 
*/

void NetworkServer::CreateIdentity(PlayerIdentity &ident, Ref<SquadIdentity> squad)
{
	// check if player is still connected
	if (!GetPlayerInfo(ident.dpnid))
	{
		// player already disconnected
		return;
	}

	int index = _identities.Add(ident);
	PlayerIdentity &identity = _identities[index];
	identity.playerid = _nextPlayerId++;
	if (identity.dpnid == _botClient) identity._rights |= PRServer;

#if LOG_PLAYERS
RptF
(
	"Server: Identity added - name %s, id %d (total %d identities, %d players)",
	(const char *)ident.name, ident.dpnid, _identities.Size(), _players.Size()
);
#endif

	if (squad)
	{
		// add squad and send it to all players except new (do not send twice)
		_squads.Add(squad);
		RString src, dst;
		if (squad->picture.GetLength() > 0)
		{
			src = GetServerTmpDir() + RString("\\squads\\") + squad->nick + RString("\\") + squad->picture;
			if (QIFStream::FileExists(src))
				dst = RString("tmp\\squads\\") + squad->nick + RString("\\") + squad->picture;
			else src = "";
		}
/*
		if (squad->picture.GetLength() > 0)
		{
			src = GetServerTmpDir() + RString("\\squads\\") + squad->nick + RString("\\") + squad->picture;
			CreatePath(src);
			char url[256];
			strcpy(url, squad->id);
			char *ptr = strrchr(url, '/');
			// FIX allow also \ in url
			if (!ptr) ptr = strrchr(url, '\\');
			if (ptr) {ptr++; *ptr = 0;}
			else ptr = url;
			strcpy(ptr, squad->picture);
			if (DownloadFile(url, src))
				dst = RString("tmp\\squads\\") + squad->nick + RString("\\") + squad->picture;
			else src = "";
		}
*/
		for (int i=0; i<_identities.Size(); i++)
			if (i != index)
			{
				if (src.GetLength() > 0)
					TransferFile(_identities[i].dpnid, dst, src);
				SendMsg(_identities[i].dpnid, squad, NMFGuaranteed);
			}
//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
		if (_dedicated)
		{
			NetworkClient *client = _parent->GetClient();
			DoAssert(client && client->GetPlayer() != identity.dpnid);
			// send new identity to bot client
			if (src.GetLength() > 0)
				TransferFile(client->GetPlayer(), dst, src);
			SendMsg(client->GetPlayer(), squad, NMFGuaranteed);
		}
#endif
//}
	}
	NetworkPlayerInfo *info = GetPlayerInfo(identity.dpnid);
	if (!info)
	{
		info = OnPlayerCreate(identity.dpnid, identity.name);
	}
	identity.name = info->name;
	for (int i=0; i<_squads.Size(); i++)
	{
		SquadIdentity *squad = _squads[i];
		if (squad->picture.GetLength() > 0)
		{
			RString src = GetServerTmpDir() + RString("\\squads\\") + squad->nick + RString("\\") + squad->picture;
			if (QIFStream::FileExists(src))
			{
				RString dst = RString("tmp\\squads\\") + squad->nick + RString("\\") + squad->picture;
				TransferFile(identity.dpnid, dst, src);
			}
		}
		SendMsg(identity.dpnid, squad, NMFGuaranteed);
	}

	bool hasCustomFace = stricmp(identity.face, "custom") == 0;

	for (int i=0; i<_identities.Size(); i++)
	{
		PlayerIdentity &player = _identities[i];
		if (i != index)
		{
			// send all faces and sounds to new player
//						if (notBotClient)
			{
				if (stricmp(player.face, "custom") == 0)
					TransferFace(identity.dpnid, player.name);
				TransferCustomRadio(identity.dpnid, player.name);
			}
			// send new face and sound to all players
//						if (!client || client->GetPlayer() != player.dpnid)
			{
				if (hasCustomFace)
					TransferFace(player.dpnid, identity.name);
				TransferCustomRadio(player.dpnid, identity.name);
			}
			// send all identities to new player
			NetworkPlayerInfo *pInfo = GetPlayerInfo(player.dpnid);
			if (pInfo) player.state = pInfo->state;
			SendMsg(identity.dpnid, &player, NMFGuaranteed);
		}
		// send new identity to all players
		SendMsg(player.dpnid, &identity, NMFGuaranteed);
	}
//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
	if (_dedicated)
	{
		NetworkClient *client = _parent->GetClient();
		DoAssert(client && client->GetPlayer() != identity.dpnid);
		// send new identity to bot client
		SendMsg(client->GetPlayer(), &identity, NMFGuaranteed);
	}
#endif
//}
	info->state = NGSLogin;

	// all message formats registered, we may send messages
	// send state dependent info 
	if (_state >= NGSPrepareSide)
	{
		// send mission info
		SendMissionInfo(info->dpid);
		info->state = NGSPrepareSide;
		info->missionFileValid = false;
	}
	// send current state
	ChangeGameState gs(_state);
	SendMsg(info->dpid, &gs, NMFGuaranteed);

	// if game is already in progress, send how long is played already
	if (_state == NGSPlay)
	{
		NetworkCommandMessage msg;
		msg.type = NCMTMissionTimeElapsed;
		int timeElapsed = GlobalTickCount() - _missionHeader.start;
		msg.content.Write(&timeElapsed, sizeof(timeElapsed));
		SendMsg(info->dpid, &msg, NMFGuaranteed);
	}

	identity.state = info->state;

	// send player state to all clients
	SetPlayerState(info->dpid, info->state);

	identity._minPing = 10;
	identity._avgPing = 100;
	identity._maxPing = 1000;

	identity._minBandwidth = 14;
	identity._avgBandwidth = 28;
	identity._maxBandwidth = 33;

	// Send chat message about player is connected
	{
		char message[256];
		sprintf(message, LocalizeString(IDS_MP_CONNECT), (const char *)identity.name);
		RefArray<NetworkObject> dummy;
		GNetworkManager.Chat(CCGlobal, "", dummy, message);
		GChatList.Add(CCGlobal, NULL, message, false, true);
		DWORD updatePingTime = GlobalTickCount() + 5000;
		if (_pingUpdateNext>updatePingTime) _pingUpdateNext = updatePingTime;
		#if _ENABLE_DEDICATED_SERVER
			if (_dedicated)
			{
				ConsoleF
				(
					"Player %s connected (id=%s)",
					(const char *)identity.name, (const char *)identity.id
				);
			}
		#endif
	}

	// send basic integrity questions
	// do not send integrity question to myself or to botclient
	if (info->dpid != _player && info->dpid != _botClient)
	{
		// if player is using different version, it is impossible to check integrity
		int ver = identity.version;
		// temporary: enable 1.47 server checking config of 146 client
		if
		(
			ver!=MP_VERSION_ACTUAL &&
			( ver!=146 || MP_VERSION_ACTUAL<147 || MP_VERSION_ACTUAL>149)
		)
		{
			// never check
			info->integrityCheckNext = UINT_MAX;
			char buf[256];
			if (ver)
			{
				sprintf(buf,"%d.%02d",ver/100,ver%100);
			}
			else
			{
				strcpy(buf,"1.40..1.42");
			}
			OnIntegrityCheckFailed(info->dpid,IQTConfig,buf,true);
		}
		else
		{
			PerformInitialIntegrityCheck(*info);
		  info->integrityCheckNext = GlobalTickCount()+1000*toInt(20+GRandGen.RandomValue()*30);
		}
	}

//{ QUERY & REPORTING SDK
#if _ENABLE_DEDICATED_SERVER && _ENABLE_GAMESPY
	if (IsDedicatedServer()) ChangeStateQR();
#endif
//}


#if _ENABLE_DEDICATED_SERVER
	if (_state == NGSCreate && _mission.GetLength() == 1 && _mission[0] == '?')
	{
		NetworkCommandMessage answer;
		answer.type = NCMTVoteMission;
		AddMissionList(answer);
		SendMsg(identity.dpnid, &answer, NMFGuaranteed);
	}
#endif
}

void NetworkServer::SetPlayerState(int dpid, NetworkGameState state)
{
	PlayerStateMessage ps;
	ps.player = dpid;
	ps.state = state;
	for (int i=0; i<_players.Size(); i++)
	{
		SendMsg(_players[i].dpid, &ps, NMFGuaranteed);
	}
}

#ifndef _WIN32
extern bool interrupted;
#endif


static bool CheckValidUpload(RString path, RString name)
{
	RString prefixShouldBe = GetServerTmpDir() + RString("\\players\\") + name + RString("\\");
	return !strnicmp(path,prefixShouldBe,prefixShouldBe.GetLength());
}

static RString GetRelUploadPath(RString path, RString name)
{
	RString prefixShouldBe = GetServerTmpDir() + RString("\\players\\") + name + RString("\\");
	if (strnicmp(path,prefixShouldBe,prefixShouldBe.GetLength())) return path;
	return path.Substring(prefixShouldBe.GetLength(),INT_MAX);
}

int NetworkPlayerInfo::FindIntegrityQuestion
(
	IntegrityQuestionType type, const IntegrityQuestion &q
) const
{
	for (int i=0; i<integrityQuestions.Size(); i++)
	{
		const IntegrityQuestionInfo &qi = integrityQuestions[i];
		if (qi.type==type && qi.q.name == q.name && qi.q.offset==q.offset && qi.q.size==q.size) return i;
	}
	return -1;
}

int NetworkPlayerInfo::FindIntegrityQuestion(IntegrityQuestionType type) const
{
	for (int i=0; i<integrityQuestions.Size(); i++)
	{
		const IntegrityQuestionInfo &qi = integrityQuestions[i];
		if (qi.type==type) return i;
	}
	return -1;
}

int NetworkPlayerInfo::FindIntegrityQuestion(int id) const
{
	for (int i=0; i<integrityQuestions.Size(); i++)
	{
		const IntegrityQuestionInfo &qi = integrityQuestions[i];
		if (qi.id==id) return i;
	}
	return -1;
}


/*!
\patch_internal 1.01 Date 6/22/2001 by Ondra.
- Added: ID==0 refusal
\patch 1.02 Date 7/12/2001 by Jirka:
	- Fixed: error in assignment
	- allow player assignment only in appropriate state
	- ignore messages arrived after state has changed
\patch_internal 1.03 Date 7/13/2001 by Jirka.
- Fixed: Allow \ in url of squad (for debugging purposes)
\patch_internal 1.14 Date 8/9/2001 by Ondra.
- Added: ID>110000000 || ID>10000000 && ID<100000000 refusal
\patch 1.21 Date 8/24/2001 by Jirka:
- Fixed: error in assignment
\patch 1.25 Date 9/29/2001 by Ondra:
- New: Ping to all clients displayed on server.
\patch 1.27 Date 10/15/2001 by Jirka
- Fixed: When CAPS LOCK was pressed, Voice Over Net was not disabled (bug since 1.26)
\patch 1.30 Date 11/01/2001 by Jirka
- Fixed: Voting is echoed to clients
\patch 1.33 Date 11/29/2001 by Jirka
- Improved: Global ban list reloads before player is checked
\patch_internal 1.43 Date 1/31/2002 by Jirka
- Fixed: Server does not send create and update messages to non-playing clients
\patch 1.82 Date 8/19/2002 by Ondra
- Fixed: Voting is echoed even to the player voting.
\patch 1.86 Date 10/16/2002 by Jirka
- Fixed: On dedicated server with mission rotating, after #login and #missions no missions appears
\patch  1.96b Date 07/05/2005 by Ondra
- Fixed: Chat message limited to 200 characters to prevent chat abuse.
*/


void NetworkServer::OnMessage(int from, NetworkMessage *msg, NetworkMessageType type)
{
	NetworkMessageFormatBase *format = GetFormat(/*from, */type);
	if (!format)
	{
		RptF("Server: Bad message %d(%s) format", (int)type, (const char *)NetworkMessageTypeNames[type]);
		return;
	}
	NetworkMessageContext ctx(msg, format, this, from, MSG_RECEIVE);

	#if _ENABLE_CHEATS
	int level = GetDiagLevel(type,from!=_parent->GetClient()->GetPlayer());
	if (level >= 2)
	{
		DiagLogF("Server: received message %s from %d", NetworkMessageTypeNames[type], from);		
		ctx.LogMessage(level, "\t");
	}
	#endif

	int index1 = -1, index2 = -1;

	switch (type)
	{
/*
		case NMTMsgFormat:
			{
				NetworkPlayerInfo *info = GetPlayerInfo(from);
				if (!info) info = OnPlayerCreate(from, "");
				int index = msg->id.id - NMTFirstVariant;
				if (index >= info->formats.Size())
					info->formats.Resize(index + 1);
				info->formats[index].TransferMsg(ctx);
			}
			break;
*/
		case NMTIntegrityAnswer:
			{
				IntegrityAnswerMessage answer;
				answer.TransferMsg(ctx);
				NetworkPlayerInfo *info = GetPlayerInfo(from);
				if (info)
				{
					int qId = answer.id;
					IntegrityQuestionType qType = answer.type;
					//Assert (info->integrityQuestionTimeout[qType]!=UINT_MAX);
					// it should be an answer to the first question of given type

					int qIndex = -1;
					if (qId > 0) qIndex = info->FindIntegrityQuestion(qId);
					else qIndex = info->FindIntegrityQuestion(qType);

					if (qIndex>=0)
					{
						const IntegrityQuestion &q = info->integrityQuestions[qIndex].q;
						//LogF
						//(
						//	"%s - %s: Received answer %d",
						//	(const char *)info->name,(const char *)q.name,answer.answer
						//);
#if _ENABLE_DEDICATED_SERVER
						bool dedicated = IsDedicatedServer();
#else
						bool dedicated = false;
#endif
						unsigned int myAnswer = IntegrityCheckAnswer(qType,q,dedicated);
						// integrity question may be a part of active investigation
            #if _ENABLE_REPORT
              if (qType==IQTExe)
              {
                static LogFile crcFileS;
                if (!crcFileS.IsOpen()) crcFileS.Open("exeS.crc");

                //LogF("{0x%08x,0x%04x,0x%08x}",q.offset,q.size,answer.answer);
                crcFileS.PrintF("{0x%08x,0x%04x,0x%08x},\n",q.offset,q.size,answer.answer);
              }
						  else if (myAnswer!=answer.answer)
						  {
							  LogF
							  (
								  "%s - %s: Received answer %x, expected %x",
								  (const char *)info->name,(const char *)q.name,
								  answer.answer,myAnswer
							  );
						  }
            #endif
						bool wasIA = IntegrityAnswerReceived(info,qType,q,myAnswer==answer.answer);
						if (!wasIA && myAnswer!=answer.answer)
						{
							// calculate correct answer
							// try to provide more info if possible
							if (qType==IQTConfig)
							{
								OnIntegrityCheckFailed(from,(IntegrityQuestionType)qType,q.name,false);
								// find where integrity failed - ask more detailed questions
								info->integrityInvestigation[qType] = new IntegrityInvestigationConfig(q.name);
							}
							else if (qType==IQTExe)
              {
                // timing is random - kickoff immediately
		            for (int i=0; i<_identities.Size(); i++)
		            {
			            PlayerIdentity &identity = _identities[i];
                  if (identity.dpnid==from && identity.kickOffTime==UITIME_MAX)
                  {
								    //OnIntegrityCheckFailed(from,(IntegrityQuestionType)qType,q.name,true);
                    #if !_ENABLE_REPORT
          					float timeToPlay = GRandGen.Gauss(10,15,30);
          					identity.kickOffTime = Glob.uiTime+timeToPlay;
                    identity.kickOffState = KOExe;
                    #endif
                  }
                }
              }
							else
							{
								OnIntegrityCheckFailed(from,(IntegrityQuestionType)qType,q.name,true);
							}
							// we discovered one modification
							// report it and stop checking
							// otherwise log would be full of "modified xxxxxx" messages
						}
						info->integrityQuestions.Delete(qIndex);
					}
          #if _ENABLE_REPORT
          else
          {
            LogF("Received unknown answer %08x",answer.id);
          }
          #endif
					//info->integrityQuestionTimeout[qType] = UINT_MAX;
				}
			}
			break;
		case NMTSelectPlayer:
			{
				SelectPlayerMessage pl;
				pl.TransferMsg(ctx);
				Assert(pl.player != AI_PLAYER);
				NetworkPlayerInfo *info = GetPlayerInfo(pl.player);
				Assert(info);
				if (!info) break;
				info->person = pl.person;
				info->cameraPosition = pl.position;
				info->cameraPositionTime = msg->time;
				// fulfill info->unit, info->group
				NetworkId brain = PersonToUnit(pl.person);
				info->unit = brain;

				if (from != pl.player)
				{
					NetworkComponent::SendMsg(pl.player, msg, type, NMFGuaranteed);
				}
				NetworkObjectInfo *oInfo = GetObjectInfo(pl.person);
				DoAssert(oInfo);
				if (!oInfo) break;
				ChangeOwner(pl.person, oInfo->owner, pl.player);
				if (!brain.IsNull())
				{
					NetworkObjectInfo *oInfo = GetObjectInfo(brain);
					DoAssert(oInfo);
					if (!oInfo) break;
					ChangeOwner(brain, oInfo->owner, pl.player);
				}
			}
			break;
		case NMTAttachPerson:
			{
				Assert(dynamic_cast<const IndicesAttachPerson *>(ctx.GetIndices()))
				const IndicesAttachPerson *indices = static_cast<const IndicesAttachPerson *>(ctx.GetIndices());

				NetworkId person;
				if (ctx.IdxGetId(indices->person, person) != TMOK) break;
				NetworkId unit;
				if (ctx.IdxGetId(indices->unit, unit) != TMOK) break;
				if (unit.IsNull()) break;
				for (int i=0; i<_mapPersonUnit.Size(); i++)
				{
					PersonUnitPair &pair = _mapPersonUnit[i];
					if (pair.unit == unit)
					{
						pair.person = person;
						break;
					}
				}
			}
			break;
		case NMTAskMissionFile:
			{
				AskMissionFileMessage ask;
				ask.TransferMsg(ctx);
				if (ask.valid)
				{
					NetworkPlayerInfo *info = GetPlayerInfo(from);
					if (info) info->missionFileValid = true;
				}
			}
			break;
		case NMTGameState:
			{
				ChangeGameState gs;
				gs.TransferMsg(ctx);
				switch (gs.gameState)
				{
/*
				case NGSCreate:
					{
						// all message formats registered, we may send messages
						NetworkPlayerInfo *info = GetPlayerInfo(from);
						Assert(info);
						info->state = NGSCreate;

						// send state dependent info 
						if (_state >= NGSPrepare)
						{
							// send mission info
							SendMissionInfo(from);
							info->state = NGSPrepare;
							info->missionFileValid = false;
						}

						// send current state
						ChangeGameState gs(_state);
						SendMsg(from, &gs, 0, 0, NMFGuaranteed);
					}
					break;
*/
				case NGSLogin:
				case NGSMissionVoted:
				case NGSPrepareSide:
				case NGSPrepareRole:
				case NGSPrepareOK:
				case NGSPlay:
				case NGSDebriefing:
				case NGSDebriefingOK:
					{
						NetworkPlayerInfo *info = GetPlayerInfo(from);
						Assert(info);
						if (info && info->state >= NGSCreate)
						{
							info->state = gs.gameState;
							SetPlayerState(from, gs.gameState);
						}
					}
					break;
				case NGSBriefing:
					{
						if (from == _botClient)
						{
							// select players
							for (int i=0; i<_players.Size(); i++)
							{
								NetworkPlayerInfo &info = _players[i];
								NetworkId &id = info.person;
								if (id.IsNull()) continue;
								NetworkObjectInfo *oInfo = GetObjectInfo(id);
								if (!oInfo) continue;
								SelectPlayerMessage pl(info.dpid, id, info.cameraPosition, false);
								NetworkComponent::SendMsg(pl.player, &pl, NMFGuaranteed);
								ChangeOwner(id, oInfo->owner, pl.player);
							}
							// change state
							SetGameState(NGSBriefing);
						}
						NetworkPlayerInfo *info = GetPlayerInfo(from);
						Assert(info);
						if (info && info->state >= NGSCreate)
						{
							info->state = NGSBriefing;
							SetPlayerState(from, NGSBriefing);
							// ask player about world file CRC
							RString worldFile = GetWorldName(Glob.header.worldname);
							PerformFileIntegrityCheck(worldFile,info->dpid);
						}
					}
					break;
				default:
					RptF("Server: Unexpected game state %d", gs.gameState);
					break;
				}
			}
			break;
		case NMTLogin:
			{
				//{ CHANGED
				PlayerIdentity identity;
				identity.TransferMsg(ctx);
/*
				int index = _identities.Add();
				PlayerIdentity &identity = _identities[index];
				identity.TransferMsg(ctx);
*/
				//}

				__int64 id64 = _atoi64(identity.id);
				// check if not in ban lists
				if (from != _botClient)
				{
					// reload ban list
					LoadBanList("ban.txt", _banListGlobal);
					
					bool ban = false;
					for (int i=0; i<_banListGlobal.Size(); i++)
						if (id64 == _banListGlobal[i]) {ban = true; break;}
					if (!ban) for (int i=0; i<_banListLocal.Size(); i++)
						if (id64 == _banListLocal[i]) {ban = true; break;}
					if (ban)
					{
						//{ CHANGED
						// _identities.Delete(index);
						//}
						KickOff(from,KORBan);
						break;
					}
				}

				//{ ADD hacked version - invalid ID
				//#if _SUPER_RELEASE
				// check if same squad id is already used
				// only in SUPER_RELEASE,
				// so that in RELEASE we may test server/client on single computer
				if (id64 != 0 && id64 != 999)
				{
					// check only when id!=0 and id!=999
					// for id==0 we have better handling
					// id==999 is used in Multiplayer Demo
					bool validateId = true;
					for (int i=0; i<_identities.Size(); i++)
					{
						if (_identities[i].id==identity.id)
						{
							validateId = false;
						}
					}
					if (!validateId)
					{
						// report problems on global channel
						RString string = LocalizeString(IDS_FADE_AWAY);
						RString senderName = LocalizeString(IDS_FADE_REMMEMBER);
						RefArray<NetworkObject> dummy;
						GNetworkManager.Chat(CCGlobal,senderName,dummy,string);
						GChatList.Add(CCGlobal, senderName, string, false, true);

						#if _ENABLE_DEDICATED_SERVER
						if (_kickDuplicate) KickOff(identity.dpnid, KORKick);
						#endif
					}
				}
				//#endif
				//}

				//{ CHANGED
				bool isSquad = identity.squadId.GetLength() > 0;
				if (isSquad)
				{
					Ref<SquadIdentity> squad;
					for (int i=0; i<_squads.Size(); i++)
						if (_squads[i]->id == identity.squadId)
						{
							squad = _squads[i];
							break;
						}

					bool newSquad = false;
					if (!squad)
					{
						squad = new SquadIdentity();
						squad->id = identity.squadId;
						newSquad = true;
					}

					// create object
					_squadChecks.Add(new CheckSquadObject(identity, squad, newSquad, _proxy));

#if LOG_PLAYERS
RptF
(
	"Server: Identity check started - name %s, id %d (total %d identities, %d players)",
	(const char *)identity.name, identity.dpnid, _identities.Size(), _players.Size()
);
#endif
				}
				else
				{
					CreateIdentity(identity, NULL);
				}
				//}
			}
			break;
		case NMTPlayerRole:
			OnMessagePlayerRole(from, type, ctx);
			break;
		case NMTAskForAnimationPhase:
			{
				Assert(dynamic_cast<const IndicesAskForAnimationPhase *>(ctx.GetIndices()))
				const IndicesAskForAnimationPhase *indices = static_cast<const IndicesAskForAnimationPhase *>(ctx.GetIndices());
				index1 = indices->vehicle;
			}
			goto LabelAskForDammage;
		case NMTAskForDammage:
			{
				Assert(dynamic_cast<const IndicesAskForDammage *>(ctx.GetIndices()))
				const IndicesAskForDammage *indices = static_cast<const IndicesAskForDammage *>(ctx.GetIndices());
				index1 = indices->who;
			}
			goto LabelAskForDammage;
		case NMTAskForSetDammage:
			{
				Assert(dynamic_cast<const IndicesAskForSetDammage *>(ctx.GetIndices()))
				const IndicesAskForSetDammage *indices = static_cast<const IndicesAskForSetDammage *>(ctx.GetIndices());
				index1 = indices->who;
			}
		LabelAskForDammage:
			{
				NetworkId id;
				if (ctx.IdxGetId(index1, id) == TMOK)
				{
					if (id.creator == STATIC_OBJECT)
					{
						for (int i=0; i<_players.Size(); i++)
						{
							NetworkPlayerInfo &info = _players[i];
							if (info.dpid == from) continue;
							if (info.state < NGSPlay) continue;
							NetworkComponent::SendMsg(info.dpid, msg, type, NMFNone);
						}
					}
					else
					{
						NetworkObjectInfo *info = GetObjectInfo(id);
						if (info)
						{
							NetworkComponent::SendMsg(info->owner, msg, type, NMFGuaranteed);
						}
						else
						{
							RptF("Server: Object %d:%d not found (message %s)", id.creator, id.id, NetworkMessageTypeNames[type]);
						}
					}
				}
			}
			break;
		case NMTAskForGetIn:
			{
				Assert(dynamic_cast<const IndicesAskForGetIn *>(ctx.GetIndices()))
				const IndicesAskForGetIn *indices = static_cast<const IndicesAskForGetIn *>(ctx.GetIndices());
				index1 = indices->vehicle;
			}
			goto LabelAskWithVehicle;
		case NMTAskForGetOut:
			{
				Assert(dynamic_cast<const IndicesAskForGetOut *>(ctx.GetIndices()))
				const IndicesAskForGetOut *indices = static_cast<const IndicesAskForGetOut *>(ctx.GetIndices());
				index1 = indices->vehicle;
			}
			goto LabelAskWithVehicle;
		case NMTAskForChangePosition:
			{
				Assert(dynamic_cast<const IndicesAskForChangePosition *>(ctx.GetIndices()))
				const IndicesAskForChangePosition *indices = static_cast<const IndicesAskForChangePosition *>(ctx.GetIndices());
				index1 = indices->vehicle;
			}
			goto LabelAskWithVehicle;
		case NMTAskForAimWeapon:
			{
				Assert(dynamic_cast<const IndicesAskForAimWeapon *>(ctx.GetIndices()))
				const IndicesAskForAimWeapon *indices = static_cast<const IndicesAskForAimWeapon *>(ctx.GetIndices());
				index1 = indices->vehicle;
			}
			goto LabelAskWithVehicle;
		case NMTAskForAimObserver:
			{
				Assert(dynamic_cast<const IndicesAskForAimObserver *>(ctx.GetIndices()))
				const IndicesAskForAimObserver *indices = static_cast<const IndicesAskForAimObserver *>(ctx.GetIndices());
				index1 = indices->vehicle;
			}
			goto LabelAskWithVehicle;
		case NMTAskForSelectWeapon:
			{
				Assert(dynamic_cast<const IndicesAskForSelectWeapon *>(ctx.GetIndices()))
				const IndicesAskForSelectWeapon *indices = static_cast<const IndicesAskForSelectWeapon *>(ctx.GetIndices());
				index1 = indices->vehicle;
			}
			goto LabelAskWithVehicle;
		case NMTAskForAmmo:
			{
				Assert(dynamic_cast<const IndicesAskForAmmo *>(ctx.GetIndices()))
				const IndicesAskForAmmo *indices = static_cast<const IndicesAskForAmmo *>(ctx.GetIndices());
				index1 = indices->vehicle;
			}
			goto LabelAskWithVehicle;
		case NMTAskForAddImpulse:
			{
				Assert(dynamic_cast<const IndicesAskForAddImpulse *>(ctx.GetIndices()))
				const IndicesAskForAddImpulse *indices = static_cast<const IndicesAskForAddImpulse *>(ctx.GetIndices());
				index1 = indices->vehicle;
			}
			goto LabelAskWithVehicle;
		case NMTAskForMoveVector:
			{
				Assert(dynamic_cast<const IndicesAskForMoveVector *>(ctx.GetIndices()))
				const IndicesAskForMoveVector *indices = static_cast<const IndicesAskForMoveVector *>(ctx.GetIndices());
				index1 = indices->vehicle;
			}
			goto LabelAskWithVehicle;
		case NMTAskForMoveMatrix:
			{
				Assert(dynamic_cast<const IndicesAskForMoveMatrix *>(ctx.GetIndices()))
				const IndicesAskForMoveMatrix *indices = static_cast<const IndicesAskForMoveMatrix *>(ctx.GetIndices());
				index1 = indices->vehicle;
			}
			goto LabelAskWithVehicle;
		case NMTAskForJoinGroup:
			{
				Assert(dynamic_cast<const IndicesAskForJoinGroup *>(ctx.GetIndices()))
				const IndicesAskForJoinGroup *indices = static_cast<const IndicesAskForJoinGroup *>(ctx.GetIndices());
				index1 = indices->join;
			}
			goto LabelAskWithVehicle;
		case NMTAskForJoinUnits:
			{
				Assert(dynamic_cast<const IndicesAskForJoinUnits *>(ctx.GetIndices()))
				const IndicesAskForJoinUnits *indices = static_cast<const IndicesAskForJoinUnits *>(ctx.GetIndices());
				index1 = indices->join;
			}
			goto LabelAskWithVehicle;
		case NMTAskForHideBody:
			{
				Assert(dynamic_cast<const IndicesAskForHideBody *>(ctx.GetIndices()))
				const IndicesAskForHideBody *indices = static_cast<const IndicesAskForHideBody *>(ctx.GetIndices());
				index1 = indices->vehicle;
			}
			goto LabelAskWithVehicle;
		case NMTUpdateWeapons:
			{
				Assert(dynamic_cast<const IndicesUpdateWeapons *>(ctx.GetIndices()))
				const IndicesUpdateWeapons *indices = static_cast<const IndicesUpdateWeapons *>(ctx.GetIndices());
				index1 = indices->vehicle;
			}
			goto LabelAskWithVehicle;
		case NMTShowTarget:
			{
				Assert(dynamic_cast<const IndicesShowTarget *>(ctx.GetIndices()))
				const IndicesShowTarget *indices = static_cast<const IndicesShowTarget *>(ctx.GetIndices());
				index1 = indices->vehicle;
			}
			goto LabelAskWithVehicle;
		case NMTShowGroupDir:
			{
				Assert(dynamic_cast<const IndicesShowGroupDir *>(ctx.GetIndices()))
				const IndicesShowGroupDir *indices = static_cast<const IndicesShowGroupDir *>(ctx.GetIndices());
				index1 = indices->vehicle;
			}
			goto LabelAskWithVehicle;
		case NMTAskForCreateUnit:
			{
				Assert(dynamic_cast<const IndicesAskForCreateUnit *>(ctx.GetIndices()))
				const IndicesAskForCreateUnit *indices = static_cast<const IndicesAskForCreateUnit *>(ctx.GetIndices());
				index1 = indices->group;
			}
			goto LabelAskWithVehicle;
		case NMTAskForDeleteVehicle:
			{
				Assert(dynamic_cast<const IndicesAskForDeleteVehicle *>(ctx.GetIndices()))
				const IndicesAskForDeleteVehicle *indices = static_cast<const IndicesAskForDeleteVehicle *>(ctx.GetIndices());
				index1 = indices->vehicle;
			}
			goto LabelAskWithVehicle;
		case NMTAskForGroupRespawn:
			{
				Assert(dynamic_cast<const IndicesAskForGroupRespawn *>(ctx.GetIndices()))
				const IndicesAskForGroupRespawn *indices = static_cast<const IndicesAskForGroupRespawn *>(ctx.GetIndices());
				index1 = indices->group;
			}
			goto LabelAskWithVehicle;
		case NMTAskForActivateMine:
			{
				Assert(dynamic_cast<const IndicesAskForActivateMine *>(ctx.GetIndices()))
				const IndicesAskForActivateMine *indices = static_cast<const IndicesAskForActivateMine *>(ctx.GetIndices());
				index1 = indices->mine;
			}
			goto LabelAskWithVehicle;
		case NMTAskForInflameFire:
			{
				Assert(dynamic_cast<const IndicesAskForInflameFire *>(ctx.GetIndices()))
				const IndicesAskForInflameFire *indices = static_cast<const IndicesAskForInflameFire *>(ctx.GetIndices());
				index1 = indices->fireplace;
			}
			goto LabelAskWithVehicle;
		case NMTMsgVTarget:
		case NMTMsgVFire:
		case NMTMsgVMove:
		case NMTMsgVFormation:
		case NMTMsgVSimpleCommand:
		case NMTMsgVLoad:
		case NMTMsgVAzimut:
		case NMTMsgVStopTurning:
		case NMTMsgVFireFailed:
			{
				Assert(dynamic_cast<const IndicesVMessage *>(ctx.GetIndices()))
				const IndicesVMessage *indices = static_cast<const IndicesVMessage *>(ctx.GetIndices());
				index1 = indices->vehicle;
			}
		LabelAskWithVehicle:
			{
				NetworkId id;
				if (ctx.IdxGetId(index1, id) == TMOK)
				{
					NetworkObjectInfo *info = GetObjectInfo(id);
					if (info)
					{
						NetworkComponent::SendMsg(info->owner, msg, type, NMFGuaranteed);
					}
					else
					{
						RptF("Server: Object %d:%d not found (message %s)", id.creator, id.id, NetworkMessageTypeNames[type]);
					}
				}
			}
			break;
		case NMTGroupRespawnDone:
			{
				Assert(dynamic_cast<const IndicesGroupRespawnDone *>(ctx.GetIndices()))
				const IndicesGroupRespawnDone *indices = static_cast<const IndicesGroupRespawnDone *>(ctx.GetIndices());
				int to;
				if (ctx.IdxTransfer(indices->to, to) == TMOK)
					NetworkComponent::SendMsg(to, msg, type, NMFGuaranteed);
			}
			goto LabelAskWithVehicle;
		case NMTAskForReceiveUnitAnswer:
			{
				Assert(dynamic_cast<const IndicesAskForReceiveUnitAnswer *>(ctx.GetIndices()))
				const IndicesAskForReceiveUnitAnswer *indices = static_cast<const IndicesAskForReceiveUnitAnswer *>(ctx.GetIndices());
				index1 = indices->to;
				// it is not vehicle index, but it does not matter
				// all we need is to be able to get owner of given network object
			}
			goto LabelAskWithVehicle;
		case NMTMarkerDelete:
			// send to all except from (state Briefing expected)
			for (int i=0; i<_players.Size(); i++)
			{
				NetworkPlayerInfo &info = _players[i];
				if (info.dpid == from) continue;
				if (info.state < NGSBriefing) continue;
				NetworkComponent::SendMsg(info.dpid, msg, type, NMFGuaranteed);
			}
			break;
		case NMTPlaySound:
		case NMTFireWeapon:
		case NMTAddWeaponCargo:
		case NMTRemoveWeaponCargo:
		case NMTAddMagazineCargo:
		case NMTRemoveMagazineCargo:
		case NMTExplosionDammageEffects:
		case NMTSoundState:
		case NMTVehicleDestroyed:
		case NMTGroupSynchronization:
		case NMTDetectorActivation:
		case NMTCopyUnitInfo:
		case NMTVehicleDamaged:
		case NMTIncomingMissile:
			// send to all except from (state Play expected)
			for (int i=0; i<_players.Size(); i++)
			{
				NetworkPlayerInfo &info = _players[i];
				if (info.dpid == from) continue;
				if (info.state < NGSPlay) continue;
				NetworkComponent::SendMsg(info.dpid, msg, type, NMFGuaranteed);
			}
			break;
		case NMTTransferFile:
		case NMTTransferMissionFile:
		case NMTVehicleInit:
		case NMTPublicVariable:
			// send to all except from (state Create expected)
			for (int i=0; i<_players.Size(); i++)
			{
				NetworkPlayerInfo &info = _players[i];
				if (info.dpid == from) continue;
				if (info.state < NGSLoadIsland) continue;
				NetworkComponent::SendMsg(info.dpid, msg, type, NMFGuaranteed);
			}
			break;
		case NMTTransferFileToServer:
			{
				TransferFileMessage transfer;
				transfer.TransferMsg(ctx);
				// check if path is valid upload path
				// if not, kick sender
				PlayerIdentity *id = FindIdentity(from);
				bool alreadyKicked = false;
				if (id && id->destroy)
				{
					alreadyKicked = true;
				}

				RString name;
				for (int i=0; i<_players.Size(); i++)
				{
					if (_players[i].dpid == from)
					{
						name = _players[i].name;
						if (_players[i].kickedOff) alreadyKicked = true;
						break;
					}
				}

				if (alreadyKicked)
				{
					LogF
					(
						"Transfer file %s request ignored, sender is being kicked off",
						(const char *)transfer.path
					);
					break;
				}
				// check if file transferred is within allowed range
				// if not, kick off sender
				if (transfer.totSize>MaxCustomFileSize)
				{
					// check name of player (from file name)
					ServerMessage
					(
						Format
						(
							"Player %s kicked off - too big custom file '%s' (%d B > %d B)",
							(const char *)name,(const char *)GetRelUploadPath(transfer.path,name),
							transfer.totSize,MaxCustomFileSize
						)
					);
					KickOff(from,KORKick);
					break;
				}

				if (!CheckValidUpload(transfer.path,name))
				{
					ServerMessage
					(
						Format
						(
							"Player %s kicked off - invalid custom file '%s'",
							(const char *)name,(const char *)transfer.path
						)
					);
					KickOff(from,KORKick);
					break;
				}

				ReceiveFileSegment(transfer);
			}
			break;
/*
		case NMTRespawn:
			{
				NetworkId group;
				if (ctx.GetId("group", group) == TMOK)
				{
					NetworkObjectInfo *info = GetObjectInfo(group);
					if (info)
					{
						NetworkComponent::SendMsg(info->owner, msg, type, NMFGuaranteed);
					}
					else
					{
						RptF("Server: Group %d:%d not found", group.creator, group.id);
					}
				}
			}
			break;
*/
		case NMTSetFlagOwner:
			{
				Assert(dynamic_cast<const IndicesSetFlagOwner *>(ctx.GetIndices()))
				const IndicesSetFlagOwner *indices = static_cast<const IndicesSetFlagOwner *>(ctx.GetIndices());
				
				NetworkId id;
				if (ctx.IdxGetId(indices->carrier, id) == TMOK)
				{
					NetworkObjectInfo *info = GetObjectInfo(id);
					if (info)
					{
						NetworkComponent::SendMsg(info->owner, msg, type, NMFGuaranteed);
					}
					else
					{
						RptF("Server: Object %d:%d not found (message %s)", id.creator, id.id, NetworkMessageTypeNames[type]);
					}
				}
			}
			break;
		case NMTSetFlagCarrier:
			{
				Assert(dynamic_cast<const IndicesSetFlagOwner *>(ctx.GetIndices()))
				const IndicesSetFlagOwner *indices = static_cast<const IndicesSetFlagOwner *>(ctx.GetIndices());

				NetworkId id;
				if (ctx.IdxGetId(indices->owner, id) == TMOK)
				{
					NetworkObjectInfo *info = GetObjectInfo(id);
					if (info)
					{
						NetworkComponent::SendMsg(info->owner, msg, type, NMFGuaranteed);
					}
					else
					{
						RptF("Server: Object %d:%d not found (message %s)", id.creator, id.id, NetworkMessageTypeNames[type]);
					}
				}
			}
			break;
		case NMTDeleteObject:
			if (_state < NGSLoadIsland) break; // updates from the last session
			{
				Assert(dynamic_cast<const IndicesDeleteObject *>(ctx.GetIndices()))
				const IndicesDeleteObject *indices = static_cast<const IndicesDeleteObject *>(ctx.GetIndices());

				NetworkId id;
				if (ctx.IdxTransfer(indices->creator, id.creator) != TMOK) break;
				if (ctx.IdxTransfer(indices->id, id.id) != TMOK) break;
				OnObjectDestroy(id);
// RptF("Server: Object %d:%d deleted", id.creator, id.id);
			}
			break;
		case NMTDeleteCommand:
			if (_state < NGSLoadIsland) break; // updates from the last session
			{
				Assert(dynamic_cast<const IndicesDeleteCommand *>(ctx.GetIndices()))
				const IndicesDeleteCommand *indices = static_cast<const IndicesDeleteCommand *>(ctx.GetIndices());

				NetworkId id;
				if (ctx.IdxTransfer(indices->creator, id.creator) != TMOK) break;
				if (ctx.IdxTransfer(indices->id, id.id) != TMOK) break;
				// destroy command object
				int owner = PerformObjectDestroy(id);
				// notify all players command was deleted
				for (int i=0; i<_players.Size(); i++)
				{
					NetworkPlayerInfo &pInfo = _players[i];
					if (pInfo.state < NGSLoadIsland) continue;
					if (pInfo.dpid == owner) continue;
					NetworkComponent::SendMsg(pInfo.dpid, msg, type, NMFGuaranteed);
				}
			}
			break;
		case NMTCreateObject:
		case NMTCreateVehicle:
		case NMTCreateDetector:
		case NMTCreateShot:
		case NMTCreateExplosion:
		case NMTCreateCrater:
		case NMTCreateCraterOnVehicle:
		case NMTCreateObjectDestructed:
		case NMTCreateAICenter:
		case NMTCreateAIGroup:
		case NMTCreateAISubgroup:
		case NMTCreateAIUnit:
		case NMTCreateCommand:
		case NMTCreateHelicopter:
			if (_state < NGSLoadIsland) break; // updates from the last session
			{
				Assert(dynamic_cast<const IndicesNetworkObject *>(ctx.GetIndices()))
				const IndicesNetworkObject *indices = static_cast<const IndicesNetworkObject *>(ctx.GetIndices());

				NetworkId id;
				if (ctx.IdxTransfer(indices->objectCreator, id.creator) != TMOK) break;
				if (ctx.IdxTransfer(indices->objectId, id.id) != TMOK) break;
				OnObjectCreate(id, from);
// RptF("Server: Object %d:%d created, type %s", id.creator, id.id, NetworkMessageTypeNames[type]);
				for (int i=0; i<_players.Size(); i++)
				{
					NetworkPlayerInfo &info = _players[i];
					if (info.dpid == from) continue;
					if (info.state < NGSLoadIsland) continue;
					NetworkComponent::SendMsg(info.dpid, msg, type, NMFGuaranteed);
				}
				if (type == NMTCreateAIUnit)
				{
					Assert(dynamic_cast<const IndicesCreateAIUnit *>(ctx.GetIndices()))
					const IndicesCreateAIUnit *indices = static_cast<const IndicesCreateAIUnit *>(ctx.GetIndices());

					NetworkId person;
					if (ctx.IdxGetId(indices->person, person) == TMOK)
					{
						for (int i=0; i<_players.Size(); i++)
						{
							AddPersonUnitPair(person, id);
							NetworkPlayerInfo &info = _players[i];
							if (info.person == person)
							{
								// change owner
								info.unit = id;
								ChangeOwner(id, from, info.dpid);
								break;
							}
						}
					}
				}
			}
			break;
		case NMTUpdateDammageVehicleAI:
		case NMTUpdateDammageObject:
			ctx.SetClass(NMCUpdateDammage);
			goto Update;
		case NMTUpdatePositionVehicle:
		case NMTUpdatePositionMan:
		case NMTUpdatePositionTank:
		case NMTUpdatePositionCar:
		case NMTUpdatePositionAirplane:
		case NMTUpdatePositionHelicopter:
		case NMTUpdatePositionShip:
		case NMTUpdatePositionSeagull:
		case NMTUpdatePositionMotorcycle:
			ctx.SetClass(NMCUpdatePosition);
			// continue with update
		case NMTUpdateTransport:
		case NMTUpdateTankOrCar:
		case NMTUpdateTank:
		case NMTUpdateCar:
		case NMTUpdateAirplane:
		case NMTUpdateHelicopter:
		case NMTUpdateParachute:
		case NMTUpdateShip:
		case NMTUpdateSeagull:
		case NMTUpdateObject:
		case NMTUpdateVehicle:
		case NMTUpdateDetector:
		case NMTUpdateFlag:
		case NMTUpdateShot:
		case NMTUpdateMine:
		case NMTUpdateVehicleAI:
		case NMTUpdateVehicleBrain:
		case NMTUpdateVehicleSupply:
		case NMTUpdateMan:
		case NMTUpdateAICenter:
		case NMTUpdateAIGroup:
		case NMTUpdateAISubgroup:
		case NMTUpdateAIUnit:
		case NMTUpdateCommand:
		case NMTUpdateMotorcycle:
		case NMTUpdateFireplace:
		Update:
			if (_state < NGSLoadIsland) break; // updates from the last session
			{
				Assert(dynamic_cast<const IndicesNetworkObject *>(ctx.GetIndices()))
				const IndicesNetworkObject *indices = static_cast<const IndicesNetworkObject *>(ctx.GetIndices());

				NetworkId id;
				if (ctx.IdxTransfer(indices->objectCreator, id.creator) != TMOK) break;
				if (ctx.IdxTransfer(indices->objectId, id.id) != TMOK) break;
				NetworkObjectInfo *oInfo = OnObjectUpdate(id, from, msg, type, ctx.GetClass());
				if (!oInfo) break;
				bool guaranteed;
				if (ctx.IdxTransfer(indices->guaranteed, guaranteed) != TMOK) guaranteed = false;
				// if (_state < NGSPlay)
				if (guaranteed)
				{
					// init state transfer, send immediately
					for (int i=0; i<_players.Size(); i++)
					{
						NetworkPlayerInfo &pInfo = _players[i];
						if (pInfo.dpid == from) continue;
						if (pInfo.state < NGSLoadIsland) continue;

						UpdateObject(&pInfo, oInfo, ctx.GetClass(), NMFGuaranteed);
					}
				}

				if (ctx.GetClass() != NMCUpdateGeneric) break;
				if (IsUpdateTransport(type))
				{
					Assert(dynamic_cast<const IndicesUpdateTransport *>(ctx.GetIndices()))
					const IndicesUpdateTransport *indices = static_cast<const IndicesUpdateTransport *>(ctx.GetIndices());

					NetworkId driver;
					// unit is always simulated where driver is local
					// if there is no driver, it can be simulated on any computer
					bool ownerKnown = false;

					if (ctx.IdxGetId(indices->driver, driver) == TMOK)
					{
						// check for NULL id
						if (!driver.IsNull())
						{
							// check who is owner of the driver
							for (int i=0; i<_objects.Size(); i++)
							{
								NetworkObjectInfo &dInfo = *_objects[i];
								if (!dInfo.current) continue;
								if (dInfo.id == driver)
								{
									ChangeOwner(id, oInfo->owner, dInfo.owner);
									ownerKnown = true;
									break;
								}
							}
							if (!ownerKnown)
							{
								LogF("Owner not known - searching player");
								for (int i=0; i<_players.Size(); i++)
								{
									NetworkPlayerInfo &pInfo = _players[i];
									if (pInfo.person == driver)
									{
										// change owner
										LogF("Owner not known - player found");
										ChangeOwner(id, oInfo->owner, pInfo.dpid);
									}
								}
							}
						}
					}
				}
				else if (type == NMTUpdateAIGroup)
				{
					Assert(dynamic_cast<const IndicesUpdateAIGroup *>(ctx.GetIndices()))
					const IndicesUpdateAIGroup *indices = static_cast<const IndicesUpdateAIGroup *>(ctx.GetIndices());

					NetworkId leader;
					if (ctx.IdxGetId(indices->leader, leader) == TMOK)
					UpdateGroupLeader(id, leader);
				}
			}
			break;
		case NMTUpdateClientInfo:
			{
				ClientInfoObject clientInfo;
				clientInfo.TransferMsg(ctx);
				NetworkPlayerInfo *pInfo = GetPlayerInfo(from);
				if (pInfo && msg->time > pInfo->cameraPositionTime)
				{
					pInfo->cameraPosition = clientInfo._cameraPosition;
					pInfo->cameraPositionTime = msg->time;
				}
			}
			break;
		case NMTChat:
			{
				Assert(dynamic_cast<const IndicesChat *>(ctx.GetIndices()))
				const IndicesChat *indices = static_cast<const IndicesChat *>(ctx.GetIndices());

				AutoArray<NetworkId> units;
				if (ctx.IdxGetIds(indices->units, units) != TMOK) break;
				AUTO_STATIC_ARRAY(int, players, 32);
				int channel;
				if (ctx.IdxTransfer(indices->channel, channel) != TMOK) break;
				if (channel != CCGlobal && units.Size() > 0)
					GetPlayersOnChannel(players, units, from, false);
				else
					GetPlayersOnChannel(players, channel, from, false);
  
        if (indices->text>=0)
        {
          // read message text
          RString text;
      	  ctx.IdxTransfer(indices->text, text);
          const int maxLength = 200;
          if (text.GetLength()>maxLength)
          {
            // reduce text length
            text = text.Substring(0,maxLength);
        		msg->values[indices->text] = RefNetworkDataTyped<RString>(text);
          }
        }

				for (int i=0; i<players.Size(); i++)
				{
					NetMsgFlags priority =
					(
						_state==NGSPlay ? NMFNone : NMFHighPriority
					); 
					NetworkComponent::SendMsg(players[i], msg, type, NMFGuaranteed|priority);
				}
			}
			break;
		case NMTRadioChat:
			{
				Assert(dynamic_cast<const IndicesRadioChat *>(ctx.GetIndices()))
				const IndicesRadioChat *indices = static_cast<const IndicesRadioChat *>(ctx.GetIndices());
				index1 = indices->units;
				index2 = indices->channel;
			}
			goto LabelChat;
		case NMTRadioChatWave:
			{
				Assert(dynamic_cast<const IndicesRadioChatWave *>(ctx.GetIndices()))
				const IndicesRadioChatWave *indices = static_cast<const IndicesRadioChatWave *>(ctx.GetIndices());
				index1 = indices->units;
				index2 = indices->channel;
			}
			goto LabelChat;
		case NMTMarkerCreate:
			{
				Assert(dynamic_cast<const IndicesMarkerCreate *>(ctx.GetIndices()))
				const IndicesMarkerCreate *indices = static_cast<const IndicesMarkerCreate *>(ctx.GetIndices());
				index1 = indices->units;
				index2 = indices->channel;
			}
		LabelChat:
			{
				AutoArray<NetworkId> units;
				if (ctx.IdxGetIds(index1, units) != TMOK) break;
				AUTO_STATIC_ARRAY(int, players, 32);
				int channel;
				if (ctx.IdxTransfer(index2, channel) != TMOK) break;
				if (channel != CCGlobal)
					GetPlayersOnChannel(players, units, from, false);
				else
					GetPlayersOnChannel(players, channel, from, false);
				for (int i=0; i<players.Size(); i++)
				{
					NetworkComponent::SendMsg(players[i], msg, type, NMFGuaranteed);
				}
			}
			break;
		case NMTSetVoiceChannel:
			{
				Assert(dynamic_cast<const IndicesSetVoiceChannel *>(ctx.GetIndices()))
				const IndicesSetVoiceChannel *indices = static_cast<const IndicesSetVoiceChannel *>(ctx.GetIndices());

				AutoArray<NetworkId> units;
				if (ctx.IdxGetIds(indices->units, units) != TMOK) break;
				AUTO_STATIC_ARRAY(int, players, 32);
				int channel;
				if (ctx.IdxTransfer(indices->channel, channel) != TMOK) break;
				if (channel != CCGlobal && units.Size() > 0)
					GetPlayersOnChannel(players, units, from, true);
				else
					GetPlayersOnChannel(players, channel, from, true);
				NetworkPlayerInfo *info = GetPlayerInfo(from);
				if (info)
				{
					if (info->channel == CCDirect)
					{
						AUTO_STATIC_ARRAY(int, oldPlayers, 32);
						_server->GetTransmitTargets(from, oldPlayers);
						SetSpeakerMessage message(from, false, info->person);
						for (int i=0; i<oldPlayers.Size(); i++)
						{
							SendMsg(oldPlayers[i], &message, NMFGuaranteed);
						}
					}
					if (channel == CCDirect)
					{
						SetSpeakerMessage message(from, true, info->person);
						for (int i=0; i<players.Size(); i++)
						{
							SendMsg(players[i], &message, NMFGuaranteed);
						}
					}
					info->channel = channel;
				}
				_server->SetTransmitTargets(from, players);
			}
			break;
		case NMTNetworkCommand:
			{
				NetworkCommandMessage cmd;
				cmd.TransferMsg(ctx);
				switch (cmd.type)
				{
#if _ENABLE_DEDICATED_SERVER
				case NCMTLogin:
					{
						if (_dedicated && (_gameMaster == AI_PLAYER || _admin))
						{
							RString name;
              int ident;
							for (ident=0; ident<_identities.Size(); ident++)
              {
                if (_identities[ident].dpnid==from)
                {
                  name = name + RString(" (id=") + _identities[ident].id + RString(")");
                  break;
                }
              }
              if (ident<_identities.Size())
              {
							  RString password = cmd.content.ReadString();
							  ParamEntry *entry = _serverCfg.FindEntry("passwordAdmin");
							  if (entry)
							  {
								  RString realPassword = *entry;
								  if (strcmp(password, realPassword) != 0)
                  {
                    if (++_identities[ident].failedLogin>=10)
                    {
                      ConsoleF("Admin login rejected for %s", (const char *)name);
                      ConsoleF("  Reason: Too many attempts with bad passwords");
                      KickOff(from,KOROther);
                    }
                    break;
                  }
							  }

                // if password is ok, reset the counter
                _identities[ident].failedLogin = 0;

							  if (_gameMaster != AI_PLAYER)
							  {
								  NetworkCommandMessage answer;
								  answer.type = NCMTLoggedOut;
								  SendMsg(_gameMaster, &answer, NMFGuaranteed);
							  }

							  _gameMaster = from;
							  _admin = false;
							  ConsoleF("Admin %s logged in.", (const char *)name);

							  _votings.Clear();

							  NetworkCommandMessage answer;
							  answer.type = NCMTLogged;
							  answer.content.Write(&_admin, sizeof(_admin));
							  // AddMissionList(answer);
							  SendMsg(from, &answer, NMFGuaranteed);

							  if (_mission[0] == '?' && _mission[1] == 0)
							  {
								  NetworkCommandMessage answer;
								  answer.type = NCMTVoteMission;
								  AddMissionList(answer);
								  SendMsg(from, &answer, NMFGuaranteed);
							  }

							  UpdateAdminState();
              }
						}
					}
					break;
				case NCMTLogout:
					if (_dedicated && from == _gameMaster)
					{
						NetworkCommandMessage answer;
						answer.type = NCMTLoggedOut;
						SendMsg(_gameMaster, &answer, NMFGuaranteed);

						RString name;
						for (int i=0; i<_players.Size(); i++)
							if (_players[i].dpid == _gameMaster)
							{
								name = _players[i].name;
								break;
							}
						ConsoleF("Admin %s logged out.", (const char *)name);

						_gameMaster = AI_PLAYER;
						_sessionLocked = false;
						_debugOn.Clear();

						UpdateAdminState();

						if (_mission[0] == '?' && _mission[1] == 0)
						{
							// when admin logged out, perform mission voting instead
							NetworkCommandMessage answer;
							answer.type = NCMTVoteMission;
							AddMissionList(answer);
							for (int i=0; i<_identities.Size(); i++)
								SendMsg(_identities[i].dpnid, &answer, NMFGuaranteed);
						}
					}
					break;
				case NCMTShutdown:
					if (_dedicated && from == _gameMaster && !_admin)
					{
            #ifdef _WIN32
						::PostMessage(hwndApp, WM_CLOSE, 0, 0);
            #else
            interrupted = true;
            #endif
					}
					break;
				case NCMTMonitorAsk:
					if (_dedicated && from == _gameMaster)
					{
						float interval;
						cmd.content.Read(&interval, sizeof(interval));
						Monitor(interval);
					}
					break;
				case NCMTDebugAsk:
					if
					(
						_dedicated && from == _gameMaster ||
						!_dedicated && from==_botClient
					)
					{
						DebugAsk(cmd.content.ReadString(),from,!_admin);
					}
					break;
				case NCMTMission:
					if (_dedicated && from == _gameMaster)
					{
						_mission = cmd.content.ReadString();
						cmd.content.Read(&_cadetMode, sizeof(bool));
						if (_mission.GetLength() > 0) _restart = true;
						if (_state != NGSPlay)
						{
							GNetworkManager.DestroyAllObjects();
							SetGameState(NGSCreate);
						}
					}
					break;
				case NCMTMissions:
					if (_dedicated && from == _gameMaster)
					{
						_mission = "?";
						_restart = true;
						if (_state != NGSPlay)
						{
							GNetworkManager.DestroyAllObjects();
							SetGameState(NGSCreate);
						}
						NetworkCommandMessage answer;
						answer.type = NCMTVoteMission;
						AddMissionList(answer);
						SendMsg(_gameMaster, &answer, NMFGuaranteed);
					}
					break;
				case NCMTRestart:
					if (_dedicated && _state == NGSPlay && from == _gameMaster) _restart = true;
					break;
				case NCMTReassign:
					if (_dedicated && from == _gameMaster)
					{
						_restart = true;
						_reassign = true;
						if (_state != NGSPlay)
						{
							GNetworkManager.DestroyAllObjects();

							// unassign all players
							#if _ENABLE_AI
								int defaultPlayer = _missionHeader.disabledAI ? NO_PLAYER : AI_PLAYER;
							#else
								int defaultPlayer = NO_PLAYER;
							#endif
							for (int i=0; i<_playerRoles.Size(); i++)
							{
								_playerRoles[i].player = defaultPlayer;
								_playerRoles[i].roleLocked = false;
							}
							// send to clients
							for (int i=0; i<_players.Size(); i++)
							{
								NetworkPlayerInfo &info = _players[i];
								if (info.state < NGSCreate) continue;
								SendMissionInfo(info.dpid, true);
								info.state = NGSPrepareSide;
								SetPlayerState(info.dpid, NGSPrepareSide);
							}

							SetGameState(NGSPrepareSide);

							_restart = false;
							_reassign = false;
						}
					}
					break;
				case NCMTInit:
					if (_dedicated && from == _gameMaster)
					{
						RString GetServerConfig();
						_serverCfg.Parse(GetServerConfig());
					}
					break;
				case NCMTLockSession:
					if (_dedicated && from == _gameMaster && !_admin)
					{
						bool lock;
						if (cmd.content.Read(&lock, sizeof(bool)))
							_sessionLocked = lock;
					}
					break;
#endif
				case NCMTKick:
					if
					(
						from == _botClient
#if _ENABLE_DEDICATED_SERVER
						|| from == _gameMaster
#endif
					)
					{
						int player;
						if
						(
							cmd.content.Read(&player, sizeof(int)) &&
							player != _botClient
						) KickOff(player,KORKick);
					}
					break;
#if _ENABLE_DEDICATED_SERVER
				case NCMTVote:
					if (_dedicated && _gameMaster == AI_PLAYER)
					{
						const NetworkPlayerInfo *info = GetPlayerInfo(from);
						if (!info) break;
						bool doEcho = false;
						char echo[512]; sprintf(echo, "%s votes: ", (const char *)info->name);
						int subtype;
						cmd.content.Read(&subtype, sizeof(int));
						switch (subtype)
						{
							case NCMTMission:
								{
									char *ptr = cmd.content.Data() + cmd.content.GetPos();
									int size = cmd.content.Size() - cmd.content.GetPos();
									_votings.Add
									(
										this, (char *)&subtype, sizeof(int), 0.9999, from,
										ptr, size, true
									);
									sprintf(echo + strlen(echo), "Mission %s", ptr); doEcho = true;
								}
								break;
							case NCMTMissions:
								_votings.Add(this, (char *)&subtype, sizeof(int), _voteThreshold, from);
								strcat(echo, "Missions"); doEcho = true;
								break;
							case NCMTRestart:
								_votings.Add(this, (char *)&subtype, sizeof(int), _voteThreshold, from);
								strcat(echo, "Restart"); doEcho = true;
								break;
							case NCMTReassign:
								_votings.Add(this, (char *)&subtype, sizeof(int), _voteThreshold, from);
								strcat(echo, "Reassign"); doEcho = true;
								break;
							case NCMTKick:
								{
									int id[2];
									id[0] = subtype;
									cmd.content.Read(&id[1], sizeof(int));
									_votings.Add(this, (char *)id, 2 * sizeof(int), _voteThreshold, from);
									const NetworkPlayerInfo *info = GetPlayerInfo(id[1]);
									if (info)
									{
										sprintf(echo + strlen(echo), "Kick %s", (const char *)info->name); doEcho = true;
									}
								}
								break;
							case NCMTAdmin:
								{
									char *ptr = cmd.content.Data() + cmd.content.GetPos();
									int size = cmd.content.Size() - cmd.content.GetPos();
									_votings.Add
									(
										this, (char *)&subtype, sizeof(int), _voteThreshold, from,
										ptr, size
									);
									int player = *(int *)ptr;
									RString name;
									for (int i=0; i<_players.Size(); i++)
										if (_players[i].dpid == player)
										{
											name = _players[i].name;
											break;
										}
									sprintf(echo + strlen(echo), "Admin %s", (const char *)name); doEcho = true;
								}
								break;
						}
						// Send voting echo to all other players
						if (doEcho)
						{
							RefArray<NetworkObject> dummy;
							ChatMessage msg(CCGlobal, NULL, dummy, "", echo);
							for (int i=0; i<_players.Size(); i++)
							{
								if (_players[i].state >= NGSCreate)
									SendMsg(_players[i].dpid, &msg, NMFGuaranteed);
							}
						}
					}
					break;
#endif
				default:
					Fail("Network command type");
					break;
				}
			}
			break;
		case NMTMissionParams:
			{
				MissionParamsMessage params;
				params.TransferMsg(ctx);

				_param1 = params._param1;
				_param2 = params._param2;

				for (int i=0; i<_players.Size(); i++)
				{
					NetworkPlayerInfo &info = _players[i];
					if (info.dpid == from) continue;
					if (info.state < NGSCreate) continue;
					NetworkComponent::SendMsg(info.dpid, msg, type, NMFGuaranteed);
				}
			}
			break;
		default:
			RptF("Server: Unhandled user message %d", (int)type);
			break;
	}
}

static bool IsAllDisabled(const AutoArray<PlayerRole> &roles)
{
	bool allDisabled = true;
	bool allPlayers = true;
	for (int i=0; i<roles.Size(); i++)
	{
		const PlayerRole &role = roles[i];
		if (role.player == AI_PLAYER)
		{
			allDisabled = false;
			allPlayers = false;
			break;
		}
		else if (role.player == NO_PLAYER)
			allPlayers = false;
	}
	return allDisabled && !allPlayers;
}

void NetworkServer::OnMessagePlayerRole(int from, NetworkMessageType type, NetworkMessageContext &ctx)
{
	// PATCHED
	if (_state != NGSPrepareSide) return;

	Assert(dynamic_cast<const IndicesPlayerRole *>(ctx.GetIndices()))
	const IndicesPlayerRole *indices = static_cast<const IndicesPlayerRole *>(ctx.GetIndices());

	int index;
	ctx.IdxTransfer(indices->index, index);
	Assert(index < _playerRoles.Size());
	PlayerRole info;
	info.TransferMsg(ctx);

	// ADDED in patch 1.01 - disable AI
#if _ENABLE_AI
	int noPlayer = _missionHeader.disabledAI ? NO_PLAYER : AI_PLAYER;
#else
	int noPlayer = NO_PLAYER;
#endif
	if (info.player == AI_PLAYER) info.player = noPlayer;
	
	if (noPlayer == AI_PLAYER)
	{
		// if all AI is disabled, replace removed player by NO_PLAYER
		if (IsAllDisabled(_playerRoles)) noPlayer = NO_PLAYER;
	}

	int iFound = -1;
	if (info.player != NO_PLAYER && info.player != AI_PLAYER)
		for (int i=0; i<_playerRoles.Size(); i++)
		{
			if (_playerRoles[i].player == info.player)
			{
				iFound = i;
				break;
			}
		}
	
	bool lock = 
#if _ENABLE_DEDICATED_SERVER
			from == _gameMaster ||
#endif
			from == _botClient;

	if (lock)
	{
		if (iFound >= 0)
		{
			_playerRoles[iFound].player = noPlayer;
			_playerRoles[iFound].roleLocked = false;
		}
		info.roleLocked = (info.player != AI_PLAYER && info.player != NO_PLAYER && info.player != from);
		_playerRoles[index] = info;
	}
	else
	{
		if (_playerRoles[index].roleLocked) return;
		if (iFound >= 0)
		{
			if (_playerRoles[iFound].roleLocked) return;
		}
		if
		(
			(
				_playerRoles[index].player == AI_PLAYER ||
				_playerRoles[index].player == NO_PLAYER
			) && info.player == from ||
			_playerRoles[index].player == from &&
			(
				info.player == AI_PLAYER ||
				info.player == NO_PLAYER
			) ||
			_playerRoles[index].player == AI_PLAYER && info.player == NO_PLAYER ||	// disable AI
			_playerRoles[index].player == NO_PLAYER && info.player == AI_PLAYER			// enable AI
		)
		{
			_playerRoles[index] = info;
		}
		else return;
		if (iFound >= 0)
		{
			_playerRoles[iFound].player = noPlayer;
			_playerRoles[iFound].roleLocked = false;
		}
	}

	{
		// create message
		Ref<NetworkMessage> msgSend = new NetworkMessage();
		msgSend->time = Glob.time;
		NetworkMessageContext ctxSend(msgSend, ctx.GetFormat(), this, TO_SERVER, MSG_SEND);

		Assert(dynamic_cast<const IndicesPlayerRole *>(ctxSend.GetIndices()))
		const IndicesPlayerRole *indices = static_cast<const IndicesPlayerRole *>(ctxSend.GetIndices());

		TMError err = ctxSend.IdxTransfer(indices->index, index);
		if (err != TMOK) return;
		err = info.TransferMsg(ctxSend);
		if (err != TMOK) return;
		
		// send message
		for (int i=0; i<_players.Size(); i++)
		{
			NetworkPlayerInfo &player = _players[i];
			if (player.state < NGSCreate) continue;
//					SendMsg(player.dpid, &info, 0, index, NMFGuaranteed);
			NetworkComponent::SendMsg(player.dpid, msgSend, type, NMFGuaranteed);
		}
	}
	if (iFound >= 0)
	{
		// create message
		Ref<NetworkMessage> msgSend = new NetworkMessage();
		msgSend->time = Glob.time;
		NetworkMessageContext ctxSend(msgSend, ctx.GetFormat(), this, TO_SERVER, MSG_SEND);

		Assert(dynamic_cast<const IndicesPlayerRole *>(ctxSend.GetIndices()))
		const IndicesPlayerRole *indices = static_cast<const IndicesPlayerRole *>(ctxSend.GetIndices());

		TMError err = ctxSend.IdxTransfer(indices->index, iFound);
		if (err != TMOK) return;
		err = _playerRoles[iFound].TransferMsg(ctxSend);
		if (err != TMOK) return;
		
		// send message
		for (int i=0; i<_players.Size(); i++)
		{
			NetworkPlayerInfo &player = _players[i];
			if (player.state < NGSCreate) continue;
//					SendMsg(player.dpid, &info, 0, index, NMFGuaranteed);
			NetworkComponent::SendMsg(player.dpid, msgSend, type, NMFGuaranteed);
		}
	}
}

DWORD NetworkServer::SendMsg
(
	int to, NetworkSimpleObject *object, NetMsgFlags dwFlags
)
{
	// check if message can be sent
	NetworkPlayerInfo *info = GetPlayerInfo(to);
	if (!info)
	{
		RptF("Server: cannot send message - player %d is not known.", to);
		return 0xFFFFFFFF;
	}
	if (info->state < NGSCreate && object->GetNMType() >= NMTFirstVariant)
	{
		RptF("Server: cannot send message - player's %s messages are not registred yet.", (const char *)info->name);
		return 0xFFFFFFFF;
	}
	return NetworkComponent::SendMsg(to, object, dwFlags);
}

void NetworkServer::EnqueueMsg
(
	int to, NetworkMessage *msg,
	NetworkMessageType type
)
{
	NetworkPlayerInfo *info = GetPlayerInfo(to);
	if (!info)
	{
		Fail("PlayerInfo");
		return;
	}
	// if 
	int index = info->_messageQueue.Add();
	info->_messageQueue[index].type = type;
	info->_messageQueue[index].msg = msg;
}

void NetworkServer::EnqueueMsgNonGuaranteed
(
	int to, NetworkMessage *msg,
	NetworkMessageType type
)
{
	NetworkPlayerInfo *info = GetPlayerInfo(to);
	if (!info)
	{
		Fail("PlayerInfo");
		return;
	}
	// if 
	int index = info->_messageQueueNonGuaranteed.Add();
	info->_messageQueueNonGuaranteed[index].type = type;
	info->_messageQueueNonGuaranteed[index].msg = msg;
}

/*!
\patch 1.43 Date 1/23/2002 by Ondra
- Fixed: Config protection message "... uses modified config file", is now more specific,
exact location of changed value is displayed.
- New: When user with different version is connected,
message containing version information is shown.
*/

bool NetworkServer::IntegrityCheck(int dpnid, IntegrityQuestionType type, const IntegrityQuestion &q)
{
	NetworkPlayerInfo *info = GetPlayerInfo(dpnid);
	if (!info) return false;
	//if (info->integrityQuestions.SizeTimeout[type] != UINT_MAX) return false; // integrity check in progress

	const int timeout = 120000; // in ms
	int id = info->nextQuestionId++;

	//info->integrityQuestion = value;
	IntegrityQuestionInfo &qi = info->integrityQuestions.Append();
	qi.id = id;
	qi.type = type;
	qi.timeout = GlobalTickCount() + timeout;
	qi.q = q;

	#if _ENABLE_REPORT
		if (type==IQTData)
		{
			LogF("Asking %s about file %s",(const char *)info->name,(const char *)q.name);
		}
	#endif

	IntegrityQuestionMessage msg;
	msg.id = id;
	msg.type = type;
	msg.q = q;
	SendMsg(dpnid, &msg, NMFGuaranteed|NMFHighPriority);
	return true;
}

const ParamEntry *FindConfigParamEntry(const char *path);

IntegrityInvestigationConfig::IntegrityInvestigationConfig(const char *path)
{
	// find entry by query
	_root = NULL;
	const ParamEntry *entry = FindConfigParamEntry(path);
	if (!entry) entry = &Pars;
	if (!entry->IsClass())
	{
		_class = NULL;
		return;
	}
	Assert(dynamic_cast<const ParamClass *>(entry));

	_class = static_cast<const ParamClass *>(entry);
	_root = _class;
	_questionTimeout = UINT_MAX;
}

RString IntegrityInvestigationConfig::GetResult() const
{
	return _result;
}

static RString PathFirstFolder(const char *path)
{
	const char *next = strchr(path,'/');
	if (!next) return "";
	return RString(path,next-path);
}

static RString ConvertContextToCfgPath(RString path)
{
	RString base = PathFirstFolder(path);
	if (!strcmpi(base,Pars.GetName()) )
	{
		RString stripBase = path.Substring(base.GetLength(),INT_MAX);
		return RString("cfg")+stripBase;
	}
	else if (!strcmpi(base,Res.GetName()) )
	{
		RString stripBase = path.Substring(base.GetLength(),INT_MAX);
		return RString("rsc")+stripBase;
	}
	return path;	
}

bool IntegrityInvestigationConfig::Proceed(NetworkServer *server, NetworkPlayerInfo *pi)
{
	//if (_result.GetLength()>0) return false;
	if (!_class)
	{
		return false;
	}
	// if some question is in progress, wait until it is answered
	if (_questionTimeout<UINT_MAX)
	{
		// if investigation timed out, give result
		if (GlobalTickCount()>_questionTimeout)
		{
			Log("Investigation timed out");
			_result = "Time out (probably cheat is used)";
			return false;
		}
		return true;
	}

	// we may ask client only about verified class or about parent of verified class
	// otherwise we have no guarantee that he is able to reply, as class
	// may be non-existing in his config
	if (_class==_root)
	{
		// _root is already known to be invalid, there is no need to ask about it
		QuestionAnswered(false);
	}
	else if (_class->HasChecksum() && _class!=_root)
	{
		// perform test with current class
		RString cfgPath = ConvertContextToCfgPath(_class->GetContext());
		_question = IntegrityQuestion(cfgPath,0,0);
		// we expect to get answer within two minutes
		// if not, we consider this test failed
		_questionTimeout = GlobalTickCount()+120000;
		// ask given player this question
		#if 1
		LogF
		(
			"Asking player %s about %s",
			(const char *)pi->name,(const char *)_class->GetContext()
		);
		#endif
		server->IntegrityCheck(pi->dpid,IQTConfig,_question);
	}
	else
	{
		// class not protected - skip it
		// pretend we received an answer class is OK
		do
		{
			#if 0
			Log
			(
				"Skipping player %s about %s",
				(const char *)pi->name,(const char *)_class->GetContext()
			);
			#endif
			QuestionAnswered(true);
		}
		while (_class && !_class->HasChecksum());
	}
	
	return true;
}

bool IntegrityInvestigationConfig::QuestionMatching(const IntegrityQuestion &q) const
{
	if (q.name!=_question.name) return false;
	//if (q.offset!=_question.offset) return false;
	//if (q.size!=_question.size) return false;
	return true;
}

void IntegrityInvestigationConfig::QuestionAnswered(bool answerOK)
{
	_questionTimeout = UINT_MAX;
	// update investigation, if neccessary, set error code
	if (!answerOK)
	{
		_result = _question.name;
	}
	// if question was answered with no problems detected, we may proceed to next class
	// we should:
	// 1) pass control to our first child
	// 2) if there is none, find next class in our parent
	// if class reply is OK, there is no need to check children
	if (!answerOK)
	for (int i=0; i<_class->GetEntryCount(); i++)
	{
		const ParamEntry *entry = &_class->GetEntry(i);
		if (!entry->IsClass()) continue;
		Assert(dynamic_cast<const ParamClass *>(entry));
		_class = static_cast<const ParamClass *>(entry);
		return;
	}
	// no child, return back to our parent and find its next child
	// check if there are any more entries in current class
	/*
	if (_result.GetLength()>0)
	{
		// if we already have some result, we have to deepest node
		// we therefore want to finish
		_class = NULL;
		return;
	}
	*/
	for(;;)
	{
		if (_class==_root)
		{
			// we reached root - terminate
			if (_class && _result.GetLength()==0)
			{
				_result = ConvertContextToCfgPath(_class->GetContext());
			}
			_class = NULL;
			return;
		}
		const ParamClass *parent = _class->GetParent();
		if (!parent)
		{
			if (_class==&Pars)
			{
				// after completed with Pars, Res should be processed
				// unless _root is Pars, but this is already handled by _class=_root
				_class = &Res;
				return;
			}
			_class = NULL;
			return;
		}
		// find _class in its parent
		int index = -1;
		for (int i=0; i<parent->GetEntryCount(); i++)
		{
			if (&parent->GetEntry(i)==_class) {index = i+1;break;}
		}
		Assert(index>=0);
		// find next child of our parent
		while (index<parent->GetEntryCount() && !parent->GetEntry(index).IsClass())
		{
			index++;
		}
		if (index>=parent->GetEntryCount())
		{
			// parent has no more children, proceed with its parent
			_class = parent;
		}
		else
		{
			const ParamEntry *entry = &parent->GetEntry(index);
			Assert (entry->IsClass());
			Assert(dynamic_cast<const ParamClass *>(entry));
			_class = static_cast<const ParamClass *>(entry);
			return;
		}
	}
}

/*!
Check if given question is part of any investigation
and let investiogation process it.

\return true if question was part of investigation
*/


bool NetworkServer::IntegrityAnswerReceived
(
	NetworkPlayerInfo *pi,
	IntegrityQuestionType qType,
	const IntegrityQuestion &q, bool answerOK
)
{
	for (int qt=0; qt<IntegrityQuestionTypeCount; qt++)
	{
		IntegrityInvestigation *ii = pi->integrityInvestigation[qt];
		if (!ii) continue;
		// check if given answer is answer to any given question
		if (ii->QuestionMatching(q))
		{
			// update result
			ii->QuestionAnswered(answerOK);
			return true;
		}
	}
	return false;
}

void NetworkServer::PerformInitialIntegrityCheck(NetworkPlayerInfo &pi)
{
	IntegrityCheck(pi.dpid,IQTConfig,IntegrityQuestion());
#if _ENABLE_DEDICATED_SERVER
	const ParamEntry *datafiles = _serverCfg.FindEntry("CheckFiles");
	if (datafiles)
	{
		for (int i=0; i<datafiles->GetSize(); i++)
		{
			RStringB file = (*datafiles)[i];
			IntegrityCheck(pi.dpid,IQTData,IntegrityQuestion(file,0,INT_MAX));
		}
	}
#endif
  const ExeCRCBlock *GetCodeCheck();
  for (const ExeCRCBlock *crc = GetCodeCheck(); crc->size>0; crc++)
  {
    // perform only one test here  - ID changer
    IntegrityCheck(pi.dpid,IQTExe,IntegrityQuestion("",crc->offset,crc->size));
    break;
  }

	//IntegrityCheck(player,IQTExe,IntegrityQuestion());
	//IntegrityCheck(player,IQTData,IntegrityQuestion());

}

void NetworkServer::PerformFileIntegrityCheck(const char *file, int dpid)
{
	IntegrityQuestion q;
	q.name = file;
	
	if (dpid>=0)
	{
		IntegrityCheck(dpid,IQTData,q);
	}
	else
	{
		for (int i=0; i<_players.Size(); i++)
		{
			const NetworkPlayerInfo &pi = _players[i];
      if (pi.dpid==_botClient) continue;
			IntegrityCheck(pi.dpid,IQTData,q);
		}
	}
}

void NetworkServer::PerformExeIntegrityCheck(const char *location, int dpid)
{
  int offset=0,size=0;
  sscanf(location,"%x:%x",&offset,&size);
  if (size<=0) return;
  const int granularity = 1024;
  int align = offset&(granularity-1);
  offset -= align;
  size += align;
  int alignSize = (granularity-size)&(granularity-1);
  size += alignSize;
  while (size>0)
  {
    int checkSize = size<granularity ? size : granularity;
    // perform a granular check
	  IntegrityQuestion q;
	  q.name = 0;
    q.offset = offset;
    q.size = checkSize;
	  
	  if (dpid>=0)
	  {
		  IntegrityCheck(dpid,IQTExe,q);
	  }
	  else
	  {
		  for (int i=0; i<_players.Size(); i++)
		  {
			  const NetworkPlayerInfo &pi = _players[i];
        if (pi.dpid==_botClient) continue;
			  IntegrityCheck(pi.dpid,IQTExe,q);
		  }
	  }
    offset += checkSize;
    size -= checkSize;
  }
}

void NetworkServer::PerformRandomIntegrityCheck(NetworkPlayerInfo &pi)
{
  const ExeCRCBlock *GetCodeCheck();
  for (const ExeCRCBlock *crc = GetCodeCheck(); crc->size>0; crc++)
  {
    IntegrityCheck(pi.dpid,IQTExe,IntegrityQuestion("",crc->offset,crc->size));
  }
  /*
	// select randomly from first generation of verified classes
	const ParamClass *selC = &Pars;
	for(;;)
	{
		float random = GRandGen.RandomValue();
		const float thold = 0.8f;
		const float invThold = 1/thold;
		if (random>=thold)
		{
			// we are happy with this class
			break;
		}
		// we want to select some child
		random *= invThold;
		int nClasses = 0;
		for (int i=0; i<selC->GetEntryCount(); i++)
		{
			const ParamEntry &entry = selC->GetEntry(i);
			if (!entry.IsClass()) continue;
			const ParamClass &entryC = static_cast<const ParamClass &>(entry);
			if (!entryC.HasChecksum()) continue;
			nClasses++;
		}
		if (nClasses<=0)
		{
			// no selectable children - we must be content with this class
			break;
		}
		const ParamClass *selChild = NULL;
		int sel = toIntFloor(random*nClasses);
		saturate(sel,0,nClasses-1);
		for (int i=0; i<selC->GetEntryCount(); i++)
		{
			const ParamEntry &entry = selC->GetEntry(i);
			if (!entry.IsClass()) continue;
			const ParamClass &entryC = static_cast<const ParamClass &>(entry);
			if (!entryC.HasChecksum()) continue;
			if (--sel<0)
			{
				selChild = &entryC;
				break;
			}
		}
		Assert( selC );
		if (!selC) break;
		selC = selChild;
	}

	// select random config class to check integrity
	LogF
	(
		"%s: random check of %s",
		(const char *)pi.name,
		(const char *)selC->GetContext()
	);
	RString cfgPath = ConvertContextToCfgPath(selC->GetContext());
	IntegrityCheck(pi.dpid,IQTConfig,IntegrityQuestion(cfgPath,0,0));
	//IntegrityCheck(player,IQTExe,IntegrityQuestion());
	//IntegrityCheck(player,IQTData,IntegrityQuestion());
  */
}

void NetworkServer::PerformIntegrityInvestigations()
{
	// scan all players
	for (int i=0; i<_players.Size(); i++)
	{
		NetworkPlayerInfo &pi = _players[i];
		bool someInvestigation = false;
		for (int q=0; q<IntegrityQuestionTypeCount; q++)
		{
			IntegrityInvestigation *ii = pi.integrityInvestigation[q];
			if (!ii) continue;
			someInvestigation = true;
			bool toBeContinued = ii->Proceed(this,&pi);
			if (!toBeContinued)
			{
				RString result = ii->GetResult();
				if (result.GetLength()>0)
				{
					// inform about result

					RString format = LocalizeString(IDS_MP_VALIDERROR + q);

					char message[512];
					sprintf(message, format, (const char *)pi.name);
					sprintf(message+strlen(message)," - %s",(const char *)result);
					RefArray<NetworkObject> dummy;
					GNetworkManager.Chat(CCGlobal,"",dummy,message);
					GChatList.Add(CCGlobal, NULL, message, false, true);
#if _ENABLE_DEDICATED_SERVER
					ConsoleF("%s",message);
#endif
					Log(message);
				}
				// cancel test
				pi.integrityInvestigation[q].Free();
			}
		}
		if (!someInvestigation)
		{
			// if there is no investigation running, we may perform random integrity check
			DWORD time = GlobalTickCount();
			if (time>pi.integrityCheckNext)
			{
				int qIndex = pi.FindIntegrityQuestion(IQTExe);
				if (qIndex>=0)
				{
					//LogF("EXE Integrity question in progress");
				}
				else
				{
					PerformRandomIntegrityCheck(pi);
          // avoid regular checks to avoid regular countermeasures
					pi.integrityCheckNext = time+1000*toInt(20+GRandGen.RandomValue()*30);
				}
			}
		}
	}
}

void NetworkServer::OnIntegrityCheckFailed
(
	int dpnid, IntegrityQuestionType type, const char *text, bool final
)
{
	NetworkPlayerInfo *info = GetPlayerInfo(dpnid);
	if (!info) return;
	RString format = LocalizeString(IDS_MP_VALIDERROR + type);

	char message[512];
	sprintf(message, format, (const char *)info->name);
	if (text && *text)
	{
		sprintf(message+strlen(message)," - %s",text);
	}
	if (final)
	{
		RefArray<NetworkObject> dummy;
		GNetworkManager.Chat(CCGlobal, "", dummy, message);
	}
	GChatList.Add(CCGlobal, NULL, message, false, true);
#if _ENABLE_DEDICATED_SERVER
	ConsoleF(message);
#endif
}

NetworkPlayerInfo *NetworkServer::GetPlayerInfo(int dpid)
{
	for (int i=0; i<_players.Size(); i++)
	{
		NetworkPlayerInfo &info = _players[i];
		if (info.dpid == dpid)
			return &info;
	}
	return NULL;
}

/*!
\patch 1.25 Date 10/01/2001 by Jirka
- Added: Send chat message "Player XXX connecting" when login process is initiated.
*/

NetworkPlayerInfo *NetworkServer::OnPlayerCreate(int dpid, const char *name)
{
	char playerName[256];
	strcpy(playerName, name);

	bool ok = false;
	int suffix = 1;
	while (!ok)
	{
		ok = true;
		for (int i=0; i<_players.Size(); i++)
		{
			NetworkPlayerInfo &info = _players[i];
			if (info.dpid == dpid) continue;
			if (stricmp(info.name, playerName) == 0)
			{
				sprintf(playerName, "%s (%d)", name, ++suffix);
				ok = false;
				break;
			}
		}
	}

	{
		NetworkPlayerInfo *info = GetPlayerInfo(dpid);
		if (info)
		{
			DoAssert(strcmp(info->name, playerName) == 0);
			return info;
		}
	}

	int index = _players.Add();
	NetworkPlayerInfo &info = _players[index];
	info.dpid = dpid;
	info.dvid = 0;
	info.channel = CCNone; 	
	info.name = playerName;
	info.cameraPosition = InvalidCamPos;
	info.cameraPositionTime = TIME_MIN;
	info.integrityCheckNext = UINT_MAX;
	info.connectionProblemsReported = false;
	info.kickedOff = false;
	info.nextQuestionId = 1;

#if LOG_PLAYERS
RptF
(
	"Server: Player info added - name %s, id %d (total %d identities, %d players)",
	(const char *)playerName, dpid, _identities.Size(), _players.Size()
);
#endif

//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
	if (_dedicated)
	{
		info.motdIndex = -1;
		if (dpid != _botClient && _motd.Size() > 0) info.motdIndex = 0;
	}
#endif
//}

	//info.integrityQuestion = 0;
	/*
	for (int t=0; t<IntegrityQuestionTypeCount; t++)
	{
		info.integrityQuestionTimeout[t] = UINT_MAX;
		info.integrityInvestigation[t] = NULL;
	}
	*/

	// register formats
	for (int mt=NMTFirstVariant; mt<NMTN; mt++)
	{
		NetworkMessageFormatBase *item = GetFormat(mt);

		// create message
		NetworkMessageType type = item->GetNMType(NMCCreate);
		NetworkMessageFormatBase *format = GetFormat(type);
		Ref<NetworkMessage> msg = new NetworkMessage();
		msg->time = Glob.time;
		NetworkMessageContext ctx(msg, format, this, dpid, MSG_SEND);

		int IndicesMsgFormatGetIndex(const NetworkMessageIndices *ind);
		int index = IndicesMsgFormatGetIndex(ctx.GetIndices());

		TMError err;
		err = ctx.IdxTransfer(index, mt);
		if (err != TMOK) continue;
		err = item->TransferMsg(ctx);
		if (err != TMOK) continue;

		// send message
		NetworkComponent::SendMsg(dpid, msg, type, NMFGuaranteed);
	}
	info.state = NGSCreate;
	ChangeGameState gs(NGSCreate);
	SendMsg(dpid, &gs, NMFGuaranteed);

	// Send chat message about player is connecting
	{
		char message[256];
		sprintf(message, LocalizeString(IDS_MP_CONNECTING), (const char *)info.name);
		RefArray<NetworkObject> dummy;
		ChatMessage msg(CCGlobal, NULL, dummy, "", message);
		for (int i=0; i<_players.Size(); i++)
		{
			if (_players[i].state >= NGSCreate)
				SendMsg(_players[i].dpid, &msg, NMFGuaranteed);
		}
	}

	return &info;
}

void NetworkServer::UpdateAdminState()
{
//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
	for (int i=0; i<_identities.Size(); i++)
	{
		PlayerIdentity &pi = _identities[i];
		pi._rights &= ~(PRAdmin|PRVotedAdmin);
		if (pi.dpnid==_gameMaster)
		{
			pi._rights |= _admin ? PRVotedAdmin : PRAdmin;
		}
	}
	DWORD updatePingTime = GlobalTickCount() + 1000;
	if (_pingUpdateNext>updatePingTime) _pingUpdateNext = updatePingTime;
#endif
//}
}

void NetworkServer::OnPlayerDestroy(int dpid)
{
	if (dpid == _botClient)
	{
		Fail("Not implemented.");
		// new bot client must be selected
		return;
	}

//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
	RString name;
	if (dpid == _gameMaster)
	{
		for (int i=0; i<_players.Size(); i++)
			if (_players[i].dpid == _gameMaster)
			{
				name = _players[i].name;
				break;
			}
	}
#endif
//}

	NetworkPlayerInfo *info = GetPlayerInfo(dpid);
	if (info)
	{
		char message[256];
		sprintf(message, LocalizeString(IDS_MP_DISCONNECT), (const char *)info->name);
		RefArray<NetworkObject> dummy;
		GNetworkManager.Chat(CCGlobal, "", dummy, message);
		GChatList.Add(CCGlobal, NULL, message, false, true);

//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
		if (_dedicated) ConsoleF("Player %s disconnected", (const char *)info->name);
#endif
//}

		for (int i=0; i<_players.Size(); i++)
		{
			NetworkPlayerInfo &info = _players[i];
			if (info.dpid == dpid)
			{
#if LOG_PLAYERS
RptF
(
	"Server: Player info removed - name %s, id %d (total %d identities, %d players)",
	(const char *)info.name, dpid, _identities.Size(), _players.Size() - 1
);
#endif
				_players.Delete(i);
				break;
			}
		}
		if (_players.Size() == 0)
		{
			_nextPlayerId = 1;
			_votings.Clear();
		}
//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
		else if (_dedicated && _players.Size() == 1)
		{
			_nextPlayerId = 1;
			_votings.Clear();
		}
#endif
//}
	}

	for (int i=0; i<_objects.Size(); i++)
	{
		NetworkObjectInfo &oInfo = *_objects[i];
		oInfo.DeletePlayerObjectInfo(dpid);

		for (int j=NMCUpdateFirst; j<NMCUpdateN; j++)
		{
			NetworkCurrentInfo &info = oInfo.current[j];
			if (info.from == dpid)
			{
				info.from = _botClient;
				info.message = NULL;
			}
		}
		if (oInfo.owner == dpid)
		{
			oInfo.owner = _botClient;
			if (!oInfo.id.IsNull())
			{
				ChangeOwnerMessage msg(oInfo.id, _botClient);
				SendMsg(_botClient, &msg, NMFGuaranteed);
			}
		}
	}
	for (int i=0; i<_playerRoles.Size(); i++)
	{
		PlayerRole &role = _playerRoles[i];
		if (role.player == dpid)
		{
#if _ENABLE_AI
			role.player = _missionHeader.disabledAI ? NO_PLAYER : AI_PLAYER;
#else
			role.player = NO_PLAYER;
#endif
			role.roleLocked = false;

			// create message
			NetworkMessageType type = role.GetNMType(NMCCreate);
			NetworkMessageFormatBase *format = GetFormat(type);
			Ref<NetworkMessage> msg = new NetworkMessage();
			msg->time = Glob.time;
			NetworkMessageContext ctx(msg, format, this, TO_SERVER, MSG_SEND);

			Assert(dynamic_cast<const IndicesPlayerRole *>(ctx.GetIndices()))
			const IndicesPlayerRole *indices = static_cast<const IndicesPlayerRole *>(ctx.GetIndices());

			TMError err = ctx.IdxTransfer(indices->index, i);
			if (err != TMOK) break;
			err = role.TransferMsg(ctx);
			if (err != TMOK) break;
			
			// send message
			for (int i=0; i<_players.Size(); i++)
			{
				NetworkPlayerInfo &player = _players[i];
				if (player.state < NGSCreate) continue;
				NetworkComponent::SendMsg(player.dpid, msg, type, NMFGuaranteed);
			}

			break;
		}
	}
	LogoutMessage logout;
	logout.dpnid = dpid;
	for (int i=0; i<_identities.Size(); i++)
	{
		if (_identities[i].dpnid == dpid)
		{
			SquadIdentity *squad = _identities[i].squad;
#if LOG_PLAYERS
RptF
(
	"Server: Identity removed - name %s, id %d (total %d identities, %d players)",
	(const char *)_identities[i].name, dpid, _identities.Size() - 1, _players.Size()
);
#endif
			_identities.Delete(i);
			// check if player info was deleted properly
			NetworkPlayerInfo *check = GetPlayerInfo(dpid);
			if (check)
			{
				RptF
				(
					"Server error: identity deleted, player info still exist (%s, id %d)",
					(const char *)check->name, dpid
				);
			}

			if (squad)
			{
				// check if squad is used
				bool found = false;
				for (int j=0; j<_identities.Size(); j++)
				{
					if (_identities[j].squad == squad)
					{
						found = true;
						break;
					}
				}
				// if not, delete it
				if (!found) for (int j=0; j<_squads.Size(); j++)
				{
					if (_squads[j] == squad)
					{
						_squads.Delete(j);
						break;
					}
				}
			}
			_votings.Check(this);
			break;
		}
	}
	for (int i=0; i<_identities.Size(); i++)
	{
		PlayerIdentity &player = _identities[i];
		SendMsg(player.dpnid, &logout, NMFGuaranteed);
	}

//{ QUERY & REPORTING SDK
#if _ENABLE_DEDICATED_SERVER
	if (IsDedicatedServer())
	{
#if _ENABLE_GAMESPY
		ChangeStateQR();
#endif
		NetworkClient *client = _parent->GetClient();
		DoAssert(client && client->GetPlayer() != logout.dpnid);
		// send new identity to bot client
		SendMsg(client->GetPlayer(), &logout, NMFGuaranteed);
	}
#endif
//}

//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
	if (_dedicated)
	{
		if (dpid == _gameMaster)
		{
			ConsoleF("Admin %s logged out.", (const char *)name);

			_gameMaster = AI_PLAYER;
			_sessionLocked = false;
			_debugOn.Clear();

			if (_state == NGSCreate && _mission.GetLength() == 1 && _mission[0] == '?')
			{
				// enable other players vote mission or continue with mission defined in config
				_mission = "";
				_restart = false;
				_reassign = false;
			}
			UpdateAdminState();
		}
	}
#endif
//}
}

void NetworkServer::SetEstimatedEndTime(Time time)
{
	_missionHeader.estimatedEndTime = time;
	_missionHeader.updateOnly = true;

	// send mission description and roles
	for (int i=0; i<_players.Size(); i++)
	{
		NetworkPlayerInfo &info = _players[i];
		if (info.state < NGSCreate) continue;
		SendMsg(info.dpid, &_missionHeader, NMFGuaranteed);
	}
}

NetworkObjectInfo *NetworkServer::GetObjectInfo(NetworkId &id)
{
	for (int i=0; i<_objects.Size(); i++)
	{
		NetworkObjectInfo &info = *_objects[i];
		if (info.id == id)
			return &info;
	}
	return NULL;
}

NetworkObjectInfo *NetworkServer::OnObjectCreate(NetworkId &id, int owner)
{
	{
		NetworkObjectInfo *info = GetObjectInfo(id);
		if (info)
		{
			info->owner = owner;
			return info;
		}
	}

	int index = _objects.Add();
	_objects[index] = new NetworkObjectInfo;
	NetworkObjectInfo &info = *_objects[index];
	info.id = id;
	info.owner = owner;
	return &info;
}

/*!
\patch_internal 1.28 Date 10/29/2001 by Jirka
- Fixed: update objects only by messages by object's owner
*/
NetworkObjectInfo *NetworkServer::OnObjectUpdate(NetworkId &id, int from, NetworkMessage *msg, NetworkMessageType type, NetworkMessageClass cls)
{
	NetworkObjectInfo *oInfo = GetObjectInfo(id);
	if (!oInfo)
	{
		RptF("Server: Object %d:%d not found (message %s)", id.creator, id.id, NetworkMessageTypeNames[type]);
		return NULL;
	}

	if (oInfo->owner != from)
	{
		LogF("Update of object %d:%d arrived from nonowner", id.creator, id.id);
		return NULL;
	}

	NetworkCurrentInfo &info = oInfo->current[cls];

	NetworkMessageFormatBase *format = GetFormat(/*from, */type);
	if (!format)
	{
		RptF("Server: Bad message %d(%s) format", (int)type, (const char *)NetworkMessageTypeNames[type]);
		return NULL;
	}
	
	if (info.message && info.message->time > msg->time)
	{
		// do not update
		if (DiagLevel >= 1)
			DiagLogF("Server: update message is old (%.3f s)", info.message->time - msg->time);
		return NULL;
	}

	info.from = from;
	info.type = type;
	info.message = msg;

	if (DiagLevel >= 4)
		DiagLogF("Server: object %d:%d updated", id.creator, id.id);

	return oInfo;
}

int NetworkServer::PerformObjectDestroy(const NetworkId &id)
{
	//DoAssert(CheckIntegrityOfPendingMessages());
	int owner = 0;
	for (int i=0; i<_objects.Size(); i++)
	{
		NetworkObjectInfo *info = _objects[i];
		if (info->id == id)
		{
			// check all pending messages
			for (int j=0; j<_pendingMessages.Size(); j++)
			{
				NetPendingMessage &pend = _pendingMessages[j];
				if (pend.info==info)
				{
					LogF("Deleted pending message %x for object %x",pend.msgID,pend.info);
					_pendingMessages.Delete(j);
					j--;
				}
			}
			owner = info->owner;
			_objects.Delete(i);
			//DoAssert(CheckIntegrityOfPendingMessages());
			break;
		}
	}
	return owner;
}

void NetworkServer::OnObjectDestroy(const NetworkId &id)
{
	int owner = PerformObjectDestroy(id);
	DeleteObjectMessage msg;
	msg.object = id;
	
	for (int i=0; i<_players.Size(); i++)
	{
		NetworkPlayerInfo &pInfo = _players[i];
		if (pInfo.state < NGSLoadIsland) continue;
		if (pInfo.dpid == owner) continue;
		SendMsg(pInfo.dpid, &msg, NMFGuaranteed);
	}
}

int NetworkServer::UpdateObject(NetworkPlayerInfo *pInfo, NetworkObjectInfo *oInfo, NetworkMessageClass cls, NetMsgFlags dwFlags)
{
	if (!oInfo)
	{
		// TODO: BUG
		DiagLogF("Server: Object not found");
		return -1;
	}

	NetworkCurrentInfo &info = oInfo->current[cls];
	if (!info.message) return -1;

	NetworkMessageType type = info.type;
	int to = pInfo->dpid;

	DWORD dwMsgId = NetworkComponent::SendMsg(to, info.message, type, dwFlags);
	if (dwMsgId == 0xFFFFFFFF) return -1;

	// update object info
	NetworkPlayerObjectInfo *poInfo = oInfo->CreatePlayerObjectInfo(to);
	if (dwMsgId == 0)
	{
		// sent as sync message
		if(poInfo->updates[cls].lastCreatedMsg)
		{
			// some update is pending, but we want to send guaranteed update
			// remove the pending one
			DoAssert(dwFlags&NMFGuaranteed);
			//RptF("Guaranteed update superseded pending non-guaranteed update");
			int index = FindPendingMessage(&poInfo->updates[cls]);
			DoAssert (index>=0);
			_pendingMessages.Delete(index);

			// no more messages to the update may exist now
			DoAssert( FindPendingMessage(&poInfo->updates[cls])<0 );

			poInfo->updates[cls].lastCreatedMsg = NULL;
			poInfo->updates[cls].lastCreatedMsgId = 0xffffffff;
			poInfo->updates[cls].lastCreatedMsgTime = 0;

			//DoAssert(CheckIntegrityOfPendingMessages());
		}

		DoAssert(poInfo->updates[cls].lastCreatedMsg==NULL);
		DoAssert(poInfo->updates[cls].lastCreatedMsgId == 0xffffffff);

		poInfo->updates[cls].lastCreatedMsgId = 0xFFFFFFFF;
		poInfo->updates[cls].lastCreatedMsgTime = 0;
		poInfo->updates[cls].lastSentMsg = info.message;
	}
	else
	{
		DoAssert(dwMsgId == 1);
		// !!! Pointer to static structure - must be process before structure changed
		info.message->objectUpdateInfo = &poInfo->updates[cls];
		info.message->objectServerInfo = oInfo;
		info.message->objectPlayerInfo = poInfo;

		// we want to set lastCreatedMsg
		// if it was already set, we have a conflict
		DoAssert(poInfo->updates[cls].lastCreatedMsg==NULL);
		DoAssert(poInfo->updates[cls].lastCreatedMsgId == 0xffffffff);

		poInfo->updates[cls].lastCreatedMsg = info.message;
		poInfo->updates[cls].lastCreatedMsgId = MSGID_REPLACE;
		poInfo->updates[cls].lastCreatedMsgTime = ::GlobalTickCount();
		poInfo->updates[cls].canCancel = (dwFlags & NMFGuaranteed) == 0;

#if LOG_SEND_PROCESS
		LogF("Server: Update info %x marked for send", &poInfo->updates[cls]);
#endif
	}
	return info.message->size;
}

//! Simplified enumeration of array items
#define FOR_EACH(type,var,array) \
	for (type *var = array.Data(), *end = array.Data() + array.Size(); var < end; var++)

void NetworkServer::DestroyAllObjects()
{
	_objects.Clear();
	_pendingMessages.Clear();
	FOR_EACH(NetworkPlayerInfo, player, _players)
	{
		player->person = NetworkId::Null();
		player->unit = NetworkId::Null();
		player->group = NetworkId::Null();
	}
	_mapPersonUnit.Clear();

	// avoid id duplicity at next missions
	// _nextId = 0;
}

bool NetworkServer::CheckIntegrity() const
{
	bool ret = true;
	if (!CheckIntegrityOfPendingMessages()) ret = false;
	return ret;
}

bool NetworkServer::CheckIntegrityOfPendingMessages() const
{
	// check if each pending message is using correct message ID
	bool ret = true;
	for (int p=0; p<_pendingMessages.Size(); p++)
	{
		const NetPendingMessage &pend = _pendingMessages[p];
		const NetworkObjectInfo *oInfo = pend.info;
		const NetworkPlayerObjectInfo *poInfo = pend.player;
		const NetworkUpdateInfo *uInfo = pend.update;
		// verify update is in player
		if (uInfo<poInfo->updates && uInfo>=poInfo->updates+NMCUpdateN)
		{
			RptF("Pending message %x - wrong update pointer",pend.msgID);
			ret = false;
			continue;
		}
		// verify player exists in given object
		bool found = false;
		for (int i=0; i<oInfo->playerObjects.Size(); i++)
		{
			if (oInfo->playerObjects[i]==poInfo) found = true;
		}
		if (!found)
		{
			RptF("Pending message %x not found in playerObjects",pend.msgID);
			ret = false;
			continue;
		}
		if(uInfo->lastCreatedMsgId!=pend.msgID)
		{
			RptF("Pending message invalid ID %x!=%x",uInfo->lastCreatedMsgId,pend.msgID);
			ret = false;
			continue;
		}
	}
	// check if each message that is created but not sent
	// is recorded in pending messages
	for (int o=0; o<_objects.Size(); o++)
	{
		const NetworkObjectInfo *oInfo = _objects[o];
		for (int i=0; i<oInfo->playerObjects.Size(); i++)
		{
			NetworkPlayerObjectInfo &poInfo = *oInfo->playerObjects[i];
			for (int j=NMCUpdateFirst; j<NMCUpdateN; j++)
			{
				const NetworkUpdateInfo &info = poInfo.updates[j];
				if (info.lastCreatedMsg && !info.lastSentMsg)
				{
					// message is created but not sent,
					// it is pending
					// check if we can find it in the pending list
					if (info.lastCreatedMsgId==0)
					{
						// message not created, creation in progress
					}
					else if (FindPendingMessage(info.lastCreatedMsgId)<0)
					{
						RptF("Pending message %x not found",info.lastCreatedMsgId);
						RptF("  info.lastCreatedMsg: %x",info.lastCreatedMsg.GetRef());
						ret = false;
					}
				}
			}
		}
	}

	return ret;
}

NetworkMessageFormatBase *NetworkServer::GetFormat(/*int client, */int type)
{
	Assert(type >= 0);
	if (type >= NMTN) return NULL; // unknown message
	return GMsgFormats[type];
}

void NetworkServer::CreateMission(RString mission, RString world)
{
	_originalName = mission;
	if (mission.GetLength() > 0)
	{
		RString filename = RString("mpmissions\\") + mission + RString(".") + world;
		CreateMPMissionBank(filename, world);
	}
	else
	{
		RemoveBank("mpmissions\\__cur_mp.");
	}
}

//! Text representation of RespawnMode enum values
const EnumName RespawnNames[]=
{
	EnumName(RespawnNone,"NONE"),
	EnumName(RespawnSeaGull,"BIRD"),
	EnumName(RespawnAtPlace,"INSTANT"),
	EnumName(RespawnInBase,"BASE"),
	EnumName(RespawnToGroup,"GROUP"),
	EnumName(RespawnToFriendly,"SIDE"),
	EnumName()
};

//! Map RespawnMode enum values to its text representation
template<>
const EnumName *GetEnumNames(RespawnMode dummy) {return RespawnNames;}

void NetworkServer::InitMission(bool cadetMode)
{
	// unlock session
	_sessionLocked = false;

	//extern RString GetBaseDirectory();
	_missionHeader.estimatedEndTime = TIME_MIN;

	// mission description
	_missionHeader.island = Glob.header.worldname;
	_missionHeader.name = Localize(CurrentTemplate.intel.briefingName);
	if (_missionHeader.name.GetLength() == 0)
		_missionHeader.name = _originalName.GetLength() > 0 ? _originalName : Glob.header.filename;
	_missionHeader.description = CurrentTemplate.intel.briefingDescription;

	// ADDED in patch 1.01 - checks of addons
	_missionHeader.addOns = CurrentTemplate.addOns;

	// ADDED in patch 1.01 - disable AI
#if _ENABLE_AI
	_missionHeader.disabledAI = false;
	const ParamEntry *disabledAI = ExtParsMission.FindEntry("disabledAI");
	if (disabledAI) _missionHeader.disabledAI = *disabledAI;
#endif

	// difficulty
	_missionHeader.cadetMode = cadetMode;
	if (cadetMode)
		for (int i=0; i<DTN; i++) _missionHeader.difficulty[i] = Glob.config.cadetDifficulty[i];
	else
		for (int i=0; i<DTN; i++) _missionHeader.difficulty[i] = Glob.config.veteranDifficulty[i];

	// mission respawn info
	_missionHeader.respawn = RespawnSeaGull;
	_missionHeader.respawnDelay = 0;
	const ParamEntry *respawn = ExtParsMission.FindEntry("respawn");
	if (respawn)
	{
		// check if it is numeric name
		RStringB respawnMode = *respawn;
		int mode = GetEnumValue<RespawnMode>(respawnMode);
		if (mode<0)
		{
			mode = *respawn;
		}
		if (mode<0 || mode>RespawnToFriendly) mode = RespawnNone;
		_missionHeader.respawn = (RespawnMode)mode;
	}
	respawn = ExtParsMission.FindEntry("respawnDelay");
	if (respawn) _missionHeader.respawnDelay = *respawn;

	_missionHeader.aiKills = false;
	const ParamEntry *aiKills = ExtParsMission.FindEntry("aiKills");
	if (aiKills) _missionHeader.aiKills = *aiKills;

	// mission parameters
	const ParamEntry *entry = ExtParsMission.FindEntry("titleParam1");
	_missionHeader.titleParam1 = entry ? (RString)*entry : "";
	_missionHeader.valuesParam1.Resize(0);
	_missionHeader.textsParam1.Resize(0);
	entry = ExtParsMission.FindEntry("valuesParam1");
	if (entry)
	{
		int n = entry->GetSize();
		_missionHeader.valuesParam1.Resize(n);
		for (int i=0; i<n; i++) _missionHeader.valuesParam1[i] = (*entry)[i];
	
		const ParamEntry *entryTexts = ExtParsMission.FindEntry("textsParam1");
		if (!entryTexts || entryTexts->GetSize() != n) entryTexts = entry;

		if (entryTexts)
		{
			_missionHeader.textsParam1.Resize(n);
			for (int i=0; i<n; i++) _missionHeader.textsParam1[i] = (*entryTexts)[i];
		}
	}
	entry = ExtParsMission.FindEntry("defValueParam1");
	_missionHeader.defValueParam1 = entry ? *entry : 0;
	
	entry = ExtParsMission.FindEntry("titleParam2");
	_missionHeader.titleParam2 = entry ? (RString)*entry : "";
	_missionHeader.valuesParam2.Resize(0);
	_missionHeader.textsParam2.Resize(0);
	entry = ExtParsMission.FindEntry("valuesParam2");
	if (entry)
	{
		int n = entry->GetSize();
		_missionHeader.valuesParam2.Resize(n);
		for (int i=0; i<n; i++) _missionHeader.valuesParam2[i] = (*entry)[i];
	
		const ParamEntry *entryTexts = ExtParsMission.FindEntry("textsParam2");
		if (!entryTexts || entryTexts->GetSize() != n) entryTexts = entry;

		if (entryTexts)
		{
			_missionHeader.textsParam2.Resize(n);
			for (int i=0; i<n; i++) _missionHeader.textsParam2[i] = (*entryTexts)[i];
		}
	}
	entry = ExtParsMission.FindEntry("defValueParam2");
	_missionHeader.defValueParam2 = entry ? *entry : 0;

	_param1 = _missionHeader.defValueParam1;
	_param2 = _missionHeader.defValueParam2;

	// mission file
	QFBank *bank = FindBank("MPMissions\\__cur_mp.");
	if (bank)
	{
		// mission in bank
		_missionHeader.fileDir = RString("MPMissions\\");
		_missionHeader.fileName = _originalName + RString(".") + _missionHeader.island;
		_missionBank = _missionHeader.fileDir + _missionHeader.fileName + RString(".pbo");
	}
	else
	{
		_missionHeader.fileDir = RString("tmp\\");
		_missionHeader.fileName = RString("__cur_mp");

		RString srcDir = GetServerTmpDir() + RString("\\");
		_missionBank = srcDir + _missionHeader.fileName + RString(".pbo"); 

		::CreateDirectory(srcDir, NULL);
		::DeleteFile(_missionBank);
		FileBankManager mgr;
		RString GetMissionDirectory();
		// TODO: do not create temporary __CUR_MP
		mgr.Create(_missionBank, GetMissionDirectory(), true);
		// TODO: transfer only if client's file is invalid
	}
#ifdef _WIN32

#if 1
	HANDLE handle = ::CreateFile
	(
		_missionBank, GENERIC_READ, FILE_SHARE_READ,
		NULL,OPEN_EXISTING, 0, NULL
	);
	DoAssert(handle != INVALID_HANDLE_VALUE);
	BY_HANDLE_FILE_INFORMATION info;
	::GetFileInformationByHandle(handle, &info);
	::CloseHandle(handle);
#else
	WIN32_FILE_ATTRIBUTE_DATA info;
	::GetFileAttributesEx(_missionBank, GetFileExInfoStandard, &info);
#endif
	_missionHeader.fileSizeL = info.nFileSizeLow;
	_missionHeader.fileSizeH = info.nFileSizeHigh;
  
#else    // !defined _WIN32

  LocalPath(fn,_missionBank);
  struct stat st;
  if ( !stat(fn,&st) ) {
#if 1       // (sizeof(off_t) <= 4)
    _missionHeader.fileSizeH = 0;
    _missionHeader.fileSizeL = (DWORD)st.st_size;
#else
    _missionHeader.fileSizeH = (DWORD)(st.st_size >> 32);
    _missionHeader.fileSizeL = (DWORD)(st.st_size & 0xffffffff);
#endif
    }
  else
    _missionHeader.fileSizeH =
    _missionHeader.fileSizeL = 0;

#endif

	/*
	_missionHeader.fileTimeL = info.ftLastWriteTime.dwLowDateTime;
	_missionHeader.fileTimeH = info.ftLastWriteTime.dwHighDateTime;
	*/
	{
		QIFStream f;
		f.open(_missionBank);
		CRCCalculator crc;
		_missionHeader.fileCRC = crc.CRC(f.act(), f.rest());
	}

	// init roles for players
	_playerRoles.Resize(0);
	// ADDED in patch 1.01 - disable AI
#if _ENABLE_AI
	int defaultPlayer = _missionHeader.disabledAI ? NO_PLAYER : AI_PLAYER;
#else
	int defaultPlayer = NO_PLAYER;
#endif
	int east = 0, west = 0, guer = 0, civl = 0;
	for (int i=0; i<CurrentTemplate.groups.Size(); i++)
	{
		ArcadeGroupInfo &gInfo = CurrentTemplate.groups[i];
		TargetSide side = gInfo.side;
		int grp;
		switch (side)
		{
		case TEast:
			grp = east++; break;
		case TWest:
			grp = west++; break;
		case TGuerrila:
			grp = guer++; break;
		case TCivilian:
			grp = civl++; break;
		default:
			continue;
		}

		PlayerRole role;
		role.side = side;
		role.group = grp;
		role.roleLocked = false;
		// ADDED in patch 1.01 - disable AI
		role.player = defaultPlayer;
		for (int j=0; j<gInfo.units.Size(); j++)
		{
			ArcadeUnitInfo &uInfo = gInfo.units[j];
			switch (uInfo.player)
			{
			case APPlayerCommander:
				{
					role.unit = j;
					role.vehicle = uInfo.vehicle;
					VehicleType *type = dynamic_cast<VehicleType *>
					(
						VehicleTypes.New(uInfo.vehicle)
					);
					if (type && type->HasCommander())
						role.position = PRPCommander;
					else
						role.position = PRPNone;
					role.leader = uInfo.leader;
					_playerRoles.Add(role);
				}
				break;
			case APPlayerDriver:
				role.unit = j;
				role.vehicle = uInfo.vehicle;
				role.position = PRPDriver;
				role.leader = false;
				_playerRoles.Add(role);
				break;
			case APPlayerGunner:
				role.unit = j;
				role.vehicle = uInfo.vehicle;
				role.position = PRPGunner;
				role.leader = false;
				_playerRoles.Add(role);
				break;
			case APPlayableC:
				role.unit = j;
				role.vehicle = uInfo.vehicle;
				role.position = PRPCommander;
				role.leader = uInfo.leader;
				_playerRoles.Add(role);
				break;
			case APPlayableD:
				role.unit = j;
				role.vehicle = uInfo.vehicle;
				role.position = PRPDriver;
				role.leader = false;
				_playerRoles.Add(role);
				break;
			case APPlayableG:
				role.unit = j;
				role.vehicle = uInfo.vehicle;
				role.position = PRPGunner;
				role.leader = false;
				_playerRoles.Add(role);
				break;
			case APPlayableCD:
				role.unit = j;
				role.vehicle = uInfo.vehicle;
				role.position = PRPCommander;
				role.leader = uInfo.leader;
				_playerRoles.Add(role);
				role.position = PRPDriver;
				role.leader = false;
				_playerRoles.Add(role);
				break;
			case APPlayableCG:
				role.unit = j;
				role.vehicle = uInfo.vehicle;
				role.position = PRPCommander;
				role.leader = uInfo.leader;
				_playerRoles.Add(role);
				role.position = PRPGunner;
				role.leader = false;
				_playerRoles.Add(role);
				break;
			case APPlayableDG:
				role.unit = j;
				role.vehicle = uInfo.vehicle;
				role.position = PRPDriver;
				role.leader = false;
				_playerRoles.Add(role);
				role.position = PRPGunner;
				_playerRoles.Add(role);
				break;
			case APPlayableCDG:
				// all available
				{
					role.unit = j;
					role.vehicle = uInfo.vehicle;
					VehicleType *type = dynamic_cast<VehicleType *>
					(
						VehicleTypes.New(uInfo.vehicle)
					);
					DoAssert(type);
					if (type->HasCommander())
					{
						role.position = PRPCommander;
						role.leader = uInfo.leader;
						_playerRoles.Add(role);
						role.position = PRPDriver;
						role.leader = false;
						_playerRoles.Add(role);
						if (type->HasGunner())
						{
							role.position = PRPGunner;
							_playerRoles.Add(role);
						}
					}
					else
					{
						if (type->HasGunner())
						{
							role.position = PRPGunner;
							role.leader = false;
							_playerRoles.Add(role);
							role.position = PRPDriver;
						}
						else
						{
							role.position = PRPNone;
						}
						role.leader = uInfo.leader;
						_playerRoles.Add(role);
					}
				}
				break;
			}
		}
	}

	// send mission description and roles
	for (int i=0; i<_players.Size(); i++)
	{
		NetworkPlayerInfo &info = _players[i];
		if (info.state < NGSCreate) continue;
		SendMissionInfo(info.dpid);
		info.state = NGSPrepareSide;
		SetPlayerState(info.dpid, NGSPrepareSide);
		info.missionFileValid = false;
	}
}

void NetworkServer::ChangeOwner(NetworkId &id, int from, int to)
{
	if (from == to) return;
	if (id.IsNull()) return;

	NetworkObjectInfo *oInfo = GetObjectInfo(id);
	if (!oInfo)
	{
		RptF("Server: Object info %d:%d not found.", id.creator, id.id);
		return;
	}
	oInfo->owner = to;

	NetworkPlayerInfo *pInfo = GetPlayerInfo(to);
	if (pInfo)
	{
		for (int cls=NMCUpdateFirst; cls<NMCUpdateN; cls++)
			UpdateObject(pInfo, oInfo, (NetworkMessageClass)cls, NMFGuaranteed);
//		pInfo->DeleteObjectInfo(id);
	}
	oInfo->DeletePlayerObjectInfo(from);

	// PlayerObjectInfo for object creates automatically

	ChangeOwnerMessage msg(id, to);
	SendMsg(from, &msg, NMFGuaranteed);
	SendMsg(to, &msg, NMFGuaranteed);
}

void NetworkServer::UpdateGroupLeader(NetworkId &group, NetworkId &leader)
{
	NetworkPlayerInfo *oldOwnerInfo = NULL;
	NetworkPlayerInfo *newOwnerInfo = NULL;
	for (int i=0; i<_players.Size(); i++)
	{
		NetworkPlayerInfo &pInfo = _players[i];
		if (pInfo.group == group)
			oldOwnerInfo = &pInfo;
		if (pInfo.unit == leader)
			newOwnerInfo = &pInfo;
	}

	int oldOwner = oldOwnerInfo ? oldOwnerInfo->dpid : _botClient;
	int newOwner = newOwnerInfo ? newOwnerInfo->dpid : _botClient;

	// if (oldOwner == newOwner) return;

	// group leader owner changed

	// change group owner
	if (oldOwnerInfo) oldOwnerInfo->group = NetworkId::Null();
	if (newOwnerInfo) newOwnerInfo->group = group;
	ChangeOwner(group, oldOwner, newOwner);

	// change subgroups owner
	AutoArray<NetworkId> persons;
	for (int i=0; i<_objects.Size(); i++)
	{
		NetworkObjectInfo &oInfo = *_objects[i];
		NetworkCurrentInfo &info = oInfo.current[NMCUpdateGeneric];
		if (!info.message) continue;
		if (info.type != NMTUpdateAISubgroup) continue;
		NetworkMessageFormatBase *format = GetFormat(info.type);
		Assert(format);
		NetworkMessageContext ctx(info.message, format, this, info.from, MSG_RECEIVE);

		Assert(dynamic_cast<const IndicesUpdateAISubgroup *>(ctx.GetIndices()))
		const IndicesUpdateAISubgroup *indices = static_cast<const IndicesUpdateAISubgroup *>(ctx.GetIndices());

		NetworkId grp;
		if (ctx.IdxGetId(indices->group, grp) != TMOK) continue;
		if (grp != group) continue;
		ChangeOwner(oInfo.id, oInfo.owner, newOwner);
		
		AutoArray<NetworkId> units;
		if (ctx.IdxGetIds(indices->units, units) != TMOK) continue;
		for (int j=0; j<units.Size(); j++)
		{
			NetworkId unit = units[j];
			if (unit.IsNull()) continue;
			// avoid players' units change
			bool found = false;
			for (int j=0; j<_players.Size(); j++)
			{
				NetworkPlayerInfo &pInfo = _players[j];
				if (pInfo.unit == unit)
				{
					found = true;
					break;
				}
			}
			if (found) continue;
			NetworkObjectInfo *oInfo = GetObjectInfo(unit);
			if (!oInfo) continue;
			int oldOwner = oInfo->owner;
			ChangeOwner(unit, oldOwner, newOwner);
			// change person owner
			NetworkId person = UnitToPerson(unit);
			persons.Add(person);
			ChangeOwner(person, oldOwner, newOwner);
		}
	}

	// change transports owner
	for (int i=0; i<_objects.Size(); i++)
	{
		NetworkObjectInfo &oInfo = *_objects[i];
		NetworkCurrentInfo &info = oInfo.current[NMCUpdateGeneric];
		if (!info.message) continue;
		if (!IsUpdateTransport(info.type)) continue;
		NetworkMessageFormatBase *format = GetFormat(info.type);
		Assert(format);
		NetworkMessageContext ctx(info.message, format, this, info.from, MSG_RECEIVE);

		Assert(dynamic_cast<const IndicesUpdateTransport *>(ctx.GetIndices()))
		const IndicesUpdateTransport *indices = static_cast<const IndicesUpdateTransport *>(ctx.GetIndices());

		NetworkId driver;
		if (ctx.IdxGetId(indices->driver, driver) != TMOK) continue;
		bool found = false;
		for (int j=0; j<persons.Size(); j++)
			if (persons[j] == driver)
			{
				found = true;
				break;
			}
		if (!found) continue;
		ChangeOwner(oInfo.id, oInfo.owner, newOwner);
	}
}

void NetworkServer::SendMissionInfo(int to, bool onlyPlayers)
{
	_missionHeader.updateOnly = false;

	if (!onlyPlayers)
	{
		SendMsg(to, &_missionHeader, NMFGuaranteed);

		MissionParamsMessage msg;
		msg._param1 = _param1;
		msg._param2 = _param2;
		SendMsg(to, &msg, NMFGuaranteed);
	}

	for (int i=0; i<_playerRoles.Size(); i++)
	{
		// create message
		NetworkMessageType type = _playerRoles[i].GetNMType(NMCCreate);
		NetworkMessageFormatBase *format = GetFormat(type);
		Ref<NetworkMessage> msg = new NetworkMessage();
		msg->time = Glob.time;
		NetworkMessageContext ctx(msg, format, this, to, MSG_SEND);

		Assert(dynamic_cast<const IndicesPlayerRole *>(ctx.GetIndices()))
		const IndicesPlayerRole *indices = static_cast<const IndicesPlayerRole *>(ctx.GetIndices());

		TMError err;
		err = ctx.IdxTransfer(indices->index, i);
		if (err != TMOK) continue;
		err = _playerRoles[i].TransferMsg(ctx);
		if (err != TMOK) continue;

		// send message
		NetworkComponent::SendMsg(to, msg, type, NMFGuaranteed);
	}
}

void Encrypt(QOStream &out,const unsigned char *publicKey, int keySize);

//{ DEDICATED SERVER SUPPORT
bool NetworkServer::IsDedicatedBotClient(int dpnid) const
{
#if _ENABLE_DEDICATED_SERVER
	return _dedicated && dpnid == _botClient;
#else
	return false;
#endif
}
//}

/*!
\patch 1.32 Date 11/26/2001 by Jirka
- Fixed: players' states are properly send to all clients
*/
void NetworkServer::SetGameState(NetworkGameState state)
{
	#if _ENABLE_MP
	if (state == NGSPrepareRole && _state > NGSPrepareRole)
	{
		// cancel all messages - updates from last sessions, file transfers etc.
		_server->CancelAllMessages();
		// reset all camera information from previous session
		for (int i=0; i<_players.Size(); i++)
		{
			NetworkPlayerInfo &p = _players[i];
			p.cameraPosition = InvalidCamPos;
			p.cameraPositionTime = TIME_MIN;
		}
	}
	#endif

	if (state == NGSPlay) _missionHeader.start = GlobalTickCount();

	#if _ENABLE_MP && !defined _XBOX
	if (_state == NGSPlay && state == NGSDebriefing)
	{
		// end of mission - create log file
		RString GetRankingLog();
		RString ranking = GetRankingLog();
		if (ranking.GetLength() > 0)
		{
			const char *ptr = strrchr(ranking, ':');
			if (ptr)
			{
				RString ip = ranking.Substring(0, ptr - ranking);
				int port = atoi(ptr + 1);
				
				// create paramfile
				ParamFile f;
				f.Add("password", "Naskove3Praha5");
				AutoArray<AIStatsMPRow> &table = GStats._mission._tableMP;
				for (int i=0; i<table.Size(); i++)
				{
					char buffer[32];
					sprintf(buffer, "Player%d", i + 1);
					ParamEntry *cls = f.AddClass(buffer);
					if (cls)
					{
						AIStatsMPRow &row = table[i];
						cls->Add("name", row.player);
						cls->Add("killsInfantry", row.killsInfantry);
						cls->Add("killsSoft", row.killsSoft);
						cls->Add("killsArmor", row.killsArmor);
						cls->Add("killsAir", row.killsAir);
						cls->Add("killsPlayers", row.killsPlayers);
						cls->Add("customScore", row.customScore);
						cls->Add("killsTotal", row.killsTotal);
						cls->Add("killed", row.killed);
					}
				}
				// encrypt and save it
				QOStream s;
				f.Save(s, 0);

				static const unsigned char publicKey[] =
				{
					0x11, 0x00, 0x00, 0x00, 0x59, 0x40, 0x61, 0xd6, 0x4f, 0x0a, 0xc4, 0x58, 0xea, 0x72, 0x28, 0x0b,
					0xf4, 0x9c, 0x87, 0xbe, 0x31, 0x7d, 0x46, 0x1c, 0x70, 0xd6, 0xff, 0x8f, 0x2a, 0x2d, 0xe8, 0x98,
					0xb8, 0xd4, 0x5a, 0xf1, 0xba, 0x32, 0xff, 0x2e, 0x70, 0xec, 0x3f, 0x7f, 0xb0, 0xa1, 0xb3, 0x91,
					0xec, 0x86, 0xa7, 0x37, 0xe3, 0xe2, 0x6d, 0xba, 0x27, 0x0f, 0x57, 0xe0, 0x2d, 0x76, 0xb8, 0x43,
					0xda, 0xaf, 0x17, 0x8a
				};
				Encrypt(s, publicKey, sizeof(publicKey) - 4);
				{
					// send it throught sockets
          #ifdef _WIN32
					WSADATA data;
					WSAStartup(0x0101, &data);
          #endif

					sockaddr_in addr;
					addr.sin_family = AF_INET;
/*
					addr.sin_addr.s_addr = inet_addr("210.116.114.250");
					addr.sin_port = htons(3389);
*/
					addr.sin_addr.s_addr = inet_addr(ip);
					addr.sin_port = htons(port);

					SOCKET sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
					connect(sock, (sockaddr *)&addr, sizeof(addr));
					send(sock, s.str(), s.pcount(), 0);
					closesocket(sock);

					#ifdef _WIN32
					WSACleanup();
          #endif
				}
			}
		}
	}
	#endif

	_state = state;
	ChangeGameState gs(state);

	// send to all players
	for (int i=0; i<_players.Size(); i++)
	{
		NetworkPlayerInfo &info = _players[i];
		if (info.state < NGSCreate) continue;
/*
		if (state == NGSPrepareRole && info.state > NGSPrepareRole)
		{
			if (info.state == NGSTransferMission) info.missionFileValid = false; // transfer canceled
			info.state = NGSPrepareRole;
		}
*/
		SendMsg(info.dpid, &gs, NMFGuaranteed);

//		if (state <= NGSPrepareOK) SetPlayerState(info.dpid, state);
		// Duplicity with NetworkClient::OnMessage - case NMTGameState [1/31/2002]
//LogF("Old state for player %d: %s, received %s", info.dpid, GameStateNames[info.state], GameStateNames[state]);
		switch (state)
		{
			case NGSPrepareRole:
				if (info.state > NGSPrepareRole)
				{
					if (info.state == NGSTransferMission) info.missionFileValid = false; // transfer canceled
					info.state = NGSPrepareRole;
				}
				else if (IsDedicatedBotClient(info.dpid))
					info.state = NGSPrepareRole;
				break;
			case NGSPrepareOK:
				if (FindPlayerRole(info.dpid) || IsDedicatedBotClient(info.dpid)) info.state = NGSPrepareOK;
				break;
			case NGSDebriefing:
			case NGSDebriefingOK:
				if (info.state >= NGSDebriefing) info.state = state;
				break;
			case NGSTransferMission:
				if (FindPlayerRole(info.dpid) || IsDedicatedBotClient(info.dpid)) info.state = NGSTransferMission;
				break;
			case NGSLoadIsland:
				if (FindPlayerRole(info.dpid) || IsDedicatedBotClient(info.dpid))
				{
					if (info.dpid == _botClient)
					{
						info.state = NGSLoadIsland;
					}
					else
					{
						if (info.missionFileValid)
						{
							info.state = NGSLoadIsland;
						}
						// else if (info.state < NGSPrepareSide) info.state = NGSPrepareSide;
					}
				}
				break;
			case NGSBriefing:
				if (info.state >= NGSLoadIsland) info.state = NGSBriefing;
				break;
			case NGSPlay:
				if (info.state == NGSBriefing) info.state = NGSPlay;
				break;
			default:
				info.state = state;
				break;
		}
//LogF("New state for player %d: %s", info.dpid, GameStateNames[info.state]);
	}

//{ QUERY & REPORTING SDK
#if _ENABLE_DEDICATED_SERVER && _ENABLE_GAMESPY
	if (IsDedicatedServer()) ChangeStateQR();
#endif
//}

	UpdateSessionDescription();
}

NetworkGameState NetworkServer::GetPlayerState(int dpid)
{
	NetworkPlayerInfo *info = GetPlayerInfo(dpid);
	if (info) return info->state;
	else return NGSNone;
}

RString NetworkServer::GetPlayerName(int dpid)
{
	NetworkPlayerInfo *info = GetPlayerInfo(dpid);
	Assert(info);
	if (info) return info->name;
	else return "Unknown player";
}

Vector3 NetworkServer::GetCameraPosition(int dpid)
{
	NetworkPlayerInfo *info = GetPlayerInfo(dpid);
	Assert(info);
	if (info) return info->cameraPosition;
	else return VZero;
}

NetworkObject *NetworkServer::GetObject(NetworkId &id)
{
	Fail("Server: GetObject is not implemented");
	return NULL;
}

//! Subsidiary structure for sorting messages by error
struct UpdateObjectInfo
{
	//! Object info
	NetworkObjectInfo *oInfo;
	//! Player object info
	NetworkPlayerObjectInfo *poInfo;
	//! Message class type
	NetworkMessageClass cls;
	//! Error (difference), distance adjusted
	float error;
	//! Error (difference)
	float errorNoCoef;
	//! Critical update
	bool criticalUpdate;
};
TypeIsSimple(UpdateObjectInfo)

//! Compare player object infos by error
int CmpPlayerObjects
(
	const UpdateObjectInfo *info1,
	const UpdateObjectInfo *info2
)
{
	float diff = info2->error - info1->error;
	return sign(diff);
}

void NetworkServer::KickOff(int dpnid, KickOffReason reason)
{
	RString format;
	switch (reason)
	{
		case KORKick: format = LocalizeString(IDS_MP_KICKED); break;
		case KORBan: format = LocalizeString(IDS_MP_BANNED); break;
	}
	PlayerIdentity *id = FindIdentity(dpnid);
	if (id)
	{
		if (id->destroy)
		{
			LogF("%s: KickOff in progress, new request ignored",(const char *)id->name);
			return;
		}
		if (format.GetLength() > 0)
		{
			RString message = Format(format,(const char *)id->name);
			RefArray<NetworkObject> dummy;
			GNetworkManager.Chat(CCGlobal,NULL,dummy,message);
			GChatList.Add(CCGlobal, NULL, message, false, true);
		}
		id->destroy = true;
		id->destroyTime = Glob.uiTime + 15; // destroy player after 15 seconds if no system message arrive
	}
	// mark player kickoff is initiated
	for (int i=0; i<_players.Size(); i++)
	{
		if (_players[i].dpid==dpnid) _players[i].kickedOff = true;
	}
	NetTerminationReason netReason = NTROther;
	switch (reason)
	{
		case KORKick: netReason = NTRKicked; break;
		case KORBan: netReason = NTRBanned; break;
		case KORFade: netReason = NTROther; break;
		case KORAddon: netReason = NTRMissingAddon; break;
	}
	_server->KickOff(dpnid,netReason);
}

void NetworkServer::Ban(int dpnid)
{
	// add to ban list
	const PlayerIdentity *identity = FindIdentity(dpnid);
	if (!identity) return;
	__int64 id64 = _atoi64(identity->id);
	_banListLocal.AddUnique(id64);

	// save ban list
	RString banList = GetUserDirectory() + RString("ban.txt");
	QOFStream f(banList);
	for (int i=0; i<_banListLocal.Size(); i++)
	{
		char buffer[32];
		_i64toa(_banListLocal[i], buffer, 10);
		f.write(buffer, strlen(buffer));
		f.put('\r');
		f.put('\n');
	}
	f.close();

	// kick off
	_server->KickOff(dpnid,NTRBanned);
}

#if LOG_ERRORS
//! diagnostic logs of error (difference) between messages
static void LogError
(
	NetworkMessageType type,
	int dpid1, NetworkMessage *msg1,
	int dpid2, NetworkMessage *msg2
)
{
	if (type < 0 || type >= NMTN) return;
	NetworkMessageFormat *format = GMsgFormats[type];

	float dt = msg1->time - msg2->time;

	// new version of message comparing
	for (int i=0; i<format->NItems(); i++)
	{
		const NetworkMessageErrorInfo &info = format->GetErrorInfo(i);
		if (info.type == ET_NONE) continue;

		const RefNetworkData &value1 = msg1->values[i];
		const RefNetworkData &value2 = msg2->values[i];
		float value = value1.CalculateError(info.type, value2, format->GetItem(i),dt);
		float coef = info.coef;
		float error = coef * value;

		if (error > 0)
			DiagLogF
			(
				"    - %s: %g * %g = %g", (const char *)format->GetItem(i).name,
				coef, value, error
			);
	}
}
#endif

#if _ENABLE_REPORT
	#define LOG_QUEUE_PROGRESS 0
#endif

/*!
\patch_internal 1.01 Date 6/26/2001 by Ondra. Added hacked version player kickoff.
*/
void NetworkServer::CheckFadeOut()
{
	if (GetNetworkManager().GetGameState() >= NGSPlay)
	{
		for (int i=0; i<_identities.Size(); i++)
		{
			PlayerIdentity &identity = _identities[i];
			if (Glob.uiTime>=identity.kickOffTime)
			{
				switch (identity.kickOffState)
				{
					case KOWait:
						{
							// transmit explanation
							identity.kickOffState = KOWarning;
							identity.kickOffTime = Glob.uiTime;
							RString string = LocalizeString(IDS_FADE_AWAY);
							RString senderName = identity.GetName();
							RefArray<NetworkObject> dummy;
							GNetworkManager.Chat(CCGlobal,senderName,dummy,string);
							GChatList.Add(CCGlobal, senderName, string, false, true);
						}
						break;
					case KOExe:
					case KOWarning:
						if (Glob.uiTime>identity.kickOffTime+15)
						{
              bool wasExe = identity.kickOffState==KOExe;
							identity.kickOffState = KODone;
							KickOff(identity.dpnid,KORFade);
              if (wasExe)
              {
					      RString format = LocalizeString(IDS_MP_VALIDERROR_1);

					      char message[512];
					      sprintf(message, format, (const char *)identity.GetName());
					      RefArray<NetworkObject> dummy;
					      GNetworkManager.Chat(CCGlobal,"",dummy,message);
					      GChatList.Add(CCGlobal, NULL, message, false, true);
      #if _ENABLE_DEDICATED_SERVER
					      ConsoleF("%s",message);
      #endif
              }
//{ QUERY & REPORTING SDK
#if _ENABLE_DEDICATED_SERVER && _ENABLE_GAMESPY
	if (IsDedicatedServer()) ChangeStateQR();
#endif
//}
						}
						break;
					default:
						Fail("KickOffState!");
					case KODone:
						break;
				}
			}
		}
	}
}

/*!
\patch_internal 1.50 Date 4/8/2002 by Jirka
- Fixed: No limits for server - bot client communication.
\patch 1.53 Date 4/23/2002 by Jirka
- Fixed: Bandwidth estimation on dedicated server
*/
void NetworkServer::EstimateBandwidth(NetworkPlayerInfo &pInfo, int nPlayers, int &nMsgMax, int &nBytesMax)
{
	if (pInfo.dpid == _botClient)
	{
		nBytesMax = INT_MAX;
		nMsgMax = INT_MAX;

		pInfo._ping.Sample(0);
		pInfo._bandwidth.Sample(INT_MAX);
	}
	else
	{
		int nMsg = 0, nBytes = 0, nMsgG = 0, nBytesG = 0;

		// calculate quotas
		_server->GetSendQueueInfo(pInfo.dpid, nMsg, nBytes, nMsgG, nBytesG);
		if (nMsgG > 0)
		{
			nMsg += nMsgG;
			nBytes += nBytesG;
		}

		int minBandwidth;
//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
		if (IsDedicatedServer())
		{
			nMsgMax = DSMaxMsgSend;
			minBandwidth = DSMinBandwidth;
		}
		else
#endif
//}
		{
			nMsgMax = MaxMsgSend;
			minBandwidth = MinBandwidth;
		}
		int bandwidth = 1024;
	#if _ENABLE_MP
		int latencyMS = 0, throughputBPS = 0;
		if (_server->GetConnectionInfo(pInfo.dpid, latencyMS, throughputBPS))
		{
			// nBytesMax += 1.25 * throughputBPS;
			bandwidth += throughputBPS;
			bandwidth += throughputBPS >> 2; // throughputBPS / 4
		}
	#endif
		// assume server upload bandwidth is at least MinBandwidth
		if (nPlayers > 0) saturate(bandwidth, minBandwidth/(8*nPlayers), MaxBandwidth/8);

		nBytesMax = toInt(0.001 * GEngine->GetAvgFrameDuration() * bandwidth);
	
		nBytesMax -= nBytes;
		nMsgMax -= nMsg;

		{
			// statistics
			int latencyMSRaw = 0, throughputBPSRaw = 0;
		#if _ENABLE_MP
			_server->GetConnectionInfoRaw(pInfo.dpid, latencyMSRaw, throughputBPSRaw);
		#endif
			pInfo._ping.Sample(latencyMSRaw);
			pInfo._bandwidth.Sample(throughputBPSRaw);
		}

		// write statistics
#if _ENABLE_CHEATS
		if (outputLogs) LogF("Server to %s: Limit (%d, %d)", (const char *)pInfo.name, nBytesMax, nMsgMax);
#endif
	}
}

/*!
\patch_internal 1.12 Date 08/07/2001 by Jirka
- Added: error calculation diagnostics
*/
void NetworkServer::CreateObjectsList(AutoArray<UpdateObjectInfo, MemAllocSA> &objects, NetworkPlayerInfo &pInfo)
{
	// calculate errors and place object in list sorted by error
	for (int j=0; j<_objects.Size(); j++)
	{
		NetworkObjectInfo &oInfo = *_objects[j];
		if (oInfo.owner == pInfo.dpid) continue; // no updates for local objects

		// found best object position and calculate error coef
		Time time = TIME_MIN;
		NetworkCurrentInfo *best = NULL;
		for (int j=NMCUpdateFirst; j<NMCUpdateN; j++)
		{
			NetworkCurrentInfo &info = oInfo.current[j];
			NetworkMessage *msg = info.message;
			if (msg)
			{
				if (msg->time > time)
				{
					best = &info;
					time = msg->time; 
				}
			}
		}
		if (!best) continue;
		Vector3 bestPosition(-FLT_MAX, -FLT_MAX, -FLT_MAX);
		{
			NetworkMessageFormat *format = GMsgFormats[best->type];
			NetworkMessageContext ctx(best->message, format, this, best->from, MSG_RECEIVE);

			Assert(dynamic_cast<const IndicesNetworkObject *>(ctx.GetIndices()))
			const IndicesNetworkObject *indices = static_cast<const IndicesNetworkObject *>(ctx.GetIndices());
			
			if (ctx.IdxTransfer(indices->objectPosition, bestPosition) != TMOK) continue;
		}

		float errCoef = CalculateErrorCoef(best->type, pInfo.cameraPosition, bestPosition);
					
		NetworkPlayerObjectInfo *poInfo = oInfo.CreatePlayerObjectInfo(pInfo.dpid);

		for (int j=NMCUpdateFirst; j<NMCUpdateN; j++)
		{
			NetworkCurrentInfo &info = oInfo.current[j];
			if (!info.message) continue;	// no current state

			NetworkUpdateInfo &update = poInfo->updates[j];
			
			float error;
			// error without distance coef
			float errorNoCoef;
			if (update.lastCreatedMsg)
			{
				// message on the way
				continue;
			}
			else if (!update.lastSentMsg)
			{
				errorNoCoef = error = FLT_MAX;
#if LOG_ERRORS
				DiagLogF
				(
					"Object %s %d:%d - first update to %d",
					NetworkMessageTypeNames[info.type], oInfo.id.creator, oInfo.id.id,
					pInfo.dpid
				);
#endif
			}
			else
			{
				errorNoCoef = error = CalculateError
				(
					info.type,
					info.from, info.message,
					pInfo.dpid, update.lastSentMsg
				);
#if LOG_ERRORS
				if (error * errCoef >= LogErrorLimit)
				{
					DiagLogF
					(
						"Object %s %d:%d - update to %d",
						NetworkMessageTypeNames[info.type], oInfo.id.creator, oInfo.id.id,
						pInfo.dpid
					);
					DiagLogF
					(
						"  - error %.3f * %.0f = %.0f", errCoef, error, errCoef * error 
					);
					if (pInfo.cameraPosition[1]!=-FLT_MAX)
					{
						DiagLogF
						(
							"  - camera [%.0f, %.0f, %.0f]",
							pInfo.cameraPosition[0], pInfo.cameraPosition[1], pInfo.cameraPosition[2]
						);
						DiagLogF
						(
							"  - position [%.0f, %.0f, %.0f] (distance %.0f m)",
							bestPosition[0], bestPosition[1], bestPosition[2],
							bestPosition.Distance(pInfo.cameraPosition)
						);
					}
					else
					{
						DiagLogF
						(
							"  - position [%.0f, %.0f, %.0f], no camera position",
							bestPosition[0], bestPosition[1], bestPosition[2]
						);
					}
					LogError
					(
						info.type,
						info.from, info.message,
						pInfo.dpid, update.lastSentMsg
					);
				}
#endif
				error *= errCoef;
			}

			// note: following condition seems to be identical to error>MinErrorToSend
			// but it is different when error is NaN
			if (!(error <= MinErrorToSend))
			{
				int index = objects.Add();
				// objects[index].pInfo = &pInfo;
				objects[index].oInfo = &oInfo;
				objects[index].poInfo = poInfo;
				objects[index].cls = (NetworkMessageClass)j;
				objects[index].error = error;
				objects[index].errorNoCoef = errorNoCoef;
				objects[index].criticalUpdate = false;

				#if _DEBUG
					#define NOP __asm {nop}
					switch (info.type)
					{
						case NMTUpdateAIUnit: NOP; break;
						case NMTUpdateMan: NOP; break;
						case NMTUpdatePositionMan: NOP; break;
						case NMTUpdateVehicleAI: NOP; break;
						case NMTUpdateDammageVehicleAI: NOP; break;
						case NMTUpdateObject: NOP; break;
						case NMTUpdateDammageObject: NOP; break;
					}
					if (update.lastSentMsg)
					{
						// tracing opportunity
						// place breakpoint depending on message type
						switch (info.type)
						{
							case NMTUpdateAIUnit: NOP; break;
							case NMTUpdateMan: NOP; break;
							case NMTUpdatePositionMan: NOP; break;
							case NMTUpdateVehicleAI: NOP; break;
							case NMTUpdateDammageVehicleAI: NOP; break;
							case NMTUpdateObject: NOP; break;
							case NMTUpdateDammageObject: NOP; break;
						}
						float verError = CalculateError
						(
							info.type,
							info.from, info.message,
							pInfo.dpid, update.lastSentMsg
						);
						NOP;
					}
					
				#endif
			}
		}
	}

	// sort objects by errors
	QSort(objects.Data(), objects.Size(), CmpPlayerObjects);
}

void NetworkServer::OnSendStarted(DWORD msgID, const NetworkMessageQueueItem &item)
{
	DoAssert(msgID != MSGID_REPLACE);
	if (item.msg->objectUpdateInfo)
	{
#if LOG_SEND_PROCESS
		LogF("  - update info %x updated", item.msg->objectUpdateInfo);
#endif
		if (item.msg->objectUpdateInfo->lastCreatedMsg != item.msg)
		{
			RptF("Last created message is bad:");
			RptF("  sending message: %x (type %s), id = %x", item.msg.GetRef(), NetworkMessageTypeNames[item.type], msgID);
			RptF("	attached object info: %x", item.msg->objectUpdateInfo);
			RptF("	last created message: %x, id = %x", item.msg->objectUpdateInfo->lastCreatedMsg.GetRef(), item.msg->objectUpdateInfo->lastCreatedMsgId);
			RptF("	last sent message: %x", item.msg->objectUpdateInfo->lastSentMsg.GetRef());

			item.msg->objectUpdateInfo->lastCreatedMsg = item.msg;
		}
		DoAssert(item.msg->objectUpdateInfo->lastCreatedMsgId == MSGID_REPLACE);
		item.msg->objectUpdateInfo->lastCreatedMsgId = msgID;
		if (msgID == 0xffffffff)
		{
			item.msg->objectUpdateInfo->lastCreatedMsg = NULL;
			item.msg->objectUpdateInfo->lastCreatedMsgTime = 0;
			// no pending message may exist to this - it had id=MSGID_REPLACE
			//DoAssert(CheckIntegrityOfPendingMessages());
		}
		else
		{
			AddPendingMessage
			(
				msgID,item.msg->objectServerInfo,
				item.msg->objectUpdateInfo,item.msg->objectPlayerInfo
			);
			//DoAssert(CheckIntegrityOfPendingMessages());
		}
	}
}

/*!
\patch 1.27 Date 10/09/2001 by Ondra
- Fixed: Ping times on clients were not updated when mission was not running.
\patch_internal 1.47 Date 3/11/2002 by Ondra
- Fixed: Improved latency on kill.
\patch_internal 1.48 Date 3/12/2002 by Ondra
- Fixed: Latency on kill further improved.
Bug introduced in 1.47 beta causing increased lag of movement fixed.
\patch_internal 1.49 Date 3/14/2002 by Ondra
- Fixed: Latence handling code reverted to version 1.45.
\patch 1.53 Date 4/23/2002 by Ondex
- Fixed: Kill/dammage message have now higher priority - this should improve latency on kill.
*/
void NetworkServer::SendMessages()
{
#if _ENABLE_CHEATS
	// remove statistics info
	static AutoArray<int> texts;
	for (int i=0; i<texts.Size(); i++)
	{
		if (texts[i] >= 0) GEngine->RemoveText(texts[i]);
	}
	texts.Resize(0);
	int y = 40;
#endif

#if _ENABLE_CHEATS
	#define LOG_DESYNC 1
#endif

#if LOG_DESYNC
	static DWORD time = 0;
	DWORD now = GlobalTickCount();
	bool logDesync = false;
	if (now >= time + 1000)
	{
		logDesync = true;
		time = now;
	}
#endif

	int nPlayers = _identities.Size() - 1;
//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
	if (IsDedicatedServer()) nPlayers++;
#endif
//}

	for (int i=0; i<_players.Size(); i++)
	{
		NetworkPlayerInfo &pInfo = _players[i];

		int nMsg = 0;
		int nBytes = 0;
		int nMsgMax, nBytesMax;

    EstimateBandwidth(pInfo, nPlayers, nMsgMax, nBytesMax);

	#if LOG_QUEUE_PROGRESS
		if (pInfo._messageQueue.Size() > 0)
		{
			LogF
			(
				"%s: nBytesMax %d, nBytes %d, queue size %d",
				(const char *)pInfo.name,
				nBytesMax, nBytes, pInfo._messageQueue.Size()
			);
		}
	#endif

		// send enqueued guaranteed messages
		int maxSize = MaxSizeGuaranteed;

		#if 0
		float throttle = floatMax(1,ThrottleGuaranteed);
		float nMsgMaxThrottle = nMsgMax*throttle;
		float nBytesMaxThrottle = nBytesMax*throttle;
		saturate(nMsgMaxThrottle,1,INT_MAX-1);
		saturate(nBytesMaxThrottle,1,INT_MAX-1);
		const int nMsgMaxGuaranteed = toLargeInt(nMsgMaxThrottle);
		const int nBytesMaxGuaranteed = toLargeInt(nBytesMaxThrottle);
		#else
		const int nMsgMaxGuaranteed = nMsgMax;
		const int nBytesMaxGuaranteed = nBytesMax;
		#endif

		while (pInfo._messageQueue.Size() > 0)
		{
			if (nMsg >= nMsgMaxGuaranteed || nBytes >= nBytesMaxGuaranteed)
			{
			#if LOG_QUEUE_PROGRESS
				int qBytes = 0;
				for (int i=0; i<pInfo._messageQueue.Size(); i++)
				{
					qBytes += pInfo._messageQueue[i].msg->size;
				}
				LogF
				(
					"  -- queue left %d (%d B)",pInfo._messageQueue.Size(),qBytes
				);
			#endif
				break;
			}

			NetworkMessage *msg = pInfo._messageQueue[0].msg;
			int size = msg->size;
			if (size >= maxSize)
			{
				//DWORD dwMsgId =
				SendMsgRemote(pInfo.dpid, msg, pInfo._messageQueue[0].type, NMFGuaranteed|NMFStatsAlreadyDone);
				pInfo._messageQueue.Delete(0);
				nMsg++;
				nBytes += size;
			}
			else
			{
				int totalSize = 0;
				int i;
				for (i=0; i<pInfo._messageQueue.Size(); i++)
				{
					NetworkMessage *msg = pInfo._messageQueue[i].msg;
					int size = msg->size;
					if (totalSize + size > maxSize) break;
					totalSize += size;
				}
				//DWORD dwMsgId =
				SendMsgQueue(pInfo.dpid, pInfo._messageQueue, 0, i, NMFGuaranteed);
				pInfo._messageQueue.Delete(0, i);
				nMsg++;
				nBytes += totalSize;
			}
		}

		#if LOG_QUEUE_PROGRESS
		if (nBytes>0)
		{
			LogF
			(
				"  -- nMsg %d (nBytes %d)",nMsg,nBytes
			);
			if (pInfo._messageQueue.Size()==0)
			{
				LogF("  ** Queue empty");
			}
		}
		#endif

		AUTO_STATIC_ARRAY(UpdateObjectInfo, objects, 256);

	#define ENABLE_CRITICAL_UPDATES 1

	#if !ENABLE_CRITICAL_UPDATES
		if (nMsg >= nMsgMax || nBytes >= nBytesMax)
		{
			#if _ENABLE_CHEATS
				if (outputLogs) LogF("  Server to %s: limit reached (guaranteed)", (const char *)pInfo.name);
			#endif
			goto DoNotSendUpdates;
		}
	#endif

		// update objects
		if (_state >= NGSPlay && pInfo.state >= NGSPlay)
		{
			// update errors of objects
			CreateObjectsList(objects, pInfo);

			// create and send critical updates			
		#if ENABLE_CRITICAL_UPDATES
			for (int i=0; i<objects.Size(); i++)
			{
				NetworkObjectInfo *oInfo = objects[i].oInfo;
				NetworkMessageClass cls = objects[i].cls;

				float error = objects[i].error;
				float errorNoCoef = objects[i].errorNoCoef;

				// any message about dammage that transmit change
				// that could mean dead should be considered high priority
				// errorNoCoef - error with no regard to distance
				// error - distance adjusted error
				if
				(
					cls != NMCUpdateDammage ||
					error < ERR_COEF_STRUCTURE * 0.01 || errorNoCoef < ERR_COEF_STRUCTURE
				) continue;

				objects[i].criticalUpdate = true;
				int bytes = UpdateObject(&pInfo, oInfo, cls, NMFGuaranteed | NMFHighPriority);


			#if _ENABLE_CHEATS
				if (outputLogs)
				{
					LogF
					(
						"  Server to %s: Object %d:%d updated - size %d bytes, critical",
						(const char *)pInfo.name, objects[i].oInfo->id.creator, objects[i].oInfo->id.id, bytes
					);
				}
			#endif

				if (bytes >= 0)
				{
					// no error
					nMsg++;
					nBytes += bytes;
				}
			}

			if (nMsg >= nMsgMax || nBytes >= nBytesMax)
			{
			#if _ENABLE_CHEATS
				if (outputLogs) LogF("  Server to %s: limit reached (guaranteed)", (const char *)pInfo.name);
			#endif
				goto DoNotSendUpdates;
			}
		#endif // ENABLE_CRITICAL_UPDATES
		}

DoNotSendUpdates:
		// send nonguaranteed enqueued messages
		maxSize = MaxSizeNonguaranteed;
		int next = 0;	// index of next object to update
		bool empty = false;
		while (true)
		{
			// enforce send last message
			if (!empty || pInfo._messageQueueNonGuaranteed.Size() <= 0)
			{
				if (nMsg >= nMsgMax || nBytes >= nBytesMax) break;
			}

			if (pInfo._messageQueueNonGuaranteed.Size() <= 0)
			{
				empty = true;
				// starving, try to create further message
				if (!PrepareNextUpdate(pInfo, objects, next)) break;
				// local communication - message was already sent
				if (pInfo.dpid == _botClient)
				{
					DoAssert(pInfo._messageQueueNonGuaranteed.Size() == 0);
					nMsg++;
					continue;
				}
				DoAssert(pInfo._messageQueueNonGuaranteed.Size() > 0);
			}

			NetworkMessageQueueItem &item = pInfo._messageQueueNonGuaranteed[0];
			int size = item.msg->size;
			// enforce send last message
			if (size >= maxSize || nMsg >= nMsgMax || nBytes >= nBytesMax)
			{
				DWORD dwMsgId = SendMsgRemote(pInfo.dpid, item.msg, item.type, NMFStatsAlreadyDone);
#if LOG_SEND_PROCESS
				LogF("Server: Message %x sent", dwMsgId);
#endif
				OnSendStarted(dwMsgId,item);
				pInfo._messageQueueNonGuaranteed.Delete(0);
				nMsg++;
				nBytes += size;
			}
			else
			{
				int totalSize = 0;
				int i;
				for (i=0; true; i++)
				{
					if (i >= pInfo._messageQueueNonGuaranteed.Size())
					{
						empty = true;
						// starving, try to create further message
						if (!PrepareNextUpdate(pInfo, objects, next)) break;
						DoAssert(pInfo.dpid != _botClient);
						DoAssert(i < pInfo._messageQueueNonGuaranteed.Size());
					}

					NetworkMessageQueueItem &item = pInfo._messageQueueNonGuaranteed[i];
					int size = item.msg->size;
					if (totalSize + size > maxSize) break;
					totalSize += size;
				}
				DWORD dwMsgId = SendMsgQueue(pInfo.dpid, pInfo._messageQueueNonGuaranteed, 0, i, NMFNone);
#if LOG_SEND_PROCESS
				LogF("Server: Message %x sent", dwMsgId);
#endif
				for (int j=0; j<i; j++)
				{
					NetworkMessageQueueItem &item = pInfo._messageQueueNonGuaranteed[j];
					OnSendStarted(dwMsgId,item);
				}
				pInfo._messageQueueNonGuaranteed.Delete(0, i);
				nMsg++;
				nBytes += totalSize;
			}
		}
		DoAssert(!empty || pInfo._messageQueueNonGuaranteed.Size() == 0);

		float sumError = 0;	// messages not sent total error
		if (next < pInfo._messageQueueNonGuaranteed.Size())
		{
		#if _ENABLE_CHEATS
			if (outputLogs) LogF("  Server to %s: limit reached", (const char *)pInfo.name);
		#endif
			for (int i=next; i<objects.Size(); i++)
			{
				if (objects[i].error < FLT_MAX)
				{
					sumError += objects[i].error;
				}
			}
		}

		pInfo._desync.Sample(sumError,1e-3);

#if 0 //LOG_DESYNC
		if (logDesync) ConsoleF("%s: desync %8.2f", (const char *)pInfo.name, sumError);
#endif
		

		// show server diagnostics
#if _ENABLE_CHEATS

		if (outputDiags == 1)
		{
			int count = pInfo._messageQueue.Size(), size = 0;
			for (int i=0; i<count; i++) size += pInfo._messageQueue[i].msg->size;

			RString output =
				Format("%s: HLWait%3d(%5dB) ", (const char *)pInfo.name, count, size) +
				_server->GetStatistics(pInfo.dpid);

			texts.Add(GEngine->ShowTextF(1000, 10, y, output));
			y += 25;
		}
#endif
	}
}

bool NetworkServer::PrepareNextUpdate(NetworkPlayerInfo &pInfo, AutoArray<UpdateObjectInfo, MemAllocSA> &objects, int &next)
{
	while (next < objects.Size())
	{
		UpdateObjectInfo &info = objects[next++];
		if (info.criticalUpdate) continue;

/*
		#if _ENABLE_REPORT
		int oldCount = pInfo._messageQueueNonGuaranteed.Size();
		#endif
*/
		NetworkObjectInfo *oInfo = info.oInfo;
		NetworkMessageClass cls = info.cls;
		int bytes = UpdateObject(&pInfo, oInfo, cls, NMFNone);
		if (bytes < 0) continue;
/*
		#if _ENABLE_REPORT
		if (oldCount == pInfo._messageQueueNonGuaranteed.Size())
		{
			Fail("No message added during NetworkServer::PrepareNextUpdate");
			continue;
		}
		#endif
*/

#if _ENABLE_CHEATS
		if (outputLogs) LogF("  Server to %s: Object updated - size %d bytes", (const char *)pInfo.name, bytes);
#endif
		return true;
	}
	return false;
}

float NetworkServer::CalculateErrorCoef
(
	NetworkMessageType type, Vector3Par cameraPosition, Vector3Val position
)
{
	switch (type)
	{
	case NMTUpdateDammageObject:
	case NMTUpdateObject:
		return Object::CalculateErrorCoef(position, cameraPosition);
	case NMTUpdatePositionVehicle:
	case NMTUpdateVehicle:
		return Vehicle::CalculateErrorCoef(position, cameraPosition);
	case NMTUpdateDetector:
		return Detector::CalculateErrorCoef(position, cameraPosition);
	case NMTUpdateFlag:
		return FlagCarrier::CalculateErrorCoef(position, cameraPosition);
	case NMTUpdateShot:
		return Shot::CalculateErrorCoef(position, cameraPosition);
	case NMTUpdateMine:
		return Mine::CalculateErrorCoef(position, cameraPosition);
	case NMTUpdateDammageVehicleAI:
	case NMTUpdateVehicleAI:
		return EntityAI::CalculateErrorCoef(position, cameraPosition);
	case NMTUpdateVehicleBrain:
		return Person::CalculateErrorCoef(position, cameraPosition);
	case NMTUpdateVehicleSupply:
		return VehicleSupply::CalculateErrorCoef(position, cameraPosition);
	case NMTUpdateTransport:
		return Transport::CalculateErrorCoef(position, cameraPosition);
	case NMTUpdatePositionMan:
	case NMTUpdateMan:
		return Man::CalculateErrorCoef(position, cameraPosition);
	case NMTUpdateTankOrCar:
		return TankOrCar::CalculateErrorCoef(position, cameraPosition);
	case NMTUpdatePositionTank:
	case NMTUpdateTank:
		return TankWithAI::CalculateErrorCoef(position, cameraPosition);
	case NMTUpdatePositionMotorcycle:
	case NMTUpdateMotorcycle:
		return Motorcycle::CalculateErrorCoef(position, cameraPosition);
	case NMTUpdatePositionCar:
	case NMTUpdateCar:
		return Car::CalculateErrorCoef(position, cameraPosition);
	case NMTUpdatePositionAirplane:
	case NMTUpdateAirplane:
		return AirplaneAuto::CalculateErrorCoef(position, cameraPosition);
	case NMTUpdatePositionHelicopter:
	case NMTUpdateHelicopter:
		return HelicopterAuto::CalculateErrorCoef(position, cameraPosition);
	case NMTUpdateParachute:
		return ParachuteAuto::CalculateErrorCoef(position, cameraPosition);
	case NMTUpdatePositionShip:
	case NMTUpdateShip:
		return ShipWithAI::CalculateErrorCoef(position, cameraPosition);
	case NMTUpdatePositionSeagull:
	case NMTUpdateSeagull:
		return SeaGullAuto::CalculateErrorCoef(position, cameraPosition);
	case NMTUpdateAICenter:
		return AICenter::CalculateErrorCoef(position, cameraPosition);
	case NMTUpdateAIGroup:
		return AIGroup::CalculateErrorCoef(position, cameraPosition);
	case NMTUpdateAISubgroup:
		return AISubgroup::CalculateErrorCoef(position, cameraPosition);
	case NMTUpdateAIUnit:
		return AIUnit::CalculateErrorCoef(position, cameraPosition);
	case NMTUpdateCommand:
		return Command::CalculateErrorCoef(position, cameraPosition);
	case NMTUpdateClientInfo:
		return ClientInfoObject::CalculateErrorCoef(position, cameraPosition);
	case NMTUpdateFireplace:
		return Fireplace::CalculateErrorCoef(position, cameraPosition);
	default:
		RptF("Server: Unknown update message %d(%s)", (int)type, (const char *)NetworkMessageTypeNames[type]);
		return 0;
	}
}

float NetworkServer::CalculateError
(
	NetworkMessageType type,
	int dpid1, NetworkMessage *msg1,
	int dpid2, NetworkMessage *msg2
)
{
	if (type < 0 || type >= NMTN)
	{
		RptF("Server: Bad message %d format", (int)type);
		return 0.0;
	}
	NetworkMessageFormat *format = GMsgFormats[type];

	float errCoefTime;
	switch (type)
	{
		case NMTUpdatePositionVehicle:
		case NMTUpdatePositionMan:
		case NMTUpdatePositionTank:
		case NMTUpdatePositionCar:
		case NMTUpdatePositionAirplane:
		case NMTUpdatePositionHelicopter:
		case NMTUpdatePositionShip:
		case NMTUpdatePositionSeagull:
		case NMTUpdatePositionMotorcycle:
			errCoefTime = ERR_COEF_TIME_POSITION;
			break;
		default:
			errCoefTime = ERR_COEF_TIME_GENERIC;
			break;
	}

	float dt = msg1->time - msg2->time;
	float errValue = errCoefTime * dt;

	// new version of message comparing
	for (int i=0; i<format->NItems(); i++)
	{
		const NetworkMessageErrorInfo &info = format->GetErrorInfo(i);
		if (info.type == ET_NONE) continue;

		const RefNetworkData &value1 = msg1->values[i];
		const RefNetworkData &value2 = msg2->values[i];
		errValue += info.coef * value1.CalculateError(info.type, value2, format->GetItem(i), dt);
	}

	return errValue;
}

NetworkId NetworkServer::PersonToUnit(NetworkId &person)
{
	for (int i=0; i<_mapPersonUnit.Size(); i++)
	{
		PersonUnitPair &pair = _mapPersonUnit[i];
		if (pair.person == person) return pair.unit;
	}
	return NetworkId::Null();
}

NetworkId NetworkServer::UnitToPerson(NetworkId &unit)
{
	for (int i=0; i<_mapPersonUnit.Size(); i++)
	{
		PersonUnitPair &pair = _mapPersonUnit[i];
		if (pair.unit == unit) return pair.person;
	}
	return NetworkId::Null();
}

int NetworkServer::AddPersonUnitPair(NetworkId &person, NetworkId &unit)
{
	int index = _mapPersonUnit.Add();
	PersonUnitPair &pair = _mapPersonUnit[index];
	pair.person = person;
	pair.unit = unit;
	return index;
}

void NetworkServer::SendMissionFile()
{
	bool found = false;
	for (int i=0; i<_players.Size(); i++)
	{
		NetworkPlayerInfo &info = _players[i];
		if (info.dpid == _botClient) continue;
		if (info.state < NGSCreate) continue;
		if (!info.missionFileValid)
		{
			found = true;
			break;
		}
	}
	if (!found) return;

	int maxSegmentSize;
	if (IsUseSockets())
		maxSegmentSize = 512 - 50;
	else
		maxSegmentSize = 0x10000;

	QIFStreamB f;
	RString src = _missionBank;
// FIX: transfer mission file always into tmp directory (do not rewrite original file)
//	RString dst = _missionHeader.fileDir + _missionHeader.fileName + RString(".pbo");
	RString dst = RString("MPMissionsCache\\") + _missionHeader.fileName + RString(".pbo");
	f.AutoOpen(src);

	TransferMissionFileMessage msg;
	msg.path = dst;
	msg.totSize = f.GetBuffer()->GetSize();
	msg.totSegments = (msg.totSize + maxSegmentSize - 1) / maxSegmentSize;
	msg.offset = 0;
	for (int i=0; i<msg.totSegments; i++)
	{
		msg.curSegment = i;
		int size = min(maxSegmentSize, msg.totSize - msg.offset);
		msg.data.Resize(size);
		memcpy(msg.data.Data(), f.GetBuffer()->GetData() + msg.offset, size);
		for (int j=0; j<_players.Size(); j++)
		{
			NetworkPlayerInfo &info = _players[j];
			if (info.dpid == _botClient) continue;
			if (info.state < NGSCreate) continue;
			if (!info.missionFileValid)
			{
				SendMsg(info.dpid, &msg, NMFGuaranteed);
			}
		}
		msg.offset += size;
	}

	// do not send twice
	for (int j=0; j<_players.Size(); j++)
	{
		NetworkPlayerInfo &info = _players[j];
		if (info.dpid == _botClient) continue;
		if (info.state < NGSCreate) continue;
		info.missionFileValid = true;
	}
}

//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER

RString GetPidFileName ();
extern bool MyPidFile;

//! Create dedicated server
/*!
\param config name of server configuaration file
*/
bool CreateDedicatedServer(RString config)
{
	if (GWorld->Options())
	{
		GWorld->Options()->DestroyHUD(-1);
	}

	RString password("");
	ParamFile serverCfg;
	serverCfg.Parse(config);
	if (serverCfg.FindEntry("password")) password = serverCfg >> "password";

	int port = GetNetworkPort();
// Avoid sessions enumeration:	GNetworkManager.Init("", port); 
	InitMsgFormats();

	GNetworkManager.CreateSession(port, password);
	if (GNetworkManager.GetServer())
	{
		GNetworkManager.GetServer()->SetDedicated(config);
		const char *imp = IsUseSockets() ? "Sockets" : "DirectPlay";
#if _VBS1
		RString title = "VBS1 Console version %s: port %d - %s";
#elif _COLD_WAR_ASSAULT
		RString title = "Cold War Assault Console version %s: port %d - %s";
#else
		RString title = "Operation Flashpoint Console version %s: port %d - %s";
#endif
		ConsoleTitle
		(
			title, APP_VERSION_TEXT, port, imp
		);
      // writting the pid of the server instance:
    if ( GetPidFileName().GetLength() )
    {
      FILE *pidFp;
      if ( (pidFp = fopen((const char*)GetPidFileName(),"wt")) != NULL )
      {
        fprintf(pidFp,"%d\n",getpid());
        fclose(pidFp);
#ifdef _WIN32
#else
        fchmod(fileno(pidFp),(S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH));
#endif
        MyPidFile = true;
      }
    }
    return true;
	}

  ConsoleTitle("Server creation failed : %d", port);
  return false;
}

/*!
\patch 1.24 Date 09/19/2001 by Jirka
- Added: parameters "motdInterval" and "motd" (for message of the day) added to dedicated server config
\patch 1.24 Date 09/21/2001 by Jirka
- Added: parameter "voteThreshold" added to dedicated server config
\patch 1.27 Date 10/16/2001 by Jirka
- Added: parameter "reportingIP" added to dedicated server config
to change / disable IP adress of Query & Reporting master server.
 When empty, reporting is disabled.
 If entry missing, master.gamespy.com is used and server is advertised to GameSpy.
\patch 1.82 Date 8/27/2002 by Jirka
- Added: parameter "kickDuplicate" added to dedicated server config
\patch 1.92 Date 6/23/2003 by Jirka
- Added: New server.cfg option "equalModRequired".
When used, users with different -mod than the one used on the server cannot connect.
*/

void NetworkServer::SetDedicated(RString config)
{
	_dedicated = true;

	_serverCfg.Parse(config);
	_missionIndex = -1;

	_motdInterval = 5;
	const ParamEntry *entry = _serverCfg.FindEntry("motdInterval");
	if (entry) _motdInterval = *entry;

	entry = _serverCfg.FindEntry("motd");
	if (entry)
	{
		for (int i=0; i<(*entry).GetSize(); i++) _motd.Add((*entry)[i]);
	}

	_voteThreshold = 0.5;
	entry = _serverCfg.FindEntry("voteThreshold");
	if (entry) _voteThreshold = *entry;

	entry = _serverCfg.FindEntry("reportingIP");
	if (entry) _reportingIP = *entry;
	else _reportingIP = "master.gamespy.com";

	entry = _serverCfg.FindEntry("kickDuplicate");
	if (entry) _kickDuplicate = *entry;

  entry = _serverCfg.FindEntry("equalModRequired");
  if (entry) _equalModRequired = *entry;

//{ QUERY & REPORTING SDK
#if _ENABLE_GAMESPY
	InitQR();
#endif
//}
}

void NetworkServer::Monitor(float interval)
{
	_monitorInterval = interval;
	_monitorFrames = 0;
	_monitorIn = 0;
	_monitorOut = 0;
	if (_monitorInterval > 0) _monitorNext = GlobalTickCount() + toInt(1000.0 * _monitorInterval);
	else _monitorInterval = UINT_MAX,_monitorNext = UINT_MAX;
}

void NetworkServer::OnMonitorIn(int size)
{
	if (_dedicated) _monitorIn += size;
}

#endif
//}
