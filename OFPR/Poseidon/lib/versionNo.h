// to change version number
// change following lines
#define APP_VERSION_MAJOR 1
#define APP_VERSION_MINOR 99
#define APP_VERSION_TEXT "1.99"

#if _VBS1
#define APP_NAME				"VBS1"
#define APP_NAME_SHORT	"VBS1"
#elif _GALATEA
#define APP_NAME				"Galatea"
#define APP_NAME_SHORT	"Galatea"
#elif _COLD_WAR_ASSAULT
#define APP_NAME				"Cold War Assault"
#define APP_NAME_SHORT	"Cold War Assault"
#else
#define APP_NAME				"Operation Flashpoint"
#define APP_NAME_SHORT	"Flashpoint"
#endif

// end of changes
// do not change following lines

// use APP_VERSION_NUM to retrieve version number as integer

#define APP_VERSION_NUM	APP_VERSION_MAJOR*100+APP_VERSION_MINOR

#define FILEVER					APP_VERSION_MAJOR,0,0,APP_VERSION_MINOR
#define PRODUCTVER			APP_VERSION_MINOR,0,0,APP_VERSION_MINOR

#define STRFILEVER			APP_VERSION_TEXT "\0"
#define STRPRODUCTVER		APP_VERSION_TEXT "\0" 

#define PRODUCTNAME			APP_NAME "\0"
#define INTERNALNAME		APP_NAME_SHORT "\0"
