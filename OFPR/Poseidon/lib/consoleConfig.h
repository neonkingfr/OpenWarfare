// Configuration parameters for console application

#define _VERIFY_KEY								0
#define _ENABLE_EDITOR						0
#define _ENABLE_ALL_MISSIONS			0
#define _ENABLE_MP								0
#define _ENABLE_AI								1
#define _ENABLE_CAMPAIGN					0
#define _ENABLE_SINGLE_MISSION		0
#define _ENABLE_BRIEF_EQ					0
#define _ENABLE_BRIEF_GRP					0
#define _ENABLE_PARAMS						1
#define _ENABLE_CHEATS						0
#define _ENABLE_ADDONS						1
#define _ENABLE_DEDICATED_SERVER	0
#define _ENABLE_GAMESPY						0
#define _FORCE_DEMO_ISLAND				0
#define _FORCE_SINGLE_VOICE				0
#define _ENABLE_AIRPLANES					0
#define _ENABLE_HELICOPTERS				0
#define _ENABLE_SHIPS							0
#define _ENABLE_CARS							0
#define _ENABLE_TANKS							0
#define _ENABLE_MOTORCYCLES				0
#define _ENABLE_PARACHUTES				0
#define _ENABLE_DATADISC					1
#define _ENABLE_VBS								0

#define _DISABLE_GUI              1

#define _TIME_ACC_MIN							(1.0 / 128.0)
#define _TIME_ACC_MAX							4.0

// Registry key for saving player's name, IP address of server etc.
#define ConfigApp "Software\\Codemasters\\Operation Flashpoint"

// Application name
#if _RUSSIAN
	#define AppName "\x0CE\x0EF\x0E5\x0F0\x0E0\x0F6\x0E8\x0FF Flashpoint"
#else
	#define AppName "Operation Flashpoint"
#endif

// Preferences program
#define PreferencesExe	"OpFlashPreferences.exe"

// default mod path 
#define _MOD_PATH_DEFAULT "RES"
