#include "wpch.hpp"

//#include "win.h"
#include "engine.hpp"
#include <El/QStream/QBStream.hpp>

extern bool UseGlide;
#ifndef _WIN32
bool LogStatesOnce = false;
#endif

// some engine implementation is neccessary
// we define dummy engine that provides no functionality

class TextureDummy: public Texture
{
	friend class TextBankDummy;
	//! texture source provider
	/*!
	\patch_internal 1.11 Date 7/31/2001 by Ondra
	- Fixed: TextureDummy converted from pac/paa only	to universal data source.
	*/
	SRef<ITextureSource> _src;

	bool _glideEmulation;
	int _aRatio;
	int _w,_h;

	int _nMipmaps;
	PacLevelMem _mipmaps[MAX_MIPMAPS];

	int Init();

	public:
	TextureDummy();
	int AWidth(int)const {return _w;}
	int AHeight(int)const {return _h;}
	int ANMipmaps(void)const {return 8;}
	void ASetNMipmaps(int){}
	int ALoad(int,int){return 0;}
	Color GetPixel(int,float,float)const {return HBlack;}

	bool IsTransparent() const {return _src && _src->IsTransparent();}
	bool IsAlpha() const {return _src && _src->IsAlpha();}
	Color GetColor() {return _src ? _src->GetAverageColor() : HBlack;}

	Color CalculateColor() {return _src ? _src->GetAverageColor() : HBlack;}
	bool VerifyChecksum(const PacLevelMem *)const {return true;}

	void SetMaxSize(int){}
	int AMaxSize()const {return 256;}
	const class PacLevelMem &AMipmap(int)const {return *(PacLevelMem *)NULL;}
	class PacLevelMem &AMipmap(int){return *(PacLevelMem *)NULL;}
	bool VerifyChecksum(const class MipInfo &) const {return true;}

	// some APIs (Glide) require u,v conversion
	float UToPhysical( float u ) const;
	float VToPhysical( float v ) const;

	float UToLogical( float u ) const;
	float VToLogical( float v ) const;
};

class TextBankDummy: public AbstractTextBank
{
	private:
	LLinkArray<TextureDummy> _texture;

	public:
	TextBankDummy();
	~TextBankDummy();


	int Find( RStringB name1, TextureDummy *interpolate=NULL );
	Ref<Texture> Load(RStringB);
	Ref<Texture> LoadInterpolated(RStringB ,RStringB ,float){return NULL;}
	MipInfo UseMipmap(class Texture *tex,int level,int top){return MipInfo(tex,level);}

	void Compact() {}
	void ReleaseMipmap(void){}
	void Preload(void){}

	int NTextures() const {return _texture.Size();}
	virtual Texture *GetTexture( int i ) const {return _texture[i];}

	void FlushTextures() {} // flush temporary data
	void FlushBank(QFBank *bank) {}
};

class EngineDummy: public Engine
{
	private:
	TextBankDummy *_bank;

	public:
	EngineDummy();
	~EngineDummy();

	RString GetDebugName() const {return "No";}
	void InitDraw(void){}
	void FinishDraw(void){}
	void Pause(void){}
	void Restore(void){}
	void DrawPicture555(unsigned short *){}
	void FogColorChanged(const class Color &){}
	void LightChanged(const class Color &,const class Color &){}
	void NightEffectChanged(float){}


	bool SwitchRes(int w, int h, int bpp) {return true;} // switch to resolution nearest to w,h
	bool SwitchRefreshRate(int refresh) {return true;} // switch to resolution nearest to w,h
	bool SwitchWindowed(bool windowed) {return true;}

	void ListResolutions(FindArray<ResolutionInfo> &ret) {}
	void ListRefreshRates(FindArray<int> &ret) {}

	bool CanZBias() const {return false;}
	bool ZBiasExclusion() const {return false;}

	int PixelSize() const {return 32;}
	int RefreshRate() const {return 0;}
	bool CanBeWindowed() const {return true;}
	bool IsWindowed() const {return true;}

	//void DrawTriangle(class TLVertexMesh &,int,int,int,const class PacLevelMem *,int){}
	void BeginMesh(class TLVertexMesh &,int){}
	void EndMesh(class TLVertexMesh &){}
	class AbstractTextBank *TextBank(void){return _bank;}
	//HWND GetWindowHandle(void)const {return NULL;}
	float ZShadowEpsilon(void)const {return 0;}
	float ZRoadEpsilon(void)const {return 0;}
	float ObjMipmapCoef(void)const {return 1;}
	float LandMipmapCoef(void)const {return 1;}
	bool ShadowsFirst(void)const {return false;}
	bool SortByShape(void)const {return false;}
	int GetBias(void){return 0;}
	void SetBias(int){}
	void GetZCoefs(float &zAdd, float &zMult) {zAdd=0, zMult=1;}
	int Width(void)const {return 160;}
	int Height(void)const {return 120;}
	int Width2D(void)const {return 80;}
	int Height2D(void)const {return 60;}
	int AFrameTime(void)const {return 0;}

	void PrepareTriangle(const class PacLevelMem *,int){}
	void DrawPolygon( const VertexIndex *i, int n ){}
	void DrawSection(const class FaceArray &,Offset b,Offset e) {}

	void DrawDecal
	(
		Vector3Par pos, float rhw, float sizeX, float sizeY, PackedColor col,
		const MipInfo &mip, int specFlags
	){}
	void Draw2D(const class PacLevelMem *,class PackedColor,float,float,float,float,float,float,float,float){}

	void Clear(bool, bool, PackedColor) {}
	void SetGamma(float) {}
	float GetGamma() const {return 0;}
	void PrepareTriangle(const MipInfo &, int) {}
	void DrawPolygon(TLVertexTable &, const short *,int) {}
	void Draw2D(const Draw2DPars &, const Rect2DAbs &, const Rect2DAbs &) {}
	virtual void DrawLine(int beg, int end){}
	void DrawLine(const Line2DAbs &, PackedColor, PackedColor, const Rect2DAbs &) {}
	void PrepareMesh( int spec ) {}
	void BeginMesh(TLVertexTable &, int) {}
	void EndMesh(TLVertexTable &) {}
	void TextureDestroyed(Texture *) {}
	float GetZCoef() const {return 1;}

	void DrawPoly
	(
		const MipInfo &mip, const Vertex2DPixel *vertices, int n,
		const Rect2DPixel &clipRect, int specFlags
	){}
	void DrawPoly
	(
		const MipInfo &mip, const Vertex2DAbs *vertices, int n,
		const Rect2DAbs &clipRect, int specFlags
	){}

};

TextureDummy::TextureDummy()
{
}

static PacFormat BasicFormat( const char *name )
{
	const char *ext=strrchr(name,'.');
	if( ext && !strcmpi(ext,".paa") ) return PacARGB4444;
	else return PacARGB1555;
}

static PacFormat DstFormat( PacFormat srcFormat, int dxt )
{
	// memory representation
	switch (srcFormat)
	{
		case PacARGB1555:
		case PacRGB565:
		case PacARGB4444:
			return srcFormat;
		case PacP8:
			return PacARGB1555;
		case PacAI88:
			return srcFormat;
		case PacDXT1:
			return srcFormat;
		default:
			LogF("Unsupported source format %d",srcFormat);
			// TODO: fail
			return srcFormat;
	}
}

int TextureDummy::Init()
{
	_glideEmulation = UseGlide;
	_aRatio = 0;
	// parse texture header

	PacFormat format=BasicFormat(Name());

	//bool isPaa = ( format==PacARGB4444 );

	ITextureSourceFactory *factory = SelectTextureSourceFactory(Name());
	if (factory) _src = factory->Create(Name(),_mipmaps,MAX_MIPMAPS);

	if (_src)
	{

		//in.seekg(0,QIOS::beg);
		// .paa should start with format marker

		#if REPORT_ALLOC>=20
		LogF("Init texture %s",(const char *)Name());
		#endif
		format = _src->GetFormat();

		if
		(
			format==PacARGB4444 ||
			format==PacAI88 ||
			format==PacARGB8888
		)
		{
			_src->ForceAlpha();
		}


		int dxt = 0;
		PacFormat dFormat = DstFormat(format,dxt);

		int nMipmaps = _src->GetMipmapCount();
		int i;
		for( i=0; i<nMipmaps; i++ )
		{
			PacLevelMem &mip=_mipmaps[i];

			mip.SetDestFormat(dFormat,8);

			// do not load too small mip-maps
			if( mip._w<2 ) break;
			if( mip._h<2 ) break;
		}

		_nMipmaps=i;

		_w = _mipmaps[0]._w;
		_h = _mipmaps[0]._h;

		int w=_w;
		int h=_h;
		if( h==w ) _aRatio=0;
		else if( h*2==w ) _aRatio=+1;
		else if( h==w*2 ) _aRatio=-1;
		else if( h*4==w ) _aRatio=+2;
		else if( h==w*4 ) _aRatio=-2;
		else if( h*8==w ) _aRatio=+3;
		else if( h==w*8 ) _aRatio=-3;
		else ErrorMessage("Invalid texture aspect ratio (%dx%d)",w,h);

		return 0;
	}	
	return -1;

}

// some APIs (Glide) require u,v conversion

float TextureDummy::UToPhysical( float u ) const
{
	if (!_glideEmulation) return u;
	int uf,vf;
	int aRatio=_aRatio;
	if( aRatio>0 ) uf=256,vf=256>>aRatio;
	else vf=256,uf=256>>-aRatio;
	return u*uf;
}

float TextureDummy::VToPhysical( float v ) const
{
	if (!_glideEmulation) return v;
	int uf,vf;
	int aRatio=_aRatio;
	if( aRatio>0 ) uf=256,vf=256>>aRatio;
	else vf=256,uf=256>>-aRatio;
	return v*vf;
}

float TextureDummy::UToLogical( float u ) const
{
	if (!_glideEmulation) return u;
	int uif,vif;
	int aRatio=_aRatio;
	if( aRatio>0 ) uif=1,vif=1<<aRatio;
	else vif=1,uif=1<<-aRatio;
	return u*(1.0f/256)*uif;
}

float TextureDummy::VToLogical( float v ) const
{
	if (!_glideEmulation) return v;
	int uif,vif;
	int aRatio=_aRatio;
	if( aRatio>0 ) uif=1,vif=1<<aRatio;
	else vif=1,uif=1<<-aRatio;
	return v*(1.0f/256)*vif;
}


TextBankDummy::TextBankDummy()
{
}

TextBankDummy::~TextBankDummy()
{
	UnlockAllTextures();
	DeleteAllAnimated();
}

int TextBankDummy::Find( RStringB name1, TextureDummy *interpolate )
{
	int i;
	for( i=0; i<_texture.Size(); i++ )
	{
		TextureDummy *texture=_texture[i];
		if( texture )
		{
			if( texture->GetName()!=name1 ) continue;
			//if( texture->_interpolate!=interpolate ) continue;
			return i;
		}
	}
	return -1;
}

Ref<Texture> TextBankDummy::Load( RStringB name )
{
	int index=Find(name);
	if( index>=0 ) return (Texture*)_texture[index];

	int iFree=_texture.Add();
	TextureDummy *texture=new TextureDummy;
	
	//texture->Load(name);
	texture->SetName(name);
	_texture[iFree]=texture;
	texture->Init();
	return texture;
}

EngineDummy::EngineDummy()
{
	_bank=new TextBankDummy();
}

EngineDummy::~EngineDummy()
{
	if( _bank ) delete _bank,_bank=NULL;
}

Engine *CreateEngineDummy()
{
	return new EngineDummy();
}
