#ifdef _MSC_VER
#pragma once
#endif

#ifndef __ENGINE_HPP
#define __ENGINE_HPP

#include "types.hpp"
#include <El/Math/math3d.hpp>
#include "colors.hpp"
#include "font.hpp"
#include "clipShape.hpp"
#include "pactext.hpp"


#include <Es/Containers/array.hpp>

// abstract engine definition - suitable for software/DirectX/MSI interfaces
// low-level drawing
// current implementations
// Glide 2/3 -> EngineGlide
// DirectX 5/6 -> EngineD3D
// DirectDraw 5 (SW Only) -> EngineDD

class Counter
{
	private:
	int _count;
	
	public:
	Counter(){_count=0;}
	void operator += ( int a ) {_count+=a;}
	operator int () const {return _count;}
	void Reset() {_count=0;}
	int Count() const {return _count;}
};

#define PERF_STATS 1


struct TextInfo
{
	int _handle;
	DWORD _hideTime;
	Ref<Font> _font;
	PackedColor _color;
	float _size; // size - relative to default size
	float _x,_y; // relative position
	Temp<char> _text; // remmember text

	TextInfo(){}
	TextInfo
	(
		int handle,
		Engine *engine, DWORD hideTime,
		Font *font, PackedColor color,
		float size, float x, float y, const char *text
	);
	TextInfo( const TextInfo &src );
	TextInfo &operator =( const TextInfo &src );

};

TypeIsMovableZeroed(TextInfo);


struct Draw2DPars
{
	MipInfo mip; // which texture
	PackedColor colorTL,colorTR,colorBL,colorBR;
	void SetColor( PackedColor c ) {colorTL=colorTR=colorBL=colorBR=c;}
	int spec; // which specflags are used
	//float u0,v0,u1,v1; // u,v range
	float uTL,vTL,uTR,vTR; // u,v range
	float uBL,vBL,uBR,vBR; // u,v range
	void SetU( float u0, float u1 ) {uTL=uBL=u0,uTR=uBR=u1;}
	void SetV( float v0, float v1 ) {vTL=vTR=v0,vBL=vBR=v1;}
	void Init();
};

struct FontID;

class FontCache
{
	// remmember chars to avoid loading/unloading too often
	struct CachedChar
	{
		Ref<Texture> _texture;
		Font *_font; // will be removed when font is destroyed
		//int _c; // ascii code
		RStringB _c;

		ClassIsMovable(CachedChar);
	};

	AutoArray<CachedChar> _lastChars;
	RefArray<Font> _fonts;

	public:
	Font *Load(FontID id);
	//Texture *Load( Font *font, int c );
	Texture *Load( Font *font, RStringB name );
	void RemoveFont( Font *font );
	void Clear();
};


class LightList;

struct ResolutionInfo
{
	int w,h,bpp;
	bool operator == (const ResolutionInfo &info) const
	{
		return w==info.w && h==info.h && bpp==info.bpp;
	}
};
TypeIsSimple(ResolutionInfo);

const int DefSpecFlags2D=NoZBuf|IsAlpha|ClampU|ClampV|IsAlphaFog;

struct Char3DContext
{
	Vector3 dir;
	Vector3 up;
	Font *font;
	Object *obj;
	float z2;
	float x1c;
	float x2c;
	float y1c;
	float y2c;
	ClipFlags clip;
	int spec;
};

//! logical viewport (viewport containing usefull information) settings
struct AspectSettings
{
	//@{ wide screen settings (ratio world to screen)
	float leftFOV;
	float topFOV;
	//}@
	//@{ 2D UI region settings (0..1 range)
	float uiTopLeftX,uiTopLeftY;
	float uiBottomRightX,uiBottomRightY;
	//@}
};

//@{
/*!\name 2D coordinate system
Various systems of 2D coordinates.
*/

//! position of point on screen in pixels (absolute)
/*!
Onscreen range: x = <0,GEngine-Width()), y = <0,GEngine->Height())
*/
struct Point2DAbs
{
	float x,y;
	Point2DAbs(){}
	Point2DAbs(float xx, float yy):x(xx),y(yy){}
};

//! 2d rectangle
struct Rect2DAbs
{
	float x,y,w,h; // rectangle
	Rect2DAbs(){}
	Rect2DAbs( float xx, float yy, float ww, float hh ){x=xx,y=yy,w=ww,h=hh;}
	Rect2DAbs( const Point2DAbs &pos, float ww, float hh ){x=pos.x,y=pos.y,w=ww,h=hh;}
};
struct Line2DAbs
{
	Point2DAbs beg,end;
	Line2DAbs(){}
	Line2DAbs( float x0, float y0, float x1, float y1){beg.x=x0,beg.y=y0,end.x=x1,end.y=y1;}
};
//! uses same coordinate system as Point2DPixel and Rect2DPixel
struct Vertex2DAbs: Point2DAbs
{
	float z,w; // screen coordinates
	float u,v; // texture coordinates
	PackedColor color; // color

	Vertex2DAbs(){z = 0.5f,w = 1.0f;}
};

//! default clipping rectangle
extern Rect2DAbs Rect2DClipAbs;

//! position of point in viewport in pixels
/*!
Insideviewport range: x = <0,GEngine->Width2D()), y = <0,GEngine->Height2D())
*/
struct Point2DPixel
{
	float x,y;
	Point2DPixel(){}
	Point2DPixel(float xx, float yy):x(xx),y(yy){}
};
//! position of rectangle in viewport in pixels
struct Rect2DPixel
{
	float x,y,w,h; // rectangle
	Rect2DPixel(){}
	Rect2DPixel( float xx, float yy, float ww, float hh ){x=xx,y=yy,w=ww,h=hh;}
};

struct Line2DPixel
{
	Point2DPixel beg,end;
	Line2DPixel(){}
	Line2DPixel( float x0, float y0, float x1, float y1){beg.x=x0,beg.y=y0,end.x=x1,end.y=y1;}
};
//! uses same coordinate system as Point2DPixel and Rect2DPixel
struct Vertex2DPixel: Point2DPixel
{
	float z,w; // screen coordinates
	float u,v; // texture coordinates
	PackedColor color; // color

	Vertex2DPixel(){z = 0.5f,w = 1.0f;}
};

extern Rect2DPixel Rect2DClipPixel;


//! position of point on screen in 2D viewport coordinates
/*!
Insideviewport range: x = <0,1), y = <0,1)
*/
struct Point2DFloat
{
	float x,y;
	Point2DFloat(){}
	Point2DFloat(float xx, float yy):x(xx),y(yy){}
};
//! position of rectangle on screen in 2D viewport coordinates
struct Rect2DFloat
{
	float x,y,w,h; // rectangle
	Rect2DFloat(){}
	Rect2DFloat( float xx, float yy, float ww, float hh ){x=xx,y=yy,w=ww,h=hh;}
};

struct Line2DFloat
{
	Point2DFloat beg,end;
	Line2DFloat(){}
	Line2DFloat( float x0, float y0, float x1, float y1){beg.x=x0,beg.y=y0,end.x=x1,end.y=y1;}
};

//@}

class Engine
{
	protected:
	int _messageHandle;
	int _textHandle;

	Color _fogColor;
	Color _accomodateEye; // color filter
	float _usrBrightness; // user brightness control
	int _shadowFactor; // alpha values used for full shadows - from 0 to 255

	float _avgBrightness; // average screen brightness
	bool _nightVision;
	bool _multitexturing;
	int _showFps;
	AspectSettings _aspectSettings;

	Ref<Font> _showTextFont; // actual parameters for ShowText and ShowTextF
	PackedColor _showTextColor;
	float _showTextSize;

	AutoArray<TextInfo> _texts;
	FontCache _fonts;

	DWORD _frameTime,_frameTime0; // last frame stats
	DWORD _startTime;
	DWORD _lastFrameDuration; // duration of last frame (in ms)
	DWORD _startGame; // time the game started

	enum {NFrameDurations=16};
	DWORD _frameDurations[NFrameDurations];
	DWORD _frameTriangles[NFrameDurations];

	public:
	void ToggleFps( int state ){_showFps=state;}

	// get stats to be able to scale

	DWORD GetLastFrameDuration() const {return _lastFrameDuration;}
	DWORD GetAvgFrameDuration( int nFrames=8 ) const;
	DWORD GetTimeStartGame() const {return _startGame;}
	void SetTimeStartGame( DWORD time ) {_startGame=time;}
	void ResetFrameDuration();

	void SetNightVision( bool state ){_nightVision=state;}
	bool GetNightVision() const {return _nightVision;}

	bool IsMultitexturing() const {return _multitexturing;}
	void SetMultitexturing(bool set);

	void SetAspectSettings(const AspectSettings &set){_aspectSettings=set;}
	void GetAspectSettings(AspectSettings &get){get=_aspectSettings;}

	virtual bool IsWBuffer() const {return false;}
	virtual bool CanWBuffer() const {return false;}
	virtual void SetWBuffer(bool val) {}

	ColorVal GetAccomodateEye() const {return _accomodateEye;} // color filter

	virtual void EnableNightEye(float night) {}

	int ShowFps() const {return _showFps;}
	void CCALL ShowMessage( int timeMs, const char *fmt, ... );

	void SetFogColor( ColorVal fogColor );
	ColorVal FogColor(){return _fogColor;}

	void SetShadowFactor( int shadowFactor) {_shadowFactor = shadowFactor;}
	int GetShadowFactor() const {return _shadowFactor;}
	
	private:
	Engine( const Engine &src ); // no copy
	void operator = ( const Engine &src );

	public:
	Engine();
	virtual ~Engine();

	virtual bool IsAbleToDraw(){return true;}
	virtual void Clear( bool clearZ=true, bool clear=true, PackedColor color=PackedColor(0) ) = NULL;
	void DrawFinishTexts();
	virtual void InitDraw( bool clear=false, PackedColor color=PackedColor(0) ); // Begin scene
	virtual void FinishDraw(); // End scene
	virtual void NextFrame(); // swap frames - get ready for next frame
	virtual bool InitDrawDone() {return true;} 
	virtual void Pause() = NULL; // stop and prepare everything for GDI
	virtual void Restore() = NULL; // restore after minimized - before app goes to fullscreen
	virtual void StopAll() {} // stop all background activity - used before termination
	virtual void FogColorChanged( ColorVal fogColor ) = NULL;
	
	virtual bool SwitchRes(int w, int h, int bpp) = NULL; // switch to resolution nearest to w,h
	virtual bool SwitchRefreshRate(int refresh) = NULL; // switch to resolution nearest to w,h
	virtual bool SwitchWindowed(bool windowed) = NULL;
	virtual RString GetDebugName() const = NULL;

	virtual void ListResolutions(FindArray<ResolutionInfo> &ret) = NULL;
	virtual void ListRefreshRates(FindArray<int> &ret) = NULL;
	virtual void SetGamma( float g ) = NULL;
	virtual float GetGamma() const = NULL;

	void SaveConfig();
	void LoadConfig();

	virtual void SetBrightness( float v ) {_usrBrightness=v;}
	virtual float GetBrightness() const {return _usrBrightness;}

	virtual void PrepareTriangleTL( const MipInfo &mip, int specFlags ) {}
	virtual void PrepareTriangle( const MipInfo &mip, int specFlags )=NULL;
	virtual void DrawPolygon( const VertexIndex *i, int n )=NULL;
	virtual void DrawSection(const FaceArray &face, Offset beg, Offset end)=NULL;

	virtual void EnableReorderQueues( bool enableReorded ) {}
	virtual void FlushQueues() {}

	// integrated transform&lighting
	virtual bool GetTL() const {return false;}
	virtual bool HasWBuffer() const {return false;} // far plane important

	virtual void SetMaterial(const TLMaterial &mat,const LightList &lights, int spec) {}
	virtual void EnableSunLight(bool enable) {}
	
	virtual void PrepareMeshTL( const LightList &lights, const Matrix4 &modelToWorld, int spec ) {} // prepare internal variables
	virtual void BeginMeshTL( const Shape &sMesh, int spec, bool dynamic=false ) {} // convert all mesh vertices
	virtual void EndMeshTL( const Shape &sMesh ) {} // forget mesh
	virtual void DrawSectionTL(const Shape &sMesh, int beg, int end) {}
	
	virtual int HowLongIdle() {return 0;}
	virtual void DrawDecal
	(
		Vector3Par pos, float rhw, float sizeX, float sizeY, PackedColor col,
		const MipInfo &mip, int specFlags
	)=NULL; // 3D rectangle
	virtual void Draw2D
	(
		const Draw2DPars &pars, const Rect2DAbs &rect, const Rect2DAbs &clip=Rect2DClipAbs
	)=NULL; // 2D rectangle
	virtual void Draw2D
	(
		const Draw2DPars &pars, const Rect2DPixel &rect, const Rect2DPixel &clip=Rect2DClipPixel
	)
	{
		Rect2DAbs rectA,clipA;
		Convert(rectA,rect);
		Convert(clipA,clip);
		Draw2D(pars,rectA,clipA);
	}

	virtual void DrawPoly
	(
		const MipInfo &mip, const Vertex2DAbs *vertices, int nVertices,
		const Rect2DAbs &clip=Rect2DClipAbs, int specFlags=DefSpecFlags2D
	) = NULL;
	virtual void DrawPoly
	(
		const MipInfo &mip, const Vertex2DPixel *vertices, int nVertices,
		const Rect2DPixel &clip=Rect2DClipPixel, int specFlags=DefSpecFlags2D
	) = NULL;
	virtual void DrawLine
	(
		const Line2DAbs &rect,
		PackedColor c0, PackedColor c1,
		const Rect2DAbs &clip=Rect2DClipAbs
	)=NULL; // 2D line
	virtual void DrawLine
	(
		const Line2DPixel &rect,
		PackedColor c0, PackedColor c1,
		const Rect2DPixel &clip=Rect2DClipPixel
	)
	{
		Line2DAbs rectA;
		Rect2DAbs clipA;
		Convert(rectA,rect);
		Convert(clipA,clip);
		DrawLine(rectA,c0,c1,clipA);
	}
	virtual void DrawLine
	(
		int beg, int end
	)=NULL;  // 3D line - width in m
	void Draw2D
	(
		const MipInfo &mip, PackedColor color,
		const Rect2DAbs &rect,
		const Rect2DAbs &clip=Rect2DClipAbs
	) // wrapper to keep old interface working
	{
		Draw2DPars pars;
		pars.mip=mip;
		pars.SetColor(color);
		pars.Init();
		// call wrapped function
		Draw2D(pars,rect,clip);
	}
	void Draw2D
	(
		const MipInfo &mip, PackedColor color,
		const Rect2DPixel &rect,
		const Rect2DPixel &clip=Rect2DClipPixel
	) // wrapper to keep old interface working
	{
		Rect2DAbs rectA,clipA;
		Convert(rectA,rect);
		Convert(clipA,clip);
		Draw2DPars pars;
		pars.mip=mip;
		pars.SetColor(color);
		pars.Init();
		// call wrapped function
		Draw2D(pars,rectA,clipA);
	}
	virtual void DrawPoints( int beg, int end ){} // 3D points

	virtual void PrepareMesh( int spec ) = NULL; // prepare internal variables
	virtual void BeginMesh( TLVertexTable &mesh, int spec ) = NULL; // convert all mesh vertices
	virtual void EndMesh( TLVertexTable &mesh ) = NULL; // forget mesh

	virtual AbstractTextBank *TextBank()=NULL; // texture management
	
	virtual VertexBuffer *CreateVertexBuffer(const Shape &src, VBType type) {return NULL;}
	virtual int CompareBuffers(const Shape &s1, const Shape &s2) {return 0;}

	//virtual HWND GetWindowHandle() const = NULL;

	// shadow related functions
	virtual float ZShadowEpsilon() const = NULL; // bias used for shadows
	virtual float ZRoadEpsilon() const = NULL; // bias used for roads
	virtual float ObjMipmapCoef() const = NULL; // pixel size multiplier
	//virtual float LandMipmapCoef() const = NULL; // pixel size multiplier

	virtual void GetZCoefs(float &zAdd, float &zMult) = NULL;
	virtual int GetBias() = NULL;
	virtual void SetBias( int value ) = NULL;

	virtual void SetGrassParams(float a1, float a2, float a3=0, float a4=0) {}
	virtual bool CanGrass() const {return false;}

	virtual bool CanZBias() const = NULL;
	virtual bool ZBiasExclusion() const = NULL;

	//@{ 2D viewport dimensions
	int Width2D() const;
	int Height2D() const;
	int Top2D() const;
	int Left2D() const;
	//@}

	//@{ 2D viewport conversions
	void Convert(Point2DAbs &to, const Point2DPixel &from);
	void Convert(Point2DAbs &to, const Point2DFloat &from);
	void Convert(Point2DPixel &to, const Point2DAbs &from);
	void Convert(Point2DFloat &to, const Point2DAbs &from);

	void Convert(Rect2DAbs &to, const Rect2DPixel &from);
	void Convert(Rect2DAbs &to, const Rect2DFloat &from);
	void Convert(Rect2DPixel &to, const Rect2DAbs &from);
	void Convert(Rect2DFloat &to, const Rect2DAbs &from);

	void Convert(Line2DAbs &to, const Line2DPixel &from);
	void Convert(Line2DAbs &to, const Line2DFloat &from);
	void Convert(Line2DPixel &to, const Line2DAbs &from);
	void Convert(Line2DFloat &to, const Line2DAbs &from);
	//@}
	
	void PixelAlignXY(Point2DAbs &pos);
	void PixelAlignX(Point2DAbs &pos);
	void PixelAlignY(Point2DAbs &pos);
	void PixelAlignXY(Point2DPixel &pos);
	void PixelAlignX(Point2DPixel &pos);
	void PixelAlignY(Point2DPixel &pos);

	float PixelAlignedX(float x);
	float PixelAlignedY(float x);

	// general
	virtual int Width() const =NULL;
	virtual int Height() const =NULL;
	virtual int PixelSize() const = NULL; // 16 or 32 bit mode?
	virtual int RefreshRate() const = NULL;
	virtual bool CanBeWindowed() const = NULL;
	virtual bool IsWindowed() const = NULL;

	virtual int MinGuardX() const {return 0;} // used for guard band clipping
	virtual int MaxGuardX() const {return Width();}
	virtual int MinGuardY() const {return 0;}
	virtual int MaxGuardY() const {return Height();}

	virtual int MinSatX() const {return 0;} // used for saturation
	virtual int MaxSatX() const {return Width();}
	virtual int MinSatY() const {return 0;}
	virtual int MaxSatY() const {return Height();}

	virtual int AFrameTime() const =NULL;

	void FontDestroyed( Font *font );

	#ifndef ACCESS_ONLY
	virtual void TextureDestroyed( Texture *tex ) = NULL;

	// 3D texture drawing
	void Draw3D
	(
		Vector3Par pos, Vector3Par up, Vector3Par dir, ClipFlags clip,
		PackedColor color, int spec, Texture *tex,
		float x1c = 0, float y1c = 0, float x2c = 1, float y2c = 1
	);
	void DrawLine3D
	(
		Vector3Par start, Vector3Par end,
		PackedColor color, int spec
	);

	// text drawing
	Font *LoadFont(FontID id);
	void DrawText3D
	(
		Vector3Par pos, Vector3Par up, Vector3Par dir, ClipFlags clip,
		Font *font, PackedColor color, int spec, const char *text,
		float x1c = 0, float y1c = 0, float x2c = 1e6, float y2c = 1
	);
	void CCALL DrawText3DF
	(
		Vector3Par pos, Vector3Par up, Vector3Par dir, ClipFlags clip,
		Font *font, PackedColor color, int spec, const char *text, ...
	);
	Vector3 GetText3DWidth
	(
		Vector3Par dir, Font *font, const char *text
	);
	Vector3 CCALL GetText3DWidthF
	(
		Vector3Par dir, Font *font, const char *text, ...
	);
	void DrawText
	(
		const Point2DFloat &pos, float size,
		Font *font, PackedColor color, const char *text
	);
	void DrawText
	(
		const Point2DAbs &pos, float size,
		Font *font, PackedColor color, const char *text
	);
	void DrawText
	(
		const Point2DFloat &pos, float size,
		const Rect2DFloat &clip,
		Font *font, PackedColor color, const char *text
	);
	void DrawText
	(
		const Point2DAbs &pos, float size,
		const Rect2DAbs &clip,
		Font *font, PackedColor color, const char *text
	);
	void DrawTextVertical
	(
		const Point2DFloat &pos, float size,
		Font *font, PackedColor color, const char *text
	);
	void DrawTextVertical
	(
		const Point2DFloat &pos, float size,
		const Rect2DFloat &clip,
		Font *font, PackedColor color, const char *text
	);
	float GetTextWidth
	(
		float size, Font *font, const char *text
	);
	int GetTextPosition
	(
		float x, float size, Font *font, const char *text
	);

	void CCALL DrawTextF
	(
		const Point2DFloat &pos, float size,
		Font *font, PackedColor color, const char *text, ...
	);
	void CCALL DrawTextF
	(
		const Point2DAbs &pos, float size,
		Font *font, PackedColor color, const char *text, ...
	);
	void CCALL DrawTextF
	(
		const Point2DFloat &pos, float size,
		const Rect2DFloat &clip,
		Font *font, PackedColor color, const char *text, ...
	);
	void CCALL DrawTextVerticalF
	(
		const Point2DFloat &pos, float size,
		Font *font, PackedColor color, const char *text, ...
	);
	void CCALL DrawTextVerticalF
	(
		const Point2DFloat &pos, float size,
		const Rect2DFloat &clip,
		Font *font, PackedColor color, const char *text, ...
	);
	float CCALL GetTextWidthF
	(
		float size, Font *font, const char *text, ...
	);
	#endif

	void ShowFont
	(
		Font *font,
		PackedColor color=PackedColor(0xff000000), float size=1.0
	);
	void RemoveText( int handle );
	int ShowText( DWORD timeToLive, int x, int y, const char *text );
	int CCALL ShowTextF( DWORD timeToLive, int x, int y, const char *text, ... );

	void ReinitCounters();
	
	// handling windows messages
	/*
	virtual bool CaptureMessage( HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam )
	{ // Glide needs to react to some window messages
		(void)hwnd,(void)message,(void)wParam,(void)lParam;
		return true;
	}
	*/
	// give opportunity to react to window changes
	virtual void Activate() {}
	virtual void Deactivate() {}
	virtual void Resize( int x, int y, int w, int h ) {}

	virtual void Screenshot(RString filename) {}

protected:
	const char *DrawComposedChar
	(
		Point2DAbs &pos,
		Draw2DPars &pars, const Rect2DAbs &clip, float sizeH, float sizeW, Font *font,
		const char *text
	);
	void DrawComposedChar
	(
		const Point2DAbs &pos, float w, float h,
		Draw2DPars &pars, const Rect2DAbs &clip, float sizeH, float sizeW, Font *font,
		short c
	);

	const char *DrawComposedChar3D
	(
		float &xPos, Vector3 &lPos,
		Char3DContext &ctx,
		const char *text
	);
	void DrawComposedChar3D
	(
		float xPos, Vector3Par lPos,
		float w, float h,
		Char3DContext &ctx,
		short c
	);
};

extern Engine *GEngine;

#define GLOB_ENGINE ( GEngine )

#endif

