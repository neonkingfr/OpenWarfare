///////////////////////////////////////////////////////////////////////////////
// Implementation of Arcade FSM

#include "wpch.hpp"
#include "ai.hpp"
#include "aiRadio.hpp"
#include "fsm.hpp"
#include "global.hpp"
#include "world.hpp"
#include "landscape.hpp"
#include "detector.hpp"
#include "house.hpp"
#include "person.hpp"
#include "dynSound.hpp"
#include "arcadeTemplate.hpp"

#include "camEffects.hpp"
#include "titEffects.hpp"
#include "keyInput.hpp"

#include <El/ParamFile/paramFile.hpp>
extern class ParamFile Res;
//#include "strIncl.hpp"
#include "stringtableExt.hpp"

#include <El/Common/randomGen.hpp>

#include "gameStateExt.hpp"
#include "network.hpp"

inline void Trace(AIGroup *grp, const char *waypoint, const char *state)
{
#if 1
	if (grp)
		LogF
		(
			"FSM: %s - waypoint %s - state %s, time %.1f",
			(const char *)grp->GetDebugName(), waypoint, state, Glob.time.toFloat()
		);
#endif
}

static void AllowGetIn(AIGroup *group, bool allow = true)
{
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = group->UnitWithID(i + 1);
		if (unit) unit->AllowGetIn(allow);
	}
}

static bool IsTargetValid(const Target *info, AIUnit *leader)
{
	return info && !info->destroyed && !info->vanished && info->IsKnownBy(leader);
}

#define STATE_PREFIX \
	AIGroup *group = context->_group; \
	Assert(group); \
	Mission *mission = context->_task; \
	Assert(mission); \
	(void)mission; \
	(void)group; \

///////////////////////////////////////////////////////////////////////////////
// FSM Variables usage

// Var(0) - waypoint index
// Var(1) - waypoint type
// Var(2) - waypoint idStatic
// Var(3) - waypoint id
// Var(4) - waiting for sychronization
// Var(5) - counter for Brown movement
// VarTime(0) - time limit

///////////////////////////////////////////////////////////////////////////////
// Generic Mission Functions

void MissionSucceed(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);

	group->SendAnswer(AI::MissionCompleted);
	//group->MainSubgroup()->ClearMissionCommands();
}

void CheckMissionSucceed(AIGroupContext *context)
{
//	do not delete Arcade FSM - because of flee command
//	context->_fsm->SetState(FSM::FinalState, context);
}

void MissionFailed(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);

	group->SendAnswer(AI::MissionFailed);
	//group->MainSubgroup()->ClearMissionCommands();
}

void CheckMissionFailed(AIGroupContext *context)
{
//	do not delete Arcade FSM - because of flee command
//	context->_fsm->SetState(FSM::FinalState, context);
}

///////////////////////////////////////////////////////////////////////////////
// No Mission FSM

static void NoMissionWait(AIGroupContext *context)
{
}

static void CheckNoMissionWait(AIGroupContext *context)
{
}

///////////////////////////////////////////////////////////////////////////////
// Mission Arcade FSM

enum ArcadeFSMStates
{
	SArcadeInit,
	SArcadeTurn,
	SArcadeMoveMove,
	SArcadeTalkMove,
	SArcadeTalkGetOut,
	SArcadeTalkWalk,
	SArcadeDestroyMove,
	SArcadeDestroyBrown,
	SArcadeDestroyAttack,
	SArcadeGetInMove,
	SArcadeGetInSync,
	SArcadeGetInGetIn,
	SArcadeSeekAndDestroyMove,
	SArcadeSeekAndDestroyCheck,
	SArcadeSeekAndDestroyWait,
	SArcadeSeekAndDestroyOverlook,
	SArcadeSeekAndDestroyBrown,
	SArcadeJoinMove,
	SArcadeJoinSync,
	SArcadeJoinJoin,
	SArcadeLeaderMove,
	SArcadeLeaderSync,
	SArcadeLeaderJoin,
	SArcadeGetOutMove,
	SArcadeGetOutGetOut,
	SArcadeLoadMove,
	SArcadeLoadGetIn,
	SArcadeUnloadMove,
	SArcadeUnloadGetOut,
	SArcadeTransportUnloadMove,
	SArcadeTransportUnloadGetOut,
	SArcadeHoldMove,
	SArcadeHoldWait,
	SArcadeHoldOverlook,
	SArcadeSentryMove,
	SArcadeSentryWait,
	SArcadeSentryOverlook,
	SArcadeSentryBrown,
	SArcadeGuardMove,
	SArcadeGuardWait,
	SArcadeGuardAttack,
	SArcadeGuardOverlook,
	SArcadeGuardBrown,
	SArcadeGuardBrownTarget,
	SArcadeGravonWait,
	SArcadeGravonAttack,
	SArcadeGravonOverlook,
	SArcadeGravonMove,
	SArcadeSupportMove,
	SArcadeSupportWait,
	SArcadeSupportTransport,
	SArcadeSupportSupply,
	SArcadeScripted,
	SArcadeLogic,
	SArcadeSync,
	SArcadeCountdown,
	SArcadeNext,
	SArcadeUnlock,

	SArcadeFlee,

	SArcadeDone, SArcadeFail
};

void Flee(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);

	// reset radio messages to be able to react quickly
	group->GetRadio().CancelAllMessages();
	// reset all commands, join all subgroups
	for (int i=0; i<group->NSubgroups();)
	{
		AISubgroup *subgrp = group->GetSubgroup(i);
		Assert(subgrp); // RefArray
		if (subgrp == group->MainSubgroup())
		{
			subgrp->ClearAllCommands();
			i++;
		}
		else
		{
			int nSubgroups = group->NSubgroups();
			subgrp->JoinToSubgroup(group->MainSubgroup());
			if (group->NSubgroups() >= nSubgroups)
			{
				Fail("Subgroup not joined");
				i++;
			}
		}
	}

	context->_fsm->SetState(SArcadeFlee, context);
}

void Unflee(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);

	AI::Semaphore sem = AI::SemaphoreYellow;
	SpeedMode speed = SpeedNormal;
	int &index = context->_fsm->Var(0);
	if (index < group->NWaypoints())
	{
		const ArcadeWaypointInfo &wInfo = group->GetWaypoint(index);
		speed = wInfo.speed;
		if (wInfo.combatMode >= 0)
			sem = (AI::Semaphore)wInfo.combatMode;
	}

	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = group->UnitWithID(i + 1);
		if (unit) unit->SetSemaphore(sem);
	}
	group->SetSemaphore(sem);
	group->MainSubgroup()->SetSpeedMode(speed);

	context->_fsm->SetState(SArcadeTurn,context);
}

#define SYNC_TIMEOUT				600.0f	// 10 min
#define PRECISION_COEF			5.0f		// benevolence of waypoints accomplishment for player
#define MOVE_BACK_COEF			3.5f
#define MOVE_BACK_MINIMUM		10.0f

static void SetFormMode(AIGroup *group, int index)
{
	// combat mode, formation		
	if (!group->IsPlayerGroup())
	{
		PackedBoolArray all;
		for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
		{
			AIUnit *unit = group->UnitWithID(i + 1);
			if (!unit) continue;
			all.Set(i,true);
		}

		const ArcadeWaypointInfo &wInfo = group->GetWaypoint(index);
		if (wInfo.formation >= 0)
		{
			AI::Formation f = wInfo.formation;
			group->SendFormation(f, group->MainSubgroup());
		}
		if (wInfo.combatMode >= 0)
		{
			AI::Semaphore s = wInfo.combatMode;
			group->SetSemaphore(s);
			group->SendSemaphore(s, all);
		}
		if (wInfo.speed != SpeedUnchanged)
			group->MainSubgroup()->SetSpeedMode(wInfo.speed);
		if (wInfo.combat != CMUnchanged)
//			group->SetCombatModeMajor(wInfo.combat);
			group->SendBehaviour(wInfo.combat, all);
	}
}

bool IsSyncActive(AIGroup *group, ArcadeWaypointInfo &wInfo, int oper = ACAND)
{
	AIUnit *leader = group->Leader();
	Object *leaderVeh = leader ? leader->GetPerson() : NULL;

	GameState *gstate = GWorld->GetGameState();
	GameValue thisList = gstate->CreateGameValue(GameArray);
	GameArrayType &array = thisList;
	if (group)
	{
		array.Realloc(group->NUnits());
		for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
		{
			AIUnit *unit = group->UnitWithID(i+1);
			if (!unit) continue;
			EntityAI *veh = unit->GetPerson();
			array.Add(GameValueExt(veh));
		}
	}
	gstate->VarSet("this", GameValueExt(leaderVeh), true);
	gstate->VarSet("thisList", thisList, true);
	if (!gstate->EvaluateBool(wInfo.expCond)) return true;

	int n = wInfo.synchronizations.Size();
	if (n == 0) return false;
	switch (oper)
	{
		case ACOR:
			for (int i=0; i<n; i++)
			{
				int sync = wInfo.synchronizations[i];
				Assert(sync >= 0);
				if (!synchronized[sync].IsActive(group))
					return false;
			}
			return true;
		case ACAND:
		default:
			for (int i=0; i<n; i++)
			{
				int sync = wInfo.synchronizations[i];
				Assert(sync >= 0);
				if (synchronized[sync].IsActive(group))
					return true;
			}
			return false;
	}
}

static void ArcadeInit(AIGroupContext *context)
{
	int &index = context->_fsm->Var(0);
	index = 1;
	int &waiting = context->_fsm->Var(4);
	waiting = FALSE;
}

static void CheckArcadeInit(AIGroupContext *context)
{
	context->_fsm->SetState(SArcadeTurn, context);
}

static void ArcadeTurn(AIGroupContext *context)
{
	int &index = context->_fsm->Var(0);
	int &type = context->_fsm->Var(1);

	int &waiting = context->_fsm->Var(4);
	waiting = FALSE;
	
	STATE_PREFIX

	if (index < group->NWaypoints())
	{
getWaypoint:
		const ArcadeWaypointInfo &wInfo = group->GetWaypoint(index);
		
		type = wInfo.type;
		if (type == ACCYCLE)
		{
			if (index == 0)
			{
				context->_fsm->SetState(SArcadeDone, context);
				return;
			}
			float minDist2 = FLT_MAX;
			int iBest = -1;
			for (int i=0; i<index; i++)
			{
				float dist2 =
					(group->GetWaypoint(i).position - wInfo.position).SquareSizeXZ();
				if (dist2 < minDist2)
				{
					iBest = i;
					minDist2 = dist2;
				}
			}
			Assert(iBest >= 0);
			for (int i=iBest; i<index; i++)
			{
				ArcadeWaypointInfo &wInfo = group->GetWaypoint(i);
				for (int j=0; j<wInfo.synchronizations.Size(); j++)
				{
					int sync = wInfo.synchronizations[j];
					Assert(sync >= 0);
					synchronized[sync].SetActive(group, true);
					GetNetworkManager().GroupSynchronization(group, sync, true);
				}
			}

			index = iBest;
			goto getWaypoint;
		}

		mission->_destination = wInfo.position;
/*
		RString description = wInfo.description;
		if (description.GetLength() > 0 && group->Leader())
		{
			group->GetRadio().Transmit
			(
				new RadioMessageText(description, "", 5.0F),
				group->GetCenter()->GetLanguage(),
				group->Leader()->GetSpeaker()
			);
		}
*/
		int &id = context->_fsm->Var(3);
		id = wInfo.id;
		int &idStatic = context->_fsm->Var(2);
		idStatic = wInfo.idStatic;
	}
	else
	{
		context->_fsm->SetState(SArcadeDone, context);
	}
}

void OnWaypointsUpdated(AIGroupContext *context)
{
  if (context->_fsm->GetState() == SArcadeDone)
    context->_fsm->SetState(SArcadeTurn, context); // for finished FSM get chance to process new waypoints
}

static void CheckArcadeTurn(AIGroupContext *context)
{
	int &type = context->_fsm->Var(1);
	AIGroup *group = context->_group;
	Assert(group);

	SetFormMode(group, context->_fsm->Var(0));
	switch (type)
	{
		case ACMOVE:
			context->_fsm->SetState(SArcadeMoveMove, context);
			break;
		case ACTALK:
			context->_fsm->SetState(SArcadeTalkMove, context);
			break;
		case ACDESTROY:
			context->_fsm->SetState(SArcadeDestroyMove, context);
			break;
		case ACGETIN:
			context->_fsm->SetState(SArcadeGetInMove, context);
			break;
		case ACSEEKANDDESTROY:
			context->_fsm->SetState(SArcadeSeekAndDestroyMove, context);
			break;
		case ACJOIN:
			context->_fsm->SetState(SArcadeJoinMove, context);
			break;
		case ACLEADER:
			context->_fsm->SetState(SArcadeLeaderMove, context);
			break;
		case ACGETOUT:
			context->_fsm->SetState(SArcadeGetOutMove, context);
			break;
		case ACLOAD:
			context->_fsm->SetState(SArcadeLoadMove, context);
			break;
		case ACUNLOAD:
			context->_fsm->SetState(SArcadeUnloadMove, context);
			break;
		case ACTRANSPORTUNLOAD:
			context->_fsm->SetState(SArcadeTransportUnloadMove, context);
			break;
		case ACHOLD:
			context->_fsm->SetState(SArcadeHoldMove, context);
			break;
		case ACSENTRY:
			context->_fsm->SetState(SArcadeSentryMove, context);
			break;
		case ACGUARD:
			context->_fsm->SetState(SArcadeGuardMove, context);
			break;
		case ACSUPPORT:
			context->_fsm->SetState(SArcadeSupportMove, context);
			break;
		case ACSCRIPTED:
			context->_fsm->SetState(SArcadeScripted, context);
			break;
		case ACOR:
		case ACAND:
			context->_fsm->SetState(SArcadeLogic, context);
			break;
	}

	// TODO: Place this insurance into some CHECK functions
	/*
	if (group->GetAllDone())
	{
		context->_fsm->SetState(SArcadeSync, context);
	}
	*/
}

static Vector3 GetWaypointPosition(AIGroup *group, int index)
{
	DoAssert(group);
	DoAssert(index < group->NWaypoints());
	const ArcadeWaypointInfo &wInfo = group->GetWaypoint(index);

	EntityAI *target = NULL;
	if (wInfo.id >= 0 && wInfo.id < vehiclesMap.Size())
		target = vehiclesMap[wInfo.id];
	else if (wInfo.idStatic >= 0)
		target = dyn_cast<EntityAI>(GLOB_LAND->FindObject(wInfo.idStatic));
	
	if (target)
	{
		const AITargetInfo *info = group->GetCenter()->FindTargetInfo(target);
		if (info) return info->_realPos;
	}

	return wInfo.position;
}

static void ArcadeMove(AIGroupContext *context)
{
	STATE_PREFIX

	int &index = context->_fsm->Var(0);
	mission->_destination = GetWaypointPosition(group, index);

	if (group->IsPlayerGroup()) return;

/*
	AIUnit *leader = group->MainSubgroup()->Leader();
	if (leader)
	{
		Vector3Val posL = leader->Position();
		Vector3Val posD = mission->_destination;
		float prec = PRECISION_COEF * leader->GetVehicle()->GetPrecision();
		saturateMax(prec, 10.0F);
		if ((posD - posL).SquareSizeXZ() <= Square(prec)) return;
	}
*/

	group->Move
	(
		group->MainSubgroup(),	// who
		mission->_destination,	// where
		Command::Undefined			// how
	);
}

static bool CheckArcadeMove(AIGroupContext *context, ArcadeFSMStates next)
{
	STATE_PREFIX

	AIUnit *leader = group->Leader();
	if (!leader) return false; 

	if (group->IsPlayerDrivenGroup())
	{
		for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
		{
			AIUnit *unit = group->UnitWithID(i + 1);
			if (!unit) continue;

			Vector3Val posU = unit->Position();
			Vector3Val posD = mission->_destination;
			float prec = PRECISION_COEF * unit->GetVehicle()->GetPrecision();
			saturateMax(prec, 10.0F);
			if ((posD - posU).SquareSizeXZ() <= Square(prec))
			{
				context->_fsm->SetState(next, context);
				return true;
			}
		}
		return false;
	}
	else if (group->GetCenter()->GetSide() == TLogic)
	{
		context->_fsm->SetState(next, context);
		return true;
	}
	else
	{
		if (group->GetAllDone())
		{
			context->_fsm->SetState(next, context);
			return true;
		}
		else
		{
			AIUnit *vehicleCommander = leader->GetVehicle()->CommanderUnit();
			if (vehicleCommander && vehicleCommander->GetGroup() == group)
			{
				Vector3Val posL = leader->Position();
				Vector3Val posD = mission->_destination;
				float prec = 1.0 * leader->GetVehicle()->GetPrecision();
				if (posD.Distance2(posL) <= Square(prec))
				{
					context->_fsm->SetState(next, context);
					return true;
				}
			}
			return false;
		}
	}
}

static void ArcadeWait(AIGroupContext *context)
{
/*
	STATE_PREFIX

	if (!group->IsPlayerGroup())
		group->Wait
		(
			group->MainSubgroup(),	// who
			TIME_MIN,								// until
			Command::Undefined			// how
		);
*/
}

extern SoundPars EnvSoundPars[];
extern SoundPars EnvSoundParsNight[];
void FindEnvSound(RString name, SoundPars &day, SoundPars &night);
const ParamEntry *FindMusic(RString name, SoundPars &pars);
const ParamEntry *FindRscTitle(RString name);

/*!
\patch 1.93 Date 9/10/2003 by Ondra
- Fixed: Crash possible when deleteVehicle was used in waypoint "On activation" field.
*/

void ApplyEffects(AIGroup *group, int index)
{
	Assert(group);
	if (!group) return;
	Assert(index < group->NWaypoints());
	if (index >= group->NWaypoints()) return;
	const ArcadeWaypointInfo &wInfo = group->GetWaypoint(index);
	const ArcadeEffects &effects = wInfo.effects;

	OLink<AIUnit> leader = group->Leader();
	OLink<Object> leaderVeh = leader ? leader->GetPerson() : NULL;

	GameState *gstate = GWorld->GetGameState();
	GameValue thisList = gstate->CreateGameValue(GameArray);
	GameArrayType &array = thisList;
	if (group)
	{
		array.Realloc(group->NUnits());
		for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
		{
			AIUnit *unit = group->UnitWithID(i+1);
			if (!unit) continue;
			EntityAI *veh = unit->GetPerson();
			array.Add(GameValueExt(veh));
		}
	}
	gstate->VarSet("this", GameValueExt(leaderVeh), true);
	gstate->VarSet("thisList", thisList, true);
	gstate->Execute(wInfo.expActiv);
	GameValue result = gstate->Evaluate(effects.condition);
	if (result.GetType() == GameObject)
	{
		AIUnit *player = GWorld->FocusOn();
		if (!player) return;
		Object *obj = static_cast<GameDataObject *>(result.GetData())->GetObject();
		if
		(
			player->GetPerson() != obj &&
			player->GetVehicle() != obj
		) return;
	}
	else if (result.GetType() == GameArray)
	{
		AIUnit *player = GWorld->FocusOn();
		if (!player) return;
		bool found = false;
		const GameArrayType &array = (const GameArrayType &)(GameArrayType&)result;
		for (int i=0; i<array.Size(); i++)
		{
			const GameValue &item = array[i];
			Object *obj = static_cast<GameDataObject *>(item.GetData())->GetObject();
			if
			(
				player->GetPerson() == obj ||
				player->GetVehicle() == obj
			)
			{
				found = true;
				break;
			}
		}
		if (!found) return;
	}
	else if (result.GetType() & GameBool)
	{
		if (!(bool)result) return;
	}
	else return;

	Object *obj = leader ? leader->GetVehicle() : NULL;
	
	if (effects.cameraEffect.GetLength() > 0)
	{
		if (stricmp(effects.cameraEffect, "$TERMINATE$") == 0)
			GLOB_WORLD->SetCameraEffect(NULL);
		else
			GLOB_WORLD->SetCameraEffect
			(
				CreateCameraEffect
				(
					obj,
					effects.cameraEffect,
					effects.cameraPosition
				)
			);
	}

	if (stricmp(effects.sound, "$NONE$") != 0)
	{
		Vehicle *veh = new SoundOnVehicle(effects.sound, NULL);
		GWorld->AddBuilding(veh);
	}

	if (effects.voice.GetLength() > 0)
	{
		Vehicle *veh = new SoundOnVehicle(effects.voice, obj);
		if (obj) veh->SetPosition(obj->Position());
		GWorld->AddBuilding(veh);
	}

	if (effects.soundEnv.GetLength() > 0)
		FindEnvSound(effects.soundEnv, EnvSoundPars[5], EnvSoundParsNight[5]);

	// effects.soundDet - used only for detector

	if (stricmp(effects.track, "$NONE$") == 0)
	{
		// nothing to do
	}
	else if (stricmp(effects.track, "$STOP$") == 0)
	{
		// stop musical track
		GSoundScene->StopMusicTrack();
	}
	else
	{
		// start musical track
		SoundPars sound;
		if (FindMusic(effects.track, sound))
			// start sound as musical track
			GSoundScene->StartMusicTrack(sound);
	}

	switch (effects.titleType)
	{
		case TitleNone:
			break;
		case TitleObject:
			GLOB_WORLD->SetTitleEffect
			(
				CreateTitleEffectObj
				(
					effects.titleEffect,
					Pars >> "CfgTitles" >> effects.title
				)
			);
			break;
		case TitleResource:
			{
				const ParamEntry *cls = FindRscTitle(effects.title);
				if (cls) GWorld->SetTitleEffect
				(
					CreateTitleEffectRsc(effects.titleEffect, *cls)
				);
			}
			break;
		case TitleText:
			GLOB_WORLD->SetTitleEffect
			(
				CreateTitleEffect
				(
					effects.titleEffect,
					Localize(effects.title)
				)
			);
			break;
	}
}

static void ArcadeSync(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);

	int &index = context->_fsm->Var(0);
	ArcadeWaypointInfo &wInfo = group->GetWaypoint(index);

	for (int j=0; j<wInfo.synchronizations.Size(); j++)
	{
		int sync = wInfo.synchronizations[j];
		Assert(sync >= 0);
		synchronized[sync].SetActive(group, false);
		GetNetworkManager().GroupSynchronization(group, sync, false);
	}

	if (IsSyncActive(group, wInfo))
	{
		int &waiting = context->_fsm->Var(4);
		waiting = TRUE;

		// wait
		if (!group->IsPlayerGroup())
			group->Wait
			(
				group->MainSubgroup(),		// who
				Glob.time + SYNC_TIMEOUT,	// timeout
				Command::Undefined				// how
			);
	}
}

static void CheckArcadeSync(AIGroupContext *context, ArcadeFSMStates next)
{
	AIGroup *group = context->_group;
	Assert(group);
	int &index = context->_fsm->Var(0);
	ArcadeWaypointInfo &wInfo = group->GetWaypoint(index);

	if (!IsSyncActive(group, wInfo))
	{
		int &waiting = context->_fsm->Var(4);
		waiting = FALSE;

		context->_fsm->SetState(next, context);
	}
}

static void CheckArcadeSync(AIGroupContext *context)
{
	CheckArcadeSync(context, SArcadeCountdown);
}

static void ArcadeCountdown(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);

	int &index = context->_fsm->Var(0);

	ArcadeWaypointInfo &wInfo = group->GetWaypoint(index);
	Time limit = Glob.time + GRandGen.Gauss
	(
		wInfo.timeoutMin, wInfo.timeoutMid, wInfo.timeoutMax
	);
	Time &time = context->_fsm->VarTime(0);
	time = limit;
}

static void CheckArcadeCountdown(AIGroupContext *context)
{
	Time &time = context->_fsm->VarTime(0);
	if (Glob.time >= time)
	{
		context->_fsm->SetState(SArcadeNext, context);
	}
}

static void ArcadeNext(AIGroupContext *context)
{
}

static void CheckArcadeNext(AIGroupContext *context)
{
	context->_fsm->SetState(SArcadeUnlock, context);

	AIGroup *group = context->_group;
	Assert(group);
	int &index = context->_fsm->Var(0);
	ApplyEffects(group, index);
}

static void ArcadeUnlock(AIGroupContext *context)
{
}

static void CheckArcadeUnlock(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);
	if (!group->IsLockedWP())
	{
		int &index = context->_fsm->Var(0);
		index++;
		context->_fsm->SetState(SArcadeTurn, context);
	}
}

///////////////////////////////////////////////////////////////////////////////
// Move waypoint

static void ArcadeMoveMove(AIGroupContext *context)
{
	STATE_PREFIX
	if (group->IsPlayerGroup()) return;

	int &index = context->_fsm->Var(0);
	ArcadeWaypointInfo &wInfo = group->GetWaypoint(index);

	mission->_destination = GetWaypointPosition(group, index);

	if (group->GetCenter()->GetSide() == TLogic)
	{
		group->Leader()->GetVehicle()->Move(mission->_destination);
		return;
	}

	AISubgroup *subgrp = group->MainSubgroup();
	Assert(subgrp);
	AIUnit *leader = subgrp->Leader();
	Vector3Val posD = mission->_destination;
	if (leader)
	{
		Vector3Val posL = leader->Position();
/*
		float prec = PRECISION_COEF * leader->GetVehicle()->GetPrecision();
		saturateMax(prec, 10.0F);
		if ((posD - posL).SquareSizeXZ() <= Square(prec)) return;
*/

		float dist = 200;
		if ((posL - posD).SquareSizeXZ() > Square(dist))
		{
			for (int i=0; i<subgrp->NUnits(); i++)
			{
				AIUnit *unit = subgrp->GetUnit(i);
				if (unit) unit->OrderGetIn(true);
			}
		}
		group->AssignVehicles();
		group->GetInVehicles();
	}

	Command cmd;
	cmd._message = Command::Move;
	cmd._destination = posD;
	if (wInfo.idStatic >= 0 && wInfo.housePos >= 0)
	{
		for (int i=0; i<GWorld->NBuildings(); i++)
		{
			EntityAI *veh = dyn_cast<EntityAI>(GWorld->GetBuilding(i));
			if (!veh) continue;
			if (veh->ID() == wInfo.idStatic)
			{
				cmd._target = veh;
				cmd._param = wInfo.housePos;
				const IPaths *house = veh->GetIPaths();
				if (house)
				{
					if (wInfo.housePos >= 0 && wInfo.housePos < house->NPos())
						cmd._destination = house->GetPosition(house->GetPos(wInfo.housePos));
				}
				break;
			}
		}
	}
	cmd._discretion = Command::Undefined;
	cmd._context = Command::CtxMission;
	group->SendCommand(cmd);
}

static void CheckArcadeMoveMove(AIGroupContext *context)
{
	CheckArcadeMove(context, SArcadeSync);
}

///////////////////////////////////////////////////////////////////////////////
// Scripted waypoint

static void ArcadeScripted(AIGroupContext *context)
{
	STATE_PREFIX

//	mission->_destination = GetWaypointPosition(group, index);

	int &index = context->_fsm->Var(0);
	ArcadeWaypointInfo &wInfo = group->GetWaypoint(index);

	GameArrayType position;
	position.Resize(3);
	position[0] = wInfo.position.X();
	position[1] = wInfo.position.Z();
	position[2] = 0.0f;

	EntityAI *target = NULL;
	if (wInfo.id >= 0 && wInfo.id < vehiclesMap.Size())
		target = vehiclesMap[wInfo.id];
	else if (wInfo.idStatic >= 0)
		target = dyn_cast<EntityAI>(GLOB_LAND->FindObject(wInfo.idStatic));

	RString nameScript = wInfo.script;
	RString nameArgs;
	const char *space = strchr(nameScript, ' ');
	if (space)
	{
		nameArgs = space + 1;
		nameScript = nameScript.Substring(0, space - nameScript);
	}

	GameArrayType arguments;
	arguments.Add(GameValueExt(group));
	arguments.Add(GameValue(position));
	arguments.Add(GameValueExt(target));

	if (nameScript.GetLength() > 0)
	{
		GameState *gstate = GWorld->GetGameState();
		GameValue value = gstate->Evaluate(nameArgs);
		if (gstate->GetLastError() == EvalOK)
		{
			if (value.GetType() == GameArray)
			{
				GameArrayType& array = value;
				for (int i=0; i<array.Size(); i++) 
					arguments.Add(array[i]);
			}
			else
				arguments.Add(value);
		}
	}

	Script *script = new Script(nameScript, GameValue(arguments));
	group->SetScript(script);
}

static void CheckArcadeScripted(AIGroupContext *context)
{
	STATE_PREFIX

	Script *script = group->GetScript();
	if (!script || script->OnSimulate())
	{
		group->SetScript(NULL);
		context->_fsm->SetState(SArcadeSync, context);
	}
}

///////////////////////////////////////////////////////////////////////////////
// Talk waypoint

static bool GetTalkTarget(AIGroupContext *context, Vector3 &pos)
{
	STATE_PREFIX

	int &id = context->_fsm->Var(3);

	if (id >= 0 && id < vehiclesMap.Size())
	{
		EntityAI *veh = vehiclesMap[id];
		if (!veh) return false;
		const Target *target = group->FindTarget(veh);
		if (target)
		{
			pos = target->position;
			return true;
		}
		const AITargetInfo *info = group->GetCenter()->FindTargetInfo(veh);
		if (info)
		{
			pos = info->_realPos;
			return true;
		}
	}
	return false;
}

static void ArcadeTalkMove(AIGroupContext *context)
{
	STATE_PREFIX
	if (group->IsPlayerGroup()) return;

	Vector3 posN;
	if (!GetTalkTarget(context, posN))
	{
		context->_fsm->SetState(SArcadeSync, context);
		return;
	}

	mission->_destination = posN;

	Command cmd;
	cmd._message = Command::Move;
	cmd._destination = posN;
	cmd._discretion = Command::Undefined;
	cmd._context = Command::CtxMission;
	group->SendCommand(cmd);
}

static void CheckArcadeTalkMove(AIGroupContext *context)
{
	STATE_PREFIX
	if (group->IsPlayerGroup())
	{
		context->_fsm->SetState(SArcadeTalkWalk, context);
		return;
	}

	Vector3 posN;
	if (!GetTalkTarget(context, posN))
	{
		context->_fsm->SetState(SArcadeSync, context);
		return;
	}
	
	if (!group->Leader()) return;
	Vector3Val posL = group->Leader()->Position();
	Vector3Val posD = mission->_destination;

	float dist2LN = posL.Distance2(posN);
	if (dist2LN <= Square(25))
	{
		context->_fsm->SetState(SArcadeTalkGetOut, context);
		return;
	}
	float dist2DN = posD.Distance2(posN);
	if (dist2DN > 0.01 * dist2LN || group->GetAllDone())
	{
		// update command
		mission->_destination = posN;

		Command cmd;
		cmd._message = Command::Move;
		cmd._destination = posN;
		cmd._discretion = Command::Undefined;
		cmd._context = Command::CtxMission;
		group->SendCommand(cmd);
		return;
	}
}

static void ArcadeTalkGetOut(AIGroupContext *context)
{
	STATE_PREFIX
	if (group->IsPlayerGroup()) return;

	AllowGetIn(group, false);
}

static void CheckArcadeTalkGetOut(AIGroupContext *context)
{
	STATE_PREFIX
	if (group->IsPlayerGroup())
	{
		context->_fsm->SetState(SArcadeTalkWalk, context);
		return;
	}

	if (!group->Leader()) return;
	if (group->Leader()->IsFreeSoldier())
	{
		context->_fsm->SetState(SArcadeTalkWalk, context);
	}
}

static void ArcadeTalkWalk(AIGroupContext *context)
{
	STATE_PREFIX
	if (group->IsPlayerGroup()) return;

	int &id = context->_fsm->Var(3);
	if (id < 0 || id >= vehiclesMap.Size())
	{
		context->_fsm->SetState(SArcadeSync, context);
		return;
	}
	Object *veh = vehiclesMap[id];
	if (!veh)
	{
		context->_fsm->SetState(SArcadeSync, context);
		return;
	}
	
	// vehicle is near (25 m), can work directly with veh
	Vector3 pos = veh->Position() + 3.0 * veh->Direction();
	mission->_destination = pos;

	Command cmd;
	cmd._message = Command::Move;
	cmd._destination = pos;
	cmd._discretion = Command::Undefined;
	cmd._context = Command::CtxMission;
	group->SendCommand(cmd);
}

static void CheckArcadeTalkWalk(AIGroupContext *context)
{
	STATE_PREFIX
	if (group->IsPlayerGroup())
	{
		if (group->GetAllDone())
		{
			context->_fsm->SetState(SArcadeSync, context);
			return;
		}
	}
	else
	{
		int &id = context->_fsm->Var(3);
		if (id < 0 || id >= vehiclesMap.Size())
		{
			context->_fsm->SetState(SArcadeSync, context);
			return;
		}
		Object *veh = vehiclesMap[id];
		if (!veh)
		{
			context->_fsm->SetState(SArcadeSync, context);
			return;
		}

		if (!group->Leader()) return;
		if (!group->Leader()->IsFreeSoldier()) return;
		if (group->Leader()->Position().Distance2(veh->Position()) <= Square(5))
		{
			context->_fsm->SetState(SArcadeSync, context);
			return;
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
// Destroy waypoint


static TargetType *GetDestroyTarget( AIGroupContext *context )
{
	STATE_PREFIX

	int &id = context->_fsm->Var(3);
	int &idStatic = context->_fsm->Var(2);
	EntityAI *target=NULL;

	if (id >= 0 && id < vehiclesMap.Size())
	{
		target = vehiclesMap[id];
	}
	else if (idStatic >= 0)
	{
		target = dyn_cast<TargetType>(GLOB_LAND->FindObject(idStatic));
	}
	else
	{
		target = dyn_cast<TargetType>
		(
			GLOB_LAND->NearestObject
			(
				mission->_destination,
				100.0f,
				TypeVehicle
			)
		);
	}
	return target;
}

static bool CheckArcadeDestroyMoveHelper(AIGroupContext *context)
{
	STATE_PREFIX

	AIUnit *leader = group->Leader();
	if (!leader) return false; 

	Vector3Val posL = leader->Position();
	Vector3Val posD = mission->_destination;
	const float maxDist = 1000;
	const float minDist = 50;
	float dist2 = (posD - posL).SquareSizeXZ();
	if (dist2 <= Square(maxDist))
	{
		// check if target is visible
		TargetType *obj = GetDestroyTarget(context);
		if (!obj)
		{
			context->_fsm->SetState(SArcadeDestroyAttack, context);
			return true;
		}
		Target *tgt = group->FindTarget(obj);
		if (tgt && tgt->IsKnownBy(leader))
		{
			// target found - start attacking
			context->_fsm->SetState(SArcadeDestroyAttack, context);
			return true;
		}
		if (dist2 <= Square(minDist))
		{
			// target not found - start checking around
			context->_fsm->SetState(SArcadeDestroyBrown, context);
			return true;
		}
	}
	return false;
}

static void ArcadeDestroyMove(AIGroupContext *context)
{
	// Var(5) - counter for Brown movement
	int &counter=context->_fsm->Var(5);
	counter=5;

	if (!CheckArcadeDestroyMoveHelper(context))
		ArcadeMove(context);
}

static void CheckArcadeDestroyMove(AIGroupContext *context)
{
	STATE_PREFIX

	if (group->IsPlayerGroup())
	{
		context->_fsm->SetState(SArcadeDestroyAttack, context);
		return;
	}
	else
	{
		if (group->GetAllDone())
		{
			context->_fsm->SetState(SArcadeDestroyBrown, context);
			return;
		}
		else
		{
			CheckArcadeDestroyMoveHelper(context);
		}
	}
}

static void SetBrownMove(AIGroupContext *context, float maxTime=20.0f )
{
	STATE_PREFIX

	AIUnit *leader=group->Leader();

	const float maxSpeed = leader->GetVehicle()->GetType()->GetMaxSpeedMs();
	float maxDist = maxTime * maxSpeed;

	//Vector3Val posL = leader->Position();
	Vector3Val posL = mission->_destination;
	Vector3 posD = posL + Vector3
	(
		GRandGen.PlusMinus(0, maxDist), 0, GRandGen.PlusMinus(0, maxDist)
	);
	posD[1] = GLandscape->RoadSurfaceY(posD[0], posD[2]);

	Command cmd;
	cmd._message = Command::Move;
	cmd._destination = posD;
	cmd._discretion = Command::Undefined;
	cmd._context = Command::CtxMission;
	group->SendCommand(cmd);
}

static void ArcadeDestroyBrown(AIGroupContext *context)
{
	STATE_PREFIX

	EntityAI *target=GetDestroyTarget(context);
	if( target->Static() )
	{
		SetBrownMove(context,0);
	}
	else
	{
		SetBrownMove(context);
	}
}

static void CheckArcadeDestroyBrown(AIGroupContext *context)
{
	STATE_PREFIX

	TargetType *target=GetDestroyTarget(context);
	if (!target || target->IsDammageDestroyed())
	{
		// target destroyed
		context->_fsm->SetState(SArcadeSync, context);
		return;
	}

	// check if target is visible
	AIUnit *leader = group->Leader();
	Target *tgt=group->FindTarget(target);
	if( tgt && tgt->IsKnownBy(leader) )
	{
		// target seen recently - start attacking
		context->_fsm->SetState(SArcadeDestroyAttack, context);
		return;
	}


	if( group->GetAllDone() )
	{
		int &counter=context->_fsm->Var(5);
		if( --counter<=0 )
		{
			// TODO: this waypoint actually failed - some reaction?
			context->_fsm->SetState(SArcadeSync, context);
			return;
		}
		SetBrownMove(context);
		return;
	}
}

static void ArcadeDestroyAttack(AIGroupContext *context)
{
	STATE_PREFIX

	TargetType *target=GetDestroyTarget(context);
	if (!target || target->IsDammageDestroyed())
	{
		context->_fsm->SetState(SArcadeSync, context);
		return;
	}

	
	mission->_target = target;
	if (!group->IsPlayerGroup())
	{
		AIUnit *leader = group->Leader();
		Target *tgt=group->FindTarget(target);
		if( !tgt || !tgt->IsKnownBy(leader) )
		{
			// target lost - return to brown phase
			context->_fsm->SetState(SArcadeDestroyBrown, context);
			mission->_destination = tgt->AimingPosition();

			return;
		}
		// send target
		// consider: let mission designer set Engage / Fire status
		PackedBoolArray list;
		// set as target to all units of the group
		AISubgroup *subgrp = group->MainSubgroup();
		for (int i=0; i<subgrp->NUnits(); i++)
		{
			AIUnit *unit = subgrp->GetUnit(i);
			list.Set(unit->ID()-1,true);
		}
		group->SendTarget(tgt,true,true,list);
	}
}

static void CheckArcadeDestroyAttack(AIGroupContext *context)
{
	STATE_PREFIX

	TargetType *target=GetDestroyTarget(context);
	if (!target || target->IsDammageDestroyed())
	{
		context->_fsm->SetState(SArcadeSync, context);
		return;
	}

	if (!group->IsPlayerGroup())
	{

		Target *tgt=group->FindTarget(target);
		if( !tgt )
		{
			// target lost - seek for it
			context->_fsm->SetState(SArcadeDestroyBrown, context);
			return;
		}
		else
		{
			// target seen - update position
			mission->_destination = tgt->AimingPosition();
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
// Get In waypoint

static bool IsTargetVehicle(AIGroup *group, int index)
{
	ArcadeWaypointInfo &wInfo = group->GetWaypoint(index);
	if (wInfo.id >= 0 && wInfo.id < vehiclesMap.Size()) return true;
	for (int j=0; j<wInfo.synchronizations.Size(); j++)
	{
		int sync = wInfo.synchronizations[j];
		Assert(sync >= 0);
		if (synchronized[sync].groups.Size() == 2) return true;
	}
	return false;
}

static EntityAI *FindTargetVehicle(AIGroup *group, int index)
{
	ArcadeWaypointInfo &wInfo = group->GetWaypoint(index);

	if (wInfo.id >= 0 && wInfo.id < vehiclesMap.Size())
	{
		return dyn_cast<EntityAI>(vehiclesMap[wInfo.id].GetLink());
	}

	AIGroup *grpInto = NULL;
	for (int j=0; j<wInfo.synchronizations.Size(); j++)
	{
		int sync = wInfo.synchronizations[j];
		Assert(sync >= 0);
		if (synchronized[sync].groups.Size() == 2)
		{
			if (synchronized[sync].groups[0].group != group)
			{
				grpInto = synchronized[sync].groups[0].group;
			}
			else
			{
				grpInto = synchronized[sync].groups[1].group;
				Assert(grpInto != group);
			}
			break;
		}
	}

	if (grpInto)
	{
		for (int i=0; i<grpInto->NVehicles(); i++)
		{
			Transport *veh = grpInto->GetVehicle(i);
			if (veh && veh->GetFreeManCargo() > 0)
				return veh;
		}
	}
	return NULL;
}

static void CheckArcadeGetInMove(AIGroupContext *context)
{
	STATE_PREFIX

	int &index = context->_fsm->Var(0);
	if (!IsTargetVehicle(group, index))
	{
		context->_fsm->SetState(SArcadeGetInSync, context);
		return;
	}

	// if (!group->IsPlayerGroup())
	{
		AIUnit *leader = group->Leader();
		if (leader)
		{
			Vector3Val posL = leader->Position();
			Vector3Val posD = mission->_destination;

			// issue get-in when you are near the vehicle
			float prec = 100;
			if ((posD - posL).SquareSizeXZ() <= Square(prec))
			{
				int &index = context->_fsm->Var(0);
				EntityAI *veh =FindTargetVehicle(group,index);
				if( veh && veh->Position().Distance2(posL)<prec )
				{
					context->_fsm->SetState(SArcadeGetInSync, context);
					return;
				}
			}
		}
	}

	CheckArcadeMove(context, SArcadeGetInSync);
}

static void ArcadeGetInSync(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);

	int &index = context->_fsm->Var(0);
	ArcadeWaypointInfo &wInfo = group->GetWaypoint(index);

	if (IsSyncActive(group, wInfo))
	{
		int &waiting = context->_fsm->Var(4);
		waiting = TRUE;

		// wait
		if (!group->IsPlayerGroup())
			group->Wait
			(
				group->MainSubgroup(),		// who
				Glob.time + SYNC_TIMEOUT,	// timeout
				Command::Undefined				// how
			);
	}
}

static void CheckArcadeGetInSync(AIGroupContext *context)
{
	CheckArcadeSync(context, SArcadeGetInGetIn);
}

static void ArcadeGetInGetIn(AIGroupContext *context)
{
	STATE_PREFIX
	int &index = context->_fsm->Var(0);

	Transport *veh = dyn_cast<Transport>(FindTargetVehicle(group, index));
	mission->_target = veh;

	AIUnit *leader = group->Leader();
	Assert(leader);
	if (veh)
	{
		if (group->IsPlayerGroup())
		{
			veh->WaitForGetIn(leader);
		}
		else if (veh->GetDriverAssigned())
		{
			// assign all members to cargo
			if (veh->GetDriverAssigned() != leader)
			{
				if (leader->AssignAsCargo(veh))
				{
					for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
					{
						AIUnit *unit = group->UnitWithID(i + 1);
						if (!unit || unit == leader) continue;
						if (unit->VehicleAssigned() == veh) continue;
						if (!unit->AssignAsCargo(veh)) break;
					}
				}
				else
				{
					// nothing to do
				}
			}
		}
		else
		{
			if
			(
				!veh->GetGroupAssigned() && group->NVehicles()<=0 && !leader->VehicleAssigned()
			)
			{
				// specific case - 1st vehicle of the group
				// take care of the vehicle
				// force leader to that vehicle, but not to any specific position?
				group->AddVehicle(veh);
			}
			else
			{
				// fall back - old implementation
				Assert(!veh->QIsDriverIn());
				leader->AssignAsDriver(veh);
				group->AddVehicle(veh);
			}
		}
	}
	// assign anybody left to any free vehicles we have
	group->AssignVehicles();

	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = group->UnitWithID(i + 1);
		if (unit)
		{
			unit->AllowGetIn(true);
			unit->OrderGetIn(true);
		}
	}
}

//! Get in waypoint, Get In state, check function
/*!
	\patch 1.01 Date 06/11/2001 by Jirka
	- Fixed: waypoint Get In with no vehicle assigned was completed immediatelly
*/

static void CheckArcadeGetInGetIn(AIGroupContext *context)
{
	STATE_PREFIX

	AIUnit *leader = group->Leader();
	if (!leader) return; 

	int &index = context->_fsm->Var(0);
	ArcadeWaypointInfo &wInfo = group->GetWaypoint(index);

	EntityAI *veh = mission->_target;
	// fixed - veh == NULL -> get in assigned vehicle
	if (veh && veh->IsDammageDestroyed())
	{
		for (int j=0; j<wInfo.synchronizations.Size(); j++)
		{
			int sync = wInfo.synchronizations[j];
			Assert(sync >= 0);
			synchronized[sync].SetActive(group, false);
			GetNetworkManager().GroupSynchronization(group, sync, false);
		}

		context->_fsm->SetState(SArcadeCountdown, context);
		return;
	}

	if (group->IsPlayerGroup())
	{
		// check if leader inside
		// fixed - veh == NULL -> get in assigned vehicle
		if (!veh) veh = leader->VehicleAssigned();
		if (veh && leader->GetVehicleIn() != veh) return;
	}
	else
	{
		// check if all vehicles
		for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
		{
			AIUnit *unit = group->UnitWithID(i + 1);
			if (!unit) continue;
			if (!unit->VehicleAssigned()) continue;
			if (unit->VehicleAssigned() != unit->GetVehicleIn()) return; // wait
		}
	}

	for (int j=0; j<wInfo.synchronizations.Size(); j++)
	{
		int sync = wInfo.synchronizations[j];
		Assert(sync >= 0);
		synchronized[sync].SetActive(group, false);
		GetNetworkManager().GroupSynchronization(group, sync, false);
	}

	context->_fsm->SetState(SArcadeCountdown, context);
}

///////////////////////////////////////////////////////////////////////////////
// Join waypoint

static AIGroup *FindTargetGroup(AIGroup *group, int index)
{
	ArcadeWaypointInfo &wInfo = group->GetWaypoint(index);

	if (wInfo.id >= 0 && wInfo.id < vehiclesMap.Size())
	{
		EntityAI *veh = dyn_cast<EntityAI>(vehiclesMap[wInfo.id].GetLink());
		AIUnit *unit = veh ? veh->CommanderUnit() : NULL;
		return unit ? unit->GetGroup() : NULL;
	}

	for (int j=0; j<wInfo.synchronizations.Size(); j++)
	{
		int sync = wInfo.synchronizations[j];
		Assert(sync >= 0);
		if (synchronized[sync].groups.Size() == 2)
		{
			if (synchronized[sync].groups[0].group != group)
			{
				return synchronized[sync].groups[0].group;
			}
			else
			{
				Assert(synchronized[sync].groups[1].group != group);
				return synchronized[sync].groups[1].group;
			}
		}
	}

	return NULL;
}

TypeIsSimple(AIUnit *)

/*!
\patch 1.34 Date 12/05/2001 by Ondra
- Fixed: Dead units reported "Ready" after join.
*/

void ProcessJoinGroups(AIGroup *from, AIGroup *to)
{
	// move assigned vehicles
	for (int i=0; i<from->NVehicles(); i++)
	{
		Transport *veh = from->GetVehicle(i);
		if (veh) to->AddVehicle(veh);
	}

	// join
	AUTO_STATIC_ARRAY(AIUnit *,joined, 32)
	bool sendJoin = false;
	if (to->Leader() && from->Leader())
	{
		sendJoin = to->Leader() != GWorld->FocusOn() ||
		to->Leader()->Position().Distance2(from->Leader()->Position()) < Square(200);
	}
	Person *player = GWorld->GetRealPlayer();
	AIUnit *playerUnit = player ? player->Brain() : NULL;
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		if (to->NUnits() >= MAX_UNITS_PER_GROUP) break;

		Ref<AIUnit> unit = from->UnitWithID(i + 1);
		if (!unit) continue;
		int id = -1;
		if (GWorld->GetMode() != GModeNetware && unit == playerUnit) id = unit->ID();
		unit->ForceRemoveFromGroup();
		to->AddUnit(unit, id);
		joined.Add(unit);

		if (!sendJoin)
		{
			AISubgroup *subgrp = new AISubgroup();
			to->AddSubgroup(subgrp);
			subgrp->AddUnit(unit);
			subgrp->SelectLeader(unit);
			if (GWorld->GetMode() == GModeNetware)
			{
				GetNetworkManager().CreateObject(subgrp);
				GetNetworkManager().UpdateObject(subgrp);
			}
		}
	}
	
	if (GWorld->GetMode() == GModeNetware)
	{
		GetNetworkManager().UpdateObject(to->MainSubgroup());
		GetNetworkManager().UpdateObject(to);
	}
	if (from->NUnits() == 0)
		from->ForceRemoveFromCenter();
	else
	{
		if (!from->Leader()) from->GetCenter()->SelectLeader(from);
	}

	// update values for flee
	to->CalculateMaximalStrength();

	if (to->NUnits() == 0) return;

	// select leader
	AICenter *center = to->GetCenter();
	if (!to->Leader()) center->SelectLeader(to);
	Assert(to->Leader());

	// special case - formation leader inside vehicle
	AIUnit *formLeader = to->MainSubgroup()->Leader();
	if (formLeader && !formLeader->IsUnit())
		to->MainSubgroup()->SelectLeader();

	// radio
	GWorld->SetActiveChannels();
	PackedBoolArray list;
	Command cmd; cmd._message = Command::Join; cmd._context = Command::CtxMission;
	for (int i=0; i<joined.Size(); i++) list.Set(joined[i]->ID() - 1, true);
	to->GetRadio().Transmit
	(
		new RadioMessageJoin(to, list),
		to->GetCenter()->GetLanguage()
	);
	for (int i=0; i<joined.Size(); i++)
		if (joined[i] != playerUnit && joined[i]->GetLifeState()==AIUnit::LSAlive)
		{
			to->GetRadio().Transmit
			(
				new RadioMessageJoinDone(joined[i], to),
				to->GetCenter()->GetLanguage()
			);
		}

	// allow get in by leader
	bool allow = to->Leader()->IsGetInAllowed();
	AllowGetIn(to, allow);

	DoAssert( to->AssertValid() );
}

static void JoinGroups(AIGroup *from, AIGroup *to)
{
	Assert(from);
	Assert(to);
	Assert(from != to);
	DoAssert( from->AssertValid() );
	DoAssert( to->AssertValid() );

	ApplyEffects(from, from->GetCurrent()->_fsm->Var(0));

	if (to->IsLocal()) ProcessJoinGroups(from, to);
	else GetNetworkManager().AskForJoin(to, from);
}

static void CheckArcadeJoinMove(AIGroupContext *context)
{
	CheckArcadeMove(context, SArcadeJoinSync);
}

static void CheckArcadeJoinSync(AIGroupContext *context)
{
	CheckArcadeSync(context, SArcadeJoinJoin);
}

static void ArcadeJoinJoin(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);
	Trace(group, "Join", "Join");

	int &index = context->_fsm->Var(0);

	AIGroup *join = FindTargetGroup(group, index);
	if (!join || join == group)
	{
		context->_fsm->SetState(SArcadeCountdown, context);
		return;
	}

	JoinGroups(group, join);
}

static void CheckArcadeJoinJoin(AIGroupContext *context)
{
	context->_fsm->SetState(SArcadeCountdown, context);
}

///////////////////////////////////////////////////////////////////////////////
// Join And Lead waypoint

static void CheckArcadeLeaderMove(AIGroupContext *context)
{
	CheckArcadeMove(context, SArcadeLeaderSync);
}

static void CheckArcadeLeaderSync(AIGroupContext *context)
{
	CheckArcadeSync(context, SArcadeLeaderJoin);
}

static void ArcadeLeaderJoin(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);
	Trace(group, "Join and Lead", "Join");

	int &index = context->_fsm->Var(0);

	AIGroup *join = FindTargetGroup(group, index);
	if (!join || join == group)
	{
		context->_fsm->SetState(SArcadeCountdown, context);
		return;
	}

	JoinGroups(join, group);
}

static void CheckArcadeLeaderJoin(AIGroupContext *context)
{
	context->_fsm->SetState(SArcadeCountdown, context);
}

///////////////////////////////////////////////////////////////////////////////
// Get Out waypoint

static void CheckArcadeGetOutMove(AIGroupContext *context)
{
	CheckArcadeMove(context, SArcadeGetOutGetOut);
}

static void ArcadeGetOutGetOut(AIGroupContext *context)
{
	STATE_PREFIX

	AllowGetIn(group, false);
}

static void CheckArcadeGetOutGetOut(AIGroupContext *context)
{
	STATE_PREFIX

	AIUnit *leader = group->Leader();

	if (!leader) return; 

	if (leader->IsFreeSoldier())
	{
		context->_fsm->SetState(SArcadeSync, context);
	}
}

///////////////////////////////////////////////////////////////////////////////
// Load waypoint

static void CheckArcadeLoadMove(AIGroupContext *context)
{
	CheckArcadeMove(context, SArcadeLoadGetIn);
}

static void ArcadeLoadGetIn(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);

	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = group->UnitWithID(i + 1);
		if (unit)
		{
			unit->AllowGetIn(true);
			unit->OrderGetIn(true);
		}
	}
}

static void CheckArcadeLoadGetIn(AIGroupContext *context)
{
	// TODO: ?? check if command succeed
	context->_fsm->SetState(SArcadeSync, context);
}

///////////////////////////////////////////////////////////////////////////////
// Unload waypoint

static void CheckArcadeUnloadMove(AIGroupContext *context)
{
	CheckArcadeMove(context, SArcadeUnloadGetOut);
}

static void ArcadeUnloadGetOut(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);

	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = group->UnitWithID(i + 1);
		if (!unit) continue;
		if (unit->IsInCargo()) unit->AllowGetIn(false);
		else
		{
			Transport *veh = unit->VehicleAssigned();
			if (veh)
			{
				bool assignedAsCargo = false;
				for (int i=0; i<veh->NCargoAssigned(); i++)
				{
					if (veh->GetCargoAssigned(i) == unit)
					{
						assignedAsCargo = true; break;
					}
				}
				if (assignedAsCargo) unit->AllowGetIn(false);
			}
		}
	}
}

static void CheckArcadeUnloadGetOut(AIGroupContext *context)
{
	// TODO: ?? check if command succeed
	context->_fsm->SetState(SArcadeSync, context);
}

///////////////////////////////////////////////////////////////////////////////
// Transport Unload waypoint

static void CheckArcadeTransportUnloadMove(AIGroupContext *context)
{
	CheckArcadeMove(context, SArcadeTransportUnloadGetOut);
}

static void ArcadeTransportUnloadGetOut(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);

	for (int i=0; i<group->NVehicles(); i++)
	{
		Transport *veh = group->GetVehicle(i);
		if (!veh) continue;
		const ManCargo &cargo = veh->GetManCargo();
		for (int j=cargo.Size() - 1; j>=0; j--)
		{
			Person *man = cargo[j];
			if (!man) continue;
			AIUnit *unit = man->Brain();
			if (unit && unit->GetGroup() != group)
			{
				unit->AllowGetIn(false);
				unit->OrderGetIn(false);
				unit->UnassignVehicle();
				if (unit->IsGroupLeader() && unit->IsPlayer())
					veh->WaitForGetOut(unit);
			}
		}
	}
}

static void CheckArcadeTransportUnloadGetOut(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);

	// check if none foreign units inside
	for (int i=0; i<group->NVehicles(); i++)
	{
		const Transport *veh = group->GetVehicle(i);
		if (!veh) continue;
		const ManCargo &cargo = veh->GetManCargo();
		for (int j=cargo.Size() - 1; j>=0; j--)
		{
			Person *man = cargo[j];
			if (!man) continue;
			AIUnit *unit = man->Brain();
			if (unit && unit->GetGroup() != group) return;
		}
	}

	context->_fsm->SetState(SArcadeSync, context);
}

///////////////////////////////////////////////////////////////////////////////
// Hold waypoint

const Target *FindHoldTarget(AIGroupContext *context)
{
	const Target *bestEnemy = NULL;
	const Target *bestUnknown = NULL;
	float dist2Enemy = FLT_MAX;
	float dist2Unknown = FLT_MAX;

	AIGroup *group = context->_group;
	Assert(group);
	AICenter *center = group->GetCenter();
	Assert(center);
	AIUnit *leader = group->Leader();
	Vector3Val posL = leader->Position();

	const TargetList &list = group->GetTargetList();
	for (int i=0; i<list.Size(); i++)
	{
		const Target &info = *list[i];
		if (!info.IsKnownBy(leader)) continue;
		if (info.type->IsKindOf(GLOB_WORLD->Preloaded(VTypeStatic)))
			continue;
		if (info.side == TSideUnknown)
		{
			float dist2 = info.position.Distance2(posL);
			if (dist2 < dist2Unknown)
			{
				dist2Unknown = dist2;
				bestUnknown = &info;
			}
		}
		else if (center->IsEnemy(info.side))
		{
			float dist2 = info.position.Distance2(posL);
			if (dist2 < dist2Enemy)
			{
				dist2Enemy = dist2;
				bestEnemy = &info;
			}
		}
	}

	if (bestEnemy) return bestEnemy;
	else return bestUnknown;
}

enum CheckOverlookResult
{
	CORDone,
	CORIdentified,
	CORContinue,
};

static CheckOverlookResult CheckOverlook(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);

	if (group->IsPlayerGroup()) return CORDone;
	if (group->GetAllDone()) return CORDone;

	// check if target is identify
	TargetType *target = group->GetOverlookTarget();
	if (!target) return CORContinue;
	Target *info = group->FindTarget(target);
	if (!info) return CORContinue;

	if (info->side != TSideUnknown) return CORIdentified;

	return CORContinue;
}

/*
static void CheckOverlook(AIGroupContext *context, ArcadeFSMStates next)
{
	AIGroup *group = context->_group;
	Assert(group);

	if (group->IsPlayerGroup())
	{
		context->_fsm->SetState(next, context);
		return;
	}

	if (group->GetAllDone())
	{
		context->_fsm->SetState(next, context);
		return;
	}

	// check if target is identify
	TargetId target = group->GetOverlookTarget();
	if (!target) return;
	Target *info = group->FindTarget(target);
	if (!info) return;
	if (info->side != TSideUnknown)
	{
		context->_fsm->SetState(next, context);
		return;
	}
}
*/

static void CheckArcadeHoldMove(AIGroupContext *context)
{
	CheckArcadeMove(context, SArcadeHoldWait);
}

static void CheckArcadeHoldWait(AIGroupContext *context)
{
	STATE_PREFIX
	if (group->IsPlayerGroup()) return;
	AIUnit *leader = group->Leader();
	if (!leader) return; 

	const Target *info = FindHoldTarget(context);
	if (IsTargetValid(info, leader) && info->side == TSideUnknown)
	{
		// do not overlook too far
		Vector3Val posI = info->position;
		Vector3Val posD = mission->_destination;
		float dist = (posI - posD).SizeXZ();

		float threshold = group->UpdateAndGetThreshold();
		
		const float distMin = 400.0f;
		const float distMax = 800.0f;
		const float invDist = 1.0f / (distMax - distMin);
		float value = (dist - distMin) * invDist;
#if 0
LogF
(
	"Check first value %.2f (distance %.0f) - threshold %.2f",
	value, dist, threshold
);
#endif
		if (value >= threshold) return;

		// avoid duplicity of FindHoldTarget
		context->_fsm->SetState(SArcadeHoldOverlook, context);
		group->SetOverlookTarget(info->idExact);

		Command cmd;
		cmd._message = Command::Move;
		cmd._destination = posI;
		cmd._discretion = Command::Undefined;
		cmd._context = Command::CtxMission;
		group->SendCommand(cmd);
		return;
	}
	else
	{
		// no target or enemy target
		Vector3Val posL = leader->Position();
		Vector3Val posD = mission->_destination;
		float prec = MOVE_BACK_COEF * leader->GetVehicle()->GetPrecision();
		saturateMax(prec,5);
		if ((posD - posL).SquareSizeXZ() > Square(prec))
		{
			// move back
			context->_fsm->SetState(SArcadeHoldMove, context);
			return;
		}
	}
}

static void ArcadeHoldOverlook(AIGroupContext *context)
{
	// nothing to do
}

static void CheckArcadeHoldOverlook(AIGroupContext *context)
{
	STATE_PREFIX

	switch (CheckOverlook(context))
	{
		case CORDone:
			{
				TargetType *id = group->GetOverlookTarget();
				if (id)
				{
					const Target *info = group->FindTarget(id);
					if (info && info->side == TSideUnknown)
					{
						// do not overlook too far
						Vector3Val posI = info->position;
						Vector3Val posD = mission->_destination;
						float dist = (posI - posD).SizeXZ();

						float threshold = group->UpdateAndGetThreshold();
						
						const float distMin = 600.0f;
						const float distMax = 1000.0f;
						const float invDist = 1.0f / (distMax - distMin);

						float value = (dist - distMin) * invDist;
#if 0
LogF
(
	"Check further value %.2f (distance %.0f) - threshold %.2f",
	value, dist, threshold
);
#endif
						if (value < threshold)
						{
							// avoid duplicity of FindHoldTarget
							Command cmd;
							cmd._message = Command::Move;
							cmd._destination = posI;
							cmd._discretion = Command::Undefined;
							cmd._context = Command::CtxMission;
							group->SendCommand(cmd);
							return;
						}
					}
				}
				// do not continue with overlook
				context->_fsm->SetState(SArcadeHoldWait, context);
			}
			return;
		case CORIdentified:
			context->_fsm->SetState(SArcadeHoldWait, context);
			return;
		case CORContinue:
			return;
	}
}

///////////////////////////////////////////////////////////////////////////////
// Sentry waypoint

static void CheckArcadeSentryMove(AIGroupContext *context)
{
	CheckArcadeMove(context, SArcadeSentryWait);
}

static void CheckArcadeSentryWait(AIGroupContext *context)
{
	STATE_PREFIX
	if (group->IsPlayerGroup()) return;
	AIUnit *leader = group->Leader();
	if (!leader) return; 

	const Target *info = FindHoldTarget(context);
	if (!IsTargetValid(info, leader))
	{
		Vector3Val posL = leader->Position();
		Vector3Val posD = mission->_destination;
		float prec = MOVE_BACK_COEF * leader->GetVehicle()->GetPrecision();
		saturateMax(prec, MOVE_BACK_MINIMUM);
		if ((posD - posL).SquareSizeXZ() > Square(prec))
		{
			// move back
			context->_fsm->SetState(SArcadeSentryMove, context);
		}
		return;
	}
	else if (info->side == TSideUnknown)
	{
		// do not overlook too far
		Vector3Val posI = info->position;
		Vector3Val posD = mission->_destination;
		float dist = (posI - posD).SizeXZ();

		float threshold = group->UpdateAndGetThreshold();
		
		const float distMin = 400.0f;
		const float distMax = 1600.0f;
		const float invDist = 1.0f / (distMax - distMin);
		float value = (dist - distMin) * invDist;
#if 0
LogF
(
	"Check first value %.2f (distance %.0f) - threshold %.2f",
	value, dist, threshold
);
#endif
		if (value >= threshold) return;

		// avoid duplicity of FindHoldTarget
		context->_fsm->SetState(SArcadeSentryOverlook, context);
		group->SetOverlookTarget(info->idExact);

		Command cmd;
		cmd._message = Command::Move;
		cmd._destination = posI;
		cmd._discretion = Command::Undefined;
		cmd._context = Command::CtxMission;
		group->SendCommand(cmd);
		return;
	}
	else
	{
		// enemy target
		context->_fsm->SetState(SArcadeSync, context);
		return;
	}

/*
	if (CheckHoldTarget(context, SArcadeSentryOverlook, SArcadeSentryMove))
	{
		// enemy detected
		// continue
		context->_fsm->SetState(SArcadeSync, context);
		return;
	}
*/
}

static void ArcadeSentryOverlook(AIGroupContext *context)
{
	// nothing to do
}

static void CheckArcadeSentryOverlook(AIGroupContext *context)
{
	STATE_PREFIX

	switch (CheckOverlook(context))
	{
		case CORDone:
			{
				TargetType *id = group->GetOverlookTarget();
				if (id)
				{
					const Target *info = group->FindTarget(id);
					if (!info)
					{
						// target disappeared - try to find it with Brown movement
						int &counter = context->_fsm->Var(5);
						counter = 1;

						Vector3 posL = group->Leader()->Position();
						group->SetGuardPosition(posL);
						
						const float maxTime = 4.0f;
						const float maxSpeed = group->Leader()->GetVehicle()->GetType()->GetMaxSpeedMs();
						float maxDist = maxTime * maxSpeed;
						Vector3 posD = posL + Vector3
						(
							GRandGen.PlusMinus(0, maxDist), 0, GRandGen.PlusMinus(0, maxDist)
						);
						posD[1] = GLandscape->RoadSurfaceY(posD[0], posD[2]);

						Command cmd;
						cmd._message = Command::Move;
						cmd._destination = posD;
						cmd._discretion = Command::Undefined;
						cmd._context = Command::CtxMission;
						group->SendCommand(cmd);

						context->_fsm->SetState(SArcadeSentryBrown, context);
						return;
					}
					else if (info->side == TSideUnknown)
					{
						// do not overlook too far
						Vector3Val posI = info->position;
						Vector3Val posD = mission->_destination;
						float dist = (posI - posD).SizeXZ();

						float threshold = group->UpdateAndGetThreshold();
						
						const float distMin = 600.0f;
						const float distMax = 2000.0f;
						const float invDist = 1.0f / (distMax - distMin);

						float value = (dist - distMin) * invDist;
#if 0
LogF
(
	"Check further value %.2f (distance %.0f) - threshold %.2f",
	value, dist, threshold
);
#endif
						if (value < threshold)
						{
							// avoid duplicity of FindHoldTarget
							Command cmd;
							cmd._message = Command::Move;
							cmd._destination = posI;
							cmd._discretion = Command::Undefined;
							cmd._context = Command::CtxMission;
							group->SendCommand(cmd);
							return;
						}
					}
				}
				// do not continue with overlook
				context->_fsm->SetState(SArcadeSentryWait, context);
			}
			return;
		case CORIdentified:
			context->_fsm->SetState(SArcadeSentryWait, context);
			return;
		case CORContinue:
			return;
	}
/*
	CheckOverlook(context, SArcadeSentryWait);
*/
}

static void ArcadeSentryBrown(AIGroupContext *context)
{
}

static void CheckArcadeSentryBrown(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);

	if (group->IsPlayerGroup())
	{
		context->_fsm->SetState(SArcadeSentryWait, context);
		return;
	}

	AIUnit *leader = group->Leader();
	if (!leader) return; 

	const Target *info = FindHoldTarget(context);
	if (IsTargetValid(info, leader))
	{
		context->_fsm->SetState(SArcadeSentryWait, context);
		return;
	}

	if (group->GetAllDone())
	{
		int &counter = context->_fsm->Var(5);
		counter++;

		if (counter >= 5)
		{
			// cannot find it - move back
			context->_fsm->SetState(SArcadeSentryMove, context);
		}
		else
		{
			const float maxTime = 4.0f;
			const float maxSpeed = group->Leader()->GetVehicle()->GetType()->GetMaxSpeedMs();
			float maxDist = maxTime * maxSpeed;
			Vector3 posD = group->GetGuardPosition() + Vector3
			(
				GRandGen.PlusMinus(0, maxDist), 0, GRandGen.PlusMinus(0, maxDist)
			);
			posD[1] = GLandscape->RoadSurfaceY(posD[0], posD[2]);

			Command cmd;
			cmd._message = Command::Move;
			cmd._destination = posD;
			cmd._discretion = Command::Undefined;
			cmd._context = Command::CtxMission;
			group->SendCommand(cmd);
		}
		return;
	}
}

///////////////////////////////////////////////////////////////////////////////
// Seek And Destroy waypoint

static void CheckArcadeSeekAndDestroyMove(AIGroupContext *context)
{
	CheckArcadeMove(context, SArcadeSeekAndDestroyCheck);
}

static void ArcadeSeekAndDestroyCheck(AIGroupContext *context)
{
	context->_group->SetCheckTime(Glob.time + 15.0);
	int &counter = context->_fsm->Var(5);
	counter = 0;
}

static void CheckArcadeSeekAndDestroyCheck(AIGroupContext *context)
{
	context->_fsm->SetState(SArcadeSeekAndDestroyWait, context);
}

static void CheckArcadeSeekAndDestroyWait(AIGroupContext *context)
{
	STATE_PREFIX

	AIUnit *leader = group->Leader();
	if (!leader) return; 

	if (group->IsPlayerGroup())
	{
		// another processing for player group
		const Target *info = FindHoldTarget(context);
		if (IsTargetValid(info, leader) && info->side != TSideUnknown)
		{
			// enemy detected
			context->_fsm->SetState(SArcadeSeekAndDestroyCheck, context);
		}
		else if (Glob.time >= group->GetCheckTime())
		{
			// check time expired
			context->_fsm->SetState(SArcadeSync, context);
		}
		return;
	}

	const Target *info = FindHoldTarget(context);
	if (!IsTargetValid(info, leader))
	{
		int &counter = context->_fsm->Var(5);
		if (counter >= 5)
		{
			context->_fsm->SetState(SArcadeSync, context);
		}
		else
		{
			context->_fsm->SetState(SArcadeSeekAndDestroyBrown, context);
		}
		return;
	}
	else if (info->side == TSideUnknown)
	{
		// do not overlook too far
		Vector3Val posI = info->position;
		Vector3Val posD = mission->_destination;
		float dist = (posI - posD).SizeXZ();

		float threshold = group->UpdateAndGetThreshold();
		
		const float distMin = 400.0f;
		const float distMax = 1600.0f;
		const float invDist = 1.0f / (distMax - distMin);
		float value = (dist - distMin) * invDist;
#if 0
LogF
(
	"Check first value %.2f (distance %.0f) - threshold %.2f",
	value, dist, threshold
);
#endif
		if (value >= threshold) return;

		// avoid duplicity of FindHoldTarget
		context->_fsm->SetState(SArcadeSeekAndDestroyOverlook, context);
		group->SetOverlookTarget(info->idExact);

		Command cmd;
		cmd._message = Command::Move;
		cmd._destination = posI;
		cmd._discretion = Command::Undefined;
		cmd._context = Command::CtxMission;
		group->SendCommand(cmd);
		return;
	}
	else
	{
		// enemy target
		context->_fsm->SetState(SArcadeSeekAndDestroyCheck, context);
		return;
	}
	
/*
	if (CheckHoldTarget(context, SArcadeSeekAndDestroyOverlook, SArcadeSeekAndDestroyMove))
	{
		// enemy detected
		context->_fsm->SetState(SArcadeSeekAndDestroyCheck, context);
		return;
	}

	AIGroup *group = context->_group;
	Assert(group);

	if
	(
		context->_fsm->GetState() == SArcadeSeekAndDestroyWait &&
		Glob.time >= group->GetCheckTime())
	{
		// state did not changed && check time expired
		context->_fsm->SetState(SArcadeSync, context);
		return;
	}
*/
}

static void ArcadeSeekAndDestroyOverlook(AIGroupContext *context)
{
	// nothing to do
}

static void CheckArcadeSeekAndDestroyOverlook(AIGroupContext *context)
{
	STATE_PREFIX

	switch (CheckOverlook(context))
	{
		case CORDone:
			{
				TargetType *id = group->GetOverlookTarget();
				if (id)
				{
					const Target *info = group->FindTarget(id);
					if (info && info->side == TSideUnknown)
					{
						// do not overlook too far
						Vector3Val posI = info->position;
						Vector3Val posD = mission->_destination;
						float dist = (posI - posD).SizeXZ();

						float threshold = group->UpdateAndGetThreshold();
						
						const float distMin = 600.0f;
						const float distMax = 2000.0f;
						const float invDist = 1.0f / (distMax - distMin);

						float value = (dist - distMin) * invDist;
#if 0
LogF
(
	"Check further value %.2f (distance %.0f) - threshold %.2f",
	value, dist, threshold
);
#endif
						if (value < threshold)
						{
							// avoid duplicity of FindHoldTarget
							Command cmd;
							cmd._message = Command::Move;
							cmd._destination = posI;
							cmd._discretion = Command::Undefined;
							cmd._context = Command::CtxMission;
							group->SendCommand(cmd);
							return;
						}
					}
				}
				// do not continue with overlook - move back
				context->_fsm->SetState(SArcadeSeekAndDestroyMove, context);
			}
			return;
		case CORIdentified:
			context->_fsm->SetState(SArcadeSeekAndDestroyCheck, context);
			return;
		case CORContinue:
			return;
	}
/*
	CheckOverlook(context, SArcadeSeekAndDestroyCheck);
*/
}

static void ArcadeSeekAndDestroyBrown(AIGroupContext *context)
{
	STATE_PREFIX

	const float maxTime = 6.0f;
	const float maxSpeed = group->Leader()->GetVehicle()->GetType()->GetMaxSpeedMs();
	float maxDist = maxTime * maxSpeed;
	Vector3 posD = mission->_destination + Vector3
	(
		GRandGen.PlusMinus(0, maxDist), 0, GRandGen.PlusMinus(0, maxDist)
	);
	posD[1] = GLandscape->RoadSurfaceY(posD[0], posD[2]);

	Command cmd;
	cmd._message = Command::Move;
	cmd._destination = posD;
	cmd._discretion = Command::Undefined;
	cmd._context = Command::CtxMission;
	group->SendCommand(cmd);
}

static void CheckArcadeSeekAndDestroyBrown(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);

	if (group->IsPlayerGroup())
	{
		context->_fsm->SetState(SArcadeSeekAndDestroyWait, context);
		return;
	}

	AIUnit *leader = group->Leader();
	if (!leader) return; 

	const Target *info = FindHoldTarget(context);
	if (IsTargetValid(info, leader))
	{
		context->_fsm->SetState(SArcadeSeekAndDestroyWait, context);
		return;
	}

	if (group->GetAllDone())
	{
		context->_fsm->SetState(SArcadeSeekAndDestroyWait, context);
		int &counter = context->_fsm->Var(5);
		counter++;
		return;
	}
}

///////////////////////////////////////////////////////////////////////////////
// Guard waypoint

bool IsGuardSynchronized(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);
	int &index = context->_fsm->Var(0);
	ArcadeWaypointInfo &wInfo = group->GetWaypoint(index);

	for (int j=0; j<wInfo.synchronizations.Size(); j++)
	{
		int sync = wInfo.synchronizations[j];
		Assert(sync >= 0);
		if (synchronized[sync].sensors.Size() > 0)
			return true;
	}
	return false;
}

const AITargetInfo *FindGuardTarget(AIGroupContext *context)
{
	const AITargetInfo *bestEnemy = NULL;
	const AITargetInfo *bestUnknown = NULL;
	float dist2Enemy = FLT_MAX;
	float dist2Unknown = FLT_MAX;

	AIGroup *group = context->_group;
	Assert(group);
	AICenter *center = group->GetCenter();
	Assert(center);
	int &index = context->_fsm->Var(0);
	ArcadeWaypointInfo &wInfo = group->GetWaypoint(index);
	Vector3Val posL = group->Leader()->Position();

	bool found = false;
	for (int j=0; j<wInfo.synchronizations.Size(); j++)
	{
		int sync = wInfo.synchronizations[j];
		Assert(sync >= 0);
		int n = synchronized[sync].sensors.Size();
		if (n > 0)
		{
			found = true;
			for (int i=0; i<n; i++)
			{
				SynchronizedSensor &sensor = synchronized[sync].sensors[i];
				Vehicle *detVeh = sensor.sensor;
				if (!detVeh) continue;
				Detector *det = dyn_cast<Detector>(detVeh);
				if (!det) continue;
				int o = det->NVehicles();
				for (int k=0; k<o; k++)
				{
					TargetType *obj = det->GetVehicle(k);
					if (!obj) continue;
					const AITargetInfo *info = center->FindTargetInfo(obj);
					if (!info) continue;
					if (info->_side == TSideUnknown)
					{
						float dist2 = info->_realPos.Distance2(posL);
						if (dist2 < dist2Unknown)
						{
							dist2Unknown = dist2;
							bestUnknown = info;
						}
					}
					else if (center->IsEnemy(info->_side))
					{
						float dist2 = info->_realPos.Distance2(posL);
						if (dist2 < dist2Enemy)
						{
							dist2Enemy = dist2;
							bestEnemy = info;
						}
					}
				}
			}
		}
	}
	Assert(found);

	if (bestEnemy) return bestEnemy;
	else return bestUnknown;
}

Vector3Val GetSensorPosition(AIGroupContext *context)
{
	const Object *bestDetector = NULL;
	float dist2Min = FLT_MAX;

	AIGroup *group = context->_group;
	Assert(group);
	Assert(group->GetCenter());
	int &index = context->_fsm->Var(0);
	ArcadeWaypointInfo &wInfo = group->GetWaypoint(index);
	Vector3Val posL = group->Leader()->Position();

	for (int j=0; j<wInfo.synchronizations.Size(); j++)
	{
		int sync = wInfo.synchronizations[j];
		Assert(sync >= 0);
		int n = synchronized[sync].sensors.Size();
		if (n > 0)
		{
			for (int i=0; i<n; i++)
			{
				SynchronizedSensor &sensor = synchronized[sync].sensors[i];
				Vehicle *detVeh = sensor.sensor;
				if (!detVeh) continue;
				float dist2 = (detVeh->Position() - posL).SquareSizeXZ();
				if (dist2 < dist2Min)
				{
					dist2Min = dist2;
					bestDetector = detVeh;
				}
			}
		}
	}

	Assert(bestDetector);
	if (!bestDetector) return posL;

	return bestDetector->Position();
}

static void CheckArcadeGuardMove(AIGroupContext *context)
{
	ArcadeFSMStates state = IsGuardSynchronized(context) ?
		SArcadeGuardWait :
		SArcadeGravonWait;

	if (CheckArcadeMove(context, state))
	{
		Time limit = Glob.time + GRandGen.PlusMinus(450.0f, 150.0f);
		Time &time = context->_fsm->VarTime(0);
		time = limit;
	}
}

static void CheckArcadeGuardWait(AIGroupContext *context)
{
	STATE_PREFIX
	if (group->IsPlayerGroup()) return;
	AIUnit *leader = group->Leader();
	if (!leader) return; 

	const AITargetInfo *info = FindGuardTarget(context);
	if (info)
	{
		const Target *vInfo = group->FindTarget(info->_idExact);
		if (!vInfo || vInfo->side == TSideUnknown)
		{
			Trace(group, "GUARD", "OVERLOOK");

			context->_fsm->SetState(SArcadeGuardOverlook, context);
			group->SetOverlookTarget(info->_idExact);
			// avoid duplicity of FindGuardTarget
			Command cmd;
			cmd._message = Command::Move;
			cmd._destination = info->_realPos;
			cmd._discretion = Command::Undefined;
			cmd._context = Command::CtxMission;
			group->SendCommand(cmd);
			AllowGetIn(group);
			return;
		}
		else
		{
			Trace(group, "GUARD", "ATTACK");

			// enemy
			context->_fsm->SetState(SArcadeGuardAttack, context);
			// avoid duplicity of FindGuardTarget
			Command cmd;
			cmd._message = Command::AttackAndFire;
			cmd._destination = leader->Position();	// target is known
			cmd._target = info->_idExact;
			cmd._discretion = Command::Undefined;
			cmd._context = Command::CtxMission;
			group->SendCommand(cmd);
			AllowGetIn(group);
			return;
		}
	}

	Vector3Val posL = leader->Position();
	Vector3Val posD = mission->_destination;
	float prec = MOVE_BACK_COEF * leader->GetVehicle()->GetPrecision();
	saturateMax(prec, MOVE_BACK_MINIMUM);
	float dist2 = (posD - posL).SquareSizeXZ();
	if (dist2 > Square(prec))
	{
		Trace(group, "GUARD", "MOVE BACK");

		// move back
		context->_fsm->SetState(SArcadeGuardMove, context);
		prec = 50.0f;
		if (leader->VehicleAssigned())
			prec = MOVE_BACK_COEF * leader->VehicleAssigned()->GetPrecision();
		saturateMax(prec, MOVE_BACK_MINIMUM);
		if (dist2 > Square(prec)) AllowGetIn(group);
		return;
	}

	Time &time = context->_fsm->VarTime(0);
	if (Glob.time >= time)
	{
		// look at sensor
		int &counter = context->_fsm->Var(5);
		counter = 1;

		Vector3 posS = GetSensorPosition(context);
		group->SetGuardPosition(posS);
		
		const float maxTime = 4.0f;
		const float maxSpeed = group->Leader()->GetVehicle()->GetType()->GetMaxSpeedMs();
		float maxDist = maxTime * maxSpeed;
		Vector3 posD = posS + Vector3
		(
			GRandGen.PlusMinus(0, maxDist), 0, GRandGen.PlusMinus(0, maxDist)
		);
		posD[1] = GLandscape->RoadSurfaceY(posD[0], posD[2]);

		Command cmd;
		cmd._message = Command::Move;
		cmd._destination = posD;
		cmd._discretion = Command::Undefined;
		cmd._context = Command::CtxMission;
		group->SendCommand(cmd);
		context->_fsm->SetState(SArcadeGuardBrown, context);
		AllowGetIn(group);
		return;
	}

	// idle state - get out from vehicles
	AllowGetIn(group, false);
}

static void ArcadeGuardAttack(AIGroupContext *context)
{
	// nothing to do
}

static void CheckArcadeGuardAttack(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);

	if (group->IsPlayerGroup())
	{
		context->_fsm->SetState(SArcadeGuardWait, context);
		return;
	}

	if (group->GetAllDone())
	{
		context->_fsm->SetState(SArcadeGuardWait, context);
		return;
	}
}

static void ArcadeGuardOverlook(AIGroupContext *context)
{
	// nothing to do
}

static void CheckArcadeGuardOverlook(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);

	if (group->IsPlayerGroup())
	{
		context->_fsm->SetState(SArcadeGuardWait, context);
		return;
	}

	if (group->GetAllDone())
	{
		// target was not found - try to find it with brown movement
		int &counter = context->_fsm->Var(5);
		counter = 1;

		Vector3 posL = group->Leader()->Position();
		group->SetGuardPosition(posL);
		
		const float maxTime = 4.0f;
		const float maxSpeed = group->Leader()->GetVehicle()->GetType()->GetMaxSpeedMs();
		float maxDist = maxTime * maxSpeed;
		Vector3 posD = posL + Vector3
		(
			GRandGen.PlusMinus(0, maxDist), 0, GRandGen.PlusMinus(0, maxDist)
		);
		posD[1] = GLandscape->RoadSurfaceY(posD[0], posD[2]);

		Command cmd;
		cmd._message = Command::Move;
		cmd._destination = posD;
		cmd._discretion = Command::Undefined;
		cmd._context = Command::CtxMission;
		group->SendCommand(cmd);

		context->_fsm->SetState(SArcadeGuardBrownTarget, context);
		return;
	}

	// check if target is identify
	TargetType *target = group->GetOverlookTarget();
	if (!target) return;
	const Target *vInfo = group->FindTarget(target);
	if (vInfo && vInfo->side != TSideUnknown)
	{
		context->_fsm->SetState(SArcadeGuardWait, context);
		return;
	}

/*
	AICenter *center = group->GetCenter();
	Assert(center);
	const AITargetInfo *info = center->FindTargetInfo(target);
	if (!info) return;
	if (info->_side != TSideUnknown)
	{
		context->_fsm->SetState(SArcadeGuardWait, context);
		return;
	}
*/
}

static void ArcadeGuardBrown(AIGroupContext *context)
{
	// do nothing
}

static void CheckArcadeGuardBrown(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);

	if (group->IsPlayerGroup())
	{
		context->_fsm->SetState(SArcadeGuardWait, context);
		return;
	}

	const AITargetInfo *info = FindGuardTarget(context);
	if (info)
	{
		// something found
		context->_fsm->SetState(SArcadeGuardWait, context);
	}

	if (group->GetAllDone())
	{
		int &counter = context->_fsm->Var(5);
		counter++;

		if (counter >= 5)
		{
			// cannot find anything - move back
			context->_fsm->SetState(SArcadeGuardMove, context);
		}
		else
		{
			const float maxTime = 4.0f;
			const float maxSpeed = group->Leader()->GetVehicle()->GetType()->GetMaxSpeedMs();
			float maxDist = maxTime * maxSpeed;
			Vector3 posD = group->GetGuardPosition() + Vector3
			(
				GRandGen.PlusMinus(0, maxDist), 0, GRandGen.PlusMinus(0, maxDist)
			);
			posD[1] = GLandscape->RoadSurfaceY(posD[0], posD[2]);

			Command cmd;
			cmd._message = Command::Move;
			cmd._destination = posD;
			cmd._discretion = Command::Undefined;
			cmd._context = Command::CtxMission;
			group->SendCommand(cmd);
		}
		return;
	}
}

static void ArcadeGuardBrownTarget(AIGroupContext *context)
{
	// do nothing
}

static void CheckArcadeGuardBrownTarget(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);

	if (group->IsPlayerGroup())
	{
		context->_fsm->SetState(SArcadeGuardWait, context);
		return;
	}

	// check if target is identify
	TargetType *target = group->GetOverlookTarget();
	if (target)
	{
		const Target *vInfo = group->FindTarget(target);
		if (vInfo && vInfo->side != TSideUnknown)
		{
			context->_fsm->SetState(SArcadeGuardWait, context);
			return;
		}
	}

	if (group->GetAllDone())
	{
		int &counter = context->_fsm->Var(5);
		counter++;

		if (counter >= 5)
		{
			// cannot find anything - move back
			context->_fsm->SetState(SArcadeGuardMove, context);
		}
		else
		{
			const float maxTime = 4.0f;
			const float maxSpeed = group->Leader()->GetVehicle()->GetType()->GetMaxSpeedMs();
			float maxDist = maxTime * maxSpeed;
			Vector3 posD = group->GetGuardPosition() + Vector3
			(
				GRandGen.PlusMinus(0, maxDist), 0, GRandGen.PlusMinus(0, maxDist)
			);
			posD[1] = GLandscape->RoadSurfaceY(posD[0], posD[2]);

			Command cmd;
			cmd._message = Command::Move;
			cmd._destination = posD;
			cmd._discretion = Command::Undefined;
			cmd._context = Command::CtxMission;
			group->SendCommand(cmd);
		}
		return;
	}
}

///////////////////////////////////////////////////////////////////////////////
// Guard waypoint - Gravon subdiagram

static void CheckArcadeGravonWait(AIGroupContext *context)
{
	STATE_PREFIX
	if (group->IsPlayerGroup()) return;

	AICenter *center = group->GetCenter();
	Assert(center);
	AIUnit *leader = group->Leader();
	if (!leader) return; 

	AIGuardTarget tgt = center->GetGuardTarget(group);
	switch (tgt.type)
	{
		case GTTNothing:
			{
				Vector3Val posL = leader->Position();
				Vector3Val posD = mission->_destination;
				float prec = MOVE_BACK_COEF * leader->GetVehicle()->GetPrecision();
				saturateMax(prec, MOVE_BACK_MINIMUM);
				if ((posD - posL).SquareSizeXZ() > Square(prec))
				{
					// move back
					context->_fsm->SetState(SArcadeGuardMove, context);
				}
			}
			break;
		case GTTVehicle:
			{
				const AITargetInfo *info = center->FindTargetInfo(tgt.vehicle);
				Assert(info);
				if (!info) return;
				Target *target = group->FindTarget(tgt.vehicle);
				if (target)
				{
					if (target->destroyed) return;
				}
				else
				{
					Vector3Val posL = leader->Position();
					float prec = MOVE_BACK_COEF * leader->GetVehicle()->GetPrecision();
					saturateMax(prec, 10.0);
					if (info->_realPos.Distance2(posL) < Square(prec)) return;
				}
				group->SetOverlookTarget(tgt.vehicle);
				if (center->IsEnemy(info->_side))
				{
					context->_fsm->SetState(SArcadeGravonAttack, context);

					Command cmd;
					cmd._message = Command::AttackAndFire;
					cmd._destination = info->_realPos;//leader->Position();
					cmd._target = tgt.vehicle;
					cmd._discretion = Command::Undefined;
					cmd._context = Command::CtxMission;
					group->SendCommand(cmd);
				}
				else if (info->_side == TSideUnknown)
				{
					context->_fsm->SetState(SArcadeGravonOverlook, context);

					Command cmd;
					cmd._message = Command::Move;
					cmd._destination = info->_realPos;
					cmd._discretion = Command::Undefined;
					cmd._context = Command::CtxMission;
					group->SendCommand(cmd);
				}
				else	// friendly - ignore
				{
					LogF("Avoid attacking friendly.");
				}
			}
			break;
		case GTTPoint:
			{
				Vector3Val posL = leader->Position();
				Vector3Val posD = tgt.position;
				group->SetGuardPosition(posD);
				float prec = MOVE_BACK_COEF * leader->GetVehicle()->GetPrecision();
				saturateMax(prec, MOVE_BACK_MINIMUM);
				if ((posD - posL).SquareSizeXZ() > Square(prec))
				{
					context->_fsm->SetState(SArcadeGravonMove, context);

					Command cmd;
					cmd._message = Command::Move;
					cmd._destination = posD;
					cmd._discretion = Command::Undefined;
					cmd._context = Command::CtxMission;
					group->SendCommand(cmd);
				}
			}
			break;
		default:
			Fail("Guard target type");
			break;
	}
}

static void ArcadeGravonAttack(AIGroupContext *context)
{
	// nothing to do
}

static void CheckArcadeGravonAttack(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);
	AICenter *center = group->GetCenter();
	Assert(center);

	if (group->IsPlayerGroup())
	{
		context->_fsm->SetState(SArcadeGravonWait, context);
		return;
	}

	// TODO: check if tgt doesn't changed
	AIGuardTarget tgt = center->GetGuardTarget(group);
	if (tgt.type != GTTVehicle || tgt.vehicle != group->GetOverlookTarget())
	{
		context->_fsm->SetState(SArcadeGravonWait, context);
		return;
	}

	// if all attacks are completed, we can pause
	if (group->GetAllDone())
	{
		context->_fsm->SetState(SArcadeGravonWait, context);
		return;
	}
}

static void ArcadeGravonOverlook(AIGroupContext *context)
{
	// nothing to do
}

static void CheckArcadeGravonOverlook(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);
	AICenter *center = group->GetCenter();
	Assert(center);

	if (group->IsPlayerGroup())
	{
		context->_fsm->SetState(SArcadeGravonWait, context);
		return;
	}

	// TODO: check if tgt doesn't changed
	AIGuardTarget tgt = center->GetGuardTarget(group);
	if (tgt.type != GTTVehicle || tgt.vehicle != group->GetOverlookTarget())
	{
		context->_fsm->SetState(SArcadeGravonWait, context);
		return;
	}

	if (group->GetAllDone())
	{
		context->_fsm->SetState(SArcadeGravonWait, context);
		return;
	}

	// check if target is identify
	TargetType *target = group->GetOverlookTarget();
	if (!target) return;
	const AITargetInfo *info = center->FindTargetInfo(target);
	if (!info) return;
	if (info->_side != TSideUnknown)
	{
		context->_fsm->SetState(SArcadeGravonWait, context);
		return;
	}
}

static void ArcadeGravonMove(AIGroupContext *context)
{
	// nothing to do
}

static void CheckArcadeGravonMove(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);
	AICenter *center = group->GetCenter();
	Assert(center);

	if (group->IsPlayerGroup())
	{
		context->_fsm->SetState(SArcadeGravonWait, context);
		return;
	}

	// check if tgt doesn't changed
	// if there is some target, go immediatelly to wait state
	AIGuardTarget tgt = center->GetGuardTarget(group);
	if (tgt.type != GTTPoint || (tgt.position - group->GetGuardPosition()).SquareSizeXZ() > 1.0)
	{
		context->_fsm->SetState(SArcadeGravonWait, context);
		return;
	}

	if (group->GetAllDone())
	{
		context->_fsm->SetState(SArcadeGravonWait, context);
		return;
	}
}

///////////////////////////////////////////////////////////////////////////////
// Support

static void CheckArcadeSupportMove(AIGroupContext *context)
{
	if (CheckArcadeMove(context, SArcadeSupportWait))
	{
		AIGroup *group = context->_group;
		Assert(group);
		AICenter *center = group->GetCenter();
		Assert(center);

		center->ReadyForSupport(group);
		AllowGetIn(group, false);
	}
}

static void CheckArcadeSupportWait(AIGroupContext *context)
{
	STATE_PREFIX

	if (group->GetSupportedGroup())
	{
		mission->_destination = group->GetSupportPos();
		context->_fsm->SetState(SArcadeSupportTransport, context);
		AllowGetIn(group, true);
	}
}

static void SupportConfirmed(AIGroup *group, Vector3Val pos)
{
	AICenter *center = group->GetCenter();
	center->GetRadio().Transmit
	(
		new RadioMessageSupportConfirm(group),
		center->GetLanguage()
	);

	if (group->IsPlayerGroup()) return;

	group->Move
	(
		group->MainSubgroup(),	// who
		pos,										// where
		Command::Undefined			// how
	);
}

static void ArcadeSupportTransport(AIGroupContext *context)
{
	STATE_PREFIX
	SupportConfirmed(group, mission->_destination);
}

static void CheckArcadeSupportTransport(AIGroupContext *context)
{
	STATE_PREFIX

	if (!group->GetSupportedGroup())
	{
		context->_fsm->SetState(SArcadeSupportWait, context);
		AllowGetIn(group, false);
		return;
	}
	else if (mission->_destination.Distance2(group->GetSupportPos()) > Square(20))
	{
		// support position changed
		mission->_destination = group->GetSupportPos();
		SupportConfirmed(group, mission->_destination);
		return;
	}
	CheckArcadeMove(context, SArcadeSupportSupply);
}

static void ArcadeSupportSupply(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);

	AllowGetIn(group, false);

	AICenter *center = group->GetCenter();
	center->GetRadio().Transmit
	(
		new RadioMessageSupportReady(group),
		center->GetLanguage()
	);
}

static void CheckArcadeSupportSupply(AIGroupContext *context)
{
	STATE_PREFIX

	if (!group->GetSupportedGroup())
	{
		context->_fsm->SetState(SArcadeSupportWait, context);
		return;
	}
	else if (mission->_destination.Distance2(group->GetSupportPos()) > Square(20))
	{
		// support position changed
		AllowGetIn(group);
		mission->_destination = group->GetSupportPos();
		context->_fsm->SetState(SArcadeSupportTransport, context);
		return;
	}
}

///////////////////////////////////////////////////////////////////////////////
// Logic subdiagram

static void ArcadeLogic(AIGroupContext *context)
{
	STATE_PREFIX
	Assert(group->GetCenter()->GetSide() == TLogic)

	int &index = context->_fsm->Var(0);
	ArcadeWaypointInfo &wInfo = group->GetWaypoint(index);
	group->Leader()->GetVehicle()->Move(wInfo.position);

	for (int j=0; j<wInfo.synchronizations.Size(); j++)
	{
		int sync = wInfo.synchronizations[j];
		Assert(sync >= 0);
		synchronized[sync].SetActive(group, false);
		GetNetworkManager().GroupSynchronization(group, sync, false);
	}

	int &waiting = context->_fsm->Var(4);
	waiting = TRUE;
}

static void CheckArcadeLogic(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);
	int &index = context->_fsm->Var(0);
	ArcadeWaypointInfo &wInfo = group->GetWaypoint(index);

	int &type = context->_fsm->Var(1);
	if (!IsSyncActive(group, wInfo, type))
	{
		int &waiting = context->_fsm->Var(4);
		waiting = FALSE;

		context->_fsm->SetState(SArcadeCountdown, context);
	}
}

///////////////////////////////////////////////////////////////////////////////
// Flee subdiagram

Vector3 FindFleePoint( AIGroup *group, bool &forced )
{
	forced=false;

	Assert(!group->IsPlayerGroup());
	Assert(group->NWaypoints() > 0); // waypoint 0 is initial position
	Assert(group->Leader());
	AICenter *center = group->GetCenter();
	Assert(center);

	Vector3Val posL = group->Leader()->Position();
	float minCost = FLT_MAX;
	const AITargetInfo *bestInfo = NULL;
	const float costPerDist = 0.1f;

	for (int i=0; i<center->NTargets(); i++)
	{
		const AITargetInfo &info = center->GetTarget(i);
		if (!info._idExact) continue;
		if (!(info._side == TCivilian) && !(center->IsFriendly(info._side)))
			continue;

		VehicleSupply *veh = dyn_cast<Transport, Object>(info._idExact);
		if (!veh) continue;

		// check if it is my group vehicle
		AIGroup *infoGroup = info._idExact->GetGroup();
		if (infoGroup==group) continue;
		// not in my group - may be flee target
		float coef = 1;
		if (info._type->IsAttendant())
		{
			coef = 1/2.5f;
		}
		else if (info._type->GetMaxRepairCargo() > 0)
		{
			if (veh->GetRepairCargo() <= 0) coef = 1/0.1f;
			else coef = 1/2.0f;
		}
		else if (info._type->GetMaxAmmoCargo() > 0)
		{
			if (veh->GetAmmoCargo() <= 0) coef = 1/0.1f;
			else coef = 1/1.1f;
		}
		else if (info._type->GetMagazineCargo().Size() > 0)
		{
			// assume some ammo is there
			coef = 1/1.1f;
		}
		else if (info._type->GetMaxFuelCargo() > 0)
		{
			if (veh->GetFuelCargo() <= 0) coef = 1/0.1f;
			else coef = 1/0.6f;
		}
		else
		{
			continue;
		}

		float cost = center->GetExposurePessimistic(info._realPos);

		float dist = (posL - info._realPos).SizeXZ();

		cost += dist*costPerDist;
		cost *= coef;
		if (cost < minCost)
		{
			minCost = cost;
			bestInfo = &info;
		}
	}
	// check first waypoint cost
	float coef = 1/0.1f;

	const ArcadeWaypointInfo &wInfo = group->GetWaypoint(0);
	float cost = center->GetExposurePessimistic(wInfo.position);
	float dist = (posL - wInfo.position).SizeXZ();
	cost += dist*costPerDist;

	cost *= coef;

	if (minCost<cost)
	{
		forced = true;
		return bestInfo->_realPos;
	}
	else
	{
		return wInfo.position;
	}
}

static void ArcadeFlee(AIGroupContext *context)
{
	STATE_PREFIX
	
	AIUnit *leader=group->Leader();
	if( !leader ) return;

	bool forced;
	mission->_destination = FindFleePoint(context->_group,forced);
	
	for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
	{
		AIUnit *unit = group->UnitWithID(i + 1);
		if (!unit) continue;
		if (!unit->IsFreeSoldier())
		{
			unit->SetSemaphore(AI::SemaphoreYellow);
		}
		else
		{
			unit->SetSemaphore(AI::SemaphoreGreen);
		}
	}

	Vector3 normal=VUp;
	leader->FindFreePosition(mission->_destination,normal);	

	group->SetSemaphore(AI::SemaphoreGreen);
	group->MainSubgroup()->SetSpeedMode(SpeedFull);

	group->Move
	(
		group->MainSubgroup(),	// who
		mission->_destination,	// where
		Command::Undefined			// how
	);
}

static void CheckArcadeFlee(AIGroupContext *context)
{
	AIGroup *group = context->_group;
	Assert(group);
	if (group->IsPlayerGroup()) return;

	if (group->GetAllDone())
	{
		for (int i=0; i<MAX_UNITS_PER_GROUP; i++)
		{
			AIUnit *unit = group->UnitWithID(i + 1);
			if (unit) unit->SetSemaphore(AI::SemaphoreYellow);
		}
		group->SetSemaphore(AI::SemaphoreYellow);
		group->MainSubgroup()->SetSpeedMode(SpeedNormal);
		group->CalculateMaximalStrength();
	}
}

///////////////////////////////////////////////////////////////////////////////
// Registration of FSMs

static AIGroupFSM::StateInfo noMissionStates[] =
{
	AIGroupFSM::StateInfo("Wait", NoMissionWait, CheckNoMissionWait)
};

static AIGroupFSM::StateInfo arcadeStates[] =
{
	AIGroupFSM::StateInfo("Init", ArcadeInit, CheckArcadeInit),
	AIGroupFSM::StateInfo("Turn", ArcadeTurn, CheckArcadeTurn),
	AIGroupFSM::StateInfo("Move Move", ArcadeMoveMove, CheckArcadeMoveMove),
	AIGroupFSM::StateInfo("Talk Move", ArcadeTalkMove, CheckArcadeTalkMove),
	AIGroupFSM::StateInfo("Talk GetOut", ArcadeTalkGetOut, CheckArcadeTalkGetOut),
	AIGroupFSM::StateInfo("Talk Walk", ArcadeTalkWalk, CheckArcadeTalkWalk),
	AIGroupFSM::StateInfo("Destroy Move", ArcadeDestroyMove, CheckArcadeDestroyMove),
	AIGroupFSM::StateInfo("Destroy Brown", ArcadeDestroyBrown, CheckArcadeDestroyBrown),
	AIGroupFSM::StateInfo("Destroy Attack", ArcadeDestroyAttack, CheckArcadeDestroyAttack),
	AIGroupFSM::StateInfo("GetIn Move", ArcadeMove, CheckArcadeGetInMove),
	AIGroupFSM::StateInfo("GetIn Sync", ArcadeGetInSync, CheckArcadeGetInSync),
	AIGroupFSM::StateInfo("GetIn GetIn", ArcadeGetInGetIn, CheckArcadeGetInGetIn),
	AIGroupFSM::StateInfo("SAD Move", ArcadeMove, CheckArcadeSeekAndDestroyMove),
	AIGroupFSM::StateInfo("SAD Check", ArcadeSeekAndDestroyCheck, CheckArcadeSeekAndDestroyCheck),
	AIGroupFSM::StateInfo("SAD Wait", ArcadeWait, CheckArcadeSeekAndDestroyWait),
	AIGroupFSM::StateInfo("SAD Overlook", ArcadeSeekAndDestroyOverlook, CheckArcadeSeekAndDestroyOverlook),
	AIGroupFSM::StateInfo("SAD Brown", ArcadeSeekAndDestroyBrown, CheckArcadeSeekAndDestroyBrown),
	AIGroupFSM::StateInfo("Join Move", ArcadeMove, CheckArcadeJoinMove),
	AIGroupFSM::StateInfo("Join Sync", ArcadeSync, CheckArcadeJoinSync),
	AIGroupFSM::StateInfo("Join Join", ArcadeJoinJoin, CheckArcadeJoinJoin),
	AIGroupFSM::StateInfo("Leader Move", ArcadeMove, CheckArcadeLeaderMove),
	AIGroupFSM::StateInfo("Leader Sync", ArcadeSync, CheckArcadeLeaderSync),
	AIGroupFSM::StateInfo("Leader Join", ArcadeLeaderJoin, CheckArcadeLeaderJoin),
	AIGroupFSM::StateInfo("GetOut Move", ArcadeMove, CheckArcadeGetOutMove),
	AIGroupFSM::StateInfo("GetOut GetOut", ArcadeGetOutGetOut, CheckArcadeGetOutGetOut),
	AIGroupFSM::StateInfo("Load Move", ArcadeMove, CheckArcadeLoadMove),
	AIGroupFSM::StateInfo("Load GetIn", ArcadeLoadGetIn, CheckArcadeLoadGetIn),
	AIGroupFSM::StateInfo("Unload Move", ArcadeMove, CheckArcadeUnloadMove),
	AIGroupFSM::StateInfo("Unload GetOut", ArcadeUnloadGetOut, CheckArcadeUnloadGetOut),
	AIGroupFSM::StateInfo("TransportUnload Move", ArcadeMove, CheckArcadeTransportUnloadMove),
	AIGroupFSM::StateInfo("TransportUnload GetOut", ArcadeTransportUnloadGetOut, CheckArcadeTransportUnloadGetOut),
	AIGroupFSM::StateInfo("Hold Move", ArcadeMove, CheckArcadeHoldMove),
	AIGroupFSM::StateInfo("Hold Wait", ArcadeWait, CheckArcadeHoldWait),
	AIGroupFSM::StateInfo("Hold Overlook", ArcadeHoldOverlook, CheckArcadeHoldOverlook),
	AIGroupFSM::StateInfo("Sentry Move", ArcadeMove, CheckArcadeSentryMove),
	AIGroupFSM::StateInfo("Sentry Wait", ArcadeWait, CheckArcadeSentryWait),
	AIGroupFSM::StateInfo("Sentry Overlook", ArcadeSentryOverlook, CheckArcadeSentryOverlook),
	AIGroupFSM::StateInfo("Sentry Brown", ArcadeSentryBrown, CheckArcadeSentryBrown),
	AIGroupFSM::StateInfo("Guard Move", ArcadeMove, CheckArcadeGuardMove),
	AIGroupFSM::StateInfo("Guard Wait", ArcadeWait, CheckArcadeGuardWait),
	AIGroupFSM::StateInfo("Guard Attack", ArcadeGuardAttack, CheckArcadeGuardAttack),
	AIGroupFSM::StateInfo("Guard Overlook", ArcadeGuardOverlook, CheckArcadeGuardOverlook),
	AIGroupFSM::StateInfo("Guard Brown", ArcadeGuardBrown, CheckArcadeGuardBrown),
	AIGroupFSM::StateInfo("Guard BrownTarget", ArcadeGuardBrownTarget, CheckArcadeGuardBrownTarget),
	AIGroupFSM::StateInfo("Gravon Wait", ArcadeWait, CheckArcadeGravonWait),
	AIGroupFSM::StateInfo("Gravon Attack", ArcadeGravonAttack, CheckArcadeGravonAttack),
	AIGroupFSM::StateInfo("Gravon Overlook", ArcadeGravonOverlook, CheckArcadeGravonOverlook),
	AIGroupFSM::StateInfo("Gravon Move", ArcadeGravonMove, CheckArcadeGravonMove),
	AIGroupFSM::StateInfo("Support Move", ArcadeMove, CheckArcadeSupportMove),
	AIGroupFSM::StateInfo("Support Wait", ArcadeWait, CheckArcadeSupportWait),
	AIGroupFSM::StateInfo("Support Transport", ArcadeSupportTransport, CheckArcadeSupportTransport),
	AIGroupFSM::StateInfo("Support Supply", ArcadeSupportSupply, CheckArcadeSupportSupply),
	AIGroupFSM::StateInfo("Scripted", ArcadeScripted, CheckArcadeScripted),
	AIGroupFSM::StateInfo("Logic", ArcadeLogic, CheckArcadeLogic),
	AIGroupFSM::StateInfo("Sync", ArcadeSync, CheckArcadeSync),
	AIGroupFSM::StateInfo("Countdown", ArcadeCountdown, CheckArcadeCountdown),
	AIGroupFSM::StateInfo("Next", ArcadeNext, CheckArcadeNext),
	AIGroupFSM::StateInfo("Unlock", ArcadeUnlock, CheckArcadeUnlock),

	AIGroupFSM::StateInfo("Flee", ArcadeFlee, CheckArcadeFlee),
	
	AIGroupFSM::StateInfo("Succeed", MissionSucceed, CheckMissionSucceed),
	AIGroupFSM::StateInfo("Failed", MissionFailed, CheckMissionFailed)
};

FSM *AbstractAIMachine<Mission, AIGroupContext>::CreateFSM(int taskType)
{
	//int itemSize = sizeof(AIGroupFSM::StateInfo);
	switch (taskType)
	{
		case Mission::NoMission:
			return new AIGroupFSM
			(
				noMissionStates,
				sizeof(noMissionStates) / sizeof(*noMissionStates)
			);
		case Mission::Arcade:
			return new AIGroupFSM
			(
				arcadeStates,
				sizeof(arcadeStates) / sizeof(*arcadeStates)
			);
	}
	Fail("Unknown mission type");
	return NULL;
}
