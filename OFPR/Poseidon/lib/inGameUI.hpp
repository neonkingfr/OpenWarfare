#ifdef _MSC_VER
#pragma once
#endif

#ifndef _IN_GAME_UI_HPP
#define _IN_GAME_UI_HPP

#include "types.hpp"
#include <El/Math/math3d.hpp>
#include "aiTypes.hpp"

class AbstractUI
{
protected:
	bool _showUnitInfo;
	bool _showTacticalDisplay;
	bool _showCompass;
	bool _showTankDirection;
	bool _showMenu;
	bool _showGroupInfo;

	bool _showCursors;

public:
	// draw overlay - called before FinishDraw
	virtual void DrawHUD
	(
		const Camera &camera, EntityAI *vehicle, CameraType cam
	)=NULL;

	// simulate controls - called during simulation
	virtual void SimulateHUD
	(
		const Camera &camera, EntityAI *vehicle, CameraType cam, float deltaT
	)=NULL;

	virtual void DrawHUDNonAI
	(
		const Camera &camera, Entity *vehicle, CameraType cam
	)=NULL;

	// simulate controls - called during simulation
	virtual void SimulateHUDNonAI
	(
		const Camera &camera, Entity *vehicle, CameraType cam, float deltaT
	)=NULL;

	// called in BrowseCamera - when HUD subject changes
	virtual void Init() = NULL;
	virtual void ResetHUD() = NULL;
	virtual void ResetVehicle( EntityAI *vehicle ) = NULL; // when vehicle is changed
	virtual void OnWeaponRemoved(int slot) = NULL;

	virtual const AutoArray<RString> &GetCustomRadio() const = NULL;

	void ShowUnitInfo(bool show = true) {_showUnitInfo = show;}
	void ShowTacticalDisplay(bool show = true) {_showTacticalDisplay = show;}
	void ShowCompass(bool show = true) {_showCompass = show;}
	void ShowMenu(bool show = true) {_showMenu = show;}
	void ShowTankDirection(bool show = true) {_showTankDirection = show;}
	void ShowGroupInfo(bool show = true) {_showGroupInfo = show;}
	void ShowAll(bool show = true);

	void ShowCursors(bool show = true) {_showCursors = show;}

	virtual void ShowMe() = NULL;
	virtual void ShowTarget() = NULL;
	virtual void ShowFormPosition() = NULL;
	virtual void ShowGroupDir() = NULL;

	virtual void ShowHint(RString hint) = NULL;

	// used to handle mouse movements
	virtual Vector3 GetCursorDirection() const = NULL;
	virtual void SetCursorDirection( Vector3Par dir ) = NULL;

	virtual void SetCursorMode( bool world ) = NULL; // mouse used - switch to world mode
	virtual bool GetCursorMode() const = NULL; // mouse used - switch to world mode

	virtual Vector3 GetWorldCursor() const = NULL;
	virtual void SetWorldCursor( Vector3Par dir ) = NULL;
	virtual Vector3 GetModelCursor() const = NULL;
	virtual void SetModelCursor( Vector3Par dir ) = NULL;
	
	virtual float GetCursorAge() const = NULL;
	
	virtual void SwitchToStrategy( EntityAI *vehicle ) = NULL;
	virtual void SwitchToFire( EntityAI *vehicle ) = NULL;
	
	virtual ~AbstractUI(){}
};

AbstractUI *CreateInGameUI();
AbstractUI *CreateMenuUI();

#endif
