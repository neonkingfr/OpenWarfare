//

#include "wpch.hpp"

#if !defined _XBOX

#include "serial.hpp"
#include <El/QStream/QStream.hpp>
#include <RSA/rsa.h>
#include <ctype.h>

static const char *UsableChars = "0123456789ABCDEFGHJKLMNPRSTVWXYZ";

// decode 120 bit number into / from CD KEY

__forceinline void EraseMemory(unsigned char *buffer, int size)
{
  volatile unsigned char *erase = buffer;
  for(int i=0; i<size; i++)
  {
    erase = 0;
  }
}


static bool DecodeMsg(unsigned char *msg, const char *buffer)
{
	for (int i=0; i<3; i++)
	{
		unsigned __int64 value = 0;
		int offset = 0;
		for (int k=0; k<8; k++)
		{
			int c = *buffer++;
			while (c && !isalnum((int)c)) c = *buffer++;
			if (!c) return false;
			c = toupper(c);
			// FIX: there are no letters O and I in serial number
			// if users enters it, it must be misread number
			if (c=='O') c='0';
			if (c=='I') c='1';
			const char *pos = strchr(UsableChars, c);
			if (!pos) return false;
			c = pos - UsableChars;
			unsigned __int64 cc = c;
			value |= cc << offset;
			offset += 5;
		}
		offset = 4 * 8;
		for (int j=0; j<5; j++)
		{
			*msg++ = (byte)((value >> offset) & 0xff);
			offset -= 8;
		}
	}
	return true;
}

// create / output MPI number from / into buffer

MPI CreateMPI(const unsigned char *buffer, DWORD size)
{
	MPI val = MPI_NULL;
	mpi_limb_t a;

	int nbytes = size;
	int nlimbs = (size + BYTES_PER_MPI_LIMB - 1) / BYTES_PER_MPI_LIMB;
	val = mpi_alloc(nlimbs);

	buffer += size - 1;

	int i = BYTES_PER_MPI_LIMB - nbytes % BYTES_PER_MPI_LIMB;
	i %= BYTES_PER_MPI_LIMB;
	val->nbits = nbytes * 8;
	int j = val->nlimbs = nlimbs;
	val->sign = 0;
	for( ; j > 0; j-- )
	{
		a = 0;
		for(; i < BYTES_PER_MPI_LIMB; i++)
		{
			a <<= 8;
			a |= *buffer--;
		}
		i = 0;
		val->d[j-1] = a;
	}
	return val;
}

__forceinline void EraseMPI(MPI &val, DWORD size)
{
	int nlimbs = (size + BYTES_PER_MPI_LIMB - 1) / BYTES_PER_MPI_LIMB;

	int j = nlimbs;
	for( ; j > 0; j-- )
	{
		val->d[j-1] = 0;
	}
}

void OutputMPI(MPI &val, unsigned char *buffer, DWORD size)
{
	mpi_limb_t a;

	int nbytes = size;
	int nlimbs = (size + BYTES_PER_MPI_LIMB - 1) / BYTES_PER_MPI_LIMB;

	buffer += size - 1;

	int i = BYTES_PER_MPI_LIMB - nbytes % BYTES_PER_MPI_LIMB;
	i %= BYTES_PER_MPI_LIMB;
	int j = nlimbs;
	for( ; j > 0; j-- )
	{
		a = val->d[j-1];
		int offset = (BYTES_PER_MPI_LIMB - i - 1) * 8;
		for(; i < BYTES_PER_MPI_LIMB; i++)
		{
			*buffer-- = (a >> offset) & 0xff;
			offset -= 8;
		}
		i = 0;
	}
}

void Encrypt(QOStream &out,const unsigned char *publicKey, int keySize)
{
	int msgSize = out.pcount();
	if (msgSize == 0) return;

	int nChunks = (msgSize + keySize - 1) / keySize;
	int newSize = nChunks * keySize;
	while (out.pcount() < newSize) out.put(' ');

	RSA_public_key key;
	// public exponent
	key.e = CreateMPI(publicKey, 4);
	// modulus
	key.n = CreateMPI(publicKey + 4, keySize);

	unsigned char *ptr = (unsigned char *)out.str();
	for (int i=0; i<nChunks; i++)
	{
		// prepare input
		MPI input = CreateMPI(ptr, keySize);

		// prepare output
		MPI output = mpi_alloc(mpi_get_nlimbs(key.n));

		// encode
		publ(output, input, &key);
		OutputMPI(output, ptr, keySize);

		m_free(input);
		m_free(output);

		ptr += keySize;
	}

	m_free(key.e);
	m_free(key.n);
}


CDKey::CDKey()
{
  #if DECODE_ON_GET
	memset(_key, 0, KEY_BYTES);
	memset(_message, 0, KEY_BYTES);
  #else
	memset(_buffer, 0, KEY_BYTES);
  #endif
}

bool CDKey::DecodeMsg(unsigned char *msg, const char *buffer)
{
	return ::DecodeMsg(msg, buffer);
}

void CDKey::Decrypt(
  unsigned char *buffer,
  const unsigned char *cdKey, const unsigned char *publicKey
)
{
	RSA_public_key key;
	// public exponent
	key.e = CreateMPI(publicKey, 4);
	// modulus
	key.n = CreateMPI(publicKey + 4, KEY_BYTES);

	// prepare input
	MPI input = CreateMPI(cdKey, KEY_BYTES);

	// prepare output
	MPI output = mpi_alloc(mpi_get_nlimbs(key.n));

	// decode
	publ(output, input, &key);
	OutputMPI(output, buffer, KEY_BYTES);
  // secure all data
  EraseMPI(output, KEY_BYTES);
  EraseMPI(input, KEY_BYTES);
  EraseMPI(key.e, 4);
  EraseMPI(key.n, KEY_BYTES);

	m_free(input);
	m_free(output);

	m_free(key.e);
	m_free(key.n);
}

void CDKey::Init(const unsigned char *cdKey, const unsigned char *publicKey)
{
  #if DECODE_ON_GET
  memcpy(_key,publicKey,sizeof(_key));
  memcpy(_message,cdKey,sizeof(_message));
  #else
  Decrypt(_buffer,cdKey,publicKey);
  #endif
}

bool CDKey::Check(int offset, const char *with)
{
  #if DECODE_ON_GET
  unsigned char buffer[KEY_BYTES];
  Decrypt(buffer,_message,_key);
  #else
  const unsigned char *buffer = _buffer;
  #endif
	bool ok = (memcmp(buffer + offset, with, strlen(with)) == 0);
  #if DECODE_ON_GET
  EraseMemory(buffer,KEY_BYTES);
  #endif
  return ok;
}

__int64 CDKey::GetValue(int offset, int size)
{
  #if DECODE_ON_GET
  unsigned char buffer[KEY_BYTES];
  Decrypt(buffer,_message,_key);
  #else
  const unsigned char *buffer = _buffer;
  #endif
	__int64 value = 0;
	const unsigned char *ptr = buffer + offset + size - 1;
	for (int i=0; i<size; i++)
	{
		value <<= 8;
		value |= *ptr--;
	}
  #if DECODE_ON_GET
  EraseMemory(buffer,KEY_BYTES);
  #endif
	return value;
}

#endif
