// global redefiniton of new, delete operators
#include "wpch.hpp"

#if 0
//#include "engineDll.hpp"

#include <class\normalNew.hpp>

#define CountNew(file,line,size)

void MemoryFootprint(){}

#if _MSC_VER
	// we want Memory Heap to deallocate last
	#pragma warning(disable:4073)
	#pragma init_seg(lib)
#endif


static HANDLE Heap;

void MemoryInit()
{
	//Memory=new MemHeap(size*4,MemAlign);
	Heap=HeapCreate
	(
		0,
		//HEAP_NO_SERIALIZE,
		32*1024*1024, // initial commited
		160*1024*1024 // initial reserved
	);
}

void MemoryDone()
{
	HeapDestroy(Heap);
}

int MemoryUsed()
{
	//HeapSize(Heap,0,
	return 0;
}

int MemoryFreeBlocks()
{
	return 0;
}

int MemoryAllocatedBlocks()
{
	return 0;
}

#if !DO_STATS
void ReportMemory()
{
}
#endif

bool MemoryCheck()
{
	return true;
}

void MemoryError( int size )
{
	ErrorMessage
	(
		"Out of memory (%d KB).\n",
		size/1024
	);
}


#if _MSC_VER
#define MEM_CONV __cdecl
#else
#define MEM_CONV
#endif

inline void *ActualAlloc( size_t size )
{
	return HeapAlloc(Heap,0,size);
}

inline void ActualFree( void *mem )
{
	HeapFree(Heap,0,mem);
}

void * MEM_CONV operator new ( size_t size )
{
	CountNew("nofile",0,size);
	#if CHECK
		size=PrepareAlloc(size);
	#endif
	void *ret=ActualAlloc(size);
	if( !ret && size ) MemoryError(size);
	#if CHECK
		ret=FinishAlloc(ret,size);
	#endif
	return ret;
}


void * __cdecl operator new( size_t size ,int addr, char const *file, int line )
{ // placement new
	return (void *)addr;
}

void * MEM_CONV operator new ( size_t size, const char *file, int line )
{
	CountNew(file,line,size);
	#if CHECK
		size=PrepareAlloc(size);
	#endif
	void *ret=ActualAlloc(size);
	if( !ret && size ) MemoryError(size);
	#if CHECK
		ret=FinishAlloc(ret,size,file,line);
	#endif
	return ret;
}

void MEM_CONV operator delete ( void *mem )
{
	if( !mem ) return;
	#if CHECK
		mem=PrepareFree(mem);
	#endif
	// determine which heap it is in
	ActualFree(mem);
}

#endif