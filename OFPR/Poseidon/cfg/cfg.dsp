# Microsoft Developer Studio Project File - Name="cfg" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 60000
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=cfg - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "cfg.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "cfg.mak" CFG="cfg - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "cfg - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "cfg - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/Poseidon/cfg", HBCAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "cfg - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\Config"
# PROP Intermediate_Dir "..\Config"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /G6 /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /EP /FD /P /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /pdb:none /machine:I386

!ELSEIF  "$(CFG)" == "cfg - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "d:\Poseidon Setup\Poseidon\Bin"
# PROP Intermediate_Dir "..\Config"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /EP /FD /P /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /incremental:no /machine:I386 /pdbtype:sept
# SUBTRACT LINK32 /debug

!ENDIF 

# Begin Target

# Name "cfg - Win32 Release"
# Name "cfg - Win32 Debug"
# Begin Source File

SOURCE=O:\StatusQuo\bin\abel.h
# End Source File
# Begin Source File

SOURCE=O:\StatusQuo\bin\cain.h
# End Source File
# Begin Source File

SOURCE=O:\StatusQuo\bin\cfgMaterials.hpp
# End Source File
# Begin Source File

SOURCE=O:\StatusQuo\bin\cfgModels.hpp
# End Source File
# Begin Source File

SOURCE=O:\StatusQuo\bin\cfgMoves.hpp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=O:\StatusQuo\bin\cfgVehicles.hpp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=O:\StatusQuo\bin\cfgWeapons.hpp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=O:\StatusQuo\bin\civiliannames.hpp
# End Source File
# Begin Source File

SOURCE=O:\StatusQuo\bin\config.cpp

!IF  "$(CFG)" == "cfg - Win32 Release"

# PROP Exclude_From_Build 1
# PROP Ignore_Default_Tool 1

!ELSEIF  "$(CFG)" == "cfg - Win32 Debug"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=O:\StatusQuo\bin\czechnames.hpp

!IF  "$(CFG)" == "cfg - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "cfg - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\lib\dikCodes.h

!IF  "$(CFG)" == "cfg - Win32 Release"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Copying $(InputPath) to  o:\StatusQuo\bin\$(InputName).h
InputPath=..\lib\dikCodes.h
InputName=dikCodes

"o:\StatusQuo\bin\$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Copy $(InputPath)  o:\StatusQuo\bin\$(InputName).h

# End Custom Build

!ELSEIF  "$(CFG)" == "cfg - Win32 Debug"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Copying $(InputPath) to  o:\StatusQuo\bin\$(InputName).h
InputPath=..\lib\dikCodes.h
InputName=dikCodes

"o:\StatusQuo\bin\$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Copy $(InputPath)  o:\StatusQuo\bin\$(InputName).h

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=O:\StatusQuo\bin\eden.h
# End Source File
# Begin Source File

SOURCE=O:\StatusQuo\bin\englishnames.hpp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\lib\languages.hpp

!IF  "$(CFG)" == "cfg - Win32 Release"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Copying $(InputPath) to  o:\StatusQuo\bin\$(InputName).hpp
InputPath=..\lib\languages.hpp
InputName=languages

"o:\StatusQuo\bin\$(InputName).hpp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Copy $(InputPath) o:\StatusQuo\bin\$(InputName).hpp

# End Custom Build

!ELSEIF  "$(CFG)" == "cfg - Win32 Debug"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Copying $(InputPath) to  o:\StatusQuo\bin\$(InputName).hpp
InputPath=..\lib\languages.hpp
InputName=languages

"o:\StatusQuo\bin\$(InputName).hpp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Copy $(InputPath) o:\StatusQuo\bin\$(InputName).hpp

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\manActions.hpp

!IF  "$(CFG)" == "cfg - Win32 Release"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Copying $(InputPath) to  o:\StatusQuo\bin\$(InputName).hpp
InputPath=.\manActions.hpp
InputName=manActions

"o:\StatusQuo\bin\$(InputName).hpp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Copy $(InputPath) o:\StatusQuo\bin\$(InputName).hpp

# End Custom Build

!ELSEIF  "$(CFG)" == "cfg - Win32 Debug"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Copying $(InputPath) to  o:\StatusQuo\bin\$(InputName).hpp
InputPath=.\manActions.hpp
InputName=manActions

"o:\StatusQuo\bin\$(InputName).hpp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Copy $(InputPath) o:\StatusQuo\bin\$(InputName).hpp

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=O:\StatusQuo\bin\minimal.h
# End Source File
# Begin Source File

SOURCE=.\resincl.hpp

!IF  "$(CFG)" == "cfg - Win32 Release"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Copying $(InputPath) to  o:\StatusQuo\bin\$(InputName).hpp
InputPath=.\resincl.hpp
InputName=resincl

"o:\StatusQuo\bin\$(InputName).hpp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Copy $(InputPath) o:\StatusQuo\bin\$(InputName).hpp

# End Custom Build

!ELSEIF  "$(CFG)" == "cfg - Win32 Debug"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Copying $(InputPath) to  o:\StatusQuo\bin\$(InputName).hpp
InputPath=.\resincl.hpp
InputName=resincl

"o:\StatusQuo\bin\$(InputName).hpp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Copy $(InputPath) o:\StatusQuo\bin\$(InputName).hpp

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=O:\StatusQuo\bin\resource.cpp

!IF  "$(CFG)" == "cfg - Win32 Release"

# PROP Exclude_From_Build 1
# PROP Ignore_Default_Tool 1

!ELSEIF  "$(CFG)" == "cfg - Win32 Debug"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=O:\StatusQuo\bin\rscDialog.hpp

!IF  "$(CFG)" == "cfg - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "cfg - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=O:\StatusQuo\bin\rscIGUI.hpp

!IF  "$(CFG)" == "cfg - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "cfg - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=O:\StatusQuo\bin\rscMenu.hpp

!IF  "$(CFG)" == "cfg - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "cfg - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=O:\StatusQuo\bin\rscScreen.hpp

!IF  "$(CFG)" == "cfg - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "cfg - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=O:\StatusQuo\bin\russiannames.hpp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=O:\StatusQuo\bin\sp_sentences.hpp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=O:\StatusQuo\bin\stringtable.csv
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=O:\StatusQuo\bin\wounds.hpp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=O:\StatusQuo\bin\woundsFace.hpp
# End Source File
# End Target
# End Project
