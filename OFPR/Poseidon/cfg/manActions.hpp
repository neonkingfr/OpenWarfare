// basic actions list
ACTION(Stop)
ACTION(StopRelaxed)

ACTION(TurnL) // turning
ACTION(TurnR)

ACTION(TurnLRelaxed) // turning
ACTION(TurnRRelaxed)

ACTION(ReloadMagazine) // special actions
ACTION(ReloadMGun)
ACTION(ReloadAT)
ACTION(ReloadMortar)
ACTION(ThrowGrenade)

/*!
\internal 1.05 Date 7/17/2001 by Ondra.
- Changed: different logic used to control walking.
It was controlled by mode before, slow actions were added instead.
*/

// ADD BEGIN
ACTION(WalkF)
ACTION(WalkLF)
ACTION(WalkRF)
ACTION(WalkL)
ACTION(WalkR)
ACTION(WalkLB)
ACTION(WalkRB)
ACTION(WalkB)
// ADD END

ACTION(SlowF)
ACTION(SlowLF)
ACTION(SlowRF)
ACTION(SlowL)
ACTION(SlowR)
ACTION(SlowLB)
ACTION(SlowRB)
ACTION(SlowB)

ACTION(FastF)
ACTION(FastLF)
ACTION(FastRF)
ACTION(FastL)
ACTION(FastR)
ACTION(FastLB)
ACTION(FastRB)
ACTION(FastB)

ACTION(Down) // incremental change
ACTION(Up)

ACTION(Lying) // direct change
ACTION(Stand)
ACTION(Combat)
ACTION(Crouch)
//ACTION(OpticsOn)
//ACTION(OpticsOff)
ACTION(Civil)
ACTION(CivilLying)
ACTION(FireNotPossible) // attempt to fire when weapons are disabled


ACTION(Die)

ACTION(WeaponOn) // activate AT weapon
ACTION(WeaponOff) // deactivate AT weapon

ACTION(Default) // no action - used for initialization
ACTION(JumpOff) // no action - used after get out

// special actions - for scripting and effects

ACTION(StrokeFist)
ACTION(StrokeGun)

ACTION(SitDown)
ACTION(Salute)

ACTION(BinocOn)
ACTION(BinocOff)

ACTION(PutDown) // used during some actions
//ACTION(PutDownEnd) // used during some actions

ACTION(Medic)
ACTION(Treated)

ACTION(LadderOnDown)
ACTION(LadderOnUp)
ACTION(LadderOff)
ACTION(LadderOffTop)
ACTION(LadderOffBottom)

// get-in/out

ACTION(GetInCar)
ACTION(GetOutCar)
ACTION(GetInTank)
ACTION(GetOutTank)

/*!
\patch_internal 1.21 Date 8/22/2001 by Ondra.
- New: TakeFlag action for better TakeFlag synchronization.
*/

ACTION(TakeFlag) // used during some actions

ACTION(HandGunOn)
