# Microsoft Developer Studio Project File - Name="vorbisfile" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 60000
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=vorbisfile - Win32 Debug MT
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "vorbisfile.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "vorbisfile.mak" CFG="vorbisfile - Win32 Debug MT"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "vorbisfile - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "vorbisfile - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "vorbisfile - Win32 Release MT" (based on "Win32 (x86) Static Library")
!MESSAGE "vorbisfile - Win32 Debug MT" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/ogg/libvorbis/vorbisfile", QBEAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "vorbisfile - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /G6 /W2 /GR /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "vorbisfile - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /GR /Z7 /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"Debug\vorbisfiled.lib"

!ELSEIF  "$(CFG)" == "vorbisfile - Win32 Release MT"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "vorbisfile___Win32_Release_MT"
# PROP BASE Intermediate_Dir "vorbisfile___Win32_Release_MT"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release_MT"
# PROP Intermediate_Dir "Release_MT"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /G6 /W2 /GR /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /G6 /MT /W2 /GR /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"Release_MT\vorbisfileMT.lib"

!ELSEIF  "$(CFG)" == "vorbisfile - Win32 Debug MT"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "vorbisfile___Win32_Debug_MT"
# PROP BASE Intermediate_Dir "vorbisfile___Win32_Debug_MT"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug_MT"
# PROP Intermediate_Dir "Debug_MT"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GR /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /GR /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"Debug_MT\vorbisfilemtd.lib"

!ENDIF 

# Begin Target

# Name "vorbisfile - Win32 Release"
# Name "vorbisfile - Win32 Debug"
# Name "vorbisfile - Win32 Release MT"
# Name "vorbisfile - Win32 Debug MT"
# Begin Group "Include"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\..\extern\vorbis\codec.h
# End Source File
# Begin Source File

SOURCE=..\..\..\extern\ogg\ogg.h
# End Source File
# Begin Source File

SOURCE=..\..\..\extern\ogg\os_types.h
# End Source File
# Begin Source File

SOURCE=..\..\..\extern\vorbis\vorbisenc.h
# End Source File
# Begin Source File

SOURCE=..\..\..\extern\vorbis\vorbisfile.h
# End Source File
# End Group
# Begin Group "Custom"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\customize.txt
# End Source File
# Begin Source File

SOURCE=.\libsToExtern.bat
# End Source File
# End Group
# Begin Group "Books"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\lib\books\floor\floor_books.h
# End Source File
# Begin Source File

SOURCE=..\lib\books\coupled\res_books_stereo.h
# End Source File
# Begin Source File

SOURCE=..\lib\books\uncoupled\res_books_uncoupled.h
# End Source File
# End Group
# Begin Group "Modes"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\lib\modes\floor_all.h
# End Source File
# Begin Source File

SOURCE=..\lib\modes\psych_11.h
# End Source File
# Begin Source File

SOURCE=..\lib\modes\psych_16.h
# End Source File
# Begin Source File

SOURCE=..\lib\modes\psych_44.h
# End Source File
# Begin Source File

SOURCE=..\lib\modes\psych_8.h
# End Source File
# Begin Source File

SOURCE=..\lib\modes\residue_16.h
# End Source File
# Begin Source File

SOURCE=..\lib\modes\residue_44.h
# End Source File
# Begin Source File

SOURCE=..\lib\modes\residue_44u.h
# End Source File
# Begin Source File

SOURCE=..\lib\modes\residue_8.h
# End Source File
# Begin Source File

SOURCE=..\lib\modes\setup_11.h
# End Source File
# Begin Source File

SOURCE=..\lib\modes\setup_16.h
# End Source File
# Begin Source File

SOURCE=..\lib\modes\setup_22.h
# End Source File
# Begin Source File

SOURCE=..\lib\modes\setup_32.h
# End Source File
# Begin Source File

SOURCE=..\lib\modes\setup_44.h
# End Source File
# Begin Source File

SOURCE=..\lib\modes\setup_44u.h
# End Source File
# Begin Source File

SOURCE=..\lib\modes\setup_8.h
# End Source File
# Begin Source File

SOURCE=..\lib\modes\setup_X.h
# End Source File
# End Group
# Begin Source File

SOURCE=..\lib\analysis.c
# End Source File
# Begin Source File

SOURCE=..\lib\backends.h
# End Source File
# Begin Source File

SOURCE=..\lib\barkmel.c
# End Source File
# Begin Source File

SOURCE=..\lib\bitrate.c
# End Source File
# Begin Source File

SOURCE=..\lib\bitrate.h
# End Source File
# Begin Source File

SOURCE=..\lib\block.c
# End Source File
# Begin Source File

SOURCE=..\lib\codebook.c
# End Source File
# Begin Source File

SOURCE=..\lib\codebook.h
# End Source File
# Begin Source File

SOURCE=..\lib\codec_internal.h
# End Source File
# Begin Source File

SOURCE=..\lib\envelope.c
# End Source File
# Begin Source File

SOURCE=..\lib\envelope.h
# End Source File
# Begin Source File

SOURCE=..\lib\floor0.c
# End Source File
# Begin Source File

SOURCE=..\lib\floor1.c
# End Source File
# Begin Source File

SOURCE=..\lib\highlevel.h
# End Source File
# Begin Source File

SOURCE=..\lib\info.c
# End Source File
# Begin Source File

SOURCE=..\lib\lookup.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\lib\lookup.h
# End Source File
# Begin Source File

SOURCE=..\lib\lookup_data.h
# End Source File
# Begin Source File

SOURCE=..\lib\lpc.c
# End Source File
# Begin Source File

SOURCE=..\lib\lpc.h
# End Source File
# Begin Source File

SOURCE=..\lib\lsp.c
# End Source File
# Begin Source File

SOURCE=..\lib\lsp.h
# End Source File
# Begin Source File

SOURCE=..\lib\mapping0.c
# End Source File
# Begin Source File

SOURCE=..\lib\masking.h
# End Source File
# Begin Source File

SOURCE=..\lib\mdct.c
# End Source File
# Begin Source File

SOURCE=..\lib\mdct.h
# End Source File
# Begin Source File

SOURCE=..\lib\misc.h
# End Source File
# Begin Source File

SOURCE=..\lib\os.h
# End Source File
# Begin Source File

SOURCE=..\lib\psy.c
# End Source File
# Begin Source File

SOURCE=..\lib\psy.h
# End Source File
# Begin Source File

SOURCE=..\lib\registry.c
# End Source File
# Begin Source File

SOURCE=..\lib\registry.h
# End Source File
# Begin Source File

SOURCE=..\lib\res0.c
# End Source File
# Begin Source File

SOURCE=..\lib\scales.h
# End Source File
# Begin Source File

SOURCE=..\lib\sharedbook.c
# End Source File
# Begin Source File

SOURCE=..\lib\smallft.c
# End Source File
# Begin Source File

SOURCE=..\lib\smallft.h
# End Source File
# Begin Source File

SOURCE=..\lib\synthesis.c
# End Source File
# Begin Source File

SOURCE=..\lib\vorbisenc.c
# End Source File
# Begin Source File

SOURCE=..\lib\vorbisfile.c
# End Source File
# Begin Source File

SOURCE=..\lib\window.c
# End Source File
# Begin Source File

SOURCE=..\lib\window.h
# End Source File
# End Target
# End Project
