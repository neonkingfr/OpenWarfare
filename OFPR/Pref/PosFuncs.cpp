/*********************************************************************/
/*																	 */
/*	Poseidon Preferences										     */
/*									Copyright � Mentar, 1999         */
/*																	 */
/*********************************************************************/

// PosPref.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Pref.h"
#include "PrefObj.h"
#include "PrefDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// set window text

void MntSetWindowText(HWND hWnd,const char* lpText)
{
	int nNewLen = lstrlen(lpText);
	int nOldLen = ::GetWindowTextLength(hWnd);
	char* szOld = new char [nOldLen+1];
	if (szOld)
	{
		// fast check to see if text really changes (reduces flash in controls)
		if (nNewLen == nOldLen)
		{
			::GetWindowText(hWnd, szOld, nOldLen+1);
			if (lstrcmp(szOld, lpText) == 0)
			{
				delete [] szOld;
				return;
			}
		}
		// change it
		::SetWindowText(hWnd, lpText);
		delete [] szOld;
	} else  
	{
		::SetWindowText(hWnd, lpText);
	}
};

void MntSprintfInt(CString& sText,const char* lpText,int nNum)
{
	sprintf(sText.GetBuffer(lstrlen(lpText)+20),lpText,nNum);
	sText.ReleaseBuffer();
};

void MntSprintfFloat(CString& sText,const char* lpText,float nNum)
{
	sprintf(sText.GetBuffer(lstrlen(lpText)+20),lpText,nNum);
	sText.ReleaseBuffer();
};
