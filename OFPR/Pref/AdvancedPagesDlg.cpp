/*********************************************************************/
/*																	 */
/*	Poseidon Preferences										     */
/*									Copyright � Mentar, 1999         */
/*																	 */
/*********************************************************************/

// PosPrefDlg.cpp : implementation file
//

#include "stdafx.h"
#include "math.h"
#include "Pref.h"
#include "PrefObj.h"
#include "PrefDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPropPerformancePage dialog

class CPropPerformancePage : public CPropertyPage
{
// Construction
public:
	CPropPerformancePage();

// Dialog Data
	CPosPrefObject*	m_pPreferences;

	//{{AFX_DATA(CPropPerformancePage)
	enum { IDD = IDD_PROP_PERFORMANCE };
	CSliderCtrl	m_cSliderTotal;
	CSliderCtrl	m_cSliderHeap;
	CSliderCtrl	m_cSliderFile;
	CSliderCtrl	m_cSliderGeometry;
	//}}AFX_DATA


// Overrides
			void UpdateOtherPages();
			void UpdateAll();
			void UpdateMemory();
			void UpdateGraphics();

			void OnChangeSliderTotal();
			void OnChangeSliderFile();
			void OnChangeSliderHeap();
			void OnChangeSliderGeometry();

	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPropPerformancePage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CPropPerformancePage)
	virtual BOOL OnInitDialog();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

CPropPerformancePage::CPropPerformancePage() : CPropertyPage(CPropPerformancePage::IDD)
{
	//{{AFX_DATA_INIT(CPropPerformancePage)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

BOOL CPropPerformancePage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

	m_cSliderTotal.SetRange(8,128);
	m_cSliderTotal.SetPageSize(4);
	m_cSliderTotal.SetSelection(8,0);
	m_cSliderTotal.SetTicFreq(8);

	m_cSliderHeap.SetRange(1,8);
	m_cSliderHeap.SetPageSize(1);
	m_cSliderHeap.SetSelection(1,0);
	m_cSliderHeap.SetTicFreq(1);

	m_cSliderFile.SetRange(1,8);
	m_cSliderFile.SetPageSize(1);
	m_cSliderFile.SetSelection(1,0);
	m_cSliderFile.SetTicFreq(1);

	//m_cSliderGeometry.SetRange(48,120);
	m_cSliderGeometry.SetRange(80,180);
	m_cSliderGeometry.SetPageSize(4);
	//m_cSliderGeometry.SetSelection(65,0);
	m_cSliderGeometry.SetSelection(80,0);
	m_cSliderGeometry.SetTicFreq(20);

	// update hodnot
	UpdateAll();

	((CPosPrefApp*) AfxGetApp ())->ApplyApplicationNameAndStringTable (this);
	return TRUE;
}

void CPropPerformancePage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPropPerformancePage)
	DDX_Control(pDX, IDC_SLIDER_TOTAL, m_cSliderTotal);
	DDX_Control(pDX, IDC_SLIDER_HEAP, m_cSliderHeap);
	DDX_Control(pDX, IDC_SLIDER_GEOMETRY, m_cSliderGeometry);
	DDX_Control(pDX, IDC_SLIDER_FILE, m_cSliderFile);
	//}}AFX_DATA_MAP
	if (SAVEDATA)
	{
		char buff[20];
		m_pPreferences->m_sTotalMem = itoa(m_cSliderTotal.GetPos()*16,buff,10);
		m_pPreferences->m_sHeapSize = itoa(m_cSliderHeap.GetPos()*2,buff,10);
		m_pPreferences->m_sFileHeapSize = itoa(m_cSliderFile.GetPos()*2,buff,10);
	}
}


BEGIN_MESSAGE_MAP(CPropPerformancePage, CPropertyPage)
	//{{AFX_MSG_MAP(CPropPerformancePage)
	ON_WM_HSCROLL()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CPropPerformancePage::UpdateOtherPages()
{
}

void CPropPerformancePage::UpdateAll()
{
	// pozice slider�
    m_cSliderTotal.SetPos(atoi(m_pPreferences->m_sTotalMem)/16);
    m_cSliderHeap.SetPos(atoi(m_pPreferences->m_sHeapSize)/2);
    m_cSliderFile.SetPos(atoi(m_pPreferences->m_sFileHeapSize)/2);

	int nP	 = atoi(m_pPreferences->m_sCalcPerform);
	double nP1 = log(nP*12/100);
	double nP2 = log(1.05);
    m_cSliderGeometry.SetPos((int)(nP1/nP2+0.5));
	// zobrazen� textu
	CString sText;
	MntSprintfInt(sText,"%d MB",m_cSliderTotal.GetPos()*16);
	MntSetWindowText(DLGCTRL(IDC_SHOW_TOTAL),sText);
	MntSprintfInt(sText,"%d MB",m_cSliderHeap.GetPos()*2);
	MntSetWindowText(DLGCTRL(IDC_SHOW_TEXTURE),sText);
	MntSprintfInt(sText,"%d MB",m_cSliderFile.GetPos()*2);
	MntSetWindowText(DLGCTRL(IDC_SHOW_FILE),sText);

	int size=(int)((int)(pow(1.05,m_cSliderGeometry.GetPos()))*(100.0/12));
	MntSprintfInt(sText,"%d",size);
	MntSetWindowText(DLGCTRL(IDC_SHOW_GEOMETRY),sText);

	//UpdateData(false);
	/*
	for (int i=0; i<sheet->GetPageCount(); i++)
	{
		CPropertyPage *page = sheet->GetPage(i);
		if (page!=this) page->UpdateData(false);
	}
	*/

};

void CPropPerformancePage::UpdateMemory()
{
	if (UpdateData(TRUE))
	{
		m_pPreferences->DefaultMemory();
		m_pPreferences->DefaultTextures();
		m_pPreferences->DefaultGeometry();
		UpdateAll();

		/*
		// update data on other pages
		CWnd *parent = GetParent();
		CPropertySheet *sheet = static_cast<CPropertySheet *>(parent);

		sheet->UpdateData(false);
		*/
	}
};

void CPropPerformancePage::UpdateGraphics()
{
	if (UpdateData(TRUE))
	{
		m_pPreferences->DefaultGeometry();
		UpdateAll();
	}
};

void CPropPerformancePage::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	if (pScrollBar == (CScrollBar*)&m_cSliderTotal)
		OnChangeSliderTotal();
	else
	if (pScrollBar == (CScrollBar*)&m_cSliderHeap)
		OnChangeSliderHeap();
	else
	if (pScrollBar == (CScrollBar*)&m_cSliderFile)
		OnChangeSliderFile();
	else
	if (pScrollBar == (CScrollBar*)&m_cSliderGeometry)
		OnChangeSliderGeometry();
	else
	CPropertyPage::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CPropPerformancePage::OnChangeSliderTotal()
{
    int size = m_cSliderTotal.GetPos()*16;
    UpdateMemory();
};

void CPropPerformancePage::OnChangeSliderFile()
{
    int size=m_cSliderFile.GetPos()*2;
    // set texture sizes to default values
	if (UpdateData(TRUE))
	{
		m_pPreferences->DefaultTextures();
		UpdateAll();
	}
};

void CPropPerformancePage::OnChangeSliderHeap()
{
    int size=m_cSliderHeap.GetPos()*2;
    // set texture sizes to default values
	if (UpdateData(TRUE))
	{
		m_pPreferences->DefaultTextures();
		UpdateAll();
	}
};

void CPropPerformancePage::OnChangeSliderGeometry()
{
	char buff[20];
	m_pPreferences->m_sCalcPerform = itoa((int)((int)(pow(1.05,m_cSliderGeometry.GetPos()))*(100.0/12)),buff,10);
	UpdateGraphics();
};

/////////////////////////////////////////////////////////////////////////////
// CPropEffectsPage dialog

class CPropEffectsPage : public CPropertyPage
{
// Construction
public:
	CPropEffectsPage();

// Dialog Data
	CPosPrefObject*	m_pPreferences;

	//{{AFX_DATA(CPropEffectsPage)
	enum { IDD = IDD_PROP_EFFECTS };
	CSliderCtrl	m_cSliderLights;
	//}}AFX_DATA

			void OnChangeMaxLights();


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPropEffectsPage)
	public:
	virtual BOOL OnSetActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CPropEffectsPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

CPropEffectsPage::CPropEffectsPage() : CPropertyPage(CPropEffectsPage::IDD)
{
	//{{AFX_DATA_INIT(CPropEffectsPage)
	//}}AFX_DATA_INIT
}

BOOL CPropEffectsPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

	m_cSliderLights.SetRange(1,32);
	m_cSliderLights.SetPageSize(10);
	m_cSliderLights.SetSelection(1,0);
	m_cSliderLights.SetTicFreq(2);
	m_cSliderLights.SetPos(m_pPreferences->m_nMaxLights);
	OnChangeMaxLights();	

	((CPosPrefApp*) AfxGetApp ())->ApplyApplicationNameAndStringTable (this);
	return TRUE;
}

void CPropEffectsPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPropEffectsPage)
	DDX_Control(pDX, IDC_SLIDER_LIGHTS, m_cSliderLights);
	//}}AFX_DATA_MAP

/*
	DDX_Check(pDX, IDC_CHECK_CLOUDLETS, m_pPreferences->m_bCloudlets);
	DDX_Check(pDX, IDC_CHECK_VEHSHADOWS, m_pPreferences->m_bVehicleShadows);
	DDX_Check(pDX, IDC_CHECK_OBJSHADOWS, m_pPreferences->m_bObjectShadows);
*/
	DDX_Check(pDX, IDC_ADD_EXPLOSIONS,	 m_pPreferences->m_bLightExplo);
	DDX_Check(pDX, IDC_ADD_MISSILES,	 m_pPreferences->m_bLightMissile);
	DDX_Check(pDX, IDC_ADD_STATIC,		 m_pPreferences->m_bLightStatic);

	if (SAVEDATA)
		m_pPreferences->m_nMaxLights = m_cSliderLights.GetPos();
}


BEGIN_MESSAGE_MAP(CPropEffectsPage, CPropertyPage)
	//{{AFX_MSG_MAP(CPropEffectsPage)
	ON_WM_HSCROLL()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CPropEffectsPage::OnChangeMaxLights()
{
	int nLights = m_cSliderLights.GetPos();
	CString sText;
	MntSprintfInt(sText,"%d",nLights);
	MntSetWindowText(DLGCTRL(IDC_SHOW_LIGHTS),sText);
};

void CPropEffectsPage::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	if (pScrollBar == (CScrollBar*)&m_cSliderLights)
	{	// zm�na Max lights
		OnChangeMaxLights();
	} else
	CPropertyPage::OnHScroll(nSBCode, nPos, pScrollBar);
}

/////////////////////////////////////////////////////////////////////////////
// CPropTexturesPage dialog

class CPropTexturesPage : public CPropertyPage
{
// Construction
public:
	CPropTexturesPage();

// Dialog Data
	CPosPrefObject*	m_pPreferences;

	//{{AFX_DATA(CPropTexturesPage)
	enum { IDD = IDD_PROP_TEXTURES };
	CComboBox	m_cObjects;
	CComboBox	m_cLandscape;
	CComboBox	m_cEffect;
	CComboBox	m_cDrop;
	CComboBox	m_cCocpits;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPropTexturesPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CPropTexturesPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void InitTextureSizes(CComboBox &ctrl, LONG minSize, LONG maxSize, LONG curSize);
	LONG GetTextureSize(CComboBox &ctrl);
	void SetTextureSize(CComboBox &ctrl, LONG size);
};

CPropTexturesPage::CPropTexturesPage() : CPropertyPage(CPropTexturesPage::IDD)
{
	//{{AFX_DATA_INIT(CPropTexturesPage)
	//}}AFX_DATA_INIT
}

void CPropTexturesPage::InitTextureSizes(CComboBox &ctrl, LONG minSize, LONG maxSize, LONG curSize)
{
	ctrl.Clear();
	for (LONG size = minSize; size <= maxSize; size *= 2)
	{
		char buffer[32];
		sprintf(buffer, "%d x %d", size, size);
		int index = ctrl.AddString(buffer);
		ctrl.SetItemData(index, size);
	}

	SetTextureSize(ctrl, curSize);
}

LONG CPropTexturesPage::GetTextureSize(CComboBox &ctrl)
{
	int index = ctrl.GetCurSel();
	if (index < 0) return 256;
	return ctrl.GetItemData(index);
}

void CPropTexturesPage::SetTextureSize(CComboBox &ctrl, LONG size)
{
	int index = -1;
	for (int i=0; i<ctrl.GetCount(); i++)
	{
		if (ctrl.GetItemData(i) == size)
		{
			index = i;
			break;
		}
	}
	if (index < 0) index = ctrl.GetCount() - 1;
	ctrl.SetCurSel(index);
}

BOOL CPropTexturesPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	InitTextureSizes(m_cCocpits, 64, m_pPreferences->m_MaxTextureSize, m_pPreferences->m_nTexCock);
	InitTextureSizes(m_cObjects, 64, m_pPreferences->m_MaxTextureSize, m_pPreferences->m_nTexObj);
	InitTextureSizes(m_cLandscape, 64, m_pPreferences->m_MaxTextureSize, m_pPreferences->m_nTexLand);
	InitTextureSizes(m_cEffect, 32, m_pPreferences->m_MaxTextureSize, m_pPreferences->m_nTexAnim);
	
	// TODO: Add extra initialization here
	
	((CPosPrefApp*) AfxGetApp ())->ApplyApplicationNameAndStringTable (this);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CPropTexturesPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPropTexturesPage)
	DDX_Control(pDX, IDC_OBJECTS, m_cObjects);
	DDX_Control(pDX, IDC_LANDSCAPE, m_cLandscape);
	DDX_Control(pDX, IDC_EFFECTS, m_cEffect);
	DDX_Control(pDX, IDC_DROP, m_cDrop);
	DDX_Control(pDX, IDC_COCPITS, m_cCocpits);
	//}}AFX_DATA_MAP
	if (SAVEDATA)
	{
/*
		switch(m_cCocpits.GetCurSel())
		{
		case 0: m_pPreferences->m_nTexCock = 64; break;
		case 1: m_pPreferences->m_nTexCock = 128; break;
		case 2: m_pPreferences->m_nTexCock = 256; break;
		};
		switch(m_cObjects.GetCurSel())
		{
		case 0: m_pPreferences->m_nTexObj = 64; break;
		case 1: m_pPreferences->m_nTexObj = 128; break;
		case 2: m_pPreferences->m_nTexObj = 256; break;
		};
		switch(m_cLandscape.GetCurSel())
		{
		case 0: m_pPreferences->m_nTexLand = 64; break;
		case 1: m_pPreferences->m_nTexLand = 128; break;
		};
		switch(m_cEffect.GetCurSel())
		{
		case 0: m_pPreferences->m_nTexAnim = 32; break;
		case 1: m_pPreferences->m_nTexAnim = 64; break;
		case 2: m_pPreferences->m_nTexAnim = 128; break;
		};
*/
		m_pPreferences->m_nTexCock = GetTextureSize(m_cCocpits);
		m_pPreferences->m_nTexObj = GetTextureSize(m_cObjects);
		m_pPreferences->m_nTexLand = GetTextureSize(m_cLandscape);
		m_pPreferences->m_nTexAnim = GetTextureSize(m_cEffect);

		switch(m_cDrop.GetCurSel())
		{
		case 0: m_pPreferences->m_nTexDrop = 8; break;
		case 1: m_pPreferences->m_nTexDrop = 4; break;
		case 2: m_pPreferences->m_nTexDrop = 2; break;
		case 3: m_pPreferences->m_nTexDrop = 0; break;
		};
	} else
	{
/*
		if (m_pPreferences->m_nTexCock == 256) m_cCocpits.SetCurSel(2); else
		if (m_pPreferences->m_nTexCock == 128) m_cCocpits.SetCurSel(1); else
		m_cCocpits.SetCurSel(0);

		if (m_pPreferences->m_nTexObj == 256) m_cObjects.SetCurSel(2); else
		if (m_pPreferences->m_nTexObj == 128) m_cObjects.SetCurSel(1); else
		m_cObjects.SetCurSel(0);
	
		if (m_pPreferences->m_nTexLand == 128) m_cLandscape.SetCurSel(1); else
		m_cLandscape.SetCurSel(0);
		
			if (m_pPreferences->m_nTexAnim == 128) m_cEffect.SetCurSel(2); else
		if (m_pPreferences->m_nTexAnim == 64)  m_cEffect.SetCurSel(1); else
		m_cEffect.SetCurSel(0);
*/
		SetTextureSize(m_cCocpits, m_pPreferences->m_nTexCock);
		SetTextureSize(m_cObjects, m_pPreferences->m_nTexObj);
		SetTextureSize(m_cLandscape, m_pPreferences->m_nTexLand);
		SetTextureSize(m_cEffect, m_pPreferences->m_nTexAnim);


		if (m_pPreferences->m_nTexDrop == 0) m_cDrop.SetCurSel(3); else
		if (m_pPreferences->m_nTexDrop == 2) m_cDrop.SetCurSel(2); else
		if (m_pPreferences->m_nTexDrop == 4) m_cDrop.SetCurSel(1); else
		m_cDrop.SetCurSel(0);
	};
}


BEGIN_MESSAGE_MAP(CPropTexturesPage, CPropertyPage)
	//{{AFX_MSG_MAP(CPropTexturesPage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPropAdvancedPage dialog

class CPropAdvancedPage : public CPropertyPage
{
// Construction
public:
	CPropAdvancedPage();

// Dialog Data
	CPosPrefObject*	m_pPreferences;

	//{{AFX_DATA(CPropAdvancedPage)
	enum { IDD = IDD_PROP_AVANCED };
	CSliderCtrl	m_cSliderLODShadows;
	CSliderCtrl	m_cSliderLODObjects;
	CSliderCtrl	m_cSliderMaxObjects;
	//}}AFX_DATA


// Overrides
			void UpdateAll();

			void UpdateMaxMin();
			//void UpdateDistances(BOOL bChngL, BOOL bChngO, BOOL bChngS);

	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPropAdvancedPage)
	public:
	virtual BOOL OnSetActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CPropAdvancedPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

CPropAdvancedPage::CPropAdvancedPage() : CPropertyPage(CPropAdvancedPage::IDD)
{
	//{{AFX_DATA_INIT(CPropAdvancedPage)
	//}}AFX_DATA_INIT
}

BOOL CPropAdvancedPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	// nastaven� slider�

	m_cSliderMaxObjects.SetRange(20,256);
	m_cSliderMaxObjects.SetPageSize(10);
	m_cSliderMaxObjects.SetSelection(20,0);
	m_cSliderMaxObjects.SetTicFreq(5);

	m_cSliderLODObjects.SetRange(0,55);
	m_cSliderLODObjects.SetPageSize(10);
	//m_cSliderLODObjects.SetSelection(2,0);
	m_cSliderLODObjects.SetTicFreq(5);

	m_cSliderLODShadows.SetRange(0,55);
	m_cSliderLODShadows.SetPageSize(10);
	//m_cSliderLODShadows.SetSelection(2,0);
	m_cSliderLODShadows.SetTicFreq(5);

	// update pozice a text�
	UpdateAll();

	((CPosPrefApp*) AfxGetApp ())->ApplyApplicationNameAndStringTable (this);
	return TRUE; 
}

void CPropAdvancedPage::DoDataExchange(CDataExchange* pDX)
{
	// perform data exchange of all children pages
	/*
	CWnd *parent = GetParent();
	CPropertySheet *sheet = static_cast<CPropertySheet *>(parent);
	for (int i=0; i<sheet->GetPageCount(); i++)
	{
		CPropertyPage *page = sheet->GetPage(i);
		if (page!=this) page->UpdateData(false);
	}
	*/

	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPropAdvancedPage)
	DDX_Control(pDX, IDC_SLIDER_LODSAHDOWS, m_cSliderLODShadows);
	DDX_Control(pDX, IDC_SLIDER_LODOBJECT, m_cSliderLODObjects);
	DDX_Control(pDX, IDC_SLIDER_MAXOBJECTS, m_cSliderMaxObjects);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPropAdvancedPage, CPropertyPage)
	//{{AFX_MSG_MAP(CPropAdvancedPage)
	ON_WM_HSCROLL()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CPropAdvancedPage::UpdateAll()
{
	// distance
	{
		//float realLandDist  = (float)m_pPreferences->m_nLandDist;
		//float realObjDist   = (float)m_pPreferences->m_nObjDist;
		//float realShadowDist= (float)m_pPreferences->m_nShadowDist;
		//int lDist= (int)(ceil(log(realLandDist/400)/log(1.1))+0.5);
		//int oDist= (int)(ceil(log(realObjDist/400)/log(1.1))+0.5);
		//int sDist= (int)(ceil(log(realShadowDist/400)/log(1.1))+0.5);
		//m_cSliderShadows.SetPos(sDist);
		// texty
		//CString sText;
		//MntSprintfInt(sText,"%d m",m_pPreferences->m_nLandDist);
		//MntSetWindowText(DLGCTRL(IDC_SHOW_LANDSCAPE),sText);
		//MntSprintfInt(sText,"%d m",m_pPreferences->m_nObjDist);
		//MntSetWindowText(DLGCTRL(IDC_SHOW_OBJECTS),sText);
		//MntSprintfInt(sText,"%d m",m_pPreferences->m_nShadowDist);
		//MntSetWindowText(DLGCTRL(IDC_SHOW_SHADOWS),sText);
	}
	// maximum
	{
		m_cSliderMaxObjects.SetPos(m_pPreferences->m_nMaxObjects);
		CString sText;
		MntSprintfInt(sText,"%d",m_pPreferences->m_nMaxObjects);
		MntSetWindowText(DLGCTRL(IDC_SHOW_MAXOBJECTS),sText);
	}
	// LOD
	{
		{
			float realLOD = m_pPreferences->m_nObjectLOD;
			int lodPos=(int)(log(realLOD)/log(1.1)+0.5);
			CString sText;
			MntSprintfFloat(sText,"%.1f",m_pPreferences->m_nObjectLOD);
			MntSetWindowText(DLGCTRL(IDC_SHOW_LOD),sText);
		}
		{
		    float realLOD = 1.f/m_pPreferences->m_nLimitLOD;
			int lodPos =(int)(log(realLOD)/log(1.1)+0.5);
			m_cSliderLODObjects.SetPos(lodPos);
			CString sText;
			MntSprintfFloat(sText,"%.3f",m_pPreferences->m_nLimitLOD);
			MntSetWindowText(DLGCTRL(IDC_SHOW_LODOBJECT),sText);
		}
		{
		    float realLOD = 1.f/m_pPreferences->m_nShadowLOD;
			int lodPos = (int)(log(realLOD)/log(1.1)+0.5);
			m_cSliderLODShadows.SetPos(lodPos);
			CString sText;
			MntSprintfFloat(sText,"%.3f",m_pPreferences->m_nShadowLOD);
			MntSetWindowText(DLGCTRL(IDC_SHOW_LODSAHDOWS),sText);
		}
	}
};

void CPropAdvancedPage::UpdateMaxMin()
{
	m_pPreferences->m_nMaxObjects =	m_cSliderMaxObjects.GetPos();
	UpdateAll();
};

/*
void CPropAdvancedPage::UpdateDistances(BOOL bChngL, BOOL bChngO, BOOL bChngS)
{
	if (bChngS)
	{
		int sDist=m_cSliderShadows.GetPos();
	    int realShadowDist=(int)(pow(1.1,sDist)*400);
		m_pPreferences->m_nShadowDist = realShadowDist/50*50;
	}
	UpdateAll();
};
*/

void CPropAdvancedPage::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	/*
	if (pScrollBar == (CScrollBar*)&m_cSliderShadows)
	{
		BOOL bChngO = FALSE, bChngL = FALSE;
		UpdateDistances(bChngL, bChngO, TRUE);
	} else
	*/
	if (pScrollBar == (CScrollBar*)&m_cSliderMaxObjects)
	{
		UpdateMaxMin();
	} else
	/*
	if (pScrollBar == (CScrollBar*)&m_cSliderMaxShadows)
	{
		if (m_cSliderMaxObjects.GetPos() < m_cSliderMaxShadows.GetPos())
			m_cSliderMaxObjects.SetPos(m_cSliderMaxShadows.GetPos());
		UpdateMaxMin();
	} else
	*/
	if (pScrollBar == (CScrollBar*)&m_cSliderLODObjects)
	{
		int lodLog=m_cSliderLODObjects.GetPos();
		m_pPreferences->m_nLimitLOD=1.f/(float)(pow(1.1,lodLog));
		UpdateAll();	
	} else
	if (pScrollBar == (CScrollBar*)&m_cSliderLODShadows)
	{
		int lodLog=m_cSliderLODShadows.GetPos();
		m_pPreferences->m_nShadowLOD=1.f/(float)(pow(1.1,lodLog));
		UpdateAll();	
	} else
	CPropertyPage::OnHScroll(nSBCode, nPos, pScrollBar);
}

/////////////////////////////////////////////////////////////////////////////
// PropBeta dialog

class PropBeta : public CPropertyPage
{
// Construction
public:
	CPosPrefObject*	m_pPreferences;

	PropBeta(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(PropBeta)
	enum { IDD = IDD_PROP_BETA };
	CString	m_BetaKey;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(PropBeta)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(PropBeta)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// PropBeta dialog


PropBeta::PropBeta(CWnd* pParent /*=NULL*/)
	: CPropertyPage(PropBeta::IDD)
{
	//{{AFX_DATA_INIT(PropBeta)
	m_BetaKey = _T("");
	//}}AFX_DATA_INIT
}


void PropBeta::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PropBeta)
	DDX_Text(pDX, IDC_EDIT_CDKEY, m_BetaKey);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(PropBeta, CPropertyPage)
	//{{AFX_MSG_MAP(PropBeta)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAdvancedPropSheet

class CAdvancedPropSheet : public CPropertySheet
{
// Construction
public:
	CAdvancedPropSheet(LPCTSTR sCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);

	int	nLastSelPage;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAdvancedPropSheet)
	public:
	virtual BOOL DestroyWindow();
	virtual BOOL OnInitDialog();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CAdvancedPropSheet)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAdvancedPropSheet::CAdvancedPropSheet(LPCTSTR sCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(sCaption, pParentWnd, iSelectPage)
{
	m_psh.dwFlags |= PSH_NOAPPLYNOW;
	m_psh.dwFlags &= ~PSH_HASHELP;
	nLastSelPage = iSelectPage;
}

BOOL CAdvancedPropSheet::OnInitDialog() 
{
	BOOL bResult = CPropertySheet::OnInitDialog();
	// BUTTON v�dy anglicky
	CString sBtnName = ((CPosPrefApp*) AfxGetApp ())->PrefLocalizeString (_T ("PREF_STR_BTN_CANCEL"));
	::SetWindowText(DLGCTRL(IDCANCEL),sBtnName);
	sBtnName = ((CPosPrefApp*) AfxGetApp ())->PrefLocalizeString (_T ("PREF_STR_BTN_OK"));
	::SetWindowText(DLGCTRL(IDOK),sBtnName);
	
	((CPosPrefApp*) AfxGetApp ())->ApplyApplicationNameAndStringTable (this);

	CTabCtrl *tab = GetTabControl ();
	if (tab != NULL)
	{
		CString buf;
		int n = tab->GetItemCount ();
		for (int i = 0; i < n; ++i)
		{
			TCITEM item;
			item.mask = TCIF_TEXT;
			item.pszText = buf.GetBuffer (256);
			item.cchTextMax = 256;
			if (item.pszText != NULL && tab->GetItem (i, &item))
			{
				buf.ReleaseBuffer ();
				((CPosPrefApp*) AfxGetApp ())->ApplyApplicationNameAndStringTable (buf, true);
				item.mask = TCIF_TEXT;
				item.pszText = buf.LockBuffer ();
				tab->SetItem (i, &item);
				buf.UnlockBuffer ();
			}
			else
			{
				buf.ReleaseBuffer ();
			}
		}
	}
	return bResult;
}

BOOL CAdvancedPropSheet::DestroyWindow() 
{
	nLastSelPage = GetActiveIndex();
	return CPropertySheet::DestroyWindow();
}

BEGIN_MESSAGE_MAP(CAdvancedPropSheet, CPropertySheet)
	//{{AFX_MSG_MAP(CAdvancedPropSheet)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DoEditAdvancedPerformance

static int  nLastSelPage = 0;
BOOL DoEditAdvancedPerformance(CPosPrefObject* pPreferences)
{
	CString caption = ((CPosPrefApp*) AfxGetApp ())->PrefLocalizeString (_T ("PREF_STR_ADVANCEDDLG"));
	CAdvancedPropSheet		dlg((LPCTSTR) caption,NULL,nLastSelPage);

	// kopie dat
	CPosPrefObject			m_Preferences;
	m_Preferences.CopyFrom(pPreferences);
	// str�nky
	CPropPerformancePage	pagePerformance; pagePerformance.m_pPreferences = &m_Preferences;
	CPropEffectsPage		pageEffects;	 pageEffects.m_pPreferences = &m_Preferences;
	CPropTexturesPage		pageTextures;	 pageTextures.m_pPreferences = &m_Preferences;
	CPropAdvancedPage		pageAdvanced;	 pageAdvanced.m_pPreferences = &m_Preferences;
	//PropBeta pageBeta;	 pageBeta.m_pPreferences = &m_Preferences;
	// spu�t�n� okna
	dlg.AddPage(&pagePerformance);
	dlg.AddPage(&pageEffects);
	dlg.AddPage(&pageTextures);
	dlg.AddPage(&pageAdvanced);
	//dlg.AddPage(&pageBeta);
	int nRes	 = dlg.DoModal();
	nLastSelPage = dlg.nLastSelPage;
	if (nRes != IDOK)
		return FALSE;
	// p�ijata data
	pPreferences->CopyFrom(&m_Preferences);
	return TRUE;
};


BOOL CPropAdvancedPage::OnSetActive() 
{
	UpdateAll();
	
	return CPropertyPage::OnSetActive();
}

BOOL CPropEffectsPage::OnSetActive() 
{
	UpdateData(false);
	
	return CPropertyPage::OnSetActive();
}
