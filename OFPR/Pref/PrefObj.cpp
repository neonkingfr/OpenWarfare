/*********************************************************************/
/*																	 */
/*	Poseidon Preferences										     */
/*									Copyright � Mentar, 1999         */
/*																	 */
/*********************************************************************/


#include "stdafx.h"
#include "math.h"
#include "mmsystem.h"
#include "Pref.h"
#include "PrefObj.h"
#include "PosRegistry.h"

#include <dinput.h>
#include <d3d8.h>
#include <ddraw.h>
#include <Es/Framework/debugLog.hpp>
#include <El/Math/math3d.hpp>

#pragma comment(lib,"ddraw")
/*
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
*/

/////////////////////////////////////////////////////////////////////////////
// CPosPrefObject

char *CPosPrefObject::HWNames[]=
{
    "Glide",
    "Direct3D",
    "Direct3D HW T&L",
    NULL,
};

/*
char *CPosPrefObject::CPUNames[]=
{
	"Generic",
	"Intel Vector SSE",
	"Intel Scalar SSE",
	NULL
};
*/


CPosPrefObject::CPosPrefObject()
{
  m_benchmark= 0;
	
	m_MaxTextureSize = 256;
	_d3d = Direct3DCreate8(D3D_SDK_VERSION);
	// we know DX 8.1 features are not used
	// if 8.1 creating will fail, try creating 8.0
	#define D3D_SDK_VERSION_80 120
	if (!_d3d)
	{
		_d3d = Direct3DCreate8(D3D_SDK_VERSION_80);
	}
};

CPosPrefObject::~CPosPrefObject()
{
};

// kopie dat
void CPosPrefObject::CopyFrom(const CPosPrefObject* pSrc)
{
	_d3d = pSrc->_d3d;
    m_sHwType		= pSrc->m_sHwType;

	m_sCpu			= pSrc->m_sCpu;
	m_sRam			= pSrc->m_sRam;
	m_sPerform		= pSrc->m_sPerform;
	m_sFillRate		= pSrc->m_sFillRate;

    m_sCalcPerform	= pSrc->m_sCalcPerform;
    m_sCalcMemPerform=pSrc->m_sCalcMemPerform;
    //m_sBenchmark	= pSrc->m_sBenchmark;
	m_sDefPerform	= pSrc->m_sDefPerform;
    m_sResolW		= pSrc->m_sResolW;
	m_sResolH		= pSrc->m_sResolH;
	m_sResolBpp		= pSrc->m_sResolBpp;

	m_sTotalMem		= pSrc->m_sTotalMem;
    m_sHeapSize		= pSrc->m_sHeapSize;
    m_sFileHeapSize	= pSrc->m_sFileHeapSize;

/*
		m_bBackground	= pSrc->m_bBackground;
    m_bObjectShadows= pSrc->m_bObjectShadows;
    m_bVehicleShadows= pSrc->m_bVehicleShadows;
    m_bCloudlets= pSrc->m_bCloudlets;
    m_bReflections	= pSrc->m_bReflections;
*/
		
    m_bLightExplo	= pSrc->m_bLightExplo;
    m_bLightMissile	= pSrc->m_bLightMissile;
    m_bLightStatic	= pSrc->m_bLightStatic;
    m_nMaxLights	= pSrc->m_nMaxLights;

    m_nTexCock		= pSrc->m_nTexCock;
    m_nTexLand		= pSrc->m_nTexLand;
    m_nTexObj		= pSrc->m_nTexObj;
    m_nTexAnim		= pSrc->m_nTexAnim;
    m_nTexDrop		= pSrc->m_nTexDrop;

    //m_nLandDist		= pSrc->m_nLandDist;
    //m_nObjDist		= pSrc->m_nObjDist;
    //m_nShadowDist	= pSrc->m_nShadowDist;

    m_nMaxObjects	= pSrc->m_nMaxObjects;

    m_nObjectLOD	= pSrc->m_nObjectLOD;
    m_nLimitLOD		= pSrc->m_nLimitLOD;
    m_nShadowLOD	= pSrc->m_nShadowLOD;

    m_nSpeedPref	= pSrc->m_nSpeedPref;
    m_nQualityPref	= pSrc->m_nQualityPref;

	m_sCpuName		= pSrc->m_sCpuName;
    m_isMMX			= pSrc->m_isMMX;
    m_isSSE			= pSrc->m_isSSE;
    m_cpuPerf		= pSrc->m_cpuPerf;
    m_benchmark		= pSrc->m_benchmark;

	m_hwType		= pSrc->m_hwType;
	m_adapterOrdinal = pSrc->m_adapterOrdinal;

	//m_cpuType		= pSrc->m_cpuType;
    m_ramMB			= pSrc->m_ramMB;
    m_glide			= pSrc->m_glide;
	m_d3D			= pSrc->m_d3D;
	m_d3DHWTL			= pSrc->m_d3DHWTL;
	m_info3Dfx		= pSrc->m_info3Dfx;
	m_infoD3D		= pSrc->m_infoD3D;
	m_FrameMB	= pSrc->m_FrameMB;
	m_TexMB	= pSrc->m_TexMB;


	m_MaxTextureSize = pSrc->m_MaxTextureSize;
};


// rozli�en� obrazovky
int CPosPrefObject::GetResolutionID(LPCTSTR pResW,LPCTSTR pResH,LPCTSTR pResB) const
{
	int nW = atoi(pResW);
	int nH = atoi(pResH);
	int nB = atoi(pResB);
	// match all
	int i;
	for(i=0; i<m_Resolutions.Size(); i++)
	{
		if ((m_Resolutions[i].nWidth  == nW)&&
			(m_Resolutions[i].nHeight == nH)&&
			(m_Resolutions[i].nBPP    == nB))
			return i;
	}
	// match w,h
	for(i=0; i<m_Resolutions.Size(); i++)
	{
		if ((m_Resolutions[i].nWidth  == nW)&&
			(m_Resolutions[i].nHeight == nH))
			return i;
	}
	// default 640,480,16
	return 0;
};

void CPosPrefObject::SetResolution(int nResID)
{
	if ((nResID >=0)&&(nResID<m_Resolutions.Size()))
	{
		char buff[20];
		m_sResolW	= itoa(m_Resolutions[nResID].nWidth,buff,10);
		m_sResolH	= itoa(m_Resolutions[nResID].nHeight,buff,10);
		m_sResolBpp	= itoa(m_Resolutions[nResID].nBPP,buff,10);
	} else
	{
		m_sResolW	= "640";
		m_sResolH	= "480";
		m_sResolBpp	= "16";
	};
};

#if _VBS1
#define RegKey "Software\\BIS\\VBS1"
#elif _GALATEA
#define RegKey "Software\\Bohemia Interactive Studio\\GALATEA"
#elif _COLD_WAR_ASSAULT
#define RegKey "Software\\BIS\\ColdWarAssault"
#elif _DEMO
#define RegKey "Software\\Codemasters\\Operation Flashpoint Demo"
#elif _MP_DEMO
#define RegKey "Software\\Codemasters\\Operation Flashpoint MP Demo"
#else
#define RegKey "Software\\Codemasters\\Operation Flashpoint"
#endif

#if _VBS1
#define POS_FILE	"VBS1.cfg"
#elif _GALATEA
#define POS_FILE	"GALATEA.cfg"
#elif _COLD_WAR_ASSAULT
#define POS_FILE	"ColdWarAssault.cfg"
#else
#define POS_FILE	"Flashpoint.cfg"
#endif

// ukl�d�n� konfigurace
void CPosPrefObject::LoadConfig()
{
	// na�ten� souboru
	char buffer[_MAX_PATH];
	GetCurrentDirectory(_MAX_PATH, buffer);
	RString filename = RString(buffer) + RString("\\") + RString(POS_FILE);
	m_registry.Parse(filename);

	// na�ten� hodnot
	HandleValues(&m_registry,true);
};

void CPosPrefObject::SaveConfig()
{
	// ulo�en� hodnot
	HandleValues(&m_registry,false);

	// ulo�en� souboru
	char buffer[_MAX_PATH];
	GetCurrentDirectory(_MAX_PATH, buffer);
	RString filename = RString(buffer) + RString("\\") + RString(POS_FILE);
	m_registry.Save(filename);
};

template <class Type>
void ReadValue(ParamFile *cls, const char *name, Type &value)
{
	const ParamEntry *entry = cls->FindEntry(name);
	if (entry) value = *entry;
}

template <> void ReadValue(ParamFile *cls, const char *name, CString &value)
{
	const ParamEntry *entry = cls->FindEntry(name);
	if (entry)
	{
		RString val = *entry;
		value = (const char *)val;
	}
}

template <> void ReadValue(ParamFile *cls, const char *name, int &value)
{
	const ParamEntry *entry = cls->FindEntry(name);
	if (entry)
	{
		float val = *entry;
		value = toInt(val);
	}
}

template <> void ReadValue(ParamFile *cls, const char *name, unsigned int &value)
{
	const ParamEntry *entry = cls->FindEntry(name);
	if (entry)
	{
		float val = *entry;
		value = toInt(val);
	}
}

template <> void ReadValue(ParamFile *cls, const char *name, long &value)
{
	const ParamEntry *entry = cls->FindEntry(name);
	if (entry)
	{
		float val = *entry;
		value = toInt(val);
	}
}

template <class Type>
void WriteValue(ParamFile *cls, const char *name, Type &value)
{
	cls->Add(name, value);
}

template <> void WriteValue(ParamFile *cls, const char *name, CString &value)
{
	RString val = (const char *)value;
	cls->Add(name, val);
}

template <> void WriteValue(ParamFile *cls, const char *name, unsigned int &value)
{
	int val = value;
	cls->Add(name, val);
}

void CPosPrefObject::HandleValues(ParamFile *cls, bool load)
{
	if (load)
	{
		ReadValue(cls, "Product", _product.className);
		ReadValue(cls, "Language", m_sLanguage);
			
		ReadValue(cls, "HW_Type", m_sHwType);
		ReadValue(cls, "Adapter", m_adapterOrdinal);
		ReadValue(cls, "3D_Performance", m_sCalcPerform);
		ReadValue(cls, "CPU_Benchmark", m_benchmark);

		ReadValue(cls, "Resolution_W", m_sResolW);
		ReadValue(cls, "Resolution_H", m_sResolH);
		ReadValue(cls, "Resolution_Bpp", m_sResolBpp);

		ReadValue(cls, "LOD", m_nObjectLOD);
		ReadValue(cls, "Limit_LOD", m_nLimitLOD);
		ReadValue(cls, "Shadows_LOD", m_nShadowLOD);

		ReadValue(cls, "MaxObjects", m_nMaxObjects);

		ReadValue(cls, "Cockpit_Textures", m_nTexCock);
		ReadValue(cls, "Landscape_Textures", m_nTexLand);
		ReadValue(cls, "Object_Textures", m_nTexObj);
		ReadValue(cls, "Animated_Textures", m_nTexAnim);
		ReadValue(cls, "Textures_Drop_Down", m_nTexDrop);

		ReadValue(cls, "Texture_Heap", m_sHeapSize);
		ReadValue(cls, "File_Heap", m_sFileHeapSize);
		ReadValue(cls, "Total_Memory", m_sTotalMem);

		ReadValue(cls, "MaxLights", m_nMaxLights);
		ReadValue(cls, "Light_Explo", m_bLightExplo);
		ReadValue(cls, "Light_Missile", m_bLightMissile);
		ReadValue(cls, "Light_Static", m_bLightStatic);

		ReadValue(cls, "Frame_Rate_Pref", m_nSpeedPref);
		ReadValue(cls, "Quality_Pref", m_nQualityPref);
	}
	else
	{
		WriteValue(cls, "Product", _product.className);
		WriteValue(cls, "Language", m_sLanguage);
			
		WriteValue(cls, "HW_Type", m_sHwType);
		WriteValue(cls, "Adapter", m_adapterOrdinal);
		WriteValue(cls, "3D_Performance", m_sCalcPerform);
		WriteValue(cls, "CPU_Benchmark", m_benchmark);

		WriteValue(cls, "Resolution_W", m_sResolW);
		WriteValue(cls, "Resolution_H", m_sResolH);
		WriteValue(cls, "Resolution_Bpp", m_sResolBpp);

		WriteValue(cls, "LOD", m_nObjectLOD);
		WriteValue(cls, "Limit_LOD", m_nLimitLOD);
		WriteValue(cls, "Shadows_LOD", m_nShadowLOD);

		WriteValue(cls, "MaxObjects", m_nMaxObjects);

		WriteValue(cls, "Cockpit_Textures", m_nTexCock);
		WriteValue(cls, "Landscape_Textures", m_nTexLand);
		WriteValue(cls, "Object_Textures", m_nTexObj);
		WriteValue(cls, "Animated_Textures", m_nTexAnim);
		WriteValue(cls, "Textures_Drop_Down", m_nTexDrop);

		WriteValue(cls, "Texture_Heap", m_sHeapSize);
		WriteValue(cls, "File_Heap", m_sFileHeapSize);
		WriteValue(cls, "Total_Memory", m_sTotalMem);

		WriteValue(cls, "MaxLights", m_nMaxLights);
		WriteValue(cls, "Light_Explo", m_bLightExplo);
		WriteValue(cls, "Light_Missile", m_bLightMissile);
		WriteValue(cls, "Light_Static", m_bLightStatic);

		WriteValue(cls, "Frame_Rate_Pref", m_nSpeedPref);
		WriteValue(cls, "Quality_Pref", m_nQualityPref);
	}
/*
	pReg->SetLoad(load);

	pReg->HandleStringValue("Product",_product.className);
	pReg->HandleStringValue("Language",m_sLanguage);
		
	pReg->HandleStringValue("HW Type",m_sHwType);
	pReg->HandleUINTValue("Adapter",m_adapterOrdinal);
	//pReg->HandleStringValue("CPU Type",m_sCpuType);
	pReg->HandleStringValue("3D Performance",m_sCalcPerform);
	pReg->HandleLongValue("CPU Benchmark",m_benchmark);

	pReg->HandleStringValue("Resolution W",m_sResolW);
	pReg->HandleStringValue("Resolution H",m_sResolH);
	pReg->HandleStringValue("Resolution Bpp",m_sResolBpp);

	//pReg->HandleLongValue("Landscape Visibility",m_nLandDist);
	//pReg->HandleLongValue("Objects Visibility",m_nObjDist);
	//pReg->HandleLongValue("Shadows Visibility",m_nShadowDist);

	pReg->HandleFloatValue("LOD",m_nObjectLOD);
	pReg->HandleFloatValue("Limit LOD",m_nLimitLOD);
	pReg->HandleFloatValue("Shadows LOD",m_nShadowLOD);

	pReg->HandleLongValue("MaxObjects",m_nMaxObjects);

	pReg->HandleLongValue("Cockpit Textures",m_nTexCock);
	pReg->HandleLongValue("Landscape Textures",m_nTexLand);
	pReg->HandleLongValue("Object Textures",m_nTexObj);
	pReg->HandleLongValue("Animated Textures",m_nTexAnim);
	pReg->HandleLongValue("Textures Drop Down",m_nTexDrop);

	pReg->HandleStringValue("Texture Heap",m_sHeapSize);
	pReg->HandleStringValue("File Heap",m_sFileHeapSize);
	pReg->HandleStringValue("Total Memory",m_sTotalMem);

	pReg->HandleLongValue("MaxLights",m_nMaxLights);
	pReg->HandleBoolValue("Light Explo",m_bLightExplo);
	pReg->HandleBoolValue("Light Missile",m_bLightMissile);
	pReg->HandleBoolValue("Light Static",m_bLightStatic);

	pReg->HandleLongValue("Frame Rate Pref",m_nSpeedPref);
	pReg->HandleLongValue("Quality Pref",m_nQualityPref);
*/
};

/////////////////////////////////////////////////////////////////////////////
// pr�ce s daty

void CPosPrefObject::AutoDetectD3D()
{
	// assume some minimum card abilities (16 MB VRAM)
	m_FrameMB = 4;
	m_TexMB = 12;
	UINT adapter = 0;

	// ask directDraw to get information about video ram?
	// we do not know how adapters ID maps to ddraw GUIDS
	// we can handle only the simple case - default adapter is used
	// if users has more adapters, we assume has has plenty of VRAM
	ComRef<IDirectDraw7> ddraw;
	DirectDrawCreateEx(NULL,(void **)ddraw.Init(),IID_IDirectDraw7,NULL);
	if (ddraw)
	{
		DDSCAPS2 caps;
		DWORD totalmem = 0,freemem = 0;
		caps.dwCaps = DDSCAPS_LOCALVIDMEM|DDSCAPS_TEXTURE|DDSCAPS_VIDEOMEMORY;
		caps.dwCaps2 = 0;
		caps.dwCaps3 = 0;
		caps.dwCaps4 = 0;
		ddraw->GetAvailableVidMem(&caps,&totalmem,&freemem);
		if (totalmem<=0)
		{
			caps.dwCaps = DDSCAPS_NONLOCALVIDMEM|DDSCAPS_TEXTURE|DDSCAPS_VIDEOMEMORY;
			ddraw->GetAvailableVidMem(&caps,&totalmem,&freemem);
		}
		if (totalmem>0)
		{
			m_TexMB = totalmem/(1024*1024)-m_FrameMB;
			if (m_TexMB<4) m_TexMB = 4;
		}

	}
	/*
	D3DCAPS8 caps;
	_d3d->GetDeviceCaps(adapter,D3DDEVTYPE_HAL,&caps);
	*/



}

#if _MSC_VER < 1300
typedef struct _MEMORYSTATUSEX 
{
    DWORD dwLength;
    DWORD dwMemoryLoad;
    DWORDLONG ullTotalPhys;
    DWORDLONG ullAvailPhys;
    DWORDLONG ullTotalPageFile;
    DWORDLONG ullAvailPageFile;
    DWORDLONG ullTotalVirtual;
    DWORDLONG ullAvailVirtual;
    DWORDLONG ullAvailExtendedVirtual;
} MEMORYSTATUSEX, *LPMEMORYSTATUSEX;
#endif

typedef BOOL (__stdcall *GlobalMemoryStatusExFunc)(LPMEMORYSTATUSEX); 

void CPosPrefObject::Autodetect()
{
	m_sCpuName = DetectCPU();

    m_hwType= HWNames[TypeDirect3D];
    m_glide	= false;
		m_d3D	  = true;
		m_d3DHWTL = false;


    //m_cpuType= CPUNames[CPUGeneric];

		//if( m_isSSE ) m_cpuType= CPUNames[CPUIntelScalar];
	 //if( m_isSSE ) m_cpuType= CPUNames[CPUIntelSSE];

		m_FrameMB=2;
		m_TexMB=2;

    HINSTANCE lib;
    lib=::LoadLibrary("glide3x.dll");
    if( lib )
    {
        ::FreeLibrary(lib);
			lib=::LoadLibrary("is3Dfx.dll");
			if( lib )
			{
					FARPROC proc=::GetProcAddress(lib,"_Get3DfxInfo@4");
					if( proc )
					{
							typedef bool __stdcall infoType( Info3Dfx *info );
							infoType *infoProc=(infoType *)proc;
							Info3Dfx info;
							if( infoProc(&info) )
							{
					sprintf(m_info3Dfx.GetBuffer(1000),"%s (%d/%d MB), %s",
							info.name,info.frameBufMB,info.totalTexMB,info.version);
					m_info3Dfx.ReleaseBuffer();
									m_FrameMB=info.frameBufMB;
									m_TexMB=info.totalTexMB;
									m_hwType=HWNames[Type3Dfx];
									m_glide=true;
							}
					}
					::FreeLibrary(lib);
			}
		}

		if (!m_glide)
		{
			// D3D video memory size detection
			AutoDetectD3D();

		}

		if (_d3d)
		{
			UINT adapter = 0;
			D3DCAPS8 caps;
			HRESULT hr = _d3d->GetDeviceCaps(adapter, D3DDEVTYPE_HAL, &caps);
			if (hr==D3D_OK)
			{
				m_d3DHWTL = (caps.DevCaps&D3DDEVCAPS_HWTRANSFORMANDLIGHT)!=0;
				if (m_d3DHWTL)
				{
						m_hwType=HWNames[TypeDirect3DTL];
				}
			}
		}

    {
		HINSTANCE lib = LoadLibrary("kernel32.dll");
		GlobalMemoryStatusExFunc func = (GlobalMemoryStatusExFunc)GetProcAddress(lib, "GlobalMemoryStatusEx");

		__int64 total;
		if (func)
		{
			MEMORYSTATUSEX mem;
			mem.dwLength=sizeof(mem);
			(*func)(&mem);
			total = mem.ullTotalPhys;
		}
		else
		{
			MEMORYSTATUS mem;
			mem.dwLength=sizeof(mem);
			::GlobalMemoryStatus(&mem);
			total = mem.dwTotalPhys;
		}

        // it seems that memory is not reported correctly (31 MB instead of 32 MB)
        int memSize = (total + 1024 * 1024 - 1) / (1024 * 1024);
        m_ramMB = min(memSize, 1024); // limit to 1 GB to prevent overflow
    }
};

#include <Es/Types/pointers.hpp>

#define DECLARE_PRIORITY	\
	HANDLE hProc = GetCurrentProcess();\
	HANDLE hThrd = GetCurrentThread();\
	DWORD dwPrtyCls = GetPriorityClass(hProc);\
	DWORD dwPrty = GetThreadPriority(hThrd)

#define BOOST_PRIORITY	\
	if (dwPrtyCls)\
		if (!SetPriorityClass(hProc, REALTIME_PRIORITY_CLASS  /*HIGH_PRIORITY_CLASS*/))\
			dwPrtyCls = 0;\
	if (dwPrty)\
		if (!SetThreadPriority(hThrd, THREAD_PRIORITY_HIGHEST))\
			dwPrty = 0

#define RESTORE_PRIORITY	\
	if (dwPrty)\
		SetThreadPriority(hThrd, dwPrty);\
	if (dwPrtyCls)\
		SetPriorityClass(hProc, dwPrtyCls)

void CPosPrefObject::Benchmark()
{
	#if _DEBUG
  m_benchmark=800;
	return;
	#endif

	AfxGetApp()->BeginWaitCursor();


  #define NVec 100000

	DECLARE_PRIORITY;
	BOOST_PRIORITY;

  Temp<Vector3> src(NVec),dst(NVec);
  DWORD timeStart;
  //Vector3 src[NVec],dst[NVec];
  for( int b=0; b<20; b++ )
  {
		int i;
		if( b==2 ) timeStart=::GetTickCount();
		for( i=0; i<NVec; i++ )
		{
			src[i]=Vector3(0.2f,0.6f,-0.5f);
		}
		Matrix4 translation(MTranslation,Vector3(56.23f*b,253.2f,-569.3f));
		Matrix4 rotationY(MRotationY,1.25f*b);
		Matrix4 rotationZ(MRotationZ,-0.66f*b);
		for( i=0; i<NVec; i++ )
		{
			Matrix4 transform=rotationZ*rotationY*translation;
			dst[i].SetFastTransform(transform,src[i]);
		}
  }

  DWORD timeEnd=::GetTickCount();
  //double benchmark=3600000./(timeEnd-timeStart);
	//double benchmark=1350000./(timeEnd-timeStart);
	double benchmark=1500000./(timeEnd-timeStart);
  m_benchmark=(int)benchmark;

	RESTORE_PRIORITY;

	AfxGetApp()->EndWaitCursor();
};



#pragma warning( disable : 4035 )
#define CPUID	__asm __emit 0fh __asm __emit 0a2h
#define RDTCS	__asm __emit 0fh __asm __emit 031h

int CanCpuid()
{
	__asm
	{
		pushfd
		pop eax
		mov ecx,eax
		xor eax,200000h
		push eax
		popfd
		pushfd
		pop eax
		xor eax, ecx
	}
};

int CpuInfo( char *vendor )
{
	__asm
	{
		mov  esi, vendor
		push ebx
		push ecx
		push edx
		xor eax,eax
	}
	CPUID
	__asm
	{
		mov [esi],ebx
		mov [esi+4],edx
		mov [esi+8],ecx
		pop edx
		pop ecx
		pop ebx
	}
};

int CpuExtInfo( int &sig, int &feat )
{
	__asm
	{
		mov  esi, sig
		mov  edi, feat
		push ebx
		push ecx
		push edx
		mov eax,1
	}
	CPUID
	__asm
	{
		mov [esi],eax
		mov [edi],edx
		pop edx
		pop ecx
		pop ebx
	}
};

int CpuRdtsc()
{
	int nVal;
	__asm
	{
		push ebx
		push ecx
		push edx
		xor eax,eax
	}
	CPUID
	RDTCS
	__asm
	{
		mov esi,eax
		xor eax,eax
		mov nVal,esi
	}
	CPUID
	__asm
	{
		pop edx
		pop ecx
		pop ebx
	}
	return nVal;
};

CString	CPosPrefObject::DetectCPU()
{
	m_isMMX = false;
	m_isSSE = false;

	CString sResult;

    char cpuVendor[12];
    // first of all check if CPUID is available
    int canCpuid=CanCpuid();
    if( !canCpuid )
    {
        m_cpuPerf=30; // very low performance
        sResult = "486 ";
		return sResult;
    }
    
    int  cpuInfo=CpuInfo(cpuVendor);
    bool isIntel=false;
    bool isAMD=false;
    enum {CPUIntel,CPUAMD,CPUCyrix,CPUUnknown} type=CPUUnknown;
    if( !strncmp(cpuVendor,"GenuineIntel",12) )
    {
        isIntel=true;
        sResult+="Intel Pentium ";
        type=CPUIntel;
    }
    else if( !strncmp(cpuVendor,"AuthenticAMD",12) )
    {
        sResult+="AMD K5 ";
        type=CPUAMD;
		    isAMD=true;
    }
    else if( !strncmp(cpuVendor,"CyrixInstead",12) )
    {
        sResult+="Cyrix 6x86 ";
        type=CPUCyrix;
    }
    else
    {
        sResult+="Unknown 586 ";
        type=CPUUnknown;
    }

    int signature,features;
    float perfBoost=1.0f;
    CpuExtInfo(signature,features);
    bool isMMX= (features&(1<<23))!=0;
    bool isCMOV=(features&(1<<15))!=0;
    bool isFXSR=(features&(1<<24))!=0;
    bool isXMM= (features&(1<<25))!=0;
    if( isIntel )
    {
        int type=(signature>>4)&0xff;
        switch( type )
        {
            case 0x51:
            case 0x52:
            case 0x53:
                sResult="Intel Pentium ";
            break;
            case 0x54:
                sResult="Intel Pentium MMX ";
                perfBoost=1.12f;
            break;
            case 0x61:
                sResult="Intel Pentium Pro ";
                perfBoost=1.4f;
            break;
            case 0x63:
                sResult="Intel Pentium II ";
                perfBoost=1.6f;
            break;
            case 0x65:
                sResult="Intel Pentium II "; // unknown name
                perfBoost=1.6f;
            break;
            case 0x66:
                sResult="Intel Celeron "; // unknown name
                perfBoost=1.5f;
            break;
            case 0x67:
                sResult="Intel Pentium II "; // P2/Celeron/Katmai
                perfBoost=1.6f;
                if( isXMM ) sResult="Intel Pentium III "; // P2/Celeron/Katmai
            break;
            case 0x68:
                sResult="Intel Pentium III "; // P2/Celeron/Katmai
                perfBoost=1.6f;
						break;
						case 0xf0:
                perfBoost=1.4f;
                sResult="Intel Pentium 4 ";
						break;
            default:
                // unknow model - check feature flags
                sResult+="Pro ";
                if( type>=0x60 ) perfBoost=1.5f;
                if( isXMM ) sResult+="SSE ";
                else if( isMMX ) sResult+="MMX ";
            break;
        }
    }
		else if (isAMD)
		{
        int type=(signature>>4)&0xff;
				switch (type)
				{
					case 0x50:
					case 0x51:
					case 0x52:
					case 0x53:
						sResult = "AMD-K5 ";
					break;
					case 0x56:
					case 0x57:
						sResult = "AMD-K6 ";
					break;
					case 0x58:
						sResult = "AMD-K6-2 ";
					break;
					case 0x59:
						sResult = "AMD-K6-III ";
					break;
					case 0x61:
					case 0x62:
					case 0x64:
						sResult = "AMD-Athlon ";
					break;
					case 0x63:
						sResult = "AMD-Duron ";
					break;
					default:
						sResult = "AMD ";
					break;
				}
			
		}
    else
    {
        if( isXMM ) perfBoost=1.6f,sResult+="SSE ";
        else if( isCMOV ) perfBoost=1.3f,sResult+="PPro "; // PPro class procesor
        else if( isMMX ) perfBoost=1.12f,sResult+="MMX ";
    }
    float maxClock=0;


    if (isMMX) m_isMMX=true;
		if (isXMM) m_isSSE = true;
    
    // performance counter not available
    // set timer to maximal precision
    bool msTimer=(timeBeginPeriod(1)==TIMERR_NOERROR);
    int timeResol=1000;
    if( msTimer ) timeResol=1;
    else
    {
        // determine actual timer precision
        int begTime=timeGetTime();
        int time=begTime;
        while( time<begTime+100 )
        {
            int lastTime=time;
            time=timeGetTime();
            if( time>lastTime && time<lastTime+timeResol ) timeResol=time-lastTime;
        }
    }
    //WMessageBox::Messagef(this,WMsgBSOk,"Timer","%d ms",timeResol);
    // measure CPU speed
    for( int i=0; i<3; i++ )
    {
        // note: eTime-sTime >= actual time elapsed between RDTSC
        // therefore: clock <= actual CPU clock
        int sTime=timeGetTime();
        int sClock=CpuRdtsc();
        int eTime;
        do
        {
            eTime=timeGetTime();
        } while( eTime<sTime+500 );
        int eClock=CpuRdtsc();
        eTime=timeGetTime()+timeResol;
        float clock=(float)(double(eClock-sClock)*1e-3/(eTime-sTime));
        if( clock>maxClock ) maxClock=clock;
    }
    if( msTimer ) timeEndPeriod(1);
    m_cpuPerf  = (int)(maxClock*perfBoost);


	char clockStr[20];
	sprintf(clockStr,"%.1f",maxClock);
  sResult+=clockStr;
	sResult+=" MHz";
	return sResult;
};

// nastaven� hodnot
void CPosPrefObject::DefaultValues(BOOL bUseAutodetect,BOOL bUseAutodetectPerf)
{
	m_sLanguage = "English";

	m_sCpu = m_sCpuName;

  if( bUseAutodetect )
	{
		m_sHwType = m_hwType; // accelerator name
		m_adapterOrdinal = 0;
		//m_sCpuType = m_cpuType; // CPU name
	}
	
	
	sprintf(m_sRam.GetBuffer(1000),"%d MB",m_ramMB); // physical RAM size
	m_sRam.ReleaseBuffer();

	sprintf(m_sPerform.GetBuffer(100),"%d",m_cpuPerf); // Pentium clock - estimated
	m_sPerform.ReleaseBuffer();

	EstimatePerf();

  if( bUseAutodetectPerf )
  {
      m_sCalcPerform = m_sDefPerform;
      m_sTotalMem = m_sRam;
  }
  
  DefaultMemory();
  DefaultGeometry();
  DefaultTextures();

  if( bUseAutodetectPerf )
  {
		m_sResolBpp = "16";
    if( !strcmpi(m_sHwType,HWNames[Type3Dfx]) )
    {
      if( m_FrameMB<4 )
      {
				m_sResolW="640",m_sResolH="480";
      }
      else if( m_FrameMB<5 )
      {
				m_sResolW="800",m_sResolH="600";
      }
			else if( m_FrameMB<16 )
      {
				m_sResolW="1024",m_sResolH="768";
      }
			else
			{
				m_sResolW="1280",m_sResolH="1024";
			}
    }
    else
    {
			// TODO: better autodetection
			// get device caps
      m_sResolW="1024",m_sResolH="768";
			m_sResolBpp = "32";
    }
  }

};

void CPosPrefObject::DefaultMemory()
{
  int ram=atoi(m_sTotalMem);
  int wantFile=(ram-20)/12;
  if( wantFile<4 ) wantFile=4;
  else if( wantFile>16 ) wantFile=16;
  wantFile&=~1;
  int wantHeap=wantFile/2;
	if (ram >= 128)
		wantHeap = wantFile;
  wantHeap&=~1;
	// memory
	MntSprintfInt(m_sHeapSize,"%d",wantHeap);
	MntSprintfInt(m_sFileHeapSize,"%d",wantFile);

  m_nSpeedPref    = 500;
  m_nQualityPref  = 500;    
};

inline int limit( int a, int b )
{
  return ( a<b ? a : b );
}

void CPosPrefObject::DefaultTextures()
{
  int wantHeap=atoi(m_sHeapSize);
  m_nTexCock=256;
  m_nTexObj =128;
  m_nTexLand=128;
  m_nTexAnim =64;
  m_nTexDrop=4;

	int maxTexSize = 2048;
	UINT adapter = m_adapterOrdinal;
	if (_d3d)
	{
		D3DCAPS8 caps;
		_d3d->GetDeviceCaps(adapter, D3DDEVTYPE_HAL, &caps);
		maxTexSize = min(caps.MaxTextureHeight, caps.MaxTextureWidth);
		if (maxTexSize<256) maxTexSize = 256;
	}

  if( m_TexMB>=60 && !m_glide)
	{
    m_nTexCock  = limit(4096,maxTexSize);
    m_nTexObj  =limit(4096,maxTexSize);
    m_nTexLand  =limit(4096,maxTexSize);
	}
  else if( m_TexMB>=28 && !m_glide)
	{
    m_nTexCock  = limit(2048,maxTexSize);
    m_nTexObj  =limit(2048,maxTexSize);
    m_nTexLand  =limit(2048,maxTexSize);
	}
  else if( m_TexMB>=12 && !m_glide)
	{
    m_nTexCock  = limit(1024,maxTexSize);
    m_nTexObj  =limit(1024,maxTexSize);
    m_nTexLand  =limit(1024,maxTexSize);
	}
  else if( m_TexMB>=8 )
  {
    m_nTexAnim =128;
    m_nTexObj  =256;
  }
  //  do not use large textures on systems with little system memory
  if( wantHeap<=10 )
	{
		m_nTexCock =limit(m_nTexObj,1024);
		m_nTexObj =limit(m_nTexObj,256);
		m_nTexLand =limit(m_nTexObj,256);
		m_nTexAnim=limit(m_nTexAnim,64);
		if( wantHeap<=6 )
		{
			m_nTexCock =limit(m_nTexCock,512);
			m_nTexObj =limit(m_nTexObj,128);
			m_nTexLand =limit(m_nTexObj,128);
			m_nTexAnim=limit(m_nTexAnim,64);
			if( wantHeap<=4 )
			{
				m_nTexObj =limit(m_nTexObj,64);
				m_nTexAnim=limit(m_nTexAnim,64);
				if( wantHeap<=2 )
				{
					m_nTexLand=limit(m_nTexCock,64);
					m_nTexLand=limit(m_nTexLand,64);
					m_nTexAnim=limit(m_nTexAnim,32);
				}
			}
		}
	}
}

static double LimitPerformance( double cpu, double ram )
{
  double limitPerf=ram/32.0*220;
  if( cpu<limitPerf ) return cpu;
  cpu=limitPerf+(cpu-limitPerf)*0.25;
  if( cpu>limitPerf*2 ) cpu=limitPerf*2;
  return cpu;
}

void CPosPrefObject::DefaultGeometry()
{
  bool enableEffects = true;

  //m_nLandDist  =600;
  //m_nObjDist   =400;
  //m_nShadowDist=200;
  
/*
  m_bBackground = false;
  m_bObjectShadows =false;
  m_bVehicleShadows = true;
  m_bCloudlets = true;
  m_bReflections =false;
*/

  m_bLightExplo=false;
  m_bLightMissile=false;
  m_bLightStatic=false;
  m_nMaxLights=4;

  m_nObjectLOD = 15.f;
m_nLimitLOD	 =
m_nShadowLOD = 0.5f;

  m_nMaxObjects =64;

  EstimatePerf();

  int userBenchmark=atoi(m_sCalcPerform);
  int ram=atoi(m_sTotalMem);
  userBenchmark=(int)(LimitPerformance(userBenchmark,ram)+0.5);
  //fdx->calcMemPerform=NumStr(userBenchmark);
  
  double quaCoef=m_nQualityPref*(1.0/500);
  double relativePerf=userBenchmark*(1.0f/150.0f);
  double sqrtRelativePerf=sqrt(relativePerf);
  // for maximum scalability use calculated values
  // square root for distance

  m_nTexAnim=64;

  // note: some features may have better performance on MMX (lighting, sound mixing)
  double mmxBenchmark=userBenchmark;
  //if( m_isMMX ) mmxBenchmark *=2;

  int m_nMaxSound=(int)(userBenchmark/60);
  if( m_nMaxSound<4 )  m_nMaxSound=4;
  if( m_nMaxSound>16 ) m_nMaxSound=16;

double quaBenchmark=userBenchmark*quaCoef;
  int maxLights=(int)(quaBenchmark/75);
  if( maxLights<2 ) maxLights=2;
  else if( maxLights>32 ) maxLights=32;
  m_nMaxLights= maxLights;
  if( quaBenchmark>=200 )
  {
    m_bLightExplo=true;
    if( quaBenchmark>=300 )
    {
      m_bLightMissile=true;
      m_bLightStatic=true;
    }
  }
/*
  if( enableEffects )
  {
    if( quaBenchmark>=1200 )
    {
      m_bObjectShadows =true;
	    if( quaBenchmark>=800 )
			{
				m_bVehicleShadows = true;
			}
    }
    if( quaBenchmark>=200 )
    {
			m_bReflections = true;
      m_bBackground=true;
    }
  }
*/
  
  //double lDist=0.7 * (sqrtRelativePerf*250+relativePerf*150);
  //double oDist=lDist*(400.0/600.0);
  //double sDist=0.2*(sqrtRelativePerf*250+relativePerf*150)*quaCoef;
  //if( sDist<200 ) sDist=200;if( sDist>600 ) sDist=600;
  //m_nShadowDist = (LONG)sDist;

  // linear for count
  double maxObj=1.2 * (sqrtRelativePerf*32+relativePerf*32)*quaCoef;
  double maxShad=maxObj*0.25;
  double maxRefl=maxObj*0.4;
  if( maxObj<80 ) maxObj=80;if( maxObj>256 ) maxObj=256;
  if( maxShad<8 ) maxShad=8;if( maxShad>maxObj ) maxShad=maxObj;
  if( maxRefl<8 ) maxRefl=8;if( maxRefl>maxObj ) maxRefl=maxObj;
  m_nMaxObjects = (LONG)maxObj;

  float LOD=0.5f * (float)(25.0/((sqrtRelativePerf*0.3+relativePerf*0.7)*quaCoef));
  if( LOD<7.5 ) LOD=7.5;if( LOD>20.0 ) LOD=20.0;

	float limitLOD=(float)((6-relativePerf)*0.4+0.05);
  float shadowLOD=(float)(limitLOD*2);
  if( limitLOD<0.02f ) limitLOD=0.02f;
  if( limitLOD>1.0f ) limitLOD=1.0f;
  if( shadowLOD<0.05f ) shadowLOD=0.05f;
  if( shadowLOD>1.0f ) shadowLOD=1.0f;

	m_nObjectLOD= ((int)(LOD*10.f))/10.f;
	m_nLimitLOD = ((int)(limitLOD*1000.f))/1000.f;
   m_nShadowLOD= ((int)(shadowLOD*1000.f))/1000.f;
};

void CPosPrefObject::EstimatePerf()
{
  int estFillRate=100;
  int estBenchmark= m_benchmark;
  if( estBenchmark<=10 ) estBenchmark= m_cpuPerf;
	sprintf(m_sDefPerform.GetBuffer(100),"%d",estBenchmark); // physical RAM size
	m_sDefPerform.ReleaseBuffer();
	sprintf(m_sFillRate.GetBuffer(100),"%d",estFillRate); // physical RAM size
	m_sFillRate.ReleaseBuffer();
};




