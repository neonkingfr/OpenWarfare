/*********************************************************************/
/*																	 */
/*	Poseidon Preferences										     */
/*									Copyright � Mentar, 1999         */
/*																	 */
/*********************************************************************/


/////////////////////////////////////////////////////////////////////////////
// CPosPrefObject - data storage


//#include "posRegistry.h"
#include <Es/Containers/array.hpp>
#include <Es/Strings/rstring.hpp>
#include <El/paramfile/paramfile.hpp>

struct Info3Dfx
{
	char	name[80];
	char	version[80];
	int		frameBufMB;
	int		totalTexMB;
	int		totalTMUse;
};

struct SResolution
{
	CString pText;
	int			nWidth;
	int			nHeight;
	int			nBPP;

	SResolution(){}
	SResolution(int w, int h, int bpp)
	:nWidth(w),nHeight(h),nBPP(bpp)
	{
		pText.Format("%5d x %4d x %2d",w,h,bpp);
	}
	bool operator == (const SResolution &w) const;
};
TypeIsGeneric(SResolution)

struct SAdapter
{
	UINT ordinal; // ordinal
	CString name; // user friendly name
};
TypeIsGeneric(SAdapter)

struct SControlDef
{
	const char*	pAction;		
	const char*	pPredefined;
	BOOL		bUserDef;
	LONG		nDefValue;
	LONG		nCurValue;
	const char* pCfgName;
};

struct ProductInfo
{
	RString className;
	RString name;
	RString application;
	RString arguments;
};
TypeIsMovableZeroed(ProductInfo)

#include <d3d8.h>

class CPosRegistry;
class CPosPrefObject : public CObject
{
	ComRef<IDirect3D8> _d3d;
public:
						CPosPrefObject();
						~CPosPrefObject();

						// kopie dat
				void	CopyFrom(const CPosPrefObject*);

						// ukl�d�n� konfigurace
				void	LoadConfig();
				void	SaveConfig();
				void	HandleValues(ParamFile *cls, bool load);

						// pr�ce s daty
				void	Autodetect();
				void	Benchmark();
				void AutoDetectD3D();

			 CString	DetectCPU();

						// nastaven� hodnot
				void	DefaultValues(BOOL bUseAutodetect,BOOL bUseAutodetectPerf);

				void	DefaultMemory();
				void	DefaultTextures();
				void	DefaultGeometry();

				void	EstimatePerf();

				// autodetection results
				bool	IsGlide() const {return m_glide;}
				bool	IsMMX() const {return m_isMMX;}
				bool	IsSSE() const {return m_isSSE;}
				bool	IsDirect3D() const {return m_d3D;}
				bool	IsDirect3DHWTL() const {return m_d3DHWTL;}

				// rozli�en� obrazovky
				int		GetResolutionID(LPCTSTR pResW,LPCTSTR pResH,LPCTSTR pResB) const;
				void	SetResolution(int nResID);


// data
public:
	CString	m_sLanguage;	// user selected language

	ProductInfo _product;	// product info

		CString		m_sHwType;	// user selected device
    //CString		m_sCpuType;	// user selected CPU

	//////////////////////////
    // read only strings

	CString		m_sCpu;		 // CPU description
	CString		m_sRam;		 // RAM in MB
	CString		m_sPerform;  // total CPU performance
	CString		m_sFillRate; // estimated fill rate

  CString		m_sCalcPerform;					 // user benchmark - geometry performance
  CString		m_sCalcMemPerform;				 // geometry performance with regard to memory
  //CString		m_sBenchmark;					 // CPU benchmark
	CString		m_sDefPerform;					 // autodetected performance
  CString		m_sResolW,m_sResolH,m_sResolBpp; // resolution

	CString		m_sTotalMem;
	CString		m_sHeapSize;
	CString		m_sFileHeapSize;

/*
	BOOL		m_bBackground;
  BOOL		m_bObjectShadows;
  BOOL		m_bVehicleShadows;
  BOOL		m_bCloudlets;
  BOOL		m_bReflections;
*/
	
  BOOL		m_bLightExplo;
  BOOL		m_bLightMissile;
  BOOL		m_bLightStatic;
  LONG		m_nMaxLights;

  LONG		m_nTexCock;
  LONG		m_nTexLand;
  LONG		m_nTexObj;
  LONG		m_nTexAnim;
  LONG		m_nTexDrop;

  //LONG		m_nLandDist;
  //LONG		m_nObjDist;
  //LONG		m_nShadowDist;

  LONG		m_nMaxObjects;

  float		m_nObjectLOD;
  float		m_nLimitLOD;
  float		m_nShadowLOD;

	// preferences - grafika
	enum { nMinGrPef = 100, nMaxGrPref = 1500, nDefGrPref = 500 };
  LONG		m_nSpeedPref;
  LONG		m_nQualityPref;

	//////////////////////////
	// controls

	//////////////////////////
	// Autodetect informations
	CString		m_sCpuName;
    bool		m_isMMX;		// MMX present
    bool		m_isSSE;		// MMX present
    int			m_cpuPerf;		// Pentium clock - estimated
    long			m_benchmark;	// Pentium clock - estimated

	CString		m_hwType;		// accelerator name
	UINT m_adapterOrdinal;
	//CString		m_cpuType;		// accelerator name
    int			m_ramMB;		// physical RAM size
    bool		m_glide,m_d3D,m_d3DHWTL;	// HW present
	CString		m_info3Dfx;		// HW additional information
	CString		m_infoD3D;
	int			m_FrameMB; // frame buffer mem. in MB
	int			m_TexMB;	// texture mem. in MB

	//////////////////////////
	// konstanty

	enum {Type3Dfx,TypeDirect3D,TypeDirect3DTL,TypeMax};
	static char *HWNames[];

	//enum {CPUGeneric,CPUIntelSSE,CPUIntelScalar,CPUMax};
	//static char *CPUNames[];

	FindArray<SResolution> m_Resolutions;
	AutoArray<SAdapter> m_Adapters;
	LONG m_MaxTextureSize;

	// config file
	// CPosRegistry m_registry;
	ParamFile m_registry;
};

/////////////////////////////////////////////////////////////////////////////

