/*********************************************************************/
/*																	 */
/*	Poseidon Preferences										     */
/*									Copyright � Mentar, 1999         */
/*																	 */
/*********************************************************************/

#include "resource.h"		// main symbols
// PosPref.h : main header file for the POSPREF application
//


/////////////////////////////////////////////////////////////////////////////
// CPosPrefApp:

class CPosPrefObject;

class CPosPrefApp : public CWinApp
{
public:
	CPosPrefApp();

	CString _prefAppName;
// Overrides
			void RunGame(CPosPrefObject* pData);
			CString PrefLocalizeString (LPCTSTR str);
			bool ApplyApplicationNameAndStringTable (CString &text, bool applyApplName);
			int PrefFindOneOf (CString &where, LPCTSTR charSet, int start);
			void ApplyApplicationNameAndStringTable (CWnd *item);

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPosPrefApp)
	public:
	virtual BOOL InitInstance();
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CPosPrefApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////
// Functions - (PosFuncs.cpp

void MntSetWindowText(HWND hWnd,const char* lpText);
void MntSprintfInt(CString&,const char* lpText,int nNum);
void MntSprintfFloat(CString&,const char* lpText,float nNum);


/////////////////////////////////////////////////////////////////////////////