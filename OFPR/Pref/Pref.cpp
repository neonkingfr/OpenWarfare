/*********************************************************************/
/*																	 */
/*	Poseidon Preferences										     */
/*									Copyright © Mentar, 1999         */
/*																	 */
/*********************************************************************/

// PosPref.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Pref.h"
#include "PrefObj.h"
#include "PrefDlg.h"
#include "PosRegistry.h"

#include <El/stringtable/stringtable.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPosPrefApp

BEGIN_MESSAGE_MAP(CPosPrefApp, CWinApp)
	//{{AFX_MSG_MAP(CPosPrefApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPosPrefApp construction

CPosPrefApp::CPosPrefApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CPosPrefApp object

CPosPrefApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CPosPrefApp initialization

extern void DoSendErrorFile(LPTSTR pCfg);

BOOL CPosPrefApp::InitInstance()
{
	AfxEnableControlContainer();

#ifdef _AFXDLL
	Enable3dControls();			
#else
	Enable3dControlsStatic();	
#endif

	switch (PRIMARYLANGID(GetUserDefaultLangID()))
	{
	case LANG_FRENCH:
		GLanguage = "French";
		break;
	case LANG_SPANISH:
		GLanguage = "Spanish";
		break;
	case LANG_ITALIAN:
		GLanguage = "Italian";
		break;
	case LANG_GERMAN:
		GLanguage = "German";
		break;
	case LANG_CZECH:
		GLanguage = "Czech";
		break;
	case LANG_POLISH:
		GLanguage = "Polish";
		break;
	case LANG_RUSSIAN:
		GLanguage = "Russian";
		break;
	default:
		GLanguage = "English";
		break;
	}
	LoadStringtable("global", "preferences.csv");

	_prefAppName = PrefLocalizeString (_T ("PREF_APPLICATION_NAME"));

	// error file
	// CPosRegistry	Registry;
	// DoSendErrorFile(Registry.m_sFileName.GetBuffer(1000));

	CPosPrefDlg dlg;
	/*THIS IS WHAT? m_pMainWnd = &dlg;*/

	// načtení konfigurace
	dlg.m_Preferences.Autodetect();
	dlg.m_Preferences.DefaultValues(TRUE,TRUE);
	dlg.m_Preferences.LoadConfig();
	// benchmark, pokud nebyl udelan
	if (dlg.m_Preferences.m_benchmark <= 10)
	{
		dlg.m_Preferences.Benchmark();
		dlg.m_Preferences.DefaultValues(TRUE,TRUE);
	}

	// preferences
	int nResponse = dlg.DoModal();
	
	// výsledky zpracování
	if (nResponse == IDC_MAIN_PLAY)
	{
		dlg.m_Preferences.SaveConfig();
		// spuštění aplikace
		RunGame(&dlg.m_Preferences);
		// AfxMessageBox("PLAY",MB_OK);
	} else
	if (nResponse == IDOK)
	{
		dlg.m_Preferences.SaveConfig();
	}
	//else if (nResponse == IDCANCEL)
	/*THIS IS WHAT? return FALSE;*/ return TRUE;
}

BOOL CPosPrefApp::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo) 
{
	// no help
	if (nID == ID_HELP)
		return FALSE;
	return CWinApp::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

/////////////////////////////////////////////////////////////////////////////
// RunGame

void CPosPrefApp::RunGame(CPosPrefObject* pData)
{
	STARTUPINFO startupInfo;
	PROCESS_INFORMATION processInformation;

	startupInfo.cb=sizeof(startupInfo); 
	startupInfo.lpReserved=0;
	startupInfo.lpDesktop=NULL;
	startupInfo.lpTitle=NULL;
	startupInfo.dwFlags=0;
	startupInfo. cbReserved2=0; 
	startupInfo.lpReserved2=NULL; 

/*	
	char name[1024];
#if _DEMO
	strcpy(name,"OperationFlashpointDemo.exe");
//	strcpy(name,"bin\\demo.bin");
#elif _MP_DEMO
	strcpy(name,"OperationFlashpointMPDemo.exe");
#elif _VBS1
	strcpy(name,"VBS1.exe");
#else
	strcpy(name,"OperationFlashpoint.exe");
//	strcpy(name,"bin\\generic.bin");
	//if( !strcmpi(pData->m_sCpuType,pData->CPUNames[pData->CPUIntelSSE]) ) strcpy(name,"bin\\intel_sse.bin");
	//else if( !strcmpi(pData->m_sCpuType,pData->CPUNames[pData->CPUIntelScalar]) ) strcpy(name,"bin\\generic.bin -piii");
#endif

	//if( !strcmpi(pData->m_sHwType,pData->HWNames[pData->TypeDirect3D]) ) strcat(name," -dx");
*/
	
	char name[1024];
	strcpy(name, pData->_product.application);

	if (pData->_product.arguments.GetLength() > 0)
	{
		strcat(name, " ");
		strcat(name, pData->_product.arguments);
	}
	
	for (int i = 1; i < __argc; i++)
	{
		LPCTSTR pszParam = __targv[i];
		strcat(name," ");
		strcat(name,pszParam);
	};
	// nastavení aktuálního adresáre
	{
		char buff[_MAX_PATH+1];
		DWORD nLen = GetModuleFileName(m_hInstance,buff,_MAX_PATH);
		while((buff[nLen-1]!=_T('\\'))&&(buff[nLen-1]!=_T('/'))) 
			nLen--;
		buff[nLen-1]=_T(0);
		SetCurrentDirectory(buff);
	}
	BOOL success=CreateProcess
	(
		NULL,name,
		NULL,NULL, // security
		FALSE, // inheritance
		0, // creation flags 
		NULL, // env
		NULL, // pointer to current directory name 
		&startupInfo,&processInformation 
	); 
};

/*
void __cdecl LogF(char const *,...)
{
};

void __cdecl ErrF(char const *,...)
{
};

void __cdecl LstF(char const *,...)
{
};
*/

CString CPosPrefApp::PrefLocalizeString (LPCTSTR str)
{
	CString d;
	CString s = LocalizeString (str);
	int a = 0;
	int f = s.Find (_T ('\\'));
	int max = s.GetLength () - 1;
	while (f >= 0 && f < max)
	{
		switch (s[f + 1])
		{
		case _T ('\\'):
			d += s.Mid (a, f - a);
			d += _T ('\\');
			a = f + 2;
			f = s.Find (_T ('\\'), a);
			break;

		case _T ('t'):
			d += s.Mid (a, f - a);
			d += _T ('\t');
			a = f + 2;
			f = s.Find (_T ('\\'), a);
			break;

		case _T ('n'):
			d += s.Mid (a, f - a);
			d += _T ('\n');
			a = f + 2;
			f = s.Find (_T ('\\'), a);
			break;

		case _T ('r'):
			d += s.Mid (a, f - a);
			d += _T ('\r');
			a = f + 2;
			f = s.Find (_T ('\\'), a);
			break;

		case _T ('\"'):
			d += s.Mid (a, f - a);
			d += _T ('\"');
			a = f + 2;
			f = s.Find (_T ('\\'), a);
			break;

		case _T ('\''):
			d += s.Mid (a, f - a);
			d += _T ('\'');
			a = f + 2;
			f = s.Find (_T ('\\'), a);
			break;

		default:
			f = s.Find (_T ('\\'), f + 1);
			break;
		}
	}
	d += s.Mid (a);
	return d;
}

bool CPosPrefApp::ApplyApplicationNameAndStringTable (CString &text, bool applyApplName)
{
	bool r = false;
	int f = text.FindOneOf (_T ("%$"));
	int max = text.GetLength () - 1;
	if (f >= 0 && f < max)
	{
		do {
			switch (text[f])
			{
			case _T ('$'):
				{
					int a = f + 1;
					while ((a <= max) && 
						(text[a] >= _T ('A') && text[a] <= _T ('Z') ||
						text[a] >= _T ('0') && text[a] <= _T ('9') ||
						text[a] >= _T ('a') && text[a] <= _T ('z') ||
						text[a] == _T ('_')))
					{
						++a;
					}
					text = text.Left (f) + 
						PrefLocalizeString (text.Mid (f + 1, a - f - 1)) + 
						text.Mid (a);
					max = text.GetLength () - 1;
					f = PrefFindOneOf (text, _T ("%$"), f);
					r = true;
				}
				break;

			case _T ('%'):
				switch (text[f + 1])
				{
				case _T ('0'):
					if (applyApplName)
					{
						text = text.Left (f) + _prefAppName + text.Mid (f + 2);
						max = text.GetLength () - 1;
						f = PrefFindOneOf (text, _T ("%$"), f);
						r = true;
					}
					else
					{
						f = PrefFindOneOf (text, _T ("%$"), f + 1);
					}
					break;

				default:
					f = PrefFindOneOf (text, _T ("%$"), f + 1);
					break;
				}
				break;

			default:
				f = PrefFindOneOf (text, _T ("%$"), f);
				break;
			}
		} while (f >= 0 && f < max);
	}

	return r;
}

int CPosPrefApp::PrefFindOneOf (CString &where, LPCTSTR charSet, int start)
{
	int len = where.GetLength ();
	if (start < 0 || start >= len)
	{
		return -1;
	}

	int charSetLen = _tcslen (charSet);
	while (start < len)
	{
		TCHAR c = where[start];
		for (int i = 0; i < charSetLen; ++i)
		{
			if (c != charSet[i])
			{
				continue;
			}

			return start;
		}
		++start;
	}
	return -1;
}

void CPosPrefApp::ApplyApplicationNameAndStringTable (CWnd *item)
{
	CString text;
	item->GetWindowText (text);

	if (ApplyApplicationNameAndStringTable (text, true))
	{
		item->SetWindowText (text);
	}

	CWnd *subItem = item->GetTopWindow ();
	while (subItem != NULL)
	{
		ApplyApplicationNameAndStringTable (subItem);
		subItem = subItem->GetNextWindow (GW_HWNDNEXT);
	}
}
