/*********************************************************************/
/*																	 */
/*	Poseidon Preferences										     */
/*									Copyright � Mentar, 1999         */
/*																	 */
/*********************************************************************/


/////////////////////////////////////////////////////////////////////////////
// CPosRegistry - data storage

#ifndef _POS_REGISTRY_H
#define _POS_REGISTRY_H

/*

#include <class\rstring.hpp>

class CPosRegistry : public CObject
{
public:
				CPosRegistry();
				~CPosRegistry();

				// serializace
				BOOL	LoadFile();
				BOOL	SaveFile();

				// pr�ce s prom�nn�mi
				void	AddStringValue(const char* pKey, const char* pValue);
				void	AddLongValue(const char* pKey, LONG nValue);
				void	AddUINTValue(const char* pKey, UINT nValue);
				void	AddFloatValue(const char* pKey, float nValue);
				void	AddBoolValue(const char* pKey, BOOL nValue);

				BOOL	GetStringValue(const char* pKey, CString& nValue);
				BOOL	GetStringValue(const char* pKey, RString& nValue);
				BOOL	GetLongValue(const char* pKey, LONG& nValue);
				BOOL	GetUINTValue(const char* pKey, UINT& nValue);
				BOOL	GetFloatValue(const char* pKey, float& nValue);
				BOOL	GetBoolValue(const char* pKey, BOOL& nValue);

				// operace s prom�nn�mi
				BOOL	HandleStringValue(const char* pKey, CString& nValue);
				BOOL	HandleStringValue(const char* pKey, RString& nValue);
				BOOL	HandleLongValue(const char* pKey, LONG& nValue);
				BOOL	HandleUINTValue(const char* pKey, UINT& nValue);
				BOOL	HandleFloatValue(const char* pKey, float& nValue);
				BOOL	HandleBoolValue(const char* pKey, BOOL& nValue);

				// normalizace jmena (odstaneni mezer)
				CString NormalizeName( CString name );
				CString UnnormalizeName( CString name );

				void SetLoad( bool load ){m_bLoad=load;}
				

// data
public:
	BOOL				m_bLoad;
	CString				m_sFileName;

	CMapStringToString	m_Values;
};

*/

#endif

/////////////////////////////////////////////////////////////////////////////

