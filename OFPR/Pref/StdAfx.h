/*********************************************************************/
/*																	 */
/*	Poseidon Preferences										     */
/*									Copyright � Mentar, 1999         */
/*																	 */
/*********************************************************************/

// PosPref.h : main header file for the POSPREF application
//
#if !defined(AFX_STDAFX_H__E10989EB_C6D8_11D2_809B_0060083C95F5__INCLUDED_)
#define AFX_STDAFX_H__E10989EB_C6D8_11D2_809B_0060083C95F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC OLE automation classes
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT


//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__E10989EB_C6D8_11D2_809B_0060083C95F5__INCLUDED_)

#define SAVEDATA		(pDX->m_bSaveAndValidate)
#define DLGCTRL(idc)	(::GetDlgItem(m_hWnd,idc))
