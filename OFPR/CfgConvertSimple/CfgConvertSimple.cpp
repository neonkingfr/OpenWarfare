// CfgConvert.cpp : Defines the entry point for the console application.
//

#include <El/elementpch.hpp>
#include <El/ParamFile/paramFile.hpp>
// #include <El/ParamXML/paramXML.hpp>

#include <El/evaluator/express.hpp>

#include <string.h>
#include <stdio.h>
#include <time.h>


static bool LoadParamFile(ParamFile &f, const char *name)
{
  // autodetect format based on extension or content?
	if (f.ParseBin(name))
  {
    return true;
  }
/*
  if (ParseXML(f,name))
  {
    return true;
  }
*/
  return f.Parse(name)==LSOK;
}

int main(int argc, char* argv[])
{
	// initialization of Element library
	GGameState.Init();

	argv++;
	argc--;
  enum OutMode {Txt,Bin/*,Xml*/} outMode = Txt;
	const char *dst = NULL; 
	bool nextDst = false;
	int ret = 0;
	if (argc < 1) goto Usage;
	while (argc>0)
	{
		const char *arg = *argv++;
		argc--;
		if (*arg == '-' || *arg == '/')
		{
			arg++; // skip dash
			if (!stricmp(arg,"bin")) outMode = Bin;
			else if (!stricmp(arg,"txt")) outMode = Txt;
/*
			else if (!stricmp(arg,"xml")) outMode = Xml;
*/
			else if (!stricmp(arg,"dst")) nextDst = true;
			else goto Usage;
		}
		else if (nextDst)
		{
			dst = arg;
			nextDst = false;
		}
		else
    {
			ParamFile f;
			if (!LoadParamFile(f,arg))
			{
				fprintf(stderr,"Error reading binary file '%s'\n",arg);
				ret = 1;
				continue;
			}
      if (outMode==Bin)
			{
				ParamFile f;
				clock_t startTime = clock();
				if (f.Parse(arg) != LSOK)
				{
					fprintf(stderr,"Error reading text file '%s'\n",arg);
  				ret = 1;
					continue;
				}
				double elapsed = double(clock()-startTime)/CLOCKS_PER_SEC*1000;
				printf("Parse: %.0f ms\n",elapsed);
				f.SaveBin(dst ? dst : arg);
				dst = NULL;
			}
			else if (outMode==Txt)
			{
				f.Save(dst ? dst : arg);
				dst = NULL;
			}
/*
			else if (outMode==Xml)
      {
				SaveXML(f,dst ? dst : arg);
				dst = NULL;
      }
*/
    }

	}
	return ret;

Usage:
	printf("Usage\n");
	printf("   cfgConvert [-bin | -txt | -xml] {[-dst <destination>] <source>}\n");
	return 1;		
}

/*
#include <Es/Memory/normalNew.hpp>

void *operator new ( size_t size, const char *file, int line )
{
	return malloc(size);
}
*/
