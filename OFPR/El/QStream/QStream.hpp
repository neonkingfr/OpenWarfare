#ifdef _MSC_VER
#pragma once
#endif

#ifndef _QSTREAM_HPP
#define _QSTREAM_HPP

#include <Es/Strings/rString.hpp>

#include <Es/Types/enum_decl.hpp>

DEFINE_ENUM_BEG(LSError)
	LSOK,
	LSFileNotFound, // no such file
	LSBadFile, // error in loaded file (CRC error...)
	LSStructure, // fire structure error - caused by programm bug
	LSUnsupportedFormat, // attempt to load other file format
	LSVersionTooNew, // attempt to load unknown version
	LSVersionTooOld, // attempt to load version that is no longer supported
	LSDiskFull, // cannot save - disk full
	LSAccessDenied, // read only, directory permiss...
	LSDiskError, // some disk error
	LSNoEntry,		// entry in ParamArchive not found
	LSNoAddOn,		// ADDED in patch 1.01 - AddOns check
	LSUnknownError,
DEFINE_ENUM_END(LSError)

class QIOS
{
	public:
	// namespace for definitions
	enum {beg,cur,end};
	enum {binary=1,text=2,in=4};
	typedef int seekdir;
	typedef int openmode;
};

// istream like simple and fast implementaion of file access
class QIStream
{
	protected:
	char *_buf;
	int _len;
	int _readFrom;
	bool _fail,_eof;
	LSError _error;

	protected:
	void Assign( const QIStream &src ); // this points to same data as from
	void Close();

	private: // disable copying
	QIStream &operator = ( const QIStream &src );
	QIStream( const QIStream &src );

	public:
	QIStream()
	:_buf(NULL),_len(0),_readFrom(0),_fail(true),_eof(false)
	{}
	QIStream( const void *buf, int len )
	:_buf((char *)buf),_len(len),_readFrom(0),_fail(false),_eof(false)
	{}
	void init( const void *buf, int len )
	{
		_buf=(char *)buf;
		_len=len;
		_readFrom=0;
		_fail=false;
		_eof=false;
	}
	int get()
	{
		// get single character
		if( _readFrom>=_len ) {_eof=true;return EOF;}
		return (unsigned char)_buf[_readFrom++];
	}
	void unget()
	{
		if( _readFrom>0 ) _readFrom--;
	}
	void read( void *buf, int n )
	{
		int left=_len-_readFrom;
		if( n>left )
		{
			if( left==0 ) _eof=true;
			_fail=true;
			return;
		}
		// note: buf and _buf may be unalligned - we cannot avoid this
		memcpy(buf,_buf+_readFrom,n);
		//for( int i=0; i<n; i++ ) buf[i]=_buf[_readFrom+i];
		_readFrom+=n;
	}
	void seekg( int pos, QIOS::seekdir dir )
	{
		int nPos;
		switch( dir )
		{
			case QIOS::beg:
				nPos=pos;
			break;
			case QIOS::end:
				nPos=_len+pos;
			break;
			default:
				nPos=_readFrom+pos;
			break;
		}
		if( nPos<0 || nPos>_len ) _fail=true;
		else _readFrom=nPos,_fail=false;
	}
	bool fail() const {return _fail;}
	bool eof() const {return _eof;}
	LSError error() const {return _error;}

    //! Reads the stream until EOLN (returns true) or EOF (returns false) is reached.
    //! Stops AFTER the EOLN.
    bool nextLine ();

    /**
        Reads the actual line into the supplied buffer.
        @param  buf External buffer.
        @param  bufLen Size of the buffer (including \0 terminator), 0 for unrestricted read.
                Too long lines will be truncated to fit in <code>buffer</code>.
        @return <code>true</code> if the line was read successfully.
    */
    bool readLine ( char *buf, int bufLen =0 );

	// caution: following functions are not available in istream
	// use them with care
	int tellg() const {return _readFrom;}
	const char *act() const {return _buf+_readFrom;}
	int rest() const {return _len-_readFrom;}
};

/*!
\patch_internal 1.31 Date 11/22/2001 by Jirka
- Added: Encryption using RSA
*/
class QOStream
{
	protected:
	AutoArray<char> _buffer;
	int _writeTo;

	private: // disable copying
	QOStream &operator = ( const QOStream &src );
	QOStream( const QOStream &src );

	public:
	QOStream():_writeTo(0){_buffer.Realloc(64*1024);}

	void rewind() {_buffer.Resize(0),_writeTo=0;}
	void setbuffer( int size ) {_buffer.Reserve(size,size);}

	void put( char c )
	{
		if( _writeTo>=_buffer.Size() ) _buffer.Add(c),_writeTo=_buffer.Size();
		else _buffer[_writeTo++]=c;
	}
	void write( const void *buf, int n )
	{
		int minSize=_writeTo+n;
		if( _buffer.Size()<minSize ) _buffer.Resize(minSize);
		memcpy(_buffer.Data()+_writeTo,buf,n);
		_writeTo+=n;
	}

	int tellp() const {return _writeTo;}
	
	void seekp( int pos, QIOS::seekdir dir )
	{
		int nPos;
		switch( dir )
		{
			case QIOS::beg:
				nPos=pos;
			break;
			case QIOS::end:
				nPos=_buffer.Size()+pos;
			break;
			default:
				nPos=_writeTo+pos;
			break;
		}
		if( nPos<0 || nPos>_buffer.Size() ) {}
		else _writeTo=nPos;
	}
	// str and pcount: see ostrstream
	const char *str() const {return _buffer.Data();} // get data
	int pcount() const {return _buffer.Size();} // get data size

	QOStream &operator <<(const char *buf)
	{
		write(buf, strlen(buf));
		return *this;
	}
};

//! class of callback functions
class ClipboardFunctions
{
public:
	ClipboardFunctions() {}
	virtual ~ClipboardFunctions() {}

	//! callback function to copy string into clipboard
	virtual void Copy(const char *buffer, int size) {}
};

class QOFStream: public QOStream
{
protected:
	RString _file;
	bool _fail;
	LSError _error;

	static ClipboardFunctions *_defaultClipboardFunctions;

public:
	QOFStream():_file(NULL),_fail(false){}

	QOFStream( const char *file )
	:_file(file),_fail(false)
	{}
	void open( const char *file );
	void close();
	void export_clip( const char *name ); // export to file or clipboard
	void close( const void *header, int headerSize );
	bool fail() const {return _fail;}
	LSError error() const {return _error;}

	~QOFStream(); // perform actual save

	static void SetDefaultClipboardFunctions(ClipboardFunctions *f) {_defaultClipboardFunctions = f;}
};

//#include "multiSync.hpp"

class QFBank;

//! file buffer interface
class IFileBuffer: public RefCount
{
	private:
	//! no copy allowed
	IFileBuffer( const IFileBuffer & );
	//! no copy allowed
	IFileBuffer &operator = ( const IFileBuffer & );

	public:
	IFileBuffer() {}

	virtual bool GetError() const = NULL;
	//! get data
	virtual const char *GetData() const = NULL;
	//! get data size
	virtual int GetSize() const = NULL;
	//! check if buffer is from given bank
	virtual bool IsFromBank(QFBank *bank) const = NULL;
	//! some data sources (overlapped IO) may need some time to get ready
	virtual bool IsReady() const = NULL;
};

class FileBufferMemory: public IFileBuffer
{
	protected:
	Buffer<char> _data;

	public:
	FileBufferMemory() {}
	FileBufferMemory( int size ) {_data.Init(size);}

	void Realloc( int size ) {_data.Init(size);}
	const char *GetData() const {return _data.Data();}
	// non-virtual writable access
	char *GetWritableData() {return _data.Data();}

	int GetSize() const {return _data.Size();}
	bool GetError() const {return false;}
	bool IsFromBank(QFBank *bank) const {return false;}
	bool IsReady() const {return true;}
};

class QIFStream: public QIStream
{
	private:
	Ref<IFileBuffer> _sharedData;

	//CriticalSection _serialize; // serialize disk access
	friend class QIFStreamB;

	public:
	QIFStream(){}
	QIFStream( const QIFStream &src ){DoConstruct(src);}
	void operator =( const QIFStream &src ){DoDestruct();DoConstruct(src);}

	//! open some memory data
	void open( Ref<IFileBuffer> buffer ){OpenBuffer(buffer);}

	//! open some memory data
	void OpenBuffer( Ref<IFileBuffer> buffer );

	IFileBuffer *GetBuffer() const {return _sharedData;}

	void import( const char *name ); // import from file or clipboard
	void open( const char *name ); // open and preload file

	void DoDestruct(); // close file and destroy buffer
	~QIFStream();

	void DoConstruct( const QIFStream &from ); // close from, open this
	static bool FileExists( const char *name ); // check normal file existence
	static bool FileReadOnly( const char *name ); // check normal file existence
};

//! basic most compatible file buffer implementation
/*!
	This implementation does not require any special features like file memory mapping
	or overlapped IO, but general performance is not very good.
*/

class FileBufferLoaded: public FileBufferMemory
{
	public:
	FileBufferLoaded( const char *name );
	~FileBufferLoaded();

	const char *GetData() const {return (char *)_data.Data();}
	int GetSize() const {return _data.Size();}

};


class SSCompress
{
	enum {N=4096};
	enum {F=18};
	enum {THRESHOLD=2};

	unsigned char text_buf[N + F - 1];
	int match_position,match_len;
	int lsons[N+1],rsons[N+257],dads[N+1];

	void InitTree();
	void InsertNode( int p );
	void DeleteNode( int p );
	
	public:
	bool Decode( char *dst, long lensb, QIStream &in );
	void Encode( QOStream &out, const char *dst, long lensb );
};

#ifndef _WIN32
int FileSize ( int handle );
#endif

#endif
