#include <El/elementpch.hpp>

#include "fileOverlapped.hpp"
#include "QBStream.hpp"

void CALLBACK FileBufferOverlappedCompleted
(
  DWORD dwErrorCode,                // completion code
  DWORD dwNumberOfBytesTransfered,  // number of bytes transferred
  LPOVERLAPPED lpOverlapped         // pointer to structure with I/O 
                                   // information
)
{
	// ingore canceled operations, they should be handled by caller of CancelIo()
	if (dwErrorCode==ERROR_OPERATION_ABORTED) return;

	FileBufferOverlapped *fbo = (FileBufferOverlapped *)lpOverlapped->hEvent;
	fbo->CompletedCallback(dwErrorCode,dwNumberOfBytesTransfered);
}

void FileBufferOverlapped::CompletedCallback(DWORD dwErrorCode, DWORD dwNumberOfBytesTransfered) const
{
	_completed = true;
	if (dwErrorCode!=0 || dwNumberOfBytesTransfered!=_buffer.Size())
	{
		_buffer.Delete();
	}
}

void FileBufferOverlapped::Open( HANDLE fileHandle, int start=0, int size=INT_MAX )
{
	_fileHandle = fileHandle;
	int fileSize = ::GetFileSize(fileHandle,NULL);

	saturate(start,0,fileSize);
	saturate(size,0,fileSize-start);

	// start overlapped reading
	_buffer.Resize(size);
	if (!_compressed) _uncompressedSize = size;

	_data.Offset = start;
	_data.OffsetHigh = 0;
	_data.hEvent = this;

	BOOL ok = ReadFileEx
	(
		_fileHandle,_buffer.Data(),_buffer.Size(),&_data,FileBufferOverlappedCompleted
	);
	if (!ok)
	{
		HRESULT hr = GetLastError();
		_completed = true;
		_buffer.Release();
	}
}

FileBufferOverlapped::FileBufferOverlapped
(
	HANDLE file, int uncompressedSize, int start, int size
)
{
	_size = 0;
	_fileHandle = NULL;
	_completed = false;
	_compressed = true;
	_uncompressedSize = uncompressedSize;

	if (size<=0) return; // zero sized - no data
	
	Open(file,start,size);
}

FileBufferOverlapped::FileBufferOverlapped( HANDLE file, int start, int size)
{
	_size = 0;
	_fileHandle = NULL;
	_completed = false;
	_compressed = false;
	_uncompressedSize = size;

	if (size<=0) return; // zero sized - no data
	
	Open(file,start,size);
}

bool FileBufferOverlapped::IsDone() const
{
	if (!_completed)
	{
		SleepEx(0,TRUE);
	}
	return _completed;
}

int FileBufferOverlapped::GetSize() const
{
	if (_compressed)
	{
		return _uncompressedSize;
	}
	return _buffer.Size();
}

const char *FileBufferOverlapped::GetData() const
{
	// wait until overlapped function completed
	while (!_completed)
	{
		SleepEx(0,TRUE);
	}
	if (_compressed)
	{
		Buffer<char> uncompressed;
		uncompressed.Resize(_uncompressedSize);
		SSCompress ss;
		QIStream in(_buffer.Data(),_buffer.Size());
		ss.Decode(uncompressed.Data(),uncompressed.Size(),in);
		// TODO: imp
		_buffer.Init(uncompressed.Data(),uncompressed.Size());
	}
	return _buffer.Data();
}

FileBufferOverlapped::~FileBufferOverlapped()
{
	// cancel IO
	#ifndef _XBOX
	CancelIo(_fileHandle);
	#endif
	_buffer.Release();
	_completed = true;
}

/*!
\patch_internal 1.11 Date 07/30/2001 by Ondra
- Fixed: when file mapping failed, application may crash.
*/

bool FileBufferOverlapped::GetError() const
{
	return _buffer.Size()==0;
}

bool FileBufferOverlapped::IsReady() const
{
	return IsDone();
}



bool FileBufferOverlapped::IsFromBank(QFBank *bank) const
{
	return bank->BufferOwned(this);	
}

HANDLE FileBufferOverlapped::GetFileHandle() const
{
	return _fileHandle;
}


