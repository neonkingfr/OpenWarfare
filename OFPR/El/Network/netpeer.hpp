#ifdef _MSC_VER
#  pragma once
#endif

/**
    @file   netpeer.hpp
    @brief  Network peer object (pilot implementation using UDP)

    Copyright &copy; 2001-2002 by BIStudio (www.bistudio.com)
    @author PE
    @since  18.11.2001
    @date   16.6.2002
*/

#ifndef _NETPEER_H
#define _NETPEER_H

//------------------------------------------------------------
//  Support:

/**
    Retrieves the local network address.
    @param  me <code>sockaddr_in</code> structure to be filled.
    @param  port Port number to be used.
    @return <code>true</code> if succeeded.
*/
bool getLocalAddress ( struct sockaddr_in &me, unsigned16 port );

/**
    Retrieves name of the local host.
    @param  name Buffer to hold the result.
    @param  len Buffer length.
    @return <code>true</code> if succeeded.
*/
bool getLocalName ( char *name, unsigned len );

/**
    Fills-in the <code>sockaddr_in</code> address from string.
    @param  host <code>sockaddr_in</code> structure to be filled.
    @param  ip String representation of network address.
    @param  port Port number.
    @return <code>true</code> if succeeded.
*/
bool getHostAddress ( struct sockaddr_in &host, const char *ip, unsigned16 port );

//------------------------------------------------------------
//  NetPeerUDP class:

/// Size of sliding window for I/O peer/channel statistics. Number of items = SLIDING_WINDOW / 2.
const int SLIDING_WINDOW = 64;

/// Different sliding-window size for sent messages.
const int SLIDING_WINDOW_SEND = 256;

/**
    Net-peer class. If opened it is bound to the specific
    UDP port and does all (duplex) communication through that port
    (the whole family of [localIP:port]-[*:*] requests).
    <p>Individual I/O requests are processed by net-channels.
    Each net-channel object is responsible for respective
    [localIP:port]-[distantIP:port] communication.
    @since  18.11.2001
    @see    NetChannel
*/
class NetPeerUDP : public NetPeer {

protected:

    /// channel routing table - mapping:  [distantIP:port] -> NetChannel
    ImplicitMap<unsigned64,RefD<NetChannel>,channelKey,true,MemAllocSafe> chMap;

    /// shared SOCKET handle
    SOCKET sock;

    /// listener thread
    ThreadId listener;

    /// sender thread
    ThreadId sender;

    /// is this port in 'listening' state?
    bool listen;

    /// is this port in 'sending' state?
    bool sending;

    friend THREAD_PROC_RETURN THREAD_PROC_MODE udpListen ( void *param );
    friend THREAD_PROC_RETURN THREAD_PROC_MODE udpSend ( void *param );

    /// Reconnects the associated port after the WSAECONNRESET error..
    void reconnect ();

    /// The peer is being reconnected.
    bool reconnecting;

public:

    /**
        Default constructor. For service use only.
        @param  _pool Net-pool to work with
    */
    NetPeerUDP ( NetPool *_pool );

    /**
        Default constructor - does initialization.
        @param  _sock Socket handle (with successfully finished <code>bind()</code> operation).
        @param  _port Port number.
        @param  _pool Net-pool to work with
    */
    NetPeerUDP ( SOCKET _sock, unsigned16 _port, NetPool *_pool );

    /**
        Retrieves local network address (peer).
        @param  local Buffer the result will be filled in
    */
    virtual void getLocalAddress ( struct sockaddr_in &local ) const;

    /// Returns maximum data length for communication through this net-peer
    virtual unsigned maxMessageData () const;

    /**
        Registers a new net-channel into the net-peer.
        <p>There shouldn't be two channels sharing the same distant network address and port number.
        @param  distant Regularly filled <code>sockaddr_in</code> structure containing
                        address of distant peer
        @param  ch Net-channel bound to the given address
        @return <code>true</code> if succeeded
    */
    virtual bool registerChannel ( struct sockaddr_in &distant, NetChannel *ch );

    /**
        Unregisters the given net-channel.
        @param  ch Previously registered net-channel
    */
    virtual void unregisterChannel ( NetChannel *ch );

    /**
        Looks for a net-channel bound to the given network address.
        @param  distant Regularly filled <code>sockaddr_in</code> structure containing
                        address of distant peer
        @return Reference to a suitable net-channel (<code>NULL</code> if failed)
    */
    virtual NetChannel *findChannel ( const struct sockaddr_in &distant );

    /// Closes the net-peer including all associates channels.
    virtual void close ();

    //  network I/O:

    /**
        Process the given incoming data.
        Asynchronously called by the listener thread. Must not call channel-processing,
        used for peer statistics only.
        @param  hdr Header that was received (includes message length,
                    continues with message data itself).
        @param  distant IP address data came from.
    */
    virtual void processData ( MsgHeader *hdr, const struct sockaddr_in &distant );

    /**
        Sends the given data to the given network address.
        @param  hdr Prepared message header (includes message length,
                    continues with message data itself).
        @param  distant IP destination address (can be <code>Zero()</code> for
                        broadcast).
    */
    virtual NetStatus sendData ( MsgHeader *hdr, struct sockaddr_in distant );

    /**
        Cancels all pending messages (already dispatched for delivery).
    */
    virtual void cancelAllMessages ();

    /**
        Initializes dispatcher status. Dispatcher takes care of all messages waiting for departure.
        This method is used for status initializing.
        @param  data Buffer to receive dispatcher statistics. If it is <code>NULL</code>, only the required data-length is returned.
        @return Required length of <code>data</code> struct.
    */
    virtual unsigned initDispatcherStatus ( DispatcherStatus *data );

    virtual ~NetPeerUDP ();

    };

#endif
