#ifdef _MSC_VER
#  pragma once
#endif

/*
    @file   peerfactory.hpp
    @brief  Net-peer factory creating NetPeerUDP instances.
    
    Copyright &copy; 2001-2002 by BIStudio (www.bistudio.com)
    @author PE
    @since  3.12.2001
    @date   16.6.2002
*/

#ifndef _PEERFACTORY_H
#define _PEERFACTORY_H

//------------------------------------------------------------
//  PeerChannelFactoryUDP class:

/**
    Factory for creating new NetPeer instances.
*/
class PeerChannelFactoryUDP : public PeerChannelFactory {

protected:

    static int instances;                   ///< number of instances (for WSAStartup)

public:

    PeerChannelFactoryUDP ();

    /**
        Creates a new net-peer.
        @param  pool Network pool to work with.
        @param  tryPorts Local ports to try (if <code>NULL</code>, use <code>pool->getLocalPorts()</code>).
        @return Reference to the new peer (<code>NULL</code> if failed)
    */
    virtual NetPeer *createPeer ( NetPool *pool, BitMask *tryPorts );

    /**
        Creates a new net-channel.
        @param  pool Network pool to work with.
        @param  control Create control channel?
        @return Reference to the new channel (<code>NULL</code> if failed).
    */
    virtual NetChannel *createChannel ( NetPool *pool, bool control =false );

    virtual ~PeerChannelFactoryUDP ();

    };


#endif
