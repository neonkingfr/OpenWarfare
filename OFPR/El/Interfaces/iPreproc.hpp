#ifdef _MSC_VER
#pragma once
#endif

#ifndef _I_PREPROC_HPP
#define _I_PREPROC_HPP

#include <El/QStream/QBStream.hpp>

//! class of callback functions
class PreprocessorFunctions
{
public:
	PreprocessorFunctions() {}
	virtual ~PreprocessorFunctions() {}

	//! callback function to preprocess of stream content
	virtual bool Preprocess(QOStream &out, const char *name)
	{
		if (!QIFStreamB::FileExist(name))
			return false;
		
		QIFStreamB in;
		in.AutoOpen(name);
		out.write(in.act(),in.rest());
		return true;
	}
};

#endif
