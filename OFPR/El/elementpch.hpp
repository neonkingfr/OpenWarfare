// precompiled header files - many files use something of Windows API
#include "PCH/ext_options.hpp"

// platform-dependent definitions
#include <Es/essencepch.hpp>

#include <Es/Types/enum_decl.hpp>

#ifndef _ENABLE_REPORT
#error _ENABLE_REPORT must be defined for Elements
/*
_ENABLE_REPORT is defined by Essence header, which are always used when using Element
*/
#endif

#if _SUPER_RELEASE
	#include "PCH/sReleaseConfig.h"
#elif _USE_AFX
	#include "PCH/afxConfig.h"
#else
	#include "PCH/normalConfig.h"
#endif

#include "PCH/stdIncludes.h"

// global support for array new/delete
/*
// init_seg user is default
#if _MSC_VER
	#pragma init_seg(user)
#endif
*/

void *ArrayNew( size_t size );
void *ArrayNew( size_t size, const char *file, int line );
void ArrayDelete( void *mem );


#define WIN32_LEAN_AND_MEAN // no OLE or other complicated stuff
#define STRICT 1


// user general include
#include "PCH/libIncludes.hpp"

// some basic types

// enum declaration seems not to be supported by some compilers
DECL_ENUM(LSError)

class ParamArchive;
// interface for classes with serialization
class SerializeClass
{
public:
	//! if this function return true, no serialization is necessary
	virtual bool IsDefaultValue(ParamArchive &ar) const {return false;}
	//! serialize all values into current class
	virtual LSError Serialize(ParamArchive &ar) = NULL;
	//! if IsDefaultValue returned true during Save, no class is saved
	//! and LoadDefaultValues is used instead of Serialize during Load
	virtual void LoadDefaultValues(ParamArchive &ar) {}
};

// I_AM_ALIVE default (empty) implementation
class GlobalAliveInterface
{
public:
	virtual void Alive() {}
	static void Set(GlobalAliveInterface *functor);
};
void GlobalAlive();
#define I_AM_ALIVE() GlobalAlive()
