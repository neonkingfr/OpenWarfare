#include <El/elementpch.hpp>
#include <El/QStream/QBStream.hpp>
#include <ctype.h>
#include "stringtable.hpp"

struct StringTableItem
{
	RString name;
	RString value;

	StringTableItem() {}
	StringTableItem(RString n, RString v) {name = n; value = v;}
	const char *GetKey() const {return name;}
};
TypeIsMovableZeroed(StringTableItem)

typedef MapStringToClass< StringTableItem, AutoArray<StringTableItem> > StringTableMap;

class StringTableDynamic : public StringTableMap
{
	typedef StringTableMap base;
public:
	void Init();
	void Add(RString name, RString value);
	void Load(const char *filename);
};

void StringTableDynamic::Init()
{
	base::Clear();
}

void StringTableDynamic::Add(RString name, RString value)
{
	// avoid duplicity in names
	const StringTableItem &check = Get(name);
	if (!IsNull(check))
	{
		RptF("Item %s listed twice", (const char *)name);
		return;
	}

	base::Add(StringTableItem(name, value));
}


/* replaced by   QIStream::nextLine()
   8/27/2002 Pepca

void NextLine(QIStream &f)
{
	while (!f.eof() && f.get() != 0x0D);
	if (!f.eof() && f.get() != 0x0A) f.unget();
}
*/

RString ReadColumn(QIStream &f)
{
	int c = f.get();
	if (c < 0) return "";
        // skip leading spaces:
	while (c != 0x0D && c != 0x0A && isspace(c))
	{
		c = f.get();
		if (c < 0) return "";
	}
	char buffer[4096];
	int i = 0;
	if (c == '\"')
	{
		c = f.get();
		if (c < 0) return "";
		while (true)
		{
			if (c == '\"')
			{
				c = f.get();
				if (c < 0)
				{
					buffer[i] = 0;
					return buffer;
				}
				if (c != '"')
				{
					while (c != 0x0D && c != 0x0A && c != ',')		
					{
						c = f.get();
						if (c < 0)
						{
							buffer[i] = 0;
							return buffer;
						}
					}
					if (c == 0x0D || c == 0x0A) f.unget();
					if (i == 0) return "";
					buffer[i] = 0;
					return buffer;
				}
			}
			if (i < sizeof(buffer) - 1) buffer[i++] = c;
			c = f.get();
			if (c < 0)
			{
				Assert(i > 0);
				buffer[i] = 0;
				return buffer;
			}
		}
		// Unaccessible
	}
	else
	{
		while (c != 0x0D && c != 0x0A && c != ',')		
		{
			if (i < sizeof(buffer) - 1) buffer[i++] = c;
			c = f.get();
			if (c < 0) return buffer;
		}
		if (c == 0x0D || c == 0x0A) f.unget();
		if (i == 0) return "";
		buffer[i] = 0;
		return buffer;
	}
}

void StringTableDynamic::Load(const char *filename)
{
	QIFStreamB f;
	f.AutoOpen(filename);
	int column = -1;
	while (true)
	{
		if (f.eof()) return;
		RString name = ReadColumn(f);
		if (name && stricmp(name, "LANGUAGE") == 0)
		{
			RString value = ReadColumn(f);
			int c = 0;
			while (value.GetLength() > 0)
			{
				if (stricmp(value, GLanguage) == 0)
				{
					column = c;
					break;
				}
				value = ReadColumn(f);
				c++;
			}
			f.nextLine();
			break;
		}
		else
			f.nextLine();
	}
	if (column < 0)
	{
		RptF("Unsupported language %s in %s",(const char *)GLanguage,filename);
		column = 0;
	}

	while (!f.eof())
	{
		RString name = ReadColumn(f);
		if (name.GetLength()<=0 || stricmp(name, "COMMENT") == 0)
		{
			f.nextLine();
			continue;
		}
		for (int c=0; c<column; c++) ReadColumn(f);
		RString value = ReadColumn(f);
		if (value) Add(name, value);
		f.nextLine();
	}
}

class StringTable
{
protected:
	// TODO: priority array of StringTableDynamic
	StringTableDynamic _tableGlobal;
	StringTableDynamic _tableCampaign;
	StringTableDynamic _tableMission;
	AutoArray<StringTableItem> _registered;

public:
	StringTable() {}
	void Clear();

	void Load(RString type, RString filename, float priority, bool init);
	
	int Register(RString name);
	
	RString Localize(int ids);
	RString Localize(const char *str);
};

void StringTable::Clear()
{
	_tableGlobal.Clear();
	_tableCampaign.Clear();
	_tableMission.Clear();
	_registered.Clear();
}

void StringTable::Load(RString type, RString filename, float priority, bool init)
{
	StringTableDynamic *table = NULL;
	if (stricmp(type, "global") == 0) table = &_tableGlobal;
	else if (stricmp(type, "campaign") == 0) table = &_tableCampaign;
	else if (stricmp(type, "mission") == 0) table = &_tableMission;
	else
	{
		RptF("Unknown stringtable type %s", (const char *)type);
		return;
	}

	if (init) table->Init();
	if (QIFStreamB::FileExist(filename)) table->Load(filename);
}

int StringTable::Register(RString name)
{
	// only strings from global stringtable can be registered
	const StringTableItem &item = _tableGlobal[name];
	if (_tableGlobal.IsNull(item))
	{
		RptF("Cannot register unknown string %s", (const char *)name);
		return -1;
	}
	return _registered.Add(item);
}

RString StringTable::Localize(int ids)
{
	if (ids < 0 || ids >= _registered.Size())
	{
		ErrF("String id %d is not registered", ids);
#if _ENABLE_CHEATS
		return "!!! UNREGISTERED STRING";
#else
		return "";
#endif
	}
	return _registered[ids].value;
}

RString StringTable::Localize(const char *str)
{
	const StringTableItem &item1 = _tableMission[str];
	if (!_tableMission.IsNull(item1)) return item1.value;

	const StringTableItem &item2 = _tableCampaign[str];
	if (!_tableCampaign.IsNull(item2)) return item2.value;

	const StringTableItem &item3 = _tableGlobal[str];
	if (!_tableGlobal.IsNull(item3)) return item3.value;

	RptF("String %s not found", (const char *)str);
#if _ENABLE_CHEATS
	return "!!! MISSING STRING";
#else
	return "";
#endif
}

RString GLanguage;
StringTable GStringTable;

void LoadStringtable(RString type, RString filename, float priority, bool init)
{
	GStringTable.Load(type, filename, priority, init);
}

int RegisterString(RString name)
{
	return GStringTable.Register(name);
}

RString LocalizeString(int ids)
{
	return GStringTable.Localize(ids);
}

RString LocalizeString(const char *str)
{
	return GStringTable.Localize(str);
}

RString Localize(RString str)
{
	if (str[0] == '@') return LocalizeString((const char *)str + 1);
	return str;
}

void ClearStringtable()
{
	GStringTable.Clear();
}
