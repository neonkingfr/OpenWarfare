#ifdef _MSC_VER
#pragma once
#endif

#ifndef _STRINGTABLE_HPP
#define _STRINGTABLE_HPP

#include <Es/Strings/rString.hpp>

extern RString GLanguage;

void LoadStringtable(RString type, RString filename, float priority = 0, bool init = true);

int RegisterString(RString name);

RString LocalizeString(int ids);
RString LocalizeString(const char *str);

RString Localize(RString str);

void ClearStringtable();

#define DEFINE_STRING(name)			extern int IDS_##name;
#define DECLARE_STRING(name)		int IDS_##name;
#define REGISTER_STRING(name) \
	int RegisterString(RString name); \
	IDS_##name = RegisterString("STR_"#name);

#endif
