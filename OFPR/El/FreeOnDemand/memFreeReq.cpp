#include "El/elementpch.hpp"
#include "memFreeReq.hpp"

/*!
\patch_internal 1.44 Date 2/13/2002 by Ondra
- New: Various caches deallocated when memory allocation is not successful.
*/

size_t MemoryFreeOnDemandHelper::Free(size_t ammount)
{
	// free first candidate
	size_t freedTotal = 0;
	for(;;)
	{
		size_t freed = FreeOneItem();
		if (freed==0) break;
		freedTotal += freed;
		if (ammount<=freedTotal) break;
	}
	return freedTotal;
}

size_t MemoryFreeOnDemandHelper::FreeAll()
{
	return Free(UINT_MAX);
}

void MemoryFreeOnDemandList::Register(IMemoryFreeOnDemand *object)
{
	// _freeOnDemand list is maintained sorted by priority
	// lowest priority first
	// find suitable position to insert this object
	for(IMemoryFreeOnDemand *walk=Start(); NotEnd(walk); walk = Advance(walk))
	{
		if (object->Priority()<walk->Priority())
		{
			InsertBefore(walk,object);
			return;
		}
	}

	// no existing object has higher priority
	// we have the highest and we need to be added at the list end
	Add(object);
}

size_t MemoryFreeOnDemandList::Free(size_t ammount)
{
	size_t freedTotal = 0;
	for(IMemoryFreeOnDemand *walk=Start(); NotEnd(walk); walk = Advance(walk))
	{
		size_t freed = walk->Free(ammount);
		freedTotal += freed;
		#if _ENABLE_REPORT
		if (freed>0)
		{
			LogF("%x:%s: Flush of %d B",walk,typeid(*walk).name(),freed);
		}
		#endif
		if (ammount<=freedTotal) break;
		// if we freed enough memory, we can stop freeing it
	}

	return freedTotal;
}

size_t MemoryFreeOnDemandList::FreeAll()
{
	int freedTotal = 0;
	for(IMemoryFreeOnDemand *walk=Start(); NotEnd(walk); walk = Advance(walk))
	{
		int freed = walk->FreeAll();
		freedTotal += freed;
	}

	return freedTotal;
}
