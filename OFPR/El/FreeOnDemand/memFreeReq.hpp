#ifdef _MSC_VER
#pragma once
#endif

#ifndef _MEM_FREE_REQ_HPP
#define _MEM_FREE_REQ_HPP

#include <Es/Containers/cachelist.hpp>

//! interface for on-demand deallocator (any class that is able to free memory on demand)
/*!
 this is used when memory allocation fails to gain any memory
 that may be used by variaos caches
 each MemHeap maintains list of objects that are able to free some memory in it
*/

class IMemoryFreeOnDemand: public CLDLink
{
	public:
	//! try to free given ammount of memory
	/*!
	\param ammount ammount of memory that needs to be freed
	\return ammount of memory actually freed
	*/
	virtual size_t Free(size_t ammount) = 0;
	//! try to free as much memory as possible
	/*!
	\return ammount of memory actually freed
	*/
	virtual size_t FreeAll() = 0;
	//! estimate cost associated with recreating objects freed
	/*
	lowest priority objects are freed first.
	Static property: used when on-demand deallocator is registered.
	\return estimated number of CPU cycles required to recreate one byte of controled objects
	*/
	virtual float Priority() = 0;

	//! virtual destructor
	virtual ~IMemoryFreeOnDemand(){}
};

//! list of objects that are able to free memory
/*!
this list is maintained sorted by priority (lowest first)
*/

class MemoryFreeOnDemandList: public CLList<IMemoryFreeOnDemand>
{
	public:
	void Register(IMemoryFreeOnDemand *object);
	size_t Free(size_t ammount);
	size_t FreeAll();
};

class MemoryFreeOnDemandHelper: public IMemoryFreeOnDemand
{
	// typical implementation frame
	public:
	//! free one item you (preferably the cheapest one)
	virtual size_t FreeOneItem() = 0;

	//! using FreeOneItem we implement some functions of IMemoryFreeOnDemand
	virtual size_t Free(size_t ammount);
	virtual size_t FreeAll();

};

extern void RegisterMemoryFreeOnDemand(IMemoryFreeOnDemand *object);

#endif
