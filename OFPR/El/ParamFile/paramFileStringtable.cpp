// Add stringtable functionality to ParamFile
// - must be in main project (not in lib)

#include <El/elementpch.hpp>
#include "paramFile.hpp"
#include <El/Stringtable/stringtable.hpp>

//! class of callback functions
static class StringtableFunctions : public LocalizeStringFunctions
{
public:
	//! callback function to load string from stringtable
	virtual RString LocalizeString(const char *str);

	StringtableFunctions() {ParamFile::SetDefaultLocalizeStringFunctions(this);}
} GStringtableFunctions;

void InitParamFileStringtable()
{
	volatile void *ptr = &GStringtableFunctions;
}

RString StringtableFunctions::LocalizeString(const char *str)
{
	return ::LocalizeString(str);
}



