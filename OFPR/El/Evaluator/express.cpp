#include <El/elementpch.hpp>

#include "express.hpp"
#include <ctype.h>
#include <stdarg.h>
#include <stdlib.h>
#include <Es/Strings/bstring.hpp>

/*
#ifndef ACCESS_ONLY
#include "paramFile.hpp"
#include "paramArchive.hpp"
#endif
*/

#include <El/Common/randomGen.hpp>
#include <El/Common/perfLog.hpp>

#ifndef CHECK
#define CHECK(command) {LSError err = command; if (err != 0) return err;}
#endif

static ArchiveFunctions GArchiveFunctions;
ArchiveFunctions *GameState::_defaultArchiveFunctions = &GArchiveFunctions;

static GameStateInfoFunctions GGameStateInfoFunctions;
GameStateInfoFunctions *GameState::_defaultInfoFunctions = &GGameStateInfoFunctions;

DEFINE_FAST_ALLOCATOR(GameDataNil)
DEFINE_FAST_ALLOCATOR(GameDataScalar)
DEFINE_FAST_ALLOCATOR(GameDataBool)
DEFINE_FAST_ALLOCATOR(GameDataNothing)
DEFINE_FAST_ALLOCATOR(GameDataString)
DEFINE_FAST_ALLOCATOR(GameDataArray)
DEFINE_FAST_ALLOCATOR(GameEvaluator)

static GameValue GameValueNil(new GameDataNil(GameVoid));

#define MAX_EXPR_LEN 1024

#define lenof(arr) ( sizeof(arr)/sizeof(*arr) )

#ifdef _WIN32
#define M_PI 3.1415926536
#endif
#define NOTHING GameValue()


// muzeme pouzivat slozky pole vysledky od 0 do Ind-1

/*!
\patch 1.41 Date 1/2/2002 by Jirka
- Improved: Expression evaluation optimized (faster script execution).
*/

void GameState::SetError(EvalError error, ...) const
{
	// chyba na pozici _e->_pos
	_e->_error=error;
	if (error==EvalOK)
	{
		_e->_errorText = RString();
		return;
	}
	#ifndef ACCESS_ONLY
	const char *errorText = _defaultInfoFunctions->GetErrorString(error);
	// LoadString( IDS_EVAL_GEN + error - EvalGen );
	BString<MAX_EXPR_LEN> formated;
	// pass any addional string that were specified

	va_list va;
	va_start(va,error); 
	vsprintf((char *)(const char *)formated,errorText,va);
	va_end(va);

	_e->_errorText = formated.cstr();
	_e->_errorCarretPos = _e->_pos-_e->_pos0;

	#endif
}

void GameState::TypeError(GameType exp, GameType was, const char *name) const
{
	char expName[512];
	char wasName[512];
	strcpy(expName,GetTypeName(exp));
	strcpy(wasName,GetTypeName(was));
	SetError(EvalType,wasName,expName);
	if (name)
	{
		// append name before error message
		RString withName = name + RString(": ") + _e->_errorText;
		_e->_errorText = withName;
	}
}

void GameState::vynech() const
{
	while (isspace(*_e->_pos)) _e->_pos++;
	//while( (*_e->_pos==' ' || *_e->_pos=='\t') ) _e->_pos++;
}

Real GameState::sejmid() const
{

	//const char *Po1;
	//const char *Po2;
	//const struct lconv *Tecka;
	char *end;
	Real cisl = strtod(_e->_pos, &end);
	if (end == _e->_pos)
	{
		SetError(EvalNum);
		return 0;
	}
	_e->_pos = end;
	return cisl;

/*	
	int i, Fg;
	
	Po1=_e->_pos;
	cisl=0;
	Fg=0;
	while( *_e->_pos>='0' && *_e->_pos<='9' )
	{
		Fg=1;
		cisl*=10;
		cisl+= (Real) *_e->_pos-'0';
		_e->_pos++;
	}
	if( *_e->_pos=='.' )
	{
		i=0;
		_e->_pos++;
		while( *_e->_pos>='0' && *_e->_pos<='9' )
		{
			Fg=1;
			i++;
			cisl+=(*_e->_pos - '0')/pow(10,i);
			_e->_pos++;
		}
	}
	if( Fg && (*_e->_pos=='e' || *_e->_pos=='E') )
	{
		int c=0, zn;
		
		_e->_pos++;
		zn=1;
		switch (*_e->_pos)
		{
		case '-':
			zn=-1;
			_e->_pos++;
			break;
		case '+':
			_e->_pos++;
			break;
		}
		Po2=_e->_pos;
		while( *_e->_pos>='0' && *_e->_pos<='9' )
		{
			c*=10;
			c+= *_e->_pos-'0';
			_e->_pos++;
		}
		if( Po2==_e->_pos )
		{
			SetError(EvalExpo);
			return 0;
		}
		if( c>DBL_MAX_10_EXP || c<DBL_MIN_10_EXP)
		{
			SetError(EvalExpo);
			return 0;
		}
		cisl=cisl*pow(10,c*zn);
	}
	if( Po1==_e->_pos ) SetError(EvalNum);
	return cisl;
*/
}

inline bool isalphaext( char c )
{
	return isalpha(c) || c=='_';
}

inline bool isalnumext( char c )
{
	return isalnum(c) || c=='_';
}

#define OPEN_STRING '{'
#define CLOSE_STRING '}'

/*!
\patch 1.43 Date 1/31/2002 by Ondra
- New: Scripting: Two quote marks encountered inside of string are parsed as quote mark.
\patch 1.82 Date 8/12/2002 by Ondra
- New: Scripting: Curled braces { and } can be used to enclose string constants.
*/

GameValue GameState::Const() const
{
	//GameValue vrt;
	vynech();
	//if( isalpha(_e->_pos[0]) && !isalpha(_e->_pos[1])) /* promenna */
	if( isalphaext(_e->_pos[0]) ) // variable
	{
		// scan variable name
		char name[MAX_EXPR_LEN];
		char *wName=name;
		int i = 0;
		while( isalnumext(_e->_pos[0]) )
		{
			if (i<MAX_EXPR_LEN-1) *wName++=*_e->_pos, i++;
			_e->_pos++;
		}
		*wName=0;
		strlwr(name);
		// scan nular operators 
		if( *name=='_' )
		{
			// local variable
			GameVarSpace *space=_e->local;
			if( !space )
			{
				SetError(EvalNamespace);
				return GameValue();
			}
			GameValue value;
			space->VarGet(name,value);
			return value;
		}
		if (_e->_checkOnly || IsNull(_vars[name]))
		{
			const GameNular &o = _nulars[name];
			if (_nulars.NotNull(o))
			{
				if( _e->_checkOnly )
				{
					return GameValue(new GameDataNil(o._retType));
				}
				else
				{
					return (*o._proc)(this);
				}
			}
		}

		if( !VarGoodName(name) )
		{
			SetError(EvalBadVar);
			return GameValue(); 
		}
		if( _e->_checkOnly )
		{
			return CreateGameValue(GameVoid);
		}
		return VarGet(name);
	}
	else if (*_e->_pos=='"')
	{
		// string
		const char *start=++_e->_pos;
		RString string = "";
		for(;;)
		{
			while( *_e->_pos!='"' )
			{
				if( !*_e->_pos )
				{
					SetError(EvalNum); // TODO: EvalMissing"
					return RString();
				}
				_e->_pos++;
			}
			// string from start to _e->_pos
			string = string + RString(start,_e->_pos-start);
			// check if another quote mark is encountered
			// if yes, append it, and continue parsing, otherwise we are done
			++_e->_pos;
			if (*_e->_pos=='"')
			{
				string = string + RString("\"");
				++_e->_pos;
				start = _e->_pos;
			}
			else
			{
				break;
			}
		}

		return string;
	}
	else if (_e->_pos[0]==OPEN_STRING)
	{
		// special case of string value
		const char *start = ++_e->_pos;
		bool closed = false;
		const char *end = start;
		int level = 1;
		for(;;)
		{
			// note: any quotes inside may prevent closing bracket
			if (*end=='"')
			{
				end++;
				// skip anything until another quote mark
				while (*end!=0 && *end!='"')
				{
					end++;
				}
				if (*end!='"')
				{
					SetError(EvalCloseB);
					return RString();
				}
			}
			else if (*end==OPEN_STRING)
			{
				level++;
			}
			else if (*end==CLOSE_STRING)
			{
				if (--level==0)
				{
					closed = true;
					break;
				}
			}
			else if (*end==0) break;
			end++;
		}
		if (closed)
		{
			_e->_pos = end+1;
			return RString(start,end-start);
		}
		else
		{
			SetError(EvalCloseB);
			return RString();
		}
	}
	else if( _e->_pos[0]=='0' && _e->_pos[1]=='x')
	{
		bool Fg=false;
		int vrt=0;
		_e->_pos += 2;
		while( isxdigit(*_e->_pos) )
		{
			Fg=true;
			vrt*=16;
			if( isdigit(*_e->_pos) ) vrt+=*_e->_pos-'0';
			else vrt+=toupper(*_e->_pos)-('A'-10);
			_e->_pos++;
		}
		if( !Fg ) SetError(EvalNum);
		return float(vrt);
	}
	else if( *_e->_pos=='$' ) /* hex */
	{
		bool Fg=false;
		int vrt=0;
		_e->_pos++;
		while( isxdigit(*_e->_pos) )
		{
			Fg=true;
			vrt*=16;
			if( isdigit(*_e->_pos) ) vrt+=*_e->_pos-'0';
			else vrt+=toupper(*_e->_pos)-('A'-10);
			_e->_pos++;
		}
		if( !Fg ) SetError(EvalNum);
		return float(vrt);
	}
	else return sejmid(); 
}

static GameValue Soucet( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	return (float)oper1+(float)oper2;
}
static GameValue Rozdil( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	return (float)oper1-(float)oper2;
}
static GameValue Soucin( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	return (float)oper1*(float)oper2;
}
static GameValue Podil(const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	if( (float)oper2==0 )
	{
		state->SetError(EvalDivZero);
		return 0.0f;
	}
	return (float)oper1/(float)oper2;
}
static GameValue Zbytek( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	if( (float)oper2==0 )
	{
		state->SetError(EvalDivZero);
		return 0.0f;
	}
	return (float)fmod((float)oper1,(float)oper2);
}
static GameValue Na( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	return (float)pow((float)oper1, (float)oper2);
}

static GameValue CmpE( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	return (float)oper1==(float)oper2;
}
static GameValue CmpL( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	return (float)oper1<(float)oper2;
}
static GameValue CmpG( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	return (float)oper1>(float)oper2;
}
static GameValue CmpLE( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	return (float)oper1<=(float)oper2;
}
static GameValue CmpGE( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	return (float)oper1>=(float)oper2;
}
static GameValue CmpNE( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	return (float)oper1!=(float)oper2;
}

static GameValue StrCmpNE( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	return strcmpi((GameStringType)oper1,(GameStringType)oper2)!=0;
}

static GameValue StrCmpE( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	return strcmpi((GameStringType)oper1,(GameStringType)oper2)==0;
}

static GameValue VoidCmpNE( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	return !oper1.IsEqualTo(oper2);
}

static GameValue VoidCmpE( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	return oper1.IsEqualTo(oper2);
}

static GameValue StrAdd( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	//GameValue o1=oper1;
	//RStringT s1=(const char *)(o1);
	return (GameStringType)oper1+(GameStringType)oper2;
}

static GameValue ListAdd( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	// make return object to avoid unneccessary copy
	GameValue retValue(new GameDataArray);
	GameArrayType &ret=retValue;

	const GameArrayType &add1=oper1;
	const GameArrayType &add2=oper2;
	ret.Realloc(add1.Size()+add2.Size());

	for( int i=0; i<add1.Size(); i++ )
	{
		ret.Add(add1[i]);
	}
	for( int i=0; i<add2.Size(); i++ )
	{
		ret.Add(add2[i]);
	}
	return retValue;
}

static GameValue ListSub( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	// make return object to avoid unneccessary copy
	GameValue retValue(new GameDataArray);
	GameArrayType &ret=retValue;

	const GameArrayType &op1=oper1;
	const GameArrayType &op2=oper2;
	ret.Realloc(op1.Size());

	for( int i=0; i<op1.Size(); i++ )
	{
		// check if it is member of op2
		bool isInOp2 = false;
		for (int j=0; j<op2.Size(); j++)
		{
			if (op1[i].IsEqualTo(op2[j]))
			{
				isInOp2 = true;
				break;
			}
		}
		if (isInOp2) continue;
		// if not, add it to returned list
		ret.Add(op1[i]);
	}
	return retValue;
}


static GameValue BoolAnd( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	return bool(oper1) && bool(oper2);
}
static GameValue BoolOr( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	return bool(oper1) || bool(oper2);
}

static GameValue Plus( const GameState *state, GameValuePar oper1 )
{
	// make identical copy of argument
	return GameValue(oper1.GetData()->Clone());
}
static GameValue Minus( const GameState *state, GameValuePar oper1 )
{
	return -(float)oper1;
}
static GameValue Stupne( const GameState *state, GameValuePar oper1 )
{
	return (float)(2*M_PI*(float)oper1/360);
}
static GameValue Radian( const GameState *state, GameValuePar oper1 )
{
	return (float)((float)oper1*360/(2*M_PI));
}

static GameValue Random( const GameState *state, GameValuePar oper1 )
{
	float value=GRandGen.RandomValue();
	return (float)(value*(float)oper1);
}

static GameValue Sinus( const GameState *state, GameValuePar oper1 )
{
	return (float)sin(Stupne(state, oper1));
}
static GameValue Cosinus( const GameState *state, GameValuePar oper1 )
{
	return (float)cos(Stupne(state, (float)oper1));
}
static GameValue Tangens( const GameState *state, GameValuePar oper1 )
{
	return (float)tan(Stupne(state, (float)oper1));
}
static GameValue ASinus( const GameState *state, GameValuePar oper1 )
{
	return Radian(state, (float)asin((float)oper1));
}
static GameValue ACosinus( const GameState *state, GameValuePar oper1 )
{
	return Radian(state, (float)acos((float)oper1));
}
static GameValue ATangens( const GameState *state, GameValuePar oper1 )
{
	return Radian(state, (float)atan((float)oper1));
}
static GameValue Atan2( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	if( (float)oper1==0 && (float)oper2==0 )
	{
		state->SetError(EvalDivZero);
		return 0.0f;
	}
	return Radian(state, (float)atan2((float)oper1, (float)oper2) );
}
static GameValue FLog( const GameState *state, GameValuePar oper1 )
{
	return (float)log10((float)oper1);
}
static GameValue FLn( const GameState *state, GameValuePar oper1 )
{
	return (float)log((float)oper1);
}
static GameValue Exp( const GameState *state, GameValuePar oper1 )
{
	return (float)exp((float)oper1);
}
static GameValue Odmocnina( const GameState *state, GameValuePar oper1 )
{
	return (float)sqrt((float)oper1);
}

static GameValue Abs( const GameState *state, GameValuePar oper1 )
{
	return (float)fabs((float)oper1);
}

static GameValue BoolNot( const GameState *state, GameValuePar oper1 )
{
	return !bool(oper1);
}

static GameValue VoidNil(const GameState *state)
{
	return GameValue(new GameDataNil(GameVoid));
}

static GameValue BoolTrue(const GameState *state)
{
	return true;
}
static GameValue BoolFalse(const GameState *state)
{
	return false;
}

static GameValue ScalarPi(const GameState *state)
{
	return float(M_PI);
}

static GameValue ListCount( const GameState *state, GameValuePar oper1 )
{
	const GameArrayType &array=oper1;
	return (float)array.Size();
}

static GameValue ListSelect( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	const GameArrayType &array=oper1;
	float index = oper2;
	int sel = toInt(index);
	if( sel<0 || sel>=array.Size() )
	{
		if( sel!=array.Size() )
		{
			state->SetError(EvalDivZero);
		}
		return GameValue();
	}
	return array[sel];
}

/*!
\patch 1.44 Date 2/11/2002 by Ondra
- New: New scripting function: array resize newSize.
*/

static GameValue ListResize( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	if (oper1.GetReadOnly())
	{
		state->SetError(EvalBadVar);
		return NOTHING;
	}
	const GameArrayType &array=oper1;
	float index = oper2;
	int sel = toInt(index);
	GameArrayType &arrayNC = const_cast<GameArrayType &>(array);
	arrayNC.Resize(sel);
	return NOTHING;
}

static bool CheckArrayInValue(const GameArrayType &array1, const GameValue &value)
{
	if (value.GetType()!=GameArray) return false;
	const GameArrayType &array2 = value;
	if (&array1==&array2) return true;
	for (int i=0; i<array2.Size(); i++)
	{
		if (CheckArrayInValue(array1,array2[i])) return true;
	}
	return false;
}

/*!
\patch 1.44 Date 2/11/2002 by Ondra
- New: New scripting function: array set [index,value].
\patch_internal 1.80 Date 8/6/2002 by Ondra
- Fixed: Crash when array set [-1,value] was used.
*/

static GameValue ListSet( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	const GameArrayType &array=oper1;
	const GameArrayType &arg=oper2;
	if (arg.Size()!=2)
	{
		state->SetError(EvalDim,array.Size(),2);
		return NOTHING;
	}
	if (arg[0].GetType()!=GameScalar)
	{
		state->TypeError(GameScalar,arg[0].GetType());
		return NOTHING;
	}

	float index = arg[0];
	const GameValue &value = arg[1];
	int sel = toInt(index);
	if (sel<0)
	{
		state->SetError(EvalDivZero);
		return NOTHING;
	}

	if (oper1.GetReadOnly())
	{
		state->SetError(EvalBadVar);
		return NOTHING;
	}
	GameArrayType &arrayNC = const_cast<GameArrayType &>(array);
	// resize array as necessary
	arrayNC.Access(sel);
	// if value contains array, recursive array would be created
	// as this would lead to crashes, it need to be prevented
	if (CheckArrayInValue(array,value))
	{
		Fail("Recursive array");
		state->SetError(EvalDim);
		return NOTHING;
	}

	// set given value
	arrayNC[sel] = value;
	
	return NOTHING;
}

/*!
\patch 1.82 Date 8/9/2002 by Ondra
- New: Scripting: Added Functions:
	call <string>,
	if <bool> then <string> else <string>,
	while <string> do <string>.
*/

static GameValue StringCall( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	GameVarSpace local(state->GetContext());
	state->BeginContext(&local);
	state->VarSetLocal("_this",oper1,true);
	RString expression=oper2;
	GameValue ret = state->EvaluateMultiple(expression);
	state->EndContext();
	return ret;
}

/*!
\patch 1.82 Date 8/12/2002 by Ondra
- New: Scripting: Function "private" introduces local variable in the innermost scope.
*/

static GameValue StringLocal(const GameState *state, GameValuePar oper1)
{
	if (oper1.GetType()==GameString)
	{
		RString varName = oper1;
		if (varName[0]!='_')
		{
			state->SetError(EvalNamespace);
			return NOTHING;
		}
		const_cast<GameState *>(state)->VarLocal(varName);
	}
	else
	{
		const GameArrayType &array = oper1;
		for (int i=0; i<array.Size(); i++)
		{
			if (array[i].GetType()!=GameString)
			{
				state->TypeError(GameString,array[i].GetType());
				break;
			}
			RString varName = array[i];
			if (varName[0]!='_')
			{
				state->SetError(EvalNamespace);
				return NOTHING;
			}
			const_cast<GameState *>(state)->VarLocal(varName);
			
		}
	}
	return NOTHING;
}

static GameValue StringCallNoArg( const GameState *state, GameValuePar oper1)
{
	GameVarSpace local(state->GetContext());
	state->BeginContext(&local);
	RString expression=oper1;
	GameValue ret = state->EvaluateMultiple(expression);
	state->EndContext();
	return ret;
}

static GameValue StringIgnore( const GameState *state, GameValuePar oper1)
{
	return NOTHING;
}

static GameValue StringRepeat( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	int maxIters = 10000;
	for (int i=0; ; i++)
	{
		if (i>=maxIters)
		{
			RptF("Max iteration count exceeded in while loop");
			state->SetError(EvalGen);
			break;
		}
		// test condition
		{
			GameVarSpace local(state->GetContext());
			state->BeginContext(&local);
			RString expression=oper1;


			GameValue value = state->EvaluateMultiple(expression);
			bool test = false;
			if (value.GetType()&GameBool)
			{
				test = bool(value);
			}
			else
			{
				state->TypeError(GameBool,value.GetType());
			}

			state->EndContext();
			if (!test) break;
		}
		// perform loop body
		{
			GameVarSpace local(state->GetContext());
			state->BeginContext(&local);
			RString expression=oper2;
			const_cast<GameState *>(state)->Execute(expression);
			state->EndContext();
		}
	}
	return NOTHING;
}

static GameValue StringElse( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	GameValue arrayVal = state->CreateGameValue(GameArray);
	GameArrayType &array = arrayVal;
	array.Realloc(2);
	array.Append() = oper1;
	array.Append() = oper2;
	return arrayVal;
}

static GameValue IfBool( const GameState *state, GameValuePar oper1)
{
	bool value = oper1;
	return GameValue(new GameDataIf(value));
}

static GameValue WhileString( const GameState *state, GameValuePar oper1)
{
	GameStringType value = oper1;
	return GameValue(new GameDataWhile(value));
}

static GameValue StringThen( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	bool test = oper1;
	if (test)
	{
		GameVarSpace local(state->GetContext());
		state->BeginContext(&local);
		RString expression=oper2;
		GameValue ret = state->EvaluateMultiple(expression);
		state->EndContext();
		return ret;
	}
	return NOTHING;
}

static GameValue ArrayThen( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	bool test = oper1;
	const GameArrayType &array = oper2;

	if (array.Size()!=2)
	{
		state->SetError(EvalDim,array.Size(),2);
		return NOTHING;
	}
	if (array[0].GetType()!=GameString)
	{
		state->TypeError(GameScalar,array[0].GetType());
		return NOTHING;
	}
	if (array[1].GetType()!=GameString)
	{
		state->TypeError(GameScalar,array[1].GetType());
		return NOTHING;
	}

	GameVarSpace local(state->GetContext());
	state->BeginContext(&local);

	RString expression = test ? array[0] : array[1];
	GameValue ret = state->EvaluateMultiple(expression);
	state->EndContext();
	return ret;
}

static GameValue StringEval( const GameState *state, GameValuePar oper1)
{
	GameVarSpace local(state->GetContext());
	state->BeginContext(&local);
	RString expression=oper1;
	GameValue ret = state->Evaluate(expression);
	state->EndContext();
	return ret;
}

static GameValue ListCountCond( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	GameVarSpace local(state->GetContext());
	int count=0;
	state->BeginContext(&local);
	RString expression=oper1;
	const GameArrayType &array=oper2;
	for( int i=0; i<array.Size(); i++ )
	{
		const GameValue &val=array[i];
		// set local varible - will be deleted on EndContext
		state->VarSetLocal("_x",val,true);
		if( state->EvaluateBool(expression) ) count++;
		if( state->GetLastError() )
		{
			state->EndContext();
			return GameValue();
		}
	}
	state->EndContext();
	return (float)count;
}

static GameValue ListForEach( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	GameVarSpace local(state->GetContext());
	state->BeginContext(&local);
	RString expression=oper1;
	const GameArrayType &array=oper2;
	for( int i=0; i<array.Size(); i++ )
	{
		const GameValue &val=array[i];
		// set local varible - will be deleted on EndContext
		state->VarSetLocal("_x",val,true);
		const_cast<GameState *>(state)->Execute(expression);
		if( state->GetLastError() )
		{
			state->EndContext();
			return GameValue();
		}
	}
	state->EndContext();
	return NOTHING;
}

static GameValue ListIsIn( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	const GameArrayType &array=oper2;
	for( int i=0; i<array.Size(); i++ )
	{
		const GameValue &val=array[i];
		if (val.IsEqualTo(oper1)) return true;
	}
	return false;
}

/*!
\patch 1.97 Date 4/1/2004 by Jirka
- Added: function: array find item.
*/
static GameValue ListFind( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
	const GameArrayType &array=oper1;
	for( int i=0; i<array.Size(); i++ )
	{
		const GameValue &val=array[i];
		if (val.IsEqualTo(oper2)) return (float)i;
	}
	return -1.0f;
}

static GameOperator DefaultBinary[]=
{
	GameOperator( GameScalar, "^", mocnina, Na, GameScalar, GameScalar ),
	GameOperator( GameScalar, "*", soucin, Soucin, GameScalar, GameScalar ),
	GameOperator( GameScalar, "/", soucin, Podil, GameScalar, GameScalar ),
	GameOperator( GameScalar, "%", soucin, Zbytek, GameScalar, GameScalar ),
	GameOperator( GameScalar, "mod", soucin, Zbytek, GameScalar, GameScalar ),
	GameOperator( GameScalar, "atan2", soucin, Atan2, GameScalar, GameScalar ),
	GameOperator( GameScalar, "+", soucet, Soucet, GameScalar, GameScalar ),
	GameOperator( GameScalar, "-", soucet, Rozdil, GameScalar, GameScalar ),
	GameOperator( GameBool, ">=", compare, CmpGE, GameScalar, GameScalar ),
	GameOperator( GameBool, "<=", compare, CmpLE, GameScalar, GameScalar ),
	GameOperator( GameBool, ">", compare, CmpG, GameScalar, GameScalar ),
	GameOperator( GameBool, "<", compare, CmpL, GameScalar, GameScalar ),
	GameOperator( GameBool, "==", compare, CmpE, GameScalar, GameScalar ),
	GameOperator( GameBool, "!=", compare, CmpNE, GameScalar, GameScalar ),

	GameOperator( GameBool, "==", compare, StrCmpE, GameString, GameString ),
	GameOperator( GameBool, "!=", compare, StrCmpNE, GameString, GameString ),

	GameOperator( GameString, "+", soucet, StrAdd, GameString, GameString ),
	GameOperator( GameArray, "+", soucet, ListAdd, GameArray, GameArray ),
	GameOperator( GameArray, "-", soucet, ListSub, GameArray, GameArray ),

	GameOperator( GameBool, "&&", logicAnd, BoolAnd, GameBool, GameBool ),
	GameOperator( GameBool, "and", logicAnd, BoolAnd, GameBool, GameBool ),
	GameOperator( GameBool, "||", logicOr, BoolOr, GameBool, GameBool ),
	GameOperator( GameBool, "or", logicOr, BoolOr, GameBool, GameBool ),

	GameOperator( GameVoid, "select", function, ListSelect, GameArray, GameScalar ),
	GameOperator( GameVoid, "select", function, ListSelect, GameArray, GameBool ),
	GameOperator( GameNothing, "set", function, ListSet, GameArray, GameArray ),
	GameOperator( GameNothing, "resize", function, ListResize, GameArray, GameScalar ),
	GameOperator( GameScalar, "count", function, ListCountCond, GameString, GameArray ),
	GameOperator( GameNothing, "forEach", function, ListForEach, GameString, GameArray ),
	GameOperator( GameBool, "in", function, ListIsIn, GameVoid, GameArray ),
	GameOperator( GameScalar, "find", function, ListFind, GameArray, GameVoid ),

	GameOperator( GameAny, "then", function, StringThen, GameIf, GameString ),
	GameOperator( GameAny, "then", function, ArrayThen, GameIf, GameArray ),
	GameOperator( GameArray, "else", functionFirst, StringElse, GameString, GameString ),
	GameOperator( GameNothing, "do", function, StringRepeat, GameWhile, GameString ),

	GameOperator( GameAny, "call", function, StringCall, GameVoid, GameString ),	
};

static GameFunction DefaultUnary[]=
{
	GameFunction( GameScalar, "sin", Sinus, GameScalar ),
	GameFunction( GameScalar, "random", Random, GameScalar ),
	GameFunction( GameScalar, "cos", Cosinus, GameScalar ),
	GameFunction( GameScalar, "tg", Tangens, GameScalar ),
	GameFunction( GameScalar, "tan", Tangens, GameScalar ),
	GameFunction( GameScalar, "asin", ASinus, GameScalar ),
	GameFunction( GameScalar, "acos", ACosinus, GameScalar ),
	GameFunction( GameScalar, "atg", ATangens, GameScalar ),
	GameFunction( GameScalar, "atan", ATangens, GameScalar ),
	GameFunction( GameScalar, "deg", Radian, GameScalar ),
	GameFunction( GameScalar, "rad", Stupne, GameScalar ),
	GameFunction( GameScalar, "log", FLog, GameScalar ),
	GameFunction( GameScalar, "ln", FLn, GameScalar ),
	GameFunction( GameScalar, "exp", Exp, GameScalar ),
	GameFunction( GameScalar, "sqrt", Odmocnina, GameScalar ),
	GameFunction( GameScalar, "abs", Abs, GameScalar ),
	GameFunction( GameScalar, "+", Plus, GameScalar ), // known special cases
	GameFunction( GameArray, "+", Plus, GameArray ), // replication
	GameFunction( GameScalar, "-", Minus, GameScalar ),
	GameFunction( GameBool, "!", BoolNot, GameBool ),
	GameFunction( GameBool, "not", BoolNot, GameBool ),

	GameFunction( GameScalar, "count", ListCount, GameArray ),
	GameFunction( GameAny, "call", StringCallNoArg, GameString ),	
	GameFunction( GameNothing, "comment", StringIgnore, GameString ),	
	GameFunction( GameNothing, "private", StringLocal, GameString|GameArray ),	

	GameFunction( GameIf, "if", IfBool, GameBool ),
	GameFunction( GameWhile, "while", WhileString, GameString ),
};

static GameNular DefaultNular[]=
{
	GameNular( GameVoid, "nil", VoidNil ),
	GameNular( GameBool, "true", BoolTrue ),
	GameNular( GameBool, "false", BoolFalse ),
	GameNular( GameScalar, "pi", ScalarPi )
};

static const RString BinOp="<bin>";
static const RString LstOp="<lst>";

void GameState::TypeErrorOperator(const char *name, GameType left, GameType right) const
{
	// check which types are accepted as operands
	GameType leftE = GameType(0);
	GameType rightE = GameType(0);
	const GameOperators &operators = _operators[name];
	if (!_operators.IsNull(operators))
	{
		for (int i=0; i<operators.Size(); i++)
		{
			leftE |= operators[i]._argType1;
			rightE |= operators[i]._argType2;
		}
	}
	if ((leftE&left)!=left)
	{
		TypeError(leftE,left,name);
	}
	else if((rightE&right)!=right)
	{
		TypeError(rightE,right,name);
	}
	else
	{
		SetError(EvalGen);
	}
}

void GameState::TypeErrorFunction(const char *name, GameType right) const
{
	// check which types are accepted as operands
	GameType rightE = GameType(0);
	const GameFunctions &functions = _functions[name];
	if (!_functions.IsNull(functions))
	{
		for (int i=0; i<functions.Size(); i++)
			rightE |= functions[i]._argType;
	}
	if((rightE&right)!=right)
	{
		TypeError(rightE,right,name);
	}
	else
	{
		SetError(EvalGen);
	}
}

/*!
\patch 1.50 Date 4/16/2002 by Ondra
- New: Scripting language hiding rules changed.
User defined variables now hide functions with identical name.
This ensures old scripts / conditions will certainly work
even when new functions conflict with names of existing variables.
*/

void GameState::VyhCast( int Prio ) const
{
	while( _e->SP>0 )
	{
		RString oper1=_e->_operStack[_e->SP-1];
		if( oper1==BinOp )
		{
			if( _e->SP<=1 ) return;
			RString oper2=_e->_operStack[_e->SP-2];
			if( oper2==BinOp || oper2==LstOp ) return;
			if( Prio>_e->_priorStack[_e->SP-2] ) return;
			// binary
			Assert( _e->UB[_e->SP-1]==-1 );
			if( _e->UB[_e->SP-2]==2 )
			{
				// find function with appropriate name
				
				if( !_e->_checkOnly )
				{
					const GameOperator *op = NULL;

					if (IsNull(_vars[oper2]))
					{
						const GameOperators &operators = _operators[oper2];
						if (!_operators.IsNull(operators))
						{
							for (int i=0; i<operators.Size(); i++)
							{
								const GameOperator &o = operators[i];
								if(!(o._argType1 & _e->_stack[_e->SP - 2].GetType())) continue;
								if(!(o._argType2 & _e->_stack[_e->SP - 1].GetType())) continue;
								op = &o;
								break;
							}
						}
					}

					if (!op)
					{
						TypeErrorOperator
						(
							oper2,
							_e->_stack[_e->SP-2].GetType(),
							_e->_stack[_e->SP-1].GetType()
						);
						return;
					}

					if( _e->_stack[_e->SP-2].GetNil() || _e->_stack[_e->SP-1].GetNil() )
					{
						_e->_stack[_e->SP-2]=GameValue( new GameDataNil(op->_retType) );
					}
					else
					{
						_e->_stack[_e->SP-2]=(*op->_proc)(this, _e->_stack[_e->SP-2], _e->_stack[_e->SP-1]);
					}
				}
				else
				{

					GameType possible(0);
					const GameOperators &operators = _operators[oper2];
					if (!_operators.IsNull(operators))
					{
						for (int i=0; i<operators.Size(); i++)
						{
							const GameOperator &o = operators[i];
							if(!(o._argType1 & _e->_stack[_e->SP - 2].GetType())) continue;
							if(!(o._argType2 & _e->_stack[_e->SP - 1].GetType())) continue;
							possible |= o._retType;
						}
					}

					if( possible==0 )
					{
						TypeErrorOperator
						(
							oper2,
							_e->_stack[_e->SP-2].GetType(),
							_e->_stack[_e->SP-1].GetType()
						);
						return;
					}

					_e->_stack[_e->SP-2]=GameValue(new GameDataNil(possible));
				}
			}
			else
			{
				Assert( _e->UB[_e->SP-2]==1 );
				if( !_e->_checkOnly )
				{
					const GameFunction *op = NULL;
					
					if( IsNull(_vars[oper2]) )
					{
						const GameFunctions &functions = _functions[oper2];
						if (!_functions.IsNull(functions))
						{
							for (int i=0; i<functions.Size(); i++)
							{
								const GameFunction &f = functions[i];
								if (f._argType & _e->_stack[_e->SP-1].GetType())
								{
									op = &f;
									break;
								}
							}
						}
					}

					if (!op)
					{
						TypeErrorFunction
						(
							oper2,
							_e->_stack[_e->SP-1].GetType()
						);
						return;
					}

					if( _e->_stack[_e->SP-1].GetNil() )
					{
						_e->_stack[_e->SP-2]=GameValue( new GameDataNil(op->_retType) );
					}
					else
					{
						_e->_stack[_e->SP-2]=(*op->_proc)(this, _e->_stack[_e->SP-1]);
					}
				}
				else
				{
					GameType possible(0);
					const GameFunctions &functions = _functions[oper2];
					if (!_functions.IsNull(functions))
					{
						for (int i=0; i<functions.Size(); i++)
						{
							const GameFunction &f = functions[i];
							if (f._argType & _e->_stack[_e->SP-1].GetType())
								possible |= f._retType;
						}
					}

					if( possible==0 )
					{
						TypeErrorFunction
						(
							oper2,
							_e->_stack[_e->SP-1].GetType()
						);
						return;
					}
					_e->_stack[_e->SP-2]=GameValue(new GameDataNil(possible));
				}
			}
			_e->_operStack[_e->SP-2]=BinOp;
			_e->_priorStack[_e->SP-2]=_e->_parPrior+_e->_listPrior;
			_e->UB[_e->SP-2]=-1;
			// remove argument
			_e->_stack[_e->SP-1]=GameValue();
			_e->SP--;
		}
		else
		{
			Assert( oper1==LstOp ); 
			// list
			return;
		}
	}
	if( _e->_error ) return;
}

inline int CMP( const char *Ps, const char *Ps1 )
{
	return strnicmp(Ps,Ps1,strlen(Ps1));
}

inline int CMPN( const char *Ps, const char *Ps1, int n )
{
	return strnicmp(Ps,Ps1,n);
}

void GameState::CleanStack() const
{
	// remove all values from the stack
	while (_e->SP>0)
	{
		_e->_stack[--_e->SP] = GameValue();
	}
}

/*!
\patch 1.43 Date 1/31/2002 by Ondra
- Fixed: Scripting: Array parsing inside of brackets was wrong.
Example of wrong expression: (+[0])
*/

GameValue GameState::Vyhod() const
{
//	int Op2;
	const GameFunctions *functions;
	const GameOperators *operators;
	_e->_parPrior=0;
	_e->_listPrior=0;
	_e->SP=0;
	_e->_error=EvalOK;
	_e->_errorText = RString();
	if (*_e->_pos==';' || *_e->_pos==0)
	{
		// detect empty expression
		return NOTHING;
	}
	for(;;)
	{
		vynech();
		if( *_e->_pos=='(' )
		{
			_e->_pos++;
			_e->_parPrior+=zavorky;
			continue;
		}
		if( *_e->_pos=='[' )
		{
			// evaluate list - no recursion?			
			_e->_pos++;
			vynech();
			if( *_e->_pos==']' )
			{
				// empty list construction
				_e->_pos++;

				if (_e->SP>=SL)
				{
					SetError(EvalLineLong);
					return NOTHING;
				}
				_e->_stack[_e->SP]=GameArrayType();
				_e->_priorStack[_e->SP]=-1;
				_e->UB[_e->SP]=-1;
				_e->_operStack[_e->SP]=BinOp;
				_e->SP++;

				goto CekejBinar;
			}
			else
			{
				_e->_listPrior+=zavorky;
			}
			continue;
		}
		// check unary operator - 
		functions = NULL;
		if( isalpha(*_e->_pos) )
		{
			// scan identifier
			int idlen=0;
			while( isalnumext(_e->_pos[idlen]) ) idlen++;

			RString name(_e->_pos, idlen);
			name.Lower();

			if( _e->_checkOnly || IsNull(_vars[name]) )
			{
				functions = &_functions[name];
			}
		}
		else
		{
			RString name2(_e->_pos, 2);
			functions = &_functions[name2];
			if (_functions.IsNull(*functions))
			{
				RString name1(_e->_pos, 1);
				functions = &_functions[name1];
			}
		}
		if (!functions || _functions.IsNull(*functions))
		{
			if (_e->SP>=SL)
			{
				SetError(EvalLineLong);
				return NOTHING;
			}
			// value on stack top
			_e->_stack[_e->SP]=Const();
			_e->_priorStack[_e->SP]=-1;
			_e->UB[_e->SP]=-1;
			_e->_operStack[_e->SP]=BinOp;
			_e->SP++;
			
			if( _e->_error ) return NOTHING;
CekejBinar:
			vynech();
			switch( *_e->_pos )
			{
			case ',':
			{
				if (_e->_listPrior<=0)
				{
					if (_e->_checkOnly)
					{
						SetError(EvalOpenB);
						return 0.0f;
					}
					goto Semicolon;
				}
				_e->_pos++;

				VyhCast( _e->_parPrior+_e->_listPrior );

				if( _e->_error ) return NOTHING;
				// leave value on stack and continue with evaluation
				_e->_operStack[_e->SP-1]=LstOp; // no op
				_e->UB[_e->SP-1]=3; // list element
				_e->_priorStack[_e->SP-1]=_e->_parPrior+_e->_listPrior;

				break;
			}
			case ']':
			{
				_e->_pos++;

				VyhCast( _e->_parPrior+_e->_listPrior );
				if( _e->_error ) return NOTHING;

				// change value to list value
				_e->_operStack[_e->SP-1]=LstOp; // no op
				_e->UB[_e->SP-1]=3; // list element
				_e->_priorStack[_e->SP-1]=_e->_parPrior+_e->_listPrior;
				// leave value on stack and continue with evaluation

				_e->_listPrior-=zavorky;
				if( _e->_listPrior<0 )
				{
					SetError(EvalOpenB);
					return 0.0f;
				}
				// pop all arguments with prior>=_listPrior
				GameValue arrayVal = CreateGameValue(GameArray);
				GameArrayType &array = arrayVal;
				
				int SPP=_e->SP;
				while
				(
					SPP>0 &&
					_e->_priorStack[SPP-1]>=_e->_listPrior+_e->_parPrior+zavorky
				)
				{
					Assert( _e->_operStack[SPP-1]==LstOp );
					Assert( _e->UB[SPP-1]==3 );
					SPP--;
				}
				array.Realloc(_e->SP-SPP);
				//array.Resize(_e->SP-SPP);
				//LogF("Construct array");
				for( int i=SPP; i<_e->SP; i++ )
				{
					//LogF("  %s",(const char *)(_e->_stack[i].GetText()));
					array.Add(_e->_stack[i]);
				}
				_e->SP=SPP;
				if (_e->SP>=SL)
				{
					SetError(EvalLineLong);
					return NOTHING;
				}
				// push array value
				_e->_stack[_e->SP]=arrayVal;
				_e->_priorStack[_e->SP]=-1;
				_e->UB[_e->SP]=-1;
				_e->_operStack[_e->SP]=BinOp;
				_e->SP++;
				goto CekejBinar;
			}
			case ')':
				_e->_pos++;
				_e->_parPrior-=zavorky;
				if( _e->_parPrior<0 )
				{
					SetError(EvalOpenB);
					return 0.0f;
				}
				goto CekejBinar;
			case ';':
			Semicolon:
			{
				if( _e->_parPrior!=0 )
				{
					SetError(EvalCloseB);
					return 0.0f;
				} 
				VyhCast( 0 );
				if( _e->_error ) return NOTHING;

				Assert( _e->SP==1 );
				Assert( _e->UB[_e->SP-1]==-1 );
				Assert( _e->_operStack[_e->SP-1]==BinOp );
				GameValue value = _e->_stack[--_e->SP];
				_e->_stack[_e->SP] = GameValue();
				return value;
			}
			case 0:
			{
				if( _e->_parPrior!=0 )
				{
					SetError(EvalCloseB);
					return 0.0f;
				} 
				if( _e->_listPrior!=0 )
				{
					SetError(EvalCloseB);
					return 0.0f;
				} 
				VyhCast( 0 );
				if( _e->_error ) return NOTHING;

				//LogF("Expression _e->SP %d",_e->SP);
				Assert( _e->SP==1 );
				GameValue value = _e->_stack[--_e->SP];
				_e->_stack[_e->SP] = GameValue();
				return value;
			}
			default:
				{
					// if first letter is alphaext, scan whole identifier
					int idlen=0;
					operators = NULL;
					if (isalphaext(_e->_pos[0]))
					{
						while( isalnumext(_e->_pos[idlen]) ) idlen++;

						RString name(_e->_pos, idlen);
						name.Lower();
						if (_e->_checkOnly || IsNull(_vars[name]))
						{
							operators = &_operators[name];
						}
					}
					else
					{
						RString name2(_e->_pos, 2);
						operators = &_operators[name2];
						if (_operators.IsNull(*operators))
						{
							RString name1(_e->_pos, 1);
							operators = &_operators[name1];
						}
					}

					if (!operators || _operators.IsNull(*operators))
					{
						char name[256];
						strncpy(name,_e->_pos,idlen);
						name[idlen] = 0;
						SetError(EvalOper,name);
						return NOTHING;
					}   
					_e->_pos+=strlen(operators->_name);
					int prior=operators->_priority+_e->_parPrior+_e->_listPrior;
					VyhCast( prior );
					if( _e->_error ) return NOTHING;
					_e->_operStack[_e->SP-1]=operators->_name;
					_e->UB[_e->SP-1]=2;
					_e->_priorStack[_e->SP-1]=prior;
					break;
				}
			} // switch
		} // binary or special
		else // unary operator
		{
			_e->_pos+=strlen(functions->_name);
			if (_e->SP>=SL)
			{
				SetError(EvalLineLong);
				return NOTHING;
			}
			_e->_stack[_e->SP]=0.0f; // no first argument
			_e->_operStack[_e->SP]=functions->_name;
			_e->UB[_e->SP]=1;
			_e->_priorStack[_e->SP]=unar+_e->_parPrior+_e->_listPrior;
			_e->SP++;
			// save unary operator
		}
	} // while 
	/*NOTREACHED*/
}


GameValue::GameValue()
{
	_data=NULL; //new GameDataNothing();
}

GameValue::GameValue( GameData *data )
{
	_data=data;
}

#if !ACCESS_ONLY
GameData *GameData::CreateObject(ParamArchive &ar)
{
	bool nil=false;
	if( GameState::Serialize(ar, "nil", nil, 1, false) != 0 )
	{
		return NULL;
	}
	GameType type=GameVoid;
	if( GameState::Serialize(ar, "type", type, 1 ) != 0 )
	{
		return NULL;
	}
	if( nil )
	{
		// create nil object
		return new GameDataNil(type);
	}

	return GameState::CreateGameData(ar, type);
/*
	GameState *context=reinterpret_cast<GameState *>(ar.GetParams());
	return context->CreateGameData(type);
*/
}

LSError GameData::Serialize(ParamArchive &ar)
{	
	if (GameState::IsSaving(ar))
	{
		GameType type=GetType();
		CHECK( GameState::Serialize(ar, "type", type, 1));
	}
	return (LSError)0;
}
#endif

RString GameDataNil::GetText() const
{
	// print all types included
	// TODO: how to get symbolic names?
	if( _type==~0 )
	{
		return "all";
	}
	char buf[MAX_EXPR_LEN];
	strcpy(buf,"");
	int rest=_type;
	while( rest )
	{
		if( *buf ) strcat(buf," ");
		if( rest&GameScalar ) strcat(buf,"scalar"),rest&=~GameScalar;
		else if( rest&GameBool ) strcat(buf,"bool"),rest&=~GameBool;
		else if( rest&GameArray ) strcat(buf,"array"),rest&=~GameArray;
		else if( rest&GameString ) strcat(buf,"string"),rest&=~GameString;
		else if( rest&GameNothing ) strcat(buf,"nothing"),rest&=~GameNothing;
		else
		{
			sprintf(buf+strlen(buf),"0x%x",_type);
			break;
		}
	}
	return buf;
}

bool GameDataNothing::IsEqualTo(const GameData *data) const
{
	return false; // nil is never equal to anything
	//static_cast<GameDataNil *>(data);
}

bool GameDataNil::IsEqualTo(const GameData *data) const
{
	return false; // nil is never equal to anything
	//static_cast<GameDataNil *>(data);
}

#ifndef ACCESS_ONLY
LSError GameDataNil::Serialize(ParamArchive &ar)
{
	bool nil=true;
	CHECK( GameState::Serialize(ar, "nil", nil, 1));
	// nothing but type is serialized
	CHECK( GameState::Serialize(ar, "type", _type, 1));
	return (LSError)0;
}
#endif


RString GameDataScalar::GetText() const
{
	BString<MAX_EXPR_LEN> buf;
	sprintf(buf,"%g",_value);
	return (const char *)buf;
}

bool GameDataScalar::IsEqualTo(const GameData *data) const
{
	float val1 = GetScalar();
	float val2 = data->GetScalar();
	return val1==val2;
}


#ifndef ACCESS_ONLY
LSError GameDataScalar::Serialize(ParamArchive &ar)
{
	CHECK( base::Serialize(ar) );
	CHECK( GameState::Serialize(ar, "value", _value, 1));
	return (LSError)0;
}
#endif


RString GameDataBool::GetText() const
{
	return _value ? "true" : "false";
}

bool GameDataBool::IsEqualTo(const GameData *data) const
{
	bool val1 = GetBool();
	bool val2 = data->GetBool();
	return val1==val2;
}


#ifndef ACCESS_ONLY
LSError GameDataBool::Serialize(ParamArchive &ar)
{
	CHECK( base::Serialize(ar) );
	CHECK( GameState::Serialize(ar, "value", _value, 1));
	return (LSError)0;
}
#endif

/*!
\patch_internal 1.01 Date 7/10/2001 by Jirka
 - removed text length limit when displaying variable value (internal build only)
*/
RString GameDataString::GetText() const
{
	return RString("\"") + _value + RString("\"");
}

bool GameDataString::IsEqualTo(const GameData *data) const
{
	RString val1 = GetString();
	RString val2 = data->GetString();
	return val1==val2;
}

#ifndef ACCESS_ONLY
LSError GameDataString::Serialize(ParamArchive &ar)
{
	CHECK( base::Serialize(ar) );
	CHECK( GameState::Serialize(ar, "value", _value, 1));
	return (LSError)0;
}
#endif

const GameArrayType GameArrayEmpty;
GameArrayType GameArrayDummy;

GameDataArray::GameDataArray()
:_readOnly(false)
{
}

GameDataArray::GameDataArray(const GameArrayType &value)
:_value(value),_readOnly(false)
{
}

GameDataArray::~GameDataArray()
{
}


GameData *GameDataArray::Clone() const
{
	GameDataArray *clone = new GameDataArray(*this);
	// clone can be written even if source cannot
	clone->SetReadOnly(false);
	return clone;
}

/*!
\patch_internal 1.01 Date 7/10/2001 by Jirka
 - removed text length limit when displaying variable value (internal build only)
*/
RString GameDataArray::GetText() const
{
	RString val("[");
	for (int i=0; i<_value.Size(); i++)
	{
		if (i > 0) val = val + RString(",");
		const GameValue &value = _value[i];
		if (value.GetType()==GameArray)
		{
			const GameArrayType &array = value;
			if (&array==&_value)
			{
				// prevent simple recursion
				// note: same situation may happen via an intermediate array
				// but this is almost impossible to catch
				val = val + RString("...");
				continue;
			}
		}
		val = val + _value[i].GetText();
	}
	val = val + RString("]");
	return val;
}

bool GameDataArray::IsEqualTo(const GameData *data) const
{
	const GameArrayType &val1 = GetArray();
	const GameArrayType &val2 = data->GetArray();
	// check if array is same size
	//return val1==val2;
	if (val1.Size()!=val2.Size()) return false;
	return false;
}


#ifndef ACCESS_ONLY
LSError GameDataArray::Serialize(ParamArchive &ar)
{
	CHECK( base::Serialize(ar) );
	CHECK( GameState::Serialize(ar, "value", _value, 1));
	return (LSError)0;
}
#endif

GameValue::GameValue( const GameValue &value )
{
	_data=value._data;
}

void GameValue::operator = ( const GameValue &value )
{
	_data=value._data;
}

void GameValue::Duplicate( const GameValue &value )
{
	if( !value._data )
	{
		_data=NULL;
		return;
	}
	_data=value._data->Clone();
	if( !_data )
	{
		ErrF("Data not cloned: %s",(const char *)value.GetDebugText());
		_data=new GameDataNil(GameAny);
	}
}

#ifndef ACCESS_ONLY
LSError GameValue::Serialize(ParamArchive &ar)
{
	// during serialization _data must point to valid object 
	if( !_data ) _data=new GameDataNothing;
	CHECK(GameState::Serialize(ar, "data", _data, 1));
	if (_data && _data->GetType() == GameNothing) _data=NULL;
	return (LSError)0;
}
#endif

RString GameValue::GetText() const
{
	if( !_data ) return "<null>";
	return _data->GetText();
}


RString GameValue::GetDebugText() const
{
	if( !_data ) return "<null>";
	RString type=_data->GetTypeName();
	RString text=_data->GetText();
	return type+RString(" ")+text;
}

bool GameValue::IsEqualTo(const GameValue &value) const
{
	// check type
	if (GetNil() || value.GetNil()) return false;
	if (!GetData() || !value.GetData()) return false;
	if (value.GetType()!=GetType()) return false;
	// if it is same type, call corresponding method
	return GetData()->IsEqualTo(value.GetData());
}

/*
bool GameData::IsEqualTo(const GameData &value)
{
	return false;
}
*/

void GameState::NewType( const char *name, GameType val, CreateGameDataFunction *func, RString tname )
{
	GameTypeType en;
	en.name=name;
	en.value=int(val);
	en.createFunction=func;
	en.typeName = tname;
	_typeNames.Add(en);
}

void GameState::NewNularOps( const GameNular *f, int count)
{
	for( int i=0; i<count; i++ )
	{
		GGameState.NewNularOp(f[i]);
	}
}
void GameState::NewFunctions( const GameFunction *f, int count)
{
	for( int i=0; i<count; i++ )
	{
		GGameState.NewFunction(f[i]);
	}
}
void GameState::NewOperators( const GameOperator *f, int count)
{
	for( int i=0; i<count; i++ )
	{
		GGameState.NewOperator(f[i]);
	}
}

void GameState::NewNularOp( const GameNular &f )
{
	GameNular o = f;

	GameNular &nt = _nulars[f._name];
	if (_nulars.IsNull(nt))
	{
		_nulars.Add(o);
	}
}
void GameState::NewFunction(const GameFunction &f)
{
	GameFunctions &ft = _functions[f._name];
	if (_functions.IsNull(ft))
	{
		_functions.Add(GameFunctions(f._opName));
	}
	GameFunctions &functions = _functions[f._name];
	GameFunction o = f;
	o._name = functions._name;

	int n = functions.Size();
	functions.Realloc(n + 1);
	functions.Resize(n + 1);
	functions[n] = o;
}
void GameState::NewOperator( const GameOperator &op )
{
	GameOperators &ot = _operators[op._name];
	if (_operators.IsNull(ot))
	{
		_operators.Add(GameOperators(op._opName, op._priority));
	}
	GameOperators &operators = _operators[op._name];
	GameOperator o = op;
	o._name = operators._name;

	int n = operators.Size();
	operators.Realloc(n + 1);
	operators.Resize(n + 1);
	operators[n] = o;
}


struct DicContext
{
	AutoArray<RStringS> &_dic;
	bool (*_filter)(const char *word);
	DicContext
	(
		AutoArray<RStringS> &dic,
		bool (*filter)(const char *word)
	)
	:_dic(dic),_filter(filter)
	{
	}
};

inline void AddDicFunc(RString name, DicContext &context)
{
	if (context._filter(name)) context._dic.Add(name);
}

static void AddDicFuncNul(const GameNular &op, const GameNularsType *list, void *context)
{
	AddDicFunc(op._opName,*(DicContext *)context);
}
static void AddDicFuncUn(const GameFunctions &op, const GameFunctionsType *list, void *context)
{
	AddDicFunc(op._opName,*(DicContext *)context);
}
static void AddDicFuncBin(const GameOperators &op, const GameOperatorsType *list, void *context)
{
	AddDicFunc(op._opName,*(DicContext *)context);
}

void GameState::AppendNularOpList
(
	AutoArray<RStringS> &dic, bool (*filter)(const char *word)
) const
{
	DicContext context(dic,filter);
	_nulars.ForEach(AddDicFuncNul,&context);
}
void GameState::AppendFunctionList
(
	AutoArray<RStringS> &dic, bool (*filter)(const char *word)
) const
{
	DicContext context(dic,filter);
	_functions.ForEach(AddDicFuncUn,&context);
}
void GameState::AppendOperatorList
(
	AutoArray<RStringS> &dic, bool (*filter)(const char *word)
) const
{
	DicContext context(dic,filter);
	_operators.ForEach(AddDicFuncBin,&context);
}

GameState::GameState()
{
	//Init();
	Reset();
	_actContext=-1;
	BeginContext(NULL);
}

GameState::~GameState()
{
	_vars.Clear();
	_typeNames.Clear();
	_functions.Clear();
	_operators.Clear();
	_nulars.Clear();
}

GameData *CreateGameDataScalar() {return new GameDataScalar();}
GameData *CreateGameDataBool() {return new GameDataBool();}
GameData *CreateGameDataArray() {return new GameDataArray();}
GameData *CreateGameDataString() {return new GameDataString();}
GameData *CreateGameDataNothing() {return new GameDataNothing();}
GameData *CreateGameDataIf() {return new GameDataIf();}
GameData *CreateGameDataWhile() {return new GameDataWhile();}

void GameState::Init()
{
	_typeNames.Clear();
	NewType("SCALAR",GameScalar, CreateGameDataScalar, _defaultInfoFunctions->GetTypeName(GameScalar));
	NewType("BOOL",GameBool, CreateGameDataBool, _defaultInfoFunctions->GetTypeName(GameBool));
	NewType("ARRAY",GameArray, CreateGameDataArray, _defaultInfoFunctions->GetTypeName(GameArray));
	NewType("STRING",GameString, CreateGameDataString, _defaultInfoFunctions->GetTypeName(GameString));
	NewType("NOTHING",GameNothing, CreateGameDataNothing, _defaultInfoFunctions->GetTypeName(GameNothing));

	NewType("IF",GameIf, CreateGameDataIf, _defaultInfoFunctions->GetTypeName(GameIf));
	NewType("WHILE",GameWhile, CreateGameDataWhile, _defaultInfoFunctions->GetTypeName(GameWhile));

	_functions.Clear();
	for( int i=0; i<lenof(DefaultUnary); i++ )
	{
		NewFunction(DefaultUnary[i]);
	}
	_operators.Clear();
	for( int i=0; i<lenof(DefaultBinary); i++ )
	{
		NewOperator(DefaultBinary[i]);
	}
	_nulars.Clear();
	for( int i=0; i<lenof(DefaultNular); i++ )
	{
		NewNularOp(DefaultNular[i]);
	}
}

void GameState::Reset()
{
	_vars.Clear();
	// reserved internal variables
	VarSet("this",false,true);
}


GameValue GameState::VarGet( const char *name ) const
{
	// convert name to lower case
	RString source = name;
	source.Lower();

	// first try to find local variable
	const GameVarSpace *space = _e->local;
	if (space)
	{
		GameValue ret;
		bool found = space->VarGet(source,ret);
		if (found)
		{
			return ret;
		}
	}
	else
	{
		if (source[0] == '_')
		{
			SetError(EvalNamespace);
			return GameValueNil;
		}
	}

	const GameVariable &var=_vars[source];
	if( NotNull(var) )
	{
		// get variable value
		return var._value;
	}

	return GameValueNil;
}

bool GameState::IsVisible( const GameVariable &var ) const
{
	return var.IsReadOnly();
}

/*!
\patch_internal 1.44 Date 2/11/2002 by Ondra
- Fixed: Identifiers leader, who, group, ammo and magazine
were supposed to be reserved with no reason.
*/

bool GameState::VarReadOnly( const char *name ) const
{
	if( _e->_checkOnly )
	{
		// check reserved word list
		const char *reserved[]=
		{
			"this","player",
			NULL
		};
		for( const char **res=reserved; *res; res++ )
		{
			if( !strcmp(*reserved,name) ) return true;
		}
		return false;
	}
	const GameVariable &var=_vars[name];
	if( IsNull(var) ) return false;
	return var._readOnly;
}

GameVarSpace::GameVarSpace()
:_parent(NULL)
{
}

/*
\patch 1.82 Date 8/12/2002 by Ondra
- New: Scripting: Multiple level variable scopes
(new scope in each call, if, while, forEach and count expression).
*/

GameVarSpace::GameVarSpace(GameVarSpace *parent)
:_parent(parent)
{
}

void GameVarSpace::VarLocal( const char *name)
{
	const GameVariable &var=_vars[name];
	if (NotNull(var))
	{
		return;
	}

	GameVariable newvar(name,GameValueNil);
	_vars.Add(newvar);
}

void GameVarSpace::VarSet( const char *name, GameValuePar value, bool readOnly)
{
	// seek variable in this and all parents
	const GameVarSpace *seek = this;
	GameVariable *varFound = NULL;
	const GameVarSpace *space = NULL;
	while (seek)
	{
		const GameVariable &var=seek->_vars[name];
		if (NotNull(var))
		{
			varFound = const_cast<GameVariable *>(&var);
			space = seek;
			break;
		}
		seek = seek->_parent;
	}

	// we may destroy variables only in the outermost scope
	bool destroy = !readOnly && value.GetNil() && (!space || !space->_parent);
	if (varFound)
	{
		GameVariable &var=*varFound;
		
		// set variable value
		if( !destroy )
		{
			var._value=value;
			var._readOnly=readOnly;
		}
		else
		{
			_vars.Remove(name);
		}
	}
	else
	{
		// new variable
		if( !destroy )
		{
			GameVariable newvar(name,value,readOnly);
			_vars.Add(newvar);
		}
	}
}


bool GameVarSpace::VarGet(const char *name, GameValue &ret) const
{
	const GameVarSpace *seek = this;
	while (seek)
	{
		const GameVariable &var=seek->_vars[name];
		if (NotNull(var))
		{
			ret = var._value;
			return true;
		}
		seek = seek->_parent;
	}
	ret = GameValueNil;
	return false;
}

/*!
\param name variable name
\param value variable value
\param readOnly if true, variable will be marked as read only
\param forceLocal if true, variable is created in local namespace
even if it does not start with underscore.
*/

void GameState::VarSet( const char *name, GameValuePar value, bool readOnly, bool forceLocal )
{	
	// convert name to lower case
	RString source = name;
	source.Lower();

	GameVarSpace *space=( forceLocal || *name=='_' ? _e->local : this );
	if( !space )
	{
		SetError(EvalNamespace);
		return;
	}

	space->VarSet(source,value,readOnly);
}

/*!
\patch 1.93 Date 8/29/2003 by Ondra
- Fixed: Scripting command "private" did not work when variable name contained any upper case letter.
*/

void GameState::VarLocal(const char *name)
{
	GameVarSpace *space= _e->local;
	if( !space )
	{
		SetError(EvalNamespace);
		return;
	}
  RString source = name;
  source.Lower();
  space->VarLocal(source);
}

/*!
\param name variable name
\param value variable value
\param readOnly if true, variable will be marked as read only
\param forceLocal if true, variable is created in local namespace
even if it does not start with underscore.
with const variant you can set only local variables
*/
void GameState::VarSetLocal( const char *name, GameValuePar value, bool readOnly, bool forceLocal) const
{
	// convert name to lower case
	RString source = name;
	source.Lower();

	if ( forceLocal || *name=='_')
	{
	}
	else
	{
		SetError(EvalBadVar);
		return;
	}
	GameVarSpace *space = _e->local;
	if( !space )
	{
		SetError(EvalNamespace);
		return;
	}
	space->VarLocal(source);
	space->VarSet(source,value,readOnly);
}

void GameState::VarDelete( const char *name )
{
	GameVarSpace *space=( *name=='_' ? _e->local : this );
	if( !space )
	{
		SetError(EvalNamespace);
		return;
	}
	space->_vars.Remove(name);
}

EvalError GameState::GetLastError() const
{
	return _e->_error;
}

RString GameState::GetLastErrorText() const
{
	return _e->_errorText;
}


int GameState::GetLastErrorPos() const
{
	return _e->_errorCarretPos;
}

/*!
\patch 1.33 Date 11/27/2001 by Ondra
 - Fixed: Corrupt error message when expression type was wrong in waypoint condition.
*/

// evaluate an expression
GameValue GameState::Evaluate( const char *expression ) const
{
	//ADD_COUNTER(exVal,1);

	_e->_error=EvalOK;
	_e->_errorText = RString();
	_e->_pos0 = _e->_pos = expression;
	_e->_subexpBeg = expression;
	_e->_subexpEnd = expression+strlen(expression);

	//Done[index]=false;
	GameValue result;
	if( *expression )
	{
		result=Vyhod();
		CleanStack();
		ShowError();
	}
	else result=0.0f;

	_e->_pos = NULL;
	_e->_pos0 = NULL;
	_e->_subexpBeg = NULL;
	_e->_subexpEnd = NULL;

	return result;
}

static const char *CheckAssignment(const char *p)
{
	while (*p && isalnumext(*p)) p++;
	while (*p && isspace(*p)) p++;
	if (p[0]=='=' && p[1]!='=') return p;
	return NULL;
}


/*!
\param localOnly if true, only assignment to local variables is allowed
*/

GameValue GameState::EvaluateMultiple( const char *expression, bool localOnly) const
{
	//ADD_COUNTER(exExe,1);
	// command should be one or more assignements
	// scan for variable name (all before '=')
	_e->_pos = _e->_pos0 = expression;
	_e->_subexpBeg = expression;
	_e->_subexpEnd = expression+strlen(expression);

	_e->_error=EvalOK;
	_e->_errorText = RString();

	GameValue ret = NOTHING;
	while( *_e->_pos )
	{
		if ((ret.GetType()&GameNothing)==0)
		{
			if( _e->_checkOnly )
			{
				TypeError(GameNothing,ret.GetType());
				break;
			}			
		}
		vynech();
		// check if expression is assignment
		// assignment must start with identifier
		_e->_subexpBeg = _e->_pos;
		if (CheckAssignment(_e->_pos))
		{
			if (!const_cast<GameState *>(this)->PerformAssignment(localOnly))
			{
				break;
			}
			ret = NOTHING;
		}
		else // non assignment
		{
			// check for empty command
			GameValue value=Vyhod();
			CleanStack();
			if( _e->_error ) break;

			// check expression result
			// if result is nothing it's OK
			// if result is fake type it is not OK
			ret = value;
		} // single command executed
		vynech();
		char c = *_e->_pos;
		if (c==0) break;
		if (c!=';' && (c!=',' || _e->_checkOnly))
		{
			SetError(EvalSemicolon);
			break;
		}
		_e->_pos++;
	}
	ShowError();
	// when it will go out of scope, _pos will be invalid
	_e->_pos = NULL;
	_e->_pos0 = NULL;
	_e->_subexpBeg = NULL;
	_e->_subexpEnd = NULL;
	return ret;
}

/*!
\patch_internal 1.46 Date 3/4/2002 by Jirka
- Fixed: if list of expressions occured in condition, get value of first boolean one
*/

bool GameState::EvaluateBool( const char *expression ) const
{
	GameValue value = EvaluateMultiple(expression);
	if( (value.GetType()&GameBool)==0 )
	{
		TypeError(GameBool,value.GetType());
		return false;
	}
	return bool(value);
}

bool GameState::EvaluateMultipleBool(const char *expression, bool localOnly) const
{
	GameValue value = EvaluateMultiple(expression,localOnly);
	if( value.GetType()&GameBool )
	{
		return bool(value);
	}
	TypeError(GameBool,value.GetType());
	return false;
}


bool GameState::VarGoodName( const char *name ) const
{
	// check if variable name is correct
	// check all operator names
	//if (_functions.NotNull(_functions.Get(name))) return false;
	//if (_operators.NotNull(_operators.Get(name))) return false;
	//if (_nulars.NotNull(_nulars.Get(name))) return false;
	return true;
}

bool GameState::IdtfGoodName( const char *name ) const
{
	if( !isalphaext(*name) ) return false;
	for( const char *n=name; *n; n++ )
	{
		if( !isalnumext(*n) ) return false;
	}
	return VarGoodName(name);
}

bool GameState::LValueGoodName( const char *name ) const
{
	// check if variable name is correct
	if( VarReadOnly(name) ) return false;
	return VarGoodName(name);
}

/*!
\patch 1.34 Date 12/7/2001 by Ondra
- Fixed: Error in assignment command detection.
Many command expressions containing "=" sign were considered assignment.
\patch_internal 1.44 Date 2/11/2002 by Ondra
- Fixed: ',' also accepted in expression where ';' is expected.
Some 1985 campaign expressions contained this constructed and
it was not detected as error by older expression parser version.
\param localOnly if true, only assignment to local variables is allowed
\patch_internal 1.82 Date 8/13/2002 by Ondra
- Fixed: ',' no longer accepted as valid delimiter during syntax check.
*/


bool GameState::PerformAssignment(bool localOnly)
{
	char name[MAX_EXPR_LEN];
	char *wName=name;
	if( isalphaext(*_e->_pos) )
	{
		int i=0;
		while( isalnumext(*_e->_pos) )
		{
			if (i<MAX_EXPR_LEN-1)
			{
				*wName++=*_e->_pos,i++;
			}
			_e->_pos++;
		}
	}
	*wName=0;
	if( !*name )
	{
		SetError(EvalBadVar);
		return false;
	}
	vynech();
	if( *_e->_pos!='=' )
	{
		SetError(EvalEqu);
		return false;
	}

	strlwr(name);
	if( !LValueGoodName(name) )
	{
		SetError(EvalBadVar);
		return false;
	}

	_e->_pos++;
	vynech();


	GameValue value=Vyhod();
	CleanStack();
	if( _e->_error ) return false;
	if (_e->_checkOnly)
	{
		if( value.GetType()==GameNothing )
		{
			TypeError(GameAny,value.GetType());
			return false;
		}
		if (value.GetType()&FakeTypes)
		{
			TypeError(GameAny,value.GetType());
		}
	}
	if( !_e->_checkOnly )
	{
		if( value.SharedInstance() && value.GetType()!=GameArray )
		{
			//LogF("Shared data (variable?) duplicated: '%s'",expression);
			value.Duplicate(value);
		}
		if (localOnly)
		{
			VarSetLocal(name,value);
		}
		{
			VarSet(name,value);
		}
	}
	return true;
}

// execute command
void GameState::Execute( const char *expression )
{
	_e->_error=EvalOK;
	_e->_errorText = RString();

	if (expression[0]==0)
	{
		// empty expression
		return;
	}
	//ADD_COUNTER(exExe,1);
	// command should be one or more assignements
	// scan for variable name (all before '=')
	_e->_pos = _e->_pos0 = expression;
	_e->_subexpBeg = expression;
	_e->_subexpEnd = expression+strlen(expression);

	while( *_e->_pos )
	{
		vynech();
		// check if expression is assignment
		// assignment must start with identifier
		_e->_subexpBeg = _e->_pos;
		if (CheckAssignment(_e->_pos))
		{
			if (!PerformAssignment())
			{
				break;
			}
		}
		else // non assignment
		{
			GameValue value=Vyhod();
			CleanStack();
			if( _e->_error ) break;

			// check expression result
			// if result is nothing it's OK
			if( (value.GetType()&GameNothing)==0 )
			{
				// otherwise we may issue error
				if( _e->_checkOnly )
				{
					TypeError(GameNothing,value.GetType());
					break;
				}
			}
		} // single command executed
		vynech();
		char c = *_e->_pos;
		if (c==0) break;
		if (c!=';' && (c!=',' || _e->_checkOnly))
		{
			SetError(EvalSemicolon);
			break;
		}
		_e->_pos++;
	}
	ShowError();
	_e->_pos = NULL;
	_e->_pos0 = NULL;
	_e->_subexpBeg = NULL;
	_e->_subexpEnd = NULL;
}

void GameState::ShowError() const
{
	if (_e->_error)
	{
		// show message only when running in editor
		const char *start = _e->_subexpBeg ? _e->_subexpBeg : _e->_pos0;
		const char *end = _e->_subexpEnd ? _e->_subexpEnd : _e->_pos0+strlen(_e->_pos0);
		if (end>_e->_pos+256) end = _e->_pos+256;
		RString expr(start,end-start);
		RString before(start,_e->_pos-start);
		RString after(_e->_pos,end-_e->_pos);
		RString buf = before+RString("|#|")+after;

		_defaultInfoFunctions->DisplayErrorMessage(buf, _e->_errorText);
		RptF("Error in expression <%s>",(const char *)expr);
		RptF("  Error position: <%s>",(const char *)after);
		RptF("  Error %s",(const char *)_e->_errorText);
	}
}


GameEvaluator::GameEvaluator( GameVarSpace *vars )
{
	local=vars;
}

GameEvaluator::~GameEvaluator()
{
}

GameVarSpace *GameState::GetContext() const
{
	return _e->local;
}

void GameState::BeginContext( GameVarSpace *vars ) const
{
	if( _actContext>=MaxContexts )
	{
		ErrorMessage("GameState context stack overflow");
		return;
	}
	_actContext++;
	_contextStack[_actContext]=new GameEvaluator(vars);
	_e=_contextStack[_actContext];

	_e->_error=EvalOK;
	_e->_errorText = RString();
	_e->_checkOnly=false;
	_e->_errorCarretPos = 0;
}

void GameState::EndContext() const
{
	if( _actContext<=0 )
	{
		ErrorMessage("GameState context stack underflow");
		return;
	}
	_contextStack[_actContext].Free();
	_actContext--;
	_e=_contextStack[_actContext];
}

bool GameState::CheckEvaluate( const char *expression ) const
{
	_e->_checkOnly=true;
	Evaluate(expression);
	_e->_checkOnly=false;
	bool ret=( _e->_error==EvalOK );
	return ret;
}

bool GameState::CheckEvaluateBool( const char *expression ) const
{
	_e->_checkOnly=true;
	EvaluateBool(expression);
	_e->_checkOnly=false;
	bool ret=( _e->_error==EvalOK );
	return ret;
}

bool GameState::CheckExecute( const char *expression ) const
{
	_e->_checkOnly=true;
	const_cast<GameState *>(this)->Execute(expression);
	_e->_checkOnly=false;
	bool ret=( _e->_error==EvalOK );
	return ret;
}

#ifndef ACCESS_ONLY
LSError GameVariable::Serialize(ParamArchive &ar)
{
	CHECK(GameState::Serialize(ar, "name", _name, 1))
	//CHECK(ar.Serialize("value", _value, 1))
	CHECK( _value.Serialize(ar) );
	CHECK(GameState::Serialize(ar,"readOnly", _readOnly, 1))
	return (LSError)0;
}
#endif

/*
GameData *GameState::CreateGameData( GameType type ) const
{
	if( type==GameScalar ) return new GameDataScalar();
	else if( type==GameArray ) return new GameDataArray();
	else if( type==GameBool ) return new GameDataBool();
	else if( type==GameString ) return new GameDataString();
	else if( type==GameVoid ) return new GameDataNil(GameVoid);
	else if( type==GameNothing ) return new GameDataNothing();
	else
	{
		Fail("Unknown game data type");
		return NULL;
	}
}
*/

GameData *GameState::CreateGameData( GameType type ) const
{
	// GameVoid is static type
	if (type == GameVoid ) return new GameDataNil(GameVoid);
	if (type == GameAny ) return new GameDataNil(GameAny);

	for (int i=0; i<_typeNames.Size(); i++)
		if (_typeNames[i].value == type)
		{
			if (_typeNames[i].createFunction) return _typeNames[i].createFunction();
			Fail("Missing create function");
			return NULL;
		}
	Fail("Unknown data type");
	return NULL;
}

/*
RString GameState::GetTypeName(GameType type) const
{
	return _defaultInfoFunctions->GetTypeName(type);
//	if (type==GameVoid) return LoadString(IDS_EVAL_TYPEANY);
//	RString ret="";
//	if (type&GameScalar) CatType(ret,LoadString(IDS_EVAL_TYPESCALAR));
//	if (type&GameArray) CatType(ret,LoadString(IDS_EVAL_TYPEARRAY));
//	if (type&GameBool) CatType(ret,LoadString(IDS_EVAL_TYPEBOOL));
//	if (type&GameString) CatType(ret,LoadString(IDS_EVAL_TYPESTRING));
//	if (type&GameNothing) CatType(ret,LoadString(IDS_EVAL_TYPENOTHING));
//	return ret;
}
*/

static void CatType(RString &a, RString b)
{
	if (a.GetLength()<=0) a = b;
	else if (b.GetLength()<=0) ;
	else a = a + RString(",") + b;
}

RString GameState::GetTypeName(GameType type) const
{
	// GameVoid is static type
	if (type == GameVoid) return _defaultInfoFunctions->GetTypeName(GameVoid);
	if (type == GameAny) return _defaultInfoFunctions->GetTypeName(GameVoid);
	if (type == GameNothing) return _defaultInfoFunctions->GetTypeName(GameNothing);

	RString ret = "";
	if ((type&GameAny)==GameAny)
	{
		CatType(ret, _defaultInfoFunctions->GetTypeName(GameVoid));
		type &= ~GameAny;
	}
	if ((type&GameVoid)==GameVoid)
	{
		CatType(ret, _defaultInfoFunctions->GetTypeName(GameVoid));
		type &= ~GameVoid;
	}
	for (int i=0; i<_typeNames.Size(); i++)
		if (type & (_typeNames[i].value)) CatType(ret, _typeNames[i].typeName);
	
	return ret;
}

GameValue GameState::CreateGameValue( GameType type ) const
{
	GameData *data=CreateGameData(type);
	if( !data ) return GameValue();
	return GameValue(data);
}

#ifndef ACCESS_ONLY
LSError GameState::Serialize(ParamArchive &ar)
{
/*
	void *old=ar.GetParams();
	ar.SetParams(this);
	CHECK(ar.Serialize("Variables", _vars, 1))
	ar.SetParams(old);
	return (LSError)0;
*/
	return _defaultArchiveFunctions->Serialize(ar, this);
}
#endif

GameState GGameState;
