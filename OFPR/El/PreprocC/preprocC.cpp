// Add C preprocessor functionality to ParamFile

#include <El/elementpch.hpp>

#include "preprocC.hpp"
#include "Preproc.h"

#include <El/QStream/QBStream.hpp>

class Preprocessor : public Preproc
{
protected:
	QIStream *OnEnterInclude(const char *filename)
	{
		if (!QIFStreamB::FileExist(filename))
			return NULL;
		QIFStreamB *stream = new QIFStreamB();
		stream->AutoOpen(filename);
		return stream;
	}
	void OnExitInclude(QIStream *stream)
	{
		if (stream) delete (QIFStreamB *)stream;
	}
};

bool CPreprocessorFunctions::Preprocess(QOStream &out, const char *name)
{
	Preprocessor preprocessor;
	if (!preprocessor.Process(&out, name))
	{
		ErrorMessage("Preprocessor failed on file %s - error %d.", name, preprocessor.error);
		return false;
	}
	return true;
}

