#ifdef _MSC_VER
#pragma once
#endif

#ifndef _PREPROC_C_HPP
#define _PREPROC_C_HPP

#include <El/Interfaces/iPreproc.hpp>

//! class of callback functions
class CPreprocessorFunctions : public PreprocessorFunctions
{
public:
	//! callback function to preprocess of stream content
	virtual bool Preprocess(QOStream &out, const char *name);
};

#endif
