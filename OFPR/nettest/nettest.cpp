/**
    @file   nettest.cpp
    @brief  Test program for the "network" subproject
    
    Copyright &copy; 2001-2002 by BIStudio (www.bistudio.com)
    @author PE
    @since  21.10.2001
    @date   29.12.2002
*/

//------------------------------------------------------------
//  Network layer test:

#include "El/Network/netpch.hpp"
#include "El/Network/netpeer.hpp"
#include "El/Network/peerfactory.hpp"
#include "Es/Framework/appFrame.hpp"
#include "El/Common/randomJames.h"

#ifdef _WIN32
#  include "Es/Framework/netlog.hpp"
#  include "Poseidon/lib/MemHeap.hpp"
#  include "Poseidon/lib/appFrameExt.hpp"
#  include <conio.h>
#else
#  include <signal.h>
#endif

//---------------------------------------------------------------------------
//  Support:

static RandomJames rnd;

void DDTerm ()
{
}

void WarningMessageLevel ( ErrorMessageLevel level, const char *format, va_list argptr)
{
    static int avoidRecursion = 0;
    if ( avoidRecursion > 0 ) return;
    avoidRecursion++;
    char buf[256];
    vsprintf( buf, format, argptr );
#if _ENABLE_REPORT
    ::ErrF("Warning Message: %s",(const char *)buf);
#endif
    NetLog(buf);
    avoidRecursion--;
}

#ifdef EXTERN_NET_LOG
NetLogger netLogger INIT_PRIORITY_URGENT;
#endif

#ifdef _WIN32

static char msgBuf[512];

#if _ENABLE_REPORT

void OFPFrameFunctions::LogF ( const char *format, va_list argptr )
{
    sprintf(msgBuf,"LogF: ");
    vsprintf(msgBuf+6,format,argptr);
    //NetLog(msgBuf);
}

void OFPFrameFunctions::LstF ( const char *format, va_list argptr )
{
    sprintf(msgBuf,"LstF: ");
    vsprintf(msgBuf+6,format,argptr);
    NetLog(msgBuf);
}

void OFPFrameFunctions::ErrF ( const char *format, va_list argptr )
{
    sprintf(msgBuf,"ErrF: ");
    vsprintf(msgBuf+6,format,argptr);
    NetLog(msgBuf);
}

#endif

void OFPFrameFunctions::ErrorMessage ( const char *format, va_list argptr )
{
    sprintf(msgBuf,"Error: ");
    vsprintf(msgBuf+7,format,argptr);
    NetLog(msgBuf);
}

void OFPFrameFunctions::ErrorMessage ( ErrorMessageLevel level, const char *format, va_list argptr )
{
    sprintf(msgBuf,"Error(%02d): ",(int)level);
    vsprintf(msgBuf+11,format,argptr);
    NetLog(msgBuf);
}

void OFPFrameFunctions::WarningMessage ( const char *format, va_list argptr )
{
    sprintf(msgBuf,"Warning: ");
    vsprintf(msgBuf+9,format,argptr);
    NetLog(msgBuf);
}

void OFPFrameFunctions::ShowMessage ( int timeMs, const char *msg, va_list argptr )
{
    sprintf(msgBuf,"Message(%8.3f): ",1e-3*timeMs);
    vsprintf(msgBuf+19,msg,argptr);
    NetLog(msgBuf);
}

DWORD OFPFrameFunctions::TickCount ()
{
    static unsigned64 origin = getSystemTime();
    return( (DWORD)( (getSystemTime() - origin + 500) / 1000 ) );
}

#endif

//------------------------------------------------------------
//  Global constants:

#define FIRST_PORT_TO_TRY   12121
#define NUM_PORTS_TO_TRY    20
#define PORT_INTERVAL       12

#define MAX_FILE_TRANSFER   10000000
#define MAX_BANDWIDTH        5000000
#define MIN_FPS                    3
#define MAX_FPS                  100
#define MIN_MSG_SIZE        ((int)(IP_UDP_HEADER + MSG_HEADER_LEN + sizeof(CommonPacket)))

//------------------------------------------------------------
//  State variables:

bool runServer = true;

struct sockaddr_in saddr;

unsigned16 localPort = FIRST_PORT_TO_TRY;

int clientProfile = 0;

int replyProfile = 0;

int fps = 10;

unsigned64 frameTime = 100000;

int positiveCheckLogs = 0;

//------------------------------------------------------------
//  Network packet formats:

#ifndef __GNUC__
#  pragma pack (push,netPackets,2)
#endif

struct MagicPacket {

    unsigned32 magic;                       /// Universal message tag.

    } PACKED;

struct ControlPacket : public MagicPacket {

    unsigned16 tmp;                         /// Used in connecting phase.

    unsigned32 serial;                      /// Serial number (local, 3 categories: urgent, VIM, common).

    unsigned64 timeStamp;                   /// System-time of message departure.

    } PACKED;

struct CommonPacket : public ControlPacket {

    unsigned32 prolog;                      /// Consistency-check (must confirm with last msg's epilog).

    unsigned32 epilog;                      /// Consistency-check (must confirm with next msg's prolog).

    unsigned16 size;                        /// Netto message size in bytes (data[] size).

    unsigned8 data[0];                      /// Internal message data.

    } PACKED;                               // 28 bytes w/o data

#ifndef __GNUC__
#  pragma pack (pop,netPackets)
#endif

#define MAGIC_CONNECT       0xcccc2845      // 1. server(b) <- client(c)
#define MAGIC_ACCEPT        0xaaaad4ab      // 2. server(c) -> client(c)
#define MAGIC_CONNECTED     0xeeee03f7      // 3. server(c) <- client(c)
#define MAGIC_DISCONNECT    0xddddb921      // 4. server(c) <> client(c)
#define MAGIC_COMMON        0x00009f5a      // server/client(c) <-
#define MAGIC_FILE          0xffff18e3      // server/client(c) <-

/// Time to wait in network destructors (for last message departure, in milliseconds).
#define DESTRUCT_WAIT        500
/// Maximum time to wait for MAGIC_ACCEPT response (in milliseconds).
#define ACK_TIMEOUT         8000
/// Re-send time for MAGIC_CREATE_PLAYER and MAGIC_RECONNECT_PLAYER (in milliseconds).
#define CONNECT_RESEND      2000
/// Time interval to check network response (in milliseconds).
#define NET_CHECK_WAIT       100

#define SERVER_LOOP_CYCLE   2000000
#define CLIENT_LOOP_CYCLE   2000000

//------------------------------------------------------------

enum ProfileEventType {
    peFile,                                 // file-transfer start
    peUFile,                                // urgent file-transfer start
    peParams,                               // change of profile parameters
    peDisconnect                            // quit the network connection
    };

#define PROF_PARAM_SATURATION   0x0001
#define PROF_PARAM_ENTROPY      0x0002
#define PROF_PARAM_MAX_PACKET   0x0004
#define PROF_PARAM_PORTFOLIO    0x0008
#define PROF_PARAM_TIMEOUT      0x0010

class ProfileEvent : public RefCountSafe {

public:

    double time;

    ProfileEventType type;

    unsigned fileSize;

    unsigned paramFlags;

    unsigned saturationAbs;

    double saturationRel;

    double entropy;

    unsigned maxPacket;

    double portfolioUrgent;

    double portfolioVIM;

    unsigned sendTimeout;

    RefD<ProfileEvent> next;

    ProfileEvent ();

    virtual ~ProfileEvent ();

    };

//------------------------------------------------------------

class TestProfile : public RefCountSafe {

protected:

    ///{ Initial parameters.

    unsigned saturationAbs;

    double saturationRel;

    double entropy;

    unsigned maxPacket;

    double portfolioUrgent;

    double portfolioVIM;

    unsigned sendTimeout;

    ///}

    RefD<ProfileEvent> events;

    friend class ProfileIterator;
    friend TestProfile *readProfile ();

public:

    int id;

    TestProfile ( int _id );

    virtual ~TestProfile ();

    };

//------------------------------------------------------------

struct ChannelState {                       ///< To check net-transport reliability.

    unsigned32 commonSerial;                ///< Serial number of last common message.

    unsigned32 commonProlog;                ///< Next expected message-Prolog.

    unsigned32 vimSerial;                   ///< Serial number of last VIM message.

    unsigned32 vimProlog;                   ///< Next expected message-Prolog.

    unsigned32 urgentSerial;                ///< Serial number of last urgent message.

    unsigned32 urgentProlog;                ///< Next expected message-Prolog.

    void init ()                            ///< Sets initial check-state.
    {
        commonSerial = vimSerial = urgentSerial = 0;
        commonProlog = vimProlog = urgentProlog = 0;
    }

    /// Checks the next incoming message..
    bool checkMessage ( const NetMessage *msg, unsigned64 drift );

    /// Processes the new message to be sent..
    void setMessage ( NetMessage *msg );

    };

//------------------------------------------------------------

#define MAX_STAT_QUEUE      64

class ProfileIterator : public RefCountSafe {

protected:

    RefD<TestProfile> profile;

    RefD<NetChannel> channel;

    ChannelState *chState;

    unsigned totalSent;

    ///{ Actual parameters.

    unsigned saturationAbs;

    double saturationRel;

    double entropy;

    unsigned maxPacket;

    double portfolioUrgent;

    double portfolioVIM;

    unsigned sendTimeout;

    unsigned fileToSend;

    unsigned fileUToSend;

    bool disconnect;

    RefD<ProfileEvent> nextEvent;

    ///}

    ///{ Outgoing traffic history.

    unsigned64 qTime[MAX_STAT_QUEUE];

    unsigned qVolume[MAX_STAT_QUEUE];

    /// The last written queue item.
    int qPtr;

    ///}

    void executeEvent ();

public:

    ProfileIterator ();

    virtual void init ( TestProfile *p, NetChannel *ch, ChannelState *cs );

    virtual void reset ();

    virtual bool executeTo ( double time );

    virtual void fillPacket ( void *data, unsigned size );

    virtual void getStatistics ( char *buf );

    virtual unsigned getSent ();

    virtual ~ProfileIterator ();

    };

template ExplicitMap<int,RefD<TestProfile>,true,MemAllocSafe>;
int ExplicitMapTraits<int,RefD<TestProfile> >::keyNull = -1;
int ExplicitMapTraits<int,RefD<TestProfile> >::zombie  = -2;

static ExplicitMap<int,RefD<TestProfile>,true,MemAllocSafe> profiles;

//------------------------------------------------------------

#include "Es/Memory/normalNew.hpp"

class ClientInfo : public RefCountSafe {

protected:

    /// Difference (server.time - client.time).
    unsigned64 drift;

    /// Reference time of outgoing MAGIC_ACCEPT message.
    unsigned64 acceptTime;

    /// Reference time of incoming MAGIC_CONNECTED message.
    unsigned64 connectedTime;

    /// This client is brand-new (not processed yet).
    bool newClient;

    /// Input network state.
    ChannelState inState;

    /// Output network state.
    ChannelState outState;

    /// Actual profile for outgoing messages.
    int outProfile;

    /// Current output-profile state.
    ProfileIterator outProfileIt;

    friend NetStatus ctrlReceive ( NetMessage *msg, NetStatus event, void *data );
    friend NetStatus serverReceive ( NetMessage *msg, NetStatus event, void *data );

public:

    /// Implicit client's identifier.
    int id;

    ClientInfo ( int outProf, NetChannel *ch );

    /// Checks the next incoming message..
    virtual bool checkMessage ( const NetMessage *msg );

    virtual bool executeToNow ();

    virtual double getRelativeTime () const;

    virtual unsigned getSent ();

    virtual ~ClientInfo ();

    bool operator == ( const ClientInfo& info ) const
    {
        return( id == info.id );
    }

    bool operator != ( const ClientInfo& info ) const
    {
        return( id != info.id );
    }

    /// MT-safe new operator.
    static void* operator new ( size_t size );

    /// MT-safe new operator.
    static void* operator new ( size_t size, const char *file, int line );

    /// MT-safe delete operator.
    static void operator delete ( void *mem );

#ifdef __INTEL_COMPILER

    /// MT-safe delete operator.
    static void operator delete ( void *mem, const char *file, int line );
    
#endif

    };

#include "Es/Memory/debugNew.hpp"

int clientInfoId ( RefD<ClientInfo> &info )
{
    return info->id;
}

template ExplicitMap<int,RefD<NetChannel>,true,MemAllocSafe>;
int ExplicitMapTraits<int,RefD<NetChannel> >::keyNull = -1;
int ExplicitMapTraits<int,RefD<NetChannel> >::zombie  = -2;

template ImplicitMap<int,RefD<ClientInfo>,clientInfoId,true,MemAllocSafe>;
RefD<ClientInfo> ImplicitMapTraits< RefD<ClientInfo> >::null = NULL;
RefD<ClientInfo> ImplicitMapTraits< RefD<ClientInfo> >::zombie = (ClientInfo*)1;

//------------------------------------------------------------
//  Test* classes:

struct NetworkStatistics {

    unsigned numberOfClients;

    unsigned recvBytes;

    unsigned sentBytes;

    unsigned minLatency;

    unsigned maxLatency;

    unsigned recvGood;

    unsigned totalGood;

    unsigned recvBad;

    unsigned totalBad;

    unsigned outBand;

    unsigned goodBand;

    unsigned inRBBand;

    unsigned outRBBand;

    int normalQBytes;

    int vimQBytes;

    };

class TestServer : public RefCountSafe {

protected:

    /// Mapping from user ID# to NetChannels.
    ExplicitMap<int,RefD<NetChannel>,true,MemAllocSafe> clients;

    /// Mapping from user ID# to ClientInfos.
    ImplicitMap<int,RefD<ClientInfo>,clientInfoId,true,MemAllocSafe> infos;

    /// For statistics purposes only.
    unsigned totalReceived;

    /// For statistics purposes only.
    unsigned totalSent;

    /// Time of last getNetStatistics() call.
    unsigned64 lastStatistics;

    /// Number of good messages received in the recent time interval.
    unsigned receivedGood;

    /// Number of good messages received at all.
    unsigned totalGood;

    /// Number of bad messages received in the recent time interval.
    unsigned receivedBad;

    /// Number of bad messages received at all.
    unsigned totalBad;

    /// Linked list of received messages (can be NULL).
    Ref<NetMessage> received;

    /// -> last received message.
    NetMessage *lastReceived;

    /// Critical section for received.
    PoCriticalSection rcvCs;

    /// Enter the critical section for received messages.
    inline void enterRcv ()
    { rcvCs.enter(); }

    /// Leave the critical section for received messages.
    inline void leaveRcv ()
    { rcvCs.leave(); }

    /// Linked list of sent messages (can be NULL).
    Ref<NetMessage> sent;

    /// Critical section for sent.
    PoCriticalSection sndCs;

    /// Enter the critical section for sent messages.
    inline void enterSnd ()
    { sndCs.enter(); }

    /// Leave the critical section for sent messages.
    inline void leaveSnd ()
    { sndCs.leave(); }

    friend NetStatus ctrlReceive ( NetMessage *msg, NetStatus event, void *data );
    friend NetStatus serverReceive ( NetMessage *msg, NetStatus event, void *data );

public:

    TestServer ();

    virtual bool init ();

    virtual void disconnect ( int id, bool notify );

    virtual void getNetStatistics ( NetworkStatistics &stat );

    virtual void spendTime ( unsigned64 interval );

    virtual unsigned processMessages ();

    virtual void removeReceivedMessages ();

    virtual void removeSentMessages ();

    virtual bool getFirst ( IteratorState &iterator, RefD<NetChannel> &first );

    virtual bool getNext ( IteratorState &iterator, RefD<NetChannel> &next );

    virtual ~TestServer ();

protected:

    void disconnectInternal ( int id, bool notify );

    int channelToClient ( NetChannel *ch );

    };

//------------------------------------------------------------

class TestClient : public RefCountSafe {

protected:

    /// Difference (client.time - server.time).
    unsigned64 drift;

    /// Associated communication channel (peer-to-peer point).
    RefD<NetChannel> channel;

    /// PlayerNo (playerID on the server - for reconnection purposes).
    int playerNo;

    /// MAGIC_CONNECT response state (0 .. none, -1 .. negative, 1 .. positive).
    int accepted;

    /// Time of MAGIC_CONNECT request.
    unsigned64 connectTime;

    /// Time of MAGIC_ACCEPT response.
    unsigned64 acceptTime;

    /// The session was terminated (by the server or channel drop)..
    bool terminated;

    /// Data-transfer reliability check.
    ChannelState inState;

    /// Transmitter state.
    ChannelState outState;

    /// Actual profile for outgoing messages.
    int outProfile;

    /// Current output-profile state.
    ProfileIterator outProfileIt;

    /// For statistics purposes only.
    unsigned totalReceived;

    /// For statistics purposes only.
    unsigned totalSent;

    /// Time of last getNetStatistics() call.
    unsigned64 lastStatistics;

    /// Number of good messages received in the recent time interval.
    unsigned receivedGood;

    /// Number of good messages received at all.
    unsigned totalGood;

    /// Number of bad messages received in the recent time interval.
    unsigned receivedBad;

    /// Number of bad messages received at all.
    unsigned totalBad;

    /// Linked list of received messages (can be NULL).
    Ref<NetMessage> received;

    /// -> last received message.
    NetMessage *lastReceived;

    /// Critical section for received.
    PoCriticalSection rcvCs;

    /// Enter the critical section for received messages.
    inline void enterRcv ()
    { rcvCs.enter(); }

    /// Leave the critical section for received messages.
    inline void leaveRcv ()
    { rcvCs.leave(); }

    /// Linked list of sent messages (can be NULL).
    Ref<NetMessage> sent;

    /// Critical section for sent.
    PoCriticalSection sndCs;

    /// Enter the critical section for sent messages.
    inline void enterSnd ()
    { sndCs.enter(); }

    /// Leave the critical section for sent messages.
    inline void leaveSnd ()
    { sndCs.leave(); }

    friend NetStatus clientReceive ( NetMessage *msg, NetStatus event, void *data );

public:

    TestClient ();

    virtual bool open ( struct sockaddr_in &addr, int prof, int distantProf );

    virtual unsigned processMessages ();

    virtual void removeReceivedMessages ();

    virtual void removeSentMessages ();

    virtual bool isTerminated () const;

    virtual void getNetStatistics ( NetworkStatistics &stat );

    virtual void spendTime ( unsigned64 interval );

    virtual double getRelativeTime () const;

    virtual ~TestClient ();

    };

//------------------------------------------------------------
//  Global network objects, support:

NetStatus ctrlReceive ( NetMessage *msg, NetStatus event, void *data );
NetStatus serverReceive ( NetMessage *msg, NetStatus event, void *data );
NetStatus clientReceive ( NetMessage *msg, NetStatus event, void *data );

static PoCriticalSection poolLock;          ///< locking of 'pool' & 'peer'

static RefD<NetPool> pool;                  ///< Global network pool.

static RefD<NetPeer> peer;                  ///< NetPeer for general purpose.

static NetPool *getPool ()
    // should be called inside of poolLock.enter()
{
    if ( !pool )
        pool = new NetPool( new PeerChannelFactoryUDP, NULL, NULL );
    return pool;
}

static void destroyPool ()
    // should be called inside of poolLock.enter()
{
    if ( peer ) {
        if ( pool )
            pool->deletePeer(peer);
        else
            peer->close();
        peer = NULL;
        }
    if ( pool )
        pool = NULL;
}

static void setupPortBitMask ( BitMask &mask )
{
    mask.empty();
    int port = localPort;
    int i;
    for ( i = 0; i++ < NUM_PORTS_TO_TRY; port += PORT_INTERVAL )
        mask.on(port);
}

static NetPeer *getPeer ()
    // should be called inside of poolLock.enter()
{
    if ( !peer ) {
        BitMask portMask;
        setupPortBitMask(portMask);
        peer = getPool()->createPeer(&portMask);
        Assert( peer );
        NetChannel *ctrl = peer->getBroadcastChannel();
        if ( ctrl ) ctrl->setProcessRoutine(ctrlReceive,peer);
        }
    return peer;
}

/// Actual client instance (can be NULL).
RefD<TestClient> client;

/// TestServer instance to process control messages.
RefD<TestServer> server;

/// Sets actual client instance (can be NULL).
void setClient ( TestClient *cl )
{
    poolLock.enter();
    client = cl;
    if ( cl ) {
        getPool();
        getPeer();
        }
    poolLock.leave();
}

/// Sets actual server instance (can be NULL).
void setServer ( TestServer *srv )
{
    poolLock.enter();
    server = srv;
    if ( srv ) {
        getPool();
        getPeer();
        }
    poolLock.leave();
}

/// Checks client and server. If everything is NULL, destroys the global NetPool!
static void checkPool ()
{
    poolLock.enter();
    if ( !client && !server )
        destroyPool();
    poolLock.leave();
}

void cancelAllMessages ()
{
    poolLock.enter();
    if ( getPeer() )
        getPeer()->cancelAllMessages();
    poolLock.leave();
}

bool ChannelState::checkMessage ( const NetMessage *msg, unsigned64 drift )
{
    Assert( msg );
    unsigned16 flags = msg->getFlags();
    CommonPacket *packet = (CommonPacket*)msg->getData();
    unsigned32 serial = packet->serial;
    bool result = true;
    if ( flags & MSG_VIM_FLAG )
        if ( flags & MSG_URGENT_FLAG ) { // urgent message
            if ( urgentSerial+1 != serial ||
                 (urgentSerial && urgentProlog != packet->prolog) )
                result = false;
            urgentSerial = serial;
            urgentProlog = packet->epilog;
            }
        else {                          // VIM message
            if ( vimSerial+1 != serial ||
                 (vimSerial && vimProlog != packet->prolog) )
                result = false;
            vimSerial = serial;
            vimProlog = packet->epilog;
            }
    else {                              // common message
        if ( commonSerial+1 == serial &&
             commonSerial &&
             commonProlog != packet->prolog )
            result = false;
        commonSerial = serial;
        commonProlog = packet->epilog;
        }
    if ( result && positiveCheckLogs > 1 ) {
        unsigned64 there = packet->timeStamp + drift;
        unsigned64 here  = msg->getTime();
        double delay = 1.e-6 * ( (here>there) ? (here-there) : 0 );
        NetLog("Channel(%u): Received OK (len=%4u,serial=%5u,flags=%04x,delay=%6.3f,ser=%5u,magic=%x)",
               msg->getChannel()->getChannelId(),msg->getLength(),msg->getSerial(),msg->getFlags(),
               delay,serial,packet->magic);
        }
    return result;
}

void ChannelState::setMessage ( NetMessage *msg )
{
    Assert( msg );
    unsigned16 flags = msg->getFlags();
    CommonPacket *packet = (CommonPacket*)msg->getData();
    packet->epilog = (unsigned32)( UINT_MAX * rnd.uniformNumber() );
    packet->timeStamp = getSystemTime();
    if ( flags & MSG_VIM_FLAG )
        if ( flags & MSG_URGENT_FLAG ) {
            packet->prolog = urgentProlog;
            urgentProlog = packet->epilog;
            packet->serial = ++urgentSerial;
            }
        else {
            packet->prolog = vimProlog;
            vimProlog = packet->epilog;
            packet->serial = ++vimSerial;
            }
    else {
        packet->prolog = commonProlog;
        commonProlog = packet->epilog;
        packet->serial = ++commonSerial;
        }
}

//------------------------------------------------------------

ClientInfo::ClientInfo ( int outProf, NetChannel *ch )
{
    inState.init();
    outState.init();
    outProfile = outProf;
    RefD<TestProfile> p;
    if ( !profiles.get(outProf,p) ) profiles.get(0,p);
    outProfileIt.init(p,ch,&outState);
}

bool ClientInfo::checkMessage ( const NetMessage *msg )
{
    return inState.checkMessage(msg,drift);
}

double ClientInfo::getRelativeTime () const
{
    unsigned64 delta = getSystemTime() - connectedTime;
    return( 1.e-6 * delta );
}

bool ClientInfo::executeToNow ()
{
    if ( newClient ) return false;
    return outProfileIt.executeTo(getRelativeTime());
}

unsigned ClientInfo::getSent ()
{
    return outProfileIt.getSent();
}

ClientInfo::~ClientInfo ()
{
}

#include "Es/Memory/normalNew.hpp"

void* ClientInfo::operator new ( size_t size )
{
    return safeNew(size);
}

void* ClientInfo::operator new ( size_t size, const char *file, int line )
{
    return safeNew(size);
}

void ClientInfo::operator delete ( void *mem )
{
    safeDelete(mem);
}

#ifdef __INTEL_COMPILER

void ClientInfo::operator delete ( void *mem, const char *file, int line )
{
    safeDelete(mem);
}

#endif

#include "Es/Memory/debugNew.hpp"

//------------------------------------------------------------
//  Test* implementations:

ProfileEvent::ProfileEvent ()
{
    time = 100.0;                           // time in seconds
    type = peParams;                        // event type (params-change)
    paramFlags      =    0;
    saturationAbs   =    0;
    saturationRel   =  0.9;                 // just the right number
    entropy         =  1.0;                 // maximal entropy <=> random data
    maxPacket       =  470;                 // excluding system headers
    portfolioUrgent =  0.05;                // portion of urgent packets
    portfolioVIM    =  0.20;                // portion of VIM packets
    sendTimeout     = 1000000;              // send-timeout in us
    fileSize        =    0;                 // [urgent] file-size to send
}

ProfileEvent::~ProfileEvent ()
{
}

//------------------------------------------------------------

ProfileIterator::ProfileIterator ()
{
    chState = NULL;
    reset();
}

void ProfileIterator::init ( TestProfile *p, NetChannel *ch, ChannelState *cs )
{
    profile = p;
    channel = ch;
    chState = cs;
    reset();
}

void ProfileIterator::fillPacket ( void *data, unsigned size )
{
    if ( !data || !size ) return;
    unsigned8 *d = (unsigned8*)data;
    unsigned random = (unsigned)( entropy * size );
    unsigned rle    = size - random;
    unsigned8 fill  = (entropy == 0.0) ? 0 : (unsigned8)( 256.0 * rnd.uniformNumber() );
    while ( rle-- )
        *d++ = fill;
    while ( random-- )
        *d++ = (unsigned8)( 256.0 * rnd.uniformNumber() );
}

void ProfileIterator::reset ()
{
    if ( profile ) {
        saturationAbs   = profile->saturationAbs;
        saturationRel   = profile->saturationRel;   // relative to NetChannel::getOutputLatency()
        entropy         = profile->entropy;         // maximal entropy <=> random data
        maxPacket       = profile->maxPacket;       // excluding system headers
        portfolioUrgent = profile->portfolioUrgent; // portion of urgent packets
        portfolioVIM    = profile->portfolioVIM;    // portion of VIM packets
        sendTimeout     = profile->sendTimeout;     // send-timeout in us
        nextEvent       = profile->events;
        }
    else {
        saturationAbs   =    0;
        saturationRel   =  0.9;                     // just the right saturation
        entropy         =  1.0;                     // maximal entropy <=> random data
        maxPacket       =  470;                     // excluding system headers
        portfolioUrgent =  0.05;                    // portion of urgent packets
        portfolioVIM    =  0.20;                    // portion of VIM packets
        sendTimeout     = 1000000;                  // send-timeout in us
        nextEvent       = NULL;
        }
    fileToSend      = 0;                            // file-size to send
    fileUToSend     = 0;                            // urgent file-size to send
    disconnect      = false;                        // disconnect the network channel immediately
    Zero(qVolume);
    int i;
    unsigned64 init = getSystemTime() - 1000000;
    for ( i = 0; i < MAX_STAT_QUEUE; )
        qTime[i++] = init;
    qPtr = 0;
    totalSent = 0;
}


void ProfileIterator::executeEvent ()
{
    if ( !nextEvent ) return;

    switch ( nextEvent->type ) {

        case peParams:
            if ( nextEvent->paramFlags & PROF_PARAM_SATURATION ) {
                saturationAbs = nextEvent->saturationAbs;
                saturationRel = nextEvent->saturationRel;
                }
            if ( nextEvent->paramFlags & PROF_PARAM_ENTROPY ) {
                entropy = nextEvent->entropy;
                }
            if ( nextEvent->paramFlags & PROF_PARAM_MAX_PACKET ) {
                maxPacket = nextEvent->maxPacket;
                }
            if ( nextEvent->paramFlags & PROF_PARAM_PORTFOLIO ) {
                portfolioUrgent = nextEvent->portfolioUrgent;
                portfolioVIM    = nextEvent->portfolioVIM;
                }
            if ( nextEvent->paramFlags & PROF_PARAM_TIMEOUT ) {
                sendTimeout = nextEvent->sendTimeout;
                }
            break;

        case peDisconnect:
            disconnect = true;
            break;

        case peFile:
            fileToSend += nextEvent->fileSize;
            break;

        case peUFile:
            fileUToSend += nextEvent->fileSize;
            break;

        }

    nextEvent = nextEvent->next;
}


bool ProfileIterator::executeTo ( double time )
{
    if ( !profile ) return false;
    char buf[512];
    getStatistics(buf);                     // for logging purposes only

    enter();
        // 1. execute all commands before 'time'
    while ( (bool)nextEvent && nextEvent->time <= time )
        executeEvent();
    if ( disconnect ) {
        leave();
        return true;
        }

        // 2. send all messages according to the plan
    unsigned sent = 0;                      // bytes sent in this frame..
    unsigned msgs = 0;
        // 2a. determine how much traffic was sent in last 2 seconds..
    Assert( channel );
    Assert( chState );
    unsigned64 now = getSystemTime();
    int ptr = qPtr + 1;
    if ( ptr >= MAX_STAT_QUEUE ) ptr = 0;
    while ( ptr != qPtr && qTime[ptr] + 2000000 < now )
        if ( ++ptr >= MAX_STAT_QUEUE ) ptr = 0;
    if ( ptr == qPtr )
        if ( --ptr < 0 ) ptr = MAX_STAT_QUEUE - 1;
        // values from [ptr] to [qPtr] are used for outgoing bandwidth computation:
    double maxCoef = 2.e6 / (now - qTime[ptr]);
    double dCoef   = maxCoef / ( ((qPtr<ptr) ? qPtr + MAX_STAT_QUEUE : qPtr) - ptr + 2 );
    double coef    = 0.0;
    unsigned sentBW = 0.0;
    while ( ptr != qPtr ) {
        sentBW += (unsigned)( coef * qVolume[ptr++] );
        if ( ptr >= MAX_STAT_QUEUE ) ptr = 0;
        coef += dCoef;
        }
    sentBW += (unsigned)( coef * qVolume[qPtr] );

        // 2b. ask for NetChannel bandwidth estimation, determine how many bytes are to be sent
    unsigned estBW = channel->getOutputBandWidth();
    unsigned saturation = saturationAbs ? saturationAbs : (unsigned)( estBW * saturationRel );
    if ( saturation > MAX_BANDWIDTH )
        saturation = MAX_BANDWIDTH;
    int toSent = (saturation > sentBW) ? (int)( 1.03 * (saturation - sentBW) ) : 0;
                                            // to fill the required band-width precisely..

        // 2c. fill the required output bandwidth
        //     (files have the highest priority, then regular messages)
    while ( fileUToSend ) {                 // urgent file
        int len = fileUToSend;
        if ( len + MIN_MSG_SIZE > maxPacket ) len = maxPacket - MIN_MSG_SIZE;
        if ( len <= 0 ) break;
        Ref<NetMessage> out = NetMessagePool::pool()->newMessage(sizeof(CommonPacket)+len,channel);
        if ( out ) {                        // prepare & send the message:
            out->setLength(sizeof(CommonPacket)+len);
            out->setFlags(MSG_ALL_FLAGS,MSG_VIM_FLAG|MSG_URGENT_FLAG);
            out->setOrderedPrevious();
                // internal message data:
            CommonPacket *msg = (CommonPacket*)out->getData();
            msg->magic  = MAGIC_FILE;
            msg->tmp    = 0;
            msg->size   = len;
            fillPacket(&msg->data,len);
                // channel-status (consistency checks..):
            chState->setMessage(out);
                // message is ready to send:
            out->send();
            }
        else
            break;
        fileUToSend -= len;
        toSent      -= MIN_MSG_SIZE + len;
        sent        += MIN_MSG_SIZE + len;
        msgs++;
        }

    while ( fileToSend ) {                  // regular (VIM) file
        int len = fileToSend;
        if ( len + MIN_MSG_SIZE > maxPacket ) len = maxPacket - MIN_MSG_SIZE;
        if ( len <= 0 ) break;
        Ref<NetMessage> out = NetMessagePool::pool()->newMessage(sizeof(CommonPacket)+len,channel);
        if ( out ) {                        // prepare & send the message:
            out->setLength(sizeof(CommonPacket)+len);
            out->setFlags(MSG_ALL_FLAGS,MSG_VIM_FLAG);
            out->setOrderedPrevious();
                // internal message data:
            CommonPacket *msg = (CommonPacket*)out->getData();
            msg->magic  = MAGIC_FILE;
            msg->tmp    = 0;
            msg->size   = len;
            fillPacket(&msg->data,len);
                // channel-status (consistency checks..):
            chState->setMessage(out);
                // message is ready to send:
            out->send();
            }
        else
            break;
        fileToSend -= len;
        toSent     -= MIN_MSG_SIZE + len;
        sent       += MIN_MSG_SIZE + len;
        msgs++;
        }

    while ( toSent > MIN_MSG_SIZE ) {                   // regular messages
        int len = toSent - MIN_MSG_SIZE;
        if ( len + MIN_MSG_SIZE > maxPacket ) len = maxPacket - MIN_MSG_SIZE;
        if ( len <= 0 ) break;
            // len == maximum allowed message length..
        len = 1 + (int)( (len-1) * rnd.uniformNumber() );
        Ref<NetMessage> out = NetMessagePool::pool()->newMessage(sizeof(CommonPacket)+len,channel);
        if ( out ) {                        // prepare & send the message:
            out->setLength(sizeof(CommonPacket)+len);
            unsigned flags = 0;
            double random = rnd.uniformNumber();
            if ( random < portfolioVIM + portfolioUrgent ) {
                flags |= MSG_VIM_FLAG;
                if ( random < portfolioUrgent )
                    flags |= MSG_URGENT_FLAG;
                }
            if ( flags ) {
                out->setFlags(MSG_ALL_FLAGS,flags);
                out->setOrderedPrevious();
                }
            else
                if ( sendTimeout )
                    out->setSendTimeout(sendTimeout);
                // internal message data:
            CommonPacket *msg = (CommonPacket*)out->getData();
            msg->magic  = MAGIC_COMMON;
            msg->tmp    = 0;
            msg->size   = len;
            fillPacket(&msg->data,len);
                // channel-status (consistency checks..):
            chState->setMessage(out);
                // message is ready to send:
            out->send();
            }
        else
            break;
        toSent     -= MIN_MSG_SIZE + len;
        sent       += MIN_MSG_SIZE + len;
        msgs++;
        }

        // remember the actually generated output traffic:
    totalSent += sent;
#ifdef NET_LOG
    if ( positiveCheckLogs )
        NetLog("Channel(%u): executeTo(): sentBW=%u, estBW=%u, saturation=%d, toSent=%d, messages=%u, sent=%u",
               channel->getChannelId(),sentBW,estBW,saturation,toSent+sent,msgs,sent);
#endif
    if ( ++qPtr >= MAX_STAT_QUEUE ) qPtr = 0;
    qVolume[qPtr] = sent;
    qTime[qPtr]   = getSystemTime();

    leave();
    return false;
}

void ProfileIterator::getStatistics ( char *buf )
{
    enter();
    if ( !channel ) {
        leave();
        sprintf(buf,"No NetChannel is connected yet!");
        return;
        }
    int latencyAve;
    unsigned latencyAct,latencyMin;
    int throughputAve;
    latencyAve = (int)(channel->getLatency(&latencyAct,&latencyMin) / 1000);
    latencyAct /= 1000;
    latencyMin /= 1000;
    EnhancedBWInfo enhanced;
    throughputAve = (int)channel->getOutputBandWidth(&enhanced);
    int nMsg, nBytes, nMsgG, nBytesG;
    channel->getOutputQueueStatistics(nMsg,nBytes,nMsgG,nBytesG);
    ChannelStatistics stat;                 // internal statistics of the NetChannel
    Zero(stat);
    channel->getInternalStatistics(stat);
    bool kbps = (throughputAve < (9<<17));
    sprintf(buf,"ping%4dms(%4u,%4u) BW%c%c%c%5d%cb(%4u,%4u,%4u,%5u,%5u) lost%4.1f%%%%(%3u) queue%4dB(%4d) ackWait%3u(%3.1f,%3.1f)",
                latencyAve,latencyAct,latencyMin,
                (char)(enhanced.growMode+'c'),(char)(enhanced.growModePing+'c'),(char)(enhanced.growModeLost+'c'),
                kbps?((throughputAve+64)>>7):((throughputAve+65536)>>17),
                kbps?'K':'M',(enhanced.actBW+64)>>7,(enhanced.goodBW+64)>>7,(enhanced.sentBW+64)>>7,
                (enhanced.inRB+64)>>7,(enhanced.outRB+64)>>7,
                stat.ackTotal?(stat.ackLost*100.0)/stat.ackTotal:0.0,stat.ackLost,
                nBytes,nBytesG,stat.revisitedNo,1e-6*stat.revisitedAveAge,1e-6*stat.revisitedMaxAge);
#ifdef NET_LOG_TRANSP_STAT
    NetLog("Channel(%u): %s",channel->getChannelId(),buf);
#endif
    leave();
}

unsigned ProfileIterator::getSent ()
{
    unsigned result = totalSent;
    totalSent = 0;
    return result;
}

ProfileIterator::~ProfileIterator ()
{
}

//------------------------------------------------------------

TestProfile::TestProfile ( int _id )
{
    id              =  _id;
    saturationAbs   =    0;
    saturationRel   =  0.9;                 // just the right saturation
    entropy         =  1.0;                 // maximal entropy <=> random data
    maxPacket       =  470;                 // excluding system headers
    portfolioUrgent =  0.05;                // portion of urgent packets
    portfolioVIM    =  0.20;                // portion of VIM packets
    sendTimeout     = 1000000;              // send-timeout in us
}

TestProfile::~TestProfile ()
{
}

//------------------------------------------------------------

FILE *proFile = NULL;

#define MAX_TOKEN   128

bool token ( char *buf )
{
    if ( !proFile ) return false;
    int ch = getc(proFile);
    while ( ch != EOF ) {                   // looking for the next token
        while ( isspace(ch) )
            ch = getc(proFile);
        if ( ch == EOF ) break;
        if ( ch == '#' ) {                  // comment
            do
                ch = getc(proFile);
            while ( ch != EOF && ch != '\n' );
            if ( ch != EOF )
                ch = getc(proFile);
            }
        else {                              // non-space, non-comment => token start
            int count = 0;
            do {                            // copy one character into buf[]
                if ( ++count < MAX_TOKEN )
                    *buf++ = (char)ch;
                ch = getc(proFile);
                } while ( ch != EOF && !isspace(ch) );
            buf[0] = (char)0;
            return true;
            }
        }
    return false;
}

ProfileEvent *readEvent ()
{
    char buf[256];
    if ( !token(buf) || !isdigit(buf[0]) ) return NULL;
    ProfileEvent *ev = new ProfileEvent;
    ev->time = atof(buf);
    while ( token(buf) ) {                  // one token inside of [event .. endEvent]
        if ( !strcmp(buf,"endEvent") ) break;
        if ( !strcmp(buf,"saturation") ) {
            if ( token(buf) ) {
                ev->type = peParams;
                ev->paramFlags |= PROF_PARAM_SATURATION;
                bool relative = (buf[strlen(buf)-1] == '%');
                if ( relative ) {
                    ev->saturationAbs = 0;
                    ev->saturationRel = 1e-2 * atof(buf);
                    }
                else {
                    ev->saturationAbs = abs(atoi(buf)) << 7;
                    if ( ev->saturationAbs > MAX_BANDWIDTH )
                        ev->saturationAbs = MAX_BANDWIDTH;
                    ev->saturationRel = 1.0;
                    }
                }
            else
#ifdef NET_LOG
                NetLog("Error parsing event: saturation value expected");
#else
                printf("Error parsing event: saturation value expected\n");
#endif
            }
        else
        if ( !strcmp(buf,"entropy") ) {
            if ( token(buf) ) {
                ev->type = peParams;
                ev->paramFlags |= PROF_PARAM_ENTROPY;
                double ent = atof(buf);
                if ( ent < 0.0 ) ent = 0.0;
                if ( ent > 1.0 ) ent = 1.0;
                ev->entropy = ent;
                }
            else
#ifdef NET_LOG
                NetLog("Error parsing event: entropy value expected");
#else
                printf("Error parsing event: entropy value expected\n");
#endif
            }
        else
        if ( !strcmp(buf,"maxPacket") ) {
            if ( token(buf) ) {
                ev->type = peParams;
                ev->paramFlags |= PROF_PARAM_MAX_PACKET;
                unsigned len = abs(atoi(buf));
                if ( len > 1500 ) len = 1500;
                ev->maxPacket = len;
                }
            else
#ifdef NET_LOG
                NetLog("Error parsing event: maxPacket value expected");
#else
                printf("Error parsing event: maxPacket value expected\n");
#endif
            }
        else
        if ( !strcmp(buf,"portfolio") ) {
            bool ok = false;
            if ( token(buf) ) {
                double urg = 1e-2 * atof(buf);
                if ( urg < 0.0 ) urg = 0.0;
                if ( urg > 1.0 ) urg = 1.0;
                if ( token(buf) ) {
                    ok = true;
                    ev->type = peParams;
                    ev->paramFlags |= PROF_PARAM_PORTFOLIO;
                    double vim = 1e-2 * atof(buf);
                    if ( vim < 0.0 ) vim = 0.0;
                    if ( vim > 1.0-urg ) vim = 1.0-urg;
                    ev->portfolioUrgent = urg;
                    ev->portfolioVIM    = vim;
                    }
                }
            if ( !ok )
#ifdef NET_LOG
                NetLog("Error parsing event: portfolio values expected");
#else
                printf("Error parsing event: portfolio values expected\n");
#endif
            }
        else
        if ( !strcmp(buf,"timeout") ) {
            if ( token(buf) ) {
                ev->sendTimeout = abs(atoi(buf)) * 1000;
                ev->paramFlags |= PROF_PARAM_TIMEOUT;
                }
            else
#ifdef NET_LOG
                NetLog("Error parsing event: send-timeout value expected");
#else
                printf("Error parsing event: send-timeout value expected\n");
#endif
            }
        else
        if ( !strcmp(buf,"file") ) {
            if ( token(buf) ) {
                ev->type = peFile;
                unsigned len = abs(atoi(buf));
                if ( len > MAX_FILE_TRANSFER ) len = MAX_FILE_TRANSFER;
                ev->fileSize = len;
                }
            else
#ifdef NET_LOG
                NetLog("Error parsing event: file size expected");
#else
                printf("Error parsing event: file size expected\n");
#endif
            }
        else
        if ( !strcmp(buf,"fileU") ) {
            if ( token(buf) ) {
                ev->type = peUFile;
                unsigned len = abs(atoi(buf));
                if ( len > MAX_FILE_TRANSFER ) len = MAX_FILE_TRANSFER;
                ev->fileSize = len;
                }
            else
#ifdef NET_LOG
                NetLog("Error parsing event: urgent-file size expected");
#else
                printf("Error parsing event: urgent-file size expected\n");
#endif
            }
        else
        if ( !strcmp(buf,"disconnect") ) {
            ev->type = peDisconnect;
            }
        else {
#ifdef NET_LOG
            NetLog("Error parsing event: bad token '%s'",buf);
#else
            printf("Error parsing event: bad token '%s'\n",buf);
#endif
            delete ev;
            return NULL;
            }
        }
    return ev;
}

TestProfile *newProfile = NULL;

TestProfile *readProfile ()
{
    char buf[256];
    if ( !token(buf) || strcmp(buf,"profile") ||
         !token(buf) || !isdigit(buf[0]) ) return NULL;
    int id = atoi(buf);
    newProfile = new TestProfile(id);
    while ( token(buf) ) {                  // one token inside of [profile .. endProfile]
        if ( !strcmp(buf,"endProfile") ) break;
        if ( !strcmp(buf,"saturation") ) {
            if ( token(buf) ) {
                bool relative = (buf[strlen(buf)-1] == '%');
                if ( relative ) {
                    newProfile->saturationAbs = 0;
                    newProfile->saturationRel = 1e-2 * atof(buf);
                    }
                else {
                    newProfile->saturationAbs = abs(atoi(buf)) << 7;
                    if ( newProfile->saturationAbs > MAX_BANDWIDTH )
                        newProfile->saturationAbs = MAX_BANDWIDTH;
                    newProfile->saturationRel = 1.0;
                    }
                }
            else
#ifdef NET_LOG
                NetLog("Error parsing profile: saturation value expected");
#else
                printf("Error parsing profile: saturation value expected\n");
#endif
            }
        else
        if ( !strcmp(buf,"entropy") ) {
            if ( token(buf) ) {
                double ent = atof(buf);
                if ( ent < 0.0 ) ent = 0.0;
                if ( ent > 1.0 ) ent = 1.0;
                newProfile->entropy = ent;
                }
            else
#ifdef NET_LOG
                NetLog("Error parsing profile: entropy value expected");
#else
                printf("Error parsing profile: entropy value expected\n");
#endif
            }
        else
        if ( !strcmp(buf,"maxPacket") ) {
            if ( token(buf) ) {
                unsigned len = abs(atoi(buf));
                if ( len > 1500 ) len = 1500;
                newProfile->maxPacket = len;
                }
            else
#ifdef NET_LOG
                NetLog("Error parsing profile: maxPacket value expected");
#else
                printf("Error parsing profile: maxPacket value expected\n");
#endif
            }
        else
        if ( !strcmp(buf,"portfolio") ) {
            bool ok = false;
            if ( token(buf) ) {
                double urg = 1e-2 * atof(buf);
                if ( urg < 0.0 ) urg = 0.0;
                if ( urg > 1.0 ) urg = 1.0;
                if ( token(buf) ) {
                    ok = true;
                    double vim = 1e-2 * atof(buf);
                    if ( vim < 0.0 ) vim = 0.0;
                    if ( vim > 1.0-urg ) vim = 1.0-urg;
                    newProfile->portfolioUrgent = urg;
                    newProfile->portfolioVIM    = vim;
                    }
                }
            if ( !ok )
#ifdef NET_LOG
                NetLog("Error parsing profile: portfolio values expected");
#else
                printf("Error parsing profile: portfolio values expected\n");
#endif
            }
        else
        if ( !strcmp(buf,"timeout") ) {
            if ( token(buf) ) {
                newProfile->sendTimeout = abs(atoi(buf)) * 1000;
                }
            else
#ifdef NET_LOG
                NetLog("Error parsing profile: send-timeout value expected");
#else
                printf("Error parsing profile: send-timeout value expected\n");
#endif
            }
        else
        if ( !strcmp(buf,"event") ) {
            ProfileEvent *event = readEvent();
            if ( !newProfile->events ) {    // the 1st event
                newProfile->events = event;
                event->next = NULL;
                }
            else {                          // insert
                ProfileEvent *ptr = newProfile->events;
                if ( event->time < ptr->time ) {
                    event->next = ptr;
                    newProfile->events = event;
                    }
                else {
                    while ( (bool)ptr->next && ptr->next->time <= event->time )
                        ptr = ptr->next;
                    event->next = ptr->next;
                    ptr->next = event;
                    }
                }
            }
        else {
#ifdef NET_LOG
            NetLog("Error parsing profile: bad token '%s'",buf);
#else
            printf("Error parsing profile: bad token '%s'\n",buf);
#endif
            delete newProfile;
            newProfile = NULL;
            return NULL;
            }
        }
    TestProfile *result = newProfile;
    newProfile = NULL;
    return result;
}

void readProfiles ( const char *fn )        // parses input text file into 'profiles' map
/*
    profile <N>
    endProfile

    saturation {<abs>|<rel>%}
    entropy <e>
    maxPacket <max>
    portfolio <urgent>% <VIM>%
    timeout <send-timeout>

    event <time>
    endEvent

    file <size>
    fileU <size>
    disconnect
*/
{
    if ( !fn || !fn[0] ) return;
    proFile = fopen(fn,"rt");
    if ( !proFile ) return;
    RefD<TestProfile> nProfile;
    while ( (nProfile = readProfile()), nProfile )
        profiles.put(nProfile->id,nProfile);
    fclose(proFile);
    proFile = NULL;
}

//------------------------------------------------------------

TestServer::TestServer () : sndCs(), rcvCs()
{
        // to be sure:
    received = lastReceived = NULL;
    sent = NULL;
    setServer(this);
#ifdef NET_LOG_SERVER
    NetLog("Peer(%u): creating TestServer instance",getPeer()->getPeerId());
#endif
}

bool TestServer::init ()
{
    poolLock.enter();
    NetPeer *peer = getPeer();
#ifdef NET_LOG_SERVER
    NetLog("Peer(%u): TestServer::init: %s",
           peer?peer->getPeerId():0,peer?"success":"failed");
#endif
    poolLock.leave();
    totalReceived = totalSent = 0;
    lastStatistics = getSystemTime();
    receivedGood = totalGood = receivedBad = totalBad = 0;
    return( peer != NULL );
}

void TestServer::getNetStatistics ( NetworkStatistics &stat )
{
    enter();
    unsigned64 now = getSystemTime();
    double freq = 1.e6 / (now + 1.0 - lastStatistics);
    lastStatistics = now;
    stat.numberOfClients = clients.card();
    stat.recvBytes = (unsigned)( freq * totalReceived );
    totalReceived = 0;
    stat.sentBytes = (unsigned)( freq * totalSent );
    totalSent = 0;
    stat.recvBad = (unsigned)( freq * receivedBad );
    stat.totalBad = (totalBad += receivedBad);
    receivedBad = 0;
    stat.recvGood = (unsigned)( freq * receivedGood );
    stat.totalGood = (totalGood += receivedGood);
    receivedGood = 0;
    if ( stat.numberOfClients ) {
        IteratorState it;
        RefD<NetChannel> itch;
        clients.getFirst(it,itch);
        stat.minLatency = stat.maxLatency = itch->getLatency();
        while ( clients.getNext(it,itch) ) {
            unsigned lat = itch->getLatency();
            if ( lat < stat.minLatency ) stat.minLatency = lat;
            if ( lat > stat.maxLatency ) stat.maxLatency = lat;
            }
        stat.minLatency = (stat.minLatency + 500) / 1000;
        stat.maxLatency = (stat.maxLatency + 500) / 1000;
        }
    else
        stat.minLatency = stat.maxLatency = 0;
    leave();
}

void TestServer::spendTime ( unsigned64 interval )
{
    unsigned64 now = getSystemTime();
    unsigned64 globalEnd = now + interval;
    do {                                    // simulate one frame
        unsigned64 end = now + frameTime;
        IteratorState it;
        int id;
        enter();
        RefD<NetChannel> ch;
        if ( clients.getFirst(it,ch,&id) )
            do {
                RefD<ClientInfo> ci;
                infos.get(id,ci);
                Assert( ci );
                bool disconnect = ci->executeToNow();
                totalSent += ci->getSent();
                if ( disconnect || ch->dropped() )
                    disconnectInternal(id,false);
                } while ( clients.getNext(it,ch,&id) );
        totalReceived += processMessages();
        leave();
        now = getSystemTime();
        if ( now + 1000 < end )
            SLEEP_MS( (end - now + 500) / 1000 );
        now = getSystemTime();
        } while ( now < globalEnd );
}

void TestServer::disconnectInternal ( int id, bool notify )
{
    RefD<NetChannel> channel;
    if ( !clients.get(id,channel) ) return;
    if ( notify ) {
        Ref<NetMessage> out = NetMessagePool::pool()->newMessage(sizeof(ControlPacket),channel);
        if ( out ) {
            ControlPacket *msg = (ControlPacket*)out->getData();
            msg->magic     = MAGIC_DISCONNECT;
            msg->tmp       = 0;
            msg->serial    = id;
            msg->timeStamp = getSystemTime();
            out->setLength(sizeof(ControlPacket));
            out->send(true);
            SLEEP_MS(DESTRUCT_WAIT);
            }
        }
    getPool()->deleteChannel(channel);
    clients.remove(id);
    infos.remove(id);
}

void TestServer::disconnect ( int id, bool notify )
{
    enter();
    disconnectInternal(id,notify);
    leave();
}

int TestServer::channelToClient ( NetChannel *ch )
{
    if ( !ch ) return -1;
    IteratorState it;
    int i;
    enter();
    RefD<NetChannel> itch;
    if ( clients.getFirst(it,itch,&i) )
        do
            if ( (NetChannel*)itch == ch ) break;
        while ( clients.getNext(it,itch,&i) );
    if ( !itch ) i = -1;
    leave();
    return i;
}

unsigned TestServer::processMessages ()
{
    unsigned total = 0;
    enterRcv();
    Ref<NetMessage> tmp;
    while ( received ) {
        tmp = received->next;
        received->next = NULL;
            // process 'received' message:
        NetChannel *channel = received->getChannel();
        int cl = channelToClient(channel);
        if ( cl >= 0 ) {
            RefD<ClientInfo> info;
            infos.get(cl,info);
            if ( info->checkMessage(received) )
                receivedGood++;
            else {
                receivedBad++;
#ifdef NET_LOG
                NetLog("Channel(%u): Bad message received (len=%3u,serial=%4u,flags=%04x)",
                       channel->getChannelId(),received->getLength(),received->getSerial(),received->getFlags());
#endif
                }
            }
        total += received->getLength() + IP_UDP_HEADER + MSG_HEADER_LEN;
        received = tmp;
        }
    lastReceived = NULL;                    // to be sure
    leaveRcv();
    return total;
}

void TestServer::removeReceivedMessages ()
{
    enterRcv();
    Ref<NetMessage> tmp;
    while ( received ) {
        tmp = received->next;
        received->next = NULL;              // don't need this message anymore, somebody will recycle it later
        received = tmp;
        }
    lastReceived = NULL;                    // to be sure
    leaveRcv();
}

void TestServer::removeSentMessages ()
{
    enterSnd();
    Ref<NetMessage> tmp;
    while ( sent ) {
        tmp = sent->next;
        sent->next = NULL;                  // don't need this message anymore, somebody will recycle it later
        sent = tmp;
        }
    leaveSnd();
}

bool TestServer::getFirst ( IteratorState &iterator, RefD<NetChannel> &first )
{
    return clients.getFirst(iterator,first);
}

bool TestServer::getNext ( IteratorState &iterator, RefD<NetChannel> &next )
{
    return clients.getNext(iterator,next);
}

TestServer::~TestServer ()
{
        // kick-off all the clients:
    IteratorState it;
    enter();
    RefD<ClientInfo> ci;
    if ( infos.getFirst(it,ci) )
        while ( ci->id >= 0 ) {
            disconnectInternal(ci->id,true);
            if ( !infos.getNext(it,ci) ) break;
            }
    leave();

    cancelAllMessages();
    setServer(NULL);
        // recycle all pending NetMessages:
    removeReceivedMessages();
    removeSentMessages();

#ifdef NET_LOG_SERVER
    NetLog("Peer(%u): destroying NetServer instance",getPeer()->getPeerId());
#endif
    checkPool();
}

//------------------------------------------------------------

TestClient::TestClient () : sndCs(), rcvCs()
{
    drift = 0;
    accepted = 0;
        // NetChannel: not set yet
    channel = NULL;
        // to be sure:
    received = lastReceived = NULL;
    sent = NULL;
    terminated = true;
    inState.init();
    outState.init();
    outProfile = 0;
    setClient(this);
#ifdef NET_LOG_CLIENT
    NetLog("Peer(%u): creating TestClient instance",getPeer()->getPeerId());
#endif
}

bool TestClient::open ( struct sockaddr_in &addr, int prof, int distantProf )
{
    enter();
    if ( channel ) {                        // already connected => error
        leave();
        return false;
        }
        // "addr" contains valid IP address..
    terminated = false;
    inState.init();
    outState.init();
    outProfile = prof;
    totalReceived = totalSent = 0;

        // create a new NetChannel for client <-> server communication:
    poolLock.enter();
    if ( getPool() )
        channel = getPool()->createChannel(addr,getPeer());
    if ( !channel ) {
        poolLock.leave();
        leave();
        return false;
        }
    poolLock.leave();

    channel->setProcessRoutine(clientReceive,channel);

        // put together the "Connect" message:
    ControlPacket packet;
    packet.magic = MAGIC_CONNECT;
    packet.tmp = distantProf;
    packet.serial = playerNo = (int32)(getSystemTime() & 0x7fffffff);
#ifdef NET_LOG_CLIENT
    NetLog("Channel(%u): TestClient::open: server=%u.%u.%u.%u:%u, player=%d",
           channel->getChannelId(),
           (unsigned)IP4(addr),(unsigned)IP3(addr),(unsigned)IP2(addr),(unsigned)IP1(addr),
           (unsigned)PORT(addr),playerNo);
#endif
        // .. and send it to the server:
    accepted = 0;
    // server will receive this message via it's control channel so I must not use VIM flag!
    // I'll send it multiple times if necessary..

        // now I have to wait to server's response..
    unsigned64 first = acceptTime = getSystemTime();
    unsigned64 now = first;
    unsigned64 next = first;                // re-send time (init = the 1st try)
    unsigned64 timeout = now + 1000 * ACK_TIMEOUT;
    do {
        if ( now >= next ) {
            Ref<NetMessage> msg = NetMessagePool::pool()->newMessage(sizeof(packet),channel);
            if ( !msg ) {
                leave();
                return false;
                }
            msg->setFlags(MSG_ALL_FLAGS,MSG_TO_BCAST_FLAG);
            packet.timeStamp = connectTime = getSystemTime();
            msg->setData((unsigned8*)&packet,sizeof(packet));
            msg->send(true);                // urgent
            next = now + 1000 * CONNECT_RESEND;
            }
        leave();
        SLEEP_MS(NET_CHECK_WAIT);
        enter();
        now = getSystemTime();
        } while ( !accepted && now < timeout );

    bool result = (accepted > 0);
    if ( result ) {
            // initial hand-shake (to initialize band-width):
        channel->checkConnectivity(0);
            // initialize the output-profile:
        RefD<TestProfile> p;
        if ( !profiles.get(outProfile,p) ) profiles.get(0,p);
        outProfileIt.init(p,channel,&outState);
        }
    else
        terminated = true;

#ifdef NET_LOG_CLIENT
    NetLog("Channel(%u): TestClient::open: %s after %.3f seconds (player=%d)",
           channel->getChannelId(),
           result?"connected":"failed",1.e-6*(getSystemTime()-first),playerNo);
#endif
    lastStatistics = now;
    receivedGood = totalGood = receivedBad = totalBad = 0;
    leave();
    return result;
}

unsigned TestClient::processMessages ()
{
    unsigned total = 0;
    enterRcv();
    Ref<NetMessage> tmp;
    while ( received ) {
        tmp = received->next;
        received->next = NULL;
            // process 'received' message:
        if ( inState.checkMessage(received,drift) )
            receivedGood++;
        else {
            receivedBad++;
#ifdef NET_LOG
            NetLog("Channel(%u): Bad message received (len=%3u,serial=%4u,flags=%04x)",
                   channel->getChannelId(),received->getLength(),received->getSerial(),received->getFlags());
#endif
            }
        total += received->getLength() + IP_UDP_HEADER + MSG_HEADER_LEN;
        received = tmp;
        }
    lastReceived = NULL;                    // to be sure
    leaveRcv();
    return total;
}

void TestClient::removeReceivedMessages ()
{
    enterRcv();
    Ref<NetMessage> tmp;
    while ( received ) {
        tmp = received->next;
        received->next = NULL;              // don't need this message anymore, somebody will recycle it later
        received = tmp;
        }
    lastReceived = NULL;                    // to be sure
    leaveRcv();
}

void TestClient::removeSentMessages ()
{
    enterSnd();
    Ref<NetMessage> tmp;
    while ( sent ) {
        tmp = sent->next;
        sent->next = NULL;                  // don't need this message anymore, somebody will recycle it later
        sent = tmp;
        }
    leaveSnd();
}

bool TestClient::isTerminated () const
{
    return terminated;
}

void TestClient::getNetStatistics ( NetworkStatistics &stat )
{
    enter();
    unsigned64 now = getSystemTime();
    double freq = 1.e6 / (now + 1.0 - lastStatistics);
    lastStatistics = now;
    stat.numberOfClients = 1;
    stat.recvBytes = (unsigned)( freq * totalReceived );
    totalReceived = 0;
    stat.sentBytes = (unsigned)( freq * totalSent );
    totalSent = 0;
    stat.recvBad = (unsigned)( freq * receivedBad );
    stat.totalBad = (totalBad += receivedBad);
    receivedBad = 0;
    stat.recvGood = (unsigned)( freq * receivedGood );
    stat.totalGood = (totalGood += receivedGood);
    receivedGood = 0;
    stat.minLatency = stat.maxLatency = channel ? (channel->getLatency() + 500) / 1000 : 0;
    EnhancedBWInfo chInfo;
    stat.outBand = channel->getOutputBandWidth(&chInfo);
    int msgs, vimMsgs;
    channel->getOutputQueueStatistics(msgs,stat.normalQBytes,vimMsgs,stat.vimQBytes);
    leave();
    stat.goodBand  = chInfo.goodBW;
    stat.inRBBand  = chInfo.inRB;
    stat.outRBBand = chInfo.outRB;
}

double TestClient::getRelativeTime () const
{
    unsigned64 delta = getSystemTime() - acceptTime;
    return( 1.e-6 * delta );
}

void TestClient::spendTime ( unsigned64 interval )
{
    if ( terminated ) return;
    if ( (bool)channel && channel->dropped() ) {
        terminated = true;
        return;
        }
    unsigned64 now = getSystemTime();
    unsigned64 globalEnd = now + interval;
    do {                                    // simulate one frame
        unsigned64 end = now + frameTime;
        terminated = outProfileIt.executeTo(getRelativeTime());
        totalSent += outProfileIt.getSent();
        totalReceived += processMessages();
        if ( terminated ) return;
        now = getSystemTime();
        if ( now + 1000 < end )
            SLEEP_MS( (end - now + 500) / 1000 );
        now = getSystemTime();
        } while ( now < globalEnd );
}

TestClient::~TestClient ()
{
    enter();
    if ( !terminated ) {                    // I must notify my server..
        terminated = true;
            // send MAGIC_DISCONNECT message to server:
        if ( channel ) {
            Ref<NetMessage> out = NetMessagePool::pool()->newMessage(sizeof(ControlPacket),channel);
            if ( out ) {
                ControlPacket *msg = (ControlPacket*)out->getData();
                msg->magic = MAGIC_DISCONNECT;
                msg->tmp = 0;
                msg->serial = playerNo;
                msg->timeStamp = getSystemTime();
                out->setLength(sizeof(ControlPacket));
                out->send(true);
                leave();
                SLEEP_MS(DESTRUCT_WAIT);
                enter();
                }
            }
        }

    cancelAllMessages();
    setClient(NULL);
        // recycle all pending NetMessages:
    removeReceivedMessages();
    removeSentMessages();
        // destroy the NetChannel:
#ifdef NET_LOG_CLIENT
    NetLog("Channel(%u): destroying TestClient instance",channel?channel->getChannelId():0);
#endif
    if ( channel ) {
        getPool()->deleteChannel(channel);
        channel = NULL;
        }
    leave();
    checkPool();
}

//------------------------------------------------------------
//  Callback routines:

NetStatus ctrlReceive ( NetMessage *msg, NetStatus event, void *data )
    // receive routine for control (broadcast) channel of the TestServer
{
    if ( !server || !msg ) return nsNoMoreCallbacks; // fatal error
    unsigned len = msg->getLength();
    if ( len < 4 ) return nsNoMoreCallbacks; // message is too small
    unsigned32 magic = *(unsigned32*)msg->getData();
#ifdef NET_LOG_CTRL_RECEIVE
    NetLog("Peer(%u)::ctrlReceive: received message (len=%3u, serial=%4u, flags=%04x, magic=%x)",
           peer->getPeerId(),len,msg->getSerial(),(unsigned)msg->getFlags(),magic);
#endif

    switch ( magic ) {

        case MAGIC_CONNECT:                 // connection request from the new client..
            if ( len == sizeof(ControlPacket) ) {
                ControlPacket *con = (ControlPacket*)msg->getData();
                server->enter();
                if ( !server->clients.present(con->serial) ) { // I'm sending MAGIC_ACCEPT only once!
                    struct sockaddr_in distant;
                    msg->getDistant(distant);
                    poolLock.enter();
                    NetPeer *peer = getPeer();
                    Assert( peer );
                    NetChannel *ch = getPool()->createChannel(distant,peer);
                    poolLock.leave();
                    if ( ch ) {
                        server->clients.put(con->serial,ch);
                        ClientInfo *ci = new ClientInfo(con->tmp,ch);
                        ci->id = con->serial;
                        ci->acceptTime =
                        ci->connectedTime = getSystemTime();
                        ci->drift = 0;
                        ci->newClient = true;
                        ci->inState.init();
                        ci->outState.init();
                        server->infos.put(ci);
                        ch->setProcessRoutine(serverReceive,ch);
                        }
                    Ref<NetMessage> out = NetMessagePool::pool()->newMessage(sizeof(ControlPacket),ch ? ch : msg->getChannel());
                    if ( out ) {
                        if ( !ch ) out->setDistant(distant);
                        ControlPacket *acc = (ControlPacket*)out->getData();
                        acc->magic = MAGIC_ACCEPT;
                        acc->tmp = ch ? 1 : 0;
                        acc->serial = con->serial;
                        acc->timeStamp = getSystemTime();
                        out->setFlags(MSG_ALL_FLAGS,ch?MSG_VIM_FLAG:MSG_FROM_BCAST_FLAG);
                        out->setLength(sizeof(ControlPacket));
                        out->send(true);        // urgent message
                        }
                    if ( ch )                   // initial hand-shake (to initialize band-width..).
                        ch->checkConnectivity(0);
                    }
                server->leave();
                }
            break;
        }

    return nsNoMoreCallbacks;
}

NetStatus serverReceive ( NetMessage *msg, NetStatus event, void *data )
    // receive routine for common channel of the TestServer
{
    if ( !server || !msg ) return nsNoMoreCallbacks; // fatal error
    unsigned len = msg->getLength();
    if ( len < 4 ) return nsNoMoreCallbacks; // message is too small
    unsigned32 magic = *(unsigned32*)msg->getData();
    NetChannel *channel = (NetChannel*)data;
    Assert( channel );
#ifdef NET_LOG_SERVER_RECEIVE
    NetLog("Channel(%u)::serverReceive: received message (len=%3u, serial=%4u, flags=%04x, magic=%x)",
           channel->getChannelId(),len,msg->getSerial(),(unsigned)msg->getFlags(),magic);
#endif

    switch ( magic ) {

        case MAGIC_CONNECTED:               // end of the connection-handshake
            if ( len == sizeof(ControlPacket) ) {
                ControlPacket *con = (ControlPacket*)msg->getData();
                server->enter();
                if ( server->clients.present(con->serial) ) { // OK, I remember such client
                    RefD<ClientInfo> ci;
                    server->infos.get(con->serial,ci);
                    ci->connectedTime = msg->getTime();
                    ci->newClient = false;  // I'll start the profile..
                    unsigned64 rtt = ci->connectedTime - ci->acceptTime;
                    ci->drift = ci->connectedTime - (con->timeStamp + (rtt>>1));
#ifdef NET_LOG_CLIENT
                    sockaddr_in addr;
                    channel->getDistantAddress(addr);
                    NetLog("Channel(%u): new client from %u.%u.%u.%u:%u, id=%d",
                           channel->getChannelId(),
                           (unsigned)IP4(addr),(unsigned)IP3(addr),(unsigned)IP2(addr),(unsigned)IP1(addr),
                           (unsigned)PORT(addr),ci->id);
#endif
                    }
                server->leave();
                }
            break;

        case MAGIC_DISCONNECT:              // client is closing the connection..
            if ( len == sizeof(ControlPacket) ) {
                ControlPacket *dis = (ControlPacket*)msg->getData();
#ifdef NET_LOG_CLIENT
                sockaddr_in addr;
                channel->getDistantAddress(addr);
                NetLog("Channel(%u): disconnecting client at %u.%u.%u.%u:%u, id=%d",
                       channel->getChannelId(),
                       (unsigned)IP4(addr),(unsigned)IP3(addr),(unsigned)IP2(addr),(unsigned)IP1(addr),
                       (unsigned)PORT(addr),dis->serial);
#endif
                server->disconnect(dis->serial,false);
                }
            break;

        case MAGIC_COMMON:                  // common messages
        case MAGIC_FILE:
            if ( len >= sizeof(CommonPacket) ) {
                server->enterRcv();         // common received-message list!
                if ( server->received )
                    server->lastReceived->next = msg;
                else
                    server->received = msg;
                server->lastReceived = msg;
                msg->next = NULL;
                server->leaveRcv();
                }
            break;

        }

    return nsNoMoreCallbacks;
}

NetStatus clientReceive ( NetMessage *msg, NetStatus event, void *data )
    // receive routine for common channel of the TestClient
{
    if ( !client || !msg ) return nsNoMoreCallbacks; // fatal error
    if ( client->terminated ) return nsNoMoreCallbacks;
    unsigned len = msg->getLength();
    if ( len < 4 ) return nsNoMoreCallbacks; // message is too small
    unsigned32 magic = *(unsigned32*)msg->getData();
    NetChannel *channel = (NetChannel*)data;
    Assert( channel );
#ifdef NET_LOG_CLIENT_RECEIVE
    NetLog("Channel(%u)::clientReceive: received message (len=%3u, serial=%4u, flags=%04x, magic=%x)",
           channel->getChannelId(),len,msg->getSerial(),(unsigned)msg->getFlags(),magic);
#endif

    switch ( magic ) {

        case MAGIC_ACCEPT:                  // connection was accepted..
            if ( len == sizeof(ControlPacket) ) {
                ControlPacket *acc = (ControlPacket*)msg->getData();
                client->enter();
                if ( acc->serial == client->playerNo &&
                     client->accepted == 0 ) {
                        // everything is OK:
                    client->acceptTime = msg->getTime();
                    client->accepted = (acc->tmp > 0) ? 1 : -1;
                    unsigned64 rtt = client->acceptTime - client->connectTime;
                    client->drift = client->acceptTime - (acc->timeStamp + (rtt>>1));
                    if ( client->accepted > 0 ) {
                        Ref<NetMessage> out = NetMessagePool::pool()->newMessage(sizeof(ControlPacket),channel);
                        if ( out ) {
                            ControlPacket *reply = (ControlPacket*)out->getData();
                            reply->magic     = MAGIC_CONNECTED;
                            reply->tmp       = 0;
                            reply->serial    = client->playerNo;
                            reply->timeStamp = getSystemTime();
                            out->setLength(sizeof(ControlPacket));
                            out->setFlags(MSG_ALL_FLAGS,MSG_VIM_FLAG|MSG_URGENT_FLAG);
                            out->send(true);
                            }
                        }
                    }
                client->leave();
                }
            break;

        case MAGIC_DISCONNECT:              // server is closing the connection..
            if ( len == sizeof(ControlPacket) ) {
                ControlPacket *dis = (ControlPacket*)msg->getData();
                client->enter();
                if ( dis->serial == client->playerNo )
                    client->terminated = true;
                client->leave();
                }
            break;

        case MAGIC_COMMON:                  // common messages
        case MAGIC_FILE:
            if ( len >= sizeof(CommonPacket) ) {
                client->enterRcv();
                if ( client->received )
                    client->lastReceived->next = msg;
                else
                    client->received = msg;
                client->lastReceived = msg;
                msg->next = NULL;
                client->leaveRcv();
                }
            break;

        }

    return nsNoMoreCallbacks;
}

//------------------------------------------------------------
//  Main routine:

void checkProfiles ()
    // creates a default profile if no explicit one was defined..
{
    if ( profiles.present(0) ) return;
    profiles.put(0,new TestProfile(0));
}

volatile bool interrupted = false;

#ifndef _WIN32

void handleInt ( int sig )
    // handles SIGINT
{
    interrupted = true;
}

char readKey ()
    // non-blocking keyboard peek
{
    fd_set set;                             // list of receiving fd's
    static struct timeval timeout =
        { 0L, 0L };                         // select() timeout: non-blocking
    FD_ZERO ( &set );
    FD_SET ( STDIN_FILENO, &set );
    int error = select(FD_SETSIZE,&set,NULL,NULL,&timeout);
    if ( error != 1 )
        return (char)0;                     // data are not ready
    return (char)getchar();
}

#endif

void doServer ()
{
    TestServer *s = new TestServer;
    Assert( s );
    s->init();
    poolLock.enter();
    printf("TestServer is listening at port %u.\n",(unsigned)getPeer()->getLocalPort());
    poolLock.leave();
    bool verbose = true;

        // server loop:
    do {

        server->spendTime(SERVER_LOOP_CYCLE);

        NetworkStatistics stat;
        server->getNetStatistics(stat);
        char buf[256];
        sprintf(buf,"(%2u):%5ui,%5uo,RTT:%4u,%4u,rcv:%4u,%2u",
                stat.numberOfClients,
                (stat.recvBytes+64)>>7,(stat.sentBytes+64)>>7,
                stat.minLatency,stat.maxLatency,
                stat.recvGood,stat.totalBad);
        printf("%4.0f%s [q,v]:",getLogTime(),buf);
        NetLog("Console%s",buf);

            // detailed info about individual clients:
        IteratorState it;
        RefD<NetChannel> itch;
        if ( verbose && server->getFirst(it,itch) )
            do {
                EnhancedBWInfo info;
                unsigned bw = itch->getOutputBandWidth(&info);
                int msgs, bytes, vimMsgs, vimBytes;
                itch->getOutputQueueStatistics(msgs,bytes,vimMsgs,vimBytes);
                sockaddr_in dist;
                itch->getDistantAddress(dist);
                sprintf(buf,"%02x.%02x.%02x.%02x:%5u:RTT:%4u,BW:%5u,%5u,%5u,%5u,q:%3dK,%3dK",
                        (unsigned)IP4(dist),(unsigned)IP3(dist),(unsigned)IP2(dist),(unsigned)IP1(dist),
                        (unsigned)PORT(dist),(itch->getLatency()+500)/1000,
                        (bw+64)>>7,(info.goodBW+64)>>7,(info.inRB+64)>>7,(info.outRB+64)>>7,
                        (bytes+512)>>10,(vimBytes+512)>>10
                        );
                printf("\n     %s",buf);
                NetLog("Console %s",buf);
                } while ( server->getNext(it,itch) );

            // keyboard command:
        int ch = 0;
#ifdef _WIN32
        if ( _kbhit() )
            ch = _getche();
#else
        ch = readKey();
#endif
        putchar('\n');
        if ( tolower(ch) == 'q' ) break;
        if ( tolower(ch) == 'v' ) verbose = !verbose;
        } while ( !interrupted && (bool)server );

#ifndef _WIN32
    putchar('\n');
#endif
    server = NULL;
}

void doClient ()
{
    TestClient *c = new TestClient;
    Assert( c );
    printf("TestClient is connecting to TestServer(%u.%u.%u.%u:%u): ",
           (unsigned)IP4(saddr),(unsigned)IP3(saddr),(unsigned)IP2(saddr),(unsigned)IP1(saddr),
           (unsigned)PORT(saddr));
    bool ok = c->open(saddr,clientProfile,replyProfile);
    printf(ok?"OK\n":"failed\n");

    if ( ok ) {
            // client loop:
        while ( !interrupted && (bool)client && !client->isTerminated() ) {

            client->spendTime(CLIENT_LOOP_CYCLE);

            NetworkStatistics stat;
            client->getNetStatistics(stat);
            char buf[256];
            sprintf(buf,":%5ui,%5uo,RTT:%4u,BW:%5u,%5u,%5u,%5u,rcv:%4u,%2u,q:%3dK,%3dK",
                    (stat.recvBytes+64)>>7,(stat.sentBytes+64)>>7,stat.minLatency,
                    (stat.outBand+64)>>7,(stat.goodBand+64)>>7,(stat.inRBBand+64)>>7,(stat.outRBBand+64)>>7,
                    stat.recvGood,stat.totalBad,
                    (stat.normalQBytes+512)>>10,(stat.vimQBytes+512)>>10);
            printf("%4.0f%s:",getLogTime(),buf);
            NetLog("Console%s",buf);

            // keyboard command:
            int ch = 0;
#ifdef _WIN32
            if ( _kbhit() )
                ch = _getche();
#else
            ch = readKey();
#endif
            putchar('\n');
            if ( tolower(ch) == 'q' ) break;
            }

#ifndef _WIN32
        putchar('\n');
#endif
        printf("TestClient has been disconnected from the server\n");
        }

    client = NULL;
}

int main ( int argc, char **argv )
{
        // initialization:
    getPool();
    getLocalAddress(saddr,FIRST_PORT_TO_TRY);
        // command-line arguments:
    int arg = 1;
    while ( arg < argc && argv[arg][0] == '-' ) { // read one "-*"  argument
        if ( !strcmp(argv[arg],"-server") ) {    // -server
            runServer = true;
            }
        else
        if ( !strcmp(argv[arg],"-client") ) {    // -client
            runServer = false;
            }
        else
        if ( !strcmp(argv[arg],"-nolog") ) {     // -nolog
            positiveCheckLogs = 0;
            }
        else
        if ( !strcmp(argv[arg],"-log") ) {       // -log <n>
            if ( ++arg < argc ) {
                positiveCheckLogs = atoi(argv[arg]);
                }
            }
        else
        if ( !strcmp(argv[arg],"-ip") ) {        // -ip <address>
            if ( ++arg < argc ) {
                getHostAddress(saddr,argv[arg],ntohs(saddr.sin_port));
                }
            }
        else
        if ( !strcmp(argv[arg],"-port") ) {      // -port <port>
            if ( ++arg < argc ) {
                saddr.sin_port = htons(atoi(argv[arg]));
                }            
            }
        else
        if ( !strcmp(argv[arg],"-lport") ) {     // -lport <port>
            if ( ++arg < argc ) {
                localPort = atoi(argv[arg]);
                }            
            }
        else
        if ( !strcmp(argv[arg],"-profiles") ) {  // -profiles <file-name>
            if ( ++arg < argc ) {
                readProfiles(argv[arg]);
                }            
            }
        else
        if ( !strcmp(argv[arg],"-profile") ) {   // -profile <K>
            if ( ++arg < argc ) {
                clientProfile = atoi(argv[arg]);
                }            
            }
        else
        if ( !strcmp(argv[arg],"-reply") ) {     // -reply <K>
            if ( ++arg < argc ) {
                replyProfile = atoi(argv[arg]);
                }            
            }
        else
        if ( !strcmp(argv[arg],"-fps") ) {       // -fps <N>
            if ( ++arg < argc ) {
                fps = atoi(argv[arg]);
                if ( fps < MIN_FPS ) fps = MIN_FPS;
                if ( fps > MAX_FPS ) fps = MAX_FPS;
                frameTime = 1000000 / fps;
                }            
            }
        else
#ifdef NET_LOG
            NetLog("Unknown command-line argument \"%s\" (will be ignored)!",argv[arg]);
#else
            printf("Unknown command-line argument \"%s\" (will be ignored)!\n",argv[arg]);
#endif
        arg++;
        }

    checkProfiles();

#ifndef _WIN32
    signal(SIGINT,handleInt);
#endif
    if ( runServer )
        doServer();
    else
        doClient();

    return 0;
}
