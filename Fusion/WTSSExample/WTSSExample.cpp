/****************************************************************************
* Plugin to demonstrate functionality needed by WTSS project
****************************************************************************/

#pragma warning(disable: 4996)
#pragma warning(disable: 4251)

#define _BISIM_DEV_CREATE_SHOT_NEW_PARAMS 1

#include <stdio.h>
#include "WTSSExample.h"

#include <util\UnitUtilities.h>
#include <util\MissionUtilities.h>
#include <util\WeaponUtilities.h>
#include <util\GeneralUtilities.h>

using namespace VBS2Fusion;

// global variables
Unit watchedUnit;                     // unit who's canStand and alive attributes will be watched
bool watchedUnitInitialized = false;  // is true when the watched unit is already created
float timeCounter = 0.0f;             // variable to count time between 2 logs

// global constants
const float LogPeriod = 2.0f;               // how often should messages be logged and shot created (in seconds)
const char *ShotName = "vbs2_ammo_R_m72a6"; // name of shot created by createShot function
const char *WeaponName = "vbs2_2a18_122mm"; // name of weapon whose sound should be played during the shot creation
const position3D ShotSpeed(0, 100, 0);      // speed of shot created by createShot function

#define _DIAG 1
#define LOG_HEADER "[WTSSExample] "
void LogDiagMessage(const char *format, ...)
{
#if _DIAG
  va_list arglist;
  va_start(arglist, format);
  char buffer[512] = { 0 };
  vsprintf_s(buffer,512,format, arglist);
  va_end(arglist);
  OutputDebugStr(buffer);
  UnitUtilities::sideChat(watchedUnit, buffer);
#endif
}

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
  if (!watchedUnitInitialized) return;

  timeCounter += deltaT;
  if (timeCounter > LogPeriod)
  {
 		/*!
		Updates  the dynamic properties of the newUnit: 
		*/
		UnitUtilities::updateDynamicProperties(watchedUnit);

    // Log messages about unit being able to stand and being alive
    timeCounter = 0.0f;
    LogDiagMessage(LOG_HEADER"Can stand: %s\n", UnitUtilities::canStand(watchedUnit) ? "yes" : "no");
    LogDiagMessage(LOG_HEADER"Is alive: %s\n", UnitUtilities::isAlive(watchedUnit) ? "yes" : "no");

    // Create shot
    position3D shotPos(watchedUnit.getPosition());
    shotPos.setZ(shotPos.z()+2.0f);
    shotPos.setY(shotPos.y()+2.0f);
    vector<double> time = GeneralUtilities::getSystemTime();
    LogDiagMessage(LOG_HEADER"Missile creation invoked at: %.0f:%02.0f:%02.0f:%03.0f", time[3], time[4], time[5], time[6]);
    WeaponUtilities::createShot(ShotName, shotPos, ShotSpeed, WeaponName);
  }
}

VBSPLUGIN_EXPORT void WINAPI OnMissionStart()
{
  /*!
  -get the player unit
  */
  Unit playerUnit = MissionUtilities::getPlayer();

  /*!
  Updates the static properties of the playerUnit: 
  -Endurance
  -Experience
  -Group
  -Leadership
  -Rank
  -Skill
  -Type.
  */

  UnitUtilities::updateDynamicProperties(playerUnit);

  /*!
  Defines a new position which is an object of the 3D position Class of VBS2Fusion
  */
  position3D newPosition;
  double randNo = rand() % 5;

  double newX, newZ, newY;
  /*!
  playerUnit.getPosition() - Return the  position of the player unit in position3D format 
  getX() - get the x cordinate - Refer to the VBS cordinate system 
  getY() - get the y cordinate - Refer to the VBS cordinate system 
  getZ() - get the z cordinate - Refer to the VBS cordinate system 
  */
  newX = playerUnit.getPosition().getX() + randNo;
  newZ = playerUnit.getPosition().getZ() + randNo;
  newY = 0;

  /*!
  Set values to new position 
  setX() - get the x coordinate - Refer to the VBS coordinate system 
  setY() - get the y coordinate - Refer to the VBS coordinate system 
  setZ() - get the z coordinate - Refer to the VBS coordinate system 
  */

  newPosition.setX(newX);
  newPosition.setZ(newZ);
  newPosition.setY(newY);

  UnitUtilities::CreateUnit(watchedUnit, newPosition);	
  watchedUnitInitialized = true;
};

// DllMain
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
  switch(fdwReason)
  {
  case DLL_PROCESS_ATTACH:
    break;
  case DLL_PROCESS_DETACH:
    break;
  case DLL_THREAD_ATTACH:
    break;
  case DLL_THREAD_DETACH:
    break;
  }
  return TRUE;
}

