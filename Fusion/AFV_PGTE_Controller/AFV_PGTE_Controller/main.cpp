#include "pluginheader.h"
using namespace VBS2Fusion;

SPINKIT_HANDLE g_hSpinHandle = NULL;		/* handle to the spinkit device */
SPINKIT_DATA g_Data;						/* data structure for spinkit controller*/
int g_rngSpinBuffer[SPIN_CONTROLLERS];		/* buffer for data from the spin controller */
int g_rngButtonBuffer[BUTTON_CONTROLLERS];	/* buffer for data from the button controllers */
volatile bool g_loaded=false;				/* boolean which tells us the spinkit DLL is loaded */

Unit g_player;								/* reference to the player unit */
Vehicle g_playerVehicle;					/* reference to the player vehicle */
Turret g_mainTurret;						/* reference to the player vehicle turret */
Turret::TurretPath g_mainTurretPath;		/* reference to the player vehicle turret path */
EVehType g_eVehicleType =  eVT_NOP;
volatile bool g_started = false;			/* boolean which tells us that the mission is started */

std::string g_szCurrentWeapon;				/* string reference to the current weapon in the turret */
std::vector<std::string> g_rngSzMuzzles;	/* vector of strings referencing the main turret muzzles */
std::string g_szHEATAmmo = "vbs2_mag_3rnd_30mm_HEI_rarden"; /* Ammo HEAT type*/
std::string g_szAPAmmo = "vbs2_mag_3rnd_30mm_APDS_rarden"; /* Ammo AP type */
float g_rardenRateOfFire;					/* rate of fire of the rarden */
float g_coaxRateOfFire;						/* coax rate of fire */
double g_rateOfFire;						/* wait on the rate of fire */
bool g_roundLoaded = true;					/* boolean to stop double firing of rarden */

std::vector<double> g_originalOpticsOffset; /* vehicle original optics offset */

double g_coarseRateDivider = 100.0f;
double g_fineRateDivider = 400.0f;
double g_elevRateDivider = 200.0f;
double g_rateDivider;						/* divider for azimuth rate */


volatile bool g_weaponChanging = false;		/* boolean to ensure single weapon changing */
volatile bool g_changeWeapon = false;
float g_timeSkip = 0.0f;					/* time skip for fire button */

EAmmoType	g_eCurrentAmmoType;				/* Enumeration for the current ammo type*/

double g_maxElevation = 35.0f;				/* maximum barrel elevation */
double g_minElevation = -10.0f;				/* minimum barrel elevation */

//////////////////////////////////////////////////////////////////////////
// Debug Trace
//////////////////////////////////////////////////////////////////////////
std::string g_szDisplayString = "";

NetworkID ConvertStringToNetworkID(std::string netID)
{
	NetworkID _netId(netID);
	return _netId;
}

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
	if(g_started && g_loaded)
	{
		// clear buffers
		memset(g_rngSpinBuffer, 0, SPIN_CONTROLLERS);
		memset(g_rngButtonBuffer, 0, BUTTON_CONTROLLERS);
		// read spincontroller
		BOOL _dataRecv = SpinKitReadNonBlocking(g_hSpinHandle, &g_Data);
		if(_dataRecv)
		{
			for(int i = 0; i < SPIN_CONTROLLERS; ++i) g_rngSpinBuffer[i] = g_Data.Spins[i];
			for(int i = 0; i < BUTTON_CONTROLLERS; ++i) g_rngButtonBuffer[i] = g_Data.Buttons[i];
		}

		// here we have a safety statement which stops erroneous values from the controller
		if((g_rngSpinBuffer[1] < -200 || g_rngSpinBuffer[1] > 200))
		{
			g_rngSpinBuffer[1] = 0;
		}

		// update player and vehicle
		UnitUtilities::updateDynamicProperties(g_player);
		VehicleUtilities::updateVehicle(g_playerVehicle);
		VehicleUtilities::updateTurrets(g_playerVehicle);

		//////////////////////////////////////////////////////////////////////////
		// Calculate Azimuth Handle Rate
		//////////////////////////////////////////////////////////////////////////
		if(g_rngButtonBuffer[6] == 1)
		{
			g_rateDivider = g_coarseRateDivider;
		}
		else
		{
			g_rateDivider = g_fineRateDivider;
		}	

		//////////////////////////////////////////////////////////////////////////
		// Weapon Direction Setting Code
		//////////////////////////////////////////////////////////////////////////
		position3D _weaponDirection = VehicleUtilities::getWeaponDirectionVector(g_playerVehicle, g_mainTurret);
		double _azimuth = RAD_TO_DEG(atan2(_weaponDirection.getX(), _weaponDirection.getZ()));
		double _elevation = RAD_TO_DEG(asin(_weaponDirection.getY()));
		_azimuth += g_rngSpinBuffer[0]/(double)g_rateDivider;
		_elevation += g_rngSpinBuffer[1]/(double)g_elevRateDivider;
		if(_elevation > g_maxElevation) _elevation = g_maxElevation;
		else if(_elevation < g_minElevation) _elevation = g_minElevation;
		VehicleUtilities::applyWeaponDirection(g_playerVehicle, g_mainTurret, _azimuth, _elevation, false);

		//////////////////////////////////////////////////////////////////////////
		// Fire button implementation code
		//////////////////////////////////////////////////////////////////////////
		if (g_roundLoaded)
		{
			if(g_rngButtonBuffer[5] == 1)
			{
				VehicleUtilities::applyFire(g_playerVehicle, g_szCurrentWeapon);
				g_roundLoaded = false;
			}		
		}
		else
		{
			g_timeSkip += deltaT;
			if(g_timeSkip > g_rateOfFire)
			{
				g_timeSkip = 0.0f;
				g_roundLoaded = true;
			}
		}
		//////////////////////////////////////////////////////////////////////////
		// Lase button implementation
		//////////////////////////////////////////////////////////////////////////
		if(g_rngButtonBuffer[1] == 1)
		{
			VehicleUtilities::applyLaserRange(g_playerVehicle, g_player);
		}

		//////////////////////////////////////////////////////////////////////////
		// Change Ammo Type implementation
		//////////////////////////////////////////////////////////////////////////
		if(g_eVehicleType == eVT_SCIMITAR)
		{
			if(g_rngButtonBuffer[2] == 1)
			{
				g_changeWeapon = true;
			}
			else if (g_rngButtonBuffer[2] == 0)
			{
				g_changeWeapon = false;
			}
		}
		else if (g_eVehicleType == eVT_WARRIOR)
		{
			if(InputUtilities::isKeyPressed(0x39))
			{
				g_changeWeapon = true;
			}
			else if(!InputUtilities::isKeyPressed(0x39))
			{
				g_changeWeapon = false;
			}

		}
		if(g_changeWeapon && !g_weaponChanging)
		{
			switch(g_eCurrentAmmoType)
			{
			case EAT_AP:
				{
					g_eCurrentAmmoType = EAT_MG1;
					g_szCurrentWeapon = g_rngSzMuzzles[1];
					VehicleUtilities::applyWeaponSelection(g_playerVehicle, g_szCurrentWeapon);
					g_rateOfFire = g_coaxRateOfFire;
					if(g_eVehicleType == eVT_SCIMITAR)
					{
						VehicleUtilities::applyOpticsOffset(g_playerVehicle, g_mainTurretPath,
							g_originalOpticsOffset[0] + 0,
							g_originalOpticsOffset[1] -0.15, false);
					}
					else if (g_eVehicleType == eVT_WARRIOR)
					{
						VehicleUtilities::applyOpticsOffset(g_playerVehicle, g_mainTurretPath,
							g_originalOpticsOffset[0] + 0,
							g_originalOpticsOffset[1], false);
					}
					break;
				}
			case EAT_MG1:
				{
					g_eCurrentAmmoType = EAT_HE;
					g_szCurrentWeapon = g_rngSzMuzzles[0];
					VehicleUtilities::applyWeaponSelection(g_playerVehicle, g_szCurrentWeapon);
					std::string _ammoCmd = stringbuilder() << "(vehicle player) loadMagazine [" << g_mainTurret.getTurretpathString() << ",\"" << g_szHEATAmmo << "\"];";
					ExecutionUtilities::ExecuteStringAndForget(_ammoCmd);
					g_rateOfFire = g_rardenRateOfFire;
					if(g_eVehicleType == eVT_SCIMITAR)
					{
						VehicleUtilities::applyOpticsOffset(g_playerVehicle, g_mainTurretPath,
							g_originalOpticsOffset[0] + 0,
							g_originalOpticsOffset[1] + 0.165, false);
					}
					else if (g_eVehicleType == eVT_WARRIOR)
					{
						VehicleUtilities::applyOpticsOffset(g_playerVehicle, g_mainTurretPath,
							g_originalOpticsOffset[0] + 0,
							g_originalOpticsOffset[1] + 0.155, false);
					}
					break;
				}
			case EAT_HE:
				{
					g_eCurrentAmmoType = EAT_MG2;
					g_szCurrentWeapon = g_rngSzMuzzles[1];
					VehicleUtilities::applyWeaponSelection(g_playerVehicle, g_szCurrentWeapon);
					g_rateOfFire = g_coaxRateOfFire;
					if(g_eVehicleType == eVT_SCIMITAR)
					{
						VehicleUtilities::applyOpticsOffset(g_playerVehicle, g_mainTurretPath,
							g_originalOpticsOffset[0] + 0,
							g_originalOpticsOffset[1] - 0.15, false);
					}
					else if (g_eVehicleType == eVT_WARRIOR)
					{
						VehicleUtilities::applyOpticsOffset(g_playerVehicle, g_mainTurretPath,
							g_originalOpticsOffset[0] + 0,
							g_originalOpticsOffset[1] , false);
					}
					break;
				}
			case EAT_MG2:
				{
					g_eCurrentAmmoType = EAT_AP;
					g_szCurrentWeapon = g_rngSzMuzzles[0];
					VehicleUtilities::applyWeaponSelection(g_playerVehicle, g_szCurrentWeapon);
					std::string _ammoCmd = stringbuilder() << "(vehicle player) loadMagazine [" << g_mainTurret.getTurretpathString() << ",\"" << g_szAPAmmo << "\"];";
					ExecutionUtilities::ExecuteStringAndForget(_ammoCmd);
					g_rateOfFire = g_rardenRateOfFire;
					if(g_eVehicleType == eVT_SCIMITAR)
					{
						VehicleUtilities::applyOpticsOffset(g_playerVehicle, g_mainTurretPath,
							g_originalOpticsOffset[0] + 0,
							g_originalOpticsOffset[1] + 0.145, false);
					}
					else if (g_eVehicleType == eVT_WARRIOR)
					{
						VehicleUtilities::applyOpticsOffset(g_playerVehicle, g_mainTurretPath,
							g_originalOpticsOffset[0] + 0,
							g_originalOpticsOffset[1] + 0.140, false);
					}
					break;
				}
			default:
				break;
			}
			g_weaponChanging = true;
		}
		else if (!g_changeWeapon && g_weaponChanging)
		{
			g_weaponChanging = false;
		}	

		//////////////////////////////////////////////////////////////////////////
		// Debug Trace
		//////////////////////////////////////////////////////////////////////////
// 		g_szDisplayString = stringbuilder() << "AzmMils: " << _azimuth * 17.7 << "\\nElevMils: " << _elevation * 17.7;
// 		DisplayFunctions::DisplayString(g_szDisplayString);
	}
	else if(GeneralUtilities::isMissionRunning())
	{


		// if spinkit is loaded then load and link references to Fusion Variables
		if(g_loaded)
		{
			std::string _getPlayerIdCmd = "ObjToIdEx player";
			std::string _getPlayerIdRet = ExecutionUtilities::ExecuteStringAndReturn(_getPlayerIdCmd);
			std::string _getPlayerVehCmd = "ObjToIdEx (vehicle player)";
			std::string _getPlayerVehRet = ExecutionUtilities::ExecuteStringAndReturn(_getPlayerVehCmd);
			if(_getPlayerIdRet.compare(_getPlayerVehRet) != 0)
			{
				// player is in a vehicle get refs to turret, weapons, magazines etc
				NetworkID _id = ConvertStringToNetworkID(_getPlayerVehRet);
				Vehicle _tempV(_id);
				g_playerVehicle = _tempV;
				VehicleUtilities::updateVehicle(g_playerVehicle);
				VehicleUtilities::updateTurrets(g_playerVehicle);
				VehicleUtilities::updateVehicleArmedProperties(g_playerVehicle);
				VehicleUtilities::updateWeaponTypes(g_playerVehicle);

				g_mainTurret = g_playerVehicle.getMainTurret();
				g_mainTurretPath = g_mainTurret.getTurretPath();
				TurretUtilities::loadWeapons(g_playerVehicle, g_mainTurret);
				TurretUtilities::loadMagazines(g_playerVehicle, g_mainTurret);

				// get our vehicle type
				std::string _szVehicleType = ControllableObjectUtilities::getType(g_playerVehicle);
				if(_szVehicleType.find("FV107_PGTE") != std::string::npos) g_eVehicleType = eVT_SCIMITAR;
				else if (_szVehicleType.find("FV510_PGTE") != std::string::npos) g_eVehicleType = eVT_WARRIOR;
				else
				{
					g_eVehicleType = eVT_NOP;
					// error message here about incorrect vehicle type
					SpinKitCloseDevice(g_hSpinHandle);
					g_loaded = false;
					return;
				}

				// get the optics offset (if there, like in the warrior)
				g_originalOpticsOffset = VehicleUtilities::getOpticsOffset(g_playerVehicle, g_mainTurretPath);
				if (g_eVehicleType == eVT_SCIMITAR)
				{
					VehicleUtilities::applyOpticsOffset(g_playerVehicle, g_mainTurretPath,
						g_originalOpticsOffset[0] + 0,
						g_originalOpticsOffset[1] + 0.145, false);
				}
				else if (g_eVehicleType == eVT_WARRIOR)
				{
					VehicleUtilities::applyOpticsOffset(g_playerVehicle, g_mainTurretPath,
						g_originalOpticsOffset[0] + 0,
						g_originalOpticsOffset[1] + 0.140, false);
				}

				// get the rate of fire of our weapons
				std::string _rardenRateOfFireCmd = "getNumber(configFile>>'CfgWeapons'>>'vbs2_l21a1_rarden'>>'FullAuto'>>'roundsPerMinute')";
				std::string _rardenRateOfFireRet = ExecutionUtilities::ExecuteStringAndReturn(_rardenRateOfFireCmd);
				g_rardenRateOfFire = 60/::atof(_rardenRateOfFireRet.c_str());

				std::string _coaxRateOfFireCmd = "getNumber(configFile>>'CfgWeapons'>>'VBS2_GB_L94A1'>>'FullAuto'>>'roundsPerMinute')";
				std::string _coaxRateOfFireRet = ExecutionUtilities::ExecuteStringAndReturn(_coaxRateOfFireCmd);
				g_coaxRateOfFire = 60/::atof(_coaxRateOfFireRet.c_str());

				// get our muzzles
				g_rngSzMuzzles = VehicleUtilities::getMuzzles(g_playerVehicle);
				g_szCurrentWeapon = g_rngSzMuzzles[0];

				// set the current magazine
				std::string _ammoCmd = stringbuilder() << "(vehicle player) loadMagazine [" << g_mainTurret.getTurretpathString() << ",\"" << g_szAPAmmo << "\"];";
				ExecutionUtilities::ExecuteStringAndForget(_ammoCmd);
				g_eCurrentAmmoType = EAT_AP;
				g_rateOfFire = g_rardenRateOfFire;
				g_roundLoaded = true;

				// disable gunner input from mouse and keyboard
				std::string _disableGunnerInputCmd = "(vehicle player) disableGunnerInput[[0], true, true];";
				ExecutionUtilities::ExecuteStringAndForget(_disableGunnerInputCmd);

				// release weapon safety
				VehicleUtilities::applyWeaponSafety(g_playerVehicle, g_mainTurret, false);

				g_started = true;
			}
			else
			{
				// error message here about the player not being in a vehicle
				SpinKitCloseDevice(g_hSpinHandle);
				g_loaded = false;
			}
		}
	}
}

VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
	static const char result[]="[1.0, 3.75]";

	return result;
}

VBSPLUGIN_EXPORT void WINAPI OnMissionStart()
{
	g_hSpinHandle = SpinKitOpenDevice();
	if(g_hSpinHandle != NULL)
	{
		// success, clear buffers
		for(int i = 0; i < SPIN_CONTROLLERS; ++i) g_rngSpinBuffer[i] = 0;
		for(int i = 0; i < BUTTON_CONTROLLERS; ++i) g_rngButtonBuffer[i] = 0;

		// set the spinkit loaded variable to true
		g_loaded = true;
	}
	else
	{
		// error report here about spinkit handle not being instantiated
		return;
	}
}

VBSPLUGIN_EXPORT void WINAPI OnMissionEnd()
{
	if(g_loaded)
	{
		SpinKitCloseDevice(g_hSpinHandle);
		g_loaded = false;
	}
	if(g_started)
	{
		std::string _disableGunnerCmd = "(vehicle player) disableGunnerInput[[0], false, true];";
		ExecutionUtilities::ExecuteStringAndForget(_disableGunnerCmd);
		VehicleUtilities::applyWeaponSafety(g_playerVehicle, g_mainTurret, true);
		g_started = false;
	}

}

BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
	switch(fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	}

	return TRUE;
};