#include <windows.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <sstream>
#include "VBS2Fusion.h"
#include "exports.h"
#include "lib/spinkit.h"

#define VBSPLUGIN_EXPORT __declspec(dllexport)
#define SPIN_CONTROLLERS 6
#define BUTTON_CONTROLLERS 7
#define DEG_TO_RAD(X) (X * M_PI/180)
#define RAD_TO_DEG(X) (X * 180/M_PI)

// ammo type enumeration
enum EAmmoType
{
	EAT_AP = 0,
	EAT_MG1,
	EAT_HE,
	EAT_MG2
};

// vehicle type enumeration
enum EVehType
{
	eVT_NOP,
	eVT_SCIMITAR,
	eVT_WARRIOR
};

struct stringbuilder
{
	std::stringstream ss;
	template<typename T>
	stringbuilder & operator << (const T &data)
	{
		ss << data;
		return *this;
	}
	operator std::string() { return ss.str(); }
};

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT);
VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input);
VBSPLUGIN_EXPORT void WINAPI OnMissionStart();
VBSPLUGIN_EXPORT void WINAPI OnMissionEnd();