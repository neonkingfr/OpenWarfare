/****************************************************************************
* Plugin for testing Fusion EventHandler                                    *
****************************************************************************/

#pragma warning(disable: 4996)
#pragma warning(disable: 4251)

#include <stdio.h>
#include "AAREventsTest.h"

#define _DIAG 1
#define LOG_HEADER "[AAREventsTest] "
void LogDiagMessage(const char *format, ...)
{
#if _DIAG
  va_list arglist;
  va_start(arglist, format);
  char buffer[512] = { 0 };
  vsprintf_s(buffer,512,format, arglist);
  va_end(arglist);
  OutputDebugString(buffer);
#endif
}

#include <Events\EventHandlerEx.h>

VBS2Fusion::EventHandlerEx eHandler;

void AmmoExplodeEventFunc(const VBS2Fusion::AmmoExplodeEvent* event)
{
  VBS2Fusion::position3D pos = event->getImpactPosition();
  LogDiagMessage(LOG_HEADER"AmmoExplodeEvent pos:[%0.2f,%0.2f,%0.2f] damage:%f \n", pos.getX(), pos.getY(), pos.getZ(), event->getAmmoExplosiveDamage());
}

void FiredEventFunc(const VBS2Fusion::FiredEvent* event)
{
  LogDiagMessage(LOG_HEADER"FiredEventFunc ammo:%s weapon:%s \n", event->getAmmo().c_str(), (const char*)event->getWeapon().c_str() );
}

VBSPLUGIN_EXPORT void WINAPI OnMissionStart()
{
  eHandler.addEventHandler(&AmmoExplodeEventFunc, "ammoExplodeTest", VBS2Fusion::AMMOEXPLODE);
  eHandler.addEventHandler(&FiredEventFunc, "firedTest", VBS2Fusion::MUZZLEFIRE);
};

// DllMain
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
  switch(fdwReason)
  {
  case DLL_PROCESS_ATTACH:
    break;
  case DLL_PROCESS_DETACH:
    break;
  case DLL_THREAD_ATTACH:
    break;
  case DLL_THREAD_DETACH:
    break;
  }
  return TRUE;
}

