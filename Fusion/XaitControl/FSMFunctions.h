#ifndef __FSMFUNCTIONS_H__
#define __FSMFUNCTIONS_H__

#include "XaitControl.h"
#include "FSMUnit.h"

void  RegisterGlobalFunctionsAndEvents(FSMInfo &info);
float Distance2(XVec3Par v1, XVec3Par v2);
float Distance(XVec3Par v1, XVec3Par v2);
float DistanceXZ2(XVec3Par v1, XVec3Par v2);
float DistanceXZ(XVec3Par v1, XVec3Par v2);

int MaxInt(int a, int b);
int MinInt(int a, int b);
float MaxFloat(float a, float b);
float MinFloat(float a, float b);

XVec3 GetMarkerPos(XStrPar str);
bool  ExistMarker(XStrPar str);
float GetRain();
int   RandomInt(int val);
float RandomFloat(float val);
int   AbsInt  (int   val);
float AbsFloat(float val);
int   ExecVM(XStrPar textArg, XStrPar filename);
bool  IsScriptDone(int scriptHandler);
void  Terminate(int scriptHandler);

int  SideWEST();
int  SideEAST();
int  SideGUER();
int  SideCIV();
XStr SideName(int side);


XUID ObjNull();

XVec3 VehicleFleeingPos(XStrPar type, XStrPar variable, int index, int side);

void GlobDebugLog2(bool cond, XStrPar text);
void GlobDebugLog(XStrPar text);
void GlobEcho2(bool cond, XStrPar text);
void GlobEcho(XStrPar text);
float GlobTime();

bool DifficultyEnabled(XStrPar name);

#endif