#include "FSMUnit.h"

#include "util\FSMUtilities.h"

using namespace VBS2Fusion;

//------------------------CONDITIONS--------------------------------
bool FSMUnit::FSMBehaviourCombat()
{
  return FsmUtilities::BehaviourCombat(Unit(_fusionUnitID));
}

bool FSMUnit::FSMVehicleAir()
{
  return FsmUtilities::VehicleAir(Unit(_fusionUnitID));
}

bool FSMUnit::FSMVehicle()
{
  return FsmUtilities::Vehicle(Unit(_fusionUnitID));
}

bool FSMUnit::FSMFormationIsMember()
{
  return FsmUtilities::FormationIsMember(Unit(_fusionUnitID));
}

bool FSMUnit::FSMFormationIsLeader()
{
  return FsmUtilities::FormationIsLeader(Unit(_fusionUnitID));
}

bool FSMUnit::FSMFormationNotEnoughCover()
{
  return FsmUtilities::FormationNotEnoughCover(Unit(_fusionUnitID));
}

bool FSMUnit::FSMReloadNeeded()
{
  return FsmUtilities::ReloadNeeded(Unit(_fusionUnitID));
}

bool FSMUnit::FSMFormationEnemy(float distance, float randomThreshold)
{
  return FsmUtilities::FormationEnemy(Unit(_fusionUnitID),distance,randomThreshold);
}

bool FSMUnit::FSMFormationTooFar()
{
  return FsmUtilities::FormationTooFar(Unit(_fusionUnitID));
}

bool FSMUnit::FSMCheckCover()
{
  return FsmUtilities::CheckCover(Unit(_fusionUnitID));
}

bool FSMUnit::FSMCoverReached()
{
  return FsmUtilities::CoverReached(Unit(_fusionUnitID));
}

bool FSMUnit::FSMRandomDelay()
{
  return FsmUtilities::RandomDelay(Unit(_fusionUnitID));
}

bool FSMUnit::FSMFormationTooFarAreCovered()
{
  return FsmUtilities::FormationTooFarAreCovered(Unit(_fusionUnitID));
}

bool FSMUnit::FSMFormationDeadlock()
{
  return FsmUtilities::FormationDeadlock(Unit(_fusionUnitID));
}

bool FSMUnit::FSMRandomDelayExt()
{
  return FsmUtilities::RandomDelayExt(Unit(_fusionUnitID));
}

bool FSMUnit::FSMCoverExist()
{
  return FsmUtilities::CoverExist(Unit(_fusionUnitID));
}


//------------------------ACTIONS--------------------------------    
void FSMUnit::FSMFormationNoCombat()
{
  FsmUtilities::FormationNoCombat(Unit(_fusionUnitID));
};

void FSMUnit::FSMFormationReturn()
{
  FsmUtilities::FormationReturn(Unit(_fusionUnitID));
};
void FSMUnit::FSMFormationExcluded()
{
  FsmUtilities::FormationExcluded(Unit(_fusionUnitID));
};

void FSMUnit::FSMFormationInit()
{
  FsmUtilities::FormationInit(Unit(_fusionUnitID));
};

void FSMUnit::FSMFormationMember()
{
  FsmUtilities::FormationMember(Unit(_fusionUnitID));
};

void FSMUnit::FSMGoToCover()
{
  FsmUtilities::GoToCover(Unit(_fusionUnitID));
};

void FSMUnit::FSMFormationProvideCover()
{
  FsmUtilities::FormationProvideCover(Unit(_fusionUnitID));
};
void FSMUnit::FSMFormationLeader()
{
  FsmUtilities::FormationLeader(Unit(_fusionUnitID));
};

void FSMUnit::FSMFormationNextTarget()
{
  FsmUtilities::FormationNextTarget(Unit(_fusionUnitID));
};

void FSMUnit::FSMFormationSetHideBehind()
{
  FsmUtilities::FormationSetHideBehind(Unit(_fusionUnitID));
};

void FSMUnit::FSMSetUnitPosToDown()
{
  FsmUtilities::SetUnitPosToDown(Unit(_fusionUnitID));
};

void FSMUnit::FSMReload()
{
  FsmUtilities::Reload(Unit(_fusionUnitID));
};

void FSMUnit::FSMFormationCleanUp()
{
  FsmUtilities::FormationCleanUp(Unit(_fusionUnitID));
};