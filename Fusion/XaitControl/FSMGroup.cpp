
#include "FSMUnit.h"

using namespace VBS2Fusion;

#include "util\WorldUtilities.h"
#include "util\VariableUtilities.h"
#include "util\GroupUtilities.h"
#include "util\VehicleUtilities.h"


void FSMUnit::RegisterGroupFunctionsAndEvents(FSMInfo &info)
{
  info._fsm->registerFunction("group.GetVariableString",              *this, &FSMUnit::GroupGetVariableString);
  info._fsm->registerFunction("group.ExistVariable",                  *this, &FSMUnit::GroupExistVariable);
  info._fsm->registerFunction("group.Size",                           *this, &FSMUnit::GroupSize);  // substitution for script command "count group _this"
  info._fsm->registerFunction("group.ForEachAllowFleeing",            *this, &FSMUnit::GroupForEachAllowFleeing);
  info._fsm->registerFunction("group.ForEachSetVariable",             *this, &FSMUnit::GroupForEachSetVariableInt);
  info._fsm->registerFunction("group.ForEachSetVariable",             *this, &FSMUnit::GroupForEachSetVariableInt_bPublic);
  info._fsm->registerFunction("group.ForEachSetVariable",             *this, &FSMUnit::GroupForEachSetVariableBool);
  info._fsm->registerFunction("group.ForEachSetVariable",             *this, &FSMUnit::GroupForEachSetVariableBool_bPublic);
  info._fsm->registerFunction("group.ForEachSetVariable",             *this, &FSMUnit::GroupForEachSetVariableStr);
  info._fsm->registerFunction("group.ForEachSetVariable",             *this, &FSMUnit::GroupForEachSetVariableStr_bPublic);
  info._fsm->registerFunction("group.ForEachSetVariable",             *this, &FSMUnit::GroupForEachSetVariableFloat);
  info._fsm->registerFunction("group.ForEachSetVariable",             *this, &FSMUnit::GroupForEachSetVariableFloat_bPublic);
  info._fsm->registerFunction("group.ForEachSetVariable",             *this, &FSMUnit::GroupForEachSetVariableVector);
  info._fsm->registerFunction("group.ForEachSetVariable",             *this, &FSMUnit::GroupForEachSetVariableVector_bPublic);

  info._fsm->registerFunction("group.ForEachSetVariableNil",          *this, &FSMUnit::GroupForEachSetVariableNil);
  info._fsm->registerFunction("group.ForEachSetVariableNil",          *this, &FSMUnit::GroupForEachSetVariableNil_bPublic);
   
  info._fsm->registerFunction("group.ForEachSetUnitPosAUTO",          *this, &FSMUnit::GroupForEachSetUnitPosAUTO);
  info._fsm->registerFunction("group.ForEachSetUnitPosDOWN",          *this, &FSMUnit::GroupForEachSetUnitPosDOWN);
  info._fsm->registerFunction("group.ForEachSetUnitPosUP",            *this, &FSMUnit::GroupForEachSetUnitPosUP);
  
  info._fsm->registerFunction("group.ForEachDoMove",                  *this, &FSMUnit::GroupForEachDoMove);
  info._fsm->registerFunction("group.ForEachDoStop",                  *this, &FSMUnit::GroupForEachDoStop);
  
  info._fsm->registerFunction("group.ForEachExecVM",                  *this, &FSMUnit::GroupForEachExecVM);  
  info._fsm->registerFunction("group.ForEachExecVM_onlyAI",           *this, &FSMUnit::GroupForEachExecVM_onlyAI);  
}

XStr FSMUnit::GroupGetVariableString(XStrPar name)
{
  Unit unit(_fusionUnitID);
  if(!ControllableObjectUtilities::ControllableObjectExists(unit))
    return "";

  UnitUtilities::updateDynamicProperties(unit);
  UnitUtilities::updateStaticProperties(unit);
  Group group = UnitUtilities::getGroup(unit);

  VBS2Variable var = GroupUtilities::getVariable(group, strconv(name));

  if (var.TypeOf() != VBS2Variable::STRING)
    return "";

  return strconv(string(var));
}

bool FSMUnit::GroupExistVariable(XStrPar name)
{
  Unit unit(_fusionUnitID);
  if(!ControllableObjectUtilities::ControllableObjectExists(unit))
    return false;

  UnitUtilities::updateDynamicProperties(unit);
  UnitUtilities::updateStaticProperties(unit);
  Group group = UnitUtilities::getGroup(unit);

  VBS2Variable var = GroupUtilities::getVariable(group,  strconv(name));

  return var.TypeOf() != VBS2Variable::NONE;
}

void FSMUnit::GroupForEachAllowFleeing(float allow)
{
  NetworkID who = _fusionUnitID;
  MAKE_AND_CHECK(;);

  Group group = UnitUtilities::getGroup(unit);

  vector<Unit> members = GroupUtilities::getUnits(group);

  for (int i=0; i< (int)members.size(); ++i)
    AllowFleeing(members[i].getNetworkID(), allow); // TODO AllowFleeing(units[i])
}

void FSMUnit::GroupForEachSetVariableInt_bPublic(   XStrPar name, int val, bool bPublic )
{
  NetworkID who = _fusionUnitID;
  MAKE_AND_CHECK(;);

  Group group = UnitUtilities::getGroup(unit);
  vector<Unit> members = GroupUtilities::getUnits(group);

  for (int i=0; i< (int)members.size(); ++i)
    SetVariableInt(members[i].getNetworkID(), name, val, bPublic); // TODO SetVariableInt(units[i], name, val, bPublic)
};

void FSMUnit::GroupForEachSetVariableBool_bPublic(  XStrPar name, bool val, bool bPublic )
{
  NetworkID who = _fusionUnitID;
  MAKE_AND_CHECK(;);

  Group group = UnitUtilities::getGroup(unit);
  vector<Unit> members = GroupUtilities::getUnits(group);

  for (int i=0; i< (int)members.size(); ++i)
    SetVariableInt(members[i].getNetworkID(), name, val, bPublic); // TODO SetVariableInt(units[i], name, val, bPublic)
};

void FSMUnit::GroupForEachSetVariableFloat_bPublic( XStrPar name, float val, bool bPublic )
{
  NetworkID who = _fusionUnitID;
  MAKE_AND_CHECK(;);

  Group group = UnitUtilities::getGroup(unit);
  vector<Unit> members = GroupUtilities::getUnits(group);

  for (int i=0; i< (int)members.size(); ++i)
    SetVariableFloat(members[i].getNetworkID(), name, val, bPublic); // TODO SetVariableInt(units[i], name, val, bPublic)
};

void FSMUnit::GroupForEachSetVariableStr_bPublic(   XStrPar name, XStrPar val, bool bPublic )
{
  NetworkID who = _fusionUnitID;
  MAKE_AND_CHECK(;);

  Group group = UnitUtilities::getGroup(unit);
  vector<Unit> members = GroupUtilities::getUnits(group);

  for (int i=0; i< (int)members.size(); ++i)
    SetVariableStr(members[i].getNetworkID(), name, val, bPublic); // TODO SetVariableInt(units[i], name, val, bPublic)
};

void FSMUnit::GroupForEachSetVariableVector_bPublic(XStrPar name, XVec3Par val, bool bPublic )
{
  NetworkID who = _fusionUnitID;
  MAKE_AND_CHECK(;);

  Group group = UnitUtilities::getGroup(unit);
  vector<Unit> members = GroupUtilities::getUnits(group);

  for (int i=0; i< (int)members.size(); ++i)
    SetVariableVector(members[i].getNetworkID(), name, val, bPublic); // TODO SetVariableInt(units[i], name, val, bPublic)
};

void FSMUnit::GroupForEachSetVariableNil_bPublic(XStrPar name, bool bPublic )
{
  NetworkID who = _fusionUnitID;
  MAKE_AND_CHECK(;);
  //VariableUtilities::setVariableNil(unit, strconv(name),  bPublic); // TODO missing in Fusion
}

int FSMUnit::GroupSize()
{
  Unit unit(_fusionUnitID);
  if(!ControllableObjectUtilities::ControllableObjectExists(unit))
    return -1;

  UnitUtilities::updateDynamicProperties(unit);
  UnitUtilities::updateStaticProperties(unit);
  Group group = UnitUtilities::getGroup(unit);

  return (int)GroupUtilities::getUnits(group).size(); // may be there is better way
}


void FSMUnit::GroupForEachSetUnitPosAUTO()
{
  NetworkID who = _fusionUnitID;
  MAKE_AND_CHECK(;);

  Group group = UnitUtilities::getGroup(unit);
  vector<Unit> members = GroupUtilities::getUnits(group);

  for (int i=0; i< (int)members.size(); ++i)
    SetUnitPosAUTO(members[i].getNetworkID());
};

void FSMUnit::GroupForEachSetUnitPosDOWN()
{
  NetworkID who = _fusionUnitID;
  MAKE_AND_CHECK(;);

  Group group = UnitUtilities::getGroup(unit);
  vector<Unit> members = GroupUtilities::getUnits(group);

  for (int i=0; i< (int)members.size(); ++i)
    SetUnitPosDOWN(members[i].getNetworkID());
};

void FSMUnit::GroupForEachSetUnitPosUP()
{
  NetworkID who = _fusionUnitID;
  MAKE_AND_CHECK(;);

  Group group = UnitUtilities::getGroup(unit);
  vector<Unit> members = GroupUtilities::getUnits(group);

  for (int i=0; i< (int)members.size(); ++i)
    SetUnitPosUP(members[i].getNetworkID());
};


//{ _x domove #pos# }forEach (units group _this);
void FSMUnit::GroupForEachDoMove(XVec3Par pos)
{
  NetworkID who = _fusionUnitID;
  MAKE_AND_CHECK(;);

  Group group = UnitUtilities::getGroup(unit);
  vector<Unit> members = GroupUtilities::getUnits(group);

  for (int i=0; i< (int)members.size(); ++i)
    UnitUtilities::applyMove(members[i],Xvec3TOpos3D(pos));
}

//{ _x domove (getpos _x) }forEach (units group _this);
void FSMUnit::GroupForEachDoStop()
{
  NetworkID who = _fusionUnitID;
  MAKE_AND_CHECK(;);

  Group group = UnitUtilities::getGroup(unit);
  vector<Unit> members = GroupUtilities::getUnits(group);

  for (int i=0; i< (int)members.size(); ++i)
    UnitUtilities::applyMove(members[i],UnitUtilities::getPosition(members[i]));
}

void FSMUnit::GroupForEachExecVM(XStrPar fileName)  
{
  NetworkID who = _fusionUnitID;
  MAKE_AND_CHECK(;);

  Group group = UnitUtilities::getGroup(unit);
  vector<Unit> members = GroupUtilities::getUnits(group);

  for (int i=0; i< (int)members.size(); ++i)
    ExecutionUtilities::ExecuteVM(members[i],strconv(fileName),true);
}

void FSMUnit::GroupForEachExecVM_onlyAI(XStrPar fileName)  
{
  NetworkID who = _fusionUnitID;
  MAKE_AND_CHECK(;);

  Group group = UnitUtilities::getGroup(unit);
  vector<Unit> members = GroupUtilities::getUnits(group);

  for (int i=0; i< (int)members.size(); ++i)
  {
    if (!UnitUtilities::isPlayerControlled(members[i]))
      ExecutionUtilities::ExecuteVM(members[i],strconv(fileName),true);
  }
}
