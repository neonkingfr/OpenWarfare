// Engine implementation of RefCount

class EngineRefCount
{
private:
  //! Number of references to this object.
  mutable int _count;
public:
  //! Default constructor.
  EngineRefCount(){_count=0;}
  //! Copy constructor (_count attribute will not be copied).
  /*!
    \param src Reference to source object.
  */
  EngineRefCount( const EngineRefCount &src ){_count=0;}
  //! Copying of object (_count attribute will not be copied).
  void operator =( const EngineRefCount &src ){} // FIX 4/6/2004 - count was zeroed here before
  //! Destructor
public:
  virtual ~EngineRefCount(){} // use destructor of derived class
  /// assert/debugging opportunity - verify object state is correct when used
  /**
   Debugging only - Never called in release build.
  */
  virtual void OnUsed() const {}
  //! Adding of reference to this object.
  /*!
    \return Number of references.
  */
  __forceinline int AddRef() const
  {
    #if _DEBUG
      if (_count==0) OnUsed();
    #endif
    int ret = ++_count;
    return ret;
  }
  //! Determines number of references to this object.
  /*!
    \return Number of references.
  */

  //! Releasing of reference to this object.
  /*!
    \return Number of references.
  */
  __forceinline int Release() const
  {
    int ret=--_count;
    if( ret==0 ) OnUnused();
    return ret;
  }
  /// destroy an object using delete
  void Destroy() const
  {
    delete const_cast<EngineRefCount *>(this);
  }
  /// called when object is no longer used and may be released
  /**
  Default implementation calls delete
  Typical override may perform some cacheing
  */
  virtual void OnUnused() const {Destroy();}
  /// check reference count
  __forceinline int RefCounter() const {return _count;}
  /// get memory used by this object
  /**
  Does not include memory used to hold this object itself,
  which is already covered by sizeof
  */
  virtual double GetMemoryUsed() const {return 0;}

  protected:
  //@{ pretend the shape is still loaded during its unloading to avoid destructor recursion
  void TempAddRef() {_count++;}
  void TempRelease() {_count--;}
  //@}
};
