#include "FSMFunctions.h"
#include "xait/common/debugger/VariableType.h"
#include "data/EnvironmentState.h"
#include "util/EnvironmentStateUtilities.h"
#include "util/ExecutionUtilities.h"
#include "util/missionutilities.h"
#include "DisplayFunctions.h"
#include "data/vbs2variable.h"
#include "util/variableutilities.h"
#include "util/GeneralUtilities.h"

extern CBGetMarkerPosType CBGetMarkerPos;

using namespace VBS2Fusion;
VBS2Fusion::EnvironmentState es;


void RegisterGlobalFunctionsAndEvents(FSMInfo &info)
{
  FSMManager* fsmManager= Control::Interface::getFSMManager();

  // EventHandlers
  info._EVupdate= fsmManager->getEventID(info._fsm->getFSMSetID(),"Loc.Update");

  FSMSetID sID = info._fsm->getFSMSetID();
  info._EVrandomDelay= fsmManager->getEventID(sID,"glob.RandomDelay");

  // Functions callbacks
  info._fsm->registerFunction("glob.GetMarkerPos",&GetMarkerPos);
  info._fsm->registerFunction("glob.ExistMarker", &ExistMarker);
  info._fsm->registerFunction("glob.Distance",    &Distance);
  info._fsm->registerFunction("glob.Distance2",   &Distance2);
  info._fsm->registerFunction("glob.DistanceXZ",  &DistanceXZ);
  info._fsm->registerFunction("glob.DistanceXZ2", &DistanceXZ2);
  
  info._fsm->registerFunction("glob.Max", &MaxInt);
  info._fsm->registerFunction("glob.Min", &MinInt);
  info._fsm->registerFunction("glob.Max", &MaxFloat);
  info._fsm->registerFunction("glob.Min", &MinFloat);
  
  info._fsm->registerFunction("glob.Random",      &RandomInt);
  info._fsm->registerFunction("glob.Random",      &RandomFloat);
  info._fsm->registerFunction("glob.Abs",         &AbsInt);
  info._fsm->registerFunction("glob.Abs",         &AbsFloat);
  info._fsm->registerFunction("glob.ExecVM",      &ExecVM);
  info._fsm->registerFunction("glob.Terminate",   &Terminate);
  info._fsm->registerFunction("glob.IsScriptDone",&IsScriptDone);

  info._fsm->registerFunction("glob.ObjNull",     &ObjNull);

  info._fsm->registerFunction("glob.VehicleFleeingPos",          &VehicleFleeingPos);

  info._fsm->registerFunction("glob.DebugLog",    &GlobDebugLog);
  info._fsm->registerFunction("glob.DebugLog",    &GlobDebugLog2);
  info._fsm->registerFunction("glob.Echo",        &GlobEcho);
  info._fsm->registerFunction("glob.Echo",        &GlobEcho2);

  info._fsm->registerFunction("glob.Time",        &GlobTime);

  info._fsm->registerFunction("glob.West",&SideWEST);
  info._fsm->registerFunction("glob.East",&SideEAST);
  info._fsm->registerFunction("glob.Resistance",&SideGUER);
  info._fsm->registerFunction("glob.Civilian",  &SideCIV);
  
  info._fsm->registerFunction("glob.SideName",&SideName);
  
  info._fsm->registerFunction("glob.DifficultyEnabled",&DifficultyEnabled);


  /*
  info._fsm->registerFunction("glob.GetRain",     &GetRain);*/ // todo create "weather."
}

float GetRain()
{
  VBS2Fusion::EnvironmentStateUtilities::loadEnvironmentState(es);
  return (float)es.getRain();
}

int RandomInt(int val)
{
  return (int)((val)*rand()/(RAND_MAX + 1));
}
float RandomFloat(float val)
{
  return val*((float)rand())/RAND_MAX;
}

int   AbsInt  (int   val) { return abs(val);}
float AbsFloat(float val) { return abs(val);}

float Distance2(XVec3Par v1, XVec3Par v2)
{
  return (v1[0]-v2[0])*(v1[0]-v2[0]) + (v1[1]-v2[1])*(v1[1]-v2[1]) +(v1[2]-v2[2])*(v1[2]-v2[2]);
}

float DistanceXZ2(XVec3Par v1, XVec3Par v2)
{
  return (v1[0]-v2[0])*(v1[0]-v2[0]) + (v1[2]-v2[2])*(v1[2]-v2[2]);
}

float Distance(  XVec3Par v1, XVec3Par v2){return sqrt(Distance2(v1,v2));}
float DistanceXZ(XVec3Par v1, XVec3Par v2){return sqrt(DistanceXZ2(v1,v2));}

int MaxInt(int a, int b) {return max(a,b);}
int MinInt(int a, int b) {return min(a,b);}
float MaxFloat(float a, float b) {return max(a,b);}
float MinFloat(float a, float b) {return min(a,b);}

XVec3 GetMarkerPos(XStrPar str) // TODO use FUSION function
{
  XVec3 result = XVec3(0.0f,0.0f,0.0f);

  if (CBGetMarkerPos)
  {
    LogDiagMessage(LOG_HEADER" called function GetMarkerPos(\"%s\") directly from FSM.\n",str.getConstCharPtr());
    float target[3];
    CBGetMarkerPos(str.getConstCharPtr(),target);
    result[0] = target[0];
    result[1] = target[2]; // swap X,Y  engine[XYZ], scripts [XZY]
    result[2] = target[1];
  }
  else
    LogDiagMessage(LOG_HEADER" GetMarkerPos in not initialized.\n");

  return result;
}

bool ExistMarker(XStrPar str) // TODO use FUSION function
{
  bool result = false;
  if (CBGetMarkerPos)
  {
    LogDiagMessage(LOG_HEADER" called function VBSExistMarker(\"%s\") directly from FSM.\n",str.getConstCharPtr());
    float target[3];
    result = CBGetMarkerPos(str.getConstCharPtr(),target)!=0;
  }
  else
    LogDiagMessage(LOG_HEADER" GetMarkerPos in not initialized.\n");

  return result;
}

int ExecVM(XStrPar textArg, XStrPar filename)
{
  return ExecutionUtilities::ExecuteVM(strconv(textArg), strconv(filename));
}

bool IsScriptDone(int scriptHandler)
{
  return ExecutionUtilities::isScriptDone(scriptHandler);
};

void Terminate(int scriptHandler)
{
  ExecutionUtilities::terminate(scriptHandler);
};

XUID ObjNull()
{
  XUID id(3, XAIT::Common::Interface::getGlobalAllocator());
  id[0] = -1;
  id[1] = -1;
  id[2] = -1;
  return id;
};

XVec3 VehicleFleeingPos(XStrPar type, XStrPar variable, int index, int side)
{
// code from original FSM, moved because of too complicated
/*{
	if ((_x getVariable "VBS2_FLEEING_POSITION" select 0) == side _this) exitWith
	{
		_objectWhereHide = _x;
	};
} forEach (allStaticVehicles "_x isKindOf 'vbs2_scriptLogic_Script'");*/
  string eval = "_x isKindOf '"+strconv(type)+"'";

  //vector<ControllableObject> objs = MissionUtilities::getAllStaticVehicle(eval);
  //for (int i =0; i< (int)objs.size(); ++i)
  //{
  //  VBS2Variable var = VariableUtilities::getVariableFromArray(objs[i],strconv(variable),index);
  //  if (var.TypeOf()==VBS2Variable::INT)
  //  {
  //    ControllableObjectUtilities::updateSide(objs[i]);
  //    if ((int)var == side)
  //    {
  //      ControllableObjectUtilities::updatePosition(objs[i]);
  //      return POS3dTOXvec3(objs[i].getPosition());
  //    }
  //  }
  //}
  return XVec3(0,0,0);
}

void GlobDebugLog2(bool cond, XStrPar text)
{
  if (cond)
  {
    string stdStr = strconv(text);
    //DisplayFunctions::DisplayDebugLog(stdStr); // TODO
  }
}
void GlobDebugLog(XStrPar text){GlobDebugLog2(true,text);}

void GlobEcho2(bool cond, XStrPar text)
{
  if (cond)
  {
    XStr msg = LOG_HEADER" "+text+"\n";
    LogDiagMessage(msg.getConstCharPtr());
  }
}
void GlobEcho(XStrPar text){GlobEcho2(true,text);}

float GlobTime()
{
  // return global script variable "time"
  VBS2Fusion::EnvironmentStateUtilities::loadEnvironmentState(es);
  return (float) es.getTimeElapsed(); 
}


int  SideWEST(){ return WEST; };
int  SideEAST(){ return EAST; };
int  SideGUER(){ return RESISTANCE; };
int  SideCIV() { return CIVILIAN; };

XStr SideName(int side)
{
  // do not use getSideString, it return different name then VBS
  // copy paste from VBS: static const EnumName SideNames[]
  switch (side)
  {
    case WEST: return "WEST";
    case EAST: return "EAST";
    case CIVILIAN: return "CIV";
    case RESISTANCE: return "GUER";
  }
  return XStrError;
};


bool DifficultyEnabled(XStrPar name)
{
  return GeneralUtilities::IsDifficultyEnabled(strconv(name));
}