#include "FSMUnit.h"
#include "FSMFunctions.h"

using namespace VBS2Fusion;
#include "util\MissionUtilities.h"
#include "util\WorldUtilities.h"
#include "util\VariableUtilities.h"
#include "util\CollisionUtilities.h"
#include "util\VisibilityUtilities.h"

extern CBGetMarkerPosType CBGetMarkerPos;

FSMUnit::FSMUnit()
{
  for (int i=0; i<7; ++i)
    _dangerPriorities[i] = -1;
};

FSMUnit::~FSMUnit()
{
  FSMManager* fsmManager= Control::Interface::getFSMManager();

  vector<FSMInfo>::iterator itInfo;
  for (itInfo = _info.begin(); itInfo != _info.end(); itInfo++)
  {
    if (itInfo->_fsm)
      fsmManager->removeFSMInstance(itInfo->_fsm->getInstanceID());
  }
};

void FSMUnit::RegisterFunctionsAndEvents(FSMInfo &info)
{
  // hardcoded engine function
  info._fsm->registerFunction("VBS.BehaviourCombat",         *this, &FSMUnit::FSMBehaviourCombat);
  info._fsm->registerFunction("VBS.VehicleAir",              *this, &FSMUnit::FSMVehicleAir);
  info._fsm->registerFunction("VBS.Vehicle",                 *this, &FSMUnit::FSMVehicle);
  info._fsm->registerFunction("VBS.FormationIsMember",       *this, &FSMUnit::FSMFormationIsMember);
  info._fsm->registerFunction("VBS.FormationIsLeader",       *this, &FSMUnit::FSMFormationIsLeader);
  info._fsm->registerFunction("VBS.FormationNotEnoughCover", *this, &FSMUnit::FSMFormationNotEnoughCover);
  info._fsm->registerFunction("VBS.ReloadNeeded",            *this, &FSMUnit::FSMReloadNeeded);
  info._fsm->registerFunction("VBS.FormationEnemy",          *this, &FSMUnit::FSMFormationEnemy);
  info._fsm->registerFunction("VBS.FormationTooFar",         *this, &FSMUnit::FSMFormationTooFar);
  info._fsm->registerFunction("VBS.CheckCover",              *this, &FSMUnit::FSMCheckCover);
  info._fsm->registerFunction("VBS.CoverReached",            *this, &FSMUnit::FSMCoverReached);
  info._fsm->registerFunction("VBS.RandomDelay",             *this, &FSMUnit::FSMRandomDelay);
  info._fsm->registerFunction("VBS.FormationTooFarAreCovered",*this,&FSMUnit::FSMFormationTooFarAreCovered);
  info._fsm->registerFunction("VBS.FormationDeadlock",       *this, &FSMUnit::FSMFormationDeadlock);
  info._fsm->registerFunction("VBS.RandomDelayExt",          *this, &FSMUnit::FSMRandomDelayExt);
  info._fsm->registerFunction("VBS.CoverExist",              *this, &FSMUnit::FSMCoverExist); 

  info._fsm->registerFunction("VBS.FormationNoCombat",       *this, &FSMUnit::FSMFormationNoCombat);
  info._fsm->registerFunction("VBS.FormationReturn",         *this, &FSMUnit::FSMFormationReturn);
  info._fsm->registerFunction("VBS.FormationExcluded",       *this, &FSMUnit::FSMFormationExcluded);
  info._fsm->registerFunction("VBS.FormationInit",           *this, &FSMUnit::FSMFormationInit);
  info._fsm->registerFunction("VBS.FormationMember",         *this, &FSMUnit::FSMFormationMember);
  info._fsm->registerFunction("VBS.GoToCover",               *this, &FSMUnit::FSMGoToCover);
  info._fsm->registerFunction("VBS.FormationProvideCover",   *this, &FSMUnit::FSMFormationProvideCover);
  info._fsm->registerFunction("VBS.FormationLeader",         *this, &FSMUnit::FSMFormationLeader);
  info._fsm->registerFunction("VBS.FormationNextTarget",     *this, &FSMUnit::FSMFormationNextTarget);
  info._fsm->registerFunction("VBS.FormationSetHideBehind",  *this, &FSMUnit::FSMFormationSetHideBehind);
  info._fsm->registerFunction("VBS.SetUnitPosToDown",        *this, &FSMUnit::FSMSetUnitPosToDown);
  info._fsm->registerFunction("VBS.Reload",                  *this, &FSMUnit::FSMReload);
  info._fsm->registerFunction("VBS.FormationCleanUp",        *this, &FSMUnit::FSMFormationCleanUp);

/*  info._fsm->registerFunction("VBS.CoverReached",         *this, &FSMUnit::CoverReached); /NG
  info._fsm->registerFunction("VBS.FormationCanLeaveCover",*this,&FSMUnit::FormationCanLeaveCover); //NG
  info._fsm->registerFunction("VBS.Surended",             *this, &FSMUnit::Surended); // NG
  info._fsm->registerFunction("VBS.SearchPath",           *this, &FSMUnit::SearchPath); //NG*/

  REGISTER_FCE(unit)
  REGISTER_FCE(leader)
  REGISTER_FCE(formationLeader)
  REGISTER_FCE(effectiveCommander)

  info._fsm->registerFunction("danger.IsInDanger",   *this, &FSMUnit::DangerIsInDanger);
  info._fsm->registerFunction("danger.Close",        *this, &FSMUnit::DangerClose);
  info._fsm->registerFunction("danger.Cause",        *this, &FSMUnit::DangerCause);
  info._fsm->registerFunction("danger.UpdateBiggestDanger",        *this, &FSMUnit::DangerUpdateBiggestDanger);
  info._fsm->registerFunction("danger.Pos",          *this, &FSMUnit::DangerPos);
  info._fsm->registerFunction("danger.Until",        *this, &FSMUnit::DangerUntil);
  info._fsm->registerFunction("danger.SetPriorities",*this, &FSMUnit::DangerSetPriorities);

  info._fsm->registerFunction("vehicle.IsKindOf",    *this, &FSMUnit::vehicle_IsKindOf);
  info._fsm->registerFunction("vehicle.DoMove",      *this, &FSMUnit::vehicle_DoMove);

  // special case, "call" have to pass unit as "_this" automatically
  info._fsm->registerFunction("glob.Call",           *this, &FSMUnit::ExecuteStringAndReturn);
}


//  create functions for all known FSM object a.g. unit.Alive() leaser.Alive()
DEFINE_FCE(unit)
DEFINE_FCE(leader)
DEFINE_FCE(formationLeader)
DEFINE_FCE(effectiveCommander)


VBS2Fusion::NetworkID FSMUnit::Get_leader()
{
  Unit unit(_fusionUnitID);
  if(!ControllableObjectUtilities::ControllableObjectExists(unit))
    return VBS2Fusion::NetworkID();

  UnitUtilities::updateDynamicProperties(unit);
  UnitUtilities::updateStaticProperties(unit);

  return  UnitUtilities::getGroupLeader(unit).getNetworkID();
};

VBS2Fusion::NetworkID FSMUnit::Get_formationLeader()
{
  Unit unit(_fusionUnitID);
  if(!ControllableObjectUtilities::ControllableObjectExists(unit))
    return VBS2Fusion::NetworkID();

  UnitUtilities::updateDynamicProperties(unit);
  UnitUtilities::updateStaticProperties(unit);

  return UnitUtilities::getFormLeader(unit).getNetworkID();
};

// get EffectiveCommander of unit, in vehicle it's commander of vehicle otherwise it's leader of unit
VBS2Fusion::NetworkID FSMUnit::Get_effectiveCommander()
{
  Unit unit(_fusionUnitID);
  if(!ControllableObjectUtilities::ControllableObjectExists(unit))
    return VBS2Fusion::NetworkID();

  Unit effectiveCommander = unit;

  UnitUtilities::updateDynamicProperties(unit);
  UnitUtilities::updateStaticProperties(unit);

  Vehicle veh = UnitUtilities::getVehicle(unit);
  VehicleUtilities::updateDynamicProperties(veh);
  VehicleUtilities::updateStaticProperties(veh);

  if ( VehicleUtilities::isInVehicle(veh, unit))
    effectiveCommander = VehicleUtilities::getEffectiveCommander(veh);

  return effectiveCommander.getNetworkID();
};

void FSMUnit::InitUnit(int64 id[3])
{
  _fusionUnitID = NetID(id);
}

XStr FSMUnit::GetName(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(XStrError);
  return strconv(unit.getName());
};

int FSMUnit::GetSide(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(INT_MIN);
  return unit.getSide();
};

XStr FSMUnit::GetSideString(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(XStrError);
  return SideName(unit.getSide());
};

XStr FSMUnit::GetType(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(XStrError);
  return strconv(unit.getType());
};

XVec3 FSMUnit::GetPosition(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(XVec3Error);
  return POS3dTOXvec3(unit.getPosition());
}

XVec3 FSMUnit::GetPositionASL(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(XVec3Error);
  return POS3dTOXvec3(unit.getPositionASL());
}

bool FSMUnit::Alive(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(false);
  return unit.getAlive();
}

int FSMUnit::CaptiveNum(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(false);
  return UnitUtilities::getCaptiveNumber(unit);
}

float FSMUnit::GetDamage(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(FloatError);
  return (float)unit.getDamage();
}

float FSMUnit::GetSpeed(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(FloatError);
  return (float)unit.getSpeed();
}

float FSMUnit::GetDirection(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(FloatError);
  return (float)unit.getDirection();
}

int FSMUnit::GetNumberOfWeapons(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(-1);
  UnitUtilities::updateWeaponTypes(unit); // check if needed
  return unit.getNumberOfWeapons();
}

int FSMUnit::GetNumberOfMagazines(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(-1);
  UnitUtilities::updateWeaponTypes(unit);
  return unit.getNumberOfMagazines();
}

float FSMUnit::GetMoraleLevel(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(FloatError);
  return (float)UnitUtilities::getMoraleLevel(unit);
};

float FSMUnit::GetMoraleLongTerm(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(FloatError);
  return (float)UnitUtilities::getMoraleLongTerm(unit);
};

XStr FSMUnit::GetRank(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(XStrError);
  return strconv(UnitUtilities::getRank(unit));
}

float FSMUnit::GetSkill(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(FloatError);
  return (float)UnitUtilities::getSkill(unit);
}

XStr FSMUnit::GetGroup(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(XStrError);
  return strconv(UnitUtilities::getGroup(unit));
}

float FSMUnit::GetEndurance(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(FloatError);
  return (float)UnitUtilities::getEndurance(unit);
}

float FSMUnit::GetExperience(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(FloatError);
  return (float)UnitUtilities::getExperience(unit);
}

float FSMUnit::GetFatigue(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(FloatError);
  return (float)UnitUtilities::getFatigue(unit);
}

float FSMUnit::GetLeadership(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(FloatError);
  return (float)UnitUtilities::getLeadership(unit);
}

XUID FSMUnit::GetID(VBS2Fusion::NetworkID who)
{
  // be sure that VBS2Fusion::NetworkID is still same
  if(sizeof(VBS2Fusion::NetworkID)!=sizeof(int)+sizeof(int)+sizeof(INT64))
  {
    LogDiagMessage(LOG_HEADER" [error] size of VBS2Fusion::NetworkID was changed\n");
    __debugbreak();
  }

  XUID id(3, XAIT::Common::Interface::getGlobalAllocator());
  id[0] = who.getVal1();
  id[1] = who.getVal2();
  id[2] = who.getVal3();
  return id;
}

XStr FSMUnit::GetUnitPos(VBS2Fusion::NetworkID who)
{
  static XStr unitpos[] = {"UP" , "DOWN" , "AUTO"};
  MAKE_AND_CHECK(XStrError);
  return unitpos[(int)UnitUtilities::getUnitPos(unit)];
}

XStr FSMUnit::PistolWeapon(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(XStrError);
  return strconv(UnitUtilities::getPistolWeapon(unit));
}


XStr FSMUnit::PrimaryWeapon(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(XStrError);
  return strconv(UnitUtilities::getPrimaryWeapon(unit));
}

XStr FSMUnit::GetGroupSpeedMode(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(XStrError);
  Group group1 = Group(UnitUtilities::getGroup(unit));
  GroupUtilities::updateGroup(group1);

  return strconv(GroupUtilities::getSpeedMode(group1));
}

void FSMUnit::SetCaptive(VBS2Fusion::NetworkID who, int val)
{
  MAKE_AND_CHECK(;);
  UnitUtilities::applyCaptive(unit, val);
}

void FSMUnit::SetDamage(VBS2Fusion::NetworkID who, float val)
{
  MAKE_AND_CHECK(;);
  unit.setDamage(val);
}

void FSMUnit::SetSpeed(VBS2Fusion::NetworkID who, float val)
{
  MAKE_AND_CHECK(;);
  unit.setSpeed(val);
}

void FSMUnit::SetGroupSpeedMode(VBS2Fusion::NetworkID who, XStrPar val)
{
  MAKE_AND_CHECK(;);

  Group group1 = Group(UnitUtilities::getGroup(unit));
  GroupUtilities::updateGroup(group1);

  const char * str = val.getConstCharPtr();

  if (stricmp(str,"UNCHANGD")==0 || stricmp(str,"UNCHANGED")==0)
    GroupUtilities::applySpeedMode(group1,UNCHANGD);
  else if (stricmp(str,"LIMITED")==0)
    GroupUtilities::applySpeedMode(group1,LIMITED);
  else if (stricmp(str,"NORMAL")==0)
      GroupUtilities::applySpeedMode(group1,NORMAL);
  else if (stricmp(str,"FULL")==0)
        GroupUtilities::applySpeedMode(group1,FULL);
  else {/* TODO ERR message*/};
}

bool FSMUnit::ReloadNeeded(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(false);
  return UnitUtilities::getNeedReload(unit) >=1.0;
}

void FSMUnit::Reload(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(;);
  UnitUtilities::reloadWeapons(unit);
}

void FSMUnit::SetBehaviourInt(VBS2Fusion::NetworkID who, int val)
{
  MAKE_AND_CHECK(;);
  UnitUtilities::setUnitBehaviour(unit,(BEHAVIOUR) val);
}

void FSMUnit::SetBehaviourStr(VBS2Fusion::NetworkID who, XStrPar val)
{
  MAKE_AND_CHECK(;);
  const char *str = val.getConstCharPtr();

  if (stricmp(str,"CARELESS")==0)
    UnitUtilities::setUnitBehaviour(unit,B_CARELESS);
  else   if (stricmp(str,"SAFE")==0)
    UnitUtilities::setUnitBehaviour(unit,B_SAFE);
  else if (stricmp(str,"AWARE")==0)
    UnitUtilities::setUnitBehaviour(unit,B_AWARE);
  else if (stricmp(str,"COMBAT")==0)
    UnitUtilities::setUnitBehaviour(unit,B_COMBAT);
  else if (stricmp(str,"STEALTH")==0)
    UnitUtilities::setUnitBehaviour(unit,B_STEALTH);
  else {/* TODO ERR message*/};
}

void FSMUnit::CommandStop(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(;);
  UnitUtilities::commandStop(unit);
}

void FSMUnit::DoFollow(VBS2Fusion::NetworkID who, XUIDPar id)
{
  if (id.size()!=3)
    return; 

  // be sure that VBS2Fusion::NetworkID is still same
  if(sizeof(VBS2Fusion::NetworkID)!=sizeof(int)+sizeof(int)+sizeof(INT64))
  {
    LogDiagMessage(LOG_HEADER" [error] size of VBS2Fusion::NetworkID was changed\n");
    __debugbreak();
  }

  MAKE_AND_CHECK(;);

  VBS2Fusion::NetworkID followerID(NetID(id));

  Unit follower(followerID);
  if(!ControllableObjectUtilities::ControllableObjectExists(follower))
    return;
  UnitUtilities::updateStaticProperties(follower);
  UnitUtilities::updateDynamicProperties(follower);
  vector<ControllableObject> flv;
  flv.push_back(follower);

  ControllableObjectUtilities::doFollow(flv, unit);
}

void FSMUnit::DoMove(VBS2Fusion::NetworkID who, XVec3Par pos)
{
  MAKE_AND_CHECK(;);
  UnitUtilities::applyMove(unit,Xvec3TOpos3D(pos));
}

void FSMUnit::vehicle_DoMove(XVec3Par pos)
{
  NetworkID who = _fusionUnitID;
  MAKE_AND_CHECK(;);

  Vehicle veh = UnitUtilities::getVehicle(unit);
  if (ControllableObjectUtilities::ControllableObjectExists(veh)) // isFreeSoldier
  {
    VehicleUtilities::updateDynamicProperties(veh);
    VehicleUtilities::updateStaticProperties(veh);
    VehicleUtilities::applyMove(veh,Xvec3TOpos3D(pos));    
  }
  else
    UnitUtilities::applyMove(unit,Xvec3TOpos3D(pos));
}

void FSMUnit::DoWatch(VBS2Fusion::NetworkID who, XVec3Par pos)
{
  MAKE_AND_CHECK(;);
  UnitUtilities::doWatch(unit,Xvec3TOpos3D(pos));
}

void FSMUnit::SetUnitPosAUTO(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(;);
  UnitUtilities::unitPosAUTO(unit);
}

void FSMUnit::SetUnitPosDOWN(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(;);
  UnitUtilities::unitPosDOWN(unit);
}

void FSMUnit::SetUnitPosUP(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(;);
  UnitUtilities::unitPosUP(unit);
}


bool FSMUnit::InVehicle(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(false);
  Vehicle veh = UnitUtilities::getVehicle(unit);
  VehicleUtilities::updateDynamicProperties(veh);
  VehicleUtilities::updateStaticProperties(veh);

  return VehicleUtilities::isInVehicle(veh, unit); // TODO
  // todo use proper isFreeSoldier or isInVehicle
}


void FSMUnit::SetDestination(VBS2Fusion::NetworkID who, XVec3Par pos)
{
  MAKE_AND_CHECK(;);
  bool forceReplan = UnitUtilities::getExpectedDestinationForceReplan(unit);
  string mode = UnitUtilities::getExpectedDestinationPlanMode(unit);
  ControllableObjectUtilities::setDestination(unit, Xvec3TOpos3D(pos), mode, forceReplan);  
}

void FSMUnit::SetDestination3(VBS2Fusion::NetworkID who, XVec3Par pos, XStrPar mode, bool forceReplan)
{
  MAKE_AND_CHECK(;);
  ControllableObjectUtilities::setDestination(unit, Xvec3TOpos3D(pos), strconv(mode), forceReplan);  
}

bool  FSMUnit::SomeAmmo(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(false);
  return UnitUtilities::hasSomeAmmo(unit);
}

float Distance2(const position3D &v1, const position3D &v2)
{
  return (float)((v1.x()-v2.x())*(v1.x()-v2.x()) + (v1.y()-v2.y())*(v1.y()-v2.y()) +(v1.z()-v2.z())*(v1.z()-v2.z()));
}

XStr FSMUnit::GetBehavior(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(XStrError);
  return strconv(UnitUtilities::getUnitBehaviour(unit));
}

const char* VBS2VariableTypeNames[] = {"STRING", "INT", "FLOAT", "DOUBLE", "BOOL", "POSITION", "NONE"};

bool FSMUnit::GetVariableBool(VBS2Fusion::NetworkID who, XStrPar name)
{
  MAKE_AND_CHECK(false);
  VBS2Variable var = VariableUtilities::getVariable(unit, strconv(name));

  if (var.TypeOf() != VBS2Variable::BOOL)
  {
    LogDiagMessage(LOG_HEADER" [error] GetVariableBool(%s) expected type BOOL, detected %s\n", name.getConstCharPtr(), VBS2VariableTypeNames[var.TypeOf()]);
    return false;
  }

  return (bool)var;
}

int FSMUnit::GetVariableInt(VBS2Fusion::NetworkID who, XStrPar name)
{
  MAKE_AND_CHECK(INT_MIN);
  VBS2Variable var = VariableUtilities::getVariable(unit, strconv(name));

  if (var.TypeOf() != VBS2Variable::INT)
  {
    LogDiagMessage(LOG_HEADER" [error] GetVariableInt(%s) expected type INT, detected %s\n", name.getConstCharPtr(), VBS2VariableTypeNames[var.TypeOf()]);
    return INT_MIN;
  }

  return (int)var;
}

float FSMUnit::GetVariableFloat(VBS2Fusion::NetworkID who, XStrPar name)
{
  MAKE_AND_CHECK(FloatError);
  VBS2Variable var = VariableUtilities::getVariable(unit, strconv(name));

  if (var.TypeOf() == VBS2Variable::INT)
    return (float)(int) var;
    
  if (var.TypeOf() == VBS2Variable::FLOAT)
    return (float) var;
    
  if (var.TypeOf() == VBS2Variable::DOUBLE)
    return (float)(double) var;

  LogDiagMessage(LOG_HEADER" [error] GetVariableFloat(%s) expected type Float, detected %s\n", name.getConstCharPtr(), VBS2VariableTypeNames[var.TypeOf()]);
  return FloatError;    
}

XStr FSMUnit::GetVariableString(VBS2Fusion::NetworkID who, XStrPar name)
{
  MAKE_AND_CHECK(XStrError);
  VBS2Variable var = VariableUtilities::getVariable(unit, strconv(name));

  if (var.TypeOf() != VBS2Variable::STRING)
  {
    LogDiagMessage(LOG_HEADER" [error] GetVariableString(%s) expected type String, detected %s\n", name.getConstCharPtr(), VBS2VariableTypeNames[var.TypeOf()]);
    return XStrError;
  }
  return strconv(var);
}


XVec3 FSMUnit::GetVariableVector(VBS2Fusion::NetworkID who, XStrPar name)
{
  MAKE_AND_CHECK(XVec3Error);
  VBS2Variable var = VariableUtilities::getVariable(unit, strconv(name));

  if (var.TypeOf() != VBS2Variable::POSITION)
  {
    LogDiagMessage(LOG_HEADER" [error] GetVariableVector(%s) expected type VECTOR, detected %s\n", name.getConstCharPtr(), VBS2VariableTypeNames[var.TypeOf()]);
    return XVec3Error;
  }
  return POS3dTOXvec3(var); 
}

XVec3 FSMUnit::FindCover(VBS2Fusion::NetworkID who, XVec3Par pos, XVec3Par hidePos, float maxDist, float minDist)
{
  MAKE_AND_CHECK(XVec3Error);
  //warning C4244, findCover min max should be float [VBS-12159]
  return POS3dTOXvec3(UnitUtilities::findCover(unit, Xvec3TOpos3D(pos), Xvec3TOpos3D(hidePos), maxDist, minDist));
}


// hardcoded script {_this distance _x < val} count (units group _this - [_this]) !=0
bool FSMUnit::AnyGroupMemberCloserThen(VBS2Fusion::NetworkID who, float val)
{
  MAKE_AND_CHECK(false);
  Group group = UnitUtilities::getGroup(unit);

  position3D thisPos = unit.getPosition();
  vector<Unit> units = GroupUtilities::getUnits(group);

  for (int i=0; i< (int)units.size(); ++i)
  {
    if (units[i]==unit) continue;
    if (Distance2(units[i].getPosition(), thisPos) < val*val)
      return true;
  }
  return false;
};

float FSMUnit::Distance(VBS2Fusion::NetworkID who,  XVec3Par pos)
{
  MAKE_AND_CHECK(FloatError);
  return sqrt(Distance2(Xvec3TOpos3D(pos), unit.getPosition()));
}

bool FSMUnit::ExistVariable(VBS2Fusion::NetworkID who, XStrPar name)
{
  MAKE_AND_CHECK(false);
  VBS2Variable var = VariableUtilities::getVariable(unit, strconv(name));
  return var.TypeOf() != VBS2Variable::NONE;
}

int FSMUnit::ExecVM(VBS2Fusion::NetworkID who, XStrPar fileName)
{
  MAKE_AND_CHECK(false);
  return ExecutionUtilities::ExecuteVM(unit,strconv(fileName),true);
}

bool FSMUnit::IsCollision(VBS2Fusion::NetworkID who, XVec3Par pos)
{
  MAKE_AND_CHECK(false);
  return CollisionUtilities::isCollision(Xvec3TOpos3D(pos),unit);
};


bool FSMUnit::IsKindOf(VBS2Fusion::NetworkID who, XStrPar name)
{
  MAKE_AND_CHECK(false);
  return UnitUtilities::isKindOf(unit,strconv(name));
}

bool FSMUnit::vehicle_IsKindOf(XStrPar name)
{
  NetworkID who = _fusionUnitID;
  MAKE_AND_CHECK(false);

  Vehicle veh = UnitUtilities::getVehicle(unit);
  VehicleUtilities::updateDynamicProperties(veh);
  VehicleUtilities::updateStaticProperties(veh);

  if ( VehicleUtilities::isInVehicle(veh, unit)) // isFreeSoldier
    return VehicleUtilities::isKindOf(veh,strconv(name));

  //else
  return UnitUtilities::isKindOf(unit,strconv(name));
}

void FSMUnit::SetHideBehind(VBS2Fusion::NetworkID who,     XUIDPar id  , XVec3Par  pos)
{
  if (id.size()!=3)
    return; 
  MAKE_AND_CHECK(;);
  ControllableObject target(NetID(id));
  UnitUtilities::setHideBehind(unit,target,Xvec3TOpos3D(pos));
}


void FSMUnit::SetVariableInt(VBS2Fusion::NetworkID who, XStrPar name, int val, bool bPublic)
{
  MAKE_AND_CHECK(;);
  VariableUtilities::setVariable(unit, strconv(name), val, bPublic);
}

void FSMUnit::SetVariableBool(VBS2Fusion::NetworkID who, XStrPar name, bool val, bool bPublic)
{
  MAKE_AND_CHECK(;);
  VariableUtilities::setVariable(unit, strconv(name), val, bPublic);
}

void FSMUnit::SetVariableFloat(VBS2Fusion::NetworkID who, XStrPar name, float val, bool bPublic)
{
  MAKE_AND_CHECK(;);
  VariableUtilities::setVariable(unit, strconv(name), val, bPublic);
}

void FSMUnit::SetVariableStr(VBS2Fusion::NetworkID who, XStrPar name, XStrPar val, bool bPublic)
{
  MAKE_AND_CHECK(;);
  VariableUtilities::setVariable(unit, strconv(name), strconv(val), bPublic);
}

void FSMUnit::SetVariableVector(VBS2Fusion::NetworkID who, XStrPar name, XVec3Par val, bool bPublic)
{
  MAKE_AND_CHECK(;);
  VariableUtilities::setVariable(unit, strconv(name), Xvec3TOpos3D(val), bPublic);
}

void FSMUnit::SetVariableNil(VBS2Fusion::NetworkID who, XStrPar name, bool bPublic)
{
  MAKE_AND_CHECK(;);
  VariableUtilities::setVariableNil(unit, strconv(name),  bPublic);
}

void FSMUnit::OrderGetIn(VBS2Fusion::NetworkID who,  bool val)
{
  MAKE_AND_CHECK(;);
  VehicleUtilities::orderGetIn(unit, val);
}



bool FSMUnit::Fleeing(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(false);
  return UnitUtilities::isFleeing(unit);
}

float FSMUnit::GetFleeing(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(FloatError);
  return (float)UnitUtilities::getFleeing(unit);
}

void FSMUnit::AllowFleeing(VBS2Fusion::NetworkID who, float allow)
{
  MAKE_AND_CHECK(;);
  UnitUtilities::allowFleeing(unit,allow);
}


void FSMUnit::PlayMove(VBS2Fusion::NetworkID who, XStrPar name)
{
  MAKE_AND_CHECK(;);
  UnitUtilities::playMove(unit, strconv(name));
}

bool FSMUnit::IsFormationLeader(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(false);
  return UnitUtilities::isFormationLeader(unit);
}

bool FSMUnit::IsAttached(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(false);
  return UnitUtilities::isAttached(unit);
}

bool FSMUnit::IsHidden(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(false);
  return UnitUtilities::isHidden(unit);
}

bool FSMUnit::IsHideBehindScripted(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(false);
  return UnitUtilities::isHideBehindScripted(unit);
}

bool FSMUnit::IsLeader(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(false);
  return UnitUtilities::getGroupLeader(unit)== unit;
}

bool FSMUnit::IsEffectiveCommander(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(false);
  return Unit(Get_effectiveCommander()) == unit;
}

int FSMUnit::IndexInGroup(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(INT_MIN);
  Group group = UnitUtilities::getGroup(unit);

  vector<Unit> units = GroupUtilities::getUnits(group);
  for (int i=0; i< (int)units.size(); ++i)
    if (units[i]==unit)
      return i;

  return INT_MIN;
};

bool FSMUnit::NearCapturer(VBS2Fusion::NetworkID who, float distance, XVec3Par relativePos)
{
  MAKE_AND_CHECK(false);
  //** original FSM script **//
  //if(!(
  //{((_this distance (vehicle _x)) <= 50)&& (_this countEnemy [_x] == 1) &&
  //(side _x != side _this)&&(((vehicle _x) getvisibility (_this modelToWorld [0,0,1.5])) > 0)&&
  //(if((primaryweapon _x)in[""""])then{if((pistolweapon _x)in[""""])then{false}else{true};}else{true})
  //}count allunits > 0))then{nic = [_this] execVM ""\vbs2\people\data\scripts\quit_surrender_pick_up_weapons.sqf"";true}else{false};
  vector<Unit> units = MissionUtilities::getAllUnits();
  int side = unit.getSide();
  position3D pos = Xvec3TOpos3D(relativePos);
  for (int i = 0; i < (int)units.size(); ++i)
  { 
    Unit &tested = units[i];
    if(!ControllableObjectUtilities::ControllableObjectExists(tested))continue;
    
    UnitUtilities::updateStaticProperties(tested);
    UnitUtilities::updateDynamicProperties(tested);

    if (side != tested.getSide() )
    {
      list<Unit> testedArr;
      testedArr.push_back(tested);
      
      position3D tPos;
      Vehicle veh = UnitUtilities::getVehicle(tested);
      if (ControllableObjectUtilities::ControllableObjectExists(veh))
        tPos = VehicleUtilities::getPosition(veh);
      else
        tPos = UnitUtilities::getPosition(tested);

      if (MissionUtilities::countEnemy(unit, testedArr)==1 && Distance2(unit.getPosition(), tPos) <= distance*distance && 
          UnitUtilities::getPrimaryWeapon(tested) != "" && UnitUtilities::getPistolWeapon(tested) != "")
      {
        if (ControllableObjectUtilities::ControllableObjectExists(veh))
        {
          // in vehicle
          if (VisibilityUtilities::getVisibility(veh,   UnitUtilities::modelToWorld(unit, pos)) > 0) 
            return true; 
        } else
        {
          // free soldier
          if (VisibilityUtilities::getVisibility(tested,UnitUtilities::modelToWorld(unit, pos)) > 0) 
            return true; 
        }
      }
    }
  }
  return false;
}

// default LowFriendNumbers(1, 20.0, 2.0)
// threshold - true if count of friendly units including caller equal or lower 
// distance - radius where to search for units
// factor - return true if countDeath/factor > countAlive
bool FSMUnit::LowFriendsNumbers(VBS2Fusion::NetworkID who, int threshold, float distance, float factor)
{
  //** original FSM script **//
  //if(
  //(
  //  (
  //    {((_this distance (vehicle _x)) <= 20)&&!(alive _x)&& (side [_x,true] == side _this)} count allunits)/2 > 
  //    {((_this distance (vehicle _x)) <= 20)&& (alive _x)&& (side _x == side _this)} count allunits
  //  ) || 
  //  (
  //    {
  //      ((_this distance (vehicle _x)) <= 20)&&(alive _x)&& (side _x == side _this)
  //    } count allunits == 1
  //  ) 
  //)then{true}else{false};
  
  MAKE_AND_CHECK(false);
  int uSide = unit.getSide();
  position3D uPos = UnitUtilities::getPosition(unit);

  vector<Unit> units = MissionUtilities::getAllUnits();

  int countAlive = 0;
  int countDeath = 0;
  for (int i=0; i< (int)units.size(); ++i)
  {
    Unit & tested = units[i] ;
    UnitUtilities::updateDynamicProperties(tested);
    UnitUtilities::updateStaticProperties(tested); 
    
    position3D tPos;
    Vehicle veh = UnitUtilities::getVehicle(tested);
    if (ControllableObjectUtilities::ControllableObjectExists(veh))
      tPos = VehicleUtilities::getPosition(veh);
    else
      tPos = UnitUtilities::getPosition(tested);
        
    if (UnitUtilities::isAlive(tested)) 
    {
      if (tested.getSide() == uSide && Distance2(uPos, tPos) <= distance*distance)
       ++countAlive;
    }
    else
    {
      if (UnitUtilities::getSideAsIfAlive(tested)==uSide && Distance2(uPos, tPos) <= distance*distance)
        ++countDeath;      
    }
  }
  return countAlive <= threshold || (countDeath > countAlive * factor);
}

bool FSMUnit::OutNumbered(VBS2Fusion::NetworkID who, float distance)
{
//** original FSM script **//
//if(
//{
//  ((_this distance (vehicle _x)) <= 100)&&(_this countEnemy [_x] == 1) && (alive _x) && (side _x != side _this)&&
//  (if ((primaryweapon _x)in[""""]) then {if((pistolweapon _x)in[""""])then{false}else{true};}else{true})
//} count allunits
// >= 
//{((_this distance (vehicle _x)) <= 100)&&(alive _x)&& (_this countFriendly [_x] == 1)
//} count allunits
//)then{true}else{false};

  MAKE_AND_CHECK(false);
  int uSide = unit.getSide();
  position3D uPos = UnitUtilities::getPosition(unit);

  vector<Unit> units = MissionUtilities::getAllUnits();

  int countFriend = 0;
  int countEnemy = 0;
  for (int i=0; i< (int)units.size(); ++i)
  {
    Unit & tested = units[i] ;
    UnitUtilities::updateDynamicProperties(tested);
    UnitUtilities::updateStaticProperties(tested); 
    if (UnitUtilities::isAlive(tested))
    {
      list<Unit> testedArr;
      testedArr.push_back(tested);
          
      position3D tPos;
      Vehicle veh = UnitUtilities::getVehicle(tested);
      if (ControllableObjectUtilities::ControllableObjectExists(veh))
        tPos = VehicleUtilities::getPosition(veh);
      else
        tPos = UnitUtilities::getPosition(tested);
        
      if (uSide != tested.getSide() && MissionUtilities::countEnemy(unit, testedArr)==1 && Distance2(unit.getPosition(), tPos) <= distance*distance && 
        UnitUtilities::getPrimaryWeapon(tested) != "" && UnitUtilities::getPistolWeapon(tested) != "")   
          ++countEnemy;
         
      if (MissionUtilities::countFriendly(unit, testedArr)==1 && Distance2(unit.getPosition(), tPos) <= distance*distance)
          ++countFriend;         
    }
  }
  return countEnemy >= countFriend;
}


void FSMUnit::ForceSpeed(VBS2Fusion::NetworkID who, float val)
{
  MAKE_AND_CHECK(;);
  UnitUtilities::forceSpeed(unit, val );
}

float FSMUnit::GetForceSpeed(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(FloatError);
  return (float)ControllableObjectUtilities::getForceSpeed(unit);
}

void FSMUnit::HideBehindScripted(VBS2Fusion::NetworkID who, bool hide)
{
  MAKE_AND_CHECK(;);
  UnitUtilities::hideBehindScripted(unit, hide);
}


XVec3 FSMUnit::ModelToWorld(VBS2Fusion::NetworkID who, XVec3Par pos)
{
  MAKE_AND_CHECK(XVec3Error);
  return POS3dTOXvec3(UnitUtilities::modelToWorld(unit, Xvec3TOpos3D(pos)));
}

XVec3 FSMUnit::WorldToModel(VBS2Fusion::NetworkID who, XVec3Par pos)
{
  MAKE_AND_CHECK(XVec3Error);
  return POS3dTOXvec3(UnitUtilities::worldToModel(unit, Xvec3TOpos3D(pos)));
}

XVec3 FSMUnit::ModelToWorldASL(VBS2Fusion::NetworkID who, XVec3Par pos)
{
  MAKE_AND_CHECK(XVec3Error);
  return POS3dTOXvec3(UnitUtilities::modelToWorldASL(unit, Xvec3TOpos3D(pos)));
}

XVec3 FSMUnit::WorldToModelASL(VBS2Fusion::NetworkID who, XVec3Par pos)
{
  MAKE_AND_CHECK(XVec3Error);
  return POS3dTOXvec3(UnitUtilities::worldToModelASL(unit, Xvec3TOpos3D(pos)));
}

XVec3 FSMUnit::GetExpectedDestinationPos(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(XVec3Error);
  return POS3dTOXvec3(UnitUtilities::getExpectedDestinationPos(unit));
}

bool FSMUnit::GetExpectedDestinationForceReplan(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(false);
  return UnitUtilities::getExpectedDestinationForceReplan(unit);
}

XStr FSMUnit::GetExpectedDestinationMode(VBS2Fusion::NetworkID who)
{
  MAKE_AND_CHECK(XStrError);
  return strconv(UnitUtilities::getExpectedDestinationPlanMode(unit));
}


bool  FSMUnit::DangerIsInDanger() {return _danger._cause!=-1;};
int   FSMUnit::DangerCause(){ return _danger._cause; };
XVec3 FSMUnit::DangerPos()  { return _danger._pos;   };
float FSMUnit::DangerUntil(){ return _danger._until; };
void  FSMUnit::DangerUpdateBiggestDanger()
{
//  original FSM script
/*  _funcBiggestDanger = 
  {
    private ["_priorCur"];
    _priorCur = _priors select _dangerCause;

    for "_i" from 0 to ((count _queue) - 1) do 
    {
      private ["_event", "_cause", "_priorNew"];
      _event = _queue select _i;
      _cause = _event select 0;
      _priorNew = _priors select _cause;

      if (_priorNew > _priorCur) exitWith 
      {
        _oldDangerCause = _dangerCause;
        _dangerCause = _cause;
        _dangerPos = _event select 1;
        _dangerUntil = _event select 2;
      };
    };

    _queue = [];
  };*/
  
  std::vector<DangerInfo>::iterator it;
  for (it = _dangerQueue.begin(); it != _dangerQueue.end(); ++it)
  {
    if ( _dangerPriorities[it->_cause] > _dangerPriorities[_danger._cause])
    _danger = *it;
  }  
  _dangerQueue.clear();
};

void  FSMUnit::DangerClose()
{
  _danger._cause = -1;
  _dangerQueue.clear();
};

void  FSMUnit::DangerSetPriorities(int p1, int p2, int p3, int p4, int p5, int p6, int p7)
{
  _dangerPriorities[0] = p1;
  _dangerPriorities[1] = p2;
  _dangerPriorities[2] = p3;
  _dangerPriorities[3] = p4;
  _dangerPriorities[4] = p5;
  _dangerPriorities[5] = p6;
  _dangerPriorities[6] = p7;
};

// script command "call compile"
XStr FSMUnit::ExecuteStringAndReturn(XStrPar str)
{
  // we need to pass at least "unit" as "_this" into called script, script called by "call compile" can see and alter all variables from script that call it. it is more like run-time include  
  NetworkID who = _fusionUnitID;
  MAKE_AND_CHECK(XStrError);
  return strconv(ExecutionUtilities::ExecuteStringAndReturn(unit, strconv(str), false));
}