#ifndef __FSMUNIT_H__
#define __FSMUNIT_H__

#include "XaitControl.h"

#include "../../Common/BIVector.h"
#include "../../Common/BIMatrix.h"
#include "data/unit.h"
#include "data/Vehicle.h"
#include "EngineRefCount.h"

#include "util/UnitUtilities.h"

#define FUNCTIONS0(XX,YY) \
  XX(Alive              ,     ,YY , bool)\
  XX(CaptiveNum         ,     ,YY , int)\
  XX(GetBehavior        ,     ,YY , XStr)\
  XX(GetDamage          ,     ,YY , float)\
  XX(GetDirection       ,     ,YY , float)\
  XX(GetEndurance       ,     ,YY , float)\
  XX(GetExpectedDestinationPos         ,     ,YY , XVec3)\
  XX(GetExpectedDestinationForceReplan ,     ,YY , bool)\
  XX(GetExpectedDestinationMode        ,     ,YY , XStr)\
  XX(GetExperience      ,     ,YY , float)\
  XX(GetFatigue         ,     ,YY , float)\
  XX(GetFleeing         ,     ,YY , float)\
  XX(GetForceSpeed      ,     ,YY , float)\
  XX(GetGroup           ,     ,YY , XStr)\
  XX(GetGroupSpeedMode  ,     ,YY , XStr)\
  XX(GetLeadership      ,     ,YY , float)\
  XX(GetID              ,     ,YY , XUID)\
  XX(GetName            ,     ,YY , XStr)\
  XX(GetNumberOfWeapons ,     ,YY , int)\
  XX(GetNumberOfMagazines,    ,YY , int)\
  XX(GetMoraleLevel     ,     ,YY , float)\
  XX(GetMoraleLongTerm  ,     ,YY , float)\
  XX(GetPosition        ,     ,YY , XVec3)\
  XX(GetPositionASL     ,     ,YY , XVec3)\
  XX(GetRank            ,     ,YY , XStr)\
  XX(GetSide            ,     ,YY , int)\
  XX(GetSideString      ,     ,YY , XStr)\
  XX(GetSpeed           ,     ,YY , float)\
  XX(GetSkill           ,     ,YY , float)\
  XX(GetType            ,     ,YY , XStr)\
  XX(GetUnitPos         ,     ,YY , XStr)\
  XX(Fleeing            ,     ,YY , bool)\
  XX(IsEffectiveCommander,     ,YY , bool)\
  XX(IsAttached         ,     ,YY , bool)\
  XX(IsHidden           ,     ,YY , bool)\
  XX(IsHideBehindScripted,    ,YY , bool)\
  XX(IsLeader           ,     ,YY , bool)\
  XX(IsFormationLeader  ,     ,YY , bool)\
  XX(IndexInGroup       ,     ,YY , int)\
  XX(InVehicle          ,     ,YY , bool)\
  XX(PrimaryWeapon      ,     ,YY , XStr)\
  XX(PistolWeapon       ,     ,YY , XStr)\
  XX(ReloadNeeded       ,     ,YY , bool)\
  XX(SomeAmmo           ,     ,YY , bool)\

#define FUNCTIONS1(XX,YY) \
  XX(AnyGroupMemberCloserThen,,YY , bool  , float)/* hardcoded script {_this distance _x < val} count (units group _this - [_this]) !=0*/\
  XX(Distance           ,     ,YY , float , XVec3Par)\
  XX(ExistVariable      ,     ,YY , bool  , XStrPar)\
  XX(ExecVM             ,     ,YY , int   , XStrPar)\
  XX(IsCollision        ,     ,YY , bool  , XVec3Par)\
  XX(IsKindOf           ,     ,YY , bool  , XStrPar)\
  XX(GetVariableBool    ,     ,YY , bool  , XStrPar)\
  XX(GetVariableInt     ,     ,YY , int   , XStrPar)\
  XX(GetVariableFloat   ,     ,YY , float , XStrPar)\
  XX(GetVariableString  ,     ,YY , XStr  , XStrPar)\
  XX(GetVariableVector  ,     ,YY , XVec3 , XStrPar)\
  XX(ModelToWorld       ,     ,YY , XVec3 , XVec3Par)\
  XX(ModelToWorldASL    ,     ,YY , XVec3 , XVec3Par)\
  XX(WorldToModel       ,     ,YY , XVec3 , XVec3Par)\
  XX(WorldToModelASL    ,     ,YY , XVec3 , XVec3Par)\
  XX(OutNumbered        ,     ,YY , bool  , float)\


#define FUNCTIONS2(XX,YY) \
  XX(NearCapturer     ,       ,YY , bool , float, XVec3Par)\

#define FUNCTIONS3(XX,YY) \
  XX(LowFriendsNumbers,       ,YY , bool , int, float, float)\

#define FUNCTIONS4(XX,YY) \
  XX(FindCover        ,       ,YY , XVec3, XVec3Par, XVec3Par, float, float)\


#define PROCEDURES0(XX,YY) \
  XX(CommandStop        ,     ,YY)\
  XX(Reload             ,     ,YY)\
  XX(SetUnitPosAUTO     ,     ,YY)\
  XX(SetUnitPosDOWN     ,     ,YY)\
  XX(SetUnitPosUP       ,     ,YY)\

#define PROCEDURES1(XX,YY) \
  XX(AllowFleeing       ,     ,YY , float)\
  XX(DoFollow           ,     ,YY , XUIDPar)\
  XX(DoMove             ,     ,YY , XVec3Par)\
  XX(DoWatch            ,     ,YY , XVec3Par)\
  XX(ForceSpeed         ,     ,YY , float)\
  XX(HideBehindScripted ,     ,YY , bool)\
  XX(PlayMove           ,     ,YY , XStrPar)\
  XX(SetCaptive,        ,      YY , int)\
  XX(SetDamage          ,     ,YY , float)\
  XX(SetDestination     ,     ,YY , XVec3Par)\
  XX(SetGroupSpeedMode  ,     ,YY , XStrPar)\
  XX(SetSpeed           ,     ,YY , float)\
  XX(SetBehaviour       ,Str  ,YY , XStrPar)\
  XX(SetBehaviour       ,Int  ,YY , int)\
  XX(SetVariableNil     ,     ,YY , XStrPar)\
  XX(OrderGetIn         ,     ,YY , bool)\


#define PROCEDURES2(XX,YY)\
  XX(SetHideBehind      ,         ,YY , XUIDPar, XVec3Par  )\
  XX(SetVariable        ,Bool     ,YY , XStrPar, bool    )\
  XX(SetVariable        ,Int      ,YY , XStrPar, int     )\
  XX(SetVariable        ,Float    ,YY , XStrPar, float   )\
  XX(SetVariable        ,Str      ,YY , XStrPar, XStrPar )\
  XX(SetVariable        ,Vector   ,YY , XStrPar, XVec3Par)\
  XX(SetVariableNil     ,_bPublic ,YY , XStrPar, bool)\

#define PROCEDURES3(XX,YY) \
  XX(SetDestination     ,3                ,YY , XVec3Par, XStrPar , bool)\
  XX(SetVariable        ,Bool_bPublic     ,YY , XStrPar , bool    , bool)\
  XX(SetVariable        ,Int_bPublic      ,YY , XStrPar , int     , bool)\
  XX(SetVariable        ,Float_bPublic    ,YY , XStrPar , float   , bool)\
  XX(SetVariable        ,Str_bPublic      ,YY , XStrPar , XStrPar , bool)\
  XX(SetVariable        ,Vector_bPublic   ,YY , XStrPar , XVec3Par, bool)\

#define PROCEDURES4(XX,YY)




#define DEC_FCE0(name, spec, who, RET) \
  RET who##_##name##spec();

#define DEC_FCE1(name, spec, who, RET, VAL1) \
  RET who##_##name##spec(VAL1 val);

#define DEC_FCE2(name, spec, who, RET, VAL1, VAL2) \
  RET who##_##name##spec(VAL1 val1, VAL2 val2);

#define DEC_FCE3(name, spec, who, RET, VAL1, VAL2, VAL3) \
  RET who##_##name##spec(VAL1 val1, VAL2 val2, VAL3 val3);

#define DEC_FCE4(name, spec, who, RET, VAL1, VAL2, VAL3, VAL4) \
  RET who##_##name##spec(VAL1 val1, VAL2 val2, VAL3 val3, VAL4 val4);

#define DEC_PROC0(name, spec, who) \
  void who##_##name##spec();

#define DEC_PROC1(name, spec, who, VAL1) \
  void who##_##name##spec(VAL1 val);

#define DEC_PROC2(name, spec, who, VAL1, VAL2) \
  void who##_##name##spec(VAL1 val1, VAL2 val2);

#define DEC_PROC2(name, spec, who, VAL1, VAL2) \
  void who##_##name##spec(VAL1 val1, VAL2 val2);

#define DEC_PROC3(name, spec, who, VAL1, VAL2, VAL3) \
  void who##_##name##spec(VAL1 val1, VAL2 val2, VAL3 val3);

#define DEC_PROC4(name, spec, who, VAL1, VAL2, VAL3, VAL4) \
  void who##_##name##spec(VAL1 val1, VAL2 val2, VAL3 val3, VAL4 val4);


#define DECLARE_FCE(XX) \
  FUNCTIONS0(DEC_FCE0,XX) \
  FUNCTIONS1(DEC_FCE1,XX) \
  FUNCTIONS2(DEC_FCE2,XX) \
  FUNCTIONS3(DEC_FCE3,XX) \
  FUNCTIONS4(DEC_FCE4,XX) \
  PROCEDURES0(DEC_PROC0,XX) \
  PROCEDURES1(DEC_PROC1,XX) \
  PROCEDURES2(DEC_PROC2,XX) \
  PROCEDURES3(DEC_PROC3,XX) \
  PROCEDURES4(DEC_PROC4,XX) \


#define DEF_FCE0(name, spec, who, RET) \
  RET FSMUnit::who##_##name##spec() {return name##spec##(Get_##who##());};

#define DEF_FCE1(name, spec, who, RET, VAL1) \
  RET FSMUnit::who##_##name##spec(VAL1 val) {return name##spec##(Get_##who##(), val);};

#define DEF_FCE2(name, spec, who, RET, VAL1, VAL2) \
  RET FSMUnit::who##_##name##spec(VAL1 val1, VAL2 val2) {return name##spec##(Get_##who##(), val1, val2);};

#define DEF_FCE3(name, spec, who, RET, VAL1, VAL2, VAL3) \
  RET FSMUnit::who##_##name##spec(VAL1 val1, VAL2 val2, VAL3 val3) {return name##spec##(Get_##who##(), val1, val2, val3);};

#define DEF_FCE4(name, spec, who, RET, VAL1, VAL2, VAL3, VAL4) \
  RET FSMUnit::who##_##name##spec(VAL1 val1, VAL2 val2, VAL3 val3, VAL4 val4) {return name##spec##(Get_##who##(), val1, val2, val3, val4);};

#define DEF_PROC0(name, spec, who) \
  void FSMUnit::who##_##name##spec() {name##spec##(Get_##who##());};

#define DEF_PROC1(name, spec, who, VAL1) \
  void FSMUnit::who##_##name##spec(VAL1 val) {name##spec##(Get_##who##(), val);};

#define DEF_PROC2(name, spec, who, VAL1, VAL2) \
  void FSMUnit::who##_##name##spec(VAL1 val1, VAL2 val2) {name##spec##(Get_##who##(), val1, val2);};

#define DEF_PROC3(name, spec, who, VAL1, VAL2, VAL3) \
  void FSMUnit::who##_##name##spec(VAL1 val1, VAL2 val2, VAL3 val3) {name##spec##(Get_##who##(), val1, val2, val3);};

#define DEF_PROC4(name, spec, who, VAL1, VAL2, VAL3, VAL4) \
  void FSMUnit::who##_##name##spec(VAL1 val1, VAL2 val2, VAL3 val3, VAL4 val4) {name##spec##(Get_##who##(), val1, val2, val3, val4);};


#define DEFINE_FCE(XX) \
  FUNCTIONS0(DEF_FCE0,XX) \
  FUNCTIONS1(DEF_FCE1,XX) \
  FUNCTIONS2(DEF_FCE2,XX) \
  FUNCTIONS3(DEF_FCE3,XX) \
  FUNCTIONS4(DEF_FCE4,XX) \
  PROCEDURES0(DEF_PROC0,XX) \
  PROCEDURES1(DEF_PROC1,XX) \
  PROCEDURES2(DEF_PROC2,XX) \
  PROCEDURES3(DEF_PROC3,XX) \
  PROCEDURES4(DEF_PROC4,XX) \

#define REGISTER_FC(name, spec, who) \
  info._fsm->registerFunction(#who"."#name,    *this, &FSMUnit::##who##_##name##spec);

#define REG_FCE0(name, spec, who, RET)         REGISTER_FC(name, spec, who);
#define REG_FCE1(name, spec, who, RET, VAL1)   REGISTER_FC(name, spec, who);
#define REG_FCE2(name, spec, who, RET, VAL1, VAL2) REGISTER_FC(name, spec, who);
#define REG_FCE3(name, spec, who, RET, VAL1, VAL2, VAL3) REGISTER_FC(name, spec, who);
#define REG_FCE4(name, spec, who, RET, VAL1, VAL2, VAL3, VAL4)REGISTER_FC(name, spec, who);
#define REG_PROC0(name, spec, who)             REGISTER_FC(name, spec, who);
#define REG_PROC1(name, spec, who, VAL1)       REGISTER_FC(name, spec, who);
#define REG_PROC2(name, spec, who, VAL1, VAL2) REGISTER_FC(name, spec, who);
#define REG_PROC3(name, spec, who, VAL1, VAL2, VAL3) REGISTER_FC(name, spec, who);
#define REG_PROC4(name, spec, who, VAL1, VAL2, VAL3, VAL4)    REGISTER_FC(name, spec, who);

#define REGISTER_FCE(XX) \
  FUNCTIONS0(REG_FCE0,XX) \
  FUNCTIONS1(REG_FCE1,XX) \
  FUNCTIONS2(REG_FCE2,XX) \
  FUNCTIONS3(REG_FCE3,XX) \
  FUNCTIONS4(REG_FCE4,XX) \
  PROCEDURES0(REG_PROC0,XX) \
  PROCEDURES1(REG_PROC1,XX) \
  PROCEDURES2(REG_PROC2,XX) \
  PROCEDURES3(REG_PROC3,XX) \
  PROCEDURES4(REG_PROC4,XX) \


void SetUnitBehaviourStr(Common::String val);

struct FSMInfo
{
  FSMInstance* _fsm;
  FSMEventID _EVupdate;
  FSMEventID _EVrandomDelay;
  string _name; // needed for restart

  FSMInfo(FSMInstance* fsm, const string &name):_fsm(fsm),_name(name){};
};

struct DangerInfo
{
  int _cause;
  XVec3 _pos;
  float _until;
  DangerInfo():_cause(-1),_pos(0,0,0),_until(-1){}
  DangerInfo(const int c, const XVec3 p, const float u):_cause(c),_pos(p),_until(u){}
};

class FSMUnit : public RefCount
{
private:
  vector<FSMInfo> _info;
  VBS2Fusion::NetworkID _fusionUnitID;

  int _dangerPriorities[7];
  DangerInfo _danger;
  std::vector<DangerInfo> _dangerQueue;
  

  VBS2Fusion::NetworkID Get_unit() {return _fusionUnitID;};
  VBS2Fusion::NetworkID Get_leader();
  VBS2Fusion::NetworkID Get_formationLeader();
  VBS2Fusion::NetworkID Get_effectiveCommander();
public:
  Common::String GetName(){return GetName(_fusionUnitID);};

  vector<FSMInfo>       & GetInfo() {return _info;};
  VBS2Fusion::NetworkID & GetFusionID() {return _fusionUnitID;};

  DangerInfo &GetDangerInfo(){return _danger;}
  std::vector<DangerInfo> &GetDangerQueue(){return _dangerQueue;}


  FSMUnit();
  ~FSMUnit();

  void RegisterFunctionsAndEvents(FSMInfo &info);
  void RegisterGroupFunctionsAndEvents(FSMInfo &info);
  void InitUnit(int64 id[3]);

  // hardcoded FSM code in engine callbacks, may be use different prefix VBS?
/*  void FSMSearchPath(float maxCost, float maxDelay); // NG only
  bool FSMCoverReached();
  bool FSMFormationCanLeaveCover();
  bool FSMSurended();*/

  bool FSMBehaviourCombat();
  bool FSMVehicleAir();
  bool FSMVehicle();
  bool FSMFormationIsMember();
  bool FSMFormationIsLeader();
  bool FSMFormationNotEnoughCover();
  bool FSMReloadNeeded();
  bool FSMFormationEnemy(float distance, float randomThreshold);
  bool FSMFormationTooFar();
  bool FSMCheckCover();
  bool FSMCoverReached();
  bool FSMRandomDelay();
  bool FSMFormationTooFarAreCovered();
  bool FSMFormationDeadlock();
  bool FSMRandomDelayExt();
  bool FSMCoverExist();

  void FSMFormationNoCombat();
  void FSMFormationReturn();
  void FSMFormationExcluded();
  void FSMFormationInit();
  void FSMFormationMember();
  void FSMGoToCover();
  void FSMFormationProvideCover();
  void FSMFormationLeader();
  void FSMFormationNextTarget();
  void FSMFormationSetHideBehind();
  void FSMSetUnitPosToDown();
  void FSMReload();
  void FSMFormationCleanUp();


  // Functions (no arguments)
  bool  Alive(VBS2Fusion::NetworkID who);
  int   CaptiveNum(VBS2Fusion::NetworkID who);
  XStr  GetBehavior(VBS2Fusion::NetworkID who);
  float GetDamage(VBS2Fusion::NetworkID who);
  float GetDirection(VBS2Fusion::NetworkID who);
  float GetEndurance(VBS2Fusion::NetworkID who);
  XVec3 GetExpectedDestinationPos(VBS2Fusion::NetworkID who);
  bool  GetExpectedDestinationForceReplan(VBS2Fusion::NetworkID who);
  XStr  GetExpectedDestinationMode(VBS2Fusion::NetworkID who);  
  float GetExperience(VBS2Fusion::NetworkID who);
  float GetFatigue(VBS2Fusion::NetworkID who);
  float GetFleeing(VBS2Fusion::NetworkID who);
  float GetForceSpeed(VBS2Fusion::NetworkID who);
  XStr  GetGroup(VBS2Fusion::NetworkID who);
  XStr  GetGroupSpeedMode(VBS2Fusion::NetworkID who);
  float GetLeadership(VBS2Fusion::NetworkID who);
  XUID  GetID(VBS2Fusion::NetworkID who);
  XStr  GetName(VBS2Fusion::NetworkID who);
  int   GetNumberOfWeapons(VBS2Fusion::NetworkID who);
  int   GetNumberOfMagazines(VBS2Fusion::NetworkID who);
  float GetMoraleLevel(VBS2Fusion::NetworkID who);
  float GetMoraleLongTerm(VBS2Fusion::NetworkID who);
  XVec3 GetPosition(VBS2Fusion::NetworkID who);
  XVec3 GetPositionASL(VBS2Fusion::NetworkID who);
  XStr  PistolWeapon(VBS2Fusion::NetworkID who);
  XStr  PrimaryWeapon(VBS2Fusion::NetworkID who);
  XStr  GetRank(VBS2Fusion::NetworkID who);
  int   GetSide(VBS2Fusion::NetworkID who);
  XStr  GetSideString(VBS2Fusion::NetworkID who);
  float GetSpeed(VBS2Fusion::NetworkID who);
  float GetSkill(VBS2Fusion::NetworkID who);
  XStr  GetType(VBS2Fusion::NetworkID who);
  XStr  GetUnitPos(VBS2Fusion::NetworkID who);
  bool  Fleeing(VBS2Fusion::NetworkID who);
  bool  IsEffectiveCommander(VBS2Fusion::NetworkID who);
  bool  IsFormationLeader(VBS2Fusion::NetworkID who);
  bool  IsAttached(VBS2Fusion::NetworkID who);
  bool  IsHidden(VBS2Fusion::NetworkID who);
  bool  IsHideBehindScripted(VBS2Fusion::NetworkID who);
  bool  IsLeader(VBS2Fusion::NetworkID who);
  int   IndexInGroup(VBS2Fusion::NetworkID who);
  bool  NearCapturer(VBS2Fusion::NetworkID who, float distance, XVec3Par relativePos);  
  bool  InVehicle(VBS2Fusion::NetworkID who);
  bool  ReloadNeeded(VBS2Fusion::NetworkID who);
  bool  SomeAmmo(VBS2Fusion::NetworkID who);

  // Functions (1 argument)
  bool  AnyGroupMemberCloserThen(VBS2Fusion::NetworkID who, float val);
  float Distance(VBS2Fusion::NetworkID who,          XVec3Par pos);
  bool  ExistVariable(VBS2Fusion::NetworkID who,     XStrPar name);
  int   ExecVM(VBS2Fusion::NetworkID who,            XStrPar fileName);
  bool  IsCollision(VBS2Fusion::NetworkID who,       XVec3Par pos);
  bool  IsKindOf(VBS2Fusion::NetworkID who,          XStrPar name);
  bool  vehicle_IsKindOf(XStrPar name);
  bool  GetVariableBool(VBS2Fusion::NetworkID who,   XStrPar name);
  int   GetVariableInt(VBS2Fusion::NetworkID who,    XStrPar name);
  float GetVariableFloat(VBS2Fusion::NetworkID who,  XStrPar name);
  XStr  GetVariableString(VBS2Fusion::NetworkID who, XStrPar name);
  XVec3 GetVariableVector(VBS2Fusion::NetworkID who, XStrPar name);
  bool  OutNumbered(VBS2Fusion::NetworkID who, float distance);
  
  // Functions (3 arguments)
  bool  LowFriendsNumbers(VBS2Fusion::NetworkID who, int threshold, float distance, float factor);  

  // Functions (4 arguments)
  XVec3 FindCover(VBS2Fusion::NetworkID who, XVec3Par pos, XVec3Par hidePos, float maxDist, float minDist);



  // procedures (no arguments)
  void CommandStop(VBS2Fusion::NetworkID who);
  void Reload(VBS2Fusion::NetworkID who);
  void SetDestination(VBS2Fusion::NetworkID who, XVec3Par pos);
  void SetDestination3(VBS2Fusion::NetworkID who, XVec3Par pos, XStrPar mode, bool forceReplan);
  void SetUnitPosAUTO(VBS2Fusion::NetworkID who);
  void SetUnitPosDOWN(VBS2Fusion::NetworkID who);
  void SetUnitPosUP(VBS2Fusion::NetworkID who);

  // procedures (1 argument)
  void AllowFleeing(VBS2Fusion::NetworkID who,        float allow);
  void DoFollow(VBS2Fusion::NetworkID who,            XUIDPar pos);
  void DoMove(VBS2Fusion::NetworkID who,              XVec3Par pos);
  void vehicle_DoMove(XVec3Par pos);
  void DoWatch(VBS2Fusion::NetworkID who,             XVec3Par pos);
  void HideBehindScripted(VBS2Fusion::NetworkID who,  bool hide);
  void ForceSpeed(VBS2Fusion::NetworkID who,          float val);
  void PlayMove(VBS2Fusion::NetworkID who,            XStrPar name);
  void SetCaptive(VBS2Fusion::NetworkID who,          int val);
  void SetDamage(VBS2Fusion::NetworkID who,           float val);
  void SetGroupSpeedMode(VBS2Fusion::NetworkID who,   XStrPar val);
  void SetSpeed(VBS2Fusion::NetworkID who,            float val);
  void SetBehaviourInt(VBS2Fusion::NetworkID who,     int val);
  void SetBehaviourStr(VBS2Fusion::NetworkID who,     XStrPar val);
  void OrderGetIn(VBS2Fusion::NetworkID who,          bool val);

  // procedures (2 arguments)
  void SetHideBehind(VBS2Fusion::NetworkID who,     XUIDPar id  , XVec3Par  pos);
  void SetVariableInt(VBS2Fusion::NetworkID who,    XStrPar name, int val, bool bPublic = true);
  void SetVariableBool(VBS2Fusion::NetworkID who,   XStrPar name, bool val, bool bPublic = true);
  void SetVariableFloat(VBS2Fusion::NetworkID who,  XStrPar name, float val, bool bPublic = true);
  void SetVariableStr(VBS2Fusion::NetworkID who,    XStrPar name, XStrPar val, bool bPublic = true);
  void SetVariableVector(VBS2Fusion::NetworkID who, XStrPar name, XVec3Par val, bool bPublic = true);

  void SetVariableInt_bPublic(VBS2Fusion::NetworkID who,    XStrPar name, int val, bool bPublic)     {SetVariableInt   (who,name,val,bPublic);};
  void SetVariableBool_bPublic(VBS2Fusion::NetworkID who,   XStrPar name, bool val, bool bPublic)    {SetVariableBool  (who,name,val,bPublic);};
  void SetVariableFloat_bPublic(VBS2Fusion::NetworkID who,  XStrPar name, float val, bool bPublic)   {SetVariableFloat (who,name,val,bPublic);};
  void SetVariableStr_bPublic(VBS2Fusion::NetworkID who,    XStrPar name, XStrPar val, bool bPublic) {SetVariableStr   (who,name,val,bPublic);};
  void SetVariableVector_bPublic(VBS2Fusion::NetworkID who, XStrPar name, XVec3Par val, bool bPublic){SetVariableVector(who,name,val,bPublic);};

  void SetVariableNil(VBS2Fusion::NetworkID who,    XStrPar name, bool bPublic = true);
  void SetVariableNil_bPublic(VBS2Fusion::NetworkID who,    XStrPar name, bool bPublic) {SetVariableNil(who, name, bPublic);};

  XVec3 ModelToWorld(VBS2Fusion::NetworkID who, XVec3Par pos);
  XVec3 ModelToWorldASL(VBS2Fusion::NetworkID who, XVec3Par pos);
  XVec3 WorldToModel(VBS2Fusion::NetworkID who, XVec3Par pos);
  XVec3 WorldToModelASL(VBS2Fusion::NetworkID who, XVec3Par pos);

  //declare function e.g. unit_GetAlive, leader_GetAlive,...
  DECLARE_FCE(unit)
  DECLARE_FCE(leader)
  DECLARE_FCE(formationLeader)
  DECLARE_FCE(effectiveCommander)

  // Function for group of given unit
  XStr GroupGetVariableString(XStrPar name);
  bool GroupExistVariable(XStrPar name);
  int  GroupSize();
  void GroupForEachAllowFleeing(float allow);

  void GroupForEachSetVariableInt_bPublic(   XStrPar name, int val, bool bPublic = true);
  void GroupForEachSetVariableBool_bPublic(  XStrPar name, bool val, bool bPublic = true);
  void GroupForEachSetVariableFloat_bPublic( XStrPar name, float val, bool bPublic = true);
  void GroupForEachSetVariableStr_bPublic(   XStrPar name, XStrPar val, bool bPublic = true);
  void GroupForEachSetVariableVector_bPublic(XStrPar name, XVec3Par val, bool bPublic = true);

  void GroupForEachSetVariableInt(   XStrPar name, int val)     {GroupForEachSetVariableInt_bPublic   (name,val);};
  void GroupForEachSetVariableBool(  XStrPar name, bool val)    {GroupForEachSetVariableBool_bPublic  (name,val);};
  void GroupForEachSetVariableFloat( XStrPar name, float val)   {GroupForEachSetVariableFloat_bPublic (name,val);};
  void GroupForEachSetVariableStr(   XStrPar name, XStrPar val) {GroupForEachSetVariableStr_bPublic   (name,val);};
  void GroupForEachSetVariableVector(XStrPar name, XVec3Par val){GroupForEachSetVariableVector_bPublic(name,val);};

  void GroupForEachSetVariableNil_bPublic(XStrPar name, bool bPublic = true);
  void GroupForEachSetVariableNil(XStrPar name) {GroupForEachSetVariableNil_bPublic(name);};

  void GroupForEachSetUnitPosAUTO();
  void GroupForEachSetUnitPosDOWN();
  void GroupForEachSetUnitPosUP();
  
  void GroupForEachExecVM(XStrPar fileName);
  void GroupForEachExecVM_onlyAI(XStrPar fileName);

  void GroupForEachDoMove(XVec3Par pos);
  void GroupForEachDoStop();
  
  XStr ExecuteStringAndReturn(XStrPar str);
 
  // functions handling danger infos
  bool  DangerIsInDanger();
  int   DangerCause();
  void  DangerUpdateBiggestDanger();
  XVec3 DangerPos();
  float DangerUntil();
  void  DangerSetPriorities(int p1, int p2, int p3, int p4, int p5, int p6, int p7);
  void  DangerClose();
};

#define MAKE_AND_CHECK(XX) \
  Unit unit(who);\
  if(!ControllableObjectUtilities::ControllableObjectExists(unit)) return XX;\
  UnitUtilities::updateStaticProperties(unit);UnitUtilities::updateDynamicProperties(unit)\


#endif