/****************************************************************************
* Fusion plug-in for demonstrating RTT functionality.                       *
*                                                                           *
* You need to have Fusion headers and libs in                               *
*  w:\v\SimCentric\Fusion2\VBS2Fusion\includes                              *
*  w:\v\SimCentric\Fusion2\VBS2Fusion\libs                                  *
* You can get them from                                                     * 
*  https//prague.bisimulations.com:475/svn/builds/trunk/SimCentric/Fusion2  *
*                                                                           *
*****************************************************************************/

#pragma warning(disable: 4996)

#pragma warning (disable : 4251)
#pragma warning (disable : 4996)
#pragma warning (disable : 4005)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// VBS2Fusion interface library forced linking
// Needed for plugins that don't use any of VBS2Fusion symbols
// This will insure that VBS2Fusion_2005 library will be linked to the plugin
//  and the plugin will require to have VBS2Fusion_2005.dll
// It is required by Simcentric that each Fusion plugin links with VBS2Fusion_2005.lib or VBS2_Fusion_2008.lib.
#include "VBS2FusionAppContext.h"
VBS2Fusion::VBS2FusionAppContext appContext;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "pluginHeader.h"

/*!
Following function is called by the VBS2 engine in each an every frame

\param deltatT is an floating point argument which returns times
since the last call.
*/

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{	
};

// in - array[handle,time] & text , out - handle
void ExecuteDiagMessage(int &handle, int time, char* text)
{
  if(!text) {handle = -1; return;}

  // fill command line
  char commandBuff[256];
  sprintf(commandBuff,"[%d,%d] diagMessage \"%s\"",handle,time,text);

  char resultBuff[256];

  // send command
  if(!ExecuteCommand(commandBuff, resultBuff, 256))
  {//failed
    OutputDebugString(resultBuff);
    handle = -1; 
    return;
  }

  //parse to handle
  handle = atoi(resultBuff);
  return;
}

// use Format function to preformat if necessary
#define DIAG_MESSAGE(time,text) do \
{	\
  static int handle=-1; \
  ExecuteDiagMessage(handle, time, text); \
} while (false);

/*!
The plugin function is called at the initialization of the plugin.

\param input is  an character which controls the execution of the
plugin
*/

void SetUpRTTTurretCamera(int id, const char *unitName, const char *rttName)
{
  char commandBuff[256];
  char resultBuff[256];

  //create camera
  sprintf(commandBuff, "rttcam%i = \"camera\" camCreate (getPosASL2 %s)", id, unitName);
  ExecuteCommand(commandBuff, resultBuff, 256);
  //set the render target for the camera
  sprintf(commandBuff, "rttcam%i cameraEffect[\"internal\", \"back\", \"%s\", %s]", id, rttName, unitName);   
  ExecuteCommand(commandBuff, resultBuff, 256);
}

void SetUpRTTExternalCamera(int id, const char *vehName, const char *rttName, const char *attachPos)
{
  char commandBuff[256];
  char resultBuff[256];

  //create camera
  sprintf(commandBuff, "rttcam%i = \"camera\" camCreate (position %s)", id,vehName);
  ExecuteCommand(commandBuff, resultBuff, 256);

  //set bank if attached to the cmera to bank with the vehicle
  sprintf(commandBuff, "rttcam%i camSetBankIfAttached true", id);   
  ExecuteCommand(commandBuff, resultBuff, 256);

  sprintf(commandBuff, "rttcam%i camSetFocus [500,0]", id);
  ExecuteCommand(commandBuff, resultBuff, 256);
  sprintf(commandBuff, "rttcam%i attachTo [%s,%s,\"\",false]", id, vehName, attachPos);
  ExecuteCommand(commandBuff, resultBuff, 256);
  sprintf(commandBuff, "rttcam%i camSetAttachedLookDir [0,1,0]", id);
  ExecuteCommand(commandBuff, resultBuff, 256);
  sprintf(commandBuff, "rttcam%i camSetFov 0.7", id);
  ExecuteCommand(commandBuff, resultBuff, 256);
  sprintf(commandBuff, "rttcam%i camCommit 0", id);
  ExecuteCommand(commandBuff, resultBuff, 256);

  //set the render target for the camera
  sprintf(commandBuff, "rttcam%i cameraEffect [\"internal\", \"front\", \"%s\"]", id, rttName);   
  ExecuteCommand(commandBuff, resultBuff, 256);
}

VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
  static const char result[]="[\"RTTCameraDemo done\"]";

  char commandBuff[256];
  char resultBuff[256];

  //creation of UI element with texture to render to
  //the dialog is defined in file description.ext located in directory of a mission
  sprintf(commandBuff, "createDialog \"Dlg\"");
  ExecuteCommand(commandBuff, resultBuff, 256);
  
  SetUpRTTTurretCamera(0, "vehGunner", "dlgrtt0");
  SetUpRTTTurretCamera(1, "vehCommand", "dlgrtt1");
  SetUpRTTTurretCamera(2, "vehDriver", "dlgrtt2");
  SetUpRTTTurretCamera(3, "vehAssist", "dlgrtt3");

  SetUpRTTExternalCamera(4, "veh", "dlgrtt4","[-1.924,0.020,1.13]");
  SetUpRTTExternalCamera(5, "veh", "dlgrtt5","[1.924,0.020,1.13]");
  SetUpRTTExternalCamera(6, "veh", "dlgrtt6","[0,2.00,0.2]");

  return result;
};

/*!
Do not modify this code. Defines the DLL main code fragment
*/

BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
	switch(fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	}

	return TRUE;
};