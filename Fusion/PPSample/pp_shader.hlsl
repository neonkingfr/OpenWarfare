// Very simple shaders for PPSample, see 3DSampleNG for better comments
sampler2D sampler0            : register(s0);

struct VS_INPUT
{
    float4 Position   : POSITION;
	float2 Tex0       : TEXCOORD0;
};

struct VS_OUTPUT
{
    float4 Position   : POSITION;
	float2 Tex0       : TEXCOORD0;
};

struct PS_OUTPUT
{
    float4 Color   : COLOR0;
};

PS_OUTPUT psmain(in VS_OUTPUT In)
{
	PS_OUTPUT Out;
	Out.Color = tex2D(sampler0, In.Tex0);
   //Out.Color = float4(dot(tex2D(sampler0, In.Tex0).rgb, float3(0.299, 0.587, 0.114)).rrr, tex2D(sampler0, In.Tex0).a);
	return Out;
};

VS_OUTPUT vsui(in VS_INPUT In)
{
	VS_OUTPUT Out;
	Out.Position = In.Position;
	Out.Position.w = 1.0f;
	Out.Tex0 = In.Tex0;
	return Out;
}