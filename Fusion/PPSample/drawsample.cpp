//
// Sample postprocess functionality for VBS2 fusion draw interface
// see main.cpp for more comments
//

// To use this plugin, copy pp_shader.hlsl to your VBS2NG folder

#include "drawsample.h"
#include "../../Common/Essential/AppFrameWork.hpp"

#define SHADER_PATH "pp_shader.hlsl"

#define SAFE_RELEASE(p) {if((p)){(p)->Release();(p)=NULL;}}
extern ExecuteCommandType ExecuteCommand;

struct UVVERTEX {
    float x, y, z;
    float u, v;
};

drawsample::drawsample(IDirect3DDevice9 *d3d)
{  
  _ps = NULL;
  _vertexDecl = NULL;
  _bb = NULL;
  _bbSurface = NULL;

  _ready = false;

  LogF("Initializing drawsample...");

  // Compile pixel & vertex shaders
  // The VS & PS are contained in the same HLSL file, using functions "vsmain" and "psmain"
  LPD3DXBUFFER codevs, codeps;
  LPD3DXBUFFER err = NULL;

  if(D3DXCompileShaderFromFile(SHADER_PATH, NULL, NULL, "psmain", "ps_3_0", NULL, &codeps, &err, NULL) != D3D_OK)
  {
    if(err) LogF("Error compiling pixel shader: %s", (char*)err->GetBufferPointer());
    else    LogF("Error compiling pixel shader (file not found?)");
    return;
  }

  d3d->CreatePixelShader((DWORD*)codeps->GetBufferPointer(), &_ps);

  codeps->Release();

  if(D3DXCompileShaderFromFile(SHADER_PATH, NULL, NULL, "vsui", "vs_3_0", NULL, &codevs, &err, NULL) != D3D_OK)
  {
    if(err) LogF("Error compiling vertex shader: %s", (char*)err->GetBufferPointer());
    else    LogF("Error compiling vertex shader (file not found?)");
    return;
  }

  d3d->CreateVertexShader((DWORD*)codevs->GetBufferPointer(), &_vsUI);
  codevs->Release();

  // Create vertex declaration
  // Similar to the FVF in fixed function pipeline, this sets the structure of our vertex data,
  // it must match the vertex data we use (UVVERTEX) and the vertex shader input (VS_INPUT in hlsl)
  D3DVERTEXELEMENT9 decl[] = {{0,
                               0,
                               D3DDECLTYPE_FLOAT3,
                               D3DDECLMETHOD_DEFAULT,
                               D3DDECLUSAGE_POSITION,
                               0},
                              {0,
                               12,
                               D3DDECLTYPE_FLOAT2,
                               D3DDECLMETHOD_DEFAULT,
                               D3DDECLUSAGE_TEXCOORD,
                               0},
                              D3DDECL_END()};
  if(d3d->CreateVertexDeclaration(decl, &_vertexDecl) != D3D_OK)
  {
    LogF("CreateVertexDeclaration failed!");
    return;
  }

  _ready = true;
}

drawsample::~drawsample()
{
  LogF("Deinitializing drawsample...");
  _ready = false;

  SAFE_RELEASE(_vertexDecl);
  SAFE_RELEASE(_ps);
  SAFE_RELEASE(_bb);
  SAFE_RELEASE(_bbSurface);
}

void drawsample::manageObjects(IDirect3DDevice9 *d3d, int width, int height)
{
  if(!_bb)
  {
    // Create surface that will store the backbuffer
    if (d3d->CreateTexture(width, height, 0, D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &_bb, NULL) != D3D_OK)
    {
      LogF("CreateTexture failed!");
      return;
    }
    if (_bb->GetSurfaceLevel(0, &_bbSurface) != D3D_OK)
    {
      LogF("GetSurfaceLevel failed!");
      return;
    }
  }
}

void drawsample::reverseScreen(IDirect3DDevice9 *d3d, IDirect3DSurface9 *renderTarget)
{
  if(!_ready || !_bb) return;

  IDirect3DStateBlock9 *sblock;

  if(d3d->CreateStateBlock(D3DSBT_ALL, &sblock) != D3D_OK) return;
  sblock->Capture();

  setDefaultState(d3d);

  d3d->SetPixelShader(_ps);
  d3d->SetVertexShader(_vsUI);
  d3d->SetVertexDeclaration(_vertexDecl);

  d3d->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
  
  d3d->StretchRect(renderTarget, NULL, _bbSurface, NULL, D3DTEXF_LINEAR);

  d3d->SetTexture(0, _bb);

  float x = -1.0f;
  float y = -1.0f;
  float w = 2.0f;
  float h = 2.0f;

  // Get half-texel offset
  D3DSURFACE_DESC desc;
  _bbSurface->GetDesc(&desc);
  float htx = 0.5f/(float)desc.Width;
  float hty = 0.5f/(float)desc.Height;

  UVVERTEX vdata[] = {
  // X     Y     Z         U         V
    {x,    y, 0.1f, 1.0f+htx, 1.0f+hty},
    {x+w,  y, 0.1f, 0.0f+htx, 1.0f+hty},
    {x,  y+h, 0.1f, 1.0f+htx, 0.0f+hty},
    {x+w,y+h, 0.1f, 0.0f+htx, 0.0f+hty},
  };
  d3d->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, vdata, sizeof(UVVERTEX));
  sblock->Apply();
  sblock->Release();
}

void drawsample::setDefaultState(IDirect3DDevice9 *d3dd)
{
  // Set D3D device properties to their default state
  // This is to ensure things are in a predictable state when coming from engine rendering code
  const float zerof = 0.0f;
  const float onef = 1.0f;
  #define ZEROf	*((DWORD*) (&zerof))
  #define ONEf	*((DWORD*) (&onef))

  d3dd->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
  d3dd->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
  d3dd->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_GOURAUD);
  d3dd->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
  d3dd->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_LASTPIXEL, TRUE);
  d3dd->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
  d3dd->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ZERO);
  d3dd->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
  d3dd->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
  d3dd->SetRenderState(D3DRS_ALPHAREF, 0);
  d3dd->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_ALWAYS);
  d3dd->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_FOGENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_SPECULARENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_FOGCOLOR, 0);
  d3dd->SetRenderState(D3DRS_FOGTABLEMODE, D3DFOG_NONE);
  d3dd->SetRenderState(D3DRS_FOGSTART, ZEROf);
  d3dd->SetRenderState(D3DRS_FOGEND, ONEf);
  d3dd->SetRenderState(D3DRS_FOGDENSITY, ONEf);
  d3dd->SetRenderState(D3DRS_RANGEFOGENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_STENCILENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_ALWAYS);
  d3dd->SetRenderState(D3DRS_STENCILREF, 0);
  d3dd->SetRenderState(D3DRS_STENCILMASK, 0xffffffff);
  d3dd->SetRenderState(D3DRS_STENCILWRITEMASK, 0xffffffff);
  d3dd->SetRenderState(D3DRS_TEXTUREFACTOR, 0xffffffff);
  d3dd->SetRenderState(D3DRS_WRAP0, 0);
  d3dd->SetRenderState(D3DRS_WRAP1, 0);
  d3dd->SetRenderState(D3DRS_WRAP2, 0);
  d3dd->SetRenderState(D3DRS_WRAP3, 0);
  d3dd->SetRenderState(D3DRS_WRAP4, 0);
  d3dd->SetRenderState(D3DRS_WRAP5, 0);
  d3dd->SetRenderState(D3DRS_WRAP6, 0);
  d3dd->SetRenderState(D3DRS_WRAP7, 0);
  d3dd->SetRenderState(D3DRS_CLIPPING, TRUE);
  d3dd->SetRenderState(D3DRS_AMBIENT, 0);
  d3dd->SetRenderState(D3DRS_LIGHTING, TRUE);
  d3dd->SetRenderState(D3DRS_FOGVERTEXMODE, D3DFOG_NONE);
  d3dd->SetRenderState(D3DRS_COLORVERTEX, TRUE);
  d3dd->SetRenderState(D3DRS_LOCALVIEWER, TRUE);
  d3dd->SetRenderState(D3DRS_NORMALIZENORMALS, FALSE);
  d3dd->SetRenderState(D3DRS_DIFFUSEMATERIALSOURCE, D3DMCS_COLOR1);
  d3dd->SetRenderState(D3DRS_SPECULARMATERIALSOURCE, D3DMCS_COLOR2);
  d3dd->SetRenderState(D3DRS_AMBIENTMATERIALSOURCE, D3DMCS_MATERIAL);
  d3dd->SetRenderState(D3DRS_EMISSIVEMATERIALSOURCE, D3DMCS_MATERIAL);
  d3dd->SetRenderState(D3DRS_CLIPPLANEENABLE, 0);
  d3dd->SetRenderState(D3DRS_POINTSIZE_MIN, ONEf);
  d3dd->SetRenderState(D3DRS_POINTSPRITEENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_MULTISAMPLEANTIALIAS, TRUE);
  d3dd->SetRenderState(D3DRS_MULTISAMPLEMASK, 0xffffffff);
  d3dd->SetRenderState(D3DRS_PATCHEDGESTYLE, D3DPATCHEDGE_DISCRETE);
  d3dd->SetRenderState(D3DRS_POINTSIZE_MAX, ONEf);
  d3dd->SetRenderState(D3DRS_COLORWRITEENABLE, 0x0000000f);
  d3dd->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
  d3dd->SetRenderState(D3DRS_POSITIONDEGREE, D3DDEGREE_CUBIC);
  d3dd->SetRenderState(D3DRS_NORMALDEGREE, D3DDEGREE_LINEAR);
  d3dd->SetRenderState(D3DRS_SCISSORTESTENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_SLOPESCALEDEPTHBIAS, 0);
  d3dd->SetRenderState(D3DRS_MINTESSELLATIONLEVEL, ONEf);
  d3dd->SetRenderState(D3DRS_MAXTESSELLATIONLEVEL, ONEf);
  d3dd->SetRenderState(D3DRS_ADAPTIVETESS_X, ZEROf);
  d3dd->SetRenderState(D3DRS_ADAPTIVETESS_Y, ZEROf);
  d3dd->SetRenderState(D3DRS_ADAPTIVETESS_Z, ONEf);
  d3dd->SetRenderState(D3DRS_ADAPTIVETESS_W, ZEROf);
  d3dd->SetRenderState(D3DRS_ENABLEADAPTIVETESSELLATION, FALSE);
  d3dd->SetRenderState(D3DRS_TWOSIDEDSTENCILMODE, FALSE);
  d3dd->SetRenderState(D3DRS_CCW_STENCILFAIL, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_CCW_STENCILZFAIL, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_CCW_STENCILPASS, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_CCW_STENCILFUNC, D3DCMP_ALWAYS);
  d3dd->SetRenderState(D3DRS_COLORWRITEENABLE1, 0x0000000f);
  d3dd->SetRenderState(D3DRS_COLORWRITEENABLE2, 0x0000000f);
  d3dd->SetRenderState(D3DRS_COLORWRITEENABLE3, 0x0000000f);
  d3dd->SetRenderState(D3DRS_BLENDFACTOR, 0xffffffff);
  d3dd->SetRenderState(D3DRS_SRGBWRITEENABLE, 0);
  d3dd->SetRenderState(D3DRS_DEPTHBIAS, 0);
  d3dd->SetRenderState(D3DRS_WRAP8, 0);
  d3dd->SetRenderState(D3DRS_WRAP9, 0);
  d3dd->SetRenderState(D3DRS_WRAP10, 0);
  d3dd->SetRenderState(D3DRS_WRAP11, 0);
  d3dd->SetRenderState(D3DRS_WRAP12, 0);
  d3dd->SetRenderState(D3DRS_WRAP13, 0);
  d3dd->SetRenderState(D3DRS_WRAP14, 0);
  d3dd->SetRenderState(D3DRS_WRAP15, 0);
  d3dd->SetRenderState(D3DRS_SEPARATEALPHABLENDENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_SRCBLENDALPHA, D3DBLEND_ONE);
  d3dd->SetRenderState(D3DRS_DESTBLENDALPHA, D3DBLEND_ZERO);
  d3dd->SetRenderState(D3DRS_BLENDOPALPHA, D3DBLENDOP_ADD);
  d3dd->SetRenderState(D3DRS_DITHERENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_VERTEXBLEND, D3DVBF_DISABLE);
  d3dd->SetRenderState(D3DRS_POINTSIZE, ONEf);
  d3dd->SetRenderState(D3DRS_POINTSCALEENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_POINTSCALE_A, ONEf);
  d3dd->SetRenderState(D3DRS_POINTSCALE_B, ZEROf);
  d3dd->SetRenderState(D3DRS_POINTSCALE_C, ZEROf);
  d3dd->SetRenderState(D3DRS_INDEXEDVERTEXBLENDENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_TWEENFACTOR, ZEROf);
  d3dd->SetRenderState(D3DRS_ANTIALIASEDLINEENABLE, FALSE);

  for(int i=0;i<8;i++) {
	  d3dd->SetSamplerState(i, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	  d3dd->SetSamplerState(i, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	  d3dd->SetSamplerState(i, D3DSAMP_MIPFILTER, D3DTEXF_NONE);
	  d3dd->SetSamplerState(i, D3DSAMP_MIPMAPLODBIAS, 0);
	  d3dd->SetSamplerState(i, D3DSAMP_MAXMIPLEVEL, 0);			
	  d3dd->SetSamplerState(i, D3DSAMP_MAXANISOTROPY, 1);
	  d3dd->SetSamplerState(i, D3DSAMP_SRGBTEXTURE, 0);			
	  d3dd->SetSamplerState(i, D3DSAMP_ELEMENTINDEX, 0);
	  d3dd->SetSamplerState(i, D3DSAMP_DMAPOFFSET, 0);			
	  d3dd->SetSamplerState(i, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
	  d3dd->SetSamplerState(i, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
	  d3dd->SetSamplerState(i, D3DSAMP_ADDRESSW, D3DTADDRESS_WRAP);

    d3dd->SetTexture(i, NULL);
  }

  #undef ZEROf
  #undef ONEf
}
