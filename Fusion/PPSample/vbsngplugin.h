#ifndef __VBSPLUGIN_H__
#define __VBSPLUGIN_H__

#include <windows.h>
#include <d3d9.h>

#define VBSPLUGIN_EXPORT __declspec(dllexport)

// Command function declaration
typedef int (WINAPI * ExecuteCommandType)(const char *command, char *result, int resultLength);

//void LogF(const char* format, ...);

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT);
VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input);
VBSPLUGIN_EXPORT void WINAPI OnFilledBackbuffer();
VBSPLUGIN_EXPORT void WINAPI OnD3DeviceInit(IDirect3DDevice9 *device);
VBSPLUGIN_EXPORT void WINAPI OnD3DeviceInvalidate();
VBSPLUGIN_EXPORT void WINAPI OnD3DeviceRestore();
VBSPLUGIN_EXPORT void WINAPI OnUnload();
VBSPLUGIN_EXPORT void WINAPI RegisterHWND(void *hwnd, void *hins);
VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc);

#endif