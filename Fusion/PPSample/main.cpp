#include <windows.h>
#include "vbsngplugin.h"

#include <d3d9.h>
#include <d3dx9.h>

#include "../../Common/Essential/AppFrameWork.hpp"

#include "drawsample.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// VBS2Fusion interface library forced linking
// Needed for plugins that don't use any of VBS2Fusion symbols
// This will insure that VBS2Fusion_2005 library will be linked to the plugin
//  and the plugin will require to have VBS2Fusion_2005.dll
// It is required by Simcentric that each Fusion plugin links with VBS2Fusion_2005.lib or VBS2_Fusion_2008.lib.
#include "VBS2FusionAppContext.h"
VBS2Fusion::VBS2FusionAppContext appContext;
#pragma comment(lib, "../_depends/Fusion/lib/VBS2Fusion_2005.lib")
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

IDirect3DDevice9 *dev = NULL;
IDirect3DSurface9 *renderTarget = NULL;
int renderWidth, renderHeight;
drawsample *sample = NULL;

void LogF(const char* format, ...)
{
	va_list     argptr;
	char        str[1024];
	va_start (argptr,format);
	vsnprintf (str,1024,format,argptr);
	va_end   (argptr);
  OutputDebugString(str);
}

ExecuteCommandType ExecuteCommand = NULL;

VBSPLUGIN_EXPORT void WINAPI OnFilledBackbuffer()
{
  if (sample)
  {
    dev->GetRenderTarget(0, &renderTarget);
    sample->manageObjects(dev, renderWidth, renderHeight);
    sample->reverseScreen(dev, renderTarget);
    renderTarget->Release();
  }
}

VBSPLUGIN_EXPORT void WINAPI OnD3DeviceInit(IDirect3DDevice9 *device)
{  
  // store a pointer to the d3dDevice
  dev = device;
  // Create the sample
  if(!sample) sample = new drawsample(device);
}

VBSPLUGIN_EXPORT void WINAPI OnD3DeviceInvalidate()
{
  // Delete the sample object, this will release everything it has created
  delete sample;
  sample = NULL;
}

VBSPLUGIN_EXPORT void WINAPI OnD3DeviceRestore()
{
  // Re-create the sample
  if(!sample) sample = new drawsample(dev);
}


VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
  return "[]";
}

VBSPLUGIN_EXPORT void WINAPI OnUnload() 
{
  // Whole plugin is unloaded, release the sample clear pointer to dxdevice
  dev = NULL;
  delete sample;
  sample = NULL;
}

VBSPLUGIN_EXPORT void WINAPI RegisterHWND(void *hwnd, void *hins) 
{
  RECT rect;
  HWND vbsWinHandle = (HWND)hwnd;
  ::GetClientRect(vbsWinHandle, &rect);
  renderWidth = rect.right - rect.left;
  renderHeight = rect.bottom - rect.top;
}

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT) {}

VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc)
{
  ExecuteCommand = (ExecuteCommandType)executeCommandFnc;
}

BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
   switch(fdwReason)
   {
      case DLL_PROCESS_ATTACH:
        break;
      case DLL_PROCESS_DETACH:
        break;
   }
   return TRUE;
}



