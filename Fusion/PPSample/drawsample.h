#ifndef __DRAWSAMPLE_H__
#define __DRAWSAMPLE_H__

#include <windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <vector>

#include "vbsngplugin.h"

// A very simple object definition, with just position, color and spin
struct simpleObject
{
    float _x,_y,_z;
    float _r, _g, _b;
    float _spin;
    simpleObject(float x, float y, float z, float r, float g, float b, float spin) :
                 _x(x), _y(y), _z(z), _r(r), _g(g), _b(b), _spin(spin) {};
};

class drawsample
{
public:
  drawsample(IDirect3DDevice9 *d3d);
  ~drawsample();

  void manageObjects(IDirect3DDevice9 *d3d, int width, int height);
  void reverseScreen(IDirect3DDevice9 *d3d, IDirect3DSurface9 *renderTarget);

private:
  void setDefaultState(IDirect3DDevice9 *d3d);

  bool _ready;
  std::vector<simpleObject> _objects;

  IDirect3DPixelShader9 *_ps;
  IDirect3DVertexShader9 *_vsUI; // Simple VS for 2D drawing
  IDirect3DVertexDeclaration9 *_vertexDecl;
  IDirect3DTexture9 *_bb;
  IDirect3DSurface9 *_bbSurface;
};

#endif