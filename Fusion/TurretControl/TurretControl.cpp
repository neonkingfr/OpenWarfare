/****************************************************************************
* Plugin for testing turrets controllers                                    *
****************************************************************************/

#pragma warning(disable: 4996)
#pragma warning(disable: 4251)

#include <stdio.h>
#include <VBS2Fusion.h>
#include "TurretControl.h"

#define _BISIM_DEV_SET_TURRET_STABILIZATION 1
#define _BISIM_DEV_SET_OPTICSDIRECTION 1
#define _BISIM_DEV_FORCE_TURRET_SPEED 1
#define WEAPON_HANDLING_OLD 1
#define WEAPON_HANDLING 1

#include <util/MissionUtilities.h>
#include <util/VehicleUtilities.h>
#include <util/UnitUtilities.h>
#include <data/Turret.h>

double azimuth = 0;
double elevation = 0;
#if TURRET_CONTROL_ONLY_SPEED
double azimuthSpeed = 0;
double elevationSpeed = 0;
#else
double azimuthSpeed = 40;
double elevationSpeed = 40;
#endif
bool azimuthStabil = true;
bool elevationStabil = true;

#define _DIAG 1
#define LOG_HEADER "[TurretControl] "
void LogDiagMessage(const char *format, ...)
{
#if _DIAG
  va_list arglist;
  va_start(arglist, format);
  char buffer[512] = { 0 };
  vsprintf_s(buffer,512,format, arglist);
  va_end(arglist);
  OutputDebugString(buffer);
#endif
}

VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
  static const char *result = "";

  VBS2Fusion::Unit playerUnit = VBS2Fusion::MissionUtilities::getPlayer();
  VBS2Fusion::Vehicle tank = VBS2Fusion::UnitUtilities::getMountedVehicle(playerUnit);
  VBS2Fusion::VehicleUtilities::updateVehicle(tank);
  
  VBS2Fusion::Turret::TurretPath tupath;
  tupath.resize(1);
  tupath.at(0) = 0;
  VBS2Fusion::Turret turret;
  turret.setTurretPath(tupath);

  // "r" means 90 degrees clockwise
  if (stricmp(input, "r") == 0)
  {
    azimuth += 90;
    if (azimuth >= 360)
      azimuth = azimuth - 360;
  }
  // "l" means 90 degrees counter clockwise
  else if (stricmp(input, "l") == 0)
  {
    azimuth -= 90;
    if (azimuth < 0)
      azimuth = 360 + azimuth;
  }
  // "u" means 5 degrees up (turret might not allow it though)
  else if (stricmp(input, "u") == 0)
  {
    elevation += 5;
    if (elevation > 90)
      elevation = 90;
  }
  // "d" means 5 degrees down (turret might not allow it though)
  else if (stricmp(input, "d") == 0)
  {
    elevation -= 5;
    if (elevation < -90)
      elevation = -90;
  }
  // "fa" means azimuth change speed faster by 5 degrees/second
  else if (stricmp(input, "fa") == 0)
    azimuthSpeed += 5;
  // "sa" means azimuth change speed slower by 5 degrees/second
  else if (stricmp(input, "sa") == 0)
    azimuthSpeed -= 5;
  // "fe" means elevation change speed faster by 5 degrees/second
  else if (stricmp(input, "fe") == 0)
    elevationSpeed += 5;
  // "se" means elevation change speed slower by 5 degrees/second
  else if (stricmp(input, "se") == 0)
    elevationSpeed -= 5;
  else if (stricmp(input, "aon") == 0)
    azimuthStabil = true;
  else if (stricmp(input, "aoff") == 0)
    azimuthStabil = false;
  else if (stricmp(input, "eon") == 0)
    elevationStabil = true;
  else if (stricmp(input, "eoff") == 0)
    elevationStabil = false;

  VBS2Fusion::VehicleUtilities::applyOpticsDirection(tank, turret, azimuth, elevation);
  VBS2Fusion::VehicleUtilities::applyTurretSpeed(tank, turret, azimuthSpeed, elevationSpeed);
  VBS2Fusion::VehicleUtilities::applyTurretStabilizaton(tank, turret, azimuthStabil, elevationStabil);

  return result;
}

// DllMain
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
  switch(fdwReason)
  {
  case DLL_PROCESS_ATTACH:
    break;
  case DLL_PROCESS_DETACH:
    break;
  case DLL_THREAD_ATTACH:
    break;
  case DLL_THREAD_DETACH:
    break;
  }
  return TRUE;
}

