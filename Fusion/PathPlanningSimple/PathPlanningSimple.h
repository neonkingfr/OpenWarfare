#include <windows.h>

#define VBSPLUGIN_EXPORT __declspec(dllexport)

typedef int (WINAPI * ExecuteCommandType)(const char *command, char *result, int resultLength);

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT);
VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input);
VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc);




