///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// VBS2Fusion interface library forced linking
// Needed for plugins that don't use any of VBS2Fusion symbols
// This will insure that VBS2Fusion_2005 library will be linked to the plugin
//  and the plugin will require to have VBS2Fusion_2005.dll
// It is required by Simcentric that each Fusion plugin links with VBS2Fusion_2005.lib or VBS2_Fusion_2008.lib.
#include "VBS2FusionAppContext.h"
VBS2Fusion::VBS2FusionAppContext appContext;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <xait/map/xaitMap.h>
#include <xait/common/callback/FileIOCallbackWin32.h>

#include "PathPlanningSimple.h"

const char *NavMeshFileName = "navmesh.xmap";

XAIT::Map::NavMeshContainer* navMesh = NULL;
XAIT::Map::Path::PathResult* path = NULL;
XAIT::uint32 waypointIndex=1;

std::string unitNameStr;

char retStr[512];

// Command function definition
ExecuteCommandType ExecuteCommand = NULL;
VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc)
{
  ExecuteCommand = (ExecuteCommandType)executeCommandFnc;
}



void GetPosASL2(const char* unitName, float& x, float& y, float& z)
{
  if(!unitName) return;

  char command[256];
  sprintf(command, "GetPosASL2 (%s)", unitName);

  // send command
  char resultBuff[256];
  if(!ExecuteCommand(command, resultBuff, 256))
  {
    char debugText[256];
    sprintf(debugText, "GetPosASL2 failed: %s\n", resultBuff);
    OutputDebugString(debugText);
    return;
  }
  
  // parse results
  int parse = sscanf(resultBuff, "[%f,%f,%f]", &x, &y, &z);
  if (parse != 3)
  {
    OutputDebugString("GetPosASL2 failed: can't parse command result\n");
    return;
  }
}

// This function will be executed every simulation step (every frame) and took a part in the simulation procedure.
// We can be sure in this function the ExecuteCommand registering was already done.
// deltaT is time in seconds since the last simulation step
VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
  if(!path)
		return;
	//else-> test if the character has reached its waypoint

	float x,y,z;
	GetPosASL2(unitNameStr.c_str(), x,z,y);

  const XAIT::float32 sqDistance  = path->getSegment(waypointIndex).mStartPos.getDistance2(XAIT::Common::Math::Vec3f(x,y,z));

	char commandBuff[512];
	char resultBuff[512];

	if (sqDistance < 1.0)
	{
		if(++waypointIndex <= path->getNumSegments())
		{
      XAIT::Common::Math::Vec3f point = path->getSegment(waypointIndex).mStartPos;
			sprintf(commandBuff, "a = (%s) doMove [%f,%f,%f]", unitNameStr.c_str(), point.mX, point.mZ, point.mY);
			ExecuteCommand(commandBuff, resultBuff, 256);
		}
		else //reached last waypoint
		{
			delete path;
			path = NULL;
			waypointIndex = 1;
		}
	}
}

// This function will be executed every time the script in the engine calls the script function "pluginFunction"
// We can be sure in this function the interface functions registering was already done.
// Note that the plugin takes responsibility for allocating and deleting the returned string

//Calling Syntax: 
// res = pluginFunction["PathPlanningSimple","[posx,posy,posz],name"];
//E.g.: result = fusionFunction["PathPlanningSimple",str(getPosASL dest) + ",dude1"];
VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{

  static const char resultOK[]="";
  static const char resultNoNavMesh[]="[\"PathPlanningPlugin failed: No navmesh loaded\"]";
  static const char resultNoPath[]="[\"PathPlanningPlugin failed: No path to target\"]";
  static const char resultSearchFailed[]="[\"PathPlanningPlugin failed: Path search failed\"]";
  static const char parseError[]="[\"PathPlanningPlugin failed: Input values parse error\"]";
  
  if (path)
    delete path;
  path = NULL;
  waypointIndex = 1;

  if (!navMesh)
    return resultNoNavMesh;

  float fromX = 0.0f;
  float fromY = 0.0f;
  float fromZ = 0.0f;
  float toX = 0.0f;
  float toY = 0.0f;
  float toZ = 0.0f;
  unsigned int id;
  char unitName[128];

  int parsed = sscanf(input, "[%f,%f,%f],%s", &toX, &toZ, &toY, unitName);
  if (parsed != 4)
    return parseError;
    
  GetPosASL2(unitName,fromX,fromZ,fromY);

  char debugText[256];
  sprintf(debugText, "Pathplanning request received - unit %s, from [%.1f, %.1f, %.1f], to [%.1f, %.1f, %.1f]\n", unitName, fromX, fromY, fromZ, toX, toY, toZ);
  OutputDebugString(debugText);

  unitNameStr = unitName;

  // PathPlanning code start
  // Create a path search.
  // The path search type specifies how the navmesh should be split up into a graph
  // so that a path can be searched. There are three different types of path searches
  // MidpointPathSearch		Searches across the midpoints of the navigation mesh. This search
  //							is fast but deliverers inaccurate path results if the navigation mesh
  //							has some very big navigation nodes.
  // PolyEdgePathSearch		Searches across the edges of the navigation mesh. This search is slower
  //							than the midpoint search but delivers better path results.
  // SubdivisionPathSearch	Searches also across the edges of the navigation mesh, but splits edges
  //							if they are too long. This delivers even better results than PolyEdgePathSearch
  //							but is also two times slower.
  XAIT::Map::Path::PathSearch* search = new XAIT::Map::Path::MidpointPathSearch(navMesh);

  // In order to run a path search on the search, we need to create a path search instance. A path search
  // instance holds the runtime information of the path search. If you want todo several path searches synchronously,
  // you have to create an instance for every synchronous running path search. Instances can be reused after the
  // search request has been finished. In a normal, non-threaded environment, you need only one path search instance.
  XAIT::Map::Path::PathSearchInstance* instance= search->createInstance();

  // A new search is started by filling a search request.
  // Each search request needs a start-/endpoint pair and a search config
  // which specifies the path costs. The search config has several possibilities
  // to specify cost factors for different characters and different surface types.
  // See the API documentation for more information on that.
  XAIT::Map::Path::SearchConfig searchConfig;

  XAIT::Map::Path::SearchRequest request(XAIT::Common::Math::Vec3f(fromX,fromY,fromZ), XAIT::Common::Math::Vec3f(toX,toY,toZ),& searchConfig);

  XAIT::Map::Path::SearchRequest::PathSmoothing smoothingType = XAIT::Map::Path::SearchRequest::PS_SMOOTHING;
  request.setPathSmoothing(smoothingType);


  // now we initialize the path search instance with a new search request. This will check
  // if a path for the specified start and end positions can be found. If not the init returns
  // false. Further information can be queried with getErrorCode
  instance->init(&request);

  // The state and error code of a path request can be queried on the search request object.
  if (request.getErrorCode() != XAIT::Map::Path::SearchRequest::ERROR_NO_ERROR)
  {
    sprintf(retStr, "[\"no Path to Target:[%f,%f,%f] -> [%f,%f,%f]\"]", fromX,fromY,fromZ,toX,toY,toZ);
    return retStr;
  }

  // Now that the path search is initialized, we can start the path search.
  // A call to PathSearchInstance::run processes a part of the path search.
  // The interface provides a time slice mechanism, where you can specify the
  // a amount of internal steps of the path search. The method run returns true
  // as long the path search is in state Running. If it succeeds or fails, it returns
  // false. If the steps parameter is omitted, the call to run is blocking until the path
  // search is finished.
  while (instance->run(1000))
  {}

  // now we can query the state of the path search, if it has failed or succeeded.
  if (request.getSearchState() != XAIT::Map::Path::SearchRequest::SEARCH_SUCCEEDED || request.getPath()->getNumSegments() <= 0)
    return resultSearchFailed;

  //0.5f is the characters radius
  XAIT::Map::Path::PathOptimizer optimizer;
  path = optimizer.computeOptimizedPath(request.getPath());

  delete instance;
  delete search;

  char commandBuff[256];
  char resultBuff[256];
  XAIT::Common::Math::Vec3f point = path->getSegment(waypointIndex).mStartPos;
  sprintf(commandBuff, "a = (%s) doMove [%f,%f,%f]", unitName, point.mX, point.mZ, point.mY);
  ExecuteCommand(commandBuff, resultBuff, 256);

  return resultOK;
}


// DllMain
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
  //   GHDll = hDll;
  switch(fdwReason)
  {
  case DLL_PROCESS_ATTACH:
    {
      OutputDebugString("Called DllMain with DLL_PROCESS_ATTACH\n");
      // initialize the common and navmesh library with
      // the default callbacks delivered by the library itself
      XAIT::Common::Callback::MemoryCallback::Ptr cbMemory(new XAIT::Common::Callback::MemoryCallback());
      XAIT::Common::Callback::FileIOCallback::Ptr cbFileIO(new XAIT::Common::Callback::FileIOCallbackWin32());

      XAIT::Common::Interface::initLibrary(cbMemory);
      XAIT::Map::Interface::initLibrary(cbFileIO);

      navMesh = XAIT::Map::NavMeshSerializer::readNavMesh( NavMeshFileName );
      if (!navMesh)
      {
        char buff[512];
        sprintf(buff, "[ERROR] PathPlanningSimple: Can't load xaitMap navmesh file '%s'.\n", NavMeshFileName);
        OutputDebugString(buff);
      }
    }
    break;
  case DLL_PROCESS_DETACH:
    OutputDebugString("Called DllMain with DLL_PROCESS_DETACH\n");
    if (navMesh)
      delete navMesh;
    XAIT::Map::Interface::closeLibrary();
    XAIT::Common::Interface::closeLibrary();
    break;
  case DLL_THREAD_ATTACH:
    OutputDebugString("Called DllMain with DLL_THREAD_ATTACH\n");
    break;
  case DLL_THREAD_DETACH:
    OutputDebugString("Called DllMain with DLL_THREAD_DETACH\n");
    break;
  }
  return TRUE;
}