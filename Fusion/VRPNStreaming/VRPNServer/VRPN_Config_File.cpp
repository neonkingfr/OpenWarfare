/*
*	VRPN_Config_File.cpp
*	
*	Implementation for Cortex VRPN server configuration
*
*	Copyright Motion Analysis Corporation 2011
*/

#include "VRPN_Config_File.h"


char * StartTokenValues[START_TOKEN_COUNT] =
{
	"vrpn_Tracker_MotionAnalysis",
	"MAC_Tracker_Body",
	"MAC_Tracker_Segment",
	"MAC_Tracker_Marker",
	"MAC_Tracker_Debug",
	"MAC_Tracker_Delimiter",
	"MAC_Tracker_TrackerPerBody",
	"MAC_Tracker_SendInvalidData"
};

SegmentConfig::SegmentConfig(char * pBodyName /* = "Unknown" */,
							 char * pName /* = "Unknown" */,
							 int iSensorIndex)
{
	if(pBodyName)
	{
		sprintf_s(m_szBodyName, pBodyName);
	}
	if(pName)
	{
		sprintf_s(m_szName, pName);
	}
	m_iSensorIndex = iSensorIndex;
}

MarkerConfig::MarkerConfig(char * pBodyName /* = "Unknown" */,
						   char * pName /* = "Unknown" */,
						   int iSensorIndex)
{
	if(pBodyName)
	{
		sprintf_s(m_szBodyName, pBodyName);
	}
	if(pName)
	{
		sprintf_s(m_szName, pName);
	}
	m_iSensorIndex = iSensorIndex;
}

BodyConfig::BodyConfig(char * pBodyName /* = NULL */,
					   char * pAliasName /* = NULL */)
{
	if(pBodyName)
	{
		sprintf_s(m_szBodyName, pBodyName);
	}
	else
	{
		sprintf_s(m_szBodyName, "Unknown");
	}

	if(pAliasName)
	{
		sprintf_s(m_szAliasName, pAliasName);
	}
	else
	{
		sprintf_s(m_szAliasName, m_szBodyName);
	}

}

SegmentConfig * BodyConfig::FindSegmentConfig(char * pName)
{
	std::list<SegmentConfig*>::iterator iter;
	for(iter = m_SegmentConfigList.begin(); iter != m_SegmentConfigList.end(); iter++)
	{
		SegmentConfig * pSegmentConfig = (SegmentConfig*)*iter;
		if(strcmp(pName, pSegmentConfig->m_szName) == 0)
		{
			return pSegmentConfig;
		}
	}
	return NULL;
}

MarkerConfig * BodyConfig::FindMarkerConfig(char * pName)
{
	std::list<MarkerConfig*>::iterator iter;
	for(iter = m_MarkerConfigList.begin(); iter != m_MarkerConfigList.end(); iter++)
	{
		MarkerConfig * pMarkerConfig = (MarkerConfig*)*iter;
		if(strcmp(pName, pMarkerConfig->m_szName) == 0)
		{
			return pMarkerConfig;
		}
	}
	return NULL;
}

BodyConfig::~BodyConfig()
{
	std::list<SegmentConfig*>::iterator segIter;
	std::list<MarkerConfig*>::iterator markerIter;

	for(segIter = m_SegmentConfigList.begin(); segIter != m_SegmentConfigList.end(); segIter++)
	{
		SegmentConfig * pSegConfig = (SegmentConfig*)*segIter;

		if(pSegConfig)
		{
			delete pSegConfig;
		}
	}
	m_SegmentConfigList.clear();
	
	for(markerIter = m_MarkerConfigList.begin(); markerIter != m_MarkerConfigList.end(); markerIter++)
	{
		MarkerConfig * pMarkerConfig = (MarkerConfig*)*markerIter;

		if(pMarkerConfig)
		{
			delete pMarkerConfig;
		}
	}
	m_MarkerConfigList.clear();
}

MAC_VRPN_Config::MAC_VRPN_Config()
{
	sprintf_s(m_szCurrentBodyName, "Unknown");
	sprintf_s(m_szCurrentAlias, "None");
	m_bDebug = false;
	m_bSendInvalidData = false;
	m_optionTracker = Tracker_PerBody;
	m_cDelimiter = ':';
	m_sClientIP = "127.0.0.1";
	m_sServerIP = "127.0.0.1";
}

MAC_VRPN_Config::~MAC_VRPN_Config()
{
	//ClearMarkerList();
	//ClearSegmentList();
}

bool MAC_VRPN_Config::ReadConfig()
{
	FILE * hConfigFile;

	char szWork[MAX_PATH];
	char szFile[MAX_PATH] = "";
	_getcwd(szWork, MAX_PATH);
	strcat_s(szWork, "\\VRPN.cfg");

	for(int i = 0; i < (int)strlen(szWork); i++)
	{
		if(szWork[i] == 92) // backslash
		{
			strcat_s(szFile, "/");
			//i++;
			continue;
		}
		int iLen = strlen(szFile);
		szFile[iLen++] = szWork[i];
		szFile[iLen] = 0;
	}

	errno_t err = fopen_s(&hConfigFile, szFile, "r");
	if(err)
	{
		cout << "Error opening configuration file: ";
		cout << szFile;

		switch(err)
		{
		case EACCES:
			cout << "File access error opening file\n";
			break;
		case ENOENT:
			cout << "Error opening file - doesn't exist:\n";
			cout << szFile;
			cout << endl;
			break;
		default:
			char szErrorMessage[50];
			sprintf_s(szErrorMessage, "General error opening file: %d\n", err);
			cout << szErrorMessage;
			break;
		}
	}
	else
	{
		if(hConfigFile)
		{
			cout << "Config file ";
			cout << szWork;
			cout << " was opened\n";

			bool bKeepReading = true;

			//ClearSegmentList();
			//ClearMarkerList();

			while(bKeepReading)
			{
				char * pLine = ReadLine(hConfigFile);
				if(pLine)
				{
					if( (pLine[0] == '#') ||
						(pLine[0] == 0x0D) ||
						(pLine[0] == 0x0A) ||
						(pLine[0] == 0) )
					{
						//cout << "Comment or blank: |";
						//cout << pLine;
						//cout << "|";
						//cout << endl;
						continue;
					}
					else
					{
						for(int i = 0; i < START_TOKEN_COUNT; i++)
						{
							if(_strnicmp(pLine, StartTokenValues[i], strlen(StartTokenValues[i])) == 0)
							{
								//cout << "MAC Line discovered:\n";
								ParseLine(pLine, (startTokenItem)i);
								break;
							}
							else
							{
								//cout << "Not a mac line:\n";
							}
						}
						//cout << pLine;
						//cout << endl;
					}
				}
				else
				{
					bKeepReading = false;
				}
			}
			fclose(hConfigFile);
			cout << endl;
		}
	}
	return true;
}

std::string & MAC_VRPN_Config::ClientIP()
{
	return m_sClientIP;
}

std::string & MAC_VRPN_Config::ServerIP()
{
	return m_sServerIP;
}

char * MAC_VRPN_Config::ReadLine(FILE * hFile)
{
	char * pChar = m_szLine;
	while(fread(pChar, 1, 1, hFile))
	{
		if((*pChar == 0x0D) || (*pChar == 0x0A))
		{
			*pChar = 0;
			return m_szLine;
		}
		pChar++;
	}
	return NULL;
}

bool MAC_VRPN_Config::ParseLine(char * pLine, startTokenItem iStartTokenIndex)
{
	int iCount;
	char szToken[MAX_PATH];
	char szName[MAX_PATH];
	char szAlias[MAX_PATH];
	char szValue[MAX_PATH];
	BodyConfig * pBodyConfig = NULL;

	if(!pLine) return false;

	switch (iStartTokenIndex)
	{
	case Token_Tracker:
		{
			char szToken[MAX_PATH];
			char szClientIP[MAX_PATH];
			char szServerIP[MAX_PATH];

			iCount = sscanf_s(pLine, 
				"%s %s %s", 
				szToken, _countof(szToken), 
				szServerIP, _countof(szServerIP), 
				szClientIP, _countof(szClientIP)); 

			if(strcmp(szToken, StartTokenValues[Token_Tracker]) != 0) return false;
			m_sClientIP = szClientIP;
			m_sServerIP = szServerIP;
		}
		break;

	case Token_Tracker_Body:
		iCount = sscanf_s(pLine,
			"%s %s %s",
			szToken, _countof(szToken),
			szName, _countof(szName),
			szAlias, _countof(szAlias));


		if(iCount == 2)
		{
			strcpy_s(m_szCurrentBodyName, szName);
			strcpy_s(m_szCurrentAlias, szName);
		}
		else if (iCount == 3)
		{
			strcpy_s(m_szCurrentBodyName, szName);
			strcpy_s(m_szCurrentAlias, szAlias);
		}
		else
		{
			strcpy_s(m_szCurrentBodyName, "Unknown");
			strcpy_s(m_szCurrentAlias, "Unknown");
			DebugMsg("Body config entry not properly read\n");
			break;
		}

		pBodyConfig = new BodyConfig(m_szCurrentBodyName, m_szCurrentAlias);
		m_BodyConfigList.push_back(pBodyConfig);

		break;
	case Token_Tracker_Segment:
		{
			iCount = sscanf_s(pLine,
				"%s %s %s",
				szToken, _countof(szToken),
				szName, _countof(szName),
				szValue, _countof(szValue));

			if(iCount == 3)
			{
				SegmentConfig * pSegConfig = new SegmentConfig(m_szCurrentBodyName,
																szName,   
																atoi(szValue));
				BodyConfig * pBodyConfig = LookupBodyConfig(m_szCurrentBodyName);
				if(pBodyConfig)
				{
					pBodyConfig->m_SegmentConfigList.push_back(pSegConfig);
				}
				else
				{
					DebugMsg("Body config not found\n");
				}
			}
		}
		break;
	case Token_Tracker_Marker:
		{
			iCount = sscanf_s(pLine,
				"%s %s %s",
				szToken, _countof(szToken),
				szName, _countof(szName),
				szValue, _countof(szValue));

			if(iCount == 3)
			{
				MarkerConfig * pMarkerConfig = new MarkerConfig(m_szCurrentBodyName,
																szName,   
																atoi(szValue));
				BodyConfig * pBodyConfig = LookupBodyConfig(m_szCurrentBodyName);
				if(pBodyConfig)
				{
					pBodyConfig->m_MarkerConfigList.push_back(pMarkerConfig);
				}
				else
				{
					DebugMsg("Body config not found\n");
				}
			}
		}
		break;
	case Token_Tracker_Debug:
		{
			iCount = sscanf_s(pLine, 
				"%s %s", 
				szToken, _countof(szToken), 
				szValue, _countof(szValue)); 

			if(strcmp(szToken, StartTokenValues[Token_Tracker_Debug]) != 0) return false;

			m_bDebug = false;
			if(_strnicoll(szValue, "true", 4) == 0)
			{
				m_bDebug = true;
			}
		}
		break;
	case Token_Tracker_TrackerPerBody:
		{
			iCount = sscanf_s(pLine, 
				"%s %s", 
				szToken, _countof(szToken), 
				szValue, _countof(szValue)); 

			if(strcmp(szToken, StartTokenValues[Token_Tracker_TrackerPerBody]) != 0) return false;
			m_optionTracker = Tracker_PerBody;
			if(_stricoll(szValue, "false") == 0)
			{
				m_optionTracker = Tracker_PerSegment;
			}
		}
		break;
	case Token_Tracker_SendInvalidData:
		{
			iCount = sscanf_s(pLine, 
				"%s %s", 
				szToken, _countof(szToken), 
				szValue, _countof(szValue)); 

			if(strcmp(szToken, StartTokenValues[Token_Tracker_SendInvalidData]) != 0) return false;
			m_bSendInvalidData = false;
			if(_stricoll(szValue, "true") == 0)
			{
				m_bSendInvalidData = true;
			}
		}
		break;
	case Token_Tracker_Delimiter:
			
		iCount = sscanf_s(pLine,
			"%s %s",
			szToken, _countof(szToken),
			szValue, _countof(szValue));

			if(_stricmp(szToken, StartTokenValues[Token_Tracker_Delimiter]) != 0) return false;
			m_cDelimiter = szValue[0];
		break;

	default:
		return false;
		break;
	}
	return true;
}

BodyConfig * MAC_VRPN_Config::LookupBodyConfig(char * pName)
{
	std::list<BodyConfig*>::iterator iter;

	for(iter = m_BodyConfigList.begin(); iter != m_BodyConfigList.end(); iter++)
	{
		BodyConfig * pBodyConfig = (BodyConfig*)*iter;
		if(_stricmp(pName, pBodyConfig->m_szBodyName) == 0)
		{
			return pBodyConfig;
		}
	}
	return NULL;
}

void MAC_VRPN_Config::DebugMsg(const char* szMessage)
{
	if(m_bDebug)
	{
		Msg(szMessage);
	}
}

void MAC_VRPN_Config::Msg(const char* szMessage)
{
	cout << szMessage;
}