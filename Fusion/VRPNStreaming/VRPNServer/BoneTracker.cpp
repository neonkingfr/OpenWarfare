#include "stdafx.h"
#include "BoneTracker.h"
#include "SDK2_Connection.h"
#include "M3x3.h"
#include "VRPN_Config_File.h"

extern tSegmentData * g_pGTR_SegmentData[];	// GTR segment data for the last frame of data
extern HANDLE dataMutex;
extern MAC_VRPN_Config g_Config;

BoneTracker::BoneTracker( const char* name, vrpn_Connection *c /*= 0 */ , int iSensorNumber /* = 0 */) :
	vrpn_Tracker( name, c )
{
	char szMsg[MAX_PATH];

	d_sensor = iSensorNumber;
	sprintf(szMsg, "Tracker created: %s Sensor Index: %d\n",
		name,
		iSensorNumber);

	g_Config.DebugMsg(szMsg);
}

void BoneTracker::mainloop()
{
	// Update all tracker data in this method

	vrpn_gettimeofday(&_timestamp, NULL);
	vrpn_Tracker::timestamp = _timestamp;

	if(dataMutex)
	{
		DWORD dWaitResult = WaitForSingleObject(dataMutex, 2000);
		if(dWaitResult == WAIT_OBJECT_0)
		{
			if(g_pGTR_SegmentData[0])
			{
				// Get Cortex segment data
				// in Global coordinates for each segment
				const tSegmentData & tSegment = g_pGTR_SegmentData[m_iBodyOffset][m_iDataOffset];

				pos[0] = tSegment[0];// 1000;
				pos[1] = tSegment[1];// 1000;
				pos[2] = tSegment[2];// 1000;  
				double xRot, yRot, zRot;

				if(!g_Config.m_bSendInvalidData)
				{
					if(pos[0] == XEMPTY)
					{
						ReleaseMutex(dataMutex);
						return;
					}
				}

				xRot = Q_DEG_TO_RAD(tSegment[3]);
				yRot = Q_DEG_TO_RAD(tSegment[4]);
				zRot = Q_DEG_TO_RAD(tSegment[5]);

				// construct quaternion from cortex segment global euler angles:
				// These are in order as described in the quat.h comments:

				/*****************************************************************************
				 *  
					q_from_euler - converts 3 euler angles (in radians) to a quaternion
				     
				   Assumes roll is rotation about X, pitch
				   is rotation about Y, yaw is about Z.  Assumes order of 
				   yaw, pitch, roll applied as follows:
				       
					   p' = roll( pitch( yaw(p) ) )

					  See comments for q_euler_to_col_matrix for more on this.
				 *
				 *****************************************************************************/
				//void q_from_euler (q_type destQuat, double yaw, double pitch, double roll);

				q_from_euler(d_quat, zRot, yRot, xRot);
			}
			ReleaseMutex(dataMutex);
		}
		else
		{
			g_Config.DebugMsg("Mutex timeout in BoneTracker mainloop()\n");
		}
	}

	// pack network message
	char msgbuf[1000];
	int  len = vrpn_Tracker::encode_to(msgbuf);

	if (d_connection->pack_message(len, _timestamp, position_m_id, d_sender_id, msgbuf, vrpn_CONNECTION_LOW_LATENCY))
	{
		g_Config.Msg("can't write message: tossing\n");
	}
			
	server_mainloop();
}

