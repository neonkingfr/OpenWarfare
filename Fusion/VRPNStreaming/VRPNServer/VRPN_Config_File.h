/*
*		VRPN_Config_File.h
*		
*		Manages the configuration of the Cortex VRPN server
*		via the file vrpn.cfg
*
*		Copyright Motion Analysis Corporation 2011
*
*/

#pragma once

#include "stdafx.h"

#define MAX_LINE 1024
#define MAC_TRACKER_LINE_TOKEN "vrpn_Tracker_MotionAnalysis"
#define START_TOKEN_COUNT 8

#define DEBUG_FRAME_MESSAGE_COUNT 240

extern char * StartTokenValues[];

enum startTokenItem
{
	Token_Tracker = 0,
	Token_Tracker_Body,
	Token_Tracker_Segment,
	Token_Tracker_Marker,
	Token_Tracker_Debug,
	Token_Tracker_Delimiter,
	Token_Tracker_TrackerPerBody,
	Token_Tracker_SendInvalidData
};

enum trackerOption
{
	Tracker_PerBody = 0,
	Tracker_PerSegment
};

class SegmentConfig
{
public:
	SegmentConfig(char * pBodyName = "Unknown", char* pName = "Unknown", int iSensorIndex = 0);
	
	char m_szBodyName[MAX_PATH];
	char m_szName[MAX_PATH];
	int m_iSensorIndex;
};

class MarkerConfig
{
public:
	MarkerConfig(char * pBodyName = "Unknown", char * pName = "Unknown", int iSensorIndex = 0);

	char m_szBodyName[MAX_PATH];
	char m_szName[MAX_PATH];
	int m_iSensorIndex;
};

class BodyConfig
{
public:
	BodyConfig(char * pBodyName = "Unknown", char * pAliasName = "Unknown");
	~BodyConfig();

	SegmentConfig * FindSegmentConfig(char * pName);
	MarkerConfig * FindMarkerConfig(char * pName);

	char m_szBodyName[MAX_PATH];
	char m_szAliasName[MAX_PATH];

	std::list<SegmentConfig*> m_SegmentConfigList;
	std::list<MarkerConfig*> m_MarkerConfigList;
};

class MAC_VRPN_Config
{
public:

	MAC_VRPN_Config();
	~MAC_VRPN_Config();

	bool ReadConfig();

	// Connection details
	std::string & ClientIP();
	std::string & ServerIP();

	// Messaging functions
	void DebugMsg(const char * szMessage);
	void Msg(const char * szMessage);

	// Find a segment configuration by body name and segment name
	BodyConfig * LookupBodyConfig(char * szBody);

	bool m_bDebug;
	bool m_bSendInvalidData;

	trackerOption m_optionTracker;


	char * ReadLine(FILE * pFile);
	bool ParseLine(char * pLine, startTokenItem iStartTokenIndex);

	// These are temporary storage while reading the file
	// body name resides in the SegmentConfig or MarkerConfig
	char m_szLine[MAX_LINE];
	char m_szCurrentBodyName[MAX_PATH];
	char m_szCurrentAlias[MAX_PATH];

	// Character used to separate body name from sensor (segment) name 
	char m_cDelimiter;

	std::string m_sClientIP;
	std::string m_sServerIP;

	std::list<BodyConfig *> m_BodyConfigList;

};

