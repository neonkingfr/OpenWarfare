#pragma once


namespace TalonViewerLib
{
	const float CORTEX_XEMPTY = 9999999.0f; 

	//version 1 - initial ini file formant with no scaling in mappings
	//version 2 - joint mappings are now represented as full matrices
	//version 3 - no per/joint allow trans flag
	const int TV_CURRENT_FILE_VERSION = 3;

	const int TV_MAX_FILE      =  256;
	const int TV_MAX_NAME      =  128;
	const int TV_MAX_ERROR_MSG = 1024;

	const float TV_PI = 3.14159265f;

	enum tAxis
	{
		TV_X_AXIS = 0,
		TV_Y_AXIS = 1,
		TV_Z_AXIS = 2
	};

	enum tUnits
	{
		TV_METERS      = 0,
		TV_CENTIMETERS = 1,
		TV_MILLIMETERS = 2,
		TV_FEET        = 3,
		TV_INCHES      = 4
	};
};
