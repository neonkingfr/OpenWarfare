//******************************************************************************************************
//******************************************************************************************************
//
// Filename:	MACVector.h
// Author:		Ian Elsley
// Created:		26th Feb, 2001
//
//
// Documentation:
//		Vector classes and templates for the Solver Interface. This library is the prototype for the
//		maths library in general.
//
//
//
// Log:
//		Created 2/27/2001, Ian Elsley
//
// Notes:
//
//		There should perhaps be failure with asserts on access to non existant members
//
// Copyright (c) 2001 Motion Analysis Corportation.
// All rights reserved.
//
//
// This copyright needs updating, but this code is internal to MAC. Do not use, copy, look at or breathe.
//
//******************************************************************************************************
//******************************************************************************************************

#ifndef __MAC_VECTOR_H__
#define __MAC_VECTOR_H__

MAC_MATH_LIB_BEGIN

//********************************************
//
// Predefinition of classes
//
template<class T> class MACVector2;
template<class T> class MACVector3;
template<class T> class MACVector4;


#define MAC_VECTOR_ERROR_BOUND		(0.001)

//*********************************************************************
//*********************************************************************
//
// Start of templated class definitions
//

//
// MACVector
//
// Notes:
// Log:
//		Created 3/2/01, Ian Elsley
//
template<class T>
class MACVector
{

	public:		/*** Constructors and destructors ***/


		virtual ~MACVector<T>()		{}

		
	public:		/*** Accessor class virtual calls ***/
				

		virtual T&			operator[]	(const unsigned int i)			= 0;
		virtual const T&	operator[]	(const unsigned int i) const	= 0;

		virtual T	X	()	const		= 0;
		virtual T	Y	()	const		= 0;
		virtual T	Z	()	const		= 0;
		virtual T	W	()	const		= 0;


	public:

		T			dot3	(const MACVector<T>& other)	const;
		friend T	dot3	(const MACVector<T>& v1, const MACVector<T>& v2);
};

//
// End of templated class definitions
//
//***********************************************************************
//***********************************************************************


//*********************************************************************
//*********************************************************************
//
// Start of Method Code
//


// 
// Dot3 Product
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline T
MACVector<T>::dot3	(const MACVector<T>& v) const
{
	return (this->X()*v.X() + this->Y()*v.Y() + this->Z()*v.Z());
}


//
// End of Method Code
//
//***********************************************************************
//***********************************************************************


//***********************************************************************
//***********************************************************************
//
// Start of Function Code
//


// 
// Dot3 Product Function
// Notes:
//		dot product function as opposed to method
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> T
dot3	(const MACVector<T>& v1, const MACVector<T>& v2)
{
	return (v1.X()*v2.X() + v1.Y()*v2.Y() + v1.Z()*v2.Z());
}

template<class T> MACVector3<T>
cross3(const MACVector<T>& v1, const MACVector<T>& v2)
{
	MACVector3<T> result;
	
	// Cross product implemented in this fashion in case v1 or v2 is *this
	T	x1 = v1.X(), 
		y1 = v1.Y(),
		z1 = v1.Z(),
		x2 = v2.X(),
		y2 = v2.Y(),
		z2 = v2.Z();

	result.X(y1*z2 - z1*y2);
	result.Y(z1*x2 - x1*z2);
	result.Z(x1*y2 - y1*x2);

	return result;
}


//
// End of Function code
//
//***********************************************************************
//***********************************************************************

MAC_MATH_LIB_END


//********************************************
//
// Include
//
#include "MACVector2.h"
#include "MACVector3.h"
#include "MACVector4.h"

#endif __MAC_VECTOR_H__