//******************************************************************************************************
//******************************************************************************************************
//
// Filename:	MACMatrix.h
// Author:		Ian Elsley
// Created:		26th Feb, 2001
//
//
// Documentation:
//		Generalised square matrix class.
//		This will be used by all other matrix types as the generic header class.
//		In the main, all accesses to these classes will be via this.
//
//
//
// Log:
//		Created 3/1/2001, Ian Elsley
//
// Notes:
//
//      MACMFlags are not used at the moment. This iwill be extended when full generalisation required.
//
//		Failure is currently through assert though there will be throws. Users should not use local memory
//		pointers but should use the inbuilt ptr<T> forms instead to ensure cleanup on rewind.
//
// Copyright (c) 2001 Motion Analysis Corportation.
// All rights reserved.
// 
//
// This copyright needs updating, but this code is internal to MAC. Do not use, copy, look at or breathe.
//
//******************************************************************************************************
//******************************************************************************************************

#ifndef __MAC_MATRIX_H__
#define __MAC_MATRIX_H__

MAC_MATH_LIB_BEGIN

//*********************************************************************
//*********************************************************************
// 
// Start of class definitions
//

//
// Notes:
//		Will be used as a method of keeping track of matrices to make code more efficient
//
class MACMFlags 
{

	public:		/*** Constants ***/

		enum {
			IDENTITY			=	0x001,
			ROTATE				=	0x002,
			TRANSLATE			=	0x004,
			SCALE				=	0x008,
			UNIFORM_SCALE		=	0x010,
			SHEAR				=	0x020,
			UNKNOWN				=	0x800,
		};


	private:	/*** Variables ***/

		unsigned int m_flags;


	public:		/*** Constructors and Destructors ***/

		MACMFlags()						: m_flags(UNKNOWN)		{}
		MACMFlags(const MACMFlags& v)	: m_flags(v.m_flags)	{}
		MACMFlags(unsigned int v)		: m_flags(v)			{}

		virtual ~MACMFlags() {}


	public:		/*** Access	***/

		unsigned int	get()					const	{ return m_flags;					}
		
		MACMFlags&		set(const MACMFlags& v)			{ m_flags = v.get(); return *this;	}
		MACMFlags&		set(unsigned int v)				{ m_flags = v; return *this;		}

		// Assignment
		MACMFlags&	operator=	(const MACMFlags& v)	{ return set(v);					}
		MACMFlags&	operator=	(unsigned int v)		{ return set(v);					}
		MACMFlags&	operator|=	(const MACMFlags& v)	{ m_flags |= v.get(); return *this;	}
		MACMFlags&	operator|=	(unsigned int v)		{ m_flags |= v; return *this;		}
		MACMFlags&	operator&=	(const MACMFlags& v)		{ m_flags &= v.get(); return *this;	}
		MACMFlags&	operator&=	(unsigned int v)		{ m_flags &= v; return *this;		}


	public:		/*** Comparitors ***/

		bool	equal		(const MACMFlags& v)	const	{ return (m_flags == v.get());		}
		bool	equal		(unsigned int v)		const	{ return equal(v);					}

		bool	operator==	(const MACMFlags& v)	const	{ return equal(v.get());			}
		bool	operator==	(unsigned int v)		const	{ return equal(v);					}

		bool	operator!=	(const MACMFlags& v)	const	{ return !equal(v.get());			}
		bool	operator!=	(unsigned int v)		const	{ return !equal(v);					}

		bool	operator|	(const MACMFlags& v)	const	{ return (m_flags | v.get()) != 0;	}
		bool	operator|	(unsigned int v)		const	{ return (m_flags | v) != 0;		}

		bool	operator&	(const MACMFlags& v)	const	{ return (m_flags & v.get()) != 0;	}
		bool	operator&	(unsigned int v)		const	{ return (m_flags & v) != 0;		}

	

};

//
// End Of Class Definitions
//
//*********************************************************************
//*********************************************************************

MAC_MATH_LIB_END

#include "MACRotationMatrix3.h"
#include "MACHomogeneousMatrix4.h"

#endif __MAC_MATRIX_H__