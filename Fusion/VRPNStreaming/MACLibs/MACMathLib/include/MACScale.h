//******************************************************************************************************
//******************************************************************************************************
//
// Filename:	MACScale.h
// Author:		Ian Elsley
// Created:		26th Feb, 2001
//
//
// Documentation:
//		Generalised abstract Scale header class.
//		This will be used by all other Scaleantion types as the generic header class.
//		In the main, all accesses to these classes will be via this.
//
//
//
// Log:
//		Created 3/1/2001, Ian Elsley
//
// Notes:
//
//		Failure is currently through assert though there will be throws. Users should not use local memory
//		pointers but should use the inbuilt ptr<T> forms instead to ensure cleanup on rewind.
//
// Copyright (c) 2001 Motion Analysis Corportation.
// All rights reserved.
//
//
// This copyright needs updating, but this code is internal to MAC. Do not use, copy, look at or breathe.
//
//******************************************************************************************************
//******************************************************************************************************


#ifndef __MAC_SCALE_H__
#define __MAC_SCALE_H__

#include <iostream>

MAC_MATH_LIB_BEGIN

//*********************************************************************
//*********************************************************************
//
// Instanciated Scales
//
// Notes:
// Log:
//		Created 3/5/01, Ian Elsley
//
typedef MACScale<float>		MACScalef;
typedef MACScale<double>	MACScaled;
typedef MACScale<int>		MACScalei;
//
// End of Instanciated Scales
//
//*********************************************************************
//*********************************************************************

//*********************************************************************
//*********************************************************************
//
// Start of templated class definitions
//

//
// MACScale<T>
// Notes: 
//		Generalised Abstract Class for use as a container for all forms of Scale.
//		No variables can be defined within this.
//
template<class T>
class MACScale : public MACTransform<T> 
{

	private:
		
		MACVector3<T>	scale;


	public:

		MACScale<T>	()										: scale((T)1) {}
		MACScale<T>	(const T& v)							: scale(v) {}
		MACScale<T>	(const T& vx, const T& vy, const T& vz)	: scale(vx, vy, vz) {}
		MACScale<T>	(const MACVector<T>& v)					: scale(v) {}

		virtual	~MACScale<T>		()						{}

	public:		/*** Access - Set and Get ***/

		
		// Standard array access
		T&			operator[]	(const unsigned int i)			{ return scale[i]; }
		const T&	operator[]	(const unsigned int i) const	{ return scale[i]; }

	
		// Get the values	
		T			X()	const						{ return scale.X(); }
		T			Y()	const						{ return scale.Y(); }
		T			Z()	const						{ return scale.Z(); }
													/* This should perhaps fail with an assert */

		// Set the values
		T			X(T v)							{ return scale.X(v); }
		T			Y(T v)							{ return scale.Y(v); }
		T			Z(T v)							{ return scale.Z(v); }
													/* This should perhaps fail with an assert */

		// Set the values in a group
		MACScale&	set(const T& v)					{ scale.set(v); return *this }
		MACScale&	set(const T& vx, const T& vy, const T& vz)	
													{ scale.set(vx, vy, vz); return *this; }
		MACScale&	set(const MACVector<T>& v)		{ scale.set(v); return *this; }


	public:		/*** Comparitors ***/

		bool		equal		(const MACScale<T>& v, T error = ERROR_VALUE)	const;
		bool		operator==	(const MACScale<T>& v)							const;
		bool		operator!=	(const MACScale<T>& v)							const;


	public:		/*** Arithmetic Operators ***/

		MACScale<T>&	operator*=	(const T v);
	
	public:		/*** Identity Functions ***/

		MACScale<T>		identity	()				{ return MACScale<T>((T)1); }
		MACScale<T>&	setIdentity	()				{ scale.set(identity); return *this; }
		bool			isIdentity	()				{ return equal(identity); }


	protected:	/*** IO functions ***/

		friend std::istream& operator>>(std::istream&, MACScale<T>&);
		friend std::ostream& operator<<(std::ostream&, MACScale<T>&);
		// Note instanciation of the base class will have the extra effect of correctly instaciating these

};

//
// End of Templated class definitions
//
//*********************************************************************
//*********************************************************************


//*********************************************************************
//*********************************************************************
//
// Templated Method Code
//

//************************************
//
// Comparitor Functions
//
//

// 
// MACScale<T>::equal(const MACScale<T>& v, T error = ERROR_VALUE)	const
// Notes:
//		Allied with this function are the explicit instanciations of the float and double cases
//		These are identical, but are implemented like this for speed.
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline bool
MACScale<T>::equal(const MACScale<T>& r, T error)					const
{
	return (((T)fabs((double)(X()-r.X())) < error) && 
			((T)fabs((double)(Y()-r.Y())) < error) && 
			((T)fabs((double)(Z()-r.Z())) < error));
}

//
// Equal
// Notes:
//		Double version
// Log:
//		Created 2/27/01, Ian Elsley
//
inline bool
MACScale<double>::equal(const MACScale<double>& r, double error)	const
{
	return ((fabs(X()-r.X()) < error) && 
			(fabs(Y()-r.Y()) < error) && 
			(fabs(Z()-r.Z()) < error));
}

//
// Equal
// Notes:
//		Float version
// Log:
//		Created 2/27/01, Ian Elsley
//
inline bool
MACScale<float>::equal(const MACScale<float>& r, float error)		const
{
	return ((fabsf(X()-r.X()) < error) && 
			(fabsf(Y()-r.Y()) < error) && 
			(fabsf(Z()-r.Z()) < error));
}


//
// MACScale<T>::operator==(const MACScale<T>& v) const
// Notes:
//		This function may not work entirely as expected. 
//		There may be an error factor required.
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline bool
MACScale<T>::operator==(const MACScale<T>& v)						const
{
	return ((X()==v.X()) && (Y()==v.Y()) && (Z()==v.Z()));
}


//
// MACScale<T>::operator!=(const MACScale<T>& v) const
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline bool
MACScale<T>::operator!=(const MACScale<T>& v)						const
{
	return (!((*this) == v));
}


//
// MACScale<T>::operator*=(const MACScale<T>& v) const
// Notes:
// Log:
//		Created 7/18/01, Ian Elsley
//
template<class T> MACScale<T>&
MACScale<T>::operator*=(const T v)
{
	scale *= v;
	return *this;
}


//
// End of Templated Method Code
//
//*********************************************************************
//*********************************************************************


//*********************************************************************
//*********************************************************************
//
// Start of Function Code
//


//************************************************
//
// I/O Functions
//
// Notes:
//

//
// operator>>
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> std::istream& 
operator>>(std::istream& in, MACScale<T>& v)
{
	return in >> v;
}

//
// operator<<
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> std::ostream& 
operator<<(std::ostream& out, MACScale<T>& v)
{
	return out << v;
}

//
// End of Function Code
//
//*********************************************************************
//*********************************************************************

MAC_MATH_LIB_END

#endif __MAC_SCALE_H__