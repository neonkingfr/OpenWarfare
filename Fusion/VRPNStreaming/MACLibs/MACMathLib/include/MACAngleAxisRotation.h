//******************************************************************************************************
//******************************************************************************************************
//
// Filename:	MACAngleAxisRotation.h
// Author:		Ian Elsley
// Created:		26th Feb, 2001
//
//
// Documentation:
//		Generalised Angle Axis rotation header class.
//		This will be used by all other rotation types as the generic header class.
//		In the main, all accesses to these classes will be via this.
//
//
//
// Log:
//		Created 3/1/2001, Ian Elsley
//
// Notes:
//
//		Failure is currently through assert though there will be throws. Users should not use local memory
//		pointers but should use the inbuilt ptr<T> forms instead to ensure cleanup on rewind.
//
// Usable functionality:
//		set(v1, v2) seets the rotation from v1 to v2.
//
// Copyright (c) 2001 Motion Analysis Corportation.
// All rights reserved.
//
//
// This copyright needs updating, but this code is internal to MAC. Do not use, copy, look at or breathe.
//
//******************************************************************************************************
//******************************************************************************************************


#ifndef __MAC_ANGLE_AXIS_ROTATION_H__
#define __MAC_ANGLE_AXIS_ROTATION_H__

#include <iostream>

MAC_MATH_LIB_BEGIN

//*********************************************************************
//*********************************************************************
//
// Instanciated Rotations
//
// Notes:
// Log:
//		Created 28/2/01, Ian Elsley
//

typedef	MACAngleAxisRotation<double>	MACAARotd;
typedef MACAngleAxisRotation<float>		MACAARotf;


//
// End of instanciation
//
//*********************************************************************
//*********************************************************************


//*********************************************************************
//*********************************************************************
//
// Templated Class Definitions
//
template<class T>
class MACAngleAxisRotation : public MACRotation<T>
{

	private:


		T				m_angle;
		MACVector3<T>	m_axis;


	public:		/*** Constructors and destructors ***/


		MACAngleAxisRotation<T>	()														: m_angle((T)0), m_axis((T)1, (T)0, (T)0) {}
		MACAngleAxisRotation<T>	(const T& angle, const T& rx, const T& ry, const T& rz)	: m_angle((T)angle), m_axis(rx, ry, rz) {}
		MACAngleAxisRotation<T>	(const T& angle, const MACVector3<T>& axis)				: m_angle(angle), m_axis(axis) {}
		MACAngleAxisRotation<T> (const MACVector<T>& v1, const MACVector<T>& v2)		{ this->set(v1, v2); }

		MACAngleAxisRotation<T>	(const MACAngleAxisRotation<T>& v)						: m_angle(v.A()), m_axis(v.Axis()) {}
		MACAngleAxisRotation<T>	(const MACEulerRotation<T>&  v)							{ this->set(v); }
		MACAngleAxisRotation<T>	(const MACQuaternion<T>& v)								{ this->set(v); }
		MACAngleAxisRotation<T>	(const MACRotationMatrix3<T>& v)						{ this->set(v); }
		// This can be improved by tracing the matrix minus its transpose

		virtual ~MACAngleAxisRotation<T>	()						{}


	public:		/*** Access - Set and Get ***/

		
		// Set the values in a group
		MACAngleAxisRotation<T>&	set	(const T& angle)								{ A(angle); return *this; }	
		MACAngleAxisRotation<T>&	set	(const MACVector3<T>& axis)						{ m_axis = axis; return *this; }			
		MACAngleAxisRotation<T>&	set	(const T& vx, const T& vy, const T& vz)			{ X(vx); Y(vy); Z(vz); return *this; }			
		MACAngleAxisRotation<T>&	set	(const T& angle, const T& vx, const T& vy, const T& vz)	
																						{ A(angle); X(vx); Y(vy); Z(vz); return *this; }
		MACAngleAxisRotation<T>& set (const MACVector<T>& v1, const MACVector<T>& v2);	// rotation from v1 to v2

		
		MACAngleAxisRotation<T>&	set	(const MACEulerRotation<T>& v);					// NOTE: Euler angle transform is SLOW 
		MACAngleAxisRotation<T>&	set	(const MACAngleAxisRotation<T>& v)				{ A(v.A()); Axis(v.Axis()); return *this; }
		MACAngleAxisRotation<T>&	set	(const MACQuaternion<T>& v);
		MACAngleAxisRotation<T>&	set	(const MACRotationMatrix3<T>& v)				{ return this->set(MACQuaternion<T>(v)); }

		
		// Get the values
		T					A		()	const					{ return m_angle;			}	
		T					X		()	const					{ return m_axis.X();		}
		T					Y		()	const					{ return m_axis.Y();		}
		T					Z		()	const					{ return m_axis.Z();		}
		const MACVector3<T>&	Axis	()	const				{ return m_axis;			}	
		// Bad form for the code standard - fix Axis to something else - Larry?
		// Also correct use of const?
		

		// Set the values
		T					A		(T v)						{ return m_angle	= v;	}
		T					X		(T v)						{ return m_axis.X(v);		}
		T					Y		(T v)						{ return m_axis.Y(v);		}
		T					Z		(T v)						{ return m_axis.Z(v);		}
		const MACVector3<T>& Axis	(const MACVector3<T>& v)	{ return m_axis		= v;	}	
		// Bad form for the code standard - fix Axis to something else - Larry?
		// Also correct use of const?


	public:		/*** General Arithmetic functions ***/


		MACAngleAxisRotation<T>& invert		()					{ A(-A()); return *this; }
		MACAngleAxisRotation<T>	theInverse	()	const			{ return MACAngleAxisRotation<T>(-A(), X(), Y(), Z()); }

		MACAngleAxisRotation<T>&	normalize	()				{ m_axis.normalize(); return *this; }


	public:		/*** General Arithmetic operators ***/

		// Assignment
		MACAngleAxisRotation<T>&	operator=	(const MACAngleAxisRotation& r)		{ return this->set(r); }
		MACAngleAxisRotation<T>&	operator*=	(const MACAngleAxisRotation<T>& v);
		
		// Arithmetic
		MACAngleAxisRotation<T>	operator*	(const MACAngleAxisRotation<T>& v)	const;
		MACAngleAxisRotation<T>	operator-	()	const								{ return theInverse(); }
		

	public:		/*** Comparitors ***/

		bool					equal		(const MACAngleAxisRotation<T>& v, T error = MAC_ANGLE_AXIS_ERROR_LIMIT)	const;
		bool					operator==	(const MACAngleAxisRotation<T>& v)											const;
		bool					operator!=	(const MACAngleAxisRotation<T>& v)											const;

	
	public:		/*** Identity Functions ***/

		MACAngleAxisRotation<T>		identity	()		{ return MACAngleAxisRotation<T>((T)0, (T)1, (T)0, (T)0); }
		MACAngleAxisRotation<T>&	setIdentity	()		{ set((T)0, (T)1, (T)0, (T)0); return *this; }
		bool						isIdentity	()		{ return equal(identity()); }
	
		

	protected:	/*** IO functions ***/

		friend std::istream& operator>>(std::istream&, MACAngleAxisRotation<T>&);
		friend std::ostream& operator<<(std::ostream&, MACAngleAxisRotation<T>&);
		// Note instanciation of the base class will have the extra effect of correctly instaciating these

};

//
// End of Class Definitions
//
//*********************************************************************
//*********************************************************************

//*********************************************************************
//*********************************************************************
//
// Templated Method Code
//

//
// Notes:
//		Takes two vectors and produces the rotation to rotate one to the other.
// Log:
//		Created 14th March, Ian Elsley
//
template<class T> MACAngleAxisRotation<T>&	
MACAngleAxisRotation<T>::set (const MACVector<T>& v1, const MACVector<T>& v2)
{
	MACVector3<T> vna(v1), vnb(v2);

	vna.normalize();
	vnb.normalize();

	A((T)acos((double)(dot(vna, vnb))));
	Axis(cross(vna, vnb).normalize());					// Normalize not needed as already normalised

	return *this;
}

//
// Notes:
//		SLOW SLOW SLOW
// Log:
//		Created 16th March, Ian Elsley
//
template<class T> MACAngleAxisRotation<T>&	
MACAngleAxisRotation<T>::set	(const MACEulerRotation<T>& v)
{
	return this->set( MACQuaternion<T>(v) );
}

//
// Notes:
//		Approximation used to stop worst of inccuracies... should be quite robust and quick
//		Does not guarentee unit length vector.
// Log:
//		Created 16th March, Ian Elsley
//
template<class T> MACAngleAxisRotation<T>&	
MACAngleAxisRotation<T>::set	(const MACQuaternion<T>& v)
{
	/*
	auto double theta	= acos((double)v.W()) * 2.0;
	auto double st2		= sin(theta/2.0);
	double		lenSqr;

	// Are we close to no rotation?
	if (fabs(st2) < MAC_N_NEAR_ZERO)

		this->setIdentity();

	else {

		A((T)theta);														// set the angle
		m_axis.set (v.X()/(T)st2, v.Y()/(T)st2, v.Z/(T)st2);				// set the vector

		// Make sure we keep some accuracy if the vector is very short
		if ((lenSqr = (double)m_axis.lengthSquared()) < 0.2)	// 0.2 = some totally arbitrary number, where 0 < number < 1.0
		{

			if (lenSqr > MAC_LENGTH_NEAR_ZERO) 
				m_axis *= (T)sqrt(lenSqr);

			else 
				this->setIdentity();
		}
	}
	*/
	

	// This is VERY inefficient  -  Ian
	MACQuaternion<T>		aQuat(v);
	aQuat.normalize();

	double cos_a = aQuat.W();
	m_angle = (T)(acos(cos_a) * 2.0);
	T sin_a = (T)sqrt(1.0 - cos_a * cos_a);
	
	if (fabs((double)sin_a) < 0.0005) sin_a = (T)1.0;

	m_axis.X(aQuat.X() / sin_a);
	m_axis.Y(aQuat.Y() / sin_a);
	m_axis.Z(aQuat.Z() / sin_a);

	if (m_axis.X() == 0 && m_axis.Y() == 0 && m_axis.Z() == 0)
	{
		m_angle = 0;
		m_axis.X(1);
	}

	return *this;
}
			

//////////////////////////////////////////////
//
// General Arithmetic Assignment Operations
//
// Notes:
//

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACAngleAxisRotation<T>&
MACAngleAxisRotation<T>::operator*=	(const MACAngleAxisRotation<T>& v)
{
	_ASSERT(0);
	return *this;
}



//////////////////////////////////////////////
//
// General Arithmetic Operations
//
// Notes:
//

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACAngleAxisRotation<T>
MACAngleAxisRotation<T>::operator*	(const MACAngleAxisRotation<T>& v)	const
{
	_ASSERT(0);
	return MACAngleAxisRotation<T>();
}

// Log:
//		Created 2/27/01, Ian Elsley
//template<class T> inline MACAngleAxisRotation<T>
//MACAngleAxisRotation<T>::operator-	(const MACAngleAxisRotation<T>& v)	const
//{
//	_ASSERT(0);
//	return MACAngleAxisRotation<T>();
//}


//////////////////////////////////////////////
//
// Comparitor Functions
//
//

// 
// equal(const MACAngleAxisRotation<T>& v, T error = ERROR_VALUE)	const
// Notes:
//		Allied with this function are the explicit instanciations of the float and double cases
//		These are identical, but are implemented like this for speed.
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline bool
MACAngleAxisRotation<T>::equal(const MACAngleAxisRotation<T>& r, T error)					const
{
	return (((T)fabs((double)(X()-r.X())) < error) && 
			((T)fabs((double)(Y()-r.Y())) < error) && 
			((T)fabs((double)(Z()-r.Z())) < error) && 
			((T)fabs((double)(A()-r.A())) < error));
}

//
// equal(const MACAngleAxisRotation<double>& r, double error)	const
// Notes:
//		Double version
// Log:
//		Created 2/27/01, Ian Elsley
//
inline bool
MACAngleAxisRotation<double>::equal(const MACAngleAxisRotation<double>& r, double error)	const
{
	return ((fabs(X()-r.X()) < error) && 
			(fabs(Y()-r.Y()) < error) && 
			(fabs(Z()-r.Z()) < error) && 
			(fabs(A()-r.A()) < error));
}

//
// equal(const MACAngleAxisRotation<float>& r, float error)		const
// Notes:
//		Float version
// Log:
//		Created 2/27/01, Ian Elsley
//
inline bool
MACAngleAxisRotation<float>::equal(const MACAngleAxisRotation<float>& r, float error)		const
{
	return ((fabsf(X()-r.X()) < error) && 
			(fabsf(Y()-r.Y()) < error) && 
			(fabsf(Z()-r.Z()) < error) && 
			(fabsf(A()-r.A()) < error));
}


//
// operator==(const MACAngleAxisRotation<T>& v) const
// Notes:
//		This function may not work entirely as expected. 
//		There may be an error factor required.
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline bool
MACAngleAxisRotation<T>::operator==(const MACAngleAxisRotation<T>& v)						const
{
	return ((X()==v.X()) && (Y()==v.Y()) && (Z()==v.Z()) && (A()==v.A()));
}


//
// operator!=(const MACAngleAxisRotation<T>& v) const
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline bool
MACAngleAxisRotation<T>::operator!=(const MACAngleAxisRotation<T>& v)						const
{
	return (!((*this) == v));
}



//
// End of Method Code
//
//*********************************************************************
//*********************************************************************

//*********************************************************************
//*********************************************************************
//
// Start of Function Code
//


// 
// Dot Product Function
// Notes:
//		dot product function as opposed to method
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> T
dot(const MACAngleAxisRotation<T>& v1, const MACAngleAxisRotation<T>& v2)
{
	return (v1.X()*v2.X() + v1.Y()*v2.Y() + v1.Z()*v2.Z() + v1.w*v2.w);
}



//*********************************************************************
//*********************************************************************
//
// I/O Functions
//
// Notes:
//

//
// operator>>
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> std::istream& 
operator>>(std::istream& in, MACAngleAxisRotation<T>& v)
{
	return in >> m_angle >> m_axis;
}

//
// operator<<
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> std::ostream& operator<<(std::ostream& out, MACAngleAxisRotation<T>& v)
{
	return out << m_angle << ' ' << m_axis;
}

//
// End of Function Code
//
//*********************************************************************
//*********************************************************************



MAC_MATH_LIB_END

#endif __MAC_ANGLE_AXIS_ROTATION_H__