 #pragma once


namespace TalonViewerLib
{
	/* Simple vector class */
	class cVector
	{
	public:
		/* Constructor */
		cVector(); //0,0,0
		cVector(float X, float Y, float Z);

		/* Access */
		float X() const;
		float Y() const;
		float Z() const;
		const float *V() const;

		/* Modify */
		void Set(float X, float Y, float Z);
		void X(float val);
		void Y(float val);
		void Z(float val);

		/* operators */
		float Length() const;
		void Normalize();
		float Dot(const cVector &V) const;
		cVector Cross(const cVector &V) const;

		float &operator[](int xyz)      ;
		float  operator[](int xyz) const;
		
		cVector operator*(float Scalar) const;
		cVector operator/(float Scalar) const;
		cVector &operator*=(float Scalar);
		cVector &operator/=(float Scalar);
		cVector operator-(const cVector &V) const;
		cVector operator+(const cVector &V) const;
		cVector &operator-=(const cVector &V);
		cVector &operator+=(const cVector &V);

	private:
		/* The vector */
		float m_V[3];
	};
}