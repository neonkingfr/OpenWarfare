#pragma once
#include "TalonViewerLib.h"
#include "cSkeletonTransform.h"
#include <stdexcept>


namespace TalonViewerLib
{
	/** Throw a runtime error */
	void ThrowRuntimeError(const char *fmt, ...);


	/** Check that an index is in the range [0,numItems] */
	void CheckIndex(size_t iIndex, size_t numItems);


	/** Check for an OpenGL error. Throw a runtime error if there is a GL error. */
	void CheckGL(const char *fmt, ...);


	/** Check for a Cg error. Throw a runtime error if there is a Cg error. */
	void CheckCg(const char *fmt, ...);


	/** Print a debug message */
	void DPrintf(const char *fmt, ...);
	void PopupNotice(char *fmt, ...);


	/** Clamp a value */
	template<typename T>
	void Clamp(T low, T high, T &value)
	{
		if(value < low)
		{
			value = low;
		}
		if(value > high)
		{
			value = high;
		}
	}


	/** Degrees to radians */
	template<typename T>
	T DegToRad(T deg)
	{
		return deg * ((T)TV_PI)/((T)180);
	}


	/** Radians to degrees */
	template<typename T>
	T RadToDeg(T rad)
	{
		return rad * ((T)180)/((T)TV_PI);
	}


	/** Swap two items */
	template<typename T>
	void Swap(T &T1, T &T2)
	{
		T temp = T1;
		T1 = T2;
		T2 = temp;
	}


	void Assert(bool Condition);
}
