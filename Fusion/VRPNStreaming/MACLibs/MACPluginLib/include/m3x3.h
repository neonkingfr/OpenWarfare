
#ifndef M3x3_H
#define M3x3_H

#define LOCAL static
#define GLOBAL

#define PI 3.14159265358979
#define XEMPTY 9999999.0f
#define ABS(x) (((x)>0)?(x):-(x))
#define MIN(x1,x2) (((x1)<(x2))?(x1):(x2))
#define MAX(x1,x2) (((x1)>(x2))?(x1):(x2))
#define RND(F) ((int)(F > 0 ? (F+0.5) : (F-0.5)))

#define APPLY_BOUNDS(v,low,hi) if ((v)<(low)) (v)=(low); else if ((v)>(hi)) (v)=(hi)

#define MAX_INT 0x7FFFFFFF
#define MIN_INT 0x80000000
#define ERRFLAG -1
#define MEMERRFLAG -2

#define X_AXIS  0
#define Y_AXIS  1
#define Z_AXIS  2

#define ZYX_ORDER 1
#define XYZ_ORDER 2
#define YXZ_ORDER 3
#define YZX_ORDER 4
#define ZXY_ORDER 5
#define XZY_ORDER 6

// Special rotation orders
#define XYX_ORDER 7
#define XZX_ORDER 8
#define YZY_ORDER 9
#define YXY_ORDER 10
#define ZXZ_ORDER 11
#define ZYZ_ORDER 12

extern const char* szAngleOrders[13];
extern const char* szAxes[3];


void  M3x3_MulMatrix(const double m1[3][3], const double m2[3][3], double p[3][3]);
void  M3x3_MulVector(const double m[3][3], const double v[3], double p[3]);
void  M3x3_MulVector(const double v[3], const double m[3][3], double p[3]);
void  M3x3_LoadIdentity(double m[3][3]);
void  M3x3_Transpose(double m[3][3]);
void  M3x3_Copy(double src[3][3], double dst[3][3]);
void  M3x3_RotateX(double input[3][3], double degrees, double result[3][3]);
void  M3x3_RotateY(double input[3][3], double degrees, double result[3][3]);
void  M3x3_RotateZ(double input[3][3], double degrees, double result[3][3]);

void  M3x3_ExtractEulerAngles(
        const double matrix[3][3],
        const int    iRotationOrder,
        double angles[3]);

void  M3x3_ExtractAndCorrectEulerAngles(
        const double matrix[3][3],
        const int    iRotationOrder,
        double prevangles[3],
        double angles[3]);

void  M3x3_ConstructRotationMatrix(
        double ax,
        double ay,
        double az,
        int iRotationOrder,
        double matrix[3][3]);

int M3x3_BuildVMarkerRotationMatrix(
    double p0[3], // origin marker
    double p1[3], // long axis marker
    double p2[3], // plane marker
    double matrix[3][3]);

void  M3x3_ConvertToAxisAngle(double m[3][3], double* dAngle, double Axis[3]);
void  M3x3_AxisAngle2Matrix(double Axis[3], double dAngle, double m[3][3]);

int RotationOrder_StringToInt(const char *szOrder);

double MakeAngleContinuous(double angle, double prevangle);

#endif
