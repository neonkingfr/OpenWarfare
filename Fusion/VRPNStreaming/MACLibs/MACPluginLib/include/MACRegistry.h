#ifndef __MACREGISTRY_H__
#define __MACREGISTRY_H__


//
// Standard Headers
//

#include <string>
#include <fstream>


//
// SDK Headers
//

#include <windows.h>


//
// Class Definition
//

class MACRegistry
{
private:
	bool			mReady;
	HKEY			mKey;

public:
	MACRegistry		();
	MACRegistry		(const char *plugin, const char *package, const char *section);
	~MACRegistry	();

	bool			setKey				(const char *plugin, const char *package, const char *section);

	bool			getBool				(const char *valueName, bool &val) const;
	bool			setBool				(const char *valueName, const bool val) const;

	bool			getInt				(const char *valueName, int &val) const;
	bool			setInt				(const char *valueName, const int val) const;

	bool			getLong				(const char *valueName, long &val) const;
	bool			setLong				(const char *valueName, const long val) const;

	bool			getFloat			(const char *valueName, float &val) const;
	bool			setFloat			(const char *valueName, const float val) const;

	bool			getDouble			(const char *valueName, double &val) const;
	bool			setDouble			(const char *valueName, const double val) const;

	bool			getString			(const char *valueName, std::string &val) const;
	bool			setString			(const char *valueName, const std::string val) const;

	bool			existsValue			(const char *valueName) const;
	bool			removeValue			(const char *valueName) const;
};

#endif
