#include <string>

#include "pluginHeader.h"
#include "poseControlUnit.h"

#include "util/UnitUtilities.h"
#include "util/MissionUtilities.h"
#include "DisplayFunctions.h"

#include "../vrpn/vrpn/vrpn_Connection.h" // Missing this file?  Get the latest VRPN distro at
#include "../vrpn/vrpn/vrpn_Tracker.h"    // ftp://ftp.cs.unc.edu/pub/packages/GRIP/vrpn

#include "VRPNClient.h"

VRPNClient* g_pClient = NULL;

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
  if (g_pClient)
  {
    g_pClient->OnSimStep();
  }
};


VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
	static const char result[]="[1.0]";

	if (input[0] == 'i')
	{	
    if (!g_pClient)
    {
      g_pClient = new VRPNClient();
      g_pClient->SetPoseControlEnabled(true);
    }
	}

	else if (input[0] == 'd')
	{
    g_pClient->SetPoseControlEnabled(false);
	}

	else if (input[0] == 'e')
	{
    g_pClient->SetPoseControlEnabled(true);
	}

	return result;
};

VBSPLUGIN_EXPORT void WINAPI OnUnload()
{
  if (g_pClient)
  {
    g_pClient->SetPoseControlEnabled(false);
    delete g_pClient;
    g_pClient = NULL;
  }
};

BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
	switch(fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	}

	return TRUE;
};