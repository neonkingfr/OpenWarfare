
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:
		position2D.h
Purpose:
		This file contains the declaration of the position2D class.
	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			04-04/2011	YFP: Original Implementation

/*****************************************************************************/

#ifndef VBS2FUSION_POSITION_2D_H
#define VBS2FUSION_POSITION_2D_H

/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES
#include <string>
// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBS2Fusion
{
	class VBS2FUSION_API position2D
	{
	public:
		/*! The main constructor for the position2D class. Sets x and y to 0. */
		position2D();

		/*!
		Copy constructor for Position2D object.
		*/
		position2D(const position2D& pos);

		/*! Uses the passed in values to initialize x and y. */
		position2D(double x, double y);

		/*!	Converts the passed in values to doubles and initializes the object with the converted values. */
		position2D(std::string& x, std::string& y);

		/*! The primary destructor for the class. */
		~position2D();

		/*! Sets the x value of the position. */
		void setX(double val);

		/*!	Sets the y value of the position. */
		void setY(double val);

		/*!	Returns the x value of the position. */
		double getX() const;

		/*!	Returns the y value of the position. */
		double getY() const;

		/*!	Returns a string in VBS2 position format. i.e. "[ x , y ]". */
		std::string getVBSPosition2D();

		/*!
		set the position3D _x , _y , _z co-ordinates to minimum value of double.	
		*/
		void setNull();

		/*!
		check if the position2D is null.If it is null then it returns true.
		*/
		bool isNull();

		/*! Equal operator for position2D. */
		bool operator == (const position2D& rhs);

		/*! Not equal operator. */
		bool operator != (const position2D& rhs);

		/*! Assignment operators for position2D.  */

		position2D & operator = (const position2D& rhs);
		position2D & operator += (const position2D& rhs);
		position2D & operator -= (const position2D& rhs);
		position2D & operator *= (double dval);
		position2D & operator /= (double dval);

		/*! overloaded + operator */
		position2D  operator + (const position2D& rhs);

		/*! overloaded - operator */
		position2D  operator - (const position2D& rhs);

		/*! multiplication operator */
		position2D  operator * (double val);

		/*! division operator */
		position2D  operator / (double val);

	private:
		double _x,_y;

	};

}

#endif  //VBS2FUSION_POSITION_2D_H