/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	EnvironmentState.h

Purpose:

	This file contains the declaration of the EnvironmentState class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	2.0			04/11-2009	RMR: Original Implementation
	2.01		01/02-2010	MHA: Checked Comments
	2.02		03/05-2011	SSD: Added	void setSurfaceMoisture(double val);
										double getSurfaceMoisture() const;
										void setMaxRainDensity(double val);
										double getMaxRainDensity() const;
										void setMaxEvaporation(double val);
										double getMaxEvaporation() const;
										void setSurfaceDrainageSpeed(double val);
										double getSurfaceDrainageSpeed() const;
										void setWaterLevel(double val);
										double getWaterLevel();

/************************************************************************/

#ifndef VBS2FUSION_ENVIRONMENT_STATE_H
#define VBS2FUSION_ENVIRONMENT_STATE_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/

namespace VBS2Fusion
{
	typedef int (*Time)[3];	

	class VBS2FUSION_API EnvironmentState
	{
	public:
		/*! 
		The main constructor for the EnvironmentState class. 
		
		Initializes the EnvironmentState variable with the following default values. 

		_dOvercast = 0.3F;
		_dFog = 0.0F;
		_dRain = 0.0F;
		_iYear = 2007;
		_iMonth = 6;
		_iDay = 7;
		_iHours = 8;
		_iMinutes = 0;
		_iSeconds = 0;

		_dTimeElapsed = 0;

		_dSurfaceMoisture = 0.0F;
		_dMaxRainDensity = 60.0F;
		_dMaxEvaporation = 2.0F;
		_dDrainageSpeed = 4.0F;
		_dWaterLevel = 0.0F;



		*/

		EnvironmentState();

		/*!
		Copy constructor for the EnvironmentState class. 
		*/

		EnvironmentState(EnvironmentState& state);

		/*!
		Destructor for the EnvironmentState class. 
		*/
		~EnvironmentState();

		/*!
		Sets the Overcast value.
		*/
		void setOvercast(double val);

		/*!
		Returns the Overcast value.
		*/
		double getOvercast() const;
		
		/*!
		Sets the Fog value.
		*/
		void setFog(double val);

		/*!
		Returns the Fog value.
		*/
		double getFog() const;

		/*!
		Sets the Rain value.
		*/
		void setRain(double val);

		/*!
		Returns the Rain value.
		*/
		double getRain() const;

		/*!
		Sets TimeHours, TimeMinutes and TimeSeconds. 
		*/
		void setTime(int Hours, int Minutes, int Seconds);

		/*!
		Sets TimeHours, TimeMinutes and TimeSeconds. Passed in parameter is
		in Time format. 
		*/
		void setTime(Time time);

		/*!
		Returns the time in Time format. 
		*/
		Time getTime() const;

		/*!
		Sets TimeYear (_iYear). 
		*/
		void setTimeYear(int val);

		/*!
		Returns TimeYear (_iYear). 
		*/
		int getTimeYear() const;

		/*!
		Sets TimeMonth (_iMonth). 
		*/
		void setTimeMonth(int val);
		/*!
		Returns TimeMonth (_iMonth). 
		*/
		int getTimeMonth() const;

		/*!
		Sets TimeDay (_iDay). 
		*/
		void setTimeDay(int val);

		/*!
		Returns TimeDay (_iDay). 
		*/
		int getTimeDay() const;

		/*!
		Sets TimeHours (_iHours). 
		*/
		void setTimeHours(int val);
		
		/*!
		Returns TimeHours (_iHours). 
		*/
		int getTimeHours() const;

		/*!
		Sets TimeMinutes (_iMinutes). 
		*/
		void setTimeMinutes(int val);

		/*!
		Returns TimeMinutes (_iMinutes). 
		*/
		int getTimeMinutes() const;
		
		/*!
		Sets TimeSeconds (_iSeconds). 
		*/
		void setTimeSeconds(int val);

		/*!
		Returns TimeSeconds (_iSeconds). 
		*/
		int getTimeSeconds() const;

		/*!
		Sets _dTimeElapsed.
		*/
		void EnvironmentState::setTimeElapsed(double val);

		/*!
		Returns _dTimeElapsed.
		*/
		double EnvironmentState::getTimeElapsed() const;

		/*!
		Sets the wind vector. 
		*/
		void setWind(vector<float> _vals);

		/*!
		Returns the wind vector. 
		*/
		vector<float> getWind() const;


		/*!
		Sets the current surface moisture of the terrain.
		*/
		void setSurfaceMoisture(double val);
		/*!
		Returns the current surface moisture.
		*/
		double getSurfaceMoisture() const;

		/*!
		Sets the maximal rain density of the terrain.
		*/
		void setMaxRainDensity(double val);
		/*!
		Returns the maximal rain density.
		*/
		double getMaxRainDensity() const;

		/*!
		Sets the maximal evaporation for best weather conditions for the terrain.
		*/
		void setMaxEvaporation(double val);
		/*!
		Returns maximal evaporation for best weather conditions.
		*/
		double getMaxEvaporation() const;


		/*!
		Sets the surface drainage speed of the terrain.
		*/
		void setSurfaceDrainageSpeed(double val);
		/*!
		Returns the surface drainage speed.
		*/
		double getSurfaceDrainageSpeed() const;

		/*!
		Sets (raise or lower) the sea level.
		*/
		void setWaterLevel(double val);
		/*!
		Returns the sea level.
		*/
		double getWaterLevel() const;

	private:
		double _dOvercast;
		double _dFog;
		double _dRain;

		int _iYear;
		int _iMonth;
		int _iDay;
		int _iHours;
		int _iMinutes;
		int _iSeconds;

		double _dTimeElapsed;

		vector<float> _wind;



		double _dSurfaceMoisture;
		double _dMaxRainDensity;
		double _dMaxEvaporation;
		double _dDrainageSpeed;
		double _dWaterLevel;

	};
};

#endif //ENVIRONMENT_STATE_H