/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

ArmedModule.h

Purpose:

This file contains the declaration of the NetworkID class.

Version Information:

Version		Date		Author and Comments
===========================================
2.0			11-12/2009	YFP: Original Implementation
2.01		11-02/2010	MHA: Comments Checked

/************************************************************************/

#ifndef VBS2FUSION_ARMEDMODULE_H
#define VBS2FUSION_ARMEDMODULE_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "data/Turret.h"
#include "data/Magazine.h"
#include "data/Weapon.h"
#include "DataContainers/ObjectList.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/


namespace VBS2Fusion
{

	class VBS2FUSION_API ArmedModule
	{
	public:

		/*!
			Type definition of the list that contains turrets.
		*/
		typedef list<Turret> TurretList;

		/*!
			Iterator for list containing turrets. 
		*/
		typedef list<Turret>::iterator t_iterator;

		/*!
			Constant iterator for turret list. 
		*/
		typedef list<Turret>::const_iterator t_const_iterator;

		/*!
			Type definition of the list that contains weapons.
		*/
		typedef list<Weapon> WeaponList;

		/*!
			Iterator for list that contains weapons.
		*/
		typedef list<Weapon>::iterator w_iterator;

		/*!
			Constant iterator for weapon list. 
		*/
		typedef list<Weapon>::const_iterator w_const_iterator;

		/*!
			Type definition of the list that contains magazines.
		*/
		typedef list<Magazine> MagazineList;

		/*!
			Iterator for list that contains magazines.
		*/
		typedef list<Magazine>::iterator m_iterator;	

		/*!
			Constant iterator for magazine list.
		*/
		typedef list<Magazine>::const_iterator m_const_iterator;

		/*!
			Armed Module constructor. 
		*/
		ArmedModule();

		/*!
			Armed Module Destructor. 
		*/
		~ArmedModule();

		/*!
		Copy constructor for Armed Module
		*/

		ArmedModule(const ArmedModule& aModule);

		/*!
		The assignment operator for Armed Module
		*/
		ArmedModule& operator = (const ArmedModule& aModule);

		/*!
			Begin iterator for unit list. 
		*/
		t_iterator turrets_begin();

		/*!
			Const Begin iterator for unit list. 
		*/
		t_const_iterator turrets_begin() const;

		/*!
			End iterator for unit list. 
		*/
		t_iterator turrets_end();

		/*!
			Const End iterator for unit list. 
		*/
		t_const_iterator turrets_end() const;	

		/*!
			begin iterator for weapon list.
		*/
		w_iterator weapons_begin();

		/*!
			begin const iterator for weapon list.
		*/
		w_const_iterator weapons_begin() const;

		/*!
			end iterator for weapon list.
		*/
		w_iterator weapons_end();

		/*!
			end iterator for weapon list.
		*/
		w_const_iterator weapons_end() const;


		/*!
			Begin iterator for magazine list.
		*/
		m_iterator magazines_begin();

		/*!
			Begin const iterator for magazine list.
		*/
		m_const_iterator magazines_begin() const;

		/*!
			End iterator for magazine list.
		*/
		m_iterator magazines_end();

		/*!
			End const iterator for magazine list.
		*/
		m_const_iterator magazines_end() const;

		/*!
			Add a turret to the armed module.
		*/
		void setTurret(Turret turret);

		/*!
			Get the main turret of the armed module. 
		*/
		Turret getMainTurret() const;

		/*!
			Get the turret by the specified path string.
		*/
		Turret getTurretByPathString(string turretPath);

		/*!
			Get the turret by the specified name. 
		*/
		Turret getTurretByName(string turretName);
		
		/*!
			Clears all turrets in the turret list.
		*/
		void clearTurretList();

		/*!
			Check weather the turret is available. 
		*/
		bool isTurretExists(string turretPath);

		/*!
			Add magazine to the armed module.
		*/
		void addMagazine(Magazine magazine);

		/*!
			Clear magazine list.
		*/
		void clearMagazineList();


	private:

		TurretList _turretList;

		WeaponList _weaponList;

		MagazineList _magazineList;

	};

};

#endif //ARMEDMODULE_H