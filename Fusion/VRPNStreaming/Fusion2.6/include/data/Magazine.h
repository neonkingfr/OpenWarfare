/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

Magazine.h

Purpose:

This file contains the declaration of the Weapon class.

Version Information:

Version		Date		Author and Comments
===========================================
2.0			29-01/2010	YFP: Original Implementation
2.01		11-02/2010	MHA: Comments Checked

/************************************************************************/

#ifndef VBS2FUSION_MAGAZINES_H
#define VBS2FUSION_MAGAZINES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <string>

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/

namespace VBS2Fusion
{

	class VBS2FUSION_API Magazine
	{
	public:
		/*!
			Constructor for Magazine class.
			variables are initialized as follows:
				_strMagName = "";
				_iAmmoCount = 0;
		*/
		Magazine();

		/*!
			Constructor for Magazine class.
			variables are initialized as follows:
				_strMagName = name;
				_iAmmoCount = 0;
		*/
		Magazine(string name);

		/*!
			Constructor for Magazine class.
			variables are initialized as follows:
				_strMagName = name;
				_iAmmoCount = ammoCount;
		*/
		Magazine(string name , int ammoCount);

		/*!
			Main Destructor for Magazine class. 
		*/
		~Magazine();

		/*!
			Copy constructor for magazine class. 
		*/
		Magazine(const Magazine& magazine );

		/*!
			Assignment operator for magazine class.
		*/
		Magazine& operator = (const Magazine& magazine);

		/*!
			Equal operator for magazine class.
		*/
		bool operator == (const Magazine& magazine) const;

		/*!
			Less than operator for magazine class.
		*/
		bool operator < (const Magazine& magazine) const;
		
		/*!
			set magazine name.
		*/
		void setMagName(string name);

		/*!
			get magazine name.
		*/
		string getMagName() const;

		/*!
			set Ammo count.
		*/
		void setAmmoCount(int count);

		/*!
			get ammo count
		*/
		int getAmmoCount() const;


	private:
		string _strMagName;
		int _iAmmoCount;

	};
};

#endif //MAGAZINES_H
