
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/


/*************************************************************************

Name:

	ControllableObjectUtilities.h

Purpose:

	This file contains the declaration of the ControllableObjectUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			25-03/2009	RMR: Original Implementation
	1.01		27-08/2009  SDS: Added updateIsLocal(ControllableObject& co);	
								 Updated updateStaticProperties(co);
								 Updated updateStaticProperties_A(co);

	2.0			10-02/2010  YFP: Version 2 Implementation
	2.01		10-02/2010	MHA: Comments Checked
	2.02		27-04/2010  YFP: Added Methods,
									void Suspend(ControllableObject&, bool);
									void Reactivate(ControllableObject&);
									int getSimulationMode(ControllableObject&);
									void updateSimulationMode(ControllableObject&);
									void applySimulationMode(ControllableObject& , SIMULATION_MODE);
	2.03		25-05/2010  YFP: Added Methods,
									void performAction(ControllableObject&,ControllableObject&,string)
									void limitSpeed(ControllableObject&, double)
									void forceSpeed(ControllableObject&, double)

	2.04		09-06/2010  YFP: Added Methods,
									void applyVelocity(ControllableObject&)
									void applyVelocity(ControllableObject&, double)

	2.05        10-06/2010  YFP: Added Methods,
									string getType(ControllableObject&)
	2.05		02-07/2010  YFP: Added Methods,
									void applyURNCallSign(ControllableObject&);
									string getURNCallSign(ControllableObject&);
									void updateURNCallSign(ControllableObject&);
	2.06		06-10/2010  YFP: Added Methods,
									void createObject(ControllableObject&);
									void createObject(ControllableObject&, string, position3D);
	2.07		10-01/2011  CGS: Modified updateDynamicProperties(ControllableObject& co)
									createObject(ControllableObject& co, string typeName, position3D pos)
									getIDUsingAlias(ControllableObject& co)
									updateWeaponAmmo(ControllableObject& co)
									doRespawn(ControllableObject &co)
									getExternalControl(ControllableObject& co)
									setKeyAction(ControllableObject& co, string keyStroke, float amount)
									setUnitAction(ControllableObject& co,string keyStroke, float amount)

	2.08		11-08-2011	SSD: Added Methods,
									applyPositionASL(ControllableObject& co)
									applyPositionASL(ControllableObject& co, position3D aslPos)
	2.09		20-09/2011 CGS:	Added Methods
									bool isExternally(ControllableObject& co)
									bool canFloat(ControllableObject& co)
									bool isAttached(ControllableObject& co)
									double getArmsTotalWeight(ControllableObject& co)
									bool isTouchingObject(ControllableObject& co)
									vector3D getAngularVelocity(ControllableObject& co)
									vector3D getAcceleration(ControllableObject& co)
									void disableActions(ControllableObject& co, bool action=true)
									bool isActionsDisabled(ControllableObject& co)
									void applyDrawMode(ControllableObject& co, DRAWMODE mode)
									string getDrawMode(ControllableObject& co)
				21-09/2011			bool applyDrawMode(ControllableObject& co, DRAWMODE mode, Color_RGBA color, string lod)
									bool applyDrawMode(ControllableObject& co, DRAWMODE mode, Color_RGB color, double transMin, double transMax, string lod)
									bool applyDisplayText (ControllableObject& co, string message)
									bool applyDisplayText (ControllableObject& co, string message, vector3D offset, bool mode, Color_RGBA color, double size)
									bool applyScale(ControllableObject& co, double width, double height, double length)
									bool applyForce(ControllableObject& co, vector3D force, position3D position)
				22-09/2011			bool isEnabledAI(ControllableObject& co, AIBEHAVIOUR type)
									string getAnimationBone(ControllableObject& co, string animation)
									string getVarName(ControllableObject& co)
									double getDistance(ControllableObject& co, ControllableObject& tco)
				28-09/2011			void applyPositionASLEx(ControllableObject& co, position3D aslPosEx)
									bool canMove(ControllableObject& co)
									bool canFire(ControllableObject& co)
									vector3D getBoundingBoxMinExtremePoints(ControllableObject& co, ControllableObject& tco)
									vector3D getBoundingBoxMaxExtremePoints(ControllableObject& co, ControllableObject& tco)
									bool isHidden(ControllableObject& co)
									double getLength(ControllableObject& co)
									double getWeight(ControllableObject& co)
									double getVolume(ControllableObject& co)

/************************************************************************/

#ifndef VBS2FUSION_CONTROLLABLE_OBJECT_UTILITIES_H
#define VBS2FUSION_CONTROLLABLE_OBJECT_UTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>
#include <math.h>

// SIMCENTRIC INCLUDES
#include "position3D.h"
#include "VBS2Fusion.h"
#include "conversions.h"
#include "data/ControllableObject.h"
#include "util/ExecutionUtilities.h"

/**********************************************************************/
/* END INCLUDES
/**********************************************************************/

namespace VBS2Fusion
{	

	class VBS2FUSION_API ControllableObjectUtilities
	{
	public:

		/*!
			Creates a controllable object.
			- The type of object defined by co.getType() is used.
			- Creates the object at the position defined by co.getPosition().
		*/
		static void createObject(ControllableObject& co);

		/*!
			creates a controllable object.
			- TypeName and position are required.
		*/
		/*!
			creates a controllable object.
			- The type of object is as specified by parameter "typeName".
			- Creates the object at the position specified by parameter "pos".
		*/
		static void createObject(ControllableObject& co, string typeName, position3D pos);


		//-------------------------------------------------------
		// Side utilities
		//-------------------------------------------------------

		/*!
		Get the side of the object specified by co. 
		*/
		static string getSide(ControllableObject& co);

		/*!
		Updates the side of the object specified by co. This function retrieves the 
		side and stores it in the _StringSide field of co using setSide. 
		*/
		static void updateSide(ControllableObject& co);

		//-----------------------------------------------------

		/*!
		return true if the object is created in the running machine.
		*/
		static bool isLocallyOwned(ControllableObject& co);

		/*!
		Updates the locally owned property of the object specified by co. This function retrieves the 
		own status and stores it in the _boolLocallyOwned field of co using setLocallyOwned. 
		*/
		static void updateLocallyOwned(ControllableObject& co);
			
		//------------------------------------------------------

		/*!
		Get the network ID of the object specified by the co.	
		*/
		static NetworkID getNetworkID(ControllableObject& co);

		//-------------------------------------------------------
		// Position utilities
		//-------------------------------------------------------

		/*!
		Get the position of the object specified by co in position3D format.
		This is same as getPosition method.
		*/
		static position3D getPos(ControllableObject& co);

		/*!
		Get the position of the object specified by co in position3D format. 
		*/
		static position3D getPosition(ControllableObject& co);

		/*!
		Updates the position of the object specified by co. This function retrieves the 
		position and stores it in the _position field of co using setPosition. 
		
		*/
		static void updatePosition(ControllableObject& co);

		/*!
		Applies the position specified by co.getPosition() to the object
		within the game environment. 		
		*/
		static void applyPosition(ControllableObject& co);

		/*!
		Applies the position specified by pos to the object
		within the game environment. It does not automatically apply this value to 
		co. Instead, use updatePosition(co) to check whether the repositioning has 
		happened successfully. 
		*/
		static void applyPosition(ControllableObject& co, position3D pos);

		/*!
		Order the object to move to the position specified by pos. The speed of 
		movement can be changed using forceSpeed. 
		*/
		static void moveTo(ControllableObject& co, position3D pos);

		/*!
		Returns a world position (above ground level), relative to an object's origin, 
		and taking into account its direction.
		*/
		static position3D ControllableObjectUtilities::modelToWorld(ControllableObject& co, position3D& pos);

		/*!
		Returns an object's offset from the specified world position, taking into account 
		the object's origin and direction.
		*/
		static position3D ControllableObjectUtilities::worldToModel(ControllableObject& co, position3D& pos);

		/*!
		Returns a world position (above sea level), relative to an object's origin, 
		and taking into account its direction.
		*/
		static position3D ControllableObjectUtilities::modelToWorldASL(ControllableObject& co, position3D& pos);

		/*!
		Returns an object's offset from the specified world position, taking into account 
		the object's origin and direction.
		*/
		static position3D ControllableObjectUtilities::worldToModelASL(ControllableObject& co, position3D& pos);

		//-------------------------------------------------------
		// PositionASL utilities
		//-------------------------------------------------------

		/*!
		Get the position above sea level of the object specified by co in position3D format. 
		*/
		static position3D getPositionASL(ControllableObject& co);

		/*!
		Updates the position above sea level of the object specified by co. This function retrieves the 
		position and stores it in the _positionASL field of co using setPositionASL. 
		
		*/
		static void updatePositionASL(ControllableObject& co);


		//-------------------------------------------------------
		// Speed & Velocity utilities
		//-------------------------------------------------------

		/*!
			Get the speed of the object specified by co.
		*/
		static double getSpeed(ControllableObject& co);

		/*!
			Update speed of the Controllable object specified by co.
		*/
		static void updateSpeed(ControllableObject& co);
		
		//-------------------------------------------------------

		/*!
		Applies the velocity of the controllable Object. 
		*/
		static void applyVelocity(ControllableObject& co);

		/*!
		Applies the velocity of the controllable Object. 
		*/
		static void applyVelocity(ControllableObject& co, vector3D velocity);

		/*!
		Get the velocity of the object specified by co in position3D format. 
		*/

		static vector3D getVelocity(ControllableObject& co);

		/*!
		Updates the velocity of the object specified by co. This function retrieves the 
		position and stores it in the _velocity field of co using setVelocity. 
		 
		*/
		static void updateVelocity(ControllableObject& co);
		

		//-------------------------------------------------------
		// Direction utilities
		//-------------------------------------------------------

		/*!
		Get the direction the object co is facing towards. The value of the direction parameter
		of co is not changed. 

		*/
		static double getDirection(ControllableObject& co);

		/*!
		Updates the direction the object co is facing towards. It updates the _direction 
		parameter of co using setDir. 

		*/
		static void updateDirection(ControllableObject& co);

		/*!
		The direction specified by the getDir method if co is used to set the 
		direction the object is facing towards. 
		*/
		static void applyDirection(ControllableObject& co);

		/*!
		The direction specified by the dir is used to set the 
		direction the object is facing towards. The direction parameter of 
		co is not updated. Use updateDirection to check and see if the direction 
		has been registered and applied. 	
		*/
		static void applyDirection(ControllableObject& co, double dir);

		//-------------------------------------------------------
		// Type utilities
		//-------------------------------------------------------

		/*!
		Returns the VBS2 type of object. 
		*/
		static string getType(ControllableObject& co);

		/*!
		Updates the VBS2 type of co using setType(). 
		*/
		static void updateType(ControllableObject& co);

		//-------------------------------------------------------
		// Player status utilities
		//-------------------------------------------------------

		/*!
		Returns true if co is controlled by the player. False if not. 
		*/
		static bool isPlayer(ControllableObject& co);

		/*!
		Updates the player status of co. 
		*/
		static void updateIsPlayer(ControllableObject& co);

		//-------------------------------------------------------
		// Alive status utilities
		//-------------------------------------------------------

		/*!
		Returns true if co is alive, false it not. 
		*/
		static bool isAlive(ControllableObject& co);

		/*!
		Updates the alive status of co. 
		*/
		static void updateIsAlive(ControllableObject& co);

		/*!
		Uses the getAlive parameter to set the alive status of co. If the unit
		is dead, this function uses setDamage within VBS2 to revive the unit.
		*/
		static void applyIsAlive(ControllableObject& co);

		/*!
		Uses alive to either revive or kill the unit specified by co. 
		*/
		static void applyIsAlive(ControllableObject& co, bool alive);


		//-------------------------------------------------------
		// Damage utilities
		//-------------------------------------------------------

		/*!
		Returns the damage of the unit specified by co.  
		*/
		static double getDamage(ControllableObject& co);

		/*!
		Updates the damage of the unit specified by co.  
		*/
		static void updateDamage(ControllableObject& co);

		/*!
		Applies the damage value specified by co.getDamage()
		to the object specified by co.  
		*/
		static void applyDamage(ControllableObject& co);

		/*!
		Applies the damage value specified by damage
		to the object specified by co. The damage parameter of co is not updated. 
		Use updateDamage to check whether the damage has been updated.
		*/
		static void applyDamage(ControllableObject& co, double damage);

		//-------------------------------------------------------
		// Object Update utilities
		//-------------------------------------------------------
		
		/*!
		Update the properties of co which do not change frequently. 

		i.e. network ID, Type
		*/
		static void updateStaticProperties(ControllableObject& co);

		/*!
		update the properties of co which do change frequently. 

		i.e. Damage, direction, alive, isPlayer, position and positionASL. 
		*/
		static void updateDynamicProperties(ControllableObject& co);


		//-------------------------------------------------------
		// AttachTo controls
		//-------------------------------------------------------

		/*!
		Attaches an object to another object. The offset is applied to the object, memory point.
		Memory point is optional, it specifies location where the attaching should happen (it's name of the memory point in the model)
		*/
		static void attachTo(ControllableObject& object, ControllableObject& tObject, position3D offSet, string memoryPoint);

		/*!
		Attaches an object to another object. The offset is applied to the object center. 
		*/
		static void attachTo(ControllableObject& object, ControllableObject& tObject, position3D offSet);

		/*!
		detach the given object if it is attached to some object. 
		*/
		static void detach(ControllableObject &object);

		//-------------------------------------------------------
		// Object AI utilities
		//-------------------------------------------------------
		
		/*!
		Disable the AI behavior of the controllable object.
		*/
		static void disableAIbehaviour(ControllableObject& co, AIBEHAVIOUR behaviour);

		/*!
		Disable the AI behavior of the controllable object.
		*/
		static void enableAIbehaviour(ControllableObject& co, AIBEHAVIOUR behaviour);


		//-------------------------------------------------------
		// Existence check utilities
		//-------------------------------------------------------

		/*!
		return true if the object specified by co exists in the VBS2 game environment. 	
		*/
		static bool ControllableObjectExists(ControllableObject& co);

		/*!
		Returns the network ID of an object using its alias. 
		*/
		static string getIDUsingAlias(ControllableObject& co);

		/*!
		Returns the name of an object if an alias has already been assigned. 
		*/
		static string getNameUsingAlias(ControllableObject& co);

		/*!
		Returns true if the object is NULL within the VBS2 environment. 
		*/
		static bool isNULL(ControllableObject& co);

		//-------------------------------------------------------
		// Weapons List Utilities 
		//-------------------------------------------------------
		
		/*!
		Returns the name of the object's primary weapon (an empty string if there is none).	
		*/
		static string getPrimaryWeapon(ControllableObject& co);

		/*!
		Updates the primary weapon of the object. 
		*/
		static void updatePrimaryWeapon(ControllableObject& co);

		/*!
		Updates the list of weapon types assigned to the unit. A new 
		WeaponAmmo is structure is created for each recognized weapon type
		and the new structure is added to the weapons list. Each new weapon
		is assigned an ammo value of 0. 

		Note: This function does not update the ammo amounts on
		the weapons magazines. Use updateWeaponAmmo to update the ammo types.
		*/
		static void updateWeaponTypes(ControllableObject& co);

		/*!
		Select the Weapon given by the muzzle Name.
		*/
		static void selectWeapon(ControllableObject& co, string muzzleName);

		/*!
		Updates the amount of ammo left in the primary magazine of each assigned 
		weapon type. Uses the list defined by the weapons list, and therefore will only load ammo
		information for each weapon on that list. Should be used after updateWeaponTypes is called. 	
		*/
		static void updateWeaponAmmo(ControllableObject& co);



		//-------------------------------------------------------
		//Magazine Utilities 
		//-------------------------------------------------------

		/*!
		Add magazine to the controllable object.
		*/
		static void addMagazine(ControllableObject& co, string name);

		/*!
		Count number of magazines available in given controllable object.
		*/
		static int countMagazines(ControllableObject& co);

		//-------------------------------------------------------
		// Magazines List Utilities 
		//-------------------------------------------------------

		/*!
		Updates the list of magazines assigned to a unit. This function loads 
		all magazines assigned to the unit from VBS2 and adds a new MagazineAmmo structure to 
		the magazines list whenever a distinct magazine type is found. When a magazine
		type which is already assigned is found, it increments the 'number' variable for 
		the representative list item to indicate that there are multiple assignments of the same
		magazine. 

		Note: This function does not update the ammo amount list. An empty list is created for each newly 
		created entry on the list. Use updateMagazineAmmo to update this list. 

		This function uses the network ID of the object to access it within the game environment, so please
		ensure that either a registered network ID is assigned to co. 

		*/
		static void updateMagazineTypes(ControllableObject& co);

		/*!
		Used to update the amount of ammo left in each magazine assigned to the object
		and to therefore update the ammo amount list. Uses the magazine types defined in the 
		existing magazine list to search for available ammo, and therefore will
		not load ammo information for any magazines which are not on this list. Should be used after 
		updateMagazineTypes is called. 

		This function uses the network ID of the object to access it within the game environment, so please
		ensure that either a registered network ID is assigned to co. 	
		*/

		static void updateMagazineAmmo(ControllableObject& co);


		//-------------------------------------------------------
		// Respawn function  
		//-------------------------------------------------------

		/*!
		Respawns the given object immediately.  
		When object is respawn, respawn event handler only fires where the respawned unit is local.
		*/
		static void doRespawn(ControllableObject& co);

		/*!
		Respawns the unit. Care should be taken when using this command as VBS2 automatically
		assigns a new name to the respawned unit (this does not happen if the unit is a named unit). 

		This command should therefore be used in conjunction with a respawn event handler to 
		obtain the new name and to assign an alias to the object. If not, respawning can result in
		the loss of the handle to the unit and therefore could cause errors during the update cycle. 	
		*/

		static void RespawnObject(ControllableObject& co);

		//-------------------------------------------------------
		// Turn function
		//-------------------------------------------------------


		/*!
		 Turning the unit. This is different from using the applyDirection command
		 which immediately sets the direction of the unit to the direction specified. The
		 Turn command simulates the object actually turning in a more realistic manner. 
		 The direction is specified in relation to world coordinates (i.e. 0 along positive
		 x axis, 90 along positive z axie etc. )

		 Direction should be specified in degrees. 	
		*/

		static void Turn(ControllableObject& co, double direction);

		/*!
		 Turning the unit. This is different from using the applyDirection command
		 which immediately sets the direction of the unit to the direction specified. The
		 Turn command simulates the object actually turning in a more realistic manner. 
		 The direction is specified in relation to the objects current direction.

		 Direction should be specified in degrees. 
		*/

		static void TurnRelative(ControllableObject& co, double direction);


		//************************************************************
		// Movement controls
		//************************************************************

		/*!
		The moveForward function is a special function to use when a programmer want to control the movement of 
		a unit. This command should be used in conjunction with the get/set functions for MoveDir
		and Speed. A single call to this method will order the unit to move in a straight line towards the
		direction pointed to by getMoveDir() at the speed specified by getSpeed(). Use setMoveDir() to control the direction
		of movement and setSpeed() to control the speed of movement. This command can be called continuously 
		within the OnSimulationStep loop to control and manipulate the movement of the unit. Set speed to 0 to stop 
		the unit from moving. 

		The unit will keep walking forward until an obstacle is reached. When an obstacle is reached, the unit will stop. 

		Is a crude method for controlling direction and speed (i.e. steering) of an object. 

		NOTE: this function will not work if used on the player unit. Call periodically instead of at every
		OnSimulationStep call as the unit walks 20 meters ahead at each call of this function. A call every 2-3 seconds 
		will provide the same functionality as a call at every OnSimulation step and will also improve performance.

		You should however make a call to this function soon after making any changes to the moveDirection
		or speed properties of an object for them to register immediately. 

		Disable 'PATHPLAN' AI for the unit if you do not want it to perform path planning and stop at obstacles. 
		*/
		static void moveForward(ControllableObject& co);



		//-------------------------------------------------------
		// NetworkId utility
		//-------------------------------------------------------
		
		/*!
		Update "isLocal" property in co to know whether co has valid
		network id or not		
		*/
		static void updateIsLocal(ControllableObject& co);		


		//-------------------------------------------------------
		// Camera Mode utilities
		//-------------------------------------------------------
		/*!
		Applies the camera mode specified in camMode to the camera/Object. 
		*/
		static void applyCamMode(ControllableObject& c, CAMMODETYPE camMode);


		/*! 
			Get the camera mode string.
			Moved to the VBS2FusionDefinitions.h
		*/
		static string CamModeToString(CAMMODETYPE camMode);

		/*!
			Check whether the object movement is completed.   
		*/
		static bool isMoveToCompleted(ControllableObject& co);

		/*!
		Set object to be control externally.	
		*/
		static void setExternalControl(ControllableObject& co, bool control);

		/*!
		Checks whether an object is externally controlled. 
		*/
		static bool getExternalControl(ControllableObject& co);

		/*!
		Simulate user input. Amount should be 0 to 1 for digital actions, -1 to 1 for analog.
		*/
		static void setKeyAction(ControllableObject& co,string keyStroke, float amount);

		/*!
		Simulate user input for specified object. Amount should be 0 to 1 for digital actions, -1 to 1 for analog.
		This method works for externally controlled objects.
		*/
		static void setUnitAction(ControllableObject& co,string keyStroke, float amount);

		/*!
		Suspend the simulation of the object. 
		If collidable is true,then object can collide else object is disappeared form VBS2. 
		*/
		static void Suspend(ControllableObject& co , bool collidable = true );

		/*!
		Reactivate a suspended object to its normal status.
		*/
		static void Reactivate(ControllableObject& co);

		/*!
		Returns simulation mode of the object.
		*/
		static int getSimulationMode(ControllableObject& co);

		/*!
		Update simulation mode of the object.
		*/
		static void updateSimulationMode(ControllableObject& co);

		/*!
		Apply simulation mode of the object.
		
		SIM_NORMAL => [Normal  - 0 ] Object is simulated and drawn.
		SIM_FROZEN => [Frozen  - 1 ] Object is NOT simulated, but is still drawn and still can collide.
		SIM_HIDDEN => [Hidden  - 2 ] Object is not simulated and not drawn. Object can't collide with 
					  others. 
		*/
		static void applySimulationMode(ControllableObject& co , SIMULATION_MODE simmode);

		/*!
		Perform specified action by the controllable object. 
		- if the action is done by the object itself, same object should be passed as "co" and "targetCo"
		- if the action is done by or on a different object, objects should be passed accordingly.
		*/
		static void performAction(ControllableObject& co, string type, ControllableObject& targetCo);

		/*!
		Perform specified action by the controllable object. 
		- if the action is done by the object itself, same object should be passed as "co" and "targetCo"
		- if the action is done by or on a different object, objects should be passed accordingly.

		Use this function if the action is defined in the form of [type, param1, param2, param3]
		*/
		static void performAction(ControllableObject& co,list<string> strpara, ControllableObject& targetCo );

		/*!
		Limit speed of a object to a defined speed value.
		*/
		static void limitSpeed(ControllableObject& co , double speedValue);

		/*!
		Forces the speed of the unit. object will never attempt to move faster than given by forceSpeed.
		*/
		static void forceSpeed(ControllableObject& co, double forceSpeedVal);

		/*!
		Applies the alias of the object. if the applied alias has not 
		already been assigned to another object in the mission.
		*/
		static void applyAlias(ControllableObject& co);

		/*!
		Applies URN call sign of the object.
		*/
		static void applyURNCallSign(ControllableObject& co);

		/*!
		Returns URN call sign of the object.
		*/
		static string getURNCallSign(ControllableObject& co);

		/*!
		update URN call sign of the object.
		*/
		static void updateURNCallSign(ControllableObject& co);

		/*!
		Allow or prevent an object being damaged(injured or killed). 
		*/
		static void allowDamage(ControllableObject& co , bool allow);

		/*!
		Deletes specified controllable Object. Only objects created during the game can be deleted.
		*/
		static void deleteObject(ControllableObject& co);
		
		/*!
		Turns different geometry settings on and off.
		A true argument turns the specific setting OFF
		*/
		static void disableGeo(ControllableObject& co, bool visible, bool fire, bool view, bool collision);


		/*!
		Applies the position specified by co.getPositionASL() to the object
		within the game environment. 		
		*/
		static void applyPositionASL(ControllableObject& co);

		/*!
		Applies the position specified by aslPos to the object
		within the game environment as the objects ASL position.
		*/
		static void applyPositionASL(ControllableObject& co, position3D aslPos);

		/*!
		Tells whether object can float or not. Returns true if the object can float.
		*/
		static bool canFloat(ControllableObject& co);

		/*!
		Tells whether object got attached with other object. Returns true if object is attached to another one.
		*/
		static bool isAttached(ControllableObject& co);

		/*!
		Returns the total weight of weapons & magazines carried by a object.
		*/
		static double getArmsTotalWeight(ControllableObject& co);

		/*!
		Tells whether an object is currently touching another object.
		*/
		static bool isTouchingObject(ControllableObject& co);

		/*!
		Returns the angular velocity of the object.
		*/
		static vector3D getAngularVelocity(ControllableObject& co);

		/*!
		Returns the acceleration of the object .
		*/
		static vector3D getAcceleration(ControllableObject& co);

		/*!
		Disables or enables user actions for a specific object. Default value is set as true. It will disable 
		the user actions on to that object. Users can't interact with objects that have actions disabled.
		*/
		static void disableActions(ControllableObject& co, bool action=true);

		/*!
		Checks whether user actions for a specific object were disabled.
		*/
		static bool isActionsDisabled(ControllableObject& co);

		/*!
		Apply the draw mode for the object created in VBS. 
		*/
		static void applyDrawMode(ControllableObject& co, DRAWMODE mode);

		/*!
		Apply the draw mode to the object created in VBS according to the parameters passed. 
		*/
		static void applyDrawMode(ControllableObject& co, DRAWMODE mode, Color_RGBA color, string lod);

		/*!
		Apply the draw mode to the object created in VBS according to the parameters passed.
		*/
		static void applyDrawMode(ControllableObject& co, DRAWMODE mode, Color_RGB color, double transMin, double transMax, string lod);

		/*!
		Returns the draw mode of an object.
		*/
		static string getDrawMode(ControllableObject& co);

		/*!
		Displays message that is attached to object. 
		*/
		static void applyDisplayText (ControllableObject& co, string message);

		/*!
		Displays message that is attached to object. 
		*/
		static void applyDisplayText (ControllableObject& co, string message, vector3D offset, bool mode, Color_RGBA color, double size);

		/*!
		Scales the size of an object, in all three axis. For most objects the scale will reset itself at each game cycle.
		*/
		static void applyScale(ControllableObject& co, double width, double height, double length);


		/*!
		Applies force to object. Force vector is in global coordinates and magnitude of force corresponds to Newtons. 
		Position to which force is applied is in model coordinates. Only applicable on physix objects. 
		*/
		static void applyForce(ControllableObject& co, vector3D force, position3D position);


		/*!
		Returns whether certain AI skills are enabled or disabled. 
		*/
		static bool isEnabledAI(ControllableObject& co, AIBEHAVIOUR type);

		/*!
		Returns the name of the variable which contains a primary editor reference to this object.
		*/
		static string getVarName(ControllableObject& co);

		/*!
		Returns the distance in meters between two objects. 
		*/
		static double getDistance(ControllableObject& co, ControllableObject& tco);
		
		/*!
		Sets the object position above sea level. 
		*/
		static void applyPositionASLEx(ControllableObject& co, position3D aslPosEx);
		/*!
		Returns if the given object is still able to move. This command checks only the damage value, 
		not the amount of fuel. 
		*/
		static bool canMove(ControllableObject& co);

		/*!
		Returns if the given object is still able to fire. This command checks only the damage value, not the ammo. 
		*/
		static bool canFire(ControllableObject& co);

		/*!
		Returns a given object's bounding box's minimum extreme points in model coordinate space.
		*/
		static vector3D getBoundingBoxMinExtremePoints(ControllableObject& co, ControllableObject& tco);

		/*!
		Returns a given object's bounding box's maximum extreme points in model coordinate space. 
		*/
		static vector3D getBoundingBoxMaxExtremePoints(ControllableObject& co, ControllableObject& tco);

		/*!
		Return whether the object is hidden (reached the hiding position).
		*/
		static bool isHidden(ControllableObject& co);

		/*!
		Return approximate length of the object.
		*/
		static double getLength(ControllableObject& co);

		/*!
		Returns the defined weight of the object.
		*/
		static double getWeight(ControllableObject& co);

		/*!
		Returns the volume of the object in m�.
		*/
		static double getVolume(ControllableObject& co);

	};
};

#endif //VBS2FUSION_CONTROLLABLE_OBJECT_UTILITIES_H