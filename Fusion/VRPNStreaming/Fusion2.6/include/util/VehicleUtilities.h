/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

VehicleUtilities.h

Purpose:

This file contains the declaration of the VehicleUtilities class.

Version Information:

Version		Date		Author and Comments
===========================================
1.0			02-04/2009	RMR: Original Implementation

2.0			10-02/2010  YFP: Added	void animateVehicle( Vehicle& vehicle ,string animationName,double phase)
									void createVehicleLocal(Vehicle& vehicle)
									Unit getEffectiveCommander(Vehicle& vehicle)
									void animateVehicle( Vehicle& vehicle ,string animationName,double phase)
2.01		10-02/2010	MHA: Comments Checked
2.02		14-06/2010  YFP: Added void createVehicle(Vehicle& , string, string)

2.03		02-07/2010  YFP: Methods added,
									void setManualControl(Vehicle&,bool)
									void setTurnWanted(Vehicle&,double)
									void setThrustWanted(Vehicle&,double)
2.04		05-07/2010  YFP: Methods added,
									void startEngine(Vehicle&,bool);
2.05		10-01/2011  CGS: Methods added getVehicleGroup(Vehicle& vehicle)
							Modified All createVehicle
								createAndAddWaypoint(Vehicle& vehicle, Waypoint& wp)
2.06		14-09/2011	NDB: Methods Added,
								vector<CrewPos> getCrewPosition(Vehicle& vehicle)
								vector<string> getMuzzles(Vehicle& vehicle, Turret::TurretPath& turretPath)
								bool isEngineDisabled(Vehicle& vehicle)
								void applyEngineDisable(Vehicle& vehicle, bool state)
								void applyTowParent(Vehicle& vehicle1, Vehicle& vehicle2)
								Vehicle getTowParent(Vehicle& vehicle)
								void applyBlockSeat(Vehicle& vehicle, VEHICLEASSIGNMENTTYPE type, int index = 0, bool isblock = true)
								void applyBlockSeat(Vehicle& vehicle, vector<int> index, bool isblock = true)
								vector<BLOCKEDSEAT> getBlockedSeats(Vehicle& vehicle)
								int getCargoIndex(Vehicle& vehicle, Unit& unit)
								Unit getUnitAtCargoIndex(Vehicle& vehicle, int index)
								void applyTurretLockOn(Vehicle& vehicle, Turret::TurretPath turretPath, position3D& position,bool trackHidden = false)
								position3D getTurretLockedOn(Vehicle& vehicle, Turret::TurretPath turretPath)
								double getLaserRange(Vehicle& vehicle, Unit& gunner)
								void applyLaserRange(Vehicle& vehicle, double range)
								void applyLaserRange(Vehicle& vehicle, Unit& gunner, double range)
								int getLasingStatus(Vehicle& vehicle, Unit& gunner)
								void applyOpticsOffset(Vehicle& vehicle, Turret::TurretPath turretPath, double azimuth, double elevation, bool transition = true)
								vector<double> getOpticsOffset(Vehicle& vehicle, Unit& gunner)
								vector<double> getOpticsOffset(Vehicle& vehicle, Turret::TurretPath turretPath)
								void applyIndicators(Vehicle& vehicle, bool left, bool hazard, bool right)
								void applyMaxFordingDepth(Vehicle& vehicle, double depth)
								double getMaxFordingDepth(Vehicle& vehicle)
								Unit getTurretUnit(Vehicle& vehicle, Turret::TurretPath turretPath)
2.07		19-09/2011	NDB: Added Methods
								void applyThrustLeftWanted(Vehicle& vehicle, double factor)
								void applyThrustRightWanted(Vehicle& vehicle, double factor)
								void applyFireArc(Vehicle& vehicle, Turret::TurretPath& turretPath, vector3D& direction, double sideRange, double verticalRange, bool relative)
								void applyCommanderOverride(Vehicle& vehicle, Unit& unit, Turret::TurretPath turretPath)
								void land(Vehicle& vehicle, LANDMODE mode)
								void sendToVehicleRadio(Vehicle& vehicle, string name)
								void vehicleChat(Vehicle& vehicle, string message)
								bool isEngineOn(Vehicle& vehicle)
								void commandGetOut(Unit& unit)
								void doGetOut(Unit& unit)
								void applyVehicleId(Vehicle& vehicle, int id)
								void respawnVehicle(Vehicle& vehicle, double delay = -1, int count = 0)
								bool isTurnedOut(Unit& unit)
								void autoAssignVehicle(Unit& unit)
2.08		26-09/2011	NDB: Added Methods
								void assignAsCommander(Vehicle& vehicle, Unit& unit, double delay)
								void assignAsDriver(Vehicle& vehicle, Unit& unit, double delay)
								void assignAsGunner(Vehicle& vehicle, Unit& unit, double delay)
								void assignAsCargo(Vehicle& vehicle, Unit& unit, double delay)
								void orderGetIn(vector<Unit> unitVec, bool order)
								void orderGetIn(Unit& unit, bool order)
								vector<WEAPONCARGO> getWeaponFromCargo(Vehicle& vehicle)
								vector<MAGAZINECARGO> getMagazineFromCargo(Vehicle& vehicle)
								vector<string> getMuzzles(Vehicle& vehicle)
								position3D getAimingPosition(Vehicle& vehicle, ControllableObject& target)
2.09		26-09/2011	NDB:	Added Method void commandMove(Vehicle& vehicle, position3D position)
2.10		04-11/2011	NDB: Added Mehthods
								bool isKindOf(Vehicle& vehicle, string typeName)
								float knowsAbout(Vehicle& vehicle, ControllableObject& target)
2.11		08-11-2011	SSD		void applyName(Vehicle& vehicle)


/************************************************************************/ 

#ifndef VBS2FUSION_VEHICLE_UTILITIES_H
#define VBS2FUSION_VEHICLE_UTILITIES_H
/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>

// SIMCENTRIC INCLUDES
#include "position3D.h"
#include "VBS2Fusion.h"
#include "data/Vehicle.h"
#include "data/Unit.h"
#include "util/ExecutionUtilities.h"
#include "VBS2FusionAppContext.h"
#include "WaypointUtilities.h"
#include "ControllableObjectUtilities.h"
#include "data/vbs2Types.h"
#include "data/VehicleArmedModule.h"

/**********************************************************************/
/* END INCLUDES
/**********************************************************************/


namespace VBS2Fusion
{
	struct CrewPos
	{
		string type;
		position3D pos;
		int cargoID;
		Unit unit;
		Turret::TurretPath turret;
	};

	class VBS2FUSION_API VehicleUtilities : public ControllableObjectUtilities
	{
	public:	

		//********************************************************
		// Load utilities
		//********************************************************

		/*!
		Loads all units belonging to vehicle onto the vehicleList. Random
		aliases are assigned to each loaded unit. The network ID and name of 
		each unit is also loaded along with its role.  
		*/
		static void loadUnits(Vehicle& vehicle);		

		/*!
		Loads all Waypoints belonging to vehicle onto the waypointList. Random aliases
		are assigned to each waypoint. 
		*/
		static void loadWaypoints(Vehicle& vehicle);		

		////**************************************************************		

		/*!
		Loads all units and Waypoints belonging to the vehicle. 	
		*/
		static void loadVehicleUnitsAndWaypoints(Vehicle& vehicle);		

		/*!
		Updates the following:
		- The vehicle members list
		- The waypoint list. 
		- position
		- direction
		- isAlive
		- group name
		- damage
		- fuel
		- type
		- positionASL
		- the current waypoint. 

		This function uses the network ID of the object to access it within the game environment, so please
		ensure that either a registered alias is assigned to vehicle. 
		*/
		static void updateVehicle(Vehicle& vehicle);

		
		//********************************************************
		// position utilities
		//********************************************************


		/*!
		Returns the position of the vehicle in position3D format. 
		*/
		static position3D getPosition(Vehicle& object);

		/*!
		Updates the position of the vehicle.  
		*/
		static void updatePosition(Vehicle& vehicle);

		/*!
		Applies the position defined by position to the vehicle. Value is 
		not updated on the vehicle variable. Use update to verify that changes have been made. 
		*/
		static void applyPosition(Vehicle& vehicle, position3D position);

		/*!
		Applies the position defined by vehicle.getPosition() to the vehicle. 
		*/
		static void applyPosition(Vehicle& vehicle);


		//********************************************************
		// Direction utilities
		//********************************************************

		/*!
		Returns the direction of the vehicle. 
		*/
		static double getDirection(Vehicle& vehicle);

		/*!
		Updates the direction of the vehicle. 
		*/
		static void updateDirection(Vehicle& unit);

		/*!
		Applies the direction defined by direction to the vehicle. Value is 
		not updated on the vehicle variable. Use update to verify that changes have been made. 
		*/
		static void applyDirection(Vehicle& vehicle, double direction);

		/*!
		Applies the direction defined by vehicle.getDir() to the vehicle. 
		*/
		static void applyDirection(Vehicle& vehicle);


	
		//********************************************************
		// Group utilities
		//********************************************************	


		static Group getVehicleGroup(Vehicle& vehicle);

		/*!
		Returns the name of the group the vehicle belongs to. 
		*/
		static string getGroup(Vehicle& vehicle);		

		/*!
		Updates the name of the group the vehicle belongs to. 
		*/
		static void updateGroup(Vehicle& unit);


		//********************************************************
		// Vehicle alive utilities
		//********************************************************	
	
		/*!
		Updates the alive status of the vehicle. 
		*/
		static void updateAlive(Vehicle& unit);


		//********************************************************
		// Damage utilities
		//********************************************************	
			
		/*!
		Returns the current damage level of the vehicle. 
		*/
		static double getDamage(Vehicle& vehicle);

		/*!
		Updates the current damage level of the vehicle. 
		*/
		static void updateDamage(Vehicle& unit);

		/*!
		Applies the damage level specified by damage to the vehicle. Value is 
		not updated on the vehicle variable. Use update to verify that changes have been made. 
		*/
		static void applyDamage(Vehicle& vehicle, double damage);

		/*!
		Applies the damage level specified by vehicle.getDamage() to the vehicle. 
		*/
		static void applyDamage(Vehicle& vehicle);

	

		//********************************************************
		// Fuel utilities
		//********************************************************		

		/*!
		Returns the current fuel level of the vehicle. 
		*/
		static double getFuel(Vehicle& vehicle);

		/*!
		Updates the current fuel level of the vehicle. 
		*/
		static void updateFuel(Vehicle& unit);

		/*!
		Applies the fuel level specified by fuel to the vehicle. Value is 
		not updated on the vehicle variable. Use update to verify that changes have been made. 
		*/
		static void applyFuel(Vehicle& vehicle, double fuel);		

		/*!
		Applies the fuel level specified by vehicle.getFuel() to the vehicle. 
		*/
		static void applyFuel(Vehicle& vehicle);	

		//********************************************************
		// Waypoint utilities
		//********************************************************		

		/*!
		Creates a new waypoint (i.e. creates it in the game environment) and
		adds it to the vehicle. 
		*/
		static void createAndAddWaypoint(Vehicle& vehicle, Waypoint& wp);

		/*!
		Returns the index of the current waypoint. 
		*/
		static int getCurrentWaypoint(Vehicle& vehicle);			

		/*!
		Updates the index of the current waypoint. 
		*/
		static void updateCurrentWaypoint(Vehicle& vehicle);


		//********************************************************
		// Vehicle create utilities
		//********************************************************	

		/*!
		Creates a new vehicle as defined by the parameters present in vehicle. 

		- If no alias is set, a random alias is assigned. 
		- The type of vehicle defined by vehicle.getType() is used.
		- creates the vehicle at the position defined by vehicle.getPosition(). 
		- If no group is specified, a new group is created and the vehicle is added to that group. 
		*/
		static void createVehicle(Vehicle& vehicle);

		/*!
		Create a new vehicle as defined by parameters of the vehicle. 
		If the string of markers array contains several marker names, the position of a random one is used.
		Otherwise, the given position is used. The vehicle is placed inside a circle with this position 
		as center and placement as its radius. 
		Special properties can be:"NONE", "FLY" and "FORM".
		*/
		static void createVehicle(Vehicle& vehicle, string markerNames, string specialProperties);

		/*!
		Creates a new vehicle as defined by the parameters present in the vehicle locally.
		*/
		static void createVehicleLocal(Vehicle& vehicle);
		/*!
		Creates a new air plane as defined by the parameters present in vehicle. 

		- If no alias is set, a random alias is assigned. 		
		- creates the vehicle at the position defined by vehicle.getPosition(). 
		- If no group is specified, a new group is created and the vehicle is added to that group. 
		*/
		//static void createAirPlane(Vehicle& vehicle, vbs2Type::Airplane::AIRPLANE planeIndex);	

		/*!
		Creates a new Car as defined by the parameters present in vehicle. 

		- If no alias is set, a random alias is assigned. 		
		- creates the vehicle at the position defined by vehicle.getPosition(). 
		- If no group is specified, a new group is created and the vehicle is added to that group. 
		*/
		//static void createCar(Vehicle& vehicle, vbs2Type::Car::CAR carIndex);	


		/*!
		Creates a new Helicopter as defined by the parameters present in vehicle. 

		- If no alias is set, a random alias is assigned. 		
		- creates the vehicle at the position defined by vehicle.getPosition(). 
		- If no group is specified, a new group is created and the vehicle is added to that group. 
		*/
		//static void createHelicopter(Vehicle& vehicle, vbs2Type::Helicopter::HELICOPTER helicopterIndex);	


		/*!
		Creates a new Tank as defined by the parameters present in vehicle. 

		- If no alias is set, a random alias is assigned. 		
		- creates the vehicle at the position defined by vehicle.getPosition(). 
		- If no group is specified, a new group is created and the vehicle is added to that group. 
		*/
		//static void createTank(Vehicle& vehicle, vbs2Type::Tank::TANK tankIndex);	

		/*!
		Creates a new Vasi as defined by the parameters present in vehicle. 

		- If no alias is set, a random alias is assigned. 		
		- creates the vehicle at the position defined by vehicle.getPosition(). 
		- If no group is specified, a new group is created and the vehicle is added to that group. 
		*/
		//static void createVasi(Vehicle& vehicle, vbs2Type::Vasi::VASI vasiIndex);	

		//********************************************************
		// Vehicle delete utilities
		//********************************************************	

		/*!
		deletes the vehicle as defined by the parameters of vehicle. 
		*/
		static void deleteVehicle(Vehicle& vehicle);

		//********************************************************
		// Vehicle group utilities
		//********************************************************	

		/*!
		Adds vehicle to the group defined by group. The group name of vehicle is changed
		to vehicle.setGroup(). 
		*/
		static void addToGroup(Vehicle& vehicle, Group& group);		


		/*!
		returns the commander of the group. 
		*/
		static Unit getEffectiveCommander(Vehicle& vehicle);

		//********************************************************
		// Vehicle move in utilities
		//********************************************************

		/*!
		Creates a new unit as defined by unit and moves the unit
		into vehicle as the driver. 		
		*/
		static void createAndMoveInAndAsDriver(Vehicle& vehicle, Unit& unit);

		/*!
		Moves in an already created unit into vehicle as driver. 

		This function uses the network ID of the object to access it within the game environment, so please
		ensure that either a registered alias is assigned to vehicle. 
		*/
		static void moveInAndAsDriver(Vehicle& vehicle, Unit& unit);	

		/*!
		Creates a new unit as defined by unit and moves the unit
		into vehicle as turret.

		It takes the default turret
		[1,0]: second sub-turret of first main turret. 
		*/
		static void createAndMoveInAndAsTurret(Vehicle& vehicle, Unit& unit);

		
		/*!
		Moves in an already created unit into vehicle as turret.

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle.

		It take default turret
		[0,1]: second sub-turret of first main turret.
		*/
		static void moveInAndAsTurret(Vehicle& vehicle, Unit& unit);

		/*!
		Creates a new unit as defined by unit and moves the unit
		into vehicle as turret.

		Available turrets can be determined by the getCrewPos command, and is defined as an array, containing either one or two positions definitions, 
		e.g.
		[0]: first turret (no sub-turrets exist).
		[0,0]: first sub-turret of first main turret.
		[1,0]: second sub-turret of first main turret. 
		*/
		static void createAndMoveInAndAsTurret(Vehicle& vehicle, Unit& unit, string turrntPath);

		
		/*!
		Creates a new unit as defined by unit and moves the unit
		into vehicle as turret.

		Available turrets can be determined by the getCrewPos command, and is defined as an array, containing either one or two positions definitions, 
		e.g.
		[0]: first turret (no sub-turrets exist).
		[0,0]: first sub-turret of first main turret.
		[1,0]: second sub-turret of first main turret. 
		*/
		static void createAndMoveInAndAsTurret(Vehicle& vehicle, Unit& unit, Turret turretPath);

		/*!
		Moves in an already created unit into vehicle as turret.

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle.

		Available turrets can be determined by the getCrewPos command, and is defined as an array, containing either one or two positions definitions, 
		e.g.
		[0]: first turret (no sub-turrets exist).
		[0,0]: first sub-turret of first main turret.
		[1,0]: second sub-turret of first main turret.
		*/
		static void moveInAndAsTurret(Vehicle& vehicle, Unit& unit, string turretPath);

		/*!
		Moves in an already created unit into vehicle as turret.

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle.

		Available turrets can be determined by the getCrewPos command, and is defined as an array, containing either one or two positions definitions, 
		e.g.
		[0]: first turret (no sub-turrets exist).
		[0,0]: first sub-turret of first main turret.
		[1,0]: second sub-turret of first main turret.
		*/
		static void moveInAndAsTurret(Vehicle& vehicle, Unit& unit, Turret turretPath);

		/*!
		Creates a new unit as defined by unit and moves the unit
		into vehicle as cargo. 		
		*/
		static void createAndMoveInAndAsCargo(Vehicle& vehicle, Unit& unit);

		/*!
		Moves in an already created unit into vehicle as cargo. 

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 
		*/
		static void moveInAndAsCargo(Vehicle& vehicle, Unit& unit);		

		/*!
		Creates a new unit as defined by unit and moves the unit
		into vehicle as the commander. 
		*/
		static void createAndMoveInAndAsCommander(Vehicle& vehicle, Unit& unit);

		/*!
		Moves in an already created unit into vehicle as commander. 

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 		
		*/
		static void moveInAndAsCommander(Vehicle& vehicle, Unit& unit);

		/*!
		Creates a new unit as defined by unit and moves the unit
		into vehicle as gunner. 
		*/
		static void createAndMoveInAndAsGunner(Vehicle& vehicle, Unit& unit);

		/*!
		Moves in an already created unit into vehicle as gunner. 

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 
		*/
		static void moveInAndAsGunner(Vehicle& vehicle, Unit& unit);



		//********************************************************
		// Vehicle leave utilities
		//********************************************************

		/*!
		Orders unit to leave vehicle. 

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 
		*/
		static void leaveVehicle(Vehicle& vehicle, Unit& unit);


		/*!
		Orders unit with ID defined by unit.getID() to leave vehicle.

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 
		*/
		static void leaveVehicleByID(Vehicle& vehicle, Unit& unit); 

		/*!
		Orders unit with Name defined by unit.getName() to leave vehicle.

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 
		*/
		static void leaveVehicleByName(Vehicle& vehicle, Unit& unit); 


		/*!
		Orders unit with Alias defined by unit.getAlias() to leave vehicle.

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 
		*/
		static void leaveVehicleByAlias(Vehicle& vehicle, Unit& unit);		

		//*******************************************************/

		/*!
		Activate and deactivate the engine of the vehicle. If state is true, 
		engine will be on and it will stop when state is false.
		*/
		static void startEngine(Vehicle& vehicle , bool state);

		/*!
		Enable/ disable manual control of the vehicle.
		if control is TRUE  vehicle AI disabled and it can be controlled 
		by setTurnWanted & setThrustWanted functions.  
		*/
		static void setManualControl(Vehicle& vehicle , bool control);

		/*!
		set the turn wanted for the vehicle specified by the factor value.
		manual control of the vehicle should be enabled first. 
		*/
		static void setTurnWanted(Vehicle& vehicle, double factor);

		/*!
		set the thrust wanted for the vehicle to passed in. 
		manual control of the vehicle should be enabled first.
		*/
		static void setThrustWanted(Vehicle& vehicle , double factor);

		//********************************************************
		// Animate utilities
		//********************************************************

		/*!
		Animate Vehicle. 
		animationName - The name of the animation
		phase - the phase value is between 0 and 1.  

		*/
		static void animateVehicle( Vehicle& vehicle ,string animationName,double phase);

		/*!
		Returns current gear of the vehicle. 
		*/
		static int getCurrentGear(Vehicle& vehicle);


		/*!
		Returns current RPM of the vehicle. 
		*/
		static float getCurrentRPM(Vehicle& vehicle);
		
		/*!
		Returns engine strength coefficient of the vehicle.
		*/
		static double getEngineStrengthCoefficient(Vehicle& vehicle);

		/*!
		Applies vehicle engine strength coefficient.
		*/
		static void applyEngineStrengthCoefficient(Vehicle& vehicle);

		/*!
		Applies specified engine strength coefficient value to the vehicle.    
		*/
		static void applyEngineStrengthCoefficient(Vehicle& vehicle , double strengthCoeff);

		/*!
		Returns maximum speed limit of the vehicle.
		*/
		static double getMaximumSpeedLimit(Vehicle& vehicle);

		/*!
		Set vehicle's maximum speed. Maximum speed limit should be in Km/h . 
		If maximum speed limit is less than 0. Then the vehicle's maximum speed limit 
		sets to its default value.
		*/
		static void applyMaximumSpeedLimit(Vehicle& vehicle , double maxSpeedLimit);

		/*!
		Returns the name of the Vehicle's primary weapon (an empty string if there is none).	
		*/
		static string getPrimaryWeapon(Vehicle& vehicle);

		/*!
		Updates the primary weapon of the Vehicle. 
		*/
		static void updatePrimaryWeapon(Vehicle& vehicle);

		/*
		Updates the list of weapon types assigned to the Vehicle. 
		Each weapon is assigned with ammo value of 0. 

		Note: This function does not update the ammo amounts on
		the weapons magazines. Use updateWeaponAmmo to update the weapon types with ammo.
		*/
		static void updateWeaponTypes(Vehicle& vehicle);

		/*!
		Select the Weapon given by the muzzle Name.
		*/
		static void selectWeapon(Vehicle& vehicle, string muzzleName);

		/*
		Updates the amount of ammo left in the primary magazine of each assigned 
		weapon type. Uses the list defined by the weapons list within VehicleArmedModule, 
		and therefore will only load ammo information for each weapon on that list. 
		Should be used after updateWeaponTypes is called. 	
		*/
		static void updateWeaponAmmo(Vehicle& vehicle);

		/*!
		Add magazine to the Vehicle.
		*/
		static void addMagazine(Vehicle& vehicle, string name);

		/*!
		Count number of magazines available in given Vehicle object.
		*/
		static int countMagazines(Vehicle& vehicle);

		/*!
		Updates the list of magazines assigned to a vehicle's Armed Module. This function loads all 
		magazines assigned to the vehicle from VBS2 and adds a new Magazine object to Magazine list

		Note: This function does not update the ammo amount within Magazine list. 
		So within MagazineList ammo count would be zero.Use updateMagazineAmmo to update this list. 
		*/
		static void updateMagazineTypes(Vehicle& vehicle);

		/*!
		Updates the magazine ammo count & magazine name inside the vehicleArmedModule's MagazineList
		therefore update the magazine list with required details. This function is enough to update
		MagazineList correctly. No need to use updateMagazineTypes function before using this function.
		*/
		static void updateMagazineAmmo(Vehicle& vehicle);

		/*!
		Load Turrets available in the Vehicle to armed module. 
		*/
		static void updateTurrets(Vehicle& vehicle);

		//----------------------------------------------------------------------------

		/*!
		Get the elevation of the weapon in specified turret of the Armed Vehicle from VBS2 Environment.
		turret object should have the correct turret path.
		*/
		static double getElevation(Vehicle& vehicle, Turret& turret);

		/*! 
		Get azimuth of the weapon in specified turret of the armed vehicle from VBS2 Environment.  
		turret object should have the correct turret path.
		*/
		static double getAzimuth(Vehicle& vehicle, Turret& turret);
	
		/*!
		Returns the weapons position in model space. Weapon should have correct name.	
		*/
		static position3D getWeaponPosition(Vehicle& vehicle, Weapon& weapon);

		/*!
		Returns the weapons position in model space. turret should have the correct turret path.
		*/
		static position3D getWeaponPosition(Vehicle& vehicle, Turret& turret);

		/*!
		Returns the weapon firing point for a selected muzzle in model space.	
		*/
		static position3D getWeaponPoint(Vehicle& vehicle, Weapon& weapon);

		/*!
		Returns the weapon firing point for a selected muzzle in model space.	
		*/
		static position3D getWeaponPoint(Vehicle& vehicle, Turret& turret, Weapon& weapon);

		/*! 
		Get the aiming direction of the specified Weapon of the turret in armed vehicle.
		This method returns a unit vector of the direction. 
		*/
		static position3D getWeaponDirectionVector(Vehicle& vehicle, Turret& turret);

		/*! 
		set reload state of the weapon.
		*/
		static void setWeaponState(Vehicle& vehicle,Weapon& weapon, bool reloadStatus,double reloadTime);

		/*!
		Returns the list of objects the weapon is aiming at.
		*/
		static list<ControllableObject> getWeaponAimingTargets(Vehicle& vehicle, string weaponName);

		/*!
		Returns the list of objects the weapon is aiming at. Weapon object should have correct weapon name.
		*/
		static list<ControllableObject> getWeaponAimingTargets(Vehicle& vehicle, Weapon& weapon);

		/*!
		Remove the specified magazine from the Vehicle.	
		*/
		static void removeMagazine(Vehicle& vehicle, string name);

		/*!
		Remove all magazine from the vehicle.	
		*/
		static void removeallMagazines(Vehicle& vehicle, string name);

		/*!
		Add Weapon to the Vehicle. weapon slots are filled, any further addWeapon commands are ignored. 
		To ensure that the weapon is loaded at the start of the mission, at least one magazine should 
		be added, before adding the weapon.
		*/
		static void addWeapon(Vehicle& vehicle, string name);

		/*!
		Remove the specified Weapon from the unit.	
		*/
		static void removeWeapon(Vehicle& vehicle, string name);

		/*!
		Tells if a unit has the given weapon.	
		*/
		static bool hasWeapon(Vehicle& vehicle, string name);

		/*!
		Sets the direction of the Turret Weapon according to azimuth& elevation. 
		*/
		static void setWeaponDirection(Vehicle& vehicle, Turret& turret, double azimuth, double elevation, bool transition);

		/*!
		Returns how much ammunition (compared to a full state defined by the unit type) the vehicle has. 
		Value would be between 0 to 1.	
		*/
		static float getAmmoRatio(Vehicle& vehicle);

		/*!
		Sets how much ammunition (compared to a full state defined by the unit type) the vehicle has. 
		The value ranges from 0 to 1.	
		*/
		static void setVehicleAmmo(Vehicle& vehicle, float ammovalue);

		/*!
		Disables gunner input at the turret.
		*/
		static void disableGunnerInput(Vehicle& vehicle, Turret& turret, bool status = true);

		/*!
		Add weapons to the cargo space of vehicles, which can be taken out by infantry units. 
		Once the weapon cargo space is filled up, any further addWeaponCargo commands are ignored.
		*/
		static void addWeaponToCargo(Vehicle& vehicle, string weaponName, int count);

		/*!
		Add magazines to the cargo space of vehicles, which can be taken out by infantry units. 
		Once the magazine cargo space is filled up, any further addMagazineCargo commands are ignored.
		*/
		static void addMagazineToCargo(Vehicle& vehicle, string magName, int count);

		/*!
		Remove all weapons from the given vehicle's weapon cargo space.
		*/
		static void clearAllWeaponFromCargo(Vehicle& vehicle);

		/*!
		Remove all magazines from the given vehicle's magazine cargo space.
		*/
		static void clearAllMagazineFromCargo(Vehicle& vehicle);

		/*!
		Enables or Disables firing on current weapon in the selected turret. If true, the unit's current 
		weapon cannot be fired.
		*/
		static void setWeaponSafety(Vehicle& vehicle, Turret& turret, bool safe);

		/*!
		Return the current status of the weapon safety in the selected turret.
		*/
		static bool getWeaponSafety(Vehicle& vehicle, Turret& turret);

		/*!
		Updates all the Armed properties of the unit:   
		*/
		static void updateVehicleArmedProperties(Vehicle& vehicle);

		/*!
		Order the Armed Vehicle to fire on the given target (via the radio).   
		*/
		static void commandFire(Vehicle& vehicle, ControllableObject& co);

		/*!
		Order the Armed Vehicle to fire on the given target (without radio messages).   
		*/
		static void doFire(Vehicle& vehicle, ControllableObject& co);

		/*!
		Forces the Armed Vehicle to fire from the specifically named weapon.   
		*/
		static void fire(Vehicle& vehicle, string weaponName);

		/*!
		Forces the Armed Vehicle to fire according to the parameters given.  
		*/
		static void fire(Vehicle& vehicle, string muzzleName, string modeName);

		/*!
		Forces the Armed Vehicle to fire according to the parameters given.   
		*/
		static void fire(Vehicle& vehicle, string muzzleName, string modeName, string magazineName, position3D& pos, bool aim);

		/*!
		Apply the network shell mode. Normally shells are locally simulated following a fired event.
		When network shell mode is set, the simulation is broadcast to the network. 
		*/
		static void setNetworkShellMode(Vehicle& vehicle,bool mode);

		/*!
		Returns the gunner of a vehicle. 
		*/
		static Unit getGunner(Vehicle& vehicle);

		/*!
		Returns the commander of a vehicle. 
		*/
		static Unit getCommander(Vehicle& vehicle);

		/*!
		Returns the driver of a vehicle. 
		*/
		static Unit getDriver(Vehicle& vehicle);

		/*!
		Returns a list of arrays, containing information about each seat in the vehicle:

		type - Proxy type. Can be "driver", "gunner", "commander" or "cargo".
		position - Seat position in model space (i.e. relative to the vehicle).
		cargoID - Index of cargo position.
		unit - Unit occupying that seat. objNull if empty.
		turret - Turret path. For non-gunner positions this element is not returned. 
		*/
		static vector<CrewPos> getCrewPosition(Vehicle& vehicle);

		/*!
		Return all the muzzles of a turret
		*/
		static vector<string> getMuzzles(Vehicle& vehicle, Turret::TurretPath& turretPath);

		/*!
		Returns whether a vehicle's engine was disabled
		*/
		static bool isEngineDisabled(Vehicle& vehicle);


		/*!
		Disables/enables the engine of a land vehicle.

		A disabled engine will keep the vehicle from moving, but will not affect its damage status.
		Engines that have been destroyed by in-game actions (e.g. by driving through water), can also be re-enabled with this command.
		Disabled engines can be fixed by a repair truck.

		state - true to disable the engine on, false to enable it.
		*/
		static void applyEngineDisable(Vehicle& vehicle, bool state);


		/*!
		Marks a vehicle seat as blocked, so that neither AI nor players can use it.

		Blocking already used seats will not remove the occupying unit.
		A third optional boolean parameter can be used to unblock the seat. 

		vehicle: Object
		seat - Type of seat (proxy): Either "Driver", "Cargo" or "Commander" (the latter means a commander seat, not the commanding unit)
		index - Index of cargo position or turret definition. Optional for "Driver" and "Commander".
		blocked - Whether to block the seat or not. Optional (default is true).
		*/
		static void applyBlockSeat(Vehicle& vehicle, VEHICLEASSIGNMENTTYPE type, int index = 0, bool isblock = true);

		/*!
		Marks a vehicle seat as blocked, so that neither AI nor players can use it.

		Blocking already used seats will not remove the occupying unit.
		A third optional boolean parameter can be used to unblock the seat. 
		It is only for type of "Turret"
		vehicle: Object
		index - turret definition. 
		blocked - Whether to block the seat or not. Optional (default is true).
		*/
		static void applyBlockSeat(Vehicle& vehicle, vector<int> index, bool isblock = true);

		/*!
		Returns list of seats that were blocked
		*/
		static vector<BLOCKEDSEAT> getBlockedSeats(Vehicle& vehicle);

		/*!
		Returns the position of the unit in cargo.
		The first cargo position is 0. If unit is not in vehicle, -1 is returned.
		*/
		static int getCargoIndex(Vehicle& vehicle, Unit& unit);

		/*!
		Returns the unit for the specified cargo index.
		If the tested cargo position is empty, objNull is returned.
		*/
		static Unit getUnitAtCargoIndex(Vehicle& vehicle, int index);

		/*!
		Returns the current status of laser rangefinder (LRF)
		(corresponds with the in-game status color):

		0 - lasing not available (unit or vehicle position not equipped with LRF)

		1 - [red]: not lased (distance cannot be determined, e.g. water, air)

		2 - [yellow]: lasing in progress (distance is being determined)

		3 - [green]: lased (distance has been determined and displayed) 
		*/
		static int getLasingStatus(Vehicle& vehicle, Unit& gunner);

		/*!
		Controls turn indicators and hazard lights on vehicles (true=on, false=off). 

		left - Turns left indicator lights on and off. (If true, hazard light will be turned off.)
		hazard - Turns hazard lights on and off. (If true, indicator light will be turned off.)
		right - Turns right indicator lights on and off. (If true, hazard light will be turned off.)
		*/
		static void applyIndicators(Vehicle& vehicle, bool left, bool hazard, bool right);

		/*!
		Sets the new fording depth in meters for objects of the classes motorcycle, helicopter, tank and car. 
		Setting the max fording depth to -1 will cause the engine to revert to the default configuration value.
		*/
		static void applyMaxFordingDepth(Vehicle& vehicle, double depth);

		/*!
		Returns the fording depth of a vehicle in meters. 
		*/
		static double getMaxFordingDepth(Vehicle& vehicle);

		/*!
		Returns the gunner in the turret path specified. 
		*/
		static Unit getTurretUnit(Vehicle& vehicle, Turret::TurretPath turretPath);

		/*!
		Brings an aircraft to a flying altitude. Either planes or helicopters will take off realistically to reach this height
		*/
		static double makeAirborne(Vehicle& vehicle);

		/*!
		Returns whether or not the aircraft is currently airborne. 
		*/
		static bool isAirborne(Vehicle& vehicle);


		/*!
		Checks whether the light of a vehicle is turned on.
		(If used with other objects than vehicles may always return true or false.) 
		*/
		static bool isLightOn(Vehicle& vehicle);

		/*!
		Returns either the default fly-in height of aircraft (independently of its current altitude), or the value that was set via flyInHeight.
		*/
		static double getFlyInHeight(Vehicle& vehicle);


		/*!
		Sets the flying altitude for aircraft.
		Avoid too low altitudes, as helicopters and planes won't evade trees and obstacles on the ground.
		The default flying altitude depends on the "flyInHeight" definition in the vehicle's configuration. 
		*/
		static void flyInHeight(Vehicle& vehicle, double height);

		/*!
		Set object's orientation (given as direction and up vector).
		Since version +1.30 accepts additional parameter which, when object is attached, 
		specifies if vectors applied are relative to parent object's orientation.  
		*/
		static void setVectorDirAndUp(Vehicle veh,vector3D vectorDir, vector3D vectorUp, bool relative);

		/*!
		Set the passed thrust wanted for the left track of entity. Only for tracked vehicles. Vehicle needs to have the controller enabled
		*/
		static void applyThrustLeftWanted(Vehicle& vehicle, double factor);

		/*!
		Set the passed thrust wanted for the right track of entity. Only for tracked vehicles. Vehicle needs to have the controller enabled
		*/
		static void applyThrustRightWanted(Vehicle& vehicle, double factor);

		/*!
		Sets the watch direction and firing arc. In the scenario the AI gunner will only watch and engage targets within the allocated arc. Its set for a turrets gunner specified in parameters.

		Unlike setWeaponDirection, this command doesn't require disabled gunner input in order to be applied. 

		direction - Direction vector, acts as center of current arc
		sideRange - Horizontal width of arc (left/right)
		verticalRange - Vertical width of arc (up/down)
		relative - Sets if direction vector is relative to vehicle orientation
		*/
		static void applyFireArc(Vehicle& vehicle, Turret::TurretPath& turretPath, vector3D& direction, double sideRange, double verticalRange, bool relative);

		/*!
		Force aircraft landing.

		Planes will use the airport defined by assignToAirport, or the one closest to them (in this priority).
		Helicopters will use either the nearest editor-placed helipad, or the nearest flat location (in this priority). assignToAirport has no effect on helicopters, and either of them have to be within 500m, or it will not land.
		*/
		static void land(Vehicle& vehicle, LANDMODE mode);

		/*!
		Send message to vehicle radio channel. 
		*/
		static void sendToVehicleRadio(Vehicle& vehicle, string name);

		/*!
		Type text to vehicle radio channel.

		This function only types text to the list, it does not broadcast the message. If you want the message to show on all computers, you have to execute it on them.

		Object parameter must be a vehicle, not a player.
		If you are in a crew seat (i.e. driver, gunner or commander), then it will include that role in the chat name output (Eg: Driver (you_name): "Message"). 
		*/
		static void vehicleChat(Vehicle& vehicle, string message);

		/*!
		Returns true if engine is on, false if it is off. 
		*/
		static bool isEngineOn(Vehicle& vehicle);


		/*!
		Returns true if the given unit is turned out. Default is true
		*/
		static bool isTurnedOut(Unit& unit);

		/*!
		Assigns a unit as commander of a vehicle. Used together with orderGetIn to order a unit to get in as commander.

		delay - If unit already has an assigned vehicle role, time in seconds it will stay at the newly assigned position, 
		before it moves back to the original position, if that is still available. If position has been filled during the delay, 
		unit will then stay at the new position. If delay is negative, unit will always stay at new position.
		*/
		static void assignAsCommander(Vehicle& vehicle, Unit& unit, double delay);

		/*!
		Assigns a unit as driver of a vehicle. Used together with orderGetIn to order a unit to get in as commander.

		delay - If unit already has an assigned vehicle role, time in seconds it will stay at the newly assigned position, 
		before it moves back to the original position, if that is still available. If position has been filled during the delay, 
		unit will then stay at the new position. If delay is negative, unit will always stay at new position.
		*/
		static void assignAsDriver(Vehicle& vehicle, Unit& unit, double delay);

		/*!
		Assigns a unit as gunner of a vehicle. Used together with orderGetIn to order a unit to get in as commander.

		delay - If unit already has an assigned vehicle role, time in seconds it will stay at the newly assigned position, 
		before it moves back to the original position, if that is still available. If position has been filled during the delay, 
		unit will then stay at the new position. If delay is negative, unit will always stay at new position.
		*/
		static void assignAsGunner(Vehicle& vehicle, Unit& unit, double delay);

		/*!
		Assigns a unit as cargo of a vehicle. Used together with orderGetIn to order a unit to get in as commander.

		delay - If unit already has an assigned vehicle role, time in seconds it will stay at the newly assigned position, 
		before it moves back to the original position, if that is still available. If position has been filled during the delay, 
		unit will then stay at the new position. If delay is negative, unit will always stay at new position.
		*/
		static void assignAsCargo(Vehicle& vehicle, Unit& unit, double delay);

		/*!
		Assigns a unit as commander of a vehicle. Used together with orderGetIn to order a unit to get in as commander.	 
		*/
		static void assignAsCommander(Vehicle& vehicle, Unit& unit);

		/*!
		Assigns a unit as driver of a vehicle. Used together with orderGetIn to order a unit to get in as commander.
		*/
		static void assignAsDriver(Vehicle& vehicle, Unit& unit);

		/*!
		Assigns a unit as gunner of a vehicle. Used together with orderGetIn to order a unit to get in as commander.
		*/
		static void assignAsGunner(Vehicle& vehicle, Unit& unit);

		/*!
		Assigns a unit as cargo of a vehicle. Used together with orderGetIn to order a unit to get in as commander.
		*/
		static void assignAsCargo(Vehicle& vehicle, Unit& unit);

		/*!
		Force all units in the array to get in or out of their assigned vehicles. Units must be assigned to a vehicle before this command will do anything. 
		*/
		static void orderGetIn(vector<Unit> unitVec, bool order);

		/*!
		Force unit to get in or out of their assigned vehicles. Unit must be assigned to a vehicle before this command will do anything. 
		*/
		static void orderGetIn(Unit& unit, bool order);

		/*!
		Returns the weapons in cargo of the object. 
		It is added by addWeaponToCargo() method
		*/
		static vector<WEAPONCARGO> getWeaponFromCargo(Vehicle& vehicle);

		/*!
		Returns the magazines in cargo of the object. 
		It is added via addMagazineToCargo()
		*/
		static vector<MAGAZINECARGO> getMagazineFromCargo(Vehicle& vehicle);

		/*!
		Return all the muzzles of a person, vehicle
		*/
		static vector<string> getMuzzles(Vehicle& vehicle);


		/*!
		Returns the position a shooter has to aim at to hit a target with the current selected weapon.
		*/
		static position3D getAimingPosition(Vehicle& vehicle, ControllableObject& target);


		/*!
		It is only for local objects
		Order the given unit(s) to move to the given location (via the radio). 
		Exactly the same as doMove, except this command displays a radio message. 
		*/
		static void commandMove(Vehicle& vehicle, position3D position);

		/*!
		Returns the forced speed of an aircraft. 
		*/
		static double getForceSpeed(Vehicle& vehicle);

		/*!
		Returns the arc parameters set for specified turret. 
		*/
		static FIREARC getFireArcParameters(Vehicle& vehicle, Turret::TurretPath& turretPath);

		/*!
		If set to true, the player can steer the vehicle even when he is not driver. Note that an AI driver has to be 
		switched off manually with disableAIMOVE(); as this is not working for planes, you currently have to make sure that 
		the pilot is local to the player's client. 
		*/
		static void applyDriverOverride(bool overRide);

		/*!
		Deletes the specified weapon from the cargo of a vehicle. Does nothing if the weapon isn't actually in vehicle's cargo. 
		*/
		static void removeWeaponFromCargo(Vehicle& vehicle, string weapon);

		/*!
		Deletes the specified magazine from the cargo of a vehicle. Does nothing if the magazine isn't actually in vehicle's cargo. Magazines are removed in order from fullest to emptiest.
		*/
		static void removeMagazineFromCargo(Vehicle& vehicle, string magazine);

		/*!
		Removes the magazine from vehicles turret
		*/
		static void removeMagazineFromTurret(Vehicle& vehicle, string magazine, Turret::TurretPath& turretPath);

		/*!
		Add magazines to the cargo space of vehicles, which can be taken out by infantry units.
		*/
		static void addMagazineCargoEx(Vehicle& vehicle, string magazineName, int bulletCount);

		/*!
		Sets the armor (or health for men) state of the vehicle (a value from 0 to 1). 
		*/
		static void applyVehicleArmor(Vehicle& vehicle, double armor);

		/*!
		Checks whether the object is (a subtype) of the given type.
		Command works with classes contained in CfgVehicles or CfgAmmo, but not with others (e.g. CfgMagazines or CfgWeapons).
		*/
		static bool isKindOf(Vehicle& vehicle, string typeName);

		/*!
		Check if (and by how much) vehicle commander knows about target.
		And returns a number from 0 to 4.
		*/

		static float knowsAbout(Vehicle& vehicle, ControllableObject& target);

		/*!
		Applies the name of the vehicle to the VBS2 Environment.
		Prior to applying, function verifies that the name is a valid name
		according to following criteria.
		- Name has to start with letter or underscore.
		- Only special characters allowed is underscore.
		- Name should not contain spaces in between.
		- Name should not already exist in the mission.
		*/
		static void applyName(Vehicle& vehicle);

		/*!
		Returns the setting defined by setNetworkShell of the vehicle.
		*/
		static bool getNetworkShell (Vehicle& vehicle);

		/*!
		Lock or Unlock the vehicle for player. 
		*/
		static void lockVehicle(Vehicle& vehicle, bool playerLock);

		/*!
		Lock or Unlock the vehicle for player. 
		*/
		static void lockVehicle(Vehicle& vehicle, bool playerLock, bool allLock);

		/*!
		Checks whether the soldier is mounted in the vehicle.
		*/
		static bool isInVehicle(Vehicle& vehicle, Unit& unit);

		/*!
		Returns the formation leader of a given Vehicle.
		*/
		static Unit getFormLeader(Vehicle& vehicle);

		/*!
		Return the precision of the given entity.
		*/
		static float getPrecision(Vehicle& vehicle);

		/*!
		Return how much vehicle wants to reload its weapons.
		*/
		static ControllableObject getAssignedTarget(Vehicle& vehicle);

		/*!
		Returns the current position of a selection in model space.
		If a selection does not exist [0,0,0] is returned.
		*/
		static position3D getSelectionPosition(Vehicle& vehicle, string selectName);

		/*!
		Returns the current position of a selection in model space.
		If a selection does not exist [0,0,0] is returned.
		*/
		static position3D getSelectionPosition(Vehicle& vehicle, string selectName, string method);

		/*!
		Check whether magazine is reloaded whenever emptied.
		*/
		static bool isReloadEnabled(Vehicle& vehicle);

	private:

		/*!
		check for existence of the group when creating a new vehicle 
		if there isn't any group, create random group and add to it.
		*/
		static Group createGroup(Vehicle& vehicle);


	};
};
#endif //VEHICLE_UTILITIES_H