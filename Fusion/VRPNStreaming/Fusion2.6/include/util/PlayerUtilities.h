
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:
	PlayerUtilities.h
Purpose:
	This file contains the declaration of the PlayerUtilities class.
	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			17-09/2010	YFP: Original Implementation
	2.0			10-01/2011  CHS: Added getPlayerPosition()
	2.01		08-09/2011	NDB: Added Methods,
									bool isViewPlayer()
									OPTICSTATE getOpticsState()
									vector<double> getOpticsDOF()
									void applySuspendPlayer(bool suspend)
									bool isPlayerSuspended()
									int getNoOfPlayerSuspended()
									bool isJIPed()

/*****************************************************************************/

#ifndef VBS2FUSION_PLAYER_UTILITIES_H
#define VBS2FUSION_PLAYER_UTILITIES_H

/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES
#include <string>

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "../VBS2FusionDefinitions.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBS2Fusion
{
	class VBS2FUSION_API PlayerUtilities
	{
	public:
		/*!
			get player unit of the mission. 
		*/
		static Unit getPlayer();

		/*!
			set specified unit as player.player should be a playble unit.
		*/
		static bool setPlayer(Unit& player);

		/*!
			set unit as playable unit allowing MP role for the unit. 
		*/
		static void setPlayable(Unit& player);


		/*!
			update the player unit of the mission. 
		*/
		static void updatePlayer(Unit& player);

		/*!
			check weather the specified unit is player.
		*/
		static bool isPlayer(Unit& unit);

		/*!
			switch player between defined units.  	
		*/
		static bool switchPlayer(Unit& nplayer , Unit& oplayer);


		/*!
			get the player position of the mission. 
		*/
		static position3D getPlayerPosition();

		/*!
		Forces the player to fire using currently selected Weapon.
		*/
		static void forceFire(Unit& player);


		/*!
		Returns optics information for the player.

		To convert the returned zoom level into an FOV value, use 0.25/zoom.
		atan(FOV)*2 will then return the FOV in degrees. 
		*/
		static OPTICSTATE getOpticsState();

		/*!
		Returns the player's current focal settings. [focal plane, blur factor]
		*/
		static vector<double> getOpticsDOF();

		/*!
		Disables player from manipulating his avatar. Player can still access the GUI, though.
		Command is cumulative (i.e. if it is applied twice with a true argument, it also has to be applied 
		twice with a false argument, in order to enable player input again.) 
		*/
		static void applySuspendPlayer(bool suspend);

		/*!
		Returns player's suspension status
		*/
		static bool isPlayerSuspended();

		/*!
		Returns the level of player suspensions, as set by multiple, nested suspendPlayer commands.
		(Nested suspends can happen if the player is first suspended while the editor is open, and suspended another time, while the editor is open, due to shell shock effects.) 
		*/
		static int getNoOfPlayerSuspended();

	};
}


#endif