
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/


/*************************************************************************

Name:

	CameraUtilities.h

Purpose:

	This file contains the declaration of the CameraUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			14-09/2009	SLA: Original Implementation
	2.0			10-01/2009	UDW: Version 2 Implementation
	2.01		10-02/2010	MHA: Comments Checked
	2.02		27-09/2011	NDB: Added Methods
									position3D positionCameraToWorld(position3D camPos)
									void applyCameraPosition(Camera& camera, position3D position)
									void applyCameraRelativePosition(Camera& camera, position3D position)
									void applyCameraFov(Camera& camera, double level)
									void applyCameraFocus(Camera& camera, double distance, double blur)
									void applyCameraCommit(Camera& camera, double time)
									void applyCameraTarget(Camera& camera, ControllableObject& target)
									void applyCameraTarget(Camera& camera, position3D& target)

/************************************************************************/

#ifndef VBS2FUSION_CAMERA_UTILITIES_H
#define VBS2FUSION_CAMERA_UTILITIES_H

#include "position3D.h"
#include "VBS2Fusion.h"
#include "data/Camera.h"
#include "util/ExecutionUtilities.h"
#include "util/ControllableObjectUtilities.h"
#include "VBS2FusionDefinitions.h"

namespace VBS2Fusion
{		
	class VBS2FUSION_API CameraUtilities: public ControllableObjectUtilities
	{
	public:


		//********************************************************
		// Camera Effect utilities
		//********************************************************

		/*!
		Applies the effect specified by c.getCamEffectType() and c.getCamEffectPos() to the camera. 		
		\param c - A reference to a currently implemented camera object ,
 
		*/
		static void applyCamEffect(Camera& c);

		/*!
		Applies the given effect type at a given effect position, specified by user to the camera.
		 If you want to switch the screen directly to the first-person, aiming, third-person or 
		 group view of an object, use switchCamera instead.The effect type "Terminate" is used to 
		 exit the current camera view and switch back to the player's view.Needs the call of camCommit 
		 to be conducted. 
	
		 name is taken by the camEffectType and the position is taken by the camEffectPos

		-Error checking utility validates the following:
		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object ,
		\param camEffectType is the camera effect type it which is enumerated in the header file.
			values can be type of CAMEFFECTTYPE_INTERNAL, CAMEFFECTTYPE_EXTERNAL, CAMEFFECTTYPE_FIXED, 
			CAMEFFECTTYPE_FIXEDWITHZOOM, CAMEFFECTTYPE_TERMINATE. 
		\param camEffectPos is the camera effect type which is an enumerated value defined in the header file.
			values can be type of CAMEFFECTPOS_TOP, CAMEFFECTPOS_LEFT, CAMEFFECTPOS_RIGHT, CAMEFFECTPOS_FRONT,
			CAMEFFECTPOS_BACK.
		*/
		static void applyCamEffect(Camera& c, CAMEFFECTTYPE camEffectType, CAMEFFECTPOS camEffectPos);


		//********************************************************
		// Camera Target Position utilities
		//********************************************************
	
		/*!
		Applies the camera target for the position of the camera. 		
		camera is taken by the reference to the camera and position is taken by the c.getCamTarget() 
		\param c - A reference to a currently implemented camera object.
		*/

		static void applyCamTarget(Camera& c);

		/*!
		Applies the target position of the camera for the position specified by the camTarget.		
		camera is taken by the reference to the camera and position is taken by the camTarget 
	
		\param c - A reference to a currently implemented camera object 
		\param camTarget - A postion3D object where the camera will target it self to 
		*/


		static void applyCamTarget(Camera& c, position3D camTarget);

		/*!
		Applies the target position of the camera for the position of the target object.		
		camera is taken by the reference to the camera and 
		position given by the position of the ControllableObject passed as an argument 
	
		\param c - A reference to a currently implemented camera object 
		\param coTarget - the ControllableObject form which the position will be acquired. 
		*/
		static void applyCamTarget(Camera& c, ControllableObject& coTarget);

		//********************************************************
		// Camera Position utilities
		//********************************************************

		/*!
		Returns the position of camera in the VBS2 environment. The position is called
		using the alias of the camera

		Error checking utility validates the following:
		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object 
		*/

		static position3D getCamPos(Camera& c);

		/*!
		Acquires the camera position in game environment by using getCamPos(c) 
		and update it to the camera object in VBS2Fusion. 		

		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object 

		*/
		static void updateCamPos(Camera& c);

		/*!
		Applies the camera position specified in c.getPosition() to the camera. 
		Prepares the camera position		
		position is acquired by the c.getPosition()

		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object 

		*/
		static void applyCamPos(Camera& c);

		/*!
		Applies the camera position specified by the camPos object. 
		position is given by the camPos 

		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object 
		\param camPos - A postion3D object where the camera will prepare it self to 

		*/
		static void applyCamPos(Camera& c, position3D camPos);

		//********************************************************
		// Camera Relative Position utilities (Relative to Target)
		//********************************************************

		/*!
		Applies the camera relative position specified in c.getRelPos() to the camera. 
		Prepares the camera position relative to the current position of the current target
		
		position is acquired by the c.getRelPos()

		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object 

		*/

		static void applyCamRelPos(Camera& c);

		/*!
		Applies the camera relative position specified in camRelPos to the camera.  
		Prepares the camera position relative to the current position of the current target
		
		position is acquired by camRelPos

		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object 
		\param camRelPos - A postion3D object where the camera will prepare it self to 
		*/

		static void applyCamRelPos(Camera& c, position3D camRelPos);

		//********************************************************
		// Camera Commit Time utilities 
		//********************************************************

		/*!
		Applies the camera commit time specified in camCommit to the camera. 
		Smoothly conduct the changes that were assigned to a camera within the given time. 
		If the time is set to zero, the changes are done immediately.

		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object 
		\param camCommit - The time the variations should take place 
		*/
		static void applyCamCommit(Camera& c, double camCommit);

		/*!
		Returns the camera committed status.
		If camera is committed, returns true, else false.

		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object 

		*/

		static bool isCamCommitted(Camera& c);

		//********************************************************
		// Camera Focus utilities 
		//********************************************************

		/*!
		Applies the camera focus specified in c.getFocusDist() and c.getFocusBlur() to the camera. 
		Prepares the camera focus blur.[-1,1] will reset default values (auto focusing), [-1,-1] 
		will disable post processing (all is focused).
		
		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object 
		*/
		static void applyCamFocus(Camera& c);

		/*!
		Applies the camera focus specified in camFocusDist and camFocusBlur to the camera.
		Prepares the camera focus blur.[-1,1] will reset default values (auto focusing), [-1,-1] 
		will disable post processing (all is focused).

		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object 
		\param camFocusDist - Camera focus distance  
		\param camFocusBlur - camera focus Blur level  
		*/

		static void applyCamFocus(Camera& c, double camFocusDist, double camFocusBlur);

		//********************************************************
		// Camera Field of View (Fov) utilities 
		//********************************************************


/*!
		Returns the Fov of camera.Get camera FOV in form [minFov,maxFov] 
		
		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object 

		*/

		static vector<double> getCamFov(Camera& c);


		

		/*!	
		Get the camera Fov in game environment by using getCamFov(c) 
		and update it to the camera object in VBS2Fusion. Uses the setCamFov 
		method in the camera class.
	
		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object 

		*/

		static void updateCamFov(Camera& c);

		/*!	
		Applies the camera Fov specified in c.getCamFov() to the camera. 
		Prepares the camera field of view (zoom). 
		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param  c - A reference to a currently implemented camera object 

		*/

		static void applyCamFov(Camera& c);


		/*!	
		Applies the camera Fov specified in camFov to the camera. 
		Prepares the camera field of view (zoom). 
		
		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param  c - A reference to a currently implemented camera object 
		\param  camFov - the fov value specified  

		*/

		static void applyCamFov(Camera& c, double camFov);


		//********************************************************
		// Create Camera
		//********************************************************

		/*!
		Creates a new Camera. 
		Camera object need to be passed to this function and the properties that has camera object will be used during the camera creation.

		-Camera effect type, 
		-Camera effect position,
		-Camera target, 
		-Camera relative position.

		If no alias is specified in camera object, a random alias is generated.

		*/
		static void createCamera(Camera& c);

		/*!
		Creates a new Camera. 
		Camera object need to be passed to this function and the properties that has camera object will be used during the camera creation. 
		Additionally user defined camAlias value will be set.

		\param c - A reference to a currently implemented camera object 
		\param camAlias - The alias the camera should use 
 
		*/
		static void createCamera(Camera& c, string camAlias);

		/*!
		Creates a new Camera. 
		Camera object need to be passed to this function and the properties that has camera object will be used during the camera creation. 
		Additionally user defined camPos, camTarget, camAlias values will be set.

		\param c - A reference to a currently implemented camera object 
		\param camPos - The position the camera should create itself
		\param camTarget - The position the camera should focus itself
		\param camAlias - The alias the camera should use 

		*/

		static void createCamera(Camera& c, position3D camPos, position3D camTarget, string camAlias);

		/*!
		Creates a new Camera. Camera object need to be passed to this function and the properties that has camera object will be used during the camera creation. 
		Additionally user defined camEffectType, camEffectPos, camAlias values will be set.

		\param c - A reference to a currently implemented camera object 
		\param camEffectType is the camera effect type it is an enumerated value defined .
			values can be type of CAMEFFECTTYPE_INTERNAL, CAMEFFECTTYPE_EXTERNAL, CAMEFFECTTYPE_FIXED, 
			CAMEFFECTTYPE_FIXEDWITHZOOM, CAMEFFECTTYPE_TERMINATE.
		
		\param camEffectPos is the camera effect type it is an enumerated value defined in the header file.
			values can be type of CAMEFFECTPOS_TOP, CAMEFFECTPOS_LEFT, CAMEFFECTPOS_RIGHT, CAMEFFECTPOS_FRONT, CAMEFFECTPOS_BACK.		
		
		\param camAlias - The alias the camera should use 

		*/

		static void createCamera(Camera& c, CAMEFFECTTYPE camEffectType, CAMEFFECTPOS camEffectPos, string camAlias);
/*!
		Creates a new Camera. Camera object need to be passed to this function and the properties that has camera object will be used during the camera creation. 
		Additionally user defined camEffectType, camEffectPos, camPos values will be set.

		\param c - A reference to a currently implemented camera object 
		\param camEffectType is the camera effect type it is an enumerated value defined .
			values can be type of CAMEFFECTTYPE_INTERNAL, CAMEFFECTTYPE_EXTERNAL, CAMEFFECTTYPE_FIXED, 
			CAMEFFECTTYPE_FIXEDWITHZOOM, CAMEFFECTTYPE_TERMINATE.
		
		\param camEffectPos is the camera effect type it is an enumerated value defined in the header file.
			values can be type of CAMEFFECTPOS_TOP, CAMEFFECTPOS_LEFT, CAMEFFECTPOS_RIGHT, CAMEFFECTPOS_FRONT, CAMEFFECTPOS_BACK
					
		
		\param camPos - The position the camera should create itself

		*/
		static void createCamera(Camera& c, CAMEFFECTTYPE camEffectType, CAMEFFECTPOS camEffectPos, position3D camPos);

		/*!
		Creates a new Camera. 
		Camera object need to be passed to this function and the properties that has camera object will be used during the camera creation. 
		Additionally user defined camEffectType, camEffectPos, camTarget, camRelPos, camAlias values will be set.

		\param c - A reference to a currently implemented camera object 
		\param camEffectType is the camera effect type it is an enumerated value defined .
			values can be type of CAMEFFECTTYPE_INTERNAL, CAMEFFECTTYPE_EXTERNAL, CAMEFFECTTYPE_FIXED, 
			CAMEFFECTTYPE_FIXEDWITHZOOM, CAMEFFECTTYPE_TERMINATE.
		
		\param camEffectPos is the camera effect type it is an enumerated value defined in the header file.
			values can be type of CAMEFFECTPOS_TOP, CAMEFFECTPOS_LEFT, CAMEFFECTPOS_RIGHT, CAMEFFECTPOS_FRONT, CAMEFFECTPOS_BACK.		
		
		\param camTarget - The position the camera should target itself
		\param camRelPos - The position the camera should create itself
		\param camAlias - The alias the camera should use 

		*/

		static void createCamera(Camera& c, CAMEFFECTTYPE camEffectType, CAMEFFECTPOS camEffectPos, position3D camTarget, position3D camRelPos, string camAlias);

		/*!
		Creates a new Camera. 
		Camera object need to be passed to this function and the properties that has camera object will be used during the camera creation. 
		Additionally user defined camEffectType, camEffectPos, camTarget, camPos values will be set.

		\param c - A reference to a currently implemented camera object 
		\param camEffectType is the camera effect type it is an enumerated value defined .
			values can be type of CAMEFFECTTYPE_INTERNAL, CAMEFFECTTYPE_EXTERNAL, CAMEFFECTTYPE_FIXED, 
			CAMEFFECTTYPE_FIXEDWITHZOOM, CAMEFFECTTYPE_TERMINATE.
		
		\param camEffectPos is the camera effect type it is an enumerated value defined in the header file.
			values can be type of CAMEFFECTPOS_TOP, CAMEFFECTPOS_LEFT, CAMEFFECTPOS_RIGHT, CAMEFFECTPOS_FRONT, CAMEFFECTPOS_BACK.		
		
		\param camTarget - The position the camera should target itself
		\param camPos - The position the camera should create itself

		*/

		static void createCamera(Camera& c, CAMEFFECTTYPE camEffectType, CAMEFFECTPOS camEffectPos, position3D camTarget, position3D camPos);
		
		//********************************************************
		// Destroy utilities
		//********************************************************

		/*!
		Destroy the camera from the scene.
		Destroy an object created with camCreate.camDestroy is conducted immediately, the command doesn't wait for camCommit.

		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object 
		*/
		static void destroyCamera(Camera& c);
	
		/*!
		Executes a command on the given camera / actor object.
		The "manual on" and "manual off" commands are recognized for all types.
		For the "camera" type, the following commands can be used: "inertia on" and "inertia off".
		For the "seagull" type it's one of: "landed" and "airborne" these control if they land or fly. 
		When you execute camCommand "landed" on a flying seagull, it will land and sit on the floor until you call camCommand "airborne". 
		The camCommand changes are conducted immediately, the command doesn't wait for camCommit.

		\param c - A reference to a currently implemented camera object 
		\param camCommand - The command 

		*/
		static void camCommand(Camera& c, string camCommand);

		/*!
		Returns true if the camera is using radar mode else return false.
		*/
		static bool isCamRadarMode(Camera& camera);

		/*!
		Clears a camera's targets (both object and position).

		Testing code
		CameraUtilities::createCamera(camera);
		camera.setPosition(player.getPosition());
		CameraUtilities::applyPosition(camera);
		CameraUtilities::applyCamEffect(camera,CAMEFFECTTYPE::CAMEFFECTTYPE_INTERNAL,CAMEFFECTPOS::CAMEFFECTPOS_BACK);
		Then
		CameraUtilities::resetTargets(camera);
		*/
		static void resetTargets(Camera& camera);
		
		/*!
		Set camera orthography parameters.
		onoff - turn camera on or off. 
		left, right, bottom and top are respective edges of the camera.
		*/
		static bool applyCamOrthography(Camera& camera,bool onoff,double left, double right,double bottom,double top);

		/*!
		Explicitly set the direction of the camera if it is attached to an object.
		The camera direction is relative to the object. 
		Changes are committed instantly.

		Testing code
		CameraUtilities::createCamera(camera);
		CameraUtilities::applyCamEffect(camera, CAMEFFECTTYPE::CAMEFFECTTYPE_INTERNAL, CAMEFFECTPOS::CAMEFFECTPOS_BACK);
		ControllableObjectUtilities::attachTo(camera, unit, position3D(5,-2,2));
		Then
		CameraUtilities::applyAttachedLookDir(camera,vector3D(1,0,1));
		*/


		static void applyAttachedLookDir(Camera& camera, vector3D cameraDirection);

		/*!
		Returns current camera aperture. If the return value is -1 then that aperture 
		doesn't exist, and that it wasn't set by user.
		*/
		static double getAperture();

		/*!
		Sets custom camera aperture. set -1 as aperture value to do it automatically.	
		*/
		static void applyAperture(double apertureVal);

		/*!
		Set aperture for RTT(Render to texture) cameras.
		For automatic adjustment aperture should be -1.
		*/
		static void applyAperture(Camera& cameraRTT , double aperture);

		/*!
		Set camera sensor type.
		*/
		static void applySensorType(Camera& camera, CAMEFFECTMODE effectMode);

		/*!
		Returns true if the camera has finished preloading else false returns.
		*/
		static bool isPreLoaded(Camera& camera);

		/*!
		Returns camera interest for given unit. 
		*/
		static double getCameraInterest(ControllableObject& object);

		/*!
		Set camera interest for given entity.
		Note- Camera interest is by default 0. Camera interest is used to focus camera to control
		depth of field in cutscenes.Higher camera interest increases the chance of the unit being 
		focused.
		*/
		static void  applyCameraInterest(ControllableObject& object, double interestVal);

		/*!
		Returns NetworkID of the object to which the camera is attached. 
		*/
		static NetworkID  cameraOnObjectID();


		/*!
		Returns camera FOV (Feild of view) as vector [minFov, maxFov]
		*/
		static vector<float> getFovRange(Camera& camera);

		/*!
		Sets the camera dive angle. It does not automatically commit changes.
		*/
		static void applyDive(Camera& camera , double diveAngle);

		/*!
		Apply the camera heading angle. It does not automatically commit changes.
		*/
		static void applyCamDirection(Camera& camera, double direction);

		/*!
		Force drawing of cinema borders. This is normally used in cutscenes to indicate player has no control. 
		*/
		static void showCinemaBorder(bool show);

		/*!
		Sets custom camera view frustum. All parameters are given as tangents of angles from center of camera direction
		use - if set to false, resets camera frustum. 
		tanLeft, tanRight, tanBottom, tanTop are sides of the frustum respectively. 
		*/
		static bool applyFrustum(bool use, float tanLeft, float tanRight, float tanBottom, float tanTop);

		/*!
		Sets custom camera view frustum. All cameras, including the player view and editor cameras are affected by this.
		All parameters are given as tangents of angles from center of camera direction.

		use - If set to false, resets frustum.
		tanWidth - Changes only the FOV if height isn't set.
		tanHeight - Changes the aspect ratio when set with width. (Optional)
		tanWidth, tanHeight are the tan(FOV angle)
		*/
		static bool applyFrustum(bool use, float tanWidth, float tanHeight);

		/*!
		Sets custom camera view frustum. All cameras, including the player view and editor cameras are affected by this.
		All parameters are given as tangents of angles from center of camera direction.

		use - If set to false, resets frustum.
		tanWidth - Changes only the FOV
		tanWidth is the tan(FOV angle)
		*/
		static bool applyFrustum(bool use, float tanWidth);

		/*!
		Sets custom camera view frustum. All cameras, including the player view and editor cameras are affected by this.
		All parameters are given as tangents of angles from center of camera direction.

		use - If set to false, resets frustum.
		Eg: res = setCamFrustum [false] - Resets the camera frustum to default view.
		*/
		static bool applyFrustum(bool use);

		/*!
		Transform position from camera coordinate space to world coordinate space.
		*/
		static position3D positionCameraToWorld(position3D camPos);

		/*!
		Set the position of the given camera or seagull.Needs the call of applyCameraCommit() to be conducted.
		*/
		static void applyCameraPosition(Camera& camera, position3D position);

		/*!
		Smoothly conduct the changes that were assigned to a camera within the given time. If the time is set to zero, the changes are done immediately. 
		*/
		static void applyCameraCommit(Camera& camera, double time);

		/*!
		Set the target object where the given camera should point at. Needs the call of applyCameraCommit() to be conducted. 
		*/
		static void applyCameraTarget(Camera& camera, ControllableObject& target);

		/*!
		Set the target position where the given camera should point at. Needs the call of applyCameraCommit() to be conducted. 
		*/
		static void applyCameraTarget(Camera& camera, position3D& target);

		/*!
		Set the position of the given camera relative to its target set with applyCameraTarget().
		Needs the call of applyCameraCommit() to be conducted. 
		*/
		static void applyCameraRelativePosition(Camera& camera, position3D position);

		/*!
		focusRange is in format [distance,blur]. Sets the camera focus blur. It does not automatically commit changes.
		*/
		static void applyCameraFocus(Camera& camera, double distance, double blur);

		/*!
		Set the zoom level (field of view) of the given camera.

		The default zoom level is 0.7, 0.01 is the nearest and 10 the furthest zoom value (landscape distorts at FOVs > 2).
		The angle of the field of view is atan(FOV)*2 degrees. If the aspectRatio is not 3:4, then the FOV value in the equation has to be multiplied by the x-ratio (e.g. atan(.95*1.2)*2.
		Needs the call of applyCameraCommit() to be conducted. 
		*/
		static void applyCameraFov(Camera& camera, double level);

		/*!
		Sets custom camera view offset. All cameras, including the player view and editor cameras are affected by this.

		All arguments, except for the "custom" switch are optional, and default to 0.

		If the camera is set behind the player, the player head will be invisible (as it normally is, when the player is in first person view).
		
		custom - If set to false, resets frustum.
		*/

		static void applyCameraFrustumOffsets(bool custom, double azimuth, double pitch, double roll);

		/*!
		Sets custom camera view offset. All cameras, including the player view and editor cameras are affected by this.

		All arguments, except for the "custom" switch are optional, and default to 0.

		If the camera is set behind the player, the player head will be invisible (as it normally is, when the player is in first person view).
		Frustrum offsets are not reset between missions, or even editor restarts, and will have to be reset manually.

		custom - If set to false, resets frustum.
		offset - Offset in [x,y,z] 
		*/
		static void applyCameraFrustumOffsets(bool custom, double azimuth, double pitch, double roll, position3D offset);

	};
};

#endif