
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/


/*
* -- template to use error handler
* ErrorHandleUtilities::processError(CustomErrorHandler::E001,"EvenHandler", "PreocessFire");
*/

/*************************************************************************

Name:

ErrorHandleUtilities.h

Purpose:

This file contains the declaration of the Error class.

Version Information:

Version		Date		Author and Comments
===========================================
1.0			24-08/2009	SDS: Original Implementation
2.0			01-10/2010	UDW: Version 2 Implementation
2.01		10-02/2010	MHA: Comments Checked

/************************************************************************/

#ifndef VBS2FUSION_ERROR_HANDLE_H
#define VBS2FUSION_ERROR_HANDLE_H

#define VBS2FUSION_ERRORS(XX)\
	XX(E001, "E001", EE001, "Invalid Network ID!")\
	XX(E002, "E002", EE002, "Update Attempted Using Invalid ID!")\
	XX(E003, "E003", EE003, "Invalid Name!")\
	XX(E004, "E004", EE004, "Invalid Position Returned!")\
	XX(E005, "E005", EE005, "Invalid Velocity Returned!")\
	XX(E006, "E006", EE006, "Invalid Direction Returned!")\
	XX(E007, "E007", EE007, "Invalid Type Returned!")\
	XX(E008, "E008", EE008, "Invalid Player Option Returned!")\
	XX(E009, "E009", EE009, "Invalid Alive Status Returned!")\
	XX(E010, "E010", EE010, "Invalid Damage Status Returned!")\
	XX(E011, "E011", EE011, "Fired event called on non registered object!")\
	XX(E012, "E012", EE012, "Delete event called on non registered object!")\
	XX(E013, "E013", EE013, "Hit Part Event Called On Non Registered Object!")\
	XX(E014, "E014", EE014, "Waypoint Complete Event Called On Non Registered Object!")\
	XX(E015, "E015", EE015, "GetInMan Event Called On Non Registered Object!")\
	XX(E016, "E016", EE016, "GetOutMan Event Called On Non Registered Object!")\
	XX(E017, "E017", EE017, "Suppressed Event Called On Non Registered Object!")\
	XX(E018, "E018", EE018, "Respawn Event Called On Non Registered Object!")\
	XX(E019, "E019", EE019, "Invalid side passed. Cannot create group!")\
	XX(E020, "E020", EE020, "Error: Attempted To Update Invalid Group!")\
	XX(E021, "E021", EE021, "Error: Attempted To Load Invalid group!")\
	XX(E022, "E022", EE022, "Error: Attempted To Load Waypoints To Invalid Group!")\
	XX(E023, "E023", EE023, "Error: Attempted to update invalid group waypoint list!")\
	XX(E024, "E024", EE024, "Error: Attempted to load members for invalid group!")\
	XX(E025, "E025", EE025, "Empty Script Command Returned!")\
	XX(E026, "E026", EE026, "Empty Alias Returned!")\
	XX(E027, "E027", EE027, "Empty String Passed!")\
	XX(E028, "E028", EE028, "Empty Formation Returned!")\
	XX(E029, "E029", EE029, "Empty Rank!")\
	XX(E030, "E030", EE030, "Error: Invalid object reference to camera list!")\
	XX(E031, "E031", EE031, "Error: Attempted to use invalid unit!")\
	XX(E032, "E032", EE032, "AttachTo event called on non registered object!")\
	XX(E033, "E033", EE033, "Invalid Count Returned!")\
	XX(E034, "E034", EE034, "Error: VBS2 Function not registered!")\
	XX(E035, "E035", EE035, "Error: Object not activated in VBS2Fusion!")\

#define VBS2FUSION_WARNINGS(XX)\
	XX(W001, "W001", WW001, "Trying to Display an empty string" )\
	XX(W002, "W002", WW002, "Group size is zero" )\
	XX(W003, "W003", WW003, "Default Vehicle Type Used" )\
	XX(W004, "W004", WW004, "WW004" )\
	XX(W005, "W005", WW005, "WW005" )\

#define VBS2FUSION_ERRORCODE(error_code, error_code_str, error_description, error_description_str)\
	error_code,

#define VBS2FUSION_ERRORDESC(error_code, error_code_str, error_description, error_description_str)\
	error_description,

#define VBS2FUSION_WARNCODE(warn_code, warn_code_str, warn_description, warn_description_str)\
	warn_code,

#define VBS2FUSION_WARNDESC(warn_code, warn_code_str, warn_description, warn_description_str)\
	warn_description,

#define ERRORCODE_TO_STRING(error_code, error_code_str, error_description, error_description_str)\
	 case error_code :\
	 returnString = error_code_str; \
	 break;

#define WARNINGCODE_TO_STRING(warn_code, warn_code_str, warn_description, warn_description_str)\
	 case warn_code :\
	 returnString = warn_code_str;\
	 break;

#define ERRORDESC_TO_STRING(error_code, error_code_str, error_description, error_description_str)\
	 case error_description :\
	 returnString = error_description_str;\
	 break;

#define WARNINGDESC_TO_STRING(warn_code, warn_code_str, warn_description, warn_description_str)\
	 case warn_description :\
	 returnString =  warn_description_str;\
	 break;

#define CLEARLOG()\
	ofstream fout(LOGNAME);\
	fout << "";\
	fout.close();

#define LOGNAME "fusion_error_log.txt" //file name used to save the error information


/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>
#include <iostream>
#include <fstream>

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "data/ControllableObject.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/

namespace VBS2Fusion
{
	class VBS2FUSION_API ErrorHandleUtilities
	{

	public:

		/*!
		enum to define error types. 
		*/
		enum ERRORCODE
		{
			VBS2FUSION_ERRORS(VBS2FUSION_ERRORCODE)
			VBS2FUSION_E_C_TEMPLATE
		};

		/*!
		enum to define warning types. 
		*/
		enum WARNNINGCODE
		{
			VBS2FUSION_WARNINGS(VBS2FUSION_WARNCODE)
			VBS2FUSION_W_C_TEMPLATE		
		};

		/*!
		enum to define error description. 
		*/
		enum ERRORDESC
		{
			VBS2FUSION_ERRORS(VBS2FUSION_ERRORDESC)
			VBS2FUSION_E_D_TEMPLATE
		};


		/*!
		enum to define warning description. 
		*/
		enum WARNNINGDESC
		{
			VBS2FUSION_WARNINGS(VBS2FUSION_WARNDESC)
			VBS2FUSION_W_D_TEMPLATE	
		};

		/*!
		Primary constructor for the class. 
		*/
		ErrorHandleUtilities(void);

		/*!
		Primary destructor for the class. 
		*/
		~ErrorHandleUtilities(void);

		/*!
		Sets the type of error using a ERRORCODE variable. 
		"callingClass" specifies the class which cause the error and "callingFunction" specifies
		the function raise the error. These parameters and the related error description 
		will be used to either log into a file or to display error in user screen based on user input.		
		*/

		static void processError(ERRORCODE code, string callingClass, string callingFunction);

		/*!
		Sets the type of warning using a ERRORTYPES variable. Should be one of:
		"callingClass" specifies the class which cause the warning and "callingFunction" specifies
		the function raise the warning. These parameters and the related warning description 
		will be used to either log into a file or to display error in user screen based on user input.		
		*/
		static void processWarning(WARNNINGCODE type, string callingClass, string callingFunction);

		/*!
		Log the "message" in to [fusion_error_log.txt] inside the VBS2 root folder.
		*/
		static void logMessage(string message);

		/*!
		Returns true if VBS2Fusion logging is enabled, else false.
		*/
		static bool isVBS2FusionLoggingEnabled();

		/*!
		Enable or disable VBS2Fusion logging.
		*/
		static void setVBS2FusionLogging(bool status);

	private:
		/*!
		Returns the error code as a string variable. 
		*/
		static string getErrorCodeString(ERRORCODE types);

		/*!
		Returns the warning code as a string variable. 
		*/
		static string getWarningCodeString(WARNNINGCODE types);

		/*!
		Returns the error description as a string variable. 
		*/
		static  string getErrorDescString(ERRORDESC types);

		/*!
		Returns the warning description as a string variable. 
		*/
		static string getWarningDescString(WARNNINGDESC types);	

		

	};
};

#endif