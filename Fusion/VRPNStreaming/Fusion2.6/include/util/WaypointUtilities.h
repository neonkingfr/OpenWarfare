/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

WaypointUtilities.h

Purpose:

This file contains the declaration of the WaypointUtilities class.

Version Information:

Version		Date		Author and Comments
===========================================
1.0			27-03/2009	RMR: Original Implementation
1.01		27-08/2009  SDS: Added updateIsLocal(Waypoint& wp);			
							 updated updateWaypoint(Waypoint& wp);
2.0			11-01/2009	UDW: Version 2 Implementation
2.01		05-02/2010	MHA: Comments Checked
2.02		10-01/2011  CGS: Added updateNumber(Waypoint& wp)
								deleteWaypoint(Waypoint& wp )
2.03		08-09/2011	CGS: Added getAttachedObject(Waypoint& wp)
								setHousePosition(Waypoint& wp, int pos)
								getHousePosition(Waypoint& wp)
								WaypointUtilities::setWaypointVariable(Waypoint& wp, string name, VBS2Variable& val, bool publ)
								WaypointUtilities::getWaypointVariable(Waypoint& wp, string name)
2.04		04-11/2011	CGS: Added void applyRandomPosition(Waypoint& wp, position3D center, double radius)

/************************************************************************/

#ifndef VBS2FUSION_WAYPOINT_UTILITIES_H
#define VBS2FUSION_WAYPOINT_UTILITIES_H

#include <string>
#include <list>

#include "position3D.h"
#include "data/Waypoint.h"

namespace VBS2Fusion
{

	class VBS2FUSION_API WaypointUtilities
	{
	public:

		/*!
		Return the position of the waypoint.

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)		
		*/
		static position3D getPosition(Waypoint& wp);

		/*!
		Set the waypoint to the position given by wp.getPosition(). 		
		*/
		static void applyPosition(Waypoint& wp);

		/*!
		Set the waypoint to the position given by position. The position is not updated
		on the wo object. Use update to check if changes have been applied.  

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)		
		*/
		static void applyPosition(Waypoint& wp, position3D position);	

		/*!
		Update the position of the waypoint and save using wp.setPosition(). 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)		
		*/

		static void updatePosition(Waypoint& wp);

		//-------------------------------------------------------------------------

		/*!
		Get the direction of the waypoint.
		*/
		static double getDirection(Waypoint& wp);

		/*!
		Set the waypoint pointing in the direction given by wp.getDirection().		
		*/
		static void applyDirection(Waypoint& wp);		

		/*!
		Set the waypoint pointing in the direction given by direction. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)		
		*/
		static void applyDirection(Waypoint& wp, double direction);

		/*!
		Update the direction of the waypoint and save using wp.setDirection(). 
		*/
		static void updateDirection(Waypoint& wp);

	//	//-------------------------------------------------------------------------

		/*!
		Get the description of the waypoint. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)		
		*/
		static string getDescription(Waypoint& wp);

		/*!
		Apply the description given by wp.getDescription() to the waypoint. 
		*/
		static void applyDescription(Waypoint& wp);		

		/*!
		Apply the description given by description to the waypoint.

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)		
		*/
		static void applyDescription(Waypoint& wp, string description);		

		/*!
		Update the description of the waypoint and save using wp.setDescription(). 
		*/
		static void updateDescription(Waypoint& wp);

		//-------------------------------------------------------------------------

		/*!
		Get the ShowMode of the waypoint in string format. 
		*/
		static string getShowMode(Waypoint& wp);

		/*!
		Apply the ShowMode given by wp.getWaypointShowMode() to the waypoint.	
		*/
		static void applyShowMode(Waypoint& wp);		

		/*!
		Apply the ShowMode given by showMode to the waypoint. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)		
		*/
		static void applyShowMode(Waypoint& wp, string showMode);		

		/*!
		Update the showmode of the waypoint. 
		*/
		static void updateShowMode(Waypoint& wp);

		//-------------------------------------------------------------------------

		/*!
		Return the type of the waypoint in string format. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)		
		*/
		static string getType(Waypoint& wp);

		/*!
		Set the type of the waypoint using wp.getWaypointType(); 
		*/
		static void applyType(Waypoint& wp);		

		/*!
		Set the type of the waypoint using type; 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)		
		*/
		static void applyType(Waypoint& wp, string type);		

		/*!
		Update the type of the waypoint and save using wp.setWaypointType(). 
		*/
		static void updateType(Waypoint& wp);

		//	//-------------------------------------------------------------------------

		/*!
		Return the combat mode of the waypoint. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static string getCombatMode(Waypoint& wp);

		/*!
		Apply the combat mode obtained from wp.getWaypointCombatMode to the waypoint. 
		*/
		static void applyCombatMode(Waypoint& wp);		


		/*!
		Apply the combat mode obtained from combatMode to the waypoint. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static void applyCombatMode(Waypoint& wp, string combatMode);	

		/*!
		Update the combat mode of the waypoint and save using wp.setWaypointCombatMode. 
		*/
		static void updateCombatMode(Waypoint& wp);

		//	//-------------------------------------------------------------------------

		/*!
		Return the behavior of the waypoint in string format. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static string getBehaviour(Waypoint& wp);

		/*!
		Apply the behavior of the waypoint using wp.getWaypointBehaviour(). 
		*/
		static void applyBehaviour(Waypoint& wp);

		/*!
		Apply the behavior of the waypoint using behavior. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static void applyBehaviour(Waypoint& wp, string behaviour);

		/*!
		Update the behavior of the waypoint and save using wp.setWAypointBehaviour. 
		*/
		static void updateBehaviour(Waypoint& wp);

		//-------------------------------------------------------------------------

		/*!
		Return the formation of the waypoint in string format.

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static string getFormation(Waypoint& wp);

		/*!
		Apply the formation of the waypoint using wp.getWaypointFormation(). 
		*/
		static void applyFormation(Waypoint& wp);

		/*!
		Apply the formation of the waypoint using formation. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static void applyFormation(Waypoint& wp, string formation);

		/*!
		Update the formation of the waypoint and save using wp.setWaypointFormation(). 
		*/
		static void updateFormation(Waypoint& wp);

		//-------------------------------------------------------------------------

		/*!
		Return the speed mode of the waypoint in string format.

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static string getSpeedMode(Waypoint& wp);

		/*!
		Apply the speed mode of the waypoint using wp.getWaypointSpeedMode(). 
		*/
		static void applySpeedMode(Waypoint& wp);

		/*!
		Apply the speed mode of the waypoint using speedMode. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static void applySpeedMode(Waypoint& wp, string speedMode);

		/*!
		Update the speed mode of the waypoint and save using wp.setSpeedMode(). 
		*/
		static void updateSpeedMode(Waypoint& wp);


		//-------------------------------------------------------------------------

		/*!
		Return the timeout values of the waypoint using a vector<double>.

		return parameter 1 : Min
		return parameter 1 : Mid
		return parameter 1 : Max

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static vector<double> getTimeout(Waypoint& wp);

		/*!
		Apply the timeout values obtained from wp. 
		*/		
		static void applyTimeout(Waypoint& wp);

		/*!
		Apply the timeout values obtained from timeOut.

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static void applyTimeout(Waypoint& wp, vector<double> timeOut);

		/*!
		Update the timeout values and save in wp. 
		*/
		static void updateTimeout(Waypoint& wp);


		//-------------------------------------------------------------------------

		/*!
		Return the waypoint statements

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static string getWaypointStatements(Waypoint& wp);

		/*!
		Apply the statements obtained from wp. 
		*/
		static void applyStatements(Waypoint& wp);

		/*!
		Apply the statements specified in statements to the waypoint. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static void applyStatements(Waypoint& wp, string statements);

		
		static void applyStatements(Waypoint& wp, string condition, string statements);

		/*!
		Update the waypoint statements. 
		*/
		static void updateStatements(Waypoint& wp);

		//-------------------------------------------------------------------------

		/*!
		Return the waypoint script.
		
		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static string getScript(Waypoint& wp);

		/*!
		Apply the script obtained from wp. 
		*/
		static void applyScript(Waypoint& wp);

		/*!
		Apply the script obtained from script.
		
		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static void applyScript(Waypoint& wp, string script);

		/*!
		Update the script for wp. 
		*/
		static void updateScript(Waypoint& wp);

		/*!
		Update the GroupWaypointNo for wp. 
		*/
		static void updateNumber(Waypoint& wp);

		/*!
		Compound update for the following characteristics:

		- behavior
		- combat mode
		- description
		- direction
		- formation
		- position
		- script
		- show mode
		- speed mode
		- statements
		- timeout
		- type
		*/
		static void updateWaypoint(Waypoint& wp);

		/*!
		Applies changes for the following characteristics:

		- behavior
		- combat mode
		- description
		- direction
		- formation
		- position
		- script
		- show mode
		- speed mode
		- statements
		- timeout
		- type
		*/
		static void applyChanges(Waypoint& wp);

		//-------------------------------------------------------
		// Network id Utility
		//-------------------------------------------------------

		/*!
		Update "isLocal" property in co to know whether co has valid
		network id or not		
		*/
		static void updateIsLocal(Waypoint& wp);

		/*!
		Create a Waypoint in VBS2		
		*/
		static void createWaypoint(Waypoint& wp);

		/*!
		Delete a Waypoint in VBS2		
		*/
		static void deleteWaypoint(Waypoint& wp);

		/*!
		Attaches a Object to the given Waypoint.		
		*/
		static void WaypointAttachObject(Waypoint& wp, ControllableObject& co);

		/*!
		Synchronizes a waypoint with other waypoint.		
		*/
		static void synchronizeWaypoint(Waypoint& wp, Waypoint& synwp);

		/*!
		Synchronizes a waypoint with other list of waypoints.		
		*/
		static void synchronizeWaypoints(Waypoint& wp, list<Waypoint> wplist);

		/*!
		Gets the list of waypoints the specified waypoint is synchronized with.	
		*/
		static list<Waypoint> getSynchronizedWaypoints(Waypoint& wp);

		/*!
		Sets the radius for a loiter waypoint. Waypoint should be in Loiter type.	
		*/
		static void setLoiterRadius(Waypoint& wp, double radius);

		/*!
		Returns the radius of a loiter waypoint. For other waypoint types it will give -1.		
		*/
		static double getLoiterRadius(Waypoint& wp);

		/*!
		Sets the radius for a loiter waypoint. Waypoint should be in Loiter type.	
		*/
		static void setLoiterType(Waypoint& wp, string type);

		/*!
		Gets the radius for a loiter waypoint. Waypoint should be in Loiter type.	
		*/
		static string getLoiterType(Waypoint& wp);

		/*!
		Gets the Controllable object that attached to the waypoint.	
		*/
		static ControllableObject getAttachedObject(Waypoint& wp);

		/*!
		Sets the target house position for waypoints attached to a house,.	
		*/
		static void setHousePosition(Waypoint& wp, int pos);

		/*!
		Gets the house position assigned to the waypoint.	
		*/
		static int getHousePosition(Waypoint& wp);

		/*!
		Sets variable to given value in the variable space of given Waypoint.	
		*/
		static void setWaypointVariable(Waypoint& wp, string name, VBS2Variable& val, bool pub);

		/*!
		Gets the value of variable in the variable space of given Waypoint.	
		*/
		static VBS2Variable getWaypointVariable(Waypoint& wp, string name);

		/*!
		Set the waypoint to the random position in a circle with the given center and radius. The position 
		is not updated	on the wp object. Use update to check if changes have been applied.  	
		*/
		static void applyRandomPosition(Waypoint& wp, position3D center, double radius);
		
	};	
};

#endif //WAYPOINT_UTILITIES_H