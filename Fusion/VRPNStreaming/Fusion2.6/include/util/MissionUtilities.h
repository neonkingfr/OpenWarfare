/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	MissionUtilities.h

Purpose:

	This file contains the declaration of the MissionUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	2.0			01-04/2009	RMR: Original Implementation
	2.01		10-02/2010	MHA: Comments Checked
	2.02		10-01/2011  CGS: Added pauseSimulation() 
									isPaused()
									resumeSimulation()
									hostMission(string)
									endMission(string)
									playMission(string, string, bool)
    2.03	    04-03/2011  YFP: Added Methods,
									captureStart(string)
									captureStart(string,double, double,int,list<string>,bool,bool,double);
									captureStop()
									captureTest()
	2.04		21-06/2011	SSD: Added Methods;
									bool IsMapShown()
									bool IsWatchShown()
									bool IsCompassShown()
									bool IsWalkieTalkieShown()
									bool IsNotepadShown()
									bool IsGPSShown()
									void loadMissionName(Mission& mission)
									string getMissionName()
									vector<int> MissionStartTime()
	2.05		09-09/2011	NDB: Added Methods,
									bool isViewClient()
									float getWeaponSwayFactor()
									void applyWeaponSwayFactor(float WSFactor)
									vector<GeometryObject> getAllStaticObjects(string evaluation)
									vector<ControllableObject> getAllShots(string evaluation)
									vector<Vehicle> getAllVehicleExclusion(string evaluation)
									SIDE getPlayerSide()
									float getDayTime()
									bool isPlayerCadetMode()
									void SaveGame()
									void ForceEnd()
	2.06		30-09/2011	SSD: Added Methods;
									void ActivateKey(string keyName)
									bool IsKeyActive(string keyName)
	2.07		01-11/2011	CGS: Added Methods:
									string createCenter(SIDE side)
									void deleteCenter(SIDE side)
									int countEnemy(Unit unit, list<Unit> unitList)
									int countFriendly(Unit unit, list<Unit> unitList)
									int countUnknown(Unit unit, list<Unit> unitList)
									int countSide(SIDE side, list<Unit> unitList)
	2.08		21-11-2011	DMB: Added Methods:
									void onVehicleCreated(string command)
									int getPlayersNumber(SIDE side)

/************************************************************************/

#ifndef VBS2FUSION_MISSION_UTILITIES_H
#define VBS2FUSION_MISSION_UTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>
#include <list>

// SIMCENTRIC INCLUDES
#include "position3D.h"
#include "VBS2Fusion.h"
#include "data/Mission.h"
#include "data/ControllableObject.h"
#include "util/GroupListUtilities.h"
#include "util/TriggerListUtilities.h"
#include "util/VehicleListUtilities.h"
#include "util/EnvironmentStateUtilities.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBS2Fusion
{	
	class VBS2FUSION_API MissionUtilities
	{
	public:

		/*!
		Loads the mission onto the mission variable. Loads and updates (once) the following:

		- the vehicle list
		- the group list
		- the trigger list
		- the EnvironmentState variable.
		- the mission name
		*/
		static void loadMission(Mission& mission);

		/*!
		Updates the mission. Performs the following updates

		- the vehicle list
		- the group list
		- the trigger list
		- the EnvironmentState variable.
		*/
		static void updateMission(Mission& mission);

		/*!
		Returns the player unit of the mission.
		*/
		static Unit getPlayer();

		/*!
		Returns Network ID of Object which unit is mounted.
		*/
		static NetworkID getMountedObject(Unit& unit);

		/*!
		Set drive road side of the mission. Either right or left.
		*/
		static void setDriveRoadSide(ROADSIDE side);

		/*!
		get list of playable units of the mission. 
		*/
		static list<Unit> getPlaybleUnits();

		/*!
		check if the simulation is running 
		*/
		static bool isSimulationEnabled();

		/*!
		check if the simulation is paused
		*/
		static bool isPaused();

		/*!
		pause the game simulation.
		*/
		static void pauseSimulation();

		/*!
		Resume the game simulation / opposite to pauseSimulation.
		*/
		static void resumeSimulation();

		/*!
		Ends the mission with specific ending.
		*/
		static void endMission(string endtype);

		/*!
		A server is hosted and the mission is launched.
		*/
		static void hostMission(string missname);

		/*!
		Mission is launched.
		*/
		static void playMission(string campaign, string missname, bool skipBriefing);

		/*!
		Record in-game video. Recording automatically ends when Mission quit.
		If filename doesn't contain a backslash, is taken as a relative path with respect to
		the "$HOME\Documents\VBS2\video" 
		*/
		static bool captureStart(string fileName);


		/*!
		Record in-game video. Recording automatically ends when Mission quit.

		filename		- path to save video.   (backslashes needs to be escaped.)
		captureWidth	- capture video width.  (should be multiple of 4 : default 640)
		captureHeight   - capture video height. (should be multiple of 4 : default 640)
		frameRate		- Frames/Sec			(default :20)
		codercList		- List of codecs set by standard four letter names.
						  (codecs are tried one by one. If none works, Raw data is written. )
		recordSound     - Record sound on/off.
		recordWithUI	- Record video with UI.
		bufferSize		- capturing buffer size. 

		*/
		static bool captureStart(	string filename,          
									double captureWidth, 
									double captureHeight,
									int frameRate,
									list<string> codecList,
									bool recordSound,
									bool recordWithUI,
									double bufferSize
									);

		/*!
		Stop in-game video recording. 
		*/
		static void captureStop();

		/*!
		Check if video recording is running.
		*/
		static bool captureTest();

		/*!
		Returns true if player has map enabled, else false.
		*/
		static bool IsMapShown();

		/*!
		Returns true if player has watch enabled, else false.
		*/
		static bool IsWatchShown();

		/*!
		Returns true if player has compass enabled, else false.
		*/
		static bool IsCompassShown();

		/*!
		Returns true if player has radio enabled, else false.
		*/
		static bool IsWalkieTalkieShown();

		/*!
		Returns true if player has notebook enabled, else false.
		*/
		static bool IsNotepadShown();

		/*!
		Returns true if player has GPS receiver enabled, else false.
		*/
		static bool IsGPSShown();

		/*!
		Loads the mission name to the mission object.
		*/
		static void loadMissionName(Mission& mission);

		/*!
		Returns the current mission name.
		*/
		static string getMissionName();

		/*!
		Return when mission started in format [year, month, day, hour, minute, second]. 
		Works only in multi-player, in single-player all values are equal to zero [0,0,0,0,0,0]
		*/
		static vector<int> MissionStartTime();

		/*!
		Returns true if the current player is logged in as a view client
		*/
		static bool isViewClient();

		/*!
		Return the current weaponSwayFactor
		*/
		static float getWeaponSwayFactor();

		/*!
		Change default sway which is depending on fatigue. If set to 0 the weapon should be completely steady. 
		*/
		static void applyWeaponSwayFactor(float WSFactor);

		/*!
		Return a list of all vehicles in the current mission, excluding the ones covered by the evaluation.
		_x is substituted for the actual object in the evaluation.

		evaluation - Object which fulfills the specified condition will be excluded
		*/
		static vector<Vehicle> getAllVehicleExclusion(string evaluation);

		/*!
		Returns the player's side. This is valid even when the player controlled person is dead 
		*/
		static SIDE getPlayerSide();

		/*!
		Returns the current ingame time in hours. 
		Time using a 24 hour clock
		*/
		static float getDayTime();

		/*!
		Returns if the player is currently playing in cadet or veteran mode.
		*/
		static bool isPlayerCadetMode();

		/*!
		Activates the given Key Name for the current user profile.
		Keys can be used to indicate completed missions or to check whether 
		required missions have been completed. 
		*/
		static void ActivateKey(string keyName);

		/*!
		Checks whether the given Key is active in the current user profile.
		*/
		static bool IsKeyActive(string keyName);


		/*!
		Creates a new AI HQ for the given side.
		*/
		static string createCenter(SIDE side);

		/*!
		Destroys the AI center of the given side.
		*/
		static void deleteCenter(SIDE side);



		/*!
		Count how many units in the list are considered enemy to the given unit.
		*/
		static int countEnemy(Unit unit, list<Unit> unitList);

		/*!
		Count how many units in the list are considered friendly to the given unit.
		*/
		static int countFriendly(Unit unit, list<Unit> unitList);

		/*!
		Count how many units in the list are unknown to the given unit.
		*/
		static int countUnknown(Unit unit, list<Unit> unitList);

		/*!
		Count how many units in the list belong to given side.
		*/
		static int countSide(SIDE side, list<Unit> unitList);

		/*!
		This statement is launched whenever a player is disconnected from a MP session.
		*/
		static void onPlayerDisconnected(string statement);

		/*!
		Defines an action performed just after a vehicle or object is created.
		Variable command should be a valid VBS2 script command.
		*/
		static void onVehicleCreated(string command);

	};
};

#endif // MISSION_UTILITIES_H