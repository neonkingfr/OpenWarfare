 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:	MapUtilities.h

Purpose: This file contains VBS2 Real Time Editor (RTE) related Utility function definitions. 

/*****************************************************************************/
#ifndef VBS2FUSION_MAPUTILITIES_H
#define VBS2FUSION_MAPUTILITIES_H

/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES
#include <vector>

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "position3D.h"
#include "data/MapObject.h"
#include "VBS2FusionDefinitions.h"
#include "data/ControllableObject.h"
#include "data/Unit.h"

/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBS2Fusion
{
	class VBS2FUSION_API MapUtilities
	{
	public:		
		/*!
			creates a controllable object.
			- The type of object is as specified by parameter "typeName".
			- Creates the object at the position specified by parameter "pos".
		*/
		static void createObject(MapObject& co, string typeName, position3D pos);

		/*!
		Lists Id of any deleted or promoted map objects.
		As the original map objects are deleted during promotions, they will be listed as well. 
		*/
		static vector<string> getAllDeletedMapObjectsId();

		/*!
		Nearest building to given object.
		A "building" is defined as an object that is of class "House". 
		*/
		static MapObject getNearestBuilding(ControllableObject& object);

		/*!
		Deletes specified Map Object. Only objects created during the game can be deleted.
		*/
		static void deleteObject(MapObject& co);

		/*!
		Detects whether an object was part of the terrain (e.g. plants, buildings), 
		or whether it was placed via the editor or via script command. 
		obj - id and position Must be set
		*/
		static bool isMapPlaced(MapObject& obj);

		/*!
		Deletes a map (visitor placed) object.
		Returns the map id
		obj - id and position Must be set
		*/
		static string deleteMapObject(MapObject& obj);

		/*!
		Restores a map object that was previously deleted (via deleteMapObject).
		When a map object is "promoted", a copy of the map object is created as an editable object, 
		while the original object is removed from the map. If a map object is restored, this copy will 
		stay on the map, in addition to the restored map object. 
		*/
		static MapObject restoreMapObject(string id);

		/*!
		Makes a static map object "dynamic", so that changes to it will be propagated through the network. 
		*/
		static void staticToDynamic(MapObject& obj);

		/*!
		Returns the given indexed position in a building

		The index is 0-based (i.e. the first possible position would be 0. So if a building has 5 positions listed in the editor, 4 would be the highest position index usable with this command). 
		*/
		static position3D getBuildingPos(MapObject& obj, int index);
	};

};

#endif	//VBS2FUSION_RTEUTILITIES_H