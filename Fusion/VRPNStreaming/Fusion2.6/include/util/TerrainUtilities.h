
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************

Name:

	TerrainUtilities.h

Purpose:

	This file contains the declaration of the TerrainUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			28-04/2009	RMR: Original Implementation
	2.0			10-02/2010  YFP: Version 2 Implementation
	2.01		10-02/2010	MHA: Comments Checked
	2.02        26-05/2010  YFP: Added Methods,
									void exportTerrainMapSHP(string,string);
	2.03		18-01-2011	CGS:  Modified Method GetNearestCollisionInfo
	2.04		25-07-2011	SSD: Added	bool cylinderCollision(...)
	2.05		21-11-2011	DMB: Added functions
									bool surfaceIsWater(position3D pos)
									string getSurfaceType(position3D pos)
									double getSurfRoughness(position3D pos)
	

/************************************************************************/

#ifndef VBS2FUSION_TERRAIN_UTILITIES_H
#define VBS2FUSION_TERRAIN_UTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>
#include <list>
// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "util/ExecutionUtilities.h"
#include "VBS2FusionDefinitions.h"
#include "position2D.h"
#include "data/ControllableObject.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/

namespace VBS2Fusion
{
	
	class VBS2FUSION_API TerrainUtilities
	{
	public:
		
	

		/*!
		Returns the terrain cell height for coordinate position
		[xCoordinate, zCoordinate]. 
		*/
		static double TerrainHeight(double xCoordinate, double zCoordinate);


		/*!
		Returns the height above water of the road surface at the given position 
		[xCoordinate, zCoordinate]. 
		*/
		static double RoadSurfaceHeight(double xCoordinate, double zCoordinate);

		/*!
		Returns true if there is a collision detected between the two points. 

		startPos - a position3D parameter giving the position ASL of the start point. The position should be certain height from land
		endPos - a position3D parameter giving the position ASL of the end point. The position should be certain height from land
		type - The type of collision test to perform. Should be one of {FIRE, VIEW, GEOM or IFIRED}. 
		radius - The radius of the cylinder used to perform the collision test. 
		density - The spacing between casted rays. 
		ignoreObjectAlias = [Optional] parameter to specify the alias of an object to ignore during the collision test. 

		NOTE: This function will cast at least a single ray even in circumstances where the radius is much smaller
		than the density specified. Use relevant density and radius values to perform custom collision detection tasks. 
		*/
		static bool IsColliding(position3D startPos, 
			position3D endPos, 
			COLLISIONTESTTYPE type, 
			double radius, 
			double density, 
			string ignoreObjectAlias = "");


		/*!
		Returns true if there is a collision detected between the two points. 

		startPos - a position3D parameter giving the position ASL of the start point. The position should be certain height from land
		endPos - a position3D parameter giving the position ASL of the end point. The position should be certain height from land
		type - The type of collision test to perform. Should be one of {FIRE, VIEW, GEOM or IFIRED}. 
		radius - The radius of the cylinder used to perform the collision test. 
		density - The spacing between casted rays. 
		ignoreObjectAlias = [Optional] parameter to specify the alias of an object to ignore during the collision test. 

		In the case of a detected collision (i.e. return value is true), this function will also store
		the coordinates of the first collision point in the variable firstCollision and the normal of that collision
		in the parameter normal. 
		If a collision is not detected, 'endPos' will be returned as the first collision point.

		NOTE: This function will cast at least a single ray even in circumstances where the radius is much smaller
		than the density specified. Use relevant density and radius values to perform custom collision detection tasks. 
		*/
		static bool GetNearestCollisionInfo(position3D startPos, 
			position3D endPos,
			position3D& firstCollision, 
			position3D& normal,
			COLLISIONTESTTYPE type,
			double radius,
			double density,
			string ignoreObjectAlias = "");


		
		/*!
		This function returns true if a collision is detected within a cylinder of 
		height = (radius * 2) and radius = radius is placed upright 
		with its center at pos. 

		Useful function for determining if a unit or a similar object can be placed
		at a certain position. 

		Note: The considered cylinder should not collide with the land.
		So select the (position3D)center height larger than radius.
		*/
		static bool uprightCylinderCollision(position3D& center, double radius);

		/*!
		Checks the specified position is on a Road.
		*/
		static bool isOnRoad(position3D& position);

		/*!
		Check the specified object is on the road. 
		*/
		static bool isOnRoad(ControllableObject& co);

		/*! 
		Export terrain map to a shape file. 
		objectFileName - File name for export objects. 
		roadFileName   - File name for export roads.
		*/
		static bool exportTerrainMapSHP(string objectFileName , string roadFileName);	

		/*!
		Converts the an in-game position to a grid string. Returns an 8-digit string. 
		*/
		static string positionToGrid(position3D pos);

		/*!
		Converts the grid coordinates to in-game position. Returns a position3D coordinate. 
		*/
		static position3D gridtoPosition(string gridPosition);


		/*!
		Converts the game position to the MGRS co 
		ordinates.
		*/
		static string positionToMGRS(position3D pos);

		/*!
		Converts the game position to the MGRS co ordinates with precision specified(Maximum is 5).   
		*/
		static string positionToMGRS(position3D pos , int precision);

		/*!
		Converts the game position to Latitude.
		*/
		static string positionToLat(position3D pos);

		/*!
		Converts the game position to Latitude with precision specified.
		*/
		static string positionToLat(position3D pos , int precision);

		/*!
		Converts the game position to Longitude. 
		*/										
		static string positionToLon(position3D pos);

		/*!
		Converts the game position to Longitude with precision specified.
		*/										
		static string positionToLon(position3D pos, int precision);

		/*!
		Returns the top left grid position string of the map of current mission. 
		*/
		static string getTopLeftGridPos();

		/*!
		Returns the bottom right grid position string of the map of current mission. 
		*/
		static string getBottomRightGridPos();

		/*!
		Returns the top left grid position of the map of current mission. 
		*/
		static position3D getTopLeftPos();

		/*!
		Returns the bottom right grid position of the map of current mission. 
		*/
		static position3D getBottomRightPos();

		/*!
		Returns the ground intercept point (position) along the specified direction. 
		position - origin position, in other words a position in the vector line
		direction - direction to find intercept the ground
		*/
		static position3D getGroundIntercept(position3D position, vector3D direction);

		/*!
		Returns the ground intercept point (position) along the specified direction.
		position - origin position as ASL position, in other words a position in the vector line
		direction - direction to find intercept the ground
		*/
		static position3D getGroundInterceptASL(position3D position, vector3D direction);

		/*!
		Returns the ground intercept point (position) along the specified direction. 
		co - This object position is origin position, in other words a position in the vector line
		direction - direction to find intercept the ground
		*/
		static position3D getGroundIntercept(ControllableObject& co, vector3D direction);

		/*!
		Returns the ground intercept point (position) along the specified direction.
		co - This object position is origin position as ASL position, in other words a position in the vector line
		direction - direction to find intercept the ground
		*/
		static position3D getGroundInterceptASL(ControllableObject& co, vector3D direction);

		/*!
		Resets all height changes in the current map. 
		*/
		static void resetTerrainHeights();

		/*!
		Returns the grid size of the current Terrain 
		*/
		static float getTerrainGrid();

		/*!
		Changes a terrain cell height 
		*/
		static void setTerrainHeight(float x, float z, float height);

		/*!
		Changes terrain cells height within an area. It can be controlled by setting the optional 
		parameters if the lower or higher cells can be changed. 
		*/
		static void setTerrainHeightArea(position2D pos1, position2D pos2,float height, bool changeLower, bool changeHigher);

		/*!
		Returns array with [min,mid,max] height of the specified area. 
		*/
		static list<float> getTerrainHeightArea(position2D pos1, position2D pos2);

		/*!
		Flattens a crater created by an explosion.
		It flatten a circle area.
		parameters
		co - This object position is the center of the area.
		radius - It is the radius of the area
		flatten = 0.0 - flatten entirely, 1.0 - keep the same
		*/
		static void setFlattenGround(ControllableObject& co , float radius, float flatten);

		/*!
		Flattens a crater created by an explosion.
		It flatten a circle area.
		parameters
		position - This position is the center of the area.
		radius - It is the radius of the area
		flatten = 0.0 - flatten entirely, 1.0 - keep the same
		*/
		static void setFlattenGround(position3D position, float radius, float flatten);


		/*!
		Returns true if there is a collision detected between the two points. 

		startPos - a position3D parameter giving the position ASL of the start point.
		endPos - a position3D parameter giving the position ASL of the end point.
		type - The type of collision test to perform. Should be one of {FIRE, VIEW, GEOM or IFIRED}. 
		radius - The radius of the cylinder used to perform the collision test. 
		density - The spacing between casted rays. 
		ignoreObject = This object is not considered for collisions. 

		In the case of a detected collision (i.e. return value is true), this function will also store
		the coordinates of the first collision point in the variable firstCollision and the normal of that collision
		in the parameter normal. 

		If a collision is not detected, 'endPos' will be returned as the first collision point.
		*/

		static bool cylinderCollision(position3D startPos, 
			position3D endPos,
			position3D& firstCollision, 
			position3D& normal,
			COLLISIONTESTTYPE type,
			double radius,
			double density,
			ControllableObject ignoreObject);

		/*!
		Converts the grid coordinates to in-game position. 

		position - Grid position (6 or more digits)
		*/
		static position3D gridCenterToPosition(string position);

		/*!
		Export map data in into a ESRI ASCII file.
		The files are stored in the user profile's "export" folder (e.g. \My Documents\VBS2\export).
		If the UTM zone for the map is defined (not -1) a .prj file is created as well
		*/
		static bool exportASCIIGrid(string fileName);

		/*!
		Detects whether an object was part of the terrain (e.g. plants, buildings), 
		or whether it was placed via the editor or via script command. 
		*/
		static bool isMapPlaced(ControllableObject& obj);

		/*!
		Construct height map representation with respect to the given accuracy
		*/
		static double applyHeightMapAccuracy(double accuracy);

		/*!
		Returns whether water is at given position.
		*/
		static bool surfaceIsWater(position3D pos);

		/*!
		Returns what surface is at the given position.
		*/
		static string getSurfaceType(position3D pos);

		/*!
		Returns roughness on given surface position (Function does not work on paved roads).
		*/
		static double getSurfRoughness(position3D pos);


	};
};

#endif //VBS2FUSION_TERRAIN_UTILITIES_H