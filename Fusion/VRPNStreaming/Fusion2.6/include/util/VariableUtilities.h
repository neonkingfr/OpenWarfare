/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	VariableUtilities.h

Purpose:

	This file contains the declaration of the VariableUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			09-01/2010	RMR: Original Implementation
	2.01		10-02/2010	MHA: Comments Checked
	2.02		18-11-2011	DMB: Added Functions
									void bcastPublicVariable(string name)	

/************************************************************************/

#ifndef VBS2FUSION_VARIABLE_UTILITIES_H
#define VBS2FUSION_VARIABLE_UTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>
#include <math.h>

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "conversions.h"
#include "data/ControllableObject.h"
#include "data/Group.h"
#include "util/ExecutionUtilities.h"
#include "data/VBS2Variable.h"
#include "position3D.h"

namespace VBS2Fusion
{	
	

	class VBS2FUSION_API VariableUtilities
	{
	public:

		/*!
		Sets a variable of name with value to the ControllableObject defined by co. Variable is set as a double. 
		Set public to false to limit the variable to the local machine. 
		*/
		static void setVariable(ControllableObject& co, string& name, double value, bool bPublic = true);

		/*!
		Sets a variable of name with value to the ControllableObject defined by co. Variable is set as an integer. 
		Set public to false to limit the variable to the local machine. 
		*/
		static void setVariable(ControllableObject& co, string& name, int value, bool bPublic = true);

		/*!
		Sets a variable of name with value to the ControllableObject defined by co. Variable is set as a float. 
		Set public to false to limit the variable to the local machine. 
		*/
		static void setVariable(ControllableObject& co, string& name, float value, bool bPublic = true);

		/*!
		Sets a variable of name with value to the ControllableObject defined by co. Variable is set as a string. 
		Set public to false to limit the variable to the local machine. 
		*/
		static void setVariable(ControllableObject& co, string& name, string value, bool bPublic = true);

		/*!
		Sets a variable of name with value to the ControllableObject defined by co. Variable is set as a boolean. 
		Set public to false to limit the variable to the local machine. 
		*/
		static void setVariable(ControllableObject& co, string& name, bool value, bool bPublic = true);

		/*!
		Sets a variable of name with value to the ControllableObject defined by co. Variable is set as a position3D. 
		Set public to false to limit the variable to the local machine. 
		*/
		static void setVariable(ControllableObject& co, string& name, position3D value, bool bPublic = true);


		/*!
		Used to toggle civilians between standard formation (line, wedge, column, etc) and crowd formation. 
		This is a tight, random "formation", keeps them off roads, and prevents them from running to catch up with their group.
		Applies parameter globally. 

		Applies to: civilian units on foot 

		Ref: http://community.bistudio.com/wiki/VBS2:_ObjectVariables
		*/
		static void setCrowdFormation(ControllableObject& co, bool value);

		/*!
		Returns the value of the crowd formation variable. 
		*/
		static bool getCrowdFormation(ControllableObject& co);

		/*!
		Allows individual movement control of group members, without having them fall back into formation.
		Applies to: group members on foot. 

		Applies parameter globally. 

		Parameters: Position3D - destination to move to

		Ref: http://community.bistudio.com/wiki/VBS2:_ObjectVariables
		*/
		static void setForceMove(ControllableObject& co, position3D pos);

		/*!
		Returns the value of the ForceMove variable. 
		*/
		static position3D getForceMove(ControllableObject& co);



	
		/*static void setVariable(VBS2Fusion::Group& g, string& name, double value, bool bPublic = true);

		static void setVariable(VBS2Fusion::Group& g, string& name, int value, bool bPublic = true);

		static void setVariable(VBS2Fusion::Group& g, string& name, float value, bool bPublic = true);

		static void setVariable(VBS2Fusion::Group& g, string& name, string value, bool bPublic = true);

		static void setVariable(VBS2Fusion::Group& g, string& name, bool value, bool bPublic = true);



		static void setVariable(VBS2Fusion::Waypoint& w, string& name, double value, bool bPublic = true);

		static void setVariable(VBS2Fusion::Waypoint& w, string& name, int value, bool bPublic = true);

		static void setVariable(VBS2Fusion::Waypoint& w, string& name, float value, bool bPublic = true);

		static void setVariable(VBS2Fusion::Waypoint& w, string& name, string value, bool bPublic = true);

		static void setVariable(VBS2Fusion::Waypoint& w, string& name, bool value, bool bPublic = true);*/

		/*!
		Returns a variable of the name in the form of a VBS2Variable. The type of the returned value can be 
		obtained using the TypeOf method in the VBS2Variable. Type will be VBS2Variable::NONE if the returned value
		is invalid or the variable hasn't been assigned to the object, 
		*/
		static VBS2Variable getVariable(ControllableObject& co, string name);

		/*!
		Broadcast variable value to all computers.Types are supported;
		Number
		Boolean
		Object
		Group
		*/
		static void bcastPublicVariable(string name);

	};
};

#endif
