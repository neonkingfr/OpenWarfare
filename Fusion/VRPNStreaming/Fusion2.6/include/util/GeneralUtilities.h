
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/


/*************************************************************************

Name:

	GeneralUtilities.h

Purpose:

	This file contains the declaration of the CameraUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			30-09/2009	SLA: Original Implementation
	2.0			10-02/2010	UDW: Version 2 Implementation
	2.01		13-09/2010  YFP: Added Methods,
									string getVBS2Directory(int)
									string getVBS2UserDirectory()
									string getVBS2MissionDirectory()
									string getVBS2InstallDirectory()
									string getVBS2EXEDirectory()
	2.02		30-08/2011 NDB: Added Methods,
									bool isFusionDeveloper()
									double getCustomerID()
									vector<double> getSupportedResolutions()
									void exportEMF(string fileName, bool drawGrid = true, bool drawContours = true, int drawTypes = 256, bool flipVertical = false)
									bool isTypeExists(string type)
									bool isValidVariableName(string name)
									bool isValidFileName(string name)
									void useOldNetworkCode(bool mode = false)
									void openURL(string url)
									void DisableRendering(bool disable)
									void applyResolution(int width, int height)
									bool IsDifficultyEnabled(string flagName)
									bool IsServer()
									double BenchMark()
	2.03		07-09/2011 NDB: Added Methods,
									bool isTitleEffectActive()
									vector<int> getNetworkStats()
									bool isDriverOverride()
									void UnLoadAllPlugins()
									void LoadAllPlugins()
									void connectToPhyxDebugger()

	2.04		28-09/2011 CGS: Added Methods, 
									double getLengthOf(string type)
									double getWeightOf(string type)
									double getVolumeOf(string type)
	2.05		28-10/2011 NDB: Added Methods,
									bool isMultiPlayer()
									bool isLaserShot()
									void applyDate(int year, int month, int day, int hour, int minute)
									void applyEyeSeparation(double distance)
									double getAccTime()
									bool isTeamSwitchEnabled()
									vector<float> getDate()
									float getViewDistance()
									void applyViewDistance(float distance)
									void skipTime(float duration)
									void forceMap(bool show)
									bool requiredVersion(string version)
									void showMap(bool show)
									void showWatch(bool show)
									void showCompass(bool show)
									void showRadio(bool show)
									void showPad(bool show)
									void showWarrant(bool show)
									void showGPS(bool show)
									void EnableRadio(bool state)
									void applyVideoSetting(string option, float setting)
/************************************************************************/

#ifndef GENERAL_UTILITIES_H
#define GENERAL_UTILITIES_H

#include <vector>

#include "position3D.h"
#include "VBS2Fusion.h"



namespace VBS2Fusion
{	
	
	class VBS2FUSION_API GeneralUtilities
	{
	public:
			
		//********************************************************
		// Time Acceleration utilities  
		//********************************************************

		enum WindowState	{
			RTE, MISSION, MP_SELECT_ROLE,MP_SERVER_SELECT_MISSION, MAIN_MENU
		};
		
		/*!

		Set time acceleration coefficient. May be also used to slow time in cutscenes. 
		This command does NOT work in multiplayer.

		*/

		static void setAccTime(double accTime);
			
		/*!
		 returns a list of ints consisting of VBS2 version in the format major minor and release
		 */

		/*!
			returns the VBS2 build version as a vector<int> of ints
		*/
		static vector<int> getVBS2Version();

		/*!
			returns the VBS2 build version as int
		*/
		static int getVBS2BuildVersion();

		/*!
			returns the whether the mission is started or not
		*/
		static bool isMissionRunning();

		/*!
			Returns VBS2 window state.
		*/
		static string getWindowState();

		/*!
			Executes the given plugin's plugin function in accordance with
			the given parameter.
		*/
		static void pluginFunction(string pluginName, string parameter);

		/*!
		returns absolute path of the VBS2 system folders,
		0 - User directory 
		1 - Mission directory 
		2 - Installation directory 
		3 - EXE directory
		*/
		static string getVBS2Directory(int parameter);

		/*!
		returns user directory of VBS2.
		*/
		static string getVBS2UserDirectory();

		/*!
		returns VBS2 mission directory. 
		*/
		static string getVBS2MissionDirectory();

		/*!
		 returns VBS2 installation directory. 
		*/
		static string getVBS2InstallDirectory();

		/*!
		returns VBS2 executable directory. 
		*/
		static string getVBS2EXEDirectory();

		/*!
		Returns aspect ratio as [horizontal,vertical] FOV.
		*/
		static vector<double> getAspectRatio();

		/*!
		Returns screen resolution as [width,height].
		*/
		static vector<double> getScreenResolution();

		/*!
		Returns frames per second for current view. 
		*/
		static vector<double> getFPS();

		/*!
		Returns an array containing the current date & time of the computer running VBS 
		(as opposed to daytime which returns the time as defined in the mission itself). 
		*/
		static vector<double> getSystemTime();

		/*!
		Returns current state of HLA
		*/
		static bool isLVCActive();

		/*!
		Exit the application
		*/
		static void ExitVBS();

		/*!
		Returns information about the current game state
		(e.g. whether a mission is being played, or whether the options window is open).
		*/
		static string applicationState();

		/*!
		returns wheather FusionDev is enabled or not
		*/
		static bool isFusionDeveloper();

		/*!
		returns customerID 
		*/
		static double getCustomerID();

		/*!
		Returns the resolutions supported by the current aspect ratio settings.
		Only works in full-screen mode.
		*/
		static vector<double> getSupportedResolutions();

				
		/*!
		Exports the current map as EMF. 

		filename - File name or absolute path to file. (e.g. "myFile.emf" or "c:\\myFile.emf") If no path is given, file is saved into VBS install folder.
		drawgrid - Draw grid (Optional, default is true)
		drawcontours - Draw contours (Optional, default is true)
		drawtypes - Draw types (Optional, default is all types)
			Roads:1
			Buildings:2
			Vegetation:4
			Water:8
			Trees:16
			Fences:32
			Walls:64
			PowerLines:128.
			Undef:256
		Different draw types can be combined by adding their numbers (e.g. to display roads and buildings, use 3 as drawtype) - only available in V1.23 and higher
		flipvertical: Boolean - Flips the emf upside down (Optional, default is false). By default EMF assumes 0,0 is top left. - only available in V1.23 and higher
		*/
		static void exportEMF(string fileName, bool drawGrid = true, bool drawContours = true, int drawTypes = 256, bool flipVertical = false);

		/*!
		Checks whether an entity of this class exists in the current mission.
		The command takes into account any dynamically added or removed objects.
		*/
		static bool isTypeExists(string type);

		/*!
		Return true if the given variable conforms to the engine limitations (e.g. has to start with letter or underscore, no special characters except for underscore in name).
		Does not test for variables that are the same as command names, and still returns true in that situation, even though those variables shouldn't be used (e.g. isValidVarName "driver" returns true).
		*/
		static bool isValidVariableName(string name);

		/*!
		Returns true if given string is a valid file name (checks for prohibited characters). 
		*/
		static bool isValidFileName(string name);


		/*!
		Opens the supplied URL or file in the associated application (i.e. it may open a browser window for a URL,
		or notepad for a text file).
		If a Windows batch file (or application) is started this way, be aware that the "current directory" is still
		set to the VBS installation path, so any paths used in that batch file will be relative to the installation folder,
		and not the folder the batch file actually resides in.
		*/
		static void openURL(string url);

		/*!
		Disables/enables 3D scene rendering
		*/
		static void DisableRendering(bool disable);

		/*!
		Checks specific difficulty settings of the current user. Difficulty flag names can be found 
		in the server profile file under class Difficulties/xxx/Flags (xxx being regular or veteran).
		*/
		static bool IsDifficultyEnabled(string flagName);

		/*!
		Returns true if the machine is either a server in a multiplayer game or if it is running a singleplayer game.
		*/
		static bool IsServer();

		/*!
		Returns the value of "3D Performance" in OFP Preferences (flashpoint.cfg). 
		This can be used to estimate the computer performance to adapt CPU and GPU demanding settings like view distance 
		dynamically in missions. 
		*/
		static double BenchMark();

		/*!
		Returns true if a title effect is currently active. 
		*/
		static bool isTitleEffectActive();

		/*!
		Returns the network stats as [totalSentBytes,totalSentMsgs,totalRecievedBytes,totalRecievedMsgs]
		*/
		static vector<int> getNetworkStats();

		/*!
		Returns true if player is currently overriding the Driver
		*/
		static bool isDriverOverride();

		/*!
		Causes all loaded plugins in the plugins folder to be requested to be unloaded.
		On this call, its important for all DLLs to respond and to stop any currently executing threads. 
		*/
		static void UnLoadAllPlugins();

		/*!
		Loads all plugins in the plugins folder. 
		*/
		static void LoadAllPlugins();

		/*!
		Return approximate length of the object of given type.
		*/
		static double getLengthOf(string type);

		/*!
		Returns the defined weight of the object of given type.
		*/
		static double getWeightOf(string type);

		/*!
		Returns the volume of the object of given type in m�.
		*/
		static double getVolumeOf (string type);

		/*!
		Returns true if the VBS2 simulation is running in multiplayer mode
		and false if the simulation is running in single player mode. 
		*/
		static bool isMultiPlayer();


		/*!
		Returns true if the VBS2 simulation is running a lasershot compatible version.		
		*/
		static bool isLaserShot();

		/*!
		Sets the actual mission date and time. 
		*/
		static void applyDate(int year, int month, int day, int hour, int minute);

		/*!
		Returns the current time acceleration factor 
		*/
		static double getAccTime();

		/*!
		Check if Team Switch is currently enabled. Team Switch is enabled by default.
		*/
		static bool isTeamSwitchEnabled();

		/*!
		Return the actual mission date and time as an array [year, month, day, hour, minute]. Month is a full number 
		between 1 and 12, day is between 1 and 31, hour is between 0 and 23 and minute is between 0 and 59. 
		*/
		static vector<float> getDate();

		/*!
		Returns the current view distance.
		*/
		static float getViewDistance();

		/*!
		Set rendering distance, in metres. Default is 900m (in OFP) or 1,200m (in ArmA), accepted range is 500m to 
		5,000m (in OFP) or 10,000m (in ArmA). 
		*/
		static void applyViewDistance(float distance);

		/*!
		Jumps the specified number of hours forward or backward.
		The time of day and tides are adjusted, but no changes are made to any units. If present, 
		the lower level of clouds instantly jump to the position they would be in if time had passed normally.
		*/
		static void skipTime(float duration);

		/*!
		Displays the map on the screen during a mission. 
		*/
		static void forceMap(bool show);

		/*!
		Check if version of application is available. If the current version is older than the required one, 
		a warning message is shown and false is returned. Version of format Major.Minor, e.g. 1.30
		*/
		static bool requiredVersion(string version);

		/*!
		Enable and disable radio messages to be heard and shown in the left lower corner of the screen. This command can be helpful during cutscenes.
		*/
		static void EnableRadio(bool state);

		/*!
		Sets the internal engine video options.

		option - Supported options:

		"shadowDistance": maximum shadow distance
		"objectDetail": (0-4) (V1.23+)
		"viewDistance": (10-10000) (V1.23)
		"objectDrawDistance": (0-10000) (V1.23)
		"shadowsCamHeightCoef": Computing the shadows drawing distance from the camera's height above the ground. Draw distance is calculated as height * shadowsCamHeightCoef. (V1.31)
		"screenResolution": Followed by two parameters: width, height. Supported only in fullscreen mode and only for supported resolutions. List of supported resolutions is provided by getSupportedResolutions. (V1.48+) 

		setting - value, depending on option
		*/
		static void applyVideoSetting(string option, float setting);

		/*!
		Enable / disable Team Switch. The default setting is enabled. 
		*/
		static void EnableTeamSwitch(bool enable);
	};
};
#endif
