
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:	WorldUtilities.h

Purpose: utility class for world.

	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			09-07/2010	YFP: Original Implementation
	1.01		20-09/2010	YFP: methods added, getOrigin()
	1.02		03-08-2011	NDB: Added void onMapSignleClick(string command)
								 void onDoubleClick(string Map, string command)
	1.03		22-09-2011	CGS: Added double getDistance(position3D stPos, position3D enPos)
	1.04		23-09/2011	NDB: Added Methods
									double getDeclination()
									void applyDeclination(double declination)
	1.05		26-09/2011	NDB: Added Methods
									vector<string> getShapeFileList()
									ControllableObject getAttachedObject(ControllableObject& co)
									vector<ControllableObject> getAllAttachedObjects(ControllableObject& co)
									string getWorldName()
									void applyOrigin(double easting, double northing, double zone, string hemisphere)
									void switchAllLights(LIGHT_MODE lightMode)
									void initAmbientLife()
	1.06		18-11-2011	SSD: Added Methods
									Unit getNearestUnit(position3D pos)
									Unit getNearestUnit(position3D pos, string type)

/*****************************************************************************/

#ifndef VBS2FUSION_WORLDUTILITES_H
#define VBS2FUSION_WORLDUTILITES_H

/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES
#include <list>
// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "VBS2FusionDefinitions.h"
#include "position2D.h"
#include "data/Vehicle.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBS2Fusion
{
	
	class VBS2FUSION_API WorldUtilities
	{
	public:

		/*!
		Returns cost in the cost map on a given position.
		type can be 0 for vehicles and 1 for unit.
		*/
		static COST_TYPE getPositionCost(position3D pos , int type );

		/*!
		Returns center position of nearest cost field starting the search position specified by pos.
		list of cost types can query the search field. Maximum radius for search specified by radius.

		
		*/
		static position3D getNearestCostPosition(position3D pos,list<COST_TYPE> costTypes,double radius,ControllableObject& co);
		
		/*!
		returns list of network IDs of given types within a radius of a given position.
		*/
		static list<NetworkID> getNearestObjects(position3D pos,list<string> types , int radius);

		/*!
		Get the current Universal Transverse Mercator (UTM) origin of the Map.
		This returns UTM_POSITION. 	If -1 returned for east,north or zone
		*/
		static UTM_POSITION getOrigin();

		/*!
		Creates an indirect damage at the specified position according to the strength of damage given from damage
		and range of indirect damage given from range.
		*/
		static void createIndirectDamage(position3D position, int damage, int range);
		
		/*!
		Creates an indirect damage at the specified position according to the strength of damage given by damage,
		damage range given by range , disable/enable particle effects specified by disableParticles.
		*/
		static void createIndirectDamage(position3D position, int damage, int range, bool disableParticles);

		/*!
		Creates an indirect damage at the specified position according to the strength of damage given by damage,
		damage range given by range , disable/enable particle effects specified by disableParticles ,
		disable/enable creation of crates specified by disableCrates.
		*/
		static void createIndirectDamage(position3D position, int damage, int range, bool disableParticles, bool disableCrates);

		/*!
		Creates an indirect damage at the specified position according to the strength of damage given by damage,
		damage range given by range , disable/enable particle effects specified by disableParticles ,
		disable/enable creation of crates specified by disableCrates and disable/enable damage on all objects by disableDamage.
		*/
		static void createIndirectDamage(position3D position, int damage, int range, bool disableParticles, bool disableCrates, bool disableDamage);

		/*!
		Creates an indirect damage at the specified position according to the strength of damage given by damage,
		damage range given by range , disable/enable particle effects specified by disableParticles ,
		disable/enable creation of crates specified by disableCrates and disable/enable damage on all objects by disableDamage and disable/enable damage effects on units and vehicles.
		*/
		static void createIndirectDamage(position3D position, int damage, int range, bool disableParticles, bool disableCrates, bool disableDamage, bool disableDamageEntities);

		/*!
		Converts position in world space into screen space.
		*/
		static position2D worldToScreen(position3D pos);

		/*!
		Returns the position on VBS world corresponding to the given point on screen. 
		This function ignores any VBS objects and only return a point on VBS terrain.
		*/
		static position3D screenToWorld(position2D pos);
		

		/*!
		Returns the distance in meters between two positions. 
		*/
		static double getDistance(position3D stPos, position3D enPos);
		
		/*!
		Returns the magnetic declination from VBS
		*/
		static double getDeclination();

		/*!
		Sets the magnetic declination in VBS
		*/
		static void applyDeclination(double declination);

		/*!
		Returns the object to which an object or location is attached to. If it is unattached, then objNull is returned.
		*/
		static ControllableObject getAttachedObject(ControllableObject& co);

		/*!
		Returns the object that is attached to this location or object. If nothing is attached, then an empty array is returned.
		*/
		static vector<ControllableObject> getAllAttachedObjects(ControllableObject& co);

		/*!
		Return the class name of the currently loaded map (e.g. "Intro" for Rahmadi). 
		*/
		static string getWorldName();

		/*!
		Sets the origin for LVCGame
		*/
		static void applyOrigin(double easting, double northing, double zone, string hemisphere);

		/*!
		Returns the center position (i.e. X and Z coorinates);; of the currently loaded world. 
		*/
		static vector<float> GetWorldCenter();


		/*!
		Returns the nearest unit object to the given position.
		*/
		static Unit getNearestUnit(position3D pos);

		/*!
		Returns the nearest unit object to the given position of the given type.
		An invalid unit object will be returned if a proper unit type is not passed.
		*/
		static Unit getNearestUnit(position3D pos, string type);

		/*!
		According to the parameters crater will be created and particle effects of an explosion at the location specified 
		and no damage will be done by this function.
		Currently crater type not accommodated with VBS. So use empty string for type.  
		*/
		static ControllableObject createCrater(string type, position3D position, list<Marker> markerList, float randDist, 
			bool asl, float duartion, float size, string effectsType, float effectTTL);


	};

}


#endif //VBS2FUSION_WORLDUTILITES_H