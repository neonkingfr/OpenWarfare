
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/
/*************************************************************************

Name:

	UnitUtilities.h

Purpose:

	This file contains the declaration of the UnitUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			26-03/2009	RMR: Original Implementation
	1.0			26-09/2009	SDS: Update create Unit to support ENUM structure to define unit type

	2.0			10-02/2010  YFP: Added  void CreateUnit(Unit& u,string type,position3D position,string markers,double placementRadius,string specialProperties,string group)
										void setUnitBehaviour(Unit& unit, BEHAVIOUR behavior)
										void commandStop(Unit& u)
										void globalChat(Unit& u,string txtMessage)
										void reloadWeapons(Unit& u)	
	2.01		10-02/2010	MHA: Comments Checked
	2.02		25-02/2010	YFP: Added void applyAlias(Unit& u)
	2.03		24/05/2010  YFP: Added Methods, 
										void performSalute(Unit&, Unit&);
										void setHeight(Unit&, double);
										double getHeight(Unit&);
										void setBMI(Unit&, double);
										double getBMI(Unit&);
	2.04		10-01/2011  CGS: Added CreateUnit(Unit& u,Group& _group)
								 Modified applyEndurance(Unit& u, double endurance)
									doWatch(Unit& unit, position3D targetPosition)
									CreateUnit(Unit& u, position3D position)
									CreateUnit(Unit& u,string strAlias)
									CreateUnit(Unit& u)
									doFire(Unit& firingUnit, position3D pos)
									setUnitBehaviour(Unit& unit, BEHAVIOUR behaviour)
									doWatch(Unit& unit, position3D targetPosition)
									applyExperience(Unit& u, double experience)

	2.05		11-03/2011 YFP: Added Methods
									disableGunnerInput(Unit&,GUNNERINPUT_MODE)
									void applyHeadDirection(Unit&,double,double,double, bool);
	2.06		11-03/2011 SSD: Added Method isPlayerControlled(Unit);
	2.07		07-09/2011 NDB: Added Methods
									double getAerobicFatigue(Unit& unit)
									double getAnaerobicFatigue(Unit& unit)
									double getMoraleLevel(Unit& unit)
									double getMoraleLongTerm(Unit& unit)
									double getTraining(Unit& unit)
									int getUpDegree(Unit& unit)
									void EnablePersonalItems(Unit& unit, bool enable)
									bool isLookingAt(Unit& unit, ControllableObject& co, double angle)
									bool isSpeakable(Unit& unit)
									void allowSpeech(Unit& unit, bool speech)
									void applyAmputation(Unit& unit, BODYPART bodyPart)
									void applyAmputation(Unit& unit, MAIN_BODYPART bodyPart, BODY_SEGMENT segment)
									string getAmputation(Unit& unit, BODYPART bodyPart)
									void addToMorale(Unit& unit, double moralChange)
									void addToAnaerobicFatigue(Unit& unit, double AnaerobicFatigueChange)
									void addToAerobicFatigue(Unit& unit, double AerobicFatigueChange)
									Vehicle getAssignedVehicle(Unit& unit)
									void CreateUnitLocal(string type, position3D& position,Group& group, string initString = "", double skill = 0.5, string rank = "PRIVATE")
	2.08		19-09/2011	NDB: Added Method
									void StopUnit(Unit& unit)
	2.09		20-09/2011	CGS: Added Method
									void reviveUnit(Unit& unit)
	2.10		26-09/2011	NDB: Added Methods
									bool isWearingNVG(Unit& unit)
									bool isPersonalItemsEnabled(Unit& unit)
									string getWatchMode(Unit& unit)
									void applyAnimationSpeed(Unit& unit, double speed)
									double getAnimationSpeed(Unit& unit)
									ControllableObject getLockedTarget(Unit& unit)
									void applyDrawFrustrum(Unit& unit, int mode, double size, Color_RGBA color)
									void applyDrawFrustrum(Unit& unit, int mode, double size)
									DRAWFRUSTRUM getDrawFrustrum(Unit& unit)
									void applyDrawStationary(Unit& unit, int drawType, double maxSize, double growRate, Color_RGBA color, double offset = 0.1)
									void applyDrawStationary(Unit& unit, int drawType, double maxSize = 2, double growRate = 0.03)
									vector<double> getHeadDirection(Unit& unit)
									double getAzimuthHeadDirection(Unit& unit)
									double getElevationHeadDirection(Unit& unit)
									double getRollHeadDirection(Unit& unit)
									void applyLightMode(Unit& unit, int mode)
									bool isDisableAnimationMove(Unit& unit)
									void disableAnimationMove(Unit& unit, bool disable)
									void applyMinAnimationSpeed(Unit& unit, double percent)
									void applyFaceAnimation(Unit& unit, double blink)
									void applyFace(Unit& unit, FACETYPE face)
									void applyMimic(Unit& unit, MIMIC mimic)
									void ignoreRoads(Unit& unit, bool mode)
									void applyUnitCombatMode(Unit& unit, WAYPOINTCOMBATMODE mode)
									void commandWatch(Unit& unit, ControllableObject& target)
									void commandWatch(Unit& unit, position3D& target)
									void doFollow(ControllableObject& co, Group& grp)
									void moveToPos(Unit& unit, position3D& position)
									void glanceAt(Unit& unit, position3D& position)
									void LookAt(Unit& unit, position3D& position)
									void addWeaponToCargo(Unit& unit, string weaponName, int count)
									vector<WEAPONCARGO> getWeaponFromCargo(Unit& unit)
									void addMagazineToCargo(Unit& unit, string magName, int count)
									vector<MAGAZINECARGO> getMagazineFromCargo(Unit& unit)
									vector<string> getMuzzles(Unit& unit)
									position3D getAimingPosition(Unit& unit, ControllableObject& target)
	2.11		26-09/2011	NDB:	Added Method void commandMove(Unit& unit, position3D position)
	2.12		28-09/2011	NDB:  Added Methods,
									void addRating(Unit& unit, double rating)
									double getRating(Unit& unit)
									void addScore(Unit& unit, int score)
									int getScore(Unit& unit)
									void addLiveStats(Unit& unit, double score)
									bool canStand(Unit& unit)
									double getHandsHit(Unit& unit)
									bool isFleeing(Unit& unit)
									void allowFleeing(Unit& unit, double cowardice)
									bool hasSomeAmmo(Unit& unit)
									string getUnitBehaviour(Unit& unit)
									string getUnitCombatMode(Unit& unit)
	2.13		04-11/2011	NDB: Added Methods,
									bool isKindOf(Unit& unit, string typeName)
									void applyCaptive(Unit& unit, bool captive)
									void applyCaptive(Unit& unit, int captiveNum)
									int getCaptiveNumber(Unit& unit)
									float knowsAbout(Unit& unit, ControllableObject& target)
									void sideChat(Unit& unit, string chatText)
									void groupChat(Unit& unit, string chatText)
	2.13		07-11-2011	SSD		void applyName(Unit& unit)
	2.14		21-11-2011	DMB		void stop(Unit& unit, bool stop)


/************************************************************************/

#ifndef VBS2FUSION_UNIT_UTILITIES_H
#define VBS2FUSION_UNIT_UTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>
#include <time.h>

// SIMCENTRIC INCLUDES
#include "position3D.h"
#include "VBS2Fusion.h"
#include "data/Unit.h"
#include "util/ExecutionUtilities.h"
#include "util/ControllableObjectUtilities.h"
#include "VBS2FusionAppContext.h"
#include "data/vbs2Types.h"
#include "data/UnitArmedModule.h"

#include "data/Target.h"


/**********************************************************************/
/* END INCLUDES
/**********************************************************************/

namespace VBS2Fusion
{	

	class VBS2FUSION_API UnitUtilities : public ControllableObjectUtilities
	{
	public:

		//********************************************************
		// DisableAI utilities
		//********************************************************

		/*!
		Disables "MOVE" AI for the unit
		*/
		static void disableAIMOVE(Unit& u);
		
		/*!
		Disables "PATHPLAN" AI for the unit
		*/
		static void disableAIPATHPLAN(Unit& u);

		/*!
		Enables "MOVE" AI for the unit
		*/
		static void enableAIMOVE(Unit& u);
		
		/*!
		Enables "PATHPLAN" AI for the unit
		*/
		static void enableAIPATHPLAN(Unit& u);

		//********************************************************
		// Unit weapon utilities
		//********************************************************
		/*!
		Reload weapons of the unit. Reload happens when the current magazine of the weapon is empty.
		*/
		static void reloadWeapons(Unit& u);


		//********************************************************
		// chat utilities
		//********************************************************

		/*!
		Send the text message over the global radio channel.
		*/
		static void globalChat(Unit& u,string txtMessage );


		//********************************************************
		// Firing utilities
		//********************************************************

		/*!
		Orders a unit to commence firing on the given target (silently). 
		If the target is objNull, the unit is ordered to commence firing on its current target (using doTarget).
		*/
		static void doFire(Unit& firingUnit, ControllableObject& target);

		/*!
		Orders the unit (silently) to fire at the given position. Please note that the 
		position needs to be given in PositionASL format. 
		*/
		static void doFire(Unit& firingUnit, position3D pos);


		//********************************************************
		// unit behavior utilities
		//********************************************************

		/*!
			set the behavior of the unit. 
			  CARELESS,SAFE,AWARE,COMBAT,STEALTH

		*/
		static void setUnitBehaviour(Unit& unit, BEHAVIOUR behaviour);

		//********************************************************
		// Rank utilities
		//********************************************************

		/*!
		Returns the Rank of the unit in string format. 
		*/
		static string getRank(Unit& u);

		/*!
		Updates the rank of the unit. 
		*/
		static void updateRank(Unit& u);

		/*!
		Applies the rank specified by u.Rank() to the unit. 
		*/
		static void applyRank(Unit& u);

		/*!
		Applies the rank specified by rank to the unit. 
		*/
		static void applyRank(Unit& u, string rank);


		//********************************************************
		// Skill utilities
		//********************************************************

		/*!
		Returns the skill level of the unit. 
		*/
		static double getSkill(Unit& u);

		/*!
		Updates the skill level of the unit.  
		*/
		static void updateSkill(Unit& u);

		/*!
		Applies the skill level specified by u.getSkill() to the unit. 
		*/
		static void applySkill(Unit& u);

		/*!
		Applies the skill level specified by skill to the unit. 
		Does not save changes. Use update to verify changes. 
		*/
		static void applySkill(Unit& u, double skill);


		//********************************************************
		// Group utilities
		//********************************************************

		/*!
		Returns the VBS2 name of the group the unit belongs to. 
		*/
		static string getGroup(Unit& u);

		/*!
		Updates the name of the group the unit belongs to. 
		*/
		static void updateGroup(Unit& u);		


		//********************************************************
		// Endurance utilities
		//********************************************************

		/*!
		Returns the endurance of the unit. 
		*/
		static double getEndurance(Unit& u);

		/*!
		Updates the endurance of the unit.  
		*/
		static void updateEndurance(Unit& u);

		/*!
		Applies the endurance specified by u.getEndurance() to the unit. 
		*/
		static void applyEndurance(Unit& u);

		/*!
		Applies the endurance specified by endurance to the unit. 
		Does not save changes. Use update to verify changes. 
		*/
		static void applyEndurance(Unit& u, double endurance);

		//********************************************************
		// Experience utilities
		//********************************************************

		/*!
		Returns the experience level of the unit. 
		*/
		static double getExperience(Unit& u);

		/*!
		Updates the experience level of the unit. 
		*/
		static void updateExperience(Unit& u);

		/*!
		Applies the experience level obtained through u.getExperience to the unit. 
		*/
		static void applyExperience(Unit& u);

		/*!
		Applies the experience level obtained through experience to the unit. 
		Does not save changes. Use update to verify changes. 
		*/
		static void applyExperience(Unit& u, double experience);


		//********************************************************
		// Fatigue utilities
		//********************************************************

		/*!
		Returns the fatigue level of the unit. 
		*/
		static double getFatigue(Unit& u);

		/*!
		Updates the fatigue level of the unit. 
		*/
		static void updateFatigue(Unit& u);

		/*!
		Applies the fatigue level specified by u.getFatigue() to the unit. 
		*/
		static void applyFatigue(Unit& u);

		/*!
		Applies the fatigue level specified by fatigue to the unit. 
		Does not save changes. Use update to verify changes. 
		*/
		static void applyFatigue(Unit& u, double fatigue);



		//********************************************************
		// Leadership utilities
		//********************************************************

		/*!
		Returns the leadership level of the unit. 
		*/
		static double getLeadership(Unit& u);

		/*!
		Updates the leadership level of the unit. 
		*/
		static void updateLeadership(Unit& u);

		/*!
		Applies the leadership level obtained through u.getLeadership to the unit. 
		*/
		static void applyLeadership(Unit& u);

		/*!
		Applies the leadership level obtained through leadership to the unit. 
		Does not save changes. Use update to verify changes. 
		*/
		static void applyLeadership(Unit& u, double leadership);




		//********************************************************
		// Create Unit
		//********************************************************

		/*!
		Creates a new unit. The following parameters are used in the 
		creation process: Type, Position, Initialization Statements, 
		Skill, Rank. If these parameters are not specified, default parameters
		used in the initialization of an Unit object are used. 

		if the group specified does not exist, or if no group is specified, a 
		new group is created to assign to the unit. 
		*/
		static void CreateSoldier(Unit& u, vbs2Type::Soldier::SOLDIER val);
	
		/*!
		Creates a new unit at the specified position. The following parameters are used in the 
		creation process: Type, Initialization Statements, 
		Skill, Rank. If these parameters are not specified, default parameters
		used in the initialization of an Unit object are used. 

		if the group specified does not exist, or if no group is specified, a 
		new group is created to assign to the unit. 
		*/
		static void CreateSoldier(Unit& u, vbs2Type::Soldier::SOLDIER val, position3D position);

		/*!
		Creates a new unit at the specified position. The following parameters are used in the 
		creation process: Type, Initialization Statements, 
		Skill, Rank. If these parameters are not specified, default parameters
		used in the initialization of an Unit object are used. 

		if the group specified does not exist, or if no group is specified, a 
		new group is created to assign to the unit. 
		*/
		static void CreateSoldier(Unit& u, vbs2Type::Soldier::SOLDIER val, string alias);

		/*!
		Creates a new unit at the specified position. The following parameters are used in the 
		creation process: Type, Initialization Statements, 
		Skill, Rank. If these parameters are not specified, default parameters
		used in the initialization of an Unit object are used. 

		if the group specified does not exist, or if no group is specified, a 
		new group is created to assign to the unit. 
		*/
		static void CreateSoldier(Unit& u, vbs2Type::Soldier::SOLDIER val, position3D position, string alias);


		/*!
		Creates a new unit at the specified position. The following parameters are used in the 
		creation process: Type, Initialization Statements, 
		Skill, Rank. If these parameters are not specified, default parameters
		used in the initialization of an Unit object are used. 

		if the group specified does not exist, or if no group is specified, a 
		new group is created to assign to the unit. 
		*/
		static void CreateSoldier(Unit& u, vbs2Type::Soldier::SOLDIER val, string strAlias, position3D position, string initStatements, double skill, string rank, string group);


		/*!
		Creates a new unit. The following parameters are used in the 
		creation process: Type, Position, Initialization Statements, 
		Skill, Rank. If these parameters are not specified, default parameters
		used in the initialization of an Unit object are used. 

		if the group specified does not exist, or if no group is specified, a 
		new group is created to assign to the unit. 
		*/
		static void CreateUnit(Unit& u);

		/*!
		Creates a new unit at the specified position. The following parameters are used in the 
		creation process: Type, Initialization Statements, 
		Skill, Rank. If these parameters are not specified, default parameters
		used in the initialization of an Unit object are used. 

		if the group specified does not exist, or if no group is specified, a 
		new group is created to assign to the unit. 
		*/

		static void CreateUnit(Unit& u, position3D position);

		/*!
		Creates a new unit with the specified alias. The following parameters are used in the 
		creation process: Type, Initialization Statements, Position
		Skill, Rank. If these parameters are not specified, default parameters
		used in the initialization of an Unit object are used. 
		
		if the group specified does not exist, or if no group is specified, a 
		new group is created to assign to the unit. 
		*/
		static void CreateUnit(Unit& u, string strAlias);

		/*!
		Creates a new unit with the specified alias at position. The following parameters are used in the 
		creation process: Type, Initialization Statements, 
		Skill, Rank. If these parameters are not specified, default parameters
		used in the initialization of an Unit object are used. 

		if the group specified does not exist, or if no group is specified, a 
		new group is created to assign to the unit. 
		*/

		static void CreateUnit(Unit& u, position3D position, string strAlias);

		/*!
		Creates a new unit of type with the specified alias. The following parameters are used in the 
		creation process: Type, Initialization Statements, position, 
		Skill, Rank. If these parameters are not specified, default parameters
		used in the initialization of an Unit object are used. 
		
		if the group specified does not exist, or if no group is specified, a 
		new group is created to assign to the unit. 
		*/
		static void CreateUnit(Unit& u, string strAlias, string type);

		/*!
		Creates a new unit using the parameters pass in. 

		if the group specified does not exist, or if no group is specified, a 
		new group is created to assign to the unit. 
		*/

		static void CreateUnit(Unit& u, string strAlias, string type, position3D position, string initStatements, double skill, string rank, string group);

		/*!
		Creates a new unit using the parameters pass in. 

		if the group specified does not exist, or if no group is specified, a 
		new group is created to assign to the unit. 
		*/
		static void CreateUnit(	Unit& u,string type,position3D position,string markers,double placementRadius,string specialProperties,string group);


		static void UnitUtilities::CreateUnit(Unit& u,Group& _group);
 


		//********************************************************
		// Update Utilities
		//********************************************************

		/*!
		Updates the static properties (i.e. rarely changing) of the unit: 
		Endurance, Experience, Group, Leadership, Rank, Skill and Type.  
		*/
		static void updateStaticProperties(Unit& u);

		/*!
		Updates the dynamic properties of the unit: 
		fatigue, damage, direction, position, isAlive, isPlayer, positionASL 	
		*/
		static void updateDynamicProperties(Unit& u);

		//********************************************************
		// Group existence check Utility
		//********************************************************		 

		/*!
		Returns true if a group with the name/alias exists. 
		*/

		static bool groupExists(string groupName);

		//********************************************************
		// Group creation Utility
		//********************************************************

		/*!
		Creates a new group with the name/alias specified. Default side of "WEST" is used. 
		*/
		static void createGroup(string groupName, SIDE side = WEST);

		//********************************************************
		// Movement Utility
		//********************************************************

		/*!
		order unit to stop. 
		*/
		static void commandStop(Unit& u);
		
		/*!
		Orders the unit to move to movePosition. 
		*/
		static void doMove(Unit& u, position3D movePosition);

		//********************************************************
		// forceSpeed Utility
		//********************************************************

		/*!
		Forces the speed of the unit. 
		*/
		//static void forceSpeed(Unit& unit, double forceSpeedVal);			

		//********************************************************
		// Targeting utilities
		//********************************************************

		/*!
		Orders the unit to target the given target (silently).
		*/
		static void doTarget(Unit& unit, ControllableObject& co);

		//********************************************************
		// Watch utilities
		//********************************************************

		/*!
		Orders the unit to watch the given position (format position3D) (silently).
		*/
		static void doWatch(Unit& unit, position3D targetPosition);


		//********************************************************
		// UnitPos (i.e. UP, DOWN or AUTO) utilities
		//********************************************************

		/*!
		Returns the units current position in UNITPOS (i.e AUTO, UP or DOWN) format. 
		*/
		static UNITPOS getUnitPos(Unit& u);

		/*!
		Updates the units Position property. 
		*/
		static void updateUnitPos(Unit& u);

		/*!
		Applies the UNITPOS defined by pos to the Unit in the game environment. Does not update the 
		UnitPos property of the unit. Use updateUnitPos to reload and check for successful execution. 
		*/
		static void applyUnitPos(Unit& u, UNITPOS pos);

		/*!
		Applies the UNITPOS defined by u.getUnitPos() to the Unit in the game environment. Does not update the 
		UnitPos property of the unit. Use updateUnitPos to reload and check for successful execution. 
		*/
		static void applyUnitPos(Unit& u);

		/*!
		Applies the AUTO position to the unit within the game. Does not update the 
		UnitPos property of the unit. Use updateUnitPos to reload and check for successful execution. 
		*/
		static void unitPosAUTO(Unit& u);

		/*!
		Applies the DOWN position to the unit within the game. Does not update the 
		UnitPos property of the unit. Use updateUnitPos to reload and check for successful execution. 
		*/
		static void unitPosDOWN(Unit& u);

		/*!
		Applies the UP position to the unit within the game. Does not update the 
		UnitPos property of the unit. Use updateUnitPos to reload and check for successful execution. 
		*/
		static void unitPosUP(Unit& u);

		/*!
		Applies the alias of the unit in VBS2 Environment.	
		*/
		static void applyAlias(Unit& u);

		/*!
		Enable/Disable precision movement mode for specified unit. 
		*/
		static void setUnitExactMovementMode(Unit& u, bool precisionMode);

		/*!
		Return precision movement mode for specified unit.
		*/
		static bool getUnitExactMovementMode(Unit& u);

		/*!
		Delete a Unit from VBS2 Environment. 
		note - Only player unit will be visible after delete.
		*/
		static void deleteUnit(Unit& u);

		/*!
		Causes a unit to salute to a specified target unit.
		*/
		static void performSalute(Unit& unit, Unit& targetUnit);

		/*!
		set body height of the unit. height should be in meters.
		*/
		static void setHeight(Unit& u , double height);

		/*!
		get body height of the specified unit. 
		*/
		static double getHeight(Unit& u);

		/*!
		set Body Mass Index (BMI) of the unit. 
		*/
		static void setBMI(Unit& u, double index );

		/*!
		get Body Mass Index (BMI) of the unit.
		*/
		static double getBMI(Unit& u);

		/*!
		Disable gunner input of the unit.
		input Modes are
			NOTHING         - Enable disabled modes.
			WEAPON			- Disable weapon.
			HEAD			- Disable head.
			WEAPONANDHEAD	- Disable weapon and head.
		*/
		static void disableGunnerInput(Unit& unit,GUNNERINPUT_MODE mode);


		/*!
		Set the direction of the unit's head.
		Directions are relative to the unit's direction. 
		*/
		static void applyHeadDirection(Unit& unit, double azimuth, double elevation,double roll, bool transition = true);

		/*!
		Returns the name of the Unit's primary weapon (an empty string if there is none).	
		*/
		static string getPrimaryWeapon(Unit& u);

		/*!
		Updates the primary weapon of the Unit. 
		*/
		static void updatePrimaryWeapon(Unit& u);

		/*
		Updates the list of weapon types assigned to the unit. 
		Each weapon is assigned with ammo value of 0. 

		Note: This function does not update the ammo amounts on the weapons magazines. 
		Use updateWeaponAmmo function to update the weapon types with ammo.
		*/
		static void updateWeaponTypes(Unit& u);

		/*!
		Select the Weapon given by the muzzle Name.
		*/
		static void selectWeapon(Unit& u, string muzzleName);

		/*
		Updates the amount of ammo left in the primary magazine of each assigned 
		weapon type. Uses the list defined by the weapons list within UnitArmedModule, 
		and therefore will only load ammo information for each weapon on that list. 
		Should be used after updateWeaponTypes is called. 	
		*/
		static void updateWeaponAmmo(Unit& u);

		/*!
		Add magazine to the Unit.
		*/
		static void addMagazine(Unit& u, string name);

		/*!
		Count number of magazines available in given unit object.
		*/
		static int countMagazines(Unit& u);

		/*!
		Updates the list of magazines assigned to a unit's in Armed Module. This function loads all 
		magazines assigned to the unit from VBS2 and adds a new Magazine object to Magazine list.

		Note: This function does not update the ammo amount within magazine list. 
		So within MagazineList ammo count would be zero.Use updateMagazineAmmo to update this list.
		*/
		static void updateMagazineTypes(Unit& u);

		/*!
		Updates the magazine ammo count & magazine name inside the unitArmedModule's MagazineList
		therefore update the magazine list with required details. This function is enough to update
		MagazineList correctly. No need to use updateMagazineTypes function before using this function.
		*/
		static void updateMagazineAmmo(Unit& u);
		
		/*!
		Returns the Weapon Object with Current weapon details.	
		*/
		static Weapon getCurrentWeapon(Unit& u);
		
		/*!
		Gets the Weapon Object from getCurrentWeapon function & update the Weapon list.	
		*/
		static void updateCurrentWeapon(Unit& u);

		//----------------------------------------------------------------------------
		/*!
		Gets the pistol weapon name.	
		*/
		static string getPistolWeapon(Unit& u); 

		/*!
		Returns the weapons position in model space by passing Weapon Name.	
		*/
		static position3D getWeaponPosition(Unit& unit, string weaponName);

		/*!
		Returns the weapons position in model space by passing weapon object. Weapon should have correct name.
		getWeaponPosition(Unit& unit, string weaponName) function will be used within this function.
		*/
		static position3D getWeaponPosition(Unit& unit, Weapon& weapon);

		/*!
		Returns the weapon firing point for a selected muzzle in model space. Muzzle name needs to be passed.	
		*/
		static position3D getWeaponPoint(Unit& unit, string muzzleName);

		/*!
		Returns the weapon firing point in model space by passing Weapon object. Weapon should have correct 
		muzzle name. getWeaponPoint(Unit& unit, string muzzleName) function will be used within this function.
		*/
		static position3D getWeaponPoint(Unit& unit, Weapon& weapon);

		/*!
		Returns the direction that the unit Weapon is aiming in. weapon name is passed.	
		*/
		static position3D getWeaponDirection(Unit& unit, string weaponName);

		/*!
		Returns the direction that the unit Weapon is aiming in. Weapon object should have correct weapon name.
		getWeaponDirection(Unit& unit, string weaponName) function will be used within this function.
		*/
		static position3D getWeaponDirection(Unit& unit, Weapon& weapon);

		/*! 
		Get the elevation of the weapon in unit. 
		*/
		static double getWeaponElevation(Unit& unit, string weaponName);

		/*! 
		Get the elevation of the weapon in unit. Weapon object should have correct weapon name.
		This function uses getWeaponElevation(Unit& unit, string weaponName).
		*/
		static double getWeaponElevation(Unit& unit, Weapon& weapon);

		/*! 
		Get the Azimuth of the unit's weapon in VBS environment. Azimuth value will not be relative to 
		the unit's body direction.
		*/		
		static double getWeaponAzimuth(Unit& unit, string weaponName);

		/*! 
		Get the Azimuth of the unit's weapon in VBS environment. Azimuth value will not be relative to 
		the unit's body direction. This function uses getWeaponAzimuth(Unit& unit, string weaponName).
		So Weapon object should have correct weapon name.
		*/		
		static double getWeaponAzimuth(Unit& unit, Weapon& weapon);
		
		/*!
		Returns the list of objects the weapon is aiming at.
		*/
		static list<ControllableObject> getWeaponAimingTargets(Unit& unit, string weaponName);

		/*!
		Returns the list of objects the weapon is aiming at. Weapon object should have correct weapon name.
		*/
		static list<ControllableObject> getWeaponAimingTargets(Unit& unit, Weapon& weapon);

		/*!
		Remove the one specified magazine from the unit.	
		*/
		static void removeMagazine(Unit& unit, string name);

		/*!
		Remove all specified magazines from the unit.	
		*/
		static void removeallMagazines(Unit& unit, string name);

		/*!
		Add Weapon to the Unit. weapon slots are filled, any further addWeapon commands are ignored. 
		To ensure that the weapon is loaded at the start of the mission, at least one magazine should 
		be added, before adding threse weapon.
		*/
		static void addWeapon(Unit& unit, string name);

		/*!
		Remove the specified Weapon from the unit.	
		*/
		static void removeWeapon(Unit& unit, string name);

		/*!
		Tells if a unit has the given weapon.	
		*/
		static bool hasWeapon(Unit& unit, string name);
		
		/*!
		Sets the direction of the Weapon according to azimuth (relative to the unit's body direction)& elevation. 
		Must use the disableGunnerInput function before using this function.
		*/
		static void setWeaponDirection(Unit& unit, double azimuth, double elevation, bool transition);

		/*!
		Returns how much ammunition (compared to a full state defined by the unit type) the unit has. 
		Value would be between 0 to 1.	
		*/
		static float getAmmoRatio(Unit& unit);

		/*!
		Sets how much ammunition (compared to a full state defined by the unit type) the unit has. 
		The value ranges from 0 to 1.	
		*/
		static void setVehicleAmmo(Unit& unit, float ammovalue);

		/*!
		Returns the recoil impulse vectors of a soldier's weapon.Value is only available the moment 
		the weapon is actually firing	
		*/
		static vector3D getRecoilImpulse(Unit& unit);

		/*!
		Remove all weapons and magazines from the unit.
		*/
		static void removeallWeapons(Unit& unit);

		/*!
		Enables or Disables firing on current weapon. If true, the unit's current weapon cannot be fired.
		*/
		static void setWeaponSafety(Unit& unit, bool safe);

		/*!
		Return the current status of the weapon safety.
		*/
		static bool getWeaponSafety(Unit& unit);

		/*!
		Updates all the Armed properties of the unit:   
		*/
		static void updateUnitArmedProperties(Unit& unit);

		/*!
		Returns the hiding Position.
		If enemy is null it is some position in front of the object or enemy position
		unit - The unit need to hide
		enemy - The enemy unit
		*/
		static position3D getHideFrom(Unit& unit, Unit& enemy);

		/*!
		Returns a list of targets within the defined range.
		"Targets" are not restricted to enemy units.
		*/
		static vector<Target> nearTargets(Unit& unit, int range);
	
		/*!
		Returns a list of targets within the defined range.
		"Targets" are not restricted to enemy units. 
		*/
		static string* nearTarget(Unit& unit, int range);	

		/*!
		Returns true if the given object is controlled by a player somewhere on the network.
		*/		
		static bool isPlayerControlled(Unit u);

		/*!
		Returns the aerobic fatigue level of the soldier. 
		*/
		static double getAerobicFatigue(Unit& unit);

		/*!
		Returns the anaerobic fatigue level of the soldier. 
		*/
		static double getAnaerobicFatigue(Unit& unit);

		/*!
		Returns the morale level of the unit [0-1]
		*/
		static double getMoraleLevel(Unit& unit);

		/*!
		Returns the long-term (1 minute average) morale level of the unit [0-1].
		*/
		static double getMoraleLongTerm(Unit& unit);

		/*!
		Returns the training rating of the soldier. When used on a vehicle, it will return -1.
		*/
		static double getTraining(Unit& unit);

		/*!
		Returns the "up degree" of the soldier. This indicates whether the unit is prone, kneeling, or standing, as well as what weapon type is being used.
		Below is a list of stances and their return values:

		rifle, lowered - standing/sitting: 10
		rifle, raised - standing: 8
		rifle - crouched: 6  
		rifle - prone: 4

		pistol - standing: 9 
		pistol - crouched: 7  
		pistol - prone: 5 

		launcher - standing: 1
		launcher - crouched: 1 
		launcher - prone: 3

		unarmed - standing/sitting: 12
		unarmed - prone: 3

		vehicles / static objects / non people: -1
		*/
		static int getUpDegree(Unit& unit);

		/*!
		Enables personal items. Used for units inside vehicles to enable their personal equipment.
		*/
		static void EnablePersonalItems(Unit& unit, bool enable);

		/*!
		Returns true if the first object is looking into the direction of the second object 
		(within the angle specified). Whether there are objects between the "looker" and the 
		target object that might be obstructing the view is not taken into consideration.

		unit - Unit that is looking
		co - Object that is being looked at
		angle - Angle within which the target has to be.
		Angle is considered in both directions - i.e. if the object is 29� left or right of the object, an angle definition of 30 will get either.
		*/
		static bool isLookingAt(Unit& unit, ControllableObject& co, double angle);

		/*!
		Returns whether a unit is able to audibly speak or not.
		*/
		static bool isSpeakable(Unit& unit);
		/*!
		Turns the ability of a unit to audibly speak on or off.

		Does not affect orders from being issues or received.
		Units will still scream when being shot at.
		*/
		static void allowSpeech(Unit& unit, bool speech);

		/*!
		It is for VBS 1.50
		In VBS2 1.50 amputations are present for all US Army and USMC soldiers
		Removes a person's body parts.

		Proportional to the removed part there will also be blood stains applied to the unit.

		Arms and legs can be removed in two stages.
		Possible values for the removed body parts are:

		"Arm_Left_Half": Remove left hand
		"Arm_Right_Half": Remove right hand
		"Leg_Left_Half": Remove lower left leg
		"Leg_Right_Half": Remove lower right leg
		"Head": Remove head
		"Arm_Left_Full": Remove left arm
		"Arm_Right_Full": Remove right arm
		"Leg_Left_Full": Remove left leg
		"Leg_Right_Full": Remove right leg
		"Head_Reset": Re-attach head
		"Arm_Left_Reset": Re-attach left arm
		"Arm_Right_Reset": Re-attach right arm
		"Leg_Left_Reset": Re-attach left leg
		"Leg_Right_Reset": Re-attach right leg 

		Removal of body parts has no effect on the health status of that unit (i.e. a person without a head is still alive, and a person without legs can still walk), 
		so it's up to the mission designer to ensure that these effects are applied in a realistic fashion.

		unit - 
		bodypart - Part to be removed.
		*/
		static void applyAmputation(Unit& unit, BODYPART bodyPart);

		/*!
		It is for VBS 1.40
		It applicable for the US Army rifleman unit.
		unit - 
		bodypart - Main body part. Can be "head", "leftarm", "rightarm", "leftleg" or "rightleg"
		segment - Segment of specified body part. Can be "head", "lefthand", "leftforearm", "righthand", "rightforearm", "leftfoot", "leftuplegroll", "rightfoot", "rightuplegroll"
		*/
		static void applyAmputation(Unit& unit, MAIN_BODYPART bodyPart, BODY_SEGMENT segment);

		/*!
		Gets person limb amputation state
		*/
		static string getAmputation(Unit& unit, BODYPART bodyPart);


		/*!
		Adds the passed value to the unit's morale (morale is between 0=exhausted and 1=fit). 
		moralChange - It is the adding value. use (-)ve to reduce the morale
		*/
		static void addToMorale(Unit& unit, double moralChange);

		/*!
		Adds the passed value to the unit's anaerobic fatigue (fatigue is between 0 and 1). 
		moralChange - It is the adding value. use (-)ve to reduce the morale
		*/
		static void addToAnaerobicFatigue(Unit& unit, double AnaerobicFatigueChange);

		/*!
		Adds the passed value to the unit's aerobic fatigue (fatigue is between 0 and 1). 
		moralChange - It is the adding value. use (-)ve to reduce the morale
		*/
		static void addToAerobicFatigue(Unit& unit, double AerobicFatigueChange);

		/*!
		Return the vehicle a unit is assigned to.
		If no vehicle is assigned objNull is returned. 
		*/
		static Vehicle getAssignedVehicle(Unit& unit);

		/*!
		Creates a unit of the given type. Unit is only visible on the local machine.
		*/
		static void CreateUnitLocal(string type, position3D& position,Group& group, string initString = "", double skill = 0.5, string rank = "PRIVATE");

	 /*!
	 Order the given unit(s) to stop (without radio messages).
	 DoStop'ed units leave the groups formation. It will prevent the unit from moving around with their group 
	 (or formation leader), while still beeing able to turn around and even move to a new position if they see fit. 
	 They will still respond to orders from their group leader (like engage, rearm, board a vehicle), but all of their 
	 actions will be seperate from the group formation (unless ordered to return, which AI leaders don't do unless a script tells them to).
	 */
	 static void StopUnit(Unit& unit);

	 /*!
	 Returns true if the given unit is currently wearing NVG equipment.
	 */
	 static bool isWearingNVG(Unit& unit);

	 /*!
	 Returns true if personal items are enabled for the unit 
	 */
	 static bool isPersonalItemsEnabled(Unit& unit);

	 /*!
	 Returns the watch mode of the given unit.
	 "NO" if nothing (or a position) is being watched, "TGT" if watching target
	 */
	 static string getWatchMode(Unit& unit);
	
	 /*!
	 Set the speed multiplier that defines the speed at which an animation is seen in game. 
	 Changes between anim states remains the same, i.e. the unit continues to be able to change 
	 from walking to running and so forth. Note that this also depends on the setMinAnimationSpeed command, if set.
	 */
	 static void applyAnimationSpeed(Unit& unit, double speed);

	 /*!
	 Returns the speed multiplier that defines the speed at which an animation is seen in game. 
	 */
	 static double getAnimationSpeed(Unit& unit);

	 /*!
	 Draws a visualization in the editor, indicating a units view cone.
	 unit - Unit to draw frustum for
	 mode - Where to draw indicator: 0:none, 1:2D map, 2:3D map, 3:both
	 size - Size of cone in meters.
	 color - Color of cone. (Optional)
	 */
	 static void applyDrawFrustrum(Unit& unit, int mode, double size, Color_RGBA color);

	 /*!
	 Draws a visualization in the editor, indicating a units view cone.
	 unit - Unit to draw frustum for
	 mode - Where to draw indicator: 0:none, 1:2D map, 2:3D map, 3:both
	 size - Size of cone in meters.
	 */
	 static void applyDrawFrustrum(Unit& unit, int mode, double size);

	 /*!
	 Returns the current drawFrustum setting for a unit.
	 (returns [0,100,[0,0,1,0.1]] if not defined)
	 */
	 static DRAWFRUSTRUM getDrawFrustrum(Unit& unit);

	 /*!
	 Draws ink-spots in AAR to indicate the time a unit is stationary.

	 unit: Object - Unit for which inkspot is being defined.
	 drawType: Number - Type of marker to draw, options can be added together ex: ( 1 + 2 + 4 ). Possible values are: 
	 0: no drawing
	 +1: 2D drawing ON
	 +2: 3D drawing ON (disc by default)
	 +4: switch 3D drawing to a cloud (instead of disc, 2 must be set to be visible)

	 maxSize - Maximum size of the mark in meters 
	 growRate - Growth rate of the mark in meters per second
	 color - 2D and 3D Marker color 
	 offset - Offsets the default 3D disc representation above ground by the given value in meters (optional, default: 0.1)
	 */
	 static void applyDrawStationary(Unit& unit, int drawType, double maxSize, double growRate, Color_RGBA color, double offset = 0.1);

	 /*!
	 Draws ink-spots in AAR to indicate the time a unit is stationary.

	 unit: Object - Unit for which inkspot is being defined.
	 drawType: Number - Type of marker to draw, options can be added together ex: ( 1 + 2 + 4 ). Possible values are: 
	 0: no drawing
	 +1: 2D drawing ON
	 +2: 3D drawing ON (disc by default)
	 +4: switch 3D drawing to a cloud (instead of disc, 2 must be set to be visible)

	 maxSize - Maximum size of the mark in meters (optional, default: 2)
	 growRate - Growth rate of the mark in meters per second (optional, default: .03)
	 */
	 static void applyDrawStationary(Unit& unit, int drawType, double maxSize = 2, double growRate = 0.03);

	 /*!
	 Returns the direction of a unit's head. 
	 returns array has [Azimuth, Elevation, Roll] of unit's head 
	 */
	 static vector<double> getHeadDirection(Unit& unit);

	 /*!
	 returns Azimuth of unit's head 
	 */
	 static double getAzimuthHeadDirection(Unit& unit);

	 /*!
	 returns Elevation of unit's head 
	 */
	 static double getElevationHeadDirection(Unit& unit);

	 /*!
	 returns Roll of unit's head 
	 */
	 static double getRollHeadDirection(Unit& unit);

	 /*!
	 Checks whether movement has been disabled for a unit. 
	 */
	 static bool isDisableAnimationMove(Unit& unit);

	 /*!
	 Sets the minimum control input that will be used for movement speed.

	 When using a joystick or the setAction command, movement speed is analog (meaning, it can be in a range from no movement to full movement speed). This command puts a minimum on that analog range, so that the input can not be lower than the specified percent.
	 When using a keyboard, control input is always 100%, so this command is not relevant in this situation. 

	 percent - Number, 0 to 1; the minimum analog input that will be accepted for movement.
	 */
	 static void applyMinAnimationSpeed(Unit& unit, double percent);

	 /*!
	 Set facial animation phase (eye blinking), blink is in the range from 0 to 1. 
	 */
	 static void applyFaceAnimation(Unit& unit, double blink);

	 /*!
	 Set person's face. 
	 */	
	 static void applyFace(Unit& unit, FACETYPE face);

	 /*!
	 Set person's facial expression
	 */
	 static void applyMimic(Unit& unit, MIMIC mimic);

	 /*!
	 Revives an Unit immediately, preserving his name & equipment.
	 */
	 static void reviveUnit(Unit& unit);

	 /*!
	 Sets unit combat mode (engagement rules). Does not work for groups.

	 Mode may be one of:
	 "BLUE" (Never fire)
	 "GREEN" (Hold fire - defend only)
	 "WHITE" (Hold fire, engage at will)
	 "YELLOW" (Open fire)
	 "RED" (Open fire, engage at will) 

	 returns True if combat mode was applied sucessfuly, otherwise false. 
	 */
	 static void applyUnitCombatMode(Unit& unit, WAYPOINTCOMBATMODE mode);

	 /*!
	 Orders the unit to watch the given target (via the radio). 
	 Use objNull as the target to order a unit to stop watching a target 
	 */
	 static void commandWatch(Unit& unit, ControllableObject& target);

	 /*!
	 Orders the unit to watch the given position (via the radio) 
	 */
	 static void commandWatch(Unit& unit, position3D& target);

	 /*!
	 Control what the unit is glancing at (target or Position). How frequently the unit is glancing there depends on behaviour. 
	 */
	 static void glanceAt(Unit& unit, position3D& position);

	 /*!
	 Control what the unit is looking at position.
	 */
	 static void LookAt(Unit& unit, position3D& position);

		/*!
		Add weapons to the cargo space of objects, which can be taken out by infantry units. 
		Once the weapon cargo space is filled up, any further addWeaponCargo commands are ignored.
		*/
		static void addWeaponToCargo(Unit& unit, string weaponName, int count);

		/*!
		Returns the weapons in cargo of the object. 
		It is added by addWeaponToCargo() method
		*/
		static vector<WEAPONCARGO> getWeaponFromCargo(Unit& unit);

		/*!
		Add magazines to the cargo space of Objects, which can be taken out by infantry units. 
		Once the magazine cargo space is filled up, any further addMagazineCargo commands are ignored.
		*/
		static void addMagazineToCargo(Unit& unit, string magName, int count);

		/*!
		Returns the magazines in cargo of the object. 
		It is added via addMagazineToCargo()
		*/
		static vector<MAGAZINECARGO> getMagazineFromCargo(Unit& unit);

		/*!
		Return all the muzzles of a person
		*/
		static vector<string> getMuzzles(Unit& unit);

		
		/*!
		Returns the position a shooter has to aim at to hit a target with the current selected weapon.
		*/
		static position3D getAimingPosition(Unit& unit, ControllableObject& target);

		/*!
		It is only for local objects
		Order the given unit(s) to move to the given location (via the radio). 
		Exactly the same as doMove, except this command displays a radio message. 
		*/
		static void commandMove(Unit& unit, position3D position);

		/*!
		Add a number to the rating of a unit. Negative values can be used to reduce the rating. This command is 
		usually used to reward for completed mission objectives. The rating is given at the end of the mission 
		and is automatically adjusted when killing enemies or friendlies. When the rating gets below zero, the 
		unit is considered "renegade" and is an enemy to everyone.
		*/
		static void addRating(Unit& unit, double rating);

		/*!
		Check unit rating. Rating is increased for killing enemies, decreased for killing friendlies. Can be changed 
		via addRating by the mission designer. The rating of the player is displayed as the "score" at the end of the 
		mission
		*/
		static double getRating(Unit& unit);

		/*!
		Add a number to the score of a unit. This score is shown in multiplayer in the "I" screen. Negative values will remove from the score
		*/
		static void addScore(Unit& unit, int score);

		/*!
		Returns the person's score. 
		*/
		static int getScore(Unit& unit);

		/*!
		Returns if the given soldier is able to stand up. 
		*/
		static bool canStand(Unit& unit);

		/*!
		Checks if a soldier's hands are hit, which results in inaccurate aiming. 
		If the hands aren't damaged, the returned value is 0. If the hands are hit or the unit is dead, the returned value is 1. 
		*/
		static double getHandsHit(Unit& unit);

		/*!
		Check if unit has some ammo. 
		*/
		static bool hasSomeAmmo(Unit& unit);

		/*!
		Checks whether the object is (a subtype) of the given type.
		Command works with classes contained in CfgVehicles or CfgAmmo, but not with others (e.g. CfgMagazines or CfgWeapons).
		*/
		static bool isKindOf(Unit& unit, string typeName);

		/*!
		Mark a unit as captive.

		If unit is a vehicle, commander is marked.

		A captive is neutral to everyone, and will not trigger "detected by" conditions for its original side. Using a number instead of a boolean has no further effect on the engine's behavior, but can be used by scripting commands to keep track of the captivity status at a finer resolution (e.g. handcuffed, grouped, etc.)

		The syntax with numbers was introduced in VBS2 v1.19. Boolean syntax existed since v1.0
		*/
		static void applyCaptive(Unit& unit, bool captive);

		/*!
		Mark a unit as captive.

		If unit is a vehicle, commander is marked.

		A captive is neutral to everyone, and will not trigger "detected by" conditions for its original side. Using a number instead of a boolean has no further effect on the engine's behavior, but can be used by scripting commands to keep track of the captivity status at a finer resolution (e.g. handcuffed, grouped, etc.)

		The syntax with numbers was introduced in VBS2 v1.19. Boolean syntax existed since v1.0

		With V1.50+ more captive numbers have become available:

		2 - Capture: Unit receives action to be captured, i.e. to become part of the player's group
		4 - Release: Unit receives action to be released, i.e. to leave the player's group. If the AI was part of a different group before capture, he will not rejoin that group upon release.
		8 - Cuff: Unit receives action to be handcuffed. Adults will enter an animation that shows their hands on their backs, children will not display any special animation.
		16 - Uncuff: Unit receives action to be uncuffed. 

		Combinations of these modes are possible, by adding the base numbers:

		3 - Unit set captive & Capture action available (mode 1 + 2)
		10 - Capture & cuff actions available (mode 2 + 8)
		12 - Release & cuff actions available (mode 4 + 8)
		18 - Capture & uncuff actions available (mode 2 + 16)
		20 - Release & uncuff actions available (mode 4 + 16) 

		Once an action is activated, the captive value may change automatically, i.e. after the "Capture" action is executed the captiveNum value for that unit changes to 4, to indicate that he can now be released.
		*/
		static void applyCaptive(Unit& unit, int captiveNum);

		/*!
		Returns a unit's captivity status, as defined by setCaptive. 
		If a unit's captivity level was set as a Boolean, then the returned number is either 0 (for false) 
		or 1 (for true).
		*/
		static int getCaptiveNumber(Unit& unit);

		/*!
		Check if (and by how much) unit knows about target.
		And returns a number from 0 to 4.
		*/
		static float knowsAbout(Unit& unit, ControllableObject& target);

		/*!
		Types text to the side radio channel.
		*/
		static void sideChat(Unit& unit, string chatText);

		/*!
		Make a unit send a text message over the group radio channel. 
		*/
		static void groupChat(Unit& unit, string chatText);

		/*!
		Applies the name of the unit to the VBS2 Environment.
		Prior to applying function, verifies that the name is a valid name
		according to following criteria.
		- Name has to start with letter or underscore.
		- Only special characters allowed is underscore.
		- Name should not contain spaces in between.
		- Name should not already exist in the mission.
		*/
		static void applyName(Unit& unit);


		/*!
		Add an action to an object. The action is usable by the object itself, as well as anyone who is within proximity of the object and is looking at it.

		If an action is added to a unit, the action will only be visible when the unit is on foot. If the unit enters a vehicle, the action will not be visible, not to the unit, or to anyone in / near his vehicle.
		unit - Object to assign the action to
		title - The action name which is displayed in the action menu
		filename - File and Path relative to the mission folder. Called when the action is activated. Depending on the file extension (*.sqf/*.sqs) the file is executed with SQF or SQS Syntax

		It returns Index of the added action. Used to remove the action with removeActionMenuItem()
		*/
		static int addActionMenuItem(Unit& unit, string title, string fileName);

		/*!
		Remove action with given id index. 
		*/
		static void removeActionMenuItem(Unit& unit, int index);

		/*!
		Return the name of a unit's current primary animation
		*/
		static string getAnimationState(Unit& unit);

		/*!
		Returns true if the specified unit is subgroup leader. 
		*/
		static bool isFormationLeader(Unit& unit);

		/*!
		Return the direction in degrees of the 'unit' watching in formation. 
		*/
		static float getFormationDirection(Unit& unit);

		/*!
		Reload primary weapon of given unit
		*/
		static void reloadPrimaryWeapon(Unit& unit);

		/*!
		Return how much unit wants to reload its weapons. range 0 to 1
		1 - Its empty
		0 - Fully loaded
		*/
		static float getNeedReload(Unit& unit);

		/*!
		Search for selection in the object model.

		Returns the current position of a selection in model space (takes into account any animations).
		If a selection does not exist [0,0,0] is returned.

		The model is searched in this order:

		Memory
		Geometry
		FireGeometry
		LandContact
		Hitpoints
		Paths
		SubParts
		ViewGeometry

		selectionName - Possible values are,
			aiming_axis
			aimPoint
			camera
			footstepL
			footstepR
			granat
			granat2
			Head
			head_axis
			lankle
			launcher
			leaning_axis
			LeftFoot
			LeftHand
			LeftHandMiddlel
			lelbow
			lelbow_axis
			lfemur
			lknee
			lknee_axis
			lshoulder
			lwrist
			Neck
			NVG
			Pelvis
			pilot
			rankle
			rearm
			rearm2
			relbow
			relbow_axis
			rfemur
			RightFoot
			RightHand
			RightHandMiddlel
			rknee
			rknee_axis
			rshoulder
			rwrist
			Spine3
			Weapon 
		*/
		static position3D getSelectionPosition(Unit& unit, string selectionName);

		/*!
		Search for selection in the object model.

		Returns the current position of a selection in model space (takes into account any animations).
		If a selection does not exist [0,0,0] is returned.

		The model is searched in this order:

		Memory
		Geometry
		FireGeometry
		LandContact
		Hitpoints
		Paths
		SubParts
		ViewGeometry

		selectionName - Possible values are,
			aiming_axis
			aimPoint
			camera
			footstepL
			footstepR
			granat
			granat2
			Head
			head_axis
			lankle
			launcher
			leaning_axis
			LeftFoot
			LeftHand
			LeftHandMiddlel
			lelbow
			lelbow_axis
			lfemur
			lknee
			lknee_axis
			lshoulder
			lwrist
			Neck
			NVG
			Pelvis
			pilot
			rankle
			rearm
			rearm2
			relbow
			relbow_axis
			rfemur
			RightFoot
			RightHand
			RightHandMiddlel
			rknee
			rknee_axis
			rshoulder
			rwrist
			Spine3
			Weapon 

		method - How to calculate the selection center. Possible valued are:

		"average": Arithmetic average of all vertices in selection
		"bbox_center": Geometry center of the axis-aligned bounding box containing all vertices from selection.
		"allpoints": All points contained in the specific selection.
		*/
		static position3D getSelectionPosition(Unit& unit, string selectionName, string method);


		/*!
		Get the speed for the given speed mode.

		SpeedMode can be:

		"AUTO"
		"SLOW"
		"NORMAL"
		"FAST" 
		*/
		static float getSpeed(Unit& unit, string speedMode);

		/*!
		Return the target assigned to the vehicle.
		*/
		static ControllableObject getAssignedTarget(Unit& unit);


		/*!
		Stop AI unit.Stopped unit will not be able to move, fire, or change
		its orientation to follow a watched object.
		*/
		static void UnitUtilities::stop(Unit& unit, bool stop);

	private:
		friend class Unit;

	};
};


#endif