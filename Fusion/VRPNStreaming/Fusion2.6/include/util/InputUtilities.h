/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	InputUtilities.h

Purpose:

	This file contains the declaration of the InputUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			20-09-2011	SSD: Original Implementation


/************************************************************************/

#ifndef VBS2FUSION_INPUT_UTILITIES_H
#define VBS2FUSION_INPUT_UTILITIES_H

/**********************************************/
/* INCLUDES
/**********************************************/

// Standard Includes
#include <string>


// Simcentric Includes
#include "VBS2Fusion.h"
#include "VBS2FusionDefinitions.h"

/**********************************************/
/* END INCLUDES
/**********************************************/
namespace VBS2Fusion
{
	class VBS2FUSION_API InputUtilities
	{
	public:

		/*!
		Assigns a command to a key action (key board or joystick input).
		If suppress is passed as true, the action assigned to key action does not occur, 
		only the script command executes.
		A key index is returned such that it can be passed into the unBindKey function.
		For an incorrect binding (-1) is returned.
		*/
		static int bindKey(string keyAction, string command, bool suppress);
		
		/*!
		Assigns a command to a Direct Input Keyboard (DIK) key code.
		If suppress is passed as true, the action associated with the DIC key does not occur, 
		only the script command executes.
		A key index is returned such that it can be passed into the unBindKey function.
		For an incorrect binding (-1) is returned.
		*/
		static int bindKey(int keyCode, string command, bool suppress);

		/*!
		Assigns a command to a key action and returns the key index.

		keyAction -  Key board or joystick input.
		command - Script command to execute on the key action.
		suppress - If suppress is passed as true, the action assigned to key action does not occur, only the script command executes.
		enableFloat - Flag to identify whether the command should return on float inputs (like from joystick ranging from 0 to 1) 
			or boolean inputs (like from key as 0 or 1). When set to false command returns only for discrete inputs (0 or 1).
		rule - Command is executed when the button is pressed, when the value associated with the button has changed or 
			on each simulation step	as specified by the KEY_BIND_RULE enum.
		vehicleType(optional) - Command is executed only when the player is in the passed vehicle type.
		
		For an incorrect binding (-1) is returned.
		*/
		static int bindKey(string keyAction, string command, bool suppress, bool enableFloat, KEY_BIND_RULE rule, string vehicleType = "");

		/*!
		Assigns a command to a Direct Input Keyboard (DIK) key code and returns the key index.

		keyCode - DIK key code.
		command - Script command to execute on button event.
		suppress - If suppress is passed as true, the action assigned to key action does not occur, only the script command executes.
		enableFloat - Flag to identify whether the command should return on float inputs (like from joystick ranging from 0 to 1) 
			or boolean inputs (like from key as 0 or 1). When set to false command returns only for discrete inputs (0 or 1).
		rule - Command is executed when the button is pressed, when the value associated with the button has changed or 
			on each simulation step	as specified by the KEY_BIND_RULE enum.
		vehicleType(optional) - Command is executed only when the player is in the passed vehicle type.

		For an incorrect binding (-1) is returned.
		*/
		static int bindKey(int keyCode, string command, bool suppress, bool enableFloat, KEY_BIND_RULE rule, string vehicleType = "");

		/*!
		Revoke the specified key bound.
		*/
		static void unBindKey(int keyIndex);

		/*!
		Records all user input, to be later used with replayUserInput.
		Input will be recorded until either escape button is pressed or stopUserInputStreaming is executed.
		Type of "fileName" has to be [.rec]. 
		Ex: "recording.rec", "d:\\recordings\\recording.rec"
		Note that the specified folder path should exist.
		*/
		static void recordUserInput(string fileName);

		/*!
		Replays user input, which was recorded by recordUserInput. 
		Replay is looped until escape button is pressed, or stopUserInputStreaming is executed.
		Type of "fileName" has to be [.rec]. 
		Ex: "recording.rec", "d:\\recordings\\recording.rec"
		*/
		static void replayUserInput(string fileName);

		/*!
		Stops either the recording or replaying of user input. 
		Alternatively, pressing escape button will also stop either of those commands.
		*/
		static void stopUserInputStreaming();

		/*!
		Disable and enable the keyboard and mouse input.
		Be careful with the usage of this command, always remember to enable the user input again 
		once the user input is disabled.
		*/
		static void disableUserInput(bool state);

		/*!
		Returns true if the key with the specified Direct Input Keyboard (DIK) key code is 
		currently pressed. 
		*/
		static bool isKeyPressed(int keyCode);

		/*!
		Define action performed when user clicks in map by executing command string.

		the string receives 3 (localised in scope) variables:

		_pos: Array - Clicked position
		_units: Array - Units which were selected (via function keys) before opening the map (may be non-functional in Arma)
		_shift, _alt Boolean - Whether <Shift> or <Alt> were pressed when clicking on the map 

		When click is processed, code should ultimately return true back to the engine. If false is returned, default processing by the game engine is done. Return value of any other type (including Nothing) is an error. In such case default processing by the game engine is done, and error message may be displayed.

		The code is executed on every click, until the Command is

		removed via onMapSingleClick "", or
		replaced by onMapSingleClick "SomeOtherCommand(s)"
		*/
		static void onMapSingleClick(string command);

		/*!
		Defines an action performed when the user double clicks on the map.

		The following variables are available in the command section:

		_pos (Position3D) = clicked position
		_units (Array) = units which were selected (via function keys) before opening the map (may be non-functional in V1.23 and lower)
		_alt (Boolean) = whether the <Alt> key was pressed
		_map (Control) = current map control 
		*/
		static void onMapDoubleClick(string command);

		/*!
		Defines an action performed when the user double clicks on the map.
		Map - It is the map (you can use "map" for current map)

		The following variables are available in the command section:

		_pos (Position3D) = clicked position
		_units (Array) = units which were selected (via function keys) before opening the map (may be non-functional in V1.23 and lower)
		_alt (Boolean) = whether the <Alt> key was pressed
		_map (Control) = current map control 
		*/
		static void onDoubleClick(string Map, string command);

	};
};

#endif //VBS2FUSION_INPUT_UTILITIES_H