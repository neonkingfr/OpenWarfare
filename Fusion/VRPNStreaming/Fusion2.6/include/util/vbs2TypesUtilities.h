/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

vbs2Types.h

Purpose:

This file contains the declaration of the vbs2Types class.

Version Information:

Version		Date		Author and Comments
===========================================
1.0			15-09/2009	SDS: Original Implementation
2.01		10-02/2010	MHA: Comments Checked
2.02		14-10/2011	CGS: Methods Modified, vector<string> getSoldierTypes()
								vector<string> getAirplaneTypes()
								vector<string> getTankTypes()
								vector<string> getCarTypes()
								vector<string> getHelicopterTypes()
								vector<string> getVasiTypes()
								vector<string> getAllTypes()
							Methods Added vector<string> getTypesEx(vector<string>& types)
								vector<string> getShipTypes()
/************************************************************************/

#ifndef VBS2TYPES_H
#define VBS2TYPES_H

#include <iostream>
#include <fstream>

#include "VBS2Fusion.h"
#include "ExecutionUtilities.h"
#include "conversions.h"


namespace VBS2Fusion
{
	class VBS2FUSION_API VBS2TypesUtilities
	{
	public: 

		/*!
		Return all the soldier type objects in VBS engine as
		a vector of strings
		*/
		static vector<string> getSoldierTypes();

		/*!
		Return all the Airplane type objects in VBS engine as
		a vector of strings
		*/
		static vector<string> getAirplaneTypes();

		/*!
		Return all the Tank type objects in VBS engine as
		a vector of strings
		*/
		static vector<string> getTankTypes();

		/*!
		Return all the Car type objects in VBS engine as
		a vector of strings
		*/
		static vector<string> getCarTypes();

		/*!
		Return all the Helicopter type objects in VBS engine as
		a vector of strings
		*/
		static vector<string> getHelicopterTypes();

		/*!
		Return all the Vasi type objects in VBS engine as
		a vector of strings
		*/
		static vector<string> getVasiTypes();

		/*!
		Return all the All type objects in VBS engine as
		a vector of strings
		*/
		static vector<string> getAllTypes();

		static vector<string> getGrenadeTypes();

		/*!
		This method implement the logic to get different types of objects in VBS engine. object type passed
		as a parameter to method. type can be one of following. [SOLDIER, AIRPLANE, TANK, CAR, Helicopter, 
		VASI, ALL] etc.	Result returns as a vector of strings
		*/
		static vector<string> getTypesEx(vector<string>& types);

		/*!
		Return all the ship type objects in VBS engine as a vector of strings
		*/
		static vector<string> getShipTypes();


	private:
		/*!
		This method implement the logic to get different types
		of objects in VBS engine. object type passed as a parameter
		to method. type can be one of following.
		[SOLDIER, AIRPLANE, TANK, CAR, Helicopter, VASI, ALL] 
		result returns as a vector of strings

		*/
		static vector<string> getTypeLogic(string type);

	};
};

#endif
