/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

TriggerUtilities.h

Purpose:

This file contains the declaration of the TriggerUtilities class.

Version Information:

Version		Date		Author and Comments
===========================================
1.0			30-03/2009	RMR: Original Implementation
1.01		27-08/2009   SDS: Added updateIsLocal(Trigger& trig);	
							 Updated updateTrigger(Trigger& trig)		
1.02		02-08/2009		 Added getIDUsingAlias(Trigger& trig)
							 Updated getPosition(Trigger& trig)
2.0			10-02/2010	MHA: Comments Checked
/************************************************************************/

#ifndef VBS2FUSION_TRIGGER_UTILITIES_H
#define VBS2FUSION_TRIGGER_UTILITIES_H

#include <sstream>
#include <string>
#include <list>

#include "position3D.h"
#include "VBS2Fusion.h"
#include "data/Trigger.h"
#include "util/ExecutionUtilities.h"

namespace VBS2Fusion
{

	class VBS2FUSION_API TriggerUtilities
	{
	public:

		//-------------------------------------------------------------------

		/*!
		Returns the position of the trigger in position3D format.

		This function uses the network ID of the object to access it within 
		the game environment, so please ensure that either a registered alias 
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)
		- Position (outputs an error if the position returned is invalid)
		*/

		static position3D getPosition(Trigger& trig);		

		/*!
		Applies the position obtained position to the trigger. 

		This function uses the network ID of the object to access it within 
		the game environment, so please ensure that either a registered alias 
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)

		This method get parameter from Fusion then apply it in VBS2 side
		*/

		static void applyPosition(Trigger& trig, position3D position);

		/*!
		Applies the position obtained trig.getPosition() to the trigger. 
		*/

		//static void applyPosition(Trigger& trig);

		/*!
		Update the trigger position. 
		This method get the parameter from VBS2 side and update the Fusion parameter
		*/

		static void updatePosition(Trigger& trig);

		//-------------------------------------------------------------------

		/*!
		Returns the trigger statements. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)			
		*/
		static string getStatements(Trigger& trig);		

		/*!
		Applies the statements obtained through 
		trig.getStatementsInVBSFormat() to the trigger.

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)

		This method get parameter from Fusion then apply it in VBS2 side

		Need to use following Trigger fuction before use this function
		//trig is a Trigger variable
		trig.setStatementsCond("this");
		trig.setStatementsActiv("hint 'trigger on'");
		trig.setStatementsDesactiv("hint 'trigger off'");
		*/

		static void applyStatements(Trigger& trig);	

		/*!
		Updates all trigger statements for trig. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)	

		This method get the parameter from VBS2 side and update the Fusion parameter
		*/
		static void updateStatements(Trigger& trig);	

		//-------------------------------------------------------------------

		/*!
		Returns the activation statements for the trigger. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)		
		*/
		static string getActivation(Trigger& trig);

		/*!
		Applies the activation statements obtained through 
		trig.getTriggerActivationInVBSFormat(); to the trigger. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)	

		This is apply changes the following parameters in VBS2 from Fusion 
		by: String - Who activates trigger. Can be "NONE" or

		Side': "EAST", "WEST", "GUER", "CIV", "LOGIC", "ANY"
		Radio: "ALPHA", "BRAVO", "CHARLIE", "DELTA", "ECHO", "FOXTROT", "GOLF", "HOTEL", "INDIA", "JULIET"
		Object: "STATIC", "VEHICLE", "GROUP", "LEADER", "MEMBER"
		Status: "WEST SEIZED", "EAST SEIZED" or "GUER SEIZED"

		type: String - How trigger is it activated. Can be:

		Presence: "PRESENT", "NOT PRESENT"
		Detection: "WEST D", "EAST D", "GUER D" or "CIV D"

		repeating: Boolean - Activation can happen repeatedly

		The following functions are used to edit trigger parameters
		trig.setTriggerActivation();
		trig.setTriggerPresence(string presence);
		trig.setRepeating(bool repeating);

		This method get parameter from Fusion then apply it in VBS2 side
		*/		
		static void applyActivation(Trigger& trig);

		/*!
		updates the triggers activation statements. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)		

		This method get the parameter from VBS2 side and update the Fusion parameter
		*/
		static void updateActivation(Trigger& trig);

		//-------------------------------------------------------------------

		/*!
		Returns the triggers area statements. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)		
		*/
		static string getArea(Trigger& trig);

		/*!
		Applies the area statements obtained through 
		trig.getTriggerAreaInVBSFormat() to the trigger. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)	
		*/		
		static void applyArea(Trigger& trig);

		/*!
		Updates the triggers area values. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)	

		This method get the parameter from VBS2 side and update the Fusion parameter
		*/
		static void updateArea(Trigger& trig);
		//-------------------------------------------------------------------

		/*!
		Returns the triggers timeout statements. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)
		trig.setTimeoutMax(double val);
		trig.setTimeoutMid(double val);
		trig.setTimeoutMin(double val);
		*/		
		static string getTimeout(Trigger& trig);

		/*!
		Applies the timeout statements obtained through 
		trig.getTriggerTimeoutInVBSFormat() to the trigger. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)	

		trig.setTimeoutMax(double val);
		trig.setTimeoutMid(double val);
		trig.setTimeoutMin(double val);

		This method get parameter from Fusion then apply it in VBS2 side
		*/
		static void applyTimeout(Trigger& trig);

		/*!
		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)	

		This method get the parameter from VBS2 side and update the Fusion parameter
		*/
		static void updateTimeout(Trigger& trig);

		//-------------------------------------------------------------------

		/*!
		Returns the triggers text. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)	
		*/
		static string getText(Trigger& trig);

		/*!
		Applies the text obtained through trig.getText() to the trigger. 
		
		trig.setText(string text);

		This method get parameter from Fusion then apply it in VBS2 side
		*/		
		static void applyText(Trigger& trig);

		/*!
		Applies the text obtained through text to the trigger. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)	

		This method get parameter from Fusion then apply it in VBS2 side
		*/
		static void applyText(Trigger& trig, string text);

		/*!
		Updates the triggers text. 
		This method get the parameter from VBS2 side and update the Fusion parameter
		*/
		static void updateText(Trigger& trig);

		/*!
		Applies the alias of the trigger object if the applied alias has not 
		already been assigned to another object in the mission.
		*/
		static void applyAlias(Trigger& trig);

		/*!
		Returns the name of the variable which contains a primary editor reference to this trigger object.
		*/
		static string getVarName(Trigger& trig);

		/*!
		Returns the triggers type. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		*/
		static string getType(Trigger& trig);

		/*!
		Applies the type obtained from type to the trigger. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		This method get parameter from Fusion then apply it in VBS2 side

		*/
		static void applyType(Trigger& trig, string type);

		/*!
		Applies the type obtained from trig.getTriggerType() to the trigger. 

		trig.setTriggerType(TRIGGERTYPE	type);

		This method get parameter from Fusion then apply it in VBS2 side
		*/		
		static void applyType(Trigger& trig);

		/*!
		Updates the triggers type.
		This method get the parameter from VBS2 side and update the Fusion parameter
		*/
		static void updateType(Trigger& trig);

		//-------------------------------------------------------------------

		/*!
		Updates the activation status boolean of the trigger. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)		

		This method get the parameter from VBS2 side and update the Fusion parameter
		*/
		static void updateActivated(Trigger& trig);

		//-------------------------------------------------------------------

		/*!
		Compound activation function for the triggers. Updates the following:

		- Activation Statements
		- Area
		- Position
		- Statements
		- Text
		- Timeout values
		- Activation

		This method get the parameter from VBS2 side and update the Fusion parameter
		*/
		static void updateTrigger(Trigger& trig);

		/*!
		Applies all values within the following to the trigger:

		- Activation Statements
		- Area
		- Position
		- Statements
		- Text
		- Timeout values
		- Activation

		This method get parameter from Fusion then apply it in VBS2 side
		*/

		static void applyChanges(Trigger& trig);		

		/*!
		Returns the network ID of an object using its alias. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias returned is invalid)
		*/
		//static string getIDUsingAlias(Trigger& trig);

		//-------------------------------------------------------
		// NetworkId utility
		//-------------------------------------------------------

		/*!
		Update "isLocal" property in co to know whether co has valid
		network id or not	

		*/
		static void updateIsLocal(Trigger& trig);	
		
		/*!
		update the network ID of an object using its alias. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the id returned is invalid)

		This method get the parameter from VBS2 side and update the Fusion parameter
		*/

		static void updateTriggerID(Trigger& trig);

		//-------------------------------------------------------------------	

		/*!
		Remove the trigger.
		*/
		static void deleteTrigger(Trigger& trig);

		//-------------------------------------------------------------------	

		/*!
		Creates a trigger as specified by the input trigger object.
		*/
		static void createTrigger(Trigger& trig);
	
		/*!
		Sets optional script that is executed when the trigger activates. 
		*/
		static double addTriggerOnActivated(Trigger& trigger, string command);

		/*!
		The trigger is created locally.
		*/
		static Trigger createTriggerLocal(string type, position3D position);

		//static void removeTriggerOnActivated(Trigger trig, double index);

		/*!
		Defines the trigger activation type. See Mission Editor - Triggers for a thorough overview 
		of triggers and its fields for activation, effects, etc.

		by: String - Who activates trigger. Can be "NONE" or

		Side': "EAST", "WEST", "GUER", "CIV", "LOGIC", "ANY"
		Radio: "ALPHA", "BRAVO", "CHARLIE", "DELTA", "ECHO", "FOXTROT", "GOLF", "HOTEL", "INDIA", "JULIET"
		Object: "STATIC", "VEHICLE", "GROUP", "LEADER", "MEMBER"
		Status: "WEST SEIZED", "EAST SEIZED" or "GUER SEIZED"

		type: String - How trigger is it activated. Can be:

		Presence: "PRESENT", "NOT PRESENT"
		Detection: "WEST D", "EAST D", "GUER D" or "CIV D"

		repeating: Boolean - Activation can happen repeatedly
		*/
		static void setTriggerActivation(Trigger& trig, string by, string type, bool repeat);

		/*!
		Returns all objects attached to the trigger
		*/
		static list<ControllableObject> triggerAttachedObj(Trigger& trig);

		/*!
		Detaches a game object from a trigger.
		*/
		static void triggerDetachObj(Trigger& trig, ControllableObject& obj);


		/*!
		Assigns any game object to a trigger. 
		*/
		static void triggerAttachObj(Trigger& trig, ControllableObject& co);

	};


};

#endif //TRIGGER_UTILITIES_H