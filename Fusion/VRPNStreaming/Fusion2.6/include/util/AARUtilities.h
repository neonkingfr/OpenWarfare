
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:	AARUtilities.h

Purpose: This file contains After Action Review utility function definitions. 

	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			17-06/2010	YFP: Original Implementation
	2.01		29-09-2011	SSD: Added Functions
									int addBookMark(double, string, string);
									void removeBookMark(int);
									string getBookMarkName(int);
									double getBookMarkTime(int);
									string getBookMarkMessage(int);
									int getTotalBookMarks();
	2.02		04-10-2011	SSD: Added Functions
									int getStat(ControllableObject, string);
									int getStat(Group, string);
									int getStat(SIDE, string);
									int getSaveState();
									vector<string> getFileInfo();
									void goToNextMessage();
									void goToPreviousMessage();

/*****************************************************************************/
#ifndef VBS2FUSION_AARUTILITIES_H
#define VBS2FUSION_AARUTILITIES_H

/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES
#include <string>
#include <vector>

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "data/ControllableObject.h"
#include "data/Group.h"

/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBS2Fusion
{
	class VBS2FUSION_API AARUtilities
	{
	public:
		/*!
		Returns true if AAR file is loaded. 
		*/
		static bool isLoaded();
		
		/*!
		Returns current time within the AAR Mission.
		*/
		static double currentTime();

		/*!
		Returns true if the AAR mission is paused.
		*/
		static bool isPaused();

		/*!
		Returns true if the AAR is in play mode.
		*/
		static bool isPlaying();

		/*!
		Returns true if the AAR is recording.
		*/
		static bool isRecording();

		/*!
		Returns true if the AAR is in Repeat mode.
		*/
		static bool isRepeating();

		/*!
		Specify the file to load for the AAR.If sucessful it returns true.
		*/
		static bool load(string fileName);

		/*!
		starts playing a loaded AAR file.
		*/
		static void play();

		/*!
		starts recording on the current machine.
		*/
		static void record();

		/*!
		Returns the length of the current recorded AAR mission.
		*/
		static double replayLength();

		/*!
		Pauses the current playback. call again resumes play.
		*/
		static void pause();

		/*!
		Restart the AAR replay.
		*/
		static void start();

		/*!
		Saves the recorded mission to the specified file. 
		*/
		static void save(string fileName);

		/*!
		set whether the AAR playback should be in repeat mode or not.
		*/
		static void setRepeat(bool value);

		/*!
		moves to the specified time in recording. 
		*/
		static void setPlayBackTime(double time);

		/*!
		stops playing of the current mission.
		*/
		static void stop();

		/*!
		stop recording AAR information.
		*/
		static void stopRecording();

		/*!
		unload the currently loaded AAR file.
		*/
		static void unload();

		/*!
		Adds a bookmark into AAR. Returned index is used to refer the book mark later. 
		(-1) is returned if the operation fails.
		time(in seconds) - Time to associate the book mark with. 
		name - Name for the book mark.
		message - Message (description) to be associated with the book mark.
		*/
		static int addBookMark(double time, string name, string message);

		/*!
		Removes the book mark specified by the index.
		*/
		static void removeBookMark(int index);

		/*!
		Returns the name of the book mark specified by the index.
		An empty string is returned if the operation fails.
		*/
		static string getBookMarkName(int index);

		/*!
		Returns the time associated to the book mark specified by the index.
		(-1) is returned if the operation fails.
		*/
		static double getBookMarkTime(int index);

		/*!
		Returns the message associated to the book mark specified by the index.
		An empty string is returned if the operation fails.
		*/
		static string getBookMarkMessage(int index);

		/*!
		Returns the total number of book marks in the current AAR.
		(-1) is returned if the operation fails.
		*/
		static int getTotalBookMarks();

		/*!
		Returns the scalar value on the specified statistic type for the object.
		Statistic types: roundsFired, enemyKilled, friendlyKilled, enemyWounded and friendlyWounded.
		(-1) is returned if the operation fails.
		*/
		static int getStat(ControllableObject co, string type);

		/*!
		Returns the scalar value on the specified statistic type for the group.
		Statistic types: roundsFired, enemyKilled, friendlyKilled, enemyWounded and friendlyWounded.
		(-1) is returned if the operation fails.
		*/
		static int getStat(Group group, string type);

		/*!
		Returns the scalar value on the specified statistic type for the group.
		Statistic types: roundsFired, enemyKilled, friendlyKilled, enemyWounded and friendlyWounded.
		(-1) is returned if the operation fails.
		*/
		static int getStat(SIDE side, string type);


		/*!
		Returns the state of saving of AAR.
		(-2) : Recording, (-1) : Currently saving, 0 : File saving successful.
		If returned value is greater than 0, error has occurred while saving.
		*/
		static int getSaveState();

		/*!
		Returns the currently loaded AAR filename with it's folder path.
		0th position of the returned contains the folder path and 1st position contains the file name.
		If no AAR file is loaded empty strings will be returned.
		*/
		static vector<string> getFileInfo();

	};

};

#endif