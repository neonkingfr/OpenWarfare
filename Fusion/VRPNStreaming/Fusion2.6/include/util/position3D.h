/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	position3D.h

Purpose:

	This file contains the declaration of the position3D class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	2.0			23-11/2009	RMR: Original Implementation
	2.01		05-02/2010	MHA: Comments Checked
	2.02		04-11/2011	CGS: Added position3D(std::string strPos)

/************************************************************************/

#ifndef VBS2FUSION_POSITION_3D_H
#define VBS2FUSION_POSITION_3D_H

/**********************************************/
/* INCLUDES
/**********************************************/

// STANDARD INCLUDES
#include <string>

// SIMCENTRIC INCLUDES
#include "conversions.h"

/**********************************************/
/* END INCLUDES 
/**********************************************/

namespace VBS2Fusion
{
	class VBS2FUSION_API position3D
	{
	public:
		/*!
		The main constructor for the position3D class. Sets x, z, and y to 0. 
		*/
		position3D();

		/*!
		The copy constructor of the position3D class. 
		*/
		position3D(const position3D& pos);

		/*!
		Uses the passed in values to initialize x, z and y. 
		*/
		position3D(double x, double z, double y);

		/*!
		Converts the passed in values to doubles and initializes the object with the converted values. 
		*/
		position3D(std::string& x, std::string& z, std::string& y);

		/*!
		Converts the passed position string value to x,z,y doubles and initializes the object with the converted values. 
		*/
		position3D(std::string strPos);


		/*!
		The primary destructor for the class. 
		*/
		~position3D();

		//******************************************************************
		/*!
		Sets the x value of the position. 
		*/
		void setX(double val);

		/*!
		Sets the z value of the position. 
		*/
		void setZ(double val);

		/*!
		Sets the y value of the position. 
		*/
		void setY(double val);

		//******************************************************************

		/*!
		Returns the x value of the position. 
		*/
		double getX() const;

		/*!
		Returns the z value of the position. 
		*/
		double getZ() const;

		/*!
		Returns the y value of the position. 
		*/
		double getY() const;

		//******************************************************************

		/*!
		Returns the x value of the position. 
		*/
		double x() const;

		/*!
		Returns the y value of the position. 
		*/
		double y() const;

		/*!
		Returns the z value of the position. 
		*/
		double z() const;

		/*!
		Returns the right value(x) of the position. 
		*/
		double right() const;

		/*!
		Returns the front value(z) of the position. 
		*/
		double front() const;

		/*!
		Returns the up value(y) of the position. 
		*/
		double up() const;

		//******************************************************************

		/*!
		Returns a string in VBS2 position format. i.e. "[ x , z , y ]". 
		*/
		string getVBSPosition();

		//******************************************************************

		//Assignment operators

		position3D& operator=(const position3D& p);
		
		position3D& operator+=(const position3D& p);
		position3D& operator-=(const position3D& p);		
		position3D& operator*=(double d);
		position3D& operator/=(double d);

		//******************************************************************
		// Scalar Multiplication
		 position3D operator *(int val);
		 position3D operator *(double val);
		 position3D operator *(float val);

		//Scalar Division
		 position3D operator /(int val);
		 position3D operator /(double val);
		 position3D operator /(float val);

		//******************************************************************
		// Binary operators
		/*!
		overloaded + operator
		*/
		position3D operator+(const position3D& p1);
		
		/*!
		overloaded  - operator
		*/
		position3D operator-(const position3D& p1);
		
		/*!
		overloaded  == operator
		*/
		bool operator==(const position3D& p1) const;
		
		/*!
		overloaded != operator
		*/
		bool operator!=(const position3D& p1);
		
		/*!
		calculates the double distance between two positions
		*/
		static double calculateDistance(position3D p1, position3D p2);

		/*!
		check if the position3D is null.If it is null then it returns true.
		*/
		bool isNull();

		/*!
		set the position3D _x , _y , _z co-ordinates to minimum value of double.	
		*/
		void setNull();


		/*!
		Uses the passed position2D values to initialize x and z. 
		*/
		position3D(const position2D& pos);

		/*! 
		Assignment operators for position3D.  
		*/
		position3D& operator=(const position2D& rhs);
		position3D& operator+=(const position2D& rhs);
		position3D& operator-=(const position2D& rhs);

		/*!
		Overloaded + operator
		*/
		position3D operator+(const position2D& rhs);
		
		/*!
		Overloaded  - operator
		*/
		position3D operator-(const position2D& rhs);




	
	private:
		double _x,_z,_y;

	};
};
#endif //POSITION_3D_H