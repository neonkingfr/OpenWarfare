/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

GroupUtilities.h

Purpose:

This file contains the declaration of the GroupUtilities class.

Version Information:

Version		Date		Author and Comments
===========================================
1.0			31-03/2009	RMR: Original Implementation
1.01		27-08/2009  SDS: Added updateIsLocal(Group& grp);	
							 Updated updateGroupStaticProperties(Group& grp)
2.01		10-02/2010	MHA: Comments Checked
2.02        15-06/2010  YFP: Methods added,
							   void applyCustomFormation(Group&)
							   void updateCustomFormation(Group&)

2.03		04-01/2011	CGS: Methods added,
								createGroup(Group& _group, list<string> typelist,position3D pos)
								createGroup(Group & _group, list<Unit> unitlist,position3D pos)
								deleteGroup(Group& _group)
								setLeader(Group& group,Unit& u)
							  Modified,
								loadGroupMembers(Group& group)
								createAndAddWaypoint(Group& group, Waypoint& wp)
								updateGroupDynamicProperties(Group& group)
								getFormation(Group& group)
								updateFormation(Group& group)
								applyFormation(Group& group, string formation)
								getCurrentWaypoint(Group& group)
								updateCurrentWaypoint(Group& group)
								updateSide(Group& group)
								loadWaypoints(Group& group)
								updateGroupID(Group& group)
								updateIsLocal(Group& group)
								applyAlias(Group& group)
								getIDUsingAlias(Group& grp)
								applyCustomFormation(Group& group)	
2.04		28-09/2011	NDB: Added Method void allowFleeing(Group& grp, double cowardice)
2.05		04-11/2011	NDB: Added Method float knowsAbout(Group& grp, ControllableObject& target)

/************************************************************************/

#ifndef VBS2FUSION_GROUP_UTILITIES_H
#define VBS2FUSION_GROUP_UTILITIES_H

#include <sstream>
#include <string>

#include "position3D.h"
#include "VBS2Fusion.h"
#include "data/ControllableObject.h"

namespace VBS2Fusion
{
	class VBS2FUSION_API GroupUtilities
	{
	public:

		/*!
		Creates a new group using the side obtained from group.getSide(). 

		Error checking utility validates the following:

		- side(outputs an error if the side is invalid)		 		
		*/
		static void createGroup(Group& group);

		/*!
		Creates a new group using side. 

		If not alias is assigned to the group, a random alias is generated 
		and assigned. 

		Error checking utility validates the following:

		- side(outputs an error if the side is invalid)	
		*/
		static void createGroup(Group& group, string side);

		/*!
		Creates a new group using the units specified in unitlist and position. 
	 		
		*/
		static void createGroup(Group& group,list<Unit> unitlist,position3D pos);

		/*!
		Creates a new group using the units specified in type list and position. 
	 	For each entry in the type list, a unit is added to the group. 
		*/
		static void createGroup(Group& group,list<string> typelist,position3D pos);

		/*!
		Deletes a group in VBS side if there is no units inside the Group. 
	 		
		*/
		static void deleteGroup(Group& _group);

		//*******************************************************

		/*!
		Switch the unit into different Group	 		
		*/
	
		static void switchGroup(Group& group, Unit& u);
		
		/*!
		Assign a Leader to the group.	 		
		*/
		
		static void setLeader(Group& group,Unit& u);

		/*!
		Returns the formation of the group in string format.

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the id returned is invalid)
		- formation (outputs an error if empty formation returned)	
		*/
		static string getFormation(Group& group);

		/*!
		Updates and saves the formation of the group.
		*/
		static void updateFormation(Group& group);	

		/*!
		Applies the formation obtained from group.getFormation() to the group. 
		*/
		static void applyFormation(Group& group);

		/*!
		Applies the formation defined in formation to the group.

		Error checking utility validates the following:

		- formation (outputs an error if empty string passed)	
		*/
		static void applyFormation(Group& group, string formation);

		/*!
		Applies the custom formation defined in each members of the unit.
		*/
		static void applyCustomFormation(Group& group);

		/*!
		Update custom formation of members in the group.
		*/
		static void updateCustomFormation(Group& group);

		//*******************************************************

		/*!
		Return the index of the current waypoint of the group. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the id returned is invalid)
		*/
		static int getCurrentWaypoint(Group& group);		

		/*!
		Update the index of the current waypoint of the group. 
		*/
		static void updateCurrentWaypoint(Group& group);	

		/*!
		Apply the active Waypoint as a current Waypoint for the specified group.
		*/
		static void applyCurrentWaypoint(Group& group, Waypoint& wp);

		/*!
		Apply the active Waypoint as a current Waypoint for the specified group by 
		passing the correct waypoint index.
		*/
		static void applyCurrentWaypoint(Group& group);


		//*******************************************************				

		/*!
		Update the side of the group.
		*/
		static void updateSide(Group& group);

		/*!
		Return the side of the group.

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the id returned is invalid)
		*/
		static string getSide(Group& group);

		//*******************************************************

		/*!
		Return the name of the leader of group. 
		*/
		static string getLeader(Group& group);

		/*!
		Update the name of the group leader. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the id returned is invalid)

		Warning utility warns the following

		- output an warning if group size is zero
		*/

		static void updateLeader(Group& group);

		//*******************************************************

		/*!
		Compound Update for the group. Updates the following:

		- Group Dynamic properties: formation, leader, current waypoint. 
		- Group static properties: side.
		- Dynamic properties of all members.
		- Static properties of all members.
		- All waypoint properties. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the id returned is invalid)
		- group (outputs an error if group is invalid)
		*/
		static void updateGroup(Group& group);		

		//*******************************************************

		/*!
		Updates dynamic properties of the group: formation, leader and current waypoint index.

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the id returned is invalid)
		- group (outputs an error if group is invalid)
		*/
		static void updateGroupDynamicProperties(Group& group);
		
		/*!
		Updates static properties of the group: side. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the id returned is invalid)
		- group (outputs an error if group is invalid)

		Warning utility warns the following

		- output an warning if group size is zero
		*/
		static void updateGroupStaticProperties(Group& group);

		/*!
		Updates the dynamic properties of all members using UnitUtilities::updateDynamicProperties. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the id returned is invalid)
		- group (outputs an error if group is invalid)
		*/
		static void updateMembersDynamicProperties(Group& group);

		/*!
		Updates the static properties of all members using UnitUtilities::updateStaticProperties.

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the id returned is invalid)
		- group (outputs an error if group is invalid)
		*/
		static void updateMembersStaticProperties(Group& group);

		/*!
		Updates all waypoint properties.

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the id returned is invalid)
		- group (outputs an error if group is invalid)
		*/
		static void updateWaypointProperties(Group& group);

		//********************************************************

		/*!
		Loads the group from the game environment into the group variable. 

		- clears the unit and waypoint lists. 
		- loads the group name from VBS2
		- updates group static properties
		- updates group dynamic properties
		- loads group members
		- loads Waypoints
		- updates members dynamic and static properties
		- updates Waypoints dynamic and static properties

		Note: Displays an error on the screen if a group of the given name/alias (i.e. group.getAlias())
		does not exist in the VBS2 environment. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 


		Error checking utility validates the following:

		- group (outputs an error if group is invalid)
		- NetworkID (outputs an error if the id returned is invalid)
		*/
		static void loadGroup(Group& group);

		//********************************************************

		/*!
		Loads all Waypoints belonging to the group. 

		Error checking utility validates the following:

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 

		- group (outputs an error if group is invalid)
		- NetworkID (outputs an error if the id returned is invalid)
		*/
		static void loadWaypoints(Group& group);	

		/*!
		Updates the Waypoints belonging to the group.

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 

		- group (outputs an error if group is invalid)
		- NetworkID (outputs an error if the id returned is invalid)
		*/

		static void updateWaypointList(Group& group);


		//********************************************************

		/*!
		Loads all units belonging to the group. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 

		- group (outputs an error if group is invalid)
		- NetworkID (outputs an error if the id returned is invalid)
		*/
		static void loadGroupMembers(Group& group);

		/*!
		Updates all units belonging to the group. 
		*/
		static void updateGroupMembership(Group& group);

	//	//********************************************************

		/*!
		Creates a new unit specified by unit and adds as a member of the group. The new unit is added
		to the end of the unit list. If no alias is assigned to the unit, a random alias is generated. 
		*/
		static void createAndAddUnit(Group& group, Unit& unit);	

	//	//********************************************************

		/*!
		Creates a new waypoint specified by waypoint and adds as a member of the group. The new waypoint is added
		to the end of the waypoint list. If no alias is assigned to the waypoint, a random alias is generated. 
		*/

		static void createAndAddWaypoint(Group& group, Waypoint& wp);			

		//-------------------------------------------------------
		// NetworkId utility
		//-------------------------------------------------------

		/*!
		Update "isLocal" property in group to know whether co has valid
		network id or not		
		*/
		static void updateIsLocal(Group& group);	

		/*!
		updates the Waypoints belonging to the group.

		error checking utility validates the following:

		- NetworkId (outputs an error if the network id is invalid)
		*/
		static void updateGroupID(Group& grp);

		/*!
		get the NetworkId belonging to the group.

		Error checking utility validates the following:

		- alias (outputs an error if alias is invalid)
		*/
		static string getIDUsingAlias(Group& grp);

		/*!
		Return whether this group is in loaded by VBS2Fusion
		*/
		static bool groupExists(string groupName);

		/*!
		Applies alias to the group, if the applied alias has not 
		already been assigned to another group in the mission.
		*/
		static bool applyAlias(Group& group);

		/*!
		Gets the total number of waypoints belonging to the group.
		*/
		static int getNoOfWaypoints(Group& group);

		/*!
		Disable switching to next waypoints.
		*/
		static void lockWaypoints(Group& group);
		
		/*!
		Enables switching to next waypoints.
		*/
		static void unLockWaypoints(Group& group);


		/*!
		Gets the total number of waypoints belonging to the group. 
		*/
		static int getTotalWaypoints(Group& grp);

		/*!
		Returns whether smart formation re-ordering is enabled for a group. 
		*/
		static bool isEnabledSmartOrder(Group& grp);

		/*!
		Controls behavior of group members when changing formations. Default is on. 
		enable - If true, then group members might be re-ordered when assuming a new formation 
		(i.e. each unit will move to the position in the new formation that is closest to them). If false, 
		then groups will not be re-ordered.
		*/
		static void EnableSmartOrder(Group& grp, bool enable);

		/*!
		Returns the path post-processing mode for an AI group 
		returns Which group members will post-process their pathfinding ("ALL" or "ONLYLEADER")
		*/
		static string getPathPostProcessMode(Group& grp);

		/*!
		Sets the pathfinding post-processing mode for an AI group (e.g. walking on side of road).

		mode - Which group members should post-process their path:
		"ONLYLEADER" - only group leader (default)
		"ALL" - all units in group
		"NONE" - no unit in group (V1.40+)
		*/
		static void applyPathPostProcessMode(Group& grp, PATHPOSTPROCESSMODE mode);

		/*!
		Returns an array with all the units in the group or unit. For a destroyed object an empty array is returned
		*/
		static vector<Unit> getUnits(Group& grp);


		/*!
		Sets the group formation direction
		*/
		static void applyFormationDirection(Group& grp, double direction);

		/*!
		Set group formation heading. Accepted heading range is 0 to 360. 
		Formation is facing this direction unless enemy is seen. 
		When group is moving, this value is overriden by movement direction.
		*/
		static void applyFormDirection(Group& group, double direction);

		/*!
		Creates a move waypoint on given position and makes it an actual group waypoint.
		*/
		static void move(Group& grp, position3D pos);

		/*!
			set the behaviour of the Group. 
			  CARELESS,SAFE,AWARE,COMBAT,STEALTH
		*/
		static void applyGroupBehaviour(Group& grp, BEHAVIOUR behaviour);

		 /*!
		 Sets unit combat mode (engagement rules). Does not work for groups.

		 Mode may be one of:
		 "BLUE" (Never fire)
		 "GREEN" (Hold fire - defend only)
		 "WHITE" (Hold fire, engage at will)
		 "YELLOW" (Open fire)
		 "RED" (Open fire, engage at will) 

		 returns True if combat mode was applied sucessfully, otherwise false. 
		 */
		 static void applyGroupCombatMode(Group& grp, WAYPOINTCOMBATMODE mode);

		/*!
		Returns the combat mode of the Group.

		Modes returned may be:

		"BLUE" (Never fire)
		"GREEN" (Hold fire - defend only)
		"WHITE" (Hold fire, engage at will)
		"YELLOW" (Fire at will)
		"RED" (Fire at will, engage at will) 

		or

		"UNIT ERROR" (combat mode couldn't be retrieved) 
		*/
		static string getGroupCombatMode(Group& grp);

		/*!
		Set group speed mode. Mode may be one of:

		"LIMITED" (half speed)
		"NORMAL" (full speed, maintain formation)
		"FULL" (do not wait for any other units in formation) 
		*/
		static void applySpeedMode(Group& grp, WAYPOINTSPEEDMODE speedMode);

		/*!
		Return the speed mode of the group in string format.
		*/
		static string getSpeedMode(Group& grp);

		/*!
		Set variable to given value in the variable space of given element. To remove a variable, set it to nil
		_public - If true, then the value is broadcast to all computers.
		*/
		static void applyVariable(Group& grp, string name, string val, bool _public);

		/*!
		Return the value of variable in the variable space of given element.
		The command is case sensitive
		*/
		static string getVariable(Group& grp, string name);

		/*!
		Set a group's identity.

		noOfString - It should be the no of the string arguments in the function for calling

		Group ID format should be [letter, color, picture] or [letter, color].

		Letter is one of:
		"Alpha"
		"Bravo"
		"Charlie"
		"Delta"
		"Echo"
		"Foxtrot"
		"Golf"
		"Hotel"
		"Kilo"
		"Yankee" 

		"Zulu"
		"Buffalo"
		"Convoy"
		"Guardian"
		"November"
		"Two"
		"Three"
		"Fox" 

		Colour may be one of the following:
		"GroupColor0" - (Nothing)
		"GroupColor1" - Black
		"GroupColor2" - Red
		"GroupColor3" - Green
		"GroupColor4" - Blue
		"GroupColor5" - Yellow
		"GroupColor6" - Orange
		"GroupColor7" - Pink
		"Six" - Six 
		*/
		static void applyGroupID(Group& grp, int noOfString, string nameFormat1, ...);



		/*!
		Check if (and by how much) unit knows about target.
		And returns a number from 0 to 4.
		*/
		static float knowsAbout(Group& grp, ControllableObject& target);

	};
}
#endif //GROUP_UTILITIES_H