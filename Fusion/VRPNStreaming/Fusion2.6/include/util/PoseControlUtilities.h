
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name: PoseControlUtilities.h

Purpose: External pose control functions are declared in this file. 

	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			09-11/2010	YFP: Original Implementation

/*****************************************************************************/

#ifndef VBS2FUSION_POSECONTROLUTILITIES_H
#define VBS2FUSION_POSECONTROLUTILITIES_H



/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "data/Unit.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBS2Fusion
{

	class VBS2FUSION_API PoseControlUtilities
	{
	public:
		/************************************************************************/
		/* EXTERNAL POSE CONTROL                                                */
		/************************************************************************/

		/*!
		Enable/Disable the ability to modify pose of specified unit externally.
		*/
		static void setExternalPose(Unit& unit, bool status);

		/*!
		Returns true if pose of given unit is controlled externally. 
		*/
		static bool getExternalPose(Unit& unit);

		/************************************************************************/
		/* UPPER BODY CONTROL                                                   */
		/************************************************************************/

		/*!
		Enable/Disable upper body pose control of specified unit.
		Upper Body pose control of unit is disabled by default.
		*/
		static void setExternalPoseUpBody(Unit& unit ,bool status);

		/*!
		Returns true if upper body pose of given unit is Enabled.
		*/
		static bool getExternalPoseUpBody(Unit& unit);


		/************************************************************************/
		/* SKELETON CONTROL                                                    */
		/************************************************************************/

		/*!
		Enable/Disable skeleton control of a given unit.
		Skeleton control is disabled by default.
		*/
		static void setExternalPoseSkeleton(Unit& unit, bool status);

		/*!
		Return true if skeleton control is enabled.
		*/
		static bool getExternalPoseSkeleton(Unit& unit);

		/*!
		Disable Pose controlling of the unit. 
		If user unload the fusion plug in during the mission. 
		This should be called in onUnload callback.
		*/
		static bool DisablePoseControl(Unit& unit);

	};
}


#endif //VBS2FUSION_POSECONTROLUTILITIES_H