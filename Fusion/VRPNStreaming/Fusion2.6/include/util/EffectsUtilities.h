/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	EffectsUtilties.h

Purpose:

	This file contains the declaration of the EffectsUtilties class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			02-11/2011	NDB: Original Implementation
										
/************************************************************************/

#ifndef VBS2FUSION_EFFECT_UTILITIES_H
#define VBS2FUSION_EFFECT_UTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <string>

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "position3D.h"
#include "data/ControllableObject.h"
//#include "Vector3f.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/


namespace VBS2Fusion
{


	class VBS2FUSION_API EffectsUtilties
	{

	public:
		/*!
		Creates Effects in VBS environment according to given EFFECTS_TYPE
		*/
		static void CreateEffects(ControllableObject& object, EFFECTS_TYPE effectsType);

		/*!
		Creates Effects in VBS environment according to given parameters
		*/
		static void CreateEffects(ControllableObject& object, PARTICLE_PARAMS_ARRAY& paramsArray, PARTICLE_RANDOM_ARRAY& randomArray, float dropInterval);

		/*!
		Create Light in VBS environment according to given parameters.
		The light can see the in night time.
		*/
		static void CreateLight(ControllableObject& object, Color_RGB lightColor, Color_RGB ambientColor, float brightness, position3D& relatedPosition);

		/*!
		Creates a particle effect.

		This command is used to create smoke, fire and similar effects.

		The particles are single polygons with single textures that always face the player.
		They can be set to dynamically change their position, size, direction, can be set to different weights and more or less dependant on the wind. 
		*/
		static void CreateDynamicEffects(PARTICLE_PARAMS_ARRAY& particleArray);

	};
};

#endif //EFFECT_UTILITIES_H