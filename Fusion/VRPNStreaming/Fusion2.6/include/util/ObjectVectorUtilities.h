/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	ObjectVectorUtilities.h

Purpose:

	This file contains the declaration of the ObjectVectorUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	2.0			12-01/2010	RMR: Original Implementation
	2.01		10-02/2010	MHA: Comments Checked

/************************************************************************/

#ifndef VBS2FUSION_OBJECT_VECTOR_UTILITIES_H
#define VBS2FUSION_OBJECT_VECTOR_UTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>
#include <math.h>

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "conversions.h"
#include "data/ControllableObject.h"
#include "data/Group.h"
#include "util/ExecutionUtilities.h"
#include "data/VBS2Variable.h"
#include "position3D.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBS2Fusion
{	
	typedef position3D vector3D;

	class VBS2FUSION_API ObjectVectorUtilities
	{
	public:
		/*!
		Set object's up vector. Direction vector will remain unchanged.
		
		setVectorUp can only influence an object's bank. It can not influence pitch. 
		
		Example:

		player setVectorUp [0,1,0]
		If the player is facing 0 degrees (north), then this will do NOTHING.
		If the player is facing 90 degrees (east), then this will make him bank 90 degrees to his left.
		*/

		static void setVectorUp(ControllableObject& co, vector3D vec);

		/*!
		Return object's up vector in world Position coordinates ( [x, y, z] ).
		*/
		static vector3D getVectorUp(ControllableObject& co);		

		/*!
		Set object's direction vector. Up vector will remain unchanged.

		setVectorDir can only influence an object's pitch. It can not influence bank. 
		
		Example:
		player setVectorDir [0,0,1]
		If the player is facing 0 degrees (north), then this will do NOTHING.
		If the player is facing 90 degrees (east), then this will make him pitch 90 degrees up.
		
		You can't directly pitch an object beyond 90 degrees, because this would change its facing 
		direction. You must first flip it's direction using setDir, then you must bank the object 
		180 degrees, THEN you pitch the object appropriately.
		*/
		static void setVectorDir(ControllableObject& co, vector3D vec);

		/*!
		Return object's direction vector in world Position coordinates ([x, z, y]).

		Example: 

		A unit facing North would return [0,1,0]
		A unit facing East would return [1,0,0]
		A unit facing South would return [0,-1,0]
		A unit facing West would return [-1,0,0]
		*/
		static vector3D getVectorDir(ControllableObject& co);

		/*!
		Sets an objects pitch and bank angles. 
		*/
		static void setPitchAndBank(ControllableObject& co, double pitch_angle, double bank_angle);

		/*!
		Sets an objects direction using vectors. Angle is calculated in relation to the current angle. 
		*/
		static void setDirection(ControllableObject& co, double direction);

	};
};

#endif //OBJECT_VECTOR_UTILITIES_H