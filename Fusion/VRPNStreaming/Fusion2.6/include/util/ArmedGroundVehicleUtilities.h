/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

ArmedGroundVehicleUtilities.h

Purpose:

This file contains the declaration of the ArmedGroundVehicleUtilities class.

Version Information:

Version		Date		Author and Comments
===========================================
2.0			29-01/2010	YFP: Original Implementation
2.01		12-02/2010	MHA: Comments Checked

/************************************************************************/

#ifndef VBS2FUSION_ARMEDGROUNDVEHICLEUTILITIES_H
#define VBS2FUSION_ARMEDGROUNDVEHICLEUTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <string>

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "data/ControllableObject.h"
#include "data/ArmedGroundVehicle.h"
#include "util/VehicleUtilities.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/

namespace VBS2Fusion
{

	class VBS2FUSION_API ArmedGroundVehicleUtilities : public VehicleUtilities
	{
	public:

		/*!
			Creates a new vehicle as defined by the parameters present in vehicle. 
		*/
		static void createVehicle(ArmedGroundVehicle& vehicle);

		/*!
			Updates the armed ground vehicle. 			
		*/
		static void updateVehicle(ArmedGroundVehicle& vehicle);

		/*!
			Disable the turret of the armed ground vehicle specified by the turret path.
			if the status is true gunner input will be disabled. 
		*/
		static void disableGunnerInput(ArmedGroundVehicle& vehicle, string strTurretpath , bool status);

		/*!
			Armed ground vehicle fires from the specifically named weapon. 
		*/
		static void fire(ArmedGroundVehicle& vehicle, string weaponName);

		/*!
			Fire at the specified object. 
		*/
		static void doFire(ArmedGroundVehicle& vehicle,ControllableObject& object);

		/*!
			Order armed ground vehicle to fire at the specified object.
		*/
		static void commandFire(ArmedGroundVehicle& vehicle,ControllableObject& object);


		/*!
			Change the direction of turret specified by the turret path of the Armed ground vehicle to target the 
			Controllable object specified.
		*/
		static void applyWeaponDirection(ArmedGroundVehicle& vehicle, string strTurretpath,ControllableObject& object);

		/*!
			Apply Azimuth and elevation of the weapon of given turret in the armed ground vehicle.
			If the transition is TRUE then the weapon moves with transition else Direction changes without a transition.
		*/
		static void applyWeaponDirection(ArmedGroundVehicle& vehicle, string strTurretpath , double azimuth, double elevation, bool transition);

		/*!
			Returns turret elevation of the armed vehicle for a given target at specified target position.
		*/
		static double getElevation(ArmedGroundVehicle& vehicle, string strTurretpath,position3D targetPos);

		/*!
			Returns turret elevation of the armed vehicle for a given target specified as Controllable object.
		*/
		static double getElevation(ArmedGroundVehicle& vehicle, string strTurretpath, ControllableObject& object);

		/*!
			Returns turret azimuth of the armed vehicle for a given target at specified target position.
		*/
		static double getAzimuth(ArmedGroundVehicle& vehicle, string strTurretpath,position3D targetPos);

		/*!
			Returns turret azimuth of the armed vehicle for a given target specified as Controllable object.
		*/
		static double getAzimuth(ArmedGroundVehicle& vehicle, string strTurretpath, ControllableObject& object);

		/*!
			Apply the network shell mode. Normally bullets are locally simulated following a fired event.
			When network shell mode is set, the simulation is broadcast to the network. 
		*/
		static void setNetworkShellMode(ArmedGroundVehicle& vehicle,bool mode);

		
	};
};


#endif //ARMEDGROUNDVEHICLEUTILITIES_H