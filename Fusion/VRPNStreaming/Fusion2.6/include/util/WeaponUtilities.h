
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/
/*************************************************************************

Name:

WeaponUtilities.h

Purpose:

This file contains the declaration of the WeaponUtilities class.

Version Information:

Version		Date		Author and Comments
===========================================
1.0			29-01/2010	YFP: Original Implementation

/************************************************************************/

#ifndef VBS2FUSION_WEAPONUTILITIES_H
#define VBS2FUSION_WEAPONUTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <string>

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "data/ArmedGroundVehicle.h"
#include "data/Projectile.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/

namespace VBS2Fusion
{

	class VBS2FUSION_API WeaponUtilities
	{
	public:
	
		/*! Get the elevation of the weapon in specified turret of the Armed Vehicle from VBS2 Environment. */
		static double getElevation(Vehicle& vehicle, Turret& turret, Weapon& weapon);

		/*! Get azimuth of the weapon in specified turret of the armed vehicle from VBS2 Environment.  */
		static double getAzimuth(Vehicle& vehicle, Turret& turret, Weapon& weapon);

		/*! Get ammo count of the weapon in the armed vehicle from VBS2 Environment. */
		static int getAmmoCount(Vehicle& vehicle, Weapon& weapon);

		/*! Get the aiming direction of the specified Weapon of the turret in armed vehicle.
		This method returns a unit vector of the direction. 
		*/
		static position3D getWeaponDirectionVector(Vehicle& vehicle, Turret& turret, Weapon& weapon);

		/*! set reload state of the weapon.*/
		static void setWeaponState(Vehicle& vehicle,Weapon& weapon, bool reloadStatus,double reloadTime);

		/*! Get the elevation of the weapon in specified turret of the Armed Vehicle from VBS2 Environment. */
		static double getElevation(ArmedGroundVehicle& vehicle, Turret& turret, Weapon& weapon);

		/*! Get azimuth of the weapon in specified turret of the armed vehicle from VBS2 Environment.  */
		static double getAzimuth(ArmedGroundVehicle& vehicle, Turret& turret, Weapon& weapon);

		/*! Get the aiming direction of the specified Weapon of the turret in armed vehicle.
		This method returns a unit vector of the direction. 
		*/
		static position3D getWeaponDirectionVector(ArmedGroundVehicle& vehicle, Turret& turret, Weapon& weapon);

		/*! set reload state of the weapon.*/
		static void setWeaponState(ArmedGroundVehicle& vehicle,Weapon& weapon, bool reloadStatus,double reloadTime);

		/*! 
		Creates a shot of the specified type, position and velocity
		*/
		static Projectile createShot(string ammoType,position3D pos, vector3D velocity);

		/*! 
		Enables shot creation via mouse clicks. The shot will be created at the player's location, and move
		towards	the position defined by the mouse. The mouse click can either be generated via an in-game 
		dialog, or via a windows message
		*/
		static void allowCreateShot(bool enable);

		/*! 
		Returns the unit or vehicle that fired the given shot or laser target.
		*/
		static ControllableObject getShotOwner(Projectile& proj);

		/*! 
		Makes explosive shell inert. Shell wont create any visual effects or damage and will disappear.
		*/
		static void setDummyRound(Projectile& proj, bool enable = true);


	};

};

#endif

