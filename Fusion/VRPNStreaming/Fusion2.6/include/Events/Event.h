
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/
/*************************************************************************

Name:

	Event.h

Purpose:

	This file contains the declaration of the Event class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			17-04/2009	RMR: Original Implementation
	2.0			17-01/2010	UDW: Version 2 Implementation
	2.01		10-02/2010	MHA: Comments Checked

/************************************************************************/

#ifndef VBS2FUSION_EVENT_H
#define VBS2FUSION_EVENT_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <string>

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "data/NetworkID.h"
#include "data/ControllableObject.h"
#include  "VBS2EventTypes.h"
/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBS2Fusion
{

	/*!
	Is a generic storage container for storing information related to a VBS2 event. All Event classes
	used in VBS2Fusion inherit from this class. 
	*/
	class VBS2FUSION_API Event
	{
	public:
		/*!
		Primary constructor for the class. 
		*/
		Event();

		/*!
		Primary destructor for the class. 
		*/
		~Event();

		/*!
		Sets the type of event using a VBSEVENTTYPE variable. Should be one of:

		{AMMOEXPLODE, ATTACHTO, CARGOCHANGED, DEL, FIRED, GETINMAN, GETOUTMAN,
		HITPART, LOADOUTCHANGED, RESPAWN, SUPPRESSED, TURNIN,
		TURNOUT, WAYPOINTCOMPLETE}

		The current version of VBS2Fusion implements functionality for the following
		event types:

		- DEL
		- FIRED
		- GETINMAN
		- GETOUTMAN
		- HITPART
		- RESPAWN
		- SUPPRESSED
		- WAYPOINTCOMPLETE
		*/
		void setType(VBSEVENTTYPE type);

		/*!
		Sets the type of event using a string variable. Should be one of:

		{"AMMOEXPLODE", "ATTACHTO", "CARGOCHANGED", "DEL", "FIRED", "GETINMAN", "GETOUTMAN",
		"HITPART", "LOADOUTCHANGED", "RESPAWN", "SUPPRESSED", "TURNIN",
		"TURNOUT", "WAYPOINTCOMPLETE"}

		The current version of VBS2Fusion implements functionality for the following
		event types:

		- DEL
		- FIRED
		- GETINMAN
		- GETOUTMAN
		- HITPART
		- RESPAWN
		- SUPPRESSED
		- WAYPOINTCOMPLETE
		*/
		void setType(string type);

		/*!
		Returns the event type using a VBSEVENTTYPE variable. 
		*/
		VBSEVENTTYPE getType() const;

		/*!
		Returns the event type using a string variable. 
		*/
		string getTypeString(); 

		/*!
		Sets the name of the primary object. This refers to the name used in VBS2 and NOT the 
		VBS2Fusion assigned alias. 
		*/
		void setObjectName(string name);

		/*!
		Sets the name of the primary object using co.getName(). This refers to the name used in VBS2 and NOT the 
		VBS2Fusion assigned alias. 
		*/
		void setObjectName(ControllableObject& co);

		/*!
		Returns the name of the primary object. This refers to the name used in VBS2 and NOT the 
		VBS2Fusion assigned alias. 
		*/
		string getObjectName() const;

		/*!
		Converts a VBSEVENTTYPE into a string variable. 
		*/
		string VBSEVENTTYPE_To_String(VBSEVENTTYPE type);

		/*!
		Converts a string variable into a VBSEVENTTYPE variable. 
		*/
		VBSEVENTTYPE String_To_VBSEVENTTYPE(string type);

		/*!
		Set object network ID 
		*/
		void setObjectID(NetworkID _nid);

		/*!
		Returns object network ID 
		*/
		NetworkID getObjectID() const;

	private:
		VBSEVENTTYPE type;
		string ControllableObjectName;	
		NetworkID _nid;
	};
};

#endif //EVENT_H