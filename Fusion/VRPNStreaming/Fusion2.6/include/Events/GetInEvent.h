/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	GetInEvent.h

Purpose:

	This file contains the declaration of the GetInEvent class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			08-07/2011	CGS: Original Implementation

/************************************************************************/

#ifndef VBS2FUSION_GET_IN_EVENT_H
#define VBS2FUSION_GET_IN_EVENT_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <string>

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "Events/Event.h"

/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBS2Fusion
{

	/*!
	Defines a GetInEvent which is passed onto the object when an 'GETIN' event is 
	called. Usually passed into the processGetIn method of an Vehicle.
	*/
	class VBS2FUSION_API GetInEvent: public Event
	{
	public:
		/*!
		Primary constructor for the class. Calls setType(ENTITYGETIN) to initialize. 
		*/
		GetInEvent();

		/*!
		Primary destructor for the class.
		*/
		~GetInEvent();

		/*!
		Sets the VBS2 name of the unit that got into the vehicle. 
		*/
		void setUnitName(string name);

		/*!
		Returns the VBS2 name of the unit that got into the vehicle.
		*/
		string getUnitName() const;

		/*!
		Sets the VBS2Fusion alias of the unit that got into the vehicle. This value should be a valid
		alias which can be used to initialize a new unit object using setAlias() to create, update
		and manipulate it. 
		*/
		void setUnitAlias(string alias);

		/*!
		Gets the VBS2Fusion alias of the unit that got into the vehicle. This value should be a valid
		alias which can be used to initialize a new unit object using setAlias() to create, update
		and manipulate it. 
		*/
		string getUnitAlias() const;

		/*!
		Sets the VBS2 ID of the unit that got into the vehicle. 
		*/
		void setUnitID(NetworkID id);

		/*!
		Returns the VBS2 ID of the unit that got into the vehicle.
		*/
		NetworkID getUnitID() const;

		/*!
		Sets the type of role the unit was assigned within the vehicle using 
		a VEHICLEASSIGNMENTTYPE variable. Should be one of:

		- DRIVER
		- GUNNER
		- COMMANDER
		- CARGO
		*/
		void setAssignment(VEHICLEASSIGNMENTTYPE assign);

		/*!
		Returns the type of role the unit was assigned within the vehicle using 
		a VEHICLEASSIGNMENTTYPE variable. Should be one of:

		- DRIVER
		- GUNNER
		- COMMANDER
		- CARGO
		*/
		VEHICLEASSIGNMENTTYPE getAssignment() const;

		/*!
		Returns the type of role the unit was assigned within the vehicle using 
		a string variable. Should be one of:

		- "driver"
		- "gunner"
		- "commander"
		- "cargo"
		*/
		string getAssignmentString();


	private:
		string _strUnitName;
		string _strUnitAlias;
		NetworkID _strUnitID;
		VEHICLEASSIGNMENTTYPE _unitAssignment;
	};

};

#endif //GET_IN_MAN_EVENT_H