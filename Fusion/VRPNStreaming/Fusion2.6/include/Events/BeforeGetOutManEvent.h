/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	BeforeGetOutManEvent.h

Purpose:

	This file contains the declaration of the BeforeGetOutManEvent class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			03-08/2011	CGS: Original Implementation

/************************************************************************/

#ifndef VBS2FUSION_BEFORE_GET_OUT_MAN_EVENT_H
#define VBS2FUSION_BEFORE_GET_OUT_MAN_EVENT_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "Events/Event.h"
/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBS2Fusion
{


	/*!
	Defines a before get out man event which is passed onto the object 
	when an 'BEFOREGETOUTMAN' event is called. 
	*/
	class VBS2FUSION_API BeforeGetOutManEvent: public Event
	{
	public:
		/*!
		Primary constructor for the class. Calls setType(BEFOREGETOUTMAN) to initialize. 
		*/
		BeforeGetOutManEvent();

		/*!
		Primary destructor for the class. 
		*/
		~BeforeGetOutManEvent();

		/*!
		Sets the VBS2 name of the vehicle the unit got out of. 
		*/
		void setVehicleName(string name);

		/*!
		Returns the VBS2 name of the vehicle the unit got out of. 
		*/
		string getVehicleName() const;

		/*!
		Sets the VBS2 ID of the Vehicle that unit got out from. 
		*/
		void setVehicleID(NetworkID id);

		/*!
		Returns the VBS2 ID of the Vehicle that unit got out from.
		*/
		NetworkID getVehicleID() const;

		/*!
		Sets the type of role the unit was assigned within the vehicle using 
		a VEHICLEASSIGNMENTTYPE variable. Should be one of:

		- DRIVER
		- GUNNER
		- COMMANDER
		- CARGO
		*/
		void setAssignment(VEHICLEASSIGNMENTTYPE assign);

		/*!
		Returns the type of role the unit was assigned within the vehicle using 
		a VEHICLEASSIGNMENTTYPE variable. Should be one of:

		- DRIVER
		- GUNNER
		- COMMANDER
		- CARGO
		*/
		VEHICLEASSIGNMENTTYPE getAssignment() const;

		/*!
		Returns the type of role the unit was assigned within the vehicle using 
		a string variable. Should be one of:

		- "driver"
		- "gunner"
		- "commander"
		- "cargo"
		*/
		string getAssignmentString();


	private:
		string _strVehicleName;
		NetworkID _vehicleId;
		VEHICLEASSIGNMENTTYPE _assignment;
	};


};

#endif //BEFORE_GET_OUT_MAN_EVENT_H