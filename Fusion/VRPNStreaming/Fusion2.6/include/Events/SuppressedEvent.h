/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	SuppressedEvent.h

Purpose:

	This file contains the declaration of the SuppressedEvent class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			21-04/2009	RMR: Original Implementation
	2.0			11-01/2009	UDW: Version 2 Implementation
	2.01		10-02/2010	MHA: Comments Checked

/************************************************************************/

#ifndef VBS2FUSION_SUPPRESSED_EVENT_H
#define VBS2FUSION_SUPPRESSED_EVENT_H

#include <sstream>
#include <string>

#include "VBS2Fusion.h"
#include "Events/Event.h"
#include "data/Vehicle.h"

namespace VBS2Fusion
{
	
	/*!
	Defines a Suppressed event which is passed onto the object 
	when an 'SUPPRESSED' event is called. Usually passed into the processSuppressedEvent
	method of a Unit. 

	Triggered each time a unit is suppressed (by incoming rounds).  
	*/
	class VBS2FUSION_API SuppressedEvent: public Event
	{
	public:
		/*!
		Primary constructor for the class. Calls setType(SUPPRESSED) to initialize. 
		*/
		SuppressedEvent();

		/*!
		Primary destructor for the class.
		*/
		~SuppressedEvent();

		/*!
		Sets the VBS2 name of the unit/object which did the firing. 
		*/
		void setFiringObjectName(string name);

		/*!
		Returns the VBS2 name of the unit/object which did the firing. 
		*/
		string getFiringObjectName() const;

		/*!
		Sets the VBS2Fusion alias of the unit/object which did the firing. This alias can be 
		used to initialize a new object, which can then be updated and manipulated. 
		*/
		void setFiringObjectAlias(string alias);

		/*!
		Returns the VBS2Fusion alias of the unit/object which did the firing. This alias can be 
		used to initialize a new object, which can then be updated and manipulated. 
		*/
		string getFiringObjectAlias() const;

		/*!
		Sets the class name of the round which caused suppression. 
		*/
		void setRoundClassName(string name);

		/*!
		Returns the class name of the round which caused suppression. 
		*/
		string getRoundClassName() const;

		/*!
		Sets the nearest distant that round came to the suppressed unit. 
		*/
		void setDistance(double dis);

		/*!
		Returns the nearest distant that round came to the suppressed unit. 
		*/
		double getDistance() const;

		/*!
		Sets the AGL position where the round was closest to the unit. 
		*/
		void setClosestPosition(position3D pos);

		/*!
		Returns the AGL position where the round was closest to the unit. 
		*/
		position3D getClosestPosition() const;

	private:
		string firingObjectName;
		string firingObjectAlias;
		string roundClassName;
		double distance;
		position3D closestPosition;
		
	};
};

#endif //SUPPRESSED_EVENT_H