
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************

Name:

VBS2FusionDefinitions.h

Purpose:

This file contains VBS2Fusion Global ENUM definitions.

Version Information:

Version		Date		Author and Comments
===========================================
2.0			07-12/2009	YFP: Original Implementation
2.01		03-02/2010	MHA: Comments Checked

/************************************************************************/


#ifndef VBS2FUSIONDEFINITIONS_H
#define VBS2FUSIONDEFINITIONS_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <string>

// SIMCENTRIC INCLUDES
#include "position3D.h"


/**********************************************************************/
/* END INCLUDES
/**********************************************************************/

using namespace std;
namespace VBS2Fusion 
{ 

	struct  CUSTOMFORMATION
	{
		position3D offset;
		double angle;
	};

	struct OPTICSTATE
	{
		bool isActive;
		double zoom;
		int visionMode;//0 - day, 1 - night, 2 - thermal vision
		double tiMode;
		string weapon;
	};

	struct WEAPONCARGO
	{
		string weaponName;
		int count;
	};

	struct MAGAZINECARGO
	{
		string magazineName;
		vector<int> countVec;
	};

	struct BLOCKEDSEAT
	{
		string type;
		vector<int> index;
	};

	struct DRAWFRUSTRUM
	{
		int mode;
		double size;
		Color_RGBA color;
	};

	struct FIREARC
	{
		vector3D direction;
		double sideRange;
		double verticalRange;
		bool relative;
		bool enabled;
	};


	struct SHAPE_NAME
	{
		string path;
		int divisor;
		int startingRow;
		int noOfFrames;
	};
	struct PARTICLE_PARAMS_ARRAY
	{
		SHAPE_NAME shapeName;
		string animationName;
		string type;
		float timePeriod;
		float lifeTime;

		position3D position;
		vector3D moveVelocity;
		float rotationVelocity;
		float weight;
		float volume;
		float rubbing;

		vector<float> size;
		vector<Color_RGBA> color;
		vector<float> animationPhase;

		float randamDirectionPeriod;
		float randomDirectionIntensity;
		string onTimer;
		string beforeDestroy;
	};

	struct PARTICLE_RANDOM_ARRAY
	{
		float lifeTime;
		position3D position;
		vector3D moveVelocity;
		float rotationVelocity;
		float size;
		Color_RGBA color;
		float randamDirectionPeriod;
		float randomDirectionIntensity;
	};

/******************************* EFFECTS DEFINITIONS ***********************************/

	enum EFFECTS_TYPE{FIRE1, FIRE2, FIRE3, SMOKE1, SMOKE2, FLOTING_ORB, 
		HEAVY_OILY_SMOKE_SMALL, HEAVY_OILY_SMOKE_MEDIUM, HEAVY_OILY_SMOKE_LARGE, LIGHT_WOOD_SMOKE_SMALL,
		LIGHT_WOOD_SMOKE_MEDIUM, LIGHT_WOOD_SMOKE_LARGE, MIXED_SMOKE_SMALL1, MIXED_SMOKE_SMALL2, MIXED_SMOKE_MEDIUM1, 
		MIXED_SMOKE_MEDIUM2, MIXED_SMOKE_LARGE1, MIXED_SMOKE_LARGE2, ROCK_SHOWER
	};

/******************************* GROUP DEFINITIONS ***********************************/

	enum PATHPOSTPROCESSMODE {PPM_ONLYLEADER, PPM_ALL, PPM_NONE};

/******************************* VEHICLE DEFINITIONS ***********************************/		

 enum VEHICLESPECIALPROPERTIES {VNONE, FLY, FORM};
 enum VEHICLEASSIGNMENTTYPE {DRIVER, COMMANDER, CARGO, GUNNER, TURRET, VEHNONE};
 enum LANDMODE{LAND, GET_IN, GET_OUT};

/******************************** UNIT DEFINITIONS *************************************/	

 enum VBSRANK {PRIVATE, CORPORAL, SERGEANT, LIEUTENANT, CAPTAIN, MAJOR, COLONEL};
 enum UNITPOS {UP , DOWN , AUTO};
 enum SIDE {WEST, EAST, CIVILIAN, RESISTANCE};

 enum BEHAVIOUR {B_CARELESS,B_SAFE,B_AWARE,B_COMBAT,B_STEALTH};

 enum GUNNERINPUT_MODE { GUNNERINPUT_NOTHING,GUNNERINPUT_WEAPON,GUNNERINPUT_HEAD,GUNNERINPUT_WEAPONANDHEAD }; 

 enum BODYPART  { B_ARM_LEFT_HALF, B_ARM_RIGHT_HALF, B_LEG_LEFT_HALF, B_LEG_RIGHT_HALF, B_HEAD, B_ARM_LEFT_FULL, B_ARM_RIGHT_FULL, B_LEG_LEFT_FULL,
	 B_LEG_RIGHT_FULL, B_HEAD_RESET, B_ARM_LEFT_RESET, B_ARM_RIGHT_RESET, B_LEG_LEFT_RESET, B_LEG_RIGHT_RESET };
 enum MAIN_BODYPART {MB_HEAD, MB_LEFT_ARM, MB_RIGHT_ARM, MB_LEFT_LEG, MB_RIGHT_LEG};
 enum BODY_SEGMENT { BS_HEAD, BS_LEFT_HAND, BS_LEFT_FORE_ARM, BS_RIGHT_HAND, BS_RIGHT_FORE_ARM, BS_LEFT_FOOT, BS_LEFT_UPLEG_ROLL, BS_RIGHT_FOOT, BS_RIGHT_UPLEG_ROLL};
 enum FACETYPE{F_MAN, F_CHILD_AFGHAN, F_CHILD_ASIAN,F_CHILD_EUROPEAN, F_CHILDREN, F_MAN_AFRICAN, F_MAN_AMERICAN, F_MAN_AMERICAN_MILITARY, F_MAN_ASIAN, F_BEARED_MIDDLE_EAST,
	F_MAN_EUROPEAN, F_EUROPEAN_MILITARY, F_MAN_INDIAN, F_MAN_MIDDLE_EAST};
 enum MIMIC{FE_DEFAULT, FE_NORMAL, FE_SMILE, FE_HURT, FE_IRONIC, FE_SAD, FE_CYNIC, FE_SURPRISED, FE_AGRESIVE, FE_ANGRY};
 enum LOCK_STATE{LOCKED, UNLOCKED, LOCK_DEFAULT};

 /********************************OBJECT DEFINITIONS ***********************************/
 enum AIBEHAVIOUR {AITARGET, AIAUTOTARGET, AIMOVE, AIANIM,AICOLLISIONAVOID,
					AIGROUNDAVOID,AIPATHPLAN};

/*********************************COLLISION DEFINITIONS*********************************/

enum COLLISIONTESTTYPE {FIRE, VIEW, GEOM, IFIRED};

 /**************************************************************************************/

/*********************************CONTROLLABLE OBJECT DEFINITIONS***********************/
enum CAMMODETYPE{CAMMODE_INTERNAL, CAMMODE_EXTERNAL, CAMMODE_GUNNER, CAMMODE_GROUP};
enum DRAWMODE{DM_NORMAL, DM_TRANSPARENT, DM_WIREFRAME};
/***************************************************************************************/

/*********************************CAMERA OBJECT DEFINITIONS****************************/
enum CAMEFFECTTYPE{CAMEFFECTTYPE_INTERNAL, CAMEFFECTTYPE_EXTERNAL, CAMEFFECTTYPE_FIXED, CAMEFFECTTYPE_FIXEDWITHZOOM, CAMEFFECTTYPE_TERMINATE};
enum CAMEFFECTPOS{CAMEFFECTPOS_TOP, CAMEFFECTPOS_LEFT, CAMEFFECTPOS_RIGHT, CAMEFFECTPOS_FRONT, CAMEFFECTPOS_BACK};
enum CAMEFFECTMODE{VISIBLE,NIGHTVISION,THERMAL};
enum CAMCOMMANDTYPE{CAMCMD_MANUAL_ON, CAMCMD_MANUAL_OFF,CAMCMD_INERTIA_ON,CAMCMD_INERTIA_OFF,CAMCMD_LANDED,CAMCMD_AIRBORNE};
/*************************************************************************************/

/*********************************WAYPOINT OBJECT DEFINITIONS****************************/
enum WAYPOINTTYPE{MOVE, DESTROY, GETIN, SAD, JOIN, LEADER, GETOUT, CYCLE, LOAD, UNLOAD, TR_UNLOAD, HOLD, SENTRY, GUARD, TALK, SCRIPTED, SUPPORT, GETIN_NEAREST, AND, OR ,LOITER};
enum WAYPOINTCOMBATMODE{NO_CHANGE, BLUE, GREEN, WHITE, YELLOW, RED};
enum WAYPOINTBEHAVIOUR {UNCHANGED, CARELESS, SAFE, AWARE, COMBAT, STEALTH};
enum WAYPOINTSPEEDMODE {UNCHANGD, LIMITED, NORMAL, FULL};
enum FORMATION{COLUMN, STAG_COLUMN, WEDGE, ECH_LEFT, ECH_RIGHT, VEE, LINE,FILE,DIAMOND,NONE};	
enum WAYPOINTSHOWMODE{NEVER, EASY, ALWAYS};
/**************************************************************************************/

/*********************************TRIGGER OBJECT DEFINITIONS***********************/

enum TRIGGERACTIVATION{TNONE, TEAST, TWEST, TGUER, TCIV, TLOGIC, TANY, TALPHA, TBRAVO, TCHARLIE, TDELTA, TECHO, TFOXTROT, 
TGOLF, THOTEL, TINDIA, TJULIET, TSTATIC, TVEHICLE, TGROUP, TLEADER, TMEMBER, TWEST_SEIZED, TEAST_SEIZED, TGUER_SEIZED};
enum TRIGGERPRESENCE {PRESENT, NOT_PRESENT, WEST_D, EAST_D, GUER_D, CIV_D};
enum TRIGGERTYPE {TTNONE, SWITCH, END1, END2, END3, END4, END5, END6, LOOSE, WIN};
/***************************************************************************************/

/******************************MINE DEFINITIONS****************************************/
enum MINETYPE{VBS2_MINEAT_M15, VBS2_MINEAT_M19, VBS2_MINEAT_TM46, VBS2_MINEAT_TM62M};

/*****************************SIMULATION MODE*******************************************/
enum SIMULATION_MODE{SIM_NORMAL, SIM_FROZEN, SIM_HIDDEN};

/*****************************MISSION CONFIG DEFINITION*********************************/
enum ROADSIDE{ROAD_LEFT,ROAD_MIDDLE,ROAD_RIGHT};
enum ENDTYPE{ET_CONTINUE, ET_KILLED, ET_LOST, ET_END1, ET_END2, ET_END3, ET_END4, ET_END5, ET_END6};
//--------------------------------------------------------------------------------------

/*****************************GEOM OBJECT DEFINITIONS**********************************/

enum GEOMTYPE{GEOM_HOUSE, GEOM_TREE, GEOM_LANDSCAPE, GEOM_ROAD};
//-------------------------------------------------------------------------------------

/*******************************TERRAIN DEFINITIONS************************************/
enum COST_TYPE{COST_AVOID, COST_AVOIDBUSH, COST_AVOIDTREE, COST_NORMAL, COST_ROAD, COST_ROADFORCED, COST_SPACE, COST_SPACEBUSH, COST_SPACEHARDBUSH, COST_SPACEROAD, COST_SPACETREE, COST_WATER};
//--------------------------------------------------------------------------------------

/*********************************KEY BIND RULES**************************************/

enum KEY_BIND_RULE{KBR_BUTTON_PRESS, KBR_VALUE_CHANGE, KBR_SIMULATION_STEP};
//-------------------------------------------------------------------------------------
/*********************************WORLD DEFINITION**************************************/

enum LIGHT_MODE{LIGHT_ON, LIGHT_OFF, LIGHT_AUTO};
//-------------------------------------------------------------------------------------

enum APPLICATIONSTATE{_,AAR, AAR_SELECT, DEBRIEFING, MAIN_MENU, MISSION, MISSION_INTERUPT, MP_CLIENT_WAITING,
MP_PARTICIPANTS, MP_SELECT_ROLE, MP_SELECT_SERVER, MP_SERVER_GET_READY, MP_SERVER_SELECT_MISSION, OME, OME_PREVIEW,
OME_SELECT_ISLAND, OPTIONS, OPTIONS_AUDIO, OPTIONS_CONTROLS, OPTIONS_DIFFICULTY, OPTIONS_VIDEO, RTE, RTE_PREVIEW, SP_SELECT_MISSION};

/*!
	returns the name string for the application state
*/
string getApplicationStateString(APPLICATIONSTATE state);

enum SKELETON_TYPE{
	NECK = 0 ,
	NECKLOW,
	HEAD,
	LEFT_BROW,
	MIDDLE_BROW,
	RIGHT_BROW,
	LEFT_MOUTH,
	MIDDLE_MOUTH ,
	RIGHT_MOUTH,
	EYELIDS,
	LIP,
	WEAPON,
	LAUNCHER,
	CAMERA,
	SPINE,
	SPINE1,
	SPINE2,
	SPINE3,
	PELVIS,
	LEFT_SHOULDER,
	LEFT_ARM,
	LEFT_ARM_ROLL,
	LEFT_FOREARM,
	LEFT_FOREARM_ROLL,
	LEFT_HAND,
	LEFT_HAND_RING,
	LEFT_HAND_RING1,
	LEFT_HAND_RING2,
	LEFT_HAND_RING3,
	LEFT_HAND_PINKY1,
	LEFT_HAND_PINKY2,
	LEFT_HAND_PINKY3,
	LEFT_HAND_MIDDLE1,
	LEFT_HAND_MIDDLE2,
	LEFT_HAND_MIDDLE3,
	LEFT_HAND_INDEX1,
	LEFT_HAND_INDEX2,
	LEFT_HAND_INDEX3,
	LEFT_HAND_THUMB1,
	LEFT_HAND_THUMB2,
	LEFT_HAND_THUMB3,
	RIGHT_SHOULDER,
	RIGHT_ARM,
	RIGHT_ARM_ROLL,
	RIGHT_FOREARM,
	RIGHT_FOREARM_ROLL,
	RIGHT_HAND,
	RIGHT_HAND_RING,
	RIGHT_HAND_RING1,
	RIGHT_HAND_RING2,
	RIGHT_HAND_RING3,
	RIGHT_HAND_PINKY1,
	RIGHT_HAND_PINKY2,
	RIGHT_HAND_PINKY3,
	RIGHT_HAND_MIDDLE1,
	RIGHT_HAND_MIDDLE2,
	RIGHT_HAND_MIDDLE3,
	RIGHT_HAND_INDEX1,
	RIGHT_HAND_INDEX2,
	RIGHT_HAND_INDEX3,
	RIGHT_HAND_THUMB1,
	RIGHT_HAND_THUMB2,
	RIGHT_HAND_THUMB3,
	LEFT_UP_LEG,
	LEFT_UP_LEG_ROLL,
	LEFT_LEG,
	LEFT_LEG_ROLL,
	LEFT_FOOT,
	LEFT_TOEBASE,
	RIGHT_UP_LEG,
	RIGHT_UP_LEG_ROLL,
	RIGHT_LEG,
	RIGHT_LEG_ROLL,
	RIGHT_FOOT,
	RIGHT_TOEBASE
};

/*!
returns the name string for path post process mode
*/
string getPathPostProcessModeString(PATHPOSTPROCESSMODE mode);

/*!
returns the name string for waypoint speed mode
*/
string getWayPointSpeedModeString(WAYPOINTSPEEDMODE speedMode);

/*!
returns the name string for the Formation
*/
string getFormationString(FORMATION formation);

/*!
returns the name string for the LockState
*/
string getLockStateString(LOCK_STATE state);

/*!
returns the name string for the light mode
*/
string getLightModeString(LIGHT_MODE mode);

/*!
returns the name string for the unit's face Mimic
*/
string getWayPointCombatModeString(WAYPOINTCOMBATMODE mode);

/*!
returns the name string for the unit's face Mimic
*/
string getMiminString(MIMIC mimic);

/*!
returns the name string for the unit's face type 
*/
string getFaceTypeString(FACETYPE face);

/*!
returns the name string for land mode of planes
*/
string getLandModeString(LANDMODE mode);

/*!
returns the name string for the unit's body parts
*/
string getBodypartString(BODYPART bodyPart);

/*!
returns the name string for the unit's body parts
*/
string getMainBodypartString(MAIN_BODYPART bodyPart);

/*!
returns the name string for the unit's body parts
*/
string getBodySegmentString(BODY_SEGMENT segment);

/*!
returns the name string for the vehicle Assignment Type
*/
string getVehicleAssignmentTypeString(VEHICLEASSIGNMENTTYPE type);

/*!
	returns the name string for the unit's AI behavior.
*/
 string getAIBehaviourString(AIBEHAVIOUR behaviour); 

  /*!
	returns the name string for the unit behavior.
 */
 string getBehaviourString(BEHAVIOUR behaviour);

  /*!
	returns the name string for the unit rank.
 */
 string getRankString(VBSRANK rank);

  /*!
	returns the name string for the collision test type.
 */
 string getCollisionTestTypeString(COLLISIONTESTTYPE testType);

  /*!
	returns the name string for the unit position.
 */
 string getUnitPosString(UNITPOS pos);

  /*!
	returns the name string for the camera mode.
 */
 string getCamModeString(CAMMODETYPE camMode);

 /*!
 returns the name string for camera effect mode.
 */
 string getCamEffectModeString(CAMEFFECTMODE cameffectMode);

/*!
returns the command type string of Camera command type. 
*/
 string getCamCommandTypeString(CAMCOMMANDTYPE camcomtype);

  /*!
	returns the name string for the Side.
 */
 string getSideString(SIDE side);

 /*!
	returns the enum SIDE
*/
 SIDE getSide(string strSide);

 /*!
	returns the name string for the Mine type.
 */
 string getMineTypeString(MINETYPE type);

/*!
	returns simulation value. 
	NORMAL  - 0
	FROZEN  - 1
	HIDDEN  - 2
*/
 int getSimulationModeValue(SIMULATION_MODE mode);

 /*!
	returns the road side.
	ROAD_LEFT - left
	ROAD_MIDDLE - middle 
	ROAD_RIGHT - right
 */
 string getRoadSideString(ROADSIDE side);

/*!
 return the COST_TYPE Enum.
*/
 COST_TYPE getCostType(string strCost);

 /*!
 return the name string of the cost type.
 */
 string getCostTypeString(COST_TYPE type);

 /*!
 return the name string of the draw mode.
 */
 string getDrawModeString(DRAWMODE mode);

};

#endif // VBS2FUSIONDEFINITIONS_H