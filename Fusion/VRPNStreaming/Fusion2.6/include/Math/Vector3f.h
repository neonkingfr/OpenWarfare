
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:

Purpose:

	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			23-12/2011	YFP: Original Implementation

/*****************************************************************************/

#ifndef VBS2FUSION_VECTOR3D_H
#define VBS2FUSION_VECTOR3D_H


/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/
namespace VBS2Fusion
{
	class VBS2FUSION_API Vector3f
	{
	public:
		/*!
		Main constructor for this class. The following parameters are initialized as follows:
		- x,y,z =0
		*/
		Vector3f();

		/*!
		Main destructor for this class. 
		*/
		~Vector3f();

		/*!
		Main constructor for this class.
		- It creates vector with given values
		*/
		Vector3f(float x,float z,float y);

		/*!
		Copy constructor for the Vector3f class. 
		*/
		Vector3f(const Vector3f &vec);

		/*!
		The assignment operator for Vector3f.
		*/
		Vector3f & operator = (const Vector3f& rhs);

		/*!
		The negative operator for Vector3f.
		*/
		Vector3f operator -() const;

		/*!
		The addition operator for Vector3f.
		*/
		Vector3f operator +(const Vector3f& rhs);

		/*!
		The deduction operator for Vector3f.
		*/
		Vector3f operator -(const Vector3f& rhs);

		/*!
		The Equal operator for Vector3f.
		*/
		bool operator ==(const Vector3f& rhs);

		/*!
		Scalar multiplication for Vector3f.
		*/
		Vector3f operator *(const float &num);

		/*!
		Dot product for Vector3f.
		*/
		float Dot(const Vector3f& vec) const;

		/*!
		Cross product for Vector3f.
		*/
		Vector3f Cross( const Vector3f& vec) const; 

		/*!
		Dot product for Vector3f.
		*/
		float DotProduct(const Vector3f& vec) const;

		/*!
		Cross product for Vector3f.
		*/
		Vector3f CrossProduct( const Vector3f& vec) const; 

		/*!
		Normalize this vector.
		It means convert the vector value in the range of 0 -> 1.
		*/
		void Normalize();

		/*!
		get x value of Vector3f.
		*/
		 float X() const;

		 /*!
		get y value of Vector3f.
		*/
		 float Y() const;

		 /*!
		get z value of Vector3f.
		*/
		 float Z() const;

		 /*!
		set x value of Vector3f.
		*/
		 void setX(float x);

		 /*!
		set z value of Vector3f.
		*/
		 void setZ(float z);

		 /*!
		set y value of Vector3f.
		*/
		 void setY(float y);


	private:
		float _x, _y, _z;
	};
}

#endif