
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************

Name:

	VBS2Fusion.h

Purpose:

	This file contains the declaration of the VBS2Fusion class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			23-03/2009	RMR: Original Implementation
	1.001		18-09/2009	RMR: Added the VisibilityUtilities class to the definition list
	2.01		10-02/2010	MHA: Comments checked
	2.02        14-06/2010  YFP: added new typedef vector3D

/************************************************************************/

#ifndef VBS2Fusion_H
#define VBS2Fusion_H

#define VBS2FUSION_ERROR -1

#include <windows.h>

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#ifdef BISIM_DEV
#include "VBS2FusionBISIM.h"
#endif

#include "VBS2FusionDeprecationDef.h"


#ifdef VBS2FUSION_EXPORTS
#define VBS2FUSION_API __declspec(dllexport)
#else
#define VBS2FUSION_API __declspec(dllimport)
#endif

using namespace std;


typedef int (WINAPI * ExecuteCommandType)(const char *command, char *result, int resultLength);

VBS2FUSION_API void WINAPI setExecuteCommandFunction(void *executeCommandFnc);

VBS2FUSION_API void WINAPI reloadObjects();


namespace VBS2Fusion
{
	
	class conversions;
	class DisplayFunctions;
	class position2D;
	class position3D;
	class VBS2Fusion_ErrorContext;
	class VBS2FusionAppContext;
	class VBS2FusionDefinitions;


#if WEAPON_HANDLING_OLD
	class ArmedGroundVehicle;
	class ArmedModule;
#endif
	class Camera;
	class ControllableObject;
	class EnvironmentState;
	class Explosive;
#if GEOM_OBJECT
	class GeomObject;
#endif
	class GroundVehicle;
	class Group;
	class GroupList;
	class Magazine;
	class MapObject;
	class Marker;
	class Mine;
	class Mission;
	class NetworkID;
	class Projectile;
#if GEOM_OBJECT
	class RoadNetwork;
	class RoadSegment;
#endif
	class Target;
	class Trigger;
	class TriggerList;
	class Turret;
	class Unit;
	class UnitArmedModule;
	class vbs2Types;
	class VBS2Variable;
	class Vehicle;
	class VehicleArmedModule;
	class VehicleList;
	class Waypoint;
	class Weapon;
	class Event;

	class EventHandler;
	class EventHandlerEx;

 class AmmoExplodeEvent;
 class AnimChangedEvent;
 class AnimDoneEvent;
 class AttachToEvent;
 class BeforeKilledEvent;
 class CargoChangedEvent;
 class DamagedEvent;
 class DeleteEvent;
 class EngineEvent;
 class FiredEvent;
 class FuelEvent;
 class GearEvent;
 class GetInEvent;
 class GetOutEvent;
 class GetInManEvent;
 class GetOutManEvent;
 class HitEvent;
 class HitPartEvent;
 class IncomingMissileEvent;
 class InitEvent;
 class KilledEvent;
 class LandedStoppedEvent;
 class LandedTouchDownEvent;
 class LoadOutChangedEvent;
 class PositionChangedEvent;
 class RespawnEvent;
 class SuppressedEvent;
 class TurnInEvent;
 class TurnOutEvent;
 class WaypointCompleteEvent;
  class VBS2EventTypes;
 class ChangedWeaponEvent;


	class AttachToEvent;
	class DeleteEvent;
	class FiredEvent;
	class GetInManEvent;
	class GetOutManEvent;
	class HitPartEvent;
	class RespawnEvent;
	class SuppressedEvent;
	class WaypointCompleteEvent;
	class AmmoExplodeEvent;
	class CargoChangedEvent;
	class LoadOutChangedEvent;
	class TurnInEvent;
	class TurnOutEvent;



	class VBS2EventTypes;
	class AnimChangedEvent;
	class AnimDoneEvent;
	class BeforeKilledEvent;
	class CreateEvent;
	class ChangedWeaponEvent;
	class DeletedEvent;
	class DamagedEvent;
	class EngineEvent;
	class FuelEvent;
	class GearEvent;
	class GetInEvent;
	class GetOutEvent;
	class GroupChangedEvent;
	class GroupChangedRTEEvent;
	class HitEvent;
	class IncomingMissileEvent;
	class InitEvent;
	class KilledEvent;
	class LandedStoppedEvent;
	class LandedTouchDownEvent;
	class PositionChangedEvent;
	
	struct VBS2Var;

	class AARUtilities;
	class TitleTextUtilities;
#if WEAPON_HANDLING_OLD
	class ArmedGroundVehicleUtilities;
	class ArmedModuleUtilities;
#endif
	class CameraUtilities;
	class CollisionUtilities;
	class ControllableObjectUtilities;
	class EffectsUtilities;
	class EnvironmentStateUtilities;
	class ErrorHandleUtilities;
	class ExecutionUtilities;
	class ExplosiveUtilities;
	class GeneralUtilities;
	class GroupListUtilities;
	class GroupUtilities;

	class InputUtilities;

	class MarkerUtilities;
	class MapUtilities;
	class MineUtilities;
	class MissionConfigUtilities;
	class MissionUtilities;
	class ObjectVectorUtilities;
	class OrderedCommandQueue;
	class PlayerUtilities;
	class PoseControlUtilities;
	class PositionUtilities;
#if GEOM_OBJECT
	class RoadNetworkUtilities;
	class RoadSegmentUtilities;
#endif
	class RTEUtilities;
	class TerrainUtilities;
	class TIUtilities;
	class TriggerListUtilities;
	class TriggerUtilities;
#if WEAPON_HANDLING_OLD
	class TurretUtilities;
#endif
	class UnitUtilities;
	class VariableUtilities;
	class vbs2TypesUtilities;
	class VehicleListUtilities;
	class VehicleUtilities;
	class VisibilityUtilities;
	class WaypointUtilities;
	class WeaponUtilities;
	class WorldUtilities;


	/*! struct for store position in the UTM system. */
	struct UTM_POSITION
	{
		double EASTING;
		double NORTHING;
		double ZONE;
		string HEMISPHERE;
	};

	struct Color_RGBA 
	{
		double r;
		double g;
		double b;
		double a;
	};

	struct Color_RGB 
	{
		double r;
		double g;
		double b;
	};

	typedef position3D vector3D;

};

#endif //VBS2Fusion_H

