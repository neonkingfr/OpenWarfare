
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************

Name:

conversions.h

Purpose:

This file contains the declaration of the conversions class.

Version Information:

Version		Date		Author and Comments
===========================================
2.0			24-03/2009	RMR: Original Implementation
2.01		05-02/2010	MHA: Comments Checked
2.02		13-06/2011	SSD: Added	stringToUpper(string s);
									stringToLower(string s);
2.03		26-07/2011  NDB: Added	position3D convertToASL(position 3D)
									position3D convertToAGL(position 3D)


/************************************************************************/

#ifndef VBS2FUSION_CONVERSIONS_H
#define VBS2FUSION_CONVERSIONS_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "data/EnvironmentState.h"
#include "position3D.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/


namespace VBS2Fusion
	{

	class VBS2FUSION_API conversions
		{

		public:

			/*!
			Convert a double to an integer. 
			*/
			static int DoubleToInt(double d);

			/*!
			Convert a float to an integer.
			*/
			static int FloatToint(float f);

			//****************************************************************************
			/*!
			Convert a string to a double
			*/
			static double stringToDouble(string s);

			/*!
			Convert a string to a float
			*/
			static float stringToFloat(string s);

			/*!
			Convert a string to an integer
			*/
			static int stringToInt(string s);

			//****************************************************************************

			/*!
			Convert a double to string
			*/
			static string DoubleToString(double d);		

			/*!
			Convert a float to string
			*/
			static string FloatToString(float f);

			/*!
			Convert an integer to string
			*/
			static string IntToString(int i);

			////********Functions Implement to access from outside the VBS2Fusion dll******************	

			///*!
			//Convert ENUM Airplane index to String representation of Airplane type
			//*/
			//static string AirplaneToString(vbs2Type::Airplane::AIRPLANE index)
			//{
			//	vbs2Type::Airplane tmpPlane;
			//	return tmpPlane.airplane[index];
			//}

			///*!
			//Convert ENUM Car index to String representation of Car type
			//*/
			//static string CarToString(vbs2Type::Car::CAR index)
			//{
			//	vbs2Type::Car tmpCar;
			//	return tmpCar.car[index];
			//}

			///*!
			//Convert ENUM Helicopter index to String representation of Helicopter type
			//*/
			//static string HelicopterToString(vbs2Type::Helicopter::HELICOPTER index)
			//{
			//	vbs2Type::Helicopter tmpHelicopter;
			//	return tmpHelicopter.helicopter[index];
			//}


			///*!
			//Convert ENUM Soldier index to String representation of Soldier type
			//*/
			//static string SoldierToString(vbs2Type::Soldier::SOLDIER index)
			//{
			//	vbs2Type::Soldier tmpSoldier;
			//	return tmpSoldier.soldier[index];
			//}


			///*!
			//Convert ENUM Tank index to String representation of Tank type
			//*/
			//static string TankToString(vbs2Type::Tank::TANK index)
			//{
			//	vbs2Type::Tank tmpTank;
			//	return tmpTank.tank[index];
			//}


			///*!
			//Convert ENUM Vasi index to String representation of Vasi type
			//*/
			//static string VasiToString(vbs2Type::Vasi::VASI index)
			//{
			//	vbs2Type::Vasi tmpVasi;
			//	return  tmpVasi.vasi[index];
			//}



			//****************************************************************************

			/*!
			Tokenize the string str using the delimiters specified by delimiters
			and return in the vector tokens. 

			for example, given a string of the form [23, 34, 45], if TokenizeResponseString is 
			called using the tokens [,] the function would return a vector containing the three
			strings "23", "34" and "45". This function strips the square brackets (i.e. '[' and ']') from
			the ends of the string str. 

			*/
			static void TokenizeResponseString(std::string& str, vector<std::string>& tokens, const std::string& delimiters = ",");

			/*!
			Tokenize the string str using the delimiters specified by delimiters
			and return in the vector tokens. 

			for example, given a string of the form [23, 34, 45], if TokenizeResponseString is 
			called using the tokens [,] the function would return a vector containing the three
			strings "23", "34" and "45". This function strips the square brackets (i.e. '[' and ']') from
			the ends of the string str. 
			If there any empty string, It will Add empty string in return string vector.
			eg:input string 'str' is "[3,,4]", then return 'tokens' as "3","","4"
			*/
			static void TokenizeResponseStringEx(std::string str, vector<std::string>& tokens, const std::string& delimiters = ",");

			/*!
			Converts the vector of string specified by strTokens into a vector of integers. 
			*/
			static void ConvertStringTokensToInt(const vector<std::string>& strTokens, vector<int>& intTokens);

			/*!
			Converts the vector of string specified by strTokens into a vector of floats. 
			*/
			static void ConvertStringTokensToFloat(const vector<std::string>& strTokens, vector<float>& floatTokens);

			/*!
			Converts the vector of string specified by strTokens into a vector of doubles. 
			*/
			static void ConvertStringTokensToDouble(const vector<std::string>& strTokens, vector<double>& doubleTokens);

			//****************************************************************************

			/*!
			Strip inverted commas from the beginning and end of the string if present. 
			Particularly useful when VBS2 returns strings, which are usually within 
			inverted commas. 
			*/
			static string stripInvertedCommasFromEnds(string conversionString);


			/*!
			Return the name of the plugin which calls this function. If plugin name
			cannot be found this will return DefaultLog.txt. Use for error handling purposes.
			*/
			static  string processPluginName();

			/*!
			Converts a boolean into a string. Creates the string in uppercase if uppercase is set to true. Default operation 
			creates a lowercase string (i.e. "true", "false")
			*/
			static string BoolToString(bool value, bool uppercase = false);

			/*!
			Converts a String into a boolean. 
			*/
			static bool StringToBool(string value);

			/*!
			 Convert the given Time into seconds. Time can be consists of Hours,Minutes and Seconds. 
			*/		
			static int TimeToSeconds(Time time);

			/*!
			Calculate the given time variables to seconds.
			*/
			static int TimeToSeconds(int hours, int minutes, int seconds);

			//****************************************************************************
			/*!
			Convert the input string to upper case string
			*/
			static string stringToUpper(string s);

			/*!
			Convert the input string to lower case string
			*/
			static string stringToLower(string s);	

			/*!
			Converts the given position to height above ground level (AGL).
			*/
			static position3D convertToAGL(position3D position);

			/*!
			Converts the given position to height above sea level (ASL).
			*/
			static position3D convertToASL(position3D position);
		};

	};

#endif //CONVERSIONS_H