
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************

Name:

VBS2FusionDefinitions.h

Purpose:

This file contains VBS2Fusion Global ENUM definitions.

Version Information:

Version		Date		Author and Comments
===========================================
2.0			07-12/2009	YFP: Original Implementation
2.01		03-02/2010	MHA: Comments Checked

/************************************************************************/


#ifndef VBS2FUSIONDEFINITIONS_H
#define VBS2FUSIONDEFINITIONS_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <string>

// SIMCENTRIC INCLUDES
#include "position3D.h"
#include "position2D.h"
//#include <d3d9.h>
//#include <d3dx9.h>


/**********************************************************************/
/* END INCLUDES
/**********************************************************************/

using namespace std;
namespace VBS2Fusion 
{ 

	struct  CUSTOMFORMATION
	{
		position3D offset;
		double angle;
	};

	struct OPTICSTATE
	{
		bool isActive;
		double zoom;
		int visionMode;//0 - day, 1 - night, 2 - thermal vision
		double tiMode;
		string weapon;
		int transition;
		int device;
		int mode;
		OPTICSTATE():isActive(false),visionMode(0),tiMode(0),weapon(""),transition(-1),device(-1),mode(-1)
		{};
	};

	struct WEAPONCARGO
	{
		string weaponName;
		int count;
	};

	struct MAGAZINECARGO
	{
		string magazineName;
		vector<int> countVec;
	};

	struct BLOCKEDSEAT
	{
		string type;
		vector<int> index;
	};

	struct DRAWFRUSTRUM
	{
		int mode;
		double size;
		Color_RGBA color;
	};

	struct DRAWSTATIONARY
	{
		double drawType;
		double maxSize;
		double growRate;
		Color_RGBA color;
		double offset;
	};

	struct FIREARC
	{
		vector3D direction;
		double sideRange;
		double verticalRange;
		bool relative;
		bool enabled;
	};


	struct SHAPE_NAME
	{
		string path;
		int divisor;
		int startingRow;
		int noOfFrames;
	};
	struct PARTICLE_PARAMS_ARRAY
	{
		SHAPE_NAME shapeName;
		string animationName;
		string type;
		float timePeriod;
		float lifeTime;

		position3D position;
		vector3D moveVelocity;
		float rotationVelocity;
		float weight;
		float volume;
		float rubbing;

		vector<float> size;
		vector<Color_RGBA> color;
		vector<float> animationPhase;

		float randamDirectionPeriod;
		float randomDirectionIntensity;
		string onTimer;
		string beforeDestroy;
	};

	struct DYNAMIC_PARTICLE_PARAMS
	{
		string shapeName;
		string animationName;
		string type;
		float timePeriod;
		float lifeTime;

		position3D position;
		vector3D moveVelocity;
		float rotationVelocity;
		float weight;
		float volume;
		float rubbing;

		vector<float> size;
		vector<Color_RGBA> color;
		vector<float> animationPhase;

		float randamDirectionPeriod;
		float randomDirectionIntensity;
		string onTimer;
		string beforeDestroy;

		string object;
	};

	struct PARTICLE_RANDOM_ARRAY
	{
		float lifeTime;
		position3D position;
		vector3D moveVelocity;
		float rotationVelocity;
		float size;
		Color_RGBA color;
		float randamDirectionPeriod;
		float randomDirectionIntensity;
	};

	//Camera configurations for multiChanneling
	struct CONFIG_VALUES_MULTICHANNEL
	{
		string name;
		double leftAngle ;
		double rightAngle ;
		double bottomAngle ; 
		double topAngle ; 
		double azimuth; 
		double pitch ; 
		double roll;
		double xPos ; 
		double yPos ;
		double zPos ;
	};
	


	struct FILEINFO
	{
		string filename;		// filename 
		string dir_filename;	// dir\filename
	};

	/*!
	struct for hold collision information
	*/
	struct COLLISION_INFO
	{	
		bool isCollied;
		position3D normal;
		position3D firstCollision;
		COLLISION_INFO():isCollied(false)
		{}
	};


	/*!
	struct for hold intersect properties
	*/

	struct INTERSECT_INFO
	{
		string name;
		double distance;
		position3D normal;

	};
	
	/*!
	struct for hold marker layer information
	*/
	struct MARKER_LAYER
	{
		string path;
		int sizeX, sizeY;
		int offsetX, offsetY;
		MARKER_LAYER():sizeX(0), sizeY(0), offsetX(0), offsetY(0)
		{}
	};

/******************************* EFFECTS DEFINITIONS ***********************************/

	enum EFFECTS_TYPE{FIRE1, FIRE2, FIRE3, SMOKE1, SMOKE2, FLOTING_ORB, 
		HEAVY_OILY_SMOKE_SMALL, HEAVY_OILY_SMOKE_MEDIUM, HEAVY_OILY_SMOKE_LARGE, LIGHT_WOOD_SMOKE_SMALL,
		LIGHT_WOOD_SMOKE_MEDIUM, LIGHT_WOOD_SMOKE_LARGE, MIXED_SMOKE_SMALL1, MIXED_SMOKE_SMALL2, MIXED_SMOKE_MEDIUM1, 
		MIXED_SMOKE_MEDIUM2, MIXED_SMOKE_LARGE1, MIXED_SMOKE_LARGE2, ROCK_SHOWER
	};

/******************************* GROUP DEFINITIONS ***********************************/

	enum PATHPOSTPROCESSMODE {PPM_ONLYLEADER, PPM_ALL, PPM_NONE};

/******************************* VEHICLE DEFINITIONS ***********************************/		

 enum VEHICLESPECIALPROPERTIES {VNONE, FLY, FORM};
 enum VEHICLEASSIGNMENTTYPE {DRIVER, COMMANDER, CARGO, GUNNER, TURRET, VEHNONE};
 enum LANDMODE{LAND, GET_IN, GET_OUT};

/******************************** UNIT DEFINITIONS *************************************/	

 enum VBSRANK {PRIVATE, CORPORAL, SERGEANT, LIEUTENANT, CAPTAIN, MAJOR, COLONEL};
 enum UNITPOS {UP , DOWN , AUTO};
 enum SIDE {WEST, EAST, CIVILIAN, RESISTANCE};

 enum BEHAVIOUR {B_CARELESS,B_SAFE,B_AWARE,B_COMBAT,B_STEALTH};

 enum GUNNERINPUT_MODE { GUNNERINPUT_NOTHING,GUNNERINPUT_WEAPON,GUNNERINPUT_HEAD,GUNNERINPUT_WEAPONANDHEAD }; 

 enum BODYPART  { B_ARM_LEFT_HALF, B_ARM_RIGHT_HALF, B_LEG_LEFT_HALF, B_LEG_RIGHT_HALF, B_HEAD, B_ARM_LEFT_FULL, B_ARM_RIGHT_FULL, B_LEG_LEFT_FULL,
	 B_LEG_RIGHT_FULL, B_HEAD_RESET, B_ARM_LEFT_RESET, B_ARM_RIGHT_RESET, B_LEG_LEFT_RESET, B_LEG_RIGHT_RESET };
 enum MAIN_BODYPART {MB_HEAD, MB_LEFT_ARM, MB_RIGHT_ARM, MB_LEFT_LEG, MB_RIGHT_LEG};
 enum BODY_SEGMENT { BS_HEAD, BS_LEFT_HAND, BS_LEFT_FORE_ARM, BS_RIGHT_HAND, BS_RIGHT_FORE_ARM, BS_LEFT_FOOT, BS_LEFT_UPLEG_ROLL, BS_RIGHT_FOOT, BS_RIGHT_UPLEG_ROLL};
 enum FACETYPE{F_MAN, F_CHILD_AFGHAN, F_CHILD_ASIAN,F_CHILD_EUROPEAN, F_CHILDREN, F_MAN_AFRICAN, F_MAN_AMERICAN, F_MAN_AMERICAN_MILITARY, F_MAN_ASIAN, F_BEARED_MIDDLE_EAST,
	F_MAN_EUROPEAN, F_EUROPEAN_MILITARY, F_MAN_INDIAN, F_MAN_MIDDLE_EAST};
 enum MIMIC{FE_DEFAULT, FE_NORMAL, FE_SMILE, FE_HURT, FE_IRONIC, FE_SAD, FE_CYNIC, FE_SURPRISED, FE_AGRESIVE, FE_ANGRY};
 enum LOCK_STATE{LOCKED, UNLOCKED, LOCK_DEFAULT};

 enum UNITMOVE{
	 AMOVPERCMSTPSNONWNONDNON_COUGHING_V1,
	 AMOVPERCMSTPSNONWNONDNON_COUGHING_V2,
	 AMOVPERCMSTPSNONWNONDNON_COUGHING_V3,
	 AMOVPERCMSTPSNONWNONDNON_COUGHING_V4,
	 AMOVPERCMSTPSNONWNONDNON_COUGHING_V5,
	 AMOVPERCMSTPSNONWNONDNON_CHECKSIX_V1,
	 AMOVPERCMSTPSNONWNONDNON_CHECKSIX_V2,
	 AMOVPERCMSTPSNONWNONDNON_CHECKSIX_V3,
	 AMOVPERCMSTPSNONWNONDNON_CHECKSIX_V4,
	 AMOVPERCMSTPSNONWNONDNON_COMEHERE,
	 AMOVPERCMSTPSNONWNONDNON_CONTEMPT,
	 AMOVPERCMSTPSNONWNONDNON_CONTEMPTSLAP,
	 AMOVPERCMSTPSNONWNONDNON_GOAWAY,
	 AMOVPERCMSTPSNONWNONDNON_GREETING,
	 AMOVPERCMSTPSNONWNONDNON_RUBBING_HANDS_FAST_V1,
	 AMOVPERCMSTPSNONWNONDNON_RUBBING_HANDS_FAST_V2,
	 AMOVPERCMSTPSNONWNONDNON_RUBBING_HANDS_FAST_V3,
	 AMOVPERCMSTPSNONWNONDNON_RUBBING_HANDS_FAST_V4,
	 AMOVPERCMSTPSNONWNONDNON_RUBBING_HANDS_FAST_V5,
	 AMOVPERCMSTPSNONWNONDNON_SMOKINGCASUAL_V1_LONG,
	 AMOVPERCMSTPSNONWNONDNON_SMOKINGCASUAL_V1_SHORT,
	 AMOVPERCMSTPSNONWNONDNON_SMOKINGCASUAL_V2_LONG,
	 AMOVPERCMSTPSNONWNONDNON_SMOKINGCASUAL_V2_SHORT,
	 AMOVPERCMSTPSNONWNONDNON_SMOKINGCASUAL_V3_LONG,
	 AMOVPERCMSTPSNONWNONDNON_SMOKINGCASUAL_V3_SHORT,
	 AMOVPERCMSTPSNONWNONDNON_SMOKINGTALKINGCASUAL_V1_COMPLEX_1,
	 AMOVPERCMSTPSNONWNONDNON_SMOKINGTALKINGCASUAL_V1_COMPLEX_2,
	 AMOVPERCMSTPSNONWNONDNON_TEARGAS_V1,
	 AMOVPERCMSTPSNONWNONDNON_TEARGAS_V2,
	 AMOVPERCMSTPSNONWNONDNON_TEARGAS_V3,
	 AMOVPERCMSTPSNONWNONDNON_WAIT,
	 AMOVPERCMSTPSNONWNONDNON_WALK_FORWARD_CHECKSIX_V1,
	 AMOVPERCMSTPSNONWNONDNON_WALK_FORWARD_CHECKSIX_V2,
	 AMOVPERCMSTPSNONWNONDNON_WALK_FORWARD_CHECKSIX_V3,
	 AMOVPERCMSTPSNONWNONDNON_WALK_FORWARD_CHECKSIX_V4,
	 AMOVPERCMSTPSNONWNONDNON_WALK_FORWARD_CHECKSIX_V5,
	 AMOVPERCMSTPSNONWNONDNON_WRINGING_HANDS_SLOW_V1,
	 AMOVPERCMSTPSNONWNONDNON_WRINGING_HANDS_SLOW_V2
 };


 /********************************OBJECT DEFINITIONS ***********************************/
 enum AIBEHAVIOUR {AITARGET, AIAUTOTARGET, AIMOVE, AIANIM,AICOLLISIONAVOID,
					AIGROUNDAVOID,AIPATHPLAN};

/*********************************COLLISION DEFINITIONS*********************************/

enum COLLISIONTESTTYPE {FIRE, VIEW, GEOM, IFIRED};

 /**************************************************************************************/

/*********************************CONTROLLABLE OBJECT DEFINITIONS***********************/
enum CAMMODETYPE{CAMMODE_INTERNAL, CAMMODE_EXTERNAL, CAMMODE_GUNNER, CAMMODE_GROUP};
enum DRAWMODE{DM_NORMAL, DM_TRANSPARENT, DM_WIREFRAME};
enum COMBATMODE{COMBAT_ERROR, COMBAT_BLUE, COMBAT_GREEN, COMBAT_WHITE, COMBAT_YELLOW, COMBAT_RED};
/***************************************************************************************/

/*********************************CAMERA OBJECT DEFINITIONS****************************/
enum CAMEFFECTTYPE{CAMEFFECTTYPE_INTERNAL, CAMEFFECTTYPE_EXTERNAL, CAMEFFECTTYPE_FIXED, CAMEFFECTTYPE_FIXEDWITHZOOM, CAMEFFECTTYPE_TERMINATE};
enum CAMEFFECTPOS{CAMEFFECTPOS_TOP, CAMEFFECTPOS_LEFT, CAMEFFECTPOS_RIGHT, CAMEFFECTPOS_FRONT, CAMEFFECTPOS_BACK};
enum CAMEFFECTMODE{VISIBLE,NIGHTVISION,THERMAL};
enum CAMCOMMANDTYPE{CAMCMD_MANUAL_ON, CAMCMD_MANUAL_OFF,CAMCMD_INERTIA_ON,CAMCMD_INERTIA_OFF,CAMCMD_LANDED,CAMCMD_AIRBORNE};
/*************************************************************************************/

/*********************************WAYPOINT OBJECT DEFINITIONS****************************/
enum WAYPOINTTYPE{MOVE, DESTROY, GETIN, SAD, JOIN, LEADER, GETOUT, CYCLE, LOAD, UNLOAD, TR_UNLOAD, HOLD, SENTRY, GUARD, TALK, SCRIPTED, SUPPORT, GETIN_NEAREST, AND, OR ,LOITER};
enum WAYPOINTCOMBATMODE{NO_CHANGE, BLUE, GREEN, WHITE, YELLOW, RED};
enum WAYPOINTBEHAVIOUR {UNCHANGED, CARELESS, SAFE, AWARE, COMBAT, STEALTH};
enum WAYPOINTSPEEDMODE {UNCHANGD, LIMITED, NORMAL, FULL};
enum FORMATION{COLUMN, STAG_COLUMN, WEDGE, ECH_LEFT, ECH_RIGHT, VEE, LINE,FILE,DIAMOND,NONE};	
enum WAYPOINTSHOWMODE{NEVER, EASY, ALWAYS};
/**************************************************************************************/

/*********************************TRIGGER OBJECT DEFINITIONS***********************/

enum TRIGGERACTIVATION{TNONE, TEAST, TWEST, TGUER, TCIV, TLOGIC, TANY, TALPHA, TBRAVO, TCHARLIE, TDELTA, TECHO, TFOXTROT, 
TGOLF, THOTEL, TINDIA, TJULIET, TSTATIC, TVEHICLE, TGROUP, TLEADER, TMEMBER, TWEST_SEIZED, TEAST_SEIZED, TGUER_SEIZED};
enum TRIGGERPRESENCE {PRESENT, NOT_PRESENT, WEST_D, EAST_D, GUER_D, CIV_D};
enum TRIGGERTYPE {TTNONE, SWITCH, END1, END2, END3, END4, END5, END6, LOOSE, WIN};
/***************************************************************************************/

/******************************MINE DEFINITIONS****************************************/
enum MINETYPE{VBS2_MINEAT_M15, VBS2_MINEAT_M19, VBS2_MINEAT_TM46, VBS2_MINEAT_TM62M};

/*****************************SIMULATION MODE*******************************************/
enum SIMULATION_MODE{SIM_NORMAL, SIM_FROZEN, SIM_HIDDEN};

/*****************************MISSION CONFIG DEFINITION*********************************/
enum ROADSIDE{ROAD_LEFT,ROAD_MIDDLE,ROAD_RIGHT};
enum ENDTYPE{ET_CONTINUE, ET_KILLED, ET_LOST, ET_END1, ET_END2, ET_END3, ET_END4, ET_END5, ET_END6};
//--------------------------------------------------------------------------------------

/*****************************MISSION DEFINITION*********************************/
enum PICTURE_FILETYPE{BMP,JPG,TGA,PNG,DDS,PPM,DIB,HDR,PFM};
//--------------------------------------------------------------------------------------

/*****************************GEOM OBJECT DEFINITIONS**********************************/

enum GEOMTYPE{GEOM_HOUSE, GEOM_TREE, GEOM_LANDSCAPE, GEOM_ROAD};
//-------------------------------------------------------------------------------------

/*******************************TERRAIN DEFINITIONS************************************/
enum COST_TYPE{COST_AVOID, COST_AVOIDBUSH, COST_AVOIDTREE, COST_NORMAL, COST_ROAD, COST_ROADFORCED, COST_SPACE, COST_SPACEBUSH, COST_SPACEHARDBUSH, COST_SPACEROAD, COST_SPACETREE, COST_WATER};
//--------------------------------------------------------------------------------------

/*********************************RTE DEFINITIONS**************************************/

enum VIEW_TYPE{VP_CONTOUR, VP_GRID, VP_HIT, VP_TRAIL};
//-------------------------------------------------------------------------------------
/*********************************KEY BIND RULES**************************************/

enum KEY_BIND_RULE{KBR_BUTTON_PRESS, KBR_VALUE_CHANGE, KBR_SIMULATION_STEP};
//-------------------------------------------------------------------------------------
/*********************************WORLD DEFINITION**************************************/

enum LIGHT_MODE{LIGHT_ON, LIGHT_OFF, LIGHT_AUTO};
//-------------------------------------------------------------------------------------

enum APPLICATIONSTATE{_,AAR, AAR_SELECT, DEBRIEFING, MAIN_MENU, MISSION, MISSION_INTERUPT, MP_CLIENT_WAITING,
MP_PARTICIPANTS, MP_SELECT_ROLE, MP_SELECT_SERVER, MP_SERVER_GET_READY, MP_SERVER_SELECT_MISSION, OME, OME_PREVIEW,
OME_SELECT_ISLAND, OPTIONS, OPTIONS_AUDIO, OPTIONS_CONTROLS, OPTIONS_DIFFICULTY, OPTIONS_VIDEO, RTE, RTE_PREVIEW, SP_SELECT_MISSION};

enum SKELETON_TYPE{
	NECK = 0 ,
	NECKLOW,
	HEAD,
	LEFT_BROW,
	MIDDLE_BROW,
	RIGHT_BROW,
	LEFT_MOUTH,
	MIDDLE_MOUTH ,
	RIGHT_MOUTH,
	EYELIDS,
	LIP,
	WEAPON,
	LAUNCHER,
	CAMERA,
	SPINE,
	SPINE1,
	SPINE2,
	SPINE3,
	PELVIS,
	LEFT_SHOULDER,
	LEFT_ARM,
	LEFT_ARM_ROLL,
	LEFT_FOREARM,
	LEFT_FOREARM_ROLL,
	LEFT_HAND,
	LEFT_HAND_RING,
	LEFT_HAND_RING1,
	LEFT_HAND_RING2,
	LEFT_HAND_RING3,
	LEFT_HAND_PINKY1,
	LEFT_HAND_PINKY2,
	LEFT_HAND_PINKY3,
	LEFT_HAND_MIDDLE1,
	LEFT_HAND_MIDDLE2,
	LEFT_HAND_MIDDLE3,
	LEFT_HAND_INDEX1,
	LEFT_HAND_INDEX2,
	LEFT_HAND_INDEX3,
	LEFT_HAND_THUMB1,
	LEFT_HAND_THUMB2,
	LEFT_HAND_THUMB3,
	RIGHT_SHOULDER,
	RIGHT_ARM,
	RIGHT_ARM_ROLL,
	RIGHT_FOREARM,
	RIGHT_FOREARM_ROLL,
	RIGHT_HAND,
	RIGHT_HAND_RING,
	RIGHT_HAND_RING1,
	RIGHT_HAND_RING2,
	RIGHT_HAND_RING3,
	RIGHT_HAND_PINKY1,
	RIGHT_HAND_PINKY2,
	RIGHT_HAND_PINKY3,
	RIGHT_HAND_MIDDLE1,
	RIGHT_HAND_MIDDLE2,
	RIGHT_HAND_MIDDLE3,
	RIGHT_HAND_INDEX1,
	RIGHT_HAND_INDEX2,
	RIGHT_HAND_INDEX3,
	RIGHT_HAND_THUMB1,
	RIGHT_HAND_THUMB2,
	RIGHT_HAND_THUMB3,
	LEFT_UP_LEG,
	LEFT_UP_LEG_ROLL,
	LEFT_LEG,
	LEFT_LEG_ROLL,
	LEFT_FOOT,
	LEFT_TOEBASE,
	RIGHT_UP_LEG,
	RIGHT_UP_LEG_ROLL,
	RIGHT_LEG,
	RIGHT_LEG_ROLL,
	RIGHT_FOOT,
	RIGHT_TOEBASE
};

struct KILLS_INFO
{
	double timeOfKill;
	struct UNIT_INFO
	{
		string name;
		bool isPlayer;
		position2D position;
		SIDE side;
		string type;
	} killedUnitInfo;

	UNIT_INFO killerUnitInfo;
	double distance;

};

struct INJURY_INFO
{
	double timeOfInjury;
	struct UNIT_INFO
	{
		string name;
		bool isPlayer;
		position2D position;
		SIDE side;
		string type;
	} injuredUnitInfo;

	UNIT_INFO attackUnitInfo;
	double damage;
	string typeOfAmmo;

};

/************************* GEOMETRY COORDINATE DEFINITIONS ****************************/

enum GEOMETRY_COORDINATE_TYPE{MGRS, LL, LLMS, UTM, UTMB};

struct GRID_FORMAT_ELEMENT
{
	string format;		// Grid orientation. "XY" for MGRS coordinate systems, otherwise empty. 
	string formatX;		// Number of digits to use for grid labels. String is a mask, containing one "0" for each digit to use.
	string formatY;
	double stepX;		// Distance between grid lines. Positive or negative values determine incrementation direction from origin.
	double stepY;	
};

struct GEOMETRY_COORDINATE_SYSTEM 
{
	GRID_FORMAT_ELEMENT zoomLevel_1;
	GRID_FORMAT_ELEMENT zoomLevel_2;
	GRID_FORMAT_ELEMENT zoomLevel_3;
	GEOMETRY_COORDINATE_TYPE coordinateSystem;

	GEOMETRY_COORDINATE_SYSTEM()
	{
		zoomLevel_1.format = "XY";
		zoomLevel_1.formatX = "0000";
		zoomLevel_1.formatY = "0000";
		zoomLevel_1.stepX = 10;
		zoomLevel_1.stepY = -10;

		zoomLevel_2.format = "XY";
		zoomLevel_2.formatX = "000";
		zoomLevel_2.formatY = "000";
		zoomLevel_2.stepX = 100;
		zoomLevel_2.stepY = -100;

		zoomLevel_3.format = "XY";
		zoomLevel_3.formatX = "00";
		zoomLevel_3.formatY = "00";
		zoomLevel_3.stepX = 1000;
		zoomLevel_3.stepY = -1000;

		coordinateSystem = MGRS;
	}
};

struct DEVICE_JOYCAPS_DATA{
	int deviceID;
	int  wMid;
	int  wPid;
	unsigned int wXmin;
	unsigned int wXmax;
	unsigned int wYmin;
	unsigned int wYmax;
	unsigned int wZmin;
	unsigned int wZmax;
	unsigned int wNumButtons;
	unsigned int wPeriodMin;    
	unsigned int wPeriodMax;
	unsigned int wRmin;
	unsigned int wRmax;
	unsigned int wUmin;
	unsigned int wUmax;
	unsigned int wVmin;
	unsigned int wVmax;
	unsigned int wCaps;
	unsigned int wMaxAxes;
	unsigned int wNumAxes;
	unsigned int wMaxButtons;
	char regKey[33];
};

struct STATE_JOYINFO_DATA
{	
	int deviceID;
	unsigned int wXpos;
	unsigned int wYpos;
	unsigned int wZpos;
	unsigned int wButtons;
};


/*!
A Title Effect Type is a String 
*/
enum TITLE_EFFECT_TYPE{PLAIN,PLAIN_DOWN,BLACK,BLACK_FADED,BLACK_OUT,BLACK_IN,WHITE_OUT,WHITE_IN}; 

/*!
Add sound types for vehicle
*/
enum VEHICLE_SOUND_TYPES{SOUNDENGINE,SOUNDENVIRON,SOUNDCRASH,SOUNDDAMMAGE,SOUNDLANDCRASH,SOUNDWATERCRASH,SOUNDGETIN,SOUNDGETOUT,SOUNDSERVO};
/*!
Add vehicle related action enum
*/
enum VEHICLE_ACTION{VA_GETOUT,VA_TURNOUT,VA_TURNIN};

struct RANK_CLASS
{
	string rank;
	int rating;
};

/******************************** SIMPLE COMMAND DEFINITIONS ****************************/

enum SIMPLE_COMMAND{COMMAND_FORWARD, COMMAND_BACK, COMMAND_SLOW, COMMAND_FAST, COMMAND_LEFT, COMMAND_RIGHT, COMMAND_STOP, COMMAND_FIRE,
	COMMAND_CEASE_FIRE, COMMAND_MANUAL_FIRE, COMMAND_CANCEL_MANUAL_FIRE};

/*!
A Title Effect Class 
*/
enum TITLE_EFFECT_CLASS{TEC_NONE,TEC_OBJECT,TEC_RES,TEC_TEXT}; 

};

#endif // VBS2FUSIONDEFINITIONS_H