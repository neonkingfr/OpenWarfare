
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:	TurnInEvent.h

Purpose:   This file contains the declaration of the TurnInEvent class.

	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			02-06/2011  CGS: Original Implementation

/*****************************************************************************/

#ifndef  VBS2FUSION_TURNIN_EVENT_H
#define  VBS2FUSION_TURNIN_EVENT_H

/**************************************************************************
  To disable warnings raised at the compile time of a Dynamic Link Library
  (DLL) due to defining objects of classes which has members of types 
  defined in the Standard Template Library (STL). 
  Warning identifier [C:4251]
  Added in stage 2 warning removal process.
/**************************************************************************/
#pragma warning (push)
#pragma warning (disable: 4251)


/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "Event.h"
#include "data\Turret.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/
namespace VBS2Fusion {



	/*!
	Defines a CargoChanged event which is passed onto the vehicle object when an 'CARGOCHANGE' 
	event is called. 
	*/

	class VBS2FUSION_API TurnInEvent : public Event	{

	public:

		/*!
		No argument constructor for the class. Calls setSubType(CARGOCHANGED) to initialize. 
		*/
		TurnInEvent();

		/*!
		Primary destructor for the class. 
		*/
		~TurnInEvent();

		/*!
		Sets the VBS2 name of the Object to which was attached or an empty string if the event 
		is of type detached 
		*/
		void setTurnInUnitName(string name);

		/*!
		Returns the VBS2 name the Object to which was attached or an empty string if the event
		is of type detached
		*/
		string getTurnInUnitName() const;

		/*!
		Sets the VBS2 name of the Object to which was attached or an empty string if the event 
		is of type detached 
		*/
		void setTurnInUnitAlias(string alias);

		/*!
		Returns the VBS2 name the Object to which was attached or an empty string if the event
		is of type detached
		*/
		string getTurnInUnitAlias() const;

		/*!
		Sets the VBS2 name of the Object to which was attached or an empty string if the event 
		is of type detached 
		*/
		void setTurnInUnitID(NetworkID id);

		/*!
		Returns the VBS2 name the Object to which was attached or an empty string if the event
		is of type detached
		*/
		NetworkID getTurnInUnitID() const;

		/*!
		Sets the VBS2 name of the Object to which was attached or an empty string if the event 
		is of type detached 
		*/
		void setTurnInTurretPath(Turret::TurretPath turretPath);

		/*!
		Returns the VBS2 name the Object to which was attached or an empty string if the event
		is of type detached
		*/
		Turret::TurretPath getTurnInTurretPath() const;

	private:
		string _unitName;
		string _unitAlias;
		NetworkID _unitID;
		Turret::TurretPath _turretpath;

	};


}

#pragma warning(pop) // Enable warnings [C:4251]

#endif  //VBS2FUSION_TURNIN_EVENT_H