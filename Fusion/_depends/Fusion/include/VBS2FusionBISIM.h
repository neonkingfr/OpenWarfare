/**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************

Name:

	VBS2FusionBISIM.h

Purpose:

	This file contains definitions of all technologies created by BISIM programmers.

/************************************************************************/

#ifdef BISIM_DEV
#ifndef VBS2FUSION_BISIM_H
#define VBS2FUSION_BISIM_H

#define _BISIM_DEV_ON                            1             // Macro for disabling all technologies below

#define _BISIM_DEV_OPERATIVE_PATH_PLUGIN         _BISIM_DEV_ON // Units can use external pathplanning plugin to create operative path.
#define _BISIM_DEV_PLUGIN_FILE_ACCESS            _BISIM_DEV_ON // Allows plugins to access VBS2 files.
#define _BISIM_DEV_ON_MODEL_BINARIZED            _BISIM_DEV_ON // Introduces new event handler called from VBS2 binarization after a model is binarized
#define _BISIM_DEV_GAMEOBJ_TO_GEOMOBJ            _BISIM_DEV_ON // Introduces API function which for GameObject returns GeometryObject structure
#define _BISIM_DEV_DRAW_NAVMESH                  _BISIM_DEV_ON // Adds function to draw object navigation mesh.
#define _BISIM_DEV_ISONNAVMESH                   _BISIM_DEV_ON // Adds function to tell whether position is on navigation mesh.
#define _BISIM_DEV_DRAW_NAVMESH_EXT              _BISIM_DEV_ON // Adds improved function to draw navigation mesh.
#define _BISIM_DEV_AI_REQUEST_PATH_FIX           _BISIM_DEV_ON // OnAIPathRequest now works also for .fusion files.

#define _BISIM_DEV_OVERRIDE_WINDOW_INPUT         _BISIM_DEV_ON // Adds Fusion ability to intercept VBS2 input messages before VBS2 receives
#define _BISIM_DEV_MOUSEWHEEL_DELTA              _BISIM_DEV_ON // Adds mouse wheel functionality and ability to intercept input
#define _BISIM_DEV_ON_DRAWN_RTT                  _BISIM_DEV_ON // New event OnDrawnRttScenes, called when render-to-texture scenes are rendered with list of them
#define _BISIM_DEV_CREATE_SHOT_NEW_PARAMS        _BISIM_DEV_ON // Adds support for new parameter sound to function createShot
#define _BISIM_DEV_AAR_GET_ORIGINAL_ID           _BISIM_DEV_ON // Implements function to get id which a entity had originally during AAR recording when AAR is replayed
#define _BISIM_DEV_SET_OPTICSDIRECTION           _BISIM_DEV_ON // Implements function for setting direction of optic on turret.
#define _BISIM_DEV_FORCE_TURRET_SPEED            _BISIM_DEV_ON // Implements function for setting speed of turret azimuth and elevation changes.
#define _BISIM_DEV_SET_TURRET_STABILIZATION      _BISIM_DEV_ON // Implements function for switching stabilization of turret in both axes (azimuth, elevation) on and off.
#define _BISIM_DEV_NEW_COMMANDS_DEFINITIONS      _BISIM_DEV_ON // Makes changes in macros to cope with changes in macro definitions for VBS2 script commands.
#define _BISIM_DEV_CHANGE_TURRET_DIRECTION       _BISIM_DEV_ON // Implements function for making turret to change it's direction with given speed.
#endif //VBS2FUSION_BISIM_H
#endif //BISIM_DEV