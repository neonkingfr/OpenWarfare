
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************

Name:

	exports.h

Purpose:

	This file contains all the header files exported with the VBS2Fusion DLL .

Version Information:

	Version		Date		Author and Comments
	===========================================
	2.0			23-11/2009	RMR: Original Implementation
	2.01		05-02/2010	MHA: Comments Checked

/************************************************************************/

#ifndef VBS2FUSION_EXPORTS_H
#define VBS2FUSION_EXPORTS_H


#include "conversions.h"
#include "DisplayFunctions.h"
#include "position3D.h"
#include "position2D.h"
#include "VBS2Fusion_ErrorContext.h"
#include "VBS2FusionAppContext.h"
#include "VBS2FusionDefinitions.h"

#if WEAPON_HANDLING_OLD
#include "data\ArmedGroundVehicle.h"
#include "data\ArmedModule.h"
#endif
#include "data\Camera.h"
#include "data\ControllableObject.h"
#include "data\EnvironmentState.h"
#include "data\Explosive.h"
#include "data\FSM.h"
#if GEOM_OBJECT
#include "data\GeomObject.h"
#endif
#include "data\GroundVehicle.h"
#include "data\Group.h"
#include "data\GroupList.h"
#include "data\Magazine.h"
#include "data\MapObject.h"
#include "data\Marker.h"
#include "data\MarkerList.h"
#include "data\Mine.h"
#include "data\Mission.h"
#include "data\NetworkID.h"
#include "data\Projectile.h"
#if GEOM_OBJECT
#include "data\RoadNetwork.h"
#include "data\RoadSegment.h"
#endif
#include "data\Target.h"
#include "data\Trigger.h"
#include "data\TriggerList.h"
#include "data\Turret.h"
#include "data\Unit.h"
#include "data\UnitArmedModule.h"
#include "data\vbs2Types.h"
#include "data\VBS2Variable.h"
#include "data\Vehicle.h"
#include "data\VehicleArmedModule.h"
#include "data\VehicleList.h"
#include "data\Waypoint.h"
#include "data\Weapon.h"

#include "Events\AfterGetInEvent.h"
#include "Events\AfterGetInManEvent.h"
#include "Events\AfterGetOutEvent.h"
#include "Events\AfterGetOutManEvent.h"
#include "Events\AmmoDeleteEvent.h"
#include "Events\AmmoExplodeEvent.h"
#include "Events\AmmoHitEvent.h"
#include "Events\AnimChangedEvent.h"
#include "Events\AnimDoneEvent.h"
#include "Events\AttachToEvent.h"
#include "Events\BeforeGetInEvent.h"
#include "Events\BeforeGetInManEvent.h"
#include "Events\BeforeGetOutEvent.h"
#include "Events\BeforeGetOutManEvent.h"
#include "Events\BeforeKilledEvent.h"
#include "Events\CargoChangedEvent.h"
#include "Events\ChangedWeaponEvent.h"
#include "Events\CreateEvent.h"
#include "Events\DamagedEvent.h"
#include "Events\DeleteEvent.h"
#include "Events\DeletedEvent.h"
#include "Events\EngineEvent.h"
#include "Events\Event.h"
#include "Events\EventHandler.h"
#include "Events\EventHandlerEx.h"
#include "Events\FiredEvent.h"
#include "Events\FuelEvent.h"
#include "Events\GetInManEvent.h"
#include "Events\GetOutManEvent.h"
#include "Events\GearEvent.h"
#include "Events\GetInEvent.h"
#include "Events\GetOutEvent.h"
#include "Events\GroupChangedEvent.h"
#include "Events\GroupChangedRTEEvent.h"
#include "Events\HitEvent.h"
#include "Events\HitPartEvent.h"
#include "Events\IncomingMissileEvent.h"
#include "Events\InitEvent.h"
#include "Events\KilledEvent.h"
#include "Events\LandedStoppedEvent.h"
#include "Events\LandedTouchDownEvent.h"
#include "Events\LoadOutChangedEvent.h"
#include "Events\MuzzleFiredEvent.h"
#include "Events\PasteEvent.h"
#include "Events\PositionChangedEvent.h"
#include "Events\RespawnEvent.h"
#include "Events\SuppressedEvent.h"
#include "Events\TurnInEvent.h"
#include "Events\TurnOutEvent.h"
#include "Events\VBS2EventTypes.h"
#include "Events\WaypointCompleteEvent.h"

#include "Math\Matrix4f.h"
#include "Math\Vector3f.h"

#include "Thread\Thread.h"

#include "util\AARUtilities.h"
#if WEAPON_HANDLING_OLD
#include "util\ArmedGroundVehicleUtilities.h"
#include "util\ArmedModuleUtilities.h"
#endif
#include "util\CameraUtilities.h"
#include "util\CollisionUtilities.h"
#include "util\ControllableObjectUtilities.h"
#include "util\EffectsUtilities.h"
#include "util\EnvironmentStateUtilities.h"
#include "util\ErrorHandleUtilities.h"
#include "util\ExecutionUtilities.h"
#include "util\ExplosiveUtilities.h"
#include "util\FSMUtilities.h"
#include "util\GeneralUtilities.h"
#include "util\GroupListUtilities.h"
#include "util\GroupUtilities.h"

#include "util\InputUtilities.h"
#include "util\MapUtilities.h"
#include "util\MarkerUtilities.h"
#include "util\MarkerListUtilities.h"

#include "util\MineUtilities.h"
#include "util\MissionConfigUtilities.h"
#include "util\MissionUtilities.h"
#include "util\ObjectVectorUtilities.h"
#include "util\OrderedCommandQueue.h"
#include "util\PlayerUtilities.h"
#include "util\PoseControlUtilities.h"

#if GEOM_OBJECT
#include "util\RoadNetworkUtilities.h"
#include "util\RoadSegmentUtilities.h"
#endif

#include "util\RTEUtilities.h"
#include "util\TerrainUtilities.h"
#include "util\TitleTextUtilities.h"
#include "util\TIUtilities.h"
#include "util\TriggerListUtilities.h"
#include "util\TriggerUtilities.h"
#if WEAPON_HANDLING_OLD
#include "util\TurretUtilities.h"
#endif
#include "util\UnitUtilities.h"
#include "util\VariableUtilities.h"
#include "util\vbs2TypesUtilities.h"
#include "util\VehicleListUtilities.h"
#include "util\VehicleUtilities.h"
#include "util\VisibilityUtilities.h"
#include "util\WaypointUtilities.h"
#include "util\WeaponUtilities.h"
#include "util\WorldUtilities.h"


#endif