/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	DisplayFunctions.h

Purpose:

	This file contains the declaration of the DisplayFunctions class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			24-03/2009	RMR: Original Implementation
	2.0			18-12/2009	UDW: Version 2 Implementation
	2.01		10-02/2010	MHA: Comments Checked
	2.02		22-11-2011	DMB: Added Methods
									void DisplayDialog(std::string& title,
										std::string& text)
									void DisplayStructuredTextDialog(
										std::string& title,
										std::string& structuredText)
									void DisplayDialog(std::string& title,
										const vector<std::string> &pointVector)

	2.03		12-01-2012	RDJ:  Added following Methods
									 void DisplayWarningMessage(std::string& message);
									 int  DisplayDiagMessage(int handle,float duration,std::string& message);
									 void WriteConsoleLog(std::string& message);
									 void DisplayTextLog(std::string& message);
									 void DisplayTextLog(double number);
									 void DisplayTextLog(int number);
									 void DisplayTextLog(bool val);
									 void DisplayTextLog(ControllableObject& target);
									 void DisplayTextLog(position3D pos);
									 void DisplayDebugLog(std::string& message);
									 void DisplayDebugLog(double number);
									 void DisplayDebugLog(int number);
									 void DisplayDebugLog(bool val);
									 void DisplayDebugLog(ControllableObject& target);
									 void DisplayDebugLog(position3D pos);

/************************************************************************/

#ifndef VBS2FUSION_DISPLAY_FUNCTIONS_H
#define VBS2FUSION_DISPLAY_FUNCTIONS_H

/**********************************************/
/* INCLUDES
/**********************************************/

// Standard Includes
#include <string>
#include <sstream>


// Simcentric Includes
#include "VBS2Fusion.h"

/**********************************************/
/* END INCLUDES
/**********************************************/
namespace VBS2Fusion
{
	class VBS2FUSION_API DisplayFunctions
	{
	public:

		/*!
		Display the string given by strDisplayString on the VBS2 game screen.

		Warning utility warn the following:

		- Empty String (outputs an warn if empty string pass to display)	
		*/
		static void DisplayString(std::string& strDisplayString);	

		/*!
		Temporary test function
		*/

		/*!
		Display the int, valueToDisplay on the VBS2 game screen.		
		*/
		static void DisplayInt(int valueToDisplay);


		/*!
		Display the double, valueToDisplay on the VBS2 game screen.		
		*/
		static void DisplayDouble(double valueToDisplay);

		/*!
		Display the float, valueToDisplay on the VBS2 game screen.		
		*/
		static void DisplayFloat(float valueToDisplay);

		/*!
		Display the string vector contents as string separated by commas
		on the VBS2 game screen.

		- Each empty string (outputs a warn if empty string pass to display)

		*/
		static void DisplayStringVector(const vector<std::string> &strVector);

		/*!
		Creates a hint dialog with the given title and text.
		Command will first create a centered dialog, only showing the hint title.
		The game will continue to run in the background, and the player can still 
		move around.
		Once he presses Escape, a regular hint window will show the title, the 
		first line preceded by a bullet, and the remaining structured text.

		title:Text shown on first popup dialog
		structuredText: Structured text shown in hint dialog.
		*/
		static void DisplayStructuredTextDialog(std::string& title, std::string& structuredText);

#ifdef DEVELOPMENT

		/*! 
		Display the warning message in the VBS2 screen.
		*/

		static void DisplayWarningMessage(std::string& message);

		/*!
		Display the Diag Message in the VBS2 screen
		handle:	Handle that identifies an existing message. By using the same handle as an existing message, the text can be updated. (-1: new message.) 
		duration: Time in milliseconds the message will be displayed. In order to update the same message, display the first with the longest duration practical (the limit for the command seems to lie around 4,000,000, which is a bit over one hour
		message: Message displayed.

		*/

		static int	DisplayDiagMessage(int handle,float duration,std::string& message);

		/*!
		Outputs text to the dedicated server console. 
		message: string to be written
		*/

		static void WriteConsoleLog(std::string& message);

		/*!
		 Dump argument value to debugging output. 
		 message : string Value
		*/
		static void DisplayTextLog(std::string& message);

		/*!
		 Dump argument value to debugging output. 
		 message : double or float
		*/
		static void DisplayTextLog(double number);

		/*!
		 Dump argument value to debugging output. 
		 message : int value
		*/
		static void DisplayTextLog(int number);

		/*!
		 Dump argument value to debugging output. 
		 message : bool
		*/
		static void DisplayTextLog(bool val);

		/*!
		Dump argument value to debugging output. 
		message : controllable Object
		*/
		static void DisplayTextLog(ControllableObject& target);

		/*!
		Dump argument value to debugging output. 
		message : 3d position
		*/

		static void DisplayTextLog(position3D pos);	
		
		
		/*!
		Dump argument type and value to debugging output. 
		message: string value
		*/
			
		static void DisplayDebugLog(std::string& message);

		/*!
		Dump argument type and value to debugging output. 
		message: double or float
		*/

		static void DisplayDebugLog(double number);

		/*!
		Dump argument type and value to debugging output. 
		message: int
		*/

		static void DisplayDebugLog(int number);

		/*!
		Dump argument type and value to debugging output. 
		message: bool
		*/

		static void DisplayDebugLog(bool val);

		/*!
		Dump argument type and value to debugging output. 
		message: controllable Objects
		*/

		static void DisplayDebugLog(ControllableObject& target);
		
		/*!
		Dump argument type and value to debugging output. 
		message: 3d positions
		*/

		static void DisplayDebugLog(position3D pos);
		
#endif // DEVELOPMENT

		/*!
		Display a hint dialog with the given title and text on the VBS2 game screen. 

		title:Text shown on first popup dialog
		text: Text shown in hint dialog.
		*/
		static void DisplayDialog(std::string& title, std::string& text);

		/*!
		Display a hint dialog with the given title and bullet point texts on the VBS2 
		game screen. Texts can be plain or structured. 

		title:Text shown on first popup dialog
		pointVector: Vector of string (Text shown in hint dialog)
		*/
		static void DisplayDialog(std::string& title, const vector<std::string> &pointVector);
	};
};

#endif //DISPLAY_FUNCTIONS_H