
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************

Name:

	RoadSegment.h

Purpose:

	This file contains the declaration of the RoadSegment class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			12-06/2011	NDB: Original Implementation

/************************************************************************/

#ifndef VBS2FUSION_ROAD_SEGMENT_H
#define VBS2FUSION_ROAD_SEGMENT_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/

// Standard C++ Includes

// Simcentric Includes
#include "position3D.h"
#include "VBS2FusionDefinitions.h"
#include "data/GeomObject.h"
#include "../../Private_Include/VBS2FusionConfig.h"

/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBS2Fusion
{

#if GEOM_OBJECT

	class VBS2FUSION_API RoadSegment :public GeomObject
	{
	public:

		/*!
		Main constructor for this class. The following parameters are initialized as follows:
		*/
		RoadSegment(const RoadSegment& roadSegment);

		/*!
		Main constructor for this class. The following parameters are initialized as follows:
		*/
		RoadSegment();

		/*!
		Main destructor for this class. 
		*/
		~RoadSegment();

		/*!
		sets Road segment name
		*/
		void setName(string strName);

		/*!
		returns road segment name
		*/
		string getName() const;

		/*!
		sets road segment position
		*/
		void setPosition(position3D pos);

		/*!
		returns road segment position
		*/
		position3D getPosition() const;

#ifdef DEVELOPMENT
		/*!
		returns the no of connected segments
		*/
		int getNumberOfConnectedSegments();

		/*!
		returns the connected segments name
		*/
		vector<string> getConnectedSegmentsString();

		/*!
		returns all the Connected segments
		*/
		vector<RoadSegment> getConnectedSegments();
		
		/*!
		remove earlier connections and set the given set of segments as connected segments
		*/
		void setConnectedSegments(vector<string> connections);

		/*!
		add given segment as connected segment
		*/
		void addConnectedSegment(string connection);

		/*!
		add given set of segments as connected segment
		*/
		void addConnectedSegment(vector<string> connection);
#endif

		/*!
		Assignment operator for RoadSegment class.
		Assign all the attributes of RoadSegment class.
		*/
		RoadSegment& operator=(const RoadSegment& roadSegment);		
		
	private:
		void Initialize();

		string _strName;
		position3D _position;
#ifdef DEVELOPMENT
		vector<string> _connectedSegments;
#endif
	};

#endif//GEOM_OBJECT
}; 

#endif //VBS2FUSION_ROAD_SEGMENT_H
