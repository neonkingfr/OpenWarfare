
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************

Name:

	RoadNetwork.h

Purpose:

	This file contains the declaration of the RoadNetwork class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			12-06/2011	NDB: Original Implementation

/************************************************************************/

#ifndef VBS2FUSION_ROADNETWORK_H
#define VBS2FUSION_ROADNETWORK_H


/**************************************************************************
  To disable warnings raised at the compile time of a Dynamic Link Library
  (DLL) due to defining objects of classes which has members of types 
  defined in the Standard Template Library (STL). 
  Warning identifier [C:4251]
/**************************************************************************/
#pragma warning (push)
#pragma warning (disable: 4251)

/***********************************************************************/
/* INCLUDES
/***********************************************************************/

// Standard C++ Includes
#include <vector>

// Simcentric Includes
#include "VBS2FusionDefinitions.h"
#include "../../Private_Include/VBS2FusionConfig.h"
#include "data/GeomObject.h"
#include "data/RoadSegment.h"

/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBS2Fusion
{

#if GEOM_OBJECT

	class VBS2FUSION_API RoadNetwork
	{
	public:

		/*!
		Main constructor for this class. The following parameters are initialized as follows:
		*/
		RoadNetwork();

		/*!
		Main constructor for this class. The following parameters are initialized as follows:
		*/
		RoadNetwork(vector<RoadSegment> segments);

		/*!
		Main destructor for this class. 
		*/
		~RoadNetwork();

#ifdef ROAD_OLD
		/*!
		segments of the road network
		*/
		struct SegmentAndConnections
		{
			RoadSegment _segment;
			vector<string> _connections;
		};

#endif
		/*!
		Iterator for segment list. 
		*/
		typedef vector<RoadSegment>::iterator RoadNetwork_Iterator;

		/*!
		Const Iterator for segment list. 
		*/
		typedef vector<RoadSegment>::const_iterator RoadNetwork_Const_Iterator;

		/*!
		Begin iterator for segment list. 
		*/
		RoadNetwork_Iterator begin();

		/*!
		const Begin iterator for segment list. 
		*/
		RoadNetwork_Const_Iterator begin() const;

		/*!
		End iterator for segment list. 
		*/
		RoadNetwork_Iterator end();

		/*!
		Const End iterator for segment list. 
		*/
		RoadNetwork_Const_Iterator end() const;

		/*!
		sets the given segment list in the RoadNetwork
		*/
		void setSegments(vector<RoadSegment> segments);

#ifdef DEVELOPMENT
		/*!
		add new segment in segment list of RoadNetwork
		*/
		void addSegment(RoadSegment segment);
#endif

#ifdef ROAD_OLD
		/*!
		sets the connections list to given road segment
		*/
		void setConnections(string strSegmentName, vector<string> connections);

		/*!
		sets the connections list to given road segment
		*/
		void setConnections(RoadSegment segment, vector<string> connections);

		/*!
		If the road segment name available in the RoadNetwork,
		returns the updated road segment
		*/
		RoadSegment getSegment(RoadSegment segment);
#endif

		/*!
		If the road segment name available in the RoadNetwork,
		returns the updated road segment
		*/
		RoadSegment getSegment(string strSegmentName);

		/*!
		returns the road segment of the given index from the RoadNetwork
		*/
		RoadSegment getSegment(int idx);

		/*!
		returns the no of road segment of the RoadNetwork
		*/
		int getNumberOfSegments();

#ifdef ROAD_OLD
		/*!
		returns the no of connections of the given segment
		*/
		int getNumberOfConnections(RoadSegment seg);

		/*!
		returns the no of connections of the given segment
		*/
		int getNumberOfConnections(string strSegmentName);

		/*!
		returns the connections of the given segment
		*/
		vector<string> getConnections(string strSegmentName);

		/*!
		returns the connections of the given segment
		*/
		vector<string> getConnections(RoadSegment segment);

		/*!
		returns all the segments connected with the given segment
		*/
		vector<RoadSegment> Connections(RoadSegment segment);
#endif
		int getIndex(string strSegmentName);

	private:
		void Initialize();
		int getIndex(RoadSegment segment);
		
#ifdef ROAD_OLD
		vector<SegmentAndConnections> segments;
#endif
		vector<RoadSegment> _segmentsVec;
		
	};

#endif//GEOM_OBJECT
}; 

#pragma warning (pop) // Enable warnings [C:4251]

#endif //VBS2FUSION_ROADNETWORK_H
