
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************

Name:

	ControllableObject.h

Purpose:

	This file contains the declaration of the ControllableObject class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	2.0			25-11/2009	RMR: Original Implementation
	2.0			10-02/2010  YFP: Added  operators = , == , <  
										ControllableObject(const ControllableObject& object)
										enum OBJECTTYPE
										setObjMapIndex(int i);
										getObjMapIndex() const;
										isActivated()const;
										getLogFileName() const;
										getObjectType()
	2.01		01-02/2010	MHA: Checked Comments
	2.02		14-06/2010  YFP: define struct CUSTOMFORMATION 
									add CUSTOMFORMATION variable for controllableobject Class.
	2.03		10-01/2011  CGS: Modifed setAlias(string& strAlias) 
									setName(string strName) 
												
	
/************************************************************************/

#ifndef VBS2FUSION_CONTROLLABLE_OBJECT_H
#define VBS2FUSION_CONTROLLABLE_OBJECT_H


/**************************************************************************
  To disable warnings raised at the compile time of a Dynamic Link Library
  (DLL) due to defining objects of classes which has members of types 
  defined in the Standard Template Library (STL). 
  Warning identifier [C:4251]
/**************************************************************************/
#pragma warning (push)
#pragma warning (disable: 4251)

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>
#include <list>

// SIMCENTRIC INCLUDES
#include "position3D.h"
#include "VBS2Fusion.h"
#include "conversions.h"
#include "data/NetworkID.h"
#include "DisplayFunctions.h"
#include "VBS2FusionDefinitions.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/


class GameValue;

namespace VBS2Fusion
{
	/*!
	Storage structure for storing information on weapons and the amount of 
	ammo in its current magazine. 
	*/
	struct VBS2FUSION_API WeaponAmmo
	{
		/*!
		String representing the class name of the weapon. 
		*/
		string strWeaponName;

		/*!
		Stores the amount of ammo remaining in the weapon loaded magazine. 
		*/
		int ammo;
	};

	/*!
	Storage structure for storing magazine information. The magazineammo
	structure can be used to store the magazine's class name, the number of 
	magazines of that type currently allocated and the amount of ammo
	remaining in each magazine. 
	*/

	struct VBS2FUSION_API MagazineAmmo
	{
		/*!
		Typedef for integer list to store amount of ammo
		left in each magazine of a particular class type. 
		*/
		typedef list<int> ammoAmountList;

		/*!
		Iterator for ammoAmountList. 
		*/
		typedef list<int>::iterator iterator;

		/*!
		Const iterator for ammoAmountList. 
		*/
		typedef list<int>::const_iterator const_iterator;

		/*!
		Stores the class name of the magazine. 
		*/
		string strMagazineName;

		/*!
		Stores the number of magazines of type strMagazineName currently assigned
		to the object. Value should be identical to ammo.size() is ammo is updated
		properly. 
		*/
		int number;

		/*!
		List of integer values representing the amount of ammo
		left in each magazine of strMagazineName type. 
		*/
		ammoAmountList ammo;

		/*!
		Begin iterator for ammoAmountList.
		*/
		iterator begin() {return ammo.begin();}

		/*!
		Begin const iterator for ammoAmountList.
		*/
		const_iterator begin() const {return ammo.begin();}

		/*!
		End iterator for ammoAmountList.
		*/
		iterator end() {return ammo.end();}

		/*!
		End const iterator for ammoAmountList.
		*/
		const_iterator end() const {return ammo.end();}

	};

	class VBS2FUSION_API ControllableObject
	{
	public:

		enum OBJECTTYPE{ CONTROLLABLEOBJECT,VEHICLE,UNIT,ARMEDVEHICLE , CAMERA };

		/*!
		Typedef for storage list containing WeaponAmmo structures. This list
		contains information on the objects currently assigned to weapons
		and the amount of ammo left in its current magazine. 
		*/

		typedef list<WeaponAmmo> weaponsList;

		/*!
		Iterator for weaponsList.
		*/

		typedef list<WeaponAmmo>::iterator weapons_iterator;

		/*!
		Const iterator for weaponsList.
		*/

		typedef list<WeaponAmmo>::const_iterator weapons_const_iterator;

		//****************************************************************

		/*!
		Typedef for storage list containing MagazineAmmo structures. This list
		contains information on the objects currently assigned to magazines, the number of 
		magazine types currently assigned and the amount of ammo left in each magazine. 
		*/
		typedef list<MagazineAmmo> magazineList;

		/*!
		Iterator for magazineList.
		*/

		typedef list<MagazineAmmo>::iterator magazines_iterator;

		/*!
		Const iterator for magazineList.
		*/

		typedef list<MagazineAmmo>::const_iterator magazines_const_iterator;


		//****************************************************************

		/*!
		Primary Constructor for the ControllableObject class. 

		The following variables are initialized as follows:

		_strAlias = "";
		_strName = "";
		_strID = "";
		_strInitializationStatements = "";
		_bPlayer = false;
		_bPlayable = false;
		*/
		ControllableObject();

		/*!
		Constructor for the ControllableObject class. 

		The following variables are initialized as follows:

		_strAlias = "";
		_strName = name;
		_strID = "";
		_strInitializationStatements = "";
		_bPlayer = false;
		_bPlayable = false;
		*/
		ControllableObject(string& name);



		ControllableObject(NetworkID& id);


		/*!
		Constructor for the ControllableObject class. 

		The following variables are initialized as follows:

		_strAlias = "";
		_strName = name;
		_strID = "";
		_strInitializationStatements = InitializationStatements;
		_bPlayer = false;
		_bPlayable = false;
		*/
		ControllableObject(string& name, string& InitializationStatements);
		
		/*!
		Constructor for the ControllableObject class. 

		The following variables are initialized as follows:

		_strAlias = "";
		_strName = name;
		_strID = "";
		_strInitializationStatements = "";
		_bPlayer = false;
		_bPlayable = false;
		_position = position;
		*/
		ControllableObject(string& name, position3D& position);

		/*!
		Constructor for the ControllableObject class. 

		The following variables are initialized as follows:

		_strAlias = "";
		_strName = name;
		_strID = "";
		_strInitializationStatements = InitializationStatements;
		_bPlayer = false;
		_bPlayable = false;
		_position = position;
		*/
		ControllableObject(string& name, position3D& position, string& InitializationStatements);

		/*!
		Primary destructor for the ControllableObject class. 
		*/
		~ControllableObject();

		//OPERATORS************************************************************

		/*!
			Copy operator for Controllable object.
		*/
		ControllableObject(const ControllableObject& object);

		/*!
			Assignment operator for ControllableObject class.
			Assign all the attributes of ControllableObject class.
		*/
		ControllableObject& operator =(const ControllableObject& object);

		/*!
			Equal operator for ControllableObject class.
			Compared by NetworkID.
		*/
		bool operator ==(const ControllableObject& object);

		/*!
			Less than operator for ControllableObject class.
			Compared by NetworkID.
		*/
		bool operator < (const ControllableObject& object);

		//**********************************************************************

		/*!
		Sets the name of the object. Typically the name of the object within the game
		environment. 
		*/
		virtual void setName(string& strName);

		/*!
		Returns the name of the object. 
		*/
		string getName() const;

		/*!
		Sets the VBS2Fusion alias of the object.
		*/
		void setAlias(string& strAlias);

		/*!
		Returns the VBS2Fusion alias of the object. If not alias is set (i.e == ""), it returns
		the name using getName(). 
		*/
		string getAlias() const;
			
		/*!
		Returns the VBS2 network ID of the object. 
		*/
		string getID() const;

		/*!
		Sets the VBS2 network ID of the object. 
		*/
		void setID(string& strID);

		/*!
		Sets the VBS2 network ID of the object. 
		*/
		void setNetworkID(NetworkID& id);

		/*!
		Returns the VBS2 network ID of the object. 
		*/
		NetworkID getNetworkID() const;

		/*!
		Returns a string in the form of (idtoobj this->getID())
		which can be used to access the object within the 
		VBS2 environment. Similar to using the alias, but is more
		network and multi-player friendly. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network Id is invalid)	
		*/

		string getIdToObjString();

		//****************************************************************

		/*!
		Sets the side of the group using a SIDE variable. Should be one
		of (WEST, EAST, CIVILIAN, RESISTANCE)
		*/
		void setSide(SIDE side);

		/*!
		Sets the side of the group in string format. Should be one
		of ("WEST", "EAST", "CIVILIAN", "RESISTANCE")
		*/
		void setSide(string strSide);

		/*!
		Returns the side of the group using a SIDE variable. Should be one
		of (WEST, EAST, CIVILIAN, RESISTANCE)
		*/
		SIDE getSide() const;

		/*!
		Returns the side of the group in string format. Should be one
		of ("WEST", "EAST", "CIVILIAN", "RESISTANCE")
		*/
		string getSideString() const;


		/*!
		Sets the URN Call sign of the object. 
		*/
		void setURLCallSign(string& strName);

		/*!
		Returns the URN Call sign of the object. 
		*/
		string getURLCallSign() const;

		/*!
		Sets the Type of the object. 
		*/
		void setType(string& strName);

		/*!
		Returns the Type of the object. 
		*/
		string getType() const;


		/*!
		Sets the Initialization Statements of the object. 
		*/
		void setInitializationStatements(string& strInitialization);

		/*!
		Returns the Initialization Statements of the object. 
		*/
		string getInitializationStatements()const;

		/*!
		Sets boolean which signifies whether the object is controlled by the player or not. 
		*/
		void setPlayerStatus(bool val);

		/*!
		Returns boolean which signifies whether the object is controlled by the player or not. 
		*/
		bool getPlayerStatus() const;

		/*!
		Sets boolean which signifies whether the object can be controlled
		by the player or not. 
		*/
		void setPlayableStatus(bool val);

		/*!
		Returns boolean which signifies whether the object can be controlled
		by the player or not. 
		*/
		bool getPlayableStatus() const;

		/*!
		Sets the position of the object in position3D format. 
		*/
		void setPosition(position3D& position);

		/*!
		Sets the position of the object using doubles. 

		1st parameter : x position,
		2nd parameter : z position,
		3rd parameter : y position
		*/
		void setPosition(double x, double z, double y);

		/*!
		Sets the position of the object using strings. The values are converted to
		doubles within the function. 

		1st parameter : x position,
		2nd parameter : z position,
		3rd parameter : y position
		*/
		void setPosition(string x, string z, string y);

		/*!
		Returns the position of the object in position3D format. 
		*/
		position3D getPosition() const;

		/*!
		Sets the position above sea level of the object in position3D format. 
		*/
		void setPositionASL(position3D& position);

		/*!
		Sets the position above sea level of the object using doubles. 

		1st parameter : x position,
		2nd parameter : z position,
		3rd parameter : y position
		*/
		void setPositionASL(double x, double z, double y);

		/*!
		Sets the position above sea level of the object using strings. The values are converted to
		doubles within the function. 

		1st parameter : x position,
		2nd parameter : z position,
		3rd parameter : y position
		*/
		void setPositionASL(string x, string z, string y);

		/*!
		Returns the position above sea level of the object in position3D format. 
		*/
		position3D getPositionASL() const;

		/*!
		Sets the velocity of the object in position3D format. 
		*/
		void setVelocity(position3D& velocity);

		/*!
		Sets the velocity of the object using doubles. 

		1st parameter : x component,
		2nd parameter : z component,
		3rd parameter : y component
		*/
		void setVelocity(double x, double z, double y);

		/*!
		Sets the velocity of the object using string. The values are converted into doubles 
		within the function. 

		1st parameter : x component,
		2nd parameter : z component,
		3rd parameter : y component
		*/
		void setVelocity(string x, string z, string y);

		/*!
		Returns the velocity of the object in position3D Format. 
		*/
		position3D getVelocity() const;	

		/*! 
		Set the boolean which determines whether the object is alive within the
		game environment. 
		*/
		void setAlive(bool boolAlive);

		/*! 
		Return the boolean which determines whether the object is alive within the
		game environment. 
		*/
		bool getAlive() const;

		/*! 
		Set the current damage value of the object. 
		*/
		void setDamage(double damage);

		/*! 
		Return the current damage value of the object. 
		*/
		double getDamage() const;


		/*! 
		Set the direction in which, the user would like the object to move towards. 
		Different from the setDirection command, which is used to set the actual direction 
		the object is currently facing. 
		*/
		void setMoveDirection(double direction);

		/*! 
		Return the direction in which user would like the object to move towards. 
		Different from the getDirection command, which is used to get the actual direction of 
		the object which is currently facing. 
		*/
		double getMoveDirection() const;

		/*! 
		Set the desired speed for the object. 
		*/
		void setSpeed(double speed);

		/*! 
		Return the desired speed for the object. 
		*/
		double getSpeed() const;		

		/*! 
		Set the current direction the object is facing towards. 
		*/
		void setDirection(double direction);

		/*! 
		Return the current direction the object is facing towards. 
		*/
		double getDirection() const;

		/*!
		Virtual function to handle fired events from the unit. This method is called
		by an EventHandler object in the case of a 'Fired' event being called. 

		A 'Fired' event handler for the relevant object should first be added
		using addFiredEvent on a global EventHandler object. 

		It Inherit from this class and rewrite the processFiredEvent function 
		to provide customized functionality. 

		Current functionality does not do anything. Left empty for creating custom 
		behavior. 
		*/
		virtual void processFiredEvent(FiredEvent& _event);



		/*!
		Virtual function to handle delete events from the unit. This method is called
		by an EventHandler object in the case of an 'delete' event being called. 

		A 'delete' event handler for the relevant object should first be added
		using addDeleteEvent on a global EventHandler object. 

		It Inherit from this class and rewrite the processDeleteEvent function 
		to provide customized functionality. 

		Current functionality does not do anything. Left empty for creating custom 
		behavior. 
		*/
		virtual void processDeleteEvent(DeleteEvent& _event);



		/*!
		Virtual function to handle HitPart events from the unit. This method is called
		by an EventHandler object in the case of an 'HitPart' event being called. 

		A 'HitPart' event handler for the relevant object should first be added
		using addHitPartEvent on a global EventHandler object. 

		It Inherit from this class and rewrite the processHitPartEvent function 
		to provide customized functionality. 

		Current functionality does not do anything. Left empty for creating custom 
		behavior. 
		*/
		virtual void processHitPartEvent(HitPartEvent& _event);


		/*!
		Virtual function to handle Respawn events from the unit. This method is called
		by an EventHandler object in the case of an 'Respawn' event being called. 

		A 'Respawn' event handler for the relevant object should first be added
		using addRespawnEvent on a global EventHandler object. 

		It Inherit from this class and rewrite the processRespawnEvent function 
		to provide customized functionality. 

		Current functionality does not do anything. Left empty for creating custom 
		behavior. 
		*/
		virtual void processRespawnEvent(RespawnEvent& _event);
		
		/*!
	  Virtual function to handle AttachTo events from the unit. This method is called
	  by an EventHandler object in the case of an 'AttachTo' event being called. 

	  A 'AttachTo' event handler for the relevant object should first be added
	  using addAttachToEvent on a global EventHandler object. 

	  Inherit from this class and rewrite the processRespawnEvent function 
	  to provide customized functionality. 

	  Current functionality does not do anything. Left empty for creating custom 
	  behavior. 
		  */
		 virtual void processAttachToEvent(AttachToEvent& _event);

		/*!
		Sets object is local or network object which has valid network id
		*/
		void setLocal(bool _boolLocal);

		/*!
		Returns true if object is local or false if object has valid network id
		*/
		bool isLocal() const;

		/*!
		This set to false if the creation of an object is not
		responsible by computer itself. Set to true when object 
		is created by computer it self.		
		*/
		void setLocallyOwned(bool _boolLocallyOwned);

		/*!
		This return false if the creation of an object is not
		responsible by computer itself. Returns true when object 
		is created by computer it self.
		*/
		bool isLocallyOwned();


		/*!
		Begin iterator for the weapons list.
		*/
		weapons_iterator weapons_begin();

		/*!
		Begin const iterator for the weapons list.
		*/
		weapons_const_iterator weapons_begin() const;

		/*!
		End iterator for the weapons list.
		*/
		weapons_iterator weapons_end();

		/*!
		End const iterator for the weapons list.
		*/
		weapons_const_iterator weapons_end() const;

		/*!
		Adds a new weapon using a class name. Sets the ammo value to 0 for the 
		newly created weapon. 
		*/
		void addWeapon(string weaponsName);
		
		/*!
		Clears the weapons list. 
		*/
		void clearWeaponsList();

		/*!
		Returns the number of weapons currently assigned. 
		*/

		int getNumberOfWeapons();

		/*!
		Removes weapon with class name weaponName from list. Returns false if
		operation was unsuccessful due to the weapon not being on the list. 
		*/

		bool removeWeapon(string weaponName);

		//****************************************************************
		// Magazines list control functions
		//****************************************************************

		/*!
		Begin iterator for the magazine list.
		*/
		magazines_iterator magazines_begin();

		/*!
		Begin const iterator for the magazine list.
		*/
		magazines_const_iterator magazines_begin() const;

		/*!
		End iterator for the magazine list.
		*/
		magazines_iterator magazines_end();

		/*!
		Begin const iterator for the magazine list.
		*/
		magazines_const_iterator magazines_end() const;

		/*!
		Adds a new magazine using a class name. Sets the number of magazines
		to 0 and clears the ammo amount list. 
		*/
		void addMagazine(string magazineName);

		/*!
		Clears the magazine list. 
		*/
		
		void clearMagazinesList();

		/*!
		Returns the number of magazine types currently assigned. 
		*/
		int getNumberOfMagazines();

		/*!
		Removes magazine with class name magazineName from list. Returns false if
		operation was unsuccessful due to the magazine not being on the list. 
		*/
		bool removeMagazine(string magazineName);

		/*!
		Returns true of a magazine with the class name magazineName
		is on the list. 
		*/
		bool isMagazineAdded(string magazineName);

		/*!
		Sets the name of the primary weapon. 
		*/
		void setPrimaryWeapon(string weaponName);

		/*!
		Returns the name of the primary weapon. 
		*/
		string getPrimaryWeapon() const;

		/*!
		set the object map index value of Reference container. 	
		*/
		void setObjMapIndex(int i);

		/*!
		Returns the object map index value of Reference container.
		*/
		int getObjMapIndex() const;

		/*!
		Returns the activation condition. This will return true if the object 
		has linked with VBS2 object.  
		*/
		bool isActivated()const;

		/*! 
		Set activation condition.
		*/
		void setActivated(bool condition);

		/*!
		Returns the log file name. 
		*/
		string getLogFileName() const;

		/*!
		Returns whether the assign target watching is enabled or disabled.
		*/
		bool isTargetAIEnabled() const;

		/*!
		Returns auto targeting is enabled or disabled.
		*/
		bool isAutotargetAIEnabled() const;

		/*!
		Returns Movement is enabled or disabled.
		*/
		bool isMoveAIEnabled() const;

		/*!
		Returns animation is enabled or disabled.
		*/
		bool isAnimAIEnabled() const;		

		/*!
		Set AI behavior status.
		*/
		void setAIbehaviourStatus(AIBEHAVIOUR aib, bool status);


		/*!
		set Simulation mode of the object.
		*/
		void setSimulationMode(SIMULATION_MODE mode);

		/*!
		returns simulation mode of the object.
		*/
		SIMULATION_MODE getSimulationMode() const;

		/*!
		set custom formation of the object.
		*/
		void setCustomFormation(CUSTOMFORMATION formation);

		/*!
		returns custom formation of the object. 
		*/
		CUSTOMFORMATION getCustomFormation() const;

 
	protected:

		OBJECTTYPE getObjectType()const;
		string _strName;

	private:
		bool _boolLocal;
		bool _boolLocallyOwned;
		SIDE _side;

		NetworkID _ID;
		string _strURNCallSign;
		string _strInitializationStatements;
		bool _bPlayer;
		bool _bPlayable;
		string _strType;
		double _dDamage;

		double _dDirection;
		double _dMoveDir;
		double _dSpeed;		

		position3D _position;
		position3D _positionASL;
		position3D _velocity;

		bool _bAlive;

		weaponsList _weaponList;
		magazineList _magazineList;

		string primaryWeapon;

		int _objMapIndex;

		bool _bActivated;

		string _logFileName;

		OBJECTTYPE _cobtype;

		// enable AI variables
		bool _bTargetAIEnabled;
		bool _bAutotargetAIEnabled;
		bool _bMoveAIEnabled;
		bool _bAnimAIEnabled;

		void Initialize();

		SIMULATION_MODE _simuMode;
		CUSTOMFORMATION _formation;

	};
};

#pragma warning(pop) // Enable warnings [C:4251]

#endif //CONTROLLABLE_OBJECT_H

