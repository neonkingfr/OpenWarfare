/*
* Copyright 2010 SimCentric Technologies, Pty. Ltd.  All Rights Reserved.
 *
* Permission to use, copy, modify, and distribute this software in object
* code form for any purpose and without fee is hereby granted, provided
* that the above copyright notice appears in all copies and that both
* that copyright notice and the limited warranty and restricted rights
* notice below appear in all supporting documentation.
 *
* SIMCENTRIC PROVIDES THIS PROGRAM "AS IS" AND WITH ALL FAULTS.
* SIMCENTRIC SPECIFICALLY DISCLAIMS ANY AND ALL WARRANTIES, WHETHER EXPRESS
* OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTY
* OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR USE OR NON-INFRINGEMENT
* OF THIRD PARTY RIGHTS.  SIMCENTRIC DOES NOT WARRANT THAT THE OPERATION
* OF THE PROGRAM WILL BE UNINTERRUPTED OR ERROR FREE.
 *
* In no event shall SimCentric Technologies, Pty. Ltd. be liable for any direct, indirect,
* incidental, special, exemplary, or consequential damages (including,
* but not limited to, procurement of substitute goods or services;
* loss of use, data, or profits; or business interruption) however caused
* and on any theory of liability, whether in contract, strict liability,
* or tort (including negligence or otherwise) arising in any way out
* of such code.
*/

/*************************************************************************

Name:

	Waypoint.h

Purpose:

	This file contains the declaration of the Waypoint class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			27-03/2009	RMR: Original Implementation
	1.01		27-08/2009	SDS: Added setLocal(bool _isLocal)
								 Added isLocal()
	1.02		20-10/2009	SDS: Added next and previous waypoint related stuff
								 Added setPosition(string,string,string)
	1.03		09-11/2009  SDS: added bool operator==(const Waypoint& wp1, const Waypoint& wp2);
								 added bool operator<(const Waypoint& wp1, const Waypoint& wp2);
	2.0			12-20/2009	UDW: getObjMapIndex(), setObjMapIndex(), isActivated() getLogFileName() 								 
	2.01		11-02/2010	MHA: Comments Checked
	2.02		10-01/2011  CGS: Methods added
									setActivated(bool status)	
								 Modified setName(string strName)
								 setAlias(string strAlias)
					

/************************************************************************/

#ifndef WAYPOINT_H
#define WAYPOINT_H


/***********************************************************************/
/* INCLUDES
/***********************************************************************/

// Standard Includes
#include <string>

// VBS2Fusion Includes
#include "VBS2FusionDefinitions.h"
#include "position3D.h"
#include "VBS2Fusion.h"
#include "data/NetworkID.h"

/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBS2Fusion
{
	
	class VBS2FUSION_API Waypoint
	{

	
	public:

		/*!
		Typedefs for Waypoint, WaypoitIDs are similar to networkID.
		*/
		typedef NetworkID WaypointID;

		/*!
		The main constructor for the Waypoint class. 

		Initialized as follows:

		- setWaypointType(MOVE);
		- setWaypointSpeedMode(NORMAL);
		- setWaypointCombatMode(BLUE);
		- setWaypointBehaviour(SAFE);
		*/
		Waypoint();
		
		/*!
		Copy constructor
		*/
		Waypoint(const Waypoint& _wp);
		
		/*!
		The main destructor for the Waypoint class. 
		*/
		~Waypoint();

		/*!
		Set the name used in VBS2 for the object. 
		*/
		void setName(string strName);

		/*!
		Return the name used in VBS2 for the object. 
		*/
		string getName() const;

		/*!
		Set the alias assigned by VBS2Fusion. 
		*/
		void setAlias(string strAlias);

		/*!
		Return the alias assigned by VBS2Fusion. 
		*/
		string getAlias() const;

#if DEVELOPMENT
		/*Set Waypoint ID*/
		void setWaypointID(WaypointID _waypoint);

		/*!
		Get Waypoint ID
		*/
		Waypoint::WaypointID getWaypointID();
#endif

		/*!
		Set the waypoint type using a WAYPOINTTYPE variable. 
		(MOVE, DESTROY, GETIN, SAD, JOIN, LEADER, GETOUT, CYCLE, LOAD, 
		UNLOAD, TR_UNLOAD, HOLD, SENTRY, GUARD, TALK, SCRIPTED, SUPPORT, 
		GETIN_NEAREST, AND or OR). 
		*/
		void setWaypointType(WAYPOINTTYPE _type);

		/*!
		Set the waypoint type using string variable. 
		("MOVE", "DESTROY", "GETIN", "SAD", "JOIN", "LEADER", "GETOUT", "CYCLE", "LOAD", 
		"UNLOAD", "TR_UNLOAD", "HOLD", "SENTRY", "GUARD", "TALK", "SCRIPTED", "SUPPORT", 
		"GETIN_NEAREST", "AND" or "OR"). 
		*/
		void setWaypointType(string _strType);

		/*!
		Return the waypoint type using a WAYPOINTTYPE variable 
		(MOVE, DESTROY, GETIN, SAD, JOIN, LEADER, GETOUT, CYCLE, LOAD, 
		UNLOAD, TR_UNLOAD, HOLD, SENTRY, GUARD, TALK, SCRIPTED, SUPPORT, 
		GETIN_NEAREST, AND or OR). 
		*/
		WAYPOINTTYPE getWaypointType() const;

		/*!
		Return the waypoint type using string variable. 
		("MOVE", "DESTROY", "GETIN", "SAD", "JOIN", "LEADER", "GETOUT", "CYCLE", "LOAD", 
		"UNLOAD", "TR_UNLOAD", "HOLD", "SENTRY", "GUARD", "TALK", "SCRIPTED", "SUPPORT", 
		"GETIN_NEAREST", "AND" or "OR"). 
		*/
		string getWaypointTypeString();

		/*!
		Set the waypoint combat mode using a WAYPOINTCOMBATMODE variable.
		(NO_CHANGE, BLUE, GREEN, WHITE, YELLOW,  or RED). 
		*/
		void setWaypointCombatMode(WAYPOINTCOMBATMODE _type);

		/*!
		Set the waypoint combat mode using a string variable.
		("NO_CHANGE", "BLUE", "GREEN", "WHITE", "YELLOW",  or "RED"). 
		*/
		void setWaypointCombatMode(string _strType);

		/*!
		Return the waypoint combat mode using a WAYPOINTCOMBATMODE variable.
		(NO_CHANGE, BLUE, GREEN, WHITE, YELLOW,  or RED). 
		*/
		WAYPOINTCOMBATMODE getWaypointCombatMode() const;

		/*!
		return the waypoint combat mode using a string variable.
		("NO_CHANGE", "BLUE", "GREEN", "WHITE", "YELLOW",  or "RED"). 
		*/
		string getWaypointCombatModeString();

		/*!
		Set the waypoint behavior mode using a WAYPOINTBEHAVIOUR variable.
		(UNCHANGED, CARELESS, SAFE, AWARE, COMBAT or STEALTH). 
		*/
		void setWaypointBehaviour(WAYPOINTBEHAVIOUR _type);

		/*!
		Set the waypoint behavior mode using a string variable.
		("UNCHANGED", "CARELESS", "SAFE", "AWARE", "COMBAT" or "STEALTH"). 
		*/
		void setWaypointBehaviour(string _strType);

		/*!
		Return the waypoint behavior mode using a WAYPOINTBEHAVIOUR variable.
		(UNCHANGED, CARELESS, SAFE, AWARE, COMBAT or STEALTH). 
		*/
		WAYPOINTBEHAVIOUR getWaypointBehaviour() const;

		/*!
		Return the waypoint behavior mode using a string variable.
		("UNCHANGED", "CARELESS", "SAFE", "AWARE", "COMBAT" or "STEALTH"). 
		*/
		string getWaypointBehaviourString();

		/*!
		Set the waypoint formation mode using a FORMATION variable.
		(COLUMN, STAG_COLUMN, WEDGE, ECH_LEFT, ECH_RIGHT, VEE, LINE or NONE). 
		*/
		void setWaypointFormation(FORMATION _type);

		/*!
		Set the waypoint formation mode using a string variable.
		("COLUMN", "STAG_COLUMN", "WEDGE", "ECH_LEFT", "ECH_RIGHT", "VEE", "LINE" or "NONE"). 
		*/
		void setWaypointFormation(string strFormation);

		/*!
		Return the waypoint formation mode using a FORMATION variable.
		(COLUMN, STAG_COLUMN, WEDGE, ECH_LEFT, ECH_RIGHT, VEE, LINE or NONE). 
		*/
		FORMATION getWaypointFormation() const;

		/*!
		Return the waypoint formation mode using a string variable.
		("COLUMN", "STAG_COLUMN", "WEDGE", "ECH_LEFT", "ECH_RIGHT", "VEE", "LINE" or "NONE"). 
		*/
		string getWaypointFormationString();

		/*!
		Set the waypoint speed mode mode using a WAYPOINTSPEEDMODE variable.
		(UNCHANGD, LIMITED, NORMAL or FULL). 
		*/
		void setWaypointSpeedMode(WAYPOINTSPEEDMODE _type);

		/*!
		Set the waypoint speed mode mode using a string variable.
		("UNCHANGD", "LIMITED", "NORMAL" or "FULL"). 
		*/
		void setWaypointSpeedMode(string _strType);

		/*!
		Return the waypoint speed mode mode using a WAYPOINTSPEEDMODE variable.
		(UNCHANGD, LIMITED, NORMAL or FULL). 
		*/
		WAYPOINTSPEEDMODE getWaypointSpeedMode() const;

		/*!
		Return the waypoint speed mode mode using a string variable.
		("UNCHANGD", "LIMITED", "NORMAL" or "FULL"). 
		*/
		string getWaypointSpeedModeString();

		/*!
		Set the description of the waypoint
		*/
		void setDescription(string strDescription);

		/*!
		Return the description of the waypoint
		*/
		string getDescription() const;

		/*!
		Set the direction of the waypoint
		*/
		void setDirection(double direction);

		/*!
		Return the direction of the waypoint
		*/
		double getDirection() const;

		/*!
		Set the name of next waypoint
		*/
		void setNextWaypoint(string nextWaypoint);

		/*!
		get the name of next waypoint
		*/
		string getNextWaypoint() const;

		/*!
		Set the the name of previous waypoint
		*/
		void setPreviousWaypoint(string previousWaypoint);

		/*!
		get the the name of previous waypoint
		*/
		string getPreviousWaypoint() const;

		/*!
		Set the position of the waypoint in position3D format. 
		*/
		void setPosition(position3D position);

		/*!
		Set the position of the waypoint using 3 doubles. 
		
		parameter 1: x position, 
		parameter 2: z position,
		parameter 3: y position. 
		*/
		void setPosition(double x, double z, double y);

		
		/*!
		Sets the position of the object using strings. The values are converted to
		doubles within the function. 

		1st parameter : x position,
		2nd parameter : z position,
		3rd parameter : y position
		*/
		void setPosition(string x, string z, string y);

		/*!
		Return the position of the waypoint in position3D format. 
		*/
		position3D getPosition() const;

		/*!
		Set the placement radius of the waypoint. 
		*/
		void setPlacementRadius(double radius);

		/*!
		Return the placement radius of the waypoint. 
		*/
		double getPlacementRadius() const;

		/*!
		 Set the timeout max value of the waypoint. 
		*/
		void setTimeoutMax(double timeout);

		/*!
		 Return the timeout max value of the waypoint. 
		*/
		double getTimeoutMax() const;

		/*!
		 Set the timeout mid value of the waypoint. 
		*/
		void setTimeoutMid(double timeout);

		/*!
		 Return the timeout mid value of the waypoint. 
		*/
		double getTimeoutMid() const;

		/*!
		 Set the timeout min value of the waypoint. 
		*/
		void setTimeoutMin(double timeout);

		/*!
		 Return the timeout min value of the waypoint. 
		*/
		double getTimeoutMin() const;

		/*!
		 Set the waypoint statements. 
		*/
		void setStatements(string statements);

		/*!
		 Return the waypoint statements. 
		*/
		string getStatements() const;

		/*!
		 Set the waypoint script. 
		*/
		void setScript(string script);

		/*!
		 Return the waypoint script. 
		*/
		string getScript() const;

		/*!
		Set the name of the group the waypoint belongs to. 
		*/
		void setGroup(string group);	

		/*!
		Return  the name of the group the waypoint belongs to. 
		*/
		string getGroup() const;

		/*!
		Set the number of the waypoint within the group.
		Starts with zero
		*/
		void setGroupWaypointNo(int _number);

		/*!
		Return the number of the waypoint within the group.
		*/
		int getGroupWaypointNo() const;

		/*!
		Get the name of the waypoint in VBS2 format. 
		*/
		string getVBSWaypointName() const;

		/*!
		Set the waypoint show mode mode using a WAYPOINTSHOWMODE variable.
		(NEVER, EASY or ALWAYS). 
		*/
		void setWaypointShowMode(WAYPOINTSHOWMODE _type);

		/*!
		Set the waypoint show mode mode using a string variable.
		("NEVER", "EASY" or "ALWAYS"). 
		*/
		void setWaypointShowMode(string strShowMode);

		/*!
		Return the waypoint show mode mode using a WAYPOINTSHOWMODE variable.
		(NEVER, EASY or ALWAYS). 
		*/
		WAYPOINTSHOWMODE getWaypointShowMode() const;

		/*!
		 Return the waypoint show mode mode using a string variable.
		 ("NEVER", "EASY" or "ALWAYS"). 
		*/
		string getWaypointShowModeString();

		/*!
		 Assignment operator for Waypoint class.
		 Assign all the attributes of Waypoint class.
		*/

		Waypoint& operator=(const Waypoint& wp);	

		/*!
		 Equal operator for Waypoint class.
		 Compare all the attributes of Waypoint class.
		*/
		bool operator== (const Waypoint& wp2) const;
		

		/*!
		 Less than operator for Waypoint class.
		 Compared by  alias.
		*/
		bool operator< (const Waypoint& wp2) const;

		/*!
		Sets object is local or network object which has valid network id
		*/
		void setLocal(bool _boolLocal);

		/*!
		 Returns true if object is local or false if object has valid network id
		*/
		bool isLocal();

		/*!
		 Returns the ObjectMap index for this object
		*/
		int getObjMapIndex() const;

		/*!
		 Sets the ObjectMap index for this object
		*/
		void setObjMapIndex(int index);

		/*!
		 returns true if the object actually  exists in VBS2
		*/
		bool isActivated() const;

		void setActivated(bool status);

		/*!
		 returns the log file Name
		*/
		string getLogFileName() const;

	private:
		bool _boolLocal;
		string _strName;
		string _strAlias;
		string _strGroup;
		WaypointID _waypointID;
		int _GroupWayPointNo;
		WAYPOINTTYPE _waypointtype;
		WAYPOINTCOMBATMODE _combatmodetype;
		WAYPOINTBEHAVIOUR _behaviourtype;
		FORMATION _formation;
		WAYPOINTSPEEDMODE _speedmode;
		string _strDescription;
		double _fDirection;
		position3D _position;
		double _fplacementradius;
		double _timeoutMax;
		double _timeoutMid;
		double _timeoutMin;
		string _strWaypointStatements;
		string _strWaypointScript;
		WAYPOINTSHOWMODE _showmode;
		string _nextWaypoint;
		string _prevWaypoint;
		bool _bActivated;
		NetworkID _ID;
		int _objMapIndex;
		string _LogFileName;

		void Initialize();
		
	};
};

#endif //WAYPOINT_H