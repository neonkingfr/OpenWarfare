/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	VehicleList.h

Purpose:

	This file contains the declaration of the VehicleList class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	2.0			02-04/2009	RMR: Original Implementation
	2.01		11-02/2010	MHA: Comments Checked

/************************************************************************/

#ifndef VBS2FUSION_VEHICLE_LIST_H
#define VBS2FUSION_VEHICLE_LIST_H


/**************************************************************************
  To disable warnings raised at the compile time of a Dynamic Link Library
  (DLL) due to defining objects of classes which has members of types 
  defined in the Standard Template Library (STL). 
  Warning identifier [C:4251]
/**************************************************************************/
#pragma warning (push)
#pragma warning (disable: 4251)

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>

// SIMCENTRIC INCLUDES
#include "position3D.h"
#include "VBS2Fusion.h"
#include "data/Vehicle.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/

namespace VBS2Fusion
{
	class VBS2FUSION_API VehicleList
	{
	public:	
		/*!
		Typedef for the vehicle list in the form of a Vehicle vector.
		*/
		typedef vector<Vehicle> vehicleList;

		/*!
		Iterator for the vehicle list. 
		*/
		typedef vector<Vehicle>::iterator iterator;

		/*!
		Const iterator for the vehicle list. 
		*/
		typedef vector<Vehicle>::const_iterator const_iterator;	

		/*!
		The main constructor for the VehicleList class
		*/
		VehicleList() ;

		/*!
		The main destructor for the VehicleList class
		*/
		~VehicleList() ;

		/*!
		Adds a vehicle object to the list.
		*/
		void addVehicle(const Vehicle& vehicle);	

		/*!
		Removes the vehicle defined by vehicle from the list. 
		Returns false if the vehicle is not a member of the vehicle list. 
		*/		
		bool removeVehicle(Vehicle& vehicle);

		/*!
		Removes the vehicle with the ID strID from the list. 
		Returns false if the vehicle is not a member of the vehicle list. 
		*/
		bool removeVehicleByID(string strID);

		/*!
		Removes the vehicle with the name strNameD from the list. 
		Returns false if the vehicle is not a member of the vehicle list. 
		*/
		bool removeVehicleByName(string strName);

		/*!
		Removes the vehicle with the alias strAlias from the list. 
		Returns false if the vehicle is not a member of the vehicle list. 
		*/
		bool removeVehicleByAlias(string strAlias);
		
		/*!
		Returns true if the passed in vehicle is a member of the vehicle list. 
		*/
		bool isMember(Vehicle& vehicle);

		/*!
		Returns true if a vehicle with the alias strAlias is a member of the vehicle list. 
		*/
		bool isMemberAlias(string strAlias);

		/*!
		Returns true if a vehicle with the name strName is a member of the vehicle list. 
		*/
		bool isMemberName(string strName);

		/*!
		Returns true if a vehicle with the ID strID is a member of the vehicle list. 
		*/
		bool isMemberID(string strID);

		/*!
		Start of vehicle list. i.e. begin iterator. 
		*/
		iterator begin();

		/*!
		Start of vehicle list. i.e. begin const iterator. 
		*/
		const_iterator begin() const;
		
		/*!
		End of vehicle list. i.e. end iterator. 
		*/
		iterator end();

		/*!
		End of vehicle list. i.e. const end iterator. 
		*/
		const_iterator end() const;

		/*!
		Clears the vehicle list. 
		*/
		void clearVehicles();

		/*!
		Returns the number of vehicles on the vehicle list. 
		*/
		int getNumberOfVehicles();

	private:

		vehicleList _vehicles;
	};
};

#pragma warning (pop) // Enable warnings [C:4251]

#endif //VEHICLE_LIST_H