
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************

Name:

	GeomObject.h

Purpose:

	This file contains the declaration of the GeometryObject class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			12-06/2011	NDB: Original Implementation

/************************************************************************/

#ifndef VBS2FUSION_GEOMOBJ_H
#define VBS2FUSION_GEOMOBJ_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <string>

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "VBS2FusionDefinitions.h"


/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBS2Fusion
{
	struct GeometryFace
	{
		int nIndices;     // count of indices in 'indices' array (3 or 4)
		int indices[4];   // array of indices to vertex array

		GeometryFace(){nIndices = 0;}
		GeometryFace(const GeometryFace& face){*this = face;}
	};

	struct GeometryVertex
	{
		float x,y,z;
	};
	


	class VBS2FUSION_API GeomObject
	{
	public:
#if GEOM_OBJECT
		/*!
		Main constructor for this class. The following parameters are initialized as follows:
		*/
		GeomObject();

		/*!
		Main destructor for this class. 
		*/
		~GeomObject();

		/*!
		Assignment operator for GeomObject class.
		Assign all the attributes of GeomObject class.
		*/
		GeomObject& operator =(const GeomObject& object);

		/*!
		Sets the Type of the Geometry object using a GEOMTYPE variable.
		 \param GEOMTYPE is the Geometry object type it is an enumerated value defined in the header file.
		 values can be type of GEOM_HOUSE, GEOM_TREE, GEOM_LANDSCAPE, GEOM_ROAD				
		*/	
		void setType(GEOMTYPE type);

		/*!
		returns the type of the Geometry Object
		*/
		GEOMTYPE getType() const;

		/*!
		sets the objectId
		*/
		void setObjectId(long long objectId);

		/*!
		returns the objectId
		*/
		long long getObjectId() const;

		/*!
		sets the world transform matrix of the object
		*/
		void setObjectTransform(float transform[4][3]);

		/*!
		returns the transform matrix pointer. It gives the first element pointer,
		then iterate it and get the float[4][3] (2D Array)
		*/
		float* getObjectTrasformPtr();

		/*!
		set the vertices pointer
		*/
		void setVerticesPtr(GeometryVertex* vertices);

		/*!
		returns vertices pointer
		Then iterator it to get all vertices
		*/
		GeometryVertex* getVertiecesPtr() const;

		/*!
		set no of vertices
		*/
		void setNoOfVertices(int noOfVertices);

		/*!
		get no of vertices
		*/
		int getNoOfVertices() const;

		/*!
		set the faces pointer
		*/
		void setFacesPtr(GeometryFace* faces);

		/*!
		returns faces pointer
		Then iterate it to get all the faces
		*/
		GeometryFace* getFacesPtr() const;

		/*!
		set no of faces
		*/
		void setNoOfFaces(int noOfFaces);

		/*!
		get no of faces
		*/
		int getNoOfFaces() const;
		
	private:
		void Initialize();

		GEOMTYPE _type;                // type of object
		long long _objectId;   // VBS id of object
		float _transform[4][3];    // world transformation of the object
		GeometryVertex* _vertices;        // array of object's vertices
		int _nVertices;            // number of vertices in 'vertices' array
		GeometryFace* _faces;      // array of object's faces
		int _nFaces;               // number of faces in 'faces' array
#endif	
	};

}; 

#endif //VBS2FUSION_GEOMOBJ_H
