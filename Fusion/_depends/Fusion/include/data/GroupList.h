
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************

Name:

	GroupList.h

Purpose:

	This file contains the declaration of the GroupList class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			01-04/2009	RMR: Original Implementation
	1.01		31-08/2009  SDS: updated clearGroups;
	2.0			23-12/2009  UDW: Initial implementation
								added functions getGroupList(), getStrGroupList(),
								operator= and operator==
	2.01			11-02/2010	MHA: Comments Checked


/************************************************************************/

#ifndef VBS2FUSION_GROUP_LIST_H
#define VBS2FUSION_GROUP_LIST_H


/**************************************************************************
  To disable warnings raised at the compile time of a Dynamic Link Library
  (DLL) due to defining objects of classes which has members of types 
  defined in the Standard Template Library (STL). 
  Warning identifier [C:4251]
/**************************************************************************/
#pragma warning (push)
#pragma warning (disable: 4251)

/***********************************************************************/
/* INCLUDES
/***********************************************************************/

// Standard Includes
#include <set>
#include <string>


// Simcentric Includes
#include "position3D.h"
#include "VBS2Fusion.h"
#include "data/Group.h"
#include "data/Unit.h"

/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBS2Fusion
{
	class VBS2FUSION_API GroupList
	{
	public:
		/*!
		Typedef for vector containing all groups belonging to the GroupList. 
		*/
		typedef set<Group> groupList;

		/*!
		Iterator for GroupList
		*/
		typedef set<Group>::iterator iterator;

		/*!
		Const iterator for GroupList
		*/
		typedef set<Group>::const_iterator const_iterator;
		

		/*!
		Typedef for string vector containing the names of all the 
		groups belonging to the GroupList. 
		*/
		typedef vector<string> strGroupList;

		/*!
		Main constructor for the GroupList
		*/
		GroupList();

		/*!
		 Copy constructor
		 */
		GroupList(const GroupList& _groupList);

		/*!
		Main destructor for the GroupList
		*/
		~GroupList();

		/*!
		Adds a group to the group list. 
		*/
		void addGroup(const Group& group);

#ifdef DEVELOPMENT
		/*!
		Adds a new empty group with the specified alias to the group list
		*/
		void addGroup(string strGroupName);

		/*!
		Searches the GroupList for an element with a alias of given group and 
		returns an iterator to it if found, otherwise it returns an iterator 
		to set::end (the element past the end of the container).
		*/
		GroupList::iterator getGroup(Group& group);
#endif

		/*!
		Removes the specified group from the list. Returns false if the group was not a 
		member of the list and the operation failed. 
		*/
		bool removeGroup(Group& group);

		/*!
		Removes the specified group from the list. Returns false if the group was not a 
		member of the list and the operation failed. 
		*/
		bool removeGroup(string strGroupName);

		/*!
		Removes the specified group from the list. Returns false if the group was not a 
		member of the list and the operation failed. 
		*/
		bool removeGroupByAlias(string strAlias);

		/*!
		Returns true if the group specified by group.getName() is a member of the group list. 
		*/
		bool isMember(Group& group);

		/*!
		Returns true if the group specified by strGroupName is a member of the group list. 
		*/
		bool isMember(string strGroupName);

		/*!
		Returns true if the group specified by strAlias is a member of the group list. 
		*/
		bool isMemberGroupByAlias(string strGroupAlias);

		/*!
		Returns true if the unit given by unit.getAlias() is a member of one of the groups
		belonging to the group list. 
		*/
		bool isMemberByAlias(Unit& unit);

		/*!
		Returns true if the unit given by unit.getName() is a member of one of the groups
		belonging to the group list. 
		*/
		bool isMemberByName(Unit& unit);

		/*!
		Returns true if the unit given by unit.getID() is a member of one of the groups
		belonging to the group list. 
		*/
		bool isMemberByID(Unit& unit);
	
		/*!
		Returns true if a waypoint with the alias waypoint.getAlias() is a member of the group.
		*/
		bool isWaypointMemberByAlias(Waypoint& waypoint);

		/*!
		Returns true if a waypoint with the name waypoint.getName() is a member of the group.
		*/
		bool isWaypointMemberByName(Waypoint& waypoint);

		/*!
		Returns true if the unit with the alias strUnitAlias is a member of one of the groups
		belonging to the group list. 
		*/
		bool isMemberByAlias(string strUnitAlias);

		/*!
		Returns true if the unit with the name strUnitName is a member of one of the groups
		belonging to the group list. 
		*/
		bool isMemberByName(string strUnitName);

		/*!
		Returns true if a waypoint with the alias strWaypointAlias is a member of the group.
		*/
		bool isWaypointMemberByAlias(string strWaypointAlias);

		/*!
		Returns true if a waypoint with the name strWaypointName is a member of the group.
		*/
		bool isWaypointMemberByName(string strWaypointName);

		/*!
		Returns true if a waypoint with the ID strWaypointID is a member of the group.
		*/
		bool isMemberByID(string strWaypointID);

		/*!
		Returns the member with the specified alias. Returns an empty unit 
		if the unit is not a member of the group list. 
		*/

		Unit getMemberByAlias(string strUnitAlias);

		/*!
		Returns the member with the specified name. Returns an empty unit 
		if the unit is not a member of the group list. 
		*/
		Unit getMemberByName(string strUnitName);

		/*!
		Returns the member with the specified network ID. Returns an empty unit 
		if the unit is not a member of the group list. 
		*/
		Unit getMemberByID(string strUnitID);

		/*!
		Returns the member with the specified alias. Returns an empty waypoint if the waypoint
		is not a member of the group. 
		*/
		Waypoint getWaypointMemberByAlias(string strWaypointAlias);

		/*!
		Returns the member with the specified name. Returns an empty waypoint if the waypoint
		is not a member of the group. 
		*/
		Waypoint getWaypointMemberByName(string strWaypointName);

		/*!
		Updates the the unit data inside the "group list" according to the passed-in "unit", if that unit
		exists in the group list.
		 - User must have set the "Alias" of the "unit" object (e.g. using unit.setAlias() function) and this
			"Alias" of the "unit" will be used to check whether the unit exists in the "group list".
		 - User must have set the "Group" of the "unit" object(e.g. using unit.setGroup() function) and this
			will be used to identify the "Group" of the "unit" object from the "group list".
		 - If these two parameters are not set correctly or the unit doesn't exist in the "group list", the
			function will do nothing.
		*/
		void updateMemberByAlias(Unit& unit);

		/*!
		Updates the member given by unit.getName() if it exists. If the unit 
		is not a member of one of the groups, no operation is performed. 
		*/
		void updateMemberByName(Unit& unit);

		/*!
		Updates the the unit data inside the "group list" according to the passed-in "unit", if that unit
		exists in the group list.
			- User must have set the "ID" of the "unit" object (e.g. using unit.setID() function) and this
				"ID" of the "unit" will be used to check whether the unit exists in the "group list".
			- User must have set the "Group" of the "unit" object(e.g. using unit.setGroup() function) and this
				will be used to identify the "Group" of the "unit" object from the "group list".
			- If these two parameters are not set correctly or the unit doesn't exist in the "group list", the
				function will do nothing.
		*/
		void updateMemberByID(Unit& unit);

		/*!
		Update the member with the alias given by waypoint.getAlias(). 
		*/
		void updateWaypointMemberByAlias(Waypoint& waypoint);

		/*!
		Update the member with the name given by waypoint.getName(). 
		*/
		void updateWaypointMemberByName(Waypoint& waypoint);

		/*!
		Begin iterator for group list. 
		*/
		iterator begin();

		/*!
		Begin iterator for group list. 
		*/
		const_iterator begin() const;

		/*!
		End iterator for group list. 
		*/
		iterator end();

		/*!
		End iterator for group list. 
		*/
		const_iterator end() const;

		/*!
		Clear the group list. 
		*/
		void clearGroups();

		/*!
		Returns the number of groups on the list. 
		*/
		int getNumberOfGroups();

		/*!
		 get the groupList consisting of all Units
		 */
		groupList getGroupList() const;

		/*!
		 Get the strGroupList, a list of string consisting group names
		 */
		strGroupList getStrGroupList() const;

		/*!
		Assignment operator for GroupList class.
		Assign all the attributes of GroupList class.
		*/
		GroupList& operator=(const GroupList& _groupList);

		/*!
		 Overloaded comparison operator
		 */
		bool operator==(const GroupList& _groupList) const;


	private:

		groupList _groups;
		strGroupList _strGroups;

	};
};

#pragma warning (pop) // Enable warnings [C:4251]

#endif //VBS2FUSION_GROUP_LIST_H