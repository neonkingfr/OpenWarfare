/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	MarkerList.h

Purpose:

	This file contains the declaration of the MarkerList class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			31-12-2012	RDJ: Original Implementation
	

/************************************************************************/

#ifndef VBS2FUSION_MARKER_LIST_H
#define VBS2FUSION_MARKER_LIST_H

/**************************************************************************
  To disable warnings raised at the compile time of a Dynamic Link Library
  (DLL) due to defining objects of classes which has members of types 
  defined in the Standard Template Library (STL). 
  Warning identifier [C:4251]
/**************************************************************************/
#pragma warning (push)
#pragma warning (disable: 4251)
#include "data/Marker.h"
namespace VBS2Fusion
{
	class VBS2FUSION_API MarkerList
	{
	public:	
		/*!
		Typedef for vector list of Markers. 
		*/
		typedef vector<Marker> markerList;

		/*!
		Iterator for Markers.
		*/
		typedef vector<Marker>::iterator iterator;

		/*!
		Const Iterator for Markers.
		*/
		typedef vector<Marker>::const_iterator const_iterator;	

		

		/*!
		Main constructor for MarkerList class. 
		*/
		MarkerList();

		/*!
		Main destructor for MarkerList class. 
		*/
		~MarkerList();

		/*!
		Adds a Marker to the list. 
		*/
		void addMarker(Marker& marker);	

		/*!
		Adds a new Markers to the list and assigns name strMarkerName. 
		*/
		void addMarker(const string& strMarkerName);

		/*!
		Removes marker defined by marker. Returns false if marker
		is not a member of the marker list. 
		*/
		bool removeMarker(Marker& marker);

		/*!
		Removes marker with name strMarkerName. Returns false if marker
		is not a member of the marker list. 
		*/
		bool removeMarker(string& strMarkerName);

		/*!
		Returns true if a marker with the name marker.getName()
		exists on the marker list. 
		*/
		bool isMember(Marker& marker);

		/*!
		Returns true if a marker with the name strMarkerName
		exists on the marker list. 
		*/
		bool isMember(string strMarkerName);

		/*!
		Start of marker list. i.e. begin iterator. 
		*/
		iterator begin();

		/*!
		Start of marker list. i.e. begin const iterator. 
		*/
		const_iterator begin() const;

		/*!
		End of marker list. i.e. end iterator. 
		*/
		iterator end();

		/*!
		end of marker list. i.e. const end iterator. 
		*/
		const_iterator end() const;

		/*!
		Clears the marker list. 
		*/
		void clearMarkers();

		/*!
		Returns the number of Markers. 
		*/
		int getNumberOfMarkers();

	private:

		markerList _markers;
		

	};

};
#pragma warning (pop)
#endif