
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:  Matrix4.h

Purpose:

	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			23-12/2010	YFP: Original Implementation
	1.01		20-07/2011	NDB: Edited void setRotationX(float angle)
								 void setRotationY(float angle)
								 void setRotationZ(float angle)
	1.02		26-08/2011	NDB: added void Normalize()
								 Comments Added

/*****************************************************************************/

#ifndef VBS2FUSION_MATRIX4D_H
#define VBS2FUSION_MATRIX4D_H

/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "Vector3f.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBS2Fusion
{
	class VBS2FUSION_API Matrix4f
	{

	public:
		/*!
		Main constructor for this class.
		It initialize as identity matrix
		*/
		Matrix4f();

		/*!
		Main destructor for this class. 
		*/
		~Matrix4f();

		/*!
		The assignment operator for Matrix4f.
		*/
		Matrix4f & operator = (const Matrix4f& rhs);

		/*!
		Multiplication operator for Matrix4f 
		current matrix by given matrix
		*/
		Matrix4f operator * (const Matrix4f& mat);

		/*!
		Multiplication operator for Matrix4f 
		current matrix by given matrix
		*/
		Matrix4f operator * (float sclar);

		/*!
		Multiplication operator for Matrix4f 
		current matrix by given matrix
		*/
		Matrix4f operator + (const Matrix4f& mat);
		
		/*!
		Multiplication operator for Matrix4f 
		current matrix by given matrix
		*/
		Matrix4f operator - (const Matrix4f& mat);

		/*!
		transpose current Matrix 
		*/
		void transpose();

		/*!
		return Inverse matrix
		*/
		Matrix4f inverse();

		/*!
		returns the determinant of the matrix
		*/
		float determinant();

		/*!
		set the matrix as identity matrix
		*/
		void setIdentity();

		/*!
		set the matrix value as zero
		*/
		void setZero();

		/*!
		set translation or position of the matrix
		*/
		void setTranslation(float x, float y , float z);

		/*!
		set the position of the matrix
		*/
		void setPosition(float x, float y , float z);

		/*!
		Rotation is Axis Z to Y direction
		angle - in degree
		*/
		void setRotationX(float angle);

		/*!
		Rotation is Axis Y to X direction
		angle - in degree
		*/
		void setRotationZ(float angle);

		/*!
		Rotation is Axis Z to X direction
		angle - in degree
		*/
		void setRotationY(float angle);

		/*!
		Returns Direction vector
		*/
		Vector3f getDirection();

		/*!
		Returns Up vector
		*/
		Vector3f getDirectionUp();

		/*!
		Returns Side vector
		*/
		Vector3f getDirectionSide();

		/*!
		Returns Position vector
		*/
		Vector3f getPosition() const;

		/*!
		returns the matrix element
		row,col- values range [0 -> 3]
		*/
		float getElement(int row, int col) const;

		/*!
		sets the matrix element
		row,col- values range [0 -> 3]
		val - value need to be set as element
		*/
		void setElement(int row, int col, float val);

		/*!
		sets elements of matrix from the input matrix
		input matrix should be float[4][3]
		*/
		void setMatrix(float mat[4][3]);

		/*!
		returns the matrix array
		give the reference array as a input parameter
		refMat - reference array(float[4][3]
		*/
		void getMatrix(float refMat[4][3]);

	public:
		/*float _m00, _m01 , _m02 , _m03;
		float _m10, _m11 , _m12 , _m13;
		float _m20, _m21 , _m22 , _m23;
		float _m30, _m31 , _m32 , _m33;*/

		union {
			struct {
				float _m00, _m01 , _m02 , _m03;
				float _m10, _m11 , _m12 , _m13;
				float _m20, _m21 , _m22 , _m23;
				float _m30, _m31 , _m32 , _m33;

			};
			float _m[4][4];
		};

	private:
		void setMatrixToM();
		float determinant(float arr[2][2]);
		float determinant(float arr[3][3]);		

		//float _m[4][4];

	};

}



#endif