/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	Vehicle.h

Purpose:

	This file contains the declaration of the Vehicle class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	2.0			01-04/2009	RMR: Original Implementation
	2.01		10-02/2010	MHA: Comments Checked

/************************************************************************/

#ifndef VBS2FUSION_THREAD_H
#define VBS2FUSION_THREAD_H

//project dependencies

//win32 specific
#include <Windows.h>
#include "VBS2Fusion.h"

//end project dependencies

namespace VBS2Fusion
{

	class VBS2FUSION_API Thread {

	public:
		//constructors
		Thread ();

		//destructor
		virtual ~Thread ();

		virtual bool CreateThread ();

		// returns 1 (true) if thread has been created, else 0 (false).
		int	IsCreated ()
		{	return (this->hThread != NULL);	}

		DWORD		Timeout;

		HANDLE	GetThreadHandle ()
		{	return this->hThread;	}
		DWORD	GetThreadId ()
		{	return this->hThreadId;	}
		HANDLE	GetMainThreadHandle ()
		{	return this->hMainThread;	}
		DWORD	GetMainThreadId ()
		{	return this->hMainThreadId;	}

		// start routine for the thread, which in turn calls the over-ridable Thread::Process function.
		static int runProcess (void* Param);

	protected:

		//override able
		virtual unsigned long Process (void* parameter);	

		DWORD		hThreadId;
		HANDLE		hThread;
		DWORD		hMainThreadId;
		HANDLE		hMainThread;
		
	private:

		

	};

	struct VBS2FUSION_API param {
		Thread*	pThread;
	};
};

#endif // THREAD_H