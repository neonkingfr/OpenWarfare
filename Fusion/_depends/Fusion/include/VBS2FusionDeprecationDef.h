/**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************

Name:

	VBS2FusionDeprecationDef.h

Purpose:

	This file contains the deprecation warning message definitions of VBS2Fusion.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			07-07/2012	CGS: Original Implementation
	

/************************************************************************/
#ifndef VBS2FUSION_DEPRECATION_H
#define VBS2FUSION_DEPRECATION_H

/*!
Define deprecated declaration into VBS2FUSION_DPR Macro 
*/
#define  VBS2FUSION_DPR(DPR_WarningMessage) __declspec(deprecated(DPR_WarningMessage))


/******************************* DEPRECATED CLASS MACROS ***********************************/

/*! Data Class */
#define DAGV "Deprecated Data class, Use Vehicle Data class instead."/*Warning message for ArmedGroundVehicle */
#define DARM "Deprecated Data class, Use Vehicle Data class instead."	/*Warning message for ArmedModule */
#define DGRV "Deprecated Data class, Use Vehicle Data class instead."	/*Warning message for GroundVehicle */

/*! Utility Class */
#define UAGV "Deprecated Utility class, Use VehicleUtilities class instead." /*Warning message for ArmedGroundVehicleUtilities */
#define UARM "Deprecated Utility class, Use VehicleUtilities class instead."	/*Warning message for ArmedModuleUtilities */
#define UTUR "Deprecated Utility class, Use VehicleUtilities class instead."	/*Warning message for TurretUtilities */

/******************************* DEPRECATED FUNCTION MACROS ***********************************/

/**********Data Class Functions**********/

/*! Turret Data Class */
#define DTUR001 "Deprecated function, Use Vehicle::clearMagazineList() instead" /*Warning message for clearMagazinelist() */
#define DTUR002 "Deprecated function, Use Vehicle::getMagazineCount() instead" /*Warning message for getMagazineCount() */
#define DTUR003 "Deprecated function, Use Vehicle::getCurrentMagazine() instead" /*Warning message for getCurrentMagazine() */
#define DTUR004 "Deprecated function, Use Vehicle::addMagazine(Magazine magazine) instead" /*Warning message for setMagazine(Magazine magazine) */


/**********Utility Class Functions**********/

/*! WeaponUtilities Class */
#define UWPN001 "Deprecated function, Use WeaponUtilities::getAmmoCount(Vehicle& vehicle, Weapon& weapon) instead" /*Warning message for getAmmoCount(ArmedGroundVehicle& vehicle, Weapon& weapon)*/
#define UWPN002 "Deprecated function, Use WeaponUtilities::getAzimuth(Vehicle& vehicle, Turret& turret, Weapon& weapon) instead" /*Warning message for getAzimuth(ArmedGroundVehicle& vehicle, Turret& turret, Weapon& weapon)*/
#define UWPN003 "Deprecated function, Use WeaponUtilities::getElevation(Vehicle& vehicle, Turret& turret, Weapon& weapon) instead" /*Warning message for getElevation(ArmedGroundVehicle& vehicle, Turret& turret, Weapon& weapon)*/
#define UWPN004 "Deprecated function, Use WeaponUtilities::getWeaponDirectionVector(Vehicle& vehicle, Turret& turret, Weapon& weapon) instead" /*Warning message for getWeaponDirectionVector(ArmedGroundVehicle& vehicle, Turret& turret, Weapon& weapon)*/
#define UWPN005 "Deprecated function, Use WeaponUtilities::setWeaponState(Vehicle& vehicle,Weapon& weapon, bool reloadStatus,double reloadTime) instead" /*Warning message for setWeaponState(ArmedGroundVehicle& vehicle,Weapon& weapon, bool reloadStatus,double reloadTime)*/
#define UWPN007 "Deprecated function, Use WeaponUtilities::applyDummyRound(Projectile& proj, bool enable ) instead" /*Warning message for void setDummyRound(Projectile& proj, bool enable )*/
#define UWPN008 "Deprecated function, Use WeaponUtilities::applyShotCreationEnable(bool enable) instead" /*Warning message for void allowCreateShot(bool enable)*/
#define UWPN009 "Deprecated function, Use WeaponUtilities::applyWeaponState(Vehicle& vehicle,Weapon& weapon, bool reloadStatus,double reloadTime) instead" /*Warning message for setWeaponState(Vehicle& vehicle,Weapon& weapon, bool reloadStatus,double reloadTime)*/

/*! ControllableObjectUtilities Class */
#define UCOB043 "function has been deprecated and will be removed in future, Use vector3D getBoundingBoxMinExtremePoints(ControllableObject& co) instead"  /*Warning message for vector3D getBoundingBoxMinExtremePoints(ControllableObject& co, ControllableObject& tco)*/
#define UCOB044 "function has been deprecated and will be removed in future, Use vector3D getBoundingBoxMaxExtremePoints(ControllableObject& co) instead"  /*Warning message for vector3D getBoundingBoxMaxExtremePoints(ControllableObject& co, ControllableObject& tco)*/

/*! PlayerUtilities Class*/
#define UPLY001 "Deprecated function, Use PlayerUtilities::applyPlayable(Unit& player) instead" /*Warning message for setPlayable(Unit& player)*/
#define UPLY002 "Deprecated function, Use PlayerUtilities::applySwitchPlayer(Unit& nplayer , Unit& oplayer) instead" /*Warning message for switchPlayer(Unit& nplayer , Unit& oplayer)*/
#define UPLY003 "Deprecated function, Use PlayerUtilities::applyFiringForce(Unit& player) instead" /*Warning message for forceFire(Unit& player)*/
#define UPLY004 "Deprecated function, Use PlayerUtilities::getOpticsBrightness() instead" /*Warning message for GetOpticsBrightness()*/
#define UPLY005 "Deprecated function, Use PlayerUtilities::getOpticsBrightnessMin() instead" /*Warning message for GetOpticsBrightnessMin()*/
#define UPLY006 "Deprecated function, Use PlayerUtilities::getOpticsBrightnessMax() instead" /*Warning message for GetOpticsBrightnessMax()*/
#define UPLY007 "Deprecated function, Use PlayerUtilities::getOpticsMaxGainAmplifier() instead" /*Warning message for GetOpticsMaxGainAmplifier()*/ 
#define UPLY008 "Deprecated function, Use PlayerUtilities::getOpticsBrightnessOffset() instead" /*Warning message for GetOpticsBrightnessOffset()*/ 
#define UPLY009 "Deprecated function, Use PlayerUtilities::getOpticsAutoBS() instead" /*Warning message for GetOpticsAutoBS()*/ 
#define UPLY010 "Deprecated function, Use PlayerUtilities::getOpticsBlurCoef() instead" /*Warning message for GetOpticsBlurCoef()*/ 
#define UPLY011 "Deprecated function, Use PlayerUtilities::applyOpticsBrightness(float brightnessVal) instead" /*Warning message for setOpticsBrightness(float brightnessVal)*/ 
#define UPLY012 "Deprecated function, Use PlayerUtilities::applyOpticsBrightnessMin(float minBrightVal) instead" /*Warning message for setOpticsBrightnessMin(float minBrightVal)*/  
#define UPLY013 "Deprecated function, Use PlayerUtilities::applyOpticsBrightnessMax(float maxBrightVal) instead" /*Warning message for setOpticsBrightnessMax(float maxnBrightVal)*/  
#define UPLY014 "Deprecated function, Use PlayerUtilities::applyOpticsMaxGainAmplifier(float maxGainAmp) instead" /*Warning message for setOpticsMaxGainAmplifier(float maxGainAmp)*/
#define UPLY015 "Deprecated function, Use PlayerUtilities::applyOpticsBrightnessOffset(float brightOff) instead" /*Warning message for setOpticsBrightnessOffset(float brightOff)*/ 
#define UPLY016 "Deprecated function, Use PlayerUtilities::applyOpticsAutoBC(bool mode) instead" /*Warning message for setOpticsAutoBC(bool mode)*/  
#define UPLY017 "Deprecated function, Use PlayerUtilities::applyOpticsBlurCoef(float blurAmount) instead" /*Warning message for setOpticsBlurCoef(float blurAmount)*/   
#define UPLY018 "Deprecated function, Use PlayerUtilities::applyOpticsDOF(double focus, double blur) instead" /*Warning message for setOpticsDOF(double focus, double blur)*/
#define UPLY019 "Deprecated function, Use PlayerUtilities::applyAllowControlslnDialogMove(bool boolVal) instead" /*Warning message for allowMovControlsInDialog(bool boolVal)*/ 
#define UPLY020 "Deprecated function, Use PlayerUtilities::applyEnableCommanding(bool commanding) instead" /*Warning message for enableCommanding(bool commanding)*/ 
#define UPLY021 "Deprecated function, Use PlayerUtilities::applyPlayer(Unit& player) instead" /*Warning message for setPlayer(Unit& player)*/

/*! TriggerUtilities Class */
#define UTGR001 "function has been deprecated and will be removed in future, Use double applyOnActivationStatement(Trigger& trigger, string command) instead"  /*Warning message for addTriggerOnActivated(Trigger& trigger, string command)*/
#define UTGR002 "function has been deprecated and will be removed in future, Use void applyOnActivationStatementRemoval(Trigger& trigger, double index) instead"  /*Warning message for removeTriggerOnActivated(Trigger& trigger, double index)*/
#define UTGR003 "function has been deprecated and will be removed in future, Use void applyActivationType(Trigger& trig, string& by, string& type, bool repeat) instead"  /*Warning message for setTriggerActivation(Trigger& trig, string by, string type, bool repeat)*/
#define UTGR004 "function has been deprecated and will be removed in future, Use list<ControllableObject> TriggerUtilities::getAttachedObjects(Trigger& trig) instead"  /*Warning message for triggerAttachedObj(Trigger& trig)*/
#define UTGR005 "function has been deprecated and will be removed in future, Use void applyObjectAttach(Trigger& trig, ControllableObject& co) instead"  /*Warning message for triggerAttachObj(Trigger& trig, ControllableObject& co)*/
#define UTGR006 "function has been deprecated and will be removed in future, Use void applyObjectDetach(Trigger& trig, ControllableObject& co) instead"  /*Warning message for triggerDetachObj(Trigger& trig, ControllableObject& co)*/
#define UTGR007 "function has been deprecated and will be removed in future, Use void applySynchronization(Trigger& trig, list<Waypoint> wplist) instead"  /*Warning message for synchronizeTrigger(Trigger& trig, list<Waypoint> wplist)*/
#define UTGR008 "function has been deprecated and will be removed in future, Use void applyMusicEffect(Trigger& trig, string& trackName) instead"  /*Warning message for setMusicEffect(Trigger& trig, string& trackName)*/
#define UTGR009 "function has been deprecated and will be removed in future, Use void applySoundEffect(Trigger& trig, string& sound, string& voice, string& soundEnv, string& soundDet) instead"  /*Warning message for setSoundEffect(Trigger& trig, string& sound, string& voice, string& soundEnv, string& soundDet)*/
#define UTGR010 "function has been deprecated and will be removed in future, Use void applyTitleEffect(Trigger& trig, TITLE_EFFECT_CLASS classType, TITLE_EFFECT_TYPE effect, string& text) instead"  /*Warning message for setTitleEffect(Trigger& trig, string& type, string& effect, string& text)*/
#define UTGR011 "function has been deprecated and will be removed in future, Use void applyEffectCondition(Trigger& trig, string& statement) instead"  /*Warning message for setEffectCondition(Trigger& trig, string& statement)*/
#define UTGR012 "function has been deprecated and will be removed in future, Use the alternative mechanism mentioned in header file comment" /*Warning message for networkIdToTrigger(Trigger& trig, NetworkID id)*/
#define UTGR014 "function has been deprecated and will be removed in future, Use void applyVehicleAttach(Trigger& trig, ControllableObject& co) instead" /*Warning message for triggerAttachVehicle(Trigger& trig, ControllableObject& co)*/

/*! WaypointUtilities Class */
#define UWPT001 "function has been deprecated and will be removed in future, Use void applyObjectAttach(Waypoint& wp, ControllableObject& co) instead"  /*Warning message for WaypointAttachObject(Waypoint& wp, ControllableObject& co)*/
#define UWPT002 "function has been deprecated and will be removed in future, Use void applyLoiterRadius(Waypoint& wp, double radius) instead"  /*Warning message for setLoiterRadius(Waypoint& wp, double radius)*/
#define UWPT003 "function has been deprecated and will be removed in future, Use void applyLoiterType(Waypoint& wp, string type) instead"  /*Warning message for setLoiterType(Waypoint& wp, string type)*/
#define UWPT004 "function has been deprecated and will be removed in future, Use void applyHousePosition(Waypoint& wp, int pos) instead"  /*Warning message for setHousePosition(Waypoint& wp, int pos)*/
#define UWPT005 "function has been deprecated and will be removed in future, Use void applyVariable(Waypoint& wp, string& name, VBS2Variable& val, bool isPublic) instead"  /*Warning message for setWaypointVariable(Waypoint& wp, string name, VBS2Variable& val, bool pub)*/
#define UWPT006 "function has been deprecated and will be removed in future, Use void applyMusicEffect(Waypoint& wp, string& trackName) instead"  /*Warning message for setMusicEffect(Waypoint& wp, string& trackName)*/
#define UWPT007 "function has been deprecated and will be removed in future, Use void applySoundEffect(Waypoint& wp, string& sound, string& voice, string& soundEnv, string& soundDet) instead"  /*Warning message for setSoundEffect(Waypoint& wp, string& sound, string& voice, string& soundEnv, string& soundDet))*/
#define UWPT008 "function has been deprecated and will be removed in future, Use void applyTitleEffect(Waypoint& wp, TITLE_EFFECT_CLASS classType, TITLE_EFFECT_TYPE effect, string& text) instead"  /*Warning message for setTitleEffect(Waypoint& wp, string& type, string& effect, string& text)*/
#define UWPT009 "function has been deprecated and will be removed in future, Use void applyEffectCondition(Waypoint& wp, string& statement) instead"  /*Warning message for setEffectCondition(Waypoint& wp, string& statement)*/

/*! TerrainUtilities Class */
#define UTER001 "function has been deprecated and will be removed in future, Use double getTerrainHeight(double xCoordinate, double zCoordinate) instead" /*Warning message for TerrainHeight(double xCoordinate, double zCoordinate)*/
#define UTER002 "function has been deprecated and will be removed in future, Use double getRoadSurfaceHeight(double xCoordinate, double zCoordinate) instead" /*Warning message for RoadSurfaceHeight(double xCoordinate, double zCoordinate)*/ 
#define UTER003 "function has been deprecated and will be removed in future, Use bool isUprightCylinderCollision(position3D& center, double radius) instead" /*Warning message for uprightCylinderCollision(position3D& center, double radius)*/ 
#define UTER004 "function has been deprecated and will be removed in future, Use bool applyMapExportToSHP(string objectFileName , string roadFileName) instead" /*Warning message for exportTerrainMapSHP(string objectFileName , string roadFileName)*/ 
#define UTER005 "function has been deprecated and will be removed in future, Use string getPositionToGrid(position3D pos) instead" /*Warning message for positionToGrid(position3D pos)*/
#define UTER006 "function has been deprecated and will be removed in future, Use position3D getGridToPosition(string gridPosition) instead" /*Warning message for gridtoPosition(string gridPosition)*/
#define UTER007 "function has been deprecated and will be removed in future, Use string getPositionToMGRS(position3D pos, int precision) instead" /*Warning message for positionToMGRS(position3D pos, int precision)*/
#define UTER008 "function has been deprecated and will be removed in future, Use string getPositionToMGRS(position3D pos) instead" /*Warning message for positionToMGRS(position3D pos)*/
#define UTER009 "function has been deprecated and will be removed in future, Use string getPositionToLat(position3D pos , int precision) instead"/*Warning message for positionToLat(position3D pos , int precision)*/
#define UTER010 "function has been deprecated and will be removed in future, Use string getPositionToLat(position3D pos) instead"/*Warning message for positionToLat(position3D pos)*/
#define UTER011 "function has been deprecated and will be removed in future, Use string getPositionToLon(position3D pos, int precision) instead" /*Warning message for positionToLon(position3D pos, int precision)*/
#define UTER012 "function has been deprecated and will be removed in future, Use string getPositionToLon(position3D pos) instead" /*Warning message for positionToLon(position3D pos)*/
#define UTER013 "function has been deprecated and will be removed in future, Use void applyTerrainHeightsReset() instead" /*Warning message for resetTerrainHeights()*/
#define UTER014 "function has been deprecated and will be removed in future, Use void applyTerrainHeight(float x, float z, float height) instead" /*Warning message for setTerrainHeight(float x, float z, float height)*/
#define UTER016 "function has been deprecated and will be removed in future, Use void applyTerrainHeightArea(position2D pos1, position2D pos2,float height, bool changeLower, bool changeHigher) instead" /*Warning message for setTerrainHeightArea(position2D pos1, position2D pos2,float height, bool changeLower, bool changeHigher)*/
#define UTER018 "function has been deprecated and will be removed in future, Use void applyFlattenGround(position3D position, float radius, float flatten) instead" /*Warning message for setFlattenGround(position3D position, float radius, float flatten)*/
#define UTER019 "function has been deprecated and will be removed in future, Use void applyFlattenGround(ControllableObject& co , float radius, float flatten) instead" /*Warning message for setFlattenGround(ControllableObject& co , float radius, float flatten)*/
#define UTER023 "function has been deprecated and will be removed in future, Use position3D getGridCenterToPosition(string position) instead" /*Warning message for gridCenterToPosition(string position)*/
#define UTER024 "function has been deprecated and will be removed in future, Use bool applyASCIIGridExport(string fileName) instead" /*Warning message for exportASCIIGrid(string fileName)*/
#define UTER025 "function has been deprecated and will be removed in future, Use bool isWaterOnSurface(position3D pos) instead" /*Warning message for surfaceIsWater(position3D pos)*/
#define UTER026 "function has been deprecated and will be removed in future, Use void applyUserChart(string chart) instead" /*Warning message for setUserChart(string chart)*/
#define UTER027 "function has been deprecated and will be removed in future, Use void applyUserChartDrawObjects(bool boolVal) instead" /*Warning message for setUserChartDrawObjects(bool boolVal)*/
#define UTER028 "function has been deprecated and will be removed in future, Use void applyTerrainGrid(double grid) instead" /*Warning message for setTerrainGrid(double grid)*/
#define UTER029 "function has been deprecated and will be removed in future, Use bool applyASCIIGridFileExport(string fileName) instead" /*Warning message for exportASCIIGridFile(string fileName)*/ 
#define UTER030 "function has been deprecated and will be removed in future, Use position3D getMGRSToPosition3D(string MGRSgrid) instead" /*Warning message for MGRSToPosition3D(string MGRSgrid)*/
#define UTER032 "function has been deprecated and will be removed in future, Use void applyLightMapRegenerate() instead" /*Warning message for regenerateLightMap()*/
#define UTER033 "function has been deprecated and will be removed in future, Use position3D getLLToPosition(string latitude,string longitude) instead" /*Warning message for LatToPosition(string latitude,string longitude)*/
#define UTER034 "function has been deprecated and will be removed in future, Use position3D getLLMSToPosition (double latDegree, double latMinutes, double latSecond, double longDegree, double longMinutes,double longSeconds) instead" /*Warning message for position3D LLMSToPosition (double latDegree, double latMinutes, double latSecond, double longDegree, double longMinutes,double longSeconds)*/
#define UTER035 "function has been deprecated and will be removed in future, Use position3D getUTMToPosition(string zone, string easting,string northing) instead" /*Warning message for UMTToPosition(string zone, string easting,string northing)*/
#define UTER036 "function has been deprecated and will be removed in future, Use position3D getUTMBToPosition(string zone, string easting, string northing) instead" /*Warning message for UTMBToPosition(string zone, string easting, string northing)*/
#define UTER037 "function has been deprecated and will be removed in future, Use COLLISION_INFO getCylinderCollision(position3D startPos, position3D endPos, COLLISIONTESTTYPE type, double radius, double density, ControllableObject ignoreObject) instead" /*Warning message for bool cylinderCollision(param1, param2, ect..)*/

/*! VehicleUtilities Class */
#define UVHC001 "function has been deprecated and will be removed in future, Use void applyFire(Vehicle& vehicle, ControllableObject& co) instead" /*Warning message for doFire(Vehicle& vehicle, ControllableObject& co)*/
#define UVHC002 "function has been deprecated and will be removed in future, Use void applyCommandFire(Vehicle& vehicle, ControllableObject& co) instead" /*Warning message for commandFire(Vehicle& vehicle, ControllableObject& co)*/

/*! UnitUtilities Class */
#define UUNT001 "function has been deprecated and will be removed in future, Use void applyUnitPosUP(Unit& u) instead" /*Warning message for unitPosUP(Unit& u)*/
#define UUNT002 "function has been deprecated and will be removed in future, Use void applyCommandWatch(Unit& unit, ControllableObject& target) instead" /*Warning message for commandWatch(Unit& unit, ControllableObject& target)*/  
#define UUNT003 "function has been deprecated and will be removed in future, Use void applyCommandWatch(Unit& unit, position3D& target) instead" /*Warning message for commandWatch(Unit& unit, position3D& target)*/  
#define UUNT004 "function has been deprecated and will be removed in future, Use void applyGlanceAt(Unit& unit, position3D& position) instead" /*Warning message for glanceAt(Unit& unit, position3D& position)*/  
#define UUNT005 "function has been deprecated and will be removed in future, Use void applyLookAt(Unit& unit, position3D& position) instead" /*Warning message for LookAt(Unit& unit, position3D& position)*/  

/*! AARUtilities Class */
#define UAAR001 "function has been deprecated and will be removed in future, Use bool applyAARLoad(string& fileName) instead" /*Warning message for load(string fileName)*/
#define UAAR002 "function has been deprecated and will be removed in future, Use void applyAARPlay() instead" /*Warning message for play()*/
#define UAAR003 "function has been deprecated and will be removed in future, Use void applyAARPause() instead" /*Warning message for pause()*/
#define UAAR004 "function has been deprecated and will be removed in future, Use void applyAARStart() instead" /*Warning message for start()*/
#define UAAR005 "function has been deprecated and will be removed in future, Use void applyAARStop() instead" /*Warning message for stop()*/
#define UAAR006 "function has been deprecated and will be removed in future, Use double getCurrentTime() instead" /*Warning message for currentTime()*/
#define UAAR007 "function has been deprecated and will be removed in future, Use double getReplayLength() instead" /*Warning message for replayLength()*/
#define UAAR008 "function has been deprecated and will be removed in future, Use void applyAARRepeat(bool value) instead" /*Warning message for setRepeat(bool value)*/
#define UAAR009 "function has been deprecated and will be removed in future, Use void applyAARPlayBackTime(double time) instead" /*Warning message for  setPlayBackTime(double time)*/
#define UAAR010 "function has been deprecated and will be removed in future, Use void applyAARRecord() instead" /*Warning message for record()*/
#define UAAR011 "function has been deprecated and will be removed in future, Use void applyAARRecordingStop() instead" /*Warning message for stopRecording()*/
#define UAAR012 "function has been deprecated and will be removed in future, Use void applyAARSave(string fileName) instead" /*Warning message for save(string fileName)*/
#define UAAR013 "function has been deprecated and will be removed in future, Use void applyAARUnload() instead" /*Warning message for unload*/
#define UAAR014 "function has been deprecated and will be removed in future, Use int applyAARBookMarkAddition(double time, string& name, string& message) instead" /*Warning message for addBookMark(double time, string name, string message)*/
#define UAAR015 "function has been deprecated and will be removed in future, Use void applyAARBookMarkRemove(int index) instead" /*Warning message for removeBookMark(int index)*/

/*! CameraUtilities Class */
#define UCMR001 "function has been deprecated and will be removed in future, Use void applyCamCommand(Camera& c, string camCommand) instead" /*Warning message for camCommand(Camera& c, string camCommand)*/
#define UCMR002 "function has been deprecated and will be removed in future, Use NetworkID getCameraOnObjectID() instead" /*Warning message for NetworkID cameraOnObjectID()*/
#define UCMR003 "function has been deprecated and will be removed in future, Use position3D getPositionCameraToWorld(position3D camPos) instead" /*Warning message for position3D positionCameraToWorld(position3D camPos)*/
#define UCMR004 "function has been deprecated and will be removed in future, Use void applyTargetReset(Camera& camera) instead" /*Warning message for void resetTargets(Camera& camera)*/
#define UCMR005 "function has been deprecated and will be removed in future, Use void applyCameraCollision(bool collide) instead" /*Warning message for void setCameraCollision(bool collide)*/
#define UCMR006 "function has been deprecated and will be removed in future, Use void applyCamRadar(bool mode, ControllableObject& object,position3D offset) instead" /*Warning message for void setCamRadar(bool mode, ControllableObject& object,position3D offset)*/
#define UCMR007 "function has been deprecated and will be removed in future, Use void applyCinemaBorder(bool show) instead" /*Warning message for void showCinemaBorder(bool show)*/

/*! MarkerUtilities */
#define UMKR001 "function has been deprecated and will be removed in future, Use void applyMarkerAutoSize(Marker& marker, bool condition) instead" /*Warning message for void setMarkerAutoSize(Marker& marker, bool condition)*/
#define UMKR002 "function has been deprecated and will be removed in future, Use void applyLocalMarkerAutoSize(Marker& marker, bool condition) instead" /*Warning message for void setMarkerAutoSizeLocal(Marker& marker, bool condition)*/
#define UMKR003 "function has been deprecated and will be removed in future, Use void applyMarkerCondition(Marker& marker, string condition) instead" /*Warning message for void setMarkerCondition(Marker& marker, string condition)*/
#define UMKR004 "function has been deprecated and will be removed in future, Use void applyObjectAttach(Marker& marker, ControllableObject& co, position3D offset) instead" /*Warning message for void attachToObject(Marker& marker, ControllableObject& co, position3D offset)*/
#define UMKR005 "function has been deprecated and will be removed in future, Use void applyLocalObjectAttach(Marker& marker, ControllableObject& co, position3D offset) instead" /*Warning message for void attachToObjectlocal(Marker& marker, ControllableObject& co, position3D offset)*/

/*! EffectsUtilities */
#define UEFT001 "function has been deprecated and will be removed in future, Use static void applySoundPlay(string soundName); instead" /*Warning message for static void playSound(string soundName);*/
#define UEFT002 "function has been deprecated and will be removed in future, Use static void applyMusicPlay(string musicName) instead" /*Warning message for static void playMusic(string musicName)*/
#define UEFT003 "function has been deprecated and will be removed in future, Use static void applyMusicPlay(string musicName, float time) instead" /*Warning message for static void playMusic(string musicName, float time)*/

/*! GeneralUtilities Class */
#define UGNR001 "function has been deprecated and will be removed in future, Use void applyAccTime(double accTime)" /*Warning message for void setAccTime(double accTime)*/
#define UGNR002 "function has been deprecated and will be removed in future, Use void applyShutDown()" /*Warning message for void setAccTime(double accTime)*/
#define UGNR003 "function has been deprecated and will be removed in future, Use string getApplicationState()" /*Warning message for string applicationState()*/
#define UGNR004 "function has been deprecated and will be removed in future, Use void applyEMFExport(string fileName, bool drawGrid = true, bool drawContours = true, int drawTypes = 256, bool flipVertical = false) " /*Warning message for void exportEMF(string fileName, bool drawGrid = true, bool drawContours = true, int drawTypes = 256, bool flipVertical = false)*/
#define UGNR005 "function has been deprecated and will be removed in future, Use void applyURLOpen(string url)" /*Warning message for void openURL(string url)*/
#define UGNR006 "function has been deprecated and will be removed in future, Use void applyRederingDisable(bool disable)" /*Warning message for void DisableRendering(bool disable)*/
#define UGNR007 "function has been deprecated and will be removed in future, Use double getBenchMark()" /*Warning message for double BenchMark()*/
#define UGNR008 "function has been deprecated and will be removed in future, Use void applytimeSkip(float duration)" /*Warning message for void skipTime(float duration)*/
#define UGNR009 "function has been deprecated and will be removed in future, Use void applyMapForceDisplay(bool show)" /*Warning message for void forceMap(bool show)*/
#define UGNR010 "function has been deprecated and will be removed in future, Use bool isRequiredVersion(string version)" /*Warning message for bool requiredVersion(string version)*/
#define UGNR011 "function has been deprecated and will be removed in future, Use void applyRadioEnable(bool state)" /*Warning message for void EnableRadio(bool state)*/
#define UGNR012 "function has been deprecated and will be removed in future, Use void applyTeamSwitchEnable(bool enable)" /*Warning message for void EnableTeamSwitch(bool enable)*/
#define UGNR013 "function has been deprecated and will be removed in future, Use void applyGPSEnable(bool enable)" /*Warning message for void EnableGPS(bool enable)*/
#define UGNR014 "function has been deprecated and will be removed in future, Use void applyShadowDistance(double settingValue)" /*Warning message for  void setShadowDistance(double settingValue)*/
#define UGNR015 "function has been deprecated and will be removed in future, Use void applyObjectDetail(int settingValue)" /*Warning message for void setObjectDetail(int settingValue)*/
#define UGNR016 "function has been deprecated and will be removed in future, Use void applyDistanceView(double settingValue)" /*Warning message for void setViewDistance(double settingValue)*/
#define UGNR017 "function has been deprecated and will be removed in future, Use void applyShadowsCamHeightCoef(double settingValue)" /*Warning message for  void setShadowsCamHeightCoef(double settingValue)*/
#define UGNR018 "function has been deprecated and will be removed in future, Use void applyScreenResolution(int width, int height);" /*Warning message for void setScreenResolution(int width, int height)*/
#define UGNR019 "function has been deprecated and will be removed in future, Use void applyMultiChannelConfig(list<CONFIG_VALUES_MULTICHANNEL> param)" /*Warning message for void setMultiChannelConfig(list<CONFIG_VALUES_MULTICHANNEL> param)*/

/*! EnvironmentStateUtilities */
#define UENV001 "function has been deprecated and will be removed in future, Use void applyRandomWeather(bool mode) instead" /*Warning message for void setRandomWeather(bool mode)*/
#define UENV002 "function has been deprecated and will be removed in future, Use void applyFogEffect(double time,double fog, Color_RGB col, double skyBox) instead" /*Warning message for void setFogEffect(double time,double fog, Color_RGB col, double skyBox)*/

/*! CollisionUtilities */
#define UCOL001 "function has been deprecated and will be removed in future, Use list<CollisionDetectionReturnValue> getCollision(position3D startPos, position3D endPos, double radius, ControllableObject& ignoreObject, COLLISIONTESTTYPE collisionType=FIRE) instead" /*Warning message for static list<CollisionDetectionReturnValue> CollisionDetection(position3D startPos, position3D endPos, double radius, ControllableObject& ignoreObject, COLLISIONTESTTYPE collisionType=FIRE);void playSound(string soundName)*/

/*! WorldUtilities */
#define UWLD001 "function has been deprecated and will be removed in future, Use void applyAmbientLifeInitialization() instead" /*Warning message for void applyAmbientLifeInitialization()*/
#define UWLD002 "function has been deprecated and will be removed in future, Use void applyGeometricDrawing(int lowLX, int lowLY, int upperRX, int upperRY) instead" /*Warning message for void drawGeometry(int lowLX, int lowLY, int upperRX, int upperRY)*/
#define UWLD003 "function has been deprecated and will be removed in future, Use position2D applyWorldToScreen(position3D pos) instead" /*Warning message for position2D worldToScreen(position3D pos)*/
#define UWLD004 "function has been deprecated and will be removed in future, Use position3D applyScreenToWorld(position2D pos) instead" /*Warning message for position3D ScreenToworld(position2D pos)*/

/*! PoseControlUtilities Class */
#define UPCN001 "function has been deprecated and will be removed in future, Use void applyExternalPose(Unit& unit, bool status) instead" /*Warning message for void setExternalPose(Unit& unit, bool status)*/
#define UPCN002 "function has been deprecated and will be removed in future, Use void applyExternalPoseSkeleton(Unit& unit, bool status) instead" /*Warning message for void setExternalPoseSkeleton(Unit& unit, bool status)*/
#define UPCN003 "function has been deprecated and will be removed in future, Use void applyExternalPoseUpBody(Unit& unit ,bool status) instead" /*Warning message for void setExternalPoseUpBody(Unit& unit ,bool status)*/

/*! InputUtilities Class */
#define UINP001 "function has been deprecated and will be removed in future, Use int applyKeyBinding(string keyAction, string command, bool suppress) instead" /*Warning message for int bindKey(string keyAction, string command, bool suppress)*/
#define UINP002 "function has been deprecated and will be removed in future, Use int applyKeyBinding(int keyCode, string command, bool suppress) instead" /*Warning message for int bindKey(int keyCode, string command, bool suppress)*/
#define UINP003 "function has been deprecated and will be removed in future, Use int applyKeyBinding(string keyAction, string command, bool suppress, bool enableFloat, KEY_BIND_RULE rule, string vehicleType = "") instead" /*Warning message for int bindKey(string keyAction, string command, bool suppress, bool enableFloat, KEY_BIND_RULE rule, string vehicleType = "")*/
#define UINP004 "function has been deprecated and will be removed in future, Use int applyKeyBinding(int keyCode, string command, bool suppress, bool enableFloat, KEY_BIND_RULE rule, string vehicleType = "") instead" /*Warning message for int bindKey(int keyCodes, string command, bool suppress, bool enableFloat, KEY_BIND_RULE rule, string vehicleType = "")*/
#define UINP005 "function has been deprecated and will be removed in future, Use void applyKeyUnbinding(int keyIndex) instead" /*Warning message for Use void unBindKey(int keyIndex)*/
#define UINP006 "function has been deprecated and will be removed in future, Use void applyUserInputRecord(string fileName) instead" /*Warning message for Use void recordUserInput(string fileName)*/
#define UINP007 "function has been deprecated and will be removed in future, Use void applyUserInputStreamStop() instead" /*Warning message for Use void stopUserInputStreaming()*/
#define UINP008 "function has been deprecated and will be removed in future, Use void applyUserInputDisable(bool state) instead" /*Warning message for Use void disableUserInput(bool state)*/
#define UINP009 "function has been deprecated and will be removed in future, Use float getInputActionState(string action) instead" /*Warning message for float inputAction(string action)*/
#define UINP010 "function has been deprecated and will be removed in future, Use void applyUserInputReplay(string fileName) instead" /*Warning message for void replayUserInput(string fileName)*/

/*! ExplosiveUtilities Class */
#define UEXP001 "function has been deprecated and will be removed in future, Use void applyDetonation(ControllableObject& co) instead" /*Warning message for void detonate(ControllableObject& co)*/

/*! ObjectVectorUtilities Class */
#define UOBV001 "function has been deprecated and will be removed in future, Use void applyVectorUp(ControllableObject& co, vector3D vec) instead" /*Warning message for void setVectorUp(ControllableObject& co, vector3D vec)*/
#define UOBV002 "function has been deprecated and will be removed in future, Use void applyVectorDir(ControllableObject& co, vector3D vec) instead" /*Warning message for  void setVectorDir(ControllableObject& co, vector3D vec)*/
#define UOBV003 "function has been deprecated and will be removed in future, Use void applyPitchAndBank(ControllableObject& co, double pitch_angle, double bank_angle) instead" /*Warning message for void setPitchAndBank(ControllableObject& co, double pitch_angle, double bank_angle)*/
#define UOBV004 "function has been deprecated and will be removed in future, Use void applyDirection(ControllableObject& co, double direction) instead" /*Warning message for void void setDirection(ControllableObject& co, double direction)*/

/*! MapUtilities Class */
#define UMAP001 "function has been deprecated and will be removed in future, Use MapObject applyMapObjectRestore(string id); instead" /*Warning message for MapObject restoreMapObject(string id)*/
#define UMAP002 "function has been deprecated and will be removed in future, Use void applyStaticToDynamic(MapObject& obj) instead" /*Warning message for void staticToDynamic(MapObject& obj)*/

/*! MissionUtilities Class */
#define UMIS001 "function has been deprecated and will be removed in future, Use void applyRoadSideDrive(ROADSIDE side) instead" /*Warning message for void setDriveRoadSide(ROADSIDE side)*/
#define UMIS002 "function has been deprecated and will be removed in future, Use void applySimulationPause() instead" /*Warning message for pauseSimulation()*/
#define UMIS003 "function has been deprecated and will be removed in future, Use void applySimulatinResume() instead" /*Warning message for resumeSimulation()*/
#define UMIS004 "function has been deprecated and will be removed in future, Use void applyMissionEnd(string& endtype) instead" /*Warning message for endMission(string endtype)*/
#define UMIS005 "function has been deprecated and will be removed in future, Use void applyMissionPlay(string& campaign, string& missname, bool skipBriefing)) instead" /*Warning message for playMission(string campaign, string missname, bool skipBriefing)*/
#define UMIS006 "function has been deprecated and will be removed in future, Use bool applyCaptureStart(string& filename, double captureWidth, double captureHeight, int frameRate, list<string> codecList, bool recordSound, bool recordWithUI, double bufferSize) instead" /*Warning message for captureStart(string filename, double captureWidth, double captureHeight, int frameRate, list<string> codecList, bool recordSound, bool recordWithUI, double bufferSize)*/
#define UMIS007 "function has been deprecated and will be removed in future, Use bool applyCaptureStart(string& filename) instead" /*Warning message for captureStart(string filename)*/
#define UMIS008 "function has been deprecated and will be removed in future, Use bool isCaptureTest() instead" /*Warning message for captureTest()*/
#define UMIS009 "function has been deprecated and will be removed in future, Use void applyCaptureStop() instead" /*Warning message for void captureStop()*/
#define UMIS010 "function has been deprecated and will be removed in future, Use vector<int> getMissionStartTime() instead" /*Warning message for MissionStartTime()*/
#define UMIS011 "function has been deprecated and will be removed in future, Use void applyMissionHost(string& missname) instead" /*Warning message for void hostMission(string missname)*/
#define UMIS012 "function has been deprecated and will be removed in future, Use void applyActivateKey(string& keyName) instead" /*Warning message for void ActivateKey(string keyName) */
#define UMIS013 "function has been deprecated and will be removed in future, Use void applyMapShowing(bool show)" /*Warning message for void showMap(bool show)*/
#define UMIS016 "function has been deprecated and will be removed in future, Use void applyMissionVersion(double versionNum)" /*Warning message for void setMissionVersion(double versionNum)*/
#define UMIS017 "function has been deprecated and will be removed in future, Use void applyRadioClear()" /*Warning message for void clearRadio()*/
#define UMIS018 "function has been deprecated and will be removed in future, Use void applyObjectiveStatus(string& objectiveNumber, string& status)" /*Warning message for void setObjectiveStatus(string& objectiveNumber, string& status)*/
#define UMIS019 "function has been deprecated and will be removed in future, Use void applyRadioEnable(bool state)" /*Warning message for void enableRadio(bool state)*/
#define UMIS020 "function has been deprecated and will be removed in future, Use int getEnemyCount(Unit unit, list<Unit>& unitList)" /*Warning message for int countEnemy(Unit unit, list<Unit> unitList)*/
#define UMIS024 "function has been deprecated and will be removed in future, Use void applySoundVolume(double time, double volume)" /*Warning message for void setSoundVolume(double time, double volume)*/
#define UMIS025 "function has been deprecated and will be removed in future, Use void applyEventTextAdd(string text)" /*Warning message for void addEventTextInMPRecord(string text)*/
#define UMIS027 "function has been deprecated and will be removed in future, Use void applyHeaderTextAdd(string header)" /*Warning message for void addHeaderInMPRecord(string header)*/
#define UMIS034 "function has been deprecated and will be removed in future, Use void applyWatchShowing(bool show)" /*Warning message for void showWatch(bool show)*/
#define UMIS035 "function has been deprecated and will be removed in future, Use void applyCompassShowing(bool show)" /*Warning message for void showCompass(bool show)*/

/*! AARUtilities Class */
#define URTE001 "function has been deprecated and will be removed in future, Use position3D getPositionScreenToWorld(position2D screenPos) instead" /*Warning message for position3D positionScreenToWorld(position2D screenPos)*/
#define URTE002 "function has been deprecated and will be removed in future, Use position2D RTEUtilities::getPositionWorldToScreen(position3D worldPos) instead" /*Warning message for position2D positionWorldToScreen(position3D worldPos)*/
#endif //VBS2FUSION_DEPRECATION_H