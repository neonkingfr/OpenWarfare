 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************
Name:		VBS2FusionCallBackDefinitions
Purpose:	This file contains VBS2Fusion Global ENUM definitions which are used in callbacks


Version Information:

Version		Date		Author and Comments
===========================================
1.00		26-03-2012	RDJ: Original Implementation

/************************************************************************/
#ifndef VBS2FUSIONCALLBACKDEFINITIONS_H
#define VBS2FUSIONCALLBACKDEFINITIONS_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <string>

/**********************************************************************/
/* END INCLUDES
/**********************************************************************/
using namespace std;

namespace VBS2Fusion
{
	enum mouseEventType{ MOUSEMOVE, LEFTBUTTONDOWN, LEFTBUTTONUP, RIGHTBUTTONDOWN, RIGHTBUTTONUP, MOUSEWHEEL };
	struct mouseParameters
	{
		mouseEventType _mouseEvent;
		int _xPos;
		int _yPos;
	};


	//Struct Definitions for OnSetFrustum
	// camera frustum parameters
	struct FrustumSpec
	{
	  // camera position
	  float _pointPosX;
	  float _pointPosY;
	  float _pointPosZ;
	
	  // camera speed, used for OpenAL sounds
	  float _pointSpeedX;
	  float _pointSpeedY;
	  float _pointSpeedZ;
	
	  // camera view direction
	  float _viewDirX;
	  float _viewDirY;
	  float _viewDirZ;
	
	  // camera up-view
	  float _viewUpX;
	  float _viewUpY;
	  float _viewUpZ;
	
	  // camera projection angle tangents
	  // Beware: for now, top/bottom and left/right pairs are used as single (symmetric) values
	  float _projTanTop;
	  float _projTanBottom;
	  float _projTanLeft;
	  float _projTanRight;
	
	  // near, far distance clipping
	  float _clipDistNear;
	  float _clipDistFar;
	  float _clipDistFarShadow;
	  float _clipDistFarSecShadow;
	  float _clipDistFarFog;
	
	  // standard constructor
	  FrustumSpec():
	  _pointPosX(0), _pointPosY(0), _pointPosZ(0), _pointSpeedX(0), _pointSpeedY(0), _pointSpeedZ(0),
	    _viewDirX(0), _viewDirY(0), _viewDirZ(0), _viewUpX(0), _viewUpY(0), _viewUpZ(0),
	    _projTanTop(0), _projTanBottom(0), _projTanLeft(0), _projTanRight(0),
	    _clipDistNear(0), _clipDistFar(0), _clipDistFarShadow(0), _clipDistFarSecShadow(0), _clipDistFarFog(0)
	  {}
	
	  // auxiliary setting functions
	  void SetPos(float posX, float posY, float posZ) {_pointPosX = posX; _pointPosY = posY; _pointPosZ = posZ;}
	  void SetSpeed(float speedX, float speedY, float speedZ) {_pointSpeedX = speedX; _pointSpeedY = speedY; _pointSpeedZ = speedZ;}
	  void SetDir(float dirX, float dirY, float dirZ) {_viewDirX = dirX; _viewDirY = dirY; _viewDirZ = dirZ;}
	  void SetUp(float upX, float upY, float upZ) {_viewUpX = upX; _viewUpY = upY; _viewUpZ = upZ;}
	  void SetProj(float top, float bottom, float left, float right) {_projTanTop = top; _projTanBottom = bottom; _projTanLeft = left; _projTanRight = right;}
	  void SetClip(float nearStd, float farStd, float farShadow, float farSecShadow, float farFog)
	  {
	    _clipDistNear = nearStd; _clipDistFar = farStd; _clipDistFarShadow = farShadow; _clipDistFarSecShadow = farSecShadow; _clipDistFarFog = farFog;
	  }
	
	
	};

	struct GeometryFace
	{
		int nIndices;     // count of indices in 'indices' array (3 or 4)
		int indices[4];   // array of indices to vertex array

		GeometryFace(){nIndices = 0;}
		GeometryFace(const GeometryFace& face){*this = face;}
	};

	struct GeometryVertex
	{
		float x,y,z;
	};

	struct GeometryObjectVerticesList
	{
		int nVertices;
		GeometryVertex* vertices;
	};

	struct GeometryObject
	{
		enum Type
		{
			House = 1,
			Tree = 2,
			Terrain = 4,
			Road = 8
		};
		Type type;                // type of object
		long long realObjectId;   // VBS id of object
		float transform[4][3];    // world transformation of the object
		VBS2Fusion::GeometryVertex* vertices;        // array of object's vertices
		int nVertices;            // number of vertices in 'vertices' array
		VBS2Fusion::GeometryFace* faces;      // array of object's faces
		int nFaces;               // number of faces in 'faces' array
		GeometryObjectVerticesList spawnPoints;
		GeometryObject(){nVertices = 0; nFaces = 0; 
		vertices = NULL;
		faces = NULL;}
		~GeometryObject(){ 
			if (faces != NULL)
			{
				//delete[] faces;
			}
			if (vertices != NULL)
			{
				//delete[] vertices;
			}
		}
	};

	struct VBSParametersEx
	{
		const char *_dirInstall;  // VBS installation directory
		const char *_dirUser;     // user's VBS directory
	};
}



#endif
