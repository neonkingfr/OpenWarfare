/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	MissionUtilities.h

Purpose:

	This file contains the declaration of the MissionUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	2.0			01-04/2009	RMR: Original Implementation
	2.01		10-02/2010	MHA: Comments Checked
	2.02		10-01/2011  CGS: Added pauseSimulation() 
									isPaused()
									resumeSimulation()
									hostMission(string)
									endMission(string)
									playMission(string, string, bool)
    2.03	    04-03/2011  YFP: Added Methods,
									captureStart(string)
									captureStart(string,double, double,int,list<string>,bool,bool,double);
									captureStop()
									captureTest()
	2.04		21-06/2011	SSD: Added Methods;
									bool IsMapShown()
									bool IsWatchShown()
									bool IsCompassShown()
									bool IsWalkieTalkieShown()
									bool IsNotepadShown()
									bool IsGPSShown()
									void loadMissionName(Mission& mission)
									string getMissionName()
									vector<int> MissionStartTime()
	2.05		09-09/2011	NDB: Added Methods,
									bool isViewClient()
									float getWeaponSwayFactor()
									void applyWeaponSwayFactor(float WSFactor)
									vector<GeometryObject> getAllStaticObjects(string evaluation)
									vector<ControllableObject> getAllShots(string evaluation)
									vector<Vehicle> getAllVehicleExclusion(string evaluation)
									SIDE getPlayerSide()
									float getDayTime()
									bool isPlayerCadetMode()
									void SaveGame()
									void ForceEnd()
	2.06		30-09/2011	SSD: Added Methods;
									void ActivateKey(string keyName)
									bool IsKeyActive(string keyName)
	2.07		01-11/2011	CGS: Added Methods:
									string createCenter(SIDE side)
									void deleteCenter(SIDE side)
									int countEnemy(Unit unit, list<Unit> unitList)
									int countFriendly(Unit unit, list<Unit> unitList)
									int countUnknown(Unit unit, list<Unit> unitList)
									int countSide(SIDE side, list<Unit> unitList)


/************************************************************************/

#ifndef VBS2FUSION_MISSION_UTILITIES_H
#define VBS2FUSION_MISSION_UTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>
#include <list>

// SIMCENTRIC INCLUDES
#include "position3D.h"
#include "VBS2Fusion.h"
#include "data/Mission.h"
#if GEOM_OBJECT
#include "data/GeomObject.h"
#endif
#include "data/ControllableObject.h"
#include "util/GroupListUtilities.h"
#include "util/TriggerListUtilities.h"
#include "util/VehicleListUtilities.h"
#include "util/EnvironmentStateUtilities.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBS2Fusion
{	
	class VBS2FUSION_API MissionUtilities
	{
	public:

		/*!
		Loads the mission onto the mission variable. Loads and updates (once) the following:

		- the vehicle list
		- the group list
		- the trigger list
		- the EnvironmentState variable.
		- the mission name
		*/
		static void loadMission(Mission& mission);

		/*!
		Updates the mission. Performs the following updates

		- the vehicle list
		- the group list
		- the trigger list
		- the EnvironmentState variable.
		*/
		static void updateMission(Mission& mission);

		/*!
		Returns the player unit of the mission.
		*/
		static Unit getPlayer();

		/*!
		Returns Network ID of Object which unit is mounted.
		*/
		static NetworkID getMountedObject(Unit& unit);

		/*!
		Set drive road side of the mission. Either right or left.
		Deprecated. Use void applyRoadSideDrive(ROADSIDE side)
		*/
		VBS2FUSION_DPR(UMIS001) static void setDriveRoadSide(ROADSIDE side);

		/*!
		get list of playable units of the mission. 
		*/
		static list<Unit> getPlaybleUnits();

		/*!
		check if the simulation is running 
		*/
		static bool isSimulationEnabled();

		/*!
		check if the simulation is paused
		*/
		static bool isPaused();

		/*!
		pause the game simulation.
		Deprecated. Use void applySimulationPause().
		*/
		VBS2FUSION_DPR(UMIS002)static void pauseSimulation();

		/*!
		Resume the game simulation / opposite to pauseSimulation.
		Deprecated. Use void resumeSimulation()
		*/
		VBS2FUSION_DPR(UMIS003)static void resumeSimulation();

		/*!
		Ends the mission with specific ending.
		Deprecated. Use void applyMissionEnd(string& endtype)
		*/
		VBS2FUSION_DPR(UMIS004)static void endMission(string endtype);

		/*!
		A server is hosted and the mission is launched.
		Deprecated, Use void applyMissionHost(string& missionName)
		*/
		VBS2FUSION_DPR(UMIS011) static void hostMission(string missname);

		/*!
		Mission is launched.
		Deprecated. Use void applyMissionPlay(string& campaign, string& missname, bool skipBriefing)
		*/
		VBS2FUSION_DPR(UMIS005)static void playMission(string campaign, string missname, bool skipBriefing);

		/*!
		Record in-game video. Recording automatically ends when Mission quit.
		If filename doesn't contain a backslash, is taken as a relative path with respect to
		the "$HOME\Documents\VBS2\video" 
		Deprecated. Use bool applyCaptureStart(string& filename).
		*/
		VBS2FUSION_DPR(UMIS007)static bool captureStart(string fileName);


		/*!
		Record in-game video. Recording automatically ends when Mission quit.

		filename		- path to save video.   (backslashes needs to be escaped.)
		captureWidth	- capture video width.  (should be multiple of 4 : default 640)
		captureHeight   - capture video height. (should be multiple of 4 : default 640)
		frameRate		- Frames/Sec			(default :20)
		codercList		- List of codecs set by standard four letter names.
						  (codecs are tried one by one. If none works, Raw data is written. )
		recordSound     - Record sound on/off.
		recordWithUI	- Record video with UI.
		bufferSize		- capturing buffer size. 
		Deprecated. Use bool applyCaptureStart(string& filename, double captureWidth, double captureHeight, int frameRate, list<string> codecList, bool recordSound, bool recordWithUI, double bufferSize)
		*/
		VBS2FUSION_DPR(UMIS006)static bool captureStart(	string filename,          
									double captureWidth, 
									double captureHeight,
									int frameRate,
									list<string> codecList,
									bool recordSound,
									bool recordWithUI,
									double bufferSize
									);

		/*!
		Stop in-game video recording. 
		Deprecated. Use void applyCaptureStop()
		*/
		VBS2FUSION_DPR(UMIS009) static void captureStop();

		/*!
		Check if video recording is running.
		Deprecated. Use bool isCaptureTest().
		*/
		VBS2FUSION_DPR(UMIS008)static bool captureTest();

		/*!
		Returns true if player has map enabled, else false.
		*/
		static bool IsMapShown();

		/*!
		Returns true if player has watch enabled, else false.
		*/
		static bool IsWatchShown();

		/*!
		Returns true if player has compass enabled, else false.
		*/
		static bool IsCompassShown();

		/*!
		Returns true if player has radio enabled, else false.
		*/
		static bool IsWalkieTalkieShown();

		/*!
		Returns true if player has notebook enabled, else false.
		*/
		static bool IsNotepadShown();

		/*!
		Returns true if player has GPS receiver enabled, else false.
		*/
		static bool IsGPSShown();

		/*!
		Loads the mission name to the mission object.
		*/
		static void loadMissionName(Mission& mission);

		/*!
		Returns the current mission name.
		*/
		static string getMissionName();

		/*!
		Return when mission started in format [year, month, day, hour, minute, second]. 
		Works only in multi-player, in single-player all values are equal to zero [0,0,0,0,0,0]
		Deprecated. Use vector<int> getMissionStartTime().
		*/
		VBS2FUSION_DPR(UMIS010)static vector<int> MissionStartTime();

		/*!
		Returns true if the current player is logged in as a view client
		*/
		static bool isViewClient();

		/*!
		Return the current weaponSwayFactor
		*/
		static float getWeaponSwayFactor();

		/*!
		Change default sway which is depending on fatigue. If set to 0 the weapon should be completely steady. 
		*/
		static void applyWeaponSwayFactor(float WSFactor);

#ifdef DEVELOPMENT
#if 0
		/*!
		Return a list of all editor-placed, static objects in the current mission that match the given evaluation. _x is substituted for the actual object in the evaluation.

		Includes the following simulation types:
		house
		church
		vasi 

		May also return some visitor-placed objects (which are part of the map) in versions before 1.51.

		evaluation - condition that an object has to fulfill, to be returned
		*/
		static vector<GeomObject> getAllStaticObjects(string evaluation);
#endif
#endif

		/*!
		Return a list of all vehicles in the current mission, excluding the ones covered by the evaluation.
		_x is substituted for the actual object in the evaluation.

		evaluation - Object which fulfills the specified condition will be excluded
		*/
		static vector<Vehicle> getAllVehicleExclusion(string evaluation);

		/*!
		Returns the player's side. This is valid even when the player controlled person is dead 
		*/
		static SIDE getPlayerSide();

		/*!
		Returns the current ingame time in hours. 
		Time using a 24 hour clock
		*/
		static float getDayTime();

		/*!
		Returns if the player is currently playing in cadet or veteran mode.
		*/
		static bool isPlayerCadetMode();

#ifdef DEVELOPMENT
		/*!
		Autosave game (used for Retry). 
		*/
		static void SaveGame();

		/*!
		Enforces mission termination. Can be used in an "END" trigger to force end conditions in the editor.
		*/
		static void ForceEnd();

		/*!
		Forces the mission to end with the specified ending. 
		endType - The enum has ET_CONTINUE, ET_KILLED, ET_LOST, ET_END1, ET_END2, ET_END3, ET_END4, ET_END5, ET_END6
		*/
		static void ForceEnding(ENDTYPE endType);
#endif

		/*!
		Activates the given Key Name for the current user profile.
		Keys can be used to indicate completed missions or to check whether 
		required missions have been completed. 
		Deprecated. Use void applyActivateKey(string& keyName)	
		*/
		VBS2FUSION_DPR(UMIS012) static void ActivateKey(string keyName);

		/*!
		Checks whether the given Key is active in the current user profile.
		*/
		static bool IsKeyActive(string keyName);


		/*!
		Creates a new AI HQ for the given side.
		*/
		static string createCenter(SIDE side);

		/*!
		Destroys the AI center of the given side.
		*/
		static void deleteCenter(SIDE side);



		/*!
		Count how many units in the list are considered enemy to the given unit.
		Deprecated, Use int getEnemyCount(Unit unit, list<Unit>& unitList).
		*/
		VBS2FUSION_DPR(UMIS020)static int countEnemy(Unit unit, list<Unit> unitList);

		/*!
		Count how many units in the list are considered friendly to the given unit.
		*/
		static int countFriendly(Unit unit, list<Unit> unitList);

		/*!
		Count how many units in the list are unknown to the given unit.
		*/
		static int countUnknown(Unit unit, list<Unit> unitList);

		/*!
		Count how many units in the list belong to given side.
		*/
		static int countSide(SIDE side, list<Unit> unitList);


#ifdef DEVELOPMENT

		/*!
		This statement is launched whenever a player is connected to a MP session.
		*/
		static void onPlayerConnected(string statement);
#endif

		/*!
		This statement is launched whenever a player is disconnected from a MP session.
		*/
		static void onPlayerDisconnected(string statement);

		/*!
		Defines an action performed just after a vehicle or object is created.
		Variable command should be a valid VBS2 script command.
		*/
		static void onVehicleCreated(string command);

#ifdef DEVELOPMENT
		/*!
		Return count of players playing on given side. Works only in multiplayer,
		in singleplayer always returns 0. SIDE variable should be one of 
		(WEST, EAST, CIVILIAN, RESISTANCE)
		*/
		static int getPlayersNumber(SIDE side);

#endif

		/*!
		Process statements stored using setVehicleInit. The statements will 
		only be executed once even if processInitCommands is called multiple 
		times
		*/
		static void processInitCommands();

		/*!
		Launch init.sqs or init.sqf scripts.
		*/
		static void runInitScript();

		/*!
		Enable Map (default true) 
		void GeneralUtilities::forceMap(bool show) should be call to see the effect of the function.
		Deprecated. Use void applyMapShowing(bool show)
		*/
		VBS2FUSION_DPR(UMIS013) static void showMap(bool show);

		/*!
		Shows or hides the watch on the map screen, if enabled for the mission and you possess the item. (default true) 
		(Only works for animated maps in V1.40.) 
		void GeneralUtilities::forceMap(bool show) should be call to see the effect of the function.
		Deprecated. Use void applyWatchShowing(bool show)
		*/
		VBS2FUSION_DPR(UMIS034) static void showWatch(bool show);

		/*!
		Shows or hides the compass on the map screen, if enabled for the mission and you possess the item. (default true). (Only works for animated maps in V1.40.) 
		void GeneralUtilities::forceMap(bool show) should be call to see the effect of the function.
		Deprecated. Use void applyCompassShowing(bool show)
		*/
		VBS2FUSION_DPR(UMIS035) static void showCompass(bool show);

		/*!
		Shows or hides the radio on the map screen, if enabled for the mission and you possess the item. (default true). (Only works for animated maps in V1.40.) 
		void GeneralUtilities::forceMap(bool show) should be call to see the effect of the function.
		*/
		static void showRadio(bool show);

		/*!
		Shows or hides the notebook on the map screen, if enabled for the mission. (default true). (Only works for animated maps in V1.40.) 
		void GeneralUtilities::forceMap(bool show) should be call to see the effect of the function.
		*/
		static void showPad(bool show);

#ifdef BANU_DEV_LOCAL
		/*!
		Enable ID card (default false). Obsolete command. 
		void GeneralUtilities::forceMap(bool show) should be call to see the effect of the function.
		*/
		static void showWarrant(bool show);

		/*!
		Shows or hides the GPS receiver on the map screen, if enabled for the mission and you possess the item. (default false). (Doesn't work in V1.40.)
		void GeneralUtilities::forceMap(bool show) should be call to see the effect of the function.
		*/
		static void showGPS(bool show);
#endif

		/*!
		Set HDR eye accommodation.
		*/

		static void setEyeAccom(double eyeAccValue);

		/*!
		Impulse creation, for a wave simulation, is done via this command. It sets an oscillating point. The oscillations behaves like waves, 
		they look so, and they influence objects therein. Meaining of the parameters mentioned below:
			wPos - Impulse 2D coordinates (m)
			wAmp - Amplitude of impulse (m)
			wOscTime - Oscillation time of impulse (s)
			halfDumpTime - Amplitude decreasing time to half value (s)
			lifeTime - Impulse life time (s)
			radius - Radius of impulse influence (m)
			index - Wave index (modulo 100)
		*/
		static bool setWaterImpulse(position2D& wPos, double wAmp, double wOscTime, double halfDumpTime, double lifeTime, double radius, int index);

		/*!
		Adds text into the events section of the mpreport.txt file (in installation folder). 
		Only works in MP sessions, and is only visible after session has been finished. 
		Deprecated. Use void applyEventTextAdd(string text)
		*/
		VBS2FUSION_DPR(UMIS025) static void addEventTextInMPRecord(string text);

		/*!
		Adds text into the header section of the mpreport.txt file
		Deprecated. Use void applyHeaderTextAdd(string header)
		*/
		VBS2FUSION_DPR(UMIS027) static void addHeaderInMPRecord(string header);

		/*!
		Adds text into the footer section of the mpreport.txt file.
		*/
		static void addFooterInMPRecord(string footer);

		/*!
		Clean up the content of radio protocol history.
		Deprecated. Use void applyRadioClear()
		*/
		VBS2FUSION_DPR(UMIS017) static void clearRadio();

		/*!
		Enable and disable radio messages to be heard and shown in the left lower corner of the screen.  
		True to enable the radio, false to disable it
		Deprecated. Use void applyRadioEnable(bool state)
		*/
		VBS2FUSION_DPR(UMIS019) static void enableRadio(bool state);

		/*!
		Changes the music volume smoothly within the given time. Here time in seconds.  Music volume range 0 to 1.
		*/
		static void setMusicVolume(double time, double volume);

		/*!
		Gives the current Music volume.
		*/
		static double getMusicVolume();

		/*!
		Changes the sound volume smoothly within the given time. Here time in seconds.  Sound volume range 0 to 1.
		Deprecated. Use void applySoundVolume(double time, double volume)
		*/
		VBS2FUSION_DPR(UMIS024) static void setSoundVolume(double time, double volume);

		/*!
		Gives the current Sound volume.
		*/
		static double getSoundVolume();

		/*!
		Changes the Radio volume smoothly within the given time. Here time in seconds.  Radio volume range 0 to 1.
		*/
		static void setRadioVolume(double time, double volume);

		/*!
		Gives the current Radio volume.
		*/
		static double getRadioVolume();

		/*!
		Sets the status of an objective that was defined in briefing.html.
		Status may be one of:
			- "ACTIVE"
			- "FAILED"
			- "DONE"
			- "HIDDEN" 

		To refer to an objective that is named "OBJ_1", use the index number "1". 
		Deprecated. Use void applyObjectiveStatus(string& objectiveNumber, string& status)
		*/
		VBS2FUSION_DPR(UMIS018) static void setObjectiveStatus(string& objectiveNumber, string& status);

		/*!
		Sets how friendly side1 is with side2. For a value smaller than 0.6 it results in being enemy,
		otherwise it's friendly.

		Note : This friendly behavior will not be applied for units which were created after called
		this function. If want to apply it to those units, have to call the function again.
		*/
		static void setFriend(SIDE side1, SIDE side2 , double value);

		/*!
		Get the names of playable units
		*/
		static vector<string> getPlaybleUnitNames();

		/*!
		Sets version of currently loaded mission
		Deprecated. Use void applyMissionVersion(double versionNum)
		*/
		VBS2FUSION_DPR(UMIS016) static void setMissionVersion(double versionNum);

		/*!
		Returns version of currently loaded mission
		*/
		static int getMissionVersion();

		/*!
		Return a list of all shots in the current mission that match the given evaluation. 
		_x is substituted for the actual object in the evaluation.
		*/
		static vector<ControllableObject> getAllShots(string evaluation);

		/*
		Return a list of all editor-placed, static objects in the current mission that match the given evaluation. 
		
		@param evaluation - The condition that an object has to fulfill, to be returned.

		@return Vector of ControllableObjects. Includes the following simulation types:
			- house
			- church
			- vasi 
		May also return some visitor-placed objects (which are part of the map). 
		*/
		static vector<ControllableObject> getAllStaticObjects(string evaluation);

		/*!
		@description 
		
		Forces the mission to end with the specified ending.

		@locality 

		Locally Applied Locally Effected

		@version [VBS2Fusion v2.70.2]
	
		@param endtype - Type of end as integer, values can be;
			0: Continue
			1: Killed
			2: Lost
			3: End1
			4: End2
			5: End3
			6: End4
			7: End5
			8: End6

		@return Nothing.
		
		@example

		@code

		MissionUtilities::applyForcedEnding(3);

		@endcode

		@relates to

		@overloaded

		@remark Ending modes Continue(0) and Killed(1) are ignored in multi player missions.
		*/
		static void applyForcedEnding(int endtype);

		/*!
		@description
		
		Applies drive road side of the mission.

		@locality

		Locally applied, Globally affected

		@version [VBS2Fusion v2.71]

		@param side - Drive road side. It can be right, left or middle.

		@return Nothing.

		@example

		@code

		MissionUtilities::applyRoadSideDrive(ROAD_RIGHT);

		@overloaded 

		@related

		@remarks This is a replication of void setDriveRoadSide(ROADSIDE side) function.
		*/
		static void applyRoadSideDrive(ROADSIDE side);

		/*!
		@description

		Pause the game simulation.

		@locality

		Locally applied, Globally effected

		@version [VBS2Fusion v2.71]

		@param None.

		@return Nothing.

		@example

		@code

		MissionUtilities::applySimulationPause();

		@overloaded 

		@related

		MissionUtilities::applySimulatinResume()

		@remarks This is a replication of void pauseSimulation() function.
		*/
		static void applySimulationPause();

		/*!
		@description

		A server is hosted and the mission is launched.

		@locality

		@version [VBS2Fusion v2.71]

		@param missionName - Name of the mission that need to be run.

		@return None.

		@example

		@overloaded 

		@related.

		@remarks This is a replication of void hostMission(string missname).
		*/
		static void applyMissionHost(string& missionName);

		/*!
		@description

		Activates the given keyname for the current user profile.
		The keys can be used to indicate completed missions or 
		to check whether required missions have been completed.

		@locality

		@version [VBS2Fusion v2.71]

		@param keyName - Name of the key.

		@return None.

		@example

		@overloaded 

		@related.

		@remarks This is a replication of void ActivateKey(string keyName).
		*/
		static void applyActivateKey(string& keyName);

		/*!
		@description

		Resume the game simulation / opposite to pauseSimulation.

		@locality

		Locally applied, Globally effected

		@version [VBS2Fusion v2.71]

		@param None.

		@return Nothing.

		@example

		@code

		MissionUtilities::applySimulatinResume();

		@overloaded 

		@related

		MissionUtilities::applySimulationPause()

		@remarks This is a replication of void resumeSimulation() function.
		*/
		static void applySimulatinResume();

		/*!
		@description

		Ends the mission with specific ending.

		@locality

		Locally applied, Locally effected

		@version [VBS2Fusion v2.71]

		@param None.

		@return endtype - End type can be "Continue" (mission continues normally), "Killed" (no debriefing is shown, only the "Retry" or "End" buttons)  
		"End1" - "End6", or "Loser" (appropriate debriefing section is shown)

		@example

		@code

		MissionUtilities::applyMissionEnd(string ("END1"));

		@overloaded 

		@related

		@remarks This is a replication of void endMission(string endtype) function.
		*/
		static void applyMissionEnd(string& endtype);

		/*!
		@description

		Mission is launched.

		@locality

		Locally applied, Locally effected

		@version [VBS2Fusion v2.71]

		@param campaign - Name of the campaign and it is always empty.

		@param missname - Mission name, including map extension.

		@param skipBriefing - Whether to skip the briefing and intro.

		@return Nothing.

		@example

		@code

		MissionUtilities::applyMissionPlay(string(""), string("T01_Basic_Movement.Intro"),true);

		@overloaded 

		@related

		@remarks The mission must exist, as a PBO file, in the mission folder.

		@remarks This is a replication of void playMission(string campaign, string missname, bool skipBriefing) function.
		*/
		static void applyMissionPlay(string& campaign, string& missname, bool skipBriefing);

		/*!
		@description

		Record in-game video. Recording automatically ends when Mission quit.

		@locality

		@version [VBS2Fusion v2.71]

		@param filename - Path to save video in (backslashes need to be escaped: e.g. "O:\\fp\\vbs-video.avi"). If filename doesn't contain a backslash, is taken as a relative path with respect to the "$HOME\Documents\VBS2\video". If no extension is given, AVI is used. 

		@param captureWidth	- Capture video width. (has to be a multiple of 4) 
		
		@param captureHeight - Captured video height. (has to be a multiple of 4)

		@param frameRate - Number of frames per second.

		@param codercList - List of codecs names by their standard four letter names. codecs are tried one by one. If none works, raw data is written. 

		@param recordSound - Record sound on/off.

		@param recordWithUI	- Record video with UI shown/hidden.

		@param bufferSize - Buffer size in MB. Can be up-to 100MB. It is not necessary to use such a large amount unless raw data is saved.

		@return bool - True if recoding starts successfully otherwise returns false if it already is recording 

		@example

		@overloaded 

		@related

		@remarks This is a replication of bool captureStart(string filename, double captureWidth, double captureHeight, int frameRate, list<string> codecList, bool recordSound, bool recordWithUI, double bufferSize) function.
		*/
		static bool applyCaptureStart(string& filename,
						double captureWidth,
						double captureHeight,
						int frameRate,
						list<string> codecList,
						bool recordSound, 
						bool recordWithUI,
						double bufferSize);

		/*!
		@description

		Record in-game video. Recording automatically ends when Mission quit.

		@locality

		@version [VBS2Fusion v2.71]

		@param filename - Path to save video in (backslashes need to be escaped: e.g. "O:\\fp\\vbs-video.avi"). If filename doesn't contain a backslash, is taken as a relative path with respect to the "$HOME\Documents\VBS2\video". If no extension is given, AVI is used. 

		@return bool - True if recoding starts successfully otherwise returns false if it already is recording 

		@example

		@overloaded 

		MissionUtilities::applyCaptureStart(string& filename, double captureWidth, double captureHeight, int frameRate, list<string> codecList, bool recordSound, bool recordWithUI, double bufferSize)

		@related

		@remarks This is a replication of bool captureStart(string filename) function.
		*/
		static bool applyCaptureStart(string& filename);

		/*!
		@description

		Check if video recording is running.

		@locality

		Locally applied, Locally effected

		@version [VBS2Fusion v2.71]

		@param None.

		@return bool - Return true if video recording is running otherwise returns false.

		@example

		@code

		bool testStatus=MissionUtilities::isCaptureTest();

		@overloaded 

		@related

		@remarks This is a replication of bool captureTest() function.
		*/
		static bool isCaptureTest();

		/*!
		@description

		Return when mission started.

		@locality

		@version [VBS2Fusion v2.71]

		@param None.

		@return vector<int> - Vector contains year, month, day, hour, minute, second respectively.

		@example

		@overloaded 

		@related

		@remarks Works only in multiplayer, in singleplayer all values are equal to zero.

		@remarks This is a replication of vector<int> MissionStartTime() function.
		*/
		static vector<int> getMissionStartTime();

		/*!
		@description
		Count how many units in the list are considered enemy to the given unit.

		@locality

		@version [VBS2Fusion v2.71.1]

		@param unit - Unit that need to get the enemy count.
		@param unitList - List of units that compare with the given unit.

		@return int - Return the number of enemies around the given unit.

		@example

		@overloaded 

		@related.

		@remarks This is a replication of int countEnemy(Unit unit, list<Unit> unitList).
		*/
		static int getEnemyCount(Unit unit, list<Unit>& unitList);

		/*!
		@description
		Enable or disable 2D map view in RTE.

		@locality

		@version [VBS2Fusion v2.71.1]

		@param show - show/hide map

		@return None.

		@example

		@overloaded 

		@related.

		@remarks This is a replication of void showMap(bool show).
		*/
		static void applyMapShowing(bool show);

		/*!
		@description
		Shows or hides the watch on the map screen, if enabled for the mission and you possess the item.

		@locality

		@version [VBS2Fusion v2.71.1]

		@param show - show/hide watch

		@return None.

		@example

		@overloaded 

		@related.

		@remarks This is a replication of void showMap(bool show).
		*/
		static void applyWatchShowing(bool show);

		/*!
		@description
		hows or hides the compass on the map screen, if enabled for the mission and you possess the item.

		@locality

		@version [VBS2Fusion v2.71.1]

		@param show - show/hide compass

		@return None.

		@example

		@overloaded 

		@related.

		@remarks This is a replication of void showCompass(bool show).
		*/
		static void applyCompassShowing(bool show);

		/*!
		@description
		Sets version of currently loaded mission.

		@locality

		@version [VBS2Fusion v2.71.1]

		@param show - version number

		@return None.

		@example

		@overloaded 

		@related.

		@remarks This is a replication of void setMissionVersion(double versionNum).
		*/
		static void applyMissionVersion(double versionNum);

		/*!
		@description
		Stops in-game video recording.

		@locality

		@version [VBS2Fusion v2.71.1]

		@param

		@return None.

		@example

		@overloaded 

		@related.

		@remarks This is a replication of void captureStop().
		*/
		static void applyCaptureStop();

		/*!
		@description
		Clean up the content of radio protocol history.

		@locality

		@version [VBS2Fusion v2.71.1]

		@param

		@return None.

		@example

		@overloaded 

		@related.

		@remarks This is a replication of void clearRadio().
		*/
		static void applyRadioClear();

		/*!
		@description
		Sets the status of an objective that was defined in briefing.html.

		@locality

		@version [VBS2Fusion v2.71.1]

		@param objectiveNumber - To refer to an objective that is named "OBJ_1", for example, use only the index number in this command (i.e. "1" ).

		@param status - One of: - "ACTIVE", "FAILED", "DONE", "HIDDEN".

		@return None.

		@example

		@overloaded 

		@related.

		@remarks This is a replication of void setObjectiveStatus().
		*/
		static void applyObjectiveStatus(string& objectiveNumber, string& status);

		/*!
		@description
		Enable and disable radio messages to be heard and shown in the left lower corner of the screen. This command can be helpful during cut-scenes.

		@locality

		@version [VBS2Fusion v2.71.1]

		@param state - true to enable the radio, false to disable it.

		@return None.

		@example

		@overloaded 

		@related.

		@remarks This is a replication of void enableRadio(bool state).
		*/
		static void applyRadioEnable(bool state);

		/*!
		@description
		Changes the sound volume smoothly within the given time.

		@locality

		@version [VBS2Fusion v2.71.1]

		@param time - the time in seconds.

		@param volume - sound volume range 0 to 1. Maximum volume is 1.

		@return None.

		@example

		@overloaded 

		@related.

		@remarks This is a replication of void setSoundVolume(double time, double volume).
		*/
		static void applySoundVolume(double time, double volume);

		/*!
		@description
		Adds text into the events section of the mpreport.txt file (in installation folder). Only works in MP sessions, and is only visible after session has been finished.

		@locality

		@version [VBS2Fusion v2.71.1]

		@param text - text value.

		@return None.

		@example

		@overloaded 

		@related.

		@remarks This is a replication of void addEventTextInMPRecord(string text).
		*/
		static void applyEventTextAdd(string text);

		/*!
		@description
		Adds text into the header section of the mpreport.txt file.

		@locality

		@version [VBS2Fusion v2.71.1]

		@param header - header text.

		@return None.

		@example

		@overloaded 

		@related.

		@remarks This is a replication of void addHeaderInMPRecord(string header).
		*/
		static void applyHeaderTextAdd(string header);

	};
};

#endif // MISSION_UTILITIES_H