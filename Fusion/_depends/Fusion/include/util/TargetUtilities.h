
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************

Name:

	TargetUtilities.h

Purpose:

	This file contains the declaration of the TargetUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0		 23-08/2011	NDB: Original Implementation	

/************************************************************************/

#ifndef VBS2FUSION_TARGET_UTILITIES_H
#define VBS2FUSION_TARGET_UTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/

// Standard C++ Includes


// Simcentric Includes
#include "data/Target.h"
#include "data/ControllableObject.h"

/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBS2Fusion
{

#ifdef DEVELOPMENT

	class VBS2FUSION_API TargetUtilities
	{
	public:

		//********************************************************
		// create utilities
		//********************************************************
		/*!
		Create a target.
		target - The reference of the target
		object - Object in the object/target
		type - type of the target
		position - position of the target
		typeAccuracy - type Accuracy(eg: 9x10^6)
		posAccuracy - position Accuracy(eg: 0.03)
		*/
		static void CreateTarget(Target& target, ControllableObject& object, string type, position3D position, double typeAccuracy, double posAccuracy);

		/*!
		Change information about a target.
		Returns true if it is changed.
		target - The target which need to be change
		position - position of the target
		typeAccuracy - type Accuracy(eg: 9x10^6)
		posAccuracy - position Accuracy(eg: 0.03)
		*/
		static bool moveTarget(Target& target, position3D position, double typeAccuracy, double posAccuracy);
		
	};

#endif//GEOM_OBJECT
}; 

#endif //VBS2FUSION_TARGET_UTILITIES_H
