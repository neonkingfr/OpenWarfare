/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	VisibilityUtilities.h

Purpose:

	This file contains the declaration of the VisibilityUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			17-09/2009	RMR: Original Implementation
	2.0			10-02/2009  YFP: Version 2 Implementation
	2.01		10-02/2010	MHA: Comments Checked
	
/************************************************************************/

#ifndef VBS2FUSION_VISIBILITY_UTILITIES_H
#define VBS2FUSION_VISIBILITY_UTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>
#include <math.h>
#include <list>

// SIMCENTRIC INCLUDES
#include "position3D.h"
#include "VBS2Fusion.h"
#include "conversions.h"
#include "data/ControllableObject.h"
#include "util/ExecutionUtilities.h"
#include "DataContainers/ObjectList.h"
#include "data/Group.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/

namespace VBS2Fusion
{		

	class VBS2FUSION_API VisibilityUtilities
	{
	public:

		/*!
		Used to determine if two objects are visible to each other by checking visibility between the two objects locations. The above sea level (ASL) position
		of each object (obtained by using object.getPositionASL() is used as the two positions. Basically checks for direct line of site between the two positions and 
		returns true if there are no collisions between the two locations. 

		Optional parameter maxDistance can be used as a cut-off to only perform the visibility check if the two objects are within a certain 
		distance away from each other. Default maxDistance of -1 can be used to check if the two objects are visible to each other regardless 
		of how far they are from each other. 

		parameters:

		- Obejct1 (ControllableObject) - The object to test visibility from

		- Object2 (ControllableObject) - The object to test visibility to

		- maxDistance <optional> (double)- The cut-off value to use when testing visibility. If the distance between the two units is larger than maxDistance, 
		the function will automatically return false. maxDistance of -1 is used to denote that there is no minimum distance for the visibility check. 

		- height <optional> (double) - the height from the ground from which the visibility check is performed. As the visibility check uses the values obtained from 
		Object1.getPositionASL() and Object2.getPositionASL(), the locations tested from are most probably on the ground. Use a typical value like 0.60 to test from mid torso level and 
		a value like 1.8 to test around eye level. 
		*/
		static bool isVisible(ControllableObject& Object1, ControllableObject& Object2, double maxDistance, double height);

		/*!
		Used to determine if two objects are visible to each other by checking visibility between the two objects locations. The above sea level (ASL) position
		of each object (obtained by using object.getPositionASL() is used as the two positions. Basically checks for direct line of site between the two positions and 
		returns true if there are no collisions between the two locations. 

		Distance between two objects is not considered here and uses height as 0 for the visibility checking.  

		parameters:

		- Obejct1 (ControllableObject) - The object to test visibility from

		- Object2 (ControllableObject) - The object to test visibility to
		*/
		static bool isVisible(ControllableObject& Object1, ControllableObject& Object2);

		/*!
		Returns the visibility of one object to another. The visibility is returned as a number between 0 and 1. (from 0=not visible to 1=full visibility)

		Considers only the origin (the center) of the object, so if only a part of the object is visible the command will still return 0.
		*/
		static double getVisibility(ControllableObject& Object1, ControllableObject& Object2);

		/*!
		Returns the visibility of one object to another. The visibility is returned as a number between 0 and 1. (from 0=not visible to 1=full visibility)

		If the _List contains a several objects, this function returns the visibility value of the object with the highest visibility. 

		Considers only the origin (the center) of the object, so if only a part of the object is visible the command will still return 0.
		*/
		static double getVisibility(ControllableObject& Object1, VBS2Fusion::ObjectList<ControllableObject>& _List);


		/*!
		Returns the visibility of the position to the object. The visibility is returned as a number between 0 and 1. (from 0=not visible to 1=full visibility)

		Considers only the origin (the center) of the object, so if only a part of the object is visible the command will still return 0.
		*/

		static double getVisibility(ControllableObject& Object1, position3D& position);


		/*!
		Returns the visibility of one object to another. The visibility is returned as a number between 0 and 1. (from 0=not visible to 1=full visibility)

		If the group contains a several objects, this function returns the visibility value of the object with the highest visibility. 

		Considers only the origin (the center) of the object, so if only a part of the object is visible the command will still return 0.
		*/
		static double getVisibility(ControllableObject& Object1, Group& group);


#ifdef DEVELOPMENT

		/*!
		 Turn on or off geometry setting of a object.  
			visibility - toggles object visibility.
			fire       - toggles object's fire geometry.
			view	   - toggles object's view geometry
			collision  - toggles object's collison geometry
						
		*/
		static void disableGeometySettings(ControllableObject& object, bool visibility,bool fire,bool view,bool collision);
#endif

		/*!
		Returns the visibility of one object to another. Unlike the getVisibility command, this version uses the camera position 
		of the viewer object to determine the visibility from.It only considers the origin (the center) of the object, so if only 
		a part of the object is visible the command will still return 0.
		*/
		static double getVisibilityFromCamPos(ControllableObject& viewerObj, ControllableObject& targetObj);

		/*!
		Returns the visibility of one position to another.
		The visibility is returned as a number between 0 and 1. (from 0=not visible to 1=full visibility)
		*/
		static double getVisibility(position3D& pos1, position3D& pos2);

		
	};
};

#endif //VISIBILITY_UTILITIES_H 