/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	MarkerListUtilities.h

Purpose:

	This file contains the declaration of the MarkerListUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			31-12-2012	RDJ: Original Implementation
	

/************************************************************************/
#ifndef VBS2FUSION_MARKER_LIST_UTILITIES_H
#define VBS2FUSION_MARKER_LIST_UTILITIES_H

#include "VBS2Fusion.h"
#include "data/MarkerList.h"
namespace VBS2Fusion
{
	class VBS2FUSION_API MarkerListUtilities
	{
	public:
		
		/*
		@description

		Load all marker defined on the current mission onto marker List.

		@locality

		Locally Applied.  Locally Effected.

		@version [VBS2Fusion v2.70.2]

		@param MarkerList - MarkerList object.

		@return  Nothing.

		@example

		@code

		//Create Marker List

		MarkerList mL;

		// To Load all marker defined on the current mission

		MarkerListUtilities::loadMarkerList(mL);

		@endcode

		@overloaded 

		None.		

		@related

		@remarks

		*/
		
		static void loadMarkerList(MarkerList& markerList);

		/*!
		@description

		Update all marker defined on the current mission into markerList.

		@locality

		Locally Applied.

		@version [VBS2Fusion v2.70.2]

		@param MarkerList - MarkerList object.

		@return  Nothing.

		@example

		@code

		//Create Marker List

		MarkerList mL;

		// To Load all marker defined on the current mission

		MarkerListUtilities::updateMarkerList(mL);

		@endcode

		@overloaded 

		None.		

		@related

		@remarks

		*/
		static void updateMarkerList(MarkerList& markerList);

		


	};

};
#endif
