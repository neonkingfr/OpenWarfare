
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************

Name:

	RoadNetworkUtilities.h

Purpose:

	This file contains the declaration of the RoadNetworkUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0		 12-06/2011	NDB: Original Implementation	

/************************************************************************/

#ifndef VBS2FUSION_ROADNETWORK_UTILITIES_H
#define VBS2FUSION_ROADNETWORK_UTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/

// Standard C++ Includes


// Simcentric Includes
#include "VBS2FusionDefinitions.h"
#include "../../Private_Include/VBS2FusionConfig.h"
#include "data/RoadNetwork.h"
#include "util/RoadSegmentUtilities.h"

/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBS2Fusion
{

#if GEOM_OBJECT

	class VBS2FUSION_API RoadNetworkUtilities
	{
	public:

		//********************************************************
		// Load utilities
		//********************************************************
#ifdef DEVELOPMENT
		/*!
		load Road network
		It get the road segment within circle of radius 100,000 from the center of the 
		current map in VBS2	and set it to given RoadNetwork reference
		*/
		static void loadRoadNetwork(RoadNetwork& network);	


		/*!
		loads the whole RoadNetwork related with given segment
		network - It is a reference. Function will get the values from VBS2 and load this variable.
		segment - It should be in VBS2.
		*/
		static void loadRoadNetwork(RoadNetwork& network, RoadSegment& segment);
#endif
		
	};

#endif//GEOM_OBJECT
}; 

#endif //VBS2FUSION_ROADNETWORK_UTILITIES_H
