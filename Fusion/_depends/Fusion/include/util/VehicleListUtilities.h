/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	VehicleListUtilities.h

Purpose:

	This file contains the declaration of the VehicleListUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			02-04/2009	RMR: Original Implementation
	2.0			10-02/2010  YFP: Version 2 Implementation
	2.01		10-02/2010	MHA: Comments Checked
    2.012		13-09/2010  YFP: Modified loadVehicleList();

/************************************************************************/

#ifndef VBS2FUSION_VEHICLE_LIST_UTILITIES_H
#define VBS2FUSION_VEHICLE_LIST_UTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>

// SIMCENTRIC INCLUDES
#include "position3D.h"
#include "VBS2Fusion.h"
#include "data/VehicleList.h"
#include "util/VehicleUtilities.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/

namespace VBS2Fusion
{
	class VBS2FUSION_API VehicleListUtilities
	{
	public:	

		/*!
		Loads all vehicles on the current mission onto vehicleList. 

		All the vehicles are updated once with all members and Waypoints loaded. 
		*/

		static void loadVehicleList(VehicleList& vehicleList);


		/*!
		Update and load all the vehicles in the current mission into the vehicleList.
		*/
		static void updateVehicleList(VehicleList& vehicleList);

		/*!
		Compound update for the vehicle list. Updates each vehicle along with its
		member list and waypoint list. 
		*/

		static void updateVehicles(VehicleList& vehicleList);


	};
};

#endif //VEHICLE_LIST_UTILITIES_H