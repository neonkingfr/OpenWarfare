/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	ObjectVectorUtilities.h

Purpose:

	This file contains the declaration of the ObjectVectorUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	2.0			12-01/2010	RMR: Original Implementation
	2.01		10-02/2010	MHA: Comments Checked
	2.02		09-05-2012  TLM: Modified method
									void setPitchAndBank(ControllableObject& co, double pitch_angle, double bank_angle)
	2.03		21-05-2012	TLM: Method Added
									vector<double> getPitchAndBank(ControllableObject& co)

/************************************************************************/

#ifndef VBS2FUSION_OBJECT_VECTOR_UTILITIES_H
#define VBS2FUSION_OBJECT_VECTOR_UTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>
#include <math.h>

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "conversions.h"
#include "data/ControllableObject.h"
#include "data/Group.h"
#include "util/ExecutionUtilities.h"
#include "data/VBS2Variable.h"
#include "position3D.h"
#include "ControllableObjectUtilities.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBS2Fusion
{	
	typedef position3D vector3D;

	class VBS2FUSION_API ObjectVectorUtilities
	{
	public:
		/*!
		Set object's up vector. Direction vector will remain unchanged.
		
		setVectorUp can only influence an object's bank. It can not influence pitch. 
		
		Example:

		player setVectorUp [0,1,0]
		If the player is facing 0 degrees (north), then this will do NOTHING.
		If the player is facing 90 degrees (east), then this will make him bank 90 degrees to his left.
		Deprecated. Use void applyVectorUp(ControllableObject& co, vector3D vec).
		*/

		VBS2FUSION_DPR(UOBV001)static void setVectorUp(ControllableObject& co, vector3D vec);

		/*!
		Return object's up vector in world Position coordinates ( [x, y, z] ).
		*/
		static vector3D getVectorUp(ControllableObject& co);		

		/*!
		Set object's direction vector. Up vector will remain unchanged.

		setVectorDir can only influence an object's pitch. It can not influence bank. 
		
		Example:
		player setVectorDir [0,0,1]
		If the player is facing 0 degrees (north), then this will do NOTHING.
		If the player is facing 90 degrees (east), then this will make him pitch 90 degrees up.
		
		You can't directly pitch an object beyond 90 degrees, because this would change its facing 
		direction. You must first flip it's direction using setDir, then you must bank the object 
		180 degrees, THEN you pitch the object appropriately.
		Deprecated. Use void applyVectorDir(ControllableObject& co, vector3D vec).
		*/
		VBS2FUSION_DPR(UOBV002) static void setVectorDir(ControllableObject& co, vector3D vec);

		/*!
		Return object's direction vector in world Position coordinates ([x, z, y]).

		Example: 

		A unit facing North would return [0,1,0]
		A unit facing East would return [1,0,0]
		A unit facing South would return [0,-1,0]
		A unit facing West would return [-1,0,0]
		*/
		static vector3D getVectorDir(ControllableObject& co);

		/*!
		Sets an objects pitch and bank angles.
		Deprecated. Use void applyPitchAndBank(ControllableObject& co, double pitch_angle, double bank_angle)
		*/
		VBS2FUSION_DPR(UOBV003)static void setPitchAndBank(ControllableObject& co, double pitch_angle, double bank_angle);

		/*!
		Sets an objects direction using vectors. Angle is calculated in relation to the current angle.
		Deprecated. Use void applyDirection(ControllableObject& co, double direction).
		*/
		VBS2FUSION_DPR(UOBV004) static void setDirection(ControllableObject& co, double direction);

		/*!
		Returns a vector contains pitch and bank angles in degrees. 
		*/
		static vector<double> getPitchAndBank(ControllableObject& co);

		/*!
		@description 
		Set object's up vector. Direction vector will remain unchanged.

		@locality		

		@version [VBS2Fusion v2.71] 
		
		@param co - The object for which the orientation is applied. The object should be created before passing it as a parameter.
		@param vec - The direction vector applied to the object.

		@return Nothing.

		@example 

		@code		

		@endcode

		@overloaded Nothing.

		@related  

		@remarks This is a replication of  void setVectorUp(ControllableObject& co, vector3D vec)
		*/
		static void applyVectorUp(ControllableObject& co, vector3D vec);

		/*!
		@description		
		sets object's direction vector in world Position coordinates ([x, y, z]).
		can only influence an object's pitch. It can not influence bank. 
		Since version +1.30 accepts additional parameter which,	when object is attached, 
		specifies if vector applied is relative to parent object's orientation.

		@locality		

		@version [VBS2Fusion v2.71]
		
		@param co - The object for which the orientation is applied. The object should be created before passing it as a parameter.
		@param vec - The direction vector applied to the object.

		@return Nothing.

		@example 

		@code		

		@endcode

		@overloaded Nothing.

		@related  

		@remarks This is a replication of void applyVectorDir(ControllableObject& co, vector3D vec).
		*/
		static void applyVectorDir(ControllableObject& co, vector3D vec);

		/*!
		@description
		Sets an objects pitch and bank angles.
		Plus and minus values are corresponding with the direction, Which means plus values will be applied anticlockwise and minus values will be applied clockwise.
		Pitch is 0 when the object is level, 90 when pointing straight up and -90 when pointing straight down.
		Bank is 0 when level, 90 when the object is rolled to the right, -90 when rolled to the left, and 180 when rolled upside down.
		If boolean parameter "apply" is provided, it determines whether	the pitch and bank values are applied to the object (default=true).
		For pitch it gets the remainder value of 180 and for bank it gets the remainder value of 360.

		@locality		

		@version [VBS2Fusion v2.71]
		
		@param co - The object for which the orientation is applied. The object should be created before passing it as a parameter.
		@param pitch_angle - angel of the pitch in degrees.
		@param double bank_angle - angle of the bank in degrees.

		@return Nothing.

		@example 

		@code		

		@endcode

		@overloaded Nothing.

		@related  

		@remarks This is a replication of void setPitchAndBank(ControllableObject& co, double pitch_angle, double bank_angle).
		*/
		static void applyPitchAndBank(ControllableObject& co, double pitch_angle, double bank_angle);

		/*!
		@description
		Sets an objects direction using vectors. Angle is calculated in relation to the current angle. 

		@locality		

		@version [VBS2Fusion v2.71]
		
		@param co - The object for which the orientation is applied. The object should be created before passing it as a parameter.
		@param direction - The direction in which the object should turn in degrees after applying the function.		

		@return Nothing.

		@example 

		@code		

		@endcode

		@overloaded Nothing.

		@related  

		@remarks This is a replication of void setDirection(ControllableObject& co, double direction)
		*/
		static void applyDirection(ControllableObject& co, double direction);

	};
};

#endif //OBJECT_VECTOR_UTILITIES_H