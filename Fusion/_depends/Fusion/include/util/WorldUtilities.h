
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:	WorldUtilities.h

Purpose: utility class for world.

	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			09-07/2010	YFP: Original Implementation
	1.01		20-09/2010	YFP: methods added, getOrigin()
	1.02		03-08-2011	NDB: Added void onMapSignleClick(string command)
								 void onDoubleClick(string Map, string command)
	1.03		22-09-2011	CGS: Added double getDistance(position3D stPos, position3D enPos)
	1.04		23-09/2011	NDB: Added Methods
									double getDeclination()
									void applyDeclination(double declination)
	1.05		26-09/2011	NDB: Added Methods
									vector<string> getShapeFileList()
									ControllableObject getAttachedObject(ControllableObject& co)
									vector<ControllableObject> getAllAttachedObjects(ControllableObject& co)
									string getWorldName()
									void applyOrigin(double easting, double northing, double zone, string hemisphere)
									void switchAllLights(LIGHT_MODE lightMode)
									void initAmbientLife()
	1.06		18-11-2011	SSD: Added Methods
									Unit getNearestUnit(position3D pos)
									Unit getNearestUnit(position3D pos, string type)
	1.07		08-12-2011	DMB: Added Methods
									void drawGeometry(int lowLX, int lowLY, int upperRX, int upperRY)

/*****************************************************************************/

#ifndef VBS2FUSION_WORLDUTILITES_H
#define VBS2FUSION_WORLDUTILITES_H

/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES
#include <list>
// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "VBS2FusionDefinitions.h"
#include "position2D.h"
#if GEOM_OBJECT
#include "data/GeomObject.h"
#endif
#include "data/Vehicle.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBS2Fusion
{
	
	class VBS2FUSION_API WorldUtilities
	{
	public:

		/*!
		Returns cost in the cost map on a given position.
		type can be 0 for vehicles and 1 for unit.
		*/
		static COST_TYPE getPositionCost(position3D pos , int type );

		/*!
		Returns center position of nearest cost field starting the search position specified by pos.
		list of cost types can query the search field. Maximum radius for search specified by radius.

		
		*/
		static position3D getNearestCostPosition(position3D pos,list<COST_TYPE> costTypes,double radius,ControllableObject& co);
		
		/*!
		returns list of network IDs of given types within a radius of a given position.
		*/
		static list<NetworkID> getNearestObjects(position3D pos,list<string> types , int radius);

#if DEVELOPMENT
		/*!
		Returns list of network IDs of given SIDE within a radius of a given position.
		*/
		static list<NetworkID> getNearestObjects(position3D pos, string side, int radius);
#endif

		/*!
		Get the current Universal Transverse Mercator (UTM) origin of the Map.
		This returns UTM_POSITION. 	If -1 returned for east,north or zone
		*/
		static UTM_POSITION getOrigin();

		/*!
		Creates an indirect damage at the specified position according to the strength of damage given from damage
		and range of indirect damage given from range.
		*/
		static void createIndirectDamage(position3D position, int damage, int range);
		
		/*!
		Creates an indirect damage at the specified position according to the strength of damage given by damage,
		damage range given by range , disable/enable particle effects specified by disableParticles.
		*/
		static void createIndirectDamage(position3D position, int damage, int range, bool disableParticles);

		/*!
		Creates an indirect damage at the specified position according to the strength of damage given by damage,
		damage range given by range , disable/enable particle effects specified by disableParticles ,
		disable/enable creation of crates specified by disableCrates.
		*/
		static void createIndirectDamage(position3D position, int damage, int range, bool disableParticles, bool disableCrates);

		/*!
		Creates an indirect damage at the specified position according to the strength of damage given by damage,
		damage range given by range , disable/enable particle effects specified by disableParticles ,
		disable/enable creation of crates specified by disableCrates and disable/enable damage on all objects by disableDamage.
		*/
		static void createIndirectDamage(position3D position, int damage, int range, bool disableParticles, bool disableCrates, bool disableDamage);

		/*!
		Creates an indirect damage at the specified position according to the strength of damage given by damage,
		damage range given by range , disable/enable particle effects specified by disableParticles ,
		disable/enable creation of crates specified by disableCrates and disable/enable damage on all objects by disableDamage and disable/enable damage effects on units and vehicles.
		*/
		static void createIndirectDamage(position3D position, int damage, int range, bool disableParticles, bool disableCrates, bool disableDamage, bool disableDamageEntities);

		/*!
		Converts position in world space into screen space.
		Deprecated. Use position2D applyWorldToScreen(position3D pos)
		*/
		VBS2FUSION_DPR(UWLD003) static position2D worldToScreen(position3D pos);

		/*!
		Returns the position on VBS world corresponding to the given point on screen. 
		This function ignores any VBS objects and only return a point on VBS terrain.
		Deprecated. Use position3D applyScreenToWorld(position2D pos)
		*/
		VBS2FUSION_DPR(UWLD004) static position3D screenToWorld(position2D pos);
		

		/*!
		Returns the distance in meters between two positions. 
		*/
		static double getDistance(position3D stPos, position3D enPos);
		
		/*!
		Returns the magnetic declination from VBS
		*/
		static double getDeclination();

		/*!
		Sets the magnetic declination in VBS
		*/
		static void applyDeclination(double declination);

		/*!
		Returns the object to which an object or location is attached to. If it is unattached, then objNull is returned.
		*/
		static ControllableObject getAttachedObject(ControllableObject& co);

		/*!
		Returns the object that is attached to this location or object. If nothing is attached, then an empty array is returned.
		*/
		static vector<ControllableObject> getAllAttachedObjects(ControllableObject& co);

		/*!
		Return the class name of the currently loaded map (e.g. "Intro" for Rahmadi). 
		*/
		static string getWorldName();

		/*!
		Sets the origin for LVCGame
		*/
		static void applyOrigin(double easting, double northing, double zone, string hemisphere);

		/*!
		Returns the center position (i.e. X and Z coorinates);; of the currently loaded world. 
		*/
		static vector<float> GetWorldCenter();

#ifdef DEVELOPMENT
		/*!
		Return array of loaded shape files 
		*/
		static vector<string> getShapeFileList();

		/*!
		Enables/disables custom and standard vehicle tracks. Custom user texture can be specified to be used for custom tracks.

		Requires memory points in models and new config entries.
		Config properties:

		alphaTracksCustom(n) - sets opacity for tracks, can be indexed for specific track, otherwise applied to all
		memoryPointTrackCustomLn - left memory point, N for index of specific track
		memoryPointTrackCustomRn - right memory point, N for index of specific track
		animatedTrackCustomn - enables tracks for animated selections, tracks are drawn depending on selection animation 

		standard - Enable/disable standard tracks
		custom - Enable/disable custom tracks
		texture - Texture for custom tracks (needs to be given as absolute path)
		*/
		static void applyVehicleTracks(Vehicle& vehicle, bool standard, bool custom, string texture);
#endif


		/*!
		Returns the nearest unit object to the given position within a range of 
		50 meters.
		*/
		static Unit getNearestUnit(position3D pos);

		/*!
		Returns the nearest unit object to the given position of the given type within 
		a range of 50 meters. An invalid unit object will be returned if a proper unit
		type is not passed or there is no object within the range.
		*/
		static Unit getNearestUnit(position3D pos, string type);

		/*!
		Returns the nearest object (vehicle or unit) to the given position within 
		a range of 50 meters. An invalid object will be returned if there is no object
		within the range. 
		*/
		static ControllableObject getNearestObject(position3D pos);

		/*!
		Returns the nearest object (vehicle or unit) to the given position of the 
		given type within a range of 50 meters.An invalid object will be returned 
		if there is no object within the range or a proper type is not passed.
		*/
		static ControllableObject getNearestObject(position3D pos, string type);

		/*!
		According to the parameters crater will be created and particle effects of an explosion at the location specified 
		and no damage will be done by this function.
		Currently crater type not accommodated with VBS. So use empty string for type.  
		*/
		static ControllableObject createCrater(string type, position3D position, list<Marker> markerList, float randDist, 
			bool asl, float duartion, float size, string effectsType, float effectTTL);

		/*!
		Draws geometry of the objects and landscape in given rectangle.

		lowLX	- X value of the lower-left coordinates
		lowLY	- Y value of the lower-left coordinates
		upperRX	- X value of the upper-right coordinates
		upperRY	- Y value of the upper-right coordinates

		Deprecated. Use void applyGeometricDrawing(int lowLX, int lowLY, int upperRX, int upperRY);
		*/
		VBS2FUSION_DPR(UWLD002) static void drawGeometry(int lowLX, int lowLY, int upperRX, int upperRY);

		/*!
		Returns objects (vehicles and units) in the circle with given radius.
		*/
		static vector<ControllableObject> getNearObjects(position3D pos, int radius);

		/*!
		Returns objects (vehicles and units) of given type (or its subtype) in the
		circle with given radius.
		*/
		static vector<ControllableObject> getNearObjects(position3D pos, int radius, string type);

		/*!
		Returns the nearest object (vehicle or unit) to the given position of the 
		given type within a range of 50 meters.An invalid object will be returned 
		if there is no object within the range or a proper type is not passed.
		*/
		//static GeometryObject getNearestObjectEx(position3D pos);
		// DEVELOPMENT

		/*!
		@description  

		Switches lighting mode for all street lamps.  

		@locality

		Locally applied, locally effected

		@version [VBS2Fusion v2.70.4] 

		@param enum - LIGHT_MODE. Available modes are ( LIGHT_ON : Switch on all streetlamps, LIGHT_OFF : Switch off all streetlamps, LIGHT_AUTO : Switch on all streetlamps automatically during the nighttime )

		@return nothing 

		@example

		@code

		// Switch on all street lamps.
		WorldUtilities::applySwitchOn(LIGHT_ON);

		@endcode

		@overloaded 

		@related  

		@remarks This function works only for the street lamps that come by default for each respective terrain.		
		*/
		static void applySwitchOn(LIGHT_MODE lightMode);

		/*! 
		Returns the squared value of the straight distance between the two positions.( height/Y-axis is ignored ).
		calculation ignores the elevation and calculates the distance using pythagoras theory.
		@param positionA - Start 3D position.
		@param positionB - End 3D position.
		@return double.
		*/
		static double getSquared2DDisplacement(position3D positionA, position3D positionB);

		/*!
		Initializes the Ambient Life. 
		"Ambient Life" are things like bugs and birds moving randomly on map. Not all maps may have these defined.
		Should enable the environmental effects to see the effects. 
		Deprecated. Use void applyAmbientLifeInitialization();
		*/
		VBS2FUSION_DPR(UWLD001) static void initAmbientLife();

		/*!
		 Preload all textures and models around given Position to avoid visual artifacts after camera is moved.
		 Should be used before any abrupt camera change/cut.
		 Returns true once all data is ready.
		*/
		static bool applyCameraPreload( position3D& position);

		/*! 
		Returns the squared value of the straight distance between the two positions.
		@param positionA - Start 2D position.
		@param positionB - End 2D position.
		@return double.
		*/
		static double getSquared2DDisplacement(position2D positionA, position2D positionB);

		/*!
		Returns the squared value of the straight distance between the two objects( height/Y-axis is ignored ).
		Calculation ignores the elevation and calculates the distance using Pythagoras theory.
		@param objA - Start controllable Object.
		@param objB - End  controllable Object.
		@return double.
		*/
		static double getSquared2DDisplacement(ControllableObject objA, ControllableObject objB);

		/*!
		Rotates the 'targetPoint' around the 'centerPoint' clockwise in the given angle.Rotation is done around 
		the Y-axis (from top-down view).Therefore Y components of both positions are ignored. After the rotation, 
		Y component will remain the same and only x and z components will be changed in the returned value.
		@param centerPoint - Centered 3D position.
		@param angle - Angle is in degrees.
		@param targetPoint - The 3D Position which used to rotate.
		@return position3D - The position after rotate targetPoint around centerPoint by given angle.
		@effect - 
		@sample code -
		*/
		static position3D getRotatedPoint(position3D centerPoint, double angle, position3D targetPoint);

		/*!
		Returns the compass direction from one position to another. 
		@param pivot - A 3D position.
		@param target - Another 3D position.
		@return double - direction from pivot to target in degrees.
		*/
		static double getCompassDirection(position3D pivot, position3D target);

		/*!
		Returns the compass direction from one object to another. 
		@param pivot - A controllable object.
		@param target - Another controllable object.
		@return double - direction from pivot to target in degrees.
		*/
		static double getCompassDirection(ControllableObject& pivot, ControllableObject& target);

		/*!
		Returns the compass direction from a given object to a given position. 
		@param pivot - A controllable object.
		@param target - A 3D position.
		@return double - direction from pivot to target in degrees.
		*/
		static double getCompassDirection(ControllableObject& pivot, position3D target);

		/*!
		Returns the compass direction from a given position to a given object. 
		@param pivot - A 3D position.
		@param target - A controllable object.
		@return double - direction from pivot to target in degrees.
		*/
		static double getCompassDirection(position3D pivot, ControllableObject& target);

		/*!
		Rotates the 'targetPoint' around the 'centerPoint' clockwise in the given angle.
		@param centerPoint - Centered 2D position.
		@param angle - Angle is in degrees.
		@param targetPoint - The 2D Position which used to rotate.
		@return position2D - The position after rotate targetPoint around centerPoint by given angle.
		*/
		static position2D getRotatedPoint(position2D centerPoint,double angle, position2D targetPoint);

		/*!
		Rotates the 'targetObj' around the 'pivotObj' clockwise in the given angle.Rotation is done around the Y-axis (from top-down view).
		Therefore Y components (height) of both object positions are ignored.After the rotation, Y component will remain the same and 
		only x and z components will be changed in the returned position value.
		@param pivotObj - Centered controllable object.
		@param angle - Angle is in degrees.
		@param targetObj - The controllable object which used to rotate.
		@return position3D - The position after rotate targetObj around pivotObj by given angle.
		*/
		static position3D getRotatedPoint(ControllableObject& pivotObj, double angle, ControllableObject& targetObj);

		/*!
		Returns the position that has specified distance and compass direction from the passed position.
		@param pos - A 2D position.
		@param distance - considered distance from the pos in meters.
		@param direction - compass direction from the pos in degrees.
		@return position2D.
		*/
		static position2D getRelativePosition(position2D pos, double distance, double direction);

		/*!
		Calculates the Position that has specified distance and compass direction from the passed(input) position and 
		adds an offset to it at a right-angle, by the given offsetDistance.
		@param pos - A 2D position.
		@param distance - Considered distance from the pos in meters.
		@param direction - Compass direction from the pos in degrees.
		@param offsetDistance - Distance offset, at a 90 degree angle, added to the relative position which 
				given by along the compass direction with distance in meters.
		@return position2D.
		*/
		static position2D getRelativePosition(position2D pos, double distance, double direction, double offsetDistance);

		/*!
		Returns the position that has specified distance and compass direction from the passed position.
		@param pos - A 3D position.
		@param distance - considered distance from the pos in meters.
		@param direction - compass direction from the pos in degrees.
		@return position3D.
		*/
		static position3D getRelativePosition(position3D pos, double distance, double direction);

		/*!
		Calculates the Position that has specified distance and compass direction from the passed(input) position and adds an offset to it at 
		a right-angle, by the given offsetDistance.	All the calculations are done in the XZ plane (Y axis is not used or changed).
		@param pos - A 3D position.
		@param distance - Considered distance from the pos in meters.
		@param direction - Compass direction from the pos in degrees.
		@param offsetDistance - Distance offset, at a 90 degree angle, added to the relative position which 
				given by along the compass direction with distance in meters.
		@return position3D.
		*/
		static position3D getRelativePosition(position3D pos, double distance, double direction, double offsetDistance);

		/*!
		Use this function to determine if a position lies within a certain angle from another position. 
		@param pivot - A 3D position.
		@param direction - Checked direction in degrees.
		@param angWidth - Checked angle in degrees. Angle sector split in half by the direction.
		@param target - Checked 3D position.
		@return bool - Returns true if the target position lies within the sector defined by pivot position, direction and angle width.
		*/
		static bool isInsideAngleSector(position3D pivot, double direction, double angWidth, position3D target);

		/*!
		@description

		Used to initialize the Ambient Life within the VBS2 environment. 
		"Ambient Life" is a feature which controls the random movement and sounds of bugs and birds in the map. Not all maps may have these defined.
		Should enable the environmental effects to see the effects.  

		@locality

		Globally Applied, Globally Effected

		@version [VBS2Fusion v2.70.4]

		@param None.
	
		@return None.

		@example

		@code

		WorldUtilities::applyAmbientLifeInitialization();

		@endcode

		@overloaded 

		Nothing.

		@related

		@remarks This is a replication of void initAmbientLife();
		*/
		static void applyAmbientLifeInitialization();

		/*!
		@description

		Draws geometry of the objects and landscape in given rectangle.

		@locality

		Locally Effected

		@version [VBS2Fusion v2.70.4]

		@param lowLX	- X value of the lower-left coordinates
		@param lowLY	- Y value of the lower-left coordinates
		@param upperRX	- X value of the upper-right coordinates
		@param upperRY	- Y value of the upper-right coordinates

		@return None.

		@example

		@code

		WorldUtilities::applyGeometricDrawing(2400.25463577542,2280.12866227765,2301.77099999417,2179.34106001203);

		@endcode

		@overloaded 

		Nothing.

		@related

		@remarks This is a replication of void drawGeometry(int lowLX, int lowLY, int upperRX, int upperRY);
		*/
		static void applyGeometricDrawing(int lowLX, int lowLY, int upperRX, int upperRY);

		/*!

		@description

		Used to convert positions in world space into screen (UI) space within the VBS2 environment.
		If a specified position is not within the current map view, an empty array is returned.

		@locality

		Globally Applied, Globally Effected

		@version [VBS2Fusion v2.70.4]

		@param pos - position3D Which need to be converted.

		@return position2D - Converted position of the Screen..

		@example

		@code

		//The vehicle to be created
		Vehicle v1;

		position2D pos;

		pos = WorldUtilities::applyWorldToScreen(v1.getPosition());

		displayString+="The screen position is"+pos.getVBSPosition2D();

		@endcode

		@overloaded 

		Nothing.

		@related

		@remarks This is a replication of position2D worldToScreen(position3D pos);

		@remarks For accurate values execute the function in 3rd person mode.

		@remarks To obtain the screen size position of an object created in a client machine within a network, use the function 
		MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function WorldUtilities::applyWorldToScreen(position3D pos).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static position2D applyWorldToScreen(position3D pos);

		/*!

		@description

		Used to convert positions in world space into screen (UI) space within the VBS2 environment.
		If a specified position is not within the current map view, an empty array is returned.

		@locality

		Globally Applied, Globally Effected

		@version [VBS2Fusion v2.70.4]

		@param pos - position2D Which need to be converted.

		@return position3D - Rotated position in the VBS2 Environment.

		@example

		@code

		//The vehicle to be created
		Vehicle v1;

		position2D pos2;
		position3D pos3;

		pos2 = WorldUtilities::applyWorldToScreen(v1.getPosition());
		pos3 = WorldUtilities::applyScreenToWorld(pos2);

		displayString+="The world position is"+pos3.getVBSPosition();

		@endcode

		@overloaded 

		Nothing.

		@related

		@remarks This is a replication of position3D screenToWorld(position2D pos);

		@remarks For accurate values execute the function in 3rd person mode.

		@remarks To obtain the world position from the screen position of an object created in a client machine within a network, use the function 
		MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function WorldUtilities::applyScreenToWorld(position2D pos).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static position3D applyScreenToWorld(position2D pos);	

	};

}


#endif //VBS2FUSION_WORLDUTILITES_H