
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/


/*************************************************************************

Name:

	GeneralUtilities.h

Purpose:

	This file contains the declaration of the CameraUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			30-09/2009	SLA: Original Implementation
	2.0			10-02/2010	UDW: Version 2 Implementation
	2.01		13-09/2010  YFP: Added Methods,
									string getVBS2Directory(int)
									string getVBS2UserDirectory()
									string getVBS2MissionDirectory()
									string getVBS2InstallDirectory()
									string getVBS2EXEDirectory()
	2.02		30-08/2011 NDB: Added Methods,
									bool isFusionDeveloper()
									double getCustomerID()
									vector<double> getSupportedResolutions()
									void exportEMF(string fileName, bool drawGrid = true, bool drawContours = true, int drawTypes = 256, bool flipVertical = false)
									bool isTypeExists(string type)
									bool isValidVariableName(string name)
									bool isValidFileName(string name)
									void useOldNetworkCode(bool mode = false)
									void openURL(string url)
									void DisableRendering(bool disable)
									void applyResolution(int width, int height)
									bool IsDifficultyEnabled(string flagName)
									bool IsServer()
									double BenchMark()
	2.03		07-09/2011 NDB: Added Methods,
									bool isTitleEffectActive()
									vector<int> getNetworkStats()
									bool isDriverOverride()
									void UnLoadAllPlugins()
									void LoadAllPlugins()
									void connectToPhyxDebugger()

	2.04		28-09/2011 CGS: Added Methods, 
									double getLengthOf(string type)
									double getWeightOf(string type)
									double getVolumeOf(string type)
	2.05		28-10/2011 NDB: Added Methods,
									bool isMultiPlayer()
									bool isLaserShot()
									void applyDate(int year, int month, int day, int hour, int minute)
									void applyEyeSeparation(double distance)
									double getAccTime()
									bool isTeamSwitchEnabled()
									vector<float> getDate()
									float getViewDistance()
									void applyViewDistance(float distance)
									void skipTime(float duration)
									void forceMap(bool show)
									bool requiredVersion(string version)
									void showMap(bool show)
									void showWatch(bool show)
									void showCompass(bool show)
									void showRadio(bool show)
									void showPad(bool show)
									void showWarrant(bool show)
									void showGPS(bool show)
									void EnableRadio(bool state)
									void applyVideoSetting(string option, float setting)
/************************************************************************/

#ifndef GENERAL_UTILITIES_H
#define GENERAL_UTILITIES_H

#include <vector>
#include <list>
#include "position3D.h"
#include "VBS2Fusion.h"
#include "VBS2FusionDefinitions.h"


namespace VBS2Fusion
{	
	class VBS2FUSION_API GeneralUtilities
	{
	public:
			
		//********************************************************
		// Time Acceleration utilities  
		//********************************************************

		enum WindowState	{
			RTE, MISSION, MP_SELECT_ROLE,MP_SERVER_SELECT_MISSION, MAIN_MENU
		};
		
		/*!

		Set time acceleration coefficient. May be also used to slow time in cutscenes. 
		This command does NOT work in multiplayer.
		Deprecated. Use  void applyAccTime(double accTime).
		*/
		VBS2FUSION_DPR(UGNR001) static void setAccTime(double accTime);
			
		/*!
		 returns a list of ints consisting of VBS2 version in the format major minor and release
		 */

		/*!
			returns the VBS2 build version as a vector<int> of ints
		*/
		static vector<int> getVBS2Version();

		/*!
			returns the VBS2 build version as int
		*/
		static int getVBS2BuildVersion();

		/*!
			returns the whether the mission is started or not
		*/
		static bool isMissionRunning();

		/*!
			Returns VBS2 window state.
		*/
		static string getWindowState();

		/*!
			Executes the given plugin's plugin function in accordance with
			the given parameter.
		*/
		static void pluginFunction(string pluginName, string parameter);

		/*!
		returns absolute path of the VBS2 system folders,
		0 - User directory 
		1 - Mission directory 
		2 - Installation directory 
		3 - EXE directory
		*/
		static string getVBS2Directory(int parameter);

		/*!
		returns user directory of VBS2.
		*/
		static string getVBS2UserDirectory();

		/*!
		returns VBS2 mission directory. 
		*/
		static string getVBS2MissionDirectory();

		/*!
		 returns VBS2 installation directory. 
		*/
		static string getVBS2InstallDirectory();

		/*!
		returns VBS2 executable directory. 
		*/
		static string getVBS2EXEDirectory();

		/*!
		Returns aspect ratio as [horizontal,vertical] FOV.
		*/
		static vector<double> getAspectRatio();

		/*!
		Returns screen resolution as [width,height].
		*/
		static vector<double> getScreenResolution();

		/*!
		Returns frames per second for current view. 
		*/
		static vector<double> getFPS();

		/*!
		Returns an array containing the current date & time of the computer running VBS 
		(as opposed to daytime which returns the time as defined in the mission itself). 
		*/
		static vector<double> getSystemTime();

		/*!
		Returns current state of HLA
		*/
		static bool isLVCActive();

		/*!
		Exit the application
		Deprecated. Use applyShutDown()
		*/
		VBS2FUSION_DPR(UGNR002) static void ExitVBS();

		/*!
		Returns information about the current game state
		(e.g. whether a mission is being played, or whether the options window is open).
		Deprecated. Use  string getApplicationState()
		*/
		VBS2FUSION_DPR(UGNR003) static string applicationState();

		/*!
		returns wheather FusionDev is enabled or not
		*/
		static bool isFusionDeveloper();

		/*!
		returns customerID 
		*/
		static double getCustomerID();

		/*!
		Returns the resolutions supported by the current aspect ratio settings.
		Only works in full-screen mode.
		*/
		static vector<double> getSupportedResolutions();

				
		/*!
		Exports the current map as EMF. 

		filename - File name or absolute path to file. (e.g. "myFile.emf" or "c:\\myFile.emf") If no path is given, file is saved into VBS install folder.
		drawgrid - Draw grid (Optional, default is true)
		drawcontours - Draw contours (Optional, default is true)
		drawtypes - Draw types (Optional, default is all types)
			Roads:1
			Buildings:2
			Vegetation:4
			Water:8
			Trees:16
			Fences:32
			Walls:64
			PowerLines:128.
			Undef:256
		Different draw types can be combined by adding their numbers (e.g. to display roads and buildings, use 3 as drawtype) - only available in V1.23 and higher
		flipvertical: Boolean - Flips the emf upside down (Optional, default is false). By default EMF assumes 0,0 is top left. - only available in V1.23 and higher
		Deprecated. Use  void applyEMFExport(string fileName, bool drawGrid = true, bool drawContours = true, int drawTypes = 256, bool flipVertical = false)
		*/
		VBS2FUSION_DPR(UGNR004) static void exportEMF(string fileName, bool drawGrid = true, bool drawContours = true, int drawTypes = 256, bool flipVertical = false);

		/*!
		Checks whether an entity of this class exists in the current mission.
		The command takes into account any dynamically added or removed objects.
		*/
		static bool isTypeExists(string type);

		/*!
		Return true if the given variable conforms to the engine limitations (e.g. has to start with letter or underscore, no special characters except for underscore in name).
		Does not test for variables that are the same as command names, and still returns true in that situation, even though those variables shouldn't be used (e.g. isValidVarName "driver" returns true).
		*/
		static bool isValidVariableName(string name);

		/*!
		Returns true if given string is a valid file name (checks for prohibited characters). 
		*/
		static bool isValidFileName(string name);



#ifdef DEVELOPMENT

		/*!
		mode: True - turns on old network algorithem on, False uses default new network algorithm.
		Default is false
		*/
		static void useOldNetworkCode(bool mode = false);

#endif

		/*!
		Opens the supplied URL or file in the associated application (i.e. it may open a browser window for a URL,
		or notepad for a text file).
		If a Windows batch file (or application) is started this way, be aware that the "current directory" is still
		set to the VBS installation path, so any paths used in that batch file will be relative to the installation folder,
		and not the folder the batch file actually resides in.
		Deprecated. Use void applyURLOpen(string url).
		*/
		VBS2FUSION_DPR(UGNR005) static void openURL(string url);

		/*!
		Disables/enables 3D scene rendering
		Deprecated. Use void applyRederingDisable(bool disable)
		*/
		VBS2FUSION_DPR(UGNR006) static void DisableRendering(bool disable);

		/*!
		Checks specific difficulty settings of the current user. Difficulty flag names can be found 
		in the server profile file under class Difficulties/xxx/Flags (xxx being regular or veteran).
		*/
		static bool IsDifficultyEnabled(string flagName);

		/*!
		Returns true if the machine is either a server in a multiplayer game or if it is running a singleplayer game.
		*/
		static bool IsServer();

		/*!
		Returns the value of "3D Performance" in OFP Preferences (flashpoint.cfg). 
		This can be used to estimate the computer performance to adapt CPU and GPU demanding settings like view distance 
		dynamically in missions. 
		Deprecated. Use double getBenchMark().
		*/
		VBS2FUSION_DPR(UGNR007)static double BenchMark();

		/*!
		Returns true if a title effect is currently active. 
		*/
		static bool isTitleEffectActive();

		/*!
		Returns the network stats as [totalSentBytes,totalSentMsgs,totalRecievedBytes,totalRecievedMsgs]
		*/
		static vector<int> getNetworkStats();

		/*!
		Returns true if player is currently overriding the Driver
		*/
		static bool isDriverOverride();

		/*!
		Causes all loaded plugins in the plugins folder to be requested to be unloaded.
		On this call, its important for all DLLs to respond and to stop any currently executing threads. 
		*/
		static void UnLoadAllPlugins();

		/*!
		Loads all plugins in the plugins folder. 
		*/
		static void LoadAllPlugins();

#ifdef DEVELOPMENT 

		/*!
		Connects to the remote phyx debugger
		*/
		static void connectToPhyxDebugger();
#endif


		/*!
		Return approximate length of the object of given type.
		*/
		static double getLengthOf(string type);

		/*!
		Returns the defined weight of the object of given type.
		*/
		static double getWeightOf(string type);

		/*!
		Returns the volume of the object of given type in m�.
		*/
		static double getVolumeOf (string type);

		/*!
		Returns true if the VBS2 simulation is running in multiplayer mode
		and false if the simulation is running in single player mode. 
		*/
		static bool isMultiPlayer();


		/*!
		Returns true if the VBS2 simulation is running a lasershot compatible version.		
		*/
		static bool isLaserShot();

		/*!
		Sets the actual mission date and time. 
		*/
		static void applyDate(int year, int month, int day, int hour, int minute);

#ifdef DEVELOPMENT
		/*!
		Sets the stereo rendering eye separation in meters. Default value is 0.064
		*/
		static void applyEyeSeparation(double distance);
#endif

		/*!
		Returns the current time acceleration factor 
		*/
		static double getAccTime();

		/*!
		Check if Team Switch is currently enabled. Team Switch is enabled by default.
		*/
		static bool isTeamSwitchEnabled();

		/*!
		Return the actual mission date and time as an array [year, month, day, hour, minute]. Month is a full number 
		between 1 and 12, day is between 1 and 31, hour is between 0 and 23 and minute is between 0 and 59. 
		*/
		static vector<float> getDate();

		/*!
		Returns the current view distance.
		*/
		static float getViewDistance();

		/*!
		Set rendering distance, in metres. Default is 900m (in OFP) or 1,200m (in ArmA), accepted range is 500m to 
		5,000m (in OFP) or 10,000m (in ArmA). 
		*/
		static void applyViewDistance(float distance);

		/*!
		Jumps the specified number of hours forward or backward.
		The time of day and tides are adjusted, but no changes are made to any units. If present, 
		the lower level of clouds instantly jump to the position they would be in if time had passed normally.
		Deprecated. Use void applytimeSkip(float duration)
		*/
		VBS2FUSION_DPR(UGNR008) static void skipTime(float duration);

		/*!
		Displays the map on the screen during a mission. 
		Deprecated. Use  void applyMapForceDisplay(bool show).
		*/
		VBS2FUSION_DPR(UGNR009)static void forceMap(bool show);

		/*!
		Check if version of application is available. If the current version is older than the required one, 
		a warning message is shown and false is returned. Version of format Major.Minor, e.g. 1.30
		Deprecated. Use bool isRequiredVersion(string version)
		*/
		VBS2FUSION_DPR(UGNR010)static bool requiredVersion(string version);

#ifdef DEVELOPMENT
		/*!
		Enable Map (default true) 
		*/
		static void showMap(bool show);

		/*!
		Shows or hides the watch on the map screen, if enabled for the mission and you possess the item. (default true) 
		(Only works for animated maps in V1.40.) 
		*/
		static void showWatch(bool show);

		/*!
		Shows or hides the compass on the map screen, if enabled for the mission and you possess the item. (default true). (Only works for animated maps in V1.40.) 
		*/
		static void showCompass(bool show);

		/*!
		Shows or hides the radio on the map screen, if enabled for the mission and you possess the item. (default true). (Only works for animated maps in V1.40.) 
		*/
		static void showRadio(bool show);

		/*!
		Shows or hides the notebook on the map screen, if enabled for the mission. (default true). (Only works for animated maps in V1.40.) 
		*/
		static void showPad(bool show);

		/*!
		Enable ID card (default false). Obsolete command. 
		*/
		static void showWarrant(bool show);

		/*!
		Shows or hides the GPS receiver on the map screen, if enabled for the mission and you possess the item. (default false). (Doesn't work in V1.40.)
		*/
		static void showGPS(bool show);
#endif

		/*!
		Enable and disable radio messages to be heard and shown in the left lower corner of the screen. This command can be helpful during cutscenes.
		Deprecated. Use void applyRadioEnable(bool state)
		*/
		VBS2FUSION_DPR(UGNR011)static void EnableRadio(bool state);

		/*!
		Sets the internal engine video options.

		option - Supported options:

		"shadowDistance": maximum shadow distance
		"objectDetail": (0-4) (V1.23+)
		"viewDistance": (10-10000) (V1.23)
		"objectDrawDistance": (0-10000) (V1.23)
		"shadowsCamHeightCoef": Computing the shadows drawing distance from the camera's height above the ground. Draw distance is calculated as height * shadowsCamHeightCoef. (V1.31)
		"screenResolution": Followed by two parameters: width, height. Supported only in fullscreen mode and only for supported resolutions. List of supported resolutions is provided by getSupportedResolutions. (V1.48+) 

		setting - value, depending on option
		*/
		static void applyVideoSetting(string option, float setting);

		/*!
		Enable / disable Team Switch. The default setting is enabled. 
		Deprecated. Use void applyTeamSwitchEnable(bool enable)
		*/
		VBS2FUSION_DPR(UGNR012)static void EnableTeamSwitch(bool enable);

		/*!
		Returns an array with all matching files and folders found in the specified directory.
		Does not search sub-directories.

		dir : Sub-directory to search in
		fileMask : Mask of filename to search for (Wildcards are supported via "*" and "?")
		profileDir : If true, 'dir' will be searched relative to the user's profile directory.
					 If false, 'dir' will be relative to the mission directory.
		absoluteDir: If true, 'dir' can be an absolute path (in which case 'profileDir' is ignored).

		Return value : vector of FILEINFO structs 
		*/
		static vector<FILEINFO> listFiles(string dir, string fileMask, bool profileDir, bool absoluteDir);

		/*!
		Sets the internal engine video option - 'ShadowDistance'
		settingValue - maximum shadow distance 
		Deprecated. Use  void applyShadowDistance(double settingValue)
		*/
		VBS2FUSION_DPR(UGNR014)static void setShadowDistance(double settingValue);

		/*!
		Sets the internal engine video option - 'objectDetail'
		settingValue - Value between 0 to 4 
		Deprecated. Use void applyObjectDetail(int settingValue)
		*/
		VBS2FUSION_DPR(UGNR015)static void setObjectDetail(int settingValue);

		/*!
		Sets the internal engine video option - 'viewDistance'
		settingValue - Value between 10 to 10000 (V1.23)
		viewDistance is 10000 by default. However, the setting "MaxViewDistance" can be defined within 
		the vbs2.cfg file, and its value is used instead as the max input value for this setting.
		Deprecated. Use void applyDistanceView(double settingValue).
		*/
		VBS2FUSION_DPR(UGNR016)static void setViewDistance(double settingValue);
#if DMB_TEST
		/*!
		Sets the internal engine video option - 'objectDrawDistance'
		settingValue - Value between 0 to 10000 (V1.23)
		viewDistance is 10000 by default. However, the setting "MaxObjectDrawDistance" can be defined 
		within the vbs2.cfg file, and its value is used instead as the max input value for this setting.
		*/
		static void setObjectDrawDistance(double settingValue);
#endif
		/*!
		Sets the internal engine video option - 'shadowsCamHeightCoef'
		Computing the shadows drawing distance from the camera's height above the ground. Draw distance 
		is calculated as height * shadowsCamHeightCoef. (V1.31) 
		Deprecated. Use void applyShadowsCamHeightCoef(double settingValue).
		*/
		VBS2FUSION_DPR(UGNR017)static void setShadowsCamHeightCoef(double settingValue);

		/*!
		Sets the internal engine video option - 'screenResolution'
		Input parameters: width, height. 
		Supported only in fullscreen mode and only for supported resolutions.(V1.48+) 
		Deprecated. Use void applyScreenResolution(int width, int height)
		*/
		VBS2FUSION_DPR(UGNR018)static void setScreenResolution(int width, int height);

		/*!
		Used to internationalise text messages. A string is returned from 'Stringtable.csv' which corresponds
		to the stringName.
		*/
		static string localizeStringTableValue(string& stringName);

		/*!
		Writes message to the report file (%localappdata%\VBS2\<Name of VBS2 .exe>.RPT).
		Be aware that the report file may contain other, engine-created messages.
		*/
		static void diagLog(string& message);

		/*!
		Writes message to the report file (%localappdata%\VBS2\<Name of VBS2 .exe>.RPT).
		Be aware that the report file may contain other, engine-created messages.
		*/
		static void diagLog(ControllableObject& co);


		/*!
		Takes a screenshot of current scene.

		If fileName is not present (i.e. empty string "") or ends with "\" or "/", then folder %USERPROFILE%\My Documents\VBS2\Screenshots (plus any specified subfolders) will be created if non-existent, and a file name is generated from the date information (e.g. 2008_4_18_18_0_15_250.tga).
		If fileName starts with "\" or "/", the file will be written to %USERPROFILE%\My Documents\VBS2\Screenshots. (Screenshots folder, or any referred subfolders, must exist already!)
		If fileName is present and doesn't end with "\" or "/", then it is considered an absolute file path, and will be written to that location (destination folder must already!).
		The file extension is automatically added to the fileName, according to the specified fileType.
		If size definitions are used, be aware that the screenshot is only resized. It is not possible to increase precision beyond the current screen resolution.
		If VBS was started with the forceSimul parameter screenshots can be taken even if the window has been minimized. 


		fileName - Name of file to be saved. Can include path, or be left empty for automatic name.
		fileType - Can be BMP, JPG, TGA, PNG, DDS, PPM, DIB, HDR or PFM. If not present or unknown, then JPG will be used.
		width - Width of saved screenshot. (optional, default: 0 == current resolution)
		height - Height of saved screenshot. (optional, default: 0 == current resolution)
		*/
		static void saveScreenShot(string fileName = "", PICTURE_FILETYPE fileType = JPG, double width = 0, double height = 0);

		/*!
		Checks whether cheats are enabled (whether the designers' version is running).
		*/
		static bool isCheatsEnabled();

		/*!
		Enable/Disable completely the GPS receiver. 
		Deprecated. Use void applyGPSEnable(bool enable).
		*/
		VBS2FUSION_DPR(UGNR013)static void EnableGPS(bool enable);

		/*!
		Checks whether the player has the global GPS receiver enabled.
		*/
		static bool isEnabledGPS();

		/*!
		Returns the available ranks for the given ranking system.
		Return value - vector of RANK_CLASS struct, RANK_CLASS is containing name and rating
		*/
		static vector<RANK_CLASS> getRankClasses(string& rankSystem); 

		/*!
		Returns vector of double values which contains width, height, 2D viewport Width,
		2D viewport Height, aspect ration and UI scale
		*/
		static vector<double> getResolution();


		/*
		MultiChanelling  support 
		configuration details
		CONFIG_VALUES_MULTICHANNEL 
		string name;
		double leftAngle ;
		double rightAngle ;
		double bottomAngle ; 
		double topAngle ; 
		double azimuth; 
		double pitch ; 
		double roll;
		double xPos ; 
		double yPos ;
		double zPos ;
		Deprecated. Use void applyMultiChannelConfig(list<CONFIG_VALUES_MULTICHANNEL> param)
		*/
		VBS2FUSION_DPR(UGNR019)static void setMultiChannelConfig(list<CONFIG_VALUES_MULTICHANNEL> param);

		/*
		Returns the number of currently displayed frame.
		*/
		static int getDiagFrameNo();

		/*
		Returns the seconds passed since the VBS2 was started.
		Expressed in seconds, with two decimal digits.
		*/
		static double getDiagTickTime();

		/*
		Returns whether the current player is logged in as the view server.
		*/
		static bool isViewServer();

		/*
		Returns the setting for a specific video options (as set via applyVideoSetting function or script command).
		option - Supported options:

		"shadowDistance": maximum shadow distance
		"objectDetail" 
		"viewDistance"
		"objectDrawDistance"
		"shadowsCamHeightCoef": Computing the shadows drawing distance from the camera's height above the ground.
		"screenResolution": Followed by two parameters: width, height. Supported only in fullscreen mode and only for supported resolutions.

		if you pass a invalid string as an argument, it  will return a null vector.
		*/
		static vector<double> getVideoSetting(string option);

		/*!
		@description

		Controls whether the game keeps on running, even if VBS loses focus.

		@locality

		Globally Applied,Globally Effected

		@version [VBS2Fusion v2.70.2]

		@param  enable - bool value to enable or disable forced simulation mode.

		@return Nothing

		@example

		@code

		//Enable the Simulation
		GeneralUtilities::applyForceSimulationEnable(true);

		@endcode

		@overloaded 

		None

		@related

		bool GeneralUtilities::isForceSimulationEnabled()

		@remarks

		It causes same effect as forceSimul startup parameter
		*/
		static void applyForceSimulationEnable( bool enable );

		/*!
		@description

		Checks whether is "forced simulation mode" (keeps running, even if VBS loses focus), as set by either
		void applyForceSimulationEnable( bool enable ) function or forceSimul startup parameter.

		@locality

		Globally Applied

		@version [VBS2Fusion v2.70.2]

		@param None.

		@return bool

		@example

		@code

		//Enable or Disable the Force Simulation Feature

		GeneralUtilities::applyForceSimulationEnable( bool enable );

		// Used to display if the ForceSimulation is Enabled

		displayString += �The force simulation has been enabled is� + conversions::BoolToString(GeneralUtilities::isForceSimulationEnabled());

		@endcode

		@overloaded 

		None.		

		@related

		void GeneralUtilities::applyForceSimulationEnable( bool enable )

		@remarks

		*/
		static bool isForceSimulationEnabled();

		/*!
		@description 
		Sets admin mode on/off. "Admin" in this case refers to the ability to use the RTE (not the server administrator).
		
		@locality

		Locally Applied, Locally Effected

		@version  [VBS2Fusion v2.70.3]

		@param admin - If true admin mode will on and else off.
		
		@return Nothing.

		@example

		@code

		//The value can be set as true or false
		GeneralUtilities::applyAdmin(true);

		@endcode

		@overloaded 
		
		Nothing.

		@related
		
		@remarks 	
		*/
		static void applyAdmin(bool admin);

		/*!
		@description
		Set time acceleration coefficient. May be also used to slow time in cutscenes. 
		This command does NOT work in multiplayer.

		@locality

		Locally applied.

		@version [VBS2Fusion v2.70.4] 
		
		@param	accFactor - A real number.		

		@return Nothing.

		@example

		@code

		//Apply the time acceleration coefficient.
		GeneralUtilities::applyAccTime(0.1);

		@endcode

		@overloaded

		@related  

		@remarks This is a replication of void setAccTime(double accTime)
		*/	
		static void applyAccTime(double accTime);

		/*!
		@description - Exit the application.
		
		@locality

		Locally applied, Locally effected

		@version [VBS2Fusion v2.70.4] 
		
		@param	- Nothing	

		@return - Nothing.

		@example

		@code

		//Shut down the application.
		GeneralUtilities::applyShutDown();

		@endcode

		@overloaded 

		Nothing.

		@related  

		@remarks This is a replication of void ExitVBS()
		*/	
		static void applyShutDown();

		/*!
		@description

		Returns information about the current game state
		(e.g. whether a mission is being played, or whether the options window is open).		

		@locality

		Locally applied.

		@version [VBS2Fusion v2.70.4] 
		
		@param	- Nothing

		@return - [inmission,display]
		inmission: Boolean - whether a mission is currently running (pre 1.40: false on a dedicated server)
		display: String - current dialog. Can be one of the following: ,
		"" (unknown display)
		"AAR"
		"AAR_SELECT"
		"DEBRIEFING"
		"MAIN_MENU"
		"MISSION"
		"MISSION_INTERUPT" (interrupt menu - Continue/Save/Abort)
		"MP_CLIENT_GET_READY" (client is in Briefing phase before the actual mission start) V2.10+
		"MP_CLIENT_WAITING"
		"MP_PARTICIPANTS"
		"MP_SELECT_ROLE"
		"MP_SELECT_SERVER"
		"MP_SERVER_GET_READY"
		"MP_SERVER_SELECT_MISSION"
		"OME"
		"OME_PREVIEW"
		"OME_SELECT_ISLAND"
		"OPTIONS"
		"OPTIONS_AUDIO"
		"OPTIONS_CONTROLS"
		"OPTIONS_DIFFICULTY"
		"OPTIONS_VIDEO"
		"RTE"
		"RTE_PREVIEW"
		"SP_SELECT_MISSION"

		@example

		@code

		//Get the application state.
		string AppState = GeneralUtilities::getApplicationState();

		//Display the return value.
		displayString = "Application State : "+AppState;

		@endcode

		@overloaded 

		@related  

		@remarks This is a replication of string applicationState()
		*/
		static string getApplicationState();

		/*!
		@description
		Exports the current map as EMF. DrawTypes available are Roads=1,Buildings=2,Vegetation=4. 
		To display roads and buildings use 1+2 for DrawTypes, to display only Vegetation use 4.
		DrawGrid,DrawContours,DrawTypes, and FlipVertical are optional parameters, by default DrawGrid,DrawContours are both true,
		and DrawTypes is all possible types finally FlipVertical is false. By default EMF assumes 0,0 is top left.		

		@locality

		Locally applied, locally effected.

		@version [VBS2Fusion v2.70.4]
		
		@param filename - File name or absolute path to file. (e.g. "myFile.emf" or "c:\\myFile.emf") If no path is given, file is saved into VBS install folder.		
		@param drawgrid - Draw grid (Optional, default is true).
		@param drawcontours - Draw contours (Optional, default is true).
		@param drawtypes - Draw types (Optional, default is all types).	
		@param Different draw types can be combined by adding their numbers (e.g. to display roads and buildings, use 3 as drawtype) - only available in V1.23 and higher.
		@param flipvertical - Flips the emf upside down (Optional, default is false). By default EMF assumes 0,0 is top left. - only available in V1.23 and higher.
							  Roads:1
							  Buildings:2
							  Vegetation:4
							  Water:8
							  Trees:16
							  Fences:32
							  Walls:64
							  PowerLines:128.
							  Undef:256

		@return	Nothing.

		@example

		@code

		//Export the EMF file to specified path.
		GeneralUtilities::applyEMFExport(string("D:\\myFile.emf"),true,true,256,false);

		@endcode

		@overloaded	Nothing.

		@related  

		@remarks This is a replication of void exportEMF(string fileName, bool drawGrid = true, bool drawContours = true, int drawTypes = 256, bool flipVertical = false); 
		*/
		static void applyEMFExport(string fileName, bool drawGrid = true, bool drawContours = true, int drawTypes = 256, bool flipVertical = false);

		/*!
		@description
		Opens the supplied URL or file in the associated application (i.e. it may open a browser window for a URL, or notepad for a text file).
		If a Windows batch file (or application) is started this way, be aware that the "current directory" is still set to the VBS installation path, 
		so any paths used in that batch file will be relative to the installation folder, and not the folder the batch file actually resides in.	

		@locality

		Locally applied, locally effected

		@version [VBS2Fusion v2.70.4]
		
		@param url - Web address or filename to be opened.		

		@return Nothing.

		@example

		@code

		//Open the specified URL
		GeneralUtilities::applyURLOpen(string("http://www.simct.com"));

		@endcode

		@overloaded

		Nothing

		@related  

		@remarks This is a replication of void openURL(string url)
		*/
		static void applyURLOpen(string url);

		/*!
		@description
		Turns 3D rendering on and off.
		Used mainly for debugging purposes.

		@locality

		Locally applied, locally effected

		@version [VBS2Fusion v2.70.4] 
		
		@param	disable - If true, no 3D objects are rendered		

		@return Nothing.

		@example

		@code

		//Disable rendering.
		GeneralUtilities::applyRederingDisable(true);

		@endcode

		@overloaded

		Nothing

		@related  

		@remarks This is a replication of void DisableRendering(bool disable)
		*/
		static void applyRederingDisable(bool disable);

		/*!
		@description
		Returns the value of "3D Performance" in OFP Preferences (flashpoint.cfg).
		This can be used to estimate the computer performance to adapt CPU and GPU demanding settings like view distance dynamically in missions.

		@locality

		Locally applied

		@version [VBS2Fusion v2.70.4] 
		
		@param	Nothing  	

		@return  A real number.

		@example

		@code

		//Assign the return value to a variable
		double BenchMark = GeneralUtilities::getBenchMark();

		//Display the benchmark value
		displayString = "Bench Mark : "+conversions::DoubleToString(BenchMark);

		@endcode

		@overloaded 

		@related  

		@remarks This is a replication of double BenchMark();
		*/
		static double getBenchMark();

		/*!
		@description
		Jumps the specified number of hours forward or backward.
		he time of day and tides are adjusted, but no changes are made to any units. 
		 If present, the lower level of clouds instantly jump to the position they would be in if time had passed normally.

		@locality

		Locally applied, locally effected

		@version [VBS2Fusion v2.70.4] 
		
		@param	duration: Number - Hours to skip.
					A positive value will create a forward time jump, a negative value will jump backwards.
					One second is roughly 0.00026 hours.
					Any calculations must be enclosed in parentheses, e.g. skipTime (_seconds/3600)

		@return Nothing

		@example

		@code

		//Skip time by 5.5 forward.
		GeneralUtilities::applytimeSkip(5.5)

		@endcode

		@overloaded 

		Nothing.

		@related  

		@remarks This is a replication of void skipTime(float duration)
		*/
		static void applytimeSkip(float duration);

		/*!
		@description
		Displays the map on the screen during a mission.		

		@locality

		Locally applied, locally effected

		@version [VBS2Fusion v2.70.4]
		
		@param	show - Boolean(True or false)

		@return Nothing

		@example

		@code

		//Display the map view.
		GeneralUtilities::applyMapForceDisplay(true);

		@endcode

		@overloaded

		Nothing

		@related  

		@remarks This is a replication of void forceMap(bool show)
		*/
		static void applyMapForceDisplay(bool show);

		/*!
		@description
		Check if version of application is available. If the current version is older than the required one,
		a warning message is shown and false is returned. Version of format Major.Minor, e.g. 1.30

		@locality

		Locally applied.

		@version [VBS2Fusion v2.70.4]
		
		@param	version - Version number

		@return Boolean

		@example 

		@code

		//Check the version of application is available.
		bool ver = GeneralUtilities::isRequiredVersion(string("2.15"));

		//Display the return value as a string.
		displayString = "Is required version : "+ conversions::BoolToString(ver);

		@endcode

		@overloaded

		Nothing

		@related  

		@remarks This is a replication of static bool requiredVersion(string version)
		*/
		static bool isRequiredVersion(string version);

		/*!
		@description
		Enable and disable radio messages to be heard and shown in the left
		lower corner of the screen. This command can be helpful during cutscenes.

		@locality

		Locally applied, locally effected

		@version [VBS2Fusion v2.70.4]
		
		@param	state: Boolean - true to enable the radio, false to disable it

		@return Nothing

		@example 

		@code

		//Disable the radio message.
		GeneralUtilities::applyRadioEnable(false);

		@endcode

		@overloaded 

		Nothing

		@related  

		@remarks This is a replication of void EnableRadio(bool state)
		*/
		static void applyRadioEnable(bool state);

		/*!
		@description 
		Controls availability of Team Switch. By default switching is enabled.

		@locality

		Locally applied, locally effected

		@version [VBS2Fusion v2.70.4]
		
		@param	enable - True to enable switching, false to disable.

		@return Nothing

		@example 

		@code

		//Enable team switching
		GeneralUtilities::applyTeamSwitchEnable(true);

		@endcode

		@overloaded 

		Nothing.

		@related  

		@remarks This is a replication of  void EnableTeamSwitch(bool enable)
		*/
		static void applyTeamSwitchEnable(bool enable);

		/*!
		@description 
		Enable/Disable completely the GPS receiver (the default is false).

		@locality

		Locally applied, locally effected

		@version [VBS2Fusion v2.70.4]
		
		@param	enable - True to enable GPS receiver, false to disable.

		@return Nothing

		@example 

		@code

		//Enable the GPS receiver.
		GeneralUtilities::applyGPSEnable(true);

		@endcode

		@overloaded

		Nothing

		@related  

		@remarks This is a replication of  void EnableGPS(bool enable)
		*/
		static void applyGPSEnable(bool enable);

		/*!
		@description 
		Sets the internal engine video option - 'ShadowDistance'

		@locality

		Locally applied, locally effected

		@version [VBS2Fusion v2.70.4] 
		
		@param	settingValue - Maximum shadow distance value.

		@return Nothing

		@example 

		@code

		//Shadow distance is set to 5.5
		GeneralUtilities::applyShadowDistance(5.5);

		@endcode

		@overloaded 

		@related  

		@remarks This is a replication of  void setShadowDistance(double settingValue)
		*/
		static void applyShadowDistance(double settingValue);

		/*!
		@description 
		Sets the internal engine video option - objectDetail.

		@locality

		Locally applied, locally effected.

		@version [VBS2Fusion v2.70.4] 
		
		@param settingValue - (0-4) Corresponds to ["Very low","Low","Normal","High","Very high"] options in advanced video.

		@return Nothing

		@example 

		@code

		//Apply object detail to "Low".
		GeneralUtilities::applyObjectDetail(1);

		@endcode

		@overloaded

		Nothing.

		@related  

		@remarks This is a replication of void setObjectDetail(int settingValue).
		*/
		static void applyObjectDetail(int settingValue);

		/*!
		@description
		Sets the internal engine video option - viewDistance

		@locality

		Locally applied, locally effected.

		@version [VBS2Fusion v2.70.4] 
		
		@param settingValue - sets the viewable distance limit, Range 10-10000.

		@return Nothing

		@example 

		@code

		//Apply the distance view.
		GeneralUtilities::applyDistanceView(20.3);

		@endcode

		@overloaded

		Nothing

		@related  

		@remarks This is a replication of void setViewDistance(double settingValue).
		*/
		static void applyDistanceView(double settingValue);

		/*!
		@description 
		Sets the internal engine video option - shadowsCamHeightCoef

		@locality

		Locally applied, locally effected.

		@version [VBS2Fusion v2.70.4]  
		
		@param settingValue - Computing the shadows drawing distance from the camera's height above the ground. Draw distance is calculated as height * shadowsCamHeightCoef.

		@return Nothing

		@example 

		@code

		GeneralUtilities::applyShadowsCamHeightCoef(18.9);

		@endcode

		@overloaded

		Nothing

		@related  

		@remarks This is a replication of  void setShadowsCamHeightCoef(double settingValue).
		*/
		static void applyShadowsCamHeightCoef(double settingValue);

		/*!
		@description 
		Sets the internal engine video option - screenResolution.
		Supported only in fullscreen mode and only for supported resolutions.

		@locality

		Locally applied, locally effected.

		@version [VBS2Fusion v2.70.4]   
		
		@param width - width of the screen.
		@param height - height of the screen.

		@return Nothing.

		@example 

		@code

		//Apply the screen resolution to 640x360
		GeneralUtilities::applyScreenResolution(640,360);

		@endcode

		@overloaded 
		
		Nothing.

		@related  

		@remarks This is a replication of  void setScreenResolution(int width, int height)
		@remarks Two more parameters can be defined: width, height, render percentage, windowed. e.g. [1280,720,50,true].
		*/
		static void applyScreenResolution(int width, int height);

		/*!
		@description	
		MultiChanelling  support.  
		CONFIG_VALUES_MULTICHANNEL 
		string name;
		double leftAngle ;
		double rightAngle ;
		double bottomAngle ; 
		double topAngle ; 
		double azimuth; 
		double pitch ; 
		double roll;
		double xPos ; 
		double yPos ;
		double zPos ;

		@locality

		Locally applied, Globally effected

		@version [VBS2Fusion v2.70.4] 
		
		@param param - Each struct should be added to this list.

		@return Nothing.

		@example 

		@code

		//Define the client 1.
		VBS2Fusion::CONFIG_VALUES_MULTICHANNEL client1;
		client1.name = "VC01";
		client1.leftAngle = 25;
		client1.rightAngle = 25;
		client1.bottomAngle = 15;
		client1.topAngle = 15;
		client1.azimuth = -54 ;
		client1.pitch = 0 ;
		client1.roll = 0;
		client1.xPos= 0;
		client1.yPos= 0;
		client1.zPos= 0;

		//Push Clients into list.
		list<VBS2Fusion::CONFIG_VALUES_MULTICHANNEL> clientList;
		clientList.push_back(client1);

		//Set Multichannel Configurations.
		GeneralUtilities::applyMultiChannelConfig(clientList);

		@endcode

		@overloaded Nothing.

		@related  

		@remarks This is a replication of  void setMultiChannelConfig(list<CONFIG_VALUES_MULTICHANNEL> param)
		*/
		static void applyMultiChannelConfig(list<CONFIG_VALUES_MULTICHANNEL> param);
	};
};
#endif

