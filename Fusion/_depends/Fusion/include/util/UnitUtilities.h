
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/
/*************************************************************************

Name:

	UnitUtilities.h

Purpose:

	This file contains the declaration of the UnitUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			26-03/2009	RMR: Original Implementation
	1.0			26-09/2009	SDS: Update create Unit to support ENUM structure to define unit type

	2.0			10-02/2010  YFP: Added  void CreateUnit(Unit& u,string type,position3D position,string markers,double placementRadius,string specialProperties,string group)
										void setUnitBehaviour(Unit& unit, BEHAVIOUR behavior)
										void commandStop(Unit& u)
										void globalChat(Unit& u,string txtMessage)
										void reloadWeapons(Unit& u)	
	2.01		10-02/2010	MHA: Comments Checked
	2.02		25-02/2010	YFP: Added void applyAlias(Unit& u)
	2.03		24/05/2010  YFP: Added Methods, 
										void performSalute(Unit&, Unit&);
										void setHeight(Unit&, double);
										double getHeight(Unit&);
										void setBMI(Unit&, double);
										double getBMI(Unit&);
	2.04		10-01/2011  CGS: Added CreateUnit(Unit& u,Group& _group)
								 Modified applyEndurance(Unit& u, double endurance)
									doWatch(Unit& unit, position3D targetPosition)
									CreateUnit(Unit& u, position3D position)
									CreateUnit(Unit& u,string strAlias)
									CreateUnit(Unit& u)
									doFire(Unit& firingUnit, position3D pos)
									setUnitBehaviour(Unit& unit, BEHAVIOUR behaviour)
									doWatch(Unit& unit, position3D targetPosition)
									applyExperience(Unit& u, double experience)

	2.05		11-03/2011 YFP: Added Methods
									disableGunnerInput(Unit&,GUNNERINPUT_MODE)
									void applyHeadDirection(Unit&,double,double,double, bool);
	2.06		11-03/2011 SSD: Added Method isPlayerControlled(Unit);
	2.07		07-09/2011 NDB: Added Methods
									double getAerobicFatigue(Unit& unit)
									double getAnaerobicFatigue(Unit& unit)
									double getMoraleLevel(Unit& unit)
									double getMoraleLongTerm(Unit& unit)
									double getTraining(Unit& unit)
									int getUpDegree(Unit& unit)
									void EnablePersonalItems(Unit& unit, bool enable)
									bool isLookingAt(Unit& unit, ControllableObject& co, double angle)
									bool isSpeakable(Unit& unit)
									void allowSpeech(Unit& unit, bool speech)
									void applyAmputation(Unit& unit, BODYPART bodyPart)
									void applyAmputation(Unit& unit, MAIN_BODYPART bodyPart, BODY_SEGMENT segment)
									string getAmputation(Unit& unit, BODYPART bodyPart)
									void addToMorale(Unit& unit, double moralChange)
									void addToAnaerobicFatigue(Unit& unit, double AnaerobicFatigueChange)
									void addToAerobicFatigue(Unit& unit, double AerobicFatigueChange)
									Vehicle getAssignedVehicle(Unit& unit)
									void CreateUnitLocal(string type, position3D& position,Group& group, string initString = "", double skill = 0.5, string rank = "PRIVATE")
	2.08		19-09/2011	NDB: Added Method
									void StopUnit(Unit& unit)
	2.09		20-09/2011	CGS: Added Method
									void reviveUnit(Unit& unit)
	2.10		26-09/2011	NDB: Added Methods
									bool isWearingNVG(Unit& unit)
									bool isPersonalItemsEnabled(Unit& unit)
									string getWatchMode(Unit& unit)
									void applyAnimationSpeed(Unit& unit, double speed)
									double getAnimationSpeed(Unit& unit)
									ControllableObject getLockedTarget(Unit& unit)
									void applyDrawFrustrum(Unit& unit, int mode, double size, Color_RGBA color)
									void applyDrawFrustrum(Unit& unit, int mode, double size)
									DRAWFRUSTRUM getDrawFrustrum(Unit& unit)
									void applyDrawStationary(Unit& unit, int drawType, double maxSize, double growRate, Color_RGBA color, double offset = 0.1)
									void applyDrawStationary(Unit& unit, int drawType, double maxSize = 2, double growRate = 0.03)
									vector<double> getHeadDirection(Unit& unit)
									double getAzimuthHeadDirection(Unit& unit)
									double getElevationHeadDirection(Unit& unit)
									double getRollHeadDirection(Unit& unit)
									void applyLightMode(Unit& unit, int mode)
									bool isDisableAnimationMove(Unit& unit)
									void disableAnimationMove(Unit& unit, bool disable)
									void applyMinAnimationSpeed(Unit& unit, double percent)
									void applyFaceAnimation(Unit& unit, double blink)
									void applyFace(Unit& unit, FACETYPE face)
									void applyMimic(Unit& unit, MIMIC mimic)
									void ignoreRoads(Unit& unit, bool mode)
									void applyUnitCombatMode(Unit& unit, WAYPOINTCOMBATMODE mode)
									void commandWatch(Unit& unit, ControllableObject& target)
									void commandWatch(Unit& unit, position3D& target)
									void doFollow(ControllableObject& co, Group& grp)
									void moveToPos(Unit& unit, position3D& position)
									void glanceAt(Unit& unit, position3D& position)
									void LookAt(Unit& unit, position3D& position)
									void addWeaponToCargo(Unit& unit, string weaponName, int count)
									vector<WEAPONCARGO> getWeaponFromCargo(Unit& unit)
									void addMagazineToCargo(Unit& unit, string magName, int count)
									vector<MAGAZINECARGO> getMagazineFromCargo(Unit& unit)
									vector<string> getMuzzles(Unit& unit)
									position3D getAimingPosition(Unit& unit, ControllableObject& target)
	2.11		26-09/2011	NDB:	Added Method void commandMove(Unit& unit, position3D position)
	2.12		28-09/2011	NDB:  Added Methods,
									void addRating(Unit& unit, double rating)
									double getRating(Unit& unit)
									void addScore(Unit& unit, int score)
									int getScore(Unit& unit)
									void addLiveStats(Unit& unit, double score)
									bool canStand(Unit& unit)
									double getHandsHit(Unit& unit)
									bool isFleeing(Unit& unit)
									void allowFleeing(Unit& unit, double cowardice)
									bool hasSomeAmmo(Unit& unit)
									string getUnitBehaviour(Unit& unit)
									string getUnitCombatMode(Unit& unit)
	2.13		04-11/2011	NDB: Added Methods,
									bool isKindOf(Unit& unit, string typeName)
									void applyCaptive(Unit& unit, bool captive)
									void applyCaptive(Unit& unit, int captiveNum)
									int getCaptiveNumber(Unit& unit)
									float knowsAbout(Unit& unit, ControllableObject& target)
									void sideChat(Unit& unit, string chatText)
									void groupChat(Unit& unit, string chatText)
	2.13		07-11-2011	SSD		void applyName(Unit& unit)
	2.14		21-11-2011	DMB		void stop(Unit& unit, bool stop)
	2.15		19-01-2012  RDJ		void allowGetIn(vector<Unit> unitList, bool val);
	2.16		24-01-2012	RDJ		void unAssignVehicle(Unit& unit)
	2.17		24-01-2012	RDJ		void hideBody(Unit& unit)



/************************************************************************/

#ifndef VBS2FUSION_UNIT_UTILITIES_H
#define VBS2FUSION_UNIT_UTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>
#include <time.h>

// SIMCENTRIC INCLUDES
#include "position3D.h"
#include "VBS2Fusion.h"
#include "data/Unit.h"
#include "util/ExecutionUtilities.h"
#include "util/ControllableObjectUtilities.h"
#include "VBS2FusionAppContext.h"
#include "data/vbs2Types.h"
#include "data/UnitArmedModule.h"
#include "data/Target.h"


/**********************************************************************/
/* END INCLUDES
/**********************************************************************/

namespace VBS2Fusion
{	

	class VBS2FUSION_API UnitUtilities : public ControllableObjectUtilities
	{
	public:

		//********************************************************
		// DisableAI utilities
		//********************************************************

		/*!
		Disables "MOVE" AI for the unit
		deprecated. Use void applyAIMovementDisable(Unit& u).
		*/
		static void disableAIMOVE(Unit& u);
		
		/*!
		Disables "PATHPLAN" AI for the unit
		deprecated. Use void applyAIPATHPLANINGDisable(Unit& u).
		*/
		static void disableAIPATHPLAN(Unit& u);

		/*!
		Enables "MOVE" AI for the unit
		deprecated. Use void applyAIMovementEnable(Unit& u).
		*/
		static void enableAIMOVE(Unit& u);
		
		/*!
		Enables "PATHPLAN" AI for the unit
		deprecated. Use void applyAIPATHPLANINGEnable(Unit& u);.
		*/
		static void enableAIPATHPLAN(Unit& u);

		//********************************************************
		// Unit weapon utilities
		//********************************************************
		/*!
		Reload weapons of the unit. Reload happens when the current magazine of the weapon is empty.
		Deprecated. Use void applyWeaponReload(Unit& u).
		*/
		static void reloadWeapons(Unit& u);


		//********************************************************
		// chat utilities
		//********************************************************

		/*!
		Send the text message over the global radio channel.
		Deprecated. Use void applyGlobalChat(Unit& u,string txtMessage )
		*/
		static void globalChat(Unit& u,string txtMessage );


		//********************************************************
		// Firing utilities
		//********************************************************

		/*!
		Orders a unit to commence firing on the given target (silently). 
		If the target is objNull, the unit is ordered to commence firing on its current target (using doTarget).
		Deprecated. Use applyFire(Unit& firingUnit, ControllableObject& target)
		*/
		static void doFire(Unit& firingUnit, ControllableObject& target);

		/*!
		Orders the unit (silently) to fire at the given position. Please note that the 
		position needs to be given in PositionASL format. 
		Deprecated. Use applyFire(Unit& firingUnit, position3D pos)
		*/
		static void doFire(Unit& firingUnit, position3D pos);


		//********************************************************
		// unit behavior utilities
		//********************************************************

		/*!
		set the behavior of the unit. 
		CARELESS,SAFE,AWARE,COMBAT,STEALTH
		Deprecated. Use void applyBehaviour(Unit& unit, BEHAVIOUR behaviour)
		*/
		static void setUnitBehaviour(Unit& unit, BEHAVIOUR behaviour);

		//********************************************************
		// Rank utilities
		//********************************************************

		/*!
		Returns the Rank of the unit in string format. 
		*/
		static string getRank(Unit& u);

		/*!
		Updates the rank of the unit. 
		*/
		static void updateRank(Unit& u);

		/*!
		Applies the rank specified by u.Rank() to the unit. 
		*/
		static void applyRank(Unit& u);

		/*!
		Applies the rank specified by rank to the unit. 
		*/
		static void applyRank(Unit& u, string rank);


		//********************************************************
		// Skill utilities
		//********************************************************

		/*!
		Returns the skill level of the unit. 
		*/
		static double getSkill(Unit& u);

		/*!
		Updates the skill level of the unit.  
		*/
		static void updateSkill(Unit& u);

		/*!
		Applies the skill level specified by u.getSkill() to the unit. 
		*/
		static void applySkill(Unit& u);

		/*!
		Applies the skill level specified by skill to the unit. 
		Does not save changes. Use update to verify changes. 
		*/
		static void applySkill(Unit& u, double skill);


		//********************************************************
		// Group utilities
		//********************************************************

		/*!
		Returns the VBS2 name of the group the unit belongs to. 
		*/
		static string getGroup(Unit& u);

		/*!
		Updates the name of the group the unit belongs to. 
		*/
		static void updateGroup(Unit& u);		


		//********************************************************
		// Endurance utilities
		//********************************************************

		/*!
		Returns the endurance of the unit. 
		*/
		static double getEndurance(Unit& u);

		/*!
		Updates the endurance of the unit.  
		*/
		static void updateEndurance(Unit& u);

		/*!
		Applies the endurance specified by u.getEndurance() to the unit. 
		*/
		static void applyEndurance(Unit& u);

		/*!
		Applies the endurance specified by endurance to the unit. 
		Does not save changes. Use update to verify changes. 
		*/
		static void applyEndurance(Unit& u, double endurance);

		//********************************************************
		// Experience utilities
		//********************************************************

		/*!
		Returns the experience level of the unit. 
		*/
		static double getExperience(Unit& u);

		/*!
		Updates the experience level of the unit. 
		*/
		static void updateExperience(Unit& u);

		/*!
		Applies the experience level obtained through u.getExperience to the unit. 
		*/
		static void applyExperience(Unit& u);

		/*!
		Applies the experience level obtained through experience to the unit. 
		Does not save changes. Use update to verify changes. 
		*/
		static void applyExperience(Unit& u, double experience);


		//********************************************************
		// Fatigue utilities
		//********************************************************

		/*!
		Returns the fatigue level of the unit. 
		*/
		static double getFatigue(Unit& u);

		/*!
		Updates the fatigue level of the unit. 
		*/
		static void updateFatigue(Unit& u);

		/*!
		Applies the fatigue level specified by u.getFatigue() to the unit. 
		*/
		static void applyFatigue(Unit& u);

		/*!
		Applies the fatigue level specified by fatigue to the unit. 
		Does not save changes. Use update to verify changes. 
		*/
		static void applyFatigue(Unit& u, double fatigue);



		//********************************************************
		// Leadership utilities
		//********************************************************

		/*!
		Returns the leadership level of the unit. 
		*/
		static double getLeadership(Unit& u);

		/*!
		Updates the leadership level of the unit. 
		*/
		static void updateLeadership(Unit& u);

		/*!
		Applies the leadership level obtained through u.getLeadership to the unit. 
		*/
		static void applyLeadership(Unit& u);

		/*!
		Applies the leadership level obtained through leadership to the unit. 
		Does not save changes. Use update to verify changes. 
		*/
		static void applyLeadership(Unit& u, double leadership);




		//********************************************************
		// Create Unit
		//********************************************************

		/*!
		Creates a new unit. The following parameters are used in the 
		creation process: Type, Position, Initialization Statements, 
		Skill, Rank. If these parameters are not specified, default parameters
		used in the initialization of an Unit object are used. 

		if the group specified does not exist, or if no group is specified, a 
		new group is created to assign to the unit. 
		*/
		static void CreateUnit(Unit& u);

		/*!
		Creates a new unit at the specified position. The following parameters are used in the 
		creation process: Type, Initialization Statements, 
		Skill, Rank. If these parameters are not specified, default parameters
		used in the initialization of an Unit object are used. 

		if the group specified does not exist, or if no group is specified, a 
		new group is created to assign to the unit. 
		*/

		static void CreateUnit(Unit& u, position3D position);

		/*!
		Creates a new unit with the specified alias. The following parameters are used in the 
		creation process: Type, Initialization Statements, Position
		Skill, Rank. If these parameters are not specified, default parameters
		used in the initialization of an Unit object are used. 
		
		if the group specified does not exist, or if no group is specified, a 
		new group is created to assign to the unit. 
		*/
		static void CreateUnit(Unit& u, string strAlias);

		/*!
		Creates a new unit with the specified alias at position. The following parameters are used in the 
		creation process: Type, Initialization Statements, 
		Skill, Rank. If these parameters are not specified, default parameters
		used in the initialization of an Unit object are used. 

		if the group specified does not exist, or if no group is specified, a 
		new group is created to assign to the unit. 
		*/

		static void CreateUnit(Unit& u, position3D position, string strAlias);

		/*!
		Creates a new unit of type with the specified alias. The following parameters are used in the 
		creation process: Type, Initialization Statements, position, 
		Skill, Rank. If these parameters are not specified, default parameters
		used in the initialization of an Unit object are used. 
		
		if the group specified does not exist, or if no group is specified, a 
		new group is created to assign to the unit. 
		*/
		static void CreateUnit(Unit& u, string strAlias, string type);

		/*!
		Creates a new unit using the parameters pass in. 

		if the group specified does not exist, or if no group is specified, a 
		new group is created to assign to the unit. 
		*/

		static void CreateUnit(Unit& u, string strAlias, string type, position3D position, string initStatements, double skill, string rank, string group);

		/*!
		Creates a new unit using the parameters pass in. 

		if the group specified does not exist, or if no group is specified, a 
		new group is created to assign to the unit. 
		*/
		static void CreateUnit(	Unit& u,string type,position3D position,string markers,double placementRadius,string specialProperties,string group);


		static void UnitUtilities::CreateUnit(Unit& u,Group& _group);
 


		//********************************************************
		// Update Utilities
		//********************************************************

		/*!
		Updates the static properties (i.e. rarely changing) of the unit: 
		Endurance, Experience, Group, Leadership, Rank, Skill and Type.  
		*/
		static void updateStaticProperties(Unit& u);

		/*!
		Updates the dynamic properties of the unit: 
		fatigue, damage, direction, position, isAlive, isPlayer, positionASL 	
		*/
		static void updateDynamicProperties(Unit& u);

		//********************************************************
		// Group existence check Utility
		//********************************************************		 

		/*!
		Returns true if a group with the name/alias exists. 
		Deprecated. Use bool isGroupExists(string groupName)
		*/

		static bool groupExists(string groupName);

		//********************************************************
		// Group creation Utility
		//********************************************************

		/*!
		Creates a new group with the name/alias specified. Default side of "WEST" is used. 
		*/
		static void createGroup(string groupName, SIDE side = WEST);

		//********************************************************
		// Movement Utility
		//********************************************************

		/*!
		order unit to stop. 
		deprecated. Use	- void applyCommandStop (Unit& u)
		*/
		static void commandStop(Unit& u);
		
		/*!
		Orders the unit to move to movePosition. 
		deprecated. Use	- void applyMove(Unit& u, position3D movePosition)
		*/
		static void doMove(Unit& u, position3D movePosition);

		//********************************************************
		// forceSpeed Utility
		//********************************************************

		/*!
		Forces the speed of the unit. 
		*/
		//static void forceSpeed(Unit& unit, double forceSpeedVal);			

		//********************************************************
		// Targeting utilities
		//********************************************************

		/*!
		Orders the unit to target the given target (silently).
		Deprecated. Use - applyTarget(Unit& unit, ControllableObject& co)
		*/
		static void doTarget(Unit& unit, ControllableObject& co);

		//********************************************************
		// Watch utilities
		//********************************************************

		/*!
		Orders the unit to watch the given position (format position3D) (silently).
		Deprecated. Use - applyWatch(Unit& unit, position3D targetPosition)
		*/
		static void doWatch(Unit& unit, position3D targetPosition);


		//********************************************************
		// UnitPos (i.e. UP, DOWN or AUTO) utilities
		//********************************************************

		/*!
		Returns the units current position in UNITPOS (i.e AUTO, UP or DOWN) format. 
		*/
		static UNITPOS getUnitPos(Unit& u);

		/*!
		Updates the units Position property. 
		*/
		static void updateUnitPos(Unit& u);

		/*!
		Applies the UNITPOS defined by pos to the Unit in the game environment. Does not update the 
		UnitPos property of the unit. Use updateUnitPos to reload and check for successful execution. 
		*/
		static void applyUnitPos(Unit& u, UNITPOS pos);

		/*!
		Applies the UNITPOS defined by u.getUnitPos() to the Unit in the game environment. Does not update the 
		UnitPos property of the unit. Use updateUnitPos to reload and check for successful execution. 
		*/
		static void applyUnitPos(Unit& u);

		/*!
		Applies the AUTO position to the unit within the game. Does not update the 
		UnitPos property of the unit. Use updateUnitPos to reload and check for successful execution.
		Deprecated - use void applyUnitPosAUTO(Unit& u);
		*/
		static void unitPosAUTO(Unit& u);

		/*!
		Applies the DOWN position to the unit within the game. Does not update the 
		UnitPos property of the unit. Use updateUnitPos to reload and check for successful execution.
		Deprecated - use void applyUnitPosDOWN(Unit& u)
		*/
		static void unitPosDOWN(Unit& u);

		/*!
		Applies the UP position to the unit within the game. Does not update the 
		UnitPos property of the unit. Use updateUnitPos to reload and check for successful execution. 
		Deprecated. Use void applyUnitPosUP(Unit& u)
		*/
		VBS2FUSION_DPR(UUNT001) static void unitPosUP(Unit& u);

		/*!
		Applies the alias of the unit in VBS2 Environment.	
		*/
		static void applyAlias(Unit& u);

		/*!
		Enable/Disable precision movement mode for specified unit. 
		Deprecated - Use applyUnitExactMovementMode(Unit& u, bool precisionMode)
		*/
		static void setUnitExactMovementMode(Unit& u, bool precisionMode);

		/*!
		Return precision movement mode for specified unit.
		*/
		static bool getUnitExactMovementMode(Unit& u);

		/*!
		Delete a Unit from VBS2 Environment. 
		note - Only player unit will be visible after delete.
		*/
		static void deleteUnit(Unit& u);

		/*!
		Causes a unit to salute to a specified target unit.
		Deprecated - Use void applySalutePerform(Unit& unit, Unit& targetUnit)
		*/
		static void performSalute(Unit& unit, Unit& targetUnit);

		/*!
		set body height of the unit. height should be in meters.
		Deprecated - use void applyHeight(Unit& u , double height))
		*/
		static void setHeight(Unit& u , double height);

		/*!
		get body height of the specified unit. 
		*/
		static double getHeight(Unit& u);

		/*!
		set Body Mass Index (BMI) of the unit.
		Deprecated - Use void applyBMI(Unit& u, double index )
		*/
		static void setBMI(Unit& u, double index );

		/*!
		get Body Mass Index (BMI) of the unit.
		*/
		static double getBMI(Unit& u);


#if WEAPON_HANDLING
		/*!
		Disable gunner input of the unit.
		input Modes are
			NOTHING         - Enable disabled modes.
			WEAPON			- Disable weapon.
			HEAD			- Disable head.
			WEAPONANDHEAD	- Disable weapon and head.
			Depreciated - Use applyGunnerInputDisable(Unit& unit,GUNNERINPUT_MODE mode);
		*/
		static void disableGunnerInput(Unit& unit,GUNNERINPUT_MODE mode);

#endif

		/*!
		Set the direction of the unit's head.
		Directions are relative to the unit's direction. 
		*/
		static void applyHeadDirection(Unit& unit, double azimuth, double elevation,double roll, bool transition = true);

#if WEAPON_HANDLING
		/*!
		Returns the name of the Unit's primary weapon (an empty string if there is none).	
		*/
		static string getPrimaryWeapon(Unit& u);

		/*!
		Updates the primary weapon of the Unit. 
		*/
		static void updatePrimaryWeapon(Unit& u);

		/*
		Updates the list of weapon types assigned to the unit. 
		Each weapon is assigned with ammo value of 0. 

		Note: This function does not update the ammo amounts on the weapons magazines. 
		Use updateWeaponAmmo function to update the weapon types with ammo.
		*/
		static void updateWeaponTypes(Unit& u);

		/*!
		Select the Weapon given by the muzzle Name.
		Deprecated - use applyWeaponSelect(Unit& u, string muzzleName)
		*/
		static void selectWeapon(Unit& u, string muzzleName);

		/*
		Updates the amount of ammo left in the primary magazine of each assigned 
		weapon type. Uses the list defined by the weapons list within UnitArmedModule, 
		and therefore will only load ammo information for each weapon on that list. 
		Should be used after updateWeaponTypes is called. 	
		*/
		static void updateWeaponAmmo(Unit& u);

		/*!
		Add magazine to the Unit.
		Deprecated - use applyMagazineAdd(Unit& u, string name)
		*/
		static void addMagazine(Unit& u, string name);

		/*!
		Count number of magazines available in given unit object.
		Deprecated - use getMagazinesCount(Unit& u)
		*/
		static int countMagazines(Unit& u);

		/*!
		Updates the list of magazines assigned to a unit's in Armed Module. This function loads all 
		magazines assigned to the unit from VBS2 and adds a new Magazine object to Magazine list.

		Note: This function does not update the ammo amount within magazine list. 
		So within MagazineList ammo count would be zero.Use updateMagazineAmmo to update this list.
		*/
		static void updateMagazineTypes(Unit& u);

		/*!
		Updates the magazine ammo count & magazine name inside the unitArmedModule's MagazineList
		therefore update the magazine list with required details. This function is enough to update
		MagazineList correctly. No need to use updateMagazineTypes function before using this function.
		*/
		static void updateMagazineAmmo(Unit& u);
		
		/*!
		Returns the Weapon Object with Current weapon details.	
		*/
		static Weapon getCurrentWeapon(Unit& u);
		
		/*!
		Gets the Weapon Object from getCurrentWeapon function & update the Weapon list.	
		*/
		static void updateCurrentWeapon(Unit& u);

		//----------------------------------------------------------------------------
		/*!
		Gets the pistol weapon name.	
		*/
		static string getPistolWeapon(Unit& u); 

		/*!
		Returns the weapons position in model space by passing Weapon Name.	
		*/
		static position3D getWeaponPosition(Unit& unit, string weaponName);

		/*!
		Returns the weapons position in model space by passing weapon object. Weapon should have correct name.
		getWeaponPosition(Unit& unit, string weaponName) function will be used within this function.
		*/
		static position3D getWeaponPosition(Unit& unit, Weapon& weapon);

		/*!
		Returns the weapon firing point for a selected muzzle in model space. Muzzle name needs to be passed.	
		*/
		static position3D getWeaponPoint(Unit& unit, string muzzleName);

		/*!
		Returns the weapon firing point in model space by passing Weapon object. Weapon should have correct 
		muzzle name. getWeaponPoint(Unit& unit, string muzzleName) function will be used within this function.
		*/
		static position3D getWeaponPoint(Unit& unit, Weapon& weapon);

		/*!
		Returns the direction that the unit Weapon is aiming in. weapon name is passed.	
		*/
		static position3D getWeaponDirection(Unit& unit, string weaponName);

		/*!
		Returns the direction that the unit Weapon is aiming in. Weapon object should have correct weapon name.
		getWeaponDirection(Unit& unit, string weaponName) function will be used within this function.
		*/
		static position3D getWeaponDirection(Unit& unit, Weapon& weapon);

		/*! 
		Get the elevation of the weapon in unit. 
		*/
		static double getWeaponElevation(Unit& unit, string weaponName);

		/*! 
		Get the elevation of the weapon in unit. Weapon object should have correct weapon name.
		This function uses getWeaponElevation(Unit& unit, string weaponName).
		*/
		static double getWeaponElevation(Unit& unit, Weapon& weapon);

		/*! 
		Get the Azimuth of the unit's weapon in VBS environment. Azimuth value will not be relative to 
		the unit's body direction.
		*/		
		static double getWeaponAzimuth(Unit& unit, string weaponName);

		/*! 
		Get the Azimuth of the unit's weapon in VBS environment. Azimuth value will not be relative to 
		the unit's body direction. This function uses getWeaponAzimuth(Unit& unit, string weaponName).
		So Weapon object should have correct weapon name.
		*/		
		static double getWeaponAzimuth(Unit& unit, Weapon& weapon);
		
		/*!
		Returns the list of objects the weapon is aiming at.
		*/
		static list<ControllableObject> getWeaponAimingTargets(Unit& unit, string weaponName);

		/*!
		Returns the list of objects the weapon is aiming at. Weapon object should have correct weapon name.
		*/
		static list<ControllableObject> getWeaponAimingTargets(Unit& unit, Weapon& weapon);

		/*!
		Remove the one specified magazine from the unit.
		Deprecated - use void applyMagazineRemove(Unit& unit, string name)
		*/
		static void removeMagazine(Unit& unit, string name);

		/*!
		Remove all specified magazines from the unit.
		Deprecated - use void applyMagazineRemoveAll(Unit& unit, string name)
		*/
		static void removeallMagazines(Unit& unit, string name);

		/*!
		Add Weapon to the Unit. weapon slots are filled, any further addWeapon commands are ignored. 
		To ensure that the weapon is loaded at the start of the mission, at least one magazine should 
		be added, before adding threse weapon.
		Deprecated - use applyWeaponAdd(Unit& unit, string name)
		*/
		static void addWeapon(Unit& unit, string name);

		/*!
		Remove the specified Weapon from the unit.
		Deprecated - use applyWeaponRemove(Unit& unit, string name)
		*/
		static void removeWeapon(Unit& unit, string name);

		/*!
		Tells if a unit has the given weapon.
		Deprecated - use bool isWeaponHaving(Unit& unit, string name)
		*/
		static bool hasWeapon(Unit& unit, string name);
		
		/*!
		Sets the direction of the Weapon according to azimuth (relative to the unit's body direction)& elevation. 
		Must use the disableGunnerInput function before using this function.
		Deprecated - Use  void applyWeaponDirection(Unit& unit, double azimuth, double elevation, bool transition);
		*/
		static void setWeaponDirection(Unit& unit, double azimuth, double elevation, bool transition);

		/*!
		Returns how much ammunition (compared to a full state defined by the unit type) the unit has. 
		Value would be between 0 to 1.	
		*/
		static float getAmmoRatio(Unit& unit);

		/*!
		Sets how much ammunition (compared to a full state defined by the unit type) the unit has. 
		The value ranges from 0 to 1.	
		Deprecated - applyVehicleAmmo(Unit& unit, float ammovalue)
		*/
		static void setVehicleAmmo(Unit& unit, float ammovalue);

		/*!
		Returns the recoil impulse vectors of a soldier's weapon.Value is only available the moment 
		the weapon is actually firing	
		*/
		static vector3D getRecoilImpulse(Unit& unit);

		/*!
		Remove all weapons and magazines from the unit.
		Deprecated. Use  void applyWeaponRemoveAll(Unit& unit)
		*/
		static void removeallWeapons(Unit& unit);

		/*!
		Enables or Disables firing on current weapon. If true, the unit's current weapon cannot be fired.
		Deprecated - Use void applyWeaponSafety(Unit& unit, bool safe)
		*/
		static void setWeaponSafety(Unit& unit, bool safe);

		/*!
		Return the current status of the weapon safety.
		*/
		static bool getWeaponSafety(Unit& unit);

		/*!
		Updates all the Armed properties of the unit:   
		*/
		static void updateUnitArmedProperties(Unit& unit);

#endif

		/*!
		Returns the hiding Position.
		If enemy is null it is some position in front of the object or enemy position
		unit - The unit need to hide
		enemy - The enemy unit
		*/
		static position3D getHideFrom(Unit& unit, Unit& enemy);

		/*!
		Returns a list of targets within the defined range.
		"Targets" are not restricted to enemy units.
		Deprecated - use vector<Target> getNearTargets(Unit& unit, int range)
		*/
		static vector<Target> nearTargets(Unit& unit, int range);
	
		/*!
		Returns a list of targets within the defined range.
		"Targets" are not restricted to enemy units. 
		*/
		static string* nearTarget(Unit& unit, int range);	

		/*!
		Returns true if the given object is controlled by a player somewhere on the network.
		*/		
		static bool isPlayerControlled(Unit u);

		/*!
		Returns the aerobic fatigue level of the soldier. 
		*/
		static double getAerobicFatigue(Unit& unit);

		/*!
		Returns the anaerobic fatigue level of the soldier. 
		*/
		static double getAnaerobicFatigue(Unit& unit);

		/*!
		Returns the morale level of the unit [0-1]
		*/
		static double getMoraleLevel(Unit& unit);

		/*!
		Returns the long-term (1 minute average) morale level of the unit [0-1].
		*/
		static double getMoraleLongTerm(Unit& unit);

		/*!
		Returns the training rating of the soldier. When used on a vehicle, it will return -1.
		*/
		static double getTraining(Unit& unit);

		/*!
		Returns the "up degree" of the soldier. This indicates whether the unit is prone, kneeling, or standing, as well as what weapon type is being used.
		Below is a list of stances and their return values:

		rifle, lowered - standing/sitting: 10
		rifle, raised - standing: 8
		rifle - crouched: 6  
		rifle - prone: 4

		pistol - standing: 9 
		pistol - crouched: 7  
		pistol - prone: 5 

		launcher - standing: 1
		launcher - crouched: 1 
		launcher - prone: 3

		unarmed - standing/sitting: 12
		unarmed - prone: 3

		vehicles / static objects / non people: -1
		*/
		static int getUpDegree(Unit& unit);

		/*!
		Enables personal items. Used for units inside vehicles to enable their personal equipment.
		Deprecated - Use applyPersonalItemsEnable(Unit& unit, bool enable)
		*/
		static void EnablePersonalItems(Unit& unit, bool enable);

		/*!
		Returns true if the first object is looking into the direction of the second object 
		(within the angle specified). Whether there are objects between the "looker" and the 
		target object that might be obstructing the view is not taken into consideration.

		unit - Unit that is looking
		co - Object that is being looked at
		angle - Angle within which the target has to be.
		Angle is considered in both directions - i.e. if the object is 29� left or right of the object, an angle definition of 30 will get either.
		*/
		static bool isLookingAt(Unit& unit, ControllableObject& co, double angle);

		/*!
		Returns whether a unit is able to audibly speak or not.
		*/
		static bool isSpeakable(Unit& unit);
		/*!
		Turns the ability of a unit to audibly speak on or off.

		Does not affect orders from being issues or received.
		Units will still scream when being shot at.
		Deprecated - Use void  applySpeechEnable(Unit& unit, bool speech) 
		*/
		static void allowSpeech(Unit& unit, bool speech);

		/*!
		It is for VBS 1.50
		In VBS2 1.50 amputations are present for all US Army and USMC soldiers
		Removes a person's body parts.

		Proportional to the removed part there will also be blood stains applied to the unit.

		Arms and legs can be removed in two stages.
		Possible values for the removed body parts are:

		"Arm_Left_Half": Remove left hand
		"Arm_Right_Half": Remove right hand
		"Leg_Left_Half": Remove lower left leg
		"Leg_Right_Half": Remove lower right leg
		"Head": Remove head
		"Arm_Left_Full": Remove left arm
		"Arm_Right_Full": Remove right arm
		"Leg_Left_Full": Remove left leg
		"Leg_Right_Full": Remove right leg
		"Head_Reset": Re-attach head
		"Arm_Left_Reset": Re-attach left arm
		"Arm_Right_Reset": Re-attach right arm
		"Leg_Left_Reset": Re-attach left leg
		"Leg_Right_Reset": Re-attach right leg 

		Removal of body parts has no effect on the health status of that unit (i.e. a person without a head is still alive, and a person without legs can still walk), 
		so it's up to the mission designer to ensure that these effects are applied in a realistic fashion.

		unit - 
		bodypart - Part to be removed.
		*/
		static void applyAmputation(Unit& unit, BODYPART bodyPart);

		/*!
		It is for VBS 1.40
		It applicable for the US Army rifleman unit.
		unit - 
		bodypart - Main body part. Can be "head", "leftarm", "rightarm", "leftleg" or "rightleg"
		segment - Segment of specified body part. Can be "head", "lefthand", "leftforearm", "righthand", "rightforearm", "leftfoot", "leftuplegroll", "rightfoot", "rightuplegroll"
		*/
		static void applyAmputation(Unit& unit, MAIN_BODYPART bodyPart, BODY_SEGMENT segment);

		/*!
		Gets person limb amputation state
		*/
		static string getAmputation(Unit& unit, BODYPART bodyPart);


		/*!
		Adds the passed value to the unit's morale (morale is between 0=exhausted and 1=fit). 
		moralChange - It is the adding value. use (-)ve to reduce the morale
		Deprecated - Use applyMoralAddition(Unit& unit, double moralChange)
		*/
		static void addToMorale(Unit& unit, double moralChange);

		/*!
		Adds the passed value to the unit's anaerobic fatigue (fatigue is between 0 and 1). 
		moralChange - It is the adding value. use (-)ve to reduce the morale
		Deprecated - Use applyAnaerobicFatigueAddition(Unit& unit, double AnaerobicFatigueChange)
		*/
		static void addToAnaerobicFatigue(Unit& unit, double AnaerobicFatigueChange);

		/*!
		Adds the passed value to the unit's aerobic fatigue (fatigue is between 0 and 1). 
		moralChange - It is the adding value. use (-)ve to reduce the morale
		Deprecated - Use applyAerobicFatigueAddition(Unit& unit, double AerobicFatigueChange);
		*/
		static void addToAerobicFatigue(Unit& unit, double AerobicFatigueChange);

		/*!
		Return the vehicle a unit is assigned to.
		If no vehicle is assigned objNull is returned. 
		*/
		static Vehicle getAssignedVehicle(Unit& unit);

		/*!
		Creates a unit of the given type. Unit is only visible on the local machine.
		*/
		static void CreateUnitLocal(string type, position3D& position,Group& group, string initString = "", double skill = 0.5, string rank = "PRIVATE");

	 /*!
	 Order the given unit(s) to stop (without radio messages).
	 DoStop'ed units leave the groups formation. It will prevent the unit from moving around with their group 
	 (or formation leader), while still beeing able to turn around and even move to a new position if they see fit. 
	 They will still respond to orders from their group leader (like engage, rearm, board a vehicle), but all of their 
	 actions will be seperate from the group formation (unless ordered to return, which AI leaders don't do unless a script tells them to).
	 Deprecated - use applyStop(Unit& unit)
	 */
	 static void StopUnit(Unit& unit);

	 /*!
	 Returns true if the given unit is currently wearing NVG equipment.
	 */
	 static bool isWearingNVG(Unit& unit);

	 /*!
	 Returns true if personal items are enabled for the unit 
	 */
	 static bool isPersonalItemsEnabled(Unit& unit);

	 /*!
	 Returns the watch mode of the given unit.
	 "NO" if nothing (or a position) is being watched, "TGT" if watching target
	 */
	 static string getWatchMode(Unit& unit);
	
	 /*!
	 Set the speed multiplier that defines the speed at which an animation is seen in game. 
	 Changes between anim states remains the same, i.e. the unit continues to be able to change 
	 from walking to running and so forth. Note that this also depends on the setMinAnimationSpeed command, if set.
	 */
	 static void applyAnimationSpeed(Unit& unit, double speed);

	 /*!
	 Returns the speed multiplier that defines the speed at which an animation is seen in game. 
	 */
	 static double getAnimationSpeed(Unit& unit);

#ifdef DEVELOPMENT
	 /*!
	 Returns the currently locked target object on players HUD or target assigned to AI. 
	 If the unit is a gunner in a vehicle, target currently locked by the turret gunner is controlling is returned. 
	 */
	 static ControllableObject getLockedTarget(Unit& unit);

#endif

	 /*!
	 Draws a visualization in the editor, indicating a units view cone.
	 unit - Unit to draw frustum for
	 mode - Where to draw indicator: 0:none, 1:2D map, 2:3D map, 3:both
	 size - Size of cone in meters.
	 color - Color of cone. (Optional)
	 */
	 static void applyDrawFrustrum(Unit& unit, int mode, double size, Color_RGBA color);

	 /*!
	 Draws a visualization in the editor, indicating a units view cone.
	 unit - Unit to draw frustum for
	 mode - Where to draw indicator: 0:none, 1:2D map, 2:3D map, 3:both
	 size - Size of cone in meters.
	 */
	 static void applyDrawFrustrum(Unit& unit, int mode, double size);

	 /*!
	 Returns the current drawFrustum setting for a unit.
	 (returns [0,100,[0,0,1,0.1]] if not defined)
	 */
	 static DRAWFRUSTRUM getDrawFrustrum(Unit& unit);

	 /*!
	 Draws ink-spots in AAR to indicate the time a unit is stationary.

	 unit: Object - Unit for which inkspot is being defined.
	 drawType: Number - Type of marker to draw, options can be added together ex: ( 1 + 2 + 4 ). Possible values are: 
	 0: no drawing
	 +1: 2D drawing ON
	 +2: 3D drawing ON (disc by default)
	 +4: switch 3D drawing to a cloud (instead of disc, 2 must be set to be visible)

	 maxSize - Maximum size of the mark in meters 
	 growRate - Growth rate of the mark in meters per second
	 color - 2D and 3D Marker color 
	 offset - Offsets the default 3D disc representation above ground by the given value in meters (optional, default: 0.1)
	 */
	 static void applyDrawStationary(Unit& unit, int drawType, double maxSize, double growRate, Color_RGBA color, double offset = 0.1);

	 /*!
	 Draws ink-spots in AAR to indicate the time a unit is stationary.

	 unit: Object - Unit for which inkspot is being defined.
	 drawType: Number - Type of marker to draw, options can be added together ex: ( 1 + 2 + 4 ). Possible values are: 
	 0: no drawing
	 +1: 2D drawing ON
	 +2: 3D drawing ON (disc by default)
	 +4: switch 3D drawing to a cloud (instead of disc, 2 must be set to be visible)

	 maxSize - Maximum size of the mark in meters (optional, default: 2)
	 growRate - Growth rate of the mark in meters per second (optional, default: .03)
	 */
	 static void applyDrawStationary(Unit& unit, int drawType, double maxSize = 2, double growRate = 0.03);

	 /*!
	 Returns the direction of a unit's head. 
	 returns array has [Azimuth, Elevation, Roll] of unit's head 
	 */
	 static vector<double> getHeadDirection(Unit& unit);

	 /*!
	 returns Azimuth of unit's head 
	 */
	 static double getAzimuthHeadDirection(Unit& unit);

	 /*!
	 returns Elevation of unit's head 
	 */
	 static double getElevationHeadDirection(Unit& unit);

	 /*!
	 returns Roll of unit's head 
	 */
	 static double getRollHeadDirection(Unit& unit);

#ifdef DEVELOPMENT
	 /*!
	 Set light mode for a unit. 
	 mode - (Day=1, NV=2, TI=4, combinations possible)
	 */
	 static void applyLightMode(Unit& unit, int mode);
#endif

	 /*!
	 Checks whether movement has been disabled for a unit. 
	 */
	 static bool isDisableAnimationMove(Unit& unit);

#ifdef DEVELOPMENT
	 /*!
	 Locks unit into current location. Player can still chance stance, aim and perform other actions, but not move. 
	 If applied to AI units, they will still go through movement animations, but without actually moving. 
	 */
	 static void disableAnimationMove(Unit& unit, bool disable);

#endif

	 /*!
	 Sets the minimum control input that will be used for movement speed.

	 When using a joystick or the setAction command, movement speed is analog (meaning, it can be in a range from no movement to full movement speed). This command puts a minimum on that analog range, so that the input can not be lower than the specified percent.
	 When using a keyboard, control input is always 100%, so this command is not relevant in this situation. 

	 percent - Number, 0 to 1; the minimum analog input that will be accepted for movement.
	 */
	 static void applyMinAnimationSpeed(Unit& unit, double percent);

	 /*!
	 Set facial animation phase (eye blinking), blink is in the range from 0 to 1. 
	 */
	 static void applyFaceAnimation(Unit& unit, double blink);

	 /*!
	 Set person's face. 
	 */	
	 static void applyFace(Unit& unit, FACETYPE face);

	 /*!
	 Set person's facial expression
	 */
	 static void applyMimic(Unit& unit, MIMIC mimic);

	 /*!
	 Revives an Unit immediately, preserving his name & equipment.
	 Deprecated - use applyRevive(Unit& unit)
	 */
	 static void reviveUnit(Unit& unit);

#ifdef DEVELOPMENT
	 /*!
	 Sets unit or group to ignore roads during pathfinding.
	 Vehicles that are set to ignore roads via their configuration are not overridden via a driver that is set to prefer (ignoreRoads=false) them. 
	 */
	 static void ignoreRoads(Unit& unit, bool mode);
#endif

	 /*!
	 
	 @description
	 
	 Sets unit combat mode (engagement rules). Does not work for groups.

	 @locality
	 
	 Locally applied Globally effected.

	 @version [VBS2Fusion v2.07.2]

	 @param unit - A unit that suppose to assign combat mode.
	 @param mode - Mode may be one of:
				 "BLUE" (Never fire)
				 "GREEN" (Hold fire - defend only)
				 "WHITE" (Hold fire, engage at will)
				 "YELLOW" (Open fire)
				 "RED" (Open fire, engage at will) 

	 @return Nothing.

	 @example 

	 @code
	
	 //Unit to be created
	 Unit u;
	
	 //Applied CombatMode GREEN to the unit.
	 UnitUtilities::applyUnitCombatMode(unit1,WAYPOINTCOMBATMODE::GREEN);

	 @endcode

	 @overloaded
	 
	 @related

	 @remarks

	 */
	 static void applyUnitCombatMode(Unit& unit, WAYPOINTCOMBATMODE mode);

	 /*!
	 Orders the unit to watch the given target (via the radio). 
	 Use objNull as the target to order a unit to stop watching a target 

	 deprecated. Use void applyCommandWatch(Unit& unit, ControllableObject& target)
	 */
	 VBS2FUSION_DPR(UUNT002) static void commandWatch(Unit& unit, ControllableObject& target);

	 /*!
	 Orders the unit to watch the given position (via the radio) 

	 deprecated. Use void applyCommandWatch(Unit& unit, position3D& target)
	 */
	 VBS2FUSION_DPR(UUNT003) static void commandWatch(Unit& unit, position3D& target);

#ifdef DEVELOPMENT
	 /*!
	 Order the given unit(s) to follow the given other unit or vehicle (without radio messages).
	 All involved units must belong to the same group. 
	 grp - followers
	 co - leader
	 */

	 static void doFollow(ControllableObject& co, Group& grp);

	 /*!
	 Low level command to person to move to given position. 
	 */
	 static void moveToPos(Unit& unit, position3D& position);

#endif

	 /*!
	 Control what the unit is glancing at (target or Position). How frequently the unit is glancing there depends on behaviour. 

	 Deprecated. Use void applyGlanceAt(Unit& unit, position3D& position)
	 */
	 VBS2FUSION_DPR(UUNT004) static void glanceAt(Unit& unit, position3D& position);

	 /*!
	 Control what the unit is looking at position.

	 Deprecated. Use void applyLookAt(Unit& unit, position3D& position)
	 */
	 VBS2FUSION_DPR(UUNT005) static void LookAt(Unit& unit, position3D& position);

		/*!
		Add weapons to the cargo space of objects, which can be taken out by infantry units. 
		Once the weapon cargo space is filled up, any further addWeaponCargo commands are ignored.
		Deprecated - use void applyWeaponCargo(Unit& unit, string weaponName, int count)
		*/
		static void addWeaponToCargo(Unit& unit, string weaponName, int count);

		/*!
		Returns the weapons in cargo of the object. 
		It is added by addWeaponToCargo() method
		*/
		static vector<WEAPONCARGO> getWeaponFromCargo(Unit& unit);

		/*!
		Add magazines to the cargo space of Objects, which can be taken out by infantry units. 
		Once the magazine cargo space is filled up, any further addMagazineCargo commands are ignored.
		Deprecated - use void applyMagazineCargo(Unit& unit, string magName, int count)
		*/
		static void addMagazineToCargo(Unit& unit, string magName, int count);

		/*!
		Returns the magazines in cargo of the object. 
		It is added via addMagazineToCargo()
		*/
		static vector<MAGAZINECARGO> getMagazineFromCargo(Unit& unit);

		/*!
		Return all the muzzles of a person
		*/
		static vector<string> getMuzzles(Unit& unit);

		
		/*!
		Returns the position a shooter has to aim at to hit a target with the current selected weapon.
		*/
		static position3D getAimingPosition(Unit& unit, ControllableObject& target);

		/*!
		It is only for local objects
		Order the given unit(s) to move to the given location (via the radio). 
		Exactly the same as doMove, except this command displays a radio message. 
		Deprecated - use void applyCommandMove(Unit& unit, position3D position)
		*/
		static void commandMove(Unit& unit, position3D position);

		/*!
		Add a number to the rating of a unit. Negative values can be used to reduce the rating. This command is 
		usually used to reward for completed mission objectives. The rating is given at the end of the mission 
		and is automatically adjusted when killing enemies or friendlies. When the rating gets below zero, the 
		unit is considered "renegade" and is an enemy to everyone.
		Deprecated- use void applyRatingIncrease(Unit& unit, double rating)
		*/
		static void addRating(Unit& unit, double rating);

		/*!
		Check unit rating. Rating is increased for killing enemies, decreased for killing friendlies. Can be changed 
		via addRating by the mission designer. The rating of the player is displayed as the "score" at the end of the 
		mission
		*/
		static double getRating(Unit& unit);

		/*!
		Add a number to the score of a unit. This score is shown in multiplayer in the "I" screen. Negative values will remove from the score
		Deprecated - use void applyScoreIncrease(Unit& unit, int score)
		*/
		static void addScore(Unit& unit, int score);

		/*!
		Returns the person's score. 
		*/
		static int getScore(Unit& unit);
	
#ifdef DEVELOPMENT
		/*!
		Adds score to the Xbox Live Statistics score for the given unit (or the commander unit of the given vehicle). 
		(Also available in OFPE & VBS2) 
		*/
		static void addLiveStats(Unit& unit, double score);

#endif

		/*!
		Returns if the given soldier is able to stand up. 
		
		*/
		static bool canStand(Unit& unit);

		/*!
		Checks if a soldier's hands are hit, which results in inaccurate aiming. 
		If the hands aren't damaged, the returned value is 0. If the hands are hit or the unit is dead, the returned value is 1. 
		*/
		static double getHandsHit(Unit& unit);

		/*!
		Check if unit has some ammo. 
		Deprecated -  use isAmmoAvailable(Unit& unit)
		*/
		static bool hasSomeAmmo(Unit& unit);

		/*!
		Checks whether the object is (a subtype) of the given type.
		Command works with classes contained in CfgVehicles or CfgAmmo, but not with others (e.g. CfgMagazines or CfgWeapons).
		*/
		static bool isKindOf(Unit& unit, string typeName);

		/*!
		Mark a unit as captive.

		If unit is a vehicle, commander is marked.

		A captive is neutral to everyone, and will not trigger "detected by" conditions for its original side. Using a number instead of a boolean has no further effect on the engine's behavior, but can be used by scripting commands to keep track of the captivity status at a finer resolution (e.g. handcuffed, grouped, etc.)

		The syntax with numbers was introduced in VBS2 v1.19. Boolean syntax existed since v1.0
		*/
		static void applyCaptive(Unit& unit, bool captive);

		/*!
		Mark a unit as captive.

		If unit is a vehicle, commander is marked.

		A captive is neutral to everyone, and will not trigger "detected by" conditions for its original side. Using a number instead of a boolean has no further effect on the engine's behavior, but can be used by scripting commands to keep track of the captivity status at a finer resolution (e.g. handcuffed, grouped, etc.)

		The syntax with numbers was introduced in VBS2 v1.19. Boolean syntax existed since v1.0

		With V1.50+ more captive numbers have become available:

		2 - Capture: Unit receives action to be captured, i.e. to become part of the player's group
		4 - Release: Unit receives action to be released, i.e. to leave the player's group. If the AI was part of a different group before capture, he will not rejoin that group upon release.
		8 - Cuff: Unit receives action to be handcuffed. Adults will enter an animation that shows their hands on their backs, children will not display any special animation.
		16 - Uncuff: Unit receives action to be uncuffed. 

		Combinations of these modes are possible, by adding the base numbers:

		3 - Unit set captive & Capture action available (mode 1 + 2)
		10 - Capture & cuff actions available (mode 2 + 8)
		12 - Release & cuff actions available (mode 4 + 8)
		18 - Capture & uncuff actions available (mode 2 + 16)
		20 - Release & uncuff actions available (mode 4 + 16) 

		Once an action is activated, the captive value may change automatically, i.e. after the "Capture" action is executed the captiveNum value for that unit changes to 4, to indicate that he can now be released.
		*/
		static void applyCaptive(Unit& unit, int captiveNum);

		/*!
		Returns a unit's captivity status, as defined by setCaptive. 
		If a unit's captivity level was set as a Boolean, then the returned number is either 0 (for false) 
		or 1 (for true).
		*/
		static int getCaptiveNumber(Unit& unit);

		/*!
		Check if (and by how much) unit knows about target.
		And returns a number from 0 to 4.
		Deprecated - float getKnowsAbout(Unit& unit, ControllableObject& target)
		*/
		static float knowsAbout(Unit& unit, ControllableObject& target);

		/*!
		Types text to the side radio channel.
		Deprecated - use void applySideChat(Unit& unit, string chatText)
		*/
		static void sideChat(Unit& unit, string chatText);

		/*!
		Make a unit send a text message over the group radio channel. 
		Deprecated - use applyGroupChat(Unit& unit, string chatText)
		*/
		static void groupChat(Unit& unit, string chatText);

		/*!
		Applies the name of the unit to the VBS2 Environment.
		Prior to applying function, verifies that the name is a valid name
		according to following criteria.
		- Name has to start with letter or underscore.
		- Only special characters allowed is underscore.
		- Name should not contain spaces in between.
		- Name should not already exist in the mission.
		*/
		static void applyName(Unit& unit);


		/*!
		Add an action to an object. The action is usable by the object itself, as well as anyone who is within proximity of the object and is looking at it.

		If an action is added to a unit, the action will only be visible when the unit is on foot. If the unit enters a vehicle, the action will not be visible, not to the unit, or to anyone in / near his vehicle.
		unit - Object to assign the action to
		title - The action name which is displayed in the action menu
		filename - File and Path relative to the mission folder. Called when the action is activated. Depending on the file extension (*.sqf/*.sqs) the file is executed with SQF or SQS Syntax

		It returns Index of the added action. Used to remove the action with removeActionMenuItem()
		Deprecated -  use int createActionMenuItem(Unit& unit, string title, string fileName)
		*/
		static int addActionMenuItem(Unit& unit, string title, string fileName);

		/*!
		Remove action with given id index.
		Deprecated - use void deleteActionMenuItem(Unit& unit, int index)
		*/
		static void removeActionMenuItem(Unit& unit, int index);

		/*!
		Returns the formation leader of a given unit. This is often the same as the group leader, but not always, for example in cases when a unit is ordered to follow another unit. 
		*/
		static Unit getFormLeader(Unit& unit);

		/*!
		Return the name of a unit's current primary animation
		*/
		static string getAnimationState(Unit& unit);

		/*!
		Return leader of the formation. 
		*/
		static Unit getFormationLeader(Unit& unit);

		/*!
		Returns true if the specified unit is subgroup leader. 
		*/
		static bool isFormationLeader(Unit& unit);

		/*!
		Return list of units in the formation.
		*/
		static vector<Unit> getFormationMembers(Unit& unit);

		/*!
		Return the direction in degrees of the 'unit' watching in formation. 
		*/
		static float getFormationDirection(Unit& unit);

		/*!
		Reload primary weapon of given unit
		Deprecated - use void applyPrimaryWeaponReload(Unit& unit)
		*/
		static void reloadPrimaryWeapon(Unit& unit);

		/*!
		Return how much unit wants to reload its weapons. range 0 to 1
		1 - Its empty
		0 - Fully loaded
		*/
		static float getNeedReload(Unit& unit);

		/*!
		Search for selection in the object model.

		Returns the current position of a selection in model space (takes into account any animations).
		If a selection does not exist [0,0,0] is returned.

		The model is searched in this order:

		Memory
		Geometry
		FireGeometry
		LandContact
		Hitpoints
		Paths
		SubParts
		ViewGeometry

		selectionName - Possible values are,
			aiming_axis
			aimPoint
			camera
			footstepL
			footstepR
			granat
			granat2
			Head
			head_axis
			lankle
			launcher
			leaning_axis
			LeftFoot
			LeftHand
			LeftHandMiddlel
			lelbow
			lelbow_axis
			lfemur
			lknee
			lknee_axis
			lshoulder
			lwrist
			Neck
			NVG
			Pelvis
			pilot
			rankle
			rearm
			rearm2
			relbow
			relbow_axis
			rfemur
			RightFoot
			RightHand
			RightHandMiddlel
			rknee
			rknee_axis
			rshoulder
			rwrist
			Spine3
			Weapon 
		*/
		static position3D getSelectionPosition(Unit& unit, string selectionName);

		/*!
		Search for selection in the object model.

		Returns the current position of a selection in model space (takes into account any animations).
		If a selection does not exist [0,0,0] is returned.

		The model is searched in this order:

		Memory
		Geometry
		FireGeometry
		LandContact
		Hitpoints
		Paths
		SubParts
		ViewGeometry

		selectionName - Possible values are,
			aiming_axis
			aimPoint
			camera
			footstepL
			footstepR
			granat
			granat2
			Head
			head_axis
			lankle
			launcher
			leaning_axis
			LeftFoot
			LeftHand
			LeftHandMiddlel
			lelbow
			lelbow_axis
			lfemur
			lknee
			lknee_axis
			lshoulder
			lwrist
			Neck
			NVG
			Pelvis
			pilot
			rankle
			rearm
			rearm2
			relbow
			relbow_axis
			rfemur
			RightFoot
			RightHand
			RightHandMiddlel
			rknee
			rknee_axis
			rshoulder
			rwrist
			Spine3
			Weapon 

		method - How to calculate the selection center. Possible valued are:

		"average": Arithmetic average of all vertices in selection
		"bbox_center": Geometry center of the axis-aligned bounding box containing all vertices from selection.
		"allpoints": All points contained in the specific selection.
		*/
		static position3D getSelectionPosition(Unit& unit, string selectionName, string method);


		/*!
		Get the speed for the given speed mode.

		SpeedMode can be:

		"AUTO"
		"SLOW"
		"NORMAL"
		"FAST" 
		*/
		static float getSpeed(Unit& unit, string speedMode);

		/*!
		Return the target assigned to the vehicle.
		*/
		static ControllableObject getAssignedTarget(Unit& unit);


		/*!
		Stop AI unit.Stopped unit will not be able to move, fire, or change
		its orientation to follow a watched object.
		Deprecated- use void applyStop(Unit& unit, bool stop)
		*/
		static void UnitUtilities::stop(Unit& unit, bool stop);

		/*!
		void ControllableObjectUtilities::setExternalControl(ControllableObject& co, bool control) should be call with parameter control as 'true'.
		When used on person, smooth transition to given move will be done.
		Deprecated - use void applyMovement(Unit& unit, UNITMOVE move)
		*/
		static void playMove(Unit& unit, UNITMOVE move);

		/*!
		void ControllableObjectUtilities::setExternalControl(ControllableObject& co, bool control) should be call with parameter control as 'true'.
		When used on a person, the given move is started immediately (there is no transition). Use parameter 'moveName' as "" to switch back to the default movement if there is no transition back, otherwise the person may be stuck. 
		Deprecated - use void applySwitchMove(Unit& unit, UNITMOVE move);
		*/
		static void switchMove(Unit& unit, UNITMOVE move);


		/*!
		void ControllableObjectUtilities::setExternalControl(ControllableObject& co, bool control) should be call with parameter control as 'true'.
		When used on person, smooth transition to given move will be done. 
		moveName : It can be one of the following
		AmovPercMstpSnonWnonDnon_coughing_v1
		AmovPercMstpSnonWnonDnon_coughing_v2
		AmovPercMstpSnonWnonDnon_coughing_v3
		AmovPercMstpSnonWnonDnon_coughing_v4
		AmovPercMstpSnonWnonDnon_coughing_v5
		AmovPercMstpSnonWnonDnon_CheckSix_v1
		AmovPercMstpSnonWnonDnon_CheckSix_v2
		AmovPercMstpSnonWnonDnon_CheckSix_v3
		AmovPercMstpSnonWnonDnon_CheckSix_v4
		AmovPercMstpSnonWnonDnon_ComeHere
		AmovPercMstpSnonWnonDnon_Contempt
		AmovPercMstpSnonWnonDnon_ContemptSlap
		AmovPercMstpSnonWnonDnon_GoAway
		AmovPercMstpSnonWnonDnon_Greeting
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v1
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v2
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v3
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v4
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v5
		AmovPercMstpSnonWnonDnon_SmokingCasual_v1_long
		AmovPercMstpSnonWnonDnon_SmokingCasual_v1_short
		AmovPercMstpSnonWnonDnon_SmokingCasual_v2_long
		AmovPercMstpSnonWnonDnon_SmokingCasual_v2_short
		AmovPercMstpSnonWnonDnon_SmokingCasual_v3_long
		AmovPercMstpSnonWnonDnon_SmokingCasual_v3_short
		AmovPercMstpSnonWnonDnon_SmokingTalkingCasual_v1_complex_1
		AmovPercMstpSnonWnonDnon_SmokingTalkingCasual_v1_complex_2
		AmovPercMstpSnonWnonDnon_teargas_v1
		AmovPercMstpSnonWnonDnon_teargas_v2
		AmovPercMstpSnonWnonDnon_teargas_v3
		AmovPercMstpSnonWnonDnon_Wait
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v1
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v2
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v3
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v4
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v5
		AmovPercMstpSnonWnonDnon_Wringing_Hands_Slow_v1
		AmovPercMstpSnonWnonDnon_Wringing_Hands_Slow_v2
		Deprecated - use void applyMovement(Unit& unit, string moveName)
		*/
		static void playMove(Unit& unit, string moveName);


		/*!
		void ControllableObjectUtilities::setExternalControl(ControllableObject& co, bool control) should be call with parameter control as 'true'.
		When used on a person, the given move is started immediately (there is no transition). Use parameter 'moveName' as "" to switch back to the default movement if there is no transition back, otherwise the person may be stuck. 

		moveName : It can be one of the following
		AmovPercMstpSnonWnonDnon_coughing_v1
		AmovPercMstpSnonWnonDnon_coughing_v2
		AmovPercMstpSnonWnonDnon_coughing_v3
		AmovPercMstpSnonWnonDnon_coughing_v4
		AmovPercMstpSnonWnonDnon_coughing_v5
		AmovPercMstpSnonWnonDnon_CheckSix_v1
		AmovPercMstpSnonWnonDnon_CheckSix_v2
		AmovPercMstpSnonWnonDnon_CheckSix_v3
		AmovPercMstpSnonWnonDnon_CheckSix_v4
		AmovPercMstpSnonWnonDnon_ComeHere
		AmovPercMstpSnonWnonDnon_Contempt
		AmovPercMstpSnonWnonDnon_ContemptSlap
		AmovPercMstpSnonWnonDnon_GoAway
		AmovPercMstpSnonWnonDnon_Greeting
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v1
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v2
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v3
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v4
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v5
		AmovPercMstpSnonWnonDnon_SmokingCasual_v1_long
		AmovPercMstpSnonWnonDnon_SmokingCasual_v1_short
		AmovPercMstpSnonWnonDnon_SmokingCasual_v2_long
		AmovPercMstpSnonWnonDnon_SmokingCasual_v2_short
		AmovPercMstpSnonWnonDnon_SmokingCasual_v3_long
		AmovPercMstpSnonWnonDnon_SmokingCasual_v3_short
		AmovPercMstpSnonWnonDnon_SmokingTalkingCasual_v1_complex_1
		AmovPercMstpSnonWnonDnon_SmokingTalkingCasual_v1_complex_2
		AmovPercMstpSnonWnonDnon_teargas_v1
		AmovPercMstpSnonWnonDnon_teargas_v2
		AmovPercMstpSnonWnonDnon_teargas_v3
		AmovPercMstpSnonWnonDnon_Wait
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v1
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v2
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v3
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v4
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v5
		AmovPercMstpSnonWnonDnon_Wringing_Hands_Slow_v1
		AmovPercMstpSnonWnonDnon_Wringing_Hands_Slow_v2

		Deprecated - use void applySwitchMove(Unit& unit, string moveName)
		*/
		static void switchMove(Unit& unit, string moveName);

		/*!
		Check whether magazine is reloaded whenever emptied.
		*/
		static bool isReloadEnabled(Unit& unit);

		/*!
		Attach a statement to a unit. The statement is propagated over the network
		in MP games, it can be executed by invoking processInitCommands. 
		*/
		static void setInitStatement(Unit& unit, string& statement) ;

		/*!
		Clear the unit's init statement.
		*/
		static void clearInitStatement(Unit& unit);

#ifdef DEVELOPMENT
		/*!
		The name given to a unit using the applyIdentity() function or selected randomly by the game engine 
		if applyIdentity() has not been used on the unit.
		*/
		static string getName(Unit& unit);

		/*!
		Returns if the given unit is captive. "captive" means that enemies will not shoot at the unit.
		*/
		static bool isCaptive(Unit& unit);
#endif
		/*!
		Set if the units given in the list are allowed to enter vehicles. 
		Deprecated - use void applyAllowGetIn(vector<Unit> unitList, bool val)
		*/
		static void allowGetIn(vector<Unit> unitList, bool val);

		/*!
		Returns the type name of Binocular weapon which is in the WeaponSlotBinocular.
		Here Unit's side can't be civilian.
		*/
		static string getBinocularWeaponName(Unit& unit);

		/*!
		Returns the expected destination position of the unit.
		*/
		static position3D getExpectedDestinationPos(Unit& unit);

		/*!
		Returns the expected destination planning mode of the unit.
		*/
		static string getExpectedDestinationPlanMode(Unit& unit);

		/*!
		Tells whether destination's replanning of the path was forced.
		*/
		static bool getExpectedDestinationForceReplan(Unit& unit);

		/*!
		Returns position of unit in the formation.
		*/
		static position3D getFormationPosition(Unit& unit);

		/*!
		Return the current command type for a given soldier. 
		*/
		static string getCurrentCommand(Unit& unit);

		/*!
		Unassigns a unit from whichever vehicle that unit is currently assigned to. 
		If the unit is currently in that vehicle, the group leader will issue an order to disembark. 
		Deprecated - use void applyVehicleUnAssign(Unit& unit)
		*/

		static void unAssignVehicle(Unit& unit);

		/*!
		 Hides the body of the given person. 
		 Deprecated - use void applyBodyHiding(Unit& unit)
		*/
		
		static void hideBody(Unit& unit);
		/*!
		Returns the group leader for the given unit. For dead units, objNull is returned
		*/
		static Unit getGroupLeader(Unit& unit);

		/*!
		Returns an array with all the units in the group of the given object. For a destroyed object an empty array is returned.
		*/
		static vector<Unit> getGroupUnits(Unit& unit);

		/*!
		Check if unit is stopped by stopUnit() function. 
		*/
		static bool isStopped(Unit& unit);

		/*!
		Check if the unit is ready. Unit is busy when it is given some command like move, until the command is finished. 
		*/
		static bool isReady(Unit& unit);

		/*!
		Unselect a unit in the players group (in the UI command bar). Only valid if the player is commander of a group.
		Deprecated - use void applyUnselectFromGroup(Unit& unit)
		*/
		static void unselectUnitInGroup(Unit& unit);

		/*!
		Set if leader can issue attack commands to the soldiers in his group
		Deprecated - use void applyAttackEnable(Unit& unit, bool boolVal)  
		*/
		static void setEnableAttack(Unit& unit, bool boolVal);

		/*!
			Return whether a group's leader can issue attack commands to soldiers under his command. 
		*/
			static bool isAttackEnabled(Unit& unit);

		/*!
		The unit will play the given sound. If the unit is a person, it will also pefrorm the corresponding
		lipsync effect, provided an appropriate .lip file has been created for this sound. The sound is 
		defined in the description.ext file.
		Deprecated - use void applySpeech(Unit& unit, string& speechName)
		*/
		static void say(Unit& unit, string& speechName);

		/*!
		The unit will play the given sound. If the unit is a person, it will also pefrorm the corresponding 
		lipsync effect. If the camera is not withing the given range, the title is not shown. The sound is 
		defined in the description.ext file.
		Deprecated - use void applySpeech(Unit& unit, string& speechName, double maxTitlesDistance)
		*/
		static void say(Unit& unit, string& speechName, double maxTitlesDistance);

		
		/*!
		Locks unit into current location. Player can still chance stance, aim and perform other actions, but not move.
		If applied to AI units, they will still go through movement animations, but without actually moving. 
		Deprecated - use void applyAnimationMoveDisable(Unit& unit, bool disable)
		*/
		static void setDisableAnimationMove(Unit& unit, bool disable);

		/*!
		Checks whether movement has been disabled for a unit.
		*/
		static bool getDisableAnimationMove(Unit& unit);

		/*!
		Send the message to the side radio channel.
		Message is defined in description.ext or CfgRadio.
		Deprecated - use void applySideRadioMessage(Unit& unit, string& messageName)
		*/
		static void sendSideRadioMessage(Unit& unit, string& messageName);

		/*!
		Make a unit send a message over the group radio channel.
		Message is defined in description.ext or CfgRadio.
		Deprecated - use void applyGroupRadioMessage(Unit& unit, string& messageName)
		*/
		static void sendGroupRadioMessage(Unit& unit, string& radioName);

		/*!
		Make a unit send a message over the global radio channel.
		Message is defined in description.ext or CfgRadio.
		Deprecated - use void applyGlobalRadioMessage(Unit& unit, string& radioName)
		
		*/
		static void sendGlobalRadioMessage(Unit& unit, string& radioName);

		/*!
		Order the given unit(s) to target the given target (via the radio). 
		Deprecated - use void applyCommandTarget(vector<Unit>& unitList, Unit& target)
		*/
		static void commandTarget(vector<Unit>& unitList, Unit& target);

		/*!
		Order the given unit(s) to follow the given other unit (via the radio).
		All involved units must belong to the same group. 
		*/
		static void commandFollow(vector<Unit>& followerList, Unit& leader);

		/*!
		Sets the preference of AI to cross through buildings, when trying to reach their destination.
		The higher the number (0-1), the less likely AI will use buildings during their pathplanning.
		At 1 it will never use buildings.
		Deprecated - use void applyAIPathwayCost(Unit& AIunit, double cost)
		*/
		static void setAIPathwayCost(Unit& AIunit, double cost);

		/*!
		Returns AI's avoidance of buildings during pathfinding.
		Return value : Value between 0 and 1 (1: never use buildings)
		*/
		static double getAIPathwayCost(Unit& AIunit);

		/*!
		Returns a list of all kills caused by a player unit during a MP session. 
		Return value is Nested array, with one sub-array for each death, containing the following data:

		Time of kill (seconds from mission start)
		[name of unit that was killed, position, side, unit type]
		[name of unit which caused the death, position, side, unit type] (identical to dead unit, if self-inflicted - e.g. fall or crash)
		Distance between dead unit and killer 
		*/
		static vector<KILLS_INFO> getAllKilledByInfo(Unit& unit);

		/*!
		Returns a list of all deaths that occurred to a player unit during a MP session. 
		Nested array, with one sub-array for each death, containing the following data:

		Time of death (seconds from mission start)
		[name of unit that died, position, side, unit type]
		[name of unit which caused the death, position, side, unit type] (identical to dead unit, if self-inflicted - e.g. fall or crash)
		Distance between dead unit and killer
		*/
		static vector<KILLS_INFO> getAllDeathInfo(Unit& unit);

		/*!
		Returns a list of all injuries a player unit has received during a MP session.
		Returns Nested array, with one sub-array for each individual injury containing the following data:

		Time of injury (seconds from mission start)
		[name of injured unit, position, side, unit type]
		[name of unit which inflicted the injury, position, side, unit type] (identical to injured unit, if self-inflicted - e.g. fall or crash)
		Amount of damage inflicted
		Ammo type that inflicted the damage (empty, in case of fall or crash)
		*/
		static vector<INJURY_INFO> getAllInjuriesInfo(Unit& unit);

#if DMB_TEST_UNIT
		static void selectUnitInGroup(Unit& leader, Unit& unit, bool select);
#endif

		/*!
		Search for selection in the object model (Fire Geo only). Returns center in model space.
		*/
		static position3D getSelectionCenter(Unit& unit,string& selectionName);

		/*!
		Sets rotation rate when unit is colliding.
		Deprecated - use void applySlideOnPush(Unit& unit, double rotationRate)
		*/
		static void setSlideOnPush(Unit& unit, double rotationRate);

		/*!
		Returns the type of weapon currently being used by the unit. Return value can be one of these,
				"PRIMARY" "SECONDARY" "PISTOL" "NONE"
		*/
		static string getSelectedWeaponType(Unit& unit);

		/*!
		Returns the stance that the unit is currently in. Return value can be one of these,
				"UP" "MIDDLE" "DOWN"
		*/
		static string getUnitStance(Unit& unit);

		/*!
		Returns a position in front of the passed person where an object could be dropped. 
		*/
		static position3D getDropPos(Unit& unit);

		/*!
		Sets the training rating of the unit. 
		*/
		static void applyTraining(Unit& unit, double rating);

		/*!
		Returns unit's current allowed fleeing.
		*/
		static double getFleeing(Unit& unit);

		/*!
		Returns the combat mode (engagement rules) of the unit.

		Modes returned may be:

		"BLUE" (Never fire)
		"GREEN" (Hold fire - defend only)
		"WHITE" (Hold fire, engage at will)
		"YELLOW" (Fire at will)
		"RED" (Fire at will, engage at will) 

		or

		"UNIT ERROR" (will be returned if group was passed or combat mode couldn't be retrieved) 
		*/
		static string getUnitCombatMode(Unit& unit);

		/*!
		Return the precision of the given unit.
		*/
		static double getPrecision(Unit& unit);

		/*!
		Reveals a unit to an another unit.
		It does not matter whether revealing unit can know about the the revealed unit or not. 
		The knowledge value will be set to the highest level if revealing unit has about the revealed unit.
		If the revealing unit has no knowledge about the revealed unit, the knowledge value will be set to 1. 

		revealingUnit - Unit which receives revealing information
		revealedUnit - Unit which is revealed
		*/
		static void applyReveal(Unit& revealingUnit, Unit& revealedUnit);

		/*!
		Sets skill of given unit.Skill may vary from 0.2 to 1.0. 

		NOTE : This function will probably differ in some future products, 
		but currently it does the same as setSkill.
		*/
		static void applyUnitAbility(Unit& unit, double skill);

		/*!
		Checks if a unit is fleeing.
		*/
		static bool isFleeing(Unit& unit);

		/*!
		Sets the cowardice level (the lack of courage or bravery) of a unit.

		By setting fleeing to 0.75, units have a 75% chance of fleeing (and 25% of not fleeing). 
		Once they have decided not to flee, they will never flee.

		0 means maximum courage, while 1 means always fleeing.
		V1.50+: If allowFleeing is set to 0 for a unit that is already fleeing, that unit will stop. 
		In previous versions changing allowFleeing in a situation like this would not have had any effect.)
		Deprecated - use applyFleeing(Unit& unit, double cowardice)
		*/
		static void allowFleeing(Unit& unit, double cowardice);

		/*!
		Return the behaviour of a unit.
		Return value can be one of "CARELESS", "SAFE", "AWARE", "COMBAT" and "STEALTH"
		*/
		static string getUnitBehaviour(Unit& unit);

		/*!
		Find the nearest enemy to the specified position. 
		Returns a null object if the object's group does not know about any enemies.

		unit - The unit which is used to select the side of the enemy.
		positionUnit - This unit's position is used to select the nearest enemy unit from that position
		Deprecated - use Unit getNearestEnemy(Unit& unit, Unit& positionUnit)
		*/
		static Unit findNearestEnemy(Unit& unit, Unit& positionUnit);

		/*!
		Find the nearest enemy to the specified position. 
		Returns a null object if the object's group does not know about any enemies.

		unit - The unit which is used to select the side of the enemy.
		position - This position is used to select the nearest enemy unit from that position
		Deprecated -  use Unit getNearestEnemy(Unit& unit, position3D position)
		*/
		static Unit findNearestEnemy(Unit& unit, position3D position);

		/*!
		Returns the position around where the unit finds cover.

		position - Position (Position of hiding object)
		hidePos - Position of that which the unit should hide from
		maxDist - Maximum Distance (Look for a distance not more than this far away)
		Deprecated - use position3D getCover(Unit& unit, position3D pos, position3D hidePos, int maxDist)
		*/
		static position3D findCover(Unit& unit, position3D pos, position3D hidePos, int maxDist);

		/*!
		Returns the position around where the unit finds cover.

		position - Position (Position of hiding object)
		hidePos - Position of that which the unit should hide from
		maxDist - Maximum Distance (Look for a distance not more than this far away)
		minDist - Minimum Distance (Look for a distance more than this far away)
		Deprecated - use position3D getCover(Unit& unit, position3D pos, position3D hidePos, int maxDist, int minDist)
		*/
		static position3D findCover(Unit& unit, position3D pos, position3D hidePos, int maxDist, int minDist);

		/*!
		Returns the position around where the unit finds cover.

		position - Position (Position of hiding object)
		hidePos - Position of that which the unit should hide from
		maxDist - Maximum Distance (Look for a distance not more than this far away)
		minDist - Minimum Distance (Look for a distance more than this far away)
		visibilityPos - used to select cover that unit can see in that direction from
		Deprecated - use position3D getCover(Unit& unit, position3D pos, position3D hidePos, int maxDist, int minDist, position3D visibilityPos)
		*/
		static position3D findCover(Unit& unit, position3D pos, position3D hidePos, int maxDist, int minDist, position3D visibilityPos);

		/*!
		Returns the position around where the unit finds cover.

		position - Position (Position of hiding object)
		hidePos - Position of that which the unit should hide from
		maxDist - Maximum Distance (Look for a distance not more than this far away)
		minDist - Minimum Distance (Look for a distance more than this far away)
		visibilityPos - used to select cover that unit can see in that direction from
		ignoreObj - Ignore this object in visibility check
		Deprecated -  use position3D getCover(Unit& unit, position3D pos, position3D hidePos, int maxDist, int minDist, position3D visibilityPos, ControllableObject& ignoreObj);
		*/
		static position3D findCover(Unit& unit, position3D pos, position3D hidePos, int maxDist, int minDist, position3D visibilityPos, ControllableObject& ignoreObj);

		/*!
		Sets the data for hiding. 

		unit - The unit need to hide.
		objWhrHide - The Unit is hide behind this object.
		hidePos - The hiding Position, can be taken using getHideFrom [position3D getHideFrom(Unit& unit, Unit& enemy)].
		Deprecated - use void applyHideBehind(Unit& unit, ControllableObject& objWhrHide, position3D hidePos)
		*/
		static void setHideBehind(Unit& unit, ControllableObject& objWhrHide, position3D hidePos);

		/*!
		Disables "MOVE" AI for the unit
		*/
		static void applyAIMovementDisable(Unit& u);

		/*!
		Disables "PATHPLAN" AI for the unit
		*/
		static void applyAIPATHPLANINGDisable(Unit& u);

		/*!
		Enables "MOVE" AI for the unit
		*/
		static void applyAIMovementEnable(Unit& u);

		/*!
		Enables "PATHPLAN" AI for the unit
		*/
		static void applyAIPATHPLANINGEnable(Unit& u);

		/*!
		Reload weapons of the unit. Reload happens when the current magazine of the weapon is empty.
		*/
		static void applyWeaponReload(Unit& u);

		/*!
		Send the text message over the global radio channel.
		*/
		static void applyGlobalChat(Unit& u,string txtMessage );
		/*!
		Orders a unit to commence firing on the given target (silently). 
		If the target is objNull, the unit is ordered to commence firing on its current target (using doTarget).		
		*/
		static void applyFire(Unit& firingUnit, ControllableObject& target);
		/*!
		Orders the unit (silently) to fire at the given position. Please note that the 
		position needs to be given in PositionASL format. 
		
		*/
		static void applyFire(Unit& firingUnit, position3D pos);

		/*!
		set the behavior of the unit. 
		 CARELESS,SAFE,AWARE,COMBAT,STEALTH
		*/
		static void applyBehaviour(Unit& unit, BEHAVIOUR behaviour);

		/*!
		Returns true if a group with the name/alias exists. 
		*/

		static bool isGroupExists(string groupName);
		
		/*!
		order unit to stop.		
		*/
		static void applyCommandStop(Unit& u);

		/*
		Respawns the unit. Care should be taken when using this command as VBS2 automatically
		assigns a new name to the respawned unit (this does not happen if the unit is a named unit). 

		This command should therefore be used in conjunction with a respawn event handler to 
		obtain the new name and to assign an alias to the object. If not, respawning can result in
		the loss of the handle to the unit and therefore could cause errors during the update cycle. 	
		*/
		static void applyRespawn(Unit& unit);

		/*!
		Orders the unit to move to movePosition. 
		*/
		static void applyMove(Unit& u, position3D movePosition);

		/*!
		Orders the unit to target the given target (silently).		
		*/
		static void applyTarget(Unit& unit, ControllableObject& co);

		/*!
		Orders the unit to watch the given position (format position3D) (silently).
		
		*/
		static void applyWatch(Unit& unit, position3D targetPosition);

		/*!
		Applies the AUTO position to the unit within the game. Does not update the 
		UnitPos property of the unit. Use updateUnitPos to reload and check for successful execution.		
		*/
		static void applyUnitPosAUTO(Unit& u);

		/*!
		Applies the DOWN position to the unit within the game. Does not update the 
		UnitPos property of the unit. Use updateUnitPos to reload and check for successful execution.
		
		*/
		static void applyUnitPosDOWN(Unit& u);

		/*!
		Enable/Disable precision movement mode for specified unit. 
		This enables unit to move to exact position 
		(e.g. as specified by doMove) requested. By enabling precision movement, units precision is forced to 0. 
		Command only modifies movement in final steps, thus path-finding and animation are not affected. 
		
		*/
		static void applyUnitExactMovementMode(Unit& u, bool precisionMode);
		/*!
		Causes a unit to salute to a specified target unit.
		
		*/
		static void applySalutePerform(Unit& unit, Unit& targetUnit);

		/*!
		set body height of the unit. height should be in meters.
		
		*/
		static void applyHeight(Unit& u , double height);

		/*!
		set Body Mass Index (BMI) of the unit.
		
		*/
		static void applyBMI(Unit& u, double index );
		/*!
		Disable gunner input of the unit.
		input Modes are
			NOTHING         - Enable disabled modes.
			WEAPON			- Disable weapon.
			HEAD			- Disable head.
			WEAPONANDHEAD	- Disable weapon and head.
		*/
		static void applyGunnerInputDisable(Unit& unit,GUNNERINPUT_MODE mode);

		#if WEAPON_HANDLING

		/*!
		Select the Weapon given by the muzzle Name.		
		*/
		static void applyWeaponSelect(Unit& u, string muzzleName);

		/*!
		Add magazine to the Unit.
		
		*/
		static void applyMagazineAdd(Unit& u, string name);

		/*!
		Count number of magazines available in given unit object.
		
		*/
		static int getMagazinesCount(Unit& u);
		/*!
		Remove the one specified magazine from the unit.
		
		*/
		static void applyMagazineRemove(Unit& unit, string name);

		/*!
		Remove all specified magazines from the unit.
		
		*/
		static void applyMagazineRemoveAll(Unit& unit, string name);

		/*!
		Add Weapon to the Unit. weapon slots are filled, any further addWeapon commands are ignored. 
		To ensure that the weapon is loaded at the start of the mission, at least one magazine should 
		be added, before adding threse weapon.
		
		*/
		static void applyWeaponAdd(Unit& unit, string name);

		/*!
		Remove the specified Weapon from the unit.
		
		*/
		static void applyWeaponRemove(Unit& unit, string name);
		/*!
		Tells if a unit has the given weapon.
		
		*/
		static bool isWeaponHaving(Unit& unit, string name);

		/*!
		Sets the direction of the Weapon according to azimuth (relative to the unit's body direction)& elevation. 
		Must use the disableGunnerInput function before using this function.
		
		*/
		static void applyWeaponDirection(Unit& unit, double azimuth, double elevation, bool transition);

		/*!
		Sets how much ammunition (compared to a full state defined by the unit type) the unit has. 
		The value ranges from 0 to 1.	
		
		*/
		static void applyVehicleAmmo(Unit& unit, float ammovalue);

		/*!
		Remove all weapons and magazines from the unit.
		
		*/
		static void applyWeaponRemoveAll(Unit& unit);


		/*!
		Enables or Disables firing on current weapon. If true, the unit's current weapon cannot be fired.
		
		*/
		static void applyWeaponSafety(Unit& unit, bool safe);
		
		#endif
		/*!
		Returns a list of targets within the defined range.
		"Targets" are not restricted to enemy units.		
		*/
		static vector<Target> getNearTargets(Unit& unit, double range);
		/*!
		Enables personal items. Used for units inside vehicles to enable their personal equipment.
		
		*/
		static void applyPersonalItemsEnable(Unit& unit, bool enable);

		/*!
		Turns the ability of a unit to audibly speak on or off.

		Does not affect orders from being issues or received.
		Units will still scream when being shot at.
		
		*/
		static void  applySpeechEnable(Unit& unit, bool speech);
		/*!
		Adds the passed value to the unit's morale (morale is between 0=exhausted and 1=fit). 
		moralChange - It is the adding value. use (-)ve to reduce the morale
		
		*/
		static void applyMoralAddition(Unit& unit, double moralChange);

		/*!
		Adds the passed value to the unit's anaerobic fatigue (fatigue is between 0 and 1). 
		AnaerobicFatigueChange - It is the adding value. use (-)ve to reduce the AnaerobicFatigue
		
		*/
		static void applyAnaerobicFatigueAddition(Unit& unit, double AnaerobicFatigueChange);

		/*!
		Adds the passed value to the unit's aerobic fatigue (fatigue is between 0 and 1). 
		AerobicFatigueChange - It is the adding value. use (-)ve to reduce the AerobicFatigue
		
		*/
		static void  applyAerobicFatigueAddition(Unit& unit, double AerobicFatigueChange);

		 /*!
		 Order the given unit(s) to stop (without radio messages).
		 DoStop'ed units leave the groups formation. It will prevent the unit from moving around with their group 
		 (or formation leader), while still being able to turn around and even move to a new position if they see fit. 
		 They will still respond to orders from their group leader (like engage, rearm, board a vehicle), but all of their 
		 actions will be separate from the group formation (unless ordered to return, which AI leaders don't do unless a script tells them to).
		
		*/
		static void applyStop(Unit& unit);


		/*!
		Revives an Unit immediately, preserving his name & equipment.		
		*/
	    static void applyRevive(Unit& unit);

		
		/*!
		Add weapons to the cargo space of objects, which can be taken out by infantry units. 
		Once the weapon cargo space is filled up, any further addWeaponCargo commands are ignored.
		
		*/
		static void applyWeaponCargo(Unit& unit, string weaponName, int count);

		/*!
		Add magazines to the cargo space of Objects, which can be taken out by infantry units. 
		Once the magazine cargo space is filled up, any further addMagazineCargo commands are ignored.
		
		*/
		static void applyMagazineCargo(Unit& unit, string magName, int count);

		/*!
		It is only for local objects
		Order the given unit(s) to move to the given location (via the radio). 
		Exactly the same as doMove, except this command displays a radio message.
		*/
		static void applyCommandMove(Unit& unit, position3D position);

		/*!
		Add a number to the rating of a unit. Negative values can be used to reduce the rating. This command is 
		usually used to reward for completed mission objectives. The rating is given at the end of the mission 
		and is automatically adjusted when killing enemies or friendlies. When the rating gets below zero, the 
		unit is considered "renegade" and is an enemy to everyone.
		
		*/
		static void applyRatingIncrease(Unit& unit, double rating);

		/*!
		Add a number to the score of a unit. This score is shown in debriefing page. Negative values will remove from the score
		
		*/
		static void applyScoreIncrease(Unit& unit, int score);
		

		/*!
		Check if unit has some ammo.		
		*/
		static bool isAmmoAvailable(Unit& unit);

		/*!
		Check if (and by how much) unit knows about target.
		And returns a number from 0 to 4.
		
		*/
		static float getKnowsAbout(Unit& unit, ControllableObject& target);

		/*!
		Types text to the side radio channel.		
		*/
		static void applySideChat(Unit& unit, string chatText);

		/*!
		Make a unit send a text message over the group radio channel. 
		
		*/
		static void applyGroupChat(Unit& unit, string chatText);
		/*!
		Add an action to an object. The action is usable by the object itself, as well as anyone who is within proximity of the object and is looking at it.

		If an action is added to a unit, the action will only be visible when the unit is on foot. If the unit enters a vehicle, the action will not be visible, not to the unit, or to anyone in / near his vehicle.
		unit - Object to assign the action to
		title - The action name which is displayed in the action menu
		filename - File and Path relative to the mission folder. Called when the action is activated. Depending on the file extension (*.sqf/*.sqs) the file is executed with SQF or SQS Syntax

		It returns Index of the added action. Used to remove the action with deleteActionMenuItem()
		
		*/
		static int createActionMenuItem(Unit& unit, string title, string fileName);

		/*!
		Remove action with given id index. 		
		*/
		static void deleteActionMenuItem(Unit& unit, int index);
		
		/*!
		Reload primary weapon of given unit		
		*/
		static void applyPrimaryWeaponReload(Unit& unit);
		
		/*!
		Stop AI unit.Stopped unit will not be able to move, fire, or change
		its orientation to follow a watched object.
		
		*/
		static void applyStop(Unit& unit, bool stop);

		/*!
		void ControllableObjectUtilities::setExternalControl(ControllableObject& co, bool control) should be call with parameter control as 'true'.
		When used on person, smooth transition to given move will be done.
		
		*/
		static void applyMovement(Unit& unit, UNITMOVE move);

		/*!
		void ControllableObjectUtilities::setExternalControl(ControllableObject& co, bool control) should be call with parameter control as 'true'.
		When used on a person, the given move is started immediately (there is no transition). Use parameter 'moveName' as "" to switch back to the default movement if there is no transition back, otherwise the person may be stuck. 
		*/
		static void applySwitchMove(Unit& unit, UNITMOVE move);

		/*!
		void ControllableObjectUtilities::setExternalControl(ControllableObject& co, bool control) should be call with parameter control as 'true'.
		When used on person, smooth transition to given move will be done. 
		moveName : It can be one of the following
		AmovPercMstpSnonWnonDnon_coughing_v1
		AmovPercMstpSnonWnonDnon_coughing_v2
		AmovPercMstpSnonWnonDnon_coughing_v3
		AmovPercMstpSnonWnonDnon_coughing_v4
		AmovPercMstpSnonWnonDnon_coughing_v5
		AmovPercMstpSnonWnonDnon_CheckSix_v1
		AmovPercMstpSnonWnonDnon_CheckSix_v2
		AmovPercMstpSnonWnonDnon_CheckSix_v3
		AmovPercMstpSnonWnonDnon_CheckSix_v4
		AmovPercMstpSnonWnonDnon_ComeHere
		AmovPercMstpSnonWnonDnon_Contempt
		AmovPercMstpSnonWnonDnon_ContemptSlap
		AmovPercMstpSnonWnonDnon_GoAway
		AmovPercMstpSnonWnonDnon_Greeting
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v1
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v2
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v3
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v4
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v5
		AmovPercMstpSnonWnonDnon_SmokingCasual_v1_long
		AmovPercMstpSnonWnonDnon_SmokingCasual_v1_short
		AmovPercMstpSnonWnonDnon_SmokingCasual_v2_long
		AmovPercMstpSnonWnonDnon_SmokingCasual_v2_short
		AmovPercMstpSnonWnonDnon_SmokingCasual_v3_long
		AmovPercMstpSnonWnonDnon_SmokingCasual_v3_short
		AmovPercMstpSnonWnonDnon_SmokingTalkingCasual_v1_complex_1
		AmovPercMstpSnonWnonDnon_SmokingTalkingCasual_v1_complex_2
		AmovPercMstpSnonWnonDnon_teargas_v1
		AmovPercMstpSnonWnonDnon_teargas_v2
		AmovPercMstpSnonWnonDnon_teargas_v3
		AmovPercMstpSnonWnonDnon_Wait
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v1
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v2
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v3
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v4
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v5
		AmovPercMstpSnonWnonDnon_Wringing_Hands_Slow_v1
		AmovPercMstpSnonWnonDnon_Wringing_Hands_Slow_v2
		
		
		*/

		static void applyMovement(Unit& unit, string moveName);

		/*!
		void ControllableObjectUtilities::setExternalControl(ControllableObject& co, bool control) should be call with parameter control as 'true'.
		When used on a person, the given move is started immediately (there is no transition). Use parameter 'moveName' as "" to switch back to the default movement if there is no transition back, otherwise the person may be stuck. 

		moveName : It can be one of the following
		AmovPercMstpSnonWnonDnon_coughing_v1
		AmovPercMstpSnonWnonDnon_coughing_v2
		AmovPercMstpSnonWnonDnon_coughing_v3
		AmovPercMstpSnonWnonDnon_coughing_v4
		AmovPercMstpSnonWnonDnon_coughing_v5
		AmovPercMstpSnonWnonDnon_CheckSix_v1
		AmovPercMstpSnonWnonDnon_CheckSix_v2
		AmovPercMstpSnonWnonDnon_CheckSix_v3
		AmovPercMstpSnonWnonDnon_CheckSix_v4
		AmovPercMstpSnonWnonDnon_ComeHere
		AmovPercMstpSnonWnonDnon_Contempt
		AmovPercMstpSnonWnonDnon_ContemptSlap
		AmovPercMstpSnonWnonDnon_GoAway
		AmovPercMstpSnonWnonDnon_Greeting
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v1
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v2
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v3
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v4
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v5
		AmovPercMstpSnonWnonDnon_SmokingCasual_v1_long
		AmovPercMstpSnonWnonDnon_SmokingCasual_v1_short
		AmovPercMstpSnonWnonDnon_SmokingCasual_v2_long
		AmovPercMstpSnonWnonDnon_SmokingCasual_v2_short
		AmovPercMstpSnonWnonDnon_SmokingCasual_v3_long
		AmovPercMstpSnonWnonDnon_SmokingCasual_v3_short
		AmovPercMstpSnonWnonDnon_SmokingTalkingCasual_v1_complex_1
		AmovPercMstpSnonWnonDnon_SmokingTalkingCasual_v1_complex_2
		AmovPercMstpSnonWnonDnon_teargas_v1
		AmovPercMstpSnonWnonDnon_teargas_v2
		AmovPercMstpSnonWnonDnon_teargas_v3
		AmovPercMstpSnonWnonDnon_Wait
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v1
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v2
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v3
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v4
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v5
		AmovPercMstpSnonWnonDnon_Wringing_Hands_Slow_v1
		AmovPercMstpSnonWnonDnon_Wringing_Hands_Slow_v2	
		
		*/
		static void applySwitchMove(Unit& unit, string moveName);


		/*!
		Set if the units given in the list are allowed to enter vehicles.
		
		*/
		static void applyAllowGetIn(vector<Unit> unitList, bool val);

		/*!
		Unassigns a unit from whichever vehicle that unit is currently assigned to. 
		If the unit is currently in that vehicle, the group leader will issue an order to disembark. 
		
		*/

		static void applyVehicleUnassign(Unit& unit);

		/*!
		 Hides the body of the given person.		
		*/
		
		static void applyBodyHiding(Unit& unit);

		/*!
		Un select a unit in the players group (in the UI command bar). Only valid if the player is commander of a group. 
		
		*/
		static void applyUnselectFromGroup(Unit& unit);

		/*!
		Set if leader can issue attack commands to the soldiers in his group
		*/
		static void applyAttackEnable(Unit& unit, bool boolVal);

		/*!
		The unit will play the given sound. If the unit is a person, it will also pefrorm the corresponding
		lipsync effect, provided an appropriate .lip file has been created for this sound. The sound is 
		defined in the description.ext file.
		
		*/
		static void applySpeech(Unit& unit, string& speechName);

		/*!
		The unit will play the given sound. If the unit is a person, it will also pefrorm the corresponding 
		lipsync effect. If the camera is not withing the given range, the title is not shown. The sound is 
		defined in the description.ext file.		
		*/
		static void applySpeech(Unit& unit, string& speechName, double maxTitlesDistance);

		/*!
		The unit will play the given sound. If the unit is a person, it will also perform the corresponding 
		lipsync effect. If the camera is not withing the given range, the title is not shown. The sound is 
		defined in the description.ext file. The parameter 'speed' defines the speed of subtitle lines if
		subtitle text has defined for the relevant sound.	
		*/
		static void applySpeech(Unit& unit, string& speechName, double maxTitlesDistance, double speed);

		/*!
		Locks unit into current location. Player can still chance stance, aim and perform other actions, but not move.
		If applied to AI units, they will still go through movement animations, but without actually moving. 
		*/
		static void applyAnimationMoveDisable(Unit& unit, bool disable);

		/*!
		Send the message to the side radio channel.
		Message is defined in description.ext or CfgRadio. 
		*/
		static void applySideRadioMessage(Unit& unit, string& messageName);
		
		/*!
		Make a unit send a message over the group radio channel.
		Message is defined in description.ext or CfgRadio.
		*/
		static void applyGroupRadioMessage(Unit& unit, string& messageName);
		/*!
		Make a unit send a message over the global radio channel.
		Message is defined in description.ext or CfgRadio.
		*/
		static void applyGlobalRadioMessage(Unit& unit, string& radioName);

		/*!
		Order the given unit(s) to target the given target (via the radio). 
		*/
		static void applyCommandTarget(vector<Unit>& unitList, Unit& target);

		/*!
		Sets the preference of AI to cross through buildings, when trying to reach their destination.
		The higher the number (0-1), the less likely AI will use buildings during their pathplanning.
		At 1 it will never use buildings.
		*/
		static void applyAIPathwayCost(Unit& AIunit, double cost);

		/*!
		Sets rotation rate when unit is colliding.		
		*/
		static void applySlideOnPush(Unit& unit, double rotationRate);
		/*!
		Sets the cowardice level (the lack of courage or bravery) of a unit.

		By setting fleeing to 0.75, units have a 75% chance of fleeing (and 25% of not fleeing). 
		Once they have decided not to flee, they will never flee.

		0 means maximum courage, while 1 means always fleeing.
		V1.50+: If allowFleeing is set to 0 for a unit that is already fleeing, that unit will stop. 
		In previous versions changing allowFleeing in a situation like this would not have had any effect.)
		
		*/
		static void applyFleeing(Unit& unit, double cowardice);

		/*!
		Find the nearest enemy to the specified position. 
		Returns a null object if the object's group does not know about any enemies.

		unit - The unit which is used to select the side of the enemy.
		positionUnit - This unit's position is used to select the nearest enemy unit from that position
		*/
		static Unit getNearestEnemy(Unit& unit, Unit& positionUnit);

		/*!
		Find the nearest enemy to the specified position. 
		Returns a null object if the object's group does not know about any enemies.

		unit - The unit which is used to select the side of the enemy.
		position - This position is used to select the nearest enemy unit from that position
		
		*/
		static Unit getNearestEnemy(Unit& unit, position3D position);

		/*!
		Returns the position around where the unit finds cover.

		position - Position (Position of hiding object)
		hidePos - Position of that which the unit should hide from
		maxDist - Maximum Distance (Look for a distance not more than this far away)
		
		*/
		static position3D getCover(Unit& unit, position3D pos, position3D hidePos, int maxDist);
		/*!
		Returns the position around where the unit finds cover.

		position - Position (Position of hiding object)
		hidePos - Position of that which the unit should hide from
		maxDist - Maximum Distance (Look for a distance not more than this far away)
		minDist - Minimum Distance (Look for a distance more than this far away)		
		*/
		static position3D getCover(Unit& unit, position3D pos, position3D hidePos, int maxDist, int minDist);

		/*!
		Returns the position around where the unit finds cover.

		position - Position (Position of hiding object)
		hidePos - Position of that which the unit should hide from
		maxDist - Maximum Distance (Look for a distance not more than this far away)
		minDist - Minimum Distance (Look for a distance more than this far away)
		visibilityPos - used to select cover that unit can see in that direction from
		
		*/
		static position3D getCover(Unit& unit, position3D pos, position3D hidePos, int maxDist, int minDist, position3D visibilityPos);

		/*!
		Returns the position around where the unit finds cover.

		position - Position (Position of hiding object)
		hidePos - Position of that which the unit should hide from
		maxDist - Maximum Distance (Look for a distance not more than this far away)
		minDist - Minimum Distance (Look for a distance more than this far away)
		visibilityPos - used to select cover that unit can see in that direction from
		ignoreObj - Ignore this object in visibility check
		
		*/
		static position3D getCover(Unit& unit, position3D pos, position3D hidePos, int maxDist, int minDist, position3D visibilityPos, ControllableObject& ignoreObj);

		/*!
		Sets the data for hiding. 

		unit - The unit need to hide.
		objWhrHide - The Unit is hide behind this object.
		hidePos - The hiding Position, can be taken using getHideFrom [position3D getHideFrom(Unit& unit, Unit& enemy)].
		
		*/
		static void applyHideBehind(Unit& unit, ControllableObject& objWhrHide, position3D hidePos);

		/*!
		Switch on/off forced colliding of AI units
		*/
		static void applyCollisionEnable(Unit& unit,bool val);

		/*! 
		Return the vehicle which given unit is mounted.
		
		@param unit - Unit that will be returned it's mounted vehicle.

		@return Vehicle - Vehicle object will return if unit is mounted to a vehicle, if it is not NULL object will return.
		*/
		static Vehicle getMountedVehicle(Unit& unit);

		/*!
		@description

		Applies the UP position to the unit within the game. Does not update the  UnitPos property of the unit. Use UnitUtilities::updateUnitPos to reload and check for successful execution. 

		@locality 

		Locally Applied Globally Effected

		@version [VBS2Fusion v2.07.2]

		@param u - unit that suppose to apply UP position.

		@return Nothing.

		@example

		@code

		//Unit to be created
		Unit u;

		//Apply the applyUnitPosUP function to unit u.
		UnitUtilities::applyUnitPosUP(u);
		UnitUtilities::updateUnitPos(u);

		@endcode

		@relates to 

		void UnitUtilities::unitPosUP(Unit& u)

		@overloaded

		void UnitUtilities::applyUnitPos(Unit& u, UNITPOS pos);

		@remarks This is a replication of void UnitUtilities::unitPosUP(Unit& u)
		*/
		static void applyUnitPosUP(Unit& u);

		/*!
		@description 
		
		Orders the unit to watch the given target (via the radio).Use objNull as the target to order a unit to stop watching a target 
	
		@locality 

		Locally Applied Globally Effected

		@version [VBS2Fusion v2.07.2]

		@param unit - A unit who get the command to watch at some ControllableObject.

		@param target - The ControllableObject which unit watch.

		@return Nothing.

		@example

		@code
		
		//Unit to be created
		Unit u;
	
		//Apply the applyCommandWatch function to unit u on the player unit
		UnitUtilities::applyCommandWatch(u,player);

		@endcode

		@overloaded 
		
		void UnitUtilities::applyCommandWatch(Unit& unit, position3D& target)

		@relates to 
		
		void UnitUtilities::commandWatch(Unit& unit, ControllableObject& target)	

		@remarks This is a replication of UnitUtilities::commandWatch(Unit& unit, ControllableObject& target)

		*/
		static void applyCommandWatch(Unit& unit, ControllableObject& target);

		/*!		
		@description 
		
		Orders the unit to watch the given position (via the radio).
		
		@locality

		Locally Applied Globally Effected

		@version [VBS2Fusion v2.07.2]
	
		@param unit - A unit who get the command to watch at some target position.

		@param target - The position which unit watch.

		@return Nothing.

		@example

		@code

		//Unit to be created
		Unit u;

		//Apply the applyCommandWatch function to unit u on the player units position.
		UnitUtilities::applyCommandWatch(u,player.getPosition());

		@endcode

		@relates to
		
		void UnitUtilities::commandWatch(Unit& unit, position3D& target)

		@overloaded
		
		void UnitUtilities::applyCommandWatch(Unit& unit, ControllableObject& target)

		@remarks This is a replication of UnitUtilities::commandWatch(Unit& unit, position3D& target)
		*/
		static void applyCommandWatch(Unit& unit, position3D& target);

		/*!
		@description 
		
		Control what the unit is glancing at (target or Position). How frequently the unit is glancing there depends on behaviour. 

		@locality

		Locally Applied Globally Effected

		@version [VBS2Fusion v2.07.2]

		@param unit - A unit who get the command to glance at some target position.

		@param target - The position which unit glance at.

		@return Nothing.

		@example

		@code

		//Unit to be created
		Unit u;

		//Apply the applyGlanceAt function to unit u on the player units position.
		UnitUtilities::applyGlanceAt(u,player.getPosition());

		@endcode

		@relates to 
		
		void UnitUtilities::glanceAt(Unit& unit, position3D& position)

		@overloaded

		@remarks This is a replication of UnitUtilities::glanceAt(Unit& unit, position3D& position)
		*/
		static void applyGlanceAt(Unit& unit, position3D& position);

		/*!

		@description 
		
		Control what the unit is looking at (position).

		@locality  
		
		Locally Applied Globally Effected

		@version [VBS2Fusion v2.07.2]

		@param unit - A unit who get the command to look at some target position.

		@param target - The position which unit look at.

		@return Nothing.

		@example

		@code

		//Unit to be created
		Unit u;

		//Apply the applyLookAt function to unit u on the player units position.
		UnitUtilities::applyLookAt(u,player.getPosition());

		@endcode

		@relates to 
		
		void UnitUtilities::LookAt(Unit& unit, position3D& position)

		@overloaded

		@remarks This is a replication of UnitUtilities::LookAt(Unit& unit, position3D& position)
		*/
		static void applyLookAt(Unit& unit, position3D& position);

		/*!
		@description 

		Damage or repair part of a unit.

		@locality 

		@version [VBS2Fusion v]

		@param unit - The unit that is hit.

		@param part - The part of the unit which is hit. Part can be "body", "hands" or "legs". 

		@param damage - Damage level between 0 and 1. Damage 0 means fully functional, damage 1 means completely dead.

		@return Nothing.

		@example

		@code

		@relates

		@overloaded

		@remarks This function will trigger the EventHandlers for "Dammaged", "DamagedHitPart" & "Killed". 
		*/
		static void applyHit(Unit& unit, std::string& part, double damage);

	private:
		friend class Unit;

	};
};


#endif