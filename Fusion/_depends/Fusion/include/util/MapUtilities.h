 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:	MapUtilities.h

Purpose: This file contains VBS2 Real Time Editor (RTE) related Utility function definitions. 

/*****************************************************************************/
#ifndef VBS2FUSION_MAPUTILITIES_H
#define VBS2FUSION_MAPUTILITIES_H

/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES
#include <vector>

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "position3D.h"
#include "data/MapObject.h"
#include "VBS2FusionDefinitions.h"
#include "data/ControllableObject.h"
#include "data/Unit.h"

/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBS2Fusion
{
	class VBS2FUSION_API MapUtilities
	{
	public:		
		/*!
			creates a controllable object.
			- The type of object is as specified by parameter "typeName".
			- Creates the object at the position specified by parameter "pos".
		*/
		static void createObject(MapObject& co, string typeName, position3D pos);

		/*!
		Lists Id of any deleted or promoted map objects.
		As the original map objects are deleted during promotions, they will be listed as well. 
		*/
		static vector<string> getAllDeletedMapObjectsId();

		/*!
		Nearest building to given object.
		A "building" is defined as an object that is of class "House". 
		*/
		static MapObject getNearestBuilding(ControllableObject& object);

		/*!
		Deletes specified Map Object. Only objects created during the game can be deleted.
		*/
		static void deleteObject(MapObject& co);

		/*!
		Detects whether an object was part of the terrain (e.g. plants, buildings), 
		or whether it was placed via the editor or via script command. 
		obj - id and position Must be set
		*/
		static bool isMapPlaced(MapObject& obj);

		/*!
		Deletes a map (visitor placed) object.
		Returns the map id
		obj - id and position Must be set
		*/
		static string deleteMapObject(MapObject& obj);

		/*!
		Restores a map object that was previously deleted (via deleteMapObject).
		When a map object is "promoted", a copy of the map object is created as an editable object, 
		while the original object is removed from the map. If a map object is restored, this copy will 
		stay on the map, in addition to the restored map object.
		Deprecated. Use MapObject applyMapObjectRestore(string id).
		*/
		VBS2FUSION_DPR(UMAP001)static MapObject restoreMapObject(string id);

		/*!
		Makes a static map object "dynamic", so that changes to it will be propagated through the network.
		Deprecated. Use void applyStaticToDynamic(MapObject& obj).
		*/
		VBS2FUSION_DPR(UMAP002)static void staticToDynamic(MapObject& obj);

		/*!
		Returns the given indexed position in a building

		The index is 0-based (i.e. the first possible position would be 0. So if a building has 5 positions listed 
		in the editor, 4 would be the highest position index usable with this command). 
		*/
		static position3D getBuildingPos(MapObject& obj, int index);

		/*!
		Returns an array of buildings (sorted by distance) within a given range.
		A "building" is defined as an object that is of class "House". 
		*/
		static vector<MapObject> getNearestBuildings(ControllableObject co , double range);

		/*!
		Returns an array of buildings (sorted by distance) within a given range.
		A "building" is defined as an object that is of class "House". 
		*/
		static vector<MapObject> getNearestBuildings(position3D pos , double range);

		
		/*!
		@description  

		Returns an array of positions linked to the building position closest to the given position. 

		@locality

		Locally Applied Globally Effected

		@version [VBS2Fusion v2.70.2]

		@param ControllableObject - building.

		@param position3D - given position.

		@return vector<position3D> - vector of position3D

		@example

		@code

		//ControllableObject to be created
		ControllableObject co;

		//The controllable object type of "Land_prs_whitehouse" (A building type of house) that would be used to create the ControllableObject.
		co.setType(string("Land_prs_whitehouse"));


		vector<position3D> vecPos = MapUtilities::getLinkedBuildingPositions(co,ControllableObjectUtilities::getPosition(co));

		@endcode

		@overloaded 

		@related  

		@remarks What must be noted is that this function will not return values for normal ControllableObject's like Unit, Vehicle etc. They need to be specified types like buildings (as shown in the example above) etc.
		*/

		static vector<position3D> getLinkedBuildingPositions(ControllableObject& co ,position3D pos) ;

		/*!
		@description  

		Control fireplace burning. Set inflame to true (on) or false (off). 

		@locality

		Locally Applied Globally Effected

		@version [VBS2Fusion v2.70.2]

		@param ControllableObject - fireplace.

		@param bool - true/false.

		@return Nothing

		@example

		@code

		//ControllableObject to be created
		ControllableObject co;

		//The ControllableObject Type to create
		co.setType(string("Fire"));
		ControllableObjectUtilities::createObject(co);
		ControllableObjectUtilities::updateStaticProperties(co);
		ControllableObjectUtilities::updateDynamicProperties(co);

		MapUtilities::applyInflame(co1,true);

		@endCode

		@overloaded 

		@related  

		@remarks 
		*/
		static void applyInflame(ControllableObject& co ,bool val) ;

		/*!
		@description  

		Check if fireplace is inflamed (burning) or not. 

		@locality

		Locally Applied Globally Effected

		@version [VBS2Fusion v2.70.2] 

		@param ControllableObject - fireplace.

		@return bool - true/false.

		@example

		@code

		//ControllableObject to be created
		ControllableObject co;

		displayString = "co isInflamed : " + conversions::BoolToString(MapUtilities::isInflamed(co));

		@endcode

		@overloaded 

		@related  void ControllableObjectUtilities::applyInflame(ControllableObject& co ,bool val)

		@remarks 
		*/
		static bool isInflamed(ControllableObject& co ) ;



		/*!
		@description  

		Nearest object of given type to given position within a range of 50 meters. If class types are used in the filter, then in Arma any object derived from the type is found as well. In OFP, only objects with exactly the type given are found. 

		@locality

		Globally applied

		@version [VBS2Fusion v2.70.3] 

		@param ControllableObject - Object which suppose to get nearest MapObject.

		@param string - type of map object

		@return MapObject 

		@example

		@code

		//Returns the nearest night lamp post to the u1 unit
		MapObject streetLamp = MapUtilities::getNearestObject(u1, string("StreetLamp"));
		displayString=displayString+"Lamp_id: "+ streetLamp.getID();

		@endcode

		@overloaded 

		@related  

		@remarks 
		*/
		static MapObject getNearestObject(ControllableObject& co, string type);

		
		/*!
		@description  

		Controls whether a lamp is lit or not.  

		@locality

		Globally applied, Locally effected

		@version [VBS2Fusion v2.70.3] 

		@param MapObject - lamp to be lit.

		@param enum - LIGHT_MODE  "ON", "OFF" or "AUTO" .

		@return nothing 

		@example

		@code

		//Returns the nearest night lamp post to the u1 unit
		MapObject streetLamp = MapUtilities::getNearestObject(u1, string("StreetLamp"));

		//Applies the light mode to the streetLamp map object 
		MapUtilities::applySwitchOn(streetLamp,LIGHT_OFF);

		@endcode

		@overloaded 

		@related  

		MapUtilities::getNearestObject(ControllableObject& co, string type);

		@remarks 
		*/
		
		static void applySwitchOn(MapObject& co, LIGHT_MODE mode);


		/*!
		@description  

		Check if lampost is on (shining).  

		@locality

		Locally Effected

		@version [VBS2Fusion v2.70.3] 

		@param MapObject - lamp which needs to check the status

		@return string - Status of the light 

		@example

		@code

		//Returns the nearest night lamp post to the u1 unit
		MapObject streetLampNetwork = MapUtilities::getNearestObject(u1, string("StreetLamp"));

		//Returns the light status(mode)
		string lampStatusNetwork = MapUtilities::getLightStatus(streetLampNetwork);

		@endcode

		@overloaded 

		@related  

		@remarks 
		*/
		
		static string getLightStatus(MapObject& co);

		/*!
		@description 
		Executes a command when a map object is deleted via the editor's context menu. Can only be used inside the editor.
		
		@locality

		Locally Effected

		@version  [VBS2Fusion v2.70.4]

		@param command - Code to be executed when map object is deleted.
					
		@return Nothing.

		@example

		@code

		MapUtilities::applyOnMapObjectDeleted(string("hintC 'Deleted'"));

		@endcode

		@overloaded  
		
		None.

		@related

		@remarks 
		*/
		static void applyOnMapObjectDeleted(string command);

		/*!
		@description 

		Executes a command when a map object is promoted via the editor's context menu. Can only be used inside the editor.
		
		@locality

		Locally Effected

		@version  [VBS2Fusion v2.70.4]

		@param command - Code to be executed when object is promoted.

		@return Nothing.

		@example

		@code

		MapUtilities::applyOnMapObjectPromoted(string("hintC 'Promoted'"));

		@endcode

		@overloaded  

		None.
		
		@related

		@remarks 
		*/
		static void applyOnMapObjectPromoted(string command);

		/*!
		@description
		Restores a map object that was previously deleted (via deleteMapObject).
		When a map object is "promoted", a copy of the map object is created as an editable object, 
		while the original object is removed from the map. If a map object is restored, this copy will 
		stay on the map, in addition to the restored map object.

		@locality		

		@version [VBS2Fusion v2.71] 
		
		@param id -	Map ID.		

		@return Object that was restored.

		@example 

		@code		

		@endcode

		@overloaded Nothing.

		@related  

		@remarks This is a replication of MapObject restoreMapObject(string id).
		*/
		static MapObject applyMapObjectRestore(string id);

		/*!
		@description
		Makes a static map object "dynamic", so that changes to it will be propagated through the network.

		@locality		

		@version [VBS2Fusion v2.71] 
		
		@param obj- Map object.	

		@return Nothing.

		@example 

		@code		

		@endcode

		@overloaded Nothing.

		@related  

		@remarks This is a replication of void staticToDynamic(MapObject& obj)
		*/
		static void applyStaticToDynamic(MapObject& obj);

		/*!
		@description 

		Sets the map pre-rendering parameters, threshold==0 means always set on

		@locality

		@version  [VBS2Fusion v2.71]

		@param mapDrawThreshold -  This is used to say if map prerendering took too long. 

		@param mapScaleDealy -  Sets delay after which scaling is switched off.

		@param txtCheckInterval - Sets how frequently 2d map is checked for pre rendering.

		@param maxObjectListChange - 

		@return bool

		@example

		@code

		@endcode

		@overloaded None

		@related 

		@remarks 
		*/
		static bool applyMapPrerender(double mapDrawThreshold, double mapScaleDealy, double txtCheckInterval, int maxObjectListChange);

	};

};

#endif	//VBS2FUSION_RTEUTILITIES_H