/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	EffectsUtilities.h

Purpose:

	This file contains the declaration of the EffectsUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			02-11/2011	NDB: Original Implementation
										
/************************************************************************/

#ifndef VBS2FUSION_EFFECT_UTILITIES_H
#define VBS2FUSION_EFFECT_UTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <string>

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "position3D.h"
#include "data/ControllableObject.h"
//#include "Vector3f.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/


namespace VBS2Fusion
{


	class VBS2FUSION_API EffectsUtilities
	{

	public:
		/*!
		Creates an Effect of given EFFECTS_TYPE , which is attached to the given ControllableObject.
		Returns an integer which is the reference number for the created effect.
		The returned reference number can be passed to the following functions to manipulate the effect
			- void DeleteEffect(int referenceNo)
			- void applyParticleCircle(int referenceNo, float radius, vector3D& velocity)
			- void applyParticleRandom(int referenceNo, PARTICLE_RANDOM_ARRAY& randomArray)
			- void applyDropInterval(int referenceNo, float interval)
			- void applyParticleParams(int referenceNo, ControllableObject& object, PARTICLE_PARAMS_ARRAY& paramsArray)
			- void applyParticleParams(int referenceNo, PARTICLE_PARAMS_ARRAY& paramsArray)
		*/
		static int CreateEffectWithReference(ControllableObject& object, EFFECTS_TYPE effectsType);

		/*!
		Creates an Effect of given EFFECTS_TYPE , which is attached to the given ControllableObject.
		Returns an integer which is the reference number for the created effect.
		The returned reference number can be passed to the following functions to manipulate the effect
			- void DeleteEffect(int referenceNo)
			- void applyParticleCircle(int referenceNo, float radius, vector3D& velocity)
			- void applyParticleRandom(int referenceNo, PARTICLE_RANDOM_ARRAY& randomArray)
			- void applyDropInterval(int referenceNo, float interval)
			- void applyParticleParams(int referenceNo, ControllableObject& object, PARTICLE_PARAMS_ARRAY& paramsArray)
			- void applyParticleParams(int referenceNo, PARTICLE_PARAMS_ARRAY& paramsArray)
		*/
		static int CreateEffectWithReference(ControllableObject& object, PARTICLE_PARAMS_ARRAY& paramsArray, PARTICLE_RANDOM_ARRAY& randomArray, float dropInterval);

		/*!
		Creates Effects in VBS environment according to given EFFECTS_TYPE (void return type)
		*/
		static void CreateEffects(ControllableObject& object, EFFECTS_TYPE effectsType);

		/*!
		Creates Effects in VBS environment according to given parameters (void return type)
		*/
		static void CreateEffects(ControllableObject& object, PARTICLE_PARAMS_ARRAY& paramsArray, PARTICLE_RANDOM_ARRAY& randomArray, float dropInterval);

		
		/*!
		Create a Light in VBS environment according to given parameters
		which is attached to the given ControllableObject and 
		Returns an integer which is the reference number for the created light.
		The lights are clearly visible during the night time of the simulation.
		The returned reference number can can be passed to the following function to delete the light object
			- void DeleteEffect(int referenceNo)
		*/
		static int CreateLightObjWithRef(ControllableObject& object, Color_RGB lightColor, Color_RGB ambientColor, float brightness, position3D& relatedPosition);

		/*!
		Create Light in VBS environment according to given parameters.
		The light can see the in night time.(void Method)
		*/
		static void CreateLight(ControllableObject& object, Color_RGB lightColor, Color_RGB ambientColor, float brightness, position3D& relatedPosition);
		
		/*!
		Creates a particle effect.

		This command is used to create smoke, fire and similar effects.

		The particles are single polygons with single textures that always face the player.
		They can be set to dynamically change their position, size, direction, can be set to different weights and more or less dependant on the wind. 
		*/
		static void CreateDynamicEffects(PARTICLE_PARAMS_ARRAY& particleArray);
		
		/*!
		Creates a particle effect.

		This command is used to create smoke, fire and similar effects.

		The particles are single polygons with single textures that always face the player.
		They can be set to dynamically change their position, size, direction, can be set to different weights and more or less dependant on the wind. 
		*/
		static void CreateDynamicEffects(DYNAMIC_PARTICLE_PARAMS& particleArray);

		/*!
		Deletes the Effect particles and the created Effect 
		referenceNo - The reference number of the particle which is returned from a CreateEffect function or a CreateLight function.
		*/
		static void DeleteEffect(int referenceNo);

		/*!
		Update particle source to create particles on circle with given radius.
		Velocity is transformed and added to total velocity. 
		referenceNo - The reference number returned from a CreateEffect function.
		*/
		static void applyParticleCircle(int referenceNo, float radius, vector3D& velocity);

		/*!
		Set randomization of particle source parameters. 
		referenceNo - The reference number returned from a CreateEffect function.
		*/
		static void applyParticleRandom(int referenceNo, PARTICLE_RANDOM_ARRAY& randomArray);

		/*!
		Set interval of emitting particles from particle source. 
		referenceNo - The reference number returned from a CreateEffect function.
		*/
		static void applyDropInterval(int referenceNo, float interval);
 
		/*!
		Set parameters to particle source.
		referenceNo - The reference number returned from a CreateEffect function.
		*/
		static void applyParticleParams(int referenceNo, ControllableObject& object, PARTICLE_PARAMS_ARRAY& paramsArray);

		/*!
		Set parameters to particle source.
		referenceNo - The reference number returned from a CreateEffect function.
		*/
		static void applyParticleParams(int referenceNo, PARTICLE_PARAMS_ARRAY& paramsArray);

		/*!
		Applies the position specified by pos to particle
		within the game environment.  
		The reference number of the particle which is returned from a CreateEffectParticle function.
		*/
		static void applyPosition(int referenceNo, position3D pos);

		/*!
		Creates an empty particle effect at the given position and
		Returns an integer which is the reference number for the created effect.
		The returned reference number can be passed to the following functions to manipulate the effect
			- void DeleteEffect(int referenceNo)
			- void applyParticleCircle(int referenceNo, float radius, vector3D& velocity)
			- void applyParticleRandom(int referenceNo, PARTICLE_RANDOM_ARRAY& randomArray)
			- void applyDropInterval(int referenceNo, float interval)
			- void applyParticleParams(int referenceNo, ControllableObject& object, PARTICLE_PARAMS_ARRAY& paramsArray)
			- void applyParticleParams(int referenceNo, PARTICLE_PARAMS_ARRAY& paramsArray)
			- void applyPosition(int referenceNo, position3D pos)
		*/
		static int CreateEffectParticle(position3D pos);

		/*!
		Creates an empty particle effect locally at the given position and
		Returns an integer which is the reference number for the created effect.
		The returned reference number can be passed to the following functions to manipulate the effect
		- void DeleteEffect(int referenceNo)
		- void applyParticleCircle(int referenceNo, float radius, vector3D& velocity)
		- void applyParticleRandom(int referenceNo, PARTICLE_RANDOM_ARRAY& randomArray)
		- void applyDropInterval(int referenceNo, float interval)
		- void applyParticleParams(int referenceNo, ControllableObject& object, PARTICLE_PARAMS_ARRAY& paramsArray)
		- void applyParticleParams(int referenceNo, PARTICLE_PARAMS_ARRAY& paramsArray)
		- void applyPosition(int referenceNo, position3D pos)
		*/
		static int CreateEffectParticleLocal(position3D pos);

		/*!
		Plays sound from CfgSounds, either already part of VBS2.
		Deprecated, use void applySoundPlay(string soundName) instead.
		*/
		VBS2FUSION_DPR(UEFT001) static void playSound(string soundName);

		/*!
		Plays music defined in description.ext or CfgMusic.
		musicName - Class name of sound to play. If empty ("") the currently played music will stop.
		Deprecated, use void applyMusicPlay(string musicName) instead.
		*/
		VBS2FUSION_DPR(UEFT002) static void playMusic(string musicName);

		/*!
		Plays music defined in description.ext or CfgMusic.
		musicName - Class name of sound to play. If empty ("") the currently played music will stop.
		time - in seconds. Time where the music will start. float time
		Deprecated, use void applyMusicPlay(string musicName, float time) instead.
		*/
		VBS2FUSION_DPR(UEFT003) static void playMusic(string musicName, float time);

		/*!
		Creates a sound source of the given type.

		Type is the name of the subclass of CfgVehicles. If the markers array contains several marker names, the position of a random one is used, otherwise, the given position is used. The sound source is placed inside a circle with this position as its center and placement as its radius.
		To stop the sound, delete the created sound object via ControllableObjectUtilities::deleteObject(). 

		type - String as per CfgSFX
		position - placement position
		markers - vector of markers to specify the possible placement position
		radius - repeated sounds will be generated within this radius around the specified position
		*/
		static ControllableObject CreateSoundSource(string type, position3D position, vector<Marker> markers, float radius);


		/*!
		Invokes flashbang and sets its duration.
		*/
		static void applyFlashBang(float duration);

		/*!
		Detach light from object. 
		*/
		static void applyDetachLight(int referenceNo) ;

		/*!
		@description

		Plays sound from CfgSounds, either already part of VBS2.

		@locality

		Locally applied, Locally effected

		@version [VBS2Fusion v2.70.4]

		@param soundName - Name of the sound file to play.

		@return Nothing.

		@example

		@code

		EffectsUtilities::applySoundPlay("VBS2_ext_AV_door_close_v1");

		@endcode

		@overloaded

		@related

		@remarks This is a replication of void playSound(string soundName) function.
		*/
		static void applySoundPlay(string soundName);

		/*!
		@description

		Plays music defined in description.ext or CfgMusic.

		@locality

		Locally applied, Locally effected

		@version [VBS2Fusion v2.70.4]

		@param musicName - Class name of sound to play. If empty ("") the currently played music will stop.

		@return Nothing.

		@example

		@code

		EffectsUtilities::applyMusicPlay("ATrack27");

		@endcode

		@overloaded

		@related

		@remarks This is a replication of void PlayMusic(string musicName) function.
		*/
		static void applyMusicPlay(string musicName);

		/*!
		@description

		Used to play music defined in description.ext or CfgMusic within the VBS2 environment.

		@locality

		Locally applied, Locally effected

		@version [VBS2Fusion v2.70.4]

		@param musicName - Class name of sound to play. If empty ("") the currently played music will stop.
		@param time - The time frame/point from which the music should start playing. (i.e if 3 sec is given the music will skip the first 3 seconds of the music file and  continue playing from the 3rd second).

		@return Nothing.

		@example

		@code

		EffectsUtilities::applyMusicPlay("ATrack10",3.0);

		@endcode

		@overloaded

		@related

		@remarks This is a replication of void PlayMusic(string musicName, float time) function.
		*/
		static void applyMusicPlay(string musicName, float time);

	};
	typedef EffectsUtilities EffectsUtilties;
};

#endif //EFFECT_UTILITIES_H