/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

MarkerUtilities.h

Purpose:

This file contains the declaration of the MarkerUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0		12-04/2011  CGS: Original Implementation
/************************************************************************/

#ifndef VBS2FUSION_MARKER_UTILITIES_H
#define VBS2FUSION_MARKER_UTILITIES_H

#include <sstream>
#include <string>

#include "position3D.h"
#include "position2D.h"
#include "VBS2Fusion.h"
#include "data/Marker.h"
#include "util/ExecutionUtilities.h"

namespace VBS2Fusion
{

	class VBS2FUSION_API MarkerUtilities
	{
	public:

		/*!
		Returns the position of the Marker in position3D format. Third coordinate 
		would always be zero. In other words y coordinate is zero.
		*/
		static position3D getPosition(Marker& marker);		

		/*!
		Applies the position obtained to the Marker. Position format would be Position2D.
		Even though we give the Position3D VBS2 doesn't take the y coordinate. It will be assigned as zero 
		*/
		static void applyPosition(Marker& marker, position3D position);

		/*!
		Applies the position obtained to the Marker locally. Position format would be Position2D.
		Even though we give the Position3D VBS2 doesn't take the y coordinate. It will be assigned as zero 
		*/
		static void applyPositionLocal(Marker& marker, position3D position);

		/*!
		Applies the position obtained marker.getPosition() to the Marker. 
		*/
		static void applyPosition(Marker& marker);

		/*!
		Applies the position obtained marker.getPosition() to the Marker locally. 
		*/
		static void applyPositionLocal(Marker& marker);

		/*!
		Update the marker position. 
		*/
		static void updatePosition(Marker& marker);

		/*!
		Returns the Marker text. 
		*/
		static string getText(Marker& marker);

		/*!
		Applies the text obtained through text to the Marker. 
		*/
		static void applyText(Marker& marker, string text);

		/*!
		Applies the text obtained through text to the Marker locally. 
		*/
		static void applyTextLocal(Marker& marker, string text);

		/*!
		Applies the text obtained through marker.getText() to the Marker. 
		*/
		static void applyText(Marker& marker);

		/*!
		Applies the text obtained through marker.getText() to the Marker. 
		*/
		static void applyTextLocal(Marker& marker);

		/*!
		Updates the Marker text. 
		*/
		static void updateText(Marker& marker);

		/*!
		Creates a new marker at the given position. The marker name given in the 
		Marker object has to be unique. In order to create a marker successfully 
		at least the MarkerType needs to be defined in the marker object.
		*/
		static void createMarker(Marker& marker);

		/*!
		Creates a new marker locally at the given position. The marker name given in the 
		Marker object has to be unique. In order to create a marker successfully 
		at least the MarkerType needs to be defined in the marker object.
		*/
		static void createMarkerLocal(Marker& marker);

		/*!
		Delete the given marker
		*/
		static void deleteMarker(Marker& marker);

		/*!
		Delete the given marker locally
		*/
		static void deleteMarkerLocal(Marker& marker);

		/*!
		Returns the Marker type. 
		*/
		static string getType(Marker& marker);

		/*!
		Applies the type obtained from marker.getMarkerType() to the marker. 
		*/
		static void applyType(Marker& marker, string type);

		/*!
		Applies the type obtained from marker.getMarkerType() to the marker locally. 
		*/
		static void applyTypeLocal(Marker& marker, string type);

		/*!
		Applies the type obtained from type to the marker.
		*/
		static void applyType(Marker& marker);

		/*!
		Applies the type obtained from type to the marker locally.
		*/
		static void applyTypeLocal(Marker& marker);

		/*!
		Updates the Marker type. 
		*/
		static void updateType(Marker& marker);

		/*!
		Returns the Marker color. 
		*/
		static string getColor(Marker& marker);

		/*!
		Applies the color to the marker. 
		*/
		static void applyColor(Marker& marker, string type);

		/*!
		Applies the color to the marker locally. 
		*/
		static void applyColorLocal(Marker& marker, string type);

		/*!
		Applies the color obtained from marker.getColor() to the marker. 
		*/
		static void applyColor(Marker& marker);

		/*!
		Applies the color obtained from marker.getColor() to the marker locally. 
		*/
		static void applyColorLocal(Marker& marker);

		/*!
		Updates the Marker color. 
		*/
		static void updateColor(Marker& marker);

		/*!
		Returns the Marker Shape. 
		*/
		static string getShape(Marker& marker);

		/*!
		Applies the Shape to the marker. 
		*/
		static void applyShape(Marker& marker, string type);

		/*!
		Applies the Shape to the marker locally. 
		*/
		static void applyShapeLocal(Marker& marker, string type);

		/*!
		Applies the Shape obtained from marker.getShape() to the marker. 
		*/
		static void applyShape(Marker& marker);

		/*!
		Applies the Shape obtained from marker.getShape() to the marker locally. 
		*/
		static void applyShapeLocal(Marker& marker);

		/*!
		Updates the Marker Shape. 
		*/
		static void updateShape(Marker& marker);

		/*!
		Returns the Marker Direction. 
		*/
		static double getDirection(Marker& marker);

		/*!
		Applies the Direction to the marker. 
		*/
		static void applyDirection(Marker& marker, double angle);

		/*!
		Applies the Direction to the marker locally. 
		*/
		static void applyDirectionLocal(Marker& marker, double angle);

		/*!
		Applies the Direction obtained from marker.getDirection() to the marker. 
		*/
		static void applyDirection(Marker& marker);

		/*!
		Applies the Direction obtained from marker.getDirection() to the marker locally. 
		*/
		static void applyDirectionLocal(Marker& marker);

		/*!
		Updates the Marker Direction. 
		*/
		static void updateDirection(Marker& marker);

		/*!
		Returns the Marker Brush. 
		*/
		static string getBrush(Marker& marker);

		/*!
		Applies the Brush to the marker. 
		*/
		static void applyBrush(Marker& marker, string type);

		/*!
		Applies the Brush to the marker locally. 
		*/
		static void applyBrushLocal(Marker& marker, string type);

		/*!
		Applies the Brush obtained from marker.getBrush() to the marker. 
		*/
		static void applyBrush(Marker& marker);

		/*!
		Applies the Brush locally obtained from marker.getBrush() to the marker. 
		*/
		static void applyBrushLocal(Marker& marker);

		/*!
		Updates the Marker Brush. 
		*/
		static void updateBrush(Marker& marker);

		/*!
		Returns the Marker Width. 
		*/
		static double getWidth(Marker& marker);

		/*!
		Returns the Marker Height. 
		*/
		static double getHeight(Marker& marker);

		/*!
		Applies the width & height to the marker. 
		*/
		static void applySize(Marker& marker, double width, double height);

		/*!
		Applies the width & height to the marker locally. 
		*/
		static void applySizeLocal(Marker& marker, double width, double height);

		/*!
		Applies the Size obtained from marker object to the marker. 
		*/
		static void applySize(Marker& marker);

		/*!
		Applies the Size obtained from marker object to the marker locally. 
		*/
		static void applySizeLocal(Marker& marker);

		/*!
		Updates the Marker Size. 
		*/
		static void updateSize(Marker& marker);

		/*!
		Attaches a marker to an object.

		Deprecated function.
		Use void MarkerUtilities::applyObjectAttach(Marker& marker, ControllableObject& co, position3D offset) instead.
		*/
		VBS2FUSION_DPR(UMKR004) static void attachToObject(Marker& marker, ControllableObject& co, position3D offset);

		/*!
		Attaches a marker to an object locally.

		Deprecated function.
		Use void MarkerUtilities::applyLocalObjectAttach(Marker& marker, ControllableObject& co, position3D offset) instead.
		*/
		VBS2FUSION_DPR(UMKR005) static void attachToObjectlocal(Marker& marker, ControllableObject& co, position3D offset);

		/*!
		Updates the following properties of Marker object:
		- Position
		- Text
		- Type
		- Color
		- Shape
		- Direction
		- Brush
		- Height and width
		*/
		static void updateMarker(Marker& marker);

		/*!
		Applies all the following properties to the Marker:
		- Position
		- Text
		- Type
		- Color
		- Shape
		- Direction
		- Brush
		- Height and width
		*/
		static void applyChanges(Marker& marker);		

		/*!
		Returns whether the marker is set to "autosize" (resizes with map). 
		*/
		static bool isMarkerAutosize(Marker& marker);

		/*!
		Creates a new marker at the given position on all computers,
		where condition equals true. The marker name has to be unique.
		*/
		static void createMarkerConditional(Marker& marker, string condition);

		/*!
		Gets the condition for marker visibility.
		*/
		static string getMarkerCondition(Marker& marker);

		/*!
		Sets the marker condition, which determines whether the marker will be shown.
		
		Deprecated function.
		Use void MarkerUtilities::applyMarkerCondition(Marker& marker, string condition) instead.
		*/
		VBS2FUSION_DPR(UMKR003) static void setMarkerCondition(Marker& marker, string condition);

		/*!
		If autosize is true, markers will resize in proportion to current map zoom.
		If false (default), markers will always stay the same size.
		The marker is modified on all computers in a network session.

		Deprecated function.
		Use void  MarkerUtilities::applyMarkerAutoSize(Marker& marker, bool condition) instead.
		*/
		VBS2FUSION_DPR(UMKR001) static void setMarkerAutoSize(Marker& marker, bool condition);

		/*!
		If autosize is true, markers will resize in proportion to current map zoom.
		If false (default), markers will always stay the same size. 
		The marker is only modified on the computer where the command is called.

		Deprecated function.
		Use void MarkerUtilities::applyLocalMarkerAutoSize(Marker& marker, bool condition) instead.
		*/
		VBS2FUSION_DPR(UMKR002) static void setMarkerAutoSizeLocal(Marker& marker, bool condition);

		/*!
		Gets the marker attachment settings.  
		Return value : String which can be evaluated and returned an object. 
		*/
		static string getMarkerAttachObjectString(Marker& marker);

		/*!
		Gets the marker attachment settings.  
		Return value : Positional offset of marker to attached object. 
		*/
		static position2D getMarkerAttachOffset(Marker& marker);

		/*!
		Gets the marker attachment settings.  
		Return value : Vector of ControllableObject. 
		*/
		static vector<ControllableObject> getMarkerAttachList(Marker& marker);
		
		/*!
		@description
		
		Defines whether a marker will change size proportional to the map's zoom level.
		The values returned by markerSize are not affected by this setting, only the visual appearance on the map is effected.
		Only works with markers of type Icon.

		@locality

		Globally Applied, Globally Effected.

		@version [VBS2Fusion v2.70.3]

		@param marker - Reference to created marker
		
		@param condition -  If true (default), then the marker size will always stay the same. 
							If false, then marker will be resized with zoom level.

		@return Nothing.

		@example

		@code

		//Marker to be created.
		Marker marker1;

		MarkerUtilities::applyMarkerAutoSize(marker1,false);

		@endcode

		@overloaded 
		
		None

		@related

		None

		@remarks This is a replication of void setMarkerAutoSize(Marker& marker, bool condition) function.
		*/
		static void  applyMarkerAutoSize(Marker& marker, bool condition);

		/*!
		@description 
		Returns defined marker layers.
		
		@locality

		Globally applied

		@version  [VBS2Fusion v2.70.3]

		@param marker - Marker object.
		
		@return vector<MARKER_LAYER> - This is container of MARKER_LAYER. if considered instance of container is LyrList.
				LyrList.path    - Path to layer icon.
				LyrList.sizeX   - Size of layer in X.
				LyrList.sizeY   - Size of layer in Y.	
				LyrList.offsetX	- Offset X of the layer from center of the marker
				LyrList.offsetY - Offset Y of the layer from center of the marker

		@example

		@code

		//Define the vector v1
		vector<MARKER_LAYER> v1;

		//Assign the return value to v1
		v1 = MarkerUtilities::getMarkerLayers(marker1);

		//Display the paths
		for(vector<MARKER_LAYER>::iterator itr = v1.begin();itr !=v1.end();itr++)
		{
		displayString += "\\nPath : " +itr->path;
		}

		@endcode

		@overloaded None.

		@related
		
		@remarks 	
		*/
		static vector<MARKER_LAYER> getMarkerLayers(Marker& marker);

		/*!
		@description 
		Defines an action performed when a marker is created.
		
		@locality

		Globally applied globally effected

		@version  [VBS2Fusion v2.70.3]

		@param marker - A script command which need to execute.
		
		@return Nothing.

		@example

		@code

		//Display the string when the marker is created
		MarkerUtilities::applyOnMarkerCreated(string("hint 'Marker is created'"));

		@endcode

		@overloaded None.

		@related
		void MarkerUtilities::applyOnMarkerDeleted(string command)
		
		@remarks 	
		*/
		static void applyOnMarkerCreated(string command);

		/*!
		@description 
		Defines an action performed when a marker is deleted.
		
		@locality

		Globally applied globally effected

		@version  [VBS2Fusion v2.70.3]

		@param marker - A script command which need to execute.
		
		@return Nothing.

		@example

		@code

		//Display the string when the marker is deleted
		MarkerUtilities::applyOnMarkerDeleted(string("hint 'Marker is deleted'"));

		@endcode

		@overloaded None.

		@related
		void MarkerUtilities::applyOnMarkerCreated(string command)
		
		@remarks 	
		*/
		static void applyOnMarkerDeleted(string command);
		
		/*!
		@description

		If autosize is true, markers will resize in proportion to current map zoom.
		If false (default), markers will always stay the same size.

		@locality

		Locally Applied, Locally Effected

		@version [VBS2Fusion v2.70.3]

		@param marker - Reference to created marker

		@param condition -  If true (default), then the marker size will always stay the same. 
		If false, then marker will be resized with zoom level.

		@return Nothing.

		@example

		@code
		
		//The Marker to be created
		Marker marker1;

		MarkerUtilities::applyLocalMarkerAutoSize(marker1,false);

		@endcode

		@overloaded  
		
		None

		@related

		None
		
		@remarks The marker is only modified on the computer where the command is called.
		
		@remarks This is a replication of void setMarkerAutoSizeLocal(Marker& marker, bool condition) function.			
		*/
		static void applyLocalMarkerAutoSize(Marker& marker, bool condition);

		/*!
		@description

		Sets the marker condition, which determines whether the marker will be shown.

		@locality

		Locally Applied, Locally Effected

		@version [VBS2Fusion v2.70.3]

		@param marker - Reference to created marker

		@param condition -  Condition to be fulfilled

		@return Nothing.

		@example

		@code

		//Marker to be created
		Marker marker1;

		MarkerUtilities::applyMarkerCondition(marker1,string("side player == WEST"));

		@endcode

		@overloaded  
		
		None

		@related

		MarkerUtilities::getMarkerCondition(Marker& marker)

		@remarks This is a replication of void setMarkerCondition(Marker& marker, string condition) function.
		*/
		static void applyMarkerCondition(Marker& marker, string condition);

		/*!
		@description
		Attaches a marker to an object.
		The marker will also be attached to the object in RTE according to these parameters.

		@locality

		@version [VBS2Fusion v2.70.3]

		@param marker - Reference to created merker 
		@param co - Reference to created object. This code is evaluated and has to return an object. 
		The return value of the last argument (list) can be used as '_this' in the evaluation.
		@param offset -  Positional offset of marker to object

		@return Nothing.

		@example

		@code

		@overloaded  None.

		@related

		@remarks This is a replication of void attachToObject(Marker& marker, ControllableObject& co, position3D offset) function.
		*/
		static void applyObjectAttach(Marker& marker, ControllableObject& co, position3D offset);

		/*!
		@description
		Attaches a marker to an object.
		The marker will also be attached to the object in RTE according to these parameters.

		@locality

		@version [VBS2Fusion v2.70.3]

		@param marker - Reference to created merker 
		@param co - Reference to created object. This code is evaluated and has to return an object. 
		The return value of the last argument (list) can be used as '_this' in the evaluation.
		@param offset -  Positional offset of marker to object

		@return Nothing.

		@example

		@code

		@overloaded  None.

		@related

		@remarks This is a replication of void attachToObjectlocal(Marker& marker, ControllableObject& co, position3D offset) function.
		*/
		static void applyLocalObjectAttach(Marker& marker, ControllableObject& co, position3D offset);
	};

};

#endif //MARKER_UTILITIES_H