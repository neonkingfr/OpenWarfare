
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:	AARUtilities.h

Purpose: This file contains After Action Review utility function definitions. 

	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			17-06/2010	YFP: Original Implementation
	2.01		29-09-2011	SSD: Added Functions
									int addBookMark(double, string, string);
									void removeBookMark(int);
									string getBookMarkName(int);
									double getBookMarkTime(int);
									string getBookMarkMessage(int);
									int getTotalBookMarks();
	2.02		04-10-2011	SSD: Added Functions
									int getStat(ControllableObject, string);
									int getStat(Group, string);
									int getStat(SIDE, string);
									int getSaveState();
									vector<string> getFileInfo();
									void goToNextMessage();
									void goToPreviousMessage();

/*****************************************************************************/
#ifndef VBS2FUSION_AARUTILITIES_H
#define VBS2FUSION_AARUTILITIES_H

/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES
#include <string>
#include <vector>

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "data/ControllableObject.h"
#include "data/Group.h"

/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBS2Fusion
{
	class VBS2FUSION_API AARUtilities
	{
	public:
		/*!
		Returns true if AAR file is loaded. 
		*/
		static bool isLoaded();
		
		/*!
		Returns current time within the AAR Mission.
		Deprecated. Use void getCurrentTime()
		*/
		VBS2FUSION_DPR(UAAR006) static double currentTime();

		/*!
		Returns true if the AAR mission is paused.
		*/
		static bool isPaused();

		/*!
		Returns true if the AAR is in play mode.
		*/
		static bool isPlaying();

		/*!
		Returns true if the AAR is recording.
		*/
		static bool isRecording();

		/*!
		Returns true if the AAR is in Repeat mode.
		*/
		static bool isRepeating();

		/*!
		Specify the file to load for the AAR.If sucessful it returns true.
		Deprecated. Use bool applyAARLoad(string& fileName)
		*/
		VBS2FUSION_DPR(UAAR001) static bool load(string fileName);

		/*!
		starts playing a loaded AAR file.
		Deprecated. Use void applyAARPlay()
		*/
		VBS2FUSION_DPR(UAAR002) static void play();

		/*!
		starts recording on the current machine.
		Deprecated. Use void applyAARRecord()
		*/
		VBS2FUSION_DPR(UAAR010) static void record();

		/*!
		Returns the length of the current recorded AAR mission.
		Deprecated. Use double getReplayLength()
		*/
		VBS2FUSION_DPR(UAAR007) static double replayLength();

		/*!
		Pauses the current playback. call again resumes play.
		Deprecated. Use void applyAARPause()
		*/
		VBS2FUSION_DPR(UAAR003) static void pause();

		/*!
		Restart the AAR replay.
		Deprecated. Use void applyAARStart()
		*/
		VBS2FUSION_DPR(UAAR004) static void start();

		/*!
		Saves the recorded mission to the specified file. 
		Deprecated. Use void applyAARSave(string fileName)
		*/
		VBS2FUSION_DPR(UAAR012) static void save(string fileName);

		/*!
		set whether the AAR playback should be in repeat mode or not.
		Deprecated. Use void applyAARRepeat(bool value)
		*/
		VBS2FUSION_DPR(UAAR008) static void setRepeat(bool value);

		/*!
		moves to the specified time in recording. 
		Deprecated. Use void applyAARPlayBackTime(double time)
		*/
		VBS2FUSION_DPR(UAAR009) static void setPlayBackTime(double time);

		/*!
		stops playing of the current mission.
		Deprecated. Use void applyAARStop()
		*/
		VBS2FUSION_DPR(UAAR005) static void stop();

		/*!
		stop recording AAR information.
		Deprecated. Use void applyAARRecordingStop()
		*/
		VBS2FUSION_DPR(UAAR011) static void stopRecording();

		/*!
		unload the currently loaded AAR file.
		Deprecated. Use void applyAARUnload() 
		*/
		VBS2FUSION_DPR(UAAR013) static void unload();

		/*!
		Adds a bookmark into AAR. Returned index is used to refer the book mark later. 
		(-1) is returned if the operation fails.
		time(in seconds) - Time to associate the book mark with. 
		name - Name for the book mark.
		message - Message (description) to be associated with the book mark.

		Deprecated. Use int applyAARBookMarkAddition(double time, string& name, string& message)
		*/
		VBS2FUSION_DPR(UAAR014) static int addBookMark(double time, string name, string message);

		/*!
		Removes the book mark specified by the index.
		Deprecated. Use void applyAARBookMarkRemove(int index)
		*/
		VBS2FUSION_DPR(UAAR015) static void removeBookMark(int index);

		/*!
		Returns the name of the book mark specified by the index.
		An empty string is returned if the operation fails.
		*/
		static string getBookMarkName(int index);

		/*!
		Returns the time associated to the book mark specified by the index.
		(-1) is returned if the operation fails.
		*/
		static double getBookMarkTime(int index);

		/*!
		Returns the message associated to the book mark specified by the index.
		An empty string is returned if the operation fails.
		*/
		static string getBookMarkMessage(int index);

		/*!
		Returns the total number of book marks in the current AAR.
		(-1) is returned if the operation fails.
		*/
		static int getTotalBookMarks();

		/*!
		Returns the scalar value on the specified statistic type for the object.
		Statistic types: roundsFired, enemyKilled, friendlyKilled, enemyWounded and friendlyWounded.
		(-1) is returned if the operation fails.
		*/
		static int getStat(ControllableObject co, string type);

		/*!
		Returns the scalar value on the specified statistic type for the group.
		Statistic types: roundsFired, enemyKilled, friendlyKilled, enemyWounded and friendlyWounded.
		(-1) is returned if the operation fails.
		*/
		static int getStat(Group group, string type);

		/*!
		Returns the scalar value on the specified statistic type for the group.
		Statistic types: roundsFired, enemyKilled, friendlyKilled, enemyWounded and friendlyWounded.
		(-1) is returned if the operation fails.
		*/
		static int getStat(SIDE side, string type);


		/*!
		Returns the state of saving of AAR.
		(-2) : Recording, (-1) : Currently saving, 0 : File saving successful.
		If returned value is greater than 0, error has occurred while saving.
		*/
		static int getSaveState();

		/*!
		Returns the currently loaded AAR filename with it's folder path.
		0th position of the returned contains the folder path and 1st position contains the file name.
		If no AAR file is loaded empty strings will be returned.
		*/
		static vector<string> getFileInfo();

		/*!
		Returns string representing network ID of the object which the object had during AAR recording.
		IDs of objects are not the same during AAR recording and AAR replay.
		@param co - object to which the check to be made
		@return Object's string ID 
		@remarks This does not return all 3 parts of NetworkId but only the first 2 parts since the third part is not valid 
		during AAR replay but the first 2 parts of the returned string can be used as unique identifier. Eg: "2:2"
		@remarks This is needed when you want to identify object during AAR replay and you only know id from the time when 
		the AAR was recorded. If the object can't be found the returned value would be empty string
		*/
		static string getOriginalId(ControllableObject& object);


#ifdef DEVELOPMENT
		/*!
		Go to the next message on the list (debugging). 
		*/
		static void goToNextMessage();

		/*!
		Go to the previous message on the list (debugging). 
		*/
		static void goToPreviousMessage();



#endif

		/*!
		@description
		
		Specify the file to load for the AAR.

		@locality

		Locally Applied, Locally Effected

		@version [VBS2Fusion v2.70.3]

		@param fileName - Name of the AAR file.

		@return bool - If file is loaded successfully, returns true otherwise returns false.

		@example

		@code

		AARUtilities::applyAARLoad(string("C:\\Users\\<Your user profile>\\Documents\\VBS2\\AAR\\2013_4_11_10_44.LastMission.Intro.aar"));

		@endcode

		@overloaded 

		None

		@related

		AARUtilities::isLoaded()

		@remarks Before calling this function there needs to be a recorded AAR before and the AAR name passed in the function needs to be similar to the recorded AAR name. 

		@remarks This function would only load the AAR, but will not play the AAR. To play the AAR you would need to call AARUtilities::applyAARPlay();

		@remarks This is a replication of bool load(string fileName) function.
		*/
		static bool applyAARLoad(string& fileName);

		/*!
		@description
		
		Starts playing the loaded AAR file.

		@locality

		Locally Applied, Globally Effected

		@version [VBS2Fusion v2.70.3]

		@param Nothing.

		@return Nothing.

		@example

		@code

		AARUtilities::applyAARPlay();

		@endcode

		@overloaded 

		None

		@related

		AARUtilities::isPlaying()

		@remarks Before calling this function there needs to be a recorded AAR.

		@remarks This is a replication of void play() function.
		*/
		static void applyAARPlay();

		/*!
		@description
		
		Pauses the current playback. Call again to resume the playback. 

		@locality

		Locally Applied, Globally Effected

		@version [VBS2Fusion v2.70.3]

		@param Nothing.

		@return Nothing.

		@example

		@code

		AARUtilities::applyAARPause();

		@endcode

		@overloaded

		None

		@related

		AARUtilities::isPause()

		@remarks This is a replication of void pause() function.
		*/
		static void applyAARPause();

		/*!
		@description
		
		Starts playing the currently paused playback or restart the current playback.  

		@locality

		Locally Applied, Globally Effected

		@version [VBS2Fusion v2.70.3]

		@param Nothing.

		@return Nothing.

		@example

		@code

		AARUtilities::applyAARStart();

		@endcode

		@overloaded 

		None

		@related

		AARUtilities::applyAARPlay()

		AARUtilities::applyAARPause()

		@remarks This is a replication of void start() function.
		*/
		static void applyAARStart();

		/*!
		@description
		
		Stops playing of the current playback.  

		@locality

		Locally Applied, Globally Effected

		@version [VBS2Fusion v2.70.3]

		@param Nothing.

		@return Nothing.

		@example

		@code

		AARUtilities::applyAARStop();

		@endcode

		@overloaded 

		None

		@related

		AARUtilities::applyAARPlay()

		AARUtilities::applyAARPause()

		AARUtilities::applyAARStart()

		@remarks This is a replication of void stop() function.
		*/
		static void applyAARStop();

		/*!
		@description
		
		Returns current time within the AAR Mission. 

		@locality

		Locally Applied, Locally Effected

		@version [VBS2Fusion v2.70.3]

		@param Nothing.

		@return double - current time in seconds 

		@example

		@code

		displayString = "Current time of the AAR : " + conversions::DoubleToString(AARUtilities::getCurrentTime());

		@endcode

		@overloaded 

		None

		@related

		None

		@remarks The AAR should be playing to get the current time of the AAR.

		@remarks This is a replication of double currentTime() function.
		*/
		static double getCurrentTime();


		/*!
		@description

		Returns the length of the current recorded AAR mission. 

		@locality

		Locally Applied, Locally Effected

		@version [VBS2Fusion v2.70.3]

		@param Nothing.

		@return double - Length of the recorded mission in seconds 

		@example

		@code

		displayString = "Length of AAR : " + conversions::DoubleToString(AARUtilities::getReplayLength());

		@endcode

		@overloaded 

		None

		@related

		None

		@remarks This is a replication of double replayLength() function.
		*/
		static double getReplayLength();

		/*!
		@description

		Set whether the AAR playback should be in repeat mode or not. If bool value is set to true the AAR keeps on repeating. 

		@locality

		Locally Applied, Globally Effected

		@version [VBS2Fusion v2.70.3]

		@param value - If true enables the repeat mode.

		@return Nothing.

		@example

		@code

		AARUtilities::applyAARRepeat(true);

		@endcode

		@overloaded 

		None

		@related

		None 

		@remarks This is a replication of void setRepeat(bool value) function.
		*/
		static void applyAARRepeat(bool value);

		/*!
		@description

		Moves to the specified time in recording. 

		@locality

		Locally Applied, Globally Effected

		@version [VBS2Fusion v2.70.3]

		@param time - Time to be moved in AAR playback in seconds.

		@return Nothing.

		@example

		@code

		AARUtilities::applyAARPlayBackTime(5);

		@endcode

		@overloaded 

		None

		@related

		None

		@remarks This is a replication of void setPlayBackTime(double time) function.
		*/
		static void applyAARPlayBackTime(double time);

		/*!
		@description

		Starts recording on the current mission.  

		@locality

		Globally Applied, Locally Effected

		@version [VBS2Fusion v2.70.3]

		@param Nothing.

		@return Nothing.

		@example

		@code

		AARUtilities::applyAARRecord();

		@endcode

		@overloaded 

		None

		@related

		AARUtilities::applyAARRecordingStop()

		@remarks This is a replication of void record() function.
		*/
		static void applyAARRecord();

		/*!
		@description
  
		Stops the current recording of the mission. 

		@locality

		Globally Applied, Locally Effected

		@version [VBS2Fusion v2.70.3]

		@param Nothing.

		@return Nothing.

		@example

		@code
		
		AARUtilities::applyAARRecordingStop();

		@endcode

		@overloaded 

		None

		@related

		AARUtilities::applyAARRecord()

		@remarks This is a replication of void stopRecording() function.
		*/
		static void applyAARRecordingStop();

		/*!
		@description
  
		Saves the recorded mission to the specified file. 

		@locality

		Locally Applied, Locally Effected

		@version [VBS2Fusion v2.70.3]

		@param fileName - Name of the file, recorded mission to be saved.

		@return Nothing.

		@example

		@code

		AARUtilities::applyAARSave("myAARFile");

		@endcode

		@overloaded 

		None

		@related

		None

		@remarks This is a replication of void save(string fileName) function.
		*/
		static void applyAARSave(string fileName);

		/*!
		@description
  
		Unloads the currently loaded AAR file.

		@locality

		Locally Applied, Globally Effected

		@version [VBS2Fusion v2.70.3]

		@param Nothing.

		@return Nothing.

		@example

		@code

		AARUtilities::applyAARUnload();

		@endcode

		@overloaded 

		None

		@related

		AARUtilities::applyAARLoad();

		@remarks This is a replication of void unload() function.
		*/
		static void applyAARUnload();

		/*!
		@description
  
		Adds a bookmark into AAR.

		@locality

		Locally Applied, Locally Effected

		@version [VBS2Fusion v2.70.3]

		@param time - Time to associate the book mark with (in seconds). 
		
		@param name - Name for the book mark.
		
		@param message - Message (description) to be associated with the book mark.

		@return int - Returned index is used to refer the book mark later. (-1) is returned if the operation fails.

		@example

		@code

		int bookMarkIndex = AARUtilities::applyAARBookMarkAddition(5,string("myBookMark"),string("This is a bookmark"));

		@endcode

		@overloaded 

		None

		@related
		
		AARUtilities::applyAARBookMarkRemove(int index)

		AARUtilities::getBookMarkName(int index);
		
		AARUtilities::getBookMarkTime(int index);
		
		AARUtilities::getBookMarkMessage(int index);

		@remarks This is a replication of int addBookMark(double time, string name, string message) function.
		*/
		static int applyAARBookMarkAddition(double time, string& name, string& message);

		/*!
		@description
  
		Remove selected bookmark from the list.

		@locality

		Locally Applied, Locally Effected

		@version [VBS2Fusion v2.70.3]

		@param index - Index of the book mark to be removed.

		@return Nothing.

		@example

		@code

		AARUtilities::applyAARBookMarkRemove(bookMarkIndex);

		@endcode

		@overloaded 

		None

		@related

		AARUtilities::applyAARBookMarkAddition(double time, string& name, string& message)

		@remarks This is a replication of void removeBookMark(int index) function.
		*/
		static void applyAARBookMarkRemove(int index);

		/*!
		@description 

		Returns "inkspots" settings, which were defined via setDrawStationary.

		@locality

		Globally Effected

		@version  [VBS2Fusion v2.71]

		@param unit - Unit that we want to get the coresponding "inkspots" settings.

		@return DRAWSTATIONARY This struct contain the coresponding "inkspot" settings, which were defined via setDrawStationary.

		@example

		@code

		DRAWSTATIONARY _drawstationary;
		player = PlayerUtilities::getPlayer();
		ControllableObjectUtilities::updateDynamicProperties(player);
		ControllableObjectUtilities::updateStaticProperties(player);
		_drawstationary = AARUtilities::getAARStationaryDraw(player);

		@endcode

		@overloaded None

		@related 

		@remarks 
		*/
		static DRAWSTATIONARY getAARStationaryDraw(Unit& unit);
	};

};

#endif