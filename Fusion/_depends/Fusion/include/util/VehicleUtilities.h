/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

VehicleUtilities.h

Purpose:

This file contains the declaration of the VehicleUtilities class.

Version Information:

Version		Date		Author and Comments
===========================================
1.0			02-04/2009	RMR: Original Implementation

2.0			10-02/2010  YFP: Added	void animateVehicle( Vehicle& vehicle ,string animationName,double phase)
									void createVehicleLocal(Vehicle& vehicle)
									Unit getEffectiveCommander(Vehicle& vehicle)
									void animateVehicle( Vehicle& vehicle ,string animationName,double phase)
2.01		10-02/2010	MHA: Comments Checked
2.02		14-06/2010  YFP: Added void createVehicle(Vehicle& , string, string)

2.03		02-07/2010  YFP: Methods added,
									void setManualControl(Vehicle&,bool)
									void setTurnWanted(Vehicle&,double)
									void setThrustWanted(Vehicle&,double)
2.04		05-07/2010  YFP: Methods added,
									void startEngine(Vehicle&,bool);
2.05		10-01/2011  CGS: Methods added getVehicleGroup(Vehicle& vehicle)
							Modified All createVehicle
								createAndAddWaypoint(Vehicle& vehicle, Waypoint& wp)
2.06		14-09/2011	NDB: Methods Added,
								vector<CrewPos> getCrewPosition(Vehicle& vehicle)
								vector<string> getMuzzles(Vehicle& vehicle, Turret::TurretPath& turretPath)
								bool isEngineDisabled(Vehicle& vehicle)
								void applyEngineDisable(Vehicle& vehicle, bool state)
								void applyTowParent(Vehicle& vehicle1, Vehicle& vehicle2)
								Vehicle getTowParent(Vehicle& vehicle)
								void applyBlockSeat(Vehicle& vehicle, VEHICLEASSIGNMENTTYPE type, int index = 0, bool isblock = true)
								void applyBlockSeat(Vehicle& vehicle, vector<int> index, bool isblock = true)
								vector<BLOCKEDSEAT> getBlockedSeats(Vehicle& vehicle)
								int getCargoIndex(Vehicle& vehicle, Unit& unit)
								Unit getUnitAtCargoIndex(Vehicle& vehicle, int index)
								void applyTurretLockOn(Vehicle& vehicle, Turret::TurretPath turretPath, position3D& position,bool trackHidden = false)
								position3D getTurretLockedOn(Vehicle& vehicle, Turret::TurretPath turretPath)
								double getLaserRange(Vehicle& vehicle, Unit& gunner)
								void applyLaserRange(Vehicle& vehicle, double range)
								void applyLaserRange(Vehicle& vehicle, Unit& gunner, double range)
								int getLasingStatus(Vehicle& vehicle, Unit& gunner)
								void applyOpticsOffset(Vehicle& vehicle, Turret::TurretPath turretPath, double azimuth, double elevation, bool transition = true)
								vector<double> getOpticsOffset(Vehicle& vehicle, Unit& gunner)
								vector<double> getOpticsOffset(Vehicle& vehicle, Turret::TurretPath turretPath)
								void applyIndicators(Vehicle& vehicle, bool left, bool hazard, bool right)
								void applyMaxFordingDepth(Vehicle& vehicle, double depth)
								double getMaxFordingDepth(Vehicle& vehicle)
								Unit getTurretUnit(Vehicle& vehicle, Turret::TurretPath turretPath)
2.07		19-09/2011	NDB: Added Methods
								void applyThrustLeftWanted(Vehicle& vehicle, double factor)
								void applyThrustRightWanted(Vehicle& vehicle, double factor)
								void applyFireArc(Vehicle& vehicle, Turret::TurretPath& turretPath, vector3D& direction, double sideRange, double verticalRange, bool relative)
								void applyCommanderOverride(Vehicle& vehicle, Unit& unit, Turret::TurretPath turretPath)
								void land(Vehicle& vehicle, LANDMODE mode)
								void sendToVehicleRadio(Vehicle& vehicle, string name)
								void vehicleChat(Vehicle& vehicle, string message)
								bool isEngineOn(Vehicle& vehicle)
								void commandGetOut(Unit& unit)
								void doGetOut(Unit& unit)
								void applyVehicleId(Vehicle& vehicle, int id)
								void respawnVehicle(Vehicle& vehicle, double delay = -1, int count = 0)
								bool isTurnedOut(Unit& unit)
								void autoAssignVehicle(Unit& unit)
2.08		26-09/2011	NDB: Added Methods
								void assignAsCommander(Vehicle& vehicle, Unit& unit, double delay)
								void assignAsDriver(Vehicle& vehicle, Unit& unit, double delay)
								void assignAsGunner(Vehicle& vehicle, Unit& unit, double delay)
								void assignAsCargo(Vehicle& vehicle, Unit& unit, double delay)
								void orderGetIn(vector<Unit> unitVec, bool order)
								void orderGetIn(Unit& unit, bool order)
								vector<WEAPONCARGO> getWeaponFromCargo(Vehicle& vehicle)
								vector<MAGAZINECARGO> getMagazineFromCargo(Vehicle& vehicle)
								vector<string> getMuzzles(Vehicle& vehicle)
								position3D getAimingPosition(Vehicle& vehicle, ControllableObject& target)
2.09		26-09/2011	NDB:	Added Method void commandMove(Vehicle& vehicle, position3D position)
2.10		04-11/2011	NDB: Added Mehthods
								bool isKindOf(Vehicle& vehicle, string typeName)
								float knowsAbout(Vehicle& vehicle, ControllableObject& target)
2.11		08-11-2011	SSD		void applyName(Vehicle& vehicle)
2.12        13-01-2012  RDJ     Add Methods
								static void landTo(Vehicle& vehicle, int port)
								static void assignToAirport(Vehicle& vehicle, int port)
2.12		18-01-2012	RDJ		Add the method islocked(Vehicle& vehicle)
2.13		18-01-2012	RDJ		Add the method void moveInCargoWithPosition(Unit& unit,Vehicle& vehicle, int cargoIndex)
2.14		19-01-2012  RDJ		Add the Method void groupLeaveVehicle(Group& group, Vehicle& vehicle);
2.15		19-01-2012	RDJ		Add the Method void groupLeaveVehicle(Unit& unit, Vehicle& vehicle);



/************************************************************************/ 

#ifndef VBS2FUSION_VEHICLE_UTILITIES_H
#define VBS2FUSION_VEHICLE_UTILITIES_H
/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>

// SIMCENTRIC INCLUDES
#include "position3D.h"
#include "VBS2Fusion.h"
#include "data/Vehicle.h"
#include "data/Unit.h"
#include "util/ExecutionUtilities.h"
#include "VBS2FusionAppContext.h"
#include "WaypointUtilities.h"
#include "ControllableObjectUtilities.h"
#include "data/vbs2Types.h"
#include "data/VehicleArmedModule.h"

/**********************************************************************/
/* END INCLUDES
/**********************************************************************/


namespace VBS2Fusion
{
	struct CrewPos
	{
		string type;
		position3D pos;
		int cargoID;
		Unit unit;
		Turret::TurretPath turret;
	};

	class VBS2FUSION_API VehicleUtilities : public ControllableObjectUtilities
	{
	public:	

		//********************************************************
		// Load utilities
		//********************************************************

		/*!
		Loads all units belonging to vehicle onto the vehicleList. Random
		aliases are assigned to each loaded unit. The network ID and name of 
		each unit is also loaded along with its role.  
		*/
		static void loadUnits(Vehicle& vehicle);		

		/*!
		Loads all Waypoints belonging to vehicle onto the waypointList. Random aliases
		are assigned to each waypoint. 
		*/
		static void loadWaypoints(Vehicle& vehicle);		

		////**************************************************************		

		/*!
		Loads all units and Waypoints belonging to the vehicle. 	
		*/
		static void loadVehicleUnitsAndWaypoints(Vehicle& vehicle);		

		/*!
		Updates the following:
		- The vehicle members list
		- The waypoint list. 
		- position
		- direction
		- isAlive
		- group name
		- damage
		- fuel
		- type
		- positionASL
		- the current waypoint. 

		This function uses the network ID of the object to access it within the game environment, so please
		ensure that either a registered alias is assigned to vehicle. 
		*/
		static void updateVehicle(Vehicle& vehicle);

		
		//********************************************************
		// position utilities
		//********************************************************


		/*!
		Returns the position of the vehicle in position3D format. 
		*/
		static position3D getPosition(Vehicle& object);

		/*!
		Updates the position of the vehicle.  
		*/
		static void updatePosition(Vehicle& vehicle);

		/*!
		Applies the position defined by position to the vehicle. Value is 
		not updated on the vehicle variable. Use update to verify that changes have been made. 
		*/
		static void applyPosition(Vehicle& vehicle, position3D position);

		/*!
		Applies the position defined by vehicle.getPosition() to the vehicle. 
		*/
		static void applyPosition(Vehicle& vehicle);


		//********************************************************
		// Direction utilities
		//********************************************************

		/*!
		Returns the direction of the vehicle. 
		*/
		static double getDirection(Vehicle& vehicle);

		/*!
		Updates the direction of the vehicle. 
		*/
		static void updateDirection(Vehicle& unit);

		/*!
		Applies the direction defined by direction to the vehicle. Value is 
		not updated on the vehicle variable. Use update to verify that changes have been made. 
		*/
		static void applyDirection(Vehicle& vehicle, double direction);

		/*!
		Applies the direction defined by vehicle.getDir() to the vehicle. 
		*/
		static void applyDirection(Vehicle& vehicle);


	
		//********************************************************
		// Group utilities
		//********************************************************	


		static Group getVehicleGroup(Vehicle& vehicle);

		/*!
		Returns the name of the group the vehicle belongs to. 
		*/
		static string getGroup(Vehicle& vehicle);		

		/*!
		Updates the name of the group the vehicle belongs to. 
		*/
		static void updateGroup(Vehicle& unit);


		//********************************************************
		// Vehicle alive utilities
		//********************************************************	
	
		/*!
		Updates the alive status of the vehicle. 
		*/
		static void updateAlive(Vehicle& unit);


		//********************************************************
		// Damage utilities
		//********************************************************	
			
		/*!
		Returns the current damage level of the vehicle. 
		*/
		static double getDamage(Vehicle& vehicle);

		/*!
		Updates the current damage level of the vehicle. 
		*/
		static void updateDamage(Vehicle& unit);

		/*!
		Applies the damage level specified by damage to the vehicle. Value is 
		not updated on the vehicle variable. Use update to verify that changes have been made. 
		*/
		static void applyDamage(Vehicle& vehicle, double damage);

		/*!
		Applies the damage level specified by vehicle.getDamage() to the vehicle. 
		*/
		static void applyDamage(Vehicle& vehicle);

	

		//********************************************************
		// Fuel utilities
		//********************************************************		

		/*!
		Returns the current fuel level of the vehicle. 
		*/
		static double getFuel(Vehicle& vehicle);

		/*!
		Updates the current fuel level of the vehicle. 
		*/
		static void updateFuel(Vehicle& unit);

		/*!
		Applies the fuel level specified by fuel to the vehicle. Value is 
		not updated on the vehicle variable. Use update to verify that changes have been made. 
		*/
		static void applyFuel(Vehicle& vehicle, double fuel);		

		/*!
		Applies the fuel level specified by vehicle.getFuel() to the vehicle. 
		*/
		static void applyFuel(Vehicle& vehicle);

		//********************************************************
		// Waypoint utilities
		//********************************************************		

		/*!
		Creates a new waypoint (i.e. creates it in the game environment) and
		adds it to the vehicle. 
		*/
		static void createAndAddWaypoint(Vehicle& vehicle, Waypoint& wp);

		/*!
		Returns the index of the current waypoint. 
		*/
		static int getCurrentWaypoint(Vehicle& vehicle);			

		/*!
		Updates the index of the current waypoint. 
		*/
		static void updateCurrentWaypoint(Vehicle& vehicle);


		//********************************************************
		// Vehicle create utilities
		//********************************************************	

		/*!
		Creates a new vehicle as defined by the parameters present in vehicle. 

		- If no alias is set, a random alias is assigned. 
		- The type of vehicle defined by vehicle.getType() is used.
		- creates the vehicle at the position defined by vehicle.getPosition(). 
		- If no group is specified, a new group is created and the vehicle is added to that group. 
		*/
		static void createVehicle(Vehicle& vehicle);

		/*!
		Create a new vehicle as defined by parameters of the vehicle. 
		If the string of markers array contains several marker names, the position of a random one is used.
		Otherwise, the given position is used. The vehicle is placed inside a circle with this position 
		as center and placement as its radius. 
		Special properties can be:"NONE", "FLY" and "FORM".
		*/
		static void createVehicle(Vehicle& vehicle, string markerNames, string specialProperties);

		/*!
		Creates a new vehicle as defined by the parameters present in the vehicle locally.
		*/
		static void createVehicleLocal(Vehicle& vehicle);
		
		//********************************************************
		// Vehicle delete utilities
		//********************************************************	

		/*!
		deletes the vehicle as defined by the parameters of vehicle. 
		*/
		static void deleteVehicle(Vehicle& vehicle);

		//********************************************************
		// Vehicle group utilities
		//********************************************************	

		/*!
		Adds vehicle to the group defined by group. The group name of vehicle is changed
		to vehicle.setGroup(). 
		*/
		static void addToGroup(Vehicle& vehicle, Group& group);		


		/*!
		returns the commander of the group. 
		*/
		static Unit getEffectiveCommander(Vehicle& vehicle);

		//********************************************************
		// Vehicle move in utilities
		//********************************************************

		/*!
		Creates a new unit as defined by unit and moves the unit
		into vehicle as the driver. 		
		*/
		static void createAndMoveInAndAsDriver(Vehicle& vehicle, Unit& unit);

		/*!
		Moves in an already created unit into vehicle as driver. 

		This function uses the network ID of the object to access it within the game environment, so please
		ensure that either a registered alias is assigned to vehicle. 
		Deprecated. Use void applyMoveInAsDriver(Vehicle& vehicle, Unit& unit)
		*/
		static void moveInAndAsDriver(Vehicle& vehicle, Unit& unit);	

		/*!
		Creates a new unit as defined by unit and moves the unit
		into vehicle as turret.

		It takes the default turret
		[1,0]: second sub-turret of first main turret. 
		*/
		static void createAndMoveInAndAsTurret(Vehicle& vehicle, Unit& unit);

		
		/*!
		Moves in an already created unit into vehicle as turret.

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle.

		It take default turret
		[0,1]: second sub-turret of first main turret.

		Deprecated. Use void applyMoveInAsTurret(Vehicle& vehicle, Unit& unit)
		*/
		static void moveInAndAsTurret(Vehicle& vehicle, Unit& unit);

		/*!
		Creates a new unit as defined by unit and moves the unit
		into vehicle as turret.

		Available turrets can be determined by the getCrewPos command, and is defined as an array, containing either one or two positions definitions, 
		e.g.
		[0]: first turret (no sub-turrets exist).
		[0,0]: first sub-turret of first main turret.
		[1,0]: second sub-turret of first main turret. 
		*/
		static void createAndMoveInAndAsTurret(Vehicle& vehicle, Unit& unit, string turrntPath);

		
		/*!
		Creates a new unit as defined by unit and moves the unit
		into vehicle as turret.

		Available turrets can be determined by the getCrewPos command, and is defined as an array, containing either one or two positions definitions, 
		e.g.
		[0]: first turret (no sub-turrets exist).
		[0,0]: first sub-turret of first main turret.
		[1,0]: second sub-turret of first main turret. 
		*/
		static void createAndMoveInAndAsTurret(Vehicle& vehicle, Unit& unit, Turret turretPath);

		/*!
		Moves in an already created unit into vehicle as turret.

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle.

		Available turrets can be determined by the getCrewPos command, and is defined as an array, containing either one or two positions definitions, 
		e.g.
		[0]: first turret (no sub-turrets exist).
		[0,0]: first sub-turret of first main turret.
		[1,0]: first sub-turret of second main turret.

		Deprecated. Use void applyMoveInAsTurret(Vehicle& vehicle, Unit& unit, string turretPath)
		*/
		static void moveInAndAsTurret(Vehicle& vehicle, Unit& unit, string turretPath);

		/*!
		Moves in an already created unit into vehicle as turret.

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle.

		Available turrets can be determined by the getCrewPos command, and is defined as an array, containing either one or two positions definitions, 
		e.g.
		[0]: first turret (no sub-turrets exist).
		[0,0]: first sub-turret of first main turret.
		[1,0]: first sub-turret of second main turret.

		Deprecated. Use void applyMoveInAsTurret(Vehicle& vehicle, Unit& unit, Turret turret)
		*/
		static void moveInAndAsTurret(Vehicle& vehicle, Unit& unit, Turret turretPath);

		/*!
		Creates a new unit as defined by unit and moves the unit
		into vehicle as cargo. 		
		*/
		static void createAndMoveInAndAsCargo(Vehicle& vehicle, Unit& unit);

		/*!
		Moves in an already created unit into vehicle as cargo. 

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 
		Deprecated. Use void applyMoveInAsCargo(Vehicle& vehicle, Unit& unit)
		*/
		static void moveInAndAsCargo(Vehicle& vehicle, Unit& unit);		

		/*!
		Creates a new unit as defined by unit and moves the unit
		into vehicle as the commander. 
		*/
		static void createAndMoveInAndAsCommander(Vehicle& vehicle, Unit& unit);

		/*!
		Moves in an already created unit into vehicle as commander. 

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 
		Deprecated. Use void applyMoveInAsCommander(Vehicle& vehicle, Unit& unit)
		*/
		static void moveInAndAsCommander(Vehicle& vehicle, Unit& unit);

		/*!
		Creates a new unit as defined by unit and moves the unit
		into vehicle as gunner. 
		*/
		static void createAndMoveInAndAsGunner(Vehicle& vehicle, Unit& unit);

		/*!
		Moves in an already created unit into vehicle as gunner. 

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 
		Deprecated. Use void applyMoveInAsGunner(Vehicle& vehicle, Unit& unit)
		*/
		static void moveInAndAsGunner(Vehicle& vehicle, Unit& unit);



		//********************************************************
		// Vehicle leave utilities
		//********************************************************

		/*!
		Orders unit to leave vehicle. 

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 
		Deprecated. Use void applyUnitLeave(Vehicle& vehicle, Unit& unit)
		*/
		static void leaveVehicle(Vehicle& vehicle, Unit& unit);


		/*!
		Orders unit with ID defined by unit.getID() to leave vehicle.

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 
		Deprecated. Use void applyUnitLeave(Vehicle& vehicle, Unit& unit)
		*/
		static void leaveVehicleByID(Vehicle& vehicle, Unit& unit); 

		/*!
		Orders unit with Name defined by unit.getName() to leave vehicle.

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 
		Deprecated. Use void applyUnitLeave(Vehicle& vehicle, Unit& unit)
		*/
		static void leaveVehicleByName(Vehicle& vehicle, Unit& unit); 


		/*!
		Orders unit with Alias defined by unit.getAlias() to leave vehicle.

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 
		Deprecated. Use void applyUnitLeave(Vehicle& vehicle, Unit& unit)
		*/
		static void leaveVehicleByAlias(Vehicle& vehicle, Unit& unit);		

		//*******************************************************/

		/*!
		Activate and deactivate the engine of the vehicle. If state is true, 
		engine will be on and it will stop when state is false.
		Deprecated. Use void applyEngineStart(Vehicle& vehicle , bool state)
		*/
		static void startEngine(Vehicle& vehicle , bool state);

		/*!
		Enable/ disable manual control of the vehicle.
		if control is TRUE  vehicle AI disabled and it can be controlled 
		by setTurnWanted & setThrustWanted functions. 
		Deprecated. Use void applyManualControl(Vehicle& vehicle , bool control)
		*/
		static void setManualControl(Vehicle& vehicle , bool control);

		/*!
		set the turn wanted for the vehicle specified by the factor value.
		manual control of the vehicle should be enabled first. 
		Deprecated. Use void applyTurnWanted(Vehicle& vehicle, double factor)
		*/
		static void setTurnWanted(Vehicle& vehicle, double factor);

		/*!
		set the thrust wanted for the vehicle to passed in. 
		manual control of the vehicle should be enabled first.
		Deprecated. Use void applyThrustWanted(Vehicle& vehicle, double factor)
		*/
		static void setThrustWanted(Vehicle& vehicle , double factor);

		//********************************************************
		// Animate utilities
		//********************************************************

		/*!
		Animate Vehicle. 
		animationName - The name of the animation
		phase - the phase value is between 0 and 1.  

		Deprecated. Use void applyAnimation(Vehicle& vehicle, string animationName, double phase)
		*/
		static void animateVehicle( Vehicle& vehicle ,string animationName,double phase);

		/*!
		Returns current gear of the vehicle. 
		*/
		static int getCurrentGear(Vehicle& vehicle);


		/*!
		Returns current RPM of the vehicle. 
		*/
		static float getCurrentRPM(Vehicle& vehicle);
		
		/*!
		Returns engine strength coefficient of the vehicle.
		*/
		static double getEngineStrengthCoefficient(Vehicle& vehicle);

		/*!
		Applies vehicle engine strength coefficient.
		*/
		static void applyEngineStrengthCoefficient(Vehicle& vehicle);

		/*!
		Applies specified engine strength coefficient value to the vehicle.    
		*/
		static void applyEngineStrengthCoefficient(Vehicle& vehicle , double strengthCoeff);

		/*!
		Returns maximum speed limit of the vehicle.
		*/
		static double getMaximumSpeedLimit(Vehicle& vehicle);

		/*!
		Set vehicle's maximum speed. Maximum speed limit should be in Km/h . 
		If maximum speed limit is less than 0. Then the vehicle's maximum speed limit 
		sets to its default value.
		*/
		static void applyMaximumSpeedLimit(Vehicle& vehicle , double maxSpeedLimit);

#if WEAPON_HANDLING
		/*!
		Returns the name of the Vehicle's primary weapon (an empty string if there is none).	
		*/
		static string getPrimaryWeapon(Vehicle& vehicle);

		/*!
		Returns the Weapon Object with Current weapon details.	
		*/
		static Weapon getWeaponState(Vehicle& vehicle, Turret::TurretPath& tPath);

		/*!
		Updates the primary weapon of the Vehicle. 
		*/
		static void updatePrimaryWeapon(Vehicle& vehicle);

		/*
		Updates the list of weapon types assigned to the Vehicle. 
		Each weapon is assigned with ammo value of 0. 

		Note: This function does not update the ammo amounts on
		the weapons magazines. Use updateWeaponAmmo to update the weapon types with ammo.
		*/
		static void updateWeaponTypes(Vehicle& vehicle);

		/*!
		Select the Weapon given by the muzzle Name.
		Deprecated. Use void applyWeaponSelection(Vehicle& vehicle, string muzzleName)
		*/
		static void selectWeapon(Vehicle& vehicle, string muzzleName);

		/*
		Updates the amount of ammo left in the primary magazine of each assigned 
		weapon type. Uses the list defined by the weapons list within VehicleArmedModule, 
		and therefore will only load ammo information for each weapon on that list. 
		Should be used after updateWeaponTypes is called. 	
		*/
		static void updateWeaponAmmo(Vehicle& vehicle);

		/*!
		Add magazine to the Vehicle.
		Deprecated. Use void applyMagazineAddition(Vehicle& vehicle, string magName)
		*/
		static void addMagazine(Vehicle& vehicle, string name);

		/*!
		Count number of magazines available in given Vehicle object.
		Deprecated. Use int getMagazineCount(Vehicle& vehicle)
		*/
		static int countMagazines(Vehicle& vehicle);

		/*!
		Updates the list of magazines assigned to a vehicle's Armed Module. This function loads all 
		magazines assigned to the vehicle from VBS2 and adds a new Magazine object to Magazine list

		Note: This function does not update the ammo amount within Magazine list. 
		So within MagazineList ammo count would be zero.Use updateMagazineAmmo to update this list. 
		*/
		static void updateMagazineTypes(Vehicle& vehicle);

		/*!
		Updates the magazine ammo count & magazine name inside the vehicleArmedModule's MagazineList
		therefore update the magazine list with required details. This function is enough to update
		MagazineList correctly. No need to use updateMagazineTypes function before using this function.
		*/
		static void updateMagazineAmmo(Vehicle& vehicle);

		/*!
		Load Turrets available in the Vehicle to armed module. 
		*/
		static void updateTurrets(Vehicle& vehicle);

		//----------------------------------------------------------------------------

		/*!
		Get the elevation of the weapon in specified turret of the Armed Vehicle from VBS2 Environment.
		turret object should have the correct turret path.
		*/
		static double getElevation(Vehicle& vehicle, Turret& turret);

		/*! 
		Get azimuth of the weapon in specified turret of the armed vehicle from VBS2 Environment.  
		turret object should have the correct turret path.
		*/
		static double getAzimuth(Vehicle& vehicle, Turret& turret);
	
		/*!
		Returns the weapons position in model space. Weapon should have correct name.	
		*/
		static position3D getWeaponPosition(Vehicle& vehicle, Weapon& weapon);

		/*!
		Returns the weapons position in model space. turret should have the correct turret path.
		*/
		static position3D getWeaponPosition(Vehicle& vehicle, Turret& turret);

		/*!
		Returns the weapon firing point for a selected muzzle in model space.	
		*/
		static position3D getWeaponPoint(Vehicle& vehicle, Weapon& weapon);

		/*!
		Returns the weapon firing point for a selected muzzle in model space.	
		*/
		static position3D getWeaponPoint(Vehicle& vehicle, Turret& turret, Weapon& weapon);

		/*! 
		Get the aiming direction of the specified Weapon of the turret in armed vehicle.
		This method returns a unit vector of the direction. 
		*/
		static position3D getWeaponDirectionVector(Vehicle& vehicle, Turret& turret);

		/*! 
		set reload state of the weapon.
		Deprecated. Use void applyWeaponState(Vehicle& vehicle, Weapon& weapon, bool reloadStatus, double reloadTime)
		*/
		static void setWeaponState(Vehicle& vehicle,Weapon& weapon, bool reloadStatus,double reloadTime);

		/*!
		Returns the list of objects the weapon is aiming at.
		*/
		static list<ControllableObject> getWeaponAimingTargets(Vehicle& vehicle, string weaponName);

		/*!
		Returns the list of objects the weapon is aiming at. Weapon object should have correct weapon name.
		*/
		static list<ControllableObject> getWeaponAimingTargets(Vehicle& vehicle, Weapon& weapon);

		/*!
		Remove the specified magazine from the Vehicle.	
		Deprecated. Use void applyMagazineRemove(Vehicle& vehicle, string name)
		*/
		static void removeMagazine(Vehicle& vehicle, string name);

		/*!
		Remove all magazine from the vehicle.	
		Deprecated. Use void applyMagazineRemoveAll(Vehicle& vehicle, string name)
		*/
		static void removeallMagazines(Vehicle& vehicle, string name);

		/*!
		Add Weapon to the Vehicle. weapon slots are filled, any further addWeapon commands are ignored. 
		To ensure that the weapon is loaded at the start of the mission, at least one magazine should 
		be added, before adding the weapon.
		Deprecated. Use void applyWeaponAddition(Vehicle& vehicle, string name)
		*/
		static void addWeapon(Vehicle& vehicle, string name);

		/*!
		Remove the specified Weapon from the unit.	
		Deprecated. Use void applyWeaponRemove(Vehicle& vehicle, string name)
		*/
		static void removeWeapon(Vehicle& vehicle, string name);

		/*!
		Tells if a unit has the given weapon.
		Deprecated. Use bool isWeaponAvailable(Vehicle& vehicle, string name)
		*/
		static bool hasWeapon(Vehicle& vehicle, string name);

		/*!
		Sets the direction of the Turret Weapon according to azimuth& elevation. 
		Deprecated. Use void applyWeaponDirection(Vehicle& vehicle, Turret& turret, double azimuth, double elevation, bool transition)
		*/
		static void setWeaponDirection(Vehicle& vehicle, Turret& turret, double azimuth, double elevation, bool transition);

		/*!
		Returns how much ammunition (compared to a full state defined by the unit type) the vehicle has. 
		Value would be between 0 to 1.	
		*/
		static float getAmmoRatio(Vehicle& vehicle);

		/*!
		Sets how much ammunition (compared to a full state defined by the unit type) the vehicle has. 
		The value ranges from 0 to 1.	
		Deprecated. Use void applyVehicleAmmo(Vehicle& vehicle, double ammovalue)
		*/
		static void setVehicleAmmo(Vehicle& vehicle, float ammovalue);

		/*!
		Disables gunner input at the turret.
		Deprecated. Use void applyGunnerInputDisable(Vehicle& vehicle, Turret& turret, bool status = true)
		*/
		static void disableGunnerInput(Vehicle& vehicle, Turret& turret, bool status = true);

		/*!
		Add weapons to the cargo space of vehicles, which can be taken out by infantry units. 
		Once the weapon cargo space is filled up, any further addWeaponCargo commands are ignored.
		Deprecated. Use void applyWeaponCargo(Vehicle& vehicle, string weaponName, int count)
		*/
		static void addWeaponToCargo(Vehicle& vehicle, string weaponName, int count);

		/*!
		Add magazines to the cargo space of vehicles, which can be taken out by infantry units. 
		Once the magazine cargo space is filled up, any further addMagazineCargo commands are ignored.
		Deprecated. Use void applyMagazineCargo(Vehicle& vehicle, string magName, int count)
		*/
		static void addMagazineToCargo(Vehicle& vehicle, string magName, int count);

		/*!
		Remove all weapons from the given vehicle's weapon cargo space.
		Deprecated. Use void applyWeaponCargoRemoveAll(Vehicle& vehicle)
		*/
		static void clearAllWeaponFromCargo(Vehicle& vehicle);

		/*!
		Remove all magazines from the given vehicle's magazine cargo space.
		Deprecated. Use void applyMagazineCargoRemoveAll(Vehicle& vehicle)
		*/
		static void clearAllMagazineFromCargo(Vehicle& vehicle);

		/*!
		Enables or Disables firing on current weapon in the selected turret. If true, the unit's current 
		weapon cannot be fired.
		Deprecated. Use void applyWeaponSafety(Vehicle& vehicle, Turret& turret, bool safe)
		*/
		static void setWeaponSafety(Vehicle& vehicle, Turret& turret, bool safe);

		/*!
		Return the current status of the weapon safety in the selected turret.
		*/
		static bool getWeaponSafety(Vehicle& vehicle, Turret& turret);

		/*!
		Updates all the Armed properties of the unit:   
		*/
		static void updateVehicleArmedProperties(Vehicle& vehicle);

		/*!
		Order the Armed Vehicle to fire on the given target (via the radio).
		Deprecated. Use void applyCommandFire(Vehicle& vehicle, ControllableObject& co)
		*/
		VBS2FUSION_DPR(UVHC002) static void commandFire(Vehicle& vehicle, ControllableObject& co);

		/*!
		Order the Armed Vehicle to fire on the given target (without radio messages). 
		Deprecated. Use void applyFire(Vehicle& vehicle, ControllableObject& co)
		*/
		VBS2FUSION_DPR(UVHC001) static void doFire(Vehicle& vehicle, ControllableObject& co);

		/*!
		Forces the Armed Vehicle to fire from the specifically named weapon. 
		Deprecated. Use void applyFire(Vehicle& vehicle, string weaponName)
		*/
		static void fire(Vehicle& vehicle, string weaponName);

		/*!
		Forces the Armed Vehicle to fire according to the parameters given. 
		Deprecated. Use void applyFire(Vehicle& vehicle, string muzzleName, string modeName)
		*/
		static void fire(Vehicle& vehicle, string muzzleName, string modeName);

		/*!
		Forces the Armed Vehicle to fire according to the parameters given. 
		Deprecated. Use void applyFire(Vehicle& vehicle, string muzzleName, string modeName, string magazineName, position3D& pos, bool aim)
		*/
		static void fire(Vehicle& vehicle, string muzzleName, string modeName, string magazineName, position3D& pos, bool aim);

		/*!
		Apply the network shell mode. Normally shells are locally simulated following a fired event.
		When network shell mode is set, the simulation is broadcast to the network. 
		Deprecated. Use void applyNetworkShellMode(Vehicle& vehicle,bool mode)
		*/
		static void setNetworkShellMode(Vehicle& vehicle,bool mode);

#endif

	/*!
	Returns the gunner of a vehicle. 
	*/
	static Unit getGunner(Vehicle& vehicle);

	/*!
	Returns the commander of a vehicle. 
	*/
	static Unit getCommander(Vehicle& vehicle);

	/*!
	Returns the driver of a vehicle. 
	*/
	static Unit getDriver(Vehicle& vehicle);

	/*!
	Returns a list of arrays, containing information about each seat in the vehicle:

	type - Proxy type. Can be "driver", "gunner", "commander" or "cargo".
	position - Seat position in model space (i.e. relative to the vehicle).
	cargoID - Index of cargo position.
	unit - Unit occupying that seat. objNull if empty.
	turret - Turret path. For non-gunner positions this element is not returned. 
	*/
	static vector<CrewPos> getCrewPosition(Vehicle& vehicle);

	/*!
	Return all the muzzles of a turret
	*/
	static vector<string> getMuzzles(Vehicle& vehicle, Turret::TurretPath& turretPath);

	/*!
	Returns whether a vehicle's engine was disabled
	*/
	static bool isEngineDisabled(Vehicle& vehicle);


	/*!
	Disables/enables the engine of a land vehicle.

	A disabled engine will keep the vehicle from moving, but will not affect its damage status.
	Engines that have been destroyed by in-game actions (e.g. by driving through water), can also be re-enabled with this command.
	Disabled engines can be fixed by a repair truck.

	state - true to disable the engine on, false to enable it.
	*/
	static void applyEngineDisable(Vehicle& vehicle, bool state);

#ifdef DEVELOPMENT
	/*!
	Causes a vehicle (even if it's empty) to adjust its steering and braking to follow the tow vehicle.
	vehicle1 - Vehicle that is being towed
	vehicle2 - Vehicle that is towing vehcle1. If set to objNull, automatic steering/braking is disabled.
	*/
	static void applyTowParent(Vehicle& vehicle1, Vehicle& vehicle2);

	/*!
	Returns the vehicle that is defines as the tow vehicle by applyTowParent. 
	*/
	static Vehicle getTowParent(Vehicle& vehicle);
#endif

	/*!
	Marks a vehicle seat as blocked, so that neither AI nor players can use it.

	Blocking already used seats will not remove the occupying unit.
	A third optional boolean parameter can be used to unblock the seat. 

	vehicle: Object
	seat - Type of seat (proxy): Either "Driver", "Cargo" or "Commander" (the latter means a commander seat, not the commanding unit)
	index - Index of cargo position or turret definition. Optional for "Driver" and "Commander".
	blocked - Whether to block the seat or not. Optional (default is true).
	*/
	static void applyBlockSeat(Vehicle& vehicle, VEHICLEASSIGNMENTTYPE type, int index = 0, bool isblock = true);

	/*!
	Marks a vehicle seat as blocked, so that neither AI nor players can use it.

	Blocking already used seats will not remove the occupying unit.
	A third optional boolean parameter can be used to unblock the seat. 
	It is only for type of "Turret"
	vehicle: Object
	index - turret definition. 
	blocked - Whether to block the seat or not. Optional (default is true).
	*/
	static void applyBlockSeat(Vehicle& vehicle, vector<int> index, bool isblock = true);

	/*!
	Returns list of seats that were blocked
	*/
	static vector<BLOCKEDSEAT> getBlockedSeats(Vehicle& vehicle);

	/*!
	Returns the position of the unit in cargo.
	The first cargo position is 0. If unit is not in vehicle, -1 is returned.
	*/
	static int getCargoIndex(Vehicle& vehicle, Unit& unit);

	/*!
	Returns the unit for the specified cargo index.
	If the tested cargo position is empty, objNull is returned.
	*/
	static Unit getUnitAtCargoIndex(Vehicle& vehicle, int index);

	/*!
	@description

	Locks a turret onto an object

	@locality

	Locally Applied Globally Effected

	@version [VBS2Fusion v2.70.2]

	@param vehicle - vehicle to lock
	
	@param turretpath - turret path to be locked
	
	@param co - Object to lock on to.

	@param trackhidden - (optional, default: false)If it is true tracking will be continued even after unit entered
				   into vehicle, otherwise continuous tracking won't be there.

    @return Nothing

	@example

	@code

	//Vehicle to be created
	Vehicle v;

	Turret::TurretPath tupath1;
	tupath1.resize(1);
	tupath1.at(0) = 0;
	tur.setTurretPath(tupath1);
	VehicleUtilities::updateTurrets(_vehicle);

	//apply the turret lock on to the player unit
	VehicleUtilities::applyTurretLockOn(_vehicle,tupath1,player,true);

	@endcode

	@overloaded VehicleUtilities::applyTurretLockOn(Vehicle& vehicle, Turret::TurretPath turretPath, position3D& position)

	@related  VehicleUtilities::getTurretLockedOn(Vehicle& vehicle, Turret::TurretPath turretPath

	@remarks
	*/
	static void applyTurretLockOn(Vehicle& vehicle, Turret::TurretPath& turretPath, ControllableObject& co,bool trackHidden = false);

	/*!
	Locks a turret onto an object or ASL position

	vehicle - vehicle to lock
	turretpath - turret to be locked
	position - What to lock on to. Locking can be disabled by passing [0,0,0]
	*/
	static void applyTurretLockOn(Vehicle& vehicle, Turret::TurretPath turretPath, position3D& position);

	/*!
	Returns the target position a turret is locked on.
	Returns the position of the object if the turret is locked on an object.
	[0,0,0] if it is not locked. 
	*/
	static position3D getTurretLockedOn(Vehicle& vehicle, Turret::TurretPath turretPath);

	/*!
	Returns the currently lased range. (Does not work on water.)
	returns -1 if gunner is not operating a range finder weapon
	*/
	static double getLaserRange(Vehicle& vehicle, Unit& gunner);

	/*!
	Overrides the actual laser measurements (which are used to calculate the ballistic calculations) with a value provided by this command.
	This value will be shown in the gunner's optics view, until he does another manual lasing.

	vehicle - 
	gunner - Unit that is manning the affected weapon. 
	range - Distance (in meters) to be used in ballistics calculations.
	*/
	static void applyLaserRange(Vehicle& vehicle, Unit& gunner, double range);

	

	/*!
	Sets a new offset between weapon orientation and optics. Note: Lasing will change the elevation offset. 
	This function works only if the gunner is the player.
	*/
	static void applyOpticsOffset(Vehicle& vehicle, Turret::TurretPath turretPath, double azimuth, double elevation, bool transition = true);

	/*!
	Returns the offset between weapon orientation and optics.
	*/
	static vector<double> getOpticsOffset(Vehicle& vehicle, Unit& gunner);

	/*!
	Returns the current status of laser rangefinder (LRF)
	(corresponds with the in-game status color):

	0 - lasing not available (unit or vehicle position not equipped with LRF)

	1 - [red]: not lased (distance cannot be determined, e.g. water, air)

	2 - [yellow]: lasing in progress (distance is being determined)

	3 - [green]: lased (distance has been determined and displayed) 
	*/
	static int getLasingStatus(Vehicle& vehicle, Unit& gunner);

	/*!
	Controls turn indicators and hazard lights on vehicles (true=on, false=off). 

	left - Turns left indicator lights on and off. (If true, hazard light will be turned off.)
	hazard - Turns hazard lights on and off. (If true, indicator light will be turned off.)
	right - Turns right indicator lights on and off. (If true, hazard light will be turned off.)
	*/
	static void applyIndicators(Vehicle& vehicle, bool left, bool hazard, bool right);

	/*!
	Sets the new fording depth in meters for objects of the classes motorcycle, helicopter, tank and car. 
	Setting the max fording depth to -1 will cause the engine to revert to the default configuration value.
	*/
	static void applyMaxFordingDepth(Vehicle& vehicle, double depth);

	/*!
	Returns the fording depth of a vehicle in meters. 
	*/
	static double getMaxFordingDepth(Vehicle& vehicle);

	/*!
	Returns the gunner in the turret path specified. 
	*/
	static Unit getTurretUnit(Vehicle& vehicle, Turret::TurretPath turretPath);

	/*!
	Brings an aircraft to a flying altitude. Either planes or helicopters will take off realistically to reach this height
	Deprecated. Use double applyAirborne(Vehicle& vehicle)
	*/
     static double makeAirborne(Vehicle& vehicle);
 
	 /*!
	 Returns whether or not the aircraft is currently airborne. 
	 */
	 static bool isAirborne(Vehicle& vehicle);


	 /*!
	 Checks whether the light of a vehicle is turned on.
	 (If used with other objects than vehicles may always return true or false.) 
	 */
	 static bool isLightOn(Vehicle& vehicle);

	 /*!
	 Returns either the default fly-in height of aircraft (independently of its current altitude), or the value that was set via flyInHeight.
	 */
	 static double getFlyInHeight(Vehicle& vehicle);


	 /*!
	 Sets the flying altitude for aircraft.
	 Avoid too low altitudes, as helicopters and planes won't evade trees and obstacles on the ground.
	 The default flying altitude depends on the "flyInHeight" definition in the vehicle's configuration. 
	
	 Deprecated. Use void applyHeightFlyIn(Vehicle& vehicle, double height)
	 */
	 static void flyInHeight(Vehicle& vehicle, double height);

	 /*!
	 Set object's orientation (given as direction and up vector).
	 Since version +1.30 accepts additional parameter which, when object is attached, 
	 specifies if vectors applied are relative to parent object's orientation.  

	 Deprecated. Use void applyVectorDirAndUp(Vehicle veh, vector3D vectorDir, vector3D vectorUp, bool relative)
	 */
	 static void setVectorDirAndUp(Vehicle veh,vector3D vectorDir, vector3D vectorUp, bool relative);

	 /*!
	 Set the passed thrust wanted for the left track of entity. Only for tracked vehicles. Vehicle needs to have the controller enabled
	 */
	 static void applyThrustLeftWanted(Vehicle& vehicle, double factor);

	 /*!
	 Set the passed thrust wanted for the right track of entity. Only for tracked vehicles. Vehicle needs to have the controller enabled
	 */
	 static void applyThrustRightWanted(Vehicle& vehicle, double factor);

	 /*!
	 Sets the watch direction and firing arc. In the scenario the AI gunner will only watch and engage targets within the allocated arc. Its set for a turrets gunner specified in parameters.

	 Unlike setWeaponDirection, this command doesn't require disabled gunner input in order to be applied. 

	 direction - Direction vector, acts as center of current arc
	 sideRange - Horizontal width of arc (left/right)
	 verticalRange - Vertical width of arc (up/down)
	 relative - Sets if direction vector is relative to vehicle orientation
	 */
	 static void applyFireArc(Vehicle& vehicle, Turret::TurretPath& turretPath, vector3D& direction, double sideRange, double verticalRange, bool relative);

#ifdef DEVELOPMENT
	 /*!
	 Activates commander override. Pass ObjNull to deactivate. 
	 */
	 static void applyCommanderOverride(Vehicle& vehicle, Unit& unit, Turret::TurretPath turretPath);
#endif

	 /*!
	 Force aircraft landing.

	 Planes will use the airport defined by assignToAirport, or the one closest to them (in this priority).
	 Helicopters will use either the nearest editor-placed helipad, or the nearest flat location (in this priority). assignToAirport has no effect on helicopters, and either of them have to be within 500m, or it will not land.
	 
	 Deprecated. Use void appyCustomizedLanding(Vehicle& vehicle, LANDMODE mode)
	 */
	 static void land(Vehicle& vehicle, LANDMODE mode);

	 /*!
	 Send message to vehicle radio channel. 
	 Deprecated. Use void applyVehicleRadioBroadcast(Vehicle& vehicle, string message)
	 */
	 static void sendToVehicleRadio(Vehicle& vehicle, string name);

	 /*!
	 Type text to vehicle radio channel.

	 This function only types text to the list, it does not broadcast the message. If you want the message to show on all computers, you have to execute it on them.

	 Object parameter must be a vehicle, not a player.
	 If you are in a crew seat (i.e. driver, gunner or commander), then it will include that role in the chat name output (Eg: Driver (you_name): "Message"). 
	 
	 Deprecated. Use void applyVehicleChat(Vehicle& vehicle, string message)
	 */
	 static void vehicleChat(Vehicle& vehicle, string message);

	 /*!
	 Returns true if engine is on, false if it is off. 
	 */
	 static bool isEngineOn(Vehicle& vehicle);

#ifdef DEVELOPMENT
	 /*!
	 Sets id (integer value) to vehicle. By this id vehicle is referenced by triggers and waypoints.
	 */
	 static void applyVehicleId(Vehicle& vehicle, int id);

	 /*!
	 Set vehicle as respawnable in MP games
	 delay - Default -1 (use respawnDelay from Description.ext)
	 count - Default 0 (unlimited respawns)
	 */
	 static void respawnVehicle(Vehicle& vehicle, double delay = -1, int count = 0);
#endif

	 
	 /*!
	 Returns true if the given unit is turned out. Default is true
	 */
	 static bool isTurnedOut(Unit& unit);

#ifdef DEVELOPMENT

	 /*!
	 Causes a unit to find and enter a new available vehicle position, if the position it was assigned to is now taken 
	 (e.g. it was temporarily ejected, to make room for some other unit to take its place). 
	 */
	 static void autoAssignVehicle(Unit& unit);
#endif

	 /*!
	 Assigns a unit as commander of a vehicle. Used together with orderGetIn to order a unit to get in as commander.

	 delay - If unit already has an assigned vehicle role, time in seconds it will stay at the newly assigned position, 
	 before it moves back to the original position, if that is still available. If position has been filled during the delay, 
	 unit will then stay at the new position. If delay is negative, unit will always stay at new position.

	 Deprecated. Use void applyAssignAsCommander(Vehicle& vehicle, Unit& unit, double delay)
	 */
	 static void assignAsCommander(Vehicle& vehicle, Unit& unit, double delay);

	 /*!
	 Assigns a unit as driver of a vehicle. Used together with orderGetIn to order a unit to get in as driver.

	 delay - If unit already has an assigned vehicle role, time in seconds it will stay at the newly assigned position, 
	 before it moves back to the original position, if that is still available. If position has been filled during the delay, 
	 unit will then stay at the new position. If delay is negative, unit will always stay at new position.

	 Deprecated. Use void applyAssignAsDriver(Vehicle& vehicle, Unit& unit, double delay)
	 */
	 static void assignAsDriver(Vehicle& vehicle, Unit& unit, double delay);

	 /*!
	 Assigns a unit as gunner of a vehicle. Used together with orderGetIn to order a unit to get in as gunner.

	 delay - If unit already has an assigned vehicle role, time in seconds it will stay at the newly assigned position, 
	 before it moves back to the original position, if that is still available. If position has been filled during the delay, 
	 unit will then stay at the new position. If delay is negative, unit will always stay at new position.

	 Deprecated. Use void applyAssignAsGunner(Vehicle& vehicle, Unit& unit, double delay)
	 */
	 static void assignAsGunner(Vehicle& vehicle, Unit& unit, double delay);

	 /*!
	 Assigns a unit as cargo of a vehicle. Used together with orderGetIn to order a unit to get in as cargo.

	 delay - If unit already has an assigned vehicle role, time in seconds it will stay at the newly assigned position, 
	 before it moves back to the original position, if that is still available. If position has been filled during the delay, 
	 unit will then stay at the new position. If delay is negative, unit will always stay at new position.

	 Deprecated. Use void applyAssignAsCargo(Vehicle& vehicle, Unit& unit, double delay)
	 */
	 static void assignAsCargo(Vehicle& vehicle, Unit& unit, double delay);

	 /*!
	 Assigns a unit as commander of a vehicle. Used together with orderGetIn to order a unit to get in as commander.
	 Deprecated. Use void applyAssignAsCommander(Vehicle& vehicle, Unit& unit)
	 */
	 static void assignAsCommander(Vehicle& vehicle, Unit& unit);

	 /*!
	 Assigns a unit as driver of a vehicle. Used together with orderGetIn to order a unit to get in as driver.
	 Deprecated. Use void applyAssignAsDriver(Vehicle& vehicle, Unit& unit)
	 */
	 static void assignAsDriver(Vehicle& vehicle, Unit& unit);

	 /*!
	 Assigns a unit as gunner of a vehicle. Used together with orderGetIn to order a unit to get in as gunner.
	 Deprecated. Use void applyAssignAsGunner(Vehicle& vehicle, Unit& unit)
	 */
	 static void assignAsGunner(Vehicle& vehicle, Unit& unit);

	 /*!
	 Assigns a unit as cargo of a vehicle. Used together with orderGetIn to order a unit to get in as cargo.
	 Deprecated. Use void applyAssignAsCargo(Vehicle& vehicle, Unit& unit)
	 */
	 static void assignAsCargo(Vehicle& vehicle, Unit& unit);

	 /*!
	 Force all units in the array to get in or out of their assigned vehicles. Units must be assigned to a vehicle before this command will do anything. 
	 Deprecated. Use void applyGetIn(vector<Unit> unitVec, bool order)
	 */
	 static void orderGetIn(vector<Unit> unitVec, bool order);

	 /*!
	 Force unit to get in or out of their assigned vehicles. Unit must be assigned to a vehicle before this command will do anything. 
	 Deprecated. Use void applyGetIn(Unit& unit, bool order)
	 */
	 static void orderGetIn(Unit& unit, bool order);

		/*!
		Returns the weapons in cargo of the object. 
		It is added by addWeaponToCargo() method
		*/
		static vector<WEAPONCARGO> getWeaponFromCargo(Vehicle& vehicle);

		/*!
		Returns the magazines in cargo of the object. 
		It is added via addMagazineToCargo()
		*/
		static vector<MAGAZINECARGO> getMagazineFromCargo(Vehicle& vehicle);

		/*!
		Return all the muzzles of a person, vehicle
		*/
		static vector<string> getMuzzles(Vehicle& vehicle);

		
		/*!
		Returns the position a shooter has to aim at to hit a target with the current selected weapon.
		*/
		static position3D getAimingPosition(Vehicle& vehicle, ControllableObject& target);


		/*!
		It is only for local objects
		Order the given unit(s) to move to the given location (via the radio). 
		Exactly the same as doMove, except this command displays a radio message. 

		Deprecated. Use void applyCommandMove(Vehicle& vehicle, position3D position)
		*/
		static void commandMove(Vehicle& vehicle, position3D position);

		/*!
		Returns the forced speed of an aircraft. 
		*/
		static double getForceSpeed(Vehicle& vehicle);

#ifdef DEVELOPMENT
		/*!
		Checks if the coordinates are whitin firing arc set for specified turret. 
		Returns true if they are, otherwise false.

		vehicle - Vehicle on which the turret is located
		turret - Turret for which specified arc is set
		position - Position being checked if within turrets arc
		*/
		static bool isWithinFireArc(Vehicle& vehicle, Turret::TurretPath& turretPath, position3D& position);
#endif

		/*!
		Returns the arc parameters set for specified turret. 
		*/
		static FIREARC getFireArcParameters(Vehicle& vehicle, Turret::TurretPath& turretPath);

		/*!
		If set to true, the player can steer the vehicle even when he is not driver. Note that an AI driver has to be 
		switched off manually with disableAIMOVE(); as this is not working for planes, you currently have to make sure that 
		the pilot is local to the player's client. 
		*/
		static void applyDriverOverride(bool overRide);

		/*!
		Deletes the specified weapon from the cargo of a vehicle. Does nothing if the weapon isn't actually in vehicle's cargo. 
		Deprecated. Use void applyWeaponCargoRemove(Vehicle& vehicle, string weapon)
		*/
		static void removeWeaponFromCargo(Vehicle& vehicle, string weapon);

		/*!
		Deletes the specified magazine from the cargo of a vehicle. Does nothing if the magazine isn't actually in vehicle's cargo.
		Magazines are removed in order from fullest to emptiest.
		Deprecated. Use void applyMagazineCargoRemove(Vehicle& vehicle, string magazine)
		*/
		static void removeMagazineFromCargo(Vehicle& vehicle, string magazine);

		/*!
		Removes the magazine from vehicles turret
		Deprecated. Use void applyMagazineTurretRemove(Vehicle& vehicle, string magazine, Turret::TurretPath& turretPath)
		*/
		static void removeMagazineFromTurret(Vehicle& vehicle, string magazine, Turret::TurretPath& turretPath);

		/*!
		Add magazines to the cargo space of vehicles, which can be taken out by infantry units.
		Deprecated. Use void applySingleMagazineCargo(Vehicle& vehicle, string magazineName, int bulletCount)
		*/
		static void addMagazineCargoEx(Vehicle& vehicle, string magazineName, int bulletCount);

		/*!
		Sets the armor (or health for men) state of the vehicle (a value from 0 to 1). 
		*/
		static void applyVehicleArmor(Vehicle& vehicle, double armor);

#ifdef DEVELOPMENT
		/*!
		Sets the vehicle formation direction
		*/
		static void applyFormationDirection(Vehicle& vehicle, double direction);
		//----------------------------------------------------------------------------
#endif


		/*!
		Checks whether the object is (a subtype) of the given type.
		Command works with classes contained in CfgVehicles or CfgAmmo, but not with others (e.g. CfgMagazines or CfgWeapons).
		*/
		static bool isKindOf(Vehicle& vehicle, string typeName);

		/*!
		Check if (and by how much) vehicle commander knows about target.
		And returns a number from 0 to 4.
		Deprecated. Use double getKnowsAbout(Vehicle& vehicle, ControllableObject& target)
		*/	
		static float knowsAbout(Vehicle& vehicle, ControllableObject& target);

		/*!
		Applies the name of the vehicle to the VBS2 Environment.
		Prior to applying, function verifies that the name is a valid name
		according to following criteria.
		- Name has to start with letter or underscore.
		- Only special characters allowed is underscore.
		- Name should not contain spaces in between.
		- Name should not already exist in the mission.
		*/
		static void applyName(Vehicle& vehicle);

		/*!
		Returns the setting defined by setNetworkShell of the vehicle.
		*/
		static bool getNetworkShell (Vehicle& vehicle);

		/*!
		Lock or Unlock the vehicle for player. 
		Deprecated. Use void applyVehicleLockEnable(Vehicle& vehicle, bool playerLock)
		*/
		static void lockVehicle(Vehicle& vehicle, bool playerLock);

		/*!
		Lock or Unlock the vehicle for player. 
		Deprecated. Use void applyVehicleLockEnable(Vehicle& vehicle, bool playerLock, bool allLock)
		*/
		static void lockVehicle(Vehicle& vehicle, bool playerLock, bool allLock);

		/*!
		Checks whether the soldier is mounted in the vehicle.
		*/
		static bool isInVehicle(Vehicle& vehicle, Unit& unit);

		/*!
		Returns the formation leader of a given Vehicle.
		*/
		static Unit getFormLeader(Vehicle& vehicle);

		/*!
		Return the precision of the given entity.
		*/
		static float getPrecision(Vehicle& vehicle);

#ifdef DEVELOPMENT
		/*!
		Return how much vehicle wants to reload its weapons.
		*/
		static float getNeedReload(Vehicle& vehicle);
#endif

		/*!
		Return how much vehicle wants to reload its weapons.
		*/
		static ControllableObject getAssignedTarget(Vehicle& vehicle);

#ifdef DEVELOPMENT
		/*!
		Get the speed for the given speed mode.
		*/
		static float getSpeed(Vehicle& vehicle, string mode);
#endif

		/*!
		Returns the current position of a selection in model space.
		If a selection does not exist [0,0,0] is returned.
		*/
		static position3D getSelectionPosition(Vehicle& vehicle, string selectName);

		/*!
		Returns the current position of a selection in model space.
		If a selection does not exist [0,0,0] is returned.
		*/
		static position3D getSelectionPosition(Vehicle& vehicle, string selectName, string method);

#ifdef DEVELOPMENT
		/*!
		Returns name of a vehicle's secondary weapon (empty string if none).
		*/
		static string getSecondaryWeapon(Vehicle& vehicle);
#endif

		/*!
		Check whether magazine is reloaded whenever emptied.
		*/
		static bool isReloadEnabled(Vehicle& vehicle);

		/*!
		Attach a statement to a Vehicle. The statement is propagated over the network
		in MP games, it can be executed by invoking processInitCommands. 
		Deprecated. Use void applyInitStatement(Vehicle& vehicle, string statement)
		*/
		static void setInitStatement(Vehicle& vehicle, string& statement) ;

		/*!
		Clear the vehicle's init statement.
		Deprecated. Use void applyInitStatementClear(Vehicle& vehicle)
		*/
		static void clearInitStatement(Vehicle& vehicle) ;

#ifdef DEVELOPMENT
		/*!
		returns Name of first crew member(in order commander, driver, gunner).
		*/
		static string getName(Vehicle& vehicle);

		/*!
		Returns if the given Vehicle is captive. "captive" means that enemies will not shoot at the Vehicle.
		*/
		static bool isCaptive(Vehicle& vehicle);
#endif

		/*!
		Orders an AI-controlled aircraft to land at a given airport.
		The available airport codes are listed on the respective map pages.

		Deprecated. Use void applyLanding(Vehicle& vehicle, int port)
		*/
		static void landTo(Vehicle& vehicle, int port);

		/*!
		Assigns the airport that will be used when issuing either a land command to an AI-controlled aircraft, 
		or selecting the "Landing autopilot" action, if the aircraft is player-controlled.
		The available airport codes are listed on the respective map pages.

		Deprecated. Use void applyAirportAssignment(Vehicle& vehicle, int port)
		*/
		static void assignToAirport(Vehicle& vehicle, int port);

		/* !
		Check if vehicle is locked for Persons. If it is locked, Persons cannot mount / dismount without order. 
		*/

		static bool islocked(Vehicle& vehicle);

		/*!
		Assigns cargo role to unit, and places him into the vehicle (immediate, no animation). 
		Deprecated. Use void applyMoveInAsCargo(Vehicle& vehicle, Unit& unit, int cargoIndex)
		*/
		static void moveInCargoWithPosition(Unit& unit,Vehicle& vehicle, int cargoIndex);

		/*!
		Ceases the using of the vehicle by a group. It unassigns them from the vehicle. 
		Deprecated. Use void applyGroupLeave(Vehicle& vehicle, Group& group)
		*/
		static void groupLeaveVehicle(Group& group, Vehicle& vehicle);

		/*!
		Ceases the using of the vehicle by  a unit. It unassigns them from the vehicle. 
		Deprecated. Use void applyGroupLeave(Vehicle& vehicle, Unit& unit)
		*/
		static void groupLeaveVehicle(Unit& unit, Vehicle& vehicle);

		/*!
		Returns the type name of Binocular weapon which is in the WeaponSlotBinocular.
		Here Vehicle's side can't be civilian. 
		*/
		static string getBinocularWeaponName(Vehicle& vehicle);

		/*!
		Returns the expected destination position of vehicle.
		*/
		static position3D getExpectedDestinationPos(Vehicle& vehicle);

		/*!
		Returns the expected destination planning mode of vehicle.
		*/
		static string getExpectedDestinationPlanMode(Vehicle& vehicle);

		/*!
		Tells whether destination's replanning of the path was forced.
		*/
		static bool getExpectedDestinationForceReplan(Vehicle& vehicle);

		/*!
		Returns position of vehicle in the formation.
		*/
		static position3D getFormationPosition(Vehicle& vehicle);

		/*!
		Return the current command type for the commander of given vehicle. 
		*/
		static string getCurrentCommand(Vehicle& vehicle);

		/*!
		Returns the group leader for the given vehicle. For dead units, objNull is returned
		*/
		static Unit getGroupLeader(Vehicle& vehicle);

		/*!
		Returns an array with all the units in the group of the given object. For a destroyed object an empty array is returned.
		*/
		static vector<Unit> getGroupUnits(Vehicle& vehicle);

		/*!
		Check if vehicle is stopped by stop command 
		*/
		static bool isStopped(Vehicle& vehicle);

		/*!
		Check if the vehicle is ready. Unit is busy when it is given some command like move, until the command is finished. 
		*/
		static bool isReady(Vehicle& vehicle);

		/*!
		Set if leader can issue attack commands to the soldiers in his group
		Deprecated. Use void applyAttackEnable(Vehicle& vehicle, bool boolVal)
		*/
		static void setEnableAttack(Vehicle& vehicle, bool boolVal);

		/*!
			Return whether a group's leader can issue attack commands to soldiers under his command. 
		*/
			static bool isAttackEnabled(Vehicle& vehicle);

		/*!
		Sets the rotor wash effect of helicopters (applies to all helicopters in mission).

		This affects the dust created by the helicopter, as well as the wind effect on objects nearby.
		While it is not possible to turn off the dust effect altogether, it is possible to make it somewhat 
		invisible by using very high values (e.g. [100,100]) in the parameters. This only works though, 
		if there are no objects nearby that might be affected by the generated wind (e.g. trees), as they would then show unrealistic wind strain.
		
		Deprecated. Use bool applyRotorWash(double strength, double diameter)
		*/
		static bool setRotorWash(double strength, double diameter);

		/*!
		Enables or disables specific sound types for a vehicle:

		"SOUNDENGINE": Engine
		"SOUNDENVIRON": Movement sounds (e.g. tank tracks)
		"SOUNDCRASH": Impact with another object
		"SOUNDDAMMAGE": Alarm sound if vehicle was hit
		"SOUNDLANDCRASH": Impact with land (e.g. aircraft crash)
		"SOUNDWATERCRASH": Impact with water (e.g. aircraft crash)
		"SOUNDGETIN": Entering vehicle
		"SOUNDGETOUT": Exiting vehicle
		"SOUNDSERVO": Vehicle attachments (e.g. tank turrets)

		Be aware that not all of these sounds are defined in every vehicle.

		Deprecated. Use void applySoundEnable(Vehicle& vehicle, VEHICLE_SOUND_TYPES types, bool value)
		*/
		static void allowSound(Vehicle& vehicle,VEHICLE_SOUND_TYPES types,bool value);

	
		/*
		Controls screen blanking during certain actions. 
		Action with an associated screen blanking. Supported actions: "GETOUT", "TURNOUT", "TURNIN" 
		Enum :VA_GETOUT,VA_TURNOUT,VA_TURNIN

		Deprecated. Use void applyBlackFadeEnable(Vehicle& vehicle, VEHICLE_ACTION actions, bool value)
		*/
		static void allowBlackFade(Vehicle& vehicle, VEHICLE_ACTION actions, bool value);
		
		/*!
		Returns list of sounds that were disabled via allowSound
		*/
		static vector<string> getDisallowedSounds(Vehicle& vehicle);

		/*!
		Search for selection in the object model (Fire Geo only). Returns center in model space.
		*/
		static position3D getSelectionCenter(Vehicle& vehicle, string& selectionName);

		/*!
		Returns the number of given positions in the vehicle. Positions can be "Commander", "Driver", "Gunner" or "Cargo"
		*/
		static int getEmptyPositions(Vehicle& vehicle,VEHICLEASSIGNMENTTYPE vehPosition);

		/*!
		Returns the absolute speed of the vehicle in km/h
		*/
		static double getAbsSpeed(Vehicle& vehicle);
		
		/*!
		Sets vehicle's lights states.
		(-2 forced off, -1 off (user controlled), 0 user controlled, 1 on (user controlled), 2 forced on)

			main	- Driving lights (head lights, rear lights and brake lights)
			side	- Side lights (small edge indicator lights - only available on some vehicles)
			convoy	- White rear light for convoy driving (only available on some vehicles). Activating the convoy light will disable driving and indicator lights while on.
			left	- Left turn signal
			right	- Right turn signal

		Deprecated. Use void applyVehicleLights(Vehicle& vehicle, int main, int side, int convoy, int left, int right)
		*/
		static void setVehicleLights(Vehicle& vehicle,int main, int side, int convoy, int left, int right);

		/*!
		Returns the status of different vehicle lights, as set by either the user's input (via action menu or control keys), or the command setVehicleLights.

			Each light element can have one of the following states:

			-2: Forced off (user cannot override it, either via action menu or control buttons)
			-1: off (user can turn it on again)
			 1: on (user can turn it off again)
			 2: forced on (user cannot override it, either via action menu or control buttons) 
		*/
		static vector<int> getVehicleLights(Vehicle& vehicle);

		/*!
			Returns the path to the turret (as a TurretPath object) the unit is in, on the given vehicle.
			'VehicleUtilities::updateVehicle' and 'VehicleUtilities::updateTurrets' must be called on the vehicle object, before calling this function.
			input - A Vehicle and a Unit that mounted inside the vehicle.
			output - Returns the Turret Path of the turret which the unit is mounted on.
			If the unit is not mounted on the given vehicle OR the unit is mounted on some other crew position other than
			a turret, an empty Turret Path will be returned.
		*/
		static Turret::TurretPath GetTurretPathOfUnit( Vehicle & vehicle, Unit & unit );
		
		/*!
		Returns whether a turret is locked to something or not.
		*/
		static bool IsTurretLocked(Vehicle& vehi, Turret::TurretPath turretPath);

		/*!
			Returns the path to the turret (as a TurretPath object), if a turret exists by the given turretName on the given vehicleType.
			\param  a vehicle type as a string ( e.g. "vbs2_cz_army_bmp2_w_x" ).
			\param a turret name of a turret on that 'vehicle type' (e.g. "maingun" ).
			If the vehicle type doesn't exist OR no turret found in that vehicle type which has that turretName, an
			empty Turret Path is returned
		*/
		static Turret::TurretPath GetTurretPathByName( string vehicleType, string turretName );

		/*!
		Returns the offset between weapon orientation and optics.
		*/
		static vector<double> getOpticsOffset(Vehicle& vehicle, Turret::TurretPath turretPath);

		/*
		Returns maximal limit of dive (pitch) for a helicopter or airplane.
		*/
		static double getMaxDive(Vehicle &vehicle);

		/*
		Apply the maximal limit of dive (pitch) for a helicopter or airplane.
		*/
		static void applyMaxDive(Vehicle &vehicle, double maximalLimit );

		/*
		Returns maximal limit of bank (roll) for a helicopter or airplane.
		*/
		static double getMaxBank(Vehicle &vehicle);

		/*
		Applies maximal limit of bank (roll) for a helicopter or airplane.
		*/
		static void applyMaxBank(Vehicle &vehicle, double maximalLimit );

		/*!
		Set how vehicle is locked for player
		*/
		static void applyVehicleLock(Vehicle& vehicle, LOCK_STATE state);

		/*!
		Orders a unit or units to get out from the vehicle (silently).
		Deprecated. Use void applyGetOut(Unit& unit)
		*/
		static void doGetOut(Unit& unit);

		/*!
		Orders the unit to get out from the vehicle (via the radio). 
		This command is only effective when used on local units. When used on a remote unit, this command will create radio 
		dialog on the machine the unit is local to, but the unit will not leave the vehicle.

		Deprecated. Use void applyCommandGetOut(Unit& unit)
		*/
		static void commandGetOut(Unit& unit);

		/*!
		Returns the combat mode (engagement rules) of the vehicle.

		Modes returned may be:

		COMBAT_BLUE (Never fire)
		COMBAT_GREEN (Hold fire - defend only)
		COMBAT_WHITE (Hold fire, engage at will)
		COMBAT_YELLOW (Fire at will)
		COMBAT_RED (Fire at will, engage at will) 

		or

		COMBAT_ERROR (will be returned if group was passed or combat mode couldn't be retrieved) 
		*/
		static COMBATMODE getVehicleCombatMode(Vehicle& vehicle);

		/*!
		Activate and deactivate the engine of the vehicle. If state is true, 
		engine will be on and it will stop when state is false.
		*/
		static void applyEngineStart(Vehicle& vehicle , bool state);

		/*!
		Enable/ disable manual control of the vehicle.
		if control is TRUE vehicle AI disabled and it can be controlled 
		by applyTurnWanted & applyThrustWanted functions.  
		*/
		static void applyManualControl(Vehicle& vehicle , bool control);

		/*!
		Apply the turn wanted for the vehicle specified by the factor value.
		manual control of the vehicle should be enabled first. 
		*/
		static void applyTurnWanted(Vehicle& vehicle, double factor);

		/*!
		Applies vehicle's maximum speed defined by vehicle.getMaximumSpeedLimit() to the vehicle. 
		*/
		static void applyMaximumSpeedLimit(Vehicle& vehicle);

		/*!
		Updates the maximum speed limit of the vehicle.  
		*/
		static void updateMaximumSpeedLimit(Vehicle& vehicle);

		/*!
		Moves in an already created unit into vehicle as driver. 
		*/
		static void applyMoveInAsDriver(Vehicle& vehicle, Unit& unit);

		/*!
		Moves in an already created unit into vehicle as commander. 		
		*/
		static void applyMoveInAsCommander(Vehicle& vehicle, Unit& unit);

		/*!
		Moves in an already created unit into vehicle as gunner. 
		*/
		static void applyMoveInAsGunner(Vehicle& vehicle, Unit& unit);

		/*!
		Moves in an already created unit into vehicle as cargo. 
		*/
		static void applyMoveInAsCargo(Vehicle& vehicle, Unit& unit);	

		/*!
		Moves in an already created unit into vehicle as turret.

		Available turrets can be determined by the getCrewPos command, and is defined as an array, containing
		either one or two positions definitions, 
		e.g.
		[0]: first turret (no sub-turrets exist).
		[0,0]: first sub-turret of first main turret.
		[1,0]: first sub-turret of second main turret.
		*/
		static void applyMoveInAsTurret(Vehicle& vehicle, Unit& unit, string turretPath);

		/*!
		Moves in an already created unit into vehicle as turret.

		It take default turret
		[0,1]: second sub-turret of first main turret.
		*/
		static void applyMoveInAsTurret(Vehicle& vehicle, Unit& unit);

		/*!
		Moves in an already created unit into vehicle as turret.

		Available turrets can be determined by the getCrewPos command, and is defined as an array, containing
		either one or two positions definitions, 
		e.g.
		[0]: first turret (no sub-turrets exist).
		[0,0]: first sub-turret of first main turret.
		[1,0]: first sub-turret of second main turret.
		*/
		static void applyMoveInAsTurret(Vehicle& vehicle, Unit& unit, Turret turret);

#if WEAPON_HANDLING

		/*!
		Applies how much ammunition (compared to a full state defined by the unit type) the vehicle has. 
		The value ranges from 0 to 1.	
		*/
		static void applyVehicleAmmo(Vehicle& vehicle, double ammovalue);

		/*!
		Applies the direction of the Turret Weapon according to azimuth& elevation. 
		*/
		static void applyWeaponDirection(Vehicle& vehicle, Turret& turret, double azimuth, double elevation, bool transition);

		/*!
		@description 
		
		Sets world space azimuth and elevation for optics on turret.If direction of optics on turret is changed, weapon on the turret also changes direction to look where the optics points.

		@locality

		Locally Applied Globally Effected

		@version [VBS2Fusion v2.70.2]

		@param vehicle - A vehicle with turrets.

		@param turret - Considered turret.

		@param azimuth - azimuth in degrees.

		@param elevation - elevation in degrees.

		@return Nothing

		@example

		@code

		//Vehicle created
		Vehicle v;

		//Add turret path of the weapon to the Turret
		Turret::TurretPath tupath;
		tupath.resize(1);
		tupath.at(0) = 0;
		Turret turret
		turret.setTurretPath(tupath);

		// set turret azimuth to 90 degrees and 25 degrees elevation
		VehicleUtilities::applyOpticsDirection(tank, turret, 90, 25);

		@endcode

		@overloaded 

		@related

		@remarks To apply the optics direction for an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities::applyPositionASL(ControllableObject& co, position3D aslpos).
		*/
		static void applyOpticsDirection(Vehicle& vehicle, Turret& turret, double azimuth, double elevation);

		/*!
		@description 
		
		Sets speed of turret azimuth and elevation changes in degrees per second.
		The higher speed the faster the turret changes it's orientation.
		Negative number for azimuthSpeed or elevationSpeed set the speed back to config defaults.

		@locality

		@version [VBS2Fusion v2.70.2]

		@param vehicle - A vehicle with turrets.

		@param turret - Considered turret.

		@param azimuthSpeed - azimuth speed in degrees/s.

		@param elevationSpeed - elevation speed in degrees/s.

		@return Nothing

		@example

		@code

		//Vehicle to be created.
		Vehicle v;
		//Setting up turrets and turret path.

		Turret::TurretPath tupath;
		tupath.resize(1);
		tupath.at(0) = 0;
		VBS2Fusion::Turret turret;
		turret.setTurretPath(tupath);

		//Apply azimuth and elevation speed to 1,1
		VehicleUtilities::applyTurretSpeed(v, turret, 1, 1);

		//Set direction of the turret
		VehicleUtilities::applyOpticsDirection(v, turret, 90, 25);

		@endcode

		@overloaded 

		@related

		@remarks
		*/
		static void applyTurretSpeed(Vehicle& vehicle, Turret& turret, double azimuthSpeed, double elevationSpeed);

		/*!
		@description 
		
		Switches stabilization of turret in azimuth and elevation axes on/off. 
		If set stabilization in both axes off That means that after this is applied the turret will not try to point in the world direction it did 
		but will turn together with tank as the tank turns as if it was sealed with the tank. 
		If the stabilization is on then the turret tries to always point in the same world direction which results
		in the turret staying on place when tank under it turns.

		@locality

		@version [VBS2Fusion v2.70.2]

		@param vehicle - A vehicle with turrets.

		@param turret - Considered turret.

		@param azimuthStabil - If true azimuth stabilization on.

		@param elevationStabil - If true elevation stabilization on.

		@return Nothing

		@example

		@code

		//Vehicle to be created.
		Vehicle v;
		//Setting up turrets and turret path.

		Turret::TurretPath tupath;
		tupath.resize(1);
		tupath.at(0) = 0;
		VBS2Fusion::Turret turret;
		turret.setTurretPath(tupath);

		//Set direction of the turret
		VehicleUtilities::applyOpticsDirection(v, turret, 90, 25);

		//Set the stabilization off
		VehicleUtilities::applyTurretStabilizaton(tank, turret, false, false);

		@endcode

		@overloaded 

		@related

		@remarks
		*/
		static void applyTurretStabilizaton(Vehicle& vehicle, Turret& turret, bool azimuthStabil, bool elevationStabil);

#if _BISIM_DEV_CHANGE_TURRET_DIRECTION
		/*!
		@description 
		
    Makes turret to change it's direction using given speed. If the turret is stabilized then the direction change happens around world axes. When the turret is not stabilized the direction change happens around the local axes of the vehicle.
    When the turret is changing it's direction the user or AI can't interact with the turret.
    There is no specific direction to change to given so once this command is used the turret changes it's direction using given speed until the speed is set to 0.

		@locality

		@version 

		@param vehicle - A vehicle with turrets.

		@param turret - Considered turret.

    @param azimuthSpeedl - speed in degrees per second of the azimuth direction change
                           negative numbers mean turning clockwise
                           positive numbers mean turning counter-clockwise

		@param elevationSpeed - speed in degrees per second of the elevation direction change
                            negative numbers mean turning down
                            positive numbers mean turning up

    @param useAzimuthAcceleration - if true then the turret uses it's configured acceleration to reach the given azimuth change speed
                                    if false then the turret reaches given azimuth change speed immediately without accelerating.

    @param useElevationAcceleration - if true then the turret uses it's configured acceleration to reach the given elevation change speed
                                      if false then the turret reaches given elevation change speed immediately without accelerating.

		@return Nothing

		@example

		@code

		//Vehicle to be created.
		Vehicle v;
		//Setting up turrets and turret path.

		Turret::TurretPath tupath;
		tupath.resize(1);
		tupath.at(0) = 0;
		VBS2Fusion::Turret turret;
		turret.setTurretPath(tupath);

		//Apply the change of direction
		VehicleUtilities::applyTurretDirectionCahnge(tank, turret, 10, 5, false, false);

		@endcode

		@overloaded 

		@related

		@remarks
		*/
    static void applyTurretDirectionChange(Vehicle& vehicle, Turret& turret, float azimuthSpeed, float elevationSpeed, bool useAimuthAcceleration = false, bool useElevationAcceleration = false);
#endif

		/*!
		Disables gunner input at the turret.

		status - Is optional. 'true' sets as the default value then AI will not control turret anymore. If
		it sets to 'false', AI will control turret. 
		*/
		static void applyGunnerInputDisable(Vehicle& vehicle, Turret& turret, bool status = true);

		/*!
		Select the Weapon given by the muzzle Name.
		*/
		static void applyWeaponSelection(Vehicle& vehicle, string muzzleName);

		/*!
		Add magazine to the Vehicle.
		*/
		static void applyMagazineAddition(Vehicle& vehicle, string magName);

		/*!
		Count number of magazines available in given Vehicle object.
		*/
		static int getMagazineCount(Vehicle& vehicle);

		/*! 
		Applies reload state of the weapon.
		*/
		static void applyWeaponState(Vehicle& vehicle, Weapon& weapon, bool reloadStatus, double reloadTime);

		/*!
		Remove the specified magazine from the Vehicle.	
		*/
		static void applyMagazineRemove(Vehicle& vehicle, string name);

		/*!
		Remove all magazine from the vehicle.	
		*/
		static void applyMagazineRemoveAll(Vehicle& vehicle, string name);

		/*!
		Add Weapon to the Vehicle. If weapon slots are filled, any further applyWeaponAddition functions are ignored. 
		To ensure that the weapon is loaded at the start of the mission, at least one magazine should 
		be added, before adding the weapon.
		*/
		static void applyWeaponAddition(Vehicle& vehicle, string name);

		/*!
		Remove the specified Weapon from the unit.	
		*/
		static void applyWeaponRemove(Vehicle& vehicle, string name);

		/*!
		Tells if a vehicle has the given weapon.	
		*/
		static bool isWeaponAvailable(Vehicle& vehicle, string name);

		/*!
		Add weapons to the cargo space of vehicles, which can be taken out by infantry units. 
		Once the weapon cargo space is filled up, any further applyWeaponCargo functions are ignored.
		*/
		static void applyWeaponCargo(Vehicle& vehicle, string weaponName, int count);

		/*!
		Add magazines to the cargo space of vehicles, which can be taken out by infantry units. 
		Once the magazine cargo space is filled up, any further applyMagazineCargo functions are ignored.
		*/
		static void applyMagazineCargo(Vehicle& vehicle, string magName, int count);

		/*!
		Remove all weapons from the given vehicle's weapon cargo space.
		*/
		static void applyWeaponCargoRemoveAll(Vehicle& vehicle);

		/*!
		Remove all magazines from the given vehicle's magazine cargo space.
		*/
		static void applyMagazineCargoRemoveAll(Vehicle& vehicle);

		/*!
		Enables or Disables firing on current weapon in the selected turret. If true, the unit's current 
		weapon cannot be fired.
		*/
		static void applyWeaponSafety(Vehicle& vehicle, Turret& turret, bool safe);

		/*!
		Forces the Armed Vehicle to fire from the specifically named weapon.   
		*/
		static void applyFire(Vehicle& vehicle, string weaponName);

		/*!
		Forces the Armed Vehicle to fire according to the parameters given.  
		*/
		static void applyFire(Vehicle& vehicle, string muzzleName, string modeName);

		/*!
		Forces the Armed Vehicle to fire according to the parameters given.   
		*/
		static void applyFire(Vehicle& vehicle, string muzzleName, string modeName, string magazineName, position3D& pos, bool aim);

		/*!
		Apply the network shell mode. Normally shells are locally simulated following a fired event.
		When network shell mode is set, the simulation is broadcast to the network. 
		*/
		static void applyNetworkShellMode(Vehicle& vehicle,bool mode);

		/*!
		Order the Armed Vehicle to fire on the given target (without radio messages).   

		@param vehicle - Vehicle that is fired on given target.
		@param co - Target object.

		@return Nothing.

		@remarks This is a replication of void doFire(Vehicle& vehicle, ControllableObject& co) function.
		*/
		static void applyFire(Vehicle& vehicle, ControllableObject& co);

		/*!
		Forces the Armed Vehicle to fire on the given position from the specified turret path.   
		*/
		static void applyFire(Vehicle& vehicle, position3D& pos, Turret::TurretPath turretPath);
		
		/*!
		Order the Armed Vehicle to fire on the given target (via the radio).   

		@param vehicle - Vehicle that is fired on given target.
		@param co - Target object.

		@return Nothing.

		@remarks This is a replication of void commandFire(Vehicle& vehicle, ControllableObject& co) function.
		*/
		static void applyCommandFire(Vehicle& vehicle, ControllableObject& co);

#endif

		/*!
		Orders unit to leave vehicle.
		Vehicle should be properly updated.
		*/
		static void applyUnitLeave(Vehicle& vehicle, Unit& unit);

		/*!
		Assigns a unit as commander of a vehicle. Used together with applyGetIn to order a unit to get in as commander.	 
		*/
		static void applyAssignAsCommander(Vehicle& vehicle, Unit& unit);

		/*!
		Assigns a unit as commander of a vehicle. Used together with applyGetIn to order a unit to get in as commander.	

		delay - If unit already has an assigned vehicle role, time in seconds it will stay at the newly assigned position, 
		before it moves back to the original position, if that is still available. If position has been filled during the delay, 
		unit will then stay at the new position. If delay is negative, unit will always stay at new position.
		*/
		static void applyAssignAsCommander(Vehicle& vehicle, Unit& unit, double delay);

		/*!
		Assigns a unit as driver of a vehicle. Used together with applyGetIn to order a unit to get in as driver.
		*/
		static void applyAssignAsDriver(Vehicle& vehicle, Unit& unit);

		/*!
		Assigns a unit as driver of a vehicle. Used together with orderGetIn to order a unit to get in as driver.

		delay - If unit already has an assigned vehicle role, time in seconds it will stay at the newly assigned position, 
		before it moves back to the original position, if that is still available. If position has been filled during the delay, 
		unit will then stay at the new position. If delay is negative, unit will always stay at new position.
		*/
		static void applyAssignAsDriver(Vehicle& vehicle, Unit& unit, double delay);

		/*!
		Assigns a unit as gunner of a vehicle. Used together with applyGetIn to order a unit to get in as gunner.
		*/
		static void applyAssignAsGunner(Vehicle& vehicle, Unit& unit);

		/*!
		Assigns a unit as gunner of a vehicle. Used together with orderGetIn to order a unit to get in as gunner.

		delay - If unit already has an assigned vehicle role, time in seconds it will stay at the newly assigned position, 
		before it moves back to the original position, if that is still available. If position has been filled during the delay, 
		unit will then stay at the new position. If delay is negative, unit will always stay at new position.
		*/
		static void applyAssignAsGunner(Vehicle& vehicle, Unit& unit, double delay);

		/*!
		Assigns a unit as cargo of a vehicle. Used together with orderGetIn to order a unit to get in as cargo.
		*/
		static void applyAssignAsCargo(Vehicle& vehicle, Unit& unit);

		/*!
		Assigns a unit as cargo of a vehicle. Used together with orderGetIn to order a unit to get in as cargo.

		delay - If unit already has an assigned vehicle role, time in seconds it will stay at the newly assigned position, 
		before it moves back to the original position, if that is still available. If position has been filled during the delay, 
		unit will then stay at the new position. If delay is negative, unit will always stay at new position.
		*/
		static void applyAssignAsCargo(Vehicle& vehicle, Unit& unit, double delay);

		/*!
		Force unit to get in or out of their assigned vehicles. Unit must be assigned to a vehicle before this command will do anything. 
		*/
		static void applyGetIn(Unit& unit, bool order); 

		/*!
		Force all units in the array to get in or out of their assigned vehicles. Units must be assigned to a vehicle before
		this command will do anything. 
		*/
		static void applyGetIn(vector<Unit> unitVec, bool order);

		/*
		Deletes the specified weapon from the cargo of a vehicle. Does nothing if the weapon isn't actually in vehicle's cargo. 
		*/
		static void applyWeaponCargoRemove(Vehicle& vehicle, string weapon);

		/*!
		Deletes the specified magazine from the cargo of a vehicle. Does nothing if the magazine isn't actually in vehicle's cargo.
		Magazines are removed in order from fullest to emptiest.
		*/
		static void applyMagazineCargoRemove(Vehicle& vehicle, string magazine);

		/*!
		Add magazines to the cargo space of vehicles, which can be taken out by infantry units.

		vehicle - vehicle to add the magazines to
		magazineName - magazine name
		bulletCount - number of bullets
		*/
		static void applySingleMagazineCargo(Vehicle& vehicle, string magazineName, int bulletCount);

		/*!
		Check if (and by how much) vehicle commander knows about target.
		And returns a number from 0 to 4.
		*/		
		static double getKnowsAbout(Vehicle& vehicle, ControllableObject& target);

		/*!
		Lock or Unlock the vehicle (disable mounting / dismounting) for player. 
		*/
		static void applyVehicleLockEnable(Vehicle& vehicle, bool playerLock);

		/*!
		Lock or Unlock the vehicle (disable mounting / dismounting) for player. 

		playerLock - Should vehicle be locked for player
		allLock - Should vehicle be locked for all units including player
		*/
		static void applyVehicleLockEnable(Vehicle& vehicle, bool playerLock, bool allLock);

		/*!
		Assigns cargo role to unit, and places him into the vehicle (immediate, no animation). 
		*/
		static void applyMoveInAsCargo(Vehicle& vehicle, Unit& unit, int cargoIndex);

		/*!
		Ceases the using of the vehicle by a group. It unassigns them from the vehicle. 

		@param vehicle - Vehicle that given group leave from.
		@param group - Group to leave.

		@return Nothing.

		@remarks This is a replication of void groupLeaveVehicle(Group& group, Vehicle& vehicle) function.
		*/
		static void applyGroupLeave(Vehicle& vehicle, Group& group);

		/*!
		Ceases the using of the vehicle by a unit. It unassigns the units those who are belonging to given
		unit's group from the vehicle. 

		@param vehicle - Vehicle that given unit's group leave from.
		@param unit - Unit to leave.

		@return Nothing.

		@remarks This is a replication of void groupLeaveVehicle(Unit& unit, Vehicle& vehicle) function.
		*/
		static void applyGroupLeave(Vehicle& vehicle, Unit& unit);

		/*!
		Set if leader can issue attack commands to the soldiers in his group.
		*/
		static void applyAttackEnable(Vehicle& vehicle, bool boolVal);

		/*!
		Enables or disables specific sound types for a vehicle:

		"SOUNDENGINE": Engine
		"SOUNDENVIRON": Movement sounds (e.g. tank tracks)
		"SOUNDCRASH": Impact with another object
		"SOUNDDAMMAGE": Alarm sound if vehicle was hit
		"SOUNDLANDCRASH": Impact with land (e.g. aircraft crash)
		"SOUNDWATERCRASH": Impact with water (e.g. aircraft crash)
		"SOUNDGETIN": Entering vehicle
		"SOUNDGETOUT": Exiting vehicle
		"SOUNDSERVO": Vehicle attachments (e.g. tank turrets)

		Be aware that not all of these sounds are defined in every vehicle.
		*/
		static void applySoundEnable(Vehicle& vehicle, VEHICLE_SOUND_TYPES types, bool value);

		/*!
		Order the given vehicle to move to the given location (via the radio). It is only for local objects
		*/
		static void applyCommandMove(Vehicle& vehicle, position3D position);

		/*!
		Orders a unit to get out from the vehicle (silently).
		*/
		static void applyGetOut(Unit& unit);

		/*!
		Orders units to get out from the vehicle (silently).
		*/
		static void applyGetOut(vector<Unit> unitVec);

		/*!
		Orders the unit to get out from the vehicle (via the radio). 
		This command is only effective when used on local units. When used on a remote unit, this command will create radio 
		dialog on the machine the unit is local to, but the unit will not leave the vehicle.
		*/
		static void applyCommandGetOut(Unit& unit);

		/*!
		Process an animation on a vehicle. 

		animationName - The name of the animation
		phase - the phase value is between 0 and 1.  
		*/
		static void applyAnimation(Vehicle& vehicle, string animationName, double phase);

		/*!
		Set object's orientation (given as direction and up vector).
		Since version +1.30 accepts additional parameter which, when object is attached, 
		specifies if vectors applied are relative to parent object's orientation.  
		*/
		static void applyVectorDirAndUp(Vehicle veh, vector3D vectorDir, vector3D vectorUp, bool relative);

		/*!
		Send message to vehicle radio channel. 
		Message is defined in description.ext, CfgRadio or VBS2_EnglishLanguageGrammar. 
		*/
		static void applyVehicleRadioBroadcast(Vehicle& vehicle, string message);

		/*!
		Type text to vehicle radio channel.

		This function only types text to the list, it does not broadcast the message. If you want the message to
		show on all computers, you have to execute it on them.
		If you are in a crew seat (i.e. driver, gunner or commander), then it will include that role in the chat
		name output.
			Eg: Driver (you_name): "Message" 
		*/
		static void applyVehicleChat(Vehicle& vehicle, string message);

		/*!
		Attach a statement to a Vehicle. The statement is propagated over the network
		in MP games, it can be executed by invoking processInitCommands. 
		*/
		static void applyInitStatement(Vehicle& vehicle, string statement);

		/*!
		Clear the vehicle's init statements.
		*/
		static void applyInitStatementClear(Vehicle& vehicle);

		/*!
		Force aircraft landing.

		Planes will use the airport defined by applyAirportAssignment function, or the one closest to them (in this priority).

		Helicopters will use either the nearest editor-placed helipad, or the nearest flat location (in this priority). 
		applyAirportAssignment function has no effect on helicopters, and either of them have to be within 500m, or it will not land.
		*/
		static void appyCustomizedLanding(Vehicle& vehicle, LANDMODE mode);

		/*!
		Orders an AI-controlled aircraft to land at a given airport.
		The available airport codes are listed on the respective map pages.
		*/
		static void applyLanding(Vehicle& vehicle, int port);

		/*!
		Assigns the airport that will be used when issuing either a land command to an AI-controlled aircraft, 
		or selecting the "Landing autopilot" action, if the aircraft is player-controlled.
		The available airport codes are listed on the respective map pages. 
		*/
		static void applyAirportAssignment(Vehicle& vehicle, int port);

		/*!
		Sets the rotor wash effect of helicopters (applies to all helicopters in mission).

		This affects the dust created by the helicopter, as well as the wind effect on objects nearby.

		While it is not possible to turn off the dust effect altogether, it is possible to make it somewhat 
		invisible by using very high values (e.g. [100,100]) in the parameters. This only works though, 
		if there are no objects nearby that might be affected by the generated wind (e.g. trees), as they
		would then show unrealistic wind strain.
		*/
		static bool applyRotorWash(double strength, double diameter);

		/*!
		Sets vehicle's lights states.
		(-2 forced off, -1 off (user controlled), 0 user controlled, 1 on (user controlled), 2 forced on)

			main	- Driving lights (head lights, rear lights and brake lights)
			side	- Side lights (small edge indicator lights - only available on some vehicles)
			convoy	- White rear light for convoy driving (only available on some vehicles). Activating the convoy light will disable driving and indicator lights while on.
			left	- Left turn signal
			right	- Right turn signal
		*/
		static void applyVehicleLights(Vehicle& vehicle, int main, int side, int convoy, int left, int right);

		/*!
		Removes the magazine from specified turret in the vehicle.
		*/
		static void applyMagazineTurretRemove(Vehicle& vehicle, string magazine, Turret::TurretPath& turretPath);

		/*!
		Brings an aircraft to a flying altitude. Either planes or helicopters will take off realistically to reach this height
		*/
		static double applyAirborne(Vehicle& vehicle);

		/*
		Controls screen blanking during certain actions. 
		Action with an associated screen blanking. Supported actions: "GETOUT", "TURNOUT", "TURNIN" 
		Enum :VA_GETOUT,VA_TURNOUT,VA_TURNIN
		*/
		static void applyBlackFadeEnable(Vehicle& vehicle, VEHICLE_ACTION actions, bool value);

		/*!
		Sends a simple command to the vehicle's driver or gunner. 
		Player should be a vehicle's crew member to use this function. 
		*/
		static void applyCommand(Vehicle& vehicle, SIMPLE_COMMAND command);

		/*!
		Forces gunner in vehicle to recalculate the current lasing distance.

		Overrides the actual laser measurements (which are used to calculate the ballistic calculations) with a value provided by this command.
		This value will be shown in the gunner's optics view, until he does another manual lasing.
		*/
		static void applyLaserRange(Vehicle& vehicle, Unit& gunner);

		/*!
		Apply the thrust wanted for the vehicle to passed in. 
		manual control of the vehicle should be enabled first.
		*/
		static void applyThrustWanted(Vehicle& vehicle, double factor);

		/*!
		Applies the target flying altitude for aircraft. This can not be used to start an aircraft at the given height. 
		For this purpose use setPos combined with flyInHeight in the init line.

		Avoid too low altitudes, as helicopters and planes won't evade trees and obstacles on the ground.
		The default flying altitude depends on the "flyInHeight" definition in the vehicle's configuration. 

		height - Flying altitude in meters (Above Ground Level).
		*/
		static void applyHeightFlyIn(Vehicle& vehicle, double height);

		/*
		Set vehicle as respawnable in Multi Player games. So this function should be applied before vehicle destroyed. 
		When the vehicle is destroyed new vehicle will be created by VBS2 but the destroyed vehicle will be kept in 
		the mission.
		@param vehicle - Vehicle Object to which respawn function is applied.
		@param delay - Default value is -1, time (seconds) taken for respawn 
		@param count - Default value is 0 (Unlimited respawns), Number of respawn times 
		@return Nothing
		@remarks In order to access the new vehicle MissionUtilities::loadMission should be correctly used.
		@remarks Mission should have Description.ext file with "respawn" value set.
		*/
		static void applyRespawn(Vehicle& vehicle, double delay = -1, int count = 0);

		/*!
		Returns the vehicle position occupied by the passed unit (driver, cargo, or gunner), and the path to that position.
		@param unit - Considered unit.
		@return Vehicle::VehicleAssignmentInfo - struct which contains the assignment type role, turretPath and cargoIndex.
		If the Unit is not alive ( if isActivated() returns false ), this function will return VEHNONE assignment type role
		Values of cargoIndex or turretPath should be considered only when the returned assignment type role is CARGO or GUNNER respectively.
		If the Unit is not mounted on a vehicle, returns VEHNONE as assignment type role.
		If the Unit is mounted as driver of the vehicle, returns DRIVER as assignment type role.
		If the Unit is mounted on a turret ( as a gunner or commander ), returns GUNNER as assignment type role with the turretPath.
		If the Unit is mounted as cargo , returns CARGO as assignment type role with the cargoIndex.
		*/
		static Vehicle::VehicleAssignmentInfo getMountedPosition(Unit& unit);

		/*!
		Returns the vehicle position occupied by the passed Unit in the given Vehicle (driver, cargo, or gunner), and the path to that position.
		@param unit - Considered unit.
		@param vehicle - The vehicle that suppose to check unit where is.
		@return Vehicle::VehicleAssignmentInfo - struct which contains the assignment type role, turretPath and cargoIndex.
		If the given Unit or the Vehicle is not alive ( if isActivated() returns false ), this function will return VEHNONE assignment type role
		Values of cargoIndex or turretPath should be considered only when the returned assignment type role is CARGO or GUNNER respectively.
		If the Unit is not mounted on the given vehicle, returns VEHNONE as assignment type role.
		If the Unit is mounted as driver of the vehicle, returns DRIVER as assignment type role.
		If the Unit is mounted on a turret ( as a gunner or commander ), returns GUNNER as assignment type role with the turretPath.
		If the Unit is mounted as cargo , returns CARGO as assignment type role with the cargoIndex.
		*/
		static Vehicle::VehicleAssignmentInfo getMountedPosition(Unit& unit, Vehicle& vehicle);

		/*! 
		Orders the given vehicle to move to the given position (without radio messages). 

		@param vehicle - Vehicle that will be moved.
		@param position - Position to be moved.

		@return Nothing.
		*/
		static void applyMove(Vehicle& vehicle, position3D position);

		/*!
		@description 

		Checks whether the specified vehicle is currently on top of another vehicle (e.g. a trailer). Only works with physX vehicles.
		
		@locality

		Globally Applied Globally Effected

		@version  [VBS2Fusion v2.70.4]

		@param vehicle - Specified vehicle that suppose to check whether top of another vehicle or not.

		@return bool - If true then on top of another vehicle.

		@example
		
		Vehicle v;
		bool isDOn = VehicleUtilities::isDrivingOn(v);

		@overloaded  
		
		@related

		@remarks 
		*/
		static bool isDrivingOn(Vehicle& vehicle);

		/*!
		@description 
		Returns maximal of dive(pitch) for a helicopter or airplane.
		
		@locality

		Globally Applied

		@version  [VBS2Fusion v2.70.4]

		@param vehicle - Specified vehicle that suppose to get maximal of dive.

		@return double - limit (range =< 0 or >= 1).

		@example

		@code

		//Created the vehicle
		Vehicle airCraft;

		//Check the function
		double retVal = VehicleUtilities::getMaximumAirDive(airCraft);
		
		//Display the return value
		string display = conversions::DoubleToString(retVal);
		displayString="Ret. Val: "+display+"\\n";

		@endcode

		@overloaded  
		
		@related

		@remarks 
		*/
		static double getMaximumAirDive(Vehicle& vehicle);

		/*!
		@description 
		Returns maximal of bank(roll) for a helicopter or airplane.
		
		@locality

		Globally Applied

		@version  [VBS2Fusion v2.70.4]

		@param vehicle - Specified vehicle that suppose to get maximal of bank.

		@return double - limit (range =< 0 or >= 1).

		@example

		@code

		//Created the vehicle
		Vehicle airCraft;

		//Check the function
		double retValue = VehicleUtilities::getMaximumAirBank(airCraft);

		//Display the return value
		string displayValue = conversions::DoubleToString(retValue);
		displayString="Ret.Val(Bank): "+displayValue+"\\n";

		@endcode

		@overloaded  
		
		@related

		@remarks 
		*/
		static double getMaximumAirBank(Vehicle& vehicle);

	private:

		/*!
		check for existence of the group when creating a new vehicle 
		if there isn't any group, create random group and add to it.
		*/
		static Group createGroup(Vehicle& vehicle);


	};
};
#endif //VEHICLE_UTILITIES_H