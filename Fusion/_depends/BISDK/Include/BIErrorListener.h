#ifndef __ERRORLISTENER__
#define __ERRORLISTENER__
#include "BISDKCommon.h"
#include "BIListener.h"

#define BI_CONTINUE true
#define BI_ABORT	false

class BISDK_DLLEXTERN BI_ErrorListener : public BI_Listener
{
protected:
  bool _errorSet;
  int _maxSeverity;

public:
  BI_ErrorListener():_maxSeverity(0), _errorSet(false){}
  /** \brief Severity of the message */
	/** Severity is later used for determining if the message should be passed to user interface of be ignored by it */
	enum Severity
	{
		/** Informative means no error. It is there to denote the additional information, for example used memory, time passed etc. */
		Info, 
		/** Warning is here just for better understanding what may not work correctly */
		Warning, 
		/** Error means that some models may be missing, invisible or not working properly, but still the pbo can be created */
		Error, 
		/** Fatal error is always passed to user, since it contains grammar errors definitely not meant by user */
		FatalError 
	};
 
	/** \brief Set severity propagated */
	/** \param s is Severity that must be propagated to the user error handling interface */
  void SetSeverity(Severity s)
  {
    _maxSeverity = s;
  }
  void SetError(bool error)
  {
    _errorSet |= error;
  }
  bool GetError() const
  {
    return _errorSet;
  }
  /** \brief check if user wants to see the error */
  virtual bool OnError(Severity severity, const char * message)
  {
    if (severity <= _maxSeverity )
      return BI_CONTINUE; //no error in fact, we want to continue according to the max_severity
    bool b = this->OnErrorUser(severity, message);
    SetError( b == BI_ABORT );
    return b;
  }
  /* \brief what to to when errors occurs */
  virtual bool OnErrorUser(BI_ErrorListener::Severity severity, const char * msg) = 0;
};
#endif __ERRORLISTENER__