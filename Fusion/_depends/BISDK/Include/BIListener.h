#ifndef _BISDK_SDKWORLD_LISTENER_
#define _BISDK_SDKWORLD_LISTENER_

// BISSDK includes
#include "BISDKCommon.h"

/** \addtogroup WorldSDK */ 
/** \{ */

/** \brief Base class defining a listener which can be used to receive notifications. */
/** Allows to react to progress events during time consuming operations and to abort them, if needed.\n
 *  The user should derive his own class from this one and implement the Update() method to perform the desired behaviour.\n
 *  To let the SDK to abort an operation supporting listeners, it is enough to exit the Update() method returning <b>false</b>.\n\n
 *  <b>Example</b>: (Listener using wxWidgets' progress dialog)\n\n
 *  Listener definition:\n
 *  \code
 *  class wxListener : public BI_Listener
 *  {
 *    wxProgressDialog* _dlg;
 *    wxString          _msg;
 *  
 *  public:
 *    wxListener( wxWindow* parent, const wxString& message )
 *    : _dlg( NULL )
 *    , _msg( message )
 *    {
 *      _dlg = new wxProgressDialog( "Wait please", _msg, 100, parent, wxPD_AUTO_HIDE | wxPD_SMOOTH | wxPD_REMAINING_TIME | wxPD_CAN_ABORT );	
 *      _dlg->SetFocus();
 *    }
 *
 *    virtual ~wxListener()
 *    {
 *      _dlg->Destroy();
 *    }
 *  
 *    virtual bool Update( float progress )
 *    {
 *      int percent = (int)(progress * 100.0f);
 *      return (_dlg->Update( percent, _msg + wxString::Format( " (%d%% completed)", percent ) ) );
 *    }
 *  };
 *  \endcode
 *  Listener use:
 *  \code
 *  ...
 *  ...
 *  BI_VBSMap _map;
 *  BI_TerrainData _terrain;
 *  BI_ImageData _satellite;
 *  BI_TexturesData _textures;
 *  ...
 *  ...
 *  wxListener listener( this, "Splitting satellite image" );
 *  _map.RegisterListener( &listener );
 *  BI_ErrCode err = _map.SplitSatelliteImage( _terrain, _satellite, _textures );
 *  _map.UnregisterListener( &listener );
 *  ...
 *  ...
 *  \endcode
*/
class BISDK_DLLEXTERN BI_Listener
{
public:
  /** \brief Destructor */
  virtual ~BI_Listener()
  {
    // do nothing
  }

  /** \brief Called by functions and methods which trigger a progress event
   *  \param progress A float number in the range [0..1] keeping track of the current progress status
   *  \return <b>false</b> If the user wants to abort the current running operation
   */
  virtual bool Update( float progress ) = 0;

  /** \brief Called by functions and methods which trigger a report event
   *  \param message A message 
   *  \return <b>false</b> If the user wants to abort the current running operation
   */
  virtual bool Report( const char* message ) = 0;
};

/** \} */

#endif // _BISDK_SDKWORLD_LISTENER_