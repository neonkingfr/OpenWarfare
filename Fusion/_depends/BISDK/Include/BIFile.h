#ifndef __BI_FILE___
#define __BI_FILE___

#include <assert.h>
#include "BISDKCommon.h" //..
/** \addtogroup modelSDK */
/** \{ */

/** \brief Dummy file for logging purposes */
/** Dummy log file act as "black hole", anything that goes in, never shows up */
class BISDK_DLLEXTERN BI_DummyLogFile
{
public:
	/** \brief Constructor */
	BI_DummyLogFile() {}//nothing happens

	/** \brief discards all changes that could be written */
	virtual size_t Write(const void * p, size_t elementSize, size_t elements) { return 0;}

	virtual ~BI_DummyLogFile() {}//nothing happens
};

/** \brief Class for managing file content of file created in dll */
/** This class provides basic manipulation with file created in dll. It is a replacement for FILE strucrue, 
	so the read/write operation are very similar. This class is used only when callbacking and should not be used otherwise, 
	as it is here only because of dll-interface
  No copying is allowed
*/
class BISDK_DLLEXTERN BI_File : public BI_DummyLogFile //TODO change BI_File so it wil be only for writing or reading ( no changing values ) - as it was before
{
  char * _stream; //TODO change
	FILE * f;
	bool _handle;
  size_t _bytesRead;
  size_t _size;

private:
  BI_File ( const BI_File & file);

public:

  bool Valid() { return f != 0; }

  BI_File(int size) : f(NULL),_handle(false),_bytesRead(0), _stream(new char[size]) ,_size(size){  } //wait for setStream

	/** \brief Constructor - external source */
	/** Associates file with BI_File interface */
  BI_File (FILE * ff) : _handle(false),  f (ff), _bytesRead(ftell(ff)), _stream(0) 
  {
    assert(f!=NULL);
    fseek(f,0,SEEK_END);
    _size= ftell(f);
    fseek(f,(long)_bytesRead,SEEK_SET);
  }

	/** \brief Constructor according to name */
	BI_File (const char * name, const char * mode) : _handle(true),  f (fopen(name,mode)), _bytesRead(0), _stream(0)
	{
    _handle = (f != 0);
    if (!f)
      return;
    fseek(f,0,SEEK_END);
    _size= ftell(f);
    fseek(f,(long)_bytesRead,SEEK_SET);
  }
  /*void SetStream()
  {
    if (!Valid())
      return;
    setvbuf ( f , buffer , _IOFBF , size );
  }*/
	size_t GetSize()
	{
    return _size;	
	}

	size_t GetPos(){ return _bytesRead; }

	/** \brief Write operation */
	/** Write array of element to stream using fstream 
	 * \param p location of first element in array 
	 * \param elementSize size of one element of the array
	 * \param elements number of elements
	 */
	size_t Write(const void * p, size_t elementSize, size_t elements)
	{ 
    if (!_stream)
      return fwrite(p,elementSize,elements,f)*elementSize;
    memcpy(&_stream[_bytesRead],p,elementSize*elements);
    _bytesRead += elementSize*elements;
    return elements * elementSize;
	}
  void SetStream(char * stream, int size)
  {
    if (_stream)
      delete _stream;
    _size= size;
    _stream =  stream;
    _bytesRead = 0;
  }
  const char * GetStream()const
  {
    return _stream;
  }

  /** \brief Reads an integer from the stream */
  int ReadInt()
  {
    int num;
    Read(&num,sizeof(int),1);
    return num;
  }
  void SetPos(int offset)
  {
    _bytesRead = offset;
    if (!_stream)
      fseek(f,offset,SEEK_SET);
  }
  /** \brief Reads a char from the stream */
  int ReadChar()
  {
    char chr;
    Read(&chr,sizeof(char),1);
    return chr;
  }
  float ReadFloat()
  {
    float flt;
    Read(&flt,sizeof(float),1);
    return flt;
  }
  short ReadShort() 
  {
    short res;
    Read(&res,sizeof(short),1);
    return res;
  }
  /** \brief Reads a bool from the stream */
  bool ReadBool()
  {
    bool b;
    Read(&b,sizeof(bool),1);
    return b;
  }

  /** Skips next few bytes */
  void Skip( const int bytes )
  {
    if (_stream)
    {
      _bytesRead += bytes;
      return;
    }
    char buffer[4*1024]; //half of the buffer fread uses
    for ( int i =0; i< bytes; i+=8000)
    {
      Read(buffer,1,bytes%8000);
    }
  }

	/** \brief Read operation */
	/** Reads elements and writes them to array 
	 * \param p pointer to location of first element n array 
	 * \param elementSize size of one element of the array
	 * \param elements number of elements
	 */
	size_t Read(void * p, size_t elementSize, size_t elements)
	{
    size_t r = elements*elementSize;
    if (_stream)
      memcpy(p,&_stream[_bytesRead], r);
    else 
      r = fread(p,elementSize,elements,f)*(elementSize); //fread return only number of elements, so we have to multiply ( we quarantee to return number of bytes read*/

		_bytesRead += r;
    return r;
	}

	/** \brief reads a line from file*/
	/** In this state we expect that buffer size is long enough */
	bool ReadLine(char * buffer = NULL, char end = '\n')
	{
		char c;
		int i = 0;
		while ((c = GetChar()) != end && (c!= '\n') && !Eof())
		{
      if (buffer)
			  buffer[i] = c;
			i++;
		}
		buffer[i] = '\0';
		return !Eof();
	}

	/** \brief Retrieves char from file stream */
	unsigned char GetChar()
	{
    unsigned char c;
    Read(&c,sizeof (unsigned char),1);
		return c;
	}

	/** \brief Checks if error occured */
	bool GetError()
	{
    if (_stream)
      return false;
		return ferror(f)>0;
	}
	/** \brief Checks if end of file occured */
	bool Eof()
	{
    return (_size - _bytesRead) <= 0;
	}

	/** \brief Destructor */
	virtual ~BI_File()
	{
    if (_stream)
      delete _stream;
		if (!_handle)
			return;
		fclose(f);
	}
};
/** \} */
#endif //__BI_FILE___