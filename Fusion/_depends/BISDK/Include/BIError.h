#ifndef _BI_SDK_BI_ERROR_H
#define _BI_SDK_BI_ERROR_H

/** \addtogroup Common */
/** \{ */
/** \brief List of error codes */
/** Error codes will be generated via preprocessor. */

#define BI_ERRLIST( XX ) \
  XX( OK,                       "Operation succeeded" ) \
  XX( InvalidInput,             "Invalid input data" ) \
  XX( FileNotFound,             "File not found" ) \
  XX( UnableToOpen,             "Unable to open file" ) \
  XX( InvalidFilename,          "Invalid filename" ) \
  XX( InvalidDirname,           "Invalid dirname" ) \
  XX( InvalidFile,              "Invalid file" ) \
  XX( UnableToCreateFolder,     "Unable to create folder" ) \
  XX( UnableToLoad,             "Unable to load file" ) \
  XX( UnableToSave,             "Unable to save file" ) \
  XX( FileExists,               "File AlreadyExists") \
  XX( OutOfMemory,              "OutOfMemory" ) \
  XX( HeightmapAlreadySet,      "Heightmap already set" ) \
  XX( DifferentSizeRange,       "Array have wrong size") \
  XX( UnableToSplitImage,       "Unable to split image" ) \
  XX( OutOfRange,               "Access out of range" ) \
  XX( InvalidStructure,         "This structure is not valid" ) \
  XX( OwnerNotSet,              "This structure needs to have owner set to work correct." ) \
  XX( NoSuchUVSet,              "Nonexistent UVSET." ) \
  XX( NotSelected,              "Trying to change not selected object" ) \
  XX( UnableToBind,             "Object was not binded from original parent" ) \
  XX( UnableToUnBind,           "Object was not unbinded from original parent" ) \
  XX( TooMuchUVSets,            "Number of sets is more than expected" ) \
  XX( NotBounded,               "Object is not bounded  anywhere and it is necessary" ) \
  XX( NoSuchId,                 "No such id was found" ) \
  XX( UnableToLoadCallbackData,	"No data found to load" ) \
  XX( UnableToSaveCallbackData,	"Data cannot be saved" ) \
  XX( UnableToWrite,            "Data cannot be written (disk full, too many locks?) " ) \
  XX( OperationAborted,         "The operation was aborted by the user" ) \
  XX( CannotBinarize,           "Binarization was unsuccessful, see BI_Log for details" ) \
  XX( ErrorProcessingModule,    "Error while processing a module" ) \
  XX( TooManyVertices,          "Number of vertices is more than expected" ) \
  XX( Rewrite,			        "Structure was replaced" )	\
  XX( AddonMissing,  			"An addon was not found" ) \
  XX( VariableNotFound,  		"A variable was not found" ) \
  XX( AccessProblem,			"Unable to encrypt"	)	\
  XX( DiskFull,					"No space left on disk"	)	\
  XX( ConfigFileNotFound,		"Config file was not found" )	\
  XX( UnableCreateEngine,		"Engine could not be created. Probably memory issue." )	\
  XX( UnsupportedFormat,		"Format of file is not supported" )	\
  XX( DiskError,				"Error writing to disk" )	\
  XX( StructNotFound,			"Structure ( class definition, variable ) was not found" ) \
  XX( AddonCannotBeFound,		"Addon could not be found" )	\
  XX( VersionNotSupported,   "Version is not supported" ) \
  XX( ParseError,       "Parsing Error"  ) \
  XX( InvalidExtension,  "Invalid extension"  ) \
  XX( NotEnoughInformation,  "Insufficient information"  ) \
  XX( UnknownError,				"Unexpected error" )	\

/** Enum that stores error codes that functions of BI_SDK can return
 * Enum is created in preproccesor time.
 * When handling errors, all error codes have BIEC_ prefix, like BIEC_OK or BIEC_InvalidInput to differ them from other errors
 * To get detail information about error, you have to implement structure that will hold description of each error. That is done by
 * defining array of strings, whose values will be ( similarly to enum creating ) again list of all errors, but its value will be second part
 * of definition. 
 * For example:
 * \code
 * //we will define which part we want are interested in, in our case in strings
 * #define BI_ERRLIST_CREATE_STRING(name,text) text,
 *
 * // now we create array of strings from std namespace and fill it with messages using defined macro BI_ERRLIST
 * // macro BI_ERRLIST will process every defined error code and macro BI_ERRLIST_CREATE_STRING will select only text
 * std::string stringerrors[] =
 * {
 *	BI_ERRLIST(BI_ERRLIST_CREATE_STRING)
 * }
 *
 * //now, when error code occurs, you can access the message by stringerrors[BIEC_name_of_the_code]
 * \endcode
 */
#define BI_ERRLIST_CREATE_ENUM( name, text ) BIEC_##name,

/** Enum consisting of all errors that can occur in SDK functioning */
enum BI_ErrCode
{
  BI_ERRLIST( BI_ERRLIST_CREATE_ENUM )
};
/** \} */
#endif // _BI_SDK_BI_ERROR_H