#ifndef __BI_EDGES__
#define __BI_EDGES__

#include "../BISDKCommon.h"

/** \addtogroup modelSDK */
/** \{ */

/** \brief Class representing set of Edges */
/** Edges are defined by two points and they are considered oriented.
*/
class BISDK_DLLEXTERN BI_Edges  
{
	/** \brief List of edges */
	/** this array is dynamically resizes, so we do not suppose it will change much once it is created. */
	int **_edgelist;

	/** \brief Number of vertexes participating in edges */
	/** This is only used when traversing through all edges 
	 */
	int _nvertx;
	/** \brief Maximum degree achieved */
	/** It increases every time an edge is added to a vertex that has all iits levels full. ( Vertex with the 
	 highest degree) */
	int _levels;

	/** \brief Sorts subpart */
	/** \param endpoints data that should be sorted
	 * \param size sizeof the data
	 */
	void Sort( int * endpoints, int size );
public:
	
	/** \brief Contructor */
	/** Creates set with predefined number of point. This number can be changed by resize method.*/
	BI_Edges(int npoints=0); 

	/** \brief Copy constructor */
	/** Creates a copy instance of BI_Edge */
	BI_Edges (const BI_Edges& edges);

	/** \brief End point of edge */
	/** \return index representing end point od edge, -1 if there is no such edge. This can be used when iterating through all endpoints */
	int GetEdge( int idx, int i ) const;

	/** \brief Number of edges */
	/** \return Number of edges that were set. Edges (i,j) and (j,i) are counted as two different edges 
	 */
	int Size() const;

	/** \brief Removes all edges */
	/** This do not decrease number of levels. For this to be done, call SetVertices method. This behaviour we've chosen
	 * by assumption that it is very likeli to create similar big array and so we avoid reallocating space 
	 */
	void Clear();

	/** \brief Removes edge from set */
	/** No space is deallocated. 
	 */
	bool RemoveEdge(int va, int vb);

	/** \brief Removes edge from set */
	/** No space is deallocated. 
	 */
	bool DeleteEdge(int a, int b);

	/** \brief Sets number of vertices */
	/** If number of vertices is the same as was before, all edges are just cleared nad no memory is deallocated. Otherwise memory is 
	 * released and number of levels is reverted to zero.
	 * \param cnt is number of vertices that we will use  
	 * \note this should NOT be udes when working with BI_Edge bounded in mesh. It's size will be automaticaly resize when needed.
	 */
	void SetVertices( int cnt );

	/** \brief Inserts the edge into set */
	/** This operation may increase the number of levels */
	void SetEdge(int a, int b);

	/** \brief Checks if gived vertices are edges */
	/** \retval true if there is such edge in this set
	 * \retval false otherwise
	 */
	bool IsEdge(int va,int vb) const;

	/** \brief Tests edge direction */
	/** \param begin is id of first vertex
	 * \param end is id of last vertex 
	 * \retval true if the edge is present
	 * \retval false otherwise
	 */
	bool IsEdgeInDirection(int begin, int end) const;   

	/** \name Operators */
	/** \{ */
	/** parenthesis operator. This is replacement for IsEdge method
	 \return true if there is an oriented edge that consist of point va and vb
	 */
	bool operator()(int a, int b);

	/** \brief Plus operator */
	/** Merges edges 
	 * \param edges to be merged
	 * \note This operator changes the size levels, but number of edges remain the same, because the instance describes set of vertices maintained otherwise, so the number should be the same
	 */
	void operator +=(const BI_Edges& edges);

	/** \brief Minus operator */
	/** Remove edges declared in parameter from edge. There is no need fo the two instances to have the same number of vertices
	 * \param edges edges to remove
	 * \note This operator does not change number of vertices used
	 * \note This can result in not sorted edges
	 */
	void operator -=(const BI_Edges& edges);

	/** \brief Assigment operator */
	/** Instance changes so that every edge will contain only edges declared in edge */
	BI_Edges& operator=(const BI_Edges & edge) ;
	
	/** \} */

	/** \brief Resizes according to new point number */
	/** resizes the edge according to new number of points and max degree achieved
	 * \param newpoints number of point we want to resize the set
	 * \param degree is maximum degree we expect to achieve. Default value is -1. That means that no more degree expanding is expected
	 */
	void Resize( int newpoints, int degree =-1 );

	/** \brief Remove index from set of edges */
	/** \note this results in memory reallocation, that could cause time consumption */
	void DeletePoint(int index);

	/** \brief Remove index from set of edges */
	/** \note this results in memory reallocation, that could cause time consumption */
	void RemovePoint(int index);

	/** \brief Permutes vertices in edged acording to permutation */
	/** Permutation is in form { a,b,c,d.. } where a is point that should contain point 1 data, b should contain point 2 data etc.
	 * \param perm defined the permutation. Size of permutation buffer must be more or equal to number of points defined in edges.
	 */
	void Permute( int * perm);

	/** \brief Computes memory allocated for BI_Edges*/
	/** \return size_t bytes of allocated memory 
	 */
	int GetMemorySize();

	/** \brief Number of vertices */
	/** \return integer that represents number of vertices we intend to use. When using it in mesh, this sould not be called, as vertexcount is maintained by mesh.
	 */
	int GetNumberOfVertices()const;

	/** \brief Return maximum degree that can be found in edges */
	/** This method chooses vertex with lowest ID, that is incides with maximum number of edges.
	 * \param v is variable that will be filled with id number of vertexm that has maximum connections to othe vertices
	 * \return degree of found vertex
	 */
	int GetMaxDegree(int & v);

	/** \brief Sets minimum memory to occupy */
	/** This will release all unused memory.
	 * \return ratio memory_before / memory_now
	 */
	float Compact();

	/** \brief Sorts edges */
	/** This will show when acessing edges. 
	 * When iterating through edges at one vertex, all further edges will have their ending point higher that the edge before
	 */
	void Sort();

	/** \brief Destructor */
	/** Releases all memory allocated when holding edges 
	 */
	virtual ~BI_Edges();

	/** \brief Maximum degree achieved by adding vertices */
	/** This does not need to correspond to actual maximum degree that was achieved, only to maximum degree that was achieved during lifetime 
	 * of this object. Number of degrees can be j*/
	int Degree() const;

	/** \brief Degree of of one vertex */
	/** \param a point in question
	 * \returns how many a point labelled a edges it indices 
	 */
	int Degree(int a) const;

protected:
	/** \brief Resize to number of level */
	/** This method will be used just internally and only when Compacting */
	void ResizeLevels(int levels);

	/** \brief Expand the set*/
	/** This is used when structrure the edge with beginning point A cannot hold any more edges. 
	 * However, it will affect all other places for vertexes 
	 */
	void ExpandLevel();

	/** \brief Tests if there is such directed edge */
	bool TestSharp(int a, int b) const;
};
/** \} */
//--------------------------------------------------

#endif

