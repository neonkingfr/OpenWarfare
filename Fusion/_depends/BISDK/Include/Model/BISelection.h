#ifndef __BI_Selection__
#define __BI_Selection__

#include "..\BISDKCommon.h"
#include "BISDKReference.h"
#include "..\BIError.h"
#include "..\BIBoundingBox.h"
#include "BIModelDef.h"
#include "..\BIFile.h"

class BI_Mesh;

/** \addtogroup modelSDK */
/** \{ */
/** \brief Class represents selection. */
/** Selection contains points and faces. There has to be a valid BI_Mesh that this selection refers to.
 * Selection class has no other purpose that to hold information about which points or faces are marked.
 * working with it does not change anything in parent.
 * Selection class can live outside BI_Mesh and BI_Mesh also does not change anything in instance.
 * When selection lives outside the BI_Mesh, it serves just as temporary container os selected points. To reflect changes made in referring mesh, BI_Selection::Synchronize is provided.
 */
class BISDK_DLLEXTERN BI_Selection
{
  int index;

	/** whether or not are we handling data */
	bool _handleData;

	/** Pointer to BI_Mesh that we refer to */
	BI_ManipulationData * _data;

	/** How much place have we allocated for faces */
	int _sizeOfFaces;

	/** Capacity */
	int _allocatedSizeOfFaces;

	/** How much place have we allocated for places */
	int _sizeOfPoints;

	/** capacity of faces */
	int _allocatedSizeOfPoints;

	/** Array of float indicated selection. If set to non-null at position index, point with id index is selected, otherwise it is selected not */
	Weight * _selectedPoints;

	/** Array of bool indicated selection. If set to true at position index, point with id index is selected.
	 * otherwise it is selected not */
    bool * _selectedFaces;

	/* This should be called only by BI_ManipulationData when deleting point*/
    void DeletePoint( int index, int range = 1 );
	
	/* This should be called only by BI_ManipulationData when deleting point*/
	void RemovePoint( int index, int range = 1 );

	/* This should be called only by BI_ManipulationData when deleting face*/
    void DeleteFace( int index, int range = 1 );

	/* This should be called only by BI_ManipulationData when deleting face*/
    void RemoveFace( int index, int range = 1 );

	/* Constructor */
	BI_Selection( BI_ManipulationData * data);

	/* This should be called when selection is not quite our owner */
	void Synchronize(BI_ManipulationData * d);
	
	/* this is here to revert bad influences when assigning selection to another selection ( there will be owner change ) */
	void SetOwner(BI_ManipulationData * data);

	friend BI_ManipulationData;
  friend BI_InternalLODObject;
  friend BI_Mesh;
public:

	/** \brief Enum describing Mode selection */
	/** This is mainly used so specify behavior of selection when trying to set it to current selection */
	enum BI_SelectionMode 
	{ 
		/** \brief This creates union of selection that was and selection chosen */
		AddToSelection, 	
		/** \brief This subtracts from other selection. No points, that were marked in selection will be marked as selected in current selection */
		RemoveFromSelection,
		/** \brief Replaces actual selection by selection chosen */
		SelectOnly 
	};

	/**	\brief Empty constructor */
	/** For this class to work correct, it has to be created in some BI_Mesh by calling
	* There should be no need to create BI_Selection outside BI_Mesh class, however for possibility to hold these construction 
	* in std::vector or similar containers using default constructor.
	*/
	BI_Selection();

  /** \brief initialization of selection with predefined number of points */
  BI_Selection(int points, int faces);

	/** \brief Constructor */
	/** This constructor sets according to the parent containing points to
	 * be manipulated with.
	 */
	BI_Selection(const BI_Mesh & mesh);

	/** \brief Copy constructor */
	/** Creates copy instance of selection.
	 */
	BI_Selection(const BI_Selection & selection);

	/** \brief Destructor */
	~BI_Selection();

	/** \brief Checks if data are valid */
	/** Selection is valid, if there is referring mesh and selection 
	 * is synchronized with mesh
	 */
	bool IsValid() const;

	/* \brief Synchronizes selection with ts mesh */
	/** We call this every time we want to do something with size of point because there could be some change in parent. 
	 * If none, nothing happens.
	 */
	void Synchronize();

	/** \brief Synchronize number of point to points that are in parent mesh */
	void SynchronizePoints(int points);
	
	/** \brief Synchronize number of point to points that are in parent mesh */
	void SynchronizeFaces(int faces);

	/** \brief Computes memory size allocated */
	/** Computes all memory that was allocated by this structure. This should be the same for 
	 * every selection located in BI_Mesh. If not, selection is not synchronized with mesh */
	int GetMemorySize();

  /**
   \brief Gets all names of named selections that it intersects
  */
  /** \note this is here for the compatibility purposes
  */
  bool GetNextProxy(int& i, const char * prefix);

	/** \brief Inverts selection */
	/** Sets new selection that select the points and faces that were not selected otherwise
	 * always checks if there was some change in parent
	 */
	BI_Selection& InvertSelection(); 

	/** \brief Gets index of first point with state requested */
	/** \param selstate \li true it is looked for selected point
						\li false if is looked for unselected point
	    \param from is the first point that will be looked from
	    \retval -1 if there is no point of that state
	    \retval non-negative id of point that have state specified 
	*/
	int NextPoint( bool selstate, int from = -1 )const;

	/** \brief Gets index of next face of selected state */
	/** \param selstate  \li true it is looked for selected face 
						 \li false if is looked for unselected face
	    \param from is id of the face that will be looked from
	    \return \li -1 if there is no face of that state
				\li non-negative id of face that have state specified 
	*/
	int NextFace( bool selstate, int from = -1 )const;

	/** \name Selects */
  /** This method do not invoke validation, if not sure whether you are out of range , call BI_Selection::Synchronize before */
	/** \brief Checks if point is selected */
	/** \retval false if its weight is zero
		\retval true otherwise 
	 */
	bool PointSelected( int i ) const;

	/** \brief Selects point */
	/** Point is marked as selected and its weight is 1 */
	void PointSelect( int i, Weight state = 0xff );

  /** \brief Selected range of points */
  /** \param begin is point that should it start with 
      \param is the point it should end with. This point will be also selected. It the end point is negative, all point from the start will be selected 
      \param state sets weight on selected points. 
  */
  void PointSelectRange(int begin, int end, Weight state = 0xff);

	/** \brief Gets level of importance of selected point **/
	/** Value of point is between 0 and one */
	float PointWeight( int i ) const;

	/** \brief Changes point weight */
	/** Point weight represents importance of selected point The range that it can achieve is aritmetic sequence 
	 * starting with zero ( not selected )and continuing with difference 1/255 until value 1.0. This provides enough
	 * level of importance level and stores well.
	 * Point should be selected if changing value.
	 * When point marked as selected, its weight will be 1
	 * \return BIEC_NotSelected if trying to set weight to not selected point. Weight will not be change as 
	 * it would mean that the point is selected :)
	 * \return BIEC_OK if set was changed
	 */
	BI_ErrCode SetPointWeight( int i, float w );

	/** \brief Deselects point */
	/** Sets point weight to 0,, thus marking it as unselected */
	void PointUnselect( int i );

	/** \brief Checks if face is selected */
	/** \return true if the was was marked as selected
	 *  \return false otherwise
	 */
	bool FaceSelected( int i ) const;

	/** \brief Select Face */
	/** Adds BI_Face with index i to selection.  */
	void FaceSelect( int i, bool state = true );

  /** \brief Selects range of faces */
  /** \param begin first face to be selected 
      \param end last face to be selected 
      \param state sets state as selected(true) or unselected ( false )
  */
  void FaceSelectRange(int begin, int end, bool state = true);

	/** \brief Unselect face */
	/** Sets face as unselected. If wasn't selected, not is won't be selected for sure */
	void FaceUnselect( int i );
    
/** \} */
	/** \brief Clears all selection */
	/** Sets all points and faces as unselected. No reallocation or validation is performed */
    void Clear();

	/** \brief Sets all faces as unselected */
	/** this do not affect points in any way */
    void ClearFaces();

	/** \brief Sets all points as unselected */
	/** this do not affect facess in any way */
    void ClearPoints();

	/** \brief Saves selection to file */
    void Save( BI_File& f );

	/** \brief Load selection from file */
    void Load( BI_File& file ); //sizeNorm is not there, no one is using it
    
	/** \brief Permutes faces selection */
	/* Face will be selected according to face, whose index is on the position of face in question 
	 * \param permutation is array of integers ended by -1 that defined permutation. Actual size of array 
	 * is arbitrarily. Values in it should not reach beyond number of points in parent.
	 */
    void PermuteFaces( const int *permutation );

	/** \brief Permutes points selection */
	/* Point will be given the same weight as to point, whose index is on the position of point in question 
	 * \param permutation is array of integers of more or equal size to number of point in referring mesh. 
	 * value in it must be also in valid range
	 */
    void PermutePoints( const int *permutation );

	/** \brief Checks if there ia anything selected */
	bool IsEmpty() const;

	/** Returns number of selected points */
    int NumberOfSelectedPoints() const;

	/** Returns number of selected Faces */
    int NumberOfSelectedFaces() const;

    /** Selects just points used by selected faces */
	/** When this is called, all points are unselectt. Only Only selected faces points are set.
	 * \return number of points that was selected 
	 */
    int SelectPointsFromFaces();

    /** \brief Selects each face frompoints*/
	/** To select face, alll ites points must be selected 
	 * \return number of faces that was selected
	 */
    int SelectFacesFromPoints();

    /** \brief Selects each face which has at least one point selected 
	 * this is just more ffrely acting methos that SelectFacesFromPoints()
	 * \return number of faces that was selected
	 */
    int SelectFacesFromPointsTouchMode();

    /** \brief Extends selection to connected partition */
	/** Let's have some selected points and/or faces selected. As a result of this method, there will be 
	 * selected the minimal set of faces and points such, that every selected face has at least one point 
	 * selected and each selected point belog to faces that are selected 
	 */
    void SelectConnectedFaces();

	/** \brief Computes location of centre of selected points */
	BI_Vector3 GetCenter();

	/** \brief Computes bounding box of evry selected point or face */
	/** \return bounding whise edged are aligned to X, Y, Z axis. For every selected object bounding 
	 * box is large enough that all parts of selected object should be inside the box 
	 */
	BI_BoundingBox3 GetBoundingBox();

	/** \brief Everything in mesh will me marked as selected */
	void SelectAll();

	/** \brief Fills parameter with lowest and highest point index selected */
	/** \param lower integer variable that should be filled with lowest index selected
	 *  \param higher integer variable that should be filled with highest index selected
	 * parameter can be filled with the same value. If value is -1 for both, nothing is selected
	 */
	void GetSelectedPointsRange(int &lower, int & higher);

	/** \brief Fills parameter with lowest and highest face index selected */
	/** \param lowest integer variable that should be filled with lowest index selected
	 *  \param highest integer variable that should be filled with highest index selected
	 * parameter can be filled with the same value. If value is -1 for both, nothing is selected
	 */
	void GetSelectedFacesRange(int & lowest, int & highest );

	/** \brief Release as many memory as possible */
	/** \return ratio of size before compacting and size after compacting */
	float Compact();

	/** \name Operators */
	/** \{ */
	/** \brief Adds selection */
	/** Selection will be extended by other selection. For this to work correctly, selections must be bounded to same mesh */
	BI_Selection& operator += ( const BI_Selection &src );

	/** \brief Adds selection */
	/** Selection will be extended by other selection. For this to work correctly, selections must be bounded to same mesh */
	BI_Selection operator + ( const BI_Selection &src )const;

	/** \brief Remove selection */
	/* sets the selection do that there is no selection intersetion */
	BI_Selection& operator -= ( const BI_Selection &src );

	/** \brief Remove selection */
	/* sets the selection do that there is no selection intersetion */
	BI_Selection operator - ( const BI_Selection &src )const;

	/** \brief Logical and for selection */
	/** performs intersection */
	BI_Selection& operator &= ( const BI_Selection &src );

	/** \brief Logical and for selection */
	/** performs intersection */
	BI_Selection operator & ( const BI_Selection &src )const;

	/** \brief Logical or for selection */
	/** performs selection union, the same as + operator */
	BI_Selection& operator |= ( const BI_Selection &src );

	/** \brief Logical or for selection */
	/** performs selection union, the same as + operator */
	BI_Selection operator | ( const BI_Selection &src )const;

	/** Equality operator */
	/** \return true if there is the same selection and same owner 
	 * \note that point weight is not considered
	 */
	bool operator==(const BI_Selection &other) const;

	/** Inequality operator */
	/** \return true if there is not the same selection and same owner 
	 *  \note that point weight is not considered
	 */
	bool operator!=(const BI_Selection &other) const;

	/** Assign operator */
	/** there is need to create assign operator, because owner should not be changed 
	 * when BI_Selection was create byt copy constructor. Also pointer sould be kind of non-sahred.
	 * The should be only possibility to bind the selection, by BI_Mesh::Bind method 
	 */
  BI_Mesh GetOwner() const;
	BI_Selection operator=(const BI_Selection &other);
  int Points()const;
  int Faces()const;
  void DeleteOnData();
  void MergeOnData(float distance);
  /** \} */
}; 
/** \} */
#endif