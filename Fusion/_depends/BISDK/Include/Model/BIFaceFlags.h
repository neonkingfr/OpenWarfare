#define BI_FACE_NORMALLIGHT         0x0 
#define BI_FACE_NOLIGHT             0x1 // active colors
#define BI_FACE_AMBIENT             0x2
#define BI_FACE_FULLLIGHT           0x4
#define BI_FACE_BOTHSIDESLIGHT     0x20 // Objektiv normal calculation
#define BI_FACE_SKYLIGHT           0x80

#define BI_FACE_REVERSELIGHT   0x100000 // Objektiv normal calculation
#define BI_FACE_FLATLIGHT      0x200000 // Objektiv normal calculation
#define BI_FACE_LIGHT_MASK     0x3000a7

//#define BI_FACE_ISSHADOW     0x8
#define BI_FACE_ISSHADOW     0x0  // Shadow enabled returns 0x0
#define BI_FACE_NOSHADOW    0x10
#define BI_FACE_SHADOW_MASK 0x18

#define BI_FACE_Z_BIAS_MASK 0x300
#define BI_FACE_Z_BIAS_STEP 0x100

#define BI_FACE_FANSTRIP_MASK    0xf0000
#define BI_FACE_BEGIN_FAN        0x10000
#define BI_FACE_BEGIN_STRIP      0x20000
#define BI_FACE_CONTINUE_FAN     0x40000
#define BI_FACE_CONTINUE_STRIP   0x80000

#define BI_FACE_DISABLE_TEXMERGE 0x1000000

#define BI_FACE_USER_MASK 0xFE000000
#define BI_FACE_USER_STEP  0x02000000
#define BI_FACE_USER_SHIFT 25

#define BI_FACE_COLORIZE_MASK 0xF000
#define BI_FACE_COLORIZE_STEP 0x1000
#define BI_FACE_COLORIZE_SHIFT 12

#define BI_FACE_PROXY_VISIBLE 0x800000 

#define BI_FACE_DISABLE_TEXMERGE	0x1000000
#define BI_FACE_ENABLE_TEXMERGE	0x0

#define BI_FACE_Z_BIAS_MASK 0x300
#define FACE_Z_BIAS_STEP 0x100

#define BI_FACE_Z_BIAS_NONE	0x0
#define BI_FACE_Z_BIAS_LOW		0x100
#define BI_FACE_Z_BIAS_MIDDLE	0x200
#define BI_FACE_Z_BIAS_HIGH	0x300

