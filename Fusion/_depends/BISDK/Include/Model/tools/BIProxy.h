#include "Model/BIMesh.h"
#include "Model/BISelection.h"
#include "BIMatrix.h"
#include "BIString.h"

#define ProxyPrefix "proxy:"

/** \brief class for manipulating with proxy */
class BI_ProxyTool
{
public:
  /** \brief Created proxy name in recognizable format */
  /** proxy result array must be array long enough */
  static BI_String CreateProxyName(const char *fName, int index)
  {
    BI_String fnamecopy = fName;
    //delete extension
    BI_String fileName,dummy;
    fnamecopy.Split(".",&fileName, &dummy);
    bool slash = fileName.Contains('\\') == 0;
    BI_String res(ProxyPrefix);
    assert(index < 1000);
    if (!slash)
      res += "\\";
    res += fileName +"."+ BI_String(index,3);
    return res;
  }

  /** This method will create proxy object at location specified by BI_Matrix */
  /** \param obj mesh where proxy should be inserted
      \param filename name of the proxy file
      \param trans transformation matrix for the proxy 
      */
  static bool CreateProxy(BI_Mesh &obj, const char *filename, const BI_Matrix4& trans)
  {
    int faceIndex=obj.NumberOfFaces();
    BI_Face face(obj);
    obj.Bind(face);

    // create 3 new points. That will be our proxy face. It depicts center, where new proxy object should be later inserted
    int pIndex=obj.ReservePoints(3);
    // we will create three points in mesh that will form a proxy face
    BI_Vertex &np0 = obj.Point(pIndex+0);
    BI_Vertex &np1 = obj.Point(pIndex+1);
    BI_Vertex &np2 = obj.Point(pIndex+2);

    /** let's set these points to the created face. The face is bound to all 
      the setting will be propagated */
    face.SetNextPoint(pIndex,0);
    face.SetNextPoint(pIndex+1,0);
    face.SetNextPoint(pIndex+2,0);

    /** now the first point will be the one */
    BI_Vertex v(trans.GetPos(),BI_POINT_SPECIAL_HIDDEN);
    obj.SetPoint(pIndex,trans.GetPos());

    np1=np0;
    (np1)[0]+= 2*trans.Get(0,1);
    (np1)[1]+= 2*trans.Get(1,1);
    (np1)[2]+= 2*trans.Get(2,1);

    np2=np0;
    (np2)[0]+= trans.Get(0,2);
    (np2)[1]+= trans.Get(1,2);
    (np2)[2]+= trans.Get(2,2);  

    if( obj.NumberOfNormals()<=0 ) 
      obj.AddNormal(BI_Vector3());

    // select new face
    BI_Selection newsel(obj);  
    newsel.PointSelect(pIndex);
    newsel.PointSelect(pIndex+1);
    newsel.PointSelect(pIndex+2);
    newsel.FaceSelect(faceIndex);

    // now we create a named selection that is actually our proxy
    int index=0;
    //this should cover all needs
    BI_String proxynewname;
    /** We do not want to corrupt our previous proxy. 
        That's why we create a unique selection by adding a number to the name */
    do
    {
      if(index == 999)
        break; //no more than 1000 proxies is allowed. If more proxy is there, then will be saved as last
      proxynewname = BI_ProxyTool::CreateProxyName(filename,++index);
    }
    while (obj.GetNamedSel(proxynewname.c_str(),BI_Selection())==BIEC_OK);
    obj.SaveNamedSel(proxynewname.c_str(),newsel);
    obj.UseSelection(newsel);
    return true;
  }
};

const char * GetProxy(const BI_Mesh & mesh, int index)
{
  int i = 0;
  int len = strlen(ProxyPrefix);
  const char * ret = NULL;
  do
  {
    ret = mesh.GetSelectionName(i);
    if (!ret)
      return NULL;
    if (strncmp(ProxyPrefix,ret, len ))
      i++;
  }
  while (i < index);
  return ret;
}