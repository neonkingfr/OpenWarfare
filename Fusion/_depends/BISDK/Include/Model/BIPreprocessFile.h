#ifndef __PREPPROCESS_FILE___
#define __PREPPROCESS_FILE___

#include "../BISDKCommon.h"
#include "../BIFile.h"

/** \brief enum of extensions we are able to handle */
enum SupportedType
{
  TypeP3d,
  TypeFbx
};

/** \addtogroup modelSDK */
/** \{ */

/** \brief Abstract class for defining file preprocessing when loading */
/** Typical use id that user want to extract user-speficic data. 
 * To use it, derive the class, implement preprocessing and call BI_Model::Load(filename, your_class)
 * The class is implemented just to unify all instances that could possibly acess the file
 * This class will handle the data only from the beginning of the file. For data saving is used class
 * \ref BI_ModelPreprocessSaveCallback, which will add the data to the right position so user don't need
 * to check this.
 * Example using this class :
 * \code
  class Data;
  class DataLoader : public BI_ModelPreprocessLoadCallback
 {
 	Data * _specialData;
 	public:
 		DataLoader() { _specialData = new Data(); }
 		virtual bool LoadCallBack(std::istream &f)		
			{ 
				return data.add(f.read(n)); 
			}
 };
 BI_Model obj;
 DataLoader * d = new DataLoader();
 obj.Load("./modifiedData.p3d", d);
 \endcode
 */ 
class BISDK_DLLEXTERN BI_ModelPreprocessLoadCallback {
public:

	/** \brief Extracting user data */
	/** \return true if the data were successfully loaded, false otherwise. 
	 * if data were not succesfully loaded, no further loading will be performed
	 * All exception that can be thrown will be handled by user
	 */
	virtual bool LoadCallBack( BI_File & f ) = 0;

  /** this method will be used for preprocessing FBX. FBX has its own handler for handling all objects in the scene */
  virtual bool LoadCallBack( SupportedType type, void * handler ) { return true;}; //do nothing.
	
};

/** \brief Abstract class for defining file preprocessing when loading */
/** Typical use id that user want to extract user-speficic data. 
 * To use it, derive the class, implement preprocessing and call BI_Model::Load(filename, youe_class)
 * The class is implemented just to unify all instances that could possibly acess the file
 * This class will handle the data only from the beginning of the file. For data saving is used class
 * \ref BI_ModelPreprocessSaveCallback, which will add the data to the right position so user don't need
 * to check this.
 * Example using this class :
 * \code
  class Data;
  class DataSaver : public BI_ModelPreprocessLoadCallback
 {
 	Data * _specialData;
 	public:
 		DataSaver(Data *d ) { _specialData = d; }
 		virtual bool SaveCallBack(std::istream &f)		
		{ 
			///here's go your code
			return true;
		}
 };
 BI_Model obj;
 DataSaver * d = new DataSaver(data);
 obj.Save("./modifiedData.p3d", d);
 \endcode
 */ 
class BISDK_DLLEXTERN BI_ModelPreprocessSaveCallback {
public:
	/** \brief Storing user data */
	/** if data were not sucesfully saved, no further saving will be performed. 
	 * All exception that could be possibly thrown should be handled by user
	 * \return true if the data were sucessfully saved, false otherwise. 
	 */
	virtual bool SaveCallBack( BI_File & f ) = 0;

  /** \brief for supported type that requies special handling */
  virtual bool SaveCallBack( SupportedType type, void * handler ) { return true;}; //do nothing. User does not need define this since this is added feature
};
/** \} */
#endif //__PREPPROCESS_FILE___