#ifndef __DEFINITES__
#define __DEFINITES__

/** \addtogroup modelSDK */
/** \{ */

/** \brief Maximum number of selection that can be stored in BI_Mesh */
/** More that this number of selection will not be saved in BI_Mesh. */
#define BI_MAX_NAMED_SEL 2048
/** \brief Maximum polygon size that p3d can handle so far.*/
/** Every face that is declared must have number of vertices between 0 and this number. 
 * Declaring face with more that this number of vertices is impossible. 
 */
#define BI_DATA_MAX_POLY 4

/** \brief Maximum number of properties that p3d can handle so far. */
/** If nore that this number properties is declared, the topover item will be discarded 
 */
#define BI_MAX_NAMED_PROP 128

/** \brief Version od p3d we are working with */
#define BI_P3D_VERSION 1
	
/** \brief Type used to determine how much are point important when selected */
typedef unsigned char Weight;

/** \brief Enum describing in what relationship can two faces be */
/** However the relationship can be defined differently according to face we take as reference. This means that from point of one face the other is up and from point. In teh sdk when comparinf position of two faces, the result location is the one that is more concrete ( denoted by lower number ) */
enum BI_FaceToFaceLocation
{
	/** \brief This is actually used only internal, so no need to check it */
	BI_FaceIntermediate = 0 , //first face is intermediate with second (some points are above and some points are below), or evry point is on the face
	/** \brief All points of other face are above the plane that face creates  */
	BI_FaceAbove , //first face is above second
	/** \brief Both faces creates such planes that each is above another */
	BI_FaceAboveBoth , //first face is above and also second face is above first
	/** \brief All points of other face are below the plane that face creates  */
	BI_FaceBelow , //first face is below second
	/** \brief Both faces creates such planes that each is below another */
	BI_FaceBelowBoth, //first face is below and also second face is below first	
	/** \brief Faces are crossed */
	BI_FaceCross, //faces are crossed. 
}; //images!

/** \} */
#endif