// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/FundamentalTypes.h>
#include <xait/common/parser/formula/MathValueInstance.h>
#include <xait/control/FSMManager.h>

#include <xait/common/memory/MemoryManaged.h>
#include <xait/common/platform/Time.h>


namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace Formula
			{
				template<typename T_VALUE_TYPE>
				class TypedMathValue;
			}
		}	
#ifdef XAIT_CONTROL_DEBUGGER
		namespace Debugger
		{
			class BreakpointInstance;
		}
#endif
	}
	
	namespace Control
	{
		class Command;

		struct FSMTransition
		{
			//! \brief Constructor.
			FSMTransition();

			//! \brief Copy constructor.
			//! \remark Removes both the condition and the commands from original transition.
			FSMTransition(const FSMTransition& other);

			//! \brief Destructor.
			virtual ~FSMTransition();

			void cleanUp(const Common::Memory::AllocatorPtr& allocator);

			inline bool operator==(const FSMTransition& other) const
			{
				return mTransitionID == other.mTransitionID;
			}
			
			float32													mProbability;
			uint32													mDestinationFSM;
			uint32													mTransitionID;
			FSMStateID												mDestinationState;
			StaticManagedContainer<Command*>::Vector				mCommands;
			Common::Parser::Formula::TypedMathValueInstance<bool>*	mCondition;
			uint32													mPriority;
			FSMTransitionCollectionID								mTransitionCollection;
			bool													mClosed;

#ifdef XAIT_CONTROL_DEBUGGER
			Common::String											mTransitionName;
			Common::String											mSourceName;
			Common::Debugger::BreakpointInstance*					mEnterBreakpoint;
			Common::Debugger::BreakpointInstance*					mLeaveBreakpoint;
			Common::Debugger::BreakpointInstance*					mPreconditionBreakpoint;
#endif
		};

		struct FSMTransitionCollection 
		{
			//! \brief Constructor.
			FSMTransitionCollection( FSMTransitionCollectionID id);

			//! \brief Destructor.
			virtual ~FSMTransitionCollection();

			//! \brief Generates the NOOP probabilities.
			void generateNoOpProbabilites();

			void cleanUp(const Common::Memory::AllocatorPtr& allocator);

			void addTransition(FSMTransition* transition, bool elseTransition);

			void closeTransitionGroup();
			void openTransitionGroup();

			StaticManagedContainer<FSMTransition*>::Vector& getTransitionsByCase(bool elseTransition)
			{
				if(elseTransition)
					return mElseTransitions;
				return mTransitions;
			}

			const StaticManagedContainer<FSMTransition*>::Vector& getTransitionsByCase(bool elseTransition) const
			{
				if(elseTransition)
					return mElseTransitions;
				return mTransitions;
			}

			StaticManagedContainer<FSMTransition*>::Vector& getTransitions()
			{
				return mTransitions;
			}

			const StaticManagedContainer<FSMTransition*>::Vector& getTransitions() const
			{
				return mTransitions;
			}

			StaticManagedContainer<uint08>::Vector& getTransitionPrioritySpans()
			{
				return mTransitionsPrioritySpans;
			}

			const StaticManagedContainer<uint08>::Vector& getTransitionPrioritySpans() const
			{
				return mTransitionsPrioritySpans;
			}

			StaticManagedContainer<FSMTransition*>::Vector& getElseTransitions()
			{
				return mElseTransitions;
			}

			const StaticManagedContainer<FSMTransition*>::Vector& getElseTransitions() const
			{
				return mElseTransitions;
			}

			StaticManagedContainer<uint08>::Vector& getElseTransitionPrioritySpans()
			{
				return mElseTransitionsPrioritySpans;
			}

			const StaticManagedContainer<uint08>::Vector& getElseTransitionPrioritySpans() const
			{
				return mElseTransitionsPrioritySpans;
			}

			void handleOnlyElseTransitions();
			void generatePrioritySpans();

			FSMTransitionCollectionID					mTransitionCollectionID;
			
			float32										mNoopProbability;

		private:
			StaticManagedContainer<FSMTransition*>::Vector	mTransitions;
			StaticManagedContainer<FSMTransition*>::Vector	mElseTransitions;
			StaticManagedContainer<uint08>::Vector	mTransitionsPrioritySpans;
			StaticManagedContainer<uint08>::Vector	mElseTransitionsPrioritySpans;
		};

		struct FSMTimeBasedTransition
		{
			FSMTimeBasedTransition(FSMTransitionCollectionID transitionCollectionID, Common::Platform::MilliSeconds time);

			bool operator<(const FSMTimeBasedTransition& other) const;

			FSMTransitionCollectionID					mTransitions;
			Common::Platform::MilliSeconds				mTimeSpan;
		};
	}
}
