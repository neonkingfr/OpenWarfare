// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/control/FSMTypedefs.h>
#include <xait/common/reflection/function/FunctionArgumentList.h>

namespace XAIT
{
	namespace Control
	{	
		class FSMInstanceWrapper
		{
			friend class FSM;
			friend class FSMInstance;

		public:
			XAIT_CONTROL_DLLAPI FSMInstanceWrapper();
			XAIT_CONTROL_DLLAPI FSMInstanceWrapper(FSMID subType);
			XAIT_CONTROL_DLLAPI FSMInstanceWrapper(FSMInstanceID id);
			//XAIT_CONTROL_DLLAPI FSMInstanceWrapper(FSMInstanceWrapper& other);

			XAIT_CONTROL_DLLAPI void start();
			XAIT_CONTROL_DLLAPI void resume();
			XAIT_CONTROL_DLLAPI void suspend();
			XAIT_CONTROL_DLLAPI bool isValid();
			XAIT_CONTROL_DLLAPI void handleEvent(FSMEventID eventID, Common::Reflection::Function::FunctionArgumentList& arguments);

			XAIT_CONTROL_DLLAPI FSMInstance* getFSMInstance();
			FSMInstanceID getFSMInstanceID() const {return mInstanceID;}
			FSMID getFSMID() const {return mSubType;}

			XAIT_CONTROL_DLLAPI bool operator==(const FSMInstanceWrapper& other) const;
			XAIT_CONTROL_DLLAPI bool operator!=(const FSMInstanceWrapper& other) const;
			XAIT_CONTROL_DLLAPI void operator=(const FSMInstanceWrapper& other);
			XAIT_CONTROL_DLLAPI void operator=(const FSMInstanceID& id);

		private:
			FSMInstanceID mInstanceID;
			FSMID		  mSubType;

			void setSubType(FSMID subTypeID){mSubType = subTypeID;}

			FSMInstanceWrapper(FSMInstanceID instanceID, FSMID typeID);
		};
	}

	namespace Common
	{
		namespace Container
		{
			template<>
			class Vector<Control::FSMInstanceWrapper>
			{
			public:
				typedef ::std::vector<Control::FSMInstanceWrapper,Memory::STLAllocator<Control::FSMInstanceWrapper> >	ArrayType;

				typedef  ArrayType::iterator				Iterator;
				typedef  ArrayType::const_iterator			ConstIterator;
				typedef  ArrayType::reference				Reference;
				typedef  ArrayType::const_reference			ConstReference;

			private:
				ArrayType	mVec;		//!< array containing the elements of the vector
				Control::FSMID mFSMType;

			public:

				//! \brief default constructor
				//! \param allocator	memory allocator for new elements
				Vector(const Memory::Allocator::Ptr& allocator)
					: mVec(allocator)
				{}

				//! \brief construct a vector with size elements 
				//! \param size		number of elements in the vector
				//! \param allocator	memory allocator for new elements
				//! \remark elements will be initialized to their default values
				Vector(const uint32 size, const Memory::Allocator::Ptr& allocator) 
					: mVec((std::size_t)size,Control::FSMInstanceWrapper(),allocator)
				{}

				//! \brief construct a vector with size elements and init elements with a predefined one
				//! \param size		number of elements in the vector
				//! \param value	value to use for element initialization 
				//! \param allocator	memory allocator for new elements
				Vector(const uint32 size, Control::FSMInstanceWrapper& value, const Memory::Allocator::Ptr& allocator)
					: mVec((std::size_t)size,value,allocator)
				{}

				//! \brief default destructor
				~Vector()
				{}

				void setSubType(Control::FSMID subType)
				{
					mFSMType = subType;
				}

				//! \brief get the memory allocator for this vector
				inline Memory::Allocator::Ptr getAllocator() const
				{
					return mVec.get_allocator().getMemoryAlloc();
				}

				//! \brief append an element at the end of the vector
				//! \param value	value to add
				void pushBack(Control::FSMInstanceWrapper& value)
				{
					//X_ASSERT_MSG(mFSMType == value.getFSMID(), "SDFASGSADFHASDGASDGASDGASDFG");
					mVec.push_back(value);
				}

				//! \brief remove an element at the end of the vector
				inline Control::FSMInstanceWrapper popBack()
				{
					Control::FSMInstanceWrapper returnValue = back();
					mVec.pop_back();

					return returnValue;
				}

				//! \brief get the size of the vector
				//! \returns the number of elements in the vector
				uint32 size() const
				{
					return (uint32)mVec.size();
				}

				//! \brief remove an element where the iterator points to
				//! \param iter		iterator to the element we want to delete
				//! \returns an iterator behind the deleted element
				//! \remark This function has runtime O(n)
				Iterator erase(const Iterator& iter)
				{
					return mVec.erase(iter);
				}

				//! \brief erase elements between start (inclusive) and end iterator (exclusive) [start,end[
				//! \param start	iterator to the first element to delete
				//! \param end		iterator beyond to the last element to delete
				//! \returns an iterator beyond the last deleted element
				//! \remark This function has runtime O(n)
				Iterator erase(const Iterator& start, const Iterator& end)
				{
					return mVec.erase(start,end);
				}

				//! \brief remove an element at the specified position
				//! \param pos		position of the element to delete
				//! \remark This function has runtime O(n)
				void erase(const int32 pos)
				{
					Iterator iter= begin();
					iter+= pos;
					mVec.erase(iter);
				}

				//! \brief insert an element before a position specified by an iterator
				//! \param iter		position before which we should insert
				//! \param value	value to insert
				//! \returns an iterator to the position of the new element
				//! \remark This function has runtime O(n)
				Iterator insert(const Iterator& iter,Control::FSMInstanceWrapper& value)
				{
					return mVec.insert(iter,value);
				}

				//! \brief insert a number of elements before the specified position
				//! \param iter		position before which we should insert
				//! \param size		number of elements to insert
				//! \param value	value we should use for every new element
				//! \remark This function has runtime O(n)
				void insert(const Iterator& iter,const uint32 size, Control::FSMInstanceWrapper& value)
				{
					mVec.insert(iter,size,value);
				}

				//! \brief insert an element at the specified position
				//! \param pos		position where we should insert the new value
				//! \param value	value to insert
				//! \remark The new element will have position pos. This function has runtime O(n)
				void insert(const uint32 pos, Control::FSMInstanceWrapper& value)
				{
					Iterator iter= begin();
					iter+= pos;
					mVec.insert(iter,value);
				}

				//! \brief insert a number of elements before the specified position
				//! \param pos		position before which we should insert
				//! \param size		number of elements to insert
				//! \param value	value we should use for every new element
				//! \remark This function has runtime O(n)
				void insert(const uint32 pos, const uint32 size, Control::FSMInstanceWrapper& value)
				{
					Iterator iter= begin();
					iter+= pos;
					mVec.insert(iter,size,value);
				}

				//! \brief insert a number of elements [start,end[ from another container
				//! \param iter		position before which we should insert
				//! \param start	start of the element range to copy from
				//! \param end		element beyond the last element to copy from
				//! \remark This function has runtime O(n)
				template<class INPUT_ITERATOR>
				void insert(const Iterator& iter,const INPUT_ITERATOR& start, const INPUT_ITERATOR& end)
				{
					X_ASSERT_MSG(false, "This method should not be used");

					mVec.insert(iter,start,end);
				}

				//! \brief resize the vector
				//! \param newSize		the number of elements the vector has after calling
				//! \param value		all added elmenents will be initialized with
				void resize(const uint32 newSize, Control::FSMInstanceWrapper& value )
				{
					mVec.resize(newSize,value);
				}

				//! \brief resize the vector
				//! \param newSize		the number of elements the vector has after calling
				void resize(const uint32 newSize)
				{
					mVec.resize(newSize);
				}

				//! \brief reserve internal memory for at least size elements
				//! \param size			number of elements to reserve memory
				void reserve(const uint32 size)
				{
					mVec.reserve(size);
				}

				//! \brief get the begin iterator of the container
				inline Iterator begin() 
				{	
					return mVec.begin();
				}

				//! \brief get the const begin iterator of the container
				inline ConstIterator begin() const
				{	
					return mVec.begin();
				}

				//! \brief get the end iterator of the container (beyond the last element)
				inline Iterator end()
				{
					return mVec.end();
				}

				//! \brief get the const end iterator of the container (beyond the last element)
				inline ConstIterator end() const
				{
					return mVec.end();
				}

				//! \brief get a reference to the first element
				//! \returns the a reference to the first element
				//! \remark This reference is only valid, if the vector is not empty
				inline Reference front()
				{
					return mVec.front();
				}

				//! \brief get a const reference to the first element
				//! \returns the a reference to the first element
				//! \remark This reference is only valid, if the vector is not empty
				inline ConstReference front() const
				{
					return mVec.front();
				}

				//! \brief find an element
				//! \param value	value to search for
				//! \returns an iterator to the found position or end-iterator if nothing found
				inline ConstIterator find(Control::FSMInstanceWrapper& value) const
				{
					ConstIterator iter = mVec.begin();
					ConstIterator end  = mVec.end();

					while( iter != end)
					{
						if(*iter == value) return iter;

						++iter;
					}
					return end;
				}


				//! \brief find an element
				//! \param value	value to search for
				//! \returns an iterator to the found position or end-iterator if nothing found
				inline Iterator find(Control::FSMInstanceWrapper& value)
				{
					Iterator iter = mVec.begin();
					Iterator end  = mVec.end();

					while( iter != end)
					{
						if(*iter == value) return iter;

						++iter;
					}
					return end;
				}

				//! \brief test if an element exists
				//! \param value	value to search for
				//! \returns true if the element exists at least one time in the vector, or false otherwise
				bool exists(Control::FSMInstanceWrapper& value) const
				{
					ConstIterator iter = find(value);

					if (iter != mVec.end())
						return true;
					else
						return false;
				}

				//! \brief test if vector is empty
				//! \returns true if empty, false otherwise
				bool empty() const
				{
					return mVec.empty();
				}

				//! \brief clear the vector
				//! \remark The vector will be resized to zero (also frees the allocated memory)
				void clear()
				{
					mVec.clear();
				}

				//! \brief get the element at the specified index
				//! \param index	index of the element
				//! \returns a reference to the element
				Control::FSMInstanceWrapper& getElement(const uint32 index)
				{
					return mVec[index];
				}

				//! \brief set the element at the specified index
				//! \param index	index of the element
				//! \param value	the new value
				void setElement(const uint32 index, Control::FSMInstanceWrapper& value)
				{
					mVec[index] = value;
				}

				//! \brief get the element at the specified index
				//! \param index	index of the element
				//! \returns a reference to the element
				inline Reference operator[](const int32 index) 
				{
					return mVec[index];
				}

				//! \brief get the element at the specified index
				//! \param index	index of the element
				//! \returns a reference to the element
				inline ConstReference operator[](const int32 index) const
				{
					return mVec[index];
				}

				//! \brief make a copy from another vector of the wrapped array type
				void operator=(const ArrayType& x)
				{
					mVec = x;
				}

				bool operator==(const Vector<Control::FSMInstanceWrapper>& v) const
				{
					if(size() != v.size())
						return false;

					for (uint32 i = 0; i < size(); i++)
					{
						if(mVec[i] != v.mVec[i])
							return false;
					}

					return true;
				}

				bool operator!=(const Vector<Control::FSMInstanceWrapper>& v) const
				{
					if(size() != v.size())
						return true;

					for (uint32 i = 0; i < size(); i++)
					{
						if(mVec[i] != v.mVec[i])
							return true;
					}

					return false;
				}

				//! \brief sort the vector ascending with operator<
				void sort()
				{
					X_ASSERT_MSG(false, "This method should not be used");
				}

				//! \brief get the last element in the vector
				//! \returns a const reference to the last element in the vector
				ConstReference back() const
				{
					return mVec.back(); 
				}

				//! \brief get the last element in the vector
				//! \returns a reference to the last element in the vector
				Reference back()
				{
					return mVec.back(); 
				}

				//! \brief swap all element from one vector to another
				//! \param from		other vector to swap element with
				void swap(Vector& from)
				{
					mVec.swap(from.mVec);
				}

				int32 findElementPosition(const Control::FSMInstanceWrapper& value) const
				{
					for (uint32 i = 0; i < size(); i++)
					{
						if(mVec[i] == value) return i;
					}
					return -1;
				}
			};
		}
	}

	namespace Common
	{
		namespace Reflection
		{
			namespace Datatype
			{				
				template<>
				struct DatatypeSerializer<Control::FSMInstanceWrapper>
				{
					static void readValue(Serialize::SerializeStorage* stream, Control::FSMInstanceWrapper& value)
					{
						uint64 innerValue;
						XAIT_SERIALIZE_READ_VALUE_NAME_CHECK("innerValue",innerValue,stream,"ID");
						value = *((Control::FSMInstanceWrapper*)&innerValue);
					}

					static void writeValue(Serialize::SerializeStorage* stream, const Control::FSMInstanceWrapper& value)
					{
						uint64 innerValue;
						innerValue = *((uint64*)&value);
						stream->writeValue("innerValue",innerValue);
					}
				};
			}
		}
	}

	namespace Control
	{

		/*typedef Common::Container::Vector<FSMInstanceWrapper> FSMInstanceVector;

		class TypedFSMInstanceVector : public FSMInstanceVector
		{
		public:
		TypedFSMInstanceVector()
		{}			
		};*/

		class ControlDatatypesInitHelper
		{
		public:
			XAIT_CONTROL_DLLAPI static FSMEventID* stringToFSMEventID(const Common::String& string, const Common::Memory::AllocatorPtr& allocator);
			XAIT_CONTROL_DLLAPI static FSMInstanceWrapper* stringToFSMInstanceID(const Common::String& string, const Common::Memory::AllocatorPtr& allocator);
			XAIT_CONTROL_DLLAPI static Common::Container::Vector<FSMEventID>* stringToVectorFSMEventID(const Common::String& string, const Common::Memory::AllocatorPtr& allocator);
			XAIT_CONTROL_DLLAPI static Common::Container::Vector<FSMInstanceWrapper>* stringToVectorFSMInstanceWrapper(const Common::String& string, const Common::Memory::AllocatorPtr& allocator);

			static void registerControlDatatypes(Common::Reflection::Datatype::DatatypeManager::Ptr manager)
			{
				if (Common::Reflection::Datatype::Datatype<FSMEventID>::exists())
					return;

				manager->registerDatatype<FSMEventID>("Event", FSMEventID(), stringToFSMEventID);

				Common::Reflection::Datatype::Datatype<FSMInstanceWrapper>* instanceType = manager->registerDatatype<FSMInstanceWrapper>("FSMInstance", FSMInstanceWrapper(FSMManager::InvalidFSMInstanceID), stringToFSMInstanceID, NULL, NULL, NULL, NULL, &FSMInstanceWrapper::operator==, &FSMInstanceWrapper::operator!=);				
				instanceType->registerMemberFunction<FSMInstanceWrapper>("Start", &Control::FSMInstanceWrapper::start);
				instanceType->registerMemberFunction<FSMInstanceWrapper>("Suspend", &Control::FSMInstanceWrapper::suspend);
				instanceType->registerMemberFunction<FSMInstanceWrapper>("Resume", &Control::FSMInstanceWrapper::resume);
				instanceType->registerMemberFunction<FSMInstanceWrapper>("IsValid", &Control::FSMInstanceWrapper::isValid);
				instanceType->registerMemberFunction<FSMInstanceWrapper>("HandleEvent", &Control::FSMInstanceWrapper::handleEvent);

				Common::Memory::AllocatorPtr allocator = manager->getMemoryAllocator();
				
				//FSMInstance Container
				Common::Reflection::Datatype::Datatype<Common::Container::Vector<FSMInstanceWrapper> >* vecFSMInstance = manager->registerDatatype<Common::Container::Vector<FSMInstanceWrapper> >
					("Container[FSMInstance]",
					Common::Container::Vector<FSMInstanceWrapper>(allocator),
					stringToVectorFSMInstanceWrapper,
					NULL,
					NULL,
					NULL,
					NULL,
					&Common::Container::Vector<FSMInstanceWrapper>::operator==,
					&Common::Container::Vector<FSMInstanceWrapper>::operator!=);

				vecFSMInstance->registerMemberFunction<Common::Container::Vector<FSMInstanceWrapper> >("Clear",&Common::Container::Vector<FSMInstanceWrapper>::clear);
				vecFSMInstance->registerMemberFunction<Common::Container::Vector<FSMInstanceWrapper> >("GetElement",&Common::Container::Vector<FSMInstanceWrapper>::getElement);
				vecFSMInstance->registerMemberFunction<Common::Container::Vector<FSMInstanceWrapper> >("PushBack",&Common::Container::Vector<FSMInstanceWrapper>::pushBack);
				vecFSMInstance->registerMemberFunction<Common::Container::Vector<FSMInstanceWrapper> >("RemoveElement",(void (Common::Container::Vector<FSMInstanceWrapper>::*)(const int32))&Common::Container::Vector<FSMInstanceWrapper>::erase);
				vecFSMInstance->registerMemberFunction<Common::Container::Vector<FSMInstanceWrapper> >("SetElement",&Common::Container::Vector<FSMInstanceWrapper>::setElement);
				vecFSMInstance->registerMemberFunction<Common::Container::Vector<FSMInstanceWrapper> >("Size",&Common::Container::Vector<FSMInstanceWrapper>::size);

				vecFSMInstance->registerMemberFunction<Common::Container::Vector<FSMInstanceWrapper> >("PopBack",&Common::Container::Vector<FSMInstanceWrapper>::popBack);
				vecFSMInstance->registerMemberFunction<Common::Container::Vector<FSMInstanceWrapper> >("Empty",&Common::Container::Vector<FSMInstanceWrapper>::empty);
				vecFSMInstance->registerMemberFunction<Common::Container::Vector<FSMInstanceWrapper> >("Exists",&Common::Container::Vector<FSMInstanceWrapper>::findElementPosition);				
			}
		};	
	}
}
