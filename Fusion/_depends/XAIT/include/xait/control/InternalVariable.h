// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/control/Variable.h>

namespace XAIT
{
	namespace Control
	{
		//! \brief Default variable type that provides standard functionality. Memory is allocated by xaitControl Library.
		class InternalVariable : public Variable
		{

		private:
			void* mValue;

			//! \brief Deinitialises the variable.
			void deinit(const Common::Memory::AllocatorPtr& allocator);
		
		public:

			//! \brief default constructor
			InternalVariable();

			//! \brief constructor
			InternalVariable(Common::Reflection::Datatype::DatatypeID datatypeID, void* value);

			//! \brief copy constructor			
			InternalVariable(const InternalVariable& variable);
			
			//! \brief destructor
			~InternalVariable();
			
			//! \brief Assignment operator.
			InternalVariable& operator=(const InternalVariable& variable);

			//! \brief retrieve the stored value
			void* getValuePtr() const;
			
			//! \brief set the store value
			void setValueByPtr(void* value);

			void writeToStream(Common::Serialize::SerializeStorage* stream);

			void readFromStream(Common::Serialize::SerializeStorage* stream);

		protected:

			CONCRETETYPE getVarType()
			{
				return INTERNAL;
			}
		};
	}
}