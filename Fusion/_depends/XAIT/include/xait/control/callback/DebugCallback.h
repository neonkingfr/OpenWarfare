// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/control/FSMManager.h>
#include <xait/control/FSMInstance.h>
#include <xait/common/memory/AllocatorCaptured.h>

namespace XAIT
{
	namespace Control
	{
		class Interface;

		namespace Callback
		{
			//! \brief This class contains functions to get debug information from the debug DLL. Note that the release DLL will not call any function in this callback.
			class DebugCallback : public Common::Memory::AllocatorCaptured
			{
				friend class Control::Interface;
			public:
				typedef Common::SharedPtr<DebugCallback>	Ptr;

				//! \brief constrtuctor
				DebugCallback()
				{}

				//! \brief constrtuctor
				//! \param dealloc		custom deallocator
				DebugCallback(Common::Memory::AllocatorCaptured::MemoryDeallocator dealloc)
					: AllocatorCaptured(dealloc)
				{}

				//! \brief This function is called on handling events.
				//! \param fsmInstanceID	the instance id of the FSM which handled the event
				//! \param eventID			the handled event
				//! \see queuedEventMessage
				virtual void handleEventMessage(const FSMInstance* fsmInstance, const FSMEvent* event)
				{
					std::cout << "FSMInstance: " << fsmInstance->getName()  << " is handling event: " << event->getFullName() << std::endl; 
				}

				//! \brief This function is called on queuing events.
				//! \param fsmInstanceID	the instance id of the FSM instance which queued the event
				//! \param eventID			the handled event
				//! \see handleEventMessage
				virtual void queuedEventMessage(const FSMInstance* fsmInstance, const FSMEvent* event)
				{
					std::cout << "FSMInstance: " << fsmInstance->getName()  << " is queuing event: " << event->getFullName() << std::endl; 
				}

				//! \brief This function is called when event handling finished.
				//! \param fsmInstanceID	the instance id of the FSM instance which finished event handling
				//! \param eventID			the handled event
				virtual void finishedHandlingEventMessage(const FSMInstance* fsmInstance, const FSMEvent* event)
				{
					std::cout << "FSMInstance: " << fsmInstance->getName()  << " finished handling event: " << event->getFullName() << std::endl; 
				}
 
				//! \brief This function is called when the state changes.
				//! \param fsmInstanceID	the instance ID of the FSM instance
				//! \param oldStateID		the old state
				//! \param newStateID		the new state
				//! \param oldFSMID			the FSMID of the previous fsm 
				//! \param newFSMID			the FSMID of the new fsm ( = oldFSMID if subFSMID == -1)
				//! \param subFSMID			contains the SubFSMID of the new sub FSM. >=0 if there is a change into a sub FSM; == -1 if the fsm stays the same and < -1 if there is a change into a higher level in the fsm hierarchy
				virtual void handleStateChangeMessage(const FSMInstance* fsmInstance,const Common::String& oldStateName ,const Common::String& newStateName,
					const FSM* oldFSM, const FSM* newFSM, const uint32 subFSMID) 
				{
					if (subFSMID == (uint32)-1)
						std::cout << "FSM Instance: " << fsmInstance->getName() << " changed to state: " << newStateName << std::endl;
					else
						std::cout << "FSM Instance: " << fsmInstance->getName() << " changed from FSM: " << oldFSM->getName() << " to FSM: " << newFSM->getName() << " into the state: " << newStateName << std::endl;
				}	

				//! \brief This function is called when a transitioning is rejected by NOOP probability
				//! \param instanceID	the instance ID of the FSM instance
				virtual void rejectedByNoopMessage(FSMInstance* instanceID)
				{}

		 		

			protected:
				FSMManager*	mFSMManager;
			};
		}
	}
}
