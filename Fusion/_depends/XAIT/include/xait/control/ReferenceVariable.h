// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/control/Variable.h>

namespace XAIT
{
	namespace Control
	{
		//! \brief Functionally identical to InternalVariable except that the value pointer 
		//!	\brief used by this class is provided by the user. Memory is allocated by client code.
		class ReferenceVariable : public Variable
		{

		private:
			void* mValue;

			//! \brief Deinitialises the variable.
			void deinit(const Common::Memory::AllocatorPtr& allocator);

		public:

			//! \brief default constructor
			ReferenceVariable();

			//! \brief constructor
			ReferenceVariable(Common::Reflection::Datatype::DatatypeID datatypeID, void* value);

			//! \brief copy constructor			
			ReferenceVariable(const ReferenceVariable& variable);

			//! \brief destructor
			~ReferenceVariable();

			//! \brief Assignment operator.
			ReferenceVariable& operator=(const ReferenceVariable& variable);
			
			//! \brief get value from user pointer
			void* getValuePtr() const;

			//! \brief set value by user pointer
			void setValueByPtr(void* value);

			void writeToStream(Common::Serialize::SerializeStorage* stream);

			void readFromStream(Common::Serialize::SerializeStorage* stream);
			
		protected:

			CONCRETETYPE getVarType()
			{
				return REFERENCE;
			}
		};
	}
}