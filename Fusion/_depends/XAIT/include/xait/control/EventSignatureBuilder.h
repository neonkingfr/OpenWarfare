// (C) xaitment GmbH 2006-2012

// (C) 2011 xaitment GmbH

#pragma once

#include <xait/common/container/Vector.h>
#include <xait/common/reflection/datatype/DatatypeTypedefs.h>
#include <xait/common/reflection/function/FunctionArgumentList.h>

namespace XAIT
{
	namespace Control
	{
		class EventSignatureBuilder
		{
			Common::Container::Vector<Common::Reflection::Datatype::DatatypeID> mSignature;
			bool mContainsError;

		public:

			EventSignatureBuilder(uint32 size, const Common::Memory::AllocatorPtr& allocator)
				:mSignature(size, Common::Reflection::Datatype::DatatypeID(), allocator), mContainsError(false)
			{

			}

			inline void setLen(const uint32 len)
			{
			}

			inline bool containsError()
			{
				return mContainsError;
			}

			inline Common::Container::Vector<Common::Reflection::Datatype::DatatypeID>& getSignature()
			{
				return mSignature;
			}

			template<typename T_PARAM>
			inline void process(const uint32 paramID, T_PARAM& param)
			{
				if(Common::Reflection::Datatype::Datatype<T_PARAM>::exists())
				{
					Common::Reflection::Datatype::DatatypeID typeID = GET_DATATYPE_ID(T_PARAM);
					mSignature[paramID] = typeID;	
				}
				else
				{
					mSignature[paramID] = Common::Reflection::Datatype::DatatypeID();
					mContainsError = true;
					XLOG_MSG("Triggered event with not supported type at index: " << paramID);
				}
			}
		};

		class EventArgumentBuilder
		{
			Common::Reflection::Function::FunctionArgumentList mParameters;

		public:

			EventArgumentBuilder(const Common::Container::Vector<Common::Reflection::Datatype::DatatypeID>& signature, const Common::Memory::AllocatorPtr& allocator)
				:mParameters(signature, allocator)
			{
			}

			inline void setLen(const uint32 len)
			{
			}

			inline Common::Reflection::Function::FunctionArgumentList& getArgumentList()
			{
				return mParameters;
			}

			template<typename T_PARAM>
			inline void process(const uint32 paramID, T_PARAM& param)
			{
				mParameters.setValue(&param, paramID);
			}
		};
	}
}