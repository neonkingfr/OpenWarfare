// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/callback/MemoryCallback.h>
#include <xait/common/callback/FileIOCallback.h>
#include <xait/common/callback/MathCallback.h>
#include <xait/common/reflection/ReflectionInterface.h>
#include <xait/common/reflection/ReflectionInterfaceInit.h>
#include <xait/common/Interface.h>

#include <xait/control/callback/DebugCallback.h>
#include <xait/control/FSMManager.h>
#include <xait/control/DebuggerAddress.h>
#include <xait/control/ControlDatatypes.h>

namespace XAIT
{
	namespace Control
	{
		//! \brief The main library interface. It provides functions to initialize and close the library and to get access to the FSMManager.
		//!
		//! The first call to the interface has to be initLibrary. You can then get the FSM Manager with the method getFSMManager. When you close your project, close the xaitControl library with a call to closeLibrary. 
		class Interface
		{
		public:
			//! \brief Initializes the xaitControl library.
			//!
			//! The first function call to the xaitControl library must be initLibrary.
			//! \param ioCallback	a pointer to the file I/O callback object
			//! \param mathCallback	a pointer to the math callback object
			//! \see closeLibrary
			static void initLibrary(const XAIT::Common::Callback::FileIOCallback::Ptr& ioCallback,
									const XAIT::Common::Callback::MathCallback::Ptr& mathCallback,
									const DebuggerAddress& localAddress= DebuggerAddress()
									)
			{
				initLibrary(ioCallback,mathCallback,XAIT::Control::Callback::DebugCallback::Ptr(NULL),localAddress);
			}

			//! \brief Initializes the xaitControl library with a particular debug callback
			//!
			//! The first function call to the xaitControl library must be initLibrary.
			//! \param ioCallback		a pointer to the file I/O callback object
			//! \param mathCallback		a pointer to the math callback object
			//! \param debugCallback	a pointer to the debug callback object
			//! \see closeLibrary
			static void initLibrary(const XAIT::Common::Callback::FileIOCallback::Ptr& ioCallback,
									const XAIT::Common::Callback::MathCallback::Ptr& mathCallback,
									const XAIT::Control::Callback::DebugCallback::Ptr& debugCallback,
									const DebuggerAddress& localAddress= DebuggerAddress()
									)
			{
				// if the library is dynamic, it has its own address space and needs therefore its own init call to common
				XAIT_INIT_REFLECTION(Common::Interface::getGlobalAllocator());
				XAIT::Control::ControlDatatypesInitHelper::registerControlDatatypes(XAIT::Common::Reflection::ReflectionInterface::getDatatypeManager());
				initLibrary(ioCallback,mathCallback,debugCallback,Common::Reflection::ReflectionInterface::getDatatypeManager(),localAddress);
			}

			//! \brief Initializes the xaitControl library with a particular debug callback and a particular data type manager.
			//!
			//! The first function call to the xaitControl library must be initLibrary.
			//! \param ioCallback		a pointer to the file I/O callback object
			//! \param mathCallback		a pointer to the math callback object
			//! \param debugCallback	a pointer to the debug callback object
			//! \param datatypeManager	a pointer to the data type manager object
			//! \param localAddress		local debugger address, can override default device, default port or both
			//! \see closeLibrary
			XAIT_CONTROL_DLLAPI static void initLibrary(const XAIT::Common::Callback::FileIOCallback::Ptr& ioCallback,
														const XAIT::Common::Callback::MathCallback::Ptr& mathCallback,
														const XAIT::Control::Callback::DebugCallback::Ptr& debugCallback,
														const XAIT::Common::Reflection::Datatype::DatatypeManager::Ptr& datatypeManager,
														const DebuggerAddress& localAddress= DebuggerAddress()
														);


			//! \brief Closes the library and releases all resources.
			//!
			//! The last function call to the xaitControl library should be closeLibrary.
			//! \see initLibrary
			XAIT_CONTROL_DLLAPI static void closeLibrary()
			{
				Common::Reflection::ReflectionInterface::close();
				internalClose();
			}

			//! \brief Returns the FSMManager from the initialized xaitControl library.
			//!
			//! The FSM Manager gives you access to the functionality of the xaitControl library.
			//! \returns the FSMManager instance that is active in the library
			XAIT_CONTROL_DLLAPI static FSMManager* getFSMManager();

			//! \brief Returns the memory allocator that is used by the xaitControl library.
			//! \returns the memory allocator that is used by the library
			XAIT_CONTROL_DLLAPI static const Common::Memory::AllocatorPtr& getMemoryAllocator();
		

		private:
			XAIT_CONTROL_DLLAPI static void internalClose();
		};
	}
}

