// (C) xaitment GmbH 2006-2012

#pragma once

namespace XAIT
{
	namespace Control
	{
		class ExternalVariableCallback
		{
		public:
			virtual ~ExternalVariableCallback(){};

			virtual void* getValue() = 0;
			virtual void setValue(void* value) = 0;
		};
	}
}