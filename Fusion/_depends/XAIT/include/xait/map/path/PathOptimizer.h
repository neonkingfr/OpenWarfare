// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/map/CompileDefines.h>
#include <xait/map/path/PathResult.h>
#include <xait/map/path/CompiledSearchConfig.h>

namespace XAIT
{
	namespace Map
	{
		namespace Path
		{
			class PathOptimizer
			{
			public:
				//! \brief constructor
				XAIT_MAP_DLLAPI PathOptimizer();

				//! \brief constructor
				//! \param searchConfig			the search config that is used to recompute the path costs. implicitly activates storeAllPolygons
				XAIT_MAP_DLLAPI PathOptimizer(const CompiledSearchConfig::Ptr& searchConfig);

				//! \brief Computes an optimized path from an unoptimized path (funneling)
				//! \returns An optimized path. The object returned must be externally deleted (use normal delete)
				//! \remarks Optimization of an already optimized path can lead to unpredicted behavior or even crash.
				//! \remarks the optimized path does NOT leave the given polygons of the original path
				XAIT_MAP_DLLAPI PathResult* computeOptimizedPath(const PathResult* origPath);

				
				//! \brief Computes an optimized path from an unoptimized path
				//! \returns An optimized path. The object returned must be externally deleted (use normal delete)
				//! \remarks the optimized path DOES leave the given polygons of the original path
				XAIT_MAP_DLLAPI PathResult* computeRayTracedPath(const PathResult* origPath);
		
			protected:
				enum Orientation
				{
					LEFT,
					RIGHT,
					PARALLEL
				};  

				struct Delimiter
				{
					Delimiter()
						: mPoint(0.0f, 0.0f, 0.0f), mIndex(X_MAX_UINT32)
					{
					};
					Delimiter(const XAIT::Common::Math::Vec3f& point)
						: mPoint(point), mIndex(X_MAX_UINT32)
					{
					};
				
				
					XAIT::Common::Math::Vec3f	mPoint;
					XAIT::uint32				mIndex;
				}; 


				const PathResult*				mOriginalPath;
				PathResult*						mOptimizedPath;
				CompiledSearchConfig::Ptr		mSearchConfig;

				// funneling
				bool getEdgeVertices(const PathSegment& pathSegment,XAIT::Common::Math::Vec3f& edgeStart, XAIT::Common::Math::Vec3f& edgeEnd );

				//! \returns Left if p2 is on the left side of p1 - origin
				Orientation getOrientationOf( const XAIT::Common::Math::Vec3f& origin, const XAIT::Common::Math::Vec3f& point1, const XAIT::Common::Math::Vec3f& point2) const;
				
				void addNavPoint(const Common::Math::Vec3f& point, const GlobalNodeID nodeID, const uint32 edgeID);

				void buildLinearPath(uint32 startIndex, uint32 endIndex, const XAIT::Common::Math::Vec3f& newPathStart, const XAIT::Common::Math::Vec3f& newPathEnd);

				// raytracing
				uint32 cutNext(uint32 startIndex, GlobalNodeID& nextNodeID);

			};

		}
	}
}
