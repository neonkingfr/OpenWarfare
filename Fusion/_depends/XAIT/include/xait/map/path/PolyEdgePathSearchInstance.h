// (C) xaitment GmbH 2006-2012

#pragma once


#include <xait/map/path/PolyEdgePathSearch.h>
#include <xait/map/path/PathSearchInstance.h>

namespace XAIT
{
	namespace Map
	{
		namespace Path
		{
			class SearchRequest;

			//! \brief	this class implements the PathSearchInstance interface with a polyedge path search 
			//!			see the PolyedgePathSearch.h for more informations
			class PolyEdgePathSearchInstance : public PathSearchInstance
			{
				friend class PolyEdgePathSearch;
				struct Storage;
			public:
				//! \brief destructor
				XAIT_MAP_DLLAPI virtual ~PolyEdgePathSearchInstance();

			private:
				PolyEdgePathSearchInstance(PolyEdgePathSearch* owner, const PolyEdgeAStarMap* map);

				virtual PathResult* getPath();

				virtual int runTypedSearch(const uint32 numSteps);

				virtual bool initTypedSearch(SearchRequest* searchRequest);

				virtual bool existsPossiblePath(const SearchRequest* request) const;


				Storage*	mStorage;
			};
		}
	}
}
