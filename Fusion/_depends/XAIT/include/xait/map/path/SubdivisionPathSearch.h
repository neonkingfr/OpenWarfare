// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/map/path/PathSearch.h>

namespace XAIT
{
	namespace Map
	{
		class NavMeshContainer;

		namespace Path
		{
			class SubdivisionAStarMap;

			//! \brief	this class implements a concrete pathsearch that uses the subdivisions of a polygon as nodes in astar
			//!			the subdivision uses astar nodes on the polygon edges like the polyedge algorithm
			//!			if length of a polygon edge is too long multiple astar nodes are created on the edge
			//!			this algorithm provides the best path results with the worst performance
			class SubdivisionPathSearch : public PathSearch
			{
			public:

				//! \brief constructor
				//! \param maxEdgeLen	the maximal length of edge that is not splitted in two subedges
				//! \param navMesh		the used navigation mesh
				XAIT_MAP_DLLAPI SubdivisionPathSearch(const float32 maxEdgeLen, const NavMeshContainer* navMesh);

				//! \brief destructor
				XAIT_MAP_DLLAPI virtual ~SubdivisionPathSearch();

				//! \brief creates an instance of the path search
				//! \returns an instance of the path search
				XAIT_MAP_DLLAPI virtual PathSearchInstance* createInstance(const Common::Memory::Allocator::Ptr& allocator= Common::Memory::Allocator::Ptr());

				virtual Common::String getTypeName() const
				{
					return "Subdivision";
				}

			protected:
				SubdivisionAStarMap*						mMap;
			};
		}
	}
}

