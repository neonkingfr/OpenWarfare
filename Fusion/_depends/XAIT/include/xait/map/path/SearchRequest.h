// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/map/path/SearchConfig.h>
#include <xait/map/path/PathResult.h>
#include <xait/map/path/PathOptimizer.h>

namespace XAIT
{
	namespace Map
	{
		namespace Path
		{
			class PathSearchInstance;

			//! \brief	This class encapsulates path search requests with all needed informations
			//!			path search request can be processed by path search instances
			//!			the actual state and the result be also queried
			class SearchRequest 
			{
				friend class PathSearchInstance;
			public:
				enum SearchState 
				{
					SEARCH_NOT_STARTED,		//!< Search not startet yet
					SEARCH_INITIALIZED,		//!< Search initialized (start/end point found) 
					SEARCH_RUNNING,			//!< Search is running
					SEARCH_SUCCEEDED,		//!< Path has been found
					SEARCH_FAILED,			//!< Path has not been found. \see getErrorCode for further informations.
				};

				enum ErrorCode
				{
					ERROR_NO_ERROR,					//!< No error
					ERROR_INVALID_START_POINT,		//!< Start point outside of the navigation mesh
					ERROR_INVALID_END_POINT,		//!< End point outside of the navigation mesh
					ERROR_NO_PATH_FOUND,			//!< No Path found for given start/endpoint and specified search config
					ERROR_ABORTED,					//!< Search has been aborted by a new search request given to the PathSearchInstance
					ERROR_UNKNOWN,					//!< unknown error
				};

				enum PathSmoothing
				{
					PS_NO_SMOOTHING,		//!< No path smoothing is applied
					PS_SMOOTHING,			//!< Path is smoothed but cannot leave the given polygons (default)
					PS_RAYTRACED,			//!< Uses raytracing to find a better path. the resulting path probably uses other polygons (slower than normal smoothing)
				};

				//! \brief constructor (compiles a search config)
				//! \param startPosition	the start position of the path
				//! \param endPosition		the end position of the path
				//! \param config			the configuration of the search
				XAIT_MAP_DLLAPI SearchRequest(const Common::Math::Vec3f& startPosition, const Common::Math::Vec3f& endPosition, const SearchConfig* config);

				//! \brief constructor uses default search config
				//! \param startPosition	the start position of the path
				//! \param endPosition		the end position of the path
				XAIT_MAP_DLLAPI SearchRequest(const Common::Math::Vec3f& startPosition, const Common::Math::Vec3f& endPosition);

				//! \brief constructor 
				//! \param startPosition	the start position of the path
				//! \param endPosition		the end position of the path
				//! \param config			the configuration of the search
				XAIT_MAP_DLLAPI SearchRequest(const Common::Math::Vec3f& startPosition, const Common::Math::Vec3f& endPosition, const CompiledSearchConfig::Ptr& config);

				//! \brief copy constructor
				XAIT_MAP_DLLAPI SearchRequest(const SearchRequest& src);

				~SearchRequest()
				{
					delete mPath;
				}

				//! \brief gets the state of the path search
				//! \returns the state of the path search
				inline SearchState getSearchState() const
				{
					return mState;
				}

				//! \brief Creates a compiled search config, or returns the cached one from the constructor.
				//! \returns The the cached compiled search config or creates a new one for the specified navmesh.
				XAIT_MAP_DLLAPI const CompiledSearchConfig::Ptr& getCompiledConfig(const NavMeshContainer* navMesh);

				//! \brief gets the used start position
				//! \returns the used start position
				inline const Common::Math::Vec3f& getStartPosition() const
				{
					return mStartPosition;
				}

				//! \brief gets the used end position
				//! \returns the used end position
				inline const Common::Math::Vec3f& getEndPosition() const
				{
					return mEndPosition;
				}

				//! \brief gets the resulting path if the search was successful
				//! \returns the result of the path search
				inline const PathResult* getPath() const
				{
					return mPath;
				}

				//! \brief gets the current error code
				//! \returns the init state of the search request
				inline ErrorCode getErrorCode() const
				{
					return mCurrentErrorCode;
				}

				//! \brief sets the path smoothing type
				//! \param type		the used algorithm
				//! \remark has to be set before sending the request
				//! \remark the PathOptimizer class is used to perform path smoothing after the path search
				inline void setPathSmoothing(PathSmoothing type)
				{
					mPathSmoothingType = type;
				}

				//! \brief gets the path smoothing type
				//! \returns the path smoothing type
				inline PathSmoothing getPathSmoothing() const
				{
					return mPathSmoothingType;
				}

				//! \brief reset search request
				//! 
				//! Resets result values of the search request, lets source values untouched.
				XAIT_MAP_DLLAPI void reset();

				//! \brief assignment operator
				XAIT_MAP_DLLAPI SearchRequest& operator=(const SearchRequest& src);

			private:
				CompiledSearchConfig::Ptr	mCompiledConfig;
				const SearchConfig*			mSearchConfig;

				Common::Math::Vec3f			mStartPosition;
				Common::Math::Vec3f			mEndPosition;


				SearchState					mState;
				ErrorCode					mCurrentErrorCode;

				PathResult*					mPath;

				PathSmoothing				mPathSmoothingType;

				void setPath(PathResult* path);
			};
}
	}
}
