// (C) xaitment GmbH 2006-2012

#pragma once 


#include <xait/common/memory/StaticMemoryManaged.h>
#include <xait/common/container/LookUp.h>
#include <xait/map/NavMeshContainer.h>
#include <xait/map/path/MultiStateCharacter.h>
#include <xait/map/MapMemoryPool.h>

namespace XAIT
{
	namespace Map
	{
		namespace Path
		{
			class SearchConfig;

			//! \brief	this class contains all configuration that are required to start a path request for certain characters
			//!			in contrast to the search config this class depends on the concrete nav mesh that is used by the path search
			//!			Also this class holds the costfunction that is used by the astar.
			class CompiledSearchConfig : public Common::Memory::StaticMemoryManaged<MapMemoryPool::getLowAllocThreadSafeAllocator>
			{
				friend class SearchConfig;
			public:
				typedef Common::SharedPtr<CompiledSearchConfig>		Ptr;
			

				//! \brief destructor
				XAIT_MAP_DLLAPI ~CompiledSearchConfig();

				//! \brief computes the costs of an edge
				//! \param startNodeID		the id of the startnode of the edge
				//! \param endNodeID		the id of the endnode of the edge
				//! \param edgeLength		the length of the edge
				//! \returns the costs of the edge (used by astar)
				//! \remark to implement a custom cost function you have the inherite this class and reimplement this member function
				XAIT_MAP_DLLAPI virtual float32 computeEdgeCosts(const GlobalNodeID startNodeID, const GlobalNodeID endNodeID, float32 edgeLength);

				//! \brief gets the navmesh container
				//! \returns used navmesh container
				inline const NavMeshContainer* getNavMesh() const
				{
					return mNavMesh;
				}

				//! \brief gets the minimal heightID
				//! \returns the minimal heightID
				inline uint32	getMinHeightID() const
				{
					return mMinHeightID;
				}

				//! \brief gets the contained characters
				//! \returns the contained characters
				inline const Common::Container::Vector<MultiStateCharacter>& getCharacters() const
				{
					return mCharacters;
				}


			protected:
				Common::Container::Vector<MultiStateCharacter>								mCharacters;
				Common::Container::Vector<Common::Container::LookUp<uint16,float32>*>		mSurfaceCostFactors;


				uint32						mMinHeightID;

				const NavMeshContainer*		mNavMesh;

				//! \brief constructor
				//! \param navMesh			the used navmesh
				//! \param searchConfig		the searchConfig that is compiled
				XAIT_MAP_DLLAPI CompiledSearchConfig(const NavMeshContainer* navMesh, const SearchConfig* searchConfig);

				//! \brief gets the surface costs in regard to a certain height class and surface type
				//! \brief heightClassID	the heightClass of the polygon
				//! \brief surfaceID		the surface type of the polygon
				//! \returns the cost factor of the given surface and height
				XAIT_MAP_DLLAPI float32 getSurfaceCostFactor(uint32 heightClassID, uint16 surfaceID) const;

				XAIT_MAP_DLLAPI Common::Container::LookUp<uint16,float32>* createSurfaceCostTable(float32 defaultValue);
			};
		}
	}
}

