// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/map/path/CharacterConfig.h>
#include <xait/map/MapMemoryPool.h>

namespace XAIT
{
	namespace Map
	{
		namespace Path
		{
			//! \brief	this class allows to define characters that are able to change their state
			//!			for example if you defined 2 characters in the xaitMap Creator that represent
			//!			two different states of one character (e.g. StandingSoldier and DuckedSoldier)
			class MultiStateCharacter : public Common::Memory::StaticMemoryManaged<MapMemoryPool::getLowAllocThreadSafeAllocator>
			{
			public:
				//! \brief constructor
				XAIT_MAP_DLLAPI MultiStateCharacter();

				//! \brief constructor
				//! \param characterConfig	the first character 
				XAIT_MAP_DLLAPI MultiStateCharacter(const CharacterConfig& characterConfig);

				//! \brief adds a character to the collection
				//! \param characterConfig		the characterConfig of the character
				XAIT_MAP_DLLAPI void addCharacter(const CharacterConfig& characterConfig);

				//! \brief gets the characters
				//! \returns all characters in sorted by their height
				inline const Common::Container::Vector<CharacterConfig>& getCharacters() const
				{
					return mCharacters;
				}

			private:
				Common::Container::Vector<CharacterConfig> mCharacters;
			};
		}
	}
}
