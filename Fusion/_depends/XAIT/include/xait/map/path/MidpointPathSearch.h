// (C) xaitment GmbH 2006-2012

#pragma once 
 
#include <xait/map/path/PathSearch.h>

namespace XAIT
{
	namespace Map
	{
		class NavMeshContainer;

		namespace Path
		{
			class MidpointAStarMap;

			//! \brief this class implements a concrete pathsearch that uses the midpoints of a polygon as nodes in astar
			class MidpointPathSearch : public PathSearch
			{
			public:

				//! \brief constructor
				//! \param navMesh		the used navigation mesh
				XAIT_MAP_DLLAPI MidpointPathSearch(const NavMeshContainer* navMesh);

				//! \brief destructor
				XAIT_MAP_DLLAPI virtual ~MidpointPathSearch();
				
				//! \brief creates an instance of the path search
				//! \returns an instance of the path search
				XAIT_MAP_DLLAPI virtual PathSearchInstance* createInstance(const Common::Memory::Allocator::Ptr& allocator= Common::Memory::Allocator::Ptr());

				virtual Common::String getTypeName() const
				{
					return "Midpoint";
				}

			protected:
				MidpointAStarMap*						mMap;
			};
		}
	}
}

