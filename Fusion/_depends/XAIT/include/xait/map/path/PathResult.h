// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/container/Vector.h>
#include <xait/common/math/Vec3.h>
#include <xait/map/NavMeshContainer.h>
#include <xait/common/memory/StaticMemoryManaged.h>
#include <xait/map/MapMemoryPool.h>
#include <xait/map/move/PathIterator.h>
#include <xait/map/path/PathResult.h>

namespace XAIT
{
	namespace Map
	{
		namespace Path
		{
			//! \brief A Line segment of a path
			//!
			//! The path segment represents a part of a path. Its like a line
			//! segment with a start and end position where the end position is the
			//! start of the next path segment.
			//! The start of the segment is also connected to a node in the navigation
			//! mesh. If the end of the segment is in a different node, mPolygonEdgeID
			//! is a valid edge id, pointing to the neighbor node which holds the end point.
			//! There is also a cost entry which holds the costs of the path until the
			//! start of the current segment. 
			struct PathSegment
			{
				XAIT::Common::Math::Vec3f	mStartPos;			//!< Start position of the path segment
				GlobalNodeID				mStartNodeID;		//!< ID of the navnode where this segment starts
				uint32						mPolygonEdgeID;		//!< ID of the polygon border in the above specified navnode this path segment travels across, could be invalid if it stays in the same polygon.
				float32						mCosts;				//!< Path costs until start position of the segment

				PathSegment()
					: mPolygonEdgeID(NavMeshNode::INVALID_POLYEDGEID)
					, mCosts(-1.0f)
				{}
			};

			//! \brief The PathResult class hosts a list of PathSegment which represent the found path.
			//!
			//! The PathResult class has a list of PathSegments which define the path returned by a path search.
			//! Further more there is an Iterator class that allows it to iterate over this path.
			class PathResult : public Common::Memory::StaticMemoryManaged<MapMemoryPool::getLowAllocThreadSafeAllocator>
			{
				Common::Container::Vector<PathSegment>	mPath;
				const NavMeshContainer*					mNavMesh;

			public:
				//! \brief The Iterator class can be used to iterate along a path.
				class Iterator
				{
					const PathResult*	mResult;
					const PathSegment*	mCurrSegment;
					uint32				mCurrPos;
					const NavMeshNode*	mCurrNode;

				public:
					Iterator()
						: mResult(NULL)
						, mCurrSegment(NULL)
						, mCurrPos(X_MAX_UINT32)
						, mCurrNode(NULL)
					{}

					//! \brief Test if the iterator is valid
					inline bool isValid() const
					{
						return mResult != NULL && (mCurrPos + 1) < mResult->mPath.size();
					}

					//! \brief Increment a valid iterator to the next path segment
					inline Iterator& operator++()
					{
						++mCurrPos;
						mCurrSegment= &mResult->mPath[mCurrPos];
						return *this;
					}

					//! \brief Increment a valid iterator to the next path segment
					inline Iterator& operator--()
					{
						--mCurrPos;
						mCurrSegment= &mResult->mPath[mCurrPos];
						return *this;
					}

					inline bool operator==(const Iterator& other) const
					{
						return mCurrSegment == other.mCurrSegment;
					}

					inline bool operator!=(const Iterator& other) const
					{
						return mCurrSegment != other.mCurrSegment;
					}

					//! \brief Get the current path segment
					inline const PathSegment& operator*() const
					{
						return *mCurrSegment;
					}

					//! \brief Get the owner of the iterator
					inline const PathResult* getPath() const
					{
						return mResult;
					}

					//! \brief Get the current path segment
					inline const PathSegment& getPathSegment() const
					{
						return *mCurrSegment;
					}

					//! \brief Get the start node id of the current PathSegment
					inline GlobalNodeID getStartNodeID() const
					{
						return mCurrSegment->mStartNodeID;
					}

					//! \brief Get the end node id of the current PathSegment
					//! \remark This method make a lookup to the next path segment, which makes it slower than getStartNodeID.
					inline GlobalNodeID getEndNodeID() const
					{
						if (mCurrSegment->mPolygonEdgeID == NavMeshNode::INVALID_POLYEDGEID)
							return mCurrSegment->mStartNodeID;	// return start node if we do not leave this node

						return mResult->mNavMesh->getNode(mCurrSegment->mStartNodeID).getPolygonEdge(mCurrSegment->mPolygonEdgeID).mNeighbourID;
					}

					//! \brief Get the start node of the current PathSegment
					inline const NavMeshNode& getStartNode() const
					{
						return mResult->mNavMesh->getNode(getStartNodeID());
					}

					//! \brief Get the end node of the current PathSegment
					//! \remark This method makes a lookup to the next path segment, which makes it slower than getStartNode
					inline const NavMeshNode& getEndNode() const
					{
						return mResult->mNavMesh->getNode(getEndNodeID());
					}

					//! \brief Get the id of the crossed edge of the start node
					//! \remark if the segment does not cross an edge, the id is X_MAX_UINT32
					inline uint32 getCrossedEdgeID() const
					{
						return  mCurrSegment->mPolygonEdgeID;
					}

					//! \brief Get the 3D start position of the path segment
					inline Common::Math::Vec3f getStartPos() const
					{
						return mCurrSegment->mStartPos;
					}

					//! \brief Get the 3D end position of the path segment
					//! \remark This method makes a lookup to the next path segment, which makes it slower than getStartPos
					inline Common::Math::Vec3f getEndPos() const
					{
						return getNextSegment().mStartPos;
					}

					//! \brief Get the costs of the complete path until the start of this segment
					inline float32 getStartCosts() const
					{
						return mCurrSegment->mCosts;
					}

					//! \brief Get the costs of the complete path until the end of this segment
					//! \remark This method makes a lookup to the next path segment, which makes it slower than getStartCosts
					inline float32 getEndCosts() const
					{
						return getNextSegment().mCosts;
					}

					//! \brief gets the current segment ID
					inline uint32 getSegmentID() const
					{
						return mCurrPos;
					}


				private:
					friend class PathResult;

					Iterator(const PathResult* result, const uint32 startPos)
						: mResult(result)
						, mCurrPos(startPos)
					{
						mCurrSegment= &mResult->mPath[mCurrPos];
					}

					const PathSegment& getNextSegment() const
					{
						return mResult->mPath[mCurrPos + 1];
					}
				};

				PathResult(const NavMeshContainer* navMesh)
					: mPath(getAllocator())
					, mNavMesh(navMesh)
				{}

				//! \brief Get the navmesh this path is created for
				inline const NavMeshContainer* getNavMesh() const
				{
					return mNavMesh;
				}

				//! \brief Test if the path is empty
				inline bool empty() const
				{
					return mPath.empty();
				}

				//! \brief Test if the path is still valid
				inline bool validate(Move::PathIterator pathiter) const
				{
					X_ASSERT_MSG(pathiter.getCurrentPath() == this, "Call to validate with mismatching paths!");
					Iterator iter = getPathIterator(pathiter.getCurrentSegmentID());
					return validate(iter);
				}

				inline bool validate() const
				{
					Iterator iter = getPathIterator();
					return validate(iter);
				}

				inline bool validate(Iterator iter) const
				{
					for (;iter.isValid(); ++iter)
					{
						uint32 edgeID = iter.getCrossedEdgeID();
						if (edgeID < 3)
						{
							const PolygonEdge& edge = iter.getStartNode().getPolygonEdge(edgeID);
							if (edge.mNeighbourID == GlobalNodeID())
								return false;
						}
					}
					return true;
				}

				//! \brief Get the complete path costs
				inline float32 getCosts() const
				{
					return getEndSegment().mCosts;
				}

				//! \brief Get the start position of the path
				inline const Common::Math::Vec3f& getStartPos() const
				{ 
					return getFirstSegment().mStartPos;
				}

				//! \brief Get the end position of the path
				inline const Common::Math::Vec3f& getEndPos() const
				{
					return getEndSegment().mStartPos;
				}

				//! \brief Get a path iterator for path traversal
				//! \param startSegmentID		ID of the segment the iterator should start. The first is 0.
				inline Iterator getPathIterator(const uint32 startSegmentID= 0) const
				{
					X_ASSERT_MSG2(startSegmentID < mPath.size(),"startSegmentID out of range !",startSegmentID,mPath.size());
					return Iterator(this,startSegmentID);
				}

				//! \brief Get the number of segment in the path
				inline uint32 getNumSegments() const
				{
					return mPath.size() - 1;
				}

				//! \brief Get a path segment by its ID
				//! \param segmentID	ID of the segment to retrieve
				PathSegment& getSegment(const int32 segmentID)
				{
					return mPath[segmentID];
				}

				//! \brief Get a path segment by its ID
				//! \param segmentID	ID of the segment to retrieve
				const PathSegment& getSegment(const int32 segmentID) const
				{
					return mPath[segmentID];
				}


				//! \name Internal use
				//@{
				inline void addSegment(const PathSegment& chunk)
				{
					mPath.pushBack(chunk);
				}

				void reversePath()
				{
					const uint32 pathSize= (uint32)mPath.size();
					uint32 reversePos = pathSize - 1;
					PathSegment tmp;
					for(uint32 i= 0; i < pathSize / 2; ++i,--reversePos)
					{
						tmp = mPath[i];
						mPath[i]= mPath[reversePos];
						mPath[reversePos]= tmp;
					}
				}
				//@}


			private:
				const PathSegment& getFirstSegment() const
				{
					X_ASSERT_MSG(!empty(),"Path is empty !");
					return mPath.front();
				}

				const PathSegment& getEndSegment() const
				{
					X_ASSERT_MSG(!empty(),"Path is empty !");
					return mPath.back();
				}
			};
		}
	}
}

