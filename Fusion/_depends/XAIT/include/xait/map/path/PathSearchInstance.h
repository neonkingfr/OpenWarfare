// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/map/GlobalNodeID.h>
#include <xait/map/path/SearchRequest.h>

namespace XAIT
{
	namespace Map
	{
		namespace Path
		{
			class PathSearch;

			//! \brief	this class contains all dynamic data (e.g. astar storage) of a path search and
			//!			provides all functionality to proceed a path request
			//!			the creation of path search instance requires the allocation of own storage
			//!			multiple instance of the same path search can be used to proceed path requests in parallel
			class PathSearchInstance : public Common::Memory::MemoryManaged
			{
			public:
				//! \brief destructor
				XAIT_MAP_DLLAPI virtual ~PathSearchInstance();

				//! \returns true if the search was initialized correctly, false otherwise (e.g. start/endpoint not found)
				//! \remark if error occurred and function returned false, a more detailed error code can be queried from the search request
				//! \remark if there is another unfinished search request the previous request is aborted
				XAIT_MAP_DLLAPI bool init(SearchRequest* searchRequest);

				//! \brief run path search for certain amount of steps
				//! \param numSteps		number of iterations the a-star should run
				//! \returns true if the search is still running, false otherwise (error or done)
				XAIT_MAP_DLLAPI bool run(const uint32 numSteps= 0xFFFFFFFF);

				//! \brief abort current running path search
				XAIT_MAP_DLLAPI void abort();

				//! \brief gets the current search request
				//! \returns the current search request;
				//! \remark The search request is set as long the search is still running. If the search is failed or succeeded, this returns NULL.
				XAIT_MAP_DLLAPI SearchRequest* getCurrentSearchRequest();


			protected:
				PathSearchInstance(PathSearch* owner);

				void setSearchRequestState(const SearchRequest::SearchState searchState);
				void setSearchRequestErrorCode(const SearchRequest::ErrorCode errorCode);

				CompiledSearchConfig* getCompiledConfig(SearchRequest* searchRequest);

				void finishSearch(); 

				virtual int runTypedSearch(const uint32 numSteps) = 0;

				virtual bool initTypedSearch(SearchRequest* searchRequest) = 0;

				virtual PathResult* getPath() = 0; 

				virtual bool existsPossiblePath(const SearchRequest* request) const= 0;

			private:
				PathSearch*				mOwner;
				SearchRequest*			mCurrentSearchRequest;

				void attachEvents();
				void removeEvents();

				void onNavMeshChanged(NavMeshContainer* navmesh);
			};
		}
	}
}
