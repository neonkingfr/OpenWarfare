// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/map/NavMeshContainer.h>
#include <xait/common/String.h>
#include <xait/common/io/Stream.h>
#include <xait/common/platform/Endian.h>
#include <xait/common/memory/ReadOnlyMemoryStream.h>
#include <xait/common/Interface.h>

namespace XAIT
{
	namespace Map
	{
		//! \brief Serializes the NavMesh.
		//!
		//! This class uses the memory allocator of the NavMesh to allocate additional memory.
		class NavMeshSerializer
		{
		public:
			//! \brief Reads a previously stored NavMesh from a file.
			//!
			//! This method needs a working FileIOCallback. It throws exceptions if something went wrong.
			//! \param fileName				the file to open and read from
			//! \param initialView			the view which should be initially set
			//! \param loadAllSectors		if true, all sectors of the current view will be loaded, if false no sectors will be loaded
			//! \param bufferSize			the size of local read ahead buffer (-1 == buffer disabled, useful if you have already a buffered stream)
			//! \returns an instance of the newly created NavMesh, or NULL if something went wrong
			//! \see writeNavMesh
			XAIT_MAP_DLLAPI static NavMeshContainer* XAIT_CDECL readNavMesh(const Common::String& fileName, const Common::String& initialView= XAIT_MAP_DEFAULT_VIEW_NAME, const bool loadAllSectors= true, const int32 bufferSize= 32768);

			//! \brief Reads a previously stored NavMesh from the given stream.
			//!
			//!	This method throws exceptions if something went wrong.
			//! \param inStream				the stream from which to read (needs to be readable)
			//! \param initialView			the view which should be initially set
			//! \param loadAllSectors		if true, all sectors of the current view will be loaded, if false no sectors will be loaded
			//! \param bufferSize			the size of local read ahead buffer (-1 == buffer disabled, useful if you have already a buffered stream)
			//! \returns an instance of the newly created NavMesh, or NULL if something went wrong
			//! \see writeNavMesh
			XAIT_MAP_DLLAPI static NavMeshContainer* XAIT_CDECL readNavMesh(const Common::SharedPtr<Common::IO::Stream>& inStream, const Common::String& initialView= XAIT_MAP_DEFAULT_VIEW_NAME, const bool loadAllSectors= true, const int32 bufferSize= 32768);

			//! \brief Reads a previously stored NavMesh from a memory buffer.
			//!
			//! The input buffer must stay valid until the NavMesh has been closed.
			//! If you want to initialize the NavMesh and then release the buffer afterwards, you should
			//! use the direct interface of the NavMeshContainer. There you can do memory streaming by
			//! providing the data streams when needed.
			//! This method throws exceptions if something went wrong.
			//! \param buffer				the buffer to read from
			//! \param bufferSize			the size of the buffer
			//! \param initialView			the view which should be initially set
			//! \param loadAllSectors		if true, all sectors of the current view will be loaded, if false no sectors will be loaded
			//! \returns an instance of the newly created NavMesh, or NULL if something went wrong
			//! \remark No FileIOCallback needed
			//! \see writeNavMesh
			XAIT_MAP_DLLAPI static NavMeshContainer* XAIT_CDECL readNavMesh(const void* buffer, const uint32 bufferSize,const Common::String& initialView= XAIT_MAP_DEFAULT_VIEW_NAME, const bool loadAllSectors= true);

			//! \brief Writes a navmesh to the disk.
			//!
			//! This needs a working FileIOCallback. Throws exceptions if something went wrong.
			//! \param fileName		the filename of the output file
			//! \param navMesh		the NavMesh which should be written
			//! \param endiness		the endiness that should be used to write the NavMesh
			//! \param bufferSize	size of local write buffer (-1 == buffer disabled, useful if you have already a buffered stream)
			//! \returns True if the navmesh could be written or false if not. See log for more informations if write failed.
			//! \see readNavMesh
			XAIT_MAP_DLLAPI static bool XAIT_CDECL writeNavMesh(const Common::String& fileName, const NavMeshContainer* navMesh, const Common::Platform::Endian::Endiness endiness, const int32 bufferSize= 64 * 1024);

			//! \brief Writes a NavMesh to a stream.
			//!
			//! This methods throws exceptions if something went wrong.
			//! \param outStream	the stream which should receive the NavMesh (must be writable)
			//! \param navMesh		the NavMesh which should be written
			//! \param endiness		the endiness that should be used to write the NavMesh
			//! \param bufferSize	the size of local write buffer (-1 == buffer disabled, useful if you have already a buffered stream)
			//! \returns True if the navmesh could be written or false if not. See log for more informations if write failed.
			//! \see readNavMesh
			XAIT_MAP_DLLAPI static bool XAIT_CDECL writeNavMesh(Common::IO::Stream* outStream, const NavMeshContainer* navMesh, const Common::Platform::Endian::Endiness endiness, const int32 bufferSize= 64 * 1024);
		};
	}
}
