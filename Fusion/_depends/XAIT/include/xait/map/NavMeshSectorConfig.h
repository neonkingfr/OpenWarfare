// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/FundamentalTypes.h>
#include <xait/common/math/Vec2.h>
#include <xait/common/geom/AABB2D.h>

namespace XAIT
{
	namespace Map
	{
		//! \brief Description of a NavMesh sector configuration.
		struct NavMeshSectorConfig
		{
			uint32					mSectorID;		//!< the sector ID
			Common::Math::Vec2f		mMin;			//!< the lower left offset of the navigation mesh sector (projected into a plane)
			float32					mEdgeLen;		//!< squared edge length

			//! \brief Constructor.
			//! \param sectorID the sector ID of the sector to which this configuration belongs
			//! \param min the lower left corner of the sector
			//! \param edgeLen the dimensions of the sector
			NavMeshSectorConfig(const uint32 sectorID, const Common::Math::Vec2f& min, const float32 edgeLen)
				: mSectorID(sectorID)
				, mMin(min)
				, mEdgeLen(edgeLen)
			{}

			//! \brief Default constructor.
			NavMeshSectorConfig()
				: mSectorID(0xFFFFFFFF)
				, mMin(Common::Math::Vec2f::ZERO)
				, mEdgeLen(0)
			{}

			//! \brief Gets the upper right corner of the sector configuration.
			//! \return a vector pointing to the upper right corner
			inline Common::Math::Vec2f getMax() const
			{
				return mMin + Common::Math::Vec2f(mEdgeLen,mEdgeLen);
			}

			//! \brief Gets the bounding box of the sector.
			//!
			//! Note that the bounding box is slightly increased by a small constant factor.
			//! \return the bounding box of the sector
			inline Common::Geom::AABB2Df getAABB() const
			{
				Common::Geom::AABB2Df sectorAABB;
				const Common::Math::Vec2f epsilon(X_EPSILON_F,X_EPSILON_F);

				sectorAABB.mMin= mMin - epsilon;
				sectorAABB.mMax= getMax() + epsilon;

				return sectorAABB;
			}
		};
	}
}
