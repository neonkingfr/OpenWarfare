// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/map/SolidPathObject.h>
#include <xait/common/math/Vec3.h>

namespace XAIT
{
	namespace Map
	{
		class BlockingPlaneSectorData;

		//! This path object represents a rectangle that is coplanar to a plane. The
		//! rectangle has a width and a height. Its orientation it specified by the up
		//! direction and the face-a direction vector. The negative face-a direction vector
		//! gives the direction in which the object is blocked when side A is blocked. 
		//! You can switch blocking of side A and B indivdually thus creating unidirectional
		//! connections.
		//! The default directions are:
		//! upDir		- z axis
		//! face a dir	- y axis
		//! States:
		//! Enable = OnState Setting
		//! Disable = Unblocked
		//! \brief A blocking path object with the shape of a rectangle that is coplanar to a plane. 
		class BlockingPlanePathObject : public SolidPathObject
		{
		public:
			//! \brief States of blocking if object is enabled (on)
			//! \remark This enum consists of  bit flags 
			enum OnStates
			{
				ONSTATE_BLOCKED		= 0x00,		//!< all sides blocked
				ONSTATE_A_UNBLOCKED	= 0x01,		//!< side A unblocked
				ONSTATE_B_UNBLOCKED	= 0x02,		//!< side B unblocked
				ONSTATE_UNBLOCKED = ONSTATE_A_UNBLOCKED | ONSTATE_B_UNBLOCKED,	//!< both sides unblocked
			};

			//! \brief Constructs a blocking plane path object
			//! \param name		Unique name
			//! \remark The name must be unique with the navmesh the object is intend to be used.
			XAIT_MAP_DLLAPI BlockingPlanePathObject(const Common::String& name);

			XAIT_MAP_DLLAPI virtual Common::Geom::AABB3Df getBoundingBox() const;

			//! \brief Sets the on state of the blocking plane object
			//! \remark This can also update the owning navmesh and there are no checks for thread safety.
			XAIT_MAP_DLLAPI void setOnState(const uint08 blockState);

			//! \brief Gets the on state of the blocking plane object
			uint08 getOnState() const { return mOnState; }

			//! \brief Sets the complete transformation of this blocking plane object
			//! \param upDir		up direction of the plane
			//! \param faceADir		direction vector in which side A will show (also the plane normal)
			//! \param height		height of the rectangle on the plane
			//! \param width		width of the rectangle on the plane
			//! \param pos			position of the rectangle where the origin of the rectangle is at the bottom
			//! \remark The position is a the bottom of the rectangle not in the center
			//! \remark The upDir and faceADir must not be normalized but they should be somehow perpendicular. If they are not perpendicular the up direction will be favored.
			XAIT_MAP_DLLAPI void setTransform(const Common::Math::Vec3f& upDir, const Common::Math::Vec3f& faceADir, const float32 height, const float32 width, const Common::Math::Vec3f& pos);

			//! \brief Gets the up direction
			XAIT_MAP_DLLAPI Common::Math::Vec3f getUpDirection() const;

			//! \brief Gets the face a direction
			XAIT_MAP_DLLAPI Common::Math::Vec3f getFaceADirection() const;

			//! \brief Gets the plane normal
			//! \remark The plane normal is equal to the face a direction
			inline Common::Math::Vec3f getNormal() const { return getFaceADirection(); }

			//! \brief Gets the position of the rectangle
			//! \remark The position is at the bottom of the rectangle not in the center.
			Common::Math::Vec3f getPosition() const { return getTransform().getPos(); }

			//! \brief Sets the position of the rectangle
			//! \remark The position is at the bottom of the rectangle not in the center.
			void setPosition(const Common::Math::Vec3f& pos) { getTransform().setPos(pos); }

			//! \brief Gets the height of the rectangle
			XAIT_MAP_DLLAPI float32 getHeight() const;

			//! \brief Gets the with of the rectangle
			XAIT_MAP_DLLAPI float32 getWidth() const;

			//! \brief Debug function for drawing the plane, returns the 4 vertices of the plane
			//! \param dst		dst will receive vertices and must be at least 4 elements long
			//! \returns True if the vertices could be generated, i.e vectors are valid, false otherwise
			//! \remarks The vertices are arrange like this
			//!  3 ---- 2
			//!  |      |
			//!  0 ---- 1
			XAIT_MAP_DLLAPI bool getVertices(Common::Math::Vec3f* dst) const;

			//! \brief Debug function for drawing the plane, returns 4 triangles -> 12 indices of the double faced plane
			//! \returns The 12 indices of the double faced plane
			XAIT_MAP_DLLAPI const uint32* getIndices() const;

			//! \brief Gets the number of debug indicies
			uint32 getNumIndices() const { return 12; }

			virtual PathObject* clone() const { return new BlockingPlanePathObject(*this); }

			XAIT_MAP_DLLAPI void refreshSectorData(PathObjectSectorData* data) const;
			XAIT_MAP_DLLAPI virtual void detachSectorData(PathObjectSectorData* data) const;
			XAIT_MAP_DLLAPI	virtual void appendSectorData(PathObjectSectorData* data);
			XAIT_MAP_DLLAPI	virtual void removeSectorData(PathObjectSectorData* data);
			XAIT_MAP_DLLAPI virtual uint32 getMemoryUsage() const;
		protected:
			XAIT_MAP_DLLAPI virtual bool initFromStream(Common::Serialize::SerializeStorage* serializer);
			XAIT_MAP_DLLAPI virtual void writeToStream(Common::Serialize::SerializeStorage* serializer) const;

		private:
			StaticContainer<BlockingPlaneSectorData*>::Vector	mSectorDatas;
			uint08												mOnState;

			void refreshConnections();
			void applyConnections(const uint08 state) const;
			void applyConnections(PathObjectSectorData* data, const uint08 state) const;

			XAIT_MAP_DLLAPI virtual void onEnableChanged();


		};
	}
}
