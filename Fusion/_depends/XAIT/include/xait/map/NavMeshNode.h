// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/memory/Allocator.h>
#include <xait/map/GlobalNodeID.h>
#include <xait/map/HighLevelNodeID.h>

namespace XAIT
{
	namespace Map
	{
		namespace Build
		{
			struct IndexedSurfacePolygon;
		}

		//! \brief A single edge of a polygon.
		struct PolygonEdge
		{
			uint32			mVertexID;                       //!< index in the vertex array of the NavMesh sector
			GlobalNodeID	mNeighbourID;                    //!< neighbor node of this edge. This is a GlobalNodeID that can be casted. If the node id is invalid, then there is no neighbor.

			//! \brief Default Constructor
			PolygonEdge()
			{}
				

			//! \brief Constructor.
			//! \param vertexID the index in the vertex array of the NavMesh sector
			//! \param neighbourID the ID of the neighbor node of the edge
			PolygonEdge(uint32 vertexID, uint32 neighbourID)
				: mVertexID(vertexID)
				, mNeighbourID(neighbourID)
			{}
		};

		//! \brief A single NavMesh node which represents a convex polygon.
		struct NavMeshNode
		{
		private:
			PolygonEdge			mPolygon[3];			//!< the list of polygon edges which form the polygon
			HighLevelNodeID		mHighlevelNodeID;	//!< link to the corresponding node in the HighLevelGraph

		public:
			enum Flags
			{
				FLAG_DELETED		= 0x01,		//!< mark node as deleted

				// special flags temporarily used for algorithms
				FLAG_VISITED		= 0x02,		//!< mark node as visited by an algorithm
			};
			enum
			{
				INVALID_POLYEDGEID	= 0xFFFFFFFF,	//!< Invalid polygon edge id
			};

			uint16				mSurfaceTypeID;	//!< surface type of this navigation node
			uint08				mHeightClassID;	//!< height classifier id for this node
			uint08				mFlags;			//!< nav node flags 

			//! \brief Constructor.
			XAIT_MAP_DLLAPI NavMeshNode();

			//! \brief Constructor.
			XAIT_MAP_DLLAPI NavMeshNode(const uint08 heightClassID, const uint16 surfaceTypeID);

			//! \brief Gets the number of edges of the polygon.
			inline uint32 getPolygonSize() const
			{
				return 3;
			}

			//! \brief Gets a particular polygon edge.
			//! \param edgeID	the ID of the edge, must be smaller than polygonSize
			//! \return the PolygonEdge
			inline PolygonEdge& getPolygonEdge(const uint32 edgeID)
			{
				X_ASSERT_MSG_DBG1(edgeID < 3,"edgeID out of range",edgeID);
				return mPolygon[edgeID];
			}

			//! \brief Gets a particular polygon edge.
			//! \param edgeID	the ID of the edge, must be smaller than polygonSize
			//! \return the PolygonEdge
			inline const PolygonEdge& getPolygonEdge(const uint32 edgeID) const
			{
				X_ASSERT_MSG_DBG1(edgeID < 3,"edgeID out of range",edgeID);
				return mPolygon[edgeID];
			}

			//! \brief Gets the height class ID of this node.
			//! \return the height class ID
			inline uint32 getHeightClassID() const
			{
				return mHeightClassID;
			}

			//! \brief Gets the surface type ID of this node.
			//! \return the surface type ID
			inline uint16 getSurfaceTypeID() const
			{
				return mSurfaceTypeID;
			}

			//! \brief Gets a particular polygon edge.
			//! \param edgeID	id of the edge, must be smaller than polygonSize
			inline PolygonEdge& operator[](const uint32 edgeID)
			{
				X_ASSERT_MSG_DBG1(edgeID < 3,"edgeID out of range",edgeID);
				return mPolygon[edgeID];
			}

			//! \brief get a certain polygon edge
			//! \param edgeID	id of the edge, must be below polygonSize
			//! \return the PolygonEdge
			inline const PolygonEdge& operator[](const uint32 edgeID) const
			{
				X_ASSERT_MSG_DBG1(edgeID < 3,"edgeID out of range",edgeID);
				return mPolygon[edgeID];
			}

			//! \brief Gets the complete polygon as an array of PolygonEdges.
			//! \return an array holding the polygon edges
			inline PolygonEdge* getPolygon()
			{
				return &mPolygon[0];
			}

			//! \brief Gets the complete polygon as an array of PolygonEdges.
			//! \return an array holding the polygon edges
			inline const PolygonEdge* getPolygon() const
			{
				return &mPolygon[0];
			}

			//! \brief Sets a single node flag.
			//! \param flag the flag
			//! \see Flags
			//! \see hasFlag
			//! \see clearFlag
			inline void setFlag(const Flags flag)
			{
				mFlags|= (uint16)flag;
			}

			//! \brief Clears the given flag.
			//! \param flag the flag
			//! \see Flags
			//! \see hasFlag
			//! \see setFlag
			inline void clearFlag(const Flags flag)
			{
				mFlags&= ~(uint16)flag;
			}

			//! \brief Tests whether the given flag is set.
			//! \param flag the flag
			//! \return true, if it is set, and false otherwise
			//! \see Flags
			//! \see setFlag
			//! \see clearFlag
			inline bool hasFlag(const Flags flag) const
			{
				return (mFlags & (uint16)flag) != 0;
			}

			//! \brief Gets the memory usage of this NavMeshNode
			//! \return the amount of memory occupied by this node
			inline uint32 getMemoryUsage() const
			{
				return sizeof(NavMeshNode) + 3 * sizeof(PolygonEdge);
			}

			//! \brief Sets the HighLevelGraph node ID to which this node belongs to.
			//! \param nodeID the HighLevelGraph node ID
			//! \see getHighLevelGraphNodeId
			inline void setHighLevelGraphNodeID( const HighLevelNodeID nodeID ) 
			{
				mHighlevelNodeID = nodeID;
			}

			//! \brief Gets the underlying HighLevelGraph node ID.
			//! \return the HighLevelGraph node ID
			//! \see setHighLevelGraphNodeId
			inline HighLevelNodeID getHighLevelGraphNodeID() const
			{
				return mHighlevelNodeID;
			}

		};
	
	}
}
