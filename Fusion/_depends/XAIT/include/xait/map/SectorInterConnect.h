// (C) 2011 xaitment GmbH

#pragma once 

#include <xait/common/memory/Allocator.h>
#include <xait/common/memory/MemoryManaged.h>
#include <xait/common/serialize/SerializeStorage.h>
#include <xait/map/GlobalNodeID.h>

namespace XAIT
{
	namespace Map
	{
		//! \brief This struct holds the edge information of two nodes connected by a polygon edge.
		struct SectorInterEdge
		{
			uint32		 mFromNodeID; //!< the NavMesh node which is the source of the edge
			uint32		 mFromPolygonEdge; //!< the edge
			GlobalNodeID mNeighbourID; //!< the NavMesh node which is the target of the edge

			//! \brief Constructor.
			//! \param fromNodeID the source node of the SectorInterEdge
			//! \param fromPolygonEdge the polygon edge which connects the source node with the target node
			//! \param neighbourID the target node of the SectorInterEdge
			SectorInterEdge(const uint32 fromNodeID, const uint32 fromPolygonEdge, const GlobalNodeID neighbourID)
				: mFromNodeID(fromNodeID)
				, mFromPolygonEdge(fromPolygonEdge)
				, mNeighbourID(neighbourID)
			{}

			//! \brief Default Constructor.
			SectorInterEdge()
			{}
		};

		//! \brief This class describes by which edges two sectors are connected with each other.
		class SectorInterConnect 
		{
			Common::Container::Vector<SectorInterEdge>	mEdges; //!< the connections between the sectors

			uint32					mSectorIDA; //!< the ID of the first sector
			uint32					mSectorIDB; //!< the ID of the second sector
			bool					mIsLoaded; //!< true, if this SectorInterConnect has been loaded, and false otherwise

		public:
			//! \brief Constructor.
			//! \param alloc the memory allocator
			//! \param sectorIDA the ID of the first sector
			//! \param sectorIDB the ID of the second sector
			SectorInterConnect(const Common::Memory::Allocator::Ptr& alloc, const uint32 sectorIDA, const uint32 sectorIDB)
				: mEdges(alloc)
				, mSectorIDA(sectorIDA)
				, mSectorIDB(sectorIDB)
				, mIsLoaded(false)
			{}

			//! \brief Stream constructor. Use this when you initialize from a stream.
			//! \param alloc the memory allocator
			SectorInterConnect(const Common::Memory::Allocator::Ptr& alloc)
				: mEdges(alloc)
				, mSectorIDA(0)
				, mSectorIDB(0)
				, mIsLoaded(false)
			{}

			//! \brief Gets the edges connecting the two sectors.
			//! \return a vector containing the edge connection information
			inline Common::Container::Vector<SectorInterEdge>& getEdges()
			{
				return mEdges;
			}

			//! \brief Gets the edges connecting the two sectors.
			//! \return a vector containing the edge connection information
			inline const Common::Container::Vector<SectorInterEdge>& getEdges() const
			{
				return mEdges;
			}

			//! \brief Gets the sector ID of the first sector.
			//! \return the sector ID of the first sector
			//! \see getSectorIDB
			inline uint32 getSectorIDA() const
			{
				return mSectorIDA;
			}

			//! \brief Gets the sector ID of the second sector.
			//! \return the sector ID of the second sector
			//! \see getSectorIDA
			inline uint32 getSectorIDB() const
			{
				return mSectorIDB;
			}

			//! \brief Checks whether or not this SectorInterConnect is loaded.
			//! \return true, if it is loaded, and false otherwise
			//! \see setIsLoaded
			inline bool getIsLoaded() const
			{
				return mIsLoaded;
			}

			//! \brief Sets the IsLoaded flag.
			//! \param isloaded the new value for the flag
			//! \see getIsLoaded
			inline void setIsLoaded(const bool isloaded)
			{
				mIsLoaded= isloaded;
			}

			//! \brief Gets the memory usage of this SectorInterConnect.
			//! \return the number of bytes used
			inline uint32 getMemoryUsage() const
			{
				return sizeof(SectorInterConnect) + mEdges.size() * sizeof(SectorInterEdge);
			}

			//! \brief Writes this SectorInterConnect to a storage stream.
			//! \param storage the storage stream
			void writeToStream(Common::Serialize::SerializeStorage* storage) const;

			//! \brief Initializes this SectorInterConnect from the given storage stream.
			//! \param storage the storage stream
			void initFromStream(Common::Serialize::SerializeStorage* storage);
		};

	}
}

