// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/String.h>
#include <xait/common/container/Vector.h>
#include <xait/common/io/PackedStreamDictionary.h>
#include <xait/common/memory/MemoryStream.h>
#include <xait/map/build/GeometryInfo.h>
#include <xait/map/build/TriangleClassifier.h>

namespace XAIT
{
	namespace Map
	{
		namespace Build
		{
			//Comporator for TriangleInfo
			struct TriComp
			{
				bool operator()(const Map::Build::TriangleInfo& tri1, const Map::Build::TriangleInfo& tri2) const 
				{ 
					if (tri1.mSurfaceTypeID < tri2.mSurfaceTypeID) 
						return true; 

					if (tri1.mSurfaceTypeID == tri2.mSurfaceTypeID) 
						return tri1.mWalkable < tri2.mWalkable; 

					return false; 
				}
			};

			//VRML file descriptor
			struct VRMLDescriptor
			{
				TriangleInfo		mTriangleInfo;
				Common::String		mFilePath;

				VRMLDescriptor(TriangleInfo triInfo, Common::String filePath)
					: mTriangleInfo(triInfo),
					mFilePath(filePath)
				{}

				VRMLDescriptor()
					: mTriangleInfo(),
					mFilePath("")
				{}
			};

			class DebugInfo
			{				
				struct TriangleStream
				{
					XAIT::Common::IO::Stream*	mStream;
					uint32						mTriangleCount;

					TriangleStream(XAIT::Common::IO::Stream* stream, uint32 triangleCount = 0)
						: mStream(stream)
						, mTriangleCount(triangleCount)						
					{}

					TriangleStream()
						: mStream(NULL)
						, mTriangleCount(0)
					{}
				};

			public:
				XAIT_MAP_DLLAPI DebugInfo();
				XAIT_MAP_DLLAPI ~DebugInfo();
				
				//! \brief initializes the instance with data from a stream
				//! \param stream source of configuration data
				XAIT_MAP_DLLAPI bool initFromStream(XAIT::Common::IO::Stream* stream);

				//! \brief initializes the instance with data from a stream
				//! \param stream destination stream
				XAIT_MAP_DLLAPI bool storeToStream(XAIT::Common::IO::Stream* stream);

				//! \brief used to specify build configuration data 
				//! \param config source build config
				XAIT_MAP_DLLAPI void setBuildConfig(NavMeshBuildConfig* config);

				//! \brief retrieves navmesh build config data				
				XAIT_MAP_DLLAPI NavMeshBuildConfig* getBuildConfig();

				XAIT_MAP_DLLAPI bool hasGeometry();

				//! \brief used to specify the geometry to export
				//! \param infos the set of GeometryInfos that are to be exported
				XAIT_MAP_DLLAPI void setGeometryInfos(XAIT::Common::Container::Vector<GeometryInfo>* infos);

				//! \brief Writes geometry data, if available, into a set of VRML Files in path specified.
				//! \brief Check if geometry is available using hasGeometry()
				//! \param path Destination path for VRML files
				XAIT_MAP_DLLAPI XAIT::Common::Container::Vector<VRMLDescriptor> createVRMLGeometry(XAIT::Common::String path);

				//! \brief Specify a navmesh to include with the debug information
				//! \param navmesh instance to export
				XAIT_MAP_DLLAPI void setNavMesh(NavMeshContainer* navmesh);

				//! \brief retrieve the navmesh instance
				//! \returns navmesh pointer or null if navmesh is not available
				XAIT_MAP_DLLAPI NavMeshContainer* getNavMesh();

			private:
				bool readConfig(Common::IO::Stream* stream);				
				bool readGeometryToBuffer(Common::IO::Stream* stream);
				bool readNavMeshToBuffer(Common::IO::Stream* stream);
				bool storeConfig(Common::IO::Stream* stream);
				bool storeGeometry(Common::IO::Stream* stream);
				bool storeNavmesh(Common::IO::Stream* stream);				
				XAIT::Common::Math::Vec3f readVertex(XAIT::Common::Serialize::SerializeStorage* storage); 
				void writeVertex(XAIT::Common::Serialize::SerializeStorage* storage, const XAIT::Common::Math::Vec3f& vertex);
				bool writeToMemBuffer(Common::IO::Stream* stream, Common::SharedPtr<XAIT::Common::Memory::MemoryStream> buf);
				XAIT::Common::String createVRMLFileName(uint16 surfaceTypeID, bool walkable);
				inline void writeStr(XAIT::Common::IO::Stream* stream, const char* str)
				{
					stream->write(str, XAIT::Common::StringCFuncs<char>::StrLen(str));
				}				
				
				NavMeshBuildConfig*											mBuildConfig;
				NavMeshBuildConfig*											mBuildConfigInternal;
				NavMeshContainer*											mNavMesh;			
				Common::SharedPtr<XAIT::Common::Memory::MemoryStream>		mNavMemStream;
				Common::SharedPtr<XAIT::Common::Memory::MemoryStream>		mGeomMemStream;
				XAIT::Common::Container::Vector<GeometryInfo>				mGeometry;			
			};
		}
	}
}
