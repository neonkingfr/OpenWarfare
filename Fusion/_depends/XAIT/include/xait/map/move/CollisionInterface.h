// (C) xaitment GmbH 2006-2012

 #pragma once


#include <xait/map/CompileDefines.h>
#include <xait/map/move/Frametime.h>
#include <xait/map/MapMemManagedObject.h>


namespace XAIT
{
	namespace Map
	{
		namespace Move
		{
			class CollisionShape;
			class SimulationWorld;

			/*! \brief this interface provides functions to query all dynamic collider in a certain range.
			 *	It is used by MoveEntities to find possible dynamic obstacle.
			 *	Obstacles that are not managed by the simulation world have to be update by the application.
			 */
			class CollisionInterface : public MapMemManagedObject
			{
				friend class SimulationWorld;
			public:
				//! \brief the informations that can be queried to determine collision between registered entities
				struct CollisionQueryEntry
				{
					//! \brief constructor
					CollisionQueryEntry(const Common::Math::Vec3f& position,
										 const Common::Math::Vec3f& velocity, 
										 float32 collisionDistance, 
										 uint32 colliderID)
						: mPosition(position)
						, mVelocity(velocity)
						, mCollisionDistance(collisionDistance)
						, mColliderID(colliderID)
					{}

					Common::Math::Vec3f	mPosition;
					Common::Math::Vec3f	mVelocity;

					float32	mCollisionDistance;		//! distance to the collider (to the hull of the collider)
					uint32	mColliderID;			//! unique id of the collider
				};

				//! \brief register an obstacle with a certain collision shape
				//! \param collisionShape		the collision shape of the the obstacle
				//! \returns an unique collider id
				//! \remark MoveEntities that are registered in the simulation world are registered automatically
				XAIT_MAP_DLLAPI virtual uint32 registerObstacle(CollisionShape* collisionShape) = 0;

				//! \brief deregister an obstacle 
				//! \param colliderID		the colliderID of the obstacle
				//! \return true if the obstacle is deregistered, false if collider is unknown
				//! \remark MoveEntities that are registered in the simulation world are deregistered automatically
				XAIT_MAP_DLLAPI virtual bool deregisterObstacle(uint32 colliderID) = 0;

				//! \brief update the data of an obstacle
				//! \param colliderID	the colliderID of the obstacle
				//! \param position		the new position
				//! \param velocity		the new velocity
				XAIT_MAP_DLLAPI virtual void updateObstacle(uint32 colliderID,const Common::Math::Vec3f& position, const Common::Math::Vec3f& velocity) = 0;

				//! \brief query all registered obstacles in a certain range
				//! \param obstacles		the obstacles in the given range (return value)
				//! \param position			the position of the query
				//! \param radius			the radius of the query
				//! \param ignoreID			a ColliderID that is ignored (e.g. colliderID of the own entity)
				XAIT_MAP_DLLAPI virtual void getDynamicObstaclesInRange(Common::Container::Vector<CollisionQueryEntry>& obstacles, const Common::Math::Vec3f& position, float32 radius, uint32 ignoreID = X_MAX_UINT32) = 0;

				//! \brief retrieve the biggest radius of all registered obstacles
				//! \returns the biggest radius of all registered obstacles
				XAIT_MAP_DLLAPI virtual float getBiggestObstacleRadius() = 0;

			protected:
				//! \brief updatecall from the simulation world at the start of a frame
				//! \param frametime	the current gametime
				virtual void update(const Frametime& frametime) = 0;
			};
		}
	}
}
