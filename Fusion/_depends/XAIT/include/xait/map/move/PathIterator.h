// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/map/GlobalNodeID.h>


namespace XAIT
{
	namespace Map
	{
		namespace Path
		{
			class PathResult;
		}

		namespace Move
		{
			class PathManager;

			//! \brief iterator that abstract the multiple paths 
			class PathIterator
			{
				friend class PathManager;
			public:
				//! \brief default constructor
				PathIterator()
					: mPathManager(NULL)
					, mCurrentPath(NULL)
					, mCurrentPos(X_MAX_UINT32)
					, mTempPath(false)
				{}

				//! \brief copy constructor
				PathIterator(const PathIterator& other)
					: mPathManager(other.mPathManager)
					, mCurrentPath(other.mCurrentPath)
					, mCurrentPos(other.mCurrentPos)
					, mTempPath(other.mTempPath)
				{}

				//! \brief assignment operator
				const PathIterator& operator=(const PathIterator& other)
				{
					mPathManager = other.mPathManager;
					mCurrentPath = other.mCurrentPath;
					mCurrentPos = other.mCurrentPos;
					mTempPath = other.mTempPath;

					return *this;
				}

				//! \brief get the start position of the current path
				//! \returns the start position of the current path
				XAIT_MAP_DLLAPI const Common::Math::Vec3f& getStartPos() const;

				//! \brief get the start node of the current path
				//! \returns the start node of the current path
				XAIT_MAP_DLLAPI const GlobalNodeID& getStartNodeID() const;

				//! \brief get the end position of the current path
				//! \returns the end position of the current path
				XAIT_MAP_DLLAPI const Common::Math::Vec3f& getEndPos() const;

				//! \brief get the end node of the current path
				//! \returns the end node of the current path
				XAIT_MAP_DLLAPI const GlobalNodeID& getEndNodeID() const;

				//! \brief get the id of the crossed edge
				//! \returns the id of the crossed edge
				XAIT_MAP_DLLAPI const uint32 getCrossedEdgeID() const;

				//! \brief Increment a valid iterator to the next path segment
				//! \returns the iterator itself
				XAIT_MAP_DLLAPI PathIterator& operator++();

				//! \brief Decrement a valid iterator to the next path segment
				//! \returns the iterator itself
				XAIT_MAP_DLLAPI PathIterator& operator--();

				//! \brief checks if the iterator is valid
				XAIT_MAP_DLLAPI bool isValid() const;

				//! \brief checks if the iterator is on the last segment
				//! \returns true if the iterator is on the last segment, false otherwise
				inline bool isLastSegment() const
				{
					if (!isValid())
						return false;

					PathIterator tmp = *this;
					++tmp;

					return !tmp.isValid();
				}

				//! \brief checks if the iterator reached the next path
				//! \returns true if the iterator is on the next path, false otherwise
				XAIT_MAP_DLLAPI bool nextPathReached() const;

				//! \brief get the current path segment id
				//! \returns the current segment id
				inline uint32 getCurrentSegmentID() const
				{
					return mCurrentPos;
				}

				//! \brief checks if the iterator is on a temporary path
				//! \returns true if the iterator is on a temporary path, false otherwise
				inline bool isOnTempPath() const
				{
					return mTempPath;
				}

				//! \brief sets the current path
				//! \param path			the new current path
				//! \param onTempPath	flag, if we are currently on a temporary path
				void setCurrentPath(Path::PathResult* path, bool onTempPath);

				Path::PathResult* getCurrentPath() const;

			protected:
				//! \brief constructor
				XAIT_MAP_DLLAPI PathIterator(PathManager* pathManager,Path::PathResult* path, uint32 segmentID, bool tempPath);

				PathManager*		mPathManager;
				Path::PathResult*	mCurrentPath;
				uint32				mCurrentPos;
				bool				mTempPath;
			};

		}
	}
}
