// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/map/move/CollisionShape.h>

#include <xait/common/math/Vec3.h>

namespace XAIT
{
	namespace Map
	{
		namespace Move
		{
			//! \brief implements the CollisionShape interface for circles
			class CircleCollisionShape : public CollisionShape
			{
			public:
				static const uint32 mShapeID = 123;

				//! \brief constructor
				//! \param position		the center of the circle
				//! \param velocity		the velocity of the object
				//! \param radius		the radius of the circle
				CircleCollisionShape(const Common::Math::Vec3f& position, const Common::Math::Vec3f& velocity, float32 radius)
					: mPosition(position)
					, mVelocity(velocity)
					, mRadius(radius)
				{}

				//! \brief destructor
				virtual ~CircleCollisionShape()
				{}

				//! \brief get the shape id
				//! \returns the shape id
				virtual uint32 getShapeID() const
				{
					return mShapeID;
				}

				//! \brief get the position/center of the shape
				//! \returns the position/center of the shape
				inline const Common::Math::Vec3f& getPosition() const
				{
					return mPosition;
				}

				//! \brief get the velocity of the shape
				//! \returns the velocity of the shape
				inline const Common::Math::Vec3f& getVelocity() const
				{
					return mVelocity;
				}

				//! \brief get the radius of the shape
				//! \returns the radius of the shape
				inline float32 getRadius() const
				{
					return mRadius;
				}



			protected:
				Common::Math::Vec3f mPosition;
				Common::Math::Vec3f mVelocity;
				float32				mRadius;
			};
		}
	}
}

