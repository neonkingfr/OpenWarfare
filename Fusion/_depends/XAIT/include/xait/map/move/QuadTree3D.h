// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/math/Vec2.h>
#include <xait/common/math/Vec3.h>
#include <xait/common/geom/AABB2D.h>

#include <xait/common/container/IDVector.h>

namespace XAIT
{
	namespace Map
	{
		namespace Move
		{
			//! \brief the class provides efficient range searches in a 3d space
			class QuadTree3D : public MapMemManagedObject
			{
				struct DataEntry
				{
					DataEntry(const Common::Math::Vec3f& position, uint32 storedID)
						: mPosition(position)
						, mStoredID(storedID)
						, mNodeID(X_MAX_UINT32)
						, mNextDataID(X_MAX_UINT32)
					{}

					Common::Math::Vec3f mPosition;
					uint32				mStoredID;
					uint32				mNodeID;
					uint32				mNextDataID;
				};

				//! \brief	a node of the quadtree
				struct QuadNode : public MapMemManagedObject
				{
					QuadNode()
						: mParent(X_MAX_UINT32)
						, mFirstDataEntry(X_MAX_UINT32)
						, mDepth(X_MAX_UINT32)
					{
						for (uint32 i=0; i<4; ++i)
							mChilds[i] = X_MAX_UINT32;
					}

					QuadNode(uint32 parent, uint32 depth)
						: mParent(parent)
						, mFirstDataEntry(X_MAX_UINT32)
						, mDepth(depth)
					{
						for (uint32 i=0; i<4; ++i)
							mChilds[i] = X_MAX_UINT32;
					}

					QuadNode(const QuadNode& other)
						: mBoundingBox(other.mBoundingBox)
						, mParent(other.mParent)
						, mFirstDataEntry(other.mFirstDataEntry)
						, mDepth(other.mDepth)
					{
						for (uint32 i=0; i<4; ++i)
							mChilds[i] = other.mChilds[i];
					}

					inline uint32 getChild(uint32 index) const
					{
						return mChilds[index];
					}

					inline bool isLeaf() const
					{
						bool result = true;
						for (uint32 i=0; i<4; ++i)
							result &= (mChilds[i] == X_MAX_UINT32);
						return result;
					}

					inline bool hasData() const
					{
						return mFirstDataEntry != X_MAX_UINT32;
					}

					inline bool isInside(const Common::Math::Vec2f& point)
					{
						return mBoundingBox.isPointInside(point);
					}

					inline void increaseDepth(QuadTree3D* owner)
					{
						mDepth++;

						if (!isLeaf())
						{
							for (uint32 i=0; i<4; ++i)
							{
								if (mChilds[i] != X_MAX_UINT32)
									owner->mNodes[mChilds[i]].increaseDepth(owner);
							}
						}
					}

					inline void decreaseDepth(QuadTree3D* owner)
					{
						mDepth--;

						if (!isLeaf())
						{
							for (uint32 i=0; i<4; ++i)
							{
								if (mChilds[i] != X_MAX_UINT32)
									owner->mNodes[mChilds[i]].decreaseDepth(owner);
							}
						}
					}

					Common::Geom::AABB2Df mBoundingBox;

					uint32		mChilds[4]; //!< id of the upper left child
					uint32		mParent;
					uint32		mFirstDataEntry;
					uint32		mDepth;

				};

			public:

				//! \brief constructor
				QuadTree3D() ;

				//! \brief destructor
				~QuadTree3D();

				//! \brief initalizes the quadtree
				//! \param maxEntriesPerQuad	the max number of entries in cell before the cell is splitted
				//! \param minCellSize			the minimal size of an cell 
				//! \param upComp				the up component of the world space
				void init(uint32 maxEntriesPerQuad, float32 minCellSize, uint32 upComp);

				//! \brief adds a point to the quadtree
				//! \param point	the added point
				//! \param data		the stored data
				//! \returns the id of the entry
				uint32 addPoint(const Common::Math::Vec3f& point,const uint32 data);

				//! \brief removes an entry from the quadtree
				//! \param id		the id fo the entry
				void remove(uint32 id);

				//! \brief cleans the quadtree
				//! \remark the current implementation tries to throw away unnecessary node on the top of the tree
				void clean();

				

				//! \brief moves an entry to a new position
				//! \param id			the id of the entry
				//! \param newPosition	 the new position of the entry
				void movePoint(uint32 id, const Common::Math::Vec3f& newPosition);

				//! \brief searchs all entries in a certain range
				//! \param entries		the return value
				//! \param center		the center of the search
				//! \param radius		the radius of the search
				//! \param ignoreEntry	an id that is ignored
				//! \remark the current implementation uses a cylinder (height=radius) instead of a sphere
				void doRangeSearch( Common::Container::Vector<uint32>& entries, const Common::Math::Vec3f& center,const float32 radius, uint32 ignoreEntry) const;

				//! \brief gets the position of an entry
				//! \param id	the entry id
				//! \returns the position of the entry
				inline const Common::Math::Vec3f& getPosition(uint32 id) const
				{
					return mData[id].mPosition;
				}

				//! \brief generates debug output for internal use
				void draw() const;


			private:


				uint32										mMaxEntries;	
				uint32										mRoot;			
				Common::Container::IDVector<QuadNode>		mNodes;			
				Common::Container::IDVector<DataEntry>		mData;
				uint08										mUpComponent;
				float32										mMinCellSize;

		

				void doRangeSearch(uint32 nodeID, Common::Container::Vector<uint32>& entries, const Common::Math::Vec2f& center2D,float32 upValue, float32 sqRadius, uint32 ignoreEntry) const ;
				void extendQuadtree(const Common::Math::Vec2f& point);
				void addDataToNode(uint32 dataID, uint32 nodeID);
				void removeDataFromNode(uint32 id);
				void splitNode(uint32 nodeID);
				void setChildBoundingBox(QuadNode& node, uint32 pos,const QuadNode& parent) const;
				void cleanUpNode(uint32 nodeID);
				uint32 getLeaf(const Common::Math::Vec2f& point);

			};
		}
	}
}


