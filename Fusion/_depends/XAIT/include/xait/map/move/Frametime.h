// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>

namespace XAIT
{
	namespace Map
	{
		namespace Move
		{
			//! \brief this class encapsulates the current and the most recent game time 
			struct Frametime
			{
				uint32	mFrametime;
				uint32	mLastFrametime; 

				//! \brief default constructor
				Frametime()
					: mFrametime(0)
					, mLastFrametime(X_MAX_UINT32)
				{}

				//! \brief constructor
				Frametime(uint32 currentFrametime)
					: mFrametime(currentFrametime)
					, mLastFrametime(X_MAX_UINT32)
				{}

				//! \brief get the current time in milliseconds
				//! \returns the time in milliseconds
				inline uint32 getMilliseconds() const
				{
					return mFrametime;
				}

				//! \brief get the current time in seconds
				inline float32 getSeconds() const
				{
					return ((float32)mFrametime) * 0.001f;
				}

				//! \brief get the last frame time in milliseconds
				//! \returns the last frame time in milliseconds
				inline uint32 getLastFrameMilliseconds() const
				{
					return mLastFrametime;
				}

				//! \brief get the last frame time in seconds
				//! \returns the last frame time in seconds
				inline float32 getLastFrameSeconds() const
				{
					return ((float32)mLastFrametime) * 0.001f;
				}

				//! \brief get the time delta between the current time and the last frametime in milliseconds
				//! \returns the time delta in milliseconds
				inline uint32 getTimeDeltaMilliseconds() const
				{
					return mFrametime - mLastFrametime;
				}

				//! \brief get the time delta between the current time and the last frametime in seconds
				//! \returns the time delta in seconds
				inline float32 getTimeDeltaSeconds() const
				{
					return ((float32)mFrametime - mLastFrametime) * 0.001f;
				}
			};
		}
	}
}
