// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/container/Vector.h>
#include <xait/common/container/Pair.h>
#include <xait/common/math/Vec3.h>
#include <xait/common/math/Matrix4.h>
#include <xait/common/crypto/MD5Sum.h>
#include <xait/common/serialize/SerializeStorage.h>
#include <xait/common/geom/AABB3D.h>
#include <xait/common/platform/Endian.h>
#include <xait/map/CompileDefines.h>
#include <xait/map/NavMeshNode.h>
#include <xait/map/NavMeshSectorConfig.h>
#include <xait/map/CustomDataDesc.h>
#include <xait/map/MapMemManagedObject.h>

#define XAIT_MAP_SECTOR_FILEVERSION	33

namespace XAIT
{
	namespace Map
	{
		class SectorInterConnect;
		class NavMeshContainer;
		class NavMeshGrid;
		class PathObject;
		class PathObjectSectorData;
		class EdgeConnectDataSet;


		//! \brief The class that manages a single NavMesh sector.
		class NavMeshSector : public NonVirtualMapMemManagedObject
		{
		public:
			//! \brief An invalid unique ID.
			static const Common::Crypto::MD5Sum INVALID_UNIQUEID;

			//! \brief Constructor.
			//! \param parent the NavMeshContainer to which this sector belongs to
			//! \param sectorInfo the sector configuration
			//! \param boundingBox the bounding box of the sector
			//! \param checkSumA
			NavMeshSector(NavMeshContainer* parent, const NavMeshSectorConfig& sectorInfo, const Common::Geom::AABB3Df& boundingBox, const Common::Crypto::MD5Sum& checkSumA);

			//! \brief Constructor.
			//! \param parent the NavMeshContainer to which this sector belongs to
			NavMeshSector(NavMeshContainer* parent);

			//! \brief Destructor.
			~NavMeshSector();

			//! \brief Gets the sector ID of this sector.
			//! \return the sector ID
			inline uint32 getID() const
			{
				return mConfig.mSectorID;
			}

			//! \brief Returns the unique ID of this sector.
			//! \return the MD5 checksum
			inline const Common::Crypto::MD5Sum& getUniqueID() const
			{
				return mUniqueID;
			}

			//! \brief Gets the sector configuration.
			//!
			//! Contains (among others) the size of the sector.
			//! \return the sector configuration
			inline const NavMeshSectorConfig& getConfig() const
			{
				return mConfig;
			}

			//! \brief Gets the corresponding NavMeshContainer.
			//! \return a pointer to the NavMeshContainer
			inline const NavMeshContainer* getContainer() const
			{
				return mParent;
			}

			//! \brief Gets the corresponding NavMeshContainer.
			//! \return a pointer to the NavMeshContainer
			inline NavMeshContainer* getContainer()
			{
				return mParent;
			}

			//! \brief Gets the bounding box of the NavMesh sector including the height of the polygons.
			inline const Common::Geom::AABB3Df& getBoundingBox() const
			{
				return mBoundingBox;
			}

			//! \brief Gets the bounding box of a NavMesh node.
			//! \param nodeID the NavMesh node for which the bounding box is computed
			//! \return the bounding box
			XAIT_MAP_DLLAPI Common::Geom::AABB3Df getNodeBoundingBox(const uint32 nodeID) const;

			//! \brief Gets the midpoint of a NavMesh node.
			//! \param nodeID		the index of the NavMesh node
			//! \returns the midpoint
			//! \see getEdgeMiddlePoint
			XAIT_MAP_DLLAPI Common::Math::Vec3f XAIT_CDECL getNodeMiddlePoint(const uint32 nodeID) const;

			//! \brief Checks whether a point is inside a certain node.
			//! \param point		the point which should be tested
			//! \param nodeID		the node which should be tested
			//! \return true if the point is inside the volume of the specified node, and false otherwise
			XAIT_MAP_DLLAPI bool XAIT_CDECL isPointInsideNode(const Common::Math::Vec3f& point, const uint32 nodeID) const;

			//! \brief Gets the start and the end point of a polygon edge.
			//! \param startPos			returned start position of the edge
			//! \param endPos			returned end position of the edge
			//! \param nodeID			index of the NavMesh containing the edge
			//! \param edgeID			local edge index
			XAIT_MAP_DLLAPI void getEdgeEndpoints(Common::Math::Vec3f& startPos, Common::Math::Vec3f& endPos, const uint32 nodeID, uint32 edgeID) const;

			//! \brief Checks whether a ray intersects a NavMesh node.
			//! \param ray			the ray to test
			//! \param nodeID		the ID of the node which should be tested
			//! \return true if the polygon (not the extended volume) intersects the ray, and false otherwise
			XAIT_MAP_DLLAPI bool intersectNode(Common::Geom::Ray3D<uint32>& ray, const uint32 nodeID) const;

			//! \brief Checks whether a ray intersects one of the navmesh nodes inside the sector
			//! \param ray			the ray to test
			//! \return true if a polygon has been hit
			XAIT_MAP_DLLAPI bool intersectSector(Common::Geom::Ray3D<uint32>& ray);

			//! \brief Calculate the minimum distance from a given point to the polygon edges of a certain NavMesh node.
			//! \param point			the point to test
			//! \param nodeID			the edges of the polygon of this node will be checked
			//! \return					the distance to the nearest edge
			XAIT_MAP_DLLAPI float32 XAIT_CDECL getDistanceToNearestEdgeOfNode(const Common::Math::Vec3f& point, const uint32 nodeID) const;

			//! \brief Gets a NavMesh node to a specified point.
			//!
			//! The point must be inside the navigation mesh. If it is outside of the navigation mesh, this method
			//! will not search for the nearest alternative. You must have initialized the node search system.
			//! \param point					position to search for
			//! \param applyHeightConstraints	if true, the functions checks if the point is inside (bounding box) the node
			//! \returns		the id of node, if a node was found, or INVALID_NODEID if no node was found
			XAIT_MAP_DLLAPI uint32 XAIT_CDECL searchNode(const Common::Math::Vec3f& point, bool applyHeightConstraints) const;

			//! \brief Gets the nearest position on the navmesh to the given position (using euclidean distance)
			//! \param distance			the nearest distance
			//! \param nearestNode		the nodeID of the nearest found node (invalid if no node in given radius found)
			//! \param point			the given position
			//! \param radius			the search radius around the given position
			//! \returns the nearest point on the navigation mesh. Vec3f(X_MAX_FLOAT32,X_MAX_FLOAT32,X_MAX_FLOAT32) if no position was found
			XAIT_MAP_DLLAPI Common::Math::Vec3f searchNearestPoint(float32& distance, uint32& nearestNode, const Common::Math::Vec3f& point, float32 radius) const;

			//! \brief Gets the up direction of the sector.
			//! 
			//! Currently the up direction is the same for entire NavMesh.
			//! \return the up direction
			XAIT_MAP_DLLAPI Common::Math::Vec3f getUpDirection();

			//! \brief Gets a vector that is perpendicular to the up-direction. Can be used along with getYAxis to form a coordinate system.
			//! \return the x-axis vector
			//! \see getYAxis
			XAIT_MAP_DLLAPI const Common::Math::Vec3f& getXAxis() const;

			//! \brief Gets a vector that is perpendicular to the up-direction. Can be used along with getXAxis to form a coordinate system.
			//! \return the y-axis vector
			//! \see getXAxis
			XAIT_MAP_DLLAPI const Common::Math::Vec3f& getYAxis() const;

			//! \brief Returns a particular node from this sector.
			//! \param nodeID	a valid NavMesh node ID
			//! \return the corresponding NavMesh node
			inline const NavMeshNode& getNode(const uint32 nodeID) const
			{
				X_ASSERT_MSG_DBG2(nodeID < mNumNodes,"nodeID out of range",nodeID,mNumNodes);
				return mNodes[nodeID];
			}

			//! \brief Returns a particular node from this sector.
			//! \param nodeID	a valid NavMesh node ID
			//! \return the corresponding NavMesh node
			inline NavMeshNode& getNode(const uint32 nodeID)
			{
				X_ASSERT_MSG_DBG2(nodeID < mNumNodes,"nodeID out of range",nodeID,mNumNodes);
				return mNodes[nodeID];
			}

			//! \brief Computes the normal of the plane defined by a node
			//! \param node		node from this sector to compute normal for
			inline Common::Math::Vec3f getNodeNormal(const NavMeshNode& node) const
			{
				const Common::Math::Vec3f ab= mVertices[node.getPolygonEdge(1).mVertexID] - mVertices[node.getPolygonEdge(0).mVertexID];
				const Common::Math::Vec3f ac= mVertices[node.getPolygonEdge(2).mVertexID] - mVertices[node.getPolygonEdge(0).mVertexID];

				Common::Math::Vec3f normal= ab.crossProduct(ac);
				normal.normalize();
				return normal;
			}

			//! \brief Computes the normal of the plane defined by a node
			//! \param nodeID	nodeID from this sector to compute normal for
			inline Common::Math::Vec3f getNodeNormal(const uint32 nodeID) const
			{
				return getNodeNormal(getNode(nodeID));
			}

			//! \brief Gets an array containing all nodes of the sector.
			//! \return the NavMesh node array
			inline const NavMeshNode* getNodes() const
			{
				return mNodes;
			}

			//! \brief Gets an array containing all nodes of the sector.
			//! \return the NavMesh node array
			inline NavMeshNode* getNodes()
			{
				return mNodes;
			}

			//! \brief Gets the number of NavMesh nodes in this sector.
			//! \return the number of nodes
			inline uint32 getNumNodes() const
			{
				return mNumNodes;
			}

			//! \brief Gets a NavMesh vertex.
			//! \param vertexID		id of the vertex
			//! \return the NavMesh vertex
			//! \see getNumVertices
			//! \see getVertices
			inline const Common::Math::Vec3f& getVertex(const uint32 vertexID) const
			{
				X_ASSERT_MSG_DBG2(vertexID < mNumVertices,"vertexID out of range",vertexID,mNumVertices);
				return mVertices[vertexID];
			}

			//! \brief Gets a NavMesh vertex.
			//! \param vertexID		id of the vertex
			//! \return the NavMesh vertex
			//! \see getNumVertices
			//! \see getVertices
			inline Common::Math::Vec3f& getVertex(const uint32 vertexID)
			{
				X_ASSERT_MSG_DBG2(vertexID < mNumVertices,"vertexID out of range",vertexID,mNumVertices);
				return mVertices[vertexID];
			}

			//! \brief Gets an array of all vertices.
			//! \return an array containing all vertices
			//! \see getVertex
			inline const Common::Math::Vec3f* getVertices() const
			{
				return mVertices;
			}

			//! \brief Gets an array of all vertices.
			//! \return an array containing all vertices
			//! \see getVertex
			inline Common::Math::Vec3f* getVertices()
			{
				return mVertices;
			}

			//! \brief Gets the number of vertices.
			//! \return the number of vertices
			inline uint32 getNumVertices() const
			{
				return mNumVertices;
			}

			//! \brief Gets the pointer to the complete custom data array
			inline void* getCustomDataPtr() { return mCustomData; }

			//! \brief Gets the pointer to the complete custom data array
			inline const void* getCustomDataPtr() const { return mCustomData; }

			//! \brief Gets the pointer to the custom data of a specific node
			inline void* getCustomDataPtr(const uint32 nodeID) { return (uint08*)mCustomData + (nodeID * mCustomDataItemSize); }

			//! \brief Gets the pointer to the custom data of a specific node
			inline const void* getCustomDataPtr(const uint32 nodeID) const { return (uint08*)mCustomData + (nodeID * mCustomDataItemSize); }

			//! \brief Get a single member from custom data
			template<typename T>
			inline T& getCustomDataMember(const uint32 nodeID, const uint32 memberID)
			{
				uint08* nodePtr= (uint08*)getCustomDataPtr(nodeID);
				return *((T*)(nodePtr + getCustomDataDesc().getDataMember(memberID).mOffset));
			}

			//! \brief Get a single member from custom data
			template<typename T>
			inline const T& getCustomDataMember(const uint32 nodeID, const uint32 memberID) const
			{
				uint08* nodePtr= (uint08*)getCustomDataPtr(nodeID);
				return *((T*)(nodePtr + getCustomDataDesc().getDataMember(memberID).mOffset));
			}

			//! \brief Gets the custom data description
			XAIT_MAP_DLLAPI const CustomDataDesc& getCustomDataDesc() const;

			//! \brief Gets the build checksum.
			//! \return the checksum
			inline const Common::Crypto::MD5Sum& getCheckSumA() const
			{
				return mCheckSumA;
			}

			//! \brief Invalidates the build checksum.
			inline void invalidateCheckSumA()
			{
				mCheckSumA= Common::Crypto::MD5Sum();
			}

			//! \brief Gets the memory usage of the sector.
			//! \return the number of bytes occupied by the sector
			XAIT_MAP_DLLAPI uint32 getMemoryUsage() const;

			//! \brief Returns the memory usage of the given node.
			//! \param nodeID the ID of the node
			//! \return the number of bytes occupied by the node
			inline uint32 getMemoryUsage( const uint32 nodeID ) const
			{
				X_ASSERT_MSG2(nodeID < mNumNodes,"nodeID out of range",nodeID,mNumNodes);
				return mNodes[nodeID].getMemoryUsage();
			}

			bool initFromStream(Common::IO::Stream* inStream);
			void writeToStream(Common::IO::Stream* outStream, const Common::Platform::Endian::Endiness endiness) const;

			void resizeNodes(const uint32 newNumNodes);
			void resizeVertices(const uint32 newNumVertices);

			void resizePathObjectSectorData(const uint32 numPathObjectSectorData);
			inline PathObjectSectorData** getPathObjectSectorData() const { return mPathObjectData; }
			inline uint32 getNumPathObjectSectorData() const { return mNumPathObjectData; }

			void gatherEdgeConnectDataSets(Common::Container::Vector<EdgeConnectDataSet*>& edgeConnectDataSets) const;

			void remapNodeIds(const uint32* nodeIDMap);

			void deleteInterConnects();
			void createInterConnect(const uint32 dstSectorID);

			void enableEditing();
			void finishEditing();

		private:
			friend class NavMeshContainer;

			NavMeshContainer*		mParent;				
			NavMeshSectorConfig		mConfig;				
			Common::Geom::AABB3Df	mBoundingBox;			

			Common::Math::Vec3f*	mVertices;				
			uint32					mNumVertices;			

			NavMeshNode*			mNodes;					
			uint32					mNumNodes;				
			void*					mCustomData;			
			uint32					mCustomDataItemSize;

			PathObjectSectorData**	mPathObjectData;
			uint32					mNumPathObjectData;

			NavMeshGrid*			mGrid;

			StaticContainer<SectorInterConnect*>::Vector	mInterConnects; 

			Common::Crypto::MD5Sum		mUniqueID;	
			Common::Crypto::MD5Sum		mCheckSumA;

			void setParent(NavMeshContainer* parent);

			bool initFromStorage(Common::Serialize::SerializeStorage* inStream);

			void writeToStorage(Common::Serialize::SerializeStorage* outStream) const;

			void loadInterConnect(const uint32 dstSectorID);
			void unloadInterConnect(const uint32 dstSectorID);

			void deleteInterConnect(const uint32 dstSectorID);
			SectorInterConnect* findInterConnect(const uint32 dstSectorID) const;
			int32 findInterConnectID(const uint32 dstSectorID) const;
			void loadInterConnect(SectorInterConnect* interConn);
			void unloadInterConnect(SectorInterConnect* interConn);

			bool isPointInsidePolygon(const NavMeshNode& node, const Common::Math::Vec3f& pos) const;

			void remapCustomData(const uint32* nodeIDMap);
			void writeCustomDataToStream(Common::Serialize::SerializeStorage* outStream) const;
			bool initCustomDataFromStream(Common::Serialize::SerializeStorage* inStream);

			void destroyPathObjectSectorData();
			void refreshPathObjectSectorData();
			void remapPathObjectSectorData(const uint32* nodeIDMap);
			void integratePathObjectSectorData();

			bool bindDataParent(const uint16 parentTypeID ,const Common::String& parentName, PathObjectSectorData* data);
			void bindDataParent(PathObject* parent, PathObjectSectorData* data);
			void unbindDataParent(PathObjectSectorData* data);
			void rebindDataParent(PathObjectSectorData* data);

			void updateUniqueID();
			void updateGrid();
		};
	}
}

