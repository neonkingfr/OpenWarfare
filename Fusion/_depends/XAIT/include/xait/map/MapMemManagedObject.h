// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/map/MapMemoryPool.h>
#include <xait/common/memory/StaticMemoryManaged.h>
#include <xait/common/memory/StaticSTLAllocator.h>
#include <xait/common/container/Vector.h>

namespace XAIT
{
	namespace Map
	{
		class MapMemManagedObject : public Common::Memory::StaticMemoryManaged<MapMemoryPool::getLowAllocThreadSafeAllocator>
		{
			
		};

		class NonVirtualMapMemManagedObject : public Common::Memory::StaticMemoryManaged<MapMemoryPool::getLowAllocThreadSafeAllocator,Common::Memory::NonVirtualBaseClass>
		{

		};

		template<typename T_ELEMENT>
		class StaticContainer
		{
		public:
			typedef Common::Container::Vector<T_ELEMENT,Common::Memory::StaticSTLAllocator<T_ELEMENT,MapMemoryPool::getLowAllocThreadSafeAllocator> >	Vector;
		};

	}
}