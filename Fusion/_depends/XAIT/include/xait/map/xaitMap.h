// (C) xaitment GmbH 2006-2012

#pragma once 
// Main include file

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/Interface.h>
#include <xait/common/exceptions/Exception.h>

#include <xait/map/Interface.h>
#include <xait/map/NavMeshContainer.h>
#include <xait/map/NavMeshSerializer.h>

#include <xait/map/BlockingBoxPathObject.h>
#include <xait/map/BlockingPlanePathObject.h>
#include <xait/map/BlockedAreaPathObject.h>
#include <xait/map/SpawnpointPathObject.h>
#include <xait/map/PathObjectGroup.h>

#include <xait/map/build/TriangleClassifier.h>
#include <xait/map/build/NavMeshBuilder.h>
#include <xait/map/build/NavMeshOptimizer.h>

#include <xait/map/path/MidpointPathSearch.h>
#include <xait/map/path/PolyEdgePathSearch.h>
#include <xait/map/path/SubdivisionPathSearch.h>
#include <xait/map/path/NavMeshRayTrace.h>

#include <xait/map/move/SimulationWorld.h>
#include <xait/map/move/PathFollowMoveEntity.h>
#include <xait/map/move/CircleCollisionShape.h>


