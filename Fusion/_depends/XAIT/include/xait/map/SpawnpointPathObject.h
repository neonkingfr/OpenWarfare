// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/map/BuildOnlyPathObject.h>
#include <xait/common/math/Vec3.h>

namespace XAIT
{
	namespace Map
	{
		//! A path object that marks a position in the geometry where characters can spawn into the world.
		//! If one or more spawn points are attached to a navmesh build, the final navmesh will be only in
		//! that places which can be reached by those spawn points. The spawn point is a build only path object
		//! and has no functionality during runtime. Nevertheless it can be stored in the navmesh. (\see NavMeshBuild::setStoreBuildOnlyPathObjects)
		//! \brief A path object that marks a position in the geometry where characters can spawn into the world
		class SpawnpointPathObject : public BuildOnlyPathObject
		{
		public:
			//! \brief Constructs a spawn point path object
			//! \param name		Unique name
			//! \remark The name must be unique in the owning navmesh.
			XAIT_MAP_DLLAPI SpawnpointPathObject(const Common::String& name);

			//! \brief Gets the world position of this spawn point
			inline const Common::Math::Vec3f& getPosition() const { return mPosition; }

			//! \brief Sets the world position of this spawn point
			inline void setPosition(const Common::Math::Vec3f& pos) { mPosition= pos; }

			virtual PathObject* clone() const { return new SpawnpointPathObject(*this); }
			virtual uint32 getMemoryUsage() const { return sizeof(SpawnpointPathObject); }
		protected:
			XAIT_MAP_DLLAPI virtual bool initFromStream(Common::Serialize::SerializeStorage* serializer);
			XAIT_MAP_DLLAPI virtual void writeToStream(Common::Serialize::SerializeStorage* serializer) const;

		private:
			Common::Math::Vec3f		mPosition;
		};
	}
}
