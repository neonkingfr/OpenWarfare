// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/debug/Log.h>
#include <xait/common/Library.h>

#define X_COMMON_LOG_REPORTER	XAIT::Common::Library::getLogReporter()
#define X_COMMON_MSG_DBG(msg)	XAIT_MESSAGE_DBG(X_COMMON_LOG_REPORTER,msg)
#define X_COMMON_ERR_DBG(msg)	XAIT_ERROR_DBG(X_COMMON_LOG_REPORTER,msg)
#define X_COMMON_MESSAGE(msg)	XAIT_MESSAGE(X_COMMON_LOG_REPORTER,msg)
#define X_COMMON_WARNING(msg)	XAIT_WARNING(X_COMMON_LOG_REPORTER,msg)
#define X_COMMON_ERROR(msg)		XAIT_ERROR(X_COMMON_LOG_REPORTER,msg)

#define XLOG_MSG(msg)			X_COMMON_MESSAGE(msg)
#define XLOG_ERR(msg)			X_COMMON_ERROR(msg)
#define XLOG_MSG_DBG(msg)		X_COMMON_MSG_DBG(msg)
#define XLOG_ERR_DBG(msg)		X_COMMON_ERR_DBG(msg)
