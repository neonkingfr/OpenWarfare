// (C) 2011 xaitment GmbH

#pragma once 

#include <xait/common/memory/MemoryManaged.h>
#include <xait/common/serialize/serializer/FundamentalSerializer.h>
#include <xait/common/serialize/serializer/SerializerHelper.h>
#include <xait/common/serialize/SerializeStorage.h>
#include <xait/common/serialize/PatchInterface.h>
#include <xait/common/serialize/VersionMismatchException.h>
#include <xait/common/serialize/InstanceNameMismatchException.h>
#include <xait/common/serialize/TypeMismatchException.h>

#define XAIT_SERIALIZE_STREAM_CHECK_INSTNAME(expectedName,foundName)\
	if (expectedName != foundName) \
	{ throw new InstanceNameMismatchException("Wrong instance name. Expected: '" + expectedName + "' Found: '" + foundName + "'",foundName,expectedName);}\

#define XAIT_SERIALIZE_STREAM_CHECK_TYPENAME(expectedType,foundType)\
	if (expectedType != foundType) \
	{ throw new TypeMismatchException("Wrong type. Expected: '" + expectedType + "' Found: '" + foundType + "'",foundType,expectedType); }

namespace XAIT
{
	namespace Common
	{
		namespace Serialize
		{
			class ConfigStream;

			using namespace Serializer;

			//! \brief base class for serializer functionality on a stream
			class SerializeStream : public Memory::MemoryManaged
			{
			public:
				enum SerializeMode
				{
					SMODE_INPUT = SerializeStorage::SMODE_INPUT,
					SMODE_OUTPUT = SerializeStorage::SMODE_OUTPUT,
				};

				//! \brief create a serializer
				//! \param stream	stream which receives/holds the serialized attributes
				SerializeStream(const SharedPtr<SerializeStorage>& serialStorage);

				//! \brief destructor, needed 
				~SerializeStream()
				{

				}

				//! \brief a helper function to create fast a serialize stream
				//! \param allocPtr			allocator for all the needed memory
				//! \param serialMode		what should the serialize stream be able todo
				//! \param storageType		type of the storage xml/binary
				//! \param lowLevelStream	opened stream, where we should write/read to/from the serialize data
				//! \remark The storages will be create with default parameter.
				//!			There are only binary and xml storage supported
				static AutoPtr<SerializeStream> createSerializeStream(
					const Memory::Allocator::Ptr& allocPtr, 
					const SerializeMode serialMode, 
					const SerializeStorage::StorageType storageType, 
					IO::Stream* lowLevelStream);


				//! \brief get the serialize mode of the serializer
				inline const SerializeMode getSerializeMode() const
				{
					return mSerializeMode;
				}

				//! \brief serialize an object
				template<typename OBJECT_T, typename INST_T>
				inline void serialize(const Common::String& name, OBJECT_T& object, INST_T* instCreate)
				{
					typename GetSerializer<OBJECT_T,INST_T>::Serializer serializer= BuildSerializer(object,instCreate);
					serializeValue(name,serializer,object,instCreate);
				}

			public:
				struct ClassNode
				{
					Common::String		mName;
					Common::String		mTypeName;
					Common::String		mSubTypeName;

					ClassNode(const Common::String& name, const Common::String& typeName, const Common::String& subTypeName)
						: mName(name),mTypeName(typeName),mSubTypeName(subTypeName)
					{}

					ClassNode()
					{}
				};


			private:
				SerializeMode									mSerializeMode;		//!< shadowed serialize mode from serialize storage
				SharedPtr<SerializeStorage>						mStorage;			//!< current used storage
				SharedPtr<SerializeStorage>						mUnpatchedStorage;	//!< storage without modifications
				Container::List<ClassNode>						mClassStack;		//!< ids of currently streamed classes

				
				template<typename SERIALIZER_T,typename OBJECT_T, typename INST_T>
				inline void serializeValue(const Common::String& name, SERIALIZER_T& serializer, OBJECT_T& object, INST_T* helper)
				{
					serializeClass(name,serializer,object,helper);
				}


				// here we stream definitively a combined datatype
				template<typename OBJECT_T, typename INST_T>
				inline void serializeClass(const Common::String& name, typename GetSerializer<OBJECT_T,INST_T>::Serializer& serializer, OBJECT_T& object, INST_T* helper)
				{
					const Common::String typeName= serializer.getTypeName();
					const Common::String subTypeName= serializer.getSubTypeName();
					const int32 version= serializer.getVersion();

					mClassStack.pushBack(ClassNode(name,typeName,subTypeName));

					if (mSerializeMode == SMODE_OUTPUT)
					{
						mStorage->writeEnterClassTree(name,typeName,subTypeName,version);

						serializer.handleBeforeSerialize(object);
						serializer.serialize(object,this);

						mStorage->writeExitClassTree();
					}
					else
					{
						Common::String streamName;
						Common::String streamTypeName;
						Common::String streamSubTypeName;
						int32 streamVersion;
						mStorage->readEnterClassTree(streamName,streamTypeName,streamSubTypeName,streamVersion);

						// check if we have the correct item
						XAIT_SERIALIZE_STREAM_CHECK_INSTNAME(name,streamName);
						XAIT_SERIALIZE_STREAM_CHECK_TYPENAME(typeName,streamTypeName);

						// check if stream and serializer have the same version
						// if not so call the update method from the serializer as
						// long as the stream is not up to date
						if (version != streamVersion)
						{
							SharedPtr<SerializeStorage> patchedStorage;
							SharedPtr<SerializeStorage> currStorage= mStorage;
							int32 currVersion= streamVersion;
							int32 lastValidVersion;

							do 
							{
								AutoPtr<PatchInterface> patchIFace(new(mAllocator)PatchInterface(currStorage));
								lastValidVersion= currVersion;
								currVersion= serializer.addPatchToStream(object,version,patchIFace.get());

								currStorage= patchIFace->getPatchedStorage();
							} while(currVersion != version || currVersion == -1);
					
							if (currVersion == -1)
							{
								throw new VersionMismatchException("No patch available for object '" + typeName + "' from version '" + lastValidVersion + "' to version '" + version + "'",lastValidVersion,version);
							}

							SharedPtr<SerializeStorage> oldStorage= mStorage;
							mStorage= currStorage;

							serializer.serialize(object,this);

							mStorage->close();
							mStorage= oldStorage;
						}
						else
						{
							serializer.serialize(object,this);
						}


						mStorage->readExitClassTree();
					}

					mClassStack.popBack();
				}

				template<typename FUND_T,typename INST_T>
				inline void serializeValue(const Common::String& name, FundamentalSerializer<FUND_T>& serializer, FUND_T& object, INST_T* helper)
				{
					if (mSerializeMode == SMODE_OUTPUT)
					{
						mStorage->writeValue(name,object);
					}
					else
					{
						Common::String streamName;
						mStorage->readValue(streamName,object);
						XAIT_SERIALIZE_STREAM_CHECK_INSTNAME(name,streamName);
					}
				}
			};
		}
	}
}
