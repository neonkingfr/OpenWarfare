// (C) 2011 xaitment GmbH

#pragma once 
#ifndef _XAIT_COMMON_SERIALIZE_PATCHINTERFACE_H_
#define _XAIT_COMMON_SERIALIZE_PATCHINTERFACE_H_

#include <xait/common/memory/MemoryManaged.h>
#include <xait/common/serialize/SerializeStorage.h>
#include <xait/common/serialize/PatchInterface.h>
#include <xait/common/serialize/SerializeStream.h>
#include <xait/common/serialize/serializer/FundamentalSerializer.h>
#include <xait/common/serialize/SegmentedStorage.h>
#include <xait/common/serialize/InstanceNameMismatchException.h>

namespace XAIT
{
	namespace Common
	{
		namespace Serialize
		{
			class SerializeStream;
			
			class PatchInterface : public Memory::MemoryManaged
			{
			public:
				//! \brief create a patch interface on two serialize storages,
				//!		   where one storage provides the saved data and the patched
				//!		   storage stores the new added items
				//! \param nativeStorage	unpatched data
				PatchInterface(const SharedPtr<SerializeStorage>& nativeStorage);

				//! \brief get the final patched storage
				//! \remark should be called at the end of the patching, since this will finalize
				//!	the patching
				SharedPtr<SerializeStorage> getPatchedStorage();

				//! \brief preserves an entry from the native storage to the patched storage
				//! \remark most the time, the data will be copied from the native storage to the patched storage
				//! \param name		instance name which should be preserved (will be checked)
				void preserveItem(const Common::String& name);

				//! \brief Remove an entry from the native storage. Thus it will not be copied
				//!	to the patched storage
				//! \param name		instance name which should be skipped (will be checked)
				void skipItem(const Common::String& name);

				//! \brief here you can only read built in values of type
				//!	bool,int,float,string
				//! \remark If this method throws an compiler error, that method readValue(x,y)
				//!			does not exists, this is because you used a combined (or pointer) type 
				//!			for the value type.
				template<typename VALUE_T, typename INST_T>
				inline void readValue(const Common::String& name, VALUE_T& val)
				{
					void* instCreate= NULL;
					typename Serializer::GetSerializer<VALUE_T,INST_T>::Serializer serializer = BuildSerializer(val,instCreate);
					readValue(name,serializer,val);
				}

				//! \brief here you can write everything
				template<typename OBJECT_T, typename INST_T>
				void writePatched(const Common::String& name, const OBJECT_T& val)
				{
					int08* instCreator= NULL;
					typename Serializer::GetSerializer<OBJECT_T,INST_T>::Serializer serializer= BuildSerializer(val,instCreator);
					writeValue(name,serializer,val);
				}

			private:
				SharedPtr<SerializeStorage>				mNativeStorage;
				SharedPtr<SerializeStorage>				mPatchedStorage;
				SharedPtr<SegmentedStorage>				mSegStorage;
				SharedPtr<SerializeStorage::Position>	mPatchedStart;
				SharedPtr<IO::Stream>					mPatchedStream;

				template<typename FUND_T>
				inline void readValue(const Common::String& name, Serializer::FundamentalSerializer<FUND_T>& serializer, FUND_T& object)
				{
					Common::String instName;
					mNativeStorage->readValue(instName,object);
					if (instName != name)
					{
						throw new InstanceNameMismatchException("Cannot patch item of wrong name. Expected: '" + name + "' Found: '" + instName + "'",instName,name);
					}
				}

				// here we stream definitively a combined datatype
				template<typename SERIALIZER_T,typename OBJECT_T>
				inline void writeValue(const Common::String& name, SERIALIZER_T& serializer, OBJECT_T& object)
				{
					const Common::String typeName= serializer.getTypeName();
					const Common::String subTypeName= serializer.getSubTypeName();
					const int32 version= serializer.getVersion();

					mPatchedStorage->writeEnterClassTree(name,typeName,subTypeName,version);
					serializer.handleBeforeSerialize(object);

					AutoPtr<SerializeStream> sStream(new(mAllocator)SerializeStream(mPatchedStorage));
					serializer.serialize(object,sStream.get());

					mPatchedStorage->writeExitClassTree();
				}

				template<typename FUND_T>
				inline void writeValue(const Common::String& name, Serializer::FundamentalSerializer<FUND_T>& serializer, FUND_T& object)
				{
					mPatchedStorage->writeValue(name,object);
				}

			};
		}
	}
}
#endif

