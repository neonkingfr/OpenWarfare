// (C) 2011 xaitment GmbH

#pragma once 

#include <xait/common/serialize/SerializeException.h>

namespace XAIT
{
	namespace Common
	{
		namespace Serialize
		{
			class VersionMismatchException : public SerializeException
			{
			public:
				VersionMismatchException(const Common::String& errorMsg, const int32 foundVersion, const int32 expectedVersion)
					: SerializeException(errorMsg),mFoundVersion(foundVersion),mExpectedVersion(expectedVersion)
				{

				}

				virtual Common::String getExceptionName() const
				{
					return "TypeMismatchException";
				}

				//! \brief get the version that we have found
				inline int32 getFoundVersion() const
				{
					return mFoundVersion;
				}

				//! \brief get the version that we expected
				inline int32 getExpectedVersion() const
				{
					return mExpectedVersion;
				}

			protected:
				virtual void getExceptionParameter(Container::List<Container::Pair<Common::String,Common::String> >& parameters)
				{
					SerializeException::getExceptionParameter(parameters);

					parameters.pushBack(Container::MakePair(Common::String("FoundVersion"),Common::String(mFoundVersion)));
					parameters.pushBack(Container::MakePair(Common::String("ExpectedVersion"),Common::String(mExpectedVersion)));
				}

			private:
				int32	mFoundVersion;
				int32	mExpectedVersion;
			};
		}
	}
}
