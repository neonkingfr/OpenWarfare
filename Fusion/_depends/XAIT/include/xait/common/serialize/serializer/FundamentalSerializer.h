// (C) 2011 xaitment GmbH

#pragma once 

#include <xait/common/FundamentalTypes.h>
#include <xait/common/serialize/serializer/SerializerHelper.h>

namespace XAIT
{
	namespace Common
	{
		namespace Serialize
		{
			namespace Serializer
			{
				//! \brief serializer for fundamental types (dummy class)
				template<typename FUND_T>
				class FundamentalSerializer
				{
				public:
					typedef FUND_T		ValueType;
				};

				// extend GetStreamer to get this streamer type for fundamentals
#define FUNDSTREAMER_EXTEND_GETSTREAMER(fundType) \
				template<typename INST_T>\
				class GetSerializer<fundType,INST_T>\
				{\
				public:\
					typedef FundamentalSerializer<fundType>		Serializer;\
				}

				FUNDSTREAMER_EXTEND_GETSTREAMER(bool);
				FUNDSTREAMER_EXTEND_GETSTREAMER(uint08);
				FUNDSTREAMER_EXTEND_GETSTREAMER(int08);
				FUNDSTREAMER_EXTEND_GETSTREAMER(uint16);
				FUNDSTREAMER_EXTEND_GETSTREAMER(int16);
				FUNDSTREAMER_EXTEND_GETSTREAMER(uint32);
				FUNDSTREAMER_EXTEND_GETSTREAMER(int32);
				FUNDSTREAMER_EXTEND_GETSTREAMER(uint64);
				FUNDSTREAMER_EXTEND_GETSTREAMER(int64);
				FUNDSTREAMER_EXTEND_GETSTREAMER(float32);
				FUNDSTREAMER_EXTEND_GETSTREAMER(float64);
				FUNDSTREAMER_EXTEND_GETSTREAMER(Common::BString);
				FUNDSTREAMER_EXTEND_GETSTREAMER(Common::WString);
			}
		}
	}
}
