// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/network/message/MessageBuilder.h>
#include <xait/common/network/message/MessageReader.h>
#include <xait/common/container/List.h>
#include <xait/common/container/LookUp.h>
#include <xait/common/debugger/BreakpointInstance.h>
#include <xait/common/debugger/HistoryEntry.h>
#include <xait/common/debugger/VariableInstance.h>
#include <xait/common/debugger/ClientInfo.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			class Module;
			class ObjectType;
			//class Watchpoint;
			class DebuggerVariableAccessor;
			class BreakpointInstance;
			class GetVariableMsgHandler;
			class SetVariableMsgHandler;
			class SetBreakpointObjectInstanceMsgHandler;
			class SetWatchesMsgHandler;
			class HistoryEntry;
		}
	}
}

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
#define INVALID_OBJECT_INSTANCE_ID		(uint32)-1

			//! \brief This class describes an instance of an ObjectType containing only the data that differentiates this instance from other instances.
			//!
			//! An object instance associates an debugger object with an object from the library.
			//! To create a new ObjectInstance, call ObjectType::createObjectInstance. Once it has been created,
			//! you can register variables to it using the registerVariable-method.
			//! In order to use the ObjectInstance, it you have to call the attach-method.
			//! \see ObjectType
			class ObjectInstance : public Memory::MemoryManaged
			{
			public:
				//! \brief Gets the name of the instance.
				//! \return the name
				inline const String& getName() const { return mName; }

				//! \brief Gets the ObjectType of the instance.
				//! \return the ObjectType
				inline ObjectType* getType() const { return mType; }

				//! \brief Gets the ID of the instance.
				//! \return the ID
				inline uint32 getID() const { return mID; }

				//! \brief Attaches the ObjectInstance to the debugger.
				//!
				//! Note that all variables have to be registered before the instance is attached.
				XAIT_COMMON_DLLAPI void attach();

				//! \brief Checks whether the ObjectInstance is attached to the debugger.
				//! \return true, if it is attached to the debugger, and false otherwise
				inline bool isAttached() const {return mIsAttached;}

				//! \brief Registers a new variable.
				//! \param varName		the name of the variable
				//! \param varPtr		the pointer to the variable
				//! \return true if the Variable wasn't registered before and could be registered, and false otherwise
				XAIT_COMMON_DLLAPI bool registerVariable(const String& varName, void* varPtr);

				//! \brief Registers a new variable.
				//! \param variable		the variable
				//! \return true if the Variable wasn't registered before and could be registered, and false otherwise
				XAIT_COMMON_DLLAPI bool registerVariable(VariableInstance* variable);

				//! \brief Unregisters an existing variable.
				//! \param variableID		the ID of the variable
				//! \returns true if the variable was registered before and could be unregistered, and false otherwise
				XAIT_COMMON_DLLAPI bool unregisterVariable(const uint32 variableID);

				//! \brief Gets the variable ID corresponding to the given variableName.
				//! \param id		stores the variable id
				//! \param name		the variable name
				//! \returns true, if the variable name was known and the variable ID was found, and false otherwise
				XAIT_COMMON_DLLAPI bool getVarID(uint32& id, const String& name);

				//! \brief Enters the break area in the server.
				XAIT_COMMON_DLLAPI void enterBreakArea();

				//! \brief Leaves the break area in the server. Releases the GlobalBreakpoint if a Breakpoint was holding within this BreakArea.
				XAIT_COMMON_DLLAPI void leaveBreakArea();

				//! \brief Allows to enable the next breakpoint entered by this ObjectInstance.
				XAIT_COMMON_DLLAPI void stopInNextBreakpoint();


			private:
				friend class Module;
				friend class ObjectType;
				friend class DebuggerVariableAccessor;
				friend class BreakpointInstance;
				friend class GetVariableMsgHandler;
				friend class SetVariableMsgHandler;
				friend class SetBreakpointObjectInstanceMsgHandler;
				friend class SetWatchesMsgHandler;
				friend class HistoryEntry;
				friend class ObjectInstanceReleaseMsgHandler;


				//! \brief Constructor.
				//! \param objectType	the ObjectType this instance is derived from
				//! \param name			the name of the instance
				//! \param isClientOnly			flag to separate dummy Instances that are created by the client
				//! \param breakPointEnableFlag		flag allowing to activate all breakpoints within ObjectInstance (set to false by default)
				ObjectInstance(ObjectType* objectType, const String& name, const bool isClientOnly, const bool breakPointEnableFlag=false);

				//! \brief CopyConstructor.
				//! 
				//! Calling this method will fail. Don't copy ObjectInstances.
				ObjectInstance(const ObjectInstance& other);

				//! \brief Destructor.
				virtual ~ObjectInstance();

				//! \brief deletes all registered variables
				void deleteVariables();

				//! \brief Initializes the ObjectInstance ID.
				//!
				//! This function can only be used once. The ID must not be INVALID_OBJECT_INSTANCE_ID.
				//! \param id	the id to be set
				void initID( const uint32 id );

				//! \brief Checks whether a variable with the given variable ID exists.
				//! \param varID the variable ID
				//! \return true, when it exists, and false otherwise
				bool existsWatchedVariable( const uint32 varID ) const;

				//! \brief Checks whether the initialization has been completed.
				//! \return true, if it has been completed, and false otherwise.
				bool isInitComplete() const;

				//! \brief Registers a new watched variable.
				//! \param variableID			the ID of the variable
				//! \return true, if the variable was known and wasn't watched before, and false otherwise
				bool registerWatchedVariable( const uint32 variableID );

				//! \brief Unregisters a watched variable.
				//! \param variableID			the ID of the variable
				//! \return true, if the variable was known and was watched before, and false otherwise
				bool unregisterWatchedVariable(const uint32 variableID);

				//! \brief Collects the IDs of all Watchpoints within this ObjectInstance.
				//! \param ids		list to fill with the registered watchpointIDs
				//void getWatchpointIDs(Container::List<uint32>& ids) const;

				//! \brief Writes the value of the variable with the given name into the stream via the given MessageBuilder.
				//! \param msgBuilder			the MessageBuilder to write the variable to
				//! \param variableName			the name of the variable
				//! \return true, if the variable could be written correctly, and false otherwise
				bool getVariableData(Network::Message::MessageBuilder& msgBuilder, const String& varName) const;

				//! \brief Writes the value of the variable with the given ID into the stream via the given MessageBuilder.
				//! \param msgBuilder			the MessageBuilder to write the variable to
				//! \param variableID			the name of the variable
				//! \return true, if the variable could be written correctly, and false otherwise
				bool getVariableData(Network::Message::MessageBuilder& msgBuilder, const uint32 variableID) const;

				//! \brief collects the values of all variables under this ObjectInstance within given HistoryEntry
				//! \param hEntry	the HistoryEntry to write to
				//void getWatchesSnapshot(HistoryEntry* hEntry) const;

				//! \brief Called when the connected client closed the connection.
				//! 
				//! No breakpoints and watchpoints points should report anymore.
				void onClientDisconnect();

				//! \brief Sends all information about the variables to the client.
				//! \param newClientInfo	the client info of the newly connected client
				void onClientConnect(ClientInfo* newClientInfo);

				//! \brief Sends an ObjectInstance registration.
				void sendObjectInstanceRegistration();

				//! \brief sends an ObjectInstance update
				void sendObjectInstanceUpdate();

				//! \brief Sets the value of a variable.
				//! \param variableID	the ID of the variable
				//! \param msgReader	the reader to get the data from
				//! \returns 0: no error, 1: varUnknown, 2: dataError
				uint08 setVariableValue(const uint32 variableID, Network::Message::MessageReader* msgReader);

				//! \brief Gets the value of a variable.
				//! \param value	stores the value
				//! \param varName	the name of the variable
				template<typename T_VARIABLE_TYPE>
				void getVariableValue(T_VARIABLE_TYPE& value, const String& varName) 
				{
					//ToDo: check that the variable has the correct type
					value = *((T_VARIABLE_TYPE*)getVariableValuePtr(varName));
				}

				//! \brief Gets the value of a variable.
				//! \param varName		the name of the variable
				//! \returns a pointer to the value
				void* getVariableValuePtr(const String& varName);

				//! \brief Gets the variable instance with the given name.
				//! \param var		stores the pointer to the variable instance
				//! \param varName	the name of the variable
				//! \return true, if the variable instance was found, and false otherwise
				bool getVariableInstance(VariableInstance*& var, const String& varName) const;

				//! \brief Gets the variable instance with the given ID.
				//! \param var		stores the pointer to the variable instance
				//! \param varID	the ID of the variable
				//! \return true, if the variable instance was found, and false otherwise
				bool getVariableInstance(VariableInstance*& var, const uint32& varID) const;

				//! \brief Sets the break flag to the given value.
				//! \param val the new value
				void setBreakflag(const bool val);

				//! \brief Checks whether the break flag is activated.
				//! \return true, if it is activated, and false otherwise
				bool getBreakflag() const { return mBreakPointEnableFlag; }

				//! \brief Checks whether the library flag is activated.
				//! \return true, if it is activated, and false otherwise
				bool getLibraryFlag() const { return mLibraryBreakFlag; };

				//! \brief Sets the library flag to false.
				void disableLibraryFlag() { mLibraryBreakFlag = false; };

				//! \brief Checks whether a client is connected and whether its break flag is set.
				//! \returns true if a client is connected and its break flag is set, and false otherwise
				bool getClientBreakflag() const;

				//! \brief Gets the DebuggerVariableAccessor.
				//! \return the DebuggerVariableAccessor.
				DebuggerVariableAccessor* getVariableAccessor();

				//! \brief deletes this ObjectInstance if it isn't existing within game
				//! \returns false if this ObjectInhstance is still within the game
				//! \remark use setIsClientOnly(true) to mark an ObjectInstance not to be within game
				bool destroyClientOnlyObjectInstance();

				//! \brief removes the ObjectInstance from game or marks it to be within game
				//! \param val	the new value indicating whether this ObjectInstance is within game
				//! \returns true if the ObjectInstance changed from/to game
				//! \remark removing ObjectInstances from the game result in being unattached
				bool setIsClientOnly(const bool val);

				//! \brief returns whether this instance is created within the game
				//! \return true if this instance is created within the game, false else
				bool getIsClientOnly() const;

			//member variables
				String		mName;					//!< name of the instance
				ObjectType*	mType;					//!< ObjectType this instance is derived from
				uint32		mID;					//!< UserID (is send to the Remote Debugger Client)

				bool	mIsClientOnly;		//!< flag indicating whether this instance is created within the game

				Container::LookUp<uint32, VariableInstance*>		mVariableList; //!< the list of variables
				DebuggerVariableAccessor*	mVariableAccessor;	//!< the DebuggerVariableAccessor
				Container::List<uint32>		mWatchpointList;	//!< list of variable IDs that are marked to be watched

				bool		mBreakPointEnableFlag;	//!< Breakpoint Enable Override Flag allows to activate/deactivate all breakpoints within the ObjectInstance
				bool		mIsAttached;			//!< flag indicating whether this instance is attached to the module
				ClientInfo* mClientConnected;		//!< the client info of the connected client

				bool		mLibraryBreakFlag;		//!< the break flag of the library

				uint08		mNumCreatedByClient;
			};
		}
	}
}
