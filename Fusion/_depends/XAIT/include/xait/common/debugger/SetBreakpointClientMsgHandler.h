// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/debugger/Server.h>
#include <xait/common/network/message/MessageHandler.h>
#include <xait/common/debugger/ObjectType.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief Message handler handling MESSAGE_BREAK.
			//!
			//! Handles the client message asking the server to set the break flag for the client to true.
			//! The result is a GlobalBreak state for all modules connected to this client.
			//! Has to be registered with the server.
			//! \see Server
			class SetBreakpointClientMsgHandler : public Network::Message::MessageHandler
			{
#define MSGHANDLER_SETBREAKPOINTCLIENT_PARAMETER	"setBreakpointClientMessage: {}"

			public:
				//! \brief Constructor.
				//! \param server	the server allows access to the registered modules
				SetBreakpointClientMsgHandler( Server* server );

				//! \brief Handles the message by reading the data with the MessageReader.
				//! \param msgReader	the reader containing the message
				//! \param connectionID		the identifier of the client the message is from
				//! \return true, if message was handled correctly, and false otherwise
				bool handleMessage(Network::Message::MessageReader* msgReader, const uint32 connectionID );

			protected:

			private:
				//! \brief Reads the message parameters from the stream.
				//! \param msgReader the message reader
				bool getParameters(Network::Message::MessageReader* msgReader);
				//bool reportReply( const uint32 connectionID );


				Server*		mServer; //!< the server
			};

		}
	}
}
