// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/debugger/Server.h>
#include <xait/common/debugger/ObjectType.h>
#include <xait/common/network/message/MessageHandler.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief Message handler handling MESSAGE_OBJECTTYPE_ACK.
			//!
			//! This message handler is called when the client has received an MESSAGE_OBJECTTYPE_REGISTER
			//! and wants to give this ObjectType free for ObjectInstance registrations within the server.
			//! Unless this message all ObjectInstance register requests from the game will be catched by a mutex
			//! The message handler has to be registered with the server.
			//! \see Server
			class ObjectTypeAckMsgHandler : public Network::Message::MessageHandler
			{
#define MSGHANDLER_OBJECTTYPE_ACK_PARAMETER	"objectTypeAckMessage: { uint32:moduleID, uint32:objectTypeID }"

			public:
				//! \brief Constructor.
				//! \param server the server
				ObjectTypeAckMsgHandler( Server* server );

				//! \brief Handles the message by reading the data through the MessageReader.
				//! \param msgReader	the reader containing the message
				//! \param connectionID		the identifier of the client the message is from
				//! \returns true, if message was handled correctly, and false otherwise
				bool handleMessage(Network::Message::MessageReader* msgReader, const uint32 connectionID );

			private:
				//! \brief Reads the message parameters from the stream.
				//! \param msgReader the message reader
				bool getParameters(Network::Message::MessageReader* msgReader);

				Server*		mServer;

				uint32 mModuleID;
				uint32 mObjectTypeID;
				bool   mServerOnlyType;

				Module* mModule;
				ObjectType* mObjectType;
			};

		}
	}
}
