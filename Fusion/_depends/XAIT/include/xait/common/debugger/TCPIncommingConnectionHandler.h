// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/network/sockets/TCPSocket.h>
#include <xait/common/network/InputHandler.h>
#include <xait/common/debugger/TCPConnectionLayer.h>
#include <xait/common/memory/MemoryStream.h>


namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief Handles incoming TCP connections.
			//!
			//! Checks periodically whether there are incoming TCP connections and tries to accept them.
			//! Has to be registered with the reactor.
			//! \see TCPConnectionLayer
			//! \see Reactor
			class TCPIncommingConnectionHandler : public Network::InputHandler
			{
			public:
				//! \brief Constructor
				//! \param socket	the connection given as TCPSocket
				//! \param tcpConnectionLayer	needs to know all new connections
				TCPIncommingConnectionHandler(Network::Sockets::TCPSocket* socket, TCPConnectionLayer* tcpConnectionLayer);

				//! \brief Destructor
				~TCPIncommingConnectionHandler();

				//! \brief	Initializes the connection on the socket.
				//! \returns true, if the initialization was successfull, and false otherwise
				//! \see XAIT::Common::Network::Sockets::SocketError::getErrorText()
				bool init();

				//! \brief Update function to assign computation time to the handler.
				bool onTick();

			private:
				TCPConnectionLayer*	mConnectionLayer; //!< the TCP connection layer
			};

		} // Network
	} // Common
} // XAIT
