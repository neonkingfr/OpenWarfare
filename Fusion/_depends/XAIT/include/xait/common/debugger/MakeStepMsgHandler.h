// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/debugger/Server.h>
#include <xait/common/network/message/MessageHandler.h>
#include <xait/common/debugger/ObjectType.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief Message handler handling MESSAGE_MAKE_STEP.
			//!
			//! Releases the thread that entered the Breakpoint and activated the holding-function.
			//! If the BreakpointTypeFilterID is set, then BreakpointInstances need the same 
			//! BreakpointType to be activated. If ObjectInstanceFilter is activated then
			//! the BreakpointInstances need to be called by the same ObjectInstance that was 
			//! holding before. Both filters can be set independently. If no filter is set all
			//! BreakpointInstances of all modules the Client is connected with will hold. 
			//! Exception: BreakpointInstances that are set explicitly will
			//! be able to hold the thread too.
			//! The message handler has to be registered with the server.
			//! \see Server
			class MakeStepMsgHandler : public Network::Message::MessageHandler
			{
#define MSGHANDLER_MAKESTEP_PARAMETER	"makeStepMsgHandler: { bool:objectInstanceFilter, uint32:breakpointTypeFilterID }"

			public:
				//! \brief Constructor.
				//! \param server	the server allows access to the registered modules
				MakeStepMsgHandler( Server* server );

				//! \brief Handles the message by reading the data through the MessageReader.
				//! \param msgReader	the reader containing the message
				//! \param connectionID		the identifier of the client the message is from
				//! \return true, if message was handled correctly, and false otherwise
				bool handleMessage(Network::Message::MessageReader* msgReader, const uint32 connectionID );

			protected:

			private:
				//! \brief Reads the message parameters from the stream.
				//! \param msgReader the message reader
				bool getParameters(Network::Message::MessageReader* msgReader);

				Server*		mServer;					//!< the server
				bool		mObjectInstanceFilter;		//!< the object instance filter flag
				uint32		mBreakpointTypeFilterID;	//!< the breakpoint type filter flag
			};

		}
	}
}
