// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/container/List.h>
#include <xait/common/debugger/BreakpointType.h>
#include <xait/common/network/message/MessageBuilder.h>
#include <xait/common/debugger/HistoryEntry.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			class Module;
			class ObjectType;
			class ObjectInstance;
			class HistoryEntry;
			class SetBreakpointConditionMsgHandler;
			class SetBreakpointMsgHandler;
		}

		namespace Parser
		{
			namespace Formula
			{
				class MathValue;
			}
		}
	}
}

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
#define INVALID_BREAKPOINT_INSTANCE_ID		(uint32)-1


			//! \brief Defines an instance of a breakpoint.
			//!
			//! Breakpoint instances are actual breakpoints that appear in the program that uses the debugger.
			//! They are defined by their BreakpointType.
			//! \see BreakpointType.
			class BreakpointInstance : public Memory::MemoryManaged
			{
			public:
				//! \brief This method enters the breakpoint.
				//!
				//! This function call locks your program if the BreakpointInstance is active until the BreakpointInstance is deactivated.
				//! \param triggerObjInst	the ObjectInstance that entered the Breakpoint
				XAIT_COMMON_DLLAPI void enterBreakpoint(ObjectInstance* triggerObjInst);

				//! \brief This method enters the breakpoint and creates a static history entry.
				//!
				//! This function call locks your program if the BreakpointInstance is active until the BreakpointInstance is deactivated.
				//! \param triggerObjInst	the ObjectInstance that entered the Breakpoint
				//! \param historyEntry an iterator to the static history entry
				XAIT_COMMON_DLLAPI void enterStaticBreakpoint(ObjectInstance* triggerObjInst, HistoryEntry*& historyEntry);

				//! \brief Gets the type of the breakpoint.
				//! \return the type of the breakpoint
				inline BreakpointType* getType() const {return mType;}

				//! \brief Gets the name of the breakpoint.
				//! \return the name of the breakpoint
				inline const String& getName() const {return mName;}

				//! \brief Gets the ID of the breakpoint.
				//! \return the ID of the breakpoint
				inline uint32 getID() const {return mID;}

				//! \brief Gets the object type of the breakpoint.
				//! \return the object type of the breakpoint
				inline ObjectType* getObjectType() const {return mObjectType;}

				//! \brief Gets a list containing all variables.
				//! \param storeVariables the variables will be written to this list
				XAIT_COMMON_DLLAPI void getStoredVariables(Container::List<String>& storeVariables) const;

				//! \brief set a user defined tag
				//! \param tag the user defined tag
				XAIT_COMMON_DLLAPI void setTag(void* tag){mTag = tag;}

				//! \brief get a user defined tag
				//! \return the user defined tag
				XAIT_COMMON_DLLAPI void* getTag() const {return mTag;}

			private:
				friend class ObjectType;
				friend class Module;
				friend class HistoryEntry;

				friend class SetBreakpointConditionMsgHandler;
				friend class SetBreakpointMsgHandler;

				//! \brief Constructor.
				//! \param type		the BreakpointType of this BreakpointInstance
				//! \param objType	the ObjectType to which this BreakpointInstance belongs
				//! \param name		the name of this BreakpointInstance
				//! \param storeVariables	list of variable names that are stored to the history each time this BreakpointInstance is entered
				BreakpointInstance(BreakpointType* type, ObjectType* objType, const String& name, const Container::List<String>& storeVariables);

				//! \brief Destructor.
				virtual ~BreakpointInstance();

				//! \brief Initializes the ID of the breakpoint instance.
				//!
				//! The identification string will be changed. This function can only be used once.
				//! The id must not be INVALID_BREAKPOINT_INSTANCE_ID.
				//! \param id	the new ID
				void initID( const uint32 id );

				//! \brief Checks whether the breakpoint has been activated.
				//! \return true, if the breakpoint has been activated, and false otherwise
				bool isBreakpointActivated() const;

				//! \brief Checks whether the breakpoint is currently holding.
				//! \returns true, if the breakpoint is holding, and false otherwise
				bool isBreakpointHolding() const { return mIsHolding; };

				//! \brief Combines the type of the breakpoint, the name of the breakpoint and its ID into a string: typeName + '_' + ownName + '_' + ownID
				//! \returns an identification string
				const String& getIdentificationString() const { return mIdentificationString; }

				//! \brief This method is called if the breakpoint should hold the calling thread.
				//! \param triggerObjInst	the ObjectInstance that entered the Breakpoint
				void enterBreakpointHolding(ObjectInstance* triggerObjInst);

				//! \brief This method is called if the breakpoint should only notice a calling thread passing.
				//! \param triggerObjInst	the ObjectInstance that entered the Breakpoint
				void enterBreakpointPassing(ObjectInstance* triggerObjInst);

				//! \brief Sets this BreakpointInstance active.
				//! \see setInactive
				void setActive();

				//! \brief Sets this BreakpointInstance inactive.
				//! \see setActive
				void setInactive();

				//! \brief Changes the active state of this BreakpointInstance.
				//! \param val		the new value
				void setActiveState(const bool val);

				//! \brief Sets the condition that has to be satisfied before this breapointInstance holds if it is activated.
				//! \param condition	the condition
				//! \returns 0, if the new condition was set, and 1 if the condition wasn't of type bool
				uint08 setCondition(Parser::Formula::MathValue* condition);

				//! \brief Sets the condition that has to be satisfied before this breapointInstance holds if it is activated.
				//! \param conditionStream	a stream holding the condition
				//! \returns 0, if the new condition was set, and 1 if the condition wasn't of type bool
				uint08 setCondition(IO::Stream* conditionStream);

				//! \brief Checks whether the BreakpointCondition evaluates to true for the given ObjectInstance.
				//! \param objInst	the ObjectInstance
				//! \returns true, if the condition evaluates to true for given ObjectInstance (or no condition was set), and false otherwise
				bool evaluateCondition(ObjectInstance* objInst) const;

				//! \brief Copy constructor.
				//! \param other	the other BreakpointInstance to copy from
				BreakpointInstance(const BreakpointInstance& other);

				//! \brief Collects information that was bound late.
				//! \param msgBuilder	the MessageBuilder
				void getLateBoundInformation(Network::Message::MessageBuilder& msgBuilder) const;

				//! \brief Stores the wanted variable data in the MessageBuilder
				//! \param objectInstance	the ObjectInstance delivering the needed VariableInstances
				//! \param msgBuilder		the MessageBuilder collecting the data
				void getStoredVariablesMemento(const ObjectInstance* objectInstance, Network::Message::MessageBuilder& msgBuilder) const;
				
				//! \brief retrieves the number of stored Variables
				uint32 getNumStoredVariables() const;

				//! \brief Stores a history entry iterator so that it can be released later
				//! \parem historyEntry the iterator to the history entry
				void setStaticHistoryEntry(HistoryEntry* historyEntry);

			//member
				Threading::SpinLock*	mMutexEnterBreakpointSync;	//!< allows only one thread at once entering a Breakpoint


				BreakpointType*	mType;							//!< the type of the breakpoint
				String			mName;							//!< the name of the breakpoint
				uint32			mID;							//!< the ID of the breakpoint
				String			mIdentificationString;			//!< the identification string, composed of the type, the name and the ID
				bool			mIsBreakpointActivated;			//!< flag indicating whether this breakpoint should hold or not
				bool			mIsHolding;						//!< flag indicating whether the breakpoint is currently holding
				ObjectType*		mObjectType;					//!< the object type to which this breakpoint belongs to

				bool			mCreateStaticHistoryEntries;	//!< states whether static history entries should be generated
				HistoryEntry*	mHistoryEntry;					//!< the last static history entry that has been generated

				Parser::Formula::MathValue*		mCondition;		//!< conditional expression

				Container::List<String>		mStoreVariables;	//!< list of the variables

				void*	mTag;									//!< tag for userdefined data
			};
		}
	}
}
