// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/threading/Mutex.h>


namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief A security gate allowing to lock a public thread by a security thread and ensure that the public thread is released after the gate is enabled again.
			//!
			//! The securing thread controls the mutex gate. It decides when to let another public thread through.
			//! Supports currently one public thread (TODO: multiple threads).
			//!
			//! (securing thread) (enter mutex) (control mutex) (exit mutex) (public thread)
			//!       lock ------------>|               |            |                     
			//!                         |               |<-----------+----------- lock                                     
			//!                         |<--------------+------------+----------- lock
			//!       lock -------------+---------------+----------->|
			//!      unlock ----------->|               |            |                                     
			//!                         |<--------------+------------+---------- unlock
			//!       lock -------------+-------------->|            |
			//!                         |               |            |
			//! (let the public thread  |               |            | (do some stuff while the securing thread
			//!     do some stuff)      |               |            |  is locked on the control mutex)
			//!                         |               |            |
			//!                         |               |<-----------+---------- unlock                                     
			//!      unlock ------------+-------------->|            |                                     
			//!       lock ------------>|               |            |                                     
			//!		 unlock ------------+---------------+----------->|
			//!                         |               |            |<---------- lock                         
			//!                         |               |            |<--------- unlock
			//!                         |               |            |
			class MutexGate
			{
			public:
				//! \brief Constructor.
				XAIT_COMMON_DLLAPI MutexGate();

				//! \brief Destructor.
				XAIT_COMMON_DLLAPI ~MutexGate();

				//! \brief Lets a public thread to go through the public site of the gate.
				//!
				//! Note that only one public thread is supported.
				XAIT_COMMON_DLLAPI void enterPublic();

				//! \brief Activates the gate.
				XAIT_COMMON_DLLAPI void activate();

				//! \brief Deactivates the gate.
				XAIT_COMMON_DLLAPI void deactivate();

				//! \brief Closes the mutex gate and allows the locked thread to pass.
				XAIT_COMMON_DLLAPI void close();

				//! \brief Allows the security thread to let a public thread pass.
				//! 
				//! Needs to be active and have a waiting public thread.
				XAIT_COMMON_DLLAPI bool allowOnePublicPass();

				//! \brief Checks whether the gate is active.
				//! \returns true, if the Gate is active, and false otherwise
				XAIT_COMMON_DLLAPI bool isActivated() const { return mActive; };

			private:
				uint08		mPublicCount;			//!< counts the number of public threads
				bool		mActive;				//!< flag indicating whether the gate is active
				Threading::Mutex	mMutexControl;	//!< control mutex
				Threading::Mutex	mMutexEnter;	//!< enter mutex
				Threading::Mutex	mMutexExit;		//!< exit mutex
			};
		}
	}
}
