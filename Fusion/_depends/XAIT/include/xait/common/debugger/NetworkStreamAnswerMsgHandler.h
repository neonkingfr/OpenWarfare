// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/debugger/NetworkStream.h>
#include <xait/common/debugger/Server.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief Message handler handling MESSAGE_NETWORKSTREAM_DATA_REPLY.
			//!
			//! The client answers the request from the server by sending the network stream data.
			//! Has to be registered with the server.
			//! \see Server
			class NetworkStreamAnswerMsgHandler : public Network::Message::MessageHandler
			{
#define MSGHANDLER_NETWORKSTREAM_ANSWER_PARAMETER	"updateDataNetworkStreamMessage: { uint32:networkStreamID, dataStream:NetworkStream }"

			public:
				//! \brief Constructor.
				//! \param server	the server allows access to the registered modules
				NetworkStreamAnswerMsgHandler( Server* server );

				//! \brief Destructor
				virtual ~NetworkStreamAnswerMsgHandler();

				//! \brief Handles the message by reading the data through the MessageReader.
				//! \param msgReader	the reader containing the message
				//! \param connectionID		the identifier of the client the message is from
				//! \returns true, if the message was handled correctly, and false otherwise
				bool handleMessage(Network::Message::MessageReader* msgReader, const uint32 connectionID );

			protected:

			private:
				//! \brief Reads the message parameters from the stream.
				//! \param msgReader the message reader
				bool getParameters(Network::Message::MessageReader* msgReader);

				//! \brief Resets the internal variables.
				void reinitVariables();

				Server*		mServer;				//!< the server

				uint32		mCurNetworkStreamID;	//!< the network stream ID
				IO::Stream*	mCurStream;				//!< the network stream
			};
		}
	}
}
