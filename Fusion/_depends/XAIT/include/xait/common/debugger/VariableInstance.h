// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/network/message/MessageBuilder.h>
#include <xait/common/network/message/MessageReader.h>
#include <xait/common/container/List.h>
#include <xait/common/container/LookUp.h>
#include <xait/common/debugger/BreakpointInstance.h>
#include <xait/common/debugger/HistoryEntry.h>
#include <xait/common/debugger/VariableType.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			class Module;
		}
	}
}

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
#define INVALID_WATCHPOINT_ID		(uint32)-1

			//! \brief Defines the access rights to a variable.
			enum VariableAccessRights
			{
				VARIABLE_ACCESS_READ		= 0x01,
				VARIABLE_ACCESS_READ_WRITE	= 0x03,
			};

			//! \brief Defines an instance of a variable based on a variable type.
			//!
			//! Variable instances are used in ObjectInstances to represent actual variables.
			//! \see ObjectInstance
			//! \see VariableType
			class VariableInstance : public Memory::MemoryManaged
			{
			public:

				//! \brief Constructor.
				//! \param type		the type of the variable
				XAIT_COMMON_DLLAPI VariableInstance(VariableType* type);
				
				//! \param varPtr	the pointer to the variable
				VariableInstance(VariableType* type, void* varPtr);

				//! \brief Copy constructor.
				//! \param other	the VariableInstance to copy
				VariableInstance(const VariableInstance& other);

				//! \brief Destructor.
				XAIT_COMMON_DLLAPI ~VariableInstance();

				//! \brief Reads the data from the variable and writes it to the stream with the given MessageBuilder.
				//! \param msgBuilder	the MessageBuilder storing the data of the variable
				//! \returns true, if the variable could be read correctly, and false otherwise
				XAIT_COMMON_DLLAPI virtual bool readTo(Network::Message::MessageBuilder& msgBuilder) const;

				//! \brief Reads the data from the variable and writes it as a own stream to the stream with the given MessageBuilder.
				//! \param msgBuilder	the MessageBuilder storing the data of the variable
				//! \returns true, if the variable could be read correctly, and false otherwise
				XAIT_COMMON_DLLAPI virtual bool readToAsStream(Network::Message::MessageBuilder& msgBuilder) const;

				//! \brief Writes the data to the variable from the stream with the given MessageReader.
				//! \param msgReader	the MessageReader storing the data of the variable
				//! \returns true, if the variable could be written correctly, and false otherwise
				XAIT_COMMON_DLLAPI virtual bool writeFrom(Network::Message::MessageReader* msgReader);

				uint32 getID() const { return mType->getID(); }
				VariableAccessRights getAccessRight() const { return mAccessRight; }
				String getName() const { return mType->getName(); }
				Network::Message::StreamIDs getVarType() const { return mType->getType(); }
				virtual void* getVarPtr() const { return mVariablePtr; }

			protected:

				void*		mVariablePtr;				//!< a pointer to the variable
				VariableType*	mType;					//!< the type of the variable
				VariableAccessRights	mAccessRight;	//!< the access rights of the variable
			};


			////A Watchpoint defines the variable that should be written into the history each time a breakpoint is passed
			//class Watchpoint
			//{
			//public:
			//	Watchpoint(VariableInstance* variable)
			//		: mInstanceID(INVALID_WATCHPOINT_ID), mVariable(variable)
			//	{}

			//	bool isEqual(const Watchpoint& other);

			//	uint32 getID() const {return mInstanceID;}
			//	VariableInstance* getVariable() const { return mVariable; }

			//private:
			//	friend class Module;


			//	//! \brief sets the ID for the first time
			//	//! \param id	the id to set
			//	//! \remark this function can only be used once and the id must not be INVALID_WATCHPOINT_ID
			//	void initID( const uint32 id );

			//	uint32	mInstanceID;
			//	VariableInstance*	mVariable;
			//};
		}
	}
}

