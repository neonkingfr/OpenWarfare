// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/network/message/MessageBuilder.h>
#include <xait/common/network/message/MessageReader.h>
#include <xait/common/container/List.h>
#include <xait/common/container/LookUp.h>
#include <xait/common/debugger/BreakpointInstance.h>
#include <xait/common/debugger/HistoryEntry.h>
#include <xait/common/reflection/datatype/Datatypes.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			class Module;
		}
	}
}

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
#define INVALID_VARIABLE_TYPE_ID		(uint32)-1

			//! \brief Defines the type of a variable.
			//!
			//! In order to use a VariableType, you have to register it with an ObjectType. 
			//! \see ObjectType
			//! \see VariableInstance
			class VariableType : public Memory::MemoryManaged
			{
			public:
				//! \brief Destructor.
				virtual ~VariableType();

				const Network::Message::StreamIDs& getType() const { return mVariableType; }
				const String& getName() const { return mName; }
				const uint32& getID() const { return mVarID; }
				bool isReadOnly() const { return mReadOnly; }
				virtual Reflection::Datatype::DatatypeID getDatatypeID() const =0;

				//! \brief Creates a new variable type.
				//! \param allocator	a memory allocator
				//! \param varType		the type of the variable type
				//! \param name			the name of the variable type
				//! \param readOnly		flag that indicates whether or not the variable type should be read only
				//! \return a new VariableType
				static VariableType* createVariableType(Memory::Allocator::Ptr allocator, const Network::Message::StreamIDs varType, const String& name, const bool readOnly);

			protected:
				//! \brief Constructor
				//! \param varType		the type of the variable
				//! \param varPtr		the pointer to the variable
				//! \param name			the name of the variable
				VariableType(const Network::Message::StreamIDs varType, const String& name, const bool readOnly);

				//! \brief CopyConstructor
				//! \param other	the other VariableType to copy from
				VariableType(const VariableType& other);

			private:
				friend class Module;

				//! \brief sets the ID for the first time
				//! \param id	the id to set
				//! \remark this function can only be used once and the id must not be INVALID_VARIABLE_TYPE_ID
				void initID( const uint32 id );

				Network::Message::StreamIDs		mVariableType;
				String		mName;
				uint32		mVarID;
				bool		mReadOnly;
			};


			class VariableTypeBool : public VariableType { public:		VariableTypeBool(const String& name, const bool readOnly) : VariableType(Network::Message::STREAM_TYPE_BOOL, name, readOnly) {};					Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(bool);};	};
			class VariableTypeUint08 : public VariableType { public:	VariableTypeUint08(const String& name, const bool readOnly) : VariableType(Network::Message::STREAM_TYPE_UINT08, name, readOnly) {};				Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(uint08);};	};
			class VariableTypeUint16 : public VariableType { public:	VariableTypeUint16(const String& name, const bool readOnly) : VariableType(Network::Message::STREAM_TYPE_UINT16, name, readOnly) {};				Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(uint16);};	};
			class VariableTypeUint32 : public VariableType { public:	VariableTypeUint32(const String& name, const bool readOnly) : VariableType(Network::Message::STREAM_TYPE_UINT32, name, readOnly) {};				Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(uint32);};	};
			class VariableTypeUint64 : public VariableType { public:	VariableTypeUint64(const String& name, const bool readOnly) : VariableType(Network::Message::STREAM_TYPE_UINT64, name, readOnly) {};				Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(uint64);};	};
			class VariableTypeInt08 : public VariableType { public:		VariableTypeInt08(const String& name, const bool readOnly) : VariableType(Network::Message::STREAM_TYPE_INT08, name, readOnly) {};					Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(int08);};	};
			class VariableTypeInt16 : public VariableType { public:		VariableTypeInt16(const String& name, const bool readOnly) : VariableType(Network::Message::STREAM_TYPE_INT16, name, readOnly) {};					Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(int16);};	};
			class VariableTypeInt32 : public VariableType { public:		VariableTypeInt32(const String& name, const bool readOnly) : VariableType(Network::Message::STREAM_TYPE_INT32, name, readOnly) {};					Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(int32);};	};
			class VariableTypeInt64 : public VariableType { public:		VariableTypeInt64(const String& name, const bool readOnly) : VariableType(Network::Message::STREAM_TYPE_INT64, name, readOnly) {};					Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(int64);};	};
			class VariableTypeFloat32 : public VariableType { public:	VariableTypeFloat32(const String& name, const bool readOnly) : VariableType(Network::Message::STREAM_TYPE_FLOAT32, name, readOnly) {};				Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(float32);};	};
			class VariableTypeFloat64 : public VariableType { public:	VariableTypeFloat64(const String& name, const bool readOnly) : VariableType(Network::Message::STREAM_TYPE_FLOAT64, name, readOnly) {};				Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(float64);};	};
			class VariableTypeBString : public VariableType { public:	VariableTypeBString(const String& name, const bool readOnly) : VariableType(Network::Message::STREAM_TYPE_BSTRING, name, readOnly) {};				Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(BString);};	};
			class VariableTypeWString : public VariableType { public:	VariableTypeWString(const String& name, const bool readOnly) : VariableType(Network::Message::STREAM_TYPE_WSTRING, name, readOnly) {};				Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(WString);};	};
			class VariableTypeVec2f : public VariableType { public:		VariableTypeVec2f(const String& name, const bool readOnly) : VariableType(Network::Message::STREAM_TYPE_VEC_2_FLOAT32, name, readOnly) {};			Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Math::Vec2f);};	};
			class VariableTypeVec2d : public VariableType { public:		VariableTypeVec2d(const String& name, const bool readOnly) : VariableType(Network::Message::STREAM_TYPE_VEC_2_FLOAT64, name, readOnly) {};			Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Math::Vec2d);};	};
			class VariableTypeVec2i32 : public VariableType { public:	VariableTypeVec2i32(const String& name, const bool readOnly) : VariableType(Network::Message::STREAM_TYPE_VEC_2_INT32, name, readOnly) {};			Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Math::Vec2i32);};	};
			class VariableTypeVec2u32 : public VariableType { public:	VariableTypeVec2u32(const String& name, const bool readOnly) : VariableType(Network::Message::STREAM_TYPE_VEC_2_UINT32, name, readOnly) {};		Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Math::Vec2u32);};	};
			class VariableTypeVec3f : public VariableType { public:		VariableTypeVec3f(const String& name, const bool readOnly) : VariableType(Network::Message::STREAM_TYPE_VEC_3_FLOAT32, name, readOnly) {};			Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Math::Vec3f);};	};
			class VariableTypeVec3d : public VariableType { public:		VariableTypeVec3d(const String& name, const bool readOnly) : VariableType(Network::Message::STREAM_TYPE_VEC_3_FLOAT64, name, readOnly) {};			Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Math::Vec3d);};	};
			class VariableTypeVec3i32 : public VariableType { public:	VariableTypeVec3i32(const String& name, const bool readOnly) : VariableType(Network::Message::STREAM_TYPE_VEC_3_INT32, name, readOnly) {};			Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Math::Vec3i32);};	};
			class VariableTypeVec3u32 : public VariableType { public:	VariableTypeVec3u32(const String& name, const bool readOnly) : VariableType(Network::Message::STREAM_TYPE_VEC_3_UINT32, name, readOnly) {};		Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Math::Vec3u32);};	};

			/************************************************************************/
			/* variable types with array                                            */
			/************************************************************************/
			class VariableTypeArrayBool : public VariableType { public:			VariableTypeArrayBool(const String& name, const bool readOnly) : VariableType(Network::Message::StreamIDs(Network::Message::STREAM_TYPE_BOOL | Network::Message::STREAM_TYPE_ARRAY), name, readOnly) {};					Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Container::Vector<bool>);};			};
			class VariableTypeArrayUint08 : public VariableType { public:		VariableTypeArrayUint08(const String& name, const bool readOnly) : VariableType(Network::Message::StreamIDs(Network::Message::STREAM_TYPE_UINT08 | Network::Message::STREAM_TYPE_ARRAY), name, readOnly) {};				Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Container::Vector<uint08>);};		};
			class VariableTypeArrayUint16 : public VariableType { public:		VariableTypeArrayUint16(const String& name, const bool readOnly) : VariableType(Network::Message::StreamIDs(Network::Message::STREAM_TYPE_UINT16 | Network::Message::STREAM_TYPE_ARRAY), name, readOnly) {};				Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Container::Vector<uint16>);};		};
			class VariableTypeArrayUint32 : public VariableType { public:		VariableTypeArrayUint32(const String& name, const bool readOnly) : VariableType(Network::Message::StreamIDs(Network::Message::STREAM_TYPE_UINT32 | Network::Message::STREAM_TYPE_ARRAY), name, readOnly) {};				Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Container::Vector<uint32>);};		};
			class VariableTypeArrayUint64 : public VariableType { public:		VariableTypeArrayUint64(const String& name, const bool readOnly) : VariableType(Network::Message::StreamIDs(Network::Message::STREAM_TYPE_UINT64 | Network::Message::STREAM_TYPE_ARRAY), name, readOnly) {};				Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Container::Vector<uint64>);};		};
			class VariableTypeArrayInt08 : public VariableType { public:		VariableTypeArrayInt08(const String& name, const bool readOnly) : VariableType(Network::Message::StreamIDs(Network::Message::STREAM_TYPE_INT08 | Network::Message::STREAM_TYPE_ARRAY), name, readOnly) {};					Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Container::Vector<int08>);};		};
			class VariableTypeArrayInt16 : public VariableType { public:		VariableTypeArrayInt16(const String& name, const bool readOnly) : VariableType(Network::Message::StreamIDs(Network::Message::STREAM_TYPE_INT16 | Network::Message::STREAM_TYPE_ARRAY), name, readOnly) {};					Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Container::Vector<int16>);};		};
			class VariableTypeArrayInt32 : public VariableType { public:		VariableTypeArrayInt32(const String& name, const bool readOnly) : VariableType(Network::Message::StreamIDs(Network::Message::STREAM_TYPE_INT32 | Network::Message::STREAM_TYPE_ARRAY), name, readOnly) {};					Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Container::Vector<int32>);};		};
			class VariableTypeArrayInt64 : public VariableType { public:		VariableTypeArrayInt64(const String& name, const bool readOnly) : VariableType(Network::Message::StreamIDs(Network::Message::STREAM_TYPE_INT64 | Network::Message::STREAM_TYPE_ARRAY), name, readOnly) {};					Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Container::Vector<int64>);};		};
			class VariableTypeArrayFloat32 : public VariableType { public:		VariableTypeArrayFloat32(const String& name, const bool readOnly) : VariableType(Network::Message::StreamIDs(Network::Message::STREAM_TYPE_FLOAT32 | Network::Message::STREAM_TYPE_ARRAY), name, readOnly) {};				Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Container::Vector<float32>);};		};
			class VariableTypeArrayFloat64 : public VariableType { public:		VariableTypeArrayFloat64(const String& name, const bool readOnly) : VariableType(Network::Message::StreamIDs(Network::Message::STREAM_TYPE_FLOAT64 | Network::Message::STREAM_TYPE_ARRAY), name, readOnly) {};				Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Container::Vector<float64>);};		};
			class VariableTypeArrayBString : public VariableType { public:		VariableTypeArrayBString(const String& name, const bool readOnly) : VariableType(Network::Message::StreamIDs(Network::Message::STREAM_TYPE_BSTRING | Network::Message::STREAM_TYPE_ARRAY), name, readOnly) {};				Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Container::Vector<BString>);};		};
			class VariableTypeArrayWString : public VariableType { public:		VariableTypeArrayWString(const String& name, const bool readOnly) : VariableType(Network::Message::StreamIDs(Network::Message::STREAM_TYPE_WSTRING | Network::Message::STREAM_TYPE_ARRAY), name, readOnly) {};				Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Container::Vector<WString>);};		};
			class VariableTypeArrayVec2f : public VariableType { public:		VariableTypeArrayVec2f(const String& name, const bool readOnly) : VariableType(Network::Message::StreamIDs(Network::Message::STREAM_TYPE_VEC_2_FLOAT32 | Network::Message::STREAM_TYPE_ARRAY), name, readOnly) {};			Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Container::Vector<Math::Vec2f>);};	};
			class VariableTypeArrayVec2d : public VariableType { public:		VariableTypeArrayVec2d(const String& name, const bool readOnly) : VariableType(Network::Message::StreamIDs(Network::Message::STREAM_TYPE_VEC_2_FLOAT64 | Network::Message::STREAM_TYPE_ARRAY), name, readOnly) {};			Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Container::Vector<Math::Vec2d>);};	};
			class VariableTypeArrayVec2i32 : public VariableType { public:		VariableTypeArrayVec2i32(const String& name, const bool readOnly) : VariableType(Network::Message::StreamIDs(Network::Message::STREAM_TYPE_VEC_2_INT32 | Network::Message::STREAM_TYPE_ARRAY), name, readOnly) {};			Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Container::Vector<Math::Vec2i32>);};	};
			class VariableTypeArrayVec2u32 : public VariableType { public:		VariableTypeArrayVec2u32(const String& name, const bool readOnly) : VariableType(Network::Message::StreamIDs(Network::Message::STREAM_TYPE_VEC_2_UINT32 | Network::Message::STREAM_TYPE_ARRAY), name, readOnly) {};		Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Container::Vector<Math::Vec2u32>);};	};
			class VariableTypeArrayVec3f : public VariableType { public:		VariableTypeArrayVec3f(const String& name, const bool readOnly) : VariableType(Network::Message::StreamIDs(Network::Message::STREAM_TYPE_VEC_3_FLOAT32 | Network::Message::STREAM_TYPE_ARRAY), name, readOnly) {};			Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Container::Vector<Math::Vec3f>);};	};
			class VariableTypeArrayVec3d : public VariableType { public:		VariableTypeArrayVec3d(const String& name, const bool readOnly) : VariableType(Network::Message::StreamIDs(Network::Message::STREAM_TYPE_VEC_3_FLOAT64 | Network::Message::STREAM_TYPE_ARRAY), name, readOnly) {};			Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Container::Vector<Math::Vec3d>);};	};
			class VariableTypeArrayVec3i32 : public VariableType { public:		VariableTypeArrayVec3i32(const String& name, const bool readOnly) : VariableType(Network::Message::StreamIDs(Network::Message::STREAM_TYPE_VEC_3_INT32 | Network::Message::STREAM_TYPE_ARRAY), name, readOnly) {};			Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Container::Vector<Math::Vec3i32>);};	};
			class VariableTypeArrayVec3u32 : public VariableType { public:		VariableTypeArrayVec3u32(const String& name, const bool readOnly) : VariableType(Network::Message::StreamIDs(Network::Message::STREAM_TYPE_VEC_3_UINT32 | Network::Message::STREAM_TYPE_ARRAY), name, readOnly) {};		Reflection::Datatype::DatatypeID getDatatypeID() const { return GET_DATATYPE_ID(Container::Vector<Math::Vec3u32>);};	};
		}
	}
}

