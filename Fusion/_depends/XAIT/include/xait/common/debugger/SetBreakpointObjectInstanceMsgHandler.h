// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/debugger/Server.h>
#include <xait/common/network/message/MessageHandler.h>
#include <xait/common/debugger/ObjectType.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief Message handler handling MESSAGE_SET_BREAKFLAG_OBJECT_INSTANCE.
			//!
			//! Handles the client request asking the server to set a breakflag for an object instance.
			//! All breakpoints this object instance enters will be holding.
			//! Has to be registered with the server.
			//! \see Server
			class SetBreakpointObjectInstanceMsgHandler : public Network::Message::MessageHandler
			{
#define MSGHANDLER_SETBREAKPOINTOBJECTINSTANCE_PARAMETER	"setBreakpointObjectInstanceMessage: { uint32:ModuleID, uint32:ObjectInstanceID, bool:newValue }"

			public:
				//! \brief Constructor.
				//! \param server	the server allows access to the registered modules
				SetBreakpointObjectInstanceMsgHandler( Server* server );

				//! \brief Handles the message by reading the data with the MessageReader.
				//! \param msgReader	the reader containing the message
				//! \param connectionID	the identifier of the client the message is from
				//! \returns true, if the message was handled correctly, and false otherwise
				bool handleMessage(Network::Message::MessageReader* msgReader, const uint32 connectionID );

			protected:

			private:
				//! \brief Reads the message parameters from the stream.
				//! \param msgReader the message reader
				bool getParameters(Network::Message::MessageReader* msgReader);

				//! \brief Sends the reply to the given connection ID.
				//! \param connectionID the connection ID
				//! \return true, if sending the reply was successful, and false otherwise
				bool reportReply( const uint32 connectionID );


				Server*		mServer;			//!< the server
				uint32	mModuleID;				//!< the ID of the respective module
				uint32	mObjectInstanceID;		//!< the ID of the object instance
				bool	mNewValue;
			};

		}
	}
}
