// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/reflection/function/FunctionRegistrar.h>
#include <xait/common/parser/formula/VariableAccessor.h>
#include <xait/common/debugger/ObjectInstance.h>


namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief This class enables the debugger to access variables.
			class DebuggerVariableAccessor : public Parser::Formula::VariableAccessor
			{
			public:
				//! \brief Constructor.
				//! \param objInstance the object instance to which this variable accessor is bound to
				//! \param functionRegistrar the function registrar
				DebuggerVariableAccessor(ObjectInstance* objInstance, Reflection::Function::FunctionRegistrar* functionRegistrar=NULL);

				//! \brief Destructor.
				XAIT_COMMON_DLLAPI virtual ~DebuggerVariableAccessor();

				//! \brief Gets the value of variable from the memory.
				//! \param value	the value is written to this variable
				//! \param varName	the name of the variable
				template<typename T_VALUE_TYPE>
				void getValue(T_VALUE_TYPE& value, const String& varName)
				{
					mObjectInstance->getVariableValue(value,varName);
				}

				//! \brief Gets the pointer to the variable value.
				//! \param varName the name of the variable
				//! \return a pointer to the value of the variable
				void* getValuePtr(const String& varName);

				//! \brief Gets the function registrar.
				//! \return the function registrar
				XAIT_COMMON_DLLAPI virtual Reflection::Function::FunctionRegistrar* getFunctionRegistrar() const;

				//! \brief Sets the function registrar.
				//! \param functionRegistrar the function registrar
				void setFunctionRegistrar(Common::Reflection::Function::FunctionRegistrar* functionRegistrar);

				//! \brief Gets the object instance.
				//! \return a pointer to the object instance
				ObjectInstance* getObjectInstance() const {return mObjectInstance;}

			protected:
			private:
				ObjectInstance*		mObjectInstance;	//!< the object instance to which this variable accessor is bound to
				Common::Reflection::Function::FunctionRegistrar*		mFunctionRegistrar;	 //!< the function registrar
			};

		}
	}
}
