// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/container/List.h>
#include <xait/common/debugger/ObjectType.h>
#include <xait/common/parser/formula/VariableResolver.h>


namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace Formula
			{
				class MathValue;
			}
		}



		namespace Debugger
		{
			//! \brief This class allows to create MathValues from variables defined in the corresponding ObjectType.
			class DebuggerVariableResolver : public Parser::Formula::VariableResolver
			{
			public:
				//! \brief Constructor.
				//! \param allocator a memory allocator
				//! \param objType the type of the object to which this resolver is bound to
				DebuggerVariableResolver(const Common::Memory::AllocatorPtr& allocator, ObjectType* objType);

				//! \brief Destructor.
				virtual ~DebuggerVariableResolver();

				//! \brief Creates an variable MathValue.
				//! \param expression		a XML node that contains the expression
				//! \param errorMsg			an error message container which stores errors that might perhaps occur
				//! returns the new Mathvalue, or NULL if name cannot be resolved
				virtual Common::Parser::Formula::MathValue* getVariableMathValue(Common::Parser::XML::XMLNodeElements* expression, Common::Parser::Formula::Expression::ErrorMsg& errorMsg);

				//! \brief creates an variable mathvalue
				//! \param reader			a binary reader
				//! \param errorMsg			an error message container which stores occuring errors
				//! returns the new Mathvalue; NULL if name cannot be resolved
				virtual Common::Parser::Formula::MathValue* getVariableMathValueBinary(Common::IO::BinaryReader* reader, Common::Parser::Formula::Expression::ErrorMsg& errorMsg);

				//! \brief Creates an membervariable MathValue.
				//! \param expression		a XML node that contains the expression
				//! \param errorMsg			an error message container which stores errors that might perhaps occur
				//! returns the new Mathvalue, or NULL if name cannot be resolved
				virtual Common::Parser::Formula::MathValue* getMemberVariableMathValue(Common::Parser::XML::XMLNodeElements* expression, Common::Parser::Formula::FunctionEnvironment* functionEnvironment, Common::Parser::Formula::Expression::ErrorMsg& errorMsg);
				
				//! \brief creates an membervariable mathvalue
				//! \param reader			a binary reader
				//! \param errorMsg			an error message container which stores occuring errors
				//! returns the new Mathvalue; NULL if name cannot be resolved
				virtual Common::Parser::Formula::MathValue* getMemberVariableMathValueBinary(Common::IO::BinaryReader* reader, Common::Parser::Formula::FunctionEnvironment* functionEnvironment, Common::Parser::Formula::Expression::ErrorMsg& errorMsg);
								
				//! \brief Creates an constant MathValue.
				//! \param expression		a XML node that contains the expression
				//! \param errorMsg			an error message container which stores errors that might perhaps occur
				//! returns the new Mathvalue, or NULL if name cannot be resolved
				virtual Common::Parser::Formula::MathValue* getConstantMathValue(Common::Parser::XML::XMLNodeElements* expression, Common::Parser::Formula::Expression::ErrorMsg& errorMsg);

				//! \brief creates an constant mathvalue
				//! \param reader			a binary reader
				//! \param errorMsg			an error message container which stores occuring errors
				//! returns the new Mathvalue; NULL if name cannot be resolved
				virtual Common::Parser::Formula::MathValue* getConstantMathValueBinary(Common::IO::BinaryReader* reader, Common::Parser::Formula::Expression::ErrorMsg& errorMsg);

				//! \brief Creates an custom MathValue.
				//! \param expression				a XML node that contains the expression
				//! \param functionEnvironment		the used function environment
				//! \param errorMsg					an error message container which stores errors that might perhaps occur
				//! returns the new Mathvalue, or NULL if name cannot be resolved
				virtual Common::Parser::Formula::MathValue* getCustomMathValue(Common::Parser::XML::XMLNodeElements* expression, Common::Parser::Formula::FunctionEnvironment* functionEnvironment, Common::Parser::Formula::Expression::ErrorMsg& errorMsg);


				//! \brief creates an custom mathvalue
				//! \param reader			a binary reader
				//! \param functionEnvironment		the used function environment
				//! \param errorMsg					an error message container which stores occuring errors
				//! returns the new Mathvalue; NULL if name cannot be resolved
				virtual Common::Parser::Formula::MathValue* getCustomMathValueBinary(Common::IO::BinaryReader* reader, Common::Parser::Formula::FunctionEnvironment* functionEnvironment, Common::Parser::Formula::Expression::ErrorMsg& errorMsg);


			protected:
				Common::Memory::AllocatorPtr	mAllocator; //!< the memory allocator

			private:
				ObjectType*		mObjectType; //!< the ObjectType to which this resolver is bound
			};
		}
	}
}
