// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/io/Stream.h>
#include <xait/common/debugger/MutexGate.h>
#include <xait/common/debugger/Server.h>


#define NETWORK_STREAM_MAX_TRAFFIC_PER_MESSAGE		64

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			#define INVALID_NETWORKSTREAM_ID		X_MAX_UINT32
			#define NETWORK_STREAM_SUPPORT_WRITE	false



			//! \brief Wrapper class for a memory efficient stream that contains network messages.
			class NetworkStream : public IO::Stream
			{
			public:
				//! \brief Constructor.
				//! \param server		the network server
				//! \param id			the id of this memory stream
				//! \param name			the name of this memory stream
				//! \param length		the initial length of the stream
				//! \param connectionID the connection ID
				NetworkStream( Server* server, const uint32 id, const Common::String& name, const uint32 length, const uint32 connectionID );

				//! \brief Destructor. Closes the stream on delete.
				virtual ~NetworkStream();

				// derived methods

				//! \brief Gets the name of the stream.
				//! \return the name
				virtual const Common::String& getName() const { return mName; };

				//! \brief Flag indicating whether this stream can be read.
				//! \return true, if the stream can be read, and false otherwise
				virtual bool canRead() const { return true; };

				//! \brief Flag indicating whether one can write to the stream.
				//! \return true, if one can write to the stream, and false otherwise
				virtual bool canWrite() const { return NETWORK_STREAM_SUPPORT_WRITE; };

				//! \brief Flag indicating whether one has random access to the stream
				//! \return true, if one has random access, and false otherwise
				virtual bool canSeek() const { return true; };

				//! \brief Flag indicating whether this stream is closed.
				//! \return true, if the stream is closed, and false otherwise
				virtual bool isClosed() const { return mIsClosed; };

				//! \brief Gets the length of the stream.
				//! \return the length of the stream
				virtual uint64 getLength() const { return (uint64)mLength; };

				//! \brief Gets the current position in the stream.
				//! \return the current position
				virtual uint64 getPosition() const { return (uint64)mPosition; };

				//! \brief Reads a certain number of bytes from the stream
				//! \param dstBuffer the buffer to which the bytes will be written
				//! \param numBytes the number of bytes that should be read
				//! \return the actual number of bytes that were read
				virtual uint64 read(void* dstBuffer, const uint64 numBytes);

				//! \brief Writes a certain number of bytes from a buffer to the stream
				//! \param srcBuffer the source buffer
				//! \param numBytes the number of bytes that should be written
				virtual void write(const void* srcBuffer, const uint64 numBytes);

				//! \brief Seeks a certain position in the stream.
				//! \param offset the position relative to the origin
				//! \param origin the origin of the search
				//! \see SeekOrigin
				virtual uint64 seek(const int64 offset, const SeekOrigin origin);

				//! \brief Flushes the stream.
				virtual void flush() {};

				//! \brief Closes the stream.
				virtual void close();


			private:
				friend class Server;

				//! \brief Updates the network stream with a data stream.
				//! \param dataStream the data stream
				//! \return true, if updating was successful, and false otherwise
				bool updateData( IO::Stream* dataStream );

				//! \brief Sends a network stream data request to the client
				//! \return true, if sending was successful, and false otherwise
				bool askData();

				Server*		mServer;			//!< the server
				uint32		mID;				//!< the ID of the stream
				Common::String		mName;		//!< the name of the stream
				uint32		mLength;			//!< the length of the stream
				uint32		mConnectionID;		//!< the connection ID

				int32		mPosition;			//!< the current position
				bool		mIsClosed;			//!< flag indicating whether the stream is closed

				IO::Stream*	mDataBuffer;		//!< the stream data
				uint32		mNumBytesNeeded;	//!< the number of bytes needed to read

				MutexGate		mMutexGate;		//!< the mutex gate synchronizing network reads
			};
		}
	}
}
