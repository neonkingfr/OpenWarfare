// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/memory/MemoryManaged.h>
#include <xait/common/String.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			class Module;
			class BreakpointInstance;
		}
	}
}

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
#define INVALID_BREAKPOINT_TYPE_ID		(uint32)-1

			//! \brief Class defining the type of a breakpoint.
			//!
			//! Breakpoint types can be used to group breakpoint instances according to some criterium.
			//! As an example, one breakpoint type can describe all precondition breakpoint instances,
			//! and another breakpoint type can describe all postcondition breakpoint instances.
			//! \see BreakpointInstance
			//! \see Module
			class BreakpointType : public Memory::MemoryManaged
			{
			public:
				//! \brief Gets the module to which this BreakpointType belongs to.
				//! \returns the module
				Module* getModule() const {return mModule;}

				//! \brief Gets the name of this BreakpointType.
				//! \returns the name
				const String& getName() const {return mName;}

				//! \brief Gets the ID of this BreakpointType.
				//! \returns the ID
				uint32 getID() const { return mID; }

				//! \brief Checks whether the breakpoint type is activated.
				//! \return true, if it is activated, and false otherwise
				XAIT_COMMON_DLLAPI bool isBreakflagActivated() const;


			private:
				friend class Module;
				friend class BreakpointInstance;

				//! \brief Constructor.
				//! \param name		the name of the BreakpointType
				//! \param module	pointer to the module to which this BreakpointType belongs to
				BreakpointType(const String& name,  Module* module);

				//! \brief Copy constructor.
				//! \param other	the other to copy from
				BreakpointType(const BreakpointType& other);

				//! \brief Destructor.
				virtual ~BreakpointType();

				//! \brief Initializes the BreakpointType ID.
				//!
				//! This function should only be called once. The ID must not be INVALID_BREAKPOINT_TYPE_ID.
				//! \param id	the ID
				void initID( const uint32 id );

				//! \brief Checks whether this BreakpointType is equal to another.
				//!
				//! BreakpointTypes from different modules are incomparable, and therefore not equal.
				//! \returns true, if this BreakpointType is equal to another, false else
				bool isEqual(const BreakpointType* other) const;

				//! \brief Sets the activated flag of breakpoint type.
				//! \param val the new value
				void setBreakpointActiveValue( const bool val ) { mIsBreakpointActivated = val; }

			//member
				String		mName;		//!< the name of the breakpoint type
				uint32		mID;		//!< the ID of the breakpoint type
				Module*		mModule;	//!< the module to which this breakpoint type belongs to
				bool		mIsBreakpointActivated; //!< flag indicating whether the breakpoint type is activated or not
			};
		}
	}
}
