// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/network/EventHandler.h>
#include <xait/common/container/List.h>

namespace XAIT
{
	namespace Common
	{
		namespace Network
		{
			class Reactor;
		}
	}
}

namespace XAIT
{
	namespace Common
	{
		namespace Network
		{
			class OutputHandler : public EventHandler
			{
			public:
				enum OutputHandlerState
				{
					OUTHANDLER_ERROR,
					OUTHANDLER_INIT,
					OUTHANDLER_WAITING,
					OUTHANDLER_SEND_LENGTH,
					OUTHANDLER_SEND_DATA,
				};

				//! \brief Destructor
				virtual ~OutputHandler();

				//! \brief adds a message stream this OutputHandler should send
				//! \param message	the data to send
				void sendMessage(IO::Stream* message);

			protected:
				//! \brief Constructor for sending data via socket
				//! \param socket	the socket to send to
				//! \param reactor	the reactor triggering this handler
				OutputHandler(Sockets::Socket* socket, Reactor* reactor);

				//! \brief deregisters this OutputHandler within the reactor after all messages are send
				void unregisterForWrite();

				OutputHandlerState		mState;
				Container::List<IO::Stream*>	mMessageStreams;
				Reactor*	mReactor;
			};

		} // Network
	} // Common
} // XAIT
