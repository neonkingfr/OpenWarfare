// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/memory/MemoryManaged.h>
#include <xait/common/io/Stream.h>
#include <xait/common/network/message/StreamIDs.h>
#include <xait/common/container/Vector.h>
#include <xait/common/platform/Endian.h>


namespace XAIT
{
	namespace Common
	{
		namespace Network
		{
			namespace Message
			{
				class MessageReader
				{
				public:
					//! \brief Constructor
					//! \param msgStream	stream to write the data to
					//! \param useType	defines whether the types should be added in front of each data
					//! \remark the current position of the stream is set to start s.t. the next read will retrieve the first bytes
					XAIT_COMMON_DLLAPI MessageReader(IO::Stream* msgStream, const bool useType=true);

					//! \Destructor
					XAIT_COMMON_DLLAPI ~MessageReader();

					//! \brief returns true if the stream is read completely
					//! \returns true if the stream is read completely
					XAIT_COMMON_DLLAPI bool isFinished();

					//! \brief converts the void* to the specified variable and reads its data from stream
					//! \param source	pointer to the variable
					//! \param type		the type the source should be converted to
					//! \returns false if the type isn't supported, else true
					XAIT_COMMON_DLLAPI bool read( void* source, const StreamIDs type );

					//! \brief converts the void* to the specified variable and reads its data from stream
					//! \param source	pointer to the variable
					//! \param type		the type the source should be converted to
					//! \param useType		determines whether the type have to be verified before reading
					//! \remark must be written with same useType, will be read independently of own useType
					//! \returns false if the type isn't supported, else true
					XAIT_COMMON_DLLAPI bool readForceUseType( void* source, const StreamIDs type, const bool useType );

					//! \brief converts the void* to the specified variable and reads its data from stream
					//! \param source	pointer to the variable
					//! \remark only valid if useType is enabled
					//! \returns false if the type isn't supported, else true
					XAIT_COMMON_DLLAPI bool read( void* source );

					XAIT_COMMON_DLLAPI bool readType(uint08& type);

					//! \brief reads a stream object out of internal stream
					//! \param object	the data that should be extracted from the stream
					//! \remark see IO::Stream::readToStream for exceptions
					XAIT_COMMON_DLLAPI bool readStream( IO::Stream* object);

					//! \brief reads pure data into another stream (use for copying)
					//! \param object	the data that should be extracted from the stream
					//! \param numBytes	the amount of bytes to read
					//! \remark see IO::Stream::readToStream for exceptions
					XAIT_COMMON_DLLAPI bool readToStream( IO::Stream* object, const uint64 numBytes);

					XAIT_COMMON_DLLAPI bool UseType() const { return mUseType; }
					XAIT_COMMON_DLLAPI void UseType(bool val) { mUseType = val; }

					//! \brief reads the data from the stream
					//! \param object	the data that should be extracted from the stream
					XAIT_COMMON_DLLAPI bool read(bool& object);
					XAIT_COMMON_DLLAPI bool read(uint08& object);
					XAIT_COMMON_DLLAPI bool read(uint16& object);
					XAIT_COMMON_DLLAPI bool read(uint32& object);
					XAIT_COMMON_DLLAPI bool read(uint64& object);
					XAIT_COMMON_DLLAPI bool read(int08& object);
					XAIT_COMMON_DLLAPI bool read(int16& object);
					XAIT_COMMON_DLLAPI bool read(int32& object);
					XAIT_COMMON_DLLAPI bool read(int64& object);
					XAIT_COMMON_DLLAPI bool read(float32& object);
					XAIT_COMMON_DLLAPI bool read(float64& object);
					XAIT_COMMON_DLLAPI bool read(BString& object);
					XAIT_COMMON_DLLAPI bool read(WString& object);
					XAIT_COMMON_DLLAPI bool read(Math::Vec2f& object);
					XAIT_COMMON_DLLAPI bool read(Math::Vec2d& object);
					XAIT_COMMON_DLLAPI bool read(Math::Vec2i32& object);
					XAIT_COMMON_DLLAPI bool read(Math::Vec2u32& object);
					XAIT_COMMON_DLLAPI bool read(Math::Vec3f& object);
					XAIT_COMMON_DLLAPI bool read(Math::Vec3d& object);
					XAIT_COMMON_DLLAPI bool read(Math::Vec3i32& object);
					XAIT_COMMON_DLLAPI bool read(Math::Vec3u32& object);
					XAIT_COMMON_DLLAPI bool read(StreamIDs& object);


					template <typename T> 
					bool read(Container::Vector<T>& object)
					{
						StreamIDs childID = getStreamID<T>();

						if (!verifyGivenType((StreamIDs)(STREAM_TYPE_ARRAY | childID)))
						{
							return false;
						}
						//get the number of children
						uint32 numChildren = 0;
						if(!readWithEndian(numChildren))
						{
							XAIT_FAIL("Couldn't read the size of the vector");
							return false;
						}
						object.resize(numChildren);

						//read in all children
						for (uint32 i=0; i<numChildren; i++)
						{
							T child = object[i];

							bool oldValue = mUseType;
							mUseType = false;	//!< don't use types for children
							bool success = read(child);
							mUseType = oldValue;

							if(!success)
							{
								XAIT_FAIL("Can't read the (" << i << ")th element from stream");
								return false;
							}
							object[i] = child;
						}
						return true;
					}

					XAIT_COMMON_DLLAPI IO::Stream* getStream() {return mStream;}

				private:
					//! \brief the reading from the stream have to care about the endianness
					template <typename T> 
					bool readWithEndian(T& object)
					{
						T objectBigEndian;
						uint32 numBytesToRead = sizeof(T);

						// read and check
						uint32 numBytesRead = (uint32)mStream->read(&objectBigEndian, numBytesToRead);
						if(numBytesRead != numBytesToRead)
							return false;

						//if reading successful convert
						Platform::Endian::convertBigToSystem(object,objectBigEndian);
						return true;
					}

					//! \brief read the number of child nodes for arrays/Strings
					//! \returns the number of children
					uint32 extractChildNumber();

					//! \brief verifies that the given type is equal to the type written in the stream
					//! \param expectedType		the typeID to check for
					//! \returns true, if the type could be verified else returns false
					//! \remark checks are only done if the the mUseType flag is set
					XAIT_COMMON_DLLAPI bool verifyGivenType(const StreamIDs& expectedType);


					IO::Stream*	mStream;	//!< stream to write to
					bool		mUseType;	//!< flag to use types for each data
				};
			}
		} // Network
	} // Common
} // XAIT

