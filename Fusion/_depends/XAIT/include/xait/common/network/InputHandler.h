// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/network/EventHandler.h>
#include <xait/common/network/message/Dispatcher.h>


namespace XAIT
{
	namespace Common
	{
		namespace Network
		{
			class InputHandler : public EventHandler
			{
			public:
				enum InputHandlerState
				{
					INHANDLER_ERROR,
					INHANDLER_INIT,
					INHANDLER_RECEIVE_LENGTH,
					INHANDLER_RECEIVE_DATA,
				};

				//! \brief Destructor
				virtual ~InputHandler();

			protected:
				//! \brief Constructor for receiving data via Socket
				//! \param socket	the socket to receive from
				//! \param dispatcher	pointer to the dispatcher to send the message to
				InputHandler(Sockets::Socket* socket, Message::Dispatcher* dispatcher);

				Message::Dispatcher*	mDispatcher;
				InputHandlerState		mState;
				IO::Stream*				mMessageStream;
			};

		} // Network
	} // Common
} // XAIT
