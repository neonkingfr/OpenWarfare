// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/platform/Time.h>
#include <xait/common/network/sockets/Socket.h>
#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/container/LookUp.h>
#include <xait/common/container/Set.h>
#include <xait/common/network/EventHandler.h>
#include <xait/common/network/OutputHandler.h>

namespace XAIT
{
	namespace Common
	{
		namespace Network
		{
			/*
			*/
			class Reactor : public Common::Memory::MemoryManaged
			{
			public:
				//! \brief Constructor
				Reactor(const Platform::MicroSeconds waitTimeMicroSeconds = Platform::MicroSeconds(100000) );

				//! \brief Destructor
				virtual ~Reactor();

				//! \brief calls the Reactor to check all connections once
				//! \returns 0, if no problem occurred
				int32 onTick();

				//! \brief returns the waiting time in micro seconds
				//! \returns the waiting time in micro seconds
				Platform::MicroSeconds getWaitTime() const { return mWaitTimeMicroSeconds; }

				//! \brief changes the waiting time in micro seconds
				//! \param waitTimeMicroSeconds		the new waiting time in micro seconds
				//void changeWaitTime(const Platform::MicroSeconds waitTimeMicroSeconds);
				
				//! \brief returns a new list of pointers to the open TCPSockets
				//! \returns a new list of pointers to the open TCPSockets
				Container::List<Sockets::Socket*> getConnectionListCopy() { return mConnections; }

				//! \brief returns a new list of pointers to the open TCPSockets and data needs to be send on
				//! \returns a new list of pointers to the open TCPSockets and data needs to be send on
				Container::List<Sockets::Socket*> getConnectionListCopyForWrite() { return mConnectionsForWrite; }

				//! \brief adds a new connection socket without registering an EventHandler
				//! \param connection	the new connection Socket
				//! \returns true, if the Socket wasn't already added else false
				bool addConnection(Sockets::Socket* connection);

				//! \brief notifies that the connection should be removed and all EventHandler responsible for this connection
				//! \param connection	the connection Socket
				//! \returns true, if the Socket was known within the reactor
				//! \remark can be called even from EventHandler that should be deleted because it is only notified -> deletion on next update step
				bool removeConnectionRequest(Sockets::Socket* connection);

				//! \brief registers an EventHandler for its Socket and its EventHandlerType
				//! \param handler	the EventHandler to register on its Socket
				//! \returns true, if the EventHandler was registered correctly, false if its Socket is already occupied
				//! \remarks if the Socket of the EventHandler is a NULL-Pointer he will be registered on special EventHandlerNoSocket list and its EventHandlerType will be ignored
				bool registerEventHandler(EventHandler* handler);

				//! \brief unregisters an EventHandler
				//! \param handler	the EventHandler to unregister
				//! \returns true, if the EventHandler was unregistered correctly, false if he wasn't registered before
				bool unregisterEventHandler(EventHandler* handler);

			private:
				friend class OutputHandler;

				//! \brief testing for dirty elements and deleting them
				//! \returns true if all dirty elements could be removed, false else
				bool checkDirty();

				//! \brief removes a connection socket and unregisters all EventHandler on this Socket
				//! \param connection	the connection Socket
				//! \returns true, if the Socket was known within the reactor
				bool removeConnection(Sockets::Socket* connection);

				//! \brief checks with a select call whether Sockets allow read/write/error functionality and calls the corresponding EventHandler
				bool checkExistingConnections();

				//! \brief calls all EventHandler that were registered without binding to a Socket
				bool checkSpecialHandler();

				//! \brief calls for a given list of Sockets the registered EventHandler for write functionality
				//! \param connectionList	the list of Sockets select function allows write functionality
				bool checkHandlerWrite(const Container::List<Sockets::Socket*>& connectionList);

				//! \brief calls for a given list of Sockets the registered EventHandler for read functionality
				//! \param connectionList	the list of Sockets select function allows read functionality
				bool checkHandlerRead(const Container::List<Sockets::Socket*>& connectionList);

				//! \brief calls for a given list of Sockets the registered EventHandler for error functionality
				//! \param connectionList	the list of Sockets select function allows error functionality
				bool checkHandlerError(const Container::List<Sockets::Socket*>& connectionList);

				//! \brief finds and calls the matching EventHandler to the given Socket list
				//! \param connectionList	the list of Sockets to find the EventHandler
				//! \param handlerList	the list of EventHandler to match to the Socket list and call once
				bool checkHandler(const Container::List<Sockets::Socket*>& connectionList, const Container::LookUp<Sockets::Socket*, EventHandler*>& handlerList);

				//! \brief finds a registered EventHandler for read functionality on Socket
				//! \param socket	the connection Socket to find the registered EventHandler
				//! \param handler	the EventHandler that was registered on the given Socket
				//! \returns true, if an EventHandler was found
				bool getRegisteredEventHandlerRead(EventHandler*& handler, Sockets::Socket* const socket);

				//! \brief finds a registered EventHandler for write functionality on Socket
				//! \param socket	the connection Socket to find the registered EventHandler
				//! \param handler	the EventHandler that was registered on the given Socket
				//! \returns true, if an EventHandler was found
				bool getRegisteredEventHandlerWrite(EventHandler*& handler, Sockets::Socket* const socket);

				//! \brief finds a registered EventHandler for error functionality on Socket
				//! \param socket	the connection Socket to find the registered EventHandler
				//! \param handler	the EventHandler that was registered on the given Socket
				//! \returns true, if an EventHandler was found
				bool getRegisteredEventHandlerError(EventHandler*& handler, Sockets::Socket* const socket);

				//! \brief registers an EventHandler for its Socket and will be called if select on read succeeded
				//! \param handler	the EventHandler to register on its Socket
				//! \returns true, if the EventHandler was registered correctly, false if its Socket is already occupied
				//! \remarks if the Socket of the EventHandler is a NULL-Pointer he woun't be registered on special EventHandler list
				bool registerEventHandlerRead(EventHandler* handler);

				//! \brief registers an EventHandler for its Socket and will be called if select on write succeeded
				//! \param handler	the EventHandler to register on its Socket
				//! \returns true, if the EventHandler was registered correctly, false if its Socket is already occupied
				//! \remarks if the Socket of the EventHandler is a NULL-Pointer he woun't be registered on special EventHandler list
				bool registerEventHandlerWrite(EventHandler* handler);

				//! \brief registers an EventHandler for its Socket and will be called if select on error succeeded
				//! \param handler	the EventHandler to register on its Socket
				//! \returns true, if the EventHandler was registered correctly, false if its Socket is already occupied
				//! \remarks if the Socket of the EventHandler is a NULL-Pointer he woun't be registered on special EventHandler list
				bool registerEventHandlerError(EventHandler* handler);

				//! \brief registers an EventHandler that has no Socket - will be called each time Reactor is called
				//! \param handler	the EventHandler to register
				void registerEventHandlerNoSocket(EventHandler* handler);

				//! \brief unregisters an EventHandler within the HandlerRead LookUp
				//! \param handler	the EventHandler to unregister
				//! \returns true, if the EventHandler was unregistered correctly, false if he wasn't registered
				bool unregisterEventHandlerRead(EventHandler* handler);

				//! \brief unregisters an EventHandler within the HandlerWrite LookUp
				//! \param handler	the EventHandler to unregister
				//! \returns true, if the EventHandler was unregistered correctly, false if he wasn't registered
				bool unregisterEventHandlerWrite(EventHandler* handler);

				//! \brief unregisters an EventHandler within the HandlerError LookUp
				//! \param handler	the EventHandler to unregister
				//! \returns true, if the EventHandler was unregistered correctly, false if he wasn't registered
				bool unregisterEventHandlerError(EventHandler* handler);

				//! \brief unregisters an EventHandler within the HandlerNoSocket LookUp
				//! \param handler	the EventHandler to unregister
				//! \returns true, if the EventHandler was unregistered correctly, false if he wasn't registered
				bool unregisterEventHandlerNoSocket(EventHandler* handler);

				//! \brief remarks that an Outhandler has data to write on Socket
				//! \param socket	the connection Socket to send data on
				void registerEventHandlerWaitingToWrite(Sockets::Socket* const socket);

				//! \brief remarks that an Outhandler has no more data to write on Socket
				//! \param socket	the connection Socket
				void unregisterEventHandlerWaitingToWrite(Sockets::Socket* const socket);


				Platform::MicroSeconds			mWaitTimeMicroSeconds;			//!< time in microseconds to use for waiting
				
				Container::List<EventHandler*>	mHandlerNoSocket;	//!< list of EventHandler that cannot be combined with select functionality on a Socket
				Container::List<Sockets::Socket*>	mConnections;					//!< list of all open connections via Sockets
				Container::Set<Sockets::Socket*>	mConnectionsDirty;				//!< list of connections that should be deleted on next update step
				Container::List<Sockets::Socket*>	mConnectionsForWrite;					//!< list of all open connections via Sockets


				Container::LookUp<Sockets::Socket*, EventHandler*>	mHandlerRead;	//!< list of registered EventHandler for read on socket
				Container::LookUp<Sockets::Socket*, EventHandler*>	mHandlerWrite;	//!< list of registered EventHandler for write on socket
				Container::LookUp<Sockets::Socket*, EventHandler*>	mHandlerError;	//!< list of registered EventHandler for error on socket

			};

		} // Network
	} // Common
} // XAIT
