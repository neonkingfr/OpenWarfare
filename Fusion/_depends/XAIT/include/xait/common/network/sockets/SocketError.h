// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/network/NetworkPlatform.h>
#include <xait/common/String.h>
#include <xait/common/utility/EnumEncapsulate.h>

namespace XAIT
{
	namespace Common
	{
		namespace Network
		{
			namespace Sockets
			{
				struct SocketError
				{
					enum Enum
					{
						ERR_NOERROR				= _SOCKET_ERR_NOERROR,
						ERR_NOTINITIALISED		= _SOCKET_ERR_NOTINITIALISED,
						ERR_ENETDOWN			= _SOCKET_ERR_ENETDOWN,
						ERR_EADDRINUSE			= _SOCKET_ERR_EADDRINUSE,
						ERR_EINTR				= _SOCKET_ERR_EINTR,
						ERR_EINPROGRESS			= _SOCKET_ERR_EINPROGRESS,
						ERR_EALREADY			= _SOCKET_ERR_EALREADY,
						ERR_EADDRNOTAVAIL		= _SOCKET_ERR_EADDRNOTAVAIL,
						ERR_EAFNOSUPPORT		= _SOCKET_ERR_EAFNOSUPPORT,
						ERR_ECONNREFUSED		= _SOCKET_ERR_ECONNREFUSED,
						ERR_EFAULT				= _SOCKET_ERR_EFAULT,
						ERR_EINVAL				= _SOCKET_ERR_EINVAL,
						ERR_EISCONN				= _SOCKET_ERR_EISCONN,
						ERR_ENETUNREACH			= _SOCKET_ERR_ENETUNREACH,
						ERR_ENOBUFS				= _SOCKET_ERR_ENOBUFS,
						ERR_ENOTSOCK			= _SOCKET_ERR_ENOTSOCK,
						ERR_ETIMEDOUT			= _SOCKET_ERR_ETIMEDOUT,
						ERR_EACCES				= _SOCKET_ERR_EACCES,
					};

					Common::String getErrorText();

					STRUCT_TO_ENUM_MEMBERS(SocketError,Enum,ERR_NOERROR);
				};
			}
		}
	}
}

