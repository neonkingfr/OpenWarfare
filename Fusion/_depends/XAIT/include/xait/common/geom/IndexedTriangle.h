// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/FundamentalTypes.h>

namespace XAIT
{
	namespace Common
	{
		namespace Geom
		{
			struct IndexedTriangle
			{
				int32	mVert[3];

				IndexedTriangle()
				{
					mVert[0]= -1;
					mVert[1]= -1;
					mVert[2]= -1;
				}

				IndexedTriangle(const int32 a, const int32 b, const int32 c)
				{
					mVert[0]= a;
					mVert[1]= b;
					mVert[2]= c;
				}
			};
		}
	}
}
