// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/math/Vec3.h>
#include <xait/common/debug/Assert.h>

namespace XAIT
{
	namespace Common
	{
		namespace Geom
		{
			//! \brief template struct for ray (use with float32 or float64)
			template<typename T_OBJECTID, typename T_COMP = float32>
			struct Ray3D 
			{
				Math::Vec3<T_COMP>	mStart;		//!< start point of ray
				Math::Vec3<T_COMP>	mEnd;		//!< end point of ray
				T_COMP				mT;			//!< value between 0 .. 1 which specifies the part of ray until it hits something
				T_OBJECTID			mObjectID;	//!< object that has been hit. only valid if t < 1

				//! \brief constructor
				Ray3D()
					: mT((T_COMP)1)
				{}

				//! \brief constructor
				//! \param start	ray origin
				//! \param dir		ray direction (normalized vector)
				//! \param length	ray length
				Ray3D(const Math::Vec3<T_COMP>& start, const Math::Vec3<T_COMP>& dir, const float32& length)
					: mStart(start)
					, mEnd(start + dir * length)
					, mT((T_COMP)1)
				{
					X_ASSERT_MSG_DBG(dir.isNormalized(),"ray direction vector must be normalized");
				}


				//! brief constructor takes start point and endpoint
				//! \param start	start of ray
				//! \param end		end of ray
				Ray3D(const Math::Vec3<T_COMP>& start, const Math::Vec3<T_COMP>& end)
					: mStart(start)
					, mEnd(end)
					, mT((T_COMP)1)
				{
				}

				//! \brief get hitpoint
				//! \remark Use this function only if there has been something hit, 
				//!			otherwise this method will return nonsense.
				Math::Vec3<T_COMP> getHitPoint() const
				{
					return mStart + (mEnd - mStart) * mT;
				}

				//! \brief Set the distance of an hit from start point
				//! \param hitDistance	distance from start point in world units
				//! \remark This modifies only mT
				void setHitDistance(const T_COMP hitDistance)
				{
					const T_COMP fullLength= (mEnd - mStart).getLength();
					mT= hitDistance / fullLength;
				}

				//! \brief get direction of ray
				Math::Vec3<T_COMP> getDirection() const
				{
					Math::Vec3<T_COMP> dir= mEnd - mStart;
					dir.normalize();
					return dir;
				}

				//! \brief get length of the ray as 2D vector
				Math::Vec3<T_COMP> getLength3D() const
				{
					return (mEnd - mStart) * mT;
				}

				//! \brief get length of the ray till the hit point
				//! \remark Use this function only if there has been something hit, 
				//!			otherwise this method will return nonsense.
				T_COMP getLength() const
				{
					return (mEnd - mStart).getLength() * mT;
				}

				//! \brief get a point on this ray
				//! \param	distance	the distance of the point from start coordinate in world units
				//!\remark this point can be behind the end point
				Math::Vec3<T_COMP> getPointOnRay(const float32& distance) const
				{
					Math::Vec3<T_COMP> dir= mEnd - mStart;
					dir.normalize();

					return mStart + dir * distance;
				}
			};	// struct Ray
		}	// namespace Geom
	}	// namespace Common
}	// namespace XAIT


