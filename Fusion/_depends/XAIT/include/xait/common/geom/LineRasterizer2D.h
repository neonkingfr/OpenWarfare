// (C) xaitment GmbH 2006-2012

#ifndef _BRESENHAM_H_
#define _BRESENHAM_H_

#include <xait/common/math/Vec2.h>
#include <xait/common/container/Vector.h>

// ---------------------------------------------------------------------------------------------------------
//							Bresenham line drawing (and rasterization) algorithm
//							-actually it's the midpoint algorithm  
//							(Pitteway, M.L.V., "Algorithms for Drawing Ellipses or Hyperbolae
//							with a digital plotter", Computer J., 10(3), November 1967, 282-289)
//							but the midpoint algorithm reduces for line to the Bresenham algorithm-
//							Please note that the grid size is assumed to be 1 in each direction
//
//
//	Author:		Andreas Kleer
//
// ---------------------------------------------------------------------------------------------------------


namespace XAIT
{
	namespace Common
	{
		namespace Geom
		{

			//! \brief Bresenham line drawing (and rasterization) algorithm
			class LineRasterizer2D
			{
			public:

				//! \brief Constructor
				LineRasterizer2D()
				{};

				//! \brief initialize the line to be drawn/rasterized
				//! \param start the starting point of the line in grid coordinates
				//! \param end the end point of the line in grid coordinates
				void init(const Math::Vec2i32& start,const Math::Vec2i32& end);


				//! \brief computes all grid cells between start and end (see init method)
				//! \param gridCells	the grid cells between [start;end] (output parameter), results will be appended
				void getGridCells(Container::Vector<Math::Vec2i32>& gridCells);

				//! \brief computes the next grid cell (instead of resturning all cells as getGridCells)
				//! \note the starting point (start) is NOT returned. The first point you'll get is the one after start
				//! \param the next grid cell, gridCell is undefined if false is returned (output parameter)
				//! \return true if there's a next cell, false if the end cell is reached
				bool getNextGridCell(Math::Vec2i32& gridCell);

			private:
				Math::Vec2i32	mStart;		//!< start point of the line (this value may be changed during calls to getNextGridCell)
				Math::Vec2i32 mDelta;
				Math::Vec2i32 mSign;
				int32 mMaxDist;
				int32 mStep;
				Math::Vec2i32 mError;

			};	// class Bresenham
		}	// namespace Geom
	}	// namespace Common
}	// namespace XAIT

#endif
