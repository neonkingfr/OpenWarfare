// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/memory/MemoryManaged.h>

namespace XAIT
{
	namespace Common
	{
		namespace Threading
		{
			class ThreadPool;

			//! \brief Base class for tasks
			class Task : public Memory::MemoryManaged
			{
			public:
				Task()
					: mRunning(false),mFinished(false)
				{}

				//! \brief start the execution of the task
				//! \param tls	set the thread local storage which can be used inside main
				inline void run(ThreadPool* threadPool, const Common::Memory::Allocator::Ptr& tls)
				{
					mTLS= tls;
					mThreadPool= threadPool;
					mFinished= false;
					mRunning= true;
					main();
					mRunning= false;
					mFinished= true;
				}

				//! \brief Get the running state of the task
				//! \returns True while the task is processing its work, False otherwise.
				inline bool isRunning() const
				{
					return mRunning;
				}

				//! \brief Get the finished state of the task
				//! \returns True if the task has been finished, false otherwise.
				//! \remark If the task gets started a second time the finished flag will be set to false at the
				//!			start of the second execution.
				inline bool isFinished() const
				{
					return mFinished;
				}

				//! \brief get the threadpool running this task
				inline ThreadPool* getThreadPool() const
				{
					return mThreadPool;
				}

			protected:
				//! \brief Main method. Implement this for the work being done inside the thread.
				virtual void main()= 0;

				//! \brief get thread local storage
				//! \remark This is only available inside the main function !!!
				inline const Memory::Allocator::Ptr& getTLS() const
				{
					return mTLS;
				}

			private:
				volatile bool			mRunning;
				volatile bool			mFinished;
				Memory::Allocator::Ptr	mTLS;
				ThreadPool*				mThreadPool;
			};
		}
	}
}
