// (C) xaitment GmbH 2006-2012

#pragma once 
#include <xait/common/DLLDefines.h>
#include <xait/common/FundamentalTypes.h>

#if XAIT_OS == XAIT_OS_WIN

#include <windows.h>

// we use the critical section from win32 since this seems to be a
// spinlock mechanism
#define XAIT_SPINLOCK_HANDLE_DEF()	typedef CRITICAL_SECTION	SpinLockOSHandle;

#elif XAIT_OS == XAIT_OS_XBOX360

#include <xtl.h>
#define XAIT_SPINLOCK_HANDLE_DEF()	typedef CRITICAL_SECTION	SpinLockOSHandle;

#elif XAIT_OS == XAIT_OS_XBOX360EXT

#include <xdk.h>
#include <wrl.h>
#define XAIT_SPINLOCK_HANDLE_DEF()	typedef CRITICAL_SECTION	SpinLockOSHandle;

#elif XAIT_OS == XAIT_OS_PS3
#define XAIT_SPINLOCK_HANDLE_DEF()	typedef int					SpinLockOSHandle;

#elif XAIT_OS == XAIT_OS_PS4
#include <pthread.h>
#define XAIT_SPINLOCK_HANDLE_DEF()	typedef pthread_mutex_t		SpinLockOSHandle;

#elif XAIT_OS == XAIT_OS_WII

#include <revolution/os.h>
#define XAIT_SPINLOCK_HANDLE_DEF()	typedef OSMutex				SpinLockOSHandle;

#elif XAIT_OS == XAIT_OS_LINUX

#include <pthread.h>
#define XAIT_SPINLOCK_HANDLE_DEF()	typedef pthread_spinlock_t	SpinLockOSHandle;

#elif XAIT_OS == XAIT_OS_MAC

#include <pthread.h>
//! Mutex on OSX tries first spinlock and uses real mutex as fallback
//! see http://attractivechaos.wordpress.com/2011/10/06/multi-threaded-programming-efficiency-of-locking/
#define XAIT_SPINLOCK_HANDLE_DEF()	typedef pthread_mutex_t	SpinLockOSHandle;

#else
#error "Unknow OS"
#endif


namespace XAIT
{
	namespace Common
	{
		namespace Threading
		{
			XAIT_SPINLOCK_HANDLE_DEF();

			//! The spin lock class tries todo a spinning lock on every platform.
			//! Currently there is no support on wii and osx so a normal mutex will be used.
			//! Spinning locks should be used in places where only a small area should
			//! be secured against data race. The advantage of a spinning lock against
			//! a normal mutex is, that it does not give the thread scheduling slice 
			//! back to the scheduler and increases this way the performance if the lock
			//! is only locked for a very short time.
			//! \brief Spinlock Class
			class XAIT_COMMON_DLLAPI SpinLock
			{
			public:
				//! \brief Creates a new spinlock
				//! \param spinCount	specifies the amount of spin before the lock uses a semaphore. This is a win32 only behaviour, other platforms ignore this.
				SpinLock(const uint32 spinCount= 4000);

				//! \brief Destructs the spinlock to the os
				~SpinLock();

				//! \brief Gets the ownership of the lock. If the lock has already an owner
				//!		the function will block until the lock is free.
				//! \remark Spinning locks cannot be applied recursively as for the normal mutex.
				void lock();

				//! \brief Try to get a lock.
				//! \returns true if the lock could be aquired or false if already locked.
				//! \remark Spinning locks cannot be applied recursively as for the normal mutex.
				bool tryLock();

				//! \brief unlock a previously locked spinning lock
				void unlock();

			private:
				SpinLockOSHandle	mHandle;
			};
		}
	}
}
