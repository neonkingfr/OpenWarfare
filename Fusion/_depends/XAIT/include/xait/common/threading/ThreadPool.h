// (C) xaitment GmbH 2006-2012

#pragma once 
#include <xait/common/threading/Task.h>
#include <xait/common/threading/Thread.h>
#include <xait/common/threading/Threaded.h>
#include <xait/common/threading/SpinLock.h>
#include <xait/common/threading/Semaphore.h>
#include <xait/common/container/Queue.h>
#include <xait/common/memory/MemoryManaged.h>

// global compile defines
//#define XAIT_THREADPOOL_DISABLE_THREADING	// disables complete threading and executes tasks directly in add/prependTask


namespace XAIT
{
	namespace Common
	{
		namespace Threading
		{
			class ThreadPool;

			//! \brief worker which will fetches itself the next task
			class Worker : public Threaded
			{
			public:
				//! \brief Constructor
				//! \param tPool	thread pool which created the memory pool
				//! \param tls		thread local storage
				Worker(ThreadPool* tPool, const Memory::Allocator::Ptr& tls);

				//! \brief blocks until the worker has no more tasks.
				//! \remark The worker is not idle between switching tasks
				inline void waitForIdle()
				{
					mIdle.wait();
					mIdle.signal();
				}

			private:
				ThreadPool*				mThreadPool;
				Semaphore				mIdle;
				Memory::Allocator::Ptr	mTLS;

				int32 main();
			};

			//! \brief A simple thread pool with fifo scheduling implementation
			class ThreadPool : public Memory::MemoryManaged
			{
			public:
				//! \brief Create a new thread pool
				//! \param alloc			allocator used for the object creation, if the allocator is not thread safe it will be wrapped in a thread safe one
				//! \param numThreads		Number of threads to create. If thread count is 0 the pool allocates as many threads as there are logical processors.
				//! \param maxTaskCount		Maximum number of tasks until add task will block. 0 means auto (double the amount of threads)
				XAIT_COMMON_DLLAPI static ThreadPool* createThreadPool(const Memory::Allocator::Ptr& alloc,const uint32 numThreads= 0, const uint32 maxTaskCount= 0);

				//! \brief Destroy Pool and wait for pending task
				XAIT_COMMON_DLLAPI ~ThreadPool();

				//! \brief Get an allocator that is thread safe
				inline const Memory::Allocator::Ptr& getThreadSafeAllocator() const
				{
					return mAllocator;
				}

				//! \brief get number of threads
				inline uint32 getNumThreads() const
				{
					return mThreads.size();
				}

				//! \brief get the maximum task count
				//! \returns the maximum amount of task being scheduled (if infinite value is set to MAX_UINT32)
				inline uint32 getMaxTaskCount() const
				{
					return mNumMaxTasks;
				}

				//! \brief wait for all tasks being finished
				XAIT_COMMON_DLLAPI void waitForTasksFinished();

				//! \brief append a task at the end of the queue
				XAIT_COMMON_DLLAPI void appendTask(Task* task);

				//! \brief add the task in front of the queue
				//! \remark Since the scheduling is done with fifo, the task will be executed next time a thread is available.
				XAIT_COMMON_DLLAPI void prependTask(Task* task);

				//! \brief clear the task list
				XAIT_COMMON_DLLAPI void clearTasks();

			private:
				friend class Worker;

				SpinLock							mTaskQueueLock;
				Container::List<Task*>				mTaskQueue;		//!< task queue 
				Semaphore							mTaskCount;		//!< semaphore with task count

				uint32								mNumMaxTasks;	//!< maximum number of task which can be hold by the queue
				Semaphore							mMaxTasks;

				Container::Vector<Thread*>			mThreads;
				Container::Vector<Worker*>			mWorker;

				//! \brief Create a new thread pool
				//! \param numThreads		Number of threads to create. If thread count is 0 the pool allocates as many threads as there are logical processors.
				//! \param maxTaskCount		Maximum number of tasks until add task will block. 0 means auto (double the amount of threads)
				ThreadPool(const uint32 numThreads= 0, const uint32 maxTaskCount= 0);

				//! \brief get the next task being processed
				Task* getNextTask();

			};
		}
	}
}
