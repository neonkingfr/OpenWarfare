// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/Platform.h>
#include <xait/common/FundamentalTypes.h>

//////////////////////////////////////////////////////////////////////////
// WIN
//////////////////////////////////////////////////////////////////////////
#if XAIT_OS == XAIT_OS_WIN

#define XAIT_SEMAPHORE_HANDLE_DEF()	typedef HANDLE		SemaphoreOSHandle;

//////////////////////////////////////////////////////////////////////////
// LINUX
//////////////////////////////////////////////////////////////////////////
#elif XAIT_OS == XAIT_OS_LINUX

#include <semaphore.h>

#define XAIT_SEMAPHORE_HANDLE_DEF()	typedef sem_t		SemaphoreOSHandle;

//////////////////////////////////////////////////////////////////////////
// MACOS
//////////////////////////////////////////////////////////////////////////
#elif XAIT_OS == XAIT_OS_MAC

#include <semaphore.h>

#define XAIT_SEMAPHORE_HANDLE_DEF()	typedef sem_t*		SemaphoreOSHandle;

//////////////////////////////////////////////////////////////////////////
// PS3
//////////////////////////////////////////////////////////////////////////
#elif XAIT_OS == XAIT_OS_PS3

#include <sys/synchronization.h>

#define XAIT_SEMAPHORE_HANDLE_DEF()	typedef sys_semaphore_t	SemaphoreOSHandle;

//////////////////////////////////////////////////////////////////////////
// PS4
//////////////////////////////////////////////////////////////////////////
#elif XAIT_OS == XAIT_OS_PS4

#include <kernel/semaphore.h>

#define XAIT_SEMAPHORE_HANDLE_DEF()	typedef SceKernelSema		SemaphoreOSHandle;

//////////////////////////////////////////////////////////////////////////
// XBOX360
//////////////////////////////////////////////////////////////////////////
#elif XAIT_OS == XAIT_OS_XBOX360

#include <xtl.h>
#define XAIT_SEMAPHORE_HANDLE_DEF()	typedef HANDLE		SemaphoreOSHandle;

//////////////////////////////////////////////////////////////////////////
// XBOX360EXT
//////////////////////////////////////////////////////////////////////////
#elif XAIT_OS == XAIT_OS_XBOX360EXT

#include <xdk.h>
#include <wrl.h>
#define XAIT_SEMAPHORE_HANDLE_DEF()	typedef HANDLE		SemaphoreOSHandle;

//////////////////////////////////////////////////////////////////////////
// WII
//////////////////////////////////////////////////////////////////////////
#elif XAIT_OS == XAIT_OS_WII

#include <revolution/os.h>

#define XAIT_SEMAPHORE_HANDLE_DEF()	typedef OSSemaphore		SemaphoreOSHandle;

#endif



namespace XAIT
{
	namespace Common
	{
		namespace Threading
		{
#define XAIT_SEMAPHORE_MAX_COUNT		(X_MAX_INT32 - 1)

			XAIT_SEMAPHORE_HANDLE_DEF();

			//! \brief Semaphore class for thread synchronization
			class Semaphore
			{
			public:
				//! \brief Create a new semaphore
				//! \param initialCount		the amount of threads which can concurrently access the area protected by the semaphore
				//! \param maxCount			the maximum available count for this semaphore, will be checked on nearly all system except wii
				Semaphore(const int32 initialCount, const int32 maxCount= XAIT_SEMAPHORE_MAX_COUNT);

				//! \brief Create a new semaphore which is not initialized
				Semaphore();

				//! \brief Destructor
				~Semaphore();

				//! \brief initialise the semaphore
				void init(const int32 initialCount, const int32 maxCount= XAIT_SEMAPHORE_MAX_COUNT);

				//! \brief Wait for the semaphore. If there is no count available, this call blocks the calling thread.
				void wait();

				//! \brief Try to wait for the semaphore. (non-blocking)
				//! \returns True if there is a count available or false if not.
				bool tryWait();

				//! \brief Signal the semaphore (release the semaphore)
				void signal();

			private:
				SemaphoreOSHandle		mHandle;
#if XAIT_OS_MAC
                int mSemaphoreID;
#endif
			};
		}
	}
}
