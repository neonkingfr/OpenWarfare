// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/Platform.h>
#include <xait/common/DLLDefines.h>

#if XAIT_OS == XAIT_OS_WIN

#define MUTEX_HANDLE_DEFINE()	typedef CRITICAL_SECTION MutexOSHandle;

#elif XAIT_OS == XAIT_OS_XBOX360

#include <xtl.h>
#define MUTEX_HANDLE_DEFINE()	typedef CRITICAL_SECTION	MutexOSHandle;

#elif XAIT_OS == XAIT_OS_XBOX360EXT

#include <xdk.h>
#include <wrl.h>
#define MUTEX_HANDLE_DEFINE()	typedef CRITICAL_SECTION	MutexOSHandle;

#elif XAIT_OS == XAIT_OS_PS3

#include <sys/synchronization.h>

#define MUTEX_HANDLE_DEFINE()	typedef sys_mutex_t		MutexOSHandle;

#elif XAIT_OS == XAIT_OS_WII

#include <revolution/os.h>

#define MUTEX_HANDLE_DEFINE()	typedef OSMutex			MutexOSHandle;

#elif XAIT_OS == XAIT_OS_LINUX || XAIT_OS == XAIT_OS_MAC

#include <pthread.h>

#define MUTEX_HANDLE_DEFINE()	typedef pthread_mutex_t	MutexOSHandle;

#elif XAIT_OS == XAIT_OS_PS4

#include <kernel.h>

#define MUTEX_HANDLE_DEFINE()	typedef ScePthreadMutex	MutexOSHandle;

#endif



namespace XAIT
{
	namespace Common
	{
		namespace Threading
		{
			MUTEX_HANDLE_DEFINE();

			class Mutex
			{
			public:
				//! \brief Create a new initialized mutex
				XAIT_COMMON_DLLAPI Mutex();

				//! \brief Destructor
				XAIT_COMMON_DLLAPI ~Mutex();

				//! \brief Get a lock on the mutex. If the mutex is locked by another thread 
				//!		the function blocks the calling thread
				//! \remark If the calling thread has locked the mutex itself this function
				//!			does not block but remembers the lock in a lock count. So there
				//!			have to be the same amount of locks <-> unlocks to unlock the
				//!			mutex for other threads. This enables the use of the mutex
				//!			in recursive calls.
				XAIT_COMMON_DLLAPI void lock();

				//! \brief Try to get a lock on the mutex. If the mutex is locked the function return immediately with false.
				//! \return true if the mutex could be acquired or false if not.
				XAIT_COMMON_DLLAPI bool tryLock();

				//! \brief Unlocks a previously locked mutex.
				XAIT_COMMON_DLLAPI void unlock();

			private:
				MutexOSHandle	mHandle;		//!< handle to the os mutex
			};
		}
	}
}
