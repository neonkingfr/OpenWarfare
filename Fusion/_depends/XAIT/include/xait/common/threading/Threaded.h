// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/memory/MemoryManaged.h>

namespace XAIT
{
	namespace Common
	{
		namespace Threading
		{
			//! A base class for object related threading.
			//! If the thread gets started, the run method will be called
			class Threaded : public Common::Memory::MemoryManaged
			{
			public:
				Threaded()
					: mErrorCode(-1)
				{}

				virtual ~Threaded() {};

				//! \brief will be called to start the execution of the thread functions
				//! \returns The error code of the execution
				inline int32 run()
				{
					mErrorCode= main();
					return mErrorCode;
				}

				//! \brief get the errorcode of the last run
				//! \returns the error code of the last run. If no run executed the
				//!			default value is -1
				int32 getErrorCode() const
				{
					return mErrorCode;
				}

			protected:
				//! \brief will be executed if the thread is started.
				//! \returns The error code of the execution
				//! \remark This method runs already inside the thread
				virtual int32 main()= 0;

			private:
				int32		mErrorCode;
			};
		}
	}
}
