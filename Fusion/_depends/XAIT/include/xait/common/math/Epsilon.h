// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/FundamentalTypes.h>


namespace XAIT
{
	namespace Common
	{
		namespace Math
		{
			//! \brief Epsilon values for various numerical values
			template<typename T_COMP>
			struct Epsilon
			{
			};

			template<>
			struct Epsilon<float32>
			{
				inline static float32 value()
				{
					return 10e-6f;
				}
			};
			template<>
			struct Epsilon<float64>
			{
				inline static float64 value()
				{
					return 10e-11;
				}
			};

		}
	}
}


