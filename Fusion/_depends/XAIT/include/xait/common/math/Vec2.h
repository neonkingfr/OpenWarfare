// (C) xaitment GmbH 2006-2012

#pragma once
#ifndef _XAIT_COMMON_MATH_VEC2_H_
#define _XAIT_COMMON_MATH_VEC2_H_

#include <xait/common/FundamentalTypes.h>
#include <xait/common/math/ValueFunc.h>
#include <xait/common/math/MathDefines.h>
#include <xait/common/debug/Assert.h>

#include <iostream>

// ---------------------------------------------------------------------------------------------------------
//							General Template Class for Vector with 3 components
//
//	Author:		(probably) Markus Wilhelm
//  Edited by:  Andreas Kleer
//	Cleaned:	Markus Wilhelm
//
// ---------------------------------------------------------------------------------------------------------


namespace XAIT
{
	namespace Common
	{
		namespace Math
		{
			//! \brief template class for vectors with 2 components
			//! \param T_COMP	type of the components (must be a number type)
			template<typename T_COMP>
			struct Vec2
			{
				typedef T_COMP	ComponentType;

				T_COMP	mX;		//!< component in x
				T_COMP	mY;		//!< component in y

				//! \brief Default constructor
				//! \remark Does not init the vector (performance issue)
				Vec2()
				{}

				//! \brief Element constructor
				//! \param x	x component
				//! \param y	y component
				Vec2(const T_COMP x, const T_COMP y)
					:mX(x),mY(y)
				{}

				//! \brief Init from a vector of the same element type
				Vec2(const Vec2& vec)
					: mX(vec.mX),mY(vec.mY)
				{}

				//! \brief Init from a vector of another element type
				template<typename OTHER_T_COMP>
				explicit Vec2(const Vec2<OTHER_T_COMP>& vec)
					: mX((const T_COMP)vec.mX),mY((const T_COMP)vec.mY)
				{}

				//! \brief inits an existing object
				//! \param x	x component
				//! \param y	y component
				void init(const T_COMP x, const T_COMP y)
				{
					mX = x;
					mY = y;
				}

				//! \brief inits an existing object
				//! \param value inits all components with the same value
				void init(const T_COMP value)
				{
					mX = value;
					mY = value;
				}

				//! \brief get the length of the vector
				//! \remark Uses square root function. If only compares are need, use the faster getLength2.
				inline T_COMP getLength() const
				{
					return Sqrt(mX * mX + mY * mY);
				}

				//! \brief get the squared distance (faster if you need only comparison)
				inline T_COMP getLength2() const
				{
					return mX * mX + mY * mY;
				}

				//! \brief normalizes a vector
				//! \returns the length of the vector before normalization
				inline T_COMP normalize()
				{
					const T_COMP length = getLength();

					if (length == 0.0f) return 0.0f;

					mX /= length;
					mY /= length;

					return length;
				}

				//! \brief fast normalize using inverse squareroot
				inline void fastNormalize()
				{
					float32 length2= getLength2();

					float32 invLength= InvSqrt(length2);
					mX*= invLength;
					mY*= invLength;
				}

				//! \brief Test if a vector is normalized
				//! \returns True if the vector is normalized or false otherwise
				//! \remark The test encounters a small epsilon, so the vector could be of length 1.0001 or 0.9999 and still being handled as normalized.
				inline bool isNormalized() const
				{
					// we can use getLength2 for this test, since sqrt(1) =  1
					const T_COMP length= getLength2();

					return (length > (T_COMP)1.0 - (T_COMP)X_EPSILON) && (length < (T_COMP)1.0 + (T_COMP)X_EPSILON);
				}

				//! \brief Get squared distance to another position
				//! \param position		position to which we want the distance
				//! \return The squared distance
				inline T_COMP getDistance2(const Vec2& position) const
				{
					return (mX - position.mX) * (mX - position.mX) + (mY - position.mY) * (mY - position.mY);
				}

				//! \brief Get distance to another position
				//! \param position		position to which we want the distance
				//! \return The distance
				//! \remark If only compares between distances are needed, use the getDistance2 method.
				inline T_COMP getDistance(const Vec2& position) const
				{
					return Sqrt((mX - position.mX) * (mX - position.mX) + (mY - position.mY) * (mY - position.mY));
				}


				//! \brief compute a perpendicular vector to this vector which points in the right halfspace
				//!	of this vector.
				//! \param perp		a perpendicular vector to this one, pointing in the right halfspace
				inline void computePerpendicular(Vec2& perp) const
				{
					perp.mX= mY;
					perp.mY= -1.0f * mX;
				}

				//! \brief compute 2x2 determinat with another vector
				//! \param v	vector to build the 2x2 matrix with
				//! \returns The determinant
				inline T_COMP getDeterminant(const Vec2& v) const
				{
					return (mX * v.mY - v.mX * mY);
				}

				//! \brief compute dot product
				//! \param v	vector to compute the dot product with
				//! \returns the dot product
				inline T_COMP dotProduct(const Vec2& v) const
				{
					return (mX * v.mX + mY * v.mY);
				}

				//! \brief rotate a vector in the plane by a given scalar angle
				//! \param r	the angle in radiants
				//! \returns The rotated vector
				//! \remark the original vector is not transformed
				inline Vec2 rotate(const T_COMP r) const
				{
					const float cosAngle = Cos(r);
					const float sinAngle = Sin(r);

					return Vec2(mX * cosAngle - mY * sinAngle, mX * sinAngle + mY * cosAngle);
				}

				//! \brief Rotate a vector in the plane by a given rotation vector
				//! \param r	rotation vector (rotation vector in a unit circle), must be normalized
				//! \returns The rotated vector
				//! \remark The original vector is not transformed
				inline Vec2 rotate(const Vec2& r) const
				{
					X_ASSERT_MSG_DBG(r.isNormalized(),"rotation vector must be normalized");
					return Vec2(mX * r.mX - mY * r.mY, mX * r.mY + mY * r.mX);
				}

				//! \brief get a component by number (0-x,1-y)
				//! \remark Not so fast as direct member access
				inline T_COMP& operator[](const uint32 comp)
				{
					X_ASSERT_MSG_DBG(comp < 2,"component id out of range [0..1]");
					return *(&mX + comp);
				}

				//! \brief get a component by number (0-x,1-y)
				//! \remark Not so fast as direct member access
				inline const T_COMP& operator[](const uint32 comp) const
				{
					X_ASSERT_MSG_DBG(comp < 2,"component id out of range [0..1]");
					return *(&mX + comp);
				}

				//! \brief set a component by number by number (0-x,1-y)
				inline void setComponent(const uint32 comp,const T_COMP& value)
				{
					operator[](comp) = value;
				}

				//! \brief get the absolute maximum component
				//! \returns the index of the maximum component
				inline int32 getMaxComp() const
				{
					if (mX > mY)
					{
						return 0; 
					}
					return 1;
				}

				//! \brief get the absolute maximum component
				//! \returns the index of the maximum component
				inline int32 getAbsMaxComp() const
				{
					if (Common::Math::Abs(mX) > Common::Math::Abs(mY))
					{
						return 0;
					}
					return 1;
				}

				//! \brief Test if two vectors are equal
				//! \remark Vectors are equal if components are equal
				inline bool operator== (const Vec2& a) const
				{
					return (a.mX == mX && a.mY == mY);
				}

				//! \brief Test if two vectors are unequal
				//! \remark Vectors are unequal if components are unequal
				inline bool operator!= (const Vec2& a) const
				{
					return (a.mX != mX || a.mY != mY);
				}

				//! \brief Add another vector to this vector
				inline Vec2& operator+=(const Vec2& b)
				{
					mX+= b.mX; 
					mY+= b.mY;

					return *this;
				}

				//! \brief Substract another vector from this vector
				inline Vec2& operator-=(const Vec2& b)
				{
					mX-= b.mX; 
					mY-= b.mY;

					return *this;
				}

				//! \brief Multiply this vector with a scalar
				inline Vec2& operator*=(const T_COMP b)
				{
					mX*= b; 
					mY*= b;

					return *this;
				}

				//! \brief multiply component wise
				inline Vec2& operator*=(const Vec2& v)
				{
					mX*= v.mX;
					mY*= v.mY;

					return *this;
				}

				//! \brief Divide this vector by a scalar
				inline Vec2& operator/=(const T_COMP b)
				{
					mX/= b; 
					mY/= b;

					return *this;
				}

				//! \brief Add this vector to another vector and return the result
				inline Vec2 operator+(const Vec2 &a) const
				{
					return Vec2(a.mX + mX,a.mY + mY);
				}

				//! \brief Negate vector and return the result
				inline Vec2 operator-() const
				{
					return Vec2(-mX,-mY);
				}

				//! \brief Substract a vector from this one and return the result
				inline Vec2 operator-(const Vec2 &a) const
				{
					return Vec2(mX - a.mX,mY - a.mY);
				}

				//! \brief Multiply a scalar to this vector and return the result
				inline Vec2 operator*(const T_COMP a) const
				{
					return Vec2(a * mX,a * mY);
				}

				//! \brief multiply component wise  and return the result
				inline Vec2 operator*(const Vec2& v) const
				{
					return Vec2(v.mX * mX,v.mY * mY);
				}

				//! \brief Divide this vector by a scalar and return the result
				inline Vec2 operator/(const T_COMP a) const
				{
					return Vec2(mX / a,mY / a);
				}

				//! \brief test if all components of the current vector are smaller than of another vector
				inline bool operator<(const Vec2& v) const
				{
					return ( (mX == v.mX) ? (mY < v.mY) : (mX < v.mX) );
				}

				//! \brief test if all components of the current vector are bigger than of another vector
				inline bool operator>(const Vec2& v) const
				{
					return ( (mX == v.mX) ? (mY > v.mY) : (mX > v.mX) );
				}

				//! \brief test if all components of the current vector are smaller or equal than of another vector
				inline bool operator<=(const Vec2& v) const
				{
					return ( (mX==v.mX) ? (mY<=v.mY) : (mX<v.mX) );
				}

				//! \brief test if all components of the current vector are greater or equal than of another vector
				inline bool operator>=(const Vec2& v) const
				{
					return ( (mX == v.mX) ? (mY >= v.mY) : (mX > v.mX) );
				}

				// predefine some common vectors
				static const Vec2<T_COMP> ZERO;
				static const Vec2<T_COMP> UNIT_X;
				static const Vec2<T_COMP> UNIT_Y;
				static const Vec2<T_COMP> NEGATIVE_UNIT_X;
				static const Vec2<T_COMP> NEGATIVE_UNIT_Y;
				static const Vec2<T_COMP> UNIT_SCALE;
			};	// class Vec2

			template <typename T_COMP> const Vec2<T_COMP> Vec2<T_COMP>::ZERO = Vec2<T_COMP>( 0, 0);
			template <typename T_COMP> const Vec2<T_COMP> Vec2<T_COMP>::UNIT_X = Vec2<T_COMP>( 1, 0);
			template <typename T_COMP> const Vec2<T_COMP> Vec2<T_COMP>::UNIT_Y = Vec2<T_COMP>( 0, 1);
			template <typename T_COMP> const Vec2<T_COMP> Vec2<T_COMP>::NEGATIVE_UNIT_X = Vec2<T_COMP>( -1,  0);
			template <typename T_COMP> const Vec2<T_COMP> Vec2<T_COMP>::NEGATIVE_UNIT_Y = Vec2<T_COMP>(  0, -1);
			template <typename T_COMP> const Vec2<T_COMP> Vec2<T_COMP>::UNIT_SCALE = Vec2<T_COMP>(1, 1);

			//template<typename T_COMP>
            //struct Vec2;
						
			typedef Vec2<float32>	Vec2f;
			typedef Vec2<float64>	Vec2d;
			typedef Vec2<int32>		Vec2i32;
			typedef Vec2<int64>		Vec2i64;
			typedef Vec2<uint32>	Vec2u32;
			typedef Vec2<uint64>	Vec2u64;
			

		}	// namespace Math
	}	// namespace Common

	template<typename COMP_T>
	inline std::ostream& operator<<(std::ostream& os,const Common::Math::Vec2<COMP_T> &v)
	{
		os << "(" << v.mX << "," << v.mY << ")";
		return os;
	}
}
#endif

