// (C) xaitment GmbH 2006-2012

#pragma once


#include <xait/common/math/Vec3.h>
#include <xait/common/math/ValueFunc.h>
#include <xait/common/math/MathDefines.h>
#include <xait/common/math/Quaternion.h>

#include <xait/common/FundamentalTypes.h>
#include <xait/common/debug/Assert.h>

#ifdef X_SIMD
#include <xmmintrin.h>
#endif




namespace XAIT
{
	namespace Common
	{
		namespace Math
		{
			template<typename T_COMP>
			struct Matrix4;

			typedef Matrix4<float32>		Matrix4f;		//!< always 32-bit
			typedef Matrix4<float64>		Matrix4d;		//!< always 64-bit


			//! Template class for 4x4 row matrix. Counting 0 .. 3 -> row1 , 4 .. 7 -> row2, 
			//! 8 .. 11 -> row3, 12 .. 15 -> row4.
			//! \brief Template class for 4x4 row matrix.
			template<typename T_COMP>
			struct Matrix4 
			{
				T_COMP		mMatrix[16];	//!< matrix rows


				//! \brief default constructor 
				//! \remark The constructor does not init the matrix in any way. (performance issue)
				Matrix4()
				{
				}

				//! \brief initialize from another matrix
				//! \param m	other matrix;
				Matrix4(const Matrix4& m)
				{
					memcpy(mMatrix,m.mMatrix,sizeof(T_COMP) * 16);
				}

				//! \brief initialize from a float array representing a 4x4 matrix
				//! \param matValue		values of the matrix
				Matrix4(const T_COMP* matValues)
				{
					memcpy(mMatrix,matValues,sizeof(T_COMP) * 16);
				}

				//! \brief initialize matrix from component values
				Matrix4(T_COMP m00, T_COMP m01,T_COMP m02,T_COMP m03,
						T_COMP m10, T_COMP m11,T_COMP m12,T_COMP m13,
						T_COMP m20, T_COMP m21,T_COMP m22,T_COMP m23,
						T_COMP m30, T_COMP m31,T_COMP m32,T_COMP m33)
				{
					mMatrix[ 0]= m00;
					mMatrix[ 1]= m01;
					mMatrix[ 2]= m02;
					mMatrix[ 3]= m03;

					mMatrix[ 4]= m10;
					mMatrix[ 5]= m11;
					mMatrix[ 6]= m12;
					mMatrix[ 7]= m13;

					mMatrix[ 8]= m20;
					mMatrix[ 9]= m21;
					mMatrix[10]= m22;
					mMatrix[11]= m23;

					mMatrix[12]= m30;
					mMatrix[13]= m31;
					mMatrix[14]= m32;
					mMatrix[15]= m33;
				}

				//! \brief initialize from another matrix of different component type
				template<typename OTHER_T_COMP>
				explicit Matrix4(const Matrix4<OTHER_T_COMP>& m)
				{
					for(uint32 i= 0; i < 16; ++i)
					{
						mMatrix[i]= (T_COMP)m.mMatrix[i];
					}
				}

				//! \brief get the identity matrix
				//! \param identMat[out]	identity matrix
				inline static void getIdentityMatrix(Matrix4& identMat)
				{
					identMat.setIdentity();
				}

				//! \brief get a rotation matrix, by rotating around an arbitrary axis
				//! \param rotation[out]	rotation matrix
				//! \param axis				rotation axis (must be non zero)
				//! \param angle			rotation angle in radians
				static void getRotationMatrix(Matrix4& rotMat, const Vec3<T_COMP>& axis, const T_COMP angle)
				{
					X_ASSERT_MSG_DBG(!axis.isZero(),"rotation axis must not be zero");

					Vec3<T_COMP> normAxis= axis;
					normAxis.normalize();

					// get the sin and cos values
					const T_COMP s = Sin(angle);
					const T_COMP c = Cos(angle);

					// make some common precomputations
					const T_COMP ab = normAxis.mX * normAxis.mY * (1 - c);
					const T_COMP bc = normAxis.mY * normAxis.mZ * (1 - c);
					const T_COMP ca = normAxis.mZ * normAxis.mX * (1 - c);

					// build a rotation matrix
					T_COMP t;
					T_COMP* rot= rotMat.mMatrix;

					t = normAxis.mX * normAxis.mX;
					rot[0] = t + c * (1-t);
					rot[6] = bc + normAxis.mX * s;
					rot[9] = bc - normAxis.mX * s;

					t = normAxis.mY * normAxis.mY;
					rot[2] = ca - normAxis.mY * s;
					rot[5] = t + c * (1-t);
					rot[8] = ca + normAxis.mY * s;

					t = normAxis.mZ * normAxis.mZ;
					rot[1] = ab + normAxis.mZ * s;
					rot[4] = ab - normAxis.mZ * s;
					rot[10] = t + c * (1-t);

					// fill in the rest of the matrix as identity
					rot[3] = 0;
					rot[7] = 0;
					rot[11] = 0;
					rot[12] = 0;
					rot[13] = 0;
					rot[14] = 0;
					rot[15] = 1;
				}

				//! \brief get a rotation matrix from a quaternion
				//! \param rotMat[out]	rotation matrix
				//! \param quaternion		quaternion to init from
				static void getRotationMatrix(Matrix4& rotMat, const Quaternion<T_COMP>& quat)
				{
					// formula from http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToMatrix/index.htm
					X_ASSERT_MSG_DBG(quat.isNormalized(),"Quaternion is not normalized !");

					T_COMP xx      = quat.mX * quat.mX;
					T_COMP xy      = quat.mX * quat.mY;
					T_COMP xz      = quat.mX * quat.mZ;
					T_COMP xw      = quat.mX * quat.mW;

					T_COMP yy      = quat.mY * quat.mY;
					T_COMP yz      = quat.mY * quat.mZ;
					T_COMP yw      = quat.mY * quat.mW;

					T_COMP zz      = quat.mZ * quat.mZ;
					T_COMP zw      = quat.mZ * quat.mW;

					rotMat.mMatrix[0] = 1 - 2 * ( yy + zz );
					rotMat.mMatrix[1] =     2 * ( xy - zw );
					rotMat.mMatrix[2] =     2 * ( xz + yw );

					rotMat.mMatrix[4] =     2 * ( xy + zw );
					rotMat.mMatrix[5] = 1 - 2 * ( xx + zz );
					rotMat.mMatrix[6] =     2 * ( yz - xw );

					rotMat.mMatrix[8] =     2 * ( xz - yw );
					rotMat.mMatrix[9] =     2 * ( yz + xw );
					rotMat.mMatrix[10]= 1 - 2 * ( xx + yy );

					rotMat.mMatrix[3]= rotMat.mMatrix[7]= rotMat.mMatrix[11]= rotMat.mMatrix[12]= rotMat.mMatrix[13]= rotMat.mMatrix[14]= 0;
					rotMat.mMatrix[15]= 1;
				}

				//! \brief get a arbitrary scaled matrix
				//! \param scaleMat[out]	scaled matrix
				//! \param scale			vector for scaling each axis
				inline static void getScaleMatrix(Matrix4& scaleMat, const Vec3<T_COMP>& scale)
				{
					scaleMat.setIdentity();

					scaleMat.mMatrix[0]= scale.mX;
					scaleMat.mMatrix[5]= scale.mY;
					scaleMat.mMatrix[10]= scale.mZ;
				}

				//! \brief get a uniform scaled matrix
				//! \param scaleMat[out]	scaled matrix
				//! \param scale			for every axis
				inline static void getScaleMatrix(Matrix4& scaleMat, const T_COMP scale)
				{
					scaleMat.setIdentity();

					scaleMat.mMatrix[0]= scale;
					scaleMat.mMatrix[5]= scale;
					scaleMat.mMatrix[10]= scale;
				}

				//! \brief get row r from the matrix
				inline T_COMP* operator[](const uint32 r)
				{
					return &mMatrix[r];
				}

				//! \brief get row r from the matrix
				inline const T_COMP* operator[](const uint32 r) const
				{
					return &mMatrix[r];
				}

				//! \brief set identity matrix
				inline void setIdentity()
				{
					mMatrix[0] = mMatrix[5] = mMatrix[10] = mMatrix[15] = 1.0f;
					mMatrix[1] = mMatrix[2] = mMatrix[3] = 0.0f;
					mMatrix[4] = mMatrix[6] = mMatrix[7] = 0.0f;
					mMatrix[8] = mMatrix[9] = mMatrix[11] = 0.0f;
					mMatrix[12] = mMatrix[13] = mMatrix[14] = 0.0f;
				}


				//! \brief Test if two matrices are equal
				//! \remark This test uses an epsilon for every comparison, so the matrices
				//! do not need to be completely equal.
				bool isEqual(const Matrix4& m) const
				{
					// We test in the crazy order, as elements 3, 7, 11, and 15 are normally 0, 0, 0, 1
					// so there is less chance of failing early with those elements.
					return ((Abs(mMatrix[0]-m.mMatrix[0]) < X_EPSILON) && 
						(Abs(mMatrix[1]-m.mMatrix[1]) < X_EPSILON) && 
						(Abs(mMatrix[2]-m.mMatrix[2]) < X_EPSILON) &&
						(Abs(mMatrix[4]-m.mMatrix[4]) < X_EPSILON) &&
						(Abs(mMatrix[5]-m.mMatrix[5]) < X_EPSILON) &&
						(Abs(mMatrix[6]-m.mMatrix[6]) < X_EPSILON) &&
						(Abs(mMatrix[8]-m.mMatrix[8]) < X_EPSILON) &&
						(Abs(mMatrix[9]-m.mMatrix[9]) < X_EPSILON) &&
						(Abs(mMatrix[10]-m.mMatrix[10]) < X_EPSILON) &&
						(Abs(mMatrix[12]-m.mMatrix[12]) < X_EPSILON) &&
						(Abs(mMatrix[13]-m.mMatrix[13]) < X_EPSILON) &&
						(Abs(mMatrix[14]-m.mMatrix[14]) < X_EPSILON) &&
						(Abs(mMatrix[3]-m.mMatrix[3]) < X_EPSILON) &&
						(Abs(mMatrix[7]-m.mMatrix[7]) < X_EPSILON) &&
						(Abs(mMatrix[11]-m.mMatrix[11]) < X_EPSILON) &&
						(Abs(mMatrix[15]-m.mMatrix[15]) < X_EPSILON));
				}


				//! \brief Translate the matrix by x,y,z
				//! \param x	translation in x
				//! \param y	translation in y
				//! \param z	translation in z
				inline void translate(const T_COMP x, const T_COMP y, const T_COMP z)
				{
					// apply a translation
					mMatrix[12] += x*mMatrix[0] + y*mMatrix[4] + z*mMatrix[8];
					mMatrix[13] += x*mMatrix[1] + y*mMatrix[5] + z*mMatrix[9];
					mMatrix[14] += x*mMatrix[2] + y*mMatrix[6] + z*mMatrix[10];
					mMatrix[15] += x*mMatrix[3] + y*mMatrix[7] + z*mMatrix[11];
					return;
				}

				//! \brief Translate the matrix by a vector
				//! \param v	vector to translate the matrix
				inline void translate(const Vec3<T_COMP>& v)
				{
					// apply a translation
					mMatrix[12] += v.mX*mMatrix[0] + v.mY*mMatrix[4] + v.mZ*mMatrix[8];
					mMatrix[13] += v.mX*mMatrix[1] + v.mY*mMatrix[5] + v.mZ*mMatrix[9];
					mMatrix[14] += v.mX*mMatrix[2] + v.mY*mMatrix[6] + v.mZ*mMatrix[10];
					mMatrix[15] += v.mX*mMatrix[3] + v.mY*mMatrix[7] + v.mZ*mMatrix[11];
					return;
				}

				//! \brief rotate this matrix around an arbitrary axis
				//! \param axis		rotation axis (must be normalized)
				//! \param angle	rotation angle in radians
				//! \remark Appends rotate
				inline void rotate(const Vec3<T_COMP>& axis, const T_COMP angle)
				{
					Matrix4 rotMat;
					getRotationMatrix(rotMat,axis,angle);

					// multiply the existing matrix with the new one
					this->multMatrix(*this , rotMat);
				}

				//! \brief rotate this matrix with a quaternion
				//! \param axis		rotation axis (must be normalized)
				//! \param angle	rotation angle in radians
				//! \remark Appends rotate
				inline void rotate(const Quaternion<T_COMP>& quat)
				{
					Matrix4 rotMat;
					getRotationMatrix(rotMat,quat);

					// multiply the existing matrix with the new one
					this->multMatrix(*this , rotMat);
				}

				//! \brief scale this matrix with different scales for every axis
				//! \param scale	scale for every axis
				//! \remark Appends scale
				inline void scale(const Vec3<T_COMP>& scale)
				{
					Matrix4 sMat;
					getScaleMatrix(sMat,scale);

					// multiply the existing matrix with the new one
					this->multMatrix(*this,sMat);
				}

				//! \brief uniformly scale this matrix
				//! \param scale	scale factor for every axis
				//! \remark Appends scale
				inline void scale(const T_COMP scale)
				{
					Matrix4 sMat;
					getScaleMatrix(sMat,scale);

					// multiply the existing matrix with the new one
					this->multMatrix(*this,sMat);

				}

				//! \brief multiply a matrix with this matrix (result= this * mat)
				//! \param result[out]	matrix resulting from the matrix multiplication
				//! \param mat			matrix to multiply
				//! \remark You can use this matrix as result (i.e. mat.multMatrix(mat,mat1))
				void multMatrix(Matrix4& result,const Matrix4& mat) const
				{
					T_COMP* dest= result.mMatrix;
					const T_COMP* b= mat.mMatrix;

					for (uint32 i=0; i<4; i++)
					{
						const T_COMP a0 = mMatrix[i];
						const T_COMP a1 = mMatrix[4+i];
						const T_COMP a2 = mMatrix[8+i];
						const T_COMP a3 = mMatrix[12+i];
						dest[i]    = a0*b[0]  + a1*b[1]  + a2*b[2]  + a3*b[3];
						dest[4+i]  = a0*b[4]  + a1*b[5]  + a2*b[6]  + a3*b[7];
						dest[8+i]  = a0*b[8]  + a1*b[9]  + a2*b[10] + a3*b[11];
						dest[12+i] = a0*b[12] + a1*b[13] + a2*b[14] + a3*b[15];
					}
				}

				bool getInverse(Matrix4& result) const
				{
					T_COMP matr[4][4], ident[4][4];
					int i, j, k, l, ll;
					int icol=0, irow=0;
					int indxc[4], indxr[4], ipiv[4];
					T_COMP big, dum, pivinv;

					for (i=0; i<4; i++) {
						for (j=0; j<4; j++) {
							matr[i][j] = mMatrix[4*i+j];
							ident[i][j] = 0.0f;
						}
						ident[i][i]=1.0f;
					} 

					// Gauss-jordan elimination with full pivoting. 
					// from numerical recipies in C second edition, pg 39

					for(j=0;j<=3;j++) ipiv[j] = 0;
					for(i=0;i<=3;i++) {
						big=0.0;
						for (j=0;j<=3;j++) {
							if(ipiv[j] != 1) {
								for (k=0;k<=3;k++) {
									if(ipiv[k] == 0) {
										if(Math::Abs(matr[j][k]) >= big) {
											big = (float) Math::Abs(matr[j][k]);
											irow=j;
											icol=k;
										}
									} else if (ipiv[k] > 1) return false;
								} 
							}
						}
						++(ipiv[icol]);
						if (irow != icol) {
							for (l=0;l<=3;l++) Math::Swap(matr[irow][l],matr[icol][l]);
							for (l=0;l<=3;l++) Math::Swap(ident[irow][l],ident[icol][l]);
						}
						indxr[i]=irow;
						indxc[i]=icol;
						if(matr[icol][icol] == 0.0f) return 1; 
						pivinv = 1.0f / matr[icol][icol];
						matr[icol][icol]=1.0f;
						for (l=0;l<=3;l++) matr[icol][l] *= pivinv;
						for (l=0;l<=3;l++) ident[icol][l] *= pivinv;
						for (ll=0;ll<=3;ll++) {
							if (ll != icol) {
								dum=matr[ll][icol];
								matr[ll][icol]=0.0f;
								for (l=0;l<=3;l++) matr[ll][l] -= matr[icol][l]*dum;
								for (l=0;l<=3;l++) ident[ll][l] -= ident[icol][l]*dum;
							}
						}
					}
					for (l=3;l>=0;l--) {
						if (indxr[l] != indxc[l]) {
							for (k=0;k<=3;k++) {
								Math::Swap(matr[k][indxr[l]],matr[k][indxc[l]]);
							}
						}
					}
					for (i=0; i<4; i++) 
						for (j=0; j<4; j++)
							result.mMatrix[4*i+j] = matr[i][j];

					return true;
				}


				//! \brief transform a vector by using this matrix
				//! \param v	vector to transform
				//! \returns transformed vector
				inline Vec3<T_COMP> transformVector(const Vec3<T_COMP>& v) const
				{
					Vec3<T_COMP> dst;
					const T_COMP x = v.mX;
					const T_COMP y = v.mY;
					const T_COMP z = v.mZ;
					dst[0] = x*mMatrix[0] + y*mMatrix[4] + z*mMatrix[8];
					dst[1] = x*mMatrix[1] + y*mMatrix[5] + z*mMatrix[9];
					dst[2] = x*mMatrix[2] + y*mMatrix[6] + z*mMatrix[10];
					return dst;
				}

				//! \brief transform a point by using this matrix (has also translation)
				//! \param v	position to transform
				//! \returns transformed position
				inline Vec3<T_COMP> transformPoint(const Vec3<T_COMP>& vin) const
				{
					Vec3<T_COMP> dst;

#ifdef X_SIMD
					__m128 row0= _mm_loadu_ps(&mMatrix[0]);
					__m128 row1= _mm_loadu_ps(&mMatrix[4]);
					__m128 row2= _mm_loadu_ps(&mMatrix[8]);
					__m128 row3= _mm_loadu_ps(&mMatrix[12]);
					
					__m128 v;
					__m128 vss;
					__m128 res;

					v= _mm_loadu_ps(&vin.mX);
					vss= _mm_shuffle_ps(v,v,0x00);
					res= _mm_mul_ps(vss,row0);

					vss= _mm_shuffle_ps(v,v,0x55);
					res= _mm_add_ps(res,_mm_mul_ps(vss,row1));

					vss= _mm_shuffle_ps(v,v,0xAA);
					res= _mm_add_ps(res,_mm_mul_ps(vss,row2));

					res= _mm_add_ps(res,row3);

					_mm_storeu_ps(&dst.mX,res);												
#else
					const T_COMP x = vin.mX;
					const T_COMP y = vin.mY;
					const T_COMP z = vin.mZ;
					dst.mX = x*mMatrix[0] + y*mMatrix[4] + z*mMatrix[8] + mMatrix[12];
					dst.mY = x*mMatrix[1] + y*mMatrix[5] + z*mMatrix[9] + mMatrix[13];
					dst.mZ = x*mMatrix[2] + y*mMatrix[6] + z*mMatrix[10] + mMatrix[14];
#endif
					return dst;
				}

				inline void transformPoints(Vec3<T_COMP>* dstVecs, const Vec3<T_COMP>* srcVecs, const uint32 len) const
				{
#ifdef X_SIMD
					__m128 row0= _mm_loadu_ps(&mMatrix[0]);
					__m128 row1= _mm_loadu_ps(&mMatrix[4]);
					__m128 row2= _mm_loadu_ps(&mMatrix[8]);
					__m128 row3= _mm_loadu_ps(&mMatrix[12]);

					__m128 vss;
					__m128 v;
					__m128 res;
					for(uint32 i= 0; i < len; ++i)
					{
						const float32* sVec= &srcVecs[i].mX;
						float32* dVec= &dstVecs[i].mX;
							
						v= _mm_loadu_ps(sVec);
						_mm_prefetch((const char*)sVec + 0x30,_MM_HINT_NTA);
						_mm_prefetch((const char*)dVec + 0x30,_MM_HINT_NTA);
						vss= _mm_shuffle_ps(v,v,0x00);
						res= _mm_mul_ps(vss,row0);

						vss= _mm_shuffle_ps(v,v,0x55);
						res= _mm_add_ps(res,_mm_mul_ps(vss,row1));

						vss= _mm_shuffle_ps(v,v,0xAA);
						res= _mm_add_ps(res,_mm_mul_ps(vss,row2));

						res= _mm_add_ps(res,row3);

						_mm_storeu_ps(dVec,res);												
					}
#else
					for(uint32 i= 0; i < len; ++i)
					{
						dstVecs[i]= transformPoint(srcVecs[i]);
					}
#endif
				}

				inline Vec3f getUp()const
				{
					Vec3f r(mMatrix[8],mMatrix[9],mMatrix[10]);
					return r;
				}

				inline Vec3f getRight()const
				{
					Vec3f r(mMatrix[0],mMatrix[1],mMatrix[2]);
					return r;
				}

				inline Vec3f getLookAt()const
				{
					Vec3f r(mMatrix[4],mMatrix[5],mMatrix[6]);
					return r;
				}

				inline Vec3f getPos() const
				{
					Vec3f r(mMatrix[12],mMatrix[13],mMatrix[14]);
					return r;
				}

				inline void setPos(const T_COMP x,const T_COMP y,const T_COMP z) 
				{
					mMatrix[12] = x;
					mMatrix[13] = y;
					mMatrix[14] = z;
				}

				inline void setPos(const Vec3f& v) 
				{
					mMatrix[12] = v.mX;
					mMatrix[13] = v.mY;
					mMatrix[14] = v.mZ;
				}

				inline void setUp(const T_COMP x,const T_COMP y,const T_COMP z) 
				{
					mMatrix[8] = x;
					mMatrix[9] = y;
					mMatrix[10] = z;
				}

				inline void setUp(const Vec3f& v) 
				{
					mMatrix[8] = v.mX;
					mMatrix[9] = v.mY;
					mMatrix[10]= v.mZ;
				}

				inline void setRight(const T_COMP x,const T_COMP y,const T_COMP z) 
				{
					mMatrix[0] = x;
					mMatrix[1] = y;
					mMatrix[2] = z;
				}

				inline void setRight(const Vec3f& v) 
				{
					mMatrix[0] = v.mX;
					mMatrix[1] = v.mY;
					mMatrix[2] = v.mZ;
				}

				inline void setLookAt(const T_COMP x,const T_COMP y,const T_COMP z) 
				{
					mMatrix[4] = x;
					mMatrix[5] = y;
					mMatrix[6] = z;
				}

				inline void setLookAt(const Vec3f& v) 
				{
					mMatrix[4] = v.mX;
					mMatrix[5] = v.mY;
					mMatrix[6] = v.mZ;
				}

				static const Matrix4<T_COMP>	IDENTITY;
			};

			template <typename T_COMP> const Matrix4<T_COMP> Matrix4<T_COMP>::IDENTITY = Matrix4<T_COMP>(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1);

		}	// namespace Math
	}	// namespace Common
}

