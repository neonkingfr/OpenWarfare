// (C) xaitment GmbH 2006-2012

#pragma once 

// ------------------------------------------------------------------------------------------------
//								Often Used Math Constants and Formulas
//
// Author: Markus Wilhelm
// ------------------------------------------------------------------------------------------------
#include <limits>

#define X_HALFPI		1.5707963267948966192313216916398
#define X_PI			3.1415926535897932384626433832795
#define X_TWOPI			6.283185307179586476925286766559
#define X_EPSILON		0.0001
#define X_DEG2RAD		0.017453292519943295769236907684886
#define X_RAD2DEG		57.295779513082320876798154814105

#define X_HALFPI_F		1.5707963267948966192313216916398f
#define X_PI_F			3.1415926535897932384626433832795f
#define X_TWOPI_F		6.283185307179586476925286766559f
#define X_EPSILON_F		0.0001f
#define X_DEG2RAD_F		0.017453292519943295769236907684886f
#define X_RAD2DEG_F		57.295779513082320876798154814105f

#define X_POS_INFINITY	3.402823465e38f
#define X_NEG_INFINITY	-3.402823465e38f

#define X_POS_INFINITY_F32	3.402823465e38f
#define X_NEG_INFINITY_F32	-3.402823465e38f

#define X_MIN(a,b)			((a) < (b) ? (a) : (b))
#define X_MAX(a,b)			((a) < (b) ? (b) : (a))
#define X_SIGN(a)			((a) < 0 ? (-1)   : (1)   )
#define X_SIGNF(a)			((a) < 0 ? (-1.0f): (1.0f))
#define X_CLAMP(x,min,max)  (X_MIN( X_MAX(x,min) ,max) )
