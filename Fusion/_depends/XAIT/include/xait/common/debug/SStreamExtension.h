// (C) xaitment GmbH 2006-2012

#pragma once 

// extends the sstream class with operator for binary print streaming
// this extension needs RTTI
// Author: Markus Wilhelm
#include <iomanip>

#include <xait/common/Platform.h>
#include <xait/common/FundamentalTypes.h>

#if XAIT_OS == XAIT_OS_WIN32

namespace XAIT
{
	namespace Common
	{
		namespace Debug
		{	
				//! \brief print 8-bit number in binary
				std::_Smanip<uint08> XAIT_CDECL bin(uint32 no);
	
				//! \brief print low nibble of 8-bit number in binary
				std::_Smanip<uint08> XAIT_CDECL lnibble(uint32 no);
	
				//! \brief print high nibble of 8-bit number in binary
				std::_Smanip<uint08> XAIT_CDECL hnibble(uint32 no);
		}
	}
}

#endif




