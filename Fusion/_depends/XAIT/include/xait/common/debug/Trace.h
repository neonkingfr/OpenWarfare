// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/Platform.h>


#if (XAIT_OS == XAIT_OS_WIN) && !defined(XAIT_TRACE_DISABLE)


#define TRACEWND_CLASSNAME		_T("XAITraceWindow")
#define MAX_MESSAGE_LENGTH		4096


#define _TRACE_DEF_STREAM			std::stringstream _trace_string
#define _TRACE_STREAM				_trace_string
#define _TRACE_GET_STREAM_STR		_trace_string.str().c_str()


#ifndef TRACEWND

#define TRACE_MESSAGE(msg) \
	{_TRACE_DEF_STREAM;\
	_TRACE_STREAM << std::dec << __FUNCTION__ << " : " << ##msg;\
	XAIT::Common::Debug::Trace::TraceMessage(XAIT::Common::Debug::Trace::TRACE_MESSAGE,_TRACE_STREAM);}

#define TRACE_ERROR(msg) \
	{_TRACE_DEF_STREAM;\
	_TRACE_STREAM << std::dec << __FUNCTION__ << " : " << ##msg;\
	XAIT::Common::Debug::Trace::TraceMessage(XAIT::Common::Debug::Trace::TRACE_ERROR,_TRACE_STREAM);}

#define DISPLAY_ERROR(msg,caption) {_TRACE_DEF_STREAM; _TRACE_STREAM << std::dec << ##msg;GERROR(##msg);XAIT::Common::Debug::Trace::displayError(_TRACE_STREAM,caption);}

#define DISPLAY_ERROR_AND_DIE(msg,caption) {_TRACE_DEF_STREAM; _TRACE_STREAM << std::dec << msg;GERROR(##msg);XAIT::Common::Debug::Trace::displayErrorAndDie(_TRACE_STREAM,caption);}

#define DISPLAY_MESSAGE(msg,caption) {_TRACE_DEF_STREAM; _TRACE_STREAM << std::dec << ##msg;TRACE_MESSAGE(##msg);XAIT::Common::Debug::Trace::displayMessage(_TRACE_STREAM,caption);}

#else  // TRACEWND

#define GMESSAGE(msg) \
{XAIT::Common::Debug::Trace::msgstream->str("");\
	(*XAIT::Common::Debug::Trace::msgstream) << std::dec << "TraceWindow: " << __FUNCTION__ << " : " << ##msg;\
	XAIT::Common::Debug::Trace::SendMessage(XAIT::Common::Debug::Trace::TRACE_TRACEINTERN_MESSAGE,*XAIT::Common::Debug::Trace::msgstream);}

#define GERROR(msg) \
{XAIT::Common::Debug::Trace::msgstream->str("");\
	(*XAIT::Common::Debug::Trace::msgstream) << std::dec << "TraceWindow: " << __FUNCTION__ << " : " << ##msg;\
	XAIT::Common::Debug::Trace::SendMessage(XAIT::Common::Debug::Trace::TRACE_TRACEINTERN_ERROR,*XAIT::Common::Debug::Trace::msgstream);}

#endif // TRACEWND



#define USE_TRACE(cmd)		cmd


// draw functions for simple drawing, using layer 0

#define DLINE2D_SLD(a,b)	XAIT::Common::Debug::Trace::DrawLine2D(a,b,XAIT::Common::Debug::Trace::LSTYLE_SOLID,0)
#define DLINE2D_DSH(a,b)	XAIT::Common::Debug::Trace::DrawLine2D(a,b,XAIT::Common::Debug::Trace::LSTYLE_DASH,0)
#define DLINE2D_DOT(a,b)	XAIT::Common::Debug::Trace::DrawLine2D(a,b,XAIT::Common::Debug::Trace::LSTYLE_DOT,0)

#define DARROW2D_SLD(a,b)	XAIT::Common::Debug::Trace::DrawLine2D(a,b,XAIT::Common::Debug::Trace::LSTYLE_ARROW_SOLID,0)

#define DPOINT2D(pos)		XAIT::Common::Debug::Trace::DrawPoint2D(pos,0);

#define DBOX2D(a,b)			XAIT::Common::Debug::Trace::DrawBox2D(a,b,XAIT::Common::Debug::Trace::BSTYLE_SOLID,0);

#define DCIRCLE2D_SLD(center,radius)	XAIT::Common::Debug::Trace::DrawCircle2D(center,radius,XAIT::Common::Debug::Trace::CSTYLE_SOLID,0)
#define DCIRCLE2D_DSH(center,radius)	XAIT::Common::Debug::Trace::DrawCircle2D(center,radius,XAIT::Common::Debug::Trace::CSTYLE_DASH,0)
#define DCIRCLE2D_DOT(center,radius)	XAIT::Common::Debug::Trace::DrawCircle2D(center,radius,XAIT::Common::Debug::Trace::CSTYLE_DOT,0)

#define DTEXT2D(posTopLeft,text)		XAIT::Common::Debug::Trace::DrawText2D(posTopLeft,text,0)

#define SETDCOLOR(r,g,b)	XAIT::Common::Debug::Trace::SetDrawColor(r,g,b,0);

#define CLEARDISPLAY2D()			XAIT::Common::Debug::Trace::ClearDisplay2D()

#define REPAINTDISPLAY2D()			XAIT::Common::Debug::Trace::RepaintDisplay2D()

#define CLEARLAYER(layer)				XAIT::Common::Debug::Trace::ClearLayer(layer)
#define CLEARALLLAYER()					XAIT::Common::Debug::Trace::ClearLayer(-1)

#define DTRIANGLE3D(a,b,c)			XAIT::Common::Debug::Trace::DrawTriangle3D(a,b,c,0)
#define DBOX3D(min_val,max_val)		XAIT::Common::Debug::Trace::DrawBox3D(min_val,max_val,0)
#define SETDCOLOREX(r,g,b,a)		XAIT::Common::Debug::Trace::SetDrawColorEx(r,g,b,a,0)
#define DFLUSH()					XAIT::Common::Debug::Trace::Flush()

// draw functions for extended drawing, using different layers

#define DLINE2D_SLD_L(a,b,layer)	XAIT::Common::Debug::Trace::DrawLine2D(a,b,XAIT::Common::Debug::Trace::LSTYLE_SOLID,layer)
#define DLINE2D_DSH_L(a,b,layer)	XAIT::Common::Debug::Trace::DrawLine2D(a,b,XAIT::Common::Debug::Trace::LSTYLE_DASH,layer)
#define DLINE2D_DOT_L(a,b,layer)	XAIT::Common::Debug::Trace::DrawLine2D(a,b,XAIT::Common::Debug::Trace::LSTYLE_DOT,layer)

#define DARROW2D_SLD_L(a,b,layer)	XAIT::Common::Debug::Trace::DrawLine2D(a,b,XAIT::Common::Debug::Trace::LSTYLE_ARROW_SOLID,layer)

#define DCIRCLE2D_SLD_L(center,radius,layer)	XAIT::Common::Debug::Trace::DrawCircle2D(center,radius,XAIT::Common::Debug::Trace::CSTYLE_SOLID,layer)
#define DCIRCLE2D_DSH_L(center,radius,layer)	XAIT::Common::Debug::Trace::DrawCircle2D(center,radius,XAIT::Common::Debug::Trace::CSTYLE_DASH,layer)
#define DCIRCLE2D_DOT_L(center,radius,layer)	XAIT::Common::Debug::Trace::DrawCircle2D(center,radius,XAIT::Common::Debug::Trace::CSTYLE_DOT,layer)

#define DTEXT2D_L(posTopLeft,text,layer)		XAIT::Common::Debug::Trace::DrawText2D(posTopLeft,text,layer)

#define DPOINT2D_L(pos,layer)		XAIT::Common::Debug::Trace::DrawPoint2D(pos,layer)


#define SETDCOLOR_L(r,g,b,layer)		XAIT::Common::Debug::Trace::SetDrawColor(r,g,b,layer)
#define SETDLAYERNAME(name,layer)		XAIT::Common::Debug::Trace::SetLayerName(name,layer)
#define SETDMODE_ALL_L(layer)			XAIT::Common::Debug::Trace::SetDrawMode(XAIT::Common::Debug::Trace::DMODE_ALL,layer)
#define SETDMODE_SINGLE_L(layer)		XAIT::Common::Debug::Trace::SetDrawMode(XAIT::Common::Debug::Trace::DMODE_SINGLE,layer)
#define SETDMODE_BEFORE_L(layer)		XAIT::Common::Debug::Trace::SetDrawMode(XAIT::Common::Debug::Trace::DMODE_BEFORE,layer)
#define SETDMODE_SINGLE_SEP_L(layer)	XAIT::Common::Debug::Trace::SetDrawMode(XAIT::Common::Debug::Trace::DMODE_SINGLE_SEP,layer)

#define DSEPERATOR()				XAIT::Common::Debug::Trace::InsertDrawSeperator(0)
#define DSEPERATOR_L(layer)			XAIT::Common::Debug::Trace::InsertDrawSeperator(layer)

#define DTRIANGLE3D_L(a,b,c,layer)			XAIT::Common::Debug::Trace::DrawTriangle3D(a,b,c,layer)
#define DBOX3D_L(min_val,max_val,layer)		XAIT::Common::Debug::Trace::DrawBox3D(min_val,max_val,layer)
#define SETDCOLOREX_L(r,g,b,a,layer)		XAIT::Common::Debug::Trace::SetDrawColorEx(r,g,b,a,layer)


#define XAIT_DEFINED_TRACE

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#endif 
#include <windows.h>

#include <stdio.h>
#if (defined _MSC_VER && _MSC_VER > 1400)
	#include <crtdefs.h>
#endif
#include <sstream>

#include <xait/common/FundamentalTypes.h>
#include <xait/common/math/Vec2.h>
#include <xait/common/math/Vec3.h>
#include <xait/common/DLLDefines.h>


namespace XAIT
{
	namespace Common
	{
		namespace Debug
		{
			class XAIT_COMMON_DLLAPI Trace
			{
			public:
				enum XAITraceType 
				{ 
					TRACE_ERROR						= 0x00, 
					TRACE_MESSAGE					= 0x01,
					TRACE_XAIDEBUG					= 0x02,
					TRACE_TRACEINTERN_ERROR			= 0x03,
					TRACE_TRACEINTERN_MESSAGE		= 0x04,
					TRACE_DRAWSETCOLOR				= 0x05,
					TRACE_DRAWSETDMODE				= 0x06,
					TRACE_DRAWLINE2D				= 0x07,
					TRACE_DRAWPOINT2D				= 0x08,
					TRACE_DRAWSEPERATOR				= 0x09,
					TRACE_DRAWBOX2D					= 0x0A,
					TRACE_DRAWCIRCLE2D				= 0x0B,
					TRACE_DRAWSETLAYERNAME			= 0x0C,
					TRACE_DRAWTEXT2D				= 0x0D,
					TRACE_CLEARDISPLAY2D			= 0x0E,
					TRACE_REPAINTDISPLAY2D			= 0x0F,
					TRACE_CLEARLAYER				= 0x10,
					TRACE_DRAWBOX3D					= 0x11,
					TRACE_DRAWSETCOLOREX			= 0x12,
					TRACE_DRAWTRIANGLE3D			= 0x13,
					TRACE_FLUSH						= 0x14,
				};

				enum LineStyle 
				{
					LSTYLE_SOLID,		// solid lines
					LSTYLE_DASH,		// dashed lines
					LSTYLE_DOT,			// dotted lines
					LSTYLE_ARROW_SOLID,	// solid arrow
				};

				enum BoxStyle 
				{
					BSTYLE_SOLID,	// solid filled box
				};

				enum CircleStyle
				{
					CSTYLE_SOLID,
					CSTYLE_DASH,
					CSTYLE_DOT,			// dotted lines
				};

				enum DrawMode 
				{
					DMODE_ALL,			//!< draw all objects in the layer
					DMODE_SINGLE,		//!< draw single selected objects with delta around him
					DMODE_SINGLE_SEP,	//!< draw single selected objects, where one object is a list of elements, seperated by a seperator
					DMODE_BEFORE		//!< draw all objects from 0 to current
				};

				struct XAIDebugMessage 
				{
					int		objectID;
					int		nameLen;	// name length with nulltermination
					int		typeID;		// typeID of data
				};

				//struct MSGClearDisplay2D
				//{
				//};

				struct MSGClearLayer
				{
					int32	layerID; //!< layer to be cleared
				};

				struct MSGSetColor 
				{
					float32		r;			//!< red
					float32		g;			//!< green
					float32		b;			//!< blue
					int32		layerID;	//!< layer to modify
				};

				struct MSGSetDrawMode 
				{
					DrawMode	dMode;		//!< new drawmode
					int32		layerID;
				};

				struct MSGDrawLine2D 
				{
					Math::Vec2f		a;			//!< start of line
					Math::Vec2f		b;			//!< end of line
					LineStyle		lineStyle;	//!< line style (solid,dash,dot)
					int32			layerID;	//!< layer in which to draw
				};

				struct MSGDrawPoint2D 
				{
					Math::Vec2f		pos;		//!< position of point
					int32			layerID;	//!< layer in which to draw
				};

				struct MSGInsertDrawSeperator 
				{
					int32		layerID;	//!< layer where to insert a seperator
				};

				struct MSGDrawBox2D 
				{
					Math::Vec2f		a;			//!< upper left edge
					Math::Vec2f		b;			//!< lower right edge
					BoxStyle		boxStyle;	//!< drawing style of the box
					int32			layerID;	//!< layer in which to draw
				};

				struct MSGDrawCircle2D 
				{
					Math::Vec2f		mCenter;		//!< center
					float32			mRadius;		//!< radius of circle
					CircleStyle		mCircleStyle;	//!< drawing style of the box
					int32			mLayerID;		//!< layer in which to draw
				};

				struct MSGDrawText2D
				{
					enum 
					{
						MAX_TEXT_SIZE= MAX_MESSAGE_LENGTH - 32,		//!< (textsize reduced by the size of the member vars + align)
					};

					Math::Vec2f		mTopLeft;				//!< top left corner of the text
					int32			mLayerID;				//!< layer in which to draw
					char			mText[MAX_TEXT_SIZE];	//!< text to display 
				};

				struct MSGSetLayerName 
				{
					enum { MaxNameLength = 30 };

					char		mName[MaxNameLength];		//!< string containing the name
					int32		mLayerID;					//!< layer which should receive a name
				};

				struct MSGDrawBox3D
				{
					float32		mMin[3];
					float32		mMax[3];
					int32		mLayerID;
				};

				struct MSGSetColorEx
				{
					float mR;
					float mG;
					float mB;
					float mA;
					int mLayerID;
				};

				struct MSGDrawTriangle3D
				{
					float32		mA[3];
					float32		mB[3];
					float32		mC[3];
					int32		mLayerID;
				};

				enum XAIDebugType 
				{
					XAI_INT, //GInt32  
					XAI_FLOAT, //float32  
					XAI_VEC3F,  
					XAI_MATRIX4x4, 
					XAI_MATRIX3x3, 
					XAI_VEHICLE_INFO, 
					XAI_STRING 
				};

				static std::stringstream*	msgstream;
				static std::stringstream*	errorstream;

				//! \brief initialize the debugging class
				//! \remark THIS MUST BE RUN BEFORE ANY CALLS TO THE DEBUG CLASS ARE ALLOWED
				static void init();
				static void destroy();

				static short unsigned int Error(const char* des, const char* fmt, ...);
				static short unsigned int Message(const char* des, const char* fmt, ...);
				
				static short unsigned int Error(const char* fmt, ...);
				static short unsigned int Message(const char* fmt, ...);

				static short unsigned int Message(const std::stringstream &msg);
				static short unsigned int Error(const std::stringstream &msg);

				static short unsigned int TraceMessage(const XAITraceType type, const std::stringstream& msg);

				static void XAIMessage(const int objectID, const char* name, const int typeID, void* data);

				// --------------------------------------------------- 2D Drawing ------------------------------------------------------

				//! \brief set the drawing color and penstyle for the next things to draw
				//! \param r		red color component [0..1]
				//! \param g		green color component [0..1]
				//! \param b		blue color component [0..1]
				//! \param layerID	id of the layer for which you want to change the color or pen style (default layer 0)
				static void SetDrawColor(const float32 r, const float32 g, const float32 b,const int32 layerID= 0);

				//! \brief set the drawingmode for a layer (this could also be done in the aitrace tool)
				//! \param dMode	drawmode
				//! \param layerID	id of the layer to change
				static void SetDrawMode(const DrawMode dMode, const int32 layerID= 0);

				//! \brief set a name for a layer in the aitrace tool
				//! \param name		name to set (length of the name will be restricted by MSGSetLayerName::MaxNameLength)
				//! \param layerID	id of the layer to modify
				static void SetLayerName(const char* name, const int32 layerID);

				//! \brief draw a vector to the 2d display
				//! \param a		start of line
				//! \param b		end of line
				//! \param lStyle	line style (solid,dash,dot)
				//! \param layerID	id of the layer in which you want to draw (default layer 0)
				static void DrawLine2D(const Math::Vec2f& a, const Math::Vec2f& b, const LineStyle lStyle, const int32 layerID= 0);

				//! \brief draw a point in the 2d display
				//! \param pos		position of the point to draw
				//! \param layerID	id of the layer in which you want to draw (default layer 0)
				static void DrawPoint2D(const Math::Vec2f& pos, const int32 layerID= 0);

				//! \brief draw a box in the 2d display
				//! \param a		upper left edge
				//! \param b		lower right edge
				//! \param bStyle	box drawing style
				//! \param layerID	id of the layer in which you want to draw (default layer 0)
				static void DrawBox2D(const Math::Vec2f& a, const Math::Vec2f& b, const BoxStyle bStyle, const int32 layerID= 0);

				//! \brief draw a circle in the 2d display
				//! \param center	center of the circle
				//! \param radius   radius of the circle
				//! \param cStyle	circle drawing style
				//! \param layerID	id of the layer in which you want to draw (default layer 0)
				static void DrawCircle2D(const Math::Vec2f& center, const float32 radius, const CircleStyle cStyle, const int32 layerID= 0);

				//! \brief draw text in the 2d display
				//! \param posTopLeft	top left position of the text (not the position at the bottom of the first character)
				//! \param text			text to display
				//! \param layerID	id of the layer in which you want to draw (default layer 0)
				static void DrawText2D(const Math::Vec2f& posTopLeft, const char* text, const int32 layerID= 0);

				//! \brief insert a seperator into a layer
				//! \param layerID	id of the layer in which to insert the seperator
				static void InsertDrawSeperator(const int32 layerID= 0);

				//! \brief clear the 2D display
				static void ClearDisplay2D();

				//! \brief repaint the 2D display
				static void RepaintDisplay2D();

				//! \brief clear the given layer
				//! \param layerID id of the layer to be cleared
				static void ClearLayer(int32 layerID);

				// --------------------------------------------- Draw 3D ----------------------------------------------------------

				//! \brief Draw a AABB from min to max
				static void DrawBox3D(const Math::Vec3f& min, const Math::Vec3f& max, const int layerID= 0);

				static void DrawTriangle3D(const Math::Vec3f& a,const Math::Vec3f& b,const Math::Vec3f& c, const int layerID= 0);

				static void SetDrawColorEx(const float red, const float green, const float blue, const float alpha, const int layerID= 0);

				static void Flush();

				// --------------------------------------------- Message Box Error ------------------------------------------------

				// error handling
				static void displayError(const std::stringstream &msg, const char* caption);
				static void displayErrorAndDie(const std::stringstream &msg, const char* caption);
				static void displayMessage(const std::stringstream &msg, const char* caption);

			private:
				static short unsigned int trace(COPYDATASTRUCT* lcds);
				template<typename T_MSG>
				static short unsigned int traceMsg(XAITraceType messageID,T_MSG* msgPtr)
				{
					COPYDATASTRUCT cds;
					cds.dwData= messageID;
					cds.cbData= sizeof(T_MSG);
					cds.lpData= (void*)msgPtr;

					return trace(&cds);
				}

				static char*	m_writestr;
			}; 

		}




	} // namespace Common


}

#else

#include <iostream>

#ifndef XAIT_SHIPPING
	#define TRACE_MESSAGE(msg) std::cout << "MSG: " << msg << std::endl;
	#define TRACE_ERROR(msg) std::cerr << "ERR: " << msg << std::endl;
#else
	#define TRACE_MESSAGE(msg)
	#define TRACE_ERROR(msg)
#endif

#define DISPLAYERROR(msg,caption) 
#define DISPLAYERRORANDDIE(msg,caption)
#define DISPLAYMESSAGE(msg,caption)

#define USE_TRACE(cmd)

#define DLINE2D_SLD(a,b)
#define DLINE2D_DSH(a,b)
#define DLINE2D_DOT(a,b)

#define DARROW2D_SLD(a,b)

#define DPOINT2D(pos)	

#define DBOX2D(a,b)		

#define DCIRCLE2D_SLD(center,radius)
#define DCIRCLE2D_DSH(center,radius)
#define DCIRCLE2D_DOT(center,radius)

#define DTEXT2D(posTopLeft,text)	

#define SETDCOLOR(r,g,b)

#define CLEARDISPLAY2D()
#define REPAINTDISPLAY2D()
#define CLEARLAYER(layer)
#define CLEARALLLAYER()

#define DTRIANGLE3D(a,b,c)			
#define DBOX3D(min_val,max_val)		
#define SETDCOLOREX(r,g,b,a)		
#define DFLUSH()					


// draw functions for extended drawing, using different layers

#define DLINE2D_SLD_L(a,b,layer)
#define DLINE2D_DSH_L(a,b,layer)
#define DLINE2D_DOT_L(a,b,layer)

#define DARROW2D_SLD_L(a,b,layer)

#define DCIRCLE2D_SLD_L(center,radius,layer)
#define DCIRCLE2D_DSH_L(center,radius,layer)
#define DCIRCLE2D_DOT_L(center,radius,layer)

#define DTEXT2D_L(posTopLeft,text,layer)	


#define DPOINT2D_L(pos,layer)

#define SETDCOLOR_L(r,g,b,layer)	
#define SETDLAYERNAME(name,layer)	
#define SETDMODE_ALL_L(layer)		
#define SETDMODE_SINGLE_L(layer)	
#define SETDMODE_BEFORE_L(layer)	
#define SETDMODE_SINGLE_SEP_L(layer)

#define DSEPERATOR()			
#define DSEPERATOR_L(layer)		

#define DTRIANGLE3D_L(a,b,c,layer)			
#define DBOX3D_L(min_val,max_val,layer)		
#define SETDCOLOREX_L(r,g,b,a,layer)		



// To keep interface consistent we need an empty Trace class for all other builds...
namespace XAIT
{
	namespace Common
	{
		namespace Debug
		{
			class Trace
			{
			public:
				static void init() {}
				static void destroy() {}
			};
		}
	}
}

#endif



