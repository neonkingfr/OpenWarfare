// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/debug/LogReport.h>
#include <xait/common/SharedPtr.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debug
		{
			class LogDispatcher
			{
			public:
				virtual ~LogDispatcher() {};
				virtual void handleLogReport(const LogReport& report)= 0;
			};
		}
	}
}

