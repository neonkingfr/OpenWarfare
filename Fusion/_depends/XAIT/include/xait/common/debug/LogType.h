// (C) xaitment GmbH 2006-2012

#pragma once 

namespace XAIT
{
	namespace Common
	{
		namespace Debug
		{
			//! \brief different log types
			//! \remark log types are ordered in relevance of their importance (higher bit number, higher importance)
			enum LogType
			{
				LT_DEBUG_MESSAGE	= 0x01,		//!< messages for debugging a class
				LT_DEBUG_ERROR		= 0x02,		//!< errors in function calls that are assigned with proper return types, but should be somehow reported.
				LT_MESSAGE			= 0x04,		//!< messages which report about the current state of a class
				LT_WARNING			= 0x08,		//!< warning which could result in an error at some other place
				LT_ERROR			= 0x10,		//!< real error, which is not intended but program execution can continue (file not found ...)
				LT_MAX_LOG_BIT		= 5,		//!< PAY ATTENTION: maximum log bit, must be updated if new log flags added 
				LT_LOG_ALL			= 0xFF,		//!< combination flag that logs all messages
				LT_LOG_NONE			= 0x00,		//!< combination for no logs at all
			};
		}
	}
}

