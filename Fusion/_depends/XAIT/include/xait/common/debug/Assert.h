// (C) xaitment GmbH 2006-2012

#pragma once

// ------------------------------ Settings -----------------------------------------------------

// Define if you want enable, but do NOT define it in this file, since this file is used over several
// different projects. 

// XAIT_ASSERT_TRACE_ASSERTS		if defined, assertions will be traced to aitrace
// XAIT_ASSERT_FORCE_DBG_ASSERTS	enable debug assertions, which are normally disabled in release mode
// XAIT_ASSERT_DISABLE_ALL			if defined, all assertions will be disabled (this disables assertions
//									even if XAIT_ASSERT_FORCE_DBG_ASSERTS is defined)

// ---------------------------------------------------------------------------------------------

#ifndef XAIT_ASSERT_DISABLE_ALL

#include <xait/common/FundamentalTypes.h>
#include <xait/common/DLLDefines.h>
#include <sstream>

namespace XAIT
{
	namespace Common
	{
		namespace Debug
		{
			XAIT_COMMON_DLLAPI void ShowAssertMessage(const char* message, const char* fileName, uint32 lineNumber);
		}
	}
}

// ------------------------------ assert functionality -----------------------------------------

#define _ASSERT_DEF_STREAM			std::stringstream _assert_string
#define _ASSERT_STREAM				_assert_string
#define _ASSERT_GET_STREAM_STR		_assert_string.str().c_str()

#define X_ASSERT(_Expression)					{(void)( (!!(_Expression)) || (XAIT::Common::Debug::ShowAssertMessage(#_Expression, __FILE__, __LINE__), 0) );}
#define X_ASSERT_MSG(_Expression, _ErrorMsg)	\
{if(!(_Expression)) \
	{_ASSERT_DEF_STREAM;\
	_ASSERT_STREAM << std::dec << #_Expression "\nError Message: " << _ErrorMsg;\
	XAIT::Common::Debug::ShowAssertMessage(_ASSERT_GET_STREAM_STR, __FILE__, __LINE__);}}

#define XAIT_FAIL(_ErrorMsg)\
	{_ASSERT_DEF_STREAM;\
	_ASSERT_STREAM << std::dec << "\nError Message: " << _ErrorMsg;\
	XAIT::Common::Debug::ShowAssertMessage(_ASSERT_GET_STREAM_STR, __FILE__, __LINE__);}

#define X_ASSERT_USE(command)	command

// ------------------------- Debug Asserts ----------------------------------------------------

#if (defined XAIT_DEBUG) || (defined XAIT_ASSERT_FORCE_DBG_ASSERTS)

	#define X_ASSERT_DBG(_Expression)					X_ASSERT(_Expression)
	#define X_ASSERT_MSG_DBG(_Expression,_ErrorMsg)		X_ASSERT_MSG(_Expression,_ErrorMsg)
	#define XAIT_FAIL_DBG(_ErrorMsg)					XAIT_FAIL(_ErrorMsg)
	#define X_ASSERT_USE_DBG(command)					command

#else

	#define X_ASSERT_DBG(_Expression)				{}
	#define X_ASSERT_MSG_DBG(_Expression,_ErrorMsg)		{}
	#define XAIT_FAIL_DBG(_ErrorMsg)	{}
	#define X_ASSERT_USE_DBG(command)	{}

#endif	// XAIT_DEBUG

// ---------------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------------
// Disable all asserts
// --------------------------------------------------------------------------------------------

#else	// XAIT_ASSERT_DISABLE_ALL

#define X_ASSERT(_Expression)	{}
#define X_ASSERT_DBG(_Expression) {}
#define X_ASSERT_MSG(_Expression, _ErrorMsg)	{}
#define X_ASSERT_MSG_DBG(_Expression,_ErrorMsg)		{}
#define XAIT_FAIL(_ErrorMsg)	{}
#define XAIT_FAIL_DBG(_ErrorMsg)	{}
#define X_ASSERT_USE(command)	{}
#define X_ASSERT_USE_DBG(command)	{}


#endif	// XAIT_ASSERT_DISABLE_ALL

#define X_ASSERT_MSG1(_Expression,_ErrorMsg,_Param1)	\
	X_ASSERT_MSG(_Expression,_ErrorMsg << "\n" #_Param1 " = " << (_Param1))
#define X_ASSERT_MSG2(_Expression,_ErrorMsg,_Param1,_Param2)	\
	X_ASSERT_MSG(_Expression,_ErrorMsg << "\n" #_Param1 " = " << (_Param1) << "\n" #_Param2 " = " << (_Param2) )
#define X_ASSERT_MSG3(_Expression,_ErrorMsg,_Param1,_Param2,_Param3)	\
	X_ASSERT_MSG(_Expression,_ErrorMsg << "\n" #_Param1 " = " << (_Param1) << "\n" #_Param2 " = " << (_Param2) << "\n" #_Param3 " = " << (_Param3))
#define X_ASSERT_MSG4(_Expression,_ErrorMsg,_Param1,_Param2,_Param3,_Param4)	\
	X_ASSERT_MSG(_Expression,_ErrorMsg << "\n" #_Param1 " = " << (_Param1) << "\n" #_Param2 " = " << (_Param2) << "\n" #_Param3 " = " << (_Param3) << "\n" #_Param4 " = " << (_Param4))

#define X_ASSERT_MSG_DBG1(_Expression,_ErrorMsg,_Param1)	\
	X_ASSERT_MSG_DBG(_Expression,_ErrorMsg << "\n" #_Param1 " = " << (_Param1))
#define X_ASSERT_MSG_DBG2(_Expression,_ErrorMsg,_Param1,_Param2)	\
	X_ASSERT_MSG_DBG(_Expression,_ErrorMsg << "\n" #_Param1 " = " << (_Param1) << "\n" #_Param2 " = " << (_Param2) )
#define X_ASSERT_MSG_DBG3(_Expression,_ErrorMsg,_Param1,_Param2,_Param3)	\
	X_ASSERT_MSG_DBG(_Expression,_ErrorMsg << "\n" #_Param1 " = " << (_Param1) << "\n" #_Param2 " = " << (_Param2) << "\n" #_Param3 " = " << (_Param3))
#define X_ASSERT_MSG_DBG4(_Expression,_ErrorMsg,_Param1,_Param2,_Param3,_Param4)	\
	X_ASSERT_MSG_DBG(_Expression,_ErrorMsg << "\n" #_Param1 " = " << (_Param1) << "\n" #_Param2 " = " << (_Param2) << "\n" #_Param3 " = " << (_Param3) << "\n" #_Param4 " = " << (_Param4))


// ---------------------------------------------------------------------------------------------

// WORK-IN-PROGRESS-ASAP:

#if XAIT_OS == XAIT_OS_PS3
#define XAIT_PLATFORM_TODO_PS3(msg) 	XAIT_FAIL("TODO PS3: " << msg)
#define XAIT_PLATFORM_TODO_PS4(msg)
#define XAIT_PLATFORM_TODO_WII(msg)
#define XAIT_PLATFORM_TODO_LINUX(msg)
#elif XAIT_OS == XAIT_OS_PS4
#define XAIT_PLATFORM_TODO_PS4(msg) 	XAIT_FAIL("TODO PS4: " << msg)
#define XAIT_PLATFORM_TODO_PS3(msg)
#define XAIT_PLATFORM_TODO_WII(msg)
#define XAIT_PLATFORM_TODO_LINUX(msg)
#elif XAIT_OS == XAIT_OS_WII
#define XAIT_PLATFORM_TODO_WII(msg) 	XAIT_FAIL("TODO WII: " << msg)
#define XAIT_PLATFORM_TODO_PS4(msg)
#define XAIT_PLATFORM_TODO_PS3(msg)
#define XAIT_PLATFORM_TODO_LINUX(msg)
#elif XAIT_OS == XAIT_OS_LINUX
#define XAIT_PLATFORM_TODO_LINUX(msg)	XAIT_FAIL("TODO LINUX: " << msg)
#define XAIT_PLATFORM_TODO_PS4(msg)
#define XAIT_PLATFORM_TODO_WII(msg) 
#define XAIT_PLATFORM_TODO_PS3(msg)
#elif XAIT_OS == XAIT_OS_MAC
#define XAIT_PLATFORM_TODO_LINUX(msg)	XAIT_FAIL("TODO MAC: " << msg)
#define XAIT_PLATFORM_TODO_PS4(msg)
#define XAIT_PLATFORM_TODO_WII(msg) 
#define XAIT_PLATFORM_TODO_PS3(msg)
#endif

