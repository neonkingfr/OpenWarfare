// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/String.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debug
		{
			//! \brief a class for showing a message box, has only static methods
			class XMessageBox
			{
			public:
				enum ShowButton
				{
					SBTTN_OK,
					SBTTN_OKCANCEL,
					SBTTN_YESNO,
					SBTTN_YESNOCANCEL,
				};

				enum ShowIcon
				{
					SICON_NOICON,
					SICON_WARNING,
					SICON_ERROR,
				};

				enum PressedButton
				{
					PBTTN_OK,
					PBTTN_YES,
					PBTTN_NO,
					PBTTN_CANCEL,
				};

				//! \brief show a simple message box with an ok button
				static void show(const Common::String& message);

				//! \brief show a message box with defined message,caption and buttons
				//! \param message		message to show (can contain newlines)
				//! \param caption		caption of the message box window
				//! \param showButton	buttons which should be shown in the messagebox
				//! \returns the button which was pressed
				//! \remark The close button of the window will result in PBTTN_CANCEL if a cancel button is available
				static PressedButton show(const Common::String& message, 
										  const Common::String& caption, 
										  const ShowButton showButton,
										  const ShowIcon icon= SICON_NOICON);
			};
		}
	}
}
