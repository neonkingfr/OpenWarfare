// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/exceptions/Exception.h>


namespace XAIT
{
	namespace Common
	{
		namespace Exceptions
		{
			//! \brief Exception if a requested feature is not supported by a class
			class NotSupportedException : public Exception
			{
			public:
				NotSupportedException(const Common::String& errorMsg)
					: Exception(errorMsg)
				{}

				virtual Common::String getExceptionName() const
				{
					return "NotSupportedException";
				}
			};
		}
	}
}
