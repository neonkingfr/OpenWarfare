// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/exceptions/ArgumentException.h>

namespace XAIT
{
	namespace Common
	{
		namespace Exceptions
		{
			//! \brief Exception if an argument is out of it valid range
			class ArgumentOutOfRangeException : public ArgumentException
			{
			public:
				ArgumentOutOfRangeException(const Common::String& argumentName,
											const Common::String& errorMsg)
				: ArgumentException(argumentName,errorMsg)
				{}


				virtual Common::String getExceptionName() const
				{
					return "ArgumentOutOfRangeException";
				}
			};
		}
	}
}
