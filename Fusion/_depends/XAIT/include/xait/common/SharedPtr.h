// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/FundamentalTypes.h>
#include <xait/common/debug/Assert.h>
#include <xait/common/AutoPtr.h>
#include <xait/common/threading/SpinLock.h>


namespace XAIT
{
	namespace Common
	{
		namespace Helper
		{
			//! \brief this struct is used if you want that the shared pointer gets not initialised
			//!		   (because it is already)
			struct IgnoreInit
			{
				int32 dummy;
			};
		}

		//! \brief the standard object policy uses the delete operator to dispose an object
		class StandartObjectPolicy
		{
		public:
			StandartObjectPolicy() {};
			StandartObjectPolicy(const Common::Helper::IgnoreInit&) {};

			template<typename T>
			inline void dispose(T* object)
			{
				delete object;
			}
		};

		//! \brief the standard array object policy uses the delete[] operator to dispose an object
		class StandartArrayObjectPolicy
		{
		public:
			StandartArrayObjectPolicy() {};
			StandartArrayObjectPolicy(const Common::Helper::IgnoreInit&) {};

			template<typename T>
			inline void dispose(T* object)
			{
				delete[] object;
			}
		};



		//! \brief encapsulated reference count object (int) which can only be manipulated by its
		//! friend classes
		class ReferenceCounter
		{
			template<typename T_OBJECT, typename T_COUNT, T_COUNT T_OBJECT::*F_COUNTP>
			friend class MemberReferenceCount;

			template<typename T_OBJECT, typename T_COUNT, typename T_LOCK, T_COUNT T_OBJECT::*F_COUNTP, T_LOCK T_OBJECT::*F_LOCKP>
			friend class LockedMemberReferenceCount;


			volatile uint32		mRefCount;

			inline ReferenceCounter& operator++()
			{
				++mRefCount;
				return *this;
			}

			inline ReferenceCounter& operator--()
			{
				--mRefCount;
				return *this;
			}

			inline bool operator==(const uint32 count)
			{
				return mRefCount == count;
			}

			inline bool operator==(const ReferenceCounter& count)
			{
				return mRefCount == count.mRefCount;
			}

			inline ReferenceCounter& operator=(const uint32 count)
			{
				mRefCount= count;
				return *this;
			}

		public:
			ReferenceCounter()
				: mRefCount(0)
			{}

			//! \brief get the reference count value
			inline uint32 getRefCount() const
			{
				return mRefCount;
			}
		};

		//! \brief base class which can be used for intrusive reference counted objects
		class ReferenceCounted
		{
		public:
			virtual ~ReferenceCounted() {};

			ReferenceCounter	mRefCount;
		};

		class LockedReferenceCounted
		{
		public:
			typedef ReferenceCounter		RefCountType;
			typedef Threading::SpinLock		RefCountLockType;

			virtual ~LockedReferenceCounted() {};

			RefCountType		mRefCount;
			RefCountLockType	mRefCountLock;
		};


		//! \brief a simple invasive counter, using a member variable to count the instances
		template<typename T_OBJECT= ReferenceCounted,		// type of the object in which the counter resides
				 typename T_COUNT= ReferenceCounter,		// type of the counter
				 T_COUNT T_OBJECT::*F_COUNTP= &ReferenceCounted::mRefCount>	// the location of the counter
		class MemberReferenceCount
		{
		public:
			MemberReferenceCount() {};
			MemberReferenceCount(const Common::Helper::IgnoreInit&) {};

			//! \brief init the member variable counter to one
			inline void init(T_OBJECT* obj)
			{
				(obj->*F_COUNTP) = 1;
			}

			//! \brief nothing to dispose since this is a member variable
			inline void dispose(T_OBJECT*)
			{}

			//! \brief increment counter by one
			inline void increment(T_OBJECT* obj)
			{
				++(obj->*F_COUNTP);
			}

			//! \brief decrement counter by one
			inline void decrement(T_OBJECT* obj)
			{
				--(obj->*F_COUNTP);
			}

			//! \brief test if counter is zero
			inline bool isZero(T_OBJECT* obj) const
			{
				return (obj->*F_COUNTP) == 0;
			}

			//! \brief test if counter is one
			inline bool isOne(T_OBJECT* obj) const
			{
				return (obj->*F_COUNTP) == 1;
			}

			//! \brief take a lock on the counter
			//! \remark SimpleRefCount is not thread safe so lock is empty
			inline void lock(T_OBJECT*)
			{}

			//! \brief unlock the counter
			//! \remark SimpleRefCount is not thread safe so lock is empty
			inline void unlock(T_OBJECT*)
			{}

		};

		//! \brief a simple invasive counter, using a member variable to count the instances
		template<
			typename T_OBJECT= LockedReferenceCounted,								// type of the object in which the counter resides
			typename T_COUNT= LockedReferenceCounted::RefCountType,					// type of the counter
			typename T_LOCK= LockedReferenceCounted::RefCountLockType,				// type of the lock
			T_COUNT T_OBJECT::*F_COUNTP= &LockedReferenceCounted::mRefCount,		// the location of the counter
			T_LOCK  T_OBJECT::*F_LOCKP= &LockedReferenceCounted::mRefCountLock>		// the location of the lock
		class LockedMemberReferenceCount
		{
		public:
			LockedMemberReferenceCount() {};
			LockedMemberReferenceCount(const Common::Helper::IgnoreInit&) {};

			//! \brief init the member variable counter to one
			inline void init(T_OBJECT* obj)
			{
				(obj->*F_COUNTP) = 1;
			}

			//! \brief nothing to dispose since this is a member variable
			inline void dispose(T_OBJECT*)
			{}

			//! \brief increment counter by one
			inline void increment(T_OBJECT* obj)
			{
				++(obj->*F_COUNTP);
			}

			//! \brief decrement counter by one
			inline void decrement(T_OBJECT* obj)
			{
				--(obj->*F_COUNTP);
			}

			//! \brief test if counter is zero
			inline bool isZero(T_OBJECT* obj) const
			{
				return (obj->*F_COUNTP) == 0;
			}

			//! \brief test if counter is one
			inline bool isOne(T_OBJECT* obj) const
			{
				return (obj->*F_COUNTP) == 1;
			}

			//! \brief take a lock on the counter
			//! \remark SimpleRefCount is not thread safe so lock is empty
			inline void lock(T_OBJECT* obj)
			{
				(obj->*F_LOCKP).lock();
			}

			//! \brief unlock the counter
			//! \remark SimpleRefCount is not thread safe so lock is empty
			inline void unlock(T_OBJECT* obj)
			{
				(obj->*F_LOCKP).unlock();
			}

		};

		class SimpleReferenceCount;

		template<typename T_POINTER,
				 typename T_COUNTER_POLICY = SimpleReferenceCount,
				 typename T_OBJECT_POLICY = StandartObjectPolicy>
		class SharedPtr : private T_COUNTER_POLICY, private T_OBJECT_POLICY
		{
			typedef T_COUNTER_POLICY	CP;
			typedef T_OBJECT_POLICY		OP;

			T_POINTER*	mPtr;		//!< pointer to the object

		public:
			typedef T_POINTER*		NativeTypePtr;
			typedef T_POINTER		NativeType;

			//! \brief class for rebinding the shared pointer to a derived class
			template<typename T_POINTER_OTHER>
			struct Rebind
			{
				typedef SharedPtr<T_POINTER_OTHER,T_COUNTER_POLICY,T_OBJECT_POLICY>		Other;
			};
			
			//! \brief default constructor
			inline SharedPtr()
				: mPtr(NULL)
			{}

			//! \brief this construct does not init the shared ptr, use this only if you know, that
			//!		   the memory area of the sharedptr is already initialised (i.e. in the overloaded
			//!		   new operator)
			inline SharedPtr(const Common::Helper::IgnoreInit& dummy)
				: CP(dummy),OP(dummy)
			{}

			//! \brief conversion construction from a built in type
			inline explicit SharedPtr(T_POINTER* ptr)
			{
				this->init(ptr);
			}

			//! \brief construct from an AutoPtr, releases the AutoPtr
			inline explicit SharedPtr(AutoPtr<T_POINTER>& autoPtr)
			{
				this->init(autoPtr.release());
			}

			//! \brief copy constructor
			inline SharedPtr(const SharedPtr& sharedPtr)
				: CP((CP const&)sharedPtr),
				  OP((OP const&)sharedPtr)
			{
				this->attach(sharedPtr);	// copy pointer and increment counter
			}

			//! \brief copy constructor from another shared pointer which is either for a void* or base type pointer
			template<typename S>
			inline SharedPtr(const SharedPtr<S,CP,OP>& sPtr)
				: CP((CP const&)sPtr),
				  OP((OP const&)sPtr),
				  mPtr(sPtr.mPtr)
			{
				if (this->mPtr != NULL)
				{
					CP::lock(this->mPtr);
					CP::increment(this->mPtr);
					CP::unlock(this->mPtr);
				}
			}

			//! \brief destructor
			inline ~SharedPtr()
			{
				this->detach();		// decrement counter (and dispose if counter = 0)
			}

			//! \brief destroy the pointer of there shareptr points
			//! \remark This gives you an error, if there is more than one reference to the object
			void destroy()
			{
				this->detach();

				if (this->mPtr)
				{
#ifndef XAIT_USE_VLD
					XAIT_FAIL_DBG("there are other references to the object, delete it anyways");
#endif
					OP::dispose(this->mPtr);

					this->mPtr= NULL;
				}
			}

			//! \brief check if this is a the last reference
			//! \returns true if the reference count is one.
			//! \remark If the pointer is a NULL pointer it also returns true
			bool isLastReference() const
			{
				if (this->mPtr)
				{
					return CP::isOne(this->mPtr);
				}
				else
				{
					return true;
				}
			}

			//! \brief assign of a built-in pointer
			inline SharedPtr& operator=(T_POINTER* ptr)
			{
				// no shared pointer should point to ptr yet
				X_ASSERT_MSG(ptr != this->mPtr || this->mPtr == NULL,"No shared pointer should pointer to ptr yet");

				this->detach();
				this->init(ptr);

				return *this;
			}

			//! \brief copy assignment (beware of self-assignment)
			inline SharedPtr& operator=(const SharedPtr& other)
			{
				if (this->mPtr != other.mPtr)
				{
					this->detach();
					// copy policies
					CP::operator=((CP const&)other);
					OP::operator=((OP const&)other);

					this->attach(other);
				}
				return *this;
			}

			//! \brief member operator such that it behaves like a normal pointer
			inline T_POINTER* operator->() const
			{
				X_ASSERT_MSG_DBG(mPtr,"Try to dereference a NULL pointer");
				return mPtr;
			}

			//! \brief dereference operator like in normal pointers
			inline T_POINTER& operator*() const
			{
				X_ASSERT_MSG_DBG(mPtr,"Try to dereference a NULL pointer");
				return *mPtr;
			}

			//! \brief get the referenced object
			//! \returns the referenced object, or NULL if nothing referenced
			inline T_POINTER* get() const
			{
				return mPtr;
			}

			//////////////////////////////////////////////////////////////////////////
			///                   Comparison operators

			inline bool operator==(const SharedPtr& sPtr) const
			{
				return sPtr.mPtr == mPtr;
			}

			inline bool operator!=(const SharedPtr& sPtr) const
			{
				return sPtr.mPtr != mPtr;
			}

			inline bool operator <(const SharedPtr& sPtr) const
			{
				return mPtr < sPtr.mPtr;
			}

			inline bool operator >(const SharedPtr& sPtr) const
			{
				return mPtr > sPtr.mPtr;
			}

			inline bool operator!() const
			{
				return mPtr == NULL;
			}

			friend bool operator==(const SharedPtr& sPtr, const T_POINTER* ptr)
			{
				return sPtr.get() == ptr;
			}

			friend bool operator==(const T_POINTER* ptr, const SharedPtr& sPtr)
			{
				return sPtr.get() == ptr;
			}

			friend bool operator!=(const SharedPtr& sPtr, const T_POINTER* ptr)
			{
				return sPtr.get() != ptr;
			}

			friend bool operator!=(const T_POINTER* ptr, const SharedPtr& sPtr)
			{
				return sPtr.get() != ptr;
			}

		private:
			template<typename T2, typename CP2, typename OP2>
			friend class SharedPtr;

			//! \brief init shared pointer from ordinary pointer
			void init(T_POINTER* ptr)
			{
				if (ptr != NULL)
				{
					CP::init(ptr);
				}
				this->mPtr= ptr;
			}

			//! \brief copy pointer and increment pointer by one
			void attach(const SharedPtr& other)
			{
				this->mPtr = other.mPtr;

				if (this->mPtr != NULL)
				{
					CP::lock(this->mPtr);
					CP::increment(this->mPtr);
					CP::unlock(this->mPtr);

					//todo remove
					X_ASSERT_MSG_DBG(!(CP::isZero(this->mPtr)),"reference count was < 0");
				}
			}

			//! \brief decrement counter by one and dispose if last owner
			void detach()
			{
				if (this->mPtr != NULL)
				{
					CP::lock(this->mPtr);
					CP::decrement(this->mPtr);
					const bool isZero= CP::isZero(this->mPtr);
					CP::unlock(this->mPtr);

					if (isZero)
					{
						// dispose counter
						CP::dispose(this->mPtr);

						// dispose object
						OP::dispose(this->mPtr);

						this->mPtr= NULL;
					}
				}
			}
		};
	}
}
