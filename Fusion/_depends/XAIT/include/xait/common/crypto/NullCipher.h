// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/crypto/BlockCipher.h>

namespace XAIT
{
	namespace Common
	{
		namespace Crypto
		{
			//! \brief A block cipher without doing any encryption, for testing purposes
			class NullCipher : public BlockCipher
			{
			public:
				//! \brief default constructor
				//! \param blockSize	block size of the block cipher which should be tested
				NullCipher(const uint32 blockSize= 8)
					: BlockCipher(blockSize)
				{}

				void encryptBlock(uint08* encData, const uint08* decData)
				{
					memcpy(encData,decData,mBlockSize);
				}

				void decryptBlock(uint08* decData, const uint08* encData)
				{
					memcpy(decData,encData,mBlockSize);
				}
			};
		}
	}
}
