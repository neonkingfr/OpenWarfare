// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/io/Stream.h>
#include <xait/common/SharedPtr.h>

namespace XAIT
{
	namespace Common
	{
		namespace Crypto
		{
			class BlockCipher;

			//! The crypto stream is a stream that uses a blockcipher to encode streams passed through it. This stream
			//! can be either read or write, not both at the same time.
			//! \brief crypto stream
			class CryptoStream : public IO::Stream
			{
			public:
				//! \brief construct and initialized a crypto stream on another stream which will receive the encrypted data
				//! \param cipher		block cipher which should be used
				//! \param name			name of the stream
				//! \exception ArgumentNullException	thrown if either cipher or encStream is a NULL pointer
				CryptoStream(const SharedPtr<BlockCipher>&	cipher, const Common::String& name);

				//! \brief destructor
				~CryptoStream();

				//! \brief open stream
				//! \param encStream	stream with the encrypted data
				//! \param writable		if true the stream is only writable. If it is false
				//!						you can only read from the stream. This flag also implies that
				//!						the given encStream is either opened for write or read. The crypto
				//!						stream cannot be read and write, so you can only toggle between these
				//!						two. 
				//! \exception ArgumentNullException	thrown if encStream is a NULL pointer
				void open(IO::Stream* encStream, const bool writable);

				// -------------- derived methods ---------------------------
				const Common::String& getName() const;
				bool canRead() const;
				bool canWrite() const;
				bool canSeek() const;
				bool isClosed() const;
				uint64 getLength() const;
				uint64 getPosition() const;
				uint64 read(void* dstBuffer, const uint64 numBytes);
				void write(const void* srcBuffer, const uint64 numBytes);
				uint64 seek(const int64 offset, const SeekOrigin origin);
				void flush();
				void close();

			protected:
				SharedPtr<BlockCipher>	mCipher;		//!< pointer to the cipher algo
				Common::String			mName;			//!< name of this stream
				IO::Stream*				mEncStream;		//!< stream with encrypted data
				bool					mReadOnly;		//!< set if the stream is read only

				uint64					mPlainStreamSize;	//!< complete size of the decrypted stream
				uint64					mPlainStreamPos;	//!< current position in the stream

				uint08*					mPlainBuffer;		//!< buffer holding decrypted data
				uint08*					mEncBuffer;			//!< buffer holding encrypted data

				uint32					mPlainBufferPos;	//!< position in the decrypted data
				uint32					mPlainBufferSize;	//!< amount of the in the decrytion buffer (max size is mEncryptBlockSize)

				//! \remark Method should only be called at file open or file close (mixes up file position)
				void writePlainStreamSize(uint64 streamSize);

				//! \remark Method should only be called at file open (mixes up file position)
				uint64 readPlainStreamSize();

			};
		}
	}
}
