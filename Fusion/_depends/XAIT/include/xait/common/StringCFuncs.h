// (C) xaitment GmbH 2006-2012

#pragma once 

// ---------------------------------------------------------------------------------------
// A Wrapper for the c string functions.
// The wrapper distincts between unicode and multibyte chars
//
// Author: Markus Wilhelm
//
// ----------------------------------------------------------------------------------------
#include <stdio.h>
#include <ctype.h>
#include <wchar.h>
#include <string.h>
#include <stdlib.h>
//F5MOD-check: PS3 only?
#include <wctype.h>
//F5MOD-WII?
#include <cstdlib>

#include <xait/common/Platform.h>
#include <xait/common/platform/RuntimeLib.h>
#include <xait/common/debug/Assert.h>
#include <xait/common/FundamentalTypes.h>
#include <xait/common/AutoPtr.h>


namespace XAIT
{
	namespace Common
	{
		template<typename CHAR_TYPE>
		class StringCFuncs
		{
			
		};

		template<>
		class StringCFuncs<char>
		{
		public:
			inline static uint32 StrLen(const char* str)
			{
				return (uint32)strlen(str);
			}

			inline static bool StrEqual(const char* str1, const char* str2)
			{
				return !strcmp(str1,str2);
			}

			inline static int32 StrCmp(const char* str1, const char* str2)
			{
				return strcmp(str1,str2);
			}

			inline static char ToUpper(const char c)
			{
				return (char)toupper(c);
			}

			inline static char ToLower(const char c)
			{
				return (char)tolower(c);
			}

			inline static bool IsUpper(const char c)
			{
				return isupper(c) > 0;
			}
			
			inline static bool IsLower(const char c)
			{
				return islower(c) > 0;
			}

			inline static bool IsWhiteSpace(const char c)
			{
				return isspace(c) > 0;
			}

			inline static float32 ToFloat32(const char* str)
			{
				return (float32)atof(str);
			}

			inline static float64 ToFloat64(const char* str)
			{
				return atof(str);
			}

			inline static int32 ToInt32(const char* str)
			{
				return atoi(str);
			}

			inline static int64 ToInt64(const char* str)
			{
#if XAIT_OS == XAIT_OS_WIN || XAIT_OS == XAIT_OS_XBOX360 || XAIT_OS == XAIT_OS_XBOX360EXT
				return _atoi64(str);
#else
				return atoll(str);
#endif
			}

			inline static uint32 ToUInt32(const char* str)
			{
#if XAIT_OS == XAIT_OS_WIN || XAIT_OS == XAIT_OS_XBOX360 || XAIT_OS == XAIT_OS_XBOX360EXT
				return (uint32)_atoi64(str);
#else
				return (uint32)atoll(str);
#endif
			}

			inline static uint64 ToUInt64(const char* str)
			{
				uint64 value;
#if XAIT_OS == XAIT_OS_WIN || XAIT_OS == XAIT_OS_XBOX360 || XAIT_OS == XAIT_OS_XBOX360EXT
				sscanf_s(str,"%I64u",&value);
#else
				sscanf_s(str,"%llu",&value);
#endif

				return value;
			}


			//! \brief convert a arbitray character to the character type of this template
			inline static char ConvertChar(const wchar_t wc)
			{
				char c;
				int i;
				wctomb_s( &i, &c, sizeof(char), wc );
				return c;
			}

			inline static char ConvertChar(const char c)
			{
				return c;
			}

			inline static void StrCat(char* strA, const uint32 strASize, const char* strB)
			{
				strcat_s(strA,(size_t)strASize,strB);
			}

			inline static void StrCpy(char* dest, const uint32 destSize, const char* src)
			{
				strcpy_s(dest,(size_t)destSize,src);
			}
		};

		template<>
		class StringCFuncs<wchar_t>
		{
		public:
			inline static uint32 StrLen(const wchar_t* str)
			{
				return (uint32)wcslen(str);
			}

			inline static bool StrEqual(const wchar_t* str1, const wchar_t* str2)
			{
				return !wcscmp(str1,str2);
			}

			inline static int32 StrCmp(const wchar_t* str1, const wchar_t* str2)
			{
				return wcscmp(str1,str2);
			}


			inline static wchar_t ToUpper(const wchar_t c)
			{
				return towupper(c);
			}

			inline static wchar_t ToLower(const wchar_t c)
			{
				return towlower(c);
			}

			inline static bool IsUpper(const wchar_t c)
			{
				return iswupper(c) > 0;
			}

			inline static bool IsLower(const wchar_t c)
			{
				return iswlower(c) > 0;
			}

			inline static bool IsWhiteSpace(const wchar_t c)
			{
				return iswspace(c) > 0;
			}

			inline static float64 ToFloat64(const wchar_t* str)
			{
				//F5MOD:
				#if (XAIT_OS == XAIT_OS_WIN) || (XAIT_OS == XAIT_OS_XBOX360) || (XAIT_OS == XAIT_OS_XBOX360EXT)
					return _wtof(str);
				#else
					XAIT_PLATFORM_TODO_PS3("_wtof");
					XAIT_PLATFORM_TODO_WII("_wtof");
					return 0;
				#endif
			}

			inline static int32 ToInt32(const wchar_t* str)
			{
				//F5MOD:
				#if (XAIT_OS == XAIT_OS_WIN) || (XAIT_OS == XAIT_OS_XBOX360) || (XAIT_OS == XAIT_OS_XBOX360EXT)
					return _wtoi(str);
				#else
					XAIT_PLATFORM_TODO_PS3("_wtoi");
					XAIT_PLATFORM_TODO_WII("_wtoi");
					return 0;
				#endif
			}

			inline static int64 ToInt64(const wchar_t* str)
			{
				//F5MOD:
				#if (XAIT_OS == XAIT_OS_WIN) || (XAIT_OS == XAIT_OS_XBOX360) || (XAIT_OS == XAIT_OS_XBOX360EXT)
					return _wtoi64(str);
				#else
					XAIT_PLATFORM_TODO_PS3("_wtoi64");
					XAIT_PLATFORM_TODO_WII("_wtoi64");
					return 0;
				#endif
			}


			//! \brief convert a arbitray character to the character type of this template
			inline static wchar_t ConvertChar(const char c)
			{

				wchar_t wc;
				int success= mbtowc( &wc, &c, MB_CUR_MAX );

				if (success < 0)	// gcc warning optimization
					return wc;

				return wc;
			}

			inline static wchar_t ConvertChar(const wchar_t c)
			{
				return c;
			}

			inline static void StrCat(wchar_t* strA, const uint32 strASize, const wchar_t* strB)
			{
				wcscat_s(strA,(size_t)strASize,strB);
			}

			inline static void StrCpy(wchar_t* dest, const uint32 destSize, const wchar_t* src)
			{
				wcscpy_s(dest,(size_t)destSize,src);
			}

		};

	}
}
