// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/container/Vector.h>
#include <xait/common/clr/Marshalling.h>

// convert the Vector class from common to a .NET List generic 
namespace XAIT
{
	namespace Common
	{
		namespace CLR
		{
			template<typename T_MANAGED>
			class WrapperType<System::Collections::Generic::List<T_MANAGED>^>
			{
			public:
				typedef System::Collections::Generic::List<T_MANAGED>^								Managed;
				typedef XAIT::Common::Container::Vector<typename WrapperType<T_MANAGED>::Native>	Native;
			};

			template<typename T_NATIVE>
			class WrapperType<XAIT::Common::Container::Vector<T_NATIVE> >
			{
			public:
				typedef System::Collections::Generic::List<typename WrapperType<T_NATIVE>::Managed>^	Managed;
				typedef XAIT::Common::Container::Vector<T_NATIVE>										Native;
			};

			template<typename T_ELEMENT>
			XAIT_FORCEINLINE System::Collections::Generic::List<typename WrapperType<T_ELEMENT>::Managed>^ NETConvert(XAIT::Common::Container::Vector<T_ELEMENT>& lstNative)
			{
				System::Collections::Generic::List<typename WrapperType<T_ELEMENT>::Managed>^ lstManaged;
				lstManaged= gcnew System::Collections::Generic::List<typename WrapperType<T_ELEMENT>::Managed>();

				const uint32 nativeSize= lstNative.size();
				lstManaged->Capacity= nativeSize;

				for(uint32 i= 0; i < nativeSize; ++i)
				{
					lstManaged->Add(NETConvert(lstNative[i]));
				}

				return lstManaged;
			}

			template<typename T_MANAGED>
			XAIT_FORCEINLINE XAIT::Common::Container::Vector<typename WrapperType<T_MANAGED>::Native> NETConvert(System::Collections::Generic::List<T_MANAGED>^ lstManaged)
			{
				XAIT::Common::Container::Vector<typename WrapperType<T_MANAGED>::Native> lstNative(Common::Interface::getGlobalAllocator());

				const uint32 managedSize= lstManaged->Count;
				lstNative.reserve(managedSize);

				for(uint32 i= 0; i < managedSize; ++i)
				{
					lstNative.pushBack(NETConvert(lstManaged[i]));
				}

				return lstNative;
			}
		}
	}
}
