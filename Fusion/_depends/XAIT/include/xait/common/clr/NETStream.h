// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/io/Stream.h>

namespace XAIT
{
	namespace Common
	{
		namespace CLR
		{
			class NETStream : public Common::IO::Stream
			{
			private:
				gcroot<System::IO::Stream^> mMStream;
				const Common::String mName;

			public:

				NETStream(System::IO::Stream^ managedStream, const Common::String& name)
					: mMStream(managedStream),mName(name)
				{}

				bool canRead() const
				{
					return mMStream->CanRead;
				}

				bool canWrite() const
				{
					return mMStream->CanWrite;
				}

				bool canSeek() const
				{
					return mMStream->CanSeek;
				}

				bool isClosed() const
				{
					return false;
				}

				uint64 getLength() const
				{
					return (uint64)mMStream->Length;
				}

				uint64 getPosition() const
				{
					return (uint64)mMStream->Position;
				}

				const Common::String& getName() const
				{
					return mName;
				}

				uint64 read(void* dstBuffer, const uint64 numBytes)
				{
					if (numBytes == 0)
						return 0;

					array<System::Byte>^ clrDstBuffer= gcnew array<System::Byte>((int)numBytes);
					int readBytes= mMStream->Read(clrDstBuffer,0,(int)numBytes);

					pin_ptr<System::Byte> p= &clrDstBuffer[0];
					System::Byte* np= p;

					memcpy(dstBuffer,np,(size_t)readBytes);

					return readBytes;
				}


				void write(const void* srcBuffer, const uint64 numBytes)
				{
					if (numBytes == 0)
						return;

					array<System::Byte>^ clrSrcBuffer= gcnew array<System::Byte>((int)numBytes);
					pin_ptr<System::Byte> p= &clrSrcBuffer[0];
					System::Byte* np= p;
					memcpy(np,srcBuffer,(size_t)numBytes);

					mMStream->Write(clrSrcBuffer,0,(int)numBytes);
				}


				virtual uint64 seek(const int64 offset, const SeekOrigin origin)
				{
					switch (origin)
					{
					case SEEKORIG_CUR:
						return (uint64)mMStream->Seek(offset,System::IO::SeekOrigin::Current);
					case SEEKORIG_START:
						return (uint64)mMStream->Seek(offset,System::IO::SeekOrigin::Begin);
					case SEEKORIG_END:
						return (uint64)mMStream->Seek(offset,System::IO::SeekOrigin::End);
					}
					return 0;
				}

				virtual void flush()
				{
					mMStream->Flush();
				}

				virtual void close()
				{
					mMStream->Close();
				}

			};
		}
	}
}