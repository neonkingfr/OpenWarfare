// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/container/List.h>
#include <xait/common/clr/Marshalling.h>

// convert the List class from common to a .NET LinkedList generic
namespace XAIT
{
	namespace Common
	{
		namespace CLR
		{
			template<typename T_MANAGED>
			class WrapperType<System::Collections::Generic::LinkedList<T_MANAGED>^>
			{
			public:
				typedef System::Collections::Generic::LinkedList<T_MANAGED>^					Managed;
				typedef XAIT::Common::Container::List<typename WrapperType<T_MANAGED>::Native>	Native;
			};

			template<typename T_NATIVE>
			class WrapperType<XAIT::Common::Container::List<T_NATIVE> >
			{
			public:
				typedef System::Collections::Generic::LinkedList<typename WrapperType<T_NATIVE>::Managed>^	Managed;
				typedef XAIT::Common::Container::List<T_NATIVE>												Native;
			};

			template<typename T_ELEMENT>
			XAIT_FORCEINLINE System::Collections::Generic::LinkedList<typename WrapperType<T_ELEMENT>::Managed>^ NETConvert(XAIT::Common::Container::List<T_ELEMENT>& lstNative)
			{
				System::Collections::Generic::LinkedList<typename WrapperType<T_ELEMENT>::Managed>^ lstManaged;
				lstManaged= gcnew System::Collections::Generic::LinkedList<typename WrapperType<T_ELEMENT>::Managed>();

				XAIT::Common::Container::List<T_ELEMENT>::Iterator iter= lstNative.begin();
				XAIT::Common::Container::List<T_ELEMENT>::Iterator end= lstNative.end();

				for(; iter != end; ++iter)
				{
					lstManaged->AddLast(NETConvert(*iter));
				}

				return lstManaged;
			}

			template<typename T_MANAGED>
			XAIT_FORCEINLINE XAIT::Common::Container::List<typename WrapperType<T_MANAGED>::Native> NETConvert(System::Collections::Generic::LinkedList<T_MANAGED>^ lstManaged)
			{
				XAIT::Common::Container::List<typename WrapperType<T_MANAGED>::Native> lstNative;

				for each(T_MANAGED item in lstManaged)
				{
					lstNative.pushBack(NETConvert(item));
				}

				return lstNative;
			}


		}
	}
}
