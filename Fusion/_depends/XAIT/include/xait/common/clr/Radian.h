// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/math/Radian.h>
#include <xait/common/clr/Marshalling.h>


namespace XAIT
{
	namespace Common
	{
		namespace CLR
		{
			public value class Radian
			{
			public:
				property float Angle
				{
					inline float get()
					{
						return mAngle;
					}

					inline void set(const float rVal)
					{
						mAngle= rVal;
					}
				}

				Radian(const float angle)
					: mAngle(angle)
				{}

			private:
				float	mAngle;
			};
		}
	}
}

XAIT_NETCONVERT_DEFINE_VALUETYPE_CAST(XAIT::Common::Math::Radian,XAIT::Common::CLR::Radian);
