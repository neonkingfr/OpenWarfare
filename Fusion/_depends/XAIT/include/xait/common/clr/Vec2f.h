// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/math/Vec2.h>
#include <xait/common/clr/Marshalling.h>

// ---------------------------------------------------------------------------------------------------------
//							Vector class for two floats
//
//	Vector class must stay identical to XAIT::Common::Math::Vec2 in means of attributes and virtual methods
//	such that the cast works properly.
//
//	Author:		Markus Wilhelm
//
// ---------------------------------------------------------------------------------------------------------


namespace XAIT
{
	namespace Common
	{
		namespace CLR
		{
			//! \brief template class for vectors with 2 components
			//! \param T_COMP	type of the components (must be a number type)
			public value class Vec2f
			{
			private:
				float	mX;		//!< component in x
				float	mY;		//!< component in y

			public:
				property float X
				{
					float get() { return mX; }
					void set(const float val) { mX= val; }
				}

				property float Y
				{
					float get() { return mY; }
					void set(const float val) { mY= val; }
				}

				//! \brief get a component by number (0-x,1-y)
				//! \remark Not so fast as direct member access
				property float default[unsigned int]
				{
					inline float get(unsigned int i)
					{
						X_ASSERT_MSG_DBG(i < 2,"component index out of range");

						return *(&mX + i);
					}

					inline void set(unsigned int i, float value)
					{
						X_ASSERT_MSG_DBG(i < 2,"component index out of range");

						*(&mX + i) = value;
					}
				}


				//! \brief get the length of the vector
				//! \remark Uses square root function. If only compares are need, use the faster getLength2.
				property float Length
				{
					inline float get()
					{
						return (float)System::Math::Sqrt(mX * mX + mY * mY);
					}
				}

				//! \brief get the squared distance (faster if you need only comparison)
				property float SquaredLength
				{
					inline float get()
					{
						return mX * mX + mY * mY;
					}
				}

				//! \brief Element constructor
				//! \param x	x component
				//! \param y	y component
				Vec2f(const float x, const float y)
					:mX(x),mY(y)
				{}


				//! \brief normalizes a vector
				//! \returns the length of the vector before normalization
				inline float Normalize()
				{
					const float length = Length;

					if (length == 0.0f) return 0.0f;

					mX /= length;
					mY /= length;

					return length;
				}

				//! \brief Test if a vector is normalized
				//! \returns True if the vector is normalized or false otherwise
				//! \remark The test encounters a small epsilon, so the vector could be of length 1.0001 or 0.9999 and still being handled as normalized.
				inline bool IsNormalized()
				{
					// we can use getLength2 for this test, since sqrt(1) =  1
					const float length= SquaredLength;

					return (length > (float)1.0 - (float)X_EPSILON) && (length < (float)1.0 + (float)X_EPSILON);
				}

				//! \brief Get squared distance to another position
				//! \param position		position to which we want the distance
				//! \return The squared distance
				inline float GetSquaredDistance(const Vec2f position)
				{
					return (mX - position.mX) * (mX - position.mX) + (mY - position.mY) * (mY - position.mY);
				}

				//! \brief Get distance to another position
				//! \param position		position to which we want the distance
				//! \return The distance
				//! \remark If only compares between distances are needed, use the getDistance2 method.
				inline float GetDistance(const Vec2f position)
				{
					return (float)System::Math::Sqrt((mX - position.mX) * (mX - position.mX) + (mY - position.mY) * (mY - position.mY));
				}

				//! \brief compute a perpendicular vector to this vector which points in the right halfspace
				//!	of this vector.
				//! \returns a perpendicular vector to this one, pointing in the right halfspace
				property Vec2f Perpendicular
				{
					inline Vec2f get()
					{
						Vec2f perp;

						perp.mX= mY;
						perp.mY= -1.0f * mX;

						return perp;
					}
				}

				//! \brief compute 2x2 determinat with another vector
				//! \param v	vector to build the 2x2 matrix with
				//! \returns The determinant
				inline float GetDeterminant(const Vec2f v)
				{
					return (mX * v.mY - v.mX * mY);
				}

				//! \brief compute dot product
				//! \param v	vector to compute the dot product with
				//! \returns the dot product
				inline float DotProduct(const Vec2f v)
				{
					return (mX * v.mX + mY * v.mY);
				}

				//! \brief rotate a vector in the plane by a given scalar angle
				//! \param r	the angle in radiants
				//! \returns The rotated vector
				//! \remark the original vector is not transformed
				inline Vec2f Rotate(const float r)
				{
					const float cosAngle = (float)System::Math::Cos(r);
					const float sinAngle = (float)System::Math::Sin(r);

					return Vec2f(mX * cosAngle - mY * sinAngle, mX * sinAngle + mY * cosAngle);
				}

				//! \brief Rotate a vector in the plane by a given rotation vector
				//! \param r	rotation vector (rotation vector in a unit circle), must be normalized
				//! \returns The rotated vector
				//! \remark The original vector is not transformed
				inline Vec2f Rotate(Vec2f r)
				{
					X_ASSERT_MSG_DBG(r.IsNormalized(),"rotation vector must be normalized");
					return Vec2f(mX * r.mX - mY * r.mY, mX * r.mY + mY * r.mX);
				}

				//! \brief Test if two vectors are equal
				//! \remark Vectors are equal if components are equal
				inline bool operator== (const Vec2f a)
				{
					return (a.mX == mX && a.mY == mY);
				}

				//! \brief Test if two vectors are unequal
				//! \remark Vectors are unequal if components are unequal
				inline bool operator!= (const Vec2f a)
				{
					return (a.mX != mX || a.mY != mY);
				}

				//! \brief Add this vector to another vector and return the result
				inline static Vec2f operator+(const Vec2f a,const Vec2f b)
				{
					return Vec2f(a.mX + b.mX,a.mY + b.mY);
				}

				//! \brief Substract a vector from this one and return the result
				inline static Vec2f operator-(const Vec2f a,const Vec2f b)
				{
					return Vec2f(a.mX - b.mX,a.mY - b.mY);
				}

				//! \brief Multiply a scalar to this vector and return the result
				inline static Vec2f operator*(const Vec2f a, const float b)
				{
					return Vec2f(a.mX * b, a.mY * b);
				}

				//! \brief Multiply a scalar to this vector and return the result
				inline static Vec2f operator*(const float b, const Vec2f a)
				{
					return Vec2f(a.mX * b, a.mY * b);
				}

				//! \brief Multiply a scalar to this vector and return the result
				inline static Vec2f operator/(const Vec2f a, const float b)
				{
					return Vec2f(a.mX / b, a.mY / b);
				}

				//! \brief Multiply a scalar to this vector and return the result
				inline static Vec2f operator/(const float b, const Vec2f a)
				{
					return Vec2f(a.mX / b, a.mY / b);
				}

				// predefine some common vectors
				static const Vec2f ZERO = Vec2f( 0, 0);
				static const Vec2f UNIT_X = Vec2f( 1, 0);
				static const Vec2f UNIT_Y = Vec2f( 0, 1);
				static const Vec2f NEGATIVE_UNIT_X = Vec2f( -1,  0);
				static const Vec2f NEGATIVE_UNIT_Y = Vec2f(  0, -1);
				static const Vec2f UNIT_SCALE = Vec2f(1, 1);

			};	// class Vec2f
		}	// namespace Math
	}	// namespace CommonCLR
}	// namespace XAIT

XAIT_NETCONVERT_DEFINE_VALUETYPE_CAST(XAIT::Common::Math::Vec2f,XAIT::Common::CLR::Vec2f);
