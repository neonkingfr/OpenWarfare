// (C) xaitment GmbH 2006-2012

#pragma once 
#include <xait/common/callback/AssertCallback.h>
#include <xait/common/clr/StringConvert.h>

namespace XAIT
{
	namespace Common
	{
		namespace CLR
		{
			class ExceptionAssertCallback : public Common::Callback::AssertCallback
			{
			public:
				void showAssert(const char* message , const char *fileName, unsigned lineNumber)
				{
					System::String^ assertMsg= System::String::Format("Assert in: {0}({1}): {2}",gcnew System::String(fileName),lineNumber,gcnew System::String(message));

					throw gcnew System::InvalidOperationException(assertMsg);
				}
			};
		}
	}
}