// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/Platform.h>

// ------------------------ member conversion ---------------------------------

#define DEFINE_MANAGED_NATIVE_CONVERSIONS_FOR_VALUECLASS(T)			\
	inline static operator XAIT::Common::T& (T& obj)					\
			{															\
			return reinterpret_cast<XAIT::Common::T&>(obj);					\
			}															\
			inline static operator const T& ( const XAIT::Common::T& obj)		\
			{															\
			return reinterpret_cast<const T&>(obj);					\
			}															\
			inline static operator const T& ( const XAIT::Common::T* pobj)		\
			{															\
			return reinterpret_cast<const T&>(*pobj);				\
			}

#define DEFINE_MANAGED_NATIVE_CONVERSIONS_FOR_VALUECLASS_TEMPLATE(NameSpace, T)			\
	inline static operator const NameSpace::NETHandles::T##NativeHandle& (const T& obj)					\
			{															\
			return reinterpret_cast<const NameSpace::NETHandles::T##NativeHandle&>(obj);					\
			}															\
			inline static operator NameSpace::NETHandles::T##NativeHandle& (T& obj)					\
			{															\
			return reinterpret_cast<NameSpace::NETHandles::T##NativeHandle&>(obj);					\
			}															\
			inline static operator const T& ( const NameSpace::NETHandles::T##NativeHandle& obj)		\
			{															\
			return reinterpret_cast<const T&>(obj);					\
			}															\
			inline static operator const T& ( const NameSpace::NETHandles::T##NativeHandle* pobj)		\
			{															\
			return reinterpret_cast<const T&>(*pobj);				\
			}

// --------------------------- class conversion ------------------------------------

namespace XAIT
{
	namespace Common
	{
		namespace CLR
		{
			template<typename T>
			class WrapperType
			{
			public:
				typedef T	Native;
				typedef T	Managed;
			};
		}
	}
}


#define XAIT_NETCONVERT_DEFINE_VALUETYPE_CAST(NativeType,ManagedType)\
	namespace XAIT { namespace Common { namespace CLR {\
template<>\
class WrapperType<NativeType>\
{\
public:\
	typedef NativeType	Native;\
	typedef ManagedType	Managed;\
};\
	template<>\
class WrapperType<ManagedType>\
{\
public:\
	typedef NativeType	Native;\
	typedef ManagedType	Managed;\
};\
	XAIT_FORCEINLINE NativeType& NETConvert(ManagedType% vec)\
{\
	return reinterpret_cast<NativeType&>(vec);\
}\
	XAIT_FORCEINLINE ManagedType& NETConvert(NativeType& vec)\
{\
	return reinterpret_cast<ManagedType&>(vec);\
}\
	XAIT_FORCEINLINE const NativeType& NETConvert(const ManagedType% vec)\
{\
	return reinterpret_cast<const NativeType&>(vec);\
}\
	XAIT_FORCEINLINE const ManagedType& NETConvert(const NativeType& vec)\
{\
	return reinterpret_cast<const ManagedType&>(vec);\
}\
}}}

