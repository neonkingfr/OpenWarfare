// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/clr/Marshalling.h>
#include <xait/common/clr/FundamentalTypesConvert.h>

namespace XAIT
{
	namespace Common
	{
		namespace CLR
		{
			template<class T_MANAGED, class T_NATIVE= WrapperType<T_MANAGED>::Native>
			public ref class ArrayCLR : System::Collections::Generic::IList<T_MANAGED>
			{
				T_NATIVE*	mNative;
				uint32		mSize;

			public:

				ref class Enumerator : System::Collections::Generic::IEnumerator<T_MANAGED>
				{
				private:
					uint32			mCurrPos;
					T_NATIVE*		mNative;
					uint32			mSize;

				public:
					virtual property System::Object^ CurrentUntyped
					{
						System::Object^ get () = System::Collections::IEnumerator::Current::get
						{
							return NETConvert(mNative[mCurrPos]);
						}
					}

					virtual property T_MANAGED Current
					{
						T_MANAGED get () = System::Collections::Generic::IEnumerator<T_MANAGED>::Current::get
						{
							return NETConvert(mNative[mCurrPos]);
						}
					}


					virtual bool MoveNext ()
					{
						++mCurrPos;
						return mCurrPos < mSize;
					}

					virtual void Reset ()
					{
						mCurrPos= (uint32)-1;
					}

					Enumerator(T_NATIVE* native, uint32 size)
						: mCurrPos((uint32)-1)
						, mNative(native)
						, mSize(size)
					{}

				};


				ArrayCLR(T_NATIVE* native, uint32 size)
					: mNative(native)
					, mSize(size)
				{
				}


#pragma region IList<T> Members

				virtual int IndexOf(T_MANAGED item)
				{
					throw gcnew System::NotImplementedException("Index of not implemented");
				}

				virtual void Insert(int index, T_MANAGED item)
				{
					throw gcnew System::InvalidOperationException("Cannot do writes in read only container");
				}

				virtual void RemoveAt(int index)
				{
					throw gcnew System::InvalidOperationException("Cannot do writes in read only container");
				}

				virtual property T_MANAGED default[int]
				{
					T_MANAGED get(int index)
					{
						return NETConvert(mNative[index]);
					}
					void set(int index, T_MANAGED val)
					{
						mNative[index] = NETConvert(val);
					}
				}

#pragma endregion

#pragma region ICollection<T> Members

				virtual void Add(T_MANAGED item)
				{
					throw gcnew System::InvalidOperationException("Cannot do writes in read only container");
				}

				virtual void Clear()
				{
					throw gcnew System::InvalidOperationException("Cannot do writes in read only container");
				}

				virtual bool Contains(T_MANAGED item)
				{
					return IndexOf(item) != -1;
				}

				virtual void CopyTo(array<T_MANAGED>^ dst, int arrayIndex)
				{
					throw gcnew System::NotImplementedException("CopyTo");
				}

				virtual property int Count
				{
					int get()
					{ 
						return (int)mSize;
					}
				}

				virtual property bool IsReadOnly
				{
					bool get()
					{ 
						return true;
					}
				}

				virtual bool Remove(T_MANAGED item)
				{
					throw gcnew System::InvalidOperationException("Cannot do writes in read only container");
				}

#pragma endregion


#pragma region IEnumerable<T> Members

				virtual System::Collections::Generic::IEnumerator<T_MANAGED>^ GetEnumerator() = System::Collections::Generic::IEnumerable<T_MANAGED>::GetEnumerator
				{
					return gcnew Enumerator(mNative,mSize);
				}

#pragma endregion

#pragma region IEnumerable Members

				virtual System::Collections::IEnumerator^ GetEnumeratorOld() = System::Collections::IEnumerable::GetEnumerator
				{
					return gcnew Enumerator(mNative,mSize);
				}

#pragma endregion
			};

		}
	}
}