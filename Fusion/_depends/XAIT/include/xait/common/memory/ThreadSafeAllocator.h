// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/memory/Allocator.h>
#include <xait/common/threading/SpinLock.h>

namespace XAIT
{
	namespace Common
	{
		namespace Memory
		{
			class ThreadSafeAllocator : public Allocator
			{
			public:
				typedef ThreadSafeAllocator							ThreadSafe;
				typedef Ptr::Rebind<ThreadSafeAllocator>::Other		Ptr;

				ThreadSafeAllocator(const Allocator::Ptr& alloc)
					: Allocator("ThreadSafe"),mAlloc(alloc)
				{}

				~ThreadSafeAllocator()
				{

				}

				inline bool isClean() const
				{
					return mAlloc->isClean();
				}

				inline bool isThreadSafe() const
				{
					return true;
				}

				inline uint64 release()
				{
					return mAlloc->release();
				}

			protected:
				inline void* allocInternal(const uint32 numBytes)
				{
					mAllocLock.lock();
					void* block= mAlloc->alloc(numBytes);
					mAllocLock.unlock();
					return block;
				}

				inline void freeInternal(void* ptr)
				{
					mAllocLock.lock();
					mAlloc->free(ptr);
					mAllocLock.unlock();
				}

			private:
				Allocator::Ptr			mAlloc;
				Threading::SpinLock		mAllocLock;
			};
		}
	}
}
