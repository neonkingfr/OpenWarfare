// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/memory/Allocator.h>
#include <xait/common/exceptions/ArgumentNullException.h>
#include <xait/common/memory/OutOfMemoryException.h>
#include <xait/common/memory/ThreadSafeAllocWrapper.h>
#include <new>

//////////////////////////////////////////////////////////////////////////
///					Default Allocator
/// 
/// This allocator uses the new and delete functions for memory allocation.
//////////////////////////////////////////////////////////////////////////


namespace XAIT
{
	namespace Common
	{
		namespace Memory
		{
			class DefaultAllocator : public Allocator
			{
			public:
				typedef Ptr::Rebind<DefaultAllocator>::Other		Ptr;
				typedef ThreadSafeAllocWrapper<DefaultAllocator>	ThreadSafe;

				DefaultAllocator()
					: Allocator("Default")
				{
				}

			protected:
				inline void* allocInternal(const uint32 numBytes)
				{
					void* block= ::operator new((size_t)numBytes);

					if (!block)
					{
						XAIT_THROW_EXCEPTION(OutOfMemoryException("No Memory available"));
					}

					return block;
				}

				inline void freeInternal(void* ptr)
				{
					::operator delete(ptr);
				}
			};			
		}
	}
}
