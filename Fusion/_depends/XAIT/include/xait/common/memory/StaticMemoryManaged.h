// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/memory/Allocator.h>
#include <new>

namespace XAIT
{
	namespace Common
	{
		namespace Memory
		{
			typedef const Allocator::Ptr& (*GetAllocatorFuncType)();

			class NonVirtualBaseClass
			{	};

			class VirtualBaseClass
			{
			public:
				virtual ~VirtualBaseClass() {}
			};

			//! This class provides a memory allocator from a static allocator get function for derived classes.
			//! Instantiations with new will put the object in the memory pool specified by the template
			//! parameter. Class internal memory allocations should also use this allocator which can be
			//! get by the getAllocator member function.
			//! The get function must have the signature type GetAllocatorFuncType.
			//! Example:
			//! class TestStaticMemoryManaged : public StaticMemoryManaged<Common::Memory::Library::getStringAlloc> {};
			template<GetAllocatorFuncType F_GETALLOCATOR, typename T_BASE= VirtualBaseClass>
			class StaticMemoryManaged : public T_BASE
			{
			public:
				inline void* operator new(std::size_t numBytes)
				{
					return F_GETALLOCATOR()->alloc((uint32)numBytes);
				}

				inline void* operator new(std::size_t numBytes, void* object)
				{
					return object;
				}

				inline void* operator new[](std::size_t numBytes)
				{
					return F_GETALLOCATOR()->alloc((uint32)numBytes);
				}

				inline void* operator new[](std::size_t numBytes, void* object)
				{
					return object;
				}

				inline void operator delete(void* ptr)
				{
					F_GETALLOCATOR()->free(ptr);
				}

				inline void operator delete(void*, void*)
				{
					// only called in case of new(size_t,void*) fails on init
					// no need to free memory, as no memory has been allocated
				}

				inline void operator delete[](void * ptr)
				{
					F_GETALLOCATOR()->free(ptr);
				}

				inline void operator delete[](void*,void*)
				{
					// only called in case of new[](size_t,void*) fails on init
					// no need to free memory, as no memory has been allocated
				}

				static const Allocator::Ptr& getAllocator()
				{
					return F_GETALLOCATOR();
				}
			};
		}
	}
}
