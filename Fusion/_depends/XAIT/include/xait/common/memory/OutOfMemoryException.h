// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/memory/MemoryException.h>

namespace XAIT
{
	namespace Common
	{
		namespace Memory
		{
			class OutOfMemoryException : public MemoryException
			{
			public:
				OutOfMemoryException(const Common::String& errorMsg)
					: MemoryException(errorMsg)
				{}

				virtual Common::String getExceptionName() const
				{
					return "OutOfMemoryException";
				}
			};
		}
	}
}
