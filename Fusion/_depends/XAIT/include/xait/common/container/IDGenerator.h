// (C) xaitment GmbH 2006-2012

#ifndef _ID_GENERATOR_H_
#define _ID_GENERATOR_H_

#include <xait/common/FundamentalTypes.h>
#include <xait/common/memory/Allocator.h>
#include <xait/common/container/List.h>


namespace XAIT
{
	namespace Common
	{
		namespace Container
		{
			//! \brief generates continous ids
			class IDGenerator
			{
				friend class IDGeneratorStreamer;

			public:
				//! \brief constructor
				IDGenerator(const Memory::Allocator::Ptr& allocator)
					: mMaxID(0)
					, mFreeIDs(allocator)
					, mAllocator(allocator)
				{};

				//! \brief denstructor
				~IDGenerator()
				{};

				//! \brief returns the smallest free id
				//! \returns XAIT::uint32 smallest free id
				uint32 generateID()
				{
					if(!mFreeIDs.empty())
					{
						uint32 id = mFreeIDs.front();
						mFreeIDs.popFront();
						return id;
					}
					//else
					return (mMaxID++);
				};

				//! \brief frees the id
				//! \param uint32 id id to delete
				void deleteID(uint32 id)
				{
					if(id+1 != mMaxID)
					{
						mFreeIDs.pushBack(id);
						return;
					}
					//else
					--mMaxID;
					
					//because List.sort was disabled -> own implementation within this class (only sort for uint32 needed)
					//mFreeIDs.sort();
					//ToDo: enable List.sort()
					bubbleSort(mFreeIDs);

					while(!mFreeIDs.empty() && mMaxID-1 == mFreeIDs.back())
					{
						--mMaxID;
						mFreeIDs.popBack();
					}
				};

				//! \brief resets the IDGenerator
				void reset()
				{
					mMaxID = 0;
					mFreeIDs.clear();
				};

				uint32 getNumElements()
				{
					return (mMaxID - mFreeIDs.size());
				}


			private:
				uint32					mMaxID;         //!< max. used id
				List<uint32>			mFreeIDs;		//!< lists of id's that has been used and are free now
				Memory::Allocator::Ptr	mAllocator;		//!< allocator pointer


				void bubbleSort ( Container::List<uint32>& list )
				{
					uint32 size = list.size();
					int32 last = size - 2;
					bool isChanged = true;

					Container::List<uint32>::Iterator iterLeft = list.begin();
					Container::List<uint32>::Iterator iterRight = list.begin();
					while ( last >= 0 && isChanged )
					{
						isChanged = false;
						iterLeft = list.begin();
						iterRight = list.begin();
						++iterRight;
						for(int32 k = 0; k <= last; k++ )
						{
							if((*iterLeft) > (*iterRight))
							{
								swap((*iterLeft), (*iterRight));
								isChanged = true;
							}
							++iterLeft;
							++iterRight;
						}
						last--;
					}
				}

				void swap ( uint32& x, uint32& y )
				{
					uint32 temp;
					temp = x;
					x = y;
					y = temp;
				}
			};
		}
	}
}
#endif
