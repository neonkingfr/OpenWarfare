// (C) 2011 xaitment GmbH

#pragma once 

#include <xait/common/container/Vector.h>
#include <xait/common/String.h>
using XAIT::Common::String;

namespace XAIT
{
	namespace Common
	{
		namespace Container
		{
			//! \brief Specialized Vector<uint08> that extends some data conversion functions
			class Buffer: public Vector<uint08> 
			{
			public:
				Buffer(const Memory::Allocator::Ptr& allocator)
					: Vector<uint08>(allocator)
				{}

				Buffer(const uint32 size, const Memory::Allocator::Ptr& allocator) 
					: Vector<uint08>(size, allocator)
				{}

				Buffer(const uint32 size, const ValueType& value, const Memory::Allocator::Ptr& allocator)
					: Vector<uint08>(size, value, allocator)
				{}

				~Buffer()
				{
				}

				String toString() 
				{
					String s = "";
					for (Buffer::Iterator it = begin(); it != end(); ++it) {
						if (*it != 0)
							s += (char) *it;
					}
					return s;
				}

				void setStringValue(String& text) 
				{
					resize(text.length());
					for (uint32 i = 0; i < text.length(); i++)
						mVec[i] = text[i];
				}

				char* getCharPtr(uint32 pos=0) const
				{
					return (char*) &(mVec[pos]);
				}
			};
		}
	}
}