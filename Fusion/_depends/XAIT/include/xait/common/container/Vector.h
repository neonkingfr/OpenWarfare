// (C) xaitment GmbH 2006-2012

#pragma once


#include <vector>
#include <algorithm>
#include <xait/common/Platform.h>
#include <xait/common/FundamentalTypes.h>
#include <xait/common/memory/STLAllocator.h>

namespace XAIT
{
	namespace Common
	{
		namespace Container
		{
			template<typename T, typename T_ALLOCATOR= Memory::STLAllocator<T> >
			class Vector
			{
			public:
				typedef ::std::vector<T,T_ALLOCATOR >				ArrayType;
				typedef T											ValueType;

				typedef typename ArrayType::iterator				Iterator;
				typedef typename ArrayType::const_iterator			ConstIterator;

			protected:
				ArrayType	mVec;		//!< array containing the elements of the vector

			public:
				//! \brief default constructor
				//! \remark Works ONLY with StaticSTLAllocator, otherwise the allocator is not initialized
				Vector()
				{}

				//! \brief default constructor
				//! \param allocator	memory allocator for new elements
				Vector(const Memory::Allocator::Ptr& allocator)
					: mVec(allocator)
				{}

				//! \brief construct a vector with size elements 
				//! \param size		number of elements in the vector
				//! \param allocator	memory allocator for new elements
				//! \remark elements will be initialized to their default values
				Vector(const uint32 size, const Memory::Allocator::Ptr& allocator) 
					: mVec((size_t)size,T(),allocator)
				{}

				//! \brief construct a vector with size elements and init elements with a predefined one
				//! \param size		number of elements in the vector
				//! \param value	value to use for element initialization 
				//! \param allocator	memory allocator for new elements
				Vector(const uint32 size, const ValueType& value, const Memory::Allocator::Ptr& allocator)
					: mVec((size_t)size,value,allocator)
				{}

				//! \brief default destructor
				~Vector()
				{}

				//! \brief get the memory allocator for this vector
				inline Memory::Allocator::Ptr getAllocator() const
				{
					return mVec.get_allocator().getMemoryAlloc();
				}

				//! \brief append an element at the end of the vector
				//! \param value	value to add
				inline void pushBack(const ValueType& value)
				{
					mVec.push_back(value);
				}

				//! \brief remove an element at the end of the vector
				inline ValueType popBack()
				{
					ValueType returnValue = back();
					mVec.pop_back();

					return returnValue;
				}

				//! \brief get the size of the vector
				//! \returns the number of elements in the vector
				inline uint32 size() const
				{
					return (uint32)mVec.size();
				}

				//! \brief get the size of the allocated storage
				//! \return number of elements which can be used without any allocation
				inline uint32 capacity() const
				{
					return (uint32)mVec.capacity();
				}

				//! \brief remove an element where the iterator points to
				//! \param iter		iterator to the element we want to delete
				//! \returns an iterator behind the deleted element
				//! \remark This function has runtime O(n)
				inline Iterator erase(const Iterator& iter)
				{
					return mVec.erase(iter);
				}

				//! \brief erase elements between start (inclusive) and end iterator (exclusive) [start,end[
				//! \param start	iterator to the first element to delete
				//! \param end		iterator beyond to the last element to delete
				//! \returns an iterator beyond the last deleted element
				//! \remark This function has runtime O(n)
				inline Iterator erase(const Iterator& start, const Iterator& end)
				{
					return mVec.erase(start,end);
				}

				//! \brief remove an element at the specified position
				//! \param pos		position of the element to delete
				//! \remark This function has runtime O(n)
				inline void erase(const int32 pos)
				{
					Iterator iter= begin();
					iter+= pos;
					mVec.erase(iter);
				}

				//! \brief remove elements from start to end (exclusive)
				//! \param start	id to the first element to delete
				//! \param end		id beyond to the last element to delete
				inline void erase(const int32 start, const int32 end)
				{
					mVec.erase(mVec.begin() + start,mVec.begin() + end);
				}

				//! \brief insert an element before a position specified by an iterator
				//! \param iter		position before which we should insert
				//! \param value	value to insert
				//! \returns an iterator to the position of the new element
				//! \remark This function has runtime O(n)
				inline Iterator insert(const Iterator& iter,const T& value)
				{
					return mVec.insert(iter,value);
				}

				//! \brief insert a number of elements before the specified position
				//! \param iter		position before which we should insert
				//! \param size		number of elements to insert
				//! \param value	value we should use for every new element
				//! \remark This function has runtime O(n)
				inline void insert(const Iterator& iter,const uint32 size, const ValueType& value)
				{
					mVec.insert(iter,size,value);
				}

				//! \brief insert an element at the specified position
				//! \param pos		position where we should insert the new value
				//! \param value	value to insert
				//! \remark The new element will have position pos. This function has runtime O(n)
				inline void insert(const uint32 pos, const T& value)
				{
					Iterator iter= begin();
					iter+= pos;
					mVec.insert(iter,value);
				}

				//! \brief insert a number of elements before the specified position
				//! \param pos		position before which we should insert
				//! \param size		number of elements to insert
				//! \param value	value we should use for every new element
				//! \remark This function has runtime O(n)
				inline void insert(const uint32 pos, const uint32 size, const ValueType& value)
				{
					Iterator iter= begin();
					iter+= pos;
					mVec.insert(iter,size,value);
				}

				//! \brief insert a number of elements [start,end[ from another container
				//! \param iter		position before which we should insert
				//! \param start	start of the element range to copy from
				//! \param end		element beyond the last element to copy from
				//! \remark This function has runtime O(n)
				template<class INPUT_ITERATOR>
				inline void insert(const Iterator& iter,const INPUT_ITERATOR& start, const INPUT_ITERATOR& end)
				{
					mVec.insert(iter,start,end);
				}

				//! \brief resize the vector
				//! \param newSize		the number of elements the vector has after calling
				//! \param value		all added elmenents will be initialized with
				inline void resize(const uint32 newSize, const ValueType& value )
				{
					mVec.resize(newSize,value);
				}

				//! \brief resize the vector
				//! \param newSize		the number of elements the vector has after calling
				inline void resize(const uint32 newSize)
				{
					mVec.resize(newSize);
				}

				//! \brief reserve internal memory for at least size elements
				//! \param size			number of elements to reserve memory
				inline void reserve(const uint32 size)
				{
					mVec.reserve(size);
				}

				//! \brief get the begin iterator of the container
				inline Iterator begin() 
				{	
					return mVec.begin();
				}

				//! \brief get the const begin iterator of the container
				inline ConstIterator begin() const
				{	
					return mVec.begin();
				}

				//! \brief get the end iterator of the container (beyond the last element)
				inline Iterator end()
				{
					return mVec.end();
				}

				//! \brief get the const end iterator of the container (beyond the last element)
				inline ConstIterator end() const
				{
					return mVec.end();
				}

				//! \brief get a reference to the first element
				//! \returns the a reference to the first element
				//! \remark This reference is only valid, if the vector is not empty
				inline ValueType& front()
				{
					return mVec.front();
				}

				//! \brief get a const reference to the first element
				//! \returns the a reference to the first element
				//! \remark This reference is only valid, if the vector is not empty
				inline const ValueType& front() const
				{
					return mVec.front();
				}

				//! \brief find an element
				//! \param value	value to search for
				//! \returns an iterator to the found position or end-iterator if nothing found
				inline ConstIterator find(const ValueType& value) const
				{
					ConstIterator iter = mVec.begin();
					ConstIterator end  = mVec.end();

					while( iter != end)
					{
						if(*iter == value) return iter;

						++iter;
					}
					return end;
				}


				//! \brief find an element
				//! \param value	value to search for
				//! \returns an iterator to the found position or end-iterator if nothing found
				inline Iterator find(const ValueType& value)
				{
					Iterator iter = mVec.begin();
					Iterator end  = mVec.end();

					while( iter != end)
					{
						if(*iter == value) return iter;

						++iter;
					}
					return end;
				}

				int32 findElementPosition(const ValueType& value) const
				{
					for (uint32 i = 0; i < size(); i++)
					{
						if(mVec[i] == value) return i;
					}
					return -1;
				}

				//! \brief test if an element exists
				//! \param value	value to search for
				//! \returns true if the element exists at least one time in the vector, or false otherwise
				bool exists(const ValueType& value) const
				{
					ConstIterator iter = find(value);

					if (iter != mVec.end())
						return true;
					else
						return false;
				}

				//! \brief test if vector is empty
				//! \returns true if empty, false otherwise
				inline bool empty() const
				{
					return mVec.empty();
				}

				//! \brief clear the vector
				//! \remark The vector will be resized to zero (also frees the allocated memory)
				inline void clear()
				{
					mVec.clear();
				}
				//! \brief get the element at the specified index
				//! \param index	index of the element
				//! \returns a reference to the element
				inline ValueType& getElement(const uint32 index)
				{
					X_ASSERT_MSG_DBG(index < (uint32)mVec.size(), "Tried to access a container (size: " << mVec.size() << ") with invalid index (index: " << index << ")");
					return mVec[index];
				}

				//! \brief set the element at the specified index
				//! \param index	index of the element
				//! \param value	the new value
				inline void setElement(const uint32 index, const ValueType& value)
				{
					X_ASSERT_MSG_DBG(index < (uint32)mVec.size(), "Tried to access a container (size: " << mVec.size() << ") with invalid index (index: " << index << ")");
					mVec[index] = value;
				}

				//! \brief get the element at the specified index
				//! \param index	index of the element
				//! \returns a reference to the element
				inline ValueType& operator[](const int32 index) 
				{
					X_ASSERT_MSG_DBG(index >= 0 && index < (int32)mVec.size(), "Tried to access a container (size: " << mVec.size() << ") with invalid index (index: " << index << ")");
					return mVec[index];
				}

				//! \brief get the element at the specified index
				//! \param index	index of the element
				//! \returns a reference to the element
				inline const ValueType& operator[](const int32 index) const
				{
					X_ASSERT_MSG_DBG(index >= 0 && index < (int32)mVec.size(), "Tried to access a container (size: " << mVec.size() << ") with invalid index (index: " << index << ")");
					return mVec[index];
				}

				//! \brief make a copy from another vector of the wrapped array type
				inline void operator=(const ArrayType& x)
				{
					mVec = x;
				}

				inline bool operator==(const Vector<T>& v) const
				{
					return (mVec == v.mVec);
				}

				inline bool operator!=(const Vector<T>& v) const
				{
					return (mVec != v.mVec);
				}

				//! \brief sort the vector ascending with operator<
				inline void sort()
				{
					std::sort(mVec.begin(),mVec.end());
				}

				//! \brief sort the vector ascending with custom compare function
				//! \param compFunc		a compare function with strict weak ordering (e.g. operator<)
				template<typename T_COMPARE>
				void sort(T_COMPARE compFunction)
				{
					std::sort(mVec.begin(),mVec.end(),compFunction);
				}

				//! \brief get the last element in the vector
				//! \returns a const reference to the last element in the vector 
				inline const ValueType& back() const
				{
					return mVec.back(); 
				}

				//! \brief get the last element in the vector
				//! \returns a reference to the last element in the vector
				inline ValueType& back()
				{
					return mVec.back(); 
				}

				//! \brief swap all element from one vector to another
				//! \param from		other vector to swap element with
				inline void swap(Vector& from)
				{
					mVec.swap(from.mVec);
				}

				//! \brief create a copy of this vector as native array
				//! \param numElements	[out] returns number of elements in the new array
				//! \param alloc		allocator which will be used to create the array
				//! \returns a pointer to the new created array or NULL if vector is empty
				T* createArray(uint32& numElements, const Memory::Allocator::Ptr& alloc) const
				{
					numElements= this->size();
					if (!numElements)
						return NULL;

					T* nativeArray= Memory::AllocateArray<T>(alloc,numElements);
					for(uint32 i= 0; i < numElements; ++i)
					{
						new(&nativeArray[i]) T((*this)[i]);
					}

					return nativeArray;
				}

				//! \brief gets the native pointer to the internal array
				inline T* getPtr()
				{
					if (mVec.empty())
						return NULL;
					return &mVec[0];
				}

				inline const T* getPtr() const
				{
					if (mVec.empty())
						return NULL;
					return &mVec[0];
				}

			};

			
			template<>
			class Vector<bool>
			{
			public:
				typedef ::std::vector<bool,Memory::STLAllocator<bool> >	ArrayType;
				
				typedef  ArrayType::iterator				Iterator;
				typedef  ArrayType::const_iterator			ConstIterator;
				typedef  ArrayType::reference				Reference;
				typedef  ArrayType::const_reference			ConstReference;

			private:
				ArrayType	mVec;		//!< array containing the elements of the vector

			public:

				//! \brief default constructor
				//! \param allocator	memory allocator for new elements
				Vector(const Memory::Allocator::Ptr& allocator)
					: mVec(allocator)
				{}

				//! \brief construct a vector with size elements 
				//! \param size		number of elements in the vector
				//! \param allocator	memory allocator for new elements
				//! \remark elements will be initialized to their default values
				Vector(const uint32 size, const Memory::Allocator::Ptr& allocator) 
					: mVec((std::size_t)size,false,allocator)
				{}

				//! \brief construct a vector with size elements and init elements with a predefined one
				//! \param size		number of elements in the vector
				//! \param value	value to use for element initialization 
				//! \param allocator	memory allocator for new elements
				Vector(const uint32 size, bool value, const Memory::Allocator::Ptr& allocator)
					: mVec((std::size_t)size,value,allocator)
				{}

				//! \brief default destructor
				~Vector()
				{}

				//! \brief get the memory allocator for this vector
				inline Memory::Allocator::Ptr getAllocator() const
				{
					return mVec.get_allocator().getMemoryAlloc();
				}

				//! \brief append an element at the end of the vector
				//! \param value	value to add
				void pushBack(bool value)
				{
					mVec.push_back(value);
				}

				//! \brief remove an element at the end of the vector
				bool popBack()
				{
					bool returnValue = back();

					mVec.pop_back();

					return returnValue;
				}

				//! \brief get the size of the vector
				//! \returns the number of elements in the vector
				uint32 size() const
				{
					return (uint32)mVec.size();
				}

				//! \brief remove an element where the iterator points to
				//! \param iter		iterator to the element we want to delete
				//! \returns an iterator behind the deleted element
				//! \remark This function has runtime O(n)
				Iterator erase(const Iterator& iter)
				{
					return mVec.erase(iter);
				}

				//! \brief erase elements between start (inclusive) and end iterator (exclusive) [start,end[
				//! \param start	iterator to the first element to delete
				//! \param end		iterator beyond to the last element to delete
				//! \returns an iterator beyond the last deleted element
				//! \remark This function has runtime O(n)
				Iterator erase(const Iterator& start, const Iterator& end)
				{
					return mVec.erase(start,end);
				}

				//! \brief remove an element at the specified position
				//! \param pos		position of the element to delete
				//! \remark This function has runtime O(n)
				void erase(const int32 pos)
				{
					Iterator iter= begin();
					iter+= pos;
					mVec.erase(iter);
				}

				//! \brief insert an element before a position specified by an iterator
				//! \param iter		position before which we should insert
				//! \param value	value to insert
				//! \returns an iterator to the position of the new element
				//! \remark This function has runtime O(n)
				Iterator insert(const Iterator& iter,bool value)
				{
					return mVec.insert(iter,value);
				}

				//! \brief insert a number of elements before the specified position
				//! \param iter		position before which we should insert
				//! \param size		number of elements to insert
				//! \param value	value we should use for every new element
				//! \remark This function has runtime O(n)
				void insert(const Iterator& iter,const uint32 size, bool value)
				{
					mVec.insert(iter,size,value);
				}

				//! \brief insert an element at the specified position
				//! \param pos		position where we should insert the new value
				//! \param value	value to insert
				//! \remark The new element will have position pos. This function has runtime O(n)
				void insert(const uint32 pos, bool value)
				{
					Iterator iter= begin();
					iter+= pos;
					mVec.insert(iter,value);
				}

				//! \brief insert a number of elements before the specified position
				//! \param pos		position before which we should insert
				//! \param size		number of elements to insert
				//! \param value	value we should use for every new element
				//! \remark This function has runtime O(n)
				void insert(const uint32 pos, const uint32 size, bool value)
				{
					Iterator iter= begin();
					iter+= pos;
					mVec.insert(iter,size,value);
				}

				//! \brief insert a number of elements [start,end[ from another container
				//! \param iter		position before which we should insert
				//! \param start	start of the element range to copy from
				//! \param end		element beyond the last element to copy from
				//! \remark This function has runtime O(n)
				template<class INPUT_ITERATOR>
				void insert(const Iterator& iter,const INPUT_ITERATOR& start, const INPUT_ITERATOR& end)
				{
					mVec.insert(iter,start,end);
				}

				//! \brief resize the vector
				//! \param newSize		the number of elements the vector has after calling
				//! \param value		all added elmenents will be initialized with
				void resize(const uint32 newSize, bool value )
				{
					mVec.resize(newSize,value);
				}

				//! \brief resize the vector
				//! \param newSize		the number of elements the vector has after calling
				void resize(const uint32 newSize)
				{
					mVec.resize(newSize);
				}

				//! \brief reserve internal memory for at least size elements
				//! \param size			number of elements to reserve memory
				void reserve(const uint32 size)
				{
					mVec.reserve(size);
				}

				//! \brief get the begin iterator of the container
				inline Iterator begin() 
				{	
					return mVec.begin();
				}

				//! \brief get the const begin iterator of the container
				inline ConstIterator begin() const
				{	
					return mVec.begin();
				}

				//! \brief get the end iterator of the container (beyond the last element)
				inline Iterator end()
				{
					return mVec.end();
				}

				//! \brief get the const end iterator of the container (beyond the last element)
				inline ConstIterator end() const
				{
					return mVec.end();
				}

				//! \brief get a reference to the first element
				//! \returns the a reference to the first element
				//! \remark This reference is only valid, if the vector is not empty
				inline Reference front()
				{
					return mVec.front();
				}

				//! \brief get a const reference to the first element
				//! \returns the a reference to the first element
				//! \remark This reference is only valid, if the vector is not empty
				inline ConstReference front() const
				{
					return mVec.front();
				}

				//! \brief find an element
				//! \param value	value to search for
				//! \returns an iterator to the found position or end-iterator if nothing found
				inline ConstIterator find(bool value) const
				{
					ConstIterator iter = mVec.begin();
					ConstIterator end  = mVec.end();

					while( iter != end)
					{
						if(*iter == value) return iter;

						++iter;
					}
					return end;
				}


				//! \brief find an element
				//! \param value	value to search for
				//! \returns an iterator to the found position or end-iterator if nothing found
				inline Iterator find(bool value)
				{
					Iterator iter = mVec.begin();
					Iterator end  = mVec.end();

					while( iter != end)
					{
						if(*iter == value) return iter;

						++iter;
					}
					return end;
				}

				int32 findElementPosition(bool value) const
				{
					for (uint32 i = 0; i < size(); i++)
					{
						if(mVec[i] == value) return i;
					}
					return -1;
				}

				//! \brief test if an element exists
				//! \param value	value to search for
				//! \returns true if the element exists at least one time in the vector, or false otherwise
				bool exists(bool value) const
				{
					ConstIterator iter = find(value);

					if (iter != mVec.end())
						return true;
					else
						return false;
				}

				//! \brief test if vector is empty
				//! \returns true if empty, false otherwise
				bool empty() const
				{
					return mVec.empty();
				}

				//! \brief clear the vector
				//! \remark The vector will be resized to zero (also frees the allocated memory)
				void clear()
				{
					mVec.clear();
				}

				//! \brief get the element at the specified index
				//! \param index	index of the element
				//! \returns a reference to the element
				bool getElement(const uint32 index)
				{
					X_ASSERT_MSG_DBG(index < (uint32)mVec.size(), "Tried to access a container (size: " << mVec.size() << ") with invalid index (index: " << index << ")");
					return mVec[index];
				}

				//! \brief set the element at the specified index
				//! \param index	index of the element
				//! \param value	the new value
				void setElement(const uint32 index, bool value)
				{
					X_ASSERT_MSG_DBG(index < (uint32)mVec.size(), "Tried to access a container (size: " << mVec.size() << ") with invalid index (index: " << index << ")");
					mVec[index] = value;
				}

				//! \brief get the element at the specified index
				//! \param index	index of the element
				//! \returns a reference to the element
				inline Reference operator[](const int32 index) 
				{
					X_ASSERT_MSG_DBG(index >= 0 && index < (int32)mVec.size(), "Tried to access a container (size: " << mVec.size() << ") with invalid index (index: " << index << ")");
					return mVec[index];
				}

				//! \brief get the element at the specified index
				//! \param index	index of the element
				//! \returns a reference to the element
				inline ConstReference operator[](const int32 index) const
				{
					X_ASSERT_MSG_DBG(index >= 0 && index < (int32)mVec.size(), "Tried to access a container (size: " << mVec.size() << ") with invalid index (index: " << index << ")");
					return mVec[index];
				}

				//! \brief make a copy from another vector of the wrapped array type
				void operator=(const ArrayType& x)
				{
					mVec = x;
				}

				bool operator==(const Vector<bool>& v) const
				{
					return (mVec == v.mVec);
				}

				bool operator!=(const Vector<bool>& v) const
				{
					return (mVec != v.mVec);
				}

				//! \brief sort the vector ascending with operator<
				void sort()
				{
				//F5MOD: NO TIME FOR FIXING
				#if (XAIT_OS == XAIT_OS_WIN) || (XAIT_OS == XAIT_OS_XBOX360) || (XAIT_OS == XAIT_OS_XBOX360EXT)
					Iterator iter    = mVec.begin();
					Iterator endIter = mVec.end();
					std::sort(iter,endIter);
				#else
					XAIT_PLATFORM_TODO_PS3("std::sort");
					XAIT_PLATFORM_TODO_PS4("std::sort");
					XAIT_PLATFORM_TODO_WII("std::sort");
					XAIT_PLATFORM_TODO_LINUX("std::sort");
				#endif
				}

				//! \brief get the last element in the vector
				//! \returns a const reference to the last element in the vector
				ConstReference back() const
				{
					return mVec.back(); 
				}

				//! \brief get the last element in the vector
				//! \returns a reference to the last element in the vector
				Reference back()
				{
					return mVec.back(); 
				}

				//! \brief swap all element from one vector to another
				//! \param from		other vector to swap element with
				void swap(Vector& from)
				{
					mVec.swap(from.mVec);
				}


			};

		}	// namespace Container
	}	// namespace Common
}	// namespace XAIT
