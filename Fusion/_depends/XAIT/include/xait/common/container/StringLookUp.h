// (C) xaitment GmbH 2006-2012

#pragma once
#include <xait/common/container/LookUp.h>
#include <xait/common/container/HashMap.h>
#include <xait/common/String.h>

namespace XAIT
{
	namespace Common
	{
		namespace Container
		{

			template<typename T_VALUE>
			class StringLookUp
#if XAIT_OS	== XAIT_OS_WII
				: public XAIT::Common::Container::HashMap<XAIT::Common::String,T_VALUE>
			{
			public:
				StringLookUp(const XAIT::Common::Memory::AllocatorPtr& allocator)
					: HashMap(allocator)
				{}

				virtual ~StringLookUp()
				{}
			};
#else
				: public XAIT::Common::Container::LookUp<XAIT::Common::String,T_VALUE>
			{
			public:
				StringLookUp(const XAIT::Common::Memory::AllocatorPtr& allocator)
					: LookUp<String,T_VALUE>(allocator)
				{}

				virtual ~StringLookUp()
				{}
			};
#endif
		}
	}
}
