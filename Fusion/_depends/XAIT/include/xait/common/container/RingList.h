// (C) xaitment GmbH 2006-2012

#pragma once

// -----------------------------------------------------------------------------------------------------------------
//												Ring List
//
// Author: Markus Wilhelm
//
// a double linked list which forms a ring
// the end is the beginning, thus no enditerator exists
// the list can be used as a normal list with iterators
// but if you go from the last element to the next you will be at the beginning
// -----------------------------------------------------------------------------------------------------------------


#include <xait/common/FundamentalTypes.h>
#include <xait/common/debug/Assert.h>
#include <xait/common/memory/Allocator.h>

namespace XAIT
{
	namespace Common
	{
		namespace Container
		{
			template<typename VALUE_TYPE>
			class RingList
			{
				struct ListElement 
				{
					VALUE_TYPE			mVal;	//!< value stored in one listelement
					ListElement*		mNext;	//!< next element in list
					ListElement*		mPrev;	//!< prev element in list

					inline ListElement(const VALUE_TYPE& val)
						: mVal(val)
					{}
				};

				ListElement*			mBeginElem;		//!< first element in the list
				uint32					mListSize;		//!< number of elements in the ringlist
				Memory::Allocator::Ptr	mAllocator;		//!< memory allocator for all object

			public:
				class Iterator
				{
					friend class RingList;

					ListElement*	mElement;	//!< element this iterator points to

					Iterator(ListElement* element)
						: mElement(element)
					{}

				public:

					Iterator()
						: mElement(NULL)
					{}

					inline Iterator& operator++ ()
					{
						mElement= mElement->mNext;
						return *this;
					}

					inline Iterator& operator-- ()
					{
						mElement= mElement->mPrev;
						return *this;
					}

					inline VALUE_TYPE& operator* ()	const
					{
						return mElement->mVal;
					}

					inline VALUE_TYPE* operator->() const
					{
						return &(mElement->mVal);
					}

					inline bool operator== (const Iterator& iter) const
					{
						return (mElement == iter.mElement);
					}

					inline bool operator!= (const Iterator& iter) const
					{
						return (mElement != iter.mElement);
					}

					//! \brief increments the iterator by a certain amount
					//! \remark This operator is in O(inc)
					inline Iterator operator+ (int inc) const
					{
						if (inc < 0) return this->operator-(-1 * inc);

						Iterator iter= *this;
						while(inc) {
							++iter;
							--inc;
						}
						return iter;
					}

					//! \brief decrements the iterator by a certain amount
					//! \remark This operator is in O(dec)
					inline Iterator operator- (int dec) const
					{
						if (dec < 0) return this->operator+(-1 * dec);

						Iterator iter= *this;
						while(dec) {
							--iter;
							--dec;
						}
						return iter;
					}

					inline bool isValid() const 
					{
						if (!mElement) return false;
						return (mElement->mNext && mElement->mPrev);
					}
				};

			public:

				//! \brief default constructor
				RingList(const Memory::Allocator::Ptr& allocator)
					: mBeginElem(NULL),mListSize(0),mAllocator(allocator)
				{}

				//! \brief copy constructor
				RingList(const RingList& ringlist)
					: mBeginElem(NULL),mListSize(0)
				{
					if (ringlist.empty()) return;

					Iterator iter= ringlist.begin();
					do 
					{
						this->add(*iter);
						++iter;
					} while (iter != ringlist.begin());
				}

				//! \brief destructor
				~RingList()
				{
					clear();
				}


				//! \brief insert an element in front of the pos iterator
				//! \param pos	in front of this position the new value will be inserted
				//! \param val	value that will be inserted
				//! \returns an iterator pointing to new element
				Iterator insert(const Iterator& pos, const VALUE_TYPE& val)
				{
					assert(pos.isValid());

					ListElement* elem= Memory::New(ListElement(),mAllocator);
					elem->mVal= val;

					attachInFront(elem,pos.mElement);

					Iterator iter;
					iter.mElement= elem;
					return iter;
				}

				//! \brief insert all elements in front of pos inclusive begin and end iterator [begin,end] and copy data
				//! \param pos		in front of this iterator all elements will be inserted
				//! \param begin	start of insert
				//! \param end		end of insert
				//! \remark This method copies the data from begin,end to this list. The insert will be done by incrementing begin
				//!			iterator.
				void insert(const Iterator& pos, const Iterator& begin, const Iterator& end)
				{
					Iterator iter= begin;
					for(; iter != end; ++iter) {
						insert(pos,*iter);
					}
					insert(pos,*iter);
				}


				//! \brief adds a new element in front of begin iterator (or at the end of the list)
				//! \param val	value to add
				void add(const VALUE_TYPE& val)
				{
					ListElement* elem= XAIT_NEW(mAllocator,ListElement)(val);

					if (!mBeginElem) 
					{
						mBeginElem= elem;
						mBeginElem->mNext= mBeginElem;
						mBeginElem->mPrev= mBeginElem;
						++mListSize;
					} 
					else
					{
						attachInFront(elem,mBeginElem);
					}
				}

				//! \brief clear the complete list
				void clear()
				{
					ListElement* tmp;

					while (mBeginElem)
					{
						// disconnect element
						if (mBeginElem->mPrev) mBeginElem->mPrev->mNext= NULL;
						if (mBeginElem->mNext) mBeginElem->mNext->mPrev= NULL;

						tmp= mBeginElem;
						mBeginElem= mBeginElem->mNext;
						XAIT_DELETE(tmp,mAllocator);
					}

					mListSize= 0;
				}


				//! \brief erases element end returns an iterator in front of the delete element
				//! \param pos	iterator to element 
				//! \returns an iterator to the element leading the deleted one
				//! \remark If there is only one element in the list, and this gets delete the returned iterator is invalid.
				Iterator erase(const Iterator& pos)
				{
					Iterator ret;

					// disconnect element
					pos.mElement->mNext->mPrev= pos.mElement->mPrev;
					pos.mElement->mPrev->mNext= pos.mElement->mNext;

					if (pos.mElement->mPrev == pos.mElement) 
					{
						ret.mElement= NULL;
					}
					else
					{
						ret.mElement= pos.mElement->mPrev;
					}

					if (pos.mElement == mBeginElem)
					{
						mBeginElem= ret.mElement;
					}

					XAIT_DELETE(pos.mElement,mAllocator);
					--mListSize;

					return ret;
				}

				//! \brief check if ringlist is empty
				//! \returns true if list is empty
				inline bool empty() const
				{
					return (!mBeginElem);
				}

				//! \brief get size of ringlist
				//! \returns the number of elements in the list
				//! \remark Runtime O(1)
				inline uint32 size() const
				{
					return mListSize;
				}

				//! \brief get a begin iterator
				//! \returns an iterator of the beginning
				//! \remark This iterator is relative, since the "start" position can change due to list modifications
				inline Iterator begin() const
				{
					return Iterator(mBeginElem);
				}

				//! \brief copy all elements from another list to this list
				inline RingList& operator=(const RingList& ringlist) 
				{
					clear();

					// add all elements from other ringlist
					Iterator begin_iter= ringlist.begin();
					Iterator iter= begin_iter;
					do {
						add(*iter);
						++iter;
					} while (iter != begin_iter);

					return *this;
				}

			private:

				//! \brief attach new element in front of another element
				//! \param front	element which should be added in front of pos
				//! \param pos		element position where to add
				inline void attachInFront(ListElement* front, ListElement* pos)
				{
					front->mPrev= pos->mPrev;
					front->mNext= pos;
					pos->mPrev->mNext= front;
					pos->mPrev= front;
					++mListSize;
				}

			};

		}
	}

}



