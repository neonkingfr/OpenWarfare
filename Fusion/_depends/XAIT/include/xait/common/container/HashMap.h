// (C) xaitment GmbH 2006-2012

#pragma once


#include <xait/common/container/LookUp.h>

#if XAIT_OS == XAIT_OS_WIN || XAIT_OS == XAIT_OS_XBOX360 || XAIT_OS == XAIT_OS_XBOX360EXT

#include <hash_map>
#define XAIT_HASH_NAMESPACE			stdext
#define XAIT_HASH_BASECLASS(type)	XAIT_HASH_NAMESPACE::hash_compare<type,typename T_KEY_COMPARE>
#define XAIT_HASH_DEFAULT_COMPARE	std::less<T_KEY>

#elif XAIT_OS == XAIT_OS_WII

#include <hash_map>
#define XAIT_HASH_NAMESPACE			Metrowerks
#define XAIT_HASH_BASECLASS(type)	Metrowerks::hash<type>
#define XAIT_HASH_DEFAULT_COMPARE	std::equal_to<T_KEY> 

#elif XAIT_OS == XAIT_OS_PS3 || XAIT_OS == XAIT_OS_PS4

#include <hash_map>
#define XAIT_HASH_NAMESPACE			std
#define XAIT_HASH_BASECLASS(type)	std::hash_compare<type,T_KEY_COMPARE>
#define XAIT_HASH_DEFAULT_COMPARE	std::less<T_KEY>

#elif XAIT_OS == XAIT_OS_LINUX || XAIT_OS == XAIT_OS_MAC
#include <ext/hash_map>
#define XAIT_HASH_NAMESPACE			__gnu_cxx
#define XAIT_HASH_BASECLASS(type)	__gnu_cxx::hash<type>
#define XAIT_HASH_DEFAULT_COMPARE	std::equal_to<T_KEY>

#else
#error "HashMap not implemented for this plattform"
#endif


namespace XAIT
{
	namespace Common
	{
		namespace Container
		{

			template<typename T_KEY,typename T_VALUE,typename  T_HASH_COMPARE>
			class HashMap;

			namespace Helper
			{

				template<int N_COND>
				class ConditionalHashMapCompile
				{
				public:
					template<typename T_KEY,typename T_VALUE,typename  T_HASH_COMPARE>
					static inline T_VALUE& getValue(HashMap<T_KEY,T_VALUE,T_HASH_COMPARE>& inst, const T_KEY& key)
					{
						return inst._elementOperatorFixedAlloc(key);
					}
				};

				template<>
				class ConditionalHashMapCompile<0>
				{
				public:
					template<typename T_KEY,typename T_VALUE,typename  T_HASH_COMPARE>
					static inline T_VALUE& getValue(HashMap<T_KEY,T_VALUE,T_HASH_COMPARE>& inst, const T_KEY& key)
					{
						return inst._elementOperatorNormal(key);
					}
				};
			}


			//! \brief this class encapsulate the hash function and alle needed parameters
			//! \remark the MS implementation except an operator< as comparision but the STL implemention an operator==
			template<typename T_KEY,typename T_KEY_COMPARE>
			class HashCompare 
				: public XAIT_HASH_BASECLASS(T_KEY)
			{
			public:
				// this variables defines the parameter of the hashmap
				// all variables has to be defined in every hash compare
				// contains all parameters of ms and stl implementation
				enum
				{
					bucket_size =		4,		// MS implementation, name cannot be changed, constant size of buckets
					min_buckets =		8,		// MS implementation, name cannot be changed, starting number of buckets
					num_buckets =		0,		// standard STL implementation, starting number of buckets
					load_factor_limit = 2,		// standard STL implementation,	threshold to increase number of buckets, loadfactor == number of elements in each bucket
					growth_factor =		4		// standard STL implementation, resize factor (if load factor limit = 2 -> load factor 0.5 after resize)
				};

				typedef T_KEY_COMPARE	Compare;
			};

			template <typename T_KEY_COMPARE>
			class HashCompare<String,T_KEY_COMPARE>
				: public XAIT_HASH_BASECLASS(String)
			{
			public:
				enum
				{
					bucket_size =		4,		// MS implementation, name cannot be changed, constant size of buckets
					min_buckets =		8,		// MS implementation, name cannot be changed, starting number of buckets
					num_buckets =		0,		// standard STL implementation, starting number of buckets
					load_factor_limit = 2,		// standard STL implementation,	treshold to increase number of buckets, loadfactor == number of elements in each bucket
					growth_factor =		4		// standard STL implementation, resize factor (if load factor limit = 2 -> load factor 0.5 after resize)
				};

				typedef T_KEY_COMPARE	Compare;

				std::size_t operator()(const Common::String& key) const
				{
					size_t hash = 0;
					uint32 length = key.getLength();
					for(uint32 pos = 0; pos < length; ++pos)
					{
						hash = 31 * hash + (key[pos]);
					}
					return hash;
				} 

				// ms specific implementation
				bool operator()(const String& _Keyval1, const String& _Keyval2) const
				{	// test if _Keyval1 ordered before _Keyval2
					return (_Keyval1 < _Keyval2);
				}
			};


			template<typename T_KEY,typename T_VALUE,typename  T_HASH_COMPARE = HashCompare<T_KEY, XAIT_HASH_DEFAULT_COMPARE > >
			class HashMap
			{
			public:
#if XAIT_OS == XAIT_OS_WIN || XAIT_OS == XAIT_OS_XBOX360 || XAIT_OS == XAIT_OS_XBOX360EXT || XAIT_OS == XAIT_OS_PS3 || XAIT_OS == XAIT_OS_PS4
				typedef ::XAIT_HASH_NAMESPACE::hash_map<T_KEY,T_VALUE,T_HASH_COMPARE,Memory::STLAllocator<std::pair<T_KEY,T_VALUE> > >	MapType;
#else
				typedef ::XAIT_HASH_NAMESPACE::hash_map<T_KEY,T_VALUE,T_HASH_COMPARE,typename T_HASH_COMPARE::Compare,Memory::STLAllocator<std::pair<const T_KEY,T_VALUE> > >	MapType;
#endif
				typedef typename MapType::iterator																								Iterator;
				typedef typename MapType::const_iterator																						ConstIterator;

			private:
				MapType		mTable;


			public:
				//! \brief constructor for lookup
				//! \param allocator	allocator used for new elements in the lookup table


#if XAIT_OS == XAIT_OS_WIN || XAIT_OS == XAIT_OS_XBOX360 || XAIT_OS == XAIT_OS_XBOX360EXT || XAIT_OS == XAIT_OS_PS3 || XAIT_OS == XAIT_OS_PS4

				HashMap(const Memory::Allocator::Ptr& allocator)
					: mTable(T_HASH_COMPARE(), Memory::STLAllocator<std::pair<const T_KEY,T_VALUE> >(allocator))
				{}

#elif XAIT_OS == XAIT_OS_WII

				HashMap(const Memory::Allocator::Ptr& allocator)
					: mTable(T_HASH_COMPARE::num_buckets,
					T_HASH_COMPARE(),
					typename T_HASH_COMPARE::Compare(),
					T_HASH_COMPARE::load_factor_limit,
					T_HASH_COMPARE::growth_factor, 
					Memory::STLAllocator<std::pair<const T_KEY,T_VALUE> >(allocator))
				{} 

#else

				HashMap(const Memory::Allocator::Ptr& allocator)
					: mTable(T_HASH_COMPARE::num_buckets,
					T_HASH_COMPARE(),
					typename T_HASH_COMPARE::Compare(),
					Memory::STLAllocator<std::pair<const T_KEY,T_VALUE> >(allocator))
				{} 

#endif

				//! \brief get the memory allocator for this vector
				inline Memory::Allocator::Ptr getAllocator() const
				{
					return mTable.get_allocator().getMemoryAlloc();
				}

				//! \brief find an element to a key
				//! \param key	key to search for
				//! \returns Iterator pointing to the element with the search key, or end iterator if nothing found
				inline Iterator find(const T_KEY& key)
				{
					return mTable.find(key);
				}

				//! \brief find an element to a key
				//! \param key	key to search for
				//! \returns Iterator pointing to the element with the search key, or end iterator if nothing found
				inline ConstIterator find(const T_KEY& key) const
				{
					return mTable.find(key);
				}

				//! \brief test if lookup is empty
				//! \returns true if empty, false if not.
				inline bool empty() const 
				{
					return mTable.empty();
				}

				//! \brief get a value for a key
				//! \param key			key to search for
				//! \param value[out]	value belonging to the key 
				//! \returns true if the key has been found or false if not.
				bool get(const T_KEY& key, T_VALUE& value) const
				{
					ConstIterator i = mTable.find(key);
					if(i != mTable.end())
					{
						value = i->second;
						return true;
					}
					return false;
				}

				//! \brief tests if a key exists
				//! \param key	key to test for
				//! \returns true if the key exists
				bool exists(const T_KEY& key) const
				{
					ConstIterator i = mTable.find(key);
					if(i == mTable.end())
						return false;
					return true;
				}

				//! \brief get the begin iterator
				inline Iterator begin()
				{
					return mTable.begin();
				}

				//! \brief get the end iterator
				inline Iterator end()
				{
					return mTable.end();
				}

				//! \brief get the begin iterator
				inline ConstIterator begin() const
				{
					return mTable.begin();
				}

				//! \brief get the end iterator
				inline ConstIterator end() const
				{
					return mTable.end();
				}

				//! \brief get the number of elements in the lookup
				//! \returns the number of elements in the lookup
				inline uint32 size() const
				{
					return (uint32)mTable.size();
				}

				//! \brief add an element for a specific key
				//! \param key		key where to add the element
				//! \param value	value to insert at the key position
				//! \remark if there is already a value with the specified key, this value will NOT be
				//!			overwritten.
				inline Iterator add(const T_KEY& key, const T_VALUE& value)
				{

					return mTable.insert(std::make_pair(key,value)).first;
				}

				//! \brief remove an element with a specific key
				//! \param key	key to the element which should be removed
				//! \remark Does not produce an error if the element specified by key does not exist.
				inline void remove(const T_KEY& key)
				{
					mTable.erase(key);
				}


				//! \brief remove an element at the position of an iterator
				//! \param where	position where we should remove an element
				//! \returns iterator to the next element in the iterator listing
				inline void remove(Iterator where)
				{
					mTable.erase(where);
				}

				//! \brief remove all elements from the lookup
				inline void clear()
				{
					mTable.clear();
				}

				//! \brief swap the content of two lookups
				inline void swap(HashMap& right)
				{
					mTable.swap(right.mTable);
				}


				//! \brief access an element at the position of a key
				//! \remark This function cannot be const, since it could introduce a new element
				//!			which depends on the runtime state of the LookUp.
				inline T_VALUE& operator[](const T_KEY& key)
				{
					return Container::Helper::ConditionalHashMapCompile<Container::Helper::ConstructorEval<T_VALUE>::NEED_ALLOCATOR_INIT>::getValue(*this,key);
				}


			private:
				template<int N_COND>
				friend class Container::Helper::ConditionalHashMapCompile;

				inline T_VALUE& _elementOperatorNormal(const T_KEY& key)
				{
					return mTable[key];
				}

				inline T_VALUE& _elementOperatorFixedAlloc(const T_KEY& key)
				{
					Iterator iter= mTable.find(key);
					if (iter == mTable.end())
					{
						// insert a new element and use the memory allocator of the embedded map type
						iter= mTable.insert(iter,std::make_pair<T_KEY,T_VALUE>(key,T_VALUE(this->getAllocator())));
					}
					return iter->second;
				}
			};
		}


	}
}
