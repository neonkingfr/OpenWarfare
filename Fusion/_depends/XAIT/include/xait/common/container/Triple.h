// (C) xaitment GmbH 2006-2012

#pragma once

namespace XAIT
{
	namespace Common
	{
		namespace Container
		{

			template<typename T_FIRST, typename T_SECOND, typename T_THIRD>
			struct Triple
			{	
				T_FIRST		mFirst;		//!< first value
				T_SECOND	mSecond;	//!< second value
				T_THIRD		mThird;		//!< third value

				//! Constructor
				Triple();

				//! Constructor
				//! \param Val1		first value
				//! \param Val2		second value
				//! \param Val3		third value
				Triple(const T_FIRST& val1, const T_SECOND& val2, const T_THIRD& val3) : mFirst(val1), mSecond(val2), mThird(val3)
				{}

				//! Constructor
				//! \param otherPair   another pair
				Triple(const Triple<T_FIRST,T_SECOND,T_THIRD>& otherTriple) : mFirst(otherTriple.mFirst), mSecond(otherTriple.mSecond), mThird(otherTriple.mThird)
				{}

			};


			template<typename T_FIRST, typename T_SECOND, typename T_THIRD>
			inline Triple<T_FIRST, T_SECOND, T_THIRD> MakeTriple(const T_FIRST& a, const T_SECOND& b, const T_THIRD& c)
			{
				return Triple<T_FIRST, T_SECOND, T_THIRD>(a,b,c);
			}
		}
	}
}
