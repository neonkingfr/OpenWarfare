// (C) xaitment GmbH 2006-2012

#pragma once

#include <string.h>

#include <xait/common/FundamentalTypes.h>
#include <xait/common/math/Vec2.h>
#include <xait/common/debug/Debug.h>
#include <xait/common/memory/Allocator.h>

namespace XAIT
{
	namespace Common
	{
		namespace Container
		{
			//! \brief	CartesianGrid stores rectangular 2D data for fast access.
			//!			It uses a affine transformation to store the data at discrete points
			//!			Usually you have no need to access the transformation directly.
			//! \param	T_COMP	type of the data
			template<typename T_USERDATA>
			class CartesianGrid
			{
			public:

				//! \brief default constructor
				//! \param	allocator	memory allocator
				CartesianGrid(const Memory::Allocator::Ptr& allocator)
					: mGrid(NULL)
					, mFactorX(.0f)
					, mFactorY(.0f)
					, mSamplesX(0)
					, mSamplesY(0)
					, mMin(XAIT::Common::Math::Vec2f(.0f,.0f))
					, mMax(XAIT::Common::Math::Vec2f(.0f,.0f))
					, mAllocator(allocator)
				{};

				//! \brief				constructor
				//! \param	allocator	memory allocator
				//! \param	min			minimum of the 2D bounding rectangle (domain of the transformation)
				//! \param	max			maximum of the 2D bounding rectangle (domain of the transformation)
				//! \param	samplesX	number of samples used in x direction (co-domain of the transformation)
				//! \param	samplesY	number of samples used in y direction (co-domain of the transformation)
				CartesianGrid(const Memory::Allocator::Ptr& allocator,const XAIT::Common::Math::Vec2f& min, const XAIT::Common::Math::Vec2f& max,const XAIT::uint32 samplesX,const XAIT::uint32 samplesY)
					: mGrid(NULL)
					, mAllocator(allocator)
				{
					init(min,max,samplesX,samplesY);
				};

				//! \brief destructor
				~CartesianGrid()
				{
					reset();
				};

				//! \brief				initializes the CartesianGrid (only needed if default constructor was used)
				//! \param	min			minimum of the 2D bounding rectangle (domain of the transformation)
				//! \param	max			maximum of the 2D bounding rectangle (domain of the transformation)
				//! \param	samplesX	number of samples used in x direction (co-domain of the transformation)
				//! \param	samplesY	number of samples used in y direction (co-domain of the transformation)
				//! \remarks			it's save to call init on an already initialized instance.
				//!						But be aware that all data stored before init is called will be deleted
				void init(const XAIT::Common::Math::Vec2f& min, const XAIT::Common::Math::Vec2f& max,const XAIT::uint32 samplesX,const XAIT::uint32 samplesY)
				{
					reset();

					mMin = min;
					mMax = max;
					mSamplesX = samplesX;
					mSamplesY = samplesY;

					mFactorX = (samplesX-1)/(max.mX-min.mX);
					mFactorY = (samplesY-1)/(max.mY-min.mY);


					mGrid = (T_USERDATA*)mAllocator->alloc(sizeof(T_USERDATA) * samplesX*samplesY);

				};

				//! \brief			stores data at point (x/y)
				//! \param	x		x coordinate to store data at
				//! \param	y		y coordinate to store data at
				//! \param	data	data to be stored
				//! \returns		true if the point (x/y) is inside the domain (bounding rectangle); false otherwise
				bool setData(const XAIT::float32 x, const XAIT::float32 y, const T_USERDATA& data)
				{
					if(x<mMin.mX || y < mMin.mY || x > mMax.mX || y > mMax.mY)
						return false;

					XAIT::uint32 u = (XAIT::uint32)( (x-mMin.mX)*mFactorX);
					XAIT::uint32 v = (XAIT::uint32)( (y-mMin.mY)*mFactorY);

					mGrid[ v*mSamplesX + u ] = data;

					return true;
				};

				//! \brief			retrieves data at point (x/y)
				//! \param	x		x coordinate to get data from
				//! \param	y		y coordinate to get data from
				//! \param	data	data to be retrieved (output parameter)
				//! \returns		true if the point (x/y) is inside the domain (bounding rectangle); false otherwise
				//! \remarks		if no data is stored at (x/y) the value of the data parameter is undefined
				bool getData(const XAIT::float32 x, const XAIT::float32 y, T_USERDATA& data) const
				{

					if(x<mMin.mX || y < mMin.mY || x > mMax.mX || y > mMax.mY)
						return false;

					XAIT::uint32 u = (XAIT::uint32)( (x-mMin.mX)*mFactorX);
					XAIT::uint32 v = (XAIT::uint32)( (y-mMin.mY)*mFactorY);

					data = mGrid[ v*mSamplesX + u ];
					return true;
				};

				//! \brief			retrieves data at point (x/y) (world cooradinates)
				//! \param	x		x coordinate to get data from
				//! \param	y		y coordinate to get data from
				//! \returns		data at point (x/y). If no data is stored at (x/y) the return value is undefined
				//! \remarks		throws an assertion if the point (x/y) is outside the domain (bounding rectangle)
				T_USERDATA getData(const XAIT::float32 x, const XAIT::float32 y) const
				{
					X_ASSERT_MSG(x>=mMin.mX && y>=mMin.mY, " point (x/y) outside of grid");
					X_ASSERT_MSG(x<=mMax.mX && y<=mMax.mY, " point (x/y) outside of grid");


					XAIT::uint32 u = (XAIT::uint32)( (x-mMin.mX)*mFactorX);
					XAIT::uint32 v = (XAIT::uint32)( (y-mMin.mY)*mFactorY);

					return (mGrid[ v*mSamplesX + u ]);
				}

				T_USERDATA getLocalData(const XAIT::uint32 u, const XAIT::uint32 v) const
				{
					X_ASSERT_MSG(u>= 0 && v>=0 && u<mSamplesX && v<mSamplesY, " point (x/y) outside of grid");

					return (mGrid[ v*mSamplesX + u ]);
				}

				bool getLocalData(const XAIT::uint32 u, const XAIT::uint32 v, T_USERDATA& data) const
				{
					if( u>=mSamplesX || v>= mSamplesY)
						return false;

					data = mGrid[ v*mSamplesX + u ];
					return true;
				};

				T_USERDATA getLocalData(const XAIT::Common::Math::Vec2i32& localPoint) const
				{
					return getLocalData(localPoint.mX,localPoint.mY);
				}

				const XAIT::Common::Math::Vec2i32 worldToLocal(const XAIT::float32 x , const XAIT::float32 y) const
				{
					return (XAIT::Common::Math::Vec2i32( (XAIT::int32)((x-mMin.mX)*mFactorX ), (XAIT::int32)(( y-mMin.mY)*mFactorY )) );
				}

				const XAIT::Common::Math::Vec2i32 worldToLocal(const XAIT::Common::Math::Vec2f& worldPoint) const
				{
					return worldToLocal(worldPoint.mX,worldPoint.mY);
				}

				const XAIT::Common::Math::Vec2f localToWorld(const XAIT::Common::Math::Vec2i32& localPoint) const
				{
					return (XAIT::Common::Math::Vec2f((XAIT::float32)localPoint.mX/mFactorX + mMin.mX,(XAIT::float32)localPoint.mY/mFactorY + mMin.mY));
				}

				XAIT::uint32 getNumXSamples() const
				{
					return mSamplesX;
				}

				XAIT::uint32 getNumYSamples() const
				{
					return mSamplesY;
				}


			private:
				T_USERDATA* mGrid;							//!<	array containing the user data
				XAIT::float32 mFactorX;						//!<	scale factor in x-direction used by the transformation
				XAIT::float32 mFactorY;						//!<	scale factor in y-direction used by the transformation
				XAIT::uint32 mSamplesX;						//!<	number of samples (size of the co-domain) in x-direction
				XAIT::uint32 mSamplesY;						//!<	number of samples (size of the co-domain) in y-direction
				XAIT::Common::Math::Vec2f mMin;				//!<	minimum of the domain (bounding rectangle). Needed by the transformation
				XAIT::Common::Math::Vec2f mMax;				//!<	maximum of the domain (bounding rectangle)
				Memory::Allocator::Ptr	mAllocator;


				//! \brief		deletes the stored data
				//!	\remarks	this is just a small helper for the lazy programmer (more precisely: me ;)
				inline void reset()
				{
					if (mGrid)
						mAllocator->free(mGrid);
					mGrid = NULL;
				}

			};

		} // namespace Container
	} //namespace Common
} //namespace XAIT