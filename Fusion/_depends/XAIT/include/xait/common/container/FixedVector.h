// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/container/Array.h>

namespace XAIT
{
	namespace Common
	{
		namespace Container
		{
			//! \brief a vector style class for vectors with fixed size
			template<typename T_OBJECT, int N_NUM_ELEM>
			class FixedVector
			{
			public:
				//! \brief default constructor
				FixedVector()
					: mArray(),
					mSize(0)
						
				{}

				FixedVector(uint32 size)
					: mArray(),
					mSize(size)
				{

				}

				//! \brief copy constructor, copy the complete array
				FixedVector(const FixedVector& copyFrom)
					: mArray(copyFrom.mArray),
					mSize(copyFrom.mSize)
				{}

				FixedVector(const Common::Container::Vector<T_OBJECT>& copyFrom)
					: mArray(),
					mSize(copyFrom.size())
				{
					X_ASSERT_MSG_DBG(copyFrom.size() <= N_NUM_ELEM,"index out of range");

					for (uint32 i=0; i < copyFrom.size(); ++i)
					{
						mArray[i] = copyFrom[i];
					}

				}


				//! \brief element access
				inline T_OBJECT& operator[](const uint32 index)
				{
					X_ASSERT_MSG_DBG(index < mSize,"index out of range");
					return mArray[index];
				}

				//! \brief element access
				inline const T_OBJECT& operator[](const uint32 index) const
				{
					X_ASSERT_MSG_DBG(index < mSize,"index out of range");
					return mArray[index];
				}

				inline void pushBack(const T_OBJECT& value)
				{
					X_ASSERT_MSG_DBG(mSize < N_NUM_ELEM,"index out of range");
					mArray[mSize] = value;
					++mSize;
				}

				inline void clear()
				{
					mSize = 0;
				}

				inline uint32 size() const
				{
					return mSize;
				}


			private:
				Array<T_OBJECT, N_NUM_ELEM> mArray;
				uint32 mSize;

			};
		}
	}
}