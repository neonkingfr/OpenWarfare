// (C) xaitment GmbH 2006-2012

#pragma once

#include <list>
#include <xait/common/FundamentalTypes.h>
#include <xait/common/memory/STLAllocator.h>


namespace XAIT
{
	namespace Common
	{
		namespace Container
		{
			template<typename T>
			class List
			{
			public:
				typedef std::list<T,Memory::STLAllocator<T> >		ListType;
				typedef typename ListType::iterator					Iterator;
				typedef typename ListType::const_iterator			ConstIterator;
				typedef typename ListType::reverse_iterator			ReverseIterator;
				typedef typename ListType::const_reverse_iterator	ConstReverseIterator;

			private:
				ListType	mElements;

			public:
				List(const Memory::Allocator::Ptr& alloc)
					: mElements((Memory::STLAllocator<T>(alloc)))
				{}

				List(const List<T>& lst)
					: mElements(lst.mElements)
				{}

				~List()
				{

				}

				//! \brief get the memory allocator for this vector
				inline Memory::Allocator::Ptr getAllocator() const
				{
					return mElements.get_allocator().getMemoryAlloc();
				}

				//! \brief Add an element at the end of the list
				//! \param value	value to add
				inline void pushBack(const T& value)
				{
					mElements.push_back(value);
				}

				//! \brief Add an element at the front of the list
				//! \param value	value to add
				inline void pushFront(const T& value)
				{
					mElements.push_front(value);
				}

				//! \brief get the first element in the list
				//! \returns reference to the first element
				//! \remark If the list is empty, this will thrown an assertion
				inline T& front()
				{
					return mElements.front();
				}

				//! \brief get the first element in the list
				//! \returns reference to the first element
				//! \remark If the list is empty, this will thrown an assertion
				inline const T& front() const
				{
					return mElements.front();
				}

				//! \brief get the last element in the list
				//! \returns the last element of the list
				//! \remark If the list is empty, this will thrown an assertion
				inline T& back()
				{
					return mElements.back();
				}

				//! \brief get the last element in the list
				//! \returns the last element of the list
				//! \remark If the list is empty, this will thrown an assertion
				inline const T& back() const
				{
					return mElements.back();
				}

				//! \brief remove the first element in the list
				inline void popFront()
				{
					mElements.pop_front();
				}

				//! \brief remove the last element in the list
				inline void popBack()
				{
					mElements.pop_back();
				}

				//! \brief delete the element where the iterator points to
				//! \param iter		element which should be deleted
				//! \returns an element that points to the element after the deleted one.
				inline Iterator erase(const Iterator& iter)
				{
					return mElements.erase(iter);
				}

				//! \brief find the first element that matches the value
				//! \param value	value to match
				//! \returns an iterator to the element, or the enditerator if nothing found
				inline Iterator find(const T& value)
				{
					Iterator iter= mElements.begin();
					Iterator end= mElements.end();

					for(; iter != end; ++iter)
					{
						if (value == *iter) return iter;
					}
					return end;
				}

				//! \brief find the first element that matches the value
				//! \param value	value to match
				//! \returns an iterator to the element, or the enditerator if nothing found
				inline ConstIterator find(const T& value) const
				{
					ConstIterator iter= mElements.begin();
					ConstIterator end= mElements.end();

					for(; iter != end; ++iter)
					{
						if (value == *iter) return iter;
					}
					return end;
				}

				//! \brief tests if a value exists
				//! \param value	value to test for
				//! \returns true if the value exists
				bool exists(const T& value) const
				{
					if(find(value) == mElements.end())
						return false;
					return true;
				}

				//! \brief Get the iterator from the first element in the list
				//! \remark This iterator equals the end iterator, if the list is empty.
				inline Iterator begin()
				{
					return mElements.begin();
				}

				//! \brief Get the iterator after the last element in the list
				//! \remark This iterator is always invalid, and cannot be dereferenced.
				inline Iterator end()
				{
					return mElements.end();
				}

				//! \brief Get the iterator from the first element in the list
				//! \remark This iterator equals the end iterator, if the list is empty.
				inline ConstIterator begin() const
				{
					return mElements.begin();
				}

				//! \brief Get the iterator after the last element in the list
				//! \remark This iterator is always invalid, and cannot be dereferenced.
				inline ConstIterator end() const
				{
					return mElements.end();
				}

				//! \brief Get the reverse iterator from the last element in the list
				inline ReverseIterator rbegin()
				{
					return mElements.rbegin();
				}

				//! \brief Get the iterator before the first element in the list
				inline ReverseIterator rend()
				{
					return mElements.rend();
				}

				//! \brief Get the reverse iterator from the last element in the list
				inline ConstReverseIterator rbegin() const
				{
					return mElements.rbegin();
				}

				//! \brief Get the iterator before the first element in the list
				inline ConstReverseIterator rend() const
				{
					return mElements.rend();
				}

				//! \brief Test if list is empty
				//! \returns True if the list is empty, false otherwise
				//! \remark This is a lot faster than size == 0.
				inline bool empty() const
				{
					return mElements.empty();
				}

				//! \brief Removes all elements from the list
				inline void clear()
				{
					mElements.clear();
				}

				//! \brief Get the number of elements in the list
				//! \returns the number of elements in the list
				//! \remark Do not use this function to determine if the list is empty, use the empty methode.
				inline uint32 size() const
				{
					return ((unsigned int) mElements.size());
				}

				//! \brief Resize the list
				//! \param size		new size of the list
				//! \remark If the list is to small, new elements will be appended with there default value.
				//!			If the list is to big, elements at the end will be removed.
				inline void resize(const uint32 size)
				{
					mElements.resize(size);
				}

				//! \brief Sort the list with an user defined compare operator
				//! \param comp		class with bool operator() (x,y) for comparing two elements.
				template<class _Pr3>
				inline void sort(_Pr3 comp)
				{
					// we do not use the sort integrated into the std-list class since there is a bug
					// related to allocator objects.
					//mElements.sort(comp);

					if(mElements.size() < 2)
					{
						return;
					}
					else
					{	
						//ToDo: sorting is done without keeping old pointers valid
						//ToDo: make it faster

						Iterator iBegin = mElements.begin();
						Iterator iEnd = mElements.end();

						Memory::Allocator::Ptr allocator = mElements.get_allocator().getMemoryAlloc();

						ListType tempList((Memory::STLAllocator<T>(allocator)));
						ListType tempLittle((Memory::STLAllocator<T>(allocator)));

						tempList.push_back(*iBegin);
						++iBegin;

						while(iBegin != iEnd)
						{
							tempLittle.clear();
							tempLittle.push_back(*iBegin);

							tempList.merge(tempLittle, comp);
							++iBegin;
						}

						//ToDo: make it faster
						mElements = tempList;
					}


					//XAIT_ERROR("feature has been disabled due to a bug in the stl list class");
				}

				//! \brief Sort the list in ascending order, using operator< for every element
				inline void sort()
				{
					// we do not use the sort integrated into the std-list class since there is a bug
					// related to allocator objects.
					//mElements.sort();

					XAIT_FAIL( "feature has been disabled due to a bug in the stl list class");
				}

				//! \brief Insert an element
				//! \param pos		insert position (element will be inserted in front of this element)
				//! \param val		element to insert
				//! \returns an iterator to the newly inserted element.
				inline Iterator insert(const Iterator& pos, const T& val)
				{
					return mElements.insert(pos,val);
				}

				//! \brief Insert a range of elements
				//! \param pos		insert position (elements will be inserted in fron of this element)
				//! \param start	start iterator mark the first element which should be inserted
				//! \param end		end iterator is after the last element which should be inserted
				inline void insert(const Iterator& iter, const Iterator& start, const Iterator& end)
				{
					mElements.insert(iter,start,end);
				}

				//! \brief Splices another list into this one
				//! \param pos			position where to insert the other list
				//! \param insertList	list to insert
				//! \remark The operation is in O(1)
				inline void splice(const Iterator& pos, List<T>& insertList)
				{
					mElements.splice(pos,insertList.mElements);
				}

			};
		}	// namespace Container
	}	// namespace Common
}	// namespace XAIT
