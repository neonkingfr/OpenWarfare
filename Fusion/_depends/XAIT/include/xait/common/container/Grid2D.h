// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/FundamentalTypes.h>
#include <xait/common/memory/Allocator.h>
#include <xait/common/debug/Assert.h>
#include <xait/common/math/Vec2.h>


namespace XAIT
{
	namespace Common
	{
		namespace Container
		{
			template<typename T_GRIDNODE>
			class Grid2D
			{
			public:
				class RowIterator
				{
					T_GRIDNODE*		mPos;
				public:
					inline RowIterator()
						: mPos(NULL)
					{}

					inline RowIterator& operator++()
					{
						++mPos;
						return *this;
					}

					inline RowIterator& operator--()
					{
						--mPos;
						return *this;
					}

					inline const T_GRIDNODE& operator*() const
					{
						return *mPos;
					}

					inline T_GRIDNODE& operator*()
					{
						return *mPos;
					}

					inline const T_GRIDNODE* operator->() const
					{
						return mPos;
					}

					inline T_GRIDNODE* operator->()
					{
						return mPos;
					}

					inline T_GRIDNODE* get() 
					{
						return mPos;
					}

					inline const T_GRIDNODE* get() const
					{
						return mPos;
					}

					inline void initNode(const T_GRIDNODE& copyFrom)
					{
						new(mPos) T_GRIDNODE(copyFrom);
					}

					inline bool operator==(const RowIterator& other)
					{
						return mPos == other.mPos;
					}

					inline bool operator!=(const RowIterator& other)
					{
						return mPos != other.mPos;
					}
				private:
					friend class Grid2D;

					inline RowIterator(T_GRIDNODE* pos)
						: mPos(pos)
					{}
				};

				Grid2D(const Memory::Allocator::Ptr& alloc)
					: mAlloc(alloc),mGrid(NULL),mSize(0),mSizeX(0),mSizeY(0)
				{}

				Grid2D(const uint32 sizeX, const uint32 sizeY, const Memory::Allocator::Ptr& alloc)
					: mAlloc(alloc),mGrid(NULL),mSize(0),mSizeX(0),mSizeY(0)
				{
					init(sizeX,sizeY);
				}

				//! \brief initializes the grid using a copy from one node to init all nodes
				void init(const uint32 sizeX, const uint32 sizeY, const T_GRIDNODE& copyFrom)
				{
					reset();

					mSize= sizeX * sizeY;
					mSizeX= sizeX;
					mSizeY= sizeY;
					mGrid=  Memory::NewArray(copyFrom,mSize,mAlloc);
				}

				//! \brief init array but leaves it in an uninitialized state
				void initAlloc(const uint32 sizeX,const uint32 sizeY)
				{
					reset();

					mSize= sizeX * sizeY;
					mSizeX= sizeX;
					mSizeY= sizeY;
					mGrid= Memory::AllocateArray<T_GRIDNODE>(mAlloc,mSize);
				}

				//! \brief init a single node
				inline void initNode(const uint32 x, const uint32 y, const T_GRIDNODE& copyFrom)
				{
					const uint32 pos= computeLinearPos(x,y);
					new(&mGrid[pos]) T_GRIDNODE(copyFrom);
				}

				//! \brief init a single node
				inline void initNode(const uint32 pos, const T_GRIDNODE& copyFrom)
				{
					X_ASSERT_MSG(pos < mSize,"index out of range");
					new(&mGrid[pos]) T_GRIDNODE(copyFrom);
				}

				//! \brief clears the grid using a copy of one node
				inline void clear(const T_GRIDNODE& copyFrom)
				{
					if (!mGrid)
						return;

					RowIterator iter= beginRow();
					RowIterator end= endRow();

					for(; iter != end; ++iter)
					{
						// call destructor
						Memory::CallDestructor(iter.get());

						// initialize new element
						new(iter.get()) T_GRIDNODE(copyFrom);
					}
				}

				//! \brief Reset the grid to initial state, deleting the allocated array
				void reset()
				{
					if (mGrid)
					{
						Memory::DeleteArray(mGrid,mSize,mAlloc);
						mGrid= NULL;
						mSize= 0;
						mSizeX= 0;
						mSizeY= 0;
					}
				}

				inline RowIterator getRowIterator(const uint32 linearPos) const
				{
					X_ASSERT_MSG_DBG(linearPos < mSize,"index out of range");
					return RowIterator(mGrid + linearPos);
				}

				inline RowIterator beginRow() const
				{
					X_ASSERT_MSG_DBG(mGrid,"grid not initialized");
					return RowIterator(mGrid);
				}

				inline RowIterator endRow() const 
				{
					X_ASSERT_MSG_DBG(mGrid,"grid not initialized");
					return RowIterator(mGrid + mSize);
				}

				inline T_GRIDNODE* getGridNodePtr(const uint32 x, const uint32 y) const
				{
					X_ASSERT_MSG_DBG(x < mSizeX && y < mSizeY,"index out of range");
					return mGrid + (y * mSizeX + x);
				}

				inline T_GRIDNODE* getGridNodePtr(const Math::Vec2u32& pos) const
				{
					X_ASSERT_MSG_DBG(pos.mX < mSizeX && pos.mY < mSizeY,"index out of range");
					return mGrid + (pos.mY * mSizeX + pos.mX);
				}

				inline T_GRIDNODE* getGridNodePtr(const uint32 pos) const
				{
					X_ASSERT_MSG_DBG(pos < mSize,"index out of range");
					return mGrid + pos;
				}

				inline T_GRIDNODE* getGridNodePtrSecure(const uint32 x, const uint32 y) const
				{
					if (x >= mSizeX || y >= mSizeY)
						return NULL;

					return mGrid + (y * mSizeX + x);
				}

				inline T_GRIDNODE* getGridNodePtrSecure(const Math::Vec2u32& pos) const
				{
					if (pos.mX >= mSizeX || pos.mY >= mSizeY)
						return NULL;

					return mGrid + (pos.mY * mSizeX + pos.mX);
				}

				inline T_GRIDNODE* getGridNodePtrSecure(const uint32 pos) const
				{
					if (pos >= mSize)
						return NULL;
					return mGrid + pos;
				}

				inline T_GRIDNODE& getGridNode(const uint32 x, const uint32 y)
				{
					X_ASSERT_MSG_DBG(x < mSizeX && y < mSizeY,"index out of range");
					return mGrid[y * mSizeX + x];
				}

				inline T_GRIDNODE& getGridNode(const Math::Vec2u32& pos)
				{
					X_ASSERT_MSG_DBG(pos.mX < mSizeX && pos.mY < mSizeY,"index out of range");
					return mGrid[pos.mY * mSizeX + pos.mX];
				}

				inline T_GRIDNODE& getGridNode(const uint32 pos)
				{
					X_ASSERT_MSG_DBG(pos < mSize,"index out of range");
					return mGrid[pos];
				}

				inline const T_GRIDNODE& getGridNode(const uint32 x, const uint32 y) const
				{
					X_ASSERT_MSG_DBG(x < mSizeX && y < mSizeY,"index out of range");
					return mGrid[y * mSizeX + x];
				}

				inline const T_GRIDNODE& getGridNode(const Math::Vec2u32& pos) const
				{
					X_ASSERT_MSG_DBG(pos.mX < mSizeX && pos.mY < mSizeY,"index out of range");
					return mGrid[pos.mY * mSizeX + pos.mX];
				}

				inline const T_GRIDNODE& getGridNode(const uint32 pos) const
				{
					X_ASSERT_MSG_DBG(pos < mSize,"index out of range");
					return mGrid[pos];
				}

				inline T_GRIDNODE& operator[](const uint32 pos)
				{
					X_ASSERT_MSG_DBG(pos < mSize,"index out of range");
					return mGrid[pos];
				}

				inline const T_GRIDNODE& operator[](const uint32 pos) const
				{
					X_ASSERT_MSG_DBG(pos < mSize,"index out of range");
					return mGrid[pos];
				}

				inline void computeGridPos(uint32& x, uint32& y, const uint32 linearPos) const
				{
					x= linearPos % mSizeY;
					y= linearPos / mSizeX;
				}

				inline void computeGridPos(Math::Vec2u32& pos, const uint32 linearPos) const
				{
					pos.mX= linearPos % mSizeY;
					pos.mY= linearPos / mSizeX;
				}

				inline uint32 computeLinearPos(const uint32 x, const uint32 y) const
				{
					return y * mSizeX + x;
				}

				inline uint32 computeLinearPos(const Math::Vec2u32& pos) const
				{
					return pos.mY * mSizeX + pos.mX;
				}

				inline uint32 size() const
				{
					return mSize;
				}

				inline uint32 sizeX() const
				{
					return mSizeX;
				}

				inline uint32 sizeY() const
				{
					return mSizeY;
				}

			protected:
				Memory::Allocator::Ptr	mAlloc;
				T_GRIDNODE*				mGrid;
				uint32					mSize;
				uint32					mSizeX;
				uint32					mSizeY;

			};
		}
	}
}