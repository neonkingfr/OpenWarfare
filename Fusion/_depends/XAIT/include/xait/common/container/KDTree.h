// (C) xaitment GmbH 2006-2012

#pragma once


/*
**			KD - Tree Template
**
**	Author: Markus Wilhelm
**
**	a kdtree with arbitrary dimensions (max 127 dimensions)
**	every node can store userdata
**	the point type can be float, double, int
*/

#include <xait/common/FundamentalTypes.h>
#include <xait/common/container/Vector.h>
#include <xait/common/memory/Allocator.h>
#include <memory>
#include <functional>

#define		NODEFLAG_MASK_SPLITDIM		0x7F
#define		NODEFLAG_BIT_REMOVED		0x80


namespace XAIT
{
	namespace Common
	{
		namespace Container
		{
			// parameter:
			//	T_USERDATA		type of userdata in every node
			//	DIM				dimensions of the tree (max 127)
			//	T_POINT			type of the components in every point (float32, float64, int)
			template<typename T_USERDATA, uint08 DIM = 3,typename T_POINT = float32>
			class KDTree
			{
			public:
				typedef T_POINT		PointType[DIM];		// pointtype in kdtree
				typedef T_USERDATA	UserDataType;		// type of userdata
				typedef int32		KDNodeID;			// type of nodeID

				enum {
					KDNodeIDUndef = -1
				};

				//! \brief NodeFilter base class

				class NodeFilter {
				public:

					NodeFilter()
					{
					}
					~NodeFilter()
					{
					}

					virtual bool operator()(const KDNodeID, const PointType, const UserDataType) = 0;

					virtual KDNodeID getNodeByPositionPostProcess(Vector<KDNodeID>& neighbours) = 0;

				}; 

			private:
				// friend class declaration for the streamer
				template<typename T1, uint08 KDTREE_DIM, typename T2> friend class KDTreeStreamer;
				template<typename T1, uint08 KDTREE_DIM, typename T2> friend class KDNodeStreamer;
				template<typename T> friend class IDMapStreamer;

				struct KDNode {

					PointType		point;					// point, where the splitplane is located
					uint08			flags;					// lower 7-bit splitdim, higher 1-bit removed node
					KDNodeID		id;						// id of this node
					T_USERDATA		userData;				// userdata per node
				};

				struct QueueElement {
					T_POINT			distance;
					KDNodeID		nodeIndex;

					bool operator<(const QueueElement& elem) const
					{
						return distance < elem.distance;
					}

				};

				class StaticHeap
				{
					QueueElement*		heap;

					uint32		reserved;		// reserverd size of heapmemory
					uint32		heapsize;		// elements in heap

					std::less<QueueElement>	comp;			// comperator
					Memory::Allocator::Ptr	mAllocator;

				public:
					StaticHeap(const Memory::Allocator::Ptr& allocator)
						: heap(NULL),reserved(0),heapsize(0),mAllocator(allocator)
					{}

					StaticHeap(const StaticHeap& other)
						: heap(NULL),reserved(0),heapsize(0),mAllocator(other.mAllocator)
					{
						reserve(other.reserved);
						heapsize= other.heapsize;
						memcpy(heap,other.heap,sizeof(QueueElement) * heapsize);
					}

					~StaticHeap()
					{
						destroy();
					}

					// destroys the heap, frees memory
					void destroy()
					{
						if (heap) mAllocator->free(heap);
						heap= NULL;
						reserved= 0;
						heapsize= 0;
					}

					// reserve memory for at least _size entries
					void reserve(const uint32 _size)
					{
						if (_size <= reserved) return;
						QueueElement* nHeap= (QueueElement*)mAllocator->alloc(sizeof(QueueElement) * _size);
						reserved= _size;
						memcpy(nHeap,heap,sizeof(QueueElement) * heapsize);
						mAllocator->free(heap);
						heap= nHeap;
					}

					// clear heap does not free memory
					void clear()
					{
						heapsize= 0;
					}


					// push an element on the heap
					void push(const QueueElement& elem)
					{
						reserve(heapsize + 1);
						heap[heapsize]= elem;
						++heapsize;
						push_heap(heap,heap + heapsize,comp);
					}

					// pop an element from the heap
					void pop()
					{
						pop_heap(heap,heap + heapsize,comp);
						--heapsize;
					}

					// get top element from the heap
					inline const QueueElement& top() const
					{
						return *heap;
					}

					// get top element from the heap
					inline QueueElement& top()
					{
						return *heap;
					}

					// get size of the heap
					inline uint32 size() const
					{
						return heapsize;
					}


					// get all nodes on the heap
					// parameter:
					//	nodes		array to hold node indices
					// returns number of nodes 
					uint32 getNodes(Container::Vector<KDNodeID>& nodes)
					{
						if (nodes.size() < heapsize) nodes.resize(heapsize);

						for(uint32 i= 0; i < heapsize; ++i) {
							nodes[i]= heap[i].nodeIndex;
						}

						return heapsize;
					}
				};


				struct SmallerKDNodeOperator
				{
					uint08 currSplitDim;

					bool operator()(const KDNode& nodeA, const KDNode& nodeB) const
					{
						return nodeA.point[currSplitDim] < nodeB.point[currSplitDim];
					}
				};


				class IDMap 
				{
					template<typename T> friend class IDMapStreamer;

					Container::Vector<KDNodeID>		idmap;		// vector storing id mappings
					uint32							start;		// start of freelist

				public:
					IDMap(const Memory::Allocator::Ptr& allocator)
						: idmap(allocator),start(0)
					{
						idmap.pushBack(0);
					}


					// clear the id map
					void clear()
					{
						idmap.clear();
						idmap.pushBack(0);
						start= 0;
					}


					// get new id
					// parameter:
					//	mapTo	index where new id maps to
					// returns new id
					KDNodeID newID(const KDNodeID mapTo)
					{
						const KDNodeID id= reserveID();
						idmap[id]= mapTo;
						return id;
					}

					// delete id
					// parameter:
					//	id		id which should be deleted
					// returns true if id has been deleted
					bool deleteID(const KDNodeID id)
					{
						if (isValidID(id)) {
							releaseID(id);
							return true;
						}
						return false;
					}

					// remap id to a new map value
					// parameter:
					//	id		id where mapping value should be changed
					//	mapTo	new mapping value
					// returns true if id has been remapped
					bool remapID(const KDNodeID id, const KDNodeID mapTo)
					{
						if (isValidID(id)) {
							idmap[id]= mapTo;
							return true;
						}
						return false;
					}

					// get mapped value for an id
					// there is no check that the id is valid
					// parameter:
					//	id		id where to get the mapped value
					// returns the mapped value
					inline KDNodeID getMap(const KDNodeID id) const
					{
						return idmap[id];
					}

					// check if id is valid
					inline bool isValidID(const KDNodeID id) const
					{
						return (id >= 0 && id < (KDNodeID)(idmap.size() - 1) && idmap[id] >= 0);
					}
				private:
					// reserve an id in the map array
					KDNodeID reserveID()
					{
						const KDNodeID id= (KDNodeID)start;
						const uint32 size= (uint32)idmap.size() - 1;

						if (start == size) {
							idmap.pushBack(0);
							++start;
						} else {
							start= idmap[start] * -1;
						}
						return id;
					}

					// release an id
					void releaseID(const KDNodeID id)
					{
						// extend freelist
						idmap[id]= -1 * start;

						// set new start of the list
						start= id;
					}
				};


				int32						treeSize;			// size of elements in initialised tree (without added nodes which are not in the tree)
				Container::Vector<KDNode>	nodes;				// all nodes of the tree
				IDMap						idMap;				// maps indices 
				T_POINT						maxRadius;			// maximum radius to include all nodes

				bool						needUpdate;			// remembers update state
				bool						removedNode;		// remembers if since last update a node has been removed

				StaticHeap					heap;				// heap that is being used by the nearest neighbour search

				PointType					convPoint;			// internal point for converting points
				Memory::Allocator::Ptr		mAllocator;

			public:
				KDTree(const Memory::Allocator::Ptr& allocator)
					: treeSize(0),nodes(allocator),idMap(allocator),needUpdate(false),removedNode(false),heap(allocator),
					mAllocator(allocator)
				{}


				// destroys tree
				// does not destroy pointertype userdata
				~KDTree()
				{
					clear();
				}


				// clear tree
				// does not delete pointertype userdata
				void clear()
				{
					nodes.clear();
					idMap.clear();
					treeSize= 0;
					needUpdate= false;
				}


				// convert any pointdatatype to PointType
				// this will only work for pointdatatypes which have at the beginning the components of the point
				// in the exact same format as the kdtree template is defined for.
				// the pointdatatype could also be a class which have for example only x,y,z component.
				// the ordering of the components is also relevant.
				// use only if you now what you are doing. otherwise write your own convert function for the kdtree internal
				// point format
				// parameter:
				//	point		pointer to your pointdatastructure which should be converted
				// returns a converted point, this point is managed by the kdtree and therefore only one point can be converted at a time
				const PointType& convertPoint(const void* point)
				{
					memcpy(convPoint,point,sizeof(PointType));
					return convPoint;
				}


				// add a point to the tree
				// this will queue a new point for later insertion
				// the point will be added to the tree if updateTree is called
				// this is automaticly done if a search is started
				// parameter:
				//	point		new point to add
				//	userData	userdata associated to this point
				// returns an id to identify the point for later modification
				KDNodeID addPoint(const PointType point, T_USERDATA userData)
				{
					KDNode node;

					memcpy(node.point,point,sizeof(PointType));
					node.userData= userData;

					// get new id for this node
					node.id= idMap.newID((KDNodeID)nodes.size());

					node.flags= 0;

					nodes.pushBack(node);
					needUpdate= true;

					return node.id;
				}


				// remove point from the tree O(log n)
				// this marks the point in the tree as removed, it will no longer take part in search functions
				// the point will be physicaly removed if updateTree is called
				// parameter:
				//	point		point to remove
				//	userData	userdata of the point. this is required to identify the unique point
				// returns true if point removed
				bool removePoint(const PointType point, T_USERDATA userData)
				{
					// find all nodes that match this point
					Container::Vector<KDNodeID> matches(mAllocator);
					findAllPoints(matches,point);

					// remove that node that matches also the userdata
					for(uint32 i= 0; i < matches.size(); ++i) {
						const KDNodeID realID= idMap.getMap(matches[i]);

						if (nodes[realID].userData == userData && (!(nodes[realID].flags & NODEFLAG_BIT_REMOVED))) {
							nodes[realID].flags|= NODEFLAG_BIT_REMOVED;
							idMap.deleteID(nodes[realID].id);
							removedNode= true;
							return true;
						}
					}
					return false;
				}


				// remove point from the tree by index O(1)
				// this marks the point in the tree as removed, it will no longer take part in search functions
				// the point will be physicaly removed if updateTree is called
				// parameter:
				//	id		id of the node to remove
				// returns true if point removed
				bool removePoint(const KDNodeID id)
				{
					const KDNodeID realID= idMap.getMap(id);

					if (idMap.isValidID(id) && (!(nodes[realID].flags & NODEFLAG_BIT_REMOVED))) {
						nodes[realID].flags|= NODEFLAG_BIT_REMOVED;
						idMap.deleteID(id);
						removedNode= true;
						return true;
					}
					return false;
				}


				// move a point
				// this action requires a updateTree for propper function of searching
				// this is done automaticly if a search is started
				// parameter:
				//	id		id of the node to move
				//	point	new position where to move the node
				// returns true if point could be moved
				bool movePoint(const KDNodeID id, const PointType point)
				{
					if (!idMap.isValidID(id)) return false;

					const KDNodeID vecPos= idMap.getMap(id);

					// test wether the node is integrated into the tree, or queued at the end for later insertion
					if (vecPos < treeSize) {
						T_USERDATA userData= nodes[vecPos].userData;

						// remove the point
						removePoint(id);

						// add the point with a new position
						// because of the special implementation of IDMap the new id will be the same as the deleted one
						addPoint(point,userData);
					} else {
						// the point is not yet integrated into the tree, thus we could simply change the position
						memcpy(nodes[vecPos].point,point,sizeof(PointType));
					}
					return true;
				}


				// find first point excactly matching the specified position
				// parameter:
				//	id			the first match
				//	point		point to search for
				// returns true if an match found
				bool findFirstPoint(KDNodeID& id, const PointType point)
				{
					X_ASSERT(!"findPointRec: root not defined");
					/*
					if (needUpdate) updateTree();

					Container::Vector<KDNodeID> matches(mAllocator);
					if (matches.empty()) {
						return false
					} else {
						id= matches[0];
						return true;
					}
					*/
					return false;
				}

				// find all points with excact match for the specified position
				// parameter:
				//	matches		array of nodes indices that match (returned)
				//	point		point to search for
				void findAllPoints(Container::Vector<KDNodeID>& matches, const PointType point)
				{
					if (needUpdate) updateTree();
					findPointRec(matches,0,treeSize,point,false);
				}


				// get userdata from a node by index
				// parameter:
				//	nodeIndex	index of node
				// returns the userdata
				inline T_USERDATA getUserData(const KDNodeID nodeIndex) const
				{
					return nodes[idMap.getMap(nodeIndex)].userData;
				}

				// set userdata for a node
				// parameter:
				//	nodeIndex	index of the node
				//	userData	new userData for that node
				inline void setUserData(const KDNodeID nodeIndex, const T_USERDATA userData)
				{
					nodes[idMap.getMap(nodeIndex)].userData= userData;
				}

				// get position of a node
				// parameter:
				//	point		returned position of the node
				//	nodeIndex	index of the node
				inline void getPoint(PointType point, const KDNodeID nodeIndex) const
				{
					memcpy(point,nodes[idMap.getMap(nodeIndex)].point,sizeof(PointType));
				}

				// get position of a node
				// parameter:
				//	nodeIndex	index of the node
				// returns the point as const
				inline const PointType& getPoint(const KDNodeID nodeIndex) const
				{
					return nodes[idMap.getMap(nodeIndex)].point;
				}


				// find all neighbours with a maximum distance to the specified point of radius
				// parameter:
				//	neighbours		array of node indices of found neighbours
				//	point			position to find neighbours for
				//	radius			maximum distance of neighbour <-> point
				void doRangeSearch(Container::Vector<KDNodeID>& neighbours, const PointType point, const T_POINT radius)
				{
					if (needUpdate) updateTree();
					doRangeSearchRec(neighbours,0,treeSize,point,radius * radius);
				}


				// find "k" nearest neighbours with a maximum distance "radius" to the specified point
				// if in sphere with center point and radius are less than k nearest neighbour the search will stop
				// parameter:
				//	neighbours		array of node indices of found nearest neighbours (the array should have the size k or greater)
				//	point			position to find neighbours for
				//	k				maximum number of nearest neighbours
				//	radius			maximum distance of neighbour <-> point (if radius < 0 , maximum radius will be used)
				// returns the number of found neighbours
				uint32 doNearestNeighbourSearch(Container::Vector<KDNodeID>& neighbours, const PointType point, const uint32 k, const T_POINT _radius)
				{
					if (needUpdate) updateTree();

					heap.clear();

					T_POINT radius;
					if (_radius < 0) {
						radius= maxRadius * maxRadius;
					} else {
						radius= _radius * _radius;
					}

					doNearestNeighbourSearchRec(heap,0,treeSize,point,k,radius);

					// copy results into return array
					return heap.getNodes(neighbours);
				}

				// find "k" nearest neighbours with a maximum distance "radius" to the specified point
				// this function also filters potential nodes by a NodeFilter function
				// a user can thus decide if a node should take part in the nearest neighbour search or not
				// if in sphere with center point and radius are less than k nearest neighbour the search will stop
				// parameter:
				//	neighbours		array of node indices of found nearest neighbours (the array should have the size k or greater)
				//	point			position to find neighbours for
				//	k				maximum number of nearest neighbours
				//	radius			maximum distance of neighbour <-> point (if radius < 0, the maxium radius will be used)
				//	filter			nodefilter function
				// returns the number of found neighbours
				uint32 doFilteredNearestNeighbourSearch(Container::Vector<KDNodeID>& neighbours, const PointType point, const uint32 k, const T_POINT _radius, NodeFilter* filter)
				{
					if (needUpdate) updateTree();

					heap.clear();

					T_POINT radius;
					if (_radius < 0) {
						radius= maxRadius * maxRadius;
					} else {
						radius= _radius * _radius;
					}

					doFilteredNearestNeighbourSearchRec(heap,0,treeSize,point,k,radius,filter);

					// copy results into return array
					return heap.getNodes(neighbours);
				}


				// update / build tree
				// this does a complete rebuild of the tree
				void updateTree()
				{
					// if a node has been removed, rearrange array before buildtree
					if (removedNode) {
						uint32 assignpos= 0;
						uint32 removed= 0;
						for(uint32 i= 0; i < nodes.size(); ++i) {
							if (nodes[i].flags & NODEFLAG_BIT_REMOVED) {
								++removed;
							} else {
								nodes[assignpos]= nodes[i];
								++assignpos;
							}
						}
						nodes.resize(nodes.size() - removed);
						removedNode= false;
					}

					treeSize= (int32)nodes.size();
					maxRadius= 0;
					buildTreeRec(0,treeSize);
					needUpdate= false;
				}


			private:

				// find recursivly one or more excact matches
				// parameter:
				//	matches		array of node indices that match (returned)
				//	nodeIndex	index of actual node
				//	point		point to search
				//	first		if true search only the first match
				void findPointRec(Container::Vector<KDNodeID>& matches, const KDNodeID start, const KDNodeID end, const PointType point, bool first)
				{
					if (start == end) return;

					const KDNodeID nodeIndex= (start + end) / 2;
					const KDNode* node= &(nodes[nodeIndex]);
					const int32 removed= node->flags & NODEFLAG_BIT_REMOVED;
					const uint08 splitDim= node->flags & NODEFLAG_MASK_SPLITDIM;

					// check if this node is not deleted
					if (!removed) { 
						// test if this is a match
						if (!memcmp(point,node->point,sizeof(PointType))) {
							matches.pushBack(node->id);
							if (first) return;	// break if only one match is searched
						}
					}

					// travers into branches
					if (point[splitDim] < node->point[splitDim]) {
						findPointRec(matches,start,nodeIndex,point,first);
					} else {
						findPointRec(matches,nodeIndex + 1,end,point,first);
					}
				}

				// compute squared distance
				inline T_POINT computeSqrDistance(const PointType point1, const PointType point2)	const
				{
					T_POINT sum= 0;
					T_POINT dist;
					for(uint08 dim= 0; dim < DIM; ++dim) {
						dist= point1[dim] - point2[dim];
						sum+= dist * dist;
					}
					return sum;
				}


				// recursive version of doRangeSearch
				// parameter:
				//	neighbours		array of node indices of found neighbours
				//	nodeIndex		current node
				//	point			position to find neighbours for
				//	radius			squared maximum distance of neighbour <-> point
				void doRangeSearchRec(Container::Vector<KDNodeID>& neighbours, const KDNodeID start, const KDNodeID end, const PointType point, const T_POINT radius)
				{
					if (start == end) return;

					const KDNodeID nodeIndex= (start + end) / 2;
					const KDNode* node= &(nodes[nodeIndex]);
					const int32 removed= node->flags & NODEFLAG_BIT_REMOVED;

					// check if this node is not deleted
					if (!removed) {
						const T_POINT distance= computeSqrDistance(node->point,point);
						if (distance <= radius) {
							neighbours.pushBack(node->id);
						}
					}

					// recurse into subtrees
					const uint08 splitDim= node->flags & NODEFLAG_MASK_SPLITDIM;

					// decide where to travel left - right or both
					if ((point[splitDim] - radius) < node->point[splitDim]) {
						doRangeSearchRec(neighbours,start,nodeIndex,point,radius);
					}
					if ((point[splitDim] + radius) >= node->point[splitDim]) {
						doRangeSearchRec(neighbours,nodeIndex + 1,end,point,radius);
					}
				}

				// recursive version of doNearestNeighbourSearch
				// parameter:
				//	neighbours		array of node indices of found nearest neighbours
				//	point			position to find neighbours for
				//	start			startindex in nodearray
				//	end				endindex in nodearray
				//	k				maximum number of nearest neighbours
				//	radius			squared maximum distance of neighbour <-> point 
				void doNearestNeighbourSearchRec(StaticHeap& neighbours, const KDNodeID start, const KDNodeID end, const PointType point, const uint32 k, T_POINT& radius)
				{
					if (start == end) {
						//	TRACE_MESSAGE("done : " << start);
						return;
					}
					//TRACE_MESSAGE("call : " << start << " - " << end);

					const KDNodeID nodeIndex= (start + end) / 2;
					const KDNode& node= nodes[nodeIndex];
					const int32 removed= node.flags & NODEFLAG_BIT_REMOVED;

					// check if this node is not deleted
					if (!removed) {
						const T_POINT distance= computeSqrDistance(node.point,point);
						//TRACE_MESSAGE("distance: " << sqrtf(distance) << " radius: " << sqrtf(radius));
						if (distance <= radius) {
							QueueElement tmp;
							tmp.distance= distance;
							tmp.nodeIndex= node.id;

							// insert node into queue
							// if the size of the priority queue has not reached k , just insert
							if (neighbours.size() < k) 
							{
								neighbours.push(tmp);

								// set new radius
								radius= neighbours.top().distance;
							} 
							else 
							{					
								// add node only if the distance is smaller than the biggest element, otherwise discard this node
								if (neighbours.top().distance > tmp.distance) {
									// remove furthest away node
									neighbours.pop();
									// insert this as a new nearest neighbour
									neighbours.push(tmp);

									// set new radius
									radius= neighbours.top().distance;
								}
							}
						}
					}

					// recurse into subtrees
					// decide where to travel left - right or both
					const uint08 splitDim= node.flags & NODEFLAG_MASK_SPLITDIM;
					const T_POINT& comp= point[splitDim];
					const T_POINT& splitPoint= node.point[splitDim];

					// in the build function , left and right tree is defined as
					// left:	start=	start
					//			end=	medianpos
					// right:	start=	medianpos + 1
					//			end=	end
					// medianpos is our nodeIndex
					if (comp < splitPoint) {
						if ((comp - radius) < splitPoint) {
							doNearestNeighbourSearchRec(neighbours,start,nodeIndex,point,k,radius);
						}
						if ((comp + radius) >= splitPoint) {
							doNearestNeighbourSearchRec(neighbours,nodeIndex + 1,end,point,k,radius);
						}
					} else {
						if ((comp + radius) >= splitPoint) {
							doNearestNeighbourSearchRec(neighbours,nodeIndex + 1,end,point,k,radius);
						}
						if ((comp - radius) < splitPoint) {
							doNearestNeighbourSearchRec(neighbours,start,nodeIndex,point,k,radius);
						}
					}
				}


				// recursive version of doFilteredNearestNeighbourSearch
				// parameter:
				//	neighbours		array of node indices of found nearest neighbours
				//	point			position to find neighbours for
				//	k				maximum number of nearest neighbours
				//	radius			squared maximum distance of neighbour <-> point 
				void doFilteredNearestNeighbourSearchRec(StaticHeap& neighbours, const KDNodeID start, const KDNodeID end, const PointType point, const uint32 k, T_POINT& radius, NodeFilter* filter)
				{
					if (start == end) return;

					const KDNodeID nodeIndex= (start + end) / 2;
					const KDNode& node= nodes[nodeIndex];
					const int32 removed= node.flags & NODEFLAG_BIT_REMOVED;

					// check if this node is not deleted
					if (!removed) {
						const T_POINT distance= computeSqrDistance(node.point,point);

						if (distance <= radius) {
							if ((*filter)(nodeIndex,node.point,node.userData)) {
								QueueElement tmp;
								tmp.distance= distance;
								tmp.nodeIndex= node.id;

								// insert node into queue
								// if the size of the priority queue has not reached k , just insert
								if (neighbours.size() < k) {
									neighbours.push(tmp);
								} else {
									// add node only if the distance is smaller than the biggest element, otherwise discard this node
									if (neighbours.top().distance > tmp.distance) {
										// remove furthest away node
										neighbours.pop();
										// insert this as a new nearest neighbour
										neighbours.push(tmp);

										// set new radius
										radius= neighbours.top().distance;
									}
								}
							}
						}
					}

					// recurse into subtrees
					// decide where to travel left - right or both
					const uint08 splitDim= node.flags & NODEFLAG_MASK_SPLITDIM;
					const T_POINT& comp= point[splitDim];
					const T_POINT& splitPoint= node.point[splitDim];

					// in the build function , left and right tree is defined as
					// left:	start=	start
					//			end=	medianpos
					// right:	start=	medianpos + 1
					//			end=	end
					// medianpos is our nodeIndex
					if (comp < splitPoint) {
						if ((comp - radius) < splitPoint) {
							doFilteredNearestNeighbourSearchRec(neighbours,start,nodeIndex,point,k,radius,filter);
						}
						if ((comp + radius) >= splitPoint) {
							doFilteredNearestNeighbourSearchRec(neighbours,nodeIndex + 1,end,point,k,radius,filter);
						}
					} else {
						if ((comp + radius) >= splitPoint) {
							doFilteredNearestNeighbourSearchRec(neighbours,nodeIndex + 1,end,point,k,radius,filter);
						}
						if ((comp - radius) < splitPoint) {
							doFilteredNearestNeighbourSearchRec(neighbours,start,nodeIndex,point,k,radius,filter);
						}
					}
				}


				void buildTreeRec(const KDNodeID start, const KDNodeID end)
				{
					if (start == end) {
						// empty list ,return
						return;
					}

					// find maximum splitdimension
					T_POINT min[DIM];
					T_POINT max[DIM];
					typename Container::Vector<KDNode>::Iterator start_iter= nodes.begin() + start;
					typename Container::Vector<KDNode>::Iterator end_iter= nodes.begin() + end;

					for(uint08 dim= 0; dim < DIM; ++dim) 
					{
						min[dim]= start_iter->point[dim];
						max[dim]= start_iter->point[dim];
					}
					++start_iter;

					for(typename Container::Vector<KDNode>::Iterator iter= start_iter; iter != end_iter; ++iter) 
					{
						for(uint08 dim= 0; dim < DIM; ++dim) 
						{
							if (iter->point[dim] < min[dim]) min[dim]= iter->point[dim];
							if (iter->point[dim] > max[dim]) max[dim]= iter->point[dim];
						}
					}

					uint08 maxDim= 0;
					T_POINT maxDistance= -1;
					T_POINT distance;

					for(uint08 dim= 0; dim < DIM; ++dim) 
					{
						distance= max[dim] - min[dim];
						if (distance > maxDistance) 
						{
							maxDistance= distance;
							maxDim= dim;
						}
					}

					// set maximum radius (this will only assign a value in the first call)
					if (distance > maxRadius) maxRadius= distance;

					// sort the nodes in the maximum dimension
					SmallerKDNodeOperator smaller;
					smaller.currSplitDim= maxDim;
					sort(start_iter,end_iter,smaller);

					// compute medianposition
					KDNodeID medianpos= (end + start) / 2;

					KDNode& median= nodes[medianpos];

					// set splitdimension for medianpos
					median.flags= maxDim;

					// remap medianposition
					idMap.remapID(median.id,medianpos);

					// compute childtrees
					// left childtree
					buildTreeRec(start,medianpos);

					// right childtree
					buildTreeRec(medianpos+1,end);

					return;
				}
			};
		}
	}
}
