// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/String.h>
#include <xait/common/SharedPtr.h>
#include <xait/common/reflection/datatype/DatatypeTypedefs.h>
#include <xait/common/reflection/datatype/Datatypes.h>
#include <xait/common/reflection/function/CFunction.h>
#include <xait/common/reflection/function/FunctionRegistration.h>


namespace XAIT
{
	namespace Common
	{
		namespace Reflection
		{
			namespace Datatype
			{
				//! \brief manager that handles all datatypes
				class DatatypeManager : public Memory::MemoryManaged
				{
				public:
					typedef SharedPtr<DatatypeManager>	Ptr;

					//! \brief constructor
					XAIT_COMMON_DLLAPI DatatypeManager();

					//! \brief destructor
					XAIT_COMMON_DLLAPI ~DatatypeManager();


					template<typename T_DATATYPE>
					Datatype<T_DATATYPE>* registerFundamentalDatatype(const Common::String& name, const T_DATATYPE& defaultValue,T_DATATYPE* (*parseFunction)(const Common::String&,const Common::Memory::AllocatorPtr&))
					{
						DatatypeBase* datatype = 0;
						if (Datatype<T_DATATYPE>::exists())
						{
							datatype = Datatype<T_DATATYPE>::getDatatype();
							XAIT_FAIL("try to register DataType \"" << name.getConstCharPtr() << "\" but exists already with name \"" << datatype->getName().getConstCharPtr() << "\" !");
						}
						else
						{
#ifndef XAIT_RTTI_DISABLED
							bool exists = checkExistance(name, typeid(T_DATATYPE).name());
#else
							bool exists = checkExistance(name, name + "(no RTTI)");
#endif	
							if (!exists)
							{
								uint32 id = mDatatypes.size();
								datatype = Datatype<T_DATATYPE>::createFundamentalDatatype(name,id,defaultValue,parseFunction,mAllocator);
								mDatatypes.pushBack(datatype);
								mDatatypeNameMapping[name] = id;
#ifndef XAIT_RTTI_DISABLED
								mTypeIDNameMapping[typeid(T_DATATYPE).name()] = id;
#else
								mTypeIDNameMapping[name + "(no RTTI)"] = id;
#endif
							}
							else
							{
								Datatype<T_DATATYPE>* existantDatatype = (Datatype<T_DATATYPE>*)(getDatatype(getDatatypeID(name)));

								datatype = Datatype<T_DATATYPE>::createReference(existantDatatype,parseFunction);
							}
						}
						return (Datatype<T_DATATYPE>*)datatype;
					}

					//! \brief register a new datatype
					//! \param name					name of the datatype
					//! \param defaultValue			the default value of the datatype (used to initalize value)
					//! \param parseFunction		a function that convert a string to an instance of the datatype on the heap
					//! \param allocator			an allocatorPtr
					//! \param additionMethod		pointer to addition method
					//! \param substractionMethod	pointer to substraction method
					//! \param multiplicationMethod	pointer to multiplication method
					//! \param divisionMethod		pointer to division method
					//! \param equalMethod			pointer to equalMethod method
					//! \param unequalMethod		pointer to unequalMethod method
					//! \param greaterMethod		pointer to greaterMethod method
					//! \param greaterEqualMethod	pointer to greaterEqualMethod method
					//! \param smallerMethod		pointer to smallerMethod method
					//! \param smallerEqualMethod	pointer to smallerEqualMethod method
					template<typename T_DATATYPE>
					Datatype<T_DATATYPE>* registerDatatype(const Common::String& name, const T_DATATYPE& defaultValue,
						T_DATATYPE* (*parseFunction)(const Common::String&,const Common::Memory::AllocatorPtr&),
						typename NFDatatype<T_DATATYPE>::BasicOperation additionMethod = NULL, 
						typename NFDatatype<T_DATATYPE>::BasicOperation substractionMethod = NULL,
						typename NFDatatype<T_DATATYPE>::BasicOperation multiplicationMethod = NULL, 
						typename NFDatatype<T_DATATYPE>::BasicOperation divisionMethod = NULL,
						typename NFDatatype<T_DATATYPE>::ComparisionOperation equalMethod = NULL,
						typename NFDatatype<T_DATATYPE>::ComparisionOperation unequalMethod = NULL,
						typename NFDatatype<T_DATATYPE>::ComparisionOperation greaterMethod = NULL,
						typename NFDatatype<T_DATATYPE>::ComparisionOperation greaterEqualMethod = NULL,
						typename NFDatatype<T_DATATYPE>::ComparisionOperation smallerMethod = NULL,
						typename NFDatatype<T_DATATYPE>::ComparisionOperation smallerEqualMethod = NULL)
					{
						DatatypeBase* datatype = 0;
						if (Datatype<T_DATATYPE>::exists())
						{
							datatype = Datatype<T_DATATYPE>::getDatatype();
							XAIT_FAIL("try to register DataType \"" << name.getConstCharPtr() << "\" but exists already with name \"" << datatype->getName().getConstCharPtr() << "\" !");
						}
						else
						{
#ifndef XAIT_RTTI_DISABLED
							bool exists = checkExistance(name, typeid(T_DATATYPE).name());
#else
							bool exists = checkExistance(name, "");
#endif	
							if (!exists)
							{
								uint32 id = mDatatypes.size();
								datatype = NFDatatype<T_DATATYPE>::createDatatype(name,id,defaultValue,parseFunction,
									additionMethod,substractionMethod,multiplicationMethod,divisionMethod,
									equalMethod,unequalMethod,greaterMethod,greaterEqualMethod,
									smallerMethod,smallerEqualMethod,mAllocator);

								mDatatypes.pushBack(datatype);
								mDatatypeNameMapping[name] = id;
#ifndef XAIT_RTTI_DISABLED
								mTypeIDNameMapping[typeid(T_DATATYPE).name()] = id;
#endif
							}
							else
							{
								NFDatatype<T_DATATYPE>* existantDatatype = (NFDatatype<T_DATATYPE>*)(getDatatype(getDatatypeID(name)));

								datatype = NFDatatype<T_DATATYPE>::createReference(existantDatatype,parseFunction,
									additionMethod,substractionMethod,multiplicationMethod,divisionMethod,
									equalMethod,unequalMethod,greaterMethod,greaterEqualMethod,
									smallerMethod,smallerEqualMethod);
							}
						}
						return (Datatype<T_DATATYPE>*) datatype;

					}

					//! \brief returns the datatypeID of a registered type
					//! \param name		the name of the datatype
					//! \return the id of the datatype if the datatype is registered, an invalid datatypeID otherwise
					XAIT_COMMON_DLLAPI DatatypeID getDatatypeID(const Common::String& name) const;


					XAIT_COMMON_DLLAPI bool checkExistance(const Common::String& name, const Common::String& typeName) const;

					//! \brief gets the datatypeID by an ID
					//!	\param datatypeID		the datatypeID
					//! \returns a pointer to a datatypeID, if the datatypeID is invalid, NULL is returned
					DatatypeBase* getDatatype(DatatypeID datatypeID)
					{
						if (GetIDWrapperValue(datatypeID) >= mDatatypes.size())
							return NULL;

						return mDatatypes[GetIDWrapperValue(datatypeID)];
					}


					//! \brief finish and checks initialization of all datatypes
					//! \remarks	used but uninitialized datatypes will result in assertion at this point
					XAIT_COMMON_DLLAPI void finishInitialization();

					//! \brief gets the name of an datatype
					XAIT_COMMON_DLLAPI Common::String getDatatypeName(const DatatypeID datatypeID) const;

					XAIT_COMMON_DLLAPI const Common::Memory::AllocatorPtr& getMemoryAllocator() const;

				private:
					




					DatatypeID createDatatypeDummy(const Common::String& name);


					// todo fix Common::Container::StringLookUp<DatatypeID>			mDatatypeNameMapping;
					Common::Container::StringLookUp<DatatypeID>			mDatatypeNameMapping;
					Common::Container::StringLookUp<DatatypeID>			mTypeIDNameMapping;
					Common::Container::Vector<DatatypeBase*>			mDatatypes;	
				};
			}
		}		
	}
}


