// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/IDWrapper.h>
#include <typeinfo>

namespace XAIT
{
	namespace Common
	{
		namespace Reflection
		{
			namespace Datatype
			{
				// forward declaration
				class DatatypeManager;
			}
		}

		XAIT_IDWRAPPER_TEMPLATE_GENERATION(uint32,Reflection::Datatype::DatatypeManager,0);

		namespace Reflection
		{
			namespace Datatype
			{
				// id of a datatype
				typedef Common::IDWrapper<uint32,DatatypeManager,0>				DatatypeID;
			}
		}
	}
}
