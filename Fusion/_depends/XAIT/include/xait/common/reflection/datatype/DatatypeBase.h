// (C) xaitment GmbH 2006-2012

#pragma once


#include <xait/common/memory/MemoryManaged.h>
#include <xait/common/reflection/datatype/DatatypeTypedefs.h>
#include <xait/common/reflection/function/FunctionArgumentList.h>
#include <xait/common/serialize/SerializeStorage.h>
#include <xait/common/reflection/function/Signature.h>

// makro to get a datatypeID
#define GET_DATATYPE_ID(typename) \
	XAIT::Common::Reflection::Datatype::DatatypeIDHelpClass<typename>::getDatatypeID()


namespace XAIT
{
	namespace Common
	{
		namespace Reflection
		{
			namespace Datatype
			{
				//! \brief the abstract base class of all datatype wrapper
				class DatatypeBase : public Memory::MemoryManaged
				{
				public:
					//! \brief destructor
					virtual ~DatatypeBase()
					{}

					virtual void initializeValue(void* memoryBlock) = 0;

					//! \brief initializes a value
					//! \param source		the init value
					//! \param destination	the memory to be initialize
					virtual void initializeValue(void* source, void* destination) = 0;

					//! \brief sets a value
					//! \param source		pointer to the new value
					//! \param destination	pointer to the changed value
					virtual void setValue(void* source, void* destination) = 0;

					//! \brief sets a value to the defaultvalue of the datatype
					//! \param location		pointer to the value
					virtual void setDefaultValue(void* location) = 0;

					//! \brief getter datatypeID
					//! \returns the datatypeID
					virtual DatatypeID getDatatypeID() const = 0;


					//! \brief getter datatype name
					//! \returns the datatype name
					virtual String getName() = 0;

					//! \brief getter datatype size
					//! \returns the size of the datatype
					virtual uint32 getSize() = 0;

					//! \brief calls the destruction of an object
					//! \param location		pointer to the object
					virtual void callDestructor(void* location) = 0;

					//! \brief deletes an object
					//! \param location		pointer to the object
					//! \remark the memory will be freed
					virtual void deleteObject(void* location) = 0;

					//! \brief creates an instance of the datatype and initalize the value by parsing a string
					//! \param valueString			the initialization value of the string
					//! \param allocator			the memory allocator used for the new object
					virtual uint08* parseValue(const String& valueString, const Memory::AllocatorPtr& allocator) = 0;

					//! \brief creates an instance of the datatype on the heap
					//! \returns a pointer to the heap object
					virtual void* createHeapObject() = 0;

					//! \brief creates an instance of the datatype on the heap like a copy constructor
					//! \param value		pointer to another instance of this type
					//! \returns a pointer to the heap object
					virtual void* createHeapObject(const void* value) = 0;

					//! \brief destroys an heap instance of the datatype
					//! \param object		pointer to the object
					virtual void destroyHeapObject(void* object) = 0;

					//! \brief creates an instance of the datatype on the heap
					//! \param allocator	the used allocator
					//! \returns a pointer to the heap object
					virtual void* createHeapObject(const Common::Memory::AllocatorPtr& allocator) = 0;

					//! \brief creates an instance of the datatype on the heap like a copy constructor
					//! \param value		pointer to another instance of this type
					//! \param allocator	the used allocator
					//! \returns a pointer to the heap object
					virtual void* createHeapObject(const void* value, const Common::Memory::AllocatorPtr& allocator) = 0;

					//! \brief destroys an heap instance of the datatype
					//! \param object		pointer to the object
					//! \param allocator	the used allocator
					virtual void destroyHeapObject(void* object, const Common::Memory::AllocatorPtr& allocator) = 0;

					//! \brief gets an functionid of a member function of the datatype
					//! \param	name		name of the function
					//! \param	signature	signature of the function WITHOUT returnvalue type
					//! \returns the FunctionID of the function
					//! \remark if no function exists with this name and signature, an invalid FunctionID is returned
					virtual Function::FunctionID getFunctionIDByArgumentSignature(const String& name,const Container::Vector<Reflection::Datatype::DatatypeID>& signature) = 0;

					//! \brief gets the argument signature
					//! \param functionID	the id of the function
					//! \param signature	the argument signature of the function
					virtual void getArgumentSignature(Container::Vector<Datatype::DatatypeID>& argumentSignature,const Function::FunctionID functionID) = 0;

					//! \brief gets the return type value of an function
					//! \param functionID		the id of the function
					//! \returns the DatatypeID of the returnvalue type of the function
					virtual DatatypeID getReturnValueType(const Function::FunctionID functionID) = 0;


					//! \brief check if the datatype provides an addition operator
					//! \returns true if datatype provides an addition operator; else otherwise
					virtual bool hasAddition()
					{
						return true;
					}

					//! \brief check if the datatype provides a substraction operator
					//! \returns true if datatype provides a substraction operator; else otherwise
					virtual bool hasSubstraction()
					{
						return true;
					}

					//! \brief check if the datatype provides a multiplication operator
					//! \returns true if datatype provides a multiplication operator; else otherwise
					virtual bool hasMultiplication()
					{
						return true;
					}

					//! \brief check if the datatype provides a division operator
					//! \returns true if datatype provides a division operator; else otherwise
					virtual bool hasDivision()
					{
						return true;
					}

					//! \brief check if the datatype provides an equal operator
					//! \returns true if datatype provides an equal operator; else otherwise
					virtual bool hasEqual()
					{
						return true;
					}

					//! \brief check if the datatype provides an unequal operator
					//! \returns true if datatype provides an unequal operator; else otherwise
					virtual bool hasUnequal()
					{
						return true;
					}
					//! \brief check if the datatype provides a greater operator
					//! \returns true if datatype provides a greater operator; else otherwise
					virtual bool hasGreater()
					{
						return true;
					}

					//! \brief check if the datatype provides a greaterequal operator
					//! \returns true if datatype provides a greaterequal operator; else otherwise
					virtual bool hasGreaterEqual()
					{
						return true;
					}

					//! \brief check if the datatype provides a smaller operator
					//! \returns true if datatype provides a smaller operator; else otherwise
					virtual bool hasSmaller()
					{
						return true;
					}

					//! \brief check if the datatype provides a smallerequal operator
					//! \returns true if datatype provides a smallerequal operator; else otherwise
					virtual bool hasSmallerEqual()
					{
						return true;
					}

					//! \brief calls the addition operator
					//! \param returnValue			pointer to the resulting value 
					//! \param owner				pointer to the owner of the operation
					//! \param rightHandSideValue	pointer to the second operand
					virtual void callAddition(void* /*returnValue*/, void* /*owner*/, void* /*rightHandSideValue*/)
					{
						XAIT_FAIL("callAddition only usable with non basic datatypes");
					}

					//! \brief calls the substraction operator
					//! \param returnValue			pointer to the resulting value 
					//! \param owner				pointer to the owner of the operation
					//! \param rightHandSideValue	pointer to the second operand
					virtual void callSubstraction(void* /*returnValue*/, void* /*owner*/, void* /*rightHandSideValue*/)
					{
						XAIT_FAIL("callSubstraction only usable with non basic datatypes");
					}

					//! \brief calls the multiplication operator
					//! \param returnValue			pointer to the resulting value 
					//! \param owner				pointer to the owner of the operation
					//! \param rightHandSideValue	pointer to the second operand
					virtual void callMultiplication(void* /*returnValue*/, void* /*owner*/, void* /*rightHandSideValue*/)
					{
						XAIT_FAIL("callMultiplication only usable with non basic datatypes");
					}

					//! \brief calls the division operator
					//! \param returnValue			pointer to the resulting value 
					//! \param owner				pointer to the owner of the operation
					//! \param rightHandSideValue	pointer to the second operand
					virtual void callDivision(void* /*returnValue*/, void* /*owner*/, void* /*rightHandSideValue*/)
					{
						XAIT_FAIL("callDivision only usable with non basic datatypes");
					}

					//! \brief calls the equal operator
					//! \param returnValue			pointer to the resulting value 
					//! \param owner				pointer to the owner of the operation
					//! \param rightHandSideValue	pointer to the second operand
					virtual void callEqual(void* /*returnValue*/, void* /*owner*/, void* /*rightHandSideValue*/)
					{
						XAIT_FAIL("callEqual only usable with non basic datatypes");
					}

					//! \brief calls the unequal operator
					//! \param returnValue			pointer to the resulting value 
					//! \param owner				pointer to the owner of the operation
					//! \param rightHandSideValue	pointer to the second operand
					virtual void callUnequal(void* /*returnValue*/, void* /*owner*/, void* /*rightHandSideValue*/)
					{
						XAIT_FAIL("callUnequal only usable with non basic datatypes");
					}

					//! \brief calls the greater operator
					//! \param returnValue			pointer to the resulting value 
					//! \param owner				pointer to the owner of the operation
					//! \param rightHandSideValue	pointer to the second operand
					virtual void callGreater(void* /*returnValue*/, void* /*owner*/, void* /*rightHandSideValue*/)
					{
						XAIT_FAIL("callGreater only usable with non basic datatypes");
					}

					//! \brief calls the greaterEqual operator
					//! \param returnValue			pointer to the resulting value 
					//! \param owner				pointer to the owner of the operation
					//! \param rightHandSideValue	pointer to the second operand
					virtual void callGreaterEqual(void* /*returnValue*/, void* /*owner*/, void* /*rightHandSideValue*/)
					{
						XAIT_FAIL("callGreaterEqual only usable with non basic datatypes");
					}

					//! \brief calls the smaller operator
					//! \param returnValue			pointer to the resulting value 
					//! \param owner				pointer to the owner of the operation
					//! \param rightHandSideValue	pointer to the second operand
					virtual void callSmaller(void* /*returnValue*/, void* /*owner*/, void* /*rightHandSideValue*/)
					{
						XAIT_FAIL("callSmaller only usable with non basic datatypes");
					}

					//! \brief calls the smaller equal operator
					//! \param returnValue			pointer to the resulting value 
					//! \param owner				pointer to the owner of the operation
					//! \param rightHandSideValue	pointer to the second operand
					virtual void callSmallerEqual(void* /*returnValue*/, void* /*owner*/, void* /*rightHandSideValue*/)
					{
						XAIT_FAIL("callSmallerEqual only usable with non basic datatypes");
					}

					virtual void callFunction(Function::FunctionID /*id*/,void* /*owner*/,const Function::FunctionArgumentList& /*argList*/)
					{
						XAIT_FAIL("Datatype does not have any functions");
					}

					//! \brief call a function with id and argumentlist
					//! \param Variable* pointer to a value who should store the returnvalue
					//! \param id		id of the function
					//! \param owner	the owner of the function
					//! \param argList  argumentlist of the function
					virtual void callFunction(void* /*returnValue*/,Function::FunctionID /*id*/,void* /*owner*/,const Function::FunctionArgumentList& /*argList*/)
					{
						XAIT_FAIL("Datatype does not have any functions");
					}

					//! \brief serialize the data to a stream
					//! \param value		pointer to the value
					//! \param stream			the storage stream
					virtual void serializeWrite(void* value,Serialize::SerializeStorage* stream) = 0;

					//! \brief inits the object from a serialized stream
					//! \param value			pointer to the value (has to be a valid object)
					//! \param stream			the storage stream
					virtual void serializeRead(void* value,Serialize::SerializeStorage* stream) = 0;

				protected:


				};

				//! \brief base class of all datatypes and represents integral datatypes (int, uint, float,...)
				template<typename T_DATATYPE>
				class TypedDatatype : public DatatypeBase
				{
				public:
					//! \brief constructor
					//! \param allocator		the used memory allocator
					TypedDatatype()
						: mReferences(mAllocator)
					{
						mReferences.pushBack(&(this->mInstance));
					}

					//! \brief destructor
					virtual ~TypedDatatype()
					{
						for (uint32 i=0; i < mReferences.size(); ++i)
						{
							*(mReferences[i]) = NULL;
						}
					}

					//! \brief existance check
					//! \returns true if datatype exists, false otherwise
					static inline bool exists() 
					{
						return (mInstance != NULL);
					}

					

					//! \brief getter datatype instance
					//! \returns a pointer to the datatype
					static TypedDatatype<T_DATATYPE>* getDatatype()
					{
						return mInstance;
					}

				protected:
					static TypedDatatype<T_DATATYPE>*			  mInstance;
					Container::Vector<TypedDatatype<T_DATATYPE>**> mReferences;
				};



				//! \brief helper class to get the datatypeid
				template<typename T_DATATYPE>
				class DatatypeIDHelpClass
				{
				public:
					//! \brief returns the datatypeID of an datatype 
					static DatatypeID getDatatypeID()
					{
#ifndef XAIT_RTTI_DISABLED
						X_ASSERT_MSG(TypedDatatype<T_DATATYPE>::exists(), "requested DataType: \"" << typeid(T_DATATYPE).name() << "\" is not registered");
#else
						X_ASSERT_MSG(TypedDatatype<T_DATATYPE>::exists(), "requested DataType: \"RTTI disabled\" is not registered");
#endif					
						return TypedDatatype<T_DATATYPE>::getDatatype()->getDatatypeID();
					}
				};


				//! \brief template specialization of DatatypeIDHelpClass to handle void as datatype
				//! \remarks	returns an datatypeID with -2 as ID
				template<>
				class DatatypeIDHelpClass<void>
				{
				public:
					static DatatypeID getDatatypeID()
					{
						uint32 temp = X_MAX_UINT32 - 1;
						return *((DatatypeID*)(&temp));
					}
				};





				template<typename T_DATATYPE>
				TypedDatatype<T_DATATYPE>* TypedDatatype<T_DATATYPE>::mInstance = NULL;

			}
		}
	}
}
