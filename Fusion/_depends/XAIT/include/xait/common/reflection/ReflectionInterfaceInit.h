// (C) xaitment GmbH 2006-2012

#pragma once


#include <xait/common/reflection/ReflectionInterface.h>

#define XAIT_INIT_REFLECTION(allocator) \
{ \
	Common::Reflection::Datatype::DatatypeManager::Ptr datatypeManager = Common::Reflection::ReflectionInterface::getDatatypeManager(); \
	if ( datatypeManager == NULL) \
	{ \
	datatypeManager = Common::Reflection::Datatype::DatatypeManager::Ptr(new(allocator) Common::Reflection::Datatype::DatatypeManager()); \
	} \
	Common::Reflection::ReflectionInterface::init(datatypeManager); \
	Common::Reflection::ReflectionInterfaceInitHelper::registerDefaultDatatypes(datatypeManager); \
}

namespace XAIT
{
	namespace Common
	{
		namespace Reflection
		{

			class ReflectionInterfaceInitHelper
			{
			public:
				static void registerDefaultDatatypes(const Datatype::DatatypeManager::Ptr& datatypeManager)
				{
					// test if one of the datatypes already exists
					if (XAIT::Common::Reflection::Datatype::Datatype<bool>::exists())
						return;

					const Memory::AllocatorPtr& allocator = Common::Reflection::ReflectionInterface::getMemoryAllocator();

					// register default datatypes

					// fundamental types
					datatypeManager->registerFundamentalDatatype<bool>	("Boolean",false,Datatype::stringToBool);
					datatypeManager->registerFundamentalDatatype<int08>	("Integer08",0,Datatype::stringToInt08);
					datatypeManager->registerFundamentalDatatype<int16>	("Integer16",0,Datatype::stringToInt16);
					datatypeManager->registerFundamentalDatatype<int32>	("Integer32",0,Datatype::stringToInt32);
					datatypeManager->registerFundamentalDatatype<int64>	("Integer64",0,Datatype::stringToInt64);
					datatypeManager->registerFundamentalDatatype<uint08>("UnsignedInteger08",0,Datatype::stringToUInt08);
					datatypeManager->registerFundamentalDatatype<uint16>("UnsignedInteger16",0,Datatype::stringToUInt16);
					datatypeManager->registerFundamentalDatatype<uint32>("UnsignedInteger32",0,Datatype::stringToUInt32);
					datatypeManager->registerFundamentalDatatype<uint64>("UnsignedInteger64",0,Datatype::stringToUInt64);
					datatypeManager->registerFundamentalDatatype<float32>("Float",0.0f,Datatype::stringToFloat32);
					datatypeManager->registerFundamentalDatatype<float64>("Double",0.0,Datatype::stringToFloat64);

					datatypeManager->registerDatatype<Function::FunctionArgumentList> ("FuncArgList", Function::FunctionArgumentList(Container::Vector<Datatype::DatatypeID>(allocator), allocator), Datatype::stringToFuncArgList);

					// vec2f
					Datatype::Datatype<Math::Vec2f>* datatypeVec2f = datatypeManager->registerDatatype<Math::Vec2f>	("Vec2f",Math::Vec2f(X_MIN_FLOAT32,X_MIN_FLOAT32),Datatype::stringToVec2f,&Math::Vec2f::operator+,&Math::Vec2f::operator-,&Math::Vec2f::operator*,0,
						&Math::Vec2f::operator==, &Math::Vec2f::operator !=);
					float32& (Math::Vec2f::*comp2f)(const uint32) = &Math::Vec2f::operator[];
					datatypeVec2f->registerMemberFunction<Math::Vec2f>("GetComponent", comp2f);
					datatypeVec2f->registerMemberFunction<Math::Vec2f>("SetComponent",&Math::Vec2f::setComponent);
					datatypeVec2f->registerMemberFunction<Math::Vec2f>("GetLength",&Math::Vec2f::getLength);
					datatypeVec2f->registerMemberFunction<Math::Vec2f>("GetDistance",&Math::Vec2f::getDistance);
					datatypeVec2f->registerMemberFunction<Math::Vec2f>("Normalize",&Math::Vec2f::normalize);
					datatypeVec2f->registerMemberFunction<Math::Vec2f>("DotProduct",&Math::Vec2f::dotProduct);

					// vec2d
					Datatype::Datatype<Math::Vec2d>* datatypeVec2d = datatypeManager->registerDatatype<Math::Vec2d>	("Vec2d",Math::Vec2d(X_MIN_FLOAT64,X_MIN_FLOAT64),Datatype::stringToVec2d,&Math::Vec2d::operator+,&Math::Vec2d::operator-,0,0,
						&Math::Vec2d::operator==, &Math::Vec2d::operator !=);
					float64& (Math::Vec2d::*comp2d)(const uint32) = &Math::Vec2d::operator[];
					datatypeVec2d->registerMemberFunction<Math::Vec2d>("GetComponent", comp2d);
					datatypeVec2d->registerMemberFunction<Math::Vec2d>("SetComponent",&Math::Vec2d::setComponent);
					datatypeVec2d->registerMemberFunction<Math::Vec2d>("GetLength",&Math::Vec2d::getLength);
					datatypeVec2d->registerMemberFunction<Math::Vec2d>("GetDistance",&Math::Vec2d::getDistance);
					// vec2i32
					Datatype::Datatype<Math::Vec2i32>* datatypeVec2i32 = datatypeManager->registerDatatype<Math::Vec2i32>	("Vec2i32",Math::Vec2i32(X_MIN_INT32,X_MIN_INT32),Datatype::stringToVec2i32,&Math::Vec2i32::operator+,&Math::Vec2i32::operator-,0,0,
						&Math::Vec2i32::operator==, &Math::Vec2i32::operator !=);
					int32& (Math::Vec2i32::*comp2i32)(const uint32) = &Math::Vec2i32::operator[];
					datatypeVec2i32->registerMemberFunction<Math::Vec2i32>("GetComponent", comp2i32);
					datatypeVec2i32->registerMemberFunction<Math::Vec2i32>("SetComponent",&Math::Vec2i32::setComponent);
					datatypeVec2i32->registerMemberFunction<Math::Vec2i32>("GetLength",&Math::Vec2i32::getLength);
					datatypeVec2i32->registerMemberFunction<Math::Vec2i32>("GetDistance",&Math::Vec2i32::getDistance);
					// vec2u32
					Datatype::Datatype<Math::Vec2u32>* datatypeVec2u32 = datatypeManager->registerDatatype<Math::Vec2u32>	("Vec2u32",Math::Vec2u32(X_MIN_UINT32,X_MIN_UINT32),Datatype::stringToVec2u32,&Math::Vec2u32::operator+,&Math::Vec2u32::operator-,0,0,
						&Math::Vec2u32::operator==, &Math::Vec2u32::operator !=);
					uint32& (Math::Vec2u32::*comp2u32)(const uint32) = &Math::Vec2u32::operator[];
					datatypeVec2u32->registerMemberFunction<Math::Vec2u32>("GetComponent", comp2u32);
					datatypeVec2u32->registerMemberFunction<Math::Vec2u32>("SetComponent",&Math::Vec2u32::setComponent);
					datatypeVec2u32->registerMemberFunction<Math::Vec2u32>("GetLength",&Math::Vec2u32::getLength);
					datatypeVec2u32->registerMemberFunction<Math::Vec2u32>("GetDistance",&Math::Vec2u32::getDistance);
					// vec3f
					Datatype::Datatype<Math::Vec3f>* datatypeVec3f = datatypeManager->registerDatatype<Math::Vec3f>("Vec3f",Math::Vec3f(X_MIN_FLOAT32,X_MIN_FLOAT32,X_MIN_FLOAT32),Datatype::stringToVec3f,&Math::Vec3f::operator+,&Math::Vec3f::operator-,&Math::Vec3f::operator*,0,
						&Math::Vec3f::operator==,&Math::Vec3f::operator!= );

					float32& (Math::Vec3f::*comp3f)(const uint32) = &Math::Vec3f::operator[];
					datatypeVec3f->registerMemberFunction<Math::Vec3f>("GetComponent", comp3f);
					datatypeVec3f->registerMemberFunction<Math::Vec3f>("SetComponent",&Math::Vec3f::setComponent);
					datatypeVec3f->registerMemberFunction<Math::Vec3f>("GetLength",&Math::Vec3f::getLength);
					datatypeVec3f->registerMemberFunction<Math::Vec3f>("GetDistance",&Math::Vec3f::getDistance);
					datatypeVec3f->registerMemberFunction<Math::Vec3f>("Normalize",&Math::Vec3f::normalize);
					datatypeVec3f->registerMemberFunction<Math::Vec3f>("DotProduct",&Math::Vec3f::dotProduct);
					datatypeVec3f->registerMemberFunction<Math::Vec3f>("CrossProduct",&Math::Vec3f::crossProduct);

					// vec3d
					Datatype::Datatype<Math::Vec3d>* datatypeVec3d = datatypeManager->registerDatatype<Math::Vec3d>("Vec3d",Math::Vec3d(X_MIN_FLOAT64,X_MIN_FLOAT64,X_MIN_FLOAT64),Datatype::stringToVec3d,&Math::Vec3d::operator+,&Math::Vec3d::operator-,0,0,
						&Math::Vec3d::operator==,&Math::Vec3d::operator!= );

					float64& (Math::Vec3d::*comp3d)(const uint32) = &Math::Vec3d::operator[];
					datatypeVec3d->registerMemberFunction<Math::Vec3d>("GetComponent", comp3d);
					datatypeVec3d->registerMemberFunction<Math::Vec3d>("SetComponent",&Math::Vec3d::setComponent);
					datatypeVec3d->registerMemberFunction<Math::Vec3d>("GetLength",&Math::Vec3d::getLength);
					datatypeVec3d->registerMemberFunction<Math::Vec3d>("GetDistance",&Math::Vec3d::getDistance);
					// vec3i32
					Datatype::Datatype<Math::Vec3i32>* datatypeVec3i32 = datatypeManager->registerDatatype<Math::Vec3i32>("Vec3i32",Math::Vec3i32(X_MIN_INT32,X_MIN_INT32,X_MIN_INT32),Datatype::stringToVec3i32,&Math::Vec3i32::operator+,&Math::Vec3i32::operator-,0,0,
						&Math::Vec3i32::operator==,&Math::Vec3i32::operator!= );

					int32& (Math::Vec3i32::*comp3i32)(const uint32) = &Math::Vec3i32::operator[];
					datatypeVec3i32->registerMemberFunction<Math::Vec3i32>("GetComponent", comp3i32);
					datatypeVec3i32->registerMemberFunction<Math::Vec3i32>("SetComponent",&Math::Vec3i32::setComponent);
					datatypeVec3i32->registerMemberFunction<Math::Vec3i32>("GetLength",&Math::Vec3i32::getLength);
					datatypeVec3i32->registerMemberFunction<Math::Vec3i32>("GetDistance",&Math::Vec3i32::getDistance);
					// vec3u32
					Datatype::Datatype<Math::Vec3u32>* datatypeVec3u32 = datatypeManager->registerDatatype<Math::Vec3u32>("Vec3u32",Math::Vec3u32(X_MIN_UINT32,X_MIN_UINT32,X_MIN_UINT32),Datatype::stringToVec3u32,&Math::Vec3u32::operator+,&Math::Vec3u32::operator-,0,0,
						&Math::Vec3u32::operator==,&Math::Vec3u32::operator!= );

					uint32& (Math::Vec3u32::*comp3u32)(const uint32) = &Math::Vec3u32::operator[];
					datatypeVec3u32->registerMemberFunction<Math::Vec3u32>("GetComponent", comp3u32);
					datatypeVec3u32->registerMemberFunction<Math::Vec3u32>("SetComponent",&Math::Vec3u32::setComponent);
					datatypeVec3u32->registerMemberFunction<Math::Vec3u32>("GetLength",&Math::Vec3u32::getLength);
					datatypeVec3u32->registerMemberFunction<Math::Vec3u32>("GetDistance",&Math::Vec3u32::getDistance);

					// string
					Datatype::Datatype<String>* datatypeString = datatypeManager->registerDatatype<String>			("String",String(),Datatype::stringToString,&String::operator+,NULL,NULL,NULL,&String::operator==,&String::operator!=);
					datatypeString->registerMemberFunction<String>("GetLength",&String::getLength);
					Datatype::Datatype<WString>* datatypeWString = datatypeManager->registerDatatype<WString>		("WString",WString(),Datatype::stringToWString,&WString::operator+,NULL,NULL,NULL,&WString::operator==,&WString::operator!=);
					datatypeWString->registerMemberFunction<WString>("GetLength",&WString::getLength);

					// vector types
					Datatype::Datatype<Container::Vector<bool> >*		vecBool	  =	datatypeManager->registerDatatype<Container::Vector<bool> >	("Container[Boolean]",Container::Vector<bool>(allocator),Datatype::stringToVectorBool,NULL,NULL,NULL,NULL,&Container::Vector<bool>::operator==,&Container::Vector<bool>::operator!=);
					Datatype::Datatype<Container::Vector<int08> >*		vecInt08	  =	datatypeManager->registerDatatype<Container::Vector<int08> >	("Container[Integer08]",Container::Vector<int08>(allocator),Datatype::stringToVectorInt08,NULL,NULL,NULL,NULL,&Container::Vector<int08>::operator==,&Container::Vector<int08>::operator!=);
					Datatype::Datatype<Container::Vector<int16> >*		vecInt16	  =	datatypeManager->registerDatatype<Container::Vector<int16> >	("Container[Integer16]",Container::Vector<int16>(allocator),Datatype::stringToVectorInt16,NULL,NULL,NULL,NULL,&Container::Vector<int16>::operator==,&Container::Vector<int16>::operator!=);
					Datatype::Datatype<Container::Vector<int32> >*		vecInt32	  =	datatypeManager->registerDatatype<Container::Vector<int32> >	("Container[Integer32]",Container::Vector<int32>(allocator),Datatype::stringToVectorInt32,NULL,NULL,NULL,NULL,&Container::Vector<int32>::operator==,&Container::Vector<int32>::operator!=);
					Datatype::Datatype<Container::Vector<int64> >*		vecInt64	  =	datatypeManager->registerDatatype<Container::Vector<int64> >	("Container[Integer64]",Container::Vector<int64>(allocator),Datatype::stringToVectorInt64,NULL,NULL,NULL,NULL,&Container::Vector<int64>::operator==,&Container::Vector<int64>::operator!=);
					Datatype::Datatype<Container::Vector<uint08> >*		vecUInt08	  =	datatypeManager->registerDatatype<Container::Vector<uint08> >	("Container[UnsignedInteger08]",Container::Vector<uint08>(allocator),Datatype::stringToVectorUInt08,NULL,NULL,NULL,NULL,&Container::Vector<uint08>::operator==,&Container::Vector<uint08>::operator!=);
					Datatype::Datatype<Container::Vector<uint16> >*		vecUInt16	  =	datatypeManager->registerDatatype<Container::Vector<uint16> >	("Container[UnsignedInteger16]",Container::Vector<uint16>(allocator),Datatype::stringToVectorUInt16,NULL,NULL,NULL,NULL,&Container::Vector<uint16>::operator==,&Container::Vector<uint16>::operator!=);
					Datatype::Datatype<Container::Vector<uint32> >*		vecUInt32	  =	datatypeManager->registerDatatype<Container::Vector<uint32> >	("Container[UnsignedInteger32]",Container::Vector<uint32>(allocator),Datatype::stringToVectorUInt32,NULL,NULL,NULL,NULL,&Container::Vector<uint32>::operator==,&Container::Vector<uint32>::operator!=);
					Datatype::Datatype<Container::Vector<uint64> >*		vecUInt64	  =	datatypeManager->registerDatatype<Container::Vector<uint64> >	("Container[UnsignedInteger64]",Container::Vector<uint64>(allocator),Datatype::stringToVectorUInt64,NULL,NULL,NULL,NULL,&Container::Vector<uint64>::operator==,&Container::Vector<uint64>::operator!=);
					Datatype::Datatype<Container::Vector<float32> >*	vecFloat  =	datatypeManager->registerDatatype<Container::Vector<float32> >	("Container[Float]",Container::Vector<float32>(allocator),Datatype::stringToVectorFloat32,NULL,NULL,NULL,NULL,&Container::Vector<float32>::operator==,&Container::Vector<float32>::operator!=);
					Datatype::Datatype<Container::Vector<float64> >*	vecDouble  =	datatypeManager->registerDatatype<Container::Vector<float64> >	("Container[Double]",Container::Vector<float64>(allocator),Datatype::stringToVectorFloat64,NULL,NULL,NULL,NULL,&Container::Vector<float64>::operator==,&Container::Vector<float64>::operator!=);
					Datatype::Datatype<Container::Vector<Math::Vec2f> >*		vecVec2f  =	datatypeManager->registerDatatype<Container::Vector<Math::Vec2f> >	("Container[Vec2f]",Container::Vector<Math::Vec2f>(allocator),Datatype::stringToVectorVec2f,NULL,NULL,NULL,NULL,&Container::Vector<Math::Vec2f>::operator==,&Container::Vector<Math::Vec2f>::operator!=);
					Datatype::Datatype<Container::Vector<Math::Vec2d> >*		vecVec2d  =	datatypeManager->registerDatatype<Container::Vector<Math::Vec2d> >	("Container[Vec2d]",Container::Vector<Math::Vec2d>(allocator),Datatype::stringToVectorVec2d,NULL,NULL,NULL,NULL,&Container::Vector<Math::Vec2d>::operator==,&Container::Vector<Math::Vec2d>::operator!=);
					Datatype::Datatype<Container::Vector<Math::Vec2i32> >*		vecVec2i32  =	datatypeManager->registerDatatype<Container::Vector<Math::Vec2i32> >	("Container[Vec2i32]",Container::Vector<Math::Vec2i32>(allocator),Datatype::stringToVectorVec2i32,NULL,NULL,NULL,NULL,&Container::Vector<Math::Vec2i32>::operator==,&Container::Vector<Math::Vec2i32>::operator!=);
					Datatype::Datatype<Container::Vector<Math::Vec2u32> >*		vecVec2u32  =	datatypeManager->registerDatatype<Container::Vector<Math::Vec2u32> >	("Container[Vec2u32]",Container::Vector<Math::Vec2u32>(allocator),Datatype::stringToVectorVec2u32,NULL,NULL,NULL,NULL,&Container::Vector<Math::Vec2u32>::operator==,&Container::Vector<Math::Vec2u32>::operator!=);
					Datatype::Datatype<Container::Vector<Math::Vec3f> >*		vecVec3f  =	datatypeManager->registerDatatype<Container::Vector<Math::Vec3f> >	("Container[Vec3f]",Container::Vector<Math::Vec3f>(allocator),Datatype::stringToVectorVec3f,NULL,NULL,NULL,NULL,&Container::Vector<Math::Vec3f>::operator==,&Container::Vector<Math::Vec3f>::operator!=);
					Datatype::Datatype<Container::Vector<Math::Vec3d> >*		vecVec3d  =	datatypeManager->registerDatatype<Container::Vector<Math::Vec3d> >	("Container[Vec3d]",Container::Vector<Math::Vec3d>(allocator),Datatype::stringToVectorVec3d,NULL,NULL,NULL,NULL,&Container::Vector<Math::Vec3d>::operator==,&Container::Vector<Math::Vec3d>::operator!=);
					Datatype::Datatype<Container::Vector<Math::Vec3i32> >*		vecVec3i32  =	datatypeManager->registerDatatype<Container::Vector<Math::Vec3i32> >	("Container[Vec3i32]",Container::Vector<Math::Vec3i32>(allocator),Datatype::stringToVectorVec3i32,NULL,NULL,NULL,NULL,&Container::Vector<Math::Vec3i32>::operator==,&Container::Vector<Math::Vec3i32>::operator!=);
					Datatype::Datatype<Container::Vector<Math::Vec3u32> >*		vecVec3u32  =	datatypeManager->registerDatatype<Container::Vector<Math::Vec3u32> >	("Container[Vec3u32]",Container::Vector<Math::Vec3u32>(allocator),Datatype::stringToVectorVec3u32,NULL,NULL,NULL,NULL,&Container::Vector<Math::Vec3u32>::operator==,&Container::Vector<Math::Vec3u32>::operator!=);
					Datatype::Datatype<Container::Vector<String> >*	vecString =	datatypeManager->registerDatatype<Container::Vector<String> >	("Container[String]",Container::Vector<String>(allocator),Datatype::stringToVectorString,NULL,NULL,NULL,NULL,&Container::Vector<String>::operator==,&Container::Vector<String>::operator!=);
					Datatype::Datatype<Container::Vector<WString> >*	vecWString =	datatypeManager->registerDatatype<Container::Vector<WString> >	("Container[WString]",Container::Vector<WString>(allocator),0,NULL,NULL,NULL,NULL,&Container::Vector<WString>::operator==,&Container::Vector<WString>::operator!=);


					vecBool->registerMemberFunction<Container::Vector<bool> >("Clear",&Container::Vector<bool>::clear);
					vecInt08->registerMemberFunction<Container::Vector<int08> >("Clear",&Container::Vector<int08>::clear);
					vecInt16->registerMemberFunction<Container::Vector<int16> >("Clear",&Container::Vector<int16>::clear);
					vecInt32->registerMemberFunction<Container::Vector<int32> >("Clear",&Container::Vector<int32>::clear);
					vecInt64->registerMemberFunction<Container::Vector<int64> >("Clear",&Container::Vector<int64>::clear);
					vecUInt08->registerMemberFunction<Container::Vector<uint08> >("Clear",&Container::Vector<uint08>::clear);
					vecUInt16->registerMemberFunction<Container::Vector<uint16> >("Clear",&Container::Vector<uint16>::clear);
					vecUInt32->registerMemberFunction<Container::Vector<uint32> >("Clear",&Container::Vector<uint32>::clear);
					vecUInt64->registerMemberFunction<Container::Vector<uint64> >("Clear",&Container::Vector<uint64>::clear);
					vecFloat->registerMemberFunction<Container::Vector<float32> >("Clear",&Container::Vector<float32>::clear);
					vecDouble->registerMemberFunction<Container::Vector<float64> >("Clear",&Container::Vector<float64>::clear);
					vecVec2f->registerMemberFunction<Container::Vector<Math::Vec2f> >("Clear",&Container::Vector<Math::Vec2f>::clear);
					vecVec2d->registerMemberFunction<Container::Vector<Math::Vec2d> >("Clear",&Container::Vector<Math::Vec2d>::clear);
					vecVec2i32->registerMemberFunction<Container::Vector<Math::Vec2i32> >("Clear",&Container::Vector<Math::Vec2i32>::clear);
					vecVec2u32->registerMemberFunction<Container::Vector<Math::Vec2u32> >("Clear",&Container::Vector<Math::Vec2u32>::clear);
					vecVec3f->registerMemberFunction<Container::Vector<Math::Vec3f> >("Clear",&Container::Vector<Math::Vec3f>::clear);
					vecVec3d->registerMemberFunction<Container::Vector<Math::Vec3d> >("Clear",&Container::Vector<Math::Vec3d>::clear);
					vecVec3i32->registerMemberFunction<Container::Vector<Math::Vec3i32> >("Clear",&Container::Vector<Math::Vec3i32>::clear);
					vecVec3u32->registerMemberFunction<Container::Vector<Math::Vec3u32> >("Clear",&Container::Vector<Math::Vec3u32>::clear);
					vecString->registerMemberFunction<Container::Vector<String> >("Clear",&Container::Vector<String>::clear);
					vecWString->registerMemberFunction<Container::Vector<WString> >("Clear",&Container::Vector<WString>::clear);

					vecBool->registerMemberFunction<Container::Vector<bool> >("PushBack",&Container::Vector<bool>::pushBack);
					vecInt08->registerMemberFunction<Container::Vector<int08> >("PushBack",&Container::Vector<int08>::pushBack);
					vecInt16->registerMemberFunction<Container::Vector<int16> >("PushBack",&Container::Vector<int16>::pushBack);
					vecInt32->registerMemberFunction<Container::Vector<int32> >("PushBack",&Container::Vector<int32>::pushBack);
					vecInt64->registerMemberFunction<Container::Vector<int64> >("PushBack",&Container::Vector<int64>::pushBack);
					vecUInt08->registerMemberFunction<Container::Vector<uint08> >("PushBack",&Container::Vector<uint08>::pushBack);
					vecUInt16->registerMemberFunction<Container::Vector<uint16> >("PushBack",&Container::Vector<uint16>::pushBack);
					vecUInt32->registerMemberFunction<Container::Vector<uint32> >("PushBack",&Container::Vector<uint32>::pushBack);
					vecUInt64->registerMemberFunction<Container::Vector<uint64> >("PushBack",&Container::Vector<uint64>::pushBack);
					vecFloat->registerMemberFunction<Container::Vector<float32> >("PushBack",&Container::Vector<float32>::pushBack);
					vecDouble->registerMemberFunction<Container::Vector<float64> >("PushBack",&Container::Vector<float64>::pushBack);
					vecVec2f->registerMemberFunction<Container::Vector<Math::Vec2f> >("PushBack",&Container::Vector<Math::Vec2f>::pushBack);
					vecVec2d->registerMemberFunction<Container::Vector<Math::Vec2d> >("PushBack",&Container::Vector<Math::Vec2d>::pushBack);
					vecVec2i32->registerMemberFunction<Container::Vector<Math::Vec2i32> >("PushBack",&Container::Vector<Math::Vec2i32>::pushBack);
					vecVec2u32->registerMemberFunction<Container::Vector<Math::Vec2u32> >("PushBack",&Container::Vector<Math::Vec2u32>::pushBack);
					vecVec3f->registerMemberFunction<Container::Vector<Math::Vec3f> >("PushBack",&Container::Vector<Math::Vec3f>::pushBack);
					vecVec3d->registerMemberFunction<Container::Vector<Math::Vec3d> >("PushBack",&Container::Vector<Math::Vec3d>::pushBack);
					vecVec3i32->registerMemberFunction<Container::Vector<Math::Vec3i32> >("PushBack",&Container::Vector<Math::Vec3i32>::pushBack);
					vecVec3u32->registerMemberFunction<Container::Vector<Math::Vec3u32> >("PushBack",&Container::Vector<Math::Vec3u32>::pushBack);
					vecString->registerMemberFunction<Container::Vector<String> >("PushBack",&Container::Vector<String>::pushBack);
					vecWString->registerMemberFunction<Container::Vector<WString> >("PushBack",&Container::Vector<WString>::pushBack);

					vecBool->registerMemberFunction<Container::Vector<bool> >("Size",&Container::Vector<bool>::size);
					vecInt08->registerMemberFunction<Container::Vector<int08> >("Size",&Container::Vector<int08>::size);
					vecInt16->registerMemberFunction<Container::Vector<int16> >("Size",&Container::Vector<int16>::size);
					vecInt32->registerMemberFunction<Container::Vector<int32> >("Size",&Container::Vector<int32>::size);
					vecInt64->registerMemberFunction<Container::Vector<int64> >("Size",&Container::Vector<int64>::size);
					vecUInt08->registerMemberFunction<Container::Vector<uint08> >("Size",&Container::Vector<uint08>::size);
					vecUInt16->registerMemberFunction<Container::Vector<uint16> >("Size",&Container::Vector<uint16>::size);
					vecUInt32->registerMemberFunction<Container::Vector<uint32> >("Size",&Container::Vector<uint32>::size);
					vecUInt64->registerMemberFunction<Container::Vector<uint64> >("Size",&Container::Vector<uint64>::size);
					vecFloat->registerMemberFunction<Container::Vector<float32> >("Size",&Container::Vector<float32>::size);
					vecDouble->registerMemberFunction<Container::Vector<float64> >("Size",&Container::Vector<float64>::size);
					vecVec2f->registerMemberFunction<Container::Vector<Math::Vec2f> >("Size",&Container::Vector<Math::Vec2f>::size);
					vecVec2d->registerMemberFunction<Container::Vector<Math::Vec2d> >("Size",&Container::Vector<Math::Vec2d>::size);
					vecVec2i32->registerMemberFunction<Container::Vector<Math::Vec2i32> >("Size",&Container::Vector<Math::Vec2i32>::size);
					vecVec2u32->registerMemberFunction<Container::Vector<Math::Vec2u32> >("Size",&Container::Vector<Math::Vec2u32>::size);
					vecVec3f->registerMemberFunction<Container::Vector<Math::Vec3f> >("Size",&Container::Vector<Math::Vec3f>::size);
					vecVec3d->registerMemberFunction<Container::Vector<Math::Vec3d> >("Size",&Container::Vector<Math::Vec3d>::size);
					vecVec3i32->registerMemberFunction<Container::Vector<Math::Vec3i32> >("Size",&Container::Vector<Math::Vec3i32>::size);
					vecVec3u32->registerMemberFunction<Container::Vector<Math::Vec3u32> >("Size",&Container::Vector<Math::Vec3u32>::size);
					vecString->registerMemberFunction<Container::Vector<String> >("Size",&Container::Vector<String>::size);
					vecWString->registerMemberFunction<Container::Vector<WString> >("Size",&Container::Vector<WString>::size);

					vecBool->registerMemberFunction<Container::Vector<bool> >("GetElement",&Container::Vector<bool>::getElement);
					vecInt08->registerMemberFunction<Container::Vector<int08> >("GetElement",&Container::Vector<int08>::getElement);
					vecInt16->registerMemberFunction<Container::Vector<int16> >("GetElement",&Container::Vector<int16>::getElement);
					vecInt32->registerMemberFunction<Container::Vector<int32> >("GetElement",&Container::Vector<int32>::getElement);
					vecInt64->registerMemberFunction<Container::Vector<int64> >("GetElement",&Container::Vector<int64>::getElement);
					vecUInt08->registerMemberFunction<Container::Vector<uint08> >("GetElement",&Container::Vector<uint08>::getElement);
					vecUInt16->registerMemberFunction<Container::Vector<uint16> >("GetElement",&Container::Vector<uint16>::getElement);
					vecUInt32->registerMemberFunction<Container::Vector<uint32> >("GetElement",&Container::Vector<uint32>::getElement);
					vecUInt64->registerMemberFunction<Container::Vector<uint64> >("GetElement",&Container::Vector<uint64>::getElement);
					vecFloat->registerMemberFunction<Container::Vector<float32> >("GetElement",&Container::Vector<float32>::getElement);
					vecDouble->registerMemberFunction<Container::Vector<float64> >("GetElement",&Container::Vector<float64>::getElement);
					vecVec2f->registerMemberFunction<Container::Vector<Math::Vec2f> >("GetElement",&Container::Vector<Math::Vec2f>::getElement);
					vecVec2d->registerMemberFunction<Container::Vector<Math::Vec2d> >("GetElement",&Container::Vector<Math::Vec2d>::getElement);
					vecVec2i32->registerMemberFunction<Container::Vector<Math::Vec2i32> >("GetElement",&Container::Vector<Math::Vec2i32>::getElement);
					vecVec2u32->registerMemberFunction<Container::Vector<Math::Vec2u32> >("GetElement",&Container::Vector<Math::Vec2u32>::getElement);
					vecVec3f->registerMemberFunction<Container::Vector<Math::Vec3f> >("GetElement",&Container::Vector<Math::Vec3f>::getElement);
					vecVec3d->registerMemberFunction<Container::Vector<Math::Vec3d> >("GetElement",&Container::Vector<Math::Vec3d>::getElement);
					vecVec3i32->registerMemberFunction<Container::Vector<Math::Vec3i32> >("GetElement",&Container::Vector<Math::Vec3i32>::getElement);
					vecVec3u32->registerMemberFunction<Container::Vector<Math::Vec3u32> >("GetElement",&Container::Vector<Math::Vec3u32>::getElement);
					vecString->registerMemberFunction<Container::Vector<String> >("GetElement",&Container::Vector<String>::getElement);
					vecWString->registerMemberFunction<Container::Vector<WString> >("GetElement",&Container::Vector<WString>::getElement);

					vecBool->registerMemberFunction<Container::Vector<bool> >("SetElement",&Container::Vector<bool>::setElement);
					vecInt08->registerMemberFunction<Container::Vector<int08> >("SetElement",&Container::Vector<int08>::setElement);
					vecInt16->registerMemberFunction<Container::Vector<int16> >("SetElement",&Container::Vector<int16>::setElement);
					vecInt32->registerMemberFunction<Container::Vector<int32> >("SetElement",&Container::Vector<int32>::setElement);
					vecInt64->registerMemberFunction<Container::Vector<int64> >("SetElement",&Container::Vector<int64>::setElement);
					vecUInt08->registerMemberFunction<Container::Vector<uint08> >("SetElement",&Container::Vector<uint08>::setElement);
					vecUInt16->registerMemberFunction<Container::Vector<uint16> >("SetElement",&Container::Vector<uint16>::setElement);
					vecUInt32->registerMemberFunction<Container::Vector<uint32> >("SetElement",&Container::Vector<uint32>::setElement);
					vecUInt64->registerMemberFunction<Container::Vector<uint64> >("SetElement",&Container::Vector<uint64>::setElement);
					vecFloat->registerMemberFunction<Container::Vector<float32> >("SetElement",&Container::Vector<float32>::setElement);
					vecDouble->registerMemberFunction<Container::Vector<float64> >("SetElement",&Container::Vector<float64>::setElement);
					vecVec2f->registerMemberFunction<Container::Vector<Math::Vec2f> >("SetElement",&Container::Vector<Math::Vec2f>::setElement);
					vecVec2d->registerMemberFunction<Container::Vector<Math::Vec2d> >("SetElement",&Container::Vector<Math::Vec2d>::setElement);
					vecVec2i32->registerMemberFunction<Container::Vector<Math::Vec2i32> >("SetElement",&Container::Vector<Math::Vec2i32>::setElement);
					vecVec2u32->registerMemberFunction<Container::Vector<Math::Vec2u32> >("SetElement",&Container::Vector<Math::Vec2u32>::setElement);
					vecVec3f->registerMemberFunction<Container::Vector<Math::Vec3f> >("SetElement",&Container::Vector<Math::Vec3f>::setElement);
					vecVec3d->registerMemberFunction<Container::Vector<Math::Vec3d> >("SetElement",&Container::Vector<Math::Vec3d>::setElement);
					vecVec3i32->registerMemberFunction<Container::Vector<Math::Vec3i32> >("SetElement",&Container::Vector<Math::Vec3i32>::setElement);
					vecVec3u32->registerMemberFunction<Container::Vector<Math::Vec3u32> >("SetElement",&Container::Vector<Math::Vec3u32>::setElement);
					vecString->registerMemberFunction<Container::Vector<String> >("SetElement",&Container::Vector<String>::setElement);
					vecWString->registerMemberFunction<Container::Vector<WString> >("SetElement",&Container::Vector<WString>::setElement);

					vecBool->registerMemberFunction<Container::Vector<bool> >("RemoveElement",((void (Container::Vector<bool>::*)(const int32))&Container::Vector<bool>::erase));
					vecInt08->registerMemberFunction<Container::Vector<int08> >("RemoveElement",((void (Container::Vector<int08>::*)(const int32))&Container::Vector<int08>::erase));
					vecInt16->registerMemberFunction<Container::Vector<int16> >("RemoveElement",((void (Container::Vector<int16>::*)(const int32))&Container::Vector<int16>::erase));
					vecInt32->registerMemberFunction<Container::Vector<int32> >("RemoveElement",((void (Container::Vector<int32>::*)(const int32))&Container::Vector<int32>::erase));
					vecInt64->registerMemberFunction<Container::Vector<int64> >("RemoveElement",((void (Container::Vector<int64>::*)(const int32))&Container::Vector<int64>::erase));
					vecUInt08->registerMemberFunction<Container::Vector<uint08> >("RemoveElement",((void (Container::Vector<uint08>::*)(const int32))&Container::Vector<uint08>::erase));
					vecUInt16->registerMemberFunction<Container::Vector<uint16> >("RemoveElement",((void (Container::Vector<uint16>::*)(const int32))&Container::Vector<uint16>::erase));
					vecUInt32->registerMemberFunction<Container::Vector<uint32> >("RemoveElement",((void (Container::Vector<uint32>::*)(const int32))&Container::Vector<uint32>::erase));
					vecUInt64->registerMemberFunction<Container::Vector<uint64> >("RemoveElement",((void (Container::Vector<uint64>::*)(const int32))&Container::Vector<uint64>::erase));
					vecFloat->registerMemberFunction<Container::Vector<float32> >("RemoveElement",((void (Container::Vector<float32>::*)(const int32))&Container::Vector<float32>::erase));
					vecDouble->registerMemberFunction<Container::Vector<float64> >("RemoveElement",((void (Container::Vector<float64>::*)(const int32))&Container::Vector<float64>::erase));
					vecVec2f->registerMemberFunction<Container::Vector<Math::Vec2f> >("RemoveElement",((void (Container::Vector<Math::Vec2f>::*)(const int32))&Container::Vector<Math::Vec2f>::erase));
					vecVec2d->registerMemberFunction<Container::Vector<Math::Vec2d> >("RemoveElement",((void (Container::Vector<Math::Vec2d>::*)(const int32))&Container::Vector<Math::Vec2d>::erase));
					vecVec2i32->registerMemberFunction<Container::Vector<Math::Vec2i32> >("RemoveElement",((void (Container::Vector<Math::Vec2i32>::*)(const int32))&Container::Vector<Math::Vec2i32>::erase));
					vecVec2u32->registerMemberFunction<Container::Vector<Math::Vec2u32> >("RemoveElement",((void (Container::Vector<Math::Vec2u32>::*)(const int32))&Container::Vector<Math::Vec2u32>::erase));
					vecVec3f->registerMemberFunction<Container::Vector<Math::Vec3f> >("RemoveElement",((void (Container::Vector<Math::Vec3f>::*)(const int32))&Container::Vector<Math::Vec3f>::erase));
					vecVec3d->registerMemberFunction<Container::Vector<Math::Vec3d> >("RemoveElement",((void (Container::Vector<Math::Vec3d>::*)(const int32))&Container::Vector<Math::Vec3d>::erase));
					vecVec3i32->registerMemberFunction<Container::Vector<Math::Vec3i32> >("RemoveElement",((void (Container::Vector<Math::Vec3i32>::*)(const int32))&Container::Vector<Math::Vec3i32>::erase));
					vecVec3u32->registerMemberFunction<Container::Vector<Math::Vec3u32> >("RemoveElement",((void (Container::Vector<Math::Vec3u32>::*)(const int32))&Container::Vector<Math::Vec3u32>::erase));
					vecString->registerMemberFunction<Container::Vector<String> >("RemoveElement",((void (Container::Vector<String>::*)(const int32))&Container::Vector<String>::erase));
					vecWString->registerMemberFunction<Container::Vector<WString> >("RemoveElement",((void (Container::Vector<WString>::*)(const int32))&Container::Vector<WString>::erase));

					vecBool->registerMemberFunction<Container::Vector<bool> >("PopBack",&Container::Vector<bool>::popBack);
					vecInt08->registerMemberFunction<Container::Vector<int08> >("PopBack",&Container::Vector<int08>::popBack);
					vecInt16->registerMemberFunction<Container::Vector<int16> >("PopBack",&Container::Vector<int16>::popBack);
					vecInt32->registerMemberFunction<Container::Vector<int32> >("PopBack",&Container::Vector<int32>::popBack);
					vecInt64->registerMemberFunction<Container::Vector<int64> >("PopBack",&Container::Vector<int64>::popBack);
					vecUInt08->registerMemberFunction<Container::Vector<uint08> >("PopBack",&Container::Vector<uint08>::popBack);
					vecUInt16->registerMemberFunction<Container::Vector<uint16> >("PopBack",&Container::Vector<uint16>::popBack);
					vecUInt32->registerMemberFunction<Container::Vector<uint32> >("PopBack",&Container::Vector<uint32>::popBack);
					vecUInt64->registerMemberFunction<Container::Vector<uint64> >("PopBack",&Container::Vector<uint64>::popBack);
					vecFloat->registerMemberFunction<Container::Vector<float32> >("PopBack",&Container::Vector<float32>::popBack);
					vecDouble->registerMemberFunction<Container::Vector<float64> >("PopBack",&Container::Vector<float64>::popBack);
					vecVec2f->registerMemberFunction<Container::Vector<Math::Vec2f> >("PopBack",&Container::Vector<Math::Vec2f>::popBack);
					vecVec2d->registerMemberFunction<Container::Vector<Math::Vec2d> >("PopBack",&Container::Vector<Math::Vec2d>::popBack);
					vecVec2i32->registerMemberFunction<Container::Vector<Math::Vec2i32> >("PopBack",&Container::Vector<Math::Vec2i32>::popBack);
					vecVec2u32->registerMemberFunction<Container::Vector<Math::Vec2u32> >("PopBack",&Container::Vector<Math::Vec2u32>::popBack);
					vecVec3f->registerMemberFunction<Container::Vector<Math::Vec3f> >("PopBack",&Container::Vector<Math::Vec3f>::popBack);
					vecVec3d->registerMemberFunction<Container::Vector<Math::Vec3d> >("PopBack",&Container::Vector<Math::Vec3d>::popBack);
					vecVec3i32->registerMemberFunction<Container::Vector<Math::Vec3i32> >("PopBack",&Container::Vector<Math::Vec3i32>::popBack);
					vecVec3u32->registerMemberFunction<Container::Vector<Math::Vec3u32> >("PopBack",&Container::Vector<Math::Vec3u32>::popBack);
					vecString->registerMemberFunction<Container::Vector<String> >("PopBack",&Container::Vector<String>::popBack);
					vecWString->registerMemberFunction<Container::Vector<WString> >("PopBack",&Container::Vector<WString>::popBack);

					vecBool->registerMemberFunction<Container::Vector<bool> >("Empty",&Container::Vector<bool>::empty);
					vecInt08->registerMemberFunction<Container::Vector<int08> >("Empty",&Container::Vector<int08>::empty);
					vecInt16->registerMemberFunction<Container::Vector<int16> >("Empty",&Container::Vector<int16>::empty);
					vecInt32->registerMemberFunction<Container::Vector<int32> >("Empty",&Container::Vector<int32>::empty);
					vecInt64->registerMemberFunction<Container::Vector<int64> >("Empty",&Container::Vector<int64>::empty);
					vecUInt08->registerMemberFunction<Container::Vector<uint08> >("Empty",&Container::Vector<uint08>::empty);
					vecUInt16->registerMemberFunction<Container::Vector<uint16> >("Empty",&Container::Vector<uint16>::empty);
					vecUInt32->registerMemberFunction<Container::Vector<uint32> >("Empty",&Container::Vector<uint32>::empty);
					vecUInt64->registerMemberFunction<Container::Vector<uint64> >("Empty",&Container::Vector<uint64>::empty);
					vecFloat->registerMemberFunction<Container::Vector<float32> >("Empty",&Container::Vector<float32>::empty);
					vecDouble->registerMemberFunction<Container::Vector<float64> >("Empty",&Container::Vector<float64>::empty);
					vecVec2f->registerMemberFunction<Container::Vector<Math::Vec2f> >("Empty",&Container::Vector<Math::Vec2f>::empty);
					vecVec2d->registerMemberFunction<Container::Vector<Math::Vec2d> >("Empty",&Container::Vector<Math::Vec2d>::empty);
					vecVec2i32->registerMemberFunction<Container::Vector<Math::Vec2i32> >("Empty",&Container::Vector<Math::Vec2i32>::empty);
					vecVec2u32->registerMemberFunction<Container::Vector<Math::Vec2u32> >("Empty",&Container::Vector<Math::Vec2u32>::empty);
					vecVec3f->registerMemberFunction<Container::Vector<Math::Vec3f> >("Empty",&Container::Vector<Math::Vec3f>::empty);
					vecVec3d->registerMemberFunction<Container::Vector<Math::Vec3d> >("Empty",&Container::Vector<Math::Vec3d>::empty);
					vecVec3i32->registerMemberFunction<Container::Vector<Math::Vec3i32> >("Empty",&Container::Vector<Math::Vec3i32>::empty);
					vecVec3u32->registerMemberFunction<Container::Vector<Math::Vec3u32> >("Empty",&Container::Vector<Math::Vec3u32>::empty);
					vecString->registerMemberFunction<Container::Vector<String> >("Empty",&Container::Vector<String>::empty);
					vecWString->registerMemberFunction<Container::Vector<WString> >("Empty",&Container::Vector<WString>::empty);

					vecBool->registerMemberFunction<Container::Vector<bool> >("Exists",&Container::Vector<bool>::findElementPosition);
					vecInt08->registerMemberFunction<Container::Vector<int08> >("Exists",&Container::Vector<int08>::findElementPosition);
					vecInt16->registerMemberFunction<Container::Vector<int16> >("Exists",&Container::Vector<int16>::findElementPosition);
					vecInt32->registerMemberFunction<Container::Vector<int32> >("Exists",&Container::Vector<int32>::findElementPosition);
					vecInt64->registerMemberFunction<Container::Vector<int64> >("Exists",&Container::Vector<int64>::findElementPosition);
					vecUInt08->registerMemberFunction<Container::Vector<uint08> >("Exists",&Container::Vector<uint08>::findElementPosition);
					vecUInt16->registerMemberFunction<Container::Vector<uint16> >("Exists",&Container::Vector<uint16>::findElementPosition);
					vecUInt32->registerMemberFunction<Container::Vector<uint32> >("Exists",&Container::Vector<uint32>::findElementPosition);
					vecUInt64->registerMemberFunction<Container::Vector<uint64> >("Exists",&Container::Vector<uint64>::findElementPosition);
					vecFloat->registerMemberFunction<Container::Vector<float32> >("Exists",&Container::Vector<float32>::findElementPosition);
					vecDouble->registerMemberFunction<Container::Vector<float64> >("Exists",&Container::Vector<float64>::findElementPosition);
					vecVec2f->registerMemberFunction<Container::Vector<Math::Vec2f> >("Exists",&Container::Vector<Math::Vec2f>::findElementPosition);
					vecVec2d->registerMemberFunction<Container::Vector<Math::Vec2d> >("Exists",&Container::Vector<Math::Vec2d>::findElementPosition);
					vecVec2i32->registerMemberFunction<Container::Vector<Math::Vec2i32> >("Exists",&Container::Vector<Math::Vec2i32>::findElementPosition);
					vecVec2u32->registerMemberFunction<Container::Vector<Math::Vec2u32> >("Exists",&Container::Vector<Math::Vec2u32>::findElementPosition);
					vecVec3f->registerMemberFunction<Container::Vector<Math::Vec3f> >("Exists",&Container::Vector<Math::Vec3f>::findElementPosition);
					vecVec3d->registerMemberFunction<Container::Vector<Math::Vec3d> >("Exists",&Container::Vector<Math::Vec3d>::findElementPosition);
					vecVec3i32->registerMemberFunction<Container::Vector<Math::Vec3i32> >("Exists",&Container::Vector<Math::Vec3i32>::findElementPosition);
					vecVec3u32->registerMemberFunction<Container::Vector<Math::Vec3u32> >("Exists",&Container::Vector<Math::Vec3u32>::findElementPosition);
					vecString->registerMemberFunction<Container::Vector<String> >("Exists",&Container::Vector<String>::findElementPosition);
					vecWString->registerMemberFunction<Container::Vector<WString> >("Exists",&Container::Vector<WString>::findElementPosition);
				}
			};

		}
	}
}
