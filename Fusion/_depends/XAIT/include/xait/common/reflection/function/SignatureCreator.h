// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/reflection/datatype/DatatypeBase.h>
#include <xait/common/reflection/function/ModifierHelpFunction.h>

namespace XAIT
{
	namespace Common
	{
		namespace Reflection
		{
			namespace Function
			{

				class SignatureCreatorHelper
				{
				public:
					template<typename T_RETURN_VALUE>
					XAIT_NOINLINE static void createSignature(T_RETURN_VALUE (*func)(), Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
					}

					template <typename T_RETURN_VALUE,typename AT1>
					static void createSignature(T_RETURN_VALUE (*func)(AT1), Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(1);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
					}

					template <typename T_RETURN_VALUE,typename AT1,typename AT2>
					static void createSignature(T_RETURN_VALUE (*func)(AT1,AT2), Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(2);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
					}


					template <typename T_RETURN_VALUE,typename AT1,typename AT2,typename AT3>
					static void createSignature(T_RETURN_VALUE (*func)(AT1,AT2,AT3), Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(3);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
						signature.mArgumentSignature[2] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT3>::Type);
					}


					template <typename T_RETURN_VALUE,typename AT1,typename AT2,typename AT3,typename AT4>
					static void createSignature(T_RETURN_VALUE (*func)(AT1,AT2,AT3,AT4), Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(4);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
						signature.mArgumentSignature[2] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT3>::Type);
						signature.mArgumentSignature[3] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT4>::Type);
					}

					template <typename T_RETURN_VALUE,typename AT1,typename AT2,typename AT3,typename AT4,typename AT5>
					static void createSignature(T_RETURN_VALUE (*func)(AT1,AT2,AT3,AT4,AT5), Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(5);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
						signature.mArgumentSignature[2] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT3>::Type);
						signature.mArgumentSignature[3] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT4>::Type);
						signature.mArgumentSignature[4] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT5>::Type);
					} 

					template <typename T_RETURN_VALUE,typename AT1,typename AT2,typename AT3,typename AT4,typename AT5,typename AT6>
					static void createSignature(T_RETURN_VALUE (*func)(AT1,AT2,AT3,AT4,AT5,AT6), Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(6);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
						signature.mArgumentSignature[2] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT3>::Type);
						signature.mArgumentSignature[3] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT4>::Type);
						signature.mArgumentSignature[4] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT5>::Type);
						signature.mArgumentSignature[5] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT6>::Type);
					}


					template <typename T_RETURN_VALUE,typename AT1,typename AT2,typename AT3,typename AT4,typename AT5,
						typename AT6,typename AT7>
						static void createSignature(T_RETURN_VALUE (*func)(AT1,AT2,AT3,AT4,AT5,AT6,AT7), Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(7);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
						signature.mArgumentSignature[2] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT3>::Type);
						signature.mArgumentSignature[3] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT4>::Type);
						signature.mArgumentSignature[4] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT5>::Type);
						signature.mArgumentSignature[5] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT6>::Type);
						signature.mArgumentSignature[6] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT7>::Type);
					} 

					template <typename T_RETURN_VALUE,typename AT1,typename AT2,typename AT3,typename AT4,typename AT5,
						typename AT6,typename AT7,typename AT8>
						static void createSignature(T_RETURN_VALUE (*func)(AT1,AT2,AT3,AT4,AT5,AT6,AT7,AT8), Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(8);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
						signature.mArgumentSignature[2] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT3>::Type);
						signature.mArgumentSignature[3] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT4>::Type);
						signature.mArgumentSignature[4] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT5>::Type);
						signature.mArgumentSignature[5] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT6>::Type);
						signature.mArgumentSignature[6] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT7>::Type);
						signature.mArgumentSignature[7] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT8>::Type);
					} 

					template <typename T_RETURN_VALUE,typename AT1,typename AT2,typename AT3,typename AT4,typename AT5,
						typename AT6,typename AT7,typename AT8,typename AT9>
						static void createSignature(T_RETURN_VALUE (*func)(AT1,AT2,AT3,AT4,AT5,AT6,AT7,AT8,AT9), Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(9);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
						signature.mArgumentSignature[2] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT3>::Type);
						signature.mArgumentSignature[3] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT4>::Type);
						signature.mArgumentSignature[4] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT5>::Type);
						signature.mArgumentSignature[5] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT6>::Type);
						signature.mArgumentSignature[6] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT7>::Type);
						signature.mArgumentSignature[7] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT8>::Type);
						signature.mArgumentSignature[8] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT9>::Type);
					} 

					template <typename T_RETURN_VALUE,typename AT1,typename AT2,typename AT3,typename AT4,typename AT5,
						typename AT6,typename AT7,typename AT8,typename AT9,typename AT10>
						static void createSignature(T_RETURN_VALUE (*func)(AT1,AT2,AT3,AT4,AT5,AT6,AT7,AT8,AT9,AT10), Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(10);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
						signature.mArgumentSignature[2] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT3>::Type);
						signature.mArgumentSignature[3] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT4>::Type);
						signature.mArgumentSignature[4] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT5>::Type);
						signature.mArgumentSignature[5] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT6>::Type);
						signature.mArgumentSignature[6] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT7>::Type);
						signature.mArgumentSignature[7] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT8>::Type);
						signature.mArgumentSignature[8] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT9>::Type);
						signature.mArgumentSignature[9] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT10>::Type);
					} 

					template<typename T_FUNCTIONOWNER_TYPE,typename T_RETURN_VALUE>
					static void createMemberSignature(T_RETURN_VALUE (T_FUNCTIONOWNER_TYPE::*func)(), Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
					}


					template <typename T_FUNCTIONOWNER_TYPE,typename T_RETURN_VALUE,typename AT1>
					static void createMemberSignature(T_RETURN_VALUE (T_FUNCTIONOWNER_TYPE::*func)(AT1), Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(1);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
					}

					template <typename T_FUNCTIONOWNER_TYPE,typename T_RETURN_VALUE,typename AT1,typename AT2>
					static void createMemberSignature(T_RETURN_VALUE (T_FUNCTIONOWNER_TYPE::*func)(AT1,AT2), Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(2);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
					}


					template <typename T_FUNCTIONOWNER_TYPE,typename T_RETURN_VALUE,typename AT1,typename AT2,typename AT3>
					static void createMemberSignature(T_RETURN_VALUE (T_FUNCTIONOWNER_TYPE::*func)(AT1,AT2,AT3), Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(3);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
						signature.mArgumentSignature[2] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT3>::Type);
					}


					template <typename T_FUNCTIONOWNER_TYPE,typename T_RETURN_VALUE,typename AT1,typename AT2,typename AT3,typename AT4>
					static void createMemberSignature(T_RETURN_VALUE (T_FUNCTIONOWNER_TYPE::*func)(AT1,AT2,AT3,AT4), Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(4);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
						signature.mArgumentSignature[2] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT3>::Type);
						signature.mArgumentSignature[3] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT4>::Type);
					}

					template <typename T_FUNCTIONOWNER_TYPE,typename T_RETURN_VALUE,typename AT1,typename AT2,typename AT3,typename AT4,typename AT5>
					static void createMemberSignature(T_RETURN_VALUE (T_FUNCTIONOWNER_TYPE::*func)(AT1,AT2,AT3,AT4,AT5), Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(5);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
						signature.mArgumentSignature[2] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT3>::Type);
						signature.mArgumentSignature[3] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT4>::Type);
						signature.mArgumentSignature[4] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT5>::Type);
					} 

					template <typename T_FUNCTIONOWNER_TYPE,typename T_RETURN_VALUE,typename AT1,typename AT2,typename AT3,typename AT4,typename AT5,typename AT6>
					static void createMemberSignature(T_RETURN_VALUE (T_FUNCTIONOWNER_TYPE::*func)(AT1,AT2,AT3,AT4,AT5,AT6), Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(6);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
						signature.mArgumentSignature[2] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT3>::Type);
						signature.mArgumentSignature[3] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT4>::Type);
						signature.mArgumentSignature[4] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT5>::Type);
						signature.mArgumentSignature[5] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT6>::Type);
					}


					template <typename T_FUNCTIONOWNER_TYPE,typename T_RETURN_VALUE,typename AT1,typename AT2,typename AT3,typename AT4,typename AT5,
						typename AT6,typename AT7>
						static void createMemberSignature(T_RETURN_VALUE (T_FUNCTIONOWNER_TYPE::*func)(AT1,AT2,AT3,AT4,AT5,AT6,AT7), Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(7);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
						signature.mArgumentSignature[2] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT3>::Type);
						signature.mArgumentSignature[3] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT4>::Type);
						signature.mArgumentSignature[4] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT5>::Type);
						signature.mArgumentSignature[5] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT6>::Type);
						signature.mArgumentSignature[6] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT7>::Type);
					} 

					template <typename T_FUNCTIONOWNER_TYPE,typename T_RETURN_VALUE,typename AT1,typename AT2,typename AT3,typename AT4,typename AT5,
						typename AT6,typename AT7,typename AT8>
						static void createMemberSignature(T_RETURN_VALUE (T_FUNCTIONOWNER_TYPE::*func)(AT1,AT2,AT3,AT4,AT5,AT6,AT7,AT8), Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(8);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
						signature.mArgumentSignature[2] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT3>::Type);
						signature.mArgumentSignature[3] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT4>::Type);
						signature.mArgumentSignature[4] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT5>::Type);
						signature.mArgumentSignature[5] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT6>::Type);
						signature.mArgumentSignature[6] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT7>::Type);
						signature.mArgumentSignature[7] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT8>::Type);
					} 

					template <typename T_FUNCTIONOWNER_TYPE,typename T_RETURN_VALUE,typename AT1,typename AT2,typename AT3,typename AT4,typename AT5,
						typename AT6,typename AT7,typename AT8,typename AT9>
						static void createMemberSignature(T_RETURN_VALUE (T_FUNCTIONOWNER_TYPE::*func)(AT1,AT2,AT3,AT4,AT5,AT6,AT7,AT8,AT9), Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(9);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
						signature.mArgumentSignature[2] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT3>::Type);
						signature.mArgumentSignature[3] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT4>::Type);
						signature.mArgumentSignature[4] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT5>::Type);
						signature.mArgumentSignature[5] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT6>::Type);
						signature.mArgumentSignature[6] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT7>::Type);
						signature.mArgumentSignature[7] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT8>::Type);
						signature.mArgumentSignature[8] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT9>::Type);
					} 

					template <typename T_FUNCTIONOWNER_TYPE,typename T_RETURN_VALUE,typename AT1,typename AT2,typename AT3,typename AT4,typename AT5,
						typename AT6,typename AT7,typename AT8,typename AT9,typename AT10>
						static void createMemberSignature(T_RETURN_VALUE (T_FUNCTIONOWNER_TYPE::*func)(AT1,AT2,AT3,AT4,AT5,AT6,AT7,AT8,AT9,AT10), Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(10);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
						signature.mArgumentSignature[2] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT3>::Type);
						signature.mArgumentSignature[3] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT4>::Type);
						signature.mArgumentSignature[4] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT5>::Type);
						signature.mArgumentSignature[5] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT6>::Type);
						signature.mArgumentSignature[6] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT7>::Type);
						signature.mArgumentSignature[7] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT8>::Type);
						signature.mArgumentSignature[8] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT9>::Type);
						signature.mArgumentSignature[9] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT10>::Type);
					} 

					//const member functions

					template<typename T_FUNCTIONOWNER_TYPE,typename T_RETURN_VALUE>
					static void createMemberSignature(T_RETURN_VALUE (T_FUNCTIONOWNER_TYPE::*func)() const, Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
					}


					template <typename T_FUNCTIONOWNER_TYPE,typename T_RETURN_VALUE,typename AT1>
					static void createMemberSignature(T_RETURN_VALUE (T_FUNCTIONOWNER_TYPE::*func)(AT1) const, Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(1);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
					}

					template <typename T_FUNCTIONOWNER_TYPE,typename T_RETURN_VALUE,typename AT1,typename AT2>
					static void createMemberSignature(T_RETURN_VALUE (T_FUNCTIONOWNER_TYPE::*func)(AT1,AT2) const, Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(2);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
					}


					template <typename T_FUNCTIONOWNER_TYPE,typename T_RETURN_VALUE,typename AT1,typename AT2,typename AT3>
					static void createMemberSignature(T_RETURN_VALUE (T_FUNCTIONOWNER_TYPE::*func)(AT1,AT2,AT3) const, Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(3);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
						signature.mArgumentSignature[2] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT3>::Type);
					}


					template <typename T_FUNCTIONOWNER_TYPE,typename T_RETURN_VALUE,typename AT1,typename AT2,typename AT3,typename AT4>
					static void createMemberSignature(T_RETURN_VALUE (T_FUNCTIONOWNER_TYPE::*func)(AT1,AT2,AT3,AT4) const, Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(4);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
						signature.mArgumentSignature[2] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT3>::Type);
						signature.mArgumentSignature[3] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT4>::Type);
					}

					template <typename T_FUNCTIONOWNER_TYPE,typename T_RETURN_VALUE,typename AT1,typename AT2,typename AT3,typename AT4,typename AT5>
					static void createMemberSignature(T_RETURN_VALUE (T_FUNCTIONOWNER_TYPE::*func)(AT1,AT2,AT3,AT4,AT5) const, Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(5);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
						signature.mArgumentSignature[2] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT3>::Type);
						signature.mArgumentSignature[3] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT4>::Type);
						signature.mArgumentSignature[4] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT5>::Type);
					} 

					template <typename T_FUNCTIONOWNER_TYPE,typename T_RETURN_VALUE,typename AT1,typename AT2,typename AT3,typename AT4,typename AT5,typename AT6>
					static void createMemberSignature(T_RETURN_VALUE (T_FUNCTIONOWNER_TYPE::*func)(AT1,AT2,AT3,AT4,AT5,AT6) const, Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(6);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
						signature.mArgumentSignature[2] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT3>::Type);
						signature.mArgumentSignature[3] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT4>::Type);
						signature.mArgumentSignature[4] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT5>::Type);
						signature.mArgumentSignature[5] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT6>::Type);
					}


					template <typename T_FUNCTIONOWNER_TYPE,typename T_RETURN_VALUE,typename AT1,typename AT2,typename AT3,typename AT4,typename AT5,
						typename AT6,typename AT7>
						static void createMemberSignature(T_RETURN_VALUE (T_FUNCTIONOWNER_TYPE::*func)(AT1,AT2,AT3,AT4,AT5,AT6,AT7) const, Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(7);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
						signature.mArgumentSignature[2] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT3>::Type);
						signature.mArgumentSignature[3] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT4>::Type);
						signature.mArgumentSignature[4] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT5>::Type);
						signature.mArgumentSignature[5] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT6>::Type);
						signature.mArgumentSignature[6] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT7>::Type);
					} 

					template <typename T_FUNCTIONOWNER_TYPE,typename T_RETURN_VALUE,typename AT1,typename AT2,typename AT3,typename AT4,typename AT5,
						typename AT6,typename AT7,typename AT8>
						static void createMemberSignature(T_RETURN_VALUE (T_FUNCTIONOWNER_TYPE::*func)(AT1,AT2,AT3,AT4,AT5,AT6,AT7,AT8) const, Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(8);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
						signature.mArgumentSignature[2] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT3>::Type);
						signature.mArgumentSignature[3] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT4>::Type);
						signature.mArgumentSignature[4] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT5>::Type);
						signature.mArgumentSignature[5] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT6>::Type);
						signature.mArgumentSignature[6] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT7>::Type);
						signature.mArgumentSignature[7] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT8>::Type);
					} 

					template <typename T_FUNCTIONOWNER_TYPE,typename T_RETURN_VALUE,typename AT1,typename AT2,typename AT3,typename AT4,typename AT5,
						typename AT6,typename AT7,typename AT8,typename AT9>
						static void createMemberSignature(T_RETURN_VALUE (T_FUNCTIONOWNER_TYPE::*func)(AT1,AT2,AT3,AT4,AT5,AT6,AT7,AT8,AT9) const, Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(9);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
						signature.mArgumentSignature[2] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT3>::Type);
						signature.mArgumentSignature[3] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT4>::Type);
						signature.mArgumentSignature[4] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT5>::Type);
						signature.mArgumentSignature[5] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT6>::Type);
						signature.mArgumentSignature[6] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT7>::Type);
						signature.mArgumentSignature[7] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT8>::Type);
						signature.mArgumentSignature[8] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT9>::Type);
					} 

					template <typename T_FUNCTIONOWNER_TYPE,typename T_RETURN_VALUE,typename AT1,typename AT2,typename AT3,typename AT4,typename AT5,
						typename AT6,typename AT7,typename AT8,typename AT9,typename AT10>
						static void createMemberSignature(T_RETURN_VALUE (T_FUNCTIONOWNER_TYPE::*func)(AT1,AT2,AT3,AT4,AT5,AT6,AT7,AT8,AT9,AT10) const, Reflection::Function::Signature& signature)
					{
						signature.mArgumentSignature.clear();
						signature.mArgumentSignature.resize(10);
						signature.mReturnType = GET_DATATYPE_ID(typename ModifierHelpFunction<T_RETURN_VALUE>::Type);
						signature.mArgumentSignature[0] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT1>::Type);
						signature.mArgumentSignature[1] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT2>::Type);
						signature.mArgumentSignature[2] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT3>::Type);
						signature.mArgumentSignature[3] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT4>::Type);
						signature.mArgumentSignature[4] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT5>::Type);
						signature.mArgumentSignature[5] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT6>::Type);
						signature.mArgumentSignature[6] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT7>::Type);
						signature.mArgumentSignature[7] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT8>::Type);
						signature.mArgumentSignature[8] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT9>::Type);
						signature.mArgumentSignature[9] = GET_DATATYPE_ID(typename ModifierHelpFunction<AT10>::Type);
					} 
				};	
			}
		}
	}
}
