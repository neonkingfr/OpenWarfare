// (C) xaitment GmbH 2006-2012

#pragma once 



namespace XAIT
{
	namespace Common
	{
		namespace Reflection
		{
			namespace Function
			{
				template <typename T_TYPE>
				class ModifierHelpFunction
				{
				public:
					typedef T_TYPE	Type;
				};

				template <typename T_TYPE>
				class ModifierHelpFunction<const T_TYPE&>
				{
				public:
					typedef T_TYPE Type;
				};

				template <typename T_TYPE>
				class ModifierHelpFunction<T_TYPE&>
				{
				public:
					typedef T_TYPE Type;
				};

				template <typename T_TYPE>
				class ModifierHelpFunction<const T_TYPE>
				{
				public:
					typedef T_TYPE Type;
				};

			}
		}
	}
}
