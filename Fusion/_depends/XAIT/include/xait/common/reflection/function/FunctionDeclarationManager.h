// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/SharedPtr.h>
#include <xait/common/reflection/function/FunctionDeclaration.h>
#include <xait/common/container/StringLookUp.h>
#include <xait/common/reflection/function/SignatureCreator.h>
#include <xait/common/reflection/function/Signature.h>

namespace XAIT
{
	namespace Common
	{
		namespace Reflection
		{
			namespace Function
			{
				typedef Container::Vector<String>					SignatureNames;

				class FunctionDeclarationManager : public Memory::MemoryManaged
				{
				public:
					typedef SharedPtr<FunctionDeclarationManager>	Ptr;

					
					class FunctionIterator
					{
						friend class FunctionDeclarationManager;
					public:
						//! \brief get the name of the element		
						//! \returns the name of the element
						XAIT_COMMON_DLLAPI Common::String getFunctionName() const;

						//! \brief get the id of the element		
						//! \returns the id of the element
						XAIT_COMMON_DLLAPI FunctionID getFunctionID() const;

						XAIT_COMMON_DLLAPI void getSignature(Signature& signature) const;

						//! \brief set the iterator to the next element (if it is not the last one..)		
						//! \returns true if it was NOT the last one; else otherwise
						XAIT_COMMON_DLLAPI bool getNext();

						XAIT_COMMON_DLLAPI FunctionIterator& operator++();

						//! \brief checks if it we reached the end
						//! \returns the result of the check...
						XAIT_COMMON_DLLAPI bool reachedEnd() const;

						XAIT_COMMON_DLLAPI FunctionIterator(const FunctionIterator& rhs);

					private:
						FunctionIterator(FunctionDeclarationManager* owner,
										 const Container::StringLookUp<Container::Vector<FunctionID> >::ConstIterator& iterator, 
										 const Container::StringLookUp<Container::Vector<FunctionID> >::ConstIterator& endIterator);

						FunctionDeclarationManager*												mOwner;
						Container::StringLookUp<Container::Vector<FunctionID> >::ConstIterator	mIterator;
						Container::StringLookUp<Container::Vector<FunctionID> >::ConstIterator	mEndIterator;
						uint32																	mCurrentIndex;
					};

					//! \brief constructor
					XAIT_COMMON_DLLAPI FunctionDeclarationManager(const String& name);

					//! \brief copy constructor
					XAIT_COMMON_DLLAPI FunctionDeclarationManager(const FunctionDeclarationManager& other);

					//! \brief destructor
					XAIT_COMMON_DLLAPI ~FunctionDeclarationManager();

					//! \brief tests the existance of a function
					//! \param fullName			the fullName of the function (namespace + "." + name)
					//! \param fullSignature	the signature of the function 
					//! \returns true if function exists, false otherwise
					XAIT_COMMON_DLLAPI bool existsFunctionDeclaration(const String& fullName, const Signature& fullSignature);

					//! \brief tests the existance of a function
					//! \param name				the name of the function
					//! \param nameSpace        the nameSpace of the function
					//! \param fullSignature	the signature of the function 
					//! \returns true if function exists, false otherwise
					XAIT_COMMON_DLLAPI bool existsFunctionDeclaration(const String& name, const String& nameSpace, const Signature& fullSignature);


					//! \brief creates a function declaration
					//! \brief for performance reasons the fullname as well as the name and namespace can be provided to avoid not needed concatenations
					//! \param fullName			the fullName of the function (namespace + "." + name)
					//! \param name				the name of the function
					//! \param nameSpace        the nameSpace of the function
					//! \param fullSignature	the signature of the function 
					//! \returns the functionID of the dummy function
					//! \remark it is required to call the checkDummyFunctions at the end of the inialization step
					//! \remark if a corresponding function declaration exists, the ID of the existing declaration will be returned
					XAIT_COMMON_DLLAPI FunctionID createFunctionDeclaration(const String& fullName, const Signature& fullSignature);

					//! \brief creates a function declaration
					//! \param name				the name of the function
					//! \param nameSpace        the nameSpace of the function
					//! \param fullSignature	the signature of the function 
					//! \returns the functionID of the dummy function
					//! \remark it is required to call the checkDummyFunctions at the end of the inialization step
					//! \remark if a corresponding function declaration exists, the ID of the existing declaration will be returned
					XAIT_COMMON_DLLAPI FunctionID createFunctionDeclaration(const String& name, const String& nameSpace, const Signature& fullSignature);

					//! \brief creates a function declaration
					//! \brief for performance reasons the fullname as well as the name and namespace can be provided to avoid not needed concatenations
					//! \param fullName			the fullName of the function (namespace + "." + name)
					//! \param name				the name of the function
					//! \param nameSpace        the naneSpace of the function
					//! \param fullSignature	the signature of the function 
					//! \returns the functionID of the dummy function
					//! \remark it is required to call the checkDummyFunctions at the end of the inialization step
					//! \remark if a corresponding function declaration exists, the ID of the existing declaration will be returned
					XAIT_COMMON_DLLAPI FunctionID createFunctionDeclaration(const String& fullName, const Signature& fullSignature,const String& classifier, const SignatureNames& signatureNames);

					//! \brief creates a function declaration
					//! \param name				the name of the function
					//! \param nameSpace        the nameSpace of the function
					//! \param fullSignature	the signature of the function 
					//! \returns the functionID of the dummy function
					//! \remark it is required to call the checkDummyFunctions at the end of the inialization step
					//! \remark if a corresponding function declaration exists, the ID of the existing declaration will be returned
					XAIT_COMMON_DLLAPI FunctionID createFunctionDeclaration(const String& name, const String& nameSpace, const Signature& fullSignature,const String& classifier, const SignatureNames& signatureNames);

					//! \brief creates a function declaration
					//! \brief for performance reasons the fullname as well as the name and namespace can be provided to avoid not needed concatenations
					//! \param fullName			the fullName of the function (namespace + "." + name)
					//! \param name				the name of the function
					//! \param nameSpace        the naneSpace of the function
					//! \param fullSignature	the signature of the function (including the return value type as first entry)
					//! \returns the functionID of the dummy function
					//! \remark it is required to call the checkDummyFunctions at the end of the inialization step
					//! \remark if a corresponding function declaration exists, the ID of the existing declaration will be returned
					XAIT_COMMON_DLLAPI FunctionID createFunctionDeclaration(const String& fullName, const Signature& fullSignature,const String& classifier, const String& signatureNames);

					//! \brief creates a function declaration
					//! \param name				the name of the function
					//! \param nameSpace        the nameSpace of the function
					//! \param fullSignature	the signature of the function (including the return value type as first entry)
					//! \returns the functionID of the dummy function
					//! \remark it is required to call the checkDummyFunctions at the end of the inialization step
					//! \remark if a corresponding function declaration exists, the ID of the existing declaration will be returned
					XAIT_COMMON_DLLAPI FunctionID createFunctionDeclaration(const String& name, const String& nameSpace, const Signature& fullSignature,const String& classifier, const String& signatureNames);

					//! \brief get an id of a function 
					//! \param name		fullName of the function including nameSpace (for performance reasons)
					//! \returns the id of the function, MAX_UINT32 if function not registered
					XAIT_COMMON_DLLAPI FunctionID getFunctionID(const String& fullName, const Signature& fullSignature);

					//! \brief get an id of a function 
					//! \param name		 name of the function
					//! \param nameSpace nameSpace of the function
					//! \returns the id of the function, MAX_UINT32 if function not registered
					XAIT_COMMON_DLLAPI FunctionID getFunctionID(const String& name, const String& nameSpace, const Signature& fullSignature);

					//! \brief get an id of a function for a given argumentSignature
					//! \param name		name of the function
					//! \returns the id of the function, MAX_UINT32 if function not registered
					XAIT_COMMON_DLLAPI FunctionID getFunctionIDByArgumentSignature(const String& name, const Container::Vector<Datatype::DatatypeID>& argumentSignature);

					//! \brief get an id of a function 
					//! \param name		fullName of the function including nameSpace (for performance reasons)
					//! \returns pointer to vector of functionIDs registered with the given name, NULL pointer if name unknown
					XAIT_COMMON_DLLAPI const Container::Vector<FunctionID>* getFunctionIDs( const String& fullName );

					//! \brief get an id of a function 
					//! \param name		 name of the function
					//! \param nameSpace nameSpace of the function
					//! \returns pointer to vector of functionIDs registered with the given name, NULL pointer if name unknown
					XAIT_COMMON_DLLAPI const Container::Vector<FunctionID>* getFunctionIDs( const String& name, const String& nameSpace );

					//! \brief creates a function declaration from a given function
					//! \param name		 the name of the function declaration
					//! \pram  nameSpace the nameSpace of the function declaration
					//! \param func		the function that has the same signature
					//! \returns the functionID of the declaration
					template <typename T_FUNC>
					FunctionID createFunctionDeclaration(const String& name, const String& nameSpace, T_FUNC func)
					{
						Signature signature(mAllocator);
						SignatureCreatorHelper::createSignature(func,signature);
						return createFunctionDeclaration(name,nameSpace,signature);
					}

					//! \brief creates a function declaration from a given function (fullName can be assigned for performance reasons)
					//! \param fullName  the fullName of the function declaration
					//! \param name		 the name of the function declaration
					//! \pram  nameSpace the nameSpace of the function declaration
					//! \param func		the function that has the same signature
					//! \returns the functionID of the declaration
					template <typename T_FUNC>
					FunctionID createFunctionDeclaration(const String& fullName, T_FUNC func)
					{
						Signature signature(mAllocator);
						SignatureCreatorHelper::createSignature(func,signature);
						return createFunctionDeclaration(fullName,signature);
					}

					template <typename T_FUNCTIONOWNER_TYPE, typename T_FUNC>
					FunctionID createMemberFunctionDeclaration(const String& fullName, T_FUNC func)
					{
						Signature signature(mAllocator);
						SignatureCreatorHelper::createMemberSignature<T_FUNCTIONOWNER_TYPE>(func,signature);
						return createFunctionDeclaration(fullName,signature);
					}

					template <typename T_FUNCTIONOWNER_TYPE, typename T_FUNC>
					FunctionID createMemberFunctionDeclaration(const String& name, const String& nameSpace, T_FUNC func)
					{
						Signature signature(mAllocator);
						SignatureCreatorHelper::createMemberSignature<T_FUNCTIONOWNER_TYPE>(func,signature);
						return createFunctionDeclaration(name, nameSpace,signature);
					}

					//! \brief gets the name of the function declaration
					XAIT_COMMON_DLLAPI String getFunctionName(const FunctionID functionID) const;

					//! \brief gets the namespace of the function
					XAIT_COMMON_DLLAPI String getFunctionNameSpace(const FunctionID functionID) const;

					//! \brief gets the name of the function declaration including namespace
					XAIT_COMMON_DLLAPI String getFunctionFullName(const FunctionID functionID) const;

					//! \brief gets the returnvalue type of a functiondeclaration
					//! \param functionID	the id of the function
					XAIT_COMMON_DLLAPI Datatype::DatatypeID getReturnValueType(const FunctionID functionID);

					//! \brief gets the argument signature of a function
					//! \param signature		the signature of the function (returnvalue) 
					//! \param functionID		the id of the function
					//! \remark an invalid functionid will cause an assertion and an empty signature
					XAIT_COMMON_DLLAPI void getArgumentSignature(Container::Vector<Datatype::DatatypeID>& signature, FunctionID functionID);

					//! \brief gets the argument names of a function declaration
					//! \param functionID	the id of the function
					XAIT_COMMON_DLLAPI const SignatureNames& getSignatureNames(FunctionID functionID) const;

					//! \brief gets the classifier of a function
					//! \param functionID	the id of the function
					//! \returns the classifier of a function
					XAIT_COMMON_DLLAPI const String& getClassifier(FunctionID functionID) const;

					//! \brief gets the full signature of a functions
					//! \param signature	the signature of the function (return value)
					//! \param functionID	the id of the function
					XAIT_COMMON_DLLAPI void getSignature(Signature& signature, FunctionID functionID);

					//! \brief gets a function iterator
					//! \returns a function iterator
					XAIT_COMMON_DLLAPI FunctionIterator getFunctionIterator();

					//! \brief returns the function declaration for a functionID
					//! \returns the function declaration
					XAIT_COMMON_DLLAPI const XAIT::Common::Reflection::Function::FunctionDeclaration& getFunctionDeclaration(uint32 functionID) const;

					//! \brief returns the FunctionID of a similar declaration
					//! \brief a declaration with same name and same signature is similar
					//! \returns FunctionID if a similar declaration exits, else FunctionID()
					XAIT_COMMON_DLLAPI FunctionID getSimilarDeclaration(const XAIT::Common::Reflection::Function::FunctionDeclaration& decl) const;


					//! \brief creates an error message with two signatures
					//! \param msg					the error description
					//! \param functionName			the name of the function
					//! \param existingSignature	the existing Signature
					//! \param expectedSignature	the expected signature
					//! \returns an errormsg
					XAIT_COMMON_DLLAPI String createSignatureErrorMsg( const String& msg, 
						const String& functionName,
						const Signature& existingSignature,
						const Signature& expectedSignature);

					//! \brief gets the name of the functiondeclarations
					//! \returns the name of the functiondeclarations
					XAIT_COMMON_DLLAPI String getName() const;


				protected:
					String														mName;							
					Container::StringLookUp<Container::Vector<FunctionID> >		mNameFunctionMapping;
					Container::Vector<FunctionDeclaration>						mFunctionDeclarations;
					mutable Threading::SpinLock									mMutex;
				};
			}
		} 
	}
}
