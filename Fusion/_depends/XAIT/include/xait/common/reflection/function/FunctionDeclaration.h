// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/reflection/datatype/DatatypeTypedefs.h>
#include <xait/common/reflection/function/FunctionTypedefs.h>
#include <xait/common/reflection/function/Signature.h>
#include <xait/common/String.h>
#include <xait/common/container/Vector.h>

namespace XAIT
{
	namespace Common
	{
		namespace Reflection
		{
			namespace Function
			{
				struct FunctionDeclaration
				{
					//! \brief constructor
					//! \brief fullName         the name of the function including the namespace ("nameSpace.functionName")
					//! \param fullSignature	the full signature (including the returnvaluetype) of the function
					//! \param signatureNames	the names of the arguments
					//! \param classifier		the classifier of the functiondeclaration (e.g. module)
					//! \param functionID		the functionID of the declaration
					FunctionDeclaration(const String& fullName,
										const Signature& fullSignature, 
										const Container::Vector<String>& signatureNames, 
										const String& classifier,
										FunctionID functionID );



					//! \brief compares the argument signature with a given one
					//! \param signature		the signature to compare against
					//! \returns true if both signatures are equal, false otherwise
					bool compareArgumentSignature(const Container::Vector<Datatype::DatatypeID>& signature) const;

					//! \brief compares the full signature of the function (including returnvaluetype as first entry)
					//! \param signature		the signature to compare against
					//! \returns true if both signatures are equal, false otherwise
					bool compareFullSignature(const Signature& signature) const;

					//! \brief gets the argumentnames
					//! \returns the argumentnames
					const Container::Vector<String>& getSignatureNames() const;

					//! \brief gets the classifier of the declaration
					//! \param returns the classifier
					const String& getClassifier() const;



					String							mClassifier;
					String                          mFullName;
					FunctionID						mFunctionID;
					Signature						mSignature;
					Container::Vector<String>		mSignatureNames;

				};
			} 
		} 
	}
}
