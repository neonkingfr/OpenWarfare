// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/FundamentalTypes.h>

namespace XAIT
{
	namespace Common
	{
		namespace Utility
		{
			class InitCounter
			{
				uint32	mInitCount;
				char	mObjectName[28];
			public:
				InitCounter(const char* objectName)
					: mInitCount(0)
				{
					initObjectName(objectName);
				}

				InitCounter()
					: mInitCount(0)
				{
					initObjectName(NULL);
				}

				~InitCounter()
				{
					X_ASSERT_MSG(mInitCount == 0,mObjectName << " still has " << mInitCount << " initialisations left! ");
				}

				inline InitCounter& operator++()
				{
					++mInitCount;
					return *this;
				}

				inline InitCounter& operator--()
				{
					X_ASSERT_MSG(mInitCount > 0,"Deinit is called to often for: " << mObjectName);
					--mInitCount;
					return *this;
				}

				inline operator uint32()
				{
					return mInitCount;
				}

			private:
				void initObjectName(const char* objectName)
				{
					if (objectName == NULL)
						mObjectName[0]= 0;
					else
					{
						uint32 pos= 0;
						while(objectName[pos] && pos < 27)
						{
							mObjectName[pos]= objectName[pos];							
							++pos;
						}
						mObjectName[pos]= 0;
					}
				}
			};
		}
	}
}

