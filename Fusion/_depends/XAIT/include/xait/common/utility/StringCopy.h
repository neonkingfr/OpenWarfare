// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/memory/Allocator.h>
#include <string>

namespace XAIT
{
	namespace Common
	{
		namespace Utility
		{
			char* StringCopy(const char* src, const Memory::Allocator::Ptr& allocator);
			void StringDelete(char* src, const Memory::Allocator::Ptr& allocator);
		}
	}
}
