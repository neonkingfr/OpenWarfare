// (C) xaitment GmbH 2006-2012

#pragma once
#ifndef _XAIT_COMMON_UTILITY_H_
#define _XAIT_COMMON_UTILITY_H_

//////////////////////////////////////////////////////////////////////////
//							Some Usefull Templates 
//////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////
///                            Value 
//////////////////////////////////////////////////////////////////////////

namespace XAIT
{
	namespace Common
	{
		namespace Utility
		{

			template<typename T>
			class Value
			{
			public:
				inline static void setInitialValue(T& value)
				{
					new(&value) T();
				}
			};

			template<typename T>
			class Value<T*>
			{
			public:
				inline static void setInitialValue(T*& value)
				{
					value= NULL;
				}
			};


			//////////////////////////////////////////////////////////////////////////
			///                           Swap
			//////////////////////////////////////////////////////////////////////////
			template<typename T>
			inline void SwapValues(T& val1, T& val2)
			{
				T tmp= val1;
				val1= val2;
				val2= tmp;
			}

			//////////////////////////////////////////////////////////////////////////
			///	The next template built a construct where we can compare type sizes
			//////////////////////////////////////////////////////////////////////////

			template<typename T, int VALUE>
			class ValueBinder
			{
			public:
				typedef T	Type;
				static const int Value= VALUE;
			};

			template<typename T1, typename T2>
			class Max
			{
			public:
				typedef typename Max<ValueBinder<typename T1::Type,(int)(T1::Value) - 1>,ValueBinder<typename T2::Type,(int)(T2::Value) - 1> >::MaxVal	MaxVal;
			};

			template<typename T1,typename T2, int VALUE2>
			class Max<ValueBinder<T1,0>,ValueBinder<T2,VALUE2> >
			{
			public:
				typedef ValueBinder<T2,VALUE2> MaxVal;
			};

			template<typename T1, int VALUE1, typename T2>
			class Max<ValueBinder<T1,VALUE1>,ValueBinder<T2,0> >
			{
			public:
				typedef ValueBinder<T1,VALUE1>	MaxVal;
			};

			template<typename T1, typename T2>
			class Max<ValueBinder<T1,0>,ValueBinder<T2,0> >
			{
			public:
				typedef ValueBinder<T1,0>	MaxVal;
			};

			template<typename T1,typename T2>
			class MaxSizeType
			{
			public:
				typedef typename Max<ValueBinder<T1,sizeof(T1)>,ValueBinder<T2,sizeof(T2)> >::MaxVal::Type	Type;
			};

			//////////////////////////////////////////////////////////////////////////
			// A simple template specialisation that determines if a type is a pointer or not
			//////////////////////////////////////////////////////////////////////////
			template<typename T>
			class CheckForPointer
			{
			public:
				enum { HAS_POINTER = 0};
			};

			template<typename T>
			class CheckForPointer<T*>
			{
			public:
				enum { HAS_POINTER = 1};
			};
			

			//////////////////////////////////////////////////////////////////////////
			// This class provides for each construct for a variable number of parameters
			// to a function call and applies a functor to every parameter.
			//
			// You need a functor class that has the two methods
			// void setLen(const uint32 numParameter);
			// template<typename T_PARAM>
			// void process(const uint32 paramID,T_PARAM param);
			//
			//////////////////////////////////////////////////////////////////////////
			class ForeachParam
			{
			public:
				template<typename T_FUNC>
				static inline void apply(T_FUNC& func)
				{
					func.setLen(0);
				}

				template<typename T_FUNC,typename T_T0>
				static inline void apply(T_FUNC& func, T_T0& param0)
				{
					func.setLen(1);
					func.process(0,param0);
				}

				template<typename T_FUNC,typename T_T0,typename T_T1>
				static inline void apply(T_FUNC& func, T_T0& param0, T_T1& param1)
				{
					func.setLen(2);
					func.process(0,param0);
					func.process(1,param1);
				}

				template<typename T_FUNC,typename T_T0,typename T_T1, typename T_T2>
				static inline void apply(T_FUNC& func, T_T0& param0, T_T1& param1, T_T2& param2)
				{
					func.setLen(3);
					func.process(0,param0);
					func.process(1,param1);
					func.process(2,param2);
				}

				template<typename T_FUNC,typename T_T0,typename T_T1, typename T_T2, typename T_T3>
				static inline void apply(T_FUNC& func, T_T0& param0, T_T1& param1, T_T2& param2, T_T3& param3)
				{
					func.setLen(4);
					func.process(0,param0);
					func.process(1,param1);
					func.process(2,param2);
					func.process(3,param3);
				}

				template<typename T_FUNC,typename T_T0,typename T_T1, typename T_T2, typename T_T3, typename T_T4>
				static inline void apply(T_FUNC& func, T_T0& param0, T_T1& param1, T_T2& param2, T_T3& param3, T_T4& param4)
				{
					func.setLen(5);
					func.process(0,param0);
					func.process(1,param1);
					func.process(2,param2);
					func.process(3,param3);
					func.process(4,param4);
				}

				template<typename T_FUNC,typename T_T0,typename T_T1, typename T_T2, typename T_T3, typename T_T4, typename T_T5>
				static inline void apply(T_FUNC& func, T_T0& param0, T_T1& param1, T_T2& param2, T_T3& param3, T_T4& param4, T_T5& param5)
				{
					func.setLen(6);
					func.process(0,param0);
					func.process(1,param1);
					func.process(2,param2);
					func.process(3,param3);
					func.process(4,param4);
					func.process(5,param5);
				}

				template<typename T_FUNC,typename T_T0,typename T_T1, typename T_T2, typename T_T3, typename T_T4, typename T_T5, typename T_T6>
				static inline void apply(T_FUNC& func, T_T0& param0, T_T1& param1, T_T2& param2, T_T3& param3, T_T4& param4, T_T5& param5, T_T6& param6)
				{
					func.setLen(7);
					func.process(0,param0);
					func.process(1,param1);
					func.process(2,param2);
					func.process(3,param3);
					func.process(4,param4);
					func.process(5,param5);
					func.process(6,param6);
				}

				template<typename T_FUNC,typename T_T0,typename T_T1, typename T_T2, typename T_T3, typename T_T4, typename T_T5, typename T_T6, typename T_T7>
				static inline void apply(T_FUNC& func, T_T0& param0, T_T1& param1, T_T2& param2, T_T3& param3, T_T4& param4, T_T5& param5, T_T6& param6, T_T7& param7)
				{
					func.setLen(8);
					func.process(0,param0);
					func.process(1,param1);
					func.process(2,param2);
					func.process(3,param3);
					func.process(4,param4);
					func.process(5,param5);
					func.process(6,param6);
					func.process(7,param7);
				}

				template<typename T_FUNC,typename T_T0,typename T_T1, typename T_T2, typename T_T3, typename T_T4, typename T_T5, typename T_T6, typename T_T7, typename T_T8>
				static inline void apply(T_FUNC& func, T_T0& param0, T_T1& param1, T_T2& param2, T_T3& param3, T_T4& param4, T_T5& param5, T_T6& param6, T_T7& param7, T_T8& param8)
				{
					func.setLen(9);
					func.process(0,param0);
					func.process(1,param1);
					func.process(2,param2);
					func.process(3,param3);
					func.process(4,param4);
					func.process(5,param5);
					func.process(6,param6);
					func.process(7,param7);
					func.process(8,param8);
				}

				template<typename T_FUNC,typename T_T0,typename T_T1, typename T_T2, typename T_T3, typename T_T4, typename T_T5, typename T_T6, typename T_T7, typename T_T8, typename T_T9>
				static inline void apply(T_FUNC& func, T_T0& param0, T_T1& param1, T_T2& param2, T_T3& param3, T_T4& param4, T_T5& param5, T_T6& param6, T_T7& param7, T_T8& param8, T_T9& param9)
				{
					func.setLen(10);
					func.process(0,param0);
					func.process(1,param1);
					func.process(2,param2);
					func.process(3,param3);
					func.process(4,param4);
					func.process(5,param5);
					func.process(6,param6);
					func.process(7,param7);
					func.process(8,param8);
					func.process(9,param9);
				}

				template<typename T_FUNC,typename T_T0,typename T_T1, typename T_T2, typename T_T3, typename T_T4, typename T_T5, typename T_T6, typename T_T7, typename T_T8, typename T_T9, typename T_T10>
				static inline void apply(T_FUNC& func, T_T0& param0, T_T1& param1, T_T2& param2, T_T3& param3, T_T4& param4, T_T5& param5, T_T6& param6, T_T7& param7, T_T8& param8, T_T9& param9, T_T10& param10)
				{
					func.setLen(11);
					func.process(0,param0);
					func.process(1,param1);
					func.process(2,param2);
					func.process(3,param3);
					func.process(4,param4);
					func.process(5,param5);
					func.process(6,param6);
					func.process(7,param7);
					func.process(8,param8);
					func.process(9,param9);
					func.process(10,param10);
				}
			};
		}
	}
}

#endif

