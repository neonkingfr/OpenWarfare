// (C) xaitment GmbH 2006-2012

#pragma once

#define XAIT_GET_LOW_UINT32(x)      (uint32)   (x & 0x00000000FFFFFFFFULL)
#define XAIT_GET_HIGH_UINT32(x)     (uint32) ( (x & 0xFFFFFFFF00000000ULL) >> 32 )

namespace XAIT
{
	namespace Common
	{
		namespace Utility
		{
			//! \brief create a bitmask from the first bit until the n-th bit
			template<uint08 NUM_BITS>
			class BitMask
			{
			public:
				enum 
				{
					MASK = (1 << (NUM_BITS - 1)) | BitMask<NUM_BITS - 1>::MASK
				};
			};

			template<>
			class BitMask<0>
			{
			public:
				enum 
				{
					MASK = 0x0
				};
			};
		}
	}
}

