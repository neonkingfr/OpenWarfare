// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/DLLDefines.h>
#include <xait/common/SharedPtr.h>

namespace XAIT
{
	namespace Common
	{
		//! \brief simple reference count, using a non invasive counter
		class SimpleReferenceCount
		{
			struct ReferenceCounter
			{
				uint32						mValue;

				ReferenceCounter(const uint32 val)
					: mValue(val)
				{}
			};

			ReferenceCounter*		mCount;		//!< number of times the object has been referenced

		public:
			SimpleReferenceCount()
				: mCount(NULL)
			{}

			SimpleReferenceCount(const SimpleReferenceCount& other)
				: mCount(other.mCount)
			{}

			SimpleReferenceCount(const Common::Helper::IgnoreInit&) {};


			//! \brief allocate the counter and init to one
			template<typename T>
			inline void init(T*)
			{
				createCounter(1);
			}

			//! \brief dispose the counter
			template<typename T>
			inline void dispose(T*)
			{
				destroyCounter();
			}

			//! \brief increment the counter by one
			template<typename T>
			inline void increment(T*)
			{
				++(mCount->mValue);
			}

			//! \brief decrement the counter by one
			template<typename T>
			inline void decrement(T*)
			{
				--(mCount->mValue);
			}

			//! \brief test if counter is zero
			template<typename T>
			inline bool isZero(T*) const
			{
				return mCount->mValue == 0;
			}

			//! \brief test if counter is one
			template<typename T>
			inline bool isOne(T*) const
			{
				return mCount->mValue == 1;
			}

			//! \brief take a lock on the counter
			//! \remark SimpleRefCount is not thread safe so lock is empty
			template<typename T>
			inline void lock(T*)
			{}

			//! \brief unlock the counter
			//! \remark SimpleRefCount is not thread safe so lock is empty
			template<typename T>
			inline void unlock(T*)
			{}

		private:
			XAIT_COMMON_DLLAPI void createCounter(const uint32 val);
			XAIT_COMMON_DLLAPI void destroyCounter();
		};
	}
}
