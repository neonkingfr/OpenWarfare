// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/io/Stream.h>
#include <xait/common/container/Vector.h>
#include <xait/common/SharedPtr.h>
#include <xait/common/memory/MemoryManaged.h>

#define TOKENPARSER_DEF_CSET_TEXT		"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
#define TOKENPARSER_DEF_CSET_DIGIT		"0123456789-+.,"
#define TOKENPARSER_DEF_CSET_DIGITHEX	"0123456789ABCDEF"
#define TOKENPARSER_NEWLINE_CHAR		'\n'


namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			//! \brief The token parser can read tokens from a stream and can do other
			//!	useful functions to parse a text stream
			//! \remark This parser works only on byte characters (no unicode)
			class TokenParser : public Memory::MemoryManaged
			{
			public:
				//! \brief initialize a token parser on a stream
				TokenParser(IO::Stream* stream);

				//! \brief read an expected token (skipping additional whitespace in front)
				//! \param token	token to read
				//! \exception ParseException	throws if token could not be read
				void readExpectedToken(const BString& token);

				//! \brief read an token
				//! \remark reads characters as long as they are in the character set
				//! \remark skips whitespace in front
				BString readToken(const BString& tokenCharSet);

				//! \brief skip whitespace (also skips newline)
				void skipWhiteSpace();

				//! skips all chars until the next line
				//! \brief skip line
				void skipLine();

				//! \brief read characters until a specified character
				//! \param delimiter	character until which should be read
				//! \returns a string with the read characters
				//! \remark reads also the delimiter but does not return it
				BString readUntil(const BChar delimiter);

				//! \brief read characters until a specified token
				//! \param delimiter	character until which should be read
				//! \returns a string with the read characters
				//! \remark reads also the end token, but does not return it in the return string
				BString readUntilToken(const BString token);
				
				//! \brief get current line and row
				//! \param line[out]	current position translated into line
				//! \param row[out]		current position translated into row
				//! \returns the current parse position relative to the start of the parsing
				uint64 getPosition(uint32& line, uint32& row) const;

				//! \brief get the parse start
				uint64 getParseStartPosition() const
				{
					return mParseStartPos;
				}
				//! \brief reads next character from the stream and tests for newlines
				BChar readNextChar();

				//! \brief goto previous character
				void gotoPrevChar();

				// some predefined character sets
				static const char* CHARSET_TEXT;
				static const char* CHARSET_DIGIT;
				static const char* CHARSET_DIGITHEX;

			private:
				Container::Vector<uint64>	mNewLinePos;	//!< position in the stream where a new line was in the stream
				uint64						mParseStartPos;	//!< position where the parse was in the stream as he was started
				IO::Stream*					mStream;


			};
		}
	}
}
