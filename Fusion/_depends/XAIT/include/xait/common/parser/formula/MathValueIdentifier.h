// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/DLLDefines.h>
#include <xait/common/FundamentalTypes.h>

namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace Formula
			{
				class MathValueTypeIDHolder
				{
				public:
					XAIT_COMMON_DLLAPI static uint32 getNewID();

				private:
					static uint32 mNextID;
				};

				template<typename T_MATHVALUE>
				class MathValueTypeID
				{
				public:
					static uint32 getMathValueTypeID()
					{
						if (mID == X_MAX_UINT32)
							mID = MathValueTypeIDHolder::getNewID();

						return mID;
					}

				protected:
					static bool mExists;
					static uint32 mID;
				};

				template<typename T_MATHVALUE> bool MathValueTypeID<T_MATHVALUE>::mExists = false;
				template<typename T_MATHVALUE> uint32 MathValueTypeID<T_MATHVALUE>::mID = X_MAX_UINT32;
			}
		}
	}
}
