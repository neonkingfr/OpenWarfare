// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/parser/formula/MathValue.h>
#include <xait/common/math/Vec3.h>
#include <xait/common/math/Vec2.h>


namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace Formula
			{

				template<typename T_VALUETYPE,typename SourceType>
				class ConvertingMathValue : public TypedMathValue<T_VALUETYPE>
				{
				public:
					//! \brief constructor
					//! \param mathValue	the mathvalue to convert
					ConvertingMathValue(TypedMathValue<SourceType>* mathValue) : mSourceValue(mathValue)
					{}

					//! \see TypedMathValue
					virtual void evaluate(T_VALUETYPE& value,VariableAccessor* varAccessor)
					{
						SourceType oldValue;
						mSourceValue->evaluate(oldValue,varAccessor);
						value = T_VALUETYPE(oldValue);
					}

					virtual Common::String convertToString() const 
					{ 
						return "Convert"; 
					} 

				protected:
					TypedMathValue<SourceType>* mSourceValue;
				};


				template<>
				class ConvertingMathValue<Common::Math::Vec2f,int32> : public TypedMathValue<Common::Math::Vec2f>
				{
				public:
					//! \brief constructor
					//! \param mathValue	the mathvalue to convert
					ConvertingMathValue(TypedMathValue<int32>* mathValue) : mSourceValue(mathValue)
					{}

					//! \see TypedMathValue
					void evaluate(Common::Math::Vec2f& value,VariableAccessor* varAccessor)
					{
						int32 oldValue;
						mSourceValue->evaluate(oldValue,varAccessor);
						value = Common::Math::Vec2f((float32)oldValue,(float32)oldValue);
					}

					virtual Common::String convertToString() const 
					{ 
						return "Convert"; 
					} 

				protected:
					TypedMathValue<int32>* mSourceValue;
				};


				template<>
				class ConvertingMathValue<Common::Math::Vec3f,int32> : public TypedMathValue<Common::Math::Vec3f>
				{
				public:
					//! \brief constructor
					//! \param mathValue	the mathvalue to convert
					ConvertingMathValue(TypedMathValue<int32>* mathValue) : mSourceValue(mathValue)
					{}

					//! \see TypedMathValue
					void evaluate(Common::Math::Vec3f& value,VariableAccessor* varAccessor)
					{
						int32 oldValue;
						mSourceValue->evaluate(oldValue,varAccessor);
						value = Common::Math::Vec3f((float32)oldValue,(float32)oldValue,(float32)oldValue);
					}

					virtual Common::String convertToString() const 
					{ 
						return "Convert"; 
					} 

				protected:
					TypedMathValue<int32>* mSourceValue;
				};

			}
		}
	}
}
