// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/reflection/datatype/DatatypeTypedefs.h>
#include <xait/common/reflection/function/FunctionTypedefs.h>
#include <xait/common/memory/MemoryManaged.h>
#include <xait/common/parser/formula/Expression.h>
#include <xait/common/memory/Allocator.h>
#include <xait/common/reflection/function/FunctionArgumentList.h>
#include <xait/common/reflection/function/FunctionRegistrar.h>



namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace Formula
			{
				class MathValue;
				class VariableResolver;
				//! \brief this class could be used to add arbitrary types or function callbacks to the formula parser
				//! \brief the registration itself require some callback which create the mathvalue since we cannot decide here what to do without many restrictions...
				//! \brief atm we can add callback which call another callback to create the mathvalue from a string (for example resolve a simple Vec3f)
				//! \brief the next version will also take a signature and allow recursive resolving of the arguments
				class FunctionEnvironment 
				{
				public:
					//! \brief constructor
					//! \param	functionDeclarations		the used function declarations
					XAIT_COMMON_DLLAPI FunctionEnvironment(const Reflection::Function::FunctionDeclarationManager::Ptr& functionDeclarations, 
										Reflection::Function::FunctionRegistrar* functionRegistrar,
										const Memory::AllocatorPtr& allocator);

					//! \brief copy constructor
					//! \param other		the other function environment
					XAIT_COMMON_DLLAPI FunctionEnvironment(const FunctionEnvironment& other);


					//! \brief destructor
					XAIT_COMMON_DLLAPI ~FunctionEnvironment();


					//! \brief assignment operator
					//! \param other	the other function environment
					//! \returns itself after the assignment
					XAIT_COMMON_DLLAPI FunctionEnvironment& operator=(const FunctionEnvironment& other);


					//! \brief returns the functiondeclarations
					XAIT_COMMON_DLLAPI const Reflection::Function::FunctionDeclarationManager::Ptr& getFunctionDeclarations() const;
			
			
					//! \brief parse an expression which is of type "function"
					//! \param expression			the xmlnode containing the expression
					//! \param variableResolver		the variable resolver used to parse non core expressions
					//! \param errorMsg					an error message container which stores occuring errors
					//! \param returns the created mathvalue or NULL if an error occurred
					XAIT_COMMON_DLLAPI MathValue* getMathValue(Common::Parser::XML::XMLNodeElements* expression, VariableResolver* variableResolver, Expression::ErrorMsg& errorMsg);

					//! \brief parse an expression which is of type "function"
					//! \param reader			the reader
					//! \param variableResolver		the variable resolver used to parse non core expressions
					//! \param errorMsg					an error message container which stores occuring errors
					//! \param returns the created mathvalue or NULL if an error occurred
					XAIT_COMMON_DLLAPI MathValue* getMathValueBinary(Common::IO::BinaryReader* reader, VariableResolver* variableResolver, Expression::ErrorMsg& errorMsg);
					

					//! \brief parse an argumentlist of a function
					//! \param argumentList			the resulting argumentlist		(return value)
					//! \param expression			the xmlnode containing the argumentlist
					//! \param varResolver			the used variable resolver
					//! \param functionEnvironment	the used function environment
					//! \param errorMsg					an error message container which stores occuring errors
					//! \returns true if the no error occurs
					XAIT_COMMON_DLLAPI bool	parseFunctionArgumentList(Common::Container::Vector<MathValue*>& argumentList, 
														Common::Parser::XML::XMLNodeElements* argumentListNode, 
														VariableResolver* varResolver, 
														FunctionEnvironment* functionEnvironment, 
														Expression::ErrorMsg& errorMsg);

					//! \brief parse an argumentlist of a function
					//! \param argumentList			the resulting argumentlist		(return value)
					//! \param reader			the reader
					//! \param varResolver			the used variable resolver
					//! \param functionEnvironment	the used function environment
					//! \param errorMsg					an error message container which stores occuring errors
					//! \returns true if the no error occurs
					XAIT_COMMON_DLLAPI bool	parseFunctionArgumentListBinary(Common::Container::Vector<MathValue*>& argumentList, 
						Common::IO::BinaryReader* reader, 
						VariableResolver* varResolver, 
						FunctionEnvironment* functionEnvironment, 
						Expression::ErrorMsg& errorMsg);

				protected:
					//! \brief the used function declaration
					Memory::AllocatorPtr									mAllocator;
					Reflection::Function::FunctionDeclarationManager::Ptr	mFunctionDeclarations;
					Reflection::Function::FunctionRegistrar*				mFunctionRegistrar;
				};
			}
		}
	}
}

