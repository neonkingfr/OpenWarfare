// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/String.h>
#include <xait/common/memory/MemoryManaged.h>
#include <xait/common/parser/formula/FunctionEnvironment.h>

namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace Formula
			{
				class MathValue;

				/*! \brief 
				Base Class of variable resolvers.
				Variable resolvers are used to convert given expression to runtime evaluted mathvalues.					
				*/
				class VariableResolver 
				{
				public:
					//! \brief destructor
					virtual ~VariableResolver()
					{}

					//! \brief creates an variable mathvalue
					//! \param expression		an xml node that contains the expression
					//! \param errorMsg			an error message container which stores occuring errors
					//! returns the new Mathvalue; NULL if name cannot be resolved
					virtual MathValue* getVariableMathValue(Common::Parser::XML::XMLNodeElements* expression, Expression::ErrorMsg& errorMsg) = 0;

					//! \brief creates an variable mathvalue
					//! \param reader			a binary reader
					//! \param errorMsg			an error message container which stores occuring errors
					//! returns the new Mathvalue; NULL if name cannot be resolved
					virtual MathValue* getVariableMathValueBinary(Common::IO::BinaryReader* reader, Expression::ErrorMsg& errorMsg) = 0;



					//! \brief creates an membervariable mathvalue
					//! \param expression		an xml node that contains the expression
					//! \param errorMsg			an error message container which stores occuring errors
					//! returns the new Mathvalue; NULL if name cannot be resolved
					virtual Common::Parser::Formula::MathValue* getMemberVariableMathValue(Common::Parser::XML::XMLNodeElements* expression, FunctionEnvironment* functionEnvironment, Common::Parser::Formula::Expression::ErrorMsg& errorMsg) = 0;

					//! \brief creates an membervariable mathvalue
					//! \param reader			a binary reader
					//! \param errorMsg			an error message container which stores occuring errors
					//! returns the new Mathvalue; NULL if name cannot be resolved
					virtual Common::Parser::Formula::MathValue* getMemberVariableMathValueBinary(Common::IO::BinaryReader* reader, FunctionEnvironment* functionEnvironment, Common::Parser::Formula::Expression::ErrorMsg& errorMsg) = 0;



					//! \brief creates an constant mathvalue
					//! \param expression		an xml node that contains the expression
					//! \param errorMsg			an error message container which stores occuring errors
					//! returns the new Mathvalue; NULL if name cannot be resolved
					virtual MathValue* getConstantMathValue(Common::Parser::XML::XMLNodeElements* expression, Expression::ErrorMsg& errorMsg) = 0;

					//! \brief creates an constant mathvalue
					//! \param reader			a binary reader
					//! \param errorMsg			an error message container which stores occuring errors
					//! returns the new Mathvalue; NULL if name cannot be resolved
					virtual MathValue* getConstantMathValueBinary(Common::IO::BinaryReader* reader, Expression::ErrorMsg& errorMsg) = 0;



					//! \brief creates an custom mathvalue
					//! \param expression				an xml node that contains the expression
					//! \param functionEnvironment		the used function environment
					//! \param errorMsg					an error message container which stores occuring errors
					//! returns the new Mathvalue; NULL if name cannot be resolved
					virtual MathValue* getCustomMathValue(Common::Parser::XML::XMLNodeElements* expression, FunctionEnvironment* functionEnvironment, Expression::ErrorMsg& errorMsg) = 0;
				
					//! \brief creates an custom mathvalue
					//! \param reader			a binary reader
					//! \param functionEnvironment		the used function environment
					//! \param errorMsg					an error message container which stores occuring errors
					//! returns the new Mathvalue; NULL if name cannot be resolved
					virtual MathValue* getCustomMathValueBinary(Common::IO::BinaryReader* reader, FunctionEnvironment* functionEnvironment, Expression::ErrorMsg& errorMsg) = 0;

				};

			}
		}
	}
}
