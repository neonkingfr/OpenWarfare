// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/reflection/function/FunctionRegistrar.h>

namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace Formula
			{
				// forward declaration
				class FunctionEnvironment;

				//! \brief base class of variable accessor
				class VariableAccessor
				{
				public:
					virtual ~VariableAccessor() 
					{}

					virtual Reflection::Function::FunctionRegistrar* getFunctionRegistrar() const = 0;
				};
			}
		}
	}
}
