// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/parser/vrml2/SceneNode.h>
#include <xait/common/parser/vrml2/FieldTypes.h>
namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{

			namespace VRML2
			{

				class GeometryNode;
				class AppearanceNode;


				class ShapeNode : public SceneNode
				{
				public:
					SFSceneNode			mAppearance;
					SFGeometryNode		mGeometry;

					ShapeNode(SceneNode* parent);
					ShapeNode(const ShapeNode& other);
					~ShapeNode();

					void parseNode(ParseInterface* iParser);
					SceneNode* getCopy(const Memory::Allocator::Ptr& allocator);
					const char* getNodeName() const;

				};
			}
		}
	}
}
