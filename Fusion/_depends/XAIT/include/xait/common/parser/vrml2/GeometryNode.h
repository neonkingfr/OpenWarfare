// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/parser/vrml2/NodeParsable.h>
#include <xait/common/parser/vrml2/FieldTypes.h>
namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{

			namespace VRML2
			{

				class GeometryNode : public NodeParsable<GeometryNode>
				{
				public:
					enum NodeType
					{
						NT_Box,
						NT_Cone,
						NT_Cylinder,
						NT_ElevationGrid,
						NT_Extrusion,
						NT_IndexedFaceSet,
						NT_IndexedLineSet,
						NT_PointSet,
						NT_Sphere,
						NT_Text,
					};

					GeometryNode(const NodeType typeID)
						: mTypeID((uint08)typeID)
					{}

					virtual ~GeometryNode() {};

					inline NodeType getTypeID() const
					{
						return (NodeType)mTypeID;
					}

				private:
					uint08		mTypeID;

				};
			}

		}
	}
}
