// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/container/LookUp.h>
#include <xait/common/parser/vrml2/Parser.h>
#include <xait/common/CommonLog.h>

namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{

			namespace VRML2
			{
				template<class NODE_T>
				class NodeParser
				{
				public:
					NodeParser(const Memory::Allocator::Ptr& allocator)
						: mAllocator(allocator),
						mRegisteredNodes(allocator),
						mDefinedNodes(allocator)
					{}


					~NodeParser()
					{
						typename Container::LookUp<const char*,NODE_T*,Container::StringComp>::Iterator iter= mRegisteredNodes.begin();
						typename Container::LookUp<const char*,NODE_T*,Container::StringComp>::Iterator end= mRegisteredNodes.end();

						for(; iter != end; ++iter)
						{
							delete iter->second;
						}

						iter= mDefinedNodes.begin();
						end= mDefinedNodes.end();

						for(; iter != end; ++iter)
						{
							Memory::Free((void*)iter->first,mAllocator);
							delete iter->second;
						}
					}

					void registerNode(NODE_T* node)
					{
						const char* nodeName= node->getNodeName();

						X_ASSERT_MSG(nodeName,"NULL-Pointer node name is not allowed");
						X_ASSERT_MSG(!mRegisteredNodes.exists(nodeName),"There is already a node registered with name: " << nodeName);

						mRegisteredNodes.add(nodeName,node);
					}

					NODE_T* parseNode(ParseInterface* iParse)
					{
						bool newNodeDefinition = false;
						Common::BString defName;

						if(iParse->readExpectedToken("DEF"))
						{
							defName= iParse->readNextToken();

							// check if the name is not already used for another define
							if (iParse->existsDefineName(defName.getConstCharPtr()))
							{
								VRML2_EXCEPTION(iParse,"Name '" << defName << "' in DEF clause already defined.");
							}

							newNodeDefinition = true;
						}	
						else if(iParse->readExpectedToken("USE"))
						{
							const char* useName= iParse->readNextToken();

							if (!mDefinedNodes.exists(useName))
							{
								// if we are skipping nodes, we assume that the file was correctly formated
								// and that the def exists. Thus we return NULL on all nodes we do not known
								// since we have them possibly skipped.
								if (iParse->getSkipNotRegisteredNodes())
								{
									return NULL;
								}
								VRML2_EXCEPTION(iParse,"Node name '" << useName << "' in USE statement is not known");
							}
							return mDefinedNodes[useName]->getCopy(mAllocator);

						}

						// read first token to identify the node
						const char* nodeName= iParse->readNextToken();

						NODE_T* newNode= NULL;

						newNode = createNodeInstance(nodeName);
						XAIT_EXCEPTION_TRY()

						if (!newNode)
						{
							if (iParse->getSkipNotRegisteredNodes())
							{
								if (iParse->getReportSkippedNodes())
								{
									X_COMMON_MESSAGE("Skipping node '" << nodeName << "' as no appropriate node parser registered and node skipping enabled");
								}

								// skipping the node
								// we read opening { for the node description and then we read until we find the closing }
								// but we have to remember that there are nodes in between so we count opening brackets
								// and their corresponding closing brackets.
								// This assumes a correct formated vrml2 file
								uint32 openBrackets= 1;
								if (!iParse->readExpectedChar('{'))
									VRML2_EXCEPTION(iParse,"Was trying to skip a unknown node '" << nodeName << "' but the node does not start with '{'");

								while(openBrackets > 0)
								{
									const char currChar= iParse->readNextChar(false);
									if (currChar == '}')
										--openBrackets;
									else if (currChar == '{')
										++openBrackets;
									else if (currChar == '\"')
									{
										// we have to do a special treatment for strings, as string could include brackets that we do not want to
										// count, or that includes brackets that open but do not close again and vice versa, mixing up our
										// count system.
										Common::String tmp;
										iParse->gotoPrevChar();
										iParse->readSFString(&tmp);
									}
								}

								return NULL;	// no node created
							}
							else
								VRML2_EXCEPTION(iParse,"unkown node: " << nodeName);	
						}

						newNode->parseNode(iParse);

						if(newNodeDefinition)
						{
							// create a copy from the defName that we can add as key to the lookup
							uint32 size= defName.getLength();
							char* tmp= (char*)Memory::AllocateArray(mAllocator,size + 1,(char*)0);

							// use memcpy instead of strcpy, since we know already the length of our string
							memcpy(tmp,defName.getConstCharPtr(),size + 1);

							//insert this node in the lookup-so it can be instanciated later
							mDefinedNodes.add(tmp,newNode->getCopy(mAllocator));

							// insert name in global names list, to prevent further definition
							iParse->addDefineName(tmp);
						}
						XAIT_EXCEPTION_CATCH(...)
							delete newNode;
							newNode= NULL;
							XAIT_EXCEPTION_RETHROW();
						XAIT_EXCEPTION_END();

						return newNode;
					}

				private:
					NODE_T* createNodeInstance(const char* nodeName)
					{
						if (!mRegisteredNodes.exists(nodeName)) return NULL;

						return mRegisteredNodes[nodeName]->getCopy(mAllocator);
					}

					Memory::AllocatorPtr											mAllocator;
					Container::LookUp<const char*,NODE_T*,Container::StringComp>	mRegisteredNodes;
					Container::LookUp<const char*,NODE_T*,Container::StringComp>	mDefinedNodes;
				};
			}
		}
	}
}
