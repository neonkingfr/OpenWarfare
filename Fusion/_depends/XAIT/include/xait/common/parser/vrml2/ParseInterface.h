// (C) xaitment GmbH 2006-2012

#pragma once

#include <sstream>
#include <xait/common/container/LookUp.h>
#include <xait/common/parser/vrml2/FieldTypes.h>
#include <xait/common/memory/Allocator.h>

#define VRML2_PI_MAXTOKEN			1024

#define VRML2_EXCEPTION(parseInterface,msg)	\
	{parseInterface->mErrorMsg.str(""); \
	parseInterface->mErrorMsg << std::dec << msg; \
	parseInterface->throwException(parseInterface->mErrorMsg.str().c_str());}

namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace VRML2
			{
				class Parser;
				class SceneNode;
				class GeometryNode;

				class ParseInterface
				{
				public:
					class MemberDescription
					{
					public:
						MemberDescription(const Memory::Allocator::Ptr& allocator)
							: mMembersDesc(allocator)
						{}

						void addSFBool(const char* name, SFBool& member);
						void addSFVec3f(const char* name, SFVec3f& member);
						void addMFVec3f(const char* name, MFVec3f& member);
						void addSFVec2f(const char* name, SFVec2f& member);
						void addSFColor(const char* name, SFColor& member);
						void addMFColor(const char* name, MFColor& member);
						void addSFRotation(const char* name, SFRotation& member);
						void addSFQuaternion(const char* name, SFQuaternion& member);
						void addSFInt32(const char* name,SFInt32& member);
						void addMFInt32(const char* name,MFInt32& member);
						void addSFFloat(const char* name,SFFloat& member);
						void addMFFloat(const char* name, MFFloat& member);
						void addSFString(const char* name, SFString& member);
						void addMFString(const char* name, MFString& member);

						template<typename SF_SCENENODE_T>
						void addSFSceneNode(const char* name, SF_SCENENODE_T& member)
						{
							// should be a scene node, if so, this assignment works
#ifdef _MSC_VER		// works only in ms compiler without warning
							static_cast<SFSceneNode>(member);	// if this does not compile, read the line above
#endif

							mMembersDesc.add(name,Member((void*)&member,FT_SFSceneNode));
						}

						void addMFSceneNode(const char* name, MFSceneNode& member);
						
						void addSFGeometryNode(const char* name, SFGeometryNode& member);
						void addMFGeometryNode(const char* name, MFGeometryNode& member);


						template<typename GEOMPROP_T>
						void addSFGeometricProperty(const char* name, GEOMPROP_T& member)
						{
							// should be a geometry property, if so, this assignment works
#ifdef _MSC_VER		// works only in ms compiler without warning
							static_cast<SFGeometricProperty>(member);	// if this does not compile, read the line above
#endif

							mMembersDesc.add(name,Member((void*)&member,FT_SFGeometricProperty));
						}

					private:
						friend class ParseInterface;

						enum FieldType
						{
							FT_UNDEFINED = 0,
							FT_SFBool,
							FT_SFVec3f,
							FT_MFVec3f,
							FT_SFVec2f,
							FT_MFVec2f,
							FT_SFColor,
							FT_MFColor,
							FT_SFRotation,
							FT_MFRotation,
							FT_SFQuaternion,
							FT_MFQuaternion,
							FT_SFInt32,
							FT_MFInt32,
							FT_SFFloat,
							FT_MFFloat,
							FT_SFString,
							FT_MFString,
							FT_SFSceneNode,
							FT_MFSceneNode,
							FT_SFGeometryNode,
							FT_MFGeometryNode,
							FT_SFGeometricProperty,
						};

						struct Member
						{
							void*		mPtr;
							FieldType	mTypeID;
							bool		mRead;

							Member(void* ptr, const FieldType typeID)
								: mPtr(ptr),mTypeID(typeID),mRead(false)
							{}

							Member()
								: mPtr(NULL),mTypeID(FT_UNDEFINED),mRead(false)
							{}
						};

						Container::LookUp<const char*,Member,Container::StringComp>		mMembersDesc;
					};

					std::stringstream	mErrorMsg;		//!< buffer for error message

					ParseInterface(Parser* parser);

					void readMemberDescription(MemberDescription& memberDesc);

					const char* readNextToken();
					bool readExpectedToken(const char* token);

					void gotoPreviousToken();

					char readNextChar(const bool ignoreComments= false);
					void gotoPrevChar();
					bool readExpectedChar(const char expectedChar);
					bool readUntilChar(const char expectedChar);

					//! \brief read an url
					//! \param children		the parsed childrens from the file
					//! \param fileName		the filename must be relative to the parse directory
					void readURL(MFSceneNode* children, const Common::String& fileName);

					void readSFBool(SFBool* value);
					void readSFFloat(SFFloat* value);
					void readSFInt32(SFInt32* value);
					void readMFInt32(MFInt32* value);
					void readMFFloat(MFFloat* value);
					void readSFVec3f(SFVec3f* value);
					void readMFVec3f(MFVec3f* value);
					void readSFRotation(SFRotation* value);
					void readSFQuaternion(SFQuaternion* value);
					void readSFString(SFString* value);
					void readMFString(MFString* value);
					void readSFSceneNode(SFSceneNode* value);
					void readMFSceneNode(MFSceneNode* value);
					void readSFGeometryNode(SFGeometryNode* value);
					void readMFGeometryNode(MFGeometryNode* value);
					void readSFGeometricProperty(SFGeometricProperty* value);

					//! \brief check if we should skip not registered nodes
					bool getSkipNotRegisteredNodes() const;

					bool getReportSkippedNodes() const;

					void throwException(const char* errorMsg);

					//! \brief add a name from a DEF clause to the global names list
					//! \param name		name that should be added (pointer will not be owned)
					void addDefineName(const char* name);

					//! \brief test if a name exists in the global names list
					//! \param name		name to test
					//! \returns true if the name exists
					bool existsDefineName(const char* name) const;

					bool isEOF();

				private:
					Parser*			mParser;
					char			mToken[VRML2_PI_MAXTOKEN];
					char			mDigit[VRML2_PI_MAXTOKEN];
					char			mPrevChar;

					//! \brief skip white space characters
					//! \returns true if at minimum one character has been skipped, else false
					bool skipWhiteSpace();

					//void skipWhiteSpaceBackwards();

					GeometryNode* readGeometryNode();
					SceneNode* readSceneNode();

					template<typename SF_FIELD_T>
					bool isFieldValid(SF_FIELD_T&)
					{
						return true;
					}

					// for pointer we check if the pointer is not NULL
					template<typename SF_FIELD_T>
					bool isFieldValid(SF_FIELD_T*& field)
					{
						return field != NULL;
					}


					template<typename SF_FIELD_T>
					void readMultiField(void (ParseInterface::*SFReader)(SF_FIELD_T*), Container::Vector<SF_FIELD_T>* field)
					{
						SF_FIELD_T tmp;

						if (!readExpectedChar('[')) 
						{
							// read just one field
							(this->*SFReader)(&tmp);
							if (isFieldValid(tmp))
								field->pushBack(tmp);
							return;
						}

						// read first element
						if (readExpectedChar(']')) return;	// empty field
						(this->*SFReader)(&tmp);

						if (isFieldValid(tmp))
							field->pushBack(tmp);

						// test if there is whitespace, since it will be read by readExpectedChar
						// and we cannot control it in there.
						bool skippedWhiteSpace= skipWhiteSpace();

						while (!readExpectedChar(']'))
						{
							// read whitespace seperator
							if (!skippedWhiteSpace)
								VRML2_EXCEPTION(this,"elements in a multi field must be seperated by whitespace");

							(this->*SFReader)(&tmp);
							if (isFieldValid(tmp))
								field->pushBack(tmp);

							// test if there is whitespace, since it will be read by readExpectedChar
							// and we cannot control it in there.
							skippedWhiteSpace= skipWhiteSpace();
						}
					}

				};
			}
		}
	}
}
