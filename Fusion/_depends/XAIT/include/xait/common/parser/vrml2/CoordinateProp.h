// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/parser/vrml2/GeometricProperty.h>
namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace VRML2
			{

				class CoordinateProp : public GeometricProperty
				{
				public:
					MFVec3f		mPoint;

					CoordinateProp();

					void parseNode(ParseInterface* iParser);
					GeometricProperty* getCopy(const Memory::Allocator::Ptr& allocator);
					const char* getNodeName() const;
				};

			}
		}
	}
}
