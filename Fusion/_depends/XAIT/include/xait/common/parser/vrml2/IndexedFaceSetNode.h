// (C) xaitment GmbH 2006-2012

#pragma once
#include <xait/common/parser/vrml2/GeometryNode.h>

namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace VRML2
			{
				class CoordinateProp;

				class IndexedFaceSetNode : public GeometryNode
				{
				public:
					SFGeometricProperty		mColor;
					CoordinateProp*			mCoord;
					SFGeometricProperty		mNormal;
					SFGeometricProperty		mTexCoord;
					SFBool					mCCW;
					MFInt32					mColorIndex;
					SFBool					mColorPerVertex;
					SFBool					mConvex;
					MFInt32					mCoordIndex;
					SFFloat					mCreaseAngle;
					MFInt32					mNormalIndex;
					SFBool					mNormalPerVertex;
					SFBool					mSolid;
					MFInt32					mTexCoordIndex;


					IndexedFaceSetNode();
					~IndexedFaceSetNode();

					IndexedFaceSetNode(const IndexedFaceSetNode& other);

					void parseNode(ParseInterface* iParser);
					GeometryNode* getCopy(const Memory::Allocator::Ptr& allocator);
					const char* getNodeName() const;
				};

			}
		}
	}
}
