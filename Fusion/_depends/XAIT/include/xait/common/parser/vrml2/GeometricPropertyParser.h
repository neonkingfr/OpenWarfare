// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/parser/vrml2/NodeParser.h>
namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace VRML2
			{

				class GeometricProperty;

				typedef NodeParser<GeometricProperty>	GeometricPropertyParser;

			}

		}
	}
}
