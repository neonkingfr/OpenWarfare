// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/exceptions/Exception.h>

namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			class ParserException : public Exceptions::Exception
			{
			public:
				//! \brief constructor
				//! \param errorMsg		parse error message
				//! \param lineNumber	line in which the error occured
				//! \param rowNumber	row in which the error occured
				ParserException(const Common::String& errorMsg, const uint32 lineNumber, const uint32 rowNumber)
					: Exception(errorMsg),mLineNumber(lineNumber),mRowNumber(rowNumber)
				{}

				//! \brief get the name of the exception
				virtual Common::String getExceptionName() const
				{
					return "ParserException";
				}


				//! \brief get the line in which the error occured
				inline uint32 getLineNumber() const
				{
					return mLineNumber;
				}

				//! \brief get the row in which the error occured
				inline uint32 getRowNumber() const
				{
					return mRowNumber;
				}

			protected:
				virtual void getExceptionParameter(Container::List<Container::Pair<Common::String,Common::String> >& parameters)
				{
					parameters.pushBack(Container::MakePair(Common::String("Line"),Common::String(mLineNumber)));
					parameters.pushBack(Container::MakePair(Common::String("Row"),Common::String(mRowNumber)));
				}


			private:
				uint32	mLineNumber;		//!< line in which the parse error occured
				uint32	mRowNumber;		//!< row in which the parse error occured
			};
		}
	}
}

