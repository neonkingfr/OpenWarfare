// (C) xaitment GmbH 2006-2012

#pragma once 

// check if user has set compile mode
#if !((defined XAIT_DEBUG) || (defined XAIT_RELEASE) || (defined XAIT_SHIPPING))

// Try to figure build mode from common platform defines
#if defined _DEBUG
#define XAIT_DEBUG

#elif defined NDEBUG
#define XAIT_RELEASE

#elif defined DEBUG
#define XAIT_DEBUG

#elif defined RELEASE
#define XAIT_RELEASE

#elif defined SHIPPING
#define XAIT_SHIPPING

#endif

#endif


// check if compile mode is now available
#if !((defined XAIT_DEBUG) || (defined XAIT_RELEASE) || (defined XAIT_SHIPPING))
	#error "Missing build target define! Define one of XAIT_DEBUG,XAIT_RELEASE or XAIT_SHIPPING"
#endif


//////////////////////////////////////////////////////////////////////////
// ALL BUILDS
//////////////////////////////////////////////////////////////////////////
#define XAIT_EXCEPTION_DISABLE_CALLSTACK

//////////////////////////////////////////////////////////////////////////
// DEBUG
//////////////////////////////////////////////////////////////////////////
#if defined XAIT_DEBUG

#define XAIT_ASSERT_TRACE_ASSERTS
#define XAIT_REGISTER_DEFAULT_LOGREPORTER

//////////////////////////////////////////////////////////////////////////
// SHIPPING
//////////////////////////////////////////////////////////////////////////
#elif defined XAIT_SHIPPING

#define XAIT_ASSERT_DISABLE_ALL
#define XAIT_TRACE_DISABLE

//////////////////////////////////////////////////////////////////////////
// RELEASE
//////////////////////////////////////////////////////////////////////////
#else // release

#define XAIT_ASSERT_TRACE_ASSERTS
#define XAIT_REGISTER_DEFAULT_LOGREPORTER

#endif

