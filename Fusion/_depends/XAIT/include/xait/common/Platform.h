// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/platform/PlatformDefines.h>
#include <xait/common/platform/PlatformIncludes.h>

// Info about alignment.
// -------------------------------------------------------------------------------------------------
// Different compilers use different alignment syntax. See these examples:
//
// Alignment for entire class:
// DECL_ALIGN(16)  class SomeAlignedClass  { /* my cool members */ }  ATTR_ALIGN(16);
// 
// Alignment for some member:
// DECL_ALIGN(16) char mInplaceData[sizeof(SomeStruct)] ATTR_ALIGN(16);
//
// WARNING: !!!!Alignment must also be honored in DYNAMIC memory allocation!!!!
// Notice that this sometimes can confuse tag file parsers (SlickEdit/Intellisense)


//
// CLEANUP: Need to put this stuff into differnt files
//
// ------------------------------------------------------------------------------------------------
#if XAIT_OS == XAIT_OS_WIN
// ------------------------------------------------------------------------------------------------

#define XAIT_WARNING_PUSH()					__pragma ( warning( push ) )
#define XAIT_WARNING_DISABLE(warningNums)	__pragma ( warning( disable: warningNums ) )
#define XAIT_WARNING_POP()					__pragma ( warning( pop ) )

#define XAIT_NOINLINE		__declspec(noinline)
#define XAIT_FORCEINLINE	__forceinline
#define XAIT_CDECL			__cdecl
#define XAIT_FASTCALL		__fastcall
#define XAIT_CURRENT_FUNC	__FUNCTION__
#define XAIT_DLLEXPORT		__declspec(dllexport)
#define XAIT_DLLIMPORT		__declspec(dllimport)


#ifndef XAIT_NO_EXCEPTIONS

#define XAIT_THROW_EXCEPTION(excpetion) throw new excpetion;
#define XAIT_THROW_EXCEPTION_EX(excpetion) throw (excpetion);
#define XAIT_EXCEPTION_CLEAR_STACK_AND_RETHROW(excpetion) excpetion->clearCallStack(); throw excpetion;
#define XAIT_EXCEPTION_HAS_EXCEPTIONS
#define XAIT_EXCEPTION_INSTEAD(command)

#define XAIT_EXCEPTION_TRY() try {
#define XAIT_EXCEPTION_CATCH(param) } catch (param) {
#define XAIT_EXCEPTION_USE(command) command
#define XAIT_EXCEPTION_END() }
#define XAIT_EXCEPTION_RETHROW() throw

#else

#define XAIT_THROW_EXCEPTION(excpetion) X_ASSERT_MSG(0, #excpetion)
#define XAIT_THROW_EXCEPTION_EX(excpetion) X_ASSERT_MSG(0, #excpetion)
#define XAIT_EXCEPTION_CLEAR_STACK_AND_RETHROW(excpetion)
//#define XAIT_EXCEPTION_HAS_EXCEPTIONS // not defined since non available
#define XAIT_EXCEPTION_INSTEAD(command)		command


#define XAIT_EXCEPTION_TRY() if(1) {
#define XAIT_EXCEPTION_CATCH(param) } else {
#define XAIT_EXCEPTION_USE(command)
#define XAIT_EXCEPTION_END() }
#define XAIT_EXCEPTION_RETHROW() 

#endif

#if defined(_MSC_VER) // DevStudio
#define DECL_ALIGN(n)		__declspec(align(n))
#define ATTR_ALIGN(n)
#else
#error "Not supported compiler for windows platform"
#endif

// ------------------------------------------------------------------------------------------------
#elif XAIT_OS == XAIT_OS_PS3
// ------------------------------------------------------------------------------------------------

#define XAIT_WARNING_PUSH()					
#define XAIT_WARNING_DISABLE(warningNums)	
#define XAIT_WARNING_POP()					


#define XAIT_NOINLINE		// todo
#define XAIT_FORCEINLINE	// todo
#define XAIT_CDECL			
#define XAIT_FASTCALL		
#define XAIT_CURRENT_FUNC	__FUNCTION__
#define XAIT_DLLEXPORT		
#define XAIT_DLLIMPORT		


#define XAIT_THROW_EXCEPTION(excpetion) X_ASSERT_MSG(0, #excpetion)
#define XAIT_THROW_EXCEPTION_EX(excpetion) X_ASSERT_MSG(0, #excpetion)
#define XAIT_EXCEPTION_CLEAR_STACK_AND_RETHROW(excpetion)
//#define XAIT_EXCEPTION_HAS_EXCEPTIONS // not defined since non available
#define XAIT_EXCEPTION_INSTEAD(command)		command


#define XAIT_EXCEPTION_TRY() if(1) {
#define XAIT_EXCEPTION_CATCH(param) } else {
#define XAIT_EXCEPTION_USE(command)
#define XAIT_EXCEPTION_END() }
#define XAIT_EXCEPTION_RETHROW() 

#define DECL_ALIGN(n)
#define ATTR_ALIGN(n)		__attribute__((aligned(n)))

// ------------------------------------------------------------------------------------------------
#elif XAIT_OS == XAIT_OS_PS4
// ------------------------------------------------------------------------------------------------

#define XAIT_WARNING_PUSH()					
#define XAIT_WARNING_DISABLE(warningNums)	
#define XAIT_WARNING_POP()					


#define XAIT_NOINLINE		// todo
#define XAIT_FORCEINLINE	// todo
#define XAIT_CDECL			
#define XAIT_FASTCALL		
#define XAIT_CURRENT_FUNC	__FUNCTION__
#define XAIT_DLLEXPORT		__declspec(dllexport)
#define XAIT_DLLIMPORT		__declspec(dllimport)


#define XAIT_THROW_EXCEPTION(excpetion) X_ASSERT_MSG(0, #excpetion)
#define XAIT_THROW_EXCEPTION_EX(excpetion) X_ASSERT_MSG(0, #excpetion)
#define XAIT_EXCEPTION_CLEAR_STACK_AND_RETHROW(excpetion)
//#define XAIT_EXCEPTION_HAS_EXCEPTIONS // not defined since non available
#define XAIT_EXCEPTION_INSTEAD(command)		command


#define XAIT_EXCEPTION_TRY() if(1) {
#define XAIT_EXCEPTION_CATCH(param) } else {
#define XAIT_EXCEPTION_USE(command)
#define XAIT_EXCEPTION_END() }
#define XAIT_EXCEPTION_RETHROW() 

#define DECL_ALIGN(n)
#define ATTR_ALIGN(n)		__attribute__((aligned(n)))

// Enables dynamic memory allocation on Playstation 4 with
// sceLibcHeapSize = SCE_LIBC_HEAP_SIZE_EXTENDED_ALLOC_NO_LIMIT
// Disable the next line to manage these settings yourself
#define XAIT_PS4_DYNHEAP

// ------------------------------------------------------------------------------------------------
#elif XAIT_OS == XAIT_OS_WII
// ------------------------------------------------------------------------------------------------

#define XAIT_WARNING_PUSH()					
#define XAIT_WARNING_DISABLE(warningNums)	
#define XAIT_WARNING_POP()					

#define XAIT_NOINLINE		// todo
#define XAIT_FORCEINLINE	// todo
#define XAIT_CDECL			
#define XAIT_FASTCALL		
#define XAIT_CURRENT_FUNC	__PRETTY_FUNCTION__
#define XAIT_DLLEXPORT		
#define XAIT_DLLIMPORT		


#define XAIT_THROW_EXCEPTION(excpetion) X_ASSERT_MSG(0, #excpetion)
#define XAIT_THROW_EXCEPTION_EX(excpetion) X_ASSERT_MSG(0, #excpetion)
#define XAIT_EXCEPTION_CLEAR_STACK_AND_RETHROW(excpetion)
//#define XAIT_EXCEPTION_HAS_EXCEPTIONS // not defined since non available
#define XAIT_EXCEPTION_INSTEAD(command)		command


#define XAIT_EXCEPTION_TRY() if(1) {
#define XAIT_EXCEPTION_CATCH(param) } else {
#define XAIT_EXCEPTION_USE(command)
#define XAIT_EXCEPTION_END() }
#define XAIT_EXCEPTION_RETHROW() 

#define DECL_ALIGN(n)
#define ATTR_ALIGN(n)		__attribute__((aligned(n)))

// ------------------------------------------------------------------------------------------------
#elif XAIT_OS == XAIT_OS_XBOX360 || XAIT_OS == XAIT_OS_XBOX360EXT
// ------------------------------------------------------------------------------------------------

#define XAIT_WARNING_PUSH()					__pragma ( warning( push ) )
#define XAIT_WARNING_DISABLE(warningNums)	__pragma ( warning( disable: warningNums ) )
#define XAIT_WARNING_POP()					__pragma ( warning( pop ) )

#define XAIT_NOINLINE		__declspec(noinline)
#define XAIT_FORCEINLINE	__forceinline
#define XAIT_CDECL			__cdecl
#define XAIT_FASTCALL		__fastcall
#define XAIT_CURRENT_FUNC	__FUNCTION__
#define XAIT_DLLEXPORT		__declspec(dllexport)
#define XAIT_DLLIMPORT		__declspec(dllimport)

#define XAIT_THROW_EXCEPTION(excpetion) X_ASSERT_MSG(0, #excpetion)
#define XAIT_THROW_EXCEPTION_EX(excpetion) X_ASSERT_MSG(0, #excpetion)
#define XAIT_EXCEPTION_CLEAR_STACK_AND_RETHROW(excpetion)
//#define XAIT_EXCEPTION_HAS_EXCEPTIONS // not defined since non available
#define XAIT_EXCEPTION_INSTEAD(command)		command


#define XAIT_EXCEPTION_TRY() if(1) {
#define XAIT_EXCEPTION_CATCH(param) } else {
#define XAIT_EXCEPTION_USE(command)
#define XAIT_EXCEPTION_END() }
#define XAIT_EXCEPTION_RETHROW() 

#define DECL_ALIGN(n)		__declspec(align(n))
#define ATTR_ALIGN(n)


// ------------------------------------------------------------------------------------------------
#elif XAIT_OS == XAIT_OS_LINUX || XAIT_OS == XAIT_OS_MAC
// ------------------------------------------------------------------------------------------------

#define XAIT_WARNING_PUSH()					
#define XAIT_WARNING_DISABLE(warningNums)	
#define XAIT_WARNING_POP()					

#define XAIT_NOINLINE		// todo
#define XAIT_FORCEINLINE	// todo
#define XAIT_CDECL			
#define XAIT_FASTCALL		
#define XAIT_CURRENT_FUNC	__FUNCTION__
#define XAIT_DLLEXPORT		
#define XAIT_DLLIMPORT		

#define XAIT_THROW_EXCEPTION(excpetion) X_ASSERT_MSG(0, #excpetion)
#define XAIT_THROW_EXCEPTION_EX(excpetion) X_ASSERT_MSG(0, #excpetion)
#define XAIT_EXCEPTION_CLEAR_STACK_AND_RETHROW(excpetion)
//#define XAIT_EXCEPTION_HAS_EXCEPTIONS // not defined since non available
#define XAIT_EXCEPTION_INSTEAD(command)		command

#define XAIT_EXCEPTION_TRY() if(1) {
#define XAIT_EXCEPTION_CATCH(param) } else {
#define XAIT_EXCEPTION_USE(command)
#define XAIT_EXCEPTION_END() }
#define XAIT_EXCEPTION_RETHROW() 

#define DECL_ALIGN(n)
#define ATTR_ALIGN(n)		__attribute__((aligned(n)))


// ------------------------------------------------------------------------------------------------
#else
// ------------------------------------------------------------------------------------------------

#error "Unknown XAIT_OS"

// ------------------------------------------------------------------------------------------------
#endif
// ------------------------------------------------------------------------------------------------


// throw runtime error
#define XAIT_NOT_SUPPORTED	\
switch(XAIT_OS)\
{\
case XAIT_OS_PS3:\
	XAIT_FAIL(XAIT_CURRENT_FUNC << ": Not Supported on PS3");\
	break;\
case XAIT_OS_PS4:\
	XAIT_FAIL(XAIT_CURRENT_FUNC << ": Not Supported on PS4");\
	break;\
case XAIT_OS_WII:\
	XAIT_FAIL(XAIT_CURRENT_FUNC << ": Not Supported on WII");\
	break;\
case XAIT_OS_XBOX360:\
	XAIT_FAIL(XAIT_CURRENT_FUNC << ": Not Supported on XBox360");\
	break;\
case XAIT_OS_WIN:\
	XAIT_FAIL(XAIT_CURRENT_FUNC << ": Not Supported on Win");\
	break;\
default:\
	XAIT_FAIL(XAIT_CURRENT_FUNC << ": Not Supported on Unknown Platform");\
	break;\
}

#define XAIT_CURRENT_FILE	__FILE__
#define XAIT_CURRENT_LINE	__LINE__

// ------------------------------------------------------------------------------------------------------------

// macro for suppress unused variables macro
#define XAIT_UNUSED(x) (void)(x)




