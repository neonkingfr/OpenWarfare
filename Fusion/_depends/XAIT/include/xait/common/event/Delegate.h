// (C) xaitment GmbH 2006-2012

/*
(c) Sergey Ryazanov (http://home.onego.ru/~ryazanov)

Fast delegate compatible with C++ Standard.
*/
#pragma once

#include <xait/common/Platform.h>
#include <xait/common/debug/Assert.h>

namespace XAIT
{
	namespace Common
	{
		namespace Event
		{
			template <typename TSignature> class Delegate;
			template <typename TSignature> class Invoker;
		}
	}
}

#define SRUTIL_DELEGATE_CALLTYPE

#include <xait/common/event/detail/DelegateList.h>

#undef SRUTIL_DELEGATE_CALLTYPE

