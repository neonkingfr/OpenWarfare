// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/callback/FileIOCallback.h>
#include <xait/common/SharedPtr.h>

#include <stdio.h>

namespace XAIT
{
	namespace Common
	{
		namespace Callback
		{
			//! \brief This class is a sample implementation for the FileIOCallback class on PS4 systems
			class FileIOCallbackPS4 : public FileIOCallback
			{
			public:
				//! \brief constrtuctor
				FileIOCallbackPS4()
				{}

				//! \brief constrtuctor
				//! \param dealloc		custom deallocator
				FileIOCallbackPS4(Memory::AllocatorCaptured::MemoryDeallocator dealloc)
					: FileIOCallback(dealloc)
				{}

				//! \brief check if the file handle is valid
				//! \param fHandle		file handle to check
				//! \returns true if the handle is valid, false otherwise
				virtual bool isFileHandleValid(FileHandle* fHandle)
				{
					return fHandle != NULL;
				}

				//! \brief create an invalid filehandle
				//! \returns an invalid file handle
				virtual FileHandle* getInvalidFileHandle() const
				{
					return NULL;
				}

				//! \brief open a file 
				//! \param fHandle		returns the filehandle (returnValue!)
				//! \param fileName		name of the file
				//! \param fMode		the specific filemode
				//! \returns the error value. see enum Error
				virtual int32 openFile(FileHandle*& fHandle, const Common::String& fileName, const FileMode fMode)
				{
					const char* mode= NULL;

					switch (fMode)
					{
					case FMODE_OPEN_READ:
						mode= "rb";
						break;
					case FMODE_OPEN_READWRITE:
						mode= "rb+";
						break;
					case FMODE_CREATE_READWRITE:
						mode= "wb+";
						break;
					default:
						XAIT_FAIL("Unknown file open mode");
						break;
					}

					fHandle= fopen(fileName.getConstCharPtr(),mode);

					if (!fHandle)
					{
						return ERR_FILE_NOT_FOUND;
					}
					return ERR_NOERROR;
				}

				//! \brief	closes the file
				//! \param fHandle		a valid filehandle
				virtual void closeFile(FileHandle* fHandle)
				{
					fclose((FILE*)fHandle);
				}

				//! \brief determines the size of the file
				//! \param	fHandle		a valid filehandle
				//! \returns the size of the file, if no error happens, the errorcode otherwise (<0)see enum Error
				virtual int64 getFileSize(FileHandle* fHandle)
				{
					FILE* intHandle= (FILE*)fHandle;
					if (intHandle == NULL)
						return ERR_INVALID_HANDLE;

					long currPos= ftell(intHandle);
					fseek(intHandle,0,SEEK_END);
					long endPos= ftell(intHandle);
					fseek(intHandle,currPos,SEEK_SET);

					return (int64)endPos;
				}

				//! \brief reads a specified number of bytes into a buffer
				//! \param dstBuffer	the buffer that contains the data after the functioncall
				//! \param numBytes		Maximum number of bytes.
				//! \param fHandle		a valid filehandle
				//! \returns number of bytes read, which might be lower than numBytes
				virtual int32 readFromFile(void* dstBuffer, const int32 numBytes, FileHandle* fHandle)
				{
					FILE* intHandle= (FILE*)fHandle;
					if (intHandle == NULL)
						return ERR_INVALID_HANDLE;

					size_t readBytes= fread(dstBuffer,1,numBytes,intHandle);
					return (int32)readBytes;
				}

				//! \brief write data from a buffer to a file
				//! \param srcBuffer	the buffer containing the data
				//! \param numBytes		the size of the data in Bytes
				//! \param fHandle		a valid filehandle
				//! \returns returns numBytes if successfull, error code otherwise
				virtual int32 writeToFile(const void* srcBuffer, const int32 numBytes, FileHandle* fHandle)
				{
					FILE* intHandle= (FILE*)fHandle;
					if (intHandle == NULL)
						return ERR_INVALID_HANDLE;

					size_t readBytes= fwrite(srcBuffer,1,numBytes,intHandle);

					return (int32)readBytes;
				}

				//! \brief	get the current position in file
				//! \param fHandle		a valid handle
				//! \returns the current fileposition if the request was succesful; an errorcode otherwise
				virtual int64 getFilePosition(FileHandle* fHandle)
				{
					FILE* intHandle= (FILE*)fHandle;
					if (intHandle == NULL)
						return ERR_INVALID_HANDLE;

					return (int64)ftell(intHandle);
				}

				//! \brief	moves the position in the file
				//! \param offset	position offset to the origin
				//! \param origin	origin of the file seek (see enum SeekOrigin)
				//! \param fHandle	valid handle of the file
				//! \return the new position in file if successful; an errorcode otherwise
				virtual int64 seekFile(const int64 offset, const SeekOrigin origin, FileHandle* fHandle)
				{
					FILE* intHandle= (FILE*)fHandle;
					if (intHandle == NULL)
						return ERR_INVALID_HANDLE;

					int seekOrigin= 0;

					switch (origin)
					{
					case SEEKORIG_CUR:
						seekOrigin= SEEK_CUR;
						break;
					case SEEKORIG_END:
						seekOrigin= SEEK_END;
						break;
					case SEEKORIG_START:
						seekOrigin= SEEK_SET;
						break;
					default:
						XAIT_FAIL("Invalid SeekOrigin value");
						return -1;
					}

					int retVal= fseek(intHandle,(long)offset,seekOrigin);

					if (retVal != 0)
					{
						return ERR_UNKNOWN;
					}
					
					return (int64)ftell(intHandle);
				}


				//! \brief get the full path to a filename (only used for debugging purpose, can be the same fileName)
				//! \param path		path to the filename
				//! \returns full path
				virtual Common::String getFullFileName(const Common::String& path)
				{
					return path;
				}
			};
		}
	}
}

