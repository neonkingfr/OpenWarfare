// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/FundamentalTypes.h>
#include <xait/common/SharedPtr.h>
#include <xait/common/memory/AllocatorCaptured.h>
#include <xait/common/String.h>

namespace XAIT
{
	namespace Common
	{
		namespace Callback
		{
			//! \brief this class contains all needed function to operate on files and could be used as baseclass if there is a special filesystem
			//! \remark You have to override all methods if you override one, since they are interconnected. 
			//! \remark The FileHandle used for the identification of an opened file stream could be of any type since it is a void pointer. This way you can even use a valid class instance of your file class for opened files and a NULL pointer for invalid file handles. 
			//! \remark there is no need to check the validity in any function excepted in the "isFileHandleValid" function
			class FileIOCallback : public Memory::AllocatorCaptured, public ReferenceCounted
			{
			public:
				typedef SharedPtr<FileIOCallback, MemberReferenceCount< > >		Ptr;
				typedef void	FileHandle;

				//! \brief constrtuctor
				FileIOCallback()
				{}

				//! \brief constrtuctor
				//! \param dealloc		custom deallocator
				FileIOCallback(Memory::AllocatorCaptured::MemoryDeallocator dealloc)
					: AllocatorCaptured(dealloc)
				{}

				//! \brief Mode in which a file should be opened. All files should be handled in binary mode.
				enum FileMode
				{
					FMODE_INVALID,				//!< do not use, only for matching invalid states
					FMODE_OPEN_READ,			//!< opens a file read only, does fail if file does not exist
					FMODE_OPEN_READWRITE,		//!< opens a file read and write, does fail if file does not exists or is not writable
					FMODE_CREATE_READWRITE,		//!< creates a new file read and write, creates a new file, or overwrites an existing one
				};

				//! \brief origin of a seek
				enum SeekOrigin
				{
					SEEKORIG_CUR     = 1,		//!< seek starts at the current position
					SEEKORIG_END     = 2,		//!< seek starts at the end of the stream
					SEEKORIG_START   = 3,		//!< seek starts at the beginning of the stream
				};

				//! \brief error constants that can be returned from the file io
				enum Error
				{
					ERR_NOERROR			=  0,		//!< no error 
					ERR_FILE_NOT_FOUND	= -1,		//!< file cannot be found
					ERR_ACCESS			= -2,		//!< file cannot be opened with the requested file mode
					ERR_INVALID_HANDLE	= -3,		//!< error when file handle is used and does not refer to a file
					ERR_RANGE			= -4,		//!< error if a seek goes out of the file range
					ERR_DISK_FULL		= -5,		//!< error if disk is full
					ERR_UNKNOWN			= -6,		//!< error which does not fit into one of the others
				};

				//! \brief check if the file handle is valid
				//! \param fHandle		file handle to check
				//! \returns true if the handle is valid, false otherwise
				virtual bool isFileHandleValid(FileHandle* fHandle)= 0;

				//! \brief create an invalid filehandle
				//! \returns an invalid file handle
				virtual FileHandle* getInvalidFileHandle() const= 0;

				//! \brief open a file 
				//! \param fHandle		returns the filehandle (returnValue!)
				//! \param fileName		name of the file
				//! \param fMode		the specific filemode
				//! \returns the error value. see enum Error
				virtual int32 openFile(FileHandle*& fHandle, const Common::String& fileName, const FileMode fMode) = 0;

				//! \brief	closes the file
				//! \param fHandle		a valid filehandle
				virtual void closeFile(FileHandle* fHandle) = 0;

				//! \brief determines the size of the file
				//! \param	fHandle		a valid filehandle
				//! \returns the size of the file, if no error happens, the errorcode otherwise (<0)see enum Error
				virtual int64 getFileSize(FileHandle* fHandle) = 0;

				//! \brief reads a specified number of bytes into a buffer
				//! \param dstBuffer	the buffer that contains the data after the functioncall
				//! \param numBytes		Maximum number of bytes.
				//! \param fHandle		a valid filehandle
				//! \returns number of bytes read, which might be lower than numBytes
				virtual int32 readFromFile(void* dstBuffer, const int32 numBytes, FileHandle* fHandle) = 0;

				//! \brief write data from a buffer to a file
				//! \param srcBuffer	the buffer containing the data
				//! \param numBytes		the size of the data in Bytes
				//! \param fHandle		a valid filehandle
				//! \returns returns numBytes if successfull, error code otherwise
				virtual int32 writeToFile(const void* srcBuffer, const int32 numBytes, FileHandle* fHandle) = 0;

				//! \brief	get the current position in file
				//! \param fHandle		a valid handle
				//! \returns the current fileposition if the request was succesful; an errorcode otherwise
				virtual int64 getFilePosition(FileHandle* fHandle) = 0;

				//! \brief	moves the position in the file
				//! \param offset	position offset to the origin
				//! \param origin	origin of the file seek (see enum SeekOrigin)
				//! \param fHandle	valid handle of the file
				//! \return the new position in file if successful; an errorcode otherwise
				virtual int64 seekFile(const int64 offset, const SeekOrigin origin, FileHandle* fHandle) = 0;


				//! \brief get the full path to a filename (only used for debugging purpose, can be the same fileName)
				//! \param path		path to the filename
				//! \returns full path
				virtual Common::String getFullFileName(const Common::String& path) = 0;
			};
		}
	}
}

