// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/memory/Allocator.h>
#include <xait/common/callback/MemoryCallback.h>
#include <xait/common/memory/ThreadSafeAllocWrapper.h>

#if XAIT_OS == XAIT_OS_PS4
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wuninitialized"
#endif

namespace XAIT
{
	namespace Common
	{
		namespace Callback
		{
			//! \brief A allocator that uses externally supplied memory functions
			//! \remark The integrated memory alloc count system is very basic, use in combination with a more sophisticated allocator
			//!			to get better results.
			class CallbackAllocator : public Memory::Allocator
			{
			public:
				typedef Ptr::Rebind<CallbackAllocator>::Other				Ptr;
				typedef Memory::ThreadSafeAllocWrapper<CallbackAllocator>	ThreadSafe;

				CallbackAllocator()
					: Allocator("Callback"),mMemFunc(mMemFunc)
				{
					// Init mMemFunc with dummy, so internal value will not be overwritten
					// init with itself to increment refernece count
				}
				
				CallbackAllocator(const CallbackAllocator& other)
					: Memory::Allocator(other),mMemFunc(Helper::IgnoreInit())
				{
					// Init mMemFunc with dummy, since already initialized in new, and
					// use the assign operator for assignment of the other memory function callback.
					mMemFunc= other.mMemFunc;
				}

				inline void* operator new(size_t numBytes, const MemoryCallback::Ptr& memCallback)
				{
					CallbackAllocator* obj= (CallbackAllocator*)memCallback->allocateMemory((uint32)numBytes);

					// Initialize the memory callback already inside this new statement.
					// Since the constructor of CallbackAllocator does not init the memFunc 
					// pointer will stay valid inside the constructor and beyond.
					// Use the new-address operator so that we just call the constructor of the mMemFunc.
					// Normal assignment will result in an error, since obj->mMemFunc is not initialised.
					new (&(obj->mMemFunc)) MemoryCallback::Ptr(memCallback);

					// we have also to initialse the mExtAlloc, since it needs to be zero inside the constructor
					new (&(obj->mExtAlloc)) Allocator::Ptr();

					return obj;
				}

				inline void operator delete(void* ptr, const MemoryCallback::Ptr& memCallback)
				{
					memCallback->freeMemory(ptr);
				}

				inline void operator delete(void* ptr)
				{
					// get the allocator function from the object itself
					CallbackAllocator* obj= (CallbackAllocator*)ptr;
					MemoryCallback::Ptr memCB= obj->mMemFunc;

					// call destructor on memory callback
					Memory::CallDestructor(&obj->mMemFunc);
					
					memCB->freeMemory(ptr);

				}

			protected:
				inline void* allocInternal(const uint32 numBytes)
				{
					void* newBlock= mMemFunc->allocateMemory(numBytes);
					return newBlock;
				}

				inline void freeInternal(void* ptr)
				{
					mMemFunc->freeMemory(ptr);
				}

			private:
				MemoryCallback::Ptr		mMemFunc;
			};
		}
	}
}

#if XAIT_OS == XAIT_OS_PS4
#pragma clang diagnostic pop
#endif