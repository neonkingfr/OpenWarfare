// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/FundamentalTypes.h>
#include <xait/common/SharedPtr.h>
#include <xait/common/callback/MemoryCallback.h>

namespace XAIT
{
	namespace Common
	{
		namespace Callback
		{
			//! \brief  PS4 Memory Callback (only for testing purposes. Do NOT use this in a real game).
			class MemoryCallbackPS4 : public Callback::MemoryCallback
			{
			public:
				typedef SharedPtr<MemoryCallback>	Ptr;

				//! \brief constrtuctor
				MemoryCallbackPS4()
				: Callback::MemoryCallback()
				{
				}


				//! \brief constrtuctor
				//! \param dealloc		custom deallocator
				MemoryCallbackPS4(Memory::AllocatorCaptured::MemoryDeallocator dealloc)
					: Callback::MemoryCallback(dealloc)
				{
				}

				~MemoryCallbackPS4()
				{
				}
				

				//! \brief allocate a block of memory
				//! \param numBytes		number of bytes to allocate
				//! \returns a pointer to the allocated memory block or NULL of no memory could be allocated
				//! \remarks memory is created on external heap and aligned to 32
				virtual void* allocateMemory(const uint32 numBytes)
				{
					void* addr = NULL;

					uint32 realBytes = numBytes;

					if (numBytes == 0 || numBytes % (16 * 1024) != 0)
					{
						realBytes = ((numBytes / (16 * 1024)) + 1) * (16*1024);
					}

					int32_t ret = sceKernelMapFlexibleMemory(&addr, realBytes, SCE_KERNEL_PROT_CPU_READ | SCE_KERNEL_PROT_CPU_WRITE, 0);

					if (ret < 0)
						return NULL;

					return addr;
				}

				//! \brief delete a previously allocated memory block
				//! \param blockPtr		pointer to the block which should be deleted
				virtual void freeMemory(void* blockPtr)
				{
				}

			private:
			};
		}
	}
}


