// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/callback/FileIOCallbackLinux32.h>

namespace XAIT
{
	namespace Common
	{
		namespace Callback
		{
			typedef FileIOCallbackLinux32		FileIOCallbackPS3;
		}
	}
}