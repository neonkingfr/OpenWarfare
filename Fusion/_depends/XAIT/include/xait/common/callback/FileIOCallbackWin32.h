// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/FundamentalTypes.h>
#include <xait/common/SharedPtr.h>
#include <xait/common/memory/AllocatorCaptured.h>
#include <xait/common/String.h>
#include <xait/common/callback/FileIOCallback.h>

#include <io.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include <assert.h>
#include <tchar.h>
#include <share.h>

namespace XAIT
{
	namespace Common
	{
		namespace Callback
		{
			//! \brief This class is a sample implementation for the FileIOCallback class on Win32 systems
			class FileIOCallbackWin32 : public FileIOCallback
			{
			public:

				//! \brief constrtuctor
				FileIOCallbackWin32()
				{}

				//! \brief constrtuctor
				//! \param dealloc		custom deallocator
				FileIOCallbackWin32(Memory::AllocatorCaptured::MemoryDeallocator dealloc)
					: FileIOCallback(dealloc)
				{}

				//! \brief check if the file handle is valid
				//! \param fHandle		file handle to check
				//! \returns true if the handle is valid, false otherwise
				virtual bool isFileHandleValid(FileHandle* fHandle)
				{
					const int intHandle= *((int*)&fHandle);
					return intHandle >= 0;
				}

				//! \brief create an invalid filehandle
				//! \returns an invalid file handle
				virtual FileHandle* getInvalidFileHandle() const
				{
					return (FileHandle*)-1;
				}

				//! \brief open a file 
				//! \param fHandle		returns the filehandle (returnValue!)
				//! \param fileName		name of the file
				//! \param fMode		the specific filemode
				//! \returns the error value. see enum Error
				virtual int32 openFile(FileHandle*& fHandle, const Common::String& fileName, const FileMode fMode)
				{
					// build operation flags
					int oFlags= _O_BINARY;

					// build sharing flag
					// we do not allow writing to this file 
					int shFlags= _SH_DENYWR;

					// build permission flags
					// permission flags specify the file system permissions, if the file will be created
					// during the open process
					int pFlags= _S_IWRITE | _S_IREAD;		// allow R/W

					switch (fMode)
					{
					case FMODE_OPEN_READ:
						oFlags|= _O_RDONLY;
						break;
					case FMODE_OPEN_READWRITE:
						oFlags|= _O_RDWR;
						break;
					case FMODE_CREATE_READWRITE:
						oFlags|= _O_RDWR | _O_CREAT | _O_TRUNC;
						break;
					default:
						assert(false);
						return ERR_ACCESS;
					}

					// try to open the file
					int sysFHandle;
					errno_t err= _sopen_s(&sysFHandle,fileName.getConstCharPtr(),oFlags,shFlags,pFlags);

					Error errVal= ERR_NOERROR;
					switch (err)
					{
					case 0:
						break;
					case EACCES:
						// Given path is a directory, or file is read-only, but an open-for-writing operation was attempted
						errVal= ERR_ACCESS;
						break;
					case ENOENT:
						// File or path not found
						errVal= ERR_FILE_NOT_FOUND;
						break;
					default:
						errVal= ERR_UNKNOWN;
						break;
					}

					if (errVal != ERR_NOERROR)
					{
						sysFHandle= -1;
					}


					fHandle= *((FileHandle**)&sysFHandle);
					return errVal;
				}

				//! \brief	closes the file
				//! \param fHandle		a valid filehandle
				virtual void closeFile(FileHandle* fHandle)
				{
					const int intHandle= *((int*)&fHandle);
					_close(intHandle);
				}

				//! \brief determines the size of the file
				//! \param	fHandle		a valid filehandle
				//! \returns the size of the file, if no error happens, the errorcode otherwise (<0)see enum Error
				virtual int64 getFileSize(FileHandle* fHandle)
				{
					const int intHandle= *((int*)&fHandle);
					if (intHandle < 0)
						return ERR_INVALID_HANDLE;

					const __int64 length= _filelengthi64(intHandle);

					return (int64)length;
				}

				//! \brief reads a specified number of bytes into a buffer
				//! \param dstBuffer	the buffer that contains the data after the functioncall
				//! \param numBytes		Maximum number of bytes.
				//! \param fHandle		a valid filehandle
				//! \returns number of bytes read, which might be lower than numBytes
				virtual int32 readFromFile(void* dstBuffer, const int32 numBytes, FileHandle* fHandle)
				{
					const int intHandle= *((int*)&fHandle);
					if (intHandle < 0)
						return ERR_INVALID_HANDLE;

					const int readBytes= _read(intHandle,dstBuffer,numBytes);

					if (readBytes < 0)
					{
						// handle errors ....
						return 0;
					}

					return readBytes;
				}

				//! \brief write data from a buffer to a file
				//! \param srcBuffer	the buffer containing the data
				//! \param numBytes		the size of the data in Bytes
				//! \param fHandle		a valid filehandle
				//! \returns returns numBytes if successfull, error code otherwise
				virtual int32 writeToFile(const void* srcBuffer, const int32 numBytes, FileHandle* fHandle)
				{
					const int intHandle= *((int*)&fHandle);
					if (intHandle < 0)
						return ERR_INVALID_HANDLE;

					const int retVal= _write(intHandle,srcBuffer,numBytes);

					if (retVal == numBytes) return retVal;

					switch (errno)
					{
					case ENOSPC:
						// Cannot write to file, disk full.
						return ERR_DISK_FULL;
					}
					return ERR_UNKNOWN;
				}

				//! \brief	get the current position in file
				//! \param fHandle		a valid handle
				//! \returns the current fileposition if the request was succesful; an errorcode otherwise
				virtual int64 getFilePosition(FileHandle* fHandle)
				{
					const int intHandle= *((int*)&fHandle);
					if (intHandle < 0)
						return ERR_INVALID_HANDLE;

					const __int64 currentPos= _telli64(intHandle);

					return currentPos;
				}

				//! \brief	moves the position in the file
				//! \param offset	position offset to the origin
				//! \param origin	origin of the file seek (see enum SeekOrigin)
				//! \param fHandle	valid handle of the file
				//! \return the new position in file if successful; an errorcode otherwise
				virtual int64 seekFile(const int64 offset, const SeekOrigin origin, FileHandle* fHandle)
				{
					const int intHandle= *((int*)&fHandle);
					if (intHandle < 0)
						return ERR_INVALID_HANDLE;

					// build seek origin for open
					int nativOrigin= 0;
					switch (origin)
					{
					case SEEKORIG_CUR:
						nativOrigin= SEEK_CUR;
						break;
					case SEEKORIG_END:
						nativOrigin= SEEK_END;
						break;
					case SEEKORIG_START:
						nativOrigin= SEEK_SET;
						break;
					default:
						return ERR_UNKNOWN;
					}

					const __int64 newPos= _lseeki64(intHandle,offset,nativOrigin);

					if (newPos >= 0) return (int64)newPos;

					return ERR_RANGE;
				}


				//! \brief get the full path to a filename (only used for debugging purpose, can be the same fileName)
				//! \param path		path to the filename
				//! \returns full path
				virtual Common::String getFullFileName(const Common::String& path)
				{
					XAIT::Char* absolutePath= Memory::AllocateArray(Common::Library::getTempAlloc(),_MAX_PATH,(XAIT::Char*)0);

#if FALSE//_UNICODE
					if (!_wfullpath(absolutePath,path.getConstCharPtr(),_MAX_PATH))
#else
					if (!_fullpath(absolutePath,path.getConstCharPtr(),_MAX_PATH))
#endif // _UNICODE
					{
						// failed, return normal path
						return path;
					}

					Common::String retVal(absolutePath);
					Memory::Free(absolutePath,Common::Library::getTempAlloc());

					return retVal;
				}
			};
		}
	}
}

