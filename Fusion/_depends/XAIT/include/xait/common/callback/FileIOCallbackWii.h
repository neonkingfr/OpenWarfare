// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/FundamentalTypes.h>
#include <xait/common/SharedPtr.h>
#include <xait/common/memory/AllocatorCaptured.h>
#include <xait/common/callback/MemoryCallback.h>
#include <xait/common/String.h>

#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include <assert.h>
#include <stdio.h>


#include <revolution/dvd.h>

namespace XAIT
{
	namespace Common
	{
		namespace Callback
		{
			//! \brief This class is a sample implementation for the FileIOCallback class on Wii systems
			class FileIOCallbackWii : public FileIOCallback
			{

			private:
				u32 roundUp(const u32 value,const u32 base)
				{
					if(value % base != 0)
						return value + (base - (value % base));
					else
					return value;
				}
			
			public:
				//! \brief constrtuctor
				FileIOCallbackWii()
				{
					//init the device driver
					DVDInit();
				}

				//! \brief constrtuctor
				//! \param dealloc		custom deallocator
				FileIOCallbackWii(Memory::AllocatorCaptured::MemoryDeallocator dealloc)
					: FileIOCallback(dealloc)
				{
					//init the device driver
					DVDInit();
				}

				//! \brief check if the file handle is valid
				//! \param fHandle		file handle to check
				//! \returns true if the handle is valid, false otherwise
				virtual bool isFileHandleValid(FileHandle* fHandle)
				{
					  return (fHandle != NULL);  														
				}

				//! \brief create an invalid filehandle
				//! \returns an invalid file handle
				virtual FileHandle* getInvalidFileHandle() const
				{
					return NULL;
				}

				//! \brief open a file 
				//! \param fHandle		returns the filehandle (returnValue!)
				//! \param fileName		name of the file
				//! \param fMode		the specific filemode
				//! \returns the error value. see enum Error
				virtual int32 openFile(FileHandle*& fHandle, const Common::String& fileName, const FileMode fMode)
				{
					fHandle = getInvalidFileHandle();
					
					switch (fMode)
					{
					case FMODE_OPEN_READ:
						break;
					case FMODE_OPEN_READWRITE: 
					case FMODE_CREATE_READWRITE: 
						return ERR_ACCESS;
						break;
					default:
						assert(false);
						return ERR_ACCESS;
					}
					
					// try to open the file
					s32 retVal = DVDConvertPathToEntrynum(fileName.getConstCharPtr());

					if(retVal == -1)
						return ERR_FILE_NOT_FOUND;

					DVDFileInfo* fileInfo = new DVDFileInfo;
					if(!DVDFastOpen(retVal,fileInfo))
					{
						fHandle = getInvalidFileHandle();
						return ERR_ACCESS;
					}

					fHandle = (FileHandle*) fileInfo;

					return ERR_NOERROR;
				}

				//! \brief	closes the file
				//! \param fHandle		a valid filehandle
				virtual void closeFile(FileHandle* fHandle)
				{
					DVDClose((DVDFileInfo*)fHandle);
					delete (DVDFileInfo*)fHandle;
				}

				//! \brief determines the size of the file
				//! \param	fHandle		a valid filehandle
				//! \returns the size of the file, if no error happens, the errorcode otherwise (<0)see enum Error
				virtual int64 getFileSize(FileHandle* fHandle)
				{
					const u32 length = DVDGetLength((DVDFileInfo*)fHandle);
					return (int64)(roundUp(length,32));
				}

				//! \brief reads a specified number of bytes into a buffer
				//! \param dstBuffer	the buffer that contains the data after the functioncall
				//! \param numBytes		Maximum number of bytes (must be a multiple of 32).
				//! \param fHandle		a valid filehandle
				//! \returns number of bytes read, which might be lower than numBytes
				virtual int32 readFromFile(void* dstBuffer, const int32 numBytes, FileHandle* fHandle)
				{
					s32 retVal = DVDRead((DVDFileInfo*)fHandle,dstBuffer,numBytes,0);

					return ((int32)retVal);
				}

				//! \brief write data from a buffer to a file
				//! \param srcBuffer	the buffer containing the data
				//! \param numBytes		the size of the data in Bytes
				//! \param fHandle		a valid filehandle
				//! \returns returns numBytes if successfull, error code otherwise
				virtual int32 writeToFile(const void* srcBuffer, const int32 numBytes, FileHandle* fHandle)
				{
					XAIT_FAIL("writeToFile not allowed for DVD access");
					return ERR_UNKNOWN;
				}

				//! \brief	get the current position in file
				//! \param fHandle		a valid handle
				//! \returns the current fileposition if the request was succesful; an errorcode otherwise
				virtual int64 getFilePosition(FileHandle* fHandle)
				{
					XAIT_FAIL("getFilePosition not allowed for DVD access");
					return ERR_UNKNOWN;
				}

				//! \brief	moves the position in the file
				//! \param offset	position offset to the origin (must be a multiple of 4)
				//! \param origin	origin of the file seek (not used)
				//! \param fHandle	valid handle of the file
				//! \return the new position in file if successful; an errorcode otherwise
				virtual int64 seekFile(const int64 offset, const SeekOrigin origin, FileHandle* fHandle)
				{
					s32 retVal = DVDSeek((DVDFileInfo*)fHandle, offset);

					return (retVal == 0)?(offset):(ERR_RANGE);
				}


				//! \brief get the full path to a filename (only used for debugging purpose, can be the same fileName)
				//! \param path		path to the filename
				//! \returns full path
				virtual Common::String getFullFileName(const Common::String& path)
				{
					return path;	// fullpath not supported on wii so we return the normal path
				}
			};
		}
	}
}

