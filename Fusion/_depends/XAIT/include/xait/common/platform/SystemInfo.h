// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/FundamentalTypes.h>

namespace XAIT
{
	namespace Common
	{
		namespace Platform
		{
			//! \brief The system info provides you with informations about the system
			//!		like the processor, memory and so on.
			class SystemInfo
			{
			public:
				//! \brief The number of physical processors (cores) installed in the
				//!	system
				static int32 getNumberOfPhysicalProcessor();

				//! \brief The number of logic processors installed in the system.
				//!	This includes virtual processors resulting from HT.
				static int32 getNumberOfLogicalProcessors();
			};
		}
	}
}
