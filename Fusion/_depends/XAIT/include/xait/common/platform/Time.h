// (C) xaitment GmbH 2006-2012

#pragma once 


#if XAIT_OS == XAIT_OS_WIN && !defined XAIT_SHIPPING

#include <ctime>
#include <xait/common/CommonLog.h>

#define X_BENCH_INIT()					clock_t __start__ = 0; clock_t __start_inter__ = 0;
#define X_BENCH_BEGIN()					__start__= __start_inter__= clock();
#define X_BENCH_INTER(benchMsg)			{X_COMMON_MESSAGE(benchMsg << " Time used: " << (clock() - __start_inter__) / (float32)CLOCKS_PER_SEC); __start_inter__= clock();}
#define X_BENCH_END(benchMsg)			{X_COMMON_MESSAGE(benchMsg << " Time used: " << (clock() - __start__) / (float32)CLOCKS_PER_SEC);}

#else

#define X_BENCH_INIT()
#define X_BENCH_BEGIN()
#define X_BENCH_END(benchMsg)
#define X_BENCH_INTER(benchMsg)

#endif

#include <xait/common/DLLDefines.h>
#include <xait/common/FundamentalTypes.h>
#include <xait/common/debug/Assert.h>
#include <ostream>

namespace XAIT
{
	namespace Common
	{
		namespace Platform
		{
			struct MicroSeconds;

			//! \brief time in milliseconds (smallest resolution)
			template<typename T_MILLI>
			struct BasicMilliSeconds
			{
				T_MILLI		mVal;		//!< milli seconds

				inline BasicMilliSeconds()
					: mVal(0)
				{}

				inline BasicMilliSeconds(const T_MILLI milliSec)
					: mVal(milliSec)
				{}

				template<typename T_OTHER_MILLI>
				inline explicit BasicMilliSeconds(const BasicMilliSeconds<T_OTHER_MILLI>& other)
					: mVal((T_MILLI)other.mVal)
				{}

				BasicMilliSeconds(const MicroSeconds& microSec);

				inline void getTime(uint32& seconds, uint32& minutes, uint32& hours) const
				{
					uint32 carryover;
					hours= mVal / 3600000;
					carryover= mVal - hours * 3600000;
					minutes= carryover / 60000;
					carryover-= minutes * 60000;
					seconds= carryover / 1000;
				}

				inline bool operator==(const BasicMilliSeconds& val) const 
				{
					return mVal == val.mVal;
				}

				inline bool operator!=(const BasicMilliSeconds& val) const 
				{
					return mVal != val.mVal;
				}

				inline bool operator<(const BasicMilliSeconds& val) const
				{
					return mVal < val.mVal;
				}

				inline bool operator>(const BasicMilliSeconds& val) const
				{
					return mVal > val.mVal;
				}

				inline bool operator<=(const BasicMilliSeconds& val) const
				{
					return mVal <= val.mVal;
				}

				inline bool operator>=(const BasicMilliSeconds& val) const
				{
					return mVal >= val.mVal;
				}

				inline BasicMilliSeconds operator+(const BasicMilliSeconds& val) const
				{
					return BasicMilliSeconds(mVal + val.mVal);
				}

				inline BasicMilliSeconds operator-(const BasicMilliSeconds& val) const
				{
					X_ASSERT_MSG(mVal >= val.mVal,"Cannot substract a bigger time value from a smaller one, since negative time values are not represented");
					return BasicMilliSeconds(mVal - val.mVal);
				}

				inline BasicMilliSeconds& operator+=(const BasicMilliSeconds& val)
				{
					mVal+= val.mVal;
					return *this;
				}

				inline BasicMilliSeconds& operator-=(const BasicMilliSeconds& val)
				{
					X_ASSERT_MSG(mVal >= val.mVal,"Cannot substract a bigger time value from a smaller one, since negative time values are not represented");
					mVal-= val.mVal;
					return *this;
				}
			};

			typedef BasicMilliSeconds<uint32>	MilliSeconds;
			typedef BasicMilliSeconds<uint64>	MilliSeconds64;

			//! \brief time in microseconds (smallest resolution)
			struct MicroSeconds
			{
				uint64		mVal;		//!< micro seconds

				inline MicroSeconds()
					: mVal(0)
				{}

				inline MicroSeconds(const uint64 microSec)
					: mVal(microSec)
				{}

				MicroSeconds(const MilliSeconds& milliSec)
					: mVal(milliSec.mVal * 1000)
				{}

				inline void getTime(uint32& seconds, uint32& minutes, uint32& hours) const
				{
					uint64 carryover;
					hours= (uint32)(mVal / 3600000000UL);
					carryover= mVal - hours * 3600000000UL;
					minutes= (uint32)(carryover / 60000000UL);
					carryover-= minutes * 60000000UL;
					seconds= (uint32)(carryover / 1000000UL);
				}

				inline bool operator==(const MicroSeconds& val) const 
				{
					return mVal == val.mVal;
				}

				inline bool operator!=(const MicroSeconds& val) const 
				{
					return mVal != val.mVal;
				}

				inline bool operator<(const MicroSeconds& val) const
				{
					return mVal < val.mVal;
				}

				inline bool operator>(const MicroSeconds& val) const
				{
					return mVal > val.mVal;
				}

				inline bool operator<=(const MicroSeconds& val) const
				{
					return mVal <= val.mVal;
				}

				inline bool operator>=(const MicroSeconds& val) const
				{
					return mVal >= val.mVal;
				}

				inline MicroSeconds operator+(const MicroSeconds& val) const
				{
					return MicroSeconds(mVal + val.mVal);
				}

				inline MicroSeconds operator-(const MicroSeconds& val) const
				{
					X_ASSERT_MSG(mVal >= val.mVal,"Cannot substract a bigger time value from a smaller one, since negative time values are not represented");
					return MicroSeconds(mVal - val.mVal);
				}

				inline MicroSeconds& operator+=(const MicroSeconds& val)
				{
					mVal+= val.mVal;
					return *this;
				}

				inline MicroSeconds& operator-=(const MicroSeconds& val)
				{
					X_ASSERT_MSG(mVal >= val.mVal,"Cannot substract a bigger time value from a smaller one, since negative time values are not represented");
					mVal-= val.mVal;
					return *this;
				}
			};


			// constructor later defined
			template<typename T_MILLI>
			inline BasicMilliSeconds<T_MILLI>::BasicMilliSeconds(const MicroSeconds& microSec)
				: mVal((T_MILLI)(microSec.mVal / 1000))
			{};


			class Time
			{
			public:
				//! \brief initialize time class (done by init of common)
				static void init();

				//! \brief get the local system time
				//! \return the current local time
				XAIT_COMMON_DLLAPI static MilliSeconds64 getLocalTime();

				//! \brief get the local system time in gmt
				//! \return the current local time in gmt
				XAIT_COMMON_DLLAPI static MilliSeconds64 getGMTTime();

				//! \brief get performance count
				//! \returns a performance count in nanoseconds
				//! \remark can be used for execution time measure 
				XAIT_COMMON_DLLAPI static MicroSeconds getPerformanceCount();

				//! \brief get the wall time of the process
				//! \remark The wall time is the time since the process starts, including all cpu,io,wait time
				XAIT_COMMON_DLLAPI static MilliSeconds getWallTime();
			};

		}
	}

	inline std::ostream& operator<<(std::ostream& os,const Common::Platform::MicroSeconds &v)
	{
		os << v.mVal << " us";
		return os;
	}

	inline std::ostream& operator<<(std::ostream& os,const Common::Platform::MilliSeconds &v)
	{
		os << v.mVal << " ms";
		return os;
	}

}
