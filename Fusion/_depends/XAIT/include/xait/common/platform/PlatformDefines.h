// (C) xaitment GmbH 2006-2012

#pragma once

// defines values for XAIT_ARCH, which specifies the architecture of the system
#define XAIT_ARCH_INTEL_X86			0		// 32-bit system	little endian encoding (includes AMD)
#define XAIT_ARCH_INTEL_X64			1		// 64-bit system	little endian encoding (includes AMD)
#define XAIT_ARCH_CELL_PPU			2		// 64-bit system	big endian encoding
#define XAIT_ARCH_CELL_SPU			3		// 64-bit system	big endian encoding
#define XAIT_ARCH_POWERPC_32		4		// 32-bit system	big endian encoding
#define XAIT_ARCH_POWERPC_64		5		// 64-bit system	big endian encoding
#define XAIT_ARCH_XENON				6		// 64-bit system	big endian encoding

// Define values for XAIT_OS, which specifies the os of the system
#define XAIT_OS_LINUX		10
#define XAIT_OS_WIN			11
#define XAIT_OS_PS3			12
#define XAIT_OS_XBOX360		13
#define XAIT_OS_WII			14
#define XAIT_OS_MAC         15
#define XAIT_OS_XBOX360EXT	16
#define XAIT_OS_PS4		17

#if defined XAIT_PLATFORM_WIN32
#	define XAIT_ARCH	XAIT_ARCH_INTEL_X86
#	define XAIT_OS		XAIT_OS_WIN
#	ifndef _WIN32
#		define _WIN32
#	endif
#elif defined XAIT_PLATFORM_WIN64
#	define XAIT_ARCH	XAIT_ARCH_INTEL_X64
#	define XAIT_OS		XAIT_OS_WIN
#	ifndef _WIN64
#		define _WIN64
#	endif
#elif defined XAIT_PLATFORM_XBOX360
#	define XAIT_ARCH	XAIT_ARCH_XENON
#	define XAIT_OS		XAIT_OS_XBOX360
#	ifndef _XBOX
#		define _XBOX
#	endif
#elif defined XAIT_PLATFORM_WII
#	define XAIT_ARCH	XAIT_ARCH_POWERPC_32
#	define XAIT_OS		XAIT_OS_WII
#elif defined XAIT_PLATFORM_PS3_PPU
#	define XAIT_ARCH	XAIT_ARCH_CELL_PPU
#	define XAIT_OS		XAIT_OS_PS3
#elif defined XAIT_PLATFORM_PS4
#	define XAIT_ARCH	XAIT_ARCH_INTEL_X64
#	define XAIT_OS		XAIT_OS_PS4
#elif defined XAIT_PLATFORM_LINUX32
#	define XAIT_ARCH	XAIT_ARCH_INTEL_X86
#	define XAIT_OS		XAIT_OS_LINUX
#elif defined XAIT_PLATFORM_LINUX64
#	define XAIT_ARCH	XAIT_ARCH_INTEL_X64
#	define XAIT_OS		XAIT_OS_LINUX
#elif defined XAIT_PLATFORM_MAC32
#	define XAIT_ARCH	XAIT_ARCH_INTEL_X86
#	define XAIT_OS		XAIT_OS_MAC
#elif defined XAIT_PLATFORM_XBOX360EXT
#	define XAIT_ARCH	XAIT_ARCH_INTEL_X64
#	define XAIT_OS		XAIT_OS_XBOX360EXT
#	ifndef _XBOX
#		define _XBOX
#	endif
#endif


#ifndef XAIT_ARCH
#error "No destination architecture specified, define one off XAIT_PLATFORM_WIN32/WIN64/LINUX32/LINUX64/XBOX360/PS3/WII or XAIT_ARCH"
#endif

#ifndef XAIT_OS
#error "No destination operation system specified, define one off XAIT_PLATFORM_WIN32/WIN64/LINUX32/LINUX64/XBOX360/PS3/WII or XAIT_OS"
#endif

// defines for default architecture multi byte encoding style
#define XAIT_ARCH_ENDIAN_LITTLE		1
#define XAIT_ARCH_ENDIAN_BIG		2

// default defines for different architectures
// XAIT_ARCH_BITWIDTH		bit width of registers
// XAIT_ARCH_POINTERSIZE	size of a pointer on this architecture
// XAIT_ARCH_ENDIAN			endiness of the system
#if XAIT_ARCH == XAIT_ARCH_INTEL_X86
#	define XAIT_ARCH_BITWIDTH		32
#	define XAIT_ARCH_POINTERSIZE	4
#	define XAIT_ARCH_ENDIAN			XAIT_ARCH_ENDIAN_LITTLE
#elif XAIT_ARCH == XAIT_ARCH_INTEL_X64
#	define XAIT_ARCH_BITWIDTH		64
#	define XAIT_ARCH_POINTERSIZE	8
#	define XAIT_ARCH_ENDIAN			XAIT_ARCH_ENDIAN_LITTLE
#elif XAIT_ARCH == XAIT_ARCH_CELL_PPU
#	define XAIT_ARCH_BITWIDTH		64
#	define XAIT_ARCH_POINTERSIZE	4
#	define XAIT_ARCH_ENDIAN			XAIT_ARCH_ENDIAN_BIG
#elif XAIT_ARCH == XAIT_ARCH_CELL_SPU
#	define XAIT_ARCH_BITWIDTH		64
#	define XAIT_ARCH_POINTERSIZE	4
#	define XAIT_ARCH_ENDIAN			XAIT_ARCH_ENDIAN_BIG
#elif XAIT_ARCH == XAIT_ARCH_POWERPC_32
#	define XAIT_ARCH_BITWIDTH		32
#	define XAIT_ARCH_POINTERSIZE	4
#	define XAIT_ARCH_ENDIAN			XAIT_ARCH_ENDIAN_BIG
#elif XAIT_ARCH == XAIT_ARCH_POWERPC_64
#	define XAIT_ARCH_BITWIDTH		64
#	define XAIT_ARCH_POINTERSIZE	8
#	define XAIT_ARCH_ENDIAN			XAIT_ARCH_ENDIAN_BIG
#elif XAIT_ARCH == XAIT_ARCH_XENON
#	define XAIT_ARCH_BITWIDTH		64
#	define XAIT_ARCH_POINTERSIZE	4
#	define XAIT_ARCH_ENDIAN			XAIT_ARCH_ENDIAN_BIG
#else
#	error "Unknown architecture specified in XAIT_ARCH"
#endif

#if (defined _M_CEE) || (defined _MANAGED)
#define XAIT_CLR
#endif


#ifndef X_NOSIMD

#if !(defined XAIT_CLR) && (XAIT_ARCH == XAIT_ARCH_INTEL_X86 || XAIT_ARCH == XAIT_ARCH_INTEL_X64)

#define X_SIMD

#ifdef _MSC_VER
#ifdef _XBOX_VER
#ifndef XAIT_COMPILER_MSVC
#define XAIT_COMPILER_MSVC
#endif
#else
#if _MSC_VER >= 1600
#ifndef XAIT_COMPILER_MSVC10
#define XAIT_COMPILER_MSVC10
#endif
#elif _MSC_VER >= 1500
#ifndef XAIT_COMPILER_MSVC9
#define XAIT_COMPILER_MSVC9
#endif
#elif _MSC_VER >= 1400
#ifndef XAIT_COMPILER_MSVC8
#define XAIT_COMPILER_MSVC8
#endif
#endif
#endif
#endif


#ifdef XAIT_COMPILER_MSVC8
#define X_SIMD_SSE2
#else
#define X_SIMD_SSE3
#endif
#endif

#endif


