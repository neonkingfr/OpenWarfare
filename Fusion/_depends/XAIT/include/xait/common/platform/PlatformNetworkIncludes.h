// (C) xaitment GmbH 2006-2012

#pragma once

#if XAIT_OS == XAIT_OS_WIN
#include <winsock2.h>
#include <ws2tcpip.h>

//namespace XAIT
//{
//	namespace Common
//	{
//		namespace IO
//		{
//			typedef struct sockaddr_in HOST_ID;
//		}
//	}
//}

#else if XAIT_OS == XAIT_OS_XBOX360
#include <winsockx.h>

//namespace XAIT
//{
//	namespace Common
//	{
//		namespace IO
//		{
//			typedef struct sockaddr_in HOST_ID;
//		}
//	}
//}

#endif

