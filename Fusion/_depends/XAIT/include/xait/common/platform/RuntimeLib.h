// (C) xaitment GmbH 2006-2012

#pragma once
#ifndef _XAIT_COMMON_PLATFORM_RUNTIME_H_INCLUDED_
#define _XAIT_COMMON_PLATFORM_RUNTIME_H_INCLUDED_

#include <xait/common/platform/PlatformDefines.h>
#include <xait/common/debug/Assert.h>

#ifndef XAIT_OS
	#error "No XAIT_OS set"
#endif

// Difference in name...
// -----------------------------------------------------------------------------------------------
#if XAIT_OS == XAIT_OS_WIN
// -----------------------------------------------------------------------------------------------
	#include <stdlib.h>

	#define copysign(a,b) _copysign(a,b)


#if (defined _MSC_VER && _MSC_VER < 1400)
		inline int wctomb_s(int *, char * pS, int, wchar_t wchar)
		{
			return wctomb(pS, wchar);
		}
	#endif



// -----------------------------------------------------------------------------------------------
#elif XAIT_OS == XAIT_OS_LINUX || XAIT_OS == XAIT_OS_MAC
// -----------------------------------------------------------------------------------------------

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <wchar.h>
#include <errno.h>

// Secure functions not available on all plaforms

inline int wctomb_s(int *, char * pS, int, wchar_t wchar)
{
	return wctomb(pS, wchar);
}


/**

* Ansi C "itoa" based on Kernighan & Ritchie's "Ansi C":

*/

inline void strreverse(char* begin, char* end) {

	char aux;

	while(end>begin)

		aux=*end, *end--=*begin, *begin++=aux;

}

inline void strcat_s(char* dst, const size_t dstSize, const char* str)
{
	if (dstSize > 0)
		strncat(dst,str,dstSize-1);
}

inline void wcscat_s(wchar_t* dst, const size_t dstSize, const wchar_t* str)
{
	if (dstSize > 0)
		wcsncat(dst,str,dstSize-1);
}

inline void strcpy_s(char* dest, const size_t dstSize, const char* src)
{
	strncpy(dest,src,dstSize);
}

inline void wcscpy_s(wchar_t* dest, const size_t dstSize, const wchar_t* src)
{
	wcsncpy(dest,src,dstSize);
}

inline int fopen_s(FILE** pFile, const char* filename, const char* mode)
{
	*pFile= fopen(filename,mode);
	if (!(*pFile))
		return errno;
	else
		return 0;
}


void itoa(int value, char* str, int base);


// Dont know wide-char support here
#define _T(x)	(x)
#define _INTSIZEOF(n)   ( (sizeof(n) + sizeof(int) - 1) & ~(sizeof(int) - 1) )

#define sprintf_s snprintf
#define _itoa_s(val,str,size_str,base)	itoa(val,str,base)

#define sscanf_s sscanf
#define _stscanf_s  sscanf
#define strcpy_s(dest, len, source) strncpy(dest, source, len) // WARNING: needs rework


#ifndef _FARQ
#define _FARQ
#define _PDFT	ptrdiff_t
#define _SIZT	size_t
#endif 

#define _THROW0()


// -----------------------------------------------------------------------------------------------
#elif XAIT_OS == XAIT_OS_PS3
// -----------------------------------------------------------------------------------------------

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <wchar.h>
#include <errno.h>

// Secure functions not available on all plaforms

inline int wctomb_s(int *, char * pS, int, wchar_t wchar)
	{
	return wctomb(pS, wchar);
	}

/**

* Ansi C "itoa" based on Kernighan & Ritchie's "Ansi C":

*/

inline void strreverse(char* begin, char* end) {

	char aux;

	while(end>begin)

		aux=*end, *end--=*begin, *begin++=aux;

}

inline void strcat_s(char* dst, const size_t dstSize, const char* str)
{
	if (dstSize > 0)
		strncat(dst,str,dstSize-1);
}

inline void wcscat_s(wchar_t* dst, const size_t dstSize, const wchar_t* str)
{
	if (dstSize > 0)
		wcsncat(dst,str,dstSize-1);
}

inline void strcpy_s(char* dest, const size_t dstSize, const char* src)
{
	strncpy(dest,src,dstSize);
}

inline void wcscpy_s(wchar_t* dest, const size_t dstSize, const wchar_t* src)
{
	wcsncpy(dest,src,dstSize);
}

inline int fopen_s(FILE** pFile, const char* filename, const char* mode)
{
	*pFile= fopen(filename,mode);
	if (!(*pFile))
		return errno;
	else
		return 0;
}


void itoa(int value, char* str, int base);

// Dont know wide-char support here
#define _T(x)	(x)
#define _INTSIZEOF(n)   ( (sizeof(n) + sizeof(int) - 1) & ~(sizeof(int) - 1) )

#define sprintf_s snprintf
#define _itoa_s(val,str,size_str,base)	itoa(val,str,base)

#define sscanf_s sscanf
#define _stscanf_s  sscanf
#define strcpy_s(dest, len, source) strncpy(dest, source, len) // WARNING: needs rework


#elif XAIT_OS == XAIT_OS_PS4

#define _T(x)	(x)
#define _stscanf_s  sscanf

// -----------------------------------------------------------------------------------------------
#elif XAIT_OS == XAIT_OS_WII
// -----------------------------------------------------------------------------------------------

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <wchar.h>
#include <errno.h>


#ifndef _FARQ
 #define _FARQ
 #define _PDFT	ptrdiff_t
 #define _SIZT	size_t
#endif 

#define _THROW0()

inline int wctomb_s(int*, char* pS, int, wchar_t wChar)
	{
		return wctomb(pS, wChar);
		//XAIT_PLATFORM_TODO_WII("wctomb");
		return 0;
	}

#define sprintf_s snprintf
#define sscanf_s sscanf
#define _stscanf_s  sscanf

inline void strcat_s(char* dst, const size_t dstSize, const char* str)
{
	if (dstSize > 0)
		strncat(dst,str,dstSize-1);
}

inline void wcscat_s(wchar_t* dst, const size_t dstSize, const wchar_t* str)
{
	if (dstSize > 0)
		wcsncat(dst,str,dstSize-1);
}

inline void strcpy_s(char* dest, const size_t dstSize, const char* src)
{
	strncpy(dest,src,dstSize);
}

inline void wcscpy_s(wchar_t* dest, const size_t dstSize, const wchar_t* src)
{
	wcsncpy(dest,src,dstSize);
}

inline int fopen_s(FILE** pFile, const char* filename, const char* mode)
{
	*pFile= fopen(filename,mode);
	if (!(*pFile))
		return errno;
	else
		return 0;
}

#define _T(x)	(x)

#define _itoa_s(val,str,size_str,base)	itoa(val,str,base)
#define gmtime_s(retval,time)	(*retval= *(gmtime(time)))

// -----------------------------------------------------------------------------------------------
#elif XAIT_OS == XAIT_OS_XBOX360 || XAIT_OS == XAIT_OS_XBOX360EXT
// -----------------------------------------------------------------------------------------------
#define copysign(a,b) _copysign(a,b)


// -----------------------------------------------------------------------------------------------
#else
// -----------------------------------------------------------------------------------------------
#error "Unknown XAIT_OS* plaform"
#endif

#endif





