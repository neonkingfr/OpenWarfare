// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/DLLDefines.h>
#include <xait/common/memory/Allocator.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debug
		{
			class LogReporter;
			class LogManager;
		}

		class Library
		{
		public:
			XAIT_COMMON_DLLAPI static void XAIT_CDECL init(const Memory::Allocator::Ptr& commonAlloc);
			XAIT_COMMON_DLLAPI static void XAIT_CDECL close();

			XAIT_COMMON_DLLAPI static const Memory::Allocator::Ptr& XAIT_CDECL getStringAlloc();
			XAIT_COMMON_DLLAPI static const Memory::Allocator::Ptr& XAIT_CDECL getDebugAlloc();
			XAIT_COMMON_DLLAPI static const Memory::Allocator::Ptr& XAIT_CDECL getExceptionAlloc();
			XAIT_COMMON_DLLAPI static const Memory::Allocator::Ptr& XAIT_CDECL getTempAlloc();

			XAIT_COMMON_DLLAPI static Debug::LogReporter* XAIT_CDECL getLogReporter();

			XAIT_COMMON_DLLAPI static Debug::LogManager* XAIT_CDECL getLogManager();
		};
	}
}

