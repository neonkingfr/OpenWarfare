// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/callback/MemoryCallback.h>
#include <xait/common/callback/AssertCallback.h>
#include <xait/common/memory/Allocator.h>
#include <xait/common/DLLDefines.h>
#include <xait/common/debug/LogReporter.h>

namespace XAIT
{
	namespace Common
	{
		class Interface
		{
		public:
			//! Initialize the common library for usage of a different memory callback
			//! \brief initialize the common library
			//! \param memCallback	pointer to the memory callback that should be used, if nothing defined, the normal new and delete will be used.
			XAIT_COMMON_DLLAPI static void XAIT_CDECL initLibrary(const Callback::MemoryCallback::Ptr& memCallback= Callback::MemoryCallback::Ptr(NULL));

			//! \brief close the library
			//! \returns the initial given memory callback to initLibrary
			XAIT_COMMON_DLLAPI static void XAIT_CDECL closeLibrary();

			//! \brief Set the assert callback
			//! \remark The default assert callback opens a gui dialog
			XAIT_COMMON_DLLAPI static void XAIT_CDECL setAssertCallback(const Callback::AssertCallback::Ptr& assertCallback);

			//! \brief get the current assert callback
			XAIT_COMMON_DLLAPI static const Callback::AssertCallback::Ptr& XAIT_CDECL getAssertCallback();

			//! \brief get the log reporter of the common library
			XAIT_COMMON_DLLAPI static Debug::LogReporter* XAIT_CDECL getLogReporter();

			//! \brief get the global memory allocator defined in common
			XAIT_COMMON_DLLAPI static const Memory::Allocator::Ptr& XAIT_CDECL getGlobalAllocator();
		};
	}
}

