// (C) xaitment GmbH 2006-2012

#pragma once



namespace XAIT
{
	namespace Common
	{
		//! This class is designed to hold a reference to an object. If the reference is assigned
		//! to the AutoPtr it is managed only by the AutoPtr. The AutoPtr deletes the object if he
		//! is deleted. Special care should be taken when copying or assigning the AutoPtr. The 
		//! AutoPtr which serves as destination from the copy or assignment will receive the ownership
		//! of the object, thus the source will lose them.
		//! Example:
		//! SomeThing p= new SomeThing();
		//!	AutoPtr<SomeThing> a(p);
		//! AutoPtr<SomeThing> b(a);
		//! a -> lost reference to object p -> a.get() == NULL
		//! b -> has new ownership of p -> a.get() == p
		//!
		//! This is necessary to maintain a clear ownership of the reference to the object. Thus no
		//! two AutoPtr objects can exist with the same reference.
		//! Avoid things like this:
		//! SomeThing p= new SomeThing();
		//! AutoPtr<SomeThing> a(p);
		//! AutoPtr<SomeThing> b(a.get());
		//! 
		//! At the end of the function we will get an error, since we try to delete object p two times.
		template<typename T_PTR, typename CP, typename OP>
		class SharedPtr;

		template<typename T_HOLD>
		class AutoPtr
		{
			template<typename T_PTR, typename CP, typename OP>
			friend class SharedPtr;

		public:
			typedef	T_HOLD	PointerType;

			inline AutoPtr()
				: mPtr(NULL)
			{}


			//! \brief init constructor from a pointer
			//! \param ptr		pointer which should be managed by the AutoPtr object
			inline explicit AutoPtr(T_HOLD* ptr)
				: mPtr(ptr)
			{}

			inline AutoPtr(const AutoPtr<T_HOLD>& autoPtr)
			{
				mPtr= autoPtr.mPtr;
				const_cast<AutoPtr<T_HOLD>&>(autoPtr).mPtr= NULL;
			}

			AutoPtr& operator=(T_HOLD* ptr)
			{
				destroy();

				mPtr= ptr;
				return *this;
			}

			inline AutoPtr& operator=(const AutoPtr<T_HOLD>& autoPtr)
			{
				destroy();

				mPtr= autoPtr.mPtr;

				const_cast<AutoPtr<T_HOLD>&>(autoPtr).mPtr= NULL;
				return *this;
			}
			
			inline ~AutoPtr()
			{
				destroy();
			}

			inline T_HOLD& operator*() const
			{
				return *mPtr;
			}

			inline T_HOLD* operator->() const
			{
				return mPtr;
			}

			inline bool operator!() const
			{
				return mPtr == NULL;
			}

			//! \brief get the referenced object
			//! \returns the referenced object, or NULL if nothing referenced
			inline T_HOLD* get() const
			{
				return mPtr;
			}

			//! \brief release the ownership of the referenced object
			//! \returns the referenced object, or NULL if nothing referenced
			inline T_HOLD* release()
			{
				PointerType* retVal= mPtr;
				mPtr= NULL;

				return retVal;
			}

			//! \brief force destruction of the referenced object
			inline void destroy()
			{
				if (mPtr) 
					delete mPtr;
				mPtr= NULL;
			}

		private:
			T_HOLD*			mPtr;
		};
	}
}
