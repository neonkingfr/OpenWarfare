// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/io/FileException.h>


namespace XAIT
{
	namespace Common
	{
		namespace IO
		{
			//! \brief exception used than an file exist where it shouldn't
			class FileExistException : public FileException
			{
			public:
				//! \brief constructor
				//! \param fileName		name of the file that already exists
				//! \param fullFileName	full name of the file which raised the exception (with path name)
				//! \param errorMsg		message which describes this error in its context
				FileExistException(const Common::String& fileName, const Common::String& fullFileName, const Common::String& errorMsg)
					: FileException(fileName,fullFileName,errorMsg)
				{}

				virtual Common::String getExceptionName() const
				{
					return "FileExistException";
				}
			};
		}
	}
}



