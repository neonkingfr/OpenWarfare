// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/io/DirectoryException.h>

namespace XAIT
{
	namespace Common
	{
		namespace IO
		{
			class DirectoryNotEmptyException : public DirectoryException
			{
			public:
				DirectoryNotEmptyException(const Common::String& dirName, const Common::String& errorMsg)
					: DirectoryException(dirName,errorMsg)
				{}

				virtual Common::String getExceptionName() const
				{
					return "DirectoryNotEmptyException";
				}
			};
		}
	}
}

