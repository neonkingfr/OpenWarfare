// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/String.h>

namespace XAIT
{
	namespace Common
	{
		namespace IO
		{
			class CommonPath
			{
			public:
				
				//! \brief normalize full path name (pathname WITH filenames)
				//! \param path		path to normalize
				//! \returns normalized path
				//! \remark This makes a path kind of comparable, where dir separators
				//!			will be made unique with '/', no trailing slash will be added
				//!			since the last argument is a file
				XAIT_COMMON_DLLAPI static Common::String normalizeFullPath(const Common::String& path);
			};
		}
	}
}
