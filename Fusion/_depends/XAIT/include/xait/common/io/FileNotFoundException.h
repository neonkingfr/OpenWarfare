// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/io/FileException.h>

namespace XAIT
{
	namespace Common
	{
		namespace IO
		{
			class FileNotFoundException : public FileException
			{
			public:
				//! \brief constructor
				//! \param fileName		name of the file that is not found
				//! \param fullFileName	full name of the file which raised the exception (with path name)
				//! \param errorMsg		message which describes this error in its context
				FileNotFoundException(const Common::String& fileName, const Common::String& fullFileName, const Common::String& errorMsg)
					: FileException(fileName,fullFileName,errorMsg)
				{}

				virtual Common::String getExceptionName() const
				{
					return "FileNotFoundException";
				}
			};
		}
	}
}



