// (C) xaitment GmbH 2006-2012

#pragma once 

namespace XAIT
{
	namespace Common
	{
		namespace IO
		{
			//! \brief stream id is written in front of binary stream to identify stream
			//! \remarks Always written as 1 byte
			enum BinaryStreamID
			{
				CONT_STREAMID_STREAM_DICTIONARY				= 0x01,
				CONT_STREAMID_SERIALIZE_STORAGE				= 0x02,
				CONT_STREAMID_BASECLASS_MASK				= 0x0F,

				CONT_STREAMID_PACKED_STREAM_DICTIONARY		= 0x11,
				CONT_STREAMID_BINARY_SERIALIZE_STORAGE		= 0x12,
			};
		}
	}
}

