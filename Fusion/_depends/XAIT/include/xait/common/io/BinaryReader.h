// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/serialize/SerializeStorage.h>
#include <xait/common/platform/Endian.h>
#include <xait/common/container/LookUp.h>
#include <xait/common/container/SegmentVector.h>
#include <xait/common/serialize/BinarySerializeStorage.h>

#define XAIT_STREAM_READ_VALUE(value,stream) \
			{ \
			stream->readValue(value); \
			} \

namespace XAIT
{
	namespace Common
	{
		namespace IO
		{
			class BinaryReader : public Memory::MemoryManaged
			{
			public:
			
				//! \brief create a binary reader
				XAIT_COMMON_DLLAPI BinaryReader(IO::Stream* stream, const Platform::Endian::Endiness endiness= (Platform::Endian::Endiness)XAIT_ARCH_ENDIAN);
				XAIT_COMMON_DLLAPI ~BinaryReader();

				XAIT_COMMON_DLLAPI SharedPtr<Serialize::SerializeStorage::Position> getPosition();
				XAIT_COMMON_DLLAPI void setPosition(const SharedPtr<Serialize::SerializeStorage::Position>& storagePos);

				XAIT_COMMON_DLLAPI void readValue(bool& value);
				XAIT_COMMON_DLLAPI void readValue(uint08& value);
				XAIT_COMMON_DLLAPI void readValue(int08& value);
				XAIT_COMMON_DLLAPI void readValue(uint16& value);
				XAIT_COMMON_DLLAPI void readValue(int16& value);
				XAIT_COMMON_DLLAPI void readValue(uint32& value);
				XAIT_COMMON_DLLAPI void readValue(int32& value);
				XAIT_COMMON_DLLAPI void readValue(uint64& value);
				XAIT_COMMON_DLLAPI void readValue(int64& value);
				XAIT_COMMON_DLLAPI void readValue(float32& value);
				XAIT_COMMON_DLLAPI void readValue(float64& value);
				XAIT_COMMON_DLLAPI void readValue(Common::Math::Vec2f& value);
				XAIT_COMMON_DLLAPI void readValue(Common::Math::Vec3f& value);
				XAIT_COMMON_DLLAPI void readValue(Common::BString& value);
				XAIT_COMMON_DLLAPI void readValue(Common::WString& value);
				
			protected:
				IO::Stream*													mStream;
				Platform::Endian::Endiness									mEndiness;			//!< endiness which should be used during streaming

				//! \brief read some element unit from the stream
				//! \param val	value which should be read
				template<typename T>
				inline void readFromStream(T& val)
				{
					mStream->read(&val,sizeof(T));
					Platform::Endian::convertToSystem(val,val,mEndiness);
				}
				
				template<typename T>
				inline void readFromStream(T* val, const uint32 numElements)
				{
					if (needEndianConversion())
					{
						for(uint32 i= 0; i < numElements; ++i)
						{
							mStream->read(val[i]);
							Platform::Endian::swapBytes(val[i],val[i]);
						}
					}
					else
					{
						mStream->read(val,numElements * sizeof(T));
					}
				}

				inline bool needEndianConversion() const { return mEndiness != Platform::Endian::getSystemEncoding(); }
			};
		}
	}
}
