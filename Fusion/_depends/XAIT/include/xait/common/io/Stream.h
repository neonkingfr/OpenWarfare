// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/FundamentalTypes.h>
#include <xait/common/memory/MemoryManaged.h>
#include <xait/common/String.h>

namespace XAIT
{
	namespace Common
	{
		namespace IO
		{
			class Stream : public Memory::MemoryManaged
			{
			public:
				Stream()
				{}	// null implementation for debugging reasons

				//! \brief origin of a seek
				enum SeekOrigin
				{
					SEEKORIG_CUR     = 1,		//!< seek starts at the current position
					SEEKORIG_END     = 2,		//!< seek starts at the end of the stream
					SEEKORIG_START   = 3,		//!< seek starts at the beginning of the stream
				};

				virtual ~Stream() {};

				//! \brief can we read from the stream
				//! \returns true if we can read from the stream, false otherwise
				virtual bool canRead() const= 0;

				//! \brief can we write to the stream
				//! \returns true if we can write to the stream, false otherwise
				virtual bool canWrite() const= 0;

				//! \brief can we seek on the stream
				//! \returns true if we can seek on the stream, false otherwise
				virtual bool canSeek() const= 0;

				//! \brief is the stream closed
				//! \returns true if the stream is closed or false otherwise
				virtual bool isClosed() const= 0;

				//! \brief get the length in bytes of the stream
				//! \returns the length of the stream in bytes
				//! \exception InvalidStateException	Stream is closed
				virtual uint64 getLength() const= 0;

				//! \brief gets the current read/write position in the stream
				//! \returns the current read/write position in the stream
				//! \exception InvalidStateException	Stream is closed
				virtual uint64 getPosition() const= 0;

				//! \brief get the name of the stream
				//! \remark This could be the filename of a file stream
				virtual const Common::String& getName() const= 0;

				//! \brief read some bytes from the stream into an external buffer
				//! \param dstBuffer	buffer which should receive the read bytes
				//! \param numBytes		number of bytes we should read from the stream
				//! \returns the number of actual bytes read, this can be less than numBytes.
				//! \remark throws exceptions if i/o error, wrong parameter, not readable
				//! \exception InvalidStateException	stream is closed
				//! \exception NotSupportedException	stream does not support reading
				//! \exception ArgumentNullException	dstBuffer is NULL
				virtual uint64 read(void* dstBuffer, const uint64 numBytes)= 0;

				//! \brief read binary data from the stream for a certain type
				//! \param dstVar		variable which should be read
				//! \param returns true if the data could be completely read, false otherwise
				//! \remark This does NO endian conversion and it does not read complex types correctly.
				//!			Use the serialize system to do that !
				template<typename T>
				inline bool read(T& dstVar)
				{
					return (read(&dstVar,sizeof(T)) == sizeof(T));
				}

				//! \brief write some bytes from a buffer to the stream
				//! \param srcBuffer	buffer which we should write into the stream
				//! \param numBytes		number of bytes which we should write from the buffer
				//! \remark throws exception if disk full, wrong parameter, not writable
				//! \exception InvalidStateException	stream is closed
				//! \exception NotSupportedException	stream does not support writing
				//! \exception ArgumentNullException	srcBuffer is NULL
				//! \exception OutOfSpaceException		not enough space in the stream to hold the bytes from srcBuffer
				virtual void write(const void* srcBuffer, const uint64 numBytes)= 0;

				//! \brief write binary data to the stream for a certain type
				//! \param srcVar		variable which should be written
				//! \remark throws exception if disk full, wrong parameter, not writable
				//! \remark This does NO endian conversion and it does not write complex types correctly.
				//!			Use the serialize system to do that !
				template<typename T>
				inline void write(const T& srcVar)
				{
					write(&srcVar,sizeof(T));
				}

				//! \brief seek to a specified position in the stream
				//! \param offset		number of bytes we want to seek
				//! \param origin		position in the stream, where we want to start the seek
				//! \returns the current position from the start of the stream
				//! \remark throws exception if wrong parameter, not seekable
				//! \exception InvalidStateException		stream is closed
				//! \exception NotSupportedException		stream does not support seeking
				//! \exception ArgumentOutOfRangeException	offset or origin lead outside of the stream
				virtual uint64 seek(const int64 offset, const SeekOrigin origin)= 0;

				//! \brief clears all buffers used in the stream
				//! \exception InvalidStateException	stream is closed
				virtual void flush()= 0;

				//! \brief close the stream
				virtual void close()= 0;

				//! \brief write from another stream into this stream
				//! \exception InvalidStateException	stream is closed
				//! \exception NotSupportedException	stream does not support writing
				//! \exception ArgumentNullException	srcBuffer is NULL
				//! \exception OutOfSpaceException		not enough space in the stream to hold the bytes from srcBuffer
				XAIT_COMMON_DLLAPI void writeFromStream(Stream* src, const uint64 numBytes);

				//! \brief write complete stream into this stream
				XAIT_COMMON_DLLAPI void writeFromStream(Stream* src);

				//! \brief read into another stream 
				//! \exception InvalidStateException	stream is closed
				//! \exception NotSupportedException	stream does not support reading
				//! \exception ArgumentNullException	dstBuffer is NULL
				XAIT_COMMON_DLLAPI void readToStream(Stream* dst, const uint64 numBytes);

				//! \brief read complete stream into other stream
				XAIT_COMMON_DLLAPI void readToStream(Stream* dst);
			};

		}
	}
}
