// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/String.h>

namespace XAIT
{
	namespace Common
	{
		namespace IO
		{
			class Path
			{
			public:
				//! \brief set the current working directory
				//! \param dirName		directory to which we want to change
				//! \exception DirectoryNotFoundException	thrown if directory is invalid
				static void setCurrentWorkingDir(const Common::String& dirName);

				//! \brief get the current working directory
				//! \returns the current working directory
				static Common::String getCurrentWorkingDir();

				//! \brief get the filename from a path string
				//! \param path		path where we should get the file name from
				//! \returns the file name with its extension
				static Common::String getFileName(const Common::String& path);

				//! \brief get the filename from a path string without the file extension
				//! \param path		path where we should get the file name from
				//! \returns the file name without its extension
				static Common::String getFileNameWithoutExtension(const Common::String& path);

				//! \brief get filename extension
				//! \param path		path where we should get the file extension from
				//! \returns the extension of the filename 
				//! \remark the extension is without the leading '.'
				static Common::String getFileExtension(const Common::String& path);

				//! \brief get the full path of a partial path
				//! \param path		partial path where we should create the full path
				//! \returns the full path
				//! \remark If a partial path is specified, this function uses the current
				//!			working directory as offset.
				static Common::String getFullPath(const Common::String& path);

				//! \brief get the directory name from the path string
				//! \param path		path where we should extract the directory name
				//! \returns the directory name from the path string
				//! \remark includes trailing slash (not normalized)
				static Common::String getDirectoryName(const Common::String& path);

				//! \brief normalize directory name
				//! \param dirName		dirName to normalize
				//! \returns normalized path
				//! \remark This makes a path kind of comparable, where dir separators
				//!			will be made unique with '/', and a trailing slash will be added if necessary
				static Common::String normalizeDirectoryName(const Common::String& dirName);

				//! \brief normalize full path name (pathname WITH filenames)
				//! \param path		path to normalize
				//! \returns normalized path
				//! \remark This makes a path kind of comparable, where dir separators
				//!			will be made unique with '/', no trailing slash will be added
				//!			since the last argument is a file
				static Common::String normalizeFullPath(const Common::String& path);

				//! \brief build a relative path for a path to a certain location
				//! \param path				path which should be made relative
				//! \param relativeToDir	a directory name to which the path should be relative (can also be a relative path to the current working dir)
				//! \exception PathException		thrown if the path cannot be made relative, because the paths are from different drives
				//! \exception ArgumentException	thrown if relativeToDir is not a directory name
				static Common::String makeRelativePath(const Common::String& path, const Common::String& relativeToDir);
			};
		}
	}
}
