#include "primitivesmanager.h"

#include <d3d9.h>
#include <d3dx9.h>

#define VERTEX_FORMAT (D3DFVF_XYZ | D3DFVF_DIFFUSE)

struct VertexD3D
{
  float x,y,z;
  DWORD color;
};

//  -====================================================-
//                      PRIMITIVE3D
//  -====================================================-

Primitive3D::Primitive3D(int size)
{
  _vertices = new BI_Vector3[size];
  _nVertices = size;
}

Primitive3D::~Primitive3D()
{
  if (_vertices)
    delete [] _vertices;
}

void Primitive3D::SetColor(const BI_Color& color)
{
  _color = color;
}


//  -====================================================-
//                        TRIANGLE3D
//  -====================================================-

Triangle3D::Triangle3D(const BI_Vector3& v1, const BI_Vector3& v2, const BI_Vector3& v3) : Primitive3D(3)
{
  _vertices[0] = v1;
  _vertices[1] = v2;
  _vertices[2] = v3;
}

void Triangle3D::AddTrianglesToBuffer(char* buffer) const
{
  for (int i = 0; i < 3; i++)
  {
    VertexD3D vertex;
    int size = sizeof(VertexD3D);
    vertex.color = D3DCOLOR_COLORVALUE(_color.R(), _color.G(), _color.B(), _color.A());
    vertex.x = _vertices[i][0];
    vertex.y = _vertices[i][1];
    vertex.z = _vertices[i][2];
    
    memcpy(buffer, &vertex, size);
    buffer += size;
  }
}

//  -====================================================-
//                   PRIMITIVES MANAGER
//  -====================================================-

PrimitivesManager::PrimitivesManager()
{
  _d3dd = NULL;
}

PrimitivesManager::~PrimitivesManager()
{
  _primitives.clear();
}


void PrimitivesManager::Add(SmartRef<Primitive3D> primitive)
{
  _primitives.push_back(primitive);
}

void PrimitivesManager::Add(std::vector<SmartRef<Primitive3D>>& primitives)
{
  std::vector<SmartRef<Primitive3D>>::iterator it;
  for (it = primitives.begin(); it != primitives.end(); it++)
  {
    _primitives.push_back(*it);
  }
}

void PrimitivesManager::SetNearFarPlane(float zNear, float zFar)
{
  _frustum._clipDistFar = zFar;
  _frustum._clipDistNear = zNear;
}

void PrimitivesManager::SetCustomTransform(const BI_Matrix4& transform)
{
  _transform = transform;
}

void PrimitivesManager::Draw()
{
  if (_d3dd && _primitives.size() > 0)
  {
    // Store current device state
    IDirect3DStateBlock9 *sblock;
    if(_d3dd->CreateStateBlock(D3DSBT_ALL, &sblock) != D3D_OK)
      return;
    sblock->Capture();

    // Set device to default state
    SetDefaultState();
    
    // Create projection matrix from engine frustum data
    D3DXMATRIX proj;
    float m = _frustum._clipDistNear;
    float left = _frustum._projTanLeft * m;
    float right = _frustum._projTanRight * m;
    float bottom = _frustum._projTanBottom * m;
    float top = _frustum._projTanTop * m;  
    D3DXMatrixPerspectiveOffCenterLH(&proj, -left, right, -bottom, top, _frustum._clipDistNear, _frustum._clipDistFar);
    _d3dd->SetTransform(D3DTS_PROJECTION, &proj);

    // Create view matrix from engine camera data
    D3DXMATRIX view;  
    D3DXVECTOR3 cpoint = D3DXVECTOR3(_frustum._pointPosX, _frustum._pointPosY, _frustum._pointPosZ);
    D3DXVECTOR3 ctarget = cpoint + D3DXVECTOR3(_frustum._viewDirX, _frustum._viewDirY, _frustum._viewDirZ); // Convert camera direction vector to look-at-target
    D3DXVECTOR3 cup = D3DXVECTOR3(_frustum._viewUpX, _frustum._viewUpY, _frustum._viewUpZ);
    D3DXMatrixLookAtLH(&view, &cpoint, &ctarget, &cup);
    _d3dd->SetTransform(D3DTS_VIEW, &view);

    /**********************************************
    We don't need light now since we use emissive color
    If you need to use lights uncomment this code.
    ***********************************************/
    // Set a basic light source
    //D3DLIGHT9 light;
    //ZeroMemory(&light, sizeof(light));
    //light.Type = D3DLIGHT_DIRECTIONAL;
    //light.Diffuse = D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
    //light.Ambient = D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
    //light.Direction = D3DXVECTOR3(-0.5, -1, -0.5);
    //_d3dd->SetLight(0, &light);
    //_d3dd->LightEnable(0, TRUE);
    
    // Prepare for drawing
    //_d3dd->SetRenderState(D3DRS_AMBIENT, D3DCOLOR_XRGB(128, 128, 128));
    //_d3dd->SetRenderState(D3DRS_LIGHTING, TRUE);
    /************************************************/
    _d3dd->SetRenderState(D3DRS_ZENABLE, TRUE);
    _d3dd->SetVertexShader(NULL);
    _d3dd->SetPixelShader(NULL);  
    _d3dd->SetTexture(0, 0);

    // Create vertex buffer
    int nTriangles = NTriangles();
    
    _d3dd->SetFVF(VERTEX_FORMAT);
    LPDIRECT3DVERTEXBUFFER9 pPrimitiveVB = NULL;
    VOID* pData;

    _d3dd->CreateVertexBuffer(sizeof(VertexD3D) * nTriangles * 3, D3DUSAGE_WRITEONLY, VERTEX_FORMAT, D3DPOOL_MANAGED, &pPrimitiveVB, NULL);

    //lock buffer
    pPrimitiveVB->Lock(0, 0, (void**)&pData, 0);

    //copy data to buffer
    std::vector<SmartRef<Primitive3D>>::iterator it;
    char* buffer = (char*)pData;
    for (it = _primitives.begin(); it != _primitives.end(); it++)
    {
      (*it)->AddTrianglesToBuffer(buffer);
      buffer += sizeof(VertexD3D) * (*it)->NTriangles() * 3;
    }

    //unlock buffer
    pPrimitiveVB->Unlock();

    //set source
    _d3dd->SetStreamSource(0,pPrimitiveVB,0,sizeof(VertexD3D));

    // set orientation matrix
    D3DXMATRIX transform;
    D3DXMatrixIdentity(&transform);

    for (int j = 0; j < 3; j++)
    {
      for (int i = 0; i < 3; i++)
      {
        float& val = transform(j, i);
        val = _transform[i][j];
      }
      transform(3, j) = _transform.GetPos()[j];
    }

    _d3dd->SetTransform(D3DTS_WORLD, &transform);

    //draw vertex buffer
    _d3dd->DrawPrimitive(D3DPT_TRIANGLELIST, 0, nTriangles);

    pPrimitiveVB->Release();

    sblock->Apply();
    sblock->Release();
  }
  _primitives.clear();
  _transform = BI_Matrix4(1,0,0,0,1,0,0,0,1,0,0,0);
}

int PrimitivesManager::NTriangles()
{
  int count = 0;  
  std::vector<SmartRef<Primitive3D>>::iterator it;
  for (it = _primitives.begin(); it != _primitives.end(); it++)
  {
    count += (*it)->NTriangles();
  }
  return count;
}

void PrimitivesManager::SetFrustum(FrustumSpec* frustum)
{
  memcpy(&_frustum, frustum, sizeof(FrustumSpec));
}

void PrimitivesManager::SetDevice(IDirect3DDevice9* device)
{
  _d3dd = device;
}

void PrimitivesManager::SetDefaultState()
{
  // Set D3D device properties to their default state
  // This is to ensure things are in a predictable state when coming from engine rendering code
  // TODO: This would be more efficient if it would be captured in a state block
  float zerof = 0.0f;
  float onef = 1.0f;
  #define ZEROf	*((DWORD*) (&zerof))
  #define ONEf	*((DWORD*) (&zerof))

  _d3dd->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
  _d3dd->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
  _d3dd->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_GOURAUD);
  _d3dd->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
  _d3dd->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
  _d3dd->SetRenderState(D3DRS_LASTPIXEL, TRUE);
  _d3dd->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
  _d3dd->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ZERO);
  _d3dd->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
  _d3dd->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
  _d3dd->SetRenderState(D3DRS_ALPHAREF, 0);
  _d3dd->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_ALWAYS);
  _d3dd->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
  _d3dd->SetRenderState(D3DRS_FOGENABLE, FALSE);
  _d3dd->SetRenderState(D3DRS_SPECULARENABLE, FALSE);
  _d3dd->SetRenderState(D3DRS_FOGCOLOR, 0);
  _d3dd->SetRenderState(D3DRS_FOGTABLEMODE, D3DFOG_NONE);
  _d3dd->SetRenderState(D3DRS_FOGSTART, ZEROf);
  _d3dd->SetRenderState(D3DRS_FOGEND, ONEf);
  _d3dd->SetRenderState(D3DRS_FOGDENSITY, ONEf);
  _d3dd->SetRenderState(D3DRS_RANGEFOGENABLE, FALSE);
  _d3dd->SetRenderState(D3DRS_STENCILENABLE, FALSE);
  _d3dd->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_KEEP);
  _d3dd->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_KEEP);
  _d3dd->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_KEEP);
  _d3dd->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_ALWAYS);
  _d3dd->SetRenderState(D3DRS_STENCILREF, 0);
  _d3dd->SetRenderState(D3DRS_STENCILMASK, 0xffffffff);
  _d3dd->SetRenderState(D3DRS_STENCILWRITEMASK, 0xffffffff);
  _d3dd->SetRenderState(D3DRS_TEXTUREFACTOR, 0xffffffff);
  _d3dd->SetRenderState(D3DRS_WRAP0, 0);
  _d3dd->SetRenderState(D3DRS_WRAP1, 0);
  _d3dd->SetRenderState(D3DRS_WRAP2, 0);
  _d3dd->SetRenderState(D3DRS_WRAP3, 0);
  _d3dd->SetRenderState(D3DRS_WRAP4, 0);
  _d3dd->SetRenderState(D3DRS_WRAP5, 0);
  _d3dd->SetRenderState(D3DRS_WRAP6, 0);
  _d3dd->SetRenderState(D3DRS_WRAP7, 0);
  _d3dd->SetRenderState(D3DRS_CLIPPING, TRUE);
  _d3dd->SetRenderState(D3DRS_AMBIENT, 0);
  _d3dd->SetRenderState(D3DRS_LIGHTING, 0);
  _d3dd->SetRenderState(D3DRS_FOGVERTEXMODE, D3DFOG_NONE);
  _d3dd->SetRenderState(D3DRS_COLORVERTEX, TRUE);
  _d3dd->SetRenderState(D3DRS_LOCALVIEWER, TRUE);
  _d3dd->SetRenderState(D3DRS_NORMALIZENORMALS, FALSE);
  _d3dd->SetRenderState(D3DRS_DIFFUSEMATERIALSOURCE, D3DMCS_COLOR1);
  _d3dd->SetRenderState(D3DRS_SPECULARMATERIALSOURCE, D3DMCS_COLOR2);
  _d3dd->SetRenderState(D3DRS_AMBIENTMATERIALSOURCE, D3DMCS_MATERIAL);
  _d3dd->SetRenderState(D3DRS_EMISSIVEMATERIALSOURCE, D3DMCS_MATERIAL);
  _d3dd->SetRenderState(D3DRS_CLIPPLANEENABLE, 0);
  _d3dd->SetRenderState(D3DRS_POINTSIZE_MIN, ONEf);
  _d3dd->SetRenderState(D3DRS_POINTSPRITEENABLE, FALSE);
  _d3dd->SetRenderState(D3DRS_MULTISAMPLEANTIALIAS, TRUE);
  _d3dd->SetRenderState(D3DRS_MULTISAMPLEMASK, 0xffffffff);
  _d3dd->SetRenderState(D3DRS_PATCHEDGESTYLE, D3DPATCHEDGE_DISCRETE);
  _d3dd->SetRenderState(D3DRS_POINTSIZE_MAX, ONEf);
  _d3dd->SetRenderState(D3DRS_COLORWRITEENABLE, 0x0000000f);
  _d3dd->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
  _d3dd->SetRenderState(D3DRS_POSITIONDEGREE, D3DDEGREE_CUBIC);
  _d3dd->SetRenderState(D3DRS_NORMALDEGREE, D3DDEGREE_LINEAR);
  _d3dd->SetRenderState(D3DRS_SCISSORTESTENABLE, FALSE);
  _d3dd->SetRenderState(D3DRS_SLOPESCALEDEPTHBIAS, 0);
  _d3dd->SetRenderState(D3DRS_MINTESSELLATIONLEVEL, ONEf);
  _d3dd->SetRenderState(D3DRS_MAXTESSELLATIONLEVEL, ONEf);
  _d3dd->SetRenderState(D3DRS_ADAPTIVETESS_X, ZEROf);
  _d3dd->SetRenderState(D3DRS_ADAPTIVETESS_Y, ZEROf);
  _d3dd->SetRenderState(D3DRS_ADAPTIVETESS_Z, ONEf);
  _d3dd->SetRenderState(D3DRS_ADAPTIVETESS_W, ZEROf);
  _d3dd->SetRenderState(D3DRS_ENABLEADAPTIVETESSELLATION, FALSE);
  _d3dd->SetRenderState(D3DRS_TWOSIDEDSTENCILMODE, FALSE);
  _d3dd->SetRenderState(D3DRS_CCW_STENCILFAIL, D3DSTENCILOP_KEEP);
  _d3dd->SetRenderState(D3DRS_CCW_STENCILZFAIL, D3DSTENCILOP_KEEP);
  _d3dd->SetRenderState(D3DRS_CCW_STENCILPASS, D3DSTENCILOP_KEEP);
  _d3dd->SetRenderState(D3DRS_CCW_STENCILFUNC, D3DCMP_ALWAYS);
  _d3dd->SetRenderState(D3DRS_COLORWRITEENABLE1, 0x0000000f);
  _d3dd->SetRenderState(D3DRS_COLORWRITEENABLE2, 0x0000000f);
  _d3dd->SetRenderState(D3DRS_COLORWRITEENABLE3, 0x0000000f);
  _d3dd->SetRenderState(D3DRS_BLENDFACTOR, 0xffffffff);
  _d3dd->SetRenderState(D3DRS_SRGBWRITEENABLE, 0);
  _d3dd->SetRenderState(D3DRS_DEPTHBIAS, 0);
  _d3dd->SetRenderState(D3DRS_WRAP8, 0);
  _d3dd->SetRenderState(D3DRS_WRAP9, 0);
  _d3dd->SetRenderState(D3DRS_WRAP10, 0);
  _d3dd->SetRenderState(D3DRS_WRAP11, 0);
  _d3dd->SetRenderState(D3DRS_WRAP12, 0);
  _d3dd->SetRenderState(D3DRS_WRAP13, 0);
  _d3dd->SetRenderState(D3DRS_WRAP14, 0);
  _d3dd->SetRenderState(D3DRS_WRAP15, 0);
  _d3dd->SetRenderState(D3DRS_SEPARATEALPHABLENDENABLE, FALSE);
  _d3dd->SetRenderState(D3DRS_SRCBLENDALPHA, D3DBLEND_ONE);
  _d3dd->SetRenderState(D3DRS_DESTBLENDALPHA, D3DBLEND_ZERO);
  _d3dd->SetRenderState(D3DRS_BLENDOPALPHA, D3DBLENDOP_ADD);
  _d3dd->SetRenderState(D3DRS_DITHERENABLE, FALSE);
  _d3dd->SetRenderState(D3DRS_VERTEXBLEND, D3DVBF_DISABLE);
  _d3dd->SetRenderState(D3DRS_POINTSIZE, ONEf);
  _d3dd->SetRenderState(D3DRS_POINTSCALEENABLE, FALSE);
  _d3dd->SetRenderState(D3DRS_POINTSCALE_A, ONEf);
  _d3dd->SetRenderState(D3DRS_POINTSCALE_B, ZEROf);
  _d3dd->SetRenderState(D3DRS_POINTSCALE_C, ZEROf);
  _d3dd->SetRenderState(D3DRS_INDEXEDVERTEXBLENDENABLE, FALSE);
  _d3dd->SetRenderState(D3DRS_TWEENFACTOR, ZEROf);
  _d3dd->SetRenderState(D3DRS_ANTIALIASEDLINEENABLE, FALSE);

  for(int i=0;i<4;i++) 
  {
    _d3dd->SetSamplerState(i, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
    _d3dd->SetSamplerState(i, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
    _d3dd->SetSamplerState(i, D3DSAMP_MIPFILTER, D3DTEXF_NONE);
    _d3dd->SetSamplerState(i, D3DSAMP_MIPMAPLODBIAS, 0);
    _d3dd->SetSamplerState(i, D3DSAMP_MAXMIPLEVEL, 0);			
    _d3dd->SetSamplerState(i, D3DSAMP_MAXANISOTROPY, 1);
    _d3dd->SetSamplerState(i, D3DSAMP_SRGBTEXTURE, 0);			
    _d3dd->SetSamplerState(i, D3DSAMP_ELEMENTINDEX, 0);
    _d3dd->SetSamplerState(i, D3DSAMP_DMAPOFFSET, 0);			
    _d3dd->SetSamplerState(i, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
    _d3dd->SetSamplerState(i, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
    _d3dd->SetSamplerState(i, D3DSAMP_ADDRESSW, D3DTADDRESS_WRAP);		
  }

  for(int i=0;i<4;i++) 
  {
    _d3dd->SetTextureStageState(i, D3DTSS_TEXCOORDINDEX, i);
    _d3dd->SetTextureStageState(i, D3DTSS_COLOROP, i==0?D3DTOP_MODULATE:D3DTOP_DISABLE);
    _d3dd->SetTextureStageState(i, D3DTSS_COLORARG1, D3DTA_TEXTURE);
    _d3dd->SetTextureStageState(i, D3DTSS_COLORARG2, D3DTA_CURRENT);
    _d3dd->SetTextureStageState(i, D3DTSS_ALPHAOP, i==0?D3DTOP_SELECTARG1:D3DTOP_DISABLE);
    _d3dd->SetTextureStageState(i, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
    _d3dd->SetTextureStageState(i, D3DTSS_ALPHAARG2, D3DTA_CURRENT);
    _d3dd->SetTextureStageState(i, D3DTSS_BUMPENVMAT00, ZEROf);
    _d3dd->SetTextureStageState(i, D3DTSS_BUMPENVMAT01, ZEROf);
    _d3dd->SetTextureStageState(i, D3DTSS_BUMPENVMAT10, ZEROf);
    _d3dd->SetTextureStageState(i, D3DTSS_BUMPENVMAT11, ZEROf);
    _d3dd->SetTextureStageState(i, D3DTSS_BUMPENVLSCALE, ZEROf);
    _d3dd->SetTextureStageState(i, D3DTSS_BUMPENVLOFFSET, ZEROf);
    _d3dd->SetTextureStageState(i, D3DTSS_COLORARG0, D3DTA_CURRENT);
    _d3dd->SetTextureStageState(i, D3DTSS_ALPHAARG0, D3DTA_CURRENT);
    _d3dd->SetTextureStageState(i, D3DTSS_RESULTARG, D3DTA_CURRENT);
    _d3dd->SetTextureStageState(i, D3DTSS_CONSTANT, 0);
    _d3dd->SetTextureStageState(i, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_DISABLE);
  }

  D3DXMATRIX mat;
  D3DXMatrixIdentity(&mat);
  _d3dd->SetTransform(D3DTS_TEXTURE0, &mat);
  _d3dd->SetTransform(D3DTS_TEXTURE1, &mat);
  _d3dd->SetTransform(D3DTS_TEXTURE2, &mat);
  _d3dd->SetTransform(D3DTS_TEXTURE3, &mat);
}