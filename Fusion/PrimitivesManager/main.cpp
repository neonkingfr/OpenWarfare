#pragma warning(disable: 4996)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// VBS2Fusion interface library forced linking
// Needed for plugins that don't use any of VBS2Fusion symbols
// This will insure that VBS2Fusion_2005 library will be linked to the plugin
//  and the plugin will require to have VBS2Fusion_2005.dll
// It is required by Simcentric that each Fusion plugin links with VBS2Fusion_2005.lib or VBS2_Fusion_2008.lib.
#include "VBS2FusionAppContext.h"
VBS2Fusion::VBS2FusionAppContext appContext;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <windows.h>
#include "vbsplugin.h"

#include "primitivesmanager.h"
#include "../../Common/Essential/RefCount.hpp"
#include <vector>

PrimitivesManager primitivesManager;

std::vector<SmartRef<Primitive3D>> _pluginTriangles;

// This function will be executed every simulation step (every frame) and took a part in the simulation procedure.
// We can be sure in this function the ExecuteCommand registering was already done.
// deltaT is time in seconds since the last simulation step
VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
}


//This is the actual drawing function
VBSPLUGIN_EXPORT void WINAPI OnDrawnPassTwo(float zNear, float zFar)
{
  primitivesManager.SetNearFarPlane(zNear, zFar);
  
  //send all the triangles into the drawing manager
  std::vector<SmartRef<Primitive3D>>::iterator it;
  for (it = _pluginTriangles.begin(); it != _pluginTriangles.end(); it++)
  {
    primitivesManager.Add(*it);
  }

  //draw the geometry sent into the manager
  primitivesManager.Draw();
}

VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
  //example input: 
  //  pluginFunction("PrimitivesManager","add [0,0,0],[1,1,0],[-1,1,0],[1,1,1,1]");
  //    creates 1 triangle
  //  pluginFunction("PrimitivesManager","delete all");
  //    deletes all triangles 

  static const char resultDone[]="[true]";
  static const char resultError[]="[error]";

  if (stricmp(input, "delete all") == 0)
  {
    _pluginTriangles.clear();
  }
  else
  {
    float x1 = 0,x2 = 0,x3 = 0,y1 = 0,y2 = 0,y3 = 0,z1 = 0,z2 = 0,z3 = 0;
    float r = 0, g = 0, b = 0, a = 0;
    int parsed = sscanf(input, "add [%f,%f,%f],[%f,%f,%f],[%f,%f,%f],[%f,%f,%f,%f]", &x1, &z1, &y1, &x2, &z2, &y2, &x3, &z3, &y3, &r, &g, &b, &a);
    if (parsed != 13) return resultError;

    BI_Vector3 v1(x1, y1, z1);
    BI_Vector3 v2(x2, y2, z2);
    BI_Vector3 v3(x3, y3, z3);

    SmartRef<Primitive3D> triangle = new Triangle3D(v1, v2, v3);
    triangle->SetColor(BI_Color(r,g,b,a));

    _pluginTriangles.push_back(triangle);
  }

  return resultDone;
}


// D3D device management, after a reset
// Re-create data for Direct3D device here
VBSPLUGIN_EXPORT void WINAPI OnD3DeviceRestore()
{
}

// D3D device management, before a reset
// All handles created on the Direct3D device must be freed here - otherwise VBS2 device reset will fail
VBSPLUGIN_EXPORT void WINAPI OnD3DeviceInvalidate()
{
}

// Onload - get the d3d device
VBSPLUGIN_EXPORT int WINAPI OnLoad(VBSParameters *params) 
{
  if (params->_device)
    primitivesManager.SetDevice(params->_device);
  OnD3DeviceRestore();
  return 0;
}


VBSPLUGIN_EXPORT void WINAPI OnUnload() 
{
  primitivesManager.SetDevice(NULL);
  OnD3DeviceInvalidate();
}

// D3D device management, setting the engine D3D device
VBSPLUGIN_EXPORT void WINAPI OnD3DeviceInit(IDirect3DDevice9 *device)
{
  primitivesManager.SetDevice(device);
}

// D3D device management, removing the engine D3D device: 
VBSPLUGIN_EXPORT void WINAPI OnD3DeviceDelete()
{
  primitivesManager.SetDevice(NULL);
}

VBSPLUGIN_EXPORT void WINAPI OnSetFrustum(FrustumSpec *frustum)
{
  primitivesManager.SetFrustum(frustum);
};

BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
  switch(fdwReason)
  {
  case DLL_PROCESS_ATTACH:
    OutputDebugString("DLL_PROCESS_ATTACH");
    break;
  case DLL_PROCESS_DETACH:
    OutputDebugString("DLL_PROCESS_DETACH");
    break;
  }
  return TRUE;
}
