#define VBSPLUGIN_EXPORT __declspec(dllexport)

// Command function declaration
typedef int (WINAPI *ExecuteCommandType)(const char *command, char *result, int resultLength);



VBSPLUGIN_EXPORT void WINAPI RegisterHWND(void *hwnd, void *hins);
VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc);
VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT);
VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input);

// VID Register tyepdef's
typedef float (WINAPI *GETVIDVALUE)(UINT devID);
typedef UINT  (WINAPI *REGISTERVID)(const char *pluginName, UINT devID, UINT type, const char *devName, GETVIDVALUE getVIDValue);

VBSPLUGIN_EXPORT void WINAPI PollVIDDevices();
VBSPLUGIN_EXPORT void WINAPI RegisterVIDFunc(REGISTERVID regVID);