#include <windows.h>
#include <d3d9.h> 
#include <d3dx9.h> 
#include <dinput.h> 
#include <fstream> 
#include "VirtualInputDevice.h"



#define KEYBOARD        0
#define GAMECONTROLLERS 1

#if KEYBOARD
// Keyboard input device.
LPDIRECTINPUTDEVICE8 GKeybDevice;

// DirectInput keyboard state buffer 
BYTE GDIKS[256];
#endif

#if GAMECONTROLLERS
// Define just 10 game devices for simplicity
#define MAX_GAME_DEV 10
int nDevices;
#endif

//! It is assumed that all game controllers
//! will be polled based on their
//! button,axis, and pov. Each one of these are iterated, based off
//! the device capabilities
HWND GWND;
HINSTANCE GHINSTANCE;


// Global instance of the DirectXInput
LPDIRECTINPUT8 GDirectXInputInstance;


void Notify()
{
}

#if GAMECONTROLLERS
// Input device
LPDIRECTINPUTDEVICE8 GDevices[MAX_GAME_DEV];

// Each input capabilities
DIDEVCAPS GDevCap[MAX_GAME_DEV];

// Each game controllers state
DIJOYSTATE2 GGameController[MAX_GAME_DEV];


enum InputType
{
  devButton,
  devAxis,
  devPOV,
  unknown
};
struct InputOnGameDevice
{
  // Device raw pointer.
  LPDIRECTINPUTDEVICE8 _device;
  // Registered device ID.
  UINT _devId;
  //! Type identifiers the device input as either, button,axis, pov.
  InputType _type;
  //! Input pointer just points to the DIJOYSTATE2 structure for joysticks.
  void *_input;
  //! Sign of value, used for axis
  bool _posAxis;

  InputOnGameDevice()
  {
    _posAxis = true;
    _device = NULL;
    _input  = NULL;
    _type   = unknown;
    _devId  = 0;
  }

  float GetValue()
  {
    float value = 0.0f;

    switch (_type)
    {
      case devButton:
        {
          BYTE *input = (BYTE*) _input;
          value = *input/128 ? 1.0f : 0.0f; 
        }
        break;
      case devAxis:
        {
          LONG *input = (LONG*) _input;
          value = (*input) * 0.001;

          // The axis is split with actions between - and + values.
          if (_posAxis && value < 0) value = 0.0f;
          if (!_posAxis && value > 0) value = 0.0f;

          // All axis need to to be absolute for actions.
          value = abs(value);
        }
        break;
      case devPOV:
        {
          DWORD *input = (DWORD*) _input;
          value = 0.0f;
        }
        break;
    }

    return value;
  }
};

// Quick hack to just ignore the concept of a input device...
// instead its one giant input device
int nInputDevices;
InputOnGameDevice GInputDevices[100];

#endif

// Command function definition
ExecuteCommandType ExecuteCommand = NULL;
VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc)
{
  ExecuteCommand = (ExecuteCommandType)executeCommandFnc;
}

VBSPLUGIN_EXPORT void WINAPI OnAfterSimulation(float deltaT)
{
}

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
}

VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
  return "";
}

void InitDirectXInput()
{
  HRESULT hr = DirectInput8Create(GHINSTANCE, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&GDirectXInputInstance, NULL);
  if (FAILED(hr))
  {
    OutputDebugString("Failed to create direct input 8...\n");
    Notify();
    GDirectXInputInstance = NULL;
  }
}

#if KEYBOARD
void InitializeKeyboard(HWND han_Window)
{
  HRESULT hr;
  hr = GDirectXInputInstance->CreateDevice(GUID_SysKeyboard, &GKeybDevice, NULL);
  if (FAILED(hr))
  {
    OutputDebugString("Failed to create keyboard device\n");
    Notify();
    return;
  }

  hr = GKeybDevice->SetDataFormat(&c_dfDIKeyboard);
  if (FAILED(hr))
  {
    OutputDebugString("Failed to to set data format\n");
    Notify();
    return;
  }

  hr = GKeybDevice->SetCooperativeLevel(han_Window, DISCL_EXCLUSIVE|DISCL_FOREGROUND);
  if (FAILED(hr))
  {
    OutputDebugString("Failed to set Cooperative level\n");
    Notify();
    return;
  }

  hr = GKeybDevice->Acquire();
  if (FAILED(hr))
  {
    OutputDebugString("Failed to acquire keyboard device\n");
    Notify();
    return;
  }
}

void UnInitializeKeyboard()
{
  if (GKeybDevice) GKeybDevice->Release();
}
#endif

#if GAMECONTROLLERS
void UnInitializeGameControllers()
{
  for (int i=0; i<nDevices;i++) GDevices[i]->Release();
}



BOOL CALLBACK EnumAxesCallback(const DIDEVICEOBJECTINSTANCE* instance, VOID* context)
{
  DIPROPRANGE propRange; 
  propRange.diph.dwSize       = sizeof(DIPROPRANGE); 
  propRange.diph.dwHeaderSize = sizeof(DIPROPHEADER); 
  propRange.diph.dwHow        = DIPH_BYID; 
  propRange.diph.dwObj        = instance->dwType;
  propRange.lMin              = -1000; 
  propRange.lMax              = +1000; 

  // Set the range for the axis
  GDevices[nDevices]->SetProperty(DIPROP_RANGE, &propRange.diph);

  return DIENUM_CONTINUE;
}


BOOL CALLBACK GameControllerCallback(const DIDEVICEINSTANCE* instance, VOID* context)
{
  HRESULT hr;

  // Obtain an interface to the enumerated joystick.
  hr = GDirectXInputInstance->CreateDevice(instance->guidInstance, &GDevices[nDevices], NULL);

  // Determine how many axis the joystick has (so we don't error out setting
  // properties for unavailable axis)
  GDevCap[nDevices].dwSize = sizeof(DIDEVCAPS);

  // Initialize the keyboard with all the necessary data format, cooperation and capabilities. 
  // Then Enumerate the axe's of the joystick to set the lmin, and lmax ranges.
  if 
  (    FAILED(hr)
    || FAILED(hr = GDevices[nDevices]->SetDataFormat(&c_dfDIJoystick2))
    || FAILED(hr = GDevices[nDevices]->SetCooperativeLevel(GWND, DISCL_EXCLUSIVE|DISCL_FOREGROUND))
    || FAILED(hr = GDevices[nDevices]->GetCapabilities(&GDevCap[nDevices]))
    || FAILED(hr = GDevices[nDevices]->EnumObjects(EnumAxesCallback, NULL, DIDFT_AXIS))
  )
  {
    // Make sure we reset the devices to null
    GDevices[nDevices] = NULL;
    Notify();
    return DIENUM_CONTINUE;
  }

  // If at any point during initialization there was a problem, back out and do not use the controller
  if (GDevices[nDevices]) nDevices++;
  
  // Continue enumerating all the Game Controllers
  return DIENUM_CONTINUE;
}


void InitializeGameControllers(HWND han_Window)
{
  GDirectXInputInstance->EnumDevices(DI8DEVCLASS_GAMECTRL,GameControllerCallback,NULL, DIEDFL_ATTACHEDONLY);
}
#endif


//////////////////////////////////////////////////////////////////////////
// Interface to the VID VBS2 Start
//////////////////////////////////////////////////////////////////////////
const char *GPluginID = "MySuperPlugin";
// Typedef callback function
REGISTERVID GRegisterVID;

void WINAPI PollVIDDevices()
{
#if KEYBOARD
  // Clear all the dik's states.
  ZeroMemory( GDIKS, sizeof( GDIKS ) );

  if (GKeybDevice)
  {  
    // Get the input's device state, and put the state in dims
    HRESULT hr = GKeybDevice->GetDeviceState( sizeof( GDIKS ), GDIKS );
    if( FAILED( hr ) )
    {
      // If input is lost then acquire and keep trying 
      do 
      {
        hr = GKeybDevice->Acquire();
      } while( hr == DIERR_INPUTLOST );

      switch (hr)
      {
        case DIERR_OTHERAPPHASPRIO:
          OutputDebugString("Keyboard another app has a higher priority level, preventing this call from succeeding");
        break;
        case DIERR_NOTACQUIRED:
          OutputDebugString("Keyboard the operation cannot be performed unless the device is acquired");
        break;
      }
    }
  }
#endif

#if GAMECONTROLLERS
  /*
    Polling the game devices is pretty straight forward
   */
  for (int i=0; i<nDevices; i++)
  {
    LPDIRECTINPUTDEVICE8 device = GDevices[i];
    if (!device) continue;
    
    // Poll the device.
    HRESULT hr = device->Poll();
    if (FAILED(hr))
    {
      // Try to re-acquire the device.
      do 
      {
        hr = device->Acquire();
      } while( hr == DIERR_INPUTLOST );

      switch (hr)
      {
        case DIERR_OTHERAPPHASPRIO:
          OutputDebugString("GameController another app has a higher priority level, preventing this call from succeeding");
          break;
        case DIERR_NOTACQUIRED:
          OutputDebugString("GameController the operation cannot be performed unless the device is acquired");
          break;
      }
    }

    device->GetDeviceState(sizeof(DIJOYSTATE2), &GGameController[i]);
  }
#endif
}

#if KEYBOARD
// Do a quick look-up to see which key press has occurred.
float WINAPI GetKeyboardValue(UINT devID)
{
  if (GDIKS[devID]/128) return 1.0f;
  return 0.0f;
}
#endif

#if GAMECONTROLLERS
float WINAPI GetGameControllerValue(UINT devID)
{
  for (int i=0; i<nInputDevices; i++)
    if (GInputDevices[i]._devId==devID) return GInputDevices[i].GetValue();

  return 0.0f;
}
#endif


// DLL|Fusion exports the following C function
// Engine calls this if available when the dll is loaded
void WINAPI RegisterVIDFunc(REGISTERVID regVID)
{ 
  GRegisterVID = regVID;

#if KEYBOARD
  //! Register all the keyboard DIK's
  for (int i=0; i<256; i++)
  {
    const char *GetDIKString(UINT dikCode);
    const char *key = GetDIKString(i);
    if (!key) continue;

    // Register the dik codes.
    GRegisterVID(GPluginID,i,0,key,GetKeyboardValue);
  }
#endif

#if GAMECONTROLLERS
  int counterDevID = 300;
  for (int i=0; i<nDevices; i++)
  {
    BYTE *btnPtr = GGameController[i].rgbButtons;
    for (int x=0; x<GDevCap[i].dwButtons;x++)
    {
      char buffer[256] = {0};
      sprintf(buffer,"GDev:%d Button:%d",i,x);
      
      GInputDevices[nInputDevices]._device = GDevices[i];
      GInputDevices[nInputDevices]._devId = counterDevID++;
      GInputDevices[nInputDevices]._type = devButton;
      GInputDevices[nInputDevices]._input = btnPtr++;
      
      GRegisterVID(GPluginID,GInputDevices[nInputDevices]._devId,0,buffer,GetGameControllerValue);
      nInputDevices++;
    }

    /*
     Because of the nature with axis, we register everything
    */
    #define MAX_AXIS 8
    const char *AxisNames[MAX_AXIS] = 
    {
      "x-axis pos",
      "y-axis pos",
      "z-axis pos",
      "x-axis rot",
      "y-axis rot",
      "z-axis rot",
      "extra axis 1",
      "extra axis 2"
    };
    for (int p=0; p<2; p++)
    {
      LONG *axis = &GGameController[i].lX;
      for (int x=0; x<MAX_AXIS;x++)
      {
        char buffer[256] = {0};
        if (p)
          sprintf(buffer,"GDev:%d %s-",i,AxisNames[x]);
        else
          sprintf(buffer,"GDev:%d %s+",i,AxisNames[x]);

        GInputDevices[nInputDevices]._device = GDevices[i];
        GInputDevices[nInputDevices]._devId = counterDevID++;
        GInputDevices[nInputDevices]._type = devAxis;
        GInputDevices[nInputDevices]._input = axis++;
        GInputDevices[nInputDevices]._posAxis = p ? true : false;

        GRegisterVID(GPluginID,GInputDevices[nInputDevices]._devId,1,buffer,GetGameControllerValue);
        nInputDevices++;
      }
    }
  }
#endif
}

//////////////////////////////////////////////////////////////////////////
// VID VBS2 end
//////////////////////////////////////////////////////////////////////////

VBSPLUGIN_EXPORT void WINAPI RegisterHWND(void *hwnd, void *hins)
{
  GWND = (HWND) hwnd;

  InitDirectXInput();
  if (!GDirectXInputInstance)
  {
    OutputDebugString("Failed to create DirectInput instance\n");
    Notify();
    return;
  }

#if KEYBOARD
  InitializeKeyboard((HWND)GWND);
#endif
#if GAMECONTROLLERS
  InitializeGameControllers((HWND)GWND);
#endif
}


BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
  GHINSTANCE = hDll;

  switch(fdwReason)
  {
  case DLL_PROCESS_ATTACH:
    OutputDebugString("Called DllMain with DLL_PROCESS_ATTACH\n");
    break;
  case DLL_PROCESS_DETACH:
    OutputDebugString("Called DllMain with DLL_PROCESS_DETACH\n");
#if KEYBOARD
    UnInitializeKeyboard();
#endif
#if GAMECONTROLLERS
    UnInitializeGameControllers();
#endif
    break;
  case DLL_THREAD_ATTACH:
    OutputDebugString("Called DllMain with DLL_THREAD_ATTACH\n");
    break;
  case DLL_THREAD_DETACH:
    OutputDebugString("Called DllMain with DLL_THREAD_DETACH\n");
    break;
  }
  return TRUE;
}

#define DIK_TO_STRING(ABC) case ABC : return #ABC;

const char *GetDIKString(UINT dikCode)
{
 switch(dikCode)
 {
    DIK_TO_STRING(DIK_ESCAPE)
    DIK_TO_STRING(DIK_1)
    DIK_TO_STRING(DIK_2)
    DIK_TO_STRING(DIK_3)
    DIK_TO_STRING(DIK_4)
    DIK_TO_STRING(DIK_5)
    DIK_TO_STRING(DIK_6)
    DIK_TO_STRING(DIK_7)
    DIK_TO_STRING(DIK_8)
    DIK_TO_STRING(DIK_9)
    DIK_TO_STRING(DIK_0)
    DIK_TO_STRING(DIK_MINUS)
    DIK_TO_STRING(DIK_EQUALS)
    DIK_TO_STRING(DIK_BACK)
    DIK_TO_STRING(DIK_TAB)
    DIK_TO_STRING(DIK_Q)
    DIK_TO_STRING(DIK_W)
    DIK_TO_STRING(DIK_E)
    DIK_TO_STRING(DIK_R)
    DIK_TO_STRING(DIK_T)
    DIK_TO_STRING(DIK_Y)
    DIK_TO_STRING(DIK_U)
    DIK_TO_STRING(DIK_I)
    DIK_TO_STRING(DIK_O)
    DIK_TO_STRING(DIK_P)
    DIK_TO_STRING(DIK_LBRACKET)
    DIK_TO_STRING(DIK_RBRACKET)
    DIK_TO_STRING(DIK_RETURN)
    DIK_TO_STRING(DIK_LCONTROL)
    DIK_TO_STRING(DIK_A)
    DIK_TO_STRING(DIK_S)
    DIK_TO_STRING(DIK_D)
    DIK_TO_STRING(DIK_F)
    DIK_TO_STRING(DIK_G)
    DIK_TO_STRING(DIK_H)
    DIK_TO_STRING(DIK_J)
    DIK_TO_STRING(DIK_K)
    DIK_TO_STRING(DIK_L)
    DIK_TO_STRING(DIK_SEMICOLON)
    DIK_TO_STRING(DIK_APOSTROPHE)
    DIK_TO_STRING(DIK_GRAVE)
    DIK_TO_STRING(DIK_LSHIFT)
    DIK_TO_STRING(DIK_BACKSLASH)
    DIK_TO_STRING(DIK_Z)
    DIK_TO_STRING(DIK_X)
    DIK_TO_STRING(DIK_C)
    DIK_TO_STRING(DIK_V)
    DIK_TO_STRING(DIK_B)
    DIK_TO_STRING(DIK_N)
    DIK_TO_STRING(DIK_M)
    DIK_TO_STRING(DIK_COMMA)
    DIK_TO_STRING(DIK_PERIOD)
    DIK_TO_STRING(DIK_SLASH)
    DIK_TO_STRING(DIK_RSHIFT)
    DIK_TO_STRING(DIK_MULTIPLY)
    DIK_TO_STRING(DIK_LMENU)
    DIK_TO_STRING(DIK_SPACE)
    DIK_TO_STRING(DIK_CAPITAL)
    DIK_TO_STRING(DIK_F1)
    DIK_TO_STRING(DIK_F2)
    DIK_TO_STRING(DIK_F3)
    DIK_TO_STRING(DIK_F4)
    DIK_TO_STRING(DIK_F5)
    DIK_TO_STRING(DIK_F6)
    DIK_TO_STRING(DIK_F7)
    DIK_TO_STRING(DIK_F8)
    DIK_TO_STRING(DIK_F9)
    DIK_TO_STRING(DIK_F10)
    DIK_TO_STRING(DIK_NUMLOCK)
    DIK_TO_STRING(DIK_SCROLL)
    DIK_TO_STRING(DIK_NUMPAD7)
    DIK_TO_STRING(DIK_NUMPAD8)
    DIK_TO_STRING(DIK_NUMPAD9)
    DIK_TO_STRING(DIK_SUBTRACT)
    DIK_TO_STRING(DIK_NUMPAD4)
    DIK_TO_STRING(DIK_NUMPAD5)
    DIK_TO_STRING(DIK_NUMPAD6)
    DIK_TO_STRING(DIK_ADD)
    DIK_TO_STRING(DIK_NUMPAD1)
    DIK_TO_STRING(DIK_NUMPAD2)
    DIK_TO_STRING(DIK_NUMPAD3)
    DIK_TO_STRING(DIK_NUMPAD0)
    DIK_TO_STRING(DIK_DECIMAL)
    DIK_TO_STRING(DIK_OEM_102)
    DIK_TO_STRING(DIK_F11)
    DIK_TO_STRING(DIK_F12)
    DIK_TO_STRING(DIK_F13)
    DIK_TO_STRING(DIK_F14)
    DIK_TO_STRING(DIK_F15)
    DIK_TO_STRING(DIK_KANA)
    DIK_TO_STRING(DIK_ABNT_C1)
    DIK_TO_STRING(DIK_CONVERT)
    DIK_TO_STRING(DIK_NOCONVERT)
    DIK_TO_STRING(DIK_YEN)
    DIK_TO_STRING(DIK_ABNT_C2)
    DIK_TO_STRING(DIK_NUMPADEQUALS)
    DIK_TO_STRING(DIK_PREVTRACK)
    DIK_TO_STRING(DIK_AT)
    DIK_TO_STRING(DIK_COLON)
    DIK_TO_STRING(DIK_UNDERLINE)
    DIK_TO_STRING(DIK_KANJI)
    DIK_TO_STRING(DIK_STOP)
    DIK_TO_STRING(DIK_AX)
    DIK_TO_STRING(DIK_UNLABELED)
    DIK_TO_STRING(DIK_NEXTTRACK)
    DIK_TO_STRING(DIK_NUMPADENTER)
    DIK_TO_STRING(DIK_RCONTROL)
    DIK_TO_STRING(DIK_MUTE)
    DIK_TO_STRING(DIK_CALCULATOR)
    DIK_TO_STRING(DIK_PLAYPAUSE)
    DIK_TO_STRING(DIK_MEDIASTOP)
    DIK_TO_STRING(DIK_VOLUMEDOWN)
    DIK_TO_STRING(DIK_VOLUMEUP)
    DIK_TO_STRING(DIK_WEBHOME)
    DIK_TO_STRING(DIK_NUMPADCOMMA)
    DIK_TO_STRING(DIK_DIVIDE)
    DIK_TO_STRING(DIK_SYSRQ)
    DIK_TO_STRING(DIK_RMENU)
    DIK_TO_STRING(DIK_PAUSE)
    DIK_TO_STRING(DIK_HOME)
    DIK_TO_STRING(DIK_UP)
    DIK_TO_STRING(DIK_PRIOR)
    DIK_TO_STRING(DIK_LEFT)
    DIK_TO_STRING(DIK_RIGHT)
    DIK_TO_STRING(DIK_END)
    DIK_TO_STRING(DIK_DOWN)
    DIK_TO_STRING(DIK_NEXT)
    DIK_TO_STRING(DIK_INSERT)
    DIK_TO_STRING(DIK_DELETE)
    DIK_TO_STRING(DIK_LWIN)
    DIK_TO_STRING(DIK_RWIN)
    DIK_TO_STRING(DIK_APPS)
    DIK_TO_STRING(DIK_POWER)
    DIK_TO_STRING(DIK_SLEEP)
    DIK_TO_STRING(DIK_WAKE)
    DIK_TO_STRING(DIK_WEBSEARCH)
    DIK_TO_STRING(DIK_WEBFAVORITES)
    DIK_TO_STRING(DIK_WEBREFRESH)
    DIK_TO_STRING(DIK_WEBSTOP)
    DIK_TO_STRING(DIK_WEBFORWARD)
    DIK_TO_STRING(DIK_WEBBACK)
    DIK_TO_STRING(DIK_MYCOMPUTER)
    DIK_TO_STRING(DIK_MAIL)
    DIK_TO_STRING(DIK_MEDIASELECT)
  }
  return NULL;
}
