/****************************************************************************
* Fusion plug-in for exporting geometry from given rectangle to file.       *
*                                                                           *
* You need to have Fusion headers and libs in                               *
*  w:\v\SimCentric\Fusion2\VBS2Fusion\includes                              *
*  w:\v\SimCentric\Fusion2\VBS2Fusion\libs                                  *
* You can get them from                                                     * 
*  https//prague.bisimulations.com:475/svn/builds/trunk/SimCentric/Fusion2  *
*                                                                           *
* The output file is binary and contains faces information in format:       *
*   vertices_count                                                          *
*   vertexX,vertexY,vertexZ,vertexX,vertexY,vertexZ,...                     *
*   indices_count                                                           *
*   index1,index2,index3,index4,index1,index2,index3,index4,...             *
* Vertex coordinate is of type float in world coordinates.                  *
* Index is of type integer.                                                 *
* If a face has only 3 vertices the 4th index is -1                         *  
*                                                                           *
*****************************************************************************/

#pragma warning(disable: 4996)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// VBS2Fusion interface library forced linking
// Needed for plugins that don't use any of VBS2Fusion symbols
// This will insure that VBS2Fusion_2005 library will be linked to the plugin
//  and the plugin will require to have VBS2Fusion_2005.dll
// It is required by Simcentric that each Fusion plugin links with VBS2Fusion_2005.lib or VBS2_Fusion_2008.lib.
#include "VBS2FusionAppContext.h"
VBS2Fusion::VBS2FusionAppContext appContext;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <string>
#include <vector>
#include <stdexcept>
#include <fstream>

#pragma warning (disable : 4251)
#pragma warning (disable : 4996)

#include "pluginHeader.h"

#include "BIMatrix.h"
#include <Common/GeometryObject.h>

// Function that will register the ExecuteCommand function of the engine
VBSPLUGIN_EXPORT void WINAPI RegisterGetMapGeomFnc(void *getMapGeometryFnc)
{
  GetMapGeometry = (GetMapGeometryType)getMapGeometryFnc;
}

// Function that will register the ExecuteCommand function of the engine
VBSPLUGIN_EXPORT void WINAPI RegisterRelMapGeomFnc(void *releaseGeometryFnc)
{
  ReleaseMapGeometry = (ReleaseMapGeometryType)releaseGeometryFnc;
}

/*!
Following function is called by the VBS2 engine in each an every frame

\param deltatT is an floating point argument which returns times
since the last call.
*/

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{	
};

// in - array[handle,time] & text , out - handle
void ExecuteDiagMessage(int &handle, int time, char* text)
{
  if(!text) {handle = -1; return;}

  // fill command line
  char commandBuff[256];
  sprintf(commandBuff,"[%d,%d] diagMessage \"%s\"",handle,time,text);

  char resultBuff[256];

  // send command
  if(!ExecuteCommand(commandBuff, resultBuff, 256))
  {//failed
    OutputDebugString(resultBuff);
    handle = -1; 
    return;
  }

  //parse to handle
  handle = atoi(resultBuff);
  return;
}

// use Format function to preformat if necessary
#define DIAG_MESSAGE(time,text) do \
{	\
  static int handle=-1; \
  ExecuteDiagMessage(handle, time, text); \
} while (false);

BI_Vector3 TransformVector(BI_Vector3 &vec, float trans[4][3])
{
  BI_Matrix4 transM(
    trans[0][0], trans[1][0], trans[2][0],
    trans[0][1], trans[1][1], trans[2][1],
    trans[0][2], trans[1][2], trans[2][2],
    trans[3][0], trans[3][1], trans[3][2]);
  return transM * vec;
}
/*!
The plugin function is called at the initialization of the plugin.

\param input is  an character which controls the execution of the
plugin
*/

VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
  static const char result[]="[\"MapGeometryPlugin done\"]";

  float begX = 0, begZ = 0, endX = 0, endZ = 0;
  int objectsCount = 0;
  int type = 0;

  char filePath[512];
  memset(filePath, 512, 0);
  //input parameter parsing
  sscanf(input, "%f,%f,%f,%f,%d,%s", &begX, &begZ, &endX, &endZ, &type, filePath);

  //calling VBS2 function for getting map geometry
  GeometryObject *objects = GetMapGeometry(begX, begZ, endX, endZ, objectsCount, type);
  if (objects)
  {
    char tmpBuff[512];
    sprintf(tmpBuff, "GetMapGeometry: [%.2f, %.2f, %.2f, %.2f] -> %i objects (type %d)", begX, begZ, endX, endZ, objectsCount, type);
    DIAG_MESSAGE(5000, tmpBuff);

    std::vector<BI_Vector3> vertices;
    std::vector<int> indices;

    int objectBase = 0;
    //create arrays of all vertices and indices.
    for (int i=0; i<objectsCount; i++)
    {
      for (int v=0; v<objects[i].nVertices; v++)
      {
        BI_Vector3 vertex(objects[i].vertices[v].x, objects[i].vertices[v].y, objects[i].vertices[v].z);
        vertices.push_back(TransformVector(vertex, objects[i].transform));
      }

      for (int f=0; f<objects[i].nFaces; f++)
      {
        for (int fi=0; fi<objects[i].faces[f].nIndices; fi++)
        {
          indices.push_back(objectBase+objects[i].faces[f].indices[fi]);
        }
        if (objects[i].faces[f].nIndices == 3)
          indices.push_back(-1);
      }

      objectBase += objects[i].nVertices;
    }

    std::string fileName(filePath); 
    //remove quotes on start and end of file name;
    fileName = fileName.substr(1, fileName.length()-2);
    _iobuf *fOut = fopen(fileName.data(), "w+b");
    if (fOut)
    {
      //vertices count
      int s = vertices.size();
      fwrite(&s, sizeof(int), 1, fOut);
      //vertices
      for (unsigned i=0; i<vertices.size(); i++)
      {
        float f = vertices[i].X();
        fwrite(&f, sizeof(float), 1, fOut);
        f = vertices[i].Y();
        fwrite(&f, sizeof(float), 1, fOut);
        f = vertices[i].Z();
        fwrite(&f, sizeof(float), 1, fOut);
      }

      s = indices.size();
      fwrite(&s, sizeof(int), 1, fOut);
      for (unsigned i=0; i<indices.size(); i++)
      {
        fwrite(&indices[i], sizeof(int), 1, fOut);
      }

      fclose(fOut);
    }
    else
    {
      sprintf(tmpBuff, "GetMapGeometry: Can't open file '%s'", filePath);
      DIAG_MESSAGE(5000, tmpBuff);
    }

    //once not needed anymore the geometry must be released
    if (ReleaseMapGeometry)
      ReleaseMapGeometry(objects);
  }
  else
  {
    DIAG_MESSAGE(5000, "GetMapGeometry returned NULL");
  }

  return result;
};

void log(const char *str)
{
  std::ofstream outputFile;
  outputFile.open("vbs2fusionjni.log",std::ofstream::app);
  outputFile << str << std::endl;
	outputFile.close();
}

/*!
Do not modify this code. Defines the DLL main code fragment
*/

BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
	switch(fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	}

	return TRUE;
};