#ifndef __BI_MATRIX__
#define __BI_MATRIX__

#include "BISdkCommon.h"
#include "BIVector.h"

/** 4D BI_Matrix consisting only of zeros 
*/
#define BI_ZeroMatrix4 BI_Matrix4 (  0,0,0,	\
   								   0,0,0,	\
							       0,0,0,	\
							       0,0,0)	

/** Identity 3D BI_Matrix with las column fillled with zeroes
*/
#define BI_IdentityMatrix4 BI_Matrix4(  1,0,0,	\
									  0,1,0,	\
									  0,0,1,	\
									  0,0,0)

/** Identity 3D BI_Matrix 
*/
#define BI_IdentityMatrix3 BI_Matrix3 (  1,0,0,	\
									   0,1,0,	\
									   0,0,1);

/* 3D BI_Matrix consisting only of zeros */
#define BI_ZeroMatrix3 BI_Matrix3 (  0,0,0,	\
								   0,0,0,	\
								   0,0,0)

/** \brief Class handling operation with 3D matrix */
/** Matrix is defined by axis up, direction and aside. 
 * Class cooperates with BI_Vectors
 */
class BI_Matrix3
{
	/** Columns of matrix 
	*/
	BI_Vector3 _columns[3];

	/** Rows is represented */
	typedef BI_Vector3 Row;
	typedef BI_Vector3 Column;

public:
	/** \brief Enum deciding where are located axis */
	//TODO obrazok axises
	enum Axis { _Aside, _Direction, _Up };

	/**  Constructor */
	/** Constructs Matrix from values. Every triple value in sequence represents column.
	 */
	BI_Matrix3(float x1 =0, float x2 =0, float x3 =0, float y1 =0, float y2 =0, float y3=0, float z1 =0, float z2 =0, float z3 =0)
	{
		Set(0, 0) = x1;		Set(0, 1) = x2;		Set(0, 2) = x3;
		Set(1, 0) = y1;		Set(1, 1) = y2;		Set(1, 2) = y3;
		Set(2, 0) = z1;		Set(2, 1) = z2;		Set(2, 2) = z3;
	}
	/** \brief Contructor via _columns */
	/** Constructs matrix when there are given columns
	 */
	BI_Matrix3(const BI_Vector3& column1, const BI_Vector3 & column2, const BI_Vector3 & column3)
	{
		memcpy( &_columns[0], &column1, sizeof(float)*3 ); 
		memcpy( &_columns[1], &column2, sizeof(float)*3 );
		memcpy( &_columns[2], &column3, sizeof(float)*3 );
	}
	/** \brief Copy contructor  */
	/** Copies all data from source
	 */
	BI_Matrix3(const BI_Matrix3 &src)	{ memcpy( _columns, src._columns, sizeof(BI_Vector3)*3); }

	/** \brief Access to element of matrix */
	/** \return reference to matrix element for further modifying
	 */
	float& Set(int row, int column) { return _columns[column][row]; }

	/** \brief Setting element value */
	/** Sets value at row and column. There is no return value and thus no further modifying 
	 */
	void Set(int row, int column, float value)
	{
		Set(row, column) = value;
	}

	/** \brief Setting element value */
	/** Sets value at row and column. There is no return value and thus no further modifying 
	 */
	float Get(int row, int column)const
	{
		return _columns[column][row];
	}
	/** \brief Sets identity */
	/** changes instance to identity matrix */
	void SetIdentity() 	{ *this = BI_IdentityMatrix3; }

	/** \brief Sets rotation in X axis */
	/** \param angle is angle between vector and X axis
	 */
	void SetRotationX( float angle )
	{
		SetIdentity();
		float s = sin(angle), c = cos(angle);
		Set(1,1)=+c,Set(1,2)=-s;
		Set(2,1)=+s,Set(2,2)=+c;
	}
	void SetScale(float x, float y, float z)
	{
		*this = BI_ZeroMatrix3;
		_columns[0][0] = x;
		_columns[1][1] = y;
		_columns[2][2] = z;
	}
	/** \brief Sets rotation in Y axis */
	/** \param angle is angle between vector and Y axis
	 */
	void SetRotationY( float angle )
	{
		SetIdentity();
		float s=(float)sin(angle),c=(float)cos(angle);
		Set(0,0)=+c,Set(0,2)=-s;
		Set(2,0)=+s,Set(2,2)=+c;
	}

	/** \brief Sets rotation in Z axis */
	/** \param angle is angle between vector and Z axis
	 */
	void SetRotationZ( float angle )
	{
		SetIdentity();
		float s=(float)sin(angle),c=(float)cos(angle);
		Set(0,0)=+c,Set(0,1)=-s;
		Set(1,0)=+s,Set(1,1)=+c;
	}

	/** \brief Sets rotation according to arbitrary axis */
	/** \param axis 
	 * \param angle is angle between vector and  
	 */
	void SetRotationByAxis(const BI_Vector3 & axis, float angle)
	{
		// Normalize input vector
		BI_Vector3 axisN = axis;
		axisN.Normalize();

		// Convert axis and angle into a quaternion (w, x, y, z)
		float halfAngle = angle * 0.5f;
		float w = cos(halfAngle);
		float sinHalfAngle = sin(halfAngle);
		float x = sinHalfAngle * axisN.X();
		float y = sinHalfAngle * axisN.Y();
		float z = sinHalfAngle * axisN.Z();

		// Convert the quaternion into the matrix
		float wx = w*x*2;
		float wy = w*y*2;
		float wz = w*z*2;
		float xx = x*x*2;
		float xy = x*y*2;
		float xz = x*z*2;
		float yy = y*y*2;
		float yz = y*z*2;
		float zz = z*z*2;

		Set(0, 0) = 1 - yy - zz;
		Set(0, 1) = xy - wz;
		Set(0, 2) = xz + wy;
		Set(1, 0) = xy + wz;
		Set(1, 1) = 1 - xx - zz;
		Set(1, 2) = yz - wx;
		Set(2, 0) = xz - wy;
		Set(2, 1) = yz + wx;
		Set(2, 2) = 1 - xx - yy;
	}


	/** \brief Transpose matrix  */
	/** no values are changed
	 * \return BI_Matrix that has onposition [i][j] the value, 
	 * that can be located on original matrix on location[j][i]
	 */
	BI_Matrix3 Transpose()const 
	{
		BI_Matrix3 m(*this);
		for (int i = 0; i < 3; i++)
			for ( int j =0; j < 3; j++)
			{
				m._columns[j][i] = _columns[i][j];
			}
			return m;
	}

	/** \brief Fetch column of matrix */
	/** \return BI_Vector3 representing column of the matrix
	 */
	const BI_Vector3 & GetColumn(int idx)const { return _columns[idx]; }
	BI_Vector3& GetColumn(int idx) { return _columns[idx]; }

	/** \brief Fetch rows of the matrix  */
	/** Since the inner representation disallows immediate row, we have to compute that
	 */
	BI_Vector3 GetRow(int idx)const { return BI_Vector3(_columns[0][idx],_columns[1][idx],_columns[2][idx]); }

	/** \brief Computes determinant */
	/** \return float result
	 */
	float Determinant()const
	{ 
		return Get(0,0)*( Get(1,1)*Get(2,2) - Get(1,2)*Get(2,1) )
			 + Get(0,1)*( Get(1,2)*Get(2,0) - Get(1,0)*Get(2,2) )
			 + Get(0,2)*( Get(1,0)*Get(2,1) - Get(1,1)*Get(1,0) );
	}

	/** \brief Checks, if the matrix can be inverted */
	 /** \return true, if the natrix is invertible, false otherwise
	 */
	bool Invertible()const {	return Determinant() != 0; }

	/** \brief Gets up axis */
	const BI_Vector3& GetUp()const { return _columns[_Up]; }

	/** \brief Gets up axis */
	BI_Vector3& GetUp() { return _columns[_Up]; }

	/** \brief Gets aside axis */
	const BI_Vector3& GetAside()const { return _columns[_Aside]; }
	BI_Vector3& GetAside() { return _columns[_Aside]; }

	/** \brief Gets direction axis */
	const BI_Vector3& GetDirection()const { return _columns[_Direction]; }
	BI_Vector3& GetDirection() { return _columns[_Direction]; }

	/** \brief Sets rotation matrix */
	void SetRotationAxis(const BI_Vector3 & axis, float angle)
	{
		// Normalize input vector
		BI_Vector3 axisN = axis;
		axisN.Normalize();

		// Convert axis and angle into a quaternion (w, x, y, z)
		float halfAngle = angle * 0.5f;
		float w = cos(halfAngle);
		float sinHalfAngle = sin(halfAngle);
		float x = sinHalfAngle * axisN.X();
		float y = sinHalfAngle * axisN.Y();
		float z = sinHalfAngle * axisN.Z();

		// Convert the quaternion into the matrix
		float wx = w*x*2;
		float wy = w*y*2;
		float wz = w*z*2;
		float xx = x*x*2;
		float xy = x*y*2;
		float xz = x*z*2;
		float yy = y*y*2;
		float yz = y*z*2;
		float zz = z*z*2;

		Set(0, 0) = 1 - yy - zz;
		Set(0, 1) = xy - wz;
		Set(0, 2) = xz + wy;
		Set(1, 0) = xy + wz;
		Set(1, 1) = 1 - xx - zz;
		Set(1, 2) = yz - wx;
		Set(2, 0) = xz - wy;
		Set(2, 1) = yz + wx;
		Set(2, 2) = 1 - xx - yy;
	}
	/** \brief Invert matrix */
	/** \return valid BI_Matrix3. If matrix is invertible, returns inverted matrix, 
	 * otherwise return null matrix, which seems to be invalid the same way like input...
	 */
	BI_Matrix3 Inverted() const
	{
		BI_Matrix3 m(*this);
		return m.Invert();
	};

	/** \brief Invert matrix */
	/** \return valid BI_Matrix3. If matrix is invertible, returns inverted matrix, 
	 * otherwise return null matrix, which seems to be invalid the same way like input...
	 */
	BI_Matrix3& Invert()
	{
		if (!Invertible())
		{
			(*this) = BI_ZeroMatrix3;
			return *this;
		}
		Row rv1(Get(1,1)* Get(2,2) - Get(1,2)*Get(2,1), Get(1,0)*Get(2,1) - Get(1, 1)* Get(1,0),Get(1,0)*Get(2,1) - Get(1,1)* Get(1,0));
		Row rv2(Get(1,2)*Get(2,0) - Get(1,0)*Get(2,2), Get(1,0)*Get(2,1) - Get(1,1)*Get(1,0),Get(1,0)*Get(2,1) - Get(1,1)*Get(1,0));
		Row rv3(Get(1,0)*Get(2,1) - Get(1,1)*Get(1,0), Get(1,0)*Get(2,1) - Get(1,1)*Get(1,0),Get(1,0)*Get(2,1) - Get(1,1)*Get(1,0));
		BI_Matrix3 inv(rv1,rv2,rv3);
		(*this) = inv/Determinant();
		return (*this);
	};

	/** \brief Computes sum of square distances in all direction*/
	float Distance2(const BI_Matrix3 &mat) const
	{
		return _columns[0].Distance2(mat._columns[0])+_columns[1].Distance2(mat._columns[1])+_columns[2].Distance2(mat._columns[2]);
	}

	/** \name operators on BI_Matrix3 alone */	
	/** \{ */
	/** \brief Adding to BI_Matrix3 */
	/** Values in BI_Matrix3 elements will be increased by corresponding elements from src
	 * \param src source matrix
	 * \return reference to this changed matrix
	 */
	BI_Matrix3 & operator+=(const BI_Matrix3& src)
	{
		for ( int i =0; i<3;i++)
				_columns[i] += src._columns[i];
		return *this;
	}

	/** \brief Operation plus */
	/** \return BI_Matrix3 whose elements are increased by source matrix
	 */
	const BI_Vector3& operator[](int idx)const  { return _columns[idx]; }
	BI_Matrix3 operator+(const BI_Matrix3& src)
	{
		BI_Matrix3 m(*this);
		m+=src;
		return m;
	}

	/** \brief Operation minus */
	/** \return BI_Matrix3 whose elements are decreased by source matrix
	 */
	BI_Matrix3 & operator-=(const BI_Matrix3& src)
	{
		for ( int i =0; i<3;i++)
				_columns[i] -= src._columns[i];
		return *this;
	}

	/** \brief Operation minus */
	/** \return BI_Matrix3 whose elements are decreased by source matrix
	 */
	BI_Matrix3 operator-(const BI_Matrix3& src)const
	{
		BI_Matrix3 m(*this);
		m-=src;
		return m;
	}
	
	/** \brief Multiplying matrix */
	/** \param src parameter that every element of matrix will be multiplied by
	 * \return reference to this changed matrix
	 */
	BI_Matrix3 & operator*=(const float & src)
	{
		for ( int i =0; i<3; i++)
				_columns[i]*=src;
		return *this;
	}

	/** \brief Operation multiply */
	/** \param f parameter that every element of the matrix will be multiplied by
	 * \return BI_Matrix3 whose elements are multiplied by src. BI_Matrix3 itself remains untouched
	 */
	BI_Matrix3 & operator*(const float & f)const
	{
		BI_Matrix3 m(*this);
		return m*=f;
	}
	/** \brief BI_Matrix3 multiplication */
	/** \param src source matrix
	  * \return changed matrix 
	  */
	BI_Matrix3 & operator*=( const BI_Matrix3 & src)
	{
		BI_Matrix3 hlp(*this);
		for ( int i = 0; i< 3; i++)
		{
			BI_Vector3 v = src.GetColumn(i);
			v = hlp*v;
			memcpy(&_columns[i],&v, sizeof(float)*3);
		}
		return (*this);
	}
	/** \brief BI_Matrix3 multiplication  */
	/** \param src source matrix
	  * \return changed matrix 
	  */
	BI_Matrix3 operator*(const BI_Matrix3 & src)const
	{
		BI_Matrix3 m(*this);
		m*=src;
		return m;
	}

	/** \brief Matrix multiplication by BI_Vector3 */
	/** \param v vector that matrix will be multiplicated by
	 */
	BI_Vector3 operator*(const BI_Vector3 & v)const
	{
		BI_Vector3 r1 = GetRow(0);
		BI_Vector3 r2 = GetRow(1);
		BI_Vector3 r3 = GetRow(2);
		return BI_Vector3(r1.DotProduct(v), r2.DotProduct(v), r3.DotProduct(v));//skalarny sucin vektorov
	}

	/** \brief  Division BI_Matrix3 &*/
	/** \param f parameter that divides every element of the matrix
	 * \return reference to this changed matrix
	 */
	BI_Matrix3 operator/=(const float & f)
	{
		float t = 1/f;
		return (*this)*t;
	}

	/** \brief Division BI_Matrix3 */
	/** \return matrix whose every element is divided by \param f
	 */
	BI_Matrix3 operator/(const float & f)const
	{
		BI_Matrix3 m(*this);
		return m*(1/f);
	}
	/** \} */
};

/** \brief 4D matrix with position */
/** This matric is similar to 3D matrix, but contains also another vector, which defined position */
class BI_Matrix4 : public BI_Matrix3
{
	typedef BI_Matrix3 Base;

protected:
	/** position of the matrix */
	BI_Vector3 _position;

public:
	/** \brief Contructor */
	BI_Matrix4(const BI_Matrix3& matrix, const BI_Vector3& pos) : Base(matrix), _position(pos) {}

	/** \brief Contructor */
	BI_Matrix4(float x1 = 0, float x2 = 0, float x3 = 0,
			float y1 = 0, float y2 =0, float y3 =0,
			float z1 =0 , float z2 =0, float z3 =0,
			float p1 =0, float p2 =0, float p3 =0) : Base(x1,x2,x3,y1,y2,y3,z1,z2,z3), _position(p1,p2,p3) {}

	/** \brief Gets position vector */
	/** \param i i<sup>th</sup> item if the position vector */
	float GetPos(int i) const { return _position[i]; }

	/** \brief Gets position coordinate vector */
	/** \param i i<sup>th</sup> item if the position vector */
	float & GetPos(int i) { return _position[i]; }

	/** \brief Set method */
	float& Set(int row, int column) 
	{  
		if (row == 3)
			return SetPos(column);
		else
			return BI_Matrix3::Set(row,column);
	}

	/** \brief Set cell */
	void Set(int row, int column, float value) 
	{
		Set(row,column) = value;
	}

	/** \brief Sets position coordinate vector */
	float& SetPos(int i) { return _position[i]; }
	
	/** \brief Gets cell */
	float Get(int row, int column)const
	{
		return row == 3? GetPos(row):BI_Matrix3::Get(row,column);
	}

	/** \brief Sets position offset*/
	void SetTranslation( const BI_Vector3 & offset ) { _position = offset; }

	/** \brief Matric multiplication accordin perspective */
	BI_Matrix4& MultiplyByPerspective(const BI_Matrix4 & m)
	{
		BI_Matrix4 hlp(*this);
		BI_Matrix3::operator *=(m);
		for ( int i =0; i < 3; i++ ) //set perspective
			Set(i,2) += m.GetPos(i);
		for( int i=0; i<3; i++ )
			SetPos(i) = ( hlp.Get(i,0)*m.GetPos(0) + hlp.Get(i,1)*m.GetPos(1) + hlp.Get(i,2)*m.GetPos(2));
		return *this;
	}

	/** \brief Gets position vector */
	const BI_Vector3& GetPos() const { return _position; }

	BI_Vector3& GetPos() { return _position; }

	/** \brief Count square distance in Matrix4*/
	float Distance4( const BI_Matrix4 & m ) const { return Distance2(m) + _position.Distance(m._position); }

	/** \name Operators */
	/* \{ */

	/** \brief Assign operator */
	BI_Matrix4 & operator += (const BI_Matrix4 & matrix) //we should inherit operator += for BIMatrix3
	{
		( BI_Matrix3 )(*this) +=  ( BI_Matrix3 ) matrix;
		_position += matrix._position;
		return *this;
	}
	/** \brief Plus operator */
	BI_Matrix4 operator+(const BI_Matrix4 & matrix) const
	{
		BI_Matrix4 m (*this);
		m += matrix;
		return m;
	}
	/** \brief Minus operator */
	BI_Matrix4 & operator -= (const BI_Matrix4 & matrix) 
	{
		( BI_Matrix3 )(*this) -= ( BI_Matrix3 ) matrix;
		_position -= matrix._position;
		return *this;
	}
	/** \brief Minus operator */
	BI_Matrix4 operator-( const BI_Matrix4 & matrix ) const
	{
		BI_Matrix4 m (*this);
		m -= matrix;
		return m;
	}
	/** \brief Multiplication by vector */
	BI_Vector3 operator*( const BI_Vector3& f )const
	{
		return BI_Matrix3::operator *(f) + _position;
	}
	/** \brief Multiplication operator */
	BI_Matrix4 & operator*=( const float f ) 
	{
		( BI_Matrix3 )(*this) *= f;
		_position *=f;
		return *this;
	}
	/** \brief Plus operator */
	BI_Matrix4 operator+( const float f )const
	{
		BI_Matrix4 m (*this);
		m *= f;
		return m;
	}
	/** \} */
};
#endif