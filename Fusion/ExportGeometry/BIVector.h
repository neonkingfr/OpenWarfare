#ifndef __BI_VECTOR3__
#define __BI_VECTOR3__

#include "BISDKCommon.h"
#include <math.h>

/** \brief BI_Vector handling UV positions */
/** 2D BI_Vector describing texture position. User should not need any other methods than Set/Get UV values */
class DLLEXTERN BI_UVVector
{
	/** \brief Coordinates of the position */
	float _coords[2]; 

	/* private enum determining access to part position, since we can forget that U-coordinate is at zero index */
	enum Access { _U, _V };

public:

	/** \brief Constructor UVVector */
	/** BI_Vector with defined coordinates x, y, z. If none are specified, null BI_Vector will be created */
	BI_UVVector(float x = 0.0f, float y = 0.0f)
	{
		_coords[_U] = x;
		_coords[_V] =y;
	}

	/** \brief Copy contructor */
	/** Copies all coordinates */
	BI_UVVector(const BI_UVVector & src)
	{
		memcpy(_coords,src._coords,sizeof(float)*2);
	}

	/**\brief Gets U method */
	/** \return float U coordinate */
	float U() const { return _coords[_U]; }

	/**\brief Gets V method */
	/** \return float V coordinate */
	float V() const  { return _coords[_V]; }

	/** \brief Sets new U coordinate */
	void SetU( float value ) { _coords[_U] = value; }

	/** \brief Sets new V coordinate */
	void SetV( float value ) { _coords[_V] = value; }

	/** \brief Sets vector coordinates */
	void Set(float u, float v)
	{
		SetU(u);
		SetV(v);
	}
	/** \brief Maximum coordinate from UVVector */
	float MaxCoord()const 	{ return BI_Max(_coords[0], _coords[1]);}
	
	/** \brief Minimum coordinate from UVVector */
	float MinCoord()const { return BI_Min(_coords[_U], _coords[_V]); }

	/** \brief Checking intersection */
	/** \return true if there is intersection on texture coordinated
	 * \return false otherwise
	 */
	static bool Intersects(const BI_UVVector & a1, const BI_UVVector a2, const BI_UVVector & b1, const BI_UVVector b2 )
	{
		BI_UVVector dirA = a2 - a1;
		BI_UVVector dirB = b2 - b1;
		//If they are parrallel..
		float d = dirB.U()*dirA.V() - dirA.U()*dirB.V();
		if (fabs(d) < 0.00000000001f) return false;//rovnobezne

		float difU = a1.U() - b1.U();
		float difV = a1.V() - b1.V();

		// smerova rovnice
        float dx = difU * dirA.V() - difV * dirA.U();
        float dy = - dirB.U() * difV + dirB.V() * difU; //TODO check this
        float x = dx/d;
        float y = dy/d;
        return (x>0.0001f && x<0.9999f && y>0.001f && y<0.9999f);

	}
	/** \name Operators */
	/** \{ */
	/** \brief  Assigment operator */
	/** operation of assigmens assignes new values, destroying old. There is no difference between copy 
	 * contructor and assigment operation except for return value */
	BI_UVVector & operator=( const BI_UVVector& src)
	{
		memcpy(_coords,src._coords,sizeof(float)*2);
		return * this;
	}
	/** \brief Access operator */
	/** \returns float coordinate whose representing number is idx */
	float& operator[](int idx) { return _coords[idx]; }

	/** \brief Access operator for constant access*/
	/** \returns float coordinate whose representing number is idx */
	float operator[](int idx) const { return _coords[idx]; }

	/** \brief Equality operator */
	bool operator==(const BI_UVVector& vct)const
	{
		return ( fabs(U() - vct.U())<1e-4 ) && (fabs(V() - vct.V())<1e-4);
	}
	/** \brief Non-equality operator */
	bool operator!=(const BI_UVVector& vct)const
	{
		return !(operator==(vct));
	}
	/** \brief Unary operator - */
	BI_UVVector operator-()
	{
		BI_UVVector hlp(*this);
		hlp *=-1;
		return hlp;
	}
	/** \brief Plus operator */
	BI_UVVector& operator+=(const BI_UVVector& vector)
	{
		_coords[_U]+= vector.U();
		_coords[_V]+= vector.V();
		return *this;
	}
	/** \brief Plus operator */
	BI_UVVector operator+(const BI_UVVector& vector)const
	{
		BI_UVVector vct(*this);
		return vct+=vector;
	}
	
	/** \brief Minus operator */
	BI_UVVector& operator-=(const BI_UVVector& vector)
	{
		_coords[_U]-= vector.U();
		_coords[_V]-= vector.V();
		return *this;
	}
	/** \brief Minus operator */
	BI_UVVector operator-(const BI_UVVector& vector)const
	{
		BI_UVVector v(*this);
		v-=vector;
		return v;
	}

	/** \brief Multiplication operator */
	BI_UVVector& operator*=(float f)
	{
		_coords[_U] *= f;
		_coords[_V] *= f;
		return *this;
	}
	/** \brief Multiplication operator */
	BI_UVVector operator*(float f)
	{
		BI_UVVector v(*this);
		return v*=f;
	}

	/** \brief Division by number operator */
	BI_UVVector& operator/=(float f)
	{
		float invf = 1/f;
		return *this*=invf;
	}
	/** \brief Division by number operator */
	BI_UVVector operator/(float f)
	{
		BI_UVVector v(*this);
		return v/=f;
	}
	/** \} */
};


/** \brief Class handling BI_Vector operation in 3D Euclidean space. */
/**	This class is base for normals and vertices. Vertices behave the same, 
 * but have moreover special flags 
 */
class BI_Vector3
{
protected:
	/** \brief Float coordinates of Vector in 3-dimensional space */
	float _coords[3];

public:
	/** \brief Enum for determining which number correspond with position in BI_Vector */
	enum Access { _X, _Y, _Z};
	
	/** \brief BI_Vector contructor*/
	/** Constructs BI_Vector with defined coordinates x, y, z
	 * Default coordinates values are zeroes
	 */
	BI_Vector3(float x = 0.0f, float y = 0.0f, float z = 0.0f) {	SetX(x); SetY(y); SetZ(z);	}

	/** \brief BI_Vector constructor */
	/** Creates a complete copy of source BI_Vector
	 */
	BI_Vector3( const BI_Vector3 & vector) {	memcpy(_coords, vector._coords, sizeof(float)*3);	}

	/** \brief BI_Vector constructor */
	/**Contructs BI_Vector from beginning point and ending point. 
	 * point is there represented as another BI_Vector an no vetrex,
	 * because in this case we don't need no additional information 
	 */
	BI_Vector3( const BI_Vector3 & begin, const BI_Vector3 & end)
	{
		_coords[0]=end.X() - begin.X();
		_coords[1]=end.Y() - begin.Y();
		_coords[2]=end.Z() - begin.Z();
	}

	/** \brief Access operator */
	/** acces via range, range is <0-2> */
	const float operator[](int idx) const { return _coords[idx]; }

	/** \brief Sets BI_Vector3 coordinate */
	/** The result as the same as operator=, but without the need to call constructor on vector
	 */
	void Set(float x, float y, float z)
	{
		SetX(x); SetY(y); SetZ(z);
	}
	/** \brief Access to X coordinate*/
	float X() const { return _coords[_X]; }

	/** \brief Access to Y coordinate */
	float Y() const { return _coords[_Y]; }

	/** \brief Access to Z coordinate */
	float Z() const { return _coords[_Z]; }

	/** \brief Sets new X coordinate */
	void SetX(float value)	{ _coords[_X] = value ;}

	/** \brief Sets new Y coordinate */
	void SetY(float value) { _coords[_Y] = value; }

	/** \brief Sets new Z coordinate */
	void SetZ(float value) { _coords[_Z] = value; }

	/** \brief Finds minimum value in BI_Vector */
	/** \return minimal value that BI_Vector contains 
	 * \param idx will contain index of element holding the value 
	 * \return minimum value */
	float GetMinimum( int & idx)const
	{
		idx = 0;
		float res = _coords[0];
		for( int i =1; i< 3; i++)
			if(_coords[i] < res)
			{
				res = _coords[i];
				idx = i;
			}
			return res;
	}

	/** \brief Finds maximum value in BI_Vector */
	/** \returns maximal value that BI_Vector contains 
	 * \param idx tells which item it was 
	 */
	float GetMaximum( int & idx)const
	{
		idx = 0;
		float res = _coords[0];
		for( int i =1; i< 3; i++)
			if(_coords[i] > res)
			{
				res = _coords[i];
				idx = i;
			}
			return res;
	}

	/** \brief Access via [] */
	/** \returns reference to float coordinate whose representing number is idx 
	 */
	float& operator[](int idx) { return _coords[idx]; }

	/** \brief Equality operator*/
	/** BI_Vectors are equal if all their values at each position are the same 
	 */
	bool operator==( const BI_Vector3 & cmp ) const 
	{ return fabs( (float)( X() - cmp.X()))<1e-5 
		  && fabs( (float)( Y() - cmp.Y())) <1e-5 
		  && fabs( (float)( Z() - cmp.Z()))<1e-5;
	}
	
	/** \brief Inequality operator */
	/** BI_Vectors are not equal if any their values on ony position differ 
	 */
	bool operator!=( const BI_Vector3 & cmp ) const { return !((*this)==cmp); }

	/** \brief BI_Vector operation plus.*/
	BI_Vector3& operator+=( const BI_Vector3 & op )
	{
		_coords[0]+=op._coords[0];
		_coords[1]+=op._coords[1];
		_coords[2]+=op._coords[2];
		return *this;
	}

	/** \brief BI_Vector operatorplus */
	BI_Vector3 operator+( const BI_Vector3 & op ) const
	{
		BI_Vector3 v(*this);
		v+=op;
		return v;
	}

	/** \brief Vector operation minus */
	BI_Vector3& operator-=( const BI_Vector3 & op )
	{
		_coords[0]-=op._coords[0];
		_coords[1]-=op._coords[1];
		_coords[2]-=op._coords[2];
		return *this;
	}

	/** \brief Vector operatorminus*/
	BI_Vector3 operator-( const BI_Vector3 & op ) const
	{
		BI_Vector3 v(*this);
		v-=op;
		return v;
	}

	/** \brief Multiply each element by \param f */
	BI_Vector3& operator*=( float f )
	{
		_coords[0]*=f;
		_coords[1]*=f;
		_coords[2]*=f;
		return *this;
	}

	/** \brief Multiply each element by \param f */
	BI_Vector3 operator*( float f )const
	{
		BI_Vector3 v(*this);
		v*=(f);
		return v;
	}

	/** \brief Divides each element by \param f */
	BI_Vector3& operator/=( float f )
	{
		float invf = 1/f;
		return(*this)*=invf;
	}
	/** \brief Multiply each element by \param f */
	BI_Vector3 operator/( float f )
	{
		BI_Vector3 v(*this);
		v/=f;
		return v;
	}

	/** \brief Assign operator */
	BI_Vector3& BI_Vector3::operator=( const BI_Vector3 & src )
	{
		SetX(src.X());
		SetY(src.Y());
		SetZ(src.Z());
		return *this;
	}
	
	/** \brief Multiply BI_Vectors by elements */
	BI_Vector3 MultiplyByElements( const BI_Vector3 & op ) const
	{
		BI_Vector3 v(op.X()*X(), op.Y()*Y(), op.Z()*Z());
		return v;
	}
	/** \brief Computes BI_Vector dot product */
	float DotProduct( const BI_Vector3& op ) const
	{
		return X()*op.X()+Y()*op.Y()+Z()*op.Z();
	}

	/** \brief Computes BI_Vector dot product */
	BI_Vector3 CrossProduct( const BI_Vector3 op ) const
	{
		float ox = op.X();
		float oy = op.Y();
		float oz = op.Z();
		float lx = X();
		float ly = Y();
		float lz = Z();

		float x=ly*oz-lz*oy;
		float y=lz*ox-lx*oz;
		float z=lx*oy-ly*ox;
		return BI_Vector3(x,y,z);
	}

	/** \brief Computes BI_Vector normalization  */
	BI_Vector3 Normalized() const
	{
		BI_Vector3 v( *this );
		v.Normalize();
		return v;
	}

	/** \brief Sets BI_Vector to size 1 */
	void Normalize()
	{
		float sz = Size();
		if (sz < 1.0000e-7)
			sz =1.0;
		(*this) /= sz;
	}

	/** \brief Computes square size of BI_Vector  */
	/** \return square size of BI_Vector.
	 */
	float SquareSize() const { return X()*X()+Y()*Y()+Z()*Z(); }

	/** \brief Computes actual size of vector */
	float Size() const { return sqrt(SquareSize()); }

	/** \brief Computes cosine angle between two BI_Vector.*/
	/** @return cosine of the angle between the edges */
	float CosAngle( const BI_Vector3& op ) const
	{
		float denom2 = SquareSize()*op.SquareSize();
		return DotProduct(op)/sqrt(denom2);
	}
	
	/** \brief Computes distance in Euclidean space  */
	float Distance( const BI_Vector3& op ) const { return sqrt(Distance2(op)); }

	/** \brief Computes square distance in Euclidean space  */
	float Distance2( const BI_Vector3& op ) const { return (op-(*this)).SquareSize(); }

	/**\brief Conversion between different types of BI_Vector */
	/** Template metod for converting user defined BI_Vector to our BI_Vector. On user size there should be implemented method X(), Y(), Z()
	 * giving required value
	 */
	template<class BI_VectorClass>	void Convert(BI_VectorClass s)
	{
		SetX(s.X());SetY(s.Y());SetZ(s.Z());
	}
	/** \brief Conversion between different types of BI_Vector  */
	/** Template metod for converting our BI_Vector to user defined BI_Vector. On user size there should be implemented operator[]
	 * giving value at position required and empty contructor 
	 * \param perm describes position of axis how it is Set on user side
	 */
	template<class VectorClass>
	VectorClass Convert(int* perm = NULL )const
	{
		int permutation[] = {1,2,3};
		if(perm!=NULL)
			memcpy(perm, permutation, sizeof(int)*3);
		VectorClass v;
		v[permutation[0]]=getX();
		v[permutation[1]]=getY();
		v[permutation[2]]=getZ();
		return v;
	}
};
/* for better handling, several trivial instances are created  */

/* zero BI_UVVector */
#define BI_ZeroUVVector BI_UVVector(0,0)

/* zero 3D BI_Vector */
#define BI_ZeroVector3 BI_Vector3(0,0,0)

//intersection of two BI_Vectors?
#endif