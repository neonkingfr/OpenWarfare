This plugin was created to export the VBS2 geometry into a format that Xaitmap can read.

Example call to the plugin:

res = fusionFunction["FusionPluginExportGeometry","2000,2000,3000,3000,'c:\\test.bin'"]


In order to load the resulting geometry into Xaitmap Creator, place the ImportVBS2.dll into the XaitMap Plugin folder (default: C:\sdk\XAIT\editor\xaitMapCreator\plugins )

Start a new project using the wizzard and add as geometry your test.bin file. 
Make sure that the up vector is Y (press Y button).
