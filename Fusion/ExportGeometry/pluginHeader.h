#include <windows.h>

#define VBSPLUGIN_EXPORT __declspec(dllexport)

VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc);
VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT);
VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input);
VBSPLUGIN_EXPORT void WINAPI RegisterGetMapGeomFnc(void *getMapGeometryFnc);
VBSPLUGIN_EXPORT void WINAPI RegisterRelMapGeomFnc(void *releaseGeometryFnc);

struct GeometryObject;

typedef int (WINAPI * ExecuteCommandType)(const char *command, char *result, int resultLength);
// GetMapGeometry function declaration
typedef GeometryObject *(WINAPI * GetMapGeometryType)(float begX, float begZ, float endX, float endZ, int& geometryObjectsSize, int type);
// ReleaseMapGeometry function declaration
typedef void (WINAPI * ReleaseMapGeometryType)(GeometryObject *objects);

//Command function declaration
ExecuteCommandType ExecuteCommand = NULL;
// GetMapGeometry function definition
GetMapGeometryType GetMapGeometry = NULL;
// ReleaseMapGeometry function definition
ReleaseMapGeometryType ReleaseMapGeometry = NULL;

VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc)
{
	ExecuteCommand = (ExecuteCommandType)executeCommandFnc;
};

