#include "TurretControlGUI.hpp"
#include "TurretControlApp.hpp"

#include <wx/statline.h>
#include "../../Common/BISDKCommon.h"


#define POINTSIZED_FONT(size,style,weight) (wxFont(size, wxDEFAULT, style, weight, false, wxEmptyString, wxFONTENCODING_DEFAULT))

BEGIN_EVENT_TABLE(MainFrame, wxFrame)
EVT_CLOSE(MainFrame::OnClose)
EVT_CHECKBOX(ID_CHK_VSTAB, MainFrame::OnVStab)
EVT_CHECKBOX(ID_CHK_HSTAB, MainFrame::OnHStab)
EVT_COMMAND_SCROLL(ID_SLD_VSPEED, MainFrame::OnVSpeedSlider)
EVT_COMMAND_SCROLL(ID_SLD_HSPEED, MainFrame::OnHSpeedSlider)
EVT_TEXT_ENTER(ID_TEXT_VSPEED, MainFrame::OnVSpeedEnter)
EVT_TEXT_ENTER(ID_TEXT_HSPEED, MainFrame::OnHSpeedEnter)

#if !TURRET_CONTROL_ONLY_SPEED
EVT_COMMAND_SCROLL(ID_SLD_ELEV, MainFrame::OnElevSlider)
EVT_COMMAND_SCROLL(ID_SLD_AZIM, MainFrame::OnAzimSlider)
EVT_TEXT_ENTER(ID_TEXT_ELEV, MainFrame::OnElevEnter)
EVT_TEXT_ENTER(ID_TEXT_AZIM, MainFrame::OnAzimEnter)
#endif

EVT_TIMER(TIMER_ID,MainFrame::OnTimer)
EVT_CHOICE(ID_CHOICE_TURRET, MainFrame::OnSelectTurret)
END_EVENT_TABLE()

//_________________________________________________________________________
MainFrame::MainFrame()
: wxFrame(NULL, wxID_ANY, "Turret control", wxDefaultPosition, wxSize(800,-1))
{
  wxPanel* mainPanel = new wxPanel(this,wxID_ANY);
  wxBoxSizer* mainSizer = new wxBoxSizer(wxVERTICAL);
  mainPanel->SetSizer(mainSizer);
  mainPanel->SetBackgroundColour(wxColour(213,209,201));
  {
    wxPanel* pnlHeader = new wxPanel(mainPanel, wxID_ANY, wxDefaultPosition, wxSize(-1,20));
    mainSizer->Add(pnlHeader, 0, wxEXPAND, 0);
    pnlHeader->SetBackgroundColour(wxColour(50,50,50));

    wxBoxSizer* sizerHorizontal = new wxBoxSizer(wxHORIZONTAL);
    mainSizer->Add(sizerHorizontal, 1, wxEXPAND, 0);
    {
      // checkbox panel
      wxBoxSizer* sizerLeft = new wxBoxSizer(wxVERTICAL);
      sizerHorizontal->Add(sizerLeft, 0, wxEXPAND | wxTOP, 5);
      {
        _chkVertStabilization = new wxCheckBox(mainPanel,ID_CHK_VSTAB,"Vertical stabilization");
        _chkVertStabilization->SetValue(true);
        sizerLeft->Add(_chkVertStabilization, 0, wxEXPAND | wxALL, 5);

        _chkHorStabilization = new wxCheckBox(mainPanel,ID_CHK_HSTAB,"Horizontal stabilization");
        _chkHorStabilization->SetValue(true);
        sizerLeft->Add(_chkHorStabilization, 0, wxALL, 5);

        _choiceTurret = new wxChoice(mainPanel, ID_CHOICE_TURRET);
        sizerLeft->Add(_choiceTurret, 0, wxEXPAND | wxALL, 5);
      }

      wxStaticLine* line = new wxStaticLine(mainPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxVERTICAL);
      sizerHorizontal->Add(line,0,wxEXPAND | wxLEFT | wxRIGHT, 5);
      
      wxBoxSizer* sizerRight = new wxBoxSizer(wxVERTICAL);
      sizerHorizontal->Add(sizerRight, 1, wxEXPAND | wxTOP, 5);
      {
        // VERTICAL SPEED
        wxBoxSizer* sizerVSpeed = new wxBoxSizer(wxHORIZONTAL);
        sizerRight->Add(sizerVSpeed, 0, wxEXPAND | wxBOTTOM | wxRIGHT, 5);
        {
          wxStaticText* labelVSpeed = new wxStaticText(mainPanel, wxID_ANY, "Vertical speed");
          sizerVSpeed->Add(labelVSpeed, 0, wxEXPAND | wxALL, 5);
          labelVSpeed->SetMinSize(wxSize(120,-1));

#if TURRET_CONTROL_ONLY_SPEED
          _valVSpeed = new wxTextCtrl(mainPanel, ID_TEXT_VSPEED, "0", wxDefaultPosition, wxSize(100,-1), wxTE_PROCESS_ENTER);
#else
          _valVSpeed = new wxTextCtrl(mainPanel, ID_TEXT_VSPEED, "40", wxDefaultPosition, wxSize(100,-1), wxTE_PROCESS_ENTER);
#endif
          sizerVSpeed->Add(_valVSpeed, 0, wxEXPAND, 0);

#if TURRET_CONTROL_ONLY_SPEED
          _sldVSpeed = new wxSlider(mainPanel,ID_SLD_VSPEED,0,-SLIDER_MAX_VAL*0.1,SLIDER_MAX_VAL*0.1, wxDefaultPosition,wxDefaultSize,wxSL_HORIZONTAL/*|wxSL_LABELS*/); 
#else
          _sldVSpeed = new wxSlider(mainPanel,ID_SLD_VSPEED,SLIDER_MAX_VAL*0.4,0,SLIDER_MAX_VAL, wxDefaultPosition,wxDefaultSize,wxSL_HORIZONTAL/*|wxSL_LABELS*/); 
#endif
          sizerVSpeed->Add(_sldVSpeed, 1, wxEXPAND, 0);
        }

        // HORIZONTAL SPEED
        wxBoxSizer* sizerHSpeed = new wxBoxSizer(wxHORIZONTAL);
        sizerRight->Add(sizerHSpeed, 0, wxEXPAND | wxBOTTOM | wxRIGHT, 5);
        {
          wxStaticText* labelHSpeed = new wxStaticText(mainPanel, wxID_ANY, "Horizontal speed");
          sizerHSpeed->Add(labelHSpeed, 0, wxEXPAND | wxALL, 5);
          labelHSpeed->SetMinSize(wxSize(120,-1));

#if TURRET_CONTROL_ONLY_SPEED
          _valHSpeed = new wxTextCtrl(mainPanel, ID_TEXT_HSPEED, "0", wxDefaultPosition, wxSize(100,-1), wxTE_PROCESS_ENTER);
#else
          _valHSpeed = new wxTextCtrl(mainPanel, ID_TEXT_HSPEED, "40", wxDefaultPosition, wxSize(100,-1), wxTE_PROCESS_ENTER);
#endif
          sizerHSpeed->Add(_valHSpeed, 0, wxEXPAND, 0);

#if TURRET_CONTROL_ONLY_SPEED
          _sldHSpeed = new wxSlider(mainPanel,ID_SLD_HSPEED,0,-SLIDER_MAX_VAL,SLIDER_MAX_VAL, wxDefaultPosition,wxDefaultSize,wxSL_HORIZONTAL/*|wxSL_LABELS*/); 
#else
          _sldHSpeed = new wxSlider(mainPanel,ID_SLD_HSPEED,SLIDER_MAX_VAL*0.4,0,SLIDER_MAX_VAL, wxDefaultPosition,wxDefaultSize,wxSL_HORIZONTAL/*|wxSL_LABELS*/); 
#endif
          sizerHSpeed->Add(_sldHSpeed, 1, wxEXPAND, 0);
        }
        
        wxStaticLine* line = new wxStaticLine(mainPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHORIZONTAL);
        sizerRight->Add(line,0,wxEXPAND | wxBOTTOM | wxRIGHT, 5);
#if !TURRET_CONTROL_ONLY_SPEED
        // ELEVATION
        wxBoxSizer* sizerElev = new wxBoxSizer(wxHORIZONTAL);
        sizerRight->Add(sizerElev, 0, wxEXPAND | wxBOTTOM | wxRIGHT, 5);
        {
          wxStaticText* labelElev = new wxStaticText(mainPanel, wxID_ANY, "Elevation");
          sizerElev->Add(labelElev, 0, wxEXPAND | wxALL, 5);
          labelElev->SetMinSize(wxSize(120,-1));

          _valElevation = new wxStaticText(mainPanel, wxID_ANY, "0.0", wxDefaultPosition, wxSize(100,-1));
          sizerElev->Add(_valElevation, 0, wxEXPAND, 0);

          _sldElevation = new wxSlider(mainPanel,-1,SLIDER_MAX_VAL*0.5,0,SLIDER_MAX_VAL, wxDefaultPosition,wxDefaultSize,wxSL_HORIZONTAL/*|wxSL_LABELS*/); 
          sizerElev->Add(_sldElevation, 1, wxEXPAND, 0);
          _sldElevation->Enable(false);
        }
        
        // ELEVATION WANTED
        wxBoxSizer* sizerElevWanted = new wxBoxSizer(wxHORIZONTAL);
        sizerRight->Add(sizerElevWanted, 0, wxEXPAND | wxBOTTOM | wxRIGHT, 5);
        {
          wxStaticText* labelElev = new wxStaticText(mainPanel, wxID_ANY, "Elevation wanted");
          sizerElevWanted->Add(labelElev, 0, wxEXPAND | wxALL, 5);
          labelElev->SetMinSize(wxSize(120,-1));

          _valElevationWanted = new wxTextCtrl(mainPanel, ID_TEXT_ELEV, "0", wxDefaultPosition, wxSize(100,-1), wxTE_PROCESS_ENTER);
          sizerElevWanted->Add(_valElevationWanted, 0, wxEXPAND, 0);

          _sldElevationWanted = new wxSlider(mainPanel,ID_SLD_ELEV,SLIDER_MAX_VAL*0.5,0,SLIDER_MAX_VAL, wxDefaultPosition,wxDefaultSize,wxSL_HORIZONTAL/*|wxSL_LABELS*/); 
          sizerElevWanted->Add(_sldElevationWanted, 1, wxEXPAND, 0);
        }  
        
        wxStaticLine* line2 = new wxStaticLine(mainPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHORIZONTAL);
        sizerRight->Add(line2,0,wxEXPAND | wxBOTTOM | wxRIGHT, 5);              

        // AZIMUTH
        wxBoxSizer* sizerAzim = new wxBoxSizer(wxHORIZONTAL);
        sizerRight->Add(sizerAzim, 0, wxEXPAND | wxBOTTOM | wxRIGHT, 5);
        {
          wxStaticText* labelAzim = new wxStaticText(mainPanel, wxID_ANY, "Azimuth");
          sizerAzim->Add(labelAzim, 0, wxEXPAND | wxALL, 5);
          labelAzim->SetMinSize(wxSize(120,-1));

          _valAzimuth = new wxStaticText(mainPanel, wxID_ANY, "0.0", wxDefaultPosition, wxSize(100,-1));
          sizerAzim->Add(_valAzimuth, 0, wxEXPAND, 0);

          _sldAzimuth = new wxSlider(mainPanel,-1,SLIDER_MAX_VAL*0.5,0,SLIDER_MAX_VAL, wxDefaultPosition,wxDefaultSize,wxSL_HORIZONTAL/*|wxSL_LABELS*/); 
          sizerAzim->Add(_sldAzimuth, 1, wxEXPAND, 0);
          _sldAzimuth->Enable(false);
        }
        
        // AZIMUTH WANTED
        wxBoxSizer* sizerAzimWanted = new wxBoxSizer(wxHORIZONTAL);
        sizerRight->Add(sizerAzimWanted, 0, wxEXPAND | wxBOTTOM | wxRIGHT, 5);
        {
          wxStaticText* labelAzim = new wxStaticText(mainPanel, wxID_ANY, "Azimuth wanted");
          sizerAzimWanted->Add(labelAzim, 0, wxEXPAND | wxALL, 5);
          labelAzim->SetMinSize(wxSize(120,-1));

          _valAzimuthWanted = new wxTextCtrl(mainPanel, ID_TEXT_AZIM, "0", wxDefaultPosition, wxSize(100,-1), wxTE_PROCESS_ENTER);
          sizerAzimWanted->Add(_valAzimuthWanted, 0, wxEXPAND, 0);

          _sldAzimuthWanted = new wxSlider(mainPanel,ID_SLD_AZIM,SLIDER_MAX_VAL*0.5,0,SLIDER_MAX_VAL, wxDefaultPosition,wxDefaultSize,wxSL_HORIZONTAL/*|wxSL_LABELS*/); 
          sizerAzimWanted->Add(_sldAzimuthWanted, 1, wxEXPAND, 0);
        }   
#endif
      }
    }
  }
  _refreshTimer = new wxTimer(this,TIMER_ID);
  _refreshTimer->Start(30);  
}

//_________________________________________________________________________
void MainFrame::OnClose(wxCloseEvent& e)
{
  Destroy();
}

//_________________________________________________________________________
void MainFrame::OnVStab(wxCommandEvent& e)
{
  ::wxGetApp().UpdateVStab(_chkVertStabilization->GetValue());
}

//_________________________________________________________________________
void MainFrame::OnHStab(wxCommandEvent& e)
{
  ::wxGetApp().UpdateHStab(_chkHorStabilization->GetValue());
}  

//_________________________________________________________________________
void MainFrame::OnVSpeedEnter(wxCommandEvent& e)
{
  double val;
  if (_valVSpeed->GetValue().ToDouble(&val))
  {
#if TURRET_CONTROL_ONLY_SPEED
    if (val<-10 || val>10)
    {
      val = BI_Max(BI_Min(val,10.0),-10.0);
      wxString textVal;
      textVal << val;
      _valVSpeed->SetValue(textVal);
    }
    int gaugeValue = (int)(val/(20.0/SLIDER_MAX_VAL));
#else
    if (val<0 || val>100)
    {
      val = BI_Max(BI_Min(val,100.0),0.0);
      wxString textVal;
      textVal << val;
      _valVSpeed->SetValue(textVal);
    }
    int gaugeValue = (int)(val/(100.0/SLIDER_MAX_VAL));
#endif
    _sldVSpeed->SetValue(gaugeValue);
    ::wxGetApp().UpdateVSpeed(val);
  }
  else
  {
    wxMessageBox("Wrong speed value!");
  }
}

//_________________________________________________________________________
void MainFrame::OnHSpeedEnter(wxCommandEvent& e)
{
  double val;
#if TURRET_CONTROL_ONLY_SPEED
  if (_valHSpeed->GetValue().ToDouble(&val))
  {
    if (val<-100 || val>100)
    {
      val = BI_Max(BI_Min(val,100.0),-100.0);
      wxString textVal;
      textVal << val;
      _valHSpeed->SetValue(textVal);
    }

    int gaugeValue = (int)(val/(100.0/SLIDER_MAX_VAL));
#else
  if (_valHSpeed->GetValue().ToDouble(&val))
  {
    if (val<0 || val>100)
    {
      val = BI_Max(BI_Min(val,100.0),0.0);
      wxString textVal;
      textVal << val;
      _valHSpeed->SetValue(textVal);
    }

    int gaugeValue = (int)(val/(100.0/SLIDER_MAX_VAL));
#endif
    _sldHSpeed->SetValue(gaugeValue);
    ::wxGetApp().UpdateHSpeed(val);
  }
  else
  {
    wxMessageBox("Wrong speed value!");
  }
}  

//_________________________________________________________________________
void MainFrame::OnVSpeedSlider(wxScrollEvent& e)
{
  double vspeed = ((double)_sldVSpeed->GetValue()/SLIDER_MAX_VAL)*100.0;

  wxString textVal;
  textVal << vspeed;
  _valVSpeed->ChangeValue(textVal);

  ::wxGetApp().UpdateVSpeed(vspeed);
}

//_________________________________________________________________________
void MainFrame::OnHSpeedSlider(wxScrollEvent& e)
{
  double hspeed = ((double)_sldHSpeed->GetValue()/SLIDER_MAX_VAL)*100.0;

  wxString textVal;
  textVal << hspeed;
  _valHSpeed->ChangeValue(textVal);

  ::wxGetApp().UpdateHSpeed(hspeed);
}

#if !TURRET_CONTROL_ONLY_SPEED
#define ELEVMAX_VAL 80.0

//_________________________________________________________________________
void MainFrame::OnElevSlider(wxScrollEvent& e)
{
  double elev = ((double)_sldElevationWanted->GetValue()/SLIDER_MAX_VAL)*2*ELEVMAX_VAL - ELEVMAX_VAL;

  wxString textVal;
  textVal << elev;
  _valElevationWanted->SetValue(textVal);

  ::wxGetApp().UpdateElev(elev);
}

//_________________________________________________________________________
void MainFrame::OnElevEnter(wxCommandEvent& e)
{
  double val;
  if (_valElevationWanted->GetValue().ToDouble(&val))
  {
    if (val >ELEVMAX_VAL) 
    {
      val = ELEVMAX_VAL;
      wxString textVal;
      textVal << val;
      _valElevationWanted->SetValue(textVal);
    }

    if (val <-ELEVMAX_VAL) 
    {
      val = -ELEVMAX_VAL;
      wxString textVal;
      textVal << val;
      _valElevationWanted->SetValue(textVal);
    }

    int gaugeValue = (int)((val+ELEVMAX_VAL)/(2*ELEVMAX_VAL/SLIDER_MAX_VAL));
    _sldElevationWanted->SetValue(gaugeValue);
    ::wxGetApp().UpdateElev(val);
  }
  else
  {
    wxMessageBox("Wrong elevation value!");
  }
}  

//_________________________________________________________________________
void MainFrame::OnAzimSlider(wxScrollEvent& e)
{
  double azim = ((double)_sldAzimuthWanted->GetValue()/SLIDER_MAX_VAL)*360.0 - 180.0;

  wxString textVal;
  textVal << azim;
  _valAzimuthWanted->SetValue(textVal);

  ::wxGetApp().UpdateAzim(azim);
}

//_________________________________________________________________________
void MainFrame::OnAzimEnter(wxCommandEvent& e)
{
  double val;
  if (_valAzimuthWanted->GetValue().ToDouble(&val))
  {
    if (val >180.0) 
    {
      val = fmod(val+180,360.0)-180;
      wxString textVal;
      textVal << val;
      _valAzimuthWanted->SetValue(textVal);
    }

    if (val <-180.0) 
    {
      val = -fmod(-val+180,360.0)+180;
      wxString textVal;
      textVal << val;
      _valAzimuthWanted->SetValue(textVal);
    }

    int gaugeValue = (int)((val+180.0)/(360.0/SLIDER_MAX_VAL));
    _sldAzimuthWanted->SetValue(gaugeValue);
    ::wxGetApp().UpdateAzim(val);
  }
  else
  {
    wxMessageBox("Wrong azimuth value!");
  }
}  
#endif

void MainFrame::OnTimer(wxTimerEvent& event)
{
#if !TURRET_CONTROL_ONLY_SPEED
  double val = ::wxGetApp().GetAzimut();
  if (val >180.0) 
    val -=360.0f;

  int gaugeValue = (int)((val+180.0)/(360.0/SLIDER_MAX_VAL));
  _sldAzimuth->SetValue(gaugeValue);

  wxString textVal;
  textVal << val;
  _valAzimuth->SetLabel(textVal); 
   
   
  val = ::wxGetApp().GetElevation();
   gaugeValue = (int)((val+ELEVMAX_VAL)/(2*ELEVMAX_VAL/SLIDER_MAX_VAL));
  _sldElevation->SetValue(gaugeValue); 

  textVal.Clear();
  textVal << val;
  _valElevation->SetLabel(textVal);
#endif
}

//_________________________________________________________________________
void MainFrame::OnSelectTurret(wxCommandEvent &e)
{
  ::wxGetApp().UpdateActiveTurret(GetActiveTurret().data());
}



