/****************************************************************************
* Plugin for functionality demonstration of turrets controllers             *
*   VBS2Fusion::VehicleUtilities::applyOpticsDirection                      *
*   VBS2Fusion::VehicleUtilities::applyTurretDirectionChange                *
*   VBS2Fusion::VehicleUtilities::applyTurretSpeed                          *
*   VBS2Fusion::VehicleUtilities::applyTurretStabilizaton                   *
****************************************************************************/

#pragma warning(disable: 4996)
#pragma warning(disable: 4251)

#define WEAPON_HANDLING 1
#define _BISIM_DEV_CHANGE_TURRET_DIRECTION 1
#define WEAPON_HANDLING_OLD 1

#include <stdio.h>
#include <process.h>
#include <map>
#include <VBS2Fusion.h>
#include "TurretControl.h"
#include "TurretControlApp.hpp"

#include <util/MissionUtilities.h>
#include <util/VehicleUtilities.h>
#include <util/UnitUtilities.h>
#include <data/Turret.h>


#pragma region Global variables

/// Handle of UI applictaion thread
HANDLE gThreadHandle = NULL;

/// Since the UI runs in it's own thread we need critical section to solve thread conflicts
static CRITICAL_SECTION gCriticalSection;

bool gPositionChanged = true; //true to reset VBS2 values when dialog first time opened

double gNewAzimuthSpeed = 0;
double gNewElevationSpeed = 0;
bool gSpeedChanged = true;

bool gNewAzimuthStabil = false;
bool gNewElevationStabil = false;
bool gStabilChanged = true;

string gActiveTurret = "";  // string representing currently selected turret
std::map<string, vector<int>> gTurrets;  // map of <string_identifier, turret_path>

#pragma endregion Global variables

#define _DIAG 1
#define LOG_HEADER "[TurretControlGUI] "
void LogDiagMessage(const char *format, ...)
{
#if _DIAG
  va_list arglist;
  va_start(arglist, format);
  char buffer[512] = { 0 };
  vsprintf_s(buffer,512,format, arglist);
  va_end(arglist);
  OutputDebugString(buffer);
#endif
}

VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc)
{
}

#pragma region Functions used by UI
void SetAzimutSpeed(double val)
{
  EnterCriticalSection(&gCriticalSection);
  gSpeedChanged = true;
  gNewAzimuthSpeed = val; 
  LeaveCriticalSection(&gCriticalSection);
}

void SetElevationSpeed(double val)
{
  EnterCriticalSection(&gCriticalSection);
  gSpeedChanged = true;
  gNewElevationSpeed = val; 
  LeaveCriticalSection(&gCriticalSection);
}

void SetAzimuthStabil(bool val)
{
  EnterCriticalSection(&gCriticalSection);
  gStabilChanged = true;
  gNewAzimuthStabil = val; 
  LeaveCriticalSection(&gCriticalSection);
}

void SetElevationStabil(bool val)
{
  EnterCriticalSection(&gCriticalSection);
  gStabilChanged = true;
  gNewElevationStabil = val; 
  LeaveCriticalSection(&gCriticalSection);
}

void SetActiveTurret(const char *turret)
{
  EnterCriticalSection(&gCriticalSection);
  gActiveTurret = turret;
  gStabilChanged = true;
  gSpeedChanged = true;
  gPositionChanged = true;
  LeaveCriticalSection(&gCriticalSection);
}
#pragma endregion Functions used by UI


DWORD WINAPI ThreadProc(LPVOID lpParameter)
{
  // Start the UI
  RunApp();
  gThreadHandle = NULL;
  return true;
}

// Called from the UI initialization to fill combo box with all available turrets.
vector<string> GetTurrets()
{
  EnterCriticalSection(&gCriticalSection);
  vector<string> result;

  VBS2Fusion::Unit playerUnit = VBS2Fusion::MissionUtilities::getPlayer();
  VBS2Fusion::Vehicle tank = VBS2Fusion::UnitUtilities::getMountedVehicle(playerUnit);
  VBS2Fusion::VehicleUtilities::updateVehicle(tank);
  // VehicleUtilities::getCrewPosition returns all available positions in the vehicle.
  vector<VBS2Fusion::CrewPos> turrets = VBS2Fusion::VehicleUtilities::getCrewPosition(tank);

  VBS2Fusion::Turret turret;
  vector<VBS2Fusion::CrewPos>::const_iterator iter;
  for (iter = turrets.begin(); iter<turrets.end(); iter++)
  {
    // If there is something in CrewPos::turret then the position is turret.
    // We are interested only in those positions which are turrets.
    if ((*iter).turret.size() == 0) continue;

    // Get turret path string 
    turret.setTurretPath((*iter).turret);
    string turretPathString = turret.getTurretpathString();
    
    result.push_back(turretPathString);
    // Fill the info about the turret into the global map.
    gTurrets[turretPathString] = (*iter).turret;
  }
  LeaveCriticalSection(&gCriticalSection);
  return result;
};

VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
  static const char *result = "";

  VBS2Fusion::Unit playerUnit = VBS2Fusion::MissionUtilities::getPlayer();
  // init
  if (gThreadHandle == NULL && stricmp(input, "i") == 0)
  {
    gThreadHandle = CreateThread(NULL,0,ThreadProc,NULL,0,NULL);
  }

  return result;
}

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
  if (gThreadHandle == NULL) 
    return;

  if (gActiveTurret.length() == 0) 
    return;

  // For demonstration purposes we use turret of vehicle in which player is located
  VBS2Fusion::Unit playerUnit = VBS2Fusion::MissionUtilities::getPlayer();
  VBS2Fusion::Vehicle tank = VBS2Fusion::UnitUtilities::getMountedVehicle(playerUnit);
  VBS2Fusion::VehicleUtilities::updateVehicle(tank);

  VBS2Fusion::Turret turret;
  // Set the path to currently selected turret.
  turret.setTurretPath(gTurrets[gActiveTurret]);

  // changed of speed of turret
  bool speedChanged = gSpeedChanged;
  gSpeedChanged = false;

  double newAzimuthSpeed = gNewAzimuthSpeed;
  double newElevationSpeed = gNewElevationSpeed;

  // changed stabilization
  bool stabilChanged = gStabilChanged;
  gStabilChanged = false;

  bool newAzimuthStabil = gNewAzimuthStabil;
  bool newElevationStabil = gNewElevationStabil;

  LeaveCriticalSection(&gCriticalSection);

  // Set wanted speed
  if (speedChanged)
    VBS2Fusion::VehicleUtilities::applyTurretDirectionChange(tank, turret, newAzimuthSpeed, newElevationSpeed, false, false);
  // Set wanted stabilization flags
  if (stabilChanged)
    VBS2Fusion::VehicleUtilities::applyTurretStabilizaton(tank, turret, newAzimuthStabil, newElevationStabil);
}

// DllMain
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
  switch(fdwReason)
  {
  case DLL_PROCESS_ATTACH:
    InitializeCriticalSection(&gCriticalSection);
    break;
  case DLL_PROCESS_DETACH:
    // If the UI is running then close it.
    if (gThreadHandle != NULL)
      TerminateThread(gThreadHandle, 0);
      gThreadHandle = NULL;
    DeleteCriticalSection(&gCriticalSection);
    break;
  case DLL_THREAD_ATTACH:
    break;
  case DLL_THREAD_DETACH:
    break;
  }
  return TRUE;
}

