
#include <windows.h>
#include <vector>
#include <string>

#define VBSPLUGIN_EXPORT __declspec(dllexport)

#define TURRET_CONTROL_ONLY_SPEED 1

typedef int (WINAPI * ExecuteCommandType)(const char *command, char *result, int resultLength);

#if !TURRET_CONTROL_ONLY_SPEED
void SetAzimut(double val);
void SetElevation(double val);
double GetAzimut();
double GetElevation();
#endif

void SetAzimutSpeed(double val);
void SetElevationSpeed(double val);
void SetAzimuthStabil(bool val);
void SetElevationStabil(bool val);
void SetActiveTurret(const char *turret);

std::vector<std::string> GetTurrets();


