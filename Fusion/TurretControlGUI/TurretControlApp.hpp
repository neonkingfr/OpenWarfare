#pragma once

#ifndef TurretControlApp_h__
#define TurretControlApp_h__

#include <wx/wx.h>
#include <wx/timer.h>

#include "TurretControlGUI.hpp"

#define IMPLEMENT_APP_GET_ONLY(appname)                                             \
    appname& wxGetApp() { return *wx_static_cast(appname*, wxApp::GetInstance()); } \

#define TIMER_ID 123

///////////////////////////////////////////////////////////////////////////
class TurretControlApp: public wxApp
{
private:
  MainFrame* _mainFrame;

public:
  TurretControlApp();
  virtual ~TurretControlApp();

  virtual bool OnInit();

  void UpdateVStab(bool val);
  void UpdateHStab(bool val);
  void UpdateVSpeed(double val);
  void UpdateHSpeed(double val);
  void UpdateElev(double val);
  void UpdateAzim(double val);
  void UpdateActiveTurret(const char* turretName);
  
  double GetAzimut();
  double GetElevation();

  void AddTurret(const char* turretName);
  const char* GetActiveTurret();
  bool SetActiveTurret(const char* turretName);
};

static wxAppConsole *wxCreateApp();
void RunApp();

DECLARE_APP(TurretControlApp);

#endif // TurretControlApp_h__
