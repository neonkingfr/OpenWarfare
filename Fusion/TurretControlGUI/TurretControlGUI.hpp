#pragma once

#ifndef TurretControlGUI_h__
#define TurretControlGUI_h__

#include <wx/wx.h>

#define SLIDER_MAX_VAL 100000.0

#define ID_CHK_VSTAB 100
#define ID_CHK_HSTAB 101
#define ID_TEXT_VSPEED 102
#define ID_SLD_VSPEED 103
#define ID_TEXT_HSPEED 104
#define ID_SLD_HSPEED 105
#define ID_TEXT_ELEV 106
#define ID_SLD_ELEV 107
#define ID_TEXT_AZIM 108
#define ID_SLD_AZIM 109
#define ID_CHOICE_TURRET 110

#define TURRET_CONTROL_ONLY_SPEED 1

class MainFrame : public wxFrame
{
public:
  MainFrame();

  void OnClose(wxCloseEvent& e);

  wxTimer* _refreshTimer;

  void AddTurret(const wxString& turretName) { _choiceTurret->AppendString(turretName); }
  wxString GetActiveTurret() { return _choiceTurret->GetStringSelection(); }
  bool SetActiveTurret(const wxString& turretName) { return _choiceTurret->SetStringSelection(turretName); }
  
private:
  wxCheckBox* _chkVertStabilization;
  wxCheckBox* _chkHorStabilization;
  wxChoice* _choiceTurret;
  wxTextCtrl* _valVSpeed;
  wxSlider* _sldVSpeed;
  wxTextCtrl* _valHSpeed;
  wxSlider* _sldHSpeed;

#if !TURRET_CONTROL_ONLY_SPEED
  wxSlider* _sldElevation;
  wxSlider* _sldAzimuth;

  wxStaticText* _valAzimuth;
  wxStaticText* _valElevation; 

  wxTextCtrl* _valAzimuthWanted;
  wxTextCtrl* _valElevationWanted;

  wxSlider* _sldAzimuthWanted;
  wxSlider* _sldElevationWanted; 
#endif

  void OnVStab(wxCommandEvent& e);
  void OnHStab(wxCommandEvent& e);
  void OnVSpeedSlider(wxScrollEvent& e);
  void OnVSpeedEnter(wxCommandEvent& e);
  void OnHSpeedSlider(wxScrollEvent& e);
  void OnHSpeedEnter(wxCommandEvent& e);
#if !TURRET_CONTROL_ONLY_SPEED
  void OnElevSlider(wxScrollEvent& e);
  void OnElevEnter(wxCommandEvent& e);
  void OnAzimSlider(wxScrollEvent& e);
  void OnAzimEnter(wxCommandEvent& e);
#endif
  void OnSelectTurret(wxCommandEvent& e);

  void OnTimer(wxTimerEvent& event);

  DECLARE_EVENT_TABLE();
};

#endif // TurretControlGUI_h__
