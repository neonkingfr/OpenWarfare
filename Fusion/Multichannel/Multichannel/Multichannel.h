#include <windows.h>

#define VBSPLUGIN_EXPORT __declspec(dllexport)

struct FrustumSpec
{
	// camera position
	float _pointPosX;
	float _pointPosY;
	float _pointPosZ;

	// camera speed, used for OpenAL sounds
	float _pointSpeedX;
	float _pointSpeedY;
	float _pointSpeedZ;

	// camera view direction
	float _viewDirX;
	float _viewDirY;
	float _viewDirZ;

	// camera up-view
	float _viewUpX;
	float _viewUpY;
	float _viewUpZ;

	// camera projection angle tangents
	// Beware: for now, top/bottom and left/right pairs are used as single (symmetric) values
	float _projTanTop;
	float _projTanBottom;
	float _projTanLeft;
	float _projTanRight;

	// near, far distance clipping
	float _clipDistNear;
	float _clipDistFar;
	float _clipDistFarShadow;
	float _clipDistFarSecShadow;
	float _clipDistFarFog;

	// standard constructor
	FrustumSpec():
	_pointPosX(0), _pointPosY(0), _pointPosZ(0), _pointSpeedX(0), _pointSpeedY(0), _pointSpeedZ(0),
		_viewDirX(0), _viewDirY(0), _viewDirZ(0), _viewUpX(0), _viewUpY(0), _viewUpZ(0),
		_projTanTop(0), _projTanBottom(0), _projTanLeft(0), _projTanRight(0),
		_clipDistNear(0), _clipDistFar(0), _clipDistFarShadow(0), _clipDistFarSecShadow(0), _clipDistFarFog(0)
	{}

	// auxiliary setting functions
	void SetPos(float posX, float posY, float posZ) {_pointPosX = posX; _pointPosY = posY; _pointPosZ = posZ;}
	void SetSpeed(float speedX, float speedY, float speedZ) {_pointSpeedX = speedX; _pointSpeedY = speedY; _pointSpeedZ = speedZ;}
	void SetDir(float dirX, float dirY, float dirZ) {_viewDirX = dirX; _viewDirY = dirY; _viewDirZ = dirZ;}
	void SetUp(float upX, float upY, float upZ) {_viewUpX = upX; _viewUpY = upY; _viewUpZ = upZ;}
	void SetProj(float top, float bottom, float left, float right) {_projTanTop = top; _projTanBottom = bottom; _projTanLeft = left; _projTanRight = right;}
	void SetClip(float nearStd, float farStd, float farShadow, float farSecShadow, float farFog)
	{
		_clipDistNear = nearStd; _clipDistFar = farStd; _clipDistFarShadow = farShadow; _clipDistFarSecShadow = farSecShadow; _clipDistFarFog = farFog;
	}

};

VBSPLUGIN_EXPORT void WINAPI OnSetFrustum(FrustumSpec *frustum);
VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT);
VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input);
VBSPLUGIN_EXPORT void WINAPI OnMissionStart();
VBSPLUGIN_EXPORT void WINAPI OnMissionEnd();