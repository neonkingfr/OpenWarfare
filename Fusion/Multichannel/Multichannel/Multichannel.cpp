///////////////////////////////////////////////////////////////////////////////
/* Multichannel Fusion Plugin : Proof of Concept : Works in VBS2 1.6x
//
// VBS2 host/client shortcuts and VC0x.cfg files still have to be set up, but there is no need
// for init.sqf, viewclient.sqf or multichannel.cfg. Future plans are to include a config
// utility which will allow simple entry of viewclient numbers and parameters, this will
// generate an XML file which the plugin will then read to set the frustums and offsets
// rather than using the hard coded values that it does presently
//
// How to Use :
// Launch Viewserver and Viewclients(3) as normal, the viewclients will initialize automatically
// giving a view that rotates with the player. A view that does not rotate with the player can be
// achieved by entering the debug(or developer) console, and entering ...
// ret = fusionFunction["Multichannel.dll", "s"]
// to set back to rotating view, enter ...
// ret = fusionFunction["Multichannel.dll", "r"]
//
// For more details please see comments in code.
*/
//////////////////////////////////////////////////////////////////////////////
#include "VBS2Fusion.h" 
#include "exports.h"
#include "Multichannel.h"
#define _USE_MATH_DEFINES // fix for M_PI define
#include <math.h>
#include <sstream>

using namespace VBS2Fusion;
using namespace std;

// entity variables
Unit playerUnit;

// view client variables
int viewClientCount;				// number of viewClients
string vcID01, vcID02, vcID03;		// string ref for IDs of viewClients

// struct for frustum and offsets
struct VcSettings
{
	string camFrustum;
	string camOffsets;
};
VcSettings vc01, vc02, vc03;

// script strings
string ehCamOnChanged, camSetStr, camConditionStr ;

// flags
volatile bool started = false;		// used for dialog output if debug trace is implemented
volatile bool staticView = false;	// used to switch between static and rotating views

// In the OnSetFrustum callback, we set the view direction of the viewclient
// cameras, this locks them to a directional setting meaning that the cameras
// will not rotate with the player
VBSPLUGIN_EXPORT void WINAPI OnSetFrustum(FrustumSpec *frustum)
{
	if(staticView)
	{
		if (MissionUtilities::isViewClient())
		{
			string exCmd = "getViewClientID";
			string vcID = ExecutionUtilities::ExecuteStringAndReturn(exCmd);
			if(vcID.compare("\"VC01\"") == 0)
			{
	///////////////// Vector Direction Setting //////////////
				// 150deg = 60 deg to left of main dir
				frustum->_viewDirX = cos(150.0f * (M_PI/180));
				frustum->_viewDirZ = sin(150.0f * (M_PI/180));
				frustum->_viewDirY = 0;
				frustum->_viewUpX = 0;
				frustum->_viewUpZ = 0;
				frustum->_viewUpY = 1;
			}
			else if(vcID.compare("\"VC02\"") == 0)
			{
	///////////////// Vector Direction Setting //////////////
				// 90 deg = main dir (north)
				frustum->_viewDirX = cos(90.0f * (M_PI/180));
				frustum->_viewDirZ = sin(90.0f * (M_PI/180));
				frustum->_viewDirY = 0;
				frustum->_viewUpX = 0;
				frustum->_viewUpZ = 0;
				frustum->_viewUpY = 1;
			}
			else if(vcID.compare("\"VC03\"") == 0)
			{
	///////////////// Vector Direction Setting //////////////
				// 30deg = 60deg right of main dir
				frustum->_viewDirX = cos(30.0f * (M_PI/180));
				frustum->_viewDirZ = sin(30.0f * (M_PI/180));
				frustum->_viewDirY = 0;
				frustum->_viewUpX = 0;
				frustum->_viewUpZ = 0;
				frustum->_viewUpY = 1;
			}

		}
	}
}
VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
	// debug trace output can be coded in here
}
VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
	static const char result[] = "[player]";
	if(input[0] == 'b')				// b stands for begin
	{
		// this starts the plugin and sets up values for player unit
		// view client IDs and values for frustum and offsets
		if (!MissionUtilities::isViewClient())
		{
			// start with rotating view
			staticView = false;

			// get player unit 
			playerUnit = MissionUtilities::getPlayer();
			UnitUtilities::updateStaticProperties(playerUnit);
			UnitUtilities::updateDynamicProperties(playerUnit);
	
			// set up view client settings
			viewClientCount = 3;
			vcID01 = "VC01";
			vcID02 = "VC02";
			vcID03 = "VC03";
			
			// here we set up the view client frustums. The numerical frustum values are
			// the computed tangents of the angles (todo: make this more elegant and end user friendly)
			// tan(30) = 0.57735, tan(15) = 0.26785
			vc01.camFrustum = "[true, 0.57735, 0.57735, 0.26795, 0.26795]";
			vc01.camOffsets = "[true, -60, 0, 0, 0, 0, 0]";
			vc02.camFrustum = "[true, 0.57735, 0.57735, 0.26795, 0.26795]";
			vc02.camOffsets = "[true, 0, 0, 0, 0, 0, 0]";
			vc03.camFrustum = "[true, 0.57735, 0.57735, 0.26795, 0.26795]";
			vc03.camOffsets = "[true, 60, 0, 0, 0, 0, 0]";
	
			// camera creation call
			PluginFunction("a");
	
			// add event handler CameraOnChanged through script (as it doesnt exist in fusion)
			ehCamOnChanged = "addGlobalEventHandler[\"CameraOnChanged\", {_nul = fusionFunction[\"Multichannel.dll\", \"a\"]}]";
			ExecutionUtilities::ExecuteStringAndReturn(ehCamOnChanged);
		}
	}
	else if(input[0] == 'a') // a stands for 'activate'
	{
		// call camera creation call on all clients except the server
		// to do this we set up the script commands and then execute them 
		// on all clients except the server (but executed from the server)
		if (!MissionUtilities::isViewClient())
		{
			for (int i = 0; i < viewClientCount; ++i)
			{
						switch (i)
						{
						case 0:
							{
								stringstream ss1, ss2;
	
								ss1 << "_this switchCamera \"View\"; _cam = setCamFrustum " << vc01.camFrustum << "; setCamFrustumOffsets " << vc01.camOffsets << "; hideUI false;";
								camSetStr = ss1.str();
	
								ss2 << "getViewClientID == \"" << vcID01 << "\"";
								camConditionStr = ss2.str();
	
								ExecutionUtilities::ExecuteStringPublic(camSetStr, camConditionStr, playerUnit, false);
	
								break;
							}
						case 1:
							{
								stringstream ss1, ss2;
	
								ss1 << "_this switchCamera \"View\"; _cam = setCamFrustum " << vc02.camFrustum << "; setCamFrustumOffsets " << vc02.camOffsets << "; hideUI false;";
								camSetStr = ss1.str();
	
								ss2 << "getViewClientID == \"" << vcID02 << "\"";
								camConditionStr = ss2.str();
	
								ExecutionUtilities::ExecuteStringPublic(camSetStr, camConditionStr, playerUnit, false);
	
								break;
							}
						case 2:
							{
								stringstream ss1, ss2;
	
								ss1 << "_this switchCamera \"View\"; _cam = setCamFrustum " << vc03.camFrustum << "; setCamFrustumOffsets " << vc03.camOffsets << "; hideUI false;";
								camSetStr = ss1.str();
	
								ss2 << "getViewClientID == \"" << vcID03 << "\"";
								camConditionStr = ss2.str();
	
								ExecutionUtilities::ExecuteStringPublic(camSetStr, camConditionStr, playerUnit, false);
	
								break;
							}
						}
					}
		}
	}

	// if we wish the view to rotate with the player call this (r stands for rotate)
	// ret = fusionFunction["Multichannel.dll", "r"]
	else if (input[0] == 'r')
	{
		ExecutionUtilities::ExecuteFusionFunctionPublic("Multichannel.dll", "t", false);
	}

	// if we wish the view to NOT rotate with the player call this (s stands for static)
	// ret = fusionFunction["Multichannel.dll", "s"]
	else if (input[0] == 's')
	{
		ExecutionUtilities::ExecuteFusionFunctionPublic("Multichannel.dll", "v", false);
	}

	// these methods need to be called from the viewserver and then executed on viewclients
	// this is because each VBS instance loads its own version of the DLL and its variables
	// are in protected memory space
	else if (input[0] == 't')
	{
		if (MissionUtilities::isViewClient())
		{
			staticView = false;
		}
	}
	else if (input[0] == 'v')
	{
		if (MissionUtilities::isViewClient())
		{
			staticView = true;
		}
	}
	///////////////////////// Loading and Unloading //////////////////////////////
	//////////////////////////////////////////////////////////////////////////////
	// The two functions below are for loading and unloading the DLL on			//
	// viewclient machines, this eases the development process					//
	// allowing compilation of the plugin while VBS2 is still running			//
	//																			//
	// To unload all DLL instances first call (in debug console)				//
	// ret = fusionFunction["Multichannel.dll", "u"];							//
	// then																		//
	// ret = fusionFunction["Multichannel.dll", "unload"];						//
	// all instances of the DLL will now be unloaded							//
	//																			//
	// To load all DLL instances after compiling first call	(in debug console)	//	
	// ret = fusionFunction["Multichannel.dll", "load"];						//
	// then																		//
	// ret = fusionFunction["Multichannel.dll", "l"];							//
	// all instances of the DLL will now be loaded in							//
	// the host and the viewclients												//
	//////////////////////////////////////////////////////////////////////////////

	// this function call is for loading up the DLL on the viewclient machines (l stands for load)
	else if(input[0] == 'l')
	{
		ExecutionUtilities::ExecuteFusionFunctionPublic("Multichannel.dll", "load", false);
	}
	// this function call is for unloading the DLL on the viewclient machines. (u stands for unload)
	else if(input[0] == 'u')
	{
		ExecutionUtilities::ExecuteFusionFunctionPublic("Multichannel.dll", "unload", false);
	}
	return result;
}

VBSPLUGIN_EXPORT void WINAPI OnMissionStart()
{
	// this is called on start of the mission with a 
	// slight delay in order to make sure initialization
	// has passed
	::Sleep(500);
	PluginFunction("b");
}

VBSPLUGIN_EXPORT void WINAPI OnMissionEnd()
{
	//  possibly some shut down and clean up routines 
	//	to go in here if wanted
}

/*!
Do not modify this code. Defines the DLL main code fragment
*/
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
	switch(fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	}

	return TRUE;
};