#include "buildnavmesh.h"
#include "FileHandler.h"
#include <xait/common/callback/FileIOCallbackWin32.h>
#include <xait/common/memory/MemoryStream.h>
#include <xait/common/io/FileStream.h>
#include <xait/map/build/TriangleClassifier.h> 
#include <xait/map/xaitMap.h>
#include <fstream>

bool BuildNavmesh(const GeometryObject* gObj, void*& navmesh, void*& buildCache)
{
  if (!gObj) return false;

  const XAIT::Common::Math::Vec3f NavMeshGravityDir = XAIT::Common::Math::Vec3f(0.0f, -1.0f, 0.0f);

  XAIT::Map::Build::NavMeshBuilder* navBuilder= XAIT::Map::Interface::createNavMeshBuilder();
  if (!navBuilder) return false;
  if (!navBuilder->getBuildConfig()) 
  {
    delete navBuilder;
    return false;
  }
  
  XAIT::Common::SharedPtr<XAIT::Common::IO::Stream> mSectorCache;
  if (buildCache)
  {
    XAIT::Common::Memory::MemoryStream* cache = (XAIT::Common::Memory::MemoryStream*)buildCache;
    mSectorCache = new(XAIT::Map::MapMemoryPool::getLowAllocThreadSafeAllocator()) XAIT::Common::Memory::MemoryStream("SectorCache"); 
    cache->seek(0, XAIT::Common::IO::Stream::SEEKORIG_START);
    mSectorCache->seek(0, XAIT::Common::IO::Stream::SEEKORIG_START);
    mSectorCache->writeFromStream(cache);
    mSectorCache->seek(0, XAIT::Common::IO::Stream::SEEKORIG_START);
  }
  else mSectorCache = new(XAIT::Map::MapMemoryPool::getLowAllocThreadSafeAllocator()) XAIT::Common::Memory::MemoryStream("SectorCache"); 

  if (!navBuilder->setSectorCache(mSectorCache))
    OutputDebugString("sector cache attach failed");

  if (navmesh)
    navBuilder->setUpdateNavMesh((XAIT::Map::NavMeshContainer*) navmesh);

  XAIT::Common::Memory::Allocator::Ptr allocator = XAIT::Common::Interface::getGlobalAllocator();
  XAIT::Common::Container::Vector<XAIT::Common::Math::Vec3f> vertices(XAIT::Common::Interface::getGlobalAllocator());
  XAIT::Common::Container::Vector<XAIT::Common::Geom::IndexedTriangle> triangles(XAIT::Common::Interface::getGlobalAllocator());

  for (int i = 0; i < gObj->nVertices; i++)
  {
    vertices.pushBack(XAIT::Common::Math::Vec3f(gObj->vertices[i].x, gObj->vertices[i].y, gObj->vertices[i].z));
  }

  for (int i = 0; i < gObj->nFaces; i++)
  {
    triangles.pushBack(XAIT::Common::Geom::IndexedTriangle(gObj->faces[i].indices[0], gObj->faces[i].indices[1], gObj->faces[i].indices[2]));
  }

#if USE_SPAWN_POINTS
  for (int i = 0; i < gObj->spawnPoints.nVertices; i++)
  {
    XAIT::Map::SpawnpointPathObject* spawnPoint = new XAIT::Map::SpawnpointPathObject("");
    GeometryVertex& v = gObj->spawnPoints.vertices[i];
    spawnPoint->setPosition(XAIT::Common::Math::Vec3f(v.x, v.y, v.z));
    navBuilder->addPathObject(spawnPoint);
  }
#endif

  XAIT::Map::Build::TriangleClassifier* mTriClass= new XAIT::Map::Build::SlopeTriangleClassifier(NavMeshMaxSlope * X_DEG2RAD_F, NavMeshGravityDir,0); 

  XAIT::Map::Build::GeometryInfo geomInfo; 
  geomInfo.mNumVertices = vertices.size();
  geomInfo.mVertices = (XAIT::uint08*)&vertices[0];
  geomInfo.mVertexSize = sizeof(XAIT::Common::Math::Vec3f); //Bytes --- float 
  geomInfo.mNumTriangles = triangles.size();
  geomInfo.mIndices = (XAIT::uint08*)&triangles[0]; 
  geomInfo.mTriangleSize = sizeof(XAIT::Common::Geom::IndexedTriangle); 
  geomInfo.mTriangleClassifier= mTriClass; 
  navBuilder->clearInputGeometry();
  navBuilder->addInputGeometry(geomInfo);

  // initialize the character
  XAIT:: Map::CharacterInfo charInfo;
  charInfo.mName = "character";
  charInfo.mPhysicalHeight = NavMeshUnitHeight;
  charInfo.mRadius = NavMeshUnitRadius;
  
  
  //float sectorSize = navBuilder->getBuildConfig()->getSectorSize(true);
  navBuilder->getBuildConfig()->addCharacter(charInfo);
  // set the build parameter for the navmesh
  navBuilder->getBuildConfig()->setUpDirection(NavMeshGravityDir * -1.f);
  navBuilder->getBuildConfig()->setCubeSize(NavMeshCubeSize);
  navBuilder->getBuildConfig()->setSectorSize(NavMeshSectorSize);
  navBuilder->getBuildConfig()->setUnitRadius(NavMeshUnitRadius);
  navBuilder->getBuildConfig()->setStepHeight(NavMeshStepHeight);

  XAIT::Map::NavMeshContainer* navMesh = NULL;

  unsigned long long startTime = GetTickCount();
  navBuilder->startNavMeshGeneration(true, -1);
  unsigned long long endTime = GetTickCount();

  char text[256];
  sprintf(text, "Navmesh rebuilt in %d ms\n", endTime - startTime);
  OutputDebugString(text);

  navMesh = navBuilder->returnBuildResult();

  delete (XAIT::Map::NavMeshContainer*) navmesh;
  navmesh = navMesh;

  if (!navMesh)
    OutputDebugString("Navmesh update failed!\n");

//  XAIT::Map::NavMeshSerializer::writeNavMesh("C:\\ship.xml", navMesh, XAIT::Common::Platform::Endian::ENDIAN_LITTLE);
//  delete navMesh;
//  navMesh = XAIT::Map::NavMeshSerializer::readNavMesh("C:\\ship.xml");

  delete mTriClass;
  delete navBuilder;
  
  return true;
}