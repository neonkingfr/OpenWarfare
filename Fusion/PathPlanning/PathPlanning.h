#include <windows.h>
#include <Common/GeometryObject.h>

#define VBSPLUGIN_EXPORT __declspec(dllexport)

//VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT);
VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input);
VBSPLUGIN_EXPORT void WINAPI RegisterReturnPathFnc(void *returnPathFnc);
VBSPLUGIN_EXPORT void WINAPI UpdateNavmesh(const char* navmeshName, GeometryObject* geom);
VBSPLUGIN_EXPORT void WINAPI SetNavmeshBlockers(const char* navmeshName, GeometryObject* geom);
VBSPLUGIN_EXPORT void WINAPI DrawNavmesh(const char* objectName);
VBSPLUGIN_EXPORT void WINAPI DrawNavmeshExt(void* data, const char* navmesh, bool draw);
VBSPLUGIN_EXPORT bool WINAPI IsOnNavmesh(const char* navmeshName, float* pos);
VBSPLUGIN_EXPORT void WINAPI RegisterGetObjectTransformFnc(void *getObjectTransform);

typedef void (WINAPI * ReturnPathType)(int requestID, float* positions, int n);
typedef void (WINAPI * GetObjectTransformType)(void* data, float* matrix);
