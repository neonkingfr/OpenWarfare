/****************************************************************************
* Plugin for integration xaitment pathplanning into VBS2                    *
*                                                                           *
* You need to have xaitMap Library for Win32 installed.                     *
* You need to set paths to xaitment libraries and headers in Visual Studio. *
*                                                                           *
*****************************************************************************/

#pragma warning(disable: 4996)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// VBS2Fusion interface library forced linking
// Needed for plugins that don't use any of VBS2Fusion symbols
// This will insure that VBS2Fusion_2005 library will be linked to the plugin
//  and the plugin will require to have VBS2Fusion_2005.dll
// It is required by Simcentric that each Fusion plugin links with VBS2Fusion_2005.lib or VBS2_Fusion_2008.lib.
#include "VBS2FusionAppContext.h"
VBS2Fusion::VBS2FusionAppContext appContext;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <windows.h>
#include <stdexcept>
#include "PathPlanning.h"
#include "navmeshcache.h"
#include "buildnavmesh.h"

#include "RefCount.h"

#include "../PrimitivesManager/primitivesmanager.h"
#include "FileHandler.h"

// navmesh draw color
const BI_Color NavmeshColor(0.0f,0.3f,0.0f,1.0f);

// navmesh height above object surface
const float NavmeshHeightOffset = 0.1f;

PrimitivesManager primitivesManager;


#pragma warning (disable: 4996)

#include <xait/map/xaitMap.h>
#include <xait/common/callback/FileIOCallbackWin32.h>
#include <xait/common/memory/MemoryStream.h>
#include <xait/map/build/TriangleClassifier.h> 

using namespace XAIT;
using namespace XAIT::Common;
using namespace XAIT::Common::Math;
using namespace XAIT::Map;

char retStr[512];

std::vector<SmartRef<Primitive3D>> drawTriangles; // geometry prepared for drawing
char drawObjectName[256];                         // for backward compatibility
char drawNavmesh[256];                            // name of the navmesh we want draw
EngineRefCount* drawObject = NULL;                // pointer to object we want draw

// Command function definition
ReturnPathType ReturnPath = NULL;
GetObjectTransformType GetObjectTransform = NULL;
ExecuteCommandType ExecuteCommand = NULL;

// Function that will register the ExecuteCommand function of the engine
VBSPLUGIN_EXPORT void WINAPI RegisterReturnPathFnc(void *returnPathFnc)
{
  ReturnPath = (ReturnPathType) returnPathFnc;
}

VBSPLUGIN_EXPORT void WINAPI RegisterGetObjectTransformFnc(void *getObjectTransform)
{
  GetObjectTransform = (GetObjectTransformType) getObjectTransform;
}

VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc)
{
  ExecuteCommand = (ExecuteCommandType)executeCommandFnc;
}

// navmesh cache functions implementation
class XaitCacheFunctions : public CacheFunctions 
{
  public:
    virtual void* LoadNavmesh(const char* filename)
    {
      return XAIT::Map::NavMeshSerializer::readNavMesh(filename);
    }

    virtual void DeleteNavmesh(void* item)
    {
      if (item)
        delete (XAIT::Map::NavMeshContainer*)item;
    }

    virtual void DeleteBuildCache(void* item)
    {
      if (item)
        delete (XAIT::Common::Memory::MemoryStream*) item;
    }

    virtual int GetFileSize(const char* filename)
    {
      return FileSize(filename);
    }

    virtual void* LoadBuildCache(const char* filename)
    {
      char cacheFile[256];
      strcpy(cacheFile, filename);
      char* postfix = strrchr(cacheFile, '.');
      if (postfix)
        strcpy(postfix, ".xch");
      
      return GetStreamFromFile(cacheFile);
    }

    virtual bool Build(const void* geometry, void*& navmesh, void*& buildCache)
    {
      return BuildNavmesh((const GeometryObject*)geometry, navmesh, buildCache);
    }
};

XaitCacheFunctions xaitCacheFunctions;
NavmeshCache GNavmeshCache(&xaitCacheFunctions);  // global navmesh cache


void GetNavmeshGeometry(const char* navmeshName, std::vector<SmartRef<Primitive3D>>& primitives)
{
  primitives.clear();
  XAIT::Map::NavMeshContainer* navMesh = (XAIT::Map::NavMeshContainer*) GNavmeshCache.GetNavmesh(navmeshName);
  if (navMesh)
  {
    for (uint32 sectorIndex = 0; sectorIndex < navMesh->getNumSectors(); sectorIndex++)
    {
      if(!navMesh->isSectorLoaded(sectorIndex)) continue;

      NavMeshSector* sector = navMesh->getSector(sectorIndex);
      if (!sector) continue;

      for (uint32 nodeIndex = 0; nodeIndex < sector->getNumNodes(); nodeIndex++)
      {
        NavMeshNode& node = sector->getNode(nodeIndex);
        if (node.getPolygonSize() != 3) continue; // we can draw only triangles

        Common::Math::Vec3f v1 = sector->getVertex(node.getPolygonEdge(0).mVertexID);
        Common::Math::Vec3f v2 = sector->getVertex(node.getPolygonEdge(1).mVertexID);
        Common::Math::Vec3f v3 = sector->getVertex(node.getPolygonEdge(2).mVertexID);

        BI_Vector3 v1b(v1[0], v1[1], v1[2]);
        BI_Vector3 v2b(v2[0], v2[1], v2[2]);
        BI_Vector3 v3b(v3[0], v3[1], v3[2]);

        SmartRef<Primitive3D> triangle = new Triangle3D(v1b, v2b, v3b);

        triangle->SetColor(NavmeshColor);

        primitives.push_back(triangle);
      } 
    }
  }
}

// tells engine we don't need drawn object anymore
void StopDrawing()
{
  if (drawObject)
  {
    drawObject->Release();    
  }

  drawObject = NULL;
  drawObjectName[0] = 0;
  drawTriangles.clear();
  drawNavmesh[0] = 0;
}


VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
  StopDrawing();

  static const char resultOK[]="";
  static const char resultNoNavMesh[]="[\"PathPlanningPlugin failed: No navmesh loaded\"]";
  static const char parseError[]="[\"PathPlanningPlugin failed: Input values parse error\"]";

  if (strlen(input) > 500) return parseError;

  if (stricmp(input, "draw stop") == 0) 
  {
    return resultOK;
  }
  
  int parsed = sscanf(input, "draw %s", drawObjectName);
  if (parsed == 1)
  {
    char navmeshNameResult[256];
    char command[256];
    sprintf(command, "navmeshName %s", drawObjectName);
    ExecuteCommand(command, navmeshNameResult, sizeof(navmeshNameResult));

    char* navmeshName = navmeshNameResult + 1;
    
    if (strlen(navmeshName) > 0)
      navmeshName[strlen(navmeshName) - 1] = 0;
    strncpy(drawNavmesh, navmeshName, 256);
    
    GetNavmeshGeometry(drawNavmesh, drawTriangles);

    char text[512];
    sprintf(text, "drawing navmesh %s (%d triangles)\n", drawNavmesh, drawTriangles.size());
    OutputDebugString(text);

    return resultOK;
  }
  return parseError;
}

VBSPLUGIN_EXPORT void WINAPI DrawNavmesh(const char* objectName)
{
  StopDrawing();

  // invalid object name -> stop drawing
  if (!objectName || strlen(objectName) <= 0 || strlen(objectName) > 128) 
  {
    return;
  }
  
  strcpy(drawObjectName, objectName);

  // get navmesh name by calling engine script command
  char navmeshNameResult[256];
  char command[256];
  sprintf(command, "navmeshName %s", drawObjectName);
  ExecuteCommand(command, navmeshNameResult, sizeof(navmeshNameResult));

  // remove quotes
  char* navmeshName = navmeshNameResult + 1;
  if (strlen(navmeshName) > 0)
    navmeshName[strlen(navmeshName) - 1] = 0;
  strncpy(drawNavmesh, navmeshName, 256);

  // load navmesh geometry 
  GetNavmeshGeometry(drawNavmesh, drawTriangles);

  char text[512];
  sprintf(text, "drawing navmesh %s (%d triangles)\n", drawNavmesh, drawTriangles.size());
  OutputDebugString(text);
}

VBSPLUGIN_EXPORT void WINAPI DrawNavmeshExt(void* data, const char* navmesh, bool draw)
{
  StopDrawing();

  if (!draw || !data)
  {
    return;
  }

  if (!GetObjectTransform)
  {
    OutputDebugString("PathPlanning plugin ERROR: can't draw navigation mesh (incompatible FusionHandler.dll version).\n");
    return;
  }

  drawObject = (EngineRefCount*)data;

  strncpy(drawNavmesh, navmesh, 256);

  // load navmesh geometry 
  GetNavmeshGeometry(drawNavmesh, drawTriangles);

  char text[512];
  sprintf(text, "drawing navmesh %s (%d triangles)\n", drawNavmesh, drawTriangles.size());
  OutputDebugString(text);

  if (drawTriangles.size() > 0)
  {
    drawObject->AddRef();
  }
  else 
  {
    StopDrawing();
  }
}

VBSPLUGIN_EXPORT int WINAPI AIPathRequest(unsigned int id, float* from, float* to, const char* navmeshFile)
{
  const int ResultPluginNotFound = -1;  // not used here
  const int ResultOk = 0;
  const int ResultNoNavmesh = 1;
  const int ResultInvalidRequest = 2;
  const int ResultPathNotFound = 3;
  const int ResultNoReturnPathFunc = 4;

  // read the NavMesh
  char text[512];
  sprintf(text, "PathPlanning.dll: from = [%f, %f, %f], to = [%f, %f, %f], navmesh = %s\n", from[0], from[1], from[2], to[0], to[1], to[2],navmeshFile);
  OutputDebugString(text);
  
  XAIT::Map::NavMeshContainer* navMesh = NULL; 
  navMesh = (XAIT::Map::NavMeshContainer*) GNavmeshCache.GetNavmesh(navmeshFile);
  
  if (!navMesh) return ResultNoNavmesh;

  //XAIT_MAP_DLLAPI Common::Math::Vec3f searchNearestPoint(GlobalNodeID& nearestNodeID, const Common::Math::Vec3f& point, float32 radius) const;
  GlobalNodeID nearestNode;
  XAIT::Common::Math::Vec3f fromPos = navMesh->searchNearestPoint(nearestNode, XAIT::Common::Math::Vec3f(from[0], from[1], from[2]), 5);
  XAIT::Common::Math::Vec3f toPos = navMesh->searchNearestPoint(nearestNode, XAIT::Common::Math::Vec3f(to[0], to[1], to[2]), 5);

  if (!ReturnPath) 
  {
    return ResultNoReturnPathFunc;
  }
  // PathPlanning code start
  // Create a path search.
  // The path search type specifies how the navmesh should be split up into a graph
  // so that a path can be searched. There are three different types of path searches
  // MidpointPathSearch		Searches across the midpoints of the navigation mesh. This search
  //							is fast but deliverers inaccurate path results if the navigation mesh
  //							has some very big navigation nodes.
  // PolyEdgePathSearch		Searches across the edges of the navigation mesh. This search is slower
  //							than the midpoint search but delivers better path results.
  // SubdivisionPathSearch	Searches also across the edges of the navigation mesh, but splits edges
  //							if they are too long. This delivers even better results than PolyEdgePathSearch
  //							but is also two times slower.
  Path::PathSearch* search = new Path::MidpointPathSearch(navMesh);

  // In order to run a path search on the search, we need to create a path search instance. A path search
  // instance holds the runtime information of the path search. If you want todo several path searches synchronously,
  // you have to create an instance for every synchronous running path search. Instances can be reused after the
  // search request has been finished. In a normal, non-threaded environment, you need only one path search instance.
  Path::PathSearchInstance* instance= search->createInstance();

  // A new search is started by filling a search request.
  // Each search request needs a start-/endpoint pair and a search config
  // which specifies the path costs. The search config has several possibilities
  // to specify cost factors for different characters and different surface types.
  // See the API documentation for more information on that.
  Path::SearchConfig searchConfig;
  
  Path::SearchRequest request(fromPos, toPos, &searchConfig);

  Map::Path::SearchRequest::PathSmoothing smoothingType = Path::SearchRequest::PS_SMOOTHING;
  request.setPathSmoothing(smoothingType);


  // now we initialize the path search instance with a new search request. This will check
  // if a path for the specified start and end positions can be found. If not the init returns
  // false. Further information can be queried with getErrorCode
  instance->init(&request);

  // The state and error code of a path request can be queried on the search request object.
  if (request.getErrorCode() != Path::SearchRequest::ERROR_NO_ERROR)
  {
    //sprintf(retStr, "[\"no Path to Target:[%f,%f,%f] -> [%f,%f,%f]\"]", fromX,fromY,fromZ,toX,toY,toZ);
    //return retStr;
    return ResultInvalidRequest;
  }

  // Now that the path search is initialized, we can start the path search.
  // A call to PathSearchInstance::run processes a part of the path search.
  // The interface provides a time slice mechanism, where you can specify the
  // a amount of internal steps of the path search. The method run returns true
  // as long the path search is in state Running. If it succeeds or fails, it returns
  // false. If the steps parameter is omitted, the call to run is blocking until the path
  // search is finished.
  while (instance->run(1000))
  {}

  // now we can query the state of the path search, if it has failed or succeeded.
  if (request.getSearchState() != Path::SearchRequest::SEARCH_SUCCEEDED || request.getPath()->getNumSegments() <= 0)
    return ResultPathNotFound;

  //0.5f is the characters radius
  //path= request.computeSmoothedPath(0.5f);
  
  // exports path to engine
  int n = request.getPath()->getNumSegments();
  float* resultPath = NULL;
  if (n > 0)
  {
    resultPath = new float[n*3];
    for (int i = 0; i < n; i++)
    {
      resultPath[i*3 + 0] = request.getPath()->getSegment(i).mStartPos[0];
      resultPath[i*3 + 1] = request.getPath()->getSegment(i).mStartPos[1];
      resultPath[i*3 + 2] = request.getPath()->getSegment(i).mStartPos[2];
    }
  }

  ReturnPath(id, resultPath, request.getPath()->getNumSegments());
  // The path object from computeSmoothedPath is maintained externally thus we have to take care for deletion.
  //delete path;
  delete [] resultPath; 
  
  delete instance;
  delete search;
  
  return ResultOk;
}

VBSPLUGIN_EXPORT bool WINAPI IsOnNavmesh(const char* navmeshName, float* pos)
{
  XAIT::Map::NavMeshContainer* navMesh = NULL; 
  navMesh = (XAIT::Map::NavMeshContainer*) GNavmeshCache.GetNavmesh(navmeshName);
  if (!navMesh) return false;
  
  XAIT::Common::Math::Vec3f point(pos[0], pos[1], pos[2]);

  XAIT::Map::GlobalNodeID node = navMesh->searchNode(point);
  
  if (node.isValid()) return true;

  return false;
} 

VBSPLUGIN_EXPORT void WINAPI UpdateNavmesh(const char* navmeshName, GeometryObject* geom)
{
  GNavmeshCache.Build(navmeshName, geom);

  // reload drawn geometry
  if (drawTriangles.size() > 0 && strcmp(navmeshName, drawNavmesh) == 0)
  {
    drawTriangles.clear();
    GetNavmeshGeometry(drawNavmesh, drawTriangles);
  }
}

VBSPLUGIN_EXPORT void WINAPI SetNavmeshBlockers(const char* navmeshName, GeometryObject* geom)
{
  GNavmeshCache.Update(navmeshName, geom);

  // reload drawn geometry
  if (drawTriangles.size() > 0 && strcmp(navmeshName, drawNavmesh) == 0)
  {
    drawTriangles.clear();
    GetNavmeshGeometry(drawNavmesh, drawTriangles);
  }
}


// DllMain
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
	//   GHDll = hDll;
	switch(fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		{
			//OutputDebugString("Called DllMain with DLL_PROCESS_ATTACH\n");
			// initialize the common and navmesh library with
			// the default callbacks delivered by the library itself

      Callback::MemoryCallback::Ptr cbMemory(new Callback::MemoryCallback());
			Callback::FileIOCallback::Ptr cbFileIO(new XaitmentVBSFileHandler());

			Common::Interface::initLibrary(cbMemory);
			Map::Interface::initLibrary(cbFileIO);
		}
		break;
	case DLL_PROCESS_DETACH:
		//OutputDebugString("Called DllMain with DLL_PROCESS_DETACH\n");
    {
      StopDrawing();

      Map::Interface::closeLibrary();
      Common::Interface::closeLibrary();
    }
		break;
	case DLL_THREAD_ATTACH:
		//OutputDebugString("Called DllMain with DLL_THREAD_ATTACH\n");
		break;
	case DLL_THREAD_DETACH:
		//OutputDebugString("Called DllMain with DLL_THREAD_DETACH\n");
		break;
	}
	return TRUE;
}


VBSPLUGIN_EXPORT void WINAPI OnDrawnPassTwo(float zNear, float zFar)
{
  primitivesManager.SetNearFarPlane(zNear, zFar);
  
  if (drawTriangles.size() > 0)
  {
    BI_Matrix4 trans;
    float m[12] = {1,0,0,0,1,0,0,0,1,0,0,0};
    if (drawObject)
    {
      if (GetObjectTransform)
      {
        GetObjectTransform(drawObject, m);
      }
    }
    else
    {
      char matrixText[512];
      char command[256];
      sprintf(command, "transform %s", drawObjectName);
      ExecuteCommand(command, matrixText, sizeof(matrixText));

      int parsed = sscanf(matrixText, "[[%f,%f,%f],[%f,%f,%f],[%f,%f,%f],[%f,%f,%f]]", m, m+2, m+1, m+3, m+5, m+4, m+6, m+8, m+7, m+9, m+11, m+10);
    }
    // create bimatrix
    for (int i = 0; i < 4; i++)
      for (int j = 0; j < 3; j++)
        trans.Set(i, j, m[j + i*3]);

    // shift mesh little bit up
    trans.GetPos()[1] += NavmeshHeightOffset;

    primitivesManager.SetCustomTransform(trans);
    primitivesManager.Add(drawTriangles);
    primitivesManager.Draw();
  }

  // plugin holds the last referentce to object -> we should release it.
  if (drawObject && drawObject->RefCounter() == 1)
  { 
    StopDrawing();
  }
}


// Onload - get the d3d device
VBSPLUGIN_EXPORT int WINAPI OnLoad(VBSParameters *params) 
{
  StopDrawing();
  
  if (params->_device)
    primitivesManager.SetDevice(params->_device);
  return 0;
}

VBSPLUGIN_EXPORT void WINAPI OnUnload() 
{
  StopDrawing();
  primitivesManager.SetDevice(NULL);
}

// D3D device management, setting the engine D3D device
VBSPLUGIN_EXPORT void WINAPI OnD3DeviceInit(IDirect3DDevice9 *device)
{
  primitivesManager.SetDevice(device);
}

// D3D device management, removing the engine D3D device: 
VBSPLUGIN_EXPORT void WINAPI OnD3DeviceDelete()
{
  primitivesManager.SetDevice(NULL);
}

VBSPLUGIN_EXPORT void WINAPI OnSetFrustum(FrustumSpec *frustum)
{
  primitivesManager.SetFrustum(frustum);
}

VBSPLUGIN_EXPORT void WINAPI OnMissionEnd()
{
  StopDrawing();
  GNavmeshCache.Clear();
}
