// River Raid - wave.cpp
// Loading of wav files
// (C) 1996, SUMA

#include <El/elementpch.hpp>
#include <windows.h>
#include <stdio.h>
#include <direct.h>
#include <math.h>
#include <io.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/stat.h>
#include <Es/Framework/consoleBase.h>
#include <Es/Types/pointers.hpp>
#include <Es/Strings/rString.hpp>
#include <Es/Strings/bString.hpp>
#include <El/SoundFile/soundFile.hpp>

const double frame = 0.04;	// frame length in seconds
const double map[] =
{
	0.05, 0,
	0.2, 1,
	0.35, 2,
	0.5, 3,
	0.65, 4,
	0.8, 5,
	0.95, 6,
	0, 7
};


//inline void exc(){throw ios::failure(ios::failbit);}
inline void exc( const char *file )
{
	printf("Error loading file %s",file);
	exit(1);
}

static int ConvertFile(const char *source,const char *dst=NULL)
{
	const char *ext = strrchr(source, '.');
	if (!ext)
	{
		printf("Source file %s has no extension", source);
		return 1;
	}
	ext++;
  RString dest;
  if (dst) 
  {
    dest = RString(dst);
	  const char *destExt = strrchr(dest, '.');
	  if (!destExt)
    {
      if (dest[dest.GetLength()-1]!='\\') dest = dest + "\\";
      const char *fullname = strrchr(source, '\\');
      if (!fullname) dest = dest + RString(source, ext - source) + RString("lip");
      else dest = dest + RString(fullname+1, ext - fullname-1) + RString("lip");
    }
  }
  else
  {
    dest = RString(source, ext - source) + RString("lip");
  }
  printf("%s->%s\n", source,cc_cast(dest));

	// TODO: check time stamps
	Ref<WaveStream> input;
	SoundRequestFile(source,input,true);
	if (input.IsNull())
	{
	  printf("Cannot read file %s", cc_cast(source));
	  return 1;
	}
	
	
	WAVEFORMATEX header;
	input->GetFormat(header);
	if (header.wBitsPerSample != 16 || header.nChannels != 1)
	{
		printf("%s is not 16b mono.", source);
		return 1;
	}
	
	int handle = open(dest, _O_CREAT|_O_TRUNC|_O_BINARY|_O_WRONLY,_S_IREAD|_S_IWRITE);
	if (handle < 0)
	{
		printf("Cannot create file %s", dest);
		return 1;
	}

  // TODO: use streaming instead
  int totalSize = input->GetUncompressedSize();
  AutoArray<char> buf;
  buf.Realloc(totalSize);
  buf.Resize(totalSize);
  int offset = 0;
  int actualRead = input->GetData(buf.Data(),offset,buf.Size());
  if (actualRead<totalSize)
  {
    printf("Error while reading %s at %d",cc_cast(source),offset);
    return 1;
  }
  offset += actualRead;
  
	{
		BString<256> text;
		sprintf(text, "frame = %.3f\r\n", frame);
		write(handle, text, strlen(text));
	}

	const short *data = (const short *)buf.Data();
	int len = buf.Size() / 2;
	int lenFrame = (int)(frame * header.nSamplesPerSec);	// toInt
	int nFrames = len / lenFrame;
	double *values = new double[nFrames];
	double maxValue = 0;
	for (int f=0; f<nFrames; f++)
	{
		double sum = 0;
		int oldValue = *(data++);
		for (int i=1; i<lenFrame; i++)
		{
			int value = *(data++);
			sum += abs(value - oldValue);
		}
		values[f] = sum;
		if (sum > maxValue) maxValue = sum;
	}
	if (maxValue <=0)
	{
		printf("Source file %s contain silent only", source);
		return 1;
	}
	double invMaxValue = 1.0 / maxValue;

	double dt = lenFrame * (1.0 / header.nSamplesPerSec);
	double time = 0;
	double current = -1;
	for (int f=0; f<nFrames; f++)
	{
		double value = values[f] * invMaxValue;
		double lip = -1;
		for (int m=0; ; m+=2)
		{
			if (map[m] == 0 || value <= map[m])
			{
				lip = map[m + 1];
				break;
			}
		}
#if 1
		if (lip != current)
		{
			char text[256];
			sprintf(text, "%.3f, %.0f\r\n", time, lip);
			write(handle, text, strlen(text));
			current = lip;
		}
#else
		char text[256];
		sprintf(text, "%.3f, %.0f, %.0f\r\n", time, sum, lip);
		write(handle, text, strlen(text));
#endif
		time += dt;
	}
	if (current != -1)
	{
		char text[256];
		sprintf(text, "%.3f, -1\r\n", time);
		write(handle, text, strlen(text));
	}

	delete [] values;
	close(handle);
	return 0;
}

int consoleMain(int argc, const char *argv[])
{
  argv++;
  argc--;
  if (argc==0)
  {
    printf("Usage:\tWaveToLip <source> [<destination>]\n");
    return 1;
  }
  while (argc > 0)
  {
    const char *arg = *argv++;
    argc--;

    WIN32_FIND_DATA data;
    HANDLE found = FindFirstFile(arg, &data);
    if (found == INVALID_HANDLE_VALUE)
      continue;

    RString dir;
    const char *ext = strrchr(arg, '\\');
    if (ext) dir = RString(arg, ext - arg + 1);
    RString name = dir + RString(data.cFileName);
    
    const char *dst = 0;
    if (argc > 0)
    {
      dst= *argv++;
      argc--;
    }
    int err = ConvertFile(name,dst);
    FindClose(found);
    return err;
  }
  return 0;
}
