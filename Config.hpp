#include <string>
#include <vector>
#include <fstream>

#include "LexicalAnalyser.hpp"
#include "RefCount.hpp"

#ifndef _CONFIG_FILE_CHECKER
#define _CONFIG_FILE_CHECKER

enum ConfigDataType
{
  DTParmaFile,
  DTClass,
  DTArray,
  DTString,
  DTFloat,
  DTInt,
  DTBool,
  DTUnknown
};

// types
class ConfigClass;
class ConfigArray;

// Empty class, didn't see that coming!
class IConfigType : public RefCount
{
public:
#define DEFINETYPE(TYPE) \
  virtual bool SetVal(const TYPE &type){return false;}\
  virtual bool GetVal(TYPE &type){return false;}\

  DEFINETYPE(std::string);
  DEFINETYPE(bool);
  DEFINETYPE(float);

  // conversion from float to int, for config values of scalar
  // applies when the configType is scalar(float) and gets passed an int
  virtual bool SetVal(const int &type)
  {
    float val = (float) type;
    return SetVal(val);
  }
  virtual bool GetVal(int &type)
  {
    float val;
    if (GetVal(val))
    {
      type = (int)val;
      return true;
    }
    return false;
  }

  virtual ConfigDataType GetType(){ return DTUnknown; }
  
  // By default, everything is a entry. You just need to 
  // know its data type!
  virtual bool IsClass(){ return false; }
  virtual bool IsEntry(){ return true; }
  virtual bool IsArray(){ return false; }
  virtual bool IsInteger() { return GetType() == DTInt; }
  virtual bool IsScalar(){ return GetType() == DTFloat; }
  virtual bool IsString(){return GetType() == DTString;}
  virtual bool IsBool(){return GetType() == DTBool;}

  // Note dangerous
  ConfigClass  *GetClass();
  ConfigArray  *GetArray();

  virtual void SaveFileConfig(ofstream &outputStream, int indent = 0);
};

class ConfigBaseType : public IConfigType
{
};


template<class TYPE,ConfigDataType configDataType>
class ConfigType : public ConfigBaseType
{
  TYPE        _val;
public:
  ConfigType()
  {
  }

  // Default constructor
  ConfigType(TYPE &type)
  {
    _val = type;
  }
  virtual bool SetVal(const TYPE &type)
  {
    _val = type;
    return true;
  }
  virtual bool GetVal(TYPE &type)
  {
    type = _val;
    return true;
  }

  virtual ConfigDataType GetType(){ return configDataType; };
};

// We generate the necessary template holders
typedef ConfigType<bool,DTBool>          ConfigBool;
typedef ConfigType<float,DTFloat>        ConfigScalar; 
typedef ConfigType<int, DTInt>           ConfigInt;
typedef ConfigType<std::string,DTString> ConfigString;

class ConfigArray : public IConfigType , public std::vector< SmartRef<IConfigType> >
{
public:
  virtual bool IsArray(){ return true; }
  ConfigDataType GetType(){ return DTArray; }

  virtual void SaveFileConfig(ofstream &outputStream, int indent = 0);
};

class ConfigEntry : public RefCount
{
  std::string _name;
  SmartRef<IConfigType> _entry;

public:
  ConfigEntry(){};
  ConfigEntry(std::string name);
  ConfigEntry(std::string name, IConfigType *entry);

  // Return the entry name.
  std::string GetEntryName(){return _name; }

  virtual ConfigEntry *FindEntry(const std::string name);
  void SetEntry(IConfigType *entry);

  // By default, everything is a entry. You just need to 
  // know its data type!
  bool IsClass(){ return _entry ? _entry->IsClass() : false; }
  bool IsEntry(){ return _entry ? _entry->IsEntry() : false; }
  bool IsArray(){ return _entry ? _entry->IsArray() : false; }
  bool IsInteger(){ return _entry ? _entry->IsInteger() : false; }
  bool IsScalar(){ return _entry ? _entry->IsScalar() : false; }
  bool IsString(){ return _entry ? _entry->IsString() : false; }
  bool IsBool(){ return _entry ? _entry->IsBool() : false; }

  // Query the internal element
  ConfigClass  *GetClass(){ return _entry ? _entry->GetClass() : NULL; }
  ConfigArray  *GetArray(){ return _entry ? _entry->GetArray() : NULL; }

  ConfigDataType GetType();
  
  template<class TYPE>
  bool GetVal(TYPE& val){return _entry->GetVal(val);}
  
  template<class TYPE>
  bool SetVal(const TYPE& val){ return _entry->SetVal(val);}

  virtual void SaveFileConfig(ofstream &outputStream, int indent = 0);
};

// Config class Is a IConfigType, and a ConfigEntry because it has a name
// and also contains an arrays of ConfigEntrys.
class ConfigClass : public IConfigType ,public std::vector< SmartRef<ConfigEntry> >
{
  ConfigEntry *_configEntry;
public:
  ConfigClass()
  {
    _configEntry = NULL;
  }
  virtual ~ConfigClass()
  {
    _configEntry = NULL;
    clear();
  }
  ConfigClass(ConfigEntry *configEntry)
  {
    _configEntry = configEntry;
  }
  void SetConfigClassEntry(ConfigEntry *entry){_configEntry = entry;}
  // Get the class name.
  std::string GetName(){ return _configEntry->GetEntryName(); }

  virtual ConfigEntry *FindEntry(const std::string name);
  void Add(SmartRef<ConfigEntry> &entry);
  void Add(ConfigEntry *entry);
  void Clear(){ clear();}

  // Simple get Val from class
  template<class TYPE>
  void GetVal(const char *findEntry,TYPE &getVal)
  {
    ConfigEntry *entry = FindEntry(findEntry);
    if (entry) entry->GetVal(getVal);
  }

  // Return this is a class.
  bool IsClass();
  bool IsEntry();

  // Return that this a class type.
  ConfigDataType GetType(){return DTClass;}
  virtual void SaveFileConfig(ofstream &outputStream, int indent = 0);
};

// Responsibility's is to parse everything.
class ParamaFileConfig : public ConfigClass
{
public:
  // No main important constructor
  ParamaFileConfig(){}
  // The config file is actually no type.. its just a holder.
  bool IsClass(){return false;}
  bool IsEntry(){return false;}

  // Make it simpler to remember how to load and save the config.
  virtual void LoadFile(const char *path);
  virtual void LoadFile(ifstream &inputStream);
  // User can save a ParamFile on the fly.
  virtual void SaveFile(ofstream &outputStream, int indent = 0);

  ConfigDataType GetType(){return DTParmaFile;}
};

#endif