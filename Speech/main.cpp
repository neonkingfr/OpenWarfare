/**
    @file   main.cpp
    @brief  Speech test application.

    Copyright &copy; 2000-2002 by Josef Pelikan, MFF UK Prague
    http://cgg.ms.mff.cuni.cz/~pepca/
    @date   19.9.2002
*/

#include "essencepch.hpp"
#include "wave.h"

//-----------------------------------------------------------
//  command-line parameters:

static int ac;
static char **av;

char *nextPar ()                // returns the next command-line parameter
{
    if ( ac ) {
        ac--;
        return( *av++ );
        }
    return NULL;
}

bool isPar ()
{
    return( ac > 0 );
}

//-----------------------------------------------------------
//  commands:

int decimate ()
{
		// parameters: decimate <input.wav> <output.wav>
	if ( !isPar() ) {
		fprintf(stderr,"Input WAV file name is missing!\n");
		return 1;
		}
	char *inFile = nextPar();
	if ( !isPar() ) {
		fprintf(stderr,"Output WAV file name is missing!\n");
		return 1;
		}
	char *outFile = nextPar();

    Wave w(0);
    if ( w.load(inFile) ) {
		fprintf(stderr,"Cannot open '%s'!\n",inFile);
		return 1;
        }

    printf("Original energy: %9.6f, peak: %9.6f\n",w.energy(),w.peak());
        // replay #1: original data
    w.replay(40);
    w.decimate();
    printf("Decimated energy:%9.6f, peak: %9.6f\n",w.energy(),w.peak());
        // replay #2: decimated data
    w.save(outFile);
    w.replay(40);

	return 0;
}

int gauss ()
{
		// parameters: gauss <input.wav> <output.wav> [<frequency>]
	if ( !isPar() ) {
		fprintf(stderr,"Input WAV file name is missing!\n");
		return 1;
		}
	char *inFile = nextPar();
	if ( !isPar() ) {
		fprintf(stderr,"Output WAV file name is missing!\n");
		return 1;
		}
	char *outFile = nextPar();
    int frequency = 8000;
    if ( isPar() )
        frequency = atoi(nextPar());

    Wave w(0);
    if ( w.load(inFile) ) {
		fprintf(stderr,"Cannot open '%s'!\n",inFile);
		return 1;
        }

    printf("Original energy: %9.6f, peak: %9.6f\n",w.energy(),w.peak());
        // replay #1: original data
    w.replay(40);
    w.decimateGauss(frequency);
    printf("Resampled energy:%9.6f, peak: %9.6f\n",w.energy(),w.peak());
        // replay #2: resampled data
    w.save(outFile);
    w.replay(40);

	return 0;
}

int crop ()
{
		// parameters: crop <input.wav> <output.wav> <from> <to>
	if ( !isPar() ) {
		fprintf(stderr,"Input WAV file name is missing!\n");
		return 1;
		}
	char *inFile = nextPar();
	if ( !isPar() ) {
		fprintf(stderr,"Output WAV file name is missing!\n");
		return 1;
		}
	char *outFile = nextPar();
	if ( !isPar() ) {
		fprintf(stderr,"Start time is missing!\n");
		return 1;
		}
    double start = atof(nextPar());
	if ( !isPar() ) {
		fprintf(stderr,"Stop time is missing!\n");
		return 1;
		}
    double stop = atof(nextPar());
    if ( stop <= start ) {
		fprintf(stderr,"Empty output wave - ignored!\n");
		return 1;
        }

    Wave w(0);
    if ( w.load(inFile) ) {
		fprintf(stderr,"Cannot open '%s'!\n",inFile);
		return 1;
        }

    printf("Original energy: %9.6f, peak: %9.6f\n",w.energy(),w.peak());
    w.crop(start,stop);
    printf("Cropped energy:  %9.6f, peak: %9.6f\n",w.energy(),w.peak());
    w.save(outFile);
    w.replay(40);

	return 0;
}

int readRaw ()
{
		// parameters: raw <frequency> <depth> <endian-conversion> <input.raw> [<output.wav>]
	if ( !isPar() ) {
		fprintf(stderr,"RAW frequency is missing!\n");
		return 1;
		}
    int frequency = atoi(nextPar());
	if ( !isPar() ) {
		fprintf(stderr,"RAW depth is missing!\n");
		return 1;
		}
    int depth = atoi(nextPar());
    if ( depth < 12 ) depth = 8;
    else              depth = 16;
	if ( !isPar() ) {
		fprintf(stderr,"RAW endian-conversion is missing!\n");
		return 1;
		}
    bool endian = atoi(nextPar()) > 0;
	if ( !isPar() ) {
		fprintf(stderr,"Input RAW file name is missing!\n");
		return 1;
		}
	char *inFile = nextPar();
    char *outFile = NULL;
    if ( isPar() )
        outFile = nextPar();

    Wave w(0);
    if ( w.loadRaw(inFile,frequency,depth,endian) ) {
		fprintf(stderr,"Cannot open '%s'!\n",inFile);
		return 1;
        }
    if ( outFile )
        w.save(outFile);
    w.replay(40);

    return 0;
}

int textDump ()
{
		// parameters: dump <input.wav> [<output.txt>]
	if ( !isPar() ) {
		fprintf(stderr,"Input WAV file name is missing!\n");
		return 1;
		}
	char *inFile = nextPar();
	char *outFile = "dump.txt";
	if ( isPar() )
        outFile = nextPar();

    Wave w(0);
    if ( w.load(inFile) ) {
		fprintf(stderr,"Cannot open '%s'!\n",inFile);
		return 1;
        }
    w.textDump(outFile);

    return 0;
}

/*
    8kHz .. 8000 samples/s
    1 frame (20ms) = 160 samples .. compressed to 8 bytes (64 bits)
    1 sec = 50 frames .. compressed to 50*8 = 400 bytes (3200 bits)
    compression ratio = 64000 : 3200 = 20 : 1
*/
int encodeLpc ()
{
		// parameters: lpce <input.wav> <output.lpc> [<reconstruction.wav>]
	if ( !isPar() ) {
		fprintf(stderr,"Input WAV file name is missing!\n");
		return 1;
		}
	char *inFile = nextPar();
	if ( !isPar() ) {
		fprintf(stderr,"Output LPC file name is missing!\n");
		return 1;
		}
	char *outFile = nextPar();
    char *recFile = "reconstruction.wav";
    if ( isPar() )
        recFile = nextPar();

    Wave w(0);
    if ( w.load(inFile) ) {
		fprintf(stderr,"Cannot open '%s'!\n",inFile);
		return 1;
        }

    w.normalize(0.4);
    unsigned64 start = getSystemTime();
    w.encode(160,38);
    unsigned64 stop  = getSystemTime();
    double encTime = 1.e-6 * (stop - start);
    printf("Encoding time: %.5f sec (%.2f%% CPU)\n",encTime,encTime*w.frequency*100.0/w.length);
    w.saveCode(outFile);
    printf("Original energy:       %9.6f, peak: %9.6f\n",w.energy(),w.peak());
    w.replay(40);
    start = getSystemTime();
    w.decode();
    stop  = getSystemTime();
    encTime = 1.e-6 * (stop - start);
    printf("Decoding time: %.5f sec (%.2f%% CPU), rate: %u bits/sec\n",
           encTime,encTime*w.frequency*100.0/w.length,w.rate());
    printf("Reconstruction energy: %9.6f, peak: %9.6f\n",w.energy(),w.peak());
    w.normalize(0.9);
    w.save(recFile);
    w.replay(40);

    return 0;
}

int decodeLpc ()
{
		// parameters: lpcd <input.lpc> [<reconstruction.wav>]
	if ( !isPar() ) {
		fprintf(stderr,"Input LPC file name is missing!\n");
		return 1;
		}
	char *inFile = nextPar();
    char *outFile = "reconstruction.wav";
    if ( isPar() )
        outFile = nextPar();

    Wave w(0);
    w.loadCode(inFile);
    unsigned64 start = getSystemTime();
    w.decode();
    unsigned64 stop  = getSystemTime();
    double decTime = 1.e-6 * (stop - start);
    printf("Decoding time: %.5f sec (%.2f%% CPU), rate: %u bits/sec\n",
            decTime,decTime*w.frequency*100.0/w.length,w.rate());
    w.save(outFile);
    w.replay(40);

    return 0;
}

//-----------------------------------------------------------
//  main routine:

int CDECL main ( int argc, char *argv[] )
{
    ac = argc; av = argv;
    nextPar();
    if ( !isPar() ) {
      help:
        printf("Usage: speech decimate <input.wav> <output.wav>\n"
               "       speech gauss <input.wav> <output.wav> [<frequency>]\n"
               "       speech crop <input.wav> <output.wav> <from> <to>\n"
               "       speech raw <frequency> <depth> <endian> <input.raw> [<output.wav>]\n"
               "       speech dump <input.wav> [<output.txt>]\n"
               "       speech lpce <input.wav> <output.lpc> [<reconstruction.wav>]\n"
               "       speech lpcd <input.lpc> [<reconstruction.wav>]\n");
        return 1;
        }
    const char *command = nextPar();

    if ( !strcmp(command,"decimate") )      // decimate <input.wav> <output.wav>
        return decimate();

    if ( !strcmp(command,"gauss") )         // gauss <input.wav> <output.wav> [<frequency>]
        return gauss();

    if ( !strcmp(command,"crop") )          // crop <input.wav> <output.wav> <from> <to>
        return crop();

    if ( !strcmp(command,"raw") )           // raw <frequency> <depth> <endian-conversion> <input.raw> [<output.wav>]
        return readRaw();

    if ( !strcmp(command,"dump") )          // dump <input.wav> [<output.txt>]
        return textDump();

    if ( !strcmp(command,"lpce") )          // lpce <input.wav> <output.lpc> [<reconstruction.wav>]
        return encodeLpc();

    if ( !strcmp(command,"lpcd") )          // lpcd <input.lpc> [<reconstruction.wav>]
        return decodeLpc();

    printf("Unknown command \"%s\"!\n\n",command);
    goto help;
}
