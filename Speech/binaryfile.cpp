/**
    @file   binaryfile.cpp
    @brief  Binary input/output file object.

    Copyright &copy; 1998-2002 by Josef Pelikan, MFF UK Prague
    http://cgg.ms.mff.cuni.cz/~pepca/
    @date   4.9.2002
*/

#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include "binaryfile.h"

//-----------------------------------------------------------
//  binary input/output file object:

BinaryFile::BinaryFile ( const char *fileName, bool wr, BYTE comm, unsigned buflen )
    // opens the binary file for reading/writting, sets 'error' variable
{
    buf = NULL;
    f = NULL;
    error = true;
    locked = false;
    if ( !fileName || !fileName[0] ) return;

    if ( (writeMode = wr) ) {
        if ( !(f = fopen(fileName,"wb")) ) return;
        }
    else
        if ( !(f = fopen(fileName,"rb")) ) return;

    bufLen = (buflen > 0) ? buflen : BinaryBuffer;
    if ( !(buf = new BYTE[bufLen]) ) return;
    ptr = buf;

    if ( writeMode ) {
        end = buf + bufLen;                 // -> end of the buffer
        error = false;
        }
    else {
        fseek(f,0L,SEEK_END);
        fileSize = ftell(f);                // get the file size
        rewind(f);
        end = buf;
        readBuffer();                       // read the 1st buffer
        comment = comm;
        }
}

bool BinaryFile::readBuffer ()
    // reads the next data buffer, returns true in case of EOF
{
    if ( writeMode || locked ) return( (error = true) );

    if ( ptr > buf ) {                  // move the old data?
        if ( ptr < end ) memmove(buf,ptr,end-ptr);
        end -= (ptr - buf);
        ptr = buf;
        }
        // read new data into <end,buf+bufLen> range:
    end += fread(end,1,bufLen-(end-buf),f);
    return( (error = (end == buf)) );
}

bool BinaryFile::skip ()
    // skips over all white-spaces (and comments), returns true if failed
{
    bool inComment = false;
    do {
        if ( ptr == end && readBuffer() ) return true;  // EOF
        if ( inComment ) {              // skip over an comment
            while ( ptr < end && *ptr != '\n' && *ptr != '\r' ) ptr++;
            inComment = (ptr == end);
            }
        else {
            while ( ptr < end && isspace(*ptr) ) ptr++;     // skip over white-spaces
            if ( ptr < end )
                if ( comment && *ptr == comment ) inComment = true;
                else
                    return( (error = false) );
            }
        } while ( true );
}

int BinaryFile::readInt ()
    // reads an decimal integer number, sets 'error'
{
    if ( skip() ) return 0;             // no number found!

    if ( *ptr != '-' && !isdigit(*ptr) ) {
        error = true;
        return 0;
        }

    int len = 1;
    while ( ptr + len < end && isdigit(ptr[len]) ) len++;
    if ( ptr + len == end ) {           // read the next buffer
        readBuffer();
        while ( ptr + len < end && isdigit(ptr[len]) ) len++;
        }
    if ( len == 1 && *ptr == '-' ) {    // minus sign only
        error = true;
        return 0;
        }
    char buffer[24];                    // enough space for an integer
    memcpy(buffer,ptr,Min(len,23));
    buffer[Min(len,23)] = (char)0;
    ptr += len;
    error = false;
    return atoi(buffer);
}

BYTE BinaryFile::readByte ()
    // reads one binary byte, sets 'error'
{
    if ( ptr == end && readBuffer() ) return 0;             // EOF
    error = false;
    return( *ptr++ );
}

WORD BinaryFile::readWord ()
    // reads one binary word (written using little endian), sets 'error'
{
    WORD result = readByte();
    if ( error ) return 0;
    return( result + (((WORD)readByte()) << 8) );
}

DWORD BinaryFile::readDWord ()
    // reads one binary dword (written using little endian), sets 'error'
{
    DWORD result = readWord();
    if ( error ) return 0;
    return( result + (((DWORD)readWord()) << 16) );
}

bool BinaryFile::readBytes ( BYTE *b, unsigned len )
    // reads the given BYTE-array, returns true if failed, sets 'error'
{
    if ( !b || !len ) return false;
    do
        *b++ = readByte();
    while ( !error && --len );
    return error;
}

bool BinaryFile::writeBuffer ()
    // flushes the data buffer to disk, returns true if failed, sets 'error'
{
    if ( !writeMode || locked ) return( (error = true) );

    if ( ptr > buf ) {                  // update the buffer
        if ( fwrite(buf,1,ptr-buf,f) != (size_t)(ptr-buf) ) return( (error = true) );
        ptr = buf;
        }
    return( (error = false) );
}

bool BinaryFile::writeByte ( BYTE b )
    // writes one binary byte, returns true if failed, sets 'error'
{
    if ( ptr == end && writeBuffer() ) return true;         // file error
    *ptr++ = b;
    return( (error = false) );
}

bool BinaryFile::writeWord ( WORD w )
    // writes one binary word (using little endian), returns true if failed, sets 'error'
{
    return( writeByte((BYTE)(w & 0xff)) || writeByte((BYTE)(w >> 8)) );
}

bool BinaryFile::writeDWord ( DWORD w )
    // writes one binary dword (using little endian), returns true if failed, sets 'error'
{
    return( writeWord((WORD)(w & 0xffff)) || writeWord((WORD)(w >> 16)) );
}

bool BinaryFile::writeString ( const char *str )
    // writes the zero-terminated string, returns true if failed, sets 'error'
{
    while ( *str )
        if ( writeByte(*str++) ) return true;
    return( (error = false) );
}

bool BinaryFile::writeBytes ( const BYTE *b, unsigned len )
    // writes the given BYTE-array, returns true if failed, sets 'error'
{
    if ( !b || !len ) return false;
    do
        if ( writeByte(*b++) ) return true;
    while ( --len );
    return false;
}

bool BinaryFile::aheadBuffer ( int length )
    // prepares I/O buffer as it holds at least 'length' bytes ahead, returns true if failed
{
    if ( ptr + length <= end ) return false;    // enough ahead space
    if ( locked ) return true;

    if ( writeMode ) {
        if ( writeBuffer() ) return true;
        }
    else
        if ( readBuffer() ) return true;

    return false;
}

BYTE *BinaryFile::lockBuffer ( int length )
    // locks the I/O buffer: the position of actual (ptr->) byte won't change until the unlockBuffer()
    // function is called, returns the locked position or NULL if failed
{
    if ( locked ) return NULL;
    Ceiling(length,BinaryBuffer);

    if ( ptr + length > end )           // not enough space
        if ( writeMode ) {
            if ( writeBuffer() ) return NULL;
            }
        else
            if ( readBuffer() ) return NULL;

    locked = true;
    return ptr;
}

bool BinaryFile::unlockBuffer ()
    // unlock the buffer, allow the buffer flush, returns true if failed
{
    if ( !locked ) return true;
    return( (locked = false) );
}

void BinaryFile::close()
    // closes the associated file
{
    locked = false;
    if ( writeMode && ptr > buf ) writeBuffer();

    DeleteArr(buf);
    ptr = end = NULL;                   // to be sure
    if ( f ) {
        fclose(f);
        f = NULL;
        }
}

BinaryFile::~BinaryFile ()
    // closes the file, recycles the temporary storage
{
    close();
}
