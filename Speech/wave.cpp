/**
    @file   wave.cpp
    @brief  Sampled waveform stored in memory.
    
    File representation: WAV (PCM).
    Copyright &copy; 1998-2002 by Josef Pelikan, MFF UK Prague
    http://cgg.ms.mff.cuni.cz/~pepca/
    @date   10.9.2002
*/

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include "wave.h"
#include "wavelets.h"
#ifdef OPENLPC
  #include "openlpc.h"
#else
  #include "lpc.h"
#endif

//-----------------------------------------------------------
//  Sampled waveform:

Wave::Wave ( int len )
    // initializes a new (empty) wave.
{
    detail = false;
    data = NULL;
    data32 = NULL;
    allocate(len,2);
    frequency = 8000;
    bf = NULL;
    code = NULL;
    codeLength = 0;
    lpcHdr.init(frequency);
#ifdef _WIN32
    event = NULL;
#endif
}

Wave::Wave ( const Wave &src )
    // copy-constructor.
{
    detail = src.detail;
    data = NULL;
    data32 = NULL;
    allocate(src.length,src.border);
    memcpy(data+border,src.data+border,length*sizeof(short));
    frequency = src.frequency;
    bf = NULL;
    lpcHdr = src.lpcHdr;
    codeLength = src.codeLength;
    if ( src.code ) {
        code = (BYTE*)malloc(codeLength);
        memcpy(code,src.code,codeLength);
        }
    else
        code = NULL;
#ifdef _WIN32
    event = NULL;
#endif
}

Wave::Wave ( const char *fileName )
    // loads an existing wave (from WAV format).
{
    data = NULL;
    data32 = NULL;
    if ( load(fileName) )
        discard();
    code = NULL;
    codeLength = 0;
    lpcHdr.init(frequency);
#ifdef _WIN32
    event = NULL;
#endif
}

void Wave::allocate ( int len, int bor )
{
    short *oldData = data;
    int oldLength = length;
    int oldBorder = border;
    if ( len >= 0 ) length = len;
    if ( bor >= 0 ) border = bor;
    if ( length ) {
        data = (short*)malloc((length+border+border)*sizeof(short));
        memset(data,0,(length+border+border)*sizeof(short));
        }
    else
        data = NULL;
    if ( oldData ) {
        if ( length )
            memcpy(data+border,oldData+oldBorder,Min(length,oldLength)*sizeof(short));
        free(oldData);
        }
}

void Wave::discard ()
{
    if ( data ) {
        free(data);
        data = NULL;
        }
    if ( data32 ) {
        free(data32);
        data32 = NULL;
        }
    Delete(bf);
    if ( code ) {
        free(code);
        code = NULL;
        }
    codeLength = 0;
}

Wave::~Wave ()
    // recycles the used memory.
{
    discard();
#ifdef _WIN32
    if ( event ) CloseHandle(event), event = NULL;
#endif
}

void Wave::copyTo32 ( double mul )
    // mul .. multiplication constant
{
    if ( data32 ) {
        free(data32);
        data32 = NULL;
        }
    if ( length <= 0 || !data ) return;
    data32 = (int*)malloc((length+border+border)*sizeof(int));
    memset(data32,0,(length+border+border)*sizeof(int));
    if ( mul < 1.e-6 ) mul = 1.e-6;
    const short *ptr = data;
    int *dptr = data32;
    int i;
    for ( i = length + border + border; i--; ) {
        double val = *ptr++ * mul + 0.5;
        if ( val < INT_MIN )
            *dptr++ = INT_MIN;
        else
        if ( val > INT_MAX )
            *dptr++ = INT_MAX;
        else
            *dptr++ = (int)val;
        }
}

void Wave::copyFrom32 ( double normalize )
    // normalize < 0.0 => do not normalize (simple copy the values)
{
    if ( length <= 0 || !data || !data32 ) return;
    int i;
    const int *ptr = data32;
    short *dptr = data;
    if ( normalize < 0.0 ) {                // simple copy the values
        for ( i = length + border + border; i--; ptr++ )
            if ( *ptr < SHRT_MIN )
                *dptr++ = SHRT_MIN;
            else
            if ( *ptr > SHRT_MAX )
                *dptr++ = SHRT_MAX;
            else
                *dptr++ = (short)*ptr;
        return;
        }
        // normalization:
    int maxAbs = 0;
    for ( i = length + border + border; i--; ptr++ )
        if ( *ptr < -maxAbs || *ptr > maxAbs )
            maxAbs = abs(*ptr);
    if ( normalize < 1.e-5 ) normalize = 1.e-5;
    if ( normalize > 1.0 )   normalize = 1.0;
    double mul = SHRT_MAX * normalize / maxAbs;
    for ( ptr = data32, i = length + border + border; i--; )
        *dptr++ = (short)( *ptr++ * mul + 0.5 );
}

bool Wave::load ( const char *fileName )
    // loads the wave, returns true if failed.
{
    discard();
    if ( !(bf = new BinaryFile(fileName,false)) || bf->error ) return true;

        // read the WAV file header:
    if ( bf->readDWord() != StrToUnsigned("RIFF") ) return true;
    unsigned totalLen = bf->readDWord();
    if ( bf->readDWord() != StrToUnsigned("WAVE") ||
         bf->readDWord() != StrToUnsigned("fmt ") ) return true;
    unsigned size = bf->readDWord();
    if ( bf->readWord() != 1 ) return true; // not PCM
    if ( bf->readWord() != 1 ) return true; // STEREO is not supported yet
    frequency = (int)bf->readDWord();       // number of samples per second
    unsigned rate = bf->readDWord();        // transfer rate in bytes per second
    unsigned block = bf->readWord();        // bytes per sample (all channels)
    unsigned bits = bf->readWord();         // bits per sample
    ASSERT( block == 1 && bits == 8 || block == 2 && bits == 16 );
    while ( size > 16 ) {
        bf->readByte();
        size--;
        }

        // looking for "data" chunk:
    unsigned signature = bf->readDWord();
    while ( signature != StrToUnsigned("data") ) {
        size = bf->readDWord();
        while ( size ) {
            bf->readByte();
            size--;
            }
        signature = bf->readDWord();
        }

        // "data" chunk:
    size = bf->readDWord();
    size /= block;
    allocate(size,1);
    detail = false;
    short *ptr = data + border;
    if ( block == 1 )                       //  8 bits:   0x00 ..   0x80 ..   0xFF
        while ( size-- )
            *ptr++ = ((short)bf->readByte() - 128) * 255;
    else                                    // 16 bits: 0x8000 .. 0x0000 .. 0x7FFF
        while ( size-- )
            *ptr++ = bf->readWord();

    Delete(bf);
    return false;
}

bool Wave::save ( const char *fileName )
    // saves the wave, returns true if failed.
{
    Delete(bf);
    if ( !(bf = new BinaryFile(fileName,true)) || bf->error ) return true;
    bool result = true;
    if ( bf->writeString("RIFF") ) goto fail;
    unsigned totalLen;
    totalLen = 16 + 22 + 8 + length * sizeof(short);
    if ( bf->writeDWord(totalLen) ||
         bf->writeString("WAVEfmt ") ) goto fail;
    if ( bf->writeDWord(18) ||
         bf->writeWord(1) ||
         bf->writeWord(1) ||
         bf->writeDWord(frequency) ||
         bf->writeDWord(frequency*sizeof(short)) ||
         bf->writeWord(sizeof(short)) ||
         bf->writeWord(16) ||
         bf->writeWord(0) ) goto fail;
    if ( bf->writeString("data") ||
         bf->writeDWord(length*sizeof(short)) ) goto fail;
    short *ptr;
    ptr = data + border;
    int i;
    for ( i = 0; i++ < length; )
        if ( bf->writeWord(*ptr++) ) goto fail;
    result = false;
  fail:
    Delete(bf);
    return result;
}

bool Wave::loadRaw ( const char *fileName, int freq, int depth, bool endian )
    // loads the wave, returns true if failed. if "endian" is set, byte swapping is done.
{
    discard();
    FILE *f = fopen(fileName,"rb");
    if ( !f ) return true;
    fseek(f,0L,SEEK_END);
    int size = ftell(f);
    fseek(f,0L,SEEK_SET);
    ASSERT( depth == 8 || depth == 16 );
    frequency = freq;
    if ( depth > 8 ) size >>= 1;
    allocate(size);
    short *ptr = data + border;
    int i = size;
    if ( depth > 8 )
        if ( endian )
            while ( size-- ) {
                int buf = getc(f) << 8;
                *ptr++ = (short)( buf + (getc(f) & 0xff) );
                }
        else
            while ( size-- ) {
                int buf = getc(f) & 0xff;
                *ptr++ = (short)( buf + (getc(f) << 8) );
                }
    else
        while ( size-- )
            *ptr++ = (short)( 255 * getc(f) );
    fclose(f);
    return false;
}

void Wave::textDump ( const char *fileName ) const
    // dumps binary data using spread-sheet-readable format.
{
    if ( length <= 0 || !data ) return;
    FILE *f = fopen(fileName,"wt");
    if ( !f ) return;
    const short *ptr = data + border;
    int i = length;
    while ( i-- )
        fprintf(f,"%d\n",(int)*ptr++);
    fclose(f);
}

#ifdef _WIN32

void CALLBACK replayCallback ( HWAVEOUT hwo, UINT uMsg, DWORD dwInstance,
                               DWORD dwParam1, DWORD dwParam2 )
{
    if ( uMsg == WOM_DONE ) {
        ASSERT( dwInstance );
        SetEvent((HANDLE)dwInstance);
        }
}

#endif

void Wave::replay ( int prefix )
    // replays the sound. If "prefix > 0", it defines maximum duration in seconds.
{
#ifdef _WIN32
        // synchro-event:
    event = CreateEvent(NULL,FALSE,FALSE,NULL);
        // wave-format:
    WAVEFORMATEX fmt;
    fmt.wFormatTag      = 1;
    fmt.nChannels       = 1;
    fmt.nSamplesPerSec  = frequency;
    fmt.nAvgBytesPerSec = frequency * sizeof(short);
    fmt.nBlockAlign     = sizeof(short);
    fmt.wBitsPerSample  = 16;
    fmt.cbSize          = 0;
    HWAVEOUT hWave;                         // handle of wave-device for sound replay
    MMRESULT result;
    if ( (result = waveOutOpen(&hWave,WAVE_MAPPER,&fmt,(DWORD)replayCallback,(DWORD)event,CALLBACK_FUNCTION))
         != MMSYSERR_NOERROR ) {
        fprintf(stderr,"Error opening wave-device: %d\n",(int)result);
        goto fail;
        }
        // waveform data header:
    WAVEHDR hdr;
    hdr.lpData          = (char*)(data+border);
    hdr.dwBufferLength  = length * sizeof(short);
    hdr.dwFlags         = WHDR_BEGINLOOP | WHDR_ENDLOOP;
    hdr.dwLoops         = 1;
        // replay-machinery:
    waveOutPrepareHeader(hWave,&hdr,sizeof(hdr));
    waveOutWrite(hWave,&hdr,sizeof(hdr));
    WaitForSingleObject( event, (prefix<=0) ? INFINITE : (prefix*1000) );
    waveOutReset(hWave);
    waveOutUnprepareHeader(hWave,&hdr,sizeof(hdr));
    waveOutClose(hWave);
  fail:
    CloseHandle(event);
    event = NULL;
#else
    printf("Playing %.3f seconds of wave-data\n",(length+0.0)/frequency);
#endif
}

void Wave::decimate ()
    // resamples the data by a factor of 2. Normalizes output to 0.98.
{
    if ( length <= 1 || !data ) return;
#if 1
    Wavelet<int> wavelet;
    setBorder(wavelet.border());
    copyTo32(256.0);
    int newLen = (length + 1) >> 1;
    int *d = (int*)malloc((newLen+2)*sizeof(int));
    wavelet.lift1D(length,1,data32+border,1,data32+border,d+1);
    free(d);
    memset(data32,0,border*sizeof(int));
    memset(data32+border+newLen,0,border*sizeof(int));
    copyFrom32(0.98);
    allocate(newLen);
    detail = false;
    frequency >>= 1;
#else
    Wavelet<short> wavelet;
    setBorder(wavelet.border());
    int newLen = (length + 1) >> 1;
    short *d = (short*)malloc((newLen+2)*sizeof(short));
    wavelet.lift1D(length,1,data+border,1,data+border,d+1);
    free(d);
    allocate(newLen);
    detail = false;
    frequency >>= 1;
#endif
}

const double GAUSS_RANGE = 3.0;
    // used range of Gaussian filter (exp(-x*x))
const double GAUSS_REZIDUUM = exp(-GAUSS_RANGE*GAUSS_RANGE);
    // residual value at the ends of filter window
const double GAUSS_SPEED = GAUSS_RANGE * 1.8;
    // t = GAUSS_SPEED * x, f(t) = exp(-t*t)

void Wave::decimateGauss ( int to )
    // resamples the data to the given frequency. Normalizes output to 0.98.
{
    if ( length <= 1 || !data || to >= frequency ) return;
    double step = (frequency+0.0) / to;
    double width = step * (GAUSS_RANGE / GAUSS_SPEED);
    printf("step = %.3f, width = %.3f\n",step,width);
    setBorder( (int)(width + 1.0) );
    copyTo32(256.0);
    int newLen = (int)((length-1) / step);
    int *d = (int*)malloc(newLen*sizeof(int));
    int *outPtr = d;
    int i, j;
    double sample = border;
    const double accel = GAUSS_RANGE / step;

    for ( i = 0; i++ < newLen; sample += step ) {   // compute one output sample
        int lower = (int)ceil(sample-width);
        int upper = (int)floor(sample+width);       // filter window bounds
        double sum = 0.0;
        double sumW = 0.0;
        for ( j = lower; j <= upper; j++ ) {        // add one weighted input sample
            double x = accel * (j - sample);
            double weight = exp(-x*x) - GAUSS_REZIDUUM;
            sum  += weight * data32[j];
            sumW += weight;
            }
        if ( sumW > 1.e-6 )
            *outPtr++ = (int)( sum / sumW );
        else
            *outPtr++ = 0;
        }

    memcpy(data32+border,d,newLen*sizeof(int));
    free(d);
    memset(data32,0,border*sizeof(int));
    memset(data32+border+newLen,0,border*sizeof(int));
    copyFrom32(0.98);
    allocate(newLen);
    detail = false;
    frequency = to;
}

void Wave::crop ( double start, double stop )
    // crops the data to the given time-interval. Doesn't affect amplitudes at all.
{
    if ( length <= 0 || !data ) return;
    if ( start < 0.0 ) start = 0.0;
    if ( stop > (length-1.0)/frequency ) stop = (length-1.0)/frequency;
    if ( stop <= start ) return;
    int newLen = (int)( (stop - start) * frequency + 0.5 ) + 1;
    int starti = (int)( start * frequency + 0.5 );
    if ( starti )
        memmove(data+border,data+border+starti,newLen*sizeof(short));
    allocate(newLen);
}

void Wave::normalize ( double peak )
    // normalizes the signal.
{
    if ( length <= 0 || !data ) return;
    if ( peak < 0.0 ) peak = 0.0;
    if ( peak > 1.0 ) peak = 1.0;
        // normalization:
    int i;
    int maxAbs = 0;
    short *ptr = data + border;
    for ( i = length; i--; ptr++ )
        if ( *ptr < -maxAbs || *ptr > maxAbs )
            maxAbs = abs(*ptr);
    double mul = SHRT_MAX * peak / maxAbs;
    if ( border )
        memset(data,0,border*sizeof(short));
    for ( ptr = data+border, i = length; i--; ptr++ )
        *ptr = (short)( *ptr * mul + 0.5 );
    if ( border )
        memset(ptr,0,border*sizeof(short));
}

void Wave::setBorder ( int bor )
    // sets the border values (constant border).
{
    if ( bor >= 0 && border < bor )
        allocate(-1,bor);
    short b = data[border];
    int i;
    for ( i = border; --i >= 0; )
        data[i] = b;
    b = data[border+length-1];
    for ( i = border+length; i < border+border+length; i++ )
        data[i] = b;
}

//-----------------------------------------------------------
//  statistics:

double Wave::energy () const
    // computes RMS energy of the signal
{
    if ( length <= 0 || !data ) return 0.0;
    int i;
    const short *ptr = data + border;
    double sum = 0.0;
    for ( i = 0; i++ < length; ptr++ )
        sum += ((int)*ptr) * *ptr;
    sum /= length;
    return( sqrt(sum)/32768.0 );
}

double Wave::peak () const
    // computes peak level of the signal
{
    if ( length <= 0 || !data ) return 0.0;
    int i;
    const short *ptr = data + border;
    short maximum = 0;
    for ( i = 0; i++ < length; ptr++ )
        if ( abs(*ptr) > maximum ) maximum = abs(*ptr);
    return( maximum/32768.0 );
}

//-----------------------------------------------------------
//  LPC file format:

void LPCHeader::init ( WORD freq, WORD lpcB, WORD fr )
{
    magic     = StrToUnsigned("LPC1");
    frequency = freq;
    lpcBits   = lpcB;
    frame     = fr;
    frames    = 0;
}

bool readLPCHeader ( BinaryFile &bf, LPCHeader &hdr )
    // returns true if failed
{
    if ( (hdr.magic = bf.readDWord()) != StrToUnsigned("LPC1") ) return true;
    hdr.frequency = bf.readWord();
    hdr.lpcBits   = bf.readWord();
    hdr.frame     = bf.readWord();
    hdr.frames    = bf.readDWord();
    return false;
}

bool writeLPCHeader ( BinaryFile &bf, const LPCHeader &hdr )
    // returns true if failed
{
    ASSERT( hdr.magic == StrToUnsigned("LPC1") );
    if ( bf.writeDWord(hdr.magic) ||
         bf.writeWord(hdr.frequency) ||
         bf.writeWord(hdr.lpcBits) ||
         bf.writeWord(hdr.frame) ||
         bf.writeDWord(hdr.frames) ) return true;
    return false;
}

bool Wave::saveCode ( const char *fileName )
    // saves the LPC code, returns true if failed.
{
    if ( !code ) return true;
    Delete(bf);
    if ( !(bf = new BinaryFile(fileName,true)) || bf->error ) return true;
    bool result = writeLPCHeader(*bf,lpcHdr) ||
                  bf->writeBytes(code,codeLength);
    Delete(bf);
    return result;
}

bool Wave::loadCode ( const char *fileName )
    // loads the LPC code, returns true if failed.
{
    if ( code ) {
        free(code);
        code = NULL;
        }
    codeLength = 0;
    Delete(bf);
    if ( !(bf = new BinaryFile(fileName,false)) || bf->error ) return true;
    bool result = readLPCHeader(*bf,lpcHdr);
    if ( !result ) {
        frequency = lpcHdr.frequency;
        int frameCodeLen = (lpcHdr.lpcBits + 7) / 8 + 2;
        codeLength = lpcHdr.frames * frameCodeLen;
        code = (BYTE*)malloc(codeLength);
        result = bf->readBytes(code,codeLength);
        }
    Delete(bf);
    return result;
}

//-----------------------------------------------------------
//  LPC codec:

void Wave::encode ( short frame, short lpcBits )
    // encodes the waveform using LPC codec.
{
#ifdef OPENLPC
    lpcHdr.init(frequency,lpcBits,frame);
    lpcHdr.frames = (length + frame - 1) / frame;
    if ( length % frame )
        allocate( lpcHdr.frames * frame );  // expand the next fractional frame (zero-padding)
    int frameCodeLen = (lpcHdr.lpcBits + 7) / 8 + 2;
    codeLength = lpcHdr.frames * frameCodeLen;
    if ( code ) free(code);
    code = (BYTE*)malloc(codeLength);       // code array is prepared
    memset(code,0,codeLength);
        // do the coding job:
    const short *ptr = data + border;
    BYTE *cptr = code;
    unsigned fr;
    openlpc_encoder_state *es = create_openlpc_encoder_state();
    init_openlpc_encoder_state(es,frame,lpcBits);
    for ( fr = 0; fr++ < lpcHdr.frames; ptr += frame )
        cptr += openlpc_encode(ptr,cptr,es);
    destroy_openlpc_encoder_state(es);
#else
    frame = 160;
    lpcHdr.init(frequency,lpcBits,frame);
    lpcHdr.frames = (length + frame - 1) / frame;
    if ( length % frame )
        allocate( lpcHdr.frames * frame );  // expand the next fractional frame (zero-padding)
    int frameCodeLen = 12;
    codeLength = lpcHdr.frames * frameCodeLen;
    if ( code ) free(code);
    code = (BYTE*)malloc(codeLength);       // code array is prepared
    memset(code,0,codeLength);
        // do the coding job:
    const short *ptr = data + border;
    BYTE *cptr = code;
    unsigned fr;
    lpc_encoder_state *es = create_lpc_encoder_state();
    init_lpc_encoder_state(es);
    for ( fr = 0; fr++ < lpcHdr.frames; ptr += frame )
        cptr += lpc_encode(ptr,cptr,es);
    destroy_lpc_encoder_state(es);
#endif
}

void Wave::decode ()
    // decodes the waveform from LPC code.
{
#ifdef OPENLPC
    if ( !lpcHdr.frames || !code ) return;
    allocate( lpcHdr.frames * lpcHdr.frame );
        // do the decoding job:
    short *ptr = data + border;
    const BYTE *cptr = code;
    int frameCodeLen = (lpcHdr.lpcBits + 7) / 8 + 2;
    unsigned fr;
    openlpc_decoder_state *ds = create_openlpc_decoder_state();
    init_openlpc_decoder_state(ds,lpcHdr.frame,lpcHdr.lpcBits);
    for ( fr = 0; fr++ < lpcHdr.frames; cptr += frameCodeLen )
        ptr += openlpc_decode(cptr,ptr,ds);
    destroy_openlpc_decoder_state(ds);
#else
    if ( !lpcHdr.frames || !code ) return;
    allocate( lpcHdr.frames * lpcHdr.frame );
        // do the decoding job:
    short *ptr = data + border;
    const BYTE *cptr = code;
    int frameCodeLen = 12;
    unsigned fr;
    lpc_decoder_state *ds = create_lpc_decoder_state();
    init_lpc_decoder_state(ds);
    for ( fr = 0; fr++ < lpcHdr.frames; cptr += frameCodeLen )
        ptr += lpc_decode(cptr,ptr,ds);
    destroy_lpc_decoder_state(ds);
#endif
}

unsigned Wave::rate () const
    // computes the actual compression rate in bits per second.
{
    if ( !codeLength || !code ) return 0;
    return( ((codeLength << 3) * frequency) / length );
}
