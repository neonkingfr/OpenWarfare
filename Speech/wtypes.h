/**
    @file   wtypes.h
    @brief  Global macros/routines (for Win32-compatibility).

    Copyright &copy; 1998-2002 by Josef Pelikan, MFF UK Prague
    http://cgg.ms.mff.cuni.cz/~pepca/
    @date   8.9.2002
*/

#ifndef _WTYPES_H
#define _WTYPES_H

//-----------------------------------------------------------
//  global types:

typedef unsigned char BYTE;
#define MaxByte     255
typedef unsigned short WORD;
#define MaxWord     65535
typedef unsigned long DWORD;

//-----------------------------------------------------------
//  support macros:

#ifdef _MSC_VER
#  undef CDECL
#  define CDECL __cdecl
#else
#  define CDECL
#endif

#ifdef _DEBUG                       // debug version
#  include <stdio.h>
#  define ASSERT(bool) if(!(bool))fprintf(stderr,"Assertion error in %s, line %d\n",__FILE__,__LINE__)
#  define VERIFY(bool) ASSERT(bool)
#  define WHERE(str)   sprintf(str,"File: %s, line: %d",__FILE__,__LINE__)
#else                               // release version
#  define ASSERT(bool)
#  define VERIFY(bool) bool
#  define WHERE(str)
#endif

#define Min(x,y)          ((x)<(y)?(x):(y))
#define Max(x,y)          ((x)>(y)?(x):(y))
#ifndef Zero
#  define Zero(x)         memset(x,0,sizeof(x))
#endif
#define Range(x,min,max)  {if((x)<(min))(x)=(min);else if((x)>(max))(x)=(max);}
#define Floor(x,min)      {if((x)<(min))(x)=(min);}
#define Ceiling(x,max)    {if((x)>(max))(x)=(max);}

#define Delete(x)         {if(x)delete x;x=NULL;}
#define DeleteArr(x)      {if(x)delete[]x;x=NULL;}

#define Power(x,e)        ((x)<=0?0.0:exp(log(x)*(e)))
#define DegToRad          1.745329252e-2

#define StrToUnsigned(x)  (*(unsigned*)x)

#endif
