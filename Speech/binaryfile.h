#ifdef _MSC_VER
#  pragma once
#endif

/**
    @file   binaryfile.h
    @brief  Binary input/output file object.

    Copyright &copy; 1998-2002 by Josef Pelikan, MFF UK Prague
    http://cgg.ms.mff.cuni.cz/~pepca/
    @date   4.9.2002
*/

#ifndef _BINARYFILE_H
#define _BINARYFILE_H

#include <stdio.h>
#include "wtypes.h"

//-----------------------------------------------------------
//  binary input/output file object:

#define BinaryBuffer        4096

class BinaryFile {

public:

    bool error;             // false .. OK

    BYTE *ptr;              // -> next byte to read/write
    BYTE *end;              // -> after the buffer

    long fileSize;          // file size in bytes (valid only while reading)

    BinaryFile ( const char *fileName, bool wr =false, BYTE comm =0, unsigned buflen =BinaryBuffer );
        // opens the binary file for reading/writting, sets 'error' variable

    virtual void close();
        // closes the associated file

    virtual ~BinaryFile ();
        // closes the file, recycles the temporary storage

    //-------------------------------------------------------
    //  read methods:

    virtual bool readBuffer ();
        // reads the next data buffer, returns true in case of EOF

    bool skip ();
        // skips over all white-spaces (and comments), returns true if failed

    int readInt ();
        // reads an decimal integer number, sets 'error'

    BYTE readByte ();
        // reads one binary byte, sets 'error'

    WORD readWord ();
        // reads one binary word (written using little endian), sets 'error'

    DWORD readDWord ();
        // reads one binary dword (written using little endian), sets 'error'

    bool readBytes ( BYTE *b, unsigned len );
        // reads the given BYTE-array, returns true if failed, sets 'error'

    //-------------------------------------------------------
    //  write methods:

    bool writeBuffer ();
        // flushes the data buffer to disk, returns true if failed, sets 'error'

    bool writeByte ( BYTE b );
        // writes one binary byte, returns true if failed, sets 'error'

    bool writeWord ( WORD w );
        // writes one binary word (using little endian), returns true if failed, sets 'error'

    bool writeDWord ( DWORD w );
        // writes one binary dword (using little endian), returns true if failed, sets 'error'

    bool writeString ( const char *str );
        // writes the zero-terminated string, returns true if failed, sets 'error'

    bool writeBytes ( const BYTE *b, unsigned len );
        // writes the given BYTE-array, returns true if failed, sets 'error'

    //-------------------------------------------------------
    //  read & write methods:

    bool aheadBuffer ( int length );
        // prepares I/O buffer as it holds at least 'length' bytes ahead, returns true if failed

    BYTE *lockBuffer ( int length =BinaryBuffer>>1 );
        // locks the I/O buffer: the position of actual (ptr->) byte won't change until the unlockBuffer()
        // function is called, returns the locked position or NULL if failed

    bool unlockBuffer ();
        // unlock the buffer, allow the buffer flush, returns true if failed

protected:

    bool writeMode;         // the file is opened for writting?

    FILE *f;                // associated file (f != NULL <=> the file is opened)

    BYTE *buf;              // -> data buffer
    int bufLen;             // allocated buffer size in bytes

    bool locked;            // the buffer is locked?

    BYTE comment;           // comment to the line end, 0 .. no comment processing

    };

#endif
