#ifdef _MSC_VER
#  pragma once
#endif

/**
    @file   wave.h
    @brief  Sampled waveform stored in memory.
    
    File representation: WAV (PCM).
    Copyright &copy; 1998-2002 by Josef Pelikan, MFF UK Prague
    http://cgg.ms.mff.cuni.cz/~pepca/
    @date   9.9.2002
*/

#ifndef _WAVE_H
#define _WAVE_H

#ifdef _WIN32
  #include <windows.h>
#endif
#include "binaryfile.h"

//-----------------------------------------------------------
//  LPC file format:

//#define OPENLPC

struct LPCHeader {

    DWORD magic;                            // "LPC1"

    WORD frequency;                         // frequency in samples/s

    WORD lpcBits;                           // LPC_BITS (32, 38, 48, 80)
                                            // frame code length in bytes: (lpcBits+7)/8 + 2

    WORD frame;                             // number of samples in a frame
                                            //   reasonable values for 8kHz: 120 to 320

    DWORD frames;                           // number of frames (last one can be zero-padded)

    void init ( WORD freq =8000, WORD lpcB =48, WORD fr =120 );

    };

#define FRAMES_POS   (4+2+2+2)

//-----------------------------------------------------------
//  Sampled waveform:

class Wave {

public:

    //-------------------------------------------------------
    //  RAW data:

    bool detail;                            // detail (high-frequency) band.

    int length;                             // number of valid samples.

    int border;                             // width of the data-border.

    short *data;                            // the data array itself (allocated via malloc).

    int *data32;                            // temporary data array (bigger sample depth)

    int frequency;                          // sample frequency in samples per second (Hz).

    //-------------------------------------------------------
    //  LPC code:

    LPCHeader lpcHdr;

    BYTE *code;

    int codeLength;

    //-------------------------------------------------------

    Wave ( const Wave &src );
        // copy-constructor.

    Wave ( int len =256 );
        // initializes a new (empty) wave.

    Wave ( const char *fileName );
        // loads an existing wave (from WAV format).

    ~Wave ();
        // recycles the used memory.

    bool load ( const char *fileName );
        // loads the wave, returns true if failed.

    bool save ( const char *fileName );
        // saves the wave, returns true if failed.

    bool loadRaw ( const char *fileName, int freq =8000, int depth =16, bool endian =true );
        // loads the wave, returns true if failed. if "endian" is set, byte swapping is done.

    void replay ( int prefix =-1 );
        // replays the sound. If "prefix > 0", it defines maximum duration in seconds.

    void decimate ();
        // resamples the data by a factor of 2. Normalizes output to 0.98.

    void decimateGauss ( int to );
        // resamples the data to the given frequency. Normalizes output to 0.98.

    void crop ( double start, double stop );
        // crops the data to the given time-interval. Doesn't affect amplitudes at all.

    void normalize ( double peak =0.98 );
        // normalizes the signal.

    void setBorder ( int bor =-1 );
        // sets the border values (constant border).

    double energy () const;
        // computes RMS energy of the signal

    double peak () const;
        // computes peak level of the signal

    void textDump ( const char *fileName ) const;
        // dumps binary data using spread-sheet-readable format.

    void encode ( short frame =160, short lpcBits =48 );
        // encodes the waveform using LPC codec.

    void decode ();
        // decodes the waveform from LPC code.

    unsigned rate () const;
        // computes the actual compression rate in bits per second.

    bool loadCode ( const char *fileName );
        // loads the LPC code, returns true if failed.

    bool saveCode ( const char *fileName );
        // saves the LPC code, returns true if failed.

protected:

    BinaryFile *bf;                         // file used for I/O

#ifdef _WIN32
    HANDLE event;                           // for replay synchronization
#endif

    void allocate ( int len =0, int bor =-1 );

    void copyTo32 ( double mul = 1.0 );
        // mul .. multiplication constant

    void copyFrom32 ( double normalize = -1.0 );
        // normalize < 0.0 => do not normalize (simple copy the values)

    void discard ();

    };

#endif
