/**
    @file   wavelets.cpp
    @brief  Wavelet core, uses PPP lifting schemes.

    Copyright &copy; 2000-2002 by Josef Pelikan, MFF UK Prague
    http://cgg.ms.mff.cuni.cz/~pepca/
    @date   3.9.2002
*/

#include "wtypes.h"
#include "wavelets.h"

//-----------------------------------------------------------
//  General wavelet object - Cohen-Daubechies-Feauveau CDF (2,2):

template Wavelet<short>;
template Wavelet<int>;

template <typename SampleType>
Wavelet<SampleType> *createWavelet ( unsigned sig )
{
    switch ( sig ) {
        case WSignatureD97:
            return new WaveletD97<SampleType>;
        case WSignatureCDF42:
            return new WaveletCDF42<SampleType>;
        case WSignatureCDF44:
            return new WaveletCDF44<SampleType>;
        case WSignatureCDF222:
            return new WaveletCDF222<SampleType>;
        case WSignatureTS:
            return new WaveletTS<SampleType>;
        case WSignatureSP:
            return new WaveletSP<SampleType>;
        case WSignatureCDF62:
            return new WaveletCDF62<SampleType>;
        case WSignatureD4:
            return new WaveletD4<SampleType>;
        default:
            return new Wavelet<SampleType>;
        }
}

template <typename SampleType>
Wavelet<SampleType>::Wavelet ()
    // initializes a new object
{
    signature = WSignatureCDF22;
}

template <typename SampleType>
Wavelet<SampleType>::~Wavelet ()
    // recycles the used memory
{
}

template <typename SampleType>
int Wavelet<SampleType>::border () const
    // border needed for FWT
{
    return 1;
}

template <typename SampleType>
void Wavelet<SampleType>::lift1D ( int n0, int delta0, const SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const
    // 1D forward FWT
{
    ASSERT( n0 && delta0 && delta1 );
    ASSERT( s0 );
    ASSERT( s1 );
    ASSERT( d1 );
    int i, n1;
    n1 = (n0 + 1) >> 1;
    const SampleType *ps0;
    SampleType *ps1, *pd1;

        // split:
    ps0 = s0;
    ps1 = s1; pd1 = d1;
    for ( i = 0; i++ < n1; ) {
        *ps1 = *ps0;
        ps1 += delta1; ps0 += delta0;
        *pd1 = *ps0;
        pd1 += delta1; ps0 += delta0;
        }

        // predict:
    ps1 = s1; pd1 = d1;
    ps1[n1*delta1] = ps1[(n1-1)*delta1];    // boundary
    for ( i = 0; i++ < n1; ) {
        pd1[0] -= ((ps1[0] + ps1[delta1] + 1) >> 1);
        pd1 += delta1; ps1 += delta1;
        }

        // update:
    ps1 = s1; pd1 = d1;
    pd1[-delta1] = pd1[0];                  // boundary
    for ( i = 0; i++ < n1; ) {
        ps1[0] += ((pd1[-delta1] + pd1[0] + 2) >> 2);
        pd1 += delta1; ps1 += delta1;
        }
}

template <typename SampleType>
void Wavelet<SampleType>::unlift1D ( int n0, int delta0, SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const
    // 1D inverse FWT
{
    ASSERT( n0 && delta0 && delta1 );
    ASSERT( s0 );
    ASSERT( s1 );
    ASSERT( d1 );
    int i, n1;
    n1 = (n0 + 1) >> 1;
    SampleType *ps0;
    SampleType *ps1, *pd1;

        // update*:
    ps1 = s1; pd1 = d1;
    pd1[-delta1] = pd1[0];                  // boundary
    for ( i = 0; i++ < n1; ) {
        ps1[0] -= ((pd1[-delta1] + pd1[0] + 2) >> 2);
        pd1 += delta1; ps1 += delta1;
        }

        // predict*:
    ps1 = s1; pd1 = d1;
    ps1[n1*delta1] = ps1[(n1-1)*delta1];    // boundary
    for ( i = 0; i++ < n1; ) {
        pd1[0] += ((ps1[0] + ps1[delta1] + 1) >> 1);
        pd1 += delta1; ps1 += delta1;
        }

        // merge:
    ps0 = s0;
    ps1 = s1; pd1 = d1;
    for ( i = 0; i++ < n1; ) {
        *ps0 = *ps1;
        ps1 += delta1; ps0 += delta0;
        *ps0 = *pd1;
        pd1 += delta1; ps0 += delta0;
        }
}

//-----------------------------------------------------------
//  Daubechies (9-7) symmetric biorthogonal wavelet:

template <typename SampleType>
WaveletD97<SampleType>::WaveletD97 ()
    // initializes a new object
{
    signature = WSignatureD97;
}

template <typename SampleType>
int WaveletD97<SampleType>::border () const
    // border needed for FWT
{
    return 1;
}

template <typename SampleType>
void WaveletD97<SampleType>::lift1D ( int n0, int delta0, const SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const
    // 1D forward FWT
{
    ASSERT( n0 && delta0 && delta1 );
    ASSERT( s0 );
    ASSERT( s1 );
    ASSERT( d1 );
    int i, n1;
    n1 = (n0 + 1) >> 1;
    const SampleType *ps0;
    SampleType *ps1, *pd1;

        // split:
    ps0 = s0;
    ps1 = s1; pd1 = d1;
    for ( i = 0; i++ < n1; ) {
        *ps1 = *ps0;
        ps1 += delta1; ps0 += delta0;
        *pd1 = *ps0;
        pd1 += delta1; ps0 += delta0;
        }

        // predict 1:
    ps1 = s1; pd1 = d1;
    ps1[n1*delta1] = ps1[(n1-1)*delta1];    // boundary
    for ( i = 0; i++ < n1; ) {
        pd1[0] += (SampleType)floor(-1.586134342 * (ps1[0] + ps1[delta1]) + 0.5);
        pd1 += delta1; ps1 += delta1;
        }

        // update 1:
    ps1 = s1; pd1 = d1;
    pd1[-delta1] = pd1[0];                  // boundary
    for ( i = 0; i++ < n1; ) {
        ps1[0] += (SampleType)floor(-0.05298011854 * (pd1[-delta1] + pd1[0]) + 0.5);
        pd1 += delta1; ps1 += delta1;
        }

        // predict 2:
    ps1 = s1; pd1 = d1;
    ps1[n1*delta1] = ps1[(n1-1)*delta1];    // boundary
    for ( i = 0; i++ < n1; ) {
        pd1[0] += (SampleType)floor(0.8829110762 * (ps1[0] + ps1[delta1]) + 0.5);
        pd1 += delta1; ps1 += delta1;
        }

        // update 2:
    ps1 = s1; pd1 = d1;
    pd1[-delta1] = pd1[0];                  // boundary
    for ( i = 0; i++ < n1; ) {
        ps1[0] += (SampleType)floor(0.4435068522 * (pd1[-delta1] + pd1[0]) + 0.5);
        pd1 += delta1; ps1 += delta1;
        }
}

template <typename SampleType>
void WaveletD97<SampleType>::unlift1D ( int n0, int delta0, SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const
    // 1D inverse FWT
{
    ASSERT( n0 && delta0 && delta1 );
    ASSERT( s0 );
    ASSERT( s1 );
    ASSERT( d1 );
    int i, n1;
    n1 = (n0 + 1) >> 1;
    SampleType *ps0;
    SampleType *ps1, *pd1;

        // update 2*:
    ps1 = s1; pd1 = d1;
    pd1[-delta1] = pd1[0];                  // boundary
    for ( i = 0; i++ < n1; ) {
        ps1[0] -= (SampleType)floor(0.4435068522 * (pd1[-delta1] + pd1[0]) + 0.5);
        pd1 += delta1; ps1 += delta1;
        }

        // predict 2*:
    ps1 = s1; pd1 = d1;
    ps1[n1*delta1] = ps1[(n1-1)*delta1];    // boundary
    for ( i = 0; i++ < n1; ) {
        pd1[0] -= (SampleType)floor(0.8829110762 * (ps1[0] + ps1[delta1]) + 0.5);
        pd1 += delta1; ps1 += delta1;
        }

        // update 1*:
    ps1 = s1; pd1 = d1;
    pd1[-delta1] = pd1[0];                  // boundary
    for ( i = 0; i++ < n1; ) {
        ps1[0] -= (SampleType)floor(-0.05298011854 * (pd1[-delta1] + pd1[0]) + 0.5);
        pd1 += delta1; ps1 += delta1;
        }

        // predict 1*:
    ps1 = s1; pd1 = d1;
    ps1[n1*delta1] = ps1[(n1-1)*delta1];    // boundary
    for ( i = 0; i++ < n1; ) {
        pd1[0] -= (SampleType)floor(-1.586134342 * (ps1[0] + ps1[delta1]) + 0.5);
        pd1 += delta1; ps1 += delta1;
        }

        // merge:
    ps0 = s0;
    ps1 = s1; pd1 = d1;
    for ( i = 0; i++ < n1; ) {
        *ps0 = *ps1;
        ps1 += delta1; ps0 += delta0;
        *ps0 = *pd1;
        pd1 += delta1; ps0 += delta0;
        }
}

//-----------------------------------------------------------
//  Cohen-Daubechies-Feauveau CDF (4,2) wavelet:

template <typename SampleType>
WaveletCDF42<SampleType>::WaveletCDF42 ()
    // initializes a new object
{
    signature = WSignatureCDF42;
}

template <typename SampleType>
int WaveletCDF42<SampleType>::border () const
    // border needed for FWT
{
    return 2;
}

template <typename SampleType>
void WaveletCDF42<SampleType>::lift1D ( int n0, int delta0, const SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const
    // 1D forward FWT
{
    ASSERT( n0 && delta0 && delta1 );
    ASSERT( s0 );
    ASSERT( s1 );
    ASSERT( d1 );
    int i, n1;
    n1 = (n0 + 1) >> 1;
    const SampleType *ps0;
    SampleType *ps1, *pd1;

        // split:
    ps0 = s0;
    ps1 = s1; pd1 = d1;
    for ( i = 0; i++ < n1; ) {
        *ps1 = *ps0;
        ps1 += delta1; ps0 += delta0;
        *pd1 = *ps0;
        pd1 += delta1; ps0 += delta0;
        }

        // predict:
    ps1 = s1; pd1 = d1;
    ps1[n1*delta1] = ps1[(n1+1)*delta1] = ps1[(n1-1)*delta1];
    ps1[-delta1] = ps1[0];                  // boundary
    for ( i = 0; i++ < n1; ) {
        pd1[0] -= ((9 * (ps1[0] + ps1[delta1]) - ps1[-delta1] - ps1[delta1<<1] + 8) >> 4);
        pd1 += delta1; ps1 += delta1;
        }

        // update:
    ps1 = s1; pd1 = d1;
    pd1[-delta1] = pd1[0];                  // boundary
    for ( i = 0; i++ < n1; ) {
        ps1[0] += ((pd1[-delta1] + pd1[0] + 2) >> 2);
        pd1 += delta1; ps1 += delta1;
        }
}

template <typename SampleType>
void WaveletCDF42<SampleType>::unlift1D ( int n0, int delta0, SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const
    // 1D inverse FWT
{
    ASSERT( n0 && delta0 && delta1 );
    ASSERT( s0 );
    ASSERT( s1 );
    ASSERT( d1 );
    int i, n1;
    n1 = (n0 + 1) >> 1;
    SampleType *ps0;
    SampleType *ps1, *pd1;

        // update*:
    ps1 = s1; pd1 = d1;
    pd1[-delta1] = pd1[0];                  // boundary
    for ( i = 0; i++ < n1; ) {
        ps1[0] -= ((pd1[-delta1] + pd1[0] + 2) >> 2);
        pd1 += delta1; ps1 += delta1;
        }

        // predict*:
    ps1 = s1; pd1 = d1;
    ps1[n1*delta1] = ps1[(n1+1)*delta1] = ps1[(n1-1)*delta1];
    ps1[-delta1] = ps1[0];                  // boundary
    for ( i = 0; i++ < n1; ) {
        pd1[0] += ((9 * (ps1[0] + ps1[delta1]) - ps1[-delta1] - ps1[delta1<<1] + 8) >> 4);
        pd1 += delta1; ps1 += delta1;
        }

        // merge:
    ps0 = s0;
    ps1 = s1; pd1 = d1;
    for ( i = 0; i++ < n1; ) {
        *ps0 = *ps1;
        ps1 += delta1; ps0 += delta0;
        *ps0 = *pd1;
        pd1 += delta1; ps0 += delta0;
        }
}

//-----------------------------------------------------------
//  Cohen-Daubechies-Feauveau CDF (4,4) wavelet:

template <typename SampleType>
WaveletCDF44<SampleType>::WaveletCDF44 ()
    // initializes a new object
{
    signature = WSignatureCDF44;
}

template <typename SampleType>
int WaveletCDF44<SampleType>::border () const
    // border needed for FWT
{
    return 2;
}

template <typename SampleType>
void WaveletCDF44<SampleType>::lift1D ( int n0, int delta0, const SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const
    // 1D forward FWT
{
    ASSERT( n0 && delta0 && delta1 );
    ASSERT( s0 );
    ASSERT( s1 );
    ASSERT( d1 );
    int i, n1;
    n1 = (n0 + 1) >> 1;
    const SampleType *ps0;
    SampleType *ps1, *pd1;

        // split:
    ps0 = s0;
    ps1 = s1; pd1 = d1;
    for ( i = 0; i++ < n1; ) {
        *ps1 = *ps0;
        ps1 += delta1; ps0 += delta0;
        *pd1 = *ps0;
        pd1 += delta1; ps0 += delta0;
        }

        // predict:
    ps1 = s1; pd1 = d1;
    ps1[n1*delta1] = ps1[(n1+1)*delta1] = ps1[(n1-1)*delta1];
    ps1[-delta1] = ps1[0];                  // boundary
    for ( i = 0; i++ < n1; ) {
        pd1[0] -= ((9 * (ps1[0] + ps1[delta1]) - ps1[-delta1] - ps1[delta1<<1] + 8) >> 4);
        pd1 += delta1; ps1 += delta1;
        }

        // update:
    ps1 = s1; pd1 = d1;
    pd1[n1*delta1] = pd1[(n1-1)*delta1];    // boundary
    pd1[-delta1] = pd1[-delta1<<1] = pd1[0];
    for ( i = 0; i++ < n1; ) {
        ps1[0] += ((9 * (pd1[-delta1] + pd1[0]) - pd1[-delta1<<1] - pd1[delta1] + 16) >> 5);
        pd1 += delta1; ps1 += delta1;
        }
}

template <typename SampleType>
void WaveletCDF44<SampleType>::unlift1D ( int n0, int delta0, SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const
    // 1D inverse FWT
{
    ASSERT( n0 && delta0 && delta1 );
    ASSERT( s0 );
    ASSERT( s1 );
    ASSERT( d1 );
    int i, n1;
    n1 = (n0 + 1) >> 1;
    SampleType *ps0;
    SampleType *ps1, *pd1;

        // update*:
    ps1 = s1; pd1 = d1;
    pd1[n1*delta1] = pd1[(n1-1)*delta1];    // boundary
    pd1[-delta1] = pd1[-delta1<<1] = pd1[0];
    for ( i = 0; i++ < n1; ) {
        ps1[0] -= ((9 * (pd1[-delta1] + pd1[0]) - pd1[-delta1<<1] - pd1[delta1] + 16) >> 5);
        pd1 += delta1; ps1 += delta1;
        }

        // predict*:
    ps1 = s1; pd1 = d1;
    ps1[n1*delta1] = ps1[(n1+1)*delta1] = ps1[(n1-1)*delta1];
    ps1[-delta1] = ps1[0];                  // boundary
    for ( i = 0; i++ < n1; ) {
        pd1[0] += ((9 * (ps1[0] + ps1[delta1]) - ps1[-delta1] - ps1[delta1<<1] + 8) >> 4);
        pd1 += delta1; ps1 += delta1;
        }

        // merge:
    ps0 = s0;
    ps1 = s1; pd1 = d1;
    for ( i = 0; i++ < n1; ) {
        *ps0 = *ps1;
        ps1 += delta1; ps0 += delta0;
        *ps0 = *pd1;
        pd1 += delta1; ps0 += delta0;
        }
}

//-----------------------------------------------------------
//  Cohen-Daubechies-Feauveau CDF (2+2,2) wavelet:

template <typename SampleType>
WaveletCDF222<SampleType>::WaveletCDF222 ()
    // initializes a new object
{
    signature = WSignatureCDF222;
}

template <typename SampleType>
int WaveletCDF222<SampleType>::border () const
    // border needed for FWT
{
    return 2;
}

template <typename SampleType>
void WaveletCDF222<SampleType>::lift1D ( int n0, int delta0, const SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const
    // 1D forward FWT
{
    ASSERT( n0 && delta0 && delta1 );
    ASSERT( s0 );
    ASSERT( s1 );
    ASSERT( d1 );
    int i, n1;
    n1 = (n0 + 1) >> 1;
    const SampleType *ps0;
    SampleType *ps1, *pd1;

        // split:
    ps0 = s0;
    ps1 = s1; pd1 = d1;
    for ( i = 0; i++ < n1; ) {
        *ps1 = *ps0;
        ps1 += delta1; ps0 += delta0;
        *pd1 = *ps0;
        pd1 += delta1; ps0 += delta0;
        }

        // predict 1:
    ps1 = s1; pd1 = d1;
    ps1[n1*delta1] = ps1[(n1-1)*delta1];    // boundary
    for ( i = 0; i++ < n1; ) {
        pd1[0] -= ((ps1[0] + ps1[delta1] + 1) >> 1);
        pd1 += delta1; ps1 += delta1;
        }

        // update 1:
    ps1 = s1; pd1 = d1;
    pd1[-delta1] = pd1[0];                  // boundary
    for ( i = 0; i++ < n1; ) {
        ps1[0] += ((pd1[-delta1] + pd1[0] + 2) >> 2);
        pd1 += delta1; ps1 += delta1;
        }

        // predict 2:
    ps1 = s1; pd1 = d1;
    ps1[-delta1] = ps1[0];                  // boundary
    ps1[n1*delta1] = ps1[(n1+1)*delta1] = ps1[(n1-1)*delta1];
    for ( i = 0; i++ < n1; ) {
        pd1[0] -= ((ps1[0] + ps1[delta1] - ps1[-delta1] - ps1[delta1<<1] + 8) >> 4);
        pd1 += delta1; ps1 += delta1;
        }
}

template <typename SampleType>
void WaveletCDF222<SampleType>::unlift1D ( int n0, int delta0, SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const
    // 1D inverse FWT
{
    ASSERT( n0 && delta0 && delta1 );
    ASSERT( s0 );
    ASSERT( s1 );
    ASSERT( d1 );
    int i, n1;
    n1 = (n0 + 1) >> 1;
    SampleType *ps0;
    SampleType *ps1, *pd1;

        // predict 2*:
    ps1 = s1; pd1 = d1;
    ps1[-delta1] = ps1[0];                  // boundary
    ps1[n1*delta1] = ps1[(n1+1)*delta1] = ps1[(n1-1)*delta1];
    for ( i = 0; i++ < n1; ) {
        pd1[0] += ((ps1[0] + ps1[delta1] - ps1[-delta1] - ps1[delta1<<1] + 8) >> 4);
        pd1 += delta1; ps1 += delta1;
        }

        // update 1*:
    ps1 = s1; pd1 = d1;
    pd1[-delta1] = pd1[0];                  // boundary
    for ( i = 0; i++ < n1; ) {
        ps1[0] -= ((pd1[-delta1] + pd1[0] + 2) >> 2);
        pd1 += delta1; ps1 += delta1;
        }

        // predict 1*:
    ps1 = s1; pd1 = d1;
    ps1[n1*delta1] = ps1[(n1-1)*delta1];    // boundary
    for ( i = 0; i++ < n1; ) {
        pd1[0] += ((ps1[0] + ps1[delta1] + 1) >> 1);
        pd1 += delta1; ps1 += delta1;
        }

        // merge:
    ps0 = s0;
    ps1 = s1; pd1 = d1;
    for ( i = 0; i++ < n1; ) {
        *ps0 = *ps1;
        ps1 += delta1; ps0 += delta0;
        *ps0 = *pd1;
        pd1 += delta1; ps0 += delta0;
        }
}

//-----------------------------------------------------------
//  TS-transform wavelet:

template <typename SampleType>
WaveletTS<SampleType>::WaveletTS ()
    // initializes a new object
{
    signature = WSignatureTS;
}

template <typename SampleType>
int WaveletTS<SampleType>::border () const
    // border needed for FWT
{
    return 1;
}

template <typename SampleType>
void WaveletTS<SampleType>::lift1D ( int n0, int delta0, const SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const
    // 1D forward FWT
{
    ASSERT( n0 && delta0 && delta1 );
    ASSERT( s0 );
    ASSERT( s1 );
    ASSERT( d1 );
    int i, n1;
    n1 = (n0 + 1) >> 1;
    const SampleType *ps0;
    SampleType *ps1, *pd1;

        // split:
    ps0 = s0;
    ps1 = s1; pd1 = d1;
    for ( i = 0; i++ < n1; ) {
        *ps1 = *ps0;
        ps1 += delta1; ps0 += delta0;
        *pd1 = *ps0;
        pd1 += delta1; ps0 += delta0;
        }

        // S-transform:
    ps1 = s1; pd1 = d1;
    for ( i = 0; i++ < n1; ) {
        pd1[0] -= ps1[0];
        ps1[0] += (pd1[0] >> 1);
        pd1 += delta1; ps1 += delta1;
        }

        // predict:
    ps1 = s1; pd1 = d1;
    ps1[n1*delta1] = ps1[(n1-1)*delta1];    // boundary
    ps1[-delta1] = ps1[0];
    for ( i = 0; i++ < n1; ) {
        pd1[0] += ((ps1[-delta1] - ps1[delta1] + 2) >> 2);
        pd1 += delta1; ps1 += delta1;
        }
}

template <typename SampleType>
void WaveletTS<SampleType>::unlift1D ( int n0, int delta0, SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const
    // 1D inverse FWT
{
    ASSERT( n0 && delta0 && delta1 );
    ASSERT( s0 );
    ASSERT( s1 );
    ASSERT( d1 );
    int i, n1;
    n1 = (n0 + 1) >> 1;
    SampleType *ps0;
    SampleType *ps1, *pd1;

        // predict*:
    ps1 = s1; pd1 = d1;
    ps1[n1*delta1] = ps1[(n1-1)*delta1];    // boundary
    ps1[-delta1] = ps1[0];
    for ( i = 0; i++ < n1; ) {
        pd1[0] -= ((ps1[-delta1] - ps1[delta1] + 2) >> 2);
        pd1 += delta1; ps1 += delta1;
        }

        // S-transform*:
    ps1 = s1; pd1 = d1;
    for ( i = 0; i++ < n1; ) {
        ps1[0] -= (pd1[0] >> 1);
        pd1[0] += ps1[0];
        pd1 += delta1; ps1 += delta1;
        }

        // merge:
    ps0 = s0;
    ps1 = s1; pd1 = d1;
    for ( i = 0; i++ < n1; ) {
        *ps0 = *ps1;
        ps1 += delta1; ps0 += delta0;
        *ps0 = *pd1;
        pd1 += delta1; ps0 += delta0;
        }
}

//-----------------------------------------------------------
//  S+P transform wavelet:

template <typename SampleType>
WaveletSP<SampleType>::WaveletSP ()
    // initializes a new object
{
    signature = WSignatureSP;
}

template <typename SampleType>
int WaveletSP<SampleType>::border () const
    // border needed for FWT
{
    return 1;
}

template <typename SampleType>
void WaveletSP<SampleType>::lift1D ( int n0, int delta0, const SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const
    // 1D forward FWT
{
    ASSERT( n0 && delta0 && delta1 );
    ASSERT( s0 );
    ASSERT( s1 );
    ASSERT( d1 );
    int i, n1;
    n1 = (n0 + 1) >> 1;
    const SampleType *ps0;
    SampleType *ps1, *pd1;

        // split:
    ps0 = s0;
    ps1 = s1; pd1 = d1;
    for ( i = 0; i++ < n1; ) {
        *ps1 = *ps0;
        ps1 += delta1; ps0 += delta0;
        *pd1 = *ps0;
        pd1 += delta1; ps0 += delta0;
        }

        // S-transform:
    ps1 = s1; pd1 = d1;
    for ( i = 0; i++ < n1; ) {
        pd1[0] -= ps1[0];
        ps1[0] += (pd1[0] >> 1);
        pd1 += delta1; ps1 += delta1;
        }

        // predict:
    ps1 = s1; pd1 = d1;
    ps1[n1*delta1] = ps1[(n1-1)*delta1];    // boundary
    ps1[-delta1] = ps1[0];
    pd1[n1*delta1] = 0;
    for ( i = 0; i++ < n1; ) {
        pd1[0] += (((ps1[-delta1]<<1) + ps1[0] - 3*ps1[delta1] + (pd1[delta1]<<1)) >> 3);
        pd1 += delta1; ps1 += delta1;
        }
}

template <typename SampleType>
void WaveletSP<SampleType>::unlift1D ( int n0, int delta0, SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const
    // 1D inverse FWT
{
    ASSERT( n0 && delta0 && delta1 );
    ASSERT( s0 );
    ASSERT( s1 );
    ASSERT( d1 );
    int i, n1;
    n1 = (n0 + 1) >> 1;
    SampleType *ps0;
    SampleType *ps1, *pd1;

        // predict*:
    ps1 = s1; pd1 = d1;
    ps1[n1*delta1] = ps1[(n1-1)*delta1];    // boundary
    ps1[-delta1] = ps1[0];
    pd1[n1*delta1] = 0;
    ps1 += n1 * delta1;
    pd1 += n1 * delta1;
    for ( i = n1; --i >= 0; ) {
        pd1 -= delta1; ps1 -= delta1;
        pd1[0] -= (((ps1[-delta1]<<1) + ps1[0] - 3*ps1[delta1] + (pd1[delta1]<<1)) >> 3);
        }

        // S-transform*:
    ps1 = s1; pd1 = d1;
    for ( i = 0; i++ < n1; ) {
        ps1[0] -= (pd1[0] >> 1);
        pd1[0] += ps1[0];
        pd1 += delta1; ps1 += delta1;
        }

        // merge:
    ps0 = s0;
    ps1 = s1; pd1 = d1;
    for ( i = 0; i++ < n1; ) {
        *ps0 = *ps1;
        ps1 += delta1; ps0 += delta0;
        *ps0 = *pd1;
        pd1 += delta1; ps0 += delta0;
        }
}

//-----------------------------------------------------------
//  Cohen-Daubechies-Feauveau CDF (6,2) wavelet:

template <typename SampleType>
WaveletCDF62<SampleType>::WaveletCDF62 ()
    // initializes a new object
{
    signature = WSignatureCDF62;
}

template <typename SampleType>
int WaveletCDF62<SampleType>::border () const
    // border needed for FWT
{
    return 3;
}

template <typename SampleType>
void WaveletCDF62<SampleType>::lift1D ( int n0, int delta0, const SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const
    // 1D forward FWT
{
    ASSERT( n0 && delta0 && delta1 );
    ASSERT( s0 );
    ASSERT( s1 );
    ASSERT( d1 );
    int i, n1;
    n1 = (n0 + 1) >> 1;
    const SampleType *ps0;
    SampleType *ps1, *pd1;

        // split:
    ps0 = s0;
    ps1 = s1; pd1 = d1;
    for ( i = 0; i++ < n1; ) {
        *ps1 = *ps0;
        ps1 += delta1; ps0 += delta0;
        *pd1 = *ps0;
        pd1 += delta1; ps0 += delta0;
        }

        // predict:
    ps1 = s1; pd1 = d1;
    ps1[n1*delta1] = ps1[(n1+1)*delta1] = ps1[(n1+2)*delta1] = ps1[(n1-1)*delta1];
    ps1[-delta1] = ps1[-delta1<<1] = ps1[0]; // boundary
    for ( i = 0; i++ < n1; ) {
        pd1[0] -= ((150 * (ps1[0] + ps1[delta1]) -
                     25 * (ps1[-delta1] + ps1[delta1<<1]) + 
                      3 * (ps1[-delta1<<1] + ps1[delta1*3]) + 128) >> 8);
        pd1 += delta1; ps1 += delta1;
        }

        // update:
    ps1 = s1; pd1 = d1;
    pd1[-delta1] = pd1[0];                  // boundary
    for ( i = 0; i++ < n1; ) {
        ps1[0] += ((pd1[-delta1] + pd1[0] + 2) >> 2);
        pd1 += delta1; ps1 += delta1;
        }
}

template <typename SampleType>
void WaveletCDF62<SampleType>::unlift1D ( int n0, int delta0, SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const
    // 1D inverse FWT
{
    ASSERT( n0 && delta0 && delta1 );
    ASSERT( s0 );
    ASSERT( s1 );
    ASSERT( d1 );
    int i, n1;
    n1 = (n0 + 1) >> 1;
    SampleType *ps0;
    SampleType *ps1, *pd1;

        // update*:
    ps1 = s1; pd1 = d1;
    pd1[-delta1] = pd1[0];                  // boundary
    for ( i = 0; i++ < n1; ) {
        ps1[0] -= ((pd1[-delta1] + pd1[0] + 2) >> 2);
        pd1 += delta1; ps1 += delta1;
        }

        // predict*:
    ps1 = s1; pd1 = d1;
    ps1[n1*delta1] = ps1[(n1+1)*delta1] = ps1[(n1+2)*delta1] = ps1[(n1-1)*delta1];
    ps1[-delta1] = ps1[-delta1<<1] = ps1[0]; // boundary
    for ( i = 0; i++ < n1; ) {
        pd1[0] += ((150 * (ps1[0] + ps1[delta1]) -
                     25 * (ps1[-delta1] + ps1[delta1<<1]) + 
                      3 * (ps1[-delta1<<1] + ps1[delta1*3]) + 128) >> 8);
        pd1 += delta1; ps1 += delta1;
        }

        // merge:
    ps0 = s0;
    ps1 = s1; pd1 = d1;
    for ( i = 0; i++ < n1; ) {
        *ps0 = *ps1;
        ps1 += delta1; ps0 += delta0;
        *ps0 = *pd1;
        pd1 += delta1; ps0 += delta0;
        }
}

//-----------------------------------------------------------
//  4-tap orthonormal (D4) wavelet:

template <typename SampleType>
WaveletD4<SampleType>::WaveletD4 ()
    // initializes a new object
{
    signature = WSignatureD4;
}

template <typename SampleType>
int WaveletD4<SampleType>::border () const
    // border needed for FWT
{
    return 1;
}

template <typename SampleType>
void WaveletD4<SampleType>::lift1D ( int n0, int delta0, const SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const
    // 1D forward FWT
{
    ASSERT( n0 && delta0 && delta1 );
    ASSERT( s0 );
    ASSERT( s1 );
    ASSERT( d1 );
    int i, n1;
    n1 = (n0 + 1) >> 1;
    const SampleType *ps0;
    SampleType *ps1, *pd1;

        // split:
    ps0 = s0;
    ps1 = s1; pd1 = d1;
    for ( i = 0; i++ < n1; ) {
        *ps1 = *ps0;
        ps1 += delta1; ps0 += delta0;
        *pd1 = *ps0;
        pd1 += delta1; ps0 += delta0;
        }

        // predict 1:
    ps1 = s1; pd1 = d1;
    ps1[n1*delta1] = ps1[(n1-1)*delta1];    // boundary
    for ( i = 0; i++ < n1; ) {
        pd1[0] -= (SampleType)floor(0.57735026919 * ps1[delta1] + 0.5);
        pd1 += delta1; ps1 += delta1;
        }

        // update:
    ps1 = s1; pd1 = d1;
    pd1[-delta1] = pd1[0];                  // boundary
    for ( i = 0; i++ < n1; ) {
        ps1[0] += (SampleType)floor(0.2009618943 * pd1[0] + 0.4330127019 * pd1[-delta1] + 0.5);
        pd1 += delta1; ps1 += delta1;
        }

        // predict 2:
    ps1 = s1; pd1 = d1;
    for ( i = 0; i++ < n1; ) {
        pd1[0] -= (SampleType)floor(ps1[0] / 3.0 + 0.5);
        pd1 += delta1; ps1 += delta1;
        }
}

template <typename SampleType>
void WaveletD4<SampleType>::unlift1D ( int n0, int delta0, SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const
    // 1D inverse FWT
{
    ASSERT( n0 && delta0 && delta1 );
    ASSERT( s0 );
    ASSERT( s1 );
    ASSERT( d1 );
    int i, n1;
    n1 = (n0 + 1) >> 1;
    SampleType *ps0;
    SampleType *ps1, *pd1;

        // predict 2*:
    ps1 = s1; pd1 = d1;
    for ( i = 0; i++ < n1; ) {
        pd1[0] += (SampleType)floor(ps1[0] / 3.0 + 0.5);
        pd1 += delta1; ps1 += delta1;
        }

        // update*:
    ps1 = s1; pd1 = d1;
    pd1[-delta1] = pd1[0];                  // boundary
    for ( i = 0; i++ < n1; ) {
        ps1[0] -= (SampleType)floor(0.2009618943 * pd1[0] + 0.4330127019 * pd1[-delta1] + 0.5);
        pd1 += delta1; ps1 += delta1;
        }

        // predict 1*:
    ps1 = s1; pd1 = d1;
    ps1[n1*delta1] = ps1[(n1-1)*delta1];    // boundary
    for ( i = 0; i++ < n1; ) {
        pd1[0] += (SampleType)floor(0.57735026919 * ps1[delta1] + 0.5);
        pd1 += delta1; ps1 += delta1;
        }

        // merge:
    ps0 = s0;
    ps1 = s1; pd1 = d1;
    for ( i = 0; i++ < n1; ) {
        *ps0 = *ps1;
        ps1 += delta1; ps0 += delta0;
        *ps0 = *pd1;
        pd1 += delta1; ps0 += delta0;
        }
}

//-----------------------------------------------------------
//  Cohen-Daubechies-Feauveau CDF (4,4) unbiased wavelet:

template <typename SampleType>
WaveletCDF44b<SampleType>::WaveletCDF44b ()
    // initializes a new object
{
    signature = WSignatureCDF44b;
}

template <typename SampleType>
int WaveletCDF44b<SampleType>::border () const
    // border needed for FWT
{
    return 2;
}

template <typename SampleType>
void WaveletCDF44b<SampleType>::lift1D ( int n0, int delta0, const SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const
    // 1D forward FWT
{
    ASSERT( n0 && delta0 && delta1 );
    ASSERT( s0 );
    ASSERT( s1 );
    ASSERT( d1 );
    int i, n1;
    n1 = (n0 + 1) >> 1;
    const SampleType *ps0;
    SampleType *ps1, *pd1;

        // split:
    ps0 = s0;
    ps1 = s1; pd1 = d1;
    for ( i = 0; i++ < n1; ) {
        *ps1 = *ps0;
        ps1 += delta1; ps0 += delta0;
        *pd1 = *ps0;
        pd1 += delta1; ps0 += delta0;
        }

        // predict:
    ps1 = s1; pd1 = d1;
    ps1[n1*delta1] = ps1[(n1+1)*delta1] = ps1[(n1-1)*delta1];
    ps1[-delta1] = ps1[0];                  // boundary
    for ( i = 0; i++ < n1; ) {
        pd1[0] -= ((9 * (ps1[0] + ps1[delta1]) - ps1[-delta1] - ps1[delta1<<1] + 8) >> 4);
        pd1 += delta1; ps1 += delta1;
        }

        // update:
    ps1 = s1; pd1 = d1;
    pd1[n1*delta1] = pd1[(n1-1)*delta1];    // boundary
    pd1[-delta1] = pd1[-delta1<<1] = pd1[0];
    for ( i = 0; i++ < n1; ) {
        ps1[0] += ((9 * (pd1[-delta1] + pd1[0]) - pd1[-delta1<<1] - pd1[delta1] + 16) >> 5);
        pd1 += delta1; ps1 += delta1;
        }
}

template <typename SampleType>
void WaveletCDF44b<SampleType>::unlift1D ( int n0, int delta0, SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const
    // 1D inverse FWT
{
    ASSERT( n0 && delta0 && delta1 );
    ASSERT( s0 );
    ASSERT( s1 );
    ASSERT( d1 );
    int i, n1;
    n1 = (n0 + 1) >> 1;
    SampleType *ps0;
    SampleType *ps1, *pd1;

        // update*:
    ps1 = s1; pd1 = d1;
    pd1[n1*delta1] = pd1[(n1-1)*delta1];    // boundary
    pd1[-delta1] = pd1[-delta1<<1] = pd1[0];
    for ( i = 0; i++ < n1; ) {
        ps1[0] -= ((9 * (pd1[-delta1] + pd1[0]) - pd1[-delta1<<1] - pd1[delta1] + 16) >> 5);
        pd1 += delta1; ps1 += delta1;
        }

        // predict*:
    ps1 = s1; pd1 = d1;
    ps1[n1*delta1] = ps1[(n1+1)*delta1] = ps1[(n1-1)*delta1];
    ps1[-delta1] = ps1[0];                  // boundary
    for ( i = 0; i++ < n1; ) {
        pd1[0] += ((9 * (ps1[0] + ps1[delta1]) - ps1[-delta1] - ps1[delta1<<1] + 8) >> 4);
        pd1 += delta1; ps1 += delta1;
        }

        // merge:
    ps0 = s0;
    ps1 = s1; pd1 = d1;
    for ( i = 0; i++ < n1; ) {
        *ps0 = *ps1;
        ps1 += delta1; ps0 += delta0;
        *ps0 = *pd1;
        pd1 += delta1; ps0 += delta0;
        }
}
