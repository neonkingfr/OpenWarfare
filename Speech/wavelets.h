#ifdef _MSC_VER
#  pragma once
#endif

/**
    @file   wavelets.h
    @brief  Wavelet core, uses PPP lifting schemes.

    Copyright &copy; 2000-2002 by Josef Pelikan, MFF UK Prague
    http://cgg.ms.mff.cuni.cz/~pepca/
    @date   3.9.2002
*/

#ifndef _WAVELETS_H
#define _WAVELETS_H

//-----------------------------------------------------------
//  General wavelet object - Cohen-Daubechies-Feauveau CDF (2,2):

#define WSignatureNone      0x00
#define WSignatureCDF22     0x01

template <typename SampleType>
class Wavelet {

public:

    unsigned signature;                     // 8-bit wavelet signature

    Wavelet ();
        // initializes a new object

    virtual ~Wavelet ();
        // recycles the used memory

    // computation:

    virtual int border () const;
        // border needed for FWT

    virtual void lift1D ( int n0, int delta0, const SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const;
        // 1D forward FWT

    virtual void unlift1D ( int n0, int delta0, SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const;
        // 1D inverse FWT

    };

template <typename SampleType>
Wavelet<SampleType> *createWavelet ( unsigned sig );

//-----------------------------------------------------------
//  Daubechies (9-7) symmetric biorthogonal wavelet:

#define WSignatureD97       0x02

template <typename SampleType>
class WaveletD97 : public Wavelet<SampleType> {

public:

    WaveletD97 ();
        // initializes a new object

    // computation:

    virtual int border () const;
        // border needed for FWT

    virtual void lift1D ( int n0, int delta0, const SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const;
        // 1D forward FWT

    virtual void unlift1D ( int n0, int delta0, SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const;
        // 1D inverse FWT

    };

//-----------------------------------------------------------
//  Cohen-Daubechies-Feauveau CDF (4,2) wavelet:

#define WSignatureCDF42     0x03

template <typename SampleType>
class WaveletCDF42 : public Wavelet<SampleType> {

public:

    WaveletCDF42 ();
        // initializes a new object

    // computation:

    virtual int border () const;
        // border needed for FWT

    virtual void lift1D ( int n0, int delta0, const SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const;
        // 1D forward FWT

    virtual void unlift1D ( int n0, int delta0, SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const;
        // 1D inverse FWT

    };

//-----------------------------------------------------------
//  Cohen-Daubechies-Feauveau CDF (4,4) wavelet:

#define WSignatureCDF44     0x04

template <typename SampleType>
class WaveletCDF44 : public Wavelet<SampleType> {

public:

    WaveletCDF44 ();
        // initializes a new object

    // computation:

    virtual int border () const;
        // border needed for FWT

    virtual void lift1D ( int n0, int delta0, const SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const;
        // 1D forward FWT

    virtual void unlift1D ( int n0, int delta0, SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const;
        // 1D inverse FWT

    };

//-----------------------------------------------------------
//  Cohen-Daubechies-Feauveau CDF (2+2,2) wavelet:

#define WSignatureCDF222    0x05

template <typename SampleType>
class WaveletCDF222 : public Wavelet<SampleType> {

public:

    WaveletCDF222 ();
        // initializes a new object

    // computation:

    virtual int border () const;
        // border needed for FWT

    virtual void lift1D ( int n0, int delta0, const SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const;
        // 1D forward FWT

    virtual void unlift1D ( int n0, int delta0, SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const;
        // 1D inverse FWT

    };

//-----------------------------------------------------------
//  TS-transform wavelet:

#define WSignatureTS        0x06

template <typename SampleType>
class WaveletTS : public Wavelet<SampleType> {

public:

    WaveletTS ();
        // initializes a new object

    // computation:

    virtual int border () const;
        // border needed for FWT

    virtual void lift1D ( int n0, int delta0, const SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const;
        // 1D forward FWT

    virtual void unlift1D ( int n0, int delta0, SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const;
        // 1D inverse FWT

    };

//-----------------------------------------------------------
//  S+P transform wavelet:

#define WSignatureSP        0x07

template <typename SampleType>
class WaveletSP : public Wavelet<SampleType> {

public:

    WaveletSP ();
        // initializes a new object

    // computation:

    virtual int border () const;
        // border needed for FWT

    virtual void lift1D ( int n0, int delta0, const SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const;
        // 1D forward FWT

    virtual void unlift1D ( int n0, int delta0, SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const;
        // 1D inverse FWT

    };

//-----------------------------------------------------------
//  Cohen-Daubechies-Feauveau CDF (6,2) wavelet:

#define WSignatureCDF62     0x08

template <typename SampleType>
class WaveletCDF62 : public Wavelet<SampleType> {

public:

    WaveletCDF62 ();
        // initializes a new object

    // computation:

    virtual int border () const;
        // border needed for FWT

    virtual void lift1D ( int n0, int delta0, const SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const;
        // 1D forward FWT

    virtual void unlift1D ( int n0, int delta0, SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const;
        // 1D inverse FWT

    };

//-----------------------------------------------------------
//  4-tap orthonormal (D4) wavelet:

#define WSignatureD4        0x09

template <typename SampleType>
class WaveletD4 : public Wavelet<SampleType> {

public:

    WaveletD4 ();
        // initializes a new object

    // computation:

    virtual int border () const;
        // border needed for FWT

    virtual void lift1D ( int n0, int delta0, const SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const;
        // 1D forward FWT

    virtual void unlift1D ( int n0, int delta0, SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const;
        // 1D inverse FWT

    };

//-----------------------------------------------------------
//  Cohen-Daubechies-Feauveau CDF (4,4) unbiased wavelet:

#define WSignatureCDF44b    0x0A

template <typename SampleType>
class WaveletCDF44b : public Wavelet<SampleType> {

public:

    WaveletCDF44b ();
        // initializes a new object

    // computation:

    virtual int border () const;
        // border needed for FWT

    virtual void lift1D ( int n0, int delta0, const SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const;
        // 1D forward FWT

    virtual void unlift1D ( int n0, int delta0, SampleType *s0, int delta1, SampleType *s1, SampleType *d1 ) const;
        // 1D inverse FWT

    };

#endif
