#include "StreamTokenizer.hpp"

void StreamTokenizer::Init()
{
  _stream = NULL;
  _history.clear();
  _stringVal.clear();
}

TokenTypes StreamTokenizer::RemoveControlCharAndSpace()
{
  // For this line ignore all control characters, this also includes 
  // the delimiter of space and new line feeds.
  while (*_stream && *_stream <= (char)0x020) _stream++;
  if (!(*_stream)) 
    return TT_EOF;    
  else 
    return (TokenTypes)*_stream;
}

void StreamTokenizer::InitNextToken()
{
  _stringVal.clear();
  _history.push_back(TokenizerRollBack(_stream));
}

// we take our data.
StreamTokenizer::StreamTokenizer(ifstream &inputFile)
{
  Parse(inputFile);
}
StreamTokenizer::StreamTokenizer(const char *data)
{
  Parse(data);
}

// Will cause Object to Init itself and start again
void StreamTokenizer::Parse(ifstream &inputFile)
{
  Init();

  char buffer[1024];
  while (inputFile.good())
  {
    inputFile.read(buffer,1024);
    _rawData.append(buffer,inputFile.gcount());
  }
  _stream = _rawData.data();
}

// Will cause Object to Init itself and start again
void StreamTokenizer::Parse(const char *data)
{
  Init();

  _rawData = std::string(data);
  _stream = _rawData.data();
}

// Roll back to the previous token.
// this is easy, we pop the current one
TokenTypes StreamTokenizer::PreviousStreamToken()
{
  if (((int)_history.size())-2 < 0)
  {
    _stream = _rawData.data();
    return TT_SOF;
  }

  // Pop the current item off our history
  _history.pop_back();

  // we re-parse from this location on-wards
  _stream = _history[_history.size()-1]._pos;

  // we pop the location we're going to parse on-wards
  // off because we're going to add it again!
  _history.pop_back();

  return NextStreamToken();
}

// retrieve the next token based on the type.
TokenTypes StreamTokenizer::NextStreamToken()
{
  InitNextToken();

  while (*_stream)
  {
    if (RemoveControlCharAndSpace()==TT_EOF) return TT_EOF;

    // We do some simple parsing here.
    // if we detect // we ignore all till the control of either \n or \r
    const char *ignoreLine = "//";
    if (strncmp(_stream,ignoreLine,strlen(ignoreLine))==0)
    {
      // We hit the end of line feed!
      // Start all over again, at the new line
      while(*_stream && *_stream != '\r' && *_stream != '\n' && *_stream != '\f' ) _stream++;
      continue;
    }

    // if we detect /* we ignore all till */
    const char *ignoreStart = "/*";
    const char *ignoreEnd   = "*/";
    if (!strncmp(_stream,ignoreStart,strlen(ignoreStart)))
    {
      const char *findEnd = strstr(_stream,ignoreEnd);
      if (!findEnd) return TT_EOF;

      findEnd += 2;
      if (!(*findEnd)) return TT_EOF;

      _stream = findEnd;
      continue;          
    }

    // Everything okay, continue to the next part
    break;
  } 
  if (!(*_stream)) return TT_EOF;

  bool identifier = false;
  while (*_stream)
  {
    char myChar = (char) *_stream;
    
    bool valid = false;
    if (isalpha(myChar) || isdigit(myChar) || myChar == '_') valid = true;
    if (!valid) break; // Invalid non number or identifier detected break..

    identifier = true;

    _stringVal.append(1,myChar);
    _stream++;
  }

  bool isAllDigits = false;
  if (identifier)
  {
    // Here we check if its all numbers or not, if it is then its a 
    // number.
    const char *str = _stringVal.data();
    bool allNum = true;
    while (*str)
    {
      if (!isdigit(*str)) 
      {
        allNum = false;
        break;
      }
      str++;
    }
    if (allNum)
    {
      identifier = false;
      isAllDigits = true;
    }
  }
  

  // was a identifier, we return. very simple.
  if (identifier) return TT_WORD;
  if (!(*_stream)) return TT_EOF;

  // We couldn't read a identifier. It could be a possible string literal.
  // We read until we find the next "
  switch (*_stream)
  {
  case '\"':
    {
      _stream++;
      // We don't need to record the ", that is obvious by the type
      while (*_stream && *_stream != '\"')
      {
        if (*_stream == '\\') //escape next character
        {
          ++_stream;
        }
        _stringVal.append(1,*_stream);
        _stream++;
      }
      if (!(*_stream)) return TT_EOF;

      // Everything okay increment
      _stream++;

      // Return that we have a string literal
      return TT_STRING_LITERAL;
    }
    break;
  default:
    {
      // stream may actually be a float number 0.0
      if (isAllDigits)
      {
        std::string numberLiteral;
        bool fullStopHit=false;

        while (isdigit(*_stream) || *_stream == '.')
        {
          if (*_stream == '.' && fullStopHit) break;
          if (*_stream == '.' && !fullStopHit) fullStopHit = true;

          _stringVal.append(1,*_stream);
          _stream++;
        }

        if (!(*_stream)) TT_EOF;

        // Has a full stop its double, otherwise its just a int const
        return fullStopHit ? TT_FLOAT_CONST : TT_INT_CONST;
      }

      // Unknown single character.
      int myChar = (char)*_stream++;
      return (TokenTypes)myChar;
    }
  }
}