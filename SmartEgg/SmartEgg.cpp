#include "stdafx.h"
#include "SmartEgg.h"
#include "MainFrm.h"

#include "SmartEggDoc.h"
#include "SmartEggView.h"
#include "ChooseParticipants.h"

#include <El/elementpch.hpp>
#include <El/Modules/modules.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

LPCTSTR CSmartEggApp::_contextSection = _T( "context" );
LPCTSTR CSmartEggApp::_propertiesEntry = _T( "properties" );

BEGIN_MESSAGE_MAP(CSmartEggApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
END_MESSAGE_MAP()

CSmartEggApp::CSmartEggApp()
  : _player( -1 )
{
}

//! The one and only CSmartEggApp object
CSmartEggApp theApp;

BOOL CSmartEggApp::InitInstance()
{
  InitModules();

	// InitCommonControls() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	InitCommonControls();

	CWinApp::InitInstance();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));
	LoadStdProfileSettings(8);  // Load standard INI file options (including MRU)

  //CString curDir;
  //::GetCurrentDirectory( _MAX_PATH, curDir.GetBuffer( _MAX_PATH ) );
  //curDir.ReleaseBuffer();
  //ASSERT( _taccess( _T( "WordNet.txt" ), 04 ) == 0 );
  //_knowledgeBase->GetWordBase()->Init( "WordNet.txt" );
  if( _taccess( _T( "SmartEgg.cfg" ), 04 ) != 0 )
  {
    MessageBox( NULL, _T( "File \"SmartEgg.cfg\" is missing." ), _T( "Error" ), MB_OK );
    return FALSE;
  }
  _cfgFile.Parse( "SmartEgg.cfg" );
  if( _taccess( _T( "common.cfg" ), 04 ) != 0 )
  {
    MessageBox( NULL, _T( "File \"common.cfg\" is missing." ), _T( "Error" ), MB_OK );
    return FALSE;
  }
  _globalFile.Parse( "common.cfg" );
  _conversationCenter = new ConversationCenterDefault( _globalFile,
    _cfgFile >> "CfgKBWords", _cfgFile >> "CfgKBRules", _cfgFile >> "CfgKBResponses",
    _cfgFile >> "identitiesDir", _cfgFile >> "identitiesMask", _cfgFile >> "rulesRoot", _cfgFile, 
    _cfgFile.FindEntry( "initScript" ) );
  
  FindArray< int > disabled;
  do {
    CChooseParticipants choosePlayer( _T( "Choose Player" ), _conversationCenter, disabled );
    if( choosePlayer.DoModal() != IDOK )
    {
      return FALSE;
    }
    _player = choosePlayer.GetParticipant();
  } while( _player == -1 );

  disabled.Add( _player );
  CChooseParticipants chooseParticipants( _T( "Choose Conversation Participant(s)" ), _conversationCenter, disabled );
  if( chooseParticipants.DoModal() != IDOK )
  {
    return FALSE;
  }
  chooseParticipants.GetParticipants( _participants );
  
	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views
	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CSmartEggDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CSmartEggView));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);
	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);
	// Dispatch commands specified on the command line.  Will return FALSE if
	// app was launched with /RegServer, /Register, /Unregserver or /Unregister.
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;
	// The one and only window has been initialized, so show and update it
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();
	// call DragAcceptFiles only if there's a suffix
	//  In an SDI app, this should occur after ProcessShellCommand

	return TRUE;
}

//! CAboutDlg dialog used for App About
class CAboutDlg : public CDialog
{
public:
	enum { IDD = IDD_ABOUTBOX };

  CAboutDlg();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

void CSmartEggApp::OnAppAbout()
{
  // App command to run the dialog
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

int CSmartEggApp::ExitInstance()
{
  _conversationCenter.Free();
  _participants.Clear();
  _cfgFile.Clear();
  _globalFile.Clear();

  return CWinApp::ExitInstance();
}
