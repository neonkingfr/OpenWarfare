#include "stdafx.h"
#include "SmartEgg.h"
#include "ChooseParticipants.h"

IMPLEMENT_DYNAMIC(CChooseParticipants, CDialog)
CChooseParticipants::CChooseParticipants( LPCTSTR title, ConversationCenterDefault *conversationCenter, 
  const FindArray< int > &disabled, CWnd* pParent /*=NULL*/ )
	: CDialog(CChooseParticipants::IDD, pParent), _title( title ), 
	_conversationCenter( conversationCenter ), _disabled( disabled )
{
}

CChooseParticipants::~CChooseParticipants()
{
  _conversationCenter.Free();
  _participants.Clear();
}

void CChooseParticipants::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_PARTICIPANTS, _participantsCtrl);
}


BEGIN_MESSAGE_MAP(CChooseParticipants, CDialog)
END_MESSAGE_MAP()

BOOL CChooseParticipants::OnInitDialog()
{
  CDialog::OnInitDialog();
  SetWindowText( _title );

  _codes.Clear();
  USES_CONVERSION;
  RString name;
  int n = _conversationCenter->GetParticipantsSize();
  for( int i = 0; i < n; ++i )
  {
    if( _disabled.Find( i ) == -1 )
    {
      _codes.Add( i );
      _conversationCenter->GetParticipantName( i, name );
      _participantsCtrl.AddString( A2CT( name.Data() ) );
    }
  }
  _participantsCtrl.SetCurSel( 0 );

  _participantsCtrl.SetFocus();
  return FALSE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void CChooseParticipants::GetParticipants( AutoArray< int > &participants )
{
  participants = _participants;
}

int CChooseParticipants::GetParticipant()
{
  return _participants.Size() != 0 ? _participants[ 0 ] : -1;
}

void CChooseParticipants::OnOK()
{
  Ref< KnowledgeBase > kb;
  int n = _participantsCtrl.GetCount();
  for( int i = 0; i < n; ++i )
  {
    if( _participantsCtrl.GetSel( i ) > 0 )
    {
      _participants.Add( _codes[ i ] );
    }
  }

  CDialog::OnOK();
}
