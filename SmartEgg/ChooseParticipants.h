#pragma once
#include "afxwin.h"

//! Dialog, where user can choose the participants of the conversation.
class CChooseParticipants : public CDialog
{
	DECLARE_DYNAMIC(CChooseParticipants)

public:
  enum { IDD = IDD_CHOOSEPARTICIPANTS };

	CChooseParticipants( LPCTSTR title, ConversationCenterDefault *conversationCenter, 
	  const FindArray< int > &disabled, CWnd* pParent = NULL );   // standard constructor
	virtual ~CChooseParticipants();

protected:
  Ref< ConversationCenterDefault > _conversationCenter;
  AutoArray< int > _participants;
  CString _title;
  const FindArray< int > &_disabled;
  AutoArray< int > _codes;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  virtual void OnOK();

	DECLARE_MESSAGE_MAP()

public:
  CListBox _participantsCtrl;
  
  //! Call this method when dialog is done using OnOK() method to get the result.
  void GetParticipants( AutoArray< int > &participants );
  int GetParticipant();
  virtual BOOL OnInitDialog();
};
