
#include "stdafx.h"
#include "SmartEgg.h"
#include "SmartEggDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CSmartEggDoc, CDocument)

BEGIN_MESSAGE_MAP(CSmartEggDoc, CDocument)
END_MESSAGE_MAP()

CSmartEggDoc::CSmartEggDoc()
{
}

CSmartEggDoc::~CSmartEggDoc()
{
}

BOOL CSmartEggDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

  Ref< KnowledgeBase > kb;
  RefArray< KnowledgeBase > kbs;
  int n = theApp._participants.Size();
  for( int i = 0; i < n; ++i )
  {
    theApp._conversationCenter->GetParticipant( theApp._participants[ i ], kb );
    kbs.Add( kb );
  }

  theApp._conversationCenter->GetParticipant( theApp._player, _player );

  _knowledgeBaseUI = new KnowledgeBaseUiDefault( _player, kbs, theApp._conversationCenter );

  RString title;
  if( n != 0 )
  {
    theApp._conversationCenter->GetParticipantName( theApp._participants[ 0 ], title );
    RString name;
    for( int i = 1; i < n; ++i )
    {
    	RString tmp( title, ", " );
      theApp._conversationCenter->GetParticipantName( theApp._participants[ i ], name );
    	title = RString( tmp, name );
    }
  }
  USES_CONVERSION;
  SetTitle( A2CT( title.Data() ) );

	return TRUE;
}

void CSmartEggDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

#ifdef _DEBUG
void CSmartEggDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CSmartEggDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

void CSmartEggDoc::DeleteContents()
{
  _knowledgeBaseUI.Free();

  CDocument::DeleteContents();
}
