
#pragma once

#include "afxwin.h"
#include "SentenceWnd.h"
#include "QueryOptionsBox.h"

#define IDC_QUERY           5000
#define IDC_QUERYUNDONE     5001

class CContextContentDlg;

class CSmartEggView : public CFormView
{
protected: // create from serialization only
	CSmartEggView();
	DECLARE_DYNCREATE(CSmartEggView)

public:
	enum{ IDD = IDD_SMARTEGG_FORM };
  CQueryOptionsBox _queryOptions;
  CButton _queryTell;
  CButton _queryCancel;
  CEdit _conversation;

  CSentenceWnd _query;
  CContextContentDlg *_contextContent;

public:
  virtual ~CSmartEggView();

  void UpdateQueries();
  void UpdateTell();
  void UpdateUndo();
  void ClearMenuItems();
  void UpdateMenuItems();
  void UpdateConversation();

	CSmartEggDoc* GetDocument() const;
  afx_msg void OnBnClickedQuerycancel();
  afx_msg void OnBnClickedQuerytell();

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  virtual void OnInitialUpdate(); // called first time after construct

	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnSize(UINT nType, int cx, int cy);
  afx_msg void OnUpdateEditUndo(CCmdUI *pCmdUI);
  afx_msg void OnEditUndo();
  afx_msg void OnRuleSelDown();
  afx_msg void OnRuleSelUp();
  afx_msg void OnSentenceSelectGoRight();
  afx_msg void OnSentenceTell();
  afx_msg void OnSentenceSelectGoLeft();
  afx_msg void OnDestroy();
  afx_msg void OnViewContextContent();
  afx_msg void OnUpdateViewContextContent(CCmdUI *pCmdUI);
};

#ifndef _DEBUG  // debug version in SmartEggView.cpp
inline CSmartEggDoc* CSmartEggView::GetDocument() const
   { return reinterpret_cast<CSmartEggDoc*>(m_pDocument); }
#endif
