
#include "stdafx.h"
#include "SmartEgg.h"
#include "SentenceWnd.h"
#include "SmartEggDoc.h"
#include "SmartEggView.h"

IMPLEMENT_DYNAMIC(CSentenceWnd, CWnd)

CSentenceWnd::CSentenceWnd( CSmartEggView *view, int history )
  : _view( view), _history( history )
{
}

CSentenceWnd::~CSentenceWnd()
{
}

BEGIN_MESSAGE_MAP(CSentenceWnd, CWnd)
  ON_WM_PAINT()
  ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()

void CSentenceWnd::OnPaint()
{
#define HSPACE 10
#define VSPACE 2

  CPaintDC dc(this); // device context for painting
  
  // Add your message handler code here
  // Do not call CWnd::OnPaint() for painting messages
  
  CSmartEggDoc *doc = _view->GetDocument();

  RECT r;
  GetClientRect( &r );
  dc.FillSolidRect( &r, RGB( 230, 230, 255 ) );
  
  if( doc->_knowledgeBaseUI->SentenceIsModified() )
  {
#ifdef _DEBUG
    Log( "Regenerate text structure of the sentence." );
#endif
    doc->_knowledgeBaseUI->SentenceSetModified( false );
  }

  int y = 0;
  int x = 0;
  SentenceUiFragment::DrawIterator it( doc->_knowledgeBaseUI->SentenceGetUiFragment() );
  while( it )
  {
    if( it.HasSpaceInFront() )
    {
      x += HSPACE;
    }

    int boldStyle = ( !it.IsSelected() || _history != 0 ) ? FW_NORMAL : FW_BOLD;
    BYTE italicStyle = it.IsTerminator() ? FALSE : TRUE;

    CFont font;
    int fontHeight = MulDiv( 9, GetDeviceCaps( dc.m_hDC, LOGPIXELSY ), 72 );
    BOOL b = font.CreateFont( - fontHeight, 0, 0, 0, boldStyle, 
      italicStyle, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, 
      CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, _T( "Lucida Console" ) );
    ASSERT( b );

    CFont *oldFont = dc.SelectObject( &font );
    ASSERT( oldFont != NULL );

    DWORD abgrColor = it.GetType() == Required ? RGB( 0xA0, 0, 0 ) : it.IsTerminator() ? RGB( 0, 0, 0 ) : RGB( 0x60, 0x60, 0x60 );
    COLORREF oldColor = dc.SetTextColor( abgrColor );

    RString text;
    it.GetText( text );

    int w, h;
    GetTextExtent( dc, text, w, h );
    if( x + w > r.right )
    {
      x = 0;
      y += fontHeight + VSPACE;
    }

    USES_CONVERSION;
    dc.TextOut( x, y, A2CT( text.Data() ), text.GetLength() );

    dc.SetTextColor( oldColor );
    dc.SelectObject( oldFont );

    if( _history == 0 )
    {
      it.SetClickRect( x, y, x + w, y + h );
    }

    x += w;
    it++;
  }
  
#undef HSPACE
#undef VSPACE
}

void CSentenceWnd::GetTextExtent( CDC &dc, const RString &text, int &width, int &height )
{
  USES_CONVERSION;
  CSize s = dc.GetTextExtent( A2CT( text.Data() ), text.GetLength() );
  width = s.cx;
  height = s.cy;
}

void CSentenceWnd::OnLButtonDown(UINT nFlags, CPoint point)
{
  if( _history == 0 )
  {
    CSmartEggDoc *doc = _view->GetDocument();
    {
      CClientDC dc( this );
      doc->_knowledgeBaseUI->SentenceSelectAtPoint( point.x, point.y );
    }
    _view->UpdateQueries();
    _view->UpdateUndo();
    _view->UpdateMenuItems();
  }
}
