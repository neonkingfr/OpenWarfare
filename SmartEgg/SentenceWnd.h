
#pragma once

#include "KnowledgeBaseMFC.h"

class CSmartEggView;

class CSentenceWnd : public CWnd
{
	DECLARE_DYNAMIC(CSentenceWnd)

protected:
  CSmartEggView *_view;
  const int _history;

public:
	CSentenceWnd( CSmartEggView *view, int history );
	virtual ~CSentenceWnd();
	
  static void GetTextExtent( CDC &dc, const RString &text, int &width, int &height );

protected:
	DECLARE_MESSAGE_MAP()

public:
  afx_msg void OnPaint();
  afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
};
