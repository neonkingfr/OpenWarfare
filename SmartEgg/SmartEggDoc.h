
#pragma once

#include "KnowledgeBase/SmartBase/KnowledgeBaseDefault.hpp"
#include "KnowledgeBase/SmartBase/SentenceRuleDefault.hpp"
#include "KnowledgeBase/SmartBase/KnowledgeBaseUIDefault.hpp"

class CSmartEggDoc : public CDocument
{
protected: // create from serialization only
	CSmartEggDoc();
	DECLARE_DYNCREATE(CSmartEggDoc)
  Ref< KnowledgeBaseUiDefault > _knowledgeBaseUI;
  Ref< KnowledgeBase > _player;

public:
  virtual ~CSmartEggDoc();

	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	
	inline const KnowledgeBase *GetPlayer() const
	{
	  return _player;
	}

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	DECLARE_MESSAGE_MAP()
public:
  virtual void DeleteContents();
};
