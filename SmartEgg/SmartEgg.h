#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "KnowledgeBase/SmartBase/ConversationCenterDefault.hpp"
#include "KnowledgeBase/SmartBase/KnowledgeBase.hpp"
#include "resource.h"       // main symbols

class CSmartEggApp : public CWinApp
{
public:
  static LPCTSTR _contextSection;
  static LPCTSTR _propertiesEntry;

  ParamFile _cfgFile;
  Ref< ConversationCenterDefault > _conversationCenter;
  AutoArray< int > _participants;
  int _player;
  ParamFile _globalFile;
  
	CSmartEggApp();

	virtual BOOL InitInstance();
  virtual int ExitInstance();

	afx_msg void OnAppAbout();

	DECLARE_MESSAGE_MAP()
};

extern CSmartEggApp theApp;