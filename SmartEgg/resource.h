//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SmartEgg.rc
//
#define IDD_ABOUTBOX                    100
#define IDD_SMARTEGG_FORM               101
#define IDR_MAINFRAME                   128
#define IDR_SmartEggTYPE                129
#define IDD_CHOOSEPARTICIPANTS          131
#define IDD_CONTEXTCONTENT              132
#define IDC_QUERYOPTIONS                1001
#define IDC_CONVERSATION                1002
#define IDC_QUERY_RECT                  1005
#define IDC_QUERYTELL                   1008
#define IDC_QUERYCANCEL                 1009
#define IDC_PARTICIPANTS                1012
#define IDC_CLOSE                       1013
#define IDC_UPDATE                      1014
#define IDC_CONTEXT                     1015
#define IDC_REMOVEPROPERTYNAME          1016
#define IDC_ADDPROPERTYNAME             1017
#define IDC_ADDPROPERTY                 1018
#define IDC_REMOVEPROPERTY              1019
#define ID_RULE_SEL_UP                  32772
#define ID_RULE_SEL_DOWN                32773
#define ID_SENTENCE_TELL                32782
#define ID_SENTENCE_SELECT_GO_LEFT      -32749
#define ID_SENTENCE_SELECT_GO_RIGHT     -32748
#define ID_VIEW_CONTEXTCONTENT          32789
#define ID_BUTTON32791                  32791
#define ID_BUTTON32792                  32792

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32794
#define _APS_NEXT_CONTROL_VALUE         1020
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
