#include "stdafx.h"
#include "SmartEgg.h"

#include "SmartEggDoc.h"
#include "SmartEggView.h"
#include "ContextContentDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CSmartEggView, CFormView)

BEGIN_MESSAGE_MAP(CSmartEggView, CFormView)
ON_BN_CLICKED(IDC_QUERYCANCEL, OnBnClickedQuerycancel)
ON_BN_CLICKED(IDC_QUERYTELL, OnBnClickedQuerytell)
ON_WM_SIZE()
ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO, OnUpdateEditUndo)
ON_COMMAND(ID_EDIT_UNDO, OnEditUndo)
ON_COMMAND(ID_RULE_SEL_DOWN, OnRuleSelDown)
ON_COMMAND(ID_RULE_SEL_UP, OnRuleSelUp)
ON_COMMAND(ID_SENTENCE_SELECT_GO_RIGHT, OnSentenceSelectGoRight)
ON_COMMAND(ID_SENTENCE_TELL, OnSentenceTell)
ON_COMMAND(ID_SENTENCE_SELECT_GO_LEFT, OnSentenceSelectGoLeft)
ON_WM_DESTROY()
ON_COMMAND(ID_VIEW_CONTEXTCONTENT, OnViewContextContent)
ON_UPDATE_COMMAND_UI(ID_VIEW_CONTEXTCONTENT, OnUpdateViewContextContent)
END_MESSAGE_MAP()

CSmartEggView::CSmartEggView()
	: CFormView(CSmartEggView::IDD), _query( this, 0 ), _queryOptions( this ), _contextContent( NULL )
{
}

CSmartEggView::~CSmartEggView()
{
  if( _contextContent != NULL )
  {
    delete _contextContent;
  }
}

void CSmartEggView::DoDataExchange(CDataExchange* pDX)
{
  CFormView::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_QUERYOPTIONS, _queryOptions);
  DDX_Control(pDX, IDC_QUERYTELL, _queryTell);
  DDX_Control(pDX, IDC_QUERYCANCEL, _queryCancel);
  DDX_Control(pDX, IDC_CONVERSATION, _conversation);
}

void CSmartEggView::OnInitialUpdate()
{
  CFormView::OnInitialUpdate();

  /*
  USES_CONVERSION;
  ParamEntryVal synsets = theApp.GetKnowledgeBase().GetSynsets() >> "Synsets";
  int n = synsets.GetEntryCount();
  for( int i = 0; i < n; ++i )
  {
    ParamEntryVal synset = synsets.GetEntry( i );
    ParamEntryVal words = synset >> "Words";
    ParamEntryVal wordInfo = words.GetEntry( 0 );
    RStringB word = wordInfo >> "word";
    _queryOptions.AddString( A2CT( word ) );
  }
  */
  
  RECT r;
  CWnd *query = GetDlgItem( IDC_QUERY_RECT );
  if( query != NULL )
  {
    query->GetWindowRect( &r );
    CWnd *p = query->GetParent();
    p->ScreenToClient( &r );
    BOOL b = _query.Create( NULL, _T( "" ), WS_CHILD | WS_VISIBLE, r, p, IDC_QUERY );
    ASSERT( b );
  }

  UpdateMenuItems();
  UpdateTell();
  UpdateUndo();

  GetParentFrame()->RecalcLayout();
  ResizeParentToFit();
}

#ifdef _DEBUG
void CSmartEggView::AssertValid() const
{
	CFormView::AssertValid();
}

void CSmartEggView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CSmartEggDoc* CSmartEggView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CSmartEggDoc)));
	return (CSmartEggDoc*)m_pDocument;
}
#endif //_DEBUG

void CSmartEggView::ClearMenuItems()
{
  int n = _queryOptions.GetCount();
  for( int i = 0; i < n; ++i )
  {
    KnowledgeBaseSentenceUiItem *item =
      ( KnowledgeBaseSentenceUiItem* ) _queryOptions.GetItemData( i );
    _queryOptions.SetItemData( i, ( DWORD_PTR ) NULL );
    ASSERT( item != NULL );
    item->Release();
  }
  _queryOptions.ResetContent();
}

void CSmartEggView::UpdateMenuItems()
{
  ClearMenuItems();

  CSmartEggDoc *doc = GetDocument();
  RefArray< KnowledgeBaseSentenceUiItem > items;
  int idx = doc->_knowledgeBaseUI->GetSentenceUiItems( items );
  int n = items.Size();
  if( n != 0 )
  {
    RString text;
    USES_CONVERSION;
    for( int i = 0; i < n; ++i )
    {
      KnowledgeBaseSentenceUiItem *item = items[ i ];
      ASSERT( item != NULL );
      item->GetText( text );
      int strIdx = _queryOptions.AddString( A2CT( text.Data() ) );
      _queryOptions.SetItemData( strIdx, ( DWORD_PTR ) item );
      item->AddRef();
    }
    idx = idx > 0 ? idx : 0;
    _queryOptions.SetCurSel( idx );
    doc->_knowledgeBaseUI->ChooseSentenceUiItem( items[ idx ] );

    n = _queryOptions.GetCount();
    for( int i = 0; i < n; ++i )
    {
      KnowledgeBaseSentenceUiItem *item = ( KnowledgeBaseSentenceUiItem* ) _queryOptions.GetItemData( i );
      ASSERT( item != NULL );
    }
  }
}

void CSmartEggView::UpdateTell()
{
  CSmartEggDoc *doc = GetDocument();
  _queryTell.EnableWindow( doc->_knowledgeBaseUI->CanTell() );
}

void CSmartEggView::UpdateUndo()
{
  CSmartEggDoc *doc = GetDocument();
  _queryCancel.EnableWindow( doc->_knowledgeBaseUI->CanUndo() );
}

void CSmartEggView::OnBnClickedQuerycancel()
{
  CSmartEggDoc *doc = GetDocument();
  doc->_knowledgeBaseUI->SentenceUndo();
  UpdateMenuItems();
  UpdateTell();
  UpdateUndo();
  UpdateQueries();
}

void CSmartEggView::UpdateQueries()
{
  _query.Invalidate();
  _query.UpdateWindow();
}

void CSmartEggView::OnBnClickedQuerytell()
{
  OnSentenceTell();
}

void CSmartEggView::UpdateConversation()
{
  CSmartEggDoc *doc = GetDocument();

  AutoArray< ConversationSpeech > conversation;
  doc->_knowledgeBaseUI->GetConversation( conversation );
  CString text;
  int n = conversation.Size();
  USES_CONVERSION;
  for( int i = 0; i < n; ++i )
  {
  	text += A2CT( conversation[ i ]._speech );
    text += _T( "\r\n" );
  }
  _conversation.SetWindowText( text );
  _conversation.SetSel( INT_MAX, INT_MAX, FALSE );
}

void CSmartEggView::OnSize(UINT nType, int cx, int cy)
{
  CFormView::OnSize(nType, cx, cy);

#define VSPACE 9
#define HSPACE 9
#define GETWR( ctrl, rect )   if( !::IsWindow( ctrl.m_hWnd ) ) { return; } CRect rect; ctrl.GetWindowRect( &rect ); ScreenToClient( &rect )
#define SETWR( ctrl, x, y, w, h )  ctrl.MoveWindow( x, y, w, h, TRUE )

  GETWR( _queryOptions, queryOptionsRect );
  GETWR( _conversation, conversationRect );
  GETWR( _query, queryRect );
  GETWR( _queryTell, queryTellRect );
  GETWR( _queryCancel, queryCancelRect );

  queryOptionsRect.right = queryOptionsRect.left + cx * 0.28;

  SETWR( _queryOptions, HSPACE, VSPACE, queryOptionsRect.Width(), 
    cy - VSPACE - ( VSPACE + queryTellRect.Height() + VSPACE + queryCancelRect.Height() + VSPACE ) );
  SETWR( _conversation, queryOptionsRect.right + HSPACE, VSPACE, 
    cx - ( HSPACE + queryOptionsRect.Width() + HSPACE ) - HSPACE, 
    cy - VSPACE - ( VSPACE + queryTellRect.Height() + VSPACE + queryCancelRect.Height() + VSPACE ) );
  SETWR( _query, HSPACE + queryTellRect.Width() + HSPACE, 
    cy - queryTellRect.Height() - VSPACE - queryCancelRect.Height() - VSPACE,  
    cx - ( HSPACE + queryTellRect.Width() + HSPACE ) - HSPACE, queryTellRect.Height() + VSPACE + queryCancelRect.Height() );
  SETWR( _queryTell, HSPACE, cy - queryTellRect.Height() - VSPACE - queryCancelRect.Height() - VSPACE,
    queryTellRect.Width(), queryTellRect.Height() );
  SETWR( _queryCancel, HSPACE, cy - queryTellRect.Height() - VSPACE, 
    queryTellRect.Width(), queryTellRect.Height() );
  
#undef SETWR
#undef GETWR
#undef HSPACE
#undef VSPACE

  ShowScrollBar( SB_BOTH, FALSE );
}

void CSmartEggView::OnUpdateEditUndo(CCmdUI *pCmdUI)
{
  CSmartEggDoc *doc = GetDocument();
  pCmdUI->Enable( doc->_knowledgeBaseUI->CanUndo() );
}

void CSmartEggView::OnEditUndo()
{
  CSmartEggDoc *doc = GetDocument();
  doc->_knowledgeBaseUI->SentenceUndo();
  UpdateMenuItems();
  UpdateTell();
  UpdateUndo();
  UpdateQueries();
}

void CSmartEggView::OnRuleSelDown()
{
  _queryOptions.SendMessage( WM_KEYDOWN, VK_DOWN, 0 );
  KnowledgeBaseSentenceUiItem *item = reinterpret_cast< KnowledgeBaseSentenceUiItem* >( 
    _queryOptions.GetItemData( _queryOptions.GetCurSel() ) );
  GetDocument()->_knowledgeBaseUI->ChooseSentenceUiItem( item );
  UpdateTell();
  UpdateUndo();
  UpdateQueries();
}

void CSmartEggView::OnRuleSelUp()
{
  _queryOptions.SendMessage( WM_KEYDOWN, VK_UP, 0 );
  KnowledgeBaseSentenceUiItem *item = reinterpret_cast< KnowledgeBaseSentenceUiItem* >( 
    _queryOptions.GetItemData( _queryOptions.GetCurSel() ) );
  GetDocument()->_knowledgeBaseUI->ChooseSentenceUiItem( item );
  UpdateTell();
  UpdateUndo();
  UpdateQueries();
}

void CSmartEggView::OnSentenceSelectGoRight()
{
  CSmartEggDoc *doc = GetDocument();
  doc->_knowledgeBaseUI->SentenceMoveSelectionRight();
  
  UpdateMenuItems();
  UpdateTell();
  UpdateUndo();
  UpdateQueries();
}

void CSmartEggView::OnSentenceTell()
{
  CSmartEggDoc *doc = GetDocument();
  if( doc->_knowledgeBaseUI->CanTell() )
  {
    CSmartEggDoc *doc = GetDocument();
    doc->_knowledgeBaseUI->SentenceTell();

    UpdateConversation();
    UpdateMenuItems();
    UpdateTell();
    UpdateUndo();
    UpdateQueries();
  }
  
  if( _contextContent != NULL && ::IsWindow( *_contextContent ) )
  {
    _contextContent->UpdateContextTree();
  }
}

void CSmartEggView::OnSentenceSelectGoLeft()
{
  CSmartEggDoc *doc = GetDocument();
  doc->_knowledgeBaseUI->SentenceMoveSelectionLeft();

  UpdateMenuItems();
  UpdateTell();
  UpdateUndo();
  UpdateQueries();
}

void CSmartEggView::OnDestroy()
{
  ClearMenuItems();

  CFormView::OnDestroy();
}

void CSmartEggView::OnViewContextContent()
{
  if( _contextContent == NULL )
  {
    _contextContent = new CContextContentDlg( GetDocument()->GetPlayer(), this );
    if( _contextContent == NULL )
    {
      return;
    }
  }
  if( !::IsWindow( *_contextContent ) )
  {
    _contextContent->Create( CContextContentDlg::IDD, this );
  }
  _contextContent->ShowWindow( SW_SHOW );
}

void CSmartEggView::OnUpdateViewContextContent(CCmdUI *pCmdUI)
{
  if( _contextContent != NULL && ::IsWindow( *_contextContent ) && _contextContent->IsWindowVisible() )
  {
    pCmdUI->Enable( FALSE );
    return;
  }
}
