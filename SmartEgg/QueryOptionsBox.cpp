
#include "stdafx.h"
#include "SmartEgg.h"
#include "QueryOptionsBox.h"
#include "SmartEggDoc.h"
#include "SmartEggView.h"

IMPLEMENT_DYNAMIC(CQueryOptionsBox, CListBox)

CQueryOptionsBox::CQueryOptionsBox( CSmartEggView *view )
  : _view( view )
{
}

CQueryOptionsBox::~CQueryOptionsBox()
{
}

BEGIN_MESSAGE_MAP(CQueryOptionsBox, CListBox)
  ON_WM_LBUTTONDOWN()
  ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

void CQueryOptionsBox::OnLButtonDown(UINT nFlags, CPoint point)
{
  CListBox::OnLButtonDown(nFlags, point);
  
  int curSel = GetCurSel();
  if( curSel != LB_ERR )
  {
    KnowledgeBaseSentenceUiItem *item = reinterpret_cast< KnowledgeBaseSentenceUiItem* >( 
      GetItemData( curSel ) );
    _view->GetDocument()->_knowledgeBaseUI->ChooseSentenceUiItem( item );
    _view->UpdateTell();
    _view->UpdateUndo();
    _view->UpdateQueries();

    _view->OnSentenceSelectGoRight();
  }
}

void CQueryOptionsBox::OnMouseMove(UINT nFlags, CPoint point)
{
  CListBox::OnMouseMove(nFlags, point);
  
  BOOL outside;
  UINT itemIdx = ItemFromPoint( point, outside );
  if( !outside && itemIdx != GetCurSel() )
  {
    SetCurSel( itemIdx );
  }
}
