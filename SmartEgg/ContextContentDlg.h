#pragma once
#include "afxcmn.h"
#include "afxwin.h"

class CContextContentDlg : public CDialog
{
	DECLARE_DYNAMIC(CContextContentDlg)

public:
  enum { IDD = IDD_CONTEXTCONTENT };
  CTreeCtrl _context;
  CEdit _addPropertyName;
  CComboBox _removePropertyName;
  CButton _addProperty;
  CButton _removeProperty;
  CButton _closeBtn;
  CButton _updateBtn;

  Ref< const KnowledgeBase > _kb;

public:
	CContextContentDlg(const KnowledgeBase *kb, CWnd* pParent = NULL);   // standard constructor
	virtual ~CContextContentDlg();
	
  void UpdateContextTree();
	void UpdateRemoveCombobox();

  afx_msg void OnBnClickedClose();
  afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
  afx_msg void OnBnClickedUpdate();
  afx_msg void OnBnClickedAddProperty();
  afx_msg void OnBnClickedRemoveProperty();
  afx_msg void OnSize(UINT nType, int cx, int cy);

  virtual BOOL OnInitDialog();

protected:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

  DECLARE_MESSAGE_MAP()
};
