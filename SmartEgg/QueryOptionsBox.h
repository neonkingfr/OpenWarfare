
#pragma once

class CSmartEggView;

class CQueryOptionsBox : public CListBox
{
	DECLARE_DYNAMIC(CQueryOptionsBox)

protected:
  CSmartEggView *_view;

public:
	CQueryOptionsBox( CSmartEggView *view );
	virtual ~CQueryOptionsBox();

protected:
	DECLARE_MESSAGE_MAP()

public:
  afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
  afx_msg void OnMouseMove(UINT nFlags, CPoint point);
};
