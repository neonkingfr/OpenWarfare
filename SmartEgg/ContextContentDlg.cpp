
#include "stdafx.h"
#include "SmartEgg.h"
#include "ContextContentDlg.h"

#include "Es/essencepch.hpp"
#include "Es/Strings/bstring.hpp"

IMPLEMENT_DYNAMIC(CContextContentDlg, CDialog)

CContextContentDlg::CContextContentDlg(const KnowledgeBase *kb, CWnd* pParent /*=NULL*/)
	: CDialog(CContextContentDlg::IDD, pParent), _kb( kb )
{
}

CContextContentDlg::~CContextContentDlg()
{
}

void CContextContentDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_CONTEXT, _context);
  DDX_Control(pDX, IDC_ADDPROPERTYNAME, _addPropertyName);
  DDX_Control(pDX, IDC_REMOVEPROPERTYNAME, _removePropertyName);
  DDX_Control(pDX, IDC_ADDPROPERTY, _addProperty);
  DDX_Control(pDX, IDC_CLOSE, _closeBtn);
  DDX_Control(pDX, IDC_REMOVEPROPERTY, _removeProperty);
  DDX_Control(pDX, IDC_UPDATE, _updateBtn);
}

BEGIN_MESSAGE_MAP(CContextContentDlg, CDialog)
  ON_BN_CLICKED(IDC_CLOSE, OnBnClickedClose)
  ON_WM_SHOWWINDOW()
  ON_BN_CLICKED(IDC_UPDATE, OnBnClickedUpdate)
  ON_BN_CLICKED(IDC_ADDPROPERTY, OnBnClickedAddProperty)
  ON_BN_CLICKED(IDC_REMOVEPROPERTY, OnBnClickedRemoveProperty)
  ON_WM_SIZE()
END_MESSAGE_MAP()

void CContextContentDlg::OnBnClickedClose()
{
  ShowWindow( SW_HIDE );
}

void CContextContentDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
  CDialog::OnShowWindow(bShow, nStatus);

  if( bShow )
  {
    UpdateContextTree();
  }
}

void CContextContentDlg::OnBnClickedUpdate()
{
  UpdateContextTree();
}

void CContextContentDlg::UpdateContextTree()
{
  _context.SetRedraw( FALSE );
  _context.DeleteAllItems();
  USES_CONVERSION;
  const SemanticNetDefault *sn = static_cast< const SemanticNetDefault* >( _kb->GetSemanticNet() );
  const SemanticContext &ctx = sn->GetSemanticContext();
  int notionsN = ctx.GetNotionsSize();
  RString text;
  CString properties = theApp.GetProfileString( theApp._contextSection, theApp._propertiesEntry, _T( "" ) );
  for( int notionsI = 0; notionsI < notionsN; ++notionsI )
  {
    const SemanticContextElement &notion = ctx.GetNotion( notionsI );
    Ref< const SemanticKnowledge > know = notion.GetOutcoming();
    know->GetText( _kb->GetWordBase(), text );
    HTREEITEM i = _context.InsertItem( A2CT( text.Data() ), TVI_ROOT, TVI_LAST );
    AssertDebug( i != NULL );
    if( i != NULL )
    {
      int queriesN = notion.GetQueriesSize();
      for( int queriesI = 0; queriesI < queriesN; ++queriesI )
      {
        const SemanticQuery &q = notion.GetQuery( queriesI );
      	BString< 256 > temp;
      	temp.PrintF( "%s, %d", q.GetTable().Data(), q.GetIndex() );
        HTREEITEM si = _context.InsertItem( A2CT( temp.cstr() ), i, TVI_LAST );
        AssertDebug( si != NULL );
        if( si != NULL )
        {
          int last = 0;
          int idx = properties.Find( _T( ':' ) );
          while( idx > 0 )
          {
            CString name = properties.Mid( last, idx - last );
            RStringB strB = RStringB( T2CA( name ) );
            ConstParamEntryPtr p = SemanticNetDefault::GetEntry( *sn, q, strB );
            if( p )
            {
              QOStrStream val;
              p->Save( val, 0 );
              val.put( '\0' );
              CString cstr( A2CT( val.str() ) );
              _context.InsertItem( cstr.Left( cstr.GetLength() - 2 ), si, TVI_LAST );
            }
            else
            {
              BString< 256 > b;
              b.PrintF( "%s=<NA>;", strB.Data() );
              _context.InsertItem( A2CT( b.cstr() ), si, TVI_LAST );
            }
            last = idx + 1;
            idx = properties.Find( _T( ':' ), last );
          }
          _context.Expand( si, TVE_EXPAND );
        }
      }
      _context.Expand( i, TVE_EXPAND );
    }
  }
  _context.SetRedraw( TRUE );
}

void CContextContentDlg::OnBnClickedAddProperty()
{
  CString name;
  _addPropertyName.GetWindowText( name );
  if( !name.IsEmpty() )
  {
    int len = name.Find( _T( ':' ) );
    if( len < 0 )
    {
      CString properties = theApp.GetProfileString( theApp._contextSection, theApp._propertiesEntry, _T( "" ) );
      properties += name;
      properties += _T( ":" );
      theApp.WriteProfileString( theApp._contextSection, theApp._propertiesEntry, properties );
      UpdateContextTree();
      UpdateRemoveCombobox();
    }
    else
    {
      MessageBox( _T( "Error: Property name can not contain ':'." ), _T( "Error"), MB_OK );
    }
  }
}

void CContextContentDlg::OnBnClickedRemoveProperty()
{
  CString name;
  _removePropertyName.GetWindowText( name );
  if( !name.IsEmpty() )
  {
    name += _T( ":" );
    
    CString properties = theApp.GetProfileString( theApp._contextSection, theApp._propertiesEntry, _T( "" ) );
    int idx = properties.Find( name );
    if( idx >= 0 )
    {
      CString newProperties = properties.Left( idx );
      int len = idx + name.GetLength();
      if( len < properties.GetLength() )
      {
        newProperties += properties.Mid( len );
      }
      theApp.WriteProfileString( theApp._contextSection, theApp._propertiesEntry, newProperties );
      UpdateContextTree();
      UpdateRemoveCombobox();
    }
  }
}

void CContextContentDlg::UpdateRemoveCombobox()
{
  _removePropertyName.SetRedraw( FALSE );
  _removePropertyName.ResetContent();
  CString properties = theApp.GetProfileString( theApp._contextSection, theApp._propertiesEntry, _T( "" ) );
  int last = 0;
  int idx = properties.Find( _T( ':' ) );
  while( idx > 0 )
  {
    CString name = properties.Mid( last, idx - last );
    _removePropertyName.AddString( name );
    last = idx + 1;
    idx = properties.Find( _T( ':' ), last );
  }
  _removePropertyName.SetRedraw( TRUE );
  _removePropertyName.SetCurSel( 0 );
}

BOOL CContextContentDlg::OnInitDialog()
{
  CDialog::OnInitDialog();

  UpdateRemoveCombobox();

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void CContextContentDlg::OnSize(UINT nType, int cx, int cy)
{
  SetRedraw( FALSE );

#define VSPACE 9
#define HSPACE 9
#define GETWR( ctrl, rect )   if( !::IsWindow( ctrl.m_hWnd ) ) { return; } CRect rect; ctrl.GetWindowRect( &rect ); ScreenToClient( &rect )
#define GETCBR( ctrl, rect )   if( !::IsWindow( ctrl.m_hWnd ) ) { return; } CRect rect; ctrl.GetDroppedControlRect( &rect ); ScreenToClient( &rect )
#define SETWR( ctrl, x, y, w, h )  ctrl.MoveWindow( x, y, w, h, FALSE )

  GETWR( _context, contextRect );
  GETWR( _closeBtn, closeBtnRect );
  GETWR( _updateBtn, updateBtnRect );
  GETWR( _addPropertyName, addPropertyNameRect );
  GETWR( _addProperty, addPropertyRect );
  GETCBR( _removePropertyName, removePropertyNameRect );
  GETWR( _removeProperty, removePropertyRect );

  int tx = cx - HSPACE - closeBtnRect.Width();
  SETWR( _context, HSPACE, VSPACE, cx - 3 * HSPACE - closeBtnRect.Width(), cy - 2 * HSPACE );
  SETWR( _closeBtn, tx, VSPACE, closeBtnRect.Width(), closeBtnRect.Height() );
  SETWR( _updateBtn, tx, 2 * VSPACE + closeBtnRect.Height(), closeBtnRect.Width(), closeBtnRect.Height() );
  SETWR( _addPropertyName, tx, cy - 5 * VSPACE - 4 * closeBtnRect.Height(), closeBtnRect.Width(), closeBtnRect.Height() );
  SETWR( _addProperty, tx, cy - 4 * VSPACE - 3 * closeBtnRect.Height(), closeBtnRect.Width(), closeBtnRect.Height() );
  SETWR( _removePropertyName, tx, cy - 2 * VSPACE - 2 * closeBtnRect.Height(), closeBtnRect.Width(), removePropertyNameRect.Height() );
  SETWR( _removeProperty, tx, cy - VSPACE - closeBtnRect.Height(), closeBtnRect.Width(), closeBtnRect.Height() );

#undef SETWR
#undef GETWR
#undef HSPACE
#undef VSPACE
  
  SetRedraw( TRUE );
  InvalidateRect( NULL );
  UpdateWindow();
}
