/************************************************************************/
/*                                                                      */
/* Trapezoidal decomposition based on SweepLine algorithm               */
/*                                                                      */
/************************************************************************/

#include "trapezoid.h"

///static member variables
float SweepLineSegment::_sweepLineX = 0;
int SweepLineSegment::_globalId = 0;

///global helper functions
bool ZeroPoint(Point2DRound point)
{
  //Manhatan metric should be sufficient
  if ( (fabs(point.x)+fabs(point.y) ) < ZeroPointEPS ) return true;
  return false;
}

///tests if point P2 is Left|On|Right of the line P0 to P1.
///returns: >0 for left, 0 for on, and <0 for right of the line.
inline int IsLeft( Ref<Point2DRound> P0, Ref<Point2DRound> P1, Ref<Point2DRound> P2 )
{
  return FloatSgn((P1->x - P0->x)*(P2->y - P0->y) - (P2->x - P0->x)*(P1->y - P0->y));
}

inline float AlignFloat(float x, float precision)
{
  return floor((x/precision)+0.5f)*precision;
}

bool IsEqual(const float &a, const float &b)
{
  float a2 = AlignFloat(a, ZeroPointEPS);
  float b2 = AlignFloat(b, ZeroPointEPS);
  return (fabs(a2-b2) < ZeroPointEPS/2);
}

bool IsZero(const float a)
{
  return (fabs(a)<ZeroPointEPS/2);
}

bool IsZero2(const float a)
{
  return (fabs(a)<ZeroPoint2EPS);
}

bool IsLess(float &a, float &b)
{
  return (a<b && !IsZero(a-b));
}

int FloatSgn(float a)
{
  if (IsZero2(a)) return 0;
  else return (a<0.0f ? -1 : 1);
}

SweepLineSegment::SweepLineSegment(float x1, float y1, float x2, float y2)
{
  leftEvent=NULL;
  id.LowPart = ++_globalId;
  if (id.LowPart==(short)405)
    _asm nop;
  leftP = new Point2DRound(x1,y1);
  rightP = new Point2DRound(x2,y2);
  float dx = x2 - x1;
  dydx = 1.0f;
  if (IsZero(dx))
  { //vertical segment is always not less
    vertical = true;
  }
  else 
  {
    dydx = (y2-y1)/dx;
    vertical = false;
  }
  above = below = NULL;
  trapAbove = trapBelow = NULL;
}

SweepLineSegment::SweepLineSegment(Ref<Point2DRound> leftP1, Ref<Point2DRound> rightP1)
{
  leftEvent=NULL;
  id.LowPart = ++_globalId;
  if (id.LowPart==(short)405)
    _asm nop;
  leftP = leftP1; rightP = rightP1;
  float dx = rightP->x - leftP->x;
  dydx = 1.0f;
  if (IsZero(dx))
  { //vertical segment is always not less
    vertical = true;
  }
  else 
  {
    dydx = (rightP->y-leftP->y)/dx;
    vertical = false;
  }
  above = below = NULL;
  trapAbove = trapBelow = NULL;
}


SweepLineSegment::SweepLineSegment(SweepLineSegment &seg)
{
  if ((int)this==0x01688200)
    _asm nop;
  if (seg.id.QuadPart==1311125)
    _asm nop;
  vertical = seg.vertical;
  dydx = seg.dydx;
  id = seg.id;
  orientation = seg.orientation;
  color = seg.color;
  leftP=seg.leftP; rightP=seg.rightP;
  finalColor=seg.finalColor;
  visible=seg.visible;
  leftEvent=seg.leftEvent;
}

float SweepLineSegment::YIntersect(float x)
{
  if (vertical) return leftP->y;
  else return ( leftP->y + (dydx)*(x-leftP->x) );
}

// test intersect of 2 segments and return: 0=none, 1=intersect
bool SweepLineSegment::Intersect( SweepLineSegment* seg2)
{
  if (seg2 == NULL) return false; // no intersect if second segment doesn't exist
  // check for segments intersectint at their endpoints
  // but they CANNOT be consecutive A-->B-->C
  // if (leftP==seg2->rightP || rightP==seg2->leftP) return false;
  Assert(leftP!=seg2->rightP && rightP!=seg2->leftP);
  if (leftP==seg2->leftP)
  {
    return SegmentContainsPoint(seg2, rightP);
    //typically return false; //or true for overlapping segments
  }
  if (rightP==seg2->rightP)
  {
    return SegmentContainsPoint(seg2, leftP);
  }

  int lsign, rsign;
  lsign = IsLeft(leftP, rightP, seg2->leftP); // seg2 left point sign
  rsign = IsLeft(leftP, rightP, seg2->rightP); // s2 right point sign
  if (lsign * rsign > 0) // s2 endpoints have same sign relative to s1
    return false; // => on same side => no intersect is possible
  lsign = IsLeft(seg2->leftP, seg2->rightP, leftP); // s1 left point sign
  rsign = IsLeft(seg2->leftP, seg2->rightP, rightP); // s1 right point sign
  if (lsign * rsign > 0) // s1 endpoints have same sign relative to s2
    return false; // => on same side => no intersect is possible
  // the segments straddle each other
  return true; // => an intersect exists
}
bool SegmentContainsPoint(SweepLineSegment *seg, Ref<Point2DRound> point)
{
  if (seg==NULL) return false;
  if (seg->leftP==point || seg->rightP==point) return false;
  if (seg->leftP->x > point->x  || point->x > seg->rightP->x) return false;
  if (seg->IsVertical()) return IsZero(point->x - seg->leftP->x);
  float dx = point->x - seg->leftP->x;
  float dy = point->y - seg->leftP->y;
  if (IsZero(dx)) return false;
  float dydx=dy/dx;
  return IsZero(dydx - seg->Getdydx());
}
bool PointIsOnLine(float pointX, float pointY, float x1, float y1, float x2, float y2)
{
  if ((IsZero(pointX-x1) && IsZero(pointY-y1)) || (IsZero(pointX-x2) && IsZero(pointY-y2))) return true;
  if (IsZero(x1-x2)) return (IsZero(pointX-x1)); //isVertical
  if (IsZero(pointX-x1)) return false;
  float ptDydx=(pointY-y1)/(pointX-x1);
  float dydx=(y2-y1)/(x2-x1);
  return IsZero(dydx-ptDydx);
}

Point2DRound SweepLineSegment::IntersectionPoint(SweepLineSegment *seg2)
{
  //Ax+By=C
  float A1 = rightP->y-leftP->y;
  float B1 = leftP->x-rightP->x;
  float C1 = A1*leftP->x+B1*leftP->y;
  float A2 = seg2->rightP->y-seg2->leftP->y;
  float B2 = seg2->leftP->x-seg2->rightP->x;
  float C2 = A2*seg2->leftP->x+B2*seg2->leftP->y;
  //solve the two equations for the two unknowns, x and y
  //A1x + B1y = C1
  //A2x + B2y = C2
  float det = A1*B2 - A2*B1;
  if (!IsZero(det))
  {
    Point2DRound intPt((B2*C1 - B1*C2)/det, (A1*C2 - A2*C1)/det);
    if ( intPt.x<=rightP->x && intPt.x<=seg2->rightP->x 
      && intPt.x>=leftP->x && intPt.x>=seg2->leftP->x) return intPt;
    else 
      _asm nop;
  }
  //Lines are overlapping or improper intersection
  //so return suitable segment endpoint
  if (IsZero(rightP->x-_sweepLineX)) return *rightP;
  if (IsZero(seg2->rightP->x-_sweepLineX)) return *seg2->rightP;
  if (*leftP==*seg2->leftP) 
    if (fabs(rightP->x-leftP->x)+fabs(rightP->y-leftP->y) < fabs(seg2->rightP->x-leftP->x)+fabs(seg2->rightP->y-leftP->y))
      return *rightP;
    else
      return *seg2->rightP;
  else
    if (fabs(leftP->x-rightP->x)+fabs(leftP->y-rightP->y) < fabs(seg2->leftP->x-rightP->x)+fabs(seg2->leftP->y-rightP->y))
      return *leftP;
    else
      return *seg2->leftP;
}

//return true if the order based on slopes is this followed by seg2
bool SweepLineSegment::IsLessBySlope(SweepLineSegment &seg2, bool behindSweepLine)
{
  //being vertical means having infinite slope (assumption is: x1<x2)
  if (vertical)
    if (!seg2.vertical) return (behindSweepLine ? true : false);
    else return (id.QuadPart < seg2.id.QuadPart); //not touched by behindSweepLine!!!
  else 
    if (seg2.vertical) return (behindSweepLine ? false : true);
  if (IsZero2(dydx-seg2.dydx)) 
    return (id.QuadPart < seg2.id.QuadPart); //not touched by behindSweepLine!!!
  if (dydx<seg2.dydx) return (behindSweepLine ? false : true);
  else return (behindSweepLine ? true : false);
}

bool SweepLineSegment::operator<(SweepLineSegment &seg2)
{
  //_sweepLineX should be properly set!
  float myY, seg2Y;
  if (seg2.leftP==leftP)
  {
    myY = YIntersect(_sweepLineX+0.1f);
    seg2Y = seg2.YIntersect(_sweepLineX+0.1f);
  }
  else if (seg2.rightP==rightP)
  {
    myY = YIntersect(_sweepLineX-0.1f);
    seg2Y = seg2.YIntersect(_sweepLineX-0.1f);
  }
  else 
  {
    myY = YIntersect();
    seg2Y = seg2.YIntersect();
  }
  if (myY==seg2Y)
  { //should be determined by the slope
    if (IsZero2(rightP->x-_sweepLineX) && IsZero2(seg2.rightP->x-_sweepLineX))
      return IsLessBySlope(seg2, true); //reverse order behind sweepLine
    else
      return IsLessBySlope(seg2);
  }
  return (myY < seg2Y);
}

bool SweepLineSegment::operator==(SweepLineSegment &seg2)
{
  return (id.QuadPart==seg2.id.QuadPart /* && leftP==seg2.leftP && rightP==seg2.rightP*/);
  //return &seg2==this; //this is incorrect: if not equal, but coordinates equal, bintree cannot decide where to find it (left or right son?)
}

//it should create new trapezoidal stubs if necessary
void SweepLine::Add(SweepLineSegment *seg, bool handleTrapezoids)
{
//  seg->finalColor=1;
  seg->binTreeNode=_segTree.Insert(seg);
  Assert(seg->binTreeNode!=NULL);
  const Ref<SweepLineSegment> *rsegB = _segTree.GetFirstLess(*seg);
  SweepLineSegment *segB = rsegB ? *rsegB : NULL;
  SweepLineSegment *segA;
  if (segB) {
    segA = segB->above;
    segB->above=seg;
  }
  else
  {
    const Ref<SweepLineSegment> *rsegA = _segTree.GetFirstGreater(*seg);
    segA = rsegA ? *rsegA : NULL;
  }
  seg->above=segA;
  seg->below=segB;

  if (segA) segA->below=seg;
  //create new trapezoidal stubs. if existing one yet, consider him to be the upper one
  if (handleTrapezoids)
  {
    if (segA) {
      segA->below=seg;
      seg->trapAbove = segA->trapBelow;
    }
    else if (segB) {
      seg->trapAbove = segB->trapAbove;
    }
    else {
      seg->trapAbove = new Trapezoid; //new trapezoidal stub
      seg->trapAbove->x1 = seg->leftP->x;
    }
    if (!rtTr) rtTr=seg->trapAbove; //it propagates above itself, so set it only if not set yet
    seg->trapBelow = new Trapezoid; //new trapezoidal stub
    seg->trapBelow->x1 = seg->leftP->x;
    if (!rbTr || rbTr==seg->trapAbove) rbTr=seg->trapBelow;
    if (segB) segB->trapAbove = seg->trapBelow;
    Assert(rbTr!=NULL && rtTr!=NULL);
    //some new trapezoid has been created. //update up and down edges
    if (segB)
    { //corresponding trapezoid has been separated, so its edgesDown contains incorrect edge through segB
      seg->trapBelow->edgesDown.Move(seg->trapAbove->edgesDown);
      int size = segB->trapBelow->edgesUp.Size();
      Assert(size);
      if (size) segB->trapBelow->edgesUp[size-1]=seg->trapBelow;
    }
    seg->trapAbove->edgesDown.Add(seg->trapBelow);
    seg->trapBelow->edgesUp.Add(seg->trapAbove);
  }
}

void SweepLine::AddTrapezoidsToGraph()
{
  bool leftEq = ltTr==lbTr;
  bool rightEq = rtTr==rbTr;
  if (leftEq && !rightEq)
  {
    ltTr->rightTr1=rbTr; rbTr->leftTr1=ltTr;
    ltTr->rightTr2=rtTr; rtTr->leftTr1=ltTr;
  }
  else if (!leftEq && rightEq)
  {
    rtTr->leftTr1=lbTr; lbTr->rightTr1=rtTr;
    rtTr->leftTr2=ltTr; ltTr->rightTr1=rtTr;
  }
  else
  {
    Assert (!leftEq || !rightEq);
    ltTr->rightTr1=rtTr; rtTr->leftTr1=ltTr;
    lbTr->rightTr1=rbTr; rbTr->leftTr1=lbTr;
  }
}

void SweepLine::Delete(SweepLineSegment *seg)
{
  if (seg->id.QuadPart==1311125)
    _asm nop;
  if (seg->above) seg->above->below=seg->below;
  if (seg->below) seg->below->above=seg->above;
  int count = _segTree.GetCount();
  BinTree<Ref<SweepLineSegment> >::BinTreeNode *node = (BinTree<Ref<SweepLineSegment> >::BinTreeNode *)seg->binTreeNode;
  _segTree.Delete(node);
  if (node) 
    node->data->binTreeNode = node;
  Assert(count>_segTree.GetCount());
}

SweepLineSegment *SweepLine::GetFirstLessOrEqual(SweepLineSegment *seg)
{
  const Ref<SweepLineSegment> *fnd = _segTree.GetFirstLessOrEqual(*seg);
  if (fnd) return *fnd;
  else return NULL;
}
SweepLineSegment *SweepLine::GetFirstGreaterOrEqual(SweepLineSegment *seg)
{
  const Ref<SweepLineSegment> *fnd = _segTree.GetFirstGreaterOrEqual(*seg);
  if (fnd) return *fnd;
  else return NULL;
}

void Event::AddEdgeL(SweepLineSegment *seg)
{
  bool exist = false;
  for (SweepLineSegment *segE=edgeListL.Start(); edgeListL.NotEnd(segE); segE=edgeListL.Advance(segE)) 
  {
    if (*segE==*seg)
    {
      exist = true;
      LogF("Warning: Event:AddEdgeL() this segment has been already inserted!");
      break;
    }
  }
  
  if (!exist)
    edgeListL.Add(seg);
  //TODO: can exist be true?
}
void Event::AddEdgeR(SweepLineSegment *seg)
{
  bool exist = false;
  for (SweepLineSegment *segE=edgeListR.Start(); edgeListR.NotEnd(segE); segE=edgeListR.Advance(segE)) 
  {
    if (*segE==*seg) 
    {
      exist = true;
      LogF("Warning: Event:AddEdgeR() this segment has been already inserted!");
      break;
    }
  }
  if (!exist)
    edgeListR.Add(seg);
  //TODO: can exist be true?
}

void MergeEdges(AutoArray<TrapezoidPtr > &edges1, AutoArray<TrapezoidPtr > &edges2)
{
  for (int i=0, siz=edges2.Size(); i<siz; i++)
  {
    bool found=false;
    for (int j=0, siz1=edges1.Size(); j<siz1; j++)
      if (edges1[j]==edges2[i]) {found=true; break;}
    if (!found) edges1.Add(edges2[i]);
  }
}

bool MergePossible(Trapezoid *trap)
{
  if (trap->rightTr1 && trap->rightTr2) return false; //no merging to the right possible
  Assert(trap->rightTr2==NULL);
  if (trap->rightTr1==NULL) return false;
  Trapezoid *tr2 = trap->rightTr1;
  if (PointIsOnLine(trap->x2,trap->y3, trap->x1, trap->y2, tr2->x2,tr2->y3) &&
    PointIsOnLine(trap->x2,trap->y4, trap->x1, trap->y1, tr2->x2,tr2->y4)) return true;
  return false;
}
