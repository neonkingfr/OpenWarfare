#include "trapezoid.h"

#include <GL/glut.h>
 
/*
	Interactive creation of simple polygon, using rubber band lines
	and then trapezoidal decomposition.
	This is for a simple polgyon, it will not work for polygons
	who's edges intersect other than at vertices.
	Could use overlay and double buffering with advantage.
	The program *may* have a bug - but I cannot reproduce the
	situation that seemed to give an incorrect result. It is possible
	that this incorrect result was caused by a non-simple polygon.
	I welcome input on this. MS November, 1997.
*/

#define MAXPOLYGON 100

static GLint Height;		/*height of window*/
static int N;			/*number of vertices in polygon*/
static GLint X[MAXPOLYGON], Y[MAXPOLYGON];
static GLint XC,YC;		/*current mouse position*/
static GLenum Mode;		/*used to determine end of polygon*/
static TGIndex *Traplist;	/*list of trapezoids*/


static void displayTrapezium(Trapezium *trap)
{
	glBegin(GL_LINE_LOOP);
		glVertex2i(ROUND(trap->x1),trap->y1);
		glVertex2i(ROUND(trap->x2),trap->y2);
		glVertex2i(ROUND(trap->x3),trap->y2);
		glVertex2i(ROUND(trap->x4),trap->y1);
	glEnd();
}

static void displayTrapList()
{
	register int i;
	GIndex *gind;
	Trapezium *trap;

    	glClear(GL_COLOR_BUFFER_BIT);

	gind = Traplist->first;
	while(gind){
		trap = (Trapezium *)gind->object;
		displayTrapezium(trap);
		gind = gind->next;
	}
}

static void printPolygon(char *filename)
{
	FILE *fp;
	int i;

	fp = fopen(filename,"w");
	fprintf(fp,"%d\n",N);
	for(i=0;i<N;++i) fprintf(fp,"%d %d ",X[i],Y[i]);
	fprintf(fp,"\n");
	fclose(fp);
}


static void display()
{
	int i;

    	glClear(GL_COLOR_BUFFER_BIT);

	/*return if nothing defined yet*/
	if(N==0) return;

	if(N>1){/*there is something defined already*/
		glColor3f(0.0,0.0,1.0);
		glBegin(GL_LINE_LOOP);
			for(i=0;i<N;++i) glVertex2i(X[i],Y[i]);
		glEnd();
	}


	/*do the trapezoidal decomposition*/
	Traplist = trapezoidalDecomposition(N,X,Y);

	/*now display the list of trapezoids*/
	glutDisplayFunc(displayTrapList);
}
	

static void mouseButton(int button, int state, int x, int y)
/*button is GLUT_LEFT_BUTTON, GLUT_MIDDLE_BUTTON, GLUT_RIGHT_BUTTON
  state is GLUT_UP GLUT_DOWN
*/
{
	if(button==GLUT_LEFT_BUTTON && state==GLUT_DOWN){
		X[N] = x;
		Y[N] = Height - y;
		N++;
	}

	if(button==GLUT_RIGHT_BUTTON && state==GLUT_DOWN) {
		/*change the display function*/
		glutDisplayFunc(display); 
	}
		
}

static void mouseMotion(int x, int y)
{
	XC = x; YC = Height - y;

	/*force a call to display*/
	glutPostRedisplay();
}


static void interactiveDisplay ()
{
	int i;

    	glClear(GL_COLOR_BUFFER_BIT);

	/*return if nothing defined yet*/
	if(N==0) return;

	if(N>1){/*there is something defined already*/
		glColor3f(0.0,0.0,1.0);
		glBegin(GL_LINE_STRIP);
			for(i=0;i<N;++i) glVertex2i(X[i],Y[i]);
		glEnd();
	}
	
	/*the new edge - set the current colour to red*/
	glColor3f(1.0,0.0,0.0);

	/*draw a single line*/
    	glBegin(GL_LINES);
		glVertex2i(X[N-1],Y[N-1]);
		glVertex2i(XC,YC);
	glEnd();
}

 
 
static void reshape(GLsizei width, GLsizei height)
{ 	
	/*define the viewport - width and height of display window*/
	glViewport (0, 0, width, height);

	/*save the height to turn mouse coords right way up*/
	Height = height;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0,(GLdouble)width,0.0,(GLdouble)height,-1.0,1.0);

	/*for 2D the modelview matrix is the identity*/
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

}
	
static void initialise(void) 
{

    	/*set the background (clear) Color to white*/
    	glClearColor(1.0,1.0,1.0,0.0);

	/*initialise number of vertices in polygon so far*/
	N = 0;

	/*initialise drawing mode*/
	Mode = GL_LINE_STRIP;

}

     
int main(int argc, char** argv)
{	
	int window;

	glutInit(&argc,argv);

	glutInitWindowSize(500,500);

	glutInitDisplayMode(GLUT_RGBA);

	window = glutCreateWindow("Line");
	glutSetWindow(window);

    	initialise();

	/*register callbacks*/
	glutDisplayFunc(interactiveDisplay); /*display function*/
	glutReshapeFunc(reshape);
	glutMouseFunc(mouseButton);
	glutPassiveMotionFunc(mouseMotion);

	glutMainLoop();
}
