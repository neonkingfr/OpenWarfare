#include "trapezoid.h"

#define MAXSCANLINE 1000

/*the Edge Table*/
static TGIndexPtr ET[MAXSCANLINE];

/*x-coordinate of intersection with line x1,y1 to x2,y2 and y*/
#define XINTERSECT(dxdy,x1,y1,y) ((dxdy)*((y)-(y1))+(x1))

/*sorted list of scan lines where there is a change*/
static Index *YScan;

static void initialiseET(void)
{
	int i;

	for(i=0;i<MAXSCANLINE;++i) ET[i] = newTGIndex();
}

static void printEdge(FILE *fp, void *ptr)
/*for debugging*/
{
	Edge *edge = (Edge *)ptr;

	fprintf(fp,"Edge:[(%f %d) (%f %d)]\n",edge->x1,edge->y1,edge->x2,edge->y2);
}


static void printTrap(FILE *fp, void *ptr)
/*for debugging*/
{
	Trapezium *trap = (Trapezium *)ptr;
	fprintf(fp,"Trap: [(%f,%d),(%f,%d),(%f,%d),(%f,%d)]\n",
		trap->x1,trap->y1,trap->x2,trap->y2,trap->x3,trap->y2, trap->x4,trap->y1);
}


static void printET(FILE *fp)
/*for debugging*/
{
	Index *ind;
	int i;
	TGIndex *etentry;

	ind = YScan;

	while(ind){
		etentry = ET[i=valueAtIndex(ind)];
		fprintf(fp,"scanline = %d\n",i);
		fprintTGIndex(fp,etentry,printEdge);
		ind = nextIndex(ind);
	}
}
		

static short swapEdgesBySlope(Edge *edge0, Edge *edge1)
/*return 1 if the order based on slopes is edge0 followed by edge1*/
{
	double x0,x1,invslope0, invslope1;
	
	invslope0 = edge0->dxdy;
	invslope1 = edge1->dxdy;

	if(invslope0 >= 0.0 && invslope1 >= 0.0){
		if(invslope0 < invslope1) return 1;
		else return 0;
	}

	if(invslope0 <= 0.0 && invslope1 >= 0.0) return 1;

	if(invslope0 >= 0.0 && invslope1 <= 0.0) return 0;

	if(invslope0 <= 0.0 && invslope1 <= 0.0){
		if(ABS(invslope0) > ABS(invslope1)) return 1;
		else return 0;
	}
}



static void insertETEntry(TGIndex *etentry, Edge *edge)
/*inserts the edge into the ET entry, maitaining a sort as follows:
should be in increasing x1-order. Where the x's are the same then
consider the slopes. If both positive, then greater slope goes first.
If one negative and one positive, then negative goes first.
If both negative, then the one with smallest abs value goes first.
Ie,  the edge which makes greatest overall positive angle with
positive x-axis goes first*/
{
	GIndex *etind, *etprev, *gind;
	double x1, slope;
	Edge *etedge;

	if(etentry->n ==0) {
		appendTGIndex(etentry,(AnyPtr)edge);
		return;
	}

	x1 = edge->x1;
	slope = edge->dxdy;

	etprev = NOGINDEX;
	etind = etentry->first;
	gind = newGIndex((AnyPtr)edge);

	while(etind){
		etedge = (Edge *)etind->object;
		if(etedge->x1 == x1) {
			if(swapEdgesBySlope(edge,etedge)) break;
			else{/*order is etedge followed by edge*/
				gind->next = etind->next;
				etind->next = gind;
				return;
			}
		}
		else if(etedge->x1 > x1) break;
		etprev = etind;
		etind = etind->next;
	}
	

	/*means that edge is prior to etedge*/
	if(etprev==NOGINDEX){
		gind->next = etind;
		etentry->first=gind;
	}
	else{
		etprev->next = gind;
		gind->next = etind;
	}
}
			
static void insertAET(TGIndex *aet, Edge *edge)
/*inserts by x1 value the edge into the aet*/	
{
	GIndex *gind, *gprev, *newg;
	Edge *aetedge;
	
	gprev = NOGINDEX;
	gind = aet->first;
	while(gind){
		aetedge = (Edge *)gind->object;
		if(aetedge->x1 > edge->x1) break;
		gprev = gind;
		gind = gind->next;
	}

	newg = newGIndex(edge);
	if(gprev) {
		gprev->next = newg;
		newg->next = gind;
	}
	else{
		newg->next = gind;
		aet->first = newg;
	}
}
		
		

static void makeEdgeTable(int n, int x[], int y[])
/*traverses each edge (x[i],y[i]) to (x[i+1],y[i+1])
i=0,...,n-1, and makes the appropriate edge table entry*/
{
	int i;
	int y1,y2,ymin;
	double x1,x2;
	Edge *edge;

	/*initialise the ET*/
	initialiseET();

	/*go through edges*/
	x1 = x[n-1]; y1 = y[n-1];

	/*initialise the list of scan lines*/
	YScan = NOINDEX;

	for(i=0; i<n; ++i){
		x2 = x[i]; y2 = y[i];

		insertSortIndex(&YScan,y2);

		if(y1 != y2) {/*must not be horizontal*/
			edge = (Edge *)malloc(sizeof(Edge));
			if(y1 < y2){
				ymin = y1;
				edge->y1 = y1;
				edge->y2 = y2;
				edge->x1 = x1;
				edge->x2 = x2;
			}
			else{/*y1 > y2*/
				ymin = y2;
				edge->y1 = y2;
				edge->y2 = y1;
				edge->x1 = x2;
				edge->x2 = x1;
			}
			edge->dxdy = (x2-x1)/(y2-y1);
			insertETEntry(ET[ymin],edge);
		}
		x1 = x2; y1 = y2;
	}
}		
		

static void outputTrapRight(TGIndex *traplist,int y, Edge *edge0, Edge *edge1)
/*trapezium with top edge at y, corresponding to top of edge0, so intersects edge1*/
{
	Trapezium *trap;

	trap = (Trapezium *)malloc(sizeof(Trapezium));
	trap->x1 = edge0->x1;
	trap->y1 = edge0->y1;

	trap->x2 = edge0->x2;
	trap->y2 = y;

	trap->x3 = XINTERSECT(edge1->dxdy,edge1->x1,edge1->y1,y);
	
	trap->x4 = edge1->x1;

	/*update*/
	edge1->x1 = trap->x3;
	edge1->y1 = y;

	/*now join this trapezium to the list*/
	appendTGIndex(traplist,(AnyPtr)trap);
}



static void outputTrapLeft(TGIndex *traplist, int y, Edge *edge0, Edge *edge1)
/*trapezium with top edge at y, corresponding to top of edge1, so intersects edge0*/
{
	Trapezium *trap;

	trap = (Trapezium *)malloc(sizeof(Trapezium));
	trap->x1 = edge0->x1;
	trap->y1 = edge0->y1;

	trap->x2 = XINTERSECT(edge0->dxdy,edge0->x1,edge0->y1,y);
	trap->y2 = y;

	trap->x3 = edge1->x2;
	
	trap->x4 = edge1->x1;

	/*update the first edge*/
	edge0->x1 = trap->x2;
	edge0->y1 = y;

	/*now join this trapezium to the list*/
	appendTGIndex(traplist,(AnyPtr)trap);
}

static void outputTrapTop(TGIndex *traplist, Edge *edge0, Edge *edge1)
/*computes trapezium with upper edge at y and both left and right edges have height y*/
{
	Trapezium *trap;

	trap = (Trapezium *)malloc(sizeof(Trapezium));
	trap->x1 = edge0->x1;
	trap->y1 = edge0->y1;

	trap->x2 = edge0->x2;
	trap->y2 = edge0->y2;

	trap->x3 = edge1->x2;
	
	trap->x4 = edge1->x1;

	/*update*/
	/*since these are leaving the aet no need to update*/

	/*now join this trapezium to the list*/
	appendTGIndex(traplist,(AnyPtr)trap);
}

static void outputTrap(TGIndex *traplist,int y, Edge* edge0, Edge *edge1, double left, double right)
/*computes trapezium with upper edge at y and both left and right edges are intersected*/
{
	Trapezium *trap;

	trap = (Trapezium *)malloc(sizeof(Trapezium));
	trap->x1 = edge0->x1;
	trap->y1 = edge0->y1;

	trap->x2 = left;
	trap->y2 = y;

	trap->x3 = right;
	
	trap->x4 = edge1->x1;

	/*update both edges*/
	edge0->x1 = left;
	edge0->y1 = y;

	edge1->x1 = right;
	edge1->y1 = y;

	/*now join this trapezium to the list*/
	appendTGIndex(traplist,(AnyPtr)trap);
}

static void updateAET(TGIndex *aet, int y, GIndex *etind)
/*updates the aet and brings in the et entry at y*/
{
	GIndex *gind, *gprev, *gtemp;
	Edge *edge, *newedge;

	gprev = NOGINDEX;
	gind = aet->first;

	while(gind){
		edge = (Edge *)gind->object;
		if(edge->y2 == y){/*delete*/
			if(gprev) gprev->next = gind->next;
			else aet->first = gind->next;
			gtemp = gind;
			gind = gind->next;
			/*free gtemp*/
		}
		else{
			gprev = gind;
			gind = gind->next;
		}
	}

	/*now bring in the et entry if any*/
	gtemp = etind;
	while(gtemp){
		newedge = (Edge *)gtemp->object;
		insertAET(aet,newedge);
		gtemp = gtemp->next;
		
	}
}

		
static void processET(TGIndex *traplist)
/*processes the edge table*/
{
	TGIndex *aet, *et;
	Index *ind;
	int y,i,n;
	GIndex *gprev, *gright, *gleft, *etind, *gind;
	Edge *edge, *edge0, *edge1, *newedge;
	double x, left, right;

	/*initialise the active edge table*/
	ind = YScan;
	aet = ET[valueAtIndex(ind)];


	ind = nextIndex(ind);
	/*traverse through all the vertex-events*/
	while(ind){
		y = valueAtIndex(ind); /*the current scan line*/

		/*check if y is the top of any edge already in aet*/
		gleft = aet->first;
		while(gleft){
			gright = gleft->next;
			edge0 = (Edge *)gleft->object;
			edge1 = (Edge *)gright->object;
			if(edge0->y2==y && edge1->y2==y){
				outputTrapTop(traplist,edge0,edge1);
			}
			else
			if(edge0->y2==y){
				outputTrapRight(traplist,y,edge0,edge1);
			}
			else
			if(edge1->y2==y){
				outputTrapLeft(traplist,y,edge0,edge1);
			}
			gleft = gright->next;
		}

		/*if there is a pair in aet such that y1<y and y2>y then find if any
		  elements in ET[y] are inside this pair.
            	  If so then output the trap. Finally insert sort
	          all elements of ET[y] into aet*/

		et = ET[y];
		etind = et->first;
		if(etind){  
			gleft = aet->first;
			while(gleft){
				/*the two edges in aet*/
				gright = gleft->next;
				edge0 = (Edge *)gleft->object;
				edge1 = (Edge *)gright->object;
				if(edge0->y1 < y && edge1->y1 < y && 
				   edge0->y2 > y && edge1->y2 > y){
					left = XINTERSECT(edge0->dxdy,edge0->x1,edge0->y1,y); 
					right = XINTERSECT(edge1->dxdy,edge1->x1,edge1->y1,y);

					/*the list of those coming in at y*/
					etind = et->first;
					while(etind){
						newedge = (Edge *)(etind->object);
						x = newedge->x1;
						if(x > left && x < right) {
							outputTrap(traplist,y,edge0,edge1,left,right);
							break;
						}
						etind = etind->next;
					}
				}
				gleft = gright->next;
			}
		}
		/*now all traps have been output. It remains to update the AET
		and to bring ET[y] into it*/
		etind = et->first;
		updateAET(aet,y,etind);
		
		ind = nextIndex(ind);
	}
}


TGIndex *trapezoidalDecomposition(int n, int x[], int y[])
/*computes a trapezoidal decomposition for the simple polygon*/
{
	TGIndex *traplist;

	/*make a new empty list of trapezia*/
	traplist = newTGIndex();

	/*make the edge table - stored as global ET*/
	makeEdgeTable(n,x,y);

	/*process the edge table*/
	processET(traplist);

	return traplist;
}				
				
						
					
			


		


