#define _UTILITIES

#include <stdlib.h>

#define PI (3.14159265358979323846)

/*macro for absolute value*/
#define ABS(x) ( ((x) >= (double)0.0) ? (x) : -(x))

/*macro for tolerance*/
#define TOL   (1.0e-6)

/*macro for squaring*/
#define SQR(x)   ((x)*(x))

/*round*/
#define ROUND(x) ((int)((x)+0.5))

#define Malloc(Object,N) 	((Object *)myMalloc((N)*sizeof(Object)))

#define FREE(Object)		(free((Ptr)(Object)))

#define BIG (1.0e20)

typedef void *Ptr;

typedef void *AnyPtr;

char *myMalloc(int n);

typedef enum{
	false,true
} Bool;


