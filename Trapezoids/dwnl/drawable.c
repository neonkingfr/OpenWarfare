#include "device.h"

/**********************************************************/

#include <X11/Xatom.h>
#include <X11/Xmu/StdCmap.h>

/*implementation for Xlib*/

#define ROUND(x)        ((int)((x)+0.5))
#define MAXPOLYGON      20

static XPoint thePolygon[MAXPOLYGON];
static int  polySize;
GC   TheGC;
Display *TheDisplay;
int TheScreen;
int TheDepth;
static Window TheWindow;
static Drawable rootWindow, currentWindow;
static Pixmap ThePixmap;
static XStandardColormap *TheColourMap;
static unsigned long whitePixel,blackPixel, pixel;
static XFontStruct *currentFont;
static unsigned int Width, Height;

/********************************************************************/
/*         These next are private functions, not to be called by the user program	   */
/********************************************************************/

static void Private_Initialise()
{
	int count;
	TheDisplay = XOpenDisplay(NULL);
	TheScreen = DefaultScreen(TheDisplay);
	rootWindow = DefaultRootWindow(TheDisplay);
	pixel = blackPixel = BlackPixel(TheDisplay,TheScreen);
	whitePixel = WhitePixel(TheDisplay,TheScreen);
	TheGC = DefaultGC(TheDisplay,TheScreen);
	TheDepth = DefaultDepth(TheDisplay,TheScreen);
/*
	XmuAllStandardColormaps(TheDisplay);
	if(
	    XGetRGBColormaps(TheDisplay,
				  rootWindow,
				  &TheColourMap,
				  &count,
				  XA_RGB_BEST_MAP)
	==0)
	printf("couldn't allocate standard colourmap\n");
*/
}
/********************************************************************/

static Window Private_CreateWin(int posx,int posy,int width,int height)
{
	Window newWindow;
	XTextProperty win_name,icon_name;
	XSizeHints hints;
	XEvent event;
	XSetWindowAttributes attr;
	int border = 10;
	char *win_title = "3D";
	char *icon_title = "3D";

	/*create the new window*/
	newWindow = XCreateSimpleWindow(TheDisplay,rootWindow,
					posx,posy,width,height,border,
					blackPixel,whitePixel);

	/*convert the names each to a TextProperty*/
	XStringListToTextProperty(&win_title,1,&win_name);
	XStringListToTextProperty(&icon_title,1,&icon_name);

	/*set the name of the window and icon*/
	XSetWMName(TheDisplay,newWindow,&win_name);
	XSetWMIconName(TheDisplay,newWindow,&icon_name);

	hints.flags = PSize;
	hints.base_width = width;
	hints.base_height = height;
	XSetWMNormalHints(TheDisplay,newWindow,&hints);

	/*make sure that this window accepts exposure events*/
	XSelectInput(TheDisplay,newWindow,ExposureMask|ButtonPressMask|KeyPressMask);

	/*map the window and wait for an expose event*/
	XMapWindow(TheDisplay,newWindow);
	while(True){
		XNextEvent(TheDisplay,&event);
		if((event.type==Expose)&&(event.xexpose.window==newWindow))
			break;
	}


	/*set the window attributes - to always use backing store*/
	attr.backing_store = Always;
	/*change the colourmap*/
	/*
	attr.colormap = TheColourMap->colormap;
	*/
	/*means that there will be an attempt to repaint window contents
	  on Expose events, but there is no guarantee*/
	XChangeWindowAttributes(TheDisplay,newWindow,CWBackingStore/*|CWColormap*/,&attr);
	/*Note that if the window is resized then the contents may not
	  be restored. In any case there will be an Expose event generated
	  and the client should be prepared to handle this*/

	return(newWindow);
}

/********************************************************************/
/*the following functions must be defined, and are the user callable functions */
/********************************************************************/

void initialiseDrawables()
/*do whatever is necessary to start up*/
{
	Private_Initialise();
}

DRAWABLE newDrawable(int posx,int posy,int width,int height)
{
	Width = width; Height = height;
	currentWindow = Private_CreateWin(posx,SCREEN_MAX-posy-height,width,height);
	return(currentWindow);
}

DRAWABLE newBufferedDrawable(int posx, int posy, int width, int height)
/*creates a window but also an off-screen buffer. All subsequent drawing
goes into this offscreen buffer, which is a Pixmap in X11*/
{
	Width = (unsigned int) width; 
	Height = (unsigned int) height;
	TheWindow = Private_CreateWin(posx,SCREEN_MAX-posy-height,width,height);

	currentWindow = 
	ThePixmap = XCreatePixmap(TheDisplay,TheWindow,
				Width, Height, (unsigned int)TheDepth);
	return ThePixmap;
}
	
void copyBufferToDrawable()
/*copies the offscreen buffer to the drawable and clears the buffer to white*/
{
	XCopyArea(TheDisplay,ThePixmap,TheWindow,TheGC,0,0,Width,Height,0,0);
	XSetForeground(TheDisplay,TheGC,whitePixel);
	XFillRectangle(TheDisplay,ThePixmap,TheGC,0,0,Width,Height);
}

void setDrawable(DRAWABLE  drawable)
{
	currentWindow = drawable;
}

void newPolygonOnDrawable()
/*do whatever is necessary to start up a new polygon*/
{
	polySize = 0;
}

void startPolygonOnDrawable(int x,int y)
{
	thePolygon[0].x = x;
	thePolygon[0].y = y;
	++polySize;
}

void addToPolygonOnDrawable(int x,int y)
{
	thePolygon[polySize].x = x;
	thePolygon[polySize].y = y;
	++polySize;
}

void lineOnDrawable(int x1, int y1, int x2, int y2)
{
	XDrawLine(TheDisplay,currentWindow,TheGC,x1,y1,x2,y2);
}

#define ADJUST_LT(c)    {if((c)<0.0) (c) = 0.0;}
#define ADJUST_GT(c)    {if((c)>1.0) (c) = 1.0;}
#define ADJUST(c)       {ADJUST_LT((c)); ADJUST_GT((c));}

static adjustColour(red,green,blue)
REAL *red,*green,*blue;
{
	ADJUST(*red);
	ADJUST(*green);
	ADJUST(*blue);
}

void setColourOnDrawable(red,green,blue)
REAL red,green,blue;
{
	XColor colour;

	adjustColour(&red,&green,&blue);

	colour.red = ROUND(65535.0*red);
	colour.green = ROUND(65535.0*green);
	colour.blue = ROUND(65535.0*blue);
	colour.flags = DoRed|DoGreen|DoBlue;
	XAllocColor(TheDisplay,DefaultColormap(TheDisplay,TheScreen),&colour);

	XSetForeground(TheDisplay,TheGC,colour.pixel);
}

/*
#define PIXEL(C,r,g,b) (C->base_pixel + \
		((unsigned long)(0.5+(r*C->red_max))*C->red_mult)+\
		((unsigned long)(0.5+(g*C->green_max))*C->green_mult)+\
		((unsigned long)(0.5+(b*C->blue_max))*C->blue_mult))

void setColourOnDrawable(double red,double green,double blue)
{
	adjustColour(&red,&green,&blue);

	pixel = PIXEL(TheColourMap,red,green,blue);
	XSetForeground(TheDisplay,TheGC,pixel);
}
*/

void displayPolygonOnDrawable()
{
	XFillPolygon(TheDisplay,currentWindow,TheGC,
		     thePolygon,polySize,Nonconvex,CoordModeOrigin);
}

void displayWhitePolygonOnDrawable()
/*draws a white polygon, with black border, for use on monochrome display*/
{
	/*close the polygon*/
	thePolygon[polySize].x = thePolygon[0].x;
	thePolygon[polySize].y = thePolygon[0].y;
	polySize++;

	/*set drawing colour to white and draw filled polygon*/
	XSetForeground(TheDisplay,TheGC,whitePixel);
	XFillPolygon(TheDisplay,currentWindow,TheGC,
		     thePolygon,polySize,Convex,CoordModeOrigin);

	/*set drawing colour to black and draw polygon outline*/
	XSetForeground(TheDisplay,TheGC,blackPixel);
	XDrawLines(TheDisplay,currentWindow,TheGC,
		     thePolygon,polySize,CoordModeOrigin);

	/*return colour to whatever it was before*/
	XSetForeground(TheDisplay,TheGC,pixel);

}

void clearDrawable()
{
	XClearWindow(TheDisplay,currentWindow);
}


void flushDrawable()
{
	XSync(TheDisplay,False);
}

void delayOnDrawable(DRAWABLE d)
/*note the d is not actually used, but is there for upward compatibility purposes*/
{
	XEvent event;
	while(1){
		XNextEvent(TheDisplay,&event);
		if(event.type == ButtonPress) break;
	}
}

void getGeometryOfDrawable(DRAWABLE drawable,
				int *posx, int *posy, int *width, int *height)
{
	Window root;
	int x_ret,y_ret;
	unsigned int w,h,border,TheDepth;

	XGetGeometry(TheDisplay,drawable,&root,
			&x_ret,&y_ret,&w,&h,
			&border,&TheDepth);

	*posx = x_ret;
	*posy = y_ret;
	*width = w;
	*height = h;
}

short isColourDrawable()
{
	return (TheDepth > 1);
}
