#include "index.h"

#ifndef _UTILITIES
#include "utilities.h"
#endif

Index *newIndex(int i)
/*create a new linked list with first member i*/
{
	Index *ind;
	
	ind = Malloc(Index,1);
	ind->i = i;
	ind->next = NOINDEX;
	
	return(ind);
}


void freeIndex(IndexPtr *index)
/*frees the list*/
{
	Index *ind, *next;
	
	ind = *index;
	
	while(ind){
		next = nextIndex(ind);
		FREE(ind);
		ind = next;
	}
	*index = NOINDEX;
}

Index *appendIndex(Index *index, int i)
/*appends i to the end of the list, returning the Index corresponding to the new 
element*/
{
	Index *newI;
	
	newI = newIndex(i);
	index->next = newI;
	
	return(newI);
}

int valueAtIndex(Index *index)
/*returns the integer member of this index (could be done as a macro)*/
{
	return(index->i);
}

Index *nextIndex(Index *index)
/*returns the next index in the list*/
{
	return(index->next);
}

Index *consIndex(Index *index, int i)
/*returns a new Index with i at the head*/
{
	Index *ind;
	
	ind = newIndex(i);
	ind->next = index;
	return ind;
}


short memberOfIndex(Index *index, int i)
/*returns 1 if i is in index, else 0*/
{
	Index *ind;
	
	if(index == NOINDEX) return 0;
	
	ind = index;
	while(ind){
		if(valueAtIndex(ind)==i) return 1;
		ind = nextIndex(ind);
	}
	return 0;
}	

void insertIndex(IndexPtr *index, int i)
/*adds i to index provided it is not already a member*/
{
	Index *ind,*prev;
	
	if(*index == NOINDEX){
		*index = newIndex(i);
		return;
	}
	
	ind = *index;
	while(ind){
		if(valueAtIndex(ind)==i) return;
		prev = ind;
		ind = nextIndex(ind);
	}
	prev->next = newIndex(i);
	return;
}

void insertSortIndex(IndexPtr *index, int i)
/*adds i to index in ascending order, provided it is not a member*/
{
	Index *ind, *prev, *newind;
	int j;
	
	if(*index==NOINDEX){
		*index = newIndex(i);
		return;
	}

	ind = *index;
	prev = NOINDEX;
	while(ind){
		if((j=valueAtIndex(ind))==i) return;
		if(j>i) break;
		prev = ind;
		ind = nextIndex(ind);
	}

	newind = newIndex(i);
	if(prev){
		prev->next = newind;
		newind->next = ind;
	}
	else{
		newind->next = *index;
		*index = newind;
	}
	return;
}


	
void deleteIndex(IndexPtr *index,int i)
/*deletes i from the list *index*/
{
	Index *ind, *prev;
	
	if(*index == NOINDEX) return;
	
	if(valueAtIndex(*index)==i) {
		ind = *index;
		*index = nextIndex(*index);
		FREE(ind);
		return;
	}
	
	prev = *index;
	ind = nextIndex(*index);
	while(ind){
		if(valueAtIndex(ind) == i){
			prev->next = ind->next;
			FREE(ind);
			return;
		}
		prev = ind;
		ind = nextIndex(ind);
	}
}

Index *stepDeleteIndex(IndexPtr *index, int n)
/*finds and returns the Index n steps along from start, 
and deletes this entry from the list, but does not free it.
If n==0 then it returns (and deletes) the first element.
It returns NOINDEX if the list is empty or if n runs
off the end of the list.*/
{
	Index *ind, *prev, *result;
	int i;
	
	if(*index == NOINDEX) return NOINDEX;
	
	if(n == 0) {
		ind = *index;
		*index = nextIndex(*index);
		return ind;
	}
	
	prev = *index;
	ind = nextIndex(*index);
	i=1;
	while(ind){
		if(i==n){
			prev->next = ind->next;
			return ind;
		}
		prev = ind;
		++i;
		ind = nextIndex(ind);
	}
	return NOINDEX;
}		
	
TIndex *newTIndex(void)
/*returns a new, empty TIndex*/
{
	TIndex *tind;
	
	tind = Malloc(TIndex,1);
	tind->first = tind->last = NOINDEX;
	tind->n = 0;
	return tind;
}

void appendTIndex(TIndex *tindex, int i)
/*appends i to end of list referenced by tindex*/
{
	if(tindex->first)
		tindex->last = appendIndex(tindex->last,i);
	else
		tindex->first = tindex->last = newIndex(i);
	tindex->n++;
}

int popTIndex(TIndex *tindex)
/*pops the head of the list*/
{
	Index *ind;
	int i;
	
	ind = tindex->first;
	
	if(tindex->first){
		i = valueAtIndex(ind);
		tindex->first = nextIndex(tindex->first);
		tindex->n--;
		FREE(ind);
		return i;
	}
	else{
		fprintf(stderr,"attempting to pop an empty list from popTIndex\n");
		exit(1);
	}
}
		
short intersectsIndex(Index *inda, Index *indb)
/*returns 1 if intersection inda and indb is not empty*/
{
	Index *ind;
	
	if((inda==NOINDEX) || (indb==NOINDEX)) return 0;
	
	ind = inda;
	while(ind){
		if(memberOfIndex(indb,valueAtIndex(ind))) return 1;
		ind = nextIndex(ind);
	}
	return 0;
}

void fprintIndex(FILE *fp, Index *index)
{
	Index *ind;
	
	ind = index;
	fprintf(fp,"[");
	while(ind){
		fprintf(fp," %d ",valueAtIndex(ind));
		ind = nextIndex(ind);
	}
	fprintf(fp,"]");
}

void fprintTIndex(FILE *fp, TIndex *tindex)
{
	Index *ind;
	
	ind = tindex->first;
	fprintf(fp,"[");
	while(ind){
		fprintf(fp," %d ",valueAtIndex(ind));
		ind = nextIndex(ind);
	}
	fprintf(fp,"]");
}

void addIndexToTIndex(TIndex *tindex, Index *index)
/*adds everything in index to the tindex*/
{
	Index *ind;
	
	ind = index;
	while(ind){
		appendTIndex(tindex,valueAtIndex(ind));
		ind = nextIndex(ind);
	}
}

#ifdef RANDOM
TIndex *randomisedTIndex(int n)
/*builds a permutation of 0,1,...,n-1*/
{
	TIndex *tind;
	int i,m;
	Index *ind;
	Bool there;

	tind = newTIndex();

	appendTIndex(tind,discreteUniform(0,n-1));

	i = 1;
	while(i < n){
		m = discreteUniform(0,n-1);
		ind = tind->first;
		there = false;
		while(ind){
			if(m != valueAtIndex(ind)) ind = nextIndex(ind);
			else {
				there = true;
				break;
			}
		}
		if(!there) {
			appendTIndex(tind,m);
			++i;
		}
	}
	return tind;
}
#endif

GIndex *newGIndex(AnyPtr object)
{
	GIndex *gind;
	
	gind = Malloc(GIndex,1);
	gind->object = object;
	gind->next = NOGINDEX;
	
	return gind;
}

GIndex *nextGIndex(GIndex *gind)
{
	return gind->next;
}

AnyPtr objectAtGIndex(GIndex *gind)
{
	return gind->object;
}

GIndex *appendGIndex(GIndex *gindex, AnyPtr object)
/*appends object to the end of the list, returning the Index corresponding to the new 
element*/
{
	GIndex *gnew;
	
	gnew = newGIndex(object);
	gindex->next = gnew;
	
	return(gnew);
}


TGIndex *newTGIndex(void)
/*returns a new, empty TGIndex*/
{
	TGIndex *tgind;
	
	tgind = Malloc(TGIndex,1);
	tgind->first = tgind->last = NOGINDEX;
	tgind->n = 0;
	return tgind;
}

void appendTGIndex(TGIndex *tgindex, AnyPtr object)
/*appends object to end of list referenced by tgindex*/
{
	if(tgindex->first)
		tgindex->last = appendGIndex(tgindex->last,object);
	else
		tgindex->first = tgindex->last = newGIndex(object);
	tgindex->n++;
}

AnyPtr popTGIndex(TGIndex *tgindex)
/*pops the head of the list*/
{
	GIndex *gind;
	AnyPtr i;
	
	gind = tgindex->first;
	
	if(tgindex->first){
		i = objectAtGIndex(gind);
		tgindex->first = nextGIndex(tgindex->first);
		tgindex->n--;
		FREE(gind);
		return i;
	}
	else{
		fprintf(stderr,"attempting to pop an empty list from popTGIndex\n");
		exit(1);
	}
}


void fprintTGIndex(FILE *fp, TGIndex *tgindex, void (*printObject)(FILE *fp, void *p))
{
	GIndex *gind;
	
	gind = tgindex->first;
	while(gind){
		(*printObject)(fp,objectAtGIndex(gind));
		fprintf(fp,"\n");
		gind = nextGIndex(gind);
	}
	fprintf(fp,"\n");
}

void addGIndexToTGIndex(TGIndex *tgindex, GIndex *gindex)
/*adds all elements of index to the tail index*/
{
	GIndex *gind;
	
	gind = gindex;
	while(gind){
		appendTGIndex(tgindex,objectAtGIndex(gind));
		gind = nextGIndex(gind);
	}
}
