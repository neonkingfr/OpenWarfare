#define _TRAPEZOID

#include "index.h"


/*defines an edge, with lower x at x1, upper y at y2, and dx/dy*/
typedef struct{
	int y1, y2;
	double x1, x2, dxdy;
} Edge;


/*
  defines a trapezium with vertices
	(x1,y1), (x2,y2), (x3,y2), (x4,y1)
  assumed in clockwise order, with y1<y2
*/
typedef struct{
	int y1,y2;
	double x1,x2,x3,x4;
} Trapezium;


TGIndex *trapezoidalDecomposition(int n, int x[], int y[]);
/*computes a trapezoidal decomposition for the simple polygon*/
