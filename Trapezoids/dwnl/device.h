/*implementation for Xlib*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>

/*height of the screen*/
#define SCREEN_MAX  (DisplayHeight(TheDisplay,TheScreen))
#define DRAWABLE    Drawable

#define REAL  double

/*function prototypes*/

#ifdef __cplusplus
extern "C"{
#endif

void initialiseDrawables();
/*must be the first function called - initialises lots of things*/

DRAWABLE newDrawable(int posx,int posy,int width,int height);
/*creates a window starting at (posx,posy) with given size. 
the X Window manager is likely to ignore posx, posy*/

void setDrawable(DRAWABLE  drawable);
/*sets this to be the current drawing area. 
Subsequent drawing will go to this window*/

void setColourOnDrawable(REAL red, REAL green, REAL blue);
/*
sets the current colour to be the specified RGB mix.
red, green and blue should all be in the range 0.0 to 1.0.
(0.0,0.0,0.0) corresponds to black.
(1.0,1.0,1.0) corresponds to white.
Other colours can be mixed appropriately
*/


/*
these next 3 functions allow the specification of a polygon -
done in this way to preserve independence from rest of program
and to make porting easy
*/
void newPolygonOnDrawable();
/*does whatever is necessary to start up a new polygon*/

void startPolygonOnDrawable(int x,int y);
/*start the polygon with (x,y) as the first vertex*/

void addToPolygonOnDrawable(int x,int y);
/*add in another vertex (x,y)*/

void displayPolygonOnDrawable();
/*display the currently defined polygon*/

void displayWhitePolygonOnDrawable();
/*same, but draws white interior and black border, 
suitable for monochrome machines*/

void lineOnDrawable(int x1, int y1, int x2, int y2);
/*draws a line from (x1,y1) to (x2,y2) */

void clearDrawable();
/*clears the current drawing area to background colour*/

void flushDrawable();
/*flushes the Xlib buffer*/

void delayOnDrawable();
/*suspends everything until a button press*/

void getGeometryOfDrawable(DRAWABLE drawable,
				int *posx, int *posy, int *width, int *height);
/*gets information about the current drawing area, position, width and height*/

short isColourDrawable();
/*returns 1 if colour display else 0*/

DRAWABLE newBufferedDrawable(int posx, int posy, int width, int height);
/*creates a window but also an off-screen buffer. All subsequent drawing
goes into this offscreen buffer, which is a Pixmap in X11*/

void copyBufferToDrawable();
/*copies the offscreen buffer to the drawable and clears the buffer to white*/

#ifdef __cplusplus
}
#endif


