#include "trapezoid.h"
#include "device.h"

static int Height;

static void displayTrapezium(Trapezium *trap)
{
	int y, x1,y1,x2,y2,x3,x4;
	double dy, dxdy1, dxdy2, xleft, xright;

	x1 = ROUND(trap->x1);
	y1 = trap->y1;
	x2 = ROUND(trap->x2);
	y2 = trap->y2;
	x3 = ROUND(trap->x3);
	x4 = ROUND(trap->x4);

	dy = y2-y1;
	dxdy1 = (trap->x2-trap->x1)/dy;
	dxdy2 = (trap->x3-trap->x4)/dy;
	
	setColourOnDrawable(1.0,1.0,0.0);
	xleft = trap->x1; xright = trap->x4;
	for(y=y1; y<=y2; ++y){ 
		lineOnDrawable(ROUND(xleft),Height-y,ROUND(xright),Height-y);
		xleft += dxdy1;
		xright += dxdy2;
	}	

	
	setColourOnDrawable(0.0,0.0,1.0);
	lineOnDrawable(x1,Height-y1,x2,Height-y2);
	lineOnDrawable(x2,Height-y2,x3,Height-y2);
	lineOnDrawable(x3,Height-y2,x4,Height-y1);
	lineOnDrawable(x4,Height-y1,x1,Height-y1);

	
}


static void displayTrapList(TGIndex *traplist)
{
	register int i;
	GIndex *gind;
	Trapezium *trap;

	gind = traplist->first;
	while(gind){
		trap = (Trapezium *)gind->object;
		displayTrapezium(trap);
		gind = gind->next;
	}
	flushDrawable();
}


void readPolyFromFile(char *filename, int *n, int x[], int y[])
{
	FILE *fp;
	int i;

	fp = fopen(filename,"r");
	fscanf(fp,"%d",n);
	for(i=0;i<*n;++i) fscanf(fp,"%d %d",&x[i],&y[i]);
	fclose(fp);
}


#define MAXPOLY 100

int main(int argc, char **argv)
{
	Drawable d;
	int i;
	int n;
	int x[MAXPOLY];
	int y[MAXPOLY];
	TGIndex *traplist;

	readPolyFromFile("poly.dat",&n,x,y);

	traplist = trapezoidalDecomposition(n,x,y);

	/*output the trapezoid using X11*/

	initialiseDrawables();
	Height = 500;
	d = newDrawable(0,0,500,500);

	displayTrapList(traplist);

	/*wait for a button press*/
	delayOnDrawable(d);

}
