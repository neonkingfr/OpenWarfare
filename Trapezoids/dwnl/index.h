#define _INDEX

#ifndef _UTILITIES
#include "utilities.h"
#endif

#define NOINDEX ((Index *)0)
#define NOTINDEX ((TIndex *)0)
#define NOGINDEX ((GIndex *)0)

#include <stdio.h>

typedef struct _index{
	int i;
	struct _index *next;
} Index, *IndexPtr;

typedef struct{
	Index *first;
	Index *last;
	int n;
} TIndex, *TIndexPtr; /*for tail lists*/


/*list of generic pointer objects*/
typedef struct _gindex{
	AnyPtr object;
	struct _gindex *next;
} GIndex, *GIndexPtr;

typedef struct{
	GIndex *first;
	GIndex *last;
	int n;
} TGIndex, *TGIndexPtr; /*for tail generic lists*/


Index *newIndex(int i);
/*create a new linked list with first member i*/

void freeIndex(IndexPtr *index);
/*frees the list*/

Index *appendIndex(Index *index, int i);
/*appends i to the end of the list, returning the Index corresponding to the new 
element*/

int valueAtIndex(Index *index);
/*returns the integer member of this index (could be done as a macro)*/

Index *nextIndex(Index *index);
/*returns the next index in the list*/

Index *consIndex(Index *index, int i);
/*returns a new Index with i at the head*/

short memberOfIndex(Index *index, int i);
/*returns 1 if i is in index, else 0*/

void insertIndex(IndexPtr *index, int i);
/*adds i to index provided it is not already a member*/

void insertSortIndex(IndexPtr *index, int i);
/*adds i to index in ascending order, provided it is not a member*/

void deleteIndex(IndexPtr *index,int i);
/*deles i from the list *index*/

Index *stepDeleteIndex(IndexPtr *index, int n);
/*finds and returns the Index n steps along from start, 
and deletes this entry from the list, but does not free it.
If n==0 then it returns (and deletes) the first element.
It returns NOINDEX if the list is empty or if n runs
off the end of the list.*/

TIndex *newTIndex(void);
/*returns a new, empty TIndex*/

void appendTIndex(TIndex *tindex, int i);
/*appends i to end of list referenced by tindex*/

int popTIndex(TIndex *tindex);
/*pops the head of the list*/

short intersectsIndex(Index *inda, Index *indb);
/*returns 1 if intersection inda and indb is not empty*/

void fprintIndex(FILE *fp, Index *index);
/*prints the list*/

void fprintTIndex(FILE *fp,TIndex *tindex);
/*prints a tail list*/

void addIndexToTIndex(TIndex *tindex, Index *index);
/*adds all elements of index to the tail index*/

TIndex *randomisedTIndex(int n);
/*builds a permutation of 0,1,...,n-1*/

GIndex *newGIndex(AnyPtr object);
/*creates a new Generic Index*/

GIndex *nextGIndex(GIndex *gind);
/*gets the next one in the list*/

AnyPtr objectAtGIndex(GIndex *gind);
/*returns the object stored at this index*/

GIndex *appendGIndex(GIndex *gindex, AnyPtr object);
/*appends object to the end of the list, returning the Index corresponding to the new 
element*/


TGIndex *newTGIndex(void);
/*returns a new, empty TGIndex*/

void appendTGIndex(TGIndex *tgindex, AnyPtr object);
/*appends object to end of list referenced by tgindex*/

AnyPtr popTGIndex(TGIndex *tgindex);
/*pops the head of the list*/

void fprintTGIndex(FILE *fp, TGIndex *tgindex, void (*printObject)(FILE *fp, void *p));

void addGIndexToTGIndex(TGIndex *tgindex, GIndex *gindex);
/*adds all elements of index to the tail index*/



