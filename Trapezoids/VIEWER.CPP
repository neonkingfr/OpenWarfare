#include "viewer.h"

int CScreen::number = 0;
int CScreen::lastId=0;
int CScreen::activeScreenId=-1;

void CScreen::cleardevice()
{
  CDC * currentDc = new CClientDC(pWin);
  CBrush *theBrush = new CBrush(RGB(255,255,255));
  currentDc->SelectObject(theBrush);
  CRect rect; pWin->GetClientRect(&rect);

  CPoint points[5];
  points[0].x = rect.left;
  points[0].y = rect.top;
  points[1].x = rect.right;
  points[1].y = rect.top;
  points[2].x = rect.right;
  points[2].y = rect.bottom;
  points[3].x = rect.left;
  points[3].y = rect.bottom;
  points[4] = points[0];

  currentDc->Polygon( points, 5 );

  delete theBrush;
  delete currentDc;
}

//Implementation of methods in derived classes of CGraphicsObject
/*
void CCircle::Paint(CScreen &scr) { scr.Circle(centerX,centerY,radius);}
void CArc::Paint(CScreen &scr) { scr.Arc(centerX,centerY,startAngle,endAngle,radius);}
*/
void CCircle::Paint(CScreen &scr) {} //not implemented yet
void CArc::Paint(CScreen &scr) {} //not implemented yet
void CLine::Paint(CScreen &scr) { scr.Line(x1,y1,x2,y2); }
void CLine::UpdateBoundingRect(SFloatRect &rect)
{
  if (x1<rect.left) rect.left=x1;
  if (x1>rect.right) rect.right=x1;
  if (y1<rect.top) rect.top=y1;
  if (y1>rect.bottom) rect.bottom=y1;
  if (x2<rect.left) rect.left=x2;
  if (x2>rect.right) rect.right=x2;
  if (y2<rect.top) rect.top=y2;
  if (y2>rect.bottom) rect.bottom=y2;
}
void CLineto::Paint(CScreen &scr) { scr.Lineto(x1,y1); }
void CLineto::UpdateBoundingRect(SFloatRect &rect)
{
  if (x1<rect.left) rect.left=x1;
  if (x1>rect.right) rect.right=x1;
  if (y1<rect.top) rect.top=y1;
  if (y1>rect.bottom) rect.bottom=y1;
}
void CMoveto::Paint(CScreen &scr) { scr.Moveto(x1,y1); }
void CMoveto::UpdateBoundingRect(SFloatRect &rect)
{
  if (x1<rect.left) rect.left=x1;
  if (x1>rect.right) rect.right=x1;
  if (y1<rect.top) rect.top=y1;
  if (y1>rect.bottom) rect.bottom=y1;
}
void CPutPixel::Paint(CScreen &scr) { scr.PutPixel(x1,y1,color); }
void CPutPixel::UpdateBoundingRect(SFloatRect &rect)
{
  if (x1<rect.left) rect.left=x1;
  if (x1>rect.right) rect.right=x1;
  if (y1<rect.top) rect.top=y1;
  if (y1>rect.bottom) rect.bottom=y1;
}
void CColor::Paint(CScreen &scr) { scr.SetColor(color); }
void CLineStyle::Paint(CScreen &scr) { scr.SetLineStyle(linestyle, thickness); }

//Implementation of methods of class CScreen

CScreen::~CScreen()
{
  Clear();
}

CScreen::CScreen(CWnd *win)
{
  /* initialize graphics and local variables */
  zoom = 1;
  x0 = 0;
  y0 = 0;
  resetAll = true;  //update values at first possibility

  number++;
  screenId=++lastId;
  repainting=false;
  pen=NULL;
  SetLineStyle(PS_SOLID, 1);
  pWin = win;
}

void CScreen::initGraph(CDC *pDC)
{
  if (!pDC) { dc = new CPaintDC(pWin); doNotDeleteDC = false; }
  else { dc = pDC; doNotDeleteDC = true; }
  if (pen) delete pen;
  pen = new CPen(PS_SOLID, 1, RGB(0,0,0));
  dc->SelectObject(pen);
  CRect okno; pWin->GetClientRect(&okno);
  xmax = okno.Width();
  ymax = okno.Height();
}

void CScreen::closeGraph()
{
  if (pen) delete pen;
  if (!doNotDeleteDC) delete dc;
}

void CScreen::SetGlobalBorders()
{
  xMax=x0+(xmax/2)*zoom;  //xMax etc. are from clipline.h
  xMin=x0-(xmax/2)*zoom;
  yMax=y0+(ymax/2)*zoom;
  yMin=y0-(ymax/2)*zoom;
}

int CScreen::TranslateX(TCoordinate x)
{
  return int((xmax/2)+(x-x0)/zoom);
}

double CScreen::InvTranslateX(int x)
{
  return ((x-xmax/2)*(double)zoom + x0);
}

int CScreen::TranslateY(TCoordinate y)
{
  //return int( (ymax/2)+(y-y0)/zoom ); 
  return int( ymax - ( (ymax/2)+(y-y0)/zoom ) );
}

double CScreen::InvTranslateY(int y)
{
  return (y0-(y-ymax/2)*(double)zoom);
}

void CScreen::Clear()
{
  PtrListIterator<CGraphicObject> iterator(list);
  CGraphicObject *grObj;
  while ( grObj << iterator ) delete grObj;
  list.Clear();   //it is something like deep destructor
}

void CScreen::SetColor(COLORREF col) {
  color = col;
  if (repainting)
  {
    if (activeScreenId==screenId) 
    {
      if (pen) delete pen;
      pen = new CPen(nPenStyle, nWidth, color);
      dc->SelectObject(pen); 
    }
  }
  if (!repainting)
  {
    CGraphicObject *obj = new CColor(col);
    if ( !obj ) MemError();
    list << obj;
  }
}

void CScreen::SetLineStyle(int linestyle, int thickness)
{
  nPenStyle = linestyle;
  nWidth = thickness;
  if (repainting)
  {
    if (activeScreenId==screenId) 
    { 
      if (pen) delete pen;
      pen = new CPen(nPenStyle, nWidth, color);
      dc->SelectObject(pen);
    }
  }
  if (!repainting)
  {
    CGraphicObject *obj = new CLineStyle(linestyle, thickness);
    if ( !obj ) MemError();
    list << obj;
  }
}

void CScreen::Line(TNumber x1, TNumber y1, TNumber x2, TNumber y2)
{
  TCoordinate x1_,y1_,x2_,y2_;
  if (repainting)
  {
    if (activeScreenId==screenId)
    {
      x1_=x1; y1_=y1; x2_=x2; y2_=y2;
      if (CohenSutherland(x1_,y1_,x2_,y2_)) {
        dc->MoveTo(TranslateX(x1_),TranslateY(y1_));
        dc->LineTo(TranslateX(x2_),TranslateY(y2_));
      }
    }
  }
  if (!repainting)
  {
    CGraphicObject *obj = new CLine(x1,y1,x2,y2);
    if ( !obj ) MemError();
    list << obj;
  }
}

void CScreen::Lineto(TNumber x1, TNumber y1)
{
  Line(x,y,x1,y1);
  //nasledujici je asi zbytecne, nebot Line(...) uz ulozi!
  if (!repainting)
  {
    CGraphicObject *obj = new CLineto(x1,y1);
    if ( !obj ) MemError();
    list << obj;
  }

  x=x1; y=y1;
}

void CScreen::Moveto(TNumber x1, TNumber y1)
{
  if (repainting)
  {
    if (activeScreenId==screenId) dc->MoveTo(TranslateX(x1),TranslateY(y1));
  }
  if (!repainting)
  {
    CGraphicObject *obj = new CMoveto(x1,y1);
    if ( !obj ) MemError();
    list << obj;
  }
  x=x1; y=y1;
}

void CScreen::PutPixel(TNumber x1, TNumber y1, COLORREF col)
{
  if (repainting)
  {
    if (activeScreenId==screenId)
    {
      if (IsPointInside(x1,y1))
      {
        CPen  pixPen(PS_SOLID, 1, col);
        CPen *lOldPen=dc->SelectObject(&pixPen);
        CBrush brush(col);
        CBrush *lOldBrush=dc->SelectObject(&brush);
        int sx=TranslateX(x1); int sy=TranslateY(y1); int radius=4;
        dc->Ellipse(sx-radius, sy-radius, sx+radius, sy+radius);
        dc->SelectObject(lOldBrush);
        dc->SelectObject(lOldPen);
      }
    }
  }
  if (!repainting)
  {
    CGraphicObject *obj = new CPutPixel(x1,y1,col);
    if ( !obj ) MemError();
    list << obj;
  }
}

void CScreen::PutPixel(TNumber x1, TNumber y1)
{
  if (activeScreenId==screenId) PutPixel(x1,y1,color);
  if (!repainting)
  {
    CGraphicObject *obj = new CPutPixel(x1,y1,color);
    if ( !obj ) MemError();
    list << obj;
  }
}

void CScreen::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
  //setPWin(this);
  CPaintDC pDC(pWin);
  initGraph(&pDC);
  SetActive();

  bool invalidate=true;
  switch (nFlags & 0x7f) {
       case KEY_LEFT:  x0-=(xmax/4)*zoom; Repaint(); break;
       case KEY_RIGHT: x0+=(xmax/4)*zoom; Repaint(); break;
       case KEY_UP:    y0+=(ymax/4)*zoom; Repaint(); break;
       case KEY_DOWN:  y0-=(ymax/4)*zoom; Repaint(); break;
       case KEY_GRAYPLUS:   Zoom(1.5f); Repaint(); break;
       case KEY_GRAYMINUS:  Zoom(1.0f/1.5f); Repaint(); break;
       default: invalidate=false;
  }
  if (invalidate) pWin->Invalidate();
}

void CScreen::Repaint()
{
  if (pWin) {  //nemusi byt totiz nastaveno...
    PtrListMember<CGraphicObject> *next;
    CGraphicObject *key;

    //cleardevice();
    repainting=true;
    SetGlobalBorders();
    next=list.first;
    while (next!=NULL)
    {
      next = *next >> key;
      key->Paint(*this);
    }
    repainting=false;
  }
}

void CScreen::ComputeBoundingBox()
{
  PtrListMember<CGraphicObject> *next = list.first;
  CGraphicObject *key;
  bbox = SFloatRect(INFINITE_NUMBER,INFINITE_NUMBER,-INFINITE_NUMBER,-INFINITE_NUMBER);
  while (next!=NULL)
  {
    next = *next >> key;
    key->UpdateBoundingRect(bbox);
  }
  if (bbox.right-bbox.left<0.5) bbox.right=bbox.left+0.5;
  if (bbox.bottom-bbox.top<0.5) bbox.bottom=bbox.top+0.5;
}

void CScreen::SetActive()
{
  SetGlobalBorders();
  if (activeScreenId!=screenId)
  {
    activeScreenId=screenId;
    Repaint();
  }
}

LSError CScreen::Serialize(ParamArchive &ar)
{
  AutoArray<CGraphicObjectPtr> grAr;
  if (ar.IsSaving())
  {
    PtrListMember<CGraphicObject> *next;
    CGraphicObject *key;
    next=list.first;
    while (next!=NULL)
    {
      next = *next >> key;
      grAr.Add(key);
    }
    ar.Serialize("GraphicObjects", grAr, 1);
  }
  else
  {
    ar.Serialize("GraphicObjects", grAr, 1);
    for (int i=0, siz=grAr.Size(); i<siz; i++)
    {
      list << grAr[i].ptr;      
    }
  }
  return LSOK;
}
