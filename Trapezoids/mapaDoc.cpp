// mapaDoc.cpp : implementation of the CMapaDoc class
//

#include "stdafx.h"
#include "mapa.h"

#include "mapaDoc.h"
#include "mapaView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMapaDoc

IMPLEMENT_DYNCREATE(CMapaDoc, CDocument)

BEGIN_MESSAGE_MAP(CMapaDoc, CDocument)
	//{{AFX_MSG_MAP(CMapaDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMapaDoc construction/destruction

#include "trapezoid.h"

//COLORREF ColorTable[8]={RGB(255,0,0), RGB(0,190,0), RGB(0,0,190), RGB(0,150,150), RGB(200,0,200), RGB(200,200,0), RGB(200,200,200), RGB(0,0,0)};
//poseidon debug used colors
COLORREF ColorTable[]={
RGB(0, 0, 0),     // OITNormal,
RGB(0, 127, 127), // OITAvoidBush,
RGB(0, 127, 0),   // OITAvoidTree,
RGB(127, 0, 0),   // OITAvoid,
RGB(0, 0, 255),     // OITWater,
RGB(80, 80, 80),     // OITSpaceRoad,
RGB(160, 160, 160),     // OITRoad,
RGB(0, 255, 255),     // OITSpaceBush,
RGB(0, 255, 0),     // OITSpaceTree,
RGB(255, 0, 0),     // OITSpace
RGB(255, 255, 0),     // OITRoadForced
};


struct PathTypeInfo 
{
  unsigned int color;
  int lineStyle, lineWidth;
  PathTypeInfo(unsigned int c, int ls, int lw) : color(c), lineStyle(ls), lineWidth(lw) {}
};

enum OsmWayTypes
{ 
  OWTTrunk,
  OWTTrunkLink,
  OWTPrimary,
  OWTSecondary,
  OWTTertiary,
  OWTResidential,
  OWTTrack,
  OWTFootWay,
  OWTUnclassified,
  OWTBuilding,
  OWTWater,
  OWTWaterStream,
  OWTTunnel,
  OWTForest,
  OWTPark,
  OWTOther,
  OWTRed
};

PathTypeInfo PathTypeInfoTable[]={
  PathTypeInfo(RGB(0,255,0), PS_SOLID, 5), //green TRUNK, MOTORWAY
  PathTypeInfo(RGB(0,255,0), PS_SOLID, 3), //green TRUNK, MOTORWAY LINKS
  PathTypeInfo(RGB(255,0,0), PS_SOLID, 4), //red   PRIMARY
  PathTypeInfo(RGB(255,201,14), PS_SOLID, 3), //orange SECONDARY
  PathTypeInfo(RGB(230,230,0), PS_SOLID, 2), //yellow TERTIARY
  PathTypeInfo(RGB(164,82,0), PS_SOLID, 2), //brown RESIDENTIAL
  PathTypeInfo(RGB(164,82,0), PS_DASH, 1), //brown TRACK
  PathTypeInfo(RGB(220,82,0), PS_DOT, 1), //reddish FOOTWAY
  PathTypeInfo(RGB(164,82,0), PS_SOLID, 2), //brown UNCLASSIFIED (same as residential)
  PathTypeInfo(RGB(164,82,0), PS_SOLID, 1), //brown BUILDING
  PathTypeInfo(RGB(183,242,228), PS_SOLID, 5), //blue WATER
  PathTypeInfo(RGB(122,222,242), PS_SOLID, 2), //blue WATER STREAM
  PathTypeInfo(RGB(0,0,0), PS_DASHDOT, 1), //black TUNNEL
  PathTypeInfo(RGB(170,200,50), PS_SOLID, 5), //green FOREST
  PathTypeInfo(RGB(170,255,170), PS_SOLID, 5), //green PARK
  PathTypeInfo(RGB(0,0,0), PS_DOT, 1), //black rest
  PathTypeInfo(RGB(255,0,0), PS_DOT, 1) //red
};

const float GetX(Trapezoid *tr)
{
  return (tr->x1+tr->x2)/2;
}
const float GetY(Trapezoid *tr)
{
  return (tr->y1+tr->y2+tr->y3+tr->y4)/4;
}

#include "el/QStream/QStdStream.hpp"
static bool decompose = false;
static bool hobbyToleranceSquares = false;
CMapaDoc::CMapaDoc()
: docCreatedYet(false), drawEdges(true), drawTrapezoids(true), cosLatitude(1)
{
	// TODO: add one-time construction code here
	CScreen::SetInactive();
	screen.Init(0.2,0,0);
  
  bool serializeFromStdin = false;
  char *fileName=NULL;
  for (int paramIx=1; paramIx<__argc; paramIx++)
  {
    if (!strcmpi(__argv[paramIx],"-stdin")) serializeFromStdin = true;
    else if (!strcmpi(__argv[paramIx],"-d")) decompose=true;
    else if (!strcmpi(__argv[paramIx],"-tolsq")) hobbyToleranceSquares=true;
    else if (!fileName) fileName = __argv[paramIx];
  }
  if (serializeFromStdin)
  {
    ParamArchiveLoad ar;
    QStdInStream in;
    if (ar.Load(in))
    {
      LoadFromParamArchive(ar);
      docCreatedYet=true; //MFC framework calls OnDocumentNew, this will stop it
    }
  }
}

#include "MainFrm.h"
LSError CMapaDoc::LoadComponents(ParamArchive &ar)
{
  //ParamArchiveLoad ar(cc_cast(fileName));
  TrapezoidalDecomposition<AutoArray<Ref<Trapezoid> > > trapezoidalDecomposition(convexComponents, pathComponents);
  CHECK_INTERNAL(trapezoidalDecomposition.Serialize(ar));
  bool roundIt=false;
  float unitSize=0.1f;  //try 10 cm
  if (roundIt)
  {
    for (int i=0; i<convexComponents.Size(); i++)
    {
      ConvexComponent &cc=convexComponents[i];
      for (int j=0; j<cc.polygon.Size(); j++)
      {
        float newval = floorf(cc.polygon[j]/unitSize+0.5f);
        cc.polygon[j++]=newval*cosLatitude;
        newval = floorf(cc.polygon[j]/unitSize+0.5f);
        cc.polygon[j]=newval;
      }
    }
    for (int i=0; i<pathComponents.Size(); i++)
    {
      PathComponent &pc=pathComponents[i];
      for (int j=0; j<pc.path.Size(); j++)
      {
        float newval = floorf(pc.path[j]/unitSize+0.5f);
        pc.path[j++]=newval*cosLatitude;
        newval = floorf(pc.path[j]/unitSize+0.5f);
        pc.path[j]=newval;
      }
    }
  }
  if (decompose)
  {
    for (int i=0, siz=convexComponents.Size(); i<siz; i++)
    {
      ConvexComponent &comp = convexComponents[i];
      AutoArray<Point2DRound> poly;
      for (int j=0; j<comp.polygon.Size()/2; j++)
      {
        Point2DRound pt(comp.polygon[2*j]*cosLatitude, comp.polygon[2*j+1]);
        poly.Add(pt);
      }
      trapezoidalDecomposition.AddPolygon(poly, comp.color);
    }
    //trapezoidalDecomposition.Decompose();
    trapezoidalDecomposition.TwoPassDecompose();
  }
  Intersections intersections;
  AutoArray<Point2DFloat> specPoints;
  if (hobbyToleranceSquares)
  {
    for (int i=0, siz=convexComponents.Size(); i<siz; i++)
    {
      ConvexComponent &comp = convexComponents[i];
      AutoArray<Point2DFloat> poly;
      for (int j=0; j<comp.polygon.Size()/2; j++)
      {
        Point2DFloat pt(comp.polygon[2*j]*cosLatitude, comp.polygon[2*j+1]);
        poly.Add(pt);
      }
      intersections.AddPolygon(poly, comp.color);
    }
    specPoints = intersections.SnapRound();
  }
  // TODO: add one-time construction code here
  CScreen::SetInactive();
  screen.Init(0.2,0,0);

  //Testing example can be loaded from file now
  if (decompose) screen.SetLineStyle(PS_DOT, 1);
  else screen.SetLineStyle(PS_SOLID, 2);
  float maxabsx=1.0f, maxabsy=1.0f;
  int ix=0;
  for (int i=0; i<convexComponents.Size(); i++)
  {
    screen.SetColor(ColorTable[convexComponents[i].color]);
    AutoArray<float> &polygon = convexComponents[i].polygon;
    int polySize = polygon.Size()/2;
    for (int j=0; j<polySize; j++)
    {
      screen.Line(polygon[2*j]*cosLatitude, polygon[2*j+1], polygon[2*((j+1)%polySize)]*cosLatitude, polygon[2*((j+1)%polySize)+1]);
      saturateMax(maxabsx, fabs(polygon[2*j]*cosLatitude)); saturateMax(maxabsx, fabs(polygon[2*((j+1)%polySize)]*cosLatitude));
      saturateMax(maxabsy, fabs(polygon[2*j+1])); saturateMax(maxabsy, fabs(polygon[2*((j+1)%polySize)+1]));
    }
// GREEN COLOR FOR SEGMENTS ENDPOINTS
//    screen.SetColor(RGB(0,255,0));
//    for (int j=0; j<polySize; j++) screen.PutPixel(polygon[2*j], polygon[2*j+1]);
  }

  for (int i=0; i<pathComponents.Size(); i++)
  {
    PathTypeInfo &info = PathTypeInfoTable[pathComponents[i].type];
    screen.SetColor(info.color);
    screen.SetLineStyle(info.lineStyle, info.lineWidth);
    AutoArray<float> &path = pathComponents[i].path;
    int pathSize = path.Size()/2;
    for (int j=0; j<pathSize-1; j++)
    {
      screen.Line(path[2*j]*cosLatitude, path[2*j+1], path[2*(j+1)]*cosLatitude, path[2*(j+1)+1]);
      saturateMax(maxabsx, fabs(path[2*j]*cosLatitude)); saturateMax(maxabsx, fabs(path[2*(j+1)]*cosLatitude));
      saturateMax(maxabsy, fabs(path[2*j+1])); saturateMax(maxabsy, fabs(path[2*(j+1)+1]));
    }
  }

  AutoArray<Ref<Trapezoid> > &trapezoids = trapezoidalDecomposition.trapezoids;
  for (int i=0; i<trapezoids.Size(); i++)
  {
    Trapezoid *tr = trapezoids[i].GetRef();
    float maxvalx = maxabsx*1.5f;
    float maxvaly = maxabsy*1.5f;
    if (fabs(tr->y1)>maxvaly) tr->y1 = maxvaly*FloatSgn(tr->y1);
    if (fabs(tr->y2)>maxvaly) tr->y2 = maxvaly*FloatSgn(tr->y2);
    if (fabs(tr->y3)>maxvaly) tr->y3 = maxvaly*FloatSgn(tr->y3);
    if (fabs(tr->y4)>maxvaly) tr->y4 = maxvaly*FloatSgn(tr->y4);
    if (fabs(tr->x1)>maxvalx) tr->x1 = maxvalx*FloatSgn(tr->x1);
    if (fabs(tr->x2)>maxvalx) tr->x2 = maxvalx*FloatSgn(tr->x2);
    if (drawTrapezoids)
    {
      screen.SetLineStyle(PS_DOT, 1);
      screen.SetColor(RGB(0,200,0));
      screen.Line(tr->x1, tr->y1, tr->x1, tr->y2);
      screen.SetLineStyle(PS_SOLID, 2);
      screen.SetColor(RGB(0,0,200));
      screen.Line(tr->x2, tr->y3, tr->x2, tr->y4);
    }
    LogF("%d.) x=%.2f y=%.2f [%.1f,%.1f][%.1f,%.1f,%.1f,%.1f]",i+1,GetX(tr),GetY(tr), tr->x1,tr->x2,tr->y1,tr->y2,tr->y3,tr->y4);
    if (drawTrapezoids) screen.PutPixel(GetX(tr),GetY(tr),ColorTable[tr->color]);
  }
  screen.SetLineStyle(PS_SOLID, 1);
  screen.SetColor(RGB(0,0,0));
  int edgeNum = 0, edgeNum2 = 0;
  for (int i=0; i<trapezoids.Size(); i++)
  {
    Trapezoid *tr = trapezoids[i].GetRef();
    float x = GetX(tr);
    float y = GetY(tr);
    if (drawEdges)
    {
      if (tr->leftTr1) { screen.Line(x,y, GetX(tr->leftTr1), GetY(tr->leftTr1)); edgeNum++; }
      if (tr->leftTr2) { screen.Line(x,y, GetX(tr->leftTr2), GetY(tr->leftTr2)); edgeNum++; }
      if (tr->rightTr1) { screen.Line(x,y, GetX(tr->rightTr1), GetY(tr->rightTr1)); edgeNum++; }
      if (tr->rightTr2) { screen.Line(x,y, GetX(tr->rightTr2), GetY(tr->rightTr2)); edgeNum++; }
    }
    int siz = siz=tr->edgesUp.Size();
    edgeNum2 += siz;
    if (drawEdges)
    {
      for (int i=0; i<siz; i++)
      {
        screen.Line(x,y,GetX(tr->edgesUp[i]), GetY(tr->edgesUp[i]));
      }
    }
    if (tr->edgesUp.Size())
    {
      int col = max(tr->edgesUp[0]->color, tr->color);
      screen.SetColor(ColorTable[col]);
      screen.SetLineStyle(PS_SOLID, 3);
      screen.Line(tr->x1,tr->y2, tr->x2, tr->y3);
      if (drawEdges)
      {
        screen.SetLineStyle(PS_SOLID, 1);
        screen.SetColor(RGB(0,0,0));
      }
    }
  }
  if (hobbyToleranceSquares)
  {
    if (hobbyToleranceSquares) {
      screen.SetColor(RGB(80,128,80));
      screen.SetLineStyle(PS_DOT,1);
    }
    for (int i=0; i<specPoints.Size(); i++)
    {
      screen.PutPixel(specPoints[i].x, specPoints[i].y);
      if (hobbyToleranceSquares)
      {
        screen.Moveto(specPoints[i].x-0.5f, specPoints[i].y-0.5f);
        screen.Lineto(specPoints[i].x-0.5f, specPoints[i].y+0.5f);
        screen.Lineto(specPoints[i].x+0.5f, specPoints[i].y+0.5f);
        screen.Lineto(specPoints[i].x+0.5f, specPoints[i].y-0.5f);
        screen.Lineto(specPoints[i].x-0.5f, specPoints[i].y-0.5f);
      }
    }
  }
  return LSOK;
}

CMapaDoc::~CMapaDoc()
{
	screen.closeGraph();
}

BOOL CMapaDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return false;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)
  if (!docCreatedYet)
  {
    screen.Clear();
  }
  docCreatedYet=false;

	return true;
}

/////////////////////////////////////////////////////////////////////////////
// CMapaDoc serialization

#include <es/Strings/rString.hpp>
void CMapaDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
    CString fileName=ar.m_strFileName;
    ParamArchiveSave parar(1);
    if (convexComponents.Size()>0)
    {
      TrapezoidalDecomposition<AutoArray<Ref<Trapezoid> > > trapezoidalDecomposition(convexComponents, pathComponents);
      trapezoidalDecomposition.Serialize(parar);
    }
    else screen.Serialize(parar);
    QOStrStream outstream;
    parar.Save(outstream);
    QIStrStream instream; instream.init(outstream);
    int size = instream.rest();
    RString buf; buf.CreateBuffer(size+1);
    instream.read(buf.MutableData(),size);
    ar.Write(buf.Data(),size);
	}
	else
	{
		// TODO: add loading code here
    CString fileName=ar.m_strFileName;
    ParamArchiveLoad parar(fileName);
    LoadFromParamArchive(parar);
	}
}

void CMapaDoc::LoadFromParamArchive(ParamArchive &parar)
{
  screen.Clear();
  convexComponents.Clear();
  ParamArchive dummy;
  float latitude = 1.0f;
  parar.Serialize("latitude", latitude, 0, 1.0f);
  cosLatitude = cos(latitude*3.1415926535f/180);
  if (parar.OpenSubclass("ConvexComponents",dummy))
  {
    parar.CloseSubclass(dummy);
    LoadComponents(parar);
  }
  else if (parar.OpenSubclass("Paths",dummy))
  {
    parar.CloseSubclass(dummy);
    LoadComponents(parar);
  }
  else screen.Serialize(parar);

  POSITION pos = GetFirstViewPosition();
  CMapaView* mapView = (CMapaView *)GetNextView( pos );

  screen.ComputeBoundingBox();
  const SFloatRect &bb = screen.GetBoundingBox();
  screen.x0 = (bb.left+bb.right)/2;
  screen.y0 = (bb.bottom+bb.top)/2;
  screen.zoom=0.01;
  screen.resetAll = true;
}

/////////////////////////////////////////////////////////////////////////////
// CMapaDoc diagnostics

#ifdef _DEBUG
void CMapaDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMapaDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMapaDoc commands
