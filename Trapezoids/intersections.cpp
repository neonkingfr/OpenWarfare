#include "intersections.h"

///static member variables definition
float Segment::_sweepLineX;
int   Segment::_globalId=0;

void ToleranceSquare::InsertPixelIntoSegment(Segment *seg)
{
  float x = floorf(event->point.x+0.5f);
  LogF("Seg([%3.2f,%3.2f],[%3.2f,%3.2f]) <== [%3.2f,%3.2f]", 
        seg->leftP.x, seg->leftP.y, seg->rightP.x, seg->rightP.y, x,y);
}

void ToleranceSquare::FindReferenceSegment()
{
  switch (event->type)
  {
  case EventOne::E_END: 
    referenceSegment=event->segA; 
    return;
  case EventOne::E_INTERSECTION:
    if (event->segA->leftP.x!=event->point.x) { referenceSegment=event->segA; return; }
    else if (event->segB->leftP.x!=event->point.x) { referenceSegment=event->segB; return; }
    break;
  case EventOne::E_BEGIN:
    break;
  }
  //the segment below segA, which intersects sweepLine (on X==event->point.x-0.5f), will be used
  Segment *seg=event->segA->segBefore;
  float sweepX = event->point.x-0.5f;
  while (seg)
  {
    if (seg->leftP.x < sweepX) break;
    seg = seg->segBefore;
  }
  referenceSegment=seg;
}

Segment::Segment(Point2DFloat &ptLeft, Point2DFloat &ptRight)
: leftP(ptLeft), rightP(ptRight), segBefore(NULL)
{
  id.LowPart = ++_globalId;
  float dx = ptRight.x - ptLeft.x;
  dydx = 1.0f;
  if (dx<0.5f) //heavily assumes, that x,y are integer coordinates!
  {
    vertical = true;
  }
  else 
  {
    dydx = (ptRight.y-ptLeft.y)/dx;
    vertical = false;
  }
}

float Segment::YIntersect(float x)
{
  if (vertical) return leftP.y;
  return ( leftP.y + (dydx)*(x-leftP.x) );
}

//return true if the order based on slopes is this followed by seg2
bool Segment::IsLessBySlope(Segment &seg2)
{
  float dy1 = rightP.y-leftP.y;
  float dx1 = rightP.x-leftP.x;
  float dy2 = seg2.rightP.y-seg2.leftP.y;
  float dx2 = seg2.rightP.x-seg2.leftP.x;
  float result = dy1*dx2-dy2*dx1;
  if (fabs(result)<0.5f) //heavily assumes, that x,y are integer coordinates!
    return (id.QuadPart<seg2.id.QuadPart);
  return result<0.0f;
}

// test intersect of 2 segments and return: 0=none, 1=intersect
bool Segment::IntersectLowerSegment(Segment* seg2, Point2DFloat &intPt)
{
  if (seg2 == NULL) return false; // no intersect if second segment doesn't exist
  if (IsLessBySlope(*seg2)) //J.D.Hobby's proposal (need for Theorem 2.2 to ignore some intersections)
  {
    //Ax+By=C
    float A1 = rightP.y-leftP.y;
    float B1 = leftP.x-rightP.x;
    float C1 = A1*leftP.x+B1*leftP.y;
    float A2 = seg2->rightP.y-seg2->leftP.y;
    float B2 = seg2->leftP.x-seg2->rightP.x;
    float C2 = A2*seg2->leftP.x+B2*seg2->leftP.y;
    //solve the two equations for the two unknowns, x and y
    //A1x + B1y = C1
    //A2x + B2y = C2
    float det = A1*B2 - A2*B1;
    if (det>0.5f) //heavily assumes, that coordinates are integer!
    {
      intPt = Point2DFloat((B2*C1 - B1*C2)/det, (A1*C2 - A2*C1)/det);
      //test intersection point correctness (J.D.Hobby's proposal)
      if ((rightP<intPt) || (seg2->rightP<intPt) || (intPt.x<_sweepLineX)) return false;
      return true;
    }
    //Lines are overlapping, parallel or intersection on segment endpoints
    //intersections will be added at "Snap Round" pass
  }
  return false; // => no intersect exists
}

///only integer Y value of the starting endpoint of this segment 
///should be compared to active segment seg2 on SweepLine
bool Segment::operator<(Segment &seg2)
{
  //_sweepLineX should be properly set!
  float myY, seg2Y;
  //myY = (vertical ? rightP.y : leftP.y);  //Hobby's rule?!
  myY = leftP.y;
  seg2Y = (seg2.vertical ? seg2.leftP.y : seg2.YIntersect());
  if (myY==seg2Y) { //TODO: maybe, we should know, what means myY==seg2Y better (compare to Hobby's 1/(L_delta))
    //decide by slope
    return IsLessBySlope(seg2);
  }
  return (myY < seg2Y);
}

Segment *SweepLineOne::Add(Segment *seg)
{
#if _DEBUG
  count++;
  if (count>maxCount) maxCount=count;
#endif
  Segment *segi = segList.First();
  while(segi!=_segNull)
  {
    if (*seg<*segi) break;
    segi=static_cast<Segment *>(segi->next);
  }
  Segment *retval = static_cast<Segment *>(segi->prev);
  Segment *start=segList.Start();
  segList.InsertBefore(segi, seg);
  if (segi==start) return NULL;
  return retval;
}

void SweepLineOne::InsertSegAfter(Segment *seg, Segment *before)
{
  segList.InsertAfter(before, seg);
}

void SweepLineOne::InsertSeg(Segment *seg)
{
  segList.Insert(seg);
}

void SweepLineOne::Delete(Segment *seg)
{
#if _DEBUG
  count--;
#endif
  segList.Delete(seg);
}

void SweepLineOne::Swap(Segment *segA, Segment *segB)
{
#if _DEBUG
  Assert(segB->next==static_cast<Segment::Base *>(segA));
#endif
  segB->SwapWithNext();
}

Segment *SweepLineOne::GetPrev(Segment *seg)
{
  Segment *retval = static_cast<Segment *>(seg->prev);
  if (retval==_segNull) return NULL;
  return retval;
}

Segment *SweepLineOne::GetNext(Segment *seg)
{
  Segment *retval = static_cast<Segment *>(seg->next);
  if (retval==_segNull) return NULL;
  return retval;
}

Segment *SweepLineOne::GetFirst()
{
  Segment *retval = segList.First();
  if (retval==_segNull) return NULL;
  return retval;
}; //NULL if no such

#if _DEBUG
void SweepLineOne::LogDebugInfo()
{
  LogF("SweepLine status:");
  for (Segment *seg = segList.First(); seg!=segList.End(); seg=segList.Advance(seg))
  {
    LogF("  %4.2f,%4.2f ---> %4.2f,%4.2f", seg->leftP.x,seg->leftP.y, seg->rightP.x,seg->rightP.y);
  }
}
#endif

int EventOneQueue::IntersectionCode(Segment *seg1, Segment *seg2)
{
  int min,max;
  if (seg1->GetId().LowPart<seg2->GetId().LowPart)
  {
    min = seg1->GetId().LowPart; max = seg2->GetId().LowPart;
  }
  else
  {
    min = seg2->GetId().LowPart; max = seg1->GetId().LowPart;
  }
  INTEGER32 retval; 
  retval.LowPart=min;
  retval.HighPart=max;
  return retval.QuadPart;
}

EventOne *Intersections::AddEventOne(Segment *seg, EventOne::EventOneType type)
{
  EventOne *retval;
  switch (type)
  {
  case EventOne::E_BEGIN:
    {
      retval=new EventOne(seg, EventOne::E_BEGIN);
    }
    break;
  case EventOne::E_END:
    {
      retval=new EventOne(seg, EventOne::E_END);
    }
    break;
  default:
    Assert(false);
  }
  _eventQ.QueueInsert(retval);
  return retval;
}

void Intersections::Add(float x1, float y1, float x2, float y2, int color, int polyId)
{
  Segment *seg;
  Point2DFloat from(x1,y1), to(x2,y2);
  bool orientation = (from < to);
  if (!orientation) { //swap
    Point2DFloat tmp=from; from=to; to=tmp;
  }
  if (from==to)
  {
//    LogF("Zero length segment %3.2f,%3.2%f: ELIMINATED!", from.x, from.y);
    return;
  }
  seg = new Segment(from, to);
  seg->SetColor(color);
  seg->SetPolyId(polyId);
  seg->SetOrientation(orientation);
  AddEventOne(seg, EventOne::E_BEGIN);
  AddEventOne(seg, EventOne::E_END);
}

void Intersections::AddPolygon(AutoArray<Point2DFloat> &poly, int color)
{
  int n=poly.Size();
  if (n<=1) return;
  const Point2DFloat * from=&poly.Get(0);
  for (int i=1, n=poly.Size(); i<=n; i++)
  {
    const Point2DFloat * to=&poly.Get(i%n);
    Add(from->x, from->y, to->x, to->y, color, _polyNum);
    from = to;
  }
  _polyNum++;  
}

#include "trapezoid.h"
AutoArray<Point2DFloat> Intersections::SnapRound()
{
  AutoArray<Ref<EventOne> > *events2 = FirstPass();
  AutoArray<Point2DFloat> specPoints;
/*
  //return intersections as spec points
  for(int i=0; i<events2->Size(); i++)
  {
    if ((*events2)[i]->type==EventOne::E_INTERSECTION)
      specPoints.Add((*events2)[i]->point);
  }
*/
  AutoArray<Ref<Event> > *events = SecondPass(events2);
  //return tolerance squares centers as spec points
  for(int i=0; i<events->Size(); i++)
  {
    specPoints.Add(Point2DFloat((*events)[i]->point->x, (*events)[i]->point->y));
  }
  delete events;
  delete events2;
  return specPoints;
}

AutoArray<Ref<EventOne> > * Intersections::FirstPass()
{
  //suppose, that _eventQ is initialized with all segment endpoints
  //event queue should be priority queue, so it's properly lexicographically sorted
  AutoArray<Ref<EventOne> > *eventStack = new AutoArray<Ref<EventOne> >;
#if _DEBUG
  int count=1;
#endif
  while (!_eventQ.IsEmpty())
  {
    Ref<EventOne> E;
    _eventQ.QueueRemoveFirst(E);
    EventOne *newE = NULL;
    Segment::_sweepLineX=E->point.x;
#if _DEBUG
/*
    LogF("%d: _sweepLineX == %4.2f,  segE->id == %d", count++, Segment::_sweepLineX, E->segA->id);
    int debugID=262163;
    if (debugID==E->segA->id.QuadPart)
      _asm nop;
      bool debugSweepLine=true;
      if (debugSweepLine)
      {
      _sweepLineOne.LogDebugInfo();
      }
*/
#endif
    switch (E->type)
    {
    case EventOne::E_BEGIN:
      {
        Segment *segE = E->segA;
        segE->segBefore = _sweepLineOne.Add(segE);
        newE = new EventOne(segE, EventOne::E_BEGIN);
        Segment *segA = _sweepLineOne.GetNext(segE);
        Segment *segB = _sweepLineOne.GetPrev(segE);
        Point2DFloat intPt;
        if (segA && segA->IntersectLowerSegment(segE, intPt))
        {
          IntValue val(_eventQ.IntersectionCode(segA, segE));
          EventOne *newIntEvent = new EventOne(intPt, segA, segE);
          _eventQ.QueueInsert(newIntEvent);
          _eventQ._tree.Insert(val);
        }
        if (segB && segE->IntersectLowerSegment(segB, intPt))
        {
          IntValue val(_eventQ.IntersectionCode(segE, segB));
          EventOne *newIntEvent = new EventOne(intPt, segE, segB);
          _eventQ.QueueInsert(newIntEvent);
          _eventQ._tree.Insert(val);
        }
      }
      break;
    case EventOne::E_END:
      {
        Segment *segE = E->segA;
        newE = new EventOne(segE, EventOne::E_END);
        Segment *segA = _sweepLineOne.GetNext(segE);
        Segment *segB = _sweepLineOne.GetPrev(segE);
        _sweepLineOne.Delete(segE);
        Point2DFloat intPt;
        if (segA && segB && segA->IntersectLowerSegment(segB, intPt))
        {
          IntValue val(_eventQ.IntersectionCode(segA,segB));
          if (_eventQ._tree.Find(val)==NULL) //not already in the queue
          {
            EventOne *newIntEvent = new EventOne(intPt, segA, segB);
            _eventQ.QueueInsert(newIntEvent);
            _eventQ._tree.Insert(val);
          }
          else 
            _asm nop;
        }
      }
      break;
    case EventOne::E_INTERSECTION:
      {
        Segment *segA = E->segA;
        Segment *segB = E->segB;
        IntValue val(_eventQ.IntersectionCode(segA,segB));
        _eventQ._tree.Delete(val);
        if (segA == _sweepLineOne.GetNext(segB))
        {
          newE = new EventOne(E->point, segA, segB);
          _sweepLineOne.Swap(segA, segB);
          Segment *segAA = _sweepLineOne.GetNext(segB);
          Segment *segBB = _sweepLineOne.GetPrev(segA);
          Point2DFloat intPt;
          if (segAA && segAA->IntersectLowerSegment(segB, intPt))
          {
            IntValue val(_eventQ.IntersectionCode(segAA,segB));
            if (_eventQ._tree.Find(val)==NULL) //not already in the queue
            {
              EventOne *newIntEvent = new EventOne(intPt, segAA, segB);
              _eventQ.QueueInsert(newIntEvent); //TODO: only if "not already in the queue???"
              _eventQ._tree.Insert(val);
            }
            else 
              _asm nop;
          }
          if (segBB && segA->IntersectLowerSegment(segBB, intPt))
          {
            IntValue val(_eventQ.IntersectionCode(segA,segBB));
            if (_eventQ._tree.Find(val)==NULL) //not already in the queue
            {
              EventOne *newIntEvent = new EventOne(intPt, segA, segBB);
              _eventQ.QueueInsert(newIntEvent); //TODO: only if "not already in the queue???"
              _eventQ._tree.Insert(val);
            }
            else 
              _asm nop;
          }
        } //else ignore due to J.D.Hobby's Theorem 2.2
      }
      break;
    }
    if (newE) eventStack->Add(newE);
  }
#if _DEBUG
  LogF("*** Max count of segments on SweepLine was: %d",_sweepLineOne.maxCount);
#endif
  return eventStack;
}

AutoArray<Ref<Event> > * Intersections::SecondPass(AutoArray<Ref<EventOne> > *events)
{
  //suppose, that events (output of FirstPass) contains all events in order they occured
  //while processing the FirstPass. Each segment contained in E_BEGIN event should have
  //segABefore set to segment on sweepLine, after which this one was inserted (in FirstPass)
  AutoArray<Ref<Event> > *eventStack = new AutoArray<Ref<Event> >;
#if _DEBUG
  int count=1;
#endif
  ///loop through all events and process these, contained in current batch
  int batchStart=0, batchEnd, batchX;
  int evSiz=events->Size();
  while (batchStart<evSiz)
  {
    HeapArray<Ref<ToleranceSquare> > tolSquares;
    int evNum=batchStart;
    //Hobby's STEP 1
    //collect all events that falls into this batch (x depends on first event)
    Segment::_sweepLineX = floorf((*events)[evNum]->point.x+0.5f)-0.5f;
    for (; evNum<evSiz; evNum++)
    {
      Ref<EventOne> E = (*events)[evNum];
      int roundX = (int)floorf(E->point.x+0.5f);
      if (evNum==batchStart) batchX=roundX;
      if (batchX!=roundX) break; //all events for this batch collected
      //this event falls into this batch
      float roundY = floorf(E->point.y+0.5f);
      ToleranceSquare *newSq = new ToleranceSquare(roundY);
      newSq->event = E.GetRef(); //responsible event
      tolSquares.HeapInsert(newSq);
    }
    batchEnd = evNum-1;
    //Hobby's STEP 2
    //sort tolerance squares on current batch and remove duplicities
    Ref<ToleranceSquare> tolSq;
    int lastY = INT_MIN; //imposible value
    ToleranceSquare *lastSq = NULL;
    TListBidir<ToleranceSquare> tolSquareList;
    while (tolSquares.HeapRemoveFirst(tolSq))
    {
      if ((int)tolSq->y!=lastY)
      { //new tolerance square
        if ((int)tolSq->y==lastY+1) //neighbouring segments
          tolSq->bottom = lastSq->top;
        else 
          tolSq->bottom = new HorizontalSegment(tolSq->y-0.5f);
        tolSq->bottom->up = tolSq;
        tolSq->top = new HorizontalSegment(tolSq->y+0.5f);
        tolSq->top->down = tolSq;
        lastY = (int)tolSq->y;
        tolSq->FindReferenceSegment();
        if (tolSq->referenceSegment==NULL)
        {
          tolSq->referenceSegment=_sweepLineOne.GetFirst();
        }
        //Locate y values of bottom horizontal segments in the main active list 
        //starting from the reference segment
        if (tolSq->referenceSegment)
        {
          Segment *startSegment = NULL; //we should find it first (segment just above bottom horizontal segment)
          Segment *seg=tolSq->referenceSegment;
          float segY = seg->YIntersect();
          float y = tolSq->y-0.5f;
          if (segY<y)
          { //find first segment entering tolSq above seg
            do 
            {
              seg = _sweepLineOne.GetNext(seg);
              if (!seg || seg==_sweepLineOne.GetSegNull()) break; //no segment entering through left vertical edge
              segY = seg->YIntersect();
            } while (segY<y);
            if (segY>=y) startSegment = seg;
          }
          else
          { //find first segment entering tolSq below seg
            Segment *segAbove = seg;
            do 
            {
              seg = segAbove;
              segAbove = _sweepLineOne.GetPrev(seg);
              if (!segAbove || segAbove==_sweepLineOne.GetSegNull()) break;
              segY = segAbove->YIntersect();
            } while (segY>=y);
            startSegment = seg;
          }
          if (startSegment)
          { //y value of bottom horizontal segment LOCATED
            float upY = tolSq->y+0.5f;
            //Insert center of tolerance square into all segments entering into tolSq throug left vertical edge
            //go from position of bottom horizSeg in mainList up to position of top horizSeg
            while (startSegment->YIntersect() < upY)
            {
              tolSq->InsertPixelIntoSegment(startSegment); //not implemented yet, only LogF
              startSegment = _sweepLineOne.GetNext(startSegment);
              if (startSegment==_sweepLineOne.GetSegNull() || !startSegment) break;
            }
          }
          //TODO: "insert" tolSq (or its horizontal edges) into mainList (for other STEPs usage)
          // ...
        }
        //else //there is no segment on _sweepLineOne
        lastSq = tolSq.GetRef();
        tolSquareList.Add(tolSq);
        tolSq.SetRef(NULL); //forcing not being deleted when tolSq Ref destroyed
      }
      else
      { //tolerance square, which has more than one event inside
        tolSq.Free();
      }
    }
    //Hobby's STEPS 3,4,5,6
    //TODO
    //for now, only process sweepLine to step towards next batch (which allows testing STEP 2)
    for (int evi=batchStart; evi<evNum; evi++)
    {
      Ref<EventOne> E = (*events)[evi];
      switch (E->type) {
      case EventOne::E_BEGIN:
        {
          if (E->segA->segBefore)
            _sweepLineOne.InsertSegAfter(E->segA, E->segA->segBefore);
          else
            _sweepLineOne.InsertSeg(E->segA);
        }
        break;
      case EventOne::E_END:
        {
          _sweepLineOne.Delete(E->segA);
        }
        break;
      case EventOne::E_INTERSECTION:
        {
          if (E->segB->next==static_cast<Segment::Base *>(E->segA))
            _sweepLineOne.Swap(E->segA,E->segB);
          else Assert(false);
        }
        break;
      }
    }

    //
    //Create output (still, only for Debug purposes)
    //enumerates all tolSquares to show them on output
    //
    for (ToleranceSquare *sq=tolSquareList.Start(); tolSquareList.NotEnd(sq); )
    {
      eventStack->Add(new Event((float)batchX, sq->y));
      ToleranceSquare *sqNext = tolSquareList.Advance(sq);
      tolSquareList.Delete(sq); delete sq; //these were not deleted when Ref tolSq vanished
      sq = sqNext;
    }
    batchStart=evNum;
  }
  
#if _DEBUG
  LogF("*** All Events count = %d", events->Size());
  LogF("*** ToleranceSquare count = %d", eventStack->Size());
#endif
  return eventStack;
}
