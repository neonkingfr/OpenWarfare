// mapa.h : main header file for the MAPA application
//

#if !defined(AFX_MAPA_H__77A26BA0_59CE_48F6_824E_E65EABE8126B__INCLUDED_)
#define AFX_MAPA_H__77A26BA0_59CE_48F6_824E_E65EABE8126B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CMapaApp:
// See mapa.cpp for the implementation of this class
//

class CMapaApp : public CWinApp
{
public:
	CMapaApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMapaApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CMapaApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAPA_H__77A26BA0_59CE_48F6_824E_E65EABE8126B__INCLUDED_)
