// mapaView.cpp : implementation of the CMapaView class
//

#include "stdafx.h"
#include "mapa.h"

#include "mapaDoc.h"
#include "mapaView.h"
#include ".\mapaview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMapaView

IMPLEMENT_DYNCREATE(CMapaView, CView)

BEGIN_MESSAGE_MAP(CMapaView, CView)
	//{{AFX_MSG_MAP(CMapaView)
	ON_WM_KEYDOWN()
	//}}AFX_MSG_MAP
  ON_WM_MOUSEWHEEL()
  ON_WM_VSCROLL()
  ON_WM_HSCROLL()
  ON_WM_CREATE()
  ON_WM_LBUTTONDOWN()
  ON_WM_LBUTTONUP()
  ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMapaView construction/destruction

CMapaView::CMapaView()
{
	// TODO: add construction code here
  scrollBarsInitialized=false;
  screenMoving = false;
  startCursor = ::LoadCursor(NULL, IDC_APPSTARTING);
  handCursor = ::LoadCursor(NULL, IDC_HAND);
}

CMapaView::~CMapaView()
{
}

BOOL CMapaView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
  cs.style |= WS_VSCROLL | WS_HSCROLL;

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CMapaView drawing

void CMapaView::OnDraw(CDC* pDC)
{
	CMapaDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
	
	//screen je inicializovan s NULL, nastavim mu pWin spravne
	pDoc->screen.setPWin(this);
	pDoc->screen.initGraph(pDC);
  if (pDoc->screen.resetAll)
  {
    //compute zoom from bbox
    SFloatRect bbox = pDoc->screen.bbox; bbox.Enlarge(0.1);
    pDoc->screen.zoom = 2*(bbox.bottom - pDoc->screen.y0)/pDoc->screen.ymax;
    saturateMin(pDoc->screen.zoom, (2*(bbox.right - pDoc->screen.x0)/pDoc->screen.xmax));
    pDoc->screen.resetAll=false;
    scrollBarsInitialized=false;
  }
  pDoc->screen.SetActive();
	pDoc->screen.Repaint();
  if (!scrollBarsInitialized)
  {
    scrollBarsInitialized=true;
    UpdateScrollbars();
  }
}

/////////////////////////////////////////////////////////////////////////////
// CMapaView diagnostics

#ifdef _DEBUG
void CMapaView::AssertValid() const
{
	CView::AssertValid();
}

void CMapaView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMapaDoc* CMapaView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMapaDoc)));
	return (CMapaDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMapaView message handlers

void CMapaView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	CMapaDoc* pDoc = GetDocument();
  ASSERT_VALID(pDoc);

	pDoc->screen.OnKeyDown(nChar, nRepCnt, nFlags);

  UpdateScrollbars();
	
	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}

BOOL CMapaView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
  // TODO: Add your message handler code here and/or call default
	CMapaDoc* pDoc = GetDocument();
  ASSERT_VALID(pDoc);

  int ff = zDelta/WHEEL_DELTA;
  if (ff)
  {
    float zoom=1.5;
    if (ff<0) { ff=-ff; zoom=1/1.5f; }

    for (int i=0; i<ff; i++) pDoc->screen.Zoom(zoom);
    CPaintDC pDC(pDoc->screen.getPWin());
    pDoc->screen.initGraph(&pDC);
    pDoc->screen.SetActive();
    pDoc->screen.Repaint();
    pDoc->screen.getPWin()->Invalidate();
  }
  UpdateScrollbars();

  return CView::OnMouseWheel(nFlags, zDelta, pt);
}

void CMapaView::UpdateScrollbars()
{
  CMapaDoc *pDoc = GetDocument();
  CWnd *pwin = pDoc->screen.getPWin();
  CRect clientRect; pwin->GetClientRect(&clientRect);
  int sizx = clientRect.Width();
  int sizy = clientRect.Height();
  //pDoc->screen.ComputeBoundingBox();
  SFloatRect bb = pDoc->screen.GetBoundingBox(); bb.Enlarge(0.1);
  pDoc->screen.SetGlobalBorders(); //set xMin,xMax,yMin,yMax (clipline.h)

  double updFactorX = sizx / (xMax-xMin);
  double updFactorY = sizy / (yMax-yMin);

  SCROLLINFO scr;
  scr.cbSize=sizeof(scr);
  scr.fMask=SIF_ALL;
  scr.nMax=floor((bb.right-bb.left)*updFactorX);
  if (scr.nMax>32000)
  {
    updFactorX=32000/(bb.right-bb.left);
    scr.nMax=32000;
  }
  scr.nMin=0.0;
  int npage = sizx;
  if (npage>scr.nMax) npage=scr.nMax;
  scr.nPage=npage;
  int npos = floor((xMin-bb.left)*updFactorX);
  if (npos<scr.nMin) npos=scr.nMin;
  else if (npos>scr.nMax) npos=scr.nMax;
  scr.nPos=npos;
  SetScrollInfo(SB_HORZ,&scr,TRUE);
  scr.nMax=floor((bb.bottom-bb.top)*updFactorY);
  if (scr.nMax>32000)
  {
    updFactorY=32000/(bb.bottom-bb.top);
    scr.nMax=32000;
  }
  scr.nMin=0.0;
  npage = sizy;
  if (npage>scr.nMax) npage=scr.nMax;
  scr.nPage=npage;
  npos = floor((yMax-bb.top)*updFactorY);
  //npos = floor((yMax-bb.bottom)*updFactorY);
  if (npos>scr.nMax) npos=scr.nMax;
  else if (npos<scr.nMin) npos=scr.nMin;
  scr.nPos=scr.nMax-npos;
  SetScrollInfo(SB_VERT,&scr,TRUE);
}

#include <Es/Framework/debugLog.hpp>
void CMapaView::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
  CMapaDoc *pDoc = GetDocument();
  CWnd *pwin = pDoc->screen.getPWin();
  pDoc->screen.SetGlobalBorders(); //set xMin,xMax,yMin,yMax (clipline.h)
  TCoordinate &x0 = pDoc->screen.x0;
  //LogF("nPos = %ud", nPos);

  switch (nSBCode)
  {
  case SB_LINELEFT: x0 -= (pDoc->screen.xmax/10)*pDoc->screen.zoom; break;
  case SB_LINERIGHT: x0 += (pDoc->screen.xmax/10)*pDoc->screen.zoom; break;
  case SB_LEFT: x0=xMin+(pDoc->screen.xmax/2)*pDoc->screen.zoom; break;
  case SB_RIGHT: x0=xMax-(pDoc->screen.xmax/2)*pDoc->screen.zoom; break;
  case SB_PAGELEFT: x0-=(xMax-xMin)/3;break;
  case SB_PAGERIGHT: x0+=(xMax-xMin)/3;break;
  case SB_THUMBPOSITION:
  case SB_THUMBTRACK: 
    {
      CRect clientRect; pwin->GetClientRect(&clientRect);
      int sizx = clientRect.Width();
      double updFactorX = sizx / (xMax-xMin);
      //int npos = floor((xMin-bb.left)*updFactorX); xMin=x0-(xmax/2)*zoom;  
      SFloatRect bb = pDoc->screen.GetBoundingBox();  bb.Enlarge(0.1);
      int nMax=floor((bb.right-bb.left)*updFactorX);
      if (nMax>32000) updFactorX=32000/(bb.right-bb.left);
      x0= nPos/updFactorX+bb.left+(pDoc->screen.xmax/2)*pDoc->screen.zoom;
    }
    break;
  default:return;
  }
  UpdateScrollbars();
  CPaintDC pDC(pwin);
  pDoc->screen.initGraph(&pDC);
  pDoc->screen.SetActive();
  pDoc->screen.Repaint();
  pwin->Invalidate();

  CWnd::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CMapaView::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
  CMapaDoc *pDoc = GetDocument();
  CWnd *pwin = pDoc->screen.getPWin();
  pDoc->screen.SetGlobalBorders(); //set xMin,xMax,yMin,yMax (clipline.h)
  TCoordinate &y0 = pDoc->screen.y0;

  switch (nSBCode)
  {
  case SB_LINELEFT: 
    y0 += (pDoc->screen.ymax/10)*pDoc->screen.zoom; break;
  case SB_LINERIGHT: 
    y0 -= (pDoc->screen.ymax/10)*pDoc->screen.zoom; break;
  case SB_LEFT: 
    y0=yMin+(pDoc->screen.ymax/2)*pDoc->screen.zoom; break;
  case SB_RIGHT: 
    y0=yMax-(pDoc->screen.ymax/2)*pDoc->screen.zoom; break;
  case SB_PAGELEFT: 
    y0+=(yMax-yMin)/3;break;
  case SB_PAGERIGHT: 
    y0-=(yMax-yMin)/3;break;
  case SB_THUMBPOSITION:
  case SB_THUMBTRACK: 
    {
      CRect clientRect; pwin->GetClientRect(&clientRect);
      int sizy = clientRect.Height();
      double updFactorY = sizy / (yMax-yMin);
      //npos = floor((yMin-bb.top)*updFactorY); yMin=y0-(ymax/2)*zoom;  
      //y0= nPos/updFactorY+pDoc->screen.GetBoundingBox().top+(pDoc->screen.ymax/2)*pDoc->screen.zoom;
      SFloatRect bb = pDoc->screen.GetBoundingBox(); bb.Enlarge(0.1);
      int nMax=floor((bb.bottom-bb.top)*updFactorY);
      if (nMax>32000) updFactorY=32000/(bb.bottom-bb.top);
      int npos = floor((bb.bottom-bb.top)*updFactorY)-nPos;
      y0= npos/updFactorY+bb.top-(pDoc->screen.ymax/2)*pDoc->screen.zoom;
    }
    break;
  default:return;
  }
  UpdateScrollbars();
  CPaintDC pDC(pwin);
  pDoc->screen.initGraph(&pDC);
  pDoc->screen.SetActive();
  pDoc->screen.Repaint();
  pwin->Invalidate();

  CWnd::OnVScroll(nSBCode, nPos, pScrollBar);
}

int CMapaView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
  if (CView::OnCreate(lpCreateStruct) == -1)
    return -1;

  // TODO:  Add your specialized creation code here
  CMapaDoc *pDoc = GetDocument();
  pDoc->screen.setPWin(this);

  return 0;
}

#include "MainFrm.h"
void CMapaView::OnLButtonDown(UINT nFlags, CPoint point)
{
  // TODO: Add your message handler code here and/or call default
  CMainFrame *wnd = (CMainFrame *)AfxGetMainWnd();
  char coordString[128];
  CMapaDoc *pDoc = GetDocument();
  double x = pDoc->screen.InvTranslateX(point.x)/pDoc->cosLatitude;
  double y = pDoc->screen.InvTranslateY(point.y);
  sprintf(coordString, "[%f, %f]", x,y);
  wnd->SetStatusBarText(coordString);

  if (!screenMoving)
  {
    SetCapture();
    screenMoving = true;
    ::SetCursor(handCursor);
  }
  screenMovingBegin = point;

  CView::OnLButtonDown(nFlags, point);
}

void CMapaView::OnLButtonUp(UINT nFlags, CPoint point)
{
  // TODO: Add your message handler code here and/or call default
  if (screenMoving)
  {
    ::SetCursor(startCursor);
    screenMoving = false;
  }
  ReleaseCapture();

  CView::OnLButtonUp(nFlags, point);
}

void CMapaView::OnMouseMove(UINT nFlags, CPoint point)
{
  if (screenMoving)
  {
    int maxCoordDiff = max(abs(point.x-screenMovingBegin.x), abs(point.y-screenMovingBegin.y));
    if (maxCoordDiff > 50)
    {
      CMapaDoc *pDoc = GetDocument();
      CWnd *pwin = pDoc->screen.getPWin();
      pDoc->screen.SetGlobalBorders(); //set xMin,xMax,yMin,yMax (clipline.h)
      TCoordinate &y0 = pDoc->screen.y0;
      TCoordinate &x0 = pDoc->screen.x0;

      y0 += (point.y-screenMovingBegin.y)*pDoc->screen.zoom;
      x0 -= (point.x-screenMovingBegin.x)*pDoc->screen.zoom;
      
      screenMovingBegin = point;

      UpdateScrollbars();
      CPaintDC pDC(pwin);
      pDoc->screen.initGraph(&pDC);
      pDoc->screen.SetActive();
      pDoc->screen.Repaint();
      pwin->Invalidate();
    }
  }
  CView::OnMouseMove(nFlags, point);
}
