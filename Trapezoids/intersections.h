#ifndef INTERSECTION_H
#define INTERSECTION_H

/************************************************************************/
/*                                                                      */
/* Practical Segment Intersection with Finite Precision Output          */
/*    based on John D. Hobby article (Bell Laboratories)                */
/*                                                                      */
/************************************************************************/


#include <Es/Types/pointers.hpp>
#include <Es/Containers/listBidir.hpp>

struct Point2DFloat : public RefCount
{
  float x,y;
  Point2DFloat(){}
  Point2DFloat(float xx, float yy):x(xx),y(yy){}

  bool operator<(Point2DFloat &pt)
  { //lexicographic order
    if (x == pt.x)
    {
      return (y < pt.y);
    }
    else return (x<pt.x);
  }
  bool operator==(Point2DFloat &pt)
  {
    return  ((x==pt.x) && (y==pt.y));
  }
};
TypeIsSimple(Point2DFloat);

typedef union {  
  struct {    
    __int16 LowPart;    
    __int16 HighPart;  
  };  
  __int32 QuadPart;
} INTEGER32;

class SweepLineOne; 
class Segment;
struct EventOne;

class ToleranceSquare;
class HorizontalSegment : public TLinkBidirD, public RefCount
{
public:
  float y; //some int +- 1/2
  Segment *segBelow;
  ToleranceSquare *up, *down;

  HorizontalSegment(float y1) : y(y1) {segBelow=NULL; up=down=NULL;}

  bool operator<(const HorizontalSegment &hseg) const {return y<hseg.y; }
  bool operator==(const HorizontalSegment &hseg) const {return y==hseg.y; }
};
TypeIsSimple(HorizontalSegment);

class ToleranceSquare : public TLinkBidirD, public RefCount
{
public:
  float y; //but int value in fact
  Ref<HorizontalSegment> bottom, top;
  EventOne *event; //responsible event
  Segment  *referenceSegment; //segment derived from event to locate bottom horizonatal segment

  ToleranceSquare(float y1) : y(y1) {}
  void FindReferenceSegment();
  bool operator<(const ToleranceSquare &sq) const {return y<sq.y; }
  bool operator==(const ToleranceSquare &sq) const {return y==sq.y; }
  void InsertPixelIntoSegment(Segment *seg);
};
TypeIsSimple(ToleranceSquare);

///Segment
class Segment : public TLinkBidirD, public RefCount
{
private:
  friend class SweepLineOne;
  friend class Intersections;
  ///used for evaluating operator< (order of intersections with line x==_sweepLineX)
  static float _sweepLineX;
  static int _globalId;
  /*BinTree<Ref<Segment> >::BinTreeNode*/ //TODO: void * is dirty
  //void *binTreeNode;
  ///id (needed for comparision of overlapping segments (the same endpoints))
  INTEGER32 id;
  ///orientation of edge: true for polygone's body being to the right
  bool orientation;
  ///true iff line is vertical
  bool vertical;
  ///tangens (y2-y1)/(x2-x1) for not vertical line
  float dydx;  ///color of polygon containing this edge
  int color;
  void CopyAttributes(Segment *seg) 
  {
    id.HighPart=seg->id.HighPart; orientation=seg->orientation; color=seg->color;
  }
public:
  typedef TLinkBidirD Base;
  typedef Segment & KeyType;
  KeyType GetKey() {return *this; }
  ///leftmost and rightmost points (in the sense of EventOne::operator< ordering)
  Point2DFloat leftP, rightP;
  ///segment on sweepline, after which this segment was inserted
  Segment *segBefore;
  HorizontalSegment *hsegAbove;
  ///constructor compute also variables vertical and dydx
  Segment(Point2DFloat &ptLeft, Point2DFloat &ptRight);
  Segment() : segBefore(NULL) {}

  /////////////////////////////////////////////////////////////////////////////
  ///y coordinate of intersection with vertical line x=x
  float YIntersect(float x=_sweepLineX);
  ///order by increasing slope
  bool IsLessBySlope(Segment &seg2);
  ///order by intersection with line x=_sweepLineX and increasing slope
  bool operator<(Segment &seg2);
  ///true only for two identical segments (the very same endpoints)
/*
  bool operator==(Segment &seg2);
  ///true if segments has non-simple intersection
*/
  ///gets the intersection point or return false, when no such point
  ///it suppose, seg2 is under this segment on sweepLine and _sweepLineX is set (for validity testing)
  bool IntersectLowerSegment(Segment* seg2, Point2DFloat &intPt);
  ///returns if vertical
  bool IsVertical() {return vertical;}
  void SetOrientation(bool ori) {orientation=ori;}
  void SetPolyId(int polyId) {id.HighPart=(__int16)polyId;};
  void SetColor(int col) {color=col;}
  int  GetColor() {return color;}
  INTEGER32 GetId() {return id; }
};

#include <el/ParamArchive/paramArchiveDb.hpp>
#include <el/ParamArchive/paramArchive.hpp>

#define CHECK_INTERNAL(command) \
{LSError err = command; if (err != LSOK) return err;}

class ConvexComponent : public SerializeClass
{
public:
  int color;
  AutoArray<float> polygon; //x1,y1,x2,y2,x3,y3,...

  LSError Serialize(ParamArchive &ar)
  {
    ar.Serialize("color",color,1,1);
    CHECK_INTERNAL(ar.SerializeArray("polygon",polygon,1));
    return LSOK;
  }
};
TypeIsMovable(ConvexComponent);

class PathComponent : public SerializeClass
{
public:
  int type;
  AutoArray<float> path; //x1,y1,x2,y2,x3,y3,...

  LSError Serialize(ParamArchive &ar)
  {
    ar.Serialize("type",type,1,1);
    CHECK_INTERNAL(ar.SerializeArray("path",path,1));
    return LSOK;
  }
};
TypeIsMovable(PathComponent);

///the Sweep Line itself
class SweepLineOne 
{
private:
  //BinTree<Ref<Segment> > _segTree;  //for less than 100 items on SweepLineOne, double linked list is possibly better
  Segment *_segNull;
  TListBidir<Segment> segList;
public:
#if _DEBUG
  int count;
  int maxCount;
#endif
  SweepLineOne() {
    _segNull=new Segment; segList.Add(_segNull); //null segment as border
#if _DEBUG
    count=maxCount=0;
#endif
  } 
  ~SweepLineOne() {delete _segNull;}
  ///returns segment the seg was inserted before
  Segment *Add(Segment *seg);
  void InsertSegAfter(Segment *seg, Segment *before);
  void InsertSeg(Segment *seg);
  void Delete(Segment *seg);
  void Swap(Segment *segA, Segment *segB);
  Segment *GetPrev(Segment *seg); //NULL if no such
  Segment *GetNext(Segment *seg); //NULL if no such
  Segment *GetFirst();
  Segment *GetSegNull() { return _segNull; }
#if _DEBUG
  void LogDebugInfo();
#endif
};

///EventOne class for the SweepLineOne algorithm 
struct EventOne : public RefCount
{
  enum EventOneType {E_END, E_INTERSECTION, E_BEGIN} type;
  ///EventOne location
  Point2DFloat point;
  ///EventOne segments
  ///in case of E_INTERSECTION, both segments are used, or segA only
  Ref<Segment> segA, segB; //segA was above segB when event was scheduled

  EventOne() {}
  ///constructor only for E_BEGIN and E_END events
  EventOne(Segment *seg, EventOneType _type) : type(_type), segA(seg) 
  {
    point= ((_type==E_BEGIN) ? seg->leftP : seg->rightP);
  } 
  ///constructor only for E_INTERSECTION event
  EventOne(Point2DFloat intPt, Ref<Segment> _segA, Ref<Segment> _segB) : point(intPt), type(E_INTERSECTION), segA(_segA), segB(_segB) {}

  ///needed for inserting into priority queue (heapArray)
  bool operator<(EventOne &evnt)
  { //lexicographic order
    if (point.x==evnt.point.x)
    {
      if (point.y==evnt.point.y)
        return (type<evnt.type); //J.D.Hobby: in case of a tie return in order End, Intersection, Begin {due to infinitesimal shortening rule)
      else
        return (point.y<evnt.point.y);
    }
    else return (point.x<evnt.point.x);
  }
};

#include <Es/Containers/array.hpp>
#include <AITools/BinTree/BinTree.h>

class IntValue
{
  int val;
public:
  typedef int KeyType;
  KeyType GetKey() {return val;};

  IntValue(int val1=0) : val(val1) {}
  operator const IntValue &() {return *this; }
  bool operator<(const IntValue &b) const
  {
    return (val<b.val);
  }
  bool operator==(const IntValue &b) const
  {
    return (val==b.val);
  }
};

class EventOneQueue
{
friend class Intersections;
private:
  ///the priority queue
  HeapArray<Ref<EventOne> > _heap;
  ///tree to find existing intersection point events
  BinTree<IntValue> _tree; //inside are IntersectionCode(seg1, seg2) values
public:
  EventOneQueue() {}
  void QueueInsert(Ref<EventOne> EventOne)
  {
    _heap.HeapInsert(EventOne);
  }
  bool QueueGetFirst(Ref<EventOne> &EventOne)
  {
    return _heap.HeapGetFirst(EventOne);
  }
  bool QueueRemoveFirst(Ref<EventOne> &EventOne)
  {
    return (_heap.HeapRemoveFirst(EventOne));
  }
  bool IsEmpty() {return (_heap.Size()==0);}
  int IntersectionCode(Segment *seg1, Segment *seg2);
#if _DEBUG
  int Size() {return _heap.Size(); }
#endif
};

struct Event;
class Intersections
{
private:
  int _polyNum;
protected:
  EventOneQueue _eventQ;
  SweepLineOne  _sweepLineOne;
  EventOne *AddEventOne(Segment *seg, EventOne::EventOneType type);
public:
  Intersections() { _polyNum=0; }
  ///add new segment (x1<x2 supposed)
  void Add(float x1, float y1, float x2, float y2, int color=0, int polyId=0);
  void AddPolygon(AutoArray<Point2DFloat> &poly, int color=0);

  AutoArray<Point2DFloat> SnapRound();
  ///find all intersections (robust)
  AutoArray<Ref<EventOne> > *FirstPass(); //Bentley-Ottmann Sweep Line Algorithm
  ///use first pass preprocessing to Snap Round the segments
  AutoArray<Ref<Event> > *SecondPass(AutoArray<Ref<EventOne> > *events); //J.D.Hobby's Snap Round Algorithm
};

#endif
