#ifndef TRAPEZOID_H
#define TRAPEZOID_H

/************************************************************************/
/*                                                                      */
/* Trapezoidal decomposition based on SweepLine algorithm               */
/*                                                                      */
/************************************************************************/
#include "intersections.h"

template <class Type>
class PriorityList
{
public:
  PriorityList *left,*right;
  Type key;

  PriorityList(Type val) {key=val;left=right=NULL;}
  //returns the new head. It must be called on head instance.
  PriorityList *Insert(Type val);
  //
  PriorityList *Delete(Type val);
};

template <class Type>
PriorityList<Type> *PriorityList<Type>::Insert(Type val)
{
  if (key<val)
  {
    PriorityList<Type> *newItem = new PriorityList<Type>(val);
    newItem->right=this;
    newItem->left=NULL;
    left=newItem;
    return newItem;
  }
  else
  {
    PriorityList<Type> *item=this;
    while (item->right && val<item->right->key) item=item->right;
    PriorityList<Type> *newItem = new PriorityList<Type>(val);
    newItem->left=item;
    newItem->right=item->right;
    item->right=newItem;
    if (newItem->right) newItem->right->left=newItem;
    return this;
  }
}

template <class Type>
PriorityList<Type> *PriorityList<Type>::Delete(Type val)
{
  PriorityList<Type> *item=this;
  while (item && !(item->key==val)) item=item->right;
  if (item) 
  {
    PriorityList<Type> *retVal= ((item==this) ? item->right : this);
    if (item->right) item->right->left=item->left;
    if (item->left) item->left->right=item->right;
    delete item;
    return retVal;
  }
  return this;
}

#include <Es/Framework/debugLog.hpp>
#include <Es/Containers/typeOpts.hpp>
#include <Es/Common/fltopts.hpp>
#include <Es/Types/pointers.hpp>
#include <ctype.h>

#include <AITools/BinTree/BinTree.h>

const float TRAP_INFTY = 1e8;

//
// Floating point helper functions
//
const float ZeroPointEPS = 1e-2f;   //1cm
const float ZeroPoint2EPS = 1e-6f;  //the smaller the better?!

struct Point2DRound;
bool IsEqual(const float &a, const float &b);
extern bool IsLess(float &a, float &b);
extern bool IsZero(float a);
extern int FloatSgn(float a);
extern int IsLeft( Ref<Point2DRound> P0, Ref<Point2DRound> P1, Ref<Point2DRound> P2 );
extern bool ZeroPoint(Point2DRound point);

struct Point2DRound : public RefCount
{
  float x,y;
  Point2DRound(){}
  Point2DRound(float xx, float yy):x(xx),y(yy){}

  bool operator<(Point2DRound &pt) //A==B==C but A<C can hold due to intransitiveness (IsZero)
  { //lexicographic order
    if (IsZero(x - pt.x))
    {
      return IsLess(y, pt.y);
    }
    else return (x<pt.x);
  }
  bool operator==(Point2DRound &pt) //it is not transitive!
  {
    return  (IsZero(x - pt.x) && IsZero(y - pt.y));
  }
};
TypeIsSimple(Point2DRound);


template <class Container> class TrapezoidalDecomposition;

struct Trapezoid;
typedef Trapezoid * TrapezoidPtr;
TypeIsSimple(TrapezoidPtr);


///Trapezoid with vertices
///(x1,y1), (x1,y2), (x2,y2), (x2,y1)
///assumed in clockwise order, with x1<=x2
struct Trapezoid : public RefCount
{
  float y1,y2,y3,y4;
  float x1,x2;
  float sortval;
  //Graph of neigbouring trapezoids
  //edges through vertical "transparent" lines
  Trapezoid *leftTr1,*leftTr2,*rightTr1,*rightTr2;
  //edges through segments
  AutoArray<TrapezoidPtr > edgesUp, edgesDown;
  int color;
  bool merged, deleted;

  Trapezoid() { 
    leftTr1=leftTr2=rightTr1=rightTr2=NULL; 
    merged=deleted=false;
    color=0;
  }
  //TODO: add some ordering to store it in binary tree etc. //this is stupid, as it doesn't help to localise point. Some quadtree should be used instead.
  void SetSortVal() { sortval = y1+y3-y2+y4+x1-x2; }
  bool operator<(Trapezoid &tr)
  {
    return (sortval < tr.sortval);
  }
  bool operator==(Trapezoid &tr)
  {
    return (IsEqual(y1,tr.y1) && IsEqual(y2,tr.y2) && IsEqual(y3,tr.y3) && IsEqual(y4,tr.y4) && IsEqual(x1,tr.x1) && IsEqual(x2,tr.x2));
  }
};
TypeIsMovable(Trapezoid);

class SweepLine;
struct Event;

#include <Es/Containers/listBidir.hpp>
///SweepLineSegment
class SweepLineSegment : public TLinkBidirD, public TLinkBidir<1>, public RefCount
{
private:
  template <class Container> friend class TrapezoidalDecomposition;
  friend class SweepLine;
  ///used for evaluating operator< (order of intersections with line x==sweepLineX)
  static float _sweepLineX;
  static int _globalId;
  /*BinTree<Ref<SweepLineSegment> >::BinTreeNode*/ //TODO: void * is dirty
  void *binTreeNode;
  ///true iff line is vertical
  bool vertical;
  ///tangens (y2-y1)/(x2-x1) for not vertical line
  float dydx;
  ///id (needed for comparition of overlapping segments (the same endpoints))
  INTEGER32 id;
  ///orientation of edge: true for polygone's body being to the right
  bool orientation;
  ///color of polygon containing this edge
  int color;
  void CopyAttributes(SweepLineSegment *seg) {
    id.HighPart=seg->id.HighPart; orientation=seg->orientation; color=seg->color;
  }
public:
  typedef SweepLineSegment & KeyType;
  KeyType GetKey() {return *this; }
  ///leftmost and rightmost points (in the sense of Event::operator< ordering)
  Ref<Point2DRound> leftP, rightP;
  ///segments above and below this one on SweepLine
  SweepLineSegment *above, *below;
  ///trapezoidal stubs above and below this sweepLine
  Trapezoid *trapAbove, *trapBelow;
  ///default constructor
  SweepLineSegment() {leftEvent=NULL;}
#if _DEBUG
  ~SweepLineSegment() 
  {
    if ((int)this==0x0101111)
      _asm nop;
  }
#endif
  ///this constructor compute also variables vertical and dydx
  SweepLineSegment(float x1, float y1, float x2, float y2); //TODO: this shouldn't be used
  ///constructor compute also variables vertical and dydx
  SweepLineSegment(Ref<Point2DRound> from, Ref<Point2DRound> to);
  /////////////////////////////////////////////////////////////////////////////
  SweepLineSegment(SweepLineSegment &seg);
//  SweepLineSegment(Ref<Event> from, Ref<Event> to);
  ///y coordinate of intersection with vertical line x=x
  float YIntersect(float x=_sweepLineX);
  ///order by intersection with line x=_sweepLineX and increasing slope
  bool operator<(SweepLineSegment &seg2);
  ///order by increasing slope
  bool IsLessBySlope(SweepLineSegment &seg2, bool behindSweepLine=false);
  ///true only for two identical segments (the very same endpoints)
  bool operator==(SweepLineSegment &seg2);
  ///true if segments has non-simple intersection
  bool Intersect( SweepLineSegment* seg2);
  ///gets the intersection point
  Point2DRound IntersectionPoint(SweepLineSegment *seg2);
  ///returns if vertical
  bool IsVertical() {return vertical;}
  ///get dydx
  const float &Getdydx() {return dydx; }
  ///set the color
  ///final color of interval on sweepLine below this segment (being on sweepLine)
  int finalColor;
  ///true, if this segment is true segment on sweepLine (not shadowed with higher priority one)
  bool visible;
  ///helper pointer to create eventQueue for the second pass decompose
  Event *leftEvent;
  void SetColor(int col) {color=col;}
  int  GetColor() {return color;}
  void SetOrientation(bool ori) {orientation=ori;}
  void SetPolyId(int polyId) {id.HighPart=(__int16)polyId;};
};

extern bool SegmentContainsPoint(SweepLineSegment *segA, Ref<Point2DRound> point);
extern bool PointIsOnLine(int pointX, int pointY, int x1, int y1, int x2, int y2);
extern void MergeEdges(AutoArray<TrapezoidPtr > &edges1, AutoArray<TrapezoidPtr > &edges2);

///the Sweep Line itself
class SweepLine 
{
public:
  BinTree<Ref<SweepLineSegment> > _segTree;

  Trapezoid *unboundedTrap;
  Trapezoid *ltTr,*lbTr,*rtTr,*rbTr; //leftTopMostTrap,leftBottomMostTrap,rightTopMostTrap,rightBottomMostTrap

  SweepLine() { ltTr=lbTr=rtTr=rbTr=NULL; }
  ~SweepLine(void) {}

  //it also updates above/below SweepLineSegment pointers
  void Add(SweepLineSegment *seg, bool handleTrapezoids=true);
  //it also updates above/below SweepLineSegment pointers
  void Delete(SweepLineSegment *seg);
  SweepLineSegment *GetFirstLessOrEqual(SweepLineSegment *seg);
  SweepLineSegment *GetFirstGreaterOrEqual(SweepLineSegment *seg);
  void AddTrapezoidsToGraph();
  void ResetTrapezoids() {ltTr=lbTr=rtTr=rbTr=NULL;}
};

///Event class for the SweepLine algorithm 
struct Event : public RefCount
{
  ///event location
  Ref<Point2DRound> point;
  ///list of incident edges with right endpoint at this event
  TListBidir<SweepLineSegment, TLinkBidir<0> > edgeListL;
  ///list of incident edges with left endpoint at this event
  TListBidir<SweepLineSegment, TLinkBidir<1> > edgeListR;

  ///KeyType and GetKey() for BinTreeKeyTraits
  typedef Point2DRound & KeyType;
  KeyType GetKey() { return *point.GetRef(); }

  Event() {}
  Event(float x, float y) 
  {
    point = new Point2DRound(x, y);
  }
  ///needed for testing, whether Event is inserted into priority queue yet
  bool operator==(Event &evnt) {return point==evnt.point; /*ZeroPoint(*point-*evnt.point);*/ }
  ///needed for inserting into priority queue (heapArray)
  bool operator<(Event &evnt)
  { //lexicographic order
    if (IsZero(point->x - evnt.point->x))
    {
      return IsLess(point->y, evnt.point->y);
    }
    else return (point->x<evnt.point->x);
  }
  ///add edge to the edgeListL
  void AddEdgeL(SweepLineSegment *seg);
  ///add edge to the edgeListR
  void AddEdgeR(SweepLineSegment *seg);
};

#include <Es/Containers/array.hpp>
class EventQueue
{
private:
  ///the priority queue
  HeapArray<Ref<Event> > _heap;
  ///for testing, if event is inserted yet
  BinTree<Ref<Event> > _tree;
public:
  EventQueue() {}
  void QueueInsert(Ref<Event> event)
  {
    if ((int)this==0x0166BF58)
      _asm nop;
    _heap.HeapInsert(event);
    _tree.Insert(event);
  }
  bool QueueGetFirst(Ref<Event> &event)
  {
    return _heap.HeapGetFirst(event);
  }
  bool QueueRemoveFirst(Ref<Event> &event)
  {
    if (_heap.HeapRemoveFirst(event))
    {
      _tree.Delete(event);
      return true;
    }
    return false; //empty
  }
  /*Ref<Event>*/Event *QueueFind(Event *event) const
  {
    return *_tree.Find(event);
  }
  /*Ref<Event>*/Event *QueueFind(Point2DRound &pt) const
  {
    const Ref<Event> *evptr = _tree.FindKey(pt);
    if (evptr) return *evptr;
    else return NULL;
  }
  bool IsEmpty() {return (_heap.Size()==0);}
#if _DEBUG
  int Size() {return _heap.Size(); }
#endif
};

#include <el/ParamArchive/paramArchiveDb.hpp>
#include <el/ParamArchive/paramArchive.hpp>

#define CHECK_INTERNAL(command) \
{LSError err = command; if (err != LSOK) return err;}

///TrapezoidalDecomposition
/**
  \param Container the container class that must have the ability to store Ref<Trapezoid> class.
*/
template <class Container>
class TrapezoidalDecomposition : public SerializeClass
{
private:
  int _polyNum;
protected:
  EventQueue _eventQ;
  SweepLine  _sweepLine;
  ///if event with coordinates x,y exists yet, return the existing one, or create new event othervise
  Ref<Event> AddEvent(float x, float y);

  void HandleIntersection(SweepLineSegment *segA,SweepLineSegment *segB);
  void AddIntersectionEvent(SweepLineSegment *segA,SweepLineSegment *segB, Point2DRound &intPt);
  ///T-Shape improper intersection maintenance
  void AddImproperIntersectionEvent(SweepLineSegment *seg, Ref<Event> event);
  void CreateTrapLeft(SweepLineSegment *segE,SweepLineSegment *segA);
  void CreateTrapAbove(SweepLineSegment *segE,SweepLineSegment *segA);
  void CreateTrapBelow(SweepLineSegment *segE,SweepLineSegment *segA);
  void CreateTrapBoth(SweepLineSegment *segE,SweepLineSegment *segA, float x);
public:
  Container trapezoids;

  TrapezoidalDecomposition(AutoArray<ConvexComponent> &cComp, AutoArray<PathComponent> &pComp) : convexComponents(cComp), pathComponents(pComp) { _polyNum=0; }
  ///add new segment (x1<x2 supposed)
  void Add(float x1, float y1, float x2, float y2, int color=0, int polyId=0);
  void AddPolygon(AutoArray<Point2DRound> &poly, int color=0);

  ///fill in the trapezoids container with trapezoidal decomposition of Added lines
  ///return true if successfull
  bool Decompose();
  ///decompose using two passes (colors and polygon intersection handling)
  bool TwoPassDecompose();
  ///fill in the _eventQ2 with not intersecting segments, each with final color set
  AutoArray<Ref<Event> > *FirstPass();
  ///merging all neighboring trapezoids into larger one
  void MergeTrapezoids();
  ///merge neighboring trapezoids of this one specified into larger one
  void MergeTrapezoid(Trapezoid *trap);

  LSError Serialize(ParamArchive &ar);
  AutoArray<ConvexComponent> &convexComponents;
  AutoArray<PathComponent> &pathComponents;
  //bool SerializeConvexComponent(int ix, AutoArray<Point2DRound> &polygon; ParamArchive &asect);
};

template <class Container>
void TrapezoidalDecomposition<Container>::HandleIntersection(SweepLineSegment *segA,SweepLineSegment *segB)
{
  //get intersection point
  Point2DRound intPt;
  do 
  {
    if (segA==NULL || segB==NULL) return; // no intersect of not existing segments
    // check for segments intersecting at their endpoints
    // but they CANNOT be consecutive A-->B-->C
    Assert(segA->leftP!=segB->rightP && segA->rightP!=segB->leftP);
    if (segA->leftP==segB->leftP)
    {
      if (SegmentContainsPoint(segB, segA->rightP)) {intPt = *segA->rightP; break; }
      return; //no intersection
    }
    if (segA->rightP==segB->rightP)
    {
      if (SegmentContainsPoint(segB, segA->leftP)) {intPt = *segA->leftP; break; }
      return; //no intersection
    }

    int lBsign = IsLeft(segA->leftP, segA->rightP, segB->leftP); // seg2 left point sign
    if (!lBsign)
    {
      if ((segB->leftP->x >= segA->leftP->x) && (segB->leftP->x <= segA->rightP->x)) {intPt = *segB->leftP; break; }
    }
    int rBsign = IsLeft(segA->leftP, segA->rightP, segB->rightP); // s2 right point sign
    if (!rBsign)
    {
      if ((segB->rightP->x >= segA->leftP->x) && (segB->rightP->x <= segA->rightP->x)) {intPt = *segB->rightP; break; }
    }
    if (lBsign * rBsign > 0) // s2 endpoints have same sign relative to s1
      return; // => on same side => no intersect is possible
    int lAsign = IsLeft(segB->leftP, segB->rightP, segA->leftP); // s1 left point sign
    if (!lAsign)
    {
      if ((segA->leftP->x >= segB->leftP->x) && (segA->leftP->x <= segB->rightP->x)) {intPt = *segA->leftP; break; }
    }
    int rAsign = IsLeft(segB->leftP, segB->rightP, segA->rightP); // s1 right point sign
    if (!rAsign)
    {
      if ((segA->rightP->x >= segB->leftP->x) && (segA->rightP->x <= segB->rightP->x)) {intPt = *segA->rightP; break; }
    }
    if (lAsign * rAsign > 0) // s1 endpoints have same sign relative to s2
      return; // => on same side => no intersect is possible
    // the segments straddle each other => an intersect exists

    //determine intPt using Cramer rule
    //Ax+By=C
    float A1 = segA->rightP->y-segA->leftP->y;
    float B1 = segA->leftP->x-segA->rightP->x;
    float C1 = A1*segA->leftP->x+B1*segA->leftP->y;
    float A2 = segB->rightP->y-segB->leftP->y;
    float B2 = segB->leftP->x-segB->rightP->x;
    float C2 = A2*segB->leftP->x+B2*segB->leftP->y;
    //solve the two equations for the two unknowns, x and y
    //A1x + B1y = C1
    //A2x + B2y = C2
    float det = A1*B2 - A2*B1;
    if (!IsZero(det))
    {
      intPt=Point2DRound((B2*C1 - B1*C2)/det, (A1*C2 - A2*C1)/det);
      if ( intPt.x<=segA->rightP->x && intPt.x<=segB->rightP->x 
        && intPt.x>=segA->leftP->x && intPt.x>=segB->leftP->x) break;
      //else intersection lies outside of specified range
    }
    return;   //no intersection found
  } while (false);
  //use just determined intPt to add intersection event
  if (intPt.x>=SweepLineSegment::_sweepLineX) //it must be checked, really seldom fails
    AddIntersectionEvent(segA, segB, intPt);
}

template <class Container>
void TrapezoidalDecomposition<Container>::AddIntersectionEvent(SweepLineSegment *segA,SweepLineSegment *segB, Point2DRound &intPt)
{
  //segA and segB are inside edgeListL of their right endpoints, delete them from these lists
  Ref<Event> eventA = _eventQ.QueueFind(*segA->rightP);
  Assert(eventA.NotNull());
  Ref<Event> eventB = _eventQ.QueueFind(*segB->rightP);
  Assert(eventB.NotNull());
  Assert((eventA->point->x==segA->rightP->x) && (eventA->point->y==segA->rightP->y));
  Assert((eventB->point->x==segB->rightP->x) && (eventB->point->y==segB->rightP->y));
  //both segA and segB are not NULL
  if ((*eventA->point==intPt) && (*eventB->point==intPt))
    return;
  Ref<Event> intPtEv = AddEvent(intPt.x, intPt.y);
  if (eventA!=intPtEv) //or improper intersection else
  {
    //segA is inside sweepLine structure, so shorten this segment and create second halve
    eventA->edgeListL.Delete(segA);
    SweepLineSegment * segA2 = new SweepLineSegment(intPtEv->point, segA->rightP);
    segA2->CopyAttributes(segA);
    eventA->edgeListL.Add(segA2);
    segA->rightP = intPtEv->point; //shorten first halves
    //TODO: dydx could have changed
    //update intPtEv edge lists
    intPtEv->AddEdgeL(segA);
    intPtEv->AddEdgeR(segA2);// segA2->leftEvent=intPtEv;
    segA2->CopyAttributes(segA);
  }
  if (eventB!=intPtEv) 
  {
    eventB->edgeListL.Delete(segB);
    SweepLineSegment * segB2 = new SweepLineSegment(intPtEv->point, segB->rightP);
    segB2->CopyAttributes(segB);
    eventB->edgeListL.Add(segB2);
    segB->rightP = intPtEv->point;
    //TODO: dydx could have changed
    intPtEv->AddEdgeL(segB);
    intPtEv->AddEdgeR(segB2);// segB2->leftEvent=intPtEv;
    segB2->CopyAttributes(segB);
  }
}

//T-Shape improper intersection maintenance: segT->leftP is inside seg segment
template <class Container>
void TrapezoidalDecomposition<Container>::AddImproperIntersectionEvent(SweepLineSegment *seg, Ref<Event> event)
{
  float intY = seg->YIntersect(event->point->x);
  Assert(fabs(intY-event->point->y)<ZeroPointEPS);
  event->point->y = intY;
  SweepLineSegment * seg2 = new SweepLineSegment(event->point, seg->rightP);
  seg2->CopyAttributes(seg);
  Event *event2 = _eventQ.QueueFind(*seg->rightP);
  Assert(event2);
  Assert((event2->point->x==seg->rightP->x) && (event2->point->y==seg->rightP->y));
  event2->edgeListL.Delete(seg);
  event2->edgeListL.Add(seg2);
  seg->rightP = event->point; //shorten first halve
  //update intPtEv edge lists
  event->AddEdgeL(seg);
  event->AddEdgeR(seg2);
  _eventQ.QueueInsert(event);
  seg2->CopyAttributes(seg);
}

template <class Container>
void TrapezoidalDecomposition<Container>::CreateTrapAbove(SweepLineSegment *segE,SweepLineSegment *segA)
{
  Ref<Trapezoid> trap = segE->trapAbove;
  _sweepLine.ltTr = segE->trapAbove;
  //trap->x1 is already set
  trap->x2 = segE->rightP->x;
  trap->y1 = segE->YIntersect(trap->x1);
  trap->y4 = segE->rightP->y;
  if (segA)
  {
    trap->y2 = segA->YIntersect(trap->x1);
    trap->y3 = segA->YIntersect(trap->x2);
    trap->color=segA->finalColor;
    //update will be done in Decompose method
  }
  else
  {
    trap->y2 = trap->y3 = TRAP_INFTY;
    trap->color=0;
  }
  //add trapezoid to the container
  trap->SetSortVal();
  trapezoids.Add(trap);
}
template <class Container>
void TrapezoidalDecomposition<Container>::CreateTrapBelow(SweepLineSegment *segE,SweepLineSegment *segB)
{
  Ref<Trapezoid> trap = segE->trapBelow;
  _sweepLine.lbTr = segE->trapBelow;
  //trap->x1 is already set
  trap->x2 = segE->rightP->x;
  trap->y2 = segE->YIntersect(trap->x1);
  trap->y3 = segE->rightP->y;
  if (segB)
  {
    trap->y1 = segB->YIntersect(trap->x1);
    trap->y4 = segB->YIntersect(trap->x2);
    if (segB->above) {
      SweepLineSegment *segA=segB->above;
      trap->color=segA->finalColor;
    }
    //update will be done in Decompose method
  }
  else
  {
    trap->y1 = trap->y4 = -TRAP_INFTY;
    trap->color=0;
  }
  //add trapezoid to the container
  trap->SetSortVal();
  trapezoids.Add(trap);
}
//both segB and segA can be NULL
template <class Container>
void TrapezoidalDecomposition<Container>::CreateTrapBoth(SweepLineSegment *segB,SweepLineSegment *segA, float x)
{
  Ref<Trapezoid> trap = NULL; 
  if (segB) trap = segB->trapAbove;
  else if (segA) trap = segA->trapBelow;
  else if (_sweepLine.unboundedTrap) { 
    trap = _sweepLine.unboundedTrap;
    _sweepLine.unboundedTrap = NULL;
  } else {
    Fail("No unboundedTrap created prepered!");
  }
  _sweepLine.ltTr=_sweepLine.lbTr=trap.GetRef();
  if (segA)
  {
    trap->y2 = segA->YIntersect(trap->x1);
    trap->y3 = segA->YIntersect(x);
    //update
    Trapezoid *trB = new Trapezoid;
    trB->x1 = x;
    segA->trapBelow = trB;
    //update up and down edges
    int size = trap->edgesUp.Size();
    Assert(size);
    Trapezoid *rightMostTr = trap->edgesUp[size-1];
    trB->edgesUp.Add(rightMostTr); //they share this trapezoid
    rightMostTr->edgesDown.Add(trB);
  }
  else
  {
    trap->y2 = trap->y3 = TRAP_INFTY;
  }
  if (segB)
  {
    trap->y1 = segB->YIntersect(trap->x1);
    trap->y4 = segB->YIntersect(x);
    //update
    Trapezoid *trA = segA ? segA->trapBelow : new Trapezoid;
    trA->x1 = x;
    segB->trapAbove = trA;
    //update up and down edges
    int size = trap->edgesDown.Size();
    Assert(size);
    Trapezoid *rightMostTr = trap->edgesDown[size-1];
    trA->edgesDown.Add(rightMostTr); //they share this trapezoid
    rightMostTr->edgesUp.Add(trA);
  }
  else 
  {
    trap->y1 = trap->y4 = -TRAP_INFTY;
  }
  trap->x2 = x;
  //add trapezoid to the container
  trap->SetSortVal();
  if (segA) {
    trap->color = segA->finalColor;
  }
  else trap->color=0;
  trapezoids.Add(trap);
}
//both segE and segB are not NULL
template <class Container>
void TrapezoidalDecomposition<Container>::CreateTrapLeft(SweepLineSegment *segE,SweepLineSegment *segB)
{
  Ref<Trapezoid> trap = segB->trapAbove;
  //trap->x1 is already set
  trap->y1 = segB->YIntersect(trap->x1);
  trap->y2 = segE->YIntersect(trap->x1);
  trap->x2 = segE->rightP->x;
  trap->y3 = trap->y4 = segE->rightP->y;
  //add trapezoid to the container
  trap->SetSortVal();
  if (segB->above) {
    SweepLineSegment *segA=segB->above;
    trap->color = segA->finalColor;
  }
  else trap->color=0;
  trapezoids.Add(trap);
}

template <class Container>
void TrapezoidalDecomposition<Container>::Add(float x1, float y1, float x2, float y2, int color, int polyId)
{
  Ref<Event> from = AddEvent(x1,y1);
  Ref<Event> to = AddEvent(x2,y2);
  SweepLineSegment *seg;
  bool orientation = (*from->point.GetRef() < *to->point.GetRef());
  if (!orientation) { //swap
    Ref<Event> tmp=from; from=to; to=tmp;
  }
  seg = new SweepLineSegment(from->point, to->point);
  to->AddEdgeL(seg);
  from->AddEdgeR(seg);
  seg->SetColor(color);
  seg->SetPolyId(polyId);
  seg->SetOrientation(orientation);
}

template <class Container>
void TrapezoidalDecomposition<Container>::AddPolygon(AutoArray<Point2DRound> &poly, int color)
{
  int n=poly.Size();
  if (n<=1) return;
  const Point2DRound * from=&poly.Get(0);
  for (int i=1, n=poly.Size(); i<=n; i++)
  {
    const Point2DRound * to=&poly.Get(i%n);
    Add(from->x, from->y, to->x, to->y, color, _polyNum);
    from = to;
  }
  _polyNum++;
}

template <class Container>
Ref<Event> TrapezoidalDecomposition<Container>::AddEvent(float x, float y)
{
  Point2DRound pt(x,y);
  Event *event = _eventQ.QueueFind(pt);
  if (!event) 
  {
    event = new Event(x,y);
    _eventQ.QueueInsert(event);
  }
  return event;
}

struct PriorityKey
{
  int color;   //color of segment
  int polyId;  //set to id.HighPart
  PriorityKey() {}
  PriorityKey(int col, int id) : color(col), polyId(id){}
  bool operator<(const PriorityKey &b) {return color<b.color;}
  bool operator==(const PriorityKey &b) {return polyId==b.polyId;}
};

/**
Decomposition algorithm is basically as follows:
  Input: _eventQ full of segment endpoints with assosiated left and right edge lists
    
  while (event=_eventQ.POP()) {
    1. foreach segment with right endpoint in this event, create trapezoids
    2. if no such edges, create left trapezoid specifically
        (and test for improper intersection (event lies on some segment))
    3. delete all segments with right endpoint in this event 
        and test for new possible intersections of new neighbouring segments on sweepline 
        (add them as new events into _eventQ)
    4. foreach segment with left endpoint in this event, add this segment to sweepLine
        and test for new intersections (add them as new events into _eventQ)
  }
  add right most trapezoid

  TwoPassDecompose uses first pass to precalculate intersections and colors (FirstPass()). 
  It inserts only visible segments (which means these, which are not contained inside polygon with greater or equal color.
*/
template <class Container>
bool TrapezoidalDecomposition<Container>::TwoPassDecompose()
{
  AutoArray<Ref<Event> > * events2 = FirstPass();
  //suppose, that _eventQ is initialized with all segment endpoints
  //event queue should be priority queue, so it's properly lexicographically sorted
  _sweepLine.unboundedTrap = new Trapezoid;
  _sweepLine.unboundedTrap->x1 = -TRAP_INFTY;
  for (int ix=0, siz=events2->Size(); ix<siz; ix++)
  {
    Event *E = (*events2)[ix];
    if (E->edgeListL.Empty() && E->edgeListR.Empty()) continue;
    SweepLineSegment::_sweepLineX=E->point->x;
    _sweepLine.ResetTrapezoids();
    //first pass: create new trapezoids
    bool isLeftVertex = true;
    SweepLineSegment *topMostSeg=NULL, *bottomMostSeg=NULL;
    for(SweepLineSegment *segE=E->edgeListL.Start(); E->edgeListL.NotEnd(segE); segE=E->edgeListL.Advance(segE)) 
    { //iterate through list of edges which HAS right endpoint at eventE
      isLeftVertex = false; //there is an edge from left to this point
      SweepLineSegment *segA = segE->above;
      SweepLineSegment *segB = segE->below;
      if (segB && segB->rightP==segE->rightP) {
        //both lines ends in common endpoint
        CreateTrapLeft(segE,segB);
      }
      else { //create new trapezoid
        CreateTrapBelow(segE,segB); //segB can be NULL
        bottomMostSeg = segB;
      }
      if ((segA && segA->rightP!=segE->rightP) || !segA)
      { //segA won't appear as segE, so add uppermost trapezoid
        CreateTrapAbove(segE,segA); //segA can be NULL
        topMostSeg = segA;
      }
      //intersection events has been changed to vertices  
    }
    if (isLeftVertex)
    { //there has been no edge with right endpoint in this event
      //but one trapezoid must be inserted
      float dx = 1.0f;
      SweepLineSegment segE(E->point->x-dx, E->point->y, E->point->x+dx, E->point->y);
      SweepLineSegment *segB = _sweepLine.GetFirstLessOrEqual(&segE);
      SweepLineSegment *segA=NULL;
      if (segB)
        segA = segB->above;
      else {
        segA = _sweepLine.GetFirstGreaterOrEqual(&segE);
      }

      if (SegmentContainsPoint(segA, E->point))
      {
        AddImproperIntersectionEvent(segA, E);
        continue;
      }
      if (SegmentContainsPoint(segB, E->point))
      {
        AddImproperIntersectionEvent(segB, E);
        continue;
      }
      CreateTrapBoth(segB,segA,E->point->x); //both segA and segB can be NULL. It can create trapezoidal stubs inside
    }
    //second pass: update _sweepLine and eventQ and delete left edges
    for(SweepLineSegment *segE=E->edgeListL.Start(); E->edgeListL.NotEnd(segE); ) 
    {
      SweepLineSegment *segA = segE->above;
      SweepLineSegment *segB = segE->below;
      SweepLineSegment *segNext=E->edgeListL.Advance(segE);
      E->edgeListL.Delete(segE);
      _sweepLine.Delete(segE);
      segE=segNext;
      HandleIntersection(segA,segB);
    }
    //create new trapezoidal stubs for topMostSeg and bottomMostSeg segments
    if (topMostSeg) {
      topMostSeg->trapBelow = new Trapezoid;
      topMostSeg->trapBelow->x1 = SweepLineSegment::_sweepLineX;
      //update up and down edges
      Trapezoid *trap = _sweepLine.ltTr;
      Assert(trap!=NULL);
      int size = trap->edgesUp.Size();
      Assert(size);
      Trapezoid *rightMostTr = trap->edgesUp[size-1];
      topMostSeg->trapBelow->edgesUp.Add(rightMostTr); //they share this trapezoid
      rightMostTr->edgesDown.Add(topMostSeg->trapBelow);
    }
    if (bottomMostSeg) {
      if (topMostSeg && topMostSeg->below==bottomMostSeg) bottomMostSeg->trapAbove=topMostSeg->trapBelow;
      else {
        bottomMostSeg->trapAbove = new Trapezoid;
        bottomMostSeg->trapAbove->x1 = SweepLineSegment::_sweepLineX;
      }
      //update up and down edges
      Trapezoid *trap = _sweepLine.lbTr;
      Assert(trap!=NULL);
      int size = trap->edgesDown.Size();
      Assert(size);
      Trapezoid *rightMostTr = trap->edgesDown[size-1];
      bottomMostSeg->trapAbove->edgesDown.Add(rightMostTr); //they share this trapezoid
      rightMostTr->edgesUp.Add(bottomMostSeg->trapAbove);
    }
    bool isRightVertex=true;
    for(SweepLineSegment *segE=E->edgeListR.Start(); E->edgeListR.NotEnd(segE); segE=E->edgeListR.Advance(segE))
    {
      isRightVertex=false;
      //this is the only place, the segments are added into _sweepLine
      _sweepLine.Add(segE); //it should create new trapezoidal stub if necessary
      SweepLineSegment *segA = segE->above;
      SweepLineSegment *segB = segE->below;
      HandleIntersection(segA,segE);
      HandleIntersection(segB,segE);
    }
    if (isRightVertex) 
    {
      if (!bottomMostSeg && !topMostSeg)
      { //there is a gap to the next event (or last trapezoid). No stub created yet.
        _sweepLine.unboundedTrap = new Trapezoid;
        _sweepLine.unboundedTrap->x1 = SweepLineSegment::_sweepLineX;
        _sweepLine.rtTr=_sweepLine.rbTr=_sweepLine.unboundedTrap;
      }
      else if (bottomMostSeg) _sweepLine.rtTr=_sweepLine.rbTr=bottomMostSeg->trapAbove;
      else _sweepLine.rtTr=_sweepLine.rbTr=topMostSeg->trapBelow;
    }
    _sweepLine.AddTrapezoidsToGraph();
    //and there are no intersection events (they has been converted to vertex events)
  }
  //Add last trapezoid (SweepLineSegment::_sweepLineX is set to left border)
  Assert(_sweepLine.unboundedTrap);
  _sweepLine.unboundedTrap->x2 = _sweepLine.unboundedTrap->y2 = _sweepLine.unboundedTrap->y3 = TRAP_INFTY;
  _sweepLine.unboundedTrap->y1 = _sweepLine.unboundedTrap->y4 = -TRAP_INFTY;
  _sweepLine.unboundedTrap->color = 0;
  trapezoids.Add(_sweepLine.unboundedTrap);
  events2->Clear(); delete events2;
  MergeTrapezoids();
  return true;
}

template <class Container>
AutoArray<Ref<Event> > *TrapezoidalDecomposition<Container>::FirstPass()
{
  //suppose, that _eventQ is initialized with all segment endpoints
  //event queue should be priority queue, so it's properly lexicographically sorted
  AutoArray<Ref<Event> > *eventStack = new AutoArray<Ref<Event> >;
  while (!_eventQ.IsEmpty())
  {
#if _DEBUG
    int heapsize=_eventQ.Size();
    if (heapsize==101)
      _asm nop;
#endif
    Ref<Event> E;
    _eventQ.QueueRemoveFirst(E);
    Event *newE = new Event; newE->point=E->point;
    SweepLineSegment::_sweepLineX=E->point->x;
    //if (isLeftVertex)
    if (E->edgeListL.Empty())
    { //there has been no edge with right endpoint in this event
      float dx = 1.0f;
      SweepLineSegment segE(E->point->x-dx, E->point->y, E->point->x+dx, E->point->y);
      SweepLineSegment *segB = _sweepLine.GetFirstLessOrEqual(&segE);
      SweepLineSegment *segA=NULL;
      if (segB)
        segA = segB->above;
      else {
        segA = _sweepLine.GetFirstGreaterOrEqual(&segE);
      }
      if (SegmentContainsPoint(segA, E->point))
      {
        AddImproperIntersectionEvent(segA, E);
        continue;
      }
      if (SegmentContainsPoint(segB, E->point))
      {
        AddImproperIntersectionEvent(segB, E);
        continue;
      }
    }
    //second pass: update _sweepLine and eventQ and delete left edges
    for(SweepLineSegment *segE=E->edgeListL.Start(); E->edgeListL.NotEnd(segE); ) 
    {
      SweepLineSegment *segA = segE->above;
      SweepLineSegment *segB = segE->below;
      SweepLineSegment *segNext=E->edgeListL.Advance(segE);
      E->edgeListL.Delete(segE);
      SweepLineSegment *seg2 = NULL; 
      if (segE->visible) seg2 = new SweepLineSegment(*segE);
      _sweepLine.Delete(segE);
      if (seg2) {
        newE->AddEdgeL(seg2);
        seg2->leftEvent->AddEdgeR(seg2);
      }
      segE=segNext;
      HandleIntersection(segA,segB);
    }
    for(SweepLineSegment *segE=E->edgeListR.Start(); E->edgeListR.NotEnd(segE); segE=E->edgeListR.Advance(segE))
    {
      //this is the only place, the segments are added into _sweepLine
      segE->leftEvent=newE;
      _sweepLine.Add(segE,false); //it should create new trapezoidal stub if necessary
      SweepLineSegment *segA = segE->above;
      SweepLineSegment *segB = segE->below;
      HandleIntersection(segA,segE);
      HandleIntersection(segB,segE);
    }
    E->edgeListR.Clear();
    const Ref<SweepLineSegment> *rsegi=_sweepLine._segTree.GetMax();
    SweepLineSegment *segi;
    int previousColor=0;
    if (rsegi && rsegi->NotNull())
    {
      segi=rsegi->GetRef();
      PriorityList<PriorityKey> *colList = new PriorityList<PriorityKey>(PriorityKey(-1,-1));
      colList = colList->Insert(PriorityKey(segi->color, segi->id.HighPart));
      segi->finalColor=segi->color;
      segi->visible=true;
      while((segi=segi->below)!=NULL)
      {
        Assert(colList->left==NULL);
        if (segi->orientation)
        {
          colList = colList->Insert(PriorityKey(segi->color, segi->id.HighPart));
          segi->finalColor=colList->key.color;
          if (segi->id.HighPart==colList->key.polyId) segi->visible=true;
          else segi->visible=false;
        }
        else
        {
          if (segi->id.HighPart==colList->key.polyId) segi->visible=true;
          else segi->visible=false;
          colList=colList->Delete(PriorityKey(segi->color, segi->id.HighPart));
          segi->finalColor=colList->key.color<0 ? 0 : colList->key.color;
          if (colList->key.color==segi->color) segi->visible=false;
        }
        previousColor=segi->finalColor;
      }
      delete colList;
    }
    eventStack->Add(newE);
  }
  return eventStack;
}

extern bool MergePossible(Trapezoid *trap);

template <class Container>
void TrapezoidalDecomposition<Container>::MergeTrapezoid(Trapezoid *trap)
{
  trap->merged=true;
  while (MergePossible(trap))
  {
    Trapezoid *tr2 = trap->rightTr1;
    //merge these two
    if (tr2->leftTr1 && tr2->leftTr2) return;
    tr2->deleted=true;
    trap->x2=tr2->x2;
    trap->y3=tr2->y3;
    trap->y4=tr2->y4;
    trap->rightTr1=tr2->rightTr1;
    if (trap->rightTr1) {
      if (trap->rightTr1->leftTr1==tr2) trap->rightTr1->leftTr1=trap;
      else trap->rightTr1->leftTr2=trap;
    }
    trap->rightTr2=tr2->rightTr2;
    if (trap->rightTr2)
    {
      if (trap->rightTr2->leftTr1==tr2) trap->rightTr2->leftTr1=trap;
      else trap->rightTr2->leftTr2=trap;
    }
    for (int i=0, siz=tr2->edgesUp.Size(); i<siz; i++)
    {
      AutoArray<TrapezoidPtr> &artr = tr2->edgesUp[i]->edgesDown;
      for (int j=0, siz2=artr.Size(); j<siz2; j++)
        if (artr[j]==tr2) artr[j]=trap;
    }
    MergeEdges(trap->edgesUp, tr2->edgesUp);
    for (int i=0, siz=tr2->edgesDown.Size(); i<siz; i++)
    {
      AutoArray<TrapezoidPtr> &artr = tr2->edgesDown[i]->edgesUp;
      for (int j=0, siz2=artr.Size(); j<siz2; j++)
        if (artr[j]==tr2) artr[j]=trap;
    }
    MergeEdges(trap->edgesDown, tr2->edgesDown);
  }
}

template <class Container>
void TrapezoidalDecomposition<Container>::MergeTrapezoids()
{
  for (int i=0, siz=trapezoids.Size(); i<siz; i++)
  {
    while (!trapezoids[i]->merged && !trapezoids[i]->deleted)
    {
      MergeTrapezoid(trapezoids[i]);
    }
  }
  for (int i=0; i<trapezoids.Size(); i++)
  {
    if (trapezoids[i]->deleted) trapezoids.Delete(i--);
  }
}

template <class Container>
LSError TrapezoidalDecomposition<Container>::Serialize(ParamArchive &ar)
{
  ar.Serialize("ConvexComponents", convexComponents, 1);
  return ar.Serialize("Paths", pathComponents, 1);
}

#endif
