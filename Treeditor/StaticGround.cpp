// StaticGround.cpp : implementation file
//

#include "stdafx.h"
#include <El/elementpch.hpp>
#include "RV/Tree/mytree.h"
#include "Treeditor.h"
#include "StaticGround.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CStaticGround

CStaticGround::CStaticGround(COLORREF color) : _color (color)
{
}

CStaticGround::~CStaticGround()
{
}


BEGIN_MESSAGE_MAP(CStaticGround, CStatic)
	//{{AFX_MSG_MAP(CStaticGround)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStaticGround message handlers

void CStaticGround::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here
	
	// Do not call CStatic::OnPaint() for painting messages

	RECT r;
	GetClientRect (&r);
	dc.FillSolidRect (&r, _color);

	CFont *oldfont = dc.SelectObject (_font);
	CString text;
	GetWindowText (text);
	dc.DrawText (text, &r, DT_RIGHT | DT_VCENTER | DT_SINGLELINE);
	dc.SelectObject (oldfont);
}

CSize CStaticGround::CalcPixelWidth ()
{
	CString text;
	GetWindowText (text);
	CClientDC dc (this);
	CFont *oldfont = dc.SelectObject (_font);
	CSize size = dc.GetTextExtent (text);
	dc.SelectObject (oldfont);
	return size;
}

BOOL CStaticGround::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default
	
	return TRUE;
}
