#if !defined(AFX_UIPROPERTYSUBVIEW_H__BB0CEE0B_A8EB_4165_A3CD_1F339A086AF1__INCLUDED_)
#define AFX_UIPROPERTYSUBVIEW_H__BB0CEE0B_A8EB_4165_A3CD_1F339A086AF1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UiPropertySubview.h : header file
//

#include "UiProperty.h"

#define UI_PROPERTY_SCROLLBAR_WIDTH 16

/////////////////////////////////////////////////////////////////////////////
// CUiPropertySubview window

class CUiPropertySubview : public CWnd
{
// Construction
public:
	CUiPropertySubview();

// Attributes
public:
	CArray<CUiProperty*, CUiProperty*> _uis;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUiPropertySubview)
	protected:
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CUiPropertySubview();
	void AddUiProperty (LPCTSTR name, COLORREF color, const RECT &r, CWnd *parent, const CChannelProtocol &channel, long data, long use, bool selected, LPCTSTR value, bool enabled, va_list params);
	int GetPropCount () const;
	LPCTSTR GetPropValue (int i) const;
	void SetPropValue (int i, LPCTSTR val, bool updateUi);
	long GetPropData (int i) const;
	void DeleteAllProps ();
	void DeleteProp (int i);

	void SelectProp (int i, bool sel);
	void SelectOneProp (int idx);
	void SelectAllProps ();
	void DeselectAllProps ();
	int GetNextSelectedProp (int i) const;
	int GetSelectedPropCount () const;

	void UpdateScrollbars (int height);
	void ScrollBy (int dx, int dy);

	void RecalcLayout ();
	void SetEnabled (int i, bool e);
	
	void GetOptimalSize (int &w, int &h) const;

	// Generated message map functions
protected:
	//{{AFX_MSG(CUiPropertySubview)
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UIPROPERTYSUBVIEW_H__BB0CEE0B_A8EB_4165_A3CD_1F339A086AF1__INCLUDED_)
