// ScriptParser.cpp: implementation of the CScriptParser class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <El/elementpch.hpp>
#include "RV/Tree/mytree.h"
#include "treeditor.h"
#include "ScriptParser.h"

#include "TreeditorLex.h"
#include "UiProperty.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

const char CScriptParser::_d = ':';
const char CScriptParser::_let = '=';

const STreeditorKeyword CScriptParser::_scriptKeyWords[] =
{
	{"comment",			treeditorLex_comment},
};

const STreeditorKeyword CScriptParser::_scKeyWords[] =
{
	{"EXPORT",			treeditorLex_export},
	{"GROUP",			treeditorLex_group},
	{"PALETTE",			treeditorLex_palette},

	{"type",			treeditorLex_type},
	{"use",				treeditorLex_use},
	{"min",				treeditorLex_min},
	{"max",				treeditorLex_max},
	{"step",			treeditorLex_step},
	{"axisX",			treeditorLex_axisX},
	{"axisY",			treeditorLex_axisY},
	{"horizontalX",		treeditorLex_horizontalX},
	{"horizontalY",		treeditorLex_horizontalY},
	{"name",			treeditorLex_name},
	{"pairValue",		treeditorLex_pairValue},
	{"pairInterval",	treeditorLex_pairInterval},
	{"enabled",			treeditorLex_enabled},

	{"float",			treeditorLex_float},
	{"long",			treeditorLex_long},
	{"string",			treeditorLex_string},
	{"id",				treeditorLex_id},
	{"array",			treeditorLex_array},
	{"bool",			treeditorLex_bool},

	{"true",			treeditorLex_true},
	{"false",			treeditorLex_false},

	{"slider",			treeditorLex_slider},
	{"filename",		treeditorLex_filename},
	{"fixedpolyline",	treeditorLex_fixedpolyline},
	{"fixedcurve",		treeditorLex_fixedcurve},
	{"exfixedcurve",	treeditorLex_exfixedcurve},
	{"polyline",		treeditorLex_polyline},
	{"curve",			treeditorLex_curve},
	{"excurve",			treeditorLex_excurve},
	{"fixedpolylinex",	treeditorLex_fixedpolylinex},
	{"fixedcurvex",		treeditorLex_fixedcurvex},
	{"exfixedcurvex",	treeditorLex_exfixedcurvex},
	{"polylinex",		treeditorLex_polylinex},
	{"curvex",			treeditorLex_curvex},
	{"excurvex",		treeditorLex_excurvex},
	{"color",			treeditorLex_color},
	{"child",			treeditorLex_child},
	{"text",			treeditorLex_text},
	{"pair",			treeditorLex_pair},
};

CScriptParser::CScriptParser(LPCTSTR scriptText, CArray<STreeditorParamDesc*, STreeditorParamDesc*> *vars)
: _scriptText (scriptText), _input (scriptText), 
_lex (&_input, _scriptKeyWords, sizeof (_scriptKeyWords) / sizeof (STreeditorKeyword)),
_vars (vars)
{
	_lex.GetNextToken (&_token);
}

CScriptParser::~CScriptParser()
{

}

bool CScriptParser::GetGroupPalette (STreeditorParamGroupPalette *palette, LPCTSTR *endOfPrefix, LPCTSTR *startOfPostfix)
{
	for (int i = 0; i < 256; ++i)
	{
		palette->colors[i] = UI_PROPERTY_DEFAULT_COLOR;
	}
	palette->nColors = 0;

	if (_token.type == treeditorLexEof)
	{
		return false;
	}

	while (_token.type != treeditorLexEof)
	{
		if (_token.type == treeditorLex_comment && ParsePalette (palette, endOfPrefix, startOfPostfix))
		{
			return true;
		}
		
		_lex.GetNextToken(&_token);
	}

	return false;
}

bool CScriptParser::ParsePalette (STreeditorParamGroupPalette *palette, LPCTSTR *endOfPrefix, LPCTSTR *startOfPostfix)
{
	if (_token.type != treeditorLex_comment)
	{
		return false;
	}
	if (endOfPrefix != NULL)
	{
		*endOfPrefix = _input.GetCurrentPtr() - 1;
	}
	_lex.GetNextToken (&_token);

	if (_token.type != treeditorLexString)
	{
		return false;
	}
	if (startOfPostfix != NULL)
	{
		*startOfPostfix = _input.GetCurrentPtr() - 1;
	}
	// parse comment string
	STreeditorTokenDesc csToken;
	CTreeditorStringInput csInput (_token.string);
	CTreeditorLex csLex (&csInput, _scKeyWords, sizeof (_scKeyWords) / sizeof (STreeditorKeyword));
	csLex.GetNextToken(&csToken);
	switch (csToken.type)
	{
	case treeditorLex_palette:
		if (!ParsePaletteColors (palette, csLex, csToken, endOfPrefix, startOfPostfix))
		{
			return false;
		}
		break;

	default:
		return false;
	}

	return true;
}

bool CScriptParser::ParsePaletteColors (STreeditorParamGroupPalette *palette, 
										CTreeditorLex &csLex, STreeditorTokenDesc &csToken, 
										LPCTSTR *endOfPrefix, LPCTSTR *startOfPostfix)
{
	if (csToken.type != treeditorLex_palette)
	{
		return false;
	}
	csLex.GetNextToken(&csToken);

	int i = 0;
	while (csToken.type == _d)
	{
		csLex.GetNextToken(&csToken);

		switch (csToken.type)
		{
		case treeditorLexLong:
			palette->colors[i] = csToken.number;
			++i;
			csLex.GetNextToken(&csToken);
			break;

		default:
			return false;
		}
	}

	palette->nColors = i;
	return true;
}

bool CScriptParser::GetNextParam (STreeditorScriptParamDesc *desc)
{
	strcpy (desc->id, "");
	strcpy (desc->uiName, "");
	desc->type = treeditorLexString;
	strcpy (desc->defValue, "");
	desc->isChildClassName = false;
	desc->useWhat = DONT_USE_ANYTHING;
	desc->minValue = 0.f;
	desc->maxValue = 1.f;
	desc->step = 0.01f;
	desc->enumType = NO_ENUM_TYPE;
	desc->horizontalX = HORIZONTAL_Y;
	_tcscpy (desc->xDescr, _T ("x"));
	_tcscpy (desc->yDescr, _T ("y"));
	_tcscpy (desc->pairValueLabel, _T ("&Value:"));
	_tcscpy (desc->pairIntervalLabel, _T ("&Interval:"));
	desc->enabled = true;
	desc->uiUse = 0;

	if (_token.type == treeditorLexEof)
	{
		return false;
	}

	while (_token.type != treeditorLexEof)
	{
		if (_token.type == treeditorLex_comment && ParseParam (desc))
		{
			return true;
		}
		
		_lex.GetNextToken(&_token);
	}

	return false;
}

bool CScriptParser::ParseParam(STreeditorScriptParamDesc *desc)
{
	if (_token.type != treeditorLex_comment)
	{
		return false;
	}
	_lex.GetNextToken (&_token);

	if (_token.type != treeditorLexString)
	{
		return false;
	}
	// parse comment string
	STreeditorTokenDesc csToken;
	CTreeditorStringInput csInput (_token.string);
	CTreeditorLex csLex (&csInput, _scKeyWords, sizeof (_scKeyWords) / sizeof (STreeditorKeyword));
	csLex.GetNextToken(&csToken);
	switch (csToken.type)
	{
	case treeditorLex_export:
		if (!ParseParamExport (desc, csLex, csToken))
		{
			return false;
		}
		break;

	case treeditorLex_group:
		ParseParamGroup (desc, csLex, csToken);
		return false;

	default:
		return false;
	}

	return true;
}

bool CScriptParser::ParseParamGroup (STreeditorScriptParamDesc *desc, CTreeditorLex &csLex, STreeditorTokenDesc &csToken)
{
	if (csToken.type != treeditorLex_group)
	{
		return false;
	}
	csLex.GetNextToken(&csToken);

	while (csToken.type == _d)
	{
		csLex.GetNextToken(&csToken);

		switch (csToken.type)
		{
		case treeditorLex_color:
			csLex.GetNextToken(&csToken);
			
			if (csToken.type != _let)
			{
				return false;
			}
			csLex.GetNextToken(&csToken);

			if (csToken.type != treeditorLexLong)
			{
				return false;
			}
			desc->group.colorIndex = csToken.number;
			csLex.GetNextToken(&csToken);
			break;

		default:
			return false;
		}
	}

	return true;
}

bool CScriptParser::ParseParamExport (STreeditorScriptParamDesc *desc, CTreeditorLex &csLex, STreeditorTokenDesc &csToken)
{
	if (csToken.type != treeditorLex_export)
	{
		return false;
	}
	if (!ParseParamDesc(desc, csLex, csToken))
	{
		return false;
	}
	// continue behind comment
	_lex.GetNextToken (&_token);

	if (_token.type != ';')
	{
		return false;
	}
	_lex.GetNextToken(&_token);

	if (_token.type != treeditorLexId)
	{
		return false;
	}
	strcpy (desc->id, _token.string);
	if (strlen (desc->uiName) == 0)
	{
		strcpy (desc->uiName, _token.string);
	}
	_lex.GetNextToken(&_token);

	if (_token.type != '=')
	{
		return false;
	}
	desc->_endOfPrefix = _input.GetCurrentPtr() - 1;
	_lex.GetNextToken(&_token);

	switch (_token.type)
	{
	case treeditorLexLong:
		desc->_startOfPostfix = _input.GetCurrentPtr() - 1;

		sprintf (desc->defValue, "%ld", _token.number);
		_lex.GetNextToken(&_token);
		break;

	case treeditorLexFixedFloat:
		desc->_startOfPostfix = _input.GetCurrentPtr() - 1;

		sprintf(desc->defValue, "%g", _token.fnumber);
		_lex.GetNextToken(&_token);
		break;

	case treeditorLexString:
		desc->_startOfPostfix = _input.GetCurrentPtr() - 1;

		strcpy (desc->defValue, _token.string);
		_lex.GetNextToken(&_token);
		break;

	case treeditorLexId:
		desc->_startOfPostfix = _input.GetCurrentPtr() - 1;

		strcpy (desc->defValue, _token.string);
		_lex.GetNextToken(&_token);
		break;
		
	case '[':
		{
			_lex.GetNextToken(&_token);

			if (_token.type != treeditorLexFixedFloat && _token.type != treeditorLexLong)
			{
				return false;
			}
			CString arr;
			arr.Format(_T ("%g"), _token.fnumber);
			_lex.GetNextToken(&_token);

			CString tmp;
			while (_token.type == ',')
			{
				_lex.GetNextToken(&_token);

				if (_token.type != treeditorLexFixedFloat && _token.type != treeditorLexLong)
				{
					return false;
				}
				tmp.Format(_T (",%g"), _token.fnumber);
				arr += tmp;
				_lex.GetNextToken(&_token);
			}

			if (_token.type != ']')
			{
				return false;
			}
			desc->_startOfPostfix = _input.GetCurrentPtr() - 1;

			strcpy (desc->defValue, arr);
			_lex.GetNextToken(&_token);
		}
		break;
		
	default:
		return false;
	}
	
	return true;
}

bool CScriptParser::ParseParamDesc(STreeditorScriptParamDesc *desc, CTreeditorLex &csLex, STreeditorTokenDesc &csToken)
{
	if (csToken.type != treeditorLex_export)
	{
		return false;
	}
	csLex.GetNextToken(&csToken);

	while (csToken.type == _d)
	{
		csLex.GetNextToken(&csToken);

		switch (csToken.type)
		{
		case treeditorLex_type:
			if (!ParseParamType(desc, csLex, csToken))
			{
				return false;
			}
			break;
			
		case treeditorLex_use:
			if (!ParseParamUse(desc, csLex, csToken))
			{
				return false;
			}
			break;
			
		case treeditorLex_min:
			csLex.GetNextToken(&csToken);
			if (!ParseParamAssignment(desc->minValue, csLex, csToken))
			{
				return false;
			}
			break;
			
		case treeditorLex_max:
			csLex.GetNextToken(&csToken);
			if (!ParseParamAssignment(desc->maxValue, csLex, csToken))
			{
				return false;
			}
			break;
			
		case treeditorLex_step:
			csLex.GetNextToken(&csToken);
			if (!ParseParamAssignment(desc->step, csLex, csToken))
			{
				return false;
			}
			break;

		case treeditorLex_axisX:
			csLex.GetNextToken(&csToken);
			if (!ParseParamString(desc->xDescr, csLex, csToken))
			{
				return false;
			}
			break;

		case treeditorLex_axisY:
			csLex.GetNextToken(&csToken);
			if (!ParseParamString(desc->yDescr, csLex, csToken))
			{
				return false;
			}
			break;

		case treeditorLex_horizontalX:
			desc->horizontalX = true;
			csLex.GetNextToken(&csToken);
			break;

		case treeditorLex_horizontalY:
			desc->horizontalX = false;
			csLex.GetNextToken(&csToken);
			break;

		case treeditorLex_name:
			csLex.GetNextToken(&csToken);
			if (!ParseParamString(desc->uiName, csLex, csToken))
			{
				return false;
			}
			break;

		case treeditorLex_pairValue:
			csLex.GetNextToken(&csToken);
			if (!ParseParamString(desc->pairValueLabel, csLex, csToken))
			{
				return false;
			}
			break;

		case treeditorLex_pairInterval:
			csLex.GetNextToken(&csToken);
			if (!ParseParamString(desc->pairIntervalLabel, csLex, csToken))
			{
				return false;
			}
			break;

		case treeditorLex_enabled:
			csLex.GetNextToken(&csToken);
			if (!ParseParamAssignment(desc->enabled, csLex, csToken))
			{
				return false;
			}
			break;

		default:
			return false;
		}
	}

	return true;
}

bool CScriptParser::ParseParamString (char *axis, CTreeditorLex &csLex, STreeditorTokenDesc &csToken)
{
	if (csToken.type != _let)
	{
		return false;
	}
	csLex.GetNextExpressionToken(&csToken, false);

	strcpy (axis, "");
	while (csToken.type != _d && csToken.type != treeditorLexEof)
	{
		strcat (axis, csToken.string);
		csLex.GetNextExpressionToken(&csToken, false);
	}

	return true;
}

bool CScriptParser::ParseParamType(STreeditorScriptParamDesc *desc, CTreeditorLex &csLex, STreeditorTokenDesc &csToken)
{
	if (csToken.type != treeditorLex_type)
	{
		return false;
	}
	csLex.GetNextToken(&csToken);

	if (csToken.type != _let)
	{
		return false;
	}
	csLex.GetNextToken(&csToken);

	switch (csToken.type)
	{
	case treeditorLex_float:
		desc->type = treeditorLexFixedFloat;
		desc->uiUse |= USE_PROPERTY_VALUE_EDIT;
		break;

	case treeditorLex_long:
		desc->type = treeditorLexLong;
		desc->uiUse |= USE_PROPERTY_VALUE_EDIT;
		break;

	case treeditorLex_string:
		desc->type = treeditorLexString;
		break;

	case treeditorLex_id:
		desc->type = treeditorLexId;
		break;

	case treeditorLex_array:
		desc->type = treeditorLexArray;
		break;

	case treeditorLex_bool:
		desc->type = treeditorLexId;
		desc->enumType = ENUM_TYPE(3);
		desc->uiUse |= USE_PROPERTY_BOOL_CHECK;
		break;

	default:
		return false;
	}
	csLex.GetNextToken(&csToken);

	return true;
}

bool CScriptParser::ParseParamUse(STreeditorScriptParamDesc *desc, CTreeditorLex &csLex, STreeditorTokenDesc &csToken)
{
	if (csToken.type != treeditorLex_use)
	{
		return false;
	}
	csLex.GetNextToken (&csToken);

	if (csToken.type != _let)
	{
		return false;
	}
	csLex.GetNextToken (&csToken);

	switch (csToken.type)
	{
	case treeditorLex_slider:
		desc->useWhat = USE_SLIDER;
		csLex.GetNextToken(&csToken);
		break;

	case treeditorLex_filename:
		desc->useWhat = USE_OPENFILE_DLG;
		desc->uiUse |= USE_PROPERTY_EDIT_BUTTON | USE_PROPERTY_SELECT_BUTTON | USE_PROPERTY_VALUE_EDIT;
		csLex.GetNextToken(&csToken);
		break;

	case treeditorLex_fixedpolyline:
		desc->useWhat = USE_LINEAR_CURVE_DLG;
		desc->uiUse |= USE_PROPERTY_EDIT_BUTTON;
		csLex.GetNextToken(&csToken);
		break;

	case treeditorLex_fixedcurve:
		desc->useWhat = USE_QUADRATIC_CURVE_DLG;
		desc->uiUse |= USE_PROPERTY_EDIT_BUTTON;
		csLex.GetNextToken(&csToken);
		break;
		
	case treeditorLex_exfixedcurve:
		desc->useWhat = USE_EX_QUADRATIC_CURVE_DLG;
		desc->uiUse |= USE_PROPERTY_EDIT_BUTTON;
		csLex.GetNextToken(&csToken);
		break;

	case treeditorLex_polyline:
		desc->useWhat = USE_LINEAR_VAR_CURVE_DLG;
		desc->uiUse |= USE_PROPERTY_EDIT_BUTTON;
		csLex.GetNextToken(&csToken);
		break;

	case treeditorLex_curve:
		desc->useWhat = USE_QUADRATIC_VAR_CURVE_DLG;
		desc->uiUse |= USE_PROPERTY_EDIT_BUTTON;
		csLex.GetNextToken(&csToken);
		break;

	case treeditorLex_excurve:
		desc->useWhat = USE_EX_QUADRATIC_VAR_CURVE_DLG;
		desc->uiUse |= USE_PROPERTY_EDIT_BUTTON;
		csLex.GetNextToken(&csToken);
		break;

	case treeditorLex_fixedpolylinex:
		desc->useWhat = USE_LINEAR_CURVE_WITH_X_DLG;
		desc->uiUse |= USE_PROPERTY_EDIT_BUTTON;
		csLex.GetNextToken(&csToken);
		break;

	case treeditorLex_fixedcurvex:
		desc->useWhat = USE_QUADRATIC_CURVE_WITH_X_DLG;
		desc->uiUse |= USE_PROPERTY_EDIT_BUTTON;
		csLex.GetNextToken(&csToken);
		break;
		
	case treeditorLex_exfixedcurvex:
		desc->useWhat = USE_EX_QUADRATIC_CURVE_WITH_X_DLG;
		desc->uiUse |= USE_PROPERTY_EDIT_BUTTON;
		csLex.GetNextToken(&csToken);
		break;

	case treeditorLex_polylinex:
		desc->useWhat = USE_LINEAR_VAR_CURVE_WITH_X_DLG;
		desc->uiUse |= USE_PROPERTY_EDIT_BUTTON;
		csLex.GetNextToken(&csToken);
		break;

	case treeditorLex_curvex:
		desc->useWhat = USE_QUADRATIC_VAR_CURVE_WITH_X_DLG;
		desc->uiUse |= USE_PROPERTY_EDIT_BUTTON;
		csLex.GetNextToken(&csToken);
		break;

	case treeditorLex_excurvex:
		desc->useWhat = USE_EX_QUADRATIC_VAR_CURVE_WITH_X_DLG;
		desc->uiUse |= USE_PROPERTY_EDIT_BUTTON;
		csLex.GetNextToken(&csToken);
		break;

	case treeditorLex_color:
		desc->useWhat = USE_COLOR_DLG;
		desc->uiUse |= USE_PROPERTY_EDIT_BUTTON;
		csLex.GetNextToken(&csToken);
		break;

	case treeditorLex_child:
		desc->isChildClassName = true;
		desc->useWhat = DONT_USE_ANYTHING;
		desc->uiUse |= USE_PROPERTY_CHILD_COMBO;
		csLex.GetNextToken(&csToken);
		break;

	case treeditorLex_text:
		desc->useWhat = USE_TEXT_EDITOR;
		desc->uiUse |= USE_PROPERTY_EDIT_BUTTON;
		csLex.GetNextToken(&csToken);
		break;

	case treeditorLex_pair:
		desc->useWhat = USE_PAIR_DLG;
		desc->uiUse |= USE_PROPERTY_EDIT_BUTTON;
		csLex.GetNextToken(&csToken);
		break;

	default:
		return false;
	}

	return true;
}

bool CScriptParser::ParseParamAssignment(float &value, CTreeditorLex &csLex, STreeditorTokenDesc &csToken)
{
	char buf[1024];
	if (!ParseParamString (buf, csLex, csToken))
	{
		return false;
	}

	GameVarSpace vars;
	vars._vars.Clear();
	int n = _vars->GetSize ();
	RString name;
	for (int i = 0; i < n; ++i)
	{
		name = (*_vars)[i]->id;
		char *start = (*_vars)[i]->defValue;
		char *end;
		float v = strtod (start, &end);
		if (start == end)
		{
			if (strcmp (start, "true") == 0)
			{
				v = 1.0f;
			}
			else
			{
				v = 0.0f;
			}
		}
		GameVariable var(name, v, true);
		vars._vars.Add(var);
	}

	GGameState.BeginContext(&vars);
	value = GGameState.Evaluate(buf);
	GGameState.EndContext();

	return true;
}

bool CScriptParser::ParseParamAssignment(bool &value, CTreeditorLex &csLex, STreeditorTokenDesc &csToken)
{
	float f;
	if (!ParseParamAssignment (f, csLex, csToken))
	{
		return false;
	}

	value = f != 0.0f;
	return true;
}

/*
bool CScriptParser::ParseParamAssignment(float &value, CTreeditorLex &csLex, STreeditorTokenDesc &csToken)
{
	if (csToken.type != _let)
	{
		return false;
	}
	csLex.GetNextExpressionToken(&csToken);

	if (!ParseParamExpression (value, csLex, csToken))
	{
		return false;
	}

	return true;
}

bool CScriptParser::ParseParamAssignment(bool &value, CTreeditorLex &csLex, STreeditorTokenDesc &csToken)
{
	if (csToken.type != _let)
	{
		return false;
	}
	csLex.GetNextExpressionToken(&csToken);

	float f;
	if (!ParseParamExpression (f, csLex, csToken))
	{
		return false;
	}

	value = f != 0.0f;
	return true;
}

bool CScriptParser::ParseParamExpression (float &value, CTreeditorLex &csLex, STreeditorTokenDesc &csToken)
{
	switch (csToken.type)
	{
	case treeditorLexFixedFloat:
	case treeditorLexLong:
	case treeditorLexId:
	case '(':
	case '-':
	case '+':
	case '!':
	case treeditorLex_true:
	case treeditorLex_false:
		if (!ParseParamExpressionOr (value, csLex, csToken))
		{
			return false;
		}
		break;

	default:
		return false;
	}

	return true;
}

bool CScriptParser::ParseParamExpressionAddSub (float &value, CTreeditorLex &csLex, STreeditorTokenDesc &csToken)
{
	switch (csToken.type)
	{
	case treeditorLexFixedFloat:
	case treeditorLexLong:
	case treeditorLexId:
	case '(':
	case '+':
	case '-':
	case '!':
	case treeditorLex_true:
	case treeditorLex_false:
		{
			if (!ParseParamExpressionMulDiv(value, csLex, csToken))
			{
				return false;
			}
			
			do {
				float leftValue = value;
				switch (csToken.type)
				{
				case '+':
					csLex.GetNextExpressionToken(&csToken);
					
					if (!ParseParamExpressionMulDiv(value, csLex, csToken))
					{
						return false;
					}
					value = leftValue + value;
					break;
					
				case '-':
					csLex.GetNextExpressionToken(&csToken);
					
					if (!ParseParamExpressionMulDiv(value, csLex, csToken))
					{
						return false;
					}
					value = leftValue - value;
					break;
				}
			} while (csToken.type == '+' || csToken.type == '-');
		}
		break;

	default:
		return false;
	}

	return true;
}

bool CScriptParser::ParseParamExpressionMulDiv (float &value, CTreeditorLex &csLex, STreeditorTokenDesc &csToken)
{
	switch (csToken.type)
	{
	case treeditorLexFixedFloat:
	case treeditorLexLong:
	case treeditorLexId:
	case '(':
	case '+':
	case '-':
	case '!':
	case treeditorLex_true:
	case treeditorLex_false:
		{
			if (!ParseParamExpressionValue(value, csLex, csToken))
			{
				return false;
			}
			
			do {
				float leftValue = value;
				switch (csToken.type)
				{
				case '*':
					csLex.GetNextExpressionToken(&csToken);
					
					if (!ParseParamExpressionValue(value, csLex, csToken))
					{
						return false;
					}
					value = leftValue * value;
					break;
					
				case '/':
					csLex.GetNextExpressionToken(&csToken);
					
					if (!ParseParamExpressionValue(value, csLex, csToken))
					{
						return false;
					}
					value = leftValue / value;
					break;
				}
			} while (csToken.type == '*' || csToken.type == '/');
		}
		break;

	default:
		return false;
	}

	return true;
}

bool CScriptParser::ParseParamExpressionValue (float &value, CTreeditorLex &csLex, STreeditorTokenDesc &csToken)
{
	switch (csToken.type)
	{
	case treeditorLex_true:
		value = 1.0f;
		csLex.GetNextExpressionToken(&csToken);
		break;

	case treeditorLex_false:
		value = 0.0f;
		csLex.GetNextExpressionToken(&csToken);
		break;

	case treeditorLexFixedFloat:
	case treeditorLexLong:
		value = csToken.fnumber;
		csLex.GetNextExpressionToken(&csToken);
		break;

	case '!':
		csLex.GetNextExpressionToken(&csToken);

		if (!ParseParamExpressionValue(value, csLex, csToken))
		{
			return false;
		}
		value = (value != 0.0f) ? 0.0f : 1.0f;
		break;

	case '-':
		csLex.GetNextExpressionToken(&csToken);

		if (!ParseParamExpressionValue(value, csLex, csToken))
		{
			return false;
		}
		value = -value;
		break;

	case '+':
		csLex.GetNextExpressionToken(&csToken);

		if (!ParseParamExpressionValue(value, csLex, csToken))
		{
			return false;
		}
		value = value;
		break;

	case treeditorLexId:
		{
			if (_vars == NULL)
			{
				return false;
			}

			int n = _vars->GetSize ();
			for (int i = 0; i < n; ++i)
			{
				if (strcmp ((*_vars)[i]->id, csToken.string) == 0)
				{
					char *start = (*_vars)[i]->defValue;
					char *end;
					float f = (float) strtod (start, &end);
					if (end == start)
					{
						if (strcmp (start, "true") == 0)
						{
							value = 1.0f;
							csLex.GetNextExpressionToken(&csToken);
							return true;
						}
						else if (strcmp (start, "false") == 0)
						{
							value = 0.0f;
							csLex.GetNextExpressionToken(&csToken);
							return true;
						}
						return false;
					}

					value = f;
					csLex.GetNextExpressionToken(&csToken);
					return true;
				}
			}
			return false;
		}
		break;

	case '(':
		csLex.GetNextExpressionToken(&csToken);

		if (!ParseParamExpression(value, csLex, csToken))
		{
			return false;
		}

		if (csToken.type != ')')
		{
			return false;
		}
		csLex.GetNextExpressionToken(&csToken);

		return true;

	default:
		return false;
	}

	return true;
}

bool CScriptParser::ParseParamExpressionOr (float &value, CTreeditorLex &csLex, STreeditorTokenDesc &csToken)
{
	switch (csToken.type)
	{
	case treeditorLexFixedFloat:
	case treeditorLexLong:
	case treeditorLexId:
	case '(':
	case '+':
	case '-':
	case '!':
	case treeditorLex_true:
	case treeditorLex_false:
		{
			if (!ParseParamExpressionAnd(value, csLex, csToken))
			{
				return false;
			}
			
			do {
				float leftValue = value;
				switch (csToken.type)
				{
				case '|':
					csLex.GetNextExpressionToken(&csToken);
					
					if (!ParseParamExpressionAnd(value, csLex, csToken))
					{
						return false;
					}
					value = (leftValue != 0.0f || value != 0.0f) ? 1.0f: 0.0f;
					break;
				}
			} while (csToken.type == '+' || csToken.type == '-');
		}
		break;

	default:
		return false;
	}

	return true;
}

bool CScriptParser::ParseParamExpressionAnd (float &value, CTreeditorLex &csLex, STreeditorTokenDesc &csToken)
{
	switch (csToken.type)
	{
	case treeditorLexFixedFloat:
	case treeditorLexLong:
	case treeditorLexId:
	case '(':
	case '+':
	case '-':
	case '!':
	case treeditorLex_true:
	case treeditorLex_false:
		{
			if (!ParseParamExpressionAddSub(value, csLex, csToken))
			{
				return false;
			}
			
			do {
				float leftValue = value;
				switch (csToken.type)
				{
				case '&':
					csLex.GetNextExpressionToken(&csToken);
					
					if (!ParseParamExpressionAddSub(value, csLex, csToken))
					{
						return false;
					}
					value = (leftValue != 0.0f && value != 0.0f) ? 1.0f : 0.0f;
					break;
				}
			} while (csToken.type == '*' || csToken.type == '/');
		}
		break;

	default:
		return false;
	}

	return true;
}
*/
