// TreeditorClass.h: interface for the CTreeditorClass class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TREEDITORCLASS_H__2103FED2_052E_4900_A8AA_C30980843197__INCLUDED_)
#define AFX_TREEDITORCLASS_H__2103FED2_052E_4900_A8AA_C30980843197__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#define IS_CHILD_CLASS_NAME true
#define ISNT_CHILD_CLASS_NAME false

#define USE_SLIDER (1 << 0)
#define USE_COLOR_DLG (1 << 1)

#define USE_LINEAR_CURVE_DLG (1 << 2)
#define USE_LINEAR_CURVE_WITH_X_DLG (1 << 3)
#define USE_QUADRATIC_CURVE_DLG (1 << 4)
#define USE_QUADRATIC_CURVE_WITH_X_DLG (1 << 5)
#define USE_EX_QUADRATIC_CURVE_DLG (1 << 6)
#define USE_EX_QUADRATIC_CURVE_WITH_X_DLG (1 << 7)

#define USE_LINEAR_VAR_CURVE_DLG (1 << 8)
#define USE_LINEAR_VAR_CURVE_WITH_X_DLG (1 << 9)
#define USE_QUADRATIC_VAR_CURVE_DLG (1 << 10)
#define USE_QUADRATIC_VAR_CURVE_WITH_X_DLG (1 << 11)
#define USE_EX_QUADRATIC_VAR_CURVE_DLG (1 << 12)
#define USE_EX_QUADRATIC_VAR_CURVE_WITH_X_DLG (1 << 13)

#define USE_OPENFILE_DLG (1 << 14)
#define USE_TEXT_EDITOR (1 << 15)
#define USE_PAIR_DLG (1 << 16)

#define DONT_USE_ANYTHING 0
#define NO_ENUM_TYPE NULL
#define ENUM_TYPE(x) (&CTreeditorParam::enumParams[x])
#define HORIZONTAL_X true
#define HORIZONTAL_Y false


enum ETreeditorClassParam
{
	treeditorClassParam_size = 0,
	treeditorClassParam_sizeInterval,
	treeditorClassParam_sizeCoef,
	treeditorClassParam_script,
	treeditorClassParam_textureName,
	treeditorClassParam_textureSaveName,
	treeditorClassParam_textureIndex,
  treeditorClassParam_textureTilingCircumferenceCoef,
  treeditorClassParam_textureTilingLengthCoef,
	treeditorClassParam_balance,
	treeditorClassParam_balanceInterval,
	treeditorClassParam_twist,
	treeditorClassParam_twistInterval,
	treeditorClassParam_spread,
	treeditorClassParam_spreadInterval,
	treeditorClassParam_spreadRatio,
	treeditorClassParam_width,
	treeditorClassParam_height,
	treeditorClassParam_arrowWidth,
	treeditorClassParam_arrowHeight,
	treeditorClassParam_heightInterval,
	treeditorClassParam_heightCoef,
	treeditorClassParam_depth,
	treeditorClassParam_childNumber,
	treeditorClassParam_childNumberInterval,
	treeditorClassParam_childAgeCoef,
	treeditorClassParam_childAgeCoefInterval,
	treeditorClassParam_childSlopeCoef,
	treeditorClassParam_childMap,
  treeditorClassParam_sideChildNumber,
	treeditorClassParam_sideChildNumberInterval,
	treeditorClassParam_sideChildAngle,
	treeditorClassParam_sideChildAngleInterval,
	treeditorClassParam_sideChildTwist,
	treeditorClassParam_sideChildAgeCoef,
	treeditorClassParam_sideChildAgeCoefInterval,
	treeditorClassParam_photoCoef,
	treeditorClassParam_diaCoef,
	treeditorClassParam_graviCoef,
	treeditorClassParam_geoCoef,
	treeditorClassParam_polyplaneType,
	treeditorClassParam_polyplaneTreshold,
	treeditorClassParam_ageCoef,
	treeditorClassParam_windCoef,
	treeditorClassParam_randomBendRange,
	treeditorClassParam_spin,
	treeditorClassParam_spinInterval,
	treeditorClassParam_child,
	treeditorClassParam_frontChild,
	treeditorClassParam_childA,
	treeditorClassParam_childB,
	treeditorClassParam_sideChild,
	treeditorClassParam_leafChild,
	treeditorClassParam_leafTreshold,
	treeditorClassParam_frontChildTreshold,
	treeditorClassParam_leafTresholdA,
	treeditorClassParam_leafTresholdB,
	treeditorClassParam_frontLeafChild,
	treeditorClassParam_frontLeafTreshold,
	treeditorClassParam_sideLeafChild,
	treeditorClassParam_sideLeafTreshold,
  treeditorClassParam_aSideChildAge,
  treeditorClassParam_aWidth,
  treeditorClassParam_segmentCount,
	treeditorClassParam_tempCrossKind,
	treeditorClassParam_tempColor,
	treeditorClassParam_tempLinearCurve,
	treeditorClassParam_tempQuadraticCurve,
	treeditorClassParam_tempExQuadraticCurve,
	treeditorClassParam_tempLinearXCurve,
	treeditorClassParam_tempQuadraticXCurve,
	treeditorClassParam_tempExQuadraticXCurve,
	treeditorClassParam_tempBool,
	treeditorClassParam_tempScriptBool,
	treeditorClassParam_tempPair,

	treeditorClassParam_numberOfTypes,
	treeditorClassParam_stop,
};


struct STreeditorParamEnumDesc
{
	int _size;
	char *_ids[16];
	char *_values[16];
};


struct STreeditorParamGroupDesc
{
	DWORD colorIndex;
};

struct STreeditorParamDesc
{
	//ETreeditorClassParam id;
	char id[256];
	char uiName[256];
	ETreeditorTokenType type;
	char defValue[256];
	bool isChildClassName;
	unsigned long useWhat;
	float minValue;
	float maxValue;
	float step;
	const STreeditorParamEnumDesc *enumType;
	bool horizontalX;
	char xDescr[256];
	char yDescr[256];
	char pairValueLabel[256];
	char pairIntervalLabel[256];
	unsigned long uiUse;
	bool enabled;

	STreeditorParamGroupDesc group;

	static void copy (STreeditorParamDesc &dst, const STreeditorParamDesc &src)
	{
		strcpy (dst.id, src.id);
		strcpy (dst.uiName, src.uiName);
		dst.type = src.type;
		strcpy(dst.defValue, src.defValue);
		dst.isChildClassName = src.isChildClassName;
		dst.useWhat = src.useWhat;
		dst.minValue = src.minValue;
		dst.maxValue = src.maxValue;
		dst.step = src.step;
		dst.enumType = src.enumType;
		dst.horizontalX = src.horizontalX;
		_tcscpy (dst.xDescr, src.xDescr);
		_tcscpy (dst.yDescr, src.yDescr);
		_tcscpy (dst.pairValueLabel, src.pairValueLabel);
		_tcscpy (dst.pairIntervalLabel, src.pairIntervalLabel);
		dst.uiUse = src.uiUse;
		dst.enabled = src.enabled;
		dst.group = src.group;
	}
};


struct STreeditorParamGroupPalette
{
	DWORD colors[256];
	int nColors;
};

enum ETreeditorClassType
{
	treeditorClass_fern = 0,
  treeditorClass_test,
	treeditorClass_scriptStem,
	treeditorClass_scriptTree,
	treeditorClass_scriptLeaf,
	treeditorClass_scriptBunch,
	treeditorClass_scriptEmpty,
	treeditorClass_test_of_params,
	treeditorClass_test_of_params_2,
	treeditorClass_unknown,
	treeditorClass_numberOfTypes,
};


struct STreeditorClassTypeDesc
{
	//ETreeditorClassType type;
	char *name;
	ETreeditorClassParam params[treeditorClassParam_numberOfTypes + 1];
};


class CTreeditorParam
{
public:
	static const STreeditorParamDesc classParams[treeditorClassParam_numberOfTypes];
	static const STreeditorParamEnumDesc enumParams[];

	int _id;
	CString _value;

public:
	CTreeditorParam ()
		: _id (-1)
	{
	}

	void Init (int id)
	{
		ASSERT (id >= 0);
		_id = id;
		SetValue (CTreeditorParam::classParams[id].defValue);
	}

	void SetValue (LPCTSTR value)
	{
		ASSERT (_id >= 0);

		const STreeditorParamEnumDesc *en = classParams[_id].enumType;
		if (en == NULL || 
			classParams[_id].type != treeditorLexId)
		{
			_value = value;
			return;
		}

		for (int i = 0; i < en->_size; ++i)
		{
			if (_tcscmp (value, en->_ids[i]) == 0)
			{
				_value = en->_values[i];
				return;
			}
		}

		return;
	}

	void SetValue (float value)
	{
		ASSERT (_id >= 0);
		_value.Format (_T ("%g"), value);
	}

	void SetValue (long value)
	{
		ASSERT (_id >= 0);
		_value.Format (_T ("%d"), value);
	}

	LPCTSTR GetValue () const
	{
		ASSERT (_id >= 0);

		const STreeditorParamEnumDesc *en = classParams[_id].enumType;
		if (en == NULL || 
			classParams[_id].type != treeditorLexId)
		{
			return _value;
		}

		for (int i = 0; i < en->_size; ++i)
		{
			if (_value.Compare (en->_values[i]) == 0)
			{
				return en->_ids[i];
			}
		}

		return _value;
	}

	void GenerateValue (CString &desc)
	{
		ASSERT (_id >= 0);
		switch (CTreeditorParam::classParams[_id].type)
		{
		case treeditorLexArray:
			desc += CTreeditorParam::classParams[_id].id;
			desc += _T ("[] = {");
			desc += _value;
			desc += _T ("};");
			break;

		default:
			desc += CTreeditorParam::classParams[_id].id;
			desc += _T (" = ");
			desc += _value;
			desc += _T (";");
			break;
		}
	}

	void DegenerateValue (LPCTSTR value)
	{
		ASSERT (_id >= 0);
		_value = value;
	}

	void DegenerateValue (float value)
	{
		ASSERT (_id >= 0);
		_value.Format (_T ("%g"), value);
	}

	void DegenerateValue (long value)
	{
		ASSERT (_id >= 0);
		_value.Format (_T ("%d"), value);
	}
};


class CTreeditorClass  
{
public:
	static const STreeditorClassTypeDesc classTypes[treeditorClass_numberOfTypes];

public:
	int type;
	CString name;
	CTreeditorParam params[treeditorClassParam_numberOfTypes];
	bool selected;
	bool saved;
	CString attrs;
	int graphId;
	bool _ediGraphPosition;
	float _ediGraphLeft;
	float _ediGraphRight;
	float _ediGraphTop;
	float _ediGraphBottom;
	CArray<STreeditorParamDesc*, STreeditorParamDesc*> _scriptParams;
	STreeditorParamGroupPalette _scriptParamPalette;

public:
	CTreeditorClass (ETreeditorClassType t, const char *n)
		: type (t), name (n), selected (true), saved (false), graphId (-1), _ediGraphPosition (false)
	{
		for (int i = 0; i < treeditorClassParam_numberOfTypes; ++i)
		{
			params[i].Init (i);
		}
	}

	virtual ~CTreeditorClass ()
	{
		DeleteScriptParams (_scriptParams);
	}

	bool HasParam (int p) const
	{
		ETreeditorClassParam cp = classTypes[type].params[0];
		for (int i = 0; cp != treeditorClassParam_stop; ++i, cp = classTypes[type].params[i])
		{
			if (cp != p)
			{
				continue;
			}
			
			return true;
		}

		return false;
	}

	bool HasAnyChild ()
	{
		for (int i = 0; i < treeditorClassParam_numberOfTypes; ++i)
		{
			if (!CTreeditorParam::classParams[i].isChildClassName || 
				!HasParam (i))
			{
				continue;
			}
			
			return true;
		}
		
		int n = _scriptParams.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			if (!_scriptParams.GetAt (i)->isChildClassName)
			{
				continue;
			}
			
			return true;
		}

		return false;
	}

	int HasAnyChildOfName (const char *name)
	{
		int res = 0;
		for (int i = 0; i < treeditorClassParam_numberOfTypes; ++i)
		{
			if (!CTreeditorParam::classParams[i].isChildClassName || 
				!HasParam (i) ||
				_tcscmp (params[i].GetValue (), name) != 0)
			{
				continue;
			}
			
			++res;
		}

		int n = _scriptParams.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			if (!_scriptParams[i]->isChildClassName || _tcscmp (_scriptParams[i]->defValue, name) != 0)
			{
				continue;
			}
			
			++res;
		}
		
		return res;
	}

	const char* GetFirstChildClassName (unsigned long *index) const
	{
		for (int i = 0; i < treeditorClassParam_numberOfTypes; ++i)
		{
			if (!CTreeditorParam::classParams[i].isChildClassName || 
				!HasParam (i))
			{
				continue;
			}
			
			*index = i;
			return params[i].GetValue ();
		}
		
		return NULL;
	}

	const char* GetNextChildClassName (unsigned long *index) const
	{
		for (int i = ++(*index); i < treeditorClassParam_numberOfTypes; ++i)
		{
			if (!CTreeditorParam::classParams[i].isChildClassName || 
				!HasParam (i))
			{
				continue;
			}
			
			*index = i;
			return params[i].GetValue ();
		}
		
		return NULL;
	}

	void UpdateScriptParams ()
	{
		DeleteScriptParams (_scriptParams);
		GetScriptParams (&_scriptParams, &_scriptParamPalette);
	}

	void GetScriptParams (CArray<STreeditorParamDesc*, STreeditorParamDesc*> *scriptParams,
		STreeditorParamGroupPalette *palette)
	{
		if (scriptParams != NULL)
		{
			const STreeditorClassTypeDesc &ct = classTypes[type];
			
			for (int i = 0; ct.params[i] != treeditorClassParam_stop; ++i)
			{
				if (ct.params[i] == treeditorClassParam_script)
				{
					CString fn = params[treeditorClassParam_script].GetValue ();
					fn = fn.Mid (1, fn.GetLength () - 2);
					GetScriptParamsFromFile(fn, scriptParams, palette);
				}
			}
		}
	}
	
	static void GetScriptParamsFromFile(LPCTSTR fn, 
		CArray<STreeditorParamDesc*, STreeditorParamDesc*> *scriptParams,
		STreeditorParamGroupPalette *palette)
	{
		if (scriptParams != NULL)
		{
			CFile f;
			if (f.Open(fn, CFile::modeRead, NULL))
			{
				CString str;
				int fileLen = f.GetLength();
				int strLen = fileLen + sizeof (TCHAR) - 1 / sizeof (TCHAR);
				LPTSTR p = str.GetBuffer (strLen);
				bool read = f.Read(p, fileLen) == fileLen;
				str.ReleaseBuffer();
				f.Close();
				
				if (read)
				{
					GetScriptParamsFromString (str, scriptParams, palette);
				}
			}
		}
	}

	static void GetScriptParamsFromString(LPCTSTR str, 
		CArray<STreeditorParamDesc*, STreeditorParamDesc*> *scriptParams,
		STreeditorParamGroupPalette *groupPalette);

	static void DeleteScriptParams (CArray<STreeditorParamDesc*, STreeditorParamDesc*> &scriptParams)
	{
		int n = scriptParams.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			delete scriptParams[i];
		}
		scriptParams.RemoveAll ();
	}
};

#endif // !defined(AFX_TREEDITORCLASS_H__2103FED2_052E_4900_A8AA_C30980843197__INCLUDED_)
