// TreeditorView.cpp : implementation of the CTreeditorView class
//

#include "stdafx.h"
#include <El/elementpch.hpp>
#include "RV/Tree/mytree.h"
#include "Rv/Tree/auxiliary.h"
#include "Treeditor.h"
#include "TreeditorLex.h"
#include "TreeditorClass.h"
#include "RV/Tree/D3DUtil.h"

#include "TreeditorDoc.h"
#include "TreeditorView.h"
#include "ColorDlg.h"
#include "CurveDlg.h"
#include "PreferencesDlg.h"
#include "MainFrm.h"
#include "TextEditor.h"
#include "ScriptParser.h"
#include "PairDlg.h"
#include "PaletteEditor.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#define PUT_TEXT_IN_WND(str, panel, id)		\
{\
	CWnd *ctrl = (CWnd*) ((CMainFrame*) AfxGetMainWnd ())->panel.GetDlgItem (id);\
	if (ctrl != NULL)\
	{\
		ctrl->SetWindowText (str);\
	}\
}

#define GET_INT_FROM_WND(i,panel,id)	\
{\
	CWnd *ctrl = (CWnd*) ((CMainFrame*) AfxGetMainWnd ())->panel.GetDlgItem (id);\
	if (ctrl != NULL)\
	{\
		CString str;\
		ctrl->GetWindowText(str);\
		LPCTSTR start = (LPCTSTR) str;\
		LPTSTR end;\
		int ti = (int) _tcstol (start, &end, 0);\
		if (start != end)\
		{\
			i = ti;\
		}\
	}\
}

#define PUT_INT_IN_WND(i, panel, id)  \
{\
	CWnd *ctrl = (CWnd*) ((CMainFrame*) AfxGetMainWnd ())->panel.GetDlgItem (id);\
	if (ctrl != NULL)\
	{\
		CString str;\
		str.Format (_T ("%d"), i);\
		ctrl->SetWindowText(str);\
	}\
}

#define GET_FLOAT_FROM_WND(f,panel,id)	\
{\
	CWnd *ctrl = (CWnd*) ((CMainFrame*) AfxGetMainWnd ())->panel.GetDlgItem (id);\
	if (ctrl != NULL)\
	{\
		CString str;\
		ctrl->GetWindowText(str);\
		LPCTSTR start = (LPCTSTR) str;\
		LPTSTR end;\
		float tf = (float) _tcstod (start, &end);\
		if (end != start)\
		{\
			f = tf;\
		}\
	}\
}

#define PUT_FLOAT_IN_WND(f, panel, id)  \
{\
	CWnd *ctrl = (CWnd*) ((CMainFrame*) AfxGetMainWnd ())->panel.GetDlgItem (id);\
	if (ctrl != NULL)\
	{\
		CString str;\
		str.Format (_T ("%g"), f);\
		ctrl->SetWindowText(str);\
	}\
}

#define GET_INT_FROM_SLIDER(i,panel,id)  \
{\
	CSliderCtrl *ctrl = (CSliderCtrl*) ((CMainFrame*) AfxGetMainWnd ())->panel.GetDlgItem (id);\
	if (ctrl != NULL)\
	{\
		i = ctrl->GetPos ();\
	}\
}

#define PUT_INT_IN_SLIDER(i, panel, id)  \
{\
	CSliderCtrl *ctrl = (CSliderCtrl*) ((CMainFrame*) AfxGetMainWnd ())->panel.GetDlgItem (id);\
	if (ctrl != NULL)\
	{\
		ctrl->SetPos (i);\
	}\
}

#define SET_SLIDER(i, min, max, panel, id)  \
{\
	CSliderCtrl *ctrl = (CSliderCtrl*) ((CMainFrame*) AfxGetMainWnd ())->panel.GetDlgItem (id);\
	if (ctrl != NULL)\
	{\
		ctrl->SetRange (min, max, TRUE);\
		ctrl->SetPos (i);\
	}\
}

#define SET_SPIN(min, max, panel, id) \
{\
	CSpinButtonCtrl *ctrl = (CSpinButtonCtrl*) ((CMainFrame*) AfxGetMainWnd ())->panel.GetDlgItem (id);\
	if (ctrl != NULL)\
	{\
		ctrl->SetRange32 (min, max);\
	}\
}

#define SET_EDIT_FOCUS(panel, id)	\
{\
	CEdit *ctrl = (CEdit*) ((CMainFrame*) AfxGetMainWnd ())->panel.GetDlgItem (id);\
	if (ctrl != NULL)\
	{\
		ctrl->SetFocus ();\
		ctrl->SetSel (0, -1);\
	}\
}

#define MOVE_STEP 0.2f
#define ROT_STEP ((float) (3.1415926 / 45.0))
#define MAX2(a,b) ((a) > (b) ? (a) : (b))
#define MAX3(a,b,c) MAX2(a,MAX2(b,c))
#define HALF_OF_SUB(a,b) (((a) - (b)) >> 1)

#define FURTHEST_DISTANCE 500.f

struct STreeditorCustomVertex
{
    FLOAT x, y, z;        // The position.
    FLOAT nx, ny, nz;     // The surface normal for the vertex.
    DWORD color;          // The color.
    FLOAT tu, tv;         // The texture coordinates.
};


bool CTreeditorView::_viewInitialized = false;

int g_N0 = 0;
int g_N1 = 0;

/////////////////////////////////////////////////////////////////////////////
// CTreeditorView

IMPLEMENT_DYNCREATE(CTreeditorView, CFormView)

BEGIN_MESSAGE_MAP(CTreeditorView, CFormView)
	//{{AFX_MSG_MAP(CTreeditorView)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_CLASSES, OnItemChangedClasses)
	ON_NOTIFY(LVN_ITEMCHANGING, IDC_CLASSES, OnItemChangingClasses)
	ON_UPDATE_COMMAND_UI(ID_NEW_CLASS, OnUpdateNewClass)
	ON_UPDATE_COMMAND_UI(ID_DELETE_CLASS, OnUpdateDeleteClass)
	ON_COMMAND(ID_NEW_CLASS, OnNewClass)
	ON_NOTIFY(LVN_ENDLABELEDIT, IDC_CLASSES, OnEndLabelEditClasses)
	ON_COMMAND(ID_DELETE_CLASS, OnDeleteClass)
	ON_CBN_SELCHANGE(IDC_CLASSTYPE, OnSelChangeClassType)
	ON_NOTIFY(LVN_KEYDOWN, IDC_CLASSES, OnKeyDownClasses)
	ON_NOTIFY(NM_DBLCLK, IDC_CLASSES, OnDblclkClasses)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_PARAMSLIDER, OnReleasedCaptureParamSlider)
	ON_WM_SIZE()
	ON_COMMAND(ID_CLASSES_DOWN, OnClassesDown)
	ON_UPDATE_COMMAND_UI(ID_CLASSES_DOWN, OnUpdateClassesDown)
	ON_COMMAND(ID_CLASSES_UP, OnClassesUp)
	ON_UPDATE_COMMAND_UI(ID_CLASSES_UP, OnUpdateClassesUp)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CUT, OnUpdateEditCut)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_CUT, OnEditCut)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, OnUpdateEditPaste)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_WM_HSCROLL()
	ON_COMMAND(ID_CHANGEBGCOLOR, OnChangeBgColor)
	ON_COMMAND(ID_ANIMATE, OnAnimate)
	ON_UPDATE_COMMAND_UI(ID_ANIMATE, OnUpdateAnimate)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_DETAILSLIDER, OnReleasedCaptureDetailSlider)
	ON_COMMAND(ID_TREEBACKWARD, OnTreeBackward)
	ON_COMMAND(ID_TREEFORWARD, OnTreeForward)
	ON_COMMAND(ID_TREELEFT, OnTreeLeft)
	ON_COMMAND(ID_TREERIGHT, OnTreeRight)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_TREESSLIDER, OnReleasedCaptureTreesSlider)
	ON_EN_CHANGE(IDC_DETAIL, OnChangeDetail)
	ON_EN_CHANGE(IDC_TREES, OnChangeTrees)
	ON_EN_CHANGE(IDC_DIST, OnChangeDist)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_DISTSLIDER, OnReleasedCaptureDistSlider)
	ON_EN_KILLFOCUS(IDC_DETAIL, OnKillFocusDetail)
	ON_EN_KILLFOCUS(IDC_DIST, OnKillFocusDist)
	ON_EN_KILLFOCUS(IDC_TREES, OnKillFocusTrees)
	ON_COMMAND(ID_IMMEDIATERESPONSE, OnImmediateResponse)
	ON_UPDATE_COMMAND_UI(ID_IMMEDIATERESPONSE, OnUpdateImmediateResponse)
	ON_COMMAND(ID_TREEDOWN, OnTreeDown)
	ON_COMMAND(ID_TREEUP, OnTreeUp)
	ON_COMMAND(ID_TREEROTDOWN, OnTreeRotDown)
	ON_COMMAND(ID_TREEROTLEFT, OnTreeRotLeft)
	ON_COMMAND(ID_TREEROTRIGHT, OnTreeRotRight)
	ON_COMMAND(ID_TREEROTUP, OnTreeRotUp)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_EN_CHANGE(IDC_METER, OnChangeMeter)
	ON_EN_KILLFOCUS(IDC_METER, OnKillFocusMeter)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_METERSLIDER, OnReleasedCaptureMeterSlider)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_LROTYSLIDER, OnReleasedCaptureLRotYSlider)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_LROTZSLIDER, OnReleasedCaptureLRotZSlider)
	ON_COMMAND(ID_CENTERTREEVIEW, OnCenterTreeView)
	ON_UPDATE_COMMAND_UI(ID_SHOWMETER, OnUpdateShowMeter)
	ON_COMMAND(ID_SHOWMETER, OnShowMeter)
	ON_EN_CHANGE(IDC_AGE, OnChangeAge)
	ON_EN_KILLFOCUS(IDC_AGE, OnKillFocusAge)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_AGESLIDER, OnReleasedCaptureAgeSlider)
	ON_EN_CHANGE(IDC_WIND, OnChangeWind)
	ON_EN_KILLFOCUS(IDC_WIND, OnKillFocusWind)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_WINDSLIDER, OnReleasedCaptureWindSlider)
	ON_COMMAND(ID_WIREFRAME, OnWireframe)
	ON_UPDATE_COMMAND_UI(ID_WIREFRAME, OnUpdateWireframe)
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_COMMAND(ID_SHOWBIGLIGHTPYRAMID, OnShowBigLightPyramid)
	ON_UPDATE_COMMAND_UI(ID_SHOWBIGLIGHTPYRAMID, OnUpdateShowBigLightPyramid)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_WROTYSLIDER, OnReleasedCaptureWRotYSlider)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_WROTZSLIDER, OnReleasedCaptureWRotZSlider)
	ON_COMMAND(ID_SHOWBIGWINDPYRAMID, OnShowBigWindPyramid)
	ON_UPDATE_COMMAND_UI(ID_SHOWBIGWINDPYRAMID, OnUpdateShowBigWindPyramid)
	ON_COMMAND(ID_AUTODETAIL, OnAutoDetail)
	ON_UPDATE_COMMAND_UI(ID_AUTODETAIL, OnUpdateAutoDetail)
	ON_EN_CHANGE(IDC_NEAR, OnChangeNear)
	ON_EN_KILLFOCUS(IDC_NEAR, OnKillFocusNear)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_NEARSLIDER, OnReleasedCaptureNearSlider)
	ON_EN_CHANGE(IDC_FAR, OnChangeFar)
	ON_EN_KILLFOCUS(IDC_FAR, OnKillFocusFar)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_FARSLIDER, OnReleasedCaptureFarSlider)
	ON_EN_KILLFOCUS(IDC_SEED, OnKillFocusSeed)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SEEDSPIN, OnDeltaPosSeedSpin)
	ON_EN_CHANGE(IDC_SEED, OnChangeSeed)
	ON_COMMAND(ID_EDIT_UNDO, OnEditUndo)
	ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO, OnUpdateEditUndo)
	ON_COMMAND(ID_EDIT_REDO, OnEditRedo)
	ON_UPDATE_COMMAND_UI(ID_EDIT_REDO, OnUpdateEditRedo)
	ON_COMMAND(ID_CHANGEGROUND, OnChangeGround)
	ON_COMMAND(ID_SHOWGROUND, OnShowGround)
	ON_UPDATE_COMMAND_UI(ID_SHOWGROUND, OnUpdateShowGround)
	ON_COMMAND(ID_EDIT_PREFERENCES, OnEditPreferences)
	ON_COMMAND(ID_EQUALIZER_LINEAR, OnEqualizerLinear)
	ON_UPDATE_COMMAND_UI(ID_EQUALIZER_LINEAR, OnUpdateEqualizerLinear)
	ON_COMMAND(ID_EQUALIZER_QUADRATIC, OnEqualizerQuadratic)
	ON_UPDATE_COMMAND_UI(ID_EQUALIZER_QUADRATIC, OnUpdateEqualizerQuadratic)
	ON_COMMAND(ID_VIEW_REFRESH, OnViewRefresh)
	ON_COMMAND(ID_EDIT_SELALLPROP, OnEditSelAllProp)
	ON_COMMAND(ID_EDIT_DESELALLPROPS, OnEditDeselAllProps)
	ON_COMMAND(ID_CLASS_EDITPALETTE, OnClassEditPalette)
	ON_UPDATE_COMMAND_UI(ID_CLASS_EDITPALETTE, OnUpdateClassEditPalette)
	ON_COMMAND(ID_VIEW_PREVIEWON, OnViewPreviewOn)
	ON_UPDATE_COMMAND_UI(ID_VIEW_PREVIEWON, OnUpdateViewPreviewOn)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTreeditorView construction/destruction

CTreeditorView::CTreeditorView()
	: CFormView(CTreeditorView::IDD), _updateClassesTime (false), _updatePropertiesTime (false), 
	_updateComponentsTime (false), _enumPropItem (-1), _enumParamId (-1), 
	m_bgColor (D3DCOLOR_XRGB (192, 192, 208)), m_animate (false), m_immediateResponse (false),
	m_buttonDown (0), m_showMeter (true), m_wireframe (false), m_showBigLightPyramid (false), 
	m_showBigWindPyramid (false), m_treeGenerated (false), m_autoDetail (false),
	_currentUndo (NULL), m_showGround (true)
{
	//{{AFX_DATA_INIT(CTreeditorView)
	//}}AFX_DATA_INIT
	// TODO: add construction code here

	m_animate = AfxGetApp ()->GetProfileInt (_T ("tree"), _T ("animate"), 0) != 0;
	m_immediateResponse = AfxGetApp ()->GetProfileInt (_T ("editor"), _T ("immediateResponse"), 1) != 0;
	m_bgColor = (D3DCOLOR) AfxGetApp ()->GetProfileInt (_T ("editor"), _T ("bgColor"), RGB (128, 128, 128));
	m_showMeter = AfxGetApp ()->GetProfileInt (_T ("meter"), _T ("show"), 1) != 0;
	m_showGround = AfxGetApp ()->GetProfileInt (_T ("ground"), _T ("show"), 1) != 0;
	m_wireframe = AfxGetApp ()->GetProfileInt (_T ("editor"), _T ("wireframe"), 0) != 0;
	m_showBigLightPyramid = AfxGetApp ()->GetProfileInt (_T ("light"), _T ("showPyramid"), 0) != 0;
	m_showBigWindPyramid = AfxGetApp ()->GetProfileInt (_T ("wind"), _T ("showPyramid"), 0) != 0;
	m_autoDetail = AfxGetApp ()->GetProfileInt (_T ("editor"), _T ("autodetail"), 0) != 0;

	for (int i = 0; i < treeditorClassParam_numberOfTypes; ++i)
	{
		_lastPropsSelMask[i] = false;
	}

	D3DXMatrixTranslation (&_matZoomTranslation, 0.0f, -1.0f, 5.0f);
	D3DXMatrixIdentity (&_matZoomRotation);
	D3DXMatrixTranslation (&_matZoomForward, 0.0f, 0.0f, MOVE_STEP);
	D3DXMatrixTranslation (&_matZoomBackward, 0.0f, 0.0f, -MOVE_STEP);
	D3DXMatrixTranslation (&_matZoomLeft, MOVE_STEP, 0.0f, 0.0f);
	D3DXMatrixTranslation (&_matZoomRight, -MOVE_STEP, 0.0f, 0.0f);
	D3DXMatrixTranslation (&_matZoomUp, 0.0f, -MOVE_STEP, 0.0f);
	D3DXMatrixTranslation (&_matZoomDown, 0.0f, MOVE_STEP, 0.0f);
	D3DXMatrixRotationY (&_matRotLeft, ROT_STEP);
	D3DXMatrixRotationY (&_matRotRight, -ROT_STEP);
	D3DXMatrixRotationX (&_matRotUp, ROT_STEP);
	D3DXMatrixRotationX (&_matRotDown, -ROT_STEP);
}

CTreeditorView::~CTreeditorView()
{
	DeleteTrees ();
}

void CTreeditorView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTreeditorView)
	DDX_Control(pDX, IDC_RENDERVIEW, _renderView);
	//}}AFX_DATA_MAP
}

BOOL CTreeditorView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CTreeditorView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate ();
	//GetParentFrame ()->RecalcLayout ();
	//ResizeParentToFit ();

	UpdateClasses ();
	if (theApp.m_d3dDevice8 != NULL)
	{
		GetDocument ()->UpdateStamp (true, false);
		UpdateRender (INIT_METER | INIT_TREE | INIT_GROUND | INIT_LIGHT_AND_WIND);
	}
}

void CTreeditorView::UpdateClasses ()
{
	CListCtrl *classes = (CListCtrl*) ((CMainFrame*) AfxGetMainWnd ())->_componentBar.GetDlgItem (IDC_CLASSES);
	CListCtrl *infos = (CListCtrl*) ((CMainFrame*) AfxGetMainWnd ())->_infoBar.GetDlgItem (IDC_INFOLIST);
	if (!_viewInitialized)
	{

#define TO_CHARPTR(x) #x

#define PUT_PROFILE_STRING(section, entry, def, panel, idc)	\
{\
	CString str = AfxGetApp ()->GetProfileString (section, entry, def);\
	PUT_TEXT_IN_WND(str, panel, idc);\
}

#define PUT_PROFILE_INT(section, entry, def, panel, idc)	\
{\
	int i = AfxGetApp ()->GetProfileInt (section, entry, def);\
	PUT_INT_IN_WND(i, panel, idc);\
}

#ifndef DETAIL_IS_N_POLYGONS
		PUT_PROFILE_STRING (_T ("tree"), _T ("detail"), _T ("1.0"), _componentBar, IDC_DETAIL)
#else
		PUT_PROFILE_STRING (_T ("tree"), _T ("detail"), _T (TO_CHARPTR (N_POLYGONS_MAX)), _componentBar, IDC_DETAIL)
#endif
		PUT_PROFILE_INT    (_T ("tree"), _T ("number"), 1, _componentBar, IDC_TREES)
		PUT_PROFILE_STRING (_T ("tree"), _T ("distance"), _T ("1.0"), _componentBar, IDC_DIST)
		PUT_PROFILE_STRING (_T ("meter"), _T ("size"), _T ("1.0"), _componentBar, IDC_METER)
		PUT_PROFILE_STRING (_T ("tree"), _T ("age"), _T ("1.0"), _componentBar, IDC_AGE)
		PUT_PROFILE_STRING (_T ("wind"), _T ("strength"), _T ("1.0"), _componentBar, IDC_WIND)
		PUT_PROFILE_STRING (_T ("tree"), _T ("near"), _T ("0.0"), _componentBar, IDC_NEAR)
		PUT_PROFILE_STRING (_T ("tree"), _T ("far"), _T ("500.0"), _componentBar, IDC_FAR)
		PUT_PROFILE_INT    (_T ("tree"), _T ("seed"), 0, _componentBar, IDC_SEED)

		classes->SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
		int i = AfxGetApp ()->GetProfileInt (_T ("editor"), _T ("classNameColumnWidth"), 115);
		classes->InsertColumn (0, "Class Name", LVCFMT_LEFT, i);
		i = AfxGetApp ()->GetProfileInt (_T ("editor"), _T ("attrColumnWidth"), 60);
		classes->InsertColumn (1, "Attr", LVCFMT_LEFT, i);
		CComboBox *classTypeCtrl = (CComboBox*) ((CMainFrame*) AfxGetMainWnd ())->_propertyBar.GetDlgItem (IDC_CLASSTYPE);
		for (int i = 0; i < treeditorClass_numberOfTypes; ++i)
		{
			classTypeCtrl->AddString (CTreeditorClass::classTypes[i].name);
		}
		_viewInitialized = true;

		float  f;

#ifndef DETAIL_IS_N_POLYGONS
		f = 1.0f;
		GET_FLOAT_FROM_WND (f, _componentBar, IDC_DETAIL)
		SET_SLIDER((int) (f * 100), 0, 100, _componentBar, IDC_DETAILSLIDER)
#else
		int d = N_POLYGONS_MAX;
		GET_INT_FROM_WND (d, _componentBar, IDC_DETAIL)
		SET_SLIDER(d, 0, N_POLYGONS_MAX, _componentBar, IDC_DETAILSLIDER)
#endif

		GET_FLOAT_FROM_WND (f, _componentBar, IDC_NEAR)
		SET_SLIDER((int) f, 0, (int) FURTHEST_DISTANCE, _componentBar, IDC_NEARSLIDER)

		GET_FLOAT_FROM_WND (f, _componentBar, IDC_FAR)
		SET_SLIDER((int) f, 0, (int) FURTHEST_DISTANCE, _componentBar, IDC_FARSLIDER)

		GET_FLOAT_FROM_WND (f, _componentBar, IDC_TREES)
		SET_SLIDER((int) f, 1, MAX_TREES, _componentBar, IDC_TREESSLIDER)

		GET_FLOAT_FROM_WND (f, _componentBar, IDC_DIST)
		SET_SLIDER((int) (f * 100.0f), 1, 1000, _componentBar, IDC_DISTSLIDER)

		GET_FLOAT_FROM_WND (f, _componentBar, IDC_METER)
		SET_SLIDER((int) (f * 100.0f), 1, 1000, _componentBar, IDC_METERSLIDER)

		GET_FLOAT_FROM_WND (f, _componentBar, IDC_AGE)
		SET_SLIDER((int) (f * 100.0f), 0, 100, _componentBar, IDC_AGESLIDER)

		GET_FLOAT_FROM_WND (f, _componentBar, IDC_WIND)
		SET_SLIDER((int) (f * 10.0f), 0, 100, _componentBar, IDC_WINDSLIDER)

		SET_SPIN(INT_MIN, INT_MAX, _componentBar, IDC_SEEDSPIN)

		int rot = AfxGetApp ()->GetProfileInt(_T ("wind"), _T ("roty"), 210);
		SET_SLIDER(rot, 0, 360, _componentBar, IDC_WROTYSLIDER)

		rot = AfxGetApp ()->GetProfileInt(_T ("wind"), _T("rotz"), 90);
		SET_SLIDER(rot, 0, 360, _componentBar, IDC_WROTZSLIDER)

		CString str;
		GET_INT_FROM_SLIDER(rot, _componentBar, IDC_WROTYSLIDER)
		str.Format ("Wind RotY: %d", rot);
		PUT_TEXT_IN_WND(str, _componentBar, IDC_WROTYLABEL)
		GET_INT_FROM_SLIDER(rot, _componentBar, IDC_WROTZSLIDER)
		str.Format ("Wind RotZ: %d", rot);
		PUT_TEXT_IN_WND(str, _componentBar, IDC_WROTZLABEL)

		rot = AfxGetApp ()->GetProfileInt (_T ("light"), _T("roty"), 60);
		SET_SLIDER(rot, 0, 360, _componentBar, IDC_LROTYSLIDER)

		rot = AfxGetApp()->GetProfileInt(_T("light"), _T("rotz"), 120);
		SET_SLIDER(rot, 0, 360, _componentBar, IDC_LROTZSLIDER)

		GET_INT_FROM_SLIDER(rot, _componentBar, IDC_LROTYSLIDER)
		str.Format ("Light RotY: %d", rot);
		PUT_TEXT_IN_WND(str, _componentBar, IDC_LROTYLABEL)
		GET_INT_FROM_SLIDER(rot, _componentBar, IDC_LROTZSLIDER)
		str.Format ("Light RotZ: %d", rot);
		PUT_TEXT_IN_WND(str, _componentBar, IDC_LROTZLABEL)

#undef PUT_PROFILE_STRING
#undef PUT_PROFILE_INT
#undef TO_CHARPTR

		infos->SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
		i = AfxGetApp ()->GetProfileInt (_T ("editor"), _T ("infoNameColumnWidth"), 115);
		infos->InsertColumn (0, "Name", LVCFMT_LEFT, i);
		i = AfxGetApp ()->GetProfileInt (_T ("editor"), _T ("infoValueColumnWidth"), 60);
		infos->InsertColumn (1, "Value", LVCFMT_LEFT, i);
	}

	CTreeditorDoc *doc = GetDocument ();

	_updateClassesTime = true;
	classes->DeleteAllItems ();
	infos->DeleteAllItems ();
	((CMainFrame*) AfxGetMainWnd())->_graphBar._graphCtrl.DeleteAllItems();

	unsigned long i;
	CString tstr;
	for (CTreeditorClass *c = doc->GetFirstClass (&i); c != NULL; c = doc->GetNextClass (&i))
	{
		classes->InsertItem (
			LVIF_TEXT | LVIF_STATE | LVIF_PARAM, i - 1, c->name, 
			c->selected ? LVIS_SELECTED : 0, LVIS_SELECTED, 
			0, (LPARAM) c);

		tstr.Format (_T ("componentNodeColor%02d"), c->type);
		DWORD color = AfxGetApp ()->GetProfileInt(_T ("editor"), tstr, RGB (128, 128, 255));
		if (c->_ediGraphPosition)
		{
			c->graphId = ((CMainFrame*) AfxGetMainWnd())->_graphBar._graphCtrl.InsertItem (
				SGRI_NOGUESSPOSITION, color, c->name, 0.0f, 0.0f, c);
			SGraphItem gi;
			if (((CMainFrame*) AfxGetMainWnd())->_graphBar._graphCtrl.GetItem (c->graphId, &gi))
			{
				gi.flags = SGRI_CANMOVE|SGRI_CANRESIZE|SGRI_CANSELFLINKED|SGRI_CANBELINKED|SGRI_CANMAKELINKS;
				gi.rect.left = c->_ediGraphLeft;
				gi.rect.right = c->_ediGraphRight;
				gi.rect.top = c->_ediGraphTop;
				gi.rect.bottom = c->_ediGraphBottom;
				((CMainFrame*) AfxGetMainWnd())->_graphBar._graphCtrl.SetItem (c->graphId, &gi);
			}
		}
		else
		{
			c->graphId = ((CMainFrame*) AfxGetMainWnd())->_graphBar._graphCtrl.InsertItem (
				SGRI_CANMOVE|SGRI_CANRESIZE|SGRI_CANSELFLINKED|SGRI_CANBELINKED|SGRI_CANMAKELINKS,
				color, c->name, 1.0f, 0.5, c);
			SGraphItem gi;
			if (((CMainFrame*) AfxGetMainWnd())->_graphBar._graphCtrl.GetItem (c->graphId, &gi))
			{
				c->_ediGraphPosition = true;
				c->_ediGraphLeft = gi.rect.left;
				c->_ediGraphRight = gi.rect.right;
				c->_ediGraphTop = gi.rect.top;
				c->_ediGraphBottom = gi.rect.bottom;
				doc->SetModifiedFlag (TRUE);
			}
		}
	}
	((CMainFrame*) AfxGetMainWnd())->_graphBar._graphCtrl.OrderLinks();
	((CMainFrame*) AfxGetMainWnd())->_graphBar.ResetZoom();
	_updateClassesTime = false;

	UpdateProps ();
	UpdateClassType ();
	UpdateAttrs ();
	UpdateSlider ();

	CheckSelectedClasses ();
}

void CTreeditorView::UpdateAttrs ()
{
	CTreeditorDoc *doc = GetDocument ();

	((CMainFrame*) AfxGetMainWnd())->_graphBar._graphCtrl.DeleteAllLinks ();
	unsigned long i;
	CString tstr;
	for (CTreeditorClass *c = doc->GetFirstClass (&i); c != NULL; c = doc->GetNextClass (&i))
	{
		unsigned long childIt;
		ETreeditorClassParam childKind;
		for (CTreeditorClass *child = doc->GetFirstChild (c, &childIt, &childKind); child != NULL; child = doc->GetNextChild (c, &childIt, &childKind))
		{
			tstr.Format (_T ("childLinkColor%02d"), childKind);
			DWORD color = AfxGetApp ()->GetProfileInt(_T ("editor"), tstr, 0);
			((CMainFrame*) AfxGetMainWnd())->_graphBar._graphCtrl.InsertLink (c->graphId, child->graphId, color, 0, (void*) childKind);
		}
		
		int spIdx = treeditorClassParam_numberOfTypes;
		int spSize = c->_scriptParams.GetSize ();
		for (int i = 0; i < spSize; ++i)
		{
			if (c->_scriptParams[i]->isChildClassName)
			{
				CTreeditorClass *child = doc->FindClass(c->_scriptParams[i]->defValue);
				if (child != NULL)
				{
					tstr.Format (_T ("childLinkColor_%s"), c->_scriptParams[i]->uiName);
					DWORD color = AfxGetApp ()->GetProfileInt(_T ("editor"), tstr, 0);
					((CMainFrame*) AfxGetMainWnd())->_graphBar._graphCtrl.InsertLink (c->graphId, child->graphId, color, 0, (void*) spIdx);
				}
			}
			++spIdx;
		}
	}
	((CMainFrame*) AfxGetMainWnd())->_graphBar._graphCtrl.OrderLinks ();
	((CMainFrame*) AfxGetMainWnd())->_graphBar._graphCtrl.Update ();

	doc->UpdateClassAttrs ();

	CListCtrl *classes = (CListCtrl*) ((CMainFrame*) AfxGetMainWnd ())->_componentBar.GetDlgItem (IDC_CLASSES);
	for (CTreeditorClass *c = doc->GetFirstClass (&i); c != NULL; c = doc->GetNextClass (&i))
	{
		classes->SetItemText (i - 1, 1, c->attrs);
	}
}

void CTreeditorView::UpdateClassType ()
{
	CTreeditorDoc *doc = GetDocument ();
	int classType = treeditorClass_unknown;
	bool selClass = false;
	unsigned long i;

	for (CTreeditorClass *c = doc->GetFirstClass (&i); c != NULL; c = doc->GetNextClass (&i))
	{
		if (c->selected)
		{
			if (selClass)
			{
				if (classType != c->type)
				{
					classType = treeditorClass_unknown;
					break;
				}
			}
			else
			{
				selClass = true;
				classType = c->type;
			}
		}
	}

	CComboBox *classTypeCtrl = (CComboBox*) ((CMainFrame*) AfxGetMainWnd ())->_propertyBar.GetDlgItem (IDC_CLASSTYPE);
	classTypeCtrl->SetCurSel (classType);
}

void CTreeditorView::UpdateProps ()
{
	CTreeditorDoc *doc = GetDocument ();
	
	bool parInit[treeditorClassParam_numberOfTypes];
	bool params[treeditorClassParam_numberOfTypes];
	bool selMask[treeditorClassParam_numberOfTypes];
	unsigned long i;
	for (i = 0; i < treeditorClassParam_numberOfTypes; i++)
	{
		parInit[i] = false;
		params[i] = true;
		selMask[i] = false;
	}
	
	CUiPropertiesCtrl &props = ((CMainFrame*) AfxGetMainWnd ())->_propertyBar._propCtrl;

	int pos = props.GetNextSelectedProp (-1);
	while (pos >= 0)
	{
		i = props.GetPropData (pos);
		if (i < treeditorClassParam_numberOfTypes)
		{
			selMask[i] = true;
		}
		pos = props.GetNextSelectedProp (pos);
	}
	
	props.DeleteAllProps ();
	
	bool emptyProps = true;
	CString parVals[treeditorClassParam_numberOfTypes];
	int classType = treeditorClass_unknown;
	
	for (CTreeditorClass *c = doc->GetFirstSelectedClass (&i); c != NULL; c = doc->GetNextSelectedClass (&i))
	{
		if (emptyProps)
		{
			classType = c->type;
			emptyProps = false;
		}
		else
		{
			if (classType != c->type)
			{
				classType = treeditorClass_unknown;
			}
		}
		
		for (int pi = 0; pi < treeditorClassParam_numberOfTypes; ++pi)
		{
			params[pi] &= c->HasParam (pi);
		}
		
		for (unsigned long j = 0; j < treeditorClassParam_numberOfTypes; ++j)
		{
			if (params[j])
			{
				if (parInit[j])
				{
					if (parVals[j] != c->params[j].GetValue ())
					{
						parVals[j] = "";
					}
				}
				else
				{
					parVals[j] = c->params[j].GetValue ();
					parInit[j] = true;
				}
			}
		}
	}
	
	if (!emptyProps)
	{
		for (i = 0; ; ++i)
		{
			if (i == treeditorClassParam_numberOfTypes)
			{
				CopyMemory (&selMask[0], &_lastPropsSelMask[0], sizeof (selMask));
				break;
			}
			else if (selMask[i])
			{
				break;
			}
		}
		
		CComboBox *classTypeCtrl = (CComboBox*) ((CMainFrame*) AfxGetMainWnd ())->_propertyBar.GetDlgItem (IDC_CLASSTYPE);
		classTypeCtrl->SetCurSel (classType);
		CChannel<CTreeditorView> channel (this);
		CStringArray enumStrs;
		CStringArray childStrs;
		
		for (i = 0; i < treeditorClassParam_numberOfTypes; ++i)
		{
			if (params[i])
			{
				const STreeditorParamDesc *desc = &CTreeditorParam::classParams[i];
				if ((USE_PROPERTY_ENUM_COMBO & desc->uiUse) != 0)
				{
					enumStrs.RemoveAll ();
					for (int k = 0; k < desc->enumType->_size; ++k)
					{
						enumStrs.Add (desc->enumType->_ids[k]);
					}
					props.AddUiProperty (desc->uiName, desc->group.colorIndex, channel, i, desc->uiUse, selMask[i], parVals[i], desc->enabled, &enumStrs);
				}
				else if ((USE_PROPERTY_CHILD_COMBO & desc->uiUse) != 0)
				{
					childStrs.RemoveAll ();
					unsigned long k;
					for (CTreeditorClass *c = doc->GetFirstClass (&k); c != NULL; c = doc->GetNextClass (&k))
					{
						childStrs.Add (c->name);
					}
					props.AddUiProperty (desc->uiName, desc->group.colorIndex, channel, i, desc->uiUse, selMask[i], parVals[i], desc->enabled, &childStrs);
				}
				else
				{
					props.AddUiProperty (desc->uiName, desc->group.colorIndex, channel, i, desc->uiUse, selMask[i], parVals[i], desc->enabled);
				}
			}
		}
		CopyMemory (&_lastPropsSelMask[0], &selMask[0], sizeof (selMask));
	}

	// add script params
	UpdateScriptProps ();
}

void CTreeditorView::UpdateScriptProps ()
{
	CPropertyBar &propbar = ((CMainFrame*) AfxGetMainWnd ())->_propertyBar;
	CUiPropertiesCtrl &props = propbar._propCtrl;

	int n = props.GetPropCount ();
	for (int i = 0; i < n; )
	{
		if (props.GetPropData (i) >= treeditorClassParam_numberOfTypes)
		{
			props.DeleteProp (i);
			--n;
		}
		else
		{
			++i;
		}
	}

	CChannel<CTreeditorView> channel (this);
	CStringArray enumStrs;
	CStringArray childStrs;

	CTreeditorDoc *doc = GetDocument ();

	doc->UpdateScriptParams ();

	n = props.GetPropCount ();
	int i = 0;
	for (; ; ++i)
	{
		if (i == n)
		{
			break;
		}

		if (props.GetPropData (i) != treeditorClassParam_script)
		{
			continue;
		}

		CString scriptFile = props.GetPropValue (i);
		
		CTreeditorClass *c = doc->GetSelectedClass ();
		if (scriptFile == c->params[treeditorClassParam_script].GetValue ())
		{
			int n = c->_scriptParams.GetSize();
			for (i = 0; i < n; ++i)
			{
				const STreeditorParamDesc *desc = c->_scriptParams[i];
				const STreeditorParamGroupPalette &pal = c->_scriptParamPalette;

				if ((desc->uiUse & USE_PROPERTY_ENUM_COMBO) != 0)
				{
					enumStrs.RemoveAll ();
					for (int k = 0; k < desc->enumType->_size; ++k)
					{
						enumStrs.Add (desc->enumType->_ids[k]);
					}
					props.AddUiProperty (desc->uiName, pal.colors[desc->group.colorIndex], channel, treeditorClassParam_numberOfTypes + i, desc->uiUse, false, desc->defValue, desc->enabled, &enumStrs);
				}
				else if ((desc->uiUse & USE_PROPERTY_CHILD_COMBO) != 0)
				{
					unsigned long k;
					childStrs.RemoveAll ();
					for (CTreeditorClass *c = doc->GetFirstClass (&k); c != NULL; c = doc->GetNextClass (&k))
					{
						childStrs.Add (c->name);
					}
					props.AddUiProperty (desc->uiName, pal.colors[desc->group.colorIndex], channel, treeditorClassParam_numberOfTypes + i, desc->uiUse, false, desc->defValue, desc->enabled, &childStrs);
				}
				else
				{
					props.AddUiProperty (desc->uiName, pal.colors[desc->group.colorIndex], channel, treeditorClassParam_numberOfTypes + i, desc->uiUse, false, desc->defValue, desc->enabled);
				}
			}
		}
		break;
	}

	props.RecalcLayout ();

	if (propbar.IsFloating ())
	{
		int h, w;
		propbar.GetOptimalSize (w, h);
		RECT r;
		CFrameWnd *parent = propbar.GetParentFrame ();
		parent->GetWindowRect (&r);
		propbar.CalcDynamicLayout (h, LM_LENGTHY);
		propbar.CalcDynamicLayout (w, 0);
		((CMainFrame*) AfxGetMainWnd ())->FloatControlBar (&propbar, CPoint (r.left, r.top), CBRS_ALIGN_TOP | CBRS_SIZE_DYNAMIC);
		//CWnd *parent = propbar.GetParentFrame ();
		//propbar.CalcDynamicLayout(h, LM_LENGTHY);
		//parent->SendMessage (WM_SIZE, 0, (h << 16) | w);
		//parent->MoveWindow (0, 0, w, h);
	}
}

void CTreeditorView::CheckSelectedClasses ()
{
	CTreeditorDoc *doc = GetDocument ();
	doc->DeselectAllClasses ();
	CListCtrl *classes = (CListCtrl*) ((CMainFrame*) AfxGetMainWnd ())->_componentBar.GetDlgItem (IDC_CLASSES);

	POSITION pos = classes->GetFirstSelectedItemPosition ();
	while (pos != NULL)
	{
		int item = classes->GetNextSelectedItem (pos);
		doc->SelectClass (item);
	}

	unsigned long i;
	for (CTreeditorClass *c = doc->GetFirstClass(&i); c != NULL; c = doc->GetNextClass(&i))
	{
		((CMainFrame*) AfxGetMainWnd())->_graphBar._graphCtrl.SelectItem(c->graphId, c->selected);
	}
}

void CTreeditorView::UpdateSlider ()
{
	CSliderCtrl *sliderCtrl = (CSliderCtrl*) ((CMainFrame*) AfxGetMainWnd ())->_propertyBar.GetDlgItem (IDC_PARAMSLIDER);
	CUiPropertiesCtrl &props = ((CMainFrame*) AfxGetMainWnd ())->_propertyBar._propCtrl;
	UINT sc = props.GetSelectedPropCount ();
	int pos = props.GetNextSelectedProp (-1);
	CTreeditorClass *c = GetDocument ()->GetSelectedClass ();
	if (sc == 1 && c != NULL && pos != -1)
	{
		pos = props.GetPropData (pos);
		const STreeditorParamDesc *param = NULL;
		LPCTSTR value = NULL;
		if (pos < treeditorClassParam_numberOfTypes)
		{
			param = &CTreeditorParam::classParams[pos];
			value = c->params[pos].GetValue ();
		}
		else
		{
			param = c->_scriptParams[pos - treeditorClassParam_numberOfTypes];
			value = param->defValue;
		}

		sliderCtrl->EnableWindow ((param->useWhat & USE_SLIDER) != 0);
		if ((param->useWhat & USE_SLIDER) != 0)
		{
			float diff = param->maxValue - param->minValue;
			_sliderExp = 1;
			while (diff < (INT_MAX >> 1))
			{
				diff *= 2;
				_sliderExp *= 2;
			}
			_sliderDiff = (int) diff;
			sliderCtrl->SetRange (0, _sliderDiff, FALSE);
			CTreeditorStringInput inp (value);
			CTreeditorLex lex (&inp, NULL, 0);
			STreeditorTokenDesc token;
			lex.GetNextToken (&token);
			if (token.fnumber > param->maxValue)
			{
				token.fnumber = param->maxValue;
			}
			else if (token.fnumber < param->minValue)
			{
				token.fnumber = param->minValue;
			}
			sliderCtrl->SetPos ((int) ((token.fnumber - param->minValue) * _sliderExp));
			
			CWnd *min = ((CMainFrame*) AfxGetMainWnd ())->_propertyBar.GetDlgItem (IDC_MINSLIDERVALUE);
			CWnd *max = ((CMainFrame*) AfxGetMainWnd ())->_propertyBar.GetDlgItem (IDC_MAXSLIDERVALUE);
			if (min != NULL && max != NULL)
			{
				CString str;
				str.Format ("%g", param->minValue);
				min->SetWindowText (str);
				str.Format ("%g", param->maxValue);
				max->SetWindowText (str);
			}
		}
	}
	else
	{
		sliderCtrl->EnableWindow (FALSE);
		CWnd *min = ((CMainFrame*) AfxGetMainWnd ())->_propertyBar.GetDlgItem (IDC_MINSLIDERVALUE);
		CWnd *max = ((CMainFrame*) AfxGetMainWnd ())->_propertyBar.GetDlgItem (IDC_MAXSLIDERVALUE);
		if (min != NULL && max != NULL)
		{
			min->SetWindowText ("");
			max->SetWindowText ("");
		}
	}
}

bool CTreeditorView::InitRender (unsigned long initComps)
{
	if (!theApp.InitD3D (&_renderView))
	{
		return false;
	}

	// TODO: setup render states
	D3DCAPS8 d3dCaps;
	HRESULT hr = theApp.m_d3dDevice8->GetDeviceCaps (&d3dCaps);
	if (FAILED (hr))
	{
		return false;
	}

	// Setup a material
	D3DMATERIAL8 mtrl;
	D3DUtil_InitMaterial( mtrl, 1.0f, 0.0f, 0.0f, 1.0f );
	hr = theApp.m_d3dDevice8->SetMaterial( &mtrl );

	// Set up the textures
	hr = theApp.m_d3dDevice8->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_MODULATE );
	hr = theApp.m_d3dDevice8->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE );
	hr = theApp.m_d3dDevice8->SetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_DIFFUSE );

	/*
	hr = theApp.m_d3dDevice8->SetTextureStageState( 0, D3DTSS_MINFILTER, D3DTEXF_LINEAR );
	hr = theApp.m_d3dDevice8->SetTextureStageState( 0, D3DTSS_MAGFILTER, D3DTEXF_LINEAR );
	*/
	
	hr = theApp.m_d3dDevice8->SetTextureStageState( 0, D3DTSS_ADDRESSU,  D3DTADDRESS_CLAMP );
	hr = theApp.m_d3dDevice8->SetTextureStageState( 0, D3DTSS_ADDRESSV,  D3DTADDRESS_CLAMP );

	////////////////
	hr = theApp.m_d3dDevice8->SetTextureStageState( 0, D3DTSS_ALPHAOP,   D3DTOP_SELECTARG1 );
	hr = theApp.m_d3dDevice8->SetTextureStageState( 0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE );
	////////////////

	// Set miscellaneous render states
	hr = theApp.m_d3dDevice8->SetRenderState( D3DRS_DITHERENABLE,   FALSE );
	hr = theApp.m_d3dDevice8->SetRenderState( D3DRS_SPECULARENABLE, FALSE );
	hr = theApp.m_d3dDevice8->SetRenderState( D3DRS_ZENABLE,        TRUE );
	hr = theApp.m_d3dDevice8->SetRenderState( D3DRS_AMBIENT,        0x000F0F0F );

	////////////////
	// Set diffuse blending for alpha set in vertices.
	hr = theApp.m_d3dDevice8->SetRenderState( D3DRS_ALPHABLENDENABLE,   FALSE );
/*
	hr = theApp.m_d3dDevice8->SetRenderState( D3DRS_ALPHABLENDENABLE,   TRUE );
	hr = theApp.m_d3dDevice8->SetRenderState( D3DRS_BLENDOP,   D3DBLENDOP_ADD);
	hr = theApp.m_d3dDevice8->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_ONE );
	hr = theApp.m_d3dDevice8->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_ZERO );

  //hr = theApp.m_d3dDevice8->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_SRCALPHA );
	//hr = theApp.m_d3dDevice8->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );
*/


	// Enable alpha testing (skips pixels with less than a certain alpha.)
	if( d3dCaps.AlphaCmpCaps & D3DPCMPCAPS_GREATEREQUAL )
	{
		hr = theApp.m_d3dDevice8->SetRenderState( D3DRS_ALPHATESTENABLE, TRUE );
		//hr = theApp.m_d3dDevice8->SetRenderState( D3DRS_ALPHAREF,        0x08 );
    hr = theApp.m_d3dDevice8->SetRenderState( D3DRS_ALPHAREF, 128 );
		hr = theApp.m_d3dDevice8->SetRenderState( D3DRS_ALPHAFUNC, D3DCMP_GREATER );
	}
	
	////////////////
	//hr = theApp.m_d3dDevice8->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
	//hr = theApp.m_d3dDevice8->SetRenderState(D3DRS_AMBIENT, D3DCOLOR_XRGB(128, 128, 128));
	//hr = theApp.m_d3dDevice8->SetRenderState( D3DRS_LIGHTING, FALSE );

	// Create caustic texture
	/*-
	D3DDISPLAYMODE mode;
	hr = theApp.m_d3dDevice8->GetDisplayMode(&mode);
	
	if(FAILED(hr = D3DXCreateTexture(theApp.m_d3dDevice8, 128, 128, 1, D3DUSAGE_RENDERTARGET, mode.Format, D3DPOOL_DEFAULT, &m_pCausticTex)) &&
		FAILED(hr = D3DXCreateTexture(theApp.m_d3dDevice8, 128, 128, 1, 0, mode.Format, D3DPOOL_DEFAULT, &m_pCausticTex)))
	{
		return false;
	}
	D3DSURFACE_DESC desc;
	m_pCausticTex->GetSurfaceLevel(0, &m_pCausticSurf);
	m_pCausticSurf->GetDesc(&desc);
	if(FAILED(hr = D3DXCreateRenderToSurface(theApp.m_d3dDevice8, desc.Width, desc.Height, 
		desc.Format, FALSE, D3DFMT_UNKNOWN, &m_pRenderToSurface)))
	{
		return false;
	}
	-*/
	
	// Set the world matrix
	D3DXMATRIX matIdentity;
	D3DXMatrixIdentity( &matIdentity );
	hr = theApp.m_d3dDevice8->SetTransform( D3DTS_WORLD,  &matIdentity );

	// Set up our view matrix. A view matrix can be defined given an eye point,
	// a point to lookat, and a direction for which way is up. Here, we set the
	// eye five units back along the z-axis and up three units, look at the
	// origin, and define "up" to be in the y-direction.
	D3DXMATRIX matView;
	D3DXVECTOR3 vFromPt   = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	D3DXVECTOR3 vLookatPt = D3DXVECTOR3( 0.0f, 0.0f, 1.0f );
	D3DXVECTOR3 vUpVec    = D3DXVECTOR3( 0.0f, 1.0f, 0.0f );
	D3DXMatrixLookAtLH( &matView, &vFromPt, &vLookatPt, &vUpVec );
	hr = theApp.m_d3dDevice8->SetTransform( D3DTS_VIEW, &matView );
	
	// Set the projection matrix
	/*-
	D3DXMATRIX matProj;
	FLOAT fAspect = ((FLOAT)m_d3dsdBackBuffer.Width) / m_d3dsdBackBuffer.Height;
	D3DXMatrixPerspectiveFovLH( &matProj, D3DX_PI/4, fAspect, 0.1f, 100.0f );
	hr = theApp.m_d3dDevice8->SetTransform( D3DTS_PROJECTION, &matProj );
	-*/

	// Set up lighting states
	hr = theApp.m_d3dDevice8->LightEnable( 0, TRUE );
	hr = theApp.m_d3dDevice8->SetRenderState( D3DRS_LIGHTING, TRUE );

	// Create a D3D font using D3DX
	/*-
	HFONT hFont = CreateFont( 20, 0, 0, 0, FW_BOLD, FALSE, FALSE, FALSE,
		ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
		ANTIALIASED_QUALITY, FF_DONTCARE, "Arial" );      
	if( FAILED( hr = D3DXCreateFont( theApp.m_d3dDevice8, hFont, &m_pD3DXFont ) ) )
		return DXTRACE_ERR_NOMSGBOX( "D3DXCreateFont", hr );
	
	if( !m_bWindowed )
	{
		// Create a surface for configuring DInput devices
		if( FAILED( hr = theApp.m_d3dDevice8->CreateImageSurface( 640, 480, 
			m_d3dsdBackBuffer.Format, &m_pDIConfigSurface ) ) ) 
			return DXTRACE_ERR_NOMSGBOX( "CreateImageSurface", hr );
	}
	-*/

	SetRenderFillMode ();

	int trees = 1;
	GET_INT_FROM_WND(trees, _componentBar, IDC_TREES)

	UINT xTree = (UINT) ceil (sqrt (trees));
	UINT zTree = (UINT) ceil ((double) trees / (double) xTree);

	float distance = 1.0f;
	GET_FLOAT_FROM_WND(distance, _componentBar, IDC_DIST);

	float meter = 1.0f;
	GET_FLOAT_FROM_WND(meter, _componentBar, IDC_METER)

	if ((initComps & INIT_METER) != 0)
	{
		METERVERTEX *meterVertex;
		hr = theApp.m_d3dMeterVertexBuffer->Lock (0, sizeof (METERVERTEX) * 24, 
			(BYTE**) &meterVertex, 0);
		if (FAILED (hr))
		{
			return false;
		}
		
		int m = 0;
		int t = 0;
		for (int j = 0; j < zTree && t < trees; ++j)
		{
			float z = distance * ((float) j - (float) (zTree - 1) * 0.5f + 0.5f);
			for (int i = 0; i < xTree && t < trees; ++i, ++t)
			{
				float x = distance * ((float) i - (float) (xTree - 1) * 0.5f + 0.5f);
#define SET_METER_VERTEX(mx, my, mz, mnx, mny, mnz, mcolor)	meterVertex[m].x = mx;\
															meterVertex[m].y = my;\
															meterVertex[m].z = mz;\
															meterVertex[m].nx = mnx;\
															meterVertex[m].ny = mny;\
															meterVertex[m].nz = mnz;\
															meterVertex[m].color = mcolor;\
															++m;
				
				SET_METER_VERTEX (x - 0.05f, 0.0f, z, 0.0f, 0.0f, -1.0f, 0xff0000ff);
				SET_METER_VERTEX (x - 0.05f, meter, z, 0.0f, 0.0f, -1.0f, 0xff0000ff);
				SET_METER_VERTEX (x + 0.05f, 0.0f, z, 0.0f, 0.0f, -1.0f, 0xff0000ff);
				SET_METER_VERTEX (x + 0.05f, 0.0f, z, 0.0f, 0.0f, -1.0f, 0xff0000ff);
				SET_METER_VERTEX (x - 0.05f, meter, z, 0.0f, 0.0f, -1.0f, 0xff0000ff);
				SET_METER_VERTEX (x + 0.05f, meter, z, 0.0f, 0.0f, -1.0f, 0xff0000ff);
				
				SET_METER_VERTEX (x + 0.05f, 0.0f, z, 0.0f, 0.0f, 1.0f, 0xff0000ff);
				SET_METER_VERTEX (x - 0.05f, meter, z, 0.0f, 0.0f, 1.0f, 0xff0000ff);
				SET_METER_VERTEX (x - 0.05f, 0.0f, z, 0.0f, 0.0f, 1.0f, 0xff0000ff);
				SET_METER_VERTEX (x + 0.05f, meter, z, 0.0f, 0.0f, 1.0f, 0xff0000ff);
				SET_METER_VERTEX (x - 0.05f, meter, z, 0.0f, 0.0f, 1.0f, 0xff0000ff);
				SET_METER_VERTEX (x + 0.05f, 0.0f, z, 0.0f, 0.0f, 1.0f, 0xff0000ff);
				
				SET_METER_VERTEX (x, 0.0f, z - 0.05f, 1.0f, 0.0f, 0.0f, 0xff0000ff);
				SET_METER_VERTEX (x, meter, z - 0.05f, 1.0f, 0.0f, 0.0f, 0xff0000ff);
				SET_METER_VERTEX (x, 0.0f, z + 0.05f, 1.0f, 0.0f, 0.0f, 0xff0000ff);
				SET_METER_VERTEX (x, 0.0f, z + 0.05f, 1.0f, 0.0f, 0.0f, 0xff0000ff);
				SET_METER_VERTEX (x, meter, z - 0.05f, 1.0f, 0.0f, 0.0f, 0xff0000ff);
				SET_METER_VERTEX (x, meter, z + 0.05f, 1.0f, 0.0f, 0.0f, 0xff0000ff);
				
				SET_METER_VERTEX (x, 0.0f, z + 0.05f, -1.0f, 0.0f, 0.0f, 0xff0000ff);
				SET_METER_VERTEX (x, meter, z - 0.05f, -1.0f, 0.0f, 0.0f, 0xff0000ff);
				SET_METER_VERTEX (x, 0.0f, z - 0.05f, -1.0f, 0.0f, 0.0f, 0xff0000ff);
				SET_METER_VERTEX (x, meter, z + 0.05f, -1.0f, 0.0f, 0.0f, 0xff0000ff);
				SET_METER_VERTEX (x, meter, z - 0.05f, -1.0f, 0.0f, 0.0f, 0xff0000ff);
				SET_METER_VERTEX (x, 0.0f, z + 0.05f, -1.0f, 0.0f, 0.0f, 0xff0000ff);
#undef SET_METER_VERTEX
			}
		}
		
		hr = theApp.m_d3dMeterVertexBuffer->Unlock ();
		if (FAILED (hr))
		{
			return false;
		}
	}

	if ((initComps & INIT_GROUND) != 0)
	{
		GROUNDVERTEX *groundVertex;
		hr = theApp.m_d3dGroundVertexBuffer->Lock (0, sizeof (GROUNDVERTEX) * 4, 
			(BYTE**) &groundVertex, 0);
		if (FAILED (hr))
		{
			return false;
		}

		CString str = AfxGetApp ()->GetProfileString (_T ("ground"), _T("height"), _T ("1.0"));
		float gth = (float) _tcstod (str, NULL);
		str = AfxGetApp ()->GetProfileString (_T ("ground"), _T("width"), _T ("1.0"));
		float gtw = (float) _tcstod (str, NULL);

		groundVertex[0].x = distance * (- (float) (xTree + 1) * 0.5f);
		groundVertex[0].y = 0.0f;
		groundVertex[0].z = distance * ((float) (zTree + 1) * 0.5f);
		groundVertex[0].nx = 0.0f;
		groundVertex[0].ny = 1.0f;
		groundVertex[0].nz = 0.0f;
		groundVertex[0].color = 0xffffffff;
		groundVertex[0].tu = 0.0f;
		groundVertex[0].tv = 0.0f;
		
		groundVertex[1].x = distance * ((float) (xTree + 1) * 0.5f);
		groundVertex[1].y = 0.0f;
		groundVertex[1].z = distance * ((float) (zTree + 1) * 0.5f);
		groundVertex[1].nx = 0.0f;
		groundVertex[1].ny = 1.0f;
		groundVertex[1].nz = 0.0f;
		groundVertex[1].color = 0xffffffff;
		groundVertex[1].tu = distance * (xTree + 1) / gtw;
		groundVertex[1].tv = 0.0f;
		
		groundVertex[2].x = distance * (- (float) (xTree + 1) * 0.5f);
		groundVertex[2].y = 0.0f;
		groundVertex[2].z = distance * (- (float) (zTree + 1) * 0.5f);
		groundVertex[2].nx = 0.0f;
		groundVertex[2].ny = 1.0f;
		groundVertex[2].nz = 0.0f;
		groundVertex[2].color = 0xffffffff;
		groundVertex[2].tu = 0.0f;
		groundVertex[2].tv = distance * (zTree + 1) / gth;
		
		groundVertex[3].x = distance * ((float) (xTree + 1) * 0.5f);
		groundVertex[3].y = 0.0f;
		groundVertex[3].z = distance * (- (float) (zTree + 1) * 0.5f);
		groundVertex[3].nx = 0.0f;
		groundVertex[3].ny = 1.0f;
		groundVertex[3].nz = 0.0f;
		groundVertex[3].color = 0xffffffff;
		groundVertex[3].tu = distance * (xTree + 1) / gtw;
		groundVertex[3].tv = distance * (zTree + 1) / gth;

		hr = theApp.m_d3dGroundVertexBuffer->Unlock ();
		if (FAILED (hr))
		{
			return false;
		}

		// Create ground texture
		if (theApp.m_d3dGroundTexture != NULL)
		{
			theApp.m_d3dGroundTexture->Release ();
			theApp.m_d3dGroundTexture = NULL;
		}
		str = AfxGetApp()->GetProfileString(_T ("ground"), _T ("texture"), _T ("ground.bmp"));
		USES_CONVERSION;
		LPCSTR name = T2CA (str);
		hr = D3DXCreateTextureFromFile (theApp.m_d3dDevice8, name, &theApp.m_d3dGroundTexture);
		if (FAILED (hr))
		{
			CString msg;
			msg.Format (_T ("Ground texture file \"%s\" is missing."), str);
			AfxMessageBox (msg, MB_OK);
			return false;
		}
	}
	
	bool preview = AfxGetApp ()->GetProfileInt (_T ("editor"), _T ("preview"), 1) != 0;
	if (preview && (initComps & INIT_TREE) != 0)
	{
		CTreeditorDoc *doc = GetDocument ();
		if (doc->NeedToUpdatePreview ())
		{
			DeleteTrees ();
			CString treeCode;
			CString errMsg;
			m_treeGenerated = !GetDocument ()->Generate (treeCode, errMsg);
			if (m_treeGenerated)
			{
				//theApp.TreeEngine->Load(treeCode, treeCode.GetLength());
				theApp.TreeEngine->LoadPlant(treeCode, treeCode.GetLength());
				
				// -----------------------------------------------
				// Preparing the plant mesh
				float distance = 1.0f;
				GET_FLOAT_FROM_WND(distance, _componentBar, IDC_DIST);
				
				float age = 1.0f;
				GET_FLOAT_FROM_WND(age, _componentBar, IDC_AGE)
					
				int trees = 1;
				GET_INT_FROM_WND(trees, _componentBar, IDC_TREES)
					
				int seed = 0;
				GET_INT_FROM_WND(seed, _componentBar, IDC_SEED)
					
				Matrix4 origin;
				origin.SetIdentity();
				origin.SetDirectionAndUp(Vector3(0, 1, 0).Normalized(), Vector3(1, 0, 0));
				origin.Orthogonalize();
				
				static FLOAT treeDistance = 0.0f;
#ifndef DETAIL_IS_N_POLYGONS
				float detail = 1.0f;
				GET_FLOAT_FROM_WND (detail, _componentBar, IDC_DETAIL)
				if (m_autoDetail)
				{
					float newDetail;

					float neard = 0.0f;
					GET_FLOAT_FROM_WND(neard, _componentBar, IDC_NEAR)
					float fard = 500.0f;
					GET_FLOAT_FROM_WND(fard, _componentBar, IDC_FAR)
					if (treeDistance <= neard)
					{
						newDetail = 1.0f;
					}
					else if (treeDistance >= fard)
					{
						newDetail = 0.0f;
					}
					else
					{
						newDetail = (fard - treeDistance) / (fard - neard);
					}

					if (newDetail != detail)
					{
						detail = newDetail;
						PUT_FLOAT_IN_WND (detail, _componentBar, IDC_DETAIL)
						PUT_INT_IN_SLIDER((int) (detail * 100.0f), _componentBar, IDC_DETAILSLIDER)
					}
				}
				((CMainFrame*) AfxGetMainWnd ())->SetStatusText (_T ("Tree Generating..."));
				theApp.TreeEngine->PreparePlantData(seed, detail, origin, age, 3000);
				((CMainFrame*) AfxGetMainWnd ())->SetStatusText (_T ("Ready"));
#else
				int detail = N_POLYGONS_MAX;
				GET_INT_FROM_WND (detail, _componentBar, IDC_DETAIL)
				((CMainFrame*) AfxGetMainWnd ())->SetStatusText (_T ("Tree Generating..."));
				theApp.TreeEngine->PreparePlantData(seed, 1.0f, origin, age, detail, RString(), 128, false, &g_N1);
				((CMainFrame*) AfxGetMainWnd ())->SetStatusText (_T ("Ready"));
#endif

				// -----------------------------------------------
				/*
				for (int i = 0; i < trees; ++i)
				{
					tree[i] = new CTree ();
					if (NULL == tree[i])
					{
						return false;
					}
					tree[i]->Init(theApp.m_d3dDevice8);
					tree[i]->Load(theApp.m_d3dDevice8, treeCode, treeCode.GetLength ());
				}
				*/
			}
			doc->_currentStamp.SetChange (false);
			doc->_lastStamp = doc->_currentStamp;
		}
	}

	if ((initComps & INIT_LIGHT_AND_WIND) != 0)
	{
		int i;
		D3DXVECTOR3 v1, v2, cp;

#define SET_PYRAMID(lvertex, lcolor, lx0, ly0, lz0, lx1, ly1, lz1, lx2, ly2, lz2, lx3, ly3, lz3, lx4, ly4, lz4)	\
	SET_TRIANGLE (lvertex, lcolor, lx0, ly0, lz0, lx1, ly1, lz1, lx2, ly2, lz2);\
	SET_TRIANGLE (lvertex, lcolor, lx0, ly0, lz0, lx2, ly2, lz2, lx3, ly3, lz3);\
	SET_TRIANGLE (lvertex, lcolor, lx0, ly0, lz0, lx3, ly3, lz3, lx4, ly4, lz4);\
	SET_TRIANGLE (lvertex, lcolor, lx0, ly0, lz0, lx4, ly4, lz4, lx1, ly1, lz1);\
	SET_TRIANGLE (lvertex, lcolor, lx1, ly1, lz1, lx3, ly3, lz3, lx2, ly2, lz2);\
	SET_TRIANGLE (lvertex, lcolor, lx1, ly1, lz1, lx4, ly4, lz4, lx3, ly3, lz3);

#define SET_TRIANGLE(lvertex, lcolor, lx0, ly0, lz0, lx1, ly1, lz1, lx2, ly2, lz2)	\
	v1.x = lx1 - lx0;\
	v1.y = ly1 - ly0;\
	v1.z = lz1 - lz0;\
	v2.x = lx2 - lx1;\
	v2.y = ly2 - ly1;\
	v2.z = lz2 - lz1;\
	D3DXVec3Cross (&cp, &v1, &v2);\
	D3DXVec3Normalize (&cp, &cp);\
	lvertex[i].x = lx0;\
	lvertex[i].y = ly0;\
	lvertex[i].z = lz0;\
	lvertex[i].nx = cp.x;\
	lvertex[i].ny = cp.y;\
	lvertex[i].nz = cp.z;\
	lvertex[i].color = lcolor;\
	++i;\
	lvertex[i].x = lx1;\
	lvertex[i].y = ly1;\
	lvertex[i].z = lz1;\
	lvertex[i].nx = cp.x;\
	lvertex[i].ny = cp.y;\
	lvertex[i].nz = cp.z;\
	lvertex[i].color = lcolor;\
	++i;\
	lvertex[i].x = lx2;\
	lvertex[i].y = ly2;\
	lvertex[i].z = lz2;\
	lvertex[i].nx = cp.x;\
	lvertex[i].ny = cp.y;\
	lvertex[i].nz = cp.z;\
	lvertex[i].color = lcolor;\
	++i;

		LIGHTVERTEX *lightVertex;
		hr = theApp.m_d3dLightVertexBuffer->Lock (0, sizeof (LIGHTVERTEX) * 18, 
			(BYTE**) &lightVertex, 0);
		if (FAILED (hr))
		{
			return false;
		}

		i = 0;
		SET_PYRAMID (lightVertex, 0xffff0000,
			0.0f, -0.5f, 0.0f,
			0.3f, 0.5f, 0.3f,
			-0.3f, 0.5f, 0.3f,
			-0.3f, 0.5f, -0.3f,
			0.3f, 0.5f, -0.3f);

		hr = theApp.m_d3dLightVertexBuffer->Unlock ();
		if (FAILED (hr))
		{
			return false;
		}

		WINDVERTEX *windVertex;
		hr = theApp.m_d3dWindVertexBuffer->Lock (0, sizeof (WINDVERTEX) * 18, 
			(BYTE**) &windVertex, 0);
		if (FAILED (hr))
		{
			return false;
		}

		i = 0;
		SET_PYRAMID (windVertex, 0xff0000ff,
			0.0f, 0.5f, 0.0f,
			0.15f, -0.5f, -0.15f,
			-0.15f, -0.5f, -0.15f,
			-0.15f, -0.5f, 0.15f,
			0.15f, -0.5f, 0.15f);

		hr = theApp.m_d3dWindVertexBuffer->Unlock ();
		if (FAILED (hr))
		{
			return false;
		}

#undef SET_PYRAMID
#undef SET_TRIANGLE

		LIGHTPYRVERTEX *lightPyrVertex;
		hr = theApp.m_d3dLightPyrVertexBuffer->Lock (0, sizeof (LIGHTPYRVERTEX) * 5, 
			(BYTE**) &lightPyrVertex, 0);
		if (FAILED (hr))
		{
			return false;
		}

		lightPyrVertex[0].x = 0.0f;
		lightPyrVertex[0].y = -3.0f;
		lightPyrVertex[0].z = 0.0f;
		lightPyrVertex[0].color = 0xffff0000;
		lightPyrVertex[1].x = 0.9f;
		lightPyrVertex[1].y = 0.0f;
		lightPyrVertex[1].z = -0.9f;
		lightPyrVertex[1].color = 0xffff0000;
		lightPyrVertex[2].x = -0.9f;
		lightPyrVertex[2].y = 0.0f;
		lightPyrVertex[2].z = -0.9f;
		lightPyrVertex[2].color = 0xffff0000;
		lightPyrVertex[3].x = -0.9f;
		lightPyrVertex[3].y = 0.0f;
		lightPyrVertex[3].z = 0.9f;
		lightPyrVertex[3].color = 0xffff0000;
		lightPyrVertex[4].x = 0.9f;
		lightPyrVertex[4].y = 0.0f;
		lightPyrVertex[4].z = 0.9f;
		lightPyrVertex[4].color = 0xffff0000;

		hr = theApp.m_d3dLightPyrVertexBuffer->Unlock ();
		if (FAILED (hr))
		{
			return false;
		}

		short *lightPyrIndex;
		hr = theApp.m_d3dLightPyrIndexBuffer->Lock (0, 2 * 8 * 2, 
			(BYTE**) &lightPyrIndex, 0 );
		if (FAILED (hr))
		{
			return false;
		}

		lightPyrIndex[0] = 0;
		lightPyrIndex[1] = 1;
		lightPyrIndex[2] = 0;
		lightPyrIndex[3] = 2;
		lightPyrIndex[4] = 0;
		lightPyrIndex[5] = 3;
		lightPyrIndex[6] = 0;
		lightPyrIndex[7] = 4;
		lightPyrIndex[8] = 1;
		lightPyrIndex[9] = 2;
		lightPyrIndex[10] = 2;
		lightPyrIndex[11] = 3;
		lightPyrIndex[12] = 3;
		lightPyrIndex[13] = 4;
		lightPyrIndex[14] = 4;
		lightPyrIndex[15] = 1;

		hr = theApp.m_d3dLightPyrIndexBuffer->Unlock ();
		if (FAILED (hr))
		{
			return false;
		}

		WINDPYRVERTEX *windPyrVertex;
		hr = theApp.m_d3dWindPyrVertexBuffer->Lock (0, sizeof (WINDPYRVERTEX) * 5, 
			(BYTE**) &windPyrVertex, 0);
		if (FAILED (hr))
		{
			return false;
		}

		windPyrVertex[0].x = 0.0f;
		windPyrVertex[0].y = 0.0f;
		windPyrVertex[0].z = 0.0f;
		windPyrVertex[0].color = 0xff0000ff;
		windPyrVertex[1].x = 0.45f;
		windPyrVertex[1].y = -3.0f;
		windPyrVertex[1].z = -0.45f;
		windPyrVertex[1].color = 0xff0000ff;
		windPyrVertex[2].x = -0.45f;
		windPyrVertex[2].y = -3.0f;
		windPyrVertex[2].z = -0.45f;
		windPyrVertex[2].color = 0xff0000ff;
		windPyrVertex[3].x = -0.45f;
		windPyrVertex[3].y = -3.0f;
		windPyrVertex[3].z = 0.45f;
		windPyrVertex[3].color = 0xff0000ff;
		windPyrVertex[4].x = 0.45f;
		windPyrVertex[4].y = -3.0f;
		windPyrVertex[4].z = 0.45f;
		windPyrVertex[4].color = 0xff0000ff;

		hr = theApp.m_d3dWindPyrVertexBuffer->Unlock ();
		if (FAILED (hr))
		{
			return false;
		}

		short *windPyrIndex;
		hr = theApp.m_d3dWindPyrIndexBuffer->Lock (0, 2 * 8 * 2, 
			(BYTE**) &windPyrIndex, 0 );
		if (FAILED (hr))
		{
			return false;
		}

		windPyrIndex[0] = 0;
		windPyrIndex[1] = 1;
		windPyrIndex[2] = 0;
		windPyrIndex[3] = 2;
		windPyrIndex[4] = 0;
		windPyrIndex[5] = 3;
		windPyrIndex[6] = 0;
		windPyrIndex[7] = 4;
		windPyrIndex[8] = 1;
		windPyrIndex[9] = 2;
		windPyrIndex[10] = 2;
		windPyrIndex[11] = 3;
		windPyrIndex[12] = 3;
		windPyrIndex[13] = 4;
		windPyrIndex[14] = 4;
		windPyrIndex[15] = 1;

		hr = theApp.m_d3dWindPyrIndexBuffer->Unlock ();
		if (FAILED (hr))
		{
			return false;
		}
	}

    return true;
}

void CTreeditorView::SetRenderFillMode ()
{
	theApp.m_d3dDevice8->SetRenderState (D3DRS_FILLMODE, m_wireframe ? D3DFILL_WIREFRAME : D3DFILL_SOLID);
}

void CTreeditorView::DeleteTrees ()
{
	for (int i = 0; i < MAX_TREES; ++i)
	{
    // FLY
	}
}

void CTreeditorView::ResetRender ()
{
	DeleteTrees ();
	theApp.ResetD3D ();
}

void CTreeditorView::UpdateRender (unsigned long reinitComps)
{
	if (::IsWindow (_renderView.m_hWnd) && !_renderView.IsWindowVisible ())
	{
		return;
	}

	if (!InitRender (reinitComps))
	{
		return;
	}

	Render ();
}

void CTreeditorView::Render ()
{
	if (theApp.m_d3dDevice8 == NULL)
	{
		return;
	}

	HRESULT hr = theApp.m_d3dDevice8->Clear (0, NULL, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER, m_bgColor, 1.0f, 0);
	if (hr != D3D_OK)
	{
		return;
	}

	// Wind
	D3DXMATRIX mat;
	D3DXMATRIX windPyramid;
	int wrotz = 0;
	GET_INT_FROM_SLIDER(wrotz, _componentBar, IDC_WROTZSLIDER)
	int wroty = 0;
	GET_INT_FROM_SLIDER(wroty, _componentBar, IDC_WROTYSLIDER)
	D3DXMatrixRotationZ (&windPyramid, 3.1415926f * (float) wrotz / 180.0f);
	D3DXMatrixRotationY (&mat, 3.1415926f * (float) wroty / 180.0f);
	D3DXMatrixMultiply (&windPyramid, &windPyramid, &mat);
	D3DXVECTOR3 wd (0.0f, 1.0f, 0.0f);
	D3DXVec3TransformNormal (&wd, &wd, &windPyramid);

	// Light
	D3DXMATRIX lightPyramid;
	int lrotz = 0;
	GET_INT_FROM_SLIDER(lrotz, _componentBar, IDC_LROTZSLIDER)
	D3DXMatrixRotationZ (&lightPyramid, 3.1415926f * (float) lrotz / 180.0f);

	int lroty = 0;
	GET_INT_FROM_SLIDER(lroty, _componentBar, IDC_LROTYSLIDER)
	D3DXMatrixRotationY (&mat, 3.1415926f * (float) lroty / 180.0f);
	D3DXMatrixMultiply (&lightPyramid, &lightPyramid, &mat);

	D3DXVECTOR3 ld (0.0f, 1.0f, 0.0f);
	D3DXVec3TransformNormal (&ld, &ld, &lightPyramid);

	D3DLIGHT8 light;
	ZeroMemory (&light, sizeof (light));
	light.Type        = D3DLIGHT_DIRECTIONAL;
	light.Diffuse.r   = 1.0f;
	light.Diffuse.g   = 1.0f;
	light.Diffuse.b   = 1.0f;
	light.Direction.x = ld.x;
	light.Direction.y = ld.y;
	light.Direction.z = ld.z;
	hr = theApp.m_d3dDevice8->SetLight( 0, &light );

	// Prepare Tree
	hr = theApp.m_d3dDevice8->SetTextureStageState (0, D3DTSS_ADDRESSU,  D3DTADDRESS_CLAMP);
	hr = theApp.m_d3dDevice8->SetTextureStageState (0, D3DTSS_ADDRESSV,  D3DTADDRESS_CLAMP);

	RECT rvRect;
	_renderView.GetClientRect (&rvRect);
	D3DXMATRIX projMat, viewMat;
	CalcTreeProjAndViewMat (projMat, viewMat, rvRect);
	D3DXMatrixIdentity (&mat);
	hr = theApp.m_d3dDevice8->SetTransform (D3DTS_WORLD, &mat);

	static FLOAT treeDistance = 0.0f;

	D3DXVECTOR3 vec3 (0.0f, 0.0f, 0.0f);
	D3DXVec3TransformCoord (&vec3, &vec3, &viewMat);
	treeDistance = D3DXVec3Length (&vec3);;

	float distance = 1.0f;
	GET_FLOAT_FROM_WND(distance, _componentBar, IDC_DIST);

	float age = 1.0f;
	GET_FLOAT_FROM_WND(age, _componentBar, IDC_AGE)

	int trees = 1;
	GET_INT_FROM_WND(trees, _componentBar, IDC_TREES)

	int seed = 0;
	GET_INT_FROM_WND(seed, _componentBar, IDC_SEED)

	if (m_treeGenerated)
	{
#ifndef DETAIL_IS_N_POLYGONS
		float detail = 1.0f;
		GET_FLOAT_FROM_WND (detail, _componentBar, IDC_DETAIL)
		if (m_autoDetail)
		{
			float newDetail;

			float neard = 0.0f;
			GET_FLOAT_FROM_WND(neard, _componentBar, IDC_NEAR)
			float fard = 500.0f;
			GET_FLOAT_FROM_WND(fard, _componentBar, IDC_FAR)
			if (treeDistance <= neard)
			{
				newDetail = 1.0f;
			}
			else if (treeDistance >= fard)
			{
				newDetail = 0.0f;
			}
			else
			{
				newDetail = (fard - treeDistance) / (fard - neard);
			}

			if (newDetail != detail)
			{
				detail = newDetail;
				PUT_FLOAT_IN_WND (detail, _componentBar, IDC_DETAIL)
				PUT_INT_IN_SLIDER((int) (detail * 100.0f), _componentBar, IDC_DETAILSLIDER)
			}
		}
#endif

		g_N0 = 0;

		// Transform wind vector to be in model view coordinates (because wind is moving with rotation)
		D3DXVECTOR3 twd = wd;
		//D3DXVec3TransformNormal(&twd, &wd, &viewMat);
		
		// Transform light vector to be in model view coordinates (because light is moving with rotation)
		D3DXVECTOR3 tld;
		D3DXVec3TransformNormal(&tld, &ld, &viewMat);

		UINT xTree = (UINT) ceil (sqrt (trees));
		UINT zTree = (UINT) ceil ((double) trees / (double) xTree);
		
		Matrix4 origin;
		origin.SetIdentity();
		origin.SetDirectionAndUp(Vector3(0, 1, 0).Normalized(), Vector3(1, 0, 0));
		origin.Orthogonalize();
		int t = 0;
		for (int j = 0; j < zTree && t < trees; ++j)
		{
			float z = distance * ((float) j - (float) (zTree - 1) * 0.5f);
			for (int i = 0; i < xTree && t < trees; ++i, ++t)
			{
				float x = distance * ((float) i - (float) (xTree - 1) * 0.5f);
				origin.SetPosition (Vector3 (x, 0.0f, z));
				//g_N0 += theApp.TreeEngine->DrawTree(viewMat, projMat, seed + t, m_detail, origin, m_age, Vector3(tld.x, tld.y, tld.z), Vector3(twd.x, twd.y, twd.z), m_wind);
/*
        theApp.TreeEngine->PreparePlantData(
          seed + t,
          detail,
          origin,
          age);
*/
        g_N0 += theApp.TreeEngine->DrawPlant(
          viewMat,
          projMat,
          Vector3(tld.x, tld.y, tld.z));
			}
		}
	}
	
	// Begin the scene.
	hr = theApp.m_d3dDevice8->BeginScene ();
	if (hr != D3D_OK)
	{
		return;
	}

	hr = theApp.m_d3dDevice8->SetTransform(D3DTS_VIEW, &viewMat);
	hr = theApp.m_d3dDevice8->SetTransform(D3DTS_PROJECTION, &projMat);

	if (m_showGround)
	{
		// Set ground texture
		hr = theApp.m_d3dDevice8->SetTexture (0, theApp.m_d3dGroundTexture);
		hr = theApp.m_d3dDevice8->SetTextureStageState (0, D3DTSS_ADDRESSU,  D3DTADDRESS_WRAP);
		hr = theApp.m_d3dDevice8->SetTextureStageState (0, D3DTSS_ADDRESSV,  D3DTADDRESS_WRAP);
		
		// Set ground stream source
		hr = theApp.m_d3dDevice8->SetStreamSource (0, theApp.m_d3dGroundVertexBuffer, sizeof (GROUNDVERTEX));
		
		// Set vertex shader
		hr = theApp.m_d3dDevice8->SetVertexShader (D3DFVF_GROUNDVERTEX);
		hr = theApp.m_d3dDevice8->SetPixelShader(0);
		
		// Draw primitives
		hr = theApp.m_d3dDevice8->DrawPrimitive (D3DPT_TRIANGLESTRIP, 0, 2);
		
		// Reset ground texture
		hr = theApp.m_d3dDevice8->SetTexture (0, NULL);
	}

	if (m_showMeter)
	{
		// Set meter stream source
		hr = theApp.m_d3dDevice8->SetStreamSource (0, theApp.m_d3dMeterVertexBuffer, sizeof (METERVERTEX));
		
		// Set meter shader
		hr = theApp.m_d3dDevice8->SetVertexShader (D3DFVF_METERVERTEX);
		
		// Draw primitives
		hr = theApp.m_d3dDevice8->DrawPrimitive (D3DPT_TRIANGLELIST, 0, 8 * trees);
	}

	int n = theApp._arrowVertexBuffers.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		// Set vertex and index source buffers
		hr = theApp.m_d3dDevice8->SetStreamSource (0, theApp._arrowVertexBuffers[i], sizeof (ARROWVERTEX));
		hr = theApp.m_d3dDevice8->SetIndices (theApp._arrowIndexBuffers[i], 0);
		
		// Set vertex shader
		hr = theApp.m_d3dDevice8->SetVertexShader (D3DFVF_ARROWVERTEX);
		hr = theApp.m_d3dDevice8->SetPixelShader(0);
		
		// Draw primitives
		hr = theApp.m_d3dDevice8->DrawIndexedPrimitive (D3DPT_TRIANGLELIST, 0, 40, 0, 22);
	}

	D3DXMatrixMultiply (&windPyramid, &windPyramid, &_matZoomRotation);
	D3DXMatrixMultiply (&lightPyramid, &lightPyramid, &_matZoomRotation);

	// Render big light pyramid.
	if (m_showBigLightPyramid)
	{
		// Setup world matrix
		mat = lightPyramid;
		D3DXMatrixMultiply (&mat, &mat, &_matZoomTranslation);
		theApp.m_d3dDevice8->SetTransform (D3DTS_VIEW, &mat);
		
		// Set vertex and index source buffers
		hr = theApp.m_d3dDevice8->SetStreamSource (0, theApp.m_d3dLightPyrVertexBuffer, sizeof (LIGHTPYRVERTEX));
		hr = theApp.m_d3dDevice8->SetIndices (theApp.m_d3dLightPyrIndexBuffer, 0);
		
		// Set vertex shader
		hr = theApp.m_d3dDevice8->SetVertexShader (D3DFVF_LIGHTPYRVERTEX);
		
		// Draw primitives
		hr = theApp.m_d3dDevice8->SetRenderState( D3DRS_LIGHTING, FALSE );
		hr = theApp.m_d3dDevice8->DrawIndexedPrimitive (D3DPT_LINELIST, 0, 5, 0, 8);
		hr = theApp.m_d3dDevice8->SetRenderState( D3DRS_LIGHTING, TRUE );
	}

	// Render big wind pyramid.
	if (m_showBigWindPyramid)
	{
		// Setup world matrix
		mat = windPyramid;
		D3DXMatrixMultiply (&mat, &mat, &_matZoomTranslation);
		hr = theApp.m_d3dDevice8->SetTransform (D3DTS_VIEW, &mat);
		
		// Set vertex and index source buffers
		hr = theApp.m_d3dDevice8->SetStreamSource (0, theApp.m_d3dWindPyrVertexBuffer, sizeof (LIGHTPYRVERTEX));
		hr = theApp.m_d3dDevice8->SetIndices (theApp.m_d3dWindPyrIndexBuffer, 0);
		
		// Set vertex shader
		hr = theApp.m_d3dDevice8->SetVertexShader (D3DFVF_WINDPYRVERTEX);
		
		// Draw primitives
		hr = theApp.m_d3dDevice8->SetRenderState( D3DRS_LIGHTING, FALSE );
		hr = theApp.m_d3dDevice8->DrawIndexedPrimitive (D3DPT_LINELIST, 0, 5, 0, 8);
		hr = theApp.m_d3dDevice8->SetRenderState( D3DRS_LIGHTING, TRUE );
	}

	// End the scene.
	hr = theApp.m_d3dDevice8->EndScene ();

	// Save current viewport
	D3DVIEWPORT8 viewport;
	hr = theApp.m_d3dDevice8->GetViewport (&viewport);
	if (FAILED (hr))
	{
		return;
	}

	// Setup new light direction
	light.Direction.x = 0.0f;
	light.Direction.y = 0.0f;
	light.Direction.z = 1.0f;
	hr = theApp.m_d3dDevice8->SetLight (0, &light);

	// Set view and projection matrices
	D3DXMatrixPerspectiveFovLH( &mat, D3DX_PI/12, 1.0f, 0.1f, 100.0f );
	hr = theApp.m_d3dDevice8->SetTransform(D3DTS_PROJECTION, &mat);
	D3DXVECTOR3 vFromPt ( 0.0f, 0.0f, -5.0f );
	D3DXVECTOR3 vLookatPt ( 0.0f, 0.0f, 0.0f );
	D3DXVECTOR3 vUpVec ( 0.0f, 1.0f, 0.0f );
	D3DXMatrixLookAtLH( &mat, &vFromPt, &vLookatPt, &vUpVec );
	hr = theApp.m_d3dDevice8->SetTransform(D3DTS_VIEW, &mat);

	// Render light pyramid - set new viewport
	D3DVIEWPORT8 tempViewport;
	tempViewport.X = 0;
	tempViewport.Y = viewport.Y + viewport.Height - 80;
	tempViewport.Width = 80;
	tempViewport.Height = 80;
	tempViewport.MinZ = viewport.MinZ;
	tempViewport.MaxZ = viewport.MaxZ;
	hr = theApp.m_d3dDevice8->SetViewport (&tempViewport);
	if (FAILED (hr))
	{
		return;
	}

	// Setup world matrix for light pyramid
	hr = theApp.m_d3dDevice8->SetTransform (D3DTS_WORLD, &lightPyramid);

	hr = theApp.m_d3dDevice8->Clear (0, NULL, D3DCLEAR_ZBUFFER, 0, 1.0f, 0);
	if (hr != D3D_OK)
	{
		return;
	}

	hr = theApp.m_d3dDevice8->BeginScene ();
	
	// Set light stream source
	hr = theApp.m_d3dDevice8->SetStreamSource (0, theApp.m_d3dLightVertexBuffer, sizeof (LIGHTVERTEX));
	
	// Set light shader
	hr = theApp.m_d3dDevice8->SetVertexShader (D3DFVF_LIGHTVERTEX);
	
	// Draw primitives
	hr = theApp.m_d3dDevice8->DrawPrimitive (D3DPT_TRIANGLELIST, 0, 6);

	hr = theApp.m_d3dDevice8->EndScene ();
	
	// Render wind pyramid - set new viewport
	tempViewport.X = viewport.X + viewport.Width - 80;
	tempViewport.Y = viewport.Y + viewport.Height - 80;
	hr = theApp.m_d3dDevice8->SetViewport (&tempViewport);
	if (FAILED (hr))
	{
		return;
	}

	// Setup world matrix for wind pyramid
	hr = theApp.m_d3dDevice8->SetTransform (D3DTS_WORLD, &windPyramid);

	hr = theApp.m_d3dDevice8->Clear (0, NULL, D3DCLEAR_ZBUFFER, 0, 1.0f, 0);
	if (hr != D3D_OK)
	{
		return;
	}

	hr = theApp.m_d3dDevice8->BeginScene ();
	
	// Set light stream source
	hr = theApp.m_d3dDevice8->SetStreamSource (0, theApp.m_d3dWindVertexBuffer, sizeof (WINDVERTEX));
	
	// Set light shader
	hr = theApp.m_d3dDevice8->SetVertexShader (D3DFVF_WINDVERTEX);
	
	// Draw primitives
	hr = theApp.m_d3dDevice8->DrawPrimitive (D3DPT_TRIANGLELIST, 0, 6);

	hr = theApp.m_d3dDevice8->EndScene ();
	
	hr = theApp.m_d3dDevice8->Present (NULL, NULL, NULL, NULL);

	hr = theApp.m_d3dDevice8->SetViewport (&viewport);
	if (FAILED (hr))
	{
		return;
	}

	static float fps = 0.0f;

	if (m_animate)
	{
		static DWORD fpsLastSec = 0;
		static unsigned long fpsCounter = 0;
		
		++fpsCounter;
		DWORD curSec = timeGetTime ();
		if (curSec - fpsLastSec > 1000)
		{
			fps = (float) (fpsCounter * 1000) / (float) (curSec - fpsLastSec);
			fpsLastSec = curSec;
			fpsCounter = 0;
		}
	}
	else
	{
		fps = 0.0f;
	}

	ShowStats (fps, treeDistance);
}

void CTreeditorView::ShowStats (float fps, FLOAT distance)
{
	CWnd *stats = GetDlgItem (IDC_RENDERSTATS);

	if (NULL != stats)
	{
		CString str;
		str.Format ("Polygons: %d        Polyplanes: %d        FPS: %g        Distance: %g", g_N0, g_N1, fps, distance);
		stats->SetWindowText (str);
	}
}

void CTreeditorView::NextFrame ()
{
	if (m_animate)
	{
		Render ();
	}
}

/////////////////////////////////////////////////////////////////////////////
// CTreeditorView diagnostics

#ifdef _DEBUG
void CTreeditorView::AssertValid() const
{
	CFormView::AssertValid();
}

void CTreeditorView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CTreeditorDoc* CTreeditorView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTreeditorDoc)));
	return (CTreeditorDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTreeditorView message handlers

void CTreeditorView::OnEndLabelEditClasses(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	LV_ITEM      *pItem = &pDispInfo->item;
	CTreeditorDoc *doc = GetDocument ();
	if (pItem->pszText != NULL)
	{
		CTreeditorStringInput in (pItem->pszText);
		CTreeditorLex lex (&in, NULL, 0);
		STreeditorTokenDesc token;
		lex.GetNextToken (&token);
		if (token.type == treeditorLexId && 
			strcmp (&token.string[0], doc->GetClass (pItem->iItem)->name) != 0)
		{
			CTreeditorDocUndo *undo = doc->OpenUndo();
			if (undo != NULL)
			{
				if (doc->RenameClass (pItem->iItem, &token.string[0]))
				{
					CListCtrl *classes = (CListCtrl*) ((CMainFrame*) AfxGetMainWnd ())->_componentBar.GetDlgItem (IDC_CLASSES);
					classes->SetItemText (pItem->iItem, pItem->iSubItem, &token.string[0]);
					doc->SetModifiedFlag (TRUE);
					doc->UpdateStamp (true, false);
					UpdateClasses ();
					UpdateRender (INIT_TREE);
					doc->CloseUndo(undo);
				}
				else
				{
					doc->DisposeUndo (undo);
					CString str;
					str.Format ("Class name \"%s\" already exists.", &token.string[0]);
					AfxMessageBox (str, MB_OK | MB_ICONEXCLAMATION);
				}
			}
		}
	}

	*pResult = 0;
}

void CTreeditorView::OnItemChangingClasses(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here

	if (!_updateClassesTime)
	{
		CheckSelectedClasses ();
		UpdateProps ();
		UpdateClassType ();
		UpdateAttrs ();
		UpdateSlider ();
	}

	*pResult = 0;
}

void CTreeditorView::OnItemChangedClasses(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	if (!_updateClassesTime)
	{
		CheckSelectedClasses ();
		UpdateProps ();
		UpdateClassType ();
		UpdateAttrs ();
		UpdateSlider ();
	}

	*pResult = 0;
}

void CTreeditorView::OnUpdateNewClass(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable ();
}

void CTreeditorView::OnNewClass() 
{
	// TODO: Add your command handler code here
	CTreeditorDoc *doc = GetDocument ();
	CTreeditorDocUndo *undo = doc->OpenUndo();
	if (undo != NULL)
	{
		if (NULL != doc->AddNewClass (0))
		{
			doc->SetModifiedFlag (TRUE);
			doc->UpdateStamp (true, true);
			UpdateClasses ();
			UpdateRender (INIT_TREE);
			doc->CloseUndo(undo);
		}
		else
		{
			doc->DisposeUndo (undo);
		}
	}
}

void CTreeditorView::OnUpdateDeleteClass(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable (GetDocument ()->IsAnyClassSelected ());	
}

void CTreeditorView::OnDeleteClass() 
{
	// TODO: Add your command handler code here
	CTreeditorDoc *doc = GetDocument ();
	CTreeditorDocUndo *undo = doc->OpenUndo();
	if (undo != NULL)
	{
		doc->DeleteSelectedClasses ();
		doc->SetModifiedFlag (TRUE);
		doc->UpdateStamp (true, true);
		UpdateClasses ();
		UpdateRender (INIT_TREE);
		doc->CloseUndo(undo);
	}
}

void CTreeditorView::OnUpdateClassesDown(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable (GetDocument ()->IsAnyClassSelected ());	
}

void CTreeditorView::OnClassesDown() 
{
	// TODO: Add your command handler code here
	CTreeditorDoc *doc = GetDocument ();
	CTreeditorDocUndo *undo = doc->OpenUndo();
	if (undo != NULL)
	{
		doc->MoveSelectedClassesDown ();
		doc->SetModifiedFlag (TRUE);
		doc->UpdateStamp (true, true);
		UpdateClasses ();
		UpdateRender (INIT_TREE);
		doc->CloseUndo(undo);
	}
}

void CTreeditorView::OnUpdateClassesUp(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable (GetDocument ()->IsAnyClassSelected ());	
}

void CTreeditorView::OnClassesUp() 
{
	// TODO: Add your command handler code here
	CTreeditorDoc *doc = GetDocument ();
	CTreeditorDocUndo *undo = doc->OpenUndo();
	if (undo != NULL)
	{
		doc->MoveSelectedClassesUp ();
		doc->SetModifiedFlag (TRUE);
		doc->UpdateStamp (true, true);
		UpdateClasses ();
		UpdateRender (INIT_TREE);
		doc->CloseUndo(undo);
	}
}

void CTreeditorView::OnUpdateEditCopy(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	CWnd *f = GetFocus ();
	CListCtrl *cl = (CListCtrl*) ((CMainFrame*) AfxGetMainWnd ())->_componentBar.GetDlgItem (IDC_CLASSES);
	CUiPropertiesCtrl &pr = ((CMainFrame*) AfxGetMainWnd ())->_propertyBar._propCtrl;

	if (f != NULL && cl != NULL)
	{
		if (f == cl)
		{
			pCmdUI->Enable (cl->GetSelectedCount () > 0);
			return;
		}
		else if (pr.IsPropCtrl (f))
		{
			pCmdUI->Enable (pr.GetSelectedPropCount () > 0);
			return;
		}
		else if (f == cl->GetEditControl ())
		{
			int st, en;
			((CEdit*) f)->GetSel (st, en);
			pCmdUI->Enable (en > st);
			return;
		}
	}

	pCmdUI->Enable (FALSE);	
}

void CTreeditorView::OnEditCopy() 
{
	// TODO: Add your command handler code here
	CWnd *f = GetFocus ();
	CListCtrl *cl = (CListCtrl*) ((CMainFrame*) AfxGetMainWnd ())->_componentBar.GetDlgItem (IDC_CLASSES);
	CUiPropertiesCtrl &pr = ((CMainFrame*) AfxGetMainWnd ())->_propertyBar._propCtrl;

	if (f != NULL && cl != NULL)
	{
		if (f == cl && cl->GetSelectedCount () > 0)
		{
			CopyClasses ();
		}
		else if (pr.IsPropCtrl (f) && pr.GetSelectedPropCount () > 0)
		{
			CopyProps ();
		}
		else if (f == cl->GetEditControl ())
		{
			((CEdit*) f)->Copy ();
		}
	}
}

void CTreeditorView::OnUpdateEditCut(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	CWnd *f = GetFocus ();
	CListCtrl *cl = (CListCtrl*) ((CMainFrame*) AfxGetMainWnd ())->_componentBar.GetDlgItem (IDC_CLASSES);
	CUiPropertiesCtrl &pr = ((CMainFrame*) AfxGetMainWnd ())->_propertyBar._propCtrl;

	if (f != NULL && cl != NULL)
	{
		if (f == cl)
		{
			pCmdUI->Enable (cl->GetSelectedCount () > 0);
			return;
		}
		else if (pr.IsPropCtrl (f))
		{
			pCmdUI->Enable (pr.GetSelectedPropCount () > 0);
			return;
		}
		else if (f == cl->GetEditControl ())
		{
			int st, en;
			((CEdit*) f)->GetSel (st, en);
			pCmdUI->Enable (en > st);
			return;
		}
	}

	pCmdUI->Enable (FALSE);	
}

void CTreeditorView::OnEditCut() 
{
	// TODO: Add your command handler code here
	CWnd *f = GetFocus ();
	CTreeditorDoc *doc = GetDocument ();
	CListCtrl *cl = (CListCtrl*) ((CMainFrame*) AfxGetMainWnd ())->_componentBar.GetDlgItem (IDC_CLASSES);
	CUiPropertiesCtrl &pr = ((CMainFrame*) AfxGetMainWnd ())->_propertyBar._propCtrl;

	if (f != NULL && cl != NULL)
	{
		if (f == cl && cl->GetSelectedCount () > 0)
		{
			CTreeditorDocUndo *undo = doc->OpenUndo();
			if (undo != NULL)
			{
				CopyClasses ();
				doc->DeleteSelectedClasses ();
				doc->SetModifiedFlag (TRUE);
				doc->UpdateStamp (true, true);
				UpdateClasses ();
				UpdateRender (INIT_TREE);
				doc->CloseUndo(undo);
			}
		}
		else if (pr.IsPropCtrl (f) && pr.GetSelectedPropCount () > 0)
		{
			CopyProps ();
		}
		else if (f == cl->GetEditControl ())
		{
			((CEdit*) f)->Copy ();
		}
	}
}

void CTreeditorView::OnUpdateEditPaste(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	CWnd *f = GetFocus ();
	CListCtrl *cl = (CListCtrl*) ((CMainFrame*) AfxGetMainWnd ())->_componentBar.GetDlgItem (IDC_CLASSES);

	if (f != NULL && cl != NULL)
	{
		CRuntimeClass *rc = f->GetRuntimeClass ();
		if (IsClipboardFormatAvailable ('TrCl'))
		{
			pCmdUI->Enable (TRUE);
			return;
		}
		else if (IsClipboardFormatAvailable ('TrPr') && cl->GetSelectedCount () > 0)
		{
			pCmdUI->Enable (TRUE);
			return;
		}
		else if (IsClipboardFormatAvailable (CF_TEXT) && 
			(f == cl->GetEditControl () || strcmp (rc->m_lpszClassName, "CEdit") == 0))
		{
			pCmdUI->Enable (TRUE);
			return;
		}
	}

	pCmdUI->Enable (FALSE);	
}

void CTreeditorView::OnEditPaste() 
{
	// TODO: Add your command handler code here
	CWnd *f = GetFocus ();
	CListCtrl *cl = (CListCtrl*) ((CMainFrame*) AfxGetMainWnd ())->_componentBar.GetDlgItem (IDC_CLASSES);

	if (f != NULL && cl != NULL)
	{
		CRuntimeClass *rc = f->GetRuntimeClass ();
		if (IsClipboardFormatAvailable ('TrCl'))
		{
			PasteClasses ();
		}
		else if (IsClipboardFormatAvailable ('TrPr') && cl->GetSelectedCount () > 0)
		{
			PasteProps ();
		}
		else if (IsClipboardFormatAvailable (CF_TEXT))
		{
			CEdit *edit;
			if (f == (edit = cl->GetEditControl ()))
			{
				edit->Paste ();
			}
			else if (strcmp (rc->m_lpszClassName, "CEdit") == 0)
			{
				((CEdit*) f)->Paste ();
			}
		}
	}
}

void CTreeditorView::OnSelChangeClassType() 
{
	// TODO: Add your control notification handler code here
	CTreeditorDoc *doc = GetDocument ();
	CTreeditorDocUndo *undo = doc->OpenUndo();
	if (undo != NULL)
	{
		CComboBox *classTypeCtrl = (CComboBox*) ((CMainFrame*) AfxGetMainWnd ())->_propertyBar.GetDlgItem (IDC_CLASSTYPE);
		doc->SetTypeInSelection (classTypeCtrl->GetCurSel ());
		doc->SetModifiedFlag (TRUE);
		doc->UpdateStamp (true, true);
		UpdateClasses ();
		UpdateRender (INIT_TREE);
		doc->CloseUndo(undo);
	}
}

void CTreeditorView::OnKeyDownClasses(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_KEYDOWN* pLVKeyDow = (LV_KEYDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	if (pLVKeyDow->wVKey == VK_SPACE || pLVKeyDow->wVKey == VK_RETURN)
	{
		CListCtrl *classes = (CListCtrl*) ((CMainFrame*) AfxGetMainWnd ())->_componentBar.GetDlgItem (IDC_CLASSES);
		POSITION pos = classes->GetFirstSelectedItemPosition ();
		if (pos != NULL)
		{
			classes->EditLabel (classes->GetNextSelectedItem (pos));
		}
	}

	*pResult = 0;
}

void CTreeditorView::OnDblclkClasses(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	
	CListCtrl *classes = (CListCtrl*) ((CMainFrame*) AfxGetMainWnd ())->_componentBar.GetDlgItem (IDC_CLASSES);
	POSITION pos = classes->GetFirstSelectedItemPosition ();
	if (pos != NULL)
	{
		classes->EditLabel (classes->GetNextSelectedItem (pos));
	}
	
	*pResult = 0;
}

void CTreeditorView::CurveDlgLinearHookProc (const CCurveDlg *dlg, long param1, long param2, long param3)
{
	CTreeditorView *view = (CTreeditorView*) param1;
	CString str;
	dlg->GetLinearCurveString (str);
	view->ChangeParam (false, NULL, param2, param3, str, false, false, false, true);
}

void CTreeditorView::CurveDlgLinearWithXHookProc (const CCurveDlg *dlg, long param1, long param2, long param3)
{
	CTreeditorView *view = (CTreeditorView*) param1;
	CString str;
	dlg->GetLinearCurveWithXString (str);
	view->ChangeParam (false, NULL, param2, param3, str, false, false, false, true);
}

void CTreeditorView::CurveDlgQuadraticHookProc (const CCurveDlg *dlg, long param1, long param2, long param3)
{
	CTreeditorView *view = (CTreeditorView*) param1;
	CString str;
	dlg->GetQuadraticCurveString (str);
	view->ChangeParam (false, NULL, param2, param3, str, false, false, false, true);
}

void CTreeditorView::CurveDlgQuadraticWithXHookProc (const CCurveDlg *dlg, long param1, long param2, long param3)
{
	CTreeditorView *view = (CTreeditorView*) param1;
	CString str;
	dlg->GetQuadraticCurveWithXString (str);
	view->ChangeParam (false, NULL, param2, param3, str, false, false, false, true);
}

void CTreeditorView::CurveDlgExQuadraticHookProc (const CCurveDlg *dlg, long param1, long param2, long param3)
{
	CTreeditorView *view = (CTreeditorView*) param1;
	CString str;
	dlg->GetExQuadraticCurveString (str);
	view->ChangeParam (false, NULL, param2, param3, str, false, false, false, true);
}

void CTreeditorView::CurveDlgExQuadraticWithXHookProc (const CCurveDlg *dlg, long param1, long param2, long param3)
{
	CTreeditorView *view = (CTreeditorView*) param1;
	CString str;
	dlg->GetExQuadraticCurveWithXString (str);
	view->ChangeParam (false, NULL, param2, param3, str, false, false, false, true);
}

void CTreeditorView::ParseArrayOfFloats (LPCTSTR str, CArray<float, float> &vals)
{
	CTreeditorStringInput input (str);
	CTreeditorLex lex (&input, NULL, 0);
	STreeditorTokenDesc token;
	lex.GetNextToken (&token);
	while (token.type == treeditorLexLong || token.type == treeditorLexFixedFloat)
	{
		vals.Add(token.fnumber);
		lex.GetNextToken (&token);

		if (token.type != ',')
		{
			break;
		}
		lex.GetNextToken (&token);
	}
}

void CTreeditorView::OnReleasedCaptureParamSlider(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	CSliderCtrl *sliderCtrl = (CSliderCtrl*) ((CMainFrame*) AfxGetMainWnd ())->_propertyBar.GetDlgItem (IDC_PARAMSLIDER);
	CUiPropertiesCtrl &props = ((CMainFrame*) AfxGetMainWnd ())->_propertyBar._propCtrl;
	int item = props.GetNextSelectedProp (-1);
	if (item != -1)
	{
		int paramId = props.GetPropData (item);

		const STreeditorParamDesc *param = NULL;
		if (paramId < treeditorClassParam_numberOfTypes)
		{
			param = &CTreeditorParam::classParams[paramId];
		}
		else
		{
			CTreeditorDoc *doc = GetDocument ();
			CTreeditorClass *c = doc->GetSelectedClass ();
			if (c == NULL)
			{
				return;
			}
			param = c->_scriptParams[paramId - treeditorClassParam_numberOfTypes];
		}
		
		if ((param->useWhat & USE_SLIDER) != 0)
		{
			float f = (float) sliderCtrl->GetPos () / _sliderExp + param->minValue;
			CString str;
			switch (param->type)
			{
			case treeditorLexLong:
				str.Format ("%d", (long) f);
				break;

			case treeditorLexFixedFloat:
				str.Format ("%g", f);
				break;
			}

			if (_currentUndo != NULL)
			{
				ChangeParam (true, _currentUndo, paramId, item, str, true, true, !m_immediateResponse, true);
				_currentUndo = NULL;
			}
		}
	}

	*pResult = 0;
}

void CTreeditorView::OnDraw(CDC* pDC) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CFormView::OnDraw (pDC);
	
	if (theApp.m_d3dDevice8 != NULL)
	{
		if (::IsWindow (_renderView.m_hWnd) && _renderView.GetUpdateRect (NULL, FALSE))
		{
			if (theApp.m_resetDevice)
			{
				if (m_immediateResponse)
				{
					ResetRender ();
					UpdateRender (INIT_METER | INIT_TREE | INIT_GROUND | INIT_LIGHT_AND_WIND);
					theApp.m_resetDevice = false;
				}
				else
				{
					return;
				}
			}
			Render ();
			_renderView.ValidateRect (NULL);
		}
	}
}

void CTreeditorView::OnSize(UINT nType, int cx, int cy) 
{
#define SPACE1 4
#define SPACE2 7

	CFormView::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
#define CALC_CTRL_RECT(id, ctrl, r) CWnd *ctrl = GetDlgItem (id); if (ctrl == NULL) { return; } CRect r; ctrl->GetWindowRect(&r); ScreenToClient (&r)

	CALC_CTRL_RECT(IDC_RENDERVIEW, view, viewR);
	CALC_CTRL_RECT(IDC_RENDERSTATS, stats, statsR);

#undef CALC_CTRL_RECT

	viewR.SetRect (SPACE2, SPACE2, cx - SPACE2, cy - SPACE2 - (statsR.bottom - statsR.top) - SPACE2);
	statsR.SetRect (SPACE2, viewR.bottom + SPACE2, cx - SPACE2, cy - SPACE2);

	view->MoveWindow (&viewR, TRUE);
	stats->MoveWindow(&statsR, TRUE);

	ShowScrollBar (SB_BOTH, FALSE);

	theApp.m_resetDevice = true;

#undef SPACE1
#undef SPACE2
}

void CTreeditorView::CopyProps ()
{
	if (!OpenClipboard ())
	{
		return;
	}

	if (!EmptyClipboard ())
	{
		CloseClipboard ();
		return;
	}

	CUiPropertiesCtrl &props = ((CMainFrame*) AfxGetMainWnd ())->_propertyBar._propCtrl;
	int item = props.GetNextSelectedProp (-1);
	CString data;
	CString temp;
	while (item != -1)
	{
		DWORD index = props.GetPropData (item);
		if (index < treeditorClassParam_numberOfTypes)
		{
			temp.Format ("%d %s:", index, props.GetPropValue (item));
			data += temp;
		}
		item = props.GetNextSelectedProp (item);
	}
	int size = data.GetLength () + 1;
	HGLOBAL clip = GlobalAlloc (GMEM_MOVEABLE, size);
	if (clip == NULL)
	{
		CloseClipboard ();
		return;
	}

	LPVOID ptr = GlobalLock (clip);
	if (ptr == NULL)
	{
		GlobalFree (clip);
		CloseClipboard ();
		return;
	}

	CopyMemory (ptr, data, size);
	
	GlobalUnlock (clip);

	SetClipboardData ('TrPr', clip);

	CloseClipboard ();
}

void CTreeditorView::CopyClasses ()
{
	if (!OpenClipboard ())
	{
		return;
	}

	if (!EmptyClipboard ())
	{
		CloseClipboard ();
		return;
	}

	
	CTreeditorDoc *doc = GetDocument ();
	unsigned long ic;
	CString data;
	CString temp;
	for (CTreeditorClass *c = doc->GetFirstSelectedClass (&ic); c != NULL; c = doc->GetNextSelectedClass (&ic))
	{
		temp.Format ("%s %d ", c->name, c->type);
		data += temp;
		for (int i = 0; i < treeditorClassParam_numberOfTypes; ++i)
		{
			temp.Format ("%s:", c->params[i].GetValue ());
			data += temp;
		}
	}
	int size = data.GetLength () + 1;
	HGLOBAL clip = GlobalAlloc (GMEM_MOVEABLE, size);
	if (clip == NULL)
	{
		CloseClipboard ();
		return;
	}

	LPVOID ptr = GlobalLock (clip);
	if (ptr == NULL)
	{
		GlobalFree (clip);
		CloseClipboard ();
		return;
	}

	CopyMemory (ptr, data, size);
	
	GlobalUnlock (clip);

	SetClipboardData ('TrCl', clip);

	CloseClipboard ();
}

void CTreeditorView::PasteProps ()
{
	if (!OpenClipboard ())
	{
		return;
	}

	HANDLE clip = GetClipboardData ('TrPr');
	if (clip == NULL)
	{
		CloseClipboard ();
		return;
	}

	const char *str = (const char*) GlobalLock (clip);
	if (str == NULL)
	{
		CloseClipboard ();
		return;
	}

	CTreeditorDoc *doc = GetDocument ();
	CTreeditorDocUndo *undo = doc->OpenUndo();
	if (undo != NULL)
	{
		CTreeditorStringInput input (str);
		CTreeditorLex lex (&input, NULL, 0);
		STreeditorTokenDesc tok1;
		STreeditorTokenDesc tok2;
		CString tmp;
		
		lex.GetNextToken (&tok1);
		while (tok1.type != treeditorLexEof)
		{
			CString val;
			do {
				lex.GetNextToken (&tok2);
				if (tok1.type == treeditorLexLong)
				{
					switch (tok2.type)
					{
					case treeditorLexLong:
						tmp.Format ("%d", tok2.number);
						break;
						
					case treeditorLexFixedFloat:
						tmp.Format ("%g", tok2.fnumber);
						break;
						
					case treeditorLexString:
						tmp.Format ("\"%s\"", tok2.string);
						break;
						
					case treeditorLexId:
						tmp = tok2.string;
						break;
					}
					val += tmp;
					lex.GetNextToken (&tok2);
					if (tok2.type == ',')
					{
						val += _T (',');
					}
					else
					{
						tok2.type = ':';
					}
				}
				else
				{
					tok2.type = ':';
				}
			} while (tok2.type == ',');
			doc->SetParamInSelection (tok1.number, val);
			lex.GetNextToken (&tok1);
		}
		
		doc->SetModifiedFlag (TRUE);
		doc->UpdateStamp (true, true);
		UpdateProps ();
		UpdateAttrs ();
		UpdateSlider ();
		UpdateRender (INIT_TREE);
		doc->CloseUndo(undo);
	}

	GlobalUnlock (clip);
	CloseClipboard ();
}

void CTreeditorView::PasteClasses ()
{
	if (!OpenClipboard ())
	{
		return;
	}

	HANDLE clip = GetClipboardData ('TrCl');
	if (clip == NULL)
	{
		CloseClipboard ();
		return;
	}

	const char *str = (const char*) GlobalLock (clip);
	if (str == NULL)
	{
		CloseClipboard ();
		return;
	}

	CTreeditorDoc *doc = GetDocument ();
	CTreeditorDocUndo *undo = doc->OpenUndo();
	if (undo != NULL)
	{
		CTreeditorStringInput input (str);
		CTreeditorLex lex (&input, NULL, 0);
		STreeditorTokenDesc tok;
		CString par;
		int num = 0;
		
		lex.GetNextToken (&tok);
		while (tok.type == treeditorLexId)
		{
			CTreeditorClass *c = doc->AddNewClass (num++);
			if (NULL != c)
			{
				if (!doc->ClassNameExists (tok.string))
				{
					c->name = tok.string;
				}
				
				lex.GetNextToken (&tok);
				if (tok.type == treeditorLexLong)
				{
					c->type = tok.number;
					
					for (int i = 0; i < treeditorClassParam_numberOfTypes; ++i)
					{
						par.Empty ();
						CString tmp;
						do {
							lex.GetNextToken (&tok);
							switch (tok.type)
							{
							case treeditorLexLong:
								tmp.Format ("%d", tok.number);
								par += tmp;
								break;
								
							case treeditorLexFixedFloat:
								tmp.Format ("%g", tok.fnumber);
								par += tmp;
								break;
								
							case treeditorLexString:
								tmp.Format ("\"%s\"", tok.string);
								par += tmp;
								break;
								
							case treeditorLexId:
								tmp = tok.string;
								par += tmp;
								break;
								
							case ',':
								tmp = _T (',');
								par += tmp;
								break;
								
							case treeditorLexEof:
								tok.type = ':';
								break;
							}
						} while (tok.type != ':');
						c->params[i].SetValue (par);
					}
				}
			}
			lex.GetNextToken (&tok);
		}
		
		doc->SetModifiedFlag (TRUE);
		doc->UpdateStamp (true, true);
		UpdateClasses ();
		UpdateRender (INIT_TREE);
		doc->CloseUndo(undo);
	}

	GlobalUnlock (clip);
	CloseClipboard ();
}

void CTreeditorView::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: Add your message handler code here and/or call default
	CFormView::OnHScroll(nSBCode, nPos, pScrollBar);

	if (pScrollBar == ((CMainFrame*) AfxGetMainWnd ())->_propertyBar.GetDlgItem (IDC_PARAMSLIDER))
	{
		CSliderCtrl *sliderCtrl = (CSliderCtrl*) ((CMainFrame*) AfxGetMainWnd ())->_propertyBar.GetDlgItem (IDC_PARAMSLIDER);
		CUiPropertiesCtrl &props = ((CMainFrame*) AfxGetMainWnd ())->_propertyBar._propCtrl;
		int item = props.GetNextSelectedProp (-1);
		if (item != -1)
		{
			int paramId = props.GetPropData (item);

			const STreeditorParamDesc *param = NULL;
			if (paramId < treeditorClassParam_numberOfTypes)
			{
				param = &CTreeditorParam::classParams[paramId];
			}
			else
			{
				CTreeditorDoc *doc = GetDocument ();
				CTreeditorClass *c = doc->GetSelectedClass ();
				if (c == NULL)
				{
					return;
				}
				param = c->_scriptParams[paramId - treeditorClassParam_numberOfTypes];
			}

			if ((param->useWhat & USE_SLIDER) != 0)
			{
				float f = (float) sliderCtrl->GetPos () / _sliderExp + param->minValue;
				CString str;
				switch (param->type)
				{
				case treeditorLexLong:
					str.Format ("%d", (long) f);
					break;
					
				case treeditorLexFixedFloat:
					str.Format ("%g", f);
					break;
				}

				CTreeditorDoc *doc = GetDocument ();
				if (_currentUndo == NULL)
				{
					_currentUndo = doc->OpenUndo();
				}
				ChangeParam (false, NULL, paramId, item, str, true, true, m_immediateResponse, true);
			}
		}
	}
	else if (pScrollBar == ((CMainFrame*) AfxGetMainWnd ())->_componentBar.GetDlgItem (IDC_DETAILSLIDER))
	{
		_updateComponentsTime = true;

		CString str;

#ifndef DETAIL_IS_N_POLYGONS
		int detailSlider = 100;
		GET_INT_FROM_SLIDER (detailSlider, _componentBar, IDC_DETAILSLIDER)
		float detail = (float) detailSlider / 100.0f;
		PUT_FLOAT_IN_WND (detail, _componentBar, IDC_DETAIL)
		str.Format (_T ("%g"), detail);
#else
		int detailSlider = N_POLYGONS_MAX;
		GET_INT_FROM_SLIDER (detailSlider, _componentBar, IDC_DETAILSLIDER)
		PUT_INT_IN_WND (detailSlider, _componentBar, IDC_DETAIL)
		str.Format (_T ("%d"), detailSlider);
#endif

		AfxGetApp ()->WriteProfileString(_T ("tree"), _T ("detail"), str);
		if (m_immediateResponse)
		{
			GetDocument ()->UpdateStamp (true, false);
			UpdateRender (INIT_TREE);
		}

		_updateComponentsTime = false;
	}
	else if (pScrollBar == ((CMainFrame*) AfxGetMainWnd ())->_componentBar.GetDlgItem (IDC_NEARSLIDER))
	{
		_updateComponentsTime = true;

		int nears = 0.0f;
		GET_INT_FROM_SLIDER(nears, _componentBar, IDC_NEARSLIDER)
		float neard = (float) nears;
		PUT_FLOAT_IN_WND(neard, _componentBar, IDC_NEAR)
		CString str;
		str.Format (_T ("%d"), neard);
		AfxGetApp()->WriteProfileString(_T ("tree"), _T ("near"), str);
		if (m_immediateResponse && m_autoDetail && !m_animate)
		{
			Render ();
		}
	
		_updateComponentsTime = false;
	}
	else if (pScrollBar == ((CMainFrame*) AfxGetMainWnd ())->_componentBar.GetDlgItem (IDC_FARSLIDER))
	{
		_updateComponentsTime = true;
	
		int fars = 500;
		GET_INT_FROM_SLIDER(fars, _componentBar, IDC_FARSLIDER)
		float fard = (float) fars;
		PUT_FLOAT_IN_WND(fard, _componentBar, IDC_FAR)
		CString str;
		str.Format (_T ("%g"), fard);
		AfxGetApp()->WriteProfileString(_T ("tree"), _T ("far"), str);
		if (m_immediateResponse && m_autoDetail && !m_animate)
		{
			Render ();
		}

		_updateComponentsTime = false;
	}
	else if (pScrollBar == ((CMainFrame*) AfxGetMainWnd ())->_componentBar.GetDlgItem (IDC_TREESSLIDER))
	{
		_updateComponentsTime = true;
	
		int trees = 1;
		GET_INT_FROM_SLIDER(trees, _componentBar, IDC_TREESSLIDER)
		PUT_INT_IN_WND (trees, _componentBar, IDC_TREES)
		AfxGetApp()->WriteProfileInt (_T ("tree"), _T ("number"), trees);
		if (m_immediateResponse)
		{
			GetDocument ()->UpdateStamp (true, false);
			UpdateRender (INIT_METER | INIT_TREE | INIT_GROUND);
		}

		_updateComponentsTime = false;
	}
	else if (pScrollBar == ((CMainFrame*) AfxGetMainWnd ())->_componentBar.GetDlgItem (IDC_DISTSLIDER))
	{
		_updateComponentsTime = true;
	
		int distances = 100;
		GET_INT_FROM_SLIDER(distances, _componentBar, IDC_DISTSLIDER)
		float distance = (float) distances / 100.0f;
		PUT_FLOAT_IN_WND(distance, _componentBar, IDC_DIST)
		CString str;
		str.Format (_T ("%g"), distance);
		AfxGetApp()->WriteProfileString (_T ("tree"), _T ("distance"), str);
		if (m_immediateResponse)
		{
			int trees = 1;
			GET_INT_FROM_WND(trees, _componentBar, IDC_TREES)

			if (trees > 1)
			{
				GetDocument ()->UpdateStamp (true, false);
				UpdateRender (INIT_METER | INIT_TREE | INIT_GROUND);
			}
			else
			{
				UpdateRender (INIT_METER | INIT_GROUND);
			}
		}

		_updateComponentsTime = false;
	}
	else if (pScrollBar == ((CMainFrame*) AfxGetMainWnd ())->_componentBar.GetDlgItem (IDC_METERSLIDER))
	{
		_updateComponentsTime = true;

		int meters = 100;
		GET_INT_FROM_SLIDER(meters, _componentBar, IDC_METERSLIDER)
		float meter = (float) meters / 100.0f;
		PUT_FLOAT_IN_WND(meter, _componentBar, IDC_METER)
		CString str;
		str.Format(_T ("%g"), meter);
		AfxGetApp ()->WriteProfileString (_T ("meter"), _T ("size"), str);
		if (m_immediateResponse)
		{
			UpdateRender (INIT_METER);
		}

		_updateComponentsTime = false;
	}
	else if (pScrollBar == ((CMainFrame*) AfxGetMainWnd ())->_componentBar.GetDlgItem (IDC_AGESLIDER))
	{
		_updateComponentsTime = true;

		int ages = 100;
		GET_INT_FROM_SLIDER(ages, _componentBar, IDC_AGESLIDER)
		float age = (float) ages / 100.0f;
		PUT_FLOAT_IN_WND(age, _componentBar, IDC_AGE)
		CString str;
		str.Format (_T ("%g"), age);
		AfxGetApp()->WriteProfileString(_T ("tree"), _T ("age"), str);
		if (m_immediateResponse)
		{
			GetDocument ()->UpdateStamp (true, false);
			UpdateRender (INIT_TREE);
		}

		_updateComponentsTime = false;
	}
	else if (pScrollBar == ((CMainFrame*) AfxGetMainWnd ())->_componentBar.GetDlgItem (IDC_WINDSLIDER))
	{
		_updateComponentsTime = true;

		int winds = 10;
		GET_INT_FROM_SLIDER(winds, _componentBar, IDC_WINDSLIDER)
		float wind = (float) winds / 10.0f;
		PUT_FLOAT_IN_WND(wind, _componentBar, IDC_WIND)
		CString str;
		str.Format (_T ("%g"), wind);
		AfxGetApp()->WriteProfileString (_T ("wind"), _T ("strength"), str);

		_updateComponentsTime = false;
	}
	else if (pScrollBar == ((CMainFrame*) AfxGetMainWnd ())->_componentBar.GetDlgItem (IDC_WROTYSLIDER))
	{
		_updateComponentsTime = true;

		CString str;
		int rot = 0;
		GET_INT_FROM_SLIDER(rot, _componentBar, IDC_WROTYSLIDER)
		AfxGetApp()->WriteProfileInt(_T ("wind"), _T ("roty"), rot);
		str.Format ("Wind RotY: %d", rot);
		PUT_TEXT_IN_WND(str, _componentBar, IDC_WROTYLABEL)

		if (m_immediateResponse)
		{
			Render ();
		}

		_updateComponentsTime = false;
	}
	else if (pScrollBar == ((CMainFrame*) AfxGetMainWnd ())->_componentBar.GetDlgItem (IDC_WROTZSLIDER))
	{
		_updateComponentsTime = true;

		CString str;
		int rot = 0;
		GET_INT_FROM_SLIDER(rot, _componentBar, IDC_WROTZSLIDER)
		AfxGetApp()->WriteProfileInt(_T ("wind"), _T ("rotz"), rot);
		str.Format ("Wind RotZ: %d", rot);
		PUT_TEXT_IN_WND(str, _componentBar, IDC_WROTZLABEL)

		if (m_immediateResponse)
		{
			Render ();
		}

		_updateComponentsTime = false;
	}
	else if (pScrollBar == ((CMainFrame*) AfxGetMainWnd ())->_componentBar.GetDlgItem (IDC_LROTYSLIDER))
	{
		_updateComponentsTime = true;

		CString str;
		int rot = 0;
		GET_INT_FROM_SLIDER(rot, _componentBar, IDC_LROTYSLIDER)
		AfxGetApp()->WriteProfileInt(_T ("light"), _T ("roty"), rot);
		str.Format ("Light RotY: %d", rot);
		PUT_TEXT_IN_WND(str, _componentBar, IDC_LROTYLABEL)

		if (m_immediateResponse)
		{
			Render ();
		}

		_updateComponentsTime = false;
	}
	else if (pScrollBar == ((CMainFrame*) AfxGetMainWnd ())->_componentBar.GetDlgItem (IDC_LROTZSLIDER))
	{
		_updateComponentsTime = true;

		CString str;
		int rot = 0;
		GET_INT_FROM_SLIDER(rot, _componentBar, IDC_LROTZSLIDER)
		AfxGetApp()->WriteProfileInt(_T ("light"), _T ("rotz"), rot);
		str.Format ("Light RotZ: %d", rot);
		PUT_TEXT_IN_WND(str, _componentBar, IDC_LROTZLABEL)

		if (m_immediateResponse)
		{
			Render ();
		}

		_updateComponentsTime = false;
	}
}

void CTreeditorView::OnChangeBgColor() 
{
	// TODO: Add your command handler code here
	COLORREF nc = ((m_bgColor << 16) & 0x00ff0000) 
		| (m_bgColor & 0x0000ff00) | ((m_bgColor >> 16) & 0x000000ff);
	CColorDialog d (m_bgColor, 0, this);
	int r = d.DoModal ();
	if (r == IDOK)
	{
		nc = d.GetColor ();
		m_bgColor = 0xff000000 | ((nc << 16) & 0x00ff0000) 
			| (nc & 0x0000ff00) | ((nc >> 16) & 0x000000ff);
		AfxGetApp()->WriteProfileInt(_T ("editor"), _T ("bgColor"), m_bgColor);
		Render ();
	}
}

void CTreeditorView::OnAnimate() 
{
	// TODO: Add your command handler code here
	m_animate = !m_animate;

	AfxGetApp()->WriteProfileInt(_T ("tree"), _T ("animate"), m_animate);
	if (!m_animate)
	{
		Render ();
	}
}

void CTreeditorView::OnUpdateAnimate(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->SetCheck (m_animate ? 1 : 0);
}

void CTreeditorView::OnTreeBackward() 
{
	// TODO: Add your command handler code here

	D3DXMatrixMultiply (&_matZoomTranslation, &_matZoomTranslation, &_matZoomBackward);
	if (!m_animate)
	{
		Render ();
	}
}

void CTreeditorView::OnTreeForward() 
{
	// TODO: Add your command handler code here
	
	D3DXMatrixMultiply (&_matZoomTranslation, &_matZoomTranslation, &_matZoomForward);
	if (!m_animate)
	{
		Render ();
	}
}

void CTreeditorView::OnTreeLeft() 
{
	// TODO: Add your command handler code here
	
	D3DXMatrixMultiply (&_matZoomTranslation, &_matZoomTranslation, &_matZoomLeft);
	if (!m_animate)
	{
		Render ();
	}
}

void CTreeditorView::OnTreeRight() 
{
	// TODO: Add your command handler code here
	
	D3DXMatrixMultiply (&_matZoomTranslation, &_matZoomTranslation, &_matZoomRight);
	if (!m_animate)
	{
		Render ();
	}
}

void CTreeditorView::OnTreeDown() 
{
	// TODO: Add your command handler code here
	
	D3DXMatrixMultiply (&_matZoomTranslation, &_matZoomTranslation, &_matZoomDown);
	if (!m_animate)
	{
		Render ();
	}
}

void CTreeditorView::OnTreeUp() 
{
	// TODO: Add your command handler code here
	
	D3DXMatrixMultiply (&_matZoomTranslation, &_matZoomTranslation, &_matZoomUp);
	if (!m_animate)
	{
		Render ();
	}
}

void CTreeditorView::OnTreeRotUp() 
{
	// TODO: Add your command handler code here
	
	D3DXMatrixMultiply (&_matZoomRotation, &_matZoomRotation, &_matRotUp);
	if (!m_animate)
	{
		Render ();
	}
}

void CTreeditorView::OnTreeRotDown() 
{
	// TODO: Add your command handler code here
	
	D3DXMatrixMultiply (&_matZoomRotation, &_matZoomRotation, &_matRotDown);
	if (!m_animate)
	{
		Render ();
	}
}

void CTreeditorView::OnTreeRotLeft() 
{
	// TODO: Add your command handler code here
	
	D3DXMatrixMultiply (&_matZoomRotation, &_matZoomRotation, &_matRotLeft);
	if (!m_animate)
	{
		Render ();
	}
}

void CTreeditorView::OnTreeRotRight() 
{
	// TODO: Add your command handler code here
	
	D3DXMatrixMultiply (&_matZoomRotation, &_matZoomRotation, &_matRotRight);
	if (!m_animate)
	{
		Render ();
	}
}

void CTreeditorView::OnChangeDetail() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here

	if (_updateComponentsTime)
	{
		return;
	}

	CString str;

#ifndef DETAIL_IS_N_POLYGONS
	float detail = 1.0f;
	GET_FLOAT_FROM_WND(detail, _componentBar, IDC_DETAIL)
	PUT_INT_IN_SLIDER ((int) (detail * 100.0f), _componentBar, IDC_DETAILSLIDER)
	str.Format (_T ("%g"), detail);
#else
	int detail = N_POLYGONS_MAX;
	GET_INT_FROM_WND(detail, _componentBar, IDC_DETAIL)
	PUT_INT_IN_SLIDER (detail, _componentBar, IDC_DETAILSLIDER)
	str.Format (_T ("%d"), detail);
#endif

	AfxGetApp ()->WriteProfileString(_T ("tree"), _T ("detail"), str);

	if (m_immediateResponse)
	{
		GetDocument ()->UpdateStamp (true, false);
		UpdateRender (INIT_TREE);
	}
}

void CTreeditorView::OnKillFocusDetail() 
{
	// TODO: Add your control notification handler code here
	
	if (_updateComponentsTime)
	{
		return;
	}

	if (m_immediateResponse)
	{
		return;
	}

#ifndef DETAIL_IS_N_POLYGONS
	float detail = 1.0f;
	GET_FLOAT_FROM_WND(detail, _componentBar, IDC_DETAIL)
	PUT_INT_IN_SLIDER((int) (detail * 100.0f), _componentBar, IDC_DETAILSLIDER)
#else
	int detail = N_POLYGONS_MAX;
	GET_INT_FROM_WND(detail, _componentBar, IDC_DETAIL)
	PUT_INT_IN_SLIDER(detail, _componentBar, IDC_DETAILSLIDER)
#endif
	GetDocument ()->UpdateStamp (true, false);
	UpdateRender (INIT_TREE);
}

void CTreeditorView::OnReleasedCaptureDetailSlider(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	
	if (m_immediateResponse)
	{
		*pResult = 0;
		return;
	}

	_updateComponentsTime = true;

#ifndef DETAIL_IS_N_POLYGONS
	int detailSlider = 100;
	GET_INT_FROM_SLIDER(detailSlider, _componentBar, IDC_DETAILSLIDER)
	float detail = (float) detailSlider / 100.0f;
	PUT_FLOAT_IN_WND(detail, _componentBar, IDC_DETAIL)
#else
	int detailSlider = N_POLYGONS_MAX;
	GET_INT_FROM_SLIDER(detailSlider, _componentBar, IDC_DETAILSLIDER)
	PUT_INT_IN_WND(detailSlider, _componentBar, IDC_DETAIL)
#endif

	UpdateData (FALSE);
	GetDocument ()->UpdateStamp (true, false);
	UpdateRender (INIT_TREE);

	_updateComponentsTime = false;

	*pResult = 0;
}

void CTreeditorView::OnChangeNear() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here

	if (_updateComponentsTime)
	{
		return;
	}

	float neard = 0.0f;
	GET_FLOAT_FROM_WND(neard, _componentBar, IDC_NEAR)
	CString str;
	str.Format (_T ("%g"), neard);
	AfxGetApp ()->WriteProfileString (_T ("tree"), _T ("near"), str);
	PUT_INT_IN_SLIDER((int) neard, _componentBar, IDC_NEARSLIDER)
	if (m_immediateResponse && m_autoDetail && !m_animate)
	{
		Render ();
	}
}

void CTreeditorView::OnKillFocusNear() 
{
	// TODO: Add your control notification handler code here
	
	if (_updateComponentsTime)
	{
		return;
	}

	if (m_immediateResponse)
	{
		return;
	}

	float neard = 0.0f;
	GET_FLOAT_FROM_WND(neard, _componentBar, IDC_NEAR)
	PUT_INT_IN_SLIDER((int) neard, _componentBar, IDC_NEARSLIDER)

	if (m_autoDetail && !m_animate)
	{
		Render ();
	}
}

void CTreeditorView::OnReleasedCaptureNearSlider(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	
	if (m_immediateResponse)
	{
		*pResult = 0;
		return;
	}

	_updateComponentsTime = true;

	int nears = 0;
	GET_INT_FROM_SLIDER(nears, _componentBar, IDC_NEARSLIDER)
	float neard = (float) nears;
	PUT_FLOAT_IN_WND(neard, _componentBar, IDC_NEAR)

	if (m_autoDetail && !m_animate)
	{
		Render ();
	}

	_updateComponentsTime = false;
	*pResult = 0;
}

void CTreeditorView::OnChangeFar() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here

	if (_updateComponentsTime)
	{
		return;
	}

	float fard = 500.0f;
	GET_FLOAT_FROM_WND(fard, _componentBar, IDC_FAR)
	CString str;
	str.Format (_T ("%g"), fard);
	AfxGetApp ()->WriteProfileString(_T ("tree"), _T ("far"), str);
	PUT_INT_IN_SLIDER((int) fard, _componentBar, IDC_FARSLIDER)
	if (m_immediateResponse && m_autoDetail && !m_animate)
	{
		Render ();
	}
}

void CTreeditorView::OnKillFocusFar() 
{
	// TODO: Add your control notification handler code here
	
	if (_updateComponentsTime)
	{
		return;
	}

	if (m_immediateResponse)
	{
		return;
	}

	float fard = 500.0f;
	GET_FLOAT_FROM_WND(fard, _componentBar, IDC_FAR)
	PUT_INT_IN_SLIDER((int) fard, _componentBar, IDC_FARSLIDER)

	if (m_autoDetail && !m_animate)
	{
		Render ();
	}
}

void CTreeditorView::OnReleasedCaptureFarSlider(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	
	if (m_immediateResponse)
	{
		*pResult = 0;
		return;
	}

	_updateComponentsTime = true;

	int fars = 500;
	GET_INT_FROM_SLIDER(fars, _componentBar, IDC_FARSLIDER)
	float fard = (float) fars;
	PUT_FLOAT_IN_WND(fard, _componentBar, IDC_FAR)

	if (m_autoDetail && !m_animate)
	{
		Render ();
	}

	_updateComponentsTime = false;
	*pResult = 0;
}

void CTreeditorView::OnChangeTrees() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	
	if (_updateComponentsTime)
	{
		return;
	}

	int trees = 1;
	GET_INT_FROM_WND(trees, _componentBar, IDC_TREES)

	AfxGetApp ()->WriteProfileInt (_T ("tree"), _T ("number"), trees);
	PUT_INT_IN_SLIDER(trees, _componentBar, IDC_TREESSLIDER)

	if (m_immediateResponse)
	{
		GetDocument ()->UpdateStamp (true, false);
		UpdateRender (INIT_METER | INIT_TREE | INIT_GROUND);
	}
}

void CTreeditorView::OnKillFocusTrees() 
{
	// TODO: Add your control notification handler code here
	
	if (_updateComponentsTime)
	{
		return;
	}

	if (m_immediateResponse)
	{
		return;
	}

	int trees = 1;
	GET_INT_FROM_WND(trees, _componentBar, IDC_TREES)
	PUT_INT_IN_SLIDER(trees, _componentBar, IDC_TREESSLIDER)

	GetDocument ()->UpdateStamp (true, false);
	UpdateRender (INIT_METER | INIT_TREE | INIT_GROUND);
}

void CTreeditorView::OnReleasedCaptureTreesSlider(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	
	if (m_immediateResponse)
	{
		*pResult = 0;
		return;
	}

	_updateComponentsTime = true;

	int trees = 1;
	GET_INT_FROM_SLIDER(trees, _componentBar, IDC_TREESSLIDER)
	PUT_INT_IN_WND(trees, _componentBar, IDC_TREES)

	GetDocument ()->UpdateStamp (true, false);
	UpdateRender (INIT_METER | INIT_TREE | INIT_GROUND);

	_updateComponentsTime = false;
	*pResult = 0;
}

void CTreeditorView::OnChangeDist() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here

	if (_updateComponentsTime)
	{
		return;
	}

	float distance = 1.0f;
	GET_FLOAT_FROM_WND(distance, _componentBar, IDC_DIST)
	CString str;
	str.Format (_T ("%g"), distance);
	AfxGetApp ()->WriteProfileString(_T ("tree"), _T ("distance"), str);
	PUT_INT_IN_SLIDER((int) (distance * 100.0f), _componentBar, IDC_DISTSLIDER)
	if (m_immediateResponse)
	{
		int trees = 1;
		GET_INT_FROM_WND(trees, _componentBar, IDC_TREES)

		if (trees > 1)
		{
			GetDocument ()->UpdateStamp (true, false);
			UpdateRender (INIT_METER | INIT_TREE | INIT_GROUND);
		}
		else
		{
			UpdateRender (INIT_METER | INIT_GROUND);
		}
	}
}

void CTreeditorView::OnKillFocusDist() 
{
	// TODO: Add your control notification handler code here
	
	if (_updateComponentsTime)
	{
		return;
	}

	if (m_immediateResponse)
	{
		return;
	}

	float distance = 1.0f;
	GET_FLOAT_FROM_WND(distance, _componentBar, IDC_DIST)
	PUT_INT_IN_SLIDER((int) (distance * 100.0f), _componentBar, IDC_DISTSLIDER)

	int trees = 1;
	GET_INT_FROM_WND(trees, _componentBar, IDC_TREES)

	if (trees > 1)
	{
		GetDocument ()->UpdateStamp (true, false);
		UpdateRender (INIT_METER | INIT_TREE | INIT_GROUND);
	}
	else
	{
		UpdateRender (INIT_METER | INIT_GROUND);
	}
}

void CTreeditorView::OnReleasedCaptureDistSlider(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here

	if (m_immediateResponse)
	{
		*pResult = 0;
		return;
	}

	_updateComponentsTime = true;

	int distances = 100;
	GET_INT_FROM_SLIDER(distances, _componentBar, IDC_DISTSLIDER)
	PUT_FLOAT_IN_WND((float) distances / 100.0f, _componentBar, IDC_DIST)

	int trees = 1;
	GET_INT_FROM_WND(trees, _componentBar, IDC_TREES)

	if (trees > 1)
	{
		GetDocument ()->UpdateStamp (true, false);
		UpdateRender (INIT_METER | INIT_TREE | INIT_GROUND);
	}
	else
	{
		UpdateRender (INIT_METER | INIT_GROUND);
	}


	_updateComponentsTime = false;
	*pResult = 0;
}

void CTreeditorView::OnChangeMeter() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here

	if (_updateComponentsTime)
	{
		return;
	}

	float meter = 1.0f;
	GET_FLOAT_FROM_WND(meter, _componentBar, IDC_METER)
	CString str;
	str.Format (_T ("%g"), meter);
	AfxGetApp ()->WriteProfileString(_T ("meter"), _T ("size"), str);
	PUT_INT_IN_SLIDER((int) (meter * 100.0f), _componentBar, IDC_METERSLIDER)
	if (m_immediateResponse)
	{
		UpdateRender (INIT_METER);
	}
}

void CTreeditorView::OnKillFocusMeter() 
{
	// TODO: Add your control notification handler code here
	
	if (_updateComponentsTime)
	{
		return;
	}

	if (m_immediateResponse)
	{
		return;
	}

	float meter = 1.0f;
	GET_FLOAT_FROM_WND(meter, _componentBar, IDC_METER)
	PUT_INT_IN_SLIDER((int) (meter * 100.0f), _componentBar, IDC_METERSLIDER)
	UpdateRender (INIT_METER);
}

void CTreeditorView::OnReleasedCaptureMeterSlider(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	
	if (m_immediateResponse)
	{
		*pResult = 0;
		return;
	}

	_updateComponentsTime = true;

	int meters = 100;
	GET_INT_FROM_SLIDER(meters, _componentBar, IDC_METERSLIDER)
	float meter = (float) meters / 100.0f;
	PUT_FLOAT_IN_WND(meter, _componentBar, IDC_METER)
	UpdateRender (INIT_METER);

	_updateComponentsTime = false;
	*pResult = 0;
}

void CTreeditorView::OnChangeAge() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here

	if (_updateComponentsTime)
	{
		return;
	}

	float age = 1.0f;
	GET_FLOAT_FROM_WND(age, _componentBar, IDC_AGE)
	CString str;
	str.Format (_T ("%g"), age);
	AfxGetApp ()->WriteProfileString(_T ("tree"), _T ("age"), str);
	PUT_INT_IN_SLIDER((int) (age * 100.0f), _componentBar, IDC_AGESLIDER)
	if (m_immediateResponse)
	{
		GetDocument ()->UpdateStamp (true, false);
		UpdateRender (INIT_TREE);
	}
}

void CTreeditorView::OnKillFocusAge() 
{
	// TODO: Add your control notification handler code here
	
	if (_updateComponentsTime)
	{
		return;
	}

	if (m_immediateResponse)
	{
		return;
	}

	float age = 1.0f;
	GET_FLOAT_FROM_WND(age, _componentBar, IDC_AGE)
	PUT_INT_IN_SLIDER((int) (age * 100.0f), _componentBar, IDC_AGESLIDER)
	GetDocument ()->UpdateStamp (true, false);
	UpdateRender (INIT_TREE);
}

void CTreeditorView::OnReleasedCaptureAgeSlider(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	
	if (m_immediateResponse)
	{
		*pResult = 0;
		return;
	}

	_updateComponentsTime = true;

	int ages = 100;
	GET_INT_FROM_SLIDER(ages, _componentBar, IDC_AGESLIDER)
	float age = (float) ages / 100.0f;
	PUT_FLOAT_IN_WND(age, _componentBar, IDC_AGE)
	GetDocument ()->UpdateStamp (true, false);
	UpdateRender (INIT_TREE);

	_updateComponentsTime = false;
	*pResult = 0;
}

void CTreeditorView::OnChangeWind() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here

	if (_updateComponentsTime)
	{
		return;
	}

	float wind = 1.0f;
	GET_FLOAT_FROM_WND(wind, _componentBar, IDC_WIND)
	CString str;
	str.Format (_T ("%g"), wind);
	AfxGetApp ()->WriteProfileString(_T ("wind"), _T ("strength"), str);
	PUT_INT_IN_SLIDER((int) (wind * 10.0f), _componentBar, IDC_WINDSLIDER)
}

void CTreeditorView::OnKillFocusWind() 
{
	// TODO: Add your control notification handler code here
	
	if (_updateComponentsTime)
	{
		return;
	}

	if (m_immediateResponse)
	{
		return;
	}

	float wind = 1.0f;
	GET_FLOAT_FROM_WND(wind, _componentBar, IDC_WIND)
	PUT_INT_IN_SLIDER((int) (wind * 10.0f), _componentBar, IDC_WINDSLIDER)
}

void CTreeditorView::OnReleasedCaptureWindSlider(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	
	if (m_immediateResponse)
	{
		*pResult = 0;
		return;
	}

	_updateComponentsTime = true;

	int winds = 10;
	GET_INT_FROM_SLIDER(winds, _componentBar, IDC_WINDSLIDER)
	PUT_FLOAT_IN_WND((float) winds / 10.0f, _componentBar, IDC_WIND)

	_updateComponentsTime = false;
	*pResult = 0;
}

void CTreeditorView::OnReleasedCaptureWRotYSlider(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	
	if (m_immediateResponse)
	{
		*pResult = 0;
		return;
	}

	_updateComponentsTime = true;

	Render ();

	_updateComponentsTime = false;
	*pResult = 0;
}

void CTreeditorView::OnReleasedCaptureWRotZSlider(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	
	if (m_immediateResponse)
	{
		*pResult = 0;
		return;
	}

	_updateComponentsTime = true;

	Render ();

	_updateComponentsTime = false;
	*pResult = 0;
}

void CTreeditorView::OnReleasedCaptureLRotYSlider(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	
	if (m_immediateResponse)
	{
		*pResult = 0;
		return;
	}

	_updateComponentsTime = true;

	Render ();

	_updateComponentsTime = false;
	*pResult = 0;
}

void CTreeditorView::OnReleasedCaptureLRotZSlider(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	
	if (m_immediateResponse)
	{
		*pResult = 0;
		return;
	}

	_updateComponentsTime = true;

	Render ();

	_updateComponentsTime = false;
	*pResult = 0;
}

void CTreeditorView::OnImmediateResponse() 
{
	// TODO: Add your command handler code here
	m_immediateResponse = !m_immediateResponse;
	AfxGetApp ()->WriteProfileInt (_T ("editor"), _T ("immediateResponse"), m_immediateResponse);
}

void CTreeditorView::OnUpdateImmediateResponse(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->SetCheck (m_immediateResponse ? 1 : 0);
}

void CTreeditorView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	if (::IsWindow (_renderView.m_hWnd) && &_renderView == ChildWindowFromPoint (point, CWP_SKIPINVISIBLE | CWP_SKIPDISABLED))
	{
		if ((nFlags & MK_CONTROL) != 0)
		{
			Pick (point);
		}

		m_buttonDownPoint = point;
		if (++m_buttonDown == 1)
		{
			SetCapture ();
		}
	}
	else
	{
		CFormView::OnLButtonDown(nFlags, point);
	}
}

void CTreeditorView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default

	if (m_buttonDown > 0)
	{
		if (--m_buttonDown == 0)
		{
			ReleaseCapture ();
		}
	}
	else
	{
		CFormView::OnLButtonUp(nFlags, point);
	}
}

void CTreeditorView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	if (::IsWindow (_renderView.m_hWnd) && &_renderView == ChildWindowFromPoint (point, CWP_SKIPINVISIBLE | CWP_SKIPDISABLED))
	{
		m_buttonDownPoint = point;
		if (++m_buttonDown == 1)
		{
			SetCapture ();
		}
	}
	else
	{
		CFormView::OnLButtonDown(nFlags, point);
	}
}

void CTreeditorView::OnRButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	if (m_buttonDown > 0)
	{
		if (--m_buttonDown == 0)
		{
			ReleaseCapture ();
		}
	}
	else
	{
		CFormView::OnLButtonUp(nFlags, point);
	}
}

void CTreeditorView::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default

	if (m_buttonDown > 0 && ((nFlags & MK_LBUTTON) != 0))
	{
		if (m_buttonDown > 0 && ((nFlags & MK_RBUTTON) != 0))
		{
			static D3DXMATRIX mat;
			float f;
			
			f = (((double) (point.x - m_buttonDownPoint.x) / 8.0));
			D3DXMatrixRotationZ (&mat, -ROT_STEP * f);
			m_buttonDownPoint.x = point.x;
			D3DXMatrixMultiply (&_matZoomRotation, &_matZoomRotation, &mat);
			
			f = (((double) (point.y - m_buttonDownPoint.y) / 8.0));
			D3DXMatrixTranslation (&mat, 0.0f, 0.0f, MOVE_STEP * f);
			m_buttonDownPoint.y = point.y;
			D3DXMatrixMultiply (&_matZoomTranslation, &_matZoomTranslation, &mat);
			
			if (!m_animate)
			{
				Render ();
			}
		}
		else
		{
			static D3DXMATRIX mat;
			float f;
			
			f = (((double) (point.x - m_buttonDownPoint.x) / 8.0));
			D3DXMatrixTranslation (&mat, MOVE_STEP * f, 0.0f, 0.0f);
			m_buttonDownPoint.x = point.x;
			D3DXMatrixMultiply (&_matZoomTranslation, &_matZoomTranslation, &mat);
			
			f = (((double) (point.y - m_buttonDownPoint.y) / 8.0));
			D3DXMatrixTranslation (&mat, 0.0f, -MOVE_STEP * f, 0.0f);
			m_buttonDownPoint.y = point.y;
			D3DXMatrixMultiply (&_matZoomTranslation, &_matZoomTranslation, &mat);
			
			if (!m_animate)
			{
				Render ();
			}
		}
	}
	else if (m_buttonDown > 0 && ((nFlags & MK_RBUTTON) != 0))
	{
		static D3DXMATRIX mat;
		float f;
		
		f = (((double) (point.x - m_buttonDownPoint.x) / 8.0));
		D3DXMatrixRotationY (&mat, -ROT_STEP * f);
		m_buttonDownPoint.x = point.x;
		D3DXMatrixMultiply (&_matZoomRotation, &_matZoomRotation, &mat);
		
		f = (((double) (point.y - m_buttonDownPoint.y) / 8.0));
		D3DXMatrixRotationX (&mat, -ROT_STEP * f);
		m_buttonDownPoint.y = point.y;
		D3DXMatrixMultiply (&_matZoomRotation, &_matZoomRotation, &mat);
		
		if (!m_animate)
		{
			Render ();
		}
	}
	else
	{
		CFormView::OnMouseMove(nFlags, point);
	}
}

void CTreeditorView::OnCenterTreeView() 
{
	// TODO: Add your command handler code here
	
	D3DXMatrixTranslation (&_matZoomTranslation, 0.0f, -1.0f, 5.0f);
	D3DXMatrixIdentity (&_matZoomRotation);
	if (!m_animate)
	{
		Render ();
	}
}

void CTreeditorView::OnUpdateShowMeter(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
	pCmdUI->SetCheck (m_showMeter ? 1 : 0);
}

void CTreeditorView::OnShowMeter() 
{
	// TODO: Add your command handler code here
	
	m_showMeter = !m_showMeter;
	AfxGetApp ()->WriteProfileInt (_T ("meter"), _T ("show"), m_showMeter);
	if (!m_animate)
	{
		Render ();
	}
}

void CTreeditorView::OnWireframe() 
{
	// TODO: Add your command handler code here
	
	m_wireframe = !m_wireframe;
	SetRenderFillMode ();
	AfxGetApp ()->WriteProfileInt (_T ("editor"), _T ("wireframe"), m_wireframe);
	if (!m_animate)
	{
		GetDocument ()->UpdateStamp (true, false);
		UpdateRender (INIT_TREE);
	}
}

void CTreeditorView::OnUpdateWireframe(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
	pCmdUI->SetCheck (m_wireframe ? 1 : 0);
}

void CTreeditorView::OnShowBigLightPyramid() 
{
	// TODO: Add your command handler code here

	m_showBigLightPyramid = !m_showBigLightPyramid;
	AfxGetApp ()->WriteProfileInt (_T ("light"), _T ("showPyramid"), m_showBigLightPyramid);
	if (!m_animate)
	{
		Render ();
	}
}

void CTreeditorView::OnUpdateShowBigLightPyramid(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
	pCmdUI->SetCheck (m_showBigLightPyramid ? 1 : 0);
}

void CTreeditorView::OnShowBigWindPyramid() 
{
	// TODO: Add your command handler code here
	
	m_showBigWindPyramid = !m_showBigWindPyramid;
	AfxGetApp ()->WriteProfileInt (_T ("wind"), _T ("showPyramid"), m_showBigWindPyramid);
	if (!m_animate)
	{
		Render ();
	}
}

void CTreeditorView::OnUpdateShowBigWindPyramid(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
	pCmdUI->SetCheck (m_showBigWindPyramid ? 1 : 0);
}

void CTreeditorView::OnAutoDetail() 
{
	// TODO: Add your command handler code here
	
	m_autoDetail = !m_autoDetail;
	AfxGetApp ()->WriteProfileInt (_T ("editor"), _T ("autodetail"), m_autoDetail);

	if (m_autoDetail && !m_animate)
	{
		Render ();
	}
}

void CTreeditorView::OnUpdateAutoDetail(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
	pCmdUI->SetCheck (m_autoDetail ? 1 : 0);
}

void CTreeditorView::OnChangeSeed() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here

	if (_updateComponentsTime)
	{
		return;
	}

	int seed = 0;
	GET_INT_FROM_WND(seed, _componentBar, IDC_SEED)
	AfxGetApp ()->WriteProfileInt (_T ("tree"), _T ("seed"), seed);

	if (m_immediateResponse)
	{
		GetDocument ()->UpdateStamp (true, false);
		UpdateRender (INIT_TREE);
	}
}

void CTreeditorView::OnKillFocusSeed() 
{
	// TODO: Add your control notification handler code here
	
	if (_updateComponentsTime)
	{
		return;
	}

	int seed = 0;
	GET_INT_FROM_WND(seed, _componentBar, IDC_SEED)
	AfxGetApp ()->WriteProfileInt (_T ("tree"), _T ("seed"), seed);

	if (!m_immediateResponse)
	{
		GetDocument ()->UpdateStamp (true, false);
		UpdateRender (INIT_TREE);
	}
}

void CTreeditorView::OnDeltaPosSeedSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here

	_updateComponentsTime = true;

	int seed = 0;
	GET_INT_FROM_WND(seed, _componentBar, IDC_SEED)
	seed += pNMUpDown->iDelta;
	PUT_INT_IN_WND(seed, _componentBar, IDC_SEED)
	AfxGetApp ()->WriteProfileInt (_T ("tree"), _T ("seed"), seed);
	// SET_EDIT_FOCUS(_componentBar, IDC_SEED)

	GetDocument ()->UpdateStamp (true, false);
	UpdateRender (INIT_TREE);

	_updateComponentsTime = false;
	*pResult = 0;
}

void CTreeditorView::EqualizerLinearHookProc (const CCurveDlg *dlg, long param1, long param2, long param3)
{
	CTreeditorView *view = (CTreeditorView*) param1;
	CTreeditorDoc *doc = view->GetDocument();
	int v = 0;
	unsigned long cli;
	for (CTreeditorClass *cl = doc->GetFirstSelectedClass (&cli); cl != NULL; cl = doc->GetNextSelectedClass (&cli))
	{
		cl->params[param2].SetValue (dlg->GetMarkValue(v++));
	}
	view->UpdateProps ();
	view->UpdateSlider ();
	doc->UpdateStamp (true, false);
	view->UpdateRender (INIT_TREE);
}

void CTreeditorView::EqualizerQuadraticHookProc (const CCurveDlg *dlg, long param1, long param2, long param3)
{
	CTreeditorView *view = (CTreeditorView*) param1;
	CTreeditorDoc *doc = view->GetDocument();
	int v = 0;
	unsigned long cli;
	for (CTreeditorClass *cl = doc->GetFirstSelectedClass (&cli); cl != NULL; cl = doc->GetNextSelectedClass (&cli))
	{
		cl->params[param2].SetValue (dlg->GetMarkValue(++v));
	}
	view->UpdateProps ();
	view->UpdateSlider ();
	doc->UpdateStamp (true, false);
	view->UpdateRender (INIT_TREE);
}

void CTreeditorView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	// TODO: Add your specialized code here and/or call the base class

	switch (lHint)
	{
	case UH_UNDO:
		UpdateClasses ();
		UpdateRender (INIT_TREE);
		break;

	default:
		CView::OnUpdate(pSender, lHint, pHint);
		break;
	}
}

void CTreeditorView::OnEditUndo() 
{
	// TODO: Add your command handler code here
	
	CTreeditorDoc *doc = GetDocument ();
	if (doc->_undos.GetSize () < 1)
	{
		return;
	}

	doc->DoUndo ();
}

void CTreeditorView::OnUpdateEditUndo(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
	CTreeditorDoc *doc = GetDocument ();
	if (doc->_undos.GetSize () < 1)
	{
		pCmdUI->Enable (FALSE);
		return;
	}
}

void CTreeditorView::OnEditRedo() 
{
	// TODO: Add your command handler code here
	
	CTreeditorDoc *doc = GetDocument ();
	if (doc->_redos.GetSize () < 1)
	{
		return;
	}

	doc->DoRedo();
}

void CTreeditorView::OnUpdateEditRedo(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
	CTreeditorDoc *doc = GetDocument ();
	if (doc->_redos.GetSize () < 1)
	{
		pCmdUI->Enable (FALSE);
		return;
	}
}

void CTreeditorView::OnChangeGround() 
{
	// TODO: Add your command handler code here

	CString name = AfxGetApp()->GetProfileString(_T ("ground"), _T ("texture"), _T ("ground.bmp"));
	CFileDialog dlg (TRUE, NULL, name);
	if (dlg.DoModal() == IDOK)
	{
		name = dlg.GetPathName();
		AfxGetApp ()->WriteProfileString(_T ("ground"), _T ("texture"), name);

		UpdateRender (INIT_GROUND);
	}
}

void CTreeditorView::OnShowGround() 
{
	// TODO: Add your command handler code here
	
	m_showGround = !m_showGround;
	AfxGetApp ()->WriteProfileInt (_T ("ground"), _T ("show"), m_showGround);
	if (!m_animate)
	{
		Render ();
	}
}

void CTreeditorView::OnUpdateShowGround(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
	pCmdUI->SetCheck (m_showGround ? 1 : 0);
}

void CTreeditorView::OnEditPreferences() 
{
	// TODO: Add your command handler code here
	
	CPreferencesDlg dlg;
	if (dlg.DoModal () == IDOK)
	{
		UpdateClasses ();
		UpdateRender (INIT_TREE);
	}
}

void CTreeditorView::OnEqualizerLinear() 
{
	// TODO: Add your command handler code here
	
	CUiPropertiesCtrl &props = ((CMainFrame*) AfxGetMainWnd ())->_propertyBar._propCtrl;
	if (props.GetSelectedPropCount () == 1)
	{
		int posi = props.GetNextSelectedProp (-1);
		if (posi != -1)
		{
			int pi = props.GetPropData (posi);

			CStringArray names;
			CArray<float, float> values;
			CArray<int, int> keys;

			unsigned long cli;
			CTreeditorDoc *doc = GetDocument ();
			for (CTreeditorClass *cl = doc->GetFirstSelectedClass (&cli); cl != NULL; cl = doc->GetNextSelectedClass (&cli))
			{
				names.Add (cl->name);
				values.Add (_tcstod (cl->params[pi].GetValue (), NULL));
				keys.Add (1);
			}
			
			const STreeditorParamDesc *pd = NULL;
			if (pi < treeditorClassParam_numberOfTypes)
			{
				pd = &CTreeditorParam::classParams[pi];
			}
			else
			{
				return;
			}

			if ((pd->useWhat & USE_SLIDER) != 0)
			{
				CCurveDlg eq (pd->uiName, CCurveDlg::LINEAR_CURVE, false, pd->step, keys, values, names, pd->minValue, pd->maxValue, HORIZONTAL_Y, _T ("components"), pd->uiName);

				if (m_immediateResponse)
				{
					eq.SetHookProc (EqualizerLinearHookProc, (long) this, (long) pi, 0L);
				}
				int retkey = eq.DoModal ();
				if (eq.IsModified ())
				{
					if (retkey == IDOK)
					{
						CTreeditorDocUndo *undo = doc->OpenUndo();
						if (undo != NULL)
						{
							int v = 0;
							for (CTreeditorClass *cl = doc->GetFirstSelectedClass (&cli); cl != NULL; cl = doc->GetNextSelectedClass (&cli))
							{
								cl->params[pi].SetValue (eq.GetMarkValue(v++));
							}
							doc->SetModifiedFlag (TRUE);
							doc->UpdateStamp (true, false);
							UpdateProps ();
							UpdateSlider ();
							UpdateRender (INIT_TREE);
							doc->CloseUndo(undo);
						}
					}
					else if (m_immediateResponse)
					{
						int v = 0;
						for (CTreeditorClass *cl = doc->GetFirstSelectedClass (&cli); cl != NULL; cl = doc->GetNextSelectedClass (&cli))
						{
							cl->params[pi].SetValue (values[v++]);
						}
						UpdateProps ();
						UpdateSlider ();
						doc->UpdateStamp (true, false);
						UpdateRender (INIT_TREE);
					}
				}
			}
		}
	}
}

void CTreeditorView::OnUpdateEqualizerLinear(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
	CUiPropertiesCtrl &props = ((CMainFrame*) AfxGetMainWnd ())->_propertyBar._propCtrl;
	if (props.GetSelectedPropCount () == 1)
	{
		int posi = props.GetNextSelectedProp (-1);
		if (posi != -1)
		{
			int i = props.GetPropData (posi);
			if (i < treeditorClassParam_numberOfTypes && (CTreeditorParam::classParams[i].useWhat & USE_SLIDER) != 0)
			{
				pCmdUI->Enable (TRUE);
				return;
			}
		}
	}

	pCmdUI->Enable (FALSE);
}

void CTreeditorView::OnEqualizerQuadratic() 
{
	// TODO: Add your command handler code here
	
	CUiPropertiesCtrl &props = ((CMainFrame*) AfxGetMainWnd ())->_propertyBar._propCtrl;
	if (props.GetSelectedPropCount () == 1)
	{
		int posi = props.GetNextSelectedProp (-1);
		if (posi != NULL)
		{
			int pi = props.GetPropData (posi);

			CStringArray names;
			CArray<float, float> values;
			CArray<int, int> keys;

			unsigned long cli;
			CTreeditorDoc *doc = GetDocument ();
			values.Add (0.0f);
			for (CTreeditorClass *cl = doc->GetFirstSelectedClass (&cli); cl != NULL; cl = doc->GetNextSelectedClass (&cli))
			{
				names.Add (cl->name);
				values.Add (_tcstod (cl->params[pi].GetValue (), NULL));
				keys.Add (1);
			}
			
			const STreeditorParamDesc *pd = NULL;
			if (pi < treeditorClassParam_numberOfTypes)
			{
				pd = &CTreeditorParam::classParams[pi];
			}
			else
			{
				return;
			}
			
			if ((pd->useWhat & USE_SLIDER) != 0)
			{
				CCurveDlg eq (pd->uiName, CCurveDlg::EX_QUADRATIC_CURVE, false, pd->step, keys, values, names, pd->minValue, pd->maxValue, HORIZONTAL_Y, _T ("components"), pd->uiName);
				if (m_immediateResponse)
				{
					eq.SetHookProc (EqualizerQuadraticHookProc, (long) this, (long) pi, 0L);
				}
				int retkey = eq.DoModal ();
				if (eq.IsModified ())
				{
					if (retkey == IDOK)
					{
						CTreeditorDocUndo *undo = doc->OpenUndo();
						if (undo != NULL)
						{
							int v = 0;
							for (CTreeditorClass *cl = doc->GetFirstSelectedClass (&cli); cl != NULL; cl = doc->GetNextSelectedClass (&cli))
							{
								cl->params[pi].SetValue (eq.GetMarkValue(++v));
							}
							doc->SetModifiedFlag (TRUE);
							doc->UpdateStamp (true, false);
							UpdateProps ();
							UpdateSlider ();
							UpdateRender (INIT_TREE);
							doc->CloseUndo(undo);
						}
					}
					else if (m_immediateResponse)
					{
						int v = 0;
						for (CTreeditorClass *cl = doc->GetFirstSelectedClass (&cli); cl != NULL; cl = doc->GetNextSelectedClass (&cli))
						{
							cl->params[pi].SetValue (values[v++]);
						}
						UpdateProps ();
						UpdateSlider ();
						doc->UpdateStamp (true, false);
						UpdateRender (INIT_TREE);
					}
				}
			}
		}
	}
}

void CTreeditorView::OnUpdateEqualizerQuadratic(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
	CUiPropertiesCtrl &props = ((CMainFrame*) AfxGetMainWnd ())->_propertyBar._propCtrl;
	if (props.GetSelectedPropCount () == 1)
	{
		int posi = props.GetNextSelectedProp (-1);
		if (posi != -1)
		{
			int i = props.GetPropData (posi);
			if (i < treeditorClassParam_numberOfTypes && (CTreeditorParam::classParams[i].useWhat & USE_SLIDER) != 0)
			{
				pCmdUI->Enable (TRUE);
				return;
			}
		}
	}

	pCmdUI->Enable (FALSE);
}

void CTreeditorView::ChangeParam(bool recUndo, CTreeditorDocUndo *undo, int paramIdx, int itemIdx, CString &value, bool dirty, bool slider, bool sliderUpdate, bool valEditUpdate)
{
	if (!_updatePropertiesTime)
	{
		_updatePropertiesTime = true;
		if (paramIdx < treeditorClassParam_numberOfTypes)
		{
			ChangeParamInTree (recUndo, undo, paramIdx, itemIdx, value, dirty, slider, sliderUpdate, valEditUpdate);
		}
		else
		{
			ChangeParamInScript(paramIdx, itemIdx, value, slider, sliderUpdate, valEditUpdate);
		}
		_updatePropertiesTime = false;
	}
}

void CTreeditorView::ChangeParamInTree(bool recUndo, CTreeditorDocUndo *undo, int paramIdx, int itemIdx, CString &value, bool dirty, bool slider, bool sliderUpdate, bool valEditUpdate)
{
	CTreeditorDoc *doc = GetDocument ();

	if (recUndo && undo == NULL)
	{
		undo = doc->OpenUndo();
		if (undo == NULL)
		{
			return;
		}
	}

	doc->SetParamInSelection (paramIdx, value);
	if (dirty)
	{
		doc->UpdateStamp (true, paramIdx == treeditorClassParam_script);
		doc->SetModifiedFlag (TRUE);
	}
	doc->GetParamInSelection (paramIdx, value);

	CUiPropertiesCtrl &props = ((CMainFrame*) AfxGetMainWnd ())->_propertyBar._propCtrl;
	props.SetPropValue (itemIdx, value, valEditUpdate);
	if (!slider)
	{
		doc->UpdateStamp (true, paramIdx == treeditorClassParam_script);
		UpdateAttrs ();
		UpdateSlider ();
		UpdateRender (INIT_TREE);
	}
	else if (sliderUpdate)
	{
		doc->UpdateStamp (true, paramIdx == treeditorClassParam_script);
		UpdateRender (INIT_TREE);
	}

	if (undo != NULL)
	{
		doc->CloseUndo(undo);
	}

	if (paramIdx == treeditorClassParam_script)
	{
		UpdateScriptProps ();
	}
}

void CTreeditorView::ChangeParamInScript(int paramIdx, int itemIdx, CString &value, bool slider, bool sliderUpdate, bool valEditUpdate)
{
	if (paramIdx < treeditorClassParam_numberOfTypes)
	{
		return;
	}

	paramIdx -= treeditorClassParam_numberOfTypes;
	CTreeditorDoc *doc = GetDocument ();

	CString scriptFile;
	doc->GetParamInSelection(treeditorClassParam_script, scriptFile);
	if (scriptFile.IsEmpty ())
	{
		return;
	}
	scriptFile = scriptFile.Mid (1, scriptFile.GetLength () - 2);

	CFile f;
	if (f.Open(scriptFile, CFile::modeRead, NULL))
	{
		CString str;
		int fileLen = f.GetLength();
		int strLen = fileLen + sizeof (TCHAR) - 1 / sizeof (TCHAR);
		LPTSTR p = str.GetBuffer (strLen);
		bool read = f.Read(p, fileLen) == fileLen;
		str.ReleaseBuffer();
		f.Close();
		
		if (read)
		{
			CUiPropertiesCtrl &props = ((CMainFrame*) AfxGetMainWnd ())->_propertyBar._propCtrl;
			CArray<STreeditorParamDesc*, STreeditorParamDesc*> sps;
			CScriptParser sp (str, &sps);
			STreeditorScriptParamDesc desc;
			desc.group.colorIndex = 0;
			
			int i = 0;
			while (sp.GetNextParam(&desc))
			{
				STreeditorParamDesc *pd = new STreeditorParamDesc ();
				STreeditorParamDesc::copy (*pd, desc);
				sps.Add (pd);

				if (i == paramIdx)
				{
					CString out = str.Left (desc._endOfPrefix - (LPCTSTR) str);
					out += _T (' ');
					switch (desc.type)
					{
					case treeditorLexArray:
						out += _T ('[');
						out += value;
						out += _T (']');
						break;

					case treeditorLexString:
						out += _T ('"');
						out += value;
						out += _T ('"');
						break;

					case treeditorLexId:
						if (desc.isChildClassName)
						{
							out += _T ('"');
							out += value;
							out += _T ('"');
						}
						else
						{
							out += value;
						}
						break;

					default:
						out += value;
						break;
					}
					out += desc._startOfPostfix;

					if (f.Open(scriptFile, CFile::modeCreate | CFile::modeWrite, NULL))
					{
						f.Write (out, out.GetLength() * sizeof (TCHAR));
						f.Close ();
					}

					strcpy (sps[paramIdx]->defValue, value);
					strcpy (doc->GetSelectedClass ()->_scriptParams[paramIdx]->defValue, value);
					props.SetPropValue (itemIdx, value, valEditUpdate);
					if (!slider)
					{
						doc->UpdateStamp (false, true);
						// UpdateProps ();
						// UpdateClassType ();
						UpdateAttrs ();
						UpdateSlider ();
						UpdateRender (INIT_TREE);
					}
					else if (sliderUpdate)
					{
						doc->UpdateStamp (false, true);
						UpdateRender (INIT_TREE);
					}
				}
				++i;
			}

			doc->UpdateScriptParams ();

			int n = props.GetPropCount ();
			for (int i = 0; i < n; ++i)
			{
				int idx = props.GetPropData (i);
				if (idx >= treeditorClassParam_numberOfTypes)
				{
					props.SetEnabled (i, sps[idx - treeditorClassParam_numberOfTypes]->enabled);
				}
			}

			n = sps.GetSize ();
			for (int i = 0; i < n; ++i)
			{
				delete sps[i];
			}
			sps.RemoveAll ();
		}
	}
}

void CTreeditorView::TreePreviewRefresh (bool paramChange, bool updateScripts)
{
	CTreeditorDoc *doc = GetDocument ();
	doc->UpdateStamp (paramChange, updateScripts);
	UpdateProps ();
	UpdateClassType ();
	UpdateAttrs ();
	UpdateSlider ();
	if (doc->NeedToUpdatePreview ())
	{
		((CMainFrame*) AfxGetMainWnd ())->SendMessage (WM_COMMAND, ID_EDIT_CLEARDEBUG, 0);
		UpdateRender (INIT_TREE);
	}
}

void CTreeditorView::OnViewRefresh() 
{
	// TODO: Add your command handler code here

	TreePreviewRefresh (true, true);
}

void CTreeditorView::SetPropValue (CUiPropertySubview *props, int idx, long paramIdx, LPCTSTR value) 
{
	if (value != NULL)
	{
		props->SetPropValue (idx, value, false);

		CTreeditorDoc *doc = GetDocument ();
		CTreeditorClass *c = doc->GetSelectedClass ();
		if (c != NULL)
		{
			const STreeditorParamDesc *pd = paramIdx < treeditorClassParam_numberOfTypes ? &CTreeditorParam::classParams[paramIdx] :
				c->_scriptParams[paramIdx - treeditorClassParam_numberOfTypes];
			CString str;
			switch (pd->type)
			{
			case treeditorLexArray:
				str = value;
				break;
				
			default:
				{
					CTreeditorStringInput in (value);
					CTreeditorLex lex (&in, NULL, 0);
					STreeditorTokenDesc token;
					lex.GetNextToken (&token);
					if (pd->type == token.type ||
						(pd->type == treeditorLexFixedFloat && token.type == treeditorLexLong))
					{
						switch (token.type)
						{
						case treeditorLexLong:
							if (token.number < pd->minValue)
							{
								token.number = pd->minValue;
							}
							else if (token.number > pd->maxValue)
							{
								token.number = pd->maxValue;
							}
							str.Format ("%d", token.number);
							break;
							
						case treeditorLexFixedFloat:
							if (token.fnumber < pd->minValue)
							{
								token.fnumber = pd->minValue;
							}
							else if (token.fnumber > pd->maxValue)
							{
								token.fnumber = pd->maxValue;
							}
							str.Format ("%g", token.fnumber);
							break;
							
						case treeditorLexString:
							str.Format ("\"%s\"", token.string);
							break;
							
						default: //case treeditorLexId:
							str = token.string;
							break;
						}
					}
				}
				break;
			}
			ChangeParam (true, NULL, paramIdx, idx, str, true, false, false, false);
		}
	}
}

LPCTSTR CTreeditorView::GetPropValue (CUiPropertySubview *props, int idx, long data) const
{ 
	return props->GetPropValue (idx);
}

bool CTreeditorView::SetPropSelect (CUiPropertySubview *props, int idx, long data, bool sel)
{
	CMainFrame *mf = (CMainFrame*) AfxGetMainWnd ();
	if (mf->_ctrlDown | mf->_shiftDown)
	{
		props->SelectProp (idx, sel);
		UpdateSlider ();
		return sel;
	}
	else
	{
		props->SelectOneProp (idx);
		UpdateSlider ();
		return true;
	}
}

void CTreeditorView::OnEditSelAllProp() 
{
	// TODO: Add your command handler code here
	
	CUiPropertiesCtrl &props = ((CMainFrame*) AfxGetMainWnd ())->_propertyBar._propCtrl;
	props.SelectAllProps ();
	UpdateSlider ();
}

void CTreeditorView::OnEditDeselAllProps() 
{
	// TODO: Add your command handler code here
	
	CUiPropertiesCtrl &props = ((CMainFrame*) AfxGetMainWnd ())->_propertyBar._propCtrl;
	props.DeselectAllProps ();
	UpdateSlider ();
}

void CTreeditorView::OnPropEdit (CUiPropertySubview *props, int item, long parId) 
{ 
	const STreeditorParamDesc *par = NULL;
	if (parId < treeditorClassParam_numberOfTypes)
	{
		par = &CTreeditorParam::classParams[parId];
	}
	else
	{
		CTreeditorDoc *doc = GetDocument ();
		CTreeditorClass *c = doc->GetSelectedClass ();
		if (c == NULL)
		{
			return;
		}
		par = c->_scriptParams[parId - treeditorClassParam_numberOfTypes];
	}
	
	if ((par->useWhat & USE_COLOR_DLG) != 0)
	{
		CColorDlg dlg (props->GetPropValue (item));
		if (dlg.DoModal () == IDOK)
		{
			CString str;
			str.Format (_T ("%g,%g,%g"), dlg._redFloat, dlg._greenFloat, dlg._blueFloat);
			ChangeParam (true, NULL, parId, item, str, true, false, false, true);
		}
	}
	else if ((par->useWhat & USE_LINEAR_CURVE_DLG) != 0)
	{
		CArray<float, float> vals;
		ParseArrayOfFloats (props->GetPropValue (item), vals);
		int n = vals.GetSize ();
		if (n > 0)
		{
			CArray<int, int> keys;
			CStringArray names;
			for (int i = 0; i < n; ++i)
			{
				keys.Add (1);
				names.Add (_T (""));
			}
			CString title (_T ("Linear Curve - "));
			title += par->uiName;
			CCurveDlg dlg (title, CCurveDlg::LINEAR_CURVE, false, par->step, keys, vals, names, par->minValue, par->maxValue, par->horizontalX, par->xDescr, par->yDescr);
			if (m_immediateResponse)
			{
				dlg.SetHookProc (CurveDlgLinearHookProc, (long) this, (long) parId, (long) item);
			}
			CTreeditorDoc *doc = GetDocument ();
			int retkey = dlg.DoModal ();
			if (dlg.IsModified ())
			{
				if (retkey == IDOK)
				{
					CTreeditorDocUndo *undo = doc->OpenUndo();
					if (undo != NULL)
					{
						CString str;
						dlg.GetLinearCurveString (str);
						ChangeParam (true, undo, parId, item, str, true, false, false, true);
					}
				}
				else if (m_immediateResponse)
				{
					CString str;
					str.Format (_T ("%g"), vals[0]);
					for (int i = 1; i < n; ++i)
					{
						CString tmp;
						tmp.Format (_T (", %g"), vals[i]);
						str += tmp;
					}
					
					ChangeParam (false, NULL, parId, item, str, false, false, false, true);
				}
			}
		}
	}
	else if ((par->useWhat & USE_LINEAR_CURVE_WITH_X_DLG) != 0)
	{
		CArray<float, float> vals;
		ParseArrayOfFloats (props->GetPropValue (item), vals);
		int n = vals.GetSize ();
		if (n > 0)
		{
			CArray<int, int> keys;
			CStringArray names;
			for (int i = 0; i < n; ++i)
			{
				keys.Add (1);
				names.Add (_T (""));
			}
			CString title (_T ("Linear Curve with X - "));
			title += par->uiName;
			CCurveDlg dlg (title, CCurveDlg::LINEAR_CURVE_WITH_X, false, par->step, keys, vals, names, par->minValue, par->maxValue, par->horizontalX, par->xDescr, par->yDescr);
			if (m_immediateResponse)
			{
				dlg.SetHookProc (CurveDlgLinearWithXHookProc, (long) this, (long) parId, (long) item);
			}
			CTreeditorDoc *doc = GetDocument ();
			int retkey = dlg.DoModal ();
			if (dlg.IsModified ())
			{
				if (retkey == IDOK)
				{
					CTreeditorDocUndo *undo = doc->OpenUndo();
					if (undo != NULL)
					{
						CString str;
						dlg.GetLinearCurveWithXString (str);
						ChangeParam (true, undo, parId, item, str, true, false, false, true);
					}
				}
				else if (m_immediateResponse)
				{
					CString str;
					str.Format (_T ("%g"), vals[0]);
					for (int i = 1; i < n; ++i)
					{
						CString tmp;
						tmp.Format (_T (", %g"), vals[i]);
						str += tmp;
					}
					
					ChangeParam (false, NULL, parId, item, str, false, false, false, true);
				}
			}
		}
	}
	else if ((par->useWhat & USE_LINEAR_VAR_CURVE_DLG) != 0)
	{
		CArray<float, float> vals;
		ParseArrayOfFloats (props->GetPropValue (item), vals);
		int n = vals.GetSize ();
		if (n > 0)
		{
			CArray<int, int> keys;
			CStringArray names;
			for (int i = 0; i < n; ++i)
			{
				keys.Add (1);
				names.Add(_T (""));
			}
			CString title (_T ("Linear Variable Curve - "));
			title += par->uiName;
			CCurveDlg dlg (title, CCurveDlg::LINEAR_CURVE, true, par->step, keys, vals, names, par->minValue, par->maxValue, par->horizontalX, par->xDescr, par->yDescr);
			if (m_immediateResponse)
			{
				dlg.SetHookProc (CurveDlgLinearHookProc, (long) this, (long) parId, (long) item);
			}
			CTreeditorDoc *doc = GetDocument ();
			int retkey = dlg.DoModal ();
			if (dlg.IsModified ())
			{
				if (retkey == IDOK)
				{
					CTreeditorDocUndo *undo = doc->OpenUndo();
					if (undo != NULL)
					{
						CString str;
						dlg.GetLinearCurveString (str);
						ChangeParam (true, undo, parId, item, str, true, false, false, true);
					}
				}
				else if (m_immediateResponse)
				{
					CString str;
					str.Format (_T ("%g"), vals[0]);
					for (int i = 1; i < n; ++i)
					{
						CString tmp;
						tmp.Format (_T (", %g"), vals[i]);
						str += tmp;
					}
					
					ChangeParam(false, NULL, parId, item, str, false, false, false, true);
				}
			}
		}
	}
	else if ((par->useWhat & USE_LINEAR_VAR_CURVE_WITH_X_DLG) != 0)
	{
		CArray<float, float> vals;
		ParseArrayOfFloats (props->GetPropValue (item), vals);
		int n = vals.GetSize ();
		if (n > 0)
		{
			CArray<int, int> keys;
			CStringArray names;
			for (int i = 0; i < n; ++i)
			{
				keys.Add (1);
				names.Add(_T (""));
			}
			CString title (_T ("Linear Variable Curve with X - "));
			title += par->uiName;
			CCurveDlg dlg (title, CCurveDlg::LINEAR_CURVE_WITH_X, true, par->step, keys, vals, names, par->minValue, par->maxValue, par->horizontalX, par->xDescr, par->yDescr);
			if (m_immediateResponse)
			{
				dlg.SetHookProc (CurveDlgLinearWithXHookProc, (long) this, (long) parId, (long) item);
			}
			CTreeditorDoc *doc = GetDocument ();
			int retkey = dlg.DoModal ();
			if (dlg.IsModified ())
			{
				if (retkey == IDOK)
				{
					CTreeditorDocUndo *undo = doc->OpenUndo();
					if (undo != NULL)
					{
						CString str;
						dlg.GetLinearCurveWithXString (str);
						ChangeParam (true, undo, parId, item, str, true, false, false, true);
					}
				}
				else if (m_immediateResponse)
				{
					CString str;
					str.Format (_T ("%g"), vals[0]);
					for (int i = 1; i < n; ++i)
					{
						CString tmp;
						tmp.Format (_T (", %g"), vals[i]);
						str += tmp;
					}
					
					ChangeParam(false, NULL, parId, item, str, false, false, false, true);
				}
			}
		}
	}
	else if ((par->useWhat & USE_QUADRATIC_VAR_CURVE_DLG) != 0)
	{
		CArray<float, float> vals;
		ParseArrayOfFloats (props->GetPropValue (item), vals);
		int n = vals.GetSize ();
		if (n > 0)
		{
			CArray<int, int> keys;
			CStringArray names;
			for (int i = 0; i < n; ++i)
			{
				keys.Add (1);
				names.Add(_T (""));
			}
			CString title (_T ("Quadratic Variable Curve - "));
			title += par->uiName;
			CCurveDlg dlg (title, CCurveDlg::QUADRATIC_CURVE, true, par->step, keys, vals, names, par->minValue, par->maxValue, par->horizontalX, par->xDescr, par->yDescr);
			if (m_immediateResponse)
			{
				dlg.SetHookProc (CurveDlgQuadraticHookProc, (long) this, (long) parId, (long) item);
			}
			CTreeditorDoc *doc = GetDocument ();
			int retkey = dlg.DoModal ();
			if (dlg.IsModified ())
			{
				if (retkey == IDOK)
				{
					CTreeditorDocUndo *undo = doc->OpenUndo();
					if (undo != NULL)
					{
						CString str;
						dlg.GetQuadraticCurveString (str);
						ChangeParam (true, undo, parId, item, str, true, false, false, true);
					}
				}
				else if (m_immediateResponse)
				{
					CString str;
					str.Format (_T ("%g"), vals[0]);
					for (int i = 1; i < n; ++i)
					{
						CString tmp;
						tmp.Format (_T (", %g"), vals[i]);
						str += tmp;
					}
					
					ChangeParam (false, NULL, parId, item, str, false, false, false, true);
				}
			}
		}
	}
	else if ((par->useWhat & USE_QUADRATIC_VAR_CURVE_WITH_X_DLG) != 0)
	{
		CArray<float, float> vals;
		ParseArrayOfFloats (props->GetPropValue (item), vals);
		int n = vals.GetSize ();
		if (n > 0)
		{
			CArray<int, int> keys;
			CStringArray names;
			for (int i = 0; i < n; ++i)
			{
				keys.Add (1);
				names.Add(_T (""));
			}
			CString title (_T ("Quadratic Variable Curve with X - "));
			title += par->uiName;
			CCurveDlg dlg (title, CCurveDlg::QUADRATIC_CURVE_WITH_X, true, par->step, keys, vals, names, par->minValue, par->maxValue, par->horizontalX, par->xDescr, par->yDescr);
			if (m_immediateResponse)
			{
				dlg.SetHookProc (CurveDlgQuadraticWithXHookProc, (long) this, (long) parId, (long) item);
			}
			CTreeditorDoc *doc = GetDocument ();
			int retkey = dlg.DoModal ();
			if (dlg.IsModified ())
			{
				if (retkey == IDOK)
				{
					CTreeditorDocUndo *undo = doc->OpenUndo();
					if (undo != NULL)
					{
						CString str;
						dlg.GetQuadraticCurveWithXString (str);
						ChangeParam (true, undo, parId, item, str, true, false, false, true);
					}
				}
				else if (m_immediateResponse)
				{
					CString str;
					str.Format (_T ("%g"), vals[0]);
					for (int i = 1; i < n; ++i)
					{
						CString tmp;
						tmp.Format (_T (", %g"), vals[i]);
						str += tmp;
					}
					
					ChangeParam (false, NULL, parId, item, str, false, false, false, true);
				}
			}
		}
	}
	else if ((par->useWhat & USE_QUADRATIC_CURVE_DLG) != 0)
	{
		CArray<float, float> vals;
		ParseArrayOfFloats (props->GetPropValue (item), vals);
		int n = vals.GetSize ();
		if (n > 0)
		{
			CArray<int, int> keys;
			CStringArray names;
			for (int i = 0; i < n; ++i)
			{
				keys.Add (1);
				names.Add(_T (""));
			}
			CString title (_T ("Quadratic Curve - "));
			title += par->uiName;
			CCurveDlg dlg (title, CCurveDlg::QUADRATIC_CURVE, false, par->step, keys, vals, names, par->minValue, par->maxValue, par->horizontalX, par->xDescr, par->yDescr);
			if (m_immediateResponse)
			{
				dlg.SetHookProc (CurveDlgQuadraticHookProc, (long) this, (long) parId, (long) item);
			}
			CTreeditorDoc *doc = GetDocument ();
			int retkey = dlg.DoModal ();
			if (dlg.IsModified ())
			{
				if (retkey == IDOK)
				{
					CTreeditorDocUndo *undo = doc->OpenUndo();
					if (undo != NULL)
					{
						CString str;
						dlg.GetQuadraticCurveString (str);
						ChangeParam (true, undo, parId, item, str, true, false, false, true);
					}
				}
				else if (m_immediateResponse)
				{
					CString str;
					str.Format (_T ("%g"), vals[0]);
					for (int i = 1; i < n; ++i)
					{
						CString tmp;
						tmp.Format (_T (", %g"), vals[i]);
						str += tmp;
					}
					
					ChangeParam(false, NULL, parId, item, str, false, false, false, true);
				}
			}
		}
	}
	else if ((par->useWhat & USE_QUADRATIC_CURVE_WITH_X_DLG) != 0)
	{
		CArray<float, float> vals;
		ParseArrayOfFloats (props->GetPropValue (item), vals);
		int n = vals.GetSize ();
		if (n > 0)
		{
			CArray<int, int> keys;
			CStringArray names;
			for (int i = 0; i < n; ++i)
			{
				keys.Add (1);
				names.Add(_T (""));
			}
			CString title (_T ("Quadratic Curve with X - "));
			title += par->uiName;
			CCurveDlg dlg (title, CCurveDlg::QUADRATIC_CURVE_WITH_X, false, par->step, keys, vals, names, par->minValue, par->maxValue, par->horizontalX, par->xDescr, par->yDescr);
			if (m_immediateResponse)
			{
				dlg.SetHookProc (CurveDlgQuadraticWithXHookProc, (long) this, (long) parId, (long) item);
			}
			CTreeditorDoc *doc = GetDocument ();
			int retkey = dlg.DoModal ();
			if (dlg.IsModified ())
			{
				if (retkey == IDOK)
				{
					CTreeditorDocUndo *undo = doc->OpenUndo();
					if (undo != NULL)
					{
						CString str;
						dlg.GetQuadraticCurveWithXString (str);
						ChangeParam (true, undo, parId, item, str, true, false, false, true);
					}
				}
				else if (m_immediateResponse)
				{
					CString str;
					str.Format (_T ("%g"), vals[0]);
					for (int i = 1; i < n; ++i)
					{
						CString tmp;
						tmp.Format (_T (", %g"), vals[i]);
						str += tmp;
					}
					
					ChangeParam(false, NULL, parId, item, str, false, false, false, true);
				}
			}
		}
	}
	else if ((par->useWhat & USE_EX_QUADRATIC_CURVE_DLG) != 0)
	{
		CArray<float, float> vals;
		ParseArrayOfFloats (props->GetPropValue (item), vals);
		int n = vals.GetSize ();
		if (n > 0)
		{
			CArray<int, int> keys;
			CStringArray names;
			for (int i = 0; i < n; ++i)
			{
				keys.Add (2);
				names.Add(_T (""));
			}
			CString title (_T ("ExQuadratic Curve - "));
			title += par->uiName;
			CCurveDlg dlg (title, CCurveDlg::EX_QUADRATIC_CURVE, false, par->step, keys, vals, names, par->minValue, par->maxValue, par->horizontalX, par->xDescr, par->yDescr);
			if (m_immediateResponse)
			{
				dlg.SetHookProc (CurveDlgExQuadraticHookProc, (long) this, (long) parId, (long) item);
			}
			CTreeditorDoc *doc = GetDocument ();
			int retkey = dlg.DoModal ();
			if (dlg.IsModified ())
			{
				if (retkey == IDOK)
				{
					CTreeditorDocUndo *undo = doc->OpenUndo();
					if (undo != NULL)
					{
						CString str;
						dlg.GetExQuadraticCurveString (str);
						ChangeParam (true, undo, parId, item, str, true, false, false, true);
					}
				}
				else if (m_immediateResponse)
				{
					CString str;
					str.Format (_T ("%g"), vals[0]);
					for (int i = 1; i < n; ++i)
					{
						CString tmp;
						tmp.Format (_T (", %g"), vals[i]);
						str += tmp;
					}
					
					ChangeParam (false, NULL, parId, item, str, false, false, false, true);
				}
			}
		}
	}
	else if ((par->useWhat & USE_EX_QUADRATIC_CURVE_WITH_X_DLG) != 0)
	{
		CArray<float, float> vals;
		ParseArrayOfFloats (props->GetPropValue (item), vals);
		int n = vals.GetSize ();
		if (n > 0)
		{
			CArray<int, int> keys;
			CStringArray names;
			for (int i = 0; i < n; ++i)
			{
				keys.Add (2);
				names.Add(_T (""));
			}
			CString title (_T ("ExQuadratic Curve with X - "));
			title += par->uiName;
			CCurveDlg dlg (title, CCurveDlg::EX_QUADRATIC_CURVE_WITH_X, false, par->step, keys, vals, names, par->minValue, par->maxValue, par->horizontalX, par->xDescr, par->yDescr);
			if (m_immediateResponse)
			{
				dlg.SetHookProc (CurveDlgExQuadraticWithXHookProc, (long) this, (long) parId, (long) item);
			}
			CTreeditorDoc *doc = GetDocument ();
			int retkey = dlg.DoModal ();
			if (dlg.IsModified ())
			{
				if (retkey == IDOK)
				{
					CTreeditorDocUndo *undo = doc->OpenUndo();
					if (undo != NULL)
					{
						CString str;
						dlg.GetExQuadraticCurveWithXString (str);
						ChangeParam (true, undo, parId, item, str, true, false, false, true);
					}
				}
				else if (m_immediateResponse)
				{
					CString str;
					str.Format (_T ("%g"), vals[0]);
					for (int i = 1; i < n; ++i)
					{
						CString tmp;
						tmp.Format (_T (", %g"), vals[i]);
						str += tmp;
					}
					
					ChangeParam (false, NULL, parId, item, str, false, false, false, true);
				}
			}
		}
	}
	else if ((par->useWhat & USE_EX_QUADRATIC_VAR_CURVE_DLG) != 0)
	{
		CArray<float, float> vals;
		ParseArrayOfFloats (props->GetPropValue (item), vals);
		int n = vals.GetSize ();
		if (n > 0)
		{
			CArray<int, int> keys;
			CStringArray names;
			for (int i = 0; i < (n >> 1); ++i)
			{
				keys.Add (2);
				names.Add(_T (""));
			}
			CString title (_T ("ExQuadratic Variable Curve - "));
			title += par->uiName;
			CCurveDlg dlg (title, CCurveDlg::EX_QUADRATIC_CURVE, true, par->step, keys, vals, names, par->minValue, par->maxValue, par->horizontalX, par->xDescr, par->yDescr);
			if (m_immediateResponse)
			{
				dlg.SetHookProc (CurveDlgExQuadraticHookProc, (long) this, (long) parId, (long) item);
			}
			CTreeditorDoc *doc = GetDocument ();
			int retkey = dlg.DoModal ();
			if (dlg.IsModified ())
			{
				if (retkey == IDOK)
				{
					CTreeditorDocUndo *undo = doc->OpenUndo();
					if (undo != NULL)
					{
						CString str;
						dlg.GetExQuadraticCurveString (str);
						ChangeParam (true, undo, parId, item, str, true, false, false, true);
					}
				}
				else if (m_immediateResponse)
				{
					CString str;
					str.Format (_T ("%g"), vals[0]);
					for (int i = 1; i < n; ++i)
					{
						CString tmp;
						tmp.Format (_T (", %g"), vals[i]);
						str += tmp;
					}
					
					ChangeParam (false, NULL, parId, item, str, false, false, false, true);
				}
			}
		}
	}
	else if ((par->useWhat & USE_EX_QUADRATIC_VAR_CURVE_WITH_X_DLG) != 0)
	{
		CArray<float, float> vals;
		ParseArrayOfFloats (props->GetPropValue (item), vals);
		int n = vals.GetSize ();
		if (n > 0)
		{
			CArray<int, int> keys;
			CStringArray names;
			for (int i = 0; i < (n >> 1); ++i)
			{
				keys.Add (2);
				names.Add(_T (""));
			}
			CString title (_T ("ExQuadratic Variable Curve with X - "));
			title += par->uiName;
			CCurveDlg dlg (title, CCurveDlg::EX_QUADRATIC_CURVE_WITH_X, true, par->step, keys, vals, names, par->minValue, par->maxValue, par->horizontalX, par->xDescr, par->yDescr);
			if (m_immediateResponse)
			{
				dlg.SetHookProc (CurveDlgExQuadraticWithXHookProc, (long) this, (long) parId, (long) item);
			}
			CTreeditorDoc *doc = GetDocument ();
			int retkey = dlg.DoModal ();
			if (dlg.IsModified ())
			{
				if (retkey == IDOK)
				{
					CTreeditorDocUndo *undo = doc->OpenUndo();
					if (undo != NULL)
					{
						CString str;
						dlg.GetExQuadraticCurveWithXString (str);
						ChangeParam (true, undo, parId, item, str, true, false, false, true);
					}
				}
				else if (m_immediateResponse)
				{
					CString str;
					str.Format (_T ("%g"), vals[0]);
					for (int i = 1; i < n; ++i)
					{
						CString tmp;
						tmp.Format (_T (", %g"), vals[i]);
						str += tmp;
					}
					
					ChangeParam (false, NULL, parId, item, str, false, false, false, true);
				}
			}
		}
	}
	else if ((par->useWhat & USE_OPENFILE_DLG) != 0)
	{
		CString itemfile = props->GetPropValue(item);
		itemfile = itemfile.Mid (1, itemfile.GetLength () - 2);
		
		int extidx = itemfile.ReverseFind(_T ('.'));
		CString ext = itemfile.Mid (extidx);
		CString ed = AfxGetApp ()->GetProfileString(_T ("externalEditors"), ext, NULL);

		CString dir;
		LPTSTR dirbuf = dir.GetBuffer (_MAX_PATH);
		dir.ReleaseBuffer (GetCurrentDirectory(_MAX_PATH, dirbuf));

		if (itemfile[1] != _T (':') && (itemfile[0] != _T ('\\') || itemfile[1] != _T ('\\')))
		{
			itemfile = dir + _T ('\\') + itemfile;
		}

		if (!ed.IsEmpty())
		{
			CString cmdline;
			cmdline.Format(_T ("\"%s\" \"%s\""), ed, itemfile);
			
			STARTUPINFO stInfo;
			memset (&stInfo, 0, sizeof (stInfo));
			stInfo.cb = sizeof(stInfo);
			
			PROCESS_INFORMATION prInfo;
			memset (&prInfo, 0, sizeof (prInfo));
			
			if (::CreateProcess (NULL, cmdline.GetBuffer (cmdline.GetLength () + 1), NULL, NULL,
				FALSE, CREATE_DEFAULT_ERROR_MODE | NORMAL_PRIORITY_CLASS,
				NULL, NULL, &stInfo, &prInfo))
			{
				::CloseHandle (prInfo.hThread);
				::CloseHandle (prInfo.hProcess);
			}
			cmdline.ReleaseBuffer ();
		}
		else
		{
			ShellExecute(NULL, _T ("open"), itemfile, _T (""), dir, SW_SHOWNORMAL);
		}
	}
	else if ((par->useWhat & USE_TEXT_EDITOR) != 0)
	{
		CString str (props->GetPropValue (item));
		str = str.Mid (1, str.GetLength () - 2);
		str.Replace (_T ("\"\""), _T ("\""));
		
		USES_CONVERSION;
		const char *astr = T2CA (str);
		RString script (astr);
		RString text = ConvertScript2Text (script);
		LPCTSTR ttext = A2CT (text);
		
		CTextEditor ted (ttext);
		if (ted.DoModal() == IDOK)
		{
			text = T2CA (ted._text);
			script = ConvertText2Script (text);
			astr = script;
			str = A2CT(astr);
			
			str.Replace (_T ("\""), _T ("\"\""));
			str = _T ('"') + str + _T ('"');
			
			ChangeParam (true, NULL, parId, item, str, true, false, false, true);
		}
	}
	else if ((par->useWhat & USE_PAIR_DLG) != 0)
	{
		CArray<float, float> vals;
		ParseArrayOfFloats (props->GetPropValue (item), vals);
		CPairDlg dlg (par->uiName, par->pairValueLabel, par->pairIntervalLabel, par->minValue, par->maxValue, par->step, vals);
		if (dlg.DoModal () == IDOK)
		{
			CString str;
			str.Format(_T("%g,%g"), dlg._value, dlg._interval);
			ChangeParam (true, NULL, parId, item, str, true, false, false, true);
		}
	}
}

void CTreeditorView::OnPropSelect (CUiPropertySubview *props, int item, long parId) 
{ 
	const STreeditorParamDesc *par = NULL;
	if (parId < treeditorClassParam_numberOfTypes)
	{
		par = &CTreeditorParam::classParams[parId];
	}
	else
	{
		CTreeditorDoc *doc = GetDocument ();
		CTreeditorClass *c = doc->GetSelectedClass ();
		if (c == NULL)
		{
			return;
		}
		par = c->_scriptParams[parId - treeditorClassParam_numberOfTypes];
	}

	if ((par->useWhat & USE_OPENFILE_DLG) != 0)
	{
		CString name = props->GetPropValue (item);
		name = name.Mid (1, name.GetLength () - 2);
		CFileDialog dlg (TRUE, NULL, name);
		if (dlg.DoModal() == IDOK)
		{
			name.Format(_T("\"%s\""), dlg.GetPathName());
			ChangeParam (true, NULL, parId, item, name, true, false, false, true);
		}
	}
}

void CTreeditorView::OnClassEditPalette() 
{
	// TODO: Add your command handler code here
	
	CTreeditorDoc *doc = GetDocument ();
	CString scriptFile;
	doc->GetParamInSelection(treeditorClassParam_script, scriptFile);
	if (scriptFile.IsEmpty ())
	{
		return;
	}
	scriptFile = scriptFile.Mid (1, scriptFile.GetLength () - 2);

	STreeditorParamGroupPalette &gp = doc->GetSelectedClass ()->_scriptParamPalette;
	CPaletteEditor pe (gp);
	if (pe.DoModal () == IDOK)
	{
		CFile f;
		if (f.Open(scriptFile, CFile::modeRead, NULL))
		{
			CString str;
			int fileLen = f.GetLength();
			int strLen = fileLen + sizeof (TCHAR) - 1 / sizeof (TCHAR);
			LPTSTR p = str.GetBuffer (strLen);
			bool read = f.Read(p, fileLen) == fileLen;
			str.ReleaseBuffer();
			f.Close();
			
			if (read)
			{
				CScriptParser sp (str, NULL);
				LPCTSTR endOfPrefix;
				LPCTSTR startOfPostfix;
				STreeditorParamGroupPalette tpal;
				CString out;
				if (!sp.GetGroupPalette (&tpal, &endOfPrefix, &startOfPostfix))
				{
					out = _T ("comment");
					out += _T (" \"PALETTE");
					for (int i = 0; i < pe._palette.nColors; ++i)
					{
						out += CScriptParser::_d;
						CString tmp;
						tmp.Format (_T ("0x%08x"), pe._palette.colors[i]);
						out += tmp;
					}
					out += _T ("\";\r\n\r\n");
					out += str;
				}
				else
				{
					out = str.Left (endOfPrefix - (LPCTSTR) str);
					out += _T (" \"PALETTE");
					for (int i = 0; i < pe._palette.nColors; ++i)
					{
						out += CScriptParser::_d;
						CString tmp;
						tmp.Format (_T ("0x%08x"), pe._palette.colors[i]);
						out += tmp;
					}
					out += _T ('\"');
					out += startOfPostfix;
				}


				if (f.Open(scriptFile, CFile::modeCreate | CFile::modeWrite, NULL))
				{
					f.Write (out, out.GetLength() * sizeof (TCHAR));
					f.Close ();
				}

				UpdateProps ();
			}
		}
	}
}

void CTreeditorView::OnUpdateClassEditPalette(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here

	CTreeditorDoc *doc = GetDocument ();
	CString scriptFile;
	doc->GetParamInSelection(treeditorClassParam_script, scriptFile);
	if (scriptFile.IsEmpty ())
	{
		pCmdUI->Enable (FALSE);
		return;
	}
}

void CTreeditorView::OnViewPreviewOn() 
{
	// TODO: Add your command handler code here
	
	bool preview = AfxGetApp ()->GetProfileInt (_T ("editor"), _T ("preview"), 1) != 0;
	preview = !preview;
	AfxGetApp ()->WriteProfileInt (_T ("editor"), _T ("preview"), preview ? 1 : 0);
	if (preview)
	{
		TreePreviewRefresh (false, false);
	}
}

void CTreeditorView::OnUpdateViewPreviewOn(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here

	int preview = AfxGetApp ()->GetProfileInt (_T ("editor"), _T ("preview"), 1);
	pCmdUI->SetCheck (preview);
}

void CTreeditorView::Pick (CPoint ptCursor)
{
	theApp.Remove3DArrows ();

	if (!m_treeGenerated)
	{
		return;
	}
	
	CListCtrl *nodeInfo = (CListCtrl*) ((CMainFrame*) AfxGetMainWnd ())->_infoBar.GetDlgItem (IDC_INFOLIST);
	if (nodeInfo == NULL)
	{
		return;
	}

    D3DXVECTOR3 rayDir;
    D3DXVECTOR3 rayOrig;

    // Get the pick ray from the mouse position
	RECT rvRect;
	_renderView.GetClientRect (&rvRect);

    // Get the inverse view matrix
    D3DXMATRIX matProj;
    D3DXMATRIX matView, m;
    // theApp.m_d3dDevice8->GetTransform( D3DTS_PROJECTION, &matProj );
    // theApp.m_d3dDevice8->GetTransform( D3DTS_VIEW, &matView );
	CalcTreeProjAndViewMat (matProj, matView, rvRect);
    D3DXMatrixInverse( &m, NULL, &matView );

    // Compute the vector of the pick ray in screen space
    D3DXVECTOR3 v;
    v.x =  ( ( ( 2.0f * ptCursor.x ) / (rvRect.right)  ) - 1 ) / matProj._11;
    v.y = -( ( ( 2.0f * ptCursor.y ) / (rvRect.bottom) ) - 1 ) / matProj._22;
    v.z =  1.0f;

    // Transform the screen space pick ray into 3D space
    rayDir.x  = v.x * m._11 + v.y * m._21 + v.z * m._31;
    rayDir.y  = v.x * m._12 + v.y * m._22 + v.z * m._32;
    rayDir.z  = v.x * m._13 + v.y * m._23 + v.z * m._33;
    rayOrig.x = m._41;
    rayOrig.y = m._42;
    rayOrig.z = m._43;

	D3DXVec3Normalize (&rayDir, &rayDir);
	rayOrig.x += 0.1f * rayDir.x;
	rayOrig.y += 0.1f * rayDir.y;
	rayOrig.z += 0.1f * rayDir.z;

	// Call TreeEngine to obtain node information

	// Fill
	CString tmpStr;
	nodeInfo->DeleteAllItems ();
	int item = 0;
	const CTreeditorDoc *doc = GetDocument ();

	SNodeInformation nodeInformation;
	if (theApp.TreeEngine->GetNearestNodeInfo (Vector3 (rayOrig.x, rayOrig.y, rayOrig.z), 
		Vector3 (rayDir.x, rayDir.y, rayDir.z), nodeInformation) == 0)
	{
		return;
	}

	nodeInfo->InsertItem (LVIF_TEXT, item, _T ("Orig Vector"), 0, 0, 0, 0);
	tmpStr.Format (_T ("(%g, %g, %g)"), rayOrig.x, rayOrig.y, rayOrig.z);
	nodeInfo->SetItemText (item++, 1, tmpStr);

	nodeInfo->InsertItem (LVIF_TEXT, item, _T ("Dir Vector"), 0, 0, 0, 0);
	tmpStr.Format (_T ("(%g, %g, %g)"), rayDir.x, rayDir.y, rayDir.z);
	nodeInfo->SetItemText (item++, 1, tmpStr);
	
	nodeInfo->InsertItem (LVIF_TEXT, item, _T ("Component"), 0, 0, 0, 0);
	nodeInfo->SetItemText (item++, 1, (doc->GetClass (nodeInformation._componentIndex))->name);

	nodeInfo->InsertItem (LVIF_TEXT, item, _T ("Seed"), 0, 0, 0, 0);
	tmpStr.Format (_T ("%d"), nodeInformation._actualParams._seed);
	nodeInfo->SetItemText (item++, 1, tmpStr);

	nodeInfo->InsertItem (LVIF_TEXT, item, _T ("Age"), 0, 0, 0, 0);
	tmpStr.Format (_T ("%g"), nodeInformation._actualParams._age);
	nodeInfo->SetItemText (item++, 1, tmpStr);

	nodeInfo->InsertItem (LVIF_TEXT, item, _T ("Counter"), 0, 0, 0, 0);
	tmpStr.Format (_T ("%d"), nodeInformation._actualParams._counter);
	nodeInfo->SetItemText (item++, 1, tmpStr);

	nodeInfo->InsertItem (LVIF_TEXT, item, _T ("Trunk"), 0, 0, 0, 0);
	tmpStr.Format (_T ("%g"), nodeInformation._actualParams._trunk);
	nodeInfo->SetItemText (item++, 1, tmpStr);

	nodeInfo->InsertItem (LVIF_TEXT, item, _T ("DiaOrigin"), 0, 0, 0, 0);
	tmpStr.Format (_T ("(%g, %g, %g)"), nodeInformation._actualParams._diaOrigin.X (), 
		nodeInformation._actualParams._diaOrigin.Y (), nodeInformation._actualParams._diaOrigin.Z ());
	nodeInfo->SetItemText (item++, 1, tmpStr);

	nodeInfo->InsertItem (LVIF_TEXT, item, _T ("TextureVOffset"), 0, 0, 0, 0);
	tmpStr.Format (_T ("%g"), nodeInformation._actualParams._textureVOffset);
	nodeInfo->SetItemText (item++, 1, tmpStr);

	nodeInfo->InsertItem (LVIF_TEXT, item, _T ("GeometrySpin"), 0, 0, 0, 0);
	tmpStr.Format (_T ("%g"), nodeInformation._actualParams._geometrySpin);
	nodeInfo->SetItemText (item++, 1, tmpStr);

	D3DXMATRIX mat;
	ConvertMatrix (mat, nodeInformation._actualParams._origin);

	// up
	CArrow3DInfo inf;
	inf.length = _tcstod (theApp.GetProfileString (_T ("editor"), _T ("arrowLength"), _T ("0.5")), NULL);
	inf.width = _tcstod (theApp.GetProfileString (_T ("editor"), _T ("arrowWidth"), _T ("0.1")), NULL);
	inf.tipLength = _tcstod (theApp.GetProfileString (_T ("editor"), _T ("arrowTipLength"), _T ("0.1")), NULL);
	inf.tipWidth = _tcstod (theApp.GetProfileString (_T ("editor"), _T ("arrowTipWidth"), _T ("0.2")), NULL);
	inf.color = theApp.GetProfileInt (_T ("editor"), _T ("arrowColorUp"), 0xFF00FF00);
	inf.origin = mat;
	theApp.Create3DArrow (inf);

	// direction
	D3DXMATRIX rot;
	D3DXMatrixRotationX (&rot, D3DX_PI / 2.0f);
	D3DXMatrixMultiply (&inf.origin, &rot, &mat);
	inf.color = theApp.GetProfileInt (_T ("editor"), _T ("arrowColorDir"), 0xFF0000FF);
	theApp.Create3DArrow (inf);

	// aside
	D3DXMatrixRotationZ (&rot, -D3DX_PI / 2.0f);
	D3DXMatrixMultiply (&inf.origin, &rot, &mat);
	inf.color = theApp.GetProfileInt (_T ("editor"), _T ("arrowColorDir"), 0xFFFF0000);
	theApp.Create3DArrow (inf);

	if (!m_animate)
	{
		Render ();
	}
	
    return;
}

void CTreeditorView::CalcTreeProjAndViewMat (D3DXMATRIX &projMat, D3DXMATRIX &viewMat, const RECT &r) const
{
	D3DXMatrixPerspectiveFovLH( &projMat, D3DX_PI/4, (float) r.right / (float) r.bottom, 0.1f, 500.0f );
	D3DXMatrixMultiply (&viewMat, &_matZoomRotation, &_matZoomTranslation);
}
