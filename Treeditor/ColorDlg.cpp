// ColorDlg.cpp : implementation file
//

#include "stdafx.h"
#include <El/elementpch.hpp>
#include "RV/Tree/mytree.h"
#include "Treeditor.h"
#include "TreeditorLex.h"
#include "ColorDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CColorDlg dialog


CColorDlg::CColorDlg(LPCTSTR str, CWnd* pParent /*=NULL*/)
	: CDialog(CColorDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CColorDlg)
	_blueFloat = 0.0f;
	_blueHex = 0;
	_greenFloat = 0.0f;
	_greenHex = 0;
	_redFloat = 0.0f;
	_redHex = 0;
	//}}AFX_DATA_INIT

	CTreeditorStringInput input (str);
	CTreeditorLex lex (&input, NULL, 0);
	STreeditorTokenDesc token;
	lex.GetNextToken (&token);
	if (token.type == treeditorLexLong || token.type == treeditorLexFixedFloat)
	{
		_redFloat = token.fnumber;

		lex.GetNextToken (&token);
		if (token.type == ',')
		{
			lex.GetNextToken (&token);
			if (token.type == treeditorLexLong || token.type == treeditorLexFixedFloat)
			{
				_greenFloat = token.fnumber;
				
				lex.GetNextToken (&token);
				if (token.type == ',')
				{
					lex.GetNextToken (&token);
					if (token.type == treeditorLexLong || token.type == treeditorLexFixedFloat)
					{
						_blueFloat = token.fnumber;
					}
				}
			}
		}
	}

	_oldRedFloat = _redFloat;
	_oldGreenFloat = _greenFloat;
	_oldBlueFloat = _blueFloat;
	_oldRedHex = _redHex = (int) (_redFloat * 255.0f + 0.5f);
	_oldGreenHex = _greenHex = (int) (_greenFloat * 255.0f + 0.5f);
	_oldBlueHex = _blueHex = (int) (_blueFloat * 255.0f + 0.5f);

	_oldColor.SetColor (RGB (_redHex, _greenHex, _blueHex));
	_newColor.SetColor (RGB (_redHex, _greenHex, _blueHex));
}


void CColorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CColorDlg)
	DDX_Control(pDX, IDC_OLD_COLOR, _oldColor);
	DDX_Control(pDX, IDC_NEW_COLOR, _newColor);
	DDX_Text(pDX, IDC_BLUE_FLOAT, _blueFloat);
	DDX_Text(pDX, IDC_BLUE_HEX, _blueHex);
	DDX_Text(pDX, IDC_GREEN_FLOAT, _greenFloat);
	DDX_Text(pDX, IDC_GREEN_HEX, _greenHex);
	DDX_Text(pDX, IDC_RED_FLOAT, _redFloat);
	DDX_Text(pDX, IDC_RED_HEX, _redHex);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CColorDlg, CDialog)
	//{{AFX_MSG_MAP(CColorDlg)
	ON_EN_CHANGE(IDC_RED_FLOAT, OnChangeRedFloat)
	ON_EN_CHANGE(IDC_GREEN_FLOAT, OnChangeGreenFloat)
	ON_EN_CHANGE(IDC_BLUE_FLOAT, OnChangeBlueFloat)
	ON_EN_CHANGE(IDC_RED_HEX, OnChangeRedHex)
	ON_EN_CHANGE(IDC_GREEN_HEX, OnChangeGreenHex)
	ON_EN_CHANGE(IDC_BLUE_HEX, OnChangeBlueHex)
	ON_BN_CLICKED(IDC_SYSTEM_COLOR_DIALOG, OnSystemColorDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CColorDlg message handlers

void CColorDlg::OnChangeRedFloat() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	
	UpdateData (TRUE);
	if (_redFloat != _oldRedFloat)
	{
		_oldRedFloat = _redFloat;
		_oldRedHex = _redHex = (int) (_redFloat * 255.0f + 0.5f);
		UpdateData (FALSE);
		_newColor.SetColor (RGB (_redHex, _greenHex, _blueHex));
	}
}

void CColorDlg::OnChangeGreenFloat() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	
	UpdateData (TRUE);
	if (_greenFloat != _oldGreenFloat)
	{
		_oldGreenFloat = _greenFloat;
		_oldGreenHex = _greenHex = (int) (_greenFloat * 255.0f + 0.5f);
		UpdateData (FALSE);
		_newColor.SetColor (RGB (_redHex, _greenHex, _blueHex));
	}
}

void CColorDlg::OnChangeBlueFloat() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	
	UpdateData (TRUE);
	if (_blueFloat != _oldBlueFloat)
	{
		_oldBlueFloat = _blueFloat;
		_oldBlueHex = _blueHex = (int) (_blueFloat * 255.0f + 0.5f);
		UpdateData (FALSE);
		_newColor.SetColor (RGB (_redHex, _greenHex, _blueHex));
	}
}

void CColorDlg::OnChangeRedHex() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	
	UpdateData (TRUE);
	if (_redHex != _oldRedHex)
	{
		_oldRedHex = _redHex;
		_oldRedFloat = _redFloat = (float) _redHex / 255.0f;
		UpdateData (FALSE);
		_newColor.SetColor (RGB (_redHex, _greenHex, _blueHex));
	}
}

void CColorDlg::OnChangeGreenHex() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	
	UpdateData (TRUE);
	if (_greenHex != _oldGreenHex)
	{
		_oldGreenHex = _greenHex;
		_oldGreenFloat = _greenFloat = (float) _greenHex / 255.0f;
		UpdateData (FALSE);
		_newColor.SetColor (RGB (_redHex, _greenHex, _blueHex));
	}
}

void CColorDlg::OnChangeBlueHex() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	
	UpdateData (TRUE);
	if (_blueHex != _oldBlueHex)
	{
		_oldBlueHex = _blueHex;
		_oldBlueFloat = _blueFloat = (float) _blueHex / 255.0f;
		UpdateData (FALSE);
		_newColor.SetColor (RGB (_redHex, _greenHex, _blueHex));
	}
}

void CColorDlg::OnSystemColorDialog() 
{
	// TODO: Add your control notification handler code here
	
	CColorDialog d (RGB (_redHex, _greenHex, _blueHex), 0, this);
	if (d.DoModal () == IDOK)
	{
		COLORREF c = d.GetColor ();
		_oldRedHex = _redHex = (int) (c & 0xff);
		_oldGreenHex = _greenHex = (int) ((c >> 8) & 0xff);
		_oldBlueHex = _blueHex = (int) ((c >> 16) & 0xff);
		_oldRedFloat = _redFloat = (float) _redHex / 255.0f;
		_oldGreenFloat = _greenFloat = (float) _greenHex / 255.0f;
		_oldBlueFloat = _blueFloat = (float) _blueHex / 255.0f;
		UpdateData (FALSE);
		_newColor.SetColor (RGB (_redHex, _greenHex, _blueHex));
	}
}
