// CompGraphCtrl.cpp: implementation of the CCompGraphCtrl class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <El/elementpch.hpp>
#include "RV/Tree/mytree.h"
#include "treeditor.h"
#include "CompGraphCtrl.h"

#include "MainFrm.h"
#include "TreeditorLex.h"
#include "TreeditorClass.h"
#include "TreeditorDoc.h"
#include "TreeditorView.h"
#include "SelectChildKindDlg.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCompGraphCtrl::CCompGraphCtrl()
{

}

CCompGraphCtrl::~CCompGraphCtrl()
{

}

void CCompGraphCtrl::DeleteAllItems ()
{
	DeleteAllLinks();
	for (int i; (i = EnumItems (-1)) != -1;)
	{
		DeleteItem (i);
	}
}

void CCompGraphCtrl::OnEndSelection()
{
	CTreeditorView *view = (CTreeditorView*) ((CMainFrame*) AfxGetMainWnd())->GetActiveView();
	CListCtrl *classes = (CListCtrl*) ((CMainFrame*) AfxGetMainWnd())->_componentBar.GetDlgItem (IDC_CLASSES);
	if (view != NULL && classes != NULL)
	{
		CTreeditorDoc *doc = view->GetDocument ();
		bool &uct = view->_updateClassesTime;
		uct = true;

		unsigned long cit;
		for (CTreeditorClass *c = doc->GetFirstClass (&cit); ; c = doc->GetNextClass (&cit))
		{
			if (c != NULL)
			{
				if ((GetItemFlags (c->graphId) & SGRI_SELECTED) != 0)
				{
					int idx = 0;
					for (c = doc->GetFirstClass (&cit); c != NULL; c = doc->GetNextClass (&cit))
					{
						c->selected = (GetItemFlags (c->graphId) & SGRI_SELECTED) != 0;
						classes->SetItemState (idx, c->selected ? LVIS_SELECTED : 0, LVIS_SELECTED);
						++idx;
					}
					break;
				}
			}
			else
			{
				int idx = 0;
				for (c = doc->GetFirstClass (&cit); c != NULL; c = doc->GetNextClass (&cit))
				{
					SetItemFlags (c->graphId, c->selected ? SGRI_SELECTED : 0, SGRI_SELECTED);
					classes->SetItemState (idx, c->selected ? LVIS_SELECTED : 0, LVIS_SELECTED);
					++idx;
				}
				break;
			}
		}

		uct = false;

		view->TreePreviewRefresh (false, false);
	}
}

void CCompGraphCtrl::OnLinkItems (int beginitem, int enditem)
{
	CTreeditorClass *beg = (CTreeditorClass*) GetItemData (beginitem);
	CTreeditorClass *end = (CTreeditorClass*) GetItemData (enditem);
	if (beg != NULL && end != NULL)
	{
		CSelectChildKindDlg dlg (beg, end);
		if (dlg.DoModal () == IDOK)
		{
			CTreeditorView *view = (CTreeditorView*) ((CMainFrame*) AfxGetMainWnd ())->GetActiveView ();
			if (view != NULL)
			{
				CTreeditorDoc *doc = view->GetDocument ();
				doc->ChangeParam (beg, true, NULL, dlg._childKind, end->name, true);
				view->TreePreviewRefresh (true, false);
			}
		}
	}
}

void CCompGraphCtrl::OnChangeLink (int lnk, int beginitem, int enditem, int newitem, void *data)
{
	CTreeditorClass *beg = (CTreeditorClass*) GetItemData (beginitem);
	CTreeditorClass *end = (CTreeditorClass*) GetItemData (newitem);
	int childKind = (int) data;
	if (beg != NULL && end != NULL)
	{
		CTreeditorView *view = (CTreeditorView*) ((CMainFrame*) AfxGetMainWnd ())->GetActiveView ();
		if (view != NULL)
		{
			CTreeditorDoc *doc = view->GetDocument ();
			doc->ChangeParam (beg, true, NULL, childKind, end->name, true);
			view->TreePreviewRefresh (true, false);
		}
	}
}

void CCompGraphCtrl::OnUnlinkItems (int lnk, int beginitem, int enditem, void *data)
{
	CTreeditorClass *beg = (CTreeditorClass*) GetItemData (beginitem);
	CTreeditorClass *end = (CTreeditorClass*) GetItemData (enditem);
	int childKind = (int) data;
	if (beg != NULL && end != NULL)
	{
		CTreeditorView *view = (CTreeditorView*) ((CMainFrame*) AfxGetMainWnd ())->GetActiveView ();
		if (view != NULL)
		{
			CTreeditorDoc *doc = view->GetDocument ();
			doc->ChangeParam (beg, true, NULL, childKind, _T (""), true);
			view->TreePreviewRefresh (true, false);
		}
	}
}

void CCompGraphCtrl::OnTouchLink (int lnk, int beginitem, int enditem, void *data)
{
	if (lnk == -1)
	{
		((CMainFrame*) AfxGetMainWnd())->SetMessageText(_T (""));
	}
	else
	{
		CTreeditorClass *beg = (CTreeditorClass*) GetItemData (beginitem);
		const STreeditorParamDesc *pd = (int) data < treeditorClassParam_numberOfTypes ? 
			&CTreeditorParam::classParams[(int) data] : 
			beg->_scriptParams[(int) data - treeditorClassParam_numberOfTypes];
		
		CString tstr;
		tstr.Format (_T("Kind of the child: %s"), pd->uiName);
		((CMainFrame*) AfxGetMainWnd())->SetMessageText(tstr);
		
		if (beg->selected)
		{
			CUiPropertiesCtrl &props = ((CMainFrame*) AfxGetMainWnd())->_propertyBar._propCtrl;
			int n = props.GetPropCount ();
			for (int i = 0; i < n; ++i)
			{
				if (props.GetPropData (i) == (long) data)
				{
					props.SelectOneProp (i);
					break;
				}
			}
		}
	}
}

void CCompGraphCtrl::OnEndMoveResize(int corner)
{
	CTreeditorDoc *doc = (CTreeditorDoc*) ((CFrameWnd*) AfxGetMainWnd())->GetActiveDocument();
	if (doc != NULL)
	{
		unsigned long i;
		for (CTreeditorClass *c = doc->GetFirstClass (&i); c != NULL; c = doc->GetNextClass (&i))
		{
			SGraphItem gi;
			if (GetItem (c->graphId, &gi))
			{
				c->_ediGraphPosition = true;
				c->_ediGraphLeft = gi.rect.left;
				c->_ediGraphRight = gi.rect.right;
				c->_ediGraphTop = gi.rect.top;
				c->_ediGraphBottom = gi.rect.bottom;
				doc->SetModifiedFlag (TRUE);
			}
		}
	}
}
