// InfoBar.h: interface for the CInfoBar class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INFOBAR_H__6F79FB98_72B5_4A94_95D0_27B316EA8F98__INCLUDED_)
#define AFX_INFOBAR_H__6F79FB98_72B5_4A94_95D0_27B316EA8F98__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CInfoBar : public CDialogBar  
{
public:
	CSize m_sizeDocked;
	CSize m_sizeFloating;
	BOOL m_bChangeDockedSize;

public:
	CInfoBar();
	virtual ~CInfoBar();

	BOOL Create( CWnd* pParentWnd, UINT nIDTemplate, UINT nStyle,
		UINT nID, CSize *s = NULL, BOOL = TRUE);
	BOOL Create( CWnd* pParentWnd, LPCTSTR lpszTemplateName,
		UINT nStyle, UINT nID, BOOL = TRUE);
	
	virtual CSize CalcDynamicLayout( int nLength, DWORD dwMode );
	CSize CalcDimension( int nLength, DWORD dwMode );
};

#endif // !defined(AFX_INFOBAR_H__6F79FB98_72B5_4A94_95D0_27B316EA8F98__INCLUDED_)
