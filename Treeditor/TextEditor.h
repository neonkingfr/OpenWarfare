#if !defined(AFX_TEXTEDITOR_H__CB71EB51_BD66_4B4A_B76D_8787253A644A__INCLUDED_)
#define AFX_TEXTEDITOR_H__CB71EB51_BD66_4B4A_B76D_8787253A644A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TextEditor.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTextEditor dialog

class CTextEditor : public CDialog
{
// Construction
public:
	CTextEditor(LPCTSTR text, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTextEditor)
	enum { IDD = IDD_TEXTEDITOR };
	CEdit	_textCtrl;
	//}}AFX_DATA

	bool _ctrlDown;
	CFont _font;
	CString _text;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTextEditor)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTextEditor)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEXTEDITOR_H__CB71EB51_BD66_4B4A_B76D_8787253A644A__INCLUDED_)
