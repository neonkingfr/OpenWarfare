vs.1.1

; This shader is suitable for rendering of a color map.
;
; Input:
;
;  c0-c3  - World * View matrix (4x4).
;  c4-c7  - Projection matrix (4x4)
;  c8     - Constant vector (0.5, 1.0, 0, 0)
;
;  v0     - Vertex position (x, y, z, 1) related to W*V*P matrix
;  v7.xy  - UV mapping coordinates
;
; Output:
;
;  oPos.xyz - Transformed position (related to V+P matrix).
;  oT0.xy   - Texture map coordinates

; Position transformation
m4x4 r0, v0, c0   ; W * V matrix transformation
m4x4 oPos, r0, c4 ; Projection matrix transformation

; Output texture coordinate
mov oT0.xy, v7
