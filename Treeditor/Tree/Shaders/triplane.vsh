vs.1.1

; This shader is suitable for rendering of a plate of a triplane.
;
; Input:
;
;  c0-c3  - World * View matrix (4x4).
;  c4-c7  - Projection matrix (4x4)
;  c8.xyz - Vector of light in world coordinates (we will not translate it by WV matrix).
;  c9     - Constant vector (0.5, 1.0, 0, 0).
;
;  v0     - Vertex position (x, y, z, 1) related to W*V*P matrix.
;  v3     - Vertex normal (x, y, z, 1) related to W*V matrix.
;  v7.xy  - Coordinates in the normal map and at the same time coordinates in the RGB map.
;  v8.xyz - U mapping coordinate of the normal map corresponds with the X (R) axis related to W*V matrix.
;  v9.xyz - V mapping coordinate of the normal map corresponds with the Y (G) axis related to W*V matrix.
;
; Output:
;
;  oD1.xyz  - Transformed vector of light (related to normal, U and V).
;  oPos.xyz - Transformed position (related to V+P matrix).
;  oT0.xy   - Normal map coordinates.
;  oT1.xy   - Color map coordinates.

; Transformation texture mapping coordinates into world coordiantes and creating of matrix
; which will convert vector in world coordinates (light) to texture mapping coordinates.
; V will be negative because of the fact texture has 0,0 coordinate at the top whereas
; render coordinate 0, 0, 0 is in the bottom.
; Normal will be negative because of expressing orientation of the polygon in standard way.
; (Z coordinate points away)
m3x3 r0, v8, c0  ; U
m3x3 r1, -v9, c0 ; V
m3x3 r2, -v3, c0 ; -Normal

; Transformation of light vector in world coordinates into texture mapping coordiantes.
; Negative value is here in order the dot product in ps will return proper value.
m3x3 r3, -c8, r0
mad oD1.xyz, r3, c9.xxxx, c9.xxxx

; Compute alpha using the dot product with a normal vector
;dp3 oD1.w, -v3, c2

; Position transformation
m4x4 r0, v0, c0   ; W * V matrix transformation
m4x4 oPos, r0, c4 ; Projection matrix transformation

; Texture coordinates copying
mov oT0.xy, v7
mov oT1.xy, v7