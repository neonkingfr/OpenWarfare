vs.1.1

; This shader is suitable for rendering of a normal map.
; Note that normals arn't normalized after ps interpolation.
; Note that W/V and Projection matrix are separated because of different transformation
; of the point and normal vector (to the vector we desire we must not apply
; the projection matrix).
;
; Input:
;
;  c0-c3  - World * View matrix (4x4).
;  c4-c7  - Projection matrix (4x4)
;  c8     - Constant vector (0.5, 1.0, 0, 0)
;
;  v0     - Vertex position (x, y, z, 1).
;  v3     - Vertex normal (x, y, z, 1).
;
; Output:
;
;  oD0.xyz  - Transformed normal vector (each value is from range 0..1).
;  oPos.xyz - Transformed position (related to V+P matrix).

; Normal transformation
m3x3 r0, v3, c0
mad oD0.xyz, r0, c8.xxxx, c8.xxxx
;mad oD0.w, c8.x, c8.x, c8.x

; Position transformation
m4x4 r0, v0, c0
m4x4 oPos, r0, c4

; Output texture coordinate
mov oT0.xy, v7
