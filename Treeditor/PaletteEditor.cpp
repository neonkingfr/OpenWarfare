// PaletteEditor.cpp : implementation file
//

#include "stdafx.h"
#include <El/elementpch.hpp>
#include "RV/Tree/mytree.h"
#include "treeditor.h"
#include "TreeditorLex.h"
#include "TreeditorClass.h"
#include "PaletteEditor.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPaletteEditor dialog


CPaletteEditor::CPaletteEditor(const STreeditorParamGroupPalette &palette, CWnd* pParent /*=NULL*/)
	: CDialog(CPaletteEditor::IDD, pParent), _palette (palette)
{
	//{{AFX_DATA_INIT(CPaletteEditor)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CPaletteEditor::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPaletteEditor)
	DDX_Control(pDX, IDC_COLORINDEX, _colorIndexCtrl);
	DDX_Control(pDX, IDC_COLOR, _colorCtrl);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPaletteEditor, CDialog)
	//{{AFX_MSG_MAP(CPaletteEditor)
	ON_BN_CLICKED(IDC_ADDCOLOR, OnAddColor)
	ON_BN_CLICKED(IDC_COLOR, OnColor)
	ON_BN_CLICKED(IDC_REMOVECOLOR, OnRemoveColor)
	ON_CBN_SELENDOK(IDC_COLORINDEX, OnSelEndOkColorIndex)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPaletteEditor message handlers

BOOL CPaletteEditor::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CString tmp;
	for (int i = 0; i < _palette.nColors; ++i)
	{
		tmp.Format (_T ("%d"), i);
		_colorIndexCtrl.AddString (tmp);
	}
	
	_colorIndexCtrl.SetCurSel (0);
	_colorCtrl.SetColor (_palette.colors[0]);

	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CPaletteEditor::OnAddColor() 
{
	// TODO: Add your control notification handler code here
	
	CString tmp;
	tmp.Format (_T ("%d"), _palette.nColors);
	_colorIndexCtrl.AddString (tmp);
	_colorIndexCtrl.SetCurSel (_palette.nColors);
	_colorCtrl.SetColor (_palette.colors[_palette.nColors]);
	++_palette.nColors;
}

void CPaletteEditor::OnColor() 
{
	// TODO: Add your control notification handler code here

	int i = _colorIndexCtrl.GetCurSel ();
	if (i != CB_ERR)
	{
		DWORD &p = _palette.colors[i];
		CColorDialog d (p, 0, this);
		if (d.DoModal () == IDOK)
		{
			p = d.GetColor ();
			_colorCtrl.SetColor (p);
		}
	}
}

void CPaletteEditor::OnRemoveColor() 
{
	// TODO: Add your control notification handler code here

	--_palette.nColors;
	_colorIndexCtrl.DeleteString (_palette.nColors);
}

void CPaletteEditor::OnSelEndOkColorIndex() 
{
	// TODO: Add your control notification handler code here
	
	int i = _colorIndexCtrl.GetCurSel ();
	if (i != CB_ERR)
	{
		_colorCtrl.SetColor (_palette.colors[i]);
	}
}
