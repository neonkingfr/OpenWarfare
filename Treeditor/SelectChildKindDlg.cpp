// SelectChildKindDlg.cpp : implementation file
//

#include "stdafx.h"
#include <El/elementpch.hpp>
#include "RV/Tree/mytree.h"
#include "treeditor.h"
#include "SelectChildKindDlg.h"

#include "TreeditorLex.h"
#include "TreeditorClass.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelectChildKindDlg dialog


CSelectChildKindDlg::CSelectChildKindDlg(CTreeditorClass *beg, CTreeditorClass *end, CWnd* pParent /*=NULL*/)
	: CDialog(CSelectChildKindDlg::IDD, pParent), _begin (beg), _end (end)
{
	//{{AFX_DATA_INIT(CSelectChildKindDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CSelectChildKindDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectChildKindDlg)
	DDX_Control(pDX, IDC_CHILDKIND, _childKindCtrl);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelectChildKindDlg, CDialog)
	//{{AFX_MSG_MAP(CSelectChildKindDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectChildKindDlg message handlers

BOOL CSelectChildKindDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	int selitem = 0;

	// TODO: Add extra initialization here
	const STreeditorClassTypeDesc *cd = &CTreeditorClass::classTypes[_begin->type];
	for (int i = 0; cd->params[i] != treeditorClassParam_stop; ++i)
	{
		int paramType = cd->params[i];
		const STreeditorParamDesc *pd = &CTreeditorParam::classParams[paramType];
		if (pd->isChildClassName)
		{
			int si = _childKindCtrl.AddString (pd->uiName);
			_childKindCodes.Add (paramType);
			if (_begin->params[paramType]._value == _end->name)
			{
				selitem = si;
			}
		}
	}
	_childKindCtrl.SetCurSel (selitem);

	if (_childKindCodes.GetSize() == 0)
	{
		EndDialog (IDCANCEL);
		return TRUE;
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSelectChildKindDlg::OnOK() 
{
	// TODO: Add extra validation here
	
	_childKind = _childKindCodes[_childKindCtrl.GetCurSel ()];

	CDialog::OnOK();
}
