// TreeditorView.h : interface of the CTreeditorView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_TREEDITORVIEW_H__F4ED0FC2_2DCA_43AA_B47E_908B0CE84859__INCLUDED_)
#define AFX_TREEDITORVIEW_H__F4ED0FC2_2DCA_43AA_B47E_908B0CE84859__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#define MAX_TREES 100

#define INIT_METER 1
#define INIT_TREE 2
#define INIT_GROUND 4
#define INIT_LIGHT_AND_WIND 8


class CCurveDlg;
class CUiPropertySubview;


class CTreeditorView : public CFormView
{
protected: // create from serialization only
	CTreeditorView();
	DECLARE_DYNCREATE(CTreeditorView)

public:
	//{{AFX_DATA(CTreeditorView)
	enum { IDD = IDD_TREEDITOR_FORM };
	CStatic	_renderView;
	//}}AFX_DATA

// Attributes
public:
	CTreeditorDoc* GetDocument();
	static bool _viewInitialized;
	int _sliderDiff;
	float _sliderExp;
	bool _updateClassesTime;
	bool _updatePropertiesTime;
	bool _updateComponentsTime;
	bool _lastPropsSelMask[treeditorClassParam_numberOfTypes];
	int _enumPropItem;
	int _enumParamId;
	D3DCOLOR m_bgColor;
	bool m_animate;
	bool m_immediateResponse;
	D3DXMATRIX _matZoomRotation;
	D3DXMATRIX _matZoomTranslation;
	D3DXMATRIX _matZoomForward;
	D3DXMATRIX _matZoomBackward;
	D3DXMATRIX _matZoomLeft;
	D3DXMATRIX _matZoomRight;
	D3DXMATRIX _matZoomUp;
	D3DXMATRIX _matZoomDown;
	D3DXMATRIX _matRotLeft;
	D3DXMATRIX _matRotRight;
	D3DXMATRIX _matRotUp;
	D3DXMATRIX _matRotDown;
	int m_buttonDown;
	CPoint m_buttonDownPoint;
	bool m_showMeter;
	bool m_showGround;
	bool m_wireframe;
	bool m_showBigLightPyramid;
	bool m_showBigWindPyramid;
	bool m_treeGenerated;
	bool m_autoDetail;
	CTreeditorDocUndo *_currentUndo;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTreeditorView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	virtual void OnDraw(CDC* pDC);
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTreeditorView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	void UpdateProps ();
	void UpdateScriptProps ();
	void UpdateClasses ();
	void UpdateClassType ();
	void UpdateAttrs ();
	void CheckSelectedClasses ();
	void UpdateSlider ();
	void UpdateRender (unsigned long initComps);
	void NextFrame ();
	void ResetRender ();
	static void ParseArrayOfFloats (LPCTSTR str, CArray<float, float> &vals);
	static void EqualizerLinearHookProc (const CCurveDlg *dlg, long param1, long param2, long param3);
	static void EqualizerQuadraticHookProc (const CCurveDlg *dlg, long param1, long param2, long param3);
	static void CurveDlgLinearHookProc (const CCurveDlg *dlg, long param1, long param2, long param3);
	static void CurveDlgLinearWithXHookProc (const CCurveDlg *dlg, long param1, long param2, long param3);
	static void CurveDlgQuadraticHookProc (const CCurveDlg *dlg, long param1, long param2, long param3);
	static void CurveDlgQuadraticWithXHookProc (const CCurveDlg *dlg, long param1, long param2, long param3);
	static void CurveDlgExQuadraticHookProc (const CCurveDlg *dlg, long param1, long param2, long param3);
	static void CurveDlgExQuadraticWithXHookProc (const CCurveDlg *dlg, long param1, long param2, long param3);

	void OnPropEdit (CUiPropertySubview *props, int item, long parId);
	void OnPropSelect (CUiPropertySubview *props, int item, long parId);
	void SetPropValue (CUiPropertySubview *props, int idx, long data, LPCTSTR value);
	LPCTSTR GetPropValue (CUiPropertySubview *props, int idx, long paramIdx) const;
	bool SetPropSelect (CUiPropertySubview *props, int idx, long data, bool sel);
	void TreePreviewRefresh (bool paramChange, bool updateScripts);
	
	void Pick (CPoint pt);
	void CalcTreeProjAndViewMat (D3DXMATRIX &projMat, D3DXMATRIX &viewMat, const RECT &r) const;

protected:
	bool InitRender (unsigned long initComps);
	void Render ();
	void CopyProps ();
	void CopyClasses ();
	void PasteProps ();
	void PasteClasses ();
	void DeleteTrees ();
	void ShowStats (float fps, FLOAT distance);
	void SetRenderFillMode ();

	void ChangeParam (bool recUndo, CTreeditorDocUndo *undo, int paramIdx, int itemIdx, CString &value, bool dirty, bool slider, bool sliderUpdate, bool valEditUpdate);
	void ChangeParamInTree (bool recUndo, CTreeditorDocUndo *undo, int paramIdx, int itemIdx, CString &value, bool dirty, bool slider, bool sliderUpdate, bool valEditUpdate);
	void ChangeParamInScript(int paramIdx, int itemIdx, CString &value, bool slider, bool sliderUpdate, bool valEditUpdate);

// Generated message map functions
protected:
	//{{AFX_MSG(CTreeditorView)
	afx_msg void OnItemChangedClasses(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemChangingClasses(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnUpdateNewClass(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDeleteClass(CCmdUI* pCmdUI);
	afx_msg void OnNewClass();
	afx_msg void OnEndLabelEditClasses(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeleteClass();
	afx_msg void OnSelChangeClassType();
	afx_msg void OnKeyDownClasses(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkClasses(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnReleasedCaptureParamSlider(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnClassesDown();
	afx_msg void OnUpdateClassesDown(CCmdUI* pCmdUI);
	afx_msg void OnClassesUp();
	afx_msg void OnUpdateClassesUp(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEditCut(CCmdUI* pCmdUI);
	afx_msg void OnEditCopy();
	afx_msg void OnEditCut();
	afx_msg void OnUpdateEditPaste(CCmdUI* pCmdUI);
	afx_msg void OnEditPaste();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnChangeBgColor();
	afx_msg void OnAnimate();
	afx_msg void OnUpdateAnimate(CCmdUI* pCmdUI);
	afx_msg void OnReleasedCaptureDetailSlider(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnTreeBackward();
	afx_msg void OnTreeForward();
	afx_msg void OnTreeLeft();
	afx_msg void OnTreeRight();
	afx_msg void OnReleasedCaptureTreesSlider(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeDetail();
	afx_msg void OnChangeTrees();
	afx_msg void OnChangeDist();
	afx_msg void OnReleasedCaptureDistSlider(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillFocusDetail();
	afx_msg void OnKillFocusDist();
	afx_msg void OnKillFocusTrees();
	afx_msg void OnImmediateResponse();
	afx_msg void OnUpdateImmediateResponse(CCmdUI* pCmdUI);
	afx_msg void OnTreeDown();
	afx_msg void OnTreeUp();
	afx_msg void OnTreeRotDown();
	afx_msg void OnTreeRotLeft();
	afx_msg void OnTreeRotRight();
	afx_msg void OnTreeRotUp();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnChangeMeter();
	afx_msg void OnKillFocusMeter();
	afx_msg void OnReleasedCaptureMeterSlider(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnReleasedCaptureLRotYSlider(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnReleasedCaptureLRotZSlider(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnCenterTreeView();
	afx_msg void OnUpdateShowMeter(CCmdUI* pCmdUI);
	afx_msg void OnShowMeter();
	afx_msg void OnChangeAge();
	afx_msg void OnKillFocusAge();
	afx_msg void OnReleasedCaptureAgeSlider(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeWind();
	afx_msg void OnKillFocusWind();
	afx_msg void OnReleasedCaptureWindSlider(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnWireframe();
	afx_msg void OnUpdateWireframe(CCmdUI* pCmdUI);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnShowBigLightPyramid();
	afx_msg void OnUpdateShowBigLightPyramid(CCmdUI* pCmdUI);
	afx_msg void OnReleasedCaptureWRotYSlider(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnReleasedCaptureWRotZSlider(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnShowBigWindPyramid();
	afx_msg void OnUpdateShowBigWindPyramid(CCmdUI* pCmdUI);
	afx_msg void OnAutoDetail();
	afx_msg void OnUpdateAutoDetail(CCmdUI* pCmdUI);
	afx_msg void OnChangeNear();
	afx_msg void OnKillFocusNear();
	afx_msg void OnReleasedCaptureNearSlider(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeFar();
	afx_msg void OnKillFocusFar();
	afx_msg void OnReleasedCaptureFarSlider(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillFocusSeed();
	afx_msg void OnDeltaPosSeedSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeSeed();
	afx_msg void OnEditUndo();
	afx_msg void OnUpdateEditUndo(CCmdUI* pCmdUI);
	afx_msg void OnEditRedo();
	afx_msg void OnUpdateEditRedo(CCmdUI* pCmdUI);
	afx_msg void OnChangeGround();
	afx_msg void OnShowGround();
	afx_msg void OnUpdateShowGround(CCmdUI* pCmdUI);
	afx_msg void OnEditPreferences();
	afx_msg void OnEqualizerLinear();
	afx_msg void OnUpdateEqualizerLinear(CCmdUI* pCmdUI);
	afx_msg void OnEqualizerQuadratic();
	afx_msg void OnUpdateEqualizerQuadratic(CCmdUI* pCmdUI);
	afx_msg void OnViewRefresh();
	afx_msg void OnEditSelAllProp();
	afx_msg void OnEditDeselAllProps();
	afx_msg void OnClassEditPalette();
	afx_msg void OnUpdateClassEditPalette(CCmdUI* pCmdUI);
	afx_msg void OnViewPreviewOn();
	afx_msg void OnUpdateViewPreviewOn(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in TreeditorView.cpp
inline CTreeditorDoc* CTreeditorView::GetDocument()
   { return (CTreeditorDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TREEDITORVIEW_H__F4ED0FC2_2DCA_43AA_B47E_908B0CE84859__INCLUDED_)
