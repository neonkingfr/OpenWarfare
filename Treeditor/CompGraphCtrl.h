// CompGraphCtrl.h: interface for the CCompGraphCtrl class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_COMPGRAPHCTRL_H__2A653301_B2AD_4E59_A5A7_E4EA18D8652F__INCLUDED_)
#define AFX_COMPGRAPHCTRL_H__2A653301_B2AD_4E59_A5A7_E4EA18D8652F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GraphCtrl.h"

class CCompGraphCtrl : public CGraphCtrl  
{
public:
	CCompGraphCtrl();
	virtual ~CCompGraphCtrl();

	void DeleteAllItems ();
	
	virtual void OnEndSelection();
	virtual void OnLinkItems (int beginitem, int enditem);
	virtual void OnChangeLink (int lnk, int beginitem, int enditem, int newitem, void *data);
	virtual void OnUnlinkItems (int lnk, int beginitem, int enditem, void *data);
	virtual void OnTouchLink (int lnk, int beginitem, int enditem, void *data);
	virtual void OnEndMoveResize(int corner);
};

#endif // !defined(AFX_COMPGRAPHCTRL_H__2A653301_B2AD_4E59_A5A7_E4EA18D8652F__INCLUDED_)
