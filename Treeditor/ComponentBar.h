// ComponentBar.h: interface for the CComponentBar class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_COMPONENTBAR_H__A6A155D9_D043_4D9D_98E5_AAD11E1A6E97__INCLUDED_)
#define AFX_COMPONENTBAR_H__A6A155D9_D043_4D9D_98E5_AAD11E1A6E97__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CComponentBar : public CDialogBar  
{
public:
	CSize m_sizeDocked;
	CSize m_sizeFloating;
	BOOL m_bChangeDockedSize;

public:
	CComponentBar();
	virtual ~CComponentBar();

	BOOL Create( CWnd* pParentWnd, UINT nIDTemplate, UINT nStyle,
		UINT nID, CSize *s = NULL, BOOL = TRUE);
	BOOL Create( CWnd* pParentWnd, LPCTSTR lpszTemplateName,
		UINT nStyle, UINT nID, BOOL = TRUE);
	
	virtual CSize CalcDynamicLayout( int nLength, DWORD dwMode );
	CSize CalcDimension( int nLength, DWORD dwMode );

	virtual BOOL OnWndMsg( UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult );
};

#endif // !defined(AFX_COMPONENTBAR_H__A6A155D9_D043_4D9D_98E5_AAD11E1A6E97__INCLUDED_)
