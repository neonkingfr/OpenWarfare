// Treeditor.h : main header file for the TREEDITOR application
//

#if !defined(AFX_TREEDITOR_H__030778AF_2414_4354_B07F_9AC0E5173A09__INCLUDED_)
#define AFX_TREEDITOR_H__030778AF_2414_4354_B07F_9AC0E5173A09__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

#define DETAIL_IS_N_POLYGONS
#define N_POLYGONS_MAX 20000
//#define FLOAT_BAR_HOR_ALIGN 16
//#define FLOAT_BAR_VER_ALIGN 16

/////////////////////////////////////////////////////////////////////////////
// CTreeditorApp:
// See Treeditor.cpp for the implementation of this class
//


#define D3DFVF_GROUNDVERTEX (D3DFVF_XYZ|\
                               D3DFVF_NORMAL|\
                               D3DFVF_DIFFUSE|\
                               D3DFVF_TEX1|D3DFVF_TEXCOORDSIZE2(0))
struct GROUNDVERTEX
{
    FLOAT x, y, z;        // The position.
    FLOAT nx, ny, nz;     // The surface normal for the vertex.
    DWORD color;          // The color.
    FLOAT tu, tv;         // The texture coordinates.
};

#define D3DFVF_METERVERTEX (D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_DIFFUSE)
struct METERVERTEX
{
    FLOAT x, y, z;        // The position.
    FLOAT nx, ny, nz;     // The surface normal for the vertex.
    DWORD color;          // The color.
};

#define D3DFVF_LIGHTVERTEX (D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_DIFFUSE)
struct LIGHTVERTEX
{
    FLOAT x, y, z;        // The position.
    FLOAT nx, ny, nz;     // The surface normal for the vertex.
    DWORD color;          // The color.
};

#define D3DFVF_LIGHTPYRVERTEX (D3DFVF_XYZ|D3DFVF_DIFFUSE)
struct LIGHTPYRVERTEX
{
    FLOAT x, y, z;        // The position.
    DWORD color;          // The color.
};

#define D3DFVF_WINDVERTEX (D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_DIFFUSE)
struct WINDVERTEX
{
    FLOAT x, y, z;        // The position.
    FLOAT nx, ny, nz;     // The surface normal for the vertex.
    DWORD color;          // The color.
};

#define D3DFVF_WINDPYRVERTEX (D3DFVF_XYZ|D3DFVF_DIFFUSE)
struct WINDPYRVERTEX
{
    FLOAT x, y, z;        // The position.
    DWORD color;          // The color.
};

#define D3DFVF_ARROWVERTEX (D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_DIFFUSE)
struct ARROWVERTEX
{
    FLOAT x, y, z;        // The position.
    FLOAT nx, ny, nz;     // The surface normal for the vertex.
    DWORD color;          // The color.
};

struct CArrow3DInfo
{
	float length;
	float width;
	float tipLength;
	float tipWidth;
	D3DXCOLOR color;
	D3DXMATRIX origin; 
};

class CTreeditorApp : public CWinApp
{
public:
	IDirect3D8 *m_d3d8;
	IDirect3DDevice8 *m_d3dDevice8;
	IDirect3DVertexBuffer8 *m_d3dGroundVertexBuffer;
	IDirect3DTexture8 *m_d3dGroundTexture;
	IDirect3DVertexBuffer8 *m_d3dMeterVertexBuffer;
	IDirect3DVertexBuffer8 *m_d3dLightVertexBuffer;
	IDirect3DVertexBuffer8 *m_d3dLightPyrVertexBuffer;
	IDirect3DIndexBuffer8 *m_d3dLightPyrIndexBuffer;
	IDirect3DVertexBuffer8 *m_d3dWindVertexBuffer;
	IDirect3DVertexBuffer8 *m_d3dWindPyrVertexBuffer;
	IDirect3DIndexBuffer8 *m_d3dWindPyrIndexBuffer;
	CTreeEngine *TreeEngine;
	bool m_resetDevice;
	bool _isExitting;
	CArray<IDirect3DVertexBuffer8*, IDirect3DVertexBuffer8*> _arrowVertexBuffers;
	CArray<IDirect3DIndexBuffer8*, IDirect3DIndexBuffer8*> _arrowIndexBuffers;
	CArray<CArrow3DInfo, CArrow3DInfo> _arrows;

public:
	CTreeditorApp();
	BOOL InitD3D (CWnd *rv);
	bool InitD3DResources ();
	void ExitD3D ();
	void ReleaseD3DResources ();
	void ResetD3D ();
	void AnimateTree ();
	bool Create3DArrow(const CArrow3DInfo &info);
	void Remove3DArrows();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTreeditorApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual int Run();
	virtual BOOL OnIdle(LONG lCount);
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CTreeditorApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


extern CTreeditorApp theApp;


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TREEDITOR_H__030778AF_2414_4354_B07F_9AC0E5173A09__INCLUDED_)
