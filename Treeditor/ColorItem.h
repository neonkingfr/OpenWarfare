#if !defined(AFX_COLORITEM_H__2196CB6F_0F66_4B4B_96F5_30E98E444F40__INCLUDED_)
#define AFX_COLORITEM_H__2196CB6F_0F66_4B4B_96F5_30E98E444F40__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ColorItem.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CColorItem window

class CColorItem : public CStatic
{
// Construction
public:
	CColorItem();

// Attributes
public:
	COLORREF	_color;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CColorItem)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CColorItem();
	void SetColor (COLORREF c);

	// Generated message map functions
protected:
	//{{AFX_MSG(CColorItem)
	afx_msg void OnPaint();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COLORITEM_H__2196CB6F_0F66_4B4B_96F5_30E98E444F40__INCLUDED_)
