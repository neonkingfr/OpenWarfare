// ScriptParser.h: interface for the CScriptParser class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SCRIPTPARSER_H__8AD2391E_0866_47FB_8ECA_511703BA147A__INCLUDED_)
#define AFX_SCRIPTPARSER_H__8AD2391E_0866_47FB_8ECA_511703BA147A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TreeditorLex.h"
#include "TreeditorClass.h"


struct STreeditorScriptParamDesc : public STreeditorParamDesc
{
	LPCTSTR _endOfPrefix;
	LPCTSTR _startOfPostfix;
};


class CScriptParser  
{
public:
	static const STreeditorKeyword _scriptKeyWords[];
	static const STreeditorKeyword _scKeyWords[];
	static const char _d;
	static const char _let;

	LPCTSTR _scriptText;

	CTreeditorStringInput _input;
	CTreeditorLex _lex;
	STreeditorTokenDesc _token;

	CArray<STreeditorParamDesc*, STreeditorParamDesc*> *_vars;

public:
	CScriptParser(LPCTSTR scriptText, CArray<STreeditorParamDesc*, STreeditorParamDesc*> *vars);
	virtual ~CScriptParser();

	bool GetGroupPalette (STreeditorParamGroupPalette *palette, LPCTSTR *endOfPrefix, LPCTSTR *startOfPostfix);
	bool ParsePalette (STreeditorParamGroupPalette *palette, LPCTSTR *endOfPrefix, LPCTSTR *startOfPostfix);
	bool ParsePaletteColors (STreeditorParamGroupPalette *palette, 
		CTreeditorLex &csLex, STreeditorTokenDesc &csToken,
		LPCTSTR *endOfPrefix, LPCTSTR *startOfPostfix);

	bool GetNextParam (STreeditorScriptParamDesc *desc);
	bool ParseParam (STreeditorScriptParamDesc *desc);
	bool ParseParamGroup (STreeditorScriptParamDesc *desc, CTreeditorLex &csLex, STreeditorTokenDesc &csToken);
	bool ParseParamExport (STreeditorScriptParamDesc *desc, CTreeditorLex &csLex, STreeditorTokenDesc &csToken);
	bool ParseParamDesc (STreeditorScriptParamDesc *desc, CTreeditorLex &csLex, STreeditorTokenDesc &csToken);
	bool ParseParamType (STreeditorScriptParamDesc *desc, CTreeditorLex &csLex, STreeditorTokenDesc &csToken);
	bool ParseParamUse (STreeditorScriptParamDesc *desc, CTreeditorLex &csLex, STreeditorTokenDesc &csToken);
	bool ParseParamAssignment(float &value, CTreeditorLex &csLex, STreeditorTokenDesc &csToken);
	bool ParseParamAssignment(bool &value, CTreeditorLex &csLex, STreeditorTokenDesc &csToken);
	/*
	bool ParseParamExpression (float &value, CTreeditorLex &csLex, STreeditorTokenDesc &csToken);
	bool ParseParamExpressionOr (float &value, CTreeditorLex &csLex, STreeditorTokenDesc &csToken);
	bool ParseParamExpressionAnd (float &value, CTreeditorLex &csLex, STreeditorTokenDesc &csToken);
	bool ParseParamExpressionAddSub (float &value, CTreeditorLex &csLex, STreeditorTokenDesc &csToken);
	bool ParseParamExpressionMulDiv (float &value, CTreeditorLex &csLex, STreeditorTokenDesc &csToken);
	bool ParseParamExpressionValue (float &value, CTreeditorLex &csLex, STreeditorTokenDesc &csToken);
	*/
	bool ParseParamString (char *axis, CTreeditorLex &csLex, STreeditorTokenDesc &csToken);
};

#endif // !defined(AFX_SCRIPTPARSER_H__8AD2391E_0866_47FB_8ECA_511703BA147A__INCLUDED_)
