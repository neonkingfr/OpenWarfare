#if !defined(AFX_COLORDLG_H__B097F7CA_6F43_45E8_9F0C_92EE5FDEDD13__INCLUDED_)
#define AFX_COLORDLG_H__B097F7CA_6F43_45E8_9F0C_92EE5FDEDD13__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ColorDlg.h : header file
//

#include "ColorItem.h"

/////////////////////////////////////////////////////////////////////////////
// CColorDlg dialog

class CColorDlg : public CDialog
{
// Construction
public:
	CColorDlg(LPCTSTR str, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CColorDlg)
	enum { IDD = IDD_COLORDLG };
	CColorItem	_oldColor;
	CColorItem	_newColor;
	float	_blueFloat;
	int		_blueHex;
	float	_greenFloat;
	int		_greenHex;
	float	_redFloat;
	int		_redHex;
	//}}AFX_DATA

	float _oldRedFloat, _oldGreenFloat, _oldBlueFloat;
	int _oldRedHex, _oldGreenHex, _oldBlueHex;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CColorDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CColorDlg)
	afx_msg void OnChangeRedFloat();
	afx_msg void OnChangeGreenFloat();
	afx_msg void OnChangeBlueFloat();
	afx_msg void OnChangeRedHex();
	afx_msg void OnChangeGreenHex();
	afx_msg void OnChangeBlueHex();
	afx_msg void OnSystemColorDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COLORDLG_H__B097F7CA_6F43_45E8_9F0C_92EE5FDEDD13__INCLUDED_)
