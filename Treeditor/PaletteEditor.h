#if !defined(AFX_PALETTEEDITOR_H__5463C052_8538_41B5_8018_27CA45F7880A__INCLUDED_)
#define AFX_PALETTEEDITOR_H__5463C052_8538_41B5_8018_27CA45F7880A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PaletteEditor.h : header file
//

#include "ColorItem.h"
/////////////////////////////////////////////////////////////////////////////
// CPaletteEditor dialog

class CPaletteEditor : public CDialog
{
// Construction
public:
	CPaletteEditor(const STreeditorParamGroupPalette &palette, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPaletteEditor)
	enum { IDD = IDD_PALETTEEDITOR };
	CComboBox	_colorIndexCtrl;
	CColorItem	_colorCtrl;
	//}}AFX_DATA

	STreeditorParamGroupPalette _palette;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPaletteEditor)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CPaletteEditor)
	virtual BOOL OnInitDialog();
	afx_msg void OnAddColor();
	afx_msg void OnColor();
	afx_msg void OnRemoveColor();
	afx_msg void OnSelEndOkColorIndex();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PALETTEEDITOR_H__5463C052_8538_41B5_8018_27CA45F7880A__INCLUDED_)
