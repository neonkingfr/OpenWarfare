#include <limits.h>
#include <assert.h>
#include "primitivestream.h"

//--------------------------------------------------------------------------------------

CPrimitiveStage::CPrimitiveStage() {
  _pTextureS0 = NULL;
  _pTextureS1 = NULL;
  _MinVertexIndex = UINT_MAX;
  _MaxVertexIndex = 0;
}

void CPrimitiveStage::Clear() {
  _Indices.Resize(0);
}

void CPrimitiveStage::AddIndex(const WORD &Index) {
  _Indices.Add(Index);
  if (Index < _MinVertexIndex) _MinVertexIndex = Index;
  if (Index > _MaxVertexIndex) _MaxVertexIndex = Index;
}

int CPrimitiveStage::Size() {
  return _Indices.Size();
}

const WORD *CPrimitiveStage::Data() {
  return _Indices.Data();
}

UINT CPrimitiveStage::GetMinVertexIndex() {
  return _MinVertexIndex;
}

UINT CPrimitiveStage::GetNumVertices() {
  return _MaxVertexIndex - _MinVertexIndex;
}

//--------------------------------------------------------------------------------------
CPrimitiveStream::CPrimitiveStream(IDirect3DDevice8 *pD3DDevice) {
  // Save device
  _pD3DDevice = ComRef<IDirect3DDevice8>(pD3DDevice);
}

void CPrimitiveStream::Init(int VertexSize, DWORD VertexHandle, DWORD PixelHandle, DWORD FVF) {
  // Save parametres
  _VertexSize = VertexSize;
  _VertexHandle = VertexHandle;
  _PixelHandle = PixelHandle;

  HRESULT hr;

  // Create vertex buffer
  hr = _pD3DDevice->CreateVertexBuffer(VB_SIZE * _VertexSize, D3DUSAGE_WRITEONLY, FVF, D3DPOOL_DEFAULT, _pVB.Init());
  assert(SUCCEEDED(hr));

  // Create index buffer
  hr = _pD3DDevice->CreateIndexBuffer(IB_SIZE * sizeof(WORD), D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_DEFAULT, _pIB.Init());
  assert(SUCCEEDED(hr));
}

void CPrimitiveStream::Clear() {
  for (int i = 0; i < PRIMITIVESTAGE_COUNT; i++) {
    _PS[i].Clear();
  }
  _Vertices.Resize(0);
}

int CPrimitiveStream::AddVertex(void *pData) {
  int OldSize = _Vertices.Size();
  for (int i = 0; i < _VertexSize; i++) {
    _Vertices.Add(((BYTE*)pData)[i]);
  }
  return OldSize / _VertexSize;
}

void CPrimitiveStream::AddIndex(int Stage, USHORT &Index) {
  _PS[Stage].AddIndex(Index);
}

int CPrimitiveStream::GetNewVertexIndex() {
  return _Vertices.Size() / _VertexSize;
}

void CPrimitiveStream::RegisterTextures(int Stage, IDirect3DTexture8 *pTextureS0, IDirect3DTexture8 *pTextureS1) {
  _PS[Stage]._pTextureS0 = pTextureS0;
  _PS[Stage]._pTextureS1 = pTextureS1;
}

void CPrimitiveStream::Prepare() {
  HRESULT hr;
  BYTE *pData;

  // Vertex buffer
  assert(_Vertices.Size()/3 <= VB_SIZE);
  hr = _pVB->Lock(0, _Vertices.Size(), &pData, 0);
  assert(SUCCEEDED(hr));
  memcpy(pData, _Vertices.Data(), _Vertices.Size());
  _pVB->Unlock();

  // Index buffer: Serialize data from all stages and fill the _StageIndexPos array.
  int Size = 0;
  for (int i = 0; i < PRIMITIVESTAGE_COUNT; i++) {
    Size += _PS[i].Size();
  }
  assert(Size <= IB_SIZE);
  hr = _pIB->Lock(0, Size * sizeof(WORD), &pData, 0);
  assert(SUCCEEDED(hr));
  UINT LastIndex = 0;
  for (i = 0; i < PRIMITIVESTAGE_COUNT; i++) {
    _StageIndexPos[i] = LastIndex;
    memcpy(&((WORD*)pData)[LastIndex], _PS[i].Data(), _PS[i].Size() * sizeof(WORD));
    LastIndex += _PS[i].Size();
  }
  _pIB->Unlock();
}

void CPrimitiveStream::Draw(BOOL SetTextures) {
  HRESULT hr;
  for (int i = 0; i < PRIMITIVESTAGE_COUNT; i++) {
    if (_PS[i].Size() > 0) {
      _pD3DDevice->SetTexture(0, NULL);
      _pD3DDevice->SetTexture(1, NULL);
      if (SetTextures) {
        _pD3DDevice->SetTexture(0, _PS[i]._pTextureS0);
        _pD3DDevice->SetTexture(1, _PS[i]._pTextureS1);
      }
      _pD3DDevice->SetTextureStageState(0, D3DTSS_ADDRESSU, D3DTADDRESS_WRAP);
      _pD3DDevice->SetTextureStageState(0, D3DTSS_ADDRESSV, D3DTADDRESS_WRAP);
      _pD3DDevice->SetTextureStageState(1, D3DTSS_ADDRESSU, D3DTADDRESS_WRAP);
      _pD3DDevice->SetTextureStageState(1, D3DTSS_ADDRESSV, D3DTADDRESS_WRAP);
      hr = _pD3DDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, _PS[i].GetMinVertexIndex(), _PS[i].GetNumVertices(), _StageIndexPos[i], _PS[i].Size() / 3);
      assert(SUCCEEDED(hr));
    }
  }
}
