
#ifndef _TREEDITORLEX_H_
#define _TREEDITORLEX_H_


enum ETreeditorTokenType
{
	treeditorLexLong = 256,
	treeditorLexFixedFloat,
	treeditorLexString,

	treeditorLexEof,
	treeditorLexKeyword,
	treeditorLexId,
	treeditorLexArray, // just for use as a kind of CTreeditorParam, not in lex!

	treeditorLex_class = 1000,
	treeditorLex_type,
	treeditorLex_ediGraphLeft,
	treeditorLex_ediGraphRight,
	treeditorLex_ediGraphTop,
	treeditorLex_ediGraphBottom,

	treeditorLex_comment,
	treeditorLex_export,
	treeditorLex_group,
	treeditorLex_palette,
	
	treeditorLex_use,
	treeditorLex_min,
	treeditorLex_max,
	treeditorLex_step,
	treeditorLex_axisX,
	treeditorLex_axisY,
	treeditorLex_horizontalX,
	treeditorLex_horizontalY,
	treeditorLex_name,
	treeditorLex_pairValue,
	treeditorLex_pairInterval,
	treeditorLex_enabled,

	treeditorLex_float,
	treeditorLex_long,
	treeditorLex_string,
	treeditorLex_id,
	treeditorLex_array,
	treeditorLex_bool,
	
	treeditorLex_true,
	treeditorLex_false,

	treeditorLex_slider,
	treeditorLex_filename,
	treeditorLex_fixedpolyline,
	treeditorLex_fixedcurve,
	treeditorLex_exfixedcurve,
	treeditorLex_polyline,
	treeditorLex_curve,
	treeditorLex_excurve,
	treeditorLex_fixedpolylinex,
	treeditorLex_fixedcurvex,
	treeditorLex_exfixedcurvex,
	treeditorLex_polylinex,
	treeditorLex_curvex,
	treeditorLex_excurvex,
	treeditorLex_color,
	treeditorLex_child,
	treeditorLex_text,
	treeditorLex_pair,
};


struct STreeditorKeyword
{
	char *token;
	int id;
};


struct STreeditorTokenDesc
{
	int type;
	long number;
	float fnumber;
	char string[4096];
};


class CTreeditorInput
{
public:
	virtual unsigned int Read (void *buf, unsigned int max) = 0;
};


class CTreeditorArchiveInput : public CTreeditorInput
{
protected:
	CArchive *archive;

public:
	CTreeditorArchiveInput (CArchive *ar)
		: archive (ar)
	{
	}

	virtual unsigned int Read (void *buf, unsigned int max)
	{
		return archive->Read (buf, max);
	}
};


class CTreeditorStringInput : public CTreeditorInput
{
protected:
	const char *string;
	const char *p;
	unsigned int index;
	unsigned int length;

public:
	CTreeditorStringInput (const char *str)
		: string (str), p (str), index (0), length (strlen (str))
	{
	}

	virtual unsigned int Read (void *buf, unsigned int max)
	{
		unsigned int i = length - index;
		if (i < max)
		{
			max = i;
		}

		char *dst = (char*) buf;
		for (i = 0; i < max; ++i)
		{
			*dst++ = *p++;
		}

		index += max;
		return max;
	}

	const char* GetCurrentPtr () const
	{
		return p;
	}
};


class CTreeditorLex
{
public:
	static const STreeditorKeyword treeKeywords[];
	static const unsigned int treeKeywordsLength;

protected:
	CTreeditorInput *input;
	unsigned char lastChar;
	bool isNotEof;
	const STreeditorKeyword *keywords;
	const unsigned int keywordsLength;

public:
	CTreeditorLex (CTreeditorInput *in, const STreeditorKeyword *keys, 
		const unsigned int keysLength)
		: input (in), isNotEof (true), keywords (keys), keywordsLength (keysLength)
	{
		isNotEof = in->Read (&lastChar, 1) == 1;
	}

	void GetNextExpressionToken (STreeditorTokenDesc *token, bool skipWhiteSpaces = true)
	{
		if (isNotEof)
		{
			if (skipWhiteSpaces)
			{
				SkipWhiteSpaces ();
			}
			if (isNotEof)
			{
				char *p;

				switch (lastChar)
				{
				case ' ':
				case '\t':
				case '\r':
				case '\n':
				case '{':
				case '}':
				case '=':
				case '[':
				case ']':
				case ',':
				case ':':
				case '+':
				case '-':
				case '*':
				case '/':
				case '(':
				case ')':
				case ';':
				case '!':
					token->string[0] = token->type = lastChar;
					token->string[1] = '\0';
					isNotEof = input->Read (&lastChar, 1) == 1;
					return;
					
				case '.':
					ParseNumber (token, 0);
					return;

				case '"':
					token->type = treeditorLexString;
					isNotEof = input->Read (&lastChar, 1) == 1;
					p = &token->string[0];
					for (;;)
					{
						while (isNotEof && lastChar != '"')
						{
							*p++ = lastChar;
							isNotEof = input->Read (&lastChar, 1) == 1;
						}
						isNotEof = input->Read (&lastChar, 1) == 1;
						if (!isNotEof || lastChar != '"')
						{
							break;
						}
						*p++ = '"';
						*p++ = '"';
						isNotEof = input->Read (&lastChar, 1) == 1;
					};
					*p = 0x00;
					return;
					
				default:
					if (lastChar >= '0' && lastChar <= '9')
					{
						ParseNumber (token, 0);
						return;
					}
					
					if ((lastChar >= 'a' && lastChar <= 'z') || 
						(lastChar >= 'A' && lastChar <= 'Z') ||
						lastChar == '_')
					{
						p = &token->string[0];
						*p++ = lastChar;
						isNotEof = input->Read (&lastChar, 1) == 1;
						while (isNotEof && ((lastChar >= 'a' && lastChar <= 'z') ||
							(lastChar >= 'A' && lastChar <= 'Z') ||
							(lastChar >= '0' && lastChar <= '9') ||
							lastChar == '_'))
						{
							*p++ = lastChar;
							isNotEof = input->Read (&lastChar, 1) == 1;
						}
						*p = 0x00;
						FindKeyword (token);
						return;
					}

					isNotEof = false;
					token->type = treeditorLexEof;
					return;
				}
			}
		}

		token->type = treeditorLexEof;
		return;
	}

	void GetNextToken (STreeditorTokenDesc *token, bool skipWhiteSpaces = true)
	{
		if (isNotEof)
		{
			if (skipWhiteSpaces)
			{
				SkipWhiteSpaces ();
			}
			if (isNotEof)
			{
				char *p;

				switch (lastChar)
				{
				case ' ':
				case '\t':
				case '\r':
				case '\n':
				case '{':
				case '}':
				case '=':
				case '[':
				case ']':
				case ',':
				case ':':
				case '!':
					token->string[0] = token->type = lastChar;
					token->string[1] = '\0';
					isNotEof = input->Read (&lastChar, 1) == 1;
					return;

				case ';':
					token->string[0] = token->type = lastChar;
					token->string[1] = '\0';
					SkipToNextLine ();
					return;
					
				case '+':
					token->string[0] = lastChar;
					token->string[1] = '\0';
					isNotEof = input->Read (&lastChar, 1) == 1;
					SkipWhiteSpaces ();
					if ((lastChar >= '0' && lastChar <= '9') || lastChar == '.')
					{
						ParseNumber (token, 1);
						return;
					}
					isNotEof = false;
					token->type = treeditorLexEof;
					return;
					
				case '-':
					token->string[0] = lastChar;
					token->string[1] = '\0';
					isNotEof = input->Read (&lastChar, 1) == 1;
					SkipWhiteSpaces ();
					if ((lastChar >= '0' && lastChar <= '9') || lastChar == '.')
					{
						ParseNumber (token, 1);
						token->number = -token->number;
						token->fnumber = -token->fnumber;
						return;
					}
					isNotEof = false;
					token->type = treeditorLexEof;
					return;
					
				case '.':
					ParseNumber (token, 0);
					return;

				case '"':
					token->type = treeditorLexString;
					isNotEof = input->Read (&lastChar, 1) == 1;
					p = &token->string[0];
					for (;;)
					{
						while (isNotEof && lastChar != '"')
						{
							*p++ = lastChar;
							isNotEof = input->Read (&lastChar, 1) == 1;
						}
						isNotEof = input->Read (&lastChar, 1) == 1;
						if (!isNotEof || lastChar != '"')
						{
							break;
						}
						*p++ = '"';
						*p++ = '"';
						isNotEof = input->Read (&lastChar, 1) == 1;
					};
					*p = 0x00;
					return;
					
				default:
					if (lastChar >= '0' && lastChar <= '9')
					{
						ParseNumber (token, 0);
						return;
					}
					
					if ((lastChar >= 'a' && lastChar <= 'z') || 
						(lastChar >= 'A' && lastChar <= 'Z') ||
						lastChar == '_')
					{
						p = &token->string[0];
						*p++ = lastChar;
						isNotEof = input->Read (&lastChar, 1) == 1;
						while (isNotEof && ((lastChar >= 'a' && lastChar <= 'z') ||
							(lastChar >= 'A' && lastChar <= 'Z') ||
							(lastChar >= '0' && lastChar <= '9') ||
							lastChar == '_'))
						{
							*p++ = lastChar;
							isNotEof = input->Read (&lastChar, 1) == 1;
						}
						*p = 0x00;
						FindKeyword (token);
						return;
					}

					isNotEof = false;
					token->type = treeditorLexEof;
					return;
				}
			}
		}

		token->type = treeditorLexEof;
		return;
	}

protected:
	void FindKeyword (STreeditorTokenDesc *token)
	{
		const STreeditorKeyword *key = keywords;
		for (int i = keywordsLength; i > 0; --i)
		{
			if (0 == strcmp (key->token, &token->string[0]))
			{
				token->type = key->id;
				return;
			}

			++key;
		}

		token->type = treeditorLexId;
	}

	inline void SkipWhiteSpaces ()
	{
		while (isNotEof && (lastChar == ' ' || lastChar == '\n' || lastChar == '\r' || 
			lastChar == '\t'))
		{
			isNotEof = input->Read (&lastChar, 1) == 1;
		}
	}

	inline void SkipToNextLine ()
	{
		while (isNotEof && lastChar != '\n')
		{
			isNotEof = input->Read (&lastChar, 1) == 1;
		}

		if (isNotEof)
		{
			isNotEof = input->Read (&lastChar, 1) == 1;
		}
	}

	inline void ParseNumberOctal (STreeditorTokenDesc *token, int stri)
	{
		while (isNotEof && lastChar >= '0' && lastChar <= '9')
		{
			token->string[stri++] = lastChar;
			token->fnumber *= 10.0f;
			token->fnumber += (float) (lastChar - '0');
			isNotEof = input->Read (&lastChar, 1) == 1;
		}

		token->number = (long) token->fnumber;
		token->string[stri] = '\0';
		token->type = treeditorLexLong;
		return;
	}

	inline void ParseNumberHex (STreeditorTokenDesc *token, int stri)
	{
		token->fnumber = 0.f;
		token->string[stri++] = lastChar;
		isNotEof = input->Read (&lastChar, 1) == 1;

		if (isNotEof)
		{
			if (lastChar == '.')
			{
				ParseNumberFloat (token, stri);
				return;
			}
			if (lastChar != 'x')
			{
				ParseNumberOctal (token, stri);
				return;
			}
		}

		token->string[stri++] = lastChar;
		isNotEof = input->Read (&lastChar, 1) == 1;

		while (isNotEof && (
			lastChar >= '0' && lastChar <= '9' ||
			lastChar >= 'a' && lastChar <= 'f' ||
			lastChar >= 'A' && lastChar <= 'F'))
		{
			token->string[stri++] = lastChar;
			token->fnumber *= 16.0f;
			if (lastChar >= '0' && lastChar <= '9')
			{
				token->fnumber += (float) (lastChar - '0');
			}
			else if (lastChar >= 'a' && lastChar <= 'f')
			{
				token->fnumber += (float) (lastChar - 'a') + 10.f;
			}
			else if (lastChar >= 'A' && lastChar <= 'F')
			{
				token->fnumber += (float) (lastChar - 'A') + 10.f;
			}
			isNotEof = input->Read (&lastChar, 1) == 1;
		}

		token->number = (long) token->fnumber;
		token->string[stri] = '\0';
		token->type = treeditorLexLong;
	}

	inline void ParseNumber (STreeditorTokenDesc *token, int stri)
	{
		if (isNotEof && lastChar == '0')
		{
			ParseNumberHex (token, stri);
			return;
		}

		ParseNumberFloat (token, stri);
	}
	
	inline void ParseNumberFloat (STreeditorTokenDesc *token, int stri)
	{
		token->fnumber = 0.f;

		while (isNotEof && lastChar >= '0' && lastChar <= '9')
		{
			token->string[stri++] = lastChar;
			token->fnumber *= 10.0f;
			token->fnumber += (float) (lastChar - '0');
			isNotEof = input->Read (&lastChar, 1) == 1;
		}
		token->number = (long) token->fnumber;

		if (!isNotEof || (lastChar != '.' && lastChar != 'e' && lastChar != 'E'))
		{
			token->string[stri] = '\0';
			token->type = treeditorLexLong;
			return;
		}

		token->number <<= 16;
		token->type = treeditorLexFixedFloat;

		if (lastChar == '.')
		{
			token->string[stri++] = lastChar;
			isNotEof = input->Read (&lastChar, 1) == 1;
			double part = 0.0;
			double exp = 1.0;
			while (isNotEof && lastChar >= '0' && lastChar <= '9')
			{
				token->string[stri++] = lastChar;
				part *= 10.0;
				part += lastChar - '0';
				exp *= 10.0;
				isNotEof = input->Read (&lastChar, 1) == 1;
			}
			
			token->fnumber += (float) (part / exp);
			CalcDecimalPart (token, part, exp);
		}

		if (isNotEof && (lastChar == 'e' || lastChar == 'E'))
		{
			token->string[stri++] = lastChar;
			isNotEof = input->Read (&lastChar, 1) == 1;
			if (isNotEof)
			{
				int sign;
				switch (lastChar)
				{
				case '-':
					sign = -1;
					break;
					
				default:
					sign = +1;
					break;
				}
				
				token->string[stri++] = lastChar;
				isNotEof = input->Read (&lastChar, 1) == 1;
				int exp = 0;
				while (isNotEof && lastChar >= '0' && lastChar <= '9')
				{
					token->string[stri++] = lastChar;
					exp *= 10;
					exp += lastChar - '0';
					isNotEof = input->Read (&lastChar, 1) == 1;
				}
				
				token->fnumber *= (float) pow (10.0, sign * exp);
			}
		}

		token->string[stri] = '\0';
	}

	inline void CalcDecimalPart (STreeditorTokenDesc *token, double num, double exp)
	{
		long bit = 0x10000;
		int i = 16;

		while (num > 0.0 && i > 0)
		{
			num *= 2.0;
			bit >>= 1;
			if (num >= exp)
			{
				token->number |= bit;
				num -= exp;
			}
			--i;
		}
	}
};


#endif // #ifndef _TREEDITORLEX_H_
