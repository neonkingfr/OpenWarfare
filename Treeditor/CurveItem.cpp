// CurveItem.cpp : implementation file
//

#include "stdafx.h"
#include <El/elementpch.hpp>
#include "RV/Tree/mytree.h"
#include "Treeditor.h"
#include "TreeditorLex.h"
#include "CurveDlg.h"
#include "CurveItem.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define LEFT_HALF (MARK_SIZE >> 1)
#define RIGHT_HALF ((MARK_SIZE >> 1) + 1)
#define CURVE_COLOR (RGB (0, 64, 128))

/////////////////////////////////////////////////////////////////////////////
// CCurveItem

CCurveItem::CCurveItem()
: _dlg (NULL), _draggedMark (-1), _mouseMoveMode (_mmmNone)
{
}

CCurveItem::~CCurveItem()
{
}


BEGIN_MESSAGE_MAP(CCurveItem, CButton)
	//{{AFX_MSG_MAP(CCurveItem)
	ON_WM_LBUTTONDBLCLK()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_PAINT()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_SIZE()
	ON_WM_MBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCurveItem message handlers

void CCurveItem::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
}

void CCurveItem::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default

	_mouseMoveMode = _mmmNone;

	CRect r;
	CalcCurveRect(r);
	_draggedMark = FindMark (point, r, true);

	Invalidate(FALSE);
	UpdateWindow();
	_dlg->UpdateCurveX ();
	_dlg->UpdateCurveValue ();
	_dlg->UpdateCurveTangent ();

	if (_draggedMark != -1)
	{
		int n = _dlg->GetMarkSize ();

#define INIT_REF_VALUES   \
	_mmmRefValues.RemoveAll ();\
	for (int i = 0; i < n; ++i)\
	{\
		_mmmRefValues.Add (_dlg->GetMarkValue (i));\
	}

		if ((nFlags & MK_SHIFT) != 0)
		{
			if ((nFlags & MK_CONTROL) != 0)
			{
				if (_draggedMark > 0 && _draggedMark < n - 1)
				{
					_mouseMoveMode = _mmmMoveX;
				}
			}
			else
			{
				INIT_REF_VALUES
				_mouseMoveMode = _mmmShift;
			}
		}
		else if ((nFlags & MK_CONTROL) != 0)
		{
			INIT_REF_VALUES
			_mouseMoveMode = _mmmScale;
		}

#undef INIT_REF_VALUES

	}
}

void CCurveItem::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default

	_mouseMoveMode = _mmmNone;
}

void CCurveItem::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default

	if (_draggedMark != -1 && (nFlags & MK_LBUTTON) != 0)
	{
		CRect r;
		CalcCurveRect (r);
		if (_mouseMoveMode == _mmmMoveX)
		{
			float x;
			if (_dlg->_horizontalX)
			{
				x = (float) (point.x - r.left) / (float) (r.right - 1 - r.left);
			}
			else
			{
				x = (float) (r.bottom - 1 - point.y) / (float) (r.bottom - 1 - r.top);
			}
			if (x < 0.0f)
			{
				x = 0.0f;
			}
			else if (x > 1.0f)
			{
				x = 1.0f;
			}
			_dlg->SetMarkX(_draggedMark, x);
		}
		else
		{
			float y;
			if (_dlg->_horizontalX)
			{
				y = _dlg->_min + (_dlg->_max - _dlg->_min) * (float) (r.bottom - 1 - point.y) / (float) (r.bottom - 1 - r.top);
			}
			else
			{
				y = _dlg->_min + (_dlg->_max - _dlg->_min) * (float) (point.x - r.left) / (float) (r.right - 1 - r.left);
			}
			if (y < _dlg->_min)
			{
				y = _dlg->_min;
			}
			else if (y > _dlg->_max)
			{
				y = _dlg->_max;
			}
			_dlg->SetMarkValue(_draggedMark, y);

			switch (_mouseMoveMode)
			{
			case _mmmScale:
				{
					float scaleValue = _mmmRefValues[_draggedMark] - _dlg->_min;
					if (scaleValue > 0.0f)
					{
						y -= _dlg->_min;
						int n = _dlg->GetMarkSize ();
						for (int i = 0; i < n; ++i)
						{
							if (i != _draggedMark)
							{
								_dlg->SetMarkValue (i, (_mmmRefValues[i] - _dlg->_min) * y / scaleValue + _dlg->_min);
							}
						}
					}
				}
				break;

			case _mmmShift:
				{
					float shiftValue = _mmmRefValues[_draggedMark];
					int n = _dlg->GetMarkSize ();
					for (int i = 0; i < n; ++i)
					{
						if (i != _draggedMark)
						{
							_dlg->SetMarkValue (i, _mmmRefValues[i] + y - shiftValue);
						}
					}
				}
				break;
			}
		}

		_dlg->UpdateCurveX ();
		_dlg->UpdateCurveValue ();
		_dlg->UpdateCurveTangent ();
		_dlg->CurveChanged();
	}
}

void CCurveItem::CalcCurveRect (CRect &r)
{
	GetClientRect(&r);
	r.DeflateRect (2 + (MARK_SIZE >> 1), 2 + (MARK_SIZE >> 1));
	r.left += AXIS_FONT_SIZE + 3;
	r.bottom -= AXIS_FONT_SIZE + 3;
}

void CCurveItem::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here
	
	// Do not call CButton::OnPaint() for painting messages

	CRect r;
	GetClientRect (&r);
	dc.FillSolidRect (&r, RGB (255, 255, 255));
	dc.DrawEdge (&r, EDGE_SUNKEN, BF_RECT);
	if (_dlg != NULL)
	{
		r.DeflateRect(2, 2);
		dc.IntersectClipRect (&r);
		
		CRect br = r;
		CalcCurveRect (r);

		CPen pen (PS_SOLID, 1, RGB (128, 128, 128));
		CPen *pen0 = dc.SelectObject(&pen);
		dc.MoveTo (br.left, r.bottom - 1);
		dc.LineTo (br.right, r.bottom - 1);
		dc.MoveTo (r.left, br.top);
		dc.LineTo (r.left, br.bottom);
		dc.SelectObject(pen0);

		CFont font;
		if (font.CreatePointFont(AXIS_FONT_SIZE * 10, _T ("Tahoma"), &dc))
		{
			CFont *font0 = dc.SelectObject(&font);
			LPCTSTR vDescr, hDescr;
			if (_dlg->_horizontalX)
			{
				hDescr = _dlg->_xDescr;
				vDescr = _dlg->_yDescr;
			}
			else
			{
				hDescr = _dlg->_yDescr;
				vDescr = _dlg->_xDescr;
			}
			UINT align0 = dc.SetTextAlign(TA_LEFT);
			dc.TextOut(br.left + 4, br.top, vDescr);
			dc.SetTextAlign(TA_RIGHT);
			dc.TextOut(br.right - 5, r.bottom, hDescr);
			dc.SelectObject(font0);
			dc.SetTextAlign(align0);
		}

		switch (_dlg->_curveType)
		{
		case CCurveDlg::LINEAR_CURVE:
		case CCurveDlg::LINEAR_CURVE_WITH_X:
			PaintLinearCurve (dc, r);
			break;
			
		case CCurveDlg::QUADRATIC_CURVE:
		case CCurveDlg::QUADRATIC_CURVE_WITH_X:
			PaintQuadraticCurve (dc, r);
			break;
			
		case CCurveDlg::EX_QUADRATIC_CURVE:
		case CCurveDlg::EX_QUADRATIC_CURVE_WITH_X:
			PaintExQuadraticCurve (dc, r);
			break;
		}
	}
}

void CCurveItem::PaintLinearCurve (CDC &dc, CRect &r)
{
	CPen pen (PS_SOLID, 1, CURVE_COLOR);
	CPen *oldPen = dc.SelectObject (&pen);
	int n = _dlg->GetMarkSize ();
	float lastX = _dlg->GetMarkX (0);
	float lastY = _dlg->GetMarkValue(0);
	int lastKey = 0;
	int i;
	for (i = 1; i < n; ++i)
	{
		if (_dlg->GetMarkType(i) > 0)
		{
			float y = _dlg->GetMarkValue(i);
			float x = _dlg->GetMarkX (i);
			PaintLineSegment(dc, r, lastX, lastY, x, y);

			if (i > lastKey + 1)
			{
				for (int ti = lastKey + 1; ti < i; ++ti)
				{
					float cx = _dlg->GetMarkX (ti);
					_dlg->SetMarkValue(ti, lastY + (y - lastY) * (cx - lastX) / (x - lastX));
				}
			}

			lastX = x;
			lastY = y;
			lastKey = i;
		}
	}
	if (i > lastKey + 1)
	{
		PaintLineSegment(dc, r, lastX, lastY, 1.0f, lastY);
		
		for (int ti = lastKey + 1; ti < i; ++ti)
		{
			_dlg->SetMarkValue(ti, lastY);
		}
	}
	dc.SelectObject (oldPen);

	for (i = 0; i < n; ++i)
	{
		PaintMark(dc, r, _dlg->GetMarkX(i), _dlg->GetMarkValue(i), _dlg->GetMarkType(i), i);
	}
}

void CCurveItem::CalcMarkPoint (float x, float y, CPoint &p, CRect &r)
{
	if (_dlg->_horizontalX)
	{
		p.x = r.left + x * (r.right - 1 - r.left);
		p.y = r.bottom - 1 - (y - _dlg->_min) * (r.bottom - 1 - r.top) / (_dlg->_max - _dlg->_min);
	}
	else
	{
		p.x = r.left + (y - _dlg->_min) * (r.right - 1 - r.left) / (_dlg->_max - _dlg->_min);
		p.y = r.bottom - 1 - x * (r.bottom - 1 - r.top);
	}
}

void CCurveItem::PaintMark (CDC &dc, CPoint &p, CPen &pen)
{
	CPen *oldPen = dc.SelectObject (&pen);
	dc.MoveTo(p.x - LEFT_HALF, p.y);
	dc.LineTo(p.x + RIGHT_HALF, p.y);
	dc.MoveTo(p.x, p.y - LEFT_HALF);
	dc.LineTo(p.x, p.y + RIGHT_HALF);
	for (int i = 1; i <= (MARK_WIDTH >> 1); ++i)
	{
		dc.MoveTo(p.x - LEFT_HALF, p.y + i);
		dc.LineTo(p.x + RIGHT_HALF, p.y + i);
		dc.MoveTo(p.x - LEFT_HALF, p.y - i);
		dc.LineTo(p.x + RIGHT_HALF, p.y - i);

		dc.MoveTo(p.x + i, p.y - LEFT_HALF);
		dc.LineTo(p.x + i, p.y + RIGHT_HALF);
		dc.MoveTo(p.x - i, p.y - LEFT_HALF);
		dc.LineTo(p.x - i, p.y + RIGHT_HALF);
	}
	dc.SelectObject (oldPen);
}

void CCurveItem::PaintMark(CDC &dc, CRect &r, float x, float y, int keyType, int index)
{
	CPoint p;
	CalcMarkPoint(x, y, p, r);

	switch (keyType)
	{
	case 0:
		{
			CPen pen (PS_SOLID, 1, RGB (192, 64, 64));
			PaintMark(dc, p, pen);
		}
		break;

	case 1:
		{
			CPen pen (PS_SOLID, 1, RGB (64, 192, 64));
			PaintMark(dc, p, pen);
		}
		break;

	case 2:
		{
			CPen pen (PS_SOLID, 1, RGB (64, 64, 192));
			PaintMark(dc, p, pen);
		}
		break;
	}
	if (index == _draggedMark)
	{
		RECT r = {p.x - LEFT_HALF - 1, p.y - LEFT_HALF - 1,
		p.x + RIGHT_HALF + 1, p.y + RIGHT_HALF + 1};
		CBrush br (RGB (160, 160, 160));
		dc.FrameRect(&r, &br);
	}
	CFont font;
	if (font.CreatePointFont(80, _T ("Tahoma"), &dc))
	{
		CFont *o = dc.SelectObject(&font);
		if (index == _dlg->GetMarkSize() - 1)
		{
			dc.TextOut(p.x + RIGHT_HALF, p.y - LEFT_HALF - 8, _dlg->GetMarkName(index));
			dc.SelectObject(o);
		}
		else
		{
			dc.TextOut(p.x + RIGHT_HALF, p.y + RIGHT_HALF, _dlg->GetMarkName(index));
			dc.SelectObject(o);
		}
	}
}

void CCurveItem::PaintQuadraticCurve (CDC &dc, CRect &r)
{
	CPen pen (PS_SOLID, 1, CURVE_COLOR);
	CPen *oldPen = dc.SelectObject (&pen);
	int n = _dlg->GetMarkSize ();
	int i1 = 0;
	float x1 = _dlg->GetMarkX (0);
	float y1 = _dlg->GetMarkValue(0);
	float x2;
	float y2;
	float b = _dlg->GetMarkTangent(0);
	for (int i2 = 1; i2 < n; ++i2)
	{
		if (_dlg->GetMarkType(i2) > 0)
		{
			x2 = _dlg->GetMarkX (i2);
			y2 = _dlg->GetMarkValue(i2);
			
			float dx = x2 - x1;
			float a = ((y2 - y1) - b * dx) / (dx * dx);
			PaintCurveSegment(dc, r, x1, y1, x2, y2, a, b, dx);

			if (i2 - i1 > 1)
			{
				for (int ti = i1 + 1; ti < i2; ++ti)
				{
					float x = _dlg->GetMarkX (ti) - x1;
					_dlg->SetMarkValue(ti, y1 + a * x * x + b * x);
				}
			}

			b += 2 * a * dx;
			x1 = x2;
			y1 = y2;
			i1 = i2;
		}
	}
	dc.SelectObject (oldPen);

	for (i1 = 0; i1 < n; ++i1)
	{
		PaintMark(dc, r, _dlg->GetMarkX (i1), _dlg->GetMarkValue(i1), _dlg->GetMarkType (i1), i1);
	}
}

void CCurveItem::OnRButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	CRect r;
	CalcCurveRect(r);
	int mark = FindMark (point, r, false);
	switch (_dlg->_curveType)
	{
	case CCurveDlg::LINEAR_CURVE:
	case CCurveDlg::LINEAR_CURVE_WITH_X:
	case CCurveDlg::EX_QUADRATIC_CURVE:
	case CCurveDlg::EX_QUADRATIC_CURVE_WITH_X:
	case CCurveDlg::QUADRATIC_CURVE:
	case CCurveDlg::QUADRATIC_CURVE_WITH_X:
		if (mark <= 0 || mark >= _dlg->GetMarkSize () - 1)
		{
			return;
		}
		break;

	default:
		return;
	}

	if (_dlg->ChangeMarkType(mark) > 0)
	{
		_draggedMark = mark;
	}
	else if (_draggedMark >= 0 && _dlg->GetMarkType(_draggedMark) == 0)
	{
		_draggedMark = 0;
	}

	_dlg->UpdateCurveX ();
	_dlg->UpdateCurveValue ();
	_dlg->UpdateCurveTangent ();
	_dlg->CurveChanged();
}

int CCurveItem::FindMark (CPoint &p, CRect &r, bool onlyKeys)
{
	int n = _dlg->GetMarkSize();
	float x = _dlg->_horizontalX ? 
		((float) (p.x - r.left) / (float) (r.right - 1 - r.left)) :
		((float) (r.bottom - 1 - p.y) / (float) (r.bottom - 1 - r.top));
	float y = _dlg->_horizontalX ? 
		((_dlg->_max - _dlg->_min) * (float) (r.bottom - 1 - p.y) / (float) (r.bottom - 1 - r.top)) + _dlg->_min :
		((_dlg->_max - _dlg->_min) * (float) (p.x - r.left) / (float) (r.right - 1 - r.left)) + _dlg->_min;

	float dx = (_dlg->GetMarkX (0) - x);
	float dy = (_dlg->GetMarkValue (0) - y);
	float d = dx * dx + dy * dy;
	int res = 0;
	for (int i = 1; i < n; ++i)
	{
		dx = (_dlg->GetMarkX (i) - x);
		dy = (_dlg->GetMarkValue (i) - y);
		float td = dx * dx + dy * dy;
		if (td < d)
		{
			d = td;
			res = i;
		}
	}

	return (!onlyKeys || _dlg->GetMarkType(res) != 0) ? res : -1;
}

int CCurveItem::FindLeftMark (CPoint &p, CRect &r, bool onlyKeys)
{
	int n = _dlg->GetMarkSize();
	float x = _dlg->_horizontalX ? 
		((float) (p.x - r.left) / (float) (r.right - 1 - r.left)) :
		((float) (r.bottom - 1 - p.y) / (float) (r.bottom - 1 - r.top));
	float y = _dlg->_horizontalX ? 
		((_dlg->_max - _dlg->_min) * (float) (r.bottom - 1 - p.y) / (float) (r.bottom - 1 - r.top)) + _dlg->_min :
		((_dlg->_max - _dlg->_min) * (float) (p.x - r.left) / (float) (r.right - 1 - r.left)) + _dlg->_min;

	float d = FLT_MAX;
	int res = -1;
	for (int i = 0; i < n; ++i)
	{
		float dx = (x - _dlg->GetMarkX (i));
		float dy = (y - _dlg->GetMarkValue (i));
		float td = dx * dx + dy * dy;
		if (dx < 0.0f)
		{
			break;
		}
		if (td < d)
		{
			d = td;
			res = i;
		}
	}

	return (!onlyKeys || _dlg->GetMarkType(res) != 0) ? res : -1;
}

void CCurveItem::OnRButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
}

void CCurveItem::OnSize(UINT nType, int cx, int cy) 
{
	CButton::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	
}

void CCurveItem::OnMButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default

	if (_dlg->_varPointNumber)
	{
		CRect r;
		CalcCurveRect(r);
		if ((nFlags & MK_CONTROL) == 0)
		{
			int mark = FindLeftMark (point, r, false);
			++mark;
			if (mark > 0 && mark < _dlg->GetMarkSize())
			{
				float x = (_dlg->GetMarkX (mark - 1) + _dlg->GetMarkX (mark)) / 2.0f;
				_dlg->InsertMarkAt(mark, x, _dlg->GetMarkValue (mark - 1), _dlg->GetMarkTangent (mark - 1), 0, _T (""));
			}
		}
		else
		{
			int mark = FindMark (point, r, false);
			if (mark > 0 && mark < _dlg->GetMarkSize() - 1)
			{
				_dlg->RemoveMarkAt(mark);
			}
		}
		_dlg->UpdateCurveX ();
		_dlg->UpdateCurveValue ();
		_dlg->UpdateCurveTangent ();
		_dlg->CurveChanged();
	}
}

void CCurveItem::PaintExQuadraticCurve (CDC &dc, CRect &r)
{
	CPen pen (PS_SOLID, 1, CURVE_COLOR);
	CPen *oldPen = dc.SelectObject (&pen);
	int n = _dlg->GetMarkSize ();
	int i1 = 0;
	float x1 = _dlg->GetMarkX (0);
	float y1 = _dlg->GetMarkValue(0);
	float x2;
	float y2;
	float b = _dlg->GetMarkTangent(0);
	for (int i2 = 1; i2 < n; ++i2)
	{
		int mt = _dlg->GetMarkType(i2);
		if (mt > 1)
		{
			x2 = _dlg->GetMarkX (i2);
			y2 = _dlg->GetMarkValue(i2);

			// int i = i2 - i1;
			float dx = x2 - x1;
			float a = ((y2 - y1) - b * dx) / (dx * dx);
			PaintCurveSegment(dc, r, x1, y1, x2, y2, a, b, dx);

			if (i2 - i1 > 1)
			{
				for (int ti = i1 + 1; ti < i2; ++ti)
				{
					// float x = (float) (ti - i1);
					float x = _dlg->GetMarkX (ti) - x1;
					_dlg->SetMarkValue(ti, y1 + a * x * x + b * x);
					_dlg->SetMarkTangent(ti, b + 2 * a * x);
				}
			}

			b = _dlg->GetMarkTangent(i2);
			x1 = x2;
			y1 = y2;
			i1 = i2;
		}
		else if (mt > 0)
		{
			x2 = _dlg->GetMarkX (i2);
			y2 = _dlg->GetMarkValue(i2);

			float dx = x2 - x1;
			float a = ((y2 - y1) - b * dx) / (dx * dx);
			PaintCurveSegment(dc, r, x1, y1, x2, y2, a, b, dx);

			if (i2 - i1 > 1)
			{
				for (int ti = i1 + 1; ti < i2; ++ti)
				{
					float x = _dlg->GetMarkX (ti) - x1;
					_dlg->SetMarkValue(ti, y1 + a * x * x + b * x);
					_dlg->SetMarkTangent(ti, b + 2 * a * x);
				}
			}

			b += 2 * a * dx;
			_dlg->SetMarkTangent(i2, b);
			x1 = x2;
			y1 = y2;
			i1 = i2;
		}
	}
	dc.SelectObject (oldPen);

	for (i1 = 0; i1 < n; ++i1)
	{
		PaintMark(dc, r, _dlg->GetMarkX (i1), _dlg->GetMarkValue(i1), _dlg->GetMarkType(i1), i1);
	}
}

void CCurveItem::PaintLineSegment (CDC &dc, CRect &r, float x1, float y1, float x2, float y2)
{
	CPoint p;

	CalcMarkPoint (x1, y1, p, r);
	dc.MoveTo (p.x, p.y);
	
	CalcMarkPoint (x2, y2, p, r);
	dc.LineTo (p.x, p.y);
}

void CCurveItem::PaintCurveSegment (CDC &dc, CRect &r, float x1, float y1, float x2, float y2, float a, float b, float range)
{
	CPoint p1, p2, cp;
	CalcMarkPoint(x1, y1, p1, r);
	CalcMarkPoint(x2, y2, p2, r);

	int count;
	if (_dlg->_horizontalX)
	{
		count = p2.x - p1.x;
	}
	else
	{
		count = p1.y - p2.y;
	}

	for (int pt = 0; pt < count;)
	{
		++pt;
		float xx = range * (float) pt / (float) count;
		float yy = y1 + a * xx * xx + b * xx;
		if (yy < _dlg->_min)
		{
			yy = _dlg->_min;
		}
		else if (yy > _dlg->_max)
		{
			yy = _dlg->_max;
		}
		CalcMarkPoint(x1 + (x2 - x1) * (float) pt / (float) count, yy, cp, r);

		dc.MoveTo (p1);
		dc.LineTo (cp);
		dc.SetPixelV (cp, CURVE_COLOR);

		p1 = cp;
	}
}
