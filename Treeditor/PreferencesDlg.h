#if !defined(AFX_PREFERENCESDLG_H__6C8DC8E2_919E_485C_8B35_DB4EEA223646__INCLUDED_)
#define AFX_PREFERENCESDLG_H__6C8DC8E2_919E_485C_8B35_DB4EEA223646__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PreferencesDlg.h : header file
//

#include "ColorItem.h"

/////////////////////////////////////////////////////////////////////////////
// CPreferencesDlg dialog

class CPreferencesDlg : public CDialog
{
	class StringColorAssoc
	{
	public:
		CString _name;
		COLORREF _color;
	};

	class IntColorAssoc
	{
	public:
		int _id;
		COLORREF _color;
	};

	class EditorAssoc
	{
	public:
		CString _extension;
		CString _editor;
	};

public:
	float _ground_height;
	float _ground_width;

	CArray<EditorAssoc, EditorAssoc&> _externalEditors;
	CArray<StringColorAssoc, StringColorAssoc&> _scriptChildColors;
	CArray<IntColorAssoc, IntColorAssoc&> _childColors;
	CArray<IntColorAssoc, IntColorAssoc&> _componentColors;

	DWORD _scriptChildDefColor;

// Construction
public:
	CPreferencesDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPreferencesDlg)
	enum { IDD = IDD_PREFERENCESDLG };
	CEdit	_scriptChildCtrl;
	CEdit	_exEditorCtrl;
	CEdit	_extensionCtrl;
	CListBox	_scriptChildList;
	CComboBox	_childCombo;
	CComboBox	_componentCombo;
	CListBox	_exEditorList;
	CColorItem	_componentColorCtrl;
	CColorItem	_childColorCtrl;
	CColorItem	_scriptChildDefColorCtrl;
	CColorItem	_scriptChildColorCtrl;
	CEdit	_gtWidth;
	CEdit	_gtHeight;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPreferencesDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CPreferencesDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnSelChangeExEditorList();
	afx_msg void OnSelChangeSChildList();
	afx_msg void OnSelChangeChildCombo();
	afx_msg void OnSelChangeComponentCombo();
	afx_msg void OnAddChangeExEditor();
	afx_msg void OnRemoveExEditor();
	afx_msg void OnAddChangeSChild();
	afx_msg void OnRemoveSChild();
	afx_msg void OnSChildColor();
	afx_msg void OnSChildDefColor();
	afx_msg void OnChildColor();
	afx_msg void OnComponentColor();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	bool OpenColorDialog (COLORREF &color);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PREFERENCESDLG_H__6C8DC8E2_919E_485C_8B35_DB4EEA223646__INCLUDED_)
