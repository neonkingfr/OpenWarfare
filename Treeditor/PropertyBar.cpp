// PropertyBar.cpp: implementation of the CPropertyBar class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <El/elementpch.hpp>
#include "RV/Tree/mytree.h"
#include "treeditor.h"
#include "PropertyBar.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define SPACE1 4
#define SPACE2 7

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPropertyBar::CPropertyBar()
{

}

CPropertyBar::~CPropertyBar()
{

}

CSize CPropertyBar::CalcDimension (int nLength, DWORD dwMode)
{
	// Return default if it is being docked or floated
	if ((dwMode & LM_VERTDOCK) || (dwMode & LM_HORZDOCK))
	{
		if (dwMode & LM_STRETCH) // if not docked stretch to fit
			return CSize((dwMode & LM_HORZ) ? 32767 : m_sizeDocked.cx,
			(dwMode & LM_HORZ) ? m_sizeDocked.cy : 32767);
		else
			return m_sizeDocked;
	}
	if (dwMode & LM_MRUWIDTH)
		return m_sizeFloating;
	// In all other cases, accept the dynamic length
	if (dwMode & LM_LENGTHY)
	{
		//nLength += (FLOAT_BAR_VER_ALIGN - 1);
		//nLength -= nLength % FLOAT_BAR_VER_ALIGN;
		return CSize(m_sizeFloating.cx, (m_bChangeDockedSize) ?
		m_sizeFloating.cy = m_sizeDocked.cy = nLength :
		m_sizeFloating.cy = nLength);
	}
	else
	{
		//nLength += (FLOAT_BAR_HOR_ALIGN - 1);
		//nLength -= nLength % FLOAT_BAR_HOR_ALIGN;
		return CSize((m_bChangeDockedSize) ?
		m_sizeFloating.cx = m_sizeDocked.cx = nLength :
		m_sizeFloating.cx = nLength, m_sizeFloating.cy);
	}
}

CSize CPropertyBar::CalcDynamicLayout(int nLength, DWORD dwMode)
{
	CSize dim = CalcDimension (nLength, dwMode);

#define CALC_CTRL_RECT(id, ctrl, r) CWnd *ctrl = GetDlgItem (id); if (ctrl == NULL) { return dim; } CRect r; ctrl->GetWindowRect(&r); ScreenToClient (&r)

	CALC_CTRL_RECT (IDC_CLASSTYPE, classType, classTypeR);
	CALC_CTRL_RECT (IDC_PROPSFRAME, props, propsR);
	CALC_CTRL_RECT (IDC_PARAMSLIDER, param, paramR);
	CALC_CTRL_RECT (IDC_MINSLIDERVALUE, min, minR);
	CALC_CTRL_RECT (IDC_MAXSLIDERVALUE, max, maxR);

#undef CALC_CTRL_RECT

	int t = dim.cy - (
		SPACE2
		+ classTypeR.bottom - classTypeR.top 
		+ SPACE2 
		+ SPACE2 
		+ paramR.bottom - paramR.top 
		+ minR.bottom - minR.top 
		+ SPACE2);

	classTypeR.SetRect (SPACE2, SPACE2, dim.cx - SPACE2, SPACE2 + classTypeR.bottom - classTypeR.top);
	propsR.SetRect (SPACE2, classTypeR.bottom + SPACE2, dim.cx - SPACE2, classTypeR.bottom + SPACE2 + t);
	paramR.SetRect (SPACE2, propsR.bottom + SPACE2, dim.cx - SPACE2, propsR.bottom + SPACE2 + paramR.bottom - paramR.top);
	minR.SetRect (SPACE2, paramR.bottom, SPACE2 + minR.right - minR.left, dim.cy - SPACE2);
	maxR.SetRect (dim.cx - SPACE2 - (maxR.right - maxR.left), paramR.bottom, dim.cx - SPACE2, dim.cy - SPACE2);

	classType->MoveWindow (&classTypeR, TRUE);
	props->MoveWindow(&propsR, TRUE);
	param->MoveWindow (&paramR, TRUE);
	min->MoveWindow (&minR, TRUE);
	max->MoveWindow (&maxR, TRUE);

	return dim;
}

BOOL CPropertyBar::Create( CWnd* pParentWnd, UINT nIDTemplate,
							  UINT nStyle, UINT nID, CSize *s, BOOL bChange)
{
	if(!CDialogBar::Create(pParentWnd,nIDTemplate,nStyle,nID))
		return FALSE;
	
	m_bChangeDockedSize = bChange;
	if (s != NULL)
	{
		m_sizeDefault = *s;
	}
	m_sizeFloating = m_sizeDocked = m_sizeDefault;

	_propCtrl.SubclassDlgItem (IDC_PROPSFRAME, this);
	return _propCtrl.Init ();
}

BOOL CPropertyBar::Create( CWnd* pParentWnd,
							  LPCTSTR lpszTemplateName, UINT nStyle,
							  UINT nID, BOOL bChange)
{
	if (!CDialogBar::Create( pParentWnd, lpszTemplateName,
		nStyle, nID))
		return FALSE;
	
	m_bChangeDockedSize = bChange;
	m_sizeFloating = m_sizeDocked = m_sizeDefault;
	
	_propCtrl.SubclassDlgItem (IDC_PROPSFRAME, this);
	return _propCtrl.Init ();
}

BOOL CPropertyBar::OnWndMsg( UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult )
{
	switch (message)
	{
	case WM_HSCROLL:
		GetOwner()->SendMessage(message, wParam, lParam);
		break;
	}

	return CDialogBar::OnWndMsg(message, wParam, lParam, pResult);
}

void CPropertyBar::GetOptimalSize (int &w, int &h) const
{
#define CALC_CTRL_RECT(id, ctrl, r) CWnd *ctrl = GetDlgItem (id); if (ctrl == NULL) { h = w = -1; return; } CRect r; ctrl->GetWindowRect(&r); ScreenToClient (&r)

	CALC_CTRL_RECT (IDC_CLASSTYPE, classType, classTypeR);
	CALC_CTRL_RECT (IDC_PROPSFRAME, props, propsR);
	CALC_CTRL_RECT (IDC_PARAMSLIDER, param, paramR);
	CALC_CTRL_RECT (IDC_MINSLIDERVALUE, min, minR);
	CALC_CTRL_RECT (IDC_MAXSLIDERVALUE, max, maxR);

#undef CALC_CTRL_RECT

#define CALC_RECT_HEIGHT(r) (r.bottom - r.top)

	int ow, oh;
	_propCtrl.GetOptimalSize (ow, oh);

	h = SPACE2 + 
		CALC_RECT_HEIGHT (classTypeR) +
		SPACE2 +
		oh +
		SPACE2 +
		CALC_RECT_HEIGHT (paramR) +
		CALC_RECT_HEIGHT (minR) +
		SPACE2;
	w = SPACE2 + ow + SPACE2;

	/*
	h += (FLOAT_BAR_VER_ALIGN - 1);
	h -= h % FLOAT_BAR_VER_ALIGN;
	w += (FLOAT_BAR_HOR_ALIGN - 1);
	w -= w % FLOAT_BAR_HOR_ALIGN;
	*/
}

#undef SPACE1
#undef SPACE2
