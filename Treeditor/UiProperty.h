// UiProperty.h: interface for the CUiProperty class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_UIPROPERTY_H__CC02BAFD_C22C_4EBB_9A86_7FB82A6D6EF0__INCLUDED_)
#define AFX_UIPROPERTY_H__CC02BAFD_C22C_4EBB_9A86_7FB82A6D6EF0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define IDC_PROPSUBVIEW 54999
#define UI_PROPERTY_BASE_ID 55000
enum
{
	UI_PROPERTY_EDIT_BUTTON = 0L,
	UI_PROPERTY_SELECT_BUTTON,
	UI_PROPERTY_BOOL_CHECK,
	UI_PROPERTY_ENUM_COMBO,
	UI_PROPERTY_CHILD_COMBO,
	UI_PROPERTY_VALUE_EDIT,
	UI_PROPERTY_SELECT_CHECK,
	UI_PROPERTY_LAST_ID,
};

enum
{
	USE_PROPERTY_EDIT_BUTTON = (1L << UI_PROPERTY_EDIT_BUTTON),
	USE_PROPERTY_SELECT_BUTTON = (1L << UI_PROPERTY_SELECT_BUTTON),
	USE_PROPERTY_BOOL_CHECK = (1L << UI_PROPERTY_BOOL_CHECK),
	USE_PROPERTY_ENUM_COMBO = (1L << UI_PROPERTY_ENUM_COMBO),
	USE_PROPERTY_CHILD_COMBO = (1L << UI_PROPERTY_CHILD_COMBO),
	USE_PROPERTY_VALUE_EDIT = (1L << UI_PROPERTY_VALUE_EDIT),
	USE_PROPERTY_ALL_UI = ~0L,
};

#define UI_PROPERTY_SPACE 10
#define UI_PROPERTY_HEIGHT 30
#define UI_PROPERTY_CHECK_WIDTH 13
#define UI_PROPERTY_CHECK_HEIGHT 13
#define UI_PROPERTY_LABEL_WIDTH 400
#define UI_PROPERTY_BUTTON_HEIGHT 24
#define UI_PROPERTY_BUTTON_WIDTH 60
#define UI_PROPERTY_COMBO_HEIGHT 24
#define UI_PROPERTY_COMBO_HEIGHT_EX 120
#define UI_PROPERTY_COMBO_WIDTH 90
#define UI_PROPERTY_EDIT_HEIGHT 23
#define UI_PROPERTY_EDIT_WIDTH 150
#define UI_PROPERTY_DEFAULT_COLOR RGB (255, 255, 255)

#include "StaticGround.h"

class CUiPropertySubview;

class CChannelProtocol
{
public:
	virtual CChannelProtocol* Copy () const = 0;

	virtual void OnPropEdit (CUiPropertySubview *props, int idx, long data) = 0;
	virtual void OnPropSelect (CUiPropertySubview *props, int idx, long data) = 0;
	
	virtual void SetPropValue (CUiPropertySubview *props, int idx, long data, LPCTSTR value) = 0;
	virtual LPCTSTR GetPropValue (CUiPropertySubview *props, int idx, long data) const = 0;

	virtual bool SetPropSelect (CUiPropertySubview *props, int idx, long data, bool sel) = 0;
};

template<typename DATATYPE>
class CChannel : public CChannelProtocol
{
protected:
	DATATYPE *_data;

public:
	CChannel (DATATYPE *data) : _data (data)
	{
	}

	virtual CChannelProtocol* Copy () const { return new CChannel<DATATYPE> (_data); }

	virtual void OnPropEdit (CUiPropertySubview *props, int idx, long data) { _data->OnPropEdit (props, idx, data); }
	virtual void OnPropSelect (CUiPropertySubview *props, int idx, long data) { _data->OnPropSelect (props, idx, data); }
	
	virtual void SetPropValue (CUiPropertySubview *props, int idx, long data, LPCTSTR value) { _data->SetPropValue (props, idx, data, value); }
	virtual LPCTSTR GetPropValue (CUiPropertySubview *props, int idx, long data) const { return _data->GetPropValue (props, idx, data); }

	virtual bool SetPropSelect (CUiPropertySubview *props, int idx, long data, bool sel) { return _data->SetPropSelect (props, idx, data, sel); }
};

class CUiPropertySubview;

class CUiProperty  
{
	friend CUiPropertySubview;

protected:
	CFont _font;
	CStaticGround _label;
	CStaticGround _textBg;
	CButton _edit;
	CButton _select;
	CWnd *_parent;
	CButton _selCheck;
	CButton _bool;
	CComboBox _enums;
	CComboBox _children;
	CEdit _value;
	RECT _optimalBounds;

	CChannelProtocol *_channel;
	const long _data;
	CString _valueStr;
	const long _uses;
	const int _baseIdx;

public:
	CUiProperty(LPCTSTR name, COLORREF color, const RECT &rect, CWnd *parent, const CChannelProtocol &channel, 
		long data, long use, int bidx, bool selected, LPCTSTR value, bool enabled, va_list params);
	virtual ~CUiProperty();

	void ScrollBy (int dx, int dy);
	void SetWidth (int w);
	int SetLabelWidth (int w);

	CSize GetLabelExtent ();

	void SetPropValue (LPCTSTR val, bool updateUi);
	void SetEnabled (bool e);
	
	int GetOptimalWidth () const;
	void GetOptimalSize (int &w, int &h) const;
};

#endif // !defined(AFX_UIPROPERTY_H__CC02BAFD_C22C_4EBB_9A86_7FB82A6D6EF0__INCLUDED_)
