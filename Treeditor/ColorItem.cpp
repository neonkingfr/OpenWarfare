// ColorItem.cpp : implementation file
//

#include "stdafx.h"
#include <El/elementpch.hpp>
#include "RV/Tree/mytree.h"
#include "Treeditor.h"
#include "ColorItem.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CColorItem

CColorItem::CColorItem()
: _color (RGB (0, 0, 0))
{
}

CColorItem::~CColorItem()
{
}


BEGIN_MESSAGE_MAP(CColorItem, CStatic)
	//{{AFX_MSG_MAP(CColorItem)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CColorItem message handlers

void CColorItem::SetColor (COLORREF c)
{
	_color = c;

	if (::IsWindow (m_hWnd))
	{
		Invalidate (FALSE);
		UpdateWindow ();
	}
}

void CColorItem::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here
	
	// Do not call CStatic::OnPaint() for painting messages

	CRect r;
	GetClientRect (&r);
	dc.DrawEdge (&r, EDGE_SUNKEN, BF_RECT);

	r.DeflateRect (2, 2);
	dc.FillSolidRect (&r, _color);
}
