// UiPropertiesCtrl.cpp : implementation file
//

#include "stdafx.h"
#include <El/elementpch.hpp>
#include "RV/Tree/mytree.h"
#include "Treeditor.h"
#include "UiPropertiesCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUiPropertiesCtrl

CUiPropertiesCtrl::CUiPropertiesCtrl()
{
}

CUiPropertiesCtrl::~CUiPropertiesCtrl()
{
}


BEGIN_MESSAGE_MAP(CUiPropertiesCtrl, CStatic)
	//{{AFX_MSG_MAP(CUiPropertiesCtrl)
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUiPropertiesCtrl message handlers

BOOL CUiPropertiesCtrl::Init ()
{
	_propFrame.DeleteAllProps ();

	if (!::IsWindow (_propFrame.m_hWnd))
	{
		CRect r;
		GetWindowRect (&r);
		CWnd *p = GetParent ();
		p->ScreenToClient (&r);
		r.DeflateRect (1, 1);
		return _propFrame.Create (NULL, "", WS_CHILD | WS_VISIBLE | WS_VSCROLL, r, p, IDC_PROPSUBVIEW, NULL);
	}
	return TRUE;
}

void CUiPropertiesCtrl::AddUiProperty (LPCTSTR name, COLORREF color, const CChannelProtocol &channel, long data, long use, bool selected, LPCTSTR value, bool enabled, va_list params)
{
	CRect r;
	GetWindowRect (&r);
	r.DeflateRect (1, 1);
	_propFrame.AddUiProperty (name, color, r, this, channel, data, use, selected, value, enabled, params);
}

void CUiPropertiesCtrl::AddUiProperty (LPCTSTR name, COLORREF color, const CChannelProtocol &channel, long data, long use, bool selected, LPCTSTR value, bool enabled, ...)
{
	va_list params;
	va_start (params, enabled);
	AddUiProperty (name, color, channel, data, use, selected, value, enabled, params);
	va_end (params);
}

void CUiPropertiesCtrl::AddUiProperty (LPCTSTR name, const CChannelProtocol &channel, long data, long use, bool selected, LPCTSTR value, bool enabled, ...)
{
	va_list params;
	va_start (params, enabled);
	AddUiProperty (name, UI_PROPERTY_DEFAULT_COLOR, channel, data, use, selected, value, enabled, params);
	va_end (params);
}

void CUiPropertiesCtrl::OnSize(UINT nType, int cx, int cy) 
{
	// TODO: Add your message handler code here
	
	CRect r;
	if (::IsWindow (_propFrame.m_hWnd))
	{
		GetWindowRect (&r);
		GetParent ()->ScreenToClient (&r);
		r.DeflateRect (1, 1);
		_propFrame.MoveWindow (&r, TRUE);
	}
}

void CUiPropertiesCtrl::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here
	
	// Do not call CStatic::OnPaint() for painting messages
	RECT r;
	GetClientRect (&r);
	dc.FrameRect (&r, &CBrush (RGB (0, 0, 0)));
}

BOOL CUiPropertiesCtrl::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default
	
	return TRUE;
}

int CUiPropertiesCtrl::GetPropCount () const
{
	return _propFrame.GetPropCount ();
}

void CUiPropertiesCtrl::SelectProp (int i, bool sel)
{
	_propFrame.SelectProp (i, sel);
}

void CUiPropertiesCtrl::SelectOneProp (int idx)
{
	_propFrame.SelectOneProp (idx);
}

void CUiPropertiesCtrl::SelectAllProps ()
{
	_propFrame.SelectAllProps ();
}

void CUiPropertiesCtrl::DeselectAllProps ()
{
	_propFrame.DeselectAllProps ();
}

int CUiPropertiesCtrl::GetNextSelectedProp (int i) const
{
	return _propFrame.GetNextSelectedProp (i);
}

long CUiPropertiesCtrl::GetPropData (int i) const
{
	return _propFrame.GetPropData (i);
}

void CUiPropertiesCtrl::DeleteAllProps ()
{
	_propFrame.DeleteAllProps ();
}

void CUiPropertiesCtrl::DeleteProp (int i)
{
	_propFrame.DeleteProp (i);
}

LPCTSTR CUiPropertiesCtrl::GetPropValue (int i) const
{
	return _propFrame.GetPropValue (i);
}

int CUiPropertiesCtrl::GetSelectedPropCount () const
{
	return _propFrame.GetSelectedPropCount ();
}

void CUiPropertiesCtrl::SetPropValue (int i, LPCTSTR val, bool updateUi)
{
	_propFrame.SetPropValue (i, val, updateUi);
}

bool CUiPropertiesCtrl::IsPropCtrl (const CWnd *ctrl) const
{
	int id = ctrl->GetDlgCtrlID ();
	return ctrl == this || 
		id == IDC_PROPSUBVIEW || 
		IsPropCtrlID (id);
}

bool CUiPropertiesCtrl::IsPropCtrlID (int id) const
{
	return (id >= UI_PROPERTY_BASE_ID) && (id < UI_PROPERTY_BASE_ID + _propFrame._uis.GetSize () * UI_PROPERTY_LAST_ID);
}

void CUiPropertiesCtrl::RecalcLayout ()
{
	_propFrame.RecalcLayout ();
}

void CUiPropertiesCtrl::SetEnabled (int i, bool e)
{
	_propFrame.SetEnabled (i, e);
}

void CUiPropertiesCtrl::GetOptimalSize (int &w, int &h) const
{
	_propFrame.GetOptimalSize (w, h);

	RECT r;
	GetDesktopWindow ()->GetWindowRect (&r);
	int dh = ((r.bottom - r.top) * 3) >> 2;
	if (h > dh)
	{
		h = dh;
		w += UI_PROPERTY_SCROLLBAR_WIDTH;
	}
}