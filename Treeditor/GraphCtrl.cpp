// GraphCtrl.cpp : implementation file
//

#include "stdafx.h"
#include <El/elementpch.hpp>
#include "RV/Tree/mytree.h"
#include "Treeditor.h"

#include "GraphCtrl.h"
#include <float.h>
#include <math.h>
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGraphCtrl

void _AARangeError(void *arrayptr, int &index) {ASSERT(false);}
void _AAMemoryError(void *arrayptr) {ASSERT(false);}

#define UCHYTSIZE 5

CGraphCtrl::CGraphCtrl()
{
deffont=NULL;
textcolor=GetSysColor(COLOR_WINDOWTEXT);
smallx=10;
smally=10;
ffree=0;
mode=e_normal;
padding=5;
bmpdraw=true; //zapnuto kresleni do bitmapy
showhelp=false;
focusedlink=-1;
alt=false;
timer=-1;
}

CGraphCtrl::~CGraphCtrl()
{
}


BEGIN_MESSAGE_MAP(CGraphCtrl, CWnd)
	//{{AFX_MSG_MAP(CGraphCtrl)
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_WM_SETCURSOR()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_RBUTTONUP()
	ON_WM_KILLFOCUS()
	ON_WM_SETFOCUS()
	ON_WM_CANCELMODE()
	ON_WM_MOUSEWHEEL()
	ON_WM_RBUTTONDOWN()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CGraphCtrl message handlers

inline float frand(float min, float max)
  {
  float p=(float)rand()/(float)RAND_MAX;
  return min+(max-min)*p;
  }

SFloatRect& CGraphCtrl::GetBoundingBox()
  {
  if (!updatebbox) return bbox;
  if (items.GetCount()==0) {bbox=SFloatRect(0,0,0,0);return bbox;}
  bbox=items[0].rect;
  for (int i=1;i<items.GetCount();i++)
	{
	SFloatRect &qq=items[i].rect;
	if (qq.left<bbox.left) bbox.left=qq.left;
	if (qq.top<bbox.top) bbox.top=qq.top;
	if (qq.right>bbox.right) bbox.right=qq.right;
	if (qq.bottom>bbox.bottom) bbox.bottom=qq.bottom;
	}
  updatebbox=false;
  return bbox;
  }

SFloatRect CGraphCtrl::GuessPosition(float width, float height)
  {
  SFloatRect rect=GetBoundingBox();
  SFloatRect res;
  float w20=width*0.2f;
  float h20=height*0.2f;
  bool opak;
  do
	{
	float x=frand(rect.left-width-w20,rect.right+width+w20);
	float y=frand(rect.bottom-height*2,rect.bottom+height);
	res=SFloatRect(x,y,x+width,y+height);
	SFloatRect pad=res;
	pad.left-=w20;
	pad.right+=w20;
	pad.top-=h20;
	pad.bottom+=h20;
	int i=0;
	for (;i<items.GetCount();i++)
	  {
	  SFloatRect &qq=items[i].rect;
	  if (pad.right>qq.left && pad.left<qq.right && pad.bottom>qq.top && pad.top<qq.bottom) break;
	  }	
	opak=i!=items.GetCount();
	}
  while (opak);
  return res;
  }

int CGraphCtrl::InsertItem(DWORD flags, DWORD color, const char *text, float width, float height, void *data)
  {
  SFloatRect rect;
  if (flags & SGRI_NOGUESSPOSITION) {rect=SFloatRect(0,0,width,height);flags&=~SGRI_NOGUESSPOSITION;}
  else rect=GuessPosition(width,height);
  int pos=ffree;  
  SGraphItem *gri;
  if (pos>=items.GetCount())	
	{gri=items.Add();	ffree++;}
  else
	{gri=&items[pos];ffree=gri->color;}
  gri->flags=flags;
  gri->color=color;
  strncpy(gri->text,text,SGRI_MAXTEXTLEN-1);
  gri->text[SGRI_MAXTEXTLEN-1]=0;
  gri->rect=rect;
  gri->data=data;
  if (rect.left<bbox.left) bbox.left=rect.left;
  if (rect.top<bbox.top) bbox.top=rect.top;
  if (rect.right>bbox.right) bbox.right=rect.right;
  if (rect.bottom>bbox.bottom) bbox.bottom=rect.bottom;
  return pos;
  }

void CGraphCtrl::DeleteItem(int item)
  {
  ASSERT(item>=0 && item<items.GetCount()); //Item out of range
  items[item].flags=SGRI_UNUSEDITEM;
  if ((unsigned)item<ffree)	//sestav spojovy seznam prazdnych bloku - pokud je to prvni blok od zacatku
	{
	items[item].color=ffree;  //zretez volne bloky pocinaje timto
	ffree=item; //presmeruj ffree na tento blok
	}
  else	
	{
	DWORD q=ffree;	//jinak najdi blok, ktery je pred timto
	while (items[q].color<(unsigned)item) q=items[q].color; 
	items[item].color=items[q].color; //vloz tento blok za nalezeny
	items[q].color=item;  //tak vznika postupne serazeny seznam bloku
	}
  int q=items.GetCount();
  while (q>0 && items[q-1].flags & SGRI_UNUSEDITEM) q--; //nyni lze oriznout pole, pokud vse na konec je prazdne
  items.Remove(q,items.GetCount()-q);
  updatebbox=true; //vynut si prepocet boundingboxu
  int p;
  for ( p=-1;(p=EnumLinks(p,item))!=-1;) DeleteLink(p);
  for ( p=-1;(p=EnumLinks(p,-1,item))!=-1;) DeleteLink(p);
  }

const char * CGraphCtrl::GetItemText(int item)
  {
  ASSERT(item>=0 && item<items.GetCount());
  return items[item].text;
  }

void CGraphCtrl::SetItemText(int item, const char *text)
  {
  ASSERT(item>=0 && item<items.GetCount());
  strncpy(items[item].text,text,SGRI_MAXTEXTLEN-1);
  items[item].text[SGRI_MAXTEXTLEN-1]=0;
  }

int CGraphCtrl::EnumItems(int enumerator, bool selected)
  {
  if (enumerator<0) enumerator=-1;
  if (enumerator>=items.GetCount()) return -1;
  DWORD flags;
  do
	{
	enumerator++;
	if (enumerator>=items.GetCount()) return -1;
	flags=items[enumerator].flags;	  
	}
  while (flags & SGRI_UNUSEDITEM || (!(flags & SGRI_SELECTED) && selected));
  return enumerator;
  }

bool CGraphCtrl::GetItem(int item, SGraphItem *pitm)
  {
  ASSERT(item>=0 && item<items.GetCount());
  if (items[item].flags & SGRI_UNUSEDITEM) return false;
  *pitm=items[item];  
  return true;
  }

bool CGraphCtrl::SetItem(int item, SGraphItem *pitm)
  {
  ASSERT(item>=0 && item<items.GetCount());
  if (items[item].flags & SGRI_UNUSEDITEM) return false;
  items[item]=*pitm;
  updatebbox=true;
  return true;
  }

SFloatRect CGraphCtrl::GetItemRect(int item)
  {
  ASSERT(item>=0 && item<items.GetCount());
  return items[item].rect;
  }

void CGraphCtrl::SetItemRect(int item,const SFloatRect &rect)
  {
  ASSERT(item>=0 && item<items.GetCount());
  items[item].rect=rect;
  updatebbox=true;
  }

void CGraphCtrl::SetPanZoom(const SFloatRect &panzoom, bool aspec)
  {
  pzoom=panzoom;
  aspect=aspec;
  CalcSpzoom();
  }

CPoint CGraphCtrl::MapPointToWindow(float x, float y)
  {
  return CPoint(
	(int)((x-spzoom.left)/(spzoom.right)*clxs),
	(int)((y-spzoom.top)/(spzoom.bottom)*clys));

  }

void CGraphCtrl::MapPointFromWindow(const CPoint &pt, float *x, float *y)
  {
  if (x)	
	*x=((float)pt.x/(float)clxs)*(spzoom.right)+spzoom.left;
  if (y)	
	*y=((float)pt.y/(float)clys)*(spzoom.bottom)+spzoom.top;
	
  }


void CGraphCtrl::OnSize(UINT nType, int cx, int cy) 
  {
  CWnd::OnSize(nType, cx, cy);
  if (clxs!=cx || clys!=cy)
	{
	clxs=cx;
	clys=cy;
	CalcSpzoom();
	Update();
	}
  }

CRect CGraphCtrl::MapRectToWindow(const SFloatRect &frect)
  {
  CPoint p1=MapPointToWindow(frect.left,frect.top);
  CPoint p2=MapPointToWindow(frect.right,frect.bottom);
  return CRect(p1.x,p1.y,p2.x,p2.y);
  }

SFloatRect CGraphCtrl::MapRectFromWindow(const CRect &rect)
  {
  SFloatRect res;
  MapPointFromWindow(CPoint(rect.left,rect.top),&res.left,&res.top);
  MapPointFromWindow(CPoint(rect.right,rect.bottom),&res.right,&res.bottom);
  return res;
  }

void CGraphCtrl::OnPaint() 
  {
	CPaintDC dc(this); // device context for painting
	if (bmpdraw)
	  {
	  bool draw=bmp.m_hObject==NULL;
	  if (draw) bmp.CreateCompatibleBitmap(&dc,clxs,clys);
  	  CDC cdc;
	  cdc.CreateCompatibleDC(&dc);
	  CBitmap *old=cdc.SelectObject(&bmp);
	  if (draw) Draw(cdc,clxs, clys);
	  dc.BitBlt(0,0,clxs,clys,&cdc,0,0,SRCCOPY);
	  cdc.SelectObject(old);
	  cdc.DeleteDC();
	  }
	else
	  Draw(dc,clxs,clys);
	if (showhelp)
	  {
	  char *text="Z:\tZoom\r\nR:\tReset Zoom\r\nRButt:\tZoomOut";
	  dc.SetBkMode(TRANSPARENT);
	  dc.DrawText(text,strlen(text),CRect(0,0,clxs,clys),DT_TABSTOP|DT_EXPANDTABS|0x0A00);
	  }
  // Do not call CWnd::OnPaint() for painting messages
  }



static void DrawUchyt(CDC &dc, int x, int y)
  {
  CRect rc(x-UCHYTSIZE,y-UCHYTSIZE,x+UCHYTSIZE+1,y+UCHYTSIZE+1);
  dc.InvertRect(rc);
  }

static bool TestUchyt( int x, int y, CPoint &pt)
  {
  CRect rc(x-UCHYTSIZE,y-UCHYTSIZE,x+UCHYTSIZE+1,y+UCHYTSIZE+1);
  return rc.PtInRect(pt)!=FALSE;
  }

void CGraphCtrl::Draw(CDC &dc, int clxs, int clys)
  {
  int i; 
  dc.FillSolidRect(0,0,clxs,clys,GetSysColor(COLOR_WINDOW));
  CFont *old=dc.SelectObject(deffont);
  dc.SetBkMode(TRANSPARENT);
  dc.SetTextColor(textcolor);
  for (i=0;i<items.GetCount();i++) if (!(items[i].flags & SGRI_UNUSEDITEM))
	{
	SGraphItem &itm=items[i];
	CRect rc=MapRectToWindow(itm.rect);
	int diff;
	if (itm.flags & SGRI_NOTDRAWED) diff=2;else diff=0;
	if (rc.right-rc.left<smallx+diff || rc.bottom-rc.top<smally+diff)
	  {
	  itm.flags |=SGRI_NOTDRAWED;
	  }
	else
	  {
	  if (rc.right>0 && rc.bottom>0 && rc.left<clxs && rc.top<clys)
		{
		if (!(itm.flags & SGRI_CUSTOMDRAW) || itm.flags & SGRI_FILLBKCOLOR)
		  {
		  CBrush br(itm.color);
		  CBrush *old=dc.SelectObject(&br);
		  dc.SelectStockObject(BLACK_PEN);
		  dc.Rectangle(&rc);
 		  dc.SelectObject(old);
		  }
	    CRect nrc=rc;
		if (!(itm.flags & SGRI_CUSTOMDRAW) || OnCustomDraw(i,itm, dc,nrc)==false)
		  {
		  nrc.top+=padding;
		  nrc.bottom-=padding;
		  nrc.left+=padding;
		  nrc.right-=padding;
		  dc.DrawText(itm.text,strlen(itm.text),&nrc,DT_CENTER |DT_VCENTER|DT_NOPREFIX|DT_END_ELLIPSIS|DT_SINGLELINE );
		  if (mode<e_design && itm.flags & SGRI_SELECTED) dc.InvertRect(&nrc);		
		  }
		if (mode>=e_design && itm.flags & SGRI_SELECTED && itm.flags & SGRI_CANMOVE)
		  {
		  DrawUchyt(dc,rc.left,rc.top);
		  DrawUchyt(dc,rc.left,rc.bottom);
		  DrawUchyt(dc,rc.right,rc.bottom);
		  DrawUchyt(dc,rc.right,rc.top);
		  }
		}	
	  itm.flags &= ~SGRI_NOTDRAWED ;
	  }
	}
  for (i=0;i<links.GetCount();i++)
	{
	SGraphLink &lnk=links[i];
	CPen pen(PS_SOLID,i==focusedlink?3:1,lnk.color);
	CPen *old=dc.SelectObject(&pen);
	CBrush brsh(lnk.color);
	CBrush *bold=dc.SelectObject(&brsh);
	DrawLink(dc,i);
	dc.SelectObject(old);
	dc.SelectObject(bold);
	}
  dc.SelectObject(old);
  }

void * CGraphCtrl::GetItemData(int item)
  {
  ASSERT(item>=0 && item<items.GetCount());
  return items[item].data;
  }

void CGraphCtrl::SetItemData(int item, void *data)
  {
  ASSERT(item>=0 && item<items.GetCount());
  items[item].data=data;
  }

void CGraphCtrl::CalcSpzoom()
  {  
  if (aspect && clxs && clys)
	{
	float f=(float)clxs/(float)clys;
//	float ycnt=(pzoom.top+pzoom.bottom)*0.5f;
	float scl=(pzoom.right-pzoom.left)/f;
	spzoom=pzoom;
//	spzoom.left=xcnt-scl*0.5f;
	spzoom.bottom=spzoom.top+scl;
	}
  else	
	spzoom=pzoom;
  
  spzoom.right-=spzoom.left;
  spzoom.bottom-=spzoom.top;
  UpdateScrollbars();
  }


void CGraphCtrl::UpdateScrollbars()
  {
  CRect rc;
  SFloatRect &bb=GetBoundingBox();
  CPoint pt;
  rc.left=(int)((bb.left-spzoom.right)/spzoom.right*clxs);
  rc.top=(int)((bb.top-spzoom.bottom)/spzoom.bottom*clys);
  rc.right=(int)((bb.right+spzoom.right)/spzoom.right*clxs);
  rc.bottom=(int)((bb.bottom+spzoom.bottom)/spzoom.bottom*clys);
  pt.x=(int)(spzoom.left/spzoom.right*clxs);
  pt.y=(int)(spzoom.top/spzoom.bottom*clys);
  SCROLLINFO scr;
  scr.cbSize=sizeof(scr);
  scr.fMask=SIF_ALL;
  scr.nMax=rc.right;
  scr.nMin=rc.left;
  scr.nPage=clxs;
  scr.nPos=pt.x;
  SetScrollInfo(SB_HORZ,&scr,TRUE);
  scr.nMax=rc.bottom;
  scr.nMin=rc.top;
  scr.nPage=clys;
  scr.nPos=pt.y;
  SetScrollInfo(SB_VERT,&scr,TRUE);
  }

void CGraphCtrl::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
  {
  float x=spzoom.left;
  switch (nSBCode)
	{
	case SB_LINELEFT: x-=1.0f;break;
	case SB_LINERIGHT: x+=1.0f;break;
	case SB_LEFT: x=bbox.left-spzoom.right;break;
	case SB_RIGHT: x=bbox.right;break;
	case SB_PAGELEFT: x-=spzoom.right;break;
	case SB_PAGERIGHT: x+=spzoom.right;break;
	case SB_THUMBPOSITION:
	case SB_THUMBTRACK: x=(float)((int)nPos)/clxs*spzoom.right;break;
	default:return;
	}
  pzoom.left=x;
  pzoom.right=x+spzoom.right;
  CalcSpzoom();
  Update();
  CWnd::OnHScroll(nSBCode, nPos, pScrollBar);
  }

void CGraphCtrl::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
  {
  float y=spzoom.top;
  switch (nSBCode)
	{
	case SB_LINEUP: y-=1.0f;break;
	case SB_LINEDOWN: y+=1.0f;break;
	case SB_TOP: y=bbox.top-spzoom.bottom;break;
	case SB_BOTTOM: y=bbox.bottom;break;
	case SB_PAGEUP: y-=spzoom.bottom;break;
	case SB_PAGEDOWN: y+=spzoom.bottom;break;
	case SB_THUMBPOSITION:
	case SB_THUMBTRACK: y=(float)((int)nPos)/clys*spzoom.bottom;break;
	default:return;
	}
  pzoom.top=y;
  pzoom.bottom=y+spzoom.bottom;
  CalcSpzoom();
  Update();
  CWnd::OnHScroll(nSBCode, nPos, pScrollBar);
  }

BOOL CGraphCtrl::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
  {
  if (mode==e_zoom && nHitTest==HTCLIENT)
	{
	SetCursor(::LoadCursor(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDR_GRAPHZOOMCURSOR)));
	return TRUE;
	}
  if (nHitTest==HTCLIENT && mode==e_design)
	{
	CPoint cp(GetMessagePos());
	ScreenToClient(&cp);
	int p=TestUchyt(cp);
	if (p==1 || p==3) 
	  SetCursor(::LoadCursor(NULL,IDC_SIZENWSE));
	else if (p==2 || p==4) 
	  SetCursor(::LoadCursor(NULL,IDC_SIZENESW));
	else return CWnd::OnSetCursor(pWnd, nHitTest, message);
	return TRUE;
	}
  return CWnd::OnSetCursor(pWnd, nHitTest, message);
  }

void CGraphCtrl::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	float x,y;
	MapPointFromWindow(point,&x,&y);
	int itm=ItemFromPoint(x,y,false);
	resizecorner=0;
	if (GetKeyState(VK_MENU) & 0x80) 
	  {
	  alt=true;
	  begdrg=point;
	  SetCapture();
	  }
	else if (mode==e_design && (resizecorner=TestUchyt(point))!=0)
	  {
	  begdrg=point;
	  SetCapture();
	  }
	else if (itm!=-1 && mode!=e_zoom)
	  {	  
	  if (mode==e_design) mode=e_begdragdesign;
	  else mode=e_begindraglink;
	  begdrg=point;
	  focused=itm;
	  SetCapture();
	  Update();
	  }
	else if (focusedlink!=-1 && MouseAtLink(focusedlink,point)!=0 && !(links[focusedlink].flags & SGRI_FROZEN))
	  {
	  mode=e_draglink;
	  focused=links[focusedlink].fromitem;
	  CRect rc=MapRectToWindow(items[focused].rect);
	  enddrg=begdrg=rc.CenterPoint();
	  drophilted=-1;
	  SetCapture();	  
	  }
	else
	  {
	  begdrg=point;
	  selrect=CRect(point.x,point.y,point.x,point.y);
	  SetCapture();
	  }
	CWnd::OnLButtonDown(nFlags, point);
}

void CGraphCtrl::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	if (GetCapture()==this)
	  {
	  if (alt==true)
		{
		ReleaseCapture();
		alt=false;
		}
	  if (mode==e_begindraglink || mode==e_begdragdesign)
		{
		ReleaseCapture();
  	    if (!(nFlags & MK_CONTROL)) Deselect();
		SelectItem(focused,!ItemSelected(focused));
		Update();		
		mode=mode==e_begindraglink?e_normal:e_design;
		OnEndSelection();
		}
	  else if (mode==e_draglink)
		{
		ReleaseCapture();
		float x,y;
		MapPointFromWindow(point,&x,&y);
		int fnd=ItemFromPoint(x,y,false);
		if (fnd!=-1 && !(items[fnd].flags & SGRI_CANSELFLINKED) && fnd==focused) fnd=-1;
		if (fnd!=-1 && !(items[fnd].flags & SGRI_CANBELINKED)) fnd=-1;				  
		if (focusedlink!=-1)
		  {
		  SGraphLink &gl=links[focusedlink];
		  if (fnd==-1) OnUnlinkItems(focusedlink,gl.fromitem,gl.toitem,gl.data);
		  else OnChangeLink(focusedlink,gl.fromitem,gl.toitem,fnd,gl.data);
		  }		  
		else if (fnd!=-1)
		  OnLinkItems(focused, fnd);
		Update();
		focusedlink=-1;
		mode=e_normal;
		}
	  else if (mode==e_dragdesign)
		{
		OnEndMoveResize(-1);
		mode=e_design;
		ReleaseCapture();
		updatebbox=true;
		UpdateScrollbars();
		Update();
		}
	  else if (mode==e_design && resizecorner)
		{
		OnEndMoveResize(resizecorner);
		ReleaseCapture();
		}
	  else if (!alt)
  		{
		DrawSelectionRect();
		SFloatRect frect=MapRectFromWindow(selrect);
		if  (mode==e_normal || mode==e_design)		 
		  {
		  if (resizecorner==0)
			{
			if (selrect.Size().cx<5 && selrect.Size().cy<5)
			  {
			  int item=ItemFromPoint(frect.left,frect.top,false);
			  if (!(nFlags & MK_CONTROL)) Deselect();
			  if (item!=-1)		
				SelectItem(item,!ItemSelected(item));			  
			  OnEndSelection();
			  }
			else
			 {
			 SelectItems(frect,(nFlags & (MK_SHIFT | MK_CONTROL))==0);		  
			 OnEndSelection();
			 }
			}
		  }
		else		  
		  if (selrect.Size().cx>5 && selrect.Size().cy>5)			
			SetPanZoom(frect);
		ReleaseCapture();
		Update();
		}
	  }
	CWnd::OnLButtonUp(nFlags, point);
}

void CGraphCtrl::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	if (GetCapture()==this)
	  {
	  if (alt)
		{
		float x1,y1,x2,y2;
		if (point!=begdrg)
		  {
		  MapPointFromWindow(begdrg,&x1,&y1);
		  MapPointFromWindow(point,&x2,&y2);
  		  pzoom.left=pzoom.left+x1-x2;
		  pzoom.right=pzoom.right+x1-x2;
		  pzoom.top=pzoom.top+y1-y2;
		  pzoom.bottom=pzoom.bottom+y1-y2;
		  CalcSpzoom();
		  Update();
		  point=begdrg;
		  ClientToScreen(&point);
		  SetCursorPos(point.x,point.y);
		  }
		}
	  else if (mode==e_begindraglink)
		{
		if ((abs(point.x-begdrg.x)>20 || abs(point.y-begdrg.y)>20) && (items[focused].flags & SGRI_CANMAKELINKS))
		  {		  
		  mode=e_draglink;
	  	  Deselect();
		  SelectItem(focused);
		  OnEndSelection();
		  Update();
		  enddrg=begdrg;		
		  drophilted=-1;
		  SetCursor(::LoadCursor(NULL,IDC_UPARROW));
		  }
		}
	  else if (mode==e_begdragdesign)
		{
		if (abs(point.x-begdrg.x)>5 || abs(point.y-begdrg.y)>5) 
		  {
		  if (ItemSelected(focused)==false)
			{
  			if (!(nFlags & MK_CONTROL)) Deselect();
			SelectItem(focused,true);
			OnEndSelection();
			}
		  SetCursor(::LoadCursor(NULL,IDC_SIZEALL));
		  mode=e_dragdesign;
		  }
		}
	  else if (mode==e_dragdesign)
		{
		float x1,y1,x2,y2;
		MapPointFromWindow(begdrg,&x1,&y1);
		MapPointFromWindow(point,&x2,&y2);
  	    float xs=x2-x1;
		float ys=y2-y1;
  	    CDC *dc=GetDC();

		for (int i=0;i<items.GetCount();i++) if (items[i].flags & SGRI_SELECTED && items[i].flags & SGRI_CANMOVE)
		  {
		  dc->SetROP2(R2_XORPEN);
		  dc->SelectStockObject(WHITE_PEN);
		  dc->SelectStockObject(HOLLOW_BRUSH);
		  SFloatRect &frect=items[i].rect;
		  CRect rc=MapRectToWindow(frect);
		  dc->Rectangle(&rc);
		  frect.left+=xs;
		  frect.right+=xs;
		  frect.bottom+=ys;
		  frect.top+=ys;
		  OnMoveItem(i,xs,ys);
		  rc=MapRectToWindow(frect);
  		  dc->Rectangle(&rc);
		  }
		ReleaseDC(dc);
		begdrg=point;
		}
	  else if (mode==e_draglink)
		{
		CDC *dc=GetDC();
		dc->SelectStockObject(WHITE_PEN);
		dc->SetROP2(R2_XORPEN);
		dc->MoveTo(begdrg);
		dc->LineTo(enddrg);
		dc->MoveTo(begdrg);
		dc->LineTo(point);
		enddrg=point;
		float x,y;
		MapPointFromWindow(point,&x,&y);
		int p=ItemFromPoint(x,y,false);
		if (p!=-1 && !(items[p].flags & SGRI_CANSELFLINKED) && p==focused) p=-1;
		if (p!=-1 && !(items[p].flags & SGRI_CANBELINKED)) p=-1;
		if (drophilted!=p)
		  {
		  if (drophilted!=-1)
			{
			CRect rc=MapRectToWindow(items[drophilted].rect);
			dc->InvertRect(&rc);
			}
		  if (p!=-1)
			{
  			CRect rc=MapRectToWindow(items[p].rect);
			dc->InvertRect(&rc);
			}		
		  }
		drophilted=p;
		ReleaseDC(dc);
		}
	  else if (mode==e_design && resizecorner)
		{		
		float x1,y1,x2,y2;
		MapPointFromWindow(begdrg,&x1,&y1);
		MapPointFromWindow(point,&x2,&y2);
		begdrg=point;
		ResizeSelected(resizecorner,x2-x1,y2-y1);
		Update();
		}
	  else
		{
		DrawSelectionRect();
		selrect.left=begdrg.x;
		selrect.top=begdrg.y;
		selrect.right=point.x;
		selrect.bottom=point.y;
		selrect.NormalizeRect();
		DrawSelectionRect();
		}
	  }
	else
	  {
	  int fc=focusedlink;
 	  float x,y;
	  MapPointFromWindow(point,&x,&y);
	  if (ItemFromPoint(x,y)!=-1) focusedlink=-1;
		else
	  if (focusedlink==-1 || MouseAtLink(focusedlink,point)!=1)
		{
		if (mode==e_zoom) focusedlink=-1;else focusedlink=LinkFromPoint(point);
		if (focusedlink!=-1 && links[focusedlink].flags & SGRI_DONTFOCUS) focusedlink=-1;
		}
	  if (fc!=focusedlink)
		{
		if (focusedlink==-1)
		   OnTouchLink(-1, -1, -1, NULL);
		else
		  {
		  SGraphLink &gr=links[focusedlink];
		  OnTouchLink(focusedlink,gr.fromitem,gr.toitem,gr.data );
		  }
		Update();
		}
	  }
	CWnd::OnMouseMove(nFlags, point);
}

void CGraphCtrl::DrawSelectionRect()
  {
  CDC *dc=GetDC();
  dc->SetROP2(R2_XORPEN);
  CPen pen(PS_DOT,1,(COLORREF)0);
  CPen *old=dc->SelectObject(&pen);
  dc->SelectStockObject(HOLLOW_BRUSH);
  dc->Rectangle(selrect);
  dc->SelectObject(old);
  ReleaseDC(dc);
  }

void CGraphCtrl::EnableBitmapDrawing(bool enable)
  {
  bmpdraw=enable;
  if (bmp.m_hObject) bmp.DeleteObject();
  }

void CGraphCtrl::Update()
  {
  if (bmpdraw && bmp.m_hObject) bmp.DeleteObject();
  Invalidate();
  }

void CGraphCtrl::OnRButtonUp(UINT nFlags, CPoint point) 
  {
  KillTimer(timer);
  if (mode==e_zoom)
	{
	SFloatRect rc=spzoom;
	rc.left-=rc.right*0.5f;
	rc.top-=rc.bottom*0.5f;
	rc.right=rc.left+2.0f*rc.right;
	rc.bottom=rc.top+2.0f*rc.bottom;	
	SetPanZoom(rc);
	Update();
	}
  else
    CWnd::OnRButtonUp(nFlags, point);
  }

void CGraphCtrl::OnKillFocus(CWnd* pNewWnd) 
{
mode=e_normal;
showhelp=false;
Invalidate();
Update();
	CWnd::OnKillFocus(pNewWnd);
}

void CGraphCtrl::OnSetFocus(CWnd* pOldWnd) 
{
Invalidate();
	CWnd::OnSetFocus(pOldWnd);
}




void CGraphCtrl::SelectItems(const SFloatRect &rc, bool clearsel)
  {
  int i;
  for (i=0;i<items.GetCount();i++) 
	{
	DWORD &flags=items[i].flags;
	if (!(flags & (SGRI_NOTDRAWED|SGRI_UNUSEDITEM )))
	  {
	  SFloatRect &ic=items[i].rect;
	  if (rc.left<ic.left && rc.right>ic.right && rc.top<ic.top && rc.bottom>ic.bottom)
		{
		if (!(flags & SGRI_SELECTED))
		  {
		  OnItemStateChanged(i);
		  flags|=SGRI_SELECTED;
		  }
		}

	  else 
		if (clearsel) flags&=~SGRI_SELECTED;
	  }
	else
	  if (clearsel) flags&=~SGRI_SELECTED;
	}
  }

int CGraphCtrl::ItemFromPoint(float x, float y, bool selonly)
  {
  int i;
  for (i=items.GetCount()-1;i>=0;i--) if (!(items[i].flags & (SGRI_NOTDRAWED|SGRI_UNUSEDITEM)) && (!selonly || items[i].flags & SGRI_SELECTED))
	{	
	SFloatRect &ic=items[i].rect;
	if (ic.left<x && ic.right>x && ic.top<y && ic.bottom>y) return i;
	}
  return -1;
  }

void CGraphCtrl::Deselect()
  {  
  for (int i=0;i<items.GetCount();i++) 
	{
	if (items[i].flags & SGRI_SELECTED)
	  {
	  items[i].flags &=~SGRI_SELECTED;
	  OnItemStateChanged(i);
	  }
	}
  }

void CGraphCtrl::SelectItem(int item, bool select)
  {
  ASSERT(item>=0 && item<items.GetCount());
  if (items[item].flags & SGRI_UNUSEDITEM) return;  
  if (select) items[item].flags |=SGRI_SELECTED;
  else items[item].flags &=~SGRI_SELECTED;
  OnItemStateChanged(item);
  }

bool CGraphCtrl::ItemSelected(int item)
  {
  ASSERT(item>=0 && item<items.GetCount());
  if (items[item].flags & SGRI_UNUSEDITEM) return false;
  return (items[item].flags & SGRI_SELECTED)!=0;
  }



void CGraphCtrl::OnCancelMode() 
  {
  ReleaseCapture();
  Update();
  if (mode!=e_design) mode=e_normal;
  CWnd::OnCancelMode();	
  }

int CGraphCtrl::TestUchyt(CPoint &pt)
  {
  int i;
  for (i=0;i<items.GetCount();i++) if ((items[i].flags & SGRI_SELECTED) && (items[i].flags & SGRI_CANRESIZE))
	{
	CRect rc=MapRectToWindow(items[i].rect);
	if (::TestUchyt(rc.left,rc.top,pt)) return 1;
	if (::TestUchyt(rc.right,rc.top,pt)) return 2;
	if (::TestUchyt(rc.right,rc.bottom,pt)) return 3;
	if (::TestUchyt(rc.left,rc.bottom,pt)) return 4;
	}
  return 0;
  }

void CGraphCtrl::ResizeSelected(int corner, float xs, float ys)
  {
  float minx=(float)(smallx+2)/(float)clxs*spzoom.right;
  float miny=(float)(smally+2)/(float)clys*spzoom.bottom;
  for (int i=0;i<items.GetCount();i++) if ((items[i].flags & SGRI_SELECTED) && (items[i].flags & SGRI_CANRESIZE))
	{
	SFloatRect &rc=items[i].rect;
	switch (corner)
	  {
	  case 1: rc.left+=xs;rc.top+=ys;break;
	  case 2: rc.right+=xs;rc.top+=ys;break;
	  case 3: rc.right+=xs;rc.bottom+=ys;break;
	  case 4: rc.left+=xs;rc.bottom+=ys;break;
	  }	
	if (rc.right<=rc.left+minx) rc.right=rc.left+minx;
	if (rc.bottom<=rc.top+miny) rc.bottom=rc.top+miny;
	OnResizeItem(i,corner,xs,ys);
	}
  }

bool CGraphCtrl::InsertLink(int fromitem, int toitem, DWORD color, short flags, void *data)
  {
  SGraphLink *grl=links.Add();
  if (items[fromitem].flags & SGRI_UNUSEDITEM || items[toitem].flags & SGRI_UNUSEDITEM )
	return false;
  grl->fromitem=fromitem;
  grl->toitem=toitem;
  grl->color=color;
  grl->data=data;
  grl->order=-1;
  grl->flags=flags;
  return true;
  }

int CGraphCtrl::EnumLinks(int enm, int fromitem, int toitem, void *data)
  {
  if (enm<0) enm=-1;
  SGraphLink *ll;
  do
	{
	enm++;
	if (enm>=links.GetCount()) return -1;
	ll=&links[enm];
	}
  while (!((fromitem==-1 || ll->fromitem==fromitem) &&
		 (toitem==-1 || ll->toitem==toitem) &&
		 (data==NULL || ll->data==data)));
  return enm;
  }

void CGraphCtrl::DeleteLinks(int fromitem, int toitem, void *data)
  {
  if (fromitem==-1 && toitem==-1 && data==NULL) DeleteAllLinks();
  for (int p=-1;(p=EnumLinks(p,fromitem,toitem,data))!=-1;)
	DeleteLink(p);
  focusedlink=-1;
  }

int geCompare(const SGraphLink *src1, const SGraphLink *src2)
  {
  if (src1->fromitem==src2->fromitem)
	{
	if (src1->toitem==src2->toitem)
	  return (src1->order>src2->order)-(src1->order<src2->order);
    return (src1->toitem>src2->toitem)-(src1->toitem<src2->toitem);
	}
  return (src1->fromitem>src2->fromitem)-(src1->fromitem<src2->fromitem);
  }

void CGraphCtrl::OrderLinks()
  {
  links.Sort();
  int fi=-1,ti=-1,od=-1;
  for (int i=0;i<links.GetCount();i++)
	{
	SGraphLink &lnk=links[i];
	if (lnk.fromitem!=fi || lnk.toitem!=ti) 
	  {
	  fi=lnk.fromitem;
	  ti=lnk.toitem;
	  od=0;
	  }
	lnk.order=od++;
	}
  focusedlink=-1;
  }

BOOL CGraphCtrl::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
  {
	int ff=zDelta/WHEEL_DELTA;
	float f1,f2;
	if (ff<0)
	  {
	  ff=-ff;
	  f1=0.05f;
	  f2=0.95f;
	  }
	else
	  {
	  f1=-0.05f;
	  f2=1.05f;
	  }
	  for (int i=0;i<ff;i++)
		{
		pzoom.left=spzoom.left+spzoom.right*f1;
		pzoom.right=spzoom.left+spzoom.right*f2;
		pzoom.top=spzoom.top+spzoom.bottom*f1;
		pzoom.bottom=spzoom.top+spzoom.bottom*f2;
		}
	  
  CalcSpzoom();
  Update();
  return CWnd::OnMouseWheel(nFlags, zDelta, pt);
  }

static void ClipLineByBox(SFloatRect &line, SFloatRect &box, bool begin)
  {
  float x1=line.left;
  float y1=line.top;
  float xs=line.right-line.left;
  float ys=line.bottom-line.top;  
  float k1=-(x1-box.left)/xs;
  float k2=-(x1-box.right)/xs;
  float k3=-(y1-box.top)/ys;
  float k4=-(y1-box.bottom)/ys;  
  if (!_finite(k1)) k1=2.0;
  if (!_finite(k2)) k1=2.0;
  if (!_finite(k3)) k1=2.0;
  if (!_finite(k4)) k1=2.0;

  float k=k1; 
  if (begin)
	{
	if (k2<k && k2>=0 || k<0) k=k2;
	if (k3<k && k3>=0 || k<0) k=k3;
	if (k4<k && k4>=0 || k<0) k=k4;
	line.left=x1+xs*k;
	line.top=y1+ys*k;
	}
  else
	{
	if (k2>k && k2<=1 || k>1) k=k2;
	if (k3>k && k3<=1 || k>1) k=k3;
	if (k4>k && k4<=1 || k>1) k=k4;
	line.right=x1+xs*k;
	line.bottom=y1+ys*k;
	}
  }

static void Sipka(CDC &dc, CPoint pos, CPoint dir)
  {
  int d=(int)(sqrt(dir.x*dir.x+dir.y*dir.y));
  if (d==0) return;
  dir.x=dir.x*16/d;
  dir.y=dir.y*16/d;
  CRect rc(dir.x,dir.y,dir.y,-dir.x);
  CPoint pts[5]={CPoint(0,0),CPoint(5,-2),CPoint(4,0),CPoint(5,2),CPoint(0,0)};//{{0,0},{-5,-3},{-3,0},{-5,3},{0,0}}  
  dc.BeginPath();
  for (int i=0;i<5;i++)
	{
	CPoint ptq;
	ptq.x=((rc.left*pts[i].x+rc.right*pts[i].y)>>2)+pos.x;
	ptq.y=((rc.top*pts[i].x+rc.bottom*pts[i].y)>>2)+pos.y;
	if (i==0) dc.MoveTo(ptq);else dc.LineTo(ptq);
	}
  dc.EndPath();
  dc.FillPath();  
  }

void CGraphCtrl::DrawLink(CDC &dc, int lnk)
  {
  SGraphLink &lk=links[lnk];
  CRect rc;
  if (CalcLinkRect(lk.fromitem,lk.toitem,lk.order,rc))
	{
	if (lk.flags & SGRI_CUSTOMDRAW)
	  if (OnCustomDrawLink(lnk,lk.fromitem,lk.toitem,lk.order,lk.data,dc,rc)==true) 
		return;
    SFloatRect &fr=items[lk.fromitem].rect;
	CRect pp=MapRectToWindow(fr);
	if (lk.fromitem==lk.toitem)
	  {
	  int zfx=300/(rc.right-rc.left);
	  int zfy=300/(rc.bottom-rc.top);
	  switch(lk.order & 0x3)
		{
		case 0: dc.Arc(&rc,CPoint(pp.right,pp.top),CPoint(pp.left,pp.bottom));
		  Sipka(dc,CPoint(pp.left,rc.bottom),CPoint(-10,-zfy));
		  break;
		case 1: dc.Arc(&rc,CPoint(pp.right,pp.bottom),CPoint(pp.left,pp.top));
		  Sipka(dc,CPoint(rc.left,pp.top),CPoint(zfx,-10));
		  break;
		case 2: dc.Arc(&rc,CPoint(pp.left,pp.bottom),CPoint(pp.right,pp.top));
		  Sipka(dc,CPoint(pp.right,rc.top),CPoint(10,zfy));
		  break;
		case 3: dc.Arc(&rc,CPoint(pp.left,pp.top),CPoint(pp.right,pp.bottom));
  		  Sipka(dc,CPoint(rc.right,pp.bottom),CPoint(-zfx,10));
		  break;
		}
	  }
	else
	  {
	  dc.MoveTo(rc.left,rc.top);
	  dc.LineTo(rc.right,rc.bottom);
	  Sipka(dc,CPoint(rc.right,rc.bottom),CPoint(rc.left-rc.right,rc.top-rc.bottom));
	  }
	}
  }

bool CGraphCtrl::CalcLinkRect(int from, int to, int order, CRect &rc)
  {
  if (items[from].flags & SGRI_NOTDRAWED || items[to].flags & SGRI_NOTDRAWED) return false;
  if (from==to)
	{
    SFloatRect &fr=items[from].rect;
	float sz=__min(fr.right-fr.left,fr.bottom-fr.top)/2;
	SFloatRect qq;
	float x,y;
	switch(order & 0x3)
	  {
	  case 0: x=fr.left;y=fr.top;break;
	  case 1: x=fr.right;y=fr.top;break;
	  case 2: x=fr.right;y=fr.bottom;break;
	  /* case 3: */ 
	  default: x=fr.left;y=fr.bottom;break;
	  }
	qq.left=x-sz;
	qq.right=x+sz;
	qq.top=y-sz;
	qq.bottom=y+sz;
    rc=MapRectToWindow(qq);
	}
  else
	{
	SFloatRect &fr=items[from].rect;
	SFloatRect &tt=items[to].rect;
	SFloatRect ln;
	ln.left=(fr.left+fr.right)*0.5f;
	ln.top=(fr.top+fr.bottom)*0.5f;
	ln.right=(tt.left+tt.right)*0.5f;
	ln.bottom=(tt.top+tt.bottom)*0.5f;
	float dx=ln.right-ln.left;
	float dy=ln.bottom-ln.top;
	float cx=((dy>0)-(dy<0))*(float)(order+1)*4.0f/(float)clxs*spzoom.right;
	float cy=-((dx>0)-(dx<0))*(float)(order+1)*4.0f/(float)clys*spzoom.bottom;
	ln.left+=cx;
	ln.right+=cx;
	ln.bottom+=cy;
	ln.top+=cy;
	ClipLineByBox(ln,fr,true);
	ClipLineByBox(ln,tt,false);
	rc=MapRectToWindow(ln);
	}
  return true;
  }

int CGraphCtrl::LinkFromPoint(CPoint &pt)
  {
  int i;
  int ret=-1;
  for (i=links.GetCount()-1;i>=0;i--)
	{
	int z=MouseAtLink(i,pt);
	if (z==2 && ret==-1) ret=i;
	else if (z==1) return i;
	}
  return ret;
  }
  

int CGraphCtrl::MouseAtLink(int link, CPoint &pt)
  {
  SGraphLink &lnk=links[link];
  CRect rc;  
	if (CalcLinkRect(lnk.fromitem,lnk.toitem,lnk.order,rc))
	  {
	  if (lnk.fromitem==lnk.toitem && rc.PtInRect(pt))
		{
		return 2;
		}
	  else
		{
		int xr=(rc.right-rc.left);
		int yr=(rc.bottom-rc.top);
		int m2=xr*xr+yr*yr;
		CPoint df=pt-CPoint(rc.left,rc.top);
		int t1=(xr*df.x+yr*df.y)*1000/m2;
		if (t1>0 && t1<1000)
		  {
		  df.x=rc.left+xr*t1/1000;
		  df.y=rc.top+yr*t1/1000;
		  if (abs(df.x-pt.x)<5 && abs(df.y-pt.y)<5) return 1;
		  }
		}
	  }
  return 0;
  }

int CGraphCtrl::GetFocusedLink()
  {
  return focusedlink;
  }

int CGraphCtrl::Mode(int m)
  {
  int ret;
  if (mode==e_zoom) ret=SGRM_ZOOM;
  else if (mode<e_design) ret=SGRM_NORMAL;
  else /* if (mode>=e_design) */ ret=SGRM_DESIGN;
  if (m!=SGRM_GETCURRENT) SendMessage(WM_CANCELMODE);
  switch (m)
	{
	case SGRM_ZOOM: mode=e_zoom;break;
	case SGRM_NORMAL: mode=e_normal;break;
	case SGRM_DESIGN: mode=e_design;break;
	}
  Update();
  return ret;
  }

void CGraphCtrl::OnRButtonDown(UINT nFlags, CPoint point) 
  {
  timer=SetTimer(12255,400,NULL);
  CWnd::OnRButtonDown(nFlags, point);
  } 

void CGraphCtrl::OnTimer(UINT nIDEvent) 
  {
  if (nIDEvent==timer)	
	{
	CPoint pt;
	GetCursorPos(&pt);
	SendMessage(WM_CANCELMODE,0,0);
	GetParent()->SendNotifyMessage(WM_CONTEXTMENU,(WPARAM)this->m_hWnd,MAKELPARAM(pt.x,pt.y));
	KillTimer(nIDEvent);
	}
  CWnd::OnTimer(nIDEvent);
  }
