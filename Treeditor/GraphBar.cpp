// GraphBar.cpp: implementation of the CGraphBar class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <El/elementpch.hpp>
#include "RV/Tree/mytree.h"
#include "treeditor.h"
#include "GraphBar.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGraphBar::CGraphBar()
{

}

CGraphBar::~CGraphBar()
{

}

CSize CGraphBar::CalcDimension( int nLength, DWORD dwMode )
{
	// Return default if it is being docked or floated
	if ((dwMode & LM_VERTDOCK) || (dwMode & LM_HORZDOCK))
	{
		if (dwMode & LM_STRETCH) // if not docked stretch to fit
			return CSize((dwMode & LM_HORZ) ? 32767 : m_sizeDocked.cx,
			(dwMode & LM_HORZ) ? m_sizeDocked.cy : 32767);
		else
			return m_sizeDocked;
	}
	if (dwMode & LM_MRUWIDTH)
		return m_sizeFloating;
	// In all other cases, accept the dynamic length
	if (dwMode & LM_LENGTHY)
	{
		//nLength += (FLOAT_BAR_VER_ALIGN - 1);
		//nLength -= nLength % FLOAT_BAR_VER_ALIGN;
		return CSize(m_sizeFloating.cx, (m_bChangeDockedSize) ?
		m_sizeFloating.cy = m_sizeDocked.cy = nLength :
		m_sizeFloating.cy = nLength);
	}
	else
	{
		//nLength += (FLOAT_BAR_HOR_ALIGN - 1);
		//nLength -= nLength % FLOAT_BAR_HOR_ALIGN;
		return CSize((m_bChangeDockedSize) ?
		m_sizeFloating.cx = m_sizeDocked.cx = nLength :
		m_sizeFloating.cx = nLength, m_sizeFloating.cy);
	}
}

CSize CGraphBar::CalcDynamicLayout(int nLength, DWORD dwMode)
{
	CSize dim = CalcDimension(nLength, dwMode);

#define CALC_CTRL_RECT(id, ctrl, r) CWnd *ctrl = GetDlgItem (id); if (ctrl == NULL) { return dim; } CRect r; ctrl->GetWindowRect(&r); ScreenToClient (&r)

	CALC_CTRL_RECT(IDC_GRAPHM_LABEL, gmLabel, gmLabelR);
	CALC_CTRL_RECT(IDC_GRAPHM_NORMAL, gmNormal, gmNormalR);
	CALC_CTRL_RECT(IDC_GRAPHM_DESIGN, gmDesign, gmDesignR);
	CALC_CTRL_RECT(IDC_GRAPHM_ZOOM, gmZoom, gmZoomR);
	CALC_CTRL_RECT(IDC_CENTER, gmCenter, gmCenterR);

#undef CALC_CTRL_RECT

#define SPACE1 4
#define SPACE2 7

	int labelHeight = gmLabelR.bottom - gmLabelR.top;
	int labelOffset = ((gmNormalR.bottom - gmNormalR.top) >> 1) - (labelHeight >> 1);

	CRect gcR;
	gcR.SetRect (SPACE2, SPACE2, dim.cx - SPACE2, dim.cy - 2 * SPACE2 - (gmNormalR.bottom - gmNormalR.top));

	gmLabelR.SetRect (SPACE2, gcR.bottom + SPACE2 + labelOffset, SPACE2 + (gmLabelR.right - gmLabelR.left), gcR.bottom + SPACE2 + labelOffset + labelHeight);
	gmNormalR.SetRect (gmLabelR.right + SPACE1, gcR.bottom + SPACE2, gmLabelR.right + SPACE1 + gmNormalR.right - gmNormalR.left, gcR.bottom + SPACE2 + gmNormalR.bottom - gmNormalR.top);
	gmDesignR.SetRect (gmNormalR.right + SPACE1, gcR.bottom + SPACE2, gmNormalR.right + SPACE1 + gmDesignR.right - gmDesignR.left, gcR.bottom + SPACE2 + gmDesignR.bottom - gmDesignR.top);
	gmZoomR.SetRect (gmDesignR.right + SPACE1, gcR.bottom + SPACE2, gmDesignR.right + SPACE1 + gmZoomR.right - gmZoomR.left, gcR.bottom + SPACE2 + gmZoomR.bottom - gmZoomR.top);
	gmCenterR.SetRect (gmZoomR.right + SPACE1, gcR.bottom + SPACE2, gmZoomR.right + SPACE1 + gmCenterR.right - gmCenterR.left, gcR.bottom + SPACE2 + gmCenterR.bottom - gmCenterR.top);

	gmLabel->MoveWindow(&gmLabelR, TRUE);
	gmNormal->MoveWindow(&gmNormalR, TRUE);
	gmDesign->MoveWindow(&gmDesignR, TRUE);
	gmZoom->MoveWindow(&gmZoomR, TRUE);
	gmCenter->MoveWindow(&gmCenterR, TRUE);

	_graphCtrl.MoveWindow(&gcR, TRUE);

#undef SPACE1
#undef SPACE2

	return dim;
}


BOOL CGraphBar::Create( CWnd* pParentWnd, UINT nIDTemplate,
							  UINT nStyle, UINT nID, CSize *s, BOOL bChange)
{
	if(!CDialogBar::Create(pParentWnd,nIDTemplate,nStyle,nID))
		return FALSE;
	
	_graphCtrl.Create(NULL,"",WS_VISIBLE|WS_CHILD,CRect(0,0,0,0),this,AFX_IDW_PANE_FIRST,NULL);

	m_bChangeDockedSize = bChange;
	if (s != NULL)
	{
		m_sizeDefault = *s;
	}
	m_sizeFloating = m_sizeDocked = m_sizeDefault;
	return TRUE;
}

BOOL CGraphBar::Create( CWnd* pParentWnd,
							  LPCTSTR lpszTemplateName, UINT nStyle,
							  UINT nID, BOOL bChange)
{
	if (!CDialogBar::Create( pParentWnd, lpszTemplateName,
		nStyle, nID))
		return FALSE;
	
	_graphCtrl.Create(NULL,"",WS_VISIBLE|WS_CHILD,CRect(0,0,0,0),this,AFX_IDW_PANE_FIRST,NULL);

	m_bChangeDockedSize = bChange;
	m_sizeFloating = m_sizeDocked = m_sizeDefault;
	return TRUE;
}

void CGraphBar::ResetZoom ()
{
	SFloatRect rc=_graphCtrl.GetBoundingBox();
	float xs=(rc.right-rc.left)*0.2f;
	float ys=(rc.bottom-rc.top)*0.2f;
	rc.left-=xs;
	rc.top-=ys;
	rc.right+=xs;
	rc.bottom+=ys;
	_graphCtrl.SetPanZoom(rc,true);
}

BOOL CGraphBar::OnCmdMsg (UINT nID, int nCode, void *pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
	return CDialogBar::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);

	/*
	switch (nID)
	{
	case IDC_GRAPHM_NORMAL:
		_graphCtrl.Mode(SGRM_NORMAL);
		return TRUE;

	case IDC_GRAPHM_DESIGN:
		_graphCtrl.Mode(SGRM_DESIGN);
		return TRUE;

	case IDC_GRAPHM_ZOOM:
		_graphCtrl.Mode(SGRM_ZOOM);
		return TRUE;

	default:
		return CDialogBar::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
	}
	*/
}