// PropertyBar.h: interface for the CPropertyBar class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROPERTYBAR_H__17BF3206_84CF_4A1D_8EFC_F1113412E102__INCLUDED_)
#define AFX_PROPERTYBAR_H__17BF3206_84CF_4A1D_8EFC_F1113412E102__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "UiPropertiesCtrl.h"

class CPropertyBar : public CDialogBar  
{
public:
	CSize m_sizeDocked;
	CSize m_sizeFloating;
	BOOL m_bChangeDockedSize;
	CUiPropertiesCtrl _propCtrl;
	
public:
	CPropertyBar();
	virtual ~CPropertyBar();

	BOOL Create( CWnd* pParentWnd, UINT nIDTemplate, UINT nStyle,
		UINT nID, CSize *s = NULL, BOOL = TRUE);
	BOOL Create( CWnd* pParentWnd, LPCTSTR lpszTemplateName,
		UINT nStyle, UINT nID, BOOL = TRUE);
	
	virtual CSize CalcDynamicLayout( int nLength, DWORD dwMode );
	CSize CalcDimension( int nLength, DWORD dwMode );
	
	virtual BOOL OnWndMsg( UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult );
	
	void GetOptimalSize (int &w, int &h) const;
};

#endif // !defined(AFX_PROPERTYBAR_H__17BF3206_84CF_4A1D_8EFC_F1113412E102__INCLUDED_)
