# Microsoft Developer Studio Project File - Name="Treeditor" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=Treeditor - Win32 Debug Static
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Treeditor.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Treeditor.mak" CFG="Treeditor - Win32 Debug Static"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Treeditor - Win32 Debug Static" (based on "Win32 (x86) Application")
!MESSAGE "Treeditor - Win32 Release Static" (based on "Win32 (x86) Application")
!MESSAGE "Treeditor - Win32 Release Static NoCopyRects" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName "Treeditor"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Treeditor - Win32 Debug Static"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Treeditor___Win32_Debug_Static"
# PROP BASE Intermediate_Dir "Treeditor___Win32_Debug_Static"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug_Static"
# PROP Intermediate_Dir "Debug_Static"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GR /GX /ZI /Od /I "w:\c" /D DO_COPYRECTS=1 /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D MFC_NEW=1 /D "_ENABLE_REPORT" /Fr /YX /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x405 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x405 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 d3d8.lib d3dx8.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 dxerr8.lib d3d8.lib d3dx8.lib winmm.lib w:\c\RV\Tree\Picture.lib /nologo /stack:0x3d0900 /subsystem:windows /map /debug /machine:I386 /nodefaultlib:"libc.lib" /pdbtype:sept

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Treeditor___Win32_Release_Static"
# PROP BASE Intermediate_Dir "Treeditor___Win32_Release_Static"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release_Static"
# PROP Intermediate_Dir "Release_Static"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GR /GX /Zi /O2 /I "w:\c" /D DO_COPYRECTS=1 /D "NDEBUG" /D _RELEASE=1 /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D MFC_NEW=1 /D "_ENABLE_REPORT" /YX /FD /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x405 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x405 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 d3d8.lib d3dx8.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 dxerr8.lib d3d8.lib d3dx8.lib winmm.lib w:\c\RV\Tree\Picture.lib /nologo /stack:0x3d0900 /subsystem:windows /map /machine:I386 /nodefaultlib:"libc.lib"
# SUBTRACT LINK32 /debug

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static NoCopyRects"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Treeditor___Win32_Release_Static_NoCopyRects"
# PROP BASE Intermediate_Dir "Treeditor___Win32_Release_Static_NoCopyRects"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Treeditor___Win32_Release_Static_NoCopyRects"
# PROP Intermediate_Dir "Treeditor___Win32_Release_Static_NoCopyRects"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GR /GX /Zi /O2 /I "w:\c" /D "NDEBUG" /D _RELEASE=1 /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D MFC_NEW=1 /D "_ENABLE_REPORT" /YX /FD /c
# SUBTRACT BASE CPP /Fr
# ADD CPP /nologo /MT /W3 /GR /GX /Zi /O2 /I "w:\c" /D DO_COPYRECTS=0 /D "NDEBUG" /D _RELEASE=1 /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D MFC_NEW=1 /D "_ENABLE_REPORT" /YX /FD /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x405 /d "NDEBUG"
# ADD RSC /l 0x405 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 dxerr8.lib d3d8.lib d3dx8.lib winmm.lib w:\c\RV\Tree\Picture.lib /nologo /stack:0x3d0900 /subsystem:windows /map /machine:I386 /nodefaultlib:"libc.lib"
# SUBTRACT BASE LINK32 /debug
# ADD LINK32 dxerr8.lib d3d8.lib d3dx8.lib winmm.lib w:\c\RV\Tree\Picture.lib /nologo /stack:0x3d0900 /subsystem:windows /map /machine:I386 /nodefaultlib:"libc.lib"
# SUBTRACT LINK32 /debug

!ENDIF 

# Begin Target

# Name "Treeditor - Win32 Debug Static"
# Name "Treeditor - Win32 Release Static"
# Name "Treeditor - Win32 Release Static NoCopyRects"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\ColorDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ColorItem.cpp
# End Source File
# Begin Source File

SOURCE=.\CompGraphCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\ComponentBar.cpp
# End Source File
# Begin Source File

SOURCE=.\CurveDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CurveItem.cpp
# End Source File
# Begin Source File

SOURCE=.\DebugBar.cpp
# End Source File
# Begin Source File

SOURCE=.\GraphBar.cpp
# End Source File
# Begin Source File

SOURCE=.\GraphCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\InfoBar.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\PairDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\PaletteEditor.cpp
# End Source File
# Begin Source File

SOURCE=.\PreferencesDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\PropertyBar.cpp
# End Source File
# Begin Source File

SOURCE=.\ScriptParser.cpp
# End Source File
# Begin Source File

SOURCE=.\SelectChildKindDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\StaticGround.cpp
# End Source File
# Begin Source File

SOURCE=.\TextEditor.cpp
# End Source File
# Begin Source File

SOURCE=.\Treeditor.cpp
# End Source File
# Begin Source File

SOURCE=.\Treeditor.rc
# End Source File
# Begin Source File

SOURCE=.\TreeditorClass.cpp
# End Source File
# Begin Source File

SOURCE=.\TreeditorDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\TreeditorLex.cpp
# End Source File
# Begin Source File

SOURCE=.\TreeditorView.cpp
# End Source File
# Begin Source File

SOURCE=.\UiPropertiesCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\UiProperty.cpp
# End Source File
# Begin Source File

SOURCE=.\UiPropertySubview.cpp
# End Source File
# Begin Source File

SOURCE=.\useAppFrameTreeditor.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\ColorDlg.h
# End Source File
# Begin Source File

SOURCE=.\ColorItem.h
# End Source File
# Begin Source File

SOURCE=.\CompGraphCtrl.h
# End Source File
# Begin Source File

SOURCE=.\ComponentBar.h
# End Source File
# Begin Source File

SOURCE=.\CurveDlg.h
# End Source File
# Begin Source File

SOURCE=.\CurveItem.h
# End Source File
# Begin Source File

SOURCE=.\DebugBar.h
# End Source File
# Begin Source File

SOURCE=.\geAutoArray.h
# End Source File
# Begin Source File

SOURCE=.\GraphBar.h
# End Source File
# Begin Source File

SOURCE=.\GraphCtrl.h
# End Source File
# Begin Source File

SOURCE=.\InfoBar.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\PairDlg.h
# End Source File
# Begin Source File

SOURCE=.\PaletteEditor.h
# End Source File
# Begin Source File

SOURCE=.\PreferencesDlg.h
# End Source File
# Begin Source File

SOURCE=.\PropertyBar.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\ScriptParser.h
# End Source File
# Begin Source File

SOURCE=.\SelectChildKindDlg.h
# End Source File
# Begin Source File

SOURCE=.\StaticGround.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\TextEditor.h
# End Source File
# Begin Source File

SOURCE=.\Treeditor.h
# End Source File
# Begin Source File

SOURCE=.\TreeditorClass.h
# End Source File
# Begin Source File

SOURCE=.\TreeditorDoc.h
# End Source File
# Begin Source File

SOURCE=.\TreeditorLex.h
# End Source File
# Begin Source File

SOURCE=.\TreeditorView.h
# End Source File
# Begin Source File

SOURCE=.\UiPropertiesCtrl.h
# End Source File
# Begin Source File

SOURCE=.\UiProperty.h
# End Source File
# Begin Source File

SOURCE=.\UiPropertySubview.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\lupazoom.cur
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Treeditor.ico
# End Source File
# Begin Source File

SOURCE=.\res\Treeditor.rc2
# End Source File
# Begin Source File

SOURCE=.\res\TreeditorDoc.ico
# End Source File
# End Group
# Begin Group "Es"

# PROP Default_Filter ""
# Begin Group "Algorithms"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Es\Algorithms\bSearch.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Algorithms\crc32.cpp
# End Source File
# Begin Source File

SOURCE=..\Es\Algorithms\crc32.h
# End Source File
# Begin Source File

SOURCE=..\Es\Algorithms\qsort.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Algorithms\splineEqD.hpp
# End Source File
# End Group
# Begin Group "EssenceCommon"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Es\Common\filenames.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Common\fltopts.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Common\global.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Common\mathND.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Common\platform.cpp
# End Source File
# Begin Source File

SOURCE=..\Es\Common\win.h
# End Source File
# End Group
# Begin Group "Containers"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Es\Containers\array.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\array2D.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\bankArray.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\bankInitArray.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\bigArray.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\boolArray.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\cachelist.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\compactBuf.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\hashMap.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\list.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\maps.cpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\maps.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\quadtree.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\rStringArray.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\smallArray.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\sortedArray.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\staticArray.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\streamArray.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\typeDefines.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\typeGenTemplates.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\typeOpts.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\typeTemplates.hpp
# End Source File
# End Group
# Begin Group "Framework"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Es\Framework\appFrame.cpp
# End Source File
# Begin Source File

SOURCE=..\Es\Framework\appFrame.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Framework\debugLog.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Framework\logflags.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Framework\netlog.cpp
# End Source File
# Begin Source File

SOURCE=..\Es\Framework\netlog.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Framework\optDefault.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Framework\optEnable.hpp
# End Source File
# End Group
# Begin Group "Memory"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Es\Memory\checkMem.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Memory\debugNew.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Memory\defNew.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Memory\defNormNew.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Memory\fastAlloc.cpp
# End Source File
# Begin Source File

SOURCE=..\Es\Memory\fastAlloc.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Memory\memAlloc.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Memory\normalNew.hpp
# End Source File
# End Group
# Begin Group "Strings"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Es\Strings\bstring.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Strings\rString.cpp
# End Source File
# Begin Source File

SOURCE=..\Es\Strings\rString.hpp
# End Source File
# End Group
# Begin Group "Types"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Es\Types\enum_decl.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Types\lLinks.cpp
# End Source File
# Begin Source File

SOURCE=..\Es\Types\lLinks.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Types\memtype.h
# End Source File
# Begin Source File

SOURCE=..\Es\Types\pointers.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Types\removeLinks.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Types\scopeLock.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Types\softLinks.cpp
# End Source File
# Begin Source File

SOURCE=..\Es\Types\softLinks.hpp
# End Source File
# End Group
# Begin Group "Files"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Es\Files\fileContainer.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Files\filenames.cpp
# End Source File
# Begin Source File

SOURCE=..\Es\Files\filenames.hpp
# End Source File
# End Group
# Begin Source File

SOURCE=..\Es\essencepch.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\platform.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Threads\threadSync.cpp
# End Source File
# Begin Source File

SOURCE=..\Es\Threads\threadSync.hpp
# End Source File
# End Group
# Begin Group "TreeResources"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\ground.bmp
# End Source File
# Begin Source File

SOURCE=.\kura.bmp
# End Source File
# Begin Source File

SOURCE=.\list3.tga
# End Source File
# Begin Source File

SOURCE=.\tree.txt
# End Source File
# End Group
# Begin Group "El"

# PROP Default_Filter ""
# Begin Group "ElementCommon"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\Common\enumNames.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Common\globalAlive.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Common\isaac.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Common\perfLog.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Common\randomGen.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Common\randomGen.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Common\randomJames.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Common\randomJames.h
# End Source File
# End Group
# Begin Group "Evaluator"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\Evaluator\evaluatorExpress.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Evaluator\evaluatorExpress.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Evaluator\evalUseLocalizeStringtable.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Evaluator\express.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Evaluator\express.hpp
# End Source File
# End Group
# Begin Group "ParamFile"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\ParamFile\classDbParamFile.cpp
# End Source File
# Begin Source File

SOURCE=..\El\ParamFile\classDbParamFile.hpp
# End Source File
# Begin Source File

SOURCE=..\El\ParamFile\paramFile.cpp
# End Source File
# Begin Source File

SOURCE=..\El\ParamFile\paramFile.hpp
# End Source File
# Begin Source File

SOURCE=..\El\ParamFile\paramFileCtx.cpp
# End Source File
# Begin Source File

SOURCE=..\El\ParamFile\paramFileCtx.hpp
# End Source File
# Begin Source File

SOURCE=..\El\ParamFile\paramFileUseEvalExpress.cpp
# End Source File
# Begin Source File

SOURCE=..\El\ParamFile\paramFileUseLocalizeStringtable.cpp
# End Source File
# Begin Source File

SOURCE=..\El\ParamFile\paramFileUsePreprocC.cpp
# End Source File
# Begin Source File

SOURCE=..\El\ParamFile\paramFileUseSumCalcDefault.cpp
# End Source File
# End Group
# Begin Group "Pch"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\PCH\afxConfig.h
# End Source File
# Begin Source File

SOURCE=..\El\PCH\ext_options.hpp
# End Source File
# Begin Source File

SOURCE=..\El\PCH\libIncludes.hpp
# End Source File
# Begin Source File

SOURCE=..\El\PCH\normalConfig.h
# End Source File
# Begin Source File

SOURCE=..\El\PCH\sReleaseConfig.h
# End Source File
# Begin Source File

SOURCE=..\El\PCH\stdIncludes.h
# End Source File
# End Group
# Begin Group "Stringtable"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\Stringtable\localizeStringtable.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Stringtable\localizeStringtable.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Stringtable\stringtable.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Stringtable\stringtable.hpp
# End Source File
# End Group
# Begin Group "Xml"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\XML\xml.cpp
# End Source File
# Begin Source File

SOURCE=..\El\XML\xml.hpp
# End Source File
# End Group
# Begin Group "QStream"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\QStream\fileCompress.cpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\fileCompress.hpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\fileinfo.h
# End Source File
# Begin Source File

SOURCE=..\El\QStream\fileMapping.cpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\fileMapping.hpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\fileOverlapped.cpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\fileOverlapped.hpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\qbstream.cpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\QBStream.hpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\qstream.cpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\QStream.hpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\qstreamUseBankQFBank.cpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\qstreamUseFServerDefault.cpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\serializeBin.cpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\serializeBin.hpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\ssCompress.cpp
# End Source File
# End Group
# Begin Group "Math"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\Math\math3d.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Math\math3dK.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Math\math3dK.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Math\math3dP.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Math\math3dP.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Math\math3DPK.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Math\math3dT.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Math\math3dT.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Math\mathDefs.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Math\mathOpt.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Math\mathOpt.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Math\matrix.asm
# End Source File
# Begin Source File

SOURCE=..\El\Math\matrixP3.cpp
# End Source File
# End Group
# Begin Group "Interfaces"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\Interfaces\iPreproc.hpp
# End Source File
# End Group
# Begin Group "Modules"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\Modules\modules.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Modules\modules.hpp
# End Source File
# End Group
# Begin Group "PreprocC"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\PreprocC\Preproc.cpp
# End Source File
# Begin Source File

SOURCE=..\El\PreprocC\Preproc.h
# End Source File
# Begin Source File

SOURCE=..\El\PreprocC\preprocC.cpp
# End Source File
# Begin Source File

SOURCE=..\El\PreprocC\preprocC.hpp
# End Source File
# End Group
# Begin Group "ParamArchive"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\ParamArchive\paramArchive.cpp
# End Source File
# Begin Source File

SOURCE=..\El\ParamArchive\paramArchive.hpp
# End Source File
# Begin Source File

SOURCE=..\El\ParamArchive\paramArchiveUseDbParamFile.cpp
# End Source File
# End Group
# Begin Group "Debugging"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\Debugging\debugTrap.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Debugging\debugTrap.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Debugging\imexhnd.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Debugging\imexhnd.h
# End Source File
# Begin Source File

SOURCE=..\El\Debugging\mapFile.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Debugging\mapFile.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Debugging\module.xml
# End Source File
# End Group
# Begin Source File

SOURCE=..\El\CRC\crc.cpp
# End Source File
# Begin Source File

SOURCE=..\El\CRC\crc.hpp
# End Source File
# Begin Source File

SOURCE=..\El\elementpch.hpp
# End Source File
# Begin Source File

SOURCE=..\El\AppInfo\useAppInfoDefault.cpp
# End Source File
# End Group
# Begin Group "RV"

# PROP Default_Filter ""
# Begin Group "Tree"

# PROP Default_Filter ""
# Begin Group "ObjShape"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\RV\Tree\ObjShape\data3d.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\ObjShape\Fileutil.c
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\ObjShape\Fileutil.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\ObjShape\Macros.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\ObjShape\objAnim.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\ObjShape\ObjLOD.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\ObjShape\ObjLOD.hpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\ObjShape\objMass.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\ObjShape\Objobje.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\ObjShape\objobje.hpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\ObjShape\ObjSelect.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\ObjShape\types3d.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\ObjShape\types3d.hpp
# End Source File
# End Group
# Begin Source File

SOURCE=..\RV\Tree\auxiliary.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\auxiliary.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\bbox.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\branch.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\branch.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\branchBunch.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\branchBunch.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\branchCane.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\branchCane.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\branchLeaf.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\branchLeaf.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\branchLeafStalk.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\branchLeafStalk.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\branchSimpleGutter.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\branchSimpleGutter.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\branchSplitter.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\branchSplitter.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\branchStem.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\branchStem.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\branchTree.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\branchTree.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\component.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\component.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\componentFern.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\componentFern.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\componentScriptBunch.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\componentScriptBunch.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\componentScriptEmpty.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\componentScriptEmpty.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\componentScriptLeaf.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\componentScriptLeaf.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\componentScriptStem.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\componentScriptStem.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\componentScriptTree.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\componentScriptTree.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\componentTest.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\componentTest.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\componentTree.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\componentTree.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\cutMaterial.cpp

!IF  "$(CFG)" == "Treeditor - Win32 Debug Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static NoCopyRects"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\RV\Tree\cutMaterial.hpp

!IF  "$(CFG)" == "Treeditor - Win32 Debug Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static NoCopyRects"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\RV\Tree\cutParamFileExt.cpp

!IF  "$(CFG)" == "Treeditor - Win32 Debug Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static NoCopyRects"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\RV\Tree\cutParamFileExt.hpp

!IF  "$(CFG)" == "Treeditor - Win32 Debug Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static NoCopyRects"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\RV\Tree\cutPlane.cpp

!IF  "$(CFG)" == "Treeditor - Win32 Debug Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static NoCopyRects"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\RV\Tree\cutPlane.hpp

!IF  "$(CFG)" == "Treeditor - Win32 Debug Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static NoCopyRects"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\RV\Tree\cutPoly.cpp

!IF  "$(CFG)" == "Treeditor - Win32 Debug Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static NoCopyRects"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\RV\Tree\cutPoly.hpp

!IF  "$(CFG)" == "Treeditor - Win32 Debug Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static NoCopyRects"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\RV\Tree\cutShape.cpp

!IF  "$(CFG)" == "Treeditor - Win32 Debug Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static NoCopyRects"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\RV\Tree\cutShape.hpp

!IF  "$(CFG)" == "Treeditor - Win32 Debug Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static NoCopyRects"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\RV\Tree\cutTextbank.hpp

!IF  "$(CFG)" == "Treeditor - Win32 Debug Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static NoCopyRects"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\RV\Tree\cutTypes.hpp

!IF  "$(CFG)" == "Treeditor - Win32 Debug Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static NoCopyRects"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\RV\Tree\cutVertex.cpp

!IF  "$(CFG)" == "Treeditor - Win32 Debug Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static NoCopyRects"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\RV\Tree\cutVertex.hpp

!IF  "$(CFG)" == "Treeditor - Win32 Debug Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Treeditor - Win32 Release Static NoCopyRects"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\RV\Tree\d3dutil.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\d3dutil.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\dxutil.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\dxutil.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\Macros.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\mytree.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\mytree.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\picture.hpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\pointList.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\Polyplane.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\Polyplane.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\PolyplaneBlock.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\PolyplaneBlock.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\PolyplaneBlockBend.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\PolyplaneBlockBend.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\PolyplaneBlockNorm.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\PolyplaneBlockNorm.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\PolyplaneBunch.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\PolyplaneBunch.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\primitive.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\primitive.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\primitiveBunch.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\primitiveBunch.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\primitiveLeaf.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\primitiveLeaf.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\primitiveStem.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\primitiveStem.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\primitivestream.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\primitivestream.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\primitiveTest.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\primitiveTest.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\primitiveTree.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\primitiveTree.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\randomFlyman.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\randomFlyman.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\vectorSpace.cpp
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\vectorSpace.h
# End Source File
# Begin Source File

SOURCE=..\RV\Tree\Picture.lib
# End Source File
# End Group
# End Group
# Begin Group "Shaders"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Tree\Shaders\branch.psh
# End Source File
# Begin Source File

SOURCE=.\Tree\Shaders\branch.vsh
# End Source File
# Begin Source File

SOURCE=.\Tree\Shaders\color.psh
# End Source File
# Begin Source File

SOURCE=.\Tree\Shaders\color.vsh
# End Source File
# Begin Source File

SOURCE=.\Tree\Shaders\normal.psh
# End Source File
# Begin Source File

SOURCE=.\Tree\Shaders\normal.vsh
# End Source File
# Begin Source File

SOURCE=.\Tree\Shaders\triplane.psh
# End Source File
# Begin Source File

SOURCE=.\Tree\Shaders\triplane.vsh
# End Source File
# End Group
# Begin Source File

SOURCE=.\Bugs.txt
# End Source File
# Begin Source File

SOURCE=.\geAutoArray.tli
# End Source File
# End Target
# End Project
