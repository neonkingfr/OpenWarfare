#include "stdafx.h"
#include <El/elementpch.hpp>
#include <Es/essencepch.hpp>
#include <Es/Framework/appFrame.hpp>
#include "RV/Tree/mytree.h"
#include "Treeditor.h"
#include "MainFrm.h"

#if _MSC_VER && !defined INIT_SEG_COMPILER
	#pragma warning(disable:4074)
	#pragma init_seg(compiler)
	#define INIT_SEG_COMPILER
#endif

class TreeditorFrameFunctions : public AppFrameFunctions
{
public:
	TreeditorFrameFunctions() {
    // Destroy contents of the file
    FILE *f = fopen("Treeditor.log", "w");
    if (f != NULL) {
      fclose(f);
    }
  };
	virtual ~TreeditorFrameFunctions() {};

#if _ENABLE_REPORT
/*
	virtual void LogF(const char *format, va_list argptr)
	{
		if ((!theApp._isExitting) && (AfxGetMainWnd() != NULL))
		{
			((CMainFrame*) AfxGetMainWnd())->_debugBar.LogF(format, argptr);
		}
		else
		{
			AppFrameFunctions::LogF (format, argptr);
		}
	}
*/
  // File logging
  virtual void LogF(const char *format, va_list argptr)
	{
    
    // Log to file
    FILE *f = fopen("Treeditor.log", "a");
    if (f != NULL) {
		  char buf[1024];
		  vsprintf(buf, format, argptr);
		  strcat(buf,"\r\n");
      fprintf(f, buf);
      fclose(f);
    }

    // Log to window
		if ((!theApp._isExitting) && (AfxGetMainWnd() != NULL))
		{
			((CMainFrame*) AfxGetMainWnd())->_debugBar.LogF(format, argptr);
		}
		else
		{
			AppFrameFunctions::LogF (format, argptr);
		}
	}

#endif
};

static TreeditorFrameFunctions GAppFrameFunctions INIT_PRIORITY_URGENT;
AppFrameFunctions *CurrentAppFrameFunctions = &GAppFrameFunctions;
