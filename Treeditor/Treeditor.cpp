// Treeditor.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include <El/elementpch.hpp>
#include <El/Evaluator/express.hpp>
#include <El/Modules/modules.hpp>
#include "RV/Tree/mytree.h"
#include "Treeditor.h"
#include "TreeditorLex.h"
#include "TreeditorClass.h"

#include "MainFrm.h"
#include "TreeditorDoc.h"
#include "TreeditorView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTreeditorApp

BEGIN_MESSAGE_MAP(CTreeditorApp, CWinApp)
	//{{AFX_MSG_MAP(CTreeditorApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTreeditorApp construction

CTreeditorApp::CTreeditorApp()
	: m_d3d8 (NULL), m_d3dDevice8 (NULL), m_d3dGroundVertexBuffer (NULL),
	m_d3dGroundTexture (NULL), m_d3dMeterVertexBuffer (NULL), 
	m_d3dLightVertexBuffer (NULL), m_d3dLightPyrVertexBuffer (NULL), m_d3dLightPyrIndexBuffer (NULL),
	m_d3dWindVertexBuffer (NULL), m_d3dWindPyrVertexBuffer (NULL), m_d3dWindPyrIndexBuffer (NULL),
	TreeEngine (NULL), m_resetDevice (false), _isExitting (false)
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CTreeditorApp object

CTreeditorApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CTreeditorApp initialization

BOOL CTreeditorApp::InitInstance()
{
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	SetRegistryKey(_T("Treeditor Application"));

	LoadStdProfileSettings(GetProfileInt (_T ("editor"), _T ("recentFiles"), 8));  // Load standard INI file options (including MRU)

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CTreeditorDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CTreeditorView));
	AddDocTemplate(pDocTemplate);

  // Initialize defined script modules
	InitModules();

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	// Dispatch commands specified on the command line
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The one and only window has been initialized, so show and update it.
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();

	// Enable drag/drop open.
	m_pMainWnd->DragAcceptFiles();

	return TRUE;
}

bool CTreeditorApp::InitD3DResources ()
{
	// Create ground vertex buffer
	HRESULT hr = m_d3dDevice8->CreateVertexBuffer (sizeof (GROUNDVERTEX) * 4, D3DUSAGE_WRITEONLY, 
		D3DFVF_GROUNDVERTEX, D3DPOOL_DEFAULT, &m_d3dGroundVertexBuffer);
	if (FAILED (hr))
	{
		AfxMessageBox (_T ("Ground vertex buffer failed."), MB_OK);
		PostQuitMessage (hr);
		return false;
	}

	// Create meter vertex buffer
	hr = m_d3dDevice8->CreateVertexBuffer (sizeof (METERVERTEX) * 24 * MAX_TREES, D3DUSAGE_WRITEONLY, 
		D3DFVF_METERVERTEX, D3DPOOL_DEFAULT, &m_d3dMeterVertexBuffer);
	if (FAILED (hr))
	{
		AfxMessageBox (_T ("Meter vertex buffer failed."), MB_OK);
		PostQuitMessage (hr);
		return false;
	}

	// Create light vertex buffer
	hr = m_d3dDevice8->CreateVertexBuffer (sizeof (LIGHTVERTEX) * 18, D3DUSAGE_WRITEONLY, 
		D3DFVF_LIGHTVERTEX, D3DPOOL_DEFAULT, &m_d3dLightVertexBuffer);
	if (FAILED (hr))
	{
		AfxMessageBox (_T ("Light vertex buffer failed."), MB_OK);
		PostQuitMessage (hr);
		return false;
	}

	// Create light pyr vertex buffer
	hr = m_d3dDevice8->CreateVertexBuffer (sizeof (LIGHTPYRVERTEX) * 5, D3DUSAGE_WRITEONLY, 
		D3DFVF_LIGHTPYRVERTEX, D3DPOOL_DEFAULT, &m_d3dLightPyrVertexBuffer);
	if (FAILED (hr))
	{
		AfxMessageBox (_T ("Light pyramid vertex buffer failed."), MB_OK);
		PostQuitMessage (hr);
		return false;
	}

	// Create light pyr index buffer
	hr = m_d3dDevice8->CreateIndexBuffer (2 * 8 * 2, D3DUSAGE_WRITEONLY , 
		D3DFMT_INDEX16, D3DPOOL_DEFAULT, &m_d3dLightPyrIndexBuffer);
	if (FAILED (hr))
	{
		AfxMessageBox (_T ("Light pyramid index buffer failed."), MB_OK);
		PostQuitMessage (hr);
		return false;
	}

	// Create wind vertex buffer
	hr = m_d3dDevice8->CreateVertexBuffer (sizeof (WINDVERTEX) * 18, D3DUSAGE_WRITEONLY, 
		D3DFVF_WINDVERTEX, D3DPOOL_DEFAULT, &m_d3dWindVertexBuffer);
	if (FAILED (hr))
	{
		AfxMessageBox (_T ("Wind vertex buffer failed."), MB_OK);
		PostQuitMessage (hr);
		return false;
	}
	
	// Create wind pyr vertex buffer
	hr = m_d3dDevice8->CreateVertexBuffer (sizeof (WINDPYRVERTEX) * 5, D3DUSAGE_WRITEONLY, 
		D3DFVF_WINDPYRVERTEX, D3DPOOL_DEFAULT, &m_d3dWindPyrVertexBuffer);
	if (FAILED (hr))
	{
		AfxMessageBox (_T ("Wind pyramid vertex buffer failed."), MB_OK);
		PostQuitMessage (hr);
		return false;
	}

	// Create wind pyr index buffer
	hr = m_d3dDevice8->CreateIndexBuffer (2 * 8 * 2, D3DUSAGE_WRITEONLY, 
		D3DFMT_INDEX16, D3DPOOL_DEFAULT, &m_d3dWindPyrIndexBuffer);
	if (FAILED (hr))
	{
		AfxMessageBox (_T ("Wind pyramid index buffer failed."), MB_OK);
		PostQuitMessage (hr);
		return false;
	}

	int n = _arrows.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (!Create3DArrow (_arrows[i]))
		{
			AfxMessageBox (_T ("Creating arrow failed."), MB_OK);
			PostQuitMessage (hr);
			return false;
		}
	}

	// Create tree engine
	TreeEngine = new CTreeEngine ();
	if (TreeEngine == NULL)
	{
		AfxMessageBox (_T ("CTreeEngine failed (not enough memory)."), MB_OK);
		PostQuitMessage (-1);
		return false;
	}

  m_d3dDevice8->AddRef();
	TreeEngine->Init(ComRef<struct IDirect3DDevice8>(m_d3dDevice8));
	
	return true;
}

BOOL CTreeditorApp::InitD3D (CWnd *rv)
{
	if (m_d3d8 != NULL && m_d3dDevice8 != NULL)
	{
		return TRUE;
	}

	ExitD3D ();

    // Create the Direct3D object
    m_d3d8 = Direct3DCreate8 (D3D_SDK_VERSION);
    if (m_d3d8 == NULL)
	{
        return FALSE;
	}

	D3DPRESENT_PARAMETERS d3dpp; 

	// Get default display mode
	D3DDISPLAYMODE mode;
	HRESULT hr = m_d3d8->GetAdapterDisplayMode (D3DADAPTER_DEFAULT, &mode);
	if (FAILED (hr))
	{
		return FALSE;
	}
	
	// Set Direct3D parameters
	ZeroMemory (&d3dpp, sizeof (d3dpp));
	d3dpp.Windowed = TRUE;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.BackBufferFormat = mode.Format;
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;

	// Create device
	hr = m_d3d8->CreateDevice (D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, rv->GetSafeHwnd (),
		D3DCREATE_MIXED_VERTEXPROCESSING, &d3dpp, &m_d3dDevice8);
	if (FAILED (hr))
	{
		return FALSE;
	}

	if (!InitD3DResources ())
	{
		return FALSE;
	}
	
	return TRUE;
}

int CTreeditorApp::ExitInstance() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	_isExitting = true;
	int exitCode = CWinApp::ExitInstance();

	ExitD3D ();

	return exitCode;
}

void CTreeditorApp::ReleaseD3DResources ()
{
	if (TreeEngine != NULL)
	{
		delete TreeEngine;
		TreeEngine = NULL;
	}

	if (m_d3dWindPyrIndexBuffer != NULL)
	{
		m_d3dWindPyrIndexBuffer->Release ();
		m_d3dWindPyrIndexBuffer = NULL;
	}

	if (m_d3dWindPyrVertexBuffer != NULL)
	{
		m_d3dWindPyrVertexBuffer->Release ();
		m_d3dWindPyrVertexBuffer = NULL;
	}

	if (m_d3dWindVertexBuffer != NULL)
	{
		m_d3dWindVertexBuffer->Release ();
		m_d3dWindVertexBuffer = NULL;
	}

	if (m_d3dLightPyrIndexBuffer != NULL)
	{
		m_d3dLightPyrIndexBuffer->Release ();
		m_d3dLightPyrIndexBuffer = NULL;
	}

	if (m_d3dLightPyrVertexBuffer != NULL)
	{
		m_d3dLightPyrVertexBuffer->Release ();
		m_d3dLightPyrVertexBuffer = NULL;
	}

	if (m_d3dLightVertexBuffer != NULL)
	{
		m_d3dLightVertexBuffer->Release ();
		m_d3dLightVertexBuffer = NULL;
	}

	if (m_d3dMeterVertexBuffer != NULL)
	{
		m_d3dMeterVertexBuffer->Release ();
		m_d3dMeterVertexBuffer = NULL;
	}

	if (m_d3dGroundTexture != NULL)
	{
		m_d3dGroundTexture->Release ();
		m_d3dGroundTexture = NULL;
	}

	if (m_d3dGroundVertexBuffer != NULL)
	{
		m_d3dGroundVertexBuffer->Release ();
		m_d3dGroundVertexBuffer = NULL;
	}

	int n = _arrowIndexBuffers.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		_arrowIndexBuffers[i]->Release ();
		_arrowVertexBuffers[i]->Release ();
	}
	_arrowIndexBuffers.RemoveAll ();
	_arrowVertexBuffers.RemoveAll ();
}

void CTreeditorApp::ExitD3D ()
{
	ReleaseD3DResources ();

	if (m_d3dDevice8 != NULL)
	{
		m_d3dDevice8->Release ();
		m_d3dDevice8 = NULL;
	}

	if (m_d3d8 != NULL)
	{
		m_d3d8->Release ();
		m_d3d8 = NULL;
	}
}

void CTreeditorApp::ResetD3D ()
{
	if (m_d3d8 == NULL || m_d3dDevice8 == NULL)
	{
		return;
	}

	ReleaseD3DResources ();

	D3DPRESENT_PARAMETERS d3dpp;
	
	// Get default display mode
	D3DDISPLAYMODE mode;
	HRESULT hr = m_d3d8->GetAdapterDisplayMode (D3DADAPTER_DEFAULT, &mode);
	if (FAILED (hr))
	{
		ExitD3D ();
		return;
	}
	
	// Set Direct3D parameters
	ZeroMemory (&d3dpp, sizeof (d3dpp));
	d3dpp.Windowed = TRUE;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.BackBufferFormat = mode.Format;
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;

	hr = m_d3dDevice8->Reset (&d3dpp);
	if (FAILED (hr))
	{
		ExitD3D ();
		return;
	}

	if (!InitD3DResources ())
	{
		ExitD3D ();
		return;
	}
}


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
		// No message handlers
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CTreeditorApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CTreeditorApp message handlers


int CTreeditorApp::Run() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	if (m_pMainWnd == NULL && AfxOleGetUserCtrl())
	{
		// Not launched /Embedding or /Automation, but has no main window!
		TRACE0("Warning: m_pMainWnd is NULL in CWinApp::Run - quitting application.\n");
		AfxPostQuitMessage(0);
	}

	ASSERT_VALID(this);

	// for tracking the idle time state
	BOOL bIdle = TRUE;
	LONG lIdleCount = 0;

	// acquire and dispatch messages until a WM_QUIT message is received.
	for (;;)
	{
		// phase1: check to see if we can do idle work
		while (bIdle &&
			!::PeekMessage(&m_msgCur, NULL, NULL, NULL, PM_NOREMOVE))
		{
			// call OnIdle while in bIdle state
			if (!OnIdle(lIdleCount++))
				bIdle = FALSE; // assume "no idle" state

			AnimateTree ();
		}

		// phase2: pump messages while available
		do
		{
			do {
				AnimateTree ();
			} while (!::PeekMessage(&m_msgCur, NULL, NULL, NULL, PM_NOREMOVE));
			
			// pump message, but quit on WM_QUIT
			if (!PumpMessage())
				return ExitInstance();

			// reset "no idle" state after pumping "normal" message
			if (IsIdleMessage(&m_msgCur))
			{
				bIdle = TRUE;
				lIdleCount = 0;
			}
		} while (::PeekMessage(&m_msgCur, NULL, NULL, NULL, PM_NOREMOVE));
	}

	ASSERT(FALSE);  // not reachable
}

void CTreeditorApp::AnimateTree ()
{
	POSITION dtPos = GetFirstDocTemplatePosition ();
	if (dtPos != NULL)
	{
		CDocTemplate *dt = GetNextDocTemplate (dtPos);
		CString str;
		if (dt->GetDocString (str, CDocTemplate::fileNewName) && str == "Tree")
		{
			POSITION docPos = dt->GetFirstDocPosition ();
			if (docPos != NULL)
			{
				CDocument *doc = dt->GetNextDoc (docPos);
				POSITION viewPos = doc->GetFirstViewPosition ();
				if (NULL != viewPos)
				{
					CTreeditorView *view = (CTreeditorView*) doc->GetNextView (viewPos);
					view->NextFrame ();
				}
			}
		}
	}
}

BOOL CTreeditorApp::OnIdle(LONG lCount) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	if (CWinApp::OnIdle(lCount))
	{
		return TRUE;
	}

	if (m_d3dDevice8 == NULL || m_resetDevice)
	{
		CTreeditorView *view = (CTreeditorView*) ((CFrameWnd*) AfxGetMainWnd())->GetActiveView ();
		if (view != NULL)
		{
			if (m_d3dDevice8 != NULL)
			{
				view->ResetRender ();
			}
			view->GetDocument ()->UpdateStamp (true, false);
			view->UpdateRender (INIT_METER | INIT_TREE | INIT_GROUND | INIT_LIGHT_AND_WIND);
			m_resetDevice = false;
		}
	}

	return FALSE;
}

bool CTreeditorApp::Create3DArrow(const CArrow3DInfo &info)
{
	IDirect3DVertexBuffer8 *vertices;
	IDirect3DIndexBuffer8 *indices;

	float halfWidth = info.width / 2.0f;
	float halfTipWidth = info.tipWidth / 2.0f;
	float bodyLength = info.length - info.tipLength;
	float tipEdgeLength = (float) sqrt (info.tipLength * info.tipLength + halfTipWidth * halfTipWidth);

	HRESULT hr = m_d3dDevice8->CreateVertexBuffer (sizeof (ARROWVERTEX) * 40, D3DUSAGE_WRITEONLY, 
		D3DFVF_ARROWVERTEX, D3DPOOL_DEFAULT, &vertices);

	ARROWVERTEX *vertexPtr;
	hr = vertices->Lock (0, 0, (BYTE**) &vertexPtr, 0);

	D3DXVECTOR3 tv3;
#define K 0
#define TP(dst,vx,vy,vz,vnx,vny,vnz,col)\
	{\
		tv3.x = vx; tv3.y = vy; tv3.z = vz;\
		D3DXVec3TransformCoord (&tv3, &tv3, &info.origin);\
		dst.x = tv3.x;\
		dst.y = tv3.y;\
		dst.z = tv3.z;\
		tv3.x = vnx; tv3.y = vny; tv3.z = vnz;\
		D3DXVec3TransformNormal (&tv3, &tv3, &info.origin);\
		dst.nx = tv3.x;\
		dst.ny = tv3.y;\
		dst.nz = tv3.z;\
		dst.color = col;\
	}

	// bottom polygon
	TP(vertexPtr[K + 0], halfWidth, 0.0f, halfWidth, 0, -1, 0, info.color)
	TP(vertexPtr[K + 1], halfWidth, 0, -halfWidth, 0, -1, 0, info.color)
	TP(vertexPtr[K + 2], -halfWidth, 0, -halfWidth, 0, -1, 0, info.color)
	TP(vertexPtr[K + 3], -halfWidth, 0, halfWidth, 0, -1, 0, info.color)
#undef K

#define K 4
	// left polygon
	TP(vertexPtr[K + 0], -halfWidth, bodyLength, -halfWidth, -1, 0, 0, info.color)
	TP(vertexPtr[K + 1], -halfWidth, 0, -halfWidth, -1, 0, 0, info.color)
	TP(vertexPtr[K + 2], -halfWidth, 0, halfWidth, -1, 0, 0, info.color)
	TP(vertexPtr[K + 3], -halfWidth, bodyLength, halfWidth, -1, 0, 0, info.color)
#undef K

#define K 8
	// right polygon
	TP(vertexPtr[K + 0], halfWidth, bodyLength, halfWidth, 1, 0, 0, info.color)
	TP(vertexPtr[K + 1], halfWidth, 0, halfWidth, 1, 0, 0, info.color)
	TP(vertexPtr[K + 2], halfWidth, 0, -halfWidth, 1, 0, 0, info.color)
	TP(vertexPtr[K + 3], halfWidth, bodyLength, -halfWidth, 1, 0, 0, info.color)
#undef K

#define K 12
	// front polygon
	TP(vertexPtr[K + 0], halfWidth, bodyLength, -halfWidth, 0, 0, -1, info.color)
	TP(vertexPtr[K + 1], halfWidth, 0, -halfWidth, 0, 0, -1, info.color)
	TP(vertexPtr[K + 2], -halfWidth, 0, -halfWidth, 0, 0, -1, info.color)
	TP(vertexPtr[K + 3], -halfWidth, bodyLength, -halfWidth, 0, 0, -1, info.color)
#undef K

#define K 16
	// back polygon
	TP(vertexPtr[K + 0], -halfWidth, bodyLength, halfWidth, 0, 0, 1, info.color)
	TP(vertexPtr[K + 1], -halfWidth, 0, halfWidth, 0, 0, 1, info.color)
	TP(vertexPtr[K + 2], halfWidth, 0, halfWidth, 0, 0, 1, info.color)
	TP(vertexPtr[K + 3], halfWidth, bodyLength, halfWidth, 0, 0, 1, info.color)
#undef K

#define K 20
	// tip bottom polygon
	TP(vertexPtr[K + 0], halfTipWidth, bodyLength, -halfTipWidth, 0, -1, 0, info.color)
	TP(vertexPtr[K + 1], halfTipWidth, bodyLength, halfTipWidth, 0, -1, 0, info.color)
	TP(vertexPtr[K + 2], -halfTipWidth, bodyLength, halfTipWidth, 0, -1, 0, info.color)
	TP(vertexPtr[K + 3], -halfTipWidth, bodyLength, -halfTipWidth, 0, -1, 0, info.color)
#undef K

#define K 24
	// tip bottom hole
	TP(vertexPtr[K + 0], halfWidth, bodyLength, -halfWidth, 0, -1, 0, info.color)
	TP(vertexPtr[K + 1], halfWidth, bodyLength, halfWidth, 0, -1, 0, info.color)
	TP(vertexPtr[K + 2], -halfWidth, bodyLength, halfWidth, 0, -1, 0, info.color)
	TP(vertexPtr[K + 3], -halfWidth, bodyLength, -halfWidth, 0, -1, 0, info.color)
#undef K

#define K 28
	// tip left polygon
	D3DXVECTOR3 norm;
	norm.x = -info.tipLength / tipEdgeLength;
	norm.y = halfTipWidth / tipEdgeLength;
	norm.z = 0.0f;

	TP(vertexPtr[K + 0], 0, info.length, 0, norm.x, norm.y, norm.z, info.color)
	TP(vertexPtr[K + 1], -halfTipWidth, bodyLength, -halfTipWidth, norm.x, norm.y, norm.z, info.color)
	TP(vertexPtr[K + 2], -halfTipWidth, bodyLength, halfTipWidth, norm.x, norm.y, norm.z, info.color)
#undef K

#define K 31
	// tip right polygon
	norm.x = info.tipLength / tipEdgeLength;
	norm.y = halfTipWidth / tipEdgeLength;
	norm.z = 0.0f;

	TP(vertexPtr[K + 0], 0, info.length, 0, norm.x, norm.y, norm.z, info.color)
	TP(vertexPtr[K + 1], halfTipWidth, bodyLength, halfTipWidth, norm.x, norm.y, norm.z, info.color)
	TP(vertexPtr[K + 2], halfTipWidth, bodyLength, -halfTipWidth, norm.x, norm.y, norm.z, info.color)
#undef K

#define K 34
	// tip front polygon
	norm.x = 0.0f;
	norm.y = halfTipWidth / tipEdgeLength;
	norm.z = -info.tipLength / tipEdgeLength;

	TP(vertexPtr[K + 0], 0, info.length, 0, norm.x, norm.y, norm.z, info.color)
	TP(vertexPtr[K + 1], halfTipWidth, bodyLength, -halfTipWidth, norm.x, norm.y, norm.z, info.color)
	TP(vertexPtr[K + 2], -halfTipWidth, bodyLength, -halfTipWidth, norm.x, norm.y, norm.z, info.color)
#undef K

#define K 37
	// tip back polygon
	norm.x = 0.0f;
	norm.y = halfTipWidth / tipEdgeLength;
	norm.z = info.tipLength / tipEdgeLength;

	TP(vertexPtr[K + 0], 0, info.length, 0, norm.x, norm.y, norm.z, info.color)
	TP(vertexPtr[K + 1], -halfTipWidth, bodyLength, halfTipWidth, norm.x, norm.y, norm.z, info.color)
	TP(vertexPtr[K + 2], halfTipWidth, bodyLength, halfTipWidth, norm.x, norm.y, norm.z, info.color)
#undef K

	hr = vertices->Unlock ();

	hr = m_d3dDevice8->CreateIndexBuffer (66 * 2, D3DUSAGE_WRITEONLY, 
		D3DFMT_INDEX16, D3DPOOL_DEFAULT, &indices);

	short *indexPtr;
	hr = indices->Lock (0, 0, (BYTE**) &indexPtr, 0);

#define A(a, b, c) *indexPtr++ = a; *indexPtr++ = b; *indexPtr++ = c
	// bottom polygon
#define K 0
	A(K + 0, K + 1, K + 2);
	A(K + 2, K + 3, K + 0);
#undef K

	// left polygon
#define K 4
	A(K + 0, K + 1, K + 2);
	A(K + 2, K + 3, K + 0);
#undef K

	// right polygon
#define K 8
	A(K + 0, K + 1, K + 2);
	A(K + 2, K + 3, K + 0);
#undef K

	// front polygon
#define K 12
	A(K + 0, K + 1, K + 2);
	A(K + 2, K + 3, K + 0);
#undef K

	// back polygon
#define K 16
	A(K + 0, K + 1, K + 2);
	A(K + 2, K + 3, K + 0);
#undef K

	// tip bottom polygon
#define K 20
	A(K + 0, K + 1, K + 4);
	A(K + 1, K + 5, K + 4);

	A(K + 1, K + 2, K + 5);
	A(K + 2, K + 6, K + 5);

	A(K + 2, K + 3, K + 6);
	A(K + 3, K + 7, K + 6);

	A(K + 3, K + 0, K + 7);
	A(K + 0, K + 4, K + 7);
#undef K

#define K 28
	A(K + 0, K + 1, K + 2);
#undef K

#define K 31
	A(K + 0, K + 1, K + 2);
#undef K

#define K 34
	A(K + 0, K + 1, K + 2);
#undef K

#define K 37
	A(K + 0, K + 1, K + 2);
#undef K

#undef A

	hr = indices->Unlock ();

	_arrows.Add (info);
	_arrowIndexBuffers.Add (indices);
	_arrowVertexBuffers.Add (vertices);

	return true;
}

void CTreeditorApp::Remove3DArrows ()
{
	int n = _arrowIndexBuffers.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		_arrowIndexBuffers[i]->Release ();
		_arrowVertexBuffers[i]->Release ();
	}
	_arrowIndexBuffers.RemoveAll ();
	_arrowVertexBuffers.RemoveAll ();
	_arrows.RemoveAll ();
}