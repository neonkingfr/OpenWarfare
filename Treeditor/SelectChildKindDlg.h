#if !defined(AFX_SELECTCHILDKINDDLG_H__E14EE9C6_D4DC_4B20_883C_9C365FF7802C__INCLUDED_)
#define AFX_SELECTCHILDKINDDLG_H__E14EE9C6_D4DC_4B20_883C_9C365FF7802C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectChildKindDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelectChildKindDlg dialog

class CTreeditorClass;

class CSelectChildKindDlg : public CDialog
{
// Construction
public:
	CSelectChildKindDlg(CTreeditorClass *beg, CTreeditorClass *end, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSelectChildKindDlg)
	enum { IDD = IDD_SELECTCHILDKINDDLG };
	CComboBox	_childKindCtrl;
	//}}AFX_DATA

	CTreeditorClass *_begin;
	CTreeditorClass *_end;
	CArray<int, int> _childKindCodes;
	int _childKind;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectChildKindDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSelectChildKindDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTCHILDKINDDLG_H__E14EE9C6_D4DC_4B20_883C_9C365FF7802C__INCLUDED_)
