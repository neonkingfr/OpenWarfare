#if !defined(AFX_PAIRDLG_H__E1FE406B_251F_4357_A365_DA79E44F5C26__INCLUDED_)
#define AFX_PAIRDLG_H__E1FE406B_251F_4357_A365_DA79E44F5C26__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PairDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPairDlg dialog

class CPairDlg : public CDialog
{
// Construction
public:
	CPairDlg(LPCTSTR title, LPCTSTR valueLabel, LPCTSTR intervalLabel, float min, float max, float step, const CArray<float, float> &arr, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPairDlg)
	enum { IDD = IDD_PAIR_DLG };
	CStatic	_valueLabelCtrl;
	CStatic	_intervalLabelCtrl;
	CSliderCtrl	_valueSlider;
	CEdit	_valueCtrl;
	CSliderCtrl	_intervalSlider;
	CEdit	_intervalCtrl;
	//}}AFX_DATA


	LPCTSTR _title;
	LPCTSTR _valueLabel;
	LPCTSTR _intervalLabel;
	float _min;
	float _max;
	float _step;
	float _value;
	float _interval;
	float _valueExp;
	float _valueRange;
	float _intervalExp;
	float _intervalRange;
	bool _updateLock;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPairDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	static void CalcSliderExpAndInterval (const float min, const float max, float &exp, float &range);

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CPairDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnChangeInterval();
	afx_msg void OnChangeValue();
	afx_msg void OnKillFocusInterval();
	afx_msg void OnKillFocusValue();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PAIRDLG_H__E1FE406B_251F_4357_A365_DA79E44F5C26__INCLUDED_)
