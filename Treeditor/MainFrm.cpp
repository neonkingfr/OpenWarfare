// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include <El/elementpch.hpp>
#include "RV/Tree/mytree.h"
#include "Treeditor.h"
#include "TreeditorLex.h"
#include "TreeditorClass.h"
#include "TreeditorDoc.h"
#include "TreeditorView.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_COMMAND(ID_VIEW_COMPONENTS, OnViewComponents)
	ON_UPDATE_COMMAND_UI(ID_VIEW_COMPONENTS, OnUpdateViewComponents)
	ON_COMMAND(ID_VIEW_PROPERTIES, OnViewProperties)
	ON_UPDATE_COMMAND_UI(ID_VIEW_PROPERTIES, OnUpdateViewProperties)
	ON_COMMAND(ID_VIEW_GRAPH, OnViewGraph)
	ON_UPDATE_COMMAND_UI(ID_VIEW_GRAPH, OnUpdateViewGraph)
	ON_COMMAND(IDC_GRAPHM_NORMAL, OnGraphmNormal)
	ON_UPDATE_COMMAND_UI(IDC_GRAPHM_NORMAL, OnUpdateGraphmNormal)
	ON_COMMAND(IDC_GRAPHM_DESIGN, OnGraphmDesign)
	ON_UPDATE_COMMAND_UI(IDC_GRAPHM_DESIGN, OnUpdateGraphmDesign)
	ON_COMMAND(IDC_GRAPHM_ZOOM, OnGraphmZoom)
	ON_UPDATE_COMMAND_UI(IDC_GRAPHM_ZOOM, OnUpdateGraphmZoom)
	ON_COMMAND(IDC_CENTER, OnCenter)
	ON_UPDATE_COMMAND_UI(IDC_CENTER, OnUpdateCenter)
	ON_WM_ACTIVATEAPP()
	ON_COMMAND(ID_VIEW_DEBUG, OnViewDebug)
	ON_UPDATE_COMMAND_UI(ID_VIEW_DEBUG, OnUpdateViewDebug)
	ON_COMMAND(ID_EDIT_CLEARDEBUG, OnEditClearDebug)
	ON_COMMAND(ID_VIEW_INFO, OnViewInfo)
	ON_UPDATE_COMMAND_UI(ID_VIEW_INFO, OnUpdateViewInfo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame() : _shiftDown (false), _ctrlDown (false)
{
	// TODO: add member initialization code here
	
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	WINDOWPLACEMENT wp;
	if (ReadWindowPlacement(&wp, _T ("editor"), _T ("framePlace")))
		SetWindowPlacement(&wp);

	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

	// Create property bar
	CSize s;
	CSize *ps = NULL;
	if (ReadSize (s, _T ("editor"), _T ("propertyPlace")))
	{
		ps = &s;
	}
	if (!_propertyBar.Create(this, IDD_PROPERTYBAR,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_SIZE_DYNAMIC, IDD_PROPERTYBAR, ps))
	{
		TRACE0("Failed to create CProperyBar\n");
		return -1;      // fail to create
	}
	_propertyBar.EnableDocking(CBRS_ALIGN_ANY);
	_propertyBar.SetWindowText (_T ("Properties"));
	DockControlBar(&_propertyBar);

	// Create component bar
	if (ReadSize (s, _T ("editor"), _T ("componentPlace")))
	{
		ps = &s;
	}
	if (!_componentBar.Create(this, IDD_COMPONENTBAR,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_SIZE_DYNAMIC, IDD_COMPONENTBAR, ps))
	{
		TRACE0("Failed to create CComponentBar\n");
		return -1;      // fail to create
	}
	_componentBar.EnableDocking(CBRS_ALIGN_ANY);
	_componentBar.SetWindowText (_T ("Components"));
	DockControlBar(&_componentBar);
	
	// Create graph bar
	if (ReadSize (s, _T ("editor"), _T ("graphPlace")))
	{
		ps = &s;
	}
	if (!_graphBar.Create(this, IDD_GRAPHBAR,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_SIZE_DYNAMIC, IDD_GRAPHBAR, ps))
	{
		TRACE0("Failed to create CGraphBar\n");
		return -1;      // fail to create
	}
	_graphBar.EnableDocking(CBRS_ALIGN_ANY);
	_graphBar.SetWindowText (_T ("Parent Graph"));
	DockControlBar(&_graphBar);

	// Create debug bar
	if (ReadSize (s, _T ("editor"), _T ("debugPlace")))
	{
		ps = &s;
	}
	if (!_debugBar.Create(this, IDD_DEBUGBAR,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_SIZE_DYNAMIC, IDD_DEBUGBAR, ps))
	{
		TRACE0("Failed to create CDebugBar\n");
		return -1;      // fail to create
	}
	_debugBar.EnableDocking(CBRS_ALIGN_ANY);
	_debugBar.SetWindowText (_T ("Debug Output"));
	DockControlBar(&_debugBar);

	// Create info bar
	if (ReadSize (s, _T ("editor"), _T ("infobarPlace")))
	{
		ps = &s;
	}
	if (!_infoBar.Create(this, IDD_INFOBAR,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_SIZE_DYNAMIC, IDD_INFOBAR, ps))
	{
		TRACE0("Failed to create CInfoBar\n");
		return -1;      // fail to create
	}
	_infoBar.EnableDocking(CBRS_ALIGN_ANY);
	_infoBar.SetWindowText (_T ("Info"));
	DockControlBar(&_infoBar);

	LoadBarState(_T("barState"));

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers


void CMainFrame::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	
	SaveBarState(_T("barState"));

#define WRITE_WIN_PLACEMENT(win, section, entry)		\
{\
	WINDOWPLACEMENT wp;\
	wp.length = sizeof (wp);\
	if ((win)->GetWindowPlacement(&wp))\
	{\
		wp.flags = 0;\
		if ((win)->IsZoomed())\
			wp.flags |= WPF_RESTORETOMAXIMIZED;\
		/* and write it to the .INI file */\
		WriteWindowPlacement(&wp, section, entry);\
	}\
}

	WRITE_WIN_PLACEMENT(this, _T ("editor"), _T ("framePlace"))
	
	WriteSize (_infoBar.m_sizeFloating, _T ("editor"), _T ("infobarPlace"));
	WriteSize (_debugBar.m_sizeFloating, _T ("editor"), _T ("debugPlace"));
	WriteSize (_graphBar.m_sizeFloating, _T ("editor"), _T ("graphPlace"));
	WriteSize (_componentBar.m_sizeFloating, _T ("editor"), _T ("componentPlace"));
	WriteSize (_propertyBar.m_sizeFloating, _T ("editor"), _T ("propertyPlace"));

	CListCtrl *classes = (CListCtrl*) ((CMainFrame*) AfxGetMainWnd ())->_componentBar.GetDlgItem (IDC_CLASSES);
	if (classes != NULL)
	{
		AfxGetApp ()->WriteProfileInt (_T ("editor"), _T ("classNameColumnWidth"), classes->GetColumnWidth(0));
		AfxGetApp ()->WriteProfileInt (_T ("editor"), _T ("attrColumnWidth"), classes->GetColumnWidth(1));
	}

	CListCtrl *infos = (CListCtrl*) ((CMainFrame*) AfxGetMainWnd ())->_infoBar.GetDlgItem (IDC_INFOLIST);
	if (infos != NULL)
	{
		AfxGetApp ()->WriteProfileInt (_T ("editor"), _T ("infoNameColumnWidth"), infos->GetColumnWidth(0));
		AfxGetApp ()->WriteProfileInt (_T ("editor"), _T ("infoValueColumnWidth"), infos->GetColumnWidth(1));
	}

	CFrameWnd::OnClose();
}

void CMainFrame::WriteSize(const CSize &s, LPCTSTR section, LPCTSTR entry)
{
	TCHAR szBuffer[sizeof(_T ("-32767")) * 2];

	wsprintf(szBuffer, sizeFormat, s.cx, s.cy);
	AfxGetApp()->WriteProfileString(section, entry, szBuffer);
}

BOOL CMainFrame::ReadSize (CSize &s, LPCTSTR section, LPCTSTR entry)
{
	CString strBuffer = AfxGetApp()->GetProfileString(section, entry);
	if (strBuffer.IsEmpty())
		return FALSE;

	SIZE ts;
	int nRead = _stscanf(strBuffer, sizeFormat, &ts.cx, &ts.cy);

	if (nRead != 2)
		return FALSE;

	s = ts;
	return TRUE;
}

void CMainFrame::WriteWindowPlacement(LPWINDOWPLACEMENT pwp, LPCTSTR section, LPCTSTR entry)
{
	TCHAR szBuffer[sizeof(_T ("-32767")) * 8 + sizeof(_T ("65535")) * 2];

	wsprintf(szBuffer, winPlaceFormat,
		pwp->flags, pwp->showCmd,
		pwp->ptMinPosition.x, pwp->ptMinPosition.y,
		pwp->ptMaxPosition.x, pwp->ptMaxPosition.y,
		pwp->rcNormalPosition.left, pwp->rcNormalPosition.top,
		pwp->rcNormalPosition.right, pwp->rcNormalPosition.bottom);
	AfxGetApp()->WriteProfileString(section, entry, szBuffer);
}

const TCHAR CMainFrame::winPlaceFormat[] = _T("%u,%u,%d,%d,%d,%d,%d,%d,%d,%d");
const TCHAR CMainFrame::sizeFormat[] = _T("%d,%d");

BOOL CMainFrame::ReadWindowPlacement (LPWINDOWPLACEMENT pwp, LPCTSTR section, LPCTSTR entry)
{
	CString strBuffer = AfxGetApp()->GetProfileString(section, entry);
	if (strBuffer.IsEmpty())
		return FALSE;

	WINDOWPLACEMENT wp;
	int nRead = _stscanf(strBuffer, winPlaceFormat,
		&wp.flags, &wp.showCmd,
		&wp.ptMinPosition.x, &wp.ptMinPosition.y,
		&wp.ptMaxPosition.x, &wp.ptMaxPosition.y,
		&wp.rcNormalPosition.left, &wp.rcNormalPosition.top,
		&wp.rcNormalPosition.right, &wp.rcNormalPosition.bottom);

	if (nRead != 10)
		return FALSE;

	wp.length = sizeof wp;
	*pwp = wp;
	return TRUE;
}

void CMainFrame::OnViewComponents() 
{
	// TODO: Add your command handler code here
	
	BOOL bVisible = ((_componentBar.GetStyle() & WS_VISIBLE) != 0);

	ShowControlBar(&_componentBar, !bVisible, FALSE);
	RecalcLayout();
}

void CMainFrame::OnUpdateViewComponents(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
	BOOL bVisible = ((_componentBar.GetStyle() & WS_VISIBLE) != 0);
	pCmdUI->SetCheck(bVisible);
}

void CMainFrame::OnViewProperties() 
{
	// TODO: Add your command handler code here
	
	BOOL bVisible = ((_propertyBar.GetStyle() & WS_VISIBLE) != 0);

	ShowControlBar(&_propertyBar, !bVisible, FALSE);
	RecalcLayout();
}

void CMainFrame::OnUpdateViewProperties(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
	BOOL bVisible = ((_propertyBar.GetStyle() & WS_VISIBLE) != 0);
	pCmdUI->SetCheck(bVisible);
}

void CMainFrame::OnViewGraph() 
{
	// TODO: Add your command handler code here
	
	BOOL bVisible = ((_graphBar.GetStyle() & WS_VISIBLE) != 0);

	ShowControlBar(&_graphBar, !bVisible, FALSE);
	RecalcLayout();
}

void CMainFrame::OnUpdateViewGraph(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
	BOOL bVisible = ((_graphBar.GetStyle() & WS_VISIBLE) != 0);
	pCmdUI->SetCheck(bVisible);
}

void CMainFrame::OnGraphmNormal ()
{
	if (::IsWindow (_graphBar._graphCtrl.m_hWnd))
	{
		_graphBar._graphCtrl.Mode (SGRM_NORMAL);
	}
}

void CMainFrame::OnUpdateGraphmNormal(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck (_graphBar._graphCtrl.Mode(SGRM_GETCURRENT) == SGRM_NORMAL);
}

void CMainFrame::OnGraphmDesign ()
{
	if (::IsWindow (_graphBar._graphCtrl.m_hWnd))
	{
		_graphBar._graphCtrl.Mode (SGRM_DESIGN);
	}
}

void CMainFrame::OnUpdateGraphmDesign(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck (_graphBar._graphCtrl.Mode(SGRM_GETCURRENT) == SGRM_DESIGN);
}

void CMainFrame::OnGraphmZoom ()
{
	if (::IsWindow (_graphBar._graphCtrl.m_hWnd))
	{
		_graphBar._graphCtrl.Mode (SGRM_ZOOM);
	}
}

void CMainFrame::OnUpdateGraphmZoom(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck (_graphBar._graphCtrl.Mode(SGRM_GETCURRENT) == SGRM_ZOOM);
}

void CMainFrame::OnActivateApp(BOOL bActive, HTASK hTask) 
{
	CFrameWnd::OnActivateApp(bActive, hTask);
	
	// TODO: Add your message handler code here
	if (bActive)
	{
		CTreeditorView *view = (CTreeditorView*) ((CMainFrame*) AfxGetMainWnd ())->GetActiveView ();
		if (view != NULL)
		{
			view->TreePreviewRefresh (false, true);
		}
	}
}

void CMainFrame::OnViewDebug() 
{
	// TODO: Add your command handler code here
	
	BOOL bVisible = ((_debugBar.GetStyle() & WS_VISIBLE) != 0);

	ShowControlBar(&_debugBar, !bVisible, FALSE);
	RecalcLayout();
}

void CMainFrame::OnUpdateViewDebug(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
	BOOL bVisible = ((_debugBar.GetStyle() & WS_VISIBLE) != 0);
	pCmdUI->SetCheck(bVisible);
}

void CMainFrame::OnCenter ()
{
	if (::IsWindow (_graphBar._graphCtrl.m_hWnd))
	{
		((CMainFrame*) AfxGetMainWnd())->_graphBar.ResetZoom();
	}
}

void CMainFrame::OnUpdateCenter(CCmdUI* pCmdUI)
{
}

BOOL CMainFrame::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_CONTROL:
			_ctrlDown = true;
			break;

		case VK_SHIFT:
			_shiftDown = true;
			break;
		}
	}
	else if (pMsg->message == WM_KEYUP)
	{
		switch (pMsg->wParam)
		{
		case VK_CONTROL:
			_ctrlDown = false;
			break;

		case VK_SHIFT:
			_shiftDown = false;
			break;
		}
	}

	return CFrameWnd::PreTranslateMessage(pMsg);
}

void CMainFrame::OnEditClearDebug() 
{
	// TODO: Add your command handler code here

	CEdit *debOut = (CEdit*) _debugBar.GetDlgItem (IDC_DEBUG_OUTPUT);
	if (debOut != NULL)
	{
		debOut->SetSel (0, INT_MAX, TRUE);
		debOut->Clear ();
	}
}

void CMainFrame::OnViewInfo() 
{
	// TODO: Add your command handler code here
	
	BOOL bVisible = ((_infoBar.GetStyle() & WS_VISIBLE) != 0);

	ShowControlBar(&_infoBar, !bVisible, FALSE);
	RecalcLayout();
}

void CMainFrame::OnUpdateViewInfo(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
	BOOL bVisible = ((_infoBar.GetStyle() & WS_VISIBLE) != 0);
	pCmdUI->SetCheck(bVisible);
}

void CMainFrame::SetStatusText (LPCTSTR text)
{
	m_wndStatusBar.SetPaneText (0, text);
}