// TreeditorDoc.h : interface of the CTreeditorDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_TREEDITORDOC_H__E3838C22_BBF5_4AFA_8D6D_DE36BE0A509B__INCLUDED_)
#define AFX_TREEDITORDOC_H__E3838C22_BBF5_4AFA_8D6D_DE36BE0A509B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#define UH_UNDO 32000


class CTreeditorDoc;

class CTreeditorDocState
{
protected:
	CString _code;

public:
	CTreeditorDocState ();
	~CTreeditorDocState ();

	bool Record (CTreeditorDoc *doc);
	void Reset (CTreeditorDoc *doc);

	void Erase ();
};


class CTreeditorDocUndo
{
public:
	CTreeditorDoc *_doc;
	CTreeditorDocState _before;
	CTreeditorDocState _after;

public:
	CTreeditorDocUndo ()
	{
	}

	~CTreeditorDocUndo ()
	{
	}

	void BeginAction (CTreeditorDoc *doc)
	{
		_doc = doc;
		_before.Record(doc);
	}

	void EndAction ()
	{
		_after.Record(_doc);
	}

	void Undo ()
	{
		_before.Reset(_doc);
	}

	void Redo ()
	{
		_after.Reset (_doc);
	}
};


class CTreeditorScriptTimeStamp
{
protected:
	bool _exists;
	FILETIME _lastWriteTime;
	CString _scriptFileName;

public:
	CTreeditorScriptTimeStamp ()
	{
	}

	CTreeditorScriptTimeStamp (LPCTSTR scriptFile)
		: _scriptFileName (scriptFile)
	{
		if (_scriptFileName[0] == _T ('\"'))
		{
			_scriptFileName = _scriptFileName.Mid (1, _scriptFileName.GetLength () - 2);
		}
		HANDLE file = CreateFile ((LPCTSTR) _scriptFileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
		_exists = file != INVALID_HANDLE_VALUE;
		if (_exists)
		{
			BY_HANDLE_FILE_INFORMATION info;
			if (!GetFileInformationByHandle (file, &info))
			{
				_exists = false;
				CloseHandle (file);
				return;
			}

			_lastWriteTime = info.ftLastWriteTime;
			CloseHandle (file);
		}
	}

	CTreeditorScriptTimeStamp (const CTreeditorScriptTimeStamp &src)
	{
		operator = (src);
	}

	void operator = (const CTreeditorScriptTimeStamp &src)
	{
		_scriptFileName = src._scriptFileName;
		_exists = src._exists;
		_lastWriteTime = src._lastWriteTime;
	}

	bool operator != (const CTreeditorScriptTimeStamp &stamp) const
	{
		if (_scriptFileName != stamp._scriptFileName ||
			_exists != stamp._exists ||
			_exists &&
			(_lastWriteTime.dwHighDateTime != stamp._lastWriteTime.dwHighDateTime ||
			_lastWriteTime.dwLowDateTime != stamp._lastWriteTime.dwLowDateTime))
		{
			return true;
		}

		return false;
	}
};


class CTreeditorDocStamp
{
protected:
	bool _init;
	bool _change;
	CArray<CTreeditorScriptTimeStamp, const CTreeditorScriptTimeStamp&> _scriptStamps;

public:
	CTreeditorDocStamp ()
		: _init (false)
	{
	}

	CTreeditorDocStamp (const CTreeditorDocStamp &src)
	{
		operator = (src);
	}

	void Init (const CArray<CTreeditorClass*, CTreeditorClass*> &classes, bool change);

	bool IsInit () const
	{
		return _init;
	}

	bool operator != (const CTreeditorDocStamp &stamp) const
	{
		if (_init && stamp._init)
		{
			if (_change || stamp._change)
			{
				return true;
			}

			int n = _scriptStamps.GetSize ();
			if (n != stamp._scriptStamps.GetSize ())
			{
				return true;
			}
			for (int i = 0; i < n; ++i)
			{
				if (_scriptStamps[i] != stamp._scriptStamps[i])
				{
					return true;
				}
			}
		}

		return false;
	}

	void operator = (const CTreeditorDocStamp &src)
	{
		_init = src._init;
		if (_init)
		{
			_change = src._change;
			_scriptStamps.RemoveAll ();
			int n = src._scriptStamps.GetSize ();
			for (int i = 0; i < n; ++i)
			{
				_scriptStamps.Add (src._scriptStamps[i]);
			}
		}
	}

	void SetChange (bool c = true)
	{
		_change = c;
	}

	void InitScripts (const CArray<CTreeditorClass*, CTreeditorClass*> &classes);
};


class CTreeditorDoc : public CDocument
{
protected: // create from serialization only
	CTreeditorDoc();
	DECLARE_DYNCREATE(CTreeditorDoc)

// Attributes
private:
	CArray<CTreeditorClass*, CTreeditorClass*> _classes;
	static int _classCounter;

public:
	CArray<CTreeditorDocUndo*, CTreeditorDocUndo*> _undos;
	CArray<CTreeditorDocUndo*, CTreeditorDocUndo*> _redos;
	CTreeditorDocStamp _lastStamp;
	CTreeditorDocStamp _currentStamp;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTreeditorDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual void DeleteContents();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTreeditorDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	CTreeditorClass* AddNewClass (int idx);
	void DeselectAllClasses ();
	bool IsAnyClassSelected ();
	void SetParamInSelection (unsigned long paramNum, const char *val);
	void GetParamInSelection (unsigned long paramNum, CString &val);
	bool ClassNameExists (const char *name);
	void DeleteSelectedClasses ();
	void SetTypeInSelection (int typeId);
	bool AllClassesSaved ();
	CTreeditorClass* FindClass (const char *name);
	CTreeditorClass* GetFirstClass (unsigned long *nextIndex);
	CTreeditorClass* GetNextClass (unsigned long *nextIndex);
	CTreeditorClass* GetFirstSelectedClass (unsigned long *nextIndex);
	CTreeditorClass* GetNextSelectedClass (unsigned long *nextIndex);
	CTreeditorClass* GetClass (unsigned long index) const;
	CTreeditorClass* GetFirstChild (CTreeditorClass *parent, unsigned long *childIt, ETreeditorClassParam *childKind);
	CTreeditorClass* GetNextChild (CTreeditorClass *parent, unsigned long *childIt, ETreeditorClassParam *childKind);
	void SelectClass (unsigned long item);
	bool RenameClass (unsigned long item, const char *name);
	bool IsEmpty ();
	bool InitClasses ();
	void DeleteClasses ();
	void DeleteAllClasses ();
	bool Generate (CString &desc, CString &errMsg);
	bool ParseType (CTreeditorLex &lex, STreeditorTokenDesc &token, CString &errMsg);
	bool ParseParams (CTreeditorLex &lex, STreeditorTokenDesc &token, CString &errMsg);
	bool ParseClass (CTreeditorLex &lex, STreeditorTokenDesc &token, CString &errMsg);
	bool ParseDoc (CTreeditorLex &lex, CString &errMsg);
	bool Parse1Param (CTreeditorLex &lex, STreeditorTokenDesc &token, CString &errMsg);
	bool ParseGraphPosition (CTreeditorLex &lex, STreeditorTokenDesc &token, CString &errMsg);
	unsigned long GetClassesLength ();
	void UpdateClassAttrs ();
	CTreeditorClass* GetSelectedClass ();
	void MoveSelectedClassesUp ();
	void MoveSelectedClassesDown ();
	bool AreAllChildrenSaved (const CTreeditorClass *c, bool &error, CString &errMsg);
	bool AllChildrenExist (const CTreeditorClass *c);
	CTreeditorDocUndo *OpenUndo ();
	void CloseUndo (CTreeditorDocUndo *undo);
	void DisposeUndo (CTreeditorDocUndo *undo);
	void DoUndo ();
	void DoRedo ();
	void UpdateScriptParams ();
	void UpdateStamp (bool paramChange, bool updateScripts);
	bool NeedToUpdatePreview () const;

	void ChangeParam(CTreeditorClass *c, bool recUndo, CTreeditorDocUndo *undo, int paramIdx, LPCTSTR value, bool dirty);
	void ChangeParamInTree(CTreeditorClass *c, bool recUndo, CTreeditorDocUndo *undo, int paramIdx, LPCTSTR value, bool dirty);
	void ChangeParamInScript(CTreeditorClass *c, int paramIdx, LPCTSTR value);

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CTreeditorDoc)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TREEDITORDOC_H__E3838C22_BBF5_4AFA_8D6D_DE36BE0A509B__INCLUDED_)
