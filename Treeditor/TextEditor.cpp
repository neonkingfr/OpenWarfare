// TextEditor.cpp : implementation file
//

#include "stdafx.h"
#include <El/elementpch.hpp>
#include "RV/Tree/mytree.h"
#include "treeditor.h"
#include "TextEditor.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTextEditor dialog


CTextEditor::CTextEditor(LPCTSTR text, CWnd* pParent /*=NULL*/)
	: CDialog(CTextEditor::IDD, pParent), _ctrlDown (false), _text (text)
{
	//{{AFX_DATA_INIT(CTextEditor)
	//}}AFX_DATA_INIT
}


void CTextEditor::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTextEditor)
	DDX_Control(pDX, IDC_TEXT, _textCtrl);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTextEditor, CDialog)
	//{{AFX_MSG_MAP(CTextEditor)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTextEditor message handlers

BOOL CTextEditor::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	if (pMsg->hwnd == _textCtrl.m_hWnd)
	{
		if (pMsg->message == WM_KEYDOWN)
		{
			switch (pMsg->wParam)
			{
			case VK_TAB:
				_textCtrl.ReplaceSel(_T ("\t"), TRUE);
				return TRUE;
				
			case VK_RETURN:
				if (_ctrlDown)
				{
					SendMessage (WM_COMMAND, IDOK, NULL);
					_ctrlDown = false;
					return TRUE;
				}
				break;

			case VK_CONTROL:
				_ctrlDown = true;
				break;
			}
		}
		else if (pMsg->message == WM_KEYUP)
		{
			switch (pMsg->wParam)
			{
			case VK_CONTROL:
				_ctrlDown = false;
				break;
			}
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CTextEditor::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	
	if (_font.CreatePointFont (100, _T ("Courier")))
	{
		_textCtrl.SetFont (&_font);
	}

	_textCtrl.SetWindowText(_text);

	RECT r;
	GetWindowRect (&r);
	r.left = AfxGetApp ()->GetProfileInt (_T ("editor"), _T ("textEditorLeft"), r.left);
	r.top = AfxGetApp ()->GetProfileInt (_T ("editor"), _T ("textEditorTop"), r.top);
	r.right = AfxGetApp ()->GetProfileInt (_T ("editor"), _T ("textEditorRight"), r.right);
	r.bottom = AfxGetApp ()->GetProfileInt (_T ("editor"), _T ("textEditorBottom"), r.bottom);
	MoveWindow (&r, TRUE);
	GetClientRect(&r);
	OnSize (0, r.right, r.bottom);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CTextEditor::OnOK() 
{
	// TODO: Add extra validation here

	_textCtrl.GetWindowText (_text);

	RECT r;
	GetWindowRect (&r);
	AfxGetApp ()->WriteProfileInt (_T ("editor"), _T ("textEditorLeft"), r.left);
	AfxGetApp ()->WriteProfileInt (_T ("editor"), _T ("textEditorTop"), r.top);
	AfxGetApp ()->WriteProfileInt (_T ("editor"), _T ("textEditorRight"), r.right);
	AfxGetApp ()->WriteProfileInt (_T ("editor"), _T ("textEditorBottom"), r.bottom);

	CDialog::OnOK();
}

void CTextEditor::OnSize(UINT nType, int cx, int cy) 
{
	// CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here

#define CALC_CTRL_RECT(id, ctrl, r) CWnd *ctrl = GetDlgItem (id); if (ctrl == NULL) { return; } CRect r; ctrl->GetWindowRect(&r); ScreenToClient (&r)

	CALC_CTRL_RECT(IDC_TEXT, text, textR);
	CALC_CTRL_RECT(IDOK, ok, okR);
	CALC_CTRL_RECT(IDCANCEL, cancel, cancelR);

#undef CALC_CTRL_RECT

		
#define SIZE1 4
#define SIZE2 7

	textR.SetRect (SIZE2, SIZE2, cx - SIZE2 - (okR.right - okR.left) - SIZE2, cy - SIZE2);
	okR.SetRect (textR.right + SIZE2, SIZE2, cx - SIZE2, SIZE2 + okR.bottom - okR.top);
	cancelR.SetRect (okR.left, okR.bottom + SIZE2, okR.right, okR.bottom + SIZE2 + cancelR.bottom - cancelR.top);

#undef SIZE1
#undef SIZE2

	text->MoveWindow (&textR, TRUE);
	ok->MoveWindow (&okR, TRUE);
	cancel->MoveWindow (&cancelR, TRUE);
}

void CTextEditor::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	RECT r;
	GetWindowRect (&r);
	AfxGetApp ()->WriteProfileInt (_T ("editor"), _T ("textEditorLeft"), r.left);
	AfxGetApp ()->WriteProfileInt (_T ("editor"), _T ("textEditorTop"), r.top);
	AfxGetApp ()->WriteProfileInt (_T ("editor"), _T ("textEditorRight"), r.right);
	AfxGetApp ()->WriteProfileInt (_T ("editor"), _T ("textEditorBottom"), r.bottom);

	CDialog::OnCancel();
}
