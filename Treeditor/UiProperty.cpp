// UiProperty.cpp: implementation of the CUiProperty class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <El/elementpch.hpp>
#include "RV/Tree/mytree.h"
#include "Treeditor.h"
#include "UiProperty.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CUiProperty::CUiProperty(LPCTSTR name, COLORREF color, const RECT &rect, CWnd *parent,
						 const CChannelProtocol &channel, long data, long use, int bidx, bool selected, 
						 LPCTSTR value, bool enabled, va_list params) : _parent (parent), _channel (channel.Copy ()), 
						 _data (data), _label (color), _textBg (color), _valueStr (value), _uses (use), 
						 _baseIdx (UI_PROPERTY_BASE_ID + bidx * UI_PROPERTY_LAST_ID)
{
	_font.CreatePointFont (90, "Tahoma", NULL);
	DWORD enabledFlag = enabled ? 0 : WS_DISABLED;

	_textBg.Create (NULL, WS_CHILD | WS_GROUP | WS_VISIBLE, rect, parent, IDC_STATIC);
	_textBg._font = &_font;

	RECT r = rect;
	r.left += UI_PROPERTY_SPACE;
	r.right = r.left + UI_PROPERTY_CHECK_WIDTH;
	r.top = (r.top + r.bottom - UI_PROPERTY_CHECK_HEIGHT) >> 1;
	r.bottom = r.top + UI_PROPERTY_CHECK_HEIGHT;
	_selCheck.Create (NULL, WS_CHILD | WS_GROUP | WS_TABSTOP | BS_CHECKBOX | WS_VISIBLE | enabledFlag, r, parent, _baseIdx + UI_PROPERTY_SELECT_CHECK);
	_selCheck.SetCheck (selected ? 1 : 0);
	r.left = r.right + UI_PROPERTY_SPACE;
	r.top = rect.top;
	r.bottom = rect.bottom;

	r.right = r.left + UI_PROPERTY_LABEL_WIDTH;
	_label.Create (name, WS_CHILD | WS_GROUP | WS_VISIBLE | SS_RIGHT, r, parent, IDC_STATIC);
	_label._font = &_font;
	r.left = r.right + UI_PROPERTY_SPACE;

	if ((use & USE_PROPERTY_EDIT_BUTTON) != 0)
	{
		r.right = r.left + UI_PROPERTY_BUTTON_WIDTH;
		r.top = (r.top + r.bottom - UI_PROPERTY_BUTTON_HEIGHT) >> 1;
		r.bottom = r.top + UI_PROPERTY_BUTTON_HEIGHT;
		_edit.Create (_T ("Edit..."), WS_CHILD | WS_VISIBLE | WS_TABSTOP | enabledFlag, r, parent, _baseIdx + UI_PROPERTY_EDIT_BUTTON);
		_edit.SetFont (&_font, TRUE);
		r.left = r.right + UI_PROPERTY_SPACE;
		r.top = rect.top;
		r.bottom = rect.bottom;
	}

	if ((use & USE_PROPERTY_SELECT_BUTTON) != 0)
	{
		r.right = r.left + UI_PROPERTY_BUTTON_WIDTH;
		r.top = (r.top + r.bottom - UI_PROPERTY_BUTTON_HEIGHT) >> 1;
		r.bottom = r.top + UI_PROPERTY_BUTTON_HEIGHT;
		_select.Create (_T ("Select..."), WS_CHILD | WS_VISIBLE | WS_TABSTOP | enabledFlag, r, parent, _baseIdx + UI_PROPERTY_SELECT_BUTTON);
		_select.SetFont (&_font, TRUE);
		r.left = r.right + UI_PROPERTY_SPACE;
		r.top = rect.top;
		r.bottom = rect.bottom;
	}
	
	if ((use & USE_PROPERTY_BOOL_CHECK) != 0)
	{
		r.right = r.left + UI_PROPERTY_CHECK_WIDTH;
		r.top = (r.top + r.bottom - UI_PROPERTY_CHECK_HEIGHT) >> 1;
		r.bottom = r.top + UI_PROPERTY_CHECK_HEIGHT;
		_bool.Create (NULL, WS_CHILD | WS_GROUP | WS_VISIBLE | WS_TABSTOP | BS_CHECKBOX | enabledFlag, r, parent, _baseIdx + UI_PROPERTY_BOOL_CHECK);
		_bool.SetCheck (_valueStr == _T ("true") ? 1 : 0);
		r.left = r.right + UI_PROPERTY_SPACE;
		r.top = rect.top;
		r.bottom = rect.bottom;
	}
	else if ((use & USE_PROPERTY_ENUM_COMBO) != 0)
	{
		r.right = r.left + UI_PROPERTY_COMBO_WIDTH;
		r.top = (r.top + r.bottom - UI_PROPERTY_COMBO_HEIGHT) >> 1;
		r.bottom = r.top + UI_PROPERTY_COMBO_HEIGHT_EX;
		_enums.Create (WS_CHILD | WS_VISIBLE | WS_TABSTOP | CBS_DROPDOWNLIST | enabledFlag, r, parent, _baseIdx + UI_PROPERTY_ENUM_COMBO);
		_enums.SetFont (&_font, TRUE);
		r.left = r.right + UI_PROPERTY_SPACE;
		r.top = rect.top;
		r.bottom = rect.bottom;
		const CStringArray *strs = va_arg (params, const CStringArray*);
		int n = strs->GetSize ();
		for (int i = 0; i < n; ++i)
		{
			_enums.AddString (strs->GetAt (i));
		}
		int comboSel = _enums.FindStringExact (-1, _valueStr);
		_enums.SetCurSel (comboSel != CB_ERR ? comboSel : -1);
	}
	else if ((use & USE_PROPERTY_CHILD_COMBO) != 0)
	{
		r.right = r.left + UI_PROPERTY_COMBO_WIDTH;
		r.top = (r.top + r.bottom - UI_PROPERTY_COMBO_HEIGHT) >> 1;
		r.bottom = r.top + UI_PROPERTY_COMBO_HEIGHT_EX;
		_children.Create (WS_CHILD | WS_VISIBLE | WS_TABSTOP | CBS_DROPDOWNLIST | enabledFlag, r, parent, _baseIdx + UI_PROPERTY_CHILD_COMBO);
		_children.SetFont (&_font, TRUE);
		r.left = r.right + UI_PROPERTY_SPACE;
		r.top = rect.top;
		r.bottom = rect.bottom;
		const CStringArray *strs = va_arg (params, const CStringArray*);
		int n = strs->GetSize ();
		for (int i = 0; i < n; ++i)
		{
			_children.AddString (strs->GetAt (i));
		}
		int comboSel = _children.FindStringExact (-1, _valueStr);
		_children.SetCurSel (comboSel != CB_ERR ? comboSel : -1);
	}
	else if ((use & USE_PROPERTY_VALUE_EDIT) != 0)
	{
		r.right = r.left + UI_PROPERTY_EDIT_WIDTH;
		r.top = (r.top + r.bottom - UI_PROPERTY_EDIT_HEIGHT) >> 1;
		r.bottom = r.top + UI_PROPERTY_EDIT_HEIGHT;
		_value.CreateEx (WS_EX_CLIENTEDGE, "EDIT", _valueStr, WS_CHILD | WS_VISIBLE | WS_TABSTOP | WS_BORDER | ES_AUTOHSCROLL | enabledFlag, r, parent, _baseIdx + UI_PROPERTY_VALUE_EDIT);
		_value.SetFont (&_font, TRUE);
		r.left = r.right + UI_PROPERTY_SPACE;
		r.top = rect.top;
		r.bottom = rect.bottom;
	}

	_optimalBounds.left = rect.left;
	_optimalBounds.top = r.top;
	_optimalBounds.right = r.left;
	_optimalBounds.bottom = r.bottom;
	_textBg.MoveWindow (&_optimalBounds, TRUE);
}

CUiProperty::~CUiProperty()
{
	delete _channel;
}

void CUiProperty::ScrollBy (int dx, int dy)
{
	CRect r;

#define SCROLL_ONE(one)		\
	if (::IsWindow (one.m_hWnd))\
	{\
		one.GetWindowRect (&r);\
		_parent->ScreenToClient (&r);\
		r.OffsetRect (dx, dy);\
		one.MoveWindow (&r, TRUE);\
	}

	SCROLL_ONE (_label);
	SCROLL_ONE (_textBg);
	SCROLL_ONE (_edit);
	SCROLL_ONE (_select);
	SCROLL_ONE (_selCheck);
	SCROLL_ONE (_bool);
	SCROLL_ONE (_enums);
	SCROLL_ONE (_children);
	SCROLL_ONE (_value);

#undef SCROLL_ONE
}

void CUiProperty::SetPropValue (LPCTSTR val, bool updateUi)
{
	_valueStr = val;
	
	if ((_uses & USE_PROPERTY_BOOL_CHECK) != 0)
	{
		_bool.SetCheck (_valueStr == _T ("true") ? 1 : 0);
	}
	else if ((_uses & USE_PROPERTY_ENUM_COMBO) != 0)
	{
		int comboSel = _enums.FindStringExact (-1, val);
		_enums.SetCurSel (comboSel != CB_ERR ? comboSel : -1);
	}
	else if ((_uses & USE_PROPERTY_CHILD_COMBO) != 0)
	{
		int comboSel = _children.FindStringExact (-1, val);
		_children.SetCurSel (comboSel != CB_ERR ? comboSel : -1);
	}
	else if ((_uses & USE_PROPERTY_VALUE_EDIT) != 0)
	{
		if (updateUi)
		{
			_value.SetWindowText (val);
		}
	}
}

CSize CUiProperty::GetLabelExtent ()
{
	CString name;
	_label.GetWindowText (name);

	CDC *dc = _label.GetDC ();
	CFont *oldfont = dc->SelectObject (&_font);
	CSize r = dc->GetTextExtent (name);
	dc->SelectObject (oldfont);

	return r;
}

int CUiProperty::SetLabelWidth (int w)
{
	CRect r;

	_label.GetWindowRect (&r);
	_parent->ScreenToClient (&r);
	int dw = w - (r.right - r.left);
	r.right = r.left + w;
	_label.MoveWindow (&r, TRUE);

	_optimalBounds.right += dw;
	int res = _optimalBounds.right - _optimalBounds.left;

	_textBg.GetWindowRect (&r);
	_parent->ScreenToClient (&r);
	r.right += dw;
	_textBg.MoveWindow (&r, TRUE);

#define MOVE_ONE_CTRL(ctrl)		\
	ctrl.GetWindowRect (&r);\
	_parent->ScreenToClient (&r);\
	r.OffsetRect (dw, 0);\
	ctrl.MoveWindow (&r, TRUE)

	if ((_uses & USE_PROPERTY_EDIT_BUTTON) != 0)
	{
		MOVE_ONE_CTRL (_edit);
	}

	if ((_uses & USE_PROPERTY_SELECT_BUTTON) != 0)
	{
		MOVE_ONE_CTRL (_select);
	}
	
	if ((_uses & USE_PROPERTY_BOOL_CHECK) != 0)
	{
		MOVE_ONE_CTRL (_bool);
	}
	else if ((_uses & USE_PROPERTY_ENUM_COMBO) != 0)
	{
		MOVE_ONE_CTRL (_enums);
	}
	else if ((_uses & USE_PROPERTY_CHILD_COMBO) != 0)
	{
		MOVE_ONE_CTRL (_children);
	}
	else if ((_uses & USE_PROPERTY_VALUE_EDIT) != 0)
	{
		MOVE_ONE_CTRL (_value);
	}

#undef MOVE_ONE_CTRL

	return res;
}

void CUiProperty::SetEnabled (bool e)
{
	_selCheck.EnableWindow (e ? TRUE : FALSE);
	
	if ((_uses & USE_PROPERTY_EDIT_BUTTON) != 0)
	{
		_edit.EnableWindow (e ? TRUE : FALSE);
	}
	
	if ((_uses & USE_PROPERTY_SELECT_BUTTON) != 0)
	{
		_select.EnableWindow (e ? TRUE : FALSE);
	}
	
	if ((_uses & USE_PROPERTY_BOOL_CHECK) != 0)
	{
		_bool.EnableWindow (e ? TRUE : FALSE);
	}
	else if ((_uses & USE_PROPERTY_ENUM_COMBO) != 0)
	{
		_enums.EnableWindow (e ? TRUE : FALSE);
	}
	else if ((_uses & USE_PROPERTY_CHILD_COMBO) != 0)
	{
		_children.EnableWindow (e ? TRUE : FALSE);
	}
	else if ((_uses & USE_PROPERTY_VALUE_EDIT) != 0)
	{
		_value.EnableWindow (e ? TRUE : FALSE);
	}
}

int CUiProperty::GetOptimalWidth () const
{
	return _optimalBounds.right - _optimalBounds.left;
}

void CUiProperty::GetOptimalSize (int &w, int &h) const
{
	w = _optimalBounds.right - _optimalBounds.left;
	h = _optimalBounds.bottom - _optimalBounds.top;
}

void CUiProperty::SetWidth (int w)
{
	CRect r;
	_textBg.GetWindowRect (&r);
	_parent->ScreenToClient (&r);
	r.right = r.left + w;
	_textBg.MoveWindow (&r, TRUE);
}
