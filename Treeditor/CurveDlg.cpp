// CurveDlg.cpp : implementation file
//

#include "stdafx.h"
#include <El/elementpch.hpp>
#include "RV/Tree/mytree.h"
#include "Treeditor.h"
#include "TreeditorLex.h"
#include "CurveDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCurveDlg dialog


LPCTSTR CCurveDlg::_curvePrefix[] = 
{
	_T ("linearCurve"),
	_T ("quadraticCurve"),
	_T ("exQuadraticCurve"),
	_T ("linearCurveWithX"),
	_T ("quadraticCurveWithX"),
	_T ("exQuadraticCurveWithX"),
};


CCurveDlg::CCurveDlg(LPCTSTR title,
					 const ECurveType curveType, 
					 const bool varPointNumber,
					 const float step,
					 const CArray<int, int> &keyTypes, 
					 const CArray<float, float> &values, 
					 CStringArray &names, 
					 const float min,
					 const float max,
					 const bool horizontalX,
					 LPCTSTR xDescr,
					 LPCTSTR yDescr,
					 CWnd* pParent /*=NULL*/)
	: CDialog(CCurveDlg::IDD, pParent), _curveType (curveType), _min (min), _max (max), _originalMin (min), _originalMax (max),
	_varPointNumber (varPointNumber), _step (step), _title (title),
	_hookProc (NULL), _preview (true), _names (names),
	_horizontalX (horizontalX), _xDescr (xDescr), _yDescr (yDescr), _curveEntry (_curvePrefix[_curveType]), 
	_modified (false), _updateTime (false)
{
	//{{AFX_DATA_INIT(CCurveDlg)
	//}}AFX_DATA_INIT

	switch (curveType)
	{
	case EX_QUADRATIC_CURVE:
	case EX_QUADRATIC_CURVE_WITH_X:
		_maxKeyType = 3;
		break;

	default:
		_maxKeyType = 2;
		break;
	}

	_keyTypes.Copy (keyTypes);
	_values.Copy (values);

	int n = GetMarkSize ();
	int i = _keyTypes.GetSize();
	if (i < n)
	{
		do {
			_keyTypes.Add (_maxKeyType - 1);
			++i;
		} while (i < n);
	}
	else
	{
		_keyTypes.SetSize (n);
	}

	i = _names.GetSize();
	if (i < n)
	{
		do {
			_names.Add (_T (""));
			++i;
		} while (i < n);
	}
	else
	{
		_names.SetSize (n);
	}

	_curve._dlg = this;
}


void CCurveDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCurveDlg)
	DDX_Control(pDX, IDC_STRAIGHT, _straightCtrl);
	DDX_Control(pDX, IDC_CURVE_X_LABEL, _curveXLabel);
	DDX_Control(pDX, IDC_CURVE_X, _curveXCtrl);
	DDX_Control(pDX, IDC_CURVE_SCALE_MUL, _scaleMulCtrl);
	DDX_Control(pDX, IDC_CURVE_SCALE_LABEL, _scaleLabel);
	DDX_Control(pDX, IDC_CURVE_SCALE_DIV, _scaleDivCtrl);
	DDX_Control(pDX, IDC_CURVE_SCALE, _scaleCtrl);
	DDX_Control(pDX, IDC_CURVE_DELETE, _curveDeleteCtrl);
	DDX_Control(pDX, IDC_MIRROR_X, _mirrorXCtrl);
	DDX_Control(pDX, IDC_CURVE_LOAD, _curveLoadCtrl);
	DDX_Control(pDX, IDC_CURVE_SAVE, _curveSaveCtrl);
	DDX_Control(pDX, IDC_CURVE_NAME, _curveNameCtrl);
	DDX_Control(pDX, IDC_MIRROR, _mirrorCtrl);
	DDX_Control(pDX, IDC_HORIZONTAL_X, _horizontalXCtrl);
	DDX_Control(pDX, IDC_PREVIEW, _previewCtrl);
	DDX_Control(pDX, IDC_CURVE_MIN_LABEL, _minLabel);
	DDX_Control(pDX, IDC_CURVE_MIN, _minCtrl);
	DDX_Control(pDX, IDC_CURVE_MAX_LABEL, _maxLabel);
	DDX_Control(pDX, IDC_CURVE_MAX, _maxCtrl);
	DDX_Control(pDX, IDC_CURVE_STEP_LABEL, _curveStepLabel);
	DDX_Control(pDX, IDC_CURVE_VALUE_LABEL, _curveValueLabel);
	DDX_Control(pDX, IDOK, _ok);
	DDX_Control(pDX, IDCANCEL, _cancel);
	DDX_Control(pDX, IDC_CURVE_VALUE, _curveValueCtrl);
	DDX_Control(pDX, IDC_CURVE_STRAFE, _strafeCtrl);
	DDX_Control(pDX, IDC_CURVE_STEP, _curveStepCtrl);
	DDX_Control(pDX, IDC_CURVE_DIRECTION_SPIN, _directionSpin);
	DDX_Control(pDX, IDC_CURVE_DIRECTION, _directionCtrl);
	DDX_Control(pDX, IDC_CURVE_DIRECTION_LABEL, _directionLabel);
	DDX_Control(pDX, IDC_CURVE, _curve);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCurveDlg, CDialog)
	//{{AFX_MSG_MAP(CCurveDlg)
	ON_EN_CHANGE(IDC_CURVE_DIRECTION, OnChangeCurveDirection)
	ON_EN_CHANGE(IDC_CURVE_STEP, OnChangeCurveStep)
	ON_NOTIFY(UDN_DELTAPOS, IDC_CURVE_STRAFE, OnDeltaPosCurveStrafe)
	ON_EN_CHANGE(IDC_CURVE_VALUE, OnChangeCurveValue)
	ON_WM_SIZE()
	ON_NOTIFY(UDN_DELTAPOS, IDC_CURVE_DIRECTION_SPIN, OnDeltaPosCurveDirectionSpin)
	ON_EN_CHANGE(IDC_CURVE_MIN, OnChangeCurveMin)
	ON_EN_CHANGE(IDC_CURVE_MAX, OnChangeCurveMax)
	ON_EN_KILLFOCUS(IDC_CURVE_VALUE, OnKillFocusCurveValue)
	ON_EN_KILLFOCUS(IDC_CURVE_MIN, OnKillFocusCurveMin)
	ON_EN_KILLFOCUS(IDC_CURVE_MAX, OnKillFocusCurveMax)
	ON_BN_CLICKED(IDC_PREVIEW, OnPreview)
	ON_BN_CLICKED(IDC_HORIZONTAL_X, OnHorizontalX)
	ON_BN_CLICKED(IDC_MIRROR, OnMirror)
	ON_BN_CLICKED(IDC_CURVE_SAVE, OnCurveSave)
	ON_BN_CLICKED(IDC_CURVE_LOAD, OnCurveLoad)
	ON_BN_CLICKED(IDC_MIRROR_X, OnMirrorX)
	ON_BN_CLICKED(IDC_CURVE_DELETE, OnCurveDelete)
	ON_BN_CLICKED(IDC_CURVE_SCALE_MUL, OnCurveScaleMul)
	ON_BN_CLICKED(IDC_CURVE_SCALE_DIV, OnCurveScaleDiv)
	ON_EN_CHANGE(IDC_CURVE_X, OnChangeCurveX)
	ON_EN_KILLFOCUS(IDC_CURVE_X, OnKillFocusCurveX)
	ON_BN_CLICKED(IDC_STRAIGHT, OnStraight)
	ON_EN_KILLFOCUS(IDC_CURVE_DIRECTION, OnKillFocusCurveDirection)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCurveDlg message handlers

BOOL CCurveDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	
	_updateTime = true;
	CString str;
	str.Format (_T ("%g"), _step);
	_curveStepCtrl.SetWindowText (str);
	str.Format (_T ("%g"), _min);
	_minCtrl.SetWindowText (str);
	str.Format (_T ("%g"), _max);
	_maxCtrl.SetWindowText (str);

	_strafeCtrl.SetRange(-100, 100);

	_previewCtrl.SetCheck (_preview ? 1 : 0);
	_horizontalXCtrl.SetCheck (_horizontalX ? 1 : 0);

	switch (_curveType)
	{
	case LINEAR_CURVE:
		break;

	case QUADRATIC_CURVE:
		_directionCtrl.ShowWindow(SW_SHOW);
		_directionLabel.ShowWindow(SW_SHOW);
		_directionSpin.ShowWindow(SW_SHOW);
		_directionSpin.SetRange(-90, 90);
		_straightCtrl.ShowWindow(SW_SHOW);
		break;
		
	case EX_QUADRATIC_CURVE:
		_directionCtrl.ShowWindow(SW_SHOW);
		_directionLabel.ShowWindow(SW_SHOW);
		_directionSpin.ShowWindow(SW_SHOW);
		_directionSpin.SetRange(-90, 90);
		_straightCtrl.ShowWindow(SW_SHOW);
		break;
		
	case LINEAR_CURVE_WITH_X:
		_curveXCtrl.ShowWindow(SW_SHOW);
		_curveXLabel.ShowWindow(SW_SHOW);
		break;

	case QUADRATIC_CURVE_WITH_X:
		_directionCtrl.ShowWindow(SW_SHOW);
		_directionLabel.ShowWindow(SW_SHOW);
		_directionSpin.ShowWindow(SW_SHOW);
		_directionSpin.SetRange(-90, 90);
		_straightCtrl.ShowWindow(SW_SHOW);

		_curveXCtrl.ShowWindow(SW_SHOW);
		_curveXLabel.ShowWindow(SW_SHOW);
		break;

	case EX_QUADRATIC_CURVE_WITH_X:
		_directionCtrl.ShowWindow(SW_SHOW);
		_directionLabel.ShowWindow(SW_SHOW);
		_directionSpin.ShowWindow(SW_SHOW);
		_directionSpin.SetRange(-90, 90);
		_straightCtrl.ShowWindow(SW_SHOW);

		_curveXCtrl.ShowWindow(SW_SHOW);
		_curveXLabel.ShowWindow(SW_SHOW);
		break;
	}

	_previewCtrl.ShowWindow (_hookProc != NULL ? SW_SHOW : SW_HIDE);
	
	RECT r;
	GetClientRect(&r);
	OnSize (0, r.right - r.left, r.bottom - r.top);
	SetWindowText(_title);

	str = AfxGetApp ()->GetProfileString (_T ("curves"), _curveEntry + _T (":List"), _T (""));
	int s = 0;
	int e = str.Find(_T (':'));
	while (e != -1)
	{
		CString name = str.Mid(s, e - s);
		_curveNameCtrl.AddString(name);
		s = e + 1;
		e = str.Find(_T (':'), s);
	}

	_scaleCtrl.SetWindowText (_T ("1"));

	_updateTime = false;
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCurveDlg::OnChangeCurveDirection() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	
	if (::IsWindow (_directionCtrl.m_hWnd) && _curve._draggedMark >= 0)
	{
		CString str;
		_directionCtrl.GetWindowText(str);
		LPCTSTR start = (LPCTSTR) str;
		LPTSTR end;
		float v = (float) _tcstod (start, &end);
		if (start != end)
		{
			SetMarkTangent(_curve._draggedMark, v);
			CurveChanged ();
		}
	}
}

void CCurveDlg::OnChangeCurveStep() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	
	if (::IsWindow (_curveStepCtrl.m_hWnd))
	{
		CString str;
		_curveStepCtrl.GetWindowText(str);
		_step = (float) _tcstod (str, NULL);
	}
}

void CCurveDlg::OnDeltaPosCurveStrafe(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	int n = GetMarkSize ();
	for (int i = 0; i < n; ++i)
	{
		float v = GetMarkValue (i) + _step * pNMUpDown->iDelta;
		SetMarkValue (i, v);
	}

	CurveChanged();

	*pResult = 0;
}

void CCurveDlg::OnChangeCurveValue() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	
	if (_curve._draggedMark != -1 && ::IsWindow (_curveValueCtrl.m_hWnd))
	{
		CString str;
		_curveValueCtrl.GetWindowText(str);
		LPCTSTR start = (LPCTSTR) str;
		TCHAR *end;
		float f = _tcstod (start, &end);
		if (end != start)
		{
			if (f < _originalMin)
			{
				f = _originalMin;
			}
			else if (f > _originalMax)
			{
				f = _originalMax;
			}
			SetMarkValue(_curve._draggedMark, f);
			CurveChanged();
		}
	}
}

void CCurveDlg::UpdateCurveX()
{
	if (_curve._draggedMark != -1)
	{
		CString str;
		str.Format(_T("%g"), GetMarkX(_curve._draggedMark));
		_curveXCtrl.SetWindowText(str);
	}
}

void CCurveDlg::UpdateCurveValue()
{
	if (_curve._draggedMark != -1)
	{
		CString str;
		str.Format(_T("%g"), GetMarkValue(_curve._draggedMark));
		_curveValueCtrl.SetWindowText(str);
	}
}

void CCurveDlg::UpdateCurveTangent()
{
	if (_curve._draggedMark != -1)
	{
		CString str;
		str.Format(_T("%g"), GetMarkTangent(_curve._draggedMark));
		_directionCtrl.SetWindowText(str);
	}
}

void CCurveDlg::OnSize(UINT nType, int cx, int cy) 
{
	// CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here

#define CALC_CTRL_RECT(ctrl, r) if (!::IsWindow (ctrl.m_hWnd)) { return; } CRect r; ctrl.GetWindowRect(&r);ScreenToClient (&r)

	CALC_CTRL_RECT (_curve, curveR);
	CALC_CTRL_RECT (_ok, okR);
	CALC_CTRL_RECT (_cancel, cancelR);
	CALC_CTRL_RECT (_curveStepLabel, stepLabelR);
	CALC_CTRL_RECT (_curveStepCtrl, stepCtrlR);
	CALC_CTRL_RECT (_strafeCtrl, strafeR);
	CALC_CTRL_RECT (_curveValueLabel, valueLabelR);
	CALC_CTRL_RECT (_curveValueCtrl, valueCtrlR);
	CALC_CTRL_RECT (_curveXLabel, xLabelR);
	CALC_CTRL_RECT (_curveXCtrl, xCtrlR);
	CALC_CTRL_RECT (_minLabel, minLabelR);
	CALC_CTRL_RECT (_minCtrl, minCtrlR);
	CALC_CTRL_RECT (_maxLabel, maxLabelR);
	CALC_CTRL_RECT (_maxCtrl, maxCtrlR);
	CALC_CTRL_RECT (_directionLabel, directionLabelR);
	CALC_CTRL_RECT (_directionCtrl, directionCtrlR);
	CALC_CTRL_RECT (_directionSpin, directionSpinR);
	CALC_CTRL_RECT (_straightCtrl, straightCtrlR);
	CALC_CTRL_RECT (_previewCtrl, previewCtrlR);
	CALC_CTRL_RECT (_horizontalXCtrl, horizontalXCtrlR);
	CALC_CTRL_RECT (_mirrorXCtrl, mirrorXCtrlR);
	CALC_CTRL_RECT (_mirrorCtrl, mirrorCtrlR);
	CALC_CTRL_RECT (_curveNameCtrl, curveNameCtrlR);
	CALC_CTRL_RECT (_curveLoadCtrl, curveLoadCtrlR);
	CALC_CTRL_RECT (_curveSaveCtrl, curveSaveCtrlR);
	CALC_CTRL_RECT (_curveDeleteCtrl, curveDeleteCtrlR);
	CALC_CTRL_RECT (_scaleLabel, scaleLabelR);
	CALC_CTRL_RECT (_scaleCtrl, scaleCtrlR);
	CALC_CTRL_RECT (_scaleMulCtrl, scaleMulCtrlR);
	CALC_CTRL_RECT (_scaleDivCtrl, scaleDivCtrlR);

#undef CALC_CTRL_RECT

#define SPACE1 4
#define SPACE2 7

	curveR.SetRect (SPACE2, SPACE2, cx - 2 * SPACE2 - (okR.right - okR.left), cy - SPACE2);
	okR.SetRect (cx - SPACE2 - (okR.right - okR.left), SPACE2, cx - SPACE2, SPACE2 + (okR.bottom - okR.top));
	cancelR.SetRect (okR.left, okR.bottom + SPACE2, okR.right, okR.bottom + SPACE2 + (cancelR.bottom - cancelR.top));
	stepLabelR.SetRect (okR.left, cancelR.bottom + SPACE2, okR.right, cancelR.bottom + SPACE2 + (stepLabelR.bottom - stepLabelR.top));
	stepCtrlR.SetRect (okR.left, stepLabelR.bottom + SPACE1, okR.right, stepLabelR.bottom + SPACE1 + (stepCtrlR.bottom - stepCtrlR.top));
	strafeR.SetRect (okR.left, stepCtrlR.bottom + SPACE1, okR.right, stepCtrlR.bottom + SPACE1 + (strafeR.bottom - strafeR.top));
	valueLabelR.SetRect (okR.left, strafeR.bottom + SPACE2, okR.right, strafeR.bottom + SPACE2 + (valueLabelR.bottom - valueLabelR.top));
	valueCtrlR.SetRect (okR.left, valueLabelR.bottom + SPACE1, okR.right, valueLabelR.bottom + SPACE1 + (valueCtrlR.bottom - valueCtrlR.top));
	xLabelR.SetRect (okR.left, valueCtrlR.bottom + SPACE2, okR.right, valueCtrlR.bottom + SPACE2 + (xLabelR.bottom - xLabelR.top));
	xCtrlR.SetRect (okR.left, xLabelR.bottom + SPACE1, okR.right, xLabelR.bottom + SPACE1 + (xCtrlR.bottom - xCtrlR.top));
	minLabelR.SetRect(okR.left, xCtrlR.bottom + SPACE2, okR.right, xCtrlR.bottom + SPACE2 + (minLabelR.bottom - minLabelR.top));
	minCtrlR.SetRect(okR.left, minLabelR.bottom + SPACE1, okR.right, minLabelR.bottom + SPACE1 + (minCtrlR.bottom - minCtrlR.top));
	maxLabelR.SetRect(okR.left, minCtrlR.bottom + SPACE2, okR.right, minCtrlR.bottom + SPACE2 + (maxLabelR.bottom - maxLabelR.top));
	maxCtrlR.SetRect(okR.left, maxLabelR.bottom + SPACE1, okR.right, maxLabelR.bottom + SPACE1 + (maxCtrlR.bottom - maxCtrlR.top));
	directionLabelR.SetRect (okR.left, maxCtrlR.bottom + SPACE2, okR.right, maxCtrlR.bottom + SPACE2 + (directionLabelR.bottom - directionLabelR.top));
	directionCtrlR.SetRect (okR.left, directionLabelR.bottom + SPACE1, okR.right - (directionSpinR.right - directionSpinR.left), directionLabelR.bottom + SPACE1 + (directionCtrlR.bottom - directionCtrlR.top));
	directionSpinR.SetRect (directionCtrlR.right, directionCtrlR.top, okR.right, directionCtrlR.bottom);
	straightCtrlR.SetRect (okR.left, directionCtrlR.bottom + SPACE1, okR.left + straightCtrlR.right - straightCtrlR.left, directionCtrlR.bottom + SPACE1 + straightCtrlR.bottom - straightCtrlR.top);
	previewCtrlR.SetRect (okR.left, straightCtrlR.bottom + SPACE2, okR.left + previewCtrlR.right - previewCtrlR.left, straightCtrlR.bottom + SPACE2 + previewCtrlR.bottom - previewCtrlR.top);
	horizontalXCtrlR.SetRect (okR.left, previewCtrlR.bottom + SPACE2, okR.left + horizontalXCtrlR.right - horizontalXCtrlR.left, previewCtrlR.bottom + SPACE2 + horizontalXCtrlR.bottom - horizontalXCtrlR.top);
	mirrorXCtrlR.SetRect (okR.left, horizontalXCtrlR.bottom + SPACE2, okR.left + mirrorXCtrlR.right - mirrorXCtrlR.left, horizontalXCtrlR.bottom + SPACE2 + mirrorXCtrlR.bottom - mirrorXCtrlR.top);
	mirrorCtrlR.SetRect (okR.left, mirrorXCtrlR.bottom + SPACE1, okR.left + mirrorCtrlR.right - mirrorCtrlR.left, mirrorXCtrlR.bottom + SPACE1 + mirrorCtrlR.bottom - mirrorCtrlR.top);
	curveNameCtrlR.SetRect (okR.left, mirrorCtrlR.bottom + SPACE2, okR.left + curveNameCtrlR.right - curveNameCtrlR.left, mirrorCtrlR.bottom + SPACE2 + curveNameCtrlR.bottom - curveNameCtrlR.top);
	curveLoadCtrlR.SetRect (okR.left, curveNameCtrlR.bottom + SPACE1, okR.left + curveLoadCtrlR.right - curveLoadCtrlR.left, curveNameCtrlR.bottom + SPACE1 + curveLoadCtrlR.bottom - curveLoadCtrlR.top);
	curveSaveCtrlR.SetRect (okR.left, curveLoadCtrlR.bottom + SPACE1, okR.left + curveSaveCtrlR.right - curveSaveCtrlR.left, curveLoadCtrlR.bottom + SPACE1 + curveSaveCtrlR.bottom - curveSaveCtrlR.top);
	curveDeleteCtrlR.SetRect (okR.left, curveSaveCtrlR.bottom + SPACE1, okR.left + curveDeleteCtrlR.right - curveDeleteCtrlR.left, curveSaveCtrlR.bottom + SPACE1 + curveDeleteCtrlR.bottom - curveDeleteCtrlR.top);
	scaleLabelR.SetRect (okR.left, curveDeleteCtrlR.bottom + SPACE2, okR.left + scaleLabelR.right - scaleLabelR.left, curveDeleteCtrlR.bottom + SPACE2 + scaleLabelR.bottom - scaleLabelR.top);
	scaleCtrlR.SetRect (okR.left, scaleLabelR.bottom + SPACE1, okR.left + scaleCtrlR.right - scaleCtrlR.left, scaleLabelR.bottom + SPACE1 + scaleCtrlR.bottom - scaleCtrlR.top);
	scaleMulCtrlR.SetRect (okR.left, scaleCtrlR.bottom + SPACE1, okR.left + scaleMulCtrlR.right - scaleMulCtrlR.left, scaleCtrlR.bottom + SPACE1 + scaleMulCtrlR.bottom - scaleMulCtrlR.top);
	scaleDivCtrlR.SetRect (okR.left, scaleMulCtrlR.bottom + SPACE1, okR.left + scaleDivCtrlR.right - scaleDivCtrlR.left, scaleMulCtrlR.bottom + SPACE1 + scaleDivCtrlR.bottom - scaleDivCtrlR.top);

#undef SPACE1
#undef SPACE2

	_curve.MoveWindow(&curveR, TRUE);
	_ok.MoveWindow(&okR, TRUE);
	_cancel.MoveWindow(&cancelR, TRUE);
	_curveStepLabel.MoveWindow(&stepLabelR, TRUE);
	_curveStepCtrl.MoveWindow(&stepCtrlR, TRUE);
	_strafeCtrl.MoveWindow(strafeR, TRUE);
	_curveValueLabel.MoveWindow(valueLabelR, TRUE);
	_curveValueCtrl.MoveWindow(valueCtrlR, TRUE);
	_curveXLabel.MoveWindow(xLabelR, TRUE);
	_curveXCtrl.MoveWindow(xCtrlR, TRUE);
	_minLabel.MoveWindow (minLabelR, TRUE);
	_minCtrl.MoveWindow(minCtrlR, TRUE);
	_maxLabel.MoveWindow(maxLabelR, TRUE);
	_maxCtrl.MoveWindow(maxCtrlR, TRUE);
	_directionLabel.MoveWindow(directionLabelR, TRUE);
	_directionCtrl.MoveWindow(directionCtrlR, TRUE);
	_directionSpin.MoveWindow(directionSpinR, TRUE);
	_straightCtrl.MoveWindow(straightCtrlR, TRUE);
	_previewCtrl.MoveWindow(previewCtrlR, TRUE);
	_horizontalXCtrl.MoveWindow(horizontalXCtrlR, TRUE);
	_mirrorXCtrl.MoveWindow(mirrorXCtrlR, TRUE);
	_mirrorCtrl.MoveWindow(mirrorCtrlR, TRUE);
	_curveNameCtrl.MoveWindow(curveNameCtrlR, TRUE);
	_curveLoadCtrl.MoveWindow(curveLoadCtrlR, TRUE);
	_curveSaveCtrl.MoveWindow(curveSaveCtrlR, TRUE);
	_curveDeleteCtrl.MoveWindow(curveDeleteCtrlR, TRUE);
	_scaleLabel.MoveWindow(scaleLabelR, TRUE);
	_scaleCtrl.MoveWindow(scaleCtrlR, TRUE);
	_scaleMulCtrl.MoveWindow(scaleMulCtrlR, TRUE);
	_scaleDivCtrl.MoveWindow(scaleDivCtrlR, TRUE);
}

void CCurveDlg::OnDeltaPosCurveDirectionSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	if (_curve._draggedMark >= 0)
	{
		float v = GetMarkTangent (_curve._draggedMark);
		double angle = atan (v);
		if (angle > -D3DX_PI / 2.0 && angle < D3DX_PI / 2.0)
		{
			v = tan (angle + D3DX_PI * pNMUpDown->iDelta / 180);
			SetMarkTangent(_curve._draggedMark, v);
		
			CString str;
			str.Format(_T("%g"), v);
			_directionCtrl.SetWindowText(str);
			_directionCtrl.SetFocus ();
			_directionCtrl.SetSel (0, -1);
			CurveChanged ();
		}
	}
	*pResult = 0;
}

void CCurveDlg::OnChangeCurveMin() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	
	if (::IsWindow (_minCtrl.m_hWnd))
	{
		CString str;
		_minCtrl.GetWindowText(str);
		LPCTSTR start = (LPCTSTR) str;
		TCHAR *end;
		float f = (float) _tcstod (str, &end);
		if (start != end)
		{
			if (f < _originalMin)
			{
				f = _originalMin;
			}
			else if (f > _originalMax)
			{
				f = _originalMax;
			}
			_min = f;
		}
	
		CurveChanged();
	}
}

void CCurveDlg::OnChangeCurveMax() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	
	if (::IsWindow (_maxCtrl.m_hWnd))
	{
		CString str;
		_maxCtrl.GetWindowText(str);
		LPCTSTR start = (LPCTSTR) str;
		TCHAR *end;
		float f = (float) _tcstod (start, &end);
		if (start != end)
		{
			if (f < _originalMin)
			{
				f = _originalMin;
			}
			else if (f > _originalMax)
			{
				f = _originalMax;
			}
			_max = f;
		}
	
		CurveChanged();
	}
}

void CCurveDlg::SetHookProc (void (*hook) (const CCurveDlg*, long, long, long), long param1, long param2, long param3)
{
	_hookProc = hook;
	_hookParam1 = param1;
	_hookParam2 = param2;
	_hookParam3 = param3;

	if (::IsWindow (_previewCtrl.m_hWnd))
	{
		_previewCtrl.ShowWindow (_hookProc != NULL ? SW_SHOW : SW_HIDE);
	}
}

void CCurveDlg::CurveChanged()
{
	if (_updateTime)
	{
		return;
	}

	SetModified ();
	_curve.Invalidate(FALSE);
	_curve.UpdateWindow();

	if (_preview && _hookProc != NULL)
	{
		_hookProc(this, _hookParam1, _hookParam2, _hookParam3);
	}
}

void CCurveDlg::OnKillFocusCurveValue() 
{
	// TODO: Add your control notification handler code here
	
	if (_curve._draggedMark != -1 && ::IsWindow (_curveValueCtrl.m_hWnd))
	{
		CString str;
		str.Format(_T ("%g"), GetMarkValue(_curve._draggedMark));
		_curveValueCtrl.SetWindowText(str);
	}
}

void CCurveDlg::OnKillFocusCurveMin() 
{
	// TODO: Add your control notification handler code here
	
	if (::IsWindow (_minCtrl.m_hWnd))
	{
		CString str;
		str.Format(_T ("%g"), _min);
		_minCtrl.SetWindowText(str);
	}
}

void CCurveDlg::OnKillFocusCurveMax() 
{
	// TODO: Add your control notification handler code here
	
	if (::IsWindow (_maxCtrl.m_hWnd))
	{
		CString str;
		str.Format(_T ("%g"), _max);
		_maxCtrl.SetWindowText(str);
	}
}

void CCurveDlg::OnPreview() 
{
	// TODO: Add your control notification handler code here
	
	_preview = !_preview;
	_previewCtrl.SetCheck (_preview ? 1 : 0);
}

int CCurveDlg::GetMarkSize () const
{
	switch (_curveType)
	{
	case QUADRATIC_CURVE:
		return _values.GetSize () - 1;

	case QUADRATIC_CURVE_WITH_X:
		return (_values.GetSize () - 1) >> 1;

	case EX_QUADRATIC_CURVE:
		return _values.GetSize () >> 1;

	case EX_QUADRATIC_CURVE_WITH_X:
		return _values.GetSize () / 3;

	case LINEAR_CURVE_WITH_X:
		return _values.GetSize () >> 1;

	// case LINEAR_CURVE:
	default:
		return _values.GetSize ();
	}
}

float CCurveDlg::GetMarkValue (int i) const
{
	switch (_curveType)
	{
	case LINEAR_CURVE_WITH_X:
		return _values[(i << 1) + 1];

	case QUADRATIC_CURVE:
		return _values[i + 1];

	case QUADRATIC_CURVE_WITH_X:
		return _values[(i << 1) + 2];

	case EX_QUADRATIC_CURVE:
		return _values[i << 1];

	case EX_QUADRATIC_CURVE_WITH_X:
		return _values[i * 3 + 1];

	// case LINEAR_CURVE:
	default:
		return _values[i];
	}
}

float CCurveDlg::GetMarkX (int i) const
{
	switch (_curveType)
	{
	case LINEAR_CURVE_WITH_X:
		return _values[i << 1];

	case QUADRATIC_CURVE_WITH_X:
		return _values[(i << 1) + 1];

	case EX_QUADRATIC_CURVE_WITH_X:
		return _values[i * 3];

	// case QUADRATIC_CURVE:
	// case EX_QUADRATIC_CURVE:
	// case LINEAR_CURVE:
	default:
		return (float) i / (float) (GetMarkSize () - 1);
	}
}

float CCurveDlg::GetMarkTangent (int i) const
{
	switch (_curveType)
	{
	case QUADRATIC_CURVE_WITH_X:
	case QUADRATIC_CURVE:
		if (i == 0)
		{
			return _values[0];
		}
		return 0.0f;

	case EX_QUADRATIC_CURVE:
		return _values[(i << 1) + 1];

	case EX_QUADRATIC_CURVE_WITH_X:
		return _values[i * 3 + 2];

	// case LINEAR_CURVE:
	default:
		return 0.0f;
	}
}

void CCurveDlg::SetMarkValue(int i, float v)
{
	if (v < _min)
	{
		v = _min;
	}
	else if (v > _max)
	{
		v = _max;
	}

	switch (_curveType)
	{
	case LINEAR_CURVE_WITH_X:
		_values[(i << 1) + 1] = v;
		return;

	case QUADRATIC_CURVE:
		_values[i + 1] = v;
		return;

	case QUADRATIC_CURVE_WITH_X:
		_values[(i << 1) + 2] = v;
		return;

	case EX_QUADRATIC_CURVE:
		_values[i << 1] = v;
		return;

	case EX_QUADRATIC_CURVE_WITH_X:
		_values[i * 3 + 1] = v;
		return;

	// case LINEAR_CURVE:
	default:
		_values[i] = v;
		return;
	}
}

void CCurveDlg::SetMarkX (int i, float v)
{
	float min = (i == 0) ? 0.0f : GetMarkX (i - 1);
	float max = (i == GetMarkSize () - 1) ? 1.0f : GetMarkX (i + 1);
	if (v < min)
	{
		v = min;
	}
	else if (v > max)
	{
		v = max;
	}

	switch (_curveType)
	{
	case LINEAR_CURVE_WITH_X:
		_values[i << 1] = v;
		return;

	case QUADRATIC_CURVE_WITH_X:
		_values[(i << 1) + 1] = v;
		return;

	case EX_QUADRATIC_CURVE_WITH_X:
		_values[i * 3] = v;
		return;

	// case LINEAR_CURVE:
	// case QUADRATIC_CURVE:
	// case EX_QUADRATIC_CURVE:
	default:
		return;
	}
}

void CCurveDlg::SetMarkTangent(int i, float v)
{
	switch (_curveType)
	{
	case QUADRATIC_CURVE:
	case QUADRATIC_CURVE_WITH_X:
		if (i == 0)
		{
			_values[0] = v;
		}
		return;

	case EX_QUADRATIC_CURVE:
		_values[(i << 1) + 1] = v;
		return;

	case EX_QUADRATIC_CURVE_WITH_X:
		_values[i * 3 + 2] = v;
		return;

	// case LINEAR_CURVE:
	// case LINEAR_CURVE_WITH_X:
	default:
		return;
	}
}

int CCurveDlg::GetMarkType (int i) const
{
	return _keyTypes[i];
}

void CCurveDlg::SetMarkType (int i, int keyType)
{
	_keyTypes[i] = keyType;
}

int CCurveDlg::ChangeMarkType(int i)
{
	return _keyTypes[i] = (_keyTypes[i] + 1) % _maxKeyType;
}

void CCurveDlg::InsertMarkAt(int i, float x, float value, float tangent, int keyType, LPCTSTR name)
{
	_keyTypes.InsertAt (i, keyType);
	_names.InsertAt (i, name);

	switch (_curveType)
	{
	case LINEAR_CURVE_WITH_X:
		i <<= 1;
		_values.InsertAt(i, x, 2);
		_values[++i] = value;
		break;

	case QUADRATIC_CURVE:
		_values.InsertAt(++i, value);
		break;

	case QUADRATIC_CURVE_WITH_X:
		i <<= 1;
		_values.InsertAt(++i, x, 2);
		_values[++i] = value;
		break;

	case EX_QUADRATIC_CURVE:
		i <<= 1;
		_values.InsertAt(i, value, 2);
		_values[++i] = tangent;
		break;

	case EX_QUADRATIC_CURVE_WITH_X:
		i *= 3;
		_values.InsertAt(i, x, 3);
		_values[++i] = value;
		_values[++i] = tangent;
		break;

	// case LINEAR_CURVE:
	default:
		_values.InsertAt(i, value);
		break;
	}
}

void CCurveDlg::RemoveMarkAt(int i)
{
	_keyTypes.RemoveAt (i);
	_names.RemoveAt (i);

	switch (_curveType)
	{
	case LINEAR_CURVE_WITH_X:
		i <<= 1;
		_values.RemoveAt (i, 2);
		break;

	case QUADRATIC_CURVE:
		_values.RemoveAt(++i);
		break;

	case QUADRATIC_CURVE_WITH_X:
		i <<= 1;
		_values.RemoveAt (++i, 2);
		break;

	case EX_QUADRATIC_CURVE:
		i <<= 1;
		_values.RemoveAt(i, 2);
		break;

	case EX_QUADRATIC_CURVE_WITH_X:
		i *= 3;
		_values.RemoveAt (i, 3);
		break;

	// case LINEAR_CURVE:
	default:
		_values.RemoveAt (i);
		break;
	}
}

void CCurveDlg::SetMarkName(int i, LPCTSTR name)
{
	_names[i] = name;
}

LPCTSTR CCurveDlg::GetMarkName (int i) const
{
	return _names[i];
}

void CCurveDlg::OnHorizontalX() 
{
	// TODO: Add your control notification handler code here
	
	_horizontalX = !_horizontalX;
	_horizontalXCtrl.SetCheck (_horizontalX ? 1 : 0);
	_curve.Invalidate(FALSE);
	_curve.UpdateWindow ();
}

void CCurveDlg::OnMirror() 
{
	// TODO: Add your control notification handler code here
	
	int n = GetMarkSize();
	for (int i = 0; i < n; ++i)
	{
		float f = GetMarkValue(i);
		SetMarkValue(i, _max - (f - _min));

		f = GetMarkTangent(i);
		SetMarkTangent(i, -f);
	}

	CurveChanged();
}

void CCurveDlg::OnCurveSave() 
{
	// TODO: Add your control notification handler code here
	
	CString name;
	_curveNameCtrl.GetWindowText (name);

	if (name.IsEmpty())
	{
		return;
	}
	if (name.Find(_T (':')) != -1)
	{
		MessageBox (_T ("I am sorry, but curve name can not contain char ':'."));
		return;
	}

	CString curve;
	int n = GetMarkSize();
	for (int i = 0; i < n; ++i)
	{
		CString t;
		t.Format(_T ("%g,"), GetMarkX(i));
		curve += t;
	}
	curve += _T (':');
	for (int i = 0; i < n; ++i)
	{
		CString t;
		t.Format(_T ("%g,"), GetMarkValue(i));
		curve += t;
	}
	curve += _T (':');
	for (int i = 0; i < n; ++i)
	{
		CString t;
		t.Format(_T ("%g,"), GetMarkTangent(i));
		curve += t;
	}
	curve += _T (':');
	for (int i = 0; i < n; ++i)
	{
		CString t;
		t.Format(_T ("%d,"), GetMarkType(i));
		curve += t;
	}
	curve += _T (':');

	if (_curveNameCtrl.FindStringExact(-1, name) == CB_ERR)
	{
		_curveNameCtrl.AddString (name);
		
		CString t;
		t = AfxGetApp ()->GetProfileString (_T ("curves"), _curveEntry + _T (":List"), _T (""));
		AfxGetApp ()->WriteProfileString (_T ("curves"), _curveEntry + _T (":List"), t + name + _T (':'));
	}
	AfxGetApp()->WriteProfileString (_T ("curves"), _curveEntry + _T ('_') + name, curve);
}

void CCurveDlg::OnCurveLoad() 
{
	// TODO: Add your control notification handler code here

	CString str;
	_curveNameCtrl.GetWindowText (str);

	if (str.IsEmpty())
	{
		return;
	}
	if (str.Find(_T (':')) != -1)
	{
		MessageBox (_T ("I am sorry, but curve name can not contain char ':'."));
		return;
	}

	str = AfxGetApp ()->GetProfileString(_T ("curves"), _curveEntry + _T ('_') + str, NULL);
	if (str.IsEmpty())
	{
		MessageBox (_T ("I am sorry, but any curve with this name does not exist."));
		return;
	}

	int i = str.Find (_T (':'));
	if (i == -1)
	{
		MessageBox (_T ("I am sorry, but any curve with this name does not exist."));
		return;
	}
	CString xs = str.Mid (0, i);

	++i;
	int i2 = str.Find (_T (':'), i);
	if (i2 == -1)
	{
		MessageBox (_T ("I am sorry, but any curve with this name does not exist."));
		return;
	}
	CString values = str.Mid (i, i2 - i);

	i = i2 + 1;
	i2 = str.Find (_T (':'), i);
	if (i2 == -1)
	{
		MessageBox (_T ("I am sorry, but any curve with this name does not exist."));
		return;
	}
	CString tangents = str.Mid (i, i2 - i);

	i = i2 + 1;
	i2 = str.Find (_T (':'), i);
	if (i2 == -1)
	{
		MessageBox (_T ("I am sorry, but any curve with this name does not exist."));
		return;
	}
	CString types = str.Mid (i, i2 - i);

	int n1 = GetMarkSize ();
	RemoveAllMarks();

	int xs_s = 0;
	int xs_e = xs.Find (_T (','));
	int values_s = 0;
	int values_e = values.Find (_T (','));
	int tangents_s = 0;
	int tangents_e = tangents.Find (_T (','));
	int types_s = 0;
	int types_e = types.Find (_T (','));
	float x = 0.0f, value = 0.0f, tangent = 0.0f, type = 1;
	while (xs_e != -1 && values_e != -1 && tangents_e != -1 && types_e != -1)
	{
		CString xs_str = xs.Mid (xs_s, xs_e - xs_s);
		CString values_str = values.Mid (values_s, values_e - values_s);
		CString tangents_str = tangents.Mid (tangents_s, tangents_e - tangents_s);
		CString types_str = types.Mid (types_s, types_e - types_s);

		x = (float) _tcstod (xs_str, NULL);
		value = (float) _tcstod (values_str, NULL);
		tangent = (float) _tcstod (tangents_str, NULL);
		type = (float) _tcstod (types_str, NULL);

		AddMark (x, value, tangent, type, _T (""));

		xs_s = xs_e + 1;
		xs_e = xs.Find (_T (','), xs_s);
		values_s = values_e + 1;
		values_e = values.Find (_T (','), values_s);
		tangents_s = tangents_e + 1;
		tangents_e = tangents.Find (_T (','), tangents_s);
		types_s = types_e + 1;
		types_e = types.Find (_T (','), types_s);
	}

	if (!_varPointNumber)
	{
		int n2 = GetMarkSize ();
		if (n1 != n2)
		{
			if (n1 < n2)
			{
				while (n1 < n2)
				{
					--n2;
					RemoveMarkAt(n2);
				}
				SetMarkType (n1 - 1, _maxKeyType - 1);
			}
			else
			{
				while (n1 > GetMarkSize ())
				{
					AddMark (x, value, tangent, type, _T (""));
				}
			}
		}
	}

	CurveChanged();
}

void CCurveDlg::RemoveAllMarks ()
{
	_keyTypes.RemoveAll ();
	_names.RemoveAll ();
	_values.RemoveAll ();
}

void CCurveDlg::AddMark (float x, float value, float tangent, int keyType, LPCTSTR name)
{
	_keyTypes.Add (keyType);
	_names.Add (name);

	switch (_curveType)
	{
	case LINEAR_CURVE_WITH_X:
		_values.Add(x);
		_values.Add(value);
		break;

	case QUADRATIC_CURVE:
		if (_values.GetSize() == 0)
		{
			_values.Add (tangent);
		}
		_values.Add(value);
		break;

	case QUADRATIC_CURVE_WITH_X:
		if (_values.GetSize() == 0)
		{
			_values.Add (tangent);
		}
		_values.Add(x);
		_values.Add(value);
		break;

	case EX_QUADRATIC_CURVE:
		_values.Add(value);
		_values.Add(tangent);
		break;

	case EX_QUADRATIC_CURVE_WITH_X:
		_values.Add(x);
		_values.Add(value);
		_values.Add(tangent);
		break;

	// case LINEAR_CURVE:
	default:
		_values.Add(value);
		break;
	}
}

void CCurveDlg::OnMirrorX() 
{
	// TODO: Add your control notification handler code here

	CArray<float, float> xs;
	CArray<float, float> values;
	CArray<float, float> tangents;
	CArray<int, int> types;
	CStringArray names;

	int n = GetMarkSize ();
	for (int i = 0; i < n; ++i)
	{
		xs.Add (GetMarkX(i));
		values.Add (GetMarkValue(i));
		tangents.Add (GetMarkTangent (i));
		types.Add (GetMarkType (i));
		names.Add (GetMarkName (i));
	}

	RemoveAllMarks();

	switch (_curveType)
	{
	case LINEAR_CURVE_WITH_X:
		{
			for (int i = 0; i < n; )
			{
				++i;
				AddMark (1.0f - xs[n - i], values[n - i], 0, types[n - i], names[n - i]);
			}
		}
		break;

	case EX_QUADRATIC_CURVE:
		{
			CArray<float, float> mirrorTangents;
			mirrorTangents.Add (0.0f);
			for (int i = 1; i < n; ++i)
			{
				float tan = tangents[i - 1];
				float dx = xs[i] - xs[i - 1];
				float dy = values[i] - values[i - 1];
				tan += 2 * (dy - tan * dx) / dx;
				mirrorTangents.Add (-tan);
			}
			for (int i = 0; i < n; )
			{
				++i;
				AddMark (0.0f, values[n - i], mirrorTangents[n - i], types[n - i], names[n - i]);
			}
		}
		break;

	case EX_QUADRATIC_CURVE_WITH_X:
		{
			CArray<float, float> mirrorTangents;
			mirrorTangents.Add (0.0f);
			for (int i = 1; i < n; ++i)
			{
				float tan = tangents[i - 1];
				float dx = xs[i] - xs[i - 1];
				float dy = values[i] - values[i - 1];
				tan += 2 * (dy - tan * dx) / dx;
				mirrorTangents.Add (-tan);
			}
			for (int i = 0; i < n; )
			{
				++i;
				AddMark (1.0f - xs[n - i], values[n - i], mirrorTangents[n - i], types[n - i], names[n - i]);
			}
		}
		break;

	case QUADRATIC_CURVE:
		{
			float tan = tangents[0];
			for (int i = 1; i < n; ++i)
			{
				float dx = xs[i] - xs[i - 1];
				float dy = values[i] - values[i - 1];
				tan += 2 * (dy - tan * dx) / dx;
			}
			tan = -tan;
			AddMark (0.0f, values[n - 1], tan, types[n - 1], names[n - 1]);
			for (int i = 1; i < n; )
			{
				++i;
				AddMark (0.0f, values[n - i], 0, types[n - i], names[n - i]);
			}
		}
		break;

	case QUADRATIC_CURVE_WITH_X:
		{
			float tan = tangents[0];
			for (int i = 1; i < n; ++i)
			{
				float dx = xs[i] - xs[i - 1];
				float dy = values[i] - values[i - 1];
				tan += 2 * (dy - tan * dx) / dx;
			}
			tan = -tan;
			AddMark (1.0f - xs[n - 1], values[n - 1], tan, types[n - 1], names[n - 1]);
			for (int i = 1; i < n; )
			{
				++i;
				AddMark (1.0f - xs[n - i], values[n - i], 0, types[n - i], names[n - i]);
			}
		}
		break;

	// case LINEAR_CURVE:
	default:
		{
			for (int i = 0; i < n; )
			{
				++i;
				AddMark (0.0f, values[n - i], 0, types[n - i], names[n - i]);
			}
		}
		break;
	}

	CurveChanged();
}

void CCurveDlg::OnCurveDelete() 
{
	// TODO: Add your control notification handler code here
	
	CString str;
	_curveNameCtrl.GetWindowText (str);

	if (str.IsEmpty())
	{
		return;
	}
	if (str.Find(_T (':')) != -1)
	{
		MessageBox (_T ("I am sorry, but curve name can not contain char ':'."));
		return;
	}

	CString names = AfxGetApp ()->GetProfileString (_T ("curves"), _curveEntry + _T (":List"), NULL);

	CString str2 = str + _T (':');
	int i = names.Find (str2);
	if (i != -1)
	{
		names = names.Left(i) + names.Mid (i + str2.GetLength());
		AfxGetApp()->WriteProfileString(_T ("curves"), _curveEntry + _T (":List"), names);
		_curveNameCtrl.DeleteString (_curveNameCtrl.FindStringExact(-1, str));
	}
}

void CCurveDlg::SetModified (bool modified /* = true */)
{
	_modified = modified;
}

bool CCurveDlg::IsModified () const
{
	return _modified;
}

void CCurveDlg::OnCurveScaleMul() 
{
	// TODO: Add your control notification handler code here

	CString val;
	_scaleCtrl.GetWindowText (val);
	LPCTSTR beg = (LPCTSTR) val;
	LPTSTR end;
	float mul = _tcstod (beg, &end);
	if (beg == end)
	{
		return;
	}

	int n = GetMarkSize ();
	for (int i = 0; i < n; ++i)
	{
		float f = GetMarkValue (i) - _min;
		f *= mul;
		SetMarkValue (i, f + _min);
	}

	CurveChanged ();
}

void CCurveDlg::OnCurveScaleDiv() 
{
	// TODO: Add your control notification handler code here
	
	CString val;
	_scaleCtrl.GetWindowText (val);
	LPCTSTR beg = (LPCTSTR) val;
	LPTSTR end;
	float div = _tcstod (beg, &end);
	if (beg == end)
	{
		return;
	}

	int n = GetMarkSize ();
	for (int i = 0; i < n; ++i)
	{
		float f = GetMarkValue (i) - _min;
		f /= div;
		SetMarkValue (i, f + _min);
	}

	CurveChanged ();
}

void CCurveDlg::GetLinearCurveString (CString &str) const
{
	str.Format (_T ("%g"), GetMarkValue(0));
	int n = GetMarkSize();
	for (int i = 1; i < n; ++i)
	{
		CString tmp;
		tmp.Format (_T (", %g"), GetMarkValue(i));
		str += tmp;
	}
}

void CCurveDlg::GetLinearCurveWithXString (CString &str) const
{
	str.Format (_T ("%g, %g"), GetMarkX(0), GetMarkValue(0));
	int n = GetMarkSize();
	for (int i = 1; i < n; ++i)
	{
		CString tmp;
		tmp.Format (_T (", %g, %g"), GetMarkX(i), GetMarkValue(i));
		str += tmp;
	}
}

void CCurveDlg::GetQuadraticCurveString (CString &str) const
{
	str.Format (_T ("%g"), GetMarkTangent(0));
	int n = GetMarkSize();
	for (int i = 0; i < n; ++i)
	{
		CString tmp;
		tmp.Format (_T (", %g"), GetMarkValue(i));
		str += tmp;
	}
}

void CCurveDlg::GetQuadraticCurveWithXString (CString &str) const
{
	str.Format (_T ("%g"), GetMarkTangent(0));
	int n = GetMarkSize();
	for (int i = 0; i < n; ++i)
	{
		CString tmp;
		tmp.Format (_T (", %g, %g"), GetMarkX(i), GetMarkValue(i));
		str += tmp;
	}
}

void CCurveDlg::GetExQuadraticCurveString (CString &str) const
{
	str.Format (_T ("%g, %g"), GetMarkValue(0), GetMarkTangent(0));
	int n = GetMarkSize();
	for (int i = 1; i < n; ++i)
	{
		CString tmp;
		tmp.Format (_T (", %g, %g"), GetMarkValue(i), GetMarkTangent(i));
		str += tmp;
	}
}

void CCurveDlg::GetExQuadraticCurveWithXString (CString &str) const
{
	str.Format (_T ("%g, %g, %g"), GetMarkX(0), GetMarkValue(0), GetMarkTangent(0));
	int n = GetMarkSize();
	for (int i = 1; i < n; ++i)
	{
		CString tmp;
		tmp.Format (_T (", %g, %g, %g"), GetMarkX(i), GetMarkValue(i), GetMarkTangent(i));
		str += tmp;
	}
}

void CCurveDlg::OnChangeCurveX() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	
	if (_curve._draggedMark != -1 && ::IsWindow (_curveXCtrl.m_hWnd))
	{
		CString str;
		_curveXCtrl.GetWindowText(str);
		LPCTSTR start = (LPCTSTR) str;
		TCHAR *end;
		float f = _tcstod (start, &end);
		if (end != start)
		{
			SetMarkX(_curve._draggedMark, f);
			CurveChanged();
		}
	}
}

void CCurveDlg::OnKillFocusCurveX() 
{
	// TODO: Add your control notification handler code here
	
	if (_curve._draggedMark != -1 && ::IsWindow (_curveXCtrl.m_hWnd))
	{
		CString str;
		str.Format(_T ("%g"), GetMarkX(_curve._draggedMark));
		_curveXCtrl.SetWindowText(str);
	}
}

void CCurveDlg::OnStraight() 
{
	// TODO: Add your control notification handler code here
	
	if (::IsWindow (_directionCtrl.m_hWnd) && _curve._draggedMark >= 0 && _curve._draggedMark < GetMarkSize () - 1)
	{
		int i = _curve._draggedMark;
		CString str;
		float x2 = GetMarkX (i + 1);
		float x1 = GetMarkX (i);
		float y2 = GetMarkValue (i + 1);
		float y1 = GetMarkValue (i);
		float tangent = x2 > x1 ? (y2 - y1) / (x2 - x1) : FLT_MAX;
		str.Format(_T ("%g"), tangent);
		_directionCtrl.SetWindowText(str);

		int mt = GetMarkType (i);
		while (i > 0 && mt < 2)
		{
			--i;
			mt = GetMarkType (i);
			if (mt > 0)
			{
				x2 = x1;
				x1 = GetMarkX (i);
				y2 = y1;
				y1 = GetMarkValue (i);
				float dx = x2 - x1;
				float dy = y2 - y1;
				tangent = dx != 0.0f ? (2 * dy / dx - tangent) : FLT_MAX;
			}
		}
		SetMarkTangent(i, tangent);
		CurveChanged ();
	}
}

void CCurveDlg::OnKillFocusCurveDirection() 
{
	// TODO: Add your control notification handler code here
	
	if (_curve._draggedMark != -1 && ::IsWindow (_directionCtrl.m_hWnd))
	{
		CString str;
		str.Format(_T ("%g"), GetMarkTangent(_curve._draggedMark));
		_directionCtrl.SetWindowText(str);
	}
}
