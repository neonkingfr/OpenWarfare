#if !defined(AFX_CURVEITEM_H__78EC6571_F3FC_4F03_9B2B_4AFAE808F1AD__INCLUDED_)
#define AFX_CURVEITEM_H__78EC6571_F3FC_4F03_9B2B_4AFAE808F1AD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CurveItem.h : header file
//

class CCurveDlg;

/////////////////////////////////////////////////////////////////////////////
// CCurveItem window

class CCurveItem : public CButton
{
// Construction
public:
	CCurveItem();

// Attributes
public:
	enum { MARK_SIZE = 8, };
	enum { MARK_WIDTH = 3, };
	enum { AXIS_FONT_SIZE = 9, };

	enum
	{
		_mmmNone,
		_mmmScale,
		_mmmShift,
		_mmmMoveX,
	};

	CCurveDlg *_dlg;
	int _draggedMark;
	int _mouseMoveMode;
	CArray<float, float> _mmmRefValues;

// Operations
public:
	void PaintLinearCurve (CDC &dc, CRect &r);
	void PaintQuadraticCurve (CDC &dc, CRect &r);
	void PaintExQuadraticCurve (CDC &dc, CRect &r);
	void PaintMark (CDC &dc, CPoint &p, CPen &pen);
	void PaintMark (CDC &dc, CRect &r, float x, float y, int keyType, int index);
	void CalcMarkPoint (float x, float y, CPoint &p, CRect &r);
	void CalcCurveRect (CRect &r);
	int FindMark (CPoint &p, CRect &r, bool onlyKeys);
	int FindLeftMark (CPoint &p, CRect &r, bool onlyKeys);
	void PaintLineSegment (CDC &dc, CRect &r, float x1, float y1, float x2, float y2);
	void PaintCurveSegment (CDC &dc, CRect &r, float x1, float y1, float x2, float y2, float a, float b, float range);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCurveItem)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CCurveItem();

	// Generated message map functions
protected:
	//{{AFX_MSG(CCurveItem)
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnPaint();
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CURVEITEM_H__78EC6571_F3FC_4F03_9B2B_4AFAE808F1AD__INCLUDED_)
