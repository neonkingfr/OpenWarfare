// DebugBar.h: interface for the CDebugBar class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DEBUGBAR_H__7DD156C0_2EEE_4313_A962_118BB6E3ABE5__INCLUDED_)
#define AFX_DEBUGBAR_H__7DD156C0_2EEE_4313_A962_118BB6E3ABE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CDebugBar : public CDialogBar  
{
public:
	CSize m_sizeDocked;
	CSize m_sizeFloating;
	BOOL m_bChangeDockedSize;

public:
	CDebugBar();
	virtual ~CDebugBar();

	BOOL Create( CWnd* pParentWnd, UINT nIDTemplate, UINT nStyle,
		UINT nID, CSize *s = NULL, BOOL = TRUE);
	BOOL Create( CWnd* pParentWnd, LPCTSTR lpszTemplateName,
		UINT nStyle, UINT nID, BOOL = TRUE);
	
	virtual CSize CalcDynamicLayout( int nLength, DWORD dwMode );
	CSize CalcDimension( int nLength, DWORD dwMode );

	void LogF(const char *format, va_list argptr);
};

#endif // !defined(AFX_DEBUGBAR_H__7DD156C0_2EEE_4313_A962_118BB6E3ABE5__INCLUDED_)
