// PairDlg.cpp : implementation file
//

#include "stdafx.h"
#include <El/elementpch.hpp>
#include "RV/Tree/mytree.h"
#include "treeditor.h"
#include "PairDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPairDlg dialog


CPairDlg::CPairDlg(LPCTSTR title, LPCTSTR valueLabel, LPCTSTR intervalLabel, float min, float max, float step, const CArray<float, float> &arr, CWnd* pParent /*=NULL*/)
	: CDialog(CPairDlg::IDD, pParent), _min (min), _max (max), _updateLock (false), _step (step), _title (title),
	_intervalLabel (intervalLabel), _valueLabel (valueLabel)
{
	//{{AFX_DATA_INIT(CPairDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	int n = arr.GetSize();
	_value = n > 0 ? arr[0] : 0.0f;
	_interval = n > 1 ? arr[1] : 0.0f;
}


void CPairDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPairDlg)
	DDX_Control(pDX, IDC_VALUE_LABEL, _valueLabelCtrl);
	DDX_Control(pDX, IDC_INTERVAL_LABEL, _intervalLabelCtrl);
	DDX_Control(pDX, IDC_VALUE_SLIDER, _valueSlider);
	DDX_Control(pDX, IDC_VALUE, _valueCtrl);
	DDX_Control(pDX, IDC_INTERVAL_SLIDER, _intervalSlider);
	DDX_Control(pDX, IDC_INTERVAL, _intervalCtrl);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPairDlg, CDialog)
	//{{AFX_MSG_MAP(CPairDlg)
	ON_WM_HSCROLL()
	ON_EN_CHANGE(IDC_INTERVAL, OnChangeInterval)
	ON_EN_CHANGE(IDC_VALUE, OnChangeValue)
	ON_EN_KILLFOCUS(IDC_INTERVAL, OnKillFocusInterval)
	ON_EN_KILLFOCUS(IDC_VALUE, OnKillFocusValue)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPairDlg message handlers

BOOL CPairDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString str;
	str.Format (_T ("Pair: %s"), _title);
	SetWindowText(str);

	str.Format(_T ("%g"), _value);
	_valueCtrl.SetWindowText(str);
	str.Format(_T ("%g"), _interval);
	_intervalCtrl.SetWindowText (str);

	CalcSliderExpAndInterval (_min, _max, _valueExp, _valueRange);
	_valueSlider.SetRange (0, (int) _valueRange, FALSE);
	_valueSlider.SetPos ((int) ((_value - _min) * _valueExp));

	CalcSliderExpAndInterval (0, _value - _min, _intervalExp, _intervalRange);
	_intervalSlider.SetRange (0, (int) _intervalRange, FALSE);
	_intervalSlider.SetPos ((int) ((_interval) * _intervalExp));

	str.Format (_T ("%s:"), _valueLabel);
	_valueLabelCtrl.SetWindowText(str);

	str.Format (_T ("%s:"), _intervalLabel);
	_intervalLabelCtrl.SetWindowText(str);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CPairDlg::CalcSliderExpAndInterval (const float min, const float max, float &exp, float &range)
{
	range = max - min;
	exp = 1;
	if (range == 0.0f)
	{
		return;
	}
	else if (range < (INT_MAX >> 1))
	{
		while (range < (INT_MAX >> 1))
		{
			range *= 2.0f;
			exp *= 2.0f;
		}
	}
	else
	{
		while (range > (INT_MAX >> 1))
		{
			range *= 0.5f;
			exp *= 0.5f;
		}
	}
}

void CPairDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: Add your message handler code here and/or call default
	
	if (pScrollBar == GetDlgItem (IDC_VALUE_SLIDER))
	{
		_updateLock = true;

		float newValue = _min + _valueSlider.GetPos() / _valueExp;
		_value = newValue;
		if (_step != 0.0f)
		{
			_value -= fmod (_value - _min, _step);
		}
		
		CString str;
		str.Format (_T ("%g"), _value);
		_valueCtrl.SetWindowText (str);

		CalcSliderExpAndInterval (0, _value - _min, _intervalExp, _intervalRange);
		if (_interval > _value - _min)
		{
			_interval = _value - _min;
		}
		if (_step != 0.0f)
		{
			_interval -= fmod (_interval, _step);
		}
		str.Format (_T ("%g"), _interval);
		_intervalCtrl.SetWindowText(str);
		_intervalSlider.SetRange (0, (int) _intervalRange, TRUE);
		_intervalSlider.SetPos ((int) ((_interval) * _intervalExp));

		_updateLock = false;
	}
	else if (pScrollBar == GetDlgItem (IDC_INTERVAL_SLIDER))
	{
		_updateLock = true;
	
		_interval = _intervalSlider.GetPos () / _intervalExp;
		if (_step != 0.0f)
		{
			_interval -= fmod (_interval, _step);
		}

		CString str;
		str.Format (_T ("%g"), _interval);
		_intervalCtrl.SetWindowText(str);

		_updateLock = false;
	}
	else
	{
		CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
	}
}

void CPairDlg::OnChangeInterval() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	
	if (_updateLock)
	{
		return;
	}
	_updateLock = true;

	CString str;
	_intervalCtrl.GetWindowText (str);
	
	LPCTSTR start = (LPCTSTR) str;
	LPTSTR end;
	float v = (float) _tcstod (start, &end);
	if (start != end && v <= _value - _min)
	{
		_interval = v;
		_intervalSlider.SetPos ((int) ((_interval) * _intervalExp));
	}

	_updateLock = false;
}

void CPairDlg::OnKillFocusInterval() 
{
	// TODO: Add your control notification handler code here
	
	if (_updateLock)
	{
		return;
	}
	_updateLock = true;

	CString str;
	_intervalCtrl.GetWindowText (str);
	
	LPCTSTR start = (LPCTSTR) str;
	LPTSTR end;
	float v = (float) _tcstod (start, &end);
	if (start != end && v <= _value - _min)
	{
		_interval = v;
		if (_step != 0.0f)
		{
			_interval -= fmod (_interval, _step);
		}
		_intervalSlider.SetPos ((int) ((_interval) * _intervalExp));
		str.Format (_T ("%g"), _interval);
		_intervalCtrl.SetWindowText(str);
	}

	_updateLock = false;
}

void CPairDlg::OnChangeValue() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here

	if (_updateLock)
	{
		return;
	}
	_updateLock = true;
	
	CString str;
	_valueCtrl.GetWindowText (str);
	
	LPCTSTR start = (LPCTSTR) str;
	LPTSTR end;
	float v = (float) _tcstod (start, &end);
	if (start != end && v > _min)
	{
		_value = v;
		_valueSlider.SetPos ((int) ((_value - _min) * _valueExp));
		
		CalcSliderExpAndInterval (0, _value - _min, _intervalExp, _intervalRange);
		if (_interval > _value - _min)
		{
			_interval = _value - _min;
		}
		str.Format (_T ("%g"), _interval);
		_intervalCtrl.SetWindowText(str);
		_intervalSlider.SetRange (0, (int) _intervalRange, TRUE);
		_intervalSlider.SetPos ((int) ((_interval) * _intervalExp));
	}

	_updateLock = false;
}

void CPairDlg::OnKillFocusValue() 
{
	// TODO: Add your control notification handler code here
	
	if (_updateLock)
	{
		return;
	}
	_updateLock = true;
	
	CString str;
	_valueCtrl.GetWindowText (str);
	
	LPCTSTR start = (LPCTSTR) str;
	LPTSTR end;
	float v = (float) _tcstod (start, &end);
	if (start != end && v > _min)
	{
		_value = v;
		if (_step != 0.0f)
		{
			_value -= fmod (_value - _min, _step);
		}
		_valueSlider.SetPos ((int) ((_value - _min) * _valueExp));
		str.Format (_T ("%g"), _value);
		_valueCtrl.SetWindowText(str);
		
		CalcSliderExpAndInterval (0, _value - _min, _intervalExp, _intervalRange);
		if (_interval > _value - _min)
		{
			_interval = _value - _min;
		}
		if (_step != 0.0f)
		{
			_interval -= fmod (_interval, _step);
		}
		str.Format (_T ("%g"), _interval);
		_intervalCtrl.SetWindowText(str);
		_intervalSlider.SetRange (0, (int) _intervalRange, TRUE);
		_intervalSlider.SetPos ((int) ((_interval) * _intervalExp));
	}

	_updateLock = false;
}
