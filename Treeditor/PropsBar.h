// PropsBar.h: interface for the CPropsBar class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROPSBAR_H__5B05CE54_4591_456F_8880_DBE904637403__INCLUDED_)
#define AFX_PROPSBAR_H__5B05CE54_4591_456F_8880_DBE904637403__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CPropsBar : public CToolBar  
{
public:
	CPropsBar();
	virtual ~CPropsBar();

};

#endif // !defined(AFX_PROPSBAR_H__5B05CE54_4591_456F_8880_DBE904637403__INCLUDED_)
