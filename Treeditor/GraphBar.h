// GraphBar.h: interface for the CGraphBar class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GRAPHBAR_H__DC0FF2A4_7F93_4EB5_BD43_F6371C455FAB__INCLUDED_)
#define AFX_GRAPHBAR_H__DC0FF2A4_7F93_4EB5_BD43_F6371C455FAB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "CompGraphCtrl.h"


class CGraphBar : public CDialogBar  
{
public:
	CSize m_sizeDocked;
	CSize m_sizeFloating;
	BOOL m_bChangeDockedSize;

	CCompGraphCtrl _graphCtrl;

public:
	CGraphBar();
	virtual ~CGraphBar();

	BOOL Create( CWnd* pParentWnd, UINT nIDTemplate, UINT nStyle,
		UINT nID, CSize *s = NULL, BOOL = TRUE);
	BOOL Create( CWnd* pParentWnd, LPCTSTR lpszTemplateName,
		UINT nStyle, UINT nID, BOOL = TRUE);
	
	virtual CSize CalcDynamicLayout( int nLength, DWORD dwMode );
	CSize CalcDimension( int nLength, DWORD dwMode );

	void ResetZoom();

	virtual BOOL OnCmdMsg (UINT nID, int nCode, void *pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
};

#endif // !defined(AFX_GRAPHBAR_H__DC0FF2A4_7F93_4EB5_BD43_F6371C455FAB__INCLUDED_)
