// PreferencesDlg.cpp : implementation file
//

#include "stdafx.h"
#include <El/elementpch.hpp>
#include "RV/Tree/mytree.h"
#include "Treeditor.h"
#include "TreeditorLex.h"

#include "TreeditorClass.h"
#include "PreferencesDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPreferencesDlg dialog


CPreferencesDlg::CPreferencesDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPreferencesDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPreferencesDlg)
	//}}AFX_DATA_INIT
}


void CPreferencesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPreferencesDlg)
	DDX_Control(pDX, IDC_SCHILD, _scriptChildCtrl);
	DDX_Control(pDX, IDC_EXEDITOR, _exEditorCtrl);
	DDX_Control(pDX, IDC_EXTENSION, _extensionCtrl);
	DDX_Control(pDX, IDC_SCHILD_LIST, _scriptChildList);
	DDX_Control(pDX, IDC_CHILD_COMBO, _childCombo);
	DDX_Control(pDX, IDC_COMPONENT_COMBO, _componentCombo);
	DDX_Control(pDX, IDC_EXEDITOR_LIST, _exEditorList);
	DDX_Control(pDX, IDC_COMPONENT_COLOR, _componentColorCtrl);
	DDX_Control(pDX, IDC_CHILD_COLOR, _childColorCtrl);
	DDX_Control(pDX, IDC_SCHILD_DEFCOLOR, _scriptChildDefColorCtrl);
	DDX_Control(pDX, IDC_SCHILD_COLOR, _scriptChildColorCtrl);
	DDX_Control(pDX, IDC_GT_WIDTH, _gtWidth);
	DDX_Control(pDX, IDC_GT_HEIGHT, _gtHeight);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPreferencesDlg, CDialog)
	//{{AFX_MSG_MAP(CPreferencesDlg)
	ON_LBN_SELCHANGE(IDC_EXEDITOR_LIST, OnSelChangeExEditorList)
	ON_LBN_SELCHANGE(IDC_SCHILD_LIST, OnSelChangeSChildList)
	ON_CBN_SELCHANGE(IDC_CHILD_COMBO, OnSelChangeChildCombo)
	ON_CBN_SELCHANGE(IDC_COMPONENT_COMBO, OnSelChangeComponentCombo)
	ON_BN_CLICKED(IDC_ADDCHANGE_EXEDITOR, OnAddChangeExEditor)
	ON_BN_CLICKED(IDC_REMOVE_EXEDITOR, OnRemoveExEditor)
	ON_BN_CLICKED(IDC_ADDCHANGE_SCHILD, OnAddChangeSChild)
	ON_BN_CLICKED(IDC_REMOVE_SCHILD, OnRemoveSChild)
	ON_BN_CLICKED(IDC_SCHILD_COLOR, OnSChildColor)
	ON_BN_CLICKED(IDC_SCHILD_DEFCOLOR, OnSChildDefColor)
	ON_BN_CLICKED(IDC_CHILD_COLOR, OnChildColor)
	ON_BN_CLICKED(IDC_COMPONENT_COLOR, OnComponentColor)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPreferencesDlg message handlers

BOOL CPreferencesDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString str = AfxGetApp ()->GetProfileString (_T ("ground"), _T("width"), _T ("1"));
	_gtWidth.SetWindowText(str);

	str = AfxGetApp ()->GetProfileString (_T ("ground"), _T("height"), _T ("1"));
	_gtHeight.SetWindowText (str);

	// external editors
	str = AfxGetApp ()->GetProfileString(_T ("externalEditors"), _T ("extensions"), _T (""));
	LPCTSTR p = (LPCTSTR) str;
	LPCTSTR t = _tcschr (p, _T (':'));
	while (t != NULL)
	{
		CString extension = CString (p, t - p);
		CString editor = AfxGetApp ()->GetProfileString (_T ("externalEditors"), extension, NULL);
		if (!editor.IsEmpty())
		{
			_exEditorList.AddString(extension);

			EditorAssoc ea;
			ea._extension = extension;
			ea._editor = editor;
			_externalEditors.Add (ea);
		}
		p = t;
		++p;
		t = _tcschr (p, _T (':'));
	}

	// component node colors
	for (int i = 0; i < treeditorClass_numberOfTypes; ++i)
	{
		IntColorAssoc ic;
		ic._id = i;
		CString str;
		str.Format (_T ("componentNodeColor%02d"), i);
		ic._color = AfxGetApp()->GetProfileInt(_T ("editor"), str, RGB (0, 0, 160));
		_componentColors.Add (ic);
		_componentCombo.AddString (CTreeditorClass::classTypes[i].name);
	}

	// child link color
	for (int i = 0; i < treeditorClassParam_numberOfTypes; ++i)
	{
		if (CTreeditorParam::classParams[i].isChildClassName)
		{
			IntColorAssoc ic;
			ic._id = i;
			CString str;
			str.Format(_T ("childLinkColor%02d"), i);
			ic._color = AfxGetApp()->GetProfileInt(_T ("editor"), str, RGB (0, 0, 160));
			_childColors.Add (ic);
			_childCombo.AddString (CTreeditorParam::classParams[i].uiName);
		}
	}
	
	// script child link color
	_scriptChildDefColor = AfxGetApp ()->GetProfileInt(_T ("editor"), _T ("childLinkDefColor"), RGB (0, 0, 160));
	str = AfxGetApp ()->GetProfileString(_T ("editor"), _T ("childLinkColors"), _T (""));
	p = (LPCTSTR) str;
	t = _tcschr (p, _T (':'));
	while (t != NULL)
	{
		StringColorAssoc sc;
		sc._name = CString (p, t - p);
		CString str;
		str.Format (_T ("childLinkColor_%s"), sc._name);
		sc._color = AfxGetApp ()->GetProfileInt (_T ("editor"), str, RGB (0, 0, 160));
		_scriptChildColors.Add(sc);
		_scriptChildList.AddString(sc._name);
		p = t;
		++p;
		t = _tcschr (p, _T (':'));
	}

	_scriptChildDefColorCtrl.SetColor(_scriptChildDefColor);
	if (_childColors.GetSize () > 0)
	{
		_childCombo.SetCurSel (0);
		_childColorCtrl.SetColor (_childColors[0]._color);
	}
	if (_componentColors.GetSize () > 0)
	{
		_componentCombo.SetCurSel (0);
		_componentColorCtrl.SetColor (_componentColors[0]._color);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CPreferencesDlg::OnOK() 
{
	// TODO: Add extra validation here
	
#define GET_FLOAT(ctrl,section,entry) \
	{\
		CString str;\
		ctrl.GetWindowText(str);\
		LPCTSTR start = (LPCTSTR) str;\
		LPTSTR end;\
		(void) _tcstod (start, &end);\
		if (start != end)\
		{\
			AfxGetApp ()->WriteProfileString (section, entry, str);\
		}\
	}
	
	GET_FLOAT (_gtWidth, _T ("ground"), _T ("width"))
	GET_FLOAT (_gtHeight, _T ("ground"), _T ("height"))

	AfxGetApp()->WriteProfileInt(_T ("editor"), _T ("childLinkDefColor"), _scriptChildDefColor);

	CString str;
	int n = _externalEditors.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		str += _externalEditors[i]._extension;
		str += _T (':');
		AfxGetApp()->WriteProfileString(_T ("externalEditors"), _externalEditors[i]._extension, _externalEditors[i]._editor);
	}
	AfxGetApp()->WriteProfileString(_T ("externalEditors"), _T ("extensions"), str);

	str.Empty();
	n = _scriptChildColors.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		str += _scriptChildColors[i]._name;
		str += _T (':');
		CString t;
		t.Format (_T ("childLinkColor_%s"), _scriptChildColors[i]._name);
		AfxGetApp()->WriteProfileInt(_T ("editor"), t, _scriptChildColors[i]._color);
	}
	AfxGetApp()->WriteProfileString(_T ("editor"), _T ("childLinkColors"), str);

	n = _childColors.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		str.Format(_T ("childLinkColor%02d"), _childColors[i]._id);
		AfxGetApp()->WriteProfileInt(_T ("editor"), str, _childColors[i]._color);
	}

	n = _componentColors.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		str.Format(_T ("componentNodeColor%02d"), _componentColors[i]._id);
		AfxGetApp()->WriteProfileInt(_T ("editor"), str, _componentColors[i]._color);
	}

	CDialog::OnOK();
}

void CPreferencesDlg::OnSelChangeExEditorList() 
{
	// TODO: Add your control notification handler code here

	int i = _exEditorList.GetCurSel ();
	if (i != LB_ERR)
	{
		_exEditorCtrl.SetWindowText(_externalEditors[i]._editor);
		_extensionCtrl.SetWindowText(_externalEditors[i]._extension);
	}
}

void CPreferencesDlg::OnSelChangeSChildList() 
{
	// TODO: Add your control notification handler code here
	
	int i = _scriptChildList.GetCurSel ();
	if (i != LB_ERR)
	{
		_scriptChildCtrl.SetWindowText(_scriptChildColors[i]._name);
		_scriptChildColorCtrl.SetColor(_scriptChildColors[i]._color);
	}
}

void CPreferencesDlg::OnSelChangeChildCombo() 
{
	// TODO: Add your control notification handler code here
	
	int i = _childCombo.GetCurSel ();
	if (i != CB_ERR)
	{
		_childColorCtrl.SetColor(_childColors[i]._color);
	}
}

void CPreferencesDlg::OnSelChangeComponentCombo() 
{
	// TODO: Add your control notification handler code here
	
	int i = _componentCombo.GetCurSel ();
	if (i != CB_ERR)
	{
		_componentColorCtrl.SetColor(_componentColors[i]._color);
	}
}

void CPreferencesDlg::OnAddChangeExEditor() 
{
	// TODO: Add your control notification handler code here
	
	int i = _exEditorList.GetCurSel ();
	CString newExt;
	_extensionCtrl.GetWindowText(newExt);
	if (i != LB_ERR && newExt.Compare (_externalEditors[i]._extension) == 0)
	{
		// change
		_exEditorCtrl.GetWindowText(_externalEditors[i]._editor);
	}
	else
	{
		// add
		EditorAssoc ea;
		_exEditorCtrl.GetWindowText (ea._editor);
		_extensionCtrl.GetWindowText (ea._extension);
		_externalEditors.Add (ea);
		_exEditorList.SetCurSel (_exEditorList.AddString (ea._extension));
	}
}

void CPreferencesDlg::OnRemoveExEditor() 
{
	// TODO: Add your control notification handler code here
	
	int i = _exEditorList.GetCurSel ();
	if (i != LB_ERR)
	{
		_externalEditors.RemoveAt (i);
		_exEditorList.DeleteString(i);
	}
}

void CPreferencesDlg::OnAddChangeSChild() 
{
	// TODO: Add your control notification handler code here
	
	int i = _scriptChildList.GetCurSel ();
	CString newName;
	_scriptChildCtrl.GetWindowText(newName);
	if (i != LB_ERR && newName.Compare (_scriptChildColors[i]._name) == 0)
	{
		// change
		_scriptChildColors[i]._color = _scriptChildColorCtrl._color;
	}
	else
	{
		// add
		StringColorAssoc sc;
		_scriptChildCtrl.GetWindowText (sc._name);
		sc._color = _scriptChildColorCtrl._color;
		_scriptChildColors.Add (sc);
		_scriptChildList.SetCurSel (_scriptChildList.AddString (sc._name));
	}
}

void CPreferencesDlg::OnRemoveSChild() 
{
	// TODO: Add your control notification handler code here
	
	int i = _scriptChildList.GetCurSel ();
	if (i != LB_ERR)
	{
		_scriptChildColors.RemoveAt (i);
		_scriptChildList.DeleteString(i);
	}
}

void CPreferencesDlg::OnSChildColor() 
{
	// TODO: Add your control notification handler code here

	COLORREF c = _scriptChildColorCtrl._color;
	if (OpenColorDialog(c))
	{
		_scriptChildColorCtrl.SetColor(c);
	}
}

bool CPreferencesDlg::OpenColorDialog(COLORREF &color)
{
	CColorDialog d (color, 0, this);
	if (d.DoModal () == IDOK)
	{
		color = d.GetColor ();
		return true;
	}

	return false;
}

void CPreferencesDlg::OnSChildDefColor() 
{
	// TODO: Add your control notification handler code here
	
	if (OpenColorDialog(_scriptChildDefColor))
	{
		_scriptChildDefColorCtrl.SetColor(_scriptChildDefColor);
	}
}

void CPreferencesDlg::OnChildColor() 
{
	// TODO: Add your control notification handler code here
	
	int i = _childCombo.GetCurSel ();
	if (i != CB_ERR)
	{
		if (OpenColorDialog(_childColors[i]._color))
		{
			_childColorCtrl.SetColor(_childColors[i]._color);
		}
	}
}

void CPreferencesDlg::OnComponentColor() 
{
	// TODO: Add your control notification handler code here
	
	int i = _componentCombo.GetCurSel ();
	if (i != CB_ERR)
	{
		if (OpenColorDialog(_componentColors[i]._color))
		{
			_componentColorCtrl.SetColor(_componentColors[i]._color);
		}
	}
}
