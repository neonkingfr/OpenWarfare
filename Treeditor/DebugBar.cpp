// DebugBar.cpp: implementation of the CDebugBar class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <El/elementpch.hpp>
#include "RV/Tree/mytree.h"
#include "treeditor.h"
#include "DebugBar.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDebugBar::CDebugBar()
{

}

CDebugBar::~CDebugBar()
{

}

CSize CDebugBar::CalcDimension( int nLength, DWORD dwMode )
{
	// Return default if it is being docked or floated
	if ((dwMode & LM_VERTDOCK) || (dwMode & LM_HORZDOCK))
	{
		if (dwMode & LM_STRETCH) // if not docked stretch to fit
			return CSize((dwMode & LM_HORZ) ? 32767 : m_sizeDocked.cx,
			(dwMode & LM_HORZ) ? m_sizeDocked.cy : 32767);
		else
			return m_sizeDocked;
	}
	if (dwMode & LM_MRUWIDTH)
		return m_sizeFloating;
	// In all other cases, accept the dynamic length
	if (dwMode & LM_LENGTHY)
	{
		//nLength += (FLOAT_BAR_VER_ALIGN - 1);
		//nLength -= nLength % FLOAT_BAR_VER_ALIGN;
		return CSize(m_sizeFloating.cx, (m_bChangeDockedSize) ?
		m_sizeFloating.cy = m_sizeDocked.cy = nLength :
		m_sizeFloating.cy = nLength);
	}
	else
	{
		//nLength += (FLOAT_BAR_HOR_ALIGN - 1);
		//nLength -= nLength % FLOAT_BAR_HOR_ALIGN;
		return CSize((m_bChangeDockedSize) ?
		m_sizeFloating.cx = m_sizeDocked.cx = nLength :
		m_sizeFloating.cx = nLength, m_sizeFloating.cy);
	}
}

CSize CDebugBar::CalcDynamicLayout(int nLength, DWORD dwMode)
{
	CSize dim = CalcDimension(nLength, dwMode);

#define CALC_CTRL_RECT(id, ctrl, r) CWnd *ctrl = GetDlgItem (id); if (ctrl == NULL) { return dim; } CRect r; ctrl->GetWindowRect(&r); ScreenToClient (&r)

	CALC_CTRL_RECT(IDC_DEBUG_OUTPUT, debugOutput, debugOutputR);

#undef CALC_CTRL_RECT

#define SPACE1 4
#define SPACE2 7

	debugOutputR.SetRect (SPACE2, SPACE2, dim.cx - SPACE2, dim.cy - SPACE2);

#undef SPACE1
#undef SPACE2

	debugOutput->MoveWindow (&debugOutputR, TRUE);

	return dim;
}


BOOL CDebugBar::Create( CWnd* pParentWnd, UINT nIDTemplate,
							  UINT nStyle, UINT nID, CSize *s, BOOL bChange)
{
	if(!CDialogBar::Create(pParentWnd,nIDTemplate,nStyle,nID))
		return FALSE;
	
	m_bChangeDockedSize = bChange;
	if (s != NULL)
	{
		m_sizeDefault = *s;
	}
	m_sizeFloating = m_sizeDocked = m_sizeDefault;

	CEdit *ei = (CEdit*) GetDlgItem (IDC_DEBUG_OUTPUT);
	if (ei == NULL)
	{
		return FALSE;
	}
	ei->SetLimitText (UINT_MAX);
	return TRUE;
}

BOOL CDebugBar::Create( CWnd* pParentWnd,
							  LPCTSTR lpszTemplateName, UINT nStyle,
							  UINT nID, BOOL bChange)
{
	if (!CDialogBar::Create( pParentWnd, lpszTemplateName,
		nStyle, nID))
		return FALSE;
	
	m_bChangeDockedSize = bChange;
	m_sizeFloating = m_sizeDocked = m_sizeDefault;

	CEdit *ei = (CEdit*) GetDlgItem (IDC_DEBUG_OUTPUT);
	if (ei == NULL)
	{
		return FALSE;
	}
	ei->SetLimitText (UINT_MAX);
	return TRUE;
}

void CDebugBar::LogF(const char *format, va_list argptr)
{
	CEdit *debOut = (CEdit*) GetDlgItem (IDC_DEBUG_OUTPUT);
	if (debOut != NULL)
	{
		char buf[1024];
		vsprintf(buf,format,argptr);
		strcat(buf,"\r\n");

		USES_CONVERSION;
		LPCTSTR msg = A2CT (buf);
		
		debOut->SetSel (INT_MAX, INT_MAX, TRUE);
		debOut->ReplaceSel (msg);
	}
}
