#ifndef _primitivestream_h_
#define _primitivestream_h_

#include <d3d8.h>
#include <array.hpp>

#define PRIMITIVESTAGE_COUNT 64
#define VB_SIZE 65536
#define IB_SIZE 65536

//! Set of primitives represented as a incices to vertex buffer.
/*!
  Type of index is set to D3DFMT_INDEX16. Type of primitive is D3DPT_TRIANGLELIST.
*/
class CPrimitiveStage {
private:
  //! List of triangles associated with this stage. All of them will be drawn as a single drawindexedprimitive.
  AutoArray<WORD> _Indices;
  //! Minimum index of a vertex associated with this stage.
  UINT _MinVertexIndex;
  //! Maximum index of a vertex associated with this stage.
  UINT _MaxVertexIndex;
public:
  //! Texture on stage 0.
  IDirect3DTexture8 *_pTextureS0;
  //! Texture on stage 1.
  IDirect3DTexture8 *_pTextureS1;
  //! Constructor
  CPrimitiveStage();
  //! Clears this stage.
  void Clear();
  //! Add specified index. Array will be resized if nacessary.
  void AddIndex(const WORD &Index);
  //! Returns number of indices in the array.
  int Size();
  //! Returns pointer to stage data.
  const WORD *Data();
  //! Returns minimal vertex index associated with this stage.
  UINT GetMinVertexIndex();
  //! Returns size of the vertex block associated with this stage.
  UINT GetNumVertices();
};

//! Stream of primitives to draw.
/*!
  Type of vertex is specified by _VertexFVF and its size by _VertexSize.
*/
class CPrimitiveStream : public RefCount {
private:
  //! 
  ComRef<IDirect3DDevice8> _pD3DDevice;
  ComRef<IDirect3DVertexBuffer8> _pVB;
  ComRef<IDirect3DIndexBuffer8> _pIB;
  //! List of primitive stages.
  CPrimitiveStage _PS[PRIMITIVESTAGE_COUNT];
  //! Array of vertices.
  AutoArray<BYTE> _Vertices;
  //! Size of one vertex in bytes.
  int _VertexSize;
  //! Either handle for the vertex shader or FVF code.
  DWORD _VertexHandle;
  //! Handle for the pixel shader.
  DWORD _PixelHandle;
  //! Positions of stages in index buffer. This is set by Prepare() method.
  UINT _StageIndexPos[PRIMITIVESTAGE_COUNT];
public:
  //! Constructor.
  CPrimitiveStream(IDirect3DDevice8 *pD3DDevice);
  //! Initialization.
  /*!
    \param VertexSize Size of single vertex.
    \param VertexHandle Either handle for the vertex shader or FVF code.
    \param PixelHandle Handle for the pixel shader.
    \param FVF FVF code which should correspond to VertexHandle.
  */
  void Init(int VertexSize, DWORD VertexHandle, DWORD PixelHandle, DWORD FVF);
  //! Clears all vertices and indices.
  void Clear();
  //! Add single vertex to vertex buffer.
  /*!
    \param pData Pointer to vertex data.
    \return Index of new vertex in vertex buffer.
  */
  int AddVertex(void *pData);
  //! Add single index to index buffer at specified stage.
  /*!
    \param Stage Index of stage to add index to.
    \param Index Index to add.
  */
  void AddIndex(int Stage, WORD &Index);
  //! Gets index of the new vertex in the vertex buffer.
  int GetNewVertexIndex();
  //! Registers textures on specified stage.
  void RegisterTextures(int Stage, IDirect3DTexture8 *pTextureS0, IDirect3DTexture8 *pTextureS1);
  //! Prepares vertex and index buffers.
  /*!
    Besides, this method set up the _StageIndexPos array which will be used
    in Draw method.
  */
  void Prepare();
  //! Draws primitives on the surface.
  void Draw(BOOL SetTextures = TRUE);
};

#endif