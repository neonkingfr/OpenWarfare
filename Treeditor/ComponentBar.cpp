// ComponentBar.cpp: implementation of the CComponentBar class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <El/elementpch.hpp>
#include "RV/Tree/mytree.h"
#include "treeditor.h"
#include "ComponentBar.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CComponentBar::CComponentBar()
{

}

CComponentBar::~CComponentBar()
{

}

CSize CComponentBar::CalcDimension( int nLength, DWORD dwMode )
{
	// Return default if it is being docked or floated
	if ((dwMode & LM_VERTDOCK) || (dwMode & LM_HORZDOCK))
	{
		if (dwMode & LM_STRETCH) // if not docked stretch to fit
			return CSize((dwMode & LM_HORZ) ? 32767 : m_sizeDocked.cx,
			(dwMode & LM_HORZ) ? m_sizeDocked.cy : 32767);
		else
			return m_sizeDocked;
	}
	if (dwMode & LM_MRUWIDTH)
		return m_sizeFloating;
	// In all other cases, accept the dynamic length
	if (dwMode & LM_LENGTHY)
	{
		//nLength += (FLOAT_BAR_VER_ALIGN - 1);
		//nLength -= nLength % FLOAT_BAR_VER_ALIGN;
		return CSize(m_sizeFloating.cx, (m_bChangeDockedSize) ?
		m_sizeFloating.cy = m_sizeDocked.cy = nLength :
		m_sizeFloating.cy = nLength);
	}
	else
	{
		//nLength += (FLOAT_BAR_HOR_ALIGN - 1);
		//nLength -= nLength % FLOAT_BAR_HOR_ALIGN;
		return CSize((m_bChangeDockedSize) ?
		m_sizeFloating.cx = m_sizeDocked.cx = nLength :
		m_sizeFloating.cx = nLength, m_sizeFloating.cy);
	}
}

CSize CComponentBar::CalcDynamicLayout(int nLength, DWORD dwMode)
{
	CSize dim = CalcDimension(nLength, dwMode);

#define CALC_CTRL_RECT(id, ctrl, r) CWnd *ctrl = GetDlgItem (id); if (ctrl == NULL) { return dim; } CRect r; ctrl->GetWindowRect(&r); ScreenToClient (&r)

	CALC_CTRL_RECT(IDC_CLASSES, classes, classesR);
	CALC_CTRL_RECT(IDC_DETAILLABEL, detailL, detailLR);
	CALC_CTRL_RECT(IDC_DETAIL, detail, detailR);
	CALC_CTRL_RECT(IDC_DETAILSLIDER, detailS, detailSR);
	CALC_CTRL_RECT(IDC_NEARLABEL, nearL, nearLR);
	CALC_CTRL_RECT(IDC_NEAR, nearE, nearR);
	CALC_CTRL_RECT(IDC_NEARSLIDER, nearS, nearSR);
	CALC_CTRL_RECT(IDC_FARLABEL, farL, farLR);
	CALC_CTRL_RECT(IDC_FAR, farE, farR);
	CALC_CTRL_RECT(IDC_FARSLIDER, farS, farSR);
	CALC_CTRL_RECT(IDC_TREESLABEL, treesL, treesLR);
	CALC_CTRL_RECT(IDC_TREES, trees, treesR);
	CALC_CTRL_RECT(IDC_TREESSLIDER, treesS, treesSR);
	CALC_CTRL_RECT(IDC_DISTLABEL, distL, distLR);
	CALC_CTRL_RECT(IDC_DIST, dist, distR);
	CALC_CTRL_RECT(IDC_DISTSLIDER, distS, distSR);
	CALC_CTRL_RECT(IDC_METERLABEL, meterL, meterLR);
	CALC_CTRL_RECT(IDC_METER, meter, meterR);
	CALC_CTRL_RECT(IDC_METERSLIDER, meterS, meterSR);
	CALC_CTRL_RECT(IDC_AGELABEL, ageL, ageLR);
	CALC_CTRL_RECT(IDC_AGE, age, ageR);
	CALC_CTRL_RECT(IDC_AGESLIDER, ageS, ageSR);
	CALC_CTRL_RECT(IDC_WINDLABEL, windL, windLR);
	CALC_CTRL_RECT(IDC_WIND, wind, windR);
	CALC_CTRL_RECT(IDC_WINDSLIDER, windS, windSR);
	CALC_CTRL_RECT(IDC_SEEDLABEL, seedL, seedLR);
	CALC_CTRL_RECT(IDC_SEED, seed, seedR);
	CALC_CTRL_RECT(IDC_SEEDSPIN, seedS, seedSR);
	CALC_CTRL_RECT(IDC_WROTYLABEL, wrotyL, wrotyLR);
	CALC_CTRL_RECT(IDC_WROTYSLIDER, wrotyS, wrotySR);
	CALC_CTRL_RECT(IDC_WROTZLABEL, wrotzL, wrotzLR);
	CALC_CTRL_RECT(IDC_WROTZSLIDER, wrotzS, wrotzSR);
	CALC_CTRL_RECT(IDC_LROTYLABEL, lrotyL, lrotyLR);
	CALC_CTRL_RECT(IDC_LROTYSLIDER, lrotyS, lrotySR);
	CALC_CTRL_RECT(IDC_LROTZLABEL, lrotzL, lrotzLR);
	CALC_CTRL_RECT(IDC_LROTZSLIDER, lrotzS, lrotzSR);

#undef CALC_CTRL_RECT

#define SPACE1 4
#define SPACE2 7

	int t = dim.cy - (
		SPACE2
		+ SPACE2
		+ detailR.bottom - detailR.top 
		+ SPACE2
		+ nearR.bottom - nearR.top
		+ SPACE2
		+ farR.bottom - farR.top
		+ SPACE2
		+ treesR.bottom - treesR.top
		+ SPACE2
		+ distR.bottom - distR.top
		+ SPACE2
		+ meterR.bottom - meterR.top
		+ SPACE2
		+ ageR.bottom - ageR.top
		+ SPACE2
		+ windR.bottom - windR.top
		+ SPACE2
		+ seedR.bottom - seedR.top
		+ SPACE2
		+ wrotySR.bottom - wrotySR.top
		+ SPACE2
		+ wrotzSR.bottom - wrotzSR.top
		+ SPACE2
		+ lrotySR.bottom - lrotySR.top
		+ SPACE2
		+ lrotzSR.bottom - lrotzSR.top
		+ SPACE2);

	classesR.SetRect (SPACE2, SPACE2, dim.cx - SPACE2, SPACE2 + t);

	int center, ty;

#define COMBO_CALC_3_3(basetop, label, edit, slider)    \
	center = (edit.bottom - edit.top) >> 1;\
	ty = (basetop) + center - ((label.bottom - label.top) >> 1);\
	label.SetRect (SPACE2, ty, SPACE2 + label.right - label.left, ty + label.bottom - label.top);\
	edit.SetRect (label.right + SPACE1, (basetop), label.right + SPACE1 + edit.right - edit.left, (basetop) + edit.bottom - edit.top);\
	ty = (basetop) + center - ((slider.bottom - slider.top) >> 1);\
	slider.SetRect (edit.right + SPACE1, ty, dim.cx - SPACE2, ty + slider.bottom - slider.top);

#define COMBO_CALC_3_2(basetop, label, edit, spin)    \
	center = (edit.bottom - edit.top) >> 1;\
	ty = (basetop) + center - ((label.bottom - label.top) >> 1);\
	label.SetRect (SPACE2, ty, SPACE2 + label.right - label.left, ty + label.bottom - label.top);\
	ty = (basetop) + center - ((spin.bottom - spin.top) >> 1);\
	spin.SetRect (dim.cx - SPACE2 - (spin.right - spin.left), ty, dim.cx - SPACE2, ty + spin.bottom - spin.top);\
	edit.SetRect (label.right + SPACE1, (basetop), spin.left - SPACE1, (basetop) + edit.bottom - edit.top);

#define COMBO_CALC_2_2(basetop, label, slider)    \
	center = (slider.bottom - slider.top) >> 1;\
	ty = (basetop) + center - ((label.bottom - label.top) >> 1);\
	label.SetRect (SPACE2, ty, SPACE2 + label.right - label.left, ty + label.bottom - label.top);\
	slider.SetRect (label.right + SPACE1, (basetop), dim.cx - SPACE2, (basetop) + slider.bottom - slider.top);

	COMBO_CALC_3_3 (classesR.bottom + SPACE2, detailLR, detailR, detailSR);
	COMBO_CALC_3_3 (detailR.bottom + SPACE2, nearLR, nearR, nearSR);
	COMBO_CALC_3_3 (nearR.bottom + SPACE2, farLR, farR, farSR);
	COMBO_CALC_3_3 (farR.bottom + SPACE2, treesLR, treesR, treesSR);
	COMBO_CALC_3_3 (treesR.bottom + SPACE2, distLR, distR, distSR);
	COMBO_CALC_3_3 (distR.bottom + SPACE2, meterLR, meterR, meterSR);
	COMBO_CALC_3_3 (meterR.bottom + SPACE2, ageLR, ageR, ageSR);
	COMBO_CALC_3_3 (ageR.bottom + SPACE2, windLR, windR, windSR);

	COMBO_CALC_3_2 (windR.bottom + SPACE2, seedLR, seedR, seedSR);
	
	COMBO_CALC_2_2 (seedR.bottom + SPACE2, wrotyLR, wrotySR);
	COMBO_CALC_2_2 (wrotySR.bottom + SPACE2, wrotzLR, wrotzSR);
	COMBO_CALC_2_2 (wrotzSR.bottom + SPACE2, lrotyLR, lrotySR);
	COMBO_CALC_2_2 (lrotySR.bottom + SPACE2, lrotzLR, lrotzSR);

#undef COMBO_CALC_3_3
#undef COMBO_CALC_3_2
#undef COMBO_CALC_2_2
#undef SPACE1
#undef SPACE2

	classes->MoveWindow (&classesR, TRUE);
	detailL->MoveWindow (&detailLR, TRUE);
	detail->MoveWindow (&detailR, TRUE);
	detailS->MoveWindow (&detailSR, TRUE);
	nearL->MoveWindow (&nearLR, TRUE);
	nearE->MoveWindow (&nearR, TRUE);
	nearS->MoveWindow (&nearSR, TRUE);
	farL->MoveWindow (&farLR, TRUE);
	farE->MoveWindow (&farR, TRUE);
	farS->MoveWindow (&farSR, TRUE);
	treesL->MoveWindow (&treesLR, TRUE);
	trees->MoveWindow (&treesR, TRUE);
	treesS->MoveWindow (&treesSR, TRUE);
	distL->MoveWindow (&distLR, TRUE);
	dist->MoveWindow (&distR, TRUE);
	distS->MoveWindow (&distSR, TRUE);
	meterL->MoveWindow (&meterLR, TRUE);
	meter->MoveWindow (&meterR, TRUE);
	meterS->MoveWindow (&meterSR, TRUE);
	ageL->MoveWindow (&ageLR, TRUE);
	age->MoveWindow (&ageR, TRUE);
	ageS->MoveWindow (&ageSR, TRUE);
	windL->MoveWindow (&windLR, TRUE);
	wind->MoveWindow (&windR, TRUE);
	windS->MoveWindow (&windSR, TRUE);
	seedL->MoveWindow (&seedLR, TRUE);
	seed->MoveWindow (&seedR, TRUE);
	seedS->MoveWindow (&seedSR, TRUE);
	wrotyL->MoveWindow (&wrotyLR, TRUE);
	wrotyS->MoveWindow (&wrotySR, TRUE);
	wrotzL->MoveWindow (&wrotzLR, TRUE);
	wrotzS->MoveWindow (&wrotzSR, TRUE);
	lrotyL->MoveWindow (&lrotyLR, TRUE);
	lrotyS->MoveWindow (&lrotySR, TRUE);
	lrotzL->MoveWindow (&lrotzLR, TRUE);
	lrotzS->MoveWindow (&lrotzSR, TRUE);

	return dim;
}


BOOL CComponentBar::Create( CWnd* pParentWnd, UINT nIDTemplate,
							  UINT nStyle, UINT nID, CSize *s, BOOL bChange)
{
	if(!CDialogBar::Create(pParentWnd,nIDTemplate,nStyle,nID))
		return FALSE;
	
	m_bChangeDockedSize = bChange;
	if (s != NULL)
	{
		m_sizeDefault = *s;
	}
	m_sizeFloating = m_sizeDocked = m_sizeDefault;
	return TRUE;
}

BOOL CComponentBar::Create( CWnd* pParentWnd,
							  LPCTSTR lpszTemplateName, UINT nStyle,
							  UINT nID, BOOL bChange)
{
	if (!CDialogBar::Create( pParentWnd, lpszTemplateName,
		nStyle, nID))
		return FALSE;
	
	m_bChangeDockedSize = bChange;
	m_sizeFloating = m_sizeDocked = m_sizeDefault;
	return TRUE;
}

BOOL CComponentBar::OnWndMsg( UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult )
{
	switch (message)
	{
	case WM_HSCROLL:
		GetOwner()->SendMessage(message, wParam, lParam);
		break;
	}

	return CDialogBar::OnWndMsg(message, wParam, lParam, pResult);
}
