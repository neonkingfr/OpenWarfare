// TreeditorClass.cpp: implementation of the CTreeditorClass class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <El/elementpch.hpp>
#include "RV/Tree/mytree.h"
#include "Treeditor.h"
#include "TreeditorLex.h"
#include "TreeditorClass.h"
#include "ScriptParser.h"
#include "UiPropertiesCtrl.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

const STreeditorParamEnumDesc CTreeditorParam::enumParams[] =
{
	{4, {"kind4", "kind3", "kind2", "kind1"}, {"4", "3", "2", "1"}},
	{2, {"Block", "Bunch"}, {"0", "1"}},
	{2, {"false", "true"}, {"0", "1"}},
	{2, {"false", "true"}, {"false", "true"}},
	{-1, {NULL, }, {0, }},
};

const STreeditorParamDesc CTreeditorParam::classParams[treeditorClassParam_numberOfTypes] =
{
	{/*treeditorClassParam_size,*/                            "size",                           "size",                           treeditorLexFixedFloat, "0.2",                   ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_sizeInterval,*/                    "sizeInterval",                   "sizeInterval",                   treeditorLexFixedFloat, "0.2",                   ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_sizeCoef,*/                        "sizeCoef",                       "sizeCoef",                       treeditorLexFixedFloat, "0.5",                   ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_script,*/                          "script",                         "script",                         treeditorLexString,     "\"script.txt\"",        ISNT_CHILD_CLASS_NAME, USE_OPENFILE_DLG,               0.0f,    0.0f,   0.0f,  NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_EDIT_BUTTON | USE_PROPERTY_SELECT_BUTTON | USE_PROPERTY_VALUE_EDIT,         true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_textureName,*/                     "textureName",                    "textureName",                    treeditorLexString,     "\"texture.bmp\"",       ISNT_CHILD_CLASS_NAME, USE_OPENFILE_DLG,               0.0f,    0.0f,   0.0f,  NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_EDIT_BUTTON | USE_PROPERTY_SELECT_BUTTON | USE_PROPERTY_VALUE_EDIT,         true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_textureSaveName,*/                 "textureSaveName",                "textureSaveName",                treeditorLexString,     "\"texture.paa\"",       ISNT_CHILD_CLASS_NAME, USE_OPENFILE_DLG,               0.0f,    0.0f,   0.0f,  NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_EDIT_BUTTON | USE_PROPERTY_SELECT_BUTTON | USE_PROPERTY_VALUE_EDIT,         true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_textureIndex,*/                    "textureIndex",                   "textureIndex",                   treeditorLexLong,       "0",                     ISNT_CHILD_CLASS_NAME, DONT_USE_ANYTHING,              0.0f,   20.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_textureTilingCircumferenceCoef,*/  "textureTilingCircumferenceCoef", "textureTilingCircumferenceCoef", treeditorLexFixedFloat, "1",                     ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    10.0f,  0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_textureTilingLengthCoef,*/         "textureTilingLengthCoef",        "textureTilingLengthCoef",        treeditorLexFixedFloat, "1",                     ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    10.0f,  0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_balance,*/                         "balance",                        "balance",                        treeditorLexFixedFloat, "0.5",                   ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_balanceInterval,*/                 "balanceInterval",                "balanceInterval",                treeditorLexFixedFloat, "0",                     ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_twist,*/                           "twist",                          "twist",                          treeditorLexFixedFloat, "0",                     ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    100.0f, 0.5f,  NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_twistInterval,*/                   "twistInterval",                  "twistInterval",                  treeditorLexFixedFloat, "0",                     ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    100.0f, 0.5f,  NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_spread,*/                          "spread",                         "spread",                         treeditorLexFixedFloat, "0.7",                   ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_spreadInterval,*/                  "spreadInterval",                 "spreadInterval",                 treeditorLexFixedFloat, "0",                     ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_spreadRatio,*/                     "spreadRatio",                    "spreadRatio",                    treeditorLexFixedFloat, "0",                     ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_width,*/                           "width",                          "width",                          treeditorLexFixedFloat, "0.5",                   ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_height,*/                          "height",                         "height",                         treeditorLexFixedFloat, "0.5",                   ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_arrowWidth,*/                      "arrowWidth",                     "arrowWidth",                     treeditorLexFixedFloat, "0.5",                   ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_arrowHeight,*/                     "arrowHeight",                    "arrowHeight",                    treeditorLexFixedFloat, "0.5",                   ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_heightInterval,*/                  "heightInterval",                 "heightInterval",                 treeditorLexFixedFloat, "0",                     ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_heightCoef,*/                      "heightCoef",                     "heightCoef",                     treeditorLexFixedFloat, "0.5",                   ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_depth,*/                           "depth",                          "depth",                          treeditorLexFixedFloat, "0.1",                   ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_childNumber,*/                     "childNumber",                    "childNumber",                    treeditorLexFixedFloat, "0",                     ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    2.0f,   0.04f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_childNumberInterval,*/             "childNumberInterval",            "childNumberInterval",            treeditorLexFixedFloat, "0",                     ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    4.0f,   0.08f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_childAgeCoef,*/                    "childAgeCoef",                   "childAgeCoef",                   treeditorLexFixedFloat, "0",                     ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_childAgeCoefInterval,*/            "childAgeCoefInterval",           "childAgeCoefInterval",           treeditorLexFixedFloat, "0",                     ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_childSlopeCoef,*/                  "childSlopeCoef",                 "childSlopeCoef",                 treeditorLexFixedFloat, "0",                     ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_childMap,*/                        "childMap",                       "childMap",                       treeditorLexLong,       "0",                     ISNT_CHILD_CLASS_NAME, DONT_USE_ANYTHING,              0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_sideChildNumber,*/                 "sideChildNumber",                "sideChildNumber",                treeditorLexFixedFloat, "0",                     ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    2.0f,   0.04f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_sideChildNumberInterval,*/         "sideChildNumberInterval",        "sideChildNumberInterval",        treeditorLexFixedFloat, "0",                     ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    4.0f,   0.08f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_sideChildAngle,*/                  "sideChildAngle",                 "sideChildAngle",                 treeditorLexFixedFloat, "0",                     ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_sideChildAngleInterval,*/          "sideChildAngleInterval",         "sideChildAngleInterval",         treeditorLexFixedFloat, "0",                     ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_sideChildTwist,*/                  "sideChildTwist",                 "sideChildTwist",                 treeditorLexFixedFloat, "0",                     ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    100.0f, 0.5f,  NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_sideChildAgeCoef,*/                "sideChildAgeCoef",               "sideChildAgeCoef",               treeditorLexFixedFloat, "0",                     ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_sideChildAgeCoefInterval,*/        "sideChildAgeCoefInterval",       "sideChildAgeCoefInterval",       treeditorLexFixedFloat, "0",                     ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_photoCoef,*/                       "photoCoef",                      "photoCoef",                      treeditorLexFixedFloat, "0.0",                   ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_diaCoef,*/                         "diaCoef",                        "diaCoef",                        treeditorLexFixedFloat, "0.0",                   ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_graviCoef,*/                       "graviCoef",                      "graviCoef",                      treeditorLexFixedFloat, "0.0",                   ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_geoCoef,*/                         "geoCoef",                        "geoCoef",                        treeditorLexFixedFloat, "0.0",                   ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_polyplaneType,*/                   "polyplaneType",                  "polyplaneType",                  treeditorLexId,         "0",                     ISNT_CHILD_CLASS_NAME, DONT_USE_ANYTHING,              0.0f,    1.0f,   0.02f, ENUM_TYPE(1), HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_ENUM_COMBO,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_polyplaneTreshold,*/               "polyplaneTreshold",              "polyplaneTreshold",              treeditorLexFixedFloat, "0.1",                   ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_ageCoef,*/                         "ageCoef",                        "ageCoef",                        treeditorLexFixedFloat, "0.1",                   ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_windCoef,*/                        "windCoef",                       "windCoef",                       treeditorLexFixedFloat, "0.05",                  ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_randomBendRange,*/                 "randomBendRange",                "randomBendRange",                treeditorLexFixedFloat, "0.0",                   ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_spin,*/                            "spin",                           "spin",                           treeditorLexFixedFloat, "0.0",                   ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_spinInterval,*/                    "spinInterval",                   "spinInterval",                   treeditorLexFixedFloat, "0.0",                   ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_child,*/                           "child",                          "child",                          treeditorLexId,         "",                      IS_CHILD_CLASS_NAME,   DONT_USE_ANYTHING,              0.0f,    0.0f,   0.0f,  NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_CHILD_COMBO,                                                                true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_frontChild,*/                      "frontChild",                     "frontChild",                     treeditorLexId,         "",                      IS_CHILD_CLASS_NAME,   DONT_USE_ANYTHING,              0.0f,    0.0f,   0.0f,  NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_CHILD_COMBO,                                                                true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_childA,*/                          "childA",                         "childA",                         treeditorLexId,         "",                      IS_CHILD_CLASS_NAME,   DONT_USE_ANYTHING,              0.0f,    0.0f,   0.0f,  NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_CHILD_COMBO,                                                                true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_childB,*/                          "childB",                         "childB",                         treeditorLexId,         "",                      IS_CHILD_CLASS_NAME,   DONT_USE_ANYTHING,              0.0f,    0.0f,   0.0f,  NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_CHILD_COMBO,                                                                true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_sideChild,*/                       "sideChild",                      "sideChild",                      treeditorLexId,         "",                      IS_CHILD_CLASS_NAME,   DONT_USE_ANYTHING,              0.0f,    0.0f,   0.0f,  NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_CHILD_COMBO,                                                                true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_leafChild,*/                       "leafChild",                      "leafChild",                      treeditorLexId,         "",                      IS_CHILD_CLASS_NAME,   DONT_USE_ANYTHING,              0.0f,    0.0f,   0.0f,  NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_CHILD_COMBO,                                                                true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_leafTreshold,*/                    "leafTreshold",                   "leafTreshold",                   treeditorLexFixedFloat, "0.0",                   ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_frontChildTreshold,*/              "frontChildTreshold",             "frontChildTreshold",             treeditorLexFixedFloat, "0.0",                   ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_leafTresholdA,*/                   "leafTresholdA",                  "leafTresholdA",                  treeditorLexFixedFloat, "0.1",                   ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_leafTresholdB,*/                   "leafTresholdB",                  "leafTresholdB",                  treeditorLexFixedFloat, "0.1",                   ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_frontLeafChild,*/                  "frontLeafChild",                 "frontLeafChild",                 treeditorLexId,         "",                      IS_CHILD_CLASS_NAME,   DONT_USE_ANYTHING,              0.0f,    0.0f,   0.0f,  NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_CHILD_COMBO,                                                                true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_frontLeafTreshold,*/               "frontLeafTreshold",              "frontLeafTreshold",              treeditorLexFixedFloat, "0.0",                   ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_sideLeafChild,*/                   "sideLeafChild",                  "sideLeafChild",                  treeditorLexId,         "",                      IS_CHILD_CLASS_NAME,   DONT_USE_ANYTHING,              0.0f,    0.0f,   0.0f,  NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_CHILD_COMBO,                                                                true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_sideLeafTreshold,*/                "sideLeafTreshold",               "sideLeafTreshold",               treeditorLexFixedFloat, "0.0",                   ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,    1.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_aSideChildAge,*/                   "aSideChildAge",                  "aSideChildAge",                  treeditorLexArray,      "1,1.5,1.5,2.0,1",       ISNT_CHILD_CLASS_NAME, USE_LINEAR_CURVE_DLG,           1.0f,    2.0f,   0.0f,  NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_EDIT_BUTTON,                                                                true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_aWidth,*/                          "aWidth",                         "aWidth",                         treeditorLexArray,      "1,1.5,1.5,2.0,1",       ISNT_CHILD_CLASS_NAME, USE_LINEAR_VAR_CURVE_DLG,       1.0f,    2.0f,   0.0f,  NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_EDIT_BUTTON,                                                                true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_segmentCount,*/                    "segmentCount",                   "segmentCount",                   treeditorLexLong,       "1",                     ISNT_CHILD_CLASS_NAME, USE_SLIDER,                     0.0f,   10.0f,   0.02f, NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_VALUE_EDIT,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_tempCrossKind,*/                   "tempCrossKind",                  "tempCrossKind",                  treeditorLexId,         "kind1",                 ISNT_CHILD_CLASS_NAME, DONT_USE_ANYTHING,              0.0f,    0.0f,   0.0f,  ENUM_TYPE(0), HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_ENUM_COMBO,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_tempColor,*/                       "tempColor",                      "tempColor",                      treeditorLexArray,      "0,0,0",                 ISNT_CHILD_CLASS_NAME, USE_COLOR_DLG,                  0.0f,    0.0f,   0.0f,  NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_EDIT_BUTTON,                                                                true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_tempLinearCurve,*/                 "tempLinearCurve",                "tempLinearCurve",                treeditorLexArray,      "1,1.5,1.5,2.0,1",       ISNT_CHILD_CLASS_NAME, USE_LINEAR_VAR_CURVE_DLG,       1.0f,    2.0f,   0.0f,  NO_ENUM_TYPE, HORIZONTAL_X, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_EDIT_BUTTON,                                                                true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_tempQuadraticCurve,*/              "tempQuadraticCurve",             "tempQuadraticCurve",             treeditorLexArray,      "0,1,1.5,1.5,2.0,1",     ISNT_CHILD_CLASS_NAME, USE_QUADRATIC_VAR_CURVE_DLG,    1.0f,    2.0f,   0.0f,  NO_ENUM_TYPE, HORIZONTAL_X, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_EDIT_BUTTON,                                                                true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_tempExQuadraticCurve,*/            "tempExQuadraticCurve",           "tempExQuadraticCurve",           treeditorLexArray,      "1,0.5,1.5,-0.5,1,0",    ISNT_CHILD_CLASS_NAME, USE_EX_QUADRATIC_VAR_CURVE_DLG, 1.0f,    2.0f,   0.0f,  NO_ENUM_TYPE, HORIZONTAL_X, "OSA X", "OSA Y", "&Value",     "&Interval",    USE_PROPERTY_EDIT_BUTTON,                                                                true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_tempLinearXCurve,*/                "tempLinearXCurve",               "tempLinearXCurve",               treeditorLexArray,      "0,1,0.5,1.5,1,1",       ISNT_CHILD_CLASS_NAME, USE_LINEAR_VAR_CURVE_WITH_X_DLG,1.0f,    2.0f,   0.0f,  NO_ENUM_TYPE, HORIZONTAL_X, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_EDIT_BUTTON,                                                                true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_tempQuadraticXCurve,*/             "tempQuadraticXCurve",            "tempQuadraticXCurve",            treeditorLexArray,      "0,0,1,0.75,1.5,1,1",    ISNT_CHILD_CLASS_NAME, USE_QUADRATIC_VAR_CURVE_WITH_X_DLG,1.0f, 2.0f,   0.0f,  NO_ENUM_TYPE, HORIZONTAL_X, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_EDIT_BUTTON,                                                                true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_tempExQuadraticXCurve,*/           "tempExQuadraticXCurve",          "tempExQuadraticXCurve",          treeditorLexArray,      "0,1,0.5,1,1.5,-0.5",    ISNT_CHILD_CLASS_NAME, USE_EX_QUADRATIC_VAR_CURVE_WITH_X_DLG, 1.0f,2.0f,0.0f,  NO_ENUM_TYPE, HORIZONTAL_X, "OSA X", "OSA Y", "&Value",     "&Interval",    USE_PROPERTY_EDIT_BUTTON,                                                                true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_tempBool,*/                        "tempBool",                       "tempBool",                       treeditorLexId,       "\"\"",                    ISNT_CHILD_CLASS_NAME, DONT_USE_ANYTHING,              0.0f,    0.0f,   0.0f,  ENUM_TYPE(2), HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_ENUM_COMBO,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_tempScriptBool,*/                  "tempScriptBool",                 "tempScriptBool",                 treeditorLexId,       "\"\"",                    ISNT_CHILD_CLASS_NAME, DONT_USE_ANYTHING,              0.0f,    0.0f,   0.0f,  ENUM_TYPE(3), HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_ENUM_COMBO,                                                                 true, { RGB (224, 224, 224) }, },
	{/*treeditorClassParam_tempPair,*/                        "tempPair",                       "tempPair",                       treeditorLexArray,       "1,0.5",                ISNT_CHILD_CLASS_NAME, USE_PAIR_DLG,                   0.0f,    1.0f,   0.0f,  NO_ENUM_TYPE, HORIZONTAL_Y, "x",     "y",     "&Value",     "&Interval",    USE_PROPERTY_EDIT_BUTTON,                                                                true, { RGB (224, 224, 224) }, },
};

const STreeditorClassTypeDesc CTreeditorClass::classTypes[treeditorClass_numberOfTypes] =
{
	{
		/*treeditorClass_fern,*/ "fern",
		{
			treeditorClassParam_textureName,
			treeditorClassParam_textureSaveName,
			treeditorClassParam_textureIndex,
			treeditorClassParam_width,
			treeditorClassParam_aWidth,
			treeditorClassParam_height,
			treeditorClassParam_frontChild,
			treeditorClassParam_frontChildTreshold,
			treeditorClassParam_stop,
		},
	},
	{
		/*treeditorClass_test,*/ "test",
		{
			treeditorClassParam_textureName,
			treeditorClassParam_textureSaveName,
			treeditorClassParam_textureIndex,
			treeditorClassParam_arrowWidth,
			treeditorClassParam_arrowHeight,
			treeditorClassParam_child,
			treeditorClassParam_tempLinearCurve,
			treeditorClassParam_tempQuadraticCurve,
			treeditorClassParam_tempExQuadraticCurve,
			treeditorClassParam_tempLinearXCurve,
			treeditorClassParam_tempQuadraticXCurve,
			treeditorClassParam_tempExQuadraticXCurve,
			treeditorClassParam_script,
			treeditorClassParam_stop,
		},
	},
	{
		/*treeditorClass_scriptStem,*/ "scriptStem",
		{
			treeditorClassParam_textureName,
			treeditorClassParam_textureSaveName,
			treeditorClassParam_textureIndex,
			treeditorClassParam_script,
			treeditorClassParam_stop,
		},
	},
	{
		/*treeditorClass_scriptTree,*/ "scriptTree",
		{
			treeditorClassParam_textureName,
			treeditorClassParam_textureSaveName,
			treeditorClassParam_textureIndex,
			treeditorClassParam_script,
			treeditorClassParam_stop,
		},
	},
	{
		/*treeditorClass_scriptLeaf,*/ "scriptLeaf",
		{
			treeditorClassParam_textureName,
			treeditorClassParam_textureSaveName,
			treeditorClassParam_textureIndex,
			treeditorClassParam_script,
			treeditorClassParam_stop,
		},
	},
	{
		/*treeditorClass_scriptBunch,*/ "scriptBunch",
		{
			treeditorClassParam_textureName,
			treeditorClassParam_textureSaveName,
			treeditorClassParam_textureIndex,
			treeditorClassParam_script,
			treeditorClassParam_stop,
		},
	},
	{
		/*treeditorClass_scriptEmpty,*/ "scriptEmpty",
		{
			treeditorClassParam_textureName,
			treeditorClassParam_textureSaveName,
			treeditorClassParam_textureIndex,
			treeditorClassParam_script,
			treeditorClassParam_stop,
		},
	},
	{
		/*treeditorClass_test_of_params,*/ "test_of_params", 
		{
			treeditorClassParam_script,
			treeditorClassParam_size,
			treeditorClassParam_sizeInterval,
			treeditorClassParam_sizeCoef,
			treeditorClassParam_textureName,
			treeditorClassParam_textureSaveName,
			treeditorClassParam_balance,
			treeditorClassParam_balanceInterval,
			treeditorClassParam_twist,
			treeditorClassParam_twistInterval,
			treeditorClassParam_spread,
			treeditorClassParam_spreadInterval,
			treeditorClassParam_spreadRatio,
			treeditorClassParam_width,
			treeditorClassParam_height,
			treeditorClassParam_heightInterval,
			treeditorClassParam_heightCoef,
			treeditorClassParam_depth,
		    treeditorClassParam_childNumber,
		    treeditorClassParam_childNumberInterval,
		    treeditorClassParam_childAgeCoef,
		    treeditorClassParam_childAgeCoefInterval,
			treeditorClassParam_sideChildNumber,
			treeditorClassParam_sideChildNumberInterval,
			treeditorClassParam_sideChildAngle,
			treeditorClassParam_sideChildAngleInterval,
			treeditorClassParam_sideChildAgeCoef,
			treeditorClassParam_sideChildAgeCoefInterval,
			treeditorClassParam_photoCoef,
			treeditorClassParam_diaCoef,
			treeditorClassParam_graviCoef,
			treeditorClassParam_geoCoef,
			treeditorClassParam_polyplaneType,
			treeditorClassParam_polyplaneTreshold,
			treeditorClassParam_ageCoef,
			treeditorClassParam_windCoef,
			treeditorClassParam_randomBendRange,
			treeditorClassParam_spin,
			treeditorClassParam_spinInterval,
			treeditorClassParam_child,
			treeditorClassParam_sideChild,
			treeditorClassParam_leafChild,
			treeditorClassParam_leafTreshold,
		    treeditorClassParam_tempCrossKind,
		    treeditorClassParam_tempColor,
		    treeditorClassParam_tempLinearCurve,
		    treeditorClassParam_tempQuadraticCurve,
			treeditorClassParam_tempExQuadraticCurve,
			treeditorClassParam_tempBool,
			treeditorClassParam_tempScriptBool,
			treeditorClassParam_tempPair,
			treeditorClassParam_stop,
		},
	},
	{
		/*treeditorClass_test_of_params,*/ "test_of_params_2", 
		{
			treeditorClassParam_textureName,
			treeditorClassParam_textureSaveName,
			treeditorClassParam_tempExQuadraticCurve,
			treeditorClassParam_tempLinearCurve,
			treeditorClassParam_stop,
		},
	},
	{
		/*treeditorClass_unknown,*/ "",
		{
			treeditorClassParam_stop,
		},
	},
};

void CTreeditorClass::GetScriptParamsFromString(LPCTSTR str, 
												CArray<STreeditorParamDesc*, STreeditorParamDesc*> *scriptParams, 
												STreeditorParamGroupPalette *groupPalette)
{
	if (groupPalette != NULL)
	{
		CScriptParser sp (str, scriptParams);
		sp.GetGroupPalette (groupPalette, NULL, NULL);
	}

	if (scriptParams != NULL)
	{
		CScriptParser sp (str, scriptParams);
		STreeditorScriptParamDesc desc;
		desc.group.colorIndex = 0;
		while (sp.GetNextParam(&desc))
		{
			STreeditorParamDesc *d = new STreeditorParamDesc ();
			if (d != NULL)
			{
				STreeditorParamDesc::copy(*d, desc);
				scriptParams->Add (d);
			}
		}
	}
}
