// TreeditorDoc.cpp : implementation of the CTreeditorDoc class
//

#include "stdafx.h"
#include <El/elementpch.hpp>
#include "RV/Tree/mytree.h"
#include "Treeditor.h"
#include "TreeditorLex.h"
#include "ScriptParser.h"
#include "TreeditorClass.h"

#include "TreeditorDoc.h"
#include "GraphCtrl.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

int CTreeditorDoc::_classCounter = 1;

/////////////////////////////////////////////////////////////////////////////
// CTreeditorDoc

IMPLEMENT_DYNCREATE(CTreeditorDoc, CDocument)

BEGIN_MESSAGE_MAP(CTreeditorDoc, CDocument)
	//{{AFX_MSG_MAP(CTreeditorDoc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTreeditorDoc construction/destruction

CTreeditorDoc::CTreeditorDoc()
{
	// TODO: add one-time construction code here
	_currentStamp.Init (_classes, true);
	_lastStamp = _currentStamp;
}

CTreeditorDoc::~CTreeditorDoc()
{
	DeleteClasses ();
}

BOOL CTreeditorDoc::OnNewDocument()
{
	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	if (!InitClasses ())
	{
		return FALSE;
	}

	if (!CDocument::OnNewDocument())
		return FALSE;

	_currentStamp.Init (_classes, true);
	return TRUE;
}

bool CTreeditorDoc::InitClasses ()
{
	DeleteAllClasses ();

	return true;
}

void CTreeditorDoc::DeleteClasses ()
{
	DeleteAllClasses ();
}

void CTreeditorDoc::DeleteAllClasses ()
{
	int n = _classes.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		delete _classes[i];
	}
	_classes.RemoveAll ();
}

CTreeditorClass* CTreeditorDoc::GetClass (unsigned long index) const
{
	return _classes[index];
}

CTreeditorClass* CTreeditorDoc::AddNewClass (int idx)
{
	CString newClassName;
	newClassName.Format ("NewClass%d", _classCounter);
	while (ClassNameExists (newClassName))
	{
		newClassName.Format ("NewClass%d", ++_classCounter);
	}

	CTreeditorClass *cl = new CTreeditorClass (treeditorClass_scriptLeaf, newClassName);
	if (cl == NULL)
	{
		return NULL;
	}

	DeselectAllClasses ();
	if (idx >= 0)
	{
		_classes.InsertAt(idx, cl);
	}
	else
	{
		_classes.Add(cl);
	}
	return cl;
}

void CTreeditorDoc::DeselectAllClasses ()
{
	int n = _classes.GetSize();
	for (int i = 0; i < n; ++i)
	{
		_classes[i]->selected = false;
	}
}

void CTreeditorDoc::SetParamInSelection (unsigned long paramNum, const char *val)
{
	int n = _classes.GetSize();
	for (int i = 0; i < n; ++i)
	{
		CTreeditorClass *c = _classes[i];
		if (c->selected)
		{
			c->params[paramNum].SetValue (val);
		}
	}
}

void CTreeditorDoc::GetParamInSelection (unsigned long paramNum, CString &val)
{
	bool first = true;
	int n = _classes.GetSize();
	for (int i = 0; i < n; ++i)
	{
		CTreeditorClass *c = _classes[i];
		if (c->selected)
		{
			LPCTSTR tmp = c->params[paramNum].GetValue ();
			if (first)
			{
				val = tmp;
				first = false;
			}
			else if (_tcscmp (val, tmp) != 0)
			{
				val = _T ("");
				return;
			}
		}
	}
}

bool CTreeditorDoc::ClassNameExists (const char *name)
{
	int n = _classes.GetSize();
	for (int i = 0; i < n; ++i)
	{
		if (_classes[i]->name != name)
		{
			continue;
		}

		return true;
	}

	return false;
}

void CTreeditorDoc::DeleteSelectedClasses ()
{
	int n = _classes.GetSize();
	for (int src = 0; src < n; )
	{
		CTreeditorClass *c = _classes[src];
		if (c->selected)
		{
			_classes.RemoveAt(src);
			delete c;
			--n;
		}
		else
		{
			++src;
		}
	}
}

void CTreeditorDoc::SetTypeInSelection (int typeId)
{
	int n = _classes.GetSize();
	for (int i = 0; i < n; ++i)
	{
		CTreeditorClass *c = _classes[i];
		if (c->selected)
		{
			c->type = typeId;
		}
	}
}

bool CTreeditorDoc::AllClassesSaved ()
{
	int n = _classes.GetSize();
	for (int i = 0; i < n; ++i)
	{
		if (_classes[i]->saved)
		{
			continue;
		}

		return false;
	}

	return true;
}

CTreeditorClass* CTreeditorDoc::FindClass (const char *name)
{
	int n = _classes.GetSize();
	for (int i = 0; i < n; ++i)
	{
		CTreeditorClass *c = _classes[i];
		if (c->name.Compare (name) != 0)
		{
			continue;
		}

		return c;
	}

	return NULL;
}

bool CTreeditorDoc::RenameClass (unsigned long item, const char *name)
{
	if (!ClassNameExists (name))
	{
		_classes[item]->name = name;
		return true;
	}

	return false;
}

bool CTreeditorDoc::IsEmpty ()
{
	return _classes.GetSize() > 0;
}

CTreeditorClass* CTreeditorDoc::GetFirstClass (unsigned long *nextIndex)
{
	*nextIndex = 1;
	if (_classes.GetSize() > 0)
	{
		return _classes[0];
	}

	return NULL;
}

CTreeditorClass* CTreeditorDoc::GetNextClass (unsigned long *nextIndex)
{
	if (*nextIndex < _classes.GetSize())
	{
		return _classes[(*nextIndex)++];
	}

	return NULL;
}

CTreeditorClass* CTreeditorDoc::GetFirstSelectedClass (unsigned long *index)
{
	int n = _classes.GetSize();
	for (*index = 0; *index < n; ++(*index))
	{
		CTreeditorClass *c = _classes[*index];
		if (c->selected)
		{
			return c;
		}
	}
	return NULL;
}

CTreeditorClass* CTreeditorDoc::GetNextSelectedClass (unsigned long *index)
{
	int n = _classes.GetSize();
	for (++(*index); *index < n; ++(*index))
	{
		CTreeditorClass *c = _classes[*index];
		if (c->selected)
		{
			return c;
		}
	}
	return NULL;
}

CTreeditorClass* CTreeditorDoc::GetFirstChild (CTreeditorClass *parent, unsigned long *childIt, ETreeditorClassParam *childKind)
{
	const STreeditorClassTypeDesc *parentDesc = &CTreeditorClass::classTypes[parent->type];
	for (*childIt= 0; parentDesc->params[*childIt] != treeditorClassParam_stop; ++(*childIt))
	{
		ETreeditorClassParam paramKind = parentDesc->params[*childIt];
		const STreeditorParamDesc *paramDesc = &CTreeditorParam::classParams[paramKind];
		if (paramDesc->isChildClassName)
		{
			CTreeditorClass* childClass = FindClass (parent->params[paramKind]._value);
			if (childClass != NULL)
			{
				if (childKind != NULL)
				{
					*childKind = paramKind;
				}
				return childClass;
			}
		}
	}
	return NULL;
}

CTreeditorClass* CTreeditorDoc::GetNextChild (CTreeditorClass *parent, unsigned long *childIt, ETreeditorClassParam *childKind)
{
	const STreeditorClassTypeDesc *parentDesc = &CTreeditorClass::classTypes[parent->type];
	for (++(*childIt); parentDesc->params[*childIt] != treeditorClassParam_stop; ++(*childIt))
	{
		ETreeditorClassParam paramKind = parentDesc->params[*childIt];
		const STreeditorParamDesc *paramDesc = &CTreeditorParam::classParams[paramKind];
		if (paramDesc->isChildClassName)
		{
			CTreeditorClass* childClass = FindClass (parent->params[paramKind]._value);
			if (childClass != NULL)
			{
				if (childKind != NULL)
				{
					*childKind = paramKind;
				}
				return childClass;
			}
		}
	}
	return NULL;
}

void CTreeditorDoc::SelectClass (unsigned long item)
{
	_classes[item]->selected = true;
}

bool CTreeditorDoc::Generate (CString &desc, CString &errMsg)
{
	bool error = false;
	
	int n = _classes.GetSize();
	if (n < 1)
	{
		errMsg.Format ("The document is empty.");
		return true;
	}
	
	for (int i = 0; i < n; ++i)
	{
		_classes[i]->saved = false;
	}
	
#if 1 // tree component sorting is disabled
	for (int i = 0; i < n; ++i)
	{
		CTreeditorClass *c = _classes[i];
		ASSERT (c != NULL);

		if (!AllChildrenExist (c))
		{
			error = true;
			errMsg.Format ("Not all children of the class \"%s\" exist.", c->name);
		}
#else // tree component sorting is disabled
	while (!AllClassesSaved ())
	{
		CTreeditorClass *c = NULL;
		for (i = 0; i < n; ++i)
		{
			c = _classes[i];
			if (!c->saved)
			{
				if (!c->HasAnyChild ())
				{
					break;
				}
				else
				{
					if (c->HasAnyChildOfName (c->name) > 0)
					{
						error = true;
						errMsg.Format ("The class \"%s\" is referenced by itself.", c->name);
						break;
					}
					if (AreAllChildrenSaved (c, error, errMsg))
					{
						break;
					}
				}
			}
		}

		ASSERT (c != NULL);
		
		if (i == n)
		{
			error = true;
			errMsg = "There is a cycle in the tree definition.";
			for (i = 0; i < classesLength; ++i)
			{
				c = _classes[i];
				if (!c->saved)
				{
					break;
				}
			}
		}
#endif // tree component sorting is disabled
		
		desc += "class ";
		desc += c->name;
		desc += "\n{\n\ttype = ";
		desc += CTreeditorClass::classTypes[c->type].name;
		desc += ";\n";

		if (c->graphId >= 0)
		{
			SGraphItem gi;
			if (((CMainFrame*) AfxGetMainWnd())->_graphBar._graphCtrl.GetItem (c->graphId, &gi))
			{
				c->_ediGraphPosition = true;
				c->_ediGraphLeft = gi.rect.left;
				c->_ediGraphRight = gi.rect.right;
				c->_ediGraphBottom = gi.rect.bottom;
				c->_ediGraphTop = gi.rect.top;

				CString tstr;
				tstr.Format(_T ("%g"), gi.rect.left);
				desc += "\tediGraphLeft = ";
				desc += tstr;
				desc += ";\n";
					
				tstr.Format(_T ("%g"), gi.rect.right);
				desc += "\tediGraphRight = ";
				desc += tstr;
				desc += ";\n";

				tstr.Format(_T ("%g"), gi.rect.top);
				desc += "\tediGraphTop = ";
				desc += tstr;
				desc += ";\n";

				tstr.Format(_T ("%g"), gi.rect.bottom);
				desc += "\tediGraphBottom = ";
				desc += tstr;
				desc += ";\n";
			}
		}

		for (int ip = 0; ip < treeditorClassParam_numberOfTypes; ++ip)
		{
			if (c->HasParam (ip))
			{
				desc += "\t";
				c->params[ip].GenerateValue (desc);
				desc += "\n";
			}
		}
		desc += "};\n\n";
		c->saved = true;
	}

	return error;
}

bool CTreeditorDoc::ParseType (CTreeditorLex &lex, STreeditorTokenDesc &token, CString &errMsg)
{
	lex.GetNextToken (&token);
	if (token.type == treeditorLex_type)
	{
		lex.GetNextToken (&token);
		if (token.type == '=')
		{
			lex.GetNextToken (&token);
			if (token.type == treeditorLexId)
			{
				for (int i = 0; i < treeditorClass_numberOfTypes; ++i)
				{
					if (strcmp (&token.string[0], CTreeditorClass::classTypes[i].name) != 0)
					{
						continue;
					}
					
					_classes[_classes.GetUpperBound()]->type = i;
					
					lex.GetNextToken (&token);
					if (token.type == ';')
					{
						return false;
					}
					else
					{
						errMsg = "';' is missing behind class type.";
						return true;
					}
				}

				errMsg.Format ("Unknown class type \"%s\".", &token.string[0]);
				return true;
			}
			else
			{
				errMsg = "Class type identifier is missing.";
				return true;
			}
		}
		else
		{
			errMsg = "'=' is missing.";
			return true;
		}
	}
	else
	{
		errMsg = "Keyword 'type' is missing.";
		return true;
	}
}

bool CTreeditorDoc::Parse1Param (CTreeditorLex &lex, STreeditorTokenDesc &token, CString &errMsg)
{
	bool error = false;
	for (int i = 0; i < treeditorClassParam_numberOfTypes; ++i)
	{
		if (strcmp (&token.string[0], CTreeditorParam::classParams[i].id) != 0)
		{
			continue;
		}
		
		lex.GetNextToken (&token);
		if (token.type == '[')
		{
			lex.GetNextToken (&token);
			if (token.type == ']')
			{
				lex.GetNextToken (&token);
				if (token.type == '=')
				{
					lex.GetNextToken (&token);
					if (token.type == '{')
					{
						CString str;
						bool first = true;
						do {
							lex.GetNextToken (&token);
							if (token.type != '}')
							{
								if (!first)
								{
									str += _T (",");
								}
								
								switch (token.type)
								{
								case treeditorLexLong:
									{
										CString tmp;
										tmp.Format (_T ("%d"), token.number);
										str += tmp;
										first = false;
									}
									break;
									
								case treeditorLexFixedFloat:
									{
										CString tmp;
										tmp.Format (_T ("%g"), token.fnumber);
										str += tmp;
										first = false;
									}
									break;
									
								default:
									errMsg = "'}' is missing.";
									error = true;
									token.type = '}';
									break;
								}
								
								if (token.type != '}')
								{
									lex.GetNextToken (&token);
									if (token.type != ',' && token.type != '}')
									{
										errMsg = "'}' is missing.";
										error = true;
										token.type = '}';
										break;
									}
								}
							}
						} while (token.type != '}');
						
						_classes[_classes.GetUpperBound()]->params[i].DegenerateValue (str);
						
						lex.GetNextToken (&token);
						
						if (token.type != ';')
						{
							errMsg = "';' is missing.";
							error = true;
						}
					}
					else
					{
						errMsg = "'{' is missing.";
						error = true;
					}
				}
				else
				{
					errMsg = "'=' is missing.";
					error = true;
				}
			}
			else
			{
				errMsg = "']' is missing.";
				error = true;
			}
		}
		else if (token.type == '=')
		{
			lex.GetNextToken (&token);
			switch (token.type)
			{
			case treeditorLexLong:
				_classes[_classes.GetUpperBound()]->params[i].DegenerateValue (token.number);
				break;
				
			case treeditorLexFixedFloat:
				_classes[_classes.GetUpperBound()]->params[i].DegenerateValue (token.fnumber);
				break;
				
			case treeditorLexString:
				{
					CString str;
					str.Format ("\"%s\"", token.string);
					_classes[_classes.GetUpperBound()]->params[i].DegenerateValue (str);
				}
				break;
				
			case treeditorLexId:
				_classes[_classes.GetUpperBound()]->params[i].DegenerateValue (token.string);
				break;
				
			case ';':
				break;
				
			default:
				errMsg = "Bad parameter value.";
				error = true;
				token.type = ';';
			}
			
			if (token.type != ';')
			{
				lex.GetNextToken (&token);
				if (token.type != ';')
				{
					errMsg = "';' is missing.";
					error = true;
				}
			}
		}
		else
		{
			errMsg = "'=' is missing.";
			error = true;
		}

		return error;
	}
	
	errMsg.Format ("Unknown parameter type identifier '%s'.", &token.string[0]);
	return true;
}

bool CTreeditorDoc::ParseParams (CTreeditorLex &lex, STreeditorTokenDesc &token, CString &errMsg)
{
	bool error = false;

	if (!ParseType (lex, token, errMsg))
	{
		lex.GetNextToken (&token);

		ParseGraphPosition (lex, token, errMsg);

		while (token.type == treeditorLexId)
		{
			if (Parse1Param (lex, token, errMsg))
			{
				error = true;
			}

			lex.GetNextToken (&token);
		}

		return error;
	}
	else
	{
		return true;
	}
}

bool CTreeditorDoc::ParseGraphPosition (CTreeditorLex &lex, STreeditorTokenDesc &token, CString &errMsg)
{
	CTreeditorClass *c = _classes[_classes.GetUpperBound()];
	c->_ediGraphPosition = false;

	// Left.
	if (token.type != treeditorLex_ediGraphLeft)
	{
		return false;
	}
	lex.GetNextToken(&token);

	if (token.type != '=')
	{
		errMsg = _T ("Missing '='.");
		return true;
	}
	lex.GetNextToken(&token);

	if (token.type != treeditorLexFixedFloat && token.type != treeditorLexLong)
	{
		errMsg = _T ("Missing a number.");
		return true;
	}
	c->_ediGraphLeft = token.fnumber;
	lex.GetNextToken(&token);

	if (token.type != ';')
	{
		errMsg = _T ("Missing ';'.");
		return true;
	}
	lex.GetNextToken(&token);

	// Right.
	if (token.type != treeditorLex_ediGraphRight)
	{
		errMsg = _T ("Missing ediGraphRight");
		return true;
	}
	lex.GetNextToken(&token);

	if (token.type != '=')
	{
		errMsg = _T ("Missing '='.");
		return true;
	}
	lex.GetNextToken(&token);

	if (token.type != treeditorLexFixedFloat && token.type != treeditorLexLong)
	{
		errMsg = _T ("Missing a number.");
		return true;
	}
	c->_ediGraphRight = token.fnumber;
	lex.GetNextToken(&token);

	if (token.type != ';')
	{
		errMsg = _T ("Missing ';'.");
		return true;
	}
	lex.GetNextToken(&token);

	// Top.
	if (token.type != treeditorLex_ediGraphTop)
	{
		errMsg = _T ("Missing ediGraphTop");
		return true;
	}
	lex.GetNextToken(&token);

	if (token.type != '=')
	{
		errMsg = _T ("Missing '='.");
		return true;
	}
	lex.GetNextToken(&token);

	if (token.type != treeditorLexFixedFloat && token.type != treeditorLexLong)
	{
		errMsg = _T ("Missing a number.");
		return true;
	}
	c->_ediGraphTop = token.fnumber;
	lex.GetNextToken(&token);

	if (token.type != ';')
	{
		errMsg = _T ("Missing ';'.");
		return true;
	}
	lex.GetNextToken(&token);

	// Bottom.
	if (token.type != treeditorLex_ediGraphBottom)
	{
		errMsg = _T ("Missing ediGraphBottom");
		return true;
	}
	lex.GetNextToken(&token);

	if (token.type != '=')
	{
		errMsg = _T ("Missing '='.");
		return true;
	}
	lex.GetNextToken(&token);

	if (token.type != treeditorLexFixedFloat && token.type != treeditorLexLong)
	{
		errMsg = _T ("Missing a number.");
		return true;
	}
	c->_ediGraphBottom = token.fnumber;
	lex.GetNextToken(&token);

	if (token.type != ';')
	{
		errMsg = _T ("Missing ';'.");
		return true;
	}
	lex.GetNextToken(&token);

	c->_ediGraphPosition = true;
	return false;
}

bool CTreeditorDoc::ParseClass (CTreeditorLex &lex, STreeditorTokenDesc &token, CString &errMsg)
{
	if (token.type == treeditorLex_class)
	{
		lex.GetNextToken (&token);
		if (token.type == treeditorLexId)
		{
			CTreeditorClass *last = AddNewClass (-1);
			if (NULL != last)
			{
				last->name = token.string;
				lex.GetNextToken (&token);
				if (token.type == '{')
				{
					if (!ParseParams (lex, token, errMsg))
					{
						if (token.type == '}')
						{
							lex.GetNextToken (&token);
							if (token.type == ';')
							{
								return false;
							}
							else
							{
								errMsg = "';' behind a class declaration is missing.";
								return true;
							}
						}
						else
						{
							errMsg = "'}' is missing.";
							return true;
						}
					}
					else
					{
						return true;
					}
				}
				else
				{
					errMsg = "'{' is missing.";
					return true;
				}
			}
			else
			{
				errMsg = "Add new class is impossible.";
				return true;
			}
		}
		else
		{
			errMsg = "Class identifier is missing.";
			return true;
		}
	}
	else
	{
		errMsg = "Keyword 'class' is missing.";
		return true;
	}
}

bool CTreeditorDoc::ParseDoc (CTreeditorLex &lex, CString &errMsg)
{
	STreeditorTokenDesc token;
	
	lex.GetNextToken (&token);
	while (token.type != treeditorLexEof)
	{
		if (ParseClass (lex, token, errMsg))
		{
			return true;
		}

		lex.GetNextToken (&token);
	}

	return false;
}

unsigned long CTreeditorDoc::GetClassesLength ()
{
	return _classes.GetSize();
}

void CTreeditorDoc::UpdateClassAttrs ()
{
	const int n = _classes.GetSize();
	for (int i = 0; i < n; ++i)
	{
		_classes[i]->attrs = "";
	}

	for (int i = 0; i < n; ++i)
	{
		CTreeditorClass *c = _classes[i];

		if (c->selected)
		{
			// find parents
			for (unsigned long j = 0; j < n; ++j)
			{
				CTreeditorClass *p = _classes[j];
				int n = p->HasAnyChildOfName (c->name);
				while (n > 0)
				{
					p->attrs += "P ";
					--n;
				}
			}

			// find children
			if (c->HasAnyChild ())
			{
				for (int j = 0; j < n; ++j)
				{
					CTreeditorClass *p = _classes[j];
					int n = c->HasAnyChildOfName (p->name);
					while (n > 0)
					{
						p->attrs += "C ";
						--n;
					}
				}
			}
		}
	}
}

CTreeditorClass* CTreeditorDoc::GetSelectedClass ()
{
	int n = _classes.GetSize();
	for (int i = 0; i < n; ++i)
	{
		CTreeditorClass *c = _classes[i];
		if (!c->selected)
		{
			continue;
		}

		return c;
	}

	return NULL;
}

void CTreeditorDoc::MoveSelectedClassesUp ()
{
	int n = _classes.GetSize ();
	for (int i = 1; i < n; ++i)
	{
		if (_classes[i]->selected)
		{
			CTreeditorClass *c = _classes[i - 1];
			if (!c->selected)
			{
				_classes[i - 1] = _classes[i];
				_classes[i] = c;
			}
		}
	}
}

void CTreeditorDoc::MoveSelectedClassesDown ()
{
	// it is nonsence to move down the last component
	for (int i = _classes.GetUpperBound(); i > 0;)
	{
		if (_classes[--i]->selected)
		{
			CTreeditorClass *c = _classes[i + 1];
			if (!c->selected)
			{
				_classes[i + 1] = _classes[i];
				_classes[i] = c;
			}
		}
	}
}

bool CTreeditorDoc::IsAnyClassSelected ()
{
	int n = _classes.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (!_classes[i]->selected)
		{
			continue;
		}

		return true;
	}

	return false;
}

bool CTreeditorDoc::AllChildrenExist (const CTreeditorClass *c)
{
	unsigned long i;
	for (const char *name = c->GetFirstChildClassName (&i); name != NULL; name = c->GetNextChildClassName (&i))
	{
		if (FindClass (name) == NULL)
		{
			return false;
		}
	}
	
	return true;
}

bool CTreeditorDoc::AreAllChildrenSaved (const CTreeditorClass *c, bool &error, CString &errMsg)
{
	unsigned long i;
	for (const char *name = c->GetFirstChildClassName (&i); name != NULL; name = c->GetNextChildClassName (&i))
	{
		CTreeditorClass *fc = FindClass (name);
		if (fc == NULL)
		{
			error = true;
			errMsg.Format ("Unknown child class \"%s\" in class \"%s\".", name, c->name);
			return true;
		}
		else if (fc->saved)
		{
			continue;
		}
		
		return false;
	}
	
	return true;
}


/////////////////////////////////////////////////////////////////////////////
// CTreeditorDoc serialization

void CTreeditorDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
		ar.GetFile ()->SeekToBegin ();

		CString str;
		CString errMsg;
		if (Generate (str, errMsg))
		{
			AfxMessageBox (errMsg, MB_OK | MB_ICONEXCLAMATION);
		}
		ar.WriteString (str);
	}
	else
	{
		// TODO: add loading code here
		DeleteAllClasses ();

		CTreeditorArchiveInput input (&ar);
		CTreeditorLex lex (&input, &CTreeditorLex::treeKeywords[0], 
			CTreeditorLex::treeKeywordsLength);
		CString errMsg;

		if (ParseDoc (lex, errMsg))
		{
			AfxMessageBox (errMsg, MB_OK | MB_ICONEXCLAMATION);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// CTreeditorDoc diagnostics

#ifdef _DEBUG
void CTreeditorDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CTreeditorDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTreeditorDoc commands

BOOL CTreeditorDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	// TODO: Add your specialized creation code here

	if (!InitClasses ())
	{
		return FALSE;
	}
	
	if (!CDocument::OnOpenDocument(lpszPathName))
		return FALSE;
	
	_currentStamp.Init (_classes, true);
	return TRUE;
}

void CTreeditorDoc::DeleteContents() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	DeleteClasses();
	
	int n = _undos.GetSize();
	for (int i = 0; i < n; ++i)
	{
		delete _undos[i];
	}
	_undos.RemoveAll();

	n = _redos.GetSize();
	for (int i = 0; i < n; ++i)
	{
		delete _redos[i];
	}
	_redos.RemoveAll();

	_currentStamp.Init (_classes, true);
	CDocument::DeleteContents();
}

CTreeditorDocUndo *CTreeditorDoc::OpenUndo()
{
	CTreeditorDocUndo *undo = new CTreeditorDocUndo ();
	if (undo != NULL)
	{
		undo->BeginAction(this);
	}
	return undo;
}

void CTreeditorDoc::CloseUndo(CTreeditorDocUndo *undo)
{
	if (undo != NULL)
	{
		int n = _redos.GetSize();
		for (int i = 0; i < n; ++i)
		{
			delete _redos[i];
		}
		_redos.RemoveAll();

		undo->EndAction();
		_undos.Add(undo);
	}
}

void CTreeditorDoc::DisposeUndo (CTreeditorDocUndo *undo)
{
	if (undo != NULL)
	{
		delete undo;
	}
}

void CTreeditorDoc::DoUndo()
{
	int n = _undos.GetUpperBound();
	if (n >= 0)
	{
		_undos[n]->Undo();
		_redos.InsertAt(0, _undos[n]);
		_undos.RemoveAt(n);
	}
}

void CTreeditorDoc::DoRedo()
{
	int n = _redos.GetSize();
	if (n > 0)
	{
		_redos[0]->Redo();
		_undos.Add(_redos[0]);
		_redos.RemoveAt(0);
	}
}

CTreeditorDocState::CTreeditorDocState()
{
}

CTreeditorDocState::~CTreeditorDocState()
{
	Erase();
}

void CTreeditorDocState::Erase()
{
	_code.Empty();
}

bool CTreeditorDocState::Record(CTreeditorDoc *doc)
{
	Erase();

	CString err;
	doc->Generate (_code, err);
	
	return true;
}

void CTreeditorDocState::Reset(CTreeditorDoc *doc)
{
	doc->DeleteClasses();

	CTreeditorStringInput input (_code);
	CTreeditorLex lex (&input, &CTreeditorLex::treeKeywords[0], 
		CTreeditorLex::treeKeywordsLength);
	CString errMsg;
	
	if (doc->ParseDoc (lex, errMsg))
	{
		// ?
	}
	doc->SetModifiedFlag (TRUE);
	doc->UpdateStamp (true, false);
	doc->UpdateAllViews(NULL, UH_UNDO, NULL);
}

void CTreeditorDoc::UpdateScriptParams ()
{
	int n = _classes.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		_classes[i]->UpdateScriptParams ();
	}
}

void CTreeditorDoc::ChangeParam(CTreeditorClass *c, bool recUndo, CTreeditorDocUndo *undo, int paramIdx, LPCTSTR value, bool dirty)
{
	if (paramIdx < treeditorClassParam_numberOfTypes)
	{
		ChangeParamInTree (c, recUndo, undo, paramIdx, value, dirty);
	}
	else
	{
		ChangeParamInScript(c, paramIdx, value);
	}
}

void CTreeditorDoc::ChangeParamInTree(CTreeditorClass *c, bool recUndo, CTreeditorDocUndo *undo, int paramIdx, LPCTSTR value, bool dirty)
{
	if (recUndo && undo == NULL)
	{
		undo = OpenUndo();
		if (undo == NULL)
		{
			return;
		}
	}

	c->params[paramIdx].DegenerateValue(value);
	if (dirty)
	{
		SetModifiedFlag (TRUE);
	}

	if (undo != NULL)
	{
		CloseUndo(undo);
	}

	if (paramIdx == treeditorClassParam_script)
	{
		UpdateScriptParams ();
	}

	UpdateStamp (dirty, paramIdx == treeditorClassParam_script);
}

void CTreeditorDoc::ChangeParamInScript(CTreeditorClass *c, int paramIdx, LPCTSTR value)
{
	if (paramIdx < treeditorClassParam_numberOfTypes)
	{
		return;
	}

	paramIdx -= treeditorClassParam_numberOfTypes;

	CString scriptFile = c->params[treeditorClassParam_script].GetValue ();
	scriptFile = scriptFile.Mid (1, scriptFile.GetLength () - 2);

	CFile f;
	if (f.Open(scriptFile, CFile::modeRead, NULL))
	{
		CString str;
		int fileLen = f.GetLength();
		int strLen = fileLen + sizeof (TCHAR) - 1 / sizeof (TCHAR);
		LPTSTR p = str.GetBuffer (strLen);
		bool read = f.Read(p, fileLen) == fileLen;
		str.ReleaseBuffer();
		f.Close();
		
		if (read)
		{
			CArray<STreeditorParamDesc*, STreeditorParamDesc*> sps;
			CScriptParser sp (str, &sps);
			STreeditorScriptParamDesc desc;
			desc.group.colorIndex = 0;
			
			int i = 0;
			while (sp.GetNextParam(&desc))
			{
				STreeditorParamDesc *pd = new STreeditorParamDesc ();
				STreeditorParamDesc::copy (*pd, desc);
				sps.Add (pd);

				if (i == paramIdx)
				{
					CString out = str.Left (desc._endOfPrefix - (LPCTSTR) str);
					out += _T (' ');
					switch (desc.type)
					{
					case treeditorLexArray:
						out += _T ('[');
						out += value;
						out += _T (']');
						break;

					case treeditorLexString:
						out += _T ('"');
						out += value;
						out += _T ('"');
						break;

					case treeditorLexId:
						if (desc.isChildClassName)
						{
							out += _T ('"');
							out += value;
							out += _T ('"');
						}
						else
						{
							out += value;
						}
						break;

					default:
						out += value;
						break;
					}
					out += desc._startOfPostfix;

					if (f.Open(scriptFile, CFile::modeCreate | CFile::modeWrite, NULL))
					{
						f.Write (out, out.GetLength() * sizeof (TCHAR));
						f.Close ();
					}

					strcpy (sps[paramIdx]->defValue, value);
				}
				++i;
			}

			UpdateScriptParams ();

			int n = sps.GetSize ();
			for (int i = 0; i < n; ++i)
			{
				delete sps[i];
			}
			sps.RemoveAll ();
		}
	}
}

void CTreeditorDoc::UpdateStamp (bool paramChange, bool updateScripts)
{
	if (!_currentStamp.IsInit ())
	{
		_currentStamp.Init (_classes, paramChange);
	}
	else
	{
		if (paramChange)
		{
			_currentStamp.SetChange ();
		}
		if (updateScripts)
		{
			_currentStamp.InitScripts (_classes);
		}
	}
}

bool CTreeditorDoc::NeedToUpdatePreview () const
{
	return _lastStamp != _currentStamp;
}

/////////////////////////////////////////////////////////////////////////////
// CTreeditorDocStamp

void CTreeditorDocStamp::Init (const CArray<CTreeditorClass*, CTreeditorClass*> &classes, bool change)
{
	SetChange (change);
	InitScripts (classes);
	_init = true;
}

void CTreeditorDocStamp::InitScripts (const CArray<CTreeditorClass*, CTreeditorClass*> &classes)
{
	_scriptStamps.RemoveAll ();
	int n = classes.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		CTreeditorClass *c = classes[i];
		if (c->HasParam (treeditorClassParam_script))
		{
			CTreeditorScriptTimeStamp sts (c->params[treeditorClassParam_script]._value);
			_scriptStamps.Add (sts);
		}
	}
}