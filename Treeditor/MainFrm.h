// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__1769EA7A_C46E_498C_968D_7E897A8AF352__INCLUDED_)
#define AFX_MAINFRM_H__1769EA7A_C46E_498C_968D_7E897A8AF352__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "PropertyBar.h"
#include "ComponentBar.h"
#include "GraphBar.h"
#include "DebugBar.h"
#include "InfoBar.h"

class CMainFrame : public CFrameWnd
{
	
protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// Attributes
public:
	static const TCHAR winPlaceFormat[];
	static const TCHAR sizeFormat[];

	CPropertyBar _propertyBar;
	CComponentBar _componentBar;
	CGraphBar _graphBar;
	CDebugBar _debugBar;
	CInfoBar _infoBar;

	bool _ctrlDown;
	bool _shiftDown;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	static void WriteWindowPlacement(LPWINDOWPLACEMENT pwp, LPCTSTR section, LPCTSTR entry);
	static BOOL ReadWindowPlacement (LPWINDOWPLACEMENT pwp, LPCTSTR section, LPCTSTR entry);
	static void WriteSize(const CSize &s, LPCTSTR section, LPCTSTR entry);
	static BOOL ReadSize (CSize &s, LPCTSTR section, LPCTSTR entry);
	
	void SetStatusText (LPCTSTR text);

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnViewComponents();
	afx_msg void OnUpdateViewComponents(CCmdUI* pCmdUI);
	afx_msg void OnViewProperties();
	afx_msg void OnUpdateViewProperties(CCmdUI* pCmdUI);
	afx_msg void OnViewGraph();
	afx_msg void OnUpdateViewGraph(CCmdUI* pCmdUI);
	afx_msg void OnGraphmNormal();
	afx_msg void OnUpdateGraphmNormal(CCmdUI* pCmdUI);
	afx_msg void OnGraphmDesign();
	afx_msg void OnUpdateGraphmDesign(CCmdUI* pCmdUI);
	afx_msg void OnGraphmZoom();
	afx_msg void OnUpdateGraphmZoom(CCmdUI* pCmdUI);
	afx_msg void OnCenter();
	afx_msg void OnUpdateCenter(CCmdUI* pCmdUI);
	afx_msg void OnActivateApp(BOOL bActive, HTASK hTask);
	afx_msg void OnViewDebug();
	afx_msg void OnUpdateViewDebug(CCmdUI* pCmdUI);
	afx_msg void OnEditClearDebug();
	afx_msg void OnViewInfo();
	afx_msg void OnUpdateViewInfo(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__1769EA7A_C46E_498C_968D_7E897A8AF352__INCLUDED_)
