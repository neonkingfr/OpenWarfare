
#include "stdafx.h"
#include "TreeditorLex.h"


const STreeditorKeyword CTreeditorLex::treeKeywords[] =
{
	{"class", treeditorLex_class},
	{"type", treeditorLex_type},
	{"ediGraphLeft", treeditorLex_ediGraphLeft},
	{"ediGraphRight", treeditorLex_ediGraphRight},
	{"ediGraphTop", treeditorLex_ediGraphTop},
	{"ediGraphBottom", treeditorLex_ediGraphBottom},
};

const unsigned int CTreeditorLex::treeKeywordsLength = sizeof (treeKeywords) / sizeof (STreeditorKeyword);
