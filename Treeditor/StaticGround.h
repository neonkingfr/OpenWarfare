#if !defined(AFX_STATICGROUND_H__B8FF0D9D_D4A3_461A_B992_681335C4E1F1__INCLUDED_)
#define AFX_STATICGROUND_H__B8FF0D9D_D4A3_461A_B992_681335C4E1F1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StaticGround.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CStaticGround window

class CStaticGround : public CStatic
{
// Construction
public:
	CStaticGround(COLORREF color);

// Attributes
public:
	COLORREF _color;
	CFont *_font;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStaticGround)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CStaticGround();
	CSize CalcPixelWidth ();

	// Generated message map functions
protected:
	//{{AFX_MSG(CStaticGround)
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STATICGROUND_H__B8FF0D9D_D4A3_461A_B992_681335C4E1F1__INCLUDED_)
