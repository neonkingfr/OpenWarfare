#if !defined(AFX_UIPROPERTIESCTRL_H__C865DCC3_480A_46CC_9DD4_78851E1D216D__INCLUDED_)
#define AFX_UIPROPERTIESCTRL_H__C865DCC3_480A_46CC_9DD4_78851E1D216D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UiPropertiesCtrl.h : header file
//

#include "UiPropertySubview.h"

/////////////////////////////////////////////////////////////////////////////
// CUiPropertiesCtrl window

class CUiPropertiesCtrl : public CStatic
{
// Construction
public:
	CUiPropertiesCtrl();

private:
	CUiPropertiesCtrl (const CUiPropertiesCtrl &src);

// Attributes
public:
	CUiPropertySubview _propFrame;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUiPropertiesCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CUiPropertiesCtrl();

	BOOL Init ();
	void AddUiProperty (LPCTSTR name, COLORREF color, const CChannelProtocol &channel, long data, long use, bool selected, LPCTSTR value, bool enabled, va_list params);
	void AddUiProperty (LPCTSTR name, COLORREF color, const CChannelProtocol &channel, long data, long use, bool selected, LPCTSTR value, bool enabled, ...);
	void AddUiProperty (LPCTSTR name, const CChannelProtocol &channel, long data, long use, bool selected, LPCTSTR value, bool enabled, ...);
	int GetPropCount () const;
	void DeleteAllProps ();
	void DeleteProp (int i);
	bool IsPropCtrl (const CWnd *ctrl) const;
	bool IsPropCtrlID (int id) const;

	void SelectProp (int i, bool sel);
	void SelectOneProp (int idx);
	void SelectAllProps ();
	void DeselectAllProps ();
	int GetSelectedPropCount () const;
	int GetNextSelectedProp (int i) const;

	void SetPropValue (int i, LPCTSTR val, bool updateUi);
	LPCTSTR GetPropValue (int i) const;

	long GetPropData (int i) const;

	void RecalcLayout ();
	void SetEnabled (int i, bool e);
	
	void GetOptimalSize (int &w, int &h) const;

	// Generated message map functions
protected:
	//{{AFX_MSG(CUiPropertiesCtrl)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UIPROPERTIESCTRL_H__C865DCC3_480A_46CC_9DD4_78851E1D216D__INCLUDED_)
