// UiPropertySubview.cpp : implementation file
//

#include "stdafx.h"
#include <El/elementpch.hpp>
#include "RV/Tree/mytree.h"
#include "Treeditor.h"
#include "TreeditorLex.h"
#include "TreeditorClass.h"

#include "TreeditorDoc.h"
#include "TreeditorView.h"
#include "MainFrm.h"
#include "UiPropertySubview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUiPropertySubview

CUiPropertySubview::CUiPropertySubview()
{
}

CUiPropertySubview::~CUiPropertySubview()
{
	DeleteAllProps ();
}


BEGIN_MESSAGE_MAP(CUiPropertySubview, CWnd)
	//{{AFX_MSG_MAP(CUiPropertySubview)
	ON_WM_VSCROLL()
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	ON_WM_MOUSEWHEEL()
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUiPropertySubview message handlers

void CUiPropertySubview::AddUiProperty (LPCTSTR name, COLORREF color, const RECT &r, CWnd *parent, const CChannelProtocol &channel, long data, long use, bool selected, LPCTSTR value, bool enabled, va_list params)
{
	int pos = GetScrollPos (SB_VERT);

	int top = (_uis.GetSize () - pos) * UI_PROPERTY_HEIGHT;
	RECT rect = {0, top, r.right - r.left, top + UI_PROPERTY_HEIGHT};

	CUiProperty *uiProp = new CUiProperty (name, color, rect, this, channel, data, use, _uis.GetSize (), selected, value, enabled, params);
	_uis.Add (uiProp);

	UpdateScrollbars (r.bottom - r.top);
}

void CUiPropertySubview::UpdateScrollbars (int height)
{
	int max = _uis.GetSize () - height / UI_PROPERTY_HEIGHT;
	if (max < 0)
	{
		max = 0;
	}

	int pos = GetScrollPos (SB_VERT);
	if (pos > max)
	{
		ScrollBy (0, (pos - max) * UI_PROPERTY_HEIGHT);
		pos = max;
	}

	SCROLLINFO sbinfo;
	sbinfo.cbSize = sizeof (sbinfo);
	sbinfo.fMask = SIF_ALL;
	sbinfo.nMin = 0;
	sbinfo.nMax = max;
	sbinfo.nPage = 0;
	sbinfo.nPos = pos;
	SetScrollInfo (SB_VERT, &sbinfo, TRUE);
}

void CUiPropertySubview::DeleteAllProps ()
{
	int n = _uis.GetSize ();
	int i;
	for (i = 0; i < n; ++i)
	{
		delete _uis[i];
	}
	_uis.RemoveAll ();
}

void CUiPropertySubview::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: Add your message handler code here and/or call default
	
	int pos = GetScrollPos (SB_VERT);
	int max, min;
	GetScrollRange (SB_VERT, &min, &max);

	RECT r;
	GetClientRect (&r);
	int visible = (r.bottom - r.top) / UI_PROPERTY_HEIGHT;

	int oldPos = pos;
	switch (nSBCode)
	{
	case SB_BOTTOM:
		pos = max - 1;
		break;

	case SB_ENDSCROLL:
		break;

	case SB_LINEDOWN:
		++pos;
		break;

	case SB_LINEUP:
		--pos;
		break;

	case SB_PAGEDOWN:
		pos += visible;
		break;

	case SB_PAGEUP:
		pos -= visible;
		break;

	case SB_THUMBPOSITION:
		pos = nPos;
		break;

	case SB_THUMBTRACK:
		pos = nPos;
		break;

	case SB_TOP:
		pos = 0;
		break;
	}

	if (pos < min)
	{
		pos = min;
	}
	else if (pos > max)
	{
		pos = max;
	}

	SetScrollPos (SB_VERT, pos);
	ScrollBy (0, (oldPos - pos) * UI_PROPERTY_HEIGHT);
	SetFocus ();
}

void CUiPropertySubview::ScrollBy (int dx, int dy)
{
	int n = _uis.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		_uis[i]->ScrollBy (dx, dy);
	}
}

void CUiPropertySubview::OnSize(UINT nType, int cx, int cy) 
{
	// TODO: Add your message handler code here
	
	int n = _uis.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		_uis[i]->SetWidth (cx);
	}

	UpdateScrollbars (cy);
}

void CUiPropertySubview::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here
	
	// Do not call CWnd::OnPaint() for painting messages
	RECT r;
	GetClientRect (&r);
	int pos = GetScrollPos (SB_VERT);
	r.top = (_uis.GetSize () - pos) * UI_PROPERTY_HEIGHT;
	if (r.top < r.bottom)
	{
		dc.FillSolidRect (&r, UI_PROPERTY_DEFAULT_COLOR);
	}
}

BOOL CUiPropertySubview::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default
	
	return TRUE;
}

BOOL CUiPropertySubview::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	long id = wParam & 0xffff;
	if (id >= UI_PROPERTY_BASE_ID && id < UI_PROPERTY_BASE_ID + _uis.GetSize () * UI_PROPERTY_LAST_ID)
	{
		long msg = wParam >> 16;
		long i = id - UI_PROPERTY_BASE_ID;
		int idx = i / UI_PROPERTY_LAST_ID;
		CUiProperty *ui = _uis[idx];
		switch (i % UI_PROPERTY_LAST_ID)
		{
		case UI_PROPERTY_EDIT_BUTTON:
			ui->_channel->OnPropEdit (this, idx, ui->_data);
			return TRUE;

		case UI_PROPERTY_SELECT_BUTTON:
			ui->_channel->OnPropSelect (this, idx, ui->_data);
			return TRUE;;

		case UI_PROPERTY_SELECT_CHECK:
			{
				bool s = ui->_selCheck.GetCheck () == 1;
				ui->_selCheck.SetCheck (ui->_channel->SetPropSelect (this, idx, ui->_data, !s) ? 1 : 0);
			}
			return TRUE;

		case UI_PROPERTY_BOOL_CHECK:
			{
				ui->_channel->SetPropValue (this, idx, ui->_data, ui->_bool.GetCheck () == 1 ? "false" : "true");
			}
			return TRUE;;

		case UI_PROPERTY_ENUM_COMBO:
			if (msg == CBN_SELENDOK)
			{
				CString tmp;
				//ui->_enums.GetLBText (ui->_enums.GetCurSel (), tmp);
				ui->_enums.GetWindowText (tmp);
				ui->_channel->SetPropValue (this, idx, ui->_data, tmp);
				return TRUE;
			}
			break;

		case UI_PROPERTY_CHILD_COMBO:
			if (msg == CBN_SELENDOK)
			{
				CString tmp;
				ui->_children.GetWindowText (tmp);
				ui->_channel->SetPropValue (this, idx, ui->_data, tmp);
				return TRUE;
			}
			break;

		case UI_PROPERTY_VALUE_EDIT:
			{
				bool immediateResponse = ((CTreeditorView*) ((CMainFrame*) AfxGetMainWnd ())->GetActiveView ())->m_immediateResponse;

				if (msg == EN_KILLFOCUS && !immediateResponse || msg == EN_CHANGE && immediateResponse)
				{
					CString tmp;
					ui->_value.GetWindowText (tmp);
					ui->_channel->SetPropValue (this, idx, ui->_data, tmp);
					return TRUE;
				}
			}
			break;
		}
	}

	return CWnd::OnCommand(wParam, lParam);
}

BOOL CUiPropertySubview::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
	// TODO: Add your message handler code here and/or call default
	
	int pos = GetScrollPos (SB_VERT);
	int max, min;
	GetScrollRange (SB_VERT, &min, &max);

	RECT r;
	GetClientRect (&r);

	int oldPos = pos;
	pos -= zDelta / WHEEL_DELTA;

	if (pos < min)
	{
		pos = min;
	}
	else if (pos > max)
	{
		pos = max;
	}

	SetScrollPos (SB_VERT, pos);
	ScrollBy (0, (oldPos - pos) * UI_PROPERTY_HEIGHT);

	return TRUE;
}

void CUiPropertySubview::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	CWnd::OnLButtonDown(nFlags, point);
	SetFocus ();
}

int CUiPropertySubview::GetPropCount () const
{
	return _uis.GetSize ();
}

void CUiPropertySubview::SelectProp (int i, bool sel)
{
	_uis[i]->_selCheck.SetCheck (sel ? 1 : 0);
}

void CUiPropertySubview::SelectOneProp (int idx)
{
	int n = _uis.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		_uis[i]->_selCheck.SetCheck (i == idx ? 1 : 0);
	}
}

void CUiPropertySubview::SelectAllProps ()
{
	int n = _uis.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		_uis[i]->_selCheck.SetCheck (1);
	}
}

void CUiPropertySubview::DeselectAllProps ()
{
	int n = _uis.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		_uis[i]->_selCheck.SetCheck (0);
	}
}

int CUiPropertySubview::GetNextSelectedProp (int i) const
{
	int n = _uis.GetSize ();
	for (++i; i < n; ++i)
	{
		if (_uis[i]->_selCheck.GetCheck () == 1)
		{
			return i;
		}
	}
	return -1;
}

long CUiPropertySubview::GetPropData (int i) const
{
	return _uis[i]->_data;
}

LPCTSTR CUiPropertySubview::GetPropValue (int i) const
{
	return _uis[i]->_valueStr;
}

int CUiPropertySubview::GetSelectedPropCount () const
{
	int c = 0;
	int n = _uis.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (_uis[i]->_selCheck.GetCheck () == 1)
		{
			++c;
		}
	}
	return c;
}

void CUiPropertySubview::SetPropValue (int i, LPCTSTR val, bool updateUi)
{
	_uis[i]->SetPropValue (val, updateUi);
}

void CUiPropertySubview::RecalcLayout ()
{
	int n = _uis.GetSize ();
	int w = 0;
	for (int i = 0; i < n; ++i)
	{
		CSize s = _uis[i]->GetLabelExtent ();
		if (s.cx > w)
		{
			w = s.cx;
		}
	}

	int rw = 0;
	for (int i = 0; i < n; ++i)
	{
		int iw = _uis[i]->SetLabelWidth (w);
		if (iw > rw)
		{
			rw = iw;
		}
	}

	for (int i = 0; i < n; ++i)
	{
		_uis[i]->SetWidth (rw);
	}
}

void CUiPropertySubview::SetEnabled (int i, bool e)
{
	_uis[i]->SetEnabled (e);
}

void CUiPropertySubview::DeleteProp (int i)
{
	delete _uis[i];
	_uis.RemoveAt (i);
	RECT r;
	GetClientRect (&r);
	UpdateScrollbars (r.bottom - r.top);
}

void CUiPropertySubview::GetOptimalSize (int &w, int &h) const
{
	w = -1;
	h = 2;
	int n = _uis.GetSize ();
	if (n > 0)
	{
		for (int i = 0; i < n; ++i)
		{
			int iw, ih;
			_uis[i]->GetOptimalSize (iw, ih);
			h += ih;
			if (w < iw)
			{
				w = iw;
			}
		}
		w += 2;
	}
	else
	{
		w = 200;
		h = 100;
	}
}