#if !defined(AFX_CURVEDLG_H__ECE0C329_821A_4050_B201_67711EC83FD9__INCLUDED_)
#define AFX_CURVEDLG_H__ECE0C329_821A_4050_B201_67711EC83FD9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CurveDlg.h : header file
//

#include "CurveItem.h"

/////////////////////////////////////////////////////////////////////////////
// CCurveDlg dialog

class CCurveDlg : public CDialog
{
// Construction
public:
	static LPCTSTR _curvePrefix[];
	enum ECurveType
	{
		LINEAR_CURVE = 0,
		QUADRATIC_CURVE,
		EX_QUADRATIC_CURVE,
		LINEAR_CURVE_WITH_X,
		QUADRATIC_CURVE_WITH_X,
		EX_QUADRATIC_CURVE_WITH_X
	};
	CCurveDlg(LPCTSTR title, const ECurveType _curveType, const bool varPointNumber, const float step, 
		const CArray<int, int> &keyTypes, const CArray<float, float> &values, 
		CStringArray &names, const float min, const float max, const bool horizontalX,
		LPCTSTR xDescr, LPCTSTR yDescr, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCurveDlg)
	enum { IDD = IDD_CURVEDLG };
	CButton	_straightCtrl;
	CStatic	_curveXLabel;
	CEdit	_curveXCtrl;
	CButton	_scaleMulCtrl;
	CStatic	_scaleLabel;
	CButton	_scaleDivCtrl;
	CEdit	_scaleCtrl;
	CButton	_curveDeleteCtrl;
	CButton	_mirrorXCtrl;
	CButton	_curveLoadCtrl;
	CButton	_curveSaveCtrl;
	CComboBox	_curveNameCtrl;
	CButton	_mirrorCtrl;
	CButton	_horizontalXCtrl;
	CButton	_previewCtrl;
	CStatic	_minLabel;
	CEdit	_minCtrl;
	CStatic	_maxLabel;
	CEdit	_maxCtrl;
	CStatic	_curveStepLabel;
	CStatic	_curveValueLabel;
	CButton	_ok;
	CButton	_cancel;
	CEdit	_curveValueCtrl;
	CSpinButtonCtrl	_strafeCtrl;
	CEdit	_curveStepCtrl;
	CSpinButtonCtrl	_directionSpin;
	CEdit	_directionCtrl;
	CStatic	_directionLabel;
	CCurveItem	_curve;
	//}}AFX_DATA

public:
	const ECurveType _curveType;
	float _min;
	float _max;
	const float _originalMin;
	const float _originalMax;
	const bool _varPointNumber;
	bool _horizontalX;
	CString _xDescr;
	CString _yDescr;
	bool _modified;
	bool _updateTime;

protected:
	int _maxKeyType;
	CArray<int, int> _keyTypes;
	CArray<float, float> _values;
	CStringArray &_names;
	float _step;
	LPCTSTR _title;
	void (*_hookProc) (const CCurveDlg *dlg, long param1, long param2, long param3);
	long _hookParam1;
	long _hookParam2;
	long _hookParam3;
	bool _preview;
	const CString _curveEntry;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCurveDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation	
public:
	void UpdateCurveX ();
	void UpdateCurveValue ();
	void UpdateCurveTangent ();
	void SetHookProc (void (*hook) (const CCurveDlg*, long, long, long), long param1, long param2, long param3);
	void CurveChanged ();
	float GetMarkValue (int i) const;
	float GetMarkX (int i) const;
	float GetMarkTangent (int i) const;
	void SetMarkValue (int i, float v);
	void SetMarkX (int i, float v);
	void SetMarkTangent (int i, float v);
	int GetMarkSize () const;
	int GetMarkType (int i) const;
	int ChangeMarkType (int i);
	void SetMarkType (int i, int keyType);
	LPCTSTR GetMarkName (int i) const;
	void SetMarkName (int i, LPCTSTR name);
	void InsertMarkAt (int i, float x, float value, float tangent, int keyType, LPCTSTR name);
	void RemoveMarkAt (int i);
	void RemoveAllMarks ();
	void AddMark (float x, float value, float tangent, int keyType, LPCTSTR name);
	void SetModified (bool modified = true);
	bool IsModified () const;

	void GetLinearCurveString (CString &str) const;
	void GetLinearCurveWithXString (CString &str) const;
	void GetQuadraticCurveString (CString &str) const;
	void GetQuadraticCurveWithXString (CString &str) const;
	void GetExQuadraticCurveString (CString &str) const;
	void GetExQuadraticCurveWithXString (CString &str) const;

protected:

	// Generated message map functions
	//{{AFX_MSG(CCurveDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnChangeCurveDirection();
	afx_msg void OnChangeCurveStep();
	afx_msg void OnDeltaPosCurveStrafe(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeCurveValue();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDeltaPosCurveDirectionSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeCurveMin();
	afx_msg void OnChangeCurveMax();
	afx_msg void OnKillFocusCurveValue();
	afx_msg void OnKillFocusCurveMin();
	afx_msg void OnKillFocusCurveMax();
	afx_msg void OnPreview();
	afx_msg void OnHorizontalX();
	afx_msg void OnMirror();
	afx_msg void OnCurveSave();
	afx_msg void OnCurveLoad();
	afx_msg void OnMirrorX();
	afx_msg void OnCurveDelete();
	afx_msg void OnCurveScaleMul();
	afx_msg void OnCurveScaleDiv();
	afx_msg void OnChangeCurveX();
	afx_msg void OnKillFocusCurveX();
	afx_msg void OnStraight();
	afx_msg void OnKillFocusCurveDirection();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CURVEDLG_H__ECE0C329_821A_4050_B201_67711EC83FD9__INCLUDED_)
