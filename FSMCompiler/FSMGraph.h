#ifndef _FSM_GRAPH
#define _FSM_GRAPH

#include <El/elementpch.hpp>
#include <El/ParamArchive/paramArchive.hpp>
#include <El/ParamArchive/paramArchiveDb.hpp>
#include "report.h"

//! based on project FSMEditor, DefaultItem.h typedef DefaultItemType
typedef enum
{ 
  DF_UNDEF=-1, DF_START_ITEM, DF_END_ITEM, DF_ITEM, DF_USER_ITEM, 
  DF_CONDITION, DF_USER_INPUT_CONDITION, DF_USER_CONDITION,
  DF_KNEE, DF_TRUE_CONDITION, DF_FAKE_STATE, DF_DEFAULT_ITEM_NUM
} ItemType;

#define SGRI_CUSTOMDRAW 0x2
#define TRANSPARENTCOLOR 0xFFFFFFFF

class FSMItem;
//! Represents edge in FSMGraph
struct FSMCondition
{
  //! text from FSMEditor
  RString    text;
  //! id generated from text
  RString    id;
  //! type from FSMEditor
  ItemType   type;
  //! priority from FSMEditor
  float      priority;
  //! condition text from FSMEditor
  RString    condition;
  //! action text from FSMEditor
  RString    action;
  //! preCondition text from FSMEditor
  RString    preCondition;
  //! indexes to vertices (states), which are connected by this condition
  int        from, to;   //indexes to array FSMGraph::vertices
  //! index based on project FSMEditor, CGraphCtrl::items[]
  //! for report purposes (identifies the item)
  int        ix;
  //! default empty constructor
  FSMCondition() : priority(0) {}
  //! construct condition from FSMItem (only select relevant data)
  FSMCondition(FSMItem &);
};
TypeIsMovable(FSMCondition);

//! Represents vertex in FSMGraph
struct FSMState
{
  //! text from FSMEditor
  RString  text;
  //! id generated from text
  RString  id;
  //! type from FSMEditor
  ItemType type;
  //! initCode from FSMEditor
  RString  initCode;
  //! preCondition from FSMEditor
  RString preCondition;
  //! array of edges (conditions) leading from this vertex (state)
  AutoArray<FSMCondition> edges;
  //! index based on project FSMEditor, CGraphCtrl::items[]
  //! for report purposes (identifies the item)
  int        ix;         //for report purposes (identifies the item)
  //! default empty constructor
  FSMState() {}
  //! construct state from FSMItem (only select relevant data)
  FSMState(FSMItem &);
};
TypeIsMovable(FSMState);

struct TPreSuf
{
  RString prefix, prefix2;
  RString sufix, sufix2;
  TPreSuf() {prefix=prefix2=sufix=sufix2="";}
};

struct FSMNamedAttribute : public SerializeClass
{
  RString name;
  RString value;
  TPreSuf presuf;
  FSMNamedAttribute(RString nm="", RString val="") : name(nm), value(val) {}

  virtual LSError Serialize(ParamArchive &ar)
  {
    ar.Serialize("name", name, 1);
    ar.Serialize("value", value, 1);
    return LSOK;
  }
};
TypeIsMovable(FSMNamedAttribute);

struct FSMDecompileInfoGlobal
{
  RString fsmName, compileConfig;
  AutoArray<FSMNamedAttribute> attributes;
  float _grid;
  bool _gridshow, _pageClip, _pageColor;
  int _pageColorVal, _pageImageX, _pageImageY, grpcnt, nextID;
  //link
  int defLinkColor;
  bool defLinkUseCustom;
  //zoom
  float pzoomLeft, pzoomRight, pzoomBottom, pzoomTop;
  int clxs, clys;
  bool aspect;

  void AddAttribute(RString name, RString value);
};

struct FSMDecompileInfoWindow
{
  int flags, ptMaxPositionX, ptMaxPositionY, ptMinPositionX, ptMinPositionY;
  int rcNormalPositionLeft, rcNormalPositionTop, rcNormalPositionRight, rcNormalPositionBottom;
  int showCmd, splitpos;
};

//! item from FSMEditor, which can contain data of condition or state
class FSMItem
{
public:
  int        ix;
  int        id;
  RString    text;
  RString    textRaw;
  ItemType   type;
  float      priority;  //for state undef
  RString    condition; //for state undef
  RString    action;    //for state undef
  RString    initCode;  //for condition undef
  RString    preCondition;
  FSMItem() : priority(0) {}
};
TypeIsMovable(FSMItem);

//! link from FSMEditor
class FSMLink
{
public:
  int ixFrom;
  int ixTo;
  bool operator==(const FSMLink &lnk) const {return (ixFrom==lnk.ixFrom && ixTo==lnk.ixTo);}
  FSMLink() {}
  FSMLink(int from, int to) : ixFrom(from) , ixTo(to) {}
};
TypeIsMovable(FSMLink);

class FSMDecompileInfo;
class DecompileInfo;
class ParamEntry;
class DefaultConfigInfo;
//! Base class for compiled FSM representation
/*! 
Representation of FSM compiled from FSMEditor format *.bifsm
There is only AutoArray of vertices (States).
Each vertex (state) contains AutoArray of edges (Conditions)
*/
class FSMGraph
{
private:
  void FindInitAndFinalStateNames();
public:
  static struct CurrentValues {
    FSMState *state;
    FSMCondition *link;
    int numStates, numConditions;
    RString initStateName;
    AutoArray<RString> finalStateNames; //computed by FindInitAndFinalStateNames()
    int finalIx;
    CurrentValues() : finalStateNames() 
    {
      state=NULL; link=NULL; 
      initStateName=""; 
      finalIx=numStates=numConditions=0; 
    }
  } current_values;
  static DecompileInfo *dInfo;
  //! array of items decompile info (for FSMEditor reconstruction)
  AutoArray<FSMDecompileInfo> decompileInfo;
  AutoArray<FSMLink> decompileLinkInfo;
  FSMDecompileInfoGlobal decompileInfoGlobal;
  FSMDecompileInfoWindow decompileInfoWindow; 
  DefaultConfigInfo *dconfigInfo;
  //! array of vertices (states)
  AutoArray<FSMState> vertices;
  bool clearNewLines, rewriteFile;

  FSMGraph() {clearNewLines=rewriteFile=false;}
  //! Converts FSMGraph structure to paramFile and save it to outStream
  void Save(QOStream &outStream, bool clearNewLines);
  //! Compile FSMGraph using compile config and save output to out
  void Compile(const char *configFileName, const char *compiledFileName, QOStream &out);
  //! Repair all class name duplicities 
  void GenerateUniqueIDs(Report &report, bool isCompiling);

  void ClearCurrentValues();
};
extern RString FixWhiteChars(RString text);

// Global helper variable to make the TrimWithinGivenLenghts configurable by minChars entry under class Compile
extern int GMinChars;

//! container for Unspecified (Layout) values
#include <windows.h>
enum ShapeType 
{
  stBox, stEllipse, stDiamond, stParallelogramR, stParallelogramL,stBox2, stRoundedBox
};
struct LayoutItem : public RefCount
{
  ShapeType shape;
  int bgColor;
  int lineColor;
  int fontColor;
  int textAlign;
  int lWidth;
  int lStyle;
  float ffHeight;
  float ffWidth;
  LOGFONT lgFont;
  LSError Serialize(ParamArchive &ar1, ParamArchive &ar, int type, const DefaultConfigInfo &dconfigInfo);
  bool SerializeItem(int &ix, int type, const DefaultConfigInfo &dconfigInfo, AutoArray<FSMDecompileInfo> &decompileInfo, ParamArchive &ar);
};
TypeIsMovable(LayoutItem);

//! decompile info (coordinates, type of item, ...)
class FSMDecompileInfo
{
public:
  RString id;
  RString text;
  int flags, itemtype;
  float left, top, right, bottom;
  float priority;
  Ref<LayoutItem> layout;
  FSMDecompileInfo() : priority(0) {}
};
TypeIsMovable(FSMDecompileInfo);

//! place for one FSMConfig.cfg value
struct DefaultConfigItem
{
  int     BgColor, lStyle, Shape;
  RString FontFace;
  float   FontHeight;
  int     FontWeight, FontCharSet;
  bool    FontUnderline;
};
TypeIsMovable(DefaultConfigItem);

//! container for FSMConfig.cfg values
class DefaultConfigInfo
{
public:
  AutoArray<DefaultConfigItem> items;
  bool valid;

  bool SerializeItem(int ix, DefaultConfigItem &nfo, ParamArchive &ar);
  bool Load(ParamArchive &ar);
  void Clear() {items.Clear();}
};

//! Base class used to read data from *.bifsm format generated by FSMEditor
class FSMEdGraph : public SerializeClass
{
public:
  //! array of items
  AutoArray<FSMItem> vertices;
  //! array of links
  AutoArray<FSMLink> edges;
  //! array of items decompile info
  AutoArray<FSMDecompileInfo> decompileInfo;
  FSMDecompileInfoGlobal decompileInfoGlobal;
  FSMDecompileInfoWindow decompileInfoWindow; 
  //! FSMConfig.cfg infos
  DefaultConfigInfo dconfigInfo;
  //! read (or write) it
  LSError Serialize(ParamArchive &ar);
  //! based on project FSMEditor, CGraphCtrlExt::SerializeItem(...)
  bool SerializeItem(int ix, FSMItem &nfo, ParamArchive &ar);
  //! based on project FSMEditor
  bool SerializeLink(int ix, FSMLink &lnk, ParamArchive &asect);
  //! eliminates knees, add new edges instead of knees
  bool EliminateKnees();
  //! compute output degree
  int DegOut(int vertIx);
  //! compute input degree
  int DegIn(int vertIx);
  //! compute in/out degree
  int Deg(int vertIx);
  //! generates FSMGraph, i.e. Compilation
  //! report generated
  FSMGraph *ConvertToFSMGraph(Report &report, AutoArray<FSMLink> &links, bool isCompiling);
  //! print edges for debug purposes
  void PrintEdges();
  //! delete FAKE state and every links connected with it
  void DeleteFakeState();
};

//global helper functions
RString FindFilePath(RString fname);

#define DECOMPILE_INFO_ITEM_SIZE     9
#define DECOMPILE_INFO_GLOBALS_SIZE  18
#define DECOMPILE_INFO_WINDOW_SIZE   11

#endif

