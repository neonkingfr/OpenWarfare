#ifndef _PARAM_FILE_COMPILER_H
#define _PARAM_FILE_COMPILER_H

#include "FSMGraph.h"
#include <Es/Containers/listBidir.hpp>

typedef enum 
{
  CI_ROOT /*for class Compile*/, CI_PASS, CI_STATE, CI_LINK, CI_FINAL_STATES, CI_PRINT, CI_PRESUF_CMD, 
  CI_INDENT, CI_IFSTART, CI_IFFINAL, CI_IFFIRST, CI_IFTOFINAL
} CompileItemType;

//! abstract class for any Compile item specified in paramFile configuration
class CompileItemBase
{
public:
  CompileItemType type;
  virtual ~CompileItemBase() {};
};

//! print varNames, %modifier(varName)
typedef enum
{ //Print Format Item
  PFI_PRIORITY, PFI_STATETEXT, PFI_STATENAME, PFI_LINKTEXT, PFI_LINKNAME, PFI_CONDITION, PFI_ACTION, 
  PFI_STATE_INIT, PFI_TO, PFI_TO_TEXT, PFI_INITSTATENAME, PFI_FINALSTATENAME, 
  PFI_NUM_STATES, PFI_NUM_CONDITIONS, PFI_NUM_FINAL_STATES,
  PFI_STATE_PRECONDITION, PFI_COND_PRECONDITION, PFI_FSMNAME,
  PFI_GRAPH_ITEM_NO,
  PFI_ITEM_NUMBER
} PrintFormatItem;
static const char* PrintItemsNames[PFI_ITEM_NUMBER] = {"priority", "statetext", "statename", "linktext", "linkname", "condition", 
"action", "stateinit", "to", "totext", "initstatename", "finalstatename", 
  "numstates", "numconditions", "numfinalstates", 
  "stateprecondition", "condprecondition", "fsmname", "itemno"};
static const bool ItemIsWritable[PFI_ITEM_NUMBER] = { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0 };

//! print Modifiers, %modifier(varName)
typedef enum
{ // Print Modifier Item
  PMI_QTQUOTED, PMI_QUOTED, PMI_QUOTE_IT, PMI_XML, PMI_ITEM_NUMBER
} PrintModifierItem;
static const char *PrintModifierNames[] = {"qtquoted", "quoted", "qt", "xml"};

//! each CompilePrint contains for each formatString some PrintInfo, 
//! to specify, variable substitution or/and text modifier
struct PrintInfo
{
public:
  //! specify variable to substitute
  PrintFormatItem   printFormat;
  //! specify modifier, which will be used to filter text (PMI_ITEM_NUMBER == no modifier)
  PrintModifierItem printModifier;

  PrintInfo() { printFormat=PFI_STATE_INIT; printModifier=PMI_ITEM_NUMBER; }
  PrintInfo(int format, int modifier) 
  {
    printFormat = PrintFormatItem(format);
    printModifier = PrintModifierItem(modifier);
  }
};
TypeIsMovable(PrintInfo);

//! contains one formatString/formatItemInfo for each %modifier(varName)
//! and possibly one formatString for the rest of print (text aftre last %modifier(varName))
class CompilePrint : public CompileItemBase
{
  //for every parameter, one format string
  AutoArray<RString> formatStrings;
  AutoArray<PrintInfo> formatItemInfos;
public:
  static FSMGraph *graph;
  bool hprint;
  RString tagName;
  CompilePrint(RString text, RString name, bool hprint=false);
  virtual ~CompilePrint() {}
  //! returns formated text for FSMGraph::current_values
  RString FormatCurrent();
};

class CompilePreSufCmd : public CompileItemBase
{
public:
  typedef enum {CMD_PRESUF_CHANGE, CMD_PRESUF_SAVE, CMD_PRESUF_LOAD} CmdType;
  CmdType cmdType;
  RString *strptr; //pointer to string in DecompileInfo, which will be changed
  RString newval;  //*strptr = newval, after processing commadn CMD_PRESUF_CHANGE

  CompilePreSufCmd(RString *strp, RString val, CmdType type);
  void ProcessCmd();
};

class IndentItem : public CompileItemBase
{
public:
  int indent;
  IndentItem(int indt) { indent=indt; type=CI_INDENT; }
  void UpdateIndent();
};
class IfStartItem : public CompileItemBase
{
public:
  int onOff;
  IfStartItem(int onOff1) { onOff=onOff1; type=CI_IFSTART; }
  void Update();
};
class IfFinalItem : public CompileItemBase
{
public:
  int onOff;
  IfFinalItem(int onOff1) { onOff=onOff1; type=CI_IFFINAL; }
  void Update();
};
class IfToFinalItem : public CompileItemBase
{
public:
  int onOff;
  IfToFinalItem(int onOff1) { onOff=onOff1; type=CI_IFTOFINAL; }
  void Update();
};
class IfFirstItem : public CompileItemBase
{
public:
  int onOff;
  IfFirstItem(int onOff1) { onOff=onOff1; type=CI_IFFIRST; }
  void Update();
};

class CompileItem;
class CompilePrint;
class CompileItemElement : public TLinkBidirD
{
public:
  //! item is pointer to CompileItem instance (for class item) or CompilePrint (for print item)
  CompileItemBase *item;
  CompileItemElement() : TLinkBidirD() {item=NULL;}
  CompileItem *GetCompileItem() {return (CompileItem*)item;}
  CompilePrint *GetCompilePrint() {return (CompilePrint*)item;}
  CompilePreSufCmd *GetCompilePreSufCmd() {return (CompilePreSufCmd*)item;}
};

//! any configuration class reprezentation (Compile, Pass_nn, State, Link, FinalStates, ...)
class CompileItem : public CompileItemBase
{
public:
  TListBidir<CompileItemElement> items;
  bool nodecompile;
  CompileItem(CompileItemType typ) {type = typ; nodecompile=false;}
  //! it must delete all items[ix].item (tree structure destructor)
  virtual ~CompileItem();
  void AddPrint(RString text, RString name, bool hprint=false);
  //! returns CompileItemElement, which should be filled with other elements (classes, prints)
  CompileItemElement *AddClass(CompileItemType type);
  void AddPreSufCmd(CompilePreSufCmd::CmdType tp, RString *strptr, RString val);
  void AddIndentItem(int indent);
  void AddIfStartItem(int onOff);
  void AddIfFinalItem(int onOff);
  void AddIfToFinalItem(int onOff);
  void AddIfFirstItem(int onOff);
};

class PassItem : public CompileItem
{
public:
  RString passText;
  PassItem(CompileItemType typ) : CompileItem(typ) {}
  virtual ~PassItem() {}
};

typedef enum
{
  DCI_PASS, DCI_STATES, DCI_STATE, DCI_LINKS, DCI_LINK, DCI_FINAL_STATES, 
  DCI_HPRINT, DCI_NODECOMPILE, DCI_COMPILE, DCI_HEAD, DCI_APPEND,
  DCI_CLASS_NUMBER, DCI_STATIC_TEXT, DCI_ROOT
} DecompileClass;
static const char *DecompileClassNames[] = 
{
  "pass", "states", "state", "links", "link", "finalstates", "hprint", "nodecompile", "compile", "head", "append"
};
class ParamEntryVal;
class DecompileInfo
{
public:
  bool    process;
  RString left, right; //default values are "/*" and "*/"
  int indent; //how many spaces to print before each line
  int ifStart, ifFinal, ifToFinal; //-1,0,1 section should be processed only not start, all, or start states (the very same for final)
  int ifFirst; //-1,0,1 section should be processed only not first, all, or first state in the list
  bool clearNewLines;
  
  //classes
  TPreSuf classValues[DCI_CLASS_NUMBER];
  //values
  TPreSuf itemValues[PFI_ITEM_NUMBER];
  DecompileInfo() {indent=0;ifStart=ifFinal=ifToFinal=0;ifFirst=0;clearNewLines=false;};
  DecompileInfo(const ParamEntry &);
  //!ix=0 for prefix, ix=1 for sufix, sada=0 for sufix/prefix, sada=1 for sufix2/prefix2
  void ReadPreSufixes(ParamEntryVal &, int ix, int sada); 
  RString Write(int ix, int modix, bool isClass, bool isEnd, RString val, bool omitNewLinePrefix=false, bool useIndent=false);
  //! save decompile info into savedDecompileInfo global variable
  void Save();
  //! load decompile info from savedDecompileInfo global variable
  void Load();
};
extern DecompileInfo savedDecompileInfo;

static const char *FSMSTR_ID[] = {"%FSM<",">"};

//global helper functions
void IndentText(RString &text, int indent);

#endif
