/*
  ParamArchiveForBeginners only shows usage of the basic:
 1. ParamFile methods
  It creates some paramFile (name must be supplied as commandline parameter)
  and converts it to xml (manually).
 2. ParamArchive methods
  It creates some paramArchive, save it and read it again (also without saving to file)
 3. working with QStdStream
*/


// FSMCompiler.cpp : Defines the entry point for the console application.
//

#define EXE_IS_PARAM_ARCHIVE_TEST     
//#define EXE_IS_PARAM_FILE_FOR_RABBITS 
//#define EXE_IS_QStdInStreamTest       
//#define EXE_IS_QStdOutStreamTest      

#include <El/QStream/QStdStream.hpp>
#include <El/elementpch.hpp>
#include <El/ParamFile/paramFile.hpp>
#include <Es/Strings/rstring.hpp>
#include <Es/Strings/bstring.hpp>
#include <Es/Framework/consoleBase.h>
#include <io.h>

static void CreateParamFile(const char* fileName)
{
  ParamFile f;
  f.Add("myStringEntry", "myStringValue");
  ParamClassPtr ptr = f.AddClass("myFirstClassssss");
  ParamClassPtr ptr2 = f.AddClass("myFirstClassssss"); //the same class, so ptr==ptr2
  if (ptr==ptr2) {
    ptr->Add("parentEntry", 200);
    ParamEntryPtr array = ptr->AddArray("myFirstArray");
    //IParamArrayValue *valArr = array->AddArrayValue();
    array->AddValue("ptr");
    array->AddValue("equals");
    array->AddValue("ptr2");
    ParamClassPtr subclass = ptr->AddClass("firstSubclass");
    subclass->Add("onefivethree", 153);
    subclass->Add("onepointfivethree", 1.53f);
  }
  else
  {
    ptr->Add("message","ptr does not equal to ptr2");
  }
  f.Save(fileName);
}

static RString OneTab = "\t";
BString<2048> outstr;
static void PrintAsXMLRecursive(const ParamEntry &entry, QOStream &out, RString indentS=RString())
{
  // print any entry that has "comment" member present
  // when printing, print also "scope" member (if present)
  RString indentS1 = indentS+OneTab;
  int i;
  for (i=0; i<entry.GetEntryCount(); i++)
  {
    const ParamEntry &e = entry.GetEntry(i);
    if (!e.IsClass()) 
    {
      if (e.IsArray())
      {
        outstr.PrintF("%s<array name=\"%s\">\n", indentS.Data(), e.GetName().Data()); out<<outstr;
        for (int ix=0; ix<e.GetSize(); ix++)
        {
          outstr.PrintF("%s<value>%s</value>\n", indentS1.Data(), e[ix].GetValue().Data());  out<<outstr;
        }
        outstr.PrintF("%s</array>\n", indentS.Data()); out<<outstr;
      }
      else {
        RString entryType;
        if (e.IsFloatValue()) 
        {
          entryType="float";
          //float val = float(e);  //possible
        }
        if (e.IsTextValue()) entryType="text";
        if (e.IsIntValue()) entryType="int";
        outstr.PrintF
        (
          "%s<entry name=\"%s\" type=\"%s\" val=\"%s\"/>\n", 
          indentS.Data(), e.GetName().Data(), entryType.Data(), e.GetValue().Data()
        ); out<<outstr;
      }
    }
    else
    {
      outstr.PrintF("%s<class name=\"%s\">\n", indentS.Data(), e.GetName().Data());  out<<outstr;
      PrintAsXMLRecursive(e, out, indentS1);
      outstr.PrintF("%s</class>\n", indentS.Data());  out<<outstr;
    }
  }
}

static void PrintAsXML(const ParamEntry &entry, QOStream &out)
{
  outstr.PrintF("<?xml version=\"1.0\" ?>\n" "<paramfile>\n"); out<<outstr;
  PrintAsXMLRecursive(entry, out, OneTab);
  outstr.PrintF("</paramfile>\n"); out<<outstr;
}

void TryFinding(const char* fileName)
{
  ParamFile in;
  in.Parse(fileName);
  ParamEntryPtr ptr1 = in.FindEntry("onefivethree");
  ParamEntryPtr ptr2 = in.FindEntryNoInheritance("onefivethree");
  ParamEntryPtr ptr3 = in.FindEntryPath("myFirstClassssss/firstSubclass");
  ParamEntryPtr ptr4;
  if (ptr3) ptr4 = ptr3->FindEntry("oneFivetHree"); //yes, its case insensitive
  if (ptr4 && ptr4->IsIntValue())
    printf("int valued parentEntry found\n");
  else 
    printf("int valued parentEntry not found\n");
}

#ifdef EXE_IS_PARAM_FILE_FOR_RABBITS

int consoleMain(int argc, const char *argv[])
{
  if (argc!=2) {printf("ussage: FSMCompiler fileToCreateName"); return 1; };
  CreateParamFile(argv[1]);
  ParamFile in;
  in.Parse(argv[1]);
/*
  if (in.Parse("cfgVehicles.hpp")==LSOK)  //cfgVehicles can be well parsed with Cpreprocessing (#including manActions.hpp)
    _asm nop;
*/
  QStdDebugStream outStream;
  //QStdOutStream outStream;
  //QStdErrStream outStream;
  //QOFStream outStream("test.xml");
  PrintAsXML(in, outStream);
  //TryFinding(argv[1]);
  #if _DEBUG
    wasError = true; //force "Pres ENTER to continue"
  #endif
  return 0;
}

#endif
#ifdef EXE_IS_PARAM_ARCHIVE_TEST
// ParamArchiveTest.cpp : Defines the entry point for the console application.
//
//#include <El/elementpch.hpp>
#include <El/ParamArchive/paramArchive.hpp>
#include <El/ParamArchive/paramArchiveDb.hpp>

class MyPokus : public SerializeClass
{
public:
  int _value;
  MyPokus(int val=0) : _value(val) {}
  LSError Serialize(ParamArchive &ar) {return ar.Serialize("myValue", _value, 1);}
};
#define NOT_SAVING
int consoleMain(int argc, const char* argv[])
{
  //ParamArchiveSave example
  ParamArchiveSave *arsptr = new ParamArchiveSave(1);
  ParamArchiveSave &ars = *arsptr;
  ParamArchive arCls;
  ars.OpenSubclass("pokus", arCls);
  int i=55;
  arCls.Serialize("polozka", i, 1);
  ars.CloseSubclass(arCls);
  
  MyPokus pokus(153);
  ars.Serialize("MyPokusBlablabla", pokus, 1);
  
  float x = 100;
  ars.Serialize("x", x, 1);
#ifndef NOT_SAVING
  ars.Save("test.ar");          //and save it to file!
#endif

  //ParamArchiveLoadExample
#ifndef NOT_SAVING
  ParamArchiveLoad arl("test.ar");
#else
  ParamArchiveLoad arl(ars);
#endif
  delete arsptr;
  ParamArchive arCll;
  int ix=0;
  if (arl.OpenSubclass("pokus", arCll))
  {
    arCll.Serialize("polozka", ix, 1);
    arl.CloseSubclass(arCll);
  }
  else
  { //it can be error, but not necessary
    LSError err = arCll.ErrorNoEntry();
    arCll.OnError(err, "pokus");
    return err;
  }

  MyPokus pokus2;
  arl.Serialize("MyPokusBlablabla", pokus2, 1);
  
  float y;
  arl.Serialize("x", y, 1);
  QStdDebugStream outStream;  
  outstr.PrintF("ix = %d\n", ix); outStream << outstr;
  outstr.PrintF("pokus2._value = %d\n", pokus2._value); outStream << outstr;
  outstr.PrintF("y = %f\n", y); outStream << outstr;
  return 0;
}

#endif 
#ifdef EXE_IS_QStdOutStreamTest

//test of QStdOutStream
int consoleMain(int argc, const char *argv[])
{
QStdOutStream mystdout;
BString<2048> numstr;
for(int i=0; i<10000; i++) //test, forcing QStream to call Flush more then once
{
numstr.PrintF("Dalsi cislo je: %d\n", i);
mystdout<<numstr;
}
mystdout<<"a ahoj na zaver!"<<"\n";
mystdout.close();
return 0;
}

#endif
#ifdef EXE_IS_QStdInStreamTest

//test of QStdInStream

int consoleMain( int argc, const char *argv[] )
{
  QStdInStream in;

  char line[1024];
  while (in.readLine(line,sizeof(line)))
  {
    puts(line);
  }
#if _DEBUG
  wasError = true;
#endif
  return 0;
}

#endif

